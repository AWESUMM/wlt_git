!
! Utility routines that act on vectors
!

MODULE vector_util_mod
  USE debug_vars
  USE wlt_vars
  USE wlt_trns_mod

  IMPLICIT NONE
CONTAINS

FUNCTION div ( f, nlocal, jlev, meth)
  USE precision
  USE pde
  IMPLICIT NONE

  INTEGER,                            INTENT (IN) :: nlocal, jlev, meth
  REAL (pr), DIMENSION (nlocal, dim), INTENT (IN) :: f
  REAL (pr), DIMENSION (nlocal)    :: div

  INTEGER                            :: idim
  REAL (pr), DIMENSION (dim,nlocal,dim) :: df, d2f

  CALL c_diff_fast (f(:,1:dim), df, d2f, jlev, nlocal, meth, 10, dim, 1, dim)
  
  div = 0.0_pr
  DO idim = 1, dim
     div = div + df(idim,:,idim)
  END DO

END FUNCTION div


FUNCTION grad (f, nlocal, jlev, meth)
  USE precision
  USE pde
  IMPLICIT NONE

  INTEGER,                       INTENT (IN) :: nlocal, jlev, meth
  REAL (pr), DIMENSION (nlocal), INTENT (IN) :: f
  REAL (pr), DIMENSION (nlocal,dim)          :: grad

  REAL (pr), DIMENSION (nlocal, dim) :: d2f
!df unused variable !!


  CALL c_diff_fast (f, grad, d2f, jlev, nlocal, meth, 10, 1, 1, 1) !--Forward difference
END FUNCTION grad


SUBROUTINE cal_vort (u, w, nlocal)
  USE precision
  IMPLICIT NONE
  INTEGER, INTENT (IN) :: nlocal  
  REAL (pr), DIMENSION (nlocal,dim) :: u
  REAL (pr), DIMENSION (nlocal,3-MOD(dim,3)), INTENT (OUT) :: w 
  INTEGER, PARAMETER :: meth=1

  w = curl ( u , nlocal, j_lev, meth)
END SUBROUTINE cal_vort

! Calculate the vorticity from the gradient of u; an optimization for performing
! multiple computations without repeated derivatives
SUBROUTINE cal_vort_velgrad (grad_u, w, nlocal)
  USE precision
  IMPLICIT NONE
  INTEGER, INTENT (IN) :: nlocal  
  REAL (pr), DIMENSION (dim,nlocal,dim) :: grad_u
  REAL (pr), DIMENSION (nlocal,3-MOD(dim,3)), INTENT (OUT) :: w 
  INTEGER, PARAMETER :: meth=1

  IF(dim==1) THEN
      PRINT *, 'ERROR: vorticity is not defined in one dimension'
      STOP
  ELSE IF(dim==2) THEN
     w(:,1) = grad_u(2,:,1) - grad_u(1,:,2)
  ELSE IF(dim==3) THEN
     w(:,1) =  grad_u(3,:,2) - grad_u(2,:,3)   
     w(:,2) =  grad_u(1,:,3) - grad_u(3,:,1)
     w(:,3) =  grad_u(2,:,1) - grad_u(1,:,2)
  END IF
END SUBROUTINE cal_vort_velgrad

! Calculate the magnitude of vorticity from the gradient of u; an optimization for performing
! multiple computations without repeated derivatives
SUBROUTINE cal_magvort_velgrad (grad_u, wmod, nlocal)
  USE precision
  IMPLICIT NONE
  INTEGER, INTENT (IN) :: nlocal  
  REAL (pr), DIMENSION (dim,nlocal,dim) :: grad_u
  REAL (pr), DIMENSION (nlocal), INTENT (OUT) :: wmod 
  INTEGER, PARAMETER :: meth=1

  IF(dim==1) THEN
      PRINT *, 'ERROR: vorticity is not defined in one dimension'
      STOP
  ELSE IF(dim==2) THEN
     wmod(:) = ABS( grad_u(2,:,1) - grad_u(1,:,2) )
  ELSE IF(dim==3) THEN
     wmod(:) = SQRT( ( grad_u(3,:,2) - grad_u(2,:,3) ) * ( grad_u(3,:,2) - grad_u(2,:,3) ) &
                + ( grad_u(1,:,3) - grad_u(3,:,1) ) * ( grad_u(1,:,3) - grad_u(3,:,1) ) &
                + ( grad_u(2,:,1) - grad_u(1,:,2) ) * ( grad_u(2,:,1) - grad_u(1,:,2) ) ) 
  END IF

END SUBROUTINE cal_magvort_velgrad

FUNCTION curl ( f, nlocal, jlev, meth)
  USE precision
  USE pde
  USE share_consts
  USE util_vars
  IMPLICIT NONE

  INTEGER,                           INTENT (IN) :: nlocal, jlev, meth
  REAL (pr), DIMENSION (nlocal,dim), INTENT (IN) :: f
  REAL (pr), DIMENSION (nlocal,3-MOD(dim,3))   :: curl !curl of f
                                                       ! if dim=2 then w(1), dim==3 then w(1:3)  REAL (pr), DIMENSION (dim,nlocal,dim) :: df , d2f
  REAL (pr), DIMENSION (dim,nlocal,dim) :: df, d2f

  IF(dim==1) THEN
      PRINT *, 'ERROR: curl is not defined in one dimension'
      STOP
  ELSE IF(dim==2) THEN
     !
     ! find first derivative
     ! (second deriviative not done so pass df twice as a place filler for d2f)
     !
     CALL c_diff_fast (f(:,1:dim), df, d2f, jlev, nlocal,meth, 10, dim, 1, dim)

      curl(:,1) = df(2,:,1) - df(1,:,2)
  ELSE IF(dim==3) THEN

     !
     ! find first derivative
     ! (second deriviative not done so pass df twice as a place filler for d2f)
     !
     CALL c_diff_fast (f(:,1:dim), df, d2f, jlev, nlocal,meth, 10, dim, 1, dim)

     curl(:,1) =  df(3,:,2) - df(2,:,3)   
     curl(:,2) =  df(1,:,3) - df(3,:,1)
     curl(:,3) =  df(2,:,1) - df(1,:,2)
  END IF
END FUNCTION curl

!
! Do the cross product in 3d of u^v
!
FUNCTION cross_prod(u, v, nlocal)
  USE precision
  USE pde
  IMPLICIT NONE

  INTEGER                            :: nlocal
  REAL (pr), DIMENSION (nlocal,dim)  :: u, v
  REAL (pr), DIMENSION (nlocal,dim)  :: cross_prod

  cross_prod(:,:) = 0.0_pr
  IF (dim == 2) THEN
     cross_prod(:, 1) = u(:, 1)*v(:, 2) - u(:, 2)*v(:, 1)
     ! second dimension is redundant and is 0
  ELSE IF (dim ==3) THEN
     cross_prod(:,1) = u(:,2)*v(:,3) - u(:,3)*v(:,2)
     cross_prod(:,2) = u(:,3)*v(:,1) - u(:,1)*v(:,3)
     cross_prod(:,3) = u(:,1)*v(:,2) - u(:,2)*v(:,1)
  ELSE
     PRINT *,'ERROR: cross product defined in 2d(is a number) or 3d(vector)'
  END IF

END FUNCTION cross_prod

FUNCTION cross_prod_single(u,v)
  REAL (pr), DIMENSION(dim) :: u,v
  REAL (pr), DIMENSION(dim) :: cross_prod_single

  cross_prod_single = 0.0_pr
  IF (dim == 2) THEN
     cross_prod_single(1) = u(1)*v(2) - u(2)*v(1)
  ELSE IF (dim == 3) THEN
     cross_prod_single(1) = u(2)*v(3) - u(3)*v(2)
     cross_prod_single(2) = u(3)*v(1) - u(1)*v(3)
     cross_prod_single(3) = u(1)*v(2) - u(2)*v(1)
  ELSE
     PRINT *, 'Error: Cross product not defined for 1D'
  END IF
END FUNCTION cross_prod_single

!
! Calculate 6 unique components of Sij
!
! return result in s
! s(:,1) = s_11
! s(:,2) = s_22
! s(:,3) = s_33
! s(:,4) = s_12
! s(:,5) = s_13
! s(:,6) = s_23
!
! smon(:) == ||S||
SUBROUTINE Sij( u, nlocal, s, smod, traceless) 
  USE precision
  USE parallel
  IMPLICIT NONE
  INTEGER,    INTENT (IN) :: nlocal
  REAL (pr), DIMENSION (1:nlocal,1:dim) , INTENT (IN) :: u
  LOGICAL, INTENT(IN) :: traceless
  REAL (pr), DIMENSION (1:nlocal,dim*(dim+1)/2) , INTENT (OUT):: s
  REAL (pr), DIMENSION (1:nlocal) , INTENT (OUT):: smod

  REAL (pr), DIMENSION (dim,1:nlocal,dim) ::du, d2u

  INTEGER, PARAMETER :: meth=1 ! used meth=1 to match calc_vort()
  
  IF( BTEST(debug_level,1) ) THEN
     IF (par_rank.EQ.0) THEN
        WRITE(*,'(A)') "  "
        WRITE(*,'(A)') "Debug In Sij()" 
     END IF
  END IF
  
  ! dui/dxj
  CALL c_diff_fast (u, du, d2u, j_lev, nlocal ,meth, 10, dim, 1, dim)  

  ! Compute the strain rate tensor from the gradient of u
  CALL Sij_velgrad( du, nlocal, s, smod, traceless)
  
END SUBROUTINE Sij


!
! Calculate 6 unique components of Sij from gradient of u: optimization to
! combine multiple computations without repeated derivatives
!
! return result in s
! s(:,1) = s_11
! s(:,2) = s_22
! s(:,3) = s_33
! s(:,4) = s_12
! s(:,5) = s_13
! s(:,6) = s_23
!
! smon(:) == ||S||
SUBROUTINE Sij_velgrad( grad_u, nlocal, s, smod, traceless) 
  USE precision
  USE parallel
  IMPLICIT NONE
  INTEGER,    INTENT (IN) :: nlocal
  REAL (pr), DIMENSION (1:dim,1:nlocal,1:dim) , INTENT (IN) :: grad_u
  LOGICAL, INTENT(IN) :: traceless
  REAL (pr), DIMENSION (1:nlocal,dim*(dim+1)/2) , INTENT (OUT):: s
  REAL (pr), DIMENSION (1:nlocal) , INTENT (OUT):: smod

  INTEGER :: i,j,k, idim, nS
  REAL(pr) :: tmp1, tmp2
  
  IF( BTEST(debug_level,1) ) THEN
     IF (par_rank.EQ.0) THEN
        WRITE(*,'(A)') "  "
        WRITE(*,'(A)') "Debug In Sij_velgrad()" 
     END IF
  END IF
  
  nS = dim*(dim+1)/2

  k = 0
  DO i = 1, dim
     s(:,i) = grad_u(i,:,i) ! s_ii
     DO j = i+1, dim
        k= k + 1
        s(:,dim+k) = 0.5_pr * ( grad_u(i,:,j) + grad_u(j,:,i) ) ! 0.5* (dui/dxj + dui/dxi)
     END DO
  END DO
  
  !
  ! Subtract trace 
  ! only makes sense in dim=3 case
  !
  IF(traceless) THEN
     Smod = SUM( s(:,1:dim), DIM=2 ) / REAL(dim,pr) ! trace
     DO idim = 1, dim	
        s(:,idim) = s(:,idim) - Smod ! makes it traceless
     END DO
  END IF

  Smod =  SQRT( 2.0_pr*SUM( s(:,1:dim)**2, DIM=2) + &
                4.0_pr*SUM( s(:,dim+1:nS)**2, DIM=2) )
  
  !debug
  IF( BTEST(debug_level,1) ) THEN
     DO i=1,nS
        tmp1 = MINVAL(s(:,i)); tmp2 = MAXVAL(s(:,i))
        CALL parallel_global_sum (REALMINVAL=tmp1)
        CALL parallel_global_sum (REALMAXVAL=tmp2)
        IF (par_rank.EQ.0) WRITE(*,'(A,2(e15.7,1x))') "MINMAXVAL(sij) = ", tmp1, tmp2
     END DO
     tmp1 = MINVAL(Smod(:)); tmp2 = MAXVAL(Smod(:))
     CALL parallel_global_sum (REALMINVAL=tmp1)
     CALL parallel_global_sum (REALMAXVAL=tmp2)
     IF (par_rank.EQ.0) THEN
        WRITE(*,'(A,2(e15.7,1x))') "MINMAXVAL(Smod) = ", tmp1, tmp2
        WRITE(*,'(A)') "END Debug In Sij()"
        WRITE(*,'(A)') "  "
     END IF
  END IF
  
END SUBROUTINE Sij_velgrad

!
! Calculate modu of Sij from gradient of u: optimization to
! combine multiple computations without repeated derivatives
!
! smod(:) == ||S||
SUBROUTINE modSij_velgrad( grad_u, nlocal, smod, traceless) 
  USE precision
  USE parallel
  IMPLICIT NONE
  INTEGER,    INTENT (IN) :: nlocal
  REAL (pr), DIMENSION (1:dim,1:nlocal,1:dim) , INTENT (IN) :: grad_u
  LOGICAL, INTENT(IN) :: traceless
  REAL (pr), DIMENSION (1:nlocal) , INTENT (OUT):: smod

  REAL (pr), DIMENSION (1:nlocal) :: smod_trace
  INTEGER :: i,j,k

  Smod(:) = 0.0_pr
  Smod_trace = 0.0_pr
  IF(traceless) THEN
     DO i = 1, dim
        Smod_trace(:) = Smod_trace(:) + grad_u(i,:,i) / REAL( dim, pr ) 
     END DO
  END IF 
  DO i = 1, dim
     Smod(:) = Smod(:) + 2.0_pr * ( grad_u(i,:,i) - Smod_trace(:) ) * ( grad_u(i,:,i) - Smod_trace(:) )  ! Diagonal terms
     DO j = i+1, dim
        Smod(:) = Smod(:) + 1.0_pr * ( grad_u(i,:,j) + grad_u(j,:,i) ) * ( grad_u(i,:,j) + grad_u(j,:,i) ) 
     END DO
  END DO
  Smod(:) = SQRT( Smod(:) )

END SUBROUTINE modSij_velgrad

!
! Calculate 3 unique components of Wij
!
! return result in s
! w(:,1) = W23=-0.5*curl(u)_1
! w(:,2) = W31=-0.5*curl(u)_2
! w(:,3) = W12=-0.5*curl(u)_3
!
! Wmod(:) == ||W||
SUBROUTINE Wij( u, nlocal, W, Wmod) 
  USE precision
  USE parallel
  IMPLICIT NONE
  INTEGER,    INTENT (IN) :: nlocal
  REAL (pr), DIMENSION (1:nlocal,1:dim) , INTENT (IN) :: u
  REAL (pr), DIMENSION (1:nlocal,3-MOD(dim,3)) , INTENT (OUT):: W  
  REAL (pr), DIMENSION (1:nlocal) , INTENT (OUT):: Wmod

  INTEGER, PARAMETER :: meth=1 ! used meth=1 to match calc_vort()
  INTEGER :: i
  REAL(pr) :: tmp1, tmp2
  

  w = curl ( u , nlocal, j_lev, meth)

  w= -0.5_pr*w


  Wmod =  2.0_pr * SQRT( SUM( w(:,1:3-MOD(dim,3))**2, DIM=2) ) 
 
  !debug
  IF( BTEST(debug_level,1) ) THEN
     IF (par_rank.EQ.0) THEN
        WRITE(*,'(A)') "  "
        WRITE(*,'(A)') "Debug In Wij()" 
     END IF
     DO i=1,dim
        tmp1 = MINVAL(W(:,i)); tmp2 = MAXVAL(W(:,i))
        CALL parallel_global_sum (REALMINVAL=tmp1)
        CALL parallel_global_sum (REALMAXVAL=tmp2)
        IF (par_rank.EQ.0) WRITE(*,'(A,2(e15.7,1x))') "MINMAXVAL(Wij) = ", tmp1, tmp2
     END DO
     IF (par_rank.EQ.0) THEN
        WRITE(*,'(A,2(e15.7,1x))') "MINMAXVAL(Wmod) = ", MINVAL(Wmod(:)),MAXVAL(Wmod(:))
        WRITE(*,'(A)') "END Debug In Wij()"
        WRITE(*,'(A)') "  "
     END IF
  END IF
  
END SUBROUTINE Wij

END MODULE vector_util_mod
