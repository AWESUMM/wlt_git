MODULE lagrangian

USE precision
USE parallel
USE io_3d_vars
USE wlt_vars !for prd variable (periodic boundary conditions)


IMPLICIT NONE


TYPE clist								!Define a class to store the collsion list information
	REAL(pr) :: tstar
	INTEGER :: i, j
END TYPE clist


INTEGER, PRIVATE :: np, dims, n_ghost						!Declare private variables used only in this module

REAL(pr), PRIVATE :: epsN, tol, rho, epsT, radMax

REAL(pr), ALLOCATABLE, PRIVATE :: bdim(:,:)
REAL(pr), ALLOCATABLE, PRIVATE :: rad(:), mass(:), inertia(:)
REAL(pr), ALLOCATABLE, PRIVATE :: r(:,:), v(:,:), phi(:,:), w(:,:), alpha(:,:), accel(:,:), grav_accel(:,:)
REAL(pr), ALLOCATABLE, PRIVATE :: r_ghost(:,:), v_ghost(:,:), phi_ghost(:,:), w_ghost(:,:),rad_ghost(:)

TYPE(clist), ALLOCATABLE, PRIVATE :: cseq(:)

!**********************************************************************
! Global Variables (private to this module)
!
! np:		Number of particles
! dims:		Dimensiality of the system
! epsN:		Coefficient of restitution
! epsT:		Coefficient of friction
! tol:		Small number to prevent numerical precison issues
! radMax:	Stores the maximum radius of any particle (currently
!		not used)
! bdim:		dimensions of simulation area: must be rectangular
!		bdim(1,:) stores minimum dimensions, bdim(2,:) stores
!		maximum dimensions
! rad(:)	Array storing the radii of the particles
! mass(:)	Mass of the particles
! inertia(:)	Moment of inertia of the particles
! r(:,:)	Position vector of each particle relative to the origin
!		of the domain. First index is the particle number, second
!		is the component (x, y, z)
! v(:,:)	velocities of the particles in vector components
! w(:,:)	angular velocities of the particles (is size w(np,1) for 2D and 
!		w(np,3) for 3D
! accel(:,:)	acceleratoin experienced by the particles due to the 
!		external force applied
! cseq(:)	list of the collsions that will happen in each time step
!		Varibles stored in this type:
!		tstar		time of next collsion relative to beginning of time step
!		i		index of the first particle involved
!		j		index of other particle (if wall, indexes are defined as:
!				j=-1: maximum x- wall
!				j=-2: maximum y- wall
!				j=-3: maximum z- wall
!				j=-4: minimum x- wall
!				j=-5: minimum y- wall
!				j=-6: minimum z- wall

!*******  
  ! variables for the lagrangian module
  INTEGER :: num_particle, iiii !iiii is used in initialization loop
  REAL(KIND=8), ALLOCATABLE :: r_particle(:,:), v_particle(:,:), F_particle(:,:)
  REAL(KIND=8), ALLOCATABLE :: rad_particle(:), w_particle(:,:), mass_particle(:)
!**************



CONTAINS

!**********************************************************************
! This function propogates a system of particles through a time dt
! taking all collsions into account
!
! Inputs:
! Fext:		External forces passed into this subroutine (will be 
!		written to the variable F)
! dt:		amount of time to be simulated
! simTime:	current simulation time (not modified, only for output
!		purposes)
! 
! Outputs:
! rExt:		positions of the particles at end of simulation time
! vExt:		velocities of the particles at end of simulation time
! wExt:		angular velocities of the particles at end of 
!		simulation time
!**********************************************************************

SUBROUTINE timeDrivenMDstep(dt,Fext,Text,simTime,rExt,vExt,phiExt,wExt)

IMPLICIT NONE

REAL(pr), INTENT(IN) :: dt, Fext(np,dims), simTime, Text(np,2*dims-3)
REAL(pr), INTENT(INOUT) :: rExt(np,dims), vExt(np,dims), phiExt(np,2*dims-3), wExt(np,2*dims-3)


INTEGER :: i, num
REAL(pr) :: t0, t1, t2
LOGICAL :: OK, LASTOK, init_overlap

! write the input positions and velocities to the global variables
r(:,:) = rExt(:,:)
v(:,:) = vExt(:,:)
phi(:,:) = phiExt(:,:)
w(:,:) = wExt(:,:)

!Check that there are no overlapping particles or partiles outside system
CALL checkoverlap(r,init_overlap,num,prd)
IF (init_overlap) THEN
	WRITE(*,*) "ERROR: Initial particle coordinates cause overlap (with other particle or wall), Exiting"
	STOP
END IF


DO i=1,np
	accel(i,:) = Fext(i,:)/mass(i) + grav_accel(i,:)
        alpha(i,:) = Text(i,:)/inertia(i)  ! angular acceleration
END DO	 !total acceleration is fluid force + gravity
!Place external forces from main program into the private variable F

t0 = 0.0_pr
t1 = t0
t2 = dt
LASTOK = .TRUE.

1001 CALL advanceMD(t2-t1,simTime+t1,OK)

!IF current step was handeled sucessfully
IF (OK) THEN
	!If last step was handeled sucessfully
	IF (LASTOK) THEN
                IF (prd(1)==1 .or. prd(2)==1 .or. prd(dims)==1) THEN
                   CALL addGhostParticles() !add in any ghost particles if periodic boundary conditions are used
                END IF
		rExt(:,:) = r(:,:)							!Writes current positions and velcoities to be passed back to main program
		vExt(:,:) = v(:,:)
                phiExt(:,:) = phi(:,:)
		wExt(:,:) = w(:,:)

#ifdef MULTIPROC !if running in parallel
                !Send particle information to all processors
!                IF (par_rank.EQ.0) THEN  !This if statement doesnt need to be there according to the bcast documentation
                   CALL parallel_broadcast_2D_array(rExt,SIZE(rExt))
                   CALL parallel_broadcast_2D_array(vExt,SIZE(vExt))
                   CALL parallel_broadcast_2D_array(phiExt,SIZE(phiExt))
                   CALL parallel_broadcast_2D_array(wExt,SIZE(wExt))
!                END IF
#endif


		RETURN !return to main program
	ELSE
	T1 = T2 !set starting time to end of last sucessful step
	T2 = dt !try rest of time step
	LASTOK = .TRUE.
	GOTO 1001  !call advanceMD again
	END IF
ELSE
	T2 = 0.5_pr*(t1+t2) !try half of time step
	IF ((t2-t1) < tol) THEN
		WRITE(*,*) "TIME STEP SMALLER THAN TOL (COLLSIONS NOT RESOLVED)"
		STOP
	ELSE
		LASTOK = .FALSE.
		GOTO 1001 !call advanceMD again
	END IF
END IF




END SUBROUTINE timeDrivenMDstep


!**********************************************************************
! This function attemps to advance the simulation to a time t+dt 
! accounting for all collsions. If this routine is unable to process
! the collsion sequence, it sets OK to FALSE and returns the original
! positons and velocities.
!
! Inputs:
! dt:		length of time step to proprogate
! simTime:	simulation time (used only to output the list of 
!		collsion times
!
! Outputs:
! OK:		tells if step was sucessful (system is updated if TRUE
!		and return unchanged if FALSE)
!**********************************************************************

SUBROUTINE advanceMD(dt,simTime,OK)

REAL(pr), INTENT(IN) :: dt, simTime
LOGICAL, INTENT(OUT) :: OK

INTEGER :: i, j, k, ii, jj, numOverlaps

REAL(pr) :: rNew(np,dims), vNew(np,dims), phiNew(np,2*dims-3), wNew(np,2*dims-3)
REAL(pr) :: t1 = 0.0_pr, t2

TYPE(clist) :: cTemp

LOGICAL :: swapped, overlap, overlapii, overlapjj

!store positions and velocities temporarily so that old ones are not overwritten
rNew(:,:) = r(:,:)							
vNew(:,:) = v(:,:)
phiNew(:,:) = phi(:,:)
wNew(:,:) = w(:,:)

t2 = dt	!propagate until time dt


DO i=1,np
	CALL propgation(i,rNew(i,:),vNew(i,:),phiNew(i,:),wNew(i,:),dt)
END DO

CALL checkOverlap(rNew,overlap,numOverlaps,prd)

IF (overlap) THEN
	!Calculate collsion time of all overlaps
	DO i = 1, numOverlaps	
		IF (cseq(i)%j>0) THEN !if pp collsion
			cSeq(i)%tstar = ppcollTime(i,0.0_pr,dt)
		ELSE			!if wall collsion
			cSeq(i)%tstar = pwcollTime(i,0.0_pr,dt)
		END IF
	END DO
	
	!Sort collsion be order of occurance
	DO !Do until fully sorted
	swapped = .FALSE.
	DO i = 1, numOverlaps-1
	swapped = .FALSE.
		IF (cSeq(i)%tstar > cSeq(i+1)%tstar) THEN !If next entry is less, swap
			cTemp = cSeq(i)
			cSeq(i) = cSeq(i+1)
			cSeq(i+1) = cTemp
			swapped = .TRUE.
		END IF
	END DO
	IF (swapped) THEN
		CYCLE
	ELSE
		EXIT
	END IF
	END DO
	
	!Remove any secondary collsions in list
	i = 2
	DO !i=2, numOverlaps - cant user counter-do because index needs to be modified in loop
		DO j = 1, i-1
			IF (cseq(i)%j < 0) THEN  !If wall collsion
			IF (cSeq(i)%i .NE. cSeq(j)%i .AND. &
				&cSeq(i)%i .NE. cSeq(j)%j) CYCLE !if the i particle of jth collsion is not involved in any previous collsions then continue search
			ELSE 
			IF (cSeq(i)%i .NE. cSeq(j)%i .AND. &
				&cSeq(i)%i .NE. cSeq(j)%j .AND. &
				&cSeq(i)%j .NE. cSeq(j)%i .AND. &
				&cSeq(i)%j .NE. cSeq(j)%j) CYCLE
			END IF
			!will only make it to this point if collsion i is a secondary collsion
			!need to delete ith component of cSeq and move others up
			DO k = i, numOverlaps-1
				cSeq(k) = cSeq(k+1)
			END DO
			numOverlaps = numOverlaps-1
			i = i - 1 !need to check the new collsion that was placed in ith spot
			EXIT      !exit inner loop only
		END DO
	i = i + 1
	IF (i > numOverlaps) EXIT
	END DO

	!Handle each collsion in order
	DO i = 1, numOverlaps
		IF (cSeq(i)%j < 0) THEN !If wall collsion
			!restore positions and velocities of iith particle to conditions at beginning of time step
			ii = cSeq(i)%i
			rNew(ii,:) = r(ii,:)
			vNew(ii,:) = v(ii,:)
                        phiNew(ii,:) = phi(ii,:)
                        wNew(ii,:) = w(ii,:)
			CALL propgation(ii,rNew(ii,:),vNew(ii,:),phiNew(ii,:),wNew(ii,:),cSeq(i)%tstar)
			CALL wcollsion(ii,cSeq(i)%j,vNew(ii,:),rNew(ii,:))
			CALL propgation(ii,rNew(ii,:),vNew(ii,:),phiNew(ii,:),wNew(ii,:),dt-cSeq(i)%tstar)	
			CALL checkOverlapI(ii,rNew(:,:),overlapii)
			IF (overlapii) GOTO 1002
		ELSE
			!restore positions and velocities of iith particle to conditions at beginning of time step
			ii = cSeq(i)%i
			jj = cSeq(i)%j
			rNew(ii,:) = r(ii,:)
			vNew(ii,:) = v(ii,:)
                        phiNew(ii,:) = phi(ii,:)
                        wNew(ii,:) = w(ii,:)
			rNew(jj,:) = r(jj,:)
			vNew(jj,:) = v(jj,:)
                        phiNew(jj,:) = phi(jj,:)
                        wNew(jj,:) = w(jj,:)
			CALL propgation(ii,rNew(ii,:),vNew(ii,:),phiNew(ii,:),wNew(ii,:),cSeq(i)%tstar)
			CALL propgation(jj,rNew(jj,:),vNew(jj,:),phiNew(jj,:),wNew(jj,:),cSeq(i)%tstar)
			CALL collsion(ii,jj,rNew(ii,:),rNew(jj,:),vNew(ii,:),vNew(jj,:),wNew(ii,:),wNew(jj,:))
			CALL propgation(ii,rNew(ii,:),vNew(ii,:),phiNew(ii,:),wNew(ii,:),dt-cSeq(i)%tstar)
			CALL propgation(jj,rNew(jj,:),vNew(jj,:),phiNew(jj,:),wNew(jj,:),dt-cSeq(i)%tstar)	
			CALL checkOverlapI(ii,rNew(:,:),overlapii)
			CALL checkOverlapI(jj,rNew(:,:),overlapjj)
			IF (overlapii .OR. overlapjj) GOTO 1002
		END IF
	END DO
END IF

!If sucessfully completed step, update positions and velocities
OK = .TRUE.
r(:,:) = rNew(:,:)							!Writes current positions and velcoities to be passed back to main program
v(:,:) = vNew(:,:)
phi(:,:) = phiNew(:,:)
w(:,:) = wNew(:,:)

!!$!write list of collsions in the current time step
!!$DO i=1,numOverlaps
!!$IF (cseq(i)%j>0) THEN
!!$	CALL ppCollListOutput(simTime+cseq(i)%tstar,cseq(i)%i,cseq(i)%j)
!!$ELSE
!!$	CALL wCollListOutput(simTime+cseq(i)%tstar,cseq(i)%i,cseq(i)%j)
!!$END IF
!!$END DO


RETURN

!goes to this point if collsions not handled properly, OK is set to false and positiions and velocities are restored.
1002 OK = .FALSE.
RETURN !positions and velocities are not changed.


END SUBROUTINE advanceMD

!**********************************************************************
! This function check all pairs of particles and all walls for overlap
! If there is overlap anywhere, it returns true and returns the list of
! overlap partners. 
!
! Inputs:
! r(:,:):	position of particles  (NOT THE GLOBAL VARIABLE)
!
! Outputs:
! overlap:	logical of weither or not particles overlap
! numOverlaps:	number of overlaps detected
!
!**********************************************************************

SUBROUTINE checkOverlap(r,overlap,numOverlaps,periodic_temp)
IMPLICIT NONE

LOGICAL, INTENT(OUT) :: overlap
INTEGER, INTENT(OUT) :: numOverlaps
REAL(pr), INTENT(IN) :: r(np,dims)
INTEGER, INTENT(IN) :: periodic_temp(dims)

REAL(pr) :: rimrj(dims), dist, RRAD
INTEGER :: i, j

overlap = .FALSE.
numOverlaps = 0
cseq(:)%i = 0
cseq(:)%j = 0
cseq(:)%tstar = 0

DO i=1, np
   DO j = 1, i-1
      rimrj(:) = r(i,:)-r(j,:)
      !The next line uses the minimum image criteria to find the closest particle when using periodic boundary conditions
      rimrj(:) = rimrj(:) - periodic_temp(:)*nint(rimrj(:)/(bdim(2,:)-bdim(1,:)))*(bdim(2,:)-bdim(1,:))
      RRAD = rad(i) + rad(j)
      dist = DOT_PRODUCT(rimrj(:),rimrj(:)) - RRAD*RRAD
      IF (dist<0.0_pr) THEN
         overlap = .TRUE.
         numOverlaps = numOverlaps + 1
         cseq(numOverlaps)%i = i
         cseq(numOverlaps)%j = j
      END IF
   END DO
END DO

DO i=1,np
   DO j=1,dims
      IF (periodic_temp(j)==1) THEN !if periodic boundary conditions, check for particles leaving boundary
         !check maximum dimensions of boundary
         IF (r(i,j) > bdim(2,j)) THEN
            overlap = .TRUE.
            numOverlaps = numOverlaps + 1
            cseq(numOverlaps)%i = i
            cseq(numOverlaps)%j = -j
         END IF
         !check minimum dimension of boundary
         IF (r(i,j) < bdim(1,j)) THEN
            overlap = .TRUE.
            numOverlaps = numOverlaps + 1
            cseq(numOverlaps)%i = i
            cseq(numOverlaps)%j = -j-3
         END IF
      ELSE !use non-periodic rules for checking overlap with walls
         !check maximum dimensions of boundary
         IF (r(i,j) + rad(i) > bdim(2,j)) THEN
            overlap = .TRUE.
            numOverlaps = numOverlaps + 1
            cseq(numOverlaps)%i = i
            cseq(numOverlaps)%j = -j
         END IF
         !check minimum dimension of boundary
         IF (r(i,j) - rad(i) < bdim(1,j)) THEN
            overlap = .TRUE.
            numOverlaps = numOverlaps + 1
            cseq(numOverlaps)%i = i
            cseq(numOverlaps)%j = -j-3
         END IF
      END IF
   END DO
END DO	
			


END SUBROUTINE checkOverlap



!**********************************************************************
! This function checks to see if particle ii overlaps with any other 
! particles.
!
! Inputs:
! ii: 		index of particle to check
! rNew:		positions of all particles
!
! Outputs:
! overlapii:	logical of whether or not particles overlap
!
!**********************************************************************

SUBROUTINE checkOverlapI(ii,rNew,overlapii)
IMPLICIT NONE

INTEGER, INTENT(IN) :: ii
REAL(pr), INTENT(IN) :: rNew(np,dims)
LOGICAL, INTENT(OUT) :: overlapii

REAL(pr) :: rimrj(dims), dist, RRAD
INTEGER :: j

overlapii = .FALSE.

!check for any particle-particle overlaps (skip j=i)
DO j = 1, ii-1
	rimrj(:) = rNew(ii,:)-rNew(j,:)
      !The next line uses the minimum image criteria to find the closest particle when using periodic boundary conditions
      rimrj(:) = rimrj(:) - prd(:)*nint(rimrj(:)/(bdim(2,:)-bdim(1,:)))*(bdim(2,:)-bdim(1,:))
	RRAD = rad(ii) + rad(j)
	dist = DOT_PRODUCT(rimrj(:),rimrj(:)) - RRAD*RRAD
	IF (dist< 0.0_pr) THEN
		overlapii = .TRUE.
	END IF
END DO
DO j = ii+1, np
	rimrj(:) = rNew(ii,:)-rNew(j,:)
      !The next line uses the minimum image criteria to find the closest particle when using periodic boundary conditions
      rimrj(:) = rimrj(:) - prd(:)*nint(rimrj(:)/(bdim(2,:)-bdim(1,:)))*(bdim(2,:)-bdim(1,:))
	RRAD = rad(ii) + rad(j)
	dist = DOT_PRODUCT(rimrj(:),rimrj(:)) - RRAD*RRAD
	IF (dist< 0.0_pr) THEN
		overlapii = .TRUE.
	END IF
END DO


!check for any wall overlaps
DO j=1,dims
   IF (prd(j)==1) THEN !if periodic boundary conditions, check for particles leaving boundary
      !check maximum dimensions of boundary
      IF (rNew(ii,j) > bdim(2,j)) THEN
         overlapii = .TRUE.
      END IF
      !check minimum dimension of boundary
      IF (rNew(ii,j) < bdim(1,j)) THEN
         overlapii = .TRUE.
      END IF
   ELSE !non-periodic boundary conditions
      !check maximum dimensions of boundary
      IF (rNew(ii,j) + rad(ii) > bdim(2,j)) THEN
         overlapii = .TRUE.
      END IF
      !check minimum dimension of boundary
      IF (rNew(ii,j) - rad(ii) < bdim(1,j)) THEN
         overlapii = .TRUE.
      END IF
   END IF
END DO

END SUBROUTINE checkOverlapI


!**********************************************************************
! This function propogates a particle over a given time interval using
! projectile motion equations (collsions are assumed not to occur).
!
! Inputs:
! ii:		Index of the particle to propogate 
! tprop:	time interval to propogate the particle
! r(:), v(:):	position and velocity of particle (NOT THE GLOBAL VARIABLES)
!
! Outputs:
! r(:), v(:):	new position and velocity of pariticle after time tstep
!
!**********************************************************************

SUBROUTINE propgation(ii,r,v,phi,w,tprop)
IMPLICIT NONE

INTEGER, INTENT(IN) :: ii
REAL(pr), INTENT(IN) :: tprop
REAL(pr), INTENT(INOUT) :: r(dims), v(dims), phi(2*dims-3), w(2*dims-3)

!linear motion
r(:) = r(:) + tprop*v(:) + 0.5_pr*tprop*tprop*accel(ii,:)
v(:) = v(:) + tprop*accel(ii,:)

!angular angular motion
phi(:) = phi(:) + tprop*w(:) + 0.5_pr*tprop*tprop*alpha(ii,:)  !NOTE: This is not physical in 3D
w(:) = w(:) + tprop*alpha(ii,:)

END SUBROUTINE propgation



!**********************************************************************
! This function takes an input a time when particles are not overlapped
! (t1) and a time after they are overlapped (t2) and returns the time
! (ppcollTime) that the collsion happened. This routine uses a
! combination of Newton and Bisection methods to ensure stability.
! 
! Inputs:
! t1:		time before overlap
! t2:		time after overlap
! kk: 		index where collsion is stored in the cseq variable
!
! Outputs:
! ppcollTime:	time of the collsion between the particles (time 
!		relative to t1)
!**********************************************************************

FUNCTION ppcollTime(kk,t1,t2)
IMPLICIT NONE

INTEGER :: kk, ii, jj, i
REAL(pr) :: t2, t1, th, tl, a, b, c, d, e
REAL(pr) :: daccel(dims), dr(dims), dv(dims), RRAD, dt, dtOld
REAL(pr) :: fun, fPrime, t, y, yPrime, rts
REAL(pr) :: ppcollTime

!statement function
fun(t) = a*t*t*t*t + b*t*t*t + c*t*t + d*t + e
fPrime(t) = 4.0_pr*a*t*t*t + 3.0_pr*b*t*t + 2.0_pr*c*t + d

!Determine the indices of the involved particles from cseq
ii = cseq(kk)%i
jj = cseq(kk)%j

dr(:) = r(ii,:) - r(jj,:)
!The next line uses the minimum image criteria to find the closest particle when using periodic boundary conditions
dr(:) = dr(:) - prd(:)*nint(dr(:)/(bdim(2,:)-bdim(1,:)))*(bdim(2,:)-bdim(1,:))
dv(:) = v(ii,:) - v(jj,:)
daccel(:) = accel(ii,:) - accel(jj,:)
RRAD = rad(ii) + rad(jj)

a = 0.25_pr*DOT_PRODUCT(daccel(:),daccel(:))
b = DOT_PRODUCT(dv(:),daccel(:))
c = DOT_PRODUCT(dr(:),daccel(:)) + DOT_PRODUCT(dv(:),dv(:))
d = 2.0_pr*DOT_PRODUCT(dr(:),dv(:))
e = DOT_PRODUCT(dr(:),dr(:)) - RRAD*RRAD

!Use combination of Newton-Rhapson and Bisections methods to find the
!roots of f

th = t1				!th is value of t where fun>0
tl = t2				!tl is value of t where fun<0

rts = 0.5_pr*(t1 + t2) 		!make initial guess
dtOld = DABS(t2 - t1)
dt = dtOld

y = fun(rts)
yPrime = fPrime(rts)
DO i = 1, 100
	!If Newton is out of bounds (first condition) or converging
	!slowly (second condition), bisect
	IF ((((rts-th)*yPrime-y)*((rts-tl)*yPrime-y) > 0.0_pr) .OR. &
		&  (DABS(2.0_pr*y) > DABS(dtOld*yPrime))) THEN
		dtOld = dt
		dt = 0.5*(th - tl)
		rts = tl + dt
	ELSE  !Use newton to find new guess for rts
		dtOld = dt
		dt = y/yPrime
		rts = rts - dt
	END IF
	IF (DABS(dt) < tol) THEN	!Check conv criteria
		ppcollTime = t1 + rts
		!WRITE(*,*) "Collsion time: ", ppcollTime
		RETURN
	END IF
	y = fun(rts)
	yPrime = fPrime(rts)
	IF (y < 0.0_pr) THEN		!Keep root bracketed 
		tl = rts
	ELSE
		th = rts
	END IF
END DO

!will do this if did not reach convergence after 100 iterations
WRITE(*,*) "WARNING: NEWTON DID NOT CONVERGE AFTER 100 ITERATIONS. USED LAST VALUE OF T"
ppcollTime = rts

END FUNCTION ppcollTime



!**********************************************************************
! This function algebraically computes the time that a particle collided
! with a wall in the system
! 
! Inputs:
! t1:		time before overlap
! t2:		time after overlap
! kk: 		index where collsion is stored in the cseq variable
!
! Outputs:
! pwcollTime:	time of the collsion between the particles (time 
!		relative to t1)
!**********************************************************************

FUNCTION pwcollTime(kk,t1,t2)
IMPLICIT NONE

REAL(pr) :: t1, t2
REAL(pr) :: pwcollTime

INTEGER :: i, ii, kk, jj
REAL(pr) :: t(4), bMr

!get index of particle involved
ii = cseq(kk)%i
!get index of wall involved
jj = -cseq(kk)%j

!Set correct minimized wall dimensions due to radius of particles
!IF contacted maximum wall
IF (jj <= 3) THEN
   IF (prd(jj)==1) THEN
      bMr = bdim(2,jj)  !if periodic, then looking for time when center hits wall
   ELSE
      bMr = bdim(2,jj) - rad(ii) !if not periodic find time when the outer sphere hits wall
   END IF
!Else contacted minimum wall
ELSE 
   jj = jj - 3 	!subtract 3 to get dimension of wall
   IF (prd(jj)==1) THEN
      bMr = bdim(1,jj)
   ELSE
      bMr = bdim(1,jj) + rad(ii)
   END IF
END IF


t(1) = 2.0_pr*(r(ii,jj)+bMr)/&
	&(-v(ii,jj)-DSQRT(v(ii,jj)*v(ii,jj)-2.0_pr*accel(ii,jj)*(r(ii,jj)+bMr)))
t(2) = 2.0_pr*(r(ii,jj)+bMr)/&
	&(-v(ii,jj)+DSQRT(v(ii,jj)*v(ii,jj)-2.0_pr*accel(ii,jj)*(r(ii,jj)+bMr)))
t(3) = 2.0_pr*(r(ii,jj)-bMr)/&
	&(-v(ii,jj)-DSQRT(v(ii,jj)*v(ii,jj)-2.0_pr*accel(ii,jj)*(r(ii,jj)-bMr)))
t(4) = 2.0_pr*(r(ii,jj)-bMr)/&
	&(-v(ii,jj)+DSQRT(v(ii,jj)*v(ii,jj)-2.0_pr*accel(ii,jj)*(r(ii,jj)-bMr)))

pwcollTime = t1 + MINVAL(t(:),t>0.0_pr)

END FUNCTION pwcollTime

!**********************************************************************
! This function calculates the new velocity of a particle after a wall
! collsion. The velocity is computed based on a perferctly elastic 
! collsion law.
!
! Inputs:
! ii:		Index of the particle that collides with the wall
! jj:		Index of the wall that the particle collided with
! v(dims):	velocity of pariticle before collsion
!
! Outputs:
! v(ii,:):	velocity of particle after collsion
! 
! NOTE: v is NOT the global variable 
!**********************************************************************

SUBROUTINE wcollsion(ii,jj,v,r)
IMPLICIT NONE

INTEGER, INTENT(IN) :: ii, jj
REAL(pr), INTENT(INOUT) :: v(dims), r(dims)

INTEGER :: dimc, j

!make wall index positive
j = -jj

!If contacted maximum wall
IF (j <= 3) THEN
	dimc = j
!Else contacted minimum wall
ELSE
	dimc = j-3
END IF

IF (prd(dimc)==1) THEN
   !Need to move particle to the opposite side of the domain
   IF (j <= 3) THEN !contacted maximum wall
      r(dimc) = r(dimc) - (bdim(2,dimc)-bdim(1,dimc))
   ELSE !Else contacted minimum wall
      r(dimc) = r(dimc) + (bdim(2,dimc)-bdim(1,dimc))
   END IF
ELSE !non-periodic boundary condition
   !reflect velocity
   v(dimc) = -v(dimc)
END IF

END SUBROUTINE wcollsion



!**********************************************************************
! This function calculates the new velocity of two particles after they
! collide. This function takes into account the exchange of linear and 
! angular momentum and loss of energy due to a coefficient of restitution
! and friction.
!
! Inputs:
! ii:		Index of the first particle involved in the collsion
! jj:		Index of second particle involved in the collsion
! ri(:):	position of particle ii 
! rj(:):	position of particle jj 
! vi(:):	velocity of particle ii 
! vj(:):	velocity of particle jj 
! wi(:):	angular velocity of particle ii 
! wj(:):	angular velocity of particle jj 
!
! Outputs:
! vi(:):	velocity of particle ii (modified)
! vj(:):	velocity of particle jj (modified)
! wi(:):	angular velocity of particle ii (modified)
! wj(:):	angular velocity of particle jj (modified)
!
!**********************************************************************

SUBROUTINE collsion(ii,jj,ri,rj,vi,vj,wi,wj)
IMPLICIT NONE

INTEGER, INTENT(IN) :: ii, jj

REAL(pr), INTENT(IN) :: ri(:), rj(:)
REAL(pr), INTENT(OUT) :: vi(:), vj(:), wi(:), wj(:)

REAL(pr) :: dr(dims), dist, ndr(dims), G0(dims), h, Gc0(dims), mGct0
REAL(pr) :: Gct0(dims), t(dims), n(dims), nDotG0, nCrosst(dims)
REAL(pr) :: twoBy7

dr(:) = rj(:) - ri(:)
!The next line uses the minimum image criteria to find the closest particle when using periodic boundary conditions
dr(:) = dr(:) - prd(:)*nint(dr(:)/(bdim(2,:)-bdim(1,:)))*(bdim(2,:)-bdim(1,:))
dist = DSQRT(DOT_PRODUCT(dr(:),dr(:)))
n(:) = dr(:)/dist


G0(:) = vi(:) - vj(:)
Gc0(:) = G0 + rad(ii)*CROSSPRODUCTW(wi(:),n(:)) + &
	&rad(jj)*CROSSPRODUCTW(wj(:),n(:))
Gct0(:) = Gc0(:) - DOT_PRODUCT(Gc0(:),n(:))*n(:)

mGct0 = DSQRT(DOT_PRODUCT(Gct0(:),Gct0(:)))
IF (mGct0==0.0_pr) THEN							!This prevents t from being infinity if the tangential component of the relative velocity is zero
	t(:) = 0.0_pr
ELSE
	t(:) = Gct0(:)/mGct0
END IF

nDotG0 = DOT_PRODUCT(n(:),G0(:))
nCrosst = CROSSPRODUCTN(n(:),t(:))
twoBy7 = 2.0_pr/7.0_pr

IF (nDotG0/mGct0 < twoBy7/(epsT*(1.0_pr+epsN))) THEN 		!If the condition is true then the particles continue to slide through impact
	vi(:) = vi(:) - (n(:)+epsT*t(:))*nDotG0*(1.0_pr+epsN)*mass(jj)/(mass(jj)+mass(ii))
	vj(:) = vj(:) + (n(:)+epsT*t(:))*nDotG0*(1.0_pr+epsN)*mass(ii)/(mass(jj)+mass(ii))
	wi(:) = wi(:) - (2.5_pr/rad(ii))*nDotG0*nCrosst*epsT*(1.0_pr+epsN)*mass(jj)/(mass(jj)+mass(ii))
	wj(:) = wj(:) - (2.5_pr/rad(jj))*nDotG0*nCrosst*epsT*(1.0_pr+epsN)*mass(ii)/(mass(jj)+mass(ii))
ELSE 
	vi(:) = vi(:) - ((1.0_pr+epsN)*nDotG0*n(:) + twoBy7*mGct0*t(:))*mass(jj)/(mass(jj)+mass(ii))
	vj(:) = vj(:) + ((1.0_pr+epsN)*nDotG0*n(:) + twoBy7*mGct0*t(:))*mass(ii)/(mass(jj)+mass(ii))
	wi(:) = wi(:) - 5.0_pr/(7.0_pr*rad(ii))*mGct0*nCrosst*mass(jj)/(mass(jj)+mass(ii))
	wj(:) = wj(:) - 5.0_pr/(7.0_pr*rad(jj))*mGct0*nCrosst*mass(ii)/(mass(jj)+mass(ii))
END IF



END SUBROUTINE collsion



!**********************************************************************
! This function calculates the cross product of omega and n (the normal
! unit vector of the collsion)
!
! Inputs:
! w(:):		angular velocity of the particle
! n(:):		unit normal vector of the particle
!
! Outputs:
! CROSSPRODUCTW:the resulting cross product (w x n)
!**********************************************************************

FUNCTION CROSSPRODUCTW(w,n)

REAL(pr), INTENT(IN) :: w(2*dims-3), n(dims)

REAL(pr) :: CROSSPRODUCTW(dims)

IF (dims==2) THEN
	CROSSPRODUCTW(1) = -n(2)*w(1)
	CROSSPRODUCTW(2) = n(1)*w(1)
ELSE IF (dims==3) THEN
	CROSSPRODUCTW(1) = w(2)*n(3) - w(3)*n(2)
	CROSSPRODUCTW(2) = -w(1)*n(3) + w(3)*n(1)
	CROSSPRODUCTW(3) = w(1)*n(2) - w(2)*n(1)
ELSE
	WRITE(*,*) "INVALID CROSS PRODUCT DIMENSIONS"
END IF

RETURN

END FUNCTION CROSSPRODUCTW




!**********************************************************************
! This function calculates the cross product of n and t (the normal
! unit vector and tangential unit vector of the collsion)
!
! Inputs:
! w(:):		angular velocity of the particle
! n(:):		unit normal vector of the particle
!
! Outputs:
! CROSSPRODUCTN:the resulting cross product (n x t)
!**********************************************************************

FUNCTION CROSSPRODUCTN(n,t)

REAL(pr), INTENT(IN) :: n(dims), t(dims)

REAL(pr) :: CROSSPRODUCTN(2*dims-3)

IF (dims==2) THEN
	CROSSPRODUCTN(1) = -n(2)*t(1) + n(1)*t(2)
ELSE IF (dims==3) THEN
	CROSSPRODUCTN(1) = n(2)*t(3) - n(3)*t(2)
	CROSSPRODUCTN(2) = -n(1)*t(3) + n(3)*t(1)
	CROSSPRODUCTN(3) = n(1)*t(2) - n(2)*t(1)
ELSE
	WRITE(*,*) "INVALID CROSS PRODUCT DIMENSIONS"
END IF

RETURN

END FUNCTION CROSSPRODUCTN



!**********************************************************************
! This function calculates the total linear momentum components of the 
! system
!
! Inputs:
! none
!
! Outputs:
! momentum(:):	the total linear momentum of the system
!
!**********************************************************************

SUBROUTINE sumMomentum(momentum)
IMPLICIT NONE

REAL(pr), INTENT(OUT):: momentum(dims)

INTEGER :: i

DO i=1, dims
momentum(i) = SUM(v(:,i)*mass(:))
END DO

END SUBROUTINE sumMomentum



!**********************************************************************
! This function calculates the total angular momentum components of the 
! system (with respect to the origin)
!
! Inputs:
! none
!
! Outputs:
! angmomentum(:):	the total angular momentum of the system
!
!**********************************************************************

SUBROUTINE sumAngMomentum(angMomentum)
IMPLICIT NONE

REAL(pr), INTENT(OUT) :: angMomentum(2*dims-3)

INTEGER :: i

angMomentum(:) = 0
DO i=1,np
angMomentum(:) = angMomentum(:) &
		& + mass(i)*CROSSPRODUCTN(r(i,:),v(i,:)) & !translational component
		& + inertia(i)*w(i,:) !rotation about the C.M. of particle
END DO

END SUBROUTINE sumAngMomentum



!**********************************************************************
! This function calculates the total energy (kinetic + potential) of 
! the system
!
! Inputs:
! none
!
! Outputs:
! energy(:):	the total angular momentum of the system
!**********************************************************************

SUBROUTINE sumEnergy(energy)
IMPLICIT NONE

REAL(pr), INTENT(OUT):: energy

REAL(pr) :: A

INTEGER :: i

energy = 0.0_pr
DO i=1,np
!A is moment of inertia of sphere A = 2/5*m*r^2
A = 0.4_pr*mass(i)*rad(i)*rad(i)
!total energy is sum of linear kinetic plus potential (for gravational field) plus rotational kinetic energy
energy = energy+0.5_pr*mass(i)*DOT_PRODUCT(v(i,:),v(i,:))-&
	&mass(i)*DOT_PRODUCT(accel(i,:),r(i,:))+0.5_pr*A*DOT_PRODUCT(w(i,:),w(i,:))
END DO

END SUBROUTINE sumEnergy

!**********************************************************************
! This subroutine adds in ghost particles to the rad, r, v, omega vector
! for periodic boundary conditions so that they appear in any output and 
! are passed back to the main program
!
! Be sure to only call this after a sucessful compeltion of a timeDrivenMDstep
! because this routine assumes that no particles are overlapping with other 
! particles or the walls 
!
! Inputs/Outputs:
! modifies global rad, r, v, omega arrays and np_temp
!**********************************************************************

SUBROUTINE addGhostParticles()
IMPLICIT NONE

LOGICAL :: overlap
INTEGER :: i, numOverlaps, jj, ii, j, jjj, k, jjjj, periodic_temp(dims)

!Set all periodic_temp values to .FALSE.
!Now, when check overlap is called, it will return all particles that are overlapping the edge of the domain
!because no other particles are overlapping if timeDrivenMDstep was sucessfully completed
DO i=1,dims
   periodic_temp(i) = 0
END DO

CALL checkOverlap(r,overlap,numOverlaps,periodic_temp)
!Now all particles that need ghost particles added are stored in cseq(:)

n_ghost = 0 !stores the number of ghost particles found
DO i=1,numOverlaps
   IF (cseq(i)%j>0) THEN
      WRITE(*,*) "ERROR IN addGhostParticles, NON-WALL COLLISION DETECTED" !should not find any pp-collisions at this time
   END IF
   ii = cseq(i)%i !index of the particle involved
   jj = -cseq(i)%j
   n_ghost = n_ghost+1

   !add a ghost particle
   r_ghost(n_ghost,:) = r(ii,:)
   v_ghost(n_ghost,:) = v(ii,:)
   w_ghost(n_ghost,:) = w(ii,:)
   phi_ghost(n_ghost,:) = phi(ii,:)
   rad_ghost(n_ghost) = rad(ii)

   !move ghost particle to other side of domain
   IF (jj>3) THEN !contacted the negative boundary
      jj = jj-3
      r_ghost(n_ghost,jj) = r_ghost(n_ghost,jj)+bdim(2,jj)-bdim(1,jj) !add a positive domain length
   ELSE !contacted the positive boundary
      r_ghost(n_ghost,jj) = r_ghost(n_ghost,jj)-bdim(2,jj)+bdim(1,jj) !subtract a box length off ghost particle r
   END IF

   !Check if current particle has overlapped with other walls (need to add additional ghost particles)
   DO j = 1, i-1
      IF (ii == cseq(j)%i) THEN
         n_ghost = n_ghost+1

         !add a ghost particle
         r_ghost(n_ghost,:) = r(ii,:)
         v_ghost(n_ghost,:) = v(ii,:)
         w_ghost(n_ghost,:) = w(ii,:)
         phi_ghost(n_ghost,:) = phi(ii,:)
         rad_ghost(n_ghost) = rad(ii)

         jjj = -cseq(j)%j !jjj is index of second wall
         r_ghost(n_ghost,:) = r_ghost(n_ghost-1,:) !sets coordinates to that of the previous ghost particle
         IF (jjj>3) THEN !contacted the negative boundary
            jjj = jjj-3
            r_ghost(n_ghost,jjj) = r_ghost(n_ghost,jjj)+bdim(2,jjj)-bdim(1,jjj) !add a positive domain length
         ELSE !contacted the positive boundary
            r_ghost(n_ghost,jjj) = r_ghost(n_ghost,jjj)-bdim(2,jjj)+bdim(1,jjj) !subtract a box length off ghost particle r
         END IF
         !In 3d, if particle is on corner of the domain (contacting 3-walls at the same time) need to add the 6th particle on the opposite corner.
         !The current loop would only add 7 of the 8 necessary ghost particles
         DO k = 1, j-1
            IF (ii==cseq(k)%i) THEN
               n_ghost = n_ghost+1
               
               !add a ghost particle
               r_ghost(n_ghost,:) = r(ii,:)
               v_ghost(n_ghost,:) = v(ii,:)
               w_ghost(n_ghost,:) = w(ii,:)
               phi_ghost(n_ghost,:) = phi_ghost(ii,:)
               rad_ghost(n_ghost) = rad(ii)
               
               jjjj = -cseq(k)%j !jjjj is the index of the third wall
               r_ghost(n_ghost,:) = r_ghost(n_ghost-1,:) ! set the coordinates to that of the previous ghost particle
               IF (jjjj>3) THEN !contacted the negative boundary
                  jjjj = jjjj-3
                  r_ghost(n_ghost,jjjj) = r_ghost(n_ghost,jjjj) - bdim(2,jjjj)+bdim(1,jjjj) !subtract a positive domain length in the jjjj dimension
               ELSE
                  r_ghost(n_ghost,jjjj) = r_ghost(n_ghost,jjjj) + bdim(2,jjjj)-bdim(1,jjjj) !add a domain length
               END IF
            END IF
         END DO
      END IF
   END DO
END DO


END SUBROUTINE addGhostParticles


!**********************************************************************
! INPUT/OUTPUT SUBROUTINES
!**********************************************************************

!**********************************************************************
! This subroutine is called by the main program and initializes the system
! of particles. The parameters are read from a file specified in the wlt_3d
! input file. The order of the parameters in
! the input file cannot be changed. Listed below is the required order of 
! parameters:
!
! *********Begin sample input file****************
! This is the input file for the event driven molecular dynamics program
! DO NOT CHANGE ORDER OF INPUT VARIABLES
! 
!        1500 			Number of particles
!           2 			Dimensionality of system
!   1.00000000000000           	Density of particles
!   0.90000000000000      	Coefficient of Restitution
!  0.10000000000000E+000 	Coefficient of Friction
!  1.000000000000000E-010 	Small number to prevent numerical errors
!  -12.77780000000000       -5.00000000000000      	Dimensions of box in vector format (box extends to + and - directions)
!  0.000000000000000E+000  0.000000000000000E+000 	Gravity in system in vector  format
! 
! Start of positions and coordinates of particles
! ParticleNum, Radius,  Position (x,y,z if applicable), Velocity (Vx,Vy,Vz if applicable), Angular Velocity (wx, wy, wz if applicable)
!           1  0.500000000000000      -11.0		        0		       5.000000000000000E+000  0.000000000000000E+000  0.000000000000000E+000
!           2  0.100000000000000        1.84410149220739       -2.92709502204079       0.000000000000000E+000  0.000000000000000E+000  0.000000000000000E+000
!           3  0.100000000000000       0.691608166907150       -3.45488275523001       0.000000000000000E+000  0.000000000000000E+000  0.000000000000000E+000
!          4  0.100000000000000        4.47409389671025       -1.91310208394651       0.000000000000000E+000  0.000000000000000E+000  0.000000000000000E+000
!           5  0.100000000000000        3.86565408348625        4.07589850843544       0.000000000000000E+000  0.000000000000000E+000  0.000000000000000E+000
!           6  0.100000000000000      -0.445877097630888       -2.28313426993152       0.000000000000000E+000  0.000000000000000E+000  0.000000000000000E+000
!  ... continue list of particles ...
! *********End sample input file****************
!
! For a 3D simulation, it would require the changing of the length of the vecors (box dimensions and
! gravity) to a 3 element vector. Also, the position, velocity, and anglular velocities become 3D vectors.
!
!
! Inputs:
! none
!
! Outputs:
! npExt:	Number of particles to export to the main program
! radExt:	Radii of particles for main program to read
! massExt:	Masses of particles for main program
! F:		Initial external forces on particles (currently is gravational acceleration)
!**********************************************************************

SUBROUTINE init(npExt,radExt,massExt,inertiaExt,epsNExt,epsTExt)

USE wlt_vars	!for access to dimensions and box size
USE io_3d_vars  !for access to the name of the lagrangian input file

IMPLICIT NONE

INTEGER, INTENT(IN) :: npExt

!REAL(pr), ALLOCATABLE, INTENT(OUT) :: F(:,:) 
REAL(pr), INTENT(IN) :: radExt(npExt), massExt(npExt), epsNExt, epsTExt, inertiaExt(npExt) ! Need to change back to this when we have multiple particles
!!$REAL(pr), INTENT(IN) :: radExt, massExt


INTEGER :: i, j, k, dummy, ind=1, wdims, num, ghost_allocate
LOGICAL :: init_overlap, file_exists

REAL(pr) :: fourpiOthree


!!$INQUIRE(FILE=TRIM(lagrangian_file), EXIST=file_exists)
!!$
!!$IF (file_exists) THEN
!!$   OPEN(unit=11,file=TRIM(lagrangian_file))
!!$ELSE
!!$   WRITE(*,*) " " 
!!$   WRITE (*,*) "ERROR: Input file ", TRIM(lagrangian_file), "does not exist. Exiting program."
!!$   STOP
!!$END IF

np = npExt !from input
dims = dim ! from io_3d_vars
tol = 1d-10 !hard coded
epsN = epsNExt
epsT = epsTExt

! Allocate the global variables
wdims = 2*dims-3
ALLOCATE(rad(np),mass(np), inertia(np),r(np,dims), v(np,dims), phi(np,wdims), w(np,wdims), alpha(np,wdims), cseq(np+np*(np-1)), accel(np,dims))
rad(:) = radExt(:)
mass(:) = massExt(:)
inertia(:) = inertiaExt(:)


epsN = 1.0_pr ! these need to become inputs
epsT = 0.0_pr ! 




!!$READ(11,*) !This is the input file for the event driven molecular dynamics program
!!$READ(11,*) !DO NOT CHANGE ORDER OF INPUT VARIABLES
!!$READ(11,*)
!!$READ(11,*) np
!!$READ(11,*) dims

!!$!Check that dims of input file matches dims of wavelet code
!!$IF (dims .NE. dim) THEN
!!$	WRITE(*,*) "ERROR: Dimensionality of particle input file does not match wavelet code, Exiting"
!!$	STOP
!!$END IF
!!$dims = dim

!Allocate forces on particles and dimensions of box
ALLOCATE(grav_accel(np,dims),bdim(2,dims))

!!$READ(11,*) rho
!!$READ(11,*) epsN
!!$READ(11,*) epsT
!!$READ(11,*) tol
!READ(11,*) (bdim(2,i),i=1,dims)

bdim(:,:) = xyzlimits(:,1:dims) ! from wlt_vars
grav_accel(1,:) = 0.0_pr        ! hard-coded for now

!!$READ(11,*) (grav_accel(1,i),i=1,dims)
!!$READ(11,*) 
!!$READ(11,*) !Start of initial positions and coordinates of particles
!!$READ(11,*) !ParNum	Pos				Vel	
!!$
!!$!Set gravatational acceleration on all particles equal to zero.
!!$DO i=1,dims
!!$	grav_accel(:,i) = grav_accel(1,i)
!!$END DO



!!$wdims = 2*dims-3
!cseq is allocated to np+np*(np-1) because that is the maximum number of overlap pairs possible
!!$ALLOCATE(rad(np),mass(np),r(np,dims), v(np,dims), w(np,wdims), cseq(np+np*(np-1)), accel(np,dims))
!!$ALLOCATE(massExt(np),inertia(np),radExt(np))

! allocate the ghost particle arrays
IF (prd(1)==1 .or. prd(2)==1 .or. prd(dims)==1) THEN
   ghost_allocate = np*(4*dims-4)
   ALLOCATE(rad_ghost(ghost_allocate),r_ghost(ghost_allocate,dims),v_ghost(ghost_allocate,dims), phi_ghost(ghost_allocate,dims),w_ghost(ghost_allocate,dims))
END IF

!!$accel = grav_accel !so that accel is displayed in the output for the initialization step


!!$DO i=1,np
!!$	READ(11,*) dummy, rad(i), (r(i,j),j=1,dims), (v(i,j),j=1,dims), (w(i,j),j=1,2*dims-3)
!!$END DO

!!$!Check that there are no overlapping particles or partiles outside system
!!$CALL checkoverlap(r,init_overlap,num,prd)
!!$IF (init_overlap) THEN
!!$	WRITE(*,*) "ERROR: Particle coordinates in", TRIM(lagrangian_file), "cause overlap (with other particle or wall), Exiting"
!!$	STOP
!!$END IF

!Check to make sure that the box size is at least 4Rmax in each periodic
!direction so that the minimum image criteria holds.
DO i=1,dims
   IF((prd(i) .NE. 0) .AND. (bdim(2,i)-bdim(1,i) < 4_pr*MAXVAL(rad(:)))) THEN
      WRITE(*,*) "ERROR: Domain size in the ", i,"direction needs to be at least ", 4_pr*MAXVAL(rad(:)), "for periodic boundary conditions."
      STOP
   END IF
END DO

!!$!Calculate properties of the particles and store in variables to output
!!$fourpiOthree = 16.0_pr*DATAN(1.0_pr)/3.0_pr
!!$mass(:) = fourpiOthree*rad(:)*rad(:)*rad(:)*rho
!!$inertia(:) = 0.4_pr*mass(:)*rad(:)*rad(:)
!!$massExt(:) = mass
!!$radExt(:) = rad

radMax = MAXVAL(rad(:))

!!$npExt = np
!!$
!!$IF (prd(1)==1 .or. prd(2)==1 .or. prd(dims)==1) THEN
!!$   CALL addGhostParticles
!!$END IF

!!$CLOSE(UNIT=11)

END SUBROUTINE init

!**********************************************************************
! This function outputs the informtion for the lagrangian module
!
! Inputs:
! tStart:		Starting time
! tEnd:			Ending time
! simTime:		Current time of the simulation
! psFrame:		Index of the current file number
! printInterval:	how often the program is printing
!
! Outputs:
! Writes information to the output files. 
!
!**********************************************************************

SUBROUTINE output(tStart,tEnd,tStep,simTime,psFrame,printInterval)
IMPLICIT NONE

INTEGER, INTENT(IN) :: psFrame
REAL(pr), INTENT(IN) :: tStart, tEnd, tStep, simTime, printInterval


INTEGER :: i, j, k
LOGICAL :: lopen
CHARACTER(len=4) :: number

!Check to see if system information metadata has been written
!if not, open the file and write the information
inquire(unit=12,opened=lopen)
IF (.NOT. lopen) THEN
	OPEN(unit=12,file="lagrangian/output/"//TRIM(file_gen)//"information.dat",RECL=10000)

WRITE(12,*) "Simulation information for the molecular dynamics routine."
WRITE(12,*)
WRITE(12,*)
WRITE(12,*) np, "			Number of particles"
WRITE(12,*) dims, "			Dimension of system"
WRITE(12,*) rho, "		Density of particles"
WRITE(12,*) epsN, "	Coefficient of Restitution"
WRITE(12,*) epsT, "	Coefficient of Friction"
WRITE(12,*) tol, "	Small number to prevent numerical errors"
WRITE(12,*) tStart, "	starting time"
WRITE(12,*) tEnd, "	ending time"
WRITE(12,*) tStep, "	time step size"
WRITE(12,*) printInterval, "		print interval"
WRITE(12,*) (bdim(1,i),i=1,dims), "	Minimum box dimensions"
WRITE(12,*) (bdim(2,i),i=1,dims), "	Maximum box dimensions"
WRITE(12,*) (grav_accel(1,i),i=1,dims), " 	Gravity of system in vector format"
WRITE(12,*) 

END IF


WRITE(number,100) psFrame
100 FORMAT(I4.4)


OPEN(unit=19,file="lagrangian/output/"//TRIM(file_gen)//"output_"//number//".dat",RECL=10000)
WRITE(19,*) "Positions and velocities for current time step."
WRITE(19,*)
WRITE(19,*) simTime, "	current system time"
WRITE(19,*) 
WRITE(19,*) "ParticleNum, Position (x,y,z if applicable), Velocity &
		&(Vx,Vy,Vz if applicable), Angular Velocity (wx,wy,wz if applicable)"


DO i=1,np
	WRITE(19,*) i, rad(i), (r(i,j),j=1,dims), (v(i,j),j=1,dims), (w(i,j),j=1,2*dims-3), accel(i,:)
END DO


CLOSE(unit=19)

END SUBROUTINE output

!**********************************************************************
! This subroutine prints a postscript file showing the boundaries of the
! domain and the particles.
!
! Inputs:
! simTime: 		current simulation time
! psFrame:		current file output number
!
! Outputs:
! postscript file
!
!**********************************************************************

SUBROUTINE psprint(simTime,psFrame)
IMPLICIT NONE

INTEGER, INTENT(IN) :: psFrame

REAL(pr), INTENT(IN) :: simTime


INTEGER :: pssize=400, i
CHARACTER(len=4) :: number

REAL(pr) :: pssizex, pssizey

WRITE(number,100) psFrame
100 FORMAT(I4.4)


OPEN(unit=14, file="lagrangian/frames/"//TRIM(file_gen)//"frame_"//number//".ps",&
	& RECL = 1000)

!set the width and height of the resulting postscript file
pssizex = pssize+20
pssizey = ((bdim(2,2)-bdim(1,2))/(bdim(2,1)-bdim(1,1)))*pssize + 20

!create bounding box
WRITE(14,*) "%!PS-Adobe-2.0"
WRITE(14,*) "%%BoundingBox: 0 0 ", pssizex, " ", pssizey
WRITE(14,*) "%%EndComments"
!set command to make frame from (10,10) and over and up the dimensions of the box, also scales the pixel locations so that can use the regual x,y coordinates of the particles
WRITE(14,*) "/frame {10 10 translate ", (bdim(2,1)-bdim(1,1))/(2*pssize), " setlinewidth "
WRITE(14,*) pssize/(bdim(2,1)-bdim(1,1)), " ", pssize/(bdim(2,1)-bdim(1,1)), " scale "
WRITE(14,*) "newpath ",0," ",&
		&0," moveto ",bdim(2,1)-bdim(1,1)," 0 rlineto 0 ",&
		&bdim(2,2)-bdim(1,2)," rlineto ",-(bdim(2,1)-bdim(1,1)),&
		&" 0 rlineto closepath stroke}def"
!define command to create circles for particles
WRITE(14,*) "/c { 0 360 arc stroke} def"
!set font type and size
WRITE(14,*) "/Times-Roman findfont"
WRITE(14,*) bdim(2,1)/pssize*20, " scalefont"
WRITE(14,*) "setfont"
!create bounding box and set scales
WRITE(14,*) "frame "
WRITE(14,*) "newpath"
!write time of the simulation
WRITE(14,*) "0 0 moveto"
WRITE(14,*) "(Simulation Time: ", simTime, ") show newpath"

!display all particle locations
DO i=1,np
	WRITE(14,*) r(i,1)-bdim(1,1)," ",r(i,2)-bdim(1,2)," ",rad(i)," c"
	!WRITE(14,*) "(", i, ") show newpath"
	!WRITE(14,*) r(i,1)+rad*0.01, " ", r(i,2)+rad*0.01, " moveto (", i, ") show newpath"
END DO
!display ghost particles
DO i=1,n_ghost
	WRITE(14,*) r_ghost(i,1)-bdim(1,1)," ",r_ghost(i,2)-bdim(1,2)," ",rad_ghost(i)," c"
END DO

WRITE(14,*) "stroke showpage "

CLOSE(unit=14)

END SUBROUTINE psprint

!**********************************************************************

SUBROUTINE vtk_print(vtk_frame)
IMPLICIT NONE

INTEGER, INTENT(IN) :: vtk_frame

INTEGER :: i,j
CHARACTER(len=4) :: number


WRITE(number,100) vtk_frame
100 FORMAT(I4.4)


IF (vtk_frame == 0) THEN   !create file to draw the system boundaries
   OPEN(unit=22, file="lagrangian/vtk_frames/"//TRIM(file_gen)//"box.vtk",&
	& RECL = 1000)
!   OPEN(unit=22, file="lagrangian/vtk_frames/box.vtk")
   
   !Create file for the box dimensions
   WRITE(22,'(A)') "# vtk DataFile Version 2.0"
   WRITE(22,'(A)') "Unstructured grid legacy vtk file with point scalar data"
   WRITE(22,'(A)') "ASCII"
   WRITE(22,*)
   WRITE(22,'(A)') "DATASET POLYDATA"
   WRITE(22,'(A)') "POINTS 8 float"
   IF (dims == 3) THEN !use all three dimensions of the box
      WRITE(22,*) bdim(1,1), bdim(1,2), bdim(1,3)
      WRITE(22,*) bdim(2,1), bdim(1,2), bdim(1,3)
      WRITE(22,*) bdim(2,1), bdim(2,2), bdim(1,3)
      WRITE(22,*) bdim(1,1), bdim(2,2), bdim(1,3)
      WRITE(22,*) bdim(1,1), bdim(1,2), bdim(2,3)
      WRITE(22,*) bdim(2,1), bdim(1,2), bdim(2,3)
      WRITE(22,*) bdim(2,1), bdim(2,2), bdim(2,3)
      WRITE(22,*) bdim(1,1), bdim(2,2), bdim(2,3)
   ELSE
      WRITE(22,*) bdim(1,1), bdim(1,2), 0
      WRITE(22,*) bdim(2,1), bdim(1,2), 0
      WRITE(22,*) bdim(2,1), bdim(2,2), 0
      WRITE(22,*) bdim(1,1), bdim(2,2), 0
      WRITE(22,*) bdim(1,1), bdim(1,2), 0
      WRITE(22,*) bdim(2,1), bdim(1,2), 0
      WRITE(22,*) bdim(2,1), bdim(2,2), 0
      WRITE(22,*) bdim(1,1), bdim(2,2), 0
   END IF
   WRITE(22,'(A)') "POLYGONS 6 30"
   WRITE(22,'(A)') "4 0 1 2 3"
   WRITE(22,'(A)') "4 4 5 6 7"
   WRITE(22,'(A)') "4 0 1 5 4"
   WRITE(22,'(A)') "4 2 3 7 6"
   WRITE(22,'(A)') "4 0 4 7 3"
   WRITE(22,'(A)') "4 1 2 6 5"
   
   CLOSE(unit=22)
   
END IF

OPEN(unit=21, file="lagrangian/vtk_frames/"//TRIM(file_gen)//"frame_"//number//".vtk",&
     & RECL = 1000)
!OPEN(unit=21, file="lagrangian/vtk_frames/frame_"//number//".vtk",&
!	& RECL = 1000)

!create file for particle information
WRITE(21,'(A)') "# vtk DataFile Version 2.0"
WRITE(21,'(A)') "Unstructured grid legacy vtk file with point scalar data"
WRITE(21,'(A)') "ASCII"
WRITE(21,*)
WRITE(21,'(A)') "DATASET UNSTRUCTURED_GRID"
WRITE(21,'(A,I12,A)') "POINTS", np+n_ghost," double"
DO i=1,np
   IF (dims==3) THEN
      WRITE(21,*) (r(i,j),j=1,dims)
   ELSE
      WRITE(21,*) (r(i,j),j=1,dims), 0
   END IF
END DO
DO i=1,n_ghost
   IF (dims==3) THEN
      WRITE(21,*) (r_ghost(i,j),j=1,dims)
   ELSE
      WRITE(21,*) (r_ghost(i,j),j=1,dims), 0
   END IF
END DO  
WRITE(21,*)
WRITE(21,'(A,I12)') "POINT_DATA", np+n_ghost
WRITE(21,'(A)') "SCALARS radii double"
WRITE(21,'(A)') "LOOKUP_TABLE default"
DO i=1,np
   WRITE(21,*) rad(i)
END DO
DO i=1,n_ghost
   WRITE(21,*) rad_ghost(i)
END DO  
WRITE(21,*) 
WRITE(21,*) 
IF (dims==2) THEN
   WRITE(21,'(A)') "SCALARS angular_position double"
   WRITE(21,'(A)') "LOOKUP_TABLE default"
   DO i=1,np
      WRITE(21,*) (phi(i,j),j=1,1)
   END DO
   DO i=1,n_ghost
      WRITE(21,*) (phi_ghost(i,j),j=1,1)
   END DO
   WRITE(21,*) 
ELSE IF (dims==3) THEN
   WRITE(21,'(A)') "VECTORS angular_position double"
   DO i=1,np
      WRITE(21,*) (phi(i,j),j=1,dims)
   END DO
   DO i=1,n_ghost
      WRITE(21,*) (phi_ghost(i,j),j=1,dims)
   END DO
   WRITE(21,*) 
END IF
WRITE(21,'(A)') "VECTORS velocity double"
DO i=1,np
   IF (dims==3) THEN
      WRITE(21,*) (v(i,j),j=1,dims)
   ELSE
      WRITE(21,*) (v(i,j),j=1,dims), 0
   END IF
END DO
DO i=1,n_ghost
   IF (dims==3) THEN
      WRITE(21,*) (v_ghost(i,j),j=1,dims)
   ELSE
      WRITE(21,*) (v_ghost(i,j),j=1,dims), 0
   END IF
END DO
WRITE(21,*) 
WRITE(21,'(A)') "VECTORS angular_velocity double"
DO i=1,np
   IF (dims==3) THEN
      WRITE(21,*) (w(i,j),j=1,dims)
   ELSE
      WRITE(21,*) (w(i,j),j=1,dims), 0
   END IF
END DO
DO i=1,n_ghost
   IF (dims==3) THEN
      WRITE(21,*) (w_ghost(i,j),j=1,dims)
   ELSE
      WRITE(21,*) (w_ghost(i,j),j=1,dims), 0
   END IF
END DO


CLOSE(unit=21)


END SUBROUTINE vtk_print

!**********************************************************************

SUBROUTINE momentumOutput(simTime)
IMPLICIT NONE


REAL(pr), INTENT(IN) :: simTime

REAL(pr) momentum(dims), angMomentum(2*dims-3), rad(np)

INTEGER :: i
LOGICAL :: lopen

!Calculate current system momentum
CALL sumMomentum(momentum)
CALL sumAngMomentum(angMomentum)

!Check if file has already been opened
!If not, open it and write header lines
inquire(unit=15,opened=lopen)
IF (.NOT. lopen) THEN
	OPEN(unit=15,file="lagrangian/monitor/momentum.dat",RECL=10000)
	WRITE(15,*) "System_Time	Total_Linear_Momentum_Components_(x,y,z) 	Total_Angular_Momentum_Components(x,y,z)"
END IF

WRITE(15,*) simTime,'	', (momentum(i),'	',i=1,dims),(angMomentum(i),'	',i=1,2*dims-3)

RETURN

END SUBROUTINE momentumOutput

!**********************************************************************

SUBROUTINE energyOutput(simTime)
IMPLICIT NONE


REAL(pr), INTENT(IN) :: simTime

REAL(pr) energy

INTEGER :: i
LOGICAL :: lopen

!Calculate current system momentum
CALL sumEnergy(energy)

!Check if file has already been opened
!If not, open it and write header lines
inquire(unit=16,opened=lopen)
IF (.NOT. lopen) THEN
	OPEN(unit=16,file="lagrangian/monitor/energy.dat",RECL=10000)
	WRITE(16,*) "System_Time	Total_System_Energy"
END IF

WRITE(16,*) simTime,'	', energy

RETURN

END SUBROUTINE energyOutput




!**********************************************************************

SUBROUTINE ppCollListOutput(simTime, ii, jj)
IMPLICIT NONE

INTEGER, INTENT(IN) :: ii, jj
REAL(pr), INTENT(IN) :: simTime

LOGICAL :: lopen


!Check if file has already been opened
!If not, open it and write header lines
inquire(unit=17,opened=lopen)
IF (.NOT. lopen) THEN
	OPEN(unit=17,file="lagrangian/monitor/ppCollList.dat",RECL=10000)
	WRITE(17,*) "System_Time	index_i		index_j"
END IF

WRITE(17,*) simTime,'	', ii,'	', jj

RETURN

END SUBROUTINE ppCollListOutput

!**********************************************************************

SUBROUTINE wCollListOutput(simTime, ii, jj)
IMPLICIT NONE

INTEGER, INTENT(IN) :: ii, jj
REAL(pr), INTENT(IN) :: simTime

LOGICAL :: lopen


!Check if file has already been opened
!If not, open it and write header lines
inquire(unit=18,opened=lopen)
IF (.NOT. lopen) THEN
	OPEN(unit=18,file="lagrangian/monitor/wCollList.dat",RECL=10000)
	WRITE(18,*) "System_Time		index_i"
END IF

WRITE(18,*) simTime,'	', ii, '	', jj

RETURN

END SUBROUTINE wCollListOutput

!***********************************************************************
! The two functions below are temporarily used to create the mask for the
! particles. 

SUBROUTINE posRadOutput(r_ext,rad_ext)
IMPLICIT NONE

REAL(pr), INTENT(OUT) :: r_ext(np,dims), rad_ext(np)

r_ext = r
rad_ext = rad

END SUBROUTINE posRadOutput

SUBROUTINE numParticlesOutput(np_ext)
IMPLICIT NONE

INTEGER, INTENT(OUT) :: np_ext

np_ext = np

END SUBROUTINE numParticlesOutput

!********************************

SUBROUTINE velOutput(v_ext)
IMPLICIT NONE

REAL(pr), INTENT(OUT) :: v_ext(np,dims)

v_ext = v


END SUBROUTINE velOutput
!*****************************************

!!$SUBROUTINE initialize_lagrangian_particles(t_beg_local,t_end_local,dt_local,t_local,iwrite_local,dtwrite_local)
!!$  
!!$  INTEGER, INTENT(IN) :: iwrite_local
!!$  REAL(pr), INTENT(IN) :: t_beg_local,t_end_local,dt_local,t_local,dtwrite_local
!!$
!!$  !********
!!$  ! Initialize particles -------------------------------------------
!!$  CALL init(num_particle,rad_particle,mass_particle)
!!$  ALLOCATE(r_particle(num_particle,dim),v_particle(num_particle,dim),w_particle(num_particle,dim))
!!$  ALLOCATE(F_particle(num_particle,dim))
!!$  !set the fluid force on all particles to zero
!!$  DO iiii = 1, num_particle
!!$     F_particle(iiii,:) = 0.0_pr
!!$  END DO
!!$  !call output routine to write initial conditions
!!$  CALL lagrangian_output (t_beg_local,t_end_local,dt_local,t_local,iwrite_local,dtwrite_local)
!!$  !use_lagrangian = .FALSE.
!!$  ! ----------------------------------------------------------------
!!$  !********
!!$
!!$END SUBROUTINE initialize_lagrangian_particles

SUBROUTINE lagrangian_output (t_beg_local,t_end_local,dt_local,t_local,iwrite_local,dtwrite_local)
  
  INTEGER, INTENT(IN) :: iwrite_local
  REAL(pr), INTENT(IN) :: t_beg_local,t_end_local,dt_local,t_local,dtwrite_local

  !**********
  !Data output for lagragian module
  CALL output(t_beg_local,t_end_local,dt_local,t_local,iwrite_local,dtwrite_local)
  !        CALL psprint(t,iwrite)
  CALL vtk_print(iwrite_local)
  CALL momentumOutput(t_local) 
  CALL energyOutput(t_local)
  !***********

END SUBROUTINE lagrangian_output

!!$SUBROUTINE advance_lagrangian_partilces(t_local,dt_local)
!!$
!!$  REAL(pr), INTENT(IN) :: t_local,dt_local
!!$
!!$  !**********
!!$  !Advance particles to time t+dt
!!$  CALL timeDrivenMDstep(dt_local,F_particle,t_local-dt_local,r_particle,v_particle,w_particle)
!!$  !**********     
!!$
!!$END SUBROUTINE advance_lagrangian_partilces



END MODULE lagrangian

