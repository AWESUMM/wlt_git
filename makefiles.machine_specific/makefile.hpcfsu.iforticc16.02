######################################################################################
#
### LINUX intel compilers
#
######################################################################################

# Lapack library
LIB_LAPACK =

# set flag to YES if we support the netcdf library on this machine (LEAVE NO SPACES)
SUPPORT_HDF	=	NO
HDFLIBDIR	=	/gpfs/home/xge/wlt_xge/hdf-1.8.17/BUILD_DIR/lib
HDFINC		=	/gpfs/home/xge/wlt_xge/hdf-1.8.17/BUILD_DIR/include

# CGNS library has been compiled on scales with "--with-fortran=LOWERCASE --enable-lfs"
SUPPORT_CGNS	=	YES
#CGNSLIBDIR		=	/gpfs/home/xge/wlt_xge/CGNS/src/cgns_build/lib
#CGNSINC		=	/gpfs/home/xge/wlt_xge/CGNS/src/cgns_build/include
CGNSLIBDIR		=	/gpfs/home/xge/wlt_xge/cgnslib_2.5/cgns_build/lib
CGNSINC			=	/gpfs/home/xge/wlt_xge/cgnslib_2.5/cgns_build/include

# MPI library (standard compilation)
MPIINC		=	-I/opt/hpc/intel/openmpi/include
MPILIB		=	-reentrancy threaded -L/opt/hpc/intel/openmpi/lib64 -lmpi -lopen-rte -lopen-pal -ldl -Wl,--export-dynamic -lnsl -lutil -lm -ldl -lmpi_mpifh # For Open MPI 1.8 and above #-pthread -lmpi_f90 -lmpi_f77 
# MPILIB      =  -pthread -L/usr/local/openmpi-1.4.5/intel11/lib -llammpio -llamf77mpi -lmpi -llam -lutil -ldl
MPIFLAGS	=	-DMULTIPROC -DDATABASE_MPI_APPEND_UNDERSCORE

# Zoltan library for domain partitioning
SUPPORT_ZOLTAN	=	YES
ZOLTANLIB		=	-L/gpfs/home/xge/wlt_xge/Zoltan_v3.83/BUILD_DIR/lib -lzoltan
ZOLTANINC		=	-I/gpfs/home/xge/wlt_xge/Zoltan_v3.83/BUILD_DIR/include


############################
FC	=	ifort
FC77	=	$(FC)
CC      =	icc
LINK    =	$(FC)
# LINKLIB	=	-lstdc++ #or /usr/lib/libstdc++.so.5 or /usr/lib/libstdc++.so.6
LINKLIB	=	-lstdc++ -shared-intel # -limf -lm


LOCAL_COMPILE_MODE=0

ifeq ($(LOCAL_COMPILE_MODE),0)
	#optimized
	FFLAGS		=	-fpp -O1 -DPTR_INTEGER8 -warn nousage #-w95  -cm -Vaxlib
	F77FLAGS	=	-fpp -O1  -warn nousage #-w95 -w90  -cm
	CCFLAG		=	-O1 -DDATABASE_INTERFACE_LOWERCASE -DDATABASE_APPEND_UNDERSCORE 
else
ifeq ($(LOCAL_COMPILE_MODE),1)
	#optimized + Timers + -DCOP_1
	FFLAGS   =  -fpp -DPTR_INTEGER8 -warn nousage -O1 -DCOP_1 -DUSE_DEBUG_TIMERS
	F77FLAGS =	-fpp -warn nousage -O1
	CCFLAG	=	-O1 -DDATABASE_INTERFACE_LOWERCASE -DDATABASE_APPEND_UNDERSCORE

else
ifeq ($(LOCAL_COMPILE_MODE),11)
  #optimized + Timers
  FFLAGS   =  -fpp -DPTR_INTEGER8 -warn nousage -O1 -DUSE_DEBUG_TIMERS
  F77FLAGS =	-fpp -warn nousage -O1
  CCFLAG   =	-O1 -DDATABASE_INTERFACE_LOWERCASE -DDATABASE_APPEND_UNDERSCORE

else
ifeq ($(LOCAL_COMPILE_MODE),2)
  #optimized + debug 
  FFLAGS   =  -O0 -g -fpp -DPTR_INTEGER8 -DDISTANCE_FUNCTION -warn nousage -warn errors -traceback -check all
  F77FLAGS =	-O0 -g -fpp -warn nousage -warn errors -traceback -check all
  CCFLAG   =	-O0 -g -DDATABASE_INTERFACE_LOWERCASE -DDATABASE_APPEND_UNDERSCORE  -traceback

else
ifeq ($(LOCAL_COMPILE_MODE),3)
  #optimized + debug + profile
  FFLAGS   =  -O1 -g -p -fpp -DPTR_INTEGER8 -warn nousage -warn errors -traceback -check bounds
  F77FLAGS =	-O1 -g -p -fpp -warn nousage -warn errors -traceback -check bounds 
  CCFLAG   =	-O1 -g -p -DDATABASE_INTERFACE_LOWERCASE -DDATABASE_APPEND_UNDERSCORE  -traceback

else
ifeq ($(LOCAL_COMPILE_MODE),33)
  #optimized + profile
  FFLAGS   =  -O1 -p -fpp -DPTR_INTEGER8 -warn nousage
  F77FLAGS =	-O1 -p -fpp -warn nousage
  CCFLAG   =	-O1 -p -DDATABASE_INTERFACE_LOWERCASE -DDATABASE_APPEND_UNDERSCORE

else
ifeq ($(LOCAL_COMPILE_MODE),4)
  #extra run time checks
  #FFLAGS   =	-O0 -g -fpp -traceback -heap-arrays 4194304 -check all -ftrapuv -fp-stack-check 
  #F77FLAGS =	-O0 -g -fpp #-traceback -heap-arrays 4194304 -check all -ftrapuv -fp-stack-check
  #CCFLAG   =	-O0 -g	-DDATABASE_INTERFACE_LOWERCASE  -DDATABASE_APPEND_UNDERSCORE -DEBUG_SAFE -traceback -Wall -check-uninit -ftrapuv
  #-DEBUG_AMR -DEBUG1  -DEBUG_READ -DEBUG_WRITE

else
ifeq ($(LOCAL_COMPILE_MODE),5)
  #unoptimized + debug + profile + Timers + -DCOP_1
  FFLAGS   =  -O0 -g -p -fpp -DPTR_INTEGER8 -warn nousage -DCOP_1 -DUSE_DEBUG_TIMERS  -warn errors -traceback -check bounds
  F77FLAGS =	-O0 -g -p -fpp -warn nousage  -warn errors -traceback -check bounds 
  CCFLAG   =	-O0 -g -p -DDATABASE_INTERFACE_LOWERCASE -DDATABASE_APPEND_UNDERSCORE  -traceback

else
ifeq ($(LOCAL_COMPILE_MODE),6)
  #unoptimized + debug + profile + Timers
  FFLAGS   =  -O0 -g -p -fpp -DPTR_INTEGER8 -DDISTANCE_FUNCTION -warn nousage -DUSE_DEBUG_TIMERS  -warn errors -traceback -check bounds
  F77FLAGS =	-O0 -g -p -fpp -warn nousage  -warn errors -traceback -check bounds 
  CCFLAG   =	-O0 -g -p -DDATABASE_INTERFACE_LOWERCASE -DDATABASE_APPEND_UNDERSCORE  -traceback

else
ifeq ($(LOCAL_COMPILE_MODE),7)
  #unoptimized + debug
  FFLAGS   =  -O0 -g3 -fpp -DPTR_INTEGER8 -warn nousage -warn errors -traceback -check bounds -check uninit 
  F77FLAGS =	-O0 -g3 -fpp -warn nousage  -warn errors -traceback -check bounds -check uninit 
  CCFLAG   =	-O0 -g3 -DDATABASE_INTERFACE_LOWERCASE -DDATABASE_APPEND_UNDERSCORE  
else
ifeq ($(LOCAL_COMPILE_MODE),77)
  #unoptimized + no debug
  FFLAGS   =  -O0 -g -fpp -DPTR_INTEGER8 -DDISTANCE_FUNCTION -warn nousage -warn errors 
  F77FLAGS =	-O0 -g -fpp -warn nousage  -warn errors 
  CCFLAG   =	-O0 -g -DDATABASE_INTERFACE_LOWERCASE -DDATABASE_APPEND_UNDERSCORE
else
ifeq ($(LOCAL_COMPILE_MODE),777)
  #optimized + no debug
  FFLAGS   =  -O1 -fpp -DPTR_INTEGER8 -DDISTANCE_FUNCTION -warn nousage -warn errors
  F77FLAGS =	-O1 -fpp -warn nousage  -warn errors
  CCFLAG   =	-O1 -DDATABASE_INTERFACE_LOWERCASE -DDATABASE_APPEND_UNDERSCORE
else
ifeq ($(LOCAL_COMPILE_MODE),8)
  #unoptimized + debug + -DCOP_1
  FFLAGS   =  -O0 -g -fpp -DPTR_INTEGER8 -warn nousage -warn errors -traceback -check bounds -DCOP_1
  F77FLAGS =	-O0 -g -fpp -warn nousage  -warn errors -traceback -check bounds 
  CCFLAG   =	-O0 -g -DDATABASE_INTERFACE_LOWERCASE -DDATABASE_APPEND_UNDERSCORE  -traceback

else
  $(error wrong LOCAL_COMPILE_MODE marker)

endif
endif
endif
endif
endif
endif
endif
endif
endif
endif
endif
endif
endif







#
# Flags for if8 compiler
# need -Vaxlib to use getarg() to use command line args
# debug + profile  -g   -p
#
#intel ifc compiler flags
# -cm suppress all comment messages
# -w95 Suppresses warning messages about  Fortran features which are deprecated or obsoleted in Fortran 95.
#







# compiler flag to force real size  8 bytes (required for F77 fft files)
RSIZE8 =  -r8 



# temporary compiler files that need to be cleaned
CLEANFLS =  *.o *.mod *.il  fft/*.o fft/*.mod fft/*.il

#tar flags
FTAR =  cvzf
