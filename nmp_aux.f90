!
! module NMP_AUX to be used in program NtoMonP
!
MODULE nmp_aux
  
  PUBLIC :: usage_nmp,             &  ! print usage message
       read_file_type_nmp             ! read command line parameters
  
  PRIVATE :: term                     ! terminate the code
  INTEGER, PRIVATE, PARAMETER :: MAXLEN = 512                      ! maximum length of command line argument
  CHARACTER(LEN=*), PRIVATE, PARAMETER :: NUM_FMT = '(I5)'         ! format for the variable number conversion
  
CONTAINS
  
  ! read command line parameters
  SUBROUTINE read_file_type_nmp (input_base_name, output_base_name, out_proc_num, in_proc_num, &
       verb_level, do_append, dmeth )
#ifdef _MSC_VER
    USE DFLIB         ! getarg()
#endif
    USE parallel      ! par_rank_str
    INTEGER, INTENT(INOUT) :: out_proc_num, &                    ! number of in/out files
         verb_level, dmeth, in_proc_num
    CHARACTER(LEN=MAXLEN), INTENT(INOUT) :: input_base_name, output_base_name ! in/out file names
    LOGICAL, INTENT(INOUT) :: do_append
    INTEGER, PARAMETER :: MAX_NAMES = 2
    CHARACTER (LEN=MAXLEN) :: tmp, name(1:MAX_NAMES)
    INTEGER :: i, j, name_counter, iargc
    
    ! default values -------
    out_proc_num = 0
    verb_level = 0
    do_append = .TRUE.                ! read all relevant .res files and append the data
    dmeth = 0                         ! domain decomposition method
    in_proc_num = 0
    ! default values -------

    i = 1
    name_counter = 0
    DO WHILE ( i.LE.iargc() )
       CALL getarg( i, tmp )
       IF ( tmp(1:1).EQ.'-' ) THEN ! ---- parameter
          DO j=2,LEN(TRIM(tmp))
             IF ( ( tmp(j:j).EQ.'h' ).OR. &                     ! -h -help
                  ( tmp(j:j).EQ.'-' ).OR. &                     ! --help
                  ( tmp(j:j).EQ.'?' ) ) CALL usage_nmp          ! -?
             IF ( tmp(j:j).EQ.'o' ) THEN
                i = i + 1
                CALL getarg( i, tmp )
                READ( UNIT=tmp, FMT=NUM_FMT, ERR=1 ) out_proc_num
             END IF
             IF ( tmp(j:j).EQ.'i' ) THEN
                i = i + 1
                CALL getarg( i, tmp )
                READ( UNIT=tmp, FMT=NUM_FMT, ERR=1 ) in_proc_num
             END IF
             IF ( tmp(j:j).EQ.'d' ) THEN
                i = i + 1
                CALL getarg( i, tmp )
                READ( UNIT=tmp, FMT=NUM_FMT, ERR=2 ) dmeth
             END IF
             IF ( tmp(j:j).EQ.'v' ) THEN
                i = i + 1
                CALL getarg( i, tmp )
                READ( UNIT=tmp, FMT=NUM_FMT, ERR=3 ) verb_level
             END IF
          END DO
       ELSE ! ---- file name
          name_counter = name_counter + 1
          IF (name_counter.GT.MAX_NAMES) THEN
             IF (par_rank.EQ.0) THEN
                WRITE (*,'("Error: too many arguments")')
                WRITE (*,'("Use --help for more information.")')
             END IF
             CALL term
          END IF
          CALL getarg( i, name(name_counter))
       END IF
       i = i + 1
    END DO

    ! set file names
    IF (name_counter.EQ.2) THEN
       input_base_name = TRIM(name(1))
       output_base_name = TRIM(name(2))
    ELSE
       WRITE (*,'("Error: missing arguments")')
       WRITE (*,'("Use --help for more information.")')
       CALL term
    END IF
    
    IF (out_proc_num.LE.0.) THEN
       WRITE (*,'("Error: positive output number of files should present")')
       WRITE (*,'("Use --help for more information.")')
       CALL term
    ELSE IF (par_size.GT.1.AND.out_proc_num.NE.par_size) THEN
       IF (par_rank.EQ.0) THEN
          WRITE (*,'("Error: In parallel runs, make sure that the number of processors is equal")')
          WRITE (*,'("       to the number of output files. Otherwise, the code is erroneous.")')
          WRITE (*,'("       Each processor expects to write single output file in parallel run.")')
          WRITE (*,'("Use --help for more information.")')
       END IF
       CALL term
    END IF
    IF (dmeth.LT.0) THEN
       WRITE (*,'("Error: method number should be positive")')
       WRITE (*,'("Use --help for more information.")')
       CALL term
    END IF
    RETURN

    ! conversion errors
1   WRITE (*,'("Error: strange number of files")')
    WRITE (*,'("Use --help for more information.")')
    CALL term
2   WRITE (*,'("Error: strange domain decomposition method")')
    WRITE (*,'("Use --help for more information.")')
    CALL term
3   WRITE (*,'("Error: strange verbouse level")')
    WRITE (*,'("Use --help for more information.")')
    CALL term
    
  END SUBROUTINE read_file_type_nmp
  ! print usage message
  SUBROUTINE usage_nmp
    USE parallel  ! par_rank
    IF (par_rank.EQ.0) THEN
       WRITE (*,'("Usage: n2m [OPTIONS] IN_FILE OUT_FILE")')
       WRITE (*,'("       [mpirun -np [N_PROC]] n2m -o [N_PROC] ...")')
       WRITE (*,'("Convert .res file(s) for a different domain decomposition.")')
       WRITE (*,'(" ")')
       WRITE (*,'("Options:")')
       WRITE (*,'("  IN_FILE             input filename")')
       WRITE (*,'("  -o OUT_NUM          output number of files")')
       WRITE (*,'("  OUT_FILE            base output filename (without .res extension)")')
       WRITE (*,'(" ")')
       WRITE (*,'("  -h --help -?        show this message")')
       WRITE (*,'("  -d METH             output domain decomposition method (default 0)")')
       WRITE (*,'("  -v VERB             read/save_solution verbosity level (default 0)")')
       WRITE (*,'("  -i IC_processors    optional parameter - number of input files -")')
       WRITE (*,'("                      might be required for old versions of .res files")')
       WRITE (*,'("Examples:")')
       WRITE (*,'("  n2m -o 3 results/case.000.res out.000")')
       WRITE (*,'("    produce files out.000.p0.res, out.000.p1.res, out.000.p2.res")')
       WRITE (*,'("    from single input file results/case.000.res")')
       WRITE (*,'(" ")')
       WRITE (*,'("  n2m -o 3 results/case.000.p0.res out.000")')
       WRITE (*,'("    produce files out.000.p0.res, out.000.p1.res, out.000.p2.res")')
       WRITE (*,'("    from input files results/case.000.p0.res, ...p1.res, etc.")')
       WRITE (*,'(" ")')
       WRITE (*,'("Warnings:")')
       WRITE (*,'(" In parallel runs, make sure that the number of processors is equal")')
       WRITE (*,'(" to the number of output files. Otherwise, the code is erroneous.")')
       WRITE (*,'(" Each processor expects to write single output file in parallel run.")')
    END IF
    CALL term
  END SUBROUTINE usage_nmp
  ! terminate hte code
  SUBROUTINE term
    USE parallel
    CALL parallel_finalize
    STOP
  END SUBROUTINE term
  
END MODULE nmp_aux
