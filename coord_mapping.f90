!**********************************************************************
! Routines for mapping single integer representation of nxyz(1:dim)
!**********************************************************************
MODULE coord_mapping
  INTEGER, SAVE, DIMENSION(:), ALLOCATABLE :: i_p_coord_mapping
  INTEGER, save :: dim_loc
CONTAINS

!OLEG:  setup_coord_mapping needs to be set up every time you use coord_map_ixyz, since it can be used for multiple purposes
SUBROUTINE setup_coord_mapping(nxyz_loc,dim_in)
  IMPLICIT NONE
  INTEGER , INTENT(IN) :: dim_in
  INTEGER , INTENT(IN), DIMENSION(1:dim_in) :: nxyz_loc
  INTEGER :: i

  dim_loc = dim_in ! save dim

  IF( .NOT. ALLOCATED(i_p_coord_mapping)) ALLOCATE(i_p_coord_mapping(0:dim_in))

  i_p_coord_mapping(0) = 1
  DO i=1,dim_in
     i_p_coord_mapping(i) = i_p_coord_mapping(i-1)*(1+nxyz_loc(i))
  END DO

END SUBROUTINE setup_coord_mapping

PURE SUBROUTINE coord_map_ixyz(ii, ixyz_loc )
  INTEGER , INTENT(IN) :: ii
  INTEGER , INTENT(OUT), DIMENSION(1:dim_loc) :: ixyz_loc

  ixyz_loc(1:dim_loc) = INT(MOD(ii-1,i_p_coord_mapping(1:dim_loc))/i_p_coord_mapping(0:dim_loc-1))

END SUBROUTINE coord_map_ixyz

END MODULE coord_mapping
