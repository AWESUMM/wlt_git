!
! Zoltan library based domain decomposition module
! (functions are supposed to be empty here)
MODULE domain_zoltan
  IMPLICIT NONE
  PRIVATE
  PUBLIC :: zoltan_domain_decomp

CONTAINS

  SUBROUTINE zoltan_domain_decomp( dim_loc, Number_Vertices, Mxyz_Vertices, nproc, &
       Nwlt_Vertices, Nwlt_Surface_Vertices, N_Update, N_Predict, &
       proc_to_tree, task, Partitioning_Method, EdgeWeight_Global_SafetyFactor, &
       Vertices_Coordinates, verb_level, error_out, myrank )

    USE precision
    IMPLICIT NONE
    INTEGER, INTENT(IN) ::  &
         dim_loc, &                                 ! problem geometric dimension
         Number_Vertices, &                         ! total number of vertices
         Mxyz_Vertices(dim_loc), &                  ! number of vertices in each direction
         nproc, &                                   ! total number of processors
         Nwlt_Vertices(Number_Vertices), &          ! nwlt per vertex
         Nwlt_Surface_Vertices(dim_loc,Number_Vertices), &      ! nwlt on min surfaces of vertex
         N_Update, N_Predict, &                     ! n_update, n_predict
         task, &                                    ! 0-partition, 1-repartition, 2-refine
         Partitioning_Method, &                     ! 1-Geometric, 2-Graph, 3-HyperGraph
         verb_level, &                              ! Zoltan debug level (0-10)
         myrank                                     ! processor's rank
    REAL(pr), INTENT(IN) :: &
         EdgeWeight_Global_SafetyFactor, &             ! 0.1 or so (read the manual)
         Vertices_Coordinates(Number_Vertices*dim_loc) ! (useg for geometric partitioning)
    INTEGER, INTENT(INOUT) :: &
         proc_to_tree(Number_Vertices), &              ! output partition
         error_out                                     ! should be zero if everything is OK

    error_out = 1             ! error
    IF (myrank.EQ.0) THEN
       PRINT *, '   The code has not been compiled to use Zoltan library    '
       PRINT *, '   Set SUPPORT_ZOLTAN=YES, ZOLTANLIB, and ZOLTANINC in your'
       PRINT *, '   machine specific makefile and recompile,                '
       PRINT *, '   or request a different partitioning method in your      '
       PRINT *, '   input parameter file, instead of domain_meth=2          '
    END IF
    
  END SUBROUTINE zoltan_domain_decomp
  
END MODULE domain_zoltan

