! Utility functions for compressible flow cases.
! This library contains common functionality that is useful for
!   a variety of Navier-Stokes and Euler sims

MODULE equations_compressible_utils
  USE precision
  IMPLICIT NONE

  PUBLIC :: &
    add_smooth_noise   ! Add gaussian perturbations at random locations to a field


CONTAINS
  !! ADD_SMOOTH_NOISE:  Adds gaussian perturbations at random locations to the
  !given field
  ! field       - field to add noise to
  ! nloc        - number of points in the field
  ! number_points - number of noise perturbations to add
  ! scale       - height of the perturbations
  ! seed_input  - random seed for reproducability across initial adaptation cycles
  ! OPTIONAL ARGUMENTS:
  ! DEFINE_X    - specify perturbation locations on the x plane
  ! DEFINE_Y    - specify perturbation locations on the y plane
  ! DEFINE_Z    - specify perturbation locations on the z plane
  SUBROUTINE add_smooth_noise( field, nloc, number_points, scale, seed_input, DEFINE_X, DEFINE_Y, DEFINE_Z )
    USE parallel 
    USE sizes
    USE util_vars 
    USE wlt_vars 
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: nloc, number_points, seed_input
    REAL (pr), DIMENSION(nloc), INTENT(INOUT) :: field
    REAL (pr), INTENT (IN) :: scale
    REAL (pr), OPTIONAL, INTENT (IN) :: DEFINE_X, DEFINE_Y, DEFINE_Z

    INTEGER :: iseedsize
    REAL (pr), DIMENSION(nwlt) :: rand_array
    INTEGER, ALLOCATABLE, DIMENSION(:) :: seed
    REAL (pr), DIMENSION(3) :: x_loc

    INTEGER :: ipoint, idim, iloc

    CALL RANDOM_SEED(SIZE=iseedsize)  
    
    ALLOCATE(seed(iseedsize))
    seed(:) = seed_input 
    CALL RANDOM_SEED(PUT=seed)
    DEALLOCATE(seed)

       
    DO ipoint = 1,number_points
      ! generate random point
      call RANDOM_NUMBER(x_loc)
      IF( par_rank .EQ. 0 ) THEN 
        x_loc(1:dim) = x_loc(1:dim) * (xyzlimits(2,1:dim) - xyzlimits(1,1:dim)) + xyzlimits(1,1:dim)
        !PRINT *, 'NOISE:', par_rank, INT( 1.0_pr - 2.0_pr * MODULO(ipoint, 2 )) * ipoint, x_loc(1), x_loc(2), x_loc(3)    
      ELSE
        x_loc = 0.0_pr
      END IF  
      CALL parallel_vector_sum( REAL=x_loc, LENGTH=3)
  
      IF( PRESENT(DEFINE_X) ) x_loc(1) = DEFINE_X
      IF( PRESENT(DEFINE_Y) ) x_loc(2) = DEFINE_Y
      IF( PRESENT(DEFINE_Z) ) x_loc(3) = DEFINE_Z

      ! Scale and generate point
      DO iloc = 1,nloc
        field(iloc) = field(iloc) + ( 1.0_pr - 2.0_pr * MODULO(ipoint, 2 )) * scale * EXP( - (SUM( ( x(iloc,1:dim) - x_loc(1:dim))**2 ))/ ( 2.0_pr*MINVAL(h(1,1:dim)/2.0_pr**(j_mx - 1)))   )
      END DO
    END DO

  END SUBROUTINE add_smooth_noise



END MODULE equations_compressible_utils
