MODULE time_int_cn_mod
  USE debug_vars
  USE wlt_trns_mod
  USE vector_util_mod
  USE elliptic_mod
  USE util_mod
  USE user_case
  USE penalization
  USE sizes
  IMPLICIT NONE

!!$  ! DEBUG
!!$#define DEBUG_CN

CONTAINS
  !
  ! Time advancement routines for crank-Nicholson time advancement method
  !

  !--************************************************************************
  !--Begin Crank-Nicolson time step subroutines
  !--************************************************************************
  SUBROUTINE time_adv_cn (u_local, p, nwlt_in, t_local)
    USE share_kry
    USE precision
    USE util_vars
    USE pde
    USE share_consts
    USE parallel
    USE penalization
    USE variable_mapping
    USE user_case         ! user_chi
    IMPLICIT NONE

    INTEGER,                         INTENT (IN)    :: nwlt_in
    REAL (pr),                       INTENT (INOUT) :: t_local
    REAL (pr), DIMENSION (nwlt_in),     INTENT (INOUT) :: p
    REAL (pr), DIMENSION (nwlt_in,n_integrated), INTENT (INOUT) :: u_local

    INTEGER                         :: k
    REAL (pr)                       :: err
    REAL (pr)                       :: drag, lift
    REAL (pr), DIMENSION (dim)      :: force
    REAL (pr), DIMENSION (nwlt_in)  :: p_b
    REAL (pr), DIMENSION (nwlt_in,n_integrated) :: du, u_big, u_small_1, u_small_2
    REAL (pr), DIMENSION (nwlt,dim) ::  u_force
    !  REAL (pr), PARAMETER            :: t_adapt =  2e-02_pr !moved to share_kry, read in as input parameter
    LOGICAL                         :: tmpl

    REAL(pr) :: tmp1, tmp2, tmp3
    REAL (pr) :: err2(1:n_integrated),err3(1:n_integrated),err4(1:n_integrated)  !TEST
    INTEGER :: ierr !TEST


#ifdef DEBUG_CN
    PRINT *, 'time_adv_cn (), nwlt_in,t_local = ', nwlt_in, t_local
    PRINT *,'MIN/MAX(u_local)=',MINVAL(u_local),MAXVAL(u_local)
    PRINT *,'MIN/MAX(p)=',MINVAL(p),MAXVAL(p)
#endif
    !PAUSE

    told = t_local
    n = nwlt_in * n_integrated
    ne = n_integrated
    ng = nwlt_in

    !--Allocate array for velocity at previous time step
    IF (ALLOCATED(u_prev_timestep)) DEALLOCATE(u_prev_timestep)
    ALLOCATE (u_prev_timestep(1:n))
    u_prev_timestep = 0.0_pr

    err = 1e10_pr
    k = 1
    ! May need to do this for restart?? DG   IF (t-t_begin>=t_adapt .AND. .NOT. first_loop_after_restart ) THEN !--Adaptive time step

    ! (though all global, synchronize in case of inexact comparison)
    tmpl = (t-t_begin>=t_adapt  )
    !CALL parallel_global_sum( LOGICALOR=tmpl )
    !it was scl_global related bug; no synchronization is required

    IF ( tmpl ) THEN !--Adaptive time step

       IF (verb_level.GT.0.AND.par_rank.EQ.0) &
            PRINT *,'------------------------------Adaptive time step ----------------------------'
       DO WHILE (err>tol3)
          !--Calculate solution with time step dt anf dt/2
          IF (k==1) THEN
             du = 0.0_pr
             p_b = p
             IF (verb_level.GT.0.AND.par_rank.EQ.0) PRINT *,'-- Calculate solution with time step dt -- '
             IF(.NOT. stationary_obstacle) &
                penal(:) = user_chi(nwlt_in,t_local + dt) !reset for different position due time step change
             CALL time_step_cn (u_local, u_big, du, p_b, u_force, nwlt_in)
             dt = dt/2.0_pr ! set dt to 1/2 time step
          ELSE
             dt = dt/2.0_pr !decreases time step by two
             IF(.NOT. stationary_obstacle) THEN
                penal(:) = user_chi(nwlt_in,t_local + dt) !reset for different position due time step change
                CALL time_step_cn (u_local, u_big, du, p_b, u_force, nwlt_in)
             ELSE
                u_big = u_small_1
             END IF
             dt = dt/2.0_pr ! set dt to 1/2 time step
          END IF

          !--Calculate solution with time step dt
          !p = p_b
          p = 0.5_pr*(p+p_b)

          IF (verb_level.GT.0.AND.par_rank.EQ.0) PRINT *,'-- Calculate solution with time step dt -- '
          CALL time_step_cn (u_local, u_small_1, du, p, u_force, nwlt_in)
          p_b = p

          !--Calculate another time step dt
          IF (verb_level.GT.0.AND.par_rank.EQ.0) PRINT *,'-- Calculate another time step dt -- '
          CALL time_step_cn (u_small_1, u_small_2, du, p, u_force, nwlt_in)

          dt = 2.0_pr * dt ! set dt to full time step

          !--Calculate error
          IF (imask_obstacle) THEN
             !           err = SQRT(SUM(SUM((u_big-u_small_2)**2,2)*dA*(1.0_pr-penal)))/2.0_pr !--L2 error
             !normalized by area as per Nicholas suggestion 02072005
             tmp1 = SUM(SUM((u_big-u_small_2)**2,2)*dA*(1.0_pr-penal))
             tmp2 = SUM(dA*(1.0_pr-penal))
             CALL parallel_global_sum( REAL=tmp1 )
             CALL parallel_global_sum( REAL=tmp2 )
             err = SQRT(tmp1) / (tmp2*2.0_pr) !--L2 error
          ELSE
             !original  err = SQRT(SUM(SUM((u_big-u_small_2)**2,2)*dA))/(SUM(dA)*6.0_pr) !--L2 error
             !err = SQRT(SUM(SUM((u_big-u_small_2/MAX(ABS(u_big),tol3))**2,2)*dA))/(SUM(dA)*6.0_pr) !--L2 error

             DO ierr =1,n_integrated
                err4(ierr) = MAX(SUM(dA)*scl_global(ierr)**2,SUM(u_small_2(:,ierr)**2.0_pr*dA))
!!$                err3(ierr) = &
!!$                     SQRT(SUM( (u_big(:,ierr)-u_small_2(:,ierr))**2*dA)/MAX(SUM(dA)*scl_global(ierr)**2,SUM(u_small_2(:,ierr)**2*dA))) !--L2 error
                tmp1 = SUM( (u_big(:,ierr)-u_small_2(:,ierr))**2*dA )
                tmp2 = SUM(u_small_2(:,ierr)**2*dA)
                tmp3 = SUM(dA)
                CALL parallel_global_sum( REAL=tmp1 )
                CALL parallel_global_sum( REAL=tmp2 )
                CALL parallel_global_sum( REAL=tmp3 )

                err3(ierr) = SQRT( tmp1 / MAX(tmp3*scl_global(ierr)**2,tmp2) ) !--L2 error

                IF (verb_level.GT.0.AND.par_rank.EQ.0) PRINT *,'Relative error ', ierr ,  err3(ierr),  err4(ierr)
             END DO
             err = MAXVAL( err3 )
             IF (verb_level.GT.0.AND.par_rank.EQ.0) PRINT *,'Using Relative Error '
          END IF

          IF (verb_level.GT.0.AND.par_rank.EQ.0) &
               WRITE (6,'("Time iteration ", i3,1x,"dt = ",es8.2, " L2 error = ",es8.2)') k, dt, err
          k = k+1
       END DO
       IF (imask_obstacle) THEN
          u_local = (2.0_pr**2*u_small_2-u_big)/(2.0_pr**2-1.0_pr) !--Richardson extrapolation (assume local second-order)
       ELSE
          u_local = (2.0_pr**3*u_small_2-u_big)/(2.0_pr**3-1.0_pr) !--Richardson extrapolation (assume local third-order)
       END IF
       t_local = t_local + dt
       IF(err<tol3/2.0_pr) THEN
          dt = MIN(dtmax,1.25_pr*dt)
          IF (verb_level.GT.0.AND.par_rank.EQ.0) PRINT *,'Increasing dt by 25%'
       END IF
    ELSE !--Non-adaptive time step
       !user_chi is set up for t_local + dt in wlt_3d_main
       IF(.NOT. stationary_obstacle) penal(:) = user_chi(nwlt_in,t_local + dt) !for moving or deformable
       IF (verb_level.GT.0.AND.par_rank.EQ.0) &
            PRINT *,'---------------------------Non-adaptive time step ----------------------------'
       du = 0.0_pr
       CALL time_step_cn (u_local, u_local, du, p, u_force, nwlt_in)
       t_local = t_local + dt
    END IF

#ifdef DEBUG_CN
    PRINT *, 'dt=',dt
#endif
    DO k=1,n_var
       n_var_index(k) = k
    END DO

  END SUBROUTINE time_adv_cn

  SUBROUTINE time_step_cn (u_in, u_out, du, p, u_force, nwlt_in)
    USE share_kry
    USE precision
    USE util_vars
    USE pde
    USE share_consts
    USE user_case
    USE SGS_incompressible
    USE parallel
    IMPLICIT NONE

    INTEGER,                         INTENT (IN)   :: nwlt_in
    REAL (pr), DIMENSION (nwlt_in),     INTENT(INOUT) :: p
    REAL (pr), DIMENSION (nwlt_in,n_integrated), INTENT(IN)    :: u_in
    REAL (pr), DIMENSION (nwlt_in,n_integrated), INTENT(OUT)   :: u_out
    REAL (pr), DIMENSION (nwlt_in,n_integrated), INTENT(INOUT) :: du
    REAL (pr), DIMENSION (nwlt,dim), INTENT(INOUT) :: u_force
    REAL (pr), DIMENSION (n)    :: rhs_local, rhs_BC_local
    INTEGER, DIMENSION (n_integrated) :: clip
    INTEGER                         :: ie, shift
    INTEGER, PARAMETER              :: meth=1
    REAL(pr) :: tmp1
    LOGICAL :: set_boundaries

    clip = 0
    !--Save old velocity in shared variable
    DO ie = 1, ne
       shift = (ie-1)*ng
       u_prev_timestep(shift+1:shift+ng) = u_in(:,ie)
    END DO
!!$  u_prev_timestep(1:ng*ne) = RESHAPE( u_in(1:ng,1:ne), (/ng*ne/) )

    !--Non-solenoidal step
    IF (verb_level.GT.0.AND.par_rank.EQ.0) PRINT *,'---------------Non-solenoidal step---------------------'
    rhs_local =  user_rhs(u_in,p)
    !PRINT *, 'rhs_local=',MINVAL(rhs_local), MAXVAL(rhs_local), 'j_lev=', j_lev

    !--Set operator on boundaries
    ! (to compute du all the processors have to enter)
    set_boundaries = ( Nwlt_lev(j_lev,1) >  Nwlt_lev(j_lev,0) )
    CALL parallel_global_sum( LOGICALOR=set_boundaries )
    IF( set_boundaries ) THEN
       IF(diagnostics_elliptic) WRITE(*,'("Before user_algebraic_BC_rhs (rhs_local, ne, ng, j_lev)")')
       CALL user_algebraic_BC_rhs (rhs_local, ne, ng, j_lev)

       IF(diagnostics_elliptic) &
            WRITE(*,'("Before user_algebraic_BC (rhs_BC_local, u_in, nwlt_in, n_integrated, j_lev, meth) ")')
       rhs_BC_local = 0.0_pr
       CALL user_algebraic_BC (rhs_BC_local, u_in, nwlt_in, n_integrated, j_lev, meth)

       rhs_local = rhs_local - rhs_BC_local
!!$     WHERE ( rhs_BC_local /= 0.0_pr ) rhs_local = rhs_local - rhs_BC_local
    END IF

    IF(diagnostics_elliptic) THEN
       PRINT *, 'BEFORE BiCGSTAB'
       DO ie = 1, ne
          PRINT *, 'time_step_cn ie  MINMAX u_in ', ie, MINVAL( u_in(:,ie) ), MAXVAL( u_in(:,ie) )
          PRINT *, 'time_step_cn ie  MINMAX u_out ', ie, MINVAL( u_in(:,ie) + du(:,ie) ), MAXVAL( u_in(:,ie) + du(:,ie) )
       END DO
    END IF

    CALL BiCGSTAB (j_lev, nwlt_in, n_integrated, 1000, meth, clip, tol1, du, &
	 rhs_local, Lcn, Lcn_diag, SCL=scl_global(1:n_integrated) )

    IF(diagnostics_elliptic) THEN
       PRINT *, 'AFTER BiCGSTAB'
       DO ie = 1, ne
          PRINT *, 'time_step_cn ie  MINMAX u_in ', ie, MINVAL( u_in(:,ie) ), MAXVAL( u_in(:,ie) )
          PRINT *, 'time_step_cn ie  MINMAX u_out ', ie, MINVAL( u_in(:,ie) + du(:,ie) ), MAXVAL( u_in(:,ie) + du(:,ie) )
       END DO
    END IF
    !--Save velocity for force calculation
    !Oleg: need to take out this functionality out of integration step
    IF(n_integrated >= dim) u_force(:,1:dim) = u_in(:,1:dim) + du(:,1:dim)

    u_out = u_in + du
    IF(sgsmodel /= 0) THEN
       IF(SGS_TimeIntegrationType==1 ) THEN
          IF (verb_level.GT.0 .AND. par_rank.EQ.0) &
              PRINT *,'SGS_TimeIntegrationType==1, implicit 1-st order sgs_force'
          du(:,1:N_SGS_TimeIntegration) = sgs_mdl_force(:,1:N_SGS_TimeIntegration)
          CALL user_sgs_force(u_out, nwlt_in)
          u_out(:,1:N_SGS_TimeIntegration) = u_out(:,1:N_SGS_TimeIntegration) &
               + dt*(sgs_mdl_force(:,1:N_SGS_TimeIntegration)-du(:,1:N_SGS_TimeIntegration))
       ELSE IF(SGS_TimeIntegrationType==2) THEN
          IF (verb_level.GT.0 .AND. par_rank.EQ.0) &
              PRINT *,'SGS_TimeIntegrationType==2, implicit 2-nd order sgs_force'
          du(:,1:N_SGS_TimeIntegration) = sgs_mdl_force(:,1:N_SGS_TimeIntegration)
          CALL user_sgs_force(u_out, nwlt_in)
          u_out(:,1:N_SGS_TimeIntegration) = u_out(:,1:N_SGS_TimeIntegration) &
               + 0.5_pr*dt*(sgs_mdl_force(:,1:N_SGS_TimeIntegration)-du(:,1:N_SGS_TimeIntegration))
       END IF
    END IF

    !--Projection step
    IF (verb_level.GT.0.AND.par_rank.EQ.0) PRINT *,'---------------Projection step---------------------'
    !  CALL user_project (du, p, nwlt_in, meth)  !note before it was acted on incremetnal step, now it act onthe whole solution
    ! u_out = u_out + du

#ifdef DEBUG_CN
    PRINT *, 'in time_step_cn before user_project:'
    PRINT *, 'MINMAX(u_out)=', MINVAL(u_out), MAXVAL(u_out)
    PRINT *, 'MINMAX(p)=', MINVAL(p), MAXVAL(p)
#endif

    CALL user_project (u_out, p, nwlt_in, meth)

#ifdef DEBUG_CN
    PRINT *, 'in time_step_cn after user_project:'
    PRINT *, 'MINMAX(u_out)=', MINVAL(u_out), MAXVAL(u_out)
    PRINT *, 'MINMAX(p)=', MINVAL(p), MAXVAL(p)
    !PAUSE 'in time_step_cn'
#endif

  END SUBROUTINE time_step_cn

  FUNCTION Lcn (jlev, u, nlocal, ne_local, meth)
    USE precision
    USE sizes
    USE pde
    USE share_kry
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: jlev, nlocal, ne_local, meth
    INTEGER :: i, ii, ie, shift
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT(INOUT) :: u
    REAL (pr), DIMENSION (nlocal*ne_local) :: Lcn
    REAL (pr), DIMENSION (nlocal*ne_local) :: Jac

!!$    PRINT *, 'in Lcn before user_Drhs:'
!!$    PRINT *, '  MINMAX(u)=', MINVAL(u), MAXVAL(u)
!!$    PRINT *, '  MINMAX(u_prev_timestep)=', MINVAL(u_prev_timestep), MAXVAL(u_prev_timestep)
!!$    PRINT *, '  meth=', meth
    Jac = user_Drhs(u, u_prev_timestep, meth)

!!$    PRINT *, 'in Lcn after user_Drhs:'
!!$    PRINT *, '  MINMAX(u)=', MINVAL(u), MAXVAL(u)
!!$    PRINT *, '  MINMAX(u_prev_timestep)=', MINVAL(u_prev_timestep), MAXVAL(u_prev_timestep)
!!$    PRINT *, '  MINMAX(Jac)=', MINVAL(Jac), MAXVAL(Jac)
!!$    PAUSE 'in Lcn after user_Drhs'


    Lcn = 0.0_pr

    DO ie = 1, ne_local
       shift = nlocal*(ie-1)
       Lcn(shift+1:shift+nlocal) = u(shift+1:shift+nlocal)/dt - Jac(shift+1:shift+nlocal)/2.0_pr
    END DO


!!$    PRINT *, 'in Lcn after Jac:'
!!$    PRINT *, '  MINMAX(Lcn)=', MINVAL(Lcn), MAXVAL(Lcn)
!!$    PAUSE



    !--Set operator on boundaries
    IF(MINVAL(ABS(prd)) == 0) THEN
       CALL user_algebraic_BC (Lcn, u, nlocal, ne_local, jlev, meth)

#ifdef DEBUG_CN
       PRINT *, 'in Lcn after user_algebraic_BC:'
       PRINT *, '  MINMAX(Lcn)=', MINVAL(Lcn), MAXVAL(Lcn)
       PRINT *, '  MINMAX(u)=', MINVAL(u), MAXVAL(u)
#endif

    END IF

  END FUNCTION Lcn

  FUNCTION Lcn_diag (jlev, nlocal, ne_local, meth)
    USE precision
    USE sizes
    USE pde
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: jlev, nlocal, ne_local, meth
    INTEGER :: ie, shift
    REAL (pr), DIMENSION (nlocal*ne_local) :: Lcn_diag
    REAL (pr), DIMENSION (nlocal*ne_local) :: Jac_diag

    Jac_diag = user_Drhs_diag (meth)
    DO ie = 1, ne_local
       shift = nlocal*(ie-1)
       Lcn_diag(shift+1:shift+nlocal) = 1.0_pr/dt - Jac_diag(shift+1:shift+nlocal)/2.0_pr
    END DO

    !--Set operator on boundaries
    IF(MINVAL(ABS(prd)) == 0) CALL user_algebraic_BC_diag (Lcn_diag, nlocal, ne_local, jlev, meth)


  END FUNCTION Lcn_diag

END MODULE time_int_cn_mod

!* CVS Log
!* $Log: time_int_cn.f90,v $
!* Revision 1.17  2005/06/29 16:51:45  dan
!* -- still working LDM model
!*    I implemented a relativ error for time tolerance in time_int_cn.f90
!*   and in BICGSTAB in elliptic_solve_3d.f90
!*
!* Revision 1.16  2005/06/15 22:33:58  dan
!* --Added post_process/convert_output  and manual and results (empty) directories
!*
!* Revision 1.15  2005/06/13 23:10:04  dan
!* --working on LAgrangian mdl..
!*
!* Revision 1.14  2005/06/06 16:49:53  dan
!* -- merging in Giuliano's local dynamic model case.
!*
!* Revision 1.13  2005/06/03 16:54:05  dan
!* -- merging in Giuliano's local dynamic model case.
!*
!* Revision 1.12  2005/06/02 20:24:48  dan
!* -- in main,  changed it to calculate grid and test mask if sgsmodel >= 1
!*    was >1 (this is safer because const Cs needs filter in some cases..)
!*
!*      !
!*      ! Setup up any model filter masks needed
!*      !
!*      IF(  sgsmodel >= 1 .OR. ExplicitFilter .OR. ExplicitFilterNL ) THEN
!*
!* -- moved sgs_model routines into the case file. The routine  user_sgs_force()
!*    is called at the beginning of the time step from time_adv_cn().
!*
!* -- moved usitlity routines
!*
!* -- main, for restart, initial inverse transform now done in adapt_grid.
!*    Also u_old wasn't allocated correctly for restart in wrk version.
!*    I fixed this.
!*    BUT there is still a problem with restart for the case_isoturb..?
!*
!* -- wavelet_3d_wrk.f90
!*    changed adapt_grid() for correct restart. Now all of routine is
!*    used in restart.
!*
!* -- moved sij() to vector_util.f90
!*
!* -- moved wlt_filt_wmask(), make_mdl_filts(),mdl_filt() to util_3d.f90
!*
!* -- added global logical  first_loop_after_restart which is set true for the
!*    first loop after restart. I thought this might be needed for solver on
!*    restart. But not sure yet.
!*
!* -- added routine reverse_endian.f used in reversing byte ordering.
!*    This is currently not used yet.
!*
!* Revision 1.11  2005/05/23 16:26:09  dan
!* -- changed Check_Exact_Soln() to print both abs and relative error.
!*
!* -- gmres()  added two new tolerance variable read from input file
!*     tol_gmres - used in iteration loop as tolerance for gmres
!*     tol_gmres_stop_if_larger - if true and the current error is larger then the last error gmres exits the iteration loop.
!*
!* -- added print statements in time integration routines to better follow it running
!*
!* Revision 1.10  2005/04/19 22:23:46  dan
!* -added input variable t_adapt used in time integration cn method.
!*  if t > t_adapt then adaptive time step is used...
!*
!* Revision 1.9  2005/03/15 22:11:38  dan
!* fix c_wlt_trns() in wavelet_3d_lines.f90 (It was nto putting data in db first
!*
!* Revision 1.8  2005/02/25 17:40:45  dan
!* -- Changes merged from Oleg, 2/24/2005
!*
!*  - case_vortex_timeint_cn.f90 updated to better variable names, ne-> nelocal ...
!*
!*
!* - wlt_3d_main.f90
!*    made calculation on area general for any dim
!*
!* Revision 1.7  2005/02/23 15:33:53  dan
!*
!* -- renamed u_b to u_prev_timestep
!*
!* -- implemented c_diff_fast along an array of variables
!*  - To do this I broke c_diff_fast into two routines.
!*     c_diff_fast_db does the deriviatives directly out of the db.
!*     c_diff_fast loads variables in to the db and then calls
!*     c_diff_fast_db().
!*
!* Revision 1.6  2005/02/14 18:15:58  dan
!* Added inpuit flag Zero_Mean
!*  if true a zero mean is imposed for first three variables (currently velocity).
!* This is only applied to time_int_cn method for now.
!*
!* Fixed error in interpolate that didn't zero out db for levels j_in to j_out if
!* j_in < j_out . This only effects db_lines version
!*
!* Added diff_aux2_db to do derivative in case j==j_in from c_diff_fast (db_lines version)
!*
!* Revision 1.5  2005/02/10 03:20:31  dan
!* changed error check to use L2 norm
!*
!* Revision 1.4  2005/02/07 16:00:20  dan
!* normalized L2 error by the area as per Nicholas suggestion (DG)
!*
!* Revision 1.3  2005/02/02 16:18:15  dan
!* working on new wavelet_lines weights() routine
!* I had to rename nwtl to nwlt_in in many routines becasue it confliced with
!* global nwlt. This needs to be changed to just use the global version (from shar_vars)
!*
!* Revision 1.2  2005/01/31 19:58:58  dan
!* -modified case_vortex_timeint_cn.f90 to run 2d and 3d correctly
!*
!* -default_util.f90 missing comma in write statement cause it not to compile on IBM
!*
!* -io_3d.f90 commented out a check that did not take into account additional variables
!*
