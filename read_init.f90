MODULE read_init_mod
  IMPLICIT NONE
  
  PUBLIC :: read_initial_data
  
CONTAINS
  
  
  !
  ! nx,ny,nz are the resolution of the file being read in. If ti does not
  ! match nxyz (the global unadapted resolution) then the
  ! field if projected to the new resolution.
  !
  SUBROUTINE read_initial_data( u, nlocal , ieq,  &
       filename, file_fmt,iter,j_init )
    USE PRECISION
    USE pde
    USE share_consts
    USE read_data_wry
    USE fft_module
    USE ch_resolution
    USE spectra_module
    USE wlt_trns_mod
    USE wlt_trns_util_mod
    USE util_mod
    USE debug_vars            ! verb_level
    USE sizes
    USE read_netcdf_mod
    USE share_consts
    USE parallel
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: nlocal, ieq,file_fmt, j_init
    INTEGER  , SAVE :: nxyz_init(3)
    INTEGER  , INTENT (INOUT) :: iter                      ! iteration of call while adapting initial grid
    REAL (pr), DIMENSION (nlocal,ieq), INTENT (INOUT) :: u !
    CHARACTER (LEN = *) :: filename 

    REAL (pr) , SAVE, DIMENSION(:,:,:,:),ALLOCATABLE :: field_file_res
    !REAL (pr) :: field(nxyz_init(1)+2,nxyz_init(2)+1,nxyz_init(3)+1,3)
    REAL (pr) :: the_viscosity(1), pi2
    INTEGER   :: i, k
    !INTEGER   , SAVE :: nx,ny,nz
    INTEGER   :: offset(1:dim)
    REAL(pr)  :: sumUU(1:dim)
    INTEGER :: nloc
    INTEGER, DIMENSION(dim) :: ixyz
    INTEGER, DIMENSION(nwlt) ::  i_arr,deriv_lev_loc
    INTEGER, DIMENSION(nwlt,1:dim) ::  indx_loc
    INTEGER, DIMENSION(0:dim) :: i_p
    REAL(pr) :: the_tke,the_tdiss
    INTEGER :: iostat
    LOGICAL :: do_verb

    !  INTERFACE 
    !     SUBROUTINE reverse_endian_real( buff, n_bytes_item, n_item )
    !	  USE precision
    !      IMPLICIT NONE
    !      INTEGER          ,INTENT(IN)    :: n_bytes_item, n_item
    !      REAL(pr)         ,INTENT(INOUT) :: buff(n_item)
    !	 END SUBROUTINE reverse_endian_real
    !     SUBROUTINE reverse_endian_int( buff, n_bytes_item, n_item )
    !	  USE precision
    !      IMPLICIT NONE
    !      INTEGER          ,INTENT(IN)    :: n_bytes_item, n_item
    !      INTEGER          ,INTENT(INOUT) :: buff(n_item)
    !	 END SUBROUTINE reverse_endian_int
    !  END INTERFACE

    
    do_verb = .FALSE.
    IF (BTEST(verb_level,0)) do_verb = .TRUE.
    
    
    ! On the first iteration we read in the file 
    ! 
    IF( iter == 1 ) THEN
       
       pi2 = 2.0_pr * pi
       nxyz_init(1) = 0 ; nxyz_init(2) = 0; nxyz_init(3) = 0
       IF (do_verb) PRINT *, '*** Reading intitial condition file: ', &
            filename(1:INDEX(filename,' '))
       ! check file format argument
       ! file format 
       ! 1 - netcdf file
       ! 2 - alan wrays file format in fourier space
       ! 3 - simple binary format 
       SELECT CASE ( file_fmt )
       CASE( 1 )
          IF (do_verb) PRINT *,'Reading in IC file in netcdf format '
       CASE( 2 ) !Alan Wrays file format
          IF (do_verb) PRINT *,'Reading in IC file in A. Wray format '
          ! first read in the feild dimensions
          CALL read_data_wray_dims(TRIM(filename)//' ',nxyz_init(1),nxyz_init(2),nxyz_init(3),the_viscosity(1))
       CASE( 3 ) ! simple binary file format
          IF (do_verb) PRINT *,'Reading in IC file in simple binary  format '
          OPEN(FILE=TRIM(filename),STATUS='old',UNIT=1,FORM='UNFORMATTED')

          READ(1) nxyz_init(1),nxyz_init(2),nxyz_init(3)
          IF (do_verb) PRINT *,'nxyz_init ' , nxyz_init
          !in progress! IF( .NOT. big_endien() ) CALL reverse_endian_int( nxyz_init, 3 )

          READ(1) the_viscosity(1)
          !in progress! IF( .NOT. big_endien() ) CALL reverse_endian_real( the_viscosity(1), 1 )

          IF (do_verb) PRINT *,'Dims in file:  nxyz_init(1),nxyz_init(2),nxyz_init(3)', &
               nxyz_init(1),nxyz_init(2),nxyz_init(3)
          CLOSE(1)
       CASE DEFAULT
          IF (par_rank.EQ.0) THEN
             PRINT *,'Unknown IC file format type:',  file_fmt
             PRINT *,'Exiting ...'
          END IF
          CALL parallel_finalize
          STOP
       END SELECT
       IF (do_verb) PRINT *,'Intial time step resolution:', nxyz_init(1),nxyz_init(2),nxyz_init(3)

       IF(nxyz_init(1) == 0 .OR. nxyz_init(2) == 0 .OR. nxyz_init(3) == 0 ) THEN
          IF (do_verb) PRINT *,'Couldn''t read field resolution from file.'
          IF (do_verb) PRINT *,'Setting resolution from j_init'

          !calculate initial non-adaptive resolution from levels and M
          nxyz_init(1:2)=mxyz(1:2)*2**(j_init-1) 
          nxyz_init(3)=mxyz(3)*2**(jd*(j_init-1))

       END IF
       IF (do_verb) PRINT *,'Input data field resolution ',nxyz_init(1),nxyz_init(2),nxyz_init(3)

       !
       ! Check the size of the input field will work.
       ! The input file resolutions needs to be an even multiple of the
       ! current field
       IF(  MOD(MOD(nxyz_init(1),nxyz(1)),2) /= 0 .or. &
            MOD(MOD(nxyz_init(2),nxyz(2)),2) /= 0 .or. &
            (dim == 3 .AND. MOD(MOD(nxyz_init(3),nxyz(3)),2) /= 0 ))  THEN
          PRINT *,'Error in read_intitial_data(), mod(mod(nxyz_init,nxyz)),2) /= 0 '
          PRINT *,' mod(mod(nxyz_init,nxyz)) =', &
               MOD(MOD(nxyz_init(1),nxyz(1)),2), &
               MOD(MOD(nxyz_init(2),nxyz(2)),2), &
               MOD(MOD(nxyz_init(3),nxyz(3)),2)
          PRINT *,' Exiting ... '
          STOP
       END IF

       ALLOCATE ( field_file_res(nxyz_init(1)+2,nxyz_init(2)+1,nxyz_init(3)+1,3), STAT=iostat )
       CALL test_alloc ( iostat, 'field_file_res in read_initial_data', (nxyz_init(1)+2)*(nxyz_init(2)+1)*(nxyz_init(3)+1)*3 )

       !
       ! Alan Wrays file format is saved as Fourier Modes so we will need an FFT to 
       ! recover the data
       !
       IF(  file_fmt == 2 ) THEN

          !**** Initialize Fourier Transforms to the input file resolution ****
          CALL init_fft(nxyz_init(1),nxyz_init(2),nxyz_init(3),pi2,pi2,pi2, .TRUE. )
          IF (do_verb) PRINT *, "Setting domain to 0-2Pi cube, periodic..."
!!$        CALL init_fft(nx,nxyz_init(2),nxyz_init(3),xyzlimits(2,1) - xyzlimits(1,1), &
!!$             xyzlimits(2,2) - xyzlimits(1,2), &
!!$             xyzlimits(2,3) - xyzlimits(1,3), .true. )

       END IF


       !CALL init_fft(nxyz_init(1),nxyz_init(2),nxyz_init(3),pi2,pi2,pi2, .TRUE. )
       !  CALL test_fft()
       !  stop
       ! file format 
       ! 1 - netcdf file
       ! 2 - alan wrays file format in fourier space
       ! 3 - simple binary format 
       SELECT CASE ( file_fmt )
       CASE( 1 )
          !filename = '/home/dan/data/rn10e.nc.029  '
          !filename = '/home/dan/data/rn11i.nc.069  '
          CALL read_netcdf_field_real8(field_file_res(1:nxyz_init(1),1:nxyz_init(2),1:nxyz_init(3),1),&
               nxyz_init(1),nxyz_init(2),nxyz_init(3), &
               TRIM(filename)//' ','u',the_viscosity(1))
          CALL read_netcdf_field_real8(field_file_res(1:nxyz_init(1),1:nxyz_init(2),1:nxyz_init(3),2),&
               nxyz_init(1),nxyz_init(2),nxyz_init(3), &
               TRIM(filename)//' ','v',the_viscosity(1))
          CALL read_netcdf_field_real8(field_file_res(1:nxyz_init(1),1:nxyz_init(2),1:nxyz_init(3),3),&
               nxyz_init(1),nxyz_init(2),nxyz_init(3), &
               TRIM(filename)//' ','w',the_viscosity(1))


       CASE( 2 ) !Alan Wrays file format
          CALL read_data_wray(TRIM(filename)//' ',&
               field_file_res(:,:,:,1),field_file_res(:,:,:,2),field_file_res(:,:,:,3),&
               nxyz_init(1),nxyz_init(2),nxyz_init(3),the_viscosity(1))

          ! Print Espectra of the initial.data
          CALL print_spectra2( field_file_res(:,:,:,1),field_file_res(:,:,:,2),field_file_res(:,:,:,3),&
               TRIM(filename)//'.initial.spectra ',nxyz_init(1),nxyz_init(2),nxyz_init(3),&
               the_tke,the_tdiss)

          IF (do_verb) THEN
             PRINT*,' '
             PRINT*,'Initial Data: TKE        = ', the_tke
             PRINT*,'              Total Diss = ', the_tdiss
             PRINT*,' '
          END IF

          ! Data read in Fourier space, We transform to real space here
          CALL ftort(field_file_res(:,:,:,1))
          CALL ftort(field_file_res(:,:,:,2))
          CALL ftort(field_file_res(:,:,:,3))


       CASE( 3 ) ! simple binary file format
          !filename = '/home/dan/data/rn10e.nc.029.bin'
          OPEN(FILE=TRIM(filename),STATUS='old',UNIT=1,FORM='UNFORMATTED')

          READ(1) nxyz_init(1),nxyz_init(2),nxyz_init(3)
          !in progress! IF( .NOT. big_endien() ) CALL reverse_endian_int( nxyz_init, 3 )

          READ(1) the_viscosity(1)
          !in progress! IF( .NOT. big_endien() ) CALL reverse_endian_real( the_viscosity(1), 1 )


          READ(1) field_file_res(1:nxyz_init(1),1:nxyz_init(2),1:nxyz_init(3),1)
          READ(1) field_file_res(1:nxyz_init(1),1:nxyz_init(2),1:nxyz_init(3),2)
          READ(1) field_file_res(1:nxyz_init(1),1:nxyz_init(2),1:nxyz_init(3),3)
          !in progress! IF( .NOT. big_endien() ) CALL reverse_endian_real( field_file_res, 3*nxyz_init(1)*nxyz_init(2)*nxyz_init(3) )
       END SELECT

       ! Set nu from the input file 
       ! nu = the_viscosity

       !PRINT *, '*** Setting nu = ', nu ,' from the initial condition file '
       IF (do_verb) PRINT *, 'Viscosity from input file = ', the_viscosity(1)

    END IF !done reading in file for iter == 1


    IF(    nxyz(1) > nxyz_init(1)  .or. &
         nxyz(2) > nxyz_init(2)  .or. &
         nxyz(3) > nxyz_init(3) ) THEN

       ! 
       ! we have used all the input data we can so set iter to -1
       ! (which will cause the memory to be deallocated below)
       !  
       iter = -1 
    ELSE


       !
       ! Calculate the offsets to put the data into the u array
       !
       offset(1:dim) = nxyz_init(1:dim)/nxyz(1:dim)

       ! Save the field into the flat u array that will be used in the rest 
       ! of the code

       !Assumes 3-D inital data field
       CALL get_indices_and_coordinates_by_types ( (/0,2**dim-1/), (/0,3**dim-1/), (/1,j_lev/), &
	    j_lev, nloc, i_arr, indx_loc,deriv_lev_loc)
       DO k = 1, nloc
          u(i_arr(k),1) = field_file_res(offset(1)*indx_loc(k,1)+1,offset(2)*indx_loc(k,2)+1,offset(3)*indx_loc(k,3)+1,1) 
          u(i_arr(k),2) = field_file_res(offset(1)*indx_loc(k,1)+1,offset(2)*indx_loc(k,2)+1,offset(3)*indx_loc(k,3)+1,2) 
          u(i_arr(k),3) = field_file_res(offset(1)*indx_loc(k,1)+1,offset(2)*indx_loc(k,2)+1,offset(3)*indx_loc(k,3)+1,3) 
       END DO
    END IF

    DO i=1,dim
       sumUU(i) = SUM( u(1:nlocal,i)**2)
       CALL parallel_global_sum( REAL=sumUU(i) )
    END DO

    IF (do_verb) THEN
       WRITE(*,'(" l2(old) u ", 3(es15.8,1x))' )  SQRT(sumUU(1)),&
            SQRT(sumUU(2)),&
            SQRT(sumUU(3))
       WRITE(*,'(" ||u||L2   ", 3(es15.8,1x))' ) SQRT(sumUU(1)/nlocal),&
            SQRT(sumUU(2)/nlocal),&
            SQRT(sumUU(3)/nlocal)
       WRITE(*,'(" ||u||inf  ", 3(es15.8,1x))' ) &
            MAXVAL(ABS(u(1:nlocal,1))),MAXVAL(ABS(u(1:nlocal,2))),MAXVAL(ABS(u(1:nlocal,3)))
    END IF
    
    !
    ! If this is the last call we deallocate the memory used.
    !
    IF (do_verb) PRINT *,' in read_init iter = ', iter
    IF( iter == -1 ) THEN
       DEALLOCATE(field_file_res)
       IF (do_verb) PRINT *,'DEALLOCATE(field_file_res) '
    END IF
  END SUBROUTINE read_initial_data

END MODULE read_init_mod
