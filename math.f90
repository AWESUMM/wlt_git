MODULE LOCAL_MATH
!!$  This is the local version of MATMUL xlf has some problem with.
!!$  Define OLOCAL_MATMUL and set LOCALMATH=math.o in machine specific
!!$  makefile in order to use the local version.
  PUBLIC :: LMATMUL

CONTAINS
  FUNCTION LMATMUL (A,B, SIZEA1, SIZEA2, SIZEB)
    USE precision
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: SIZEA1, SIZEA2, SIZEB
    REAL(pr), INTENT(IN) :: A(SIZEA1,SIZEA2), B(SIZEB)
    REAL(pr) :: LMATMUL(SIZEA1)
    INTEGER :: i
    
    DO i=1,SIZEA1
       LMATMUL(i) = DOT_PRODUCT(A(i,:),B(:))
    END DO
    
  END FUNCTION LMATMUL
END MODULE LOCAL_MATH
