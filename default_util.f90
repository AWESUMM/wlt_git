!
!This module contains procedures that have a user_case version
!
MODULE default_mod

  PUBLIC ::  cal_cfl,              &
       Check_Exact_Soln,           &
       check_setup_pde,            &
       place_pt_wlt_space,         &
       scales

  CHARACTER (LEN=*) , PRIVATE, PARAMETER :: Err_FMT = 'e21.11' !'e12.5' !'e21.11' 
CONTAINS

  !
  !************ Calculating Scales ***************************
  !
  ! Note the order of the components in the scl array
  ! correspond to u_tn, v_tn, w_tn, u_tn-1, v_tn-1, w_tn-1, u_tn-2, v_tn-2, w_tn-2
  ! 
  SUBROUTINE scales(flag,u_loc, nwlt_loc, ieq, l_n_var_adapt , l_n_var_adapt_index, &
       scl, scl_fltwt) !add
    USE precision
    USE wlt_vars       ! scaleCoeff
    USE util_vars
    USE pde
    USE user_case
    USE parallel
    USE debug_vars
    USE variable_mapping
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: flag ! 0 during initial adaptation, then 1 during time advancement
    INTEGER, INTENT (IN) :: nwlt_loc, ieq
    REAL (pr), DIMENSION (1:nwlt_loc,1:ieq), INTENT (INOUT) :: u_loc
    LOGICAL , INTENT (IN) :: l_n_var_adapt(ieq)
    INTEGER , INTENT (IN) :: l_n_var_adapt_index(1:ieq)
    REAL (pr), DIMENSION (1:ieq), INTENT (INOUT) :: scl
    REAL (pr) ,  INTENT(IN) ::scl_fltwt !weight for temporal filter on scl
    REAL (pr) :: floor, tmp
    INTEGER :: ie, ie_index, itmp
    REAL (pr) ,save, ALLOCATABLE, DIMENSION (:) :: scl_old
    LOGICAL  ,save :: startup_init = .TRUE.
    LOGICAL :: use_default = .TRUE.


    

    !
    ! ALLOCATE scl_old if it was not done previously in user_scales
    !
    IF( startup_init .AND. .NOT. ALLOCATED(scl_old) ) THEN
       ALLOCATE( scl_old(1:ieq) )
       scl_old = 0.0_pr
    END IF

    
    !
    ! NOTE: For a parallel run, synchronize scl(:) across the processors in user_scales.
    !       Use the subroutine scales of default_util.f90 as an example,
    !       subroutine parallel_global_sum of parallel.f90 may be helpful.
    !       Already synchronized: sumdA_global
    CALL user_scales( flag, use_default, u_loc, nwlt_loc, ieq, l_n_var_adapt , l_n_var_adapt_index, &
         scl, scl_fltwt)

    
    IF( use_default ) THEN
       IF (verb_level.GT.0.AND.par_rank.EQ.0) &
            WRITE (*,'("Using default Scales() routine, Scale_Meth = ", i3 )' ) Scale_Meth 
       
       floor = 1.e-12_pr
       scl   =1.0_pr
       DO ie=1,ieq
          IF(l_n_var_adapt(ie)) THEN
             ie_index = l_n_var_adapt_index(ie)

             IF( Scale_Meth == 1 ) THEN ! Use Linf scale                
                scl(ie) = MAXVAL ( ABS( u_loc(1:nwlt_loc,ie_index) ) )
                CALL parallel_global_sum( REALMAXVAL=scl(ie) )
                
             ELSE IF ( Scale_Meth == 2 ) THEN ! Use L2 scale
                scl(ie) = SUM( (u_loc(1:nwlt_loc,ie_index)**2) )
                itmp = nwlt_loc
                CALL parallel_global_sum( REAL=scl(ie) )
                CALL parallel_global_sum( INTEGER=itmp )
                scl(ie)= SQRT ( scl(ie)/itmp )

             ELSE IF ( Scale_Meth == 3 ) THEN ! Use L2 scale using dA
                scl(ie) = SUM( (u_loc(1:nwlt_loc,ie_index)**2)*dA )
                CALL parallel_global_sum( REAL=scl(ie) )
                scl(ie) = SQRT ( scl(ie)/sumdA_global )

             ELSE
                IF (par_rank.EQ.0) THEN
                   PRINT *, 'Error in Scales(), Unknown Scale_Meth type: ', Scale_Meth
                   PRINT *, 'Known Scale_Meth types: 1 = Linf scale, 2 = L2 scale '
                   PRINT *, 'Exiting ...'
                END IF
                CALL parallel_finalize; STOP
             END IF
             IF(scl(ie) .le. floor) scl(ie)=1.0_pr !Shouldn't this just set it to floor????
             tmp = scl(ie)
             ! temporally filter scl
             IF(.not. startup_init ) scl(ie) = scl_fltwt * scl_old(ie) + (1.0_pr - scl_fltwt) * scl(ie)
             scl_old(ie) = scl(ie) !save scl for this time step
             
             ! Save unmodified scale for temporal filtering
             scl_old(ie) = scl(ie) !save scl for this time step

             ! multiply scale coefficient read in from input file
             scl(ie) = scaleCoeff(ie) * scl(ie)
             
             IF (verb_level.GT.0.AND.par_rank.EQ.0) THEN
                WRITE (6,'("Scale factor eqn", I4 ," = ", es11.4 ," scalecoeff = ",es11.4 )' ) &
                     ie, scl(ie), scaleCoeff(ie)
                WRITE (6,'("(before filtering = ",es11.4,")" )') tmp
             END IF
          END IF
       END DO
       startup_init = .FALSE.
       !PRINT *,'TEST scl_old ', scl_old
       
    ELSE
       IF (verb_level.GT.0.AND.par_rank.EQ.0) &
            WRITE (*,'("Using user_Scales() routine, Scale_Meth = ", i3 )' ) Scale_Meth 
       
    END IF ! IF( use_default ) 
    
    scl_global(1:n_var) = scl  !copies to global scl array (module PDE) that can be used from other subrouthines 

  END SUBROUTINE scales

  !--************************************************************************
  !--Set MAXIMUM CFL
  !--************************************************************************

  SUBROUTINE cal_cfl (u_loc, cfl_out)
    USE precision
    USE sizes
    USE pde
    USE user_case
    USE parallel
    USE curvilinear_mesh 
    USE variable_mapping
    IMPLICIT NONE
    REAL (pr),                                INTENT (INOUT) :: cfl_out
    REAL (pr), DIMENSION (nwlt,n_integrated), INTENT (IN)    :: u_loc

    INTEGER                    :: i
    REAL (pr)                  :: floor
    REAL (pr), DIMENSION (1:nwlt,1:dim) :: v  ! velocity components for transformation 
    REAL (pr), DIMENSION (dim) :: cfl_all
    REAL (pr), DIMENSION(dim,nwlt) :: h_arr
    LOGICAL :: use_default = .TRUE.

    CALL user_cal_cfl ( use_default, u_loc, cfl_out)

    IF( use_default ) THEN
       floor = 1e-12_pr
       cfl_out = floor

       CALL get_all_local_h (h_arr)
       DO i = 1, nwlt
          v(i,1:dim) = u_loc(i,1:dim)+Umn(1:dim) 
       END DO
       v(:,1:dim) = transform_vector_to_comp( v(:,1:dim) )

       DO i = 1, nwlt
          cfl_all(1:dim) = ABS (v(i,1:dim) ) * dt/h_arr(1:dim,i)
          cfl_out = MAX (cfl_out, MAXVAL(cfl_all))
       END DO
    END IF ! IF( use_default )

    ! synchronize accross the processors
    CALL parallel_global_sum( REALMAXVAL=cfl_out )

  END SUBROUTINE cal_cfl


  ! 
  ! Check exact solution at this time step if there is an exact solution for this case
  !
  SUBROUTINE Check_Exact_Soln(u_loc,  u_ex_loc,  t_local, scl, l_n_var_exact_soln, VERB )
    USE precision
    USE penalization
    USE pde
    USE sizes
    USE user_case
    USE util_vars
    USE io_3D_vars
    USE io_3D
    USE parallel
    USE variable_mapping
    IMPLICIT NONE
    REAL (pr), INTENT (IN) ::  t_local
    REAL (pr), DIMENSION (nwlt,n_var), INTENT (IN) :: u_loc
    REAL (pr), DIMENSION (nwlt,n_var_exact), INTENT (INOUT) :: u_ex_loc
    REAL (pr), INTENT (IN) ::scl(1:n_var)
    REAL (pr)  :: L2err, Linferr, &
         L2error_scale, Linferror_scale, relL2err, relLinferr
    LOGICAL , INTENT (IN) :: l_n_var_exact_soln(1:n_var)
    LOGICAL , SAVE :: start = .TRUE.
    INTEGER :: i_exact, ie, i
    LOGICAL, INTENT(IN), OPTIONAL :: VERB        ! controls screen output
    LOGICAL :: verb_                             !
    
    verb_ = .FALSE.                      ! all output at
    IF (PRESENT(VERB)) verb_ = VERB      ! processor 0 only
    
    IF( n_var_exact > 0 ) THEN 
       
       ! Call the case specific exact solution routine
       call user_exact_soln(u_ex_loc, nwlt, t_local, l_n_var_exact_soln )
       
       ! Log error results on zero processor only
       IF (par_rank.EQ.0) THEN
          CALL open_file(TRIM(res_path)//TRIM(file_gen)//'err.exact.soln', it, 13, .TRUE., .TRUE.)
          CALL open_file(TRIM(res_path)//TRIM(file_gen)//'err.exact.soln_HighPrec', it, 14, .TRUE., .TRUE.)
       END IF
       
       IF( start.AND.par_rank.EQ.0 ) THEN
          WRITE(13,'("% time iteration ")',ADVANCE='NO')
          WRITE(14,'("% time iteration ")',ADVANCE='NO') !HighPrec
          i_exact = 0
          DO ie = 1,n_var
             i_exact = i_exact + 1
             IF( l_n_var_exact_soln(ie)  ) THEN
                WRITE(13,'(" L2err_ie",i2.2, " Linferr_ie",i2.2,"  ")',ADVANCE='NO') i_exact, i_exact
                WRITE(14,'(" L2err_ie",i2.2, " Linferr_ie",i2.2,"  ")',ADVANCE='NO') i_exact, i_exact
             END IF
          END DO
          WRITE(13,'(" ")') !finish line
          WRITE(14,'(" ")') !finish line !HighPrec
       END IF
       
       
       IF (par_rank.EQ.0) THEN
          WRITE(13,'(E12.5,1x,I10,1x,3(E12.5,1x) )',ADVANCE='NO') t,it
          WRITE(14,'(E12.5,1x,I10,1x,3(E12.5,1x) )',ADVANCE='NO') t,it !HighPrec
          
          ! Print out the results of comparing to the exact solution
          IF (verb_) THEN
             PRINT *,'== Compare Result to User Provided Exact Solution =='
             PRINT *,'===================================================='
          END IF
       END IF
       
       
       
       
       
       i_exact = 0
       DO ie = 1,n_var
          i_exact = i_exact + 1
          IF( l_n_var_exact_soln(ie)  ) THEN
             
             IF(imask_obstacle) THEN
                Linferr = MAXVAL (ABS ((1.0_pr-penal)*(u_loc(1:nwlt,n_var_index(ie)) - u_ex_loc(1:nwlt,i_exact))))
                L2err = DOT_PRODUCT ( &
                     (u_loc(1:nwlt,n_var_index(ie))-u_ex_loc(1:nwlt,i_exact))*(1.0_pr-penal), &
                     (u_loc(1:nwlt,n_var_index(ie))-u_ex_loc(1:nwlt,i_exact))*dA(1:nwlt) )
             ELSE
                Linferr = MAXVAL (ABS (u_loc(1:nwlt,n_var_index(ie)) - u_ex_loc(1:nwlt,i_exact)))
                L2err = DOT_PRODUCT ( &
                     u_loc(1:nwlt,n_var_index(ie))-u_ex_loc(1:nwlt,i_exact), &
                     (u_loc(1:nwlt,n_var_index(ie))-u_ex_loc(1:nwlt,i_exact))*dA(1:nwlt) )
             END IF

#ifdef MULTIPROC
             CALL parallel_global_sum (REALMAXVAL=Linferr)
             CALL parallel_global_sum (REAL=L2err)
#endif
             L2err = SQRT( L2err )/sumdA_global
             
             
             IF (par_rank.EQ.0) THEN
                IF (verb_) THEN
                   WRITE (*, '("ie=",i2," L2 error   = ",'//Err_FMT//'," scl = ",'//Err_FMT//')') &
                        n_var_index(ie), L2err , scl(ie)
                   WRITE (*, '(6x,"Linf error = ", '//Err_FMT//'/)') Linferr
                END IF
                WRITE(13,'(2(E12.5,1x) )',ADVANCE='NO') L2err , Linferr !write to log file
                WRITE(14,'(2(E20.10,1x) )',ADVANCE='NO') L2err , Linferr !HighPrec
             END IF
          END IF
       END DO
       
       
       
       
       i_exact = 0
       DO ie = 1,n_var
          i_exact = i_exact + 1
          IF( l_n_var_exact_soln(ie)  ) THEN
             
             ! now find relative error
             IF(imask_obstacle) THEN
                L2error_scale = SUM((1-penal)*u_ex_loc(:,n_var_index(ie))**2.0_pr*dA) !--L2 error 
                Linferror_scale = MAXVAL( (1-penal)*ABS(u_ex_loc(:,n_var_index(ie)))) !--Linf error
                
                relL2err = SUM( (1-penal)*(u_loc(:,n_var_index(ie))-u_ex_loc(:,i_exact))**2.0_pr*dA )
                relLinferr = MAXVAL( (1-penal)*ABS( u_loc(:,n_var_index(ie))-u_ex_loc(:,i_exact)))
             ELSE
                L2error_scale = SUM(u_ex_loc(:,n_var_index(ie))**2.0_pr*dA) !--L2 error 
                Linferror_scale = MAXVAL( ABS(u_ex_loc(:,n_var_index(ie)))) !--Linf error
                
                relL2err = SUM( (u_loc(:,n_var_index(ie))-u_ex_loc(:,i_exact))**2.0_pr*dA )
                relLinferr = MAXVAL( ABS( u_loc(:,n_var_index(ie))-u_ex_loc(:,i_exact)))
             END IF
#ifdef MULTIPROC
             CALL parallel_global_sum (REAL=L2error_scale)
             CALL parallel_global_sum (REAL=relL2err)
             CALL parallel_global_sum (REALMAXVAL=Linferror_scale)
             CALL parallel_global_sum (REALMAXVAL=relLinferr)
#endif
             
             IF( L2error_scale == 0.0_pr ) L2error_scale = 1.0_pr
             IF( Linferror_scale == 0.0_pr ) Linferror_scale = 1.0_pr
             
             relL2err = SQRT(relL2err/L2error_scale)
             relLinferr = relLinferr/Linferror_scale
             
             IF (verb_.AND.par_rank.EQ.0) THEN
                WRITE (6, '("ie=",i2," Rel. L2  error   = ", '//Err_FMT//'," scl = ",'//Err_FMT//')') &
                     n_var_index(ie), relL2err, scl(ie)
                
                WRITE (6, '(6x,"Rel. Linf error  = ", '//Err_FMT//'/)') relLinferr
             END IF
             
             
          END IF
       END DO
       
       
       IF (par_rank.EQ.0) THEN
          IF (verb_) THEN
             WRITE(13,'(" ")') !finish line
             WRITE(14,'(" ")') !finish line !HighPrec
             PRINT *,'===================================================='
          END IF
          
          CLOSE (13) !close log file
          CLOSE (14) !close log file !HighPrec
       END IF
       
       start = .FALSE.
       
    END IF
    
  END SUBROUTINE Check_Exact_Soln

  !
  ! Debugging function used to place a single point in wlt space. See call from Main()
  !
  !
  ! Debugging function used to place a single point in wlt space. See call from Main()
  !
  SUBROUTINE place_pt_wlt_space(j_local)
    USE precision
    USE pde
    USE variable_mapping   ! n_var
    USE sizes
    USE field
    USE wlt_vars                 ! j_lev
    USE wlt_trns_mod             ! update_db_from_u()
    USE wlt_trns_util_mod        ! get_indices_and_coordinates_by_types()
    IMPLICIT NONE
    INTEGER , INTENT(IN) :: j_local
    INTEGER coord(1:dim), i, nloc
    INTEGER, DIMENSION(1:nwlt) :: i_arr
    INTEGER, DIMENSION(1:nwlt,1:dim) :: indx_loc
    INTEGER, DIMENSION(1:nwlt) :: deriv_lev_loc 

    PRINT *,'Debugging, Place point =1 in wlt space'
    PRINT *,'coordinates relative to j_lev = ', j_lev
    PRINT *,'Input coordinate:'
    READ  *, coord

    PRINT *,'Placing Coordinate:', coord ,' = 1.0'

    CALL get_indices_and_coordinates_by_types ( (/0,2**dim-1/), (/0,3**dim-1/), (/1,j_lev/), j_lev, nloc, i_arr, indx_loc,deriv_lev_loc)

    DO i = 1, nloc
       IF( indx_loc(i,1) == coord(1) .AND. indx_loc(i,2) == coord(2) .AND. indx_loc(i,3) == coord(3) ) EXIT
    END DO
    IF( i == nloc +1 ) THEN
       PRINT *,'NOT FOUND '
       STOP
    ELSE
       PRINT *,'ADDING ', indx_loc(i,:)
    END IF

    u = 0.0_pr
    u(i_arr(i),:) = 100000000.0_pr

    CALL update_db_from_u(  u , nwlt , n_var, 1, 3 , 1)

    !IF ( TYPE_DB == DB_TYPE_WRK ) THEN
    !	u(i,:) = 1.0
    !  ELSE IF( TYPE_DB == DB_TYPE_LINES ) THEN
    !	db(coord(1) * 2**(j_local-j_lev), coord(2) * 2**(j_local-j_lev), coord(3) * 2**(j_local-j_lev) )%u(:) = 1.0_pr
    !    db(coord(1) * 2**(j_local-j_lev), coord(2) * 2**(j_local-j_lev), coord(3) * 2**(j_local-j_lev) )%flags = &
         !		IBSET( INT(0) , ACTIVE )
    !  END IF

  END SUBROUTINE place_pt_wlt_space

END MODULE default_mod
