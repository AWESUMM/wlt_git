!
! Variables that control debugging and timing
!

!
!#define PRINT_IGNORED_TIMERS ! show messages for ignored timers
!

#ifdef USE_DEBUG_TIMERS_S
#define USE_DEBUG_TIMERS
#endif


MODULE debug_vars
  
  INTEGER, PUBLIC :: verb_level      ! Set screen printing level:
  ! bit flag to print on the screen
  !  0 - nothing is printed except for errors, warnings, and .inp listing
  !  1 - output from I/O related subroutines is printed
  !
  !
  
  INTEGER, PUBLIC :: debug_level      ! Set debug level for print statements:
  ! it is integer in db_lines:
  !   0  - no debug statements
  !   1  - light debug statements, Things common to all dbs, solvers
  !   2  - db specific debug statements, solver specific debug statements
  ! 
  ! it is a bit flag in db_wrk/db_tree to print statistics:
  !   debug_level=1  - from MAIN                                                  ]   ]   ]
  !   debug_level=2  - from vector utilities and sgs_incompressible module        ] 3 ]   ]
  !   debug_level=4  - after each internal subroutine call from ADAPT_GRID            ] 7 ]
  !   debug_level=8  - inside DB specific subroutines (write_DB, significant_DB, etc)     ] 15
  
  INTEGER, PUBLIC :: debug_c_diff_fast
  !   0- nothing
  !   1- derivative MIN/MAXVALS are printed in c_diff_fast
  !   2- low level du calculations echoed
  
  LOGICAL, PUBLIC :: ignore_warnings           ! ignore non-fatal warnings (if TRUE, the code results might become erroneous, beware)
  LOGICAL, PUBLIC :: flag_timing               ! (db_lines) add extra timing calls
  LOGICAL, PUBLIC :: debug_zero_allocated_mem  ! (db_lines) if true, allocated memory is zeroed out, in some places
  LOGICAL, PUBLIC ::  debug_zero_db =.FALSE.   ! (db_lines) explicitly zero all db storage between use
  LOGICAL, PUBLIC :: debug_force_wrk_wlt_order ! (db_tree)  sort db_tree wavelets in db_wrk order (slow, periodic, use for debugging only)
  PUBLIC :: experimental_feature_stop, &       ! print experimental warning and terminate
       test_alloc,                     &       ! test allocation status
       debug_initialize                        ! initialize timers and echo debug status
  
  
  !  define USE_DEBUG_TIMERS to use the following
  !  timer related subroutines of that module
#ifdef USE_DEBUG_TIMERS
  INTEGER, PUBLIC, PARAMETER :: NUMBER_OF_TIMERS = 8           ! number of timers to print in log file, .LE. MAX_NUMBER_OF_TIMERS
  INTEGER, PRIVATE, PARAMETER :: MAX_NUMBER_OF_TIMERS = 8      ! actual maximum number of timers (the size of INFO array)
  REAL(4), PRIVATE :: timer(2,MAX_NUMBER_OF_TIMERS)            ! array of timers (starttime:stoptime/sum,:)
  LOGICAL, PRIVATE :: timer_is_running(MAX_NUMBER_OF_TIMERS)   ! current timer status
#endif
  
  
  PUBLIC ::                            &       ! timers:
       timer_start,                    &       !         start
       timer_stop,                     &       !         pause
       timer_val,                      &       !         display the value
       timer_reset,                    &       !           set timer to zero
       timer_reset_all,                &       !           set all timers to zero
       timer_header                            !           print info what timers do (e.g into _log file)


CONTAINS  
  !--------------------------------------------------------------------------------------------
  ! print info what timers do (e.g into _log file)
  FUNCTION timer_header( number_of_the_timer )
    INTEGER, INTENT(IN) :: number_of_the_timer
#ifdef USE_DEBUG_TIMERS
    
    CHARACTER (LEN=*), PARAMETER :: INFO(1:MAX_NUMBER_OF_TIMERS) = (/ &
         '!   timers:  1 - inputs and initializations   / reset after IC ==>  total time integration time, CUMULATIVE          ', &
         '!            2 - adapt to initial conditions  / reset after IC ==>  main time integration loop time for each timestep', &
         '!            3 - adapt_grid                   / reset after IC ==>  adapt_grid, CUMULATIVE                           ', &
         '!            4 - time advancement (time_adv_cn or kry_time_step) for each time step                                  ', &
         '!            5 - parallel_migrate             / reset after IC ==>  parallel_migrate, CUMULATIVE                     ', &
         '!            6 - parallel communicators       / reset after IC ==>  parallel communicators, CUMULATIVE               ', &
         '!            7 - wlt transform in main for each time step                                                            ', &
         '!            8 - calculate cfl for each time step                                                                    '  &
         /)
    CHARACTER (LEN=LEN(INFO(1))) :: timer_header
    
    IF (number_of_the_timer.LT.1.OR.number_of_the_timer.GT.MAX_NUMBER_OF_TIMERS) THEN
       timer_header = '' ! error value
       RETURN
    END IF
    timer_header = INFO(number_of_the_timer)

#else
    CHARACTER (LEN=1) :: timer_header
    timer_header = ''
#endif
  END FUNCTION timer_header
  !--------------------------------------------------------------------------------------------
  ! display timer value
  ! (display value of stoped timer or
  !  display the current value of running timer which will continue running)
  FUNCTION timer_val( number_of_the_timer )
    INTEGER, INTENT(IN) :: number_of_the_timer
    REAL(4) :: timer_val
#ifdef USE_DEBUG_TIMERS
    
    IF (number_of_the_timer.LT.1.OR.number_of_the_timer.GT.MAX_NUMBER_OF_TIMERS) THEN
       timer_val = -1.0 ! error value
       RETURN
    END IF
    IF (timer_is_running(number_of_the_timer)) THEN
       CALL CPU_TIME( timer_val )
       timer_val = timer_val - timer(1,number_of_the_timer)
    ELSE
       timer_val = timer(2,number_of_the_timer)
    END IF

#else    
    timer_val = -1.0 ! error value
#endif
  END FUNCTION timer_val
  !--------------------------------------------------------------------------------------------
  ! pause timer
  ! (will affect running timer only)
  SUBROUTINE timer_stop( number_of_the_timer )
    INTEGER, INTENT(IN) :: number_of_the_timer
    REAL(4) :: t
#ifdef USE_DEBUG_TIMERS
    
    CALL CPU_TIME( t )
    IF (number_of_the_timer.LT.1.OR.number_of_the_timer.GT.MAX_NUMBER_OF_TIMERS) THEN
#ifdef PRINT_IGNORED_TIMERS
       PRINT *, 'timer_stop: ignoring illegal timer number'
#endif
       RETURN
    END IF
    IF (.NOT.timer_is_running(number_of_the_timer)) THEN
#ifdef PRINT_IGNORED_TIMERS
       PRINT *, 'timer_stop: ignored for already stoped timer'
#endif
       RETURN
    END IF
    timer(2,number_of_the_timer) = timer(2,number_of_the_timer)  + (t - timer(1,number_of_the_timer))
    timer_is_running(number_of_the_timer) = .FALSE.
    
#endif
  END SUBROUTINE timer_stop
  !--------------------------------------------------------------------------------------------
  ! start timer
  ! (will not affect already running timer)
  SUBROUTINE timer_start( number_of_the_timer )
    INTEGER, INTENT(IN) :: number_of_the_timer
    REAL(4) :: t
#ifdef USE_DEBUG_TIMERS
    
    IF (number_of_the_timer.LT.1.OR.number_of_the_timer.GT.MAX_NUMBER_OF_TIMERS) THEN
#ifdef PRINT_IGNORED_TIMERS
       PRINT *, 'timer_start: ignoring illegal timer number'
#endif
       RETURN
    END IF
    IF (timer_is_running(number_of_the_timer)) THEN
#ifdef PRINT_IGNORED_TIMERS
       PRINT *, 'timer_start: ignored for already started timer'
#endif
       RETURN
    END IF
    CALL CPU_TIME( t )
    timer(1,number_of_the_timer) = t
    timer_is_running(number_of_the_timer) = .TRUE.
    
#endif
  END SUBROUTINE timer_start
  !--------------------------------------------------------------------------------------------
  ! set timer to zero
  ! (running timer will be stoped and set to zero)
  SUBROUTINE timer_reset( number_of_the_timer )
    INTEGER, INTENT(IN) :: number_of_the_timer
#ifdef USE_DEBUG_TIMERS
    
    IF (number_of_the_timer.LT.1.OR.number_of_the_timer.GT.MAX_NUMBER_OF_TIMERS) THEN
#ifdef PRINT_IGNORED_TIMERS
       PRINT *, 'timer_reset: ignoring illegal timer number'
#endif
       RETURN
    END IF
    timer(:,number_of_the_timer) = 0.0
    timer_is_running(number_of_the_timer) = .FALSE.
    
#endif
  END SUBROUTINE timer_reset
  !--------------------------------------------------------------------------------------------
  ! set all timers to zero
  ! (running timer will be stoped and set to zero)
  SUBROUTINE timer_reset_all
#ifdef USE_DEBUG_TIMERS
    
    timer = 0.0
    timer_is_running = .FALSE.
    
#endif
  END SUBROUTINE timer_reset_all
  !--------------------------------------------------------------------------------------------
  ! initialize timers and echo debug status
  SUBROUTINE debug_initialize( VERB )
    LOGICAL, INTENT(IN), OPTIONAL :: VERB
    LOGICAL :: do_verb
    
    do_verb = .FALSE.
    IF (PRESENT(VERB)) do_verb= VERB
    
    IF (do_verb) THEN
       ! echo debug status
       IF(debug_zero_db) WRITE(*,'(/,A,/)') " >>>>>>>>>>>>>>>debug_zero_db = TRUE <<<<<<<<<<<<<<<<<<<<"
    END IF
    
#ifdef USE_DEBUG_TIMERS
    ! initialize timers
    CALL timer_reset_all
    IF (do_verb) WRITE (*,'("Number of timers initialized: ",I3)') MAX_NUMBER_OF_TIMERS
#endif
    
  END SUBROUTINE debug_initialize
  
  !--------------------------------------------------------------------------------------------
  ! test allocation status
  SUBROUTINE test_alloc (status, string, length)
    INTEGER, INTENT(IN) :: status, length
    CHARACTER (LEN=*) string
    
    IF (status.NE.0) THEN
       WRITE (*,'(" ERROR: allocating ",A," of length ",I8)') string, length
       WRITE (*,'(" Terminating ...")')
       STOP 1
    END IF
    
  END SUBROUTINE test_alloc

  !--------------------------------------------------------------------------------------------
  ! print warning that the current feature is experimental
  !  and terminate 
  SUBROUTINE experimental_feature_stop (call_parent_name)
    CHARACTER (*), INTENT(IN) :: call_parent_name
    
    WRITE (*,'("---------------------- WARNING ----------------------")')
    WRITE (*,'("         The current feature is experimental         ")')
    WRITE (*,'("           and to be used at your own risk           ")')
    WRITE (*,'("-----------------------------------------------------")')
    WRITE (*,'(" Called from: ",A)') call_parent_name
    WRITE (*,'("")')
    WRITE (*,'(" Terminating ...")')
    STOP 1
    
  END SUBROUTINE experimental_feature_stop

  


END MODULE debug_vars
