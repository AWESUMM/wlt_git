MODULE wlt_trns_vars
  USE wlt_vars
  USE lines_database
  INTEGER ,PARAMETER :: TYPE_DB = DB_TYPE_LINES ! so main knows which db module is being used.

  !=========================== Public variables related to lines db tranforms =========================================
  INTEGER, DIMENSION (:), ALLOCATABLE :: lv,lvj
  INTEGER, PUBLIC :: j_mn_DB, j_mx_DB
!OLD  INTEGER, DIMENSION (:,:), ALLOCATABLE :: nlv,nlvj

!  INTEGER , DIMENSION(:,:) , ALLOCATABLE :: indx ! indx routine from indx (1-nwlt) to coordinate (1:dim)


  !=========================== TYPE structure for indx_DB ===============================================================
  TYPE, PUBLIC :: indx_list_DB
     INTEGER :: ixyz ! points to global contracted coordinate on wrk array
     INTEGER :: i    ! location in 1...nwlt index 
!UNUSED	 TYPE (grid_pt_ptr)  :: DBpointer ! pointer to actual grid node
	                 ! think how to add pointer to DB here and make it PRIVITE
  END TYPE indx_list_DB
  ! one cell of j_df x FACE x LEVEL x j matrix
  TYPE, PUBLIC :: indx_type_DB 
     TYPE ( indx_list_DB ), DIMENSION(:) , POINTER :: p
     INTEGER :: length                           ! used as a counter
     INTEGER :: real_length                      ! real size of the allocated array p(:)
     INTEGER :: shift                            ! used for finding coordinates of bounary points in V cycle, when j < j_lev_DB
  END TYPE indx_type_DB
  TYPE (indx_type_DB), ALLOCATABLE, DIMENSION(:,:,:,:), TARGET :: indx_DB
                                                          !indx_DB(j_df,wlt_type,face_type,j) j_df level of derivative
  TYPE ( indx_list_DB ) , ALLOCATABLE, DIMENSION(:) , TARGET   :: indx_list_DB_array ! main buffer for sub arrays pointed to by
                                                                                     ! p() in index_type_DB
  INTEGER , DIMENSION(:), ALLOCATABLE  :: face_type_map ! map order of faces as they will be put in flat u array

!example contracted indx to ixyz:          ixyz(1:dim) = INT(MOD(ii-1,i_p(1:dim))/i_p(0:dim-1))
END MODULE wlt_trns_vars
