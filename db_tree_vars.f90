!
! definitions of node of the tree and node pointers
!
MODULE db_tree_vars
  USE precision
#if (TREE_VERSION == 3)
  USE user_case_db
#endif
  IMPLICIT NONE
  
  !---------------------------------------------------------
  ! db_tree, db_tree1 ( C/C++ trees )
  !---------------------------------------------------------
#if ((TREE_VERSION == 0) || (TREE_VERSION == 1))
  ! C pointer to the node is passed to Fortran code as integer,
  ! so make sure that it has the correct size
  ! (integer*8 for most 64 bit systems, or integer*4 for most 32 bit systems)
#ifdef PTR_INTEGER8
  INTEGER*8, PARAMETER :: POINTER_TYPE = 0
#else
  INTEGER*4, PARAMETER :: POINTER_TYPE = 0
#endif
  INTEGER, PARAMETER, PUBLIC :: pointer_pr  = KIND(POINTER_TYPE)

  !---------------------------------------------------------
  ! db_treef ( Fortran 90 tree )
  !---------------------------------------------------------
#elif ((TREE_VERSION == 2) || (TREE_VERSION == 3))
  ! Fortran's native pointers are passed between the code and the database
  TYPE node_ptr
     TYPE(node), POINTER :: ptr
  END TYPE node_ptr
  
  TYPE node
#if (TREE_VERSION == 2)
     REAL(pr), POINTER :: val(:)        ! function values
     INTEGER, POINTER :: ival(:)        ! integer function values
     INTEGER, POINTER :: ii(:)          ! coordinates: ix, iy, iz
#else
     REAL(pr) :: val(n_var_db)          ! function values
     INTEGER :: ival(n_ivar_db)         ! integer function values
     INTEGER :: ii(max_dim)             ! coordinates: ix, iy, iz
#endif
     INTEGER :: id                           ! significance ID
     TYPE(node_ptr), POINTER :: leaves(:)       ! node's children
     TYPE(node), POINTER :: next_tlp, prev_tlp  ! type-level pointers
     INTEGER :: level                        ! internal level (root internal level is 0)
     INTEGER :: num_leaves                   ! maximum number of leaves
     INTEGER :: sig                          ! (1) sig_adj (2) gho list marker
  END TYPE node  

  !---------------------------------------------------------
  ! default
  !---------------------------------------------------------
#else
#error Please define correct TREE_VERSION value
#endif
  
END MODULE db_tree_vars
