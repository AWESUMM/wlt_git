!****************************************************************************************
!* Module for finding inverses, determinants, eigenvalues, and eigenvectors of a matrix *
!****************************************************************************************
MODULE matrixAlgebra  
 
   ! Uses module precision to normalize precision in each subroutine
   USE precision
   ! Subroutines to be used by programs using this module
   PUBLIC :: powerMethod,det,inverse,inversePowerMethod,&
   &QRDecomposition,householderQRDecomposition,getEigenvalues
   
   CONTAINS
   
   !-------------------------------------------------------------------------------------
   ! Uses diagonalization to find the determinant of a matrix
   !------------Variables------------
   ! A1 is the matrix being entered to find its determinant
   ! detA is the determinant of A and is returned by the subroutine
   ! n is the dimension of the matrix (n x n matrix)  
   FUNCTION det(A1,n)
      INTEGER :: n
      REAL(pr) :: A1(n,n)

      INTEGER :: x,y,i
      REAL(pr) :: A(n,n),B(n,n),det,tol

      tol = 1e-12
      A = A1
      B = A1
      
      ! Get the pivots and pivot the matrix values
      DO x=1,n
         DO y=x,n
            IF(ABS(A(x,y)) > ABS(B(x,y))) THEN
               DO i=1,n
                  B(y,i) = B(x,i)      
                  B(x,i) = A(y,i)
               END DO
               DO i=1,n   
                  A(y,i) = B(y,i)      
                  A(x,i) = B(x,i)
               END DO
            END IF
         END DO
      END DO
     
      ! Diagonalize the matrix
      DO x=1,n
         DO y=x,n
            DO i=1,n
               IF (x /= y) THEN
                  IF (ABS(B(x,x)) < tol) THEN
                     A(y,i) = 0.0
                  ELSE
                     A(y,i) = B(y,i) - B(x,i) * B(y,x) / B(x,x)
                  END IF
               END IF
            END DO
            DO i=1,n       
              B(y,i) = A(y,i)  
            END DO
         END DO
      END DO

      ! Get the determinant by multiplying the diagonals
      det = 1.0
      DO x=1,n
         det = det * A(x,x)
      END DO     
   END FUNCTION det
   
   !-------------------------------------------------------------------------------------
   ! Uses the power method to find the maximum eigenvalue
   !------------Variables------------
   ! A1 is the matrix being entered to find its maximum eigenvalue
   ! x1 is the guess for the eigenvector associated with the maximum eigenvalue
   ! A guess of x1 = 1 (for all indices) usually works well
   ! x1 is also the eigenvector associated with the maximum eigenvalue
   ! and x1 is returned by the powerMethod
   ! lambda is the maximum eigenvalue returned by powerMethod
   ! n is the dimension of the matrix (n x n matrix)
   SUBROUTINE powerMethod(A1,x1,lambda,n)
      INTEGER, INTENT(IN) :: n
      REAL(pr), INTENT(IN) :: A1(n,n)
      REAL(pr), INTENT(INOUT) :: x1(n,1)
      REAL(pr), INTENT(OUT) :: lambda(1,1)

      INTEGER :: x,count
      REAL(pr) :: A(n,n),lambdaOld(1,1),y1Norm,y1(n,1)
      REAL(pr) :: tol = 1e-12
      INTEGER :: countMax = 100
     
      A = A1
      
      count = 0
      lambda = 0.0
      lambdaOld(1,1) = lambda(1,1) + 1.0
      
      ! run until the eigenvalue converges
      DO WHILE (ABS(lambdaOld(1,1) - lambda(1,1)) > tol .AND. count < countMax)
         count = count + 1
         
         ! Get dummy vector
         y1 = MATMUL(A,x1)
   
         ! Get 2-Norm of dummy vector
         y1Norm = 0.0
         DO x=1,n
            y1Norm = y1Norm + y1(x,1)**2
         END DO
       
         ! Get approximate eigenvector
         IF (y1Norm < tol) THEN
            y1Norm = tol
         END IF
         x1 = y1 / SQRT(y1Norm)
       
         lambdaOld = lambda
         lambda = 0.0
       
         ! Get approximate eigenvalue
         lambda = MATMUL(TRANSPOSE(x1),MATMUL(A,x1))
      END DO
   END SUBROUTINE powerMethod

   !-------------------------------------------------------------------------------------
   ! Uses Gaussian Elimination with pivoting to determine the inverse of a matrix by
   ! transforming the matrix into Reduced Row Echelon Form (RREF)
   !------------Variables------------
   ! A is the matrix being entered to find its inverse
   ! invB is the inverse of A and is returned by the subroutine
   ! n is the dimension of the matrix (n x n matrix)    
   SUBROUTINE inverse(A1,invB,n)
      INTEGER, INTENT(IN) :: n
      REAL(pr), INTENT(IN) :: A1(n,n)
      REAL(pr), INTENT(OUT) :: invB(n,n)
   
      INTEGER :: x,y,i    
      REAL(pr) ::  B(n,n),invA(n,n),A(n,n)

      invA = 0.0
      invB = 0.0 
      A = A1

      B = A
      DO x=1,n
         invA(x,x) = 1.0
         invB(x,x) = 1.0
      END DO
     
      ! Get the pivots and pivot the matrix values
      DO x=1,n
         DO y=x,n
            IF(ABS(A(x,y)) > ABS(B(x,y))) THEN
               DO i=1,n
                  B(y,i) = B(x,i)
                  invB(y,i) = invB(x,i)
                  B(x,i) = A(y,i)
                  invB(x,i) = invA(y,i)
               END DO
               DO i=1,n   
                  A(y,i) = B(y,i)      
                  invA(y,i) = invB(y,i)
                  A(x,i) = B(x,i)
                  invA(x,i) = invB(x,i)
               END DO
            END IF
         END DO
      END DO
     
      ! Use RREF to find the inverse of the matrix
      DO x=1,n
         DO y=1,n
            DO i=1,n
               IF (x == y) THEN
                  A(y,i) = B(y,i) / B(y,y)
                  invA(y,i) = invB(y,i) / B(y,y)
               END IF
               IF (x /= y) THEN
                  A(y,i) = B(y,i) - B(x,i)* B(y,x) / B(x,x)
                  invA(y,i) = invB(y,i) - invB(x,i) * B(y,x) / B(x,x)
               END IF
            END DO
            DO i=1,n
               B(y,i) = A(y,i)
               invB(y,i) = invA(y,i)
            END DO
         END DO
      END DO
   END SUBROUTINE inverse 
   
   !-------------------------------------------------------------------------------------
   ! Uses the power method to find the eigenvalue closest to the guessed eigenvalue
   !------------Variables------------
   ! A1 is the matrix being entered to find its eigenvalue closest to 
   ! the guessed eigenvalue
   ! x1 is the guess for the eigenvector associated with the guessed eigenvalue
   ! A guess of x1 = 1 (for all indices) usually works well
   ! x1 is also the eigenvector associated with the eigenvalue
   ! and x1 is returned by the powerMethod
   ! lambda is the eigenvalue closest to the guessed eigenvalue and
   ! is returned by powerMethod
   ! lambdaGuess is the guessed eigenvalue (often called the shift)
   ! n is the dimension of the matrix (n x n matrix) 
   SUBROUTINE inversePowerMethod(A1,x1,lambda,lambdaGuess,n)
      INTEGER, INTENT(IN) :: n
      REAL(pr), INTENT(IN) :: A1(n,n),lambdaGuess
      REAL(pr), INTENT(OUT) :: lambda(1,1)
      REAL(pr), INTENT(INOUT) :: x1(n,1)

      INTEGER :: x,count
      REAL(pr) :: y1(n,1),lambdaOld(1,1),A(n,n),B(n,n),shift,S(n,n)
      REAL(pr) :: invB(n,n),y1Norm
      REAL(pr) :: tol = 1e-12
      INTEGER :: countMax = 100

      A = A1

      ! Create Shift matrix S
      S = 0.0
      shift = lambdaGuess
      DO x=1,n
         S(x,x) = shift
      END DO

      ! Get inverse of matrix minus shift
      B = A - S
      CALL inverse(B,invB,n)

      count = 0
      lambda = 0.0
      lambdaOld(1,1) = lambda(1,1) + 1.0
      ! Run until the eigenvalue converges
      DO WHILE(ABS(lambdaOld(1,1)-lambda(1,1))>tol .AND. count < countMax)
         count = count + 1
         ! Zero dummy vector
         y1 = 0

         ! Get dummy vector
         y1 = MATMUL(invB,x1)
           
         ! Get 2-Norm of dummy vector
         y1Norm = 0.0
         DO x=1,n
            y1Norm = y1Norm + y1(x,1)**2
         END DO

         ! Get approximate eigenvector
         x1 = y1 / SQRT(y1Norm)
           
         lambdaOld = lambda
         lambda = shift   

         !Get approximate eigenvalue
         lambda = MATMUL(TRANSPOSE(x1),MATMUL(invB,x1))
      END DO      
   END SUBROUTINE inversePowerMethod
   
   !-------------------------------------------------------------------------------------
   ! Obtains the Q-R decomposition of a matrix
   !------------Variables------------
   ! Q is the Q matrix in the Q-R decomposition and is returned by the subroutine
   ! R is the R matrix in the Q-R decomposition and is returned by the subroutine
   ! A1 is the matrix being entered to find its Q-R decomposition
   ! n is the dimension of the matrix (n x n matrix) 
   SUBROUTINE QRDecomposition(Q,R,A1,n)
      INTEGER, INTENT(IN) :: n
      REAL(pr), INTENT(OUT) :: Q(n,n),R(n,n)
      REAL(pr), INTENT(IN) :: A1(n,n)

      INTEGER :: x,y,i,j
      REAL(pr) :: u(n,n),A(n,n),projSum(n,n),dotSum,uNorm,rowSum

      A = A1

      ! Getting Q
      DO x=1,n
         DO y=1,n
            DO i=1,x-1
               dotSum = 0.0;
               DO j=1,n
                  dotSum = dotSum + Q(j,i) * A(j,x)
               END DO
               DO j=1,n
                  projSum(j,i) = dotSum * Q(j,i)
               END DO
            END DO
            uNorm = 0.0
            DO i=1,n
               rowSum = 0.0
               DO j=1,x-1
                  rowSum = rowSum + projSum(i,j)
               END DO
               u(i,x) = A(i,x) - rowSum
            END DO
            DO i=1,n
               uNorm = uNorm + u(i,x)**2
            END DO
            Q(y,x) = u(y,x) / SQRT(uNorm)
         END DO
      END DO

      ! Getting R
      R = 0.0
!$$$$$$       R = MATMUL(Q,A)
      DO x=1,n
         DO y=x,n
            dotSum = 0.0
            DO i=1,n
               dotSum = dotSum + Q(i,x) * A(i,y)
            END DO
            R(x,y) = dotSum
         END DO
      END DO       
   END SUBROUTINE QRDecomposition   
   
   !-------------------------------------------------------------------------------------
   ! Obtains the Q-R decomposition of a matrix using householder reflections
   !------------Variables------------
   ! Q is the Q matrix in the Q-R decomposition and is returned by the subroutine
   ! R is the R matrix in the Q-R decomposition and is returned by the subroutine
   ! A1 is the matrix being entered to find its Q-R decomposition
   ! n is the dimension of the matrix (n x n matrix) 
   SUBROUTINE householderQRDecomposition(Q,R,A1,n)
      INTEGER, INTENT(IN) :: n
      REAL(pr), INTENT(OUT) :: Q(n,n),R(n,n)
      REAL(pr), INTENT(IN) :: A1(n,n)

      INTEGER :: x,y,i,j
      REAL(pr) :: u(n,n),A(n,n),Q1(n,n),Q2(n,n)
      REAL(pr) :: ANorm, uNorm,uiAiy(n,1),Q1yiui(n,1),uiQ1iy(n,1)

      A = A1
      u = 0.0
      R = 0.0
      Q2 = 0.0
      
      DO x=1,n
         Q2(x,x) = 1.0
      END DO

      DO x=1,n
         Q = 0.0
         Q1 = 0.0
         DO y=1,n
            Q1(y,y) = 1.0
         END DO
         
         ! house(A)
         ANorm = 0.0
         uNorm = 0.0
         DO y=x,n
            ANorm = ANorm + A(y,x)**2
         END DO
         ANorm = SQRT(ANorm)
         DO y=x,n
            IF (x==y) THEN
               u(y,x) = A(y,x) + A(y,x) / ABS(A(y,x)) * ANorm
            ELSE
               u(y,x) = A(y,x)
            END IF
            uNorm = uNorm + u(y,x)**2
         END DO

         ! Get Q
         DO y=x,n
            uiAiy(y,1) = 0.0
            Q1yiui(y,1) = 0.0
            uiQ1iy(y,1) = 0.0
            DO i=x,n
               Q1yiui(y,1) = Q1yiui(y,1) + u(i,x) * Q1(y,i)
               uiAiy(y,1) = uiAiy(y,1) + u(i,x) * A(i,y)
               uiQ1iy(y,1) = uiQ1iy(y,1) + u(i,x) * Q1(i,y)
            END DO
         END DO
         DO y=x,n
            DO i=x,n
               A(y,i) = A(y,i) - 2 / uNorm * u(y,x) * uiAiy(i,1)
               Q1(y,i) = Q1(y,i) - 2 / uNorm * u(y,x) * uiQ1iy(i,1)
            END DO
         END DO
         DO y=1,n
            DO i=1,n
               DO j=1,n
                  Q(y,i) = Q(y,i) + Q2(y,j) * Q1(j,i)
               END DO
            END DO
         END DO
         Q2 = Q
      END DO 

      ! Get R
      R = A
   END SUBROUTINE householderQRDecomposition

   !-------------------------------------------------------------------------------------
   ! Uses the Q-R Decomposition to determine all eigenvalues of matrix A1
   !------------Variables------------
   ! lam is a vector containing all eigenvalues of A and is returned by the subroutine
   ! A1 is the matrix being entered to find its eigenvalues
   ! n is the dimension of the matrix (n x n matrix) 
   SUBROUTINE getEigenvalues(lam,A1,n)
      INTEGER, INTENT(IN) :: n
      REAL(pr), INTENT(IN) :: A1(n,n)
      REAL(pr), INTENT(OUT) :: lam(n,1)
      
      INTEGER :: x,y,i,count
      REAL(pr) :: A(n,n),Q(n,n),R(n,n),store
      REAL(pr) :: tol = 1e-12
      INTEGER :: countMax = 100
      
      A = A1

      store = A(1,1) + 1.0
      count = 0
      DO WHILE (ABS(store-A(1,1)) > tol .AND. count < countMax)
         count = count + 1
         store = A(1,1)
         CALL householderQRDecomposition(Q,R,A,n)
         A = 0.0
         DO x=1,n
            DO y=1,n 
               DO i=1,n
                  A(x,y) = A(x,y) + R(x,i) * Q(i,y)
               END DO
            END DO
         END DO
      END DO

      DO x=1,n
         lam(x,1) = A(x,x)
      END DO
   END SUBROUTINE getEigenvalues

   !-------------------------------------------------------------------------------------
   ! Uses inversePowerMethod to determine all eigenvectors of a matrix
   !------------Variables------------
   ! A is the matrix being entered to find its eigenvalues
   ! lam is a vector containing all eigenvalues of A
   ! xVecs is a matrix storing all eigenvectors of A
   ! n is the dimension of the matrix (n x n matrix) 
   SUBROUTINE getEigenVectors(A,lam,xVecs,n)
      INTEGER, INTENT(IN) :: n
      REAL(pr), INTENT(IN) :: A(n,n),lam(n,1)
      REAL(pr),INTENT(OUT) :: xVecs(n,n)
      
      INTEGER :: x
      REAL(pr) :: lamDummy(1,1)
      
      xVecs = 1.0
      DO x=1,n
         CALL inversePowerMethod(A,xVecs(:,x:x),lamDummy,lam(x,1),n)
      END DO
   END SUBROUTINE getEigenVectors
END MODULE matrixAlgebra
!***********************************************************************************************
!* End of Module for finding inverses, determinants, eigenvalues, and eigenvectors of a matrix *
!***********************************************************************************************


! Module for finding geometry of object and generating points outside object
! ---2011-06-29 Modifications---
! Changed resizeLineData columns from 14 to 6
! Changed check for isectRevolveLine intersection from within bounding box to
!    projected circle method
! Changed tolerance for imaginary numbers for isectSpline to 1e-5, the same as tol2
!    so deleted tol2 and replaced with tol = 1e-5
! Changed dotNormRay in isectPlane to not divide by zero when dotNormRay was zero
!    when determining where the ray intersects a plane from an origin point
! ---2011-06-30 Modifications---
! Changed tol2 in isectRevolvedLine to 0.5 because if the cross product
!    creates a normal direction in the opposite direction to the axis
!    of rotation, the magnitude of their difference will be close to
!    2.0, so a number as high as 1.5 would probably still work for the check
! Changed tol in isectCircle to 0.5 because if the cross product
!    creates a normal direction in the opposite direction to the axis
!    of rotation, the magnitude of their difference will be close to
!    2.0, so a number as high as 1.5 would probably still work for the check
! Changed isectRevolvedLine so the point on the line being rotated corresponds
!    to where the ray from the point projected onto the plane being rotated intersects
!    the rotated line on that plane
! Changed isectNURBS tol2 to 5e-3
! Changed isectNURBS to check oldQ instead of oldsguess and oldtguess
! ---2011-07-01 Modifications---
! Changed isectRevolvedLine so it only has one point on the line where the projected
!    ray intersects the line being rotated; s1, s2, and lp2 logic was removed
! Changed Broyden so it checks more s-values between s1 and s2 and t-values between
!    t1 and t2 as it iterates (may be slower)
! ---2011-07-03 Modifications---
! Changed isectRevolvedLine so it uses two points on the line again, but this time
!    the points correspond to where the point projected onto the plane of rotation
!    and its ray in the direction perpendicular to the axis of rotation intersects
!    the line being rotated. The same method is used for the point plus
!    the ray projected onto the plane of rotation.
! Changed Broyden's and Newton's methods to use the maximum and minimum values
!    for the overall set of knot sequences
! ---2011-07-05 Modifications---
! Changed Broyden and Newton's method to check if s and t are near the overall
!    maximum and minimum values for the overall set of knot sequences, and if
!    they are, set them equal to the end points of the overall sequences
! Changed Broyden and Newton's method to reguess s and t if they are outside
!    the range of knot sequences and do so starting from opposite sides of
!    the s and t ranges
! Removed revolvedLineSplines, revolvedLineSplineBreaks, revolvedCircleSplines
!    and revolvedCircleSplineBreaks from the program
! Set dataLines, parLines, and table to NULL() (dataLines(:) => NULL())
! ---2011-07-19 Modifications---
! Changed Broyden and Newton's method so if s and t are near the overall
!    maximum and minimum values for the overall set of knot sequences
!    they are offset from the maximum or minimum to be within the range
!    of those values but not equal to them (V(1) + tol or V(2) - tol)
! Added a global surface data entry to determine whether the surface
!    already has two dimensional data associated with it
! Changed bSplineSurface to reflect the check for whether the surface
!    already has two dimensional data associated with it
MODULE threedobject

   ! Uses module precision to normalize precision in each subroutine
   USE precision
   USE matrixAlgebra
   IMPLICIT NONE
   PRIVATE
   CHARACTER*1, PARAMETER :: slash = '/'                               ! folder organization
   TYPE, PRIVATE :: line
      CHARACTER*150 :: l                                               ! single line representation
   END TYPE line
   TYPE(line), PRIVATE, POINTER :: dataLines(:) => NULL()              ! array of data lines
   TYPE(line), PRIVATE, POINTER :: parLines(:) => NULL()               ! array of parameter lines
   INTEGER, PRIVATE, POINTER :: table(:) => NULL()                     ! hash table for parameter number
   !---------------------Revolved Line Data---------------------
   REAL(pr), PRIVATE, ALLOCATABLE :: revolvedLines(:,:)                ! stores revolved line data
   REAL(pr), PRIVATE, ALLOCATABLE :: revolvedLineAxes(:,:)             ! stores each revolved line's axis of rotation
   REAL(pr), PRIVATE, ALLOCATABLE :: revolvedLineRadial(:,:)           ! stores radial direction from axis of rotation
   REAL(pr), PRIVATE, ALLOCATABLE :: revolvedLineAngles(:,:)           ! stores start and end angles for each line's rotation
   INTEGER, PRIVATE, ALLOCATABLE :: revolvedLineNCircles(:,:)          ! stores the starting index and number of circles for each revolved line
   REAL(pr), PRIVATE, ALLOCATABLE :: revolvedLineCircles(:,:)          ! stores circle data for each 2D circle on revolved lines
   INTEGER, PRIVATE, ALLOCATABLE :: revolvedLineNLines(:,:)            ! stores the starting index and number of lines for each revolved line
   REAL(pr), PRIVATE, ALLOCATABLE :: revolvedLineLines(:,:)            ! stores line data for each 2D line on revolved lines
   INTEGER, PRIVATE, ALLOCATABLE :: revolvedLineNSplines(:,:)          ! stores the starting index and number of splines for each revolved line
   REAL(pr), PRIVATE, ALLOCATABLE :: revolvedLineExtrema(:,:)          ! stores the bounding box of the revolved surface
   INTEGER, PRIVATE :: revolvedLinesIndex = 0                          ! tracks how many revolved lines exist
   INTEGER, PRIVATE :: revolvedLineCirclesIndex = 0                    ! tracks how many 2D circle curves exist on revolved lines
   INTEGER, PRIVATE :: revolvedLineLinesIndex = 0                      ! tracks how many 2D line curves exist on revolved lines
   INTEGER, PRIVATE :: revolvedLineSplinesIndex = 0                    ! tracks how many 2D spline curves exist on revolved lines
   INTEGER, PRIVATE :: splineLength = 10                               ! the length will be determined by the maximum index of knots,weights,
                                                                       !    and points
   !---------------------Revolved Circle Data---------------------
   REAL(pr), PRIVATE, ALLOCATABLE :: revolvedCircles(:,:)              ! stores revolved circle data
   REAL(pr), PRIVATE, ALLOCATABLE :: revolvedCircleAxes(:,:)           ! stores each revolved circle's axis of rotation
   REAL(pr), PRIVATE, ALLOCATABLE :: revolvedCircleRadial(:,:)         ! stores radial direction from axis of rotation
   REAL(pr), PRIVATE, ALLOCATABLE :: revolvedCircleAngles(:,:)         ! stores start and end angles for each circle's rotation
   INTEGER, PRIVATE, ALLOCATABLE :: revolvedCircleNCircles(:,:)        ! stores the starting index and number of circles for each revolved circle
   REAL(pr), PRIVATE, ALLOCATABLE :: revolvedCircleCircles(:,:)        ! stores circle data for each 2D circle on revolved circles
   INTEGER, PRIVATE, ALLOCATABLE :: revolvedCircleNLines(:,:)          ! stores the starting index and number of lines for each revolved circle
   REAL(pr), PRIVATE, ALLOCATABLE :: revolvedCircleLines(:,:)          ! stores line data for each 2D line on revolved circles
   INTEGER, PRIVATE, ALLOCATABLE :: revolvedCircleNSplines(:,:)        ! stores the starting index and number of splines for each revolved circle
   REAL(pr), PRIVATE, ALLOCATABLE :: revolvedCircleExtrema(:,:)        ! stores the bounding box of the revolved surface
   INTEGER, PRIVATE :: revolvedCirclesIndex = 0                        ! tracks how many revolved circles exist
   INTEGER, PRIVATE :: revolvedCircleCirclesIndex = 0                  ! tracks how many 2D circle curves exist on revolved circles
   INTEGER, PRIVATE :: revolvedCircleLinesIndex = 0                    ! tracks how many 2D line curves exist on revolved circles
   INTEGER, PRIVATE :: revolvedCircleSplinesIndex = 0                  ! tracks how many 2D spline curves exist on revolved circles
   !--------------------B-Spline Surface Data--------------------
   REAL(pr), PRIVATE, ALLOCATABLE :: NURBSKnot1(:,:)                   ! stores the first knot sequence for each surface
   REAL(pr), PRIVATE, ALLOCATABLE :: NURBSKnot2(:,:)                   ! stores the second knot sequence for each surface
   REAL(pr), PRIVATE, ALLOCATABLE :: NURBSWeights(:,:)                 ! stores the weights for each surface
   REAL(pr), PRIVATE, ALLOCATABLE :: NURBSPoints(:,:)                  ! stores the control points for each surface
   INTEGER, PRIVATE, ALLOCATABLE :: NURBSIndices(:,:)                  ! stores the number of knots in each sequence, the number of weights,
                                                                       !    and the number of points
   REAL(pr), PRIVATE, ALLOCATABLE :: NURBSU(:,:)                       ! stores the maximum and minimum of the first knot sequence
   REAL(pr), PRIVATE, ALLOCATABLE :: NURBSV(:,:)                       ! stores the maximum and minimum of the second knot sequence
   INTEGER, PRIVATE :: NURBSIndex = 0                                  ! tracks how many B-Spline surfaces exist
   INTEGER, PRIVATE, ALLOCATABLE :: NURBSNCircles(:,:)                 ! stores the starting index and number of circles for each surface
   REAL(pr), PRIVATE, ALLOCATABLE :: NURBSCircles(:,:)                 ! stores circle data for each 2D circle on each surface
   INTEGER, PRIVATE, ALLOCATABLE :: NURBSNLines(:,:)                   ! stores the starting index and number of lines for each surface
   REAL(pr), PRIVATE, ALLOCATABLE :: NURBSLines(:,:)                   ! stores line data for each 2D line on each surface
   INTEGER, PRIVATE, ALLOCATABLE :: NURBSNSplines(:,:)                 ! stores the starting index and number of splines for each surface
   REAL(pr), PRIVATE, ALLOCATABLE :: NURBSSplines(:,:)                 ! stores spline data for each 2D spline on each surface
   INTEGER, PRIVATE, ALLOCATABLE :: NURBSSplinesBreaks(:,:)            ! stores the number of sets of coefficients for the cubic spline
   REAL(pr), PRIVATE, ALLOCATABLE :: NURBSPatch(:,:)                   ! s and t extrema for each B-Spline surface polynomial patch
   REAL(pr), PRIVATE, ALLOCATABLE :: NURBSPExtrema(:,:)                ! x,y,z extrema for each B-Spline surface polynomial patch
   REAL(pr), PRIVATE, ALLOCATABLE :: NURBSExtrema(:,:)                 ! stores the bounding box of the B-Spline surface
   INTEGER, PRIVATE, ALLOCATABLE :: eIndex(:)                          ! index of number of patches in flattened spline surface
   INTEGER, PRIVATE :: NURBSCirclesIndex = 0                           ! tracks how many 2D circle curves exist on the surface
   INTEGER, PRIVATE :: NURBSLinesIndex = 0                             ! tracks how many 2D line curves exist on the surface
   INTEGER, PRIVATE :: NURBSSplinesIndex = 0                           ! tracks how many 2D spline curves exist on the surface
   INTEGER, PRIVATE :: NURBSLength = 10                                ! the length will be determined by the maximum index of knots,weights,
                                                                       !    and points
   !------------------------2D Curve Data------------------------
   REAL(pr), PRIVATE, ALLOCATABLE :: circleData(:,:)                   ! stores data for 2D circle curve
   REAL(pr), PRIVATE, ALLOCATABLE :: lineData(:,:)                     ! stores data for 2D line curve
   REAL(pr), PRIVATE, ALLOCATABLE :: splineData(:,:)                   ! stores data for 2D spline curve
   INTEGER, PRIVATE, ALLOCATABLE :: splineBreakData(:,:)               ! stores number of knot sequence changes for B-Spline curves

   !-------------------------Bounding Box-------------------------
   REAL(pr), PRIVATE :: extrema(3,2)                                   ! gets the bounding box for the entire set of surfaces
   !-----------------------intersections-------------------------
   INTEGER, PRIVATE :: isect(6)                                        ! isect(1) corresponds to the ray in the +x-direction
                                                                       ! isect(2) corresponds to the ray in the -x-direction
                                                                       ! isect(3) corresponds to the ray in the +y-direction
                                                                       ! isect(4) corresponds to the ray in the -y-direction
                                                                       ! isect(5) corresponds to the ray in the +z-direction
                                                                       ! isect(6) corresponds to the ray in the -z-direction
   INTEGER, PRIVATE :: gSurfaceDE = 0                                  ! checks to see if the current surface data entry
                                                                       !    already has 2D curve data associated with it
!$$$$$$    INTEGER, PRIVATE :: tieMin = 2, tieMax = 4                  ! recommended values
   INTEGER, PRIVATE :: tieMin = 2, tieMax = 4                          ! any intersection value between tieMin and tieMax
                                                                       !    gives conflicting information about whether the
                                                                       !    point is inside or outside the surface
!$$$$$$    INTEGER, PRIVATE :: minInside = 4                           ! recommended value
   INTEGER, PRIVATE :: minInside = 4                                   ! the minimum value for a point to be considered inside
                                                                       !    the surface

   ! Determine whether to print statements to show the program is running properly
   LOGICAL, PRIVATE :: analyzing = .FALSE., analyzing2 = .TRUE., myWrtFlag = .FALSE.

   ! Allocated array sizes for IGS file storage
   INTEGER, PRIVATE :: dataLinesSize, parLinesSize

   ! finds what the maximum iterations for Newton to converge is (Debugging purposes only)
   INTEGER, PRIVATE :: maxIterGlobal = 0

   ! value for flattening the surface to make root finding more accurate
   !    higher value makes the program slower (recommend at least a value
   !    of 1)
   INTEGER, PRIVATE :: flatten = 3

  LOGICAL  , DIMENSION (:,:),     ALLOCATABLE :: i_in ! 1 - (T/F) - (in/out), 2 - (T/F) - (old/new)


   ! Subroutines to be used by programs using this module
   PUBLIC :: surface, initgeom, clean_the_memory, tester, CADtest, i_in

   ! The module contains the following subroutines
   CONTAINS

   !-------------------------------------------------------------------------------------
   ! gets the magnitude of a vector
   !------------Variables------------
   ! vector is the vector passed in
   ! mag is the magnitude of the vector returned by the function
   FUNCTION mag(vector)

      REAL(pr), INTENT(IN) :: vector(3,1)
      REAL(pr) :: mag

      INTEGER :: i

      mag = 0
      DO i=1,3
         mag = mag + vector(i,1)**2
      END DO
      mag = SQRT(mag)
      RETURN
   END FUNCTION mag

   !-------------------------------------------------------------------------------------
   ! gets the cross product vector from two vectors
   !------------Variables------------
   ! vec1 is the first vector in the cross product
   ! vec2 is the second vector in the cross product
   ! crossProduct is the resultant vector from the cross product
   FUNCTION crossProduct(vec1,vec2)

      REAL(pr), INTENT(IN) :: vec1(3,1),vec2(3,1)
      REAL(pr) :: crossProduct(3,1)

      crossProduct(1,1) = vec1(2,1) * vec2(3,1) - vec1(3,1) * vec2(2,1)
      crossProduct(2,1) = vec1(3,1) * vec2(1,1) - vec1(1,1) * vec2(3,1)
      crossProduct(3,1) = vec1(1,1) * vec2(2,1) - vec1(2,1) * vec2(1,1)
      RETURN
   END FUNCTION crossProduct

   ! find the point on the axis of rotation corresponding to a point in 3D space (x,y,z)
   !    the vector from the axis to the point is perpendicular to the axis
   !------------Variables------------
   ! x is the point in 3D space
   ! aL is the axis line
   ! getAxisT is the parameter value of the axis corresponding to the point in 3D space
   FUNCTION getAxisT(x,aL)
      REAL(pr), INTENT(IN) :: x(3,1),aL(3,2)
      REAL(pr) :: getAxisT

      REAL(pr) :: num,den
      ! DOT_PRODUCT(x - (axisLine(:,2) + t * (axisLine(:,1) - axisLine(:,2))),axisLine(:,1) - axisLine(:,2)) == 0
      num = DOT_PRODUCT(x(1:3,1) - aL(1:3,2),aL(1:3,1) - aL(1:3,2))
      den = DOT_PRODUCT(aL(1:3,1) - aL(1:3,2),aL(1:3,1) - aL(1:3,2))
      getAxisT = num / den
      RETURN
   END FUNCTION getAxisT

   !-------------------------------------------------------------------------------------
   ! gets the x,y,z (NURBS(1,1),NURBS(2,1),NURBS(3,1)) from the nonuniform B-Spline
   !    polynomial surface
   !------------Variables------------
   ! index is the index of the NURBS data
   ! s is the first parameter for the polynomial surface
   ! t is the second parameter for the polynomial surface
   FUNCTION NURBS(index,s,t)
      INTEGER, INTENT(IN) :: index
      REAL(pr), INTENT(IN) :: s,t
      REAL(pr) :: NURBS(3,1)

      REAL(pr), ALLOCATABLE :: si(:),ti(:),w(:),P(:,:),Bi(:),Bj(:)
      REAL(pr) :: num1,den1,num2,den2,tol,numVec(3,1),den
      INTEGER :: K1,K2,M1,M2,N1,N2,A,B,C,i,j

      ! Initialize variables
      NURBS = 0.0
      K1 = NURBSIndices(index,1)
      K2 = NURBSIndices(index,2)
      M1 = NURBSIndices(index,3)
      M2 = NURBSIndices(index,4)
      N1 = 1 + K1 - M1
      N2 = 1 + K2 - M2
      ! A number of knots for first knot sequence
      A = N1 + 2 * M1 + 1
      ! B number of knots for second knot sequence
      B = N2 + 2 * M2 + 1
      ! C weights, C control points
      C = (1 + K1) * (1 + K2)
      tol = 1e-12
      ! Allocate and assign knot sequences, weights, and control points
      !    as well as basis functions
      ALLOCATE(si(A),ti(B),w(C),P(3,C),Bi(A),Bj(B))
      si = NURBSKnot1(index,1:A)
      ti = NURBSKnot2(index,1:B)
      w = NURBSWeights(index,1:C)
      P = NURBSPoints((index-1)*3 + 1:(index-1)*3 + 3,1:C)
      ! Cox-de Boor formulas (see IGES documentation appendix B)
      DO j=0,M1
         DO i=0,K1
            IF (j == 0) THEN
               IF (si(i+1) <= s .AND. s <= si(i+2)) THEN
                  Bi(i+1) = 1.0
               ELSE
                  Bi(i+1) = 0.0
               END IF
               IF (si(i+2) <= s .AND. s <= si(i+3)) THEN
                  Bi(i+2) = 1.0
               ELSE
                  Bi(i+2) = 0.0
               END IF
            ELSE
               num1 = (s - si(i+1)) * Bi(i+1)
               den1 = si(i+1+j) - si(i+1)
               num2 = (si(i+2+j) - s) * Bi(i+2)
               den2 = si(i+2+j) - si(i+2)
               IF (ABS(den1) < tol .AND. ABS(den2) < tol) THEN
                  Bi(i+1) = 0.0
               ELSE IF (ABS(den1) < tol) THEN
                  Bi(i+1) = num2 / den2
               ELSE IF (ABS(den2) < tol) THEN
                  Bi(i+1) = num1 / den1
               ELSE
                  Bi(i+1) = num1 / den1 + num2 / den2
               END IF
            END IF
         END DO
      END DO
      DO j=0,M2
         DO i=0,K2
            IF (j == 0) THEN
               IF (ti(i+1) <= t .AND. t <= ti(i+2)) THEN
                  Bj(i+1) = 1.0
               ELSE
                  Bj(i+1) = 0.0
               END IF
               IF (ti(i+2) <= t .AND. t <= ti(i+3)) THEN
                  Bj(i+2) = 1.0
               ELSE
                  Bj(i+2) = 0.0
               END IF
            ELSE
               num1 = (t - ti(i+1)) * Bj(i+1)
               den1 = ti(i+1+j) - ti(i+1)
               num2 = (ti(i+2+j) - t) * Bj(i+2)
               den2 = ti(i+2+j) - ti(i+2)
               IF (ABS(num1) < tol .AND. ABS(den1) < tol &
               &.AND. ABS(num2) < tol .AND. ABS(den2) < tol) THEN
                  Bj(i+1) = 0.0
               ELSE IF (ABS(num1) < tol .AND. ABS(den1) < tol) THEN
                  IF (ABS(den2) < tol) THEN
                     den2 = tol
                     num2 = 0.0
                  END IF
                  Bj(i+1) = num2 / den2
               ELSE IF (ABS(num2) < tol .AND. ABS(den2) < tol) THEN
                  IF (ABS(den1) < tol) THEN
                     den1 = tol
                     num1 = 0.0
                  END IF
                  Bj(i+1) = num1 / den1
               ELSE
                  IF (ABS(den1) < tol) THEN
                     den1 = tol
                     num1 = 0.0
                  END IF
                  IF (ABS(den2) < tol) THEN
                     den2 = tol
                     num2 = 0.0
                  END IF
                  Bj(i+1) = num1 / den1 + num2 / den2
               END IF
            END IF
         END DO
      END DO
      numVec = 0.0
      den = 0.0
      DO j=0,K2
         DO i=0,K1
            numVec = numVec + w(j*(K1+1)+i+1) * Bi(i+1) * Bj(j+1) * &
                    &P(1:3,j*(K1+1)+i+1:j*(K1+1)+i+1)
            den = den + w(j*(K1+1)+i+1) * Bi(i+1) * Bj(j+1)
         END DO
      END DO
      IF (ABS(den) < tol) THEN
         den = SIGN(tol,den)
      END IF
      NURBS = 1 / den * numVec
      DEALLOCATE(si,ti,w,P,Bi,Bj)
      RETURN
   END FUNCTION NURBS

   !-------------------------------------------------------------------------------------
   ! gets the x,y,z (NURBS2D(1,1),NURBS2D(2,1),NURBS2D(3,1)) from the NonUniform B-Spline
   !    polynomial curve
   !------------Variables------------
   ! s is the parameter for the polynomial curve
   ! si is the knot sequence
   ! w is the set of weights
   ! P is the set of control points
   ! K is the upper sum
   ! M is the degree of the basis function
   FUNCTION NURBS2D(s,si,w,P,K,M)
      REAL(pr), INTENT(IN) :: s,si(:),w(:),P(:,:)
      INTEGER, INTENT(IN) :: K,M
      REAL(pr) :: NURBS2D(3,1)

      REAL(pr) :: Bi(K+2),num1,den1,num2,den2,numVec(3,1),den,tol
      INTEGER :: i,j

      tol = 1e-12

      ! Cox-de Boor formulas (see IGES documentation appendix B)
      DO j=0,M
         DO i=0,K
            IF (j == 0) THEN
               IF (si(i+1) <= s .AND. s <= si(i+2)) THEN
                  Bi(i+1) = 1.0
               ELSE
                  Bi(i+1) = 0.0
               END IF
               IF (si(i+2) <= s .AND. s <= si(i+3)) THEN
                  Bi(i+2) = 1.0
               ELSE
                  Bi(i+2) = 0.0
               END IF
            ELSE
               num1 = (s - si(i+1)) * Bi(i+1)
               den1 = si(i+1+j) - si(i+1)
               num2 = (si(i+2+j) - s) * Bi(i+2)
               den2 = si(i+2+j) - si(i+2)
               IF (ABS(den1) < tol .AND. ABS(den2) < tol) THEN
                  Bi(i+1) = 0.0
               ELSE IF (ABS(den1) < tol) THEN
                  Bi(i+1) = num2 / den2
               ELSE IF (ABS(den2) < tol) THEN
                  Bi(i+1) = num1 / den1
               ELSE
                  Bi(i+1) = num1 / den1 + num2 / den2
               END IF
            END IF
         END DO
      END DO
      den = 0.0
      numVec = 0.0
      DO j=0,K
            numVec = numVec + w(j+1) * Bi(j+1) * P(1:3,j+1:j+1)
            den = den + w(j+1) * Bi(j+1)
      END DO
      NURBS2D = 1 / den * numVec
      RETURN
   END FUNCTION NURBS2D

   !-------------------------------------------------------------------------------------
   ! checks if ray intersects bounding box of surface (or surface patch)
   !------------Variables------------
   ! x is the point of origin of the ray
   ! ray is the ray vector to intersect the surface
   ! xMin is the minimum of each axis on the bounding box
   ! xMax is the maximum of each axis on the bounding box
   FUNCTION isectTest(x,ray,xMin,xMax)
      LOGICAL :: isectTest
      REAL(pr), INTENT(IN) :: x(3,1),ray(3,1),xMin(3,1),xMax(3,1)

      REAL(pr) :: tNear,tFar,tol,plane(3,1),t0,t1,tmp,d
      INTEGER :: i

      tol = 1e-12
      isectTest = .FALSE.
      tNear = -1e12
      tFar = 1e12

      DO i=1,3
         ! YZ Plane i = 1
         ! XZ Plane i = 2
         ! XY Plane i = 3
         ! check if ray is parallel to plane
         plane = 0.0
         plane(i,1) = 1.0
         d = DOT_PRODUCT(ray(:,1),plane(:,1))
         IF (ABS(d) < tol) THEN
            ! ray is parallel to plane
            IF (x(i,1) < xMin(i,1) .OR. xMax(i,1) < x(i,1)) THEN
               ! point is not within the range of x-values
               isectTest = .FALSE.
               RETURN
            END IF
         ELSE
            ! ray not parallel to plane
            t0 = (xMin(i,1) - x(i,1)) / d
            t1 = (xMax(i,1) - x(i,1)) / d
            ! check ordering
            IF (t0 > t1) THEN
               ! swap them
               tmp = t0
               t0 = t1
               t1 = tmp
            END IF
            ! compare with current values
            IF (t0 > tNear) THEN
               tNear = t0
            END IF
            IF (t1 < tFar) THEN
               tFar = t1
            END IF
            IF (tNear > tFar) THEN
               isectTest = .FALSE.
               RETURN
            END IF
            IF (tFar < 0) THEN
               isectTest = .FALSE.
               RETURN
            END IF
         END IF
      END DO
      isectTest = .TRUE.
      RETURN
   END FUNCTION isectTest

! ---2011-11-9 Modifications---
! Fixed problem with logic for tNear and tFar mistakenly using .OR. instead of .AND.
!    when checking line end points

   !-------------------------------------------------------------------------------------
   ! checks if ray intersects revolved line
   !------------Variables------------
   ! x is the point of origin of the ray
   ! ray is the ray vector to intersect the surface
   ! j is the revolved line's index corresponding to which revolved line's data to use
   FUNCTION isectRevolvedLine(x,ray,j)
      INTEGER :: isectRevolvedLine
      REAL(pr), INTENT(IN) :: x(3,1),ray(3,1)
      INTEGER, INTENT(IN) :: j

      REAL(pr) :: aL(3,2),l(3,2),rL(3,1),t1,t2,rP1(3,1),rP2(3,1)
      REAL(pr) :: dist1,dist2,lP1(3,1),lp2(3,1),tol,xNew(3,1),s1,s2,tol3
      REAL(pr) :: aP1(3,1),aP2(3,1),tNear,tFar,lAVec(3,1),tol4
      REAL(pr) :: a,b,c,s,x1(3,1),x2(3,1),x3(3,1),alpha1,alpha2,unitAxis(3,1)
      REAL(pr) :: crossNorm(3,1),vec1(3,1),vec2(3,1),tol2,magNorm,lTvec(3,1)
      INTEGER :: dummy
      ! initialize variables
      isectRevolvedLine = 0
      tol = 1e-12
      tol2 = 0.5
      tol3 = 1e-6
      tol4 = 1e-12
      ! axis of revolution
      aL(:,1) = revolvedLineAxes(j*3+1:j*3+3,1)
      aL(:,2) = revolvedLineAxes(j*3+1:j*3+3,2)
      ! unit vector of axis of revolution
      unitAxis = aL(:,1:1) - aL(:,2:2)
      magNorm = mag(unitAxis)
      unitAxis = unitAxis / magNorm
      ! angles of rotation
      alpha1 = revolvedLineAngles(j+1,1)
      alpha2 = revolvedLineAngles(j+1,2)
      ! line being revolved
      l(:,1) = revolvedLines(j*3+1:j*3+3,1)
      l(:,2) = revolvedLines(j*3+1:j*3+3,2)
      ! radial direction
      rL = revolvedLineRadial(j*3+1:j*3+3,1:1)
      ! parameter of axis corresponding to point in space
      t1 = getAxisT(x,aL)
      ! axis point corresponding to point in space
      aP1 = aL(:,2:2) + t1 * (aL(:,1:1) - aL(:,2:2))
      ! perpendicular vector from axis to point in space
      rP1 = x - aP1
      ! perpendicular distance from axis to point in space
      dist1 = SQRT(DOT_PRODUCT(rP1(:,1),rP1(:,1)))
      ! point projected onto plane formed by line of rotation and plane of rotation
      rP1 = aP1 + dist1 * rL
      ! parameter of axis corresponding to point + ray in space
      t2 = getAxisT(x + ray,aL)
      ! axis point corresponding to point + ray in space
      aP2 = aL(:,2:2) + t2 * (aL(:,1:1) - aL(:,2:2))
      ! perpendicular vector from axis to point + ray in space
      rP2 = x + ray - aP2
      ! perpendicular distance from axis to point + ray in space
      dist2 = SQRT(DOT_PRODUCT(rP2(:,1),rP2(:,1)))
      ! point plus ray projected onto plane formed by line of rotation and plane of rotation
      rP2 = aP2 + dist2 * rL
      ! get point on line corresponding to x projected into plane of rotation
      dummy = isectLine(l,rP1,rL,s1)
      ! check if rL intersected the line
      IF (ABS(s1 - 1e12) < tol) THEN
         dummy = isectLine(l,rP1,-rL,s1)
      END IF
      lP1 = l(:,2:2) + s1 * (l(:,1:1) - l(:,2:2))
      ! get point on line corresponding to x + ray projected into plane of rotation
      dummy = isectLine(l,rP2,rL,s2)
      ! check if rL intersected the line
      IF (ABS(s2 - 1e12) < tol) THEN
         dummy = isectLine(l,rP2,-rL,s2)
      END IF
      lP2 = l(:,2:2) + s2 * (l(:,1:1) - l(:,2:2))
      IF (dummy == 5) PRINT *,dummy
      ! get vectors from aP1 to point and from aP1 to point on rotated line corresponding to point + ray
      !    these will be used to solve the quadratic equation below
      rP1 = x - aP1
      lTVec = lP2 - lP1
      lAVec = lP1 - aP1
      ! the distance from aP1 to the point plus the ray must be the same as the distance
      !    from aP1 to the point on the line corresponding to the point plus the ray
      ! DOT_PRODUCT((x + t * ray-aP1),(x + t * ray - aP1))
      ! == DOT_PRODUCT((lP1 + t * (lP2 - lP1) - aP1),(lP1 + t * (lP2 - lP1) - aP1))
      ! the solution of which is a quadratic equation a * t**2 + b * t + c == 0
      a = DOT_PRODUCT(ray(:,1),ray(:,1)) - DOT_PRODUCT(lTVec(:,1),lTVec(:,1))
      b = 2 * (DOT_PRODUCT(ray(:,1),rP1(:,1)) - DOT_PRODUCT(lTVec(:,1),lAVec(:,1)))
      c = DOT_PRODUCT(rP1(:,1),rP1(:,1)) - DOT_PRODUCT(lAVec(:,1),lAVec(:,1))
      ! if t is found by dividing by zero
      IF (a < tol) THEN
         ! no solution exists
         RETURN
      END IF
      ! if t is complex
      IF (4 * a * c > b**2) THEN
         ! no solution exists
         RETURN
      END IF
      ! quadratic equation smallest portion
      tNear = -b / 2 / a - SQRT(b**2 - 4 * a * c) / 2 / a
      ! quadratic equation farthest portion
      tFar = -b / 2 / a + SQRT(b**2 - 4 * a * c) / 2 / a
      IF (analyzing .AND. abs(x(1,1) - 0.0) < tol3 .AND. abs(x(2,1) + 4.9636365139) < tol3) THEN
        PRINT *,'tNear,tFar = ',tNear,tFar
      END IF
      ! check the portion of the line being intersected
      s = s1 + tNear * (s2 - s1)
      ! if the ray intersects in the positive direction and intersects the rotated line
      IF (tNear >= 0 .AND. s >= 0.0 - tol4 .AND. s <= 1.0 + tol4) THEN
         ! find where it intersects
         xNew = x + tNear * ray
         ! check if it intersects within the revolution
         !    by first obtaining the point on the line being rotated
         x1 = lP1 + tNear * (lP2 - lP1)
         ! revolve it alpha1 to get the first point on the circle
         CALL getArbitraryRotation(unitAxis,aP1,alpha1,x1,x2)
         ! revolve it alpha2 to get the second point on the circle
         CALL getArbitraryRotation(unitAxis,aP1,alpha2,x1,x3)
         ! get the vector between the point of intersection and the first point
         vec1 = xNew - x2
         ! and the vector between the second point and the first point
         vec2 = x3 - x2
         ! the cross product would be in the same direction as axisNorm if the intersection
         !    lies on the projected circle
         crossNorm = crossProduct(vec1,vec2)
         magNorm = mag(crossNorm)
         crossNorm = crossNorm / magNorm
         IF (analyzing .AND. abs(x(1,1) - 0.0) < tol3 .AND. abs(x(2,1) + 4.9636365139) < tol3) THEN
            PRINT *,'tNear'
            PRINT *,'xNew,crossNorm,unitAxis = ',xNew(1,1),crossNorm(1,1),unitAxis(1,1)
            PRINT *,'                          ',xNew(2,1),crossNorm(2,1),unitAxis(2,1)
            PRINT *,'                          ',xNew(3,1),crossNorm(3,1),unitAxis(3,1)
         END IF
         IF (mag(crossNorm - unitAxis) < tol2) THEN
            isectRevolvedLine = isectRevolvedLine + 1
         END IF
      END IF
      ! check the portion of the line being intersected
      s = s1 + tFar * (s2 - s1)
      ! if the ray intersects in the positive direction and intersects the rotated line
      IF (tFar >= 0 .AND. s >= 0.0 - tol4 .AND. s <= 1.0 + tol4) THEN
         ! find where it intersects
         xNew = x + tFar * ray
         ! check if it intersects within the revolution
         !    by first obtaining the point on the line being rotated
         x1 = lP1 + tFar * (lP2 - lP1)
         ! revolve it alpha1 to get the first point on the circle
         CALL getArbitraryRotation(unitAxis,aP1,alpha1,x1,x2)
         ! revolve it alpha2 to get the second point on the circle
         CALL getArbitraryRotation(unitAxis,aP1,alpha2,x1,x3)
         ! get the vector between the point of intersection and the first point
         vec1 = xNew - x2
         ! and the vector between the second point and the first point
         vec2 = x3 - x2
         ! the cross product would be in the same direction as axisNorm if the intersection
         !    lies on the projected circle
         crossNorm = crossProduct(vec1,vec2)
         magNorm = mag(crossNorm)
         crossNorm = crossNorm / magNorm
         IF (analyzing .AND. abs(x(1,1) - 0.0) < tol3 .AND. abs(x(2,1) + 4.9636365139) < tol3) THEN
            PRINT *,'tFar'
            PRINT *,'xNew,crossNorm,unitAxis = ',xNew(1,1),crossNorm(1,1),unitAxis(1,1)
            PRINT *,'                          ',xNew(2,1),crossNorm(2,1),unitAxis(2,1)
            PRINT *,'                          ',xNew(3,1),crossNorm(3,1),unitAxis(3,1)
         END IF
         IF (mag(crossNorm - unitAxis) < tol2) THEN
            isectRevolvedLine = isectRevolvedLine + 1
         END IF
      END IF

   END FUNCTION isectRevolvedLine


   !-------------------------------------------------------------------------------------
   ! checks if ray intersects revolved circle
   !------------Variables------------
   ! x is the point of origin of the ray
   ! ray is the ray vector to intersect the surface
   ! j is the revolved circle's index corresponding to which revolved circle's data to use
   FUNCTION isectRevolvedCircle(x,ray,j)
      INTEGER :: isectRevolvedCircle
      REAL(pr), INTENT(IN) :: x(3,1),ray(3,1)
      INTEGER, INTENT(IN) :: j

      REAL(pr) :: x1(3,1),x2(3,1),radius,a,b,c,xX1Vec(3,1),tNear,tFar,tol
      REAL(pr) :: xNew(3,1),minDiff(3,1),maxDiff(3,1),xMin(3,1),xMax(3,1),tol2
      REAL(pr) :: aL(3,2),theta,aP(3,1),t1,t2,t,aLVec(3,1),aLRVec(3,1),rL(3,1)
      REAL(pr) :: unitAxis(3,1),magunitAxis,magaLVec,newVec(3,1),zeroVec(3,1)
      ! bounding box of revolved surface
      xMin = revolvedCircleExtrema(j*3+1:j*3+3,1:1)
      xMax = revolvedCircleExtrema(j*3+1:j*3+3,2:2)
      ! initialize variables
      isectRevolvedCircle = 0
      tol = 1e-12
      tol2 = 1e-5
      ! radial direction
      rL = revolvedCircleRadial(j*3+1:j*3+3,1:1)
      ! circle's center
      x1(:,1) = revolvedCircles(j+1,3:5)
      ! first parametric point on circle
      x2(:,1) = revolvedCircles(j+1,6:8)
      ! circle's radius
      radius = SQRT(DOT_PRODUCT(x2(:,1) - x1(:,1),x2(:,1) - x1(:,1)))
      ! beginning and ending angles of rotation
      t1 = revolvedCircleAngles(j+1,1)
      t2 = revolvedCircleAngles(j+1,2)
      ! axis of rotation
      aL = revolvedCircleAxes(j*3+1:j*3+3,1:2)
      ! solve equation for sphere
      ! DOT_PRODUCT((x + t * ray - x1),(x + t * ray - x1)) = radius**2
      !    creates a quadratic equation for t; a * t**2 + b * t + c = 0
      xX1Vec = x - x1
      a = DOT_PRODUCT(ray(:,1),ray(:,1))
      b = 2 * DOT_PRODUCT(xX1Vec(:,1),ray(:,1))
      c = DOT_PRODUCT(xX1Vec(:,1),xX1Vec(:,1)) - radius**2
      ! if t is found by dividing by zero
      IF (a < tol) THEN
         ! no solution exists
         RETURN
      END IF
      ! if t is complex
      IF (4 * a * c > b**2) THEN
         ! no solution exists
         RETURN
      END IF
      ! quadratic equation smallest portion
      tNear = -b / 2 / a - SQRT(b**2 - 4 * a * c) / 2 / a
      ! quadratic equation farthest portion
      tFar = -b / 2 / a + SQRT(b**2 - 4 * a * c) / 2 / a
      ! if the ray intersects in the positive direction
      IF (tNear >= 0 .AND. ABS(tNear - tFar) < tol) THEN
         ! find where it intersects
         xNew = x + tNear * ray
         minDiff = xMin - xNew
         maxDiff = xNew - xMax
         ! check if it intersects within the angle of rotation
         t = getAxisT(xNew,aL)
         aP = aL(:,2:2) + t * (aL(:,1:1) - aL(:,2:2))
         aLVec = xNew - aP
         magaLVec = mag(aLVec)
         aLVec = aLVec / magaLVec
         aLRVec = rL
         unitAxis = aL(:,1:1) - aL(:,2:2)
         magunitAxis = mag(unitAxis)
         unitAxis = unitAxis / magunitAxis
         zeroVec = 0.0
         IF (ABS(DOT_PRODUCT(aLVec(:,1),aLRVec(:,1)) + 1.0) < tol) &
         &THEN
            theta = ACOS(-1.0)
         ELSE IF (ABS(DOT_PRODUCT(aLVec(:,1),aLRVec(:,1)) - 1.0) &
         &< tol) THEN
            theta = ACOS(1.0)
         ELSE IF (ABS(DOT_PRODUCT(aLVec(:,1),aLRVec(:,1))) > 1.0) &
         &THEN
            theta = t2 + tol
         ELSE
            theta = ACOS(DOT_PRODUCT(aLVec(:,1),aLRVec(:,1)))
         END IF
         CALL getArbitraryRotation(unitAxis,zeroVec,theta,aLRVec,newVec)
         ! check if the dot product gave the angle in the counterclockwise direction
         IF (ABS(mag(newVec - aLVec)) > tol2) THEN
            ! if the dot product angle was in the opposite direction,
            !    maintain counterclockwise convention by replacing theta with theta = 2.0 * PI - theta
            theta = 2.0 * ACOS(-1.0) - theta
            CALL getArbitraryRotation(unitAxis,zeroVec,theta,aLRVec,newVec)
            ! check if the logic is correct
            IF (ABS(mag(newVec - aLVec)) > tol2) THEN
               PRINT *,'***************ERROR ISECT CIRREV***************'
            END IF
         END IF
         IF (theta <= t2 .AND. theta >= t1) THEN
            ! check if it intersects within the bounding box
            IF (MAX(minDiff(1,1),minDiff(2,1),minDiff(3,1)) < 0 .AND. &
            &MAX(maxDiff(1,1),maxDiff(2,1),maxDiff(3,1)) < 0) THEN
               isectRevolvedCircle = isectRevolvedCircle + 1
               RETURN
            END IF
         END IF
      END IF
      ! if the near ray intersects in the positive direction
      IF (tNear >= 0) THEN
         ! find where it intersects
         xNew = x + tNear * ray
         minDiff = xMin - xNew
         maxDiff = xNew - xMax
         ! check if it intersects within the angle of rotation
         t = getAxisT(xNew,aL)
         aP = aL(:,2:2) + t * (aL(:,1:1) - aL(:,2:2))
         aLVec = xNew - aP
         magaLVec = mag(aLVec)
         aLVec = aLVec / magaLVec
         aLRVec = rL
         unitAxis = aL(:,1:1) - aL(:,2:2)
         magunitAxis = mag(unitAxis)
         unitAxis = unitAxis / magunitAxis
         zeroVec = 0.0
         IF (ABS(DOT_PRODUCT(aLVec(:,1),aLRVec(:,1)) + 1.0) < tol) &
         &THEN
            theta = ACOS(-1.0)
         ELSE IF (ABS(DOT_PRODUCT(aLVec(:,1),aLRVec(:,1)) - 1.0) &
         &< tol) THEN
            theta = ACOS(1.0)
         ELSE IF (ABS(DOT_PRODUCT(aLVec(:,1),aLRVec(:,1))) > 1.0) &
         &THEN
            theta = t2 + tol
         ELSE
            theta = ACOS(DOT_PRODUCT(aLVec(:,1),aLRVec(:,1)))
         END IF
         CALL getArbitraryRotation(unitAxis,zeroVec,theta,aLRVec,newVec)
         ! check if the dot product gave the angle in the counterclockwise direction
         IF (ABS(mag(newVec - aLVec)) > tol2) THEN
            ! if the dot product angle was in the opposite direction,
            !    maintain counterclockwise convention by replacing theta with theta = 2.0 * PI - theta
            theta = 2.0 * ACOS(-1.0) - theta
            CALL getArbitraryRotation(unitAxis,zeroVec,theta,aLRVec,newVec)
            ! check if the logic is correct
            IF (ABS(mag(newVec - aLVec)) > tol2) THEN
               PRINT *,'***************ERROR ISECT CIRREV***************'
            END IF
         END IF
         IF (theta <= t2 .AND. theta >= t1) THEN
            ! check if it intersects within the bounding box
            IF (MAX(minDiff(1,1),minDiff(2,1),minDiff(3,1)) < 0 .AND. &
            &MAX(maxDiff(1,1),maxDiff(2,1),maxDiff(3,1)) < 0) THEN
               isectRevolvedCircle = isectRevolvedCircle + 1
            END IF
         END IF
      END IF
      ! if the far ray intersects in the positive direction
      IF (tFar >= 0) THEN
         ! find where it intersects
         xNew = x + tFar * ray
         minDiff = xMin - xNew
         maxDiff = xNew - xMax
         ! check if it intersects within the angle of rotation
         t = getAxisT(xNew,aL)
         aP = aL(:,2:2) + t * (aL(:,1:1) - aL(:,2:2))
         aLVec = xNew - aP
         magaLVec = mag(aLVec)
         aLVec = aLVec / magaLVec
         aLRVec = rL
         unitAxis = aL(:,1:1) - aL(:,2:2)
         magunitAxis = mag(unitAxis)
         unitAxis = unitAxis / magunitAxis
         zeroVec = 0.0
         IF (ABS(DOT_PRODUCT(aLVec(:,1),aLRVec(:,1)) + 1.0) < tol) &
         &THEN
            theta = ACOS(-1.0)
         ELSE IF (ABS(DOT_PRODUCT(aLVec(:,1),aLRVec(:,1)) - 1.0) &
         &< tol) THEN
            theta = ACOS(1.0)
         ELSE IF (ABS(DOT_PRODUCT(aLVec(:,1),aLRVec(:,1))) > 1.0) &
         &THEN
            theta = t2 + tol
         ELSE
            theta = ACOS(DOT_PRODUCT(aLVec(:,1),aLRVec(:,1)))
         END IF
         CALL getArbitraryRotation(unitAxis,zeroVec,theta,aLRVec,newVec)
         ! check if the dot product gave the angle in the counterclockwise direction
         IF (ABS(mag(newVec - aLVec)) > tol2) THEN
            ! if the dot product angle was in the opposite direction,
            !    maintain counterclockwise convention by replacing theta with theta = 2.0 * PI - theta
            theta = 2.0 * ACOS(-1.0) - theta
            CALL getArbitraryRotation(unitAxis,zeroVec,theta,aLRVec,newVec)
            ! check if the logic is correct
            IF (ABS(mag(newVec - aLVec)) > tol2) THEN
               PRINT *,'***************ERROR ISECT CIRREV***************'
            END IF
         END IF
         IF (theta <= t2 .AND. theta >= t1) THEN
            ! check if it intersects within the bounding box
            IF (MAX(minDiff(1,1),minDiff(2,1),minDiff(3,1)) < 0 .AND. &
            &MAX(maxDiff(1,1),maxDiff(2,1),maxDiff(3,1)) < 0) THEN
               isectRevolvedCircle = isectRevolvedCircle + 1
            END IF
         END IF
      END IF
   END FUNCTION isectRevolvedCircle

   !-------------------------------------------------------------------------------------
   ! gets the derivative of the NURBS function with respect to s
   !------------Variables------------
   ! s is the first parameter of the B-Spline Surface
   ! t is the second parameter of the B-Spline Surface
   ! k is the B-Spline surface index corresponding to which B-Spline surface data to use
   ! h is the difference used in the finite difference equation
   ! primes is the derivative of the NURBS function with respect to s
   FUNCTION primes(s,t,k,h)
      REAL(pr), INTENT(IN) :: s,t,h
      INTEGER, INTENT(IN) :: k
      REAL(pr) :: primes(3,1)

      REAL(pr), DIMENSION(3,1) :: x1,x2,x3,x4
      REAL(pr) :: U(2)

      U = NURBSU(k,:)
      IF (ABS(s-U(2))>2*h .AND. ABS(s-U(1))>h) THEN
         x1 = nurbs(k,s-h,t)
         x2 = nurbs(k,s,t)
         x3 = nurbs(k,s+h,t)
         x4 = nurbs(k,s+2*h,t)
         primes = (-2 * x1 - 3 * x2 + 6 * x3 - x4)/(6*h)
      END IF
      IF (ABS(s-U(1))<=h) THEN
         x1 = nurbs(k,s,t)
         x2 = nurbs(k,s+h,t)
         x3 = nurbs(k,s+2*h,t)
         x4 = nurbs(k,s+3*h,t)
         primes = (-11 * x1 + 18 * x2 - 9 * x3 + 2 * x4)/(6*h)
      END IF
      IF (ABS(s-U(2))<=2*h) THEN
         x1 = nurbs(k,s,t)
         x2 = nurbs(k,s-h,t)
         x3 = nurbs(k,s-2*h,t)
         x4 = nurbs(k,s-3*h,t)
         primes = (11 * x1 - 18 * x2 + 9 * x3 - 2 * x4)/(6*h)
      END IF
   END FUNCTION primes

   !-------------------------------------------------------------------------------------
   ! gets the derivative of the NURBS function with respect to t
   !------------Variables------------
   ! s is the first parameter of the B-Spline Surface
   ! t is the second parameter of the B-Spline Surface
   ! k is the B-Spline surface index corresponding to which B-Spline surface data to use
   ! h is the difference used in the finite difference equation
   ! primes is the derivative of the NURBS function with respect to s
   FUNCTION primet(s,t,k,h)
      REAL(pr), INTENT(IN) :: s,t,h
      INTEGER, INTENT(IN) :: k
      REAL(pr) :: primet(3,1)

      REAL(pr), DIMENSION(3,1) :: x1,x2,x3,x4
      REAL(pr) :: V(2)

      V = NURBSV(k,:)
      IF (ABS(t-V(2))>2*h .AND. ABS(t-V(1))>h) THEN
         x1 = nurbs(k,s,t-h)
         x2 = nurbs(k,s,t)
         x3 = nurbs(k,s,t+h)
         x4 = nurbs(k,s,t+2*h)
         primet = (-2 * x1 - 3 * x2 + 6 * x3 - x4)/(6*h)
      END IF
      IF (ABS(t-V(1))<=h) THEN
         x1 = nurbs(k,s,t)
         x2 = nurbs(k,s,t+h)
         x3 = nurbs(k,s,t+2*h)
         x4 = nurbs(k,s,t+3*h)
         primet = (-11 * x1 + 18 * x2 - 9 * x3 + 2 * x4)/(6*h)
      END IF
      IF (ABS(t-V(2))<=2*h) THEN
         x1 = nurbs(k,s,t)
         x2 = nurbs(k,s,t-h)
         x3 = nurbs(k,s,t-2*h)
         x4 = nurbs(k,s,t-3*h)
         primet = (11 * x1 - 18 * x2 + 9 * x3 - 2 * x4)/(6*h)
      END IF
   END FUNCTION primet

   !-------------------------------------------------------------------------------------
   ! gets the second derivative of the NURBS function with respect to s
   !------------Variables------------
   ! s is the first parameter of the B-Spline Surface
   ! t is the second parameter of the B-Spline Surface
   ! k is the B-Spline surface index corresponding to which B-Spline surface data to use
   ! h is the difference used in the finite difference equation
   ! primes is the derivative of the NURBS function with respect to s
   FUNCTION dprimes(s,t,k,h)
      REAL(pr), INTENT(IN) :: s,t,h
      INTEGER, INTENT(IN) :: k
      REAL(pr) :: dprimes(3,1)

      REAL(pr), DIMENSION(3,1) :: x1,x2,x3,x4
      REAL(pr) :: U(2)

      U = NURBSU(k,:)
      IF (ABS(s-U(2))>2*h .AND. ABS(s-U(1))>h) THEN
         x1 = nurbs(k,s-h,t)
         x2 = nurbs(k,s,t)
         x3 = nurbs(k,s+h,t)
         dprimes = (x1 - 2 * x2 + x3)/(h**2)
      END IF
      IF (ABS(s-U(1))<=h) THEN
         x1 = nurbs(k,s,t)
         x2 = nurbs(k,s+h,t)
         x3 = nurbs(k,s+2*h,t)
         x4 = nurbs(k,s+3*h,t)
         dprimes = (2 * x1 - 5 * x2 + 4 * x3 - x4)/(h**2)
      END IF
      IF (ABS(s-U(2))<=2*h) THEN
         x1 = nurbs(k,s,t)
         x2 = nurbs(k,s-h,t)
         x3 = nurbs(k,s-2*h,t)
         x4 = nurbs(k,s-3*h,t)
         dprimes = (2 * x1 - 5 * x2 + 4 * x3 - x4)/(h**2)
      END IF
   END FUNCTION dprimes

   !-------------------------------------------------------------------------------------
   ! gets the second derivative of the NURBS function with respect to t
   !------------Variables------------
   ! s is the first parameter of the B-Spline Surface
   ! t is the second parameter of the B-Spline Surface
   ! k is the B-Spline surface index corresponding to which B-Spline surface data to use
   ! h is the difference used in the finite difference equation
   ! primes is the derivative of the NURBS function with respect to s
   FUNCTION dprimet(s,t,k,h)
      REAL(pr), INTENT(IN) :: s,t,h
      INTEGER, INTENT(IN) :: k
      REAL(pr) :: dprimet(3,1)

      REAL(pr), DIMENSION(3,1) :: x1,x2,x3,x4
      REAL(pr) :: V(2)

      V = NURBSV(k,:)
      IF (ABS(t-V(2))>2*h .AND. ABS(t-V(1))>h) THEN
         x1 = nurbs(k,s,t-h)
         x2 = nurbs(k,s,t)
         x3 = nurbs(k,s,t+h)
         dprimet = (x1 - 2 * x2 + x3)/(h**2)
      END IF
      IF (ABS(t-V(1))<=h) THEN
         x1 = nurbs(k,s,t)
         x2 = nurbs(k,s,t+h)
         x3 = nurbs(k,s,t+2*h)
         x4 = nurbs(k,s,t+3*h)
         dprimet = (2 * x1 - 5 * x2 + 4 * x3 - x4)/(h**2)
      END IF
      IF (ABS(t-V(2))<=2*h) THEN
         x1 = nurbs(k,s,t)
         x2 = nurbs(k,s,t-h)
         x3 = nurbs(k,s,t-2*h)
         x4 = nurbs(k,s,t-3*h)
         dprimet = (2 * x1 - 5 * x2 + 4 * x3 - x4)/(h**2)
      END IF
   END FUNCTION dprimet

   !-------------------------------------------------------------------------------------
   ! uses Broyden's method to determine whether the ray intersects the NURBS surface
   !------------Variables------------
   ! k is the B-Spline surface index corresponding to which B-Spline surface data to use
   ! n0 defines the first plane which intersects the second plane defined by n1
   ! d0 is the d value for the plane equation DOT_PRODUCT(n0,x) + d0 = 0
   ! d1 is the d value for the plane equation DOT_PRODUCT(n1,x) + d1 = 0
   ! sguess is the initial guess to the s value for finding the intersection point
   ! tguess is the initial guess to the t value for finding the intersection point
   ! x is the point of origin
   ! ray is the ray from the point of origin that may intersect the surface
   ! Q is the point of intersection on the patch
   ! s1 and s2 is the range of s values for this patch
   ! t1 and t2 is the range of t values for this patch
   FUNCTION Broyden(k,n0,n1,d0,d1,sguess,tguess,x,ray,Q,U,V,s1,s2,t1,t2)
      REAL(pr), INTENT(IN) :: n0(3,1),n1(3,1),d0,d1,x(3,1),U(2),V(2)
      REAL(pr), INTENT(IN) :: ray(3,1),s1,s2,t1,t2
      REAL(pr), INTENT(INOUT) :: sguess,tguess
      INTEGER, INTENT(IN) :: k
      REAL(pr), INTENT(OUT) :: Q(3,1)
      LOGICAL :: Broyden

      REAL(pr) :: J(2,2),invJ(2,2),SVec(2,1),SVecOld(2,1),dVec(2,1)
      REAL(pr) :: pQ0ps(3,1),pQ0pt(3,1),tol,eps,dist,s,t,d,dS(2,1),tol2
      REAL(pr) :: dJ(2,1),QxRay(3,1),magQxRay,h,olds,oldt,oldInvJ(2,2)
      INTEGER :: i,maxIter,outside

      ! Initialize variables
      maxIter = 20
      tol = 1e-12
      tol2 = 1e-3
      eps = 1e-8
      h = 1e-6
      dist = 1.0
      i = 0
      outside = 0
      Broyden = .FALSE.
      s = sguess
      t = tguess
      ! NURBS x,y,z point for the guess values
      Q = NURBS(k+1,s,t)
      ! S0 and S1 values
      SVec(1,1) = DOT_PRODUCT(n0(:,1),Q(:,1)) + d0
      SVec(2,1) = DOT_PRODUCT(n1(:,1),Q(:,1)) + d1
      ! partial derivatives of Q
      pQ0ps = primes(s,t,k+1,h)
      pQ0pt = primet(s,t,k+1,h)
      ! partial derivatives of S
      J(1,1) = DOT_PRODUCT(n0(:,1),pQ0ps(:,1))
      J(1,2) = DOT_PRODUCT(n0(:,1),pQ0pt(:,1))
      J(2,1) = DOT_PRODUCT(n1(:,1),pQ0ps(:,1))
      J(2,2) = DOT_PRODUCT(n1(:,1),pQ0pt(:,1))
      ! check if Jacobian is singular
      IF (ABS(det(J,2)) < tol) THEN
         J(1,1) = J(1,1) + tol
         J(2,2) = J(2,2) + tol
      END IF
      ! get its inverse
      CALL inverse(J,invJ,2)
      ! store old s, t, and inverse of Jacobian
      olds = s
      oldt = t
      oldInvJ = invJ
      ! while i is less than the maximum allowed number of iterations and the ray end point
      !    is still too far from the surface to consider it an intersection
      DO WHILE (i < maxIter .AND. dist > eps)
         ! linear change in s and t values with change in NURBS points
         dVec = MATMUL(invJ,SVec)
         ! get new s and t
         s = s - dVec(1,1)
         ! check s boundaries
         IF (ABS(s - U(2)) < tol2) THEN
            s = U(2) - tol
         END IF
         IF (ABS(s - U(1)) < tol2) THEN
            s = U(1) + tol
         END IF
         t = t - dVec(2,1)
         ! check t boundaries
         IF (ABS(t - V(2)) < tol2) THEN
            t = V(2) - tol
         END IF
         IF (ABS(t - V(1)) < tol2) THEN
            t = V(1) + tol
         END IF
         IF (s < U(1) .OR. s > U(2) .OR. t < V(1) .OR. &
         &t > V(2)) THEN
!$$$$$$ ! For debugging purposes
!$$$$$$             PRINT *,'outside ranges'
!$$$$$$             PRINT *,'s = ',U(1),s,U(2)
!$$$$$$             PRINT *,'t = ',V(1),t,V(2)
!$$$$$$             PRINT *,'x = ',x(:,1)
!$$$$$$             PRINT *,'Q = ',Q(:,1)
!$$$$$$             RETURN
            outside = outside + 1
            s = s1 + 2 * outside / REAL(maxIter) * (s2 - s1)
            t = t2 - 2 * outside / REAL(maxIter) * (t2 - t1)
            dVec(1,1) = olds - s
            dVec(2,1) = oldt - t
            IF (ABS(dVec(1,1) + dVec(2,1)) < tol) THEN
               outside = outside + 1
               s = s1 + 2 * outside / REAL(maxIter) * (s2 - s1)
               t = t2 - 2 * outside / REAL(maxIter) * (t2 - t1)
               dVec(1,1) = olds - s
               dVec(2,1) = oldt - t
            END IF
            invJ = oldInvJ
         END IF
         ! NURBS x,y,z point for the guess values
         Q = NURBS(k+1,s,t)
         ! store old function values
         SVecOld = SVec
         ! S0 and S1 values
         SVec(1,1) = DOT_PRODUCT(n0(:,1),Q(:,1)) + d0
         SVec(2,1) = DOT_PRODUCT(n1(:,1),Q(:,1)) + d1
         dist = DOT_PRODUCT(SVec(:,1),SVec(:,1))
         ! get change in function values
         dS = SVec - SVecOld
         ! for inverse of Jacobian
         dJ = MATMUL(invJ,dS)
         ! get denominator
         d = DOT_PRODUCT(-dVec(:,1),dJ(:,1))
         ! if this method is singular
         IF (ABS(d) < tol) THEN
!$$$$$$ ! For debugging purposes
!$$$$$$             PRINT *,'min d = ',d
!$$$$$$             PRINT *,'x = ',x(:,1)
!$$$$$$             PRINT *,'Q = ',Q(:,1)
            ! no intersection occurs
            RETURN
         END IF
         ! otherwise, get inverse of Jacobian using good Broyden method
         invJ = invJ + 1 / d * MATMUL(-dVec - MATMUL(invJ,dS),&
                                     &MATMUL(TRANSPOSE(-dVec),invJ))
         ! keep track of number of iterations so far
         i = i + 1
      END DO !WHILE
      ! make sure the ray intersection occurs in the right direction
      QxRay = Q - x
      magQxRay = mag(QxRay)
      QxRay = QxRay / magQxRay
      magQxRay = mag(QxRay - ray)
      IF (dist <= eps .AND. magQxRay < 1.0 .AND. s1 <= s .AND. s <= s2 &
      &.AND. t1 <= t .AND. t <= t2) THEN
         Broyden = .TRUE.
!$$$$$$          ! For debugging purposes
!$$$$$$          PRINT *,'intersection'
!$$$$$$          PRINT *,'s = ',s1,s,s2
!$$$$$$          PRINT *,'t = ',t1,t,t2
!$$$$$$          PRINT *,'x = ',x(:,1)
!$$$$$$          PRINT *,'Q = ',Q(:,1)
         sguess = s
         tguess = t
         IF (i > maxIterGlobal) THEN
            maxIterGlobal = i
            IF (analyzing2) PRINT *,'maxIterGlobal = ',i
         END IF
         RETURN
      END IF
   END FUNCTION Broyden



   !-------------------------------------------------------------------------------------
   ! uses Newton's method to determine whether the ray intersects the NURBS surface
   !------------Variables------------
   ! k is the B-Spline surface index corresponding to which B-Spline surface data to use
   ! n0 defines the first plane which intersects the second plane defined by n1
   ! d0 is the d value for the plane equation DOT_PRODUCT(n0,x) + d0 = 0
   ! d1 is the d value for the plane equation DOT_PRODUCT(n1,x) + d1 = 0
   ! sguess is the initial guess to the s value for finding the intersection point
   ! tguess is the initial guess to the t value for finding the intersection point
   ! x is the point of origin
   ! ray is the ray from the point of origin that may intersect the surface
   ! Q is the point of intersection on the patch
   ! s1 and s2 is the range of s values for this patch
   ! t1 and t2 is the range of t values for this patch
   FUNCTION Newton(k,n0,n1,d0,d1,sguess,tguess,x,ray,Q,U,V,s1,s2,t1,t2)
      REAL(pr), INTENT(IN) :: n0(3,1),n1(3,1),d0,d1,x(3,1)
      REAL(pr), INTENT(IN) :: ray(3,1),U(2),V(2),s1,s2,t1,t2
      REAL(pr), INTENT(INOUT) :: sguess,tguess
      INTEGER, INTENT(IN) :: k
      REAL(pr), INTENT(OUT) :: Q(3,1)
      LOGICAL :: Newton

      REAL(pr) :: J(2,2),invJ(2,2),SVec(2,1),dVec(2,1)
      REAL(pr) :: pQps(3,1),pQpt(3,1),tol,eps,dist,s,t
      REAL(pr) :: QxRay(3,1),magQxRay,h,olds,oldt,tol2,tolNewton
      INTEGER :: i,maxIter,outside

      ! Initialize variables
      maxIter = 10
      tol = 1e-12
      tol2 = 1e-3
      eps = 1e-8
      h = 1e-6
      dist = 1.0
      i = 0
      outside = 0
      Newton = .FALSE.
      s = sguess
      t = tguess
      tolNewton = 5e-2
      ! NURBS x,y,z point for the guess values
      Q = NURBS(k+1,s,t)
      ! S0 and S1 values
      SVec(1,1) = DOT_PRODUCT(n0(:,1),Q(:,1)) + d0
      SVec(2,1) = DOT_PRODUCT(n1(:,1),Q(:,1)) + d1
      ! partial derivatives of Q
      pQps = primes(s,t,k+1,h)
      pQpt = primet(s,t,k+1,h)
      ! partial derivatives of S
      J(1,1) = DOT_PRODUCT(n0(:,1),pQps(:,1))
      J(1,2) = DOT_PRODUCT(n0(:,1),pQpt(:,1))
      J(2,1) = DOT_PRODUCT(n1(:,1),pQps(:,1))
      J(2,2) = DOT_PRODUCT(n1(:,1),pQpt(:,1))
      ! check if Jacobian is singular
      IF (ABS(det(J,2)) < tol) THEN
         J(1,1) = J(1,1) + tol
         J(2,2) = J(2,2) + tol
      END IF
      ! get its inverse
      CALL inverse(J,invJ,2)
      ! store old s and t
      olds = s
      oldt = t
      ! while i is less than the maximum allowed number of iterations and the ray end point
      !    is still too far from the surface to consider it an intersection
      DO WHILE (i < maxIter .AND. dist > eps)
         ! linear change in s and t values with change in NURBS points
         dVec = MATMUL(invJ,SVec)
         ! get new s and t
         s = s - dVec(1,1)
         ! check s boundaries
         IF (ABS(s - U(2)) < tol2) THEN
            s = U(2)
         END IF
         IF (ABS(s - U(1)) < tol2) THEN
            s = U(1)
         END IF
         t = t - dVec(2,1)
         ! check t boundaries
         IF (ABS(t - V(2)) < tol2) THEN
            t = V(2)
         END IF
         IF (ABS(t - V(1)) < tol2) THEN
            t = V(1)
         END IF
!          PRINT *,'s,t = ',s,',',t
!          PRINT *,'U(1),U(2),V(1),V(2) = ',U(1),',',U(2),',',V(1),',',V(2)
         IF (s < U(1) .OR. s > U(2) .OR. t < V(1) .OR. &
         &t > V(2)) THEN
            outside = outside + 1
            s = s1 + 2 * outside / REAL(maxIter) * (s2 - s1)
            t = t2 - 2 * outside / REAL(maxIter) * (t2 - t1)
            dVec(1,1) = olds - s
            dVec(2,1) = oldt - t
            IF (ABS(dVec(1,1) + dVec(2,1)) < tol) THEN
               outside = outside + 1
               s = s1 + 2 * outside / REAL(maxIter) * (s2 - s1)
               t = t2 - 2 * outside / REAL(maxIter) * (t2 - t1)
               dVec(1,1) = olds - s
               dVec(2,1) = oldt - t
            END IF
         END IF
         ! NURBS x,y,z point for the guess values
         Q = NURBS(k+1,s,t)
         ! S0 and S1 values
         SVec(1,1) = DOT_PRODUCT(n0(:,1),Q(:,1)) + d0
         SVec(2,1) = DOT_PRODUCT(n1(:,1),Q(:,1)) + d1
         dist = DOT_PRODUCT(SVec(:,1),SVec(:,1))
         ! partial derivatives of Q
         pQps = primes(s,t,k+1,h)
         pQpt = primet(s,t,k+1,h)
         ! get new Jacobian
         J(1,1) = DOT_PRODUCT(n0(:,1),pQps(:,1))
         J(1,2) = DOT_PRODUCT(n0(:,1),pQpt(:,1))
         J(2,1) = DOT_PRODUCT(n1(:,1),pQps(:,1))
         J(2,2) = DOT_PRODUCT(n1(:,1),pQpt(:,1))
         ! check if Jacobian is singular
         IF (ABS(det(J,2)) < tol) THEN
            J(1,1) = J(1,1) + tol
            J(2,2) = J(2,2) + tol
         END IF
         ! get its inverse
         CALL inverse(J,invJ,2)
         ! keep track of number of iterations so far
         i = i + 1
      END DO !WHILE
      ! make sure the ray intersection occurs in the right direction
      QxRay = Q - x
      magQxRay = mag(QxRay)
      QxRay = QxRay / magQxRay
      magQxRay = mag(QxRay - ray)
      ! check s boundaries
      IF (ABS(s - s2) < tol2) THEN
         s = s2
      END IF
      IF (ABS(s - s1) < tol2) THEN
         s = s1
      END IF
      ! check t boundaries
      IF (ABS(t - t2) < tol2) THEN
         t = t2
      END IF
      IF (ABS(t - t1) < tol2) THEN
         t = t1
      END IF
!       PRINT *,'Q ray = ',Q(1,1),' ',ray(1,1)
!       PRINT *,'        ',Q(2,1),' ',ray(2,1)
!       PRINT *,'        ',Q(3,1),' ',ray(3,1)
!       PRINT *,'s1,s,s2 = ',s1,',',s,',',s2
!       PRINT *,'t1,t,t2 = ',t1,',',t,',',t2
      IF (dist <= eps .AND. magQxRay < tolNewton .AND. s1 <= s .AND. s <= s2 &
      &.AND. t1 <= t .AND. t <= t2) THEN
         Newton = .TRUE.
         sguess = s
         tguess = t
         IF (i > maxIterGlobal .AND. analyzing2) THEN
            maxIterGlobal = i
            PRINT *,'maxIterGlobal = ',i
         END IF
         RETURN
      END IF
   END FUNCTION Newton

! ---2011-11-11 Modifications---
! Added a check to see if planar circular arcs are intersected at their end points

   !-------------------------------------------------------------------------------------
   ! finds whether the ray from x intersects the circle
   !------------Variables------------
   ! n1 is the normal to the plane the circle lies on
   ! x1 is the circle's center point
   ! x2 is the first parametric point on the circle
   ! x3 is the second parametric point on the circle
   ! x is the origin of the ray
   ! ray is the ray from x to the circle's perimeter
   FUNCTION isectCircle(n1,x1,x2,x3,x,ray)
      REAL(pr), INTENT(IN), DIMENSION(3,1) :: n1,x1,x2,x3,x,ray
      INTEGER :: isectCircle

      REAL(pr) :: a,b,c,radius,tNear,tFar,tol,vec1(3,1),vec2(3,1),n2(3,1),magn2
      REAL(pr) :: tol2,vec2Test1,vec2Test2
      ! Initialize variables
      tol = 0.5
      isectCircle = 0
      tol2 = 1e-12
      ! find where the ray intersects the circle by generating the quadratic equation
      ! DOT_PRODUCT(x + t * ray - x1, x + t * ray - x1) = radius**2
      radius = SQRT(DOT_PRODUCT(x2(:,1) - x1(:,1), x2(:,1) - x1(:,1)))
      ! get vector from the first point on the circle to the second point on the circle
      vec1 = x3 - x2
      a = DOT_PRODUCT(ray(:,1),ray(:,1))
      b = 2 * DOT_PRODUCT(x(:,1) - x1(:,1),ray(:,1))
      c = DOT_PRODUCT(x(:,1) - x1(:,1),x(:,1) - x1(:,1)) - radius**2
      ! a * t**2 + b * t + c = 0
      IF (4 * a * c > b**2) THEN
         RETURN
      END IF
      ! if solution would divide by zero
      IF (a < tol) THEN
         ! no solution exists
         RETURN
      END IF
      tNear = -b / 2 / a - SQRT(b**2 - 4 * a * c) / 2 / a
      tFar = -b / 2 / a + SQRT(b**2 - 4 * a * c) / 2 / a
      ! if the intersection occurs only at one point
      IF (ABS(tNear - tFar) < tol .AND. tNear > 0) THEN
         ! get vector from the first point on the circle to the point of intersection on the circle
         vec2 = x + tNear * ray - x2
         ! check if the intersection occurs at the arc's endpoints
         vec2Test1 = mag(vec2 + x2 - x3)
         vec2Test2 = mag(vec2 + x2 - x1)
         ! get the normal of the cross product of vec1 with vec2
         n2 = crossProduct(vec2,vec1)
         magn2 = mag(n2)
         n2 = n2 / magn2
         ! if the normal is in the same direction as the normal to the circle
         IF (mag(n1 - n2) < tol .OR. vec2Test1 < tol2 .OR. vec2Test2 < tol2) THEN
            ! then the point of intersection is on the same side as the two points forming the circular arc
            !    and the intersection is valid
            isectCircle = isectCircle + 1
         END IF
         RETURN
      END IF
      ! if the near intersection occurs in the positive direction
      IF (tNear > 0) THEN
         ! get vector from the first point on the circle to the point of intersection on the circle
         vec2 = x + tNear * ray - x2
         ! check if the intersection occurs at the arc's endpoints
         vec2Test1 = mag(vec2 + x2 - x3)
         vec2Test2 = mag(vec2 + x2 - x1)
         ! get the normal of the cross product of vec1 with vec2
         n2 = crossProduct(vec2,vec1)
         magn2 = mag(n2)
         n2 = n2 / magn2
         ! if the normal is in the same direction as the normal to the circle
         IF (mag(n1 - n2) < tol .OR. vec2Test1 < tol2 .OR. vec2Test2 < tol2) THEN
            ! then the point of intersection is on the same side as the two points forming the circular arc
            !    and the intersection is valid
            isectCircle = isectCircle + 1
         END IF
      END IF
      ! if the far intersection occurs in the positive direction
      IF (tFar > 0) THEN
         ! get vector from the first point on the circle to the point of intersection on the circle
         vec2 = x + tFar * ray - x2
         ! check if the intersection occurs at the arc's endpoints
         vec2Test1 = mag(vec2 + x2 - x3)
         vec2Test2 = mag(vec2 + x2 - x1)
         ! get the normal of the cross product of vec1 with vec2
         n2 = crossProduct(vec2,vec1)
         magn2 = mag(n2)
         n2 = n2 / magn2
         ! if the normal is in the same direction as the normal to the circle
         IF (mag(n1 - n2) < tol .OR. vec2Test1 < tol2 .OR. vec2Test2 < tol2) THEN
            ! then the point of intersection is on the same side as the two points forming the circular arc
            !    and the intersection is valid
            isectCircle = isectCircle + 1
         END IF
      END IF
   END FUNCTION isectCircle

   !-------------------------------------------------------------------------------------
   ! finds whether the ray from x intersects the line
   !------------Variables------------
   ! line is the two points forming the line
   ! x is the origin of the ray
   ! ray is the ray from x to the line
   FUNCTION isectLine(line,x,ray,s)
      REAL(pr), INTENT(IN) :: line(3,2)
      REAL(pr), INTENT(IN), DIMENSION(3,1) :: x,ray
      REAL(pr), INTENT(OUT) :: s
      INTEGER :: isectLine

      REAL(pr), DIMENSION(3,1) :: perpRay,l1,l2,vec1,vec2
      REAL(pr) :: s1,s2,t,dist1,magperpRay,magvec1,magvec2,tol

      ! initialize variables
      isectLine = 0
      tol = 1e-5
      s = 1e12
      ! get the parameter of the line corresponding to the perpendicular vector from the line to x
      s1 = getAxisT(x,line)
      ! get the parameter of the line corresponding to the perpendicular vector from the line to x + ray
      s2 = getAxisT(x+ray,line)
      ! get the point on the line corresponding to the perpendicular vector from the line to x
      l1 = line(:,2:2) + s1 * (line(:,1:1) - line(:,2:2))
      ! get the poine on the line corresponding to the perpendicular vector from the line to x + ray
      l2 = line(:,2:2) + s2 * (line(:,1:1) - line(:,2:2))
      ! get the projection of the ray onto the vector perpendicular to the line to determine how the ray moves toward
      !    the line
      perpRay = (l1 - x) - (l2 - x - ray)
      ! make sure the projected ray is traveling toward the line
      vec1 = perpRay
      magvec1 = mag(vec1)
      ! if the ray and line are parallel
      IF (magvec1 < tol) THEN
         ! then the ray never intersects the line
         RETURN
      END IF
      vec1 = vec1 / magvec1
      vec2 = l1 - x
      magvec2 = mag(vec2)
      vec2 = vec2 / magvec2
      ! if the vectors are in opposite directions
      IF (mag(vec1 - vec2) > tol) THEN
         ! the ray never intersects the line
         RETURN
      END IF
      ! get the distances from x to the line and x + ray to the line to determine
      !    whether the ray is traveling to or away from the line
      dist1 = SQRT(DOT_PRODUCT(l1(:,1) - x(:,1),l1(:,1) - x(:,1)))
      ! otherwise, determine where it intersects by solving the following equation
      ! l1 = x + perpRay * t
      !    = x + perpRay / mag(perpRay) * dist1
      magperpRay = mag(perpRay)
      t = dist1 / magperpRay
      ! make sure the intersection point lies along the line
      !    the point on the line is described by l1 + t * (l2 - l1)
      !    the line is described by line(:,2:2) + s * (line(:,1:1) - line(:,2:2))
      !    substitution of the above two equations yields   (0.0 <= s <= 1.0)
      s = s1 + t * (s2 - s1)
      IF (s < 0.0 .OR. 1.0 < s) THEN
         RETURN
      END IF
      isectLine = isectLine + 1
   END FUNCTION isectLine

   !-------------------------------------------------------------------------------------
   ! finds whether the ray from x intersects the line
   !------------Variables------------
   ! i is the index of the spline corresponding to the B-Spline plane
   ! splineBreaks is the number of knot sequences
   ! x is the origin of the ray
   ! ray is the ray from x to the line
   FUNCTION isectSpline(i,splineBreaks,x,ray,n1)
      REAL(pr), INTENT(IN), DIMENSION(3,1) :: x,ray,n1
      INTEGER, INTENT(IN) :: i,splineBreaks
      INTEGER :: isectSpline

      REAL(pr) :: s1,s2,coeff(3,4),a(1,4),roots(3,1),sVec(4,1),tol
      REAL(pr) :: vec1(3,1),n2(3,1),x1(3,1),magvec1
      COMPLEX(pr) :: y1,y2,y3,Q,C,imag
      INTEGER :: j
      ! initialize variables
      isectSpline = 0
      tol = 1e-5
      imag = CMPLX(0.0,1.0)
      DO j = 1,splineBreaks
         ! make sure any roots not found are greater than s2
         roots = 1e12
         ! get the min and max values of the b-spline parameter
         s1 = NURBSSplines((i-1)*3+1,(j-1)*6+1)
         s2 = NURBSSplines((i-1)*3+1,(j-1)*6+2)
         ! a * s**3 + b * s**2 + c * s + d = (x,y,z)
         !    (x,y,z) is the point on the spline curve; a, b, c, and d are (3,1) vectors
         !    a * s**3 + b * s**2 + c * s + d - x = (x,y,z) - x
         !    find where the vector from the point to the curve is perpendicular to the ray
         coeff(1,1:4) = NURBSSplines((i-1)*3+1,(j-1)*6+3:(j-1)*6+6)
         coeff(2,1:4) = NURBSSplines((i-1)*3+2,(j-1)*6+3:(j-1)*6+6)
         coeff(3,1:4) = NURBSSplines((i-1)*3+3,(j-1)*6+3:(j-1)*6+6)
         coeff(:,4:4) = coeff(:,4:4) - x
         ! DOT_PRODUCT((x,y,z) - x,ray) = 0.0
         a = MATMUL(TRANSPOSE(ray),coeff)
         ! a' * s**3 + b' * s**2 + c' * s + d' = 0.0, a' = a(1,1), b' = a(1,2), c' = a(1,3), d' = a(1,4)
         ! if the first two coefficients are negligible
         IF (ABS(a(1,1)) < tol .AND. ABS(a(1,2)) < tol) THEN
            ! the equation can be treated as linear, c' * s + d' = 0.0
            roots(1,1) = -a(1,4) / a(1,3)
         ! if the first coefficient is negligible
         ELSE IF (ABS(a(1,1)) < tol) THEN
            ! the equation can be treated as a quadratic, b' * s**2 + c' * s + d' = 0.0
            y1 = -a(1,3) / 2 / a(1,2) - &
                &SQRT(CMPLX(a(1,3)**2 - 4 * a(1,2) * a(1,4)) / 2 / a(1,2))
            y2 = -a(1,3) / 2 / a(1,2) + &
                &SQRT(CMPLX(a(1,3)**2 - 4 * a(1,2) * a(1,4)) / 2 / a(1,2))
            ! If the imaginary part of the root is negligible
            IF (ABS(AIMAG(y1)) < tol) THEN
               ! the real part of the root can be taken as a root of the equation
               roots(1,1) = REAL(y1)
            END IF
            ! If the imaginary part of the root is negligible
            IF (ABS(AIMAG(y2)) < tol) THEN
               ! the real part of the root can be taken as a root of the equation
               roots(2,1) = REAL(y2)
            END IF
         ELSE
            ! use the general formula for cubic roots, a' * s**3 + b' * s**2 + c' * s + d' = 0.0
            ! Q = SQRT((2 * b'**3 - 9 * a' * b' * c' + 27 * a'**2 * d')**2 - 4 * (b'**2 - 3 * a' * c')**3)
            ! C = (0.5 * (-Q + 2 * b'**3 - 9 * a' * b' * c' + 27 * a'**2 * d'))**(1.0/3.0)
            ! x1 = -b' / 3 / a' - C / 3 / a' - (b'**2 - 3 * a' * c') / 3 / a' / C
            ! x2,3 = -b' / 3 / a' + C * (1 +/- SQRT(-1.0) * SQRT(3)) / 6 / a' + (1 -/+ SQRT(-1.0) * SQRT(3)) * (b'**2 - 3 * a' * c') / 6 / a' / C
            !
            ! if (b'**2 - 3 * a' * c' == 0) and Q /= 0, make sure C /= 0
            !    use above equations
            !
            ! if (b'**2 - 3 * a' * c' == 0) and Q == 0
            !    x1 = x2 = x3 = -b' / 3 / a'
            ! if Q == 0 and b'**2 - 3 * a' * c' /= 0
            !    x1 = x2 = (b' * c' - 9 * a' * d') / 2 / (3 * a' * c' - b'**2)
            !    x3 = (9 * a'**2 * d' - 4 * a' * b' * c' + b'**3) / a' / (3 * a' * c' - b'**2)
            Q = SQRT(CMPLX((2 * a(1,2)**3 - 9 * a(1,1) * a(1,2) * a(1,3) + &
                    &27 * a(1,1)**2 * a(1,4))**2 - 4 * (a(1,2)**2 - &
                    &3 * a(1,1) * a(1,3))**3))
            IF (ABS(a(1,2)**2 - 3 * a(1,1) * a(1,3)) < tol) THEN
               IF (ABS(REAL(Q) + AIMAG(Q)) > tol) THEN
                  C = (0.5 * (Q + 2 * a(1,2)**3 - 9 * a(1,1) * a(1,2) * a(1,3) &
                      &+ 27 * a(1,1)**2 * a(1,4)))**(1.0/3.0)
                  IF (ABS(REAL(C) + AIMAG(C)) < tol) THEN
                     C = (0.5 * (-Q + 2 * a(1,2)**3 - 9 * a(1,1) * a(1,2) * &
                         &a(1,3) + 27 * a(1,1)**2 * a(1,4)))**(1.0/3.0)
                     IF (ABS(REAL(C) + AIMAG(C)) < tol) THEN
                        PRINT *,'******ERROR in cubic root logic******'
                     END IF
                  END IF
                  y1 = -a(1,2) / 3 / a(1,1) - C / 3 / a(1,1) - (a(1,2)**2 - &
                      &3 * a(1,1) * a(1,3)) / 3 / a(1,1) / C
                  y2 = -a(1,2) / 3 / a(1,1) + C * (1 + imag * &
                      &SQRT(3.0)) / 6 / a(1,1) + (1 - imag * SQRT(3.0)) &
                      &* (a(1,2)**2 - 3 * a(1,1) * a(1,3)) / 6 / a(1,1) / C
                  y3 = -a(1,2) / 3 / a(1,1) + C * (1 - imag * &
                      &SQRT(3.0)) / 6 / a(1,1) + (1 + imag * SQRT(3.0)) &
                      &* (a(1,2)**2 - 3 * a(1,1) * a(1,3)) / 6 / a(1,1) / C
                  ! If the imaginary part of the root is negligible
                  IF (ABS(AIMAG(y1)) < tol) THEN
                     ! the real part of the root can be taken as a root of the equation
                     roots(1,1) = REAL(y1)
                  END IF
                  ! If the imaginary part of the root is negligible
                  IF (ABS(AIMAG(y2)) < tol) THEN
                     ! the real part of the root can be taken as a root of the equation
                     roots(2,1) = REAL(y2)
                  END IF
                  ! If the imaginary part of the root is negligible
                  IF (ABS(AIMAG(y3)) < tol) THEN
                     ! the real part of the root can be taken as a root of the equation
                     roots(3,1) = REAL(y3)
                  END IF
               ELSE IF (ABS(REAL(Q) + AIMAG(Q)) < tol) THEN
                  roots(1,1) = -a(1,2) / 3 / a(1,1)
               END IF
            ELSE IF(ABS(REAL(Q) + AIMAG(Q)) < tol) THEN
               y1 = (a(1,2) * a(1,3) - 9 * a(1,1) * a(1,4)) / 2 / &
                   &(3 * a(1,1) * a(1,3) - a(1,2)**2)
               y2 = (9 * a(1,1)**2 * a(1,4) - 4 * a(1,1) * a(1,2) * a(1,3) + &
                   &a(1,2)**3) / a(1,1) / (3 * a(1,1) * a(1,3) - a(1,2)**2)
               ! If the imaginary part of the root is negligible
               IF (ABS(AIMAG(y1)) < tol) THEN
                  ! the real part of the root can be taken as a root of the equation
                  roots(1,1) = REAL(y1)
               END IF
               ! If the imaginary part of the root is negligible
               IF (ABS(AIMAG(y2)) < tol) THEN
                  ! the real part of the root can be taken as a root of the equation
                  roots(2,1) = REAL(y2)
               END IF
            ELSE
               C = (0.5 * (Q + 2 * a(1,2)**3 - 9 * a(1,1) * a(1,2) * a(1,3) &
                   &+ 27 * a(1,1)**2 * a(1,4)))**(1.0/3.0)
               y1 = -a(1,2) / 3 / a(1,1) - C / 3 / a(1,1) - (a(1,2)**2 - &
                   &3 * a(1,1) * a(1,3)) / 3 / a(1,1) / C
               y2 = -a(1,2) / 3 / a(1,1) + C * (1 + imag * &
                   &SQRT(3.0)) / 6 / a(1,1) + (1 - imag * SQRT(3.0)) &
                   &* (a(1,2)**2 - 3 * a(1,1) * a(1,3)) / 6 / a(1,1) / C
               y3 = -a(1,2) / 3 / a(1,1) + C * (1 - imag * &
                   &SQRT(3.0)) / 6 / a(1,1) + (1 + imag * SQRT(3.0)) &
                   &* (a(1,2)**2 - 3 * a(1,1) * a(1,3)) / 6 / a(1,1) / C
               ! If the imaginary part of the root is negligible
               IF (ABS(AIMAG(y1)) < tol) THEN
                  ! the real part of the root can be taken as a root of the equation
                  roots(1,1) = REAL(y1)
               END IF
               ! If the imaginary part of the root is negligible
               IF (ABS(AIMAG(y2)) < tol) THEN
                  ! the real part of the root can be taken as a root of the equation
                  roots(2,1) = REAL(y2)
               END IF
               ! If the imaginary part of the root is negligible
               IF (ABS(AIMAG(y3)) < tol) THEN
                  ! the real part of the root can be taken as a root of the equation
                  roots(3,1) = REAL(y3)
               END IF
            END IF
         END IF
         ! make sure the parameter is within the bounds being analyzed
         IF (s1 <= roots(1,1) .AND. roots(1,1) <= s2) THEN
            ! get the cubic values of the parameter
            sVec(1,1) = roots(1,1)**3
            sVec(2,1) = roots(1,1)**2
            sVec(3,1) = roots(1,1)
            sVec(4,1) = 1.0
            ! get the direction of the vector from the origin point to the curve
            x1 = MATMUL(coeff,sVec)
            vec1 = x1
            magvec1 = mag(vec1)
            vec1 = vec1 / magvec1
            ! see if the cross product of the ray and vector from the origin point to the curve
            !    generates the same normal as the normal defining the spline curve's plane
            n2 = crossProduct(ray,vec1)
            IF (mag(n1 - n2) < tol) THEN
               ! if the normal generated by the cross product is the same as the normal describing
               !    the plane, then increment the intersection with the spline curve
               isectSpline = isectSpline + 1
            END IF
         END IF
         ! make sure the parameter is within the bounds being analyzed
         IF (s1 <= roots(2,1) .AND. roots(2,1) <= s2) THEN
            ! get the cubic values of the parameter
            sVec(1,1) = roots(2,1)**3
            sVec(2,1) = roots(2,1)**2
            sVec(3,1) = roots(2,1)
            sVec(4,1) = 1.0
            ! get the direction of the vector from the origin point to the curve
            x1 = MATMUL(coeff,sVec)
            vec1 = x1
            magvec1 = mag(vec1)
            vec1 = vec1 / magvec1
            ! see if the cross product of the ray and vector from the origin point to the curve
            !    generates the same normal as the normal defining the spline curve's plane
            n2 = crossProduct(ray,vec1)
            IF (mag(n1 - n2) < tol) THEN
               ! if the normal generated by the cross product is the same as the normal describing
               !    the plane, then increment the intersection with the spline curve
               isectSpline = isectSpline + 1
            END IF
         END IF
         ! make sure the parameter is within the bounds being analyzed
         IF (s1 <= roots(3,1) .AND. roots(3,1) <= s2) THEN
            sVec(1,1) = roots(3,1)**3
            sVec(2,1) = roots(3,1)**2
            sVec(3,1) = roots(3,1)
            sVec(4,1) = 1.0
            ! get the direction of the vector from the origin point to the curve
            x1 = MATMUL(coeff,sVec)
            vec1 = x1
            magvec1 = mag(vec1)
            vec1 = vec1 / magvec1
            ! see if the cross product of the ray and vector from the origin point to the curve
            !    generates the same normal as the normal defining the spline curve's plane
            n2 = crossProduct(ray,vec1)
            IF (mag(n1 - n2) < tol) THEN
               ! if the normal generated by the cross product is the same as the normal describing
               !    the plane, then increment the intersection with the spline curve
               isectSpline = isectSpline + 1
            END IF
         END IF
      END DO
   END FUNCTION isectSpline

   !-------------------------------------------------------------------------------------
   ! finds whether a ray intersects the plane defined by the B-Spline surface
   !------------Variables------------
   ! i is the B-Spline surface index corresponding to which B-Spline surface data to use
   ! x is the point of origin
   ! ray is the ray from the point of origin that may intersect the surface
   FUNCTION isectPlane(i,x,ray)
      INTEGER, INTENT(IN) :: i
      REAL(pr), INTENT(IN), DIMENSION(3,1) :: x,ray
      LOGICAL :: isectPlane

      REAL(pr), DIMENSION(3,1) :: n1,ray1,ray2,xPlane,nCir,x1,x2,x3
      REAL(pr), DIMENSION(3,8) :: lPlane
      INTEGER :: j,k,cirS,cirE,linS,linE,splS,splE
      INTEGER :: pIsect(4),splineBreaks
      REAL(pr) :: d,magn1,magray,dotNormRay,s,t,l(3,2),tol
      ! initialize variables
      isectPlane = .FALSE.
      pIsect = 0
      tol = 1e-12
      ! starting circle index
      cirS = NURBSNCircles(i+1,1)
      ! ending circle index
      cirE = NURBSNCircles(i+1,2)
      ! starting line index
      linS = NURBSNLines(i+1,1)
      ! ending line index
      linE = NURBSNLines(i+1,2)
      ! starting spline index
      splS = NURBSNSplines(i+1,1)
      ! ending spline index
      splE = NURBSNSplines(i+1,2)
      ! get the four lines making up the plane's boundary
      lPlane(:,1) = NURBSPoints(i*3+1:i*3+3,1)
      lPlane(:,2) = NURBSPoints(i*3+1:i*3+3,2)
      lPlane(:,3) = NURBSPoints(i*3+1:i*3+3,2)
      lPlane(:,4) = NURBSPoints(i*3+1:i*3+3,4)
      lPlane(:,5) = NURBSPoints(i*3+1:i*3+3,4)
      lPlane(:,6) = NURBSPoints(i*3+1:i*3+3,3)
      lPlane(:,7) = NURBSPoints(i*3+1:i*3+3,3)
      lPlane(:,8) = NURBSPoints(i*3+1:i*3+3,1)
      ! get the plane's normal vector (equation of the plane)
      ! DOT_PRODUCT(n1,pointOnPlane) + d = 0
      n1 = crossProduct(lPlane(:,2:2) - lPlane(:,1:1),&
                       &lPlane(:,4:4) - lPlane(:,3:3))
      magn1 = mag(n1)
      n1 = n1 / magn1
      d = DOT_PRODUCT(-n1(:,1),lPlane(:,1))
      ! get point of intersection with plane
      !    DOT_PRODUCT(n1,x+t*ray) - d = 0
      dotNormRay = DOT_PRODUCT(n1(:,1),ray(:,1))
      ! If the ray is parallel to the plane and the point is not on the plane
      IF (ABS(dotNormRay) < tol .AND. &
      &ABS((DOT_PRODUCT(n1(:,1),x(:,1)) + d)) > tol) THEN
         ! the ray does not intersect the plane
         RETURN
      ELSE IF (ABS(dotNormRay) < tol .AND. &
      &ABS((DOT_PRODUCT(n1(:,1),x(:,1)) + d)) < tol) THEN
         ! the ray does intersect the plane
         t = 0.0
      ELSE
         t = -(DOT_PRODUCT(n1(:,1),x(:,1)) + d) / dotNormRay
      END IF
      IF (t < 0.0) THEN
         RETURN
      END IF
      xPlane = x + t * ray
      DO k = 0,1
         ! make rays in the directions of the lines forming the plane
         ray1 = lPlane(:,k*2+2:k*2+2) - lPlane(:,k*2+1:k*2+1)
         magray = mag(ray1)
         ray1 = ray1/magray
         ray2 = -ray1
         ! get circle data and circular arc intersections
         DO j = cirS,cirE
            nCir(1:3,1) = NURBSCircles(j,12:14)
            x1(1:3,1) = NURBSCircles(j,3:5)
            x2(1:3,1) = NURBSCircles(j,6:8)
            x3(1:3,1) = NURBSCircles(j,9:11)
            pIsect(k*2+1) = pIsect(k*2+1) + &
                            isectCircle(nCir,x1,x2,x3,xPlane,ray1)
            pIsect((k+1)*2) = pIsect((k+1)*2) + &
                            isectCircle(nCir,x1,x2,x3,xPlane,ray2)
         END DO
         ! get line data and line intersections
         DO j = linS,linE
            l(:,1) = NURBSLines(j,1:3)
            l(:,2) = NURBSLines(j,4:6)
            pIsect(k*2+1) = pIsect(k*2+1) + &
                            isectLine(l,xPlane,ray1,s)
            pIsect((k+1)*2) = pIsect((k+1)*2) + &
                            isectLine(l,xPlane,ray2,s)
         END DO
         ! get spline data and spline intersections
         DO j = splS,splE
            splineBreaks = NURBSSplinesBreaks(j,1)
            pIsect(3-k) = pIsect(3-k) + &
                            isectSpline(j,splineBreaks,xPlane,ray1,n1)
            pIsect(4-k*3) = pIsect(4-k*3) + &
                            isectSpline(j,splineBreaks,xPlane,ray2,n1)
         END DO
      END DO
         ! find out whether the point is inside the two-dimensional curves
      IF (MOD(pIsect(1),2) + MOD(pIsect(2),2) + MOD(pIsect(3),2) + &
      MOD(pIsect(4),2) > 2) THEN
         IF (isectPlane) PRINT *,'here'
         isectPlane = .TRUE.
      END IF
   END FUNCTION isectPlane


   !-------------------------------------------------------------------------------------
   ! checks if ray intersects B-Spline surface
   !------------Variables------------
   ! x is the point of origin of the ray
   ! ray is the ray vector to intersect the surface
   ! j is the B-Spline surface index corresponding to which B-Spline surface data to use
   FUNCTION isectNURBS(x,ray,j)
      INTEGER :: isectNURBS
      REAL(pr), INTENT(IN) :: x(3,1),ray(3,1)
      INTEGER, INTENT(IN) :: j

      INTEGER :: i,K1,K2,C
      REAL(pr) :: xMin(3,1),xMax(3,1),tol,n0(3,1),n1(3,1),Q(3,1)
      REAL(pr) :: s1,s2,t1,t2,sguess,tguess,d0,d1,tol2,U(2),V(2)
      REAL(pr) :: dotVec,vec1(3,1),vec2(3,1),diff,oldQ(3,1)

      ! initialize variables
      K1 = NURBSIndices(j+1,1)
      K2 = NURBSIndices(j+1,2)
      ! C weights, C control points
      C = (1 + K1) * (1 + K2)
      isectNURBS = 0
      tol = 1e-5
      tol2 = 5e-3
      oldQ = 1e12
      U = NURBSU(j+1,1:2)
      V = NURBSV(j+1,1:2)
      ! find out whether spline surface forms a plane
      vec1 = NURBSPoints(j*3+1:j*3+3,2:2) - NURBSPoints(j*3+1:j*3+3,1:1)
      vec2 = NURBSPoints(j*3+1:j*3+3,4:4) - NURBSPoints(j*3+1:j*3+3,2:2)
      dotVec = ABS(DOT_PRODUCT(vec1(:,1),vec2(:,1)))
      ! if the two edges are perpendicular, the dot product will be zero
      !    and the plane will be formed by only four points (the four points forming a rectangular plane)
      IF (dotVec < tol .AND. C == 4) THEN
         IF (isectPlane(j,x,ray)) THEN
            isectNURBS = isectNURBS + 1
         END IF
         RETURN
      END IF

      ! get planes intersecting to form the ray
      n0 = 0.0
      n1 = 0.0
      IF (ABS(ray(1,1)) > ABS(ray(2,1)) .AND. ABS(ray(1,1)) > ABS(ray(3,1))) &
      &THEN
         n0(1,1) = ray(2,1)
         n0(2,1) = -ray(1,1)
      ELSE
         n0(2,1) = ray(3,1)
         n0(3,1) = -ray(2,1)
      END IF
      n1 = crossProduct(n0,ray)
      ! both plane equations must contain x
      d0 = DOT_PRODUCT(-n0(:,1),x(:,1))
      d1 = DOT_PRODUCT(-n1(:,1),x(:,1))
      ! get patch bounding box
      DO i=0,eIndex(j+1)-1
         xMin = NURBSPExtrema(j*3+1:j*3+3,i*2+1:i*2+1)
         xMax = NURBSPExtrema(j*3+1:j*3+3,i*2+2:i*2+2)
         IF (mag(xMax - xMin) < tol) THEN
            RETURN
         END IF
         ! get knot values for patches
         s1 = NURBSPatch(j*2+1,i*2+1)
         s2 = NURBSPatch(j*2+1,i*2+2)
         t1 = NURBSPatch(j*2+2,i*2+1)
         t2 = NURBSPatch(j*2+2,i*2+2)
         ! provide a guess to Broyden's method
         sguess = s1 + 0.50 * (s2 - s1)
         tguess = t1 + 0.50 * (t2 - t1)
         IF (isectTest(x,ray,xMin,xMax)) THEN
            IF (Newton(j,n0,n1,d0,d1,sguess,tguess,x,ray,Q,U,V,s1,s2,t1,t2)) THEN
            ! IF (Broyden(j,n0,n1,d0,d1,sguess,tguess,x,ray,Q,U,V,s1,s2,t1,t2)) THEN
               diff = mag(Q - oldQ)
               IF (diff > tol2) THEN
!$$$$$$                   IF (analyzing) PRINT *,diff
                  isectNURBS = isectNURBS + 1
               END IF
               oldQ = Q
            END IF
         END IF
      END DO
   END FUNCTION isectNURBS
   !-------------------------------------------------------------------------------------
   ! Initializes the geometry and determines each surface's bounding box
   !------------Variables------------
   ! dimen = 2 for 2-dimensional geometry, 3 for 3-dimensional geometry
   ! dimen is not used any longer
   ! file_local is the name of the .igs file
   SUBROUTINE initgeom(dimen,fileLocal)

      INTEGER, INTENT(IN) :: dimen
      CHARACTER (LEN=256), INTENT(IN) :: fileLocal

      IF (analyzing .AND. dimen<2) PRINT *,'dimen is less than 2'

      IF (analyzing2) PRINT *,'reading igs file into memory'
      CALL readIntoMemory ('geometry'//slash//TRIM(fileLocal))

      IF (analyzing2) PRINT *,'allocating memory'
      CALL memoryAllocation()  !error on this when running with modified db trees, allocates NURBS* and revolved*

      IF (analyzing2) PRINT *,'getting surfaces'
      CALL getSurfaces()

      IF (analyzing2) PRINT *,'writing surface data and getting bounding boxes'
      CALL writeSurfaces()

      IF (analyzing2) PRINT *,'writing 2D data'
      CALL writeCurves()

   END SUBROUTINE initgeom

   SUBROUTINE tester()

      CALL getSurfaces()

   END SUBROUTINE tester

   !-------------------------------------------------------------------------------------
   ! runs program to find whether points are inside or outside each surface
   !------------Variables------------
   ! xp(3,n) is the array of points in x,y,z space
   ! xp(1,:) is the array of points on the x-axis
   ! xp(2,:) is the array of points on the y-axis
   ! xp(3,:) is the array of points on the z-axis
   ! dimen = 2 for 2-dimensional geometry, 3 for 3-dimensional geometry
   ! dimen is not used any longer
   ! n is the number of points generated by the program that is
   ! using threedobject
   ! lp=.FALSE. -> point out, lp=.TRUE. -> point in
   ! wrt_flag = .FALSE. does not write points to the file
   ! fileName is the name of the .igs file
   SUBROUTINE surface(xp,dimen,n,lp,wrtFlag,fileName)

      INTEGER, INTENT(IN) :: dimen,n
      LOGICAL, DIMENSION(n), INTENT(INOUT) :: lp
      LOGICAL, INTENT(IN) :: wrtFlag
      CHARACTER (LEN=256), INTENT(IN) :: fileName
      REAL(pr), INTENT(INOUT), DIMENSION(3,n) :: xp

      ! Make sure the geometry is at least 2-Dimensional
      IF (analyzing .AND. dimen<2) PRINT *,'dimen is less than 2'
  !    IF (analyzing2) PRINT *,fileName

 !     IF (analyzing2) PRINT *,'determining whether points are inside or outside'
      CALL getInOutData(xp,n,lp,wrtFlag)
   END SUBROUTINE surface



   !-------------------------------------------------------------------------------------
   ! Checks whether points are inside or outside each surface
   !------------Variables------------
   ! x(3,n) is the array of points in x,y,z space
   ! x(1,:) is the array of points on the x-axis
   ! x(2,:) is the array of points on the y-axis
   ! x(3,:) is the array of points on the z-axis
   ! n is the number of points generated by the program that is
   !    using threedobject
   ! lp=.FALSE. -> point out, lp=.TRUE. -> point in
   ! wrt_flag = .FALSE. does not write points to the file
   SUBROUTINE getInOutData(x,n,lp,wrtFlag)

      INTEGER, INTENT(IN) :: n
      LOGICAL, DIMENSION(n), INTENT(INOUT) :: lp
      LOGICAL, INTENT(IN) :: wrtFlag
      REAL(pr), INTENT(INOUT), DIMENSION(3,n) :: x

      INTEGER i,j,k,iter,maxIter,numProblems
      REAL(pr) :: maxDiff(3,1),minDiff(3,1),xMax(3,1),xMin(3,1),ray(3,1)
      REAL(pr) :: randNum,perturbation,xStore(3,1)
      ! Initialize variables
      maxIter = 30
      numProblems = 0
      perturbation = 1e-6

      IF(wrtFlag) THEN
         OPEN (UNIT = 2195,FILE='geometry' //slash // 'inorout3d.txt',&
              &FORM='formatted',STATUS='replace')
         OPEN (UNIT = 2196,FILE='geometry' //slash // 'inorout3dtie.txt',&
              &FORM='formatted',STATUS='replace')
      END IF
      DO i=1,n
!       DO i=645041,645041
         minDiff = extrema(:,1:1) - x(:,i:i)
         maxDiff = x(:,i:i) - extrema(:,2:2)
         isect = 0
         IF (MAX(maxDiff(1,1),maxDiff(2,1),maxDiff(3,1)) <= 0.0 &
         &.AND. MAX(minDiff(1,1),minDiff(2,1),minDiff(3,1)) <= 0.0) THEN
            DO k=1,3
               ray = 0.0
               ray(k,1) = 1.0
               DO j=0,revolvedLinesIndex - 1
                  xMin = revolvedLineExtrema(j*3+1:j*3+3,1:1)
                  xMax = revolvedLineExtrema(j*3+1:j*3+3,2:2)
                  IF (isectTest(x(:,i:i),ray,xMin,xMax)) THEN
                     isect((k-1)*2+1) = isect((k-1)*2+1) &
                                      &+ isectRevolvedLine(x(:,i:i),ray,j)
                  END IF
                  IF (isectTest(x(:,i:i),-ray,xMin,xMax)) THEN
                     isect(k*2) = isect(k*2) &
                                      &+ isectRevolvedLine(x(:,i:i),-ray,j)
                  END IF
               END DO
               DO j=0,revolvedCirclesIndex - 1
                  xMin = revolvedCircleExtrema(j*3+1:j*3+3,1:1)
                  xMax = revolvedCircleExtrema(j*3+1:j*3+3,2:2)
                  IF (isectTest(x(:,i:i),ray,xMin,xMax)) THEN
                     isect((k-1)*2+1) = isect((k-1)*2+1) &
                                      &+ isectRevolvedCircle(x(:,i:i),ray,j)
                  END IF
                  IF (isectTest(x(:,i:i),-ray,xMin,xMax)) THEN
                     isect(k*2) = isect(k*2) &
                                      &+ isectRevolvedCircle(x(:,i:i),-ray,j)
                  END IF
               END DO
               DO j=0,NURBSIndex - 1
                  xMin = NURBSExtrema(j*3+1:j*3+3,1:1)
                  xMax = NURBSExtrema(j*3+1:j*3+3,2:2)
                  IF (isectTest(x(:,i:i),ray,xMin,xMax)) THEN
                     isect((k-1)*2+1) = isect((k-1)*2+1) &
                                      &+ isectNURBS(x(:,i:i),ray,j)
                  END IF
                  IF (isectTest(x(:,i:i),-ray,xMin,xMax)) THEN
                     isect(k*2) = isect(k*2) &
                                      &+ isectNURBS(x(:,i:i),-ray,j)
                  END IF
               END DO
            END DO
         END IF
         iter = 0
         DO WHILE (MOD(isect(1),2) + MOD(isect(2),2) + MOD(isect(3),2) + &
         &MOD(isect(4),2) + MOD(isect(5),2) + MOD(isect(6),2) > tieMin .AND. &
         &MOD(isect(1),2) + MOD(isect(2),2) + MOD(isect(3),2) + &
         &MOD(isect(4),2) + MOD(isect(5),2) + MOD(isect(6),2) < tieMax .AND. &
         &iter < maxIter)
            IF (wrtFlag .AND. iter == 0) THEN
               WRITE (UNIT = 2196,FMT = 102) x(1,i),x(2,i),x(3,i),isect(1),&
                                            &isect(2),isect(3),isect(4),isect(5),&
                                            &isect(6),i
            END IF
            iter = iter + 1
            IF (analyzing2 .AND. iter > 1) THEN
               numProblems = numProblems + 1
               !IF (MOD(numProblems,100) == 0) THEN
               !   PRINT *,'number of inconclusive points = ',numProblems
               !END IF
            END IF
            IF (analyzing) THEN
               PRINT *,'problem at ',i,' 1035'
               PRINT *,'isect = ',isect(1:6)
               PRINT *,'x = ',x(:,i)
            END IF
            isect = 0
            xStore(1,1:1) = x(1,i:i)
            xStore(2,1:1) = x(2,i:i)
            xStore(3,1:1) = x(3,i:i)
            CALL RANDOM_NUMBER(randNum)
            xStore(1,1:1) = x(1,i:i) + 2.0 * (randNum - 0.5) * perturbation
            CALL RANDOM_NUMBER(randNum)
            xStore(2,1:1) = x(2,i:i) + 2.0 * (randNum - 0.5) * perturbation
            CALL RANDOM_NUMBER(randNum)
            xStore(3,1:1) = x(3,i:i) + 2.0 * (randNum - 0.5) * perturbation
!             PRINT *,xStore(1,1),x(1,i:i),xStore(2,1),x(2,i:i),xStore(3,1),x(3,i:i)
            DO k=1,3
               ray = 0.0
               ray(k,1) = 1.0
               DO j=0,revolvedLinesIndex - 1
                  xMin = revolvedLineExtrema(j*3+1:j*3+3,1:1)
                  xMax = revolvedLineExtrema(j*3+1:j*3+3,2:2)
                  IF (isectTest(x(:,i:i),ray,xMin,xMax)) THEN
                     isect((k-1)*2+1) = isect((k-1)*2+1) &
                                      &+ isectRevolvedLine(xStore(:,1:1),ray,j)
                  END IF
                  IF (isectTest(x(:,i:i),-ray,xMin,xMax)) THEN
                     isect(k*2) = isect(k*2) &
                                      &+ isectRevolvedLine(xStore(:,1:1),-ray,j)
                  END IF
               END DO
               DO j=0,revolvedCirclesIndex - 1
                  xMin = revolvedCircleExtrema(j*3+1:j*3+3,1:1)
                  xMax = revolvedCircleExtrema(j*3+1:j*3+3,2:2)
                  IF (isectTest(x(:,i:i),ray,xMin,xMax)) THEN
                     isect((k-1)*2+1) = isect((k-1)*2+1) &
                                      &+ isectRevolvedCircle(xStore(:,1:1),ray,j)
                  END IF
                  IF (isectTest(x(:,i:i),-ray,xMin,xMax)) THEN
                     isect(k*2) = isect(k*2) &
                                      &+ isectRevolvedCircle(xStore(:,1:1),-ray,j)
                  END IF
               END DO
               DO j=0,NURBSIndex - 1
                  xMin = NURBSExtrema(j*3+1:j*3+3,1:1)
                  xMax = NURBSExtrema(j*3+1:j*3+3,2:2)
                  IF (isectTest(x(:,i:i),ray,xMin,xMax)) THEN
                     isect((k-1)*2+1) = isect((k-1)*2+1) &
                                      &+ isectNURBS(xStore(:,1:1),ray,j)
                  END IF
                  IF (isectTest(x(:,i:i),-ray,xMin,xMax)) THEN
                     isect(k*2) = isect(k*2) &
                                      &+ isectNURBS(xStore(:,1:1),-ray,j)
                  END IF
               END DO
            END DO
         END DO !WHILE
         IF (MOD(isect(1),2) + MOD(isect(2),2) + MOD(isect(3),2) + &
         &MOD(isect(4),2) + MOD(isect(5),2) + MOD(isect(6),2) >= minInside) THEN
            lp(i) = .TRUE.
         END IF
         IF (lp(i) .AND. wrtFlag) THEN
            WRITE (UNIT = 2195,FMT = 102) x(1,i),x(2,i),x(3,i),isect(1),&
                                         &isect(2),isect(3),isect(4),isect(5),&
                                         &isect(6),i
         END IF
      END DO

      !IF (analyzing2) PRINT *,'number of inconclusive points = ',numProblems

      IF(wrtFlag)  THEN 
         CLOSE (UNIT = 2195)
         CLOSE (UNIT = 2196)
      END IF
      102 FORMAT (F16.10,1x,F16.10,1x,F16.10,1x,I10,1x,I10,1x,I10,1x,I10,1x,&
                 &I10,1x,I10,1x,I10)
   END SUBROUTINE getInOutData



   !-------------------------------------------------------------------------------------
   ! reads through the .igs file for entity 142 (curve on surface entity)
   SUBROUTINE getSurfaces()

      CHARACTER :: r142*80,rSurface*80
      INTEGER :: i,j,i10,j10,count,k,surfaceDE,curveDE,i2

      ! To initialize the variable before using it in the r142 do loop
      j10 = 0

      ! read through the .igs file for 142 entities (curve on surface entities)
e142: DO i=1,parLinesSize
         r142=parLines(i)%l
         i2 = i
         IF (r142(1:3)=='142') THEN
            count = 0
e142c:      DO
               DO j=1,80
                  IF (r142(j:j)==',' .OR. r142(j:j)==';') THEN
                     i10 = j10
                     count = count + 1
                     j10 = j

                     ! Get the Parameter Data Entry (DE) line for the surface type that the curve lies on
                     IF (count==3) THEN
                        READ(r142(i10+1:j10-1),*) k
                        rSurface = parLines(table(k))%l
                        surfaceDE = table(k)
                     END IF

                     ! Get the Parameter Data Entry (DE) line for the surface type that the curve lies on
                     IF (count==5) THEN
                        READ(r142(i10+1:j10-1),*) k
                        curveDE = table(k)
                        EXIT e142c
                     END IF
                  END IF
               END DO
               i2 = i2 + 1
               r142=parLines(i)%l
               j10 = 0
            END DO e142c

            ! If the surface is a revolved surface, revolve the surface to obtain its geometry
            IF (rSurface(1:3)=='120') THEN
               CALL revolveSurface(surfaceDE,curveDE)
            END IF
            IF(rSurface(1:3)=='128') THEN
               CALL bSplineSurface(surfaceDE,curveDE)
            END IF
         END IF
      END DO e142

   END SUBROUTINE getSurfaces

   !-------------------------------------------------------------------------------------
   ! writes surface outline data to the surface.txt file and gets bounding boxes
   SUBROUTINE writeSurfaces()
      INTEGER :: i,j,i2,j2,k,l,m,surfSize
      REAL(pr) :: axisVec(3,1),magAxisVec,x(3,1),xNew(3,1),center(3,1)
      REAL(pr) :: normVec(3,1),t1,t2,s,t,tol,sStepSize,tStepSize,num
      REAL(pr) :: theta,x1(3,3),axisLine(3,2),line(3,1),magRadial,s1,s2
      REAL(pr), ALLOCATABLE :: si(:),ti(:)
      INTEGER :: K1,K2,M1,M2,N1,N2,A,B

      surfSize = 100
      tol = 1e-12
      ! trial and error value for a relatively good representation of the surface
      ! decrease for more accurate representation of the surface (slower)
      num = 25.0 * REAL(flatten + 1)

      extrema(:,1) = 1e12
      extrema(:,2) = -1e12
      revolvedLineExtrema(:,1) = 1e12
      revolvedLineExtrema(:,2) = -1e12
      revolvedCircleExtrema(:,1) = 1e12
      revolvedCircleExtrema(:,2) = -1e12
      NURBSExtrema(:,1) = 1e12
      NURBSExtrema(:,2) = -1e12

      IF(myWrtFlag) OPEN (UNIT = 2193,FILE='geometry' //slash // 'surface.txt',&
           &FORM='formatted',STATUS='replace')
      ! parametric equation of revolved line surface
      DO i=0,revolvedLinesIndex - 1
         ! axis of rotation
         axisLine = revolvedLineAxes(i * 3 + 1 : i * 3 + 3,1:2)
         ! midpoint of the line being revolved
         line = revolvedLines(i * 3 + 1 : i * 3 + 3,1:1) + &
               &0.5 * &
               &(revolvedLines(i * 3 + 1 : i * 3 + 3,2:2) - &
               & revolvedLines(i * 3 + 1 : i * 3 + 3,1:1))
         ! find the point on the axis of rotation corresponding to the centerpoint of the line being revolved
         !    the vector from the axis to the centerpoint of the line is perpendicular to the axis
         t = getAxisT(line,axisLine)
         ! the radial (perpendicular) vector from the axis of rotation to the centerpoint of the line
         revolvedLineRadial(i*3+1:i*3+3,1:1) = line(:,1:1) - (axisLine(:,2:2) &
                                              &+ t * (axisLine(:,1:1) - &
                                              &axisLine(:,2:2)))
         magRadial = mag(revolvedLineRadial(i*3+1:i*3+3,1:1))
         ! the unit radial vector
         revolvedLineRadial(i*3+1:i*3+3,1:1) = revolvedLineRadial(i*3+1:i*3+3,&
                                                                  &1:1) / magRadial
         axisVec = revolvedLineAxes(i * 3 + 1 : i * 3 + 3,1:1) - &
                  &revolvedLineAxes(i * 3 + 1 : i * 3 + 3,2:2)
         magAxisVec = mag(axisVec)
         ! unit vector for the axis of rotation
         axisVec = axisVec / magAxisVec
         center = revolvedLineAxes(i * 3 + 1 : i * 3 + 3,2:2)
         DO j = 0,surfSize
            ! parameterize line to be revolved
            x = revolvedLines(i * 3 + 1 : i * 3 + 3,1:1) + &
               &REAL(j) / REAL(surfSize) * &
               &(revolvedLines(i * 3 + 1 : i * 3 + 3,2:2) - &
               & revolvedLines(i * 3 + 1 : i * 3 + 3,1:1))
            DO k = 0,surfSize
               ! parameterize revolution
               theta = revolvedLineAngles(i+1,1) + REAL(k) / REAL(surfSize) *&
                     &(revolvedLineAngles(i+1,2) - revolvedLineAngles(i+1,1))
               CALL getArbitraryRotation(axisVec,center,theta,x,xNew)
               IF (myWrtFlag) WRITE (UNIT = 2193,FMT = 100) xNew(1,1), &
                             &xNew(2,1), xNew(3,1),revolvedLineAngles(i+1,1),&
                             &revolvedLineAngles(i+1,2)
               ! get bounding box for set of surfaces
               IF(xNew(1,1) < extrema(1,1)) THEN
                  extrema(1,1) = xNew(1,1)
               END IF
               IF(xNew(2,1) < extrema(2,1)) THEN
                  extrema(2,1) = xNew(2,1)
               END IF
               IF(xNew(3,1) < extrema(3,1)) THEN
                  extrema(3,1) = xNew(3,1)
               END IF
               IF(xNew(1,1) > extrema(1,2)) THEN
                  extrema(1,2) = xNew(1,1)
               END IF
               IF(xNew(2,1) > extrema(2,2)) THEN
                  extrema(2,2) = xNew(2,1)
               END IF
               IF(xNew(3,1) > extrema(3,2)) THEN
                  extrema(3,2) = xNew(3,1)
               END IF
               ! bounding box for each surface of revolution
               IF(xNew(1,1) < revolvedLineExtrema(i*3+1,1)) THEN
                  revolvedLineExtrema(i*3+1,1) = xNew(1,1)
               END IF
               IF(xNew(2,1) < revolvedLineExtrema(i*3+2,1)) THEN
                  revolvedLineExtrema(i*3+2,1) = xNew(2,1)
               END IF
               IF(xNew(3,1) < revolvedLineExtrema(i*3+3,1)) THEN
                  revolvedLineExtrema(i*3+3,1) = xNew(3,1)
               END IF
               IF(xNew(1,1) > revolvedLineExtrema(i*3+1,2)) THEN
                  revolvedLineExtrema(i*3+1,2) = xNew(1,1)
               END IF
               IF(xNew(2,1) > revolvedLineExtrema(i*3+2,2)) THEN
                  revolvedLineExtrema(i*3+2,2) = xNew(2,1)
               END IF
               IF(xNew(3,1) > revolvedLineExtrema(i*3+3,2)) THEN
                  revolvedLineExtrema(i*3+3,2) = xNew(3,1)
               END IF
            END DO
         END DO
      END DO

      ! parametric equation of revolved circle surface
      DO i=0,revolvedCirclesIndex - 1
         axisVec = revolvedCircleAxes(i * 3 + 1 : i * 3 + 3,1:1) - &
                  &revolvedCircleAxes(i * 3 + 1 : i * 3 + 3,2:2)
         magAxisVec = mag(axisVec)
         ! unit vector for the axis of rotation
         axisVec = axisVec / magAxisVec
         center = revolvedCircleAxes(i * 3 + 1 : i * 3 + 3,2:2)
         t1 = revolvedCircles(i+1,1)
         t2 = revolvedCircles(i+1,2)
         x1(1:3,1) = revolvedCircles(i+1,3:5)
         x1(1:3,2) = revolvedCircles(i+1,6:8)
         x1(1:3,3) = revolvedCircles(i+1,9:11)
         normVec(1:3,1) = revolvedCircles(i+1,12:14)
         ! axis of rotation
         axisLine = revolvedCircleAxes(i * 3 + 1 : i * 3 + 3,1:2)
         ! midpoint of the circle being revolved
         theta = t1 + 0.5 * (t2 - t1)
         CALL getArbitraryRotation(normVec,x1(1:3,1:1),theta,x1(1:3,2:2),x)
         ! find the point on the axis of rotation corresponding to the centerpoint of the circle being revolved
         !    the vector from the axis to the centerpoint of the circle is perpendicular to the axis
         t = getAxisT(x,axisLine)
         ! the radial (perpendicular) vector from the axis of rotation to the centerpoint of the circle
         revolvedCircleRadial(i*3+1:i*3+3,1:1) = x(:,1:1) - (axisLine(:,2:2) &
                                              &+ t * (axisLine(:,1:1) - &
                                              &axisLine(:,2:2)))
         magRadial = mag(revolvedCircleRadial(i*3+1:i*3+3,1:1))
         ! the unit radial vector
         revolvedCircleRadial(i*3+1:i*3+3,1:1) = revolvedCircleRadial(i*3+1:i*3+3,&
                                                                  &1:1) / magRadial
         DO j = 0,surfSize
            ! parameterize circle to be revolved
            theta = t1 + REAL(j) / REAL(surfSize) * (t2 - t1)
            CALL getArbitraryRotation(normVec,x1(1:3,1:1),theta,x1(1:3,2:2),x)
            DO k = 0,surfSize
               ! parameterize revolution
               theta = revolvedCircleAngles(i+1,1) + REAL(k) / REAL(surfSize) *&
                     &(revolvedCircleAngles(i+1,2) - revolvedCircleAngles(i+1,1))
               CALL getArbitraryRotation(axisVec,center,theta,x,xNew)
               IF (myWrtFlag) WRITE (UNIT = 2193,FMT = 100) xNew(1,1), &
                             &xNew(2,1), xNew(3,1),revolvedCircleAngles(i+1,1),&
                             &revolvedCircleAngles(i+1,2)
               ! get bounding box for set of surfaces
               IF(xNew(1,1) < extrema(1,1)) THEN
                  extrema(1,1) = xNew(1,1)
               END IF
               IF(xNew(2,1) < extrema(2,1)) THEN
                  extrema(2,1) = xNew(2,1)
               END IF
               IF(xNew(3,1) < extrema(3,1)) THEN
                  extrema(3,1) = xNew(3,1)
               END IF
               IF(xNew(1,1) > extrema(1,2)) THEN
                  extrema(1,2) = xNew(1,1)
               END IF
               IF(xNew(2,1) > extrema(2,2)) THEN
                  extrema(2,2) = xNew(2,1)
               END IF
               IF(xNew(3,1) > extrema(3,2)) THEN
                  extrema(3,2) = xNew(3,1)
               END IF
               ! bounding box for each surface of revolution
               IF(xNew(1,1) < revolvedCircleExtrema(i*3+1,1)) THEN
                  revolvedCircleExtrema(i*3+1,1) = xNew(1,1)
               END IF
               IF(xNew(2,1) < revolvedCircleExtrema(i*3+2,1)) THEN
                  revolvedCircleExtrema(i*3+2,1) = xNew(2,1)
               END IF
               IF(xNew(3,1) < revolvedCircleExtrema(i*3+3,1)) THEN
                  revolvedCircleExtrema(i*3+3,1) = xNew(3,1)
               END IF
               IF(xNew(1,1) > revolvedCircleExtrema(i*3+1,2)) THEN
                  revolvedCircleExtrema(i*3+1,2) = xNew(1,1)
               END IF
               IF(xNew(2,1) > revolvedCircleExtrema(i*3+2,2)) THEN
                  revolvedCircleExtrema(i*3+2,2) = xNew(2,1)
               END IF
               IF(xNew(3,1) > revolvedCircleExtrema(i*3+3,2)) THEN
                  revolvedCircleExtrema(i*3+3,2) = xNew(3,1)
               END IF
            END DO
         END DO
      END DO

      ! parametric equation of Nonuniform Rational B-Spline surface
      DO i=0,NURBSIndex - 1
         ! Initialize variables
         K1 = NURBSIndices(i+1,1)
         K2 = NURBSIndices(i+1,2)
         M1 = NURBSIndices(i+1,3)
         M2 = NURBSIndices(i+1,4)
         N1 = 1 + K1 - M1
         N2 = 1 + K2 - M2
         ! A number of knots for first knot sequence
         A = N1 + 2 * M1 + 1
         ! B number of knots for second knot sequence
         B = N2 + 2 * M2 + 1
         ALLOCATE(si(A),ti(B))
         ! Get knot vectors
         si = NURBSKnot1(i+1,1:A)
         ti = NURBSKnot2(i+1,1:B)
         ! keep track of number of polynomial patches making up B-Spline surface
         eIndex(i+1) = 0
         DO j = 1,A-1
            DO k = 1,B-1
               ! flatten the surface to find roots more accurately
               DO i2 = 0,flatten
                  s1 = si(j) + 1.0 / REAL(flatten+1) * i2 * (si(j+1) - si(j))
                  s2 = si(j) + 1.0 / REAL(flatten+1) * (i2 + 1) * (si(j+1) - &
                      &si(j))
                  DO j2 = 0,flatten
                     t1 = ti(k) + 1.0 / REAL(flatten+1) * j2 * (ti(k+1) - ti(k))
                     t2 = ti(k) + 1.0 / REAL(flatten+1) * (j2 + 1) * &
                        &(ti(k+1) - ti(k))
                     IF (ABS(t2 - t1) > tol .AND. ABS(s2 - s1) > tol)&
                     &THEN
                        sStepSize = (s2 - s1)/1000.0
                        tStepSize = (t2 - t1)/1000.0
                        l = 0
                        m = 0
                        s = s1
                        t = t1
                        DO WHILE(s < s2)
                           ! iterate based on the distance between knot sequences
                           ! num affects how small the distance between parametric points gets
                           s = s1 + REAL(l) * sStepSize * (s2 - s1)
                           IF (s > s2) THEN
                              s = s2
                           END IF
                           l = l + INT(num/(s2 - s1)) + 1
                           m = 0
                           t = t1
                           IF (ABS(s - s1) < tol) THEN
                              eIndex(i+1) = eIndex(i+1) + 1
                              IF ((eIndex(i+1)-1)*2+1>NURBSLength) THEN
                                 CALL resizeNURBSData2((eIndex(i+1)-1)*2+1+10)
                                 NURBSLength = (eIndex(i+1)-1)*2+1+10
                              END IF
                              NURBSPExtrema(i*3+1:i*3+3,(eIndex(i+1)-1)*2+1) = &
                              &1e12
                              NURBSPExtrema(i*3+1:i*3+3,(eIndex(i+1)-1)*2+2) = &
                              &-1e12
                              NURBSPatch(i*2+1,(eIndex(i+1)-1)*2+1) = s1
                              NURBSPatch(i*2+2,(eIndex(i+1)-1)*2+1) = t1
                              NURBSPatch(i*2+1,(eIndex(i+1)-1)*2+2) = s2
                              NURBSPatch(i*2+2,(eIndex(i+1)-1)*2+2) = t2
                           END IF
                           DO WHILE(t < t2)
                              ! iterate based on the distance between knot sequences
                              ! num affects how small the distance between parametric points gets
                              t = t1 + REAL(m) * tStepSize * (t2 - t1)
                              m = m + INT(num/(t2 - t1)) + 1
                              IF (t > t2) THEN
                                 t = t2
                              END IF
                              x = NURBS(i+1,s,t)
                              IF (myWrtFlag) WRITE (UNIT = 2193,FMT = 100) &
                                            &x(1,1), x(2,1), x(3,1), s, t
                              ! get bounding box for set of surfaces
                              IF(x(1,1) < extrema(1,1)) THEN
                                 extrema(1,1) = x(1,1)
                              END IF
                              IF(x(2,1) < extrema(2,1)) THEN
                                 extrema(2,1) = x(2,1)
                              END IF
                              IF(x(3,1) < extrema(3,1)) THEN
                                 extrema(3,1) = x(3,1)
                              END IF
                              IF(x(1,1) > extrema(1,2)) THEN
                                 extrema(1,2) = x(1,1)
                              END IF
                              IF(x(2,1) > extrema(2,2)) THEN
                                 extrema(2,2) = x(2,1)
                              END IF
                              IF(x(3,1) > extrema(3,2)) THEN
                                 extrema(3,2) = x(3,1)
                              END IF
                              ! bounding box for each B-Spline surface
                              IF(x(1,1) < NURBSExtrema(i*3+1,1)) THEN
                                 NURBSExtrema(i*3+1,1) = x(1,1)
                              END IF
                              IF(x(2,1) < NURBSExtrema(i*3+2,1)) THEN
                                 NURBSExtrema(i*3+2,1) = x(2,1)
                              END IF
                              IF(x(3,1) < NURBSExtrema(i*3+3,1)) THEN
                                 NURBSExtrema(i*3+3,1) = x(3,1)
                              END IF
                              IF(x(1,1) > NURBSExtrema(i*3+1,2)) THEN
                                 NURBSExtrema(i*3+1,2) = x(1,1)
                              END IF
                              IF(x(2,1) > NURBSExtrema(i*3+2,2)) THEN
                                 NURBSExtrema(i*3+2,2) = x(2,1)
                              END IF
                              IF(x(3,1) > NURBSExtrema(i*3+3,2)) THEN
                                 NURBSExtrema(i*3+3,2) = x(3,1)
                              END IF
                              ! bounding box for each B-Spline polynomial patch
                              IF(x(1,1) < NURBSPExtrema(i*3+1,&
                              &(eIndex(i+1)-1)*2+1)) THEN
                                 NURBSPExtrema(i*3+1,&
                                              &(eIndex(i+1)-1)*2+1) = x(1,1)
                              END IF
                              IF(x(2,1) < NURBSPExtrema(i*3+2,&
                              &(eIndex(i+1)-1)*2+1)) THEN
                                 NURBSPExtrema(i*3+2,&
                                              &(eIndex(i+1)-1)*2+1) = x(2,1)
                              END IF
                              IF(x(3,1) < NURBSPExtrema(i*3+3,&
                              &(eIndex(i+1)-1)*2+1)) THEN
                                 NURBSPExtrema(i*3+3,&
                                              &(eIndex(i+1)-1)*2+1) = x(3,1)
                              END IF
                              IF(x(1,1) > NURBSPExtrema(i*3+1,&
                              &(eIndex(i+1)-1)*2+2)) THEN
                                 NURBSPExtrema(i*3+1,&
                                              &(eIndex(i+1)-1)*2+2) = x(1,1)
                              END IF
                              IF(x(2,1) > NURBSPExtrema(i*3+2,&
                              &(eIndex(i+1)-1)*2+2)) THEN
                                 NURBSPExtrema(i*3+2,&
                                              &(eIndex(i+1)-1)*2+2) = x(2,1)
                              END IF
                              IF(x(3,1) > NURBSPExtrema(i*3+3,&
                              &(eIndex(i+1)-1)*2+2)) THEN
                                 NURBSPExtrema(i*3+3,&
                                              &(eIndex(i+1)-1)*2+2) = x(3,1)
                              END IF
                           END DO !WHILE
                        END DO !WHILE
                     END IF
                  END DO
               END DO
            END DO
         END DO
         DEALLOCATE(si,ti)
      END DO
      IF (analyzing2) PRINT *,'xmin,xmax,ymin,ymax,zmin,zmax = ',extrema(1,1),&
                            &extrema(1,2),extrema(2,1),extrema(2,2),&
                            &extrema(3,1),extrema(3,2)
      IF(myWrtFlag) CLOSE (UNIT=2193)
      100 FORMAT (F16.10,1x,F16.10,1x,F16.10,1x,F16.10,1x,F16.10)
   END SUBROUTINE writeSurfaces

   !-------------------------------------------------------------------------------------
   ! writes curve outline data to the curve.txt file
   SUBROUTINE writeCurves()
      INTEGER :: i,j,k,l,curveSize,i1,i2,splineBreaks
      REAL(pr) :: num,x(3,1),lp1(3,1),lp2(3,1),normVec(3,1)
      REAL(pr) :: t1,t2,x1(3,1),x2(3,1),theta,s,s1,s2,stepSize,sMat(1,4)
      REAL(pr) :: coeff(4,3),xT(1,3)

      curveSize = 100
      ! trial and error value for a relatively good representation of the curve
      ! decrease for more accurate representation of the curve (slower)
      num = 25.0

      IF(myWrtFlag) OPEN (UNIT = 2194,FILE='geometry' //slash // 'curve.txt',&
           &FORM='formatted',STATUS='replace')
      ! parametric equations of curves on a surface of revolution generated by a line
      DO i=0,revolvedLinesIndex - 1
         ! get indices of revolvedLineLines corresponding to each revolved line
         i1 = revolvedLineNLines(i+1,1)
         i2 = revolvedLineNLines(i+1,2)
         IF (i1+i2 > 0) THEN
            DO j = i1,i2
               ! get the line end points
               lp1(1:3,1) = revolvedLineLines(j,1:3)
               lp2(1:3,1) = revolvedLineLines(j,4:6)
               ! parameterize the line
               DO k = 0,curveSize
                  x = lp1 + REAL(k) / REAL(curveSize) * (lp2 - lp1)
                  IF (myWrtFlag) WRITE(UNIT=2194,FMT=101) x(1,1),x(2,1),x(3,1)
               END DO
            END DO
         END IF
         ! get indices of revolvedLineCircles corresponding to each revolved line
         i1 = revolvedLineNCircles(i+1,1)
         i2 = revolvedLineNCircles(i+1,2)
         IF (i1+i2 > 0) THEN
            DO j = i1,i2
              ! get the circle's parametric data, center, and starting point
               normVec(1:3,1) = revolvedLineCircles(j,12:14)
               t1 = revolvedLineCircles(j,1)
               t2 = revolvedLineCircles(j,2)
               x1(1:3,1) = revolvedLineCircles(j,3:5)
               x2(1:3,1) = revolvedLineCircles(j,6:8)
               ! parameterize the circle
               DO k = 0,curveSize
                  theta = t1 + REAL(k) / REAL(curveSize) * (t2 - t1)
                  CALL getArbitraryRotation(normVec,x1,theta,x2,x)
                  IF (myWrtFlag) WRITE(UNIT=2194,FMT=101) x(1,1),x(2,1),x(3,1)
               END DO
            END DO
         END IF
      END DO

      ! parametric equations of curves on a surface of revolution generated by a circle
      DO i=0,revolvedCirclesIndex - 1
         ! get indices of revolvedLineLines corresponding to each revolved circle
         i1 = revolvedCircleNLines(i+1,1)
         i2 = revolvedCircleNLines(i+1,2)
         IF (i1+i2 > 0) THEN
            DO j = i1,i2
               ! get the line end points
               lp1(1:3,1) = revolvedCircleLines(j,1:3)
               lp2(1:3,1) = revolvedCircleLines(j,4:6)
               ! parameterize the line
               DO k = 0,curveSize
                  x = lp1 + REAL(k)/REAL(curveSize) * (lp2 - lp1)
                  IF (myWrtFlag) WRITE(UNIT=2194,FMT=101) x(1,1),x(2,1),x(3,1)
               END DO
            END DO
         END IF
         ! get indices of revolvedCircleCircles corresponding to each revolved circle
         i1 = revolvedCircleNCircles(i+1,1)
         i2 = revolvedCircleNCircles(i+1,2)
         IF (i1+i2 > 0) THEN
            DO j = i1,i2
               ! get the circle's parametric data, center, and starting point
               normVec(1:3,1) = revolvedCircleCircles(j,12:14)
               t1 = revolvedCircleCircles(j,1)
               t2 = revolvedCircleCircles(j,2)
               x1(1:3,1) = revolvedCircleCircles(j,3:5)
               x2(1:3,1) = revolvedCircleCircles(j,6:8)
               ! parameterize the circle
               DO k = 0,curveSize
                  theta = t1 + REAL(k) / REAL(curveSize) * (t2 - t1)
                  CALL getArbitraryRotation(normVec,x1,theta,x2,x)
                  IF (myWrtFlag) WRITE(UNIT=2194,FMT=101) x(1,1),x(2,1),x(3,1)
               END DO
            END DO
         END IF
      END DO

      ! parametric equations of curves on a Nonuniform Rational B-Spline
      DO i=0,NURBSIndex - 1
         ! get indices of revolvedLineLines corresponding to each B-Spline surface
         i1 = NURBSNLines(i+1,1)
         i2 = NURBSNLines(i+1,2)
         IF (i1 + i2 > 0) THEN
            DO j = i1,i2
               ! get the line end points
               lp1(1:3,1) = NURBSLines(j,1:3)
               lp2(1:3,1) = NURBSLines(j,4:6)
               ! parameterize the line
               DO k = 0,curveSize
                  x = lp1 + REAL(k)/REAL(curveSize) * (lp2 - lp1)
                  IF (myWrtFlag) WRITE(UNIT=2194,FMT=101) x(1,1),x(2,1),x(3,1)
               END DO
            END DO
         END IF
         ! get indices of NURBSCircles corresponding to each B-Spline surface
         i1 = NURBSNCircles(i+1,1)
         i2 = NURBSNCircles(i+1,2)
         IF (i1+i2 > 0) THEN
            DO j = i1,i2
              ! get the circle's parametric data, center, and starting point
               normVec(1:3,1) = NURBSCircles(j,12:14)
               t1 = NURBSCircles(j,1)
               t2 = NURBSCircles(j,2)
               x1(1:3,1) = NURBSCircles(j,3:5)
               x2(1:3,1) = NURBSCircles(j,6:8)
               ! parameterize the circle
               DO k = 0,curveSize
                  theta = t1 + REAL(k) / REAL(curveSize) * (t2 - t1)
                  CALL getArbitraryRotation(normVec,x1,theta,x2,x)
                  IF (myWrtFlag) WRITE(UNIT=2194,FMT=101) x(1,1),x(2,1),x(3,1)
               END DO
            END DO
         END IF
         ! get indices of NURBSSplines corresponding to each B-Spline surface
         i1 = NURBSNSplines(i+1,1)
         i2 = NURBSNSplines(i+1,2)
         IF (i1+i2 > 0) THEN
            DO j = i1,i2
               splineBreaks = NURBSSplinesBreaks(j,1)
               DO k = 1,splineBreaks
                  s1 = NURBSSplines((j-1)*3+1,(k-1)*6+1)
                  s2 = NURBSSplines((j-1)*3+1,(k-1)*6+2)
                  stepSize = (s2 - s1) / 1000.0
                  l = 0
                  s = s1
                  DO WHILE(s < s2)
                     ! iterate based on the distance between knot sequences
                     ! num affects how small the distance between parametric points gets
                     l = l + INT(num / (s2 - s1)) + 1
                     s = s1 + REAL(l) * stepSize * (s2 - s1)
                     IF (s > s2) THEN
                        s = s2
                     END IF
                     sMat(1,1) = s**3
                     sMat(1,2) = s**2
                     sMat(1,3) = s
                     sMat(1,4) = 1
                     coeff(1:4,1) = NURBSSplines((j-1)*3+1,(k-1)*6+3:(k-1)*6+6)
                     coeff(1:4,2) = NURBSSplines((j-1)*3+2,(k-1)*6+3:(k-1)*6+6)
                     coeff(1:4,3) = NURBSSplines((j-1)*3+3,(k-1)*6+3:(k-1)*6+6)
                     xT = MATMUL(sMat,coeff)
                     x = TRANSPOSE(xT)
                     IF (myWrtFlag) WRITE(UNIT=2194,FMT=101) x(1,1),x(2,1),x(3,1)
                  END DO !WHILE
               END DO
            END DO
         END IF
      END DO
      IF(myWrtFlag) CLOSE (UNIT=2194)
      101 FORMAT (F16.10,1x,F16.10,1x,F16.10)
   END SUBROUTINE writeCurves

   !-------------------------------------------------------------------------------------
   ! gets and stores surface of revolution data into memory
   !------------Variables------------
   ! surfaceDE is the data entry line for the revolved surface
   ! curveDE is the data entry line for the 2D curve existing on the revolved surface
   SUBROUTINE revolveSurface(surfaceDE,curveDE)

      INTEGER, INTENT(IN) :: surfaceDE

      CHARACTER :: rGeneratrix*80
      INTEGER :: axisofRevolutionDE,generatrixDE,curveDE
      REAL(pr) :: startAngle,terminateAngle

      ! Get the axis of revolution DE, generatrix DE, start angle, and terminate angle
      CALL readSurfaceofRevolution(axisofRevolutionDE,generatrixDE,startAngle,terminateAngle,surfaceDE)
      rGeneratrix = parLines(generatrixDE)%l
      ! If the surface is made up of a revolved circle, revolve that circle
      IF (rGeneratrix(1:3)=='100') THEN
         CALL revolveCircle(axisofRevolutionDE,generatrixDE,curveDE)
      END IF
      ! If the surface is made up of a revolved line, revolve that line
      IF (rGeneratrix(1:3)=='110') THEN
         CALL revolveLine(axisofRevolutionDE,generatrixDE,curveDE)
      END IF
   END SUBROUTINE revolveSurface

   !-------------------------------------------------------------------------------------
   ! reads the data entry line of a revolved surface
   !------------Variables------------
   ! axisRevolutionDE is the parametric data entry line containing the line forming the axis of revolution
   ! generatrixDE is the parametric data entry line describing the entity being revolved to form the surface
   ! startAngle is the start angle of the revolution
   ! terminateAngle is the ending angle of the revolution
   ! surfaceDE is the data entry line for the revolved surface
   SUBROUTINE readSurfaceofRevolution(axisofRevolutionDE,generatrixDE,&
                                     &startAngle,terminateAngle,surfaceDE)

      INTEGER, INTENT(OUT) :: axisofRevolutionDE,generatrixDE
      REAL(pr), INTENT(OUT) :: startAngle,terminateAngle
      INTEGER, INTENT(IN) :: surfaceDE
      INTEGER :: surfaceDE2

      CHARACTER :: r120*80
      INTEGER :: j,i10,j10,count

      ! To initialize the variables before using them in the do loop
      j10 = 0
      count = 0
      surfaceDE2 = surfaceDE

      r120 = parLines(surfaceDE)%l

e120: DO
         DO j=1,80
            IF (r120(j:j)==',' .OR. r120(j:j)==';') THEN
               i10 = j10
               count = count + 1
               j10 = j

               ! Get the Parameter Data Entry (DE) for the axis of revolution
               IF (count==2) THEN
                  READ(r120(i10+1:j10-1),*) axisofRevolutionDE
                  axisofRevolutionDE = table(axisofRevolutionDE)
               END IF

               ! Get the Parameter Data Entry (DE) for the generatrix
               IF (count==3) THEN
                  READ(r120(i10+1:j10-1),*) generatrixDE
                  generatrixDE = table(generatrixDE)
               END IF

               ! Get the start angle value for the revolution
               IF (count==4) THEN
                  READ(r120(i10+1:j10-1),*) startAngle
               END IF

               ! Get the final angle value for the revolution
               IF (count==5) THEN
                  READ(r120(i10+1:j10-1),*) terminateAngle
                  EXIT e120
               END IF
            END IF
         END DO
         surfaceDE2 = surfaceDE2 + 1
         r120 = parLines(surfaceDE2)%l
         j10 = 0
      END DO e120

   END SUBROUTINE readSurfaceofRevolution

   !-------------------------------------------------------------------------------------
   ! revolves the circle around the axis of rotation line
   !------------Variables------------
   ! axisRevolutionDE is the parametric data entry line containing the line forming the axis of revolution
   ! generatrixDE is the parametric data entry line describing the circular-arc being revolved to form the surface
   ! startAngle is the start angle of the revolution
   ! terminateAngle is the ending angle of the revolution
   ! curveDE is the data entry line for the 2D curve existing on the revolved surface
   SUBROUTINE revolveCircle(axisofRevolutionDE,generatrixDE,curveDE)
      INTEGER, INTENT(IN) :: axisofRevolutionDE,generatrixDE,curveDE

      REAL(pr) :: startAngle,terminateAngle
      REAL(pr) :: t1,t2,x(3,3),cNorm(4,1),firstEndPoint(3),secondEndPoint(3)

      ! Getting and storing the axis of revolution for current surface of revolution
      ! which was created using a revolved circle
      CALL getAxisofRevolutionData(axisofRevolutionDE,firstEndPoint,&
                                  &secondEndPoint)
      revolvedCircleAxes(revolvedCirclesIndex * 3 + &
      &1 : revolvedCirclesIndex * 3 + 3,1) = secondEndPoint
      revolvedCircleAxes(revolvedCirclesIndex * 3 + &
      &1 : revolvedCirclesIndex * 3 + 3,2) = firstEndPoint
      ! Circle Data is stored as in circleData
      CALL getCircleData(generatrixDE,t1,t2,x,cNorm)
      revolvedCircles(revolvedCirclesIndex+1,1) = t1
      revolvedCircles(revolvedCirclesIndex+1,2) = t2
      revolvedCircles(revolvedCirclesIndex+1,3:5) = x(:,1)
      revolvedCircles(revolvedCirclesIndex+1,6:8) = x(:,2)
      revolvedCircles(revolvedCirclesIndex+1,9:11) = x(:,3)
      revolvedCircles(revolvedCirclesIndex+1,12:14) = cNorm(1:3,1)
      ! The angles given by the surface of revolution entity are erroneous
      ! Get the actual angles by analyzing the normals of the circles
      !    that generate the start and end of the surface of revolution
      CALL getActualCircleAngles(curveDE,startAngle,terminateAngle)
      revolvedCircleAngles(revolvedCirclesIndex+1,1) = startAngle
      revolvedCircleAngles(revolvedCirclesIndex+1,2) = terminateAngle
      revolvedCirclesIndex = revolvedCirclesIndex + 1;
      ! If the revolved circles index is greater than what is allocated for
      ! the revolved circles data, allocate more memory for data storage
      IF (MOD(revolvedCirclesIndex,10) == 0) THEN
         CALL resizeRevolvedCirclesData()
      END IF

   END SUBROUTINE revolveCircle

   !-------------------------------------------------------------------------------------
   ! revolves a line around an axis of revolution
   !------------Variables------------
   ! axisRevolutionDE is the parametric data entry line containing the line forming the axis of revolution
   ! generatrixDE is the parametric data entry line describing the line being revolved to form the surface
   ! startAngle is the start angle of the revolution
   ! terminateAngle is the ending angle of the revolution
   ! curveDE is the data entry line for the 2D curve existing on the revolved surface
   SUBROUTINE revolveLine(axisofRevolutionDE,generatrixDE,curveDE)
      INTEGER, INTENT(IN) :: axisofRevolutionDE,generatrixDE,curveDE

      REAL(pr) :: startAngle,terminateAngle
      REAL(pr), DIMENSION(3) :: firstEndPoint,secondEndPoint
      REAL(pr), DIMENSION(3) :: lineFirstEndPoint,lineSecondEndPoint

      ! Getting and storing the axis of revolution for current surface of revolution
      ! which was created using a revolved line
      CALL getAxisofRevolutionData(axisofRevolutionDE,firstEndPoint,&
                                   &secondEndPoint)
      revolvedLineAxes(revolvedLinesIndex * 3 + &
      &1 : revolvedLinesIndex * 3 + 3,1) = firstEndPoint
      revolvedLineAxes(revolvedLinesIndex * 3 + &
      &1 : revolvedLinesIndex * 3 + 3,2) = secondEndPoint
      CALL getLineData(generatrixDE,lineFirstEndPoint,lineSecondEndPoint)
      revolvedLines(revolvedLinesIndex * 3 + 1 : revolvedLinesIndex * 3 + 3,&
                   &1) = lineFirstEndPoint
      revolvedLines(revolvedLinesIndex * 3 + 1 : revolvedLinesIndex * 3 + 3,&
                   &2) = lineSecondEndPoint
      ! The angles given by the surface of revolution entity are erroneous
      ! Get the actual angles by analyzing the 2D circle the revolution projects to
      CALL getActualLineAngles(curveDE,startAngle,terminateAngle)
      revolvedLineAngles(revolvedLinesIndex+1,1) = startAngle
      revolvedLineAngles(revolvedLinesIndex+1,2) = terminateAngle
      revolvedLinesIndex = revolvedLinesIndex + 1;
      ! If the revolved lines index is greater than what is allocated for
      ! the revolved lines data, allocate more memory for data storage
      IF (MOD(revolvedLinesIndex,10) == 0) THEN
         CALL resizeRevolvedLinesData()
      END IF
   END SUBROUTINE revolveLine

   !-------------------------------------------------------------------------------------
   ! gets the axis of revolution line end points
   !------------Variables------------
   ! axisRevolutionDE is the parametric data entry line containing the line forming the axis of revolution
   ! firstEndPoint is the first end point of the axis
   ! secondEndPoint is the second end point of the axis to form a line
   SUBROUTINE getAxisofRevolutionData(axisofRevolutionDE,firstEndPoint,&
                                     &secondEndPoint)
      INTEGER, INTENT(IN) :: axisofRevolutionDE
      REAL(pr), INTENT(OUT), DIMENSION(3) :: firstEndPoint,secondEndPoint

      CHARACTER :: r110*80
      INTEGER :: j,i10,j10,count
      INTEGER :: axisofRevolutionDE2

      ! To initialize the variables before using them in the do loop
      j10 = 0
      count = 0
      axisofRevolutionDE2 = axisofRevolutionDE

      r110 = parLines(axisofRevolutionDE)%l

e110: DO
         DO j=1,80
            IF (r110(j:j)==',' .OR. r110(j:j)==';') THEN
               i10 = j10
               count = count + 1
               j10 = j

               ! Get x-value of the first end point of the axis line
               IF (count==2) THEN
                  READ(r110(i10+1:j10-1),*) firstEndPoint(1)
               END IF

               ! Get y-value of the first end point of the axis line
               IF (count==3) THEN
                  READ(r110(i10+1:j10-1),*) firstEndPoint(2)
               END IF

               ! Get z-value of the first end point of the axis line
               IF (count==4) THEN
                  READ(r110(i10+1:j10-1),*) firstEndPoint(3)
               END IF

               ! Get x-value of the second end point of the axis line
               IF (count==5) THEN
                  READ(r110(i10+1:j10-1),*) secondEndPoint(1)
               END IF

               ! Get y-value of the second end point of the axis line
               IF (count==6) THEN
                  READ(r110(i10+1:j10-1),*) secondEndPoint(2)
               END IF

               ! Get z-value of the second end point of the axis line
               IF (count==7) THEN
                  READ(r110(i10+1:j10-1),*) secondEndPoint(3)
                  EXIT e110
               END IF
            END IF
         END DO
         axisofRevolutionDE2 = axisofRevolutionDE2 + 1
         r110 = parLines(axisofRevolutionDE2)%l
         j10 = 0
      END DO e110
   END SUBROUTINE getAxisofRevolutionData

   !-------------------------------------------------------------------------------------
   ! gets the circle's radius, center, and parametric values for end points
   !------------Variables------------
   ! circleDE is the parametric data entry line describing the circle
   ! radius is the circle's radius
   ! center is the circle's center
   ! t1 is the first parametric value and t2 is the second parametric value in the following equations
   ! x2 = x1 + radius*COS(t1)
   ! y2 = y1 + radius*SIN(t1)
   ! x3 = x1 + radius*COS(t2)
   ! y3 = y1 + radius*SIN(t2)
   SUBROUTINE getCircleData(circleDE,t1,t2,xNew,normVec)
      INTEGER, INTENT(IN) :: circleDE
      REAL(pr), INTENT(OUT) :: t1,t2,xNew(3,3),normVec(4,1)

      CHARACTER :: r100*80,rd*80
      INTEGER :: i,j,i10,j10,count,entity,rotDE,circleLine
      REAL(pr) :: x(4,3),rotationMatrix(3,4),tol,theta
      REAL(pr) :: vec1(3,1),vec2(3,1),magVec1,magVec2,magRot
      INTEGER :: circleDE2

      ! To initialize the variables before using them in the do loop
      j10 = 0
      count = 0
      circleDE2 = circleDE
      tol = 1e-5

      r100 = parLines(circleDE)%l

e100: DO
         DO j=1,80
            IF (r100(j:j)==',' .OR. r100(j:j)==';') THEN
               i10 = j10
               count = count + 1
               j10 = j

               ! Get x-value of the first end point of the line
               IF (count==2) THEN
                  READ(r100(i10+1:j10-1),*) x(3,1)
                  x(3,2) = x(3,1)
                  x(3,3) = x(3,1)
                  ! to allow for translation
                  x(4,1) = 1.0
                  x(4,2) = 1.0
                  x(4,3) = 1.0
               END IF

               ! Get y-value of the first end point of the axis line
               IF (count==3) THEN
                  READ(r100(i10+1:j10-1),*) x(1,1)
               END IF

               ! Get z-value of the first end point of the axis line
               IF (count==4) THEN
                  READ(r100(i10+1:j10-1),*) x(2,1)
               END IF

               ! Get x-value of the second end point of the axis line
               IF (count==5) THEN
                  READ(r100(i10+1:j10-1),*) x(1,2)
               END IF

               ! Get y-value of the second end point of the axis line
               IF (count==6) THEN
                  READ(r100(i10+1:j10-1),*) x(2,2)
               END IF

               ! Get y-value of the second end point of the axis line
               IF (count==7) THEN
                  READ(r100(i10+1:j10-1),*) x(1,3)
               END IF

               ! Get z-value of the second end point of the axis line
               IF (count==8) THEN
                  READ(r100(i10+1:j10-1),*) x(2,3)
                  EXIT e100
               END IF
            END IF
         END DO
         circleDE2 = circleDE2 + 1
         r100 = parLines(circleDE2)%l
         j10 = 0
      END DO e100

      ! read data lines to get rotation matrix
      j10 = 0
      circleLine = 0
rDat: DO i=1,dataLinesSize
         rd = dataLines(i)%l
         count = 0
         DO j=2,80
            IF ((rd(j:j)/=' ' .AND. rd(j-1:j-1)==' ') .OR. (rd(j:j)==' ' &
            &.AND. rd(j-1:j-1)/=' ')) THEN
               i10 = j10
               count = count + 1
               j10 = j

               ! Get the entity type for the data line
               IF (count==2) THEN
                  READ(rd(i10:j10-1),*) entity
                  ! No need to continue reading the line if the entity isn't a circle
                  IF (entity /= 100) EXIT
               END IF

               ! Determine which line the circle is on in the Parameter Data
               IF (count==4 .AND. entity==100) THEN
                  READ(rd(i10:j10-1),*) circleLine
                  ! No need to keep reading if the circle data line is beyond the current circle's data line
                  IF (circleLine > circleDE) EXIT rDat
               END IF

               ! Get the parameter (P) line number for the rotation matrix
               IF (j10 - 1 - i10 <= 4 .AND. count==12 .AND. &
               &circleDE==circleLine) THEN
                  READ(rd(i10:j10-1),*) rotDE
                  rotDE = table(rotDE)
                  EXIT rDat
               END IF
            END IF
         END DO
      END DO rDat

      ! If a transformation matrix exists for this circular arc, find it in the parameter listing
      IF(circleLine == circleDE) THEN
         CALL getRotationMatrix(rotDE,rotationMatrix)
         xNew = MATMUL(rotationMatrix,x)
      ELSE
         ! The transformation matrix is the identity matrix (no transformation)
         rotationMatrix = 0.0
         rotationMatrix(1,1) = 1.0
         rotationMatrix(2,2) = 1.0
         rotationMatrix(3,3) = 1.0
         xNew = MATMUL(rotationMatrix,x)
      END IF
      ! the normal vector is the z-axis (before transformation to world coordinates)
      normVec = 0.0
      normVec(3,1) = 1.0
      ! The direction of the vector is all that is needed, so translation is 0.0
      normVec(4,1) = 0.0
      ! transformation of direction to world coordinates
      normVec(1:3,1:1) = MATMUL(rotationMatrix,normVec)
      ! vectors from the center to each point on the circle
      vec1(:,1) = xNew(:,2) - xNew(:,1)
      vec2(:,1) = xNew(:,3) - xNew(:,1)
      ! vector magnitudes for each vector
      magVec1 = mag(vec1)
      magVec2 = mag(vec2)
      vec1 = vec1/magVec1
      vec2 = vec2/magVec2
      ! find the theta value between xNew(:,2) and xNew(:,3)
      IF (ABS(DOT_PRODUCT(vec1(:,1),vec2(:,1)) + 1.0) < tol) THEN
         theta = ACOS(-1.0)
      ELSE IF (ABS(DOT_PRODUCT(vec1(:,1),vec2(:,1)) - 1.0) < tol) THEN
         theta = ACOS(1.0)
      ELSE IF (ABS(DOT_PRODUCT(vec1(:,1),vec2(:,1))) > 1.0) THEN
         theta = 0.0
      ELSE
         theta = ACOS(DOT_PRODUCT(vec1(:,1),vec2(:,1)))
      END IF

      ! check if theta is clockwise or counterclockwise, and if counterclockwise,
      !    the rotation still must be counterclockwise making theta = 2 * PI - theta
      CALL getArbitraryRotation(normVec(1:3,1:1),xNew(:,1:1),theta,xNew(:,2:2),&
                               &x(1:3,3:3))
      IF (mag(x(1:3,3:3) - xNew(1:3,3:3)) > tol) THEN
         theta = 2*ACOS(-1.0)-theta
         CALL getArbitraryRotation(normVec(1:3,1:1),xNew(:,1:1),theta,&
                                  &xNew(:,2:2),x(1:3,3:3))
         IF (mag(x(1:3,3:3) - xNew(1:3,3:3)) > tol) THEN
            PRINT *,'******ERROR IN CIRCLE DATA******'
            magRot = mag(x(1:3,3:3) - xNew(1:3,3:3))
            PRINT *,magRot
            PRINT *,normVec(1:3,1:1),xNew(:,1:1),theta,&
                                  &xNew(:,2:2),x(1:3,3:3)
            PRINT *,DOT_PRODUCT(vec1(:,1),vec2(:,1))
         END IF
      END IF
      t1 = 0.0
      t2 = theta

   END SUBROUTINE getCircleData

   !-------------------------------------------------------------------------------------
   ! gets the Transformation Matrix for transforming the circle's coordinates from
   ! its coordinate frame to the world's coordinate frame
   !------------Variables------------
   ! rotDE is the parametric data entry line describing the transformation matrix
   ! rotationMatrix is the transformation matrix being obtained from the data contained
   ! in entity 124
   SUBROUTINE getRotationMatrix(rotDE,rotationMatrix)
      INTEGER, INTENT(IN) :: rotDE
      REAL(pr), INTENT(OUT) :: rotationMatrix(3,4)

      CHARACTER :: r124*80
      INTEGER :: j,i10,j10,count
      INTEGER :: rotDE2

      ! To initialize the variables before using them in the do loop
      j10 = 0
      count = 0
      rotDE2 = rotDE

      r124 = parLines(rotDE)%l

e124: DO
         DO j=1,80
            IF (r124(j:j)==',' .OR. r124(j:j)==';') THEN
               i10 = j10
               count = count + 1
               j10 = j

               ! read rotation matrix value for first row, first column
               IF (count==2) THEN
                  READ(r124(i10+1:j10-1),*) rotationMatrix(1,1)
               END IF

               ! read rotation matrix value for first row, second column
               IF (count==3) THEN
                  READ(r124(i10+1:j10-1),*) rotationMatrix(1,2)
               END IF

               ! read rotation matrix value for first row, third column
               IF (count==4) THEN
                  READ(r124(i10+1:j10-1),*) rotationMatrix(1,3)
               END IF

               ! Read x-tranlation
               IF (count==5) THEN
                  READ(r124(i10+1:j10-1),*) rotationMatrix(1,4)
               END IF

               ! read rotation matrix value for second row, first column
               IF (count==6) THEN
                  READ(r124(i10+1:j10-1),*) rotationMatrix(2,1)
               END IF

               ! read rotation matrix value for second row, second column
               IF (count==7) THEN
                  READ(r124(i10+1:j10-1),*) rotationMatrix(2,2)
               END IF

               ! read rotation matrix value for second row, third column
               IF (count==8) THEN
                  READ(r124(i10+1:j10-1),*) rotationMatrix(2,3)
               END IF

               ! Read y-tranlation
               IF (count==9) THEN
                  READ(r124(i10+1:j10-1),*) rotationMatrix(2,4)
               END IF

               ! read rotation matrix value for third row, first column
               IF (count==10) THEN
                  READ(r124(i10+1:j10-1),*) rotationMatrix(3,1)
               END IF

               ! read rotation matrix value for third row, second column
               IF (count==11) THEN
                  READ(r124(i10+1:j10-1),*) rotationMatrix(3,2)
               END IF

               ! read rotation matrix value for third row, third column
               IF (count==12) THEN
                  READ(r124(i10+1:j10-1),*) rotationMatrix(3,3)
               END IF

               ! Read z-tranlation
               IF (count==13) THEN
                  READ(r124(i10+1:j10-1),*) rotationMatrix(3,4)
                  EXIT e124
               END IF
            END IF
         END DO
         rotDE2 = rotDE2 + 1
         r124 = parLines(rotDE2)%l
         j10 = 0
      END DO e124

   END SUBROUTINE getRotationMatrix

   !-------------------------------------------------------------------------------------
   ! gets a point's position after it is rotated about an arbitrary axis
   !------------Variables------------
   ! axisVec is the unit vector along the axis of rotation
   ! cP is a point the axis passes through
   ! theta is the angle the point is to be rotated by around the axis
   ! x is the point being rotated around the axis
   ! xNew is the point's position after it has been rotated around the axis
   SUBROUTINE getArbitraryRotation(axisVec,cP,theta,x,xNew)
      REAL(pr), INTENT (IN) :: axisVec(3,1),cP(3,1),theta,x(3,1)
      REAL(pr), INTENT (OUT) :: xNew(3,1)

      REAL(pr) :: rotationMatrix(3,4),u,v,w,a,b,c,xRot(4,1)
      ! variables for getting the rotation matrix about an arbitrary line
      u = axisVec(1,1)
      v = axisVec(2,1)
      w = axisVec(3,1)
      a = 0.0
      b = 0.0
      c = 0.0
      xRot(1:3,1:1) = x - cP
      xRot(4,1) = 1.0
      ! the rotation matrix for an arbitrary line
      rotationMatrix(1,1) = u**2 + (v**2 + w**2) * COS(theta)
      rotationMatrix(1,2) = u * v * (1 - COS(theta)) - w * SIN(theta)
      rotationMatrix(1,3) = u * w * (1 - COS(theta)) + v * SIN(theta)
      rotationMatrix(1,4) = (a * (v**2 + w**2) - u * (b * v + c * w)) * &
                           &(1 - COS(theta)) + (b * w - c * v) * SIN(theta)
      rotationMatrix(2,1) = u * v * (1 - COS(theta)) + w * SIN(theta)
      rotationMatrix(2,2) = v**2 + (u**2 + w**2) * COS(theta)
      rotationMatrix(2,3) = v * w * (1 - COS(theta)) - u * SIN(theta)
      rotationMatrix(2,4) = (b * (u**2 + w**2) - v * (a * u + c * w)) * &
                           &(1 - COS(theta)) + (c * u - a * w) * SIN(theta)
      rotationMatrix(3,1) = u * w * (1 - COS(theta)) - v * SIN(theta)
      rotationMatrix(3,2) = v * w * (1 - COS(theta)) + u * SIN(theta)
      rotationMatrix(3,3) = w**2 + (u**2 + v**2) * COS(theta)
      rotationMatrix(3,4) = (c * (u**2 + v**2) - w * (a * u + b * v)) * &
                           &(1 - COS(theta)) + (b * u - a * v) * SIN(theta)
      ! rotating the point about the arbitrary axis
      xNew = MATMUL(rotationMatrix,xRot) + cP
   END SUBROUTINE getArbitraryRotation

   !-------------------------------------------------------------------------------------
   ! Gets the actual start and end angles of the surface of revolution
   ! by obtaining the start and end angles of the normal to the plane
   ! that the starting and ending circles lie on
   !------------Variables------------
   ! curveDE is the data entry line containing the curve(s)
   ! startAngle is the start angle of the revolution
   ! terminateAngle is the ending angle of the revolution
   SUBROUTINE getActualCircleAngles(curveDE,startAngle,terminateAngle)

      REAL(pr), INTENT(OUT) :: startAngle,terminateAngle
      INTEGER, INTENT(IN) :: curveDE

      INTEGER :: nCircles,nLines,nSplines,i
      REAL(pr) :: tol,axisVec(3,1),magAxisVec,normVec(3,1),rotNorm(3,1)
      REAL(pr) :: newVec(3,1),center(3,1)

      tol = 1e-5
      ! Allocate 2D curve data before getting curve data
      CALL allocate2DCurves()
      CALL readCurve(curveDE,nCircles,nLines,nSplines)
      ! normalize the axis of revolution vector
      axisVec = revolvedCircleAxes(revolvedCirclesIndex*3 + &
               &1:revolvedCirclesIndex*3 + 3,1:1) - &
               &revolvedCircleAxes(revolvedCirclesIndex*3 + &
               &1:revolvedCirclesIndex*3 + 3,2:2)
      magAxisVec = mag(axisVec)
      axisVec = axisVec / magAxisVec
      ! store the circle data and the indices of the data for each revolved line
      revolvedCircleNCircles(revolvedCirclesIndex+1,&
                            &1) = revolvedCircleCirclesIndex + 1
      revolvedCircleNCircles(revolvedCirclesIndex+1,&
                            &2) = revolvedCircleCirclesIndex + nCircles
      revolvedCircleNLines(revolvedCirclesIndex+1,&
                          &1) = revolvedCircleLinesIndex + 1
      revolvedCircleNLines(revolvedCirclesIndex+1,&
                          &2) = revolvedCircleLinesIndex + nLines
      revolvedCircleNSplines(revolvedCirclesIndex+1,&
                            &1) = revolvedCircleSplinesIndex + 1
      revolvedCircleNSplines(revolvedCirclesIndex+1,&
                            &2) = revolvedCircleSplinesIndex + nSplines
      ! in case no start angle is found
      startAngle = 0.0
      ! in case no final angle is found
      terminateAngle = startAngle + 2 * ACOS(-1.0)
      rotNorm = TRANSPOSE(revolvedCircles(revolvedCirclesIndex+1:&
                         &revolvedCirclesIndex+1,12:14))
      DO i = 1,nCircles
         revolvedCircleCirclesIndex = revolvedCircleCirclesIndex + 1
         revolvedCircleCircles(revolvedCircleCirclesIndex,&
                              &1:14) = circleData(i,1:14)
         ! If revolvedCircleCirclesIndex will be out of array bounds of revolvedCircleCircles next iteration, resize the array
         IF (MOD(revolvedCircleCirclesIndex,10) == 0) THEN
            CALL resizeRevolvedCircleCircles()
         END IF
         ! unit normal to the circle
         normVec = TRANSPOSE(circleData(i:i,12:14))
         ! center for arbitrary rotation
         center = 0.0
         ! find angle between first circle's plane and the revolved circle's plane
         IF (i==1) THEN
            ! find the theta value between rotated entities
            IF (ABS(DOT_PRODUCT(-normVec(1:3,1),rotNorm(1:3,1)) + 1.0) < tol) &
            &THEN
               startAngle = ACOS(-1.0)
            ELSE IF (ABS(DOT_PRODUCT(-normVec(1:3,1),rotNorm(1:3,1)) - 1.0) &
            &< tol) THEN
               startAngle = ACOS(1.0)
            ELSE IF (ABS(DOT_PRODUCT(-normVec(1:3,1),rotNorm(1:3,1))) > 1.0) &
            &THEN
               startAngle = 0.0
            ELSE
               startAngle = ACOS(DOT_PRODUCT(-normVec(1:3,1),rotNorm(1:3,1)))
            END IF
            CALL getArbitraryRotation(axisVec,center,startAngle,rotNorm,newVec)
            ! check if the dot product gave the angle in the counterclockwise direction
            IF (ABS(mag(newVec + normVec)) > tol) THEN
               ! if the dot product angle was in the opposite direction,
               !    maintain counterclockwise convention by replacing theta with theta = 2.0 * PI - theta
               startAngle = 2.0 * ACOS(-1.0) - startAngle
               CALL getArbitraryRotation(axisVec,center,startAngle,rotNorm,&
                                        &newVec)
               ! check if the logic is correct
               IF (ABS(mag(newVec + normVec)) > tol) THEN
                  PRINT *,'***************ERROR IN CIRCLE REVOLUTION 1***************'
               END IF
            END IF
         END IF
         ! find angle between final circle's plane and the revolved circle's plane
         IF (i==nCircles) THEN
            IF (ABS(DOT_PRODUCT(normVec(1:3,1),rotNorm(1:3,1)) + 1.0) < tol) &
            &THEN
               terminateAngle = ACOS(-1.0)
            ELSE IF (ABS(DOT_PRODUCT(normVec(1:3,1),rotNorm(1:3,1)) - 1.0) &
            &< tol) THEN
               terminateAngle = ACOS(1.0)
            ELSE IF (ABS(DOT_PRODUCT(normVec(1:3,1),rotNorm(1:3,1))) > 1.0) &
            &THEN
               terminateAngle = startAngle + 2 * ACOS(-1.0)
            ELSE
               terminateAngle = ACOS(DOT_PRODUCT(normVec(1:3,1),rotNorm(1:3,1)))
            END IF
            CALL getArbitraryRotation(axisVec,center,terminateAngle,rotNorm,&
                                     &newVec)
            ! check if the dot product gave the angle in the counterclockwise direction
            IF (ABS(mag(newVec - normVec)) > tol) THEN
               ! if the dot product angle was in the opposite direction,
               !    maintain counterclockwise convention by replacing theta with theta = 2.0 * PI - theta
               terminateAngle = 2.0 * ACOS(-1.0) - terminateAngle
               CALL getArbitraryRotation(axisVec,center,terminateAngle,rotNorm,&
                                        &newVec)
               ! check if the logic is correct
               IF (ABS(mag(newVec - normVec)) > tol) THEN
                  PRINT *,'***************ERROR IN CIRCLE REVOLUTION 1***************'
               END IF
            END IF
            ! check if terminate angle should be 2 * PI instead of 0
            IF (ABS(terminateAngle - startAngle + ACOS(-1.0)) < tol) THEN
               terminateAngle = 2 * ACOS(-1.0)
            END IF
         END IF
      END DO
      DO i = 1,nLines
         revolvedCircleLinesIndex = revolvedCircleLinesIndex + 1
         revolvedCircleLines(revolvedCircleLinesIndex,1:6) = lineData(i,1:6)
         ! If revolvedCircleLinesIndex will be out of array bounds of revolvedCircleLines next iteration, resize the array
         IF (MOD(revolvedCircleLinesIndex,10) == 0) THEN
            CALL resizeRevolvedCircleLines()
         END IF
      END DO
      ! Deallocate 2D curve data after finished with it
      CALL deallocate2DCurves()

   END SUBROUTINE getActualCircleAngles

   !-------------------------------------------------------------------------------------
   ! gets the line end points
   !------------Variables------------
   ! lineDE is the parametric data entry line describing the line
   ! firstEndPoint is the first end point of the line
   ! secondEndPoint is the second end point of the line
   SUBROUTINE getLineData(lineDE,firstEndPoint,secondEndPoint)
      INTEGER, INTENT(IN) :: lineDE
      REAL(pr), INTENT(OUT), DIMENSION(3) :: firstEndPoint,secondEndPoint

      CHARACTER :: r110*80
      INTEGER :: j,i10,j10,count
      INTEGER :: lineDE2

      ! To initialize the variables before using them in the do loop
      j10 = 0
      count = 0
      lineDE2 = lineDE

      r110 = parLines(lineDE)%l

e110: DO
         DO j=1,80
            IF (r110(j:j)==',' .OR. r110(j:j)==';') THEN
               i10 = j10
               count = count + 1
               j10 = j

               ! Get x-value of the first end point of the line
               IF (count==2) THEN
                  READ(r110(i10+1:j10-1),*) firstEndPoint(1)
               END IF

               ! Get y-value of the first end point of the axis line
               IF (count==3) THEN
                  READ(r110(i10+1:j10-1),*) firstEndPoint(2)
               END IF

               ! Get z-value of the first end point of the axis line
               IF (count==4) THEN
                  READ(r110(i10+1:j10-1),*) firstEndPoint(3)
               END IF

               ! Get x-value of the second end point of the axis line
               IF (count==5) THEN
                  READ(r110(i10+1:j10-1),*) secondEndPoint(1)
               END IF

               ! Get y-value of the second end point of the axis line
               IF (count==6) THEN
                  READ(r110(i10+1:j10-1),*) secondEndPoint(2)
               END IF

               ! Get z-value of the second end point of the axis line
               IF (count==7) THEN
                  READ(r110(i10+1:j10-1),*) secondEndPoint(3)
                  EXIT e110
               END IF
            END IF
         END DO
         lineDE2 = lineDE2 + 1
         r110 = parLines(lineDE2)%l
         j10 = 0
      END DO e110
   END SUBROUTINE getLineData

   !-------------------------------------------------------------------------------------
   ! Gets the actual start and end angles of the surface of revolution
   ! by obtaining the start and end angles of the circle(s) generated
   ! by the projection of the revolved surface
   !------------Variables------------
   ! curveDE is the data entry line containing the curve(s)
   ! startAngle is the start angle of the revolution
   ! terminateAngle is the ending angle of the revolution
   SUBROUTINE getActualLineAngles(curveDE,startAngle,terminateAngle)

      REAL(pr), INTENT(OUT) :: startAngle,terminateAngle
      INTEGER, INTENT(IN) :: curveDE

      INTEGER :: nCircles,nLines,nSplines,i
      REAL(pr) :: a,b,c,d,tol,vec1(3,1),vec2(3,1),axisVec(3,1)
      REAL(pr) :: magVec1,magVec2,magAxisVec,magNormDiff,newVec(3,1),magRot
      REAL(pr) :: x1(3,1),x2(3,1),x3(3,1),normVec(3,1),line(3,2)

      tol = 1e-5
      ! Allocate 2D curve data before getting curve data
      CALL allocate2DCurves()
      CALL readCurve(curveDE,nCircles,nLines,nSplines)
      ! normalize the axis of revolution vector
      axisVec = revolvedLineAxes(revolvedLinesIndex*3 + &
               &1:revolvedLinesIndex*3 + 3,1:1) - &
               &revolvedLineAxes(revolvedLinesIndex*3 + &
               &1:revolvedLinesIndex*3 + 3,2:2)
      magAxisVec = mag(axisVec)
      axisVec = axisVec / magAxisVec
      ! store the circle data and the indices of the data for each revolved line
      revolvedLineNCircles(revolvedLinesIndex+1,1) = revolvedLineCirclesIndex &
                                                    &+ 1
      revolvedLineNCircles(revolvedLinesIndex+1,2) = revolvedLineCirclesIndex &
                                                    &+ nCircles
      revolvedLineNLines(revolvedLinesIndex+1,1) = revolvedLineLinesIndex + 1
      revolvedLineNLines(revolvedLinesIndex+1,2) = revolvedLineLinesIndex &
                                                  &+ nLines
      revolvedLineNSplines(revolvedLinesIndex+1,1) = revolvedLineSplinesIndex &
                                                    &+ 1
      revolvedLineNSplines(revolvedLinesIndex+1,2) = revolvedLineSplinesIndex &
                                                    &+ nSplines
      revolvedLineCirclesIndex = revolvedLineCirclesIndex + 1
      revolvedLineCircles(revolvedLineCirclesIndex,&
                         &1:14) = circleData(1,1:14)
      ! If revolvedLineCirclesIndex will be out of array bounds of revolvedLineCircles next iteration, resize the array
      IF (MOD(revolvedLineCirclesIndex,10) == 0) THEN
         CALL resizeRevolvedLineCircles()
      END IF
      ! equation of a plane is a * x + b * y + c * z = d
      ! (a,b,c) is the unit normal to the circle
      normVec = TRANSPOSE(circleData(1:1,12:14))
      a = normVec(1,1)
      b = normVec(2,1)
      c = normVec(3,1)
      ! x1 is the circle's center point
      x1 = TRANSPOSE(circleData(1:1,3:5))
      ! a * x + b * y + c * z  with x,y,z the circle's center point
      d = a * x1(1,1) + b * x1(2,1) + c * x1(3,1)
      ! in case no start angle is found
      startAngle = 0.0
      ! x2 is the first point on the circle corresponding to the parametric circle equation
      ! x3 is the final point on the circle corresponding to the parametric circle equation
      x2 = TRANSPOSE(circleData(1:1,6:8))
      x3 = TRANSPOSE(circleData(1:1,9:11))
      ! line is the line being rotated about the axis of rotation
      line = revolvedLines(revolvedLinesIndex * 3 + &
         &1 : revolvedLinesIndex * 3 + 3,1:2)
      ! If the normal vector to the circle and the axis of rotation are in the same direction
      !    and the first end point of the line lies on the circle's plane
      ! Determine whether the axis of rotation and normal to the circle are in the same direction
      magNormDiff = mag(axisVec(1:3,1:1) - normVec)
      IF ( magNormDiff < tol .AND. &
      &ABS(d - DOT_PRODUCT(line(1:3,1),normVec(1:3,1))) < tol) THEN
         ! find the angle between the line being rotated and the first parametric point
         !    on the circle to find the first angle of rotation needed to start the
         !    surface of revolution
         vec1 = line(1:3,1:1) - x1(1:3,1:1)
         vec2 = x2 - x1
         magVec1 = mag(vec1)
         magVec2 = mag(vec2)
         vec1 = vec1 / magVec1
         vec2 = vec2 / magVec2
         IF (ABS(DOT_PRODUCT(vec1(:,1),vec2(:,1)) + 1.0) < tol) &
         &THEN
            startAngle = ACOS(-1.0)
         ELSE IF (ABS(DOT_PRODUCT(vec1(:,1),vec2(:,1)) - 1.0) &
         &< tol) THEN
            startAngle = ACOS(1.0)
         ELSE IF (ABS(DOT_PRODUCT(vec1(:,1),vec2(:,1))) > 1.0) &
         &THEN
            startAngle = 0.0
         ELSE
            startAngle = ACOS(DOT_PRODUCT(vec1(:,1),vec2(:,1)))
         END IF
         ! the circle can be transformed into a unit circle by translating the unit vectors
         !    so their start points lie on the center of the circle
         vec1 = vec1 + x1
         vec2 = vec2 + x1
         ! check if rotating the line that creates the surface of revolution by the start angle
         !    places it at the starting parametric point on the circle
         CALL getArbitraryRotation(axisVec,x1,startAngle,vec1,newVec)
         IF (mag(newVec(:,1:1) - vec2(:,1:1)) > tol) THEN
            ! if the rotation of the line does not place it at the starting parametric point,
            !    the angle between the vectors is always the same, but must be in the opposite
            !    direction. This is the result given by 2 * PI - theta (theta the angle between vectors)
            startAngle = 2 * ACOS(-1.0) - startAngle
            CALL getArbitraryRotation(axisVec,x1,startAngle,vec1,newVec)
            magRot = mag(newVec(:,1:1) - vec2(:,1:1))
            ! If the rotated vector is equal to the starting parametric point of the circle,
            !    accept this as the starting angle of rotation for the surface of revolution
            IF (magRot > tol) THEN
               ! Otherwise, there must be some error in the logic (this error should never happen)
               PRINT *,'******ERROR IN REVOLVING LINE 1******'
               PRINT *,magRot
            END IF
         END IF
      END IF
      ! If the normal vector to the circle and the axis of rotation are in the same direction
      !    and the second end point of the line lies on the circle's plane
      IF ( magNormDiff < tol .AND. &
      &ABS(d - DOT_PRODUCT(line(1:3,2),normVec(1:3,1))) < tol) THEN
         ! find the angle between the line being rotated and the first parametric point
         !    on the circle to find the first angle of rotation needed to start the
         !    surface of revolution
         vec1 = line(1:3,2:2) - x1(1:3,1:1)
         vec2 = x2 - x1
         magVec1 = mag(vec1)
         magVec2 = mag(vec2)
         vec1 = vec1 / magVec1
         vec2 = vec2 / magVec2
         IF (ABS(DOT_PRODUCT(vec1(:,1),vec2(:,1)) + 1.0) < tol) &
         &THEN
            startAngle = ACOS(-1.0)
         ELSE IF (ABS(DOT_PRODUCT(vec1(:,1),vec2(:,1)) - 1.0) &
         &< tol) THEN
            startAngle = ACOS(1.0)
         ELSE IF (ABS(DOT_PRODUCT(vec1(:,1),vec2(:,1))) > 1.0) &
         &THEN
            startAngle = 0.0
         ELSE
            startAngle = ACOS(DOT_PRODUCT(vec1(:,1),vec2(:,1)))
         END IF
         ! the circle can be transformed into a unit circle by translating the unit vectors
         !    so their start points lie on the center of the circle
         vec1 = vec1 + x1
         vec2 = vec2 + x1
         ! check if rotating the line that creates the surface of revolution by the start angle
         !    places it at the starting parametric point on the circle
         CALL getArbitraryRotation(axisVec,x1,startAngle,vec1,newVec)
         IF (mag(newVec(:,1:1) - vec2(:,1:1)) > tol) THEN
            ! if the rotation of the line does not place it at the starting parametric point,
            !    the angle between the vectors is always the same, but must be in the opposite
            !    direction. This is the result given by 2 * PI - theta (theta the angle between vectors)
            startAngle = 2 * ACOS(-1.0) - startAngle
            CALL getArbitraryRotation(axisVec,x1,startAngle,vec1,newVec)
            magRot = mag(newVec(:,1:1) - vec2(:,1:1))
            ! If the rotated vector is equal to the starting parametric point of the circle,
            !    accept this as the starting angle of rotation for the surface of revolution
            IF (magRot > tol) THEN
               ! Otherwise, there must be some error in the logic (this error should never happen)
               PRINT *,'******ERROR IN REVOLVING LINE 2******'
               PRINT *,magRot
            END IF
         END IF
      END IF
      ! If the normal vector to the circle and the axis of rotation are in opposite directions
      !    and the first end point of the line lies on the circle's plane
      ! Determine whether the axis of rotation and normal to the circle are in opposite directions
      magNormDiff = mag(axisVec(1:3,1:1) + normVec)
      IF ( magNormDiff < tol .AND. &
      &ABS(d - DOT_PRODUCT(line(1:3,1),normVec(1:3,1))) < tol) THEN
         ! find the angle between the line being rotated and the first parametric point
         !    on the circle to find the first angle of rotation needed to start the
         !    surface of revolution (the first parametric point for opposite axes is x3)
         vec1 = line(1:3,1:1) - x1(1:3,1:1)
         vec2 = x3 - x1
         magVec1 = mag(vec1)
         magVec2 = mag(vec2)
         vec1 = vec1 / magVec1
         vec2 = vec2 / magVec2
         IF (ABS(DOT_PRODUCT(vec1(:,1),vec2(:,1)) + 1.0) < tol) &
         &THEN
            startAngle = ACOS(-1.0)
         ELSE IF (ABS(DOT_PRODUCT(vec1(:,1),vec2(:,1)) - 1.0) &
         &< tol) THEN
            startAngle = ACOS(1.0)
         ELSE IF (ABS(DOT_PRODUCT(vec1(:,1),vec2(:,1))) > 1.0) &
         &THEN
            startAngle = 0.0
         ELSE
            startAngle = ACOS(DOT_PRODUCT(vec1(:,1),vec2(:,1)))
         END IF
         ! the circle can be transformed into a unit circle by translating the unit vectors
         !    so their start points lie on the center of the circle
         vec1 = vec1 + x1
         vec2 = vec2 + x1
         ! check if rotating the line that creates the surface of revolution by the start angle
         !    places it at the starting parametric point on the circle
         CALL getArbitraryRotation(axisVec,x1,startAngle,vec1,newVec)
         IF (mag(newVec(:,1:1) - vec2(:,1:1)) > tol) THEN
            ! if the rotation of the line does not place it at the starting parametric point,
            !    the angle between the vectors is always the same, but must be in the opposite
            !    direction. This is the result given by 2 * PI - theta (theta the angle between vectors)
            startAngle = 2 * ACOS(-1.0) - startAngle
            CALL getArbitraryRotation(axisVec,x1,startAngle,vec1,newVec)
            magRot = mag(newVec(:,1:1) - vec2(:,1:1))
            ! If the rotated vector is equal to the starting parametric point of the circle,
            !    accept this as the starting angle of rotation for the surface of revolution
            IF (magRot > tol) THEN
               ! Otherwise, there must be some error in the logic (this error should never happen)
               PRINT *,'******ERROR IN REVOLVING LINE 3******'
               PRINT *,magRot
            END IF
         END IF
      END IF
      ! If the normal vector to the circle and the axis of rotation are in opposite directions
      !    and the second end point of the line lies on the circle's plane
      IF ( magNormDiff < tol .AND. &
      &ABS(d - DOT_PRODUCT(line(1:3,2),normVec(1:3,1))) < tol) THEN
         ! find the angle between the line being rotated and the first parametric point
         !    on the circle to find the first angle of rotation needed to start the
         !    surface of revolution (the first parametric point for opposite axes is x3)
         vec1 = line(1:3,2:2) - x1(1:3,1:1)
         vec2 = x3 - x1
         magVec1 = mag(vec1)
         magVec2 = mag(vec2)
         vec1 = vec1 / magVec1
         vec2 = vec2 / magVec2
         IF (ABS(DOT_PRODUCT(vec1(:,1),vec2(:,1)) + 1.0) < tol) &
         &THEN
            startAngle = ACOS(-1.0)
         ELSE IF (ABS(DOT_PRODUCT(vec1(:,1),vec2(:,1)) - 1.0) &
         &< tol) THEN
            startAngle = ACOS(1.0)
         ELSE IF (ABS(DOT_PRODUCT(vec1(:,1),vec2(:,1))) > 1.0) &
         &THEN
            startAngle = 0.0
         ELSE
            startAngle = ACOS(DOT_PRODUCT(vec1(:,1),vec2(:,1)))
         END IF
         ! the circle can be transformed into a unit circle by translating the unit vectors
         !    so their start points lie on the center of the circle
         vec1 = vec1 + x1
         vec2 = vec2 + x1
         ! check if rotating the line that creates the surface of revolution by the start angle
         !    places it at the starting parametric point on the circle
         CALL getArbitraryRotation(axisVec,x1,startAngle,vec1,newVec)
         IF (mag(newVec(:,1:1) - vec2(:,1:1)) > tol) THEN
            ! if the rotation of the line does not place it at the starting parametric point,
            !    the angle between the vectors is always the same, but must be in the opposite
            !    direction. This is the result given by 2 * PI - theta (theta the angle between vectors)
            startAngle = 2 * ACOS(-1.0) - startAngle
            CALL getArbitraryRotation(axisVec,x1,startAngle,vec1,newVec)
            magRot = mag(newVec(:,1:1) - vec2(:,1:1))
            ! If the rotated vector is equal to the starting parametric point of the circle,
            !    accept this as the starting angle of rotation for the surface of revolution
            IF (magRot > tol) THEN
               ! Otherwise, there must be some error in the logic (this error should never happen)
               PRINT *,'******ERROR IN REVOLVING LINE 4******'
               PRINT *,magRot
            END IF
         END IF
      END IF
      ! The final angle of revolution for the line about the axis is the starting angle plus
      !    the final parametric angle of the circle
      terminateAngle = startAngle + circleData(1,2)
      DO i = 1,nLines
         revolvedLineLinesIndex = revolvedLineLinesIndex + 1

         IF (MOD(revolvedLineLinesIndex,10) == 0) THEN   !ERIC: switched the order
            CALL resizeRevolvedLineLines()
         END IF
         revolvedLineLines(revolvedLineLinesIndex,1:6) = lineData(i,1:6)
         ! If revolvedCircleLinesIndex will be out of array bounds of revolvedCircleLines next iteration, resize the array

      END DO
      ! Deallocate 2D curve data after finished with it
      CALL deallocate2DCurves()

   END SUBROUTINE getActualLineAngles

   !-------------------------------------------------------------------------------------
   ! Reads the data from the curve entity (entity 102)
   !------------Variables------------
   ! curveDE is the data entry line containing the curve(s)
   ! nCircles is the number of circles contained in the curve
   ! circleData is an array containing data for each circle
   ! nLines is the number of lines contained in the curve
   ! lineData is an array containing data for each line
   ! nSplines is the number of splines contained in the curve
   ! splineData is an array containing data for each spline
   SUBROUTINE readCurve(curveDE,nCircles,nLines,nSplines)

      INTEGER, INTENT(IN) :: curveDE
      INTEGER, INTENT(OUT) :: nCircles,nLines,nSplines

      INTEGER :: curveDE2
      REAL(pr) :: t1,t2,x(3,3),cNorm(4,1),firstEndPoint(3),secondEndPoint(3)
      CHARACTER :: r102*80,rc*80
      INTEGER :: j,i10,j10,count,numberofEntities,splineBreaks
      INTEGER :: newSize
      INTEGER :: oldSize

      INTEGER, ALLOCATABLE :: entityDE(:),array(:)

      ALLOCATE(entityDE(10))

      ! To initialize the variables before using them in the do loop
      j10 = 0
      count = 0
      curveDE2 = curveDE
      numberofEntities = 2

      ! To keep track of the size of entityDE
      newSize = 10

      ! Keep track of how many of each curve exists
      nCircles = 0
      nLines = 0
      nSplines = 0

      r102 = parLines(curveDE)%l

e102: DO
         DO j=1,80
            IF (r102(j:j)==',' .OR. r102(j:j)==';') THEN
               i10 = j10
               count = count + 1
               j10 = j

               ! Get the number of Entities comprising the 2D curve(s)
               IF (count==2) THEN
                  READ(r102(i10+1:j10-1),*) numberofEntities
               END IF

               ! Get the Parameter Data Entry (DE) for each curve
               IF (count > 2 .AND. count <= numberofEntities + 2) THEN
                  READ(r102(i10+1:j10-1),*) entityDE(count-2)
                  entityDE(count-2) = table(entityDE(count-2))
                  rc = parLines(entityDE(count-2))%l
                  ! If the curve is a circle, store its data in memory for the entity reading the curve
                  IF (rc(1:3)=='100') THEN
                     CALL getCircleData(entityDE(count-2),t1,t2,x,cNorm)
                     nCircles = nCircles + 1
                     circleData(nCircles,1) = t1
                     circleData(nCircles,2) = t2
                     circleData(nCircles,3:5) = x(:,1)
                     circleData(nCircles,6:8) = x(:,2)
                     circleData(nCircles,9:11) = x(:,3)
                     circleData(nCircles,12:14) = cNorm(1:3,1)
                     ! If nCircles will be out of array bounds of circleData next iteration, resize the array
                     IF (MOD(nCircles,10) == 0) THEN
                        CALL resizeCircleData(nCircles)
                     END IF
                  END IF
                  ! If the curve is a line, store its data in memory for the entity reading the curve
                  IF (rc(1:3)=='110') THEN
                     CALL getLineData(entityDE(count-2),firstEndPoint,&
                                     &secondEndPoint)
                     nLines = nLines + 1
                     lineData(nLines,1:3) = firstEndPoint
                     lineData(nLines,4:6) = secondEndPoint
                     ! If nLines will be out of array bounds of lineData next iteration, resize the array
                     IF (MOD(nLines,10) == 0) THEN
                        CALL resizeLineData(nLines)
                     END IF
                  END IF
                  ! If the curve is a spline, store its data in memory for the entity reading the curve
                  IF (rc(1:3)=='126') THEN
                     CALL getSplineData(entityDE(count-2),splineBreaks,nSplines)
                     nSplines = nSplines + 1
                     splineBreakData(nSplines,1) = splineBreaks
                     ! If nSplines will be out of array bounds of splineData next iteration, resize the array
                     IF (MOD(nSplines,10) == 0) THEN
                        CALL resizeSplineData(nSplines)
                     END IF
                  END IF
               END IF

               ! Grow the entityDE array if it will exceed its indices next iteration
               IF (count-1 > newSize) THEN
                  oldSize = newSize
                  newSize = newSize + 10

                  !-------------------------------------------------------------------------------------
                  ! resize array (couldn't be done with a subroutine, unfortunately)
                  ! dynamically grows the array from it's old size (oldSize) to a bigger new size (newSize)
                  ! SUBROUTINE resizeArray(array,oldSize,newSize)
                  ALLOCATE(array(oldSize))
                  array = entityDE
                  DEALLOCATE(entityDE)
                  ALLOCATE(entityDE(newSize))
                  entityDE(1:oldSize) = array
                  DEALLOCATE(array)
                  ! END SUBROUTINE resizeArray

               END IF

               ! Exit when ';' is encountered
               IF (r102(j:j)==';') THEN
                  EXIT e102
               END IF
            END IF
         END DO
         curveDE2 = curveDE2 + 1
         r102 = parLines(curveDE2)%l
         j10 = 0
      END DO e102
   END SUBROUTINE readCurve

   !-------------------------------------------------------------------------------------
   ! gets the cubic polynomial coefficients of a 2D spline curve
   !------------Variables------------
   ! splineDE is the parametric data entry line describing the spline
   ! splineBreaks is the number of sets of cubic polynomial coefficients
   ! nSplines is the number of splines contained in the curve
   SUBROUTINE getSplineData(splineDE,splineBreaks,nSplines)
      INTEGER, INTENT(IN) :: splineDE,nSplines
      INTEGER, INTENT(OUT) :: splineBreaks

      CHARACTER :: r126*80
      INTEGER :: j,i10,j10,K,M,count,count2,splineDE2,N,A
      INTEGER :: count3,index2
      REAL(pr), ALLOCATABLE :: s(:),w(:),P(:,:)
      REAL(pr) :: V(2)

      ! initialize variables for do loop
      r126 = parLines(splineDE)%l
      count = 0
      count2 = 0
      count3 = 0
      splineDE2 = splineDE
      j10 = 0
      N = 0
      A = 0
      K = 0
      V = 0

e126: DO
         DO j=1,80
            IF (r126(j:j) == ',' .OR. r126(j:j) == ';') THEN
               i10 = j10
               count = count + 1
               j10 = j
               ! Upper index of sum
               IF (count == 2) THEN
                  READ(r126(i10+1:j10-1),*) K
               END IF
               ! Degree of basis function
               IF (count == 3) THEN
                  READ(r126(i10+1:j10-1),*) M
                  N = 1 + K - M
                  ! A + 1 number of knots for first knot sequence
                  A = N + 2 * M
                  ALLOCATE(s(A+1))
                  ALLOCATE(w(K+1))
                  ALLOCATE(P(3,K+1))
               END IF
               IF (count >= 8 .AND. count <= 8 + A) THEN
                  READ(r126(i10+1:j10-1),*) s(count - 7)
               END IF
               IF (count >= 9 + A .AND. count <= 9 + A + K) THEN
                  READ(r126(i10+1:j10-1),*) w(count - 8 - A)
               END IF
               IF (count >= 10 + A + K .AND. count <= 12 + A + 4 * K) THEN
                  count2 = count2 + 1
                  IF (count2 > 1) THEN
                     count3 = count3 + 1
                  END IF
                  index2 = count - count3 - 9 - A - K
                  READ(r126(i10+1:j10-1),*) P(count2,index2)
                  IF (count2 == 3) THEN
                     count2 = 0
                  END IF
               END IF
               IF(count ==  13 + A + 4 * K) THEN
                  READ(r126(i10+1:j10-1),*) V(1)
               END IF
               IF(count ==  14 + A + 4 * K) THEN
                  READ(r126(i10+1:j10-1),*) V(2)
               END IF
            END IF
            IF (r126(j:j)==';') EXIT e126
         END DO
         splineDE2 = splineDE2 + 1
         r126 = parLines(splineDE2)%l
         j10 = 0
      END DO e126
      CALL getCoefficients(s,w,P,K,M,A,splineBreaks,nSplines)
      DEALLOCATE (s,w,P)
      IF(V(2) > 1e12) PRINT *,V

   END SUBROUTINE getSplineData

   !-------------------------------------------------------------------------------------
   ! gets the cubic polynomial coefficients
   !------------Variables------------
   ! si is the set of knot sequences for parameterizing the curve
   ! w is the set of weights for the knot sequences
   ! P is the points on the curve corresponding to the knots and weights
   ! K is the upper index of the sum
   ! M is the degree of the basis function
   ! A is the number of knot sequences
   ! splineBreaks is the number of sets of cubic polynomial coefficients
   ! nSplines is the number of splines contained in the curve
   SUBROUTINE getCoefficients(si,w,P,K,M,A,splineBreaks,nSplines)
      REAL(pr), INTENT(IN) :: si(:),w(:),P(:,:)
      INTEGER, INTENT(IN) :: K,M,A,nSplines
      INTEGER, INTENT(OUT) :: splineBreaks

      INTEGER :: i,j,NURBS2DLength
      REAL(pr) :: s,sMat(4,4),tol,x(3,4),invSMat(4,4),coeff(4,3)

      tol = 1e-12

      NURBS2DLength = 0
      splineBreaks = 0
      x = 0.0

      DO j=0,A-2
         DO i=1,4
            IF (ABS(si(j+2) - si(j+1)) > tol) THEN
               s = si(j+1) + REAL(i) / 5.0 * (si(j+2) - si(j+1))
               x(1:3,(i):(i)) = NURBS2D(s,si,w,P,K,M)
               sMat(i,1) = s**3
               sMat(i,2) = s**2
               sMat(i,3) = s
               sMat(i,4) = 1
               IF (i==4) THEN
                  splineBreaks = splineBreaks + 1
                  NURBS2DLength = splineBreaks * 6
                  IF (NURBS2DLength > splineLength) THEN
                     CALL resizeSplineData2(nSplines,NURBS2DLength + 6)
                     splineLength = NURBS2DLength + 6
                  END IF
                  CALL inverse(sMat,invSMat,4)
                  coeff = MATMUL(invSMat,TRANSPOSE(x))
                  splineData(nSplines*3+1:nSplines*3+3,&
                            &(splineBreaks-1)*6+1) = si(j+1)
                  splineData(nSplines*3+1:nSplines*3+3,&
                            &(splineBreaks-1)*6+2) = si(j+2)
                  splineData(nSplines*3+1:nSplines*3+3,&
                            &(splineBreaks-1)*6+3:&
                            &(splineBreaks-1)*6+6) = TRANSPOSE(coeff)
               END IF
            END IF
         END DO
      END DO
   END SUBROUTINE getCoefficients

   !-------------------------------------------------------------------------------------
   ! gets and stores b-Spline Surface data into memory
   !------------Variables------------
   ! surfaceDE is the data entry line for the revolved surface
   ! curveDE is the data entry line for the 2D curve existing on the revolved surface
   SUBROUTINE bSplineSurface(surfaceDE,curveDE)

      INTEGER, INTENT(IN) :: surfaceDE,curveDE

      INTEGER :: nCircles,nLines,nSplines,i

      ! Get the knot vectors, weights, and control points for the Nonuniform Rational B-Spline Surface
      CALL readNURBS(surfaceDE)
      CALL allocate2DCurves()
      CALL readCurve(curveDE,nCircles,nLines,nSplines)
      IF (gSurfaceDE /= surfaceDE) THEN
         NURBSNCircles(NURBSIndex+1,1) = NURBSCirclesIndex + 1
         NURBSNLines(NURBSIndex+1,1) = NURBSLinesIndex + 1
         NURBSNSplines(NURBSIndex+1,1) = NURBSSplinesIndex + 1
      ELSE
         NURBSIndex = NURBSIndex - 1
      END IF
      NURBSNCircles(NURBSIndex+1,2) = NURBSCirclesIndex + nCircles
      NURBSNLines(NURBSIndex+1,2) = NURBSLinesIndex + nLines
      NURBSNSplines(NURBSIndex+1,2) = NURBSSplinesIndex + nSplines
      DO i=1,nCircles
         NURBSCirclesIndex = NURBSCirclesIndex + 1
         NURBSCircles(NURBSCirclesIndex,1:14) = circleData(i,1:14)
         IF (MOD(NURBSCirclesIndex,10) == 0) THEN
            CALL resizeNURBSCircles()
         END IF
      END DO
      DO i=1,nLines
         NURBSLinesIndex = NURBSLinesIndex + 1
         NURBSLines(NURBSLinesIndex,1:6) = lineData(i,1:6)
         IF (MOD(NURBSLinesIndex,10) == 0) THEN
            CALL resizeNURBSLines()
         END IF
      END DO
      DO i=1,nSplines
         NURBSSplinesIndex = NURBSSplinesIndex + 1
         NURBSSplines((NURBSSplinesIndex-1)*3+1:(NURBSSplinesIndex-1)*3+3,&
                     &1:splineLength) = splineData((i-1)*3+1:(i-1)*3+3,&
                                                  &1:splineLength)
         NURBSSplinesBreaks(NURBSSplinesIndex,1) = splineBreakData(i,1)
         IF (MOD(NURBSSplinesIndex,10) == 0) THEN
            CALL resizeNURBSSplines()
         END IF
      END DO
      CALL deallocate2DCurves()
      NURBSIndex = NURBSIndex + 1
      IF (MOD(NURBSIndex,10) == 0) THEN
         CALL resizeNURBSData()
      END IF
      gSurfaceDE = surfaceDE
   END SUBROUTINE bSplineSurface

   ! reads b-Spline Surface data and stores it in memory
   !------------Variables------------
   ! surfaceDE is the data entry line for the revolved surface
   SUBROUTINE readNURBS(surfaceDE)

      INTEGER, INTENT(IN) :: surfaceDE

      CHARACTER :: r128*80
      INTEGER :: j,i10,j10,K1,K2,M1,M2,count,count2,surfaceDE2,N1,N2,A,B,C
      INTEGER :: count3,index2,maxIndex
      REAL(pr), ALLOCATABLE :: s(:),t(:),w(:),P(:,:)
      REAL(pr) :: U(2),V(2)

      ! initialize variables for do loop
      r128 = parLines(surfaceDE)%l
      count = 0
      count2 = 0
      count3 = 0
      surfaceDE2 = surfaceDE
      j10 = 0
      N1 = 0
      N2 = 0
      A = 0
      B = 0
      C = 0
      U = 0
      V = 0

e128: DO
         DO j=1,80
            IF (r128(j:j) == ',' .OR. r128(j:j) == ';') THEN
               i10 = j10
               count = count + 1
               j10 = j
               ! Upper index of first sum
               IF (count == 2) THEN
                  READ(r128(i10+1:j10-1),*) K1
               END IF
               ! Upper index of second sum
               IF (count == 3) THEN
                  READ(r128(i10+1:j10-1),*) K2
               END IF
               ! Degree of first set of basis functions
               IF (count == 4) THEN
                  READ(r128(i10+1:j10-1),*) M1
               END IF
               ! Degree of second set of basis functions
               IF (count == 5) THEN
                  READ(r128(i10+1:j10-1),*) M2
                  N1 = 1 + K1 - M1
                  N2 = 1 + K2 - M2
                  ! A + 1 number of knots for first knot sequence
                  A = N1 + 2 * M1
                  ! B + 1 number of knots for second knot sequence
                  B = N2 + 2 * M2
                  ! C weights, C control points
                  C = (1 + K1) * (1 + K2)
                  ALLOCATE(s(A+1))
                  ALLOCATE(t(B+1))
                  ALLOCATE(w(C))
                  ALLOCATE(P(3,C))
                  maxIndex = MAX(A+1,B+1,C)
                  IF (maxIndex > NURBSLength) THEN
                     CALL resizeNURBSData2(maxIndex)
                     NURBSLength = maxIndex
                  END IF
                  NURBSIndices(NURBSIndex+1,1) = K1
                  NURBSIndices(NURBSIndex+1,2) = K2
                  NURBSIndices(NURBSIndex+1,3) = M1
                  NURBSIndices(NURBSIndex+1,4) = M2
               END IF
               IF (count >= 11 .AND. count <= 11 + A) THEN
                  READ(r128(i10+1:j10-1),*) s(count - 10)
                  NURBSKnot1(NURBSIndex+1,count - 10) = s(count - 10)
               END IF
               IF (count >= 12 + A .AND. count <= 12 + A + B) THEN
                  READ(r128(i10+1:j10-1),*) t(count - 11 - A)
                  NURBSKnot2(NURBSIndex+1,count - 11 - A) = t(count - 11 - A)
               END IF
               IF (count >= 13 + A + B .AND. count <= 12 + A + B + C) THEN
                  READ(r128(i10+1:j10-1),*) w(count - 12 - A - B)
                  NURBSWeights(NURBSIndex+1,count - 12 - A - B) = &
                  &w(count - 12 - A - B)
               END IF
               IF (count >= 13 + A + B + C .AND. count <= 12 + A + B + 4 * C)&
               &THEN
                  count2 = count2 + 1
                  IF (count2 > 1) THEN
                     count3 = count3 + 1
                  END IF
                  index2 = count - count3 - 12 - A - B - C
                  READ(r128(i10+1:j10-1),*) P(count2,index2)
                  NURBSPoints(3*NURBSIndex+count2,index2) = P(count2,index2)
                  IF (count2 == 3) THEN
                     count2 = 0
                  END IF
               END IF
               IF(count ==  13 + A + B + 4 * C) THEN
                  READ(r128(i10+1:j10-1),*) U(1)
                  NURBSU(NURBSIndex+1,1) = U(1)
               END IF
               IF(count ==  14 + A + B + 4 * C) THEN
                  READ(r128(i10+1:j10-1),*) U(2)
                  NURBSU(NURBSIndex+1,2) = U(2)
               END IF
               IF(count ==  15 + A + B + 4 * C) THEN
                  READ(r128(i10+1:j10-1),*) V(1)
                  NURBSV(NURBSIndex+1,1) = V(1)
               END IF
               IF(count ==  16 + A + B + 4 * C) THEN
                  READ(r128(i10+1:j10-1),*) V(2)
                  NURBSV(NURBSIndex+1,2) = V(2)
               END IF
            END IF
            IF (r128(j:j)==';') EXIT e128
         END DO
         surfaceDE2 = surfaceDE2 + 1
         r128 = parLines(surfaceDE2)%l
         j10 = 0
      END DO e128
      DEALLOCATE (s,t,w,P)
   END SUBROUTINE readNURBS

   !-------------------------------------------------------------------------------------
   ! dynamically grows the 2D Circle data from it's old size (oldSize) to a bigger new size (newSize)
   !------------Variables------------
   ! nCircles is the current number of circles, which is divisible by 10
   SUBROUTINE resizeCircleData(nCircles)
      INTEGER, INTENT(IN) :: nCircles

      INTEGER :: newSize
      REAL(pr), ALLOCATABLE :: array(:,:)

      newSize = nCircles + 10
      ALLOCATE(array(nCircles,14))
      array = circleData
      DEALLOCATE(circleData)
      ALLOCATE(circleData(newSize,14))
      circleData = 0.0
      circleData(1:nCircles,1:14) = array
      DEALLOCATE(array)
   END SUBROUTINE resizeCircleData

   !-------------------------------------------------------------------------------------
   ! dynamically grows the 2D Line data from it's old size (oldSize) to a bigger new size (newSize)
   !------------Variables------------
   ! nLines is the current number of lines, which is divisible by 10
   SUBROUTINE resizeLineData(nLines)
      INTEGER, INTENT(IN) :: nLines

      INTEGER :: newSize
      REAL(pr), ALLOCATABLE :: array(:,:)

      newSize = nLines + 10
      ALLOCATE(array(nLines,6))
      array = lineData
      DEALLOCATE(lineData)
      ALLOCATE(lineData(newSize,6))
      lineData = 0.0
      lineData(1:nLines,1:6) = array
      DEALLOCATE(array)
   END SUBROUTINE resizeLineData

   !-------------------------------------------------------------------------------------
   ! dynamically grows the 2D Spline data from it's old size (oldSize) to a bigger new size (newSize)
   !------------Variables------------
   ! nSplines is the current number of splines, which is divisible by 10
   SUBROUTINE resizeSplineData(nSplines)
      INTEGER,INTENT(IN) :: nSplines

      INTEGER :: newSize
      REAL(pr), ALLOCATABLE :: array(:,:)
      INTEGER, ALLOCATABLE :: arrayi(:,:)

      newSize = nSplines + 10
      ALLOCATE(array(nSplines*3,splineLength))
      array = splineData
      DEALLOCATE(splineData)
      ALLOCATE(splineData(newSize*3,splineLength))
      splineData = 0.0
      splineData(1:nSplines*3,1:splineLength) = array
      DEALLOCATE(array)

      ALLOCATE(arrayi(nSplines,1))
      arrayi = splineBreakData
      DEALLOCATE(splineBreakData)
      ALLOCATE(splineBreakData(newSize,1))
      splineBreakData = 0
      splineBreakData(1:nSplines,1:1) = arrayi
      DEALLOCATE(arrayi)
   END SUBROUTINE resizeSplineData

   !-------------------------------------------------------------------------------------
   ! dynamically grows the Spline data from it's old length (oldLength) to a bigger new length (newLength)
   !------------Variables------------
   ! newLength is the new length for storing coefficients for the cubic polynomial sets
   SUBROUTINE resizeSplineData2(nSplines,newLength)
      INTEGER, INTENT(IN) :: nSplines,newLength

      INTEGER :: oldSize,newSize,oldLength
      REAL(pr), ALLOCATABLE :: array(:,:)

      oldLength = splineLength
      oldSize = INT(REAL(nSplines)/10.0)*10 + 10
      newSize = oldSize

      ALLOCATE(array(3*oldSize,oldLength))
      array = splineData
      DEALLOCATE(splineData)
      ALLOCATE(splineData(3*newSize,newLength))
      splineData = 0.0
      splineData(1:3*oldSize,1:oldLength) = array
      DEALLOCATE(array)

      oldSize = INT(REAL(NURBSSplinesIndex)/10.0)*10 + 10
      newSize = oldSize
      ALLOCATE(array(3*oldSize,oldLength))
      array = NURBSSplines
      DEALLOCATE(NURBSSplines)
      ALLOCATE(NURBSSplines(3*newSize,newLength))
      NURBSSplines = 0.0
      NURBSSplines(1:3*oldSize,1:oldLength) = array
      DEALLOCATE(array)
   END SUBROUTINE resizeSplineData2

   !-------------------------------------------------------------------------------------
   ! dynamically grows the 2D circle curve data lying on a revolved surface made by a line
   SUBROUTINE resizeRevolvedLineCircles()
      INTEGER :: newSize,oldSize
      REAL(pr), ALLOCATABLE :: array(:,:)

      oldSize = revolvedLineCirclesIndex
      newSize = oldSize + 10
      ALLOCATE(array(oldSize,14))
      array = revolvedLineCircles
      DEALLOCATE(revolvedLineCircles)
      ALLOCATE(revolvedLineCircles(newSize,14))
      revolvedLineCircles = 0.0
      revolvedLineCircles(1:oldSize,1:14) = array
      DEALLOCATE(array)
   END SUBROUTINE resizeRevolvedLineCircles

   !-------------------------------------------------------------------------------------
   ! dynamically grows the 2D line curve data lying on a revolved surface made by a line
   SUBROUTINE resizeRevolvedLineLines()
      INTEGER :: newSize,oldSize
      REAL(pr), ALLOCATABLE :: array(:,:)

      oldSize = revolvedLineLinesIndex
      newSize = oldSize + 10
      ALLOCATE(array(oldSize,6))
      array = revolvedLineLines
      DEALLOCATE(revolvedLineLines)
      ALLOCATE(revolvedLineLines(newSize,6))
      revolvedLineLines = 0.0
      revolvedLineLines(1:oldSize,1:6) = array
      DEALLOCATE(array)
   END SUBROUTINE resizeRevolvedLineLines

   !-------------------------------------------------------------------------------------
   ! dynamically grows the revolved lines data from it's old size (oldSize) to a bigger new size (newSize)
   SUBROUTINE resizeRevolvedLinesData()
      INTEGER :: oldSize,newSize
      REAL(pr), ALLOCATABLE :: array(:,:)
      INTEGER, ALLOCATABLE :: arrayi(:,:)

      oldSize = revolvedLinesIndex
      newSize = revolvedLinesIndex + 10
      ALLOCATE(array(3*oldSize,2))
      array = revolvedLineAxes
      DEALLOCATE(revolvedLineAxes)
      ALLOCATE(revolvedLineAxes(3*newSize,2))
      revolvedLineAxes = 0.0
      revolvedLineAxes(1:3*oldSize,1:2) = array
      DEALLOCATE(array)

      ALLOCATE(array(3*oldSize,1))
      array = revolvedLineRadial
      DEALLOCATE(revolvedLineRadial)
      ALLOCATE(revolvedLineRadial(3*newSize,1))
      revolvedLineRadial = 0.0
      revolvedLineRadial(1:3*oldSize,1:1) = array
      DEALLOCATE(array)

      ALLOCATE(array(3*oldSize,2))
      array = revolvedLines
      DEALLOCATE(revolvedLines)
      ALLOCATE(revolvedLines(3*newSize,2))
      revolvedLines = 0.0
      revolvedLines(1:3*oldSize,1:2) = array
      DEALLOCATE(array)

      ALLOCATE(array(oldSize,2))
      array = revolvedLineAngles
      DEALLOCATE(revolvedLineAngles)
      ALLOCATE(revolvedLineAngles(newSize,2))
      revolvedLineAngles = 0.0
      revolvedLineAngles(1:oldSize,1:2) = array
      DEALLOCATE(array)

      ALLOCATE(array(oldSize*3,2))
      array = revolvedLineExtrema
      DEALLOCATE(revolvedLineExtrema)
      ALLOCATE(revolvedLineExtrema(newSize*3,2))
      revolvedLineExtrema = 0.0
      revolvedLineExtrema(1:oldSize*3,1:2) = array
      DEALLOCATE(array)

      ALLOCATE(arrayi(oldSize,2))
      arrayi = revolvedLineNCircles
      DEALLOCATE(revolvedLineNCircles)
      ALLOCATE(revolvedLineNCircles(newSize,2))
      revolvedLineNCircles = 0
      revolvedLineNCircles(1:oldSize,1:2) = arrayi
      DEALLOCATE(arrayi)

      ALLOCATE(arrayi(oldSize,2))
      arrayi = revolvedLineNLines
      DEALLOCATE(revolvedLineNLines)
      ALLOCATE(revolvedLineNLines(newSize,2))
      revolvedLineNLines = 0
      revolvedLineNLines(1:oldSize,1:2) = arrayi
      DEALLOCATE(arrayi)

      ALLOCATE(arrayi(oldSize,2))
      arrayi = revolvedLineNSplines
      DEALLOCATE(revolvedLineNSplines)
      ALLOCATE(revolvedLineNSplines(newSize,2))
      revolvedLineNSplines = 0
      revolvedLineNSplines(1:oldSize,:) = arrayi
      DEALLOCATE(arrayi)
   END SUBROUTINE resizeRevolvedLinesData

   !-------------------------------------------------------------------------------------
   ! dynamically grows the 2D circle curve data lying on a revolved surface made by a circle
   SUBROUTINE resizeRevolvedCircleCircles()
      INTEGER :: newSize,oldSize
      REAL(pr), ALLOCATABLE :: array(:,:)

      oldSize = revolvedCircleCirclesIndex
      newSize = oldSize + 10
      ALLOCATE(array(oldSize,14))
      array = revolvedCircleCircles
      DEALLOCATE(revolvedCircleCircles)
      ALLOCATE(revolvedCircleCircles(newSize,14))
      revolvedCircleCircles = 0.0
      revolvedCircleCircles(1:oldSize,1:14) = array
      DEALLOCATE(array)
   END SUBROUTINE resizeRevolvedCircleCircles

   !-------------------------------------------------------------------------------------
   ! dynamically grows the 2D line curve data lying on a revolved surface made by a line
   SUBROUTINE resizeRevolvedCircleLines()
      INTEGER :: newSize,oldSize
      REAL(pr), ALLOCATABLE :: array(:,:)

      oldSize = revolvedCircleLinesIndex
      newSize = oldSize + 10
      ALLOCATE(array(oldSize,6))
      array = revolvedCircleLines
      DEALLOCATE(revolvedCircleLines)
      ALLOCATE(revolvedCircleLines(newSize,6))
      revolvedCircleLines = 0.0
      revolvedCircleLines(1:oldSize,1:6) = array
      DEALLOCATE(array)
   END SUBROUTINE resizeRevolvedCircleLines

   !-------------------------------------------------------------------------------------
   ! dynamically grows the 2D circle curve data lying on a nonuniform rational B-Spline
   SUBROUTINE resizeNURBSCircles()
      INTEGER :: newSize,oldSize
      REAL(pr), ALLOCATABLE :: array(:,:)

      oldSize = NURBSCirclesIndex
      newSize = oldSize + 10
      ALLOCATE(array(oldSize,14))
      array = NURBSCircles
      DEALLOCATE(NURBSCircles)
      ALLOCATE(NURBSCircles(newSize,14))
      NURBSCircles = 0.0
      NURBSCircles(1:oldSize,1:14) = array
      DEALLOCATE(array)
   END SUBROUTINE resizeNURBSCircles

   !-------------------------------------------------------------------------------------
   ! dynamically grows the 2D line curve data lying on a nonuniform rational B-Spline
   SUBROUTINE resizeNURBSLines()
      INTEGER :: newSize,oldSize
      REAL(pr), ALLOCATABLE :: array(:,:)

      oldSize = NURBSLinesIndex
      newSize = oldSize + 10
      ALLOCATE(array(oldSize,6))
      array = NURBSLines
      DEALLOCATE(NURBSLines)
      ALLOCATE(NURBSLines(newSize,6))
      NURBSLines = 0.0
      NURBSLines(1:oldSize,1:6) = array
      DEALLOCATE(array)
   END SUBROUTINE resizeNURBSLines

   !-------------------------------------------------------------------------------------
   ! dynamically grows the 2D spline curve data lying on a nonuniform rational B-Spline
   SUBROUTINE resizeNURBSSplines()
      INTEGER :: newSize,oldSize
      REAL(pr), ALLOCATABLE :: array(:,:)
      INTEGER, ALLOCATABLE :: arrayi(:,:)

      oldSize = NURBSSplinesIndex
      newSize = oldSize + 10
      ALLOCATE(array(3*oldSize,splineLength))
      array = NURBSSplines
      DEALLOCATE(NURBSSplines)
      ALLOCATE(NURBSSplines(3*newSize,splineLength))
      NURBSSplines = 0.0
      NURBSSplines(1:3*oldSize,1:splineLength) = array
      DEALLOCATE(array)

      ALLOCATE(arrayi(oldSize,1))
      arrayi = NURBSSplinesBreaks
      DEALLOCATE(NURBSSplinesBreaks)
      ALLOCATE(NURBSSplinesBreaks(newSize,1))
      NURBSSplinesBreaks = 0
      NURBSSplinesBreaks(1:oldSize,:) = arrayi
      DEALLOCATE(arrayi)
   END SUBROUTINE resizeNURBSSplines

   !-------------------------------------------------------------------------------------
   ! dynamically grows the revolved circles data from it's old size (oldSize) to a bigger new size (newSize)
   SUBROUTINE resizeRevolvedCirclesData()
      INTEGER :: oldSize,newSize
      REAL(pr), ALLOCATABLE :: array(:,:)
      INTEGER, ALLOCATABLE :: arrayi(:,:)

      oldSize = revolvedCirclesIndex
      newSize = revolvedCirclesIndex + 10
      ALLOCATE(array(3*oldSize,2))
      array = revolvedCircleAxes
      DEALLOCATE(revolvedCircleAxes)
      ALLOCATE(revolvedCircleAxes(3*newSize,2))
      revolvedCircleAxes = 0.0
      revolvedCircleAxes(1:3*oldSize,1:2) = array
      DEALLOCATE(array)

      ALLOCATE(array(3*oldSize,1))
      array = revolvedCircleRadial
      DEALLOCATE(revolvedCircleRadial)
      ALLOCATE(revolvedCircleRadial(3*newSize,1))
      revolvedCircleRadial = 0.0
      revolvedCircleRadial(1:3*oldSize,1:1) = array
      DEALLOCATE(array)

      ALLOCATE(array(oldSize,14))
      array = revolvedCircles
      DEALLOCATE(revolvedCircles)
      ALLOCATE(revolvedCircles(newSize,14))
      revolvedCircles = 0.0
      revolvedCircles(1:oldSize,1:14) = array
      DEALLOCATE(array)

      ALLOCATE(array(oldSize,2))
      array = revolvedCircleAngles
      DEALLOCATE(revolvedCircleAngles)
      ALLOCATE(revolvedCircleAngles(newSize,2))
      revolvedCircleAngles = 0.0
      revolvedCircleAngles(1:oldSize,1:2) = array
      DEALLOCATE(array)

      ALLOCATE(array(oldSize*3,2))
      array = revolvedCircleExtrema
      DEALLOCATE(revolvedCircleExtrema)
      ALLOCATE(revolvedCircleExtrema(newSize*3,2))
      revolvedCircleExtrema = 0.0
      revolvedCircleExtrema(1:oldSize*3,1:2) = array
      DEALLOCATE(array)

      ALLOCATE(arrayi(oldSize,2))
      arrayi = revolvedCircleNCircles
      DEALLOCATE(revolvedCircleNCircles)
      ALLOCATE(revolvedCircleNCircles(newSize,2))
      revolvedCircleNCircles = 0
      revolvedCircleNCircles(1:oldSize,1:2) = arrayi
      DEALLOCATE(arrayi)

      ALLOCATE(arrayi(oldSize,2))
      arrayi = revolvedCircleNLines
      DEALLOCATE(revolvedCircleNLines)
      ALLOCATE(revolvedCircleNLines(newSize,2))
      revolvedCircleNLines = 0
      revolvedCircleNLines(1:oldSize,1:2) = arrayi
      DEALLOCATE(arrayi)

      ALLOCATE(arrayi(oldSize,2))
      arrayi = revolvedCircleNSplines
      DEALLOCATE(revolvedCircleNSplines)
      ALLOCATE(revolvedCircleNSplines(newSize,2))
      revolvedCircleNSplines = 0
      revolvedCircleNSplines(1:oldSize,:) = arrayi
      DEALLOCATE(arrayi)
   END SUBROUTINE resizeRevolvedCirclesData

   !-------------------------------------------------------------------------------------
   ! dynamically grows the NURBS data from it's old size (oldSize) to a bigger new size (newSize)
   SUBROUTINE resizeNURBSData()

      INTEGER :: oldSize,newSize
      REAL(pr), ALLOCATABLE :: array(:,:)
      INTEGER, ALLOCATABLE :: arrayi(:,:)
      INTEGER, ALLOCATABLE :: arrayi2(:)

      oldSize = NURBSIndex
      newSize = NURBSIndex + 10

      ALLOCATE(array(oldSize,NURBSLength))
      array = NURBSKnot1
      DEALLOCATE(NURBSKnot1)
      ALLOCATE(NURBSKnot1(newSize,NURBSLength))
      NURBSKnot1 = 0.0
      NURBSKnot1(1:oldSize,1:NURBSLength) = array
      DEALLOCATE(array)

      ALLOCATE(array(oldSize,NURBSLength))
      array = NURBSKnot2
      DEALLOCATE(NURBSKnot2)
      ALLOCATE(NURBSKnot2(newSize,NURBSLength))
      NURBSKnot2 = 0.0
      NURBSKnot2(1:oldSize,1:NURBSLength) = array
      DEALLOCATE(array)

      ALLOCATE(array(oldSize,NURBSLength))
      array = NURBSWeights
      DEALLOCATE(NURBSWeights)
      ALLOCATE(NURBSWeights(newSize,NURBSLength))
      NURBSWeights = 0.0
      NURBSWeights(1:oldSize,1:NURBSLength) = array
      DEALLOCATE(array)

      ALLOCATE(array(3*oldSize,NURBSLength))
      array = NURBSPoints
      DEALLOCATE(NURBSPoints)
      ALLOCATE(NURBSPoints(3*newSize,NURBSLength))
      NURBSPoints = 0.0
      NURBSPoints(1:3*oldSize,1:NURBSLength) = array
      DEALLOCATE(array)

      ALLOCATE(arrayi(oldSize,4))
      arrayi = NURBSIndices
      DEALLOCATE(NURBSIndices)
      ALLOCATE(NURBSIndices(newSize,4))
      NURBSIndices = 0
      NURBSIndices(1:oldSize,1:4) = arrayi
      DEALLOCATE(arrayi)

      ALLOCATE(array(oldSize,2))
      array = NURBSU
      DEALLOCATE(NURBSU)
      ALLOCATE(NURBSU(newSize,2))
      NURBSU = 0.0
      NURBSU(1:oldSize,1:2) = array
      DEALLOCATE(array)

      ALLOCATE(array(oldSize,2))
      array = NURBSV
      DEALLOCATE(NURBSV)
      ALLOCATE(NURBSV(newSize,2))
      NURBSV = 0.0
      NURBSV(1:oldSize,1:2) = array
      DEALLOCATE(array)

      ALLOCATE(array(oldSize*2,2*NURBSLength))
      array = NURBSPatch
      DEALLOCATE(NURBSPatch)
      ALLOCATE(NURBSPatch(newSize*2,2*NURBSLength))
      NURBSPatch = 0.0
      NURBSPatch(1:oldSize*2,1:2*NURBSLength) = array
      DEALLOCATE(array)

      ALLOCATE(array(oldSize*3,2*NURBSLength))
      array = NURBSPExtrema
      DEALLOCATE(NURBSPExtrema)
      ALLOCATE(NURBSPExtrema(newSize*3,2*NURBSLength))
      NURBSPExtrema = 0.0
      NURBSPExtrema(1:oldSize*3,1:2*NURBSLength) = array
      DEALLOCATE(array)

      ALLOCATE(array(oldSize*3,2))
      array = NURBSExtrema
      DEALLOCATE(NURBSExtrema)
      ALLOCATE(NURBSExtrema(newSize*3,2))
      NURBSExtrema = 0.0
      NURBSExtrema(1:oldSize*3,1:2) = array
      DEALLOCATE(array)

      ALLOCATE(arrayi(oldSize,2))
      arrayi = NURBSNCircles
      DEALLOCATE(NURBSNCircles)
      ALLOCATE(NURBSNCircles(newSize,2))
      NURBSNCircles = 0
      NURBSNCircles(1:oldSize,1:2) = arrayi
      DEALLOCATE(arrayi)

      ALLOCATE(arrayi(oldSize,2))
      arrayi = NURBSNLines
      DEALLOCATE(NURBSNLines)
      ALLOCATE(NURBSNLines(newSize,2))
      NURBSNLines = 0
      NURBSNLines(1:oldSize,1:2) = arrayi
      DEALLOCATE(arrayi)

      ALLOCATE(arrayi(oldSize,2))
      arrayi = NURBSNSplines
      DEALLOCATE(NURBSNSplines)
      ALLOCATE(NURBSNSplines(newSize,2))
      NURBSNSplines = 0
      NURBSNSplines(1:oldSize,:) = arrayi
      DEALLOCATE(arrayi)

      ALLOCATE(arrayi2(oldSize))
      arrayi2 = eIndex
      DEALLOCATE(eIndex)
      ALLOCATE(eIndex(newSize))
      eIndex = 0
      eIndex(1:oldSize) = arrayi2
      DEALLOCATE(arrayi2)
   END SUBROUTINE resizeNURBSData


   !-------------------------------------------------------------------------------------
   ! dynamically grows the NURBS data from it's old length (oldLength) to a bigger new length (newLength)
   !------------Variables------------
   ! newLength is the new length for storing knots, weights, and control points
   SUBROUTINE resizeNURBSData2(newLength)
      INTEGER, INTENT(IN) :: newLength

      INTEGER :: oldSize,newSize,oldLength
      REAL(pr), ALLOCATABLE :: array(:,:)

      oldLength = NURBSLength
      oldSize = INT(REAL(NURBSIndex)/10.0)*10 + 10
      newSize = oldSize

      ALLOCATE(array(oldSize,oldLength))
      array = NURBSKnot1
      DEALLOCATE(NURBSKnot1)
      ALLOCATE(NURBSKnot1(newSize,newLength))
      NURBSKnot1 = 0.0
      NURBSKnot1(1:oldSize,1:oldLength) = array
      DEALLOCATE(array)

      ALLOCATE(array(oldSize,oldLength))
      array = NURBSKnot2
      DEALLOCATE(NURBSKnot2)
      ALLOCATE(NURBSKnot2(newSize,newLength))
      NURBSKnot2 = 0.0
      NURBSKnot2(1:oldSize,1:oldLength) = array
      DEALLOCATE(array)

      ALLOCATE(array(oldSize,oldLength))
      array = NURBSWeights
      DEALLOCATE(NURBSWeights)
      ALLOCATE(NURBSWeights(newSize,newLength))
      NURBSWeights = 0.0
      NURBSWeights(1:oldSize,1:oldLength) = array
      DEALLOCATE(array)

      ALLOCATE(array(3*oldSize,oldLength))
      array = NURBSPoints
      DEALLOCATE(NURBSPoints)
      ALLOCATE(NURBSPoints(3*newSize,newLength))
      NURBSPoints = 0.0
      NURBSPoints(1:3*oldSize,1:oldLength) = array
      DEALLOCATE(array)

      ALLOCATE(array(3*oldSize,oldLength*2))
      array = NURBSPExtrema
      DEALLOCATE(NURBSPExtrema)
      ALLOCATE(NURBSPExtrema(3*newSize,newLength*2))
      NURBSPExtrema = 0.0
      NURBSPExtrema(1:3*oldSize,1:oldLength*2) = array
      DEALLOCATE(array)

      ALLOCATE(array(2*oldSize,oldLength*2))
      array = NURBSPatch
      DEALLOCATE(NURBSPatch)
      ALLOCATE(NURBSPatch(2*newSize,newLength*2))
      NURBSPatch = 0.0
      NURBSPatch(1:2*oldSize,1:oldLength*2) = array
      DEALLOCATE(array)
   END SUBROUTINE resizeNURBSData2

   !-------------------------------------------------------------------------------------
   ! reads the file into the memory
   SUBROUTINE readIntoMemory (filename)
      CHARACTER*(*), INTENT(IN) :: filename
      CHARACTER*80 :: r
      INTEGER :: numData, numPar
      INTEGER :: i, parI, currentParIndex

      numData = 0
      numPar = 0
      i=0
      OPEN (UNIT=11,FILE=TRIM(filename),ERR=123)
      DO                                                      ! count the lines
         READ (UNIT=11, FMT='(A80)', END=124) r
         IF (r(73:73)=='D') numData = numData + 1
         IF (r(73:73)=='P') THEN
            numPar = numPar + 1
            READ (r(65:72),*) parI
         END IF
      END DO

124   ALLOCATE (dataLines(numData), parLines(numPar))     !allocate
      ALLOCATE (table(parI))
      dataLinesSize = numData
      parLinesSize = numPar

      REWIND (11)                                             ! read into memory
      numData = 1
      numPar = 1
      currentParIndex = -1
      DO
         READ (UNIT=11, FMT='(A)', END=125) r
         IF (r(73:73)=='D') THEN
            dataLines(numData)%l = r(1:64)
            numData = numData + 1
         END IF

         IF (r(73:73)=='P') THEN
            READ (r(65:72),*) i
            IF (i.NE.currentParIndex) THEN
               currentParIndex = currentParIndex + 2
               table(currentParIndex) = numPar
            END IF
            parLines(numPar)%l = r(1:68)
            numPar = numPar + 1
         END IF

      END DO
125   CLOSE (11)
      numData = numData - 1
      numPar = numPar - 1
      RETURN

123   PRINT *, 'file not found:', TRIM(filename)

   END SUBROUTINE readIntoMemory

   !-------------------------------------------------------------------------------------
   ! Allocates memory for storing surface data
   SUBROUTINE memoryAllocation()
      !---------------------Revolved Line Data---------------------
      IF(ALLOCATED(revolvedLines))         DEALLOCATE(revolvedLines)
      IF(ALLOCATED(revolvedLineAxes))      DEALLOCATE(revolvedLineAxes)
      IF(ALLOCATED(revolvedLineRadial))      DEALLOCATE(revolvedLineRadial)
      IF(ALLOCATED(revolvedLineAngles))      DEALLOCATE(revolvedLineAngles)
      IF(ALLOCATED(revolvedLineNCircles))      DEALLOCATE(revolvedLineNCircles)
      IF(ALLOCATED(revolvedLineCircles))      DEALLOCATE(revolvedLineCircles)
      IF(ALLOCATED(revolvedLineNLines))      DEALLOCATE(revolvedLineNLines)
      IF(ALLOCATED(revolvedLineLines))      DEALLOCATE(revolvedLineLines)
      IF(ALLOCATED(revolvedLineNSplines))      DEALLOCATE(revolvedLineNSplines)
      IF(ALLOCATED(revolvedLineExtrema))      DEALLOCATE(revolvedLineExtrema)


      ALLOCATE(revolvedLines(30,2))
      ALLOCATE(revolvedLineAxes(30,2))
      ALLOCATE(revolvedLineRadial(30,1))
      ALLOCATE(revolvedLineAngles(10,2))
      ALLOCATE(revolvedLineNCircles(10,2))
      ALLOCATE(revolvedLineCircles(10,14))
      ALLOCATE(revolvedLineNLines(10,2))
      ALLOCATE(revolvedLineLines(10,6))
      ALLOCATE(revolvedLineNSplines(10,2))
      ALLOCATE(revolvedLineExtrema(30,2))
      ! to keep arrays from being resized using variables that were not set
      revolvedLines = 0.0
      revolvedLineAxes = 0.0
      revolvedLineAngles = 0.0
      revolvedLineNCircles = 0
      revolvedLineCircles = 0.0
      revolvedLineNLines = 0
      revolvedLineLines = 0.0
      revolvedLineNSplines = 0
      revolvedLineExtrema = 0.0
      revolvedLineRadial = 0.0
      !--------------------Revolved Circle Data--------------------
      IF(ALLOCATED(revolvedCircles))      DEALLOCATE(revolvedCircles)
      IF(ALLOCATED(revolvedCircleAxes))      DEALLOCATE(revolvedCircleAxes)
      IF(ALLOCATED(revolvedCircleRadial))      DEALLOCATE(revolvedCircleRadial)
      IF(ALLOCATED(revolvedCircleAngles))      DEALLOCATE(revolvedCircleAngles)
      IF(ALLOCATED(revolvedCircleNCircles))      DEALLOCATE(revolvedCircleNCircles)
      IF(ALLOCATED(revolvedCircleCircles))      DEALLOCATE(revolvedCircleCircles)
      IF(ALLOCATED(revolvedCircleNLines))      DEALLOCATE(revolvedCircleNLines)
      IF(ALLOCATED(revolvedCircleLines))      DEALLOCATE(revolvedCircleLines)
      IF(ALLOCATED(revolvedCircleNSplines))      DEALLOCATE(revolvedCircleNSplines)
      IF(ALLOCATED(revolvedCircleExtrema))      DEALLOCATE(revolvedCircleExtrema)

      ALLOCATE(revolvedCircles(10,14))
      ALLOCATE(revolvedCircleAxes(30,2))
      ALLOCATE(revolvedCircleRadial(30,2))
      ALLOCATE(revolvedCircleAngles(10,2))
      ALLOCATE(revolvedCircleNCircles(10,2))
      ALLOCATE(revolvedCircleCircles(10,14))
      ALLOCATE(revolvedCircleNLines(10,2))
      ALLOCATE(revolvedCircleLines(10,6))
      ALLOCATE(revolvedCircleNSplines(10,2))
      ALLOCATE(revolvedCircleExtrema(30,2))
      ! to keep arrays from being resized using variables that were not set
      revolvedCircles = 0.0
      revolvedCircleAxes = 0.0
      revolvedCircleAngles = 0.0
      revolvedCircleNCircles = 0
      revolvedCircleCircles = 0.0
      revolvedCircleNLines = 0
      revolvedCircleLines = 0.0
      revolvedCircleNSplines = 0
      revolvedCircleExtrema = 0.0
      revolvedCircleRadial = 0.0
      !-------------------------NURBS Data-------------------------
      IF(ALLOCATED(NURBSKnot1))      DEALLOCATE(NURBSKnot1)
      IF(ALLOCATED(NURBSKnot2))      DEALLOCATE(NURBSKnot2)
      IF(ALLOCATED(NURBSWeights))      DEALLOCATE(NURBSWeights)
      IF(ALLOCATED(NURBSPoints))      DEALLOCATE(NURBSPoints)
      IF(ALLOCATED(NURBSIndices))      DEALLOCATE(NURBSIndices)
      IF(ALLOCATED(NURBSU))      DEALLOCATE(NURBSU)
      IF(ALLOCATED(NURBSV))      DEALLOCATE(NURBSV)
      IF(ALLOCATED(NURBSNCircles))      DEALLOCATE(NURBSNCircles)
      IF(ALLOCATED(NURBSCircles))      DEALLOCATE(NURBSCircles)
      IF(ALLOCATED(NURBSNLines))      DEALLOCATE(NURBSNLines)
      IF(ALLOCATED(NURBSLines))      DEALLOCATE(NURBSLines)
      IF(ALLOCATED(NURBSNSplines))      DEALLOCATE(NURBSNSplines)
      IF(ALLOCATED(NURBSSplines))      DEALLOCATE(NURBSSplines)
      IF(ALLOCATED(NURBSSplinesBreaks))      DEALLOCATE(NURBSSplinesBreaks)
      IF(ALLOCATED(NURBSPatch))      DEALLOCATE(NURBSPatch)
      IF(ALLOCATED(NURBSPExtrema))      DEALLOCATE(NURBSPExtrema)
      IF(ALLOCATED(NURBSExtrema))      DEALLOCATE(NURBSExtrema)
      IF(ALLOCATED(eIndex))      DEALLOCATE(eIndex)

      ALLOCATE(NURBSKnot1(10,NURBSLength))
      ALLOCATE(NURBSKnot2(10,NURBSLength))
      ALLOCATE(NURBSWeights(10,NURBSLength))
      ALLOCATE(NURBSPoints(30,NURBSLength))
      ALLOCATE(NURBSIndices(10,4))
      ALLOCATE(NURBSU(10,2))
      ALLOCATE(NURBSV(10,2))
      ALLOCATE(NURBSNCircles(10,2))
      ALLOCATE(NURBSCircles(10,14))
      ALLOCATE(NURBSNLines(10,2))
      ALLOCATE(NURBSLines(10,6))
      ALLOCATE(NURBSNSplines(10,2))
      ALLOCATE(NURBSSplines(30,splineLength))
      ALLOCATE(NURBSSplinesBreaks(10,1))
      ALLOCATE(NURBSPatch(20,NURBSLength*2))
      ALLOCATE(NURBSPExtrema(30,NURBSLength*2))
      ALLOCATE(NURBSExtrema(30,2))
      ALLOCATE(eIndex(10))
      ! to keep arrays from being resized using variables that were not set
      NURBSKnot1 = 0.0
      NURBSKnot2 = 0.0
      NURBSWeights = 0.0
      NURBSPoints = 0.0
      NURBSIndices = 0
      NURBSU = 0.0
      NURBSV = 0.0
      NURBSNCircles = 0
      NURBSCircles = 0.0
      NURBSNLines = 0
      NURBSLines = 0.0
      NURBSNSplines = 0
      NURBSSplines = 0.0
      NURBSSplinesBreaks = 0
      NURBSPatch = 0.0
      NURBSPExtrema = 0.0
      NURBSExtrema = 0.0
      eIndex = 0

   END SUBROUTINE memoryAllocation

   ! Allocates memory for storing 2D curve data
   SUBROUTINE allocate2DCurves()
      ALLOCATE(circleData(10,14))
      ALLOCATE(lineData(10,6))
      ALLOCATE(splineData(30,splineLength))
      ALLOCATE(splineBreakData(10,1))
      ! to keep arrays from being resized using variables that were not set
      circleData = 0.0
      lineData = 0.0
      splineData = 0.0
      splineBreakData = 0
   END SUBROUTINE allocate2DCurves

   ! Cleans the memory of 2D curve data
   SUBROUTINE deallocate2DCurves()
      DEALLOCATE(circleData,lineData,splineData,splineBreakData)
   END SUBROUTINE

   !-------------------------------------------------------------------------------------
   ! Cleans the memory
   SUBROUTINE clean_the_memory
      DEALLOCATE (dataLines,parLines,table)
      !---------------------Revolved Line Data---------------------
      DEALLOCATE (revolvedLines,revolvedLineAxes,revolvedLineAngles,&
                 &revolvedLineNCircles,revolvedLineCircles,revolvedLineNLines,&
                 &revolvedLineLines,revolvedLineNSplines,&
                 &revolvedLineRadial,revolvedLineExtrema)
      !---------------------Revolved Circle Data---------------------
      DEALLOCATE (revolvedCircles,revolvedCircleAxes,revolvedCircleAngles,&
                 &revolvedCircleNCircles,revolvedCircleCircles,&
                 &revolvedCircleNLines,revolvedCircleLines,&
                 &revolvedCircleNSplines,&
                 revolvedCircleRadial,revolvedCircleExtrema)
      !-------------------------NURBS Data-------------------------
      DEALLOCATE(NURBSKnot1,NURBSKnot2,NURBSWeights,NURBSPoints,NURBSIndices,&
                &NURBSU,NURBSV,NURBSNCircles,NURBSCircles,NURBSNLines,&
                &NURBSLines,NURBSNSplines,NURBSSplines,NURBSSplinesBreaks,&
                &NURBSPatch,NURBSPExtrema,NURBSExtrema,eIndex)
   END SUBROUTINE clean_the_memory



   SUBROUTINE CADtest(nlocal,wrt_flag,CADmask)

     USE precision
     USE sizes
     USE wlt_vars
     USE io_3d_vars
     USE util_vars
     IMPLICIT NONE

     INTEGER, INTENT (IN) :: nlocal    !number of nodes
     LOGICAL, INTENT (IN) :: wrt_flag
     REAL (pr), DIMENSION(nlocal), INTENT(OUT) :: CADmask

     INTEGER :: i,nnew,n_total     !counter, number of new points
     REAL (pr), DIMENSION(3,nlocal) :: xg
     LOGICAL, DIMENSION(nlocal) :: lg
     


     IF(.NOT.ALLOCATED(i_in)) THEN

!!$        PRINT *, 'WARNING: Allocating i_in within subroutine CADtest'
!!$        PAUSE
        ALLOCATE(i_in(2,nwlt_old))  !is this the proper allocation size? only allocated on the first call
        i_in = .FALSE.
     END IF

     !Initialize local variables
     CADmask  = 0.0_pr
     xg       = 0.0_pr
     lg       = .FALSE.
         

     !Move points to xg and determine number of new points
     nnew = 0
     DO i=1,nlocal
        IF(.NOT. i_in(2,i)) THEN
           nnew=nnew+1
           xg(1:dim,nnew)=x(i,1:dim)
        END IF
     END DO
       
     IF ( .NOT. ANY(i_in) ) THEN  !PROBLEM IN TREES - calls after the first, is CAD_test being called on a reinitialized i_in (eg, has i_in been updated from the database)
        CALL initgeom(dim,file_geom) !If CADtest has been performed already
        PRINT *, 'initgeom called'
     END IF

     CALL surface(xg(1:3,1:nnew),dim,nnew,lg(1:nnew),wrt_flag,file_geom)

     !Put in/out data into global i_in
!!$     PRINT *, 'nlocal',nlocal
!!$     PRINT *, 'nnew', nnew
!!$     PRINT *, 'lg', shape(lg)
!!$     PRINT *, 'i_in', shape(i_in)     

     nnew=0
     DO i=1,nlocal
        IF(.NOT.i_in(2,i)) THEN
           nnew=nnew+1
           i_in(1,i)=lg(nnew)
           i_in(2,i)=.TRUE.
        END IF
     END DO

     WHERE(i_in(1,:))
       CADmask = 1.0_pr
    ELSEWHERE
       CADmask = 0.0_pr
    END WHERE
!!$  
!!$!-----------------------------------------------------
!!$    !Write a file of ALL points and their in/out status - for troubleshooting
!!$    OPEN(UNIT=2, FILE='inout/inout.dat', STATUS='REPLACE', FORM='FORMATTED')
!!$    WRITE(2,'(I8)') nlocal
!!$    WRITE(2,'(F15.7)') x(1:nlocal,1)
!!$    WRITE(2,'(F15.7)') x(1:nlocal,2)
!!$    IF (dim==3)  WRITE(2,'(F15.7)') x(1:nlocal,3)
!!$    WRITE(2,'(F15.7)') i_in(1,1:nlocal)
!!$    CLOSE(2)
!!$!--------------------------------------------

   END SUBROUTINE CADtest

END MODULE threedobject


