MODULE URANS_compressible_KOME
  !
  ! k-Omega URANS model for compressible turbulence
  ! Moscow, August 24, 2017
  ! 
  USE precision
  USE input_file_reader
  USE io_3D_vars
  USE pde
  USE variable_mapping 
  USE share_consts
  USE share_kry
  USE util_mod
  USE vector_util_mod
  USE spectra_module
  USE SGS_util 
  USE equations_compressible 
  USE compressible_mean_vars
  USE curvilinear
  USE curvilinear_mesh !physical coordinates
  USE field !u

  PUBLIC :: &
       KOME_compressible_additional_vars, &
       KOME_compressible_setup, &
       KOME_compressible_finalize_setup, &
       KOME_compressible_read_input, &
       KOME_compressible_rhs, &
       KOME_compressible_Drhs, &
       KOME_compressible_Drhs_diag, &
       KOME_compressible_stats, &
       KOME_compressible_post_process
  
  REAL (pr), PROTECTED, PUBLIC :: Ksgs_init, Kdiss_init, delta_n, C_delta_n

  ! Reynolds-averaged variables
  INTEGER, PROTECTED, PUBLIC :: n_var_K_avg, n_var_W_avg

 
  PRIVATE

  INTEGER, ALLOCATABLE :: &
       n_stress_mdl(:), &   ! Reynolds stresses modeled
       n_stress_avg_mdl(:)  ! Reynolds stresses modeled - Ensemble (Reynolds) Average

  LOGICAL :: external_flow   ! T in external_flow

  LOGICAL   :: adaptW, lowRe_flag, stress_limiter

  REAL (pr) :: Ksgs_min, Kdiss_min
  REAL (pr) :: C0_RANS, C1_RANS, C2_RANS, C3_RANS, C4_RANS, C5_RANS, C6_RANS, C7_RANS     ! constant parameters
  REAL (pr) :: KsgsScale
  
  LOGICAL :: stagnationBound
  LOGICAL :: KOME_debug

  REAL (pr) :: floor

CONTAINS

  SUBROUTINE KOME_compressible_additional_vars( t_local, flag )
    !USE field
    IMPLICIT NONE

    REAL (pr), INTENT (IN) ::  t_local
    INTEGER , INTENT(IN) :: flag ! 0- called during adaption to IC, 1 called during main integration loop
    LOGICAL, SAVE :: first_call = .TRUE.

    INTEGER :: idim, nS
    REAL (pr) :: dt_avg, relax_avg

    REAL (pr), DIMENSION(nwlt,dim) :: v
    REAL (pr), DIMENSION(dim, nwlt, dim) :: du, d2u
    REAL (pr), DIMENSION (nwlt,dim*(dim+1)/2) :: S_ij, S0_ij
    REAL (pr), DIMENSION(nwlt) :: temp, props, chi, fv1, Smod
    INTEGER :: i
    INTEGER, PARAMETER :: meth=1 ! used meth=1 to match calc_vort()

    ! move to case file

    DO i = 1, dim
       v(:,i) = u(:,n_mom(i)) / u(:,n_den)       ! u_i
    END DO
    CALL c_diff_fast( v(:,1:dim), du(1:dim,:,:), d2u(1:dim,:,:), j_lev, nwlt, meth, 10, dim, 1, dim )  
    CALL Sij_velgrad( du(1:dim,:,1:dim), nwlt, S_ij, Smod, .FALSE.)
    nS = dim*(dim+1)/2               ! velocity gradient

    S0_ij = S_ij
    Smod = SUM( S0_ij(:,1:dim), DIM=2 ) / 3.0_pr
    DO idim = 1, dim	
       S0_ij(:,idim) = S0_ij(:,idim) - Smod 
    END DO

    Smod =  SQRT( 2.0_pr*SUM( S0_ij(:,1:dim)**2, DIM=2) + &
                  4.0_pr*SUM( S0_ij(:,dim+1:nS)**2, DIM=2) )

    !u(:, n_var_MTB) = u(:, n_var_K)/(u(:, n_var_W)/u(:, n_den))
    IF (external_flow) THEN
      u(:, n_var_MTB) = MAX(0.0_pr,u(:,n_var_K))/MAX(MAX(Kdiss_init,u(:,n_var_W)/u(:,n_den)),C7_RANS*Smod) 
    ELSE
      u(:, n_var_MTB) = MAX(0.0_pr,u(:,n_var_K))/MAX(MAX(Kdiss_min,u(:,n_var_W)/u(:,n_den)),C7_RANS*Smod) 
    END IF

    IF ( saveReyStrMdl ) THEN
       ! Reynolds stresses (rho*tau_ij)
       DO idim = 1, nS
          u(:,n_stress_mdl(idim)) = 2.0_pr*u(:,n_var_MTB)*S0_ij(:,idim)
       END DO
       DO idim = 1, dim
          !u(:,n_stress_mdl(idim)) = u(:,n_stress_mdl(idim)) - 2.0_pr/REAL(dim,pr)*MAX(0.0_pr,u(:,n_var_K))
          u(:,n_stress_mdl(idim)) = u(:,n_stress_mdl(idim)) - 2.0_pr/3.0_pr*MAX(0.0_pr,u(:,n_var_K))
       END DO
    END IF !saveReyStrMdl

    ! Reynolds averaged vars

    dt_avg = ABS( t_local - avg_timestart )
    IF (exp_avg) THEN
      relax_avg = dt / avg_timescale
    ELSE
      relax_avg = dt / (dt_avg + dt)
    END IF

    IF (saveReyAvgRANS ) THEN
      IF( (restart_ReyAvg .OR. IC_restart_mode==0) .AND. first_call ) THEN

        IF (saveCfyPlus) THEN 
           u(:,n_var_cf_avg)    = u(:,n_var_cf)
           u(:,n_var_yPlus_avg) = u(:,n_var_yPlus)
        END IF

        u(:,n_var_K_avg)        = u(:,n_var_K)
        u(:,n_var_W_avg)        = u(:,n_var_W)
        u(:,n_var_MTB_avg)      = u(:,n_var_MTB)
      END IF

      IF (saveCfyPlus) THEN 
         u(:,n_var_cf_avg)     =    ( 1.0_pr - relax_avg ) * u(:,n_var_cf_avg)    + ( relax_avg ) * u(:,n_var_cf) 
         u(:,n_var_yPlus_avg) = ( 1.0_pr - relax_avg ) * u(:,n_var_yPlus_avg) + ( relax_avg ) * u(:,n_var_yPlus)
      END IF

      u(:,n_var_K_avg)   =   ( 1.0_pr - relax_avg ) * u(:,n_var_K_avg)   + ( relax_avg ) * u(:,n_var_K)     
      u(:,n_var_W_avg)   =   ( 1.0_pr - relax_avg ) * u(:,n_var_W_avg)   + ( relax_avg ) * u(:,n_var_W)     
      u(:,n_var_MTB_avg) = ( 1.0_pr - relax_avg ) * u(:,n_var_MTB_avg) + ( relax_avg ) * u(:,n_var_MTB) 
    END IF !saveReyAvgRANS

    IF ( saveReyStrMdl ) THEN
      IF( (restart_ReyAvg .OR. IC_restart_mode==0) .AND. first_call ) THEN
        DO idim = 1, nS
           u(:,n_stress_avg_mdl(idim)) = u(:,n_stress_mdl(idim)) 
        END DO
      END IF

      DO idim = 1, nS
         u(:,n_stress_avg_mdl(idim)) = ( 1.0_pr - relax_avg ) * u(:,n_stress_avg_mdl(idim)) + ( relax_avg )  * u(:,n_stress_mdl(idim)) 
      END DO
    END IF !saveReyStrMdl


    first_call = .FALSE.

  END SUBROUTINE KOME_compressible_additional_vars

  
  SUBROUTINE  KOME_compressible_setup ( floor_local )
  ! In SGS_setup() we setup all the varaibles necessary for SGS 
  ! The following variables must be setup in this routine: n_var_SGS
    USE parallel
    IMPLICIT NONE

    REAL (pr), INTENT(IN) :: floor_local
    
    floor = floor_local
    
    !register_var( var_name, integrated, adapt, interpolate, exact, saved, req_restart, FRONT_LOAD )

    IF (saveCfyPlus) THEN 
       CALL register_var( 'Cf', integrated=.FALSE., adapt=(/.FALSE.,.FALSE./), interpolate=(/.TRUE.,.TRUE./), exact=(/.FALSE.,.FALSE./), saved=.TRUE., req_restart=.TRUE. )
       CALL register_var( 'Yplus', integrated=.FALSE., adapt=(/.FALSE.,.FALSE./), interpolate=(/.TRUE.,.TRUE./), exact=(/.FALSE.,.FALSE./), saved=.TRUE., req_restart=.TRUE. )
      ! Reynolds Averaged integrated variables
      IF (saveReyAvgRANS ) THEN
         CALL register_var( 'Cf_AVG', integrated=.FALSE., adapt=(/.FALSE.,.FALSE./), interpolate=(/.TRUE.,.TRUE./), &
                             exact=(/.FALSE.,.FALSE./), saved=.TRUE., req_restart=.NOT.restart_ReyAvg )
         CALL register_var( 'Yplus_AVG', integrated=.FALSE., adapt=(/.FALSE.,.FALSE./), interpolate=(/.TRUE.,.TRUE./), &
                             exact=(/.FALSE.,.FALSE./), saved=.TRUE., req_restart=.NOT.restart_ReyAvg )
      END IF
    END IF


    IF (saveReyStrMdl ) THEN
    ! Reynolds stresses modeled
      CALL register_var( 'XX_Stress_MDL  ',  integrated=.FALSE.,  adapt=(/.FALSE.,.FALSE./),   interpolate=(/.TRUE.,.TRUE./), &  
           exact=(/.FALSE.,.FALSE./),   saved=.FALSE., req_restart=.NOT.restart_ReyAvg )
      CALL register_var( 'XX_Stress_AVG_MDL  ',  integrated=.FALSE.,  adapt=(/.FALSE.,.FALSE./),   interpolate=(/.TRUE.,.TRUE./), &  
           exact=(/.FALSE.,.FALSE./),   saved=.TRUE., req_restart=.NOT.restart_ReyAvg )
      IF( dim .GE. 2 ) THEN
         CALL register_var( 'XY_Stress_MDL  ',  integrated=.FALSE.,  adapt=(/.FALSE.,.FALSE./),   interpolate=(/.TRUE.,.TRUE./), &  
              exact=(/.FALSE.,.FALSE./),   saved=.FALSE., req_restart=.NOT.restart_ReyAvg )
         CALL register_var( 'XY_Stress_AVG_MDL  ',  integrated=.FALSE.,  adapt=(/.FALSE.,.FALSE./),   interpolate=(/.TRUE.,.TRUE./), &  
              exact=(/.FALSE.,.FALSE./),   saved=.TRUE., req_restart=.NOT.restart_ReyAvg )
         CALL register_var( 'YY_Stress_MDL  ',  integrated=.FALSE.,  adapt=(/.FALSE.,.FALSE./),   interpolate=(/.TRUE.,.TRUE./), &  
              exact=(/.FALSE.,.FALSE./),   saved=.FALSE., req_restart=.NOT.restart_ReyAvg )
         CALL register_var( 'YY_Stress_AVG_MDL  ',  integrated=.FALSE.,  adapt=(/.FALSE.,.FALSE./),   interpolate=(/.TRUE.,.TRUE./), &  
              exact=(/.FALSE.,.FALSE./),   saved=.TRUE., req_restart=.NOT.restart_ReyAvg )
      END IF
      IF( dim .GE. 3 ) THEN
         CALL register_var( 'XZ_Stress_MDL  ',  integrated=.FALSE.,  adapt=(/.FALSE.,.FALSE./),   interpolate=(/.TRUE.,.TRUE./), &  
              exact=(/.FALSE.,.FALSE./),   saved=.FALSE., req_restart=.NOT.restart_ReyAvg )
         CALL register_var( 'XZ_Stress_AVG_MDL  ',  integrated=.FALSE.,  adapt=(/.FALSE.,.FALSE./),   interpolate=(/.TRUE.,.TRUE./), &  
              exact=(/.FALSE.,.FALSE./),   saved=.TRUE., req_restart=.NOT.restart_ReyAvg )
         CALL register_var( 'YZ_Stress_MDL  ',  integrated=.FALSE.,  adapt=(/.FALSE.,.FALSE./),   interpolate=(/.TRUE.,.TRUE./), &  
              exact=(/.FALSE.,.FALSE./),   saved=.FALSE., req_restart=.NOT.restart_ReyAvg )
         CALL register_var( 'YZ_Stress_AVG_MDL  ',  integrated=.FALSE.,  adapt=(/.FALSE.,.FALSE./),   interpolate=(/.TRUE.,.TRUE./), &  
              exact=(/.FALSE.,.FALSE./),   saved=.TRUE., req_restart=.NOT.restart_ReyAvg )
         CALL register_var( 'ZZ_Stress_MDL  ',  integrated=.FALSE.,  adapt=(/.FALSE.,.FALSE./),   interpolate=(/.TRUE.,.TRUE./), &  
              exact=(/.FALSE.,.FALSE./),   saved=.FALSE., req_restart=.NOT.restart_ReyAvg )
         CALL register_var( 'ZZ_Stress_AVG_MDL  ',  integrated=.FALSE.,  adapt=(/.FALSE.,.FALSE./),   interpolate=(/.TRUE.,.TRUE./), &  
              exact=(/.FALSE.,.FALSE./),   saved=.TRUE., req_restart=.NOT.restart_ReyAvg )
      END IF
    END IF

    !to be slpit for KEPS compressible URANS model
    !n_var_SGS = 2 ! two additional scalars rho*K, rho*E
    !CALL register_var( 'Ksgs', integrated=.TRUE., adapt=(/.FALSE.,adaptK/), interpolate=(/.TRUE.,.TRUE./), exact=(/.FALSE.,.FALSE./), saved=.TRUE., req_restart=.TRUE. )
    !CALL register_var( 'Kdiss', integrated=.TRUE., adapt=(/.FALSE.,adaptW/), interpolate=(/.TRUE.,.TRUE./), exact=(/.FALSE.,.FALSE./), saved=.TRUE., req_restart=.TRUE. )

    n_var_SGS = 2 ! two additional scalars rho*K, rho*W
    CALL register_var( 'Ksgs', integrated=.TRUE., adapt=(/adaptK,adaptK/), interpolate=(/.TRUE.,.TRUE./), exact=(/.FALSE.,.FALSE./), saved=.TRUE., req_restart=.TRUE. )
    CALL register_var( 'Komeg', integrated=.TRUE., adapt=(/adaptW,adaptW/), interpolate=(/.TRUE.,.TRUE./), exact=(/.FALSE.,.FALSE./), saved=.TRUE., req_restart=.TRUE. )
    CALL register_var( 'mut', integrated=.FALSE., adapt=(/.FALSE.,.FALSE./), interpolate=(/.TRUE.,.TRUE./), exact=(/.FALSE.,.FALSE./), saved=.TRUE., req_restart=.FALSE. )

    ! Reynolds Averaged integrated variables
    IF (saveReyAvgRANS ) THEN
       CALL register_var( 'Ksgs_AVG', integrated=.FALSE., adapt=(/.FALSE.,.FALSE./), interpolate=(/.TRUE.,.TRUE./), &
            exact=(/.FALSE.,.FALSE./), saved=.TRUE., req_restart=.NOT.restart_ReyAvg )
       CALL register_var( 'Komeg_AVG', integrated=.FALSE., adapt=(/.FALSE.,.FALSE./), interpolate=(/.TRUE.,.TRUE./), &
            exact=(/.FALSE.,.FALSE./), saved=.TRUE., req_restart=.NOT.restart_ReyAvg )
       CALL register_var( 'mut_AVG', integrated=.FALSE., adapt=(/.FALSE.,.FALSE./), interpolate=(/.TRUE.,.TRUE./), &
            exact=(/.FALSE.,.FALSE./), saved=.TRUE., req_restart=.NOT.restart_ReyAvg )
    END IF

  END SUBROUTINE KOME_compressible_setup

  ! get the indices alloted to sgs variables
  SUBROUTINE  KOME_compressible_finalize_setup ()
    USE parallel
    IMPLICIT NONE
    INTEGER :: i

    IF (saveCfyPlus) THEN 
       n_var_cf = get_index('Cf           ' )
       n_var_yPlus = get_index('Yplus           ' )
       IF ( saveReyAvgRANS ) THEN
          n_var_cf_avg = get_index('Cf_AVG')
          n_var_yPlus_avg = get_index('Yplus_AVG')
       END IF
    END IF

    IF ( saveReyStrMdl ) THEN
      ! Reynolds stresses modeled
      IF (ALLOCATED(n_stress_mdl)) DEALLOCATE(n_stress_mdl)
      IF (ALLOCATED(n_stress_avg_mdl)) DEALLOCATE(n_stress_avg_mdl)
      ALLOCATE( n_stress_mdl(dim*(dim+1)/2) )
      ALLOCATE( n_stress_avg_mdl(dim*(dim+1)/2) )
      IF( dim .EQ. 2 )THEN
          n_stress_mdl(1)  = get_index('XX_Stress_MDL') 
          n_stress_mdl(2)  = get_index('YY_Stress_MDL') 
          n_stress_mdl(3)  = get_index('XY_Stress_MDL') 
          n_stress_avg_mdl(1)  = get_index('XX_Stress_AVG_MDL') 
          n_stress_avg_mdl(2)  = get_index('YY_Stress_AVG_MDL') 
          n_stress_avg_mdl(3)  = get_index('XY_Stress_AVG_MDL') 
      END IF
      IF( dim .EQ. 3 )THEN
          n_stress_mdl(1)  = get_index('XX_Stress_MDL') 
          n_stress_mdl(2)  = get_index('YY_Stress_MDL') 
          n_stress_mdl(3)  = get_index('ZZ_Stress_MDL') 
          n_stress_mdl(4)  = get_index('XY_Stress_MDL') 
          n_stress_mdl(5)  = get_index('XZ_Stress_MDL') 
          n_stress_mdl(6)  = get_index('YZ_Stress_MDL') 
          n_stress_avg_mdl(1)  = get_index('XX_Stress_AVG_MDL') 
          n_stress_avg_mdl(2)  = get_index('YY_Stress_AVG_MDL') 
          n_stress_avg_mdl(3)  = get_index('ZZ_Stress_AVG_MDL') 
          n_stress_avg_mdl(4)  = get_index('XY_Stress_AVG_MDL') 
          n_stress_avg_mdl(5)  = get_index('XZ_Stress_AVG_MDL') 
          n_stress_avg_mdl(6)  = get_index('YZ_Stress_AVG_MDL') 
      END IF
    END IF

    n_var_K = get_index('Ksgs           ' )
    n_var_W = get_index('Komeg          ' )
    n_var_MTB = get_index('mut           ' )
    IF ( saveReyAvgRANS ) THEN
       n_var_K_avg = get_index('Ksgs_AVG')
       n_var_W_avg = get_index('Komeg_AVG')
       n_var_MTB_avg = get_index('mut_AVG')
    END IF

    scaleCoeff(n_var_K)  = KsgsScale !1.0_pr
    scaleCoeff(n_var_W)  = 1.0_pr
    scaleCoeff(n_var_MTB)  = 1.0_pr

  END SUBROUTINE KOME_compressible_finalize_setup

  SUBROUTINE  KOME_compressible_read_input()
  !
  ! read variables from input file for this user case 
  !
     USE parallel
     IMPLICIT NONE

     INTEGER   :: io_status, eps_status
     REAL (pr) :: tmp_eps     

     adaptK = .FALSE.
     call input_logical ('adaptK',adaptK,'default', &
                         'adaptK, Adapt on sgs kinetic energy Ksgs')

     adaptW = .FALSE.
     call input_logical ('adaptW',adaptW,'default', &
                         'adaptW, Adapt on Kdiss/Komeg')

     lowRe_flag = .FALSE.
     call input_logical ('lowRe_flag',lowRe_flag,'default', &
                         'lowRe_flag, low-Reynolds formulation for wall bounded turbulence')

     stress_limiter = .TRUE.
     call input_logical ('stress_limiter',stress_limiter,'default', &
                         'stress_limiter for turbulence production (Wilcox 2008)')

     saveCfyPlus = .FALSE.
     call input_logical ('saveCfyPlus',saveCfyPlus,'default',         'saveCfyPlus: if T adds and saves Cf and yPlus fields')

     !k-omega specific constants 

     external_flow = .FALSE.
     call input_logical ('external_flow',external_flow,'default',         'external_flow: if T adds ambient decay in external flows (ONLY for KOME)')
    
     Ksgs_init = 0.0000015_pr
     call input_real ('Ksgs_init_KOME',Ksgs_init,'default', &
          '  Ksgs_init, initial positive value for Ksgs')
     
     Ksgs_min = 0.000000001_pr
     call input_real ('Ksgs_min_KOME',Ksgs_min,'default', &
          'Ksgs_min, minimum value for Ksgs')
     
     Kdiss_init = 0.00001_pr
     call input_real ('Kdiss_init_KOME',Kdiss_init,'default', &
          'Kdiss_init, initial positive value for Komeg')
     
     Kdiss_min = 0.0001_pr
     call input_real ('Kdiss_min_KOME',Kdiss_min,'default', &
          'Kdiss_min, minimum value for Komeg')
     
     delta_n = MINVAL(h(J_MX,:))
     call input_real ('delta_n',delta_n,'default', &
          'delta_n: minimum wall-normal spacing')
     
     C_delta_n = 80.0_pr
     call input_real ('C_delta_n',C_delta_n,'default', &
          'W_wall = C_delta_n * mu / (delta_min^2)')
     
     C0_RANS = 9.0_pr/100.0_pr
     call input_real ('C0_RANS_KOME',C0_RANS,'default', &
          'C0_RANS, constant parameter for nu_t definition')
     
     C1_RANS = 5.0_pr/9.0
     call input_real ('C1_RANS_KOME',C1_RANS,'default', &
          'C1_RANS, first constant parameter at RHS of Komeg equation')
     
     C2_RANS = 3.0_pr/40.0_pr
     call input_real ('C2_RANS_KOME',C2_RANS,'default', &
          'C2_RANS, second constant parameter at RHS of Komeg equation')
     
     C3_RANS = 0.5_pr
     call input_real ('C3_RANS_KOME',C3_RANS,'default', &
          'C3_RANS, constant parameter for turbulent diffusion of Ksgs')
     
     C4_RANS = 0.5_pr
     call input_real ('C4_RANS_KOME',C4_RANS,'default', &
          'C4_RANS, constant parameter for turbulent diffusion of Komeg')
     
     C5_RANS = 0.5_pr
     call input_real ('C5_RANS_KOME',C5_RANS,'default', &
          'C5_RANS, constant parameter for cross-diffusion of Komeg')
     
     C6_RANS = 8.0_pr/9.0_pr
     call input_real ('C6_RANS_KOME',C6_RANS,'default', &
          'C6_RANS, turbulent Prandtl number')
     
     C7_RANS = 2.916666667_pr
     call input_real ('C7_RANS_KOME',C7_RANS,'default', &
          'C7_RANS, constant parameter for stress-limiter (Wilcox 2008)')

     KsgsScale = 1.0_pr
     call input_real    ('KsgsScale',KsgsScale,'default',         '   KsgsScale: scale/coefficient for TKE')

     stagnationBound = .FALSE.
     call input_logical ('stagnationBound',stagnationBound,'default',         'stagnationBound: if T, then use stagnation point bound for eddy viscosity for the k-omega model')

     KOME_debug = .FALSE.
     call input_logical ('KOME_debug',KOME_debug,'default',         'KOME_debug: if T, then print debug info in the KOME module.')
     
   END SUBROUTINE  KOME_compressible_read_input

  SUBROUTINE KOME_compressible_rhs ( int_rhs, u_integrated, meth )
    !
    !-- Form RHS of KOME model equations
    !
    IMPLICIT NONE
    REAL (pr), DIMENSION (ng*ne), INTENT(INOUT) :: int_rhs
    REAL (pr), DIMENSION (ng,ne), INTENT(INOUT)    :: u_integrated ! 1D flat version of integrated variables without BC 
    INTEGER, INTENT(IN) :: meth 

    INTEGER                                 :: i, j, k, idim, shift, tempintd, jshift
    INTEGER                                 :: dimp1, dimp2, dimp3, nS
    INTEGER, DIMENSION(dim,dim)             :: ij2k
    REAL (pr), DIMENSION (ng*ne)            :: inc_rhs          ! incremental rhs
    REAL (pr), DIMENSION (ne*dim,ng,dim)    :: du, d2u
    REAL (pr), DIMENSION (ng,ne*dim)        :: F
    REAL (pr), DIMENSION (ng)               :: Smod, cross, prod, mu_t, mu_t_tmp
    REAL (pr), DIMENSION (ng,dim*(dim+1)/2) :: S_ij, S0_ij, tau_ij, tau_ij_tmp
    REAL (pr), DIMENSION (ng,dim+3)         :: v               ! velocity components + k + omega
    REAL (pr), DIMENSION (ng)               :: props           ! nondimensional dynamic viscosity (1/Re*Sutherland)

    REAL (pr)                               :: tmp1, tmp2, tmp1_old, tmp2_old      ! print

    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc
    INTEGER :: wlt_type, j_df, imin, i_bnd

    LOGICAL, SAVE :: header_flag = .TRUE.
    REAL (pr), ALLOCATABLE, DIMENSION(:,:), SAVE :: bnd_coords_btm
    INTEGER, SAVE :: n_coords_btm = 0
    REAL (pr), SAVE :: R1=0.0_pr, R2=0.0_pr
    REAL (pr), DIMENSION(dim) :: normal_dir !inward unit normal vector at a certain points of bnd_coords
    REAL (pr), DIMENSION(dim) :: tang_dir   !unit tangential vector in flow direction at a certain points of bnd_coords
    REAL (pr), DIMENSION (dim, ng, dim) :: dux
    REAL (pr), DIMENSION (dim, ng, dim) :: dux_dummy

    ! ============= SANITY CHECKS =======================================
    IF(Nspec > 1) THEN
       IF (par_rank.EQ.0) PRINT*,'ERROR: the URANS module is not set up for multiple species, requires modification'
       STOP
    END IF 
    IF(MAXVAL(ABS(int_rhs(n_eng*ng+1:ne*ng))) > 0.0_pr) THEN
      IF(par_rank .EQ. 0) PRINT *,'ERROR URANS compressible rhs: int_rhs /= 0'
      STOP
    END IF
    !===================================================================

    dimp1 = dim + 1                  ! temperature
    dimp2 = dim + 2                  ! k 
    dimp3 = dim + 3                  ! omega
    nS = dim*(dim+1)/2               ! velocity gradient

    u_integrated(:,n_var_K) = MAX(floor,u_integrated(:,n_var_K))

      IF (KOME_debug) THEN
         tmp1 = MINVAL(u_integrated(:,n_var_W)/u_integrated(:,n_den)); tmp1_old = tmp1
         tmp2 = MAXVAL(u_integrated(:,n_var_W)/u_integrated(:,n_den)); tmp2_old = tmp2
         CALL parallel_global_sum( REALMINVAL = tmp1 )
         CALL parallel_global_sum( REALMAXVAL = tmp2 )

         IF (tmp1_old == tmp1) THEN
           i = MINLOC( u_integrated(:,n_var_W)/u_integrated(:,n_den), DIM=1)
           WRITE(*,'(A, E16.6, A, 3(E16.6))'), 'W min ', u_integrated(i,n_var_W)/u_integrated(i,n_den), ' at', u(i, n_map(1:2)), x(i,dim)
         END IF

         IF (tmp2_old == tmp2) THEN
           i = MAXLOC( u_integrated(:,n_var_W)/u_integrated(:,n_den), DIM=1)
           WRITE(*,'(A, E16.6, A, 3(E16.6))'), 'W max ', u_integrated(i,n_var_W)/u_integrated(i,n_den), ' at', u(i, n_map(1:2)), x(i,dim)
         END IF
      END IF

    IF (external_flow) THEN
      u_integrated(:,n_var_W) = MAX(u(:,n_den)*Kdiss_init,u_integrated(:,n_var_W))
    ELSE                                       
      u_integrated(:,n_var_W) = MAX(u(:,n_den)*Kdiss_min,u_integrated(:,n_var_W))
    END IF

    DO i = 1, dim
        v(:,i) = u_integrated(:,n_mom(i)) / u_integrated(:,n_den)       ! u_i
    END DO

    ! temperature
    v(:,dimp1) = (u_integrated(:,n_eng)-0.5_pr*SUM(u_integrated(:,n_mom(1):n_mom(dim))**2,DIM=2)/u_integrated(:,n_den))/cv_in(Nspec)/u_integrated(:,n_den)/F_compress

    ! turbulence energy
!    v(:,dimp2) = u_integrated(:,n_var_K)  / u_integrated(:,n_den)  
    v(:,dimp2) = MAX(0.0_pr,u_integrated(:,n_var_K)) / u_integrated(:,n_den)    

    ! turbulence specific dissipation rate
    v(:,dimp3) = u_integrated(:,n_var_W) / u_integrated(:,n_den)

    ! constant viscosity
    props(:) = mu_in(Nspec)  ! Nspec=1
    ! variable viscosity
    IF ( viscmodel .EQ. viscmodel_sutherland ) THEN  ! mu_in*(1.0_pr+sutherland_const)/(v(:,dimp1)+sutherland_const)*(v(:,dimp1))**(1.5_pr)
       props(:) = mu_in(Nspec) * calc_nondim_sutherland_visc( v(:,dimp1), nwlt, sutherland_const )
    END IF

!    DO i=1,ne
!       PRINT *, 'rhs_in:', MINVAL(ABS(int_rhs((i-1)*ng+1:i*ng))), MAXVAL(ABS(int_rhs((i-1)*ng+1:i*ng)))
!    END DO
!    PRINT *, 'URANS coeffs:', C0_RANS, C1_RANS, C2_RANS, C3_RANS, C4_RANS, C5_RANS, C6_RANS, C7_RANS
!    PRINT *, 'props:', mu_in(Nspec), cp_in(Nspec), cv_in(Nspec), R_in(Nspec), kk_in(Nspec)
!    WRITE(*,'("min(k)=",E12.5," max(k)=",E12.5," min(omega)=",E12.5," max(omega)=",E12.5)') MINVAL(v(:,dimp2)),MAXVAL(v(:,dimp2)),MINVAL(v(:,dimp3)),MAXVAL(v(:,dimp3))
!    WRITE(*,'("min(T)=    ",E12.5," max(T)=    ",E12.5)') MINVAL(v(:,dimp1)),MAXVAL(v(:,dimp1))
!    PRINT *, 'min|T|=',MINVAL(ABS(v(:,dimp1))), 'max|T|=',MAXVAL(ABS(v(:,dimp1)))


    IF (verb_level.GT.0) THEN
       tmp1 = MINVAL( v(:,dimp2) )
       tmp2 = MAXVAL( v(:,dimp2) )
       CALL parallel_global_sum( REALMINVAL=tmp1 )
       CALL parallel_global_sum( REALMAXVAL=tmp2 )
       IF (par_rank.EQ.0) PRINT *,'MINMAX K(:)=', tmp1, tmp2
       tmp1 = MINVAL( v(:,dimp3) )
       tmp2 = MAXVAL( v(:,dimp3) )
       CALL parallel_global_sum( REALMINVAL=tmp1 )
       CALL parallel_global_sum( REALMAXVAL=tmp2 )
       IF (par_rank.EQ.0) PRINT *,'MINMAX W(:)=', tmp1, tmp2
    END IF


    CALL c_diff_fast( v(:,1:dimp3), du(1:dimp3,:,:), d2u(1:dimp3,:,:), j_lev, ng, meth, 10, dimp3, 1, dimp3 )  

    CALL Sij_velgrad( du(1:dim,:,1:dim), ng, S_ij,  Smod, .FALSE.)      ! strain-rate         

    S0_ij = S_ij
    Smod = SUM( S0_ij(:,1:dim), DIM=2 ) / 3.0_pr
    DO idim = 1, dim	
       S0_ij(:,idim) = S0_ij(:,idim) - Smod 
    END DO

    Smod =  SQRT( 2.0_pr*SUM( S0_ij(:,1:dim)**2, DIM=2) + &
                  4.0_pr*SUM( S0_ij(:,dim+1:nS)**2, DIM=2) )

    !cliping omega 
    !v(:,dimp3) = MAX(C7_RANS*Smod,v(:,dimp3)) !Xuan - should not overtwrite original omega, which will be used below !?

    ! cross term for the omega equation
    cross(:) = C5_RANS*MAX(0.0_pr,SUM(du(dimp2,:,1:dim)*du(dimp3,:,1:dim),DIM=2))*u_integrated(:,n_den)/v(:,dimp3) 

    ! turbulent viscosity with stress limiter (Wilcox)
!    mu_t(:) = u_integrated(:,n_var_K)/MAX(v(:,dimp3),C7_RANS*Smod)
    mu_t(:) = MAX(0.0_pr,u_integrated(:,n_var_K))/MAX(v(:,dimp3),C7_RANS*Smod) 

!------------------- Begin special treatments for inviscid top and adiabatic bottom BCs, not a generalized implementation. Needs improvement!------------------
       IF (inviscidTop) THEN
          !Imposing inviscid condition at the top wall
          i_p_face(0) = 1
          DO i=1,dim
             i_p_face(i) = i_p_face(i-1)*3
          END DO
          DO face_type = 0, 3**dim - 1
             face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
             IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
                CALL get_all_indices_by_face (face_type, j_lev, nloc, iloc)
                IF(nloc > 0 ) THEN 
                  IF( face(2) == 1 ) THEN ! inviscid top wall
                         mu_t(iloc(1:nloc)) = 0.0_pr
                  END IF
                END IF
             END IF
          END DO
       END IF

       !Modify temperature derivatives to impose adiabatic BC dT/dn =0. The current version is ONLY valid for 2D geometry.
       IF (adiabaticBtm .AND. time_integration_method /= TIME_INT_Crank_Nicholson) THEN

          !!NOTE: this jobcobian matrix dux = dx_i/dxi_i is incorrect for H-type periodic boundary condition
          !CALL c_diff_fast (u(1:ng, n_map(1:dim)), dux(1:dim,1:ng,1:dim), dux_dummy, j_lev, ng, meth, 10, dim, 1, dim, FORCE_RECTILINEAR = .TRUE.)
          !DO idim = 1, dim
          !  IF( transform_dir(idim) .NE. 1 ) THEN  ! account for non-transformed directions
          !    dux(1:dim,:,idim) = 0.0_pr
          !    dux(idim,:,1:dim) = 0.0_pr
          !    dux(idim,:,idim)  = 1.0_pr
          !  END IF
          !END DO

          DO j = 1, j_lev
             i_p_face(0) = 1
             DO i=1,dim
                i_p_face(i) = i_p_face(i-1)*3
             END DO
             DO face_type = 0, 3**dim - 1
                face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
                IF( face(2) == -1 ) THEN                !adiabatic bottom wall
                   DO wlt_type = MIN(j-1,1),2**dim-1
                      DO j_df = j, j_lev
                         DO k = 1, indx_DB(j_df,wlt_type,face_type,j)%length
                            i_bnd = indx_DB(j_df,wlt_type,face_type,j)%p(k)%i

                              normal_dir = 0.0_pr
                              tang_dir = 0.0_pr
                              tang_dir(1) = curvilinear_coordgrad(1,1,i_bnd)/sqrt(curvilinear_coordgrad(1,1,i_bnd)**2+curvilinear_coordgrad(2,1,i_bnd)**2) !normalized dx/dxi
                              tang_dir(2) = curvilinear_coordgrad(2,1,i_bnd)/sqrt(curvilinear_coordgrad(1,1,i_bnd)**2+curvilinear_coordgrad(2,1,i_bnd)**2) !normalized dy/dxi
                              normal_dir(1) = - tang_dir(2)
                              normal_dir(2) =   tang_dir(1)
                              du(dimp1,i_bnd,2) = -du(dimp1,i_bnd,1)*normal_dir(1)/SIGN(MAX(floor,ABS(normal_dir(2))), normal_dir(2)) !nx*dT/dx+ny*dT/dy=0

                         END DO
                      END DO
                   END DO
                END IF
             END DO
          END DO
       END IF
!------------------- End special treatments for inviscid top and adiabatic bottom BCs, not a generalized implementation. Needs improvement!------------------

    ! Reynolds stresses (rho*tau_ij)
    DO idim = 1, nS
       tau_ij(:,idim) = 2.0_pr*mu_t(:)*S0_ij(:,idim)
    END DO
    DO idim = 1, dim
!       tau_ij(:,idim) = tau_ij(:,idim) - 2.0_pr/REAL(dim,pr)*u_integrated(:,n_var_K)
       tau_ij(:,idim) = tau_ij(:,idim) - 2.0_pr/3.0_pr*MAX(0.0_pr,u_integrated(:,n_var_K))
    END DO

    ! turbulence production (rho*tau_ij*S_ij)
    IF (stagnationBound) THEN
      IF (dim .EQ. 2) THEN
        tmp1 = 1.5_pr
      ELSE IF (dim .EQ. 3) THEN
        tmp1 = sqrt(3.0_pr)
      END IF
      
      DO i=1,ng
        mu_t_tmp(i) = MIN(mu_t(i), u_integrated(i,n_var_K)/MAX(floor,Smod(i))/tmp1)
      END DO

      DO idim = 1, nS
         tau_ij_tmp(:,idim) = 2.0_pr*mu_t_tmp(:)*S0_ij(:,idim)
      END DO
      DO idim = 1, dim
         tau_ij_tmp(:,idim) = tau_ij_tmp(:,idim) - 2.0_pr/3.0_pr*MAX(0.0_pr,u_integrated(:,n_var_K))
      END DO

      prod(:) = SUM( tau_ij_tmp(:,1:dim)*S_ij(:,1:dim), DIM=2 ) + 2.0_pr*SUM( tau_ij_tmp(:,dim+1:nS)*S_ij(:,dim+1:nS), DIM=2 ) ! tau_ij*djdu_i = tau_ij*0.5*djdu_i+tau_ji*0.5didu_j = tau_ij*0.5*(djdu_j+didu_j) = tau_ij*S_ij (invoking tau_ij = tau_ji for the secend equation)
    ELSE
      prod(:) = SUM( tau_ij(:,1:dim)*S_ij(:,1:dim), DIM=2 ) + 2.0_pr*SUM( tau_ij(:,dim+1:nS)*S_ij(:,dim+1:nS), DIM=2 ) ! tau_ij*djdu_i = tau_ij*0.5*djdu_i+tau_ji*0.5didu_j = tau_ij*0.5*(djdu_j+didu_j) = tau_ij*S_ij (invoking tau_ij = tau_ji for the secend equation)
    END IF

          
    ! mapping from (i,j) to 1:nS
    k = dim
    DO i = 1, dim
       ij2k(i,i) = i 
       DO j = i+1, dim
          k = k+1
          ij2k(i,j) = k
          ij2k(j,i) = k
       END DO
    END DO

    ! fluxes components
    F(:,:) = 0.0_pr
    DO j = 1, dim
       jshift = ne*(j-1)

       DO i = 1, dim
          F(:,n_mom(i)+jshift) = tau_ij(:,ij2k(i,j))  ! rho*tau_ij 
       END DO

       F(:,n_eng+jshift)   =  cp_in(Nspec)*mu_t(:)/C6_RANS*F_compress*du(dimp1,:,j)  ! F_compress*Cp*mu_t/Pr_t*dT/dx_j

       F(:,n_var_K+jshift) = -u_integrated(:,n_var_K )*v(:,j) + ( props(:) + C3_RANS*MAX(0.0_pr,u_integrated(:,n_var_K))/v(:,dimp3) )*du(dimp2,:,j)                        
       F(:,n_var_W+jshift) = -u_integrated(:,n_var_W)*v(:,j) + ( props(:) + C4_RANS*MAX(0.0_pr,u_integrated(:,n_var_K))/v(:,dimp3) )*du(dimp3,:,j)                        
    END DO

    ! spatial derivatives of fluxes components
    tempintd = ne*dim
    CALL c_diff_fast(F(:,1:tempintd), du(1:tempintd,:,:), d2u(1:tempintd,:,:), j_lev, ng, meth, 10, tempintd, 1, tempintd)  ! du(i,:,k)=dF_ij/dx_k

    ! divergence of fluxes
    inc_rhs(:) = 0.0_pr
    DO j = 1, dim
       jshift = ne*(j-1)
       DO i = 1, ne
          shift = (i-1)*ng
          inc_rhs(shift+1:shift+ng) = inc_rhs(shift+1:shift+ng) + du(i+jshift,:,j)  ! dF_ij/dx_j
       END DO
    END DO

    ! additional source terms for the energy equation !Xuan - substract k equaqtion from energy equation, i.e. eqn (3) in wilcox (2008)
    jshift = (n_eng-1)*ng
    DO i = 1, dim
       shift = (n_mom(i)-1)*ng
       inc_rhs(jshift+1:jshift+ng) = inc_rhs(jshift+1:jshift+ng) + inc_rhs(shift+1:shift+ng)*v(:,i)  ! u_i d(rho tau_ij)/dx_j 
    END DO
!    inc_rhs(jshift+1:jshift+ng) = inc_rhs(jshift+1:jshift+ng) + C0_RANS*u_integrated(:,n_var_K)*v(:,dimp3)  ! beta*rho*k*omega
    inc_rhs(jshift+1:jshift+ng) = inc_rhs(jshift+1:jshift+ng) + C0_RANS*MAX(0.0_pr,u_integrated(:,n_var_K)*v(:,dimp3))  ! beta*rho*k*omega

    ! source terms for the two model equations
    shift=(n_var_K-1)*ng
!    inc_rhs(shift+1:shift+ng) = inc_rhs(shift+1:shift+ng) + prod(:) - C0_RANS*u_integrated(:,n_var_K)*v(:,dimp3)                                                             
    inc_rhs(shift+1:shift+ng) = inc_rhs(shift+1:shift+ng) + prod(:) - C0_RANS*MAX(0.0_pr,u_integrated(:,n_var_K)*v(:,dimp3))                                                             
    ! controlled decay for external flows (Menter)    !!$ambient mass density = 1 
    IF(external_flow) THEN
       inc_rhs(shift+1:shift+ng) = inc_rhs(shift+1:shift+ng) + C0_RANS*u_integrated(1:ng,n_den)*Ksgs_init*Kdiss_init !Xuan
    END IF

    shift=(n_var_W-1)*ng
!    inc_rhs(shift+1:shift+ng) = inc_rhs(shift+1:shift+ng) + cross(:) + C1_RANS*prod(:)*v(:,dimp3)/v(:,dimp2) - C2_RANS*u_integrated(:,n_var_W)*v(:,dimp3)
!    inc_rhs(shift+1:shift+ng) = inc_rhs(shift+1:shift+ng) + cross(:) + C1_RANS*prod(:)*MAX(0.0_pr,v(:,dimp3)/v(:,dimp2)) &
    inc_rhs(shift+1:shift+ng) = inc_rhs(shift+1:shift+ng) + cross(:) + C1_RANS*prod(:)*v(:,dimp3)/MAX(v(:,dimp2),floor) &
                                                                     - C2_RANS*MAX(0.0_pr,u_integrated(:,n_den)*v(:,dimp3))*v(:,dimp3)
    ! controlled decay for external flows (Menter)    !!$ambient mass density = 1 
    IF(external_flow) THEN
       inc_rhs(shift+1:shift+ng) = inc_rhs(shift+1:shift+ng) + C2_RANS*u_integrated(1:ng,n_den)*Kdiss_init**2 !Xuan
    END IF
 
    ! finalizing int_rhs
    int_rhs = int_rhs + inc_rhs


  END SUBROUTINE KOME_compressible_rhs

  SUBROUTINE KOME_compressible_Drhs (int_Drhs, u_p, u_prev, meth )
    !
    !-- Form DRHS of KOME model equations
    !
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: meth
    REAL (pr), DIMENSION (ng*ne), INTENT(INOUT) :: int_Drhs
    REAL (pr), DIMENSION (ng,ne), INTENT(INOUT) :: u_prev           ! u_prev           - previous                            
    REAL (pr), DIMENSION (ng,ne), INTENT(IN) :: u_p                 ! u_p              - perturbation                            

    REAL (pr), DIMENSION (ng*ne) :: inc_Drhs                        ! incremental Drhs
    REAL (pr), DIMENSION (2*ne*dim,ng,dim) :: du, d2u               ! 1...ne*dim       - previous, ne*dim+1...2*ne*dim         - perturbation
    REAL (pr), DIMENSION (ng,2*ne*dim) :: F                         ! 1...ne*dim       - previous, ne*dim+1...2*ne*dim         - perturbation                                                
    REAL (pr), DIMENSION (ng,dim*(dim+1)) :: S_ij, S0_ij, tau_ij    ! 1...dim(dim+1)/2 - previous, dim(dim+1)/2+1...dim(dim+1) - perturbation
    REAL (pr), DIMENSION (ng,dim*(dim+1)) :: tau_ij_tmp             ! 1...dim(dim+1)/2 - previous, dim(dim+1)/2+1...dim(dim+1) - perturbation
    REAL (pr), DIMENSION (ng,2*(dim+3)) :: v                        ! 1...dim+3)       - previous, dim+3+1...2(dim+3)          - perturbation
    REAL (pr), DIMENSION (ng,2) :: Smod, cross, prod, mu_t          ! 1                - previous, 2                           - perturbation
    REAL (pr), DIMENSION (ng,2) :: mu_t_tmp                         ! 1                - previous, 2                           - perturbation
    REAL (pr), DIMENSION (ng) :: props, props_prev                  ! nondimensional dynamic viscosity (1/Re*Sutherland)

    INTEGER :: i, j, k, idim, shift, tempintd, jshift, vshift, Fshift
    INTEGER :: dimp1, dimp2, dimp3, nS
    INTEGER, DIMENSION(dim,dim) :: ij2k

    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc
    INTEGER :: wlt_type, j_df, imin, i_bnd

    LOGICAL, SAVE :: header_flag = .TRUE.
    REAL (pr), ALLOCATABLE, DIMENSION(:,:), SAVE :: bnd_coords_btm
    INTEGER, SAVE :: n_coords_btm = 0
    REAL (pr), SAVE :: R1=0.0_pr, R2=0.0_pr
    REAL (pr), DIMENSION(dim) :: normal_dir !inward unit normal vector at a certain points of bnd_coords
    REAL (pr), DIMENSION(dim) :: tang_dir   !unit tangential vector in flow direction at a certain points of bnd_coords
    REAL (pr), DIMENSION (dim, ng, dim) :: dux
    REAL (pr), DIMENSION (dim, ng, dim) :: dux_dummy

    REAL (pr)                               :: tmp1

     ! ============= SANITY CHECKS =======================================
    IF(Nspec > 1) THEN
       IF (par_rank.EQ.0) PRINT*,'ERROR: the URANS module is not set up for multple species, requires modification'
       STOP
    END IF 
    IF(MAXVAL(ABS(int_Drhs(n_eng*ng+1:ne*ng))) > 0.0_pr) THEN
      IF(par_rank .EQ. 0) PRINT *,'ERROR URANS compressible Drhs: int_Drhs /= 0'
      STOP
    END IF
    !===================================================================

    dimp1 = dim + 1                  ! temperature
    dimp2 = dim + 2                  ! k 
    dimp3 = dim + 3                  ! omega
    nS = dim*(dim+1)/2               ! velocity gradient

    u_prev(:,n_var_K) = MAX(floor,u_prev(:,n_var_K))
    IF (external_flow) THEN
      u_prev(:,n_var_W) = MAX(u(:,n_den)*Kdiss_init,u_prev(:,n_var_W))
    ELSE                                 
      u_prev(:,n_var_W) = MAX(u(:,n_den)*Kdiss_min,u_prev(:,n_var_W))
    END IF

    vshift = dimp3
    Fshift = ne*dim

    ! velocity vector components
    DO i = 1, dim
       v(:,i) = u_prev(:,n_mom(i))/u_prev(:,n_den)  ! u_i
       v(:,vshift+i) = u_p(:,n_mom(i))/u_prev(:,n_den) - u_p(:,n_den)*u_prev(:,n_mom(i))/u_prev(:,n_den)**2  ! u'_i
    END DO

    ! temperature
    v(:,dimp1) = (u_prev(:,n_eng)-0.5_pr*SUM(u_prev(:,n_mom(1):n_mom(dim))**2,DIM=2)/u_prev(:,n_den))/cv_in(Nspec)/u_prev(:,n_den)/F_compress  ! T
    v(:,vshift+dimp1) = (u_p(:,n_eng)-(u_prev(:,n_eng)-SUM(u_prev(:,n_mom(1):n_mom(dim))**2,DIM=2)/u_prev(:,n_den))/u_prev(:,n_den)*u_p(:,n_den) &  ! T'
                      - SUM(u_prev(:,n_mom(1):n_mom(dim))*u_p(:,n_mom(1):n_mom(dim)),DIM=2)/u_prev(:,n_den))/cv_in(Nspec)/u_prev(:,n_den)/F_compress  

    ! turbulence energy
    v(:,dimp2) = MAX(0.0_pr,SIGN(1.0_pr,u_prev(:,n_var_K)))*u_prev(:,n_var_K)/u_prev(:,n_den)  ! k
    v(:,vshift+dimp2) = MAX(0.0_pr,SIGN(1.0_pr,u_prev(:,n_var_K)))/u_prev(:,n_den)*(u_p(:,n_var_K)-u_p(:,n_den)*u_prev(:,n_var_K)/u_prev(:,n_den))  ! k'

    ! turbulence specific dissipation
    v(:,dimp3) = u_prev(:,n_var_W)/u_prev(:,n_den)  ! omega
    v(:,vshift+dimp3) = u_p(:,n_var_W)/u_prev(:,n_den)-u_p(:,n_den)*v(:,dimp3)/u_prev(:,n_den)  ! omega'

    ! constant viscosity
    props_prev(:) = mu_in(Nspec)  ! Nspec=1
    props(:) = 0.0_pr
    ! variable viscosity
    IF ( viscmodel .EQ. viscmodel_sutherland ) THEN  ! mu_in*(1.0_pr+sutherland_const)/(v(:,dimp1)+sutherland_const)*(v(:,dimp1))**(1.5_pr)
       props_prev(:) = mu_in(Nspec) * calc_nondim_sutherland_visc( v(:,dimp1), nwlt, sutherland_const ) !mu_variable
       props(:) = mu_in(Nspec) * (1.0_pr+sutherland_const)/(v(:,dimp1)+sutherland_const)*v(:,dimp1)**0.5_pr*(1.5_pr - v(:,dimp1)/(v(:,dimp1)+sutherland_const))*v(:,vshift+dimp1) !mu_variable'
    END IF

    ! first spatial derivatives
    tempintd = 2*dimp3
    CALL c_diff_fast(v(:,1:tempintd), du(1:tempintd,:,:), d2u(1:tempintd,:,:), j_lev, ng, meth, 10, tempintd, 1, tempintd)  ! du(i,:,j)=dv_i/dx_j

    ! strain rate
    CALL Sij_velgrad( du(1:dim,:,1:dim), ng, S_ij(:,1:nS), Smod(:,1), .FALSE.)    !S_ij

    S0_ij(:,1:nS) = S_ij(:,1:nS)
    Smod(:,1) = SUM( S0_ij(:,1:dim), DIM=2 ) / 3.0_pr
    DO idim = 1, dim	
       S0_ij(:,idim) = S0_ij(:,idim) - Smod(:,1)
    END DO

    Smod(:,1) =  SQRT( 2.0_pr*SUM( S0_ij(:,1:dim)**2, DIM=2) + &
                  4.0_pr*SUM( S0_ij(:,dim+1:nS)**2, DIM=2) )

    CALL Sij_velgrad( du(vshift+1:vshift+dim,:,1:dim), ng, S_ij(:,nS+1:2*nS), Smod(:,2), .FALSE.)    !S'_ij

    S0_ij(:,nS+1:2*nS) = S_ij(:,nS+1:2*nS)
    Smod(:,2) = SUM( S0_ij(:,nS+1:nS+dim), DIM=2 ) / 3.0_pr
    DO idim = 1, dim	
       S0_ij(:,nS+idim) = S0_ij(:,nS+idim) - Smod(:,2)
    END DO

    Smod(:,2) = 2.0_pr*(SUM(S0_ij(:,1:dim)*S0_ij(:,nS+1:nS+dim), DIM=2) + 2.0_pr*SUM(S0_ij(:,dim+1:nS)*S0_ij(:,nS+dim+1:2*nS), DIM=2))/Smod(:,1) ! |S0|' = 2*S0_ij*S0'_ij/sqrt(2*S0_ij*S0_ij)

    ! clipping omega 
!    v(:,dimp3) = MAX(C7_RANS*Smod(:,1),v(:,dimp3)) 
!    v(:,vshift+dimp3) = u_p(:,n_var_W)/u_prev(:,n_den) - u_p(:,n_den)*v(:,dimp3)/u_prev(:,n_den)  ! omega'

    ! cross term for the omega equation
    cross(:,1) = SUM(du(dimp2,:,1:dim)*du(dimp3,:,1:dim),DIM=2)
    cross(:,2) = C5_RANS*MAX(0.0_pr,SIGN(1.0_pr,cross(:,1)))*(cross(:,1)*(2.0_pr*u_p(:,n_den)/v(:,dimp3)-u_p(:,n_var_W)/v(:,dimp3)**2) &
               + SUM(du(dimp2,:,1:dim)*du(vshift+dimp3,:,1:dim)+du(vshift+dimp2,:,1:dim)*du(dimp3,:,1:dim),DIM=2)*u_prev(:,n_den)/v(:,dimp3))
    cross(:,1) = C5_RANS*MAX(0.0_pr,cross(:,1))*u_prev(:,n_den)/v(:,dimp3)

    ! turbulent viscosity with stress limiter (Wilcox)
    mu_t(:,1) = MAX(v(:,dimp3),C7_RANS*Smod(:,1))             ! omega_tilde

    WHERE( v(:,dimp3) >= C7_RANS*Smod(:,1) )
       mu_t(:,2)=u_p(:,n_var_W)/u_prev(:,n_den)
    ELSEWHERE
       mu_t(:,2)=C7_RANS*(Smod(:,1)*u_p(:,n_den)/u_prev(:,n_den)+Smod(:,2)) 
    END WHERE
    mu_t(:,2) = - mu_t(:,2)/mu_t(:,1)*u_prev(:,n_var_K) + u_p(:,n_var_K) + u_prev(:,n_var_K)*u_p(:,n_den)/u_prev(:,n_den) 
    mu_t(:,2) =  MAX(0.0_pr,SIGN(1.0_pr,u_prev(:,n_var_K)))*mu_t(:,2)/mu_t(:,1)   !mu_t'
    mu_t(:,1) =  MAX(0.0_pr,u_prev(:,n_var_K))/mu_t(:,1)                          !mu_t

!------------------- Begin special treatments for inviscid top and adiabatic bottom BCs, not a generalized implementation. Needs improvement!------------------
       IF (inviscidTop) THEN
          !Imposing inviscid condition at the top wall
          i_p_face(0) = 1
          DO i=1,dim
             i_p_face(i) = i_p_face(i-1)*3
          END DO
          DO face_type = 0, 3**dim - 1
             face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
             IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
                CALL get_all_indices_by_face (face_type, j_lev, nloc, iloc)
                IF(nloc > 0 ) THEN 
                  IF( face(2) == 1 ) THEN ! inviscid top wall
                         mu_t(iloc(1:nloc),1) = 0.0_pr
                         mu_t(iloc(1:nloc),2) = 0.0_pr
                  END IF
                END IF
             END IF
          END DO
       END IF

       !Modify temperature derivatives to impose adiabatic BC dT/dn =0. The current version is ONLY valid for 2D geometry.
       IF (adiabaticBtm .AND. time_integration_method /= TIME_INT_Crank_Nicholson) THEN

          !!NOTE: this jobcobian matrix dux = dx_i/dxi_i is incorrect for H-type periodic boundary condition
          !CALL c_diff_fast (u(1:ng, n_map(1:dim)), dux(1:dim,1:ng,1:dim), dux_dummy, j_lev, ng, meth, 10, dim, 1, dim, FORCE_RECTILINEAR = .TRUE.)
          !DO idim = 1, dim
          !  IF( transform_dir(idim) .NE. 1 ) THEN  ! account for non-transformed directions
          !    dux(1:dim,:,idim) = 0.0_pr
          !    dux(idim,:,1:dim) = 0.0_pr
          !    dux(idim,:,idim)  = 1.0_pr
          !  END IF
          !END DO

          DO j = 1, j_lev
             i_p_face(0) = 1
             DO i=1,dim
                i_p_face(i) = i_p_face(i-1)*3
             END DO
             DO face_type = 0, 3**dim - 1
                face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
                IF( face(2) == -1 ) THEN                !adiabatic bottom wall
                   DO wlt_type = MIN(j-1,1),2**dim-1
                      DO j_df = j, j_lev
                         DO k = 1, indx_DB(j_df,wlt_type,face_type,j)%length
                            i_bnd = indx_DB(j_df,wlt_type,face_type,j)%p(k)%i

                              normal_dir = 0.0_pr
                              tang_dir = 0.0_pr
                              tang_dir(1) = curvilinear_coordgrad(1,1,i_bnd)/sqrt(curvilinear_coordgrad(1,1,i_bnd)**2+curvilinear_coordgrad(2,1,i_bnd)**2) !normalized dx/dxi
                              tang_dir(2) = curvilinear_coordgrad(2,1,i_bnd)/sqrt(curvilinear_coordgrad(1,1,i_bnd)**2+curvilinear_coordgrad(2,1,i_bnd)**2) !normalized dy/dxi
                              normal_dir(1) = - tang_dir(2)
                              normal_dir(2) =   tang_dir(1)
                              du(dimp1,i_bnd,2) = -du(dimp1,i_bnd,1)*normal_dir(1)/SIGN(MAX(floor,ABS(normal_dir(2))), normal_dir(2)) !nx*dT/dx+ny*dT/dy=0
                              du(vshift+dimp1,i_bnd,2) = -du(vshift+dimp1,i_bnd,1)*normal_dir(1)/SIGN(MAX(floor,ABS(normal_dir(2))), normal_dir(2)) !nx*dT'/dx+ny*dT'/dy=0

                         END DO
                      END DO
                   END DO
                END IF
             END DO
          END DO
       END IF
!------------------- End special treatments for inviscid top and adiabatic bottom BCs, not a generalized implementation. Needs improvement!------------------

    ! turbulent stresses
    DO idim = 1, nS
       tau_ij(:,idim) = 2.0_pr*mu_t(:,1)*S0_ij(:,idim)
       tau_ij(:,nS+idim) = 2.0_pr*(mu_t(:,2)*S0_ij(:,idim)+mu_t(:,1)*S0_ij(:,nS+idim))
    END DO
    DO idim = 1, dim
       !tau_ij(:,idim) = tau_ij(:,idim) - 2.0_pr/REAL(dim,pr)*MAX(0.0_pr,u_prev(:,n_var_K))                                           !tau_ij
       !tau_ij(:,nS+idim) = tau_ij(:,nS+idim) - 2.0_pr/REAL(dim,pr)*MAX(0.0_pr,SIGN(1.0_pr,u_prev(:,n_var_K)))*u_p(:,n_var_K)         !tau_ij'
       tau_ij(:,idim) = tau_ij(:,idim) - 2.0_pr/3.0_pr*MAX(0.0_pr,u_prev(:,n_var_K))                                           !tau_ij
       tau_ij(:,nS+idim) = tau_ij(:,nS+idim) - 2.0_pr/3.0_pr*MAX(0.0_pr,SIGN(1.0_pr,u_prev(:,n_var_K)))*u_p(:,n_var_K)         !tau_ij'
    END DO

    IF (stagnationBound) THEN
      IF (dim .EQ. 2) THEN
        tmp1 = 1.5_pr
      ELSE IF (dim .EQ. 3) THEN
        tmp1 = sqrt(3.0_pr)
      END IF
      DO i=1,ng
        mu_t_tmp(i,1) = MIN(mu_t(i,1), u_prev(i,n_var_K)/MAX(floor,Smod(i,1))/tmp1)
        IF (mu_t(i,1) .GT. u_prev(i,n_var_K)/MAX(floor,Smod(i,1))/tmp1 ) THEN
          mu_t_tmp(i,2) = u_p(i,n_var_K)/MAX(floor,Smod(i,1))/tmp1 - u_prev(i,n_var_K)*Smod(i,2)/MAX(floor,Smod(i,1)**2)/tmp1
        ELSE
          mu_t_tmp(i,2) = mu_t(i,2)
        END IF
      END DO
      DO idim = 1, nS
         tau_ij_tmp(:,idim) = 2.0_pr*mu_t_tmp(:,1)*S0_ij(:,idim)
         tau_ij_tmp(:,nS+idim) = 2.0_pr*(mu_t_tmp(:,2)*S0_ij(:,idim)+mu_t_tmp(:,1)*S0_ij(:,nS+idim))
      END DO
      DO idim = 1, dim
         tau_ij_tmp(:,idim) = tau_ij_tmp(:,idim) - 2.0_pr/3.0_pr*MAX(0.0_pr,u_prev(:,n_var_K))                                           !tau_ij
         tau_ij_tmp(:,nS+idim) = tau_ij_tmp(:,nS+idim) - 2.0_pr/3.0_pr*MAX(0.0_pr,SIGN(1.0_pr,u_prev(:,n_var_K)))*u_p(:,n_var_K)         !tau_ij'
      END DO
      prod(:,1) = SUM( tau_ij_tmp(:,1:dim)*S_ij(:,1:dim), DIM=2 ) + 2.0_pr*SUM( tau_ij_tmp(:,dim+1:nS)*S_ij(:,dim+1:nS), DIM=2 )               !prod
      prod(:,2) = SUM( tau_ij_tmp(:,nS+1:nS+dim)*S_ij(:,1:dim) + tau_ij_tmp(:,1:dim)*S_ij(:,nS+1:nS+dim), DIM=2) &
         + 2.0_pr*SUM( tau_ij_tmp(:,nS+dim+1:2*nS)*S_ij(:,dim+1:nS) + tau_ij_tmp(:,dim+1:nS)*S_ij(:,nS+dim+1:2*nS), DIM=2)                     !prod'
    ELSE
      prod(:,1) = SUM( tau_ij(:,1:dim)*S_ij(:,1:dim), DIM=2 ) + 2.0_pr*SUM( tau_ij(:,dim+1:nS)*S_ij(:,dim+1:nS), DIM=2 )               !prod
      prod(:,2) = SUM( tau_ij(:,nS+1:nS+dim)*S_ij(:,1:dim) + tau_ij(:,1:dim)*S_ij(:,nS+1:nS+dim), DIM=2) &
         + 2.0_pr*SUM( tau_ij(:,nS+dim+1:2*nS)*S_ij(:,dim+1:nS) + tau_ij(:,dim+1:nS)*S_ij(:,nS+dim+1:2*nS), DIM=2)                     !prod'
    END IF

    k = dim
    DO i = 1, dim
       ij2k(i,i) = i 
       DO j = i+1, dim
          k = k+1
          ij2k(i,j) = k
          ij2k(j,i) = k
       END DO
    END DO

    F(:,:) = 0.0_pr
    DO j = 1, dim
       jshift=ne*(j-1)
       !
       !fluxes at previous time step
       !
       DO i = 1, dim
          F(:,n_mom(i)+jshift) = tau_ij(:,ij2k(i,j))  ! rho*tau_ij 
       END DO
       F(:,n_eng  +jshift) =  cp_in(Nspec)*mu_t(:,1)/C6_RANS*F_compress*du(dimp1,:,j)
       F(:,n_var_K+jshift) = -u_prev(:,n_var_K)*v(:,j)  + ( props_prev(:) + C3_RANS*u_prev(:,n_var_K)/v(:,dimp3) ) * du(dimp2,:,j)                        
       F(:,n_var_W+jshift) = -u_prev(:,n_var_W)*v(:,j) + ( props_prev(:) + C4_RANS*u_prev(:,n_var_K)/v(:,dimp3) ) * du(dimp3,:,j)                        
       !
       ! perturbation of fluxes
       !
       DO i = 1, dim
          F(:,Fshift+n_mom(i)+jshift) = tau_ij(:,nS+ij2k(i,j))  ! (rho*tau_ij)' 
       END DO
       F(:,Fshift+n_eng+jshift) = cp_in(Nspec)/C6_RANS*F_compress*(mu_t(:,2)*du(dimp1,:,j)+mu_t(:,1)*du(vshift+dimp1,:,j)) ! F*(Cp mu_t/Pr_t*dT/dx_j)'

       F(:,Fshift+n_var_K+jshift)  = -u_p(:,n_var_K)*v(:,j) -u_prev(:,n_var_K)*v(:,vshift+j)  &
            + ( props_prev(:) + C3_RANS*u_prev(:,n_var_K)/v(:,dimp3) ) * du(vshift+dimp2,:,j)  &                       
            + ( props(:)      + C3_RANS*(u_p(:,n_var_K)/v(:,dimp3)+u_prev(:,n_var_K)/v(:,dimp3)*u_p(:,n_den)/u_prev(:,n_den) &
                                                                   - u_prev(:,n_var_K)*u_p(:,n_var_W)/u_prev(:,n_den)/v(:,dimp3)**2) ) * du(dimp2,:,j)

       F(:,Fshift+n_var_W+jshift)  = -u_p(:,n_var_W)*v(:,j) -u_prev(:,n_var_W)*v(:,vshift+j)  &
            + ( props_prev(:) + C4_RANS*u_prev(:,n_var_K)/v(:,dimp3) ) * du(vshift+dimp3,:,j)  &                       
            + ( props(:)      + C4_RANS*(u_p(:,n_var_K)/v(:,dimp3)+u_prev(:,n_var_K)/v(:,dimp3)*u_p(:,n_den)/u_prev(:,n_den) &
                                                                   - u_prev(:,n_var_K)*u_p(:,n_var_W)/u_prev(:,n_den)/v(:,dimp3)**2) ) * du(dimp3,:,j)                      
    END DO

    tempintd=2*ne*dim
    CALL c_diff_fast(F(:,1:tempintd), du(1:tempintd,:,:), d2u(1:tempintd,:,:), j_lev, ng, meth, 10, tempintd, 1, tempintd)  !du(i,:,k)=dF_ij/dx_k

    inc_Drhs(:) = 0
    DO j = 1, dim
       jshift=ne*(j-1)
       DO i = 1, ne
          shift=(i-1)*ng
          inc_Drhs(shift+1:shift+ng) = inc_Drhs(shift+1:shift+ng) + du(i+jshift,:,j)        ! dF_ij/dx_j previous
       END DO
    END DO

    jshift=(n_eng-1)*ng
    DO i = 1, dim
       shift=(n_mom(i)-1)*ng
       int_Drhs(jshift+1:jshift+ng) = int_Drhs(jshift+1:jshift+ng) + inc_Drhs(shift+1:shift+ng)*v(:,vshift+i)    ! u'_i d(rho tau_ij)/dx_j 
    END DO

    ! adding the divergence of fluxes perturbation
    inc_Drhs(:) = 0
    DO j = 1, dim
       jshift=ne*(j-1)
       DO i = 1, ne
          shift=(i-1)*ng
          inc_Drhs(shift+1:shift+ng) = inc_Drhs(shift+1:shift+ng) + du(Fshift+i+jshift,:,j)        ! dF_ij/dx_j preturbation
       END DO
    END DO

    ! temperature
    jshift=(n_eng-1)*ng
    DO i = 1, dim
       shift=(n_mom(i)-1)*ng
       inc_Drhs(jshift+1:jshift+ng) = inc_Drhs(jshift+1:jshift+ng) + inc_Drhs(shift+1:shift+ng)*v(:,i)    ! u_i d(rho tau_ij)'/dx_j
    END DO
    inc_Drhs(jshift+1:jshift+ng) = inc_Drhs(jshift+1:jshift+ng) + C0_RANS*MAX(0.0_pr,SIGN(1.0_pr,u_prev(:,n_var_K)*v(:,dimp3))) &
                                 * (u_p(:,n_var_K)*v(:,dimp3)+u_prev(:,n_var_K)*v(:,vshift+dimp3))       ! beta*((rho*k)'*omega+(rho*k)*omega')

    ! turbulence energy
    shift=(n_var_K-1)*ng
    inc_Drhs(shift+1:shift+ng) = inc_Drhs(shift+1:shift+ng) + prod(:,2) - C0_RANS*MAX(0.0_pr,SIGN(1.0_pr,u_prev(:,n_var_K)*v(:,dimp3))) &
                               * (u_p(:,n_var_K)*v(:,dimp3)+u_prev(:,n_var_K)*v(:,vshift+dimp3)) ! prod'-beta*((rho*k)'*omega+(rho*k)*omega')

    ! controlled decay for external flows (Menter)    !!$ambient mass density = 1 
    IF(external_flow) THEN
       inc_Drhs(shift+1:shift+ng) = inc_Drhs(shift+1:shift+ng) + C0_RANS*u_p(1:ng,n_den)*Ksgs_init*Kdiss_init !Xuan
    END IF

    ! turbulence specific dissipation
    shift=(n_var_W-1)*ng
    inc_Drhs(shift+1:shift+ng) = inc_Drhs(shift+1:shift+ng) &
!                               + C1_RANS*(prod(:,2)*MAX(0.0_pr,v(:,dimp3)/v(:,dimp2)) +prod(:,1)*MAX(0.0_pr,SIGN(1.0_pr,v(:,dimp3)/v(:,dimp2))) &
                               + C1_RANS*(prod(:,2)*v(:,dimp3)/MAX(v(:,dimp2),floor) +prod(:,1)*MAX(0.0_pr,SIGN(1.0_pr,v(:,dimp3)/v(:,dimp2))) &
                               * (u_p(:,n_var_W)/u_prev(:,n_var_K)-u_prev(:,n_den)*v(:,dimp3)*u_p(:,n_var_K)/u_prev(:,n_var_K)**2)) &
                               - C2_RANS*(2.0_pr*v(:,dimp3)*u_p(:,n_var_W)-v(:,dimp3)**2*u_p(:,n_den)) + cross(:,2) 

    ! controlled decay for external flows (Menter)    !!$ambient mass density = 1 
    IF(external_flow) THEN
       inc_Drhs(shift+1:shift+ng) = inc_Drhs(shift+1:shift+ng) + C2_RANS*u_p(1:ng,n_den)*Kdiss_init**2 !Xuan
    END IF

    ! finalizing int_Drhs
    int_Drhs = int_Drhs + inc_Drhs


  END SUBROUTINE KOME_compressible_Drhs

  SUBROUTINE KOME_compressible_Drhs_diag (int_Drhs_diag,u_prev,meth)
    !
    !-- Form DRHS_diag of KOME model equations
    !
    USE variable_mapping
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: meth
    REAL (pr), DIMENSION (ng*ne), INTENT(INOUT) :: int_Drhs_diag
    REAL (pr), DIMENSION (ng,ne), INTENT(INOUT) :: u_prev
    REAL (pr), DIMENSION (ng,dim) :: du_diag, d2u_diag
    REAL (pr), DIMENSION (dim+3,ng,dim) :: du, d2u   
    REAL (pr), DIMENSION (ng,dim*(dim+1)) :: S_ij, S0_ij  ! 1...dim(dim+1)/2 - previous, dim(dim+1)/2+1...dim(dim+1) - perturbation
    REAL (pr), DIMENSION (ng, dim+3) :: v_prev
    REAL (pr), DIMENSION (ng,2) :: mu_t, mu_t_tmp        ! mu_t(:,1) previous time; mu_t(:,2)= d mu_t/d (rho k) or mu_t(:,2)= d (omega mu_t/k) /d (rho omega)
    REAL (pr), DIMENSION (ng) :: Smod, cross, tmp        
    REAL (pr), DIMENSION (ng) :: props_prev     !, props
    INTEGER :: i, shift, idim
    INTEGER :: dimp1, dimp2, dimp3, nS

    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc
    INTEGER :: wlt_type, j, k, j_df, imin, i_bnd

    REAL (pr)                               :: tmp1

     ! ============= SANITY CHECKS =======================================
    IF(Nspec > 1) THEN
       IF (par_rank.EQ.0) PRINT*,'ERROR: the URANS module is not set up for multple species, requires modification'
       STOP
    END IF 
    IF(MAXVAL(ABS(int_Drhs_diag(n_eng*ng+1:ne*ng))) > 0.0_pr) THEN
      IF(par_rank .EQ. 0) PRINT *,'ERROR URANS compressible Drhs_diag: int_Drhs_diag /= 0'
      STOP
    END IF
    !===================================================================

    dimp1 = dim + 1                  ! temperature
    dimp2 = dim + 2                  ! k 
    dimp3 = dim + 3                  ! omega
    nS = dim*(dim+1)/2               ! velocity gradient

    u_prev(:,n_var_K) = MAX(floor,u_prev(:,n_var_K))
    IF (external_flow) THEN
      u_prev(:,n_var_W) = MAX(u(:,n_den)*Kdiss_init,u_prev(:,n_var_W))
    ELSE                                 
      u_prev(:,n_var_W) = MAX(u(:,n_den)*Kdiss_min,u_prev(:,n_var_W))
    END IF

    CALL c_diff_diag(du_diag, d2u_diag, j_lev, ng, meth, meth, -11)

    ! velocity vector components
    DO i = 1, dim
       v_prev(:,i) = u_prev(:,n_mom(i))/u_prev(:,n_den)  ! u_i
    END DO

    ! temperature
    v_prev(:,dimp1) = (u_prev(:,n_eng)-0.5_pr*SUM(u_prev(:,n_mom(1):n_mom(dim))**2,DIM=2)/u_prev(:,n_den))/cv_in(Nspec)/u_prev(:,n_den)/F_compress  !T

    ! turbulence energy
    v_prev(:,dimp2) = MAX(0.0_pr,SIGN(1.0_pr,u_prev(:,n_var_K)))*u_prev(:,n_var_K)/u_prev(:,n_den)  ! k

    ! turbulence specific dissipation
    v_prev(:,dimp3) = u_prev(:,n_var_W)/u_prev(:,n_den)  ! omega

    ! constant viscosity
    props_prev(:) = mu_in(Nspec)  ! Nspec=1
!    props(:) = 0.0_pr  ! d (mu) / dT
    ! variable viscosity
    IF ( viscmodel .EQ. viscmodel_sutherland ) THEN  ! mu_in*(1.0_pr+sutherland_const)/(v(:,dimp1)+sutherland_const)*(v(:,dimp1))**(1.5_pr)
       props_prev(:) = mu_in(Nspec) * calc_nondim_sutherland_visc( v_prev(:,dimp1), nwlt, sutherland_const )
!       props(:) = mu_in(Nspec) * (1.0_pr+sutherland_const)/(v_prev(:,dimp1)+sutherland_const)*v_prev(:,dimp1)**0.5_pr*(1.5_pr-v_prev(:,dimp1)/(v_prev(:,dimp1)+sutherland_const))
    END IF

    ! first spatial derivatives at previous time
    CALL c_diff_fast( v_prev(:,1:dimp3), du(1:dimp3,:,:), d2u(1:dimp3,:,:), j_lev, ng, meth, 10, dimp3, 1, dimp3 )

    ! strain rate
    CALL Sij_velgrad( du(1:dim,:,1:dim), ng, S_ij(:,1:nS),  Smod(:), .FALSE.)   ! S_ij at previous time 

    S0_ij = S_ij
    Smod = SUM( S0_ij(:,1:dim), DIM=2 ) / 3.0_pr
    DO idim = 1, dim	
       S0_ij(:,idim) = S0_ij(:,idim) - Smod 
    END DO

    Smod =  SQRT( 2.0_pr*SUM( S0_ij(:,1:dim)**2, DIM=2) + &
                  4.0_pr*SUM( S0_ij(:,dim+1:nS)**2, DIM=2) )

    ! clipping omega 
!    v_prev(:,dimp3) = MAX(C7_RANS*Smod,v_prev(:,dimp3)) 

    ! cross term for the omega equation
    tmp(:) = SUM(du(dimp2,:,1:dim)*du(dimp3,:,1:dim),DIM=2)
    cross(:) = C5_RANS*MAX(0.0_pr,SIGN(1.0_pr,tmp(:)))*(-tmp(:)/v_prev(:,dimp3)**2 + SUM(du(dimp2,:,1:dim)*du_diag(:,1:dim),DIM=2)/v_prev(:,dimp3)) 

    ! turbulent viscosity with stress limiter (Wilcox)
    mu_t(:,1) = MAX(0.0_pr,SIGN(1.0_pr,u_prev(:,n_var_K)))*u_prev(:,n_var_K)/MAX(v_prev(:,dimp3),C7_RANS*Smod(:))   ! mu_t
    mu_t(:,2) = MAX(0.0_pr,SIGN(1.0_pr,u_prev(:,n_var_K)))/MAX(v_prev(:,dimp3),C7_RANS*Smod(:))   ! d(mu_t)/d(rho k)

    IF (stagnationBound) THEN
      IF (dim .EQ. 2) THEN
        tmp1 = 1.5_pr
      ELSE IF (dim .EQ. 3) THEN
        tmp1 = sqrt(3.0_pr)
      END IF
      DO i=1,ng
        mu_t_tmp(i,1) = MIN(mu_t(i,1), u_prev(i,n_var_K)/MAX(floor,Smod(i))/tmp1)
        IF (mu_t(i,1) .GT. u_prev(i,n_var_K)/MAX(floor,Smod(i))/tmp1 ) THEN
          mu_t_tmp(i,2) = 1.0_pr/MAX(floor,Smod(i))/tmp1 
        ELSE
          mu_t_tmp(i,2) = mu_t(i,2)
        END IF
      END DO
    ELSE
      mu_t_tmp(i,2) = mu_t(i,2)
    END IF

!------------------- Begin special treatments for inviscid top, not a generalized implementation. Needs improvement!------------------
       IF (inviscidTop) THEN
          !Imposing inviscid condition at the top wall
          i_p_face(0) = 1
          DO i=1,dim
             i_p_face(i) = i_p_face(i-1)*3
          END DO
          DO face_type = 0, 3**dim - 1
             face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
             IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
                CALL get_all_indices_by_face (face_type, j_lev, nloc, iloc)
                IF(nloc > 0 ) THEN 
                  IF( face(2) == 1 ) THEN ! inviscid top wall
                         mu_t(iloc(1:nloc),1) = 0.0_pr
                         mu_t(iloc(1:nloc),2) = 0.0_pr
                  END IF
                END IF
             END IF
          END DO
       END IF
!------------------- End special treatments for inviscid top, not a generalized implementation. Needs improvement!------------------

    ! momentum
    DO i = 1, dim
       shift=(n_mom(i)-1)*ng
       int_Drhs_diag(shift+1:shift+ng) = int_Drhs_diag(shift+1:shift+ng) &
!                                       + mu_t(:,1)/u_prev(:,n_den)*( SUM(d2u_diag(:,:),DIM=2) - 1.0_pr/REAL(dim,pr)*d2u_diag(:,i))     
                                       !+ mu_t(:,1)/u_prev(:,n_den)*( SUM(d2u_diag(:,:),DIM=2) + d2u_diag(:,i) - 2.0_pr/REAL(dim,pr)*d2u_diag(:,i))     
                                       + mu_t(:,1)/u_prev(:,n_den)*( SUM(d2u_diag(:,:),DIM=2) + d2u_diag(:,i) - 2.0_pr/3.0_pr*d2u_diag(:,i))     
    END DO

    ! energy
    shift=(n_eng-1)*ng
    int_Drhs_diag(shift+1:shift+ng) = int_Drhs_diag(shift+1:shift+ng) &
!                                    + cp_in(Nspec)/C6_RANS*F_compress*mu_t(:,1)/u_prev(:,n_den)*SUM(d2u_diag(:,:),DIM=2)
                                    + cp_in(Nspec)/cv_in(Nspec)/C6_RANS/u_prev(:,n_den)*mu_t(:,1)*SUM(d2u_diag(:,:),DIM=2)

    ! turbulence energy
    shift=(n_var_K-1)*ng
    int_Drhs_diag(shift+1:shift+ng) = int_Drhs_diag(shift+1:shift+ng) &
                                    - SUM(v_prev(:,1:dim)*du_diag(:,1:dim),DIM=2) &
                                    !+ 2.0_pr*mu_t(:,2)*(SUM( S0_ij(:,1:dim)*S_ij(:,1:dim),DIM=2)+2.0_pr*SUM( S0_ij(:,dim+1:nS)*S_ij(:,dim+1:nS),DIM=2)) &
                                    + 2.0_pr*mu_t_tmp(:,2)*(SUM( S0_ij(:,1:dim)*S_ij(:,1:dim),DIM=2)+2.0_pr*SUM( S0_ij(:,dim+1:nS)*S_ij(:,dim+1:nS),DIM=2)) &
                                    !- 2.0_pr/REAL(dim,pr)*SUM(S_ij(:,1:dim), DIM=2) &
                                    - 2.0_pr/3.0_pr*SUM(S_ij(:,1:dim), DIM=2) &
                                    - C0_RANS*MAX(0.0_pr,SIGN(1.0_pr,u_prev(:,n_var_K)*v_prev(:,dimp3)))*v_prev(:,dimp3) &
                                    + (props_prev(:) + C3_RANS*u_prev(:,n_var_K)/v_prev(:,dimp3) )/u_prev(:,n_den) * SUM(d2u_diag(:,:),DIM=2)  &    
                                    + C3_RANS/u_prev(:,n_den)/v_prev(:,dimp3)*SUM(du(dimp2,:,1:dim)*du_diag(:,1:dim),DIM=2)
   
    ! re-using mu_t(:,2)
    WHERE( v_prev(:,dimp3) >= C7_RANS*Smod(:) )
       mu_t(:,2)= 0.0_pr                         !d (omega mu_t/k) /d (rho omega) 
    ELSEWHERE   
       mu_t(:,2)= 1.0_pr/(C7_RANS*Smod(:))       !d (omega mu_t/k) /d (rho omega)                                                                       
    END WHERE

    ! turbulence specific dissipation
    shift=(n_var_W-1)*ng
    int_Drhs_diag(shift+1:shift+ng) = int_Drhs_diag(shift+1:shift+ng) &
                                    - SUM(v_prev(:,1:dim)*du_diag(:,1:dim),DIM=2) &
                                    + 2.0_pr*C1_RANS*MAX(0.0_pr,SIGN(1.0_pr,v_prev(:,dimp3)/v_prev(:,dimp2))) &
                                    * (mu_t(:,2)*(SUM( S0_ij(:,1:dim)*S_ij(:,1:dim), DIM=2) + 2.0_pr*SUM( S0_ij(:,dim+1:nS)*S_ij(:,dim+1:nS), DIM=2 )) &
                                    !- 1.0_pr/REAL(dim,pr)*SUM(S_ij(:,1:dim), DIM=2) )  &
                                    - 1.0_pr/3.0_pr*SUM(S_ij(:,1:dim), DIM=2) )  &
                                    - 2.0_pr*C2_RANS*MAX(0.0_pr,SIGN(1.0_pr,v_prev(:,dimp3)))*v_prev(:,dimp3) + cross(:) &
                                    + (props_prev(:) + C4_RANS*u_prev(:,n_var_K)/v_prev(:,dimp3) )/u_prev(:,n_den) * SUM(d2u_diag(:,:),DIM=2)  &    
                                    - C4_RANS*u_prev(:,n_var_K)/u_prev(:,n_den)/v_prev(:,dimp3)**2*SUM(du(dimp3,:,1:dim)*du_diag(:,1:dim),DIM=2)

  END SUBROUTINE KOME_compressible_Drhs_diag

  SUBROUTINE KOME_compressible_stats ( u_in, total_Ksgs_local, total_Kdiss_local )
    USE util_vars        ! dA
    USE parallel
    USE io_3D_vars
   !USE fft_module
   !USE spectra_module
    IMPLICIT NONE
    REAL (pr), DIMENSION (nwlt,1:n_var), INTENT (IN) :: u_in
    REAL (pr), INTENT(INOUT) :: total_Ksgs_local, total_Kdiss_local
    INTEGER :: n_var_mask   ! variable for penalization mask 
    
    IF (imask_obstacle) THEN
       n_var_mask      = get_index('Mask    ')  
       total_Ksgs_local = SUM(dA(:)*u_in(:,n_var_K)*(1.0_pr-u_in(:,n_var_mask)))/(sumdA_global-5.0_pr)   !should be sumdA_global-height*length 
       CALL parallel_global_sum( REAL=total_Ksgs_local )
       total_Kdiss_local = SUM(dA(:)*u_in(:,n_var_W)*(1.0_pr-u_in(:,n_var_mask)))/(sumdA_global-5.0_pr)   !should be sumdA_global-height*length
       CALL parallel_global_sum( REAL=total_Kdiss_local )
    END IF

  END SUBROUTINE KOME_compressible_stats

  SUBROUTINE KOME_compressible_post_process
    USE variable_mapping
    IMPLICIT NONE

    u(:,n_var_K) = MAX(floor,u(:,n_var_K))
    IF (external_flow) THEN
      u(:,n_var_W) = MAX(u(:,n_den)*Kdiss_init,u(:,n_var_W))
    ELSE
      u(:,n_var_W) = MAX(u(:,n_den)*Kdiss_min,u(:,n_var_W))
    END IF

    IF (KOME_debug) call print_extrema(u(:,n_var_W)/u(:,n_den),nwlt,'W in postpro')
  END SUBROUTINE KOME_compressible_post_process

END MODULE URANS_compressible_KOME
