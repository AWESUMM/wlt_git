MODULE fft_util

!
! this module  has routines related to the fft that are not library specific
!
    USE PRECISION
    USE fft_module

!*******************FOR DEBUGGING*******************
!   #define WRITE_DEBUG if (debug) write (*,*) 'DEBUG:', 
! logical debug defined in params.f90 
!******************END FOR DEBUGGING****************
LOGICAL :: debug = .FALSE.


CONTAINS

!
!----------------------------------------------------
! interface routines for rtoft and ftort that accept a whole field as an argument
! (maybe the underlying routines can be rewritten to do the whole field)
  SUBROUTINE rtoft_field(uu)
    IMPLICIT NONE
    REAL (pr) uu(1:3,nxyz(1)+2,nxyz(2)+1,nxyz(3)+1)

    CALL rtoft( uu(1,:,:,:) )
    CALL rtoft( uu(2,:,:,:) )
    CALL rtoft( uu(3,:,:,:) ) 

  END SUBROUTINE rtoft_field


  SUBROUTINE ftort_field(uu)
    IMPLICIT NONE
    REAL (pr) uu(1:3,nxyz(1)+2,nxyz(2)+1,nxyz(3)+1)

    CALL ftort( uu(1,:,:,:) )
    CALL ftort( uu(2,:,:,:) )
    CALL ftort( uu(3,:,:,:) ) 

  END SUBROUTINE  ftort_field

!-------------------



! Apply band limited divergence free projection
! 
! u,v,w are the fourier space representation of the field components
SUBROUTINE div_free_proj(u,v,w,u_out,v_out,w_out)
    IMPLICIT NONE

    REAL*8 u(nxyz(1)+2,nxyz(2)+1,nxyz(3)+1),v(nxyz(1)+2,nxyz(2)+1,nxyz(3)+1),w(nxyz(1)+2,nxyz(2)+1,nxyz(3)+1)
    REAL*8 u_out(nxyz(1)+2,nxyz(2)+1,nxyz(3)+1),v_out(nxyz(1)+2,nxyz(2)+1,nxyz(3)+1),w_out(nxyz(1)+2,nxyz(2)+1,nxyz(3)+1)
    REAL*8 ksq
   INTEGER i,j,k

!$OMP PARALLEL DO SHARED(u,v,w,fac1,fac2,fac3) PRIVATE(i,j,k,ksq)
    ! do u component
    DO k = 1,nxyz(3)
       DO j = 1,nxyz(2)
          DO i = 1,nxyz(1)+2
             ksq = fsq1(i) + fsq2(j) + fsq3(k)
             IF (ksq == 0.0_pr) THEN  !to prevent div by zero
               u_out(i,j,k) = u(i,j,k)
               v_out(i,j,k) = v(i,j,k)
               w_out(i,j,k) = w(i,j,k)
             ELSE
                u_out(i,j,k) = u(i,j,k) - fac1(i) * fac1(i) * u(i,j,k) /ksq &
                              - fac1(i) * fac2(j) * v(i,j,k) /ksq &
                              - fac1(i) * fac3(k) * w(i,j,k) /ksq
                v_out(i,j,k) = v(i,j,k) - fac2(j) * fac1(i) * u(i,j,k) /ksq &
                              - fac2(j) * fac2(j) * v(i,j,k) /ksq &
                              - fac2(j) * fac3(k) * w(i,j,k) /ksq
                w_out(i,j,k) = w(i,j,k) - fac3(k) * fac1(i) * u(i,j,k) /ksq &
                              - fac3(k) * fac2(j) * v(i,j,k) /ksq &
                              - fac3(k) * fac3(k) * w(i,j,k) /ksq   
             ENDIF
          ENDDO
       ENDDO
    ENDDO
!$OMP END PARALLEL DO

END SUBROUTINE div_free_proj

! Apply band limited divergence free projection in place
! 
! u,v,w are the fourier space representation of the field components
SUBROUTINE div_free_proj_ip(u,v,w)
    IMPLICIT NONE

    REAL*8 u(nxyz(1)+2,nxyz(2)+1,nxyz(3)+1),v(nxyz(1)+2,nxyz(2)+1,nxyz(3)+1),w(nxyz(1)+2,nxyz(2)+1,nxyz(3)+1)
    REAL*8 utmp,vtmp,wtmp
    REAL*8 ksq
   INTEGER i,j,k

!$OMP PARALLEL DO SHARED(u,v,w,fac1,fac2,fac3) PRIVATE(i,j,k,ksq,utmp,vtmp,wtmp)
    ! do u component
    DO k = 1,nxyz(3)
       DO j = 1,nxyz(2)
          DO i = 1,nxyz(1)+2
             ksq = fsq1(i) + fsq2(j) + fsq3(k)
             IF (ksq .NE. 0.0_pr) THEN  !to prevent div by zero
                utmp =  u(i,j,k)
                vtmp =  v(i,j,k)
                wtmp =  w(i,j,k)

                u(i,j,k) = utmp - fac1(i) * fac1(i) * utmp /ksq &
                              - fac1(i) * fac2(j) * vtmp /ksq &
                              - fac1(i) * fac3(k) * wtmp /ksq
                v(i,j,k) = vtmp - fac2(j) * fac1(i) * utmp /ksq &
                              - fac2(j) * fac2(j) * vtmp /ksq &
                              - fac2(j) * fac3(k) * wtmp /ksq
                w(i,j,k) = wtmp - fac3(k) * fac1(i) * utmp /ksq &
                              - fac3(k) * fac2(j) * vtmp /ksq &
                              - fac3(k) * fac3(k) * wtmp /ksq   
             ENDIF
          ENDDO
       ENDDO
    ENDDO
!$OMP END PARALLEL DO

END SUBROUTINE div_free_proj_ip

! Apply band limited divergence free projection in place to a field
! 
! u is the fourier space representation of the field components
SUBROUTINE div_free_proj_fld_ip(u)
    IMPLICIT NONE

    REAL*8 u(1:3,nxyz(1)+2,nxyz(2)+1,nxyz(3)+1)
    REAL*8 utmp,vtmp,wtmp
    REAL*8 ksq
   INTEGER i,j,k
print*,'nxyz in div_free_proj_fld_ip ', nxyz

!$OMP PARALLEL DO SHARED(u,fac1,fac2,fac3) PRIVATE(i,j,k,ksq,utmp,vtmp,wtmp)
    ! do u component
    DO k = 1,nxyz(3)
       DO j = 1,nxyz(2)
          DO i = 1,nxyz(1)+2
             ksq = fsq1(i) + fsq2(j) + fsq3(k)
             IF (ksq .NE. 0.0_pr) THEN  !to prevent div by zero
                utmp =  u(1,i,j,k)
                vtmp =  u(2,i,j,k)
                wtmp =  u(3,i,j,k)

                u(1,i,j,k) = utmp - fac1(i) * fac1(i) * utmp /ksq &
                              - fac1(i) * fac2(j) * vtmp /ksq &
                              - fac1(i) * fac3(k) * wtmp /ksq
                u(2,i,j,k) = vtmp - fac2(j) * fac1(i) * utmp /ksq &
                              - fac2(j) * fac2(j) * vtmp /ksq &
                              - fac2(j) * fac3(k) * wtmp /ksq
                u(3,i,j,k) = wtmp - fac3(k) * fac1(i) * utmp /ksq &
                              - fac3(k) * fac2(j) * vtmp /ksq &
                              - fac3(k) * fac3(k) * wtmp /ksq   
             ENDIF
          ENDDO
       ENDDO
    ENDDO
!$OMP END PARALLEL DO

END SUBROUTINE div_free_proj_fld_ip


!
! Find div(uu) in fourier space
!
! args
!
! uu is a field of dimension  uu(1:3,n1pp,n2p,n3p) in fourier space 
!
! u_out is the result of dimension  u_out(n1pp,n2p,n3p) in fourier space
!
!
!
SUBROUTINE div_field( uu ,u_out  )
    IMPLICIT NONE
    REAL (pr) uu(1:3,nxyz(1)+2,nxyz(2),nxyz(3)),u_out(nxyz(1)+2,nxyz(2),nxyz(3))
   
    INTEGER , TARGET :: i,j,k !counters
   

    DO k = 1, nxyz(3)
       DO j = 1, nxyz(2)
          DO i = 1, nxyz(1)+2, 2
             
             !real part
             u_out(i,j,k) = - fac1(i) * uu(1,i+1,j,k) &
                  - fac2(j) * uu(2,i+1,j,k) & 
                 - fac3(k) * uu(3,i+1,j,k)
             !imaginary part
             u_out(i+1,j,k) = fac1(i) * uu(1,i,j,k) &
                  + fac2(j) * uu(2,i,j,k) & 
                  + fac3(k) * uu(3,i,j,k)
                 
          ENDDO
       ENDDO
    ENDDO

  END SUBROUTINE  div_field


!
! does curl u_i
!! same as  curl() but this version does calculations in place
!args in Fourier Space
! u_in
! v_in
! w_in
! u_result
! v_result
! w_result
subroutine curl_ip(u,v,w)
implicit none
REAL*8 u(nxyz(1)+2,nxyz(2)+1,nxyz(3)+1)
REAL*8 v(nxyz(1)+2,nxyz(2)+1,nxyz(3)+1)
REAL*8 w(nxyz(1)+2,nxyz(2)+1,nxyz(3)+1)

real*8 ur,ui,vr,vi,wr,wi
integer i,j,k


!
!        -------------------------
!        
!        calculate curl u
!
!        -------------------------
! 
! 
!
         do k = 1, nxyz(3)
            do j = 1, nxyz(2)
               do i = 1, nxyz(1)+2, 2
                  ur= u(i,j,k); ui= u(i+1,j,k) 
                  vr= v(i,j,k); vi= v(i+1,j,k) 
                  wr= w(i,j,k); wi= w(i+1,j,k) 
                  u(i,j,k)   =  fac3(k) * vi - fac2(j) * wi
                  u(i+1,j,k) = -fac3(k) * vr + fac2(j) * wr
                  v(i,j,k)   =  fac1(i) * wi - fac3(k) * ui
                  v(i+1,j,k) = -fac1(i) * wr + fac3(k) * ur
                  w(i,j,k)   =  fac2(j) * ui - fac1(i) * vi
                  w(i+1,j,k) = -fac2(j) * ur + fac1(i) * vr
               enddo
            enddo
         enddo

end subroutine curl_ip


!
! Find velocity from vorticity
! all fields must be in fourier space  and the result
! is returned in fourier space
!same as  vorticity_2_veloc() but this version does calculations in place

! Wu - x component of vorticity in  fourier space - input
! Wv - y component of vorticity in  fourier space - input
! Ww - z component of vorticity in  fourier space - input
! 
! u - x component of  velocity in  fourier space - result
! v - y component of  velocity in  fourier space - result
! w - z component of  velocity in  fourier space - result
! tmp123 are tmp variables
subroutine vorticity_2_veloc_ip(u,v,w)
implicit none
REAL*8 u(nxyz(1)+2,nxyz(2)+1,nxyz(3)+1)
REAL*8 v(nxyz(1)+2,nxyz(2)+1,nxyz(3)+1)
REAL*8 w(nxyz(1)+2,nxyz(2)+1,nxyz(3)+1)
integer i,j,k

real*8 tmp, ksq


!
!        -------------------------
!        
!        calculate velocity ( u v w ) from vorticity
!         w_i = 
!
!        -------------------------
! 
! 
!
! first find curl( W )

	call curl_ip(u,v,w)

! now Wu Wv Ww is the curl of W in Fourier space

         do k = 1, nxyz(3)
            do j = 1, nxyz(2)
		tmp = (fsq3(k) + fsq2(j) )
               do i = 1, nxyz(1)+2
		  ksq = ( fsq1(i) + tmp )
                  ! do special case where ksq = 0 (or near zero)
                  if( ( i .eq. 1 .or. i .eq. 2) .and. j .eq.1 .and. k .eq. 1 ) then
                      u(i,j,k) = 0.D0
                      v(i,j,k) = 0.D0
                      w(i,j,k) = 0.D0
                  else
                     u(i,j,k) = u(i,j,k) / ksq
                     v(i,j,k) = v(i,j,k) / ksq
                     w(i,j,k) = w(i,j,k) / ksq
                  end if
               enddo
            enddo
         enddo

end subroutine vorticity_2_veloc_ip


END MODULE
