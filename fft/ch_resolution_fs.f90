MODULE ch_resolution
  USE debug_vars

CONTAINS

!
! pad a field in fourier space to interpolate to a larger field
!
! u_old       input field of dimension u_old(n1_old+2,n2_old+1,n3_old+1)
! u_new       output field of dimension u_new(n1_new+2,n2_new+1,n3_new+1)
! n1_old,n2_old,n3_old      input dimensions (of actual real field)
! n1_new,n2_new,n3_new      output dimensions (of actual real field)
!
! 
subroutine ch_resolution_fs( u_old, u_new, n1_old,n2_old,n3_old, n1_new,n2_new,n3_new)
  use precision
  implicit none 
  integer n1_old, n2_old, n3_old, n1_new, n2_new, n3_new
  real(pr) u_new(n1_new+2,n2_new+1,n3_new+1), u_old(n1_old+2,n2_old+1,n3_old+1)

  u_new = 0.0_pr 

  if(  n1_new > n1_old ) then
     ! put y and z dir positive wave numbers in new array
          u_new(1:n1_old+1 , 1:(n2_old/2)+1 , 1:(n3_old/2)+1) = &
          u_old(1:n1_old+1 , 1:(n2_old/2)+1 , 1:(n3_old/2)+1) 

     ! put y dir positive wave numbers and z dir negative wave numbers, padded out,in output array
          u_new(1:n1_old+1 , 1:(n2_old/2)+1 , n3_new - (n3_old/2)  + 2 :n3_new ) = &
          u_old(1:n1_old+1 , 1:(n2_old/2)+1 , (n3_old/2) + 2           :n3_old )

     ! put z dir positive wave numbers and y dir negative wave numbers, padded out,in output array
          u_new(1:n1_old+1,  n2_new - (n2_old/2) + 2  :n2_new ,1:(n3_old/2)+1) = &
          u_old(1:n1_old+1,  (n2_old/2) + 2           :n2_old ,1:(n3_old/2)+1)

     ! put y dir negative wave numbers, padded out, and z dir negative wave numbers, padded new,
     ! in output array
          u_new(1:n1_old+1 , n2_new - (n2_old/2) + 2  :n2_new , n3_new - (n3_old/2)  + 2 :n3_new ) = &
          u_old(1:n1_old+1 , (n2_old/2) + 2           :n2_old , (n3_old/2) + 2           :n3_old ) 
          

  else !n1_old > n1_new
     ! put y and z dir positive wave numbers in new array
          u_new(1:n1_new,1:(n2_new/2)+1,1:(n3_new/2)+1) = &
          u_old(1:n1_new,1:(n2_new/2)+1,1:(n3_new/2)+1)

     ! put y dir positive wave numbers and z dir negative wave numbers,in output array
          u_new(1:n1_new , 1:(n2_new/2)+1 , n3_new - (n3_new/2) + 2 :n3_new ) = &
          u_old(1:n1_new , 1:(n2_new/2)+1 , n3_old - (n3_new/2) + 2 :n3_old )

     ! put z dir positive wave numbers and y dir negative wave numbers, padded out,in output array
          u_new(1:n1_new,  n2_new - (n2_new/2) + 2  :n2_new , 1:(n3_new/2)+1) = &
          u_old(1:n1_new,  n2_old - (n2_new/2) + 2  :n2_old , 1:(n3_new/2)+1)

     ! put y dir negative wave numbers, padded out, and z dir negative wave numbers, padded new,
     ! in output array
          u_new(1:n1_new , n2_new - (n2_new/2) + 2  :n2_new , n3_new - (n3_new/2)  + 2 :n3_new ) = &
          u_old(1:n1_new , n2_old - (n2_new/2) + 2  :n2_old , n3_old - (n3_new/2) + 2 :n3_old )


  endif
 
end subroutine ch_resolution_fs

!
! check for legal fft resolutioin
!
subroutine check_fft_res(n)
  use precision
  implicit none 
  integer n, nchk

  !check  grid size is legal.
  ! For temperton fft's: (From the library code)
  !THE LENGTH OF THE
  !C              TRANSFORMS MUST BE AN EVEN NUMBER GREATER THAN 4 THAT HAS
  !C              NO OTHER FACTORS EXCEPT POSSIBLY POWERS OF 2, 3, AND 5.
  nchk = n
  DO WHILE ( MOD( nchk, 2) == 0 )
     IF( nchk == 2 ) exit
     nchk = nchk/2
  END DO
 
  IF( .not. (nchk == 2 .OR. nchk == 3 .OR.  nchk == 5 ) ) THEN
     PRINT *, 'Bad resolution for FFT''s : ', n
     PRINT *, 'Exiting ...'
     stop
  END IF

END subroutine check_fft_res
END MODULE ch_resolution
