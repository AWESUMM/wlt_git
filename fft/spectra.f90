MODULE spectra_module
  USE debug_vars

CONTAINS

  !
  ! engr is a array of length n1+2 and diss is an array of length n1+2
  ! u,v,w are the fourier space representation of the field components
  !
  SUBROUTINE spectra(u,v,w, krad_max ,enrg, diss,n1,n2,n3)
    USE  fft_module
    IMPLICIT NONE

    INTEGER :: n1,n2,n3
    REAL (pr) u(n1+2,n2+1,n3+1),v(n1+2,n2+1,n3+1),w(n1+2,n2+1,n3+1)
    REAL (pr) enrg(n1+2), diss(n1+2)

    REAL (pr) mult, temp
    INTEGER krad, i, j, k, krad_max
    !
    !     general stuff
    !
    REAL (pr) pi, pi2
    REAL (pr) temp2


    !
    !     define mathematical constants
    !
    pi=4.0D0*ATAN(1.0D0)
    pi2=2.0D0*pi


    enrg = 0.D0
    diss = 0.D0

    DO i = 1, n1+2
       enrg(i) = 0.0D0
       diss(i) = 0.0D0
    ENDDO


    DO k = 1,  n3
       DO j = 1, n2
          temp = fsq3(k) + fsq2(j)
          DO i = 1, n1+2
             krad = INT(SQRT(temp+fsq1(i)))
             !write (*,*) 'in inner loop, krad =', krad
             IF (krad .GT. 0) THEN
                !     
                !     mult accounts for:  the "image" wavenumbers [u^*(-k) = u(k)]
                !                            when "i" is other than 1 or 2, and 
                !                         the 1/2 coefficient 
                !
                IF (i .LE. 2) THEN
                   mult = 0.5D0
                   ! write (*,*) 'in Spectra() i =' ,i
                ELSE
                   mult = 1.0D0
                ENDIF

                !
                temp2 = u(i,j,k)**2  + v(i,j,k)**2 + w(i,j,k)**2


                enrg(krad) = enrg(krad) + mult*temp2

                diss(krad) = diss(krad) + mult*(temp+fsq1(i)) &
                     *temp2
                ! was 
                !    enrg(krad) = enrg(krad) + mult*(u(i,j,k)**2  &
                !        + v(i,j,k)**2 + w(i,j,k)**2)

                !    diss(krad) = diss(krad) + mult*(temp+fsq1(i)) &
                !        *(u(i,j,k)**2 + v(i,j,k)**2 + w(i,j,k)**2)
                !

             ENDIF

          ENDDO
       ENDDO
    ENDDO
    ! calculate krad_max
    krad_max =  INT(SQRT( fsq3(n3) + fsq2(n2) +fsq1(n1+2)))
    !PRINT *,'In SPectra tke = ', SUM(enrg)

  END SUBROUTINE spectra !spectra()


  !
  ! u,v,w are the fourier space representation of the field components
  SUBROUTINE print_spectra(u,v,w,afilename,n1,n2,n3)
    USE fft_module
    IMPLICIT NONE

    INTEGER :: n1,n2,n3
    REAL (pr) u(n1+2,n2+1,n3+1),v(n1+2,n2+1,n3+1),w(n1+2,n2+1,n3+1)
    CHARACTER (LEN=*) afilename

    REAL (pr)  enrg(n1+2), diss(n1+2)
    INTEGER spectrafile, max_waven,j

    CALL spectra(u,v,w,max_waven, enrg,diss,n1,n2,n3)

    !
    ! write out the spectra 
    !

    !call freeunit(spectrafile)
    spectrafile = 10
    OPEN(FILE=afilename,UNIT=spectrafile)
    WRITE (spectrafile,*) '% wave_number engr1 diss1 '
    DO j = 1,max_waven
       WRITE (spectrafile,*) j,' ',enrg(j),' ',diss(j)
    ENDDO
    CLOSE(spectrafile)

  END SUBROUTINE print_spectra

  !
  ! u,v,w are the fourier space representation of the field components
  ! print spectra and also return total kinetic energy and total dissipation
  !
  SUBROUTINE print_spectra2(u,v,w,afilename,n1,n2,n3,the_tke,the_tdiss)
    USE fft_module
    USE wlt_vars
    IMPLICIT NONE

    INTEGER           , INTENT(IN)    :: n1,n2,n3
    REAL (pr)         , INTENT(IN)    :: u(n1+2,n2+1,n3+1),v(n1+2,n2+1,n3+1),w(n1+2,n2+1,n3+1)
    CHARACTER (LEN=*) , INTENT(IN)    :: afilename
    REAL (pr)         , INTENT(INOUT) :: the_tke,the_tdiss

    REAL (pr)  ::  enrg(n1+2), diss(n1+2)
    INTEGER :: spectrafile, max_waven,j

    CALL spectra(u,v,w,max_waven, enrg,diss,n1,n2,n3)

    the_tke   = SUM(enrg)
    the_tdiss = SUM(diss) 
    !
    ! write out the spectra 
    !

    !call freeunit(spectrafile)
    spectrafile = 10
    OPEN(FILE=afilename,UNIT=spectrafile)
    WRITE (spectrafile,*) '% wave_number engr1 diss1 '
    DO j = 1,max_waven
       WRITE (spectrafile,*) j,' ',enrg(j),' ',diss(j)
    ENDDO

    CLOSE(spectrafile)

  END SUBROUTINE print_spectra2


END MODULE spectra_module
