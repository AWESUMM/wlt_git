module fft_module
   USE debug_vars
use fft_interface_module
use precision
 logical , parameter :: debugfft = .true.
 
!	---------
!	Grid size is set in routine init_fft() 
!	-------
    integer ,PRIVATE :: n1,n2,n3
        ! other grid constants (mostly for ffts)
    integer ,PRIVATE :: n1p, n2p, n3p, n1pp, n2d, n3d,  npts
    integer ,PRIVATE :: n1h, n2h, n3h, n1hp, n2hp, n3hp
    integer ,PRIVATE :: jumpx,jumpy,jumpz,  incx,incy,incz
    integer ,PRIVATE :: n2dp2, n3dp2, loty, lotz


!
!     wave number arrays
!
      real (8) , dimension(:), Allocatable ::  fac1, fac2, fac3
      real (8) , dimension(:), Allocatable :: fsq1, fsq2, fsq3
     
 
  logical zero_oddball! module flag to zero oddball location


contains

!
!-----------------------------
! Initialize fft routines
! --------------------------------
! 
  ! n1n,n2n,n3n  - size of grid in xyz directions
  ! Lx, Ly, Lz   - physical size of domain in xyz directions
  !
  subroutine init_fft(n1n,n2n,n3n, Lx, Ly, Lz, do_zero_oddball )
    integer n1n,n2n,n3n
    real (pr) Lx, Ly, Lz
    logical do_zero_oddball

    zero_oddball = do_zero_oddball ! set module flag to zero oddball location

    ! setup field size numbers
    ! Determine other grid constants (mostly for ffts)
    n1 = n1n
    n2 = n2n
    n3 = n3n
    n1p=n1+1
    n2p=n2+1
    n3p=n3+1
    n1pp=n1+2
    n2d=n2*2
    n3d=n3*2
    !nwds=n1pp*n2p*n3p !now set in params.f90
    npts=n1*n2*n3
    n1h=n1/2
    n2h=n2/2
    n3h=n3/2 
    n1hp=n1h+1
    n2hp=n2h+1
    n3hp=n3h+1 
    n2dp2=n2d+2
    n3dp2=n3d+2 
    loty=n2
    lotz=n3 
    incx=1
    incy=1
    incz=1
    jumpx=n1pp
    jumpy=n2p
    jumpz=n3p 

    ! Init the routines used for the particular library we are using
    call init_interface_fft(n1n,n2n,n3n )

!
!     wave number arrays
!
    if( allocated( fac1) ) deallocate(fac1)
    allocate( fac1(n1pp) )
    if( allocated(fac2 ) ) deallocate(fac2)
    allocate( fac2(n2)   )
    if( allocated(fac3 ) ) deallocate(fac3)
    allocate( fac3(n3)   )

    if( allocated(fsq1 ) ) deallocate(fsq1)
    allocate( fsq1(n1pp) )
    if( allocated(fsq2 ) ) deallocate(fsq2)
    allocate( fsq2(n2)   )
    if( allocated(fsq3 ) ) deallocate(fsq3)
    allocate( fsq3(n3)   )


  !
  !     set up fourier grid, 
  !
  call setup_fgrid( Lx, Ly, Lz)
 

  end subroutine init_fft

!
!     ------------------
!     Setup fourier grid
!     ------------------
!
      subroutine setup_fgrid(hx, hy, hz)
      !use utg_module
      implicit none

      !include 'utg_f90.inc'

      real (8) :: hx, hy, hz
      real (8) :: pi2, fx, fy, fz

 

      integer i, j, k, jj, kk
!character*3 tmpch

 if (debugfft) write (*,*) 'DEBUG:',  'in setupfgrid, n1,n2,n3=',n1,n2,n3
 if (debugfft) write (*,*) 'DEBUG:',  'in setupfgrid, n1pp,n2p,n3p=',n1pp,n2p,n3p

!
!     fx, fy, fz = fundamental wave numbers
!
      pi2 = 8.0D0 *atan(1.0D0 )

      fx = pi2/hx
      fy = pi2/hy
      fz = pi2/hz
!
!     fac1, fac2, fac3 = arrays of wave numbers
!
!     -  x : ( 0 --> n1h ) x fx
!     -  y : ( -n2h --> n2h ) x fy
!     -  z : ( -n3h --> n3h ) x fz
!
!     fsq1, fsq2, fsq3 = arrays of squares of wave numbers
!
 if (debugfft) write (*,*) 'DEBUG:',  'in setupfgrid, n1,n2,n3=',n1,n2,n3
 if (debugfft) write (*,*) 'DEBUG:',  'in setupfgrid, n1pp,n2p,n3p=',n1pp,n2p,n3p


      do i=1,n1pp,2
         fac1(i)=real((i-1)/2,pr)*fx
         fsq1(i)=fac1(i)**2
         fac1(i+1)=fac1(i)
         fsq1(i+1)=fsq1(i)
      enddo

      do j=1,n2
         jj=j-1
         if ( j .gt. n2hp ) jj=jj-n2
         fac2(j)=real(jj,pr)*fy
         fsq2(j)=fac2(j)**2
      enddo
      do k=1,n3
         kk=k-1
         if ( k .gt. n3hp ) kk=kk-n3
         fac3(k)=real(kk,pr)*fz
         fsq3(k)=fac3(k)**2
      enddo


      return
      end subroutine



!
!	-------------------------
!
!	real to fourier transform
!
!	-------------------------
!
    subroutine rtoft(ersatz)
!
!      
    
   

    implicit none


    real (8) , TARGET :: ersatz(n1pp,n2p,n3p)
!    real (8) , dimension(:,:,:),POINTER :: ersatz_ptr


        !call the actual transform 
        call rtoft_interface(ersatz)


        !setting oddball points to zero
        if( zero_oddball ) then
           ersatz(2,1,1) = 0.0D0 !imaginary part of zero'th wave #
           ersatz(:,n2hp,:) = 0.0D0 ! n/2 wave number
           ersatz(:,:,n3hp) = 0.0D0 ! n/2 wave number

           !setting outlying points to zero
           ersatz(n1+1,:,:) = 0.0D0 ! n/2 wave number in the x dir (real)
           ersatz(n1+2,:,:) = 0.0D0 ! n/2 wave number in the x dir (imaginary)
        endif
!
!
!
    return
    end subroutine
!
!
!
!
!
!
!
!
!	-------------------------
!
!	fourier to real*8 transform
!
!	-------------------------
!
    subroutine ftort(ersatz)
          !

          !
          implicit none




          real (8) , TARGET :: ersatz(n1pp,n2p,n3p)
!          real (8) ,dimension(:,:,:),POINTER :: ersatz_ptr
 

          !call flushwrite('status' ,'Entering ftort()')
          if( zero_oddball ) then
             ersatz(2,1,1) = 0.0D0 !imaginary part of zero'th wave #
             !setting oddball points to zero
             ersatz(:,n2hp,:) = 0.0D0 ! n/2 wave number y dir
             ersatz(:,:,n3hp) = 0.0D0 ! n/2 wave number z dir 

             !setting outlying points to zero
             ersatz(n1+1,:,:) = 0.0D0 ! n/2 wave number x dir (real)
             ersatz(n1+2,:,:) = 0.0D0 ! n/2 wave number x dir (imaginary)
          endif
          !

          !call actual transform
          call ftort_interface(ersatz)


        ! Now we are in real space so we zero out the points outside the n1xn2xn3 grid
        ! setting outlying points to zero
        ersatz(n1p,:,:) = 0.0D0 
        ersatz(n1pp,:,:) = 0.0D0
        ersatz(:,n2p,:) = 0.0D0    
        ersatz(:,:,n3p) = 0.0D0

     end subroutine ftort



!****************************************
! fft utility routines
!****************************************

!
! this subroutine is to test ffts
!
subroutine test_fft()
  !use params
  use precision
 
  implicit none

  !local vars
  real(pr)  u(n1pp,n2p,n3p), u2(n1pp,n2p,n3p)
  real(pr) pi2
  integer i,j,k,wavenum
  character (len=3) :: wavenumstr
  real(pr) ,PARAMETER :: max_error = 1e-11
  real(pr) error

  pi2 = 8.0D0 *atan(1.0D0 )

  write(*,*) 'Entering test_fft(), n123 =', n1,n2,n3,n1pp,n2p,n3p
  write(*,*) ''
  write(*,*) ''
  write(*,*) 'Doing fft tests i dir'
  write(*,*) ''
  write(*,*) ''

  do wavenum = 1,n1/2
     write(wavenumstr,FMT='(i3.3)' ) wavenum
     !call flushwrite('status' ,&
     !     'Doing fft tests i dir, wave number = ' // wavenumstr )

     write(*,*) '**************************************************'
     write(*,*) '* wave number = ', wavenum 
  
     !fill
     do k = 1,n3
        do j = 1,n2
           do i = 1,n1
              u(i,j,k) = cos( wavenum*pi2 * real(i-1,pr)/(n1)  )
           enddo
        enddo
     enddo

     ! save original
     u2 = u

     call rtoft(u)
     call ftort(u)

     error =   maxval(abs( u(1:n1,1:n2,1:n3) -u2(1:n1,1:n2,1:n3)))
     write(*,*) ' Error after rtoft ftort = ', &
             error
     if( error < max_error ) then
        write(*,*) 'OK'
     else
        write(*,*) 'ERROR<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<'
     endif

     !fill du2/dx 
     do k = 1,n3
        do j = 1,n2
           do i = 1,n1
              u(i,j,k) = -sin( wavenum*pi2 * real(i-1,pr)/(n1)  )*real(wavenum,pr)
           enddo
        enddo
     enddo

     ! take deriviative in x-dir
     call rtoft(u2)
     call dui_dxj_Fspace_ip( u2,1 )
     call ftort(u2)
     error = maxval(abs( u(1:n1,1:n2,1:n3) -u2(1:n1,1:n2,1:n3)))
     write(*,*) ' Error max error in 1st derivative = ', &
          error
     if( error < max_error ) then
        write(*,*) 'OK'
     else
        write(*,*) 'ERROR<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<'
     endif


     write(*,*) '**************************************************'
     write(*,*) ''
  enddo

  write(*,*) ''
  write(*,*) 'End of fft tests i-dir'
  write(*,*) ''
  write(*,*) ''

  write(*,*) ''
  write(*,*) ''
  write(*,*) 'Doing fft tests j dir'
  write(*,*) ''
  write(*,*) ''

  do wavenum = 1,n2/2
     write(wavenumstr,FMT='(i3.3)' ) wavenum
     !call flushwrite('status' ,'Doing fft tests j dir, wave number = '//wavenumstr )

     write(*,*) '**************************************************'
     write(*,*) '* wave number = ', wavenum 
  
     !fill
     do k = 1,n3
        do j = 1,n2
           do i = 1,n1
              u(i,j,k) = cos( wavenum*pi2 * real(j-1,pr)/(n2)  )
           enddo
        enddo
     enddo

     ! save original
     u2 = u

     call rtoft(u)
     call ftort(u)

     error =   maxval(abs( u(1:n1,1:n2,1:n3) -u2(1:n1,1:n2,1:n3)))
     write(*,*) ' Error after rtoft ftort = ', &
                error
     if( error < max_error ) then
        write(*,*) 'OK'
     else
        write(*,*) 'ERROR<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<'
     endif

     !fill du2/dx 
     do k = 1,n3
        do j = 1,n2
           do i = 1,n1
              u(i,j,k) = -sin( wavenum*pi2 * real(j-1,pr)/(n2)  )*real(wavenum,pr)
           enddo
        enddo
     enddo

     ! take deriviative in x-dir
     call rtoft(u2)
     call dui_dxj_Fspace_ip( u2,2 )
     call ftort(u2)

     error =     maxval(abs( u(1:n1,1:n2,1:n3) -u2(1:n1,1:n2,1:n3)))
     write(*,*) ' Error max error in 1st derivative = ', &
                error
     if( error < max_error ) then
        write(*,*) 'OK'
     else
        write(*,*) 'ERROR<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<'
     endif


     write(*,*) '**************************************************'
     write(*,*) ''
  enddo

  write(*,*) ''
  write(*,*) 'End of fft tests j-dir'
  write(*,*) ''
  write(*,*) ''

  write(*,*) ''
  write(*,*) ''
  write(*,*) 'Doing fft tests k dir'
  write(*,*) ''
  write(*,*) ''

  do wavenum = 1,n3/2
    
     write(wavenumstr,FMT='(i3.3)' ) wavenum
     !call flushwrite('status' ,'Doing fft tests k dir, wave number = '//wavenumstr )

     write(*,*) '**************************************************'
     write(*,*) '* wave number = ', wavenum 
  
     !fill
     do k = 1,n3
        do j = 1,n2
           do i = 1,n1
              u(i,j,k) = cos( wavenum*pi2 * real(k-1,pr)/(n3)  )
           enddo
        enddo
     enddo

     ! save original
     u2 = u

     call rtoft(u)
     call ftort(u)


     error =     maxval(abs( u(1:n1,1:n2,1:n3) -u2(1:n1,1:n2,1:n3)))
     write(*,*) '* Error after rtoft ftort = ', &
                   error
     if( error < max_error ) then
        write(*,*) 'OK'
     else
        write(*,*) 'ERROR<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<'
     endif

     !fill du2/dx 
     do k = 1,n3
        do j = 1,n2
           do i = 1,n1
              u(i,j,k) = -sin( wavenum*pi2 * real(k-1,pr)/(n3)  )*real(wavenum,pr)
           enddo
        enddo
     enddo

     ! take deriviative in x-dir
     call rtoft(u2)
     call dui_dxj_Fspace_ip( u2,3 )
     call ftort(u2)

     error =     maxval(abs( u(1:n1,1:n2,1:n3) -u2(1:n1,1:n2,1:n3)))
     write(*,*) '* Error max error in 1st derivative = ', &
                   error
     if( error < max_error ) then
        write(*,*) 'OK'
     else
        write(*,*) 'ERROR<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<'
     endif


     write(*,*) '**************************************************'
     write(*,*) ''
  enddo

  write(*,*) ''
  write(*,*) 'End of fft tests k-dir'
  write(*,*) ''
  write(*,*) ''



end subroutine test_fft


!
! Find dui/dxj in fourier space
!
! args
!
! u is a scalar field of dimension  u(n1pp,n2p,n3p) in real space 
! jj is the direction to the do the deriviative
! u_out is the result in fourier space
!
! note u can not be the same as u_out!!
!
subroutine dui_dxj( u ,u_out ,jj )
    !use params
    use precision
    implicit none
    REAL (pr) u(n1pp,n2p,n3p),u_out(n1pp,n2p,n3p)
    integer jj

    !integer , TARGET :: i,j,k !counters
    !real (pr)  facj(n1pp)
    !real (pr)  tmpu
    !integer  , POINTER ::  facj_index

   ! save u into u_out and then tranform to fourier spcae and call the in place routine
   u_out = u
   call rtoft( u_out )
   call dui_dxj_Fspace_ip( u_out ,jj )   
   call ftort( u_out )

end subroutine  dui_dxj

subroutine dui_dxj_Fspace_ip( u ,jj )
    !use params
    use precision
    implicit none
    REAL (pr) u(n1pp,n2p,n3p)
    integer jj

    integer , TARGET :: i,j,k !counters
    real (pr)  facj(n1pp)
    real (pr)  tmpu
    integer  , POINTER ::  facj_index

! set up which wavenumber array to use depending on jj
   select case (jj)
    case (1)
       facj = fac1; facj_index => i
    case (2)
       facj(1:n2) = fac2(1:n2); facj_index => j
    case (3)
       facj(1:n3) = fac3(1:n3); facj_index => k
    case default
       write (*,*) 'Illegal value for jj:', jj, ' in dui_dxj_ip() Exiting..'
       stop
    end select
   
   
    
   do k = 1, n3
       do j = 1, n2
          do i = 1, n1pp, 2
             tmpu =  u(i,j,k); !do this because we are working inplace
             !real part real(du/dx) = i kx * i Imag( u ) = - kx * Imag(u)
             u(i,j,k) = - facj( facj_index  ) * u(i+1,j,k) 
                 
             !imaginary part Imag(du/dx) = i kx * real(u)
             u(i+1,j,k) = facj( facj_index ) * tmpu 
                 
          enddo
       enddo
    enddo

 
end subroutine  dui_dxj_Fspace_ip
!
!Find the total kenetic energy and the integral length scale 
!uin,vin,win are in fourier space
! 
subroutine  tke( uin,vin,win , the_tke, the_int_length )
  use precision
!  use params
!  use fft_util
!  use fft_module
  implicit none
  real(pr)   uin(n1pp,n2p,n3p),vin(n1pp,n2p,n3p),win(n1pp,n2p,n3p)
  real(pr)   the_tke, the_int_length
 
  !local vars
  REAL (pr) :: t11(n1pp), t12(n2p), t13(n3p)
  REAL (pr) :: e11(n1pp), e12(n2p), e13(n3p)
  real (pr) pi
  integer i,j,k

  pi=4.0D0*atan(1.0D0)


  the_tke =  0.5_pr*SUM( uin(1:2,1:n2,1:n3)**2 + vin(1:2,1:n2,1:n3)**2 &
         + win(1:2,1:n2,1:n3)**2 ) +&
          SUM( uin(3:n1pp,1:n2,1:n3)**2 + vin(3:n1pp,1:n2,1:n3)**2 &
          + win(3:n1pp,1:n2,1:n3)**2 )


       !     -----------------------------------------
       !     calculate 1D spectrum of velocity squared
       !     -----------------------------------------

       DO i = 1, n1pp
          t11(i) = SUM( uin(i,1:n2,1:n3)**2 + vin(i,1:n2,1:n3)**2 + win(i,1:n2,1:n3)**2 )
       END DO
       t11(3:n1pp) = 2.0_pr*t11(3:n1pp)
       
       DO j = 1, n2
          t12(j) = SUM( uin(1:2,j,1:n3)**2 + vin(1:2,j,1:n3)**2 + win(1:2,j,1:n3)**2 ) + &
               2.0_pr*SUM( uin(3:n1pp,j,1:n3)**2 + vin(3:n1pp,j,1:n3)**2 + win(3:n1pp,j,1:n3)**2 )
       END DO

       DO k = 1, n3
          t13(k) = SUM( uin(1:2,1:n2,k)**2 + vin(1:2,1:n2,k)**2 + win(1:2,1:n2,k)**2 ) + &
               2.0_pr*SUM( uin(3:n1pp,1:n2,k)**2 + vin(3:n1pp,1:n2,k)**2 + win(3:n1pp,1:n2,k)**2 )
       END DO

       !
       !     reduce data to meaningful 1D arrays:
       !       1) combine real*8 and imaginary parts (x-direction)
       !       2) combine k and -k wavenumbers (y- and z- directions)
       !
       
       do i = 1, n1hp
          e11(i) = t11(2*i-1) + t11(2*i)
       enddo
    
       e12(1) = t12(1)
       do j = 2, n2h
          e12(j) = t12(j) + t12(n2-j+2)
       enddo
       e12(n2hp) = t12(n2hp)
    
       e13(1) = t13(1)
       do k = 2, n3h
          e13(k) = t13(k) + t13(n3-k+2)
       enddo
       e13(n3hp) = t13(n3hp)


       the_int_length = pi/(4.0_pr*the_tke) * (e11(1) + e12(1) + e13(1)) 
 
end subroutine  tke

!
! does curl u_i
!! same as  curl() but this version does calculations in place
!args in Fourier Space
! u_in
! v_in
! w_in
! u_result
! v_result
! w_result
subroutine curl_ip_fft(u,v,w)
implicit none
REAL (8) :: u(n1+2,n2+1,n3+1)
REAL (8) v(n1+2,n2+1,n3+1)
REAL (8) w(n1+2,n2+1,n3+1)

real (8) :: ur,ui,vr,vi,wr,wi
integer i,j,k


!
!        -------------------------
!        
!        calculate curl u
!
!        -------------------------
! 
! 
!
         do k = 1, n3
            do j = 1, n2
               do i = 1, n1+2, 2
                  ur= u(i,j,k); ui= u(i+1,j,k) 
                  vr= v(i,j,k); vi= v(i+1,j,k) 
                  wr= w(i,j,k); wi= w(i+1,j,k) 
                  u(i,j,k)   =  fac3(k) * vi - fac2(j) * wi
                  u(i+1,j,k) = -fac3(k) * vr + fac2(j) * wr
                  v(i,j,k)   =  fac1(i) * wi - fac3(k) * ui
                  v(i+1,j,k) = -fac1(i) * wr + fac3(k) * ur
                  w(i,j,k)   =  fac2(j) * ui - fac1(i) * vi
                  w(i+1,j,k) = -fac2(j) * ur + fac1(i) * vr
               enddo
            enddo
         enddo

end subroutine curl_ip_fft


!
!
end module fft_module
