module fft_interface_module
!*******************FOR DEBUGGING*******************
!  #define WRITE_DEBUG if (debugfft) write (*,*) 'DEBUG:', 
!******************END FOR DEBUGGING****************
  
   USE precision
   USE debug_vars

!	---------
!	Grid size is set in routine init_fft() 
!	-------

    integer, PRIVATE :: n1,n2,n3
    ! other grid constants (mostly for ffts)
    integer, PRIVATE :: n1p, n2p, n3p, n1pp, n2d, n3d,  npts
    integer, PRIVATE :: n1h, n2h, n3h, n1hp, n2hp, n3hp
    integer, PRIVATE :: jumpx, jumpy, jumpz,  incx,  incy, incz
    integer, PRIVATE :: n2dp2, n3dp2, loty, lotz



 
!
!     arrays used for ffts with the temperton routines
!
      integer , DIMENSION (:), allocatable :: ifaxx, ifaxy, ifaxz
      real(pr)  , DIMENSION (:), allocatable :: trigsx, trigsy, trigsz
      
 
  integer start_rtoft, start_ftort


contains

!
!-----------------------------
! Initialize fft routines
! --------------------------------
! 
  subroutine init_interface_fft(n1n,n2n,n3n )
    integer n1n,n2n,n3n
 
    ! setup field size numbers
    ! Determine other grid constants (mostly for ffts)
    n1 = n1n
    n2 = n2n
    n3 = n3n
    n1p=n1+1
    n2p=n2+1
    n3p=n3+1
    n1pp=n1+2
    n2d=n2*2
    n3d=n3*2
    !nwds=n1pp*n2p*n3p !now set in params.f90
    npts=n1*n2*n3
    n1h=n1/2
    n2h=n2/2
    n3h=n3/2 
    n1hp=n1h+1
    n2hp=n2h+1
    n3hp=n3h+1 
    n2dp2=n2d+2
    n3dp2=n3d+2 
    loty=n2
    lotz=n3 
    incx=1
    incy=1
    incz=1
    jumpx=n1pp
    jumpy=n2p
    jumpz=n3p 

    ! allocate table arrays
    !
    ! It is important that we first check if the arrays ia allocated and
    ! if so we must reallocate it to the right size. This is so that the 
    ! field resizing mechanism will work right.
    !
    write(*,*) 'Allocating table array  = trigsx , length = ', 3*n1h+1
    if( allocated(trigsx ) ) deallocate(trigsx)
    allocate( trigsx(3*n1h+1) )

    write(*,*) 'Allocating table array  = trigsy , length = ', n2d
    if( allocated(trigsy ) ) deallocate(trigsy)
    allocate( trigsy(n2d) )

    write(*,*) 'Allocating table array  = trigsz , length = ', n3d
    if( allocated(trigsz ) ) deallocate(trigsz)
    allocate( trigsz(n3d) )

    if( .not. allocated( ifaxx ) ) allocate( ifaxx(13) )
    if( .not. allocated( ifaxy ) ) allocate( ifaxy(13) )
    if( .not. allocated( ifaxz ) ) allocate( ifaxz(13) )
 
  write(*,*) 'Initializing ffts'
!
!     initialize arrays for fft
!
  write(*,*) n1 ,n2, n3
  call fftfax(n1,ifaxx,trigsx)
  call cftfax(n2,ifaxy,trigsy)
  call cftfax(n3,ifaxz,trigsz)

  

  end subroutine init_interface_fft
!
!	-------------------------
!
!	real to fourier transform
!
!	-------------------------
!
  subroutine rtoft_interface(ersatz)
    !
    !       


    implicit none

    !
    integer itemp1,itemp2,jtemp1,jtemp2,ktemp1,ktemp2
    integer i,j,k , isign
    !
    real(pr) ersatz(n1pp,n2p,n3p)
    real(pr)  workx(lotz*n1p)
    real(pr)  worky(lotz*n2dp2)
    real(pr)  workz(loty*n3dp2)
    real(pr)  ax(n1pp,lotz),ay(n2dp2,lotz),az(n3dp2,loty)






    ! Set direction of the fft
    isign=-1 

    !
    !	----------------------------------------------------
    !
    !	transform in x-direction
    !       ~~~~~~~~~~~~~~~~~~~~~~~~
    !	-  send an entire slice of constant y at a time
    !	-  real to complex, but:
    !	-  comes back in real array, with real and imaginary
    !          parts alternating
    !
    !	----------------------------------------------------
    !
    do j=1,n2
       do k=1,n3
          do i=1,n1
             ax(i,k)=ersatz(i,j,k)
          enddo
          ax(n1p,k)=0.0D0
          ax(n1pp,k)=0.0D0
       enddo

       call fft991(ax,workx,trigsx,ifaxx,incx,jumpx,n1,lotz,isign)


       do k=1,n3
          do i=1,n1p
             ersatz(i,j,k)  = ax(i,k)
          enddo
          ersatz(n1pp,j,k)=0.0D0 !imaginary part of n/2 wave # is zero 
       enddo
    enddo
    !
    !	-----------------------------------------------
    !
    !	transform in y-direction
    !	~~~~~~~~~~~~~~~~~~~~~~~~
    !	-  send an entire slice of constant x at a time
    !	-  complex to complex
    !
    !	-----------------------------------------------
    !
    do i=1,n1hp
       itemp1=2*(i-1)+1
       itemp2=itemp1+1
       do k=1,n3
          do j=1,n2
             jtemp1=2*(j-1)+1
             jtemp2=jtemp1+1
             ay(jtemp1,k)=ersatz(itemp1,j,k)
             ay(jtemp2,k)=ersatz(itemp2,j,k)
          enddo
       enddo
       call cfft99(ay,worky,trigsy,ifaxy,incy,jumpy,n2,lotz,isign)


       do k=1,n3
          do j=1,n2
             jtemp1=2*(j-1)+1
             jtemp2=jtemp1+1
             ersatz(itemp1,j,k)=ay(jtemp1,k)
             ersatz(itemp2,j,k)=ay(jtemp2,k)
          enddo
       enddo
    enddo

    !
    !	-----------------------------------------------
    !
    !	transform in z-direction
    !	~~~~~~~~~~~~~~~~~~~~~~~~
    !	-  send an entire slice of constant x at a time
    !	-  complex to complex
    !
    !	-----------------------------------------------
    !
    do i=1,n1hp
       itemp1=2*(i-1)+1
       itemp2=itemp1+1
       do j=1,n2
          do k=1,n3
             ktemp1=2*(k-1)+1
             ktemp2=ktemp1+1
             az(ktemp1,j)=ersatz(itemp1,j,k)
             az(ktemp2,j)=ersatz(itemp2,j,k)		 
          enddo
       enddo

       call cfft99(az,workz,trigsz,ifaxz,incz,jumpz,n3,loty,isign)


       do j=1,n2
          do k=1,n3
             ktemp1=2*(k-1)+1
             ktemp2=ktemp1+1
             ersatz(itemp1,j,k)=az(ktemp1,j)
             ersatz(itemp2,j,k)=az(ktemp2,j)
          enddo
       enddo
    enddo
    !
    !	------------------------------
    !
    !	normalize fourier coefficients
    !
    !	------------------------------
    !
    !old  do k=1,n3p
    !old  do j=1,n2p
    do k=1,n3
       do j=1,n2
          do i=1,n1pp
             ersatz(i,j,k)=ersatz(i,j,k)/DBLE(n2*n3)
          enddo
       enddo
    enddo



    return
  end subroutine rtoft_interface
!
!
!
!
!
!
!
!
!	-------------------------
!
!	fourier to real(pr) transform
!
!	-------------------------
!
    subroutine ftort_interface(ersatz)
          !

          !
          implicit none



          !
          integer itemp1,itemp2,jtemp1,jtemp2,ktemp1,ktemp2
          integer i,j,k, isign
          !
          real(pr) ersatz(n1pp,n2p,n3p)
          real(pr) ax(n1pp,lotz), workx(lotz*n1p)
          real(pr) ay(n2dp2,lotz), worky(lotz*n2dp2)
          real(pr) az(n3dp2,loty), workz(loty*n3dp2)





          !

          ! Set direction of the fft
          isign=1 

          !write(*,*) 'in ftort , n1pp = ',n1pp,' n2p =',n2p,'n3p = ',n3p

          !
          !	-----------------------------------------------
          !
          !	transform in z-direction
          !	~~~~~~~~~~~~~~~~~~~~~~~~
          !	-  send an entire slice of constant x at a time
          !	-  complex to complex
          !
          !	-----------------------------------------------
          !
          do i=1,n1hp
             itemp1=2*(i-1)+1
             itemp2=itemp1+1
             do j=1,n2
                do k=1,n3
                   ktemp1=2*(k-1)+1
                   ktemp2=ktemp1+1
                   az(ktemp1,j)=ersatz(itemp1,j,k)
                   az(ktemp2,j)=ersatz(itemp2,j,k)
                enddo
             enddo

             call cfft99(az,workz,trigsz,ifaxz,incz,jumpz,n3,loty,isign)           


             do j=1,n2
                do k=1,n3
                   ktemp1=2*(k-1)+1
                   ktemp2=ktemp1+1
                   ersatz(itemp1,j,k)=az(ktemp1,j)
                   ersatz(itemp2,j,k)=az(ktemp2,j)
                enddo
             enddo
          enddo
          !
          !	-----------------------------------------------
          !
          !	transform in y-direction
          !	~~~~~~~~~~~~~~~~~~~~~~~~
          !	-  send an entire slice of constant x at a time
          !	-  complex to complex
          !
          !	-----------------------------------------------
          !
          do i=1,n1hp
             itemp1=2*(i-1)+1
             itemp2=itemp1+1
             do k=1,n3
                do j=1,n2
                   jtemp1=2*(j-1)+1
                   jtemp2=jtemp1+1
                   ay(jtemp1,k)=ersatz(itemp1,j,k)
                   ay(jtemp2,k)=ersatz(itemp2,j,k)
                enddo
             enddo

             call cfft99(ay,worky,trigsy,ifaxy,incy,jumpy,n2,lotz,isign)        

             do k=1,n3
                do j=1,n2
                   jtemp1=2*(j-1)+1
                   jtemp2=jtemp1+1
                   ersatz(itemp1,j,k)=ay(jtemp1,k)
                   ersatz(itemp2,j,k)=ay(jtemp2,k)
                enddo
             enddo
          enddo
          !
          !	----------------------------------------------------
          !
          !	transform in x-direction
          !       ~~~~~~~~~~~~~~~~~~~~~~~~
          !	-  send an entire slice of constant y at a time
          !	-  complex to real
          !
          !	----------------------------------------------------
          !
          do j=1,n2
             do k=1,n3
                do i=1,n1p
                   ax(i,k)=ersatz(i,j,k)
                enddo
                ax(n1pp,k)=0.0D0
             enddo

             call fft991(ax,workx,trigsx,ifaxx,incx,jumpx,n1,lotz,isign)          



             do k=1,n3
                do i=1,n1pp
                   ersatz(i,j,k)=ax(i,k)
                enddo
             enddo
          enddo
          !


          return
   end subroutine ftort_interface



end module fft_interface_module

