MODULE fft_util

!
! this module  has routines related to the fft that are not library specific
!
    USE PRECISION
    USE debug_vars
    USE wlt_vars
    USE fft_module

!*******************FOR DEBUGGING*******************
!   #define WRITE_DEBUG if (debug) write (*,*) 'DEBUG:', 
! logical debug defined in params.f90 
!******************END FOR DEBUGGING****************
LOGICAL :: debug = .FALSE.


CONTAINS

!
!----------------------------------------------------
! interface routines for rtoft and ftort that accept a whole field as an argument
! (maybe the underlying routines can be rewritten to do the whole field)
  SUBROUTINE rtoft_field(uu)
    IMPLICIT NONE
    REAL (pr) uu(1:3,nxyz(1)+2,nxyz(2)+1,nxyz(3)+1)

    CALL rtoft( uu(1,:,:,:) )
    CALL rtoft( uu(2,:,:,:) )
    CALL rtoft( uu(3,:,:,:) ) 

  END SUBROUTINE rtoft_field


  SUBROUTINE ftort_field(uu)
    IMPLICIT NONE
    REAL (pr) uu(1:3,nxyz(1)+2,nxyz(2)+1,nxyz(3)+1)

    CALL ftort( uu(1,:,:,:) )
    CALL ftort( uu(2,:,:,:) )
    CALL ftort( uu(3,:,:,:) ) 

  END SUBROUTINE  ftort_field

!-------------------



! Apply band limited divergence free projection
! 
! u,v,w are the fourier space representation of the field components
SUBROUTINE div_free_proj(u,v,w,u_out,v_out,w_out)
    IMPLICIT NONE

    REAL(8) u(nxyz(1)+2,nxyz(2)+1,nxyz(3)+1),v(nxyz(1)+2,nxyz(2)+1,nxyz(3)+1),w(nxyz(1)+2,nxyz(2)+1,nxyz(3)+1)
    REAL(8) u_out(nxyz(1)+2,nxyz(2)+1,nxyz(3)+1),v_out(nxyz(1)+2,nxyz(2)+1,nxyz(3)+1),w_out(nxyz(1)+2,nxyz(2)+1,nxyz(3)+1)
    REAL(8) ksq
   INTEGER i,j,k

!$OMP PARALLEL DO SHARED(u,v,w,fac1,fac2,fac3) PRIVATE(i,j,k,ksq)
    ! do u component
    DO k = 1,nxyz(3)
       DO j = 1,nxyz(2)
          DO i = 1,nxyz(1)+2
             ksq = fsq1(i) + fsq2(j) + fsq3(k)
             IF (ksq == 0.0_pr) THEN  !to prevent div by zero
               u_out(i,j,k) = u(i,j,k)
               v_out(i,j,k) = v(i,j,k)
               w_out(i,j,k) = w(i,j,k)
             ELSE
                u_out(i,j,k) = u(i,j,k) - fac1(i) * fac1(i) * u(i,j,k) /ksq &
                              - fac1(i) * fac2(j) * v(i,j,k) /ksq &
                              - fac1(i) * fac3(k) * w(i,j,k) /ksq
                v_out(i,j,k) = v(i,j,k) - fac2(j) * fac1(i) * u(i,j,k) /ksq &
                              - fac2(j) * fac2(j) * v(i,j,k) /ksq &
                              - fac2(j) * fac3(k) * w(i,j,k) /ksq
                w_out(i,j,k) = w(i,j,k) - fac3(k) * fac1(i) * u(i,j,k) /ksq &
                              - fac3(k) * fac2(j) * v(i,j,k) /ksq &
                              - fac3(k) * fac3(k) * w(i,j,k) /ksq   
             ENDIF
          ENDDO
       ENDDO
    ENDDO
!$OMP END PARALLEL DO

END SUBROUTINE div_free_proj

! Apply band limited divergence free projection in place
! 
! u,v,w are the fourier space representation of the field components
SUBROUTINE div_free_proj_ip(u,v,w)
    IMPLICIT NONE

    REAL(8) u(nxyz(1)+2,nxyz(2)+1,nxyz(3)+1),v(nxyz(1)+2,nxyz(2)+1,nxyz(3)+1),w(nxyz(1)+2,nxyz(2)+1,nxyz(3)+1)
    REAL(8) utmp,vtmp,wtmp
    REAL(8) ksq
   INTEGER i,j,k

!$OMP PARALLEL DO SHARED(u,v,w,fac1,fac2,fac3) PRIVATE(i,j,k,ksq,utmp,vtmp,wtmp)
    ! do u component
    DO k = 1,nxyz(3)
       DO j = 1,nxyz(2)
          DO i = 1,nxyz(1)+2
             ksq = fsq1(i) + fsq2(j) + fsq3(k)
             IF (ksq .NE. 0.0_pr) THEN  !to prevent div by zero
                utmp =  u(i,j,k)
                vtmp =  v(i,j,k)
                wtmp =  w(i,j,k)

                u(i,j,k) = utmp - fac1(i) * fac1(i) * utmp /ksq &
                              - fac1(i) * fac2(j) * vtmp /ksq &
                              - fac1(i) * fac3(k) * wtmp /ksq
                v(i,j,k) = vtmp - fac2(j) * fac1(i) * utmp /ksq &
                              - fac2(j) * fac2(j) * vtmp /ksq &
                              - fac2(j) * fac3(k) * wtmp /ksq
                w(i,j,k) = wtmp - fac3(k) * fac1(i) * utmp /ksq &
                              - fac3(k) * fac2(j) * vtmp /ksq &
                              - fac3(k) * fac3(k) * wtmp /ksq   
             ENDIF
          ENDDO
       ENDDO
    ENDDO
!$OMP END PARALLEL DO

END SUBROUTINE div_free_proj_ip

! Apply band limited divergence free projection in place to a field
! 
! u is the fourier space representation of the field components
SUBROUTINE div_free_proj_fld_ip(u)
    IMPLICIT NONE

    REAL(8) u(1:3,nxyz(1)+2,nxyz(2)+1,nxyz(3)+1)
    REAL(8) utmp,vtmp,wtmp
    REAL(8) ksq
   INTEGER i,j,k
print*,'nxyz in div_free_proj_fld_ip ', nxyz

!$OMP PARALLEL DO SHARED(u,fac1,fac2,fac3) PRIVATE(i,j,k,ksq,utmp,vtmp,wtmp)
    ! do u component
    DO k = 1,nxyz(3)
       DO j = 1,nxyz(2)
          DO i = 1,nxyz(1)+2
             ksq = fsq1(i) + fsq2(j) + fsq3(k)
             IF (ksq .NE. 0.0_pr) THEN  !to prevent div by zero
                utmp =  u(1,i,j,k)
                vtmp =  u(2,i,j,k)
                wtmp =  u(3,i,j,k)

                u(1,i,j,k) = utmp - fac1(i) * fac1(i) * utmp /ksq &
                              - fac1(i) * fac2(j) * vtmp /ksq &
                              - fac1(i) * fac3(k) * wtmp /ksq
                u(2,i,j,k) = vtmp - fac2(j) * fac1(i) * utmp /ksq &
                              - fac2(j) * fac2(j) * vtmp /ksq &
                              - fac2(j) * fac3(k) * wtmp /ksq
                u(3,i,j,k) = wtmp - fac3(k) * fac1(i) * utmp /ksq &
                              - fac3(k) * fac2(j) * vtmp /ksq &
                              - fac3(k) * fac3(k) * wtmp /ksq   
             ENDIF
          ENDDO
       ENDDO
    ENDDO
!$OMP END PARALLEL DO

END SUBROUTINE div_free_proj_fld_ip


!
! Find div(uu) in fourier space
!
! args
!
! uu is a field of dimension  uu(1:3,n1pp,n2p,n3p) in fourier space 
!
! u_out is the result of dimension  u_out(n1pp,n2p,n3p) in fourier space
!
!
!
SUBROUTINE div_field( uu ,u_out  )
    IMPLICIT NONE
    REAL (pr) uu(1:3,nxyz(1)+2,nxyz(2),nxyz(3)),u_out(nxyz(1)+2,nxyz(2),nxyz(3))
   
    INTEGER , TARGET :: i,j,k !counters
   

    DO k = 1, nxyz(3)
       DO j = 1, nxyz(2)
          DO i = 1, nxyz(1)+2, 2
             
             !real part
             u_out(i,j,k) = - fac1(i) * uu(1,i+1,j,k) &
                  - fac2(j) * uu(2,i+1,j,k) & 
                 - fac3(k) * uu(3,i+1,j,k)
             !imaginary part
             u_out(i+1,j,k) = fac1(i) * uu(1,i,j,k) &
                  + fac2(j) * uu(2,i,j,k) & 
                  + fac3(k) * uu(3,i,j,k)
                 
          ENDDO
       ENDDO
    ENDDO

  END SUBROUTINE  div_field




END MODULE
