! Fourier spectrum in wavelet space and some statistics
! by Alexei Vezolainen
! July 2006
MODULE ft_aux
  USE precision
  IMPLICIT NONE
  
  PUBLIC :: ft_read_var, ft_compute, ft_clean_var, &  ! general user functions for standalone version
       init_fft, print_spectra_a, ft_clean_var_2, &    ! special user functions for turbulent statistics module
       compute_1d_spectrum
  
  PRIVATE :: clean_after_read_solution, set_table, set_table_aux, &  ! private functions
       check_dimension, test_alloc, &
       ixyz2indx_init,  &
       get_phi, get_psi, &
       print_solution, get_f3, set_k, total_k
  
  INTEGER, PARAMETER, PRIVATE :: MAXDIM = 4                          ! maximum dimension
  INTEGER, PARAMETER, PRIVATE :: VERB_1 = 0                          ! run xmgr phi/psi
  INTEGER, PARAMETER, PRIVATE :: VERB_2 = 0                          ! run xmgr FFT of phi/psi
  
  REAL (pr), DIMENSION (:),   POINTER,     PRIVATE :: scl            ! scl(1:n_var)
  COMPLEX,   DIMENSION (:,:), ALLOCATABLE, PRIVATE :: PSI, PHI       ! tabulated wavelet functions
  INTEGER*8,                               PRIVATE :: i_p(0:MAXDIM)  ! coordinate transformation array
  REAL(pr),                   ALLOCATABLE, PRIVATE :: fsq(:)         ! k^2 linear array

!#define AdaptiveSpectrum_DEBUG  ! DEBUG: local output inside   PROGRAM wavelet_interpolation

CONTAINS





  !------------------------------------------------------------------------------------------------
  !------------------------------------------------------------------------------------------------
  ! PUBLIC
  ! to be called from TURB_STATS
  ! to compute 1D spectrum         
  ! e1_sum <--> e11(1) + e12(1) + e13(1) of non-adaptive version
  !
  SUBROUTINE compute_1d_spectrum( e1_sum, e1d_total, e1d_num )

    ! ---- the following parameters are be used to control accuracy ----
    ! PERCENT_OF_POINTS_TO_USE_1D, MIN_POINTS_1D, MAX_POINTS_1D, DO_1D
    USE spectrum_parameters
    USE wlt_vars
    USE parallel 

    REAL(pr), INTENT(IN)  :: e1d_total     ! precomputed analog of e1_sum from PRINT_SPECTRA_A
    INTEGER,  INTENT(IN)  :: e1d_num       ! ...
    REAL(pr), INTENT(OUT) :: e1_sum
    
    INTEGER :: ie_arr(1:dim), knum(1:dim), i, np, sknum, mark_limit, rndi_index, &
         k_index, i1,i2,i3, cur_dim, xyz(1:dim), rnp(1:dim), N(1:dim)
    INTEGER,  ALLOCATABLE :: kav(:,:), rndi(:), rndi_sorted(:)
    COMPLEX,  ALLOCATABLE :: res(:,:)
    REAL(pr), ALLOCATABLE :: rnd(:)
    REAL(pr) :: e1(1:3), mult              !   <--> e11(1), etc
    REAL(pr) :: rndu(1:dim-1)
    



    IF ( .NOT. DO_1D ) THEN
       e1_sum = 0.0_pr
       RETURN
    END IF

    N(1) = nxyz(1)/2+1                     ! (half space) for compilance with the                  
    N(2:dim) = nxyz(2:dim)                 ! previous non-adaptive version
    knum(1:dim) = PRODUCT( N(1:dim) )
    DO i=1,dim
       ie_arr(i) = i                       ! FT for all variables
       knum(i) = knum(i) / N(i)            ! maximum number of points on face
    END DO
    sknum = SUM( knum(1:dim) )

    ! compute number of points to use
    np = INT( sknum*PERCENT_OF_POINTS_TO_USE_1D/100.0_pr )
    np = MIN( MAX( MIN_POINTS_1D, np ), MAX_POINTS_1D )
    np = MIN( np, sknum )
    ALLOCATE( kav(1:dim,1:np), res(1:dim,1:np) )
    
    ! randomly distribute points on faces
    IF ( DO_UNIQUE_POINTS ) THEN
       !CALL RANDOM_SEED
       ALLOCATE ( rnd(sknum), rndi(sknum), rndi_sorted(sknum) )
       CALL RANDOM_NUMBER( rnd )                       ! rnd \in [0..1]
       rndi = INT( rnd*sknum )                         ! rndi \in [0..knum]
#ifdef MULTIPROC
       CALL parallel_broadcast_int_1D_array ( rndi, sknum )
#endif     
       rndi_sorted = rndi
       CALL IVEC_SORT_HEAP_A( sknum,rndi_sorted )                                                  
       mark_limit = rndi_sorted( np )                  ! mark all below the limit
       rndi_index = 1                                  ! current index in rndi(:)
       k_index = 1                                     ! current index in k(1:dim,:)
       rnp(1:dim) = 0
       c0: DO
          xyz(1) = 1                            ! X-face
          DO i2=1,N(2)
             xyz(2) = i2
             DO i3=1,N(3)
                xyz(3) = i3
                IF(rndi(rndi_index).LE.mark_limit) THEN             ! randomly mark
                   kav(1:dim,k_index) = xyz(1:dim)                  ! that point
                   k_index = k_index + 1
                   rnp(1) = rnp(1) + 1
                END IF
                rndi_index = rndi_index + 1
                IF (k_index.GT.np) EXIT c0                          ! k() array has been filled, end
                IF (rndi_index.GT.sknum) rndi_index = 1             ! reset random array marker, continue
             END DO
          END DO
          xyz(2) = 1                            ! Y-face
          DO i1=1,N(1)
             xyz(1) = i1
             DO i3=1,N(3)
                xyz(3) = i3
                IF(rndi(rndi_index).LE.mark_limit) THEN             ! randomly mark
                   kav(1:dim,k_index) = xyz(1:dim)                  ! that point
                   k_index = k_index + 1
                   rnp(2) = rnp(2) + 1
                END IF
                rndi_index = rndi_index + 1
                IF (k_index.GT.np) EXIT c0                          ! k() array has been filled, end
                IF (rndi_index.GT.sknum) rndi_index = 1             ! reset random array marker, continue
             END DO
          END DO
          xyz(3) = 1                            ! Z-face
          DO i1=1,N(1)
             xyz(1) = i1
             DO i2=1,N(2)
                xyz(2) = i2
                IF(rndi(rndi_index).LE.mark_limit) THEN             ! randomly mark
                   kav(1:dim,k_index) = xyz(1:dim)                  ! that point
                   k_index = k_index + 1
                   rnp(3) = rnp(3) + 1
                END IF
                rndi_index = rndi_index + 1
                IF (k_index.GT.np) EXIT c0                          ! k() array has been filled, end
                IF (rndi_index.GT.sknum) rndi_index = 1             ! reset random array marker, continue
             END DO
          END DO
       END DO c0
       DEALLOCATE (rnd, rndi, rndi_sorted)
    ELSE
       DO k_index = 1,np/3
          CALL RANDOM_NUMBER( rndu(1:dim-1) )
          kav(1,k_index) = 1
          kav(2,k_index) = INT( 1 + N(2)*rndu(1) )
          kav(3,k_index) = INT( 1 + N(3)*rndu(2) )
       END DO
       DO k_index = np/3+1,(2*np)/3
          CALL RANDOM_NUMBER( rndu(1:dim-1) )
          kav(1,k_index) = INT( 1 + N(1)*rndu(1) )
          kav(2,k_index) = 1
          kav(3,k_index) = INT( 1 + N(3)*rndu(2) )
       END DO
       DO k_index = (2*np)/3+1,np
          CALL RANDOM_NUMBER( rndu(1:dim-1) )
          kav(1,k_index) = INT( 1 + N(1)*rndu(1) )
          kav(2,k_index) = INT( 1 + N(2)*rndu(2) )
          kav(3,k_index) = 1
       END DO
       rnp(1) = np/3
       rnp(2) = (2*np)/3 - np/3
       rnp(3) = np - (2*np)/3
    END IF

    ! compute Fourier transform
    IF (par_rank.EQ.0) PRINT *, ' BEFORE ft_compute' 
    CALL ft_compute( res, kav, np, ie_arr, dim )                     !! This is the Fourier Transform of  u  not  \psi^{j}_{k}  ;so, we need parallel_global_sum in ft_compute. 
    IF (par_rank.EQ.0) PRINT *, ' AFTER ft_compute' 
                                                                                          
    ! --------------------------------------------------------------
    ! set k=N/2 values to zero according to the non-adaptive version
    ! and compute averages
    ! --------------------------------------------------------------
    e1(1:3) = 0.0_pr
    DO i = 1, rnp(1)
       IF ( (kav(2,i).EQ.nxyz(2)/2+1).OR.(kav(3,i).EQ.nxyz(3)/2+1) ) res(1:dim,i) = 0.0_pr
       e1(1) = e1(1) + SUM( ABS( res(1:dim,i) )**2 )
    END DO
    DO i = rnp(1)+1, rnp(1)+rnp(2)
       IF ( (kav(1,i).EQ.nxyz(1)/2+1).OR.(kav(3,i).EQ.nxyz(3)/2+1) )  res(1:dim,i) = 0.0_pr
       mult = 2.0_pr
       IF ( kav(1,i).EQ.1 ) mult = 1.0_pr
       e1(2) = e1(2) + mult*SUM( ABS( res(1:dim,i) )**2 )
    END DO
    DO i = rnp(1)+rnp(2)+1, np
       IF ( (kav(1,i).EQ.nxyz(1)/2+1).OR.(kav(2,i).EQ.nxyz(2)/2+1) )  res(1:dim,i) = 0.0_pr
       mult = 2.0_pr
       IF ( kav(1,i).EQ.1 ) mult = 1.0_pr
       e1(3) = e1(3) + mult*SUM( ABS( res(1:dim,i) )**2 )
    END DO
    e1(1:3) = e1(1:3) * knum(1:3)/(1.0_pr*rnp(1:3))
    e1_sum = SUM( e1(1:3) )
   !CALL parallel_global_sum( REAL=e1_sum )                          !! Redundant  each proc has the same copy of e1_sum
#ifdef AdaptiveSpectrum_DEBUG
    WRITE (*,'(A, I6, A, F12.5)')  'compute_1d_spectrum  par_rank=', par_rank,  '  e1_sum=', e1_sum
    IF (par_rank.EQ.0) THEN
    PRINT *, ' '
    WRITE (*,'(A, I6, A, F12.5)')  'compute_1d_spectrum  par_rank=', par_rank,  '  e1_sum=', e1_sum
    PRINT *, ' ';    PRINT *, ' '
    END IF
#endif     

    DEALLOCATE ( kav, res )
    IF ( .NOT.DO_UNIQUE_POINTS ) THEN                                ! add precomputed points (non-unique case only)
       ! does not improve accuracy much
    END IF

  END SUBROUTINE compute_1d_spectrum





  !------------------------------------------------------------------------------------------------
  ! PUBLIC
  ! adaptive version of PRINT_SPECTRA2 from SPECTRA_MODULE (fft/spectra.f90)
  SUBROUTINE print_spectra_a ( afilename, the_tke, the_tdiss, &
       enrg_total, diss_total, e1d_total, e1d_num )

    ! ---- the following parameters are be used to control accuracy ----
    ! NUMBER_OF_BINS, PERCENT_OF_POINTS_PER_BIN_TO_USE, MIN_POINTS_PER_BIN, MAX_POINTS_PER_BIN,
    ! ---- the following parameters are used to speed up the debugging ----
    ! DO_SHELL_SPECTRA, FT_RESULT_ON, FT_RESULT_FILE, FT_RESULT_READ, FT_RESULT_PRINT
    USE spectrum_parameters
    USE wlt_vars
    USE parallel 
    
    CHARACTER (LEN=*) , INTENT(IN)    :: afilename
    REAL(pr), INTENT(OUT) :: the_tke, the_tdiss        ! sum of shell averaged k \in [1,N/2]
    REAL(pr), INTENT(OUT) :: enrg_total, diss_total    ! estimate of sum of all k != 0
    REAL(pr), INTENT(OUT) :: e1d_total                 ! ... for "1D spectrum"
    INTEGER,  INTENT(OUT) :: e1d_num                   ! ... number of points dropped on the faces
    REAL(pr), ALLOCATABLE ::  enrg(:), diss(:)
    INTEGER  :: spectrafile, j, ios, N, points_per_bin, mppb
    INTEGER, DIMENSION(:,:), ALLOCATABLE :: k          ! K-vector
    COMPLEX, DIMENSION(:,:), ALLOCATABLE :: res        ! FT result for the given K-vector
    INTEGER  :: ie(1:MAXDIM)                           ! variable numbers
    REAL(pr) :: appb                                   ! actual percent of points per bin
    INTEGER  :: delta, r, np, ii, xyz(1:dim)
    INTEGER, ALLOCATABLE :: knum(:), klim(:,:)         ! number of points in a bin, bin limits
    INTEGER   :: anob                                  ! actual number of bins
    INTEGER*8 :: i_pe(0:dim)                           ! local analog of I_P()
    INTEGER*8 :: ii_64b
    REAL(pr)  :: mult, tmp1, tmp2, tmpv(1:dim), e
    INTEGER   :: k_max_size

#ifdef AdaptiveSpectrum_DEBUG
    CHARACTER (LEN=par_LENSTR1) :: my_par_rank_str, &  ! par_rank_str of .res file
                                   tmp_str
#endif     

    ! Temporary variables for Parallel Mathematical Operations (e.g. SUM, MIN, MAX)
    REAL (pr)   :: tmp5, tmp6
    
    e1d_num = 0
    e1d_total = 0.0_pr
    IF ( .NOT. DO_SHELL_SPECTRA ) THEN
       the_tke = 0.0_pr
       the_tdiss = 0.0_pr
       enrg_total = 0.0_pr
       diss_total = 0.0_pr
       RETURN
    END IF
    
    N = MINVAL( nxyz(1:dim) ) / 2               ! maximum K-vector's value    
    
    DO j=1,dim                                  ! FT for the first three variables
       ie(j) = j
    END DO
    
    delta = MAX( 1,N/NUMBER_OF_BINS )           ! radial size of a K-shell
    anob = N/delta                              ! actual number of bins to use
    
    ALLOCATE( enrg(anob), diss(anob), knum(anob), klim(anob,1:2), STAT=ios )
    CALL test_alloc (ios,'print_spectra2: alloc enrg, diss, knum, klim')    

    klim(1,1) = 0
    klim(1,2) = delta
    DO j=2,anob                                 ! set shell limits:  LO < x <= HI
       klim(j,1) = klim(j-1,2)                  ! LO: lower limit of the shell
       klim(j,2) = klim(j,1) + delta            ! HI: upper limit of the shell
    END DO
    klim(anob,2) = N

    i_pe(0) = 1                                                  ! initialize coordinate transform
    DO ii=1,dim
       i_pe(ii) = i_pe(ii-1)*(nxyz(ii))                          ! xyz \in [0..N-1]
    END DO


    !!?????????????????????????!! This Big Loop Must be modified::  1) Find np based on  Ratio of Volumes. 
    !!                                                              2) For low frequencies, do the exact count based on a smaller domain 
    !!                                                                 For smaller domain, both   nxyz(1:dim) & i_pe(0:dim)   must be calculated
    !!
    knum = 0                                    ! count number of points inside each shell
    DO ii_64b=1,PRODUCT( nxyz(1:dim) )                                                             
       xyz(1:dim) = INT(MOD(ii_64b-1,i_pe(1:dim))/i_pe(0:dim-1)) + 1     ! xyz \in [1..N]
       r = INT( SQRT( SUM(fsq(xyz(1:dim))) ) )
       
       IF ((r.GT.0).AND.(r.LE.N).AND.(xyz(1).LE.N+1)) THEN
          
          np = (r-1) / delta + 1                                         ! point belongs to bin NP
          knum(np) = knum(np) + 1
       END IF
    END DO


    ! compute maximum number of points per bin
    np = INT( MAXVAL(knum(:))*PERCENT_OF_POINTS_PER_BIN_TO_USE/100.0_pr )
    np = MIN( MAX( MIN_POINTS_PER_BIN, np ), MAX_POINTS_PER_BIN )
    np = MIN( np, MAXVAL(knum(:)) )

    ALLOCATE( k(1:dim,1:np), res(1:dim,1:np), STAT=ios )
    CALL test_alloc (ios,'print_spectra2: alloc k, res')
    k_max_size = np
    
    
    ! compute spectrum
    IF ( FT_RESULT_ON .AND. par_rank.EQ.0 )  OPEN(77, FILE='res.data', STATUS='UNKNOWN', FORM='UNFORMATTED')
    
    DO j=1,anob
       np = INT( knum(j)*PERCENT_OF_POINTS_PER_BIN_TO_USE/100.0_pr )   ! number of k-vectors to initialize inside the shell
       np = MIN( MAX( MIN_POINTS_PER_BIN, np ), MAX_POINTS_PER_BIN )   !  np \in [min..max]
       np = MIN( np, knum(j) )                                         !  np < knum
       appb = np/( 1.0_pr*knum(j) )                                    ! actual percent of points per bin
       
       IF ( FT_RESULT_PRINT .AND. par_rank.EQ.0 ) &
       WRITE (*,ADVANCE='NO',FMT='("bin=",I5,", number of points is ",I10," of ",I10,", which is ",I3," %")' )   j, np, knum(j), INT(appb*100)
       
       IF ((.NOT.FT_RESULT_ON).OR.(FT_RESULT_ON.AND.(.NOT.FT_RESULT_READ))) THEN  ! ------- compute Fourier transform ------
          
          !!!!!!  set_ku & set_k   need to be called only from  par_rank.EQ.0. 
          !!!!!!  because in orer to have unique   k(1:dim, 1:k_size)   on all processes, we need  broadcast.
          !!!!!!  But this seems not working correctly in serial! Not sure why! So, I commented the IF out and let all ranks call set_ku/set_k.
!!          IF (par_rank.EQ.0) THEN
             IF ( j.LE.UNIQUE_LIMIT ) THEN
                CALL set_ku ( k, np, klim(j,1), klim(j,2), N, knum(j) )    !  ... unique assignment
             ELSE
                CALL set_k ( k, np, klim(j,1), klim(j,2), N )              ! assign np k-points to the given shell, non-unique
             END IF
!!          END IF

#ifdef AdaptiveSpectrum_DEBUG
          WRITE( tmp_str,'(I'//CHAR(ICHAR('0')+par_LENSTR1)//')' ) par_rank
          my_par_rank_str = '.p'//TRIM(ADJUSTL(tmp_str))
    
          OPEN(111,FILE=TRIM( 'k__'//TRIM(my_par_rank_str)//'.o' ), STATUS='UNKNOWN',POSITION='APPEND')
          WRITE(111,'("bin=",I5," BEFORE   parallel_broadcast_int_array ")')   j
          DO ii=1,np
             WRITE(111,'(A, I6, A, I6, A, 3I12)') 'rank', par_rank, '  ii=', ii, '  k(:,ii)=', k(:,ii)
          END DO
          CLOSE(111)
#endif     
#ifdef MULTIPROC
          CALL parallel_broadcast_int_array ( k, dim*np )
#endif     
#ifdef AdaptiveSpectrum_DEBUG
          WRITE( tmp_str,'(I'//CHAR(ICHAR('0')+par_LENSTR1)//')' ) par_rank
          my_par_rank_str = '.p'//TRIM(ADJUSTL(tmp_str))
    
          OPEN(111,FILE=TRIM( 'k__'//TRIM(my_par_rank_str)//'.o' ), STATUS='UNKNOWN',POSITION='APPEND')
          WRITE(111,'("bin=",I5," AFTER   CALL parallel_broadcast_int_array ( k, dim*np )")')   j
          DO ii=1,np
             WRITE(111,'(A, I6, A, I6, A, 3I12)') 'rank', par_rank, '  ii=', ii, '  k(:,ii)=', k(:,ii)
          END DO
          CLOSE(111)
#endif     

          CALL ft_compute( res, k, np, ie, dim )                        ! compute FT for the given k-points
          DO ii=1,np                                                    ! set k=N/2 to zero
             IF ( ANY( k(1:dim,ii).EQ.nxyz(1:dim)/2+1 ) ) &             ! to mimic the behaviour of
                  res(1:dim,ii) = 0.0_pr                                ! the previous non-adaptive version
          END DO
          
       END IF                                                                     ! ------- read/write
       IF (FT_RESULT_ON) THEN
          IF ( .NOT. FT_RESULT_READ ) THEN
             IF (par_rank.EQ.0) &
             WRITE (77) res, k, np, appb
          ELSE
             READ (77) res, k, np, appb
          END IF
       END IF
       
       CALL total_k ( res, k, np, enrg(j), diss(j), appb, &             ! average k-values inside the shell
                      e1d_total, e1d_num )                              ! precompute value for "1D spectrum"
       
       IF ( FT_RESULT_PRINT .AND. par_rank.EQ.0 )   WRITE (*,'(20x,"enrg(j)=",F15.6,10x,"diss(j)=",F15.6)' )   enrg(j), diss(j)
    END DO
    
    ! total through the shells
    the_tke   = SUM(enrg)
    the_tdiss = SUM(diss)
    
    ! --- estimate total through the whole area ---
    !mult = (nxyz(1)/2+1)*PRODUCT(nxyz(2:dim)) / (1.0_pr*SUM(knum(1:N)))   ! extrapolate average - not very accurate
    !mult = 1.0_pr + enrg(anob)/the_tke * &                                ! extrapolate last    - looks better
    !     ( (nxyz(1)/2+1)*PRODUCT(nxyz(2:dim)) - SUM(knum(1:N)) )          !
    !enrg_total = the_tke*mult
    !diss_total = the_tdiss*mult
    ! --- compute total through the whole area ---                                               - practically exact
    ii = 1
    DO WHILE ( ii.LE.k_max_size )
       CALL RANDOM_NUMBER( tmpv(1:dim) )                                                           !??????????????????????????????????????????????????????
       xyz(1:dim) = INT( nxyz(1:dim)*tmpv(1:dim) ) + 1
       r = INT( SQRT( SUM(fsq(xyz(1:dim))) ) )
       IF ( ((r.EQ.0).OR.(r.GT.N)).AND.(xyz(1).LE.N+1) ) THEN
          k(1:dim,ii) = xyz(1:dim)
          ii = ii + 1
       END IF
    END DO
#ifdef AdaptiveSpectrum_DEBUG
    WRITE (*,'(A, I6, A, I12.5)')  'print_spectra_a  par_rank=', par_rank,  '  ii=', ii
#endif     

    IF ((.NOT. FT_RESULT_ON).OR.(FT_RESULT_ON.AND.(.NOT.FT_RESULT_READ))) THEN
       CALL ft_compute( res, k, k_max_size, ie, dim )
    END IF
    IF (FT_RESULT_ON) THEN
       IF ( .NOT. FT_RESULT_READ ) THEN
          IF (par_rank.EQ.0) &
          WRITE (77) res, k, np, appb
       ELSE
          READ (77) res, k, np, appb
       END IF
       IF (par_rank.EQ.0) CLOSE (77)
    END IF

    tmp1 = 0.0_pr
    tmp2 = 0.0_pr
    DO ii=1,k_max_size
       mult = 1.0_pr
       IF (fsq(k(1,ii)).EQ.0) mult = 0.5_pr
       
       e = mult * SUM( ABS(res(1:dim,ii))**2 )
       tmp1 = tmp1 + e                           ! energy through the noncircular chunk
       tmp2 = tmp2 + e * SUM( fsq(k(1:dim,ii)) ) ! dissipation ...
    END DO

    mult = ( (nxyz(1)/2+1)*PRODUCT(nxyz(2:dim)) - SUM(knum(1:anob)) ) / (1.0_pr * k_max_size)
    tmp1 = tmp1 * mult
    tmp2 = tmp2 * mult
    enrg_total = the_tke + tmp1                 ! energy through the whole area
    diss_total = the_tdiss + tmp2               ! dissipation ...

    ! Should not perform parallel_global_sum of  enrg_total,diss_total,the_tke,the_tdiss.
    ! They are all global since  diss(:) and enrg(:)  are global. 



    appb = anob/( 1.0_pr*N )
    spectrafile = 10
    IF (par_rank.EQ.0) THEN
       OPEN(FILE=afilename,UNIT=spectrafile)
       WRITE (spectrafile,*) '% wave_number enrg1 diss1 '
    END IF
    DO r=1,N
       DO j=1,anob
          IF ((r.GT.klim(j,1)).AND.(r.LE.klim(j,2))) THEN
             tmp5 = enrg(j)*appb
             tmp6 = diss(j)*appb
             ! There should not be any parallel_global_sum  on  diss(j)  and  enrg(j)
             ! They are both global since  ft_compute()  returns global res(:,:) 
                
             IF (par_rank.EQ.0) &
             WRITE (spectrafile,*) r,' ',tmp5,' ',tmp6
             !!!WRITE (spectrafile,*) r,' ',enrg(j)*appb,' ',diss(j)*appb
          END IF
       END DO
    END DO
    IF (par_rank.EQ.0) &
    CLOSE(spectrafile)

    DEALLOCATE( enrg, diss, k, res, knum, klim, STAT=ios )
    CALL test_alloc (ios,'free enrg, diss, k, res')
    
  END SUBROUTINE print_spectra_a



  !------------------------------------------------------------------------------------------------
  ! PRIVATE
  ! assignment of k-points to the given shell
  ! non-unique
  SUBROUTINE set_k (k, k_size, klim1, klim2, N )
    USE wlt_vars
    INTEGER, INTENT(IN) :: k_size                           ! working length of k(:)
    INTEGER, INTENT(IN) :: klim1, klim2                     ! limits of the shell
    INTEGER, INTENT(IN) ::  N                               ! maximum k
    INTEGER, INTENT(OUT) :: k(1:dim, 1:k_size)              ! asigneg k-points inside the shell
    REAL(pr) :: rnd(1:dim), pi, rppb
    INTEGER :: i, point, xyz(1:dim), r
    
    !CALL RANDOM_SEED
    ! distribute k_size k-vectors inside the bin
    point = 1
    DO WHILE (point.LE.k_size)
       CALL RANDOM_NUMBER( rnd )
       
       xyz(1) = INT( 1 + (nxyz(1)/2+1)*rnd(1) )
       xyz(2:3) = INT( 1 + nxyz(2:3)*rnd(2:3) )
       r = INT( SQRT( SUM(fsq(xyz(1:dim))) ) )
       
       IF ((r.GT.klim1).AND.(r.LE.klim2)) THEN
          k(1:dim,point) = xyz(1:dim)
          point = point + 1
       END IF
       
    END DO

  END SUBROUTINE set_k



  !------------------------------------------------------------------------------------------------
  ! PRIVATE
  ! assignment of k-points to the given shell, k \in [1..N]
  ! unique
  ! called from PRINT_SPECTRA2
  SUBROUTINE set_ku (k, k_size, klim1, klim2, k_max, knum)
    USE wlt_vars
    USE parallel
    
    INTEGER, INTENT(IN)  :: k_size                                ! working length of k(:)
    INTEGER, INTENT(IN)  :: klim1, klim2                          ! limits of the shell
    INTEGER, INTENT(IN)  ::  k_max, knum                          ! maximum k, maximum number of k
    INTEGER, INTENT(OUT) :: k(1:dim, 1:k_size)                    ! asigneg k-points inside the shell
    INTEGER :: j, rndi_index, i1, i2, i3, r, k_index
    INTEGER :: xyz(1:dim), ii, mark_limit, N
    REAL(pr), ALLOCATABLE :: rnd(:)
    INTEGER,  ALLOCATABLE :: rndi(:), rndi_sorted(:)
    
    !CALL RANDOM_SEED
    ALLOCATE ( rnd(knum), rndi(knum), rndi_sorted(knum) )
    
    ! set appropriate random array and mark limit
    CALL RANDOM_NUMBER( rnd )                       ! rnd \in [0..1]
    rndi = INT( rnd*knum )                          ! rndi \in [0..knum]
    rndi_sorted = rndi
    CALL IVEC_SORT_HEAP_A( knum,rndi_sorted )
    mark_limit = rndi_sorted( k_size )              ! mark all below the limit


!!$    PRINT *, 'klim1, klim2, k_size, knum',klim1, klim2, k_size, knum
!!$    PRINT *, 'rndi=',rndi(1:knum)
!!$    PRINT *, 'rndi_sorted=',rndi_sorted(1:knum)
!!$    PRINT *, 'mark_limit=', mark_limit

    rndi_index = 1                           ! current index in rndi(:)
    k_index = 1                              ! current index in k(1:dim,:)
    

    N = MINVAL( nxyz(1:dim) ) / 2

    c0: DO
       c1: DO i1=1,nxyz(1)/2+1
          xyz(1) = i1
          DO i2=1,nxyz(2)
             xyz(2) = i2
             DO i3=1,nxyz(3)
                xyz(3) = i3
                r = INT( SQRT( SUM(fsq(xyz(1:dim))) ) )
                IF ((r.GT.klim1).AND.(r.LE.klim2)) &
                     THEN                                              ! point inside the shell
                   IF(rndi(rndi_index).LE.mark_limit) THEN             ! randomly mark
                      k(1:dim,k_index) = xyz(1:dim)                    ! that point
                      k_index = k_index + 1
                   END IF
                   rndi_index = rndi_index + 1
                   IF (k_index.GT.k_size) EXIT c0                      ! k() array has been filled, end
                   IF (rndi_index.GT.knum)  THEN
                      rndi_index = 1                                   ! reset random array marker, continue
                      PRINT *,'reset random counter, k_index =', k_index
                   END IF
                END IF
             END DO
          END DO
       END DO c1
    END DO c0
    
!!$    PRINT *,'k_size=',k_size, 'k_index=',k_index, 'k(:)='
!!$    DO j=1,k_size
!!$       PRINT *, k(:,j), 'r=',INT( SQRT( SUM(fsq(k(1:dim,j))) ) )
!!$    END DO
!!$    !STOP
#ifdef AdaptiveSpectrum_DEBUG
    PRINT *, ' '
    WRITE (*,'(A, I6, A, 3I4, A, I9, A, I9)')  'set_ku  par_rank=', par_rank,  '  nxyz(1:dim)=', nxyz(1:dim), '  rndi_index=', rndi_index, '  k_index=', k_index
    PRINT *, ' '
#endif     
    
    DEALLOCATE (rnd, rndi, rndi_sorted)
    
  END SUBROUTINE set_ku
 
 
 
  !------------------------------------------------------------------------------------------------
  ! PRIVATE
  ! compute total energy inside the shell (similar to the non-adaptive version)
  ! called from PRINT_SPECTRA2
  SUBROUTINE total_k (res, k, k_size, enrg, diss, appb, e1d_total, e1d_num)
    USE wlt_vars
    INTEGER,  INTENT(IN)    :: k_size                        ! working length of k(:) and res(:)
    INTEGER,  INTENT(IN)    :: k(1:dim, 1:k_size)
    COMPLEX,  INTENT(IN)    :: res(1:dim, 1:k_size)          ! F(k)
    REAL(pr), INTENT(IN)    :: appb                          ! actual percent of points per bin
    REAL(pr), INTENT(OUT)   :: enrg, diss
    REAL(pr), INTENT(INOUT) :: e1d_total                     ! precomputing value "1D spectrum"
    INTEGER,  INTENT(INOUT) ::  e1d_num                      ! ...
    INTEGER  :: i
    REAL(pr) :: e, k2, mult, e1(1:3)

    enrg = 0.0_pr
    diss = 0.0_pr
    e1 = 0.0_pr
    DO i=1,k_size

       mult = 1.0_pr
       IF (fsq(k(1,i)).EQ.0) mult = 0.5_pr

       e = mult * SUM( Real(res(1:dim,i))**2 + Aimag(res(1:dim,i))**2 )
       enrg = enrg + e
       k2 = SUM( fsq(k(1:dim,i)) )
       diss = diss + k2 * e

       ! precompute "1D spectrum" things
       IF ( k(1,i).EQ.1 ) THEN
          e1(1) = e1(1) + 2.0 * e
          e1d_num = e1d_num + 1
       END IF
       IF ( k(2,i).EQ.1 ) THEN
          e1(2) = e1(2) + 2.0 * e
          e1d_num = e1d_num + 1
       END IF
       IF ( k(3,i).EQ.1 ) THEN
          e1(3) = e1(3) + 2.0 * e
          e1d_num = e1d_num + 1
       END IF
    END DO
    enrg = enrg/appb
    diss = diss/appb
    e1d_total = e1d_total + SUM( e1(1:3) )
    
  END SUBROUTINE total_k



  !------------------------------------------------------------------------------------------------
  ! PUBLIC
  !   compute Fourier transform
  !   for the given values of k(:),   k \in [1..N]
  !   for the given variables ie(:)
  ! uses global I_P(:) initialized in INIT_FFT.IXYZ2INDX_INIT
  SUBROUTINE ft_compute( res, k, k_size, ie, ie_size )
    USE pde
    USE field
    USE parallel         
    USE wlt_trns_vars
    
    INTEGER, INTENT(IN) :: k_size                         ! length of k(:)
    INTEGER, INTENT(IN) :: ie_size                        ! length of ie(:)
    INTEGER, INTENT(IN) :: k(1:dim, 1:k_size)             ! kx(1), ky(1), kx(2), ky(2), ...
    COMPLEX, INTENT(OUT) :: res(1:ie_size,1:k_size)       ! FT result
    INTEGER, INTENT(IN) :: ie(1:ie_size)                  ! array of variables to compute FFT for
    INTEGER :: ie_cur, k_cur                              ! current index
    INTEGER :: nn, j, wlt_type, face_type, j_df, kk, ios
    INTEGER :: xyz(1:MAXDIM), iwlt, done, step
    COMPLEX :: summ(ie_size), fac(1:MAXDIM), ONE
    INTEGER*8 :: ii

    ! Temporary variables 
    REAL (pr)   :: tmp_Re, tmp_Im


    ONE = -1.0D0
    fac(1:dim) = -2.0D0 * DACOS (-1.0D0) * CSQRT(ONE) / nxyz(1:dim)     ! -2\pi i / N(1:dim)
    
    done = 0
    step = k_size/100

    ! for all k(:), for all ie(:)
    DO ie_cur = 1, ie_size
       DO k_cur = 1, k_size
          
          ! ------------------------ level 1 -----------------------------
          summ(ie_cur) = (0.0,0.0)
          nn = 0
          j = 1
          wlt_type = 0
          face_type = (3**dim - 1)/2
          DO j_df = j, j_lev
             DO kk = 1, indx_DB(j_df,wlt_type,face_type,j)%length
                ii = indx_DB(j_df,wlt_type,face_type,j)%p(kk)%ixyz
                iwlt = indx_DB(j_df,wlt_type,face_type,j)%p(kk)%i
                xyz(1:dim) = INT(MOD(ii-1,i_p(1:dim))/i_p(0:dim-1)) + 1

                ! x \in [1..N]
                ! k \in [1..N]
                ! FFT reconstruction
                summ(ie_cur) = summ(ie_cur) + u(iwlt,ie_cur) * &
                     get_phi( k(1:dim,k_cur) ) * &
                     CEXP (  SUM( fac(1:dim) * (k(1:dim,k_cur)-1) * (nxyz(1:dim)-xyz(1:dim)) )  )
                
!!$                ! function reconstruction
!!$                summ(ie_cur) = summ(ie_cur) + u(iwlt,ie_cur) * get_f3 ( k(:,k_cur), xyz(1:dim), 1, 0 )

                nn = nn + 1
             END DO
          END DO


          ! ------------------------ levels 2:j_lev ----------------------
          DO j = 2, j_lev
             DO wlt_type = 1,2**dim-1
                DO face_type = 0, 3**dim - 1
                   DO j_df = j, j_lev
                      DO kk = 1, indx_DB(j_df,wlt_type,face_type,j)%length            ! sweep through all wavelets
                         
                         ii = indx_DB(j_df,wlt_type,face_type,j)%p(kk)%ixyz           ! 1D coordinate
                         iwlt = indx_DB(j_df,wlt_type,face_type,j)%p(kk)%i            ! wavelet number
                         xyz(1:dim) = INT(MOD(ii-1,i_p(1:dim))/i_p(0:dim-1)) + 1      ! 3D coordinate
                         
                         ! FFT reconstruction
                         summ(ie_cur) = summ(ie_cur) + u(iwlt,ie_cur) * get_psi( k(1:dim,k_cur),j,wlt_type ) * &
                              CEXP (  SUM( fac(1:dim) * (k(1:dim,k_cur)-1) * (nxyz(1:dim)-xyz(1:dim)) )  )

                         ! function reconstruction
                         ! summ(ie_cur) = summ(ie_cur) + u(iwlt,ie_cur) * get_f3 ( k(:,k_cur), xyz(1:dim), j, wlt_type )

                         nn = nn + 1
                      END DO
                   END DO
                END DO
             END DO
          END DO
          
          tmp_Re = real(summ(ie_cur))
          tmp_Im = aimag(summ(ie_cur))

          ! res(:,:) is the Fourier Transform of  u  not  \psi^{j}_{k}  ;so, we need parallel_global_sum here. 
          CALL parallel_global_sum( REAL=tmp_Re )  
          CALL parallel_global_sum( REAL=tmp_Im )


          res (ie_cur,k_cur) = CMPLX(tmp_Re,tmp_Im,kind(tmp_Re))
          
#ifdef AdaptiveSpectrum_DEBUG
!    WRITE (*,'(A, I6, A, I12)')  ' par_rank=', par_rank, '  nn=', nn
#endif     
       END DO
    END DO

  END SUBROUTINE ft_compute
  
  
  
  !-----------------------------------------------------------------
  ! PUBLIC, to be used in standalone version
  ! read result file
  ! call subroutine to initialize table of wavelet functions
  SUBROUTINE ft_read_var ( filename )
    USE pde
    USE field
    USE io_3D
    USE io_3D_vars
    CHARACTER*(*), INTENT(IN) :: filename
    
    CALL read_solution_dim ( filename )
    CALL read_solution ( scl, filename, DO_READ_ALL_VARS_TRUE, DO_READ_COMMONFILE_FALSE )
    n_var = SIZE(u_variable_names(:))

    ! at this point we have:
    !  scl(1:n_var), indx_DB(), u(1:nwlt,1:n_var)
    !  u_variable_names, scl_global, n_var_wlt_fmly, xx
    
    CALL init_fft
    
!!$    ! debug
!!$    CALL print_solution
    
  END SUBROUTINE ft_read_var
  
  
  
  !-----------------------------------------------------------------
  ! PUBLIC
  ! initialize table of wavelet functions
  ! initialize coordinate transformation array
  ! initialize FSQ array (similar to non-adaptive spectra version)
  SUBROUTINE init_fft
    USE wlt_vars
    INTEGER :: i
    REAL(pr) :: tmp

    ! check periodicity
    IF (ANY(prd(1:dim).EQ.0)) THEN
       PRINT *, 'ERROR: periodic data required'
       STOP 1
    END IF
    
    CALL set_table
    CALL ixyz2indx_init
    
    ! just in case, prohibit non-square grids
    ! I will think later if it matters
    IF (MINVAL(mxyz(1:dim)).NE.MAXVAL(mxyz(1:dim))) THEN
       PRINT *, 'ERROR: equal MXYZ data required (it is a BUG)'
       PRINT *, 'This will be fixed in the future'
       STOP 1
    END IF
    
    ! initialize fsq array [1..N] = 0,1,2,...,N/2,...,2,1
    IF (ALLOCATED(fsq)) DEALLOCATE (fsq)
    ALLOCATE ( fsq(nxyz(1)) )
    fsq (1)           = 0.0_pr
    fsq (nxyz(1)/2+1) = 1.0_pr * (nxyz(1)/2)**2
    DO i=2,nxyz(1)/2                            !  0, 1,2,3, 4, 3,2,1  of [1..8]
       tmp = 1.0_pr * (i-1)**2
       fsq (i)           = tmp
       fsq (nxyz(1)-i+2) = tmp
    END DO
!!$    PRINT *, fsq
!!$    STOP

  END SUBROUTINE init_fft
  
  
  
  !-----------------------------------------------------------------
  ! PUBLIC, for the standalone version
  ! clean the memory
  SUBROUTINE ft_clean_var
    USE field
    USE io_3D
    USE wlt_trns_vars
    INTEGER :: ios
    
    CALL clean_after_read_solution
    IF (ASSOCIATED(scl)) THEN 
       DEALLOCATE (scl, STAT=ios)
       CALL test_alloc (ios,'scl')
    END IF
    IF (ALLOCATED(indx_DB)) THEN
       DEALLOCATE (indx_DB, STAT=ios)
       CALL test_alloc (ios,'indx_DB')
    END IF
    IF (ALLOCATED(u)) THEN
       DEALLOCATE (u, STAT=ios)
       CALL test_alloc (ios,'u')
    END IF
    IF (ALLOCATED(PSI)) THEN
       DEALLOCATE (PSI, STAT=ios)
       CALL test_alloc (ios,'PSI')
    END IF
    IF (ALLOCATED(PHI)) THEN
       DEALLOCATE (PHI, STAT=ios)
       CALL test_alloc (ios,'PHI')
    END IF
    IF (ALLOCATED(fsq)) DEALLOCATE(fsq)
  END SUBROUTINE ft_clean_var
  
  
  
  !-----------------------------------------------------------------
  ! PUBLIC, to clean inside the loop through stations
  ! clean the memory except for U_VARIABLE_NAMES
  SUBROUTINE ft_clean_var_2
    USE field
    USE wlt_trns_vars
    INTEGER :: ios
    
    CALL clean_after_read_solution_2
    IF (ASSOCIATED(scl)) THEN 
       DEALLOCATE (scl, STAT=ios)
       CALL test_alloc (ios,'scl')
    END IF
    IF (ALLOCATED(indx_DB)) THEN
       DEALLOCATE (indx_DB, STAT=ios)
       CALL test_alloc (ios,'indx_DB')
    END IF
    IF (ALLOCATED(u)) THEN
       DEALLOCATE (u, STAT=ios)
       CALL test_alloc (ios,'u')
    END IF
    IF (ALLOCATED(PSI)) THEN
       DEALLOCATE (PSI, STAT=ios)
       CALL test_alloc (ios,'PSI')
    END IF
    IF (ALLOCATED(PHI)) THEN
       DEALLOCATE (PHI, STAT=ios)
       CALL test_alloc (ios,'PHI')
    END IF
    IF (ALLOCATED(fsq)) DEALLOCATE(fsq)
  END SUBROUTINE ft_clean_var_2

  











  !------------------------------------------------------------------------------------------------
  !------------------------------------------------------------------------------------------------
  ! PRIVATE
  !------------------------------------------------------------------------------------------------
  !------------------------------------------------------------------------------------------------
  ! return FT of wavelet function of level 1
  ! k - is the real K vector, k \in [1:N]
  FUNCTION get_phi ( k )
    USE wlt_vars
    COMPLEX :: get_phi
    INTEGER, DIMENSION(1:dim), INTENT(IN) :: k
    
    get_phi = PRODUCT ( PHI(1,k) )

  END FUNCTION get_phi
  !-----------------------------------------------------------------
  ! return FFT of wavelet function of given level and type
  ! k - is the real K vector, k \in [1:N]
  ! j - is wavelet level
  ! type - is wavelet type
  FUNCTION get_psi ( k, j, type )
    USE wlt_vars
    COMPLEX :: get_psi
    INTEGER,                   INTENT(IN) :: j, type
    INTEGER, DIMENSION(1:dim), INTENT(IN) :: k
    INTEGER :: i
    COMPLEX :: f(1:dim)
    
    DO i=1,dim
       f(i) = IBITS( type, dim-i, 1 ) * PSI( j,k(i) ) + &
            (1-IBITS( type, dim-i, 1 )) * PHI( j-1,k(i) )
    END DO
    get_psi = PRODUCT ( f )

  END FUNCTION get_psi
  !-----------------------------------------------------------------
  ! return wavelet function itself
  FUNCTION get_f3 ( k, x, j, type )
    USE wlt_vars
    REAL :: get_f3
    INTEGER,                   INTENT(IN) :: j, type
    INTEGER, DIMENSION(1:dim), INTENT(IN) :: k,x
    INTEGER :: i, c(1:dim)
    REAL    :: f(1:dim)

    c = nxyz - MOD( ABS(k-x) + 2*nxyz, nxyz)
    IF (type.EQ.0) THEN                     ! level 1
       get_f3 = PRODUCT ( PHI(1,c) )
    ELSE                                    ! level > 1
       DO i=1,dim
          f(i) = IBITS( type, dim-i, 1 ) * PSI( j,c(i) ) + &
               (1-IBITS( type, dim-i, 1 )) * PHI( j-1,c(i) )
       END DO
       get_f3 = PRODUCT ( f ) 
    END IF

  END FUNCTION get_f3
  !-----------------------------------------------------------------
  ! initialize global I_P for use in FT_COMPUTE
  ! transform 1d into 3d coordinate
  ! similar to weights from wavelet_3d_wrk.f90
  SUBROUTINE ixyz2indx_init
    USE wlt_vars
    INTEGER :: i

    i_p(0) = 1
    DO i=1,dim
       i_p(i) = i_p(i-1)*(1+nxyz(i))
    END DO
!!$    ii = indx_DB(j_df,wlt_type,face_type,j)%p(k)%ixyz
!!$    ixyz(1:dim) = INT(MOD(ii-1,i_p(1:dim))/i_p(0:dim-1))
    
  END SUBROUTINE ixyz2indx_init
  !-----------------------------------------------------------------
  ! compute wavelet functions
  ! through 1D inv wlt transform
  SUBROUTINE set_table_aux
    USE wlt_xxx
    USE wlt_trns_util_mod
    REAL(pr),                           ALLOCATABLE :: x_local(:)
    REAL (pr), DIMENSION (:,:,:,:,:,:), ALLOCATABLE :: wgh_updt, wgh_prdct
    INTEGER,   DIMENSION (:,:,:,:),     ALLOCATABLE :: lvxyz_odd,lvxyz_even
    INTEGER,   DIMENSION (:,:,:,:,:),   ALLOCATABLE :: nlvxyz_odd,nlvxyz_even
    LOGICAL,   DIMENSION (:),           ALLOCATABLE :: i_c

    INTEGER :: N, M, i_coef, wlt_fmly, trnsf_type, jd_local, j_in, j_out, j
    INTEGER :: idim, JJ, IDIM_, ios, D, ix, ix_even, ix_odd, ix_l, ix_h, ixpm
    INTEGER :: NXUZ_(1:MAXDIM), MXUZ_(1:MAXDIM), DIM_, NWLT_

    i_coef = -1
    IDIM_ = MAXLOC(nxyz(1:dim),1)
    N = nxyz( IDIM_ )
    M = mxyz( IDIM_ )

    ! save globals
    DIM_ = dim
    NXUZ_(1:dim) = nxyz(1:dim)
    MXUZ_(1:dim) = mxyz(1:dim)
    NWLT_ = nwlt
    
    IF (ALLOCATED (PSI)) DEALLOCATE (PSI)
    IF (ALLOCATED (PHI)) DEALLOCATE (PHI)
    ALLOCATE( PSI(2:j_lev,1:N), PHI(1:j_lev,1:N), x_local(0:N), STAT=ios )
    CALL test_alloc (ios, 'PSI,PHI,x in set_table_aux')
    PSI = (0.0D0, 0.0D0)
    PHI = (0.0D0, 0.0D0)
    x_local = 0.0_pr

    ALLOCATE( wgh_updt(-2*MAXVAL(n_updt):2*MAXVAL(n_updt),0:MAXVAL(nxyz),j_lev-1,0:n_wlt_fmly,0:n_trnsf_type,1:3), STAT=ios )
    
#ifdef AdaptiveSpectrum_DEBUG
    PRINT *, ' n_updt =', n_updt
    PRINT *, ' nxyz   =', nxyz
#endif     
        
    CALL test_alloc (ios, 'wgh_updt in set_table_aux')
    ALLOCATE( wgh_prdct(-2*MAXVAL(n_prdct):2*MAXVAL(n_prdct),0:MAXVAL(nxyz),j_lev-1,0:n_wlt_fmly,0:n_trnsf_type,1:3), STAT=ios )
    CALL test_alloc (ios, 'wgh_prdct in set_table_aux')
    wgh_updt  = 0.0_pr 
    wgh_prdct = 0.0_pr

    ! prepare globals for 1D wavelet transform
    nxyz(1:dim) = 1; nxyz(1) = N
    mxyz(1:dim) = 1; mxyz(1) = M
    prd(1:dim) = 1
    jd_local = 0
    dim = 1
    nwlt = N-1
    j_in = j_lev        !  Forward transform is done from level j_in to  1
    j_out = j_lev       !  Inverse transform is done from level  1 to  j_out

    ALLOCATE( i_c(0:N) )
    i_c = .TRUE.
    ALLOCATE( lvxyz_odd(0:j_lev,1:3,0:n_wlt_fmly,0:n_trnsf_type),       lvxyz_even(0:j_lev,1:3,0:n_wlt_fmly,0:n_trnsf_type) )
    ALLOCATE( nlvxyz_odd(1:5,1:3,1:2*nwlt,0:n_wlt_fmly,0:n_trnsf_type),nlvxyz_even(1:5,1:3,1:2*nwlt,0:n_wlt_fmly,0:n_trnsf_type))

    DO trnsf_type = 0,n_trnsf_type
       DO wlt_fmly = 0, n_wlt_fmly
          idim = 1
          DO j = 1, j_lev-1
             DO ix = 1, mxyz(idim)*2**(j-1)
                ix_odd = (2*ix-1)*2**(j_lev-1-j)
                ix_l   = -MIN(ix-trnsf_type,n_prdct(wlt_fmly))
                ix_h   = ix_l+2*n_prdct(wlt_fmly)-1
                ix_h   = MIN(ix_h,mxyz(idim)*2**(j-1)-ix-trnsf_type)
                ix_l   = ix_h-2*n_prdct(wlt_fmly)+1
                ix_l = (1-prd(idim))*MAX(ix_l,-ix+trnsf_type) + prd(idim)*(-n_prdct(wlt_fmly))
                ix_h = (1-prd(idim))*MIN(ix_h,mxyz(idim)*2**(j-1)-ix-trnsf_type) + prd(idim)*(n_prdct(wlt_fmly) - 1)
                DO ixpm = ix_l, ix_h
                   wgh_prdct(ixpm,ix_odd,j,wlt_fmly,trnsf_type,idim)= &
                        wgh (ixpm, ix_odd, ix_l, ix_h, 2**(j_lev-1-j), prd(idim), nxyz(idim), xx(0:nxyz(idim),idim))
                END DO
             END DO
             DO ix = trnsf_type, mxyz(idim)*2**(j-1)-prd(idim) - trnsf_type
                ix_even = ix*2**(j_lev-j)
                ix_l = - MIN (ix, n_updt(wlt_fmly))
                ix_h = ix_l + 2*n_updt(wlt_fmly) - 1
                ix_h = MIN (ix_h, mxyz(idim)*2**(j-1)-ix-1)
                ix_l = ix_h - 2*n_updt(wlt_fmly) + 1
                ix_l = (1-prd(idim))*MAX(ix_l,-ix) + prd(idim)*(-n_updt(wlt_fmly))
                ix_h = (1-prd(idim))*MIN(ix_h,mxyz(idim)*2**(j-1)-ix-1) + prd(idim)*(n_updt(wlt_fmly) - 1)
                DO ixpm = ix_l, ix_h
                   wgh_updt(ixpm,ix_even,j,wlt_fmly,trnsf_type,idim) = &
                        wgh (ixpm, ix_even, ix_l, ix_h, 2**(j_lev-1-j), prd(idim), nxyz(idim), xx(0:nxyz(idim),idim))
                END DO
             END DO
          END DO
       END DO
    END DO
    
    DO trnsf_type = 0,n_trnsf_type
       DO wlt_fmly = 0, n_wlt_fmly   
          CALL accel_size_xxx( lvxyz_odd(:,:,wlt_fmly,trnsf_type),lvxyz_even(:,:,wlt_fmly,trnsf_type), &
               nxyz,mxyz,n_prdct,n_updt,prd,trnsf_type,ibc,j_lev,i_c,jd_local,dim )
          CALL accel_indices_xxx( lvxyz_odd(:,:,wlt_fmly,trnsf_type),lvxyz_even(:,:,wlt_fmly,trnsf_type), &
               nlvxyz_odd(:,:,:,wlt_fmly,trnsf_type),nlvxyz_even(:,:,:,wlt_fmly,trnsf_type), &
               nxyz,mxyz,n_prdct(wlt_fmly),n_updt(wlt_fmly),&
               n_assym_prdct(wlt_fmly,trnsf_type),n_assym_updt(wlt_fmly,trnsf_type), &
               n_assym_prdct_bnd(wlt_fmly,trnsf_type),n_assym_updt_bnd(wlt_fmly,trnsf_type), &
               prd,trnsf_type,ibc,j_lev,i_c,jd_local,dim )
       END DO
    END DO


    wlt_fmly = 1        ! 1 - HIGH_ORDER
    trnsf_type = 0
       
    DO JJ=1,j_lev                           ! compute phi of the level JJ
       x_local = 0.0_pr;
       x_local( N-2**(j_lev-JJ) ) = 1.0_pr
       
       IF (i_coef == 1) THEN ! direct transform
          DO j = 1, j_lev-1
             call c_wlt_trns_aux_xxx( N, dim , j, j_in, nxyz, &                                                   !! wlt_xxx.f90 is 1D transform; so, no need to be parallelized. 
                  lvxyz_odd(:,:,wlt_fmly,trnsf_type), nlvxyz_odd(:,:,:,wlt_fmly,trnsf_type), &
                  lvxyz_even(:,:,wlt_fmly,trnsf_type), nlvxyz_even(:,:,:,wlt_fmly,trnsf_type), &
                  n_prdct(wlt_fmly), n_updt(wlt_fmly), wgh_prdct, wgh_updt, wlt_fmly, trnsf_type, &
                  x_local ,jd_local, prd, i_coef )
          END DO
       ELSE                  ! inverse transform
          DO j = JJ, j_out-1
             call c_wlt_trns_aux_xxx( N, 1 , j, j_out, &
                  nxyz,lvxyz_odd(:,:,wlt_fmly,trnsf_type), nlvxyz_odd(:,:,:,wlt_fmly,trnsf_type), &
                  lvxyz_even(:,:,wlt_fmly,trnsf_type), nlvxyz_even(:,:,:,wlt_fmly,trnsf_type), &
                  n_prdct(wlt_fmly), n_updt(wlt_fmly), wgh_prdct, wgh_updt, wlt_fmly, trnsf_type, &
                  x_local ,jd_local, prd, i_coef )
          END DO
       END IF

       ix = 2**(j_lev-JJ)
       PHI( JJ,1:ix ) = x_local( N-ix+1:N )
       PHI( JJ,ix+1:N ) = x_local( 1:N-ix )
    END DO

    DO JJ=2,j_lev                           ! compute psi of the level JJ
       x_local = 0.0_pr;
       x_local( N-2**(j_lev-JJ) ) = 1.0_pr
       
       IF (i_coef == 1) THEN ! direct transform
          DO j = 1, j_lev-1
             call c_wlt_trns_aux_xxx( N, dim , j, j_in, nxyz, &                                                    
                  lvxyz_odd(:,:,wlt_fmly,trnsf_type), nlvxyz_odd(:,:,:,wlt_fmly,trnsf_type), &
                  lvxyz_even(:,:,wlt_fmly,trnsf_type), nlvxyz_even(:,:,:,wlt_fmly,trnsf_type), &
                  n_prdct(wlt_fmly), n_updt(wlt_fmly), wgh_prdct, wgh_updt, wlt_fmly, trnsf_type, &
                  x_local ,jd_local, prd, i_coef )
          END DO
       ELSE                  ! inverse transform
          DO j = 1, j_out-1
             call c_wlt_trns_aux_xxx( N, 1 , j, j_out, &
                  nxyz,lvxyz_odd(:,:,wlt_fmly,trnsf_type), nlvxyz_odd(:,:,:,wlt_fmly,trnsf_type), &
                  lvxyz_even(:,:,wlt_fmly,trnsf_type), nlvxyz_even(:,:,:,wlt_fmly,trnsf_type), &
                  n_prdct(wlt_fmly), n_updt(wlt_fmly), wgh_prdct, wgh_updt, wlt_fmly, trnsf_type, &
                  x_local ,jd_local, prd, i_coef )
          END DO
       END IF

       ix = 2**(j_lev-JJ)-1
       PSI( JJ,1:ix ) = x_local( N-ix:N-1 )
       PSI( JJ,ix+1:N ) = x_local( 0:N-ix-1 )
    END DO
    
    DEALLOCATE ( x_local, wgh_prdct, wgh_updt, lvxyz_odd, lvxyz_even, nlvxyz_odd, nlvxyz_even, i_c, &
         STAT=ios )
    CALL test_alloc (ios, 'deallocating in set_table_aux_1')
    
    ! restore globals
    dim = DIM_
    nxyz(1:dim) = NXUZ_(1:dim)
    mxyz(1:dim) = MXUZ_(1:dim)
    nwlt = NWLT_

    IF (VERB_1.EQ.1) THEN
       OPEN (99,FILE='phi.dat', STATUS='UNKNOWN')
       OPEN (88,FILE='psi.dat', STATUS='UNKNOWN')
       WRITE (99,'("%#",A7,1(1X,A18))') 'i','phi(lev=...)'
       WRITE (88,'("%#",A7,1(1X,A18))') 'i','psi(lev=...)'
       WRITE (99,'(I8,100(1X,E18.8))') 0, Real(PHI( 1:j_lev,N ))
       WRITE (88,'(I8,100(1X,E18.8))') 0, 0.0, Real(PSI( 2:j_lev,N ))
       DO ix=1,N
          WRITE (99,'(I8,100(1X,E18.8))') ix, Real(PHI( 1:j_lev,ix ))
          WRITE (88,'(I8,100(1X,E18.8))') ix, 0.0, Real(PSI( 2:j_lev,ix ))
       END DO
       CLOSE (99)
       CLOSE (88)
       !CALL SYSTEM('xmgr -free -noask -nxy phi.dat&')
       !CALL SYSTEM('xmgr -free -noask -nxy psi.dat&')
!!$       PRINT *,'set_table_aux'; STOP
    END IF

  END SUBROUTINE set_table_aux
  !-----------------------------------------------------------------
  ! tabulate wavelets
  SUBROUTINE set_table
    USE wlt_vars
    INTEGER :: ios, i, j, N
    COMPLEX, DIMENSION (:), ALLOCATABLE :: w, x
    
    N = MAXVAL(nxyz(1:dim))    
    ALLOCATE( w(N), x(N), STAT=ios )
    CALL test_alloc (ios, 'w and x in set_table')
    
    ! compute wavelet functions
    CALL set_table_aux
    
    ! make FFT of wavelet functions
    DO j=2,j_lev
       x = PSI(j,1:N)
       CALL FFT ( x, N, w );
       x = x/N
       PSI(j,1:N) = x
    END DO
    DO j=1,j_lev
       x = PHI(j,1:N)
       CALL FFT ( x, N, w );
       x = x/N
       PHI(j,1:N) = x
    END DO
    
    DEALLOCATE ( w, x, STAT=ios )
    CALL test_alloc (ios, 'deallocating in set_table')
    
    IF (VERB_2.EQ.1) THEN
       OPEN (99,FILE='fft_of_phi.dat', STATUS='UNKNOWN')
       OPEN (88,FILE='fft_of_psi.dat', STATUS='UNKNOWN')
       WRITE (99,'("#",A7,200(1X,A18))') 'i', '|fft(PHI) lev=...|'
       WRITE (88,'("#",A7,200(1X,A18))') 'i', '|fft(PSI) lev=...|'
       DO i=1,N,1
          WRITE (99,'(I8,200(1X,E18.8))') i, ABS(PHI(1:j_lev,i))
          WRITE (88,'(I8,200(1X,E18.8))') i, 0.0, ABS(PSI(2:j_lev,i))
       END DO
       CLOSE (99)
       CLOSE (88)
       !CALL SYSTEM('xmgr -free -noask -nxy fft_of_phi.dat&')
       !CALL SYSTEM('xmgr -free -noask -nxy fft_of_psi.dat&')
!!$       PRINT *,'set_table'; STOP
    END IF

  END SUBROUTINE set_table
  !-----------------------------------------------------------------
  ! clean what should have been cleaned in read_solution
  SUBROUTINE clean_after_read_solution
    USE pde
    USE  wlt_vars
    INTEGER :: ios
    
    IF (ALLOCATED(u_variable_names)) THEN
       DEALLOCATE(u_variable_names, STAT=ios)
       CALL test_alloc (ios, 'u_variable_names')
    END IF
    IF (ASSOCIATED(scl_global)) THEN
       DEALLOCATE(scl_global, STAT=ios)
       CALL test_alloc (ios, 'scl_global')
    END IF
    IF (ALLOCATED(n_var_wlt_fmly)) THEN
       DEALLOCATE(n_var_wlt_fmly, STAT=ios)
       CALL test_alloc (ios, 'n_var_wlt_fmly')
    END IF
    IF (ALLOCATED(xx)) THEN
       DEALLOCATE(xx, STAT=ios)
       CALL test_alloc (ios, 'xx')
    END IF
    ! header2 - internal to read_solution()
    ! indx_DB (1:j_mx,0:2**dim-1,0:3**dim-1,1:j_mx)
    ! u (1:nwlt,n_var)
    ! scl (1:n_var)
    
  END SUBROUTINE clean_after_read_solution
  !-----------------------------------------------------------------
  ! clean what should have been cleaned in read_solution
  ! leave U_VARIABLE_NAMES allocated
  SUBROUTINE clean_after_read_solution_2
    USE pde
    USE  wlt_vars
    INTEGER :: ios
    
    IF (ASSOCIATED(scl_global)) THEN
       DEALLOCATE(scl_global, STAT=ios)
       CALL test_alloc (ios, 'scl_global')
    END IF
    IF (ALLOCATED(n_var_wlt_fmly)) THEN
       DEALLOCATE(n_var_wlt_fmly, STAT=ios)
       CALL test_alloc (ios, 'n_var_wlt_fmly')
    END IF
    IF (ALLOCATED(xx)) THEN
       DEALLOCATE(xx, STAT=ios)
       CALL test_alloc (ios, 'xx')
    END IF
  END SUBROUTINE clean_after_read_solution_2
  !-----------------------------------------------------------------
  ! check internal parameter MAXDIM
  SUBROUTINE check_dimension (dim)
    INTEGER, INTENT(IN) :: dim

    IF (dim.GT.MAXDIM) THEN
       PRINT *, 'Internal error in MODULE FT_AUX, ft_aux.f90'
       PRINT *, 'Problem has dimension:', dim
       PRINT *, 'Please increase MAXDIM parameter and recompile'
       STOP 1
    END IF

  END SUBROUTINE check_dimension
  !-----------------------------------------------------------------
  ! test (de)allocation status
  SUBROUTINE test_alloc(ios,s)
    INTEGER, INTENT(IN) :: ios
    CHARACTER*(*), INTENT(IN), OPTIONAL :: s

    IF (ios.NE.0) THEN
       WRITE (*,'("Error: (de)allocating ",A)') s
       STOP 1
    END IF

  END SUBROUTINE test_alloc
  !-----------------------------------------------------------------
  ! DEBUG
  !-----------------------------------------------------------------
  ! print U and FFT(U) into the files
  ! uses global I_P(:)
  SUBROUTINE print_solution
    USE pde
    USE field
    USE wlt_trns_vars
    USE sizes
    INTEGER :: ie_cur                                     ! current number of variable
    INTEGER :: j, wlt_type, face_type, j_df, kk, ios
    INTEGER :: xyz(1:MAXDIM), iwlt, i
    INTEGER*8 :: ii
    REAL, ALLOCATABLE :: data(:,:)
    
    WRITE (*,*) 'nxyz=', nxyz, 'mxyz=', mxyz, 'nwlt=', nwlt, 'j_lev=',j_lev, &
         'dim=',dim
    ie_cur = 1
    PRINT *,' FFT for variable #', ie_cur
    
    ALLOCATE (data(1:nxyz(1),1:nxyz(2)))
    data  = 0.0

    DO j = 1, j_lev
       DO wlt_type = MIN(j-1,1),2**dim-1
          DO face_type = 0, 3**dim - 1
             DO j_df = j, j_lev
                DO kk = 1, indx_DB(j_df,wlt_type,face_type,j)%length            ! sweep through all wavelets
                   
                   ii = indx_DB(j_df,wlt_type,face_type,j)%p(kk)%ixyz           ! 1D coordinate
                   iwlt = indx_DB(j_df,wlt_type,face_type,j)%p(kk)%i            ! wavelet number
                   xyz(1:dim) = INT(MOD(ii-1,i_p(1:dim))/i_p(0:dim-1)) + 1      ! 3D coordinate
                   !print *, wlt_type, 'xyz=',xyz(1:dim), 'u=',u(iwlt,ie_cur)
                   data(xyz(1),xyz(2)) = u(iwlt,ie_cur)
                END DO
             END DO
          END DO
       END DO
    END DO

    ! print data field
    OPEN (99,FILE='field_out.dat', STATUS='UNKNOWN')
    DO i=1,nxyz(1)
       DO j=1,nxyz(2)
          WRITE (99, *) i, j, data(i,j)
       END DO
    END DO
    CLOSE (99)
    
    ! print FFT of the data field
    CALL fft2D(data,nxyz(1),nxyz(2))
    DEALLOCATE (data)
    PRINT *, 'print_solution'; STOP 0

  END SUBROUTINE print_solution
  !-----------------------------------------------------------------
  SUBROUTINE fft2D (data,n1,n2)
    USE  wlt_vars
    INTEGER, INTENT(IN) :: n1, n2
    REAL,    INTENT(IN) :: data(n1,n2)
    COMPLEX, DIMENSION(:,:), ALLOCATABLE :: cdata
    COMPLEX, DIMENSION(:),   ALLOCATABLE :: WORK, tmp
    INTEGER :: i, j

    ! transform real data into complex aray
    ALLOCATE (cdata(n1,n2))
    cdata = (0.0,0.0)
    cdata = data
    
    ALLOCATE ( WORK(MAX(n1,n2)), tmp(MAX(n1,n2)) )

    ! x pass
    DO i=1,n2
       tmp (1:n1) = cdata(1:n1,i)
       WORK = 0.0
       CALL FFT ( tmp, n1, WORK )
       cdata(1:n1,i) = tmp (1:n1) / n1
    END DO
    ! y pass
    DO i=1,n1
       tmp (1:n2) = cdata(i,1:n2)
       WORK = 0.0
       CALL FFT ( tmp, n2, WORK )
       cdata(i,1:n2) = tmp (1:n2) / n2
    END DO

    ! write data
    OPEN (99,FILE='fft_out.dat', STATUS='UNKNOWN')
    DO i=1,nxyz(1)
       DO j=1,nxyz(2)
          WRITE (99, *) i, j, ABS(cdata(i,j))
       END DO
    END DO
    CLOSE (99)

    DEALLOCATE ( cdata, WORK, tmp )

  END SUBROUTINE fft2D
  !-----------------------------------------------------------------
END MODULE ft_aux
