! cut from wavelet_3d_wrk.f90
! in order to have 1D inverse wavelet transform
MODULE wlt_xxx
  USE precision
  USE wlt_vars
  !USE wlt_trns_util_mod
  USE wlt_trns_vars
  USE io_3D_vars
  USE sizes
  !USE additional_nodes
  IMPLICIT NONE
  PUBLIC :: c_wlt_trns_aux_xxx, accel_indices_xxx, accel_size_xxx
CONTAINS

  ! ----------------------------------------------------------------------------------------------------------------------------------

  SUBROUTINE accel_size_xxx(lvxyz_odd,lvxyz_even,nxyz,mxyz,n_prdct,n_updt,prd,trnsf_type,ibc,j_lev,i_c,jd,dim)
    USE precision
    IMPLICIT NONE
    INTEGER, INTENT (IN) ::  trnsf_type,j_lev
    INTEGER, DIMENSION(0:n_wlt_fmly), INTENT (IN) ::  n_prdct,n_updt
    INTEGER , DIMENSION(3), INTENT (IN) ::  nxyz,mxyz,prd
    INTEGER , DIMENSION(6), INTENT (IN) ::  ibc
    INTEGER, DIMENSION (0:j_lev,1:3), INTENT (INOUT) :: lvxyz_odd,lvxyz_even
    INTEGER, INTENT (IN) :: jd, dim
    LOGICAL, DIMENSION (0:nxyz(1)),  INTENT (IN) :: i_c
    INTEGER :: j
    INTEGER :: ix,ix_even,ix_odd
    INTEGER :: iy,iy_even,iy_odd
    INTEGER :: iz,iz_even,iz_odd
    INTEGER , DIMENSION(6) ::  ibch

    ibch = 0
    IF(trnsf_type == 1) ibch = ibc
    !
    !----  Setting the points for wavelet transform at different levels
    !
    lvxyz_odd(0,1)  = 0
    lvxyz_even(0,1) = 0
    lvxyz_odd(0,2)  = 0
    lvxyz_even(0,2) = 0
    lvxyz_odd(0,3)  = 0
    lvxyz_even(0,3) = 0
    DO j = 1,j_lev-1
       lvxyz_odd(j,1)  = lvxyz_odd(j-1,1)  
       lvxyz_even(j,1) = lvxyz_even(j-1,1)
       lvxyz_odd(j,2)  = lvxyz_odd(j-1,2)
       lvxyz_even(j,2) = lvxyz_even(j-1,2)
       lvxyz_odd(j,3)  = lvxyz_odd(j-1,3)
       lvxyz_even(j,3) = lvxyz_even(j-1,3)

       !
       !*********** transform in x-direction *********************
       !        
       DO ix = 1, mxyz(1)*2**(j-1)
          ix_odd = (2*ix-1)*2**(j_lev-1-j)
          IF (i_c(ix_odd)) THEN
             lvxyz_odd(j,1)=lvxyz_odd(j,1)+1
          END IF
       END DO
       DO ix = ibch(1), mxyz(1)*2**(j-1)-prd(1) - ibch(2)
          ix_even = ix*2**(j_lev-j)
          IF (i_c(ix_even)) THEN
             lvxyz_even(j,1)=lvxyz_even(j,1)+1
          END IF
       END DO

    END DO

  END SUBROUTINE accel_size_xxx

  ! ----------------------------------------------------------------------------------------------------------------------------------

  SUBROUTINE accel_indices_xxx(lvxyz_odd,lvxyz_even, & 
       nlvxyz_odd,nlvxyz_even, & 
       nxyz,mxyz,n_prdct,n_updt,&
       n_assym_prdct,n_assym_updt,n_assym_prdct_bnd,n_assym_updt_bnd,& !changed
       prd,trnsf_type,ibc,j_lev,i_c,jd,dim) 
    USE precision
    IMPLICIT NONE
    INTEGER, INTENT (IN) ::  n_prdct,n_updt,trnsf_type,j_lev
    INTEGER, INTENT (IN) ::  n_assym_prdct,n_assym_updt,n_assym_prdct_bnd,n_assym_updt_bnd
    INTEGER , DIMENSION(3), INTENT (IN) ::  nxyz,mxyz,prd
    INTEGER , DIMENSION(6), INTENT (IN) ::  ibc
    INTEGER, DIMENSION (0:j_lev,1:3), INTENT (INOUT) :: lvxyz_odd,lvxyz_even
    INTEGER, DIMENSION (1:6,1:3,*), INTENT (INOUT) :: nlvxyz_odd,nlvxyz_even
    INTEGER, INTENT (IN) :: jd,dim
    LOGICAL, DIMENSION (0:nxyz(1)),  INTENT (IN) :: i_c
    INTEGER :: i,j
    INTEGER :: ix,ix_l,ix_h,ix_even,ix_odd
    INTEGER :: iy,iy_l,iy_h,iy_even,iy_odd
    INTEGER :: iz,iz_l,iz_h,iz_even,iz_odd

    INTEGER , DIMENSION(6) ::  ibch
    INTEGER :: idim

    ibch = 0
    IF(trnsf_type == 1) ibch = ibc
    !
    !----  Setting the points for wavelet transform at different levels
    !
    lvxyz_odd(0,1)  = 0
    lvxyz_even(0,1) = 0
    lvxyz_odd(0,2)  = 0
    lvxyz_even(0,2) = 0
    lvxyz_odd(0,3)  = 0
    lvxyz_even(0,3) = 0
    DO j = 1,j_lev-1
       lvxyz_odd(j,1)  = lvxyz_odd(j-1,1)  
       lvxyz_even(j,1) = lvxyz_even(j-1,1)
       lvxyz_odd(j,2)  = lvxyz_odd(j-1,2)
       lvxyz_even(j,2) = lvxyz_even(j-1,2)
       lvxyz_odd(j,3)  = lvxyz_odd(j-1,3)
       lvxyz_even(j,3) = lvxyz_even(j-1,3)


       !
       !*********** transform in x-direction *********************
       ! 


       idim = 1
       DO ix = 1, mxyz(1)*2**(j-1)
          ! ix is consecutive from 1 to max pts on this level
          ix_odd = (2*ix-1)*2**(j_lev-1-j) 
          ! ix_odd is the actual index of the point in the 3D field (i_c)
          IF (i_c(ix_odd)) THEN
             lvxyz_odd(j,idim)=lvxyz_odd(j,idim)+1
             i=lvxyz_odd(j,idim)
             ix_l   = -MIN(ix-ibch(1),n_prdct)
             ix_h   = ix_l+2*n_prdct-1
             ix_h   = MIN(ix_h,mxyz(1)*2**(j-1)-ix-ibch(2))
             ix_l   = ix_h-2*n_prdct+1
             ix_l = (1-prd(1))*MAX(ix_l,-ix+ibch(1)) + prd(1)*(-n_prdct)
             ix_h = (1-prd(1))*MIN(ix_h,mxyz(1)*2**(j-1)-ix-ibch(2)) + prd(1)*(n_prdct - 1)

             nlvxyz_odd(1,idim,i)=ix
             nlvxyz_odd(2,idim,i)=0
             nlvxyz_odd(3,idim,i)=0
             nlvxyz_odd(4,idim,i)=ix_odd
             nlvxyz_odd(5,idim,i)=ix_l
             nlvxyz_odd(6,idim,i)=ix_h
             !write(*,'(''nlvxyz_odd '', 7(I8, 1X ) )') idim, nlvxyz_odd(:,idim,i)
          END IF
       END DO
       DO ix = ibch(1), mxyz(1)*2**(j-1)-prd(1) - ibch(2)
          ix_even = ix*2**(j_lev-j)
          IF (i_c(ix_even)) THEN
             lvxyz_even(j,idim)=lvxyz_even(j,idim)+1
             i= lvxyz_even(j,idim)
             ix_l = - MIN (ix, n_updt)
             ix_h = ix_l + 2*n_updt - 1
             ix_h = MIN (ix_h, mxyz(1)*2**(j-1)-ix-1)
             ix_l = ix_h - 2*n_updt + 1
             ix_l = (1-prd(1))*MAX(ix_l,-ix) + prd(1)*(-n_updt)
             ix_h = (1-prd(1))*MIN(ix_h,mxyz(1)*2**(j-1)-ix-1) + prd(1)*(n_updt - 1)

             nlvxyz_even(1,idim,i)=ix
             nlvxyz_even(2,idim,i)=0
             nlvxyz_even(3,idim,i)=0
             nlvxyz_even(4,idim,i)=ix_even
             nlvxyz_even(5,idim,i)=ix_l
             nlvxyz_even(6,idim,i)=ix_h
             !write(*,'(''nlvxyz_even '', 7(I8, 1X ) )') idim, nlvxyz_odd(:,idim,i)
          END IF
       END DO



    END DO

  END SUBROUTINE accel_indices_xxx

  ! ----------------------------------------------------------------------------------------------------------------------------------

  SUBROUTINE c_wlt_trns_aux_xxx(max_nxyz, dim , j, j_lev, nxyz,lvxyz_odd, nlvxyz_odd, lvxyz_even, &
       nlvxyz_even, nn_prdct, nn_updt,  wgh_prdct, wgh_updt, wlt_fmly, trnsf_type,  wrk ,jd, prd,  i_coef )
    USE precision
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: dim, j , j_lev, wlt_fmly, trnsf_type,i_coef , jd,max_nxyz
    INTEGER, DIMENSION (0:j_lev,1:3), INTENT (IN) :: lvxyz_odd,lvxyz_even
    INTEGER, DIMENSION (1:6,1:3,*), INTENT (IN) :: nlvxyz_odd,nlvxyz_even
    INTEGER, DIMENSION(3) , INTENT (IN) ::  nxyz
    INTEGER, DIMENSION(3) , INTENT (IN) ::  prd
    INTEGER, DIMENSION(0:n_wlt_fmly), INTENT (IN) :: nn_prdct, nn_updt
    REAL (pr), DIMENSION ( -2*MAXVAL(nn_updt):2*MAXVAL(nn_updt)  ,0:max_nxyz,j_lev-1,0:n_wlt_fmly,0:n_trnsf_type,1:3), INTENT (IN) :: wgh_updt
    REAL (pr), DIMENSION ( -2*MAXVAL(nn_prdct):2*MAXVAL(nn_prdct),0:max_nxyz,j_lev-1,0:n_wlt_fmly,0:n_trnsf_type,1:3), INTENT (IN) :: wgh_prdct
    REAL(pr) ,DIMENSION(0:nxyz(1)), INTENT (INOUT):: wrk
    INTEGER :: ixpm,ix_l,ix_h
    INTEGER :: ixyz(3),ixyzp(3), ixyz2
    INTEGER :: idim, i
    REAL (pr) :: c_predict

    ! Do real to wlt transform
    IF( i_coef == 1 ) THEN
       !
       !*********** transform in idim -direction *********************
       !       
       DO idim = 1,dim
          !!CALL CPU_TIME (t0)
          ! do predict stage for odd points
          ! we loop through the range (i) of the index array nlvxyz_odd(1,1:3,i)  that maps to the
          ! active points at level j in the work array wrk()
          DO i=lvxyz_odd(j-1,idim)+1,lvxyz_odd(j,idim) 
             ixyz(1)=nlvxyz_odd(1,idim,i) ! get x index in working array of point we will do predict stage
             ixyz(2)=0
             ixyz(3)=0
             ixyz2 = ixyz(idim)  !save the value from nlvxyz_odd(idim,idim,i) to use below
             ixyz(idim)=nlvxyz_odd(4,idim,i)!ix_odd the point in wrk ( in the curent direction idim)

             ! that we will do predict for
             ix_l=nlvxyz_odd(5,idim,i) 
             ix_h=nlvxyz_odd(6,idim,i)
             c_predict = 0.0_pr
             ixyzp = ixyz !save the original value of ixyz(:)

             DO ixpm = ix_l, ix_h
                ixyzp(idim) = (ixyz2+ixpm)*2**(j_lev-j)
                ixyzp(idim) = (1-prd(idim))*ixyzp(idim)+prd(idim)*MOD(ixyzp(idim)+9*nxyz(idim),nxyz(idim))
                c_predict = c_predict + wgh_prdct(ixpm,ixyz(idim),j,wlt_fmly,trnsf_type,idim) * wrk(ixyzp(1))
             END DO
             wrk(ixyz(1)) = 0.5_pr*(wrk(ixyz(1)) - c_predict)
          END DO

          !do update for even points
          DO i=lvxyz_even(j-1,idim)+1,lvxyz_even(j,idim)
             ixyz(1)=nlvxyz_even(1,idim,i)
             ixyz(2)=0
             ixyz(3)=0
             ixyz2 = ixyz(idim)  !save the value from nlvxyz_even(idim,idim,i) to use below
             ixyz(idim)=nlvxyz_even(4,idim,i)!ix_even the point in wrk ( in the curent direction idim)
             ! that we will do update for
             ix_l=nlvxyz_even(5,idim,i)
             ix_h=nlvxyz_even(6,idim,i)
             c_predict = 0.0_pr
             ixyzp = ixyz !save the original value of ixyz(:)
             DO ixpm = ix_l, ix_h
                ixyzp(idim) = (2*(ixyz2+ixpm)+1)*2**(j_lev-1-j)
                ixyzp(idim) = (1-prd(idim))*ixyzp(idim)+prd(idim)*MOD(ixyzp(idim)+9*nxyz(idim),nxyz(idim))
                c_predict = c_predict + wgh_updt(ixpm,ixyz(idim),j,wlt_fmly,trnsf_type,idim) * wrk(ixyzp(1))
             END DO
             wrk(ixyz(1)) = wrk(ixyz(1)) + c_predict
          END DO

       END DO

    ELSE IF (i_coef == -1) THEN ! Do wlt to real transform

       DO idim = dim,1,-1
          !do update for even points
          DO i=lvxyz_even(j-1,idim)+1,lvxyz_even(j,idim)
             ixyz(1)=nlvxyz_even(1,idim,i)
             ixyz(2)=0
             ixyz(3)=0
             ixyz2 = ixyz(idim)  !save the value from nlvxyz_even(idim,idim,i) to use below
             ixyz(idim)=nlvxyz_even(4,idim,i)!ix_even the point in wrk ( in the curent direction idim)

             ! that we will do update for
             ix_l=nlvxyz_even(5,idim,i)
             ix_h=nlvxyz_even(6,idim,i)
             c_predict = 0.0_pr
             ixyzp = ixyz !save the original value of ixyz(:)
             DO ixpm = ix_l, ix_h
                ixyzp(idim) = (2*(ixyz2+ixpm)+1)*2**(j_lev-1-j)
                ixyzp(idim) = (1-prd(idim))*ixyzp(idim)+prd(idim)*MOD(ixyzp(idim)+9*nxyz(idim),nxyz(idim))
                c_predict = c_predict + wgh_updt(ixpm,ixyz(idim),j,wlt_fmly,trnsf_type,idim) * wrk(ixyzp(1))
             END DO
             wrk(ixyz(1)) = wrk(ixyz(1)) - c_predict
          END DO
          ! do predict stage for odd points
          ! we loop through the range (i) of the index array nlvxyz_odd(1,1:3,i)  that maps to the
          ! active points at level j in the work array wrk()
          DO i=lvxyz_odd(j-1,idim)+1,lvxyz_odd(j,idim) 
             ixyz(1)=nlvxyz_odd(1,idim,i) ! get x index in working array of point we will do predict stage
             ixyz(2)=0
             ixyz(3)=0
             ixyz2 = ixyz(idim)  !save the value from nlvxyz_odd(idim,idim,i) to use below
             ixyz(idim)=nlvxyz_odd(4,idim,i)!ix_odd the point in wrk ( in the curent direction idim)
             ! that we will do predict for
             ix_l=nlvxyz_odd(5,idim,i) 
             ix_h=nlvxyz_odd(6,idim,i)
             c_predict = 0.0_pr
             ixyzp = ixyz !save the original value of ixyz(:)
             DO ixpm = ix_l, ix_h
                ixyzp(idim) = (ixyz2+ixpm)*2**(j_lev-j)
                ixyzp(idim) = (1-prd(idim))*ixyzp(idim)+prd(idim)*MOD(ixyzp(idim)+9*nxyz(idim),nxyz(idim))
                c_predict = c_predict + wgh_prdct(ixpm,ixyz(idim),j,wlt_fmly,trnsf_type,idim) * wrk(ixyzp(1))
             END DO
             wrk(ixyz(1)) = 2.0_pr*wrk(ixyz(1)) + c_predict
          END DO
       END DO
    ELSE
       print *,'ERROR, c_wlt_trns_aux() called with i_coeff = ', i_coef
       print *,'ERROR  Correct values are 1 -fwd trns or -1 inv trns. Exiting ....'
       stop
    ENDIF

  END SUBROUTINE c_wlt_trns_aux_xxx

  !----------------------------------------------------------------------------------------------------------------------------------

END MODULE wlt_xxx
