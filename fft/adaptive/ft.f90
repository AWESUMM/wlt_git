! Test program for MODULE ft_aux
! Fourier spectrum in wavelet space
! by Alexei Vezolainen
! June 2006
PROGRAM ft
  USE ft_aux
  USE wlt_vars
  IMPLICIT NONE
  CHARACTER*(*), PARAMETER :: filename = &           ! name of the result file
       '../../TestCases/SmallVortexArrayTest1/results/ccase_small_vortex_array_db_wrk.0006.res.3D'
       !'/home/alex/svn/tmp/ft/TestCases/fft/csmall-wlt'


  INTEGER, PARAMETER :: ie_size = 1
  INTEGER :: ie(1:ie_size)                           ! variables vector
  INTEGER :: k_size, i, j, ii
  INTEGER, DIMENSION(:,:), ALLOCATABLE :: k          ! K-vector
  COMPLEX, DIMENSION(:,:), ALLOCATABLE :: res        ! FT result
  INTEGER, DIMENSION(:), ALLOCATABLE :: i_p, xyz     ! 1D to 3D coordinate transform

  ie = 1       ! for that variable
  
  ! read result file and initialize the module
  CALL ft_read_var ( filename )

  ! allocate k-vectors
  ALLOCATE ( xyz(1:dim) )
  ALLOCATE ( i_p(0:dim) )
  i_p(0) = 1
  DO i=1,dim
     i_p(i) = i_p(i-1)*(nxyz(i))                           ! xyz \in [0..N-1]
  END DO
  k_size = PRODUCT ( nxyz(1:dim) )
  ALLOCATE ( k(1:dim,1:k_size) )
  ALLOCATE ( res(ie_size,1:k_size) )
  DO ii = 1, k_size
     xyz(1:dim) = INT(MOD(ii-1,i_p(1:dim))/i_p(0:dim-1))
     k( 1:dim,ii ) = xyz( 1:dim ) + 1                      ! k   \in [1..N]
  END DO

  ! compute FT for the given k-vectors
  CALL ft_compute ( res, k, k_size, ie, ie_size )
  
  ! print the results
  OPEN (99,FILE='k.dat',STATUS='UNKNOWN')
  DO ii = 1, k_size
     WRITE (99, *) k(1:dim,ii), ABS(res (ie,ii))
  END DO
  CLOSE (99)

  ! clean the module
  CALL ft_clean_var

  ! clean local variables
  DEALLOCATE ( xyz, i_p, k, res )

END PROGRAM ft
