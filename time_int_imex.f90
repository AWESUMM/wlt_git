MODULE imex_mod
  USE precision
  USE share_kry
  USE sizes
  USE elliptic_mod
  USE pde
  USE share_consts
  USE user_case

  IMPLICIT NONE
  REAL (pr) :: IMEXcoef

CONTAINS

!--************************************************************************ 
!--Begin IMEX time step subroutines
!--************************************************************************
SUBROUTINE time_adv_imex (u, p,  nwlt_in, ddt, t_local)
  USE debug_vars
  USE parallel
  USE variable_mapping
  IMPLICIT NONE
  
  INTEGER, INTENT (IN) :: nwlt_in
  REAL (pr) :: ddt, t_local
  REAL (pr), DIMENSION (nwlt_in*n_integrated), INTENT(INOUT) :: u  !1D flat version of integrated variables without BC
  REAL (pr), DIMENSION (nwlt_in), INTENT (INOUT) :: p   

  INTEGER :: j, j_p, m, nttl, i
  REAL (pr), DIMENSION (nwlt_in*n_integrated, exsteps) ::   rhs_ex !need s of them
  REAL (pr), DIMENSION (nwlt_in*n_integrated, imsteps+1) :: rhs_im !need s+1 of them
  REAL (pr), DIMENSION (nwlt_in*n_integrated) :: curdu, currhs
  INTEGER, PARAMETER :: meth = 1
  INTEGER :: alloc_stat
  INTEGER, DIMENSION (n_integrated) :: clip
  
  clip = 0

  told = t_local
  nttl = nwlt_in*n_integrated
  n = nttl
  ne = n_integrated
  ng = nttl/ne
  
  IF (ALLOCATED(u_prev_timestep)) DEALLOCATE(u_prev_timestep)
  ALLOCATE (u_prev_timestep(1:nttl),STAT=alloc_stat)
  CALL test_alloc( alloc_stat, 'u_prev_timestep',nttl )
  IF (ALLOCATED(u_for_BCs)) DEALLOCATE(u_for_BCs)
  ALLOCATE (u_for_BCs(1:nttl),STAT=alloc_stat)
  CALL test_alloc( alloc_stat, 'u_for_BCs',nttl )

  u_prev_timestep = u
  curdu = 0.0_pr
  u_for_BCs = u_prev_timestep
  CALL project_BC (u_prev_timestep, meth,.FALSE.)
  DO i=1,imsteps
     IMEXcoef = aim(i,i)
     !-----------------------------------------------------------------------------------------------------------------------------------
     IMEXswitch = -1 !Explicit terms only
     rhs_ex(:,i) = IMEX_rhs(u_prev_timestep+curdu,p)  !f(u_i)
    !-----------------------------------------------------------------------------------------------------------------------------------
     IMEXswitch = 1  !Implicit terms only
     rhs_im(:,i) = IMEX_rhs(u_prev_timestep+curdu,p)  !g(u_i)
     currhs(:) = IMEXcoef*rhs_im(:,1) + aex(i,1)*rhs_ex(:,1) 
     DO j=2,i
        currhs(:) = currhs(:) + aim(i,j-1)*rhs_im(:,j) + aex(i,j)*rhs_ex(:,j) 
     END DO
     IF(MINVAL(ABS(prd)) == 0) THEN
       CALL user_algebraic_BC_rhs (currhs, ne, ng, j_lev)
       curdu = 0.0_pr
       CALL user_algebraic_BC (curdu, u_prev_timestep, nwlt_in, ne, j_lev, meth) 
       currhs = currhs - curdu 
     END IF
     u_for_BCs = u_prev_timestep + curdu
     curdu = ddt*currhs
     IF(ABS(IMEXcoef) > 0.0_pr) CALL BiCGSTAB (j_lev, nwlt_in, n_integrated, 1000, meth, clip, tol1, curdu, currhs, Lcn, Lcn_diag, SCL=scl_global(1:n_integrated))
     u_for_BCs = u_prev_timestep + curdu
     CALL project_BC (curdu, meth,.TRUE.)
  END DO 
  IMEXswitch = 1  !Implicit terms only
  rhs_im(:,imsteps+1) = IMEX_rhs(u_prev_timestep+curdu,p)  !g(u_s)
 !-----------------------------------------------------------------------------------------------------------------------------------
  IMEXswitch = -1 !Explicit terms only
  IF (exsteps > imsteps) rhs_ex(:,imsteps+1) = IMEX_rhs(u_prev_timestep+curdu,p)  !f(u_s)
 !-----------------------------------------------------------------------------------------------------------------------------------
  IMEXswitch = 0 
  u = u_prev_timestep
  DO i=1,imsteps
     u(:) = u(:) + ddt*(bim(i)*rhs_im(:,i+1) + bex(i)*rhs_ex(:,i))
  END DO
  IF (exsteps > imsteps) u(:) = u(:) + ddt*bex(imsteps+1)*rhs_ex(:,imsteps+1)
  u_for_BCs = u
  CALL project_BC (u, meth,.FALSE.)
 !-----------------------------------------------------------------------------------------------------------------------------------
  t_local = t_local + ddt
  CALL user_project (u, p, nwlt_in, meth)
END SUBROUTINE time_adv_imex

FUNCTION Lcn (jlev, dui, nlocal, ne_local, meth)
  USE precision
  USE sizes
  USE pde
  USE share_kry
  IMPLICIT NONE
  INTEGER, INTENT (IN) :: jlev, nlocal, ne_local, meth
  INTEGER :: i, ii, ie, shift
  REAL (pr), DIMENSION (nlocal*ne_local), INTENT(INOUT) :: dui
  REAL (pr), DIMENSION (nlocal*ne_local) :: Lcn
  !REAL (pr), DIMENSION (nlocal*ne_local) :: Jac
  Lcn = user_Drhs(dui, u_prev_timestep, meth)
  DO ie = 1, ne_local
     shift = nlocal*(ie-1)
     Lcn(shift+1:shift+nlocal) = dui(shift+1:shift+nlocal)/dt - IMEXcoef*Lcn(shift+1:shift+nlocal)
  END DO
  IF(MINVAL(ABS(prd)) == 0) CALL user_algebraic_BC (Lcn, dui, nlocal, ne_local, jlev, meth)
END FUNCTION Lcn

FUNCTION Lcn_diag (jlev, nlocal, ne_local, meth)
  USE precision
  USE sizes
  USE pde
  IMPLICIT NONE
  INTEGER, INTENT (IN) :: jlev, nlocal, ne_local, meth
  INTEGER :: ie, shift
  REAL (pr), DIMENSION (nlocal*ne_local) :: Lcn_diag
  !REAL (pr), DIMENSION (nlocal*ne_local) :: Jac_diag 
  Lcn_diag = user_Drhs_diag (meth)
  DO ie = 1, ne_local
     shift = nlocal*(ie-1)
     Lcn_diag(shift+1:shift+nlocal) = 1.0_pr/dt - IMEXcoef*Lcn_diag(shift+1:shift+nlocal)
  END DO
  IF(MINVAL(ABS(prd)) == 0) CALL user_algebraic_BC_diag (Lcn_diag, nlocal, ne_local, jlev, meth)
END FUNCTION Lcn_diag

FUNCTION IMEX_rhs (Uvec,scalar)
  IMPLICIT NONE
  REAL (pr), DIMENSION (n), INTENT(IN) :: Uvec 
  REAL (pr), DIMENSION (ng), INTENT(IN) :: scalar
  REAL (pr), DIMENSION (n) :: IMEX_rhs
  IMEX_rhs = user_rhs (Uvec,scalar)
END FUNCTION IMEX_rhs

SUBROUTINE project_BC (Uvec, meth, homogeneous)
  IMPLICIT NONE
  REAL (pr), DIMENSION (n), INTENT(INOUT) :: Uvec
  INTEGER, INTENT(IN) :: meth
  REAL (pr), DIMENSION (n) :: rhs, RK_diag
  INTEGER :: ie, shift
  LOGICAL, INTENT(IN) :: homogeneous

  rhs = 0.0_pr  
  IF(.NOT.homogeneous) CALL user_algebraic_BC_rhs (rhs, ne, ng, j_lev)
  RK_diag = 0.0_pr
  CALL user_algebraic_BC (RK_diag, Uvec, ng, ne, j_lev, meth)
  rhs = rhs - RK_diag !f-Lu

  DO ie = 1, ne
     shift = nwlt*(ie-1)
     RK_diag(shift+nwlt_intrnl+1:shift+nwlt) = 1.0_pr
  END DO
  CALL user_algebraic_BC_diag (RK_diag, nwlt, ne, j_lev, meth)
  DO ie = 1, ne
     shift = nwlt*(ie-1)
     Uvec(shift+nwlt_intrnl+1:shift+nwlt)  = Uvec(shift+nwlt_intrnl+1:shift+nwlt) + rhs(shift+nwlt_intrnl+1:shift+nwlt)/RK_diag(shift+nwlt_intrnl+1:shift+nwlt)  
     !u_new = u_old + (f-Lu)/diag ... assumes that diagonal term is correct and there is no coupling
  END DO
END SUBROUTINE project_BC

!--********************************
!--End RK time step subroutines
!--********************************

END MODULE imex_mod
