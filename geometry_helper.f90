MODULE geometry_helper
  USE precision
  IMPLICIT NONE

  PUBLIC :: &
  bnd_coords_input, &      ! Input numerical (discretized) function, which should be single valued. Otherwise Lagrange interpolation cannot be used.
  normal_dir_bnd_coords, & ! Calculate local unit normal vector of the input numerical (discretized) function, which should be well refined (i.e. small grid spacing) since low order piecewise polynomials are used.
  interpolate_bnd_coords   ! Lagrange interpolation with degree 2 polynomial. Input numerical (discretized) function should be well refined (i.e. small grid spacing) since low order interpolation is used.

CONTAINS

  SUBROUTINE bnd_coords_input (R1, R2, n, array, filename)
    USE parallel
    USE error_handling
    IMPLICIT NONE

    REAL (pr), ALLOCATABLE, DIMENSION(:,:) :: array
    CHARACTER (LEN=*), INTENT(IN)  :: filename    
    INTEGER :: n, io, ii
    REAL (pr) :: R1, R2


    n = 0 
    io = 0
    ii = 0
    R1 = 0.0_pr
    R2 = 0.0_pr
    IF (par_rank .EQ. 0) THEN 
       OPEN(531, FILE=TRIM(filename)//'.dat', STATUS='OLD', FORM='FORMATTED', READONLY, POSITION='REWIND', IOSTAT=io)
       IF (io /= 0) THEN
          print *, 'io = ', io
          CALL error( 'Error occurs when opening boundary coords/profiles file(s)' ) 
       END IF
       READ(531,'(I8)') n
       print *, 'Reading coords/profiles for boundaries, num of points read from coord data file is: ', n
    END IF
    CALL parallel_global_sum( INTEGER = n)
    ALLOCATE(array(n,2))
    array = 0.0_pr
    IF (par_rank .EQ. 0) THEN 
       DO WHILE(io == 0)
         ii = ii+1
         READ(531,*,IOSTAT=io) array(ii,1:2)
       END DO
       IF ((ii-1)/=n) CALL error( 'n_coords is not equal to actual number of coord points!' ) 
       R1 = MINVAL( array(1:n,1) )
       R2 = MAXVAL( array(1:n,1) )
       CLOSE(531, STATUS='KEEP')
    END IF
    CALL parallel_vector_sum( REAL = array(1:n,1), LENGTH = n )
    CALL parallel_vector_sum( REAL = array(1:n,2), LENGTH = n )
    CALL parallel_global_sum(REAL=R1)
    CALL parallel_global_sum(REAL=R2)

  END SUBROUTINE bnd_coords_input

  FUNCTION normal_dir_bnd_coords (i, n, array, btype)
    USE wlt_vars
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: i, n
    REAL (pr), DIMENSION (n,2), INTENT(IN) :: array
    INTEGER, INTENT(IN) :: btype
    REAL (pr), DIMENSION (0:(n+1),2) :: array_
    REAL (pr), DIMENSION (2) :: normal_dir_bnd_coords
    REAL (pr) :: deriv
    REAL (pr) :: x1, x2, x3, y1, y2, y3

    IF(prd(1) == 1) THEN !periodicity in x-dir
       array_(0,:) = array(n-1,:)
       array_(n+1,:) = array(2,:)
       array_(1:n,:) = array(1:n,:)
       x1 = array_(i-1,1)
       x2 = array_(i,1)
       x3 = array_(i+1,1)
       y1 = array_(i-1,2)
       y2 = array_(i,2)
       y3 = array_(i+1,2)
       deriv = (2.0_pr*x2-(x2+x3))/(x1-x2)/(x1-x3)*y1 + (2.0_pr*x2-(x1+x3))/(x2-x1)/(x2-x3)*y2 + (2.0_pr*x2-(x1+x2))/(x3-x1)/(x3-x2)*y3
    ELSE
       IF (i == 1) THEN
          x1 = array(i,1)
          x2 = array(i+1,1)
          x3 = array(i+2,1)
          y1 = array(i,2)
          y2 = array(i+1,2)
          y3 = array(i+2,2)
          deriv = (2.0_pr*x1-(x2+x3))/(x1-x2)/(x1-x3)*y1 + (2.0_pr*x1-(x1+x3))/(x2-x1)/(x2-x3)*y2 + (2.0_pr*x1-(x1+x2))/(x3-x1)/(x3-x2)*y3
       ELSE IF (i == n) THEN
          x1 = array(i-2,1)
          x2 = array(i-1,1)
          x3 = array(i,1)
          y1 = array(i-2,2)
          y2 = array(i-1,2)
          y3 = array(i,2)
          deriv = (2.0_pr*x3-(x2+x3))/(x1-x2)/(x1-x3)*y1 + (2.0_pr*x3-(x1+x3))/(x2-x1)/(x2-x3)*y2 + (2.0_pr*x3-(x1+x2))/(x3-x1)/(x3-x2)*y3
       ELSE 
          x1 = array(i-1,1)
          x2 = array(i,1)
          x3 = array(i+1,1)
          y1 = array(i-1,2)
          y2 = array(i,2)
          y3 = array(i+1,2)
          deriv = (2.0_pr*x2-(x2+x3))/(x1-x2)/(x1-x3)*y1 + (2.0_pr*x2-(x1+x3))/(x2-x1)/(x2-x3)*y2 + (2.0_pr*x2-(x1+x2))/(x3-x1)/(x3-x2)*y3
       END IF
    END IF

        
    IF (btype == 0) THEN !inward unit normal vector to the bottom boundary
      normal_dir_bnd_coords(1) = - 1.0_pr/sqrt(1.0_pr+deriv**2)*deriv
      normal_dir_bnd_coords(2) = 1.0_pr/sqrt(1.0_pr+deriv**2)
    ELSE IF (btype == 1) THEN !inward unit normal vector to the top boundary
      normal_dir_bnd_coords(1) = 1.0_pr/sqrt(1.0_pr+deriv**2)*deriv
      normal_dir_bnd_coords(2) = - 1.0_pr/sqrt(1.0_pr+deriv**2)
    END IF
    
  END FUNCTION normal_dir_bnd_coords

  FUNCTION interpolate_bnd_coords (i, n, array, x_coord)
    USE wlt_vars
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: i, n
    REAL (pr), DIMENSION (n,2), INTENT(IN) :: array
    REAL (pr), INTENT(IN) :: x_coord
    REAL (pr), DIMENSION (0:(n+1),2) :: array_
    REAL (pr) :: interpolate_bnd_coords
    REAL (pr) :: x1, x2, x3, y1, y2, y3

    IF(prd(1) == 1) THEN !periodicity in x-dir
       array_(0,:) = array(n-1,:)
       array_(n+1,:) = array(2,:)
       array_(1:n,:) = array(1:n,:)
       x1 = array_(i-1,1)
       x2 = array_(i,1)
       x3 = array_(i+1,1)
       y1 = array_(i-1,2)
       y2 = array_(i,2)
       y3 = array_(i+1,2)
    ELSE
       IF (i == 1) THEN
          x1 = array(i,1)
          x2 = array(i+1,1)
          x3 = array(i+2,1)
          y1 = array(i,2)
          y2 = array(i+1,2)
          y3 = array(i+2,2)
       ELSE IF (i == n) THEN
          x1 = array(i-2,1)
          x2 = array(i-1,1)
          x3 = array(i,1)
          y1 = array(i-2,2)
          y2 = array(i-1,2)
          y3 = array(i,2)
       ELSE 
          x1 = array(i-1,1)
          x2 = array(i,1)
          x3 = array(i+1,1)
          y1 = array(i-1,2)
          y2 = array(i,2)
          y3 = array(i+1,2)
       END IF
    END IF
    
    !Lagrange interpolation with degree 2 polynomial (2nd order)
    interpolate_bnd_coords = (x_coord-x2)*(x_coord-x3)/(x1-x2)/(x1-x3)*y1 + (x_coord-x1)*(x_coord-x3)/(x2-x1)/(x2-x3)*y2 + (x_coord-x1)*(x_coord-x2)/(x3-x1)/(x3-x2)*y3
  END FUNCTION interpolate_bnd_coords

END MODULE geometry_helper

