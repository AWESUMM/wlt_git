!************** Variables defining wavelet transfrom and derivative calculations
! n_prdct - order for predict stage
! n_updt - order of update stage 
! n_diff - order of derivative operator
! j_mx - maxe wavelet level
! j_mn -min wavelet level
! j_mn_init - min wavelet level during reading any initial data and intial grid adaptation
! j_lev - maximum level of resolution 
! J_full - level below which all grid points are present (not uses defined), for faster derivative calculation              
! dim - dimensionality of the problem, 2 or 3
! nbhd - parameter for finding nearest points, used for derivative calculations
! n_df - the code has n_df+1 families of derivateives (backward_biased, forward_biased, central) * (low and high order) 
!        low order is used for V-cycle in elliptic solver
! mxyz(3) - # of points on the coarsest level in corresponding directions
! nxyz(3) - # of points at the finest level at coresponding directions
! prd(3) - 0 - non-periodic, 1 -periodic - in corresponding directions
! grid(3) - 0 -uniform grid, 1 - non-uniform grid in correspondign directions
! n_assym_**** - maximum assymetry allowed in the interpolation stencil for inner points 
!               ( used to reduce order of near boundary wlts)
! n_assym_****_bnd - maximum assymetry allowed in the interpolation stencil for boundary points
!                   ( used to reduce order of boundary wlts)
! nd_assym_*** - the same for derivative calcualtion
! backward_bias, forward_bias - vectors of dimension n_df+1 that define derivative operators, i.e. backward_bias and forward_bias biased 
! ibnd - defines the order of the boundaries (1-xmin,2-xmax,3-ymin,4-ymax,5-zmin,6-zmax), e.g. 132465
! nbnd - # of points on each boundary
! ibc - denotes wheter boundary algebraic or evolution type (not in use curently)
! xyzlimits(2,3)  - x,y,z domain min-max limits
! xyzzone(2,3)  - x, y, z, min-max limits where grid refinement is allowed, 
!                 outside of this zone the gird is uniform set to j_zn level.
! j_mx maxe wavelet level
! j_mn min wavelet level
! j_init min wavelet level during reading any initial data and intial grid adaptation
! j_tree_root - OPTIONAL: level of the tree roots for db_tree
!*********************************************************
MODULE wlt_vars
  USE precision
! Lines starting with !$ are only compiled if for openMP
!$  USE OMP_LIB
  PUBLIC
  INTEGER :: n_diff
  INTEGER :: j_lev, j_full, j_mn, j_mx, j_zn, j_tree_root !, j_full_inside
  INTEGER :: j_mn_init  ! serves as j_mn during grid adaptation for initial conditions
  INTEGER :: j_mn_evol  ! serves as j_mn during time evolution 
  INTEGER :: j_lev_init ! initial level of resolution to start grid adaptation during IC 
  INTEGER :: j_mx_adj   ! to limit the level if we start from DATA file or 
  !                     ! use fixed resolution IC with j_IC

  INTEGER ::  dim
  INTEGER, PARAMETER ::  nbhd = 1, n_df = 5, n_wlt_fmly = 1, n_trnsf_type = 1
  INTEGER, DIMENSION(0:n_wlt_fmly) :: n_prdct, n_updt, &            ! N predict/update
       n_prdct_resfile, n_updt_resfile                              ! ... as read from .res file (in restart)
  INTEGER                          :: maxval_n_prdct, maxval_n_updt !defined as maxval(n_prdct),maxval(n_updt) in io_3d:read_input()
  INTEGER, DIMENSION(3) :: mxyz, prd, grid     ! Oleg: dimensionality should not be hardwired
  INTEGER, DIMENSION(0:n_wlt_fmly,0:n_trnsf_type) ::  n_assym_prdct, n_assym_updt,n_assym_prdct_bnd,n_assym_updt_bnd
  INTEGER, DIMENSION(0:n_wlt_fmly,0:n_trnsf_type) ::  n_assym_prdct_resfile, n_assym_updt_resfile,n_assym_prdct_bnd_resfile,n_assym_updt_bnd_resfile
  INTEGER, DIMENSION(0:n_df) ::  nd_assym, nd_assym_bnd, nd2_assym, nd2_assym_bnd
  INTEGER, DIMENSION(0:n_df) ::  backward_bias = (/0,0,1,1,0,0/), forward_bias = (/0,0,0,0,1,1/) !Note if backward_bias=1, forward_bias=0 or vise versa
  INTEGER, DIMENSION(0:n_wlt_fmly) ::  wlt_backward_bias = (/0,0/), wlt_forward_bias = (/0,0/) !Note if backward_bias=1, forward_bias=0 or vise versa
  LOGICAL :: BNDzone
  ! OLEG: need to make flexible dimension and not hardwired
  INTEGER , DIMENSION(6)::  ibnd, nbnd, ibc   ! Oleg: dimensionality should not be hardwired
  REAL (pr)  :: eps      ! threshold epsilon   
  REAL (pr)  :: eps_expl ! threshold epsilon for explicit wavelet filtering 
  REAL (pr) ::  eps_init
  REAL (pr) ::  eps_run
  INTEGER   :: eps_adapt_steps 
  REAL (pr) :: cfl, cflmax, cflmin
  INTEGER, DIMENSION(1:3) :: dosymm
  LOGICAL   :: dt_changed=.TRUE.
  REAL (pr), DIMENSION (:), ALLOCATABLE :: scaleCoeff
  REAL (pr), DIMENSION (2,3) :: xyzlimits,xyzzone     ! Oleg: dimensionality should not be hardwired

  LOGICAL :: additional_planes_active
  INTEGER :: j_additional_planes, n_additional_planes
  INTEGER, DIMENSION(:), ALLOCATABLE :: dir_planes
  REAL (pr), DIMENSION(:,:), ALLOCATABLE :: xyz_planes

  LOGICAL :: additional_lines_active
  INTEGER :: j_additional_lines, n_additional_lines
  INTEGER, DIMENSION(:), ALLOCATABLE :: dir_lines
  REAL (pr), DIMENSION(:,:), ALLOCATABLE :: xyz_lines
  
  LOGICAL :: additional_points_active
  INTEGER :: j_additional_points, n_additional_points
  REAL (pr), DIMENSION(:,:), ALLOCATABLE :: xyz_points

  !--------------------------------------------------------
                                                 ! LOW_ORDER flag is now input at run time to match old functionality
  INTEGER, PARAMETER :: LOW_ORDER  = 0           ! FLAG - low order wavelet trnsform and derivative order (0 or 1)
  INTEGER, PARAMETER :: HIGH_ORDER = 1           ! FLAG - high order wavelet trnsform and derivative order
  INTEGER, PARAMETER :: BIASING_NONE     = 0     ! FLAG - no biosing in derivative operator
  INTEGER, PARAMETER :: BIASING_BACKWARD = 2     ! FLAG - no biosing in derivative operator
  INTEGER, PARAMETER :: BIASING_FORWARD  = 4     ! FLAG - no biosing in derivative operator
  INTEGER, PARAMETER :: WLT_TRNS_FWD = 1         ! FLAG forward wlt transform
  INTEGER, PARAMETER :: WLT_TRNS_INV = -1        ! FLAG inverse wlt transform
  INTEGER, PARAMETER :: WLT_TRNS_INV_wGHOST = -2 ! FLAG inverse wlt transform using ghost points
  !-------------------------------------------------------- 
  INTEGER, PARAMETER :: DB_TYPE_WRK = 1,           & ! define DB type wrk
                        DB_TYPE_LINES = 2,         & ! define DB type lines
                        DB_TYPE_TREE_C = 3,        & ! define DB type tree based on C/C++
                        DB_TYPE_TREE_F = 4,        & ! define DB type tree basedon FORTRAN
                        DB_TYPE_TREE_F_VIRTUAL = 5   ! define DB type virtual tree based on FORTRAN
 !-------------------------------------------------------- 
  INTEGER, PARAMETER :: INTERNAL = 1, NORMAL = 0     ! wavelet transfer type
  !------------------- need to be moved out to wavelet_3d_wrk, but fft does not work
  INTEGER, PUBLIC ::  jd
  INTEGER, DIMENSION(3), PUBLIC :: nxyz ! Oleg: dimensionality should not be hardwired
  ! ----------- variables common to all DBs ------------------------------------
  INTEGER, DIMENSION (:,:), ALLOCATABLE, PUBLIC :: Nwlt_lev ! used in all DBs
                                                       ! Nwlt_lev(j,0) - # of internal points on level j and below  
                                                       ! Nwlt_lev(j,1) - # of all (internal and boundary) points on level j and below 
  INTEGER, PUBLIC :: face_type_internal                ! # for internal face_type
    ! parameters used in calling c_wlt_trns_mask
  LOGICAL, PARAMETER, PUBLIC :: DO_UPDATE_DB_FROM_U = .TRUE. 
  LOGICAL, PARAMETER, PUBLIC :: DO_UPDATE_U_FROM_DB = .TRUE.
  LOGICAL, PARAMETER, PUBLIC :: DO_NOT_UPDATE_DB_FROM_U = .FALSE. 
  LOGICAL, PARAMETER, PUBLIC :: DO_NOT_UPDATE_U_FROM_DB = .FALSE.
  REAL (pr), DIMENSION (:,:), ALLOCATABLE, PUBLIC :: h
  REAL (pr), DIMENSION (:,:), ALLOCATABLE, PUBLIC :: xx
  !--- variables used by add_nodes
  INTEGER :: n_add_nodes
  INTEGER, DIMENSION(:,:), ALLOCATABLE :: ixyz_add_nodes

  INTEGER, DIMENSION (:), ALLOCATABLE :: lv_intrnl
  INTEGER, DIMENSION (:,:,:), ALLOCATABLE :: lv_bnd

CONTAINS
  !--------------------------------------------------------------------------------------------
  ! Predefine maximum assymetry allowed
  ! Used in (1) IC_file reading, (2) in init_DB while transforming to a different n_updt,
  ! (3) in post-processing codes
  SUBROUTINE set_max_assymetry ( POSTPROCESS )
    LOGICAL, OPTIONAL, INTENT(IN) :: POSTPROCESS     ! false by default
    !                                                ! If true - keep the values from .res file
    !                                                ! (in post-processing calls)
    INTEGER :: trnsf_type, &
         nd_assym_low, nd_assym_bnd_low, nd2_assym_low, nd2_assym_bnd_low, &
         nd_assym_high, nd_assym_bnd_high, nd2_assym_high, nd2_assym_bnd_high
    LOGICAL :: do_postprocess

    INTEGER :: i

    do_postprocess = .FALSE.
    IF (PRESENT(POSTPROCESS)) do_postprocess = POSTPROCESS
    
    IF (.NOT.do_postprocess) THEN
       DO trnsf_type = 0, n_trnsf_type !0 - regular, 1 - inernal
          n_assym_prdct(:,trnsf_type)     = MAX(2*n_prdct,n_prdct)!0 !  N_assym_prdct for regualr wavelet transform       %
          n_assym_prdct_bnd(:,trnsf_type) = MAX(2*n_prdct,n_prdct)!1 !  N_assym_prdct_bnd for regualr wavelet transform   % 
          n_assym_updt(:,trnsf_type)      = MAX(0*n_updt, n_updt)!0 !  N_assym_updt for regualr wavelet transform        %
          n_assym_updt_bnd(:,trnsf_type)  = MAX(0*n_updt, n_updt)!1 !  N_assym_updt_bnd for regualr wavelet transform    %
       END DO
    END IF
    
    nd_assym_high        = 2*n_diff+1!MAX(0,n_diff)!0 !  Nd_assym_high                                     %
    nd_assym_bnd_high    = 2*n_diff+1!MAX(1,n_diff)!1 !  Nd_assym_bnd_high                                 %
    nd2_assym_high       = 2*n_diff+2!MAX(0,n_diff) !  Nd2_assym_high                                    %
    nd2_assym_bnd_high   = 2*n_diff+2!MAX(2,n_diff) !  Nd2_assym_bnd_high                                %
    nd_assym_low         = 2*n_diff +3 !  Nd_assym_low                                      %
    nd_assym_bnd_low     = 2*n_diff +3 !  Nd_assym_bnd_low                                  %
    nd2_assym_low        = 2*n_diff +3 !  Nd2_assym_low                                     %
    nd2_assym_bnd_low    = 2*n_diff +3 !  Nd2_assym_bnd_low                                 %
    
    nd_assym =     (/nd_assym_low,nd_assym_high,nd_assym_low,nd_assym_high,nd_assym_low,nd_assym_high/)
    nd_assym_bnd = (/nd_assym_bnd_low,nd_assym_bnd_high,nd_assym_bnd_low,nd_assym_bnd_high,nd_assym_bnd_low,nd_assym_bnd_high/) 
    nd2_assym =    (/nd_assym_low,nd2_assym_high,nd_assym_low,nd2_assym_high,nd_assym_low,nd2_assym_high/)
    nd2_assym_bnd =(/nd2_assym_bnd_low,nd2_assym_bnd_high,nd2_assym_bnd_low,nd2_assym_bnd_high,nd2_assym_bnd_low,nd2_assym_bnd_high/)
    
  END SUBROUTINE set_max_assymetry
  
  ! Set the maximum asymmetry in the wavelet and derivative stencils based upon the 
  SUBROUTINE set_max_assymetry_from_resfile ( POSTPROCESS )
    IMPLICIT NONE
    LOGICAL, OPTIONAL, INTENT(IN) :: POSTPROCESS     ! false by default
    !                                                ! If true - set the derivative asymmetry 
    !                                                ! (in post-processing calls)
    INTEGER :: i
    INTEGER :: trnsf_type, &
         nd_assym_low, nd_assym_bnd_low, nd2_assym_low, nd2_assym_bnd_low, &
         nd_assym_high, nd_assym_bnd_high, nd2_assym_high, nd2_assym_bnd_high
    LOGICAL :: do_postprocess

    ! Set asymmetry from resfile for a correct inverse wavelet transform
    DO i = 0, n_trnsf_type !0 - regular, 1 - inernal
      n_assym_prdct(:,i)     = n_assym_prdct_resfile(:,0)
      n_assym_prdct_bnd(:,i) = n_assym_prdct_bnd_resfile(:,0)
      n_assym_updt(:,i)      = n_assym_updt_resfile(:,0)
      n_assym_updt_bnd(:,i)  = n_assym_updt_bnd_resfile(:,0)
    END DO

    do_postprocess = .FALSE.
    IF (PRESENT(POSTPROCESS)) do_postprocess = POSTPROCESS
    IF ( do_postprocess ) THEN 
      nd_assym_high        = 2*n_diff+1!MAX(0,n_diff)!0 !  Nd_assym_high                                     %
      nd_assym_bnd_high    = 2*n_diff+1!MAX(1,n_diff)!1 !  Nd_assym_bnd_high                                 %
      nd2_assym_high       = 2*n_diff+2!MAX(0,n_diff) !  Nd2_assym_high                                    %
      nd2_assym_bnd_high   = 2*n_diff+2!MAX(2,n_diff) !  Nd2_assym_bnd_high                                %
      nd_assym_low         = 2*n_diff +3 !  Nd_assym_low                                      %
      nd_assym_bnd_low     = 2*n_diff +3 !  Nd_assym_bnd_low                                  %
      nd2_assym_low        = 2*n_diff +3 !  Nd2_assym_low                                     %
      nd2_assym_bnd_low    = 2*n_diff +3 !  Nd2_assym_bnd_low                                 %
      
      nd_assym =     (/nd_assym_low,nd_assym_high,nd_assym_low,nd_assym_high,nd_assym_low,nd_assym_high/)
      nd_assym_bnd = (/nd_assym_bnd_low,nd_assym_bnd_high,nd_assym_bnd_low,nd_assym_bnd_high,nd_assym_bnd_low,nd_assym_bnd_high/) 
      nd2_assym =    (/nd_assym_low,nd2_assym_high,nd_assym_low,nd2_assym_high,nd_assym_low,nd2_assym_high/)
      nd2_assym_bnd =(/nd2_assym_bnd_low,nd2_assym_bnd_high,nd2_assym_bnd_low,nd2_assym_bnd_high,nd2_assym_bnd_low,nd2_assym_bnd_high/)
    END IF

  END SUBROUTINE set_max_assymetry_from_resfile

END MODULE wlt_vars
