!--AUTHOR: Jahrul Alam (alamj@mun.ca)
!  Dec 2006
!
! NONLINEAR_FASJ:  The algorithm was developed in
!
!        A SPACE-TIME WAVELET METHOD FOR TURBULENCE
!        PhD thesis by Jahrul Alam (2006)
!        McMaster University, Canada.
!
! NONLINEAR_FAS:   The above algorithm was modified 
!                  by using a pseudo-krylov method.
!
!        A multilevel adaptive wavelet method for nonlinear boundary value problems by
!        Jahrul Alam, Nicholas Kevlahan, and Oleg V. Vasilyev
!        (un-published work)! 
!--------------------
!
! 
! J. Alam (alamj@mun.ca): Notes
!         
!        1. There are some boundary condition issues, 
!           particularly with non-homogeneous case.
!           Neumann BC converges for some test cases.
!
#define LINSOLVE_TEST
!
MODULE nonlinear
  USE debug_vars  
  USE wlt_trns_mod
  USE wlt_vars
  USE elliptic_vars
  use util_vars, only : dA, sumdA_global
  use pde, only : wlog
  use input_file_reader
  use parallel
  implicit none
  
  ! UPDATE 
  ! is a smoother that implements local Newton's method 
  ! in such a way that if the problem is linear then
  ! UPDATE becomes JACOBI iteration.
  
  ! NONLINEAR_JACOBI 
  !        is the FAS solver that uses UPDATE for smoothing and 
  !        coarse grid solution
  ! 
  ! JFNK_SMOOTHER
  !        This is a PSEUDO-KRYLOV relaxation method,
  !        and is the core to the latest nolinear FAS solver.
  !
  ! list of private routines
  private :: &
       NONLINEAR_FASJ,& ! outer FAS solver with local Newton's method
       NJACOBI_FAS,&    ! inner FAS solver with local Newton's method
       UPDATE,&         ! local Newton's method => Jacobi smoother for linear case
       
       PEYRET,&         ! An iterative method in space-time domain
                        ! e.g. Ch.6 Computational Methods for 
                        ! Fluid Flows by R. Peyret and T. Taylor.
                        ! Currently, PEYRET is the core for the 
                        ! space-time (2D+t and 3D+t) solver.
                        ! However, the FAS-PERYRET is not 
                        ! included in this distribution 
                        ! (as of Jun, 2011).
       NONLINEAR_FAS,&  ! outer FAS solver with JFNK_SMOOTHER
       FAS,&            ! inner FAS solver with JFNK_SMOOTHER
       JFNK_SMOOTHER,&  ! pseudo-krylov smoother
       GMRES_SMOOTHER,& ! gmres-krylov smoother
       
       JACOBIAN,&       ! Frechet derivative
       FU,&             ! F(u):= f-L(u)
       
       ! These line search algorithms worked with older version of the nonlinear solver.
       ! These routines may be used in the future, if necessary for convergence
       ! The current version of the solver aims not to use
       ! the line-search method, unless can't be avoided.
       FAS_LINE_SEARCH_CORRECTION,&
       LINE_SEARCH_CORRECTION,&
       
       ! old function: may also be defined else where. 
       ! If i_clip is globally available, this should be removed from this module.
       I_CLIP

  ! 
  !  Note by J. Alam (alamj@mun.ca)
  !   -- In most GFD applications, I_CLIP is not necessary.
  !      So, this may be removed, soon.
  !   -- nsearch is needed only if line search algorithm
  !      is re-implemented in the future.
  !
  integer, private :: nsearch ! number of line-search steps
  
  !   -- Main input parameters for the nonlinear solver. 
  !   -- to be read by nonlinear_solver_input
  !   -- this rquires a call from the main program.
  !
  integer, private :: presmooth, postsmooth, mgmax, gama, kryp
  real (pr), private :: jfnk

  !   -- Two public routines
  public :: NONLINEAR_SOLVER_INPUT, NONLINEAR_SOLVER
  !   -- One public variable
  character(len=64), public :: SOLVER
  ! right place of this parameter may be PDE.mod
  integer, parameter, public:: TIME_INT_SPACE_TIME = 4
CONTAINS
  !
  subroutine nonlinear_solver_input()
    implicit none

    IF (par_rank.EQ.0) THEN
            WRITE(6,FMT='(A)') '********************************************************************'
            WRITE(6,FMT='(A)') '*  Inputs read by Nonlinear Solver                                 *'
            WRITE(6,FMT='(A)') '********************************************************************'
    END IF


    kryp = 2
    call input_integer ('kryp',kryp,'default', &
         ' krylov size for JFNK')

    gama = 0
    call input_integer ('gama',gama,'default', &
         ' gama')
    if(gama < 0 .or. gama > 2) stop 'ERROR: invalid choice of gama.'
    presmooth = 2
    call input_integer ('presmooth',presmooth,'default', &
         ' presmooth ')

    postsmooth = 2
    call input_integer ('postsmooth',postsmooth,'default', &
         ' postsmooth ')

    mgmax = 10
    call input_integer ('mgmax',mgmax,'default', &
         ' mgmax ')

    jfnk = 1.0e-8_pr
    call input_real('EPS_JFNK', jfnk, 'default', 'EPSILON for JFNK')


    SOLVER="FAS"
    call input_string('SOLVER',SOLVER,'default','nonlinear solver')

  end subroutine nonlinear_solver_input
  !
  ! This is the main solver that will be called 
  ! for both linear and nonlinear problems.
  ! For linear problems, this uses extra storage for FAS and 
  ! exctra calculation for Frechet derivative
  !

  subroutine NONLINEAR_SOLVER (uio, fio, tol, nlocal, neq,&
       ! Lulocal is the approximation of L(u)
       ! Lulocal_diag is the diagonal part of J(u) := d(L(u_i))/d(u_i)
       ! Lulocal_diag takes u as argument e.g. see the interface below
       !
       clip, Lulocal, Lulocal_diag,&
       !optional
       METHOD,SCL,verb)

    implicit none

    integer, intent (IN)                                :: nlocal
    integer, intent (IN)                                :: neq
    integer, dimension(neq),intent (inout)              :: clip
    real (pr),                        intent (IN)       :: tol
    real (pr), dimension (nlocal,neq), intent (INOUT)   :: uio
    real (pr), dimension (nlocal,neq), intent (INOUT)   :: fio
    real (pr), dimension (neq), intent (in), optional   :: SCL
    integer,intent (IN),optional                        :: verb
    character(len=*),optional                           :: METHOD
    integer                                             :: jmin
    integer                                             :: jmax
    integer                                             :: m
    integer                                             :: ie
    integer                                             :: j
    real (pr), dimension (nlocal,neq,j_lev)             :: res
    real (pr), dimension (nlocal,neq,j_lev)             :: ulocal
    real (pr), dimension (nlocal,neq,j_lev)             :: rhs
    real (pr), dimension (neq)                          :: error
    real (pr), dimension (neq)                          :: scl_u
    real (pr), dimension (neq)                          :: scl_f
    real (pr), dimension (neq)                          :: scl_local
    real (pr), dimension (neq)                          :: tolh
    real (pr)                                           :: floor_rhs
    real (pr)                                           :: reduction
    ! work in progress:
    !         min_level will be an input parameter....
    integer, parameter  :: min_level = 1

    INTERFACE
       FUNCTION Lulocal (jlev, uio, nlocal, neq, meth)
         USE precision
         USE sizes
         USE pde
         IMPLICIT NONE
         INTEGER, INTENT (IN) :: jlev, nlocal, neq, meth
         REAL (pr), DIMENSION (nlocal*neq), INTENT (INOUT) :: uio
         REAL (pr), DIMENSION (nlocal*neq) :: Lulocal
       END FUNCTION Lulocal
       FUNCTION Lulocal_diag (jlev, uio, nlocal, neq, meth)
         USE precision
         USE sizes
         USE pde
         IMPLICIT NONE
         INTEGER, INTENT (IN) :: jlev, nlocal, neq, meth
         REAL (pr), DIMENSION (nlocal*neq), INTENT (INOUT) :: uio
         REAL (pr), DIMENSION (nlocal*neq) :: Lulocal_diag
       END FUNCTION Lulocal_diag
    END INTERFACE
    real (pr) :: tmp1, tmp2

    floor_rhs = 1.e-12_pr
    !
    do ie = 1, neq !L2-norm
       tmp1=sum(fio(:,ie)**2*dA)
       tmp2=sum(uio(:,ie)**2*dA)
#ifdef MULTIPROC
       call parallel_global_sum(REAL=tmp1)
       call parallel_global_sum(REAL=tmp2)
#endif
       scl_f(ie) = sqrt(tmp1)
       scl_u(ie) = sqrt(tmp2)
    end do
    scl_local = max(scl_u,scl_f)
    if( present(SCL) ) scl_local = SCL
    where( scl_local < floor_rhs ) scl_local = 1.0_pr ! 
    tolh = tol * scl_local 

    if(par_rank == 0) then
       write(6,'("NONLINEAR SOLVER: for L(u) = f on levels: ",i2," to ", i2)') min_level, j_lev
       write(6,'("tolh=",es11.4," scl_local=",es11.4)') minval(tolh),minval(scl_local)
    end if

    if(.not. present(METHOD).or.len(trim(METHOD))==0) METHOD="FAS"

    IF (METHOD /= 'FAS') THEN
       IF (par_rank ==0) THEN
           write(6,'(5x,"PLEASE USE FAS, OTHER FEATURES ARE UNDER TESTING")')
       END IF
       call parallel_finalize
       stop
    END IF

    select case(METHOD)
    case('HELP')
       if(par_rank==0) then
          write(6,'("USAGE:")')
          write(6,'(5x,"HELP: THIS MESSAGE")')
          write(6,'(5x,"FAS: NONLINEAR AWCM-FAS SOLVER")')
          write(6,'(5x,"PLEASE USE FAS, OTHER FEATURES ARE UNDER TESTING")')
          !write(6,'(5x,"NJACOBI: NONLINEAR AWCM-FAS WITH JACOBI SMOOTHING")')
          !write(6,'(5x,"JFNK: NEWTON KRYLOV SMOTHER")')
       end if
       call parallel_finalize
       stop'----SELECT THE APPROPRIATE SOLVER----------'

    case('FASK')
       ! J Alam(alamj@mun.ca)
       ! This nonlinear solver uses a restarted krylov
       ! solver similar to restarted GMRES.
       ! This is tested for using FAS method to solve
       ! linear pressure Poisson equation.
       ! This should work for nonlinear system,
       ! which is to be tested fully.
       !
       call NONLINEAR_FASK (uio, fio, tolh, j_lev, min_level, nlocal, neq, clip, Lulocal, Lulocal_diag,SCL=scl_local,verb=1)

    case('FAS')
       ! Latest FAS nonlinear solver
       nsearch = 0
       call NONLINEAR_FAS (uio, fio, tolh, j_lev, min_level, nlocal, neq, clip, Lulocal, Lulocal_diag,SCL=scl_local,verb=1)

    case('JFNK')
       ! We can check how the smoother works on a test case.
       ! If FAS does not work, we should first check JFNK
       ! e.g switch SOLVER='FAS' or 'JFNK' in nonlinear4.inp
       !
       CALL JFNK_SMOOTHER (uio, fio, j_lev, nlocal, neq, len_iter, HIGH_ORDER, clip, Lulocal, Lulocal_diag,minval(tolh),VERB=1)

    case('RGMRES')
       ! This is restarted version of the GMRES.

       CALL GMRES_SMOOTHER (uio, fio, j_lev, nlocal, neq, 100, HIGH_ORDER, clip, Lulocal, Lulocal_diag,minval(tolh),VERB=1)

    case('FASJ')
       !
       ! FAS solver using UPDATE
       !
       call NONLINEAR_FASJ (uio, fio, tolh, j_lev, 1, nlocal, neq, clip, Lulocal, Lulocal_diag,SCL=scl_local,verb=1)

    case('NJACOBI')
       ! test the local Newton's method
       call Update(uio, fio, j_lev, nlocal, neq, 500, LOW_ORDER, clip, Lulocal, Lulocal_diag,verb=1)

    case('PEYRET')
       ! Core smoother for space-time solver
       ! This is in progress...........
       call PEYRET(uio, fio, j_lev, nlocal, neq, 500, LOW_ORDER, clip, Lulocal, Lulocal_diag,verb=1)

    end select
  end subroutine NONLINEAR_SOLVER
  !
  ! Nonlinear solver: (2009 March 17) --J.Alam
  !                 FAS is implemented using a krylov type approach
  !                 for smoothing with higher order stencil
  !
  subroutine NONLINEAR_FAS (uio, fio, tolh, jmax, jmin, nlocal, neq, clip, Lulocal, Lulocal_diag,SCL,verb)
    implicit none
    integer,                          intent (IN)    :: nlocal, neq
    integer,                          intent (IN)    :: jmin, jmax
    integer, dimension(neq),     intent (inout) :: clip
    real (pr), dimension(neq),     intent (IN)    :: tolh
    real (pr), dimension (nlocal,neq), intent (INOUT) :: uio
    real (pr), dimension (nlocal,neq), intent (INOUT) :: fio
    real (pr), dimension (neq), intent (in), optional :: SCL
    integer, optional :: verb
    INTERFACE
       FUNCTION Lulocal (jlev, u, nlocal, neq, meth)
         USE precision
         USE sizes
         USE pde
         IMPLICIT NONE
         INTEGER, INTENT (IN) :: jlev, nlocal, neq, meth
         REAL (pr), DIMENSION (nlocal*neq), INTENT (INOUT) :: u
         REAL (pr), DIMENSION (nlocal*neq) :: Lulocal
       END FUNCTION Lulocal
       FUNCTION Lulocal_diag (jlev, u, nlocal, neq, meth)
         USE precision
         USE sizes
         USE pde
         IMPLICIT NONE
         INTEGER, INTENT (IN) :: jlev, nlocal, neq, meth
         REAL (pr), DIMENSION (nlocal*neq), INTENT (INOUT) :: u
         REAL (pr), DIMENSION (nlocal*neq) :: Lulocal_diag
       END FUNCTION Lulocal_diag
    END INTERFACE
    integer :: ii, ie, k, j, m, shift
    integer, dimension(neq) :: noclip
    real(pr),dimension(neq) :: error, error0, scl_local, scl_u, scl_f
    real(pr), dimension(nlocal,neq) :: res, v, ulocal, rhs
    real(pr), dimension(nlocal,neq) :: res_tmp
    real(pr)        :: reduction, reduction0, cpu0,cpu1
    integer,   dimension (jmax)                 :: iclip
    real (pr) :: tmp1, tmp2, u_clip

    if( present(SCL) ) then 
       scl_local = SCL
    else
       scl_local = 1.0_pr
    end if

    if(present(VERB) .and. VERB==1 .and. jmax==j_lev) then
       if(par_rank==0) then
          write(6,'(/)')
          write(6,'(15x,"NONLINEAR_FAS")')
          write(6,'(7x,"Iteration",5x,"Residual",8x,"Relative Res.",3x,"reduction factor")')
       end if
    end if
    k=0
    res = fio - reshape (Lulocal (jmax, uio, nlocal, neq, HIGH_ORDER), (/nlocal,neq/))
    do ie=1,neq
       tmp1=SUM(res(:,ie)**2*dA)

       call parallel_global_sum(REAL=tmp1)

       error0(ie) = SQRT(tmp1/sumdA_global)

    end do

    reduction0=0.0_pr
    do 
       k=k+1
       call FAS (uio, fio, tolh, jmax, jmin, nlocal, neq, clip, Lulocal, Lulocal_diag,SCL=scl_local,verb=0)

       res = fio - reshape (Lulocal (jmax, uio, nlocal, neq, HIGH_ORDER), (/nlocal,neq/))
       DO ie=1, neq
          !clipping the solution for ie variable
          IF(clip(ie)==1) then
             u_clip = res(1,ie)

             call parallel_broadcast(REAL=u_clip)
!
             res(1:nlocal,ie)= res(1:nlocal,ie)-u_clip
          end IF
       END DO
       do ie=1,neq
          !Apr 10, 2009
          !Neumann BC issue: 
          !check convergence on internal points only
          tmp1=SUM(res(1:Nwlt_lev(jmax,1),ie)**2*dA)/sumdA_global
          tmp2=SUM(res(1:Nwlt_lev(jmax,0),ie)**2*dA)/sumdA_global

          call parallel_global_sum(REAL=tmp1)
          call parallel_global_sum(REAL=tmp2)

          if(SQRT(tmp1) > SQRT(tmp2)) then

             error(ie) = SQRT(tmp2)
          else
             error(ie) = SQRT(tmp1)
          end if
       end do

       reduction = sqrt(sum(error0**2))/sqrt(sum(error**2))
       if(par_rank==0) &
            write (6,'(5x, i5, 2(8x, es11.4), 5x, es9.2)') &
            k, &
            maxval(error), &
            maxval(error/maxval(scl_local)), &
            reduction
       ! How does the following line work in parallel?


       if(sum(error)/neq <= sum(tolh)/neq) exit
       !july 4, 2009
       if(1.0_pr/reduction <= minval(tolh)) then
          ! Residual has been reduced sufficiently.
          if(par_rank==0) write(6,'(5x, "MESSAGE: reduction < 1/tolerance")')
          exit
       end if
       if(k .GE. mgmax) then
          if(par_rank==0) write(6,'(5x,"WARNING: residual > tolerance!")')
          exit

       end if
       if(reduction0 >= reduction) then
          ! TODO:line search algorithm may used in this case
          !
          if(par_rank==0) write(6,'(5x,"WARNING: residual is not reduced")')
          exit

       end if

       reduction0=reduction
    end do
  END subroutine NONLINEAR_FAS
  !
  recursive subroutine FAS (uio, fio, tolh, &
       jmax, jmin, & ! levels
       nlocal, neq, clip, & ! array sizes and clipping flag
       Lulocal, Lulocal_diag,&
       SCL,verb) ! optional
    implicit none
    integer,                          intent (IN)    :: nlocal, neq
    integer,                          intent (IN)    :: jmin, jmax
    integer, dimension(neq),     intent (inout) :: clip
    real (pr), dimension(neq),     intent (IN)    :: tolh
    real (pr), dimension (nlocal,neq), intent (INOUT) :: uio
    real (pr), dimension (nlocal,neq), intent (INOUT) :: fio
    real (pr), dimension (neq), intent (in), optional :: SCL
    integer, optional :: verb
    INTERFACE
       FUNCTION Lulocal (jlev, u, nlocal, neq, meth)
         USE precision
         USE sizes
         USE pde
         IMPLICIT NONE
         INTEGER, INTENT (IN) :: jlev, nlocal, neq, meth
         REAL (pr), DIMENSION (nlocal*neq), INTENT (INOUT) :: u
         REAL (pr), DIMENSION (nlocal*neq) :: Lulocal
       END FUNCTION Lulocal
       FUNCTION Lulocal_diag (jlev, u, nlocal, neq, meth)
         USE precision
         USE sizes
         USE pde
         IMPLICIT NONE
         INTEGER, INTENT (IN) :: jlev, nlocal, neq, meth
         REAL (pr), DIMENSION (nlocal*neq), INTENT (INOUT) :: u
         REAL (pr), DIMENSION (nlocal*neq) :: Lulocal_diag
       END FUNCTION Lulocal_diag
    END INTERFACE

    integer :: ii, ie, k, j, m, shift
    real(pr),dimension(neq) :: error, error0
    real(pr), dimension(nlocal,neq) :: res, v, rhs, ulocal

    if(jmax == jmin) then
       CALL JFNK_SMOOTHER (uio, fio, jmax, nlocal, neq, 10, HIGH_ORDER, clip, Lulocal, Lulocal_diag,tol=maxval(tolh),VERB=0)
    else

       if(presmooth>0) then
          CALL JFNK_SMOOTHER (uio, fio, jmax, nlocal, neq, presmooth, HIGH_ORDER, clip, Lulocal, Lulocal_diag,tol=maxval(tolh),VERB=0)
       end if
       res = fio - reshape (Lulocal (jmax, uio, nlocal, neq, HIGH_ORDER), (/nlocal,neq/))

       v = uio
       j=jmax-1
       ! HIGH_ORDER interpolate works too, 
       !            but more tests are needed
       call wlt_interpolate(res(1:nlocal,1:neq), neq, 1, neq, jmax, j, nlocal, LOW_ORDER, internal)
       call wlt_interpolate(v(1:nlocal,1:neq), neq, 1, neq, jmax, j, nlocal, LOW_ORDER, internal)
       m  = Nwlt_lev(j,1)
       ! do I need to broadcast m to all processors?

       rhs(1:m,1:neq) = res(1:m,1:neq) + reshape (Lulocal (j, v(1:m,1:neq), m, neq, HIGH_ORDER), (/m,neq/) )

       !save restricted solution
       ulocal(1:m,1:neq)=v(1:m,1:neq)

       if(gama==0) then
          ! two level FAS
          CALL JFNK_SMOOTHER (v(1:m,1:neq), rhs(1:m,1:neq), j, m, neq, 2, HIGH_ORDER, clip, Lulocal, Lulocal_diag,tol=1.0e-7_pr,VERB=0)
       else
          ! gama = 1 => V-cycle
          ! gama = 2 => W-cycle
          do ii=1,gama
             call FAS (v(1:m,1:neq), rhs(1:m,1:neq), tolh, j, jmin, m, neq, clip, Lulocal, Lulocal_diag,SCL,verb=0)
          end do
       end if
       !error = computed - restricted
       v(1:m,1:neq) = v(1:m,1:neq) - ulocal(1:m,1:neq)

       call wlt_interpolate(v(:,1:neq), neq, 1, neq, j, jmax, nlocal, LOW_ORDER, internal)

       m  = Nwlt_lev(jmax,1)

       uio(1:m,1:neq) = uio(1:m,1:neq) + w2 * v(1:m,1:neq)

       if(postsmooth > 0) then
          CALL JFNK_SMOOTHER (uio, fio, jmax, m, neq, postsmooth, HIGH_ORDER, clip, Lulocal, Lulocal_diag,tol=maxval(tolh),VERB=0)
       end if


    end if

  END subroutine FAS
  !
  SUBROUTINE JFNK_SMOOTHER (uio, fio, jlev, nlocal, neq, &
       kry, meth, clip, &
       Lulocal, Lulocal_diag, &
       tol, VERB)

    USE precision
    USE elliptic_vars
    USE pde
    USE parallel
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: jlev, kry, nlocal, neq, meth
    INTEGER, DIMENSION(neq), INTENT(IN):: clip
    REAL (pr), DIMENSION (nlocal*neq), INTENT (IN) :: fio
    REAL (pr), DIMENSION (nlocal*neq), INTENT (INOUT) :: uio
    real (pr), intent(in) :: tol
    integer, intent(in), optional :: VERB


    INTEGER :: i, j, info, kryh, ntotal, ie, shift
    REAL (pr), DIMENSION (nlocal*neq) :: w, res
    REAL (pr), DIMENSION (nlocal*neq, kryp+1) :: v 
    REAL (pr), DIMENSION (nlocal*neq) :: sk, bb
    REAL (pr), DIMENSION (kryp+1, kryp) :: hess
    REAL (pr)                         :: hess_last
    REAL (pr), DIMENSION (kryp+1) :: e1
    REAL (pr) :: beta_loc, scl_local
    REAL (pr), DIMENSION (kryp+(kryp+1)*32) :: work
    REAL (pr), DIMENSION (nlocal*neq) :: JAC_diag ! local result of Lulocal_diag
    INTEGER :: iclip,ii,k
    INTEGER :: nlocal_global
    REAL(pr) :: tmp1, tmp2, tmp3, u_clip,bmax, bmax1, lmax

    REAL (pr), DIMENSION (nlocal*neq) :: JAC
    real (pr) :: err0, ak

    INTERFACE 
       FUNCTION Lulocal (jlev, u_local, nlocal, neq, meth)
         USE precision
         USE sizes
         USE pde
         IMPLICIT NONE
         INTEGER, INTENT (IN) :: jlev, nlocal, neq, meth
         REAL (pr), DIMENSION (nlocal*neq), INTENT (INOUT) :: u_local
         REAL (pr), DIMENSION (nlocal*neq) :: Lulocal
       END FUNCTION Lulocal
       FUNCTION Lulocal_diag (jlev, u_local, nlocal, neq, meth)
         USE precision
         USE sizes
         USE pde
         IMPLICIT NONE
         INTEGER, INTENT (IN) :: jlev, nlocal, neq, meth
         REAL (pr), DIMENSION (nlocal*neq), INTENT (INOUT) :: u_local
         REAL (pr), DIMENSION (nlocal*neq) :: Lulocal_diag
       END FUNCTION Lulocal_diag
    END INTERFACE

    if(par_rank==0) then
       if(present(VERB) .and. VERB==1) then
          write(6,'(/)')
          write(6,'(15x,"NEWTON KRYLOV SMOOTHER",1x,"LEVEL=",i1)') jlev
          write(6,'(7x,"Iteration",5x,"Residual",5x,"Relative Res.",5x, "Res. of Linear model")')
       end if
    end if
    ! Solve J(uk)v=f-L(uk)
    ! Let bb := b-L(u)
    ii=0
    
    ! how does one calculate bb in parallel?
    bb = Fu (Lulocal, jlev, uio, fio, nlocal, neq, meth)

    bmax=maxval(abs(bb))
!!!#ifdef MULTIPROC
    call parallel_global_sum(REALMAXVAL=bmax)
!!!#endif
    err0=bmax

    ! while(maxval(abs(bb))/maxval(abs(fio)) > tol)
    do 
       ii=ii+1
       ! Solve linear model: J*s_k = bb
       ! Newton error
!!$       sk = 0.0_pr
       ! RHS of linear model
       bb = Fu (Lulocal, jlev, uio, fio, nlocal, neq, meth)

       iclip = i_clip(jlev)
       DO ie=1, neq
          shift=(ie-1)*nlocal
          ! Note by J. Alam (alamj@mun.ca)
          ! This clipping is up-to-date as of Jan, 31st, 2008.
          ! Want: no cliping here.
          ! There are some test cases that require 
          ! to turn this cliping on.
          IF(clip(ie) == 1) then
             u_clip = bb(shift+iclip)
!!!#ifdef MULTIPROC
             call parallel_broadcast(REAL=u_clip)
!!!#endif
             !bb(shift+1:ie*nlocal)= bb(shift+1:ie*nlocal)-u_clip
          end IF
       END DO


       lmax = maxval(abs(bb - Jacobian (Lulocal, jlev, uio, sk, nlocal, neq, meth)))
       bmax = maxval(abs(bb))
       bmax1 = maxval(abs(fio))
!!!#ifdef MULTIPROC
       call parallel_global_sum(REALMAXVAL=lmax)
       call parallel_global_sum(REALMAXVAL=bmax)
       call parallel_global_sum(REALMAXVAL=bmax1)
!!!#endif
       if(par_rank == 0) then
          if(present(VERB) .and. VERB==1) then
             write(6,'(7x,i5,3(5x,es11.4))') ii,bmax,bmax/bmax1,lmax
          end if
       end if
       sk = 0.0_pr
       if(jlev > j_mn) then
          ! not on coarsest level
          ! check if about 50% residual is reduced
          ! during pre/post-smoothing stage.
          ! How many itereations are requested? => kry
          if(kry == presmooth) then
             ! requested # of iteration is presmooth
             if(err0/bmax > 2.0_pr) then
                !print*, 'pre ', ii, err0/bmax
                exit
             end if
          end if
          if(kry == postsmooth) then
             ! if presmooth==postsmooth in the input file
             ! will not reach here.
             if(err0/bmax > 2.0_pr) then
                !print*, 'post ', ii, err0/bmax
                exit
             end if
          end if
          if(ii > kry) then
             ! did not converge
             exit
          end if
          ! converge with any criterion
          if(bmax < tol) exit
          if(bmax/bmax1 < tol) exit
       else
          if(ii > kry) then
             exit
          end if
          if(bmax < tol) exit
          if(bmax/bmax1 < tol) exit
       end if

       nlocal_global = nlocal
       tmp1 = SUM(sk(:)**2)
       tmp2 = SUM(bb(:)**2)
!!!#ifdef MULTIPROC
       CALL parallel_global_sum( INTEGER=nlocal_global )
       CALL parallel_global_sum( REAL=tmp1 )
       CALL parallel_global_sum( REAL=tmp2 )
!!!#endif

       scl_local =  MAX(SQRT(tmp1/REAL(nlocal_global*neq,pr)),SQRT(tmp2/REAL(nlocal_global*neq,pr)),1.e-12_pr)


       ntotal = nlocal*neq
       kryh = kryp !(=2)
       hess_last = HUGE(hess_last) 
       !
       ! Want to solve: Jac(u)*sk = bb, and
       ! correction:    u <- u + sk.
       !
       ! Solve:
       !       M*r = bb-J(u)*sk, where M can be any preconditioner.
       !       Use M=D
       ! e.g. Ch.2, Templates for the solution of linear system
       !      or Brown and Saad (1990)
       !
       JAC_diag = Lulocal_diag(jlev, uio, nlocal, neq, meth)
       !
       res = 0.0_pr
       !
       JAC = Jacobian (Lulocal, jlev, uio, sk, nlocal, neq, meth)
       res = ( bb - JAC ) / Lulocal_diag(jlev, uio, nlocal, neq, meth) !JAC_diag
       !
       tmp1 = DOT_PRODUCT (res, res)
!!!#ifdef MULTIPROC
       CALL parallel_global_sum( REAL=tmp1 )
!!!#endif
       beta_loc = SQRT ( tmp1 )
       !
       hess = 0.0_pr
       w    = 0.0_pr
       v    = 0.0_pr
       v(:,1) = res / MAX(beta_loc,1.0e-24_pr) 
       j = 1 
       !
       DO WHILE( j <= kryh ) 
          !
          w = Jacobian (Lulocal, jlev, uio, v(:,j), nlocal, neq, meth) / JAC_diag
          !
          !
          DO i = 1, j
             hess (i,j) = DOT_PRODUCT (v(:,i), w)
!!!#ifdef MULTIPROC
             CALL parallel_global_sum( REAL=hess (i,j) )
!!!#endif
             !             
             w = w - v(:,i) * hess(i,j) 
          END DO

          tmp1 = DOT_PRODUCT (w, w)
!!!#ifdef MULTIPROC
          CALL parallel_global_sum( REAL=tmp1 )
!!!#endif
          hess(j+1,j) = SQRT ( tmp1 )

          IF(hess(j+1,j) > tol_gmres * scl_local .AND. .NOT. ( tol_gmres_stop_if_larger .AND. hess(j+1,j) > hess_last)  )  THEN
             v(:,j+1) = w / hess (j+1,j)
          ELSE
             IF( hess(j+1,j) > hess_last ) then
                if (par_rank==0) PRINT *,'Exiting iteration hess(j+1,j) > hess_last'
             end IF
             v(:,j+1) = 0.0_pr
             kryh=j
          END IF
          hess_last = hess(j+1,j) ! save last error criterion

          IF(present(VERB) .and. VERB==11.and. par_rank==0) WRITE (*,'("Krylov it = ",I4 ," Err = ",  es21.11 )') j, hess(j+1,j)
          j = j+1

       END DO
       e1 = 0.0_pr ; e1(1) = beta_loc

       !
       CALL DGELS ('N', kryh+1, kryh, 1, hess, kryp+1, e1, kryp+1, work, kryp+(kryp+1)*32, info)


       IF (info /=0 .and. par_rank==0) WRITE (6,'(i3)') info

       ! find the vector Sk - search direction
       ! should be in parallel ?? - JA
       sk = sk + MATMUL (v(1:nlocal*neq,1:kryh), e1(1:kryh))
       DO ie=1, neq
          !shift=(ie-1)*nlocal
          ! Debug:
          ! ensure Dirichelet BC
          !sk(shift+Nwlt_lev(jlev,0)+1:shift+Nwlt_lev(jlev,1)) = -uio(shift+Nwlt_lev(jlev,0)+1:shift+Nwlt_lev(jlev,1)) + fio(shift+Nwlt_lev(jlev,0)+1:shift+Nwlt_lev(jlev,1))
          !print*, maxval(abs(sk(shift+Nwlt_lev(jlev,0)+1:shift+Nwlt_lev(jlev,1))))
          ! Work in progress
          ! Dirichlet BC must be ensured from the above krylov iteration.
          ! This loop must be commented out for Neumann BC
          ! TODO - this must be a bug, may be in the case file.
          !        the algorithm enusures BC without this loop
       end DO

       if(present(VERB) .and. VERB==10) then
          if(par_rank==0)&
               print*,'Error of linear model', maxval(abs(bb - Jacobian (Lulocal, jlev, uio, sk, nlocal, neq, meth)))
       end if

       !Newton correction
       ak=1.0_pr
       ! ak must be calculated from a line search algorithm.
       ! e.g. the wolfe condition or the goldstein condition
       ! the following call is incosistent 
       ! with the previous version.
       !call LINE_SEARCH_CORRECTION(uio, fio, sk, nlocal, neq, Lulocal)
       DO ie=1, neq
          shift=(ie-1)*nlocal
          uio(shift+1:shift+Nwlt_lev(jlev,1)) = uio(shift+1:shift+Nwlt_lev(jlev,1)) + ak*sk(shift+1:shift+Nwlt_lev(jlev,1))
       END DO
       ! J. Alam: Aug 2010. BC has been checked. 
       !          Mathematical analysis indicates that correct BC 
       !          should be the result of above algorithm. 
       !          This does not happen for all test cases, 
       !          which "may" be a bug, 
       !          though there is a little chance that BC is broken
       !          by the Krylov method. 
       !          Should retain the following code.
       ! A temporary solution: J. Alam Dec 2009
       ! Use one step of inexact local Newton's method. 
       ! This should result in right boundary values 
       ! in the case of Dirichlet BC.
       !uio = uio + (fio - Lulocal (jlev, uio, nlocal, neq, meth))/Lulocal_diag(jlev, uio, nlocal, neq, meth)

       !March 22, 2009:
       ! the following cliping introduces small error in the boundary for case nonlinear4 with DBC
       ! May 22, 2011, this cliping helps with 3D lid cavity
       DO ie=1, neq
          shift=(ie-1)*nlocal
          !clipping the solution for ie variable
          IF(clip(ie) == 1) uio(shift+1:ie*nlocal)= uio(shift+1:ie*nlocal)-uio(shift+iclip) 
       END DO
       ! J Alam (alamj@mun.ca) Jul 5, 2011
       ! More tests are required to find out BC issues, and
       ! to remove all clippings, if Dirichlet BC is used.
    end do
  END SUBROUTINE JFNK_SMOOTHER
  !

  !
  ! Nonlinear solver: (2011 May 30) --J.Alam
  !     FAS is implemented using a krylov type approach
  !     for smoothing with re-started GMRES, 
  !     GMRES_SMOOTHER (next routine).
  !    
  !     This is a work in progress:
  !     Mathematical analysis is required to justify the convergence
  !     of the "nonlinear krylov method" tested below.
  !     This approach sends the residual of the nonlinear system
  !     directly to the Krylov space, avoiding Frechet Derivative,
  !     that is used in JFNK_SMOOTHER.
  ! Currently,
  !     NONLINEAR_FASK is verified with linear examples, however,
  !     it works fine with nonlinear examples as well.
  !
  subroutine NONLINEAR_FASK (uio, fio, tolh, &
       jmax, jmin, &
       nlocal, neq, clip, &
       Lulocal, Lulocal_diag,&
       SCL,verb)
    implicit none
    integer,                          intent (IN)    :: nlocal, neq
    integer,                          intent (IN)    :: jmin, jmax
    integer, dimension(neq),     intent (inout) :: clip
    real (pr), dimension(neq),     intent (IN)    :: tolh
    real (pr), dimension (nlocal,neq), intent (INOUT) :: uio
    real (pr), dimension (nlocal,neq), intent (INOUT) :: fio
    real (pr), dimension (neq), intent (in), optional :: SCL
    integer, optional :: verb
    INTERFACE
       FUNCTION Lulocal (jlev, u, nlocal, neq, meth)
         USE precision
         USE sizes
         USE pde
         IMPLICIT NONE
         INTEGER, INTENT (IN) :: jlev, nlocal, neq, meth
         REAL (pr), DIMENSION (nlocal*neq), INTENT (INOUT) :: u
         REAL (pr), DIMENSION (nlocal*neq) :: Lulocal
       END FUNCTION Lulocal
       FUNCTION Lulocal_diag (jlev, u, nlocal, neq, meth)
         USE precision
         USE sizes
         USE pde
         IMPLICIT NONE
         INTEGER, INTENT (IN) :: jlev, nlocal, neq, meth
         REAL (pr), DIMENSION (nlocal*neq), INTENT (INOUT) :: u
         REAL (pr), DIMENSION (nlocal*neq) :: Lulocal_diag
       END FUNCTION Lulocal_diag
    END INTERFACE
    integer :: ii, ie, k, j, m, shift
    integer, dimension(neq) :: noclip
    real(pr),dimension(neq) :: error, error0, scl_local, scl_u, scl_f
    real(pr), dimension(nlocal,neq) :: res, v, ulocal, rhs
    real(pr), dimension(nlocal,neq) :: res_tmp
    real(pr)        :: reduction, reduction0, cpu0,cpu1
    integer,   dimension (jmax)                 :: iclip
    real (pr) :: tmp1, tmp2, u_clip

    if( present(SCL) ) then 
       scl_local = SCL
    else
       scl_local = 1.0_pr
    end if

    if(present(VERB) .and. VERB==1 .and. jmax==j_lev) then
       if(par_rank==0) then
          write(6,'(/)')
          write(6,'(15x,"NONLINEAR_FASK")')
          write(6,'(7x,"Iteration",5x,"Residual",8x,"Relative Res.",3x,"reduction factor")')
       end if
    end if
    k=0
    res = fio - reshape (Lulocal (jmax, uio, nlocal, neq, HIGH_ORDER), (/nlocal,neq/))
    do ie=1,neq
       tmp1=SUM(res(:,ie)**2*dA)
#ifdef MULTIPROC
       call parallel_global_sum(REAL=tmp1)
#endif
       error0(ie) = SQRT(tmp1/sumdA_global)
       !print*,error0(ie)
    end do

    reduction0=0.0_pr
    do 
       k=k+1
       call FASK (uio, fio, tolh, jmax, jmin, nlocal, neq, clip, Lulocal, Lulocal_diag,SCL=scl_local,verb=0)

       res = fio - reshape (Lulocal (jmax, uio, nlocal, neq, HIGH_ORDER), (/nlocal,neq/))
       DO ie=1, neq
          !clipping the solution for ie variable
          IF(clip(ie)==1) then
             u_clip = res(1,ie)
#ifdef MULTIPROC
             call parallel_broadcast(REAL=u_clip)
#endif
             res(1:nlocal,ie)= res(1:nlocal,ie)-u_clip
          end IF
       END DO
       do ie=1,neq
          !Apr 10, 2009
          !Neumann BC issue: 
          !converges on internal points only
          tmp1=SUM(res(1:Nwlt_lev(jmax,1),ie)**2*dA)/sumdA_global
          tmp2=SUM(res(1:Nwlt_lev(jmax,0),ie)**2*dA)/sumdA_global
#ifdef MULTIPROC
          call parallel_global_sum(REAL=tmp1)
          call parallel_global_sum(REAL=tmp2)
#endif
          if(SQRT(tmp1) > SQRT(tmp2)) then

             error(ie) = SQRT(tmp2)
          else
             error(ie) = SQRT(tmp1)
          end if
          !print*,error(ie)
       end do

       reduction = sqrt(sum(error0**2))/sqrt(sum(error**2))
       if(par_rank==0) write (6,'(5x, i5, 2(8x, es11.4), 5x, es9.2)') k, maxval(error), maxval(error/maxval(scl_local)), reduction
       if(sum(error)/neq <= sum(tolh)/neq) exit
       !july 4, 2009
       if(1.0_pr/reduction <= minval(tolh)) then
          if(par_rank==0) write(6,'(5x, "MESSAGE: reduction < 1/tolerance")')
          exit
       end if
       if(k > mgmax) then
          if(par_rank==0) then
             write(6,'(5x, "WARNNING: residual > tolerance")')
             exit
          end if
       end if
       if(reduction0 >= reduction) then
          ! TODO:line search algorithm may used in this case
          !
          if(par_rank==0) then
             write(6,'(5x, "WARNNING: residual is not reduced")')
             exit
          end if
       end if
       reduction0=reduction
    end do
  END subroutine NONLINEAR_FASK
  !
  recursive subroutine FASK (uio, fio, tolh, &
       jmax, jmin, &
       nlocal, neq, clip, &
       Lulocal, Lulocal_diag,&
       SCL,verb)
    implicit none
    integer,                          intent (IN)    :: nlocal, neq
    integer,                          intent (IN)    :: jmin, jmax
    integer, dimension(neq),     intent (inout) :: clip
    real (pr), dimension(neq),     intent (IN)    :: tolh
    real (pr), dimension (nlocal,neq), intent (INOUT) :: uio
    real (pr), dimension (nlocal,neq), intent (INOUT) :: fio
    real (pr), dimension (neq), intent (in), optional :: SCL
    integer, optional :: verb
    INTERFACE
       FUNCTION Lulocal (jlev, u, nlocal, neq, meth)
         USE precision
         USE sizes
         USE pde
         IMPLICIT NONE
         INTEGER, INTENT (IN) :: jlev, nlocal, neq, meth
         REAL (pr), DIMENSION (nlocal*neq), INTENT (INOUT) :: u
         REAL (pr), DIMENSION (nlocal*neq) :: Lulocal
       END FUNCTION Lulocal
       FUNCTION Lulocal_diag (jlev, u, nlocal, neq, meth)
         USE precision
         USE sizes
         USE pde
         IMPLICIT NONE
         INTEGER, INTENT (IN) :: jlev, nlocal, neq, meth
         REAL (pr), DIMENSION (nlocal*neq), INTENT (INOUT) :: u
         REAL (pr), DIMENSION (nlocal*neq) :: Lulocal_diag
       END FUNCTION Lulocal_diag
    END INTERFACE

    integer :: ii, ie, k, j, m, shift
    real(pr),dimension(neq) :: error, error0
    real(pr), dimension(nlocal,neq) :: res, v, rhs, ulocal
    ! should we save local solution, see below for rcurssive call
    !real(pr), dimension(nlocal,neq,j_lev) :: ulocalj

    if(jmax == jmin) then
       CALL GMRES_SMOOTHER (uio, fio, jmax, nlocal, neq, 5, HIGH_ORDER, clip, Lulocal, Lulocal_diag,tol=6.0e-5_pr,VERB=11)
    else

       if(presmooth>0) then
          CALL GMRES_SMOOTHER (uio, fio, jmax, nlocal, neq, presmooth, HIGH_ORDER, clip, Lulocal, Lulocal_diag,tol=maxval(tolh),VERB=0)
       end if
       res = fio - reshape (Lulocal (jmax, uio, nlocal, neq, HIGH_ORDER), (/nlocal,neq/))

       v = uio
       j=jmax-1
       call wlt_interpolate(res(1:nlocal,1:neq), neq, 1, neq, jmax, j, nlocal, LOW_ORDER, internal)
       call wlt_interpolate(v(1:nlocal,1:neq), neq, 1, neq, jmax, j, nlocal, LOW_ORDER, internal)
       m  = Nwlt_lev(j,1)
       ! do I need to broadcast m to all processors?

       rhs(1:m,1:neq) = res(1:m,1:neq) + reshape (Lulocal (j, v(1:m,1:neq), m, neq, HIGH_ORDER), (/m,neq/) )

       !save restricted solution
       ulocal(1:m,1:neq)=v(1:m,1:neq)

       if(gama==0) then
          CALL GMRES_SMOOTHER (v(1:m,1:neq), rhs(1:m,1:neq), j, m, neq, 2, HIGH_ORDER, clip, Lulocal, Lulocal_diag,tol=1.0e-7_pr,VERB=1)
       else
          do ii=1,gama
             !CALL GMRES_SMOOTHER (v(1:m,1:neq), rhs(1:m,1:neq), j, m, neq, 7, HIGH_ORDER, clip, Lulocal, Lulocal_diag,tol=1.0e-7_pr,VERB=0)
             call FASK (v(1:m,1:neq), rhs(1:m,1:neq), tolh, j, jmin, m, neq, clip, Lulocal, Lulocal_diag,SCL,verb=1)
          end do
       end if
       !error = computed - restricted
       v(1:m,1:neq) = v(1:m,1:neq) - ulocal(1:m,1:neq)

       call wlt_interpolate(v(:,1:neq), neq, 1, neq, j, jmax, nlocal, LOW_ORDER, internal)

       m  = Nwlt_lev(jmax,1)

       uio(1:m,1:neq) = uio(1:m,1:neq) + w2 * v(1:m,1:neq)

       if(postsmooth > 0) then
          CALL GMRES_SMOOTHER (uio, fio, jmax, m, neq, postsmooth, HIGH_ORDER, clip, Lulocal, Lulocal_diag,tol=maxval(tolh),VERB=0)
       end if


    end if

  END subroutine FASK

  SUBROUTINE GMRES_SMOOTHER (uio, fio, jlev, nlocal, neq, kry, meth, clip, Lulocal, Lulocal_diag,tol,VERB)
    USE precision
    !USE elliptic_vars
    !USE pde
    USE parallel
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: jlev, kry, nlocal, neq, meth
    INTEGER, DIMENSION(neq), INTENT(IN):: clip
    real (pr), intent (in) :: tol
    integer, intent (in), optional :: VERB
    REAL (pr), DIMENSION (nlocal*neq), INTENT (IN) :: fio
    REAL (pr), DIMENSION (nlocal*neq), INTENT (INOUT) :: uio

    INTEGER :: kryh,ie, shift
    REAL (pr), DIMENSION (nlocal*neq) :: res
    REAL (pr) :: beta_loc, scl_local, u_clip
    REAL (pr), DIMENSION (nlocal*neq) :: diag_save
    INTEGER :: iclip
    INTEGER :: nlocal_global
    REAL(pr) :: tmp1, tmp2, tmp3

    INTERFACE 
       FUNCTION Lulocal (jlev, u_local, nlocal, neq, meth)
         USE precision
         USE sizes
         USE pde
         IMPLICIT NONE
         INTEGER, INTENT (IN) :: jlev, nlocal, neq, meth
         REAL (pr), DIMENSION (nlocal*neq), INTENT (INOUT) :: u_local
         REAL (pr), DIMENSION (nlocal*neq) :: Lulocal
       END FUNCTION Lulocal
       FUNCTION Lulocal_diag (jlev, u_local,  nlocal, neq, meth)
         USE precision
         USE sizes
         USE pde
         IMPLICIT NONE
         INTEGER, INTENT (IN) :: jlev, nlocal, neq, meth
         REAL (pr), DIMENSION (nlocal*neq) :: Lulocal_diag
         REAL (pr), DIMENSION (nlocal*neq), INTENT (INOUT) :: u_local
       END FUNCTION Lulocal_diag
    END INTERFACE

    iclip = i_clip(jlev)

    DO ie=1, neq
       shift=(ie-1)*nlocal
       !clipping the solution for ie variable
       IF(clip(ie) == 1) THEN
          u_clip = uio(shift+iclip)
          CALL parallel_broadcast( REAL=u_clip )
          uio(shift+1:ie*nlocal)= uio(shift+1:ie*nlocal)- u_clip
       END IF
    END DO
    

    nlocal_global = nlocal
    tmp1 = SUM(uio(:)**2)
    tmp2 = SUM(fio(:)**2)
    CALL parallel_global_sum( INTEGER=nlocal_global )
    CALL parallel_global_sum( REAL=tmp1 )
    CALL parallel_global_sum( REAL=tmp2 )
    scl_local =  MAX(SQRT(tmp1/REAL(nlocal_global*neq,pr)),SQRT(tmp2/REAL(nlocal_global*neq,pr)),1.e-12_pr)
    

    kryh = kry
!!$    hess_last = HUGE(hess_last) !set hass_last to max possile value
    
    
    !
    ! Call Lulocal_diag once and store result 
    !
    diag_save = Lulocal_diag(jlev, uio, nlocal, neq, meth)

    if(present(VERB) .and. VERB==1) then
       if (par_rank==0) then
          write(6,'(/)')
          write(6,'(15x,"Restarted Krylov solver")')
          write(6,'(7x,"Iteration",5x,"Residual",8x,"Relative Res.",3x,"reduction factor")')
       end if
    end if
 

    res = fio - Lulocal (jlev, uio, nlocal, neq, meth) 
    !tmp3 = sqrt(dot_product(res,res)) 
    tmp3 = maxval(abs(res)) 
#ifdef MULTIPROC
    CALL parallel_global_sum( REAL=tmp1 )
#endif
    kryh=0
    if(present(VERB) .and. VERB==11) then
       kryh=kry
    end if
    do 

       if(jlev > j_mn) then
          if(kry == presmooth .and. presmooth > 0) then
             ! used in pre-smoothing
             ! residual has been reduced by 50%
             ! Debug: not 50% for now
             if (tmp3/tmp2 > 10.0_pr) exit
          end if
          if(kry == postsmooth .and. postsmooth > 0) then
             ! used in post-smoothing
             ! residual has been reduced by 50%
             if (tmp3/tmp2 > 10.0_pr) exit
          end if
          if(kryh > kry) exit
          if (tmp2 < tol) exit
       else
          !  level = j_mn
          if(kryh > kry) exit  

       end if


       kryh=kryh+1
       
       call GMRES_RESTART (jlev, kryh, nlocal, neq, meth, clip, uio, fio, Lulocal, diag_save)
       
       
       DO ie=1, neq
          shift=(ie-1)*nlocal
          !clipping the solution for ie variable
          IF(clip(ie) == 1) THEN
             u_clip = uio(shift+iclip)
             CALL parallel_broadcast( REAL=u_clip )
             uio(shift+1:ie*nlocal)= uio(shift+1:ie*nlocal)-u_clip
          END IF
       END DO

       res = fio - Lulocal (jlev, uio, nlocal, neq, meth) 
       !tmp2 = sqrt(dot_product(res,res)) 
       tmp2 = maxval(abs(res)) 


       if(present(VERB) .and. VERB==1) then
          if (par_rank==0) then
             write(6,'(7x,i5,3(5x,es11.4))') kryh, tmp2,&
                  tmp2/maxval(abs(fio)),&
                  tmp3/tmp2
          end if
       end if

       if(tmp2<tol) exit
    end do
  END SUBROUTINE GMRES_SMOOTHER
  
  SUBROUTINE GMRES_RESTART (jlev, kryh, nlocal, neq, meth, clip, uio, fio, Lulocal, Lulocal_diag)
    USE precision
    USE elliptic_vars
    USE pde
    USE parallel
    IMPLICIT NONE
    INTEGER, INTENT (INOUT) :: kryh
    INTEGER, INTENT (IN) :: jlev, nlocal, neq, meth
    INTEGER, DIMENSION(neq), INTENT(IN):: clip
    REAL (pr), DIMENSION (nlocal*neq), INTENT (IN) :: fio
    REAL (pr), DIMENSION (nlocal*neq), INTENT (INOUT) :: uio
    REAL (pr), DIMENSION (nlocal*neq), intent (in) :: Lulocal_diag

    REAL (pr), DIMENSION (nlocal*neq) :: w, res
    real(pr) :: tmp1, beta_loc, scl_local
    REAL (pr), DIMENSION (nlocal*neq, kryh+1) :: v 
    REAL (pr), DIMENSION (kryh+1, kryh) :: hess
    REAL (pr)                         :: hess_last
    REAL (pr), DIMENSION (kryh+1) :: e1
    integer :: i, j, info
    REAL (pr), DIMENSION (kryh+(kryh+1)*32) :: work
    INTERFACE 
       FUNCTION Lulocal (jlev, u_local, nlocal, neq, meth)
         USE precision
         USE sizes
         USE pde
         IMPLICIT NONE
         INTEGER, INTENT (IN) :: jlev, nlocal, neq, meth
         REAL (pr), DIMENSION (nlocal*neq), INTENT (INOUT) :: u_local
         REAL (pr), DIMENSION (nlocal*neq) :: Lulocal
       END FUNCTION Lulocal
    END INTERFACE

    ! Solve M*r = b-A(x), A is linear or non-linear
    !       Use M=D, making a Jacobi sweep.
    res = ( fio - Lulocal (jlev, uio, nlocal, neq, meth) ) / Lulocal_diag

    tmp1 = DOT_PRODUCT (res, res)
#ifdef MULTIPROC
    CALL parallel_global_sum( REAL=tmp1 )
#endif
    beta_loc = SQRT ( tmp1 )
    
    hess = 0.0_pr
    w    = 0.0_pr
    v    = 0.0_pr
    v(:,1) = res / MAX(beta_loc,1.0e-24_pr) 
    j = 1 

    DO WHILE( j <= kryh ) 


       w = Lulocal(jlev, v(:,j), nlocal, neq, meth) / Lulocal_diag



       DO i = 1, j
          hess (i,j) = DOT_PRODUCT (v(:,i), w)
#ifdef MULTIPROC
          CALL parallel_global_sum( REAL=hess (i,j) )
#endif

          w = w - v(:,i) * hess(i,j) 
       END DO
       
       tmp1 = DOT_PRODUCT (w, w)
#ifdef MULTIPROC
       CALL parallel_global_sum( REAL=tmp1 )
#endif
       hess(j+1,j) = SQRT ( tmp1 )
       v(:,j+1) = w / hess (j+1,j)

       j = j+1

    END DO
    e1 = 0.0_pr ; e1(1) = beta_loc
    
    CALL DGELS ('N', kryh+1, kryh, 1, hess, kryh+1, e1, kryh+1, work, kryh+(kryh+1)*32, info)

    
    IF (info /=0) WRITE (6,'(i3)') info


    uio = uio + MATMUL (v(1:nlocal*neq,1:kryh), e1(1:kryh))

  end SUBROUTINE GMRES_RESTART

  FUNCTION Jacobian (Lulocal, jlev, uio, vv, nlocal, neq, meth)
    USE sizes
    use wlt_trns_util_mod
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: jlev, nlocal, neq, meth
    REAL (pr), DIMENSION (nlocal*neq), INTENT (INOUT) :: uio
    REAL (pr), DIMENSION (nlocal*neq), INTENT (INOUT) :: vv
    REAL (pr), DIMENSION (nlocal*neq) :: Jacobian

    REAL (pr), DIMENSION (nlocal*neq) :: v_local
    real (pr)                         :: epsJ, tmpj != 1.0e-8_pr
    interface
       FUNCTION Lulocal (jlev, u_local, nlocal, neq, meth)
         USE precision
         USE sizes
         USE pde
         IMPLICIT NONE
         INTEGER, INTENT (IN) :: jlev, nlocal, neq, meth
         REAL (pr), DIMENSION (nlocal*neq), INTENT (INOUT) :: u_local
         REAL (pr), DIMENSION (nlocal*neq) :: Lulocal
       END FUNCTION Lulocal
    end interface

    integer,dimension(neq) :: shift
    INTEGER :: i, ie, ii, nb, stag
    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc
    REAL (pr), DIMENSION (nlocal*neq) :: Luloc, vvloc

    ! epsJ can be calculated dynamically e.g. Knoll and Keyes (JCP 193(2) 2004)
    ! epsJ is about sqrt(C*machine precision), where C is found from some measure of error.
    ! TODO: an individual epsJ for individual component of uio.
    if(maxval(abs(uio))==0.0_pr) then
       !epsJ=1.0e-8_pr
       epsJ = jfnk ! from input file
    else
       epsJ=max(1.0e-11_pr,sum(1.0e-8_pr*abs(uio))/nlocal/dot_product(uio,uio))
    end if
    epsJ = min(epsJ,jfnk)
#ifdef MULTIPROC
    call parallel_broadcast(REAL=epsJ)
#endif

    Jacobian = - Lulocal (jlev, uio, nlocal, neq, meth)/epsJ
    v_local = uio + epsJ*vv
    Jacobian = Jacobian + Lulocal (jlev, v_local, nlocal, neq, meth)/epsJ

    ! J. Alam: Dec 2009
    ! the problem with Neumann condition is related to the following code
    ! for the latest solver, which is under development
    ! J. Alam: Aug 2010
    ! Work in progress
    ! Want to solve    L(u)=f -- internal
    !                  B(u)=g -- boundary,
    ! where L is nonlinear and B is linear.
    ! The linear model reads: J(uk)v=f-L(uk) -- internal
    !                         B(v) = g-B(uk) -- boundary
    ! J(uk)v is calculated above e.g. JFNK method.
    ! Want to implement J(uk)v=L(v) on a boundary
    ! This would result in
    !                   L(uk+1)=L(uk)+L(v)=L(uk)+f-L(v)=f
    ! since             J(uk)v=f-L(uk) is solved for v
    ! Calculate L(uk) for the use on boundaries.
    !           L(uk) = uk for Dirichlet BC
    !           L(uk) = B(uk) for Neumann BC
    Luloc = Lulocal (jlev, vv, nlocal, neq, meth)
    DO ie = 1, neq
       shift(ie) = (ie-1)*nlocal
       !--Go through all Boundary points that are specified
       i_p_face(0) = 1
       DO i=1,dim
          i_p_face(i) = i_p_face(i-1)*3
       END DO
       DO face_type = 0, 3**dim - 1
          face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
          IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
             CALL get_all_indices_by_face (face_type, jlev, nloc, iloc)
             IF(nloc > 0 ) THEN 
                IF( ALL( face == (/-1,0,0/) ) ) THEN
                   ! Ymin face (entire face) 
                   !Dirichlet conditions
                   Jacobian(shift(ie)+iloc(1:nloc)) = Luloc(shift(ie)+iloc(1:nloc)) 
                ELSE IF( ALL( face == (/1,0,0/) ) ) THEN

                   ! Ymax face (entire face) 
                   !Dirichlet conditions
                   Jacobian(shift(ie)+iloc(1:nloc)) = Luloc(shift(ie)+iloc(1:nloc)) 
                ELSE IF( ALL( face(2:dim) == (/-1,0/) ) ) THEN
                   ! ?Xmin face, edges || to X only, no corners
                   !Dirichlet conditions
                   Jacobian(shift(ie)+iloc(1:nloc)) = Luloc(shift(ie)+iloc(1:nloc)) 
                ELSE IF( ALL( face(2:dim) == (/1,0/) ) ) THEN
                   ! ?Xmax face, edges || to X only, no corners
                   !Dirichlet conditions
                   Jacobian(shift(ie)+iloc(1:nloc)) = Luloc(shift(ie)+iloc(1:nloc)) 
                ELSE IF( face(3) == -1 ) THEN  
                   ! Zmin face, no edges, no corners
                   !Dirichlet conditions
                   Jacobian(shift(ie)+iloc(1:nloc)) = Luloc(shift(ie)+iloc(1:nloc)) 
                ELSE IF( face(3) == 1 ) THEN  
                   ! Zmax face, no edges, no corners
                   !Dirichlet conditions
                   Jacobian(shift(ie)+iloc(1:nloc)) = Luloc(shift(ie)+iloc(1:nloc)) 
                END IF
             END IF
          END IF
       END DO
    END DO


  END FUNCTION Jacobian

  FUNCTION Fu (Lulocal, jlev, uio, vv, nlocal, neq, meth)
    USE sizes
    use wlt_trns_util_mod
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: jlev, nlocal, neq, meth
    REAL (pr), DIMENSION (nlocal*neq), INTENT (INOUT) :: uio ! u for L(u)
    REAL (pr), DIMENSION (nlocal*neq), INTENT (IN) :: vv ! f for L(u)=f
    REAL (pr), DIMENSION (nlocal*neq) :: Fu

    REAL (pr), DIMENSION (nlocal*neq) :: v_local
    real (pr), parameter  :: epsJ = 1.0e-5_pr
    interface
       FUNCTION Lulocal (jlev, u_local, nlocal, neq, meth)
         USE precision
         USE sizes
         USE pde
         IMPLICIT NONE
         INTEGER, INTENT (IN) :: jlev, nlocal, neq, meth
         REAL (pr), DIMENSION (nlocal*neq), INTENT (INOUT) :: u_local
         REAL (pr), DIMENSION (nlocal*neq) :: Lulocal
       END FUNCTION Lulocal
    end interface

    integer,dimension(neq) :: shift
    INTEGER :: i, ie, ii, nb, stag
    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc

    real (pr), parameter :: gam=1.0_pr
    real (pr) :: tau, tbeg
    REAL (pr), DIMENSION (nlocal*neq) :: uu

    Fu = vv - Lulocal (jlev, uio, nlocal, neq, meth)

    !J. Alam (Aug 2010)
    !      Fu should be g-B(uk) on a boundary, where
    !      B(u)=g is the boundary condition.
    !      TODO: the following code can be removed safely!!
    !
    ! work in progress: inconsistent with non-homogeneous BC
!!$    DO ie = 1, neq
!!$       shift(ie) = (ie-1)*nlocal
!!$       !--Go through all Boundary points that are specified
!!$       i_p_face(0) = 1
!!$       DO i=1,dim
!!$          i_p_face(i) = i_p_face(i-1)*3
!!$       END DO
!!$       DO face_type = 0, 3**dim - 1
!!$          face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
!!$          IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
!!$             CALL get_all_indices_by_face (face_type, jlev, nloc, iloc)
!!$             IF(nloc > 0 ) THEN 
!!$                IF( face(1) == -1  ) THEN
!!$                   ! Ymin face (entire face) 
!!$                   !Dirichlet conditions
!!$                   Fu(shift(ie)+iloc(1:nloc)) = 0.0_pr
!!$                ELSE IF( face(1) == 1 ) THEN
!!$                   ! Ymax face (entire face) 
!!$                   !Dirichlet conditions
!!$                   Fu(shift(ie)+iloc(1:nloc)) = 0.0_pr
!!$                ELSE IF( ALL( face(1:2) == (/0,-1/) ) ) THEN
!!$                   ! ?Xmin face, edges || to X only, no corners
!!$                   !Dirichlet conditions
!!$                   Fu(shift(ie)+iloc(1:nloc)) = 0.0_pr
!!$                ELSE IF( ALL( face(1:2) == (/0,1/) ) ) THEN
!!$                   ! ?Xmax face, edges || to X only, no corners
!!$                   !Dirichlet conditions
!!$                   Fu(shift(ie)+iloc(1:nloc)) =  0.0_pr
!!$                ELSE IF( ALL( face == (/0,0,-1/) ) ) THEN
!!$                   ! Zmin face, no edges, no corners
!!$                   !Dirichlet conditions
!!$                   Fu(shift(ie)+iloc(1:nloc)) = 0.0_pr
!!$                ELSE IF( ALL( face == (/0,0,1/) ) ) THEN
!!$                   ! Zmax face, no edges, no corners
!!$                   !Dirichlet conditions
!!$                   Fu(shift(ie)+iloc(1:nloc)) = 0.0_pr
!!$                END IF
!!$             END IF
!!$          END IF
!!$       END DO
!!$    END DO


  END FUNCTION Fu

  ! Nonlinear solver: (2009 March 17) --JA
  !   FAS is implemented using a local Newton method
  !       for smoothing with higher order stencil
  !                 
  subroutine NONLINEAR_FASJ (uio, fio, tolh, jmax, jmin, nlocal, neq, clip, Lulocal, Lulocal_diag,SCL,verb)
    implicit none
    integer,                          intent (IN)    :: nlocal, neq
    integer,                          intent (IN)    :: jmin, jmax
    integer, dimension(neq),     intent (inout) :: clip
    real (pr), dimension(neq),     intent (IN)    :: tolh
    real (pr), dimension (nlocal,neq), intent (INOUT) :: uio
    real (pr), dimension (nlocal,neq), intent (INOUT) :: fio
    real (pr), dimension (neq), intent (in), optional :: SCL
    integer, optional :: verb
    INTERFACE
       FUNCTION Lulocal (jlev, u, nlocal, neq, meth)
         USE precision
         USE sizes
         USE pde
         IMPLICIT NONE
         INTEGER, INTENT (IN) :: jlev, nlocal, neq, meth
         REAL (pr), DIMENSION (nlocal*neq), INTENT (INOUT) :: u
         REAL (pr), DIMENSION (nlocal*neq) :: Lulocal
       END FUNCTION Lulocal
       FUNCTION Lulocal_diag (jlev, u, nlocal, neq, meth)
         USE precision
         USE sizes
         USE pde
         IMPLICIT NONE
         INTEGER, INTENT (IN) :: jlev, nlocal, neq, meth
         REAL (pr), DIMENSION (nlocal*neq), INTENT (INOUT) :: u
         REAL (pr), DIMENSION (nlocal*neq) :: Lulocal_diag
       END FUNCTION Lulocal_diag
    END INTERFACE
    integer :: ii, ie, k, j, m, shift
    integer, dimension(neq) :: noclip
    real(pr),dimension(neq) :: error, error0, scl_local, scl_u, scl_f
    real(pr), dimension(nlocal,neq) :: res, v, ulocal, rhs
    real(pr), dimension(nlocal,neq) :: res_tmp
    real(pr)        :: reduction, reduction0, cpu0,cpu1
    integer,   dimension (jmax)                 :: iclip


    do ie = 1, neq
       scl_f(ie) = maxval(abs(fio(:,ie)))!sqrt(sum(fio(:,ie)**2*dA))
       scl_u(ie) = maxval(abs(uio(:,ie)))!sqrt(sum(uio(:,ie)**2*dA))
    end do
    scl_local = max(scl_u,scl_f)
    if( present(SCL) ) scl_local = max(scl_local,SCL)
    where(scl_local < 1.0e-12_pr) scl_local = 1.0_pr

    if(present(VERB) .and. VERB==1 .and. jmax==j_lev) then
       write(6,'(/)')
       write(6,'(15x,"NONLINEAR_FASJ")')
       write(6,'(7x,"Iteration",5x,"Residual",8x,"Relative Res.",3x,"reduction factor")')
    end if
    k=0
    res = fio - reshape (Lulocal (jmax, uio, nlocal, neq, HIGH_ORDER), (/nlocal,neq/))
    do ie=1,neq
       error0(ie) = SQRT(SUM(res(:,ie)**2*dA)/sumdA_global)
       !print*,error0(ie)
    end do

    reduction0=0.0_pr
    do 
       k=k+1
       call NJACOBI_FAS (uio, fio, tolh, jmax, jmin, nlocal, neq, clip, Lulocal, Lulocal_diag,SCL=scl_local,verb=0)

       res = fio - reshape (Lulocal (jmax, uio, nlocal, neq, HIGH_ORDER), (/nlocal,neq/))
       DO ie=1, neq
          !clipping the solution for ie variable
          IF(clip(ie)==1) res(1:nlocal,ie)= res(1:nlocal,ie)-res(1,ie)
       END DO
       do ie=1,neq
          error(ie) = SQRT(SUM(res(:,ie)**2*dA)/sumdA_global)
          !print*,error(ie)
       end do

       reduction = sqrt(sum(error0**2))/sqrt(sum(error**2))
       write (6,'(5x, i5, 2(8x, es11.4), 5x, i10)') k, maxval(error), maxval(error/maxval(scl_local)), floor(reduction)
       if(sum(error)/neq <= sum(tolh)/neq) exit
       if(1.0_pr/reduction <= minval(tolh)) then
!!$       if(any(1.0_pr/reduction <= tolh)) then
          if(par_rank==0) write(6,'(5x, "WARNNING: reduction < tolerance")')
          exit
       end if
       if(k > mgmax) then
          write(6,'(5x, "WARNNING: residual > tolerance")')
          exit
       end if
       if(reduction0 >= reduction) then
          write(6,'(5x, "WARNNING: residual is not reduced")')
          exit
       end if
       reduction0=reduction
    end do
  END subroutine NONLINEAR_FASJ
  !
  ! FAS with nonlinear Jacobi as smoother
  recursive subroutine NJACOBI_FAS (uio, fio, tolh, jmax, jmin, nlocal, neq, clip, Lulocal, Lulocal_diag,SCL,verb)
    implicit none
    integer,                          intent (IN)    :: nlocal, neq
    integer,                          intent (IN)    :: jmin, jmax
    integer, dimension(neq),     intent (inout) :: clip
    real (pr), dimension(neq),     intent (IN)    :: tolh
    real (pr), dimension (nlocal,neq), intent (INOUT) :: uio
    real (pr), dimension (nlocal,neq), intent (INOUT) :: fio
    real (pr), dimension (neq), intent (in), optional :: SCL
    integer, optional :: verb
    INTERFACE
       FUNCTION Lulocal (jlev, u, nlocal, neq, meth)
         USE precision
         USE sizes
         USE pde
         IMPLICIT NONE
         INTEGER, INTENT (IN) :: jlev, nlocal, neq, meth
         REAL (pr), DIMENSION (nlocal*neq), INTENT (INOUT) :: u
         REAL (pr), DIMENSION (nlocal*neq) :: Lulocal
       END FUNCTION Lulocal
       FUNCTION Lulocal_diag (jlev, u, nlocal, neq, meth)
         USE precision
         USE sizes
         USE pde
         IMPLICIT NONE
         INTEGER, INTENT (IN) :: jlev, nlocal, neq, meth
         REAL (pr), DIMENSION (nlocal*neq), INTENT (INOUT) :: u
         REAL (pr), DIMENSION (nlocal*neq) :: Lulocal_diag
       END FUNCTION Lulocal_diag
    END INTERFACE
    integer, parameter :: gama = 1
    integer :: ii, ie, k, j, m, shift
    integer, dimension(neq) :: noclip
    real(pr),dimension(neq) :: error, error0!, tolh!, scl_local
    real(pr), dimension(nlocal,neq) :: res, v, ulocal, rhs
    real(pr), dimension(nlocal,neq) :: res_tmp
    real(pr)        :: reduction, cpu0,cpu1
    integer,   dimension (jmax)                 :: iclip

    if(jmax == jmin) then
       CALL UPDATE(uio, fio, jmax, nlocal, neq, 10, HIGH_ORDER, clip, Lulocal, Lulocal_diag, VERB=0)
    else

       if(presmooth>0) then
          CALL UPDATE(uio, fio, jmax, nlocal, neq, presmooth, HIGH_ORDER, clip, Lulocal, Lulocal_diag, VERB=0)
       end if
       res = fio - reshape (Lulocal (jmax, uio, nlocal, neq, HIGH_ORDER), (/nlocal,neq/))

       v = uio
       j=jmax-1
       call wlt_interpolate(res(1:nlocal,1:neq), neq, 1, neq, jmax, j, nlocal, LOW_ORDER, internal)
       call wlt_interpolate(v(1:nlocal,1:neq), neq, 1, neq, jmax, j, nlocal, LOW_ORDER, internal)
       m  = Nwlt_lev(j,1)

       rhs(1:m,1:neq) = res(1:m,1:neq) + reshape (Lulocal (j, v(1:m,1:neq), m, neq, HIGH_ORDER), (/m,neq/) )

       !save restricted solution
       ulocal(1:m,1:neq)=v(1:m,1:neq)

       do ii=1,gama
          call NJACOBI_FAS (v(1:m,1:neq), rhs(1:m,1:neq), tolh, j, jmin, m, neq, clip, Lulocal, Lulocal_diag,SCL,verb=0)
          !CALL JFNK_SMOOTHER (v(1:m,1:neq), rhs(1:m,1:neq), j, m, neq, 2, HIGH_ORDER, clip, Lulocal, Lulocal_diag,tol=maxval(tolh),VERB=1)
       end do

       !error = computed - restricted
       v(1:m,1:neq) = v(1:m,1:neq) - ulocal(1:m,1:neq)

       call wlt_interpolate(v(:,1:neq), neq, 1, neq, j, jmax, nlocal, LOW_ORDER, normal)

       m  = Nwlt_lev(jmax,1)

       uio(1:m,1:neq) = uio(1:m,1:neq) + w2 * v(1:m,1:neq)

       if(postsmooth > 0) then
          CALL UPDATE(uio, fio, jmax, m, neq, postsmooth, HIGH_ORDER, clip, Lulocal, Lulocal_diag, VERB=0)
       end if


    end if

  END subroutine NJACOBI_FAS

  subroutine Peyret(uio, fio, jlev, nlocal, neq, len, meth, clip, Lulocal, Lulocal_diag, verb)
    use precision
    use sizes
    use elliptic_vars
    use share_consts
    use pde, only : tol1
    implicit none
    integer, intent(IN)  :: nlocal
    integer, intent(IN)  :: neq
    integer, intent(IN)  :: jlev
    integer, intent(IN)  :: len
    integer, intent(IN)  :: meth
    integer, dimension (neq), intent (in)  :: clip
    integer, intent(IN), optional          :: verb
    real (pr), dimension (1:nlocal*neq), intent (INOUT)    :: uio
    real (pr), dimension (1:nlocal*neq), intent (IN)       :: fio

    !--Local vaiables
    real (pr) :: u_clip
    integer :: ie, k, shift, i, iclip,jj
    real (pr), dimension (1:nlocal*neq)    :: res
    real (pr)                              :: tol
    real (pr)                              :: error, error0, ratio
    REAL (pr), DIMENSION (nlocal*neq)      :: Lulocal_diag_save
    logical            :: BND
    interface
       FUNCTION Lulocal (jlev, u, nlocal, neq, meth)
         USE precision
         USE sizes
         USE pde
         IMPLICIT NONE
         INTEGER, INTENT (IN) :: jlev, nlocal, neq, meth
         REAL (pr), DIMENSION (nlocal*neq), INTENT (INOUT) :: u
         REAL (pr), DIMENSION (nlocal*neq) :: Lulocal
       END FUNCTION Lulocal
       FUNCTION Lulocal_diag (jlev, u, nlocal, neq, meth)
         USE precision
         USE sizes
         USE pde
         IMPLICIT NONE
         INTEGER, INTENT (IN) :: jlev, nlocal, neq, meth
         REAL (pr), DIMENSION (nlocal*neq), INTENT (INOUT) :: u
         REAL (pr), DIMENSION (nlocal*neq) :: Lulocal_diag
       END FUNCTION Lulocal_diag
    end interface

    iclip = i_clip(jlev)
    Lulocal_diag_save = Lulocal_diag (jlev, uio, nlocal, neq, meth)
    tol = 5.e-09_pr
    tol = max(tol,tol1)

    DO ie=1, neq
       shift=(ie-1)*nlocal
       !clipping the solution for ie variable
       IF(clip(ie) == 1) then 
          u_clip = uio(shift+iclip)
#ifdef MULTIPROC
          call parallel_broadcast(REAL=u_clip)
#endif
          uio(shift+1:ie*nlocal)= uio(shift+1:ie*nlocal)-u_clip
       end IF
    END DO

    BND = (Nwlt_lev(jlev,1) > Nwlt_lev(jlev,0))

    k=1
    res = fio - Lulocal (jlev, uio, nlocal, neq, meth)
    error0 = SQRT(dot_product(res,res)/nlocal/neq)
!!$  error0 = maxval(abs(res))/nlocal/neq
    error=error0

    error = 100.0_pr
    do while(error > tol .and. k <= len)

       ! updating booundary points
       IF(BND) THEN
          Lulocal_diag_save = Lulocal_diag (jlev, uio, nlocal, neq, meth)

          res = ( fio - Lulocal (jlev, uio, nlocal, neq, meth) ) / Lulocal_diag_save

          DO ie=1, neq
             shift=(ie-1)*nlocal
             ! updating boundary points
             uio(shift+Nwlt_lev(jlev,0)+1:ie*nlocal)=uio(shift+Nwlt_lev(jlev,0)+1:ie*nlocal) + res(shift+Nwlt_lev(jlev,0)+1:ie*nlocal)
          END DO

          DO ie=1, neq
             shift=(ie-1)*nlocal
             !clipping the solution for ie variable
             IF(clip(ie) == 1) uio(shift+1:ie*nlocal)= uio(shift+1:ie*nlocal)-uio(shift+iclip) 
          END DO


       END IF

       Lulocal_diag_save = Lulocal_diag (jlev, uio, nlocal, neq, meth)
       res = ( fio - Lulocal (jlev, uio, nlocal, neq, meth) ) / Lulocal_diag_save

       DO ie=1, neq
          shift=(ie-1)*nlocal
          ! updating internal points
          do jj=jlev,jlev
             uio(shift+1:shift+Nwlt_lev(jj,1))=uio(shift+1:shift+Nwlt_lev(jj,1)) + res(shift+1:shift+Nwlt_lev(jj,1))
          end do
       END DO

       DO ie=1, neq
          shift=(ie-1)*nlocal
          !clipping the solution for ie variable
          IF(clip(ie) == 1) then 
             u_clip = uio(shift+iclip)
#ifdef MULTIPROC
             call parallel_broadcast(REAL=u_clip)
#endif
             uio(shift+1:ie*nlocal)= uio(shift+1:ie*nlocal)-u_clip
          end IF
       END DO

       res = ( fio - Lulocal (jlev, uio, nlocal, neq, meth) ) 
!!$       error = SQRT(dot_product(res,res)/nlocal/neq)
       error = SQRT(dot_product(res,res)/neq)
!!$       error = maxval(abs(res))
!!$       error = maxval(abs(res))/nlocal/neq
!!$       if (SQRT(DOT_PRODUCT(res,res)/nlocal) <= tol) then
       error=0.0_pr
       DO ie=1, neq
          shift=(ie-1)*nlocal
          error = error+maxval(abs(res(shift+1:shift+Nwlt_lev(jlev,0))))
       end DO
       if(present(verb) .and. verb == 1) then
          write (6,'("Peyret",1x, i5, 5x, es11.4, 5x, i6,"%")') k, error, int((error0-error)/error0*100.0_pr)
       end if
       if(error <= tol) then
          !if(present(verb) .and. verb == 1) print*, k
          exit
       end if

       error0=error
       k=k+1
    END DO

  end subroutine PEYRET
  !

  subroutine UPDATE(uio, fio, jlev, nlocal, neq, len, meth, clip, Lulocal, Lulocal_diag, verb)
    use precision
    use sizes
    use elliptic_vars
    use share_consts
    use pde, only : tol1
    implicit none
    integer, intent(IN)   :: nlocal
    integer, intent(IN)   :: neq
    integer, intent(IN)   :: jlev
    integer, intent(IN)   :: len
    integer, intent(IN)   :: meth
    integer, dimension (neq), intent (in) :: clip
    integer, intent(IN), optional         :: verb
    real (pr), dimension (1:nlocal*neq), intent (INOUT)  :: uio
    real (pr), dimension (1:nlocal*neq), intent (IN)     :: fio

    !--Local vaiables
    real (pr) :: u_clip
    integer   :: ie, k, shift, i, iclip
    real (pr) :: tol
    real (pr) :: error, error0, ratio
    logical   :: BND
    real (pr), dimension (1:nlocal*neq)  :: res
    REAL (pr), DIMENSION (nlocal*neq)    :: Lulocal_diag_save
    interface
       FUNCTION Lulocal (jlev, u, nlocal, neq, meth)
         USE precision
         USE sizes
         USE pde
         IMPLICIT NONE
         INTEGER, INTENT (IN) :: jlev, nlocal, neq, meth
         REAL (pr), DIMENSION (nlocal*neq), INTENT (INOUT) :: u
         REAL (pr), DIMENSION (nlocal*neq) :: Lulocal
       END FUNCTION Lulocal
       FUNCTION Lulocal_diag (jlev, u, nlocal, neq, meth)
         USE precision
         USE sizes
         USE pde
         IMPLICIT NONE
         INTEGER, INTENT (IN) :: jlev, nlocal, neq, meth
         REAL (pr), DIMENSION (nlocal*neq), INTENT (INOUT) :: u
         REAL (pr), DIMENSION (nlocal*neq) :: Lulocal_diag
       END FUNCTION Lulocal_diag
    end interface

    iclip = i_clip(jlev)
    Lulocal_diag_save = Lulocal_diag (jlev, uio, nlocal, neq, meth)
    tol = 5.e-09_pr
    tol = max(tol,tol1)

    DO ie=1, neq
       shift=(ie-1)*nlocal
       !clipping the solution for ie variable
       IF(clip(ie) == 1) then 
          u_clip = uio(shift+iclip)
#ifdef MULTIPROC
          call parallel_broadcast(REAL=u_clip)
#endif
          !uio(shift+1:ie*nlocal)= uio(shift+1:ie*nlocal)-u_clip
       end IF
    END DO

    BND = (Nwlt_lev(jlev,1) > Nwlt_lev(jlev,0))

    k=1
    res = fio - Lulocal (jlev, uio, nlocal, neq, meth)

    error0 = SQRT(dot_product(res,res)/nlocal/neq)
!!$  error0 = maxval(abs(res))/nlocal/neq
    error=error0

    error = 100.0_pr
    do while(error > tol .and. k <= len)


       Lulocal_diag_save = Lulocal_diag (jlev, uio, nlocal, neq, meth)
       res = ( fio - Lulocal (jlev, uio, nlocal, neq, meth) ) / Lulocal_diag_save

       DO ie=1, neq
          shift=(ie-1)*nlocal
          ! updating internal points
          uio(shift+1:shift+Nwlt_lev(jlev,0))=uio(shift+1:shift+Nwlt_lev(jlev,0)) + w2*res(shift+1:shift+Nwlt_lev(jlev,0))
       END DO

       ! updating booundary points
       IF(BND) THEN
          Lulocal_diag_save = Lulocal_diag (jlev, uio, nlocal, neq, meth)

          res = ( fio - Lulocal (jlev, uio, nlocal, neq, meth) ) / Lulocal_diag_save

          DO ie=1, neq
             shift=(ie-1)*nlocal
             ! updating boundary points
             uio(shift+Nwlt_lev(jlev,0)+1:ie*nlocal)=uio(shift+Nwlt_lev(jlev,0)+1:ie*nlocal) + w3*res(shift+Nwlt_lev(jlev,0)+1:ie*nlocal)
          END DO

          DO ie=1, neq
             shift=(ie-1)*nlocal
             !clipping the solution for ie variable
             IF(clip(ie) == 1) uio(shift+1:ie*nlocal)= uio(shift+1:ie*nlocal)-uio(shift+iclip) 
          END DO


       END IF

       DO ie=1, neq
          shift=(ie-1)*nlocal
          !clipping the solution for ie variable
          IF(clip(ie) == 1) then 
             u_clip = uio(shift+iclip)
#ifdef MULTIPROC
             call parallel_broadcast(REAL=u_clip)
#endif
             uio(shift+1:ie*nlocal)= uio(shift+1:ie*nlocal)-u_clip
          end IF
       END DO

       res = ( fio - Lulocal (jlev, uio, nlocal, neq, meth) ) 
!!$       error = SQRT(dot_product(res,res)/nlocal/neq)
       error = maxval(abs(res))!/nlocal/neq
       if (SQRT(DOT_PRODUCT(res,res)/nlocal) <= tol) then
          if(present(verb) .and. verb == 1) print*, k
          exit
       end if
       if(present(verb) .and. verb == 1) then
          write (6,'("Update",1x, i5, 5x, es11.4, 5x, i6,"%")') k, error, int((error)/error0*100.0_pr)
       end if
       error0=error
       k=k+1
    END DO

  end subroutine UPDATE
  !
  !
  subroutine FAS_LINE_SEARCH_CORRECTION(uio, fio, vio, nlocal, neq, jlev, Lulocal)
    implicit none
    INTEGER,   INTENT (IN)    :: nlocal, neq, jlev
    REAL (pr), DIMENSION (nlocal, neq), INTENT (INOUT) :: uio
    REAL (pr), DIMENSION (nlocal, neq), INTENT (IN) :: fio
    REAL (pr), DIMENSION (nlocal, neq), INTENT (IN) :: vio
    INTERFACE
       FUNCTION Lulocal (jlev, u_local, nlocal, neq, meth)
         USE precision
         USE sizes
         USE pde
         IMPLICIT NONE
         INTEGER, INTENT (IN) :: jlev, nlocal, neq, meth
         REAL (pr), DIMENSION (nlocal*neq), INTENT (INOUT) :: u_local
         REAL (pr), DIMENSION (nlocal*neq) :: Lulocal
       END FUNCTION Lulocal
    end INTERFACE
    real(pr) :: c1, c2, w
    REAL (pr), DIMENSION (nlocal, neq) :: uu, res1, res0
    integer :: i

    w = 1.0_pr
    uu = uio + w*vio
    res1 = fio-reshape(Lulocal(jlev, uu, nlocal, neq, HIGH_ORDER),(/nlocal,neq/))
    res0 = fio-reshape(Lulocal(jlev, uio, nlocal, neq, HIGH_ORDER),(/nlocal,neq/))

    do while(dot_product(sum(res1(1:nlocal,1:neq),DIM=2),sum(res1(1:nlocal,1:neq),DIM=2)) > dot_product(sum(res0(1:nlocal,1:neq),DIM=2),sum(res0(1:nlocal,1:neq),DIM=2)))
       c2 = 0.5_pr*(dot_product(res1(1:nlocal,1),res0(1:nlocal,1))/dot_product(res0(1:nlocal,1),res0(1:nlocal,1)) + dot_product(res1(1:nlocal,2),res0(1:nlocal,2))/dot_product(res0(1:nlocal,2),res0(1:nlocal,2)))

       w = 2.0_pr/c2 * (dot_product(sum(res1(1:nlocal,1:neq),DIM=2),sum(res1(1:nlocal,1:neq),DIM=2))/dot_product(sum(res0(1:nlocal,1:neq),DIM=2),sum(res0(1:nlocal,1:neq),DIM=2)) - 1.0_pr)

       uu = uio + w*vio
       res1 = fio-reshape(Lulocal(jlev, uu, nlocal, neq, HIGH_ORDER),(/nlocal,neq/))
       nsearch = nsearch + 1
    end do

    uio = uio + w * vio
  end subroutine FAS_LINE_SEARCH_CORRECTION

  subroutine LINE_SEARCH_CORRECTION(uio, fio, vio, nlocal, neq, Lulocal)
    implicit none
    INTEGER,   INTENT (IN)    :: nlocal, neq
    REAL (pr), DIMENSION (nlocal*neq), INTENT (INOUT) :: uio
    REAL (pr), DIMENSION (nlocal*neq), INTENT (IN) :: fio
    REAL (pr), DIMENSION (nlocal*neq), INTENT (IN) :: vio
    INTERFACE
       FUNCTION Lulocal (jlev, u_local, nlocal, neq, meth)
         USE precision
         USE sizes
         USE pde
         IMPLICIT NONE
         INTEGER, INTENT (IN) :: jlev, nlocal, neq, meth
         REAL (pr), DIMENSION (nlocal*neq), INTENT (INOUT) :: u_local
         REAL (pr), DIMENSION (nlocal*neq) :: Lulocal
       END FUNCTION Lulocal
    end INTERFACE
    real(pr) :: c1, c2, w
    REAL (pr), DIMENSION (nlocal*neq) :: uu, res1, res0
    integer :: i

    w = 1.0_pr
    uu = uio + w*vio
    res1 = fio-Lulocal(j_lev, uu, nlocal, neq, HIGH_ORDER)
    res0 = fio-Lulocal(j_lev, uio, nlocal, neq, HIGH_ORDER)

    do while(dot_product(res1,res1) > dot_product(res0,res0))


       c2 = dot_product(res1,res0)/dot_product(res0,res0)

       w = min(6.0e-1_pr,2.0_pr/c2 * (dot_product(res1,res1)/dot_product(res0,res0) - 1.0_pr))

       uu = uio + w*vio
       res1 = fio-Lulocal(j_lev, uu, nlocal, neq, HIGH_ORDER)
       nsearch = nsearch + 1
       write(6,'(5x,"LINE SEARCH",1x,i3,1x,"alpha=",es7.0,1x,"residual=",es7.0)') nsearch, w, maxval(abs(res1))
    end do

    uio = uio + w * vio
  end subroutine LINE_SEARCH_CORRECTION
  !
  !
  !
  PURE FUNCTION i_clip (j) 
    IMPLICIT NONE 
    INTEGER, INTENT (IN):: j
    INTEGER :: i_clip

    IF (MINVAL(prd) == 1) THEN  ! all directions periodic 
       i_clip = 1
    ELSE ! atleast one direction is non-periodic
       i_clip = Nwlt_lev(j,0) + 1 + mxyz(2)/2
    END IF


  END FUNCTION i_clip

END MODULE nonlinear

