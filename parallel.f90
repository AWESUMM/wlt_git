!-------------------------------------------------------!
! define whether to use C or Fortran style node pointer !
#ifdef TREE_NODE_POINTER_STYLE_C
#undef TREE_NODE_POINTER_STYLE_C
#endif
#ifdef TREE_NODE_POINTER_STYLE_F
#undef TREE_NODE_POINTER_STYLE_F
#endif
#if ((TREE_VERSION == 0) || (TREE_VERSION == 1))
#define TREE_NODE_POINTER_STYLE_C
#elif ((TREE_VERSION == 2) || (TREE_VERSION == 3))
#define TREE_NODE_POINTER_STYLE_F
#else
#error Please define correct TREE_VERSION
#endif

!#if TREE_VERSION == 0
!#define TREE_VERSION_0
!#endif
!#if TREE_VERSION == 1
!#define TREE_VERSION_1
!#endif
! define whether to use C or Fortran style node pointer !
!-------------------------------------------------------!





!-------------------------------------------------------!
! option to use MPI_ISend/IRecv instead of MPI_Alltoall !
! (1) DO irecv; DO isend; waitall; waitall
#define COMM_ONE2ONE_RKL
! (2) circular isend-irecv; waitall; waitall
!#define COMM_ONE2ONE_RKL_1
!-------------------------------------------------------!







!
! Parallel processing related subroutines
! which are to be empty if compiled for a
! single processor, i.e. MULTIPROC undefined
!
MODULE parallel
#ifdef MULTIPROC
  USE user_case_db                  ! use defined there max_dim for domain_split()
  USE precision
  USE db_tree_vars                  ! c_pointer's type
#endif
  IMPLICIT NONE
#ifdef MULTIPROC
  INCLUDE 'mpif.h'                  ! MPI subroutines
#endif

  PUBLIC :: parallel_init,               & ! initialize module
       parallel_finalize,                & ! deallocate the module
       parallel_domain_decompose,        & ! distribute domain among processors
       parallel_comm,                    & ! (wavelet transform)
       parallel_comm_bnd,                & ! (res2vis boundary nodes)
       parallel_comm_wgh,                & ! (weights)
       parallel_comm_adj,                & ! (adjacent_wlt_DB)
       parallel_global_sum,              & ! (indices_tree, etc)
       parallel_vector_sum,              & ! (indices_tree, etc)
       parallel_broadcast,               & ! broadcast 0 proc value to all others
       parallel_broadcast_int,           & ! broadcast 0 proc value to all others
       parallel_broadcast_int_array,     & ! broadcast 0 proc values to all others
       parallel_broadcast_int_1D_array,  & ! broadcast 0 proc values to all others
       parallel_broadcast_char,          & ! broadcast 0 proc values to all others
       parallel_gather,                  & ! gather values to proc 0
       parallel_set_files_to_read,       & ! set file names to read in parallel restart (io_3d)
       parallel_gather_rank,             &                                     
       test_error,                       & ! terminate if nonzero input
       parallel_sync                       ! synchronize across the communicator

  INTEGER, ALLOCATABLE :: par_proc_tree(:), &      ! to which processor each tree belongs - GLOBAL -
       Nwlt_per_Tree(:), &                         ! active wavelets per tree on current processor - LOCAL -
       IC_par_proc_tree(:), &                      !  same for .res file (may be different)
       IC_Nwlt_per_Tree(:), &                      ! same for .res file (may be different) !OLEG: 07.14.2011 for beter load balancing of restart files
       procbuffer(:)                               ! buffers of length of number of processors
  
  INTEGER, PARAMETER :: par_LENSTR1 = 8            ! length of par_rank_str
  CHARACTER (LEN=par_LENSTR1) :: par_rank_str      ! string representation of par_rank (p0, p1, ..., p10, p11, etc.)
  INTEGER :: IC_par_size, &                        ! par_size in .res file (may differ from par_size)
       n_transfer_send, &           ! number of sig/adj nodes to be sent to others by the current processor
       n_transfer_recv              ! number of sig/adj nodes to be received by the current processor
  

  PRIVATE :: &
       pretty_print_array                          ! pretty print array


  !=========================!
  ! PARALLEL MODE VARIABLES !
  !=========================!
#ifdef MULTIPROC
  PUBLIC
  INTEGER :: par_rank,                           & ! processor's number \in [0..par_size-1]
       par_size,                                 & ! total number of processors
       domain_split(max_dim),                    & ! [0/1] prevent/allow splitting of that direction
       domain_div(max_dim),                      & ! natural number suggests number of divisions in that directions
       dim_index(max_dim),                       & ! split direction order (default 3,2,1)
       domain_meth,                              & ! domain decomposition method
       domain_zoltan_verb                          ! Zoltan debug level (0-10)
  REAL(pr) :: domain_imbalance_tol(1:3)            ! maximum imbalance (% of MIN if MAX=100%) (par,rep,ref)
  
  LOGICAL :: domain_debug, &          ! Set true and run interactively on single processor to test domain decomposition
       IC_repartition            ! Flag to allow repartition (or refine) during adaptation to initial conditions.
  !                              ! Beware that repartitioning is changing the grid and therefore adapt grid will not
  !                              ! detect (new_grid -> false) termination of adaptation to IC cycle and run extra
  !                              ! iterations. If it is an issue, override default TRUE for that flag in your .inp file.
  
  INTEGER :: interpolation_adj ! parameter to control the interpolation zone setup at j_mn in adjacent_wlt_DB for a parallel run
  !                            !  0  - (default) adj points at j_mn are only added to the processor itself and not to the boundary zone
  !                            !  1  - ... are added at upper-rightmost side of every adj point at j_mn belonging to the current processor
  !                            !  2  - ... at all sides (+1 and -1) from point belonging to the current processor
  !                            !  3  - ... (+2 +1, -1)
  !                            !  4  - ... (+2, +1, -1, -2), etc.
  !                            ! -1  - all the points are added at j_mn level as adjacent
  
  INTEGER, ALLOCATABLE, TARGET :: &         
       procbuffer_to_send(:,:),                  & ! number of such nodes (lev,proc)
       procbuffer_to_recv(:,:),                  &
       procbuffer_to_send_gho(:,:),              &
       procbuffer_to_recv_gho(:,:)

  INTEGER*8, ALLOCATABLE, TARGET :: nodebuffer_to_send(:), &  ! 1D index of nodes to be sent        
                                    nodebuffer_to_recv(:), &  ! and received from other processors  
                                    nodebuffer_to_send_gho(:), &                                    
                                    nodebuffer_to_recv_gho(:)                                       


  INTEGER(pointer_pr), ALLOCATABLE :: nodeptrbuffer_to_send(:)

  REAL(pr), ALLOCATABLE, TARGET :: valuebuffer_to_recv(:), &  ! values to transfer
       valuebuffer_to_send(:)
  LOGICAL, ALLOCATABLE :: boolbuffer_to_recv_gho(:), &
       boolbuffer_to_send_gho(:)
  
  PUBLIC :: parallel_comm_mlfc,               & ! make_lists_for_communications() - sig/gho
       parallel_comm_rkl                        ! request_known_list()
  
  PRIVATE :: parallel_domain_decompose_1,     & ! fair fat tree distribution
       parallel_domain_decompose_zoltan,      & ! Zoltan library based
       parallel_domain_decompose_10,          & ! simple, based on tree (fat tree) number only
       parallel_domain_decompose_2,           & ! 
       parallel_subdivide_bin,                &
       parallel_read_IC,                      &
       sync_arr_per_tree,                     & ! Synchronize (small) array accross the processors
       parallel_domain_imbalanced               ! report if domain is imbalanced (according to domain_imbalance_tol)

#else
  !=======================!
  ! SERIAL MODE VARIABLES !
  !=======================!
  INTEGER, PARAMETER, PUBLIC :: par_rank = 0, par_size = 1, domain_meth = 0
#endif

  
  ! DEBUG: local output inside communicators
!!$#define COMMDEBUG_comm_mlfc
!!$##define COMMDEBUG_comm_rkl
!!$#define COMMDEBUG_comm_rkl_gho
!!$#define COMMDEBUG_comm
!!$#define COMMDEBUG_comm_adj
!!$#define COMMDEBUG_comm_wgh
!!$#define COMMDEBUG_comm_bnd
!!$#define INTEGER8_DEBUG_AR  
!!$#define COMMDEBUG_MPIALLTOALL
#define ICSETFILETOREAD_DEBUG  

CONTAINS
  !===========================!
  ! PARALLEL MODE SUBROUTINES !
  !===========================!
#ifdef MULTIPROC
  !-----------------------------------------------------------
  !  Naming conventions for MPI subroutines
  !  This is an analog to C++/lowercase.h in order to get around
  !  underscore related compiler issues.
  !
  !  Use flag -DDATABASE_MPI_APPEND_UNDERSCORE in the correspondent
  !  makefile in makefiles.machine_specific/ in order to append
  !  an underscore to MPI functions
  !  (e.g. for the use with pgi compilers which seem not to have
  !  a correspondent flag)
  !
  !  Use flag -DDATABASE_MPI_APPEND_UNDERSCORE2 in the correspondent
  !  makefile in makefiles.machine_specific/ in order to append
  !  second underscore to MPI functions
  !
#ifdef DATABASE_MPI_APPEND_UNDERSCORE
  ! append underscore to mpi subroutines
#define MPI_INIT MPI_INIT_
#define MPI_COMM_SIZE MPI_COMM_SIZE_
#define MPI_COMM_RANK MPI_COMM_RANK_
#define MPI_FINALIZE MPI_FINALIZE_
#define MPI_ALLTOALL MPI_ALLTOALL_
#define MPI_ALLTOALLV MPI_ALLTOALLV_
#define MPI_ALLREDUCE MPI_ALLREDUCE_
#define MPI_BARRIER MPI_BARRIER_
#define MPI_BCAST MPI_BCAST_
#define MPI_GATHER MPI_GATHER_
#define MPI_ISEND MPI_ISEND_
#define MPI_IRECV MPI_IRECV_
#define MPI_WAITALL MPI_WAITALL_
#define MPI_SEND MPI_SEND_
#define MPI_RECV MPI_RECV_
#endif
  !
#ifdef DATABASE_MPI_APPEND_UNDERSCORE2
  ! append second underscore to mpi subroutines
#define MPI_INIT MPI_INIT__
#define MPI_COMM_SIZE MPI_COMM_SIZE__
#define MPI_COMM_RANK MPI_COMM_RANK__
#define MPI_FINALIZE MPI_FINALIZE__
#define MPI_ALLTOALL MPI_ALLTOALL__
#define MPI_ALLTOALLV MPI_ALLTOALLV__
#define MPI_ALLREDUCE MPI_ALLREDUCE__
#define MPI_BARRIER MPI_BARRIER__
#define MPI_BCAST MPI_BCAST__
#define MPI_GATHER MPI_GATHER__
#define MPI_ISEND MPI_ISEND__
#define MPI_IRECV MPI_IRECV__
#define MPI_WAITALL MPI_WAITALL__
#define MPI_SEND MPI_SEND__
#define MPI_RECV MPI_RECV__
#endif

  !
  !
  !-----------------------------------------------------------
  ! data transfer for make_lists_for_communications() - sig/gho
  SUBROUTINE parallel_comm_mlfc (do_ghosts, n_var_comm)
    USE precision
    USE wlt_vars       ! dim, j_lev, j_mx, nxyz
    USE debug_vars     ! timers
    INTEGER, INTENT(IN) :: n_var_comm ! maximum number of variables to transmit
    LOGICAL, INTENT(IN) :: do_ghosts
    INTEGER :: ierror, proc, i, k, kk, &                            ! counters
         ixyz(1:dim), id, &                                         ! and other buffers         
         pbs(0:par_size-1), pbr(0:par_size-1), &
         pbs_gho(0:par_size-1), pbr_gho(0:par_size-1), &
         number_of_nodes_to_send, number_of_nodes_to_send_gho, &
         sdispl(0:par_size-1), rdispl(0:par_size-1)                 ! send/recv displacements
    INTEGER, PARAMETER :: pos_sig  = 0, pos_adj  = 1                ! similar to db_tree !
    
    INTEGER*8          :: i_p(0:dim)                                                            
    INTEGER*8          :: MPIALLTOALL_counter(1:3)


#ifdef COMMDEBUG_comm_mlfc
    INTEGER, PARAMETER :: DU = 555
    
    PRINT *, 'parallel_comm_mlfc()'
    OPEN (UNIT=DU,FILE='debug.parallel_comm_mlfc'//par_rank_str)
    WRITE (DU,*) 'par_rank =', par_rank
#endif
    
    !---------
    ! WARNING: could be written more efficiently
    !---------

    MPIALLTOALL_counter(:) = 0
    CALL timer_start(6)

    ! Each processor has to inform all the others how many nodes it wants from them
    pbr = j_mx
    sdispl(0) = 0
    DO proc=1, par_size-1
       sdispl(proc) = sdispl(proc-1) + j_mx
    END DO
    CALL MPI_ALLTOALLV( procbuffer_to_recv,  &
         pbr,                                &
         sdispl, MPI_INTEGER,                &
         procbuffer_to_send,                 &
         pbr,                                &
         sdispl, MPI_INTEGER,                &
         MPI_COMM_WORLD, ierror )
    MPIALLTOALL_counter(1) = MPIALLTOALL_counter(1) + SUM(sdispl(0:par_size-1))
    IF (do_ghosts) THEN
       CALL MPI_ALLTOALLV( procbuffer_to_recv_gho,  &
            pbr,                                &
            sdispl, MPI_INTEGER,                &
            procbuffer_to_send_gho,                 &
            pbr,                                &
            sdispl, MPI_INTEGER,                &
            MPI_COMM_WORLD, ierror )
       MPIALLTOALL_counter(1) = MPIALLTOALL_counter(1) + SUM(sdispl(0:par_size-1))
       pbr_gho(0:par_size-1) = SUM( procbuffer_to_recv_gho(1:j_mx,0:par_size-1), DIM=1 )
       pbs_gho(0:par_size-1) = SUM( procbuffer_to_send_gho(1:j_mx,0:par_size-1), DIM=1 )
    END IF

    ! The total numbers of nodes to receive/send are pbr/pbs (0:par_size-1)
    pbr    (0:par_size-1) = SUM( procbuffer_to_recv    (1:j_mx,0:par_size-1), DIM=1 )
    pbs    (0:par_size-1) = SUM( procbuffer_to_send    (1:j_mx,0:par_size-1), DIM=1 )

#ifdef COMMDEBUG_comm_mlfc
    WRITE (DU,*) '   procbuffer_to_recv = ',procbuffer_to_recv
    WRITE (DU,*) '   procbuffer_to_send = ',procbuffer_to_send
    WRITE (DU,*) '   pbr=',pbr(0:par_size-1)
    WRITE (DU,*) '   pbs=',pbs(0:par_size-1)
    IF (do_ghosts) THEN
       WRITE (DU,*) '   procbuffer_to_recv_gho = ',procbuffer_to_recv_gho
       WRITE (DU,*) '   procbuffer_to_send_gho = ',procbuffer_to_send_gho
       WRITE (DU,*) '   pbr_gho=',pbr_gho(0:par_size-1)
       WRITE (DU,*) '   pbs_gho=',pbs_gho(0:par_size-1)
    END IF
#endif
    
    number_of_nodes_to_send_gho = 0
    
    ! Allocate space for nodes to be sent to other processors (for j_mx)
    
    IF (do_ghosts) THEN
       number_of_nodes_to_send_gho = SUM(pbs_gho(0:par_size-1))    
       IF (ALLOCATED(nodebuffer_to_send_gho) .AND. SIZE(nodebuffer_to_send_gho).LT.number_of_nodes_to_send_gho ) THEN
          DEALLOCATE( nodebuffer_to_send_gho )
          DEALLOCATE( boolbuffer_to_send_gho )
       END IF
       IF (.NOT.ALLOCATED(nodebuffer_to_send_gho) ) THEN
          ALLOCATE( nodebuffer_to_send_gho(number_of_nodes_to_send_gho) )
          ALLOCATE( boolbuffer_to_send_gho(number_of_nodes_to_send_gho) )
       END IF
       nodebuffer_to_send_gho = 0
       boolbuffer_to_send_gho = .FALSE.
    END IF
    
    number_of_nodes_to_send     = SUM(pbs    (0:par_size-1))
    IF (ALLOCATED(nodebuffer_to_send) .AND. SIZE(nodebuffer_to_send).LT.number_of_nodes_to_send+number_of_nodes_to_send_gho ) THEN
       DEALLOCATE( nodebuffer_to_send )
       DEALLOCATE( nodeptrbuffer_to_send )
       DEALLOCATE( valuebuffer_to_send )
    END IF
    IF (.NOT.ALLOCATED(nodebuffer_to_send) ) THEN
       ALLOCATE( nodebuffer_to_send(number_of_nodes_to_send+number_of_nodes_to_send_gho) )
       ALLOCATE( nodeptrbuffer_to_send(number_of_nodes_to_send+number_of_nodes_to_send_gho) )
       ALLOCATE( valuebuffer_to_send((number_of_nodes_to_send+number_of_nodes_to_send_gho)*n_var_comm) )
    END IF
    nodebuffer_to_send = 0
    nodeptrbuffer_to_send = 0
    valuebuffer_to_send = 0.0_pr
    
#ifdef COMMDEBUG_comm_mlfc
    WRITE (DU,*) 'number_of_nodes_to_send     =',number_of_nodes_to_send
    IF (do_ghosts) WRITE (DU,*) 'number_of_nodes_to_send_gho =',number_of_nodes_to_send_gho
#endif

    ! Redistribure 1D index among the processors
    sdispl(0) = 0
    rdispl(0) = 0
    DO proc=1, par_size-1
       sdispl(proc) = sdispl(proc-1) + pbr(proc-1)
       rdispl(proc) = rdispl(proc-1) + pbs(proc-1)
    END DO
    CALL MPI_ALLTOALLV( nodebuffer_to_recv,  &
         pbr,                                &
         sdispl, MPI_INTEGER8,               &                                                     
         nodebuffer_to_send,                 &
         pbs,                                &
         rdispl, MPI_INTEGER8,               &                                                     
         MPI_COMM_WORLD, ierror )
    MPIALLTOALL_counter(2) = MPIALLTOALL_counter(2) + SUM(sdispl(0:par_size-1))
    
    IF (do_ghosts) THEN
       sdispl(0) = 0
       rdispl(0) = 0
       DO proc=1, par_size-1
          sdispl(proc) = sdispl(proc-1) + pbr_gho(proc-1)
          rdispl(proc) = rdispl(proc-1) + pbs_gho(proc-1)
       END DO
       CALL MPI_ALLTOALLV( nodebuffer_to_recv_gho,  &
            pbr_gho,                                &
            sdispl, MPI_INTEGER8,                   &                                            
            nodebuffer_to_send_gho,                 &
            pbs_gho,                                &
            rdispl, MPI_INTEGER8,                   &                                            
            MPI_COMM_WORLD, ierror )
       MPIALLTOALL_counter(2) = MPIALLTOALL_counter(2) + SUM(sdispl(0:par_size-1))
    END IF
    
#ifdef COMMDEBUG_comm_mlfc
    WRITE (DU,*) 'nodebuffer_to_recv',nodebuffer_to_recv
    WRITE (DU,*) 'nodebuffer_to_send',nodebuffer_to_send
    IF (do_ghosts) WRITE (DU,*) 'nodebuffer_to_recv_gho',nodebuffer_to_recv_gho
    IF (do_ghosts) WRITE (DU,*) 'nodebuffer_to_send_gho',nodebuffer_to_send_gho
    IF (.NOT.do_ghosts) CLOSE(DU)
#endif

    IF (.NOT.do_ghosts) RETURN


    ! ghost extra:
    ! find out which of the requested ghosts are sig and inform other processors
    i_p(0) = 1
    DO i=1,dim
       i_p(i) = i_p(i-1)*(mxyz(i)*2**(j_mx-1) + 1)
    END DO
    
    DO i=1,number_of_nodes_to_send_gho
       ixyz(1:dim) = INT(MOD(nodebuffer_to_send_gho(i)-1,i_p(1:dim))/i_p(0:dim-1))
       CALL DB_get_id_by_jmax_coord_or_zero( ixyz, id )
       IF (BTEST(id,pos_sig).OR.BTEST(id,pos_adj)) THEN
          boolbuffer_to_send_gho(i) = .TRUE.
       END IF
#ifdef COMMDEBUG_comm_mlfc
       WRITE (DU,*) 'n/ixyz/bool=',nodebuffer_to_send_gho(i),ixyz(1:dim),boolbuffer_to_send_gho(i)
#endif
    END DO
    CALL MPI_ALLTOALLV( boolbuffer_to_send_gho,  &
         pbs_gho,                                &
         rdispl, MPI_LOGICAL,                &
         boolbuffer_to_recv_gho,                 &
         pbr_gho,                                &
         sdispl, MPI_LOGICAL,                &
         MPI_COMM_WORLD, ierror )
    MPIALLTOALL_counter(3) = MPIALLTOALL_counter(3) + SUM(sdispl(0:par_size-1))


#ifdef COMMDEBUG_comm_mlfc
    WRITE (DU,*) 'boolbuffer_to_recv_gho=',boolbuffer_to_recv_gho
    WRITE (DU,*) 'boolbuffer_to_send_gho=',boolbuffer_to_send_gho
    CLOSE(DU)
#endif
    

    CALL timer_stop(6)



#ifdef INTEGER8_DEBUG_AR
    WRITE (*,'(A, I4, A, 4I12)')                'parallel_comm_mlfc  par_rank=', par_rank, ' i_p=', i_p
    WRITE (*,'(A, I4, A, 3I5, A, 3I4, A, 3I5)') 'parallel_comm_mlfc  par_rank=', par_rank, ' ixyz=', ixyz, ' mxyz=', mxyz, ' nxyz=', nxyz  

    IF(ALLOCATED(nodebuffer_to_send))     WRITE (*,'(A, I4, A, I12)')  'parallel_comm_mlfc  par_rank=', par_rank, ' MAXVAL(nodebuffer_to_send(:))    =', MAXVAL(nodebuffer_to_send(:))
    IF(ALLOCATED(nodebuffer_to_recv))     WRITE (*,'(A, I4, A, I12)')  'parallel_comm_mlfc  par_rank=', par_rank, ' MAXVAL(nodebuffer_to_recv(:))    =', MAXVAL(nodebuffer_to_recv(:))
    IF(ALLOCATED(nodebuffer_to_send_gho)) WRITE (*,'(A, I4, A, I12)')  'parallel_comm_mlfc  par_rank=', par_rank, ' MAXVAL(nodebuffer_to_send_gho(:))=', MAXVAL(nodebuffer_to_send_gho(:))
    IF(ALLOCATED(nodebuffer_to_recv_gho)) WRITE (*,'(A, I4, A, I12)')  'parallel_comm_mlfc  par_rank=', par_rank, ' MAXVAL(nodebuffer_to_recv_gho(:))=', MAXVAL(nodebuffer_to_recv_gho(:))
#endif
#ifdef COMMDEBUG_MPIALLTOALL
    IF (par_rank.EQ.0) WRITE (*,'(A, 3I12)') '[parallel_comm_mlfc]          MPI_ALLTOALL counter for 32b-INTEGER/64b-INTEGER/LOGICAL : ', MPIALLTOALL_counter(1:3)
#endif
    
  END SUBROUTINE parallel_comm_mlfc
  

  
  !
  !-----------------------------------------------------------
  ! data transfer for request_known_list()
  ! though 1D index and function values have been allocated for all
  ! the levels, we transfer only the nodes up to j_in
  SUBROUTINE parallel_comm_rkl( var_to_recv, var_size, j_in )
    USE precision
    USE wlt_vars       ! dim, j_lev, j_mx, nxyz
    USE debug_vars     ! timers
    INTEGER, INTENT(IN) :: j_in                        ! request nodes un to that level
    INTEGER, INTENT(IN) :: var_size                    !
    LOGICAL, INTENT(IN) :: var_to_recv(1:var_size)     ! which variables to request
    INTEGER :: ierror, proc, i, k, kk, &               ! counters
         ixyz(1:dim), i_p(0:dim), &
         pbs(0:par_size-1), pbr(0:par_size-1), &
         tpbs(0:par_size-1), tpbr(0:par_size-1), &
         sdispl(0:par_size-1), rdispl(0:par_size-1), & ! send/recv displacements
         anvar,                                      & ! number of active variables
         starti, startkk                               ! proc record begins
    REAL(pr) :: val_tmp(1:var_size)                    ! temporal value storage
    INTEGER*8          :: MPIALLTOALL_counter(1:1)
#ifdef COMMDEBUG_comm_rkl
    INTEGER, PARAMETER :: DU = 555
    PRINT *, 'parallel_comm_rkl()'

    OPEN (UNIT=DU,FILE='debug.parallel_comm_rkl'//par_rank_str)    
    WRITE (DU,*) 'proc = ', par_rank, ', j_in = ', j_in
#endif
#if defined (COMM_ONE2ONE_RKL) || defined (COMM_ONE2ONE_RKL_1)
    INTEGER :: sreq(par_size), &
         rreq(par_size), &                      ! MPI_Request
         sstat(MPI_STATUS_SIZE,par_size), &
         rstat(MPI_STATUS_SIZE,par_size), &     ! MPI_Status
         scount, rcount, shift, proc1
#endif

    MPIALLTOALL_counter(:) = 0
    
    
    CALL timer_start(6)
    
    
    ! numbers of nodes to request from other processors have been set in
    ! make_lists_for_communications as procbuffer_to_send/recv(lev,proc)
    ! and 1D index of that nodes have been stored in nodebuffer_to_send/recv(:)
    ! buffers for transfer values have been allocated as valuebuffer_to_send/recv(:)
    
    
    ! Fill the function values vector with the values requested by other processors:
    ! The values are expected to be written as (1:anvar,1:size_node) at level j_mx.
    ! On some other level, holes occur in the valuebuffer array.

!!$    i_p(0) = 1
!!$    DO i=1,dim
!!$       i_p(i) = i_p(i-1)*(mxyz(i)*2**(j_mx-1) + 1)
!!$    END DO

    ! The total numbers of nodes to receive/send are tpbr/tpbs (0:par_size-1)
    pbr(0:par_size-1) = SUM( procbuffer_to_recv(1:j_in,0:par_size-1), DIM=1 )
    pbs(0:par_size-1) = SUM( procbuffer_to_send(1:j_in,0:par_size-1), DIM=1 )
    tpbr(0:par_size-1) = SUM( procbuffer_to_recv(1:j_mx,0:par_size-1), DIM=1 )
    tpbs(0:par_size-1) = SUM( procbuffer_to_send(1:j_mx,0:par_size-1), DIM=1 )
    sdispl(0) = 0
    rdispl(0) = 0
    DO proc=1, par_size-1
       sdispl(proc) = sdispl(proc-1) + tpbr(proc-1)
       rdispl(proc) = rdispl(proc-1) + tpbs(proc-1)
    END DO

    ! kk - running index in valuebuffer_to_send(:)
    ! i - in nodebuffer_to_send(:)
    ! valuebuffers have been allocated for n_var, we send active variables only
    anvar = COUNT( var_to_recv(1:var_size) )

    starti = 1             ! this related to the sholes, the cycles could
    startkk = 1            ! be simplified
    DO proc=0, par_size-1
       kk = startkk
       DO i=starti, starti+pbs(proc)-1

!!$          ixyz(1:dim) = INT(MOD(nodebuffer_to_send(i)-1,i_p(1:dim))/i_p(0:dim-1))
!!$          CALL DB_test_node( ixyz, j_mx, test_c_pointer )
!!$
!!$          IF (test_c_pointer .NE.nodeptrbuffer_to_send(i)) THEN
!!$             PRINT *, 'ixyz=',ixyz,'i=',i, 'proc=',par_rank
!!$             PRINT *, 'c_pointer/nodeptrbuffer_to_send(i)=',test_c_pointer, nodeptrbuffer_to_send(i)
!!$             PAUSE 'wrong test_c_pointer'
!!$          END IF

!!$          IF (nodeptrbuffer_to_send(i).NE.0) &
          CALL DB_get_function_by_pointer( nodeptrbuffer_to_send(i), 1, var_size, val_tmp )
!!$          ixyz(1:dim) = INT(MOD(nodebuffer_to_send(i)-1,i_p(1:dim))/i_p(0:dim-1))
!!$          CALL DB_get_function_by_jmax_coordinates( ixyz, 1, var_size, val_tmp )
!!$#ifdef COMMDEBUG_comm_rkl
!!$          WRITE (DU,*) 'ii=',nodebuffer_to_send(i),'ixyz=',ixyz,'f=',val_tmp
!!$#endif
          DO k=1,var_size
             IF (var_to_recv(k)) THEN
                valuebuffer_to_send(kk) = val_tmp(k)
                kk = kk + 1
             END IF
          END DO
       END DO
       starti = starti + tpbs(proc)
       startkk = startkk + tpbs(proc)*anvar !n_var
    END DO
    
#ifdef COMMDEBUG_comm_rkl
    !WRITE (DU,*) '   nodebuffer_to_send = ', nodebuffer_to_send,'of size',SIZE(nodebuffer_to_send)
    !WRITE (DU,*) '   valuebuffer_to_send = ', valuebuffer_to_send,'of size',SIZE(valuebuffer_to_send)
    WRITE (DU,*) '   anvar = ', anvar*anvar
    WRITE (DU,*) '   pbr*anvar = ', pbr*anvar, ', tpbr = ', tpbr
    WRITE (DU,*) '   pbs*anvar = ', pbs*anvar, ', tpbs = ', tpbs
    WRITE (DU,*) '   rdispl*anvar = ', rdispl*anvar
    WRITE (DU,*) '   sdispl*anvar = ', sdispl*anvar
    !WRITE (DU,*) '  *valuebuffer_to_recv = ', valuebuffer_to_recv,'of size',SIZE(valuebuffer_to_recv)
#endif
    !PAUSE 'in parallel_comm_rkl'
    
    pbs = pbs*anvar
    pbr = pbr*anvar
    rdispl = rdispl*anvar
    sdispl = sdispl*anvar

    ! Redistribute function values among the processors
!#undef COMM_ONE2ONE_RKL_1
#ifdef COMM_ONE2ONE_RKL_1
    scount = 0
    rcount = 0

    DO shift=1,par_size-1
       proc1 = MOD(par_rank+shift,par_size)
       IF ( pbr(proc1).NE.0 ) THEN
          rcount = rcount + 1
          CALL MPI_IRECV( valuebuffer_to_recv(sdispl(proc1)+1), pbr(proc1), MPI_DOUBLE_PRECISION, &
               proc1, proc1, MPI_COMM_WORLD, rreq(rcount), ierror ) !dest, tag, etc.
       END IF
       proc = MOD(par_size+par_rank-shift, par_size)
       IF ( pbs(proc).NE.0 ) THEN
          scount = scount + 1
          CALL MPI_ISEND( valuebuffer_to_send(rdispl(proc)+1), pbs(proc), MPI_DOUBLE_PRECISION, &
               proc, par_rank, MPI_COMM_WORLD, sreq(scount), ierror ) !dest, tag, etc.
       END IF
    END DO

    CALL MPI_WAITALL( scount, sreq, sstat, ierror)
    CALL MPI_WAITALL( rcount, rreq, rstat, ierror)

#elif defined COMM_ONE2ONE_RKL
    scount = 0
    rcount = 0

    DO proc=0, par_size-1
       IF ( pbr(proc).NE.0 ) THEN
          rcount = rcount + 1
          CALL MPI_IRECV( valuebuffer_to_recv(sdispl(proc)+1), pbr(proc), MPI_DOUBLE_PRECISION, &
               proc, proc, MPI_COMM_WORLD, rreq(rcount), ierror ) !dest, tag, etc.
       END IF
    END DO
    DO proc=0, par_size-1
       IF ( pbs(proc).NE.0 ) THEN
          scount = scount + 1
          CALL MPI_ISEND( valuebuffer_to_send(rdispl(proc)+1), pbs(proc), MPI_DOUBLE_PRECISION, &
               proc, par_rank, MPI_COMM_WORLD, sreq(scount), ierror ) !dest, tag, etc.
       END IF
    END DO

    CALL MPI_WAITALL( scount, sreq, sstat, ierror)
    CALL MPI_WAITALL( rcount, rreq, rstat, ierror)
    
#else !ALLTOALL (default)

    CALL MPI_ALLTOALLV( valuebuffer_to_send, & ! values the current processor will send to others
         pbs,                          &
         rdispl,                       &
         MPI_DOUBLE_PRECISION,               &
         valuebuffer_to_recv,                & ! values the current processor receives
         pbr,                          &
         sdispl,                       &
         MPI_DOUBLE_PRECISION,               &
         MPI_COMM_WORLD, ierror )
    MPIALLTOALL_counter(1) = MPIALLTOALL_counter(1) + SUM(sdispl(0:par_size-1))
#endif 

    


#ifdef COMMDEBUG_comm_rkl
    !WRITE (DU,*) '   valuebuffer_to_recv = ',valuebuffer_to_recv
    CLOSE(DU)
#endif
    !PAUSE 'in parallel_comm_rkl'


    CALL timer_stop(6)

#ifdef COMMDEBUG_MPIALLTOALL
    IF (par_rank.EQ.0) WRITE (*,'(A, 1I12)') '[parallel_comm_rkl]           MPI_ALLTOALL counter for 64b-REAL                        : ', MPIALLTOALL_counter(1:1)
#endif

  END SUBROUTINE parallel_comm_rkl
  
!!$  SUBROUTINE parallel_comm_rkl_gho( var_to_recv, j_in )
!!$    USE precision
!!$    USE wlt_vars       ! dim, j_lev, j_mx, nxyz
!!$    USE pde            ! n_var
!!$    INTEGER, INTENT(IN) :: j_in                        ! request nodes un to that level
!!$    LOGICAL, INTENT(IN) :: var_to_recv(1:n_var)        ! which variables to request
!!$    INTEGER :: ierror, proc, i, k, kk, &               ! counters
!!$         ixyz(1:dim), i_p(0:dim), &
!!$         pbs(0:par_size-1), pbr(0:par_size-1), &
!!$         tpbs(0:par_size-1), tpbr(0:par_size-1), &
!!$         sdispl(0:par_size-1), rdispl(0:par_size-1), & ! send/recv displacements
!!$         anvar,                                      & ! number of active variables
!!$         starti, startkk                               ! proc record begins
!!$    REAL(pr) :: val_tmp(1:n_var)                       ! temporal value storage
!!$#ifdef COMMDEBUG_comm_rkl
!!$    INTEGER, PARAMETER :: DU = 555
!!$    PRINT *, 'parallel_comm_rkl_gho()'
!!$
!!$    OPEN (UNIT=DU,FILE='debug.parallel_comm_rkl_gho'//par_rank_str)    
!!$    WRITE (DU,*) 'proc = ', par_rank, ', j_in = ', j_in
!!$#endif
!!$    
!!$    ! numbers of nodes to request from other processors have been set in
!!$    ! make_lists_for_communications as procbuffer_to_send/recv(lev,proc)
!!$    ! and 1D index of that nodes have been stored in nodebuffer_to_send/recv(:)
!!$    ! buffers for transfer values have been allocated as valuebuffer_to_send/recv(:)
!!$    
!!$    
!!$    ! Fill the function values vector with the values requested by other processors:
!!$    ! The values are expected to be written as (1:size_var,1:size_node)
!!$    ! coordinate conversion
!!$    i_p(0) = 1
!!$    DO i=1,dim
!!$       i_p(i) = i_p(i-1)*(mxyz(i)*2**(j_mx-1) + 1)
!!$    END DO
!!$
!!$    ! The total numbers of nodes to receive/send are tpbr/tpbs (0:par_size-1)
!!$    pbr(0:par_size-1) = SUM( procbuffer_to_recv_gho(1:j_in,0:par_size-1), DIM=1 )
!!$    pbs(0:par_size-1) = SUM( procbuffer_to_send_gho(1:j_in,0:par_size-1), DIM=1 )
!!$    tpbr(0:par_size-1) = SUM( procbuffer_to_recv_gho(1:j_mx,0:par_size-1), DIM=1 )
!!$    tpbs(0:par_size-1) = SUM( procbuffer_to_send_gho(1:j_mx,0:par_size-1), DIM=1 )
!!$    sdispl(0) = 0
!!$    rdispl(0) = 0
!!$    DO proc=1, par_size-1
!!$       sdispl(proc) = sdispl(proc-1) + tpbr(proc-1)
!!$       rdispl(proc) = rdispl(proc-1) + tpbs(proc-1)
!!$    END DO
!!$
!!$    ! kk - running index in valuebuffer_to_send(:)
!!$    ! i - in nodebuffer_to_send(:)
!!$    ! valuebuffers have been allocated for n_var, we send active variables only
!!$    anvar = COUNT( var_to_recv(1:n_var) )
!!$
!!$    starti = 1
!!$    startkk = 1
!!$    DO proc=0, par_size-1
!!$       kk = startkk
!!$       DO i=starti, starti+pbs(proc)-1
!!$          IF (boolbuffer_to_send(i)) THEN
!!$             ixyz(1:dim) = INT(MOD(nodebuffer_to_send_gho(i)-1,i_p(1:dim))/i_p(0:dim-1))
!!$             CALL DB_get_function_by_jmax_coordinates( ixyz, 1, n_var, val_tmp )
!!$          ELSE
!!$             val_tmp = 0.0_pr
!!$          END IF
!!$#ifdef COMMDEBUG_comm_rkl
!!$          WRITE (DU,*) 'ii=',nodebuffer_to_send_gho(i),'ixyz=',ixyz,'f=',val_tmp
!!$#endif
!!$          DO k=1,n_var
!!$             IF (var_to_recv(k)) THEN
!!$                valuebuffer_to_send_gho(kk) = val_tmp(k)
!!$                kk = kk + 1
!!$             END IF
!!$          END DO
!!$       END DO
!!$       starti = starti + tpbs(proc)
!!$       startkk = startkk + tpbs(proc)*n_var
!!$    END DO
!!$    
!!$#ifdef COMMDEBUG_comm_rkl
!!$    WRITE (DU,*) '   nodebuffer_to_send = ', nodebuffer_to_send_gho,'of size',SIZE(nodebuffer_to_send_gho)
!!$    WRITE (DU,*) '   valuebuffer_to_send = ', valuebuffer_to_send_gho,'of size',SIZE(valuebuffer_to_send_gho)
!!$    WRITE (DU,*) '   anvar = ', anvar*anvar
!!$    WRITE (DU,*) '   pbr*anvar = ', pbr*anvar, ', tpbr = ', tpbr
!!$    WRITE (DU,*) '   pbs*anvar = ', pbs*anvar, ', tpbs = ', tpbs
!!$    WRITE (DU,*) '   rdispl*anvar = ', rdispl*anvar
!!$    WRITE (DU,*) '   sdispl*anvar = ', sdispl*anvar
!!$    WRITE (DU,*) '  *valuebuffer_to_recv = ', valuebuffer_to_recv_gho,'of size',SIZE(valuebuffer_to_recv_gho)
!!$#endif
!!$    !PAUSE 'in parallel_comm_rkl'
!!$
!!$
!!$    ! Redistribute function values among the processors
!!$    CALL MPI_ALLTOALLV( valuebuffer_to_send_gho, & ! values the current processor will send to others
!!$         pbs*anvar,                          &
!!$         rdispl*anvar,                       &
!!$         MPI_DOUBLE_PRECISION,               &
!!$         valuebuffer_to_recv_gho,                & ! values the current processor receives
!!$         pbr*anvar,                          &
!!$         sdispl*anvar,                       &
!!$         MPI_DOUBLE_PRECISION,               &
!!$         MPI_COMM_WORLD, ierror )
!!$    
!!$    
!!$#ifdef COMMDEBUG_comm_rkl
!!$    WRITE (DU,*) '   valuebuffer_to_recv = ',valuebuffer_to_recv_gho
!!$    CLOSE(DU)
!!$#endif
!!$    !PAUSE 'in parallel_comm_rkl'
!!$
!!$  END SUBROUTINE parallel_comm_rkl_gho

  !-----------------------------------------------------------
  ! perform global gather operation
  ! (values of processors  are gathered to the processor 0)
  SUBROUTINE parallel_gather ( REALARRAYIN, REALARRAYOUT, LENGTH )
    USE precision
    INTEGER, INTENT(IN) :: LENGTH
    REAL(pr), INTENT(INOUT) :: REALARRAYIN(:), REALARRAYOUT(:)
    INTEGER :: ierror

    CALL MPI_GATHER ( REALARRAYIN, LENGTH, MPI_DOUBLE_PRECISION, &
       REALARRAYOUT, LENGTH, MPI_DOUBLE_PRECISION, &
       0, MPI_COMM_WORLD, ierror)

  END SUBROUTINE parallel_gather

  !-----------------------------------------------------------
  ! perform global broadcast operation
  ! (value of processors 0 is spreaded across all the processors)
  SUBROUTINE parallel_broadcast ( REAL )
    USE precision
    REAL(pr), INTENT(INOUT) :: REAL
    
    INTEGER :: ierror
    
    CALL MPI_BCAST ( REAL, 1, MPI_DOUBLE_PRECISION, 0, MPI_COMM_WORLD, ierror)

  END SUBROUTINE parallel_broadcast

  !-----------------------------------------------------------
  ! perform global broadcast operation
  ! (value of processors 0 is spreaded across all the processors)
  SUBROUTINE parallel_broadcast_int ( INTEGER_INOUT )
    USE precision
    INTEGER, INTENT(INOUT) :: INTEGER_INOUT
    
    INTEGER :: ierror
    
    CALL MPI_BCAST ( INTEGER_INOUT, 1, MPI_INTEGER, 0, MPI_COMM_WORLD, ierror)

  END SUBROUTINE parallel_broadcast_int

  !-----------------------------------------------------------
  ! perform global broadcast operation
  ! (values of processors 0 is spreaded across all the processors)
  SUBROUTINE parallel_broadcast_int_array ( INTEGER_ARRAY_INOUT, LENGTH )
    USE precision
    INTEGER, INTENT(INOUT) :: INTEGER_ARRAY_INOUT(:,:)
    INTEGER, INTENT(IN)    :: LENGTH
    INTEGER                :: ierror
    
    CALL MPI_BCAST ( INTEGER_ARRAY_INOUT, LENGTH, MPI_INTEGER, 0, MPI_COMM_WORLD, ierror)

  END SUBROUTINE parallel_broadcast_int_array

  !-----------------------------------------------------------
  ! perform global broadcast operation
  ! (values of processors 0 is spreaded across all the processors)
  SUBROUTINE parallel_broadcast_int_1D_array ( INTEGER_ARRAY_INOUT, LENGTH )
    USE precision
    INTEGER, INTENT(INOUT) :: INTEGER_ARRAY_INOUT(:)
    INTEGER, INTENT(IN)    :: LENGTH
    INTEGER                :: ierror
    
    CALL MPI_BCAST ( INTEGER_ARRAY_INOUT, LENGTH, MPI_INTEGER, 0, MPI_COMM_WORLD, ierror)

  END SUBROUTINE parallel_broadcast_int_1D_array

  !-----------------------------------------------------------
  ! perform global broadcast operation
  ! (values of processors 0 is spreaded across all the processors)
  SUBROUTINE parallel_broadcast_char ( CHARACTER_INOUT, LENGTH )
    USE precision
    CHARACTER (LEN=LENGTH), INTENT(INOUT) :: CHARACTER_INOUT
    INTEGER,                INTENT(IN)    :: LENGTH
    INTEGER                               :: ierror
    
    CALL MPI_BCAST ( CHARACTER_INOUT, LENGTH, MPI_CHARACTER, 0, MPI_COMM_WORLD, ierror)

  END SUBROUTINE parallel_broadcast_char

!*************** 
! This was added for the Lagrangian module to broadcast particle positions and velocities (Ryan)
  !-----------------------------------------------------------
  ! perform global broadcast operation
  ! (values of processors 0 is spreaded across all the processors)
  SUBROUTINE parallel_broadcast_2D_array ( ARRAY_INOUT, LENGTH )
    USE precision
    REAL(pr), INTENT(INOUT) :: ARRAY_INOUT(:,:)
    INTEGER, INTENT(IN)    :: LENGTH
    INTEGER                :: ierror
    
    CALL MPI_BCAST (ARRAY_INOUT, LENGTH, MPI_DOUBLE_PRECISION, 0, MPI_COMM_WORLD, ierror)

  END SUBROUTINE parallel_broadcast_2D_array
!***************

  !-----------------------------------------------------------
  ! perform global operation (across all the processors)
  ! INTEGER, REAL             - global summation
  ! LOGICALOR                 - global OR operation
  ! REALMAXVAL, INTEGERMAXVAL - get maximum value across all the processors
  ! (parameter order reflects the frequency of their occurrence)
  SUBROUTINE parallel_global_sum (REAL, INTEGER, LOGICALOR, REALMAXVAL, REALMINVAL, INTEGERMAXVAL)
    USE precision
    LOGICAL, INTENT(INOUT), OPTIONAL :: LOGICALOR
    INTEGER, INTENT(INOUT), OPTIONAL :: INTEGER, INTEGERMAXVAL
    REAL(pr), INTENT(INOUT), OPTIONAL :: REAL, REALMAXVAL, REALMINVAL
    LOGICAL :: status_recv
    INTEGER :: ierror, istatus_recv
    REAL(pr) :: rstatus_recv
    
    IF (PRESENT(REAL)) THEN
       CALL MPI_ALLREDUCE ( REAL, rstatus_recv, 1, MPI_DOUBLE_PRECISION, MPI_SUM, MPI_COMM_WORLD, ierror)
       REAL = rstatus_recv
    ELSE IF (PRESENT(INTEGER)) THEN
       CALL MPI_ALLREDUCE ( INTEGER, istatus_recv, 1, MPI_INTEGER, MPI_SUM, MPI_COMM_WORLD, ierror)
       INTEGER = istatus_recv
    ELSE IF (PRESENT(LOGICALOR)) THEN
       CALL MPI_ALLREDUCE ( LOGICALOR, status_recv, 1, MPI_LOGICAL, MPI_LOR, MPI_COMM_WORLD, ierror)
       LOGICALOR = status_recv
    ELSE IF (PRESENT(REALMAXVAL)) THEN
       CALL MPI_ALLREDUCE ( REALMAXVAL, rstatus_recv, 1, MPI_DOUBLE_PRECISION, MPI_MAX, MPI_COMM_WORLD, ierror)
       REALMAXVAL = rstatus_recv
    ELSE IF (PRESENT(REALMINVAL)) THEN
       CALL MPI_ALLREDUCE ( REALMINVAL, rstatus_recv, 1, MPI_DOUBLE_PRECISION, MPI_MIN, MPI_COMM_WORLD, ierror)
       REALMINVAL = rstatus_recv
    ELSE IF (PRESENT(INTEGERMAXVAL)) THEN
       CALL MPI_ALLREDUCE ( INTEGERMAXVAL, istatus_recv, 1, MPI_INTEGER, MPI_MAX, MPI_COMM_WORLD, ierror)
       INTEGERMAXVAL = istatus_recv
    END IF
    
  END SUBROUTINE parallel_global_sum


  !-----------------------------------------------------------
  ! perform global operation (across all the processors) for a vector
  ! INTEGER, REAL             - global summation
  ! LOGICALOR                 - global OR operation
  ! REALMAXVAL, INTEGERMAXVAL - get maximum value across all the processors
  ! (parameter order reflects the frequency of their occurrence)
  SUBROUTINE parallel_vector_sum (REAL, INTEGER,LOGICALOR,INTEGERMAXVAL,REALMAXVAL, REALMINVAL, LENGTH)
    USE precision
    INTEGER, INTENT(IN) :: LENGTH
    LOGICAL, DIMENSION(LENGTH), INTENT(INOUT), OPTIONAL :: LOGICALOR
    INTEGER, DIMENSION(LENGTH), INTENT(INOUT), OPTIONAL :: INTEGER, INTEGERMAXVAL
    REAL(pr), DIMENSION(LENGTH), INTENT(INOUT), OPTIONAL :: REAL, REALMAXVAL, REALMINVAL
    LOGICAL, DIMENSION(LENGTH) :: status_recv
    INTEGER, DIMENSION(LENGTH) :: ierror, istatus_recv
    REAL(pr), DIMENSION(LENGTH) :: rstatus_recv
    
    IF (PRESENT(REAL)) THEN
       CALL MPI_ALLREDUCE ( REAL, rstatus_recv, LENGTH, MPI_DOUBLE_PRECISION, MPI_SUM, MPI_COMM_WORLD, ierror)
       REAL = rstatus_recv
    ELSE IF (PRESENT(INTEGER)) THEN
       CALL MPI_ALLREDUCE ( INTEGER, istatus_recv, LENGTH, MPI_INTEGER, MPI_SUM, MPI_COMM_WORLD, ierror)
       INTEGER = istatus_recv
    ELSE IF (PRESENT(LOGICALOR)) THEN
       CALL MPI_ALLREDUCE ( LOGICALOR, status_recv, LENGTH, MPI_LOGICAL, MPI_LOR, MPI_COMM_WORLD, ierror)
       LOGICALOR = status_recv
    ELSE IF (PRESENT(REALMAXVAL)) THEN
       CALL MPI_ALLREDUCE ( REALMAXVAL, rstatus_recv, LENGTH, MPI_DOUBLE_PRECISION, MPI_MAX, MPI_COMM_WORLD, ierror)
       REALMAXVAL = rstatus_recv
    ELSE IF (PRESENT(REALMINVAL)) THEN
       CALL MPI_ALLREDUCE ( REALMINVAL, rstatus_recv, LENGTH, MPI_DOUBLE_PRECISION, MPI_MIN, MPI_COMM_WORLD, ierror)
       REALMINVAL = rstatus_recv
    ELSE IF (PRESENT(INTEGERMAXVAL)) THEN 
       CALL MPI_ALLREDUCE ( INTEGERMAXVAL, istatus_recv, LENGTH, MPI_INTEGER, MPI_MAX, MPI_COMM_WORLD, ierror)
       INTEGERMAXVAL = istatus_recv
    END IF
    
  END SUBROUTINE parallel_vector_sum
  !
  !-----------------------------------------------------------
  ! send given nodes to other processors and receive some unknown number of nodes
  ! (required for adjacent and ghosts)
  ! nodebuffer_recv(:) of size num_recv will be allocated inside
  SUBROUTINE parallel_comm_adj (nodebuffer, procbuffer, num, &
       nodebuffer_recv, num_recv)
    USE precision
    USE debug_vars     ! timers
    INTEGER,   INTENT(IN)  :: num, procbuffer(0:par_size-1)                                        
    INTEGER*8, INTENT(IN)  :: nodebuffer(num)                                                      
    INTEGER,   INTENT(OUT) :: num_recv
    INTEGER*8, POINTER     :: nodebuffer_recv(:)                                                   
    INTEGER :: buff(0:par_size-1), sdispl(0:par_size-1), rdispl(0:par_size-1), proc, &
         ierror
    INTEGER*8          :: MPIALLTOALL_counter(1:1)
#ifdef COMMDEBUG_comm_adj
    INTEGER, PARAMETER :: DU = 555
    PRINT *,'parallel_comm_adj()'
#endif

    MPIALLTOALL_counter(:) = 0
    

    CALL timer_start(6)


    ! Each processor informs the others how many nodes it wants to send them
    CALL MPI_ALLTOALL( procbuffer, 1, MPI_INTEGER, buff, 1, MPI_INTEGER, &
         MPI_COMM_WORLD, ierror )
    
#ifdef COMMDEBUG_comm_adj
    OPEN (UNIT=DU,FILE='debug.parallel_comm_adj'//par_rank_str)      
    WRITE (DU,*) '--- debug'//TRIM(par_rank_str)//' ---'
    WRITE (DU,*) 'I want to send', procbuffer
    WRITE (DU,*) 'I will receive', buff,' total=',SUM(buff(0:par_size-1))
#endif
    
    ! space for the nodes to be received from other processors
    num_recv = SUM(buff(0:par_size-1))
    ALLOCATE( nodebuffer_recv(num_recv) )
    
    ! Redistribure 1D index and values among the processors
    sdispl(0) = 0
    rdispl(0) = 0
    DO proc=1, par_size-1
       sdispl(proc) = sdispl(proc-1) + procbuffer(proc-1)
       rdispl(proc) = rdispl(proc-1) + buff(proc-1)
    END DO

#ifdef COMMDEBUG_comm_adj
    WRITE (DU,*) 'rdispl=',rdispl,'sdispl=',sdispl
    WRITE (DU,*) 'I want to send the values:',nodebuffer
#endif
    
    CALL MPI_ALLTOALLV( nodebuffer, procbuffer, sdispl, MPI_INTEGER8, &                        
         nodebuffer_recv, buff, rdispl, MPI_INTEGER8, MPI_COMM_WORLD, ierror )                 
    MPIALLTOALL_counter(1) = MPIALLTOALL_counter(1) + SUM(sdispl(0:par_size-1))
    
#ifdef COMMDEBUG_comm_adj
    WRITE (DU,*) 'received values:',nodebuffer_recv
    CLOSE (DU)
#endif


    CALL timer_stop(6)

#ifdef INTEGER8_DEBUG_AR
    IF (num_recv.EQ.0) THEN !=================================================================
       WRITE (*,'(A, I10, A, I12)')          'parallel_comm_adj  par_rank=', par_rank, ' num_recv=', num_recv 
       WRITE (*,'(A, I10, A, I12)')          'parallel_comm_adj  par_rank=', par_rank, ' num_recv=', num_recv   
    ELSE IF (num_recv.GT.0) THEN !============================================================
       WRITE (*,'(A, I10, A, I12, A, I)')  'parallel_comm_adj  par_rank=', par_rank, ' num_recv=', num_recv, '   MAXVAL(nodebuffer(:))       =', MAXVAL(nodebuffer(:))
       WRITE (*,'(A, I10, A, I12, A, I)')  'parallel_comm_adj  par_rank=', par_rank, ' num_recv=', num_recv, '   MAXVAL(nodebuffer_recv(:))  =', MAXVAL(nodebuffer_recv(:))
    END IF !==============================================================================
#endif
#ifdef COMMDEBUG_MPIALLTOALL
    IF (par_rank.EQ.0) WRITE (*,'(A, 1I12)') '[parallel_comm_adj]           MPI_ALLTOALL counter for 64b-INTEGER                     : ', MPIALLTOALL_counter(1:1)
#endif
    

  END SUBROUTINE parallel_comm_adj
  !
  !-----------------------------------------------------------
  ! send given nodes to other processors and receive some unknown number of nodes
  ! (required for weights)
  ! nodebuffer_recv(:) and valuebuffer_recv(:) of size num_recv will be allocated inside
  SUBROUTINE parallel_comm_wgh (nodebuffer, valuebuffer, procbuffer, num, &
       nodebuffer_recv, valuebuffer_recv, num_recv)
    USE precision
    USE debug_vars     ! timers
    INTEGER, INTENT(IN)   :: num, procbuffer(0:par_size-1)      
    INTEGER*8, INTENT(IN) :: nodebuffer(num)                    
    REAL(pr), INTENT(IN) :: valuebuffer(num)
    INTEGER, INTENT(OUT) :: num_recv
    INTEGER*8, POINTER :: nodebuffer_recv(:)      
    REAL(pr), POINTER :: valuebuffer_recv(:)
    INTEGER :: buff(0:par_size-1), sdispl(0:par_size-1), rdispl(0:par_size-1), proc, &
         ierror
    INTEGER*8          :: MPIALLTOALL_counter(1:2)
#ifdef COMMDEBUG_comm_wgh
    INTEGER, PARAMETER :: DU = 555
    PRINT *, 'parallel_comm_wgh()'
#endif

    MPIALLTOALL_counter(:) = 0
    

    CALL timer_start(6)



#ifdef INTEGER8_DEBUG_AR
#ifdef PTR_INTEGER8                                                                                               
  WRITE (*,'(A, I2, A, I2)') 'db_tree_vars.f90     PTR_INTEGER8 :: INTEGER*8  pointer   /   pointer_pr=', pointer_pr, '   KIND(POINTER_TYPE)=',KIND(POINTER_TYPE)  
#else                                                                                                            
  WRITE (*,'(A, I2, A, I2)') 'db_tree_vars.f90     NO PTR_INTEGER8 :: INTEGER*4  pointer   /   pointer_pr=', pointer_pr, '   KIND(POINTER_TYPE)=',KIND(POINTER_TYPE) 
#endif                                                                                                           
#endif                                                                                                           



    ! Each processor informs the others how many trees it wants to send them
    CALL MPI_ALLTOALL( procbuffer, 1, MPI_INTEGER, buff, 1, MPI_INTEGER, &
         MPI_COMM_WORLD, ierror )

#ifdef COMMDEBUG_comm_wgh
    OPEN (UNIT=DU,FILE='debug.parallel_comm_wgh'//par_rank_str)      
    WRITE (DU,*) '--- debug'//TRIM(par_rank_str)//' ---'
    WRITE (DU,*) 'I want to send', procbuffer
    WRITE (DU,*) 'I will receive', buff,' total=',SUM(buff(0:par_size-1))
#endif
    
    ! space for the nodes to be received from other processors
    num_recv = SUM(buff(0:par_size-1))
    ALLOCATE( nodebuffer_recv(num_recv), valuebuffer_recv(num_recv) )
    
    ! ---WARNING ---
    ! it cound be combined into a single mpi_type
    ! we also don't have to use global communications
    ! isend/recv could be more optimal
    ! ---WARNING ---
    
    ! Redistribure 1D index and values among the processors
    sdispl(0) = 0
    rdispl(0) = 0
    DO proc=1, par_size-1
       sdispl(proc) = sdispl(proc-1) + procbuffer(proc-1)
       rdispl(proc) = rdispl(proc-1) + buff(proc-1)
    END DO

#ifdef COMMDEBUG_comm_wgh
    WRITE (DU,*) 'rdispl=',rdispl,'sdispl=',sdispl
    WRITE (DU,*) 'I want to send the values:',nodebuffer, valuebuffer
#endif
    
    CALL MPI_ALLTOALLV( nodebuffer, procbuffer, sdispl, MPI_INTEGER8, &               
         nodebuffer_recv, buff, rdispl, MPI_INTEGER8, MPI_COMM_WORLD, ierror )        
    CALL MPI_ALLTOALLV( valuebuffer, procbuffer, sdispl, MPI_DOUBLE_PRECISION, &
         valuebuffer_recv, buff, rdispl, MPI_DOUBLE_PRECISION, MPI_COMM_WORLD, ierror )
    MPIALLTOALL_counter(1) = MPIALLTOALL_counter(1) + SUM(sdispl(0:par_size-1))
    MPIALLTOALL_counter(2) = MPIALLTOALL_counter(1)
    
#ifdef COMMDEBUG_comm_wgh
    WRITE (DU,*) 'received values:',nodebuffer_recv, valuebuffer_recv
    CLOSE (DU)
#endif


    CALL timer_stop(6)
    

#ifdef INTEGER8_DEBUG_AR
    WRITE (*,'(A, I4, A, I12)')  'parallel_comm_wgh  par_rank=', par_rank, ' MAXVAL(nodebuffer(:))       =', MAXVAL(nodebuffer(:))
    WRITE (*,'(A, I4, A, I12)')  'parallel_comm_wgh  par_rank=', par_rank, ' MAXVAL(nodebuffer_recv(:))  =', MAXVAL(nodebuffer_recv(:))
    WRITE (*,'(A, I4, A, F)')    'parallel_comm_wgh  par_rank=', par_rank, ' MAXVAL(valuebuffer(:))      =', MAXVAL(valuebuffer(:))
    WRITE (*,'(A, I4, A, F)')    'parallel_comm_wgh  par_rank=', par_rank, ' MAXVAL(valuebuffer_recv(:)) =', MAXVAL(valuebuffer_recv(:))
#endif
#ifdef COMMDEBUG_MPIALLTOALL
    IF (par_rank.EQ.0) WRITE (*,'(A, 2I12)') '[parallel_comm_wgh]           MPI_ALLTOALL counter for 64b-INTEGER/64b-REAL            : ', MPIALLTOALL_counter(1:2)
#endif

  END SUBROUTINE parallel_comm_wgh
  !
  !-----------------------------------------------------------
  ! communicate to other processors to get the unknown number of boundary nodes
  ! (required for res2vis visualization)
  ! add_index(:) and add_u(:,:) will be allocated inside
  SUBROUTINE parallel_comm_bnd ( add_index, add_u, add_size, tree_to_recv, tr_size, buff, n_var )
    USE precision
    USE wlt_vars       ! dim, j_lev, j_mx, nxyz
    USE debug_vars     ! timers
    INTEGER*8, POINTER :: add_index(:)  ! 1D coord of boundary nodes from other processors       
    REAL(pr), POINTER :: add_u(:,:)   ! u value of the nodes (1:n_var,iwlt)
    INTEGER, INTENT(OUT) :: add_size  ! total number of the nodes
    INTEGER, INTENT(IN) :: n_var, tr_size                    ! n_var, number of trees to receive
    INTEGER, INTENT(IN) :: tree_to_recv(1:2*tr_size)         ! mask for trees the processor wants (tree number,direction)
    INTEGER, INTENT(IN) :: buff(0:par_size-1)                ! per processor number of trees to request
    INTEGER :: buff1(0:par_size-1), &
         sdispl(0:par_size-1), rdispl(0:par_size-1), &
         proc, i, j, size_to_send, buff2(0:par_size-1), &
         buff3(0:par_size-1), hs, hs1, hsa, ierror
    INTEGER,   ALLOCATABLE :: recv_buff(:), send_buff(:), num_nodes(:)       
    INTEGER*8, ALLOCATABLE :: index_to_send(:)                               
    
    REAL(pr), ALLOCATABLE :: value_to_send(:,:), value_to_send_tmp(:,:)  ! value_to_send_tmp 
    INTEGER*8          :: MPIALLTOALL_counter(1:3)
    INTEGER            :: n_var_allocate
#ifdef COMMDEBUG_comm_bnd
    INTEGER, PARAMETER :: DU = 555
    PRINT *, 'parallel_comm_bnd()'
#endif


    MPIALLTOALL_counter(:) = 0
    

    CALL timer_start(6)


    ! Each processor informs the others how many trees it wants from them
    CALL MPI_ALLTOALL( buff, 1, MPI_INTEGER, buff1, 1, MPI_INTEGER, &
         MPI_COMM_WORLD, ierror )
    
    ! --- WARNING ---
    ! from that point we do not have to use global communications
    ! isend/recv could be more optimal
    ! --- WARNING ---
    
    hs = SUM(buff(0:par_size-1))
    hs1 = SUM(buff1(0:par_size-1))
    
    ALLOCATE( send_buff(2*hs1) )             ! tree numbers to be sent to other processors
    ALLOCATE( recv_buff(2*hs) )              ! ... to be received from ...
    
    recv_buff(1:2*tr_size) = tree_to_recv(1:2*tr_size)
!!$    DO i=1,tr_size
!!$       recv_buff(2*i-1) = tree_to_recv( 2*i-1 )  ! write tree numbers the processor wants
!!$       recv_buff(2*i) = tree_to_recv( 2*i )    ! ... tree direction
!!$    END DO
    
    ! Each processor informs the others which trees it wants from them
    sdispl(0) = 0
    rdispl(0) = 0
    DO proc=1, par_size-1
       sdispl(proc) = sdispl(proc-1) + buff(proc-1)
       rdispl(proc) = rdispl(proc-1) + buff1(proc-1)
    END DO
    CALL MPI_ALLTOALLV( recv_buff,     & ! tree numbers the current processor wants from others
         2*buff,                       & ! number of such nodes ...
         2*sdispl, MPI_INTEGER,        & ! displacements ...
         send_buff,                    & ! tree numbers the current processor has to send
         2*buff1,                      & ! number of such nodes ...
         2*rdispl, MPI_INTEGER,        & ! displacements ...
         MPI_COMM_WORLD, ierror )
    MPIALLTOALL_counter(1) = MPIALLTOALL_counter(1) + 2*SUM(sdispl(0:par_size-1))
    
    
    ! Each processor has to find out how many nodes are in the requested trees and inform the recipients
    ! send/recv_buff consist of pairs (tree number, direction) - MODE 1
    ALLOCATE( num_nodes(SUM(buff1(0:par_size-1))) )
    CALL DB_count_tree_nodes( send_buff, SUM(buff1(0:par_size-1)), num_nodes, 1 )
    
#ifdef COMMDEBUG_comm_bnd
    OPEN (UNIT=DU,FILE='debug.parallel_comm_bnd'//par_rank_str)      
    WRITE (DU,*) '--- debug'//TRIM(par_rank_str)//' ---'
    WRITE (DU,*) 'I want so many trees     = ', buff
    WRITE (DU,*) 'others want so many trees= ', buff1
    WRITE (DU,*) 'I want trees     =',recv_buff
    WRITE (DU,*) 'others want trees=',send_buff
    WRITE (DU,*) 'in which num_nodes=',num_nodes,'total=',SUM(num_nodes)
#endif

    ! Eeach processor gets the number of nodes it want from the others
    CALL MPI_ALLTOALLV( num_nodes,     & ! number of nodes in each tree others want from the current processor
         buff1,                        & !
         rdispl, MPI_INTEGER,          & !
         recv_buff,                    & ! number of nodes the current processor want from the others
         buff,                         & !
         sdispl, MPI_INTEGER,          & !
         MPI_COMM_WORLD, ierror )
    MPIALLTOALL_counter(1) = MPIALLTOALL_counter(1) + SUM(sdispl(0:par_size-1))
    
#ifdef COMMDEBUG_comm_bnd
    WRITE (DU,*) 'So, now I want nodes:', recv_buff(1:hs),'total=',SUM(recv_buff(1:hs))
    WRITE (DU,*) 'buff = ', buff
    WRITE (DU,*) 'buff1= ', buff1
#endif
    

    ! The nodes the processor will receive
    add_size = SUM(recv_buff(1:hs))
    ALLOCATE( add_index(add_size), add_u(1:n_var,add_size) )
    ! Values for the nodes the processor will send has to be written
    size_to_send = SUM(num_nodes)
    ALLOCATE( index_to_send(size_to_send), value_to_send(1:n_var,size_to_send) )

    CALL DB_get_n_values ( n_var_allocate )
    ALLOCATE( value_to_send_tmp(1:n_var_allocate,size_to_send) )     ! DB_write_tree_nodes writes full number of vars in DB 
    ! index_to_send in MODE 1 is ionteger, 1D coord
    CALL DB_write_tree_nodes( send_buff, SUM(buff1(0:par_size-1)), num_nodes, index_to_send, value_to_send_tmp, 1 ) ! Writes n_var_DB to output values
    value_to_send(1:n_var,1:size_to_send) = value_to_send_tmp(1:n_var,1:size_to_send)        ! only need n_var variables
    DEALLOCATE(value_to_send_tmp)
 
#ifdef COMMDEBUG_comm_bnd
    WRITE (DU,*) 'I will send index=',index_to_send !,'and values=',value_to_send
#endif
    
    ! Eeach processor gets 1D index it wanted from the others
    buff2(0:par_size-1) = 0 ! number of nodes the current processor wants from the others
    buff3(0:par_size-1) = 0 ! number of nodes the current processor will send
    i = 0
    j = 0
    DO proc = 0, par_size-1
       IF (buff(proc).GT.0) THEN  ! we want buff(proc) trees from that processor
          buff2(proc) = buff2(proc) + SUM( recv_buff( i+1:i+buff(proc) ) )
          i = i + buff(proc)
       END IF
       IF (buff1(proc).GT.0) THEN
          buff3(proc) = buff3(proc) + SUM( num_nodes( j+1:j+buff1(proc) ) )
          j = j + buff1(proc)
       END IF
    END DO
    sdispl(0) = 0
    rdispl(0) = 0
    DO proc=1, par_size-1
       sdispl(proc) = sdispl(proc-1) + buff2(proc-1)
       rdispl(proc) = rdispl(proc-1) + buff3(proc-1)
    END DO

#ifdef COMMDEBUG_comm_bnd
    WRITE (DU,*) 'I will receive nodes:', buff2, 'SIZE(add_index)=', SIZE(add_index)
    WRITE (DU,*) 'I will send nodes:   ', buff3, 'SIZE(index_to_send)=', SIZE(index_to_send)
    WRITE (DU,*) 'sdispl=',sdispl(0:par_size-1)
    WRITE (DU,*) 'rdispl=',rdispl(0:par_size-1)
#endif

    CALL MPI_ALLTOALLV( index_to_send, buff3, rdispl, MPI_INTEGER8, &                      
         add_index, buff2, sdispl, MPI_INTEGER8, MPI_COMM_WORLD, ierror )                  
    MPIALLTOALL_counter(2) = MPIALLTOALL_counter(2) + SUM(sdispl(0:par_size-1))



#ifdef INTEGER8_DEBUG_AR
    WRITE (*,'(A, I4, A, I12)')    'parallel_comm_bnd  par_rank=', par_rank, ' MAXVAL(index_to_send(:))  =', MAXVAL(index_to_send(:))
    WRITE (*,'(A, I4, A, I12)')    'parallel_comm_bnd  par_rank=', par_rank, ' MAXVAL(add_index(:))      =', MAXVAL(add_index(:))
#endif
    
#ifdef COMMDEBUG_comm_bnd
    WRITE (DU,*) 'my received 1D index:',add_index
#endif    
    
    ! Eeach processor gets values it wanted from the others
    CALL MPI_ALLTOALLV( value_to_send,       & ! values the current processor will send to others
         buff3*n_var,                       &
         rdispl*n_var,                    &
         MPI_DOUBLE_PRECISION,               &
         add_u,                      & ! values the current processor receive
         buff2*n_var,                &
         sdispl*n_var,                    &
         MPI_DOUBLE_PRECISION,               &
         MPI_COMM_WORLD, ierror )
    MPIALLTOALL_counter(3) = MPIALLTOALL_counter(3) + n_var*SUM(sdispl(0:par_size-1))
    
#ifdef COMMDEBUG_comm_bnd
    WRITE (DU,*) 'my received values:', add_u
    CLOSE (DU)
#endif

    DEALLOCATE (send_buff, recv_buff, num_nodes, index_to_send, value_to_send)
    ! add_index() and add_u() have to remain allocated after that subroutine
    

    CALL timer_stop(6)

#ifdef COMMDEBUG_MPIALLTOALL
    IF (par_rank.EQ.0) WRITE (*,'(A, 3I12)') '[parallel_comm_bnd]           MPI_ALLTOALL counter for 32b-INTEGER/64b-INTEGER/64b-REAL: ', MPIALLTOALL_counter(1:3)
#endif


  END SUBROUTINE parallel_comm_bnd
  
  !
  !-----------------------------------------------------------
  ! communicate to other processors to get the required nodes
  ! (required for wavelet transform)
  SUBROUTINE parallel_comm (index_to_recv, value_to_recv, ivalue_to_recv, var_to_recv, size_node, size_var, n_var, mode)
    USE precision
    USE wlt_vars       ! dim, j_lev, j_mx, nxyz
    USE debug_vars     ! timers
    INTEGER, INTENT(IN) :: mode                   ! transfer: 0-real, 1-IDs
    INTEGER, INTENT(IN) :: size_node, size_var, & ! array sizes
         n_var                                    ! total number of variables (global n_var)
    REAL(pr), INTENT(OUT) :: value_to_recv(1:size_var*size_node) ! values to request
    INTEGER, INTENT(OUT) :: ivalue_to_recv(1:size_var*size_node) ! integer values to request              
    LOGICAL, INTENT(IN) :: var_to_recv(1:n_var)                  ! which variables to request

    INTEGER*8, INTENT(IN)  :: index_to_recv(1:size_node)            ! 1D coord of the nodes to request      


    INTEGER :: ierror, proc, i, k, kk, &     ! counters
         ixyz(1:dim), &                                                                                     
         pbs(0:par_size-1), &
         number_of_nodes_to_send, &
         sdispl(0:par_size-1), rdispl(0:par_size-1)  ! send/recv displacements
    REAL(pr), ALLOCATABLE :: value_to_send(:)                    ! values to send
    REAL(pr) :: val_tmp(1:n_var)            ! temporal value storage
    INTEGER :: id                           ! ID
    
    INTEGER :: maxixyz                                                                                      


    INTEGER*8, ALLOCATABLE :: index_to_send(:)                     ! 1D coord to send                       
    INTEGER,   ALLOCATABLE :: ivalue_to_send(:)                    ! integer values to send                 
    INTEGER*8              :: i_p(0:dim)                                                                    

    INTEGER*8              :: MPIALLTOALL_counter(1:3)
    
#ifdef COMMDEBUG_comm
    INTEGER, PARAMETER :: DU = 555
    PRINT *, 'parallel_comm()'
#endif
    MPIALLTOALL_counter(:) = 0
    

    CALL timer_start(6)

    !PRINT *, ' FIRST  parallel_comm >> proc', par_rank,'index_to_recv',size_node,'=',index_to_recv


    ! Each processor has to inform all the others how many nodes it wants from them
    ! The numbers of nodes to receive are procbuffer(0:par_size-1)
    ! The numbers of nodes to send are pbs(0:par_size-1)
    CALL MPI_ALLTOALL( procbuffer, 1, MPI_INTEGER, pbs, 1, MPI_INTEGER, &
         MPI_COMM_WORLD, ierror )
    
    
#ifdef COMMDEBUG_comm
    OPEN (UNIT=DU,FILE='debug.parallel_comm'//par_rank_str)    
    WRITE (DU,*) 'proc', par_rank,'procbuffer = ', procbuffer
    WRITE (DU,*) 'proc', par_rank,'pbs= ', pbs
    WRITE (DU,*) 'proc', par_rank,'index_to_recv',size_node,'=',index_to_recv
#endif

    ! space for nodes to be sent to other processors
    number_of_nodes_to_send = SUM(pbs(0:par_size-1))
    ALLOCATE( index_to_send(number_of_nodes_to_send) )
    IF (mode.EQ.0) THEN
       ALLOCATE( value_to_send(number_of_nodes_to_send*size_var) )
    ELSE IF (mode.EQ.1) THEN
       ALLOCATE( ivalue_to_send(number_of_nodes_to_send*size_var) )
    END IF



!#ifdef TREE_VERSION_0
!    PRINT *, ' TREE_VERSION_0 ' 
!#endif    
!
!#ifdef TREE_VERSION_1
!    PRINT *, ' TREE_VERSION_1 '
!#endif
!
!#ifdef TREE_NODE_POINTER_STYLE_C
!    PRINT *, ' TREE_NODE_POINTER_STYLE_C '
!#endif
!
!#ifdef TREE_NODE_POINTER_STYLE_F
!    PRINT *, ' TREE_NODE_POINTER_STYLE_F ' 
!#endif

#ifdef INTEGER8_DEBUG_AR
    PRINT *, ' parallel_comm >> number_of_nodes_to_send :: ', number_of_nodes_to_send
    PRINT *, ' parallel_comm >> number_of_nodes_to_send*size_var :: ', number_of_nodes_to_send*size_var
    !PRINT *, ' parallel_comm >> proc', par_rank,'index_to_recv',size_node,'=',index_to_recv
#endif
    
    
    ! Redistribure 1D index among the processors
    sdispl(0) = 0
    rdispl(0) = 0
    DO proc=1, par_size-1
       sdispl(proc) = sdispl(proc-1) + procbuffer(proc-1)
       rdispl(proc) = rdispl(proc-1) + pbs(proc-1)
    END DO

    CALL MPI_ALLTOALLV( index_to_recv,       & ! 1D index of the nodes the current processor wants from others
         procbuffer,                         & ! number of such nodes ...
         sdispl, MPI_INTEGER8,               & ! displacements ...                                              
         index_to_send,                      & ! 1D index of the nodes the current processor has to send
         pbs,                                & ! number of such nodes ...
         rdispl, MPI_INTEGER8,               & ! displacements ...                                              
         MPI_COMM_WORLD, ierror )
    MPIALLTOALL_counter(2) = MPIALLTOALL_counter(2) + SUM(sdispl(0:par_size-1))


    
    ! Fill the function values vector with the values requested by other processors:
    ! The values are expected to be written as (1:size_var,1:size_node)
    ! coordinate conversion
    i_p(0) = 1
    DO i=1,dim
       i_p(i) = i_p(i-1)*(mxyz(i)*2**(j_mx-1) + 1)
    END DO
#ifdef COMMDEBUG_comm
    WRITE (DU,*) 'i_p=',i_p
#endif

    !PRINT *, ' parallel_comm >> i_p=',i_p
    !PRINT *, ' parallel_comm >> i_p(0:dim-1)=',i_p(0:dim-1)
    
    maxixyz = 0
    
    IF (mode.EQ.0) THEN !=================================================================

       ! running index in value_to_send(:)
       kk = 0
       DO i=1,number_of_nodes_to_send
          ixyz(1:dim) = INT(MOD(index_to_send(i)-1,i_p(1:dim))/i_p(0:dim-1))

          !maxixyz = MAX( maxixyz , MAXVAL(ixyz(1:dim)) )
          !PRINT *, ' parallel_comm >>  par_rank,  max ixyz(1:dim) =', par_rank, maxixyz

          CALL DB_get_function_by_jmax_coordinates( ixyz, 1, n_var, val_tmp )
#ifdef COMMDEBUG_comm
          WRITE (DU,*) 'ii=',index_to_send(i),'ixyz=',ixyz,'f=',val_tmp
#endif
          DO k=1,n_var
             IF (var_to_recv(k)) THEN
                kk = kk + 1
                value_to_send(kk) = val_tmp(k)
             END IF
          END DO
       END DO

!!    PRINT *, ' parallel_comm >>  par_rank,  max ixyz(1:dim) =', par_rank, maxixyz
       
#ifdef COMMDEBUG_comm
       WRITE (DU,*) 'proc', par_rank,'index_to_send of size',SUM(pbs(0:par_size-1)),'=',index_to_send, &
            'value_to_send=', value_to_send
#endif
       
       ! Redistribute function values among the processors
       CALL MPI_ALLTOALLV( value_to_send,       & ! values the current processor will send to others
            pbs*size_var,                       &
            rdispl*size_var,                    &
            MPI_DOUBLE_PRECISION,               &
            value_to_recv,                      & ! values the current processor receive
            procbuffer*size_var,                &
            sdispl*size_var,                    &
            MPI_DOUBLE_PRECISION,               &
            MPI_COMM_WORLD, ierror )
       MPIALLTOALL_counter(3) = MPIALLTOALL_counter(3) + size_var*SUM(sdispl(0:par_size-1))
       
       
#ifdef COMMDEBUG_comm
       WRITE (DU,*) 'proc', par_rank,'value_to_recv=',value_to_recv
       CLOSE(DU)
#endif
    ELSE IF (mode.EQ.1) THEN !============================================================
       
       DO i=1,number_of_nodes_to_send
          ixyz(1:dim) = INT(MOD(index_to_send(i)-1,i_p(1:dim))/i_p(0:dim-1))
!!$          WRITE (*,*) 'ixyz=',ixyz, par_rank

          !maxixyz = MAX( maxixyz , MAXVAL(ixyz(1:dim)) )
          !PRINT *, ' parallel_comm >>  par_rank,  max ixyz(1:dim) =', par_rank, maxixyz

#ifdef COMMDEBUG_comm
          WRITE (DU,*) 'ii=',index_to_send(i),'ixyz=',ixyz,'ID=',id
#endif
          CALL DB_get_id_by_jmax_coord( ixyz, id )
!!$          WRITE (*,*) 'ixyz=',ixyz,'ID=',id, par_rank
          ivalue_to_send(i) = id
       END DO
       
#ifdef COMMDEBUG_comm
       WRITE (DU,*) 'proc', par_rank,'index_to_send of size',SUM(pbs(0:par_size-1)),'=',index_to_send, &
            'ivalue_to_send=', ivalue_to_send
#endif
       
       !AR 2/20/2011!  The following 11 lines are added by AR
       !
       !Initially, I thought   MPI_INTEGER  in the following command need to be changed to  MPI_INTEGER8
       !But, it is not required since both  ivalue_to_send  & ivalue_to_recv  are integer arrays. 
       !They are id (Node->id).
       !which  inside   SUBROUTINE make_list_and_request    [in db_tree.f90]
       !will be passed to   CALL DB_set_id_by_pointer( c_pointer2, ivaluebuffer(num) )
       !Where  in      void DB_SET_ID_BY_POINTER (const P_SIZE*const p_in, const int*const id_in)  [interface.cxx]
       !          we  have  node->id  = id    where   node->id   is of type  ID   [Ln212  tree.h]
       !          and ID is 0,1,2,4,8,16,32,64,128
       !SO,  ivaluebuffer(num)  is not (global 1D ID) but Node->id which is of Type ID (an integer)    
       !
       ! Redistribute function values among the processors
       CALL MPI_ALLTOALLV( ivalue_to_send,       & ! values the current processor will send to others
            pbs*size_var,                        &
            rdispl*size_var,                     &
            MPI_INTEGER,                         & 
            ivalue_to_recv,                      & ! values the current processor receive
            procbuffer*size_var,                 &
            sdispl*size_var,                     &
            MPI_INTEGER,                         & 
            MPI_COMM_WORLD, ierror )
       MPIALLTOALL_counter(1) = MPIALLTOALL_counter(1) + size_var*SUM(sdispl(0:par_size-1))
       
       
#ifdef COMMDEBUG_comm
       WRITE (DU,*) 'proc', par_rank,'ivalue_to_recv=',ivalue_to_recv
       CLOSE(DU)
#endif
    END IF !==============================================================================
    



#ifdef INTEGER8_DEBUG_AR
    WRITE (*,'(A, I4, A, 4I12)')                'parallel_comm  par_rank=', par_rank, ' i_p=', i_p
    WRITE (*,'(A, I4, A, 3I5, A, 3I4, A, 3I5)') 'parallel_comm  par_rank=', par_rank, ' ixyz=', ixyz, ' mxyz=', mxyz, ' nxyz=', nxyz  

!                                   WRITE (*,'(A, I4, A, I12)')  'parallel_comm  par_rank=', par_rank, ' MAXVAL(index_to_recv(:))  =', MAXVAL(index_to_recv(:))
!                                   WRITE (*,'(A, I4, A, I12)')  'parallel_comm  par_rank=', par_rank, ' MAXVAL(ivalue_to_recv(:)) =', MAXVAL(ivalue_to_recv(:))
!                                   WRITE (*,'(A, I4, A, F)')    'parallel_comm  par_rank=', par_rank, ' MAXVAL(value_to_recv(:))  =', MAXVAL(value_to_recv(:))

!    IF(ALLOCATED(index_to_send))   WRITE (*,'(A, I4, A, I12)')  'parallel_comm  par_rank=', par_rank, ' MAXVAL(index_to_send(:))  =', MAXVAL(index_to_send(:))
!    IF(ALLOCATED(ivalue_to_send))  WRITE (*,'(A, I4, A, I12)')  'parallel_comm  par_rank=', par_rank, ' MAXVAL(ivalue_to_send(:)) =', MAXVAL(ivalue_to_send(:))
!    IF(ALLOCATED(value_to_send))   WRITE (*,'(A, I4, A, F)')    'parallel_comm  par_rank=', par_rank, ' MAXVAL(value_to_send(:))  =', MAXVAL(value_to_send(:))
    IF (mode.EQ.0) THEN !=================================================================
       WRITE (*,'(A, I4, A, I1, A, I12)')  'parallel_comm  par_rank=', par_rank, ' mode=', mode, '  MAXVAL(index_to_send(:))  =', MAXVAL(index_to_send(:))
       WRITE (*,'(A, I4, A, I1, A, I12)')  'parallel_comm  par_rank=', par_rank, ' mode=', mode, '  MAXVAL(index_to_recv(:))  =', MAXVAL(index_to_recv(:))
       WRITE (*,'(A, I4, A, I1, A, F)')    'parallel_comm  par_rank=', par_rank, ' mode=', mode, '  MAXVAL(value_to_send(:))  =', MAXVAL(value_to_send(:))
       WRITE (*,'(A, I4, A, I1, A, F)')    'parallel_comm  par_rank=', par_rank, ' mode=', mode, '  MAXVAL(value_to_recv(:))  =', MAXVAL(value_to_recv(:))
    ELSE IF (mode.EQ.1) THEN !============================================================
       WRITE (*,'(A, I4, A, I1, A, I12)')  'parallel_comm  par_rank=', par_rank, ' mode=', mode, '  MAXVAL(index_to_send(:))  =', MAXVAL(index_to_send(:))
       WRITE (*,'(A, I4, A, I1, A, I12)')  'parallel_comm  par_rank=', par_rank, ' mode=', mode, '  MAXVAL(index_to_recv(:))  =', MAXVAL(index_to_recv(:))
       WRITE (*,'(A, I4, A, I1, A, I12)')  'parallel_comm  par_rank=', par_rank, ' mode=', mode, '  MAXVAL(ivalue_to_send(:)) =', MAXVAL(ivalue_to_send(:))
       WRITE (*,'(A, I4, A, I1, A, I12)')  'parallel_comm  par_rank=', par_rank, ' mode=', mode, '  MAXVAL(ivalue_to_recv(:)) =', MAXVAL(ivalue_to_recv(:))
    END IF !==============================================================================
#endif




    DEALLOCATE( index_to_send )
    IF (mode.EQ.0) THEN
       DEALLOCATE( value_to_send )
    ELSE IF (mode.EQ.1) THEN
       DEALLOCATE( ivalue_to_send )
    END IF
    

    CALL timer_stop(6)

#ifdef COMMDEBUG_MPIALLTOALL
    IF (par_rank.EQ.0) WRITE (*,'(A, 3I12)') '[parallel_comm]               MPI_ALLTOALL counter for 32b-INTEGER/64b-INTEGER/64b-REAL: ', MPIALLTOALL_counter(1:3)
#endif



  END SUBROUTINE parallel_comm

  !-----------------------------------------------------------
  ! initialize module
  SUBROUTINE parallel_init
    USE wlt_vars                           ! dim, j_lev, j_mx, nxyz
    CHARACTER (LEN=par_LENSTR1) :: tmp_str
    INTEGER :: ierror,i
    
    CALL MPI_INIT(ierror)
    CALL MPI_COMM_SIZE(MPI_COMM_WORLD, par_size, ierror)
    CALL MPI_COMM_RANK(MPI_COMM_WORLD, par_rank, ierror)

    par_rank_str = ''       ! default extension for single processor mode
    IF (par_size.GT.1) THEN
       WRITE( tmp_str,'(I'//CHAR(ICHAR('0')+par_LENSTR1)//')', ERR=1 ) par_rank
       par_rank_str = '.p'//TRIM(ADJUSTL(tmp_str))
    END IF

    ALLOCATE( procbuffer(0:par_size-1) )

    RETURN
    ! errors
1   PRINT *, ' internal error in parallel.f90:parallel_init(), proc=',par_rank
    PRINT *, ' set an appropriate value for par_LENSTR1 and recompile'
    CALL MPI_FINALIZE(ierror)
    STOP 1

  END SUBROUTINE parallel_init
  
  !-----------------------------------------------------------
  ! set file names to read in parallel restart (io_3d)
  ! (IC_)par_proc_tree and (IC_)par_size are known
  ! dim, j_tree_root, mxyz, and prd are from .inp file
  ! IC_mxyz and IC_prd are from .res file (if present there)
  SUBROUTINE parallel_set_files_to_read (fntr, IC_mxyz, IC_prd)
    USE wlt_vars                                ! dim, j_lev, j_mx
    USE debug_vars
    USE io_3D_vars
    LOGICAL, POINTER :: fntr(:)                 ! which .res files to read
    INTEGER, POINTER :: IC_mxyz(:), IC_prd(:)
    INTEGER :: number_of_trees, IC_number_of_trees, &
         ONE(dim), i_pt(0:dim), IC_i_pt(0:dim), i,  &
         t_coord(1:dim), IC_i, IC_j_tree_root
    LOGICAL :: domain_change    ! parallel domain decompose() output parameter
   
 
    ! IC_mxyz and IC_prd are not allocated in older versions;
    ! for such versions we do not know the original domain
    ! decomposition and therefore we will read all the files
    fntr(1:IC_par_size) = .TRUE.
    IF (.NOT.ASSOCIATED(IC_mxyz)) RETURN
    
    ! Single processor file has par_proc_tree(1)==0 and Nwlt_per_tree=0,
    ! therefore SIZE( IC_par_proc_tree ) is not giving number of trees
    ! and we will read that single file.
    ! Similarly, if run on a single processor, read all the files.
    IF (IC_par_size.EQ.1.OR.par_size.EQ.1) RETURN
    
    ! different mxyz or prd are not currently supported,
    ! so set for reading of all the files just in case
    IF (ANY(IC_mxyz(1:dim).NE.mxyz(1:dim)).OR.ANY(IC_prd(1:dim).NE.prd(1:dim))) RETURN

    ! error check for developers
    IF (.NOT. ALLOCATED(par_proc_tree) ) THEN
       PRINT *, ' In parallel::parallel_set_files_to_read()', &
            ' par_proc_tree() has not been allocated'
       CALL parallel_finalize
    END IF
    IF (.NOT. ALLOCATED(IC_par_proc_tree) ) THEN
       PRINT *, ' In parallel::parallel_set_files_to_read()', &
            ' IC_par_proc_tree() has not been allocated'
       CALL parallel_finalize
    END IF
    
!!$    PRINT *, 'IC_par_proc_tree=',IC_par_proc_tree
!!$    PRINT *, 'par_proc_tree   =',par_proc_tree
    
    ! newer .res versions
    fntr(1:IC_par_size) = .FALSE.
    ONE(1:dim) = 1
    number_of_trees = PRODUCT( mxyz(1:dim)*2**(j_tree_root-1) + ONE(1:dim) - prd(1:dim) )
    
#ifdef INTEGER8_DEBUG_AR
    IF (par_rank.EQ.0) &
    WRITE (*,'(A, I6, A, I10)')           'parallel_set_files_to_read  par_rank=', par_rank, ' number_of_trees=', number_of_trees 
#endif    
    
    IC_number_of_trees = SIZE( IC_par_proc_tree )
    ! find IC_j_tree_root level
    IC_j_tree_root = 1
    DO WHILE (IC_number_of_trees.NE.PRODUCT( IC_mxyz(1:dim)*2**(IC_j_tree_root-1) + ONE(1:dim) - IC_prd(1:dim) ) )
       IC_j_tree_root = IC_j_tree_root + 1
    END DO
!!$    PRINT *, 'IC_j_tree_root=',IC_j_tree_root
!!$    PRINT *, 'j_tree_root   =',j_tree_root
    ! pass through par_proc_tree(1:number_of_trees);
    ! recompute tree coordinates into IC_number_of_trees(1:IC_number_of_trees);
    ! find out to which processor belongs each tree of the current processor
    i_pt(0) = 1
    IC_i_pt(0) = 1
    DO i=1,dim
       i_pt(i) = i_pt(i-1)*( mxyz(i)*2**(j_tree_root-1) + 1 - prd(i) )
       IC_i_pt(i) = IC_i_pt(i-1)*( IC_mxyz(i)*2**(IC_j_tree_root-1) + 1 - IC_prd(i) )
    END DO

#ifdef INTEGER8_DEBUG_AR
    IF (par_rank.EQ.0) THEN
       IF (dim.EQ.3) &
          WRITE (*,'(A, I6, A, 4I12, A, 4I12)')     'parallel_set_files_to_read  par_rank=', par_rank, ' i_pt=', i_pt, ' IC_i_pt=', IC_i_pt
       IF (dim.EQ.2) &
          WRITE (*,'(A, I6, A, 3I12, A, 3I12)')     'parallel_set_files_to_read  par_rank=', par_rank, ' i_pt=', i_pt, ' IC_i_pt=', IC_i_pt
    ENDIF
#endif







    IF (j_tree_root.GT.IC_j_tree_root) THEN
       DO i=1,number_of_trees
          IF (par_rank.EQ.par_proc_tree(i)) THEN
!!$             PRINT *, 'tree #',i
             ! current processor's tree coordinates
             t_coord(1:dim) = INT(MOD(i-1,i_pt(1:dim))/i_pt(0:dim-1))
!!$             PRINT *, 't_coord=',t_coord,'-->',t_coord(1:dim)/2**(j_tree_root-IC_j_tree_root)
             ! the correspondent coordinates in .res file tree
             IC_i = 1+SUM( t_coord(1:dim)/2**(j_tree_root-IC_j_tree_root) *IC_i_pt(0:dim-1) )
!!$             PRINT *, 'IC_tree #',IC_i
             ! file to read from is IC_par_proc_tree(IC_i)
             fntr(IC_par_proc_tree(IC_i)+1) = .TRUE.
!!$             PRINT *, ' IC_par_proc_tree(IC_i)+1 =', IC_par_proc_tree(IC_i)+1
!!$             PRINT *, ' '
          END IF
       END DO
    ELSE
       DO IC_i=1,IC_number_of_trees
          ! coordinates in .res file tree
          t_coord(1:dim) = INT(MOD(IC_i-1,IC_i_pt(1:dim))/IC_i_pt(0:dim-1))
          ! the correspondent current processor's tree coordinates
          i = 1+SUM( t_coord(1:dim)/2**(IC_j_tree_root-j_tree_root) *i_pt(0:dim-1) )
          IF (par_rank.EQ.par_proc_tree(i)) THEN
             fntr(IC_par_proc_tree(IC_i)+1) = .TRUE.
          END IF
       END DO
    END IF

!!$    PRINT *, 'fntr=',fntr
    
  END SUBROUTINE parallel_set_files_to_read
  



  !-----------------------------------------------------------
  ! Distribute trees between processors
  ! (several methods available, see the code manual)
  !
  ! Called from init_db_tree_aux as ...(VERB=(par_rank.EQ.0), METH=domain_meth, FIRSTCALL=.TRUE.),
  ! second time called from adapt_grid as ...(VERB=(par_rank.EQ.0), METH=domain_meth)
  !
  SUBROUTINE parallel_domain_decompose( changes, VERBLEVEL, METH, FIRSTCALL, USESAVEDPARTITIONING, TREE_STATS)
    USE wlt_vars                                   ! dim, j_lev, j_mx
    USE debug_vars
    USE domain_geometric                           ! geom_domain_decomp
    LOGICAL, INTENT(INOUT) :: changes              ! T if redecomposed or initial partition
    INTEGER, INTENT(IN), OPTIONAL :: VERBLEVEL
    INTEGER, INTENT(IN), OPTIONAL :: METH
    LOGICAL, INTENT(IN), OPTIONAL :: FIRSTCALL                ! first call from init_db_tree_aux
    LOGICAL, INTENT(IN), OPTIONAL :: USESAVEDPARTITIONING     ! if TRUE, the same but uses Nwlt_per_Tree from RES file
    INTEGER, DIMENSION(0:dim,*), INTENT(IN), OPTIONAL :: TREE_STATS
    INTEGER :: number_of_trees, ONE(dim), &
         i, number_of_fat_trees, do_task, &
         ierr, i_pt(0:dim), wfield, &
         zave, zmin, zmax, &                         ! verb mode counters (number of trees)
         wave, wmin, wmax, &                         ! verb mode counters (number of wavelets)
         t_coord(1:dim), t_max_coord(1:dim),  &      !
         ddec(dim), field, do_meth, ii,       &
         num_per_proc(0:par_size-1),          &      ! number of trees per processors
         wum_per_proc(0:par_size-1),          &      ! number of wavelets per processors
         task,                                &      ! 0-partition, 1-repartition, 2-refine
         do_verb
    
    INTEGER, ALLOCATABLE :: par_proc_tree_old(:)     ! previous partitioning
    
    LOGICAL :: do_firstcall, do_usesavedpartitioning, &
         OK_marker                                   ! if decomposition is OK
    
    INTEGER, PARAMETER :: &     ! Zoltan partitioning input parameter
         DO_NOTHING = -1, &
         PARTITION = 0,   &
         REPARTITION = 1, &
         REFINE = 2

    INTEGER, ALLOCATABLE :: Nwlt_per_edge(:,:)

  !============================= Handling Empty Processor (Avoiding zero trees error)
  TYPE Map_Empty_Proc                                                                    ! TYPE structure for Map_Empty_Proc 
     INTEGER,              DIMENSION(1:max_dim) :: nxyz_min
     INTEGER,              DIMENSION(1:max_dim) :: nxyz_max
    !INTEGER, ALLOCATABLE, DIMENSION(:, 1:dim)   :: nxyz_Tree
     INTEGER                                     :: maptoProc
     INTEGER                                     :: LargestSpan                              
  END TYPE Map_Empty_Proc
 
  TYPE (Map_Empty_Proc), ALLOCATABLE, DIMENSION(:) :: map_Proc
  INTEGER,               ALLOCATABLE, DIMENSION(:) :: Tree_to_be_mapped
  INTEGER,               ALLOCATABLE, DIMENSION(:) :: Index__sorted_num_per_proc
  
  INTEGER :: Proc_Mid_Span, Tree_Span, Prev_Max, Next_Max, start_empty_proc, counter, k, kk                                            

  REAL(pr) :: wnum_stats(1:4)             ! (min,max,mean,stdev)[NWLT]
  !=================================================================================


    
    do_verb = 1
    IF (PRESENT(VERBLEVEL)) do_verb = VERBLEVEL
    do_meth = 0
    IF (PRESENT(METH)) do_meth = METH
    do_firstcall = .FALSE.
    IF (PRESENT(FIRSTCALL)) do_firstcall = FIRSTCALL
    do_usesavedpartitioning = .FALSE.
    IF (PRESENT(USESAVEDPARTITIONING)) do_usesavedpartitioning = USESAVEDPARTITIONING
    
    ! default - partitioning is not changed hence repeating of adjacents
    !           is not required after that subroutine call from adapt grid.
    changes = .FALSE.
    
    ! debugging the domain decomposition
!!$    IF (domain_debug) THEN
!!$       do_verb = 1
!!$       IF (par_rank.NE.0) THEN
!!$          CALL parallel_finalize; STOP
!!$       END IF
!!$       ! ---------------------------------------
!!$       ! Change these lines to adjust the domain
!!$       ! decomposition debugging inputs.
!!$       
!!$       WRITE (*,ADVANCE='NO',FMT='("par_size = ")'); READ (*,*) par_size
!!$       !WRITE (*,ADVANCE='NO',FMT='("prd(1:",I1,") = ")') dim; READ (*,*) prd(1:dim)
!!$       ! ---------------------------------------
!!$    END IF
    
    
    ONE(1:dim) = 1
    number_of_trees = PRODUCT( mxyz(1:dim)*2**(j_tree_root-1) + ONE(1:dim) - prd(1:dim) )
    number_of_fat_trees = PRODUCT( mxyz(1:dim)*2**(j_tree_root-1) )
#ifdef INTEGER8_DEBUG_AR
    IF (par_rank.EQ.0) &
    WRITE (*,'(A, I6, A, I12, A, I12)') 'parallel_domain_decompose  par_rank=', par_rank, ' number_of_trees=', number_of_trees, ' number_of_fat_trees=', number_of_fat_trees 
#endif    

    
    ! debugging the domain decomposition
!!$    do_verb = 0
!!$    debug: DO par_size = 4,4 !2,1024
    
    
    
    ! single processor shortcut
    IF (par_size.EQ.1) THEN
       number_of_trees = 1
       IF (.NOT.do_usesavedpartitioning.AND.do_firstcall) THEN
          ALLOCATE( par_proc_tree(1:number_of_trees) )
          ALLOCATE( Nwlt_per_Tree(1:number_of_trees) )
       END IF
       par_proc_tree = 0
       Nwlt_per_Tree = 0
       RETURN
    END IF
    
    
    IF (do_verb.GT.0) &
         WRITE (*,'("# ",I5,A,I5,A, I3)') &
         number_of_trees, ' trees to distribute between ', &
         par_size, ' processors with method ', do_meth
    
    
    IF (par_size.GT.number_of_trees) THEN
       PRINT *, 'ERROR: too many processors.'
       PRINT *, '       Some processors will have to have no trees,'
       PRINT *, '       which is not currently supported.'
       CALl parallel_finalize; STOP
    END IF
    
        
    
    IF (do_firstcall) THEN
       ! Called from init_DB_tree_aux before DB_declare_globals with FIRSTCALL=.TRUE.
       ! Reallocate par_proc_tree(:) for initial partition only
       ! (in case of multiple .res files read in post-processing)
       IF(.NOT.do_usesavedpartitioning) THEN 
          IF (ALLOCATED(par_proc_tree)) DEALLOCATE(par_proc_tree) ! one rank per one tree number
          ALLOCATE( par_proc_tree(1:number_of_trees) )
          IF (ALLOCATED(Nwlt_per_Tree)) DEALLOCATE(Nwlt_per_Tree) ! active wavelets per tree
          ALLOCATE( Nwlt_per_Tree(1:number_of_trees) )
          Nwlt_per_Tree = 1   ! equal geometric weight, regular grid

       ELSE
          IF (SIZE(par_proc_tree).NE.number_of_trees) THEN
             WRITE(*,'("ERROR: wrong value for the number_of_trees")') 
             CALL parallel_finalize; STOP
          END IF

       END IF
       par_proc_tree = -1  ! error value (to check unassigned trees)
       task = PARTITION    ! Partition (later, if meth=-1 in postprocessing it will copy
       !                   ! the existing one. Otherwise, it will partition the domain.)

       ! Since Nwlt_per_Edge is local, it must be accounted for here, 
       ! though this is not the most robust, since we are using a saved partitioning and restart
       ! we ought to at least provide an estimation (used in some restarts - overwritten when information becomes available)
       IF(ALLOCATED(Nwlt_per_Edge)) DEALLOCATE(Nwlt_per_Edge)  !use additional array for manipulation of weights before calling partition
       ALLOCATE(Nwlt_per_Edge(1:dim,1:number_of_trees))
       DO i = 1,dim
          Nwlt_per_Edge(i,1:number_of_trees) = FLOOR(REAL(Nwlt_per_Tree(1:number_of_trees)**(dim-1/dim),pr)* 2.0_pr * 0.1_pr); !power-law estimation 
       END DO
    ELSE
       ! Called from adapt_grid after synchronizing all the adjacent.
       ! Assign Nwlt_per_Tree
       ! (DB function counts all the nodes with significant or adjacent ID bits.
       !  It may happen that some of these nodes lie outside of the current processor domain,
       !  which is an artefact of parallel add_ghosts j_d calculations, where we add some of
       !  the outside of the domain nodes to the current processor DB with their original IDs.
       !  These extra sig nodes will be in current list_gho despite their IDs and are not used
       !  afterwards. We can safely keep the correspondent Nwlt_per_Tree nonzero.)

       ! Old
       !CALL DB_set_nwlt_per_tree (Nwlt_per_Tree, number_of_trees, ierr)   !Old, incorrect
       !CALL parallel_global_sum( INTEGER=ierr )
       !IF (ierr.NE.0) THEN
       !   PRINT *, 'ERROR: internal DB_set_nwlt_per_tree error.'
       !   CALL parallel_finalize; STOP
       !END IF

       Nwlt_per_Tree = TREE_STATS(0,1:number_of_trees)
       Nwlt_per_Tree = MAX(1,Nwlt_per_Tree)    ! Minimum value, for better partition topology with empty trees when j_tree > j_mn 

       IF(ALLOCATED(Nwlt_per_Edge)) DEALLOCATE(Nwlt_per_Edge)  !use additional array for manipulation of weights before calling partition
       ALLOCATE(Nwlt_per_Edge(1:dim,1:number_of_trees))
       Nwlt_per_Edge(1:dim,1:number_of_trees) = TREE_STATS(1:dim,1:number_of_trees)
       Nwlt_per_Edge = MAX(1,Nwlt_per_Edge)    ! Minimum value, for better partition topology with empty trees when j_tree > j_mn 
       
       ! store previous partition to use it in data migration
       IF (ALLOCATED(par_proc_tree_old)) THEN
          IF (SIZE(par_proc_tree_old).NE.number_of_trees) DEALLOCATE (par_proc_tree_old)
       END IF
       IF (.NOT.ALLOCATED(par_proc_tree_old)) ALLOCATE(par_proc_tree_old(number_of_trees))
       par_proc_tree_old = par_proc_tree
       
       ! for statistics below ( #trees(#wavelets) ) we want global Nwlt_per_Tree,
       ! therefore we make it global here.
       !CALL sync_arr_per_tree (Nwlt_per_Tree, par_proc_tree, number_of_trees)
       
       ! set task value based on the method and repartition tolerances
       CALL parallel_domain_imbalanced (task, (do_verb.GT.0), number_of_trees)
      
       CALL pretty_print_array (Nwlt_per_Tree, number_of_trees, 'Nwlt_per_Tree: (sig+adj)', (do_verb.GT.1))
       
!!$       PAUSE 'WARNING - task reset to DO_NOTHING at parallel:1350'
!!$       task = DO_NOTHING
       
    END IF
    
    
!!$    IF (Nwlt_per_Tree(1).EQ.2) Nwlt_per_Tree(1:3) = 10
!!$    par_proc_tree = 1
!!$    par_proc_tree(1:5) = 0
!!$    par_proc_tree(11) = 0
!!$    PAUSE 'parallel.f90: 1299'
    
    
    
    ! no action shortcut
    ! (unless it is a postprocessing call with METH=-1)
    IF (task.EQ.DO_NOTHING.AND.do_meth.GE.0) RETURN
    
    
    ! ---------------------------------------------------------------------------
    ! ---------------------------------------------------------------------------
    IF (do_meth.EQ.0) THEN
       ! pow(1/dim) based with Oleg's prime multipliers
       CALL geom_domain_decomp ( par_size, mxyz(1:dim)*2**(j_tree_root-1), domain_split, dim, ddec, VERB=(do_verb.GT.1) )
       CALL parallel_domain_decompose_2( par_proc_tree, number_of_trees, do_verb.GT.1, ddec )

    ELSE IF (do_meth.EQ.1) THEN
       ! pow(1/dim) based distribution
       CALL parallel_domain_decompose_2( par_proc_tree, number_of_trees, do_verb.GT.1 )
    ELSE IF (do_meth.GE.2 .AND. do_meth.LE.4) THEN
       ! Zoltan library based decomposition
       CALL parallel_domain_decompose_zoltan( par_proc_tree, number_of_trees, Nwlt_per_edge, task, do_verb.GT.1 )
    ELSE IF (do_meth.EQ.10) THEN
       ! simple based on tree number only
       CALL parallel_domain_decompose_10( par_proc_tree, number_of_trees )
    ELSE IF (do_meth.EQ.11) THEN
       ! simple fair fat tree distribution
       CALL parallel_domain_decompose_1( par_proc_tree, number_of_trees )
!!$    ELSE IF (do_meth.EQ.19) THEN
!!$       ! simple based on fat tree number only
!!$       CALL parallel_domain_decompose_10( par_proc_tree, number_of_trees,  number_of_fat_trees )
    ELSE IF (do_meth.LT.0) THEN
       ! read IC_par_proc_tree (from .res file)
       CALL parallel_read_IC( par_proc_tree, number_of_trees )
       
    ELSE
       ! DEFAULT
       ! pow(1/dim) based distribution
       CALL parallel_domain_decompose_2( par_proc_tree, number_of_trees, do_verb.GT.1 )
    END IF
!!$    ! ---------------------------------------------------------------------------
!!$    ! DEBUG: set decomposition manually
!!$    ! j_mn=2 3  4   5
!!$    !      8 32 128 512
!!$    par_proc_tree = 1
!!$    par_proc_tree(2) = 0
!!$    par_proc_tree = 1
!!$    par_proc_tree(:) = 0; !par_proc_tree(10) = 0;
!!$    
!!$    IF (par_rank.EQ.0) PRINT *, 'par_proc_tree of size',SIZE(par_proc_tree),'==',par_proc_tree
!!$    IF (par_rank.EQ.0) PRINT *,'domain_split=',domain_split
    ! ---------------------------------------------------------------------------
    ! ---------------------------------------------------------------------------
    
    
    ! print decomposition statistics (if do_verb)
    CALL pretty_print_array (par_proc_tree, number_of_trees, 'par_proc_tree:', do_verb.GT.0)
    

    
    ! test if decomposition is OK
    OK_marker = .FALSE.                                    ! == no errors
    
    ! count number of trees and wavelets per processor
    num_per_proc(0:par_size-1) = 0
    wum_per_proc(0:par_size-1) = 0
    DO i=1,number_of_trees
       IF (par_proc_tree(i).GE.par_size.OR.par_proc_tree(i).LT.0) THEN
          WRITE (*,'(3(A,I5))') &
               'wrong par_proc_tree(',i,') = ',par_proc_tree(i),' at proc',par_rank
          OK_marker = .TRUE.
       END IF
       num_per_proc(par_proc_tree(i)) = num_per_proc(par_proc_tree(i)) + 1
       wum_per_proc(par_proc_tree(i)) = wum_per_proc(par_proc_tree(i)) + Nwlt_per_Tree(i)
    END DO
    
    ! terminate if decomposition is not OK
    CALL parallel_global_sum (LOGICALOR=OK_marker)
    IF (OK_marker) THEN
       CALL parallel_finalize
       STOP 1
    END IF



    ! test if every processor has some trees
    IF (.NOT.OK_marker) THEN
       counter = 0
       IF (do_verb.GT.0) WRITE (*,'("#")')
       IF (do_verb.GT.0) WRITE (*,'("# Domain decomposition: trees (nwlt) per processor:")')
       zave = 0; zmin = num_per_proc(0); zmax = 0
       wave = 0; wmin = wum_per_proc(0); wmax = 0
       IF ( do_verb.GE.1 .AND. par_rank.EQ.0 .AND. ( ANY(num_per_proc(0:par_size-1) .EQ. 0 )) )  WRITE(UNIT=*, ADVANCE='NO', FMT='( '' These processors have been assigned zero trees: '' )' ) 
       DO i=0,par_size-1
          IF (num_per_proc(i).EQ.0) THEN
             !IF (par_rank.EQ.0) PRINT *,'this processor has been assigned zero trees:', i 
             IF (do_verb.GE.1 .AND. par_rank.EQ.0) WRITE(UNIT=*, ADVANCE='NO', FMT='( i6 )') i     
             counter = counter + 1  
             OK_marker = .TRUE.
          END IF
          zave = zave + num_per_proc(i)
          wave = wave + wum_per_proc(i)
          zmin = MIN(zmin,num_per_proc(i))
          wmin = MIN(wmin,wum_per_proc(i))
          zmax = MAX(zmax,num_per_proc(i))
          wmax = MAX(wmax,wum_per_proc(i))
       END DO
       IF ( par_rank.EQ.0 .AND. ( ANY(num_per_proc(0:par_size-1) .EQ. 0 )) ) THEN
          SELECT CASE(do_verb)
                 CASE(0)
                    WRITE(UNIT=*, ADVANCE='YES', FMT='(  '' Number of processors which have been assigned zero trees: '' i6 )' ) counter
                 CASE(1 :)
                    WRITE(UNIT=*, ADVANCE='YES', FMT='( '' '' )' ) 
                 CASE DEFAULT
                    IF (par_rank.EQ.0) THEN
                       PRINT *, 'Error ::   Unknown verb_level  in  parallel_domain_decompose()'
                       PRINT *,' Exiting ...'
                    END IF
                    CALL parallel_finalize
                    STOP
         END SELECT
         !(min, max, mean, stdev)[NWLT]
         wnum_stats(:) = 0
         wnum_stats(1) =    MINVAL( wum_per_proc(0:par_size-1), mask = wum_per_proc .GT. 0 )         
         wnum_stats(2) =    MAXVAL( wum_per_proc(0:par_size-1) )
         wnum_stats(3) =       SUM( wum_per_proc(0:par_size-1) ) / REAL(par_size)
         wnum_stats(4) = SQRT( SUM((wum_per_proc(0:par_size-1)-wnum_stats(3))**2) / REAL(par_size))
         WRITE (UNIT=*, ADVANCE='YES', FMT='( '' Before Handling Empty Processors : (min, max, mean, stdev)[NWLT] = '' 4(i12,1x)   '' min here is the Non-zero min -- MINVAL(NWLT)='' i12,1x )') INT(wnum_stats(1:4)), MINVAL( wum_per_proc(0:par_size-1) )
       END IF
    END IF
    




    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!  (Handling Empty Processor)   begin  
!!$#define ZeroTreeProc_DEBUG
  
    CALL parallel_global_sum (LOGICALOR=OK_marker)
    IF (OK_marker) THEN
       IF ( ANY(num_per_proc(0:par_size-1) .EQ. 0 )) THEN 


#ifdef ZeroTreeProc_DEBUG
             IF (par_rank.EQ.0) THEN
                !WRITE (*,'(A,I4,A,I)')  'par_rank=', par_rank, '   num_per_proc(:) = ', num_per_proc(:)               
                PRINT *, 'par_rank=', par_rank, '   num_per_proc(:) = ', num_per_proc(:)                             
                PRINT *, 'par_rank=', par_rank, '   wum_per_proc(:) = ', wum_per_proc(:)                             
             END IF
#endif  



             i_pt(0) = 1                                                             ! 1D tree coordinate transform
             DO i=1,dim
                i_pt(i) = i_pt(i-1)*( mxyz(i)*2**(j_tree_root-1) + 1 - prd(i) )
                t_max_coord(i) =  mxyz(i)*2**(j_tree_root-1)
             END DO


    
             IF (ALLOCATED(map_Proc))                   DEALLOCATE (map_Proc)
             IF (ALLOCATED(Index__sorted_num_per_proc)) DEALLOCATE (Index__sorted_num_per_proc)
             ALLOCATE (map_Proc(0:par_size-1))
             ALLOCATE (Index__sorted_num_per_proc(0:par_size-1))



             ! Finding   map_Proc%nxyz[Min - Max]  Bounding-Box   for each Processor :: Min & Max in each direction of nxyz of all trees on each proc.
             DO k=1,dim
                map_Proc(0:par_size-1)%nxyz_min(k) = t_max_coord(k)
                map_Proc(0:par_size-1)%nxyz_max(k) = 0
             END DO
             DO i=1,number_of_trees
                t_coord(1:dim) = INT(MOD(i-1,i_pt(1:dim))/i_pt(0:dim-1))
                
                !IF ( ANY(t_coord(1:dim)) >= map_Proc(par_proc_tree(i))%nxyz_max(1:dim) )   map_Proc(par_proc_tree(i))%nxyz_max(1:dim) = t_coord(1:dim)
                !IF ( ANY(t_coord(1:dim)) <= map_Proc(par_proc_tree(i))%nxyz_min(1:dim) )   map_Proc(par_proc_tree(i))%nxyz_min(1:dim) = t_coord(1:dim)
                DO k=1,dim
                   IF ( t_coord(k) >= map_Proc(par_proc_tree(i))%nxyz_max(k) )   map_Proc(par_proc_tree(i))%nxyz_max(k) = t_coord(k)
                   IF ( t_coord(k) <= map_Proc(par_proc_tree(i))%nxyz_min(k) )   map_Proc(par_proc_tree(i))%nxyz_min(k) = t_coord(k)
                ENDDO
             END DO
             
             
             
             ! Finding   LargestSpan   of  map_Proc%nxyz[Min - Max]  Bounding-Box   for each Processor
             DO i=0,par_size-1
                !map_Proc(i)%LargestSpan = MINVAL(MAXLOC( map_Proc(i)%nxyz_max(1:dim) ))                  
                map_Proc(i)%LargestSpan = MINVAL(MAXLOC( map_Proc(i)%nxyz_max(1:dim) - map_Proc(i)%nxyz_min(1:dim) ))                  
#ifdef ZeroTreeProc_DEBUG
                IF (par_rank.EQ.0) THEN
                   IF     (dim.EQ.3) THEN 
                      WRITE (*,'(A,I5,A,3I4,A,3I4,A,I4)') 'i   map_Proc(i)%nxyz_min(1:dim)   map_Proc(i)%nxyz_max(1:dim)   map_Proc(i)%LargestSpan = ', i, ' [', map_Proc(i)%nxyz_min(1:dim), '][', map_Proc(i)%nxyz_max(1:dim), ']  ', map_Proc(i)%LargestSpan      
                   ELSEIF (dim.EQ.2) THEN 
                      WRITE (*,'(A,I5,A,2I4,A,2I4,A,I4)') 'i   map_Proc(i)%nxyz_min(1:dim)   map_Proc(i)%nxyz_max(1:dim)   map_Proc(i)%LargestSpan = ', i, ' [', map_Proc(i)%nxyz_min(1:dim), '][', map_Proc(i)%nxyz_max(1:dim), ']  ', map_Proc(i)%LargestSpan      
                   END IF
                END IF
#endif  
             END DO
             
             
             
             ! Finding   Index   of   Descending Sort of num_per_proc
             i = 0
             counter = 0
             Prev_Max = MAXVAL(num_per_proc) + 100                                                       ! 100 is an arbitrary number just to set Prev_Max > MAXVAL(num_per_proc)
             DO WHILE ( i .LE. par_size-1 )
                Next_Max = MAXVAL(num_per_proc, mask = num_per_proc .LT. Prev_Max)
                counter = COUNT(num_per_proc .EQ. Next_Max)
                
#ifdef ZeroTreeProc_DEBUG
                IF (par_rank.EQ.0) WRITE (*,'(A,3I5,I9)') 'i  counter  i+counter-1  Next_Max = ', i, counter, i+counter-1, Next_Max      
#endif  
                !Index__sorted_num_per_proc(i:i+counter-1) = MAXLOC(num_per_proc, mask = num_per_proc .EQ. Next_Max) - 1                   ! This is wrong B/C MAXLOC returns only one value
                k = i  ! i:i+counter-1
                ii = 0 ! 0:par_size-1
                DO WHILE ( k .LE. i+counter-1 )
                   IF ( num_per_proc(ii) .EQ. Next_Max) THEN
                      Index__sorted_num_per_proc(k) = ii
                      k = k +1
                   END IF
                   ii = ii + 1
                END DO
                
                Prev_Max = Next_Max
                i = i + counter
             END DO
#ifdef ZeroTreeProc_DEBUG
             IF (par_rank.EQ.0) PRINT *, 'par_rank=', par_rank, '   Index__sorted_num_per_proc(:) = ', Index__sorted_num_per_proc(:)   ! Just for debugging
#endif  
             
             
             
             ! Mapping
             IF (ALLOCATED(Tree_to_be_mapped))          DEALLOCATE (Tree_to_be_mapped)
             ALLOCATE (Tree_to_be_mapped(1:MAXVAL(num_per_proc)))                                            ! OR (1:num_per_proc(Index__sorted_num_per_proc(0))))
             !start_empty_proc = MINVAL(MINLOC(num_per_proc(Index__sorted_num_per_proc(0):Index__sorted_num_per_proc(par_size-1)))) - 1 
             !IF (par_rank.EQ.0) PRINT *, 'par_rank=', par_rank, '   start_empty_proc = ', start_empty_proc   ! Just for debugging
             start_empty_proc = par_size - COUNT(num_per_proc .EQ. 0)
#ifdef ZeroTreeProc_DEBUG
             IF (par_rank.EQ.0) PRINT *, 'par_rank=', par_rank, '   start_empty_proc = ', start_empty_proc    ! Just for debugging
#endif  
             
             ! Finding   map_Proc%maptoProc   for  each Processor
             map_Proc(0:par_size-1)%maptoProc = -1
             IF ( (par_size - start_empty_proc + 1) .GT. start_empty_proc  ) THEN
                IF (par_rank.EQ.0) WRITE (*,'(" No. of Empty_Processors(zero trees)  >  No. of Non-Empty-Processors  -  Not Supported yet")')
                IF (par_rank.EQ.0) PRINT *,' Exiting ...'
                CALL parallel_finalize; STOP
             END IF
             DO i=start_empty_proc,par_size-1
                map_Proc(Index__sorted_num_per_proc(i))%maptoProc = Index__sorted_num_per_proc(i - start_empty_proc)
             END DO
             
             DO i=start_empty_proc,par_size-1
                !t_coord(1:dim) = INT(MOD(i-1,i_pt(1:dim))/i_pt(0:dim-1))
                
                ii = map_Proc(Index__sorted_num_per_proc(i))%maptoProc
                
                Tree_to_be_mapped(:) = 0
                !Tree_to_be_mapped(:) = PACK(par_proc_tree, mask = par_proc_tree .EQ. ii)  !!!! wrong !!!!
                kk=1
                DO k=1,number_of_trees
                   IF (par_proc_tree(k) .EQ. ii) THEN
                      Tree_to_be_mapped(kk) = k
                      kk = kk +1
                   END IF
                END DO
                
                Do k=1,num_per_proc(ii)
                   t_coord(1:dim) = INT(MOD( Tree_to_be_mapped(k) -1,i_pt(1:dim))/i_pt(0:dim-1))
                   
                   !Mid_Span = ( map_Proc(ii)%nxyz_min(map_Proc(ii)%LargestSpan) + map_Proc(ii)%nxyz_max(map_Proc(ii)%LargestSpan) ) / 2
                   !!IF ( ANY(t_coord(1:dim)) >= Mid_Span )  
                   !IF ( t_coord(map_Proc(ii)%LargestSpan) .GT. Mid_Span ) par_proc_tree(Tree_to_be_mapped(k)) = i 
                   Proc_Mid_Span = ( map_Proc(ii)%nxyz_max(map_Proc(ii)%LargestSpan) - map_Proc(ii)%nxyz_min(map_Proc(ii)%LargestSpan) ) / 2
                   Tree_Span = ( t_coord(map_Proc(ii)%LargestSpan) - map_Proc(ii)%nxyz_min(map_Proc(ii)%LargestSpan) )
                   IF ( Tree_Span .GT. Proc_Mid_Span ) par_proc_tree(Tree_to_be_mapped(k)) = Index__sorted_num_per_proc(i)
                END DO
             END DO
             
             
             
             IF (ALLOCATED(map_Proc))                   DEALLOCATE (map_Proc)
             IF (ALLOCATED(Tree_to_be_mapped))          DEALLOCATE (Tree_to_be_mapped)
             IF (ALLOCATED(Index__sorted_num_per_proc)) DEALLOCATE (Index__sorted_num_per_proc)
             
             
             
             ! Sanity Check ........................................................ begin
             ! test if decomposition is OK
             OK_marker = .FALSE.                                    ! == no errors
             
             ! count number of trees and wavelets per processor
             num_per_proc(0:par_size-1) = 0
             wum_per_proc(0:par_size-1) = 0
             DO i=1,number_of_trees
                IF (par_proc_tree(i).GE.par_size.OR.par_proc_tree(i).LT.0) THEN
                   WRITE (*,'(3(A,I5))') &
                        'wrong par_proc_tree(',i,') = ',par_proc_tree(i),' at proc',par_rank
                   OK_marker = .TRUE.
                END IF
                num_per_proc(par_proc_tree(i)) = num_per_proc(par_proc_tree(i)) + 1
                wum_per_proc(par_proc_tree(i)) = wum_per_proc(par_proc_tree(i)) + Nwlt_per_Tree(i)
             END DO
             
             ! terminate if decomposition is not OK
             CALL parallel_global_sum (LOGICALOR=OK_marker)
             IF (OK_marker) THEN
                CALL parallel_finalize
                STOP 1
             END IF
             
             
             
             ! test if every processor has some trees
             IF (.NOT.OK_marker) THEN
                IF (do_verb.GT.0) WRITE (*,'("#")')
                IF (do_verb.GT.0) WRITE (*,'("# Domain decomposition: trees (nwlt) per processor:")')
                zave = 0; zmin = num_per_proc(0); zmax = 0
                wave = 0; wmin = wum_per_proc(0); wmax = 0
                DO i=0,par_size-1
                   IF (num_per_proc(i).EQ.0) THEN
                      IF (par_rank.EQ.0) PRINT *,'this processor has been assigned zero trees - not supported yet:', i
                      OK_marker = .TRUE.
                   END IF
                   zave = zave + num_per_proc(i)
                   wave = wave + wum_per_proc(i)
                   zmin = MIN(zmin,num_per_proc(i))
                   wmin = MIN(wmin,wum_per_proc(i))
                   zmax = MAX(zmax,num_per_proc(i))
                   wmax = MAX(wmax,wum_per_proc(i))
                END DO
             END IF
             
             ! terminate if decomposition is not OK
             CALL parallel_global_sum (LOGICALOR=OK_marker)
             IF (OK_marker) THEN
                CALL parallel_finalize
                STOP 1
             END IF
             ! Sanity Check ........................................................ end
             
             
             
#ifdef ZeroTreeProc_DEBUG
             IF (par_rank.EQ.0) THEN
                PRINT *, 'par_rank=', par_rank, '   num_per_proc(:) = ', num_per_proc(:)                             
                PRINT *, 'par_rank=', par_rank, '   wum_per_proc(:) = ', wum_per_proc(:)                             
             END IF
#endif  


             !(min, max, mean, stdev)[NWLT]
             IF ( par_rank.EQ.0 ) THEN 
                wnum_stats(:) = 0
                wnum_stats(1) =    MINVAL( wum_per_proc(0:par_size-1) )
                wnum_stats(2) =    MAXVAL( wum_per_proc(0:par_size-1) )
                wnum_stats(3) =       SUM( wum_per_proc(0:par_size-1) ) / REAL(par_size)
                wnum_stats(4) = SQRT( SUM((wum_per_proc(0:par_size-1)-wnum_stats(3))**2) / REAL(par_size))
                WRITE (UNIT=*, ADVANCE='YES', FMT='( '' After  Handling Empty Processors : (min, max, mean, stdev)[NWLT] = '' 4(i12,1x))') INT(wnum_stats(1:4))
             END IF
             
             
       END IF
    END IF
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!  (Handling Empty Processor)   end





    field = CEILING( LOG(REAL(zmax+1,pr))/LOG(10.0_pr) ) + 1
    wfield = CEILING( LOG(REAL(wmax+1,pr))/LOG(10.0_pr) )
    IF (do_verb.GT.0) WRITE (*,ADVANCE='NO',FMT='("#")')
    DO i=0,par_size-1
       IF (do_verb.GT.0) WRITE (*,ADVANCE='NO',FMT='(I'//CHAR(ICHAR('0')+field)//',"(",I'//CHAR(ICHAR('0')+wfield)//',")")') &
            num_per_proc(i), wum_per_proc(i)
    END DO
    IF (do_verb.GT.0) WRITE (*,'(" ")')
    IF (do_verb.GT.0) WRITE (*,'(A,3(I'//CHAR(ICHAR('0')+field)//',"(",I'//CHAR(ICHAR('0')+wfield)//',") "))') &
         '# average/min/max trees (nwlt) = ', zave/par_size, wave/par_size, zmin, wmin, zmax, wmax
    IF (do_verb.GT.0) WRITE (*,'("#")')
    
!!$       WRITE (*,'("  OUTPUT ",I5,2E15.7)') par_size, zmin/REAL(zave/par_size,pr), zmax/REAL(zave/par_size,pr)
    
    IF (ANY(prd(1:dim).NE.1)) THEN
          ! count number of non-surface trees per processor
       num_per_proc(0:par_size-1) = 0
       i_pt(0) = 1
       DO i=1,dim
          i_pt(i) = i_pt(i-1)*( mxyz(i)*2**(j_tree_root-1) + 1 - prd(i) )
          t_max_coord(i) =  mxyz(i)*2**(j_tree_root-1)
       END DO
       
!#ifdef INTEGER8_DEBUG_AR
       IF (par_rank.EQ.0) THEN
          IF (dim.EQ.3) &
          WRITE (*,'(A, I6, A, 4I12, A, 3I12)') 'parallel_domain_decompose  par_rank=', par_rank, ' i_pt=', i_pt, ' t_max_coord=', t_max_coord 
          IF (dim.EQ.2) &
          WRITE (*,'(A, I6, A, 3I12, A, 2I12)') 'parallel_domain_decompose  par_rank=', par_rank, ' i_pt=', i_pt, ' t_max_coord=', t_max_coord 
       ENDIF
!#endif  
       
       DO i=1,number_of_trees
          t_coord(1:dim) = INT(MOD(i-1,i_pt(1:dim))/i_pt(0:dim-1))
          IF (ALL(t_coord(1:dim).NE.t_max_coord(1:dim))) THEN
             num_per_proc(par_proc_tree(i)) = num_per_proc(par_proc_tree(i)) + 1
          END IF
       END DO
       
       IF (do_verb.GT.0) WRITE (*,'("# Domain decomposition: fat trees per processor")')
       IF (do_verb.GT.0) WRITE (*,ADVANCE='NO',FMT='("#")')
       zave = 0; zmin = num_per_proc(0); zmax = 0
       DO i=0,par_size-1
          IF (do_verb.GT.0) WRITE (*,ADVANCE='NO',FMT='(I5)') &
               num_per_proc(i)
          IF (num_per_proc(i).EQ.0) THEN
             IF (do_verb.GT.0) PRINT *,'some processor has been assigned zero fat trees'
          END IF
          zave = zave + num_per_proc(i)
          zmin = MIN(zmin,num_per_proc(i))
          zmax = MAX(zmax,num_per_proc(i))
       END DO
       IF (do_verb.GT.0) WRITE (*,'("  average/min/max = ",3I5)') zave/par_size, zmin, zmax
    END IF
    
    
    
    
    ! migrate data in accordance with new partitioning
    !
    ! the first call is PARTITION - no need to migrate,
    ! some highly imbalanced data will be called with PARTITION too,
    ! depending on the tolerances (check parallel_domain_imbalanced of parallel.f90)
    IF (.NOT.do_firstcall) THEN
       CALL parallel_migrate (changes, par_proc_tree_old, par_proc_tree, number_of_trees, do_verb.GT.0)
       IF (ALLOCATED(par_proc_tree_old)) DEALLOCATE (par_proc_tree_old)
    END IF
    
    
    
    
!!$ END DO debug
!!$    
!!$
!!$    IF (domain_debug) THEN
!!$       IF (par_rank.EQ.0) THEN
!!$          OPEN (UNIT=123,FILE='par_proc_tree.dat',FORM='unformatted')
!!$          WRITE (123) mxyz(1:dim)*2**(j_tree_root-1) + ONE(1:dim) - prd(1:dim) ! number of trees in each direction
!!$          WRITE (123) par_proc_tree                                            ! final decomposition array
!!$          CLOSE (123)
!!$       END IF
!!$       PAUSE 'domain_debug=T induced PAUSE in parallel_domain_decompose()'
!!$    END IF
!!$    
!!$    
    IF (domain_debug) THEN
       PAUSE 'domain_debug=T induced PAUSE in parallel_domain_decompose()'
!!$       IF (par_rank.EQ.0) &
!!$            PRINT *, 'domain_debug=T induced STOP in parallel_domain_decompose()'
!!$       CALL parallel_finalize; STOP
    END IF

  END SUBROUTINE parallel_domain_decompose
  
  
  
  !-----------------------------------------------------------
  ! migrate data from old to new partition
  ! proc_to_tree_old - old
  ! proc_to_tree     - new
  ! (sig+adj nodes are to be migrated as the ones with values)
  SUBROUTINE parallel_migrate (changes, proc_to_tree_old, proc_to_tree, number_of_trees, verb)
    USE wlt_vars                                                   ! dim, j_lev, j_mx, nxyz
    INTEGER, INTENT(IN) :: number_of_trees                         !
    INTEGER, INTENT(INOUT) :: proc_to_tree_old(number_of_trees), & ! old partitioning
         proc_to_tree(number_of_trees)                             ! new partitioning
    LOGICAL, INTENT(IN) :: verb                                    ! verbose processing
    LOGICAL, INTENT(INOUT) :: changes                              ! T if migration performed
    INTEGER :: &
         ierror, &                    ! MPI related
         sdispl(0:par_size-1), &      ! ...
         rdispl(0:par_size-1), &      ! ...
         sbuff(0:par_size-1), &       ! ...
         rbuff(0:par_size-1), &       ! ...
         i, xyz(dim), &
         trees_to_send(number_of_trees), &  ! trees to send to others
         tts_to_proc_num(0:par_size-1), &   ! number of trees to send to each proc
         trees_to_recv(number_of_trees), &  ! trees to receive from others
         ttr_to_proc_num(0:par_size-1), &   ! number of trees to receive from each proc
         send_num_nodes(number_of_trees), & ! number of nodes each proc has to send to others
         recv_num_nodes(number_of_trees), &
         tot_num_to_send, &
         tot_num_to_recv, &
         n_var_DB                           ! n_var as in the database
    INTEGER*8              :: i_pm(0:dim)                                                               
    INTEGER*8, ALLOCATABLE :: &                                                                         
         index_to_send(:), &                         ! (1D coord,ID) of the nodes to be sent to others
         index_to_recv(:)                            ! (1D coord,ID) of the nodes to be received
    REAL(pr), ALLOCATABLE :: value_to_send(:,:), &   ! real values of the nodes to be sent to others
         value_to_recv(:,:)                          ! real values of the nodes to be received
!#define COMMDEBUG_migrate
#ifdef COMMDEBUG_migrate
    INTEGER, PARAMETER :: DU = 555
#endif
#ifdef TREE_NODE_POINTER_STYLE_C
    INTEGER(pointer_pr) :: c_ptr                           ! C style node pointer
#elif defined TREE_NODE_POINTER_STYLE_F
    TYPE(node), POINTER :: c_ptr                           ! (Fortran's native pointer)
#endif
    
    
    IF (ALL(proc_to_tree_old.EQ.proc_to_tree)) THEN
       IF (verb) PRINT *, 'PARALLEL_MIGRATE(): data migration is not required'
       changes = .FALSE.
       RETURN
    END IF
    
    IF (verb) PRINT *, 'PARALLEL_MIGRATE(): started'
    changes = .TRUE.
    
    ! Input arrays are global.
    ! Write trees each processor has to send -> trees_to_send(:) in processor order
    ! and set per processor counters tts_to_proc_num/ttr_to_proc_num
    tts_to_proc_num = 0
    ttr_to_proc_num = 0
    DO i=1,number_of_trees
       IF (proc_to_tree_old(i).EQ.par_rank .AND. proc_to_tree(i).NE.par_rank) THEN ! send this tree
          tts_to_proc_num(proc_to_tree(i)) = tts_to_proc_num(proc_to_tree(i)) + 1
       ELSE IF (proc_to_tree_old(i).NE.par_rank .AND. proc_to_tree(i).EQ.par_rank) THEN ! recv this tree
          ttr_to_proc_num(proc_to_tree_old(i)) = ttr_to_proc_num(proc_to_tree_old(i)) + 1
       END IF
    END DO
    sdispl(0) = 0
    rdispl(0) = 0
    sdispl(1:par_size-1) = tts_to_proc_num(0:par_size-2) ! set cumulative counters for
    rdispl(1:par_size-1) = ttr_to_proc_num(0:par_size-2) ! writing in processor order:
    DO i=2,par_size-1                                    ! displ(i) points to the beginning
       sdispl(i) = sdispl(i) + sdispl(i-1)               ! of i proc data
       rdispl(i) = rdispl(i) + rdispl(i-1)
    END DO
    tts_to_proc_num = 0
    ttr_to_proc_num = 0
    trees_to_send = 0
    trees_to_recv = 0
    DO i=1,number_of_trees
       IF (proc_to_tree_old(i).EQ.par_rank .AND. proc_to_tree(i).NE.par_rank) THEN ! send this tree to proc_to_tree(i)
          tts_to_proc_num(proc_to_tree(i)) = tts_to_proc_num(proc_to_tree(i)) + 1  ! current index in that proc groop
          trees_to_send( tts_to_proc_num(proc_to_tree(i)) + sdispl(proc_to_tree(i)) ) = i
          
       ELSE IF (proc_to_tree_old(i).NE.par_rank .AND. proc_to_tree(i).EQ.par_rank) THEN ! recv this tree
          ttr_to_proc_num(proc_to_tree_old(i)) = ttr_to_proc_num(proc_to_tree_old(i)) + 1
          trees_to_recv( ttr_to_proc_num(proc_to_tree_old(i)) + rdispl(proc_to_tree_old(i)) ) = i
          
       END IF
    END DO

    ! total number of trees to send to (receive from) others
    tot_num_to_send = SUM(tts_to_proc_num)
    tot_num_to_recv = SUM(ttr_to_proc_num)

#ifdef COMMDEBUG_migrate
    OPEN (UNIT=DU,FILE='debug.parallel_migrate'//par_rank_str)      
    WRITE (DU,*) '--- debug'//TRIM(par_rank_str)//' ---'
    WRITE (DU,*) 'Trees to be received from others: trees_to_recv:', trees_to_recv(1:tot_num_to_recv)
    WRITE (DU,*) 'ttr_to_proc_num =', ttr_to_proc_num
    WRITE (DU,*) 'Trees to be sent to others: trees_to_send:', trees_to_send(1:tot_num_to_send)
    WRITE (DU,*) 'tts_to_proc_num =', tts_to_proc_num
#endif
    
    
    ! Each processor has to find out how many nodes are in the requested trees and inform the recipients
    ! trees_to_send(1:tot_num_to_send) are tree numbers - MODE 0
    ! send_num_nodes(1:tot_num_to_send) is number of nodes per tree
    !
    ! sig+adj nodes will be sent
    CALL DB_count_tree_nodes( trees_to_send, tot_num_to_send, send_num_nodes, 0 )
    ! transform into sbuff(:), which is nodes per procesor to be sent
    sbuff = 0
    DO i=1,tot_num_to_send
       sbuff( proc_to_tree(trees_to_send(i)) ) = &
            sbuff( proc_to_tree(trees_to_send(i)) ) + send_num_nodes(i)
    END DO
    ! inform the others
    CALL MPI_ALLTOALL( sbuff, 1, MPI_INTEGER, rbuff, 1, MPI_INTEGER, &
         MPI_COMM_WORLD, ierror )
    
#ifdef COMMDEBUG_migrate
    WRITE (DU,*) 'send_num_nodes =', send_num_nodes(1:tot_num_to_send)
    WRITE (DU,*) ' sbuff, per proc', sbuff
    WRITE (DU,*) ' rbuff, per proc', rbuff
#endif
    
    ! get n_var from the database
    CALL DB_get_n_values (n_var_DB)
    
    ! write/allocate the nodes to send/receive
    ALLOCATE( index_to_send(SUM(sbuff)*2), value_to_send(1:n_var_DB,SUM(sbuff)), &
         index_to_recv(SUM(rbuff)*2), value_to_recv(1:n_var_DB,SUM(rbuff)) )
    ! index_to_send in MODE 0 is pair (1D coord, ID)
    CALL DB_write_tree_nodes( trees_to_send, tot_num_to_send, send_num_nodes, index_to_send, value_to_send, 0 )

    ! reassign trees inside the database
    CALL DB_UPDATE_PROC_INFO( proc_to_tree, number_of_trees )


    ! set transfer parameters
    sdispl(0) = 0
    rdispl(0) = 0
    DO i=1, par_size-1
       sdispl(i) = sdispl(i-1) + sbuff(i-1)
       rdispl(i) = rdispl(i-1) + rbuff(i-1)
    END DO
    
#ifdef COMMDEBUG_migrate
    WRITE (DU,*) 'index_to_send=',index_to_send
    WRITE (DU,*) 'value_to_send=',value_to_send
    WRITE (DU,*) 'sbuff =',sbuff, ',  rbuff =',rbuff
    WRITE (DU,*) 'sdispl=',sdispl,',  rdispl=',rdispl
#endif
    
    CALL MPI_ALLTOALLV( index_to_send, sbuff*2, sdispl*2, MPI_INTEGER8, &                          
         index_to_recv, rbuff*2, rdispl*2, MPI_INTEGER8, MPI_COMM_WORLD, ierror )                  
    
    CALL MPI_ALLTOALLV( value_to_send, & ! values the current processor sends to others
         sbuff*n_var_DB,               &
         sdispl*n_var_DB,              &
         MPI_DOUBLE_PRECISION,         &
         value_to_recv,                & ! values the current processor receives
         rbuff*n_var_DB,               &
         rdispl*n_var_DB,              &
         MPI_DOUBLE_PRECISION, MPI_COMM_WORLD, ierror )

#ifdef COMMDEBUG_migrate
    WRITE (DU,*) 'index_to_recv = ', index_to_recv
    WRITE (DU,*) 'value_to_recv = ', value_to_recv
    WRITE (DU,*) ' '
#endif

    ! initialize i_p array for 1d into 3d coordinate transform
    i_pm(0) = 1
    DO i=1,dim
       i_pm(i) = i_pm(i-1)*(1+mxyz(i)*2**(j_mx-1))       ! <--> j_max
    END DO

#ifdef INTEGER8_DEBUG_AR
    WRITE (*,'(A, I4, A, 4I12)')           'parallel_migrate  par_rank=', par_rank, ' i_pm=', i_pm
#endif

    
    ! write the received nodes to the database
    DO i=1,SUM(rbuff)
       xyz(1:dim) = INT(MOD(index_to_recv(2*i-1)-1,i_pm(1:dim))/i_pm(0:dim-1))  ! 3D coordinate \in [0..N-1]
       CALL DB_add_node ( xyz, j_mx, ierror, c_ptr, index_to_recv(2*i), 0 )
#ifdef TREE_NODE_POINTER_STYLE_C
       CALL DB_set_function_by_pointer ( c_ptr, 1, n_var_DB, value_to_recv(1:n_var_DB,i) )
#elif defined TREE_NODE_POINTER_STYLE_F
       c_ptr%val(1:n_var_DB) = value_to_recv(1:n_var_DB,i)
#endif
    END DO
    
    ! clean local buffers
    DEALLOCATE (index_to_send, index_to_recv, value_to_send, value_to_recv)
    
#ifdef COMMDEBUG_migrate
    CLOSE (DU)
#endif
  END SUBROUTINE parallel_migrate
  
  
  
  !-----------------------------------------------------------
  ! Synchronize (small) Array accross the processors
  ! e.g for statistics #trees(#wavelets)
  SUBROUTINE sync_arr_per_tree (Arr, par_proc_tree, number_of_trees)
    INTEGER, INTENT(IN) :: number_of_trees
    INTEGER, INTENT(INOUT) :: Arr(number_of_trees), par_proc_tree(number_of_trees)
    INTEGER :: i, tmp(number_of_trees), ierror
    
    DO i=0,par_size-1
       ! write tmp array at current ptocessor only
       IF (i.EQ.par_rank) tmp = Arr
       ! broadcast it to all the others
       CALL MPI_BCAST ( tmp, number_of_trees, MPI_INTEGER, i, MPI_COMM_WORLD, ierror)
       ! arrange the entries belonging to the current processor
       WHERE (par_proc_tree.EQ.i) Arr = tmp
    END DO
    
  END SUBROUTINE sync_arr_per_tree
  
  
  
  !-----------------------------------------------------------
  ! compute partitioning Imbalance = MIN / MAX, where
  ! MIN and MAX are the numbers of wavelets per procesor (local nwlt)
  ! Domain repartition will be forced if Imbalance < domain_imbalance_tol,
  ! where tolerance is a user defined parameter.
  ! 
  SUBROUTINE parallel_domain_imbalanced ( out_task, verb, number_of_trees )
    USE precision
    INTEGER, INTENT(OUT) :: out_task
    LOGICAL, INTENT(IN) :: verb
    INTEGER, INTENT(IN) :: number_of_trees
    REAL(pr) :: vmin, vmax, vavg, res
    INTEGER, PARAMETER :: &     ! Zoltan partitioning input parameter
         DO_NOTHING = -1, &
         PARTITION = 0,   &
         REPARTITION = 1, &
         REFINE = 2
    INTEGER :: buff(par_size), i

    
    ! count sig+adj per processor
    buff(1:par_size) = 0
    DO i=1,number_of_trees
       buff (par_proc_tree(i)+1) = buff (par_proc_tree(i)+1) + Nwlt_per_Tree(i)
    END DO
    
    
    vmin = REAL( MINVAL(buff), pr )
    vmax = REAL( MAXVAL(buff) + 0.001_pr, pr )
    vavg = REAL(SUM(buff(1:par_size)),pr)/REAL(par_size,pr)
    !res = vmin / vmax
    res = vavg / vmax
    
    
    !                                                               !
    !    Higly imbalanced data will be partitioned, moderately      !
    !    imbalanced - repartitioned, and not so imbalanced          !
    !    will be refined, depending on the Imbalance value.         !
    !                [1]              [2]          [3]              !
    !  0 ------- par_tol -------- rep_tol ---- ref_tol ---------- 1 !
    !                                                               !
    !  <-PARTITION-> <- REPARTITION-> <- REFINE -> <- DO NOTHING -> !
    !                                                               !
    
    out_task = DO_NOTHING
    IF ( res .LT. domain_imbalance_tol(3) ) out_task = REFINE
    IF ( res .LT. domain_imbalance_tol(2) ) out_task = REPARTITION
    IF ( res .LT. domain_imbalance_tol(1) ) out_task = PARTITION
    
    
    
    IF (verb) THEN
       PRINT *, '  <--------------------------------------------------->'
       PRINT *, '   In parallel_domain_imbalanced:'
       WRITE (*,'(A,2(I12,X))')    '     min/max = ', MINVAL(buff), MAXVAL(buff)
       WRITE (*,'(A,F7.5)')      '     imbalance = ', res
       WRITE (*,'(A,3(F5.3,2X))') '     tol (par,rep,ref) = ', domain_imbalance_tol(1:3)
       IF (out_task.EQ.0) THEN
          PRINT *, '   Domain is not balanced, forced PARTITION'
       ELSE IF (out_task.EQ.1) THEN
          PRINT *, '   Domain is not balanced, forced REPARTITION'
       ELSE IF (out_task.EQ.2) THEN
          PRINT *, '   Domain is not balanced, forced REFINE'
       ELSE
          PRINT *, '   Domain seems to be balanced, no action required'
       END IF
       PRINT *, '  <--------------------------------------------------->'
    END IF
    
  END SUBROUTINE parallel_domain_imbalanced
  
  
  !-----------------------------------------------------------
  ! Make sure that par_tol <= rep_tol <= ref_tol by overwriting
  ! left and write by min and max values
  SUBROUTINE parallel_check_domain_imbalance_tol
    USE precision
    REAL(pr) :: r_tmp(2)
    
    IF (par_size.GT.1.AND..NOT. &
         (domain_imbalance_tol(1).LE.domain_imbalance_tol(2).AND. &
         domain_imbalance_tol(1).LE.domain_imbalance_tol(3).AND. &
         domain_imbalance_tol(2).LE.domain_imbalance_tol(3)) ) THEN
       
       r_tmp(1) = MINVAL(domain_imbalance_tol(1:3))
       r_tmp(2) = MAXVAL(domain_imbalance_tol(1:3))
       domain_imbalance_tol(1) = r_tmp(1)
       domain_imbalance_tol(3) = r_tmp(2)
       
       IF (par_rank.EQ.0) THEN
          WRITE (*,'(" ")')
          WRITE (*,'(A)') '  WARNING: domain_imbalance_tol vector has been'
          WRITE (*,'(A)') '  reset to ensure par_tol <= rep_tol <= ref_tol'
          WRITE (*,'(A,3(F5.3,X))') '  new domain_imbalance_tol = ', domain_imbalance_tol
          WRITE (*,'(" ")')
       END IF
    END IF
    
  END SUBROUTINE parallel_check_domain_imbalance_tol
  
  
  !-----------------------------------------------------------
  ! complex pow(1/dim) based distribution
  SUBROUTINE parallel_domain_decompose_2( proc_to_tree, proc_to_tree_len, do_verb, ddec )
    USE wlt_vars                                   ! dim, j_lev, j_mx
    INTEGER, INTENT(IN) :: proc_to_tree_len
    LOGICAL, INTENT(IN) :: do_verb
    INTEGER, OPTIONAL, INTENT(IN) :: ddec(1:dim)
    INTEGER, INTENT(INOUT) :: proc_to_tree(proc_to_tree_len)
    INTEGER :: bin_limits(dim*2), i, ii, iii
    
    ! test if some dimension are dividable and reset domain_split() if necessary
    IF (par_size.GT.1.AND.ALL(domain_split(1:dim).EQ.0)) THEN
       IF (par_rank.EQ.0) THEN
          WRITE (*,'("  ----------------------------------------------------")')
          WRITE (*,'("   WARNING! domain_split is zero, does it mean we are")')
          WRITE (*,'("   using only one processor out of all the available?")')
          WRITE (*,'("   All the values of domain_split were reset to 1")')
          WRITE (*,'("  ----------------------------------------------------")')
       END IF
       domain_split = 1
    END IF
    
    
    ! array of trees has been saved in the database in fortrans (x,y,z) order,
    ! so the division of the last dimension dim on nproc^(1/dim) slabs goes first by default
    ! If domain splitting is prohibited, such direction should go first for
    ! all the processors to participate in the final division
    ! (prohibition may arize from domain_split or from prime multipliers)
    ii = dim                  ! zeroes go here
    iii = 1                   ! ones go here
    DO i=1,dim
       IF ( (.NOT.PRESENT(ddec).AND.domain_split(i).EQ.0) .OR. &
            (PRESENT(ddec).AND.ddec(i).NE.1) ) THEN
          dim_index(ii) = i
          ii = ii - 1
       ELSE
          dim_index(iii) = i
          iii = iii + 1
       END IF
    END DO
    proc_to_tree = -1
    bin_limits = 0
    
    IF (PRESENT(ddec)) THEN
       ! this is decomposition with Oleg's prime multipliers
       CALL parallel_subdivide_bin( dim, 0, par_size-1, bin_limits, proc_to_tree_len, proc_to_tree, GRID_HOLDER=ddec) !, VERB=do_verb )
       
    ELSE
       ! this is pow(1/dim) method
       CALL parallel_subdivide_bin( dim, 0, par_size-1, bin_limits, proc_to_tree_len, proc_to_tree) !, VERB=do_verb )
       
    END IF

  END SUBROUTINE parallel_domain_decompose_2
  !-----------------------------------------------------------
  RECURSIVE SUBROUTINE parallel_subdivide_bin( dim_to_div, first_proc, last_proc, &
       bin_limits, proc_to_tree_len, proc_to_tree, GRID_HOLDER, VERB )
    USE wlt_vars                             ! dim, j_lev, j_mx
    INTEGER, INTENT(IN) :: dim_to_div,    &  ! dimension to divide will be dim_index(dim_to_div)
         first_proc, last_proc,           &  ! available processor numbers
         bin_limits(dim*2),               &  ! low/high bin limits for each dimension
         proc_to_tree_len
    INTEGER, INTENT(INOUT) :: proc_to_tree(proc_to_tree_len)
    LOGICAL, OPTIONAL :: VERB
    INTEGER, OPTIONAL, INTENT(IN) :: GRID_HOLDER(1:dim) ! prime multipliers decomposition
    LOGICAL :: do_verb
    INTEGER, ALLOCATABLE :: lower_dimension_bin(:)
    INTEGER :: i, j, trees_in_dim, trees_per_proc, trees_per_bin, &
         number_of_bins, proc_num, proc_per_bin, trees_left, &
         first_proc_new, last_proc_new, bin_limits_new(dim*2), &
         tmp1, tmp2, i_p(0:dim), i_pg(0:dim), ixyz(dim), proc_left, &
         mult__proc_per_bin__trees_per_proc,                            &
         local_prod, local_side
    INTEGER*8 :: i_INTEGER8
    
    do_verb = .FALSE.
    IF (PRESENT(VERB)) do_verb = VERB
    
    IF (do_verb) THEN
       WRITE (*,'(" ")')
       WRITE (*,'(A,2I3,A,'//CHAR(ICHAR('0')+2*dim)//'I3)') 'CALL: proc limits=',first_proc, last_proc, &
            ', tree limits=',bin_limits
    END IF

    IF (dim_to_div.EQ.0) THEN ! bin_limits(:) has been completed
       i_p(0) = 1
       i_pg(0) = 1
       DO i=1,dim
          i_p(i) = i_p(i-1)*( bin_limits(2*i) - bin_limits(2*i-1) + 1 )
          i_pg(i) = i_pg(i-1)*( mxyz(i)*2**(j_tree_root-1) + 1 - prd(i) )
       END DO
       IF (do_verb) WRITE (*,'(2(A,'//CHAR(ICHAR('0')+dim+1)//'I3))') 'WRITING: i_p =',i_p,', i_pg =',i_pg
       DO i = 1,i_p(dim)
          ixyz = INT(MOD(i-1,i_p(1:dim))/i_p(0:dim-1))
          DO j=1,dim
             ixyz(j) = ixyz(j) + bin_limits(2*j-1)
          END DO
          !PRINT *, '  ixyz=',INT(MOD(i-1,i_p(1:dim))/i_p(0:dim-1)), '-->', ixyz
          
          i_INTEGER8 = 1+SUM(ixyz(1:dim)*i_pg(0:dim-1))
          IF (i_INTEGER8.GT.proc_to_tree_len) THEN
             PRINT *, 'i_INTEGER8 index is greater than number of trees'
             CALL parallel_finalize; STOP
          END IF
          IF (proc_to_tree(i_INTEGER8).NE.-1) THEN
             PRINT *, 'already assigned to', proc_to_tree (i_INTEGER8), first_proc
             CALL parallel_finalize; STOP
          END IF
          proc_to_tree (i_INTEGER8) = first_proc
       END DO
    ELSE
       ! propagate slab limits from the previous dimension
       bin_limits_new = bin_limits
       ! number of available processors
       proc_num = last_proc - first_proc + 1
       ! number of trees in that dimension
       trees_in_dim = mxyz(dim_index(dim_to_div))*2**(j_tree_root-1) + 1 - prd(dim_index(dim_to_div))
       local_side = mxyz(dim_index(dim_to_div))*2**(j_tree_root-1)
       local_prod = 1
       DO i=dim_to_div,1,-1
          local_prod = local_prod * mxyz(dim_index(i))*2**(j_tree_root-1)
       END DO
       ! subdivide current dimension so many times
       ! (if allowed by domain_split)
       number_of_bins = MIN(  NINT( local_side/EXP(LOG(local_prod/REAL(proc_num))/REAL(dim_to_div)) ), trees_in_dim )

!!$       number_of_bins = MIN(  NINT( EXP(LOG(REAL(proc_num))/REAL(dim_to_div)) ), trees_in_dim )

       IF (domain_split(dim_index(dim_to_div)).EQ.0) THEN 
          number_of_bins = 1
       ELSE
          ! overwrite number of bins (except for the last dimention):
          IF (dim_to_div.GT.1) THEN
             !-- by prime multipliers
             IF (PRESENT(GRID_HOLDER)) &
                  number_of_bins = MIN( GRID_HOLDER(dim_index(dim_to_div)), trees_in_dim )
             !-- by input parameter domain_div
             IF (domain_div(dim_index(dim_to_div)).NE.0) &
                  number_of_bins = MIN( domain_div(dim_index(dim_to_div)), trees_in_dim )
          END IF
       END IF
       proc_per_bin = proc_num / number_of_bins
       trees_per_bin = trees_in_dim / number_of_bins
       trees_per_proc = trees_in_dim / proc_num
              
       ALLOCATE( lower_dimension_bin(1:number_of_bins*2) )
       ! distribute processors through the subdivisions
       ! fields in lower_dimension_bin: 1-number of processors per that bin
       !                                2-number of trees per that bin
       trees_left = trees_in_dim
       proc_left = proc_num
       mult__proc_per_bin__trees_per_proc = proc_per_bin*trees_per_proc
       IF (trees_per_proc.EQ.0) mult__proc_per_bin__trees_per_proc = trees_per_bin

       DO i=1,number_of_bins
          lower_dimension_bin (2*i-1) = proc_per_bin
          lower_dimension_bin (2*i) = mult__proc_per_bin__trees_per_proc
          trees_left = trees_left - mult__proc_per_bin__trees_per_proc
          proc_left = proc_left - proc_per_bin
       END DO
       IF (do_verb) PRINT *,' 1) trees_left, proc_left',trees_left, proc_left
       ! backward filling will take care of non-fat trees on upper boundaries
       ! (distribute the rest of the processors and some trees)
       DO i=number_of_bins,1,-1 !number_of_bins-proc_num+proc_per_bin*number_of_bins+1,-1
          IF (proc_left.GE.1) THEN
             lower_dimension_bin (2*i-1) = lower_dimension_bin (2*i-1) + 1
             proc_left = proc_left - 1
             IF (trees_left.GE.trees_per_proc) THEN
                lower_dimension_bin (2*i) = lower_dimension_bin (2*i) + trees_per_proc
                trees_left = trees_left - trees_per_proc
             END IF
          END IF
          IF (dim_to_div.GT.1) THEN
             ! for first subduvisions prefer equal tree distribution
             ! over equal tree per processor one
             IF (trees_left.GE.trees_per_proc) THEN
                lower_dimension_bin (2*i) = lower_dimension_bin (2*i) + trees_per_proc
                trees_left = trees_left - trees_per_proc
             END IF
          END IF
       END DO
       
       IF (do_verb) THEN
          PRINT *,' 2) trees_left, proc_left',trees_left, proc_left
          PRINT *,'   dim=           ',dim_index(dim_to_div), 'number_of_bins=',number_of_bins
          PRINT *,'   proc_per_bin=  ',proc_per_bin,  'trees_in_dim=  ',trees_in_dim
          PRINT *,'   trees_per_proc=',trees_per_proc,'trees_per_bin= ',trees_per_bin
          PRINT *,'   lower_dimension_bin(:)=',lower_dimension_bin
       END IF
       ! distribute the rest of the trees among the processors
       ! (backward, for non-fat trees)
       IF (trees_left.GT.0) THEN
          trees_left_loop:DO i=number_of_bins,1,-1 !per each bin
             DO j=1,lower_dimension_bin (2*i-1)    !per each processor
                lower_dimension_bin (2*i) = lower_dimension_bin (2*i) + 1
                trees_left = trees_left - 1
                IF (trees_left.EQ.0) EXIT trees_left_loop
             END DO
          END DO trees_left_loop
       END IF
       
       ! DEBUG
       IF (do_verb) THEN
          !tmp1=0; tmp2=0
          DO i=1,number_of_bins
             PRINT *, '   bin (p,t)', lower_dimension_bin (2*i-1), lower_dimension_bin (2*i)
             !tmp1=tmp1+lower_dimension_bin (2*i-1); tmp2=tmp2+lower_dimension_bin (2*i)
          END DO
          !PRINT *,    '   tot:     ',tmp1,tmp2
       END IF
       

       ! set processor limits: first_proc, last_proc and slab limits (first/last tree)
       first_proc_new = first_proc
       bin_limits_new (dim_index(dim_to_div)*2-1) = 0
       DO i=1,number_of_bins
          last_proc_new = first_proc_new + lower_dimension_bin (2*i-1) - 1
          bin_limits_new (dim_index(dim_to_div)*2) = bin_limits_new (dim_index(dim_to_div)*2-1) + lower_dimension_bin (2*i) - 1
          CALL parallel_subdivide_bin( dim_to_div-1, first_proc_new, last_proc_new, &
               bin_limits_new, proc_to_tree_len, proc_to_tree, VERB=do_verb )
          first_proc_new = last_proc_new + 1
          bin_limits_new (dim_index(dim_to_div)*2-1) = bin_limits_new (dim_index(dim_to_div)*2) + 1
       END DO
       DEALLOCATE (lower_dimension_bin)
    END IF
    
  END SUBROUTINE parallel_subdivide_bin

  
  !-----------------------------------------------------------
  ! read decomposition from IC
  SUBROUTINE parallel_read_IC( proc_to_tree, number_of_trees )
    INTEGER, INTENT(IN) :: number_of_trees
    INTEGER, INTENT(INOUT) :: proc_to_tree(number_of_trees)

    IF (SIZE(IC_par_proc_tree).NE.number_of_trees) THEN
       IF (par_rank.EQ.0) THEN
          WRITE (*,'("Error in converting domain decomposition from .res file.")')
          WRITE (*,'("Try to set -m method or run on a single processor.")')
       END IF
       CALL parallel_finalize; STOP 1
    END IF
    proc_to_tree(1:number_of_trees) = IC_par_proc_tree(1:number_of_trees)
  END SUBROUTINE parallel_read_IC
  
  !-----------------------------------------------------------
  ! fair fat tree distribution
  SUBROUTINE parallel_domain_decompose_1( proc_to_tree, number_of_trees )
    USE wlt_vars                                   ! dim, j_lev, j_mx
    INTEGER, INTENT(IN) :: number_of_trees
    INTEGER, INTENT(INOUT) :: proc_to_tree(number_of_trees)
    INTEGER :: i, chunk,                   & !
         ft_count, pr_count,               & !
         t_coord(1:dim),                   & ! tree coordinates
         t_max_coord(1:dim),               & ! tree coordinate limits
         i_pt(0:dim), i_pft(0:dim)           ! 1D tree and fat tree indexing
    
    i_pt(0) = 1
    i_pft(0) = 1
    DO i=1,dim
       i_pt(i) = i_pt(i-1)*( mxyz(i)*2**(j_tree_root-1) + 1 - prd(i) )
       i_pft(i) = i_pft(i-1)*( mxyz(i)*2**(j_tree_root-1) )
       t_max_coord(i) =  mxyz(i)*2**(j_tree_root-1)
    END DO
    
!#ifdef INTEGER8_DEBUG_AR
    IF (par_rank.EQ.0) THEN
       IF (dim.EQ.3) &
          WRITE (*,'(A, I6, A, 4I12, A, 4I12, A, 3I12)')   'parallel_domain_decompose_1  par_rank=', par_rank, ' i_pt=', i_pt, ' i_pft=', i_pft, ' t_max_coord=', t_max_coord 
       IF (dim.EQ.2) &
          WRITE (*,'(A, I6, A, 3I12, A, 3I12, A, 2I12)')   'parallel_domain_decompose_1  par_rank=', par_rank, ' i_pt=', i_pt, ' i_pft=', i_pft, ' t_max_coord=', t_max_coord 
    ENDIF
!#endif  
    
    ft_count = 0                                    ! fat tree counter
    pr_count = 0                                    ! processor counter
    chunk = MAX( 1, i_pft(dim)/par_size )           ! processor's chunk
    DO i=1,i_pt(dim)                                ! tree counter
       
       t_coord(1:dim) = INT(MOD(i-1,i_pt(1:dim))/i_pt(0:dim-1))
       
       IF (ALL(t_coord(1:dim).NE.t_max_coord(1:dim))) THEN
          ft_count = ft_count + 1
       END IF
       IF (ft_count.GT.chunk) THEN
          ft_count = 1
          pr_count = MIN( pr_count+1, par_size-1 )
       END IF
       proc_to_tree(i) = pr_count

    END DO

  END SUBROUTINE parallel_domain_decompose_1

  !-----------------------------------------------------------
  ! simple domain decomposition based on the total tree number,
  ! or, if number_of_fat_trees present, on the number of fat (non-boundary) trees
  SUBROUTINE parallel_domain_decompose_10( proc_to_tree, number_of_trees, number_of_fat_trees )
    INTEGER, INTENT(IN) :: number_of_trees
    INTEGER, INTENT(IN), OPTIONAL :: number_of_fat_trees
    INTEGER, INTENT(INOUT) :: proc_to_tree(number_of_trees)
    INTEGER :: i, div
    
    div = number_of_trees
    IF (PRESENT(number_of_fat_trees)) div = number_of_fat_trees
    
    DO i=1,number_of_trees
       proc_to_tree(i) = MIN( (par_size*(i-1))/div, par_size-1 )
    END DO
    
  END SUBROUTINE parallel_domain_decompose_10


  !-----------------------------------------------------------
  ! pretty print array
  SUBROUTINE pretty_print_array (arr,len,str,verb)
    USE precision
    USE wlt_vars                                   ! prd, mxyz, j_tree_root
    INTEGER, INTENT(IN) :: len, arr(len)
    LOGICAL, INTENT(IN) :: verb
    CHARACTER (LEN=*),INTENT(IN) :: str
    INTEGER :: xlen, ii, field
    
    IF (verb) THEN
       xlen = mxyz(1)*2**(j_tree_root-1) + 1 - prd(1) ! length in x-direstion
       ii = 0
       field = CEILING( LOG(REAL(MAXVAL(arr)+1,pr))/LOG(10.0_pr) ) + 1
       
       WRITE (*,'(" +-> X")')
       WRITE (*,'(" | ",A)') TRIM(str)
       WRITE (*,'(" Y")')
       DO WHILE(ii.LT.len)
          ii = ii + 1
          WRITE (*,'(I'//CHAR(ICHAR('0')+field)//')', ADVANCE='NO') arr(ii)
          IF (MOD(ii,xlen).EQ.0) WRITE (*,'(" ")')
       END DO
    END IF
  END SUBROUTINE pretty_print_array
  
  
  !-----------------------------------------------------------
  ! Zoltan library based decomposition
  SUBROUTINE parallel_domain_decompose_zoltan( proc_to_tree, number_of_trees, Nwlt_per_edge, task, verb )
    USE wlt_vars                                   ! dim, j_lev, j_mx
    USE wlt_trns_util_mod                          ! set xx
    USE domain_zoltan                              ! zoltan_domain_decomp
    USE debug_vars
    
    INTEGER, INTENT(IN) :: number_of_trees                  ! number of vertices to (re)partition
    INTEGER, INTENT(INOUT) :: proc_to_tree(number_of_trees) ! vertex-to-tree map
    INTEGER, INTENT(IN) :: task                             ! 0-partition, 1-repartition, 2-refine
    INTEGER, DIMENSION(1:dim,1:number_of_trees), INTENT(IN) :: Nwlt_per_edge                             !number of wavelets on a tree edge
    
    INTEGER :: mxyz_grid(dim), &                   ! number of trees in each direction
         max_n_prdct_updt(2), &                    ! n_updt, n_prdct
         i_p(0:dim), i, j, ixyz(dim), ii, err, &
         nxyz_xx_maxval, &
         domain_zoltan_meth                        ! 1-Geometric, 3-HyperGraph
    
    REAL(pr) :: &
         Vertices_Coordinates(dim*number_of_trees), &
         h_tmp(1:j_tree_root,1:dim), &
         xx_tmp((number_of_trees+1)*dim)           ! 1D analog of xx(maxnxyz,dim) array
    
    REAL(pr), PARAMETER :: &
         EdgeWeight_Global_SafetyFactor = 0.5_pr   !0.1 or so (read the manual)
    
    INTEGER, PARAMETER :: &     ! Zoltan partitioning input parameter
         PARTITION = 0,   &
         REPARTITION = 1, &
         REFINE = 2,      &
         GEOMETRIC = 1,   &
         HYPERGRAPH = 3,  &
         HSFC = 4        
    LOGICAL, INTENT(IN) :: verb
    
    
    ! set internal domain_zoltan_meth
    ! (make sure it corresponds to the Table (\ref{domain_meth_table}) of the manual
    !  and to the switch of parallel_domain_decompose subroutine)
    domain_zoltan_meth = 0
    IF (domain_meth.EQ.2) domain_zoltan_meth = GEOMETRIC
    IF (domain_meth.EQ.3) domain_zoltan_meth = HYPERGRAPH
    IF (domain_meth.EQ.4) domain_zoltan_meth = HSFC
    
    
    ! check available partitioning methods
    ! (same check as inside zoltan_domain_decomp)
    IF (domain_zoltan_meth.EQ.0) THEN
       IF (par_rank.EQ.0) PRINT *, 'ERROR: this partitioning method is not currently supported.'
       IF (par_rank.EQ.0) PRINT *, '       Set domain_meth=2, 3 or 4 for Geometric, HyperGraph, or Space-Filling Curve.'
       CALL parallel_finalize; STOP
    END IF
    
    
    ! preliminary number of trees in each direction ---------\
    ! (without periodicity, for the use with set_xx)         |
    ! (will be corrected below)                              |
    mxyz_grid(1:dim) = mxyz(1:dim)*2**(j_tree_root-1)
    nxyz_xx_maxval = MAXVAL(mxyz_grid(1:dim))
    
    !   preliminary tree roots real coordinates (for geometrical partitioning)
    !   (using nxyz style mxyz_grid array in set_xx)
    CALL set_xx (xx_tmp, h_tmp, mxyz_grid, nxyz_xx_maxval, j_tree_root)
    
    
    ! final number of trees in each direction                |
    ! (the correction for periodicity)              ---------/
    mxyz_grid(1:dim) =  mxyz_grid(1:dim) + 1 - prd(1:dim)
    
    
    !   writing tree roots real coordinates (for geometrical partitioning)
    Vertices_Coordinates = 0.0_pr
    i_p(0) = 1
    DO i=1,dim
       i_p(i) = i_p(i-1)*mxyz_grid(i)
    END DO

#ifdef INTEGER8_DEBUG_AR
    IF (par_rank.EQ.0) THEN
       IF (dim.EQ.3) &
          WRITE (*,'(A, I6, A, 4I12, A, 4I12, A, 3I12)')   'parallel_domain_decompose_zoltan  par_rank=', par_rank, ' i_p=', i_p
       IF (dim.EQ.2) &
          WRITE (*,'(A, I6, A, 3I12, A, 3I12, A, 2I12)')   'parallel_domain_decompose_zoltan  par_rank=', par_rank, ' i_p=', i_p
    ENDIF
#endif  
    
    
    ii = 1
    DO i=1,number_of_trees
       ixyz(1:dim) = INT(MOD(i-1,i_p(1:dim))/i_p(0:dim-1))
       DO j=1,dim
          ! xx_tmp has been written as (0:MAXVAL,1:dim)
          Vertices_Coordinates(ii) = xx_tmp( ixyz(j)+1 + (j-1)*(nxyz_xx_maxval+1) )
          ii = ii + 1
       END DO
    END DO
    
    
    ! import n_updt/prdct values for edge weighting
    max_n_prdct_updt(1) = MAXVAL(n_updt)
    max_n_prdct_updt(2) = MAXVAL(n_prdct)
    
    
    IF (verb) THEN
       PRINT *, 'BEFORE Zoltan'
       IF (task.EQ.PARTITION) THEN
          PRINT *, '       TASK = PARTITION'
       ELSE IF (task.EQ.REPARTITION) THEN
          PRINT *, '       TASK = REPARTITION'
       ELSE IF (task.EQ.REFINE) THEN
          PRINT *, '       TASK = REFINE'
       ELSE
          PRINT *, '       TASK = unknown'
       END IF
    END IF
    
    CALL zoltan_domain_decomp( dim, &
         number_of_trees, &
         mxyz_grid, &
         par_size, &
         Nwlt_per_Tree, &
         Nwlt_per_Edge, &
         max_n_prdct_updt(1), &
         max_n_prdct_updt(2), &
         proc_to_tree, &
         task , &                              ! 0-partition, 1-repartition, 2-refine
         domain_zoltan_meth, &                 ! 1-Geometric, 2-Graph, 3-HyperGraph
         EdgeWeight_Global_SafetyFactor, &     ! 0.1 or so (read the manual)
         Vertices_Coordinates, &               ! (useg for geometric partitioning)
         domain_zoltan_verb, &                 ! Zoltan debug level (0-10)
         err, &                                ! should be zero if everything is OK
         par_rank )
    

    CALL parallel_global_sum( INTEGER=err )
    IF (err.NE.0) THEN
       IF (par_rank.EQ.0) PRINT *, 'ERROR: internal Zoltan library error.'
       IF (par_rank.EQ.0) PRINT *, '       Terminating ...'
       CALL parallel_finalize; STOP
    END IF
    
    
    IF (verb.AND.BTEST(debug_level,3)) THEN
       PRINT *, 'AFTER Zoltan'
       PRINT *, '       Nwlt_per_Tree=', Nwlt_per_Tree
    END IF

    
  END SUBROUTINE parallel_domain_decompose_zoltan
  
  !-----------------------------------------------------------
  ! deallocate the module
  SUBROUTINE parallel_finalize
    INTEGER :: ierror
    
    IF (ALLOCATED(par_proc_tree)) DEALLOCATE( par_proc_tree )
    IF (ALLOCATED(Nwlt_per_Tree)) DEALLOCATE( Nwlt_per_Tree )
    IF (ALLOCATED(IC_par_proc_tree)) DEALLOCATE( IC_par_proc_tree )
    IF (ALLOCATED(IC_Nwlt_per_Tree)) DEALLOCATE(IC_Nwlt_per_Tree )  !OLEG: 07.14.2011 for beter load balancing of restart files
    
    IF (ALLOCATED(procbuffer)) DEALLOCATE( procbuffer )
    !print *, "Waiting for finalize on proc", par_rank
    CALL MPI_BARRIER( MPI_COMM_WORLD, ierror )
    CALL MPI_FINALIZE(ierror)
    
  END SUBROUTINE parallel_finalize

  SUBROUTINE parallel_gather_rank(MyRank_nwlt_gather)
    USE sizes
    USE pde

    INTEGER :: ierror 
    INTEGER, INTENT(INOUT) :: MyRank_nwlt_gather(0:par_size-1) ! 

    MyRank_nwlt_gather(0:par_size-1) = 0
    MyRank_nwlt_gather(par_rank) = nwlt
   !PRINT *,  '1parallel_comm_rank=', par_rank, ' nwlt = ', nwlt
   !PRINT *,  '1parallel_comm_rank=', par_rank, ' recv(:) = ', MyRank_nwlt_gather(:)
    
    CALL MPI_GATHER( nwlt, 1, MPI_INTEGER, MyRank_nwlt_gather, 1, MPI_INTEGER, 0, MPI_COMM_WORLD, ierror); 
    
   !PRINT *,  '2parallel_comm_rank=', par_rank, ' recv(:) = ', MyRank_nwlt_gather(:)
  END SUBROUTINE parallel_gather_rank
     

  
  !=========================!
  ! SERIAL MODE SUBROUTINES !
  !=========================!
#else
  SUBROUTINE parallel_comm
  END SUBROUTINE parallel_comm
  SUBROUTINE parallel_comm_wgh
  END SUBROUTINE parallel_comm_wgh
  SUBROUTINE parallel_comm_bnd
  END SUBROUTINE parallel_comm_bnd
  SUBROUTINE parallel_comm_adj
  END SUBROUTINE parallel_comm_adj
  SUBROUTINE parallel_test_new_grid
  END SUBROUTINE parallel_test_new_grid
  !--------------------------------------------------------
  SUBROUTINE parallel_global_sum (LOGICALOR, INTEGER, REAL, REALMAXVAL, INTEGERMAXVAL, REALMINVAL)
    USE precision
    LOGICAL, INTENT(INOUT), OPTIONAL :: LOGICALOR
    INTEGER, INTENT(INOUT), OPTIONAL :: INTEGER, INTEGERMAXVAL
    REAL(pr), INTENT(INOUT), OPTIONAL :: REAL, REALMAXVAL, REALMINVAL
  END SUBROUTINE parallel_global_sum
  !--------------------------------------------------------
  SUBROUTINE parallel_vector_sum (LOGICALOR,INTEGER, REAL, REALMAXVAL, INTEGERMAXVAL, REALMINVAL,LENGTH)
    USE precision
    INTEGER, INTENT(IN) :: LENGTH
    LOGICAL, DIMENSION(LENGTH), INTENT(INOUT), OPTIONAL :: LOGICALOR
    INTEGER, DIMENSION(LENGTH), INTENT(INOUT), OPTIONAL :: INTEGER, INTEGERMAXVAL
    REAL(pr), DIMENSION(LENGTH), INTENT(INOUT), OPTIONAL :: REAL, REALMAXVAL, REALMINVAL
  END SUBROUTINE parallel_vector_sum
  !--------------------------------------------------------
  SUBROUTINE parallel_broadcast ( REAL )
    USE precision
    REAL(pr), INTENT(INOUT) :: REAL
  END SUBROUTINE parallel_broadcast
  !--------------------------------------------------------
  SUBROUTINE parallel_broadcast_int ( INTEGER_INOUT )
    USE precision
    INTEGER, INTENT(INOUT) :: INTEGER_INOUT
  END SUBROUTINE parallel_broadcast_int
  !--------------------------------------------------------
  SUBROUTINE parallel_broadcast_int_array ( INTEGER_ARRAY_INOUT, LENGTH )
    USE precision
    INTEGER, INTENT(INOUT) :: INTEGER_ARRAY_INOUT(:,:)
    INTEGER, INTENT(IN)    :: LENGTH
    INTEGER                :: ierror
  END SUBROUTINE parallel_broadcast_int_array
  !--------------------------------------------------------
  SUBROUTINE parallel_broadcast_int_1D_array ( INTEGER_ARRAY_INOUT, LENGTH )
    USE precision
    INTEGER, INTENT(INOUT) :: INTEGER_ARRAY_INOUT(:)
    INTEGER, INTENT(IN)    :: LENGTH
    INTEGER                :: ierror
  END SUBROUTINE parallel_broadcast_int_1D_array
  SUBROUTINE parallel_broadcast_char ( CHARACTER_INOUT, LENGTH )
    USE precision
    CHARACTER (LEN=LENGTH), INTENT(INOUT) :: CHARACTER_INOUT
    INTEGER,                INTENT(IN)    :: LENGTH
    INTEGER                               :: ierror
  END SUBROUTINE parallel_broadcast_char

  !--------------------------------------------------------
  SUBROUTINE parallel_gather( REALARRAYIN, REALARRAYOUT, LENGTH )
    USE precision
    INTEGER, INTENT(IN) :: LENGTH
    REAL(pr), INTENT(INOUT) :: REALARRAYIN(:), REALARRAYOUT(:)
  END SUBROUTINE parallel_gather
  !--------------------------------------------------------
  SUBROUTINE parallel_init
    par_rank_str = ''
    ! this is to shut up compiler warnings only
    ! par_proc_tree and Nwlt_per_Tree are not to be used in single processor mode
    ALLOCATE( par_proc_tree(1), Nwlt_per_tree(1) )
    par_proc_tree = 0
    Nwlt_per_Tree = 0
    ! number of sig/adj nodes to be sent/recieved by the current processor
    n_transfer_send = 0
    n_transfer_recv = 0
  END SUBROUTINE parallel_init
  !--------------------------------------------------------
  SUBROUTINE parallel_set_files_to_read (fntr, IC_mxyz, IC_prd)
    USE wlt_vars                                ! dim, j_lev, j_mx
    LOGICAL, POINTER :: fntr(:)                 ! which .res files to read
    INTEGER, POINTER :: IC_mxyz(:), IC_prd(:)
    fntr(1:IC_par_size) = .TRUE.
  END SUBROUTINE parallel_set_files_to_read
  !--------------------------------------------------------
  SUBROUTINE parallel_domain_decompose( changes, VERBLEVEL, METH, FIRSTCALL )
    LOGICAL, INTENT(INOUT) :: changes
    INTEGER, INTENT(IN), OPTIONAL :: VERBLEVEL
    INTEGER, INTENT(IN), OPTIONAL :: METH
    LOGICAL, INTENT(IN), OPTIONAL :: FIRSTCALL
  END SUBROUTINE parallel_domain_decompose

!--------------------------------------------------------
  SUBROUTINE pretty_print_array (arr,len,str,verb)
    INTEGER, INTENT(IN) :: len, arr(len)
    LOGICAL, INTENT(IN) :: verb
    CHARACTER (LEN=*),INTENT(IN) :: str
  END SUBROUTINE pretty_print_array
  !--------------------------------------------------------
  SUBROUTINE parallel_finalize
    IF (ALLOCATED(par_proc_tree)) DEALLOCATE( par_proc_tree )
    IF (ALLOCATED(IC_par_proc_tree)) DEALLOCATE( IC_par_proc_tree )
    IF (ALLOCATED(IC_Nwlt_per_Tree)) DEALLOCATE(IC_Nwlt_per_Tree )  !OLEG: 07.14.2011 for beter load balancing of restart files
  END SUBROUTINE parallel_finalize
  
  SUBROUTINE parallel_gather_rank(MyRank_nwlt_gather)
    USE sizes
    USE pde

    INTEGER, INTENT(INOUT) :: MyRank_nwlt_gather(0:par_size-1) ! 

    MyRank_nwlt_gather(0:par_size-1) = 0
    MyRank_nwlt_gather(par_rank) = nwlt
  END SUBROUTINE parallel_gather_rank
    
#endif



  !==================================!
  ! GENERAL SUBROUTINES FOR ANY MODE !
  !==================================!
  
  ! terminate if non-zero input
  SUBROUTINE test_error (ierror)
    INTEGER, INTENT(IN) :: ierror
    
    IF (ierror.EQ.0) RETURN
    CALL parallel_finalize
    STOP
  END SUBROUTINE test_error
  
  ! provides a way to synchronize the communicator
  SUBROUTINE parallel_sync ()
    IMPLICIT NONE
    INTEGER :: ierror

#ifdef MULTIPROC
    CALL MPI_BARRIER( MPI_COMM_WORLD, ierror )
#endif
    
  END SUBROUTINE parallel_sync


END MODULE parallel


#ifdef COMM_ONE2ONE_RKL
#undef COMM_ONE2ONE_RKL
#endif
