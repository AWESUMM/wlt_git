!
! SGS fixed smagorinsky 
!

MODULE SGS_compressible_SM

  USE precision
  USE wlt_vars
  USE wavelet_filters_mod
  USE wlt_trns_mod
  USE wlt_trns_vars
  USE wlt_trns_util_mod
  USE io_3d_vars
  USE util_mod
  USE util_vars
  USE share_consts
  USE pde
  USE sizes
  USE share_kry
  USE vector_util_mod
  USE input_file_reader
  USE equations_compressible 
  USE sgs_util 
  USE error_handling

  PUBLIC :: &
    fixSM_compress_read_input, &
    fixSM_compress_rhs, &
    fixSM_compress_drhs, &
    fixSM_compress_drhs_diag, &
    calc_fixSM_eddy_visc

  ! Constant Smagorinsky Coefficients
  REAL (pr), PRIVATE :: &
    Cs_in           = 0.0_pr, &
    Ciso_in         = 0.0_pr, &
    Pra_t_in        = 0.0_pr, &
    Sc_t_in         = 0.0_pr

CONTAINS

  !
  ! Read input from "case_name"_pde.inp file
  ! case_name is in string file_name read from command line
  ! in read_command_line_input()
  ! 
  SUBROUTINE fixSM_compress_read_input()
    IMPLICIT NONE

      call input_real ('Cs_in',Cs_in,'stop', &
        ' Cs_in ! Eddy viscosity coefficient (for set coefficient)')
      call input_real ('Ciso_in',Ciso_in,'stop', &
        ' Ciso_in ! Eddy viscosity coefficient for modeled isotropic part (for set coefficient)')
      call input_real ('Pra_t_in',Pra_t_in,'stop', &
        ' Pra_t_in ! Turbulent Prandtl number (for set coefficient)')
      call input_real ('Sc_t_in',Sc_t_in,'stop', &
        ' Sc_t_in ! Turbulent Schmidt number (for set coefficient)')

  END SUBROUTINE fixSM_compress_read_input


  SUBROUTINE fixSM_compress_rhs(fixSM_rhs,u_integrated, meth )
    IMPLICIT NONE
    REAL (pr), DIMENSION (ng,ne), INTENT(IN) :: u_integrated !1D flat version of integrated variables without BC 
    REAL (pr), DIMENSION (ng*ne), INTENT(INOUT) :: fixSM_rhs
    INTEGER, INTENT(IN) :: meth 

    REAL (pr), DIMENSION(nwlt,3) :: v 
    REAL (pr), DIMENSION(1,nwlt,3) :: du,d2u 
    REAL (pr), DIMENSION(nwlt,6) :: S_ij
    REAL (pr), DIMENSION(nwlt)   :: Smod
    INTEGER :: idim, shift

    ! Isotropic part
    IF( Ciso_in .GT. 1.0e-10_pr ) THEN
      DO idim = 1,dim
        v(:,idim) = u_integrated(:,n_mom(idim)) / u_integrated(:,n_den)
      END DO
      CALL Sij( v(:,1:3), nwlt, S_ij, Smod, .TRUE. ) !calculate both Sij* and |S*|, assume correct 

      v(:,1) = 2.0_pr * Ciso_in * Ciso_in * u_integrated(:,n_den) * delta(:) * delta(:) * Smod(:) * Smod(:)
      CALL c_diff_fast(v(:,1), du(1,:,:), d2u(1,:,:), j_lev, nwlt, meth, 10, 1, 1, 1)

      DO idim = 1,dim
        shift=(n_mom(idim)-1)*nwlt
        fixSM_rhs(shift+1:shift+nwlt) = fixSM_rhs(shift+1:shift+nwlt) - du(1,:,idim)
      END DO

    END IF

  END SUBROUTINE fixSM_compress_rhs


  SUBROUTINE fixSM_compress_Drhs(fixSM_Drhs, u_p, u_prev, meth )
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: meth
    REAL (pr), DIMENSION (ng,ne), INTENT(IN) :: u_p, u_prev
    REAL (pr), DIMENSION (ng*ne), INTENT(INOUT) :: fixSM_Drhs

    REAL (pr), DIMENSION(nwlt,3) :: v_p, v_prev 
    REAL (pr), DIMENSION(1,nwlt,3) :: du,d2u 
    REAL (pr), DIMENSION(nwlt,6) :: S_ij
    REAL (pr), DIMENSION(nwlt)   :: Smod_p, Smod_prev
    INTEGER :: idim, shift
    ! Isotropic part
    IF( Ciso_in .GT. 1.0e-10_pr ) THEN
      DO idim = 1,dim
        v_prev(:,idim) = u_prev(:,n_mom(idim)) / u_prev(:,n_den)
        v_p(:,idim) = ( u_p(:,n_mom(idim)) * u_prev(:,n_den) - u_prev(:,n_mom(idim)) * u_p(:,n_den) ) / ( u_prev(:,n_den) * u_prev(:,n_den) )
      END DO
      CALL Sij( v_p(:,1:3), nwlt, S_ij, Smod_p, .TRUE. ) !calculate both Sij* and |S*|, assume correct 
      CALL Sij( v_prev(:,1:3), nwlt, S_ij, Smod_prev, .TRUE. ) !calculate both Sij* and |S*|, assume correct 

      v_p(:,1) = 2.0_pr * Ciso_in * Ciso_in * delta(:) * delta(:) * ( & 
            u_p(:,n_den) * Smod_prev(:) * Smod_prev(:) &
            + 2.0_pr * u_prev(:,n_den) * Smod_prev(:) * Smod_p(:) &
          )

      CALL c_diff_fast(v_p(:,1), du(1,:,:), d2u(1,:,:), j_lev, nwlt, meth, 10, 1, 1, 1)

      DO idim = 1,dim
        shift=(n_mom(idim)-1)*nwlt
        fixSM_drhs(shift+1:shift+nwlt) = fixSM_drhs(shift+1:shift+nwlt) - du(1,:,idim)
      END DO

    END IF

  END SUBROUTINE fixSM_compress_Drhs

  SUBROUTINE fixSM_compress_Drhs_diag(fixSM_diag,u_prev,meth)
    USE variable_mapping
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: meth
    REAL (pr), DIMENSION (nwlt*n_integrated), INTENT(INOUT) :: fixSM_diag
    REAL (pr), DIMENSION (nwlt,n_integrated), INTENT(IN) :: u_prev

    REAL (pr), DIMENSION(nwlt,3) :: v_prev 
    REAL (pr), DIMENSION(3,nwlt,3) :: du,d2u 

   ! Isotropic part
   IF( Ciso_in .GT. 1.0e-10_pr ) THEN
      ! ERIC: need to finish
      !DO idim = 1,dim
      !  v_prev(:,idim) = u_prev(:,n_mom(idim)) / u_prev(:,n_den)
      !END DO

      !CALL c_diff_fast(v_prev(:,1:dim), du(1:dim,:,:), d2u(1:dim,:,:), j_lev, nwlt, meth, 10, dim, 1, dim)

   END IF

  END SUBROUTINE fixSM_compress_Drhs_diag

  ! Constant Ceofficient Smagorinsky 
  SUBROUTINE calc_fixSM_eddy_visc( mu_t_out, kk_t_out, bd_t_out, u_in ) 
    USE equations_compressible 
    USE variable_mapping 
    USE equations_compressible_vars
    IMPLICIT NONE
    REAL (pr), DIMENSION (nwlt,n_integrated), INTENT (IN)    :: u_in
    REAL (pr), DIMENSION(nwlt),               INTENT (OUT)   :: mu_t_out, kk_t_out, bd_t_out 

    REAL(pr), DIMENSION(nwlt,dim) :: v 
    REAL (pr), DIMENSION(nwlt,dim*(dim+1)/2) :: s_ij
    REAL (pr), DIMENSION(nwlt)               :: smod
    REAL (pr), DIMENSION(nwlt)               :: cp_field
    INTEGER :: i,l


    ! Calculate modulus of strain rate
    DO i = 1,dim
       v(1:nwlt,i) = u_in(1:nwlt,n_mom(i)) / u_in(1:nwlt,n_den)
    END DO
    CALL Sij( v, nwlt, s_ij, smod, .TRUE.) 

    ! Calculate the eddy viscosity
    mu_t_out(:) = u_in(:,n_den) * ( delta(:) * Cs_in )**2 * smod(:)

    ! Calculation of conductivity. Could use an averaging function 
    cp_field(:) = cp_in(Nspec) !cp_Nspec 
    IF (Nspec>1) cp_field(:) = (1.0_pr-SUM(u_in(:,n_spc(1):n_spc(Nspecm)),DIM=2)/u_in(:,n_den))*cp_in(Nspec) !cp_Nspec
    DO l=1,Nspecm
       cp_field(:) = cp_field(:) + u_in(:,n_spc(l))/u_in(:,n_den)*cp_in(l) !cp
    END DO

    kk_t_out(:) = cp_field(:) * mu_t_out(:) / Pra_t_in
    IF(Nspec>1) bD_t_out(:) = mu_t_out(:) / Sc_t_in


  END SUBROUTINE calc_fixSM_eddy_visc




END MODULE SGS_compressible_SM
