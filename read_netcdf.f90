MODULE read_netcdf_mod

  IMPLICIT NONE
CONTAINS


! 
!     ------------ 
! 
!     read in netcdf format initial data field 
! 
!     ------------ 
! 
SUBROUTINE read_netcdf_field_real8(field, dim1, dim2, dim3, filename,fieldname,the_viscosity) 
  !USE netcdf
  IMPLICIT NONE 

  INCLUDE 'netcdf.inc' 
  INTEGER dim1, dim2, dim3 
  REAL(8) field(dim1,dim2,dim3) 

  CHARACTER (*) filename, fieldname 
  REAL(8) the_viscosity

  real(4) viss

  !     usual stuff 

  INTEGER j, k 
  INTEGER cdfid, id, rcode 
  INTEGER start(3), SIZE(3) 

  WRITE(*,*) 'read_field_real8(): File opened to read input field in netcdf format:'
  WRITE(*,*)  TRIM(filename)

  IF( INDEX(filename,' ')-1 .GT. 70 ) THEN
     WRITE(*,*) 'File name is too long for netcdf routine (>70chars)...Exiting'
     STOP
  ENDIF
  ! 
  ! Open the netcdf file     
  !  

  !write (*,*) 'opening netcdf file;' , TRIM(filename) 
  cdfid = NCOPN(filename , NCNOWRIT, rcode) 

  IF ( rcode .NE. 0 )  CALL write_netcdf_error_to_stderr(rcode,'NCOPN') 

  id = NCVID(cdfid, fieldname , rcode) 
  IF ( rcode .NE. 0 )  CALL write_netcdf_error_to_stderr(rcode,'NCOPN')


  !
  ! Get the the_viscosity     
  CALL NCAGT(cdfid, NCGLOBAL, 'viscosity', the_viscosity, rcode)
  IF ( rcode .NE. 0 )  CALL write_netcdf_error_to_stderr(rcode,'NCAGT, viscosity')

! this is temporary because Giuliano's netcdf files were written
! using real*4 for the attributes DG.
print*,'real *8 the_viscosity',the_viscosity
  CALL NCAGT(cdfid, NCGLOBAL, 'viscosity', viss, rcode)
print *,'real*4 viscosity',viss

  ! 
  !     
  ! 
  SIZE(1) = dim1 
  SIZE(2) = 1 
  SIZE(3) = 1 
  start(1) = 1 

  DO k = 1, dim3 
     start(3) = k 
     DO j = 1, dim2 
        start(2) = j 
        CALL NCVGT(cdfid, id, start, size, field(1,j,k), rcode)
        IF ( rcode .NE. 0 )  &
             CALL write_netcdf_error_to_stderr(rcode,'NCOPN')
     ENDDO
  ENDDO

  CALL NCCLOS(cdfid, rcode) 
  IF ( rcode .NE. 0 )  CALL write_netcdf_error_to_stderr(rcode,'NCOPN')
  RETURN 
END SUBROUTINE read_netcdf_field_real8


SUBROUTINE write_netcdf_error_to_stderr( status , routinename ) 
  INTEGER status 
  CHARACTER (*) routinename 

  INTEGER  error_status
  CHARACTER (len=80) error_message 

!not in windows version??!   INTERFACE
!not in windows version??!      FUNCTION hestringf(status , error_message)
!not in windows version??!        INTEGER status,error_status
!not in windows version??!         CHARACTER*80 error_message
!not in windows version??!      END FUNCTION hestringf
!not in windows version??!   END INTERFACE

!not in windows version??!   error_status = hestringf(status , error_message) 
  WRITE (*,*) 'NetCDF Error:', error_message 
  WRITE (*,*) 'Occured in:', routinename 
  STOP

  RETURN 
END SUBROUTINE write_netcdf_error_to_stderr

END MODULE read_netcdf_mod
