MODULE freundBC
  USE precision
  USE wlt_trns_vars
  IMPLICIT NONE

  PUBLIC :: &
    freund_read_input, &
    freund_damping_rhs, &
    freund_damping_Drhs, &
    freund_damping_Drhs_diag, &
    freund_convect_rhs, &
    freund_convect_Drhs, &
    freund_convect_Drhs_diag, &
    setup_freund, &
    freund_cal_cfl

  PRIVATE :: & 
    calc_boundary_support

  ! These arrays need to be set up if Freund zone damping is going to be used
  REAL (pr), DIMENSION(:,:,:), ALLOCATABLE, PUBLIC :: freund_damping_target
  REAL (pr), DIMENSION(:,:,:,:), ALLOCATABLE, PUBLIC :: freund_damping_target_varying
  LOGICAL, DIMENSION(:,:,:), ALLOCATABLE, PUBLIC :: freund_damping_active, freund_convect_active

  LOGICAL, PROTECTED :: use_freund_damping, use_freund_convect

  REAL (pr), DIMENSION(2,3), PUBLIC :: &
    zone_thickness

  INTEGER, DIMENSION(2,3), PUBLIC :: &
    zone_difftype

  REAL (pr), PRIVATE :: &
    coeff_damping, &    ! Damping coefficients (1/timescale) for min/max, direction, variable
    coeff_convect       ! Convective coefficients (nondimensional velocity) for min/max, direction, variable

  REAL (pr), DIMENSION(2), PRIVATE :: & ! element 1 for min, 2 for max
    coeff_damping_vec, &    ! Damping coefficients (1/timescale) for min/max, direction, variable
    coeff_convect_vec       ! Convective coefficients (nondimensional velocity) for min/max, direction, variable

  LOGICAL, PRIVATE :: damping_target_varying ! spatially varying damping target, T for internal flows. If F, uniform damping target and convective velocity for the far field of external flows

CONTAINS

  SUBROUTINE  freund_read_input()
    USE input_file_reader
    USE error_handling
    IMPLICIT NONE
    REAL (pr), DIMENSION(2*dim) :: zone_input  ! Temporary storage for parsing inputting zone thickness
    INTEGER, DIMENSION(2*dim)   :: zone_input_int  ! Temporary storage for parsing inputting zone difftype
    INTEGER :: idim, shift

    call input_logical ('use_freund_damping',use_freund_damping,'stop', 'use_freund_damping: use Freund zone damping')
    call input_logical ('use_freund_convect',use_freund_convect,'stop', 'use_freund_convect: use Freund zone convection')
    damping_target_varying = .FALSE. 
    call input_logical ('damping_target_varying',damping_target_varying,'default', 'damping_target_varying: spatially varying damping target, T for internal flows. If F, uniform damping target and convective velocity for the far field of external flows')
   
    IF ( use_freund_damping .OR. use_freund_convect ) THEN
      call input_real_vector ('zone_thickness',zone_input(1:2*dim),2*dim,'stop',' zone_thickness: Freund zone thickness (xmin,xmax,ymin etc.) in computational domain')
      zone_thickness = 0.0_pr
      DO idim = 1,dim
        shift = (idim - 1)*2
        zone_thickness(1:2,idim) = zone_input(shift+1:shift+2)
      END DO

      IF(damping_target_varying) THEN
         call input_integer_vector('zone_difftype',zone_input_int(1:2*dim),2*dim,'stop',' zone_difftype: Freund zone differiential type (xmin,xmax,ymin etc.). Computational diff - 0 (usually for far field boundaries); Physical diff - 1.')
         DO idim = 1,dim
            shift = (idim - 1)*2
            zone_difftype(1:2,idim) = zone_input_int(shift+1:shift+2)
         END DO
      END IF
    END IF

    IF ( use_freund_damping ) THEN 
      IF (.NOT. damping_target_varying) THEN
        call input_real('coeff_damping',coeff_damping,'stop',' coeff_damping: damping coefficient for Freund BC')
      ELSE
        call input_real_vector('coeff_damping_vec',coeff_damping_vec(1:2),2,'stop',' coeff_damping_vec: damping coefficient for Freund BC')
      END IF
    END IF

    IF ( use_freund_convect ) THEN
      IF (.NOT. damping_target_varying) THEN
        call input_real('coeff_convect',coeff_convect,'stop',' coeff_convect: convective coefficient for Freund BC')
      ELSE
        call input_real_vector('coeff_convect_vec',coeff_convect_vec(1:2),2,'stop',' coeff_convect_vec: convective coefficient for Freund BC')
      END IF
    END IF

  END SUBROUTINE  freund_read_input

  ! Set up the freund zone masks and damping targets
  SUBROUTINE setup_freund()
    USE variable_mapping
    USE error_handling 
    USE sizes 
    IMPLICIT NONE

    IF( n_integrated .LE. 0 .OR. n_var .LE. 0 ) CALL error('Cannot set up freund zone, since there are no variables or integrated equations set up.  Try calling after setup_mapping')

    IF ( use_freund_damping ) THEN
      IF (.NOT. damping_target_varying) THEN
        IF( ALLOCATED(freund_damping_target) ) DEALLOCATE( freund_damping_target )
        ALLOCATE( freund_damping_target(1:2,1:dim,1:n_integrated) )
        freund_damping_target = 0.0_pr 
      END IF

      IF( ALLOCATED(freund_damping_active) ) DEALLOCATE( freund_damping_active )
      ALLOCATE( freund_damping_active(1:2,1:dim,1:n_integrated) )
      freund_damping_active = .TRUE.  
    END IF

    IF ( use_freund_convect) THEN
      IF( ALLOCATED(freund_convect_active) ) DEALLOCATE( freund_convect_active )
      ALLOCATE( freund_convect_active(1:2,1:dim,1:n_integrated) )
      freund_convect_active = .TRUE.  
    END IF

  END SUBROUTINE setup_freund


  ! Append damping RHS terms
  SUBROUTINE  freund_damping_rhs( rhs_loc, u_int_loc )
    USE variable_mapping
    USE sizes 
    USE error_handling
    IMPLICIT NONE
    REAL (pr), DIMENSION (nwlt,n_integrated), INTENT(IN) :: u_int_loc
    REAL (pr), DIMENSION (nwlt*n_integrated), INTENT(INOUT) :: rhs_loc
    REAL (pr), DIMENSION (nwlt)  :: min_zone, max_zone
    INTEGER :: idim, ie, shift
    REAL (pr) :: tol

    IF ( use_freund_damping ) THEN
      tol = 1.0e-8  ! for determining inflow, outflow, off
      DO idim = 1,dim
         min_zone = calc_boundary_support( 1, idim )
         max_zone = calc_boundary_support( 2, idim )
         DO ie=1, n_integrated
            shift = (ie-1)*nwlt
            IF (.NOT. damping_target_varying) THEN
              IF( freund_damping_active(1,idim,ie) .AND. ABS(zone_thickness(1,idim)) .GT. tol ) rhs_loc(shift+1:shift+nwlt) = rhs_loc(shift+1:shift+nwlt) - coeff_damping*( min_zone(:) * ( u_int_loc(:,ie) - freund_damping_target(1,idim,ie) ) ) 
              IF( freund_damping_active(2,idim,ie) .AND. ABS(zone_thickness(2,idim)) .GT. tol ) rhs_loc(shift+1:shift+nwlt) = rhs_loc(shift+1:shift+nwlt) - coeff_damping*( max_zone(:) * ( u_int_loc(:,ie) - freund_damping_target(2,idim,ie) ) ) 
            ELSE
              IF( freund_damping_active(1,idim,ie) .AND. ABS(zone_thickness(1,idim)) .GT. tol ) rhs_loc(shift+1:shift+nwlt) = rhs_loc(shift+1:shift+nwlt) - coeff_damping_vec(1)*( min_zone(:) * ( u_int_loc(:,ie) - freund_damping_target_varying(1:nwlt,1,idim,ie) ) ) 
              IF( freund_damping_active(2,idim,ie) .AND. ABS(zone_thickness(2,idim)) .GT. tol ) rhs_loc(shift+1:shift+nwlt) = rhs_loc(shift+1:shift+nwlt) - coeff_damping_vec(2)*( max_zone(:) * ( u_int_loc(:,ie) - freund_damping_target_varying(1:nwlt,2,idim,ie) ) ) 
              !IF (idim == 1) THEN
              !  IF(par_rank == 0) PRINT *, 'ie = ', ie
              !  CALL print_extrema(freund_damping_target(1:nwlt,1,idim,ie), nwlt, 'freund target')
              !END IF
            END IF
         END DO
      END DO
    END IF

  END SUBROUTINE  freund_damping_rhs

  ! Append damping DRHS terms
  SUBROUTINE  freund_damping_Drhs( drhs_loc, u_p_loc, u_prev_loc )
    USE variable_mapping
    USE sizes 
    IMPLICIT NONE
    REAL (pr), DIMENSION (nwlt,n_integrated), INTENT(IN) :: u_p_loc, u_prev_loc
    REAL (pr), DIMENSION (nwlt*n_integrated), INTENT(INOUT) :: drhs_loc
    REAL (pr), DIMENSION (nwlt)  :: min_zone, max_zone
    INTEGER :: idim, ie, shift
    REAL (pr) :: tol

    IF ( use_freund_damping ) THEN
      tol = 1.0e-8  ! for determining inflow, outflow, off
      DO idim = 1,dim
         min_zone = calc_boundary_support( 1, idim )
         max_zone = calc_boundary_support( 2, idim )
         DO ie=1, n_integrated
            shift = (ie-1)*nwlt
            IF (.NOT. damping_target_varying) THEN
              IF( freund_damping_active(1,idim,ie) .AND. ABS(zone_thickness(1,idim)) .GT. tol ) drhs_loc(shift+1:shift+nwlt) = drhs_loc(shift+1:shift+nwlt) - coeff_damping*u_p_loc(:,ie) * min_zone(:) 
              IF( freund_damping_active(2,idim,ie) .AND. ABS(zone_thickness(2,idim)) .GT. tol ) drhs_loc(shift+1:shift+nwlt) = drhs_loc(shift+1:shift+nwlt) - coeff_damping*u_p_loc(:,ie) * max_zone(:)
            ELSE
              IF( freund_damping_active(1,idim,ie) .AND. ABS(zone_thickness(1,idim)) .GT. tol ) drhs_loc(shift+1:shift+nwlt) = drhs_loc(shift+1:shift+nwlt) - coeff_damping_vec(1)*u_p_loc(:,ie) * min_zone(:) 
              IF( freund_damping_active(2,idim,ie) .AND. ABS(zone_thickness(2,idim)) .GT. tol ) drhs_loc(shift+1:shift+nwlt) = drhs_loc(shift+1:shift+nwlt) - coeff_damping_vec(2)*u_p_loc(:,ie) * max_zone(:)
            END IF
         END DO
      END DO
    END IF

  END SUBROUTINE  freund_damping_Drhs

  ! Append damping DRHS diagonal terms
  SUBROUTINE  freund_damping_Drhs_diag( drhs_diag_loc, u_prev_loc )
    USE variable_mapping
    USE sizes 
    IMPLICIT NONE
    REAL (pr), DIMENSION (nwlt,n_integrated), INTENT(IN) :: u_prev_loc
    REAL (pr), DIMENSION (nwlt*n_integrated), INTENT(INOUT) :: drhs_diag_loc
    REAL (pr), DIMENSION (nwlt)  :: min_zone, max_zone
    INTEGER :: idim, ie, shift
    REAL (pr) :: tol
  
    IF ( use_freund_damping ) THEN
      tol = 1.0e-8  ! for determining inflow, outflow, off
      DO idim = 1,dim
         min_zone = calc_boundary_support( 1, idim )
         max_zone = calc_boundary_support( 2, idim )
         DO ie=1, n_integrated
            shift = (ie-1)*nwlt   
            IF (.NOT. damping_target_varying) THEN
              IF( freund_damping_active(1,idim,ie) .AND. ABS(zone_thickness(1,idim)) .GT. tol ) drhs_diag_loc(shift+1:shift+nwlt) = drhs_diag_loc(shift+1:shift+nwlt) - coeff_damping * min_zone(:)
              IF( freund_damping_active(2,idim,ie) .AND. ABS(zone_thickness(2,idim)) .GT. tol ) drhs_diag_loc(shift+1:shift+nwlt) = drhs_diag_loc(shift+1:shift+nwlt) - coeff_damping * max_zone(:) 
            ELSE 
              IF( freund_damping_active(1,idim,ie) .AND. ABS(zone_thickness(1,idim)) .GT. tol ) drhs_diag_loc(shift+1:shift+nwlt) = drhs_diag_loc(shift+1:shift+nwlt) - coeff_damping_vec(1) * min_zone(:)
              IF( freund_damping_active(2,idim,ie) .AND. ABS(zone_thickness(2,idim)) .GT. tol ) drhs_diag_loc(shift+1:shift+nwlt) = drhs_diag_loc(shift+1:shift+nwlt) - coeff_damping_vec(2) * max_zone(:) 
            END IF
         END DO
      END DO
    END IF

  END SUBROUTINE  freund_damping_Drhs_diag

  SUBROUTINE  freund_convect_rhs( rhs_loc,u_int_loc, j_loc)
    USE variable_mapping
    USE wlt_trns_mod
    USE sizes 
    IMPLICIT NONE
    REAL (pr), DIMENSION (nwlt,n_integrated), INTENT(IN) :: u_int_loc
    REAL (pr), DIMENSION (nwlt*n_integrated), INTENT(INOUT) :: rhs_loc
    INTEGER, INTENT(IN) :: j_loc

    REAL (pr), DIMENSION (nwlt)  :: zone
    REAL (pr), DIMENSION (nwlt) :: u_int_pert
    REAL (pr), DIMENSION (n_integrated,nwlt,dim) :: duF, duB, d2u, duF_p, duB_p, duF_c, duB_c
    INTEGER :: mymeth_backward, mymeth_forward 
    INTEGER :: idim, ie, shift
    REAL (pr) :: tol

    IF ( use_freund_convect ) THEN
      tol = 1.0e-8  ! for determining inflow, outflow, off

      ! Take rectilinear derivative forward and backwards
       mymeth_backward = LOW_ORDER + BIASING_BACKWARD  ! Min inflow, and max outflow
       mymeth_forward  = LOW_ORDER + BIASING_FORWARD   ! Max outflow, and min inflow

      IF (.NOT. damping_target_varying) THEN
         CALL c_diff_fast( u_int_loc, duB, d2u, j_loc, nwlt, mymeth_backward, 10, n_integrated, 1, n_integrated, FORCE_RECTILINEAR=.TRUE.)
         CALL c_diff_fast( u_int_loc, duF, d2u, j_loc, nwlt, mymeth_forward, 10, n_integrated, 1, n_integrated, FORCE_RECTILINEAR=.TRUE.)
      ELSE
       IF (.NOT. ANY(zone_difftype(:,:)==0)) THEN
         CALL c_diff_fast( u_int_loc, duB, d2u, j_loc, nwlt, mymeth_backward, 10, n_integrated, 1, n_integrated)
         CALL c_diff_fast( u_int_loc, duF, d2u, j_loc, nwlt, mymeth_forward, 10, n_integrated, 1, n_integrated)
       ELSE ! at least one of the freund zones requires computational differentiation
         CALL c_diff_fast( u_int_loc, duB_c, d2u, j_loc, nwlt, mymeth_backward, 10, n_integrated, 1, n_integrated, FORCE_RECTILINEAR=.TRUE.)
         CALL c_diff_fast( u_int_loc, duF_c, d2u, j_loc, nwlt, mymeth_forward, 10, n_integrated, 1, n_integrated, FORCE_RECTILINEAR=.TRUE.)
         CALL c_diff_fast( u_int_loc, duB, d2u, j_loc, nwlt, mymeth_backward, 10, n_integrated, 1, n_integrated)
         CALL c_diff_fast( u_int_loc, duF, d2u, j_loc, nwlt, mymeth_forward, 10, n_integrated, 1, n_integrated)
       END IF !zone_difftype
      END IF

      ! do all min boundary zones
      DO idim = 1,dim
         zone = calc_boundary_support( 1, idim )! * SIGN( 1.0_pr, zone_thickness(1, idim) )
         DO ie=1, n_integrated
            shift = (ie-1)*nwlt
            IF( freund_convect_active(1,idim,ie) ) THEN 
              IF (.NOT. damping_target_varying) THEN
                IF( zone_thickness(1, idim) .LT. -tol ) rhs_loc(shift+1:shift+nwlt) = rhs_loc(shift+1:shift+nwlt) - coeff_convect*zone(:) * duB(ie,:,idim) ! Right pointing characteristic, inflow
                IF( zone_thickness(1, idim) .GT. tol )  rhs_loc(shift+1:shift+nwlt) = rhs_loc(shift+1:shift+nwlt) + coeff_convect*zone(:) * duF(ie,:,idim) ! Left pointing characteristic, outflow
              ELSE
               IF( zone_difftype(1, idim) == 1 ) THEN ! physical diff
                IF( zone_thickness(1, idim) .LT. -tol ) THEN 
                  ! For a spatially varying inflow (e.g. boundary layer), d(u - u_mean)/d(x_i) should be used instead of du/d(x_i).
                  u_int_pert(1:nwlt) = u_int_loc(1:nwlt, ie) - freund_damping_target_varying(1:nwlt,1,idim,ie)       ! u - u_mean
                  CALL c_diff_fast( u_int_pert, duB_p(1,:,:), d2u(1,:,:), j_loc, nwlt, mymeth_backward, 10, 1, 1, 1) ! d(u - u_mean)/d(x_i)
                  rhs_loc(shift+1:shift+nwlt) = rhs_loc(shift+1:shift+nwlt) - coeff_convect_vec(1)*zone(:) * duB_p(1,:,idim) ! Right pointing characteristic, inflow
                END IF
                IF( zone_thickness(1, idim) .GT. tol )  rhs_loc(shift+1:shift+nwlt) = rhs_loc(shift+1:shift+nwlt) + coeff_convect_vec(1)*zone(:) * duF(ie,:,idim) ! Left pointing characteristic, outflow
               ELSE ! computational diff
                IF( zone_thickness(1, idim) .LT. -tol ) rhs_loc(shift+1:shift+nwlt) = rhs_loc(shift+1:shift+nwlt) - coeff_convect_vec(1)*zone(:) * duB_c(ie,:,idim) ! Right pointing characteristic, inflow
                IF( zone_thickness(1, idim) .GT. tol )  rhs_loc(shift+1:shift+nwlt) = rhs_loc(shift+1:shift+nwlt) + coeff_convect_vec(1)*zone(:) * duF_c(ie,:,idim) ! Left pointing characteristic, outflow
               END IF !zone_difftype
              END IF
            END IF
         END DO
      END DO

      ! do all max boundary zones
      DO idim = 1,dim
         zone = calc_boundary_support( 2, idim ) !* SIGN( 1.0_pr, zone_thickness(2, idim) )
         DO ie=1, n_integrated
            shift = (ie-1)*nwlt
            IF( freund_convect_active(2,idim,ie) ) THEN 
              IF (.NOT. damping_target_varying) THEN
                IF( zone_thickness(2, idim) .GT. tol )  rhs_loc(shift+1:shift+nwlt) = rhs_loc(shift+1:shift+nwlt) - coeff_convect*zone(:) * duB(ie,:,idim) ! Right pointing characteristic, outflow
                IF( zone_thickness(2, idim) .LT. -tol ) rhs_loc(shift+1:shift+nwlt) = rhs_loc(shift+1:shift+nwlt) + coeff_convect*zone(:) * duF(ie,:,idim) ! Left pointing characteristic, inflow
              ELSE 
               IF( zone_difftype(2, idim) == 1 ) THEN ! physical diff
                IF( zone_thickness(2, idim) .GT. tol )  rhs_loc(shift+1:shift+nwlt) = rhs_loc(shift+1:shift+nwlt) - coeff_convect_vec(2)*zone(:) * duB(ie,:,idim) ! Right pointing characteristic, outflow
                IF( zone_thickness(2, idim) .LT. -tol ) THEN 
                  ! For a spatially varying inflow (e.g. boundary layer), d(u - u_mean)/d(x_i) should be used instead of du/d(x_i).
                  u_int_pert(1:nwlt) = u_int_loc(1:nwlt, ie) - freund_damping_target_varying(1:nwlt,2,idim,ie)      ! u - u_mean
                  CALL c_diff_fast( u_int_pert, duF_p(1,:,:), d2u(1,:,:), j_loc, nwlt, mymeth_forward, 10, 1, 1, 1) ! d(u - u_mean)/d(x_i)
                  rhs_loc(shift+1:shift+nwlt) = rhs_loc(shift+1:shift+nwlt) + coeff_convect_vec(2)*zone(:) * duF_p(1,:,idim) ! Left pointing characteristic, inflow
                END IF
               ELSE ! computational diff
                IF( zone_thickness(2, idim) .GT. tol )  rhs_loc(shift+1:shift+nwlt) = rhs_loc(shift+1:shift+nwlt) - coeff_convect_vec(2)*zone(:) * duB_c(ie,:,idim) ! Right pointing characteristic, outflow
                IF( zone_thickness(2, idim) .LT. -tol ) rhs_loc(shift+1:shift+nwlt) = rhs_loc(shift+1:shift+nwlt) + coeff_convect_vec(2)*zone(:) * duF_c(ie,:,idim) ! Left pointing characteristic, inflow
               END IF !zone_difftype
              END IF
            END IF
         END DO
      END DO

    END IF

  END SUBROUTINE  freund_convect_rhs

  SUBROUTINE  freund_convect_Drhs( drhs_loc, u_p_loc, u_prev_loc, j_loc )
    USE variable_mapping
    USE wlt_trns_mod
    USE sizes 
    IMPLICIT NONE
    REAL (pr), DIMENSION (nwlt,n_integrated), INTENT(IN) :: u_p_loc, u_prev_loc
    REAL (pr), DIMENSION (nwlt*n_integrated), INTENT(INOUT) :: drhs_loc
    INTEGER, INTENT(IN) :: j_loc

    REAL (pr), DIMENSION (nwlt)  :: zone
    REAL (pr), DIMENSION (n_integrated,nwlt,dim) :: duF, duB, d2u, duF_c, duB_c
    INTEGER :: mymeth_backward, mymeth_forward 
    INTEGER :: idim, ie, shift
    REAL (pr) :: tol

    IF ( use_freund_convect ) THEN
      tol = 1.0e-8  ! for determining inflow, outflow, off

      ! Take rectilinear derivative forward and backwards
       mymeth_backward = LOW_ORDER + BIASING_BACKWARD  ! Min inflow, and max outflow
       mymeth_forward  = LOW_ORDER + BIASING_FORWARD   ! Max outflow, and min inflow

      ! ERIC: check convect mask (directional and by variable)? 
      IF (.NOT. damping_target_varying) THEN
        CALL c_diff_fast( u_p_loc, duB, d2u, j_loc, nwlt, mymeth_backward, 10, n_integrated, 1, n_integrated, FORCE_RECTILINEAR=.TRUE.)
        CALL c_diff_fast( u_p_loc, duF, d2u, j_loc, nwlt, mymeth_forward, 10, n_integrated, 1, n_integrated, FORCE_RECTILINEAR=.TRUE.)
      ELSE
       IF (.NOT. ANY(zone_difftype(:,:)==0)) THEN
        CALL c_diff_fast( u_p_loc, duB, d2u, j_loc, nwlt, mymeth_backward, 10, n_integrated, 1, n_integrated)
        CALL c_diff_fast( u_p_loc, duF, d2u, j_loc, nwlt, mymeth_forward, 10, n_integrated, 1, n_integrated)
       ELSE ! at least one of the freund zones requires computational differentiation
        CALL c_diff_fast( u_p_loc, duB_c, d2u, j_loc, nwlt, mymeth_backward, 10, n_integrated, 1, n_integrated, FORCE_RECTILINEAR=.TRUE.)
        CALL c_diff_fast( u_p_loc, duF_c, d2u, j_loc, nwlt, mymeth_forward, 10, n_integrated, 1, n_integrated, FORCE_RECTILINEAR=.TRUE.)
        CALL c_diff_fast( u_p_loc, duB, d2u, j_loc, nwlt, mymeth_backward, 10, n_integrated, 1, n_integrated)
        CALL c_diff_fast( u_p_loc, duF, d2u, j_loc, nwlt, mymeth_forward, 10, n_integrated, 1, n_integrated)
       END IF !zone_difftype
      END IF

      ! do all min boundary zones
      DO idim = 1,dim
         zone = calc_boundary_support( 1, idim ) !* SIGN( 1.0_pr, zone_thickness(1, idim) )
         DO ie=1, n_integrated
            shift = (ie-1)*nwlt
            IF( freund_convect_active(1,idim,ie) ) THEN 
              IF (.NOT. damping_target_varying) THEN
                IF( zone_thickness(1, idim) .LT. -tol ) drhs_loc(shift+1:shift+nwlt) = drhs_loc(shift+1:shift+nwlt) - coeff_convect*zone(:) * duB(ie,:,idim) ! Right pointing characteristic, inflow
                IF( zone_thickness(1, idim) .GT. tol )  drhs_loc(shift+1:shift+nwlt) = drhs_loc(shift+1:shift+nwlt) + coeff_convect*zone(:) * duF(ie,:,idim) ! Left pointing characteristic, outflow
              ELSE
               IF( zone_difftype(1, idim) == 1 ) THEN ! physical diff
                IF( zone_thickness(1, idim) .LT. -tol ) drhs_loc(shift+1:shift+nwlt) = drhs_loc(shift+1:shift+nwlt) - coeff_convect_vec(1)*zone(:) * duB(ie,:,idim) ! Right pointing characteristic, inflow
                IF( zone_thickness(1, idim) .GT. tol )  drhs_loc(shift+1:shift+nwlt) = drhs_loc(shift+1:shift+nwlt) + coeff_convect_vec(1)*zone(:) * duF(ie,:,idim) ! Left pointing characteristic, outflow
               ELSE ! computational diff
                IF( zone_thickness(1, idim) .LT. -tol ) drhs_loc(shift+1:shift+nwlt) = drhs_loc(shift+1:shift+nwlt) - coeff_convect_vec(1)*zone(:) * duB_c(ie,:,idim) ! Right pointing characteristic, inflow
                IF( zone_thickness(1, idim) .GT. tol )  drhs_loc(shift+1:shift+nwlt) = drhs_loc(shift+1:shift+nwlt) + coeff_convect_vec(1)*zone(:) * duF_c(ie,:,idim) ! Left pointing characteristic, outflow
               END IF ! zone_difftype
              END IF
            END IF
         END DO
      END DO

      ! do all max boundary zones
      DO idim = 1,dim
         zone = calc_boundary_support( 2, idim ) !* SIGN( 1.0_pr, zone_thickness(2, idim) )
         DO ie=1, n_integrated
            shift = (ie-1)*nwlt
            IF( freund_convect_active(2,idim,ie) ) THEN 
              IF (.NOT. damping_target_varying) THEN
                IF( zone_thickness(2, idim) .GT. tol )  drhs_loc(shift+1:shift+nwlt) = drhs_loc(shift+1:shift+nwlt) - coeff_convect*zone(:) * duB(ie,:,idim) ! Right pointing characteristic, outflow
                IF( zone_thickness(2, idim) .LT. -tol ) drhs_loc(shift+1:shift+nwlt) = drhs_loc(shift+1:shift+nwlt) + coeff_convect*zone(:) * duF(ie,:,idim) ! Left pointing characteristic, inflow
              ELSE
               IF( zone_difftype(2, idim) == 1 ) THEN ! physical diff
                IF( zone_thickness(2, idim) .GT. tol )  drhs_loc(shift+1:shift+nwlt) = drhs_loc(shift+1:shift+nwlt) - coeff_convect_vec(2)*zone(:) * duB(ie,:,idim) ! Right pointing characteristic, outflow
                IF( zone_thickness(2, idim) .LT. -tol ) drhs_loc(shift+1:shift+nwlt) = drhs_loc(shift+1:shift+nwlt) + coeff_convect_vec(2)*zone(:) * duF(ie,:,idim) ! Left pointing characteristic, inflow
               ELSE ! computational diff
                IF( zone_thickness(2, idim) .GT. tol )  drhs_loc(shift+1:shift+nwlt) = drhs_loc(shift+1:shift+nwlt) - coeff_convect_vec(2)*zone(:) * duB_c(ie,:,idim) ! Right pointing characteristic, outflow
                IF( zone_thickness(2, idim) .LT. -tol ) drhs_loc(shift+1:shift+nwlt) = drhs_loc(shift+1:shift+nwlt) + coeff_convect_vec(2)*zone(:) * duF_c(ie,:,idim) ! Left pointing characteristic, inflow
               END IF ! zone_difftype
              END IF
            END IF
         END DO
      END DO

    END IF

  END SUBROUTINE  freund_convect_Drhs

  SUBROUTINE  freund_convect_Drhs_diag( drhs_diag_loc, u_prev_loc, j_loc )
    USE variable_mapping
    USE wlt_trns_mod
    USE sizes 
    IMPLICIT NONE
    REAL (pr), DIMENSION (nwlt,n_integrated), INTENT(IN) :: u_prev_loc
    REAL (pr), DIMENSION (nwlt*n_integrated), INTENT(INOUT) :: drhs_diag_loc
    INTEGER, INTENT(IN) :: j_loc

    REAL (pr), DIMENSION (nwlt)  :: zone
    REAL (pr), DIMENSION (nwlt,dim) :: duF, duB, d2u, duF_c, duB_c
    INTEGER :: mymeth_backward, mymeth_forward 
    INTEGER :: idim, ie, shift
    REAL (pr) :: tol

    IF ( use_freund_convect ) THEN
      tol = 1.0e-8  ! for determining inflow, outflow, off

      ! Take rectilinear derivative forward and backwards
       mymeth_backward = LOW_ORDER + BIASING_BACKWARD  ! Min inflow, and max outflow
       mymeth_forward  = LOW_ORDER + BIASING_FORWARD   ! Max outflow, and min inflow

      ! ERIC: check convect mask (directional and by variable)? 
      IF (.NOT. damping_target_varying) THEN
        CALL c_diff_diag( duB, d2u, j_loc, nwlt, mymeth_backward, mymeth_backward, 10, FORCE_RECTILINEAR=.TRUE.)
        CALL c_diff_diag( duF, d2u, j_loc, nwlt, mymeth_forward, mymeth_forward, 10, FORCE_RECTILINEAR=.TRUE.)
      ELSE
       IF (.NOT. ANY(zone_difftype(:,:)==0)) THEN
        CALL c_diff_diag( duB, d2u, j_loc, nwlt, mymeth_backward, mymeth_backward, 10)
        CALL c_diff_diag( duF, d2u, j_loc, nwlt, mymeth_forward, mymeth_forward, 10)
       ELSE ! at least one of the freund zones requires computational differentiation
        CALL c_diff_diag( duB_c, d2u, j_loc, nwlt, mymeth_backward, mymeth_backward, 10, FORCE_RECTILINEAR=.TRUE.)
        CALL c_diff_diag( duF_c, d2u, j_loc, nwlt, mymeth_forward, mymeth_forward, 10, FORCE_RECTILINEAR=.TRUE.)
        CALL c_diff_diag( duB, d2u, j_loc, nwlt, mymeth_backward, mymeth_backward, 10)
        CALL c_diff_diag( duF, d2u, j_loc, nwlt, mymeth_forward, mymeth_forward, 10)
       END IF ! zone_difftype
      END IF

      ! do all min boundary zones
      DO idim = 1,dim
         zone = calc_boundary_support( 1, idim ) !* SIGN( 1.0_pr, zone_thickness(1, idim) )
         DO ie=1, n_integrated
            shift = (ie-1)*nwlt
            IF( freund_convect_active(1,idim,ie) ) THEN 
              IF (.NOT. damping_target_varying) THEN
                IF( zone_thickness(1, idim) .LT. -tol ) drhs_diag_loc(shift+1:shift+nwlt) = drhs_diag_loc(shift+1:shift+nwlt) - coeff_convect*zone(:) * duB(:,idim) ! Right pointing characteristic, inflow
                IF( zone_thickness(1, idim) .GT. tol )  drhs_diag_loc(shift+1:shift+nwlt) = drhs_diag_loc(shift+1:shift+nwlt) + coeff_convect*zone(:) * duF(:,idim) ! Left pointing characteristic, outflow
              ELSE
               IF( zone_difftype(1, idim) == 1 ) THEN ! physical diff
                IF( zone_thickness(1, idim) .LT. -tol ) drhs_diag_loc(shift+1:shift+nwlt) = drhs_diag_loc(shift+1:shift+nwlt) - coeff_convect_vec(1)*zone(:) * duB(:,idim) ! Right pointing characteristic, inflow
                IF( zone_thickness(1, idim) .GT. tol )  drhs_diag_loc(shift+1:shift+nwlt) = drhs_diag_loc(shift+1:shift+nwlt) + coeff_convect_vec(1)*zone(:) * duF(:,idim) ! Left pointing characteristic, outflow
               ELSE ! computational diff
                IF( zone_thickness(1, idim) .LT. -tol ) drhs_diag_loc(shift+1:shift+nwlt) = drhs_diag_loc(shift+1:shift+nwlt) - coeff_convect_vec(1)*zone(:) * duB_c(:,idim) ! Right pointing characteristic, inflow
                IF( zone_thickness(1, idim) .GT. tol )  drhs_diag_loc(shift+1:shift+nwlt) = drhs_diag_loc(shift+1:shift+nwlt) + coeff_convect_vec(1)*zone(:) * duF_c(:,idim) ! Left pointing characteristic, outflow
               END IF ! zone_difftype
              END IF
            END IF
         END DO
      END DO

      ! do all max boundary zones
      DO idim = 1,dim
         zone = calc_boundary_support( 2, idim ) !* SIGN( 1.0_pr, zone_thickness(2, idim) )
         DO ie=1, n_integrated
            shift = (ie-1)*nwlt
            IF( freund_convect_active(2,idim,ie) ) THEN 
              IF (.NOT. damping_target_varying) THEN
                IF( zone_thickness(2, idim) .GT. tol )  drhs_diag_loc(shift+1:shift+nwlt) = drhs_diag_loc(shift+1:shift+nwlt) - coeff_convect*zone(:) * duB(:,idim) ! Right pointing characteristic, outflow
                IF( zone_thickness(2, idim) .LT. -tol ) drhs_diag_loc(shift+1:shift+nwlt) = drhs_diag_loc(shift+1:shift+nwlt) + coeff_convect*zone(:) * duF(:,idim) ! Left pointing characteristic, inflow
              ELSE
               IF( zone_difftype(2, idim) == 1 ) THEN ! physical diff
                IF( zone_thickness(2, idim) .GT. tol )  drhs_diag_loc(shift+1:shift+nwlt) = drhs_diag_loc(shift+1:shift+nwlt) - coeff_convect_vec(2)*zone(:) * duB(:,idim) ! Right pointing characteristic, outflow
                IF( zone_thickness(2, idim) .LT. -tol ) drhs_diag_loc(shift+1:shift+nwlt) = drhs_diag_loc(shift+1:shift+nwlt) + coeff_convect_vec(2)*zone(:) * duF(:,idim) ! Left pointing characteristic, inflow
               ELSE ! computational diff
                IF( zone_thickness(2, idim) .GT. tol )  drhs_diag_loc(shift+1:shift+nwlt) = drhs_diag_loc(shift+1:shift+nwlt) - coeff_convect_vec(2)*zone(:) * duB_c(:,idim) ! Right pointing characteristic, outflow
                IF( zone_thickness(2, idim) .LT. -tol ) drhs_diag_loc(shift+1:shift+nwlt) = drhs_diag_loc(shift+1:shift+nwlt) + coeff_convect_vec(2)*zone(:) * duF_c(:,idim) ! Left pointing characteristic, inflow
               END IF ! zone_difftype
              END IF
            END IF
         END DO
      END DO
    END IF

  END SUBROUTINE  freund_convect_Drhs_diag

  FUNCTION freund_cal_cfl()
    USE variable_mapping
    USE sizes 
    USE wlt_trns_util_mod 
    IMPLICIT NONE
    REAL (pr) :: freund_cal_cfl

    REAL (pr) :: flor 
    REAL (pr), DIMENSION(dim,nwlt) :: h_arr
    INTEGER idim, ibnd

    flor = 1e-12_pr
    freund_cal_cfl = flor 

    IF ( use_freund_convect ) THEN
      ! Calculate the CFL in computational space
      CALL get_all_local_h (h_arr)
      DO idim = 1,dim
        DO ibnd = 1,2  ! Min/Max faces
          IF (.NOT. damping_target_varying) THEN
            freund_cal_cfl = MAX( freund_cal_cfl, MAXVAL( coeff_convect * calc_boundary_support( ibnd, idim ) * dt / h_arr(idim,1:nwlt) ) )
          ELSE
            freund_cal_cfl = MAX( freund_cal_cfl, MAXVAL( coeff_convect_vec(ibnd) * calc_boundary_support( ibnd, idim ) * dt / h_arr(idim,1:nwlt) ) )
          END IF
        END DO
      END DO

      CALL parallel_global_SUM(REALMAXVAL=freund_cal_cfl)
    END IF



  END FUNCTION freund_cal_cfl

  ! Calculates a positive support function [0,1] for the buffer zone on a domain boundary
  FUNCTION calc_boundary_support( minmax, direction )
    USE sizes 
    USE util_vars 
    USE wlt_vars 
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: direction, minmax   ! The coordinate boundary specified by the coordinate direction (1-dim) and the min/max (1 or 2)
    REAL (pr), DIMENSION(nwlt) :: calc_boundary_support
    REAL (pr), DIMENSION(nwlt) :: distance 

    IF( ABS(zone_thickness(minmax,direction)) .GT. 1.0e-10_pr ) THEN ! check if zone is active
      distance(:) = ABS( ( xyzlimits(minmax,direction) - x(:,direction) ) / zone_thickness(minmax,direction) )  ! distance from the boundary edge, normalized and unsigned

      WHERE( distance(:) .LE. 1.0_pr )
        calc_boundary_support(:) = distance(:) * distance(:) * ( 2.0_pr * distance(:) - 3.0_pr ) + 1.0_pr 
      ELSEWHERE
        calc_boundary_support(:) = 0.0_pr
      END WHERE
    ELSE
      calc_boundary_support(:) = 0.0_pr
    END IF

  END FUNCTION calc_boundary_support

END MODULE freundBC
