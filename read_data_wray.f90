MODULE read_data_wry
  USE debug_vars

CONTAINS 

  SUBROUTINE read_data_wray_dims(afilename,nx_out,ny_out,nz_out,viscosity)
    USE precision
    !USE params
    USE fft_module
    IMPLICIT NONE
    INTEGER, PARAMETER :: prread = SELECTED_REAL_KIND (4)    
    INTEGER, INTENT(INOUT) :: nx_out, ny_out, nz_out
    INTEGER (prread) :: timestep, nx, ny, nz, nc, numr, numc
    !INTEGER , INTENT (IN) :: n1,n2,n3
    REAL (prread) :: time, viscos
    REAL (prread), DIMENSION(40) :: junk1
    !REAL (prread), DIMENSION (:,:,:), ALLOCATABLE :: un,wn
    !REAL (prread), DIMENSION (:,:), ALLOCATABLE :: vn0
    !REAL (pr), DIMENSION (n1+2,n2+1,n3+1) :: u,v,w
    REAL (pr), INTENT(OUT) :: viscosity
    CHARACTER (LEN=*), INTENT(IN)::  afilename 
  
  write(*,*) 'In read_data_wray() get dimensions'
  write(*,*) 'Openning data file:', afilename


    OPEN(1, FILE=afilename(1:INDEX(afilename,' ')-1), STATUS='old', FORM='unformatted', ACCESS='direct', recl=49*4)
      READ(1,rec=1) nx,ny,nz,nc,numr,numc, viscos, junk1(40), timestep, time
    CLOSE(1) 
    viscosity = REAL(viscos,pr)
    nx_out = nx ! pass back with default integer precision
    ny_out = ny
    nz_out = nz

END SUBROUTINE read_data_wray_dims


  SUBROUTINE read_data_wray(afilename,u,v,w,n1,n2,n3,viscosity)
    USE precision
    !USE params
    USE fft_module
    IMPLICIT NONE
    INTEGER, PARAMETER :: prread = SELECTED_REAL_KIND (4)    
    INTEGER (prread) :: timestep, nx, ny, nz, nc, numr, numc
    INTEGER :: i,j,k, xskip, mz1, x, y, z
    INTEGER , INTENT (IN) :: n1,n2,n3
    REAL (prread) :: time, viscos
    REAL (prread), DIMENSION(40) :: junk1
    REAL (prread), DIMENSION (:,:,:), ALLOCATABLE :: un,wn
    REAL (prread), DIMENSION (:,:), ALLOCATABLE :: vn0
    REAL (pr), DIMENSION (n1+2,n2+1,n3+1) :: u,v,w
    REAL (pr), INTENT(OUT) :: viscosity
    CHARACTER (LEN=*), INTENT(IN)::  afilename 
  
  write(*,*) 'In read_data_wray()'
  write(*,*) 'Openning data file:', afilename

    OPEN(1, FILE=afilename(1:INDEX(afilename,' ')-1), STATUS='old', FORM='unformatted', ACCESS='direct', recl=49*4)
      READ(1,rec=1) nx,ny,nz,nc,numr,numc, viscos, junk1(40), timestep, time
    CLOSE(1) 
    !  
    mz1 = (nz+numc-1)/numc
    xskip = numc * mz1
    viscosity = REAL(viscos,pr)
    IF( nx /= n1 .OR. ny /= n2 .OR. nz /= n3 ) THEN ! checking dimenstions
       WRITE(*,'("read_data_wray: ERROR in dimensions")')
       PRINT *,nx, ny, nz, nc, numr, numc, viscos, timestep, time
       PRINT *,mz1, xskip

       STOP
    END IF
    !
    PRINT *,nx, ny, nz, nc, numr, numc, viscos, timestep, time
    PRINT *,mz1, xskip
    !
    ALLOCATE ( un(nx,ny,nz), vn0(nx,nz), wn(nx,ny,nz)) 
    un = 0.0_prread
    vn0 = 0.0_prread
    wn = 0.0_prread 
    u = 0.0_pr
    v = 0.0_pr
    w = 0.0_pr  
    
    write(*,*) 'Openning data file:', TRIM(afilename)

!
    OPEN(1, FILE=afilename(1:INDEX(afilename,' ')-1), STATUS='old', FORM='unformatted', ACCESS='direct', recl=8*((2+nc)*ny+1))
      do x = 1, nx/2
          do z = 1, nz
             read(1,rec=1+mz1+(z-1)+xskip*(x-1)) (un(2*x-1,y,z),un(2*x,y,z), &
                                                  wn(2*x-1,y,z),wn(2*x,y,z),y=1,ny), &
                                                  vn0(2*x-1,z),vn0(2*x,z)
          end do
      end do
    CLOSE(1)
    !
    ! now we zero out "oddball" wavenumbers as mentioned above   
    !

    un(2,1,1) =0.0_prread  ! imaginary part is set to zero
    vn0(2,1) =0.0_prread  ! imaginary part is set to zero
    wn(2,1,1) =0.0_prread  ! imaginary part is set to zero
    un(:,ny/2+1,:) =0.0_prread
    un(:,:,nz/2+1) =0.0_prread
    vn0(:,nz/2+1) =0.0_prread
    wn(:,ny/2+1,:) =0.0_prread
    wn(:,:,nz/2+1) =0.0_prread
    !
    u(1:nx,1:ny,1:nz) =  REAL(un(1:nx, 1:ny, 1:nz), pr)  
    v(1:nx,1   ,1:nz) =  REAL(vn0(1:nx,      1:nz), pr)  
    w(1:nx,1:ny,1:nz) =  REAL(wn(1:nx, 1:ny, 1:nz), pr)  
    !
    DEALLOCATE(un,vn0,wn)
    !
    
    ! Recreating w using continuity
    do k = 1, nz  
       do j = 2, ny ! from2 because k_y=0 was read in from data file
          do i = 1, nx, 2
             v(i  ,j,k) = - ( fac1(i)* u(i  ,j,k) + fac3(k) * w(i  ,j,k) )/fac2(j)
             v(i+1,j,k) = - ( fac1(i)* u(i+1,j,k) + fac3(k) * w(i+1,j,k) )/fac2(j)
            enddo
       enddo
    enddo
    
  END SUBROUTINE read_data_wray
  
END MODULE read_data_wry

