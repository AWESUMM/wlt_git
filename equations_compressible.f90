! Shared variables
MODULE equations_compressible_vars
  USE precision
  IMPLICIT NONE

  ! Eddy viscosity  !ERIC: is there a better way to control this?
  LOGICAL, PUBLIC :: use_comp_eddy_visc = .FALSE.
  REAL (pr), ALLOCATABLE, DIMENSION(:), PUBLIC  :: mu_turb, kk_turb, bD_turb   ! Eddy viscosity for any appropriate LES models 

END MODULE equations_compressible_vars


MODULE equations_compressible  

  USE precision
  USE wlt_vars
  USE wavelet_filters_mod
  USE wlt_trns_mod
  USE wlt_trns_vars
  USE wlt_trns_util_mod
  USE io_3d_vars
  USE util_mod
  USE util_vars
  USE share_consts
  USE pde
  USE sizes
  USE share_kry
  USE vector_util_mod
  USE input_file_reader
  USE parallel
  USE equations_compressible_vars
  USE error_handling
  USE geometry_helper
  USE field !u

  ! Variable indices
  INTEGER,              PROTECTED :: &
    n_den   = 0, &  ! Density
    n_eng   = 0, &  ! Energy
    n_eon   = 0, &  ! Specific Energy
    n_prs   = 0, &  ! Pressure
    !n_dpdx  = 0, &  ! dp/dx
    n_buoy  = 0, &  ! Buoyancy
    n_temp  = 0, &  ! Temperature
    n_mvort = 0, &  ! Magnitude of vorticity
    n_mvel  = 0, &  ! Magnitude of velocity
    n_mods  = 0, &  ! Modulus of strain rate
    n_qcrit = 0, &  ! Q-criterion (vortex visualizatin) 
    n_divu  = 0     ! Divergence of velocity 
  INTEGER, ALLOCATABLE, PROTECTED :: &
    n_mom(:), &     ! Momentum vector
    n_spc(:), &     ! Species mass fraction array
    n_son(:), &     ! Species fraction array ( rho * phi / rho )
    n_vel(:), &     ! Velocity
    n_cvort(:), &   ! Components of vorticity
    n_gdy(:)        ! Gradient of species fraction 
  INTEGER,              PROTECTED :: Nspec, Nspecm   ! Nspec - number of species, Nspecm - number of species arrays (Nspec - 1)

  ! Public variables

  ! Properties
  REAL (pr), ALLOCATABLE, DIMENSION(:), PROTECTED ::  mu_in, kk_in, cp_in, cv_in, R_in, MW_in, gamm  
  REAL (pr), ALLOCATABLE, DIMENSION(:), PROTECTED :: bD_in, gr_in
  REAL (pr), PROTECTED :: F_compress     ! Compressibility factor 

  LOGICAL, PROTECTED :: NS !=.TRUE. ! if NS=.TRUE. - Navier-Stoke, else Euler
  LOGICAL, PROTECTED :: NSdiag !=.TRUE. ! if NSdiag=.TRUE. - use viscous terms in user_rhs_diag
  LOGICAL, PROTECTED :: GRAV !=.TRUE. ! if GRAV=.TRUE. - include gravit

  ! Private variables
  REAL (pr), PRIVATE :: MAreport, CFLreport, CFLnonl, CFLconv, CFLacou, CFLbffv, CFLbfft, CFLbffs, CFLvisc, CFLthrm, CFLspec, CFLvort, CFLmods, maxsos  ! ERIC: why are these global in the module?
  !REAL (pr), PRIVATE :: peakchange
  !REAL (pr), PRIVATE :: Pint, gammR, Y_Xhalf
  !REAL (pr), ALLOCATABLE, DIMENSION (:), PRIVATE :: YR
  INTEGER, PRIVATE :: methpd
  !LOGICAL, PRIVATE :: evolBC  ! ERIC: is this appropriate to include anymore?
  INTEGER, PRIVATE :: dervFlux
  LOGICAL, PRIVATE :: ComboIMEXRK
  LOGICAL, PRIVATE :: boundY, splitFBpress, stepsplit
  REAL (pr), PRIVATE :: visccfl, imexcfl
  LOGICAL, PRIVATE :: convcfl, bonusCFL
  LOGICAL, PRIVATE :: hybridcfl
  
  INTEGER, PRIVATE :: kinevisc

  INTEGER, PUBLIC :: viscmodel = 0 ! possibly combine with kinevisc
  INTEGER, PARAMETER, PUBLIC :: &
    viscmodel_const = 0, & ! Constant viscosity
    viscmodel_sutherland = 1    ! Sutherland's law 

  REAL (pr), PUBLIC :: sutherland_const  ! Sutherland temperature 

  !LOGICAL, PRIVATE :: adaptAt

  ! Variables control
  INTEGER, PRIVATE :: adaptMagVort = 0, adaptComVort = 0, adaptMagVel = 0, adaptNormS = 0, adaptGradY = 0, adaptbuoy = 0
  INTEGER, PRIVATE :: adaptden = 0, adaptmom = 0, adapteng = 0, adaptspc = 0, adaptprs = 0, adapttmp = 0
  INTEGER, PRIVATE :: adaptDivU = 0
  REAL (pr), PRIVATE :: magVortScale=1.0_pr, comVortScale=1.0_pr, magVelScale=1.0_pr, normSScale=1.0_pr, gradYScale=1.0_pr, buoyScale=1.0_pr
  REAL (pr), PRIVATE :: denScale=1.0_pr, momScale=1.0_pr, engScale=1.0_pr, specScale=1.0_pr, prsScale=1.0_pr, tempScale=1.0_pr
  REAL (pr), PRIVATE :: divUScale=1.0_pr
  LOGICAL, PRIVATE :: saveMagVort = .FALSE., saveComVort = .FALSE., saveMagVel = .FALSE., saveNormS = .FALSE., saveGradY = .FALSE., savebuoys = .FALSE.  !!!!
  LOGICAL, PRIVATE :: saveQCrit = .FALSE., saveDivU = .FALSE.

  LOGICAL, PROTECTED :: adaptvelonly = .FALSE., adaptspconly = .FALSE., adaptengonly = .FALSE.                   
  LOGICAL, PROTECTED :: savetmp = .FALSE.
  INTEGER, PROTECTED :: vel_vect_scale = 0        ! Scale for the velocity vector, defaults to the main scale selection

  LOGICAL :: inviscidTop = .FALSE., adiabaticBtm = .FALSE.

  REAL (pr), DIMENSION (:) , ALLOCATABLE :: distw_t !true wall distance. It is allocated and calcualted in the user_case module.
  REAL (pr), PRIVATE :: L_distw !length scale for wall distance function in CFL calculation, usually set to boundary layer thickness.


  ! Function lists - All functions must be explicitly listed either public or
  ! public for clarity
  PUBLIC :: &
    compressible_setup_pde, &   ! Set up the variable counts
    compressible_finalize_setup_pde, &     ! Set up the pde variable mappings (adapt, interpolate, save, etc.)
    compressible_pre_process, & 
    compressible_post_process, &
    compressible_cal_cfl,  &
    compressible_read_input, &
    compressible_scales, &
    compressible_rhs, &
    navier_stokes_rhs, &
    compressible_drhs, &
    compressible_drhs_diag, &
    compressible_additional_vars, &
    calc_speed_of_sound, &
    calc_temperature, &
    calc_pressure, &
    set_body_force, &
    set_F_compress, &
    set_thermo_props, &
    set_dynamic_viscosity, &
    set_conductivity, &
    set_diffusivity

  ! Helper functions
  PUBLIC :: &
    calc_dynamic_viscosity, &
    calc_conductivity, &
    calc_spec_diffusion, &
    fraction_avg1, &
    fraction_avg2, &
    fraction_avg3

  ! ERIC: check that all of the helper functions are legit
  ! Private
  PRIVATE :: &
    !dpdx, &
    !dpdx_diag, &
    bndfunc, &
    flxfunc, &
    buffkine, &
    navier_stokes_Drhs, &
    navier_stokes_Drhs_diag, &
    bonusvars_vel, &
    bonusvars_spc!, &
    !fraction_avg1, &
    !fraction_avg2, &
    !fraction_avg3

  PUBLIC ::  calc_nondim_sutherland_visc

  
CONTAINS

  !
  ! Read input from "case_name"_pde.inp file
  ! case_name is in string file_name read from command line
  ! in read_command_line_input()
  ! 
  SUBROUTINE compressible_read_input()
    USE error_handling
    IMPLICIT NONE
    INTEGER :: i, j
    CHARACTER(LEN=8):: numstrng
    LOGICAL :: checkit 
    LOGICAL :: fail

    IF (par_rank.EQ.0) THEN
       PRINT *, '*******************Compressible Equation Inputs*******************'
    END IF

    call input_logical ('GRAV',GRAV,'stop', 'GRAV: T to include body force terms, F to ingore them')
    call input_logical ('NS',NS,'stop', 'NS: T to include viscous terms (full Navier-Stokes), F for Euler Eqs')
    call input_logical ('NSdiag',NSdiag,'stop', 'NSdiag: T to use viscous terms in user_rhs_diag')

    call input_integer ('Nspec',Nspec,'stop','  Nspec: Number of species')
    call input_integer ('methpd',methpd,'stop','  methpd: meth for taking pressure derivative in time')
    call input_integer ('dervFlux',dervFlux,'stop', 'dervFlux: 0 to do derivative fluxes, 1 to set fluxes to 0 on boundary only')
    CALL input_integer ('kinevisc',kinevisc,'stop', 'kinevisc: scale mu and kk by density (1=global, 2=use zone, 3=zone with edgevalues), 0=use dynamic visc and thermal conductivity')

    CALL input_integer ('viscmodel',viscmodel,'stop', 'viscmodel: 0 - const, 1-sutherland') 
    IF ( viscmodel .LT. viscmodel_const .OR. viscmodel .GT. viscmodel_sutherland ) CALL error('Invalid selection for viscous model')
    IF ( viscmodel .EQ. viscmodel_sutherland )CALL input_real ('sutherland_const',sutherland_const,'stop', 'sutherland_const: Sutherland temperature (nondimensionalized by reference temperature') 


    ! Allocate and initialize  
    ALLOCATE(gr_in(1:Nspec*dim))  
    gr_in(:) = 0.0_pr
    ALLOCATE(bD_in(1:Nspec))
    bD_in(:) = 1.0_pr
    ALLOCATE(mu_in(1:Nspec))
    mu_in(:) = 1.0_pr
    ALLOCATE(kk_in(1:Nspec))
    kk_in(:) = 1.0_pr
    ALLOCATE(cp_in(1:Nspec))
    cp_in(:) = 1.0_pr
    ALLOCATE(MW_in(1:Nspec))
    MW_in(:) = 1.0_pr
    ALLOCATE(gamm(1:Nspec))
    gamm(:) = 1.0_pr
    ALLOCATE(cv_in(1:Nspec))
    ALLOCATE(R_in(1:Nspec))
    F_compress = 1.0 

    call input_real_vector ('gamm',gamm(1:Nspec),Nspec,'stop',' gamm: cp/(cp-R)')
    call input_real_vector ('MW',MW_in(1:Nspec),Nspec,'stop',' MW: Normalized molecular weight')
    DO i = 1,Nspec
      IF ( gamm(i) .LT. 1.0_pr ) CALL error('Gamma (heat capacity ratio) must be > 1 ')
      IF ( MW_in(i) .LT. 0.0_pr ) CALL error('Molecular weight must be > 0')
    END DO 
    


    ! CFL control
    call input_real ('visccfl',visccfl,'stop', '|visccfl|: cfl to impose on diffusion terms, set <0 to not check in time')
    call input_real ('imexcfl',imexcfl,'stop', 'imexcfl  : cfl threshold for going to imex, set <0 to not check in time')
    call input_logical ('convcfl',convcfl,'stop', 'convcfl: T to use convective cfl, F to use acoustic cfl')
    hybridcfl = .FALSE.
    call input_logical ('hybridcfl',hybridcfl,'default', 'hybridcfl: T to use hybrid CFL with wall-damping and directional anisotropy')

    !call input_logical ('evolBC',evolBC,'stop', 'evolBC: T to use evol BCs, F to use algebraic BCs')
    call input_logical ('splitFBpress',splitFBpress,'stop', 'splitFBpress: to split derivs for pressure dp/dx FB/BB light/heavy')
    call input_logical ('stepsplit',stepsplit,'stop', 'stepsplit: T to do Heaviside split, F to use X')
    call input_logical ('boundY',boundY,'stop', 'boundY: T to bound Y to [0,1] in user_pre/post_process')
    call input_logical ('bonusCFL',bonusCFL,'stop', 'bonusCFL: T to do CFL check for modulus strain rate and magnitude of vorticity')
    CALL input_logical ('ComboIMEXRK',ComboIMEXRK,'stop', 'ComboIMEXRK: T to use IMEX when cfl for diffusion terms becomes restrictive, RK otherwise')

    ! Variables control

    call input_integer ('adaptden',adaptden,'stop',         'adapt on density: 0 - off, 1 - const scale, 2 - dynamic scale')
    call input_real    ('denScale',denScale,'stop',         '   denScale: scale/coefficient for density')
    call input_integer ('adaptmom',adaptmom,'stop',         'adapt on momentum: 0 - off, 1 - const scale, 2 - dynamic scale')
    call input_real    ('momScale',momScale,'stop',         '   momScale: scale/coefficient for momentum')
    vel_vect_scale = Scale_Meth 
    call input_integer ('vel_vect_scale',vel_vect_scale,'default',         '   vel_vect_scale: Scale for the velocity vector, defaults to global method (1-Linf,2-L2,3-L2)')
    call input_logical ('adaptvelonly',adaptvelonly,'stop', '   adaptvelonly: T to adapt on velocity instead of momentum')
    call input_integer ('adapteng',adapteng,'stop',         'adapt on energy: 0 - off, 1 - const scale, 2 - dynamic scale')
    call input_real    ('engScale',engScale,'stop',         '   engScale: scale/coefficient for energy')
    call input_logical ('adaptengonly',adaptengonly,'stop', '   adaptengonly: T to adapt on specific energy instead of total')
    call input_integer ('adaptspc',adaptspc,'stop',         'adapt on species: 0 - off, 1 - const scale, 2 - dynamic scale')
    call input_real    ('specScale',specScale,'stop',       '   specScale: scale/coefficient for species')
    call input_logical ('adaptspconly',adaptspconly,'stop', '   adaptspconly: T to adapt on mass fraction (Y) instead of volume fraction (rhoY)')
    call input_integer ('adaptprs',adaptprs,'stop',         'adapt on pressure: 0 - off, 1 - const scale, 2 - dynamic scale')
    call input_real    ('prsScale',prsScale,'stop',         '   prsScale: scale/coefficient for pressure')
    call input_integer ('adapttmp',adapttmp,'stop',         'adapt on temperature: 0 - off, 1 - const scale, 2 - dynamic scale')
    call input_real    ('tempScale',tempScale,'stop',       '   tempScale: scale/coefficient for temperature')
    call input_logical ('savetmp',savetmp,'stop',           'savetmp: T to save temperature')

    call input_integer ('adaptbuoy',adaptbuoy,'stop',       'adaptbuoy: T to adapt on buoy term: 0 - off, 1 - const scale, 2 - dynamic scale')
    call input_real    ('buoyScale',buoyScale,'stop',       '   buoyScale: scale/coefficient for buoyancy term')
    call input_logical ('savebuoys',savebuoys,'stop',       'savebuoys: if T adds and saves dPdx and -(dPdx + rho*g)')
    call input_integer ('adaptMagVel',adaptMagVel,'stop',   'adaptMagVel: Adapt on the magnitude of velocity: 0 - off, 1 - const scale, 2 - dynamic scale')
    call input_real    ('magVelScale',magVelScale,'stop',   '   magVelScale: scale/coefficient for velocity magnitude')
    call input_logical ('saveMagVel',saveMagVel,'stop',     'saveMagVel: if T adds and saves magnitude of velocity')
    call input_integer ('adaptNormS',adaptNormS,'stop',     'adaptNormS: Adapt on the L2 of Sij: 0 - off, 1 - const scale, 2 - dynamic scale')
    call input_real    ('normSScale',normSScale,'stop',     '   normSScale: scale/coefficient for |S|')
    call input_logical ('saveNormS',saveNormS,'stop',       'saveNormS: if T adds and saves magnitude of strain rate')
    call input_integer ('adaptGradY',adaptGradY,'stop',     'adaptGradY: Adapt on |dY/dx_i*dY/dx_i|: 0 - off, 1 - const scale, 2 - dynamic scale')
    call input_real    ('gradYScale',gradYScale,'stop',     '   gradYScale: scale/coefficient for species grad term')
    call input_logical ('saveGradY',saveGradY,'stop',       'saveGradY: if T adds and saves |dY/dx_i*dY/dx_i|')
    IF ( dim .GE. 2 ) THEN ! Vorticity not defined in one dimension
      call input_integer ('adaptMagVort',adaptMagVort,'stop','adaptMagVort: Adapt on the magnitude of vorticity: 0 - off, 1 - const scale, 2 - dynamic scale')
      call input_real    ('magVortScale',magVortScale,'stop','   magVortScale: scale/coefficient for magnitude of vorticity')
      call input_logical ('saveMagVort',saveMagVort,'stop',  'saveMagVort: if T adds and saves magnitude of vorticity')
      call input_integer ('adaptComVort',adaptComVort,'stop','adaptComVort: Adapt on the components of vorticity: 0 - off, 1 - const scale, 2 - dynamic scale')
      call input_real    ('comVortScale',comVortScale,'stop','   comVortScale: scale/coefficient for components of vorticity')
      call input_logical ('saveComVort',saveComVort,'stop',  'saveComVort: if T adds and saves components of vorticity')
    END IF
    call input_logical ('saveQCrit',saveQCrit,'default',       'saveQCrit: if T adds and saves Q-criterion')
    call input_integer ('adaptDivU',adaptDivU,'default',       'adaptDivU: T to adapt on div(u) term: 0 - off, 1 - const scale, 2 - dynamic scale')
    call input_real    ('divUScale',divUScale,'default',       '   divUScale: scale/coefficient for divergence of velocity')
    call input_logical ('saveDivU',saveDivU,'default',         'saveDivU: if T adds and saves Divergence of Velocity')

    !Needs to be moved to user case ================================= 
    call input_logical ('inviscidTop',inviscidTop,'default',         'inviscidTop: if T imposes zero viscous fluxes at the top wall. All operations in rhs and Drhs subroutines.')
    call input_logical ('adiabaticBtm',adiabaticBtm,'default',         'adiabaticBtm: if T imposes dT/dn = 0 at the bottom wall. All operations in rhs and Drhs subroutines.')

    L_distw = 1.0_pr
    call input_real    ('L_distw',L_distw,'default',         '   L_distw: length scale for wall distance function in CFL calculation, usually set to boundary layer thickness.')
    !=================================================================

    ! Sanity checks
    fail = .FALSE.
    IF ( adaptden .LT. 0 .OR. adaptden .GT. 2 )     fail = .TRUE. 
    IF ( adaptmom .LT. 0 .OR. adaptmom .GT. 2 )     fail = .TRUE. 
    IF ( adapteng .LT. 0 .OR. adapteng .GT. 2 )     fail = .TRUE. 
    IF ( adaptspc .LT. 0 .OR. adaptspc .GT. 2 )     fail = .TRUE. 
    IF ( adaptprs .LT. 0 .OR. adaptprs .GT. 2 )     fail = .TRUE. 
    IF ( adapttmp .LT. 0 .OR. adapttmp .GT. 2 )     fail = .TRUE. 
    IF ( adaptMagVort .LT. 0 .OR. adaptMagVort .GT. 2 ) fail = .TRUE. 
    IF ( adaptComVort .LT. 0 .OR. adaptComVort .GT. 2 ) fail = .TRUE. 
    IF ( adaptMagVel .LT. 0 .OR. adaptMagVel .GT. 2 )  fail = .TRUE. 
    IF ( adaptNormS .LT. 0 .OR. adaptNormS .GT. 2 )   fail = .TRUE. 
    IF ( adaptGradY .LT. 0 .OR. adaptGradY .GT. 2 )   fail = .TRUE. 
    IF ( adaptbuoy .LT. 0 .OR. adaptbuoy .GT. 2 )    fail = .TRUE. 
    IF ( fail ) THEN
       IF (par_rank .EQ. 0) PRINT *, "ERROR: bad value for one of the adapt flags.  Pleach check that all are between 0 and 2.  Exiting..."
       CALL parallel_finalize()
       STOP
    END IF
 
    IF (bonusCFL) saveMagVort = .TRUE.
    IF (bonusCFL) saveNormS   = .TRUE.

    ! Set defaults for a single species flow
    IF (Nspec<2) THEN
       adaptspc = 0         !.FALSE. 
       adaptspconly=.FALSE. 
       adaptGradY = 0       !.FALSE. 
       saveGradY=.FALSE. 
    END IF
    Nspecm = Nspec - 1

    !IF (NOT(evolBC)) dervFlux = 0

    IF (IC_restart_mode .EQ. 1) eps_init=eps_run

    CFLnonl=0.1_pr

    IF (par_rank.EQ.0) THEN
       PRINT *, '********************************************************'
    END IF

  END SUBROUTINE compressible_read_input



   !  Setup the compressible pde equations, registering all of the variables
   SUBROUTINE  compressible_setup_pde () 
      USE variable_mapping
      IMPLICIT NONE
      INTEGER :: i, l   !4extra
      CHARACTER(LEN=8) :: specnum  !4extra
      CHARACTER (LEN=u_variable_name_len) :: specname 
      LOGICAL :: tmp_adapt, tmp_save
      
      IF (par_rank.EQ.0) THEN
        PRINT * ,''
         PRINT *, '******************* Setting up PDE ******************'
         PRINT * ,'            USING COMPRESSIBLE EQUATIONS             '
         PRINT *, '*****************************************************'
      END IF

      ! Integrated compressible flow variables
     
      ! Density
      !CALL register_var( 'Density_rho  ',  integrated=.TRUE.,  adapt=(/adaptden.GE.0,adaptden.GE.0/),   interpolate=(/.TRUE.,.TRUE./), &
      CALL register_var( 'Density_rho  ',  integrated=.TRUE.,  adapt=(/adaptden.GT.0,adaptden.GT.0/),   interpolate=(/.TRUE.,.TRUE./), &
          exact=(/.FALSE.,.FALSE./),   saved=.TRUE., req_restart=.TRUE., FRONT_LOAD=.TRUE. )

      ! Velocity
      !tmp_adapt = (adaptmom .GE. 0) .AND. .NOT. adaptvelonly
      tmp_adapt = (adaptmom .GT. 0) .AND. .NOT. adaptvelonly
      CALL register_var( 'X_Momentum  ',  integrated=.TRUE.,  adapt=(/tmp_adapt, tmp_adapt/),   interpolate=(/.TRUE.,.TRUE./), &  
          exact=(/.FALSE.,.FALSE./),   saved=.TRUE., req_restart=.TRUE., FRONT_LOAD=.TRUE. )
      IF( dim .GE. 2 ) CALL register_var( 'Y_Momentum  ',  integrated=.TRUE.,  adapt=(/tmp_adapt, tmp_adapt/),   interpolate=(/.TRUE.,.TRUE./), &  
          exact=(/.FALSE.,.FALSE./),   saved=.TRUE., req_restart=.TRUE., FRONT_LOAD=.TRUE. )
      IF( dim .GE. 3 ) CALL register_var( 'Z_Momentum  ',  integrated=.TRUE.,  adapt=(/tmp_adapt, tmp_adapt/),   interpolate=(/.TRUE.,.TRUE./), &
          exact=(/.FALSE.,.FALSE./),   saved=.TRUE., req_restart=.TRUE., FRONT_LOAD=.TRUE. )

      ! Energy
      !tmp_adapt = (adapteng.GE. 0) .AND. .NOT. adaptengonly
      tmp_adapt = (adapteng.GT. 0) .AND. .NOT. adaptengonly
      CALL register_var( 'E_Total  ',  integrated=.TRUE.,  adapt=(/tmp_adapt,tmp_adapt/),    interpolate=(/.TRUE.,.TRUE./), &
          exact=(/.FALSE.,.FALSE./),   saved=.TRUE., req_restart=.TRUE., FRONT_LOAD=.TRUE. )

      ! Species
      IF (Nspec>1) THEN
        tmp_adapt = (adaptspc.GE.0) .AND. .NOT. adaptspconly
        DO l=1,Nspecm      
            WRITE (specnum,'(I8)') l          
            WRITE (specname, u_variable_names_fmt) TRIM('Species_Scalar_'//ADJUSTL(TRIM(specnum)))//'  '
            CALL register_var( specname,  integrated=.TRUE.,  adapt=(/tmp_adapt,tmp_adapt/),   interpolate=(/.TRUE.,.TRUE./), &
                exact=(/.FALSE.,.FALSE./),   saved=.TRUE., req_restart=.TRUE., FRONT_LOAD=.TRUE. )
        END DO
      END IF


      ! Pressure
      CALL register_var( 'Pressure  ', integrated=.FALSE.,  adapt=(/adaptprs.GT.0,adaptprs.GT.0/),   interpolate=(/.TRUE.,.TRUE./), &
          exact=(/.FALSE.,.FALSE./),   saved=.TRUE., req_restart=.TRUE., FRONT_LOAD=.TRUE. )


      ! Additional Variables

      ! Temperature
      IF (adapttmp .GT. 0 .OR. savetmp) THEN 
        CALL register_var( 'Temperature ',       integrated=.FALSE.,  adapt=(/adapttmp.GT.0,adapttmp.GT.0/),   interpolate=(/savetmp,savetmp/), &
            exact=(/.FALSE.,.FALSE./),   saved=savetmp, req_restart=.FALSE. )
      END IF

      ! Velocity
      IF ( adaptmom .GT. 0 .AND. adaptvelonly ) THEN 
        CALL register_var( 'X_Velocity  ', integrated=.FALSE.,  adapt=(/.TRUE.,.TRUE./),   interpolate=(/.TRUE.,.TRUE./), &
            exact=(/.FALSE.,.FALSE./),   saved=.TRUE., req_restart=.FALSE. )
        IF( dim .GE. 2) CALL register_var( 'Y_Velocity  ', integrated=.FALSE.,  adapt=(/.TRUE.,.TRUE./),   interpolate=(/.TRUE.,.TRUE./),&
            exact=(/.FALSE.,.FALSE./),   saved=.TRUE., req_restart=.FALSE. )
        IF( dim .GE. 3) CALL register_var( 'Z_Velocity  ', integrated=.FALSE.,  adapt=(/.TRUE.,.TRUE./),   interpolate=(/.TRUE.,.TRUE./), &
            exact=(/.FALSE.,.FALSE./),   saved=.TRUE., req_restart=.FALSE. )
      END IF

      ! Specific Total Energy
      IF ( adapteng .GT. 0 .AND. adaptengonly ) THEN 
        CALL register_var( 'Spec_Energy', integrated=.FALSE.,  adapt=(/.TRUE.,.TRUE./),   interpolate=(/.TRUE.,.TRUE./), &
            exact=(/.FALSE.,.FALSE./),   saved=.TRUE., req_restart=.FALSE. )
      END IF

      ! Species mass fraction 
      IF ( adaptspc .GT. 0 .AND. adaptspconly .AND. Nspec > 1) THEN
        DO l=1,Nspecm
          WRITE (specnum,'(I8)') l          
          WRITE (specname, u_variable_names_fmt) TRIM('Mass_Frac_'//ADJUSTL(TRIM(specnum)))//'  '
          CALL register_var( specname,       integrated=.FALSE.,  adapt=(/.TRUE.,.TRUE./),   interpolate=(/.TRUE.,.TRUE./), & 
              exact=(/.FALSE.,.FALSE./),   saved=.TRUE., req_restart=.FALSE. )
        END DO
      END IF

      ! Magnitude of Vorticity 
      IF( adaptMagVort .GT. 0 .or. saveMagVort ) THEN
        CALL register_var( 'MagVort  ',  integrated=.FALSE.,  adapt=(/adaptMagVort .GT. 0,adaptMagVort .GT. 0/),   interpolate=(/saveMagVort,saveMagVort/), & 
            exact=(/.FALSE.,.FALSE./),   saved=saveMagVort, req_restart=.FALSE. )
      END IF

      ! Components of Vorticity
      IF( (adaptComVort .GT. 0 .or. saveComVort) .AND. dim==2 ) THEN
        CALL register_var( 'ComVort  ',  integrated=.FALSE.,  adapt=(/adaptComVort.GT.0,adaptComVort.GT.0/),   interpolate=(/saveComVort,saveComVort/), &
            exact=(/.FALSE.,.FALSE./),   saved=saveComVort, req_restart=.FALSE. )
      END IF
      IF( (adaptComVort .GT. 0 .or. saveComVort) .AND. dim==3 ) THEN
        CALL register_var( 'ComVort_1  ',  integrated=.FALSE.,  adapt=(/adaptComVort.GT.0,adaptComVort.GT.0/),   interpolate=(/saveComVort,saveComVort/), &
            exact=(/.FALSE.,.FALSE./),   saved=saveComVort, req_restart=.FALSE. )
        CALL register_var( 'ComVort_2  ',  integrated=.FALSE.,  adapt=(/adaptComVort.GT.0,adaptComVort.GT.0/),   interpolate=(/saveComVort,saveComVort/), &
            exact=(/.FALSE.,.FALSE./),   saved=saveComVort, req_restart=.FALSE. )
        CALL register_var( 'ComVort_3  ',  integrated=.FALSE.,  adapt=(/adaptComVort.GT.0,adaptComVort.GT.0/),   interpolate=(/saveComVort,saveComVort/), &
            exact=(/.FALSE.,.FALSE./),   saved=saveComVort, req_restart=.FALSE. )
      END IF

      ! Magnitude of velocity
      IF( adaptMagVel .GT. 0  .OR. saveMagVel  ) THEN
        CALL register_var( 'MagVel  ',       integrated=.FALSE.,  adapt=(/adaptMagVel.GT.0,adaptMagVel.GT.0/),   interpolate=(/saveMagVel,saveMagVel/), &
            exact=(/.FALSE.,.FALSE./),   saved=saveMagVel, req_restart=.FALSE. )
      END IF
      
      ! Norm of the strain rate tensor
      IF( adaptNormS .GT. 0 .OR. saveNormS   ) THEN
        CALL register_var( 'NormS  ',       integrated=.FALSE.,  adapt=(/adaptNormS .GT. 0,adaptNormS .GT. 0/),   interpolate=(/saveNormS,saveNormS/), &
            exact=(/.FALSE.,.FALSE./),   saved=saveNormS, req_restart=.FALSE. )
      END IF

      ! Buoyancy
      IF ( adaptbuoy .GT. 0 .OR. savebuoys) THEN 
        CALL register_var( 'Buoyancy ',       integrated=.FALSE.,  adapt=(/adaptbuoy.NE. 0,adaptbuoy.NE. 0/),   interpolate=(/savebuoys,savebuoys/), &
            exact=(/.FALSE.,.FALSE./),   saved=savebuoys, req_restart=.FALSE. )
      END IF

      ! Gradient of the species fraction
      IF( adaptGradY .GT. 0 .OR. saveGradY   ) THEN 
        IF (Nspec>1) THEN
            DO l=1,Nspecm
              WRITE (specnum,'(I8)') l          
              WRITE (specname, u_variable_names_fmt) TRIM('GradY_'//ADJUSTL(TRIM(specnum)))//'  '
              CALL register_var( specname,       integrated=.FALSE.,  adapt=(/adaptGradY.GT.0,adaptGradY.GT.0/), interpolate=(/saveGradY,saveGradY/), &
                  exact=(/.FALSE.,.FALSE./),   saved=saveGradY, req_restart=.FALSE. )
            END DO
        END IF
      END IF

      ! Q-criterion 
      IF( saveQCrit   ) THEN
        CALL register_var( 'QCriterion  ',       integrated=.FALSE.,  adapt=(/.FALSE.,.FALSE./),   interpolate=(/saveQCrit,saveQCrit/), &
            exact=(/.FALSE.,.FALSE./),   saved=saveQCrit, req_restart=.FALSE. )
      END IF

      ! Q-criterion 
      IF( adaptDivU .GT. 0 .OR. saveDivU   ) THEN
        CALL register_var( 'UDivergence  ',       integrated=.FALSE.,  adapt=(/adaptDivU .GT. 0,adaptDivU .GT. 0/),   interpolate=(/saveDivU,saveDivU/), &
            exact=(/.FALSE.,.FALSE./),   saved=saveDivU, req_restart=.FALSE. )
      END IF

   END SUBROUTINE  compressible_setup_pde

  
   !  Setup the compressible pde equations: set mappings (indices, adapt, interpolate, save)
   !    start_integrated: indice in u-array to start the contiguous block of integrated compressible equations 
   !    start_additional: indice in u-array to start the contiguous block of additional compressible variables 
   SUBROUTINE  compressible_finalize_setup_pde()
      USE variable_mapping
      IMPLICIT NONE
      INTEGER :: i, l   !4extra
      CHARACTER(LEN=8) :: specnum  !4extra
      CHARACTER (LEN=u_variable_name_len) :: specname 

      scaleCoeff = 1.0_pr

      ! Allocate variable indices
      IF (ALLOCATED(n_mom)) DEALLOCATE(n_mom)
      IF (ALLOCATED(n_spc)) DEALLOCATE(n_spc)
      ALLOCATE( n_mom(dim) )
      IF( Nspec .GT. 1 ) ALLOCATE(n_spc(Nspecm))  ! Only allocate species fields if the total number of species is greater than one

      ! Integrated Variables 
      n_den                     = get_index('Density_rho') 
      n_mom(1)                  = get_index('X_Momentum') 
      IF( dim .GE. 2 )n_mom(2)  = get_index('Y_Momentum') 
      IF( dim .GE. 3 )n_mom(3)  = get_index('Z_Momentum') 
      n_eng = get_index('E_Total') 
      IF (Nspec>1) THEN
        DO i=1,Nspecm
          WRITE (specnum,'(I8)') i          
          WRITE (specname, u_variable_names_fmt) TRIM('Species_Scalar_'//ADJUSTL(TRIM(specnum)))//'  '
          n_spc(i) = get_index(specname) 
        END DO
      END IF

      IF (adaptden .GT. 0) scaleCoeff(n_den)                  = denScale 
      IF (adaptmom .GT. 0) scaleCoeff(n_mom(1):n_mom(dim))    = momScale
      IF (adapteng .GT. 0) scaleCoeff(n_eng)                  = engScale
      IF (adaptspc .GT. 0 .AND. Nspec .GT. 1) scaleCoeff(n_spc(1):n_spc(Nspecm)) = specScale

      ! Additional variables
      n_prs = get_index('Pressure') 
      IF (adaptprs .GT. 0) scaleCoeff(n_prs)              = prsScale 

      ! Temperature 
      n_temp = get_index('Temperature')
      IF (adapttmp .GT. 0) scaleCoeff(n_temp)                = tempScale 

      ! Velocity 
      IF (ALLOCATED(n_vel)) DEALLOCATE(n_vel)
      ALLOCATE (n_vel(dim))
      n_vel(1) = get_index('X_Velocity') 
      IF(dim.GE.2) n_vel(2) = get_index('Y_Velocity') 
      IF(dim.GE.3) n_vel(3) = get_index('Z_Velocity') 
      IF ( adaptvelonly ) THEN
         scaleCoeff(n_mom(1):n_mom(dim))            = 1.0 
         scaleCoeff(n_vel(1):n_vel(dim))            = momScale 
      END IF

      ! Magnitude of Velocity 
      n_mvel = get_index('MagVel') 
      IF (adaptMagVel .GT. 0) scaleCoeff(n_mvel)             = magVelScale 

      ! Specific energy
      n_eon = get_index('Spec_Energy') 
      IF( adaptengonly) THEN 
        scaleCoeff(n_eng)            = 1.0 
        scaleCoeff(n_eon)            = engScale
      END IF

      ! Species Mass fractions
      IF (ALLOCATED(n_son)) DEALLOCATE(n_son)
      ALLOCATE (n_son(Nspecm))
      DO l=1,Nspecm
        WRITE (specnum,'(I8)') l          
        WRITE (specname, u_variable_names_fmt) TRIM('Mass_Frac_'//ADJUSTL(TRIM(specnum)))//'  '
        n_son(l) = get_index(specname) 
      END DO
      IF ( adaptspc .GT. 0 .AND. adaptspconly .AND. Nspec > 1 ) THEN 
        scaleCoeff(n_spc(1):n_spc(Nspecm))            = 1.0 
        scaleCoeff(n_son(1):n_son(Nspecm))            = specScale 
      END IF

      ! Buoyancy
      !n_dpdx = n_var_next   !ERIC: dpdx removed?
      !IF( savebuoys ) n_var_interpolate(n_dpdx,:)     = .TRUE.
      !IF (savebuoys) n_var_save(n_dpdx)               = .TRUE.
      !IF (savebuoys) n_var_req_restart(n_dpdx)        = .TRUE.
      n_buoy = get_index('Buoyancy')
      IF (adaptbuoy .GT. 0) scaleCoeff(n_buoy)       = buoyScale 

      ! Magnitude of Vorticity
        n_mvort = get_index('MagVort') 
        IF (adaptMagVort .GT. 0) scaleCoeff(n_mvort)            = magVortScale 

      ! Components of vorticity
      IF (ALLOCATED(n_cvort)) DEALLOCATE(n_cvort)
      IF( dim .EQ. 2) THEN
        ALLOCATE( n_cvort(1) )
        n_cvort(:) = get_index('ComVort') 
      ELSE IF ( dim .EQ. 3 ) THEN
        ALLOCATE( n_cvort(3) )
        n_cvort(1) = get_index('ComVort_1') 
        n_cvort(2) = get_index('ComVort_2') 
        n_cvort(3) = get_index('ComVort_3') 
      END IF
      IF (adaptComVort .GT. 0) scaleCoeff(n_cvort(:))            = comVortScale 

      ! Modulus of strain-rate
      n_mods = get_index( 'NormS' ) 
      IF (adaptNormS .GT. 0) scaleCoeff(n_mods)            = normSScale 

      ! Grad species
      IF (ALLOCATED(n_gdy)) DEALLOCATE(n_gdy)
      ALLOCATE(n_gdy(Nspecm))
      DO l=1,Nspecm
        WRITE (specnum,'(I8)') l
        WRITE (specname, u_variable_names_fmt) TRIM('GradY_'//ADJUSTL(TRIM(specnum)))//'  '
        n_gdy(l) = get_index(specname) 
      END DO
      IF (adaptGradY .GT. 0) scaleCoeff(n_gdy(:))            = gradYScale 

      ! Q-Criterion 
      n_qcrit = get_index('QCriterion') 

      ! Divergence of velocity
      n_divu = get_index('VelDivergence') 
      IF (adaptDivU .GT. 0) scaleCoeff(n_divu)             = divUScale 

      IF (par_rank.EQ.0) THEN
         PRINT *, '*******************Variable Names*******************'
         DO i = 1,n_var
            WRITE (*, u_variable_names_fmt) u_variable_names(i)
         END DO
         PRINT *, '****************************************************'
      END IF

  END SUBROUTINE  compressible_finalize_setup_pde



!  SUBROUTINE dpdx (pderiv,nlocal,Yloc,meth,myjl,split)
!    IMPLICIT NONE
!    INTEGER, INTENT (IN) :: nlocal
!    REAL (pr), DIMENSION (nlocal), INTENT(INOUT) :: pderiv
!    REAL (pr), DIMENSION (nlocal,Nspecm), INTENT(IN) :: Yloc
!    INTEGER, INTENT(IN) :: meth, myjl
!    LOGICAL, INTENT(IN) :: split
!    INTEGER :: i
!    REAL (pr), DIMENSION (nlocal,dim) :: du
!    REAL (pr), DIMENSION (nlocal,dim) :: d2u
!
!    IF (split) THEN 
!       CALL c_diff_fast(pderiv(:), du(:,:), d2u(:,:), myjl, nlocal, MOD(meth,2)+BIASING_BACKWARD, 10, 1, 1, 1)
!       d2u(:,1) = du(:,1)
!       CALL c_diff_fast(pderiv(:), du(:,:), d2u(:,:), myjl, nlocal, MOD(meth,2)+BIASING_FORWARD , 10, 1, 1, 1)
!       IF (stepsplit) THEN
!          d2u(:,2) = 1.0_pr
!          d2u(:,2) = (SIGN(d2u(:,2), Yloc(:,1) - Y_Xhalf) + 1.0_pr)/2.0_pr  ! ERIC: Y_Xhalf is not in this module
!       ELSE
!          d2u(:,2) = 1.0_pr  ! ERIC: check 
!          IF (Nspec>1) d2u(:,2) = (1.0_pr - SUM(Yloc(:,1:Nspecm),DIM=2))/MW_in(Nspec)  !Y_Nspec/W_Nspec
!          DO i=1,Nspecm
!             d2u(:,2) = d2u(:,2) + Yloc(:,i)/MW_in(i)  !Y_l/W_l
!          END DO
!          d2u(:,2) = Yloc(:,1)/MW_in(1)/d2u(:,2)  !X_1
!          d2u(:,2) = (d2u(:,2)-peakchange)/(1.0_pr-2.0_pr*peakchange)  !X undoing peak change
!       END IF
!       d2u(:,2) = MIN(1.0_pr,MAX(0.0_pr,d2u(:,2)))
!       pderiv(:) = d2u(:,2)*d2u(:,1) + (1.0_pr - d2u(:,2))*du(:,1)
!    ELSE
!       CALL c_diff_fast(pderiv(:), du(:,:), d2u(:,:), myjl, nlocal, meth, 10, 1, 1, 1)
!       pderiv(:) = du(:,1)
!    END IF
!  END SUBROUTINE dpdx
!
!  SUBROUTINE dpdx_diag (pd_diag,nlocal,Yloc,meth,myjl,split)
!    IMPLICIT NONE
!    INTEGER, INTENT (IN) :: nlocal
!    REAL (pr), DIMENSION (nlocal), INTENT(INOUT) :: pd_diag
!    REAL (pr), DIMENSION (nlocal,Nspecm), INTENT(IN) :: Yloc
!    INTEGER, INTENT(IN) :: meth, myjl
!    LOGICAL, INTENT(IN) :: split
!    INTEGER :: i
!    REAL (pr), DIMENSION (nlocal,dim) :: du_diag, d2u_diag
!
!    IF (split) THEN 
!       CALL c_diff_diag(du_diag, d2u_diag, myjl, nlocal, MOD(meth,2)+BIASING_BACKWARD, MOD(meth,2)+BIASING_FORWARD, 10) 
!       pd_diag(:) = du_diag(:,1)
!       CALL c_diff_diag(du_diag, d2u_diag, myjl, nlocal, MOD(meth,2)+BIASING_FORWARD, MOD(meth,2)+BIASING_BACKWARD, 10)   
!       IF (stepsplit) THEN
!          d2u_diag(:,2) = 1.0_pr
!          d2u_diag(:,2) = (SIGN(d2u_diag(:,2), Yloc(:,1) - Y_Xhalf) + 1.0_pr)/2.0_pr
!       ELSE
!          d2u_diag(:,2) = (1.0_pr - SUM(Yloc(:,1:Nspecm),DIM=2))/MW_in(Nspec)
!          DO i=1,Nspecm
!             d2u_diag(:,2) = d2u_diag(:,2) + Yloc(:,i)/MW_in(i)
!          END DO
!          d2u_diag(:,2)=Yloc(:,1)/MW_in(1)/d2u_diag(:,2)  !X
!          d2u_diag(:,2)=(d2u_diag(:,2)-peakchange)/(1.0_pr-2.0_pr*peakchange)  !X undoing peak change
!       END IF
!       d2u_diag(:,2) = MIN(1.0_pr,MAX(0.0_pr,d2u_diag(:,2)))
!       pd_diag(:) = d2u_diag(:,2)*pd_diag(:) + (1.0_pr - d2u_diag(:,2))*du_diag(:,1)
!    ELSE
!       CALL c_diff_diag(du_diag, d2u_diag, j_lev, ng, 2*methpd+meth, 2*methpd+meth, 10)   !dp/dx done using methpd 
!       pd_diag(:) = du_diag(:,1)
!    END IF
!  END SUBROUTINE dpdx_diag

!--********************************  
  ! Arguments
  ! u         - field on adaptive grid
  ! nlocal      - number of active points
  ! ne_local       - number of equations
  ! t         - current time
  !
  ! Global variables used
  ! ifn       - number of equations
  ! x()
  ! xO
  ! y0
  !--******************************** 

  SUBROUTINE bndfunc (bndonly, mynloc, myjl)
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: mynloc, myjl
    REAL (pr), DIMENSION (mynloc), INTENT(INOUT) :: bndonly
    INTEGER :: i
    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc
    bndonly(:) = 0.0_pr
    i_p_face(0) = 1
    DO i=1,dim
       i_p_face(i) = i_p_face(i-1)*3
    END DO
    DO face_type = 0, 3**dim - 1
       face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
       IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
          CALL get_all_indices_by_face (face_type, myjl, nloc, iloc)
          IF(nloc > 0 ) THEN
             IF (ABS(face(1)) == 1) THEN
                bndonly(iloc(1:nloc)) = 1.0_pr
             END IF
          END IF
       END IF
    END DO
  END SUBROUTINE bndfunc 

  SUBROUTINE flxfunc ( flx_func, mynloc, myjl)
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: mynloc, myjl
    REAL (pr), DIMENSION (dim,mynloc,dim), INTENT(INOUT) :: flx_func
    REAL (pr), DIMENSION (mynloc) :: myflx_func
    INTEGER :: i,j
       CALL bndfunc(myflx_func, mynloc, myjl)  !puts 1.0 on boundaries, 0.0 elsewhere 
       DO j=1,dim
          DO i=1,dim
             IF ((i.ne.j) .AND. (j.eq.1)) THEN
                flx_func(i,:,j) = 1.0_pr - myflx_func(:)
             ELSE
                flx_func(i,:,j) = 1.0_pr
             END IF
          END DO
       END DO
  END SUBROUTINE flxfunc

  SUBROUTINE buffkine (kinefunc, u_dens, mynloc)
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: mynloc
    REAL (pr), DIMENSION (mynloc), INTENT(INOUT) :: kinefunc
    REAL (pr), DIMENSION (mynloc), INTENT(IN) :: u_dens

    kinefunc(:) = u_dens(:)
    IF ( kinevisc .GT. 1 .OR. kinevisc .LT. 0) THEN
      IF (par_rank.EQ.0) THEN
        WRITE(*,'("************************ ERROR **************************")')
        WRITE(*,'("*******   KINEVIS IS ONLY FUNCTIONAL AS 1 OR 0   ********")')
        WRITE(*,'("************************ ERROR **************************")')
      END IF
    END IF
  
  END SUBROUTINE buffkine 

  SUBROUTINE navier_stokes_rhs (int_rhs,u_integrated, meth )
    USE variable_mapping
    USE curvilinear 
    USE curvilinear_mesh 
    IMPLICIT NONE
    REAL (pr), DIMENSION (ng,ne), INTENT(IN) :: u_integrated !1D flat version of integrated variables without BC 
    REAL (pr), DIMENSION (ng*ne), INTENT(INOUT) :: int_rhs
    INTEGER, INTENT(IN) :: meth 

    INTEGER :: i, j, l, ie, shift, tempintd, jshift
    REAL (pr), DIMENSION (ne*dim,ng,dim) :: du, d2u
    REAL (pr), DIMENSION (ng,ne*dim) :: F
    REAL (pr), DIMENSION (ng,dim+Nspec) :: v ! velocity components + temperature + Y
    REAL (pr) :: usej
    REAL (pr), DIMENSION (ng) :: p
    REAL (pr), DIMENSION (ng,3) :: props

    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc
    INTEGER :: wlt_type, k, j_df, imin, i_bnd, idim

    REAL (pr), DIMENSION(dim) :: normal_dir !inward unit normal vector at a certain points of bnd_coords
    REAL (pr), DIMENSION(dim) :: tang_dir   !unit tangential vector in flow direction at a certain points of bnd_coords

    REAL (pr) :: floor
        
    floor = 1.0e-12_pr

    int_rhs = 0.0_pr
    DO l = 1,Nspecm
       v(:,dim+1+l) = u_integrated(:,n_spc(l))/u_integrated(:,n_den) !Y_l
    END DO
    p(:) = 1.0_pr
    IF(Nspec>1)p(:) = (1.0_pr-SUM(v(:,dim+2:dim+Nspec),DIM=2))  !Y_Nspec
    props(:,1) = p(:)*cp_in(Nspec) !cp_Nspec
    !props(:,2) = p(:)/MW_in(Nspec) !R_Nspec
    props(:,2) = p(:)*R_in(Nspec) !R_Nspec
    DO l=1,Nspecm
       props(:,1) = props(:,1) + v(:,dim+1+l)*cp_in(l) !cp
       props(:,2) = props(:,2) + v(:,dim+1+l)*R_in(l) !R
       !props(:,2) = props(:,2) + v(:,dim+1+l)/MW_in(l) !R
    END DO
    p(:) = (u_integrated(:,n_eng)-0.5_pr*SUM(u_integrated(:,n_mom(1):n_mom(dim))**2,DIM=2)/u_integrated(:,n_den))/(props(:,1)/props(:,2)-1.0_pr) / F_compress  !pressure
    v(:,dim+1) = p(:)/props(:,2)/u_integrated(:,n_den)  !Temperature
    DO i = 1, dim
       v(:,i) = u_integrated(:,n_mom(i)) / u_integrated(:,n_den) ! u_i
    END DO
!    IF (((methpd .ne. 0) .OR. splitFBpress) .AND. IMEXswitch .LE. 0 .AND. Nspec>1 ) THEN  !ERIC: is there a general form of dpdx that works for single species?
!       du(1,:,1) = p(:)
!       CALL dpdx (du(1,:,1),ng,v(:,dim+2:dim+Nspec),2*methpd+meth,j_lev,splitFBpress)
!       shift = (n_mom(1)-1)*ng   
!       int_rhs(shift+1:shift+ng) = int_rhs(shift+1:shift+ng)-du(1,:,1)
!       shift = (n_eng-1)*ng   
!       int_rhs(shift+1:shift+ng) = int_rhs(shift+1:shift+ng)-du(1,:,1)*v(:,1) !u*dp/dx
!       CALL c_diff_fast(v(:,1), du(1,:,:), d2u(1,:,:), j_lev, ng, meth, 10, 1, 1, 1) ! du/dx
!       int_rhs(shift+1:shift+ng) = int_rhs(shift+1:shift+ng)-du(1,:,1)*p(:)   !p*du/dx
!    END IF
    !
    !--Form right hand side of Euler Equations
    !
    F(:,:) = 0.0_pr
    IF (IMEXswitch .LE. 0) THEN
      DO j = 1, dim
        jshift=ne*(j-1)
        F(:,n_den+jshift) = F(:,n_den+jshift) + u_integrated(:,n_mom(j)) ! rho*u_j
        DO i = 1, dim
          F(:,n_mom(i)+jshift) = F(:,n_mom(i)+jshift) + u_integrated(:,n_mom(i))*v(:,j) ! rho*u_i*u_j
        END DO
        F(:,n_mom(j)+jshift) = F(:,n_mom(j)+jshift) + F_compress * p(:) ! rho*u_i^2+p
        F(:,n_eng+jshift) = F(:,n_eng+jshift) + ( u_integrated(:,n_eng) + F_compress * p(:) )*v(:,j) ! (rho*e+p)*u_j
        DO l=1,Nspecm
          F(:,n_spc(l)+jshift) = F(:,n_spc(l)+jshift) + v(:,dim+1+l)*u_integrated(:,n_mom(j)) ! rho*Y*u_j
        END DO  
      END DO
      IF (((methpd .ne. 0) .OR. splitFBpress)) THEN
        F(:,n_mom(1)) = F(:,n_mom(1)) - F_compress * p(:)
        F(:,n_eng)    = F(:,n_eng)    - F_compress * p(:)*v(:,1)
      END IF
    END IF
    !
    !-- Form right hand side of Navier-Stokes Equations
    !
  IF (IMEXswitch .GE. 0) THEN
    IF(NS) THEN 
       CALL c_diff_fast(v, du(1:dim+Nspec,:,:), d2u(1:dim+Nspec,:,:), j_lev, ng, meth, 10, dim+Nspec, 1,dim+Nspec) ! du_i/dx_j & dT/dx_j & dY_l/dx_j
       p(:)=1.0_pr
       IF (Nspec>1) p(:)=(1.0_pr-SUM(v(:,dim+2:dim+Nspec),DIM=2))
       props(:,1)=p(:)*mu_in(Nspec)  !mu_Nspec = Re^(-1)
       props(:,2)=p(:)*kk_in(Nspec)  !kk_Nspec = Re^(-1)*Pra^(-1)*gamma/(gamma-1)
       props(:,3)=p(:)*bD_in(Nspec)  !bD_Nspec

       DO l=1,Nspecm
          props(:,1)=props(:,1)+v(:,dim+1+l)*mu_in(l)  !mu
          props(:,2)=props(:,2)+v(:,dim+1+l)*kk_in(l)  !kk
          props(:,3)=props(:,3)+v(:,dim+1+l)*bD_in(l)  !bD
       END DO
       IF (kinevisc .GT. 0) THEN
          CALL buffkine (p, u_integrated(:,n_den), ng)
          props(:,1) = props(:,1)*p(:)
          props(:,2) = props(:,2)*p(:)
       END IF

!Oleg 10.18.2018: This section is case dependent and needs to be moved out to the specific case. Kept for now.
!------------------- Begin special treatments for inviscid top and adiabatic bottom BCs, not a generalized implementation. Needs improvement!------------------
       IF (inviscidTop) THEN
          !Imposing inviscid condition at the top wall
          i_p_face(0) = 1
          DO i=1,dim
             i_p_face(i) = i_p_face(i-1)*3
          END DO
          DO face_type = 0, 3**dim - 1
             face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
             IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
                CALL get_all_indices_by_face (face_type, j_lev, nloc, iloc)
                IF(nloc > 0 ) THEN 
                  IF( face(2) == 1 ) THEN ! inviscid top wall
                         props(iloc(1:nloc),1) = 0.0_pr
                         props(iloc(1:nloc),2) = 0.0_pr
                  END IF
                END IF
             END IF
          END DO
       END IF

       !Modify temperature derivatives to impose adiabatic BC dT/dn =0. The current version is ONLY valid for 2D geometry.
       IF (adiabaticBtm .AND. time_integration_method /= TIME_INT_Crank_Nicholson) THEN

          DO j = 1, j_lev
             i_p_face(0) = 1
             DO i=1,dim
                i_p_face(i) = i_p_face(i-1)*3
             END DO
             DO face_type = 0, 3**dim - 1
                face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
                IF( face(2) == -1 .AND. face(1) /= -1 ) THEN                !adiabatic bottom wall except the inflow points
                   DO wlt_type = MIN(j-1,1),2**dim-1
                      DO j_df = j, j_lev
                         DO k = 1, indx_DB(j_df,wlt_type,face_type,j)%length
                            i_bnd = indx_DB(j_df,wlt_type,face_type,j)%p(k)%i

                              normal_dir = 0.0_pr
                              tang_dir = 0.0_pr
                              tang_dir(1) = curvilinear_coordgrad(1,1,i_bnd)/sqrt(curvilinear_coordgrad(1,1,i_bnd)**2+curvilinear_coordgrad(2,1,i_bnd)**2) !normalized dx/dxi
                              tang_dir(2) = curvilinear_coordgrad(2,1,i_bnd)/sqrt(curvilinear_coordgrad(1,1,i_bnd)**2+curvilinear_coordgrad(2,1,i_bnd)**2) !normalized dy/dxi

                              normal_dir(1) = - tang_dir(2)
                              normal_dir(2) =   tang_dir(1)
                              du(dim+1,i_bnd,2) = -du(dim+1,i_bnd,1)*normal_dir(1)/SIGN(MAX(floor,ABS(normal_dir(2))), normal_dir(2)) !nx*dT/dx+ny*dT/dy=0

                         END DO
                      END DO
                   END DO
                END IF
             END DO
          END DO
       END IF
!------------------- End special treatments for inviscid top and adiabatic bottom BCs, not a generalized implementation. Needs improvement!------------------

       IF ( viscmodel .EQ. viscmodel_sutherland ) THEN
         !CALL print_extrema(v(:,dim+1),nwlt,'temperature check in RHS')
         props(:,1) = props(:,1) * calc_nondim_sutherland_visc( v(:,dim+1), nwlt, sutherland_const ) !(1.0_pr+sutherland_const)/(v(:,dim+1) +sutherland_const)*(v(:,dim+1))**(1.5_pr)
         props(:,2) = props(:,2) * calc_nondim_sutherland_visc( v(:,dim+1), nwlt, sutherland_const ) !(1.0_pr+sutherland_const)/(v(:,dim+1) +sutherland_const)*(v(:,dim+1))**(1.5_pr)
         !CALL print_extrema(props(:,1),nwlt,'sutherland visc check in RHS')
       END IF 
       p(:) = du(1,:,1) !reuse of a variable
       DO i = 2, dim
          p(:) = p(:) + du(i,:,i) !du_i/dx_i
       END DO
       IF (dervFlux==1) THEN
          CALL flxfunc (d2u(1:dim,:,1:dim), ng, j_lev)
       ELSE
          d2u(:,:,:) = 1.0_pr
       END IF
       DO j = 1, dim
          jshift=ne*(j-1)
          DO i = 1, dim
             F(:,n_mom(i)+jshift) = F(:,n_mom(i)+jshift) - ( props(:,1) + mu_turb(:) )*( du(i,:,j)+du(j,:,i) ) * d2u(i,:,j)  !rho*u_i*u_j - 2*mu*S_ij/Re  
          END DO
          F(:,n_mom(j)+jshift) = F(:,n_mom(j)+jshift) + 2.0_pr/3.0_pr*( props(:,1) + mu_turb(:) )*p(:) * d2u(j,:,j)  !rho*u_i*u_j - tau_ij/Re  [ tau_ij = 2*mu*( S_ij - 2/3*S_kk*delta_ij ) ] 
          DO i = 1, dim
             F(:,n_eng+jshift) =  F(:,n_eng+jshift) - props(:,1)*( du(i,:,j)+du(j,:,i) )*v(:,i) * d2u(i,:,j)  !(e+p)*u_j - u_i*2*mu*S_ij/Re 
          END DO
          F(:,n_eng+jshift) =  F(:,n_eng+jshift) + 2.0_pr/3.0_pr*props(:,1)*p(:)*v(:,j) * d2u(j,:,j) - F_compress * ( props(:,2) + kk_turb(:) )*du(dim+1,:,j) * d2u(2,:,j)  
                                                                                  !(e+p)*u_j - u_i*tau_ij/Re - q_j/Re/Pra [ q_j = mu dT/dx_j ]
          IF (Nspec>1) THEN
             DO l=1,Nspecm
                F(:,n_eng+jshift) =  F(:,n_eng+jshift) - u_integrated(:,n_den)*cp_in(l)*v(:,dim+1)*props(:,3)*du(dim+1+l,:,j) * d2u(2,:,j)
             END DO
             F(:,n_eng+jshift) =  F(:,n_eng+jshift) +  u_integrated(:,n_den)*cp_in(Nspec)*v(:,dim+1)*props(:,3)*SUM(du(dim+2:dim+Nspec,:,j),DIM=1) * d2u(2,:,j)
             DO l=1,Nspecm
               F(:,n_spc(l)+jshift) =  F(:,n_spc(l)+jshift) - u_integrated(:,n_den)*( props(:,3) + bD_turb(:) )*du(dim+1+l,:,j) * d2u(2,:,j)
             END DO
          END IF
       END DO
    END IF
  END IF
    IF (GRAV .AND. IMEXswitch .LE. 0) THEN
      shift = (n_eng-1)*ng   
      DO j=1,dim
         IF (Nspec>1) THEN
            DO l=1,Nspecm
               int_rhs(shift+1:shift+ng) = int_rhs(shift+1:shift+ng)-v(:,dim+1+l)*u_integrated(:,n_mom(j))*gr_in(l+Nspec*(j-1))
            END DO
            int_rhs(shift+1:shift+ng) = int_rhs(shift+1:shift+ng) - (1.0_pr-SUM(v(:,dim+2:dim+Nspec),DIM=2))*u_integrated(:,n_mom(j))*gr_in(Nspec*j)
         ELSE
            int_rhs(shift+1:shift+ng) = int_rhs(shift+1:shift+ng) - u_integrated(:,n_mom(j))*gr_in(Nspec*j)
         END IF
      END DO
    END IF

    tempintd=ne*dim
    CALL c_diff_fast(F(:,1:tempintd), du(1:tempintd,:,:), d2u(1:tempintd,:,:), j_lev, ng, meth, 10, tempintd, 1, tempintd)  !du(i,:,k)=dF_ij/dx_k

    DO j = 1, dim
       jshift=ne*(j-1)
       DO i = 1, dim+1+Nspec
          shift=(i-1)*ng
          int_rhs(shift+1:shift+ng) = int_rhs(shift+1:shift+ng) - du(i+jshift,:,j)  ! -dF_ij/dx_j
       END DO
       IF (GRAV .AND. IMEXswitch .LE. 0) THEN
          shift = (n_mom(j)-1)*ng
          IF (Nspec>1) THEN
             DO l=1,Nspecm
                int_rhs(shift+1:shift+ng) = int_rhs(shift+1:shift+ng) - gr_in(l+Nspec*(j-1))*u_integrated(:,n_spc(l))
             END DO
             int_rhs(shift+1:shift+ng) = int_rhs(shift+1:shift+ng) - gr_in(Nspec*j)*(u_integrated(:,n_den)-SUM(u_integrated(:,n_spc(1):n_spc(Nspecm)),DIM=2))
          ELSE
             int_rhs(shift+1:shift+ng) = int_rhs(shift+1:shift+ng) - gr_in(Nspec*j)*u_integrated(:,n_den)
          END IF
       END IF
    END DO
    ! ERIC: check for single species
    IF (NS .AND. dervFlux==2 .AND. IMEXswitch .GE. 0) THEN
       CALL c_diff_fast(v, du(1:dim+Nspec,:,:), d2u(1:dim+Nspec,:,:), j_lev, ng, meth, 10, dim+Nspec, 1,dim+Nspec) ! du_i/dx_j & dT/dx_j & dY_l/dx_j
       DO i = 2, dim
          F(:,i-1) = ( props(:,1) + mu_turb(:) )*( du(i,:,1)+du(1,:,i) )  !2*mu*S_ij/Re  
       END DO
       F(:,dim) = F_compress * ( props(:,2) + kk_turb(:) )*du(dim+1,:,1)  !q_j/Re/Pra [ q_j = mu dT/dx_j ]
       IF (Nspec>1) THEN
          DO l=1,Nspecm
             F(:,dim+l) =  u_integrated(:,n_den)*( props(:,3) + bD_turb(:) )*du(dim+1+l,:,1)
          END DO
          F(:,dim+Nspec) = -u_integrated(:,n_den)*( props(:,3) + bD_turb(:) )*SUM(du(dim+2:dim+Nspec,:,1),DIM=1)
          CALL c_diff_fast(F(:,1:dim+Nspec), du(1:dim+Nspec,:,:), d2u(1:dim+Nspec,:,:), j_lev, ng, meth, 10, dim+Nspec, 1, dim+Nspec)  !du(i,:,k)=dF_ij/dx_k
       ELSE
          CALL c_diff_fast(F(:,1:dim), du(1:dim,:,:), d2u(1:dim,:,:), j_lev, ng, meth, 10, dim, 1, dim)  !du(i,:,k)=dF_ij/dx_k ERIC: just different in the number of fields
       END IF
       CALL flxfunc (d2u(1:dim,:,1:dim), ng, j_lev)
       d2u(1:dim,:,1:dim) = 1.0_pr - d2u(1:dim,:,1:dim)
       DO i=2,dim
          shift=(n_mom(i)-1)*ng
          int_rhs(shift+1:shift+ng) = int_rhs(shift+1:shift+ng) - du(i-1,:,1)*d2u(2,:,1)
       END DO
       shift=(n_eng-1)*ng
       DO i=2,dim
          int_rhs(shift+1:shift+ng) = int_rhs(shift+1:shift+ng) - du(i-1,:,1)*v(:,i)*d2u(2,:,1)
       END DO
       int_rhs(shift+1:shift+ng) = int_rhs(shift+1:shift+ng) - du(dim,:,1)*d2u(2,:,1) 
       IF (Nspec>1) THEN
          DO l=1,Nspec
             int_rhs(shift+1:shift+ng) = int_rhs(shift+1:shift+ng) - v(:,dim+1)*cp_in(l)*du(dim+l,:,1)*d2u(2,:,1)
          END DO
          DO l=1,Nspecm
             shift=(n_spc(l)-1)*ng
             int_rhs(shift+1:shift+ng) = int_rhs(shift+1:shift+ng) - du(dim+l,:,1)*d2u(2,:,1) 
          END DO
       END IF
    END IF
  END SUBROUTINE navier_stokes_rhs

  SUBROUTINE navier_stokes_Drhs (int_Drhs, u_p, u_prev, meth )
    USE curvilinear 
    USE curvilinear_mesh 
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: meth
    REAL (pr), DIMENSION (ng,ne), INTENT(IN) :: u_p, u_prev
    REAL (pr), DIMENSION (ng*ne), INTENT(INOUT) :: int_Drhs

    INTEGER :: i, j, l, ie, shift, vshift, jshift, tempintd
    REAL (pr), DIMENSION (ne*dim,ng,dim) :: du, d2u
    REAL (pr), DIMENSION (ng,ne*dim) :: F
    REAL (pr), DIMENSION (ng,(dim+Nspec)*2) :: v_prev ! velocity components + temperature
    REAL (pr), DIMENSION (ng) :: p, p_prev
    REAL (pr), DIMENSION (ng,3) :: props, props_prev

    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc
    INTEGER :: wlt_type, k, j_df, imin, i_bnd, idim

    REAL (pr), DIMENSION(dim) :: normal_dir !inward unit normal vector at a certain points of bnd_coords
    REAL (pr), DIMENSION(dim) :: tang_dir   !unit tangential vector in flow direction at a certain points of bnd_coords

    REAL (pr) :: floor
        
    floor = 1.0e-12_pr

    int_Drhs = 0.0_pr
    vshift = dim+Nspec
    DO l=1,Nspecm
       v_prev(:,dim+1+l) = u_prev(:,n_spc(l))/u_prev(:,n_den)
       v_prev(:,vshift+dim+1+l) = u_p(:,n_spc(l))/u_prev(:,n_den) - u_prev(:,n_spc(l))*u_p(:,n_den)/u_prev(:,n_den)**2
    END DO
    p_prev(:) = 1.0_pr
    p(:) = 0.0_pr
    IF (Nspec>1) p_prev(:) = (1.0_pr-SUM(v_prev(:,dim+2:dim+Nspec),DIM=2))  !Y0_Nspec
    IF (Nspec>1) p(:)      =        -SUM(v_prev(:,vshift+dim+2:vshift+dim+Nspec),DIM=2)  !Y'_Nspec
    props_prev(:,1) = p_prev(:)*cp_in(Nspec) !cp0_Nspec
    props_prev(:,2) = p_prev(:)/MW_in(Nspec) !R0_Nspec
    props(:,1)      = p(:)*cp_in(Nspec) !cp'_Nspec
    props(:,2)      = p(:)*R_in(Nspec) !R'_Nspec
    !props(:,2)      = p(:)/MW_in(Nspec) !R'_Nspec
    DO l=1,Nspecm
       props_prev(:,1) = props_prev(:,1) + v_prev(:,dim+1+l)*cp_in(l) !cp0
       props_prev(:,2) = props_prev(:,2) + v_prev(:,dim+1+l)*R_in(l) !R0
       !props_prev(:,2) = props_prev(:,2) + v_prev(:,dim+1+l)/MW_in(l) !R0
       props(:,1)      = props(:,1)      + v_prev(:,vshift+dim+1+l)*cp_in(l) !cp'
       props(:,2)      = props(:,2)      + v_prev(:,vshift+dim+1+l)*R_in(l) !R'
       !props(:,2)      = props(:,2)      + v_prev(:,vshift+dim+1+l)/MW_in(l) !R'
    END DO
    p_prev(:) = (u_prev(:,n_eng)-0.5_pr*SUM(u_prev(:,n_mom(1):n_mom(dim))**2,DIM=2)/u_prev(:,n_den))/(props_prev(:,1)/props_prev(:,2)-1.0_pr) / F_compress  !pressure
    p(:) = ( (u_prev(:,n_eng)-0.5_pr*SUM(u_prev(:,n_mom(1):n_mom(dim))**2,DIM=2)/u_prev(:,n_den)) * (props_prev(:,1)*props(:,2)-props(:,1)*props_prev(:,2)) / (props_prev(:,1)-props_prev(:,2))**2 + &
           (u_p(:,n_eng)+0.5_pr*SUM(u_prev(:,n_mom(1):n_mom(dim))**2,DIM=2)/u_prev(:,n_den)**2*u_p(:,n_den)-SUM(u_prev(:,n_mom(1):n_mom(dim))*u_p(:,n_mom(1):n_mom(dim)),DIM=2)/u_prev(:,n_den)) /  &
             (props_prev(:,1)/props_prev(:,2)-1.0_pr) ) / F_compress !pressure_prime
    v_prev(:,dim+1) = p_prev(:)/props_prev(:,2)/u_prev(:,n_den)  !Temperature
    v_prev(:,vshift+dim+1) = (p(:)-p_prev(:)*(props(:,2)/props_prev(:,2)+u_p(:,n_den)/u_prev(:,n_den)))/props_prev(:,2)/u_prev(:,n_den)  !Temperature_prime
    DO i = 1, dim
       v_prev(:,i) = u_prev(:,n_mom(i)) / u_prev(:,n_den) ! u_i
       v_prev(:,vshift+i) = u_p(:,n_mom(i))/u_prev(:,n_den) - u_prev(:,n_mom(i))*u_p(:,n_den)/u_prev(:,n_den)**2  !u_i_prime
    END DO
!    IF (((methpd .ne. 0) .OR. splitFBpress) .AND. IMEXswitch .LE. 0 .AND. Nspec>1 ) THEN
!       du(1,:,1) = p(:)
!       du(1,:,2) = p_prev(:)
!       CALL dpdx (du(1,:,1),ng,v_prev(:,dim+2:dim+Nspec),2*methpd+meth,j_lev,splitFBpress)
!       shift = (n_mom(1)-1)*ng
!       int_Drhs(shift+1:shift+ng) = int_Drhs(shift+1:shift+ng)-du(1,:,1)
!       CALL dpdx (du(1,:,2),ng,v_prev(:,dim+2:dim+Nspec),2*methpd+meth,j_lev,splitFBpress)
!       shift = (n_eng-1)*ng
!       int_Drhs(shift+1:shift+ng) = int_Drhs(shift+1:shift+ng)-du(1,:,1)*v_prev(:,1)-du(1,:,2)*v_prev(:,vshift+1) !u0*dp'/dx+u'*dp0/dx
!       CALL c_diff_fast(v_prev(:,vshift+1), du(1,:,:), d2u(1,:,:), j_lev, ng, meth, 10, 1, 1, 1) ! du'/dx
!       int_Drhs(shift+1:shift+ng) = int_Drhs(shift+1:shift+ng)-du(1,:,1)*p_prev(:)   !p0*du'/dx
!       CALL c_diff_fast(v_prev(:,1), du(1,:,:), d2u(1,:,:), j_lev, ng, meth, 10, 1, 1, 1) ! du0/dx
!       int_Drhs(shift+1:shift+ng) = int_Drhs(shift+1:shift+ng)-du(1,:,1)*p(:)   !p'*du0/dx
!    END IF
    !
    !--Form right hand side of Euler Equations
    !
    F(:,:) = 0.0_pr
  IF (IMEXswitch .LE. 0) THEN
    DO j = 1, dim
       jshift=ne*(j-1)
       F(:,n_den+jshift) = F(:,n_den+jshift) + u_p(:,n_mom(j)) ! rho*u_j
       DO i = 1, dim ! (rho*u_i*u_j)'
          F(:,n_mom(i)+jshift) = F(:,n_mom(i)+jshift) + u_prev(:,n_mom(i))*v_prev(:,vshift+j) + u_p(:,n_mom(i))*v_prev(:,j)
       END DO
       F(:,n_mom(j)+jshift) = F(:,n_mom(j)+jshift) + F_compress * p(:) ! (rho*u_i^2)'+p'
       F(:,n_eng+jshift) = F(:,n_eng+jshift) + (u_p(:,n_eng) + F_compress * p(:))*v_prev(:,j) + (u_prev(:,n_eng) + F_compress * p_prev(:))*v_prev(:,vshift+j)  ! ((e+p)*u_j)'
       DO l=1,Nspecm
          F(:,n_spc(l)+jshift) = F(:,n_spc(l)+jshift) + v_prev(:,vshift+dim+1+l)*u_prev(:,n_mom(j))+v_prev(:,dim+1+l)*u_p(:,n_mom(j))
       END DO
    END DO
    IF (((methpd .ne. 0) .OR. splitFBpress)) THEN
       F(:,n_mom(1)) = F(:,n_mom(1)) - F_compress * p(:)
       F(:,n_eng)    = F(:,n_eng)    - F_compress * ( p_prev(:)*v_prev(:,vshift+1) - p(:)*v_prev(:,1) )
    END IF
  END IF
    !
    !-- Form right hand side of Navier-Stokes Equations
    !
    IF(NS .AND. IMEXswitch .GE. 0) THEN
       tempintd = 2*vshift
       CALL c_diff_fast(v_prev(:,:), du(1:tempintd,:,:), d2u(1:tempintd,:,:), j_lev, ng, meth, 10, tempintd, 1, tempintd) ! du'_i/dx_j & dT'/dx_j
       p_prev(:) = 1.0_pr
       p(:) = 0.0_pr
       IF (Nspec>1) p_prev(:) = (1.0_pr-SUM(v_prev(:,dim+2:dim+Nspec),DIM=2))  !Y0_Nspec
       IF (Nspec>1) p(:)      =        -SUM(v_prev(:,vshift+dim+2:vshift+dim+Nspec),DIM=2)  !Y'_Nspec
       props_prev(:,1) = p_prev(:)*mu_in(Nspec)  !mu0_Nspec
       props_prev(:,2) = p_prev(:)*kk_in(Nspec)  !kk0_Nspec
       props_prev(:,3) = p_prev(:)*bD_in(Nspec)  !bD0_Nspec
       props(:,1)      = p(:)*mu_in(Nspec)  !mu'_Nspec
       props(:,2)      = p(:)*kk_in(Nspec)  !kk'_Nspec
       props(:,3)      = p(:)*bD_in(Nspec)  !bD'_Nspec
       DO l=1,Nspecm
          props_prev(:,1) = props_prev(:,1) + v_prev(:,dim+1+l)*mu_in(l)  !mu0
          props_prev(:,2) = props_prev(:,2) + v_prev(:,dim+1+l)*kk_in(l)  !kk0
          props_prev(:,3) = props_prev(:,3) + v_prev(:,dim+1+l)*bD_in(l)  !bD0
          props(:,1)      = props(:,1)      + v_prev(:,vshift+dim+1+l)*mu_in(l)  !mu'
          props(:,2)      = props(:,2)      + v_prev(:,vshift+dim+1+l)*kk_in(l)  !kk'
          props(:,3)      = props(:,3)      + v_prev(:,vshift+dim+1+l)*bD_in(l)  !bD'
       END DO
       !IF ( use_comp_eddy_visc ) THEN
       !   props_prev(:,1) = props_prev(:,1) + mu_turb 
       !   props_prev(:,2) = props_prev(:,2) + kk_turb 
       !   IF(Nspec.GT.1) props_prev(:,3) = props_prev(:,3) + bD_turb 
       !   props(:,1) = props(:,1) + mu_turb 
       !   props(:,2) = props(:,2) + kk_turb 
       !   IF(Nspec.GT.1) props(:,3) = props(:,3) + bD_turb 
       !END IF
       IF (kinevisc .GT. 0) THEN
          CALL buffkine (p_prev, u_prev(:,n_den), ng)
          CALL buffkine (p,      u_p(:,n_den),    ng)
          props_prev(:,1) = props_prev(:,1)*p_prev(:)
          props_prev(:,2) = props_prev(:,2)*p_prev(:)
          props(:,1) = props(:,1)*p_prev(:) + props_prev(:,1)*p(:) 
          props(:,2) = props(:,2)*p_prev(:) + props_prev(:,2)*p(:)
       END IF

!Oleg 10.18.2018: This section is case dependent and needs to be moved out to the specific case. Kept for now.
!------------------- Begin special treatments for inviscid top and adiabatic bottom BCs, not a generalized implementation. Needs improvement!------------------
       IF (inviscidTop) THEN
          !Imposing inviscid condition at the top wall
          i_p_face(0) = 1
          DO i=1,dim
             i_p_face(i) = i_p_face(i-1)*3
          END DO
          DO face_type = 0, 3**dim - 1
             face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
             IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
                CALL get_all_indices_by_face (face_type, j_lev, nloc, iloc)
                IF(nloc > 0 ) THEN 
                  IF( face(2) == 1 ) THEN ! inviscid top wall
                         props_prev(iloc(1:nloc),1) = 0.0_pr
                         props_prev(iloc(1:nloc),2) = 0.0_pr
                  END IF
                END IF
             END IF
          END DO
       END IF

       !Modify temperature derivatives to impose adiabatic BC dT/dn =0. The current version is ONLY valid for 2D geometry.
       IF (adiabaticBtm .AND. time_integration_method /= TIME_INT_Crank_Nicholson) THEN

          DO j = 1, j_lev
             i_p_face(0) = 1
             DO i=1,dim
                i_p_face(i) = i_p_face(i-1)*3
             END DO
             DO face_type = 0, 3**dim - 1
                face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
                IF( face(2) == -1 .AND. face(1) /= -1 ) THEN                !adiabatic bottom wall except the inflow points
                   DO wlt_type = MIN(j-1,1),2**dim-1
                      DO j_df = j, j_lev
                         DO k = 1, indx_DB(j_df,wlt_type,face_type,j)%length
                            i_bnd = indx_DB(j_df,wlt_type,face_type,j)%p(k)%i

                              normal_dir = 0.0_pr
                              tang_dir = 0.0_pr
                              tang_dir(1) = curvilinear_coordgrad(1,1,i_bnd)/sqrt(curvilinear_coordgrad(1,1,i_bnd)**2+curvilinear_coordgrad(2,1,i_bnd)**2) !normalized dx/dxi
                              tang_dir(2) = curvilinear_coordgrad(2,1,i_bnd)/sqrt(curvilinear_coordgrad(1,1,i_bnd)**2+curvilinear_coordgrad(2,1,i_bnd)**2) !normalized dy/dxi

                              normal_dir(1) = - tang_dir(2)
                              normal_dir(2) =   tang_dir(1)
                              du(dim+1,i_bnd,2) = -du(dim+1,i_bnd,1)*normal_dir(1)/SIGN(MAX(floor,ABS(normal_dir(2))), normal_dir(2)) !nx*dT/dx+ny*dT/dy=0
                              du(vshift+dim+1,i_bnd,2) = -du(vshift+dim+1,i_bnd,1)*normal_dir(1)/SIGN(MAX(floor,ABS(normal_dir(2))), normal_dir(2)) !nx*dT'/dx+ny*dT'/dy=0

                         END DO
                      END DO
                   END DO
                END IF
             END DO
          END DO
       END IF
!------------------- End special treatments for inviscid top and adiabatic bottom BCs, not a generalized implementation. Needs improvement!------------------

       IF ( viscmodel .EQ. viscmodel_sutherland ) THEN
         !OLEG: Possible bug, it sould be: p'*S(T)+ p*(dS(T)/dT)T'
         props(:,1) = props(:,1)*calc_nondim_sutherland_visc( v_prev(:,dim+1), nwlt, sutherland_const ) &
                    + props_prev(:,1) * (1.5_pr*(1.0_pr+ sutherland_const )*v_prev(:,dim+1)**0.5_pr*v_prev(:,vshift+dim+1)/(v_prev(:,dim+1)+sutherland_const) &
                    + v_prev(:,dim+1)**1.5_pr*( -(1.0_pr+ sutherland_const )*v_prev(:,vshift+dim+1) )/(v_prev(:,dim+1)+sutherland_const)**2.0_pr)
         props(:,2) = props(:,2)*calc_nondim_sutherland_visc( v_prev(:,dim+1), nwlt, sutherland_const ) &
                    + props_prev(:,2) * (1.5_pr*(1.0_pr+ sutherland_const )*v_prev(:,dim+1)**0.5_pr*v_prev(:,vshift+dim+1)/(v_prev(:,dim+1)+sutherland_const) &
                    + v_prev(:,dim+1)**1.5_pr*( -(1.0_pr+ sutherland_const )*v_prev(:,vshift+dim+1) )/(v_prev(:,dim+1)+sutherland_const)**2.0_pr)

         !CALL print_extrema(v_prev(:,dim+1),nwlt,'temperature check in RHS')
         props_prev(:,1) = props_prev(:,1) * calc_nondim_sutherland_visc( v_prev(:,dim+1), nwlt, sutherland_const ) !(1.0_pr+sutherland_const)/(v_prev(:,dim+1) +sutherland_const)*(v_prev(:,dim+1))**(1.5_pr)
         props_prev(:,2) = props_prev(:,2) * calc_nondim_sutherland_visc( v_prev(:,dim+1), nwlt, sutherland_const ) !(1.0_pr+sutherland_const)/(v_prev(:,dim+1) +sutherland_const)*(v_prev(:,dim+1))**(1.5_pr)

!!$         props(:,1) = props(:,1) * (1.5_pr*(1.0_pr+ sutherland_const )*v_prev(:,dim+1)**0.5_pr*v_prev(:,vshift+dim+1))/(v_prev(:,dim+1)+sutherland_const) &
!!$              + v_prev(:,dim+1)**1.5_pr*( -(1.0_pr+ sutherland_const )*v_prev(:,vshift+dim+1) )/(v_prev(:,dim+1)+sutherland_const)**2.0_pr
!!$         props(:,2) = props(:,2) * (1.5_pr*(1.0_pr+ sutherland_const )*v_prev(:,dim+1)**0.5_pr*v_prev(:,vshift+dim+1))/(v_prev(:,dim+1)+sutherland_const) &
!!$              + v_prev(:,dim+1)**1.5_pr*( -(1.0_pr+ sutherland_const )*v_prev(:,vshift+dim+1) )/(v_prev(:,dim+1)+sutherland_const)**2.0_pr
       END IF 
       p(:) = du(vshift+1,:,1) !reuse of a variable
       DO i = 2, dim
          p(:) = p(:) + du(vshift+i,:,i) !du'_i/dx_i
       END DO
       IF (dervFlux==1) THEN
          CALL flxfunc (d2u(1:dim,:,1:dim), ng, j_lev)
       ELSE
          d2u(:,:,:) = 1.0_pr
       END IF
       DO j = 1, dim
          jshift=ne*(j-1)
          DO i = 1, dim
             F(:,n_mom(i)+jshift) = F(:,n_mom(i)+jshift) - (props_prev(:,1) + mu_turb)*( du(vshift+i,:,j)+du(vshift+j,:,i) ) * d2u(i,:,j)  !rho*u_i*u_j - 2*mu*S_ij/Re
          END DO
          F(:,n_mom(j)+jshift) = F(:,n_mom(j)+jshift) + 2.0_pr/3.0_pr*(props_prev(:,1)+mu_turb)*p(:) * d2u(j,:,j) !rho*u_i*u_j - tau_ij/Re  [ tau_ij = 2*mu*( S_ij - 2/3*S_kk*delta_ij ) ]
          DO i = 1, dim
             F(:,n_eng+jshift) =  F(:,n_eng+jshift) - props_prev(:,1)*( du(vshift+i,:,j)+du(vshift+j,:,i) )*v_prev(:,i) * d2u(i,:,j)  !(e+p)*u_j - u_i*2*mu*S_ij/Re
          END DO
          F(:,n_eng+jshift) =  F(:,n_eng+jshift) + 2.0_pr/3.0_pr*props_prev(:,1)*p(:)*v_prev(:,j) * d2u(j,:,j) - F_compress * (props_prev(:,2)+kk_turb)*du(vshift+dim+1,:,j) * d2u(2,:,j)  
                                                                                             !(e+p)*u_j - u_i*tau_ij/Re - q_j/Re/Pra [ q_j = mu dT/dx_j ]
          IF (Nspec>1) THEN
             DO l=1,Nspecm
                F(:,n_eng+jshift) =  F(:,n_eng+jshift) - u_prev(:,n_den)*cp_in(l)*v_prev(:,dim+1)*props_prev(:,3)*du(vshift+dim+1+l,:,j) * d2u(2,:,j)
             END DO
             F(:,n_eng+jshift) =  F(:,n_eng+jshift) +  u_prev(:,n_den)*cp_in(Nspec)*v_prev(:,dim+1)*(props_prev(:,3)+bD_turb)*SUM(du(vshift+dim+2:vshift+dim+Nspec,:,j),DIM=1) * d2u(2,:,j)
             DO l=1,Nspecm
                F(:,n_spc(l)+jshift) =  F(:,n_spc(l)+jshift) - u_prev(:,n_den)*(props_prev(:,3)+bD_turb)*du(vshift+dim+1+l,:,j) * d2u(2,:,j)
             END DO
          END IF
       END DO

       p_prev(:) = du(1,:,1) !reuse of a variable
       DO i = 2, dim
          p_prev(:) = p_prev(:) + du(i,:,i) !du0_i/dx_i
       END DO
       DO j = 1, dim
          jshift=ne*(j-1)
          DO i = 1, dim
             F(:,n_mom(i)+jshift) = F(:,n_mom(i)+jshift) - props(:,1)*( du(i,:,j)+du(j,:,i) ) * d2u(i,:,j)  !rho*u_i*u_j - 2*mu*S_ij/Re
          END DO
          F(:,n_mom(j)+jshift) = F(:,n_mom(j)+jshift) + 2.0_pr/3.0_pr*props(:,1)*p_prev(:) * d2u(j,:,j) !rho*u_i*u_j - tau_ij/Re  [ tau_ij = 2*mu*( S_ij - 1/3*S_kk*delta_ij ) ]
          DO i = 1, dim
             F(:,n_eng+jshift) =  F(:,n_eng+jshift) - ( du(i,:,j)+du(j,:,i) )*(v_prev(:,i)*props(:,1)+v_prev(:,vshift+i)*props_prev(:,1)) * d2u(i,:,j)  !(e+p)*u_j - u_i*2*mu*S_ij/Re
          END DO
          F(:,n_eng+jshift) =  F(:,n_eng+jshift) + 2.0_pr/3.0_pr*p_prev(:)*(v_prev(:,j)*props(:,1)+v_prev(:,vshift+j)*props_prev(:,1)) * d2u(j,:,j) - F_compress * props(:,2)*du(dim+1,:,j) * d2u(2,:,j)  
                                                                                             !(e+p)*u_j - u_i*tau_ij/Re - q_j/Re/Pra [ q_j = mu dT/dx_j ]
          IF (Nspec>1) THEN
             DO l=1,Nspecm
                F(:,n_eng+jshift) =  F(:,n_eng+jshift) - cp_in(l)*du(dim+1+l,:,j) * &
                                ( u_p(:,n_den)*v_prev(:,dim+1)*props_prev(:,3) + u_prev(:,n_den)*(v_prev(:,vshift+dim+1)*props_prev(:,3) + v_prev(:,dim+1)*props(:,3)) ) * d2u(2,:,j)
             END DO
             F(:,n_eng+jshift) =  F(:,n_eng+jshift) +  cp_in(Nspec)*SUM(du(dim+2:dim+Nspec,:,j),DIM=1) * &
                                ( u_p(:,n_den)*v_prev(:,dim+1)*props_prev(:,3) + u_prev(:,n_den)*(v_prev(:,vshift+dim+1)*props_prev(:,3) + v_prev(:,dim+1)*props(:,3)) ) * d2u(2,:,j)
             DO l=1,Nspecm
                F(:,n_spc(l)+jshift) =  F(:,n_spc(l)+jshift) - (u_p(:,n_den)*(props_prev(:,3)+bd_turb)+u_prev(:,n_den)*props(:,3))*du(dim+1+l,:,j) * d2u(2,:,j)
             END DO
          END IF
       END DO
    END IF
    IF (GRAV .AND. IMEXswitch .LE. 0) THEN
      shift = (n_eng-1)*ng
      DO j=1,dim
         IF (Nspec>1) THEN
            DO l=1,Nspecm
               int_Drhs(shift+1:shift+ng) = int_Drhs(shift+1:shift+ng)-(v_prev(:,dim+1+l)*u_p(:,n_mom(j))+v_prev(:,vshift+dim+1+l)*u_prev(:,n_mom(j)))*gr_in(l+Nspec*(j-1))
            END DO
            int_Drhs(shift+1:shift+ng) = int_Drhs(shift+1:shift+ng) - ((1.0_pr-SUM(v_prev(:,dim+2:dim+Nspec),DIM=2))*u_p(:,n_mom(j))-SUM(v_prev(:,vshift+dim+2:vshift+dim+Nspec),DIM=2)*u_prev(:,n_mom(j)) )*gr_in(Nspec*j)
         ELSE
            int_Drhs(shift+1:shift+ng) = int_Drhs(shift+1:shift+ng) - u_p(:,n_mom(j)) * gr_in(Nspec*j)
         END IF
      END DO
    END IF
    tempintd=ne*dim
    CALL c_diff_fast(F(:,1:tempintd), du(1:tempintd,:,:), d2u(1:tempintd,:,:), j_lev, ng, meth, 10, tempintd, 1, tempintd)  !du(i,:,k)=dF_ij/dx_k

    DO j = 1, dim
       jshift=ne*(j-1)
       DO i = 1, dim+1+Nspec
          shift=(i-1)*ng
          int_Drhs(shift+1:shift+ng) = int_Drhs(shift+1:shift+ng) - du(i+jshift,:,j)  ! -dF_ij/dx_j
       END DO
       IF (GRAV .AND. IMEXswitch .LE. 0) THEN
          shift = (n_mom(j)-1)*ng
          IF (Nspec>1) THEN
             DO l=1,Nspecm        
                int_Drhs(shift+1:shift+ng) = int_Drhs(shift+1:shift+ng) - gr_in(l+Nspec*(j-1))*u_p(:,n_spc(l))
             END DO
             int_Drhs(shift+1:shift+ng) = int_Drhs(shift+1:shift+ng) - gr_in(Nspec*j)*(u_p(:,n_den)-SUM(u_p(:,n_spc(1):n_spc(Nspecm)),DIM=2))
          ELSE
             int_Drhs(shift+1:shift+ng) = int_Drhs(shift+1:shift+ng) - gr_in(Nspec*j) * u_p(:,n_den)
          END IF
       END IF
    END DO
    IF (NS .AND. dervFlux==2 .AND. IMEXswitch .GE. 0) THEN
       tempintd = 2*vshift
       CALL c_diff_fast(v_prev(:,:), du(1:tempintd,:,:), d2u(1:tempintd,:,:), j_lev, ng, meth, 10, tempintd, 1, tempintd) ! du'_i/dx_j & dT'/dx_j
       DO i = 2, dim
          F(:,i-1) = (props_prev(:,1)+mu_turb)*( du(vshift+i,:,1)+du(vshift+1,:,i) )  !2*mu*S_ij/Re
       END DO
       F(:,dim) = F_compress * (props_prev(:,2)+kk_turb)*du(vshift+dim+1,:,1)  !q_j/Re/Pra [ q_j = mu dT/dx_j ]
       IF (Nspec>1) THEN
          DO l=1,Nspecm
             F(:,dim+l) =  u_prev(:,n_den)*(props_prev(:,3)+bd_turb)*du(vshift+dim+1+l,:,1)
          END DO
          F(:,dim+Nspec) =  -u_prev(:,n_den)*(props_prev(:,3)+bd_turb)*SUM(du(vshift+dim+2:vshift+dim+Nspec,:,1),DIM=1)
       END IF
       DO i = 2, dim
          F(:,i-1) = F(:,i-1) + props(:,1)*( du(i,:,1)+du(1,:,i) )  !2*mu*S_ij/Re
       END DO
       F(:,dim) = F(:,dim) + F_compress * props(:,2)*du(dim+1,:,1)  !q_j/Re/Pra [ q_j = mu dT/dx_j ]
       IF (Nspec>1) THEN
          DO l=1,Nspecm
             F(:,dim+l) = F(:,dim+l) + (u_prev(:,n_den)*props(:,3)+u_p(:,n_den)*(props_prev(:,3)+bD_turb))*du(dim+1+l,:,1)
          END DO
          F(:,dim+Nspec) =  F(:,dim+Nspec) - (u_prev(:,n_den)*props(:,3)+u_p(:,n_den)*(props_prev(:,3)+bD_turb))*SUM(du(dim+2:dim+Nspec,:,1),DIM=1)
          DO l=1,Nspecm
             F(:,dim+l+ne) = u_prev(:,n_den)*(props_prev(:,3)+bD_turb)*du(dim+1+l,:,1)
          END DO
          F(:,dim+Nspec+ne) = -u_prev(:,n_den)*(props_prev(:,3)+bD_turb)*SUM(du(dim+2:dim+Nspec,:,1),DIM=1)
       END IF
       DO i = 2, dim
          F(:,i-1+ne) = (props_prev(:,1)+mu_turb)*( du(i,:,1)+du(1,:,i) )  !2*mu*S_ij/Re
       END DO
       F(:,dim+ne) = F_compress * (props_prev(:,2)+kk_turb)*du(dim+1,:,1)  !q_j/Re/Pra [ q_j = mu dT/dx_j ]

       tempintd = 2*vshift
       CALL c_diff_fast(F(:,1:tempintd), du(1:tempintd,:,:), d2u(1:tempintd,:,:), j_lev, ng, meth, 10, tempintd, 1, tempintd)
       CALL flxfunc (d2u(1:dim,:,1:dim), ng, j_lev)
       d2u(1:dim,:,1:dim) = 1.0_pr - d2u(1:dim,:,1:dim)
       DO i=2,dim
          shift=(n_mom(i)-1)*ng
          int_Drhs(shift+1:shift+ng) = int_Drhs(shift+1:shift+ng) - du(i-1,:,1)*d2u(2,:,1)
       END DO
       shift=(n_eng-1)*ng
       DO i=2,dim
          int_Drhs(shift+1:shift+ng) = int_Drhs(shift+1:shift+ng) - du(i-1,:,1)*v_prev(:,i)*d2u(2,:,1)
       END DO
       int_Drhs(shift+1:shift+ng) = int_Drhs(shift+1:shift+ng) - du(dim,:,1)*d2u(2,:,1)
       IF (Nspec>1) THEN
          DO l=1,Nspec
             int_Drhs(shift+1:shift+ng) = int_Drhs(shift+1:shift+ng) - v_prev(:,dim+1)*cp_in(l)*du(dim+l,:,1)*d2u(2,:,1)
          END DO
          DO l=1,Nspecm
             shift=(n_spc(l)-1)*ng 
             int_Drhs(shift+1:shift+ng) = int_Drhs(shift+1:shift+ng) - du(dim+l,:,1)*d2u(2,:,1)
          END DO
       END IF

       shift=(n_eng-1)*ng
       DO i=2,dim
          int_Drhs(shift+1:shift+ng) = int_Drhs(shift+1:shift+ng) - du(i-1+ne,:,1)*v_prev(:,vshift+i)*d2u(2,:,1)
       END DO
       IF (Nspec>1) THEN
          DO l=1,Nspec
             int_Drhs(shift+1:shift+ng) = int_Drhs(shift+1:shift+ng) - v_prev(:,vshift+dim+1)*cp_in(l)*du(dim+l+ne,:,1)*d2u(2,:,1)
          END DO
       END IF
    END IF
  END SUBROUTINE navier_stokes_Drhs

  SUBROUTINE navier_stokes_Drhs_diag (int_diag,u_prev,meth)
    USE variable_mapping
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: meth
    REAL (pr), DIMENSION (ng*ne), INTENT(INOUT) :: int_diag
    REAL (pr), DIMENSION (ng,ne), INTENT(IN) :: u_prev

    INTEGER :: i, j, l, ll, ie, shift
    REAL (pr), DIMENSION (ng,dim) :: du_diag, d2u_diag
    REAL (pr), DIMENSION (dim+Nspec,ng,dim) :: du, d2u
    REAL (pr), DIMENSION (ng,n_integrated,dim) :: F
    REAL (pr), DIMENSION (ng, Nspec) :: v_prev
    REAL (pr), DIMENSION (ng,6) :: props_prev

    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc
    INTEGER :: wlt_type, k, j_df, imin, i_bnd

    int_diag = 0.0_pr
    DO l=1,Nspecm
       v_prev(:,l) = u_prev(:,n_spc(l))/u_prev(:,n_den)  ! Y0
    END DO
    v_prev(:,Nspec) = 1.0_pr-SUM(v_prev(:,1:Nspecm),DIM=2)  !Y0_Nspec
    props_prev(:,:)=0.0_pr
    DO l=1,Nspec
       props_prev(:,1) = props_prev(:,1) + v_prev(:,l)*cp_in(l)  !cp0
       props_prev(:,2) = props_prev(:,2) + v_prev(:,l)*R_in(l)  !R0
       !props_prev(:,2) = props_prev(:,2) + v_prev(:,l)/MW_in(l)  !R0
       props_prev(:,4) = props_prev(:,4) + v_prev(:,l)*mu_in(l)  !mu0
       props_prev(:,5) = props_prev(:,5) + v_prev(:,l)*kk_in(l)  !kk0
       props_prev(:,6) = props_prev(:,6) + v_prev(:,l)*bD_in(l)  !bD0
    END DO
    !IF ( use_comp_eddy_visc ) THEN
    !   props_prev(:,4) = props_prev(:,4) + mu_turb 
    !   props_prev(:,5) = props_prev(:,5) + kk_turb 
    !   IF(Nspec.GT.1) props_prev(:,6) = props_prev(:,6) + bD_turb 
    !END IF
    IF (kinevisc .GT. 0) THEN
       CALL buffkine (props_prev(:,3), u_prev(:,n_den), ng)
       props_prev(:,4) = props_prev(:,4)*props_prev(:,3)
       props_prev(:,5) = props_prev(:,5)*props_prev(:,3)
    END IF
    IF ( viscmodel .EQ. viscmodel_sutherland ) THEN ! ERIC: Only approximate, since it is slowly varying (consistent with DRHS)
       props_prev(:,3) = ( (u_prev(:,n_eng)-0.5_pr*SUM(u_prev(:,n_mom(1):n_mom(dim))**2,DIM=2)/u_prev(:,n_den))/( u_prev(:,n_den) * (props_prev(:,1) - props_prev(:,2)) ) / F_compress)   !temperature
      !CALL print_extrema(props_prev(:,3), nwlt, 'temperature check in NS_diag')       

       props_prev(:,4) = props_prev(:,4) * calc_nondim_sutherland_visc( props_prev(:,3), nwlt, sutherland_const ) !(1.0_pr+sutherland_const)/(v_prev(:,dim+1) +sutherland_const)*(v_prev(:,dim+1))**(1.5_pr)
       props_prev(:,5) = props_prev(:,5) * calc_nondim_sutherland_visc( props_prev(:,3), nwlt, sutherland_const ) !(1.0_pr+sutherland_const)/(v_prev(:,dim+1) +sutherland_const)*(v_prev(:,dim+1))**(1.5_pr)
    END IF 
    props_prev(:,3) = props_prev(:,1)/(props_prev(:,1)-props_prev(:,2)) !gamma0 = cp0/(cp0-R0)  ERIC: better to just average?
!    IF (((methpd .ne. 0) .OR. splitFBpress) .AND. IMEXswitch .LE. 0) THEN
!       CALL c_diff_fast(u_prev(:,n_mom(1))/u_prev(:,n_den), du(1,:,:), d2u(1,:,:), j_lev, ng, meth, 10, 1, 1, 1) ! du0/dx
!       CALL dpdx_diag (du_diag(:,1),ng,v_prev(:,1:Nspecm),2*methpd+meth,j_lev,splitFBpress)
!       shift = (n_mom(1)-1)*ng
!       int_diag(shift+1:shift+ng) = int_diag(shift+1:shift+ng) + du_diag(:,1)*u_prev(:,n_mom(1))/u_prev(:,n_den)*(props_prev(:,3)-1.0_pr)  !pd_rhou*diagFB_x
!       shift = (n_eng-1)*ng
!       int_diag(shift+1:shift+ng) = int_diag(shift+1:shift+ng) - (du(1,:,1) + du_diag(:,1)*u_prev(:,n_mom(1))/u_prev(:,n_den))*(props_prev(:,3)-1.0_pr) !pd_rhoe*(du0/dx+u0*diagFB_x)
!    END IF
    CALL c_diff_diag(du_diag, d2u_diag, j_lev, ng, meth, meth, -11)
    !
    !--Form right hand side of Euler Equations
    !
    F(:,:,:) = 0.0_pr
  IF (IMEXswitch .LE. 0) THEN
    DO j = 1, dim 
       ! (rho*u_i*u_j)' = (rho*u_i)'* (rho*u_j)/rho + (rho*u_i)*(rho*u_j)'/rho - rho'*u_i*u_j
       F(:,n_mom(j),j) = F(:,n_mom(j),j) + u_prev(:,n_mom(j))/u_prev(:,n_den) ! (rho*u_j)/rho* { (rho*u_j)' }
       DO i = 1, dim 
          F(:,n_mom(i),j) = F(:,n_mom(i),j) + u_prev(:,n_mom(j))/u_prev(:,n_den) ! { (rho*u_i)' } * (rho*u_j)/rho
       END DO
       !Oleg - correction (no F_compress):
       F(:,n_mom(j),j) = F(:,n_mom(j),j) + u_prev(:,n_mom(j))/u_prev(:,n_den)*(1.0_pr-props_prev(:,3))
       F(:,n_eng,j) = F(:,n_eng,j) + props_prev(:,3)*u_prev(:,n_mom(j))/u_prev(:,n_den)
       DO l=1,Nspecm
          F(:,n_spc(l),j) = F(:,n_spc(l),j) + u_prev(:,n_mom(j))/u_prev(:,n_den)
       END DO
    END DO
    IF (((methpd .ne. 0) .OR. splitFBpress)) THEN
       !Oleg - correction (no F_compress):
       F(:,n_mom(1),1) = F(:,n_mom(1),1) + u_prev(:,n_mom(j))/u_prev(:,n_den)*(props_prev(:,3)-1.0_pr)
       F(:,n_eng,1) = F(:,n_eng,1) - (props_prev(:,3)-1.0_pr)*u_prev(:,n_mom(1))/u_prev(:,n_den)
    END IF
  END IF
    !
    !-- Form right hand side of Navier-Stokes Equations
    !
    IF(NS .AND. NSdiag .AND. IMEXswitch .GE. 0) THEN

!Oleg 10.18.2018: This section is case dependent and needs to be moved out to the specific case. Kept for now.
!------------------- Begin special treatments for inviscid top, not a generalized implementation. Needs improvement!------------------
       IF (inviscidTop) THEN
          !Imposing inviscid condition at the top wall
          i_p_face(0) = 1
          DO i=1,dim
             i_p_face(i) = i_p_face(i-1)*3
          END DO
          DO face_type = 0, 3**dim - 1
             face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
             IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
                CALL get_all_indices_by_face (face_type, j_lev, nloc, iloc)
                IF(nloc > 0 ) THEN 
                  IF( face(2) == 1 ) THEN ! inviscid top wall
                         props_prev(iloc(1:nloc),4) = 0.0_pr
                         props_prev(iloc(1:nloc),5) = 0.0_pr
                  END IF
                END IF
             END IF
          END DO
       END IF
!------------------- End special treatments for inviscid top, not a generalized implementation. Needs improvement!------------------

       CALL c_diff_fast(v_prev, du(1:Nspec,:,:), d2u(1:Nspec,:,:), j_lev, ng, meth, 10, Nspec, 1, Nspec) ! dY_l/dx_j
       IF (dervFlux==1) THEN
          CALL flxfunc (d2u(1:dim,:,1:dim), ng, j_lev)
       ELSE
          d2u(:,:,:) = 1.0_pr
       END IF
       DO j = 1, dim
          DO l=1,Nspec
             F(:,n_eng,j) =  F(:,n_eng,j) - props_prev(:,6)*du(l,:,j)*cp_in(l)/(props_prev(:,1)-props_prev(:,2))*d2u(2,:,j)
          END DO 
       END DO
       DO j = 1, dim
          DO i = 1, dim
             shift=(n_mom(i)-1)*ng
             int_diag(shift+1:shift+ng) = int_diag(shift+1:shift+ng) + (props_prev(:,4)+mu_turb(:))/u_prev(:,n_den)*d2u_diag(:,j) * d2u(i,:,j)
          END DO
          shift=(n_mom(j)-1)*ng
          int_diag(shift+1:shift+ng) = int_diag(shift+1:shift+ng) + (props_prev(:,4)+mu_turb)/u_prev(:,n_den)*d2u_diag(:,j)/3.0_pr * d2u(j,:,j)
          shift=(n_eng-1)*ng
          int_diag(shift+1:shift+ng) = int_diag(shift+1:shift+ng) + (props_prev(:,5)+kk_turb)/u_prev(:,n_den)/(props_prev(:,1)-props_prev(:,2))*d2u_diag(:,j) * d2u(2,:,j)
          DO l = 1, Nspecm
             shift=(n_spc(l)-1)*ng
             int_diag(shift+1:shift+ng) = int_diag(shift+1:shift+ng) + ((props_prev(:,6)+bD_turb)*d2u_diag(:,j)+bD_in(l)*du(l,:,j)) * d2u(2,:,j)
          END DO
       END DO
    END IF
    DO j = 1, dim
       DO i = 1, dim+1+Nspec
          shift=(i-1)*ng
          int_diag(shift+1:shift+ng) = int_diag(shift+1:shift+ng) - F(:,i,j)*du_diag(:,j)
       END DO
    END DO

    IF (NS .AND. NSdiag .AND. dervFlux==2 .AND. IMEXswitch .GE. 0) THEN
       CALL flxfunc (d2u(1:dim,:,1:dim), ng, j_lev)
       d2u(1:dim,:,1:dim) = 1.0_pr - d2u(1:dim,:,1:dim)
       DO i = 2, dim
          shift=(n_mom(i)-1)*ng
          int_diag(shift+1:shift+ng) = int_diag(shift+1:shift+ng) - (props_prev(:,4)+mu_turb)/u_prev(:,n_den)*d2u_diag(:,1) * d2u(2,:,1)
       END DO
       IF (Nspec>1) THEN
          DO l = 1, Nspecm
             shift=(n_spc(l)-1)*ng
             int_diag(shift+1:shift+ng) = int_diag(shift+1:shift+ng) - ((props_prev(:,6)+bD_turb)*d2u_diag(:,1)+bD_in(l)*du(l,:,1)) * d2u(2,:,1)
          END DO
       END IF
       shift=(n_eng-1)*ng
       !Xuan - No F_compress
       !int_diag(shift+1:shift+ng) = int_diag(shift+1:shift+ng) - F_compress * (props_prev(:,5)+kk_turb)/u_prev(:,n_den)/(props_prev(:,1)-props_prev(:,2))*d2u_diag(:,1) * d2u(2,:,1)
       int_diag(shift+1:shift+ng) = int_diag(shift+1:shift+ng) - (props_prev(:,5)+kk_turb)/u_prev(:,n_den)/(props_prev(:,1)-props_prev(:,2))*d2u_diag(:,1) * d2u(2,:,1)
       IF (Nspec>1) THEN
          DO l = 1, Nspec
             v_prev(:,l) = du(l,:,1)*(props_prev(:,6)+bD_turb)*u_prev(:,n_den)
          END DO
          CALL c_diff_fast(v_prev, du(1:Nspec,:,:), d2u(1:Nspec,:,:), j_lev, ng, meth, 10, Nspec, 1, Nspec) ! d/dx_j(rho0*D0*dY_l/dx)
          DO l = 1, Nspec
             int_diag(shift+1:shift+ng) = int_diag(shift+1:shift+ng) - cp_in(l)/u_prev(:,n_den)/(props_prev(:,1)-props_prev(:,2))*du(l,:,1) * d2u(2,:,1)
          END DO
       END IF
    END IF
  END SUBROUTINE navier_stokes_Drhs_diag

  SUBROUTINE compressible_rhs (comp_rhs, u_integrated, meth)
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: meth
    REAL (pr), DIMENSION (ng,ne), INTENT(IN)  :: u_integrated !1D flat version of integrated variables without BC 
    REAL (pr), DIMENSION (ng*ne), INTENT(INOUT) :: comp_rhs

       CALL navier_stokes_rhs(comp_rhs,u_integrated, meth )

  END SUBROUTINE compressible_rhs

  SUBROUTINE compressible_Drhs (comp_drhs, u_p, u_prev, meth)
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: meth
    REAL (pr), DIMENSION (ng,ne) :: u_p, u_prev
    REAL (pr), DIMENSION (ng*ne), INTENT(INOUT) :: comp_Drhs

      CALL navier_stokes_Drhs(comp_Drhs,u_p,u_prev,meth )

  END SUBROUTINE compressible_Drhs

  SUBROUTINE compressible_Drhs_diag (comp_drhs_diag, u_prev, meth)
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: meth
    REAL (pr), DIMENSION (ng*ne), INTENT(INOUT) :: comp_Drhs_diag
    REAL (pr), DIMENSION (ng,ne), INTENT(IN) :: u_prev
    INTEGER :: ie, shift

       CALL navier_stokes_Drhs_diag(comp_Drhs_diag,u_prev,meth )

  END SUBROUTINE compressible_Drhs_diag


  !
  ! calculate any additional variables
  ! 
  ! arg 
  ! flag  - 0 calledwhile adapting to IC, 1 - called in main time integration loop
  !
  ! These additiona variables are calculated and left in real space.
  !
  SUBROUTINE compressible_additional_vars( t_local, flag )
    USE field
    IMPLICIT NONE

    REAL (pr), INTENT (IN) ::  t_local
    INTEGER , INTENT(IN) :: flag ! 0- called during adaption to IC, 1 called during main integration loop
    REAL (pr) :: cp, R, mygr, cmax, uval
    INTEGER :: l, i, j
    REAL (pr), DIMENSION(1,nwlt,MAX(dim,Nspecm)) :: du, d2u    
    REAL (pr) :: iterdiff, initdiff, curtau, fofn, deln, delnold, delfofn
    INTEGER, PARAMETER :: meth=1
    CHARACTER(LEN=8):: numstrng
    
    LOGICAL, SAVE :: first_call = .TRUE.

    d2u(1,:,2) = cp_in(Nspec) 
    d2u(1,:,1) = R_in(Nspec)   ! ERIC: removed 1/Ma**2 factor
    !d2u(1,:,1) = 1.0_pr/MW_in(Nspec)   ! ERIC: removed 1/Ma**2 factor
    IF (Nspec>1) THEN
       d2u(1,:,2) = (1.0_pr - SUM(u(:,n_spc(1):n_spc(Nspecm)),DIM=2)/u(:,n_den))*cp_in(Nspec)
       d2u(1,:,1) = (1.0_pr - SUM(u(:,n_spc(1):n_spc(Nspecm)),DIM=2)/u(:,n_den))*R_in(Nspec)
       !d2u(1,:,1) = (1.0_pr - SUM(u(:,n_spc(1):n_spc(Nspecm)),DIM=2)/u(:,n_den))/MW_in(Nspec)
       DO l=1,Nspecm
          d2u(1,:,2) = d2u(1,:,2) + u(:,n_spc(l))/u(:,n_den)*cp_in(l) !cp
          d2u(1,:,1) = d2u(1,:,1) + u(:,n_spc(l))/u(:,n_den)*R_in(l) !R
          !d2u(1,:,1) = d2u(1,:,1) + u(:,n_spc(l))/u(:,n_den)/MW_in(l) !R
       END DO
    END IF
    u(:,n_prs) = (u(:,n_eng)-0.5_pr*SUM(u(:,n_mom(1):n_mom(dim))**2,DIM=2)/u(:,n_den))/(d2u(1,:,2)/d2u(1,:,1)-1.0_pr) / F_compress ! p

    IF (adapttmp .GT. 0 .OR. savetmp) u(:,n_temp) = u(:,n_prs)/u(:,n_den)/d2u(1,:,1)

    IF (adaptmom  .GT. 0 .AND.  adaptvelonly ) THEN
       DO i=1,dim
          u(:,n_vel(i)) = u(:,n_mom(i))/u(:,n_den)
       END DO
    END IF
    IF (adapteng  .GT. 0 .AND.  adaptengonly ) THEN
       u(:,n_eon) = u(:,n_eng)/u(:,n_den)
    END IF
    IF (adaptspc  .GT. 0 .AND. adaptspconly .AND. Nspec>1 ) THEN
       DO i=1,Nspecm
          u(:,n_son(i)) = u(:,n_spc(i))/u(:,n_den)
       END DO
    END IF
    IF ( adaptbuoy .GT. 0 .OR. savebuoys ) THEN
       !u(:,n_dpdx) = u(:,n_prs)
       u(:,n_buoy) = u(:,n_prs)  ! ERIC: dummy var, since pulling the calcs below
!       IF (Nspec>1) THEN
!          DO i=1,Nspecm
!             u(:,n_spc(i)) = u(:,n_spc(i))/u(:,n_den)
!          END DO
!          CALL dpdx (u(:,n_dpdx),nwlt,u(:,n_spc(1):n_spc(Nspecm)),2*methpd+meth,j_lev,splitFBpress)
!          DO i=1,Nspecm
!             u(:,n_spc(i)) = u(:,n_spc(i))*u(:,n_den)
!          END DO
!       ELSE
!          CALL dpdx (u(:,n_dpdx),nwlt,u(:,1),2*methpd+meth,j_lev,.FALSE.)
!       END IF
!       IF (Nspec>1) THEN
!          DO i=1,nwlt
!             mygr=gr_in(Nspec)*(1.0_pr-SUM(u(i,n_spc(1):n_spc(Nspecm)))   /u(i,n_den)   ) 
!             DO j=1,Nspecm
!                mygr=mygr+gr_in(j)*u(i,n_spc(j))   /u(i,n_den)   
!             END DO
!             u(i,n_buoy) =  u(i,n_dpdx) + mygr*u(i,n_den)
!          END DO
!       ELSE
!          DO i=1,nwlt
!             mygr=gr_in(Nspec)
!             u(i,n_buoy) =  u(i,n_dpdx) + mygr*u(i,n_den)
!          END DO
!       END IF
!       IF (par_rank.EQ.0 .AND. verb_level>0) PRINT *, 'MAXABS of dp/dx+rho*g:', MAXVAL(ABS(u(:,n_buoy)))
    END IF
    IF( adaptMagVort .GT. 0  .OR. saveMagVort &
        .OR. adaptComVort .GT. 0  .OR. saveComVort &
        .OR. adaptMagVel .GT. 0  .OR. saveMagVel &
        .OR. adaptNormS .GT. 0  .OR. saveNormS &
        .OR. saveQCrit &
        .OR. adaptDivU .GT. 0  .OR. saveDivU ) THEN
       DO i=1,dim
          du(1,:,i) = u(:,n_mom(i))/u(:,n_den)
       END DO
       CALL bonusvars_vel(du(1,:,1:dim))  !4extra - call it
    END IF
    IF( ( adaptGradY .GT. 0  .OR. saveGradY ) .AND. Nspec>1 ) THEN
       DO i=1,Nspecm
          du(1,:,i) = u(:,n_spc(i))/u(:,n_den)
       END DO
       CALL bonusvars_spc(du(1,:,1:Nspecm))  !4extra - call it
    END IF
    IF (bonusCFL) THEN   !--Updating cfl
       CFLvort = MAXVAL(u(:,n_mvort))*dt
       CFLmods = MAXVAL(u(:,n_mods))*dt
       CALL parallel_global_sum(REALMAXVAL=CFLvort)
       CALL parallel_global_sum(REALMAXVAL=CFLmods)
       cfl = MAX(cfl,CFLvort,CFLmods)
       IF (cfl>cflmax) THEN !--Limit time step
          IF (par_rank.EQ.0) PRINT *,'WARNING: dt being limited by cfl>cflmax: dt, dt_new',dt , MIN(dt * cflmax/cfl,dt_original)
          dt = MIN(dtmax,dt * cflmax/cfl,dt_original)
          cfl = cflmax
          dt_changed = .TRUE.
       ELSE IF( dt_changed .AND. t-t_begin<=t_adapt) THEN
          dt = MIN(dt, dt_original, dt * cflmax/cfl)
          !IF (par_rank.EQ.0) PRINT *, 'dt is increased toward original: dt=',dt
          IF (it>0) dt_changed = .FALSE.
          IF( dt < dt_original) dt_changed = .TRUE.
       END IF
    END IF


    first_call = .FALSE.
  END SUBROUTINE compressible_additional_vars

  SUBROUTINE bonusvars_vel(velvec)   !4extra - this whole subroutine
    USE field
    IMPLICIT NONE
    REAL (pr), INTENT(IN) :: velvec(nwlt,dim) !velocity
    REAL (pr) :: tmp(nwlt,2*dim) !tmp for vorticity
    REAL (pr), DIMENSION(dim,nwlt,dim) :: du, d2u
    REAL (pr), DIMENSION(nwlt) :: Smod
    INTEGER :: i

    IF( adaptMagVort .GT. 0  .OR. saveMagVort &
        .OR. adaptComVort .GT. 0  .OR. saveComVort &
        .OR. adaptMagVel .GT. 0  .OR. saveMagVel &
        .OR. adaptNormS .GT. 0 .OR. saveNormS &
        .OR. saveQCrit &
        .OR. adaptDivU .GT. 0  .OR. saveDivU ) THEN
       tmp = 0.0_pr
       ! Calculate the magnitude vorticity
       IF( adaptMagVort .GT. 0  .OR. saveMagVort .OR. adaptComVort .GT. 0  .OR.  saveComVort ) THEN
          CALL cal_vort (velvec, tmp(:,1:3-MOD(dim,3)), nwlt)
          IF ( adaptMagVort .GT. 0  .OR. saveMagVort )  u(:,n_mvort) = SQRT(tmp(:,1)**2.0_pr + tmp(:,2)**2.0_pr + tmp(:,3)**2.0_pr )
          IF ( adaptComVort .GT. 0  .OR. saveComVort )  THEN
             DO i=1,3
                u(:,n_cvort(4-i)) = tmp(:,4-i)
             END DO
          END IF
       END IF
       ! Calculate the magnitude of Sij (sqrt(SijSij) )
       ! s(:,1) = s_11
       ! s(:,2) = s_22
       ! s(:,3) = s_33
       ! s(:,4) = s_12
       ! s(:,5) = s_13
       ! s(:,6) = s_23
       tmp = 0.0_pr
       IF( adaptNormS .GT. 0  .OR. saveNormS) THEN
          CALL Sij( velvec ,nwlt, tmp(:,1:2*dim), u(:,n_mods), .TRUE. )
       END IF
       ! Calculate the magnitude of velocity
       IF( adaptMagVel .GT. 0  .OR. saveMagVel) THEN
          u(:,n_mvel) = 0.5_pr*(SUM(velvec(:,:)**2.0_pr,DIM=2))
       END IF
       ! Calculate the Q-criterion 
       IF( saveQCrit ) THEN
          CALL cal_vort (velvec, tmp(:,1:3-MOD(dim,3)), nwlt)
          u(:,n_qcrit) = SQRT(tmp(:,1)**2.0_pr + tmp(:,2)**2.0_pr + tmp(:,3)**2.0_pr )
          CALL Sij( velvec ,nwlt, tmp(:,1:2*dim), Smod, .TRUE. )
          u(:,n_qcrit) = 0.5_pr * ( u(:,n_qcrit) * u(:,n_qcrit) - Smod * Smod )
       END IF
       ! Calculate the Divergence of U 
       IF( adaptDivU .GT. 0  .OR. saveDivU) THEN
          CALL c_diff_fast(velvec(:,1:dim), du(1:dim,:,:), d2u(1:dim,:,:), j_lev, nwlt,HIGH_ORDER, 10, dim, 1, dim)  !du/dx
          u(:,n_divu) = du(1,:,1) 
          DO i = 2,dim
            u(:,n_divu) = du(i,:,i) 
          END DO
       END IF
    END IF
  END SUBROUTINE bonusvars_vel

  SUBROUTINE bonusvars_spc(spcvec)   !4extra - this whole subroutine
    USE field
    IMPLICIT NONE
    REAL (pr), INTENT(IN) :: spcvec(nwlt,Nspecm) !species
    REAL (pr), DIMENSION(Nspecm,nwlt,dim) :: du, d2u    
    INTEGER :: i
    INTEGER, PARAMETER :: meth=1

    IF( adaptGradY .GT. 0  .OR. saveGradY) THEN
       CALL c_diff_fast(spcvec(:,1:Nspecm), du(1:Nspecm,:,:), d2u(1:Nspecm,:,:), j_lev, nwlt,meth, 10, Nspecm, 1, Nspecm)  !dY/dx
       DO i=1,Nspecm
          u(:,n_gdy(i)) = SUM(du(i,:,:)**2,DIM=2) !|dY/dx_i*dY/dx_i|
       END DO
    END IF
  END SUBROUTINE bonusvars_spc

  !
  !************ Calculating Scales ***************************
  !
  ! Note the order of the components in the scl array
  ! correspond to u_tn, v_tn, w_tn, u_tn-1, v_tn-1, w_tn-1, u_tn-2, v_tn-2, w_tn-2
  ! 
  SUBROUTINE compressible_scales(flag,use_default, u_loc, nlocal, ne_local, l_n_var_adapt , l_n_var_adapt_index, &
       scl, scl_fltwt) !add
    USE variable_mapping
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: flag ! 0 during initial adaptation, then 1 during time advancement
    LOGICAL , INTENT(INOUT) :: use_default
    INTEGER, INTENT (IN) :: nlocal, ne_local
    REAL (pr), DIMENSION (1:nlocal,1:ne_local), INTENT (IN) :: u_loc
    LOGICAL , INTENT (IN) :: l_n_var_adapt(ne_local)
    INTEGER , INTENT (IN) :: l_n_var_adapt_index(1:ne_local)
    REAL (pr), DIMENSION (1:ne_local), INTENT (INOUT) :: scl
    REAL (pr), DIMENSION (1:ne_local) :: mean
    REAL (pr) ,  INTENT(IN) ::scl_fltwt !weight for temporal filter on scl
    REAL (pr) :: floor, tmp,tmpmax,tmpmin,tmpsum
    INTEGER :: i, ie, ie_index, tmpint

    !
    ! Ignore the output of this routine and use default scales routine
    !
    use_default = .FALSE. 

    IF( .NOT. use_default ) THEN
       scl   =1.0_pr
       !
       ! Calculate scl per component
       !
       mean = 0.0_pr
       DO ie=1,ne_local
          !IF(l_n_var_adapt(ie)) THEN
          !   ie_index = l_n_var_adapt_index(ie)
          IF( Scale_Meth == 1 ) THEN ! Use Linf scale
             !tmpmax=MAXVAL ( u_loc(1:nlocal,ie_index) )
             !tmpmin=MINVAL ( u_loc(1:nlocal,ie_index) )
             tmpmax=MAXVAL ( u_loc(1:nlocal,ie) )
             tmpmin=MINVAL ( u_loc(1:nlocal,ie) )
             CALL parallel_global_sum(REALMAXVAL=tmpmax)
             CALL parallel_global_sum(REALMINVAL=tmpmin)
             mean(ie)= 0.5_pr*(tmpmax+tmpmin)
          ELSE IF ( Scale_Meth == 2 ) THEN ! Use L2 scale
             !tmpsum=SUM( u_loc(1:nlocal,ie_index) )
             tmpsum=SUM( u_loc(1:nlocal,ie) )
             tmpint=nlocal
             CALL parallel_global_sum(REAL=tmpsum)
             CALL parallel_global_sum(INTEGER=tmpint)
             mean(ie)= tmpsum/REAL(tmpint,pr)
          ELSE IF ( Scale_Meth == 3 ) THEN ! Use L2 scale using dA
             !tmpsum=SUM( u_loc(1:nlocal,ie_index)*dA )
             tmpsum=SUM( u_loc(1:nlocal,ie)*dA )
             CALL parallel_global_sum(REAL=tmpsum)
             mean(ie)= tmpsum/ sumdA_global 
          END IF
          !END IF
       END DO
       DO ie=1,ne_local
          IF( Scale_Meth == 1 ) THEN ! Use Linf scale
             !scl(ie)= MAXVAL ( ABS( u_loc(1:nlocal,ie_index) - mean(ie) ) )
             scl(ie)= MAXVAL ( ABS( u_loc(1:nlocal,ie) - mean(ie) ) )
             CALL parallel_global_sum( REALMAXVAL=scl(ie) )
          ELSE IF ( Scale_Meth == 2 ) THEN ! Use L2 scale
             !scl(ie)= SUM( (u_loc(1:nlocal,ie_index)-mean(ie))**2 )
             !Xuan - 
             !scl(ie)= SUM( (u_loc(1:nlocal,ie)-mean(ie))**2 )
             scl(ie)= SUM( (u_loc(1:nlocal,ie))**2 )
             !Xuan end
             tmpint=nlocal
             CALL parallel_global_sum( REAL=scl(ie) )
             CALL parallel_global_sum(INTEGER=tmpint)
             scl(ie)=SQRT(scl(ie)/REAL(tmpint,pr))
          ELSE IF ( Scale_Meth == 3 ) THEN ! Use L2 scale using dA
             !scl(ie)= SUM( (u_loc(1:nlocal,ie_index)-mean(ie))**2*dA )
             !Xuan -
             !scl(ie)= SUM( (u_loc(1:nlocal,ie)-mean(ie))**2*dA )
             scl(ie)= SUM( (u_loc(1:nlocal,ie))**2*dA )
             !Xuan end
             CALL parallel_global_sum(REAL=scl(ie))
             scl(ie)=SQRT(scl(ie)/sumdA_global)
          ELSE
             PRINT *, 'Error in Scales(), Unknown Scale_Meth type: ', Scale_Meth
             PRINT *, 'Known Scale_Meth types: 1 = Linf scale, 2 = L2 scale '
             PRINT *, 'Exiting ...'
             stop
          END IF
          !END IF
       END DO


       !
       ! take appropriate vector norm over scl(1:dim)
       ! the vector norm method defaults to global Scale_Meth, but can be
       ! overridden by an optional input parameter: vel_vect_scale
       IF (adaptvelonly) THEN
          IF( vel_vect_scale == 1 ) THEN ! Use Linf scale
             scl(n_vel(1):n_vel(dim)) = MAXVAL(scl(n_vel(1):n_vel(dim)))
          ELSE IF ( vel_vect_scale == 2 ) THEN ! Use L2 scale
             scl(n_vel(1):n_vel(dim)) = SQRT(SUM(scl(n_vel(1):n_vel(dim))**2))
          ELSE IF ( vel_vect_scale == 3 ) THEN ! Use L2 scale using dA
             scl(n_vel(1):n_vel(dim)) = SQRT(SUM(scl(n_vel(1):n_vel(dim))**2))
          END IF
       END IF
       IF( vel_vect_scale == 1 ) THEN ! Use Linf scale
          scl(n_mom(1):n_mom(dim)) = MAXVAL(scl(n_mom(1):n_mom(dim)))
       ELSE IF ( vel_vect_scale == 2 ) THEN ! Use L2 scale
          scl(n_mom(1):n_mom(dim)) = SQRT(SUM(scl(n_mom(1):n_mom(dim))**2)) !now velocity vector length, rather than individual component
       ELSE IF ( vel_vect_scale == 3 ) THEN ! Use L2 scale using dA
          scl(n_mom(1):n_mom(dim)) = SQRT(SUM(scl(n_mom(1):n_mom(dim))**2)) !now velocity vector length, rather than individual component
       END IF


       IF ( adaptden .EQ. 1 ) scl(n_den) = 1.0_pr 
       IF ( adaptmom .EQ. 1 ) THEN
          DO i = 1,dim
             scl(n_mom(i)) = 1.0_pr
             IF ( adaptvelonly ) scl(n_vel(i)) = 1.0_pr
          END DO
       END IF
       IF ( adapteng .EQ. 1 ) THEN
          scl(n_eng) = 1.0_pr
          if (adaptengonly) scl(n_eon) = 1.0_pr
       END IF
       IF ( adaptspc .EQ. 1 ) THEN
          DO i = 1,Nspecm
             scl(n_spc(i)) = 1.0_pr 
             IF(adaptspconly)scl(n_son(i)) = 1.0_pr
          END DO
       END IF
       IF ( adaptprs .EQ. 1 ) scl(n_prs) = 1.0_pr 
       IF ( adapttmp .EQ. 1 ) scl(n_temp) = 1.0_pr
       IF ( adaptMagVort .EQ. 1 ) scl(n_mvort) = 1.0_pr 
       IF ( adaptComVort .EQ. 1 ) THEN 
          IF(dim==2)scl(n_cvort(1)) = 1.0_pr 
          IF(dim==3)scl(n_cvort(1:dim)) = 1.0_pr 
       END IF
       IF ( adaptMagVel .EQ. 1 ) scl(n_mvel) = 1.0_pr
       IF ( adaptNormS .EQ. 1 ) scl(n_mods) = 1.0_pr
       IF ( adaptGradY .EQ. 1 ) THEN
          scl(n_gdy(1:Nspecm)) = 1.0_pr 
       END IF
       IF ( adaptbuoy .EQ. 1 ) scl(n_buoy) = 1.0_pr 

    END IF
    
  END SUBROUTINE compressible_scales

  SUBROUTINE compressible_cal_cfl (use_default, ulocal, cfl_out)
    USE precision
    USE sizes
    USE pde
    USE variable_mapping
    USE error_handling 
    USE curvilinear 
    USE curvilinear_mesh 
    IMPLICIT NONE
    LOGICAL , INTENT(INOUT) :: use_default
    REAL (pr),                                INTENT (INOUT) :: cfl_out
    REAL (pr), DIMENSION (nwlt,n_integrated), INTENT (IN)    :: ulocal
    INTEGER                    :: i, idim, jdim, kdim
    REAL (pr)                  :: flor, npert, CFLtemp
    REAL (pr), DIMENSION(nwlt,dim) :: cfl_local, cfl_conv, cfl_acou
    REAL (pr), DIMENSION(nwlt,dim) :: v, stretch 
    REAL (pr), DIMENSION(dim,nwlt) :: h_arr
    REAL (pr), DIMENSION(dim) :: delxyz
    REAL (pr), DIMENSION(nwlt) :: den_array, sos, props1, props2, props3, props4
    INTEGER :: l, mynwlt_global, fullnwlt, maxnwlt, minnwlt, avgnwlt
    REAL (pr), DIMENSION (nwlt) :: tmpArray, tmpArray2
    REAL (pr), DIMENSION(1,1:nwlt,1:dim) :: dun, du_dummy
    REAL (pr) :: maxMa

    use_default = .FALSE.

    flor = 1e-12_pr
    cfl_out = flor
    CFLspec = flor

    CALL get_all_local_h (h_arr)

    ! Calculate contravarient of momentum vector to align with local computational coordinates
    CALL calc_mesh_stretching( v, stretch, ulocal(:,n_mom(1:dim)), MESH_STRETCHING_PHYS )

    ! Calculating speed of sound ! ERIC: wrap this in a helper function 
    !gam(:) = cp_in(Nspec)
    !sos(:) = R_in(Nspec)
    !!sos(:) = 1.0_pr/MW_in(Nspec)
    !IF (Nspec>1) gam(:) = (1.0_pr-SUM(ulocal(:,n_spc(1):n_spc(Nspecm)),DIM=2)/ulocal(:,n_den))*cp_in(Nspec) !cp_Nspec
    !IF (Nspec>1) sos(:) = (1.0_pr-SUM(ulocal(:,n_spc(1):n_spc(Nspecm)),DIM=2)/ulocal(:,n_den))*R_in(Nspec) !R_Nspec
    !!IF (Nspec>1) sos(:) = (1.0_pr-SUM(ulocal(:,n_spc(1):n_spc(Nspecm)),DIM=2)/ulocal(:,n_den))/MW_in(Nspec) !R_Nspec
    !DO l=1,Nspecm
    !   gam(:) = gam(:) + ulocal(:,n_spc(l))/ulocal(:,n_den)*cp_in(l) !cp
    !   sos(:) = sos(:) + ulocal(:,n_spc(l))/ulocal(:,n_den)*R_in(l) !R
    !   !sos(:) = sos(:) + ulocal(:,n_spc(l))/ulocal(:,n_den)/MW_in(l) !R
    !END DO
    !gam(:) = gam(:)/(gam(:)-sos(:))
    !sos(:) = SQRT(gam(:)*(gam(:)-1.0_pr)*(ulocal(:,n_eng)-0.5_pr*SUM(ulocal(:,n_mom(1):n_mom(dim))**2,DIM=2)/ulocal(:,n_den))/ulocal(:,n_den))! spd of snd = sqrt(gamma p/rho), F_compress cancels

    sos(:) = calc_speed_of_sound( ulocal, nwlt, n_integrated )
    maxsos = MAXVAL(sos(:))
    CALL parallel_global_sum(REALMAXVAL=maxsos)
    
    ! Termination check, to kill jobs that have blown up
    IF( ABS(maxsos) .GT. HUGE(maxsos) ) CALL error('Speed of sound out of bounds')

    ! ERIC: why is there a multiplication by sqrt(gamma), if it is legit, why isn't it done above?
    !maxMa = MAXVAL(SQRT(SUM(ulocal(:,n_mom(1):n_mom(dim))**2,DIM=2)/ulocal(:,n_den)**2)/sos(:)*SQRT(gam(:)))
    maxMa = MAXVAL(SQRT(SUM(ulocal(:,n_mom(1):n_mom(dim))**2,DIM=2)/ulocal(:,n_den)**2)/sos(:) ) 
    CALL parallel_global_sum(REALMAXVAL=maxMa)

    ! Transformed acoustic characteristics on the computational grid
    IF ( ANY( transform_dir(1:dim) .EQ. 1 ) ) THEN
       DO i=1,dim
          !cfl_acou(:,i) = ( sos(:) * SQRT( SUM(curvilinear_jacobian(i,1:dim,:)**2, DIM=1 ) ) ) * dt/h_arr(i,:)
          cfl_acou(:,i) = sos(:) * stretch(:,i) * dt/h_arr(i,:)
       END DO
    ELSE
       DO i=1,dim
          cfl_acou(:,i) = sos(:) * dt/h_arr(i,:)
       END DO
    END IF

    DO i=1,dim
       cfl_conv(:,i)  = ( ABS(v(:,i)/ulocal(:,n_den))        ) * dt/h_arr(i,:)
       cfl_local(:,i) = cfl_conv(:,i) + cfl_acou(:,i) 
    END DO


    !hybridcfl with wall damping and directional anisotropy
    IF (hybridcfl) THEN

      IF (.NOT. ALLOCATED(distw_t)) THEN

        CALL error('Current hybridcfl is not valid unless the wall distance function (distw_t) is defined')

      ELSE

        CALL c_diff_fast (distw_t(1:nwlt), dun(1,1:nwlt,1:dim), du_dummy, j_lev, nwlt, HIGH_ORDER, 10, 1, 1, 1)

        DO jdim = 1, dim

          !NOTE that curvilinear_coordgrad is well-defined for both w/ and w/o curviliear mapping
          DO i = 1, nwlt
            v(i, jdim) = SUM( ulocal(i,n_mom(1:dim))*curvilinear_coordgrad(1:dim,jdim,i) )
          END DO

          tmpArray2(:) = 0.0_pr
          DO idim = 1, dim
            tmpArray(:) = 0.0_pr
            DO kdim = 1, dim
              IF (kdim == idim) THEN
                tmpArray(:) = tmpArray(:) + (1.0_pr - dun(1,:,idim)*dun(1,:,kdim))*curvilinear_coordgrad(kdim,jdim,:)
              ELSE 
                tmpArray(:) = tmpArray(:) - dun(1,:,idim)*dun(1,:,kdim)*curvilinear_coordgrad(kdim,jdim,:)
              END IF
            END DO
            tmpArray2(:) = tmpArray2(:) + tmpArray(:)**2
          END DO

          cfl_local(:,jdim) = ( ABS(v(:,jdim)/ulocal(:,n_den)) + (1.0_pr - EXP(-(distw_t(:)/L_distw)**3))*sos(:)*SQRT(SUM(curvilinear_coordgrad(1:dim,jdim,:)**2,DIM=1)) + EXP(-(distw_t(:)/L_distw)**3)*sos(:)*SQRT(tmpArray2(:)) ) &
                         / SUM(curvilinear_coordgrad(1:dim,jdim,:)**2,DIM=1) * dt / h_arr(jdim,:)

        END DO
        
      END IF !.NOT. ALLOCATED(distw_t)

    END IF !hybridcfl

    CFLnonl = MAXVAL(cfl_local(:,:))
    CFLconv = MAXVAL(cfl_conv(:,:))
    CFLacou = MAXVAL(cfl_acou(:,:))
    CALL parallel_global_sum(REALMAXVAL=CFLnonl)
    CALL parallel_global_sum(REALMAXVAL=CFLconv)
    CALL parallel_global_sum(REALMAXVAL=CFLacou)
    CFLreport = CFLnonl

    ! Von Neuman analysis
    props1(:) = mu_in(Nspec) !mu_Nspec
    props2(:) = bD_in(Nspec) !bD_Nspec
    props3(:) = kk_in(Nspec) !kk_Nspec 
    props4(:) = cv_in(Nspec) !cv_Nspec 
    IF (Nspec>1) props1(:) = (1.0_pr-SUM(ulocal(:,n_spc(1):n_spc(Nspecm)),DIM=2)/ulocal(:,n_den))*mu_in(Nspec) !mu_Nspec
    IF (Nspec>1) props2(:) = (1.0_pr-SUM(ulocal(:,n_spc(1):n_spc(Nspecm)),DIM=2)/ulocal(:,n_den))*bD_in(Nspec) !bD_Nspec
    IF (Nspec>1) props3(:) = (1.0_pr-SUM(ulocal(:,n_spc(1):n_spc(Nspecm)),DIM=2)/ulocal(:,n_den))*kk_in(Nspec) !kk_Nspec
    IF (Nspec>1) props4(:) = (1.0_pr-SUM(ulocal(:,n_spc(1):n_spc(Nspecm)),DIM=2)/ulocal(:,n_den))*cv_in(Nspec) !cv_Nspec
    DO l=1,Nspecm
       props1(:) = props1(:) + ulocal(:,n_spc(l))/ulocal(:,n_den)*mu_in(l) !mu
       props2(:) = props2(:) + ulocal(:,n_spc(l))/ulocal(:,n_den)*bD_in(l) !bD
       props3(:) = props3(:) + ulocal(:,n_spc(l))/ulocal(:,n_den)*kk_in(l) !kk
       props4(:) = props4(:) + ulocal(:,n_spc(l))/ulocal(:,n_den)*cv_in(l) !cv
    END DO
    !CALL print_extrema(props1,nwlt,'CFL: mu')
    !CALL print_extrema(mu_turb,nwlt,'CFL: mu_t')
    !CALL print_extrema(props2,nwlt,'CFL: kk')
    !CALL print_extrema(kk_turb,nwlt,'CFL: kk_t')
    IF ( use_comp_eddy_visc ) THEN
       props1 = props1 + mu_turb 
       props2 = props2 + kk_turb  
       IF(Nspec.GT.1) props3 = props3 + bD_turb 
    END IF
    ! ERIC: and check kinevisc and division by density and cv, see below
    IF (kinevisc .GT. 0) THEN
       CALL buffkine (den_array(:), ulocal(:,n_den), ng)  
       props1(:) = props1(:)*den_array(:)
    END IF
    IF ( viscmodel .EQ. viscmodel_sutherland ) THEN
       tmpArray = calc_temperature(ulocal, nwlt, n_integrated) 
       !CALL print_extrema(tmpArray,nwlt,'calc_temperature function check in CFL')
       tmpArray = calc_nondim_sutherland_visc( tmpArray, nwlt, sutherland_const ) 
       !CALL print_extrema(tmpArray,nwlt,'Sutherland Scaling in CFL')
       props1(:) = props1(:) * tmpArray 
       props2(:) = props2(:) * tmpArray 
       !CALL print_extrema(props1(:),nwlt,'CFL: sutherland visc check in RHS')
    END IF
    !IMPORTANT NOTE: in order to calculate thermal CFL correctly, it should be taken into account that T=O(1/(Cv*Fcompress)), thus to get right, one need to devie by Cv (Fcompress is taken into account already)
    IF ( ANY( transform_dir(1:dim) .EQ. 1 ) ) THEN  
      CFLvisc = MAXVAL( 2.0_pr*dt*props1(:)/ulocal(:,n_den)            *MAXVAL(( SUM(curvilinear_jacobian(1:dim,1:dim,:)**2, DIM=2 ) )/(h_arr(:,:)*h_arr(:,:)),DIM=1)  )
      IF(Nspec.GT.1) CFLspec = MAXVAL( 2.0_pr*dt*props2(:)             *MAXVAL(( SUM(curvilinear_jacobian(1:dim,1:dim,:)**2, DIM=2 ) )/(h_arr(:,:)*h_arr(:,:)),DIM=1)  )
      CFLthrm = MAXVAL( 2.0_pr*dt*props3(:)/(ulocal(:,n_den)*props4(:))*MAXVAL(( SUM(curvilinear_jacobian(1:dim,1:dim,:)**2, DIM=2 ) )/(h_arr(:,:)*h_arr(:,:)),DIM=1)  ) 
   ELSE
      CFLvisc = MAXVAL( 2.0_pr*dt*props1(:)/ulocal(:,n_den)            *MAXVAL(1.0_pr/(h_arr(:,:)),DIM=1)**2  )
      IF(Nspec.GT.1) CFLspec = MAXVAL( 2.0_pr*dt*props2(:)             *MAXVAL(1.0_pr/(h_arr(:,:)),DIM=1)**2  )
      CFLthrm = MAXVAL( 2.0_pr*dt*props3(:)/(ulocal(:,n_den)*props4(:))*MAXVAL(1.0_pr/(h_arr(:,:)),DIM=1)**2  )  
    END IF
    CALL parallel_global_sum(REALMAXVAL=CFLvisc)
    CALL parallel_global_sum(REALMAXVAL=CFLspec)
    CALL parallel_global_sum(REALMAXVAL=CFLthrm)

    !ERIC: what is the point of this? 
    !delxyz(1:dim)=(xyzlimits(2,1:dim)-xyzlimits(1,1:dim))/REAL(mxyz(1:dim)*2**(j_lev-1),pr) 
    !CFLvisc = 2.0_pr*dt/Re*MAX(1.0_pr/(1.0_pr-At),Ma**2*MAXVAL(gamm-1.0_pr))*SUM(1.0_pr/(delxyz**2))
    !CFLspec = 2.0_pr*dt/Re/Sc*MAXVAL(gamm)*SUM(1.0_pr/(delxyz**2))
    !CFLthrm = 2.0_pr*dt/Re/Pra*MAXVAL(gamm-1.0_pr)*SUM(1.0_pr/(delxyz**2))

    ! ERIC: remove these
    CFLbffv = flor 
    CFLbfft = flor
    CFLbffs = flor

    CFLtemp = 0.0_pr
    IF (ComboIMEXRK) THEN
       IF (visccfl>0.0_pr) CFLtemp = MAX(CFLtemp,CFLvisc,CFLthrm,CFLspec,CFLbffv,CFLbfft,CFLbffs)
       IF (CFLtemp .GT. imexcfl) THEN
          time_integration_method = TIME_INT_IMEX
       ELSE
          time_integration_method = TIME_INT_RK
       END IF
    END IF

    !IF (time_integration_method == TIME_INT_RK .AND. NOT(ComboIMEXRK)) THEN
       IF (visccfl>0.0_pr) CFLreport = MAX(CFLreport,cflmax/ABS(visccfl)*MAX(CFLvisc,CFLthrm,CFLspec,CFLbffv,CFLbfft,CFLbffs))
    !END IF

    mynwlt_global = nwlt
    fullnwlt = PRODUCT(mxyz(1:dim))
    CALL parallel_global_sum (INTEGER=mynwlt_global)
    npert = REAL(mynwlt_global)
    DO i=1,dim
       npert = npert/REAL(mxyz(i)*2**(j_lev-1)+1-prd(i),pr)
    END DO
    avgnwlt = mynwlt_global/par_size 
    maxnwlt = nwlt
    minnwlt = -nwlt
    CALL parallel_global_sum(INTEGERMAXVAL=maxnwlt)
    CALL parallel_global_sum(INTEGERMAXVAL=minnwlt)
    minnwlt=-minnwlt
    MAreport = maxMa
    IF (convcfl) THEN
       cfl_out = MAX(cfl_out, CFLconv)
    ELSE
       cfl_out = MAX(cfl_out, CFLreport)
    END IF
    IF (par_rank.EQ.0) THEN
       PRINT *, '**********************************************************'
       PRINT *, 'case:    ', file_gen(1:LEN_TRIM(file_gen)-1)
       PRINT *, 'max Ma = ', maxMa
       PRINT *, 'max SoS = ', maxsos
       PRINT *, 'CFL    = ', cfl_out,        '  CFLacou = ', CFLacou
       PRINT *, 'CFLconv= ', cflconv,        '  CFLnonl = ', CFLnonl
       PRINT *, 'CFLvisc= ', cflvisc,        '  CFLthrm = ', CFLthrm
       PRINT *, 'it     = ', it,             '  t       = ', t
       PRINT *, 'iwrite = ', iwrite-1,       '  dt      = ', dt 
       PRINT *, 'j_mx   = ', j_mx,           '  dt_orig = ', dt_original
       PRINT *, 'j_lev  = ', j_lev,          '  twrite  = ', twrite
       PRINT *, 'nwlt_g = ', mynwlt_global,  '  cpu     = ', timer_val(2)
       PRINT *, 'n_mvec = ', fullnwlt,       '  n%      = ', npert
       PRINT *, 'nwlt   = ', nwlt,           '  eps     = ', eps
       PRINT *, 'nwlt_mx= ', maxnwlt,        '  nwlt_av = ', avgnwlt
       PRINT *, 'nwlt_mn= ', minnwlt,        '  p_size  = ', par_size
       PRINT *, '**********************************************************'
    END IF
  END SUBROUTINE compressible_cal_cfl

  SUBROUTINE  compressible_pre_process
    USE field
    IMPLICIT NONE
    INTEGER :: i, l, ie, shift
    CHARACTER(LEN=8):: numstrng
    CHARACTER(LEN=100):: commandstrng

    INTEGER, SAVE :: nwlt_last = -1

    ! ERIC: this is very very bad.  Allocations for this are made in two separate places.  The problem is that sgsmodel is handled by trunk, and compressible_equations by user
    IF ( .NOT. use_comp_eddy_visc .AND. nwlt_last .NE. nwlt) THEN
      IF( ALLOCATED(mu_turb) )         DEALLOCATE( mu_turb )
                                       ALLOCATE(   mu_turb(1:nwlt) )
      mu_turb = 0.0_pr

      IF( ALLOCATED(kk_turb) )         DEALLOCATE( kk_turb )
                                       ALLOCATE(   kk_turb(1:nwlt) )
      kk_turb = 0.0_pr

      IF( ALLOCATED(bD_turb) )         DEALLOCATE( bD_turb )
      IF( Nspec > 1          ) THEN
                                       ALLOCATE(   bD_turb(1:nwlt) )
        bD_turb = 0.0_pr
      END IF
    END IF

    IF (boundY .AND. Nspec>1 ) THEN
       DO i=1,nwlt
          u(i,n_spc(1)) = MIN(MAX(u(i,n_spc(1)),0.0_pr),u(i,n_den))
       END DO
       DO l=2,Nspecm
          DO i=1,nwlt
             u(i,n_spc(l)) = MIN(MAX(u(i,n_spc(l)),0.0_pr),u(i,n_den)-SUM(u(i,n_spc(1):n_spc(l-1))))
          END DO
       END DO
    END IF
   
  END SUBROUTINE compressible_pre_process

  SUBROUTINE  compressible_post_process
    USE field
    IMPLICIT NONE
    CALL compressible_pre_process
  END SUBROUTINE compressible_post_process

  ! Helper function to calculate the speed of sound
  FUNCTION calc_speed_of_sound( ulocal, nlocal, ne_local )
    USE precision
    USE sizes
    USE pde
    USE variable_mapping
    IMPLICIT NONE
    INTEGER, INTENT(IN)                    :: nlocal, ne_local 
    REAL (pr), DIMENSION (nlocal,ne_local), INTENT (IN)    :: ulocal
    REAL (pr), DIMENSION(nlocal) :: calc_speed_of_sound 

    REAL (pr), DIMENSION(nlocal) :: gam, props
    INTEGER :: l

    ! Calculating speed of sound 
    ! ERIC: Scott R's formulation.  Why can't we calc this directly from gamma input array (would simplify the number of intermediate calcs)?
    gam(:) = cp_in(Nspec)
    props(:) = R_in(Nspec)
    !props(:) = 1.0_pr/MW_in(Nspec)
    IF (Nspec>1) gam(:) = (1.0_pr-SUM(ulocal(:,n_spc(1):n_spc(Nspecm)),DIM=2)/ulocal(:,n_den))*cp_in(Nspec) !cp_Nspec
    IF (Nspec>1)props(:) = (1.0_pr-SUM(ulocal(:,n_spc(1):n_spc(Nspecm)),DIM=2)/ulocal(:,n_den))*R_in(Nspec) !R_Nspec
    !IF (Nspec>1)props(:) = (1.0_pr-SUM(ulocal(:,n_spc(1):n_spc(Nspecm)),DIM=2)/ulocal(:,n_den))/MW_in(Nspec) !R_Nspec
    DO l=1,Nspecm
       gam(:) = gam(:) + ulocal(:,n_spc(l))/ulocal(:,n_den)*cp_in(l) !cp
       props(:) = props(:) + ulocal(:,n_spc(l))/ulocal(:,n_den)*R_in(l) !R
       !props(:) = props(:) + ulocal(:,n_spc(l))/ulocal(:,n_den)/MW_in(l) !R
    END DO
    gam(:) = gam(:)/(gam(:)-props(:))

    calc_speed_of_sound(:) = SQRT(gam(:)*(gam(:)-1.0_pr)*(ulocal(:,n_eng)-0.5_pr*SUM(ulocal(:,n_mom(1):n_mom(dim))**2,DIM=2)/ulocal(:,n_den))/ulocal(:,n_den))! spd of snd = sqrt(gamma p/rho), F_compress cancels

  END FUNCTION calc_speed_of_sound 

  ! Helper function to calculate the temperature from integrated variables 
  PURE FUNCTION calc_temperature( ulocal, nlocal, ne_local )
    USE precision
    USE sizes
    USE pde
    USE variable_mapping
    IMPLICIT NONE
    INTEGER, INTENT(IN)                    :: nlocal, ne_local 
    REAL (pr), DIMENSION (nlocal,ne_local), INTENT (IN)    :: ulocal
    REAL (pr), DIMENSION(nlocal) :: calc_temperature

    REAL (pr), DIMENSION(nlocal) :: cp_field, R_field 
    INTEGER :: l

    ! Calculating speed of sound 
    ! ERIC: Scott R's formulation.  Why can't we calc this directly from gamma input array (would simplify the number of intermediate calcs)?
    cp_field(:) = cp_in(Nspec)
    R_field(:) = R_in(Nspec)
    IF (Nspec>1) cp_field(:) = (1.0_pr-SUM(ulocal(:,n_spc(1):n_spc(Nspecm)),DIM=2)/ulocal(:,n_den))*cp_in(Nspec) !cp_Nspec
    IF (Nspec>1) R_field(:) = (1.0_pr-SUM(ulocal(:,n_spc(1):n_spc(Nspecm)),DIM=2)/ulocal(:,n_den))*R_in(Nspec) !R_Nspec
    DO l=1,Nspecm
       cp_field(:) = cp_field(:) + ulocal(:,n_spc(l))/ulocal(:,n_den)*cp_in(l) !cp
       R_field(:) = R_field(:) + ulocal(:,n_spc(l))/ulocal(:,n_den)*R_in(l) !R
    END DO

    calc_temperature(:) = ( ulocal(:,n_eng)-0.5_pr*SUM(ulocal(:,n_mom(1):n_mom(dim))*ulocal(:,n_mom(1):n_mom(dim)),DIM=2)/ulocal(:,n_den)) &! Internal Energy
        /( ulocal(:,n_den) * ( cp_field - R_field) ) / F_compress

  END FUNCTION calc_temperature

  ! Helper function to calculate the pressure from integrated variables 
  PURE FUNCTION calc_pressure( ulocal, nlocal, ne_local )
    USE precision
    USE sizes
    USE pde
    USE variable_mapping
    IMPLICIT NONE
    INTEGER, INTENT(IN)                    :: nlocal, ne_local 
    REAL (pr), DIMENSION (nlocal,ne_local), INTENT (IN)    :: ulocal
    REAL (pr), DIMENSION(nlocal) :: calc_pressure

    REAL (pr), DIMENSION(nlocal) :: cp_field, R_field 
    INTEGER :: l

    ! Calculating speed of sound 
    ! ERIC: Scott R's formulation.  Why can't we calc this directly from gamma input array (would simplify the number of intermediate calcs)?
    cp_field(:) = cp_in(Nspec)
    R_field(:) = R_in(Nspec)
    IF (Nspec>1) cp_field(:) = (1.0_pr-SUM(ulocal(:,n_spc(1):n_spc(Nspecm)),DIM=2)/ulocal(:,n_den))*cp_in(Nspec) !cp_Nspec
    IF (Nspec>1) R_field(:) = (1.0_pr-SUM(ulocal(:,n_spc(1):n_spc(Nspecm)),DIM=2)/ulocal(:,n_den))*R_in(Nspec) !R_Nspec
    DO l=1,Nspecm
       cp_field(:) = cp_field(:) + ulocal(:,n_spc(l))/ulocal(:,n_den)*cp_in(l) !cp
       R_field(:) = R_field(:) + ulocal(:,n_spc(l))/ulocal(:,n_den)*R_in(l) !R
    END DO

    calc_pressure(:) = ( ulocal(:,n_eng)-0.5_pr*SUM(ulocal(:,n_mom(1):n_mom(dim))*ulocal(:,n_mom(1):n_mom(dim)),DIM=2)/ulocal(:,n_den)) &! Internal Energy
        /(cp_field(:)/R_field(:)-1.0_pr) / F_compress  !pressure

  END FUNCTION calc_pressure

  ! Helper function to calculate the nondimensionalized sutherland viscosity coefficient 
  PURE FUNCTION calc_nondim_sutherland_visc( temperature, nlocal, s_const )
    USE precision
    IMPLICIT NONE
    INTEGER, INTENT(IN)                           :: nlocal
    REAL (pr), DIMENSION (nlocal), INTENT (IN)    :: temperature
    REAL (pr), INTENT (IN)                        :: s_const
    REAL (pr), DIMENSION(nlocal) :: calc_nondim_sutherland_visc

    calc_nondim_sutherland_visc(:) = (1.0_pr+s_const)/(temperature +s_const)*(temperature)**(1.5_pr)

  END FUNCTION calc_nondim_sutherland_visc


  ! Accessor to set the body force coefficient
  SUBROUTINE set_body_force( coeff, species, direction ) 
    USE error_handling
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: species, direction
    REAL (pr), INTENT(IN) :: coeff

    IF ( species .LE. Nspec .AND. direction .LE. dim  ) THEN 
       gr_in( species + Nspec * (direction-1) ) = coeff
    ELSE
      CALL error("out of bounds setting of body force coeff")
    END IF

  END SUBROUTINE set_body_force

  ! Accessor to set the compressibility factor (coefficient on pressure terms for nondimensionalization of the equations) 
  SUBROUTINE set_F_compress( F_loc ) 
    USE error_handling
    IMPLICIT NONE
    REAL (pr), INTENT(IN) :: F_loc 

    F_compress = F_loc 
    IF( F_compress .LE. 0.0_pr ) CALL error("Compressibility Factor (F_compress) must be > 0")

  END SUBROUTINE set_F_compress

  ! Accessor to set the thermodynamic properties 
  SUBROUTINE set_thermo_props( cp_loc, R_loc, species ) 
    USE error_handling
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: species
    REAL (pr), INTENT(IN) :: cp_loc, R_loc

    IF ( species .LE. Nspec ) THEN 
       cp_in( species ) = cp_loc 
       R_in( species ) = R_loc 
       cv_in(species) = cp_loc - R_loc

       ! ERIC: are these consistency checks valid?
       IF( ABS( gamm(species) - cp_in(species)/(cv_in(species)) ) .GT. 1.0e-12_pr ) CALL error("Nondimensionalized thermo properties not consistent")
    ELSE
      CALL error("out of bounds setting of thermo_props")
    END IF
  END SUBROUTINE set_thermo_props 

  ! Accessor to set the dynamic viscosity 
  SUBROUTINE set_dynamic_viscosity( mu_loc, species ) 
    USE error_handling
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: species
    REAL (pr), INTENT(IN) :: mu_loc
    IF ( species .LE. Nspec ) THEN 
       mu_in( species ) = mu_loc 
       IF(mu_in(species) .LE. 0.0_pr ) CALL error("Dynamic viscosity must be >= 0")
    ELSE
      CALL error("out of bounds setting of mu")
    END IF
  END SUBROUTINE set_dynamic_viscosity

  ! Accessor to set the conductivity
  SUBROUTINE set_conductivity( kappa_loc, species ) 
    USE error_handling
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: species
    REAL (pr), INTENT(IN) :: kappa_loc
    IF ( species .LE. Nspec ) THEN 
       kk_in( species ) = kappa_loc
       IF(kk_in(species) .LE. 0.0_pr ) CALL error("Conductivity must be >= 0")
    ELSE
      CALL error("out of bounds setting of kappa")
    END IF
  END SUBROUTINE set_conductivity

  ! Accessor to set the diffusivity 
  SUBROUTINE set_diffusivity( diff_loc, species ) 
    USE error_handling
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: species
    REAL (pr), INTENT(IN) :: diff_loc
    IF ( species .LE. Nspec ) THEN 
       bD_in( species ) = diff_loc
       IF(bD_in(species) .LE. 0.0_pr ) CALL error("Diffusivity must be >= 0")
    ELSE
      CALL error("out of bounds setting of diffusivity")
    END IF
  END SUBROUTINE set_diffusivity

  ! Calculating viscosity
  PURE FUNCTION calc_dynamic_viscosity( u_int )
    USE variable_mapping
    IMPLICIT NONE
    REAL (pr), DIMENSION (nwlt,n_integrated), INTENT (IN)    :: u_int
    REAL (pr), DIMENSION (nwlt) :: calc_dynamic_viscosity

    CALL fraction_avg1( calc_dynamic_viscosity, mu_in(:), u_int )
    calc_dynamic_viscosity = calc_dynamic_viscosity * calc_nondim_sutherland_visc( calc_temperature(u_int, nwlt, n_integrated ), nwlt, sutherland_const )

  END FUNCTION calc_dynamic_viscosity

  ! Calculating thermal conductivity
  PURE FUNCTION calc_conductivity( u_int )
    USE variable_mapping
    IMPLICIT NONE
    REAL (pr), DIMENSION (nwlt,n_integrated), INTENT (IN)    :: u_int
    REAL (pr), DIMENSION (nwlt) :: calc_conductivity

    CALL fraction_avg1( calc_conductivity, kk_in(:), u_int )

  END FUNCTION calc_conductivity

  ! Calculating viscosity
  PURE FUNCTION calc_spec_diffusion( u_int )
    USE variable_mapping
    IMPLICIT NONE
    REAL (pr), DIMENSION (nwlt,n_integrated), INTENT (IN)    :: u_int
    REAL (pr), DIMENSION (nwlt) :: calc_spec_diffusion

    CALL fraction_avg1( calc_spec_diffusion, bD_in(:), u_int )

  END FUNCTION calc_spec_diffusion

  ! Routines for species averaging
  PURE SUBROUTINE fraction_avg1( avg1, qty_in1, ulocal )
    USE variable_mapping
    IMPLICIT NONE
    REAL (pr), DIMENSION(Nspec), INTENT(IN) :: qty_in1 
    REAL (pr), DIMENSION (nwlt,n_integrated), INTENT (IN)    :: ulocal
    REAL (pr), DIMENSION(nwlt), INTENT(OUT) :: avg1 
    INTEGER :: l

    avg1(:) = qty_in1(Nspec) 
    IF (Nspec>1) avg1(:) = (1.0_pr-SUM(ulocal(:,n_spc(1):n_spc(Nspecm)),DIM=2)/ulocal(:,n_den)) * qty_in1(Nspec) 
    DO l=1,Nspecm
       avg1(:) = avg1(:) + ulocal(:,n_spc(l))/ulocal(:,n_den) * qty_in1(l) 
    END DO
  END SUBROUTINE fraction_avg1

  PURE SUBROUTINE fraction_avg2( avg1, avg2, qty_in1, qty_in2, ulocal )
    USE variable_mapping
    IMPLICIT NONE
    REAL (pr), DIMENSION(Nspec), INTENT(IN) :: qty_in1, qty_in2 
    REAL (pr), DIMENSION (nwlt,n_integrated), INTENT (IN)    :: ulocal
    REAL (pr), DIMENSION(nwlt), INTENT(OUT) :: avg1, avg2 
    INTEGER :: l

    avg1(:) = qty_in1(Nspec) 
    avg2(:) = qty_in2(Nspec) 
    IF (Nspec>1) avg1(:) = (1.0_pr-SUM(ulocal(:,n_spc(1):n_spc(Nspecm)),DIM=2)/ulocal(:,n_den)) * qty_in1(Nspec) 
    IF (Nspec>1) avg2(:) = (1.0_pr-SUM(ulocal(:,n_spc(1):n_spc(Nspecm)),DIM=2)/ulocal(:,n_den)) * qty_in2(Nspec) 
    DO l=1,Nspecm
       avg1(:) = avg1(:) + ulocal(:,n_spc(l))/ulocal(:,n_den) * qty_in1(l) 
       avg2(:) = avg2(:) + ulocal(:,n_spc(l))/ulocal(:,n_den) * qty_in2(l) 
    END DO
  END SUBROUTINE fraction_avg2

  PURE SUBROUTINE fraction_avg3( avg1, avg2, avg3, qty_in1, qty_in2, qty_in3, ulocal )
    USE variable_mapping
    IMPLICIT NONE
    REAL (pr), DIMENSION(Nspec), INTENT(IN) :: qty_in1, qty_in2, qty_in3 
    REAL (pr), DIMENSION (nwlt,n_integrated), INTENT (IN)    :: ulocal
    REAL (pr), DIMENSION(nwlt), INTENT(OUT) :: avg1, avg2, avg3 
    INTEGER :: l

    avg1(:) = qty_in1(Nspec) 
    avg2(:) = qty_in2(Nspec) 
    avg3(:) = qty_in3(Nspec) 
    IF (Nspec>1) avg1(:) = (1.0_pr-SUM(ulocal(:,n_spc(1):n_spc(Nspecm)),DIM=2)/ulocal(:,n_den)) * qty_in1(Nspec) 
    IF (Nspec>1) avg2(:) = (1.0_pr-SUM(ulocal(:,n_spc(1):n_spc(Nspecm)),DIM=2)/ulocal(:,n_den)) * qty_in2(Nspec) 
    IF (Nspec>1) avg3(:) = (1.0_pr-SUM(ulocal(:,n_spc(1):n_spc(Nspecm)),DIM=2)/ulocal(:,n_den)) * qty_in3(Nspec) 
    DO l=1,Nspecm
       avg1(:) = avg1(:) + ulocal(:,n_spc(l))/ulocal(:,n_den) * qty_in1(l) 
       avg2(:) = avg2(:) + ulocal(:,n_spc(l))/ulocal(:,n_den) * qty_in2(l) 
       avg3(:) = avg3(:) + ulocal(:,n_spc(l))/ulocal(:,n_den) * qty_in3(l) 
    END DO

  END SUBROUTINE fraction_avg3



END MODULE equations_compressible 
