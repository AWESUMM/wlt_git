#ifndef ZOLTAN_PARTITIONING__HEADER_H
#define ZOLTAN_PARTITIONING__HEADER_H


// uppend underscore or set lowercase if necessary
#include "lowercase.h"


// Zoltan interface function for FORTRAN
extern "C" void ZOLTAN_DOMAIN_PARTITIONING
(
 int           *FromFortran__Number_Vertices,                 // IN [total number of vertices on all the proessors]
 int           *FromFortran__Number_Vertices_Neighbors,       // IN [number of neighbours for each vertex]
 int           *FromFortran__Number_Vertices_OnEachProcess,   // IN [number of vertices on each processor]
 int           *FromFortran__Vertices_Global_ID,              // IN [ID starting from 1 of all vertices in the order or processor rank]
 int           *FromFortran__Edges_Vertices_ID,               // IN [ID of neighbours for each vertex]
 int           *FromFortran__Edges_Vertices_Nwlt,             // IN [Nwlt of edges for each vertex]
 int           *FromFortran__nwlt_Vertices,                   // IN [nwlt per vertex] 
 int           *FromFortran__N_Predict,                       // IN [n_prdct]
 int           *FromFortran__N_Update,                        // IN [n_updt]
 int           *FromFortran__Problem_Dimension,               // IN [dim]
 double        *FromFortran__Vertices_Coordinates,            // IN [actual coordinates, used in geometric approach]
 int           *FromFortran__Partitioning_Method,             // IN [1 or 3 is currently working]
 double        *FromFortran__EdgeWeight_Global_SafetyFactor,  // IN [0.1 <-> 10% of nodes in boundary zone]
 int           *FromFortran__task,                            // IN [0-partition, 1-repartition, 2-refine]
 int           *FromFortran__verb_level,                      // IN [Zoltan verb level 0-10]
 int           *FromFortran__Global_Processor_Flag,           // OUT 
 int           *FromFortran__error_out                        // OUT - should be zero if no errors
 );


extern int           Number_Vertices;
extern int          *Number_Vertices_Neighbors;
extern int          *Number_Vertices_OnEachProcess;
extern int          *Vertices_Global_ID;
extern int          *Edges_Vertices_ID;
extern int          *Edges_Vertices_Nwlt;
extern int          *nwlt_Vertices;
extern int           N_Predict;
extern int           N_Update;
extern int           Problem_Dimension;
extern double       *Vertices_Coordinates;
extern int           Partitioning_Method;
extern double        EdgeWeight_Global_SafetyFactor;
extern int          *Global_Processor_Flag;
extern int           verbosity;

extern int myRank;
extern int numProcs;

static int argc;
static char **argv;



#endif
