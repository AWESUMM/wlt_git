
/******************************************************************************
 **                                                                           **
 ** Interface Function Between  wlt3D code  and  Zoltan Partitioning Lirary   **
 **                                                                           **
 **                                                                           **
 **    Zoltan :: Copyright (c) 2000-2008, Sandia National Laboratories.       **
 **                                                                           **
 *******************************************************************************/


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <algorithm>

#include "zoltan_cpp.h"

#include "zoltan_partitioning__header.h"
#include "zoltan_partitioning__queries.h"
#include "tree.h"



// define LOCAL_ZOLTAN_DEBUG to see some debug output
// #define LOCAL_ZOLTAN_DEBUG


                        
int           Number_Vertices;
int          *Number_Vertices_Neighbors;
int          *Number_Vertices_OnEachProcess;
int          *Vertices_Global_ID;
int          *Edges_Vertices_ID;
int          *Edges_Vertices_Nwlt;
int          *nwlt_Vertices;
int           N_Predict;
int           N_Update;
int           Problem_Dimension;
double       *Vertices_Coordinates;
int           Partitioning_Method;
double        EdgeWeight_Global_SafetyFactor;
int          *Global_Processor_Flag;


int myRank;
int numProcs;






void ZOLTAN_DOMAIN_PARTITIONING(
				int           *FromFortran__Number_Vertices,
				int           *FromFortran__Number_Vertices_Neighbors,       //[]
				int           *FromFortran__Number_Vertices_OnEachProcess,   //[]
				int           *FromFortran__Vertices_Global_ID,              //[]
				int           *FromFortran__Edges_Vertices_ID,               //[]
				int           *FromFortran__Edges_Vertices_Nwlt,             //[]
				int           *FromFortran__nwlt_Vertices,                   //[] 
				int           *FromFortran__N_Predict,                       //[] 
				int           *FromFortran__N_Update,                        //[] 
				int           *FromFortran__Problem_Dimension,
				double        *FromFortran__Vertices_Coordinates,            //[]
				int           *FromFortran__Partitioning_Method,
				double        *FromFortran__EdgeWeight_Global_SafetyFactor,
				int           *FromFortran__task,
				int           *FromFortran__verb_level,
				int           *FromFortran__Global_Processor_Flag,
				int           *FromFortran__error_out)

{





  /*************************************************************************
   ** L o c a l     V a r i a b l e s     for     Domain_Partitioning(...)
   **************************************************************************/
  int              rc, i, j, shift, ngids, nextIdx;
  
  float            ver;
  
  struct           Zoltan_Struct *zz;
  
  int              changes, numGidEntries, numLidEntries, numImport, numExport;

  ZOLTAN_ID_PTR    importGlobalGids, importLocalGids;
  ZOLTAN_ID_PTR    exportGlobalGids, exportLocalGids;
  
  int              *importProcs, *importToPart, *exportProcs, *exportToPart;
  int              *gid_flags, *gid_list;
  char             verbosity[3] = "0";
  char             task[3][15] = { "PARTITION", "REPARTITION", "REFINE" };






  /******************************************************************
   ** MPI   ::    Getting Processors Rank   and   Total Number
   ******************************************************************/
  
  // NO Need to Initialize the MPI, it has already been done in Fortran
  MPI_Comm_rank(MPI_COMM_WORLD, &myRank);
  MPI_Comm_size(MPI_COMM_WORLD, &numProcs);





  /******************************************************************
   ** A s s i g n i n g     G l o b a l     V a r i a b l e s 
   ******************************************************************/
 
  Number_Vertices                =  *FromFortran__Number_Vertices;
  Problem_Dimension              =  *FromFortran__Problem_Dimension;
  Partitioning_Method            =  *FromFortran__Partitioning_Method;
  EdgeWeight_Global_SafetyFactor =  *FromFortran__EdgeWeight_Global_SafetyFactor;          
  N_Predict                      =  *FromFortran__N_Predict;
  N_Update                       =  *FromFortran__N_Update;
  
  sprintf(verbosity,"%d",*FromFortran__verb_level);
  
  
  
  /******************************************************************
   ** A s s i g n i n g     G l o b a l     A r r a y s 
   ******************************************************************/

  Number_Vertices_Neighbors      =   FromFortran__Number_Vertices_Neighbors;       
  Number_Vertices_OnEachProcess  =   FromFortran__Number_Vertices_OnEachProcess;   
  Vertices_Global_ID             =   FromFortran__Vertices_Global_ID;              
  Edges_Vertices_ID              =   FromFortran__Edges_Vertices_ID;               
  Edges_Vertices_Nwlt            =   FromFortran__Edges_Vertices_Nwlt;               
  nwlt_Vertices                  =   FromFortran__nwlt_Vertices;                   
  Vertices_Coordinates           =   FromFortran__Vertices_Coordinates; 
  Global_Processor_Flag          =   FromFortran__Global_Processor_Flag;



  /******************************************************************
   ** Initialize Zoltan
   ******************************************************************/

  rc = Zoltan_Initialize(argc, argv, &ver);
      
  if (rc != ZOLTAN_OK){
    *FromFortran__error_out = 1;
    return;
  }





  /******************************************************************
   ** Create a Zoltan Library Structure 
   **                            for this instance of load balancing.
   ** 
   ** Set the Parameters  and  Query Functions 
   **                     that will govern the library's calculation.
   ** 
   ** See the Zoltan User's Guide
   **          for the definition of these and many other parameters.
   ******************************************************************/

  zz = Zoltan_Create(MPI_COMM_WORLD);
  
  
  /* General parameters */
  Zoltan_Set_Param(zz, "DEBUG_LEVEL",     verbosity); // amount of debug messages desired
  Zoltan_Set_Param(zz, "NUM_GID_ENTRIES", "1");       // number of integers in a global ID
  Zoltan_Set_Param(zz, "NUM_LID_ENTRIES", "1");       // number of integers in a local ID
  Zoltan_Set_Param(zz, "RETURN_LISTS",    "ALL");     // return all lists in LB_Partition
  Zoltan_Set_Param(zz, "OBJ_WEIGHT_DIM",  "1");       // dimension of a vertex weight
      
      
  if (Partitioning_Method == 3 ) { // ------ PHG HYPERGRAPH -------

    Zoltan_Set_Param(zz, "LB_METHOD",       "HYPERGRAPH");
    Zoltan_Set_Param(zz, "HYPERGRAPH_PACKAGE",          "PHG");     // version of method
    Zoltan_Set_Param(zz, "LB_APPROACH", task[*FromFortran__task] ); // 0-partition, 1-repartition, 2-refine
    //Zoltan_Set_Param(zz, "CHECK_HYPERGRAPH", "1");
    //Zoltan_Set_Param(zz, "PHG_OUTPUT_LEVEL", "1");

    Zoltan_Set_Param(zz, "ADD_OBJ_WEIGHT",              "NONE");
    Zoltan_Set_Param(zz, "EDGE_WEIGHT_DIM",             "1");      // dimension of edge weight
    Zoltan_Set_Param(zz, "PHG_EDGE_WEIGHT_OPERATION",   "MAX");
    Zoltan_Set_Param(zz, "PHG_EDGE_SIZE_THRESHOLD",     ".7");
    Zoltan_Set_Param(zz, "PHG_COARSENING_METHOD",       "ipm");
    Zoltan_Set_Param(zz, "PHG_COARSEPARTITION_METHOD",  "greedy");
    Zoltan_Set_Param(zz, "PHG_REFINEMENT_METHOD",       "fm");
    
    //Zoltan_Set_Param(zz, "PHG_REPART_MULTIPLIER", "100");
    //
    // PHG attempts to minimize the function (PHG_REPART_MULTIPLIER* edge_cut + migration volume).
    // The migration volume is measured using the ZOLTAN_OBJ_SIZE_FN query function.
    //

    /* Default Values: 
       LB_APPROACH                      =  REPARTITION
       PHG_REPART_MULTIPLIER            =  100 
       PHG_MULTILEVEL=1 if LB_APPROACH  =  partition or repartition; 0 otherwise. 
       PHG_EDGE_WEIGHT_OPERATION        =  max 
       ADD_OBJ_WEIGHT                   =  none 
       PHG_CUT_OBJECTIVE                =  connectivity 
       CHECK_HYPERGRAPH                 =  0
       PHG_OUTPUT_LEVEL                 =  0 
       PHG_COARSENING_METHOD            =  agg 
       PHG_COARSEPARTITION_METHOD       =  auto 
       PHG_REFINEMENT_METHOD            =  fm 
       PHG_REFINEMENT_QUALITY           =  1 
       PHG_RANDOMIZE_INPUT              =  0 
       PHG_EDGE_SIZE_THRESHOLD          =  0.25 
       PHG_PROCESSOR_REDUCTION_LIMIT    =  0.5 
    */


    /* Query Functions - defined in wlt3D_Partitioning__Queries.h */
    // General Query Functions 
    Zoltan_Set_Num_Obj_Fn(          zz, Get_Number_of_Objects,   &hg_data);  // ZOLTAN_NUM_OBJ_FN   ::   Number of items on processor
    Zoltan_Set_Obj_List_Fn(         zz, Get_Object_List,         &hg_data);  // ZOLTAN_OBJ_LIST_FN  ::   List of item IDs and weights
    // HyperGraph
    Zoltan_Set_Obj_Size_Fn(         zz, Get_Object_Size,         &hg_data);  // ZOLTAN_OBJ_SIZE_FN         :: Return migration volume
    Zoltan_Set_HG_Size_CS_Fn(       zz, Get_HG_Size,             &hg_data);  // ZOLTAN_HG_SIZE_CS_FN       :: Number of hyperedge pins
    Zoltan_Set_HG_CS_Fn(            zz, Get_HG,                  &hg_data);  // ZOLTAN_HG_CS_FN            :: List of hyperedge pins
    Zoltan_Set_HG_Size_Edge_Wts_Fn( zz, Get_HG_Num_Edge_Weights, &hg_data);  // ZOLTAN_HG_SIZE_EDGE_WTS_FN :: Number of hyperedge weights
    Zoltan_Set_HG_Edge_Wts_Fn(      zz, Get_HyperEdge_Weights,   &hg_data);  // ZOLTAN_HG_EDGE_WTS_FN      :: List of hyperedge weights

  } else if ( Partitioning_Method == 1 ) { // ------ RCB -------
    
    Zoltan_Set_Param(zz, "LB_METHOD", "RCB");            // recursive coordinate bisection
    Zoltan_Set_Param(zz, "RCB_OUTPUT_LEVEL", "0");       // amount of output desired
    //Zoltan_Set_Param(zz, "RCB_RECTILINEAR_BLOCKS", "1"); // create rectilinear regions


    //Zoltan_Set_Param(zz, "KEEP_CUTS", "1");              // save decomposition
    //Zoltan_Set_Param(zz, "RCB_LOCK_DIRECTIONS", "1");
    //Zoltan_Set_Param(zz, "RCB_MAX_ASPECT_RATIO","1.5");

    /* Default Values: 
       RCB_OVERALLOC             =  1.2
       RCB_REUSE                 =  0
       RCB_OUTPUT_LEVEL          =  0
       CHECK_GEOM                =  1
       KEEP_CUTS                 =  0
       AVERAGE_CUTS              =  0
       RCB_LOCK_DIRECTIONS       =  0
       REDUCE_DIMENSIONS         =  0
       DEGENERATE_RATIO          =  10
       RCB_SET_DIRECTIONS        =  0
       RCB_RECTILINEAR_BLOCKS    =  0
       RCB_RECOMPUTE_BOX         =  0
       OBJ_WEIGHTS_COMPARABLE    =  0
       RCB_MULTICRITERIA_NORM    =  1
       RCB_MAX_ASPECT_RATIO      =  10
    */


    /* Query Functions - defined in wlt3D_Partitioning__Queries.h */
    // General Query Functions 
    Zoltan_Set_Num_Obj_Fn(          zz, Get_Number_of_Objects,   NULL);  // ZOLTAN_NUM_OBJ_FN   ::   Number of items on processor
    Zoltan_Set_Obj_List_Fn(         zz, Get_Object_List,         NULL);  // ZOLTAN_OBJ_LIST_FN  ::   List of item IDs and weights
    // Geometric Query Functions
    Zoltan_Set_Num_Geom_Fn(         zz, Get_Num_Geometry,        NULL);  // ZOLTAN_NUM_GEOM_FN  ::   Dimensionality of domain
    Zoltan_Set_Geom_Multi_Fn(       zz, Get_Geometry_List,       NULL);  // ZOLTAN_GEOM_FN      ::   Coordinates of items


  } else if ( Partitioning_Method == 4 ) { // ------ Hilbert Space Filling Curve (Geometric - HSFC) -------
    
    Zoltan_Set_Param(zz, "LB_METHOD", "HSFC");           // Hilbert Space Filling Curve 

    //Zoltan_Set_Param(zz, "KEEP_CUTS", "1");              // save decomposition
    Zoltan_Set_Param(zz, "REDUCE_DIMENSIONS", "1");      // reduce dimensionality of partition 
    Zoltan_Set_Param(zz, "DEGENERATE_RATIO", "20");      // determines aspect ratio to consider "flat" if REDUCE_DIMENSIONS 

    /* Default Values: 
       KEEP_CUTS                 =  0
       REDUCE_DIMENSIONS         =  0
       DEGENERATE_RATIO          =  10 
    */


    /* Query Functions - defined in wlt3D_Partitioning__Queries.h */
    // General Query Functions 
    Zoltan_Set_Num_Obj_Fn(          zz, Get_Number_of_Objects,   NULL);  // ZOLTAN_NUM_OBJ_FN   ::   Number of items on processor
    Zoltan_Set_Obj_List_Fn(         zz, Get_Object_List,         NULL);  // ZOLTAN_OBJ_LIST_FN  ::   List of item IDs and weights
    // Geometric Query Functions
    Zoltan_Set_Num_Geom_Fn(         zz, Get_Num_Geometry,        NULL);  // ZOLTAN_NUM_GEOM_FN  ::   Dimensionality of domain
    Zoltan_Set_Geom_Multi_Fn(       zz, Get_Geometry_List,       NULL);  // ZOLTAN_GEOM_FN      ::   Coordinates of items
  } else {
    
    *FromFortran__error_out = 1;
    return;
  }





  /******************************************************************
   ** Zoltan can now partition the vertices.
   ** 
   ** Our Assumption :: 
   **                     Number of Partitions = Number of Processes.  
   ** 
   ******************************************************************/

  rc = Zoltan_LB_Partition(zz,                 /* input (all remaining fields are output) */
			   &changes,              /* 1 if partitioning was changed, 0 otherwise */ 
			   &numGidEntries,           /* Number of Integers   used for a   Global ID */
			   &numLidEntries,           /* Number of Integers   used for a   Local ID */
			   &numImport,               /* Number of vertices to be sent to me */
			   &importGlobalGids,     /* Global IDs of vertices   to be sent to   me */
			   &importLocalGids,      /* Local  IDs of vertices   to be sent to   me */
			   &importProcs,             /* Process rank   for source of   each Incoming Vertex */
			   &importToPart,            /* New Partition  for each             Incoming vertex */
			   &numExport,            /* Number of Vertices           I must send to   other Processes*/
			   &exportGlobalGids,     /* Global IDs of the vertices   I must send */
			   &exportLocalGids,      /* Local  IDs of the vertices   I must send */
			   &exportProcs,             /* Process    to which   I send each of the vertices */
			   &exportToPart);           /* Partition  to which          each        vertex  will belong */

  if (rc != ZOLTAN_OK){
    if (rc == ZOLTAN_WARN && Partitioning_Method == 4){    // Warning potentially with HSFC violating domain imbalance, not fatal
      *FromFortran__error_out = 0; 
    } else {
      *FromFortran__error_out = 1;
      return;
  }}

  if (*FromFortran__verb_level)
    for (int j=0; j<numProcs; ++j){
      MPI_Barrier(MPI_COMM_WORLD);
      if (j==myRank){
	printf ("\nmyRank = %d, changes = %d\n",myRank,changes);
	
	printf ("importGlobalGids[%d] = [ ",numImport);
	for (i=0; i<numImport; ++i) printf ("%d ", importGlobalGids[i]); printf ("]\n");
	printf ("importProcs[%d] = [ ",numImport);
	for (i=0; i<numImport; ++i) printf ("%d ", importProcs[i]); printf ("]\n");
	
	printf ("exportGlobalGids[%d] = [ ",numExport);
	for (i=0; i<numExport; ++i) printf ("%d ", exportGlobalGids[i]); printf ("]\n");
	printf ("exportProcs[%d] = [ ",numExport);
	for (i=0; i<numExport; ++i) printf ("%d ", exportProcs[i]); printf ("]\n");
      }
    }
  
  /******************************************************************
   ** Now, Rebalance the problem by 
   **                    Sending the objects to their new partitions. 
   **                    In case of REPARTITION or REFINE only.
   ******************************************************************/
  if (*FromFortran__task) {}




  //
  // sort list of exported IDs - exportGlobalGids[],
  // the list of initial IDs - Vertices_Global_ID[] - is already sorted by construction within each proc limits
  //
  std::sort (exportGlobalGids, exportGlobalGids+numExport);
  
  shift = 0;
  for (i=0; i<numProcs; ++i) {
    
    // write the corrected IDs for the current proc
    //   each processor writes its own part of Global_Processor_Flag,
    //   which is in rank order
    if (i==myRank) {
      for (j=shift; j<shift+Number_Vertices_OnEachProcess[myRank]; ++j) Global_Processor_Flag[j] = myRank;
      if (changes) {
	int i_E = 0;
	j = shift;
	while ( i_E<numExport )
	  if ( Vertices_Global_ID[j++] == exportGlobalGids[i_E] )
	    Global_Processor_Flag[j-1] = exportProcs[i_E++];
      }
    }
    
    // send it to others
    MPI_Bcast (&Global_Processor_Flag[shift], Number_Vertices_OnEachProcess[i], MPI_INT, i, MPI_COMM_WORLD);
    
    // compute the beginning of next proc limits
    shift += Number_Vertices_OnEachProcess[i];
  }
  




#ifdef LOCAL_ZOLTAN_DEBUG
  MPI_Barrier(MPI_COMM_WORLD);       
  if (myRank==0){
    printf ("\ndistribution at %d:\n",myRank);
    printf(" ID = [ ");
    for (j=0; j<Number_Vertices; j++) printf("%5d", Vertices_Global_ID[j]);
    printf ("]\n");
    printf("ini = [ ");
    for (int nv=0, pn=0, sum=Number_Vertices_OnEachProcess[pn]; nv<Number_Vertices; ++nv) {
      if (nv>=sum) sum += Number_Vertices_OnEachProcess[++pn];
      printf("%5d", pn);
    }
    printf ("]\n");
    printf("fin = [ ");	  
    for (j=0; j<Number_Vertices; ++j) printf("%5d", Global_Processor_Flag[j]);
    printf ("]\n\n");
  }



  MPI_Barrier(MPI_COMM_WORLD);  
  if (myRank==0){  
    printf(" Weights\n");
    printf(" =======\n");
  }


  float Total_Weight_Before = 0.0;
  float Total_Weight_After  = 0.0;
  int next;
  
  for (i=0; i<numProcs; i++){
    MPI_Barrier(MPI_COMM_WORLD);
    if (i==myRank){  		  
      
      shift = 0;		  
      for (j=0, shift=0; j<myRank; j++)			  
	shift += Number_Vertices_OnEachProcess[j];		  
      next=shift;

      for (j=0; j<Number_Vertices_OnEachProcess[myRank]; j++) {
	Total_Weight_Before += (float)nwlt_Vertices[ Vertices_Global_ID[next]-1 ];
	next++;
      }

      for (j=0; j<Number_Vertices; j++){	 
	if ( Global_Processor_Flag[j] == myRank )
	  Total_Weight_After += (float)nwlt_Vertices[ Vertices_Global_ID[j]-1 ];
      }

      printf(" myRank : %d\tTotal_Weight_Before : %-7.2f\tTotal_Weight_After : %-7.2f \n",myRank,Total_Weight_Before,Total_Weight_After);

    }
  }


  MPI_Barrier(MPI_COMM_WORLD);  
#endif




  






  /******************************************************************
   ** Free the arrays  allocated by      Zoltan_LB_Partition, and
   ** Free the storage allocated for the Zoltan structure.
   ******************************************************************/

  Zoltan_LB_Free_Part(&importGlobalGids, 
		      &importLocalGids, 
		      &importProcs, 
		      &importToPart);
  
  Zoltan_LB_Free_Part(&exportGlobalGids, 
		      &exportLocalGids, 
		      &exportProcs, 
		      &exportToPart);

  Zoltan_Destroy(&zz);
}


#ifdef LOCAL_ZOLTAN_DEBUG
#undef LOCAL_ZOLTAN_DEBUG
#endif
