/*----------------------------------------------------------------*/
/*     Tree Structured Database                                   */
/*                                                                */
/*     Developed by Alexei Vezolainen                             */
/*                                                                */
/*     Please respect all the time and work                       */
/*     that went into the development of the code.                */
/*                                                                */
/*----------------------------------------------------------------*/
#ifndef DATABASE_TLL_TREE_H
#define DATABASE_TLL_TREE_H

/* a simplified version of visualization tree */
#include <stdio.h>
#include <vector>
#include "tree.h"       // DIM_MAX

class Tll_simple {
private:
  int _p;        // child or data pointer
  int _is_data;
public:
  inline bool is_data () const { return _is_data; }
  inline void set_data ( const bool _s ) { _is_data = _s; }
  inline void set_p ( const int _i ) { _p = _i; }
  inline int get_p () const { return _p; }
};

typedef Tll_simple Tll;

/* struct Bll { */
/*   float b [CHNUM]; */
/* }; */

class Tll_tree {
 public:
/*   Tll_tree (const char*, const char*, const char*); // tree_info  tree_cell  plot3d_data */
/*   Tll_tree (const char*, const char*);              // _t.dat  _d.dat */
  Tll_tree (const std::vector< std::vector<bool> >&,
	    const std::vector< int >&,
	    const std::vector< int >&,
	    const int, const bool allocate_iblock=0);
  ~Tll_tree ();
/*   void print_tree () const; */
/*   void print_data () const; */
/*   void write_tree (char*, char*) const; */
  
  int get_cell ( const int&, const int* );                      // the grid is uniform
/*   int get_cell ( const int&, const int*, const int&,            // general case */
/* 		 const std::vector< float >&, */
/* 		 const std::vector< std::vector<float> >&); */
  
/*   int dim;               // dimension */
/*   int m[3];              // Mx, My, Mz */
/*   std::vector <int> nol; // number of nodes of different levels */
  Tll *cell;             // AMR tree
  int *iblock;           // AMR tree block for rendering (of size nd*chnum)
  float *fblock;         // AMR tree block for rendering (of size nd*chnum)
/*   Bll *block;            // AMR tree data, for rendering */
  int nd;                // data storage length (number of data cells)
  int nolt;              // tree storage length (number of cells)
  int chnum;             // number of children
 private:
  int jmax;              // max number of subdivisions
  int dimension;         // dimension

/*   float *data;           // AMR tree data, real */
/*   int np;                // real data storage length (number of points) */
/*   void err (const char c[], const char c1[] = "") const; */
/*   void set_statistics (); */
/*   void print_real_data () const; */
};


#endif /* DATABASE_TLL_TREE_H */
