/*----------------------------------------------------------------*/
/*     Tree Structured Database                                   */
/*                                                                */
/*     Developed by Alexei Vezolainen                             */
/*                                                                */
/*     Please respect all the time and work                       */
/*     that went into the development of the code.                */
/*                                                                */
/*----------------------------------------------------------------*/
#ifndef DATABASE_INTERFACE_H
#define DATABASE_INTERFACE_H

// uppend underscore or set lowercase if necessary
#include "lowercase.h"

#include "tree.h"

/* #if TREE_VERSION == 1 */
/* #include "tree1.h" */
/* #else */
/* #include "tree.h" */
/* #endif */

extern "C" 
{
  /*======================================================================*/
  /*===  general                                                       ===*/
  /*======================================================================*/

  /*----------------------------------------------------------------------
    Initialization of the database.
    m[dim] - number of points at the coarsest level, which is level 1
    p[dim] - periodicity marker (1 - periodic)
    j_tree - tree roots are the nodes of that level
    j_max  - maximum level of the mesh
    dim    - space dimension of the mesh (1,2,3,etc)
    f_num  - the size of function vector
    if_num - the size of integer function vector
    p_size - total number of processors
    p_rank - rank of the current processor
    proc   - which processor each tree belongs to */
  void DB_DECLARE_GLOBALS (const int* m, const int* p,
			   const int* j_tree, const int* j_max,
			   const int* dim, const int* f_num, const int* if_num,
			   const int* p_size, const int* p_rank, const int*const procs);
  
  /*----------------------------------------------------------------------
    Returns the value of the parameter */
  void DB_GET_N_VALUES (int* n_vals );
  /*----------------------------------------------------------------------
    Cleans the memory occupied by database */
  void DB_FINALIZE ();
  /*----------------------------------------------------------------------
    Redistribute the nodes in type-level lists
    to make sure that all "significant" and "adjacent" are in one list 
    and all "ghost" are in another */
  void DB_UPDATE_TYPE_LEVEL ();
  /*----------------------------------------------------------------------
    Move nodes with 0 (or OLD) id from sig/adj type-level list to ghost list */
  void DB_MOVE_ZERO_SIG_TO_GHO ();
  /*----------------------------------------------------------------------
    Clean the nodes marked by ID==to_be_deleted */
  void DB_UPDATE_DB ();
  /*----------------------------------------------------------------------
    Write the database into the file */
  void DB_WRITE_DB ();
  /*----------------------------------------------------------------------
    Read the database from the file and initialize the database */
  void DB_READ_DB ();
  /*----------------------------------------------------------------------
    Extend the grid to an AMR grid by adding extra nodes */
  void DB_MAKE_AMR ();
  /*----------------------------------------------------------------------
    Write the AMR grid into a file in VTK format
    (readable by Paraview or VisIt) for 2 or 3 dimensional case,
    and into a simple text file (readable by XMGR) for 1D.
    DB_MAKE_AMR() assumed to be called before that function
    unless SIMPLE_AMR has been defined in amr.h
    c     - file name
    clen  - length of the file name
    ft    - file type, 'b' or 'a' for binary and ASCI respectively
    u_t   - use tetrahedra if (u_t) is 1, use blocks if it is 0. */
  void DB_AMR_VTK_WRITE (const char* c, const int* const clen, const char* ft, const int* u_t);
  /*----------------------------------------------------------------------
    Write the AMR grid into a file in Ensight Gold format
    (readable by Ensight Gold) for 2 or 3 dimensional case,
    and into a simple text file (readable by XMGR) for 1D.
    DB_MAKE_AMR() assumed to be called before that function
    unless SIMPLE_AMR has been defined in amr.h
    c     - file name
    clen  - length of the file name
    ft    - file type, 'b' or 'a' for binary and ASCI respectively
    u_t   - use tetrahedra if (u_t) is 1, use blocks if it is 0. */
  void DB_AMR_ENSGHT_WRITE (const char* c, const int* const clen, const char* ft, const int* u_t);
  /*----------------------------------------------------------------------
    Write the comment line for AMR output file (less than 256 characters).
    May be called once or any number of times.
    c    - comment
    clen - length of the comment
    ir   - number of real variables to print into vtk/cgns/plot3d/xmgr file
    ii   - number of integer variables to print into vtk/cgns/plot3d/xmgr file  */
  void DB_WRITE_AMR_COMMENT (const char* c, const int* const clen, const int* const ir, const int* const ii);
  /*----------------------------------------------------------------------
    Write the AMR grid into a file in plot3d format
    (readable by Tecplot) for 2 or 3 dimensional case,
    and into a simple text file (readable by XMGR) for 1D.
    DB_MAKE_AMR() assumed to be called before that function
    unless SIMPLE_AMR has been defined in amr.h
    c     - file name
    clen  - length of the file name
    ft    - file type, 'b' or 'a' for binary and ASCI respectively
    u_t   - use tetrahedra if (u_t) is 1, use blocks if it is 0. */
  void DB_AMR_PLOT3D_WRITE (const char* c, const int* const clen, const char* ft, const int* u_t);
  /*----------------------------------------------------------------------
    Write the AMR grid into a file in CGNS format
    (readable by Tecplot) for 2 or 3 dimensional case,
    and into a simple text file (readable by XMGR) for 1D.
    c     - file name
    clen  - length of the file name
    ft    - file type. It is not used and set to binary.
    u_t   - use tetrahedra if (u_t) is 1, use blocks if it is 0. */
  void DB_AMR_CGNS_WRITE (const char* c, const int* const clen, const char* ft, const int* u_t);
  /*----------------------------------------------------------------------
    Set coordinate vector, similar to xx(0:MAXVAL(nxyz),1:dim)
    for a fixed dimension dim <-> xx(0:len,dim)
    c_i    - marker: 1(integer coordinates) 0(real)
    len    - first size of coordinate vector xx
    dim    - second size of coordinate vector xx
    xx     - coordinate vector xx */
  void DB_AMR_SET_COORDINATES (const int* c_i, const int* len, const int* dim, const Value* xx);
  /*----------------------------------------------------------------------
    Set variable names (to be used in VTK writing)
    nvar   - variable number \in [1..n_var]
    c      - variable name
    clen   - length of the variable name */
  void DB_AMR_SET_VARIABLE_NAME (const int* nvar, const char* c, const int* const clen);
  /*----------------------------------------------------------------------
    ---   MODE==1   ---
    Count/write boundary nodes for the requested trees
    tree_num      - array of (tree number, direction) pairs
    tree_num_size - size
    out_num       - returned array - number of nodes in each tree
    nodes         - returned array - 1D coordinates of the boundary nodes
    values        - returned array - function values of the boundary nodes
    ---   MODE==0   ---
    Count/write nodes for the requested trees
    tree_num      - array of tree numbers
    tree_num_size - size
    out_num       - returned array - number of nodes in each tree
    nodes         - returned array of (1D coordinate, ID) pairs of the boundary nodes
    values        - returned array - function values of the boundary nodes */
  void DB_COUNT_TREE_NODES
  (const int*const tree_num, const int*const tree_num_size, int* out_num, const int*const mode);
  void DB_WRITE_TREE_NODES
  (const int*const tree_num, const int*const tree_num_size, int* out_num, long* nodes, Value* values, const int*const mode);   //!AR 2/27/2011!  int* nodes  changed to  long* nodes
  /*----------------------------------------------------------------------
    Update tree-proc info */
  void DB_UPDATE_PROC_INFO (const int*const procs);
  /*----------------------------------------------------------------------
    Delete nodes marked as if from other processors from link lists */
  void DB_CLEAN_OTHER_PROC_NODES ();
  /*----------------------------------------------------------------------
    Write Nwlt_per_Tree array for the current processor (to be used in load balancing)
    Nwlt_per_Tree     - number of significant+adjacent nodes per tree
    number_of_trees   - number of trees
    err               - zero if no errors */
  void DB_SET_NWLT_PER_TREE (int* Nwlt_per_Tree, const int*const number_of_trees, int* err);
  
  
  /*======================================================================*/
  /*===  access by coordinates                                         ===*/
  /*======================================================================*/
  
  /*----------------------------------------------------------------------
    Add the node (or find a pointer to the existing node)
    set its ID to the given value
    and include into type-level link list system
    all the "significant" and "adjacent" nodes.
    x[dim]  - coordinates, in the units corresponding to node's level
    level   - the level
    id_in   - that bit will be "turned on" in ID of the created node
    front   - add the node to front (1) or back (0) of the link-list
    out     - returned number of nodes amended or created,
    ...         including all new intermediate nodes
    ptr     - returned integer pointer to the node created,
    ...         or existing already, or NULL if error */
  void DB_ADD_NODE (const int*const x, const int*const level, int* out, P_SIZE* ptr,
		    const int*const id_in, const int*const front);
  /*----------------------------------------------------------------------
    Return processor's rank the node belongs to.
    x[dim] - coordinates, in the units corresponding to node's level
    level  - the level
    proc   - returned processor's rank */
  void DB_GET_PROC_BY_COORDINATES (const int* const x, const int *const level, int* proc);
  /*----------------------------------------------------------------------
    Test the presence of a node
    x[dim] - coordinates, in the units corresponding to node's level
    level  - the level
    ptr    - returned pointer to the existing node, or NULL */
  void DB_TEST_NODE (const int*const x, const int*const level, P_SIZE* ptr);
  /*--------------------------------------------------------------------------
    Get vector of coordinates
    p_in   -  stored in FORTRAN pointer
    x      - returned vector of coordinates, in the units of the finest level J_max */
  void DB_GET_COORDINATES_BY_POINTER (const P_SIZE*const p_in, int *x);
  /*--------------------------------------------------------------------------
    Get node ID
    p_in    - stored in FORTRAN pointer
    id_out  - returned integer node ID */
  void DB_GET_ID_BY_POINTER (const P_SIZE*const p_in, int* id_out);
  /*------------------------------------------------------------------------
    Set node ID
    p_in    - stored in FORTRAN pointer
    id_in   - new ID for the node  */
  void DB_SET_ID_BY_POINTER (const P_SIZE*const p_in, const int*const id_in);
  /*----------------------------------------------------------------------
    Get node ID
    x[dim] - coordinates at j_max level
    id_out  - returned integer node ID, which is -1 if node does not exist */
  void DB_GET_ID_BY_JMAX_COORD (const int*const x, int* id_out);
  void DB_GET_ID_BY_JMAX_COORD_OR_ZERO (const int*const x, int* id_out);
  /*----------------------------------------------------------------------
    Get part of the vector of (real or integer) function values for the node
    p_in    - pointer to the node
    i_low   - starting index for the output vector [1:VAL_NUM]
    i_high  - ending index [1:VAL_NUM]
    f_out   - returned vector of function values of size (i_high-i_low+1)
    
    NOTE: DB_get_function_by_pointer() assumes nonzero pointer
    and will crash for zero pointer in normal operational mode,
    it returns zero values for zero pointer in DEBUG_SAFE mode only.
    If zero pointers are expected in normal operational mode,
    use DB_get_function_by_jmax_coordinates() instead of the pair
    DB_test_node & DB_get_function_by_pointer (e.g. in wlt_trns_aux_DB) */
  void DB_GET_FUNCTION_BY_POINTER (const P_SIZE*const p_in, const int*const i_low, const int*const i_high,
				   Value* f_out);
  void DB_GET_IFUNCTION_BY_POINTER (const P_SIZE*const p_in, const int*const i_low, const int*const i_high,
				    int* f_out);
  void DB_GET_FUNCTION_BY_JMAX_COORDINATES (const int*const x, const int*const i_low, const int*const i_high,
					    Value* f_out);
  /*----------------------------------------------------------------------
    Set values for the part of the vector of function values [1:VAL_NUM]
    p_in    - pointer to the node
    i_low   - starting index for the output vector
    i_high  - ending index
    f_in    - input vector of function values of size (i_high-i_low+!) */
  void DB_SET_FUNCTION_BY_POINTER (const P_SIZE*const p_in, const int*const i_low, const int*const i_high,
				   const Value*const f_in);
  void DB_SET_IFUNCTION_BY_POINTER (const P_SIZE*const p_in, const int*const i_low, const int*const i_high,
				    const int*const f_in);
  /*----------------------------------------------------------------------
    Interpolate function values to the given points.
    u         - u(1:nwlt, 1:n_var) - use that array if avm==1
    nwlt      - ...
    n_var     - ...
    du        - du(1:n_var,1:nwlt,1:dim)
    avm       - 0-functions are stored in DB, 1-iwlt is stored in DB as integer
    x         - array of points, FORTRAN x(1:x_dim,1:x_size)
    f         - returned array of interpolated function values, FORTRAN f(1:n_var,1:x_size)
    x_dim     - first size of x array
    x_size    - second size of x and f arrays
    vars      - array of numbers of variables to interpolate for, FORTRAN vars(1:vars_size)
    vars_size - length of the array of numbers of variables, first size of f array
    mode      - 0 - direct interpolation, good for small arrays of x ( general approach )
    ...       - 1 - use preprocessing, good for large arrays ( uniform coordinates only )
    ...       - 2 - use preprocessing, good for large arrays ( general approach )
    order     - order of interpolation (0-linear)
    vtr       - variable # to put into .ren file for the volume renderer
    vtn       - name of the .ren file
    vtl       - length of the .ren file  */
  void DB_INTERPOLATE (const Value*const u, const int*const nwlt, const int*const n_var,
		       const Value*const du, const int*const avm,
		       const Value*const x, Value* f, const int*const x_dim, const int* const x_size,
		       const int*const vars, const int*const vars_size,
		       const int* const mode, const int* const order,
		       const int* const vtr, const char*const vtn, const int* const vtl);
  

  /*----------------------------------------------------------------------
    Return 1 if node is in the list of significant/adjacent points,
    return 0 otherwise. This function to be used from db_tree
    indices subroutine to order wavelets similarly to db_wrk version.
    p_in       - pointer to the node
    out        - the returned output
    ----------------------------------------------------------------------*/
  void DB_TEST_IF_IN_SIG_LIST_BY_POINTER( const P_SIZE* p_in, int* out );
  
  /*----------------------------------------------------------------------
    Remove the node from its link-list
    p_in       - pointer to the node
    ----------------------------------------------------------------------*/
  void DB_REMOVE_FROM_LIST_BY_POINTER (P_SIZE* p_in);
  



  /*======================================================================*/
  /*===  access by level and type                                      ===*/
  /*======================================================================*/
  
  /*----------------------------------------------------------------------
    Access to the nodes of the given type and level.
    proc     - rank of the processor
    type     - node type (1:2^dim)
    level    - node level (1:j_max)
    facetype - node face type (1:3^dim)
    marker   - indicates which type-level list to access
    ...         1 (significant and adjacent nodes only)
    ...         2 (ghost nodes only)
    ...         3 (significant, adjacent, and ghost)
    ...         4 (del only)
    num      - returned number of nodes in the list
    ptr      - returned pointer to the first node in the list */
  void DB_GET_INITIAL_TYPE_LEVEL_NODE (const int*const proc, const int*const type, const int*const level, const int*const facetype,
				       int* num, P_SIZE* ptr, const int*const marker);
  /*----------------------------------------------------------------------
    Access to the nodes of the given type and level.
    proc             - rank of the processor
    type             - node type (1:2^dim)
    level            - node level (1:j_max)
    facetype         - node face type (1:3^dim)
    previous_element - previous node of that type and level
    marker           - indicates which type-level list to access
    ...                  1 (significant and adjacent nodes only)
    ...                  2 (ghost nodes only)
    ...                  3 (significant, adjacent, and ghost)
    next_element     - returned the pointer to the next node from the list
    ...                  of the nodes of given type and level,
    ...                  or NULL for the last element] */
  void DB_GET_NEXT_TYPE_LEVEL_NODE (const int*const proc, const int*const type, const int*const level, const int*const facetype,
				    const P_SIZE* previous_element, P_SIZE* next_element,
				    const int*const marker);
  


}
#endif /* DATABASE_INTERFACE_H */
