/*------------------------------------------------------*/
/*                                                      */
/*   Endian independent I/O library for AWCM code       */
/*   by Alexei Vezolainen                               */
/*   Jan 2007                                           */
/*                                                      */
/*------------------------------------------------------*/
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include <climits>            // CHAR_BIT
#include "io.h"

// debugging makros:
// EBUG, EBUG_WRITE, EBUG_READ
// #define EBUG
// #define EBUG_WRITE
// #define EBUG_READ

awcm_res_io *awcm_res_io_ptr = 0;

namespace data_swap {
  // --------------------------------------------------------------------
  inline r64_t r64swap (r64_t a) {
    union {
      r64_t f;
      ui8_t b[8];
    } dat1, dat2;
    dat1.f = a;
    dat2.b[0] = dat1.b[7];
    dat2.b[1] = dat1.b[6];
    dat2.b[2] = dat1.b[5];
    dat2.b[3] = dat1.b[4];
    dat2.b[4] = dat1.b[3];
    dat2.b[5] = dat1.b[2];
    dat2.b[6] = dat1.b[1];
    dat2.b[7] = dat1.b[0];
    return dat2.f;
  }
  
  // --------------------------------------------------------------------
  inline r32_t r32swap (r32_t a) {
    union {
      r32_t f;
      ui8_t b[4];
    } dat1, dat2;
    dat1.f = a;
    dat2.b[0] = dat1.b[3];
    dat2.b[1] = dat1.b[2];
    dat2.b[2] = dat1.b[1];
    dat2.b[3] = dat1.b[0];
    return dat2.f;
  }
  
  // --------------------------------------------------------------------
  inline i64_t i64swap (i64_t a){
    union {
      i64_t f;
      ui8_t b[8];
    } dat1, dat2;
    dat1.f = a;
    dat2.b[0] = dat1.b[7];
    dat2.b[1] = dat1.b[6];
    dat2.b[2] = dat1.b[5];
    dat2.b[3] = dat1.b[4];
    dat2.b[4] = dat1.b[3];
    dat2.b[5] = dat1.b[2];
    dat2.b[6] = dat1.b[1];
    dat2.b[7] = dat1.b[0];
    return dat2.f;
  }
  
  // --------------------------------------------------------------------
  inline i32_t i32swap (i32_t a){
    ui8_t b1, b2, b3, b4;
    b1 = a & 255;
    b2 = ( a >> 8  ) & 255;
    b3 = ( a >> 16 ) & 255;
    b4 = ( a >> 24 ) & 255;
    return ((i32_t)b1 << 24) + ((i32_t)b2 << 16) + ((i32_t)b3 << 8) + b4;
  }
  
  // --------------------------------------------------------------------
  inline i16_t i16swap (i16_t a){
    ui8_t b1, b2;
    b1 = a & 255;
    b2 = (a >> 8) & 255;
    return (b1 << 8) + b2;
  }
}

#ifdef EBUG
namespace testing_only {
  void print_all () {
    printf ("CHAR_BIT = %d\n",CHAR_BIT);
    printf ("sizeof(short)      = %d\n",sizeof(short));
    printf ("sizeof(int)        = %d\n",sizeof(int));
    printf ("sizeof(long)       = %d\n",sizeof(long));
    printf ("sizeof(long long)  = %d\n",sizeof(long long));
    printf ("sizeof(float)      = %d\n",sizeof(float));
    printf ("sizeof(double)     = %d\n",sizeof(double));
    printf ("sizeof(long double)= %d\n\n",sizeof(long double));
    printf ("sizeof(int8_t) = %d\n",sizeof(int8_t));
    printf ("sizeof(int16_t)= %d\n",sizeof(int16_t));
    printf ("sizeof(int32_t)= %d\n",sizeof(int32_t));
    printf ("sizeof(int64_t)= %d\n",sizeof(int64_t));
    printf ("sizeof(void*)= %d\n",sizeof(void*));
  }
}
#endif

// --------------------------------------------------------------------------------------------------------
awcm_res_io::awcm_res_io () :
  FILE_IS_OPEN(0),
  header_c(0), header_i(0), header_len(0), header_cur(0),
  header_c_loaded(0), header_i_loaded(0), header_len_loaded(0), header_cur_loaded(0) {

#ifdef EBUG
  testing_only::print_all ();
#endif

  // initialize IS_BIG_ENDIAN
  i16_t word = 0x0001;
  ui8_t *byte = (ui8_t *) &word;
  IS_BIG_ENDIAN = ! byte[0];
  
  i32_t errors = 0;
  // check char size
  if (CHAR_BIT != 8) {
    fprintf (stderr, "Error: size of 8 bit char is %d\n",CHAR_BIT);
    fprintf (stderr, "You will have to port io.h and io.cc to your system by yourself\n");
    errors ++;
  }

  // check integer size
  if (sizeof(i64_t) != 8) {
    fprintf (stderr, "Error: size of 8 byte integer is %zu\n",sizeof(i64_t));
    errors ++;
  }
  if (sizeof(i32_t) != 4) {
    fprintf (stderr, "Error: size of 4 byte integer is %zu\n",sizeof(i32_t));
    errors ++;
  }
  if (sizeof(i16_t) != 2) {
    fprintf (stderr, "Error: size of 2 byte integer is %zu\n",sizeof(i16_t));
    errors ++;
  }
  if (sizeof(i8_t) != 1) {
    fprintf (stderr, "Error: size of 1 byte integer is %zu\n",sizeof(i8_t));
    errors ++;
  }
  if (sizeof(ui8_t) != 1) {
    fprintf (stderr, "Error: size of 1 byte unsigned integer is %zu\n",sizeof(i8_t));
    errors ++;
  }

  // check float and double sizes
  if ((sizeof(r32_t) != sizeof(int32_t)) || (sizeof(r64_t) != sizeof(int64_t))) {
    if (sizeof(r32_t) != sizeof(int32_t))
      fprintf (stderr, "Error: size of 2 byte real r32_t is %zu\n",sizeof(r32_t));
    if (sizeof(r64_t) != sizeof(int64_t))
      fprintf (stderr, "Error: size of 4 byte real r64_t is %zu\n",sizeof(r64_t));
    fprintf (stderr, "Consider changing typedef's in the file io.h:10-11\n");
    errors ++;
  }

  // initialize native general header
  snprintf( header_g.part.MAGIC_STRING, sizeof(header_g.part.MAGIC_STRING), "%s", RES_MAGIC_STRING );
  snprintf( header_g.part.VERSION, sizeof(header_g.part.VERSION), "%d", static_cast<int>(RES_VERSION) );
  snprintf( header_g.part.IS_BIG, sizeof(header_g.part.IS_BIG), "%d", static_cast<int>(IS_BIG_ENDIAN) );
  snprintf( header_g.part.SIZEOF_I8, sizeof(header_g.part.SIZEOF_I8),"%d", static_cast<int>(sizeof(i8_t)) );
  snprintf( header_g.part.SIZEOF_I16, sizeof(header_g.part.SIZEOF_I16), "%d", static_cast<int>(sizeof(i16_t)) );
  snprintf( header_g.part.SIZEOF_I32, sizeof(header_g.part.SIZEOF_I32), "%d", static_cast<int>(sizeof(i32_t)) );
  snprintf( header_g.part.SIZEOF_I64, sizeof(header_g.part.SIZEOF_I64), "%d", static_cast<int>(sizeof(i64_t)) );
  snprintf( header_g.part.SIZEOF_FLOAT, sizeof(header_g.part.SIZEOF_FLOAT), "%d", static_cast<int>(sizeof(r32_t)) );
  snprintf( header_g.part.SIZEOF_DOUBLE, sizeof(header_g.part.SIZEOF_DOUBLE), "%d", static_cast<int>(sizeof(r64_t)) );
  
#ifdef EBUG
  printf( "\tMAGIC_STRING  = %s\n", header_g.part.MAGIC_STRING );
  printf ("\tRES_VERSION   = %s\n", header_g.part.VERSION);
  printf ("\tIS_BIG_ENDIAN = %s\n", header_g.part.IS_BIG);
  printf ("\tsizeof(i8_t)  = %s\n", header_g.part.SIZEOF_I8);
  printf ("\tsizeof(i16_t) = %s\n", header_g.part.SIZEOF_I16);
  printf ("\tsizeof(i32_t) = %s\n", header_g.part.SIZEOF_I32);
  printf ("\tsizeof(i64_t) = %s\n", header_g.part.SIZEOF_I64);
  printf ("\tsizeof(float) = %s\n", header_g.part.SIZEOF_FLOAT);
  printf ("\tsizeof(double)= %s\n", header_g.part.SIZEOF_DOUBLE);
#endif
  
  // test for errors
  if (errors) exit (1);
}

// --------------------------------------------------------------------------------------------------------
awcm_res_io::~awcm_res_io () {
  if (header_c) free (header_c);
  if (header_i) free (header_i);
  if (header_c_loaded) free (header_c_loaded);
  if (header_i_loaded) free (header_i_loaded);
}
 
// --------------------------------------------------------------------------------------------------------
// compare the info to that one from variable header
// called from RES_READ
void awcm_res_io::compare_to_header (const unsigned char c, const i64_t& i, const i64_t& i2) {
  // check if fread was OK
  if (i != i2) {
    fprintf (stderr, "RES_READ: fread error: file is empty or corrupted\n");
    exit (1);
  }
  if ( header_c_loaded [header_cur_loaded] != c ) {
    fprintf (stderr, "RES_READ: requested type %c is different from stored in the file %c\n",
	     c, header_c_loaded [header_cur_loaded]);
    exit (1);
  }
  if ( header_i_loaded [header_cur_loaded] != i ) {
    fprintf (stderr, "RES_READ: requested number of elements %d is different from stored in the file %d\n",
	     static_cast<i32_t>(i), static_cast<i32_t>(header_i_loaded [header_cur_loaded]));
    exit (1);
  }
  header_cur_loaded ++;
}

// --------------------------------------------------------------------------------------------------------
// add info to variable header
// called from RES_WRITE
void awcm_res_io::add_header (const unsigned char c, const i64_t& i, const i64_t& i2) {
  // check if fwrite was OK
  if (i != i2) {
    fprintf (stderr, "RES_WRITE: fwrite error\n");
    exit (1);
  }
  header_c [header_cur] = c;
  header_i [header_cur] = i;
  header_cur ++;
  if (header_cur == header_len) {
    
    unsigned char *tmp_c = (unsigned char*) malloc (sizeof(unsigned char)*(header_len + INITIAL_HDR_LEN));
    i64_t * tmp_i = (i64_t*) malloc (sizeof(i64_t)*(header_len + INITIAL_HDR_LEN));
    if (( ! tmp_c )||( ! tmp_i )) {
      fprintf (stderr, "RES_WRITE: variable header allocation error\n");
      exit (1);
    }
    memcpy ( tmp_c, header_c, header_len );
    memcpy ( tmp_i, header_i, header_len*sizeof(i64_t) );
    free (header_c);
    free (header_i);
    header_c = tmp_c;
    header_i = tmp_i;
    header_len += INITIAL_HDR_LEN;
    
#ifdef EBUG
    printf ("\t variable headers have been reallocated to %d\n",static_cast<i32_t>(header_len));
#endif
  }
}

// --------------------------------------------------------------------------------------------------------
// called from RES_OPEN
i32_t awcm_res_io::rw_header (const unsigned char c) {
  if (c == 'w') {
    OPEN_FOR_WRITE = 1;
    if ( 1 != fwrite ( header_g.all, sizeof(union header_g_t), 1, f ))       // write native general header
      return 2;
    header_after = ftell ( f );                                              // mark the position after
    if ( 0 > header_after ) {
      perror ("RES_OPEN: ftell error");
      return 2;
    }
    if ( 1 != fwrite ( &header_after, sizeof(i64_t), 1, f ))                 // write position place holder
      return 2;
    if ( 1 != fwrite ( &header_len, sizeof(i64_t), 1, f ))                   // write header length place holder
      return 2;
#ifdef EBUG
    printf ("\tposition place holder = %d\n", static_cast<i32_t>(header_after));
    printf ("\theader length place holder = %d\n", static_cast<i32_t>(header_len));
#endif
    
    header_c = (unsigned char*) malloc (sizeof(unsigned char)*INITIAL_HDR_LEN); // initialize variable headers
    header_i = (i64_t*) malloc (sizeof(i64_t)*INITIAL_HDR_LEN);
    if (( ! header_c )||( ! header_i ))
      return 5;
    
    header_len = INITIAL_HDR_LEN;
    
  } else {
    OPEN_FOR_WRITE = 0;
    if ( 1 != fread ( header_g_loaded.all, sizeof(union header_g_t), 1, f ))  // read file's general header
      return 1;
    if ( strncmp( header_g_loaded.part.MAGIC_STRING,
		  RES_MAGIC_STRING,
		  RES_MAGIC_STRING_LEN ))
      return 3;
    
    i64_t header_position;
    if ( 1 != fread ( &header_position, sizeof(i64_t), 1, f ))               // read header position
      return 1;
    if ( 1 != fread ( &header_len_loaded, sizeof(i64_t), 1, f ))             // read header length
      return 1;
    i64_t data_start_position = ftell ( f );
    if ( 0 > data_start_position ) {
      perror ("RES_OPEN: ftell error");
      return 1;
    }
    if ( (bool) atoi ( header_g_loaded.part.IS_BIG ) == IS_BIG_ENDIAN )
      SWAP_FILE_DATA = 0;
    else {
      SWAP_FILE_DATA = 1;
      header_position = data_swap::i64swap(header_position);       // swap values for header
      header_len_loaded = data_swap::i64swap(header_len_loaded);   // position and length
    }
#ifdef EBUG
    printf ("\tloaded header:\n");
    printf ("\tMAGIC_STRING  = %s\n", header_g_loaded.part.MAGIC_STRING);
    printf ("\tRES_VERSION   = %s\n", header_g_loaded.part.VERSION);
    printf ("\tIS_BIG_ENDIAN = %s\n", header_g_loaded.part.IS_BIG);
    printf ("\tsizeof(i8_t)    = %s\n", header_g_loaded.part.SIZEOF_I8);
    printf ("\tsizeof(i16_t)   = %s\n", header_g_loaded.part.SIZEOF_I16);
    printf ("\tsizeof(i32_t)   = %s\n", header_g_loaded.part.SIZEOF_I32);
    printf ("\tsizeof(i64_t)   = %s\n", header_g_loaded.part.SIZEOF_I64);
    printf ("\tsizeof(float) = %s\n", header_g_loaded.part.SIZEOF_FLOAT);
    printf ("\tsizeof(double)= %s\n", header_g_loaded.part.SIZEOF_DOUBLE);
    printf ("\tloaded variable header:\n");
    printf ("\theader position = 0x%x\n", static_cast<i32_t>(header_position));
    printf ("\theader length = 0x%x\n", static_cast<i32_t>(header_len_loaded));
    printf ("\tSWAP_FILE_DATA = %d\n",SWAP_FILE_DATA);
#endif

    if (header_c_loaded) free (header_c_loaded);             // clean previously allocated headers
    if (header_i_loaded) free (header_i_loaded);             // if it is not a first reading
    header_cur_loaded = 0;                                         // used in compare_to_header ()
    header_c_loaded = (unsigned char*) malloc (sizeof(unsigned char)*header_len_loaded);
    header_i_loaded = (i64_t*) malloc (sizeof(i64_t)*header_len_loaded);
    if (( !header_c_loaded )||( ! header_i_loaded ))               // initialize variable headers
      return 5;
    
    if (fseek ( f, header_position, SEEK_SET )) {                  // read variable headers
      perror ("RES_OPEN: fseek error");
      return 4;
    }
    if ( header_len_loaded != fread ( header_c_loaded, sizeof(unsigned char), header_len_loaded, f ))
      return 4;
    if ( header_len_loaded != fread ( header_i_loaded, sizeof(i64_t), header_len_loaded, f ))
      return 4;
    if (SWAP_FILE_DATA)                                            // swap values for integer header
      for (i64_t n=0; n<header_len_loaded; ++n)
	header_i_loaded[n] = data_swap::i64swap(header_i_loaded[n]);
    
    if (fseek ( f, data_start_position , SEEK_SET )) {             // set file pointer to data
      perror ("RES_OPEN: fseek error");
      return 4;
    }
#ifdef EBUG
    printf ("\theader_c_loaded [%d of %d] = '",static_cast<i32_t>(header_len_loaded),static_cast<i32_t>(header_len_loaded));
    i64_t i;
    for ( i=0; i<header_len_loaded; ++i )
      printf ("%c",header_c_loaded[i]);
    printf ("'\n\theader_i_loaded [%d of %d] = ",static_cast<i32_t>(header_len_loaded),static_cast<i32_t>(header_len_loaded));
    for ( i=0; i<header_len_loaded; ++i )
      printf ("%d ",static_cast<i32_t>(header_i_loaded[i]));
    printf ("\n");
    printf ("\tSWAP_FILE_DATA = %d\n",SWAP_FILE_DATA);
#endif
  }

  FILE_IS_OPEN = 1;   // set the flag that I/O file has been opened
  return 0;           // no errors
}

// --------------------------------------------------------------------------------------------------------
// called from RES_CLOSE
i32_t awcm_res_io::w_header () {
  if (OPEN_FOR_WRITE) {
    i64_t header_position = ftell ( f );                             // variable header position
    if ( 0 > header_position ) {
      perror ("RES_CLOSE: ftell error");
      return 1;
    }
    if ( header_cur != fwrite ( header_c, sizeof(unsigned char), header_cur, f ))       // write info headers
      return 1;
    if ( header_cur != fwrite ( header_i, sizeof(i64_t), header_cur, f ))
      return 1;
    if (fseek ( f, header_after, SEEK_SET )) {                             // set to header_after position
      perror ("RES_CLOSE: fseek error");
      return 1;
    }
    if ( 1 != fwrite ( &header_position, sizeof(i64_t), 1, f ))                // write position of variable header
      return 1;
    if ( 1 != fwrite ( &header_cur, sizeof(i64_t), 1, f ))                     // write header length
      return 1;
#ifdef EBUG
    printf ("\theader position = 0x%x\n", static_cast<i32_t>(header_position));
    printf ("\theader length = 0x%x\n", static_cast<i32_t>(header_cur));
    printf ("\theader_c [%d of %d] = '",static_cast<i32_t>(header_cur),static_cast<i32_t>(header_len));
    i64_t i;
    for ( i=0; i<header_cur; ++i )
      printf ("%c",header_c[i]);
    printf ("'\n\theader_i [%d of %d] = ",static_cast<i32_t>(header_cur),static_cast<i32_t>(header_len));
    for ( i=0; i<header_cur; ++i )
      printf ("%d ", static_cast<i32_t>(header_i[i]));
    printf ("\n");
#endif
  }

  FILE_IS_OPEN = 0;          // set flag that I/O file has been closed
  return 0;                  // no errors
}

// --------------------------------------------------------------------------------------------------------
// c   - data format: (I1,I2,I4,R4,R8)
// d   - data
// the corresponding types are: (i8_t, i16_t, i32_t, i64_t, r32_t, r64_t)
// and the markers are: (c, s, i, l, f, d)
void RES_WRITE (const unsigned char*c, const void*d, const i32_t*n) {
#ifdef EBUG_WRITE
  const int MAX_PRINT = 15;    // print first MAX_PRINT entries of arrays
  printf ("RES_WRITE ()\n");
#endif
  if (( ! awcm_res_io_ptr )||( ! awcm_res_io_ptr->FILE_IS_OPEN )) {
    fprintf (stderr, "RES_WRITE: file has to be opened first\n");
    exit (1);
  }
  if ( ! awcm_res_io_ptr->OPEN_FOR_WRITE ) {
    fprintf (stderr, "RES_WRITE: file has to be opened for writing\n");
    exit (1);
  }
  if ((c[0] == 'i')||(c[0] == 'I') ) {
    switch (c[1]) {
    case '1':
      {
	awcm_res_io_ptr->add_header ('c', *n, fwrite ( d, sizeof(i8_t), *n, awcm_res_io_ptr->f));
#ifdef EBUG_WRITE
	printf ("\tc[%d] %d\n", *n, (i32_t) *(i8_t*) d);
#endif
	return;
      }
    case '2':
      {
	awcm_res_io_ptr->add_header ('s', *n, fwrite ( d, sizeof(i16_t), *n, awcm_res_io_ptr->f));
#ifdef EBUG_WRITE
	printf ("\ts[%d] %d\n", *n, *(i16_t*) d);
#endif
	return;
      }
    case '4':
      {
	awcm_res_io_ptr->add_header ('i', *n, fwrite ( d, sizeof(i32_t), *n, awcm_res_io_ptr->f));
#ifdef EBUG_WRITE
	int i = 0;
	i32_t *ptr = (i32_t*) d;
	printf ("\ti[%d] =", *n);
	while ((i<*n)&&(i<MAX_PRINT)) {
	  printf (" %d", *ptr);
	  ++i; ++ptr;
	}
	printf ("\n");
#endif
	return;
      }
    case '8':
      {
	awcm_res_io_ptr->add_header ('l', *n, fwrite ( d, sizeof(i64_t), *n, awcm_res_io_ptr->f));
#ifdef EBUG_WRITE
	printf ("\tl[%d] %ld\n", *n, static_cast<i32_t>(*(i64_t*) d));
#endif
	return;
      }
    default:
      fprintf (stderr, "RES_WRITE: unknown format 'I%c'\n",c[1]);
      exit (1);
    }
  } else if ((c[0] == 'r')||(c[0] == 'R') ) {
    switch (c[1]) {
    case '4':
      {
	awcm_res_io_ptr->add_header ('f', *n, fwrite ( d, sizeof(r32_t), *n, awcm_res_io_ptr->f));
#ifdef EBUG_WRITE
	printf ("\tf[%d] %e\n", *n, *(r32_t*) d);
#endif
	return;
      }
    case '8':
      {
	awcm_res_io_ptr->add_header ('d', *n, fwrite ( d, sizeof(r64_t), *n, awcm_res_io_ptr->f));
#ifdef EBUG_WRITE
	printf ("\td[%d] %e\n", *n, *(r64_t*) d);
#endif
	return;
      }
    default:
      fprintf (stderr, "RES_WRITE: unknown format 'R%c'\n",c[1]);
      exit (1);
    }
  } else {
    fprintf (stderr, "RES_WRITE: unknown format '%c'\n",c[0]);
    exit (1);
  }
}

// --------------------------------------------------------------------------------------------------------
// c   - data format: (I1,I2,I4,R4,R8)
// d   - data
// the corresponding types are: (i8_t, i16_t, i32_t, i64_t, r32_t, r64_t)
// and the markers are: (c, s, i, l, f, d)
void RES_READ (const unsigned char*c, void*d, const i32_t*n) {
#ifdef EBUG_READ
  const int MAX_PRINT = 15;    // print first MAX_PRINT entries of arrays
  printf ("RES_READ ()\n");
#endif
  if (( ! awcm_res_io_ptr )||( ! awcm_res_io_ptr->FILE_IS_OPEN )) {
    fprintf (stderr, "RES_WRITE: file has to be opened first\n");
    exit (1);
  }
  if ( awcm_res_io_ptr->OPEN_FOR_WRITE ) {
    fprintf (stderr, "RES_WRITE: file has to be opened for reading\n");
    exit (1);
  }
  if ((c[0] == 'i')||(c[0] == 'I') ) {
    switch (c[1]) {
    case '1':
      {
	awcm_res_io_ptr->compare_to_header ('c', *n, fread ( d, sizeof(i8_t), *n, awcm_res_io_ptr->f));
#ifdef EBUG_READ
	int i = 0;
	i8_t *ptr = (i8_t*) d;
	printf ("\tc[%d] = '", *n);
	while ((i<*n)&&(i<MAX_PRINT)) {
	  printf ("%c", *ptr);
	  ++i, ++ptr;
	}
	printf ("'\n");
#endif
	return;
      }
    case '2':
      {
	awcm_res_io_ptr->compare_to_header ('s', *n, fread ( d, sizeof(i16_t), *n, awcm_res_io_ptr->f));
	i16_t *ptr = (i16_t*) d;               // pointer to the first array's element
	if (awcm_res_io_ptr->SWAP_FILE_DATA)
	  for (int i=0; i<*n; ++i, ++ptr)
	    *ptr = data_swap::i16swap (*ptr);
#ifdef EBUG_READ
	printf ("\ts[%d] %d\n", *n, *(i16_t*) d);
#endif
	return;
      }
    case '4':
      {
	awcm_res_io_ptr->compare_to_header ('i', *n, fread ( d, sizeof(i32_t), *n, awcm_res_io_ptr->f));
	i32_t *ptr = (i32_t*) d;               // pointer to the first array's element
	if (awcm_res_io_ptr->SWAP_FILE_DATA)
	  for (int i=0; i<*n; ++i, ++ptr)
	    *ptr = data_swap::i32swap (*ptr);
#ifdef EBUG_READ
	int i = 0;
	ptr = (i32_t*) d;
	printf ("\ti[%d] =", *n);
	while ((i<*n)&&(i<MAX_PRINT)) {
	  printf (" %d", *ptr);
	  ++i, ++ptr;
	}
	printf ("\n");
#endif
	return;
      }
    case '8':
      {
	awcm_res_io_ptr->compare_to_header ('l', *n, fread ( d, sizeof(i64_t), *n, awcm_res_io_ptr->f));
	i64_t *ptr = (i64_t*) d;               // pointer to the first array's element
	if (awcm_res_io_ptr->SWAP_FILE_DATA)
	  for (int i=0; i<*n; ++i, ++ptr)
	    *ptr = data_swap::i64swap (*ptr);
#ifdef EBUG_READ
	printf ("\tl[%d] %ld\n", *n, static_cast<i32_t>(*(i64_t*) d));
#endif
	return;
      }
    default:
      fprintf (stderr, "RES_WRITE: unknown format 'I%c'\n",c[1]);
      exit (1);
    }
  } else if ((c[0] == 'r')||(c[0] == 'R') ) {
    switch (c[1]) {
    case '4':
      {
	awcm_res_io_ptr->compare_to_header ('f', *n, fread ( d, sizeof(r32_t), *n, awcm_res_io_ptr->f));
	r32_t *ptr = (r32_t*) d;               // pointer to the first array's element
	if (awcm_res_io_ptr->SWAP_FILE_DATA)
	  for (int i=0; i<*n; ++i, ++ptr)
	    *ptr = data_swap::r32swap (*ptr);
#ifdef EBUG_READ
	printf ("\tf[%d] %f\n", *n, *(r32_t*) d);
#endif
	return;
      }
    case '8':
      {
	awcm_res_io_ptr->compare_to_header ('d', *n, fread ( d, sizeof(r64_t), *n, awcm_res_io_ptr->f));
	r64_t *ptr = (r64_t*) d;               // pointer to the first array's element
	if (awcm_res_io_ptr->SWAP_FILE_DATA)
	  for (int i=0; i<*n; ++i, ++ptr)
	    *ptr = data_swap::r64swap (*ptr);
#ifdef EBUG_READ
	printf ("\td[%d] %f\n", *n, *(r64_t*) d);
#endif
	return;
      }
    default:
      fprintf (stderr, "RES_WRITE: unknown format 'R%c'\n",c[1]);
      exit (1);
    }
  } else {
    fprintf (stderr, "RES_WRITE: unknown format '%c'\n",c[0]);
    exit (1);
  }
}


// --------------------------------------------------------------------------------------------------------
// close the file and clean the memory
// ierr   - returned number of errors
void RES_CLOSE (i32_t* ierr) {
#ifdef EBUG
  printf ("RES_CLOSE ()\n");
#endif
  if ((!awcm_res_io_ptr) || (!awcm_res_io_ptr->FILE_IS_OPEN)) {
    fprintf (stderr, "RES_CLOSE: the file has to be opened first\n");
    exit (1);
  }
  
  // write the header
  *ierr = awcm_res_io_ptr->w_header ();
  
  if (fclose (awcm_res_io_ptr->f)) {
    perror ("RES_CLOSE: fclose error");
    exit (1);
  }

  // clean the memory
  delete awcm_res_io_ptr;
  awcm_res_io_ptr = 0;

  switch (*ierr) {
  case 0:
    break;
  case 1:
    fprintf (stderr, "RES_CLOSE: variable header write error\n");
    break;
  default:
    fprintf (stderr, "RES_CLOSE: unknown error %d\n",*ierr);
    break;
  }
}

// --------------------------------------------------------------------------------------------------------
// open file for reading/writing, initialize headers
// mode   - read/write mode: 'r', 'w'
// c      - file name
// clen   - length of the file name
// ierr   - returned number of errors
void RES_OPEN (const char*mode, const char*c, const i32_t*clen, i32_t* ierr) {
#ifdef EBUG
  printf ("RES_OPEN ()\n");
#endif

  if (( awcm_res_io_ptr )&&( awcm_res_io_ptr->FILE_IS_OPEN )) {
    fprintf (stderr, "RES_OPEN: the file has beed already opened\n");
    exit (1);
  }
  
  awcm_res_io_ptr = new awcm_res_io;
  
  if (!awcm_res_io_ptr) {
    fprintf (stderr, "RES_OPEN: error while initializing\n");
    exit (1);
  }

  // arrange file name
  char* name=new char[*clen+1];
  strncpy ( name, (char*) c, *clen );
  name[*clen]='\0';
#ifdef EBUG
  printf ("\tfile '%s'\n",name);
#endif
  
  // arrange file mode
  char fm[] = " b";
  if ((mode[0] == 'w')||(mode[0] == 'W'))
    fm[0] = 'w';
  if ((mode[0] == 'r')||(mode[0] == 'R'))
    fm[0] = 'r';
  
  // open the file
  if ((awcm_res_io_ptr->f = fopen (name, fm)) == 0) {
    fprintf(stderr,  "RES_OPEN: cannot open the file: %s\n", name);
    perror ("RES_OPEN");
    exit(1);
    *ierr = 1;
    return;
  }
  delete [] name;

  // read/write the general header
  *ierr = awcm_res_io_ptr->rw_header (fm[0]);
  switch (*ierr) {
  case 0:
    break;
  case 1:
    fprintf (stderr, "RES_OPEN: general header read error\n");
    break;
  case 2:
    fprintf (stderr, "RES_OPEN: general header write error\n");
    break;
  case 3:
    fprintf (stderr, "RES_OPEN: strange file format\n");
    break;
  case 4:
    fprintf (stderr, "RES_OPEN: variable header read error\n");
    break;
  case 5:
    fprintf (stderr, "RES_OPEN: variable header allocation error\n");
    break;
  default:
    fprintf (stderr, "RES_OPEN: unknown error %d\n",*ierr);
    break;
  }
  if (*ierr) {
    fclose (awcm_res_io_ptr->f);
    delete awcm_res_io_ptr;
    awcm_res_io_ptr = 0;
  }
}
