/*----------------------------------------------------------------*/
/*     Tree Structured Database                                   */
/*                                                                */
/*     Developed by Alexei Vezolainen                             */
/*                                                                */
/*     Please respect all the time and work                       */
/*     that went into the development of the code.                */
/*                                                                */
/*----------------------------------------------------------------*/
#ifndef DATABASE_AMR_H
#define DATABASE_AMR_H

#include <string>
#include <iterator>
#include <vector>
#include <list>

#if ! defined (_MSC_VER) && ! defined (NETINET_NO)           // it is not MSVC compiler
#define NETINET_PRESENT_AMR     // instead of _NETINET_IN_H flag, which can
#include <netinet/in.h>         // vary (e.g. _NETINET_IN_H_ for IBM)
#endif                      // ------

/* #if (TREE_VERSION == 1) */
/* #include "tree1.h" */
/* #else */
/* #include "tree.h" */
/* #endif */

#include "tree.h"

/* -------------------------------------------------------------------------- */
/*   Define "SIMPLE_AMR" in order to ignore incomplete cells                  */
/*   during AMR output. In this case it is not necessary to call              */
/*   DB_make_amr before printing the output files with                        */
/*   DB_amr_vtk_write, DB_amr_ensght_write, or DB_amr_plot3d_write subroutines                      */
/* -------------------------------------------------------------------------- */
#define SIMPLE_AMR

/* -------------------------------------------------------------------------- */
/*   Define the following makros for some tree related statistics.            */
/*   If your plan is to generate output files in VTK or PLOT3D formats only,  */
/*   you do not need them.                                                    */
/* -------------------------------------------------------------------------- */
//#define WRITE_AMRTREE_FILE

#ifdef WRITE_AMRTREE_FILE
#define AMRTREE_FILE_NAME1 "tll_info"
#define AMRTREE_FILE_NAME2 "tll_cell"
#endif


#define RANDYSNUMBER 2          // Randy's magic number for PLOT3D files
#define SN_MAX 4                // max number of variables is 10^SN_MAX-1
/* ----------------------------------------------------------------------------- */
/*   AMR is dimension independent as well as the tree structure.                 */
/*   Different space dimension produce different output:                         */
/*   1         - in XMGR ASCI format,                                            */
/*               to be used with xmgr or similar software.                       */
/*   2 or 3    - in VTK or PLOT3D unstructured mesh ASCI or binary formats,      */
/*               to be used with ParaView, VisIt, Tecplot or similar software.   */
/*   any other - no output file                                                  */
/* ----------------------------------------------------------------------------- */


class Box {                      // 2-3D VTK cell
 public:
  Node* p[8];
  
  Box (Node* p0, Node* p1, Node* p2, Node* p3) {
    p[0]=p0; p[1]=p1; p[2]=p2; p[3]=p3;
    p[4]=0; p[5]=0; p[6]=0; p[7]=0; }
  
  Box (Node* p0, Node* p1, Node* p2, Node* p3,
       Node* p4, Node* p5, Node* p6, Node* p7) {
    p[0]=p0; p[1]=p1; p[2]=p2; p[3]=p3;
    p[4]=p4; p[5]=p5; p[6]=p6; p[7]=p7;
  }
};

class Record {                   // 1D record
 public:
  int x;                                               // coordinate
  Node * node;                                         // pointer to the node
  Record () {}
  Record (const int x_, Node *n_) : x(x_), node(n_) {}
  inline bool operator< (const Record &r) const {
    return (x < r.x); }
};



class AMR {
 public:
  AMR (Forest*, const Global_variables*);
  ~AMR ();
  void write_amr (const char*, const char*);           // 2,3D VTK
  void write_amr_ensght (const char*, const char*);    // 2,3D ENSIGHT
  void write_amr_plot3d (const char*, const char*);    // 2,3D PLOT3D
  void write_amr_cgns (const char*);                   // 2,3D CGNS
 private:
  void write_amr_xmgr (const char*);                   // 1D XMGR
  void write_amr_1d_vtk (const char*, const char*);    // 1D VTK
  void set_points_1d_vtk (FILE*, const bool);
  void set_cells_1d_vtk (FILE*, const bool);
  void write_amr_1d_cgns (const char*);                // 1D CGNS

 public:
  void set_number_of_vars_to_output (int, int);
  void set_string_header (const char*);
  void set_coord_integer (const int c_) { COORD_INTEGER = (bool) c_; } 
  void set_coord_vector (const int, const int, const Value*);            // set coordinates xx()
  void set_point_data_names (const int, char*);                          // set data names
  void set_param (const int c_) { IS_TETRAHEDRA = (bool) c_; }
  void interpolate (const Value*const, const Value*const, const int, const int,
		    const Value*const, Value*, const int, const int*const, const int, const bool, const int,
		    const char* renname="", const int renvar=-1);

  inline float get_fcoord (int*, int);      // transform integer coordinates into float (for Forest::interpolate)
  //  inline std::vector< std::vector<float> >& get_xx () { return &xx; }
  //std::vector< std::vector<float> > xx;           // similar to xx FORTRAN coordinates
  float* xx[DIM_MAX];

 private:
  bool IS_BIG_ENDIAN;                       // if the system big endian
  bool COORD_INTEGER;                       // coordinates will be integer grid values
  bool IS_TETRAHEDRA;                       // (true) 6 VTK_TETRA, (false) 1 VTK_VOXEL for each AMR cell
  bool GRID_IS_UNIFORM;
  Forest *f;
  const Global_variables *g;

  std::string string_header;
  std::vector <std::string> data_names;
  
  int size;                                  // total number of points
  int cells;                                 // total number of cells
  int val_num, val_num_i;                    // number of real and integer variables to output

  std::list <Box> final_box_list;
  std::list <Box> box_list;
  std::list <Box> next_box_list;
  std::list <Box>::iterator it;
  std::vector< std::vector<bool> > tll_info;
  std::vector< int > tll_info_len;           // tll_info.size() (to save last level from storing)
  std::vector< int > tll_cell;

  void set_points (FILE*, const bool,        // write points and set their vID's
		   const int val_index=-1);
  void set_points_ensght (FILE*, const bool,        // write points and set their vID's
			  const int val_index=-1, int=0);
  void set_cells (FILE*, const bool, const int format=0);          // write cells, VTK by default
  void vtk_print_point (Node*, FILE*, const bool);
  void ensght_print_point (Node*, FILE*, const bool, int);
  void create_box_tree (const bool flag=0);
  void set_point_data (FILE*, const bool);                         // write point data
  void set_point_data_ensght (FILE*, const bool, int);                         // write point data
  void vtk_print_point_data (Node*, FILE*, const bool, const int);
  void ensght_print_point_data (Node*, FILE*, const bool, const int);
  inline float FloatSwap( const float& );
#ifndef NETINET_PRESENT_AMR // netinet/in.h has not been included
  inline int htonl( int );
#endif

  void set_points_plot3d (const int, float*xyz=0, float*var=0, Node**nodes=0, int*inodes=0);
  bool test_presence_and_significance (Node* n[], int length);

  vector <Record> rec;                  // 1D record
  std::vector <Record>::iterator itr;   // 1D record iterator
  void fill_record_vector ();           // 1D record writer
  
  inline float get_coord (const int*const, const int&);      // transform integer coordinates into required format

  int* ielem;                              // CGNS connectivity
  int locate_in_array (const float&, const std::vector<float>&, const int&);
};


#endif /* DATABASE_AMR_H */
