/*
  Use flag -DDATABASE_INTERFACE_LOWERCASE in the correspondent
  makefile in makefiles.machine_specific/ in order to
  trunsform uppercase interface functions to lowercase
  (e.g. for the use with xlf compilers, instead of -U flag for
  the FORTRAN code, which might produce some complications)

  Use flag -DDATABASE_APPEND_UNDERSCORE in the correspondent
  makefile in makefiles.machine_specific/ in order to append
  an underscore to C++ functions
  (e.g. for the use with pgi compilers which seem not to have
  a correspondent flag)
*/
#ifndef DATABASE_INTERFACE_LOWERCASE_H
#define DATABASE_INTERFACE_LOWERCASE_H


#ifdef DATABASE_INTERFACE_LOWERCASE
#ifdef DATABASE_APPEND_UNDERSCORE
/*                                       lowercase, append underscore */
#define DB_ADD_NODE db_add_node_
#define DB_AMR_CGNS_WRITE db_amr_cgns_write_
#define DB_AMR_PLOT3D_WRITE db_amr_plot3d_write_
#define DB_AMR_SET_COORDINATES db_amr_set_coordinates_
#define DB_AMR_SET_VARIABLE_NAME db_amr_set_variable_name_
#define DB_AMR_VTK_WRITE db_amr_vtk_write_
#define DB_AMR_ENSGHT_WRITE db_amr_ensght_write_
#define DB_CLEAN_OTHER_PROC_NODES db_clean_other_proc_nodes_
#define DB_COUNT_TREE_NODES db_count_tree_nodes_
#define DB_DECLARE_GLOBALS db_declare_globals_
#define DB_FINALIZE db_finalize_
#define DB_GET_COORDINATES_BY_POINTER db_get_coordinates_by_pointer_
#define DB_GET_FUNCTION_BY_JMAX_COORDINATES db_get_function_by_jmax_coordinates_
#define DB_GET_FUNCTION_BY_POINTER db_get_function_by_pointer_
#define DB_GET_ID_BY_POINTER db_get_id_by_pointer_
#define DB_GET_ID_BY_JMAX_COORD db_get_id_by_jmax_coord_
#define DB_GET_ID_BY_JMAX_COORD_OR_ZERO db_get_id_by_jmax_coord_or_zero_
#define DB_GET_IFUNCTION_BY_POINTER db_get_ifunction_by_pointer_
#define DB_GET_INITIAL_TYPE_LEVEL_NODE db_get_initial_type_level_node_
#define DB_GET_NEXT_TYPE_LEVEL_NODE db_get_next_type_level_node_
#define DB_GET_N_VALUES db_get_n_values_
#define DB_GET_PROC_BY_COORDINATES db_get_proc_by_coordinates_
#define DB_INTERPOLATE db_interpolate_
#define DB_MAKE_AMR db_make_amr_
#define DB_MAKE_AMR_NOADD db_make_amr_noadd_
#define DB_MOVE_ZERO_SIG_TO_GHO db_move_zero_sig_to_gho_
#define DB_REMOVE_FROM_LIST_BY_POINTER db_remove_from_list_by_pointer_
#define DB_SET_FUNCTION_BY_POINTER db_set_function_by_pointer_
#define DB_SET_ID_BY_POINTER db_set_id_by_pointer_
#define DB_SET_IFUNCTION_BY_POINTER db_set_ifunction_by_pointer_
#define DB_SET_NWLT_PER_TREE db_set_nwlt_per_tree_
#define DB_TEST_IF_IN_SIG_LIST_BY_POINTER db_test_if_in_sig_list_by_pointer_
#define DB_TEST_NODE db_test_node_
#define DB_UPDATE_DB db_update_db_
#define DB_UPDATE_PROC_INFO db_update_proc_info_
#define DB_UPDATE_TYPE_LEVEL db_update_type_level_
#define DB_WRITE_AMR_COMMENT db_write_amr_comment_
#define DB_WRITE_TREE_NODES db_write_tree_nodes_
#define RES_OPEN res_open_
#define RES_WRITE res_write_
#define RES_READ res_read_
#define RES_CLOSE res_close_
#define ZOLTAN_DOMAIN_PARTITIONING zoltan_domain_partitioning_

#else
/*                                       lowercase, without underscore */
#define DB_ADD_NODE db_add_node
#define DB_AMR_CGNS_WRITE db_amr_cgns_write
#define DB_AMR_PLOT3D_WRITE db_amr_plot3d_write
#define DB_AMR_SET_COORDINATES db_amr_set_coordinates
#define DB_AMR_SET_VARIABLE_NAME db_amr_set_variable_name
#define DB_AMR_VTK_WRITE db_amr_vtk_write
#define DB_AMR_ENSGHT_WRITE db_amr_ensght_write
#define DB_CLEAN_OTHER_PROC_NODES db_clean_other_proc_nodes
#define DB_COUNT_TREE_NODES db_count_tree_nodes
#define DB_DECLARE_GLOBALS db_declare_globals
#define DB_FINALIZE db_finalize
#define DB_GET_COORDINATES_BY_POINTER db_get_coordinates_by_pointer
#define DB_GET_FUNCTION_BY_JMAX_COORDINATES db_get_function_by_jmax_coordinates
#define DB_GET_FUNCTION_BY_POINTER db_get_function_by_pointer
#define DB_GET_ID_BY_POINTER db_get_id_by_pointer
#define DB_GET_ID_BY_JMAX_COORD db_get_id_by_jmax_coord
#define DB_GET_ID_BY_JMAX_COORD_OR_ZERO db_get_id_by_jmax_coord_or_zero
#define DB_GET_IFUNCTION_BY_POINTER db_get_ifunction_by_pointer
#define DB_GET_INITIAL_TYPE_LEVEL_NODE db_get_initial_type_level_node
#define DB_GET_NEXT_TYPE_LEVEL_NODE db_get_next_type_level_node
#define DB_GET_N_VALUES db_get_n_values
#define DB_GET_PROC_BY_COORDINATES db_get_proc_by_coordinates
#define DB_INTERPOLATE db_interpolate
#define DB_MAKE_AMR db_make_amr
#define DB_MAKE_AMR_NOADD db_make_amr_noadd
#define DB_MOVE_ZERO_SIG_TO_GHO db_move_zero_sig_to_gho
#define DB_REMOVE_FROM_LIST_BY_POINTER db_remove_from_list_by_pointer
#define DB_SET_FUNCTION_BY_POINTER db_set_function_by_pointer
#define DB_SET_ID_BY_POINTER db_set_id_by_pointer
#define DB_SET_IFUNCTION_BY_POINTER db_set_ifunction_by_pointer
#define DB_SET_NWLT_PER_TREE db_set_nwlt_per_tree
#define DB_TEST_IF_IN_SIG_LIST_BY_POINTER db_test_if_in_sig_list_by_pointer
#define DB_TEST_NODE db_test_node
#define DB_UPDATE_DB db_update_db
#define DB_UPDATE_PROC_INFO db_update_proc_info
#define DB_UPDATE_TYPE_LEVEL db_update_type_level
#define DB_WRITE_AMR_COMMENT db_write_amr_comment
#define DB_WRITE_TREE_NODES db_write_tree_nodes
#define RES_OPEN res_open
#define RES_WRITE res_write
#define RES_READ res_read
#define RES_CLOSE res_close
#define ZOLTAN_DOMAIN_PARTITIONING zoltan_domain_partitioning

#endif /* DATABASE_APPEND_UNDERSCORE */
#endif /* DATABASE_INTERFACE_LOWERCASE */


/*
  A quick way to generate list of DB and I/O functions:
  grep DB_ interface.h | \
  awk '/DB_/{sub(/^.*DB_/, "#define DB_"); print $1 " " $2 " " tolower($2)}' \
  | sort -u
  
  grep "void RES_" io.h | \
  awk '/RES_/{sub(/^.*RES_/, "#define RES_"); print $1 " " $2 " " tolower($2)}' \
  | sort -u
*/

#endif /* DATABASE_INTERFACE_LOWERCASE_H */
