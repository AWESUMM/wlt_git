#ifndef ZOLTAN_PARTITIONING__QUERIES_H
#define ZOLTAN_PARTITIONING__QUERIES_H

/******************************************************************************
**                                                                           **
**                    Application Defined Query Functions                    **
**                                                                           **
**                                    f o r                                  **
**                                                                           **
** Interface Function Between  wlt3D code  and  Zoltan Partitioning Lirary   **
**                                                                           **
**                                                                           **
**    Zoltan :: Copyright (c) 2000-2008, Sandia National Laboratories.       **
**                                                                           **
*******************************************************************************/


#include "zoltan_cpp.h"
#include "zoltan_partitioning__header.h"
#include "math.h"


///// * * * * *         G e n e r a l     Q u e r i e s         * * * * * /////


/* 
 **************************************************************
 * Prototype: ZOLTAN_NUM_OBJ_FN
 *
 * Return the number of objects at current processor. 
 *
 **************************************************************
 */
static int Get_Number_of_Objects(void  *data, 
                                 int   *ierr)
{
  *ierr = ZOLTAN_OK;
  return Number_Vertices_OnEachProcess[myRank];
}




/* 
 **************************************************************
 * Prototype: ZOLTAN_OBJ_LIST_FN
 *
 * Return list of current processor's objects, with optional weights.
 *
 **************************************************************
 */
static void Get_Object_List(void           *data, 
                            int            sizeGID,
                            int            sizeLID,
                            ZOLTAN_ID_PTR  globalID,
                            ZOLTAN_ID_PTR  localID,
                            int            wgt_dim,
                            float          *obj_wgts, 
                            int            *ierr)
{
  int i, shift;
  
  if (sizeGID != 1){
    *ierr = ZOLTAN_FATAL;
    return;
  }
  
  
  //
  // find starting index of the nodes of that processor
  // in the rank-arranged array (e.g. Vertices_Global_ID of domain_zoltan.f90)
  //
  for (i=0, shift=0; i<myRank; ++i)
    shift += Number_Vertices_OnEachProcess[i];
  
  
  //
  // return the data for the current processor
  //
  // nwlt_Vertices comes from parallel.f90 Nwlt_per_Tree, which is in
  // natural order of tree numbering and therefore has to be referenced
  // through Vertices_Global_ID - that natural order index
  //
  for (i=0; i<Number_Vertices_OnEachProcess[myRank]; ++i) {
    globalID[i]  =  Vertices_Global_ID[shift+i];
    localID[i]   =  i;
    obj_wgts[i]  =  (float) nwlt_Vertices[ Vertices_Global_ID[shift+i]-1 ];
  }
  
/*   printf ("obj_wgts (at proc %d) = ",myRank); */
/*   for (i=0; i<Number_Vertices_OnEachProcess[myRank]; ++i) */
/*     printf ("%5.3f ", obj_wgts[i]); */
/*   printf("\n"); */
/*   fflush(0); */


  *ierr = ZOLTAN_OK;
  return;
}




///// * * * * *       G e o m e t r i c     M e t h o d s       * * * * * /////


/* 
 **************************************************************
 * Prototype: ZOLTAN_NUM_GEOM_FN
 *
 * Return the dimension of a vertex
 * 
 * For Geometric methods.
 **************************************************************
 */
static int Get_Num_Geometry(void  *data,
                            int   *ierr)
{
  *ierr = ZOLTAN_OK;
  return Problem_Dimension;
}




/* 
 **************************************************************
 * Prototype: ZOLTAN_GEOM_MULTI_FN
 *
 * Return the coordinates of my objects (vertices)
 * 
 * For Geometric methods.
 **************************************************************
 */
static void Get_Geometry_List(void           *data,
			      int            sizeGID,
			      int            sizeLID,
                              int            num_obj,
                              ZOLTAN_ID_PTR  globalID,
			      ZOLTAN_ID_PTR  localID,
                              int            num_dim,
			      double         *geom_vec,
			      int            *ierr)
{
  int i, dim;
  
  
  if ( (sizeGID != 1) || (sizeLID != 1) ) {
    *ierr = ZOLTAN_FATAL;
    return;
  }
  
  
  //
  // Vertices_Coordinates are in tree natural order, in parallel_domain_decompose_zoltan of parallel.f90
  // and therefore to be referenced through Global_ID - that natural order index
  //
  for (i=0;  i < num_obj; ++i)
    for (dim=0; dim < Problem_Dimension; ++dim)
      geom_vec[ Problem_Dimension*i + dim ] = Vertices_Coordinates[ Problem_Dimension*(globalID[i]-1) + dim ];
  
  
  *ierr = ZOLTAN_OK;
  return;
}





///// * * * * *      H y p e r G r a p h     M e t h o d s      * * * * * /////

static struct _hg_data{
                       int  numEdges;
                       int  numPins;
}hg_data;



/* 
 **************************************************************
 * Prototype: ZOLTAN_OBJ_SIZE_FN
 *
 * Return migration volume: the size (in bytes) of the data buffer
 * that is needed to pack all of a single object's data. 
 *
 **************************************************************
 */
static int Get_Object_Size(
			   void *data,
			   int num_gid_entries, 
			   int num_lid_entries,
			   ZOLTAN_ID_PTR global_id,
			   ZOLTAN_ID_PTR local_id,
			   int *ierr)
{  
  *ierr = ZOLTAN_OK;
  //
  // for each tree node we need its 1D coordinate and ID
  //
  return 2*sizeof(int)*nwlt_Vertices[ *global_id-1 ];
}

/* 
 **************************************************************
 * Prototype: ZOLTAN_HG_SIZE_CS_FN
 *
 * This function tells the Zoltan library how many hyperedges
 * I will supply, and how many total pins (vertices) are in those
 * hyperedges, and my choice of compressed hyperedge format.
 *
 * For HyperGraph methods.
 *
 * We will create a hypergraph out of the simple graph in the
 * following way.  For every vertex in the graph, we will 
 * create a hyperedge connecting that vertex and each of its
 * neighbors. (So for each vertex, we will have as many as
 * hyperedge as number of its neighbors.
 *
 * We will supply this hypergraph to Zoltan in compressed
 * hyperedge format.  (Compressed vertices is other choice.  See
 * the Zoltan User's Guide section on this query function for
 * more information on compressed formats.)
 *
 * We will initially divide the hyperedges across the processes
 * in a round robin manner.  However Zoltan does not require 
 * that each process supply an entire hyperedge.  Hyperedges 
 * could be distributed across processes. 
 **************************************************************
 */
void Get_HG_Size(void  *data,
		 int   *num_lists,//Upon return, the number of vertices (vertex storage) or hyperedges (hyperedge storage)
		 int   *num_pins, //Upon return, the number of pins (connections between vertices and hyperedges)
                 int   *format,   //ZOLTAN_COMPRESSED_EDGE and ZOLTAN_COMPRESSED_VERTEX
		 int   *ierr)
{
  int    i, shift;
  struct _hg_data   *hgd = (struct _hg_data *)data;
  
  hgd->numEdges = 0;
  hgd->numPins  = 0;
  
  
  //
  // find starting index of the nodes of that processor
  // in the rank-arranged array (e.g. Vertices_Global_ID of domain_zoltan.f90)
  //
  for (i=0, shift=0; i<myRank; ++i)
    shift += Number_Vertices_OnEachProcess[i];
  
  
  //
  // Number_Vertices_Neighbors is in processor rank order
  //
  for (i=0; i<Number_Vertices_OnEachProcess[myRank]; ++i) {
    //
    // each regular grid edge begets 2 pins in a hypergraph matrix
    //
    hgd->numPins   += 2 * Number_Vertices_Neighbors[ shift+i ];
    hgd->numEdges  +=     Number_Vertices_Neighbors[ shift+i ];
  }
  
  
  *num_lists = hgd->numEdges;
  *num_pins  = hgd->numPins;
  *format    = ZOLTAN_COMPRESSED_EDGE;  
  *ierr      = ZOLTAN_OK;
  
  return;
}
/* 





/* 
 **************************************************************
 * Prototype: ZOLTAN_HG_CS_FN
 *
 * Return a compressed list describing the hypergraph sparse matrix.
 * We can think of the columns as vertices and the rows as
 * hyperedges.  For each row we have ones in the column representing
 * vertices in the hyperedge, and zeroes in the other entries.
 * 
 * For HyperGraph methods.
 **************************************************************
 */
void Get_HG(void           *data,
	    int            sizeGID,
	    int            num_rows,
	    int            num_pins, 
            int            format,
	    ZOLTAN_ID_PTR  edge_GID,
	    int            *edge_ptr, 
            ZOLTAN_ID_PTR  pin_GID,
	    int            *ierr) 
{
  int               i, j, shift, npins = 0;
  struct _hg_data   *hgd = (struct _hg_data *)data;
  
  
  if ((num_rows != hgd->numEdges) || (num_pins != hgd->numPins) ||
      (format != ZOLTAN_COMPRESSED_EDGE)){
    *ierr = ZOLTAN_FATAL;
    return;
  }
  
  
  //
  // find starting index of the nodes of that processor
  // in the rank-arranged array (e.g. Vertices_Global_ID of domain_zoltan.f90)
  //
  for (i=0, shift=0; i<myRank; ++i)
    shift += Number_Vertices_OnEachProcess[i];
  
  
  //
  // Number_Vertices_Neighbors and Edges_Vertices_ID are in processor rank order
  //
  // EV_ID - initialized as GID of the first edge in the group of edges of the current processor
  //
  int  EV_ID=0;
  for (i=0; i<shift; ++i)
    EV_ID += Number_Vertices_Neighbors[i];
  
  
  int HGE_GID = EV_ID;
  
  
  for (i=0; i<Number_Vertices_OnEachProcess[myRank]; ++i) {
    //
    // list global ID of each pin (vertex) in hyperedge
    //
    for (j=0; j<Number_Vertices_Neighbors[shift+i]; ++j) {
      
      *edge_ptr++  =  npins;                                     /* index into start of Pin list */      
      *edge_GID++  =  HGE_GID;                                   /* HyperEdge Global ID */        
      *pin_GID++   =  Edges_Vertices_ID[EV_ID];                  /* HyperEdge Pins   Global ID */        
      *pin_GID++   =  Vertices_Global_ID[shift+i];               /* HyperEdge Pins   Global ID */      
      npins       +=  2;                                         /* Counting  Pins of my HyperEdge */
      HGE_GID++;
      EV_ID++;
    }
  }
  
  
  *ierr = ZOLTAN_OK;
  return;
}





/* 
 **************************************************************
 * Prototype: ZOLTAN_HG_SIZE_EDGE_WTS_FN
 *
 * This query function returns the number of hyperedges for which
 * the process will supply edge weights.  The edges for which
 * a process supplies edge weights need not be the same edges
 * for which it supplied pins.  Multiple processes can supply
 * weights for the same edge.  The multiple weights are resolved
 * according the the setting of the PGH_EDGE_WEIGHT_OPERATION 
 * parameter.  Edge weights are optional.
 *
 * For HyperGraph methods.
 **************************************************************
 */
void Get_HG_Num_Edge_Weights(void  *data, 
			     int   *num_edges,
			     int   *ierr) 
{
  struct _hg_data *hgd = (struct _hg_data *)data;
  

  *num_edges = hgd->numEdges;
  *ierr = ZOLTAN_OK;

  return;
}





/* 
 **************************************************************
 * Prototype: ZOLTAN_HG_EDGE_WTS_FN
 *
 * This query function supplies edge weights to the
 * Zoltan library.  The edge global ID corresponds to the
 * hyperedge global IDs supplied in the ZOLTAN_HG_CS_FN.
 * The edge local IDs are for consistency with the rest
 * of the Zoltan library, but at this point in time, there
 * is no interface that uses local IDs to refer to hyperedges.
 *
 * The edge weight dimension was supplied by the application 
 * as the value of the EDGE_WEIGHT_DIM parameter.  If the dimension 
 * is greater than one, list all weights for the first edge,
 * followed by all weights for the second edge, and so on.
 *
 * For HyperGraph methods.
 **************************************************************
 */
void Get_HyperEdge_Weights(void           *data,
			   int            sizeGID,          
			   int            sizeLID, 
			   int            num_edges, 
			   int            edge_weight_dim,          
			   ZOLTAN_ID_PTR  edge_GID, 
			   ZOLTAN_ID_PTR  edge_LID,          
			   float          *edge_weight, 
			   int            *ierr) 
{
  int              i, j, shift, max_N;
  struct _hg_data  *hgd = (struct _hg_data *)data;
  
  
  if ((sizeGID != 1) || (sizeLID != 1) || 
      (num_edges != hgd->numEdges) || (edge_weight_dim != 1)){
    *ierr = ZOLTAN_FATAL;
    return;
  }
  
  
  //
  // find starting index of the nodes of that processor
  // in the rank-arranged array (e.g. Vertices_Global_ID of domain_zoltan.f90)
  //
  for (i=0, shift=0; i<myRank; ++i)
    shift += Number_Vertices_OnEachProcess[i];
  
  
  //
  // Number_Vertices_Neighbors and Edges_Vertices_ID are in processor rank order
  //
  // EV_ID - initialized as GID of the first edge in the group of edges of the current processor
  //
  int  EV_ID=0;
  for (i=0; i<shift; ++i)
    EV_ID += Number_Vertices_Neighbors[i];
  
  
  int HGE_GID = EV_ID;
  
  
  max_N = N_Predict > N_Update ?  N_Predict : N_Update;
  
  
  for (i=0; i<Number_Vertices_OnEachProcess[myRank]; ++i)
    for (j=0; j<Number_Vertices_Neighbors[shift+i]; ++j) {
      
      *edge_GID++    =  HGE_GID;                                                   /* HyperEdge Global ID */        
      
     //     *edge_weight++ =                                                             /* HyperEdge Weight: Power Law Estimation    */
     // 	pow( nwlt_Vertices[ Vertices_Global_ID[shift+i]-1 ],
     //      (Problem_Dimension-1)/(1.0*Problem_Dimension)
     //      )
     // * max_N	* EdgeWeight_Global_SafetyFactor;  

      /* Edges_vertices_Nwlt is supplied from the fortran level,
	 it is the number of points on each tree face (or an estimation
	 thereof). */
      *edge_weight++ =                                                             /* HyperEdge Weight: From surface point counts    */
	Edges_Vertices_Nwlt[HGE_GID] * max_N	* EdgeWeight_Global_SafetyFactor;
      
      HGE_GID++;
    }
  
  
  *ierr = ZOLTAN_OK;
  return;
}

#endif
