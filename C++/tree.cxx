/*----------------------------------------------------------------*/
/*     Tree Structured Database                                   */
/*                                                                */
/*     Developed by Alexei Vezolainen                             */
/*                                                                */
/*     Please respect all the time and work                       */
/*     that went into the development of the code.                */
/*                                                                */
/*----------------------------------------------------------------*/
#include <vector>
#include <iostream>
#include <math.h>
#include "tree.h"

using std::endl;
using std::cout;
using std::vector;


/*--------------------------------------------------------------------------------------------*/
/* Debug                                                                                      */
/*--------------------------------------------------------------------------------------------*/
#ifdef EBUG_SAFE
/* vector output */
#include <iterator>
std::ostream& operator<< (std::ostream& out, const vector<bool>& v) {
  std::copy(v.begin(), v.end(), std::ostream_iterator<bool>(out," "));
  return out; }
std::ostream& operator<< (std::ostream& out, const vector<int>& v) {
  std::copy(v.begin(), v.end(), std::ostream_iterator<int>(out," "));
  return out; }
std::ostream& operator<< (std::ostream& out, const vector<Value>& v) {
  std::copy(v.begin(), v.end(), std::ostream_iterator<Value>(out," "));
  return out; }
std::ostream& operator<< (std::ostream& out, const vector<Node*>& v) {
  std::copy(v.begin(), v.end(), std::ostream_iterator<Node*>(out," "));
  return out; }
#endif


/*--------------------------------------------------------------------------------------------*/
/* Templates                                                                                  */
/*--------------------------------------------------------------------------------------------*/
// #define DIMENSION 3
// template <int N>
// inline int templ_sum1 (int* opt1_factor, const int* x, int x_space_pos) {
//   return templ_sum1<N-1>(opt1_factor,x,x_space_pos) + opt1_factor [N-1] * ( x[N-1] >> x_space_pos );
// }
// template <>
// inline int templ_sum1<1> (int* opt1_factor, const int* x, int x_space_pos) {
//   return opt1_factor [0] * ( x[0] >> x_space_pos );
// }




/*--------------------------------------------------------------------------------------------*/
/* Parallel related functions                                                                 */
/*--------------------------------------------------------------------------------------------*/




/*--------------------------------------------------------------------------------------------*/
/* Global variables                                                                           */
/*--------------------------------------------------------------------------------------------*/
Global_variables::Global_variables (const int*& m, const int*& p,
				    const int& j_tree, const int& j_max,
				    const int& val, const int& ival,
				    const int& dim, const int& p_size, const int& p_rank) :
  J_TREE(j_tree), J_MAX(j_max), VAL_NUM(val), VAL_NUM_I(ival), DIM_NUM(dim),
  TYPES_NUM(1<<dim),
#if TREE_VERSION == 0
  LEAVES( (1<<dim)- 1 ),
#elif TREE_VERSION == 1
  QUAD(1<<dim),
#endif //TREE_VERSION
  par_size(p_size), par_rank(p_rank)
{
  if (dim > DIM_MAX) {
    std::cout << "Internal arrays were not designed for the given dimension " << dim
   	      << "\nPlease set 'DIM_MAX " << dim << "' at line 13 of tree.h and recompile\n";
    exit (1);
  }
  
  PERIODIC = new int [dim];
#ifndef FIX_FINEST_MESH_DATABASE_TREE
  FINEST_MESH = new int [dim];
#endif
  for (int i=0; i<dim; ++i) {
    PERIODIC [i] = p[i];
    FINEST_MESH [i] =  m[i] << (j_max-1);
  }
}



Global_variables::~Global_variables () {
  delete [] PERIODIC;
#ifndef FIX_FINEST_MESH_DATABASE_TREE
  delete [] FINEST_MESH;
#endif
}





/*--------------------------------------------------------------------------------------------*/
/* Node of the tree                                                                           */
/*--------------------------------------------------------------------------------------------*/
// default empty node
#if TREE_VERSION == 0
Node::Node() :
  ii(0), val(0), ival(0), num_leaves(0), level(0), id(zero_id), leaves(0),
  tlp(0)
{}

#elif TREE_VERSION == 1

Node::Node() :
#ifndef FIX_COORD_DATABASE_TREE
  ii(0), 
#endif
  val(0), ival(0), id(0), tlp(0), ptr(0), data_ptr(0)
{}
#endif //TREE_VERSION





#if TREE_VERSION == 0
// default node with set ID and coordinates
Node::Node (const ID id_in, const int DIM_NUM, const int coord_in[]) :
  id(id_in), val(0), ival(0), num_leaves(0), level(0), leaves(0),
  tlp(0)
{
  ii = new int [DIM_NUM];                                           // coordinates
  int dim = DIM_NUM;
  while (dim--) {
    ii [dim] = coord_in [dim];
  }
}
#endif //TREE_VERSION




Node::~Node()
{
  if ( tlp )
    tlp->tlp_node_exclude (this);
  
#ifndef FIX_COORD_DATABASE_TREE
  delete[] ii;
#endif
  delete[] val;
  delete[] ival;
  id = 0;
  
  
#if TREE_VERSION == 0
#ifdef EBUG_SAFE
  int num_chi = 0;                 // test if children present
  for (int i=0; i<num_leaves; i++)
    if ( leaves[i] ) {
      delete leaves[i];
      num_chi ++;
    }
  if (num_chi)
    printf ("WARNING: ~Node(): recursive deletion\n");
#endif
  if (leaves)
    delete[] leaves;
#endif //TREE_VERSION
}





#if TREE_VERSION == 0
// increase number of leaves
void Node::rearrange_leaves (const int& LEAVES, const int& max_required) {
  if (this->num_leaves == 0) {
    num_leaves = LEAVES + max_required;
    leaves = new Node* [num_leaves];
    int i = num_leaves;
    do {
      i --;
      leaves[i] = 0;
    } while (i);
  } else {
    int new_num_leaves = LEAVES + max_required;
    Node **new_leaves = new Node* [new_num_leaves];
    int i = num_leaves;
    do {
      i --;
      new_leaves [i] = leaves [i];
    } while (i);
    for (i=num_leaves; i<new_num_leaves; i++)
      new_leaves[i] = 0;
    delete[] leaves;
    this->leaves = new_leaves;
    this->num_leaves = new_num_leaves;
  }
}
#endif //TREE_VERSION




// call this function after the node's coordinates are known
// to calculate tlp_index for the node;
// proc is the processor's rank the node belongs to
void Forest::set_type_level( Node* node, const int& proc )
{
  // -------------- compute actual level of the node ------------------
  int lev = J_MAX;
  for (int test = 1; lev>1; --lev) {
    bool one_present = 0;
    for (int dim=0; dim<DIM_NUM; ++dim)
      one_present |= test & node->ii[dim];
    if (one_present)
      break;
    test <<=1;
  }
  // -------------- compute wavelet type of the node ------------------
  //       (..., x, y, z)
  //	   odd == 1, even == 0
  //	   e.g. (odd X, odd Y, even Z) == (110)
  //
  int type = 0, factor = 1, space = 1<<(J_MAX-lev);
  if (lev > 1)
    for (int dim=DIM_NUM-1; dim >=0; --dim) {
      type += factor * ( (node->ii[dim]&(2*space-1)) / space);
      factor <<= 1;
    }
  // -------------- compute face type of the node ------------------
  //    non-periodic:      periodic for X:
  //
  //    Y   6  7  8          7 7 .
  //    |   3  4  5          4 4 .
  //    |   0  1  2          1 1 .
  //    |
  //    +------X
  // face_type is in the limits of [0 .. 3^dim-1]
  int i, face_type = 0;
  factor = 1;
  for (i=0; i < DIM_NUM; ++i) {
    face_type += PERIODIC[i] * factor + (1-PERIODIC[i]) * factor *
      ( MIN(1,node->ii[i]) + node->ii[i]/FINEST_MESH[i] );
    factor += factor<<1;
  }
  
  // -------------- set node's parameters ------------------
  node->tlp_index = f_sig.tlp_index (face_type, type, lev, proc);
  node->next_tlp = 0;
  node->prev_tlp = 0;
}





#if TREE_VERSION == 0
void Node::set_value_vector (const int& VAL_NUM, const int& VAL_NUM_I)
{
  val = new Value [VAL_NUM];                      // Value vector initialization
  int i;
  for ( i=0; i<VAL_NUM; i++)
    val [i] = 0;
  ival = new int [VAL_NUM_I];
  for ( i=0; i<VAL_NUM_I; i++)
    ival [i] = 0;
}

void Node::zero_value_vector (const int& VAL_NUM, const int& VAL_NUM_I)
{
  if (!val) return;
  int i;
  for ( i=0; i<VAL_NUM; i++) val [i] = 0;
  for ( i=0; i<VAL_NUM_I; i++) ival [i] = 0;
}

#endif //TREE_VERSION




/*--------------------------------------------------------------------------------------------*/
/* Forest                                                                                     */
/*--------------------------------------------------------------------------------------------*/
// x[]+1 (non periodic), or x[] (periodic) gives the number of point on edge, from which trees will grow
// procs[number_of_trees] - to which processor each tree belong (not defined for single processor case)
Forest::Forest (const int* const x,
		Global_variables* gl,
		Memory_management* mm,
		const int*const procs )
  :  g(gl), m(mm),
     DIM_NUM(gl->DIM_NUM), J_MAX(gl->J_MAX),
     J_TREE(gl->J_TREE), VAL_NUM(gl->VAL_NUM), VAL_NUM_I(gl->VAL_NUM_I),
#if TREE_VERSION == 1
     QUAD(1<<DIM_NUM),
#endif //TREE_VERSION
     par_rank(gl->par_rank), par_size(gl->par_size),
     f_sig(gl), f_gho(gl)
{
  
  PERIODIC = new int [DIM_NUM];
#ifndef FIX_FINEST_MESH_DATABASE_TREE
  FINEST_MESH = new int [DIM_NUM];
#endif
  
  int dim;
  for ( dim=0; dim<DIM_NUM; ++dim) {
    PERIODIC [dim] = gl->PERIODIC [dim];
    FINEST_MESH [dim] = gl->FINEST_MESH [dim];
  }                                                      // ....................................

  
#ifndef FIX_INDEX_ON_FINEST_MESH_DATABASE_TREE
  index_on_finest_mesh = new int [DIM_NUM];              // ... local buffer for finest mesh coordinates
#endif  

  
  x_space = FINEST_MESH[0] / x[0];                       // ... coarse grid spacing
  x_space_pos = J_MAX - J_TREE;                          // ...
  
  
  this->number_of_trees = 1;
  for ( dim=0; dim<DIM_NUM; ++dim) {
#ifdef EBUG_SAFE
    if (x_space != FINEST_MESH[dim] / x[dim]) {
      std::cout << "ERROR in Forest initialization\n";
      exit (1);
    }
#endif
    number_of_trees *= (x[dim]+(1-PERIODIC[dim]));
  }
  
  
#ifndef FIX_OPT1_FACTOR_DATABASE_TREE
  opt1_factor = new int [DIM_NUM];                       // ... optimized array of factors
#endif
  opt1_factor [0] = 1;
  for ( dim=0; dim<DIM_NUM-1; ++dim)
    opt1_factor [dim+1] = opt1_factor [dim] * ((FINEST_MESH[dim] >> x_space_pos) + 1 - PERIODIC[dim]);
  //                                                     // ..............................
  

  int i, coord[DIM_MAX];                    // temporary coordinates
  for ( i=0; i<DIM_NUM; ++i) coord[i] = 0;
  
  trees = new Tree* [number_of_trees];               /* populate the forest */
  
  
  for (int si=0; si<number_of_trees; ++si)
    {  
#if TREE_VERSION == 0
      Node *a = new Node (intermediate_id, DIM_NUM, coord);  // tree root      
      int a_proc = par_rank;                                 // set processor number to
      if (par_size > 1) a_proc = procs[si];                  //  which the root belongs
      
      a->set_value_vector (VAL_NUM, VAL_NUM_I);
      
      //       if (!g->par_rank) {
      // 	printf ("### tree %d: [ ",si+1);
      // 	for ( i=0; i<DIM_NUM; ++i) printf ("%d ",coord[i]);
      // 	printf ("] \t-> proc %d\n",a_proc);
      //       }
      
#elif TREE_VERSION == 1
      
#ifdef EBUG_MM
      printf ("$ creating root\n");
#endif
      Node *a = m->allocate_node ();           // MM analog ---------
      a->data_ptr = 1;
      a->id = intermediate_id;
      int a_proc = par_rank;                                 // set processor number to
      if (par_size > 1) a_proc = procs[si];                  //  which the root belongs
      int dim = DIM_NUM;
      do {
	dim --;
	a->ii [dim] = coord [dim];
      } while (dim);                           // MM analog ---------
      
      
#endif //TREE_VERSION
      
      set_type_level (a, a_proc);
      trees[si] = new Tree (a,x_space,g,this,mm,a_proc);  // grow tree from that root
      
      coord[0] += x_space;                                        // lowest bit iteration step
      for ( i=0; i<DIM_NUM-1; i++) {                     // upgrade the highest bits
	if (coord[i] == x_space * (x[i]+1-PERIODIC[i]) ) {
	  coord[i] = 0;
	  coord[i+1] += x_space;
	}
      }
    }
  
  // allocate array of zero values
  zero_val = new Value [VAL_NUM];
  for (i=0; i<VAL_NUM; ++i)
    zero_val[i] = 0;

#ifdef EBUG_SAFE
  if (!g->par_rank) {
    int jjj;
    printf ("#  Forest has been initialized with %d trees on [ ",number_of_trees);
    for ( jjj=0; jjj<DIM_NUM; ++jjj) printf ("%d ", x[jjj]+1-PERIODIC[jjj]); printf ("] grid:\n");
    printf ("#  J_TREE = %d, J_MAX = %d\n", J_TREE, J_MAX);
    printf ("#  FINEST_MESH = [ ");
    for ( jjj=0; jjj<DIM_NUM; ++jjj) printf ("%d ", FINEST_MESH[jjj]); printf ("]\n");
    printf ("#  PERIODIC = [ ");
    for ( jjj=0; jjj<DIM_NUM; ++jjj) printf ("%d ", PERIODIC[jjj]); printf ("]\n");
  }
#endif
}





Forest::~Forest()
{
  
  delete_the_forest();     // delete as much nodes as possible
  //                       //  through depth first tree search
  
  for (int i=0; i<this->number_of_trees; ++i) {
    if ( trees[i] ) {
      delete trees[i];     // delete each tree separately
      trees[i] = 0;
    }
  }
  delete[] trees;          // delete array of pointers to trees
  delete[] PERIODIC;       //  and other arrays ...

#ifndef FIX_OPT1_FACTOR_DATABASE_TREE
  delete[] opt1_factor;
#endif
#ifndef FIX_FINEST_MESH_DATABASE_TREE
  delete[] FINEST_MESH;
#endif
#ifndef FIX_INDEX_ON_FINEST_MESH_DATABASE_TREE
  delete[] index_on_finest_mesh;
#endif
  delete[] zero_val;

}





//------------------------------------------------------------------------------------------------------
// Write Nwlt_per_Tree array for the current processor (sig+adj)
// (it may be different on another processor)
//------------------------------------------------------------------------------------------------------
int Forest::set_nwlt_per_tree (int*tree_LB_weight, const int number_of_trees) {
  
  if (number_of_trees != this->number_of_trees) return 1;
  
  int main_index,
    ilow,
    ci,
    totc,
    depth = g->J_MAX - g->J_TREE + 1;                        // max path length
  vector <Node*> path_node (depth, (Node*) 0);               // current path
  vector <int>   path_cindex (depth,  0);                    // current child index
  
  
  for (int si=0; si < number_of_trees; ++si ) {     // for each tree -------------------------------
    
    if (trees[si]->get_proc() != this->par_rank) continue;      // skip trees of the other processors
    tree_LB_weight[si] = 0;                                     // write tree statistics
    
    main_index = 0;                                             // to which position path_...[] is filled
    Node *node = path_node[0] = trees[si]->get_root();          // initialize path's beginning
    path_cindex[0] = 0;                                         // ...

    
    while (true) {                                  // the main cycle ===============================
      ilow = path_cindex [main_index];
      totc = 0;

      if (found_nonzero_child_link (node,ci,ilow,totc)) {
	main_index ++;
	node = path_node [main_index] = node->get_child( ci );
	path_cindex [main_index-1] = ci+1;
	path_cindex [main_index]   = 0;
	//if (si==36) printf ("$NWLT$ - found_link   path_node[%d] : %d %d\n",main_index,node->ii[0],node->ii[1]);

      } else {
	if (node->id & (significant_id | adjacent_id) ) tree_LB_weight[si]++;
	//if (si==36) printf ("$NWLT$   path_node[%d] : %d %d, weight=%d\n",main_index,node->ii[0],node->ii[1],tree_LB_weight[si]);
	main_index --;
	if (main_index < 0) break;
	
	path_node   [main_index+1] = 0;
	path_cindex [main_index+1] = 0;
	node = path_node [main_index];
      }
    }                                               // the main cycle ===============================
    
    for (int i=1; i< depth; ++i) {                              // clear the path
      path_node   [i] = 0;                                      // ...
      path_cindex [i] = 0;                                      // ...
    }
  }                                                 // for each tree -------------------------------
  
  return 0;
}



//------------------------------------------------------------------------------------------------------
// update tree-per-proc info:
// 1) tree->proc, 2) link-lists for each node
//------------------------------------------------------------------------------------------------------
void Forest::update_proc_info (const int*const new_procs) {
  
  int main_index,
    ilow,
    ci,
    old_proc,
    totc,
    depth = g->J_MAX - g->J_TREE + 1;                        // max path length
  vector <Node*> path_node (depth, (Node*) 0);               // current path
  vector <int>   path_cindex (depth,  0);                    // current child index
  
  
  for (int si=0; si < number_of_trees; ++si ) {     // for each tree -------------------------------
    
    if (trees[si]->get_proc() == new_procs[si] ) continue;
    // skip trees which do not change owner processor
    // and update the others:
    
    
    // 1) tree->proc
    old_proc = trees[si]->get_proc();
    trees[si]->set_proc( new_procs[si] );
    
    
    // 2) link-lists for each node
    main_index = 0;                                             // to which position path_...[] is filled
    Node *node = path_node[0] = trees[si]->get_root();          // initialize path's beginning
    path_cindex[0] = 0;                                         // ...
    
    while (true) {                                  // the main cycle ===============================
      ilow = path_cindex [main_index];
      totc = 0;
      
      if (found_nonzero_child_link (node,ci,ilow,totc)) {
	main_index ++;
	node = path_node [main_index] = node->get_child( ci );
	path_cindex [main_index-1] = ci+1;
	path_cindex [main_index]   = 0;
	
      } else {
	{
	  Type_level *the_list = node->get_tlp();
	  if (the_list) the_list->tlp_node_exclude (node);                 // exclude
	  update_type_level ( node, old_proc, new_procs[si] );             // update proc
	  if (the_list) the_list->tlp_node_include_f (node);               // include
	}
	
	main_index --;
	if (main_index < 0) break;
	
	path_node   [main_index+1] = 0;
	path_cindex [main_index+1] = 0;
	node = path_node [main_index];
      }
    }                                               // the main cycle ===============================
    
    for (int i=1; i< depth; ++i) {                              // clear the path
      path_node   [i] = 0;                                      // ...
      path_cindex [i] = 0;                                      // ...
    }
  }                                                 // for each tree ------------------------------- 
}



// // Add a child to an existing parent
// void Forest::add_locally( const int coord[], Node*& parent, const int& ch_num, const int& id_in, const bool front )
// {
//   // test if the node is present already
//   if ( parent->leaves[ch_num] == 0 ) {
//     parent->leaves[ch_num] = m->allocate_node ();
//   }
  
//   parent->leaves[ch_num]->level = parent->level + 1 + ch_num/g->LEAVES;
//   for (int i=0; i<DIM_NUM; ++i)
//     parent->leaves[ch_num]->ii [i] = coord[i];
//   parent->leaves[ch_num]->id = id_in;
//   parent->leaves[ch_num]->set_type_level (J_TREE,J_MAX,DIM_NUM);
//   int ftindex = calc_face_type_index (parent->leaves[ch_num]->ii);
//   Type_level *the_list = the_list = & f_gho [ftindex];
//   if ((id_in & significant_id) || (id_in & adjacent_id))
//     the_list = & f_sig_adj [ftindex];
  
//   if (front) the_list -> tlp_node_include_f (parent->leaves[ch_num]);
//   else the_list -> tlp_node_include_b (parent->leaves[ch_num]);

// #ifdef xgraphics
//   XG->xprint_node(parent->leaves[ch_num]->ii, parent->leaves[ch_num]->my_level);
// #endif
// }



int Forest::get_proc_by_coordinates (const int x[], const int xlevel) const
{
  int
    index_in_tree_array = 0,
    dim = DIM_NUM;
  while (dim--) {
    index_in_tree_array += opt1_factor [dim] * ( (x[dim]<<(J_MAX-xlevel)) >> x_space_pos );
  }
  
  //return index_in_tree_array;
  return trees[index_in_tree_array]->get_proc();
}

int Forest::add_by_coordinates (const int x[], const int xlevel,
				Node*& output_node,
				const int& id_in, const bool front)
  /* 1)  add new node to the correspondent tree, set its ID to (ID|id_in)                */
  /*     add all intermediate nodes if necessary, set their ID's to intermediate_id      */
  /*     return the pointer to the node and the total number of the nodes created        */
  /*     and insert it into sig_adj or gho TLP system (in front or back)                 */
  /* 2)  if a node with existing ID==intermediate_id has been found,                     */
  /*     amend it's ID, return 1, and insert the node into TLP                           */
  /* 3)  return 0, pointer to the node, and do nothing                                   */
  /*     if ID of the existing node != intermediate_id                                   */
{
  int
    front_tll_marker = -1,              // do not include into the list
    index_in_tree_array = 0,
    dim = DIM_NUM;
  while (dim--) {
    index_on_finest_mesh[dim] = x[dim]<<(J_MAX-xlevel);
    index_in_tree_array += opt1_factor [dim] * ( index_on_finest_mesh[dim] >> x_space_pos );
  }
  
  
  
  //  if (trees[index_in_tree_array]->get_proc() == par_rank) {
  front_tll_marker = 1;               // add to the back of the list
  if (front) front_tll_marker = 0;    // ... front ...
  //  }

  
#ifdef EBUG_SAFE
  int jjj;
  if ( (index_in_tree_array >= number_of_trees)||(index_in_tree_array < 0) ) {
    printf ("  DEBUG: error in Forest::add_by_coordinates([ ");
    for ( jjj=0; jjj<DIM_NUM; ++jjj) printf ("%d ",x[jjj]); printf ("], %d)\n", xlevel);
    printf ("  total number of trees: %d\n", number_of_trees);
    printf ("  tree number requested: %d\n",index_in_tree_array);
          void *array[10];
          size_t size;
          size = backtrace(array, 10);// get void*'s for all entries on the stack
          backtrace_symbols_fd(array, size, STDERR_FILENO);
    exit (1);
  }
#endif
  
  
  return trees[index_in_tree_array]->
    add_by_coordinates (index_on_finest_mesh, output_node, id_in, front_tll_marker);
  
}



P_SIZE Forest::test_by_coordinates (const int x[], const int xlevel)
  /* test the presence of the node                                           */
  /* return 1 and the pointer to it if the node is present,                  */
  /* 0 and NULL if not present (or intermediate)                             */
{
  int index_in_tree_array = 0;
  {
    int dim = DIM_NUM;
    while (dim--) {
      index_on_finest_mesh[dim] = x[dim]<<(J_MAX-xlevel);
      index_in_tree_array += opt1_factor [dim] * ( index_on_finest_mesh[dim] >> x_space_pos );
    }
  }

#ifdef EBUG_SAFE
  int jjj;
  if ( (index_in_tree_array >= number_of_trees)||(index_in_tree_array < 0) ) {
    printf ("  DEBUG: error in Forest::test_by_coordinates([ ");
    for ( jjj=0; jjj<DIM_NUM; ++jjj) printf ("%d ",x[jjj]); printf ("], %d)\n", xlevel);
    printf ("  total number of trees: %d\n", number_of_trees);
    printf ("  tree number requested: %d\n",index_in_tree_array);
    exit (1);
  }
#endif
  

  return
    trees[index_in_tree_array]->
    test_by_coordinates_1 (index_on_finest_mesh);
  
  //printf ("Forest::test_by_coordinates(): [%d %d] -> %d\n",x[0],x[1],bool(res));
  //return res;
}
Node* Forest::get_root_by_jmax_coordinates (const int x[]) {
  int index_in_tree_array = 0;
  int dim = DIM_NUM;
  while (dim--) {
    index_in_tree_array += opt1_factor [dim] * ( x[dim] >> x_space_pos );
  }
  return trees[index_in_tree_array]->get_root();
//    return trees[templ_sum1<DIMENSION>(opt1_factor,x,x_space_pos)]->get_root();
}



void Forest::get_function_by_jmax_coordinates (const int* const x,
					       const int*const i_low, const int*const i_high,
					       Value* f_out)
  /* test the presence of the node                                           */
  /* return function and the pointer to it if the node is present,                  */
  /* 0 and NULL if not present (or intermediate)                             */
{
  int index_in_tree_array = 0;
#ifndef FIX_DIM_LOOPS_DATABASE_TREE
  int dim = DIM_NUM;
  while (dim--) {
    index_in_tree_array += opt1_factor [dim] * ( x[dim] >> x_space_pos );
  }
#else
  for (int dim=0; dim<DIM_MAX; ++dim)
    index_in_tree_array += opt1_factor [dim] * ( x[dim] >> x_space_pos );
#endif
  
  
#ifdef EBUG_SAFE
  int jjj;
  if ( (index_in_tree_array >= number_of_trees)||(index_in_tree_array < 0) ) {
    printf ("  DEBUG: error in Forest::test_by_coordinates([ ");
    for ( jjj=0; jjj<DIM_NUM; ++jjj) printf ("%d ",x[jjj]); printf ("])\n");
    printf ("  total number of trees: %d\n", number_of_trees);
    printf ("  tree number requested: %d\n",index_in_tree_array);
    exit (1);
  }
#endif
  
  // the required function values are here
  Value* vals = trees[index_in_tree_array]->test_by_coordinates_nonzero (x);
//  Value* vals = trees[templ_sum1<DIMENSION>(opt1_factor,x,x_space_pos)]->test_by_coordinates_nonzero (x);

  for (int pos=*i_low-1; pos < *i_high; ++pos)
    *(f_out + pos - *i_low + 1) = vals [pos];
}





// Return the number of elements in the corresponding list
// and the first element of the list
Node* Forest::get_initial_type_level_node ( const int& proc,
					    const int& type,
					    const int& level,
					    const int& facetype,
					    int* number_of_elements,
					    const int& marker )
{
  Node *result=NULL;             // this one to be returned
  Type_level *the_list;
  switch (marker) {
  case 1:
    the_list = & f_sig;
    break;
  case 2:
    the_list = & f_gho;
    break;
  case 3:
    the_list = & f_sig;
    break;
#ifdef EBUG_SAFE
  default:
    cout << "ERROR: wrong marker=" << marker
	 << " in Forest::get_initial_type_level_node()" << endl;
    exit(1);
    break;
#endif
  }

  int ini = f_sig.tlp_index( facetype, type, level, proc ); // internal index in all tlp arrays
  
  *number_of_elements = the_list->get_tlp_number_of_elements( ini );
  result = the_list->tlp_begin [ini];

  if (marker == 3) {
    if (*number_of_elements == 0) // there are no elements in the first group
      result = f_gho.tlp_begin [ini];
    
    *number_of_elements += f_gho.get_tlp_number_of_elements( ini );
  }
  return result;
}


Node* Forest::get_next_type_level_node ( const int& proc,
					 const int& type,
					 const int& level,
					 const int& facetype,
					 const Node* node,
					 const int& marker )
{
  Node *return_result = 0;                  // this one to be returned
  Type_level *the_list;
  switch (marker) {
  case 1:
    the_list = & f_sig;
    break;
  case 2:
    the_list = & f_gho;
    break;
  case 3:
    the_list = & f_sig;
    break;
#ifdef EBUG_SAFE
  default:
    cout << "ERROR 3: wrong marker=" << marker
	 << " in Forest::get_next_type_level_node()" << endl;
    exit(1);
    break;
#endif
  }

  int ini = f_sig.tlp_index( facetype, type, level, proc ); // internal index in all tlp arrays
  
  if (node == the_list->tlp_end [ini]) {

    if (marker != 3) {                 // we're reaching the end of the first list
      return_result = 0;               // and we don't need to proceed further

    } else {                           // we have to proceed ...
      return_result = f_gho.tlp_begin [ini];
    }
  } else                                                           // we are somewhere in the middle
    return_result = node->next_tlp;
  
  return return_result;  
}



// Include the node into the correspondent
// type-level system at the end of the queue
void Type_level::tlp_node_include_b (Node* node)
{
#ifdef EBUG_SAFE
  if (node->get_tlp()) {
    printf ("tlp_node_include_b: [%d %d](%d) %s\n%",node->ii[0],node->ii[1],node->id,
	    "seems to be in some list already");
    exit(1);
  }
#endif

  int index = node->tlp_index;     // index in Forest tlp arrays which has been
  //                               //  already set by set_type_level()

  node->set_tlp (this);                              // inform the node about it
  if (get_tlp_number_of_elements( index ) == 0)           // It's the first one,
    {                                                // so start a new sequence
      tlp_begin [index] = node;
      tlp_end [index] = node;
      node->prev_tlp = 0;
      node->next_tlp = 0;
    }
  else
    {                                                // There are some already
      Node *old_last_element = tlp_end [index];                 // old last element
      tlp_end [index] = node;                             // new last element
      
      old_last_element->next_tlp = node;       // links
      node->prev_tlp = old_last_element;       // ...
      node->next_tlp = 0;                      // ...
    }
  incr_tlp_number_of_elements( index );
}



// Include the node into the correspondent
// type-level system at the front of the queue
void Type_level::tlp_node_include_f (Node* node)
{
#ifdef EBUG_SAFE
  if (node->get_tlp()) {
    printf ("tlp_node_include_f: [%d %d](%d) %s\n%",node->ii[0],node->ii[1],node->id,
	    "seems to be in some list already");
    exit(1);
  }
#endif

  int index = node->tlp_index;     // index in Forest tlp arrays
  //                               //  already set by set_type_level()
  
  node->set_tlp (this);                              // inform the node about it
  if (get_tlp_number_of_elements( index ) == 0)           // It's the first one,
    {                                                // so start a new sequence
      tlp_begin [index] = node;
      tlp_end [index] = node;
      node->prev_tlp = 0;
      node->next_tlp = 0;
    }
  else
    {                                                // There are some already
      Node *old_first_element = tlp_begin [index];                 // old first element
      tlp_begin [index] = node;                             // new first element
      
      old_first_element->prev_tlp = node;    // links
      node->next_tlp = old_first_element;               // ...
      node->prev_tlp = 0;                               // ...
    }
  incr_tlp_number_of_elements( index );
}

// Exclude the node from the correspondent type-level system
void Type_level::tlp_node_exclude (Node* node)
{
  //printf ("tlp_node_exclude \t%d\t%d\n%",node->ii[0],node->ii[1]);
  //printf ("tlp_index=%d\n",node->tlp_index);

  if (get_tlp_number_of_elements( node->tlp_index ) > 0)
    {
      /* -------------------------------------------------------- if there are some elements in the list */
      Node *prev_node = node->prev_tlp;
      Node *next_node = node->next_tlp;
      
      if (prev_node != 0) {
	/* set the pointer to the new next node */
	prev_node->next_tlp = next_node;
      } else {
	/* this-> is the first node in the sequence            */
#ifdef EBUG_SAFE
	if ( node != tlp_begin[node->tlp_index] ) {
	  cout <<"exclude ERROR" << endl; exit(1);
	}
#endif
	tlp_begin [node->tlp_index] = next_node;
      }
      
      if (next_node != 0) {
	next_node->prev_tlp = prev_node;
      } else {
	/* this-> is the last element                           */
#ifdef EBUG_SAFE
	if ( node != tlp_end[node->tlp_index] ) {
	  cout << "exclude ERROR 2" << endl; exit(1);
	}
#endif
	tlp_end [node->tlp_index] =
	  prev_node;
      }
      
      decr_tlp_number_of_elements( node->tlp_index );                                // decrease the counter
      
      node->prev_tlp = 0;                                                            // nullify the pointers
      node->next_tlp = 0;
    } /* -------------------------------------------------------- if there are some elements in the list */  
  
  node->set_tlp ((Type_level*)0);
}





/*--------------------------------------------------------------------------------------------*/
/* Tree                                                                                       */
/*--------------------------------------------------------------------------------------------*/
Tree::Tree (Node* new_root,
	    const int s,                 // ... max number of points on edge in the finest_mesh system
	    Global_variables* gl,
	    Forest* forest,
	    Memory_management* mm, const int& proc_) :
  proc(proc_), root(new_root)
{
  // static members re-initialization
  f = forest;
  g = gl;
  m = mm;
  tsize = s;
  tsize_pos = 0;

  while ( tsize != 0 ) {
    tsize >>= 1;
    tsize_pos ++;
  }
  tsize = s;
  tsize_pos -= 2;

#if TREE_VERSION == 0
  root->level = 0;
#endif //TREE_VERSION
}



Tree::~Tree()
{
#if TREE_VERSION == 0
  delete root;
#elif TREE_VERSION == 1
  m->deallocate_node (root);
#endif //TREE_VERSION 
}





//   Returns the number of nodes created or amended.
//   Nodes with ID == significant_id or adjacent_id will be inserted into type-level link system.
//   'the_node' will be returned as the node created or found.
//   front_tll_marker - add to back of the list (1), add to front(0), do nothing (other value).
int Tree::add_by_coordinates (const int index_on_finest_mesh [],
			      Node*& the_node, const int& id_in, const int front_tll_marker)
{
//   bool verb = false;
//   if ((index_on_finest_mesh[0]==0)&&(index_on_finest_mesh[1]==15)) {
//     printf ("add_by_coordinates: node 0 15\n");
//     verb=true;
//   }


  unsigned int dim = g->DIM_NUM, summ_bin = 0;
  int opt_rel_coord [DIM_MAX];
  while (dim--) {
    opt_rel_coord [dim] = index_on_finest_mesh [dim] - root->ii [dim];
    summ_bin |= opt_rel_coord [dim];
  }
  if (summ_bin == 0) {   // --- this is a tree root, which might be in any list and might have any ID -------
    the_node = root;
    //    if ( (root->id == intermediate_id) || (root->id == zero_id) ) {  // 'new' root to be added
    if (root->id == intermediate_id) {  // 'new' root to be added
      root->id = id_in;
      
      //      if (root->get_tlp()) root->get_tlp()->tlp_node_exclude(root);
      //
      // WE DO NOT NEED THIS CALL
      //f->set_type_level (root, proc);
      //
      
      
      Type_level *the_list = & f->f_gho;
      if ((id_in & significant_id) || (id_in & adjacent_id))
	the_list = & f->f_sig;
      
      switch (front_tll_marker) {
      case 0:
	the_list -> tlp_node_include_f (root);
	break;
      case 1:
	the_list -> tlp_node_include_b (root);
	break;
      }
      
      return 1;
    } else {                            // roots are already in some list (or maybe not)
      root->id |= id_in;
      if ((root->id & adjacent_id) || (root->id & significant_id)) {        // if ID is changed to sig/adj
	Type_level *sig_tlp = &f->f_sig,                                              //  ...
	  *node_tlp = the_node->get_tlp();                                            //  ...
	if ( node_tlp != sig_tlp ) {                                                  //  and if node was not
	  if ( node_tlp )                                                             //  in sig/adj list
	    node_tlp->tlp_node_exclude (the_node);                                    //  then move it into
	  
	  switch (front_tll_marker) {
	  case 0:
	    sig_tlp->tlp_node_include_f (the_node);
	    break;
	  case 1:
	    sig_tlp->tlp_node_include_b (the_node);
	    break;
	  }
	  
	}
      }
      return 0;
    }
  }                    // ---------- end of tree root handling ------------------------------------------------
  
  
  
  Node *current_root = root;
  int current_size = tsize >> 1, current_root_level = 0;

#if TREE_VERSION == 0
  int nodes_created = 0, leaf_index = 0;
#elif TREE_VERSION == 1
  
  // some subdivision exists, so, allocate children (with data_ptr==0)
#ifdef childclustering_MM
  if ( !current_root->ptr ) current_root->ptr = m->allocate_nodes (0,root);
#else
  if ( !current_root->ptr ) current_root->ptr = m->allocate_nodes ();
#endif
#ifdef EBUG_MM
  if ( !current_root->ptr ) printf ("$ tree root children allocated, root->ptr=%x\n",current_root->ptr);
  else printf ("$ tree root children already exist, root->ptr=%x\n",current_root->ptr);
#endif
  
#endif //TREE_VERSION
  
  
  while (true)
    {
      int summ = 0;
      bool path [DIM_MAX];
      summ_bin = 0;
      dim = g->DIM_NUM;
      while (dim--) {
	path [dim] = (bool)(opt_rel_coord [dim] & current_size);
	summ  += path [dim] << dim;
	opt_rel_coord [dim] &= (current_size-1);
	summ_bin |= opt_rel_coord [dim];
      }
      
      
#if TREE_VERSION == 0
      
      if (summ == 0) {                                                        // it is a higher order leaf
	leaf_index += g->LEAVES;
	current_size >>= 1;
	current_root_level ++;
	continue;
      }
      if (current_root->num_leaves <= leaf_index)
	current_root->rearrange_leaves (g->LEAVES, leaf_index);
      leaf_index += summ - 1;
      
      Node* new_child =  current_root->leaves [leaf_index];
#elif TREE_VERSION == 1
      
      Node* new_child =  current_root->ptr + summ;
#ifdef EBUG_MM
      printf ("$ new_clild = %x + %d = %x\n",current_root->ptr,summ,new_child);
#endif
#endif //TREE_VERSION
      
      
      
      if (
#if TREE_VERSION == 0
	  new_child == 0
#elif TREE_VERSION == 1
	  new_child->data_ptr == 0
#endif //TREE_VERSION
	  ) {                         // add the new child ----------------
	
#if TREE_VERSION == 0
	new_child =  m->allocate_node ();
	nodes_created ++;
	current_root->leaves [leaf_index] = new_child;
	new_child->level = current_root_level + 1;          // set internal level
#elif TREE_VERSION == 1
	new_child->data_ptr = 1;
#endif //TREE_VERSION
	
	dim = g->DIM_NUM;
	while (dim--) {                                                                         // set coordinates
	  new_child->ii [dim] = ( current_root->ii [dim] +
				  current_size * path [dim] );
	}

#if TREE_VERSION == 0
	f->set_type_level (new_child, proc);              // set tlp (for intermediate also)
#elif TREE_VERSION == 1
	for ( dim=0; dim<g->VAL_NUM;   ++dim) new_child->val [dim] = 0.0;
        for ( dim=0; dim<g->VAL_NUM_I; ++dim) new_child->ival [dim] = 0;
#endif //TREE_VERSION
	
	if (summ_bin == 0) {                              // it's the target node !!!
	  new_child->id = id_in;                          //  set ID, the new one
	  the_node = new_child;
	  
#if TREE_VERSION == 1
	  f->set_type_level (new_child, proc);           //  tlp
#endif //TREE_VERSION
	  
	  
	  Type_level *the_list = & f->f_gho;
	  if ((id_in & significant_id) || (id_in & adjacent_id))
	    the_list = & f->f_sig;
	  
	  switch (front_tll_marker) {
	  case 0:
	    the_list -> tlp_node_include_f (new_child);
	    break;
	  case 1:
	    the_list -> tlp_node_include_b (new_child);
	    break;
	  }
	  
	  
#if TREE_VERSION == 0
	  return nodes_created;  // totally new ID set, inclusion into the lists is appropriate
#elif TREE_VERSION == 1
	  return 1; //data_nodes_created;//1
#endif //TREE_VERSION
	  
	}
      } else {                        // do not add a child ----------------
	if (summ_bin == 0) {                               // it's the target node !!!
	  if (new_child->id == intermediate_id) {                 // an intermediate
	    //
	    // TLP HAS BEEN PREVIOUSLY SET FOR ANY NODE
	    //f->set_type_level (new_child, proc);                //  tlp
	    //
	    
	    new_child->id = id_in;                                //  ID, the new one
	    
#if TREE_VERSION == 0
	    nodes_created ++;
#endif //TREE_VERSION
	    
	    Type_level *the_list = & f->f_gho;
	    if ((id_in & significant_id)                                             //  tlp
		|| (id_in & adjacent_id))
	      the_list = & f->f_sig;
	    
	    switch (front_tll_marker) {
	    case 0:
	      the_list -> tlp_node_include_f (new_child);
	      break;
	    case 1:
	      the_list -> tlp_node_include_b (new_child);
	      break;
	    }
	    
	    the_node = new_child;
	    
#if TREE_VERSION == 0
	    return nodes_created;
#elif TREE_VERSION == 1
	    return 1;
#endif //TREE_VERSION
	    
	  }
	  the_node = new_child;
	  the_node->id |= id_in;                                                   // ID, set one bit only
	  if ((new_child->id & adjacent_id) || (new_child->id & significant_id)) {        // if ID is changed to sig/adj
	    Type_level *sig_tlp = &f->f_sig,                                              //  ...
	      *node_tlp = the_node->get_tlp();                                            //  ...
	    if ( node_tlp != sig_tlp ) {                                                  //  and if node was not
	      if ( node_tlp )                                                             //  in sig/adj list
		node_tlp->tlp_node_exclude (the_node);                                    //  then move it into
	      
	      switch (front_tll_marker) {
	      case 0:
		sig_tlp->tlp_node_include_f (the_node);
		break;
	      case 1:
		sig_tlp->tlp_node_include_b (the_node);
		break;
	      }
	      
	    }
	  }
	  // 	    if (verb) printf ("we are here, node tlp=%x\n",the_node->get_tlp());

#if TREE_VERSION == 0
	  return nodes_created;
#elif TREE_VERSION == 1
	  return 0;
#endif //TREE_VERSION
	  
	}
      }
      
      current_root_level ++;
      current_size >>= 1;
      current_root = new_child;
      
#if TREE_VERSION == 0
      leaf_index = 0;
#elif TREE_VERSION == 1
      
      // some subdivision exists, so, allocate children (with data_ptr==0)
#ifdef childclustering_MM
      if ( ! current_root->ptr )
	current_root->ptr = m->allocate_nodes
	  ( current_root_level, current_root,  // standard childclustering_MM call (level, node)
	    summ );                            // child index of the node itself,
      //                                       // will play if the next level has been already allocated
#else
      if ( ! current_root->ptr ) current_root->ptr = m->allocate_nodes ();
#endif //childclustering_MM
#ifdef EBUG_MM
      if ( !current_root->ptr ) printf ("$ %x children allocated, root->ptr=%x\n",current_root,current_root->ptr);
      else printf ("$ %x children already exist, root->ptr=%x\n",current_root,current_root->ptr);
#endif

#endif //TREE_VERSION
      
    }
}






// void Tree::calculate_path_2 (std::vector <int>& path,
// 			     const int aim_coord [])
// {
//   opt_path_size = 0;
//   for (int dim=0; dim<DIM_NUM; dim++) {
//     int index = dim;
//     std::bitset <32> rel_coord =  (aim_coord[dim] - root->ii [dim]); 
//     int b=tsize_pos;
//     do {
//       b--;
//       opt_path [index] = rel_coord [b];
//       index += DIM_NUM;
//       opt_path_size ++;
//     } while (b);
//   }
// }
// void Tree::calculate_path_1 (std::vector <bool>& path,
// 			     const int aim_coord [])
// {
//   int summ = 0,
//     dim = DIM_NUM,
//     opt_rel_coord [DIM_NUM];
//   do {
//     dim --;
//     opt_rel_coord [dim] = aim_coord [dim] - root->ii [dim];
//     summ |= opt_rel_coord [dim];
//   } while (dim);
//   if (summ == 0)
//     return;
//   int current_size = tsize;
//   while (summ) {
//     current_size >>= 1;
//     summ = 0;
//     for (int dim=0; dim<DIM_NUM; dim++) {
//       path.push_back ( (bool) (opt_rel_coord [dim] & current_size) );
//       opt_rel_coord [dim] &= (current_size-1);
//       summ |= opt_rel_coord [dim];
//     }
//   }
// }

//------------------------------------------------------------------------------------------------------
// Interpolate function values to the given locations
//  x         - array of points, FORTRAN x(1:dim,1:xsize)
//  f         - returned array of interpolated function values, FORTRAN f(1:vsize,1:xsize)
//------------------------------------------------------------------------------------------------------
void Forest::interpolate (const Value*const x, Value* f, const int xsize, const int*const vars, const int vsize,
			  const std::vector< std::vector<float> >& xx, const int order)
{
#if TREE_VERSION == 0
  int
    ii = 0,                              // index of x[] ( x1,y1,z1, x2,y2,z2, ... )
    iii = 0,                             // index of f[]
    dim;
  
  
  // check variable numbers
  for ( dim=0; dim<vsize; ++dim )
    if ( vars[dim] > g->VAL_NUM ) {
      fprintf (stderr, "Forest::interpolate () : requested variable #%d of %d real variables\n",
	       vars[dim], g->VAL_NUM);
      exit (1);
    }
  
  
  std::vector< std::vector<float> > mx(g->DIM_NUM);  // xx array for level 0 only
  for ( dim=0; dim<g->DIM_NUM; ++dim ) {             // x_space is the coarse spacing
    int msize = FINEST_MESH[dim] / x_space;
#ifdef EBUG
    printf ("msize=%d\n",msize );
#endif
    mx[dim].reserve(msize+1);
    for (int i=0; i<=msize; ++i)
      mx[dim].push_back (xx[dim][i*x_space]);
  }
#ifdef EBUG
  printf ("bounding box size is %d\n", g->LEAVES+1);
#endif
  

  Node** box = new Node* [g->LEAVES+1];           // bounding box for a point
  float* f_tmp = new float [g->LEAVES+1];         // temporal output for linear interpolation
  
  std::vector< int > i_p(g->DIM_NUM,1), xyz(g->DIM_NUM,0);        // i_p array
  std::vector< float > fxyz(g->DIM_NUM);
  for ( dim=1; dim<g->DIM_NUM; ++dim )                            // which is
    i_p[dim] = i_p[dim-1]*                                        // 1,Mx,MxMy,etc
      ( (FINEST_MESH[dim-1] >> x_space_pos) + 1 - PERIODIC[dim] ) ;
  
  
  for (int ix=0; ix<xsize; ++ix) {    
    
    int tree_number = 0;
    
    // locate the tree point belongs to
    for ( dim=0; dim<g->DIM_NUM; ++dim, ++ii ) {
      // check periodicity
      if ((x[ii] < mx[dim][0]) || (x[ii] > mx[dim][mx[dim].size()-1])) {
	fprintf (stderr, "Error in Forest::interpolate ()\n");
	fprintf (stderr, " point's %d coordinate %f is out of limits [%f %f]\n",
		 ix,(float)x[ii],(float)mx[dim][0],(float)mx[dim][mx[dim].size()-1]);
	exit (1);
      }
      // locate tree for the current dimension
      for (int i=1; i<mx[dim].size(); ++i) {
	if (x[ii] <= mx[dim][i]) {
	  xyz[dim] = i-1;
	  break;
	}
      }
      fxyz[dim] = x[ii];                     // copy coordinate vector
      tree_number += (xyz[dim] * i_p[dim]);
      
#ifdef EBUG
      printf (" xyz[%d]=%d\n tree_number = %d\n",dim,xyz[dim],tree_number);
      printf (" x[ii]=%f\n",x[ii]);
#endif
    } // of for ( dim=0; dim<g->DIM_NUM; ++dim, ++ii )
    
    // write initial bounding box
    make_amr_box ( tree_number, box, i_p, xyz );
    
#ifdef EBUG
    printf ("initial "); print_box (box);
    printf ("final tree_number = %d\n",tree_number);
#endif
    
    // search for the smallest bounding box
    int side = x_space;                            // size of that box
    int looking_for_level = 1;                     // next bisection
    while (
	   search_smallest_amr ( box, side, fxyz, xx, looking_for_level )) {
#ifdef EBUG
      printf ("in progress "); print_box (box);
      int ret = test_amr_box (box, side, fxyz, xx);
      if (ret) {
	test_amr_box (box, side, fxyz, xx, true);
	printf ("test_amr_box () error %d, looking_for_level = %d\n",
		ret,looking_for_level);
	exit (1);
      }
#endif
      looking_for_level ++;
    }

#ifdef EBUG
    printf ("final (of size %d) ",side); print_box (box);
    test_amr_box (box, side, fxyz, xx, true);
#endif
    
    // set interpolation for the smallest bounding box
    if (order == 0)
      interpolate_to_box ( 0, 0, 0,
			   box, f_tmp, vars, vsize, fxyz, xx,
			   iii, f );
    else
      interpolate_to_box ( 0, 0, 0,
			   box, f_tmp, vars, vsize, fxyz, xx,
			   iii, f );
    
  } // of for (int ix=0; ix<xsize; ++ix)

  if (order != 0) {
    printf ("WARNING !\nHigh order interpolation is not ready for the direct mode !\n");
    printf ("Using linear interpolation instead.\n");
  }


#ifdef EBUG
  printf ("Forest::number_of_trees=%d\n",number_of_trees);
#endif
  
  delete [] box;
  delete [] f_tmp;
#else //TREE_VERSION
  terminate ("Forest::interpolate()");

#endif //TREE_VERSION
}



// dim-3rd order interpolation inside the box
// assume that iwlt is stored in DB, i.e add_var_marker == 1
void Forest::interpolate_to_box_h (const Value*const u,   // u(1:nwlt, 1:n_var)
				   const Value*const du,  // du(1:n_var,1:nwlt,1:dim)
				   const int nwlt,
				   const int add_var_marker, // 0-functions, 1-iwlt stored in DB
				   Node**box,
				   float* f_tmp,                                   // [(g->LEAVES+1)*vsize]
				   float* df_tmp,                                  // [(g->LEAVES+1)*vsize*g->DIM_NUM]
				   const int*const vars,                           // vars(1:vsize)
				   const int vsize,
				   const std::vector< float >& fxyz,               // coordinate to interpolate into
				   const std::vector< std::vector<float> >& xx,    // xx[dim][nxyz]
				   int& iii,                                       // global index in f
				   Value* f)                                       // output values f(1:vsize,1:xsize)
{
#if TREE_VERSION == 0
#ifdef EBUG1
  printf ("interpolate_to_box_h () --------------- START ------------\n");
#endif
  // write temporary arrays f_tmp[] and df_tmp[]
  // assume that iwlt are stored in DB
  for (int nv=0; nv<vsize; ++nv)
    for (int i=0; i<=g->LEAVES; ++i){
      f_tmp[i + nv*(g->LEAVES+1)] =                                                // u -> f_tmp(1:2^dim, 1:vsize)
	u[ box[i]->ival[IWLT_I_P] + nwlt * (vars[nv]-1) -1 ];                      //
#ifdef EBUG1
      printf ("f_tmp(node=%d,var=%d) = %f\n",i,nv,f_tmp[i + nv*(g->LEAVES+1)]);
      printf ("df_tmp(node=%d,var=%d)(1:%d) =",i,nv,g->DIM_NUM);
#endif
      for (int dim=0; dim<g->DIM_NUM; ++dim) {
	df_tmp[i + nv*(g->LEAVES+1) + dim*(g->LEAVES+1)*vsize] =                   // du -> df_tmp(1:2^dim, 1:vsize, 1:dim)
	  du[ (vars[nv]-1) + (box[i]->ival[IWLT_I_P]-1)*vsize + dim*nwlt*vsize ];  //
#ifdef EBUG1
	printf (" %f",df_tmp[i + nv*(g->LEAVES+1) + dim*(g->LEAVES+1)*vsize]);
#endif
      }
#ifdef EBUG1
      printf ("\n");
#endif
    }

  // start dimension reduction
  int max_num_interp = (g->LEAVES+1) >> 1;                // number of 1d interpolations required
  for (int cur_dim=g->DIM_NUM-1; cur_dim>=0; --cur_dim) {
#ifdef EBUG1
    printf ("\ncurrent dimension=%d\n",cur_dim);
#endif
    
    // perform the required 1d interpolations
    for (int num_interp=0; num_interp<max_num_interp; ++num_interp) {  // overwrite first 2^{dim-1} values of f_tmp
#ifdef EBUG1
      printf ("\tinterpolation=%d\n",num_interp);
#endif
      float f_1, f_2, df_1, df_2;                              // F(1), F(2), F'(1), F'(2) for the interpolation
      float x,s;                                               // coordinate x \in [0,s]
      x = fxyz[cur_dim] - xx[cur_dim][box[0]->ii[cur_dim]];
      s = xx[cur_dim][box[g->LEAVES]->ii[cur_dim]] - xx[cur_dim][box[0]->ii[cur_dim]];
#ifdef EBUG1
      printf ("x = %f\ts = %f\n",x,s);
#endif
      for (int vn=0; vn<vsize; ++vn) {
	int index_f1 = num_interp + vn*(g->LEAVES+1);
	f_1 = f_tmp[index_f1];                                                        // this will be overwritten by reduced box value
	f_2 = f_tmp[index_f1 + max_num_interp];                                       // this will not be used after the reduction
	int index_df1 = num_interp + vn*(g->LEAVES+1) + cur_dim*(g->LEAVES+1)*vsize;
	df_1 = df_tmp[index_df1];
	df_2 = df_tmp[index_df1 + max_num_interp];
#ifdef EBUG1
	printf ("\tf_1 = f_tmp[%d] = %f\n\tf_2 = f_tmp[%d] = %f\n",index_f1, f_1, index_f1+max_num_interp, f_2);
	printf ("\tdf_1 = df_tmp[%d] = %f\n\tdf_2 = df_tmp[%d] = %f\n",index_df1, df_1, index_df1+max_num_interp, df_2);
#endif
	// interpolate function:
	// (3rd order, F(1), F(2) and F'(1), F'(2) based)
	f_tmp[ index_f1 ] =  f_1 + df_1*x - (x*x/s/s)*( 3*(f_1-f_2) + s*(df_2+2*df_1) ) + (x*x*x/s/s/s)*( 2*(f_1-f_2) + s*(df_1+df_2) );
	
	// (3rd order, F(1), F(2), F(0), F(2) based, where F(0) and F(2) are from central approximations of F'(1) and F'(2))
	// f_tmp[ index_f1 ] =  f_1*( (x+s)*(s-x)*(3*s-2*x)/3/s/s/s ) +
	// f_2*( x*(2*s-x)*(2*x+s)/3/s/s/s ) + x*(s-x)/3/s/s* ( df_1*(2*s-x) - df_2*(x+s) );
	
	// (linear, F(1) and F(2) based)
	// f_tmp[ index_f1 ] = ((s-x)*f_1 + (x)*f_2)/s;
	
#ifdef EBUG1
	printf ("\tinterpolated function f_tmp[%d] = %f\n",index_f1, f_tmp[ index_f1 ]);
	printf ("\tinterpolated derivatives df_tmp:\n");
#endif	
	// interpolate derivatives (linear)
	for (int ddim=0; ddim<cur_dim; ++ddim) {  // all the derivatives exept the one for the dimension cur_dim
	  df_tmp[ num_interp + vn*(g->LEAVES+1) + ddim*(g->LEAVES+1)*vsize ] =
	    ( (s-x) * df_tmp[ num_interp + vn*(g->LEAVES+1) + ddim*(g->LEAVES+1)*vsize ] +
	      ( x ) * df_tmp[ num_interp + vn*(g->LEAVES+1) + ddim*(g->LEAVES+1)*vsize + max_num_interp] )/s;
#ifdef EBUG1
	  printf ("\t\t ddim=%d, df(x)[%d] & df(s-x)[%d] = ",ddim,num_interp + vn*(g->LEAVES+1) + ddim*(g->LEAVES+1)*vsize,
		  num_interp + vn*(g->LEAVES+1) + ddim*(g->LEAVES+1)*vsize + max_num_interp);
	  printf ("%f\n",df_tmp[ num_interp + vn*(g->LEAVES+1) + ddim*(g->LEAVES+1)*vsize ]);
#endif
	}
      }
    }
    max_num_interp >>= 1;                       // update number of 1d interpolations required
  }
  
  // write output values
  for (int nvv=0; nvv<vsize; ++nvv, ++iii)
    f[iii] = f_tmp[ nvv*(g->LEAVES+1) ];

#else //TREE_VERSION
  terminate ("Forest::interpolate_to_box_h()");
#endif //TREE_VERSION
}

// dim-linear interpolation inside the box
// set output according to vars[vsize] into f_tmp[g->LEAVES+1]
// add_var_marker: 0-functions stored in DB, 1-iwlt stored in DB
void Forest::interpolate_to_box (const Value*const u, const int nwlt, const int add_var_marker,
				 Node**box, float* f_tmp, const int*const vars, const int vsize,
				 const std::vector< float >& fxyz, const std::vector< std::vector<float> >& xx,
				 int& iii, Value* f)
{
#if TREE_VERSION == 0
#ifdef EBUG1
  printf ("interpolate_to_box () --------------- START ------------\n");
  //return;
#endif
  for (int i=0; i<=g->LEAVES; ++i) {
    f_tmp[i] = 1.0;                           // 000 <-> xyz, 001 <-> (s-x)yz, etc
    for (int dim=0; dim<g->DIM_NUM; ++dim)
      if ( (1<<dim) & i ) {
	f_tmp[i] *=
	  fxyz[dim] - xx[dim][box[0]->ii[dim]];             // i.e  (x)
#ifdef EBUG1
	printf ("\ti=%d, dim=%d, *= %f - %f = %f\n",i,dim,xx[dim][box[0]->ii[dim]],fxyz[dim],
		fxyz[dim] - xx[dim][box[0]->ii[dim]]);
#endif
      }
      else {
	f_tmp[i] *=
	  xx[dim][box[g->LEAVES]->ii[dim]] - fxyz[dim];     // i.e. (s-x)
#ifdef EBUG1
	printf ("\ti=%d, dim=%d, *= %f - %f = %f\n",i,dim,xx[dim][box[g->LEAVES]->ii[dim]], fxyz[dim],
		xx[dim][box[g->LEAVES]->ii[dim]] - fxyz[dim]);
#endif
      }
  }
  // as a result of that we have (for 2D) f_tmp[4] ={ xy, (s-x)y, x(s-y), (s-x)(s-y) }
  
#ifdef EBUG1
  printf ("f_tmp = ");
  for (int i=0; i<=g->LEAVES; ++i)
    printf (" %f",f_tmp[i]);
  printf ("\n");
#endif


  float delta = 1.0;
  for (int dim=0; dim<g->DIM_NUM; ++dim)
    delta *=
      xx[dim][box[g->LEAVES]->ii[dim]] - xx[dim][box[0]->ii[dim]];
  // as a result of that we have (for 2D) delta = s^2

#ifdef EBUG1
  printf ("delta = %f\n",delta);
#endif
  
  if (add_var_marker==0)                       // functions are stored in DB
    for (int nv=0; nv<vsize; ++nv, ++iii) {
      float f_tmp_nv = 0;                      // final value for that variable
      for (int i=0; i<=g->LEAVES; ++i)
	f_tmp_nv += f_tmp[i] * box[i]->val[vars[nv]-1];
      f[iii] = f_tmp_nv / delta;
#ifdef EBUG1
      printf ("f[%d] = %f\n",nv, f[iii]);
#endif
    }
  else                                         // iwlt are stored in DB
    for (int nv=0; nv<vsize; ++nv, ++iii) {
      float f_tmp_nv = 0;                      // final value for that variable
      for (int i=0; i<=g->LEAVES; ++i)
	f_tmp_nv += f_tmp[i] *
	  u[ box[i]->ival[IWLT_I_P] + nwlt * (vars[nv]-1) -1 ];
      f[iii] = f_tmp_nv / delta;
#ifdef EBUG1
      printf ("f[%d] = %f\n",nv, f[iii]);
#endif
    }
  
#ifdef EBUG1
  printf ("interpolate_to_box () --------------- END --------------\n");
#endif

#else //TREE_VERSION
  terminate ("Forest::interpolate_to_box()");
#endif //TREE_VERSION
}




// print box integer coordinates
#ifdef EBUG
void Forest::print_box (Node** box)
{
#if TREE_VERSION == 0
  printf ("box: [");
  for (int i=0; i<=g->LEAVES; ++i) {
    printf ("( ");
    for (int j=0; j<g->DIM_NUM; ++j) printf ("%d ",box[i]->ii[j]); printf (")");
  }
  printf ("]\n");
#else //TREE_VERSION
  terminate ("Forest::print_box()");
#endif //TREE_VERSION
}
#endif // EBUG




// return true if the box is corrupted
#ifdef EBUG
int Forest::test_amr_box (Node** box, const int side,
			   const std::vector< float >& fxyz,
			   const std::vector< std::vector<float> >& xx,
			  const bool verb)
{
#if TREE_VERSION == 0
  int min[DIM_MAX], max[DIM_MAX];        // min/max
  float fmin[DIM_MAX], fmax[DIM_MAX];
  for (int i=0; i<g->DIM_NUM; ++i) {
    min[i] = box[0]->ii[i];
    fmin[i] = xx[i][min[i]];
    max[i] = box[g->LEAVES]->ii[i];
    fmax[i] = xx[i][max[i]];
    if (( fxyz[i] < fmin[i] )||( fxyz[i] > fmax[i] ))
      return 1;
    if ( max[i]-min[i] != side )
      return 2;
  }
  if (verb ) {
    printf ("min = [ "); for (int i=0; i<g->DIM_NUM; ++i) printf ("%d ",min[i]); printf ("]\n");
    printf ("max = [ "); for (int i=0; i<g->DIM_NUM; ++i) printf ("%d ",max[i]); printf ("]\n");
    printf ("fmin = [ "); for (int i=0; i<g->DIM_NUM; ++i) printf ("%f ",fmin[i]); printf ("]\n");
    printf ("fmax = [ "); for (int i=0; i<g->DIM_NUM; ++i) printf ("%f ",fmax[i]); printf ("]\n");
  }
  for (int i=0; i<=g->LEAVES; ++i)
    for (int dim=0; dim<g->DIM_NUM; ++dim)
      if ((box[i]->ii[dim] > max[dim]) || (box[i]->ii[dim] < min[dim]))
	return 3;
  return 0;
#else //TREE_VERSION
  terminate ("Forest::test_amr_box()");
#endif //TREE_VERSION
}
#endif // EBUG





#if TREE_VERSION == 0
// called from Forest::interpolate
// locate bounding box at maximum possible level
// return true if bisection was successful
bool Forest::search_smallest_amr (Node** box, int& side,
				  const std::vector< float >& fxyz,
				  const std::vector< std::vector<float> >& xx,
				  const int looking_for_level)
{
  int bn;
  if ( box[0]->num_leaves <
       g->LEAVES*(looking_for_level-
		  box[0]->level) )
    return false;                                         // node 0 has children
  for ( int chnum=g->LEAVES*(looking_for_level-
			     box[0]->level-1);
	chnum<g->LEAVES*(looking_for_level-
			 box[0]->level);
	++chnum )                                         // and all the children
    if (( ! box[0]->leaves[chnum] )||                     // have to be present
	( ! box[0]->leaves[chnum]->id & significant_id))  // and significant
      return false;
  
  for ( int num_in_box = 1; num_in_box<g->LEAVES; ++num_in_box ) { // for all except 000 and 111
    //printf ("\tnum_in_box=%d\n",num_in_box);
    for ( int i=num_in_box+1; i<=g->LEAVES; ++i ) {                //  pass through all transmutations
      //printf ("\t\ti=%d, (i&num_in_box)=%d\n",i,i & num_in_box);
      if ( i & num_in_box == num_in_box ) {                        //  toward 111
	//printf ("\t\ttrue: i=%d\n",i);
	int min_leaves_array = g->LEAVES*(looking_for_level-box[num_in_box]->level);
	int child_index = i-num_in_box-1 + min_leaves_array - g->LEAVES;
	//printf ("\t\ttested child # %d\n", child_index );
	if (( box[num_in_box]->num_leaves < min_leaves_array ) ||
	    ( ! box[num_in_box]->leaves[child_index] ) ||
	    ( ! box[num_in_box]->leaves[child_index]->id & significant_id))
	  return false;
      }
    }
  }
  //printf ("the box can be bisected\n");
  // locate new node for box[0]
  int chnum_new_0 = g->LEAVES*(looking_for_level-box[0]->level-1), // initial shift in leaves array
    chnum_new_add = 0;                                             // vector of box[0] shift in space
  side >>= 1;                                                      // new box size
  for (int dim=0; dim<g->DIM_NUM; ++dim) {
    float middle = xx[dim][box[0]->ii[dim] + side];
    chnum_new_add += (((int) (fxyz[dim] > middle)) << dim);
  }
  // chnum_new_add == 0 <--> box[0] is the same
  if ( chnum_new_add ) {
    //box[0] = box[0]->leaves[chnum_new_0+chnum_new_add-1];                          !!!!!!!!!!!!
    //printf ("\tnew box[0] is the leaf %d: [ ",chnum_new_add);
    //for (int dim=0; dim<g->DIM_NUM; ++dim) printf ("%d ",
    //						   box[0]->
    //						   leaves[chnum_new_0+chnum_new_add-1]->ii[dim]);
    //printf ("]\n");
  } else {
    for (int lf=0; lf<g->LEAVES; ++lf) { // LEAVES+1 == boxsize
      box[lf+1] = box[0]->leaves[g->LEAVES*(looking_for_level-box[0]->level-1) + lf];
    }
    //printf ("\tnew box[0] is the leaf %d: [ ",chnum_new_add);
    //for (int dim=0; dim<g->DIM_NUM; ++dim) printf ("%d ",box[0]->ii[dim]);
    //printf ("]\n");
    return true;
  }

  Node * new_box[1<<DIM_MAX];

  // make smaller box if chnum_new_add > 0
  //printf ("vec=%d\n",chnum_new_add);
  for ( bn=0; bn<g->LEAVES; ++bn) {
    int code[DIM_MAX];                         // trinary code for the new box entry 0,1,2
    //printf ("\tcode for %d is ( ",bn+1);
	int dim;
    for ( dim=0; dim<g->DIM_NUM; ++dim) {                      // in xyz order
      code[dim] = ((bool)(chnum_new_add & (1<<dim))) +            // vec 01 and # 01
	((bool)((bn+1) & (1<<dim)));                              // produce 02, etc
      //printf ("%d ",code[dim]);
    } //printf (")\n");
    int root_index = 0;                        // root index for the new box node
    for ( dim=0; dim<g->DIM_NUM; ++dim)
      root_index += ( (code[dim] == 2) << dim );                  // 122 produce box[ 011 ]
    int child_index = 0;                       // child index for the root
    for ( dim=0; dim<g->DIM_NUM; ++dim)
      if ( code[dim] != 2 )
	child_index += (code[dim] << dim);                        // 122 makes ->leaves[ 100 ]
    //printf ("\troot_index = %d, child_index = %d\n",root_index,child_index);
    if (child_index)
      new_box[bn] = box[root_index]->
	leaves[ child_index - 1 +g->LEAVES*( looking_for_level - box[root_index]->level - 1) ];
    else
      new_box[bn] = box[root_index];

    //printf ("\tnew box[%d] is : [ ",bn+1 );
    //for (int dim=0; dim<g->DIM_NUM; ++dim)
    //  printf ("%d ",new_box[bn]->ii[dim]);
    //printf ("]\n");
  }
  box[0] = box[0]->leaves[chnum_new_0+chnum_new_add-1];
  for ( bn=0; bn<g->LEAVES; ++bn)
    box[bn+1] = new_box[bn];
  return true;
}
#endif //TREE_VERSION






#if TREE_VERSION == 0
// called from Forest::interpolate
// locate bounding box at level 0
void Forest::make_amr_box (const int tree_number, Node**box,
			   const std::vector< int > & i_p, const std::vector< int > & xyz)
{
  box[0] = trees[tree_number]->get_root ();
  
  for (int i=1; i<=g->LEAVES; ++i) {          // 00 01 10 11
    std::vector< int > xyz_copy = xyz;
    int tree_number_copy = 0;
    for (int dim=0; dim<g->DIM_NUM; ++dim) {  // sweep through bit positions
      xyz_copy[dim] += static_cast<bool>((1<<dim) & i);
      tree_number_copy += xyz_copy[dim] * i_p[dim];
      //printf ("\txyz_copy[%d]=%d,%d, i=%d\n",dim,xyz_copy[dim],((1<<dim) & i),i);
    }
    //printf ("\tbox[%d] <-> tree %d\n",i,tree_number_copy);
    box[i] =
      trees[tree_number_copy]->get_root ();
    //printf ("\tcoord= %d %d\n",box[i]->ii[0],box[i]->ii[1]);
  }
}
#endif //TREE_VERSION





P_SIZE Tree::test_by_coordinates_1 (const int index_on_finest_mesh [])
{
  int dim = g->DIM_NUM,
    summ = 0;
  int opt_rel_coord [DIM_MAX];
  while (dim--) {
    opt_rel_coord [dim] = index_on_finest_mesh [dim] - root->ii [dim];               // relative coordinates
    summ |= opt_rel_coord [dim];
  }
  if (summ == 0) {               // --- it is the root -----------------
    //    if ((root->id == intermediate_id) || (root->id == zero_id))
    if (root->id == intermediate_id)
      return (P_SIZE) 0;
    else
      return (P_SIZE) root;
  }                              // --- end of tree root handling ------
  
  int current_size = tsize >> 1;
  Node* current_root = root;
  
#if TREE_VERSION == 0
  int leaf_index = 0;  // leaf index, where the next root should be
  while (true) {
    if (current_root->num_leaves == 0) { return (P_SIZE) 0; }
    dim = g->DIM_NUM;
    summ = 0;
    int summ_bin = 0;
    while (dim--) {
      summ  += ((bool)(opt_rel_coord [dim] & current_size)) << dim;              // i.e    ZYX bit ordering
      opt_rel_coord [dim] &= (current_size-1);
      summ_bin |= opt_rel_coord [dim];
    }
    if (summ == 0) {
      leaf_index += g->LEAVES;
      current_size >>= 1;
      continue;
    }
    leaf_index += summ - 1;

    if (leaf_index >= current_root->num_leaves) { return (P_SIZE) 0; }
    if (summ_bin == 0) { return (P_SIZE) current_root->get_child( leaf_index ); }
    
    current_root = current_root->get_child( leaf_index );
    leaf_index = 0;
    if (current_root == 0) { return (P_SIZE) 0; }
    current_size >>= 1;                                                              // length of the step to the next root
  }
#elif TREE_VERSION == 1
  while (true) {
    if ( !current_root->ptr ) return (P_SIZE) 0;
    dim = g->DIM_NUM;
    summ = 0;
    int summ_bin = 0;
    while (dim--) {
      summ  += ((bool)(opt_rel_coord [dim] & current_size)) << dim;              // i.e    ZYX bit ordering
      opt_rel_coord [dim] &= (current_size-1);
      summ_bin |= opt_rel_coord [dim];
    }
    
    current_root = current_root->get_child( summ );
    
    if ( !current_root->data_ptr ) // data_ptr has been initialized for all intermediate nodes
      return (P_SIZE) 0;
    if (summ_bin == 0)
      return (P_SIZE) current_root;
    
    current_size >>= 1;                                                              // length of the step to the next root
  }
#endif //TREE_VERSION
}





Value* Tree::test_by_coordinates_nonzero (const int * const index_on_finest_mesh)
{
#if TREE_VERSION == 0
  int dim = g->DIM_NUM, summ = 0, opt_rel_coord [DIM_MAX];
  while (dim--) {
    opt_rel_coord [dim] = index_on_finest_mesh [dim] - root->ii [dim];               // relative coordinates
    summ |= opt_rel_coord [dim];
  }
//   {
//     opt_rel_coord [2] = index_on_finest_mesh [2] - root->ii [2];
//     summ |= opt_rel_coord [2];
//     opt_rel_coord [1] = index_on_finest_mesh [1] - root->ii [1];
//     summ |= opt_rel_coord [1];
//     opt_rel_coord [0] = index_on_finest_mesh [0] - root->ii [0];
//     summ |= opt_rel_coord [0];
//   }

  if (summ == 0) {               // --- it is the root -----------------
    if (root->id == intermediate_id)
      return f->zero_val;
    else
      return root->val;
  }                              // --- end of tree root handling ------
  
  int current_size = tsize >> 1;
  Node* current_root = root;
  int leaf_index = 0;                                                                // leaf index, where the next root should be

  while (true) {
    if (current_root->num_leaves == 0) return f->zero_val;
    dim = g->DIM_NUM;
    summ = 0;
    int summ_bin = 0;
    while (dim--) {
      summ  += ((bool)(opt_rel_coord [dim] & current_size)) << dim;              // i.e    ZYX bit ordering
      opt_rel_coord [dim] &= (current_size-1);
      summ_bin |= opt_rel_coord [dim];
    }
//     {
//       summ  += ((bool)(opt_rel_coord [2] & current_size)) << 2;
//       opt_rel_coord [2] &= (current_size-1);
//       summ_bin |= opt_rel_coord [2];
//       summ  += ((bool)(opt_rel_coord [1] & current_size)) << 1;
//       opt_rel_coord [1] &= (current_size-1);
//       summ_bin |= opt_rel_coord [1];
//       summ  += ((bool)(opt_rel_coord [0] & current_size)) << 0;
//       opt_rel_coord [0] &= (current_size-1);
//       summ_bin |= opt_rel_coord [0];
//     }
    if (summ == 0) {
      leaf_index += g->LEAVES;
      current_size >>= 1;
      continue;
    }
    leaf_index += summ - 1;
    
    if (leaf_index >= current_root->num_leaves) { return f->zero_val; }
    if (summ_bin == 0) {
      if (current_root->leaves [leaf_index])
	return current_root->leaves [leaf_index]->val;
      else
	return f->zero_val;
    }

    current_root = current_root->leaves [leaf_index];
    leaf_index = 0;
    if (current_root == 0) { return f->zero_val; }

    current_size >>= 1;                                                              // length of the step to the next root
  }

#elif TREE_VERSION == 1
  int summ = 0, opt_rel_coord [DIM_MAX];

#ifndef FIX_DIM_LOOPS_DATABASE_TREE
  int dim = g->DIM_NUM;
  while (dim--) {
    opt_rel_coord [dim] = index_on_finest_mesh [dim] - root->ii [dim];               // relative coordinates
    summ |= opt_rel_coord [dim];
  }
#else //FIX_DIM_LOOPS_DATABASE_TREE
  for (int dim=0; dim<g->DIM_MAX; ++dim) {
    opt_rel_coord [dim] = index_on_finest_mesh [dim] - root->ii [dim];
    summ |= opt_rel_coord [dim];
  }
#endif //FIX_DIM_LOOPS_DATABASE_TREE

  if (summ == 0) {               // --- it is the root -----------------
    if (root->id == intermediate_id)
      return f->zero_val;
    else
      return root->val;
  }                              // --- end of tree root handling ------
  
  int current_size = tsize >> 1;
  Node* current_root = root;

  while (true) {
    if ( !current_root->ptr ) return f->zero_val;
    summ = 0;
    int summ_bin = 0;

#ifndef FIX_DIM_LOOPS_DATABASE_TREE
    dim = g->DIM_NUM;
    while (dim--) {
      summ  += ((bool)(opt_rel_coord [dim] & current_size)) << dim;              // i.e    ZYX bit ordering
      opt_rel_coord [dim] &= (current_size-1);
      summ_bin |= opt_rel_coord [dim];
    }
#else //FIX_DIM_LOOPS_DATABASE_TREE
    for (int dim=0; dim<DIM_MAX; ++dim) {
      summ  += ((bool)(opt_rel_coord [dim] & current_size)) << dim;
      opt_rel_coord [dim] &= (current_size-1);
      summ_bin |= opt_rel_coord [dim];
    }
#endif //FIX_DIM_LOOPS_DATABASE_TREE

    current_root = current_root->ptr + summ;
    
    
    if ( !current_root->data_ptr ) // data_ptr has been initialized for all intermediate nodes
      return f->zero_val;
    if (summ_bin == 0)
      return current_root->val;

    current_size >>= 1;                                                              // length of the step to the next root
  }

#endif //TREE_VERSION
}


//------------------------------------------------------------------------------------------------------
// depth first pass
// The nodes marked by ID==to_be_deleted or ID==intermediate_id
// will be deleted from tlp lists and be deleted completely, if possible.
// Tree roots will be deleted from tlp lists only.
//------------------------------------------------------------------------------------------------------
void Forest::clean_the_forest ()
{
  //print_info(1,0);
  if (trees==NULL) return;                                   // there are no trees -----------------
  
  int depth = g->J_MAX - g->J_TREE + 1;                      // max path length
  vector <Node*> path_node (depth, (Node*) 0);               // current path
  vector <int>   path_cindex (depth,  0);                    // current child index
  
  
  for (int si=0; si < number_of_trees; ++si ) {              // for each tree ----------------------
    
    int main_index = 0;                                        // to which position path_...[] is filled
    path_node   [0] = trees[si]->get_root();                    // initialize path's beginning
    path_cindex [0] = 0;                                        // ...
    Node *node = path_node [0];
    if ( !(path_node[0]->id & (significant_id | adjacent_id | ghost_id))) { // treat tree roots
        //O+A+AR 04.23.2011: logical bug fixed
	//if ( (path_node[0]->id == zero_id) ||                 
	// (path_node[0]->id == intermediate_id) ||
	// (path_node[0]->id == old_id) ) {
      if (path_node[0]->tlp != 0)
	path_node[0]->tlp->tlp_node_exclude (path_node[0]);
      path_node[0]->id = intermediate_id;
      path_node[0]->zero_value_vector (g->VAL_NUM, g->VAL_NUM_I);
      
#ifdef EBUG_MM
      printf ("$ set to intermediate_id root node %d %d\n",node->ii[0],node->ii[1]);
#endif
    }
    
    while (true) {                                  // the main cycle ===============================
      int ilow = path_cindex [main_index];
      int ci;
      int totc = 0;
      if (found_nonzero_child_link (node,ci,ilow,totc)) {
	main_index ++;
	path_node [main_index] = node->get_child( ci );
	path_cindex [main_index-1] = ci+1;
	path_cindex [main_index]   = 0;
	node = node->get_child( ci );

#ifdef EBUG_MM
	printf ("$ path_node[%d] : %d %d\n",main_index,node->ii[0],node->ii[1]);
#endif
	
      } else {
	//num_nodes++;
	//printf ("here1: node=%x, totc=%d, main_index=%d\n", node,totc,main_index);
	if ( ( totc == 0 ) && ( main_index > 0 ) &&
             ( !(node->id & (significant_id | adjacent_id | ghost_id)) )
             //O+A+AR 04.23.2011: logical bug fixed
	     // ( (node->id == zero_id)||(node->id == intermediate_id)||(node->id == old_id) )
	     ) {
#if TREE_VERSION == 0
	  //printf ("$ deallocate %x [%d %d], main_index=%d ",node,node->ii[0],node->ii[1],main_index);
	  //printf ("of parent %x [%d %d]\n",path_node[main_index-1],path_node[main_index-1]->ii[0],path_node[main_index-1]->ii[1]);
	  m->deallocate_node (node);                                                  // delete node
	  path_node [main_index-1] -> leaves [path_cindex[main_index-1]-1] = NULL;    // inform parent
#elif TREE_VERSION == 1
	  // delete node
          // If QUAD of nodes to be deleted, they functions has to be zeroed.
          // Currently, we zero data_ptr, and zero functions afterwards
          // inside add_by_coordinates
#ifdef childclustering_MM
#ifdef EBUG_MM
	  printf ("$ deleting node [%d, %d] of internal level %d ",node->ii[0],node->ii[1],main_index);
	  printf ("of parent %x [%d %d]\n",path_node[main_index-1],path_node[main_index-1]->ii[0],path_node[main_index-1]->ii[1]);
	  //printf ("J_TREE=%d, J_MAX=%d,FINEST_MESH=%d %d\n",J_TREE,J_MAX,g->FINEST_MESH[0],g->FINEST_MESH[1]);
	  //exit(1);
#endif //EBUG_MM
	  m->deallocate_nodes (node, main_index);
#else //childclustering_MM
#ifdef EBUG_MM
	  printf ("$ deleting node %x [%d, %d] of internal level %d ",node,node->ii[0],node->ii[1],main_index);
	  printf ("of parent %x [%d %d]\n",path_node[main_index-1],path_node[main_index-1]->ii[0],path_node[main_index-1]->ii[1]);
#endif //EBUG_MM
	  m->deallocate_nodes (node);
	  
#endif //childclustering_MM
	  
#endif //TREE_VERSION
	}

	//printf ("here: main_index=%d\n",main_index);
	main_index --;
	if (main_index < 0) break;

	path_node   [main_index+1] = NULL;
	path_cindex [main_index+1] = 0;
	node = path_node [main_index];
	//printf ("here2: node=%x\n", node);
      }
    }                                               // the main cycle ===============================
    
    for (int i=1; i< depth; i++) {                              // clear the path
      path_node   [i] = NULL;                                   // ...
      path_cindex [i] = 0;                                      // ...
    }
  }                                                          // for each tree ----------------------
}




// this subroutine is required by parallel res2vis and migrate
// count significant boundary nodes for the given tree numbers and return out_num (for out_nodes == 0)
// write out_nodes and out_vals (for out_nodes != 0)
// t_num - t_num_size pairs of (tree number, direction) - MODE1 // tree numbers - MODE0
// out_nodes in MODE 0 is pair (1D coord, ID)
// direction: 1,2,4, ... - which boundary, 0 - whole tree
void Forest::count_tree_nodes (const int*const t_num, const int t_num_size, int* out_num, long* out_nodes, Value* out_vals, const int mode) {  //!AR 2/27/2011!  int* out_nodes  changed to  long* out_nodes
#if TREE_VERSION == 0
  
  int depth = g->J_MAX - g->J_TREE + 1;                      // max path length
  vector <Node*> path_node (depth, (Node*) 0);               // current path
  vector <int>   path_cindex (depth,  0);                    // current child index
//int out_count = 0, i_p[DIM_MAX+1], i, ii, iii;                                                                  //!AR 2/27/2011! commented by AR
  int out_count = 0,                 i, ii, iii;                                                                  //!AR 2/27/2011! added by AR
  long i_p[DIM_MAX+1];                                                                                            //!AR 2/27/2011! added by AR
  i_p[0] = 1;
  for ( i=1; i<=g->DIM_NUM; ++i )
    i_p[i] = i_p[i-1] * (g->FINEST_MESH[i-1] + 1);  // FINEST_MESH is mxyz*2**j_max
  //                                                // so it will be non-periodic Fortran's i_p(0:dim)
  
  
  for ( i=0; i<t_num_size; ++i ) {  // for each tree t_num[i] (Fortran's numbering)
    
    if (!out_nodes)           // reset in counting mode only
      out_num[i] = 0;
    
    
    int main_index = 0;                                        // to which position path_...[] is filled
    if (mode)
      path_node[0] = trees[t_num[2*i]-1]->get_root();         // initialize path's beginning
    else
      path_node[0] = trees[t_num[i]-1]->get_root();
    
    path_cindex [0] = 0;       // 0-x, 1-y, 3-z, i.e. direction-1
    Node *node = path_node [0];
    
    
    while (true) {                                  // the main cycle ===============================
      int ilow = path_cindex [main_index];
      int ci;
      int direction = 0;                    // mode 0 - take the whole tree
      if (mode)	direction = t_num[2*i+1];   // mode 1
      
      
      if (found_nonzero_child_link_bnd (node,ci,ilow,direction)) {
	main_index ++;
	path_node [main_index] = node->leaves [ci];
	path_cindex [main_index-1] = ci+1;
	path_cindex [main_index]   = 0;
	node = node->leaves [ci];
	
      } else {
 	if ( (mode && (node->id & significant_id)) ||                              // visualization mode
 	     (!mode && (node->id & (significant_id | adjacent_id | old_id))) ) {   // tree migration mode
	  //	if ( node->id & significant_id) {
	  
	  if (out_nodes) {               // writing mode
	    if (mode) {
	      out_nodes[out_count] = 1;
	      for ( ii=0; ii<g->DIM_NUM; ++ii )
          out_nodes[out_count] += node->ii[ii] * i_p[ii];
	    } else {
	      out_nodes[2*out_count] = 1;
	      for ( ii=0; ii<g->DIM_NUM; ++ii )
	        out_nodes[2*out_count] += node->ii[ii] * i_p[ii];
	      out_nodes[2*out_count+1] = node->id;
	    }
	    for ( ii=out_count*g->VAL_NUM, iii=0; iii<g->VAL_NUM; ++ii, ++iii )
	      out_vals[ii] = node->val[iii];
	    // if (verb) printf (" node [%d %d] <--> %d\n",node->ii[0],node->ii[1],out_nodes[out_count]);
	    out_count ++;
	  } else {                       // counting mode
	    ++out_num[i];
	  }

	}
	
	main_index --;
	if (main_index < 0) break;
	
	path_node   [main_index+1] = 0;
	path_cindex [main_index+1] = 0;
	node = path_node [main_index];
      }
    }                                               // the main cycle ===============================
    
    for ( ii=1; ii< depth; ii++) {                              // clear the path
      path_node   [ii] = 0;                                   // ...
      path_cindex [ii] = 0;                                      // ...
    }
  }                                    // for each tree t_num[i]
#else //TREE_VERSION
  terminate ("Forest::count_tree_nodes()");
#endif //TREE_VERSION
}




//------------------------------------------------------------------------------------------------------
// depth first pass
// All the nodes will be deleted from tlp lists and be deleted completely, if possible.
// Tree roots will be deleted from tlp lists only.
// The only tree to remain are the one belonging to proc_to_exempt (which is -1 by default)
//------------------------------------------------------------------------------------------------------
void Forest::delete_the_forest (int proc_to_exempt)
{

#if TREE_VERSION == 0
  
  int depth = g->J_MAX - g->J_TREE + 1;                      // max path length
  vector <Node*> path_node (depth, (Node*) 0);               // current path
  vector <int>   path_cindex (depth,  0);                    // current child index
  

  for (int si=0; si < number_of_trees; ++si )               // for each tree ----------------------
    if (trees[si]->get_proc() != proc_to_exempt) {
      //bool verb=1;
      //if (par_rank !=0 ) verb = false;
      //if (verb) printf ("# proc=%d, tree number %d, proc=%d, proc_to_exempt=%d\n",par_rank,si,trees[si]->get_proc(),proc_to_exempt);
      //if (si!=0) verb = 0;
      int main_index = 0;                                        // to which position path_...[] is filled
      path_node   [0] = trees[si]->get_root();                    // initialize path's beginning
      path_cindex [0] = 0;                                        // ...
      Node *node = path_node [0];
      
      
      while (true) {                                  // the main cycle ===============================
	int ilow = path_cindex [main_index];
	int ci;
	int totc = 0;
	if (found_nonzero_child_link (node,ci,ilow,totc)) {
	  main_index ++;
	  path_node [main_index] = node->leaves [ci];
// 	  path_node [main_index] = node->ptr + ci;
	  path_cindex [main_index-1] = ci+1;
	  path_cindex [main_index]   = 0;
	  node = node->leaves [ci];
// 	  node = node->ptr + ci;
	  //if (verb) printf ("$ path_node[%d] : %d %d\n",main_index,node->ii[0],node->ii[1]);
	} else {
	  //if (verb) printf ("here1: node=%x, totc=%d, main_index=%d\n", node,totc,main_index);
	  if (( totc == 0 )&&( main_index > 0 )) {
	    //if (verb) printf ("$ deallocate %x [%d %d], main_index=%d ",node,node->ii[0],node->ii[1],main_index);
	    //if (verb) printf ("of parent %x [%d %d]\n",path_node[main_index-1],path_node[main_index-1]->ii[0],path_node[main_index-1]->ii[1]);
	    m->deallocate_node (node);                                                  // delete node
	    path_node [main_index-1] -> leaves [path_cindex[main_index-1]-1] = NULL;    // inform parent
	    //
            // something to be done here instead of using leaves for db_tree1
	    // path_node [main_index-1] -> leaves [path_cindex[main_index-1]-1] = NULL;    // inform parent
	    //
	    //
	  }
	  //if (verb) printf ("here: main_index=%d\n",main_index);
	  main_index --;
	  if (main_index < 0) break;
	  
	  path_node   [main_index+1] = NULL;
	  path_cindex [main_index+1] = 0;
	  node = path_node [main_index];
	  //if (verb) printf ("here2: node=%x\n", node);
	}
      }                                               // the main cycle ===============================
      
      int i;
      for ( i=1; i< depth; i++) {                              // clear the path
	path_node   [i] = NULL;                                   // ...
	path_cindex [i] = 0;                                      // ...
      }
      
      if ( trees[si]->get_root()->get_tlp() )
	//if (verb) printf ("\texcluding node %x [%d %d]\n",trees[si]->get_root(),trees[si]->get_root()->ii[0],trees[si]->get_root()->ii[1]);
	trees[si]->get_root()->get_tlp()->tlp_node_exclude (trees[si]->get_root());
      trees[si]->get_root()->id = intermediate_id;
      trees[si]->get_root()->zero_value_vector (g->VAL_NUM, g->VAL_NUM_I);
    }
  //   for each tree ----------------------
#else //TREE_VERSION
  terminate ("Forest::delete_the_forest()");
#endif //TREE_VERSION
}


//------------------------------------------------------------------------------------------------------
// returns true if node has some children
// with a link number more or equal to the specified lower limit
//------------------------------------------------------------------------------------------------------
bool Forest::found_nonzero_child_link (const Node* node, int& link_index,
				       const int& lower_limit_index, int& total_children)
{
#if TREE_VERSION == 0
  int i;
  for (i = 0; i < node->num_leaves; ++i)
    total_children += (bool) node->leaves[i];
  
  //printf ("tots=%d, ",total_children);
  
  for (i = lower_limit_index; i < node->num_leaves; ++i) {        // leaves[0 ... num_leaves-1]
    if (node->leaves[i] != 0) {
      link_index = i;
      //printf ("TRUE\n");
      return true;
    }
  }
  //printf ("FALSE\n");
  return false;
  
#elif TREE_VERSION == 1
  
  if (node->ptr) {
    int i;
    for (i = 0; i < QUAD; ++i)
      total_children +=  (node->ptr+i)->data_ptr;

    for (i = lower_limit_index; i < QUAD; ++i) {        // leaves[0 ... num_leaves-1]
      if ((node->ptr+i)->data_ptr != 0) {
	link_index = i;
	return true;
      }
    }
    return false;
  }
  
  total_children = 0;
  return false; 
  
#endif //TREE_VERSION
}




// consider only boundary nodes
bool Forest::found_nonzero_child_link_bnd (const Node* node, int& link_index,
					   const int& lower_limit_index, int& direction)
{
#if TREE_VERSION == 0
  // direction - 1,2,4 for x,y,z etc.
  int i;  
  for (i = lower_limit_index; i < node->num_leaves; ++i) {
    // pass array leaves[0 ... num_leaves-1] through positions:
    // 0,1,3, 7,8,10, etc for 3D
    //direction *= 2;
    //if (direction>=g->LEAVES+1) direction = 1;
    if ( !(direction & ((i%g->LEAVES)+1))                  // (2D) OK for dir=1,2, 3-gives root only
	 && (node->leaves[i] != 0) ) {
      link_index = i;
      //printf ("TRUE: ci=%d\n",i);
      return true;
    }
  }
  //printf ("FALSE\n");
  return false;
#else //TREE_VERSION
  terminate ("Forest::found_nonzero_child_link_bnd()");
  return false; // to shut compiler warning
#endif //TREE_VERSION
}


//------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------
void Forest::print_info (bool verb, int print_tree_number)
{
#ifdef EBUG_SAFE
  std::cout << "Database info: (tree) at processor" << par_rank << "\n";
  if (trees==NULL) {                                         // there are no trees -----------------
    cout << " There are no trees\n";
    return;
  }

  int depth = g->J_MAX - g->J_TREE + 1;                      // max path length
  vector <Node*> path_node (depth, (Node*) 0);               // current path
  vector <int>   path_cindex (depth,  0);                    // current child index
  vector <int> num_nodes (depth, 0);           // total number of nodes
  vector <int> num_linked_nodes (depth, 0);    // ... without intermediate ones
  
  for (int si=0; si < number_of_trees; si++ ) {              // for each tree ----------------------
    if (verb) {
      printf ("num_nodes[0:%d] = [ ",depth-1);
      for (int j=0; j<depth; ++j)
	printf ("%d ",num_nodes[j]);
      printf ("]\n");
    }
    if (si==print_tree_number) verb = 1; else verb = 0;
    if (verb) printf ("#  tree number %d\n",si);
    
    int main_index = 0;                                        // to which position path_...[] is filled
    path_node   [0] = trees[si]->get_root();                    // initialize path's beginning
    path_cindex [0] = 0;                                        // ...
    Node *node = path_node [0];
    if (verb) {
      printf ("$ root_node[%d] : %d %d is %x\n",main_index,node->ii[0],node->ii[1],node);
      printf ("$  leaves: [ ");
#if TREE_VERSION == 0
      for(int i=0; i<node->num_leaves; ++i)
	printf ("%x ",node->leaves[i]);
#elif TREE_VERSION == 1
      if (node->ptr)
	for(int i=0; i<g->TYPES_NUM; ++i)
	  printf ("%x ",node->ptr+i);
#endif //TREE_VERSION
      printf ("]\n");
    }

    while (true) {                                  // the main cycle ===============================
      int ilow = path_cindex [main_index];
      int ci;
      int totc = 0;
      if (found_nonzero_child_link (node,ci,ilow,totc)) {
	main_index ++;
	path_node [main_index] = node->get_child( ci );
	path_cindex [main_index-1] = ci+1;
	path_cindex [main_index]   = 0;
	node = node->get_child( ci );
	if (verb) {
	  printf ("$ path_node[%d] : %d %d is %x\n",main_index,node->ii[0],node->ii[1],node);
	  printf ("$  leaves: [ ");
#if TREE_VERSION == 0
	  for(int i=0; i<node->num_leaves; ++i)
	    printf ("%x ",node->leaves[i]);
#elif TREE_VERSION == 1
	  if (node->ptr)
	    for(int i=0; i<g->TYPES_NUM; ++i)
	      printf ("%x ",node->ptr+i);
#endif //TREE_VERSION
	  printf ("]\n");
	}
      } else {

#if TREE_VERSION == 0
	int node_level = node->level;
#elif TREE_VERSION == 1
	int node_level = find_node_tree_level (node->ii);
#endif //TREE_VERSION
	
	if (node_level == 0) {
	  printf ("node [%d %d](%d) ",node->ii[0],node->ii[1],node->id);
	}

#if TREE_VERSION == 0
	num_nodes [ node_level ] ++;
#elif TREE_VERSION == 1
	if (node_level == main_index)
	  num_nodes [ node_level ] ++;
#endif //TREE_VERSION
	
	if (node->id != intermediate_id)
	  num_linked_nodes [ node_level ] ++;
	
	main_index --;
	if (main_index < 0) break;
	if (verb) printf ("$ node %x [%d %d], main_index=%d ",node,node->ii[0],node->ii[1],main_index+1);
	if (verb) printf ("of parent %x [%d %d]\n",path_node[main_index],
			  path_node[main_index]->ii[0],path_node[main_index]->ii[1]);
	path_node   [main_index+1] = 0;
	path_cindex [main_index+1] = 0;
	node = path_node [main_index];
      }
    }                                               // the main cycle ===============================
    
    for (int i=1; i< depth; i++) {                              // clear the path
      path_node   [i] = 0;                                      // ...
      path_cindex [i] = 0;                                      // ...
    }
  }                                                          // for each tree ----------------------

  {
    cout << " "
	 << num_nodes;
    int sum = 0, i;
    for ( i=0; i<depth; i++) sum += num_nodes[i];
    cout << " : " << sum << " --- total number of nodes, for levels 1-" << g->J_TREE << " ... " << g->J_MAX <<"\n";
    cout << " "
	 << num_linked_nodes;
    sum = 0; for ( i=0; i<depth; i++) sum += num_linked_nodes[i];
    cout << " : " << sum << " --- total number of nodes to be linked ...\n";
    
    for (int proc_num=0; proc_num<par_size; ++ proc_num)
      {
	cout << "Database info: (link-lists) [processor #" << proc_num <<"] at "<<par_rank<<"\n";
	int num_sigvt = 0, num_ghovt = 0, num_delvt = 0,
	  facetype, type, level;
	for ( facetype=0; facetype < f_sig.get_pow3(); facetype++)
	  for ( type=0; type < g->TYPES_NUM; type ++)
	    for ( level=1; level <= g->J_MAX; level++)  {
	      int tlp_index = f_sig.tlp_index ( facetype, type, level, proc_num );
	      num_sigvt += f_sig.get_tlp_number_of_elements( tlp_index );
	      num_ghovt += f_gho.get_tlp_number_of_elements( tlp_index );
	    }
	cout << " " << num_sigvt << "," << num_ghovt << "," << num_delvt << " : "
	     << num_sigvt+num_ghovt+num_delvt << " --- elements in sig,gho,del lists\n";
	
	cout << " get_next_ ... results:\n";
	for (int marker=1; marker < 4; marker++) {
	  cout << "sig/gho marker = " << marker << ". ";
	  int num_list = 0;
	  for ( facetype=0; facetype < f_sig.get_pow3(); facetype++)
	    for ( type=0; type < g->TYPES_NUM; type ++)
	      for ( level=1; level <= g->J_MAX; level++)  { // ---
		int num = 0, count = 0;
		Node* c_ptr = get_initial_type_level_node (proc_num, type, level, facetype, &num, marker);
		if (c_ptr) printf ("\n(%d,%d,%d):",type, level, facetype);
		while (c_ptr != 0) {
		  printf ("-[%d,%d](%d)",c_ptr->ii[0],c_ptr->ii[1],c_ptr->id);
		  fflush (stdout);
		  count ++;
		  Node *next_node = get_next_type_level_node (proc_num, type, level, facetype, c_ptr, marker);
		  c_ptr = next_node;
		}
		if (num != count) {
		  cout <<"\nERROR: num=" << num << " count=" << count
		       <<" for (type,level,facetype) = (" << type << "," << level << "," << facetype << ")\n";
		}
		num_list += count;
	      } //---
	  cout << "Number of elements in that list is " << num_list << "\n";
	}
      }
  }
#endif
}




#if TREE_VERSION == 0
// ------------------------------------------------------------------------------------------
// Add nodes to bisect the cell originated at the given node
//  assumed that number_of_leaves > 0
//   and the central node might not be present
void Forest::bisect_amr_cell (Node* origin_node, const int origin_halfsize, const int ch_level) {
//   printf ("Forest::bisect_amr_cell( [");
//   for (int i=0; i<DIM_NUM; ++i) printf (" %d ",origin_node->ii[i]);
//   printf ( "], halfsize=%d, ch_level=%d\n",origin_halfsize,ch_level);

  Node *tmp;
  int *coord = new int [DIM_NUM];    // actual coordinates
  int *index = new int [DIM_NUM];    // tuple of 0,1,2
  int i;
  for ( i=0; i<DIM_NUM; ++i )
    index [i] = 0;
  
  while ( index[DIM_NUM-1] <= 2 ) {
    
    index[0] ++;                                          // lowest bit iteration step
    for ( i=0; i<DIM_NUM-1; ++i) {                     // upgrade the highest bits
      if ( index[i] > 2 ) {
	index[i] = 0;
	index[i+1] ++;
      }
    }
    
    bool unity_present = false;                           // test presence of "1"
    for ( i=0; i<DIM_NUM; ++i)
      if ( index[i] == 1 ) {
	unity_present = true;
	break;
      }
    
    if ( unity_present ) {                                // add the node
      for ( i=0; i<DIM_NUM; ++i )
	coord [i] = origin_node->ii [i] + index [i] * origin_halfsize;

      add_by_coordinates( coord,J_MAX,tmp,id_of_added_amr,false );
      
//       for (int i=DIM_NUM-1; i>=0; --i )
//        	printf (" %d ",coord[i]);
//       printf ("\n");
    }
  }
  
  delete [] index;
  delete [] coord;
}
#endif //TREE_VERSION



#if TREE_VERSION == 0
void Forest::add_central_amr_nodes_aux (Node* node, int *coord, int *delta, vector <bool> &ch_typev, vector <bool> &mask) {

//   printf ("looking at node (");
//   for (int i=0; i<DIM_NUM; ++i ) printf (" %d ",node->ii[i]); printf (")\n");
  
  int current_size = trees[0]->tsize >> node->level;
  int max_child_level = 0;
  for ( int ch_num=node->num_leaves-1; ch_num >= 0; --ch_num )
    if ( ( node->leaves[ch_num] != 0 ) &&
	 ( node->leaves[ch_num]->id & significant_id )
	 ) { // the definition of 'real' child
      max_child_level = ch_num / g->LEAVES + 1;
//       printf ("found child # %d\n",ch_num);
      break;
    }
  if ( max_child_level != 0 ) {                // there are some 'real' children
    for ( int lev_num=0; lev_num<max_child_level; ++lev_num ) {
//  	      printf ("  lev_num = %d\n",lev_num);
//  	      printf ("  current_size = %d\n",current_size);
//  	      printf ("  max_child_level = %d\n",max_child_level);
	      
      bool coordinates_are_not_ok = false;                       // test periodicity limits
      for ( int i=0; i<DIM_NUM; ++i )
	if ( (!g->PERIODIC[i]) &&                                // ... for periodic, check
	     (node->ii[i] == g->FINEST_MESH[i])                  // ... upper boundary only
	     ) {
	  coordinates_are_not_ok = true;
	  break;
	}
      
      if ( ! coordinates_are_not_ok ) {
	bisect_amr_cell( node,current_size>>(1+lev_num),lev_num );  // bisect the cell itself
      }
      
      for ( int ch_type=1; ch_type<=g->LEAVES; ++ch_type )          // bisect nearliest cells
	if ( ( node->leaves[ch_type-1+lev_num*g->LEAVES] ) &&                       // check presence of
	     ( node->leaves[ch_type-1+lev_num*g->LEAVES]->id & significant_id )
	     ) { // nodes of that level
// 		  printf ("     ch_type = %d\n",ch_type);
	  // compute the coordinates of the nearliest cells
	  //   compute the number of zeroes in the child type
	  int num_of_zeroes = DIM_NUM, bittest = 1<<(DIM_NUM-1);
	  int z_num, i;
	  for ( z_num=0; z_num<DIM_NUM; ++z_num ) {
	    ch_typev[z_num] = (bool) ( ch_type & bittest );
	    if ( ch_type & bittest ) num_of_zeroes--;
	    bittest >>= 1;
	  }
// 		  printf ("ch_typev = (");
// 		  for ( int z_num=0; z_num<DIM_NUM; ++z_num )
// 		    printf (" %d ", (int) ch_typev[z_num]);
// 		  printf (")\n     num_of_zeroes = %d\n",num_of_zeroes);
	  
	  //   it will be 2**num_of_zeroes different tuples of coordinates
	  for ( z_num=1; z_num<(1<<num_of_zeroes); ++z_num ) {
// 		    printf ("      z_num = %d\n",z_num);
	    for ( i=0; i<DIM_NUM; ++i ) {                      // set reference coordinates
	      coord[i] = node->ii[i];
	      delta[i] = -( current_size>>lev_num );
	    }
	    int bitset = 1;
	    for ( int maski=0; maski<DIM_NUM; ++maski ) {          // set mask
	      if ( ch_typev[maski] ) {                                // 1 stands here ->
		mask[maski] = 0;                                      //  no variations of that coord
	      } else {
		mask[maski] = ( bitset & z_num );
		bitset <<= 1;
	      }
	    }
// 		    printf ("      mask = (");
// 		    for ( int i=0; i<DIM_NUM; ++i ) printf (" %d ", (int) mask[i]);
// 		    printf (")\n");
	    
	    bool coordinates_are_not_ok = false;
	    for ( i=0; i<DIM_NUM; ++i ) {                       // set nearliest node coordinates
	      coord[i] += mask[DIM_NUM-1-i]*delta[i];
	      if ( (!g->PERIODIC[i]) &&                             // and test periodicity limits
		   ( (coord[i]<0) || (coord[i]>=g->FINEST_MESH[i]) )
		   ) {
		coordinates_are_not_ok = true;
		break;
	      }
	    }
	    
// 		    printf ("      coord =(");
// 		    for ( int i=0; i<DIM_NUM; ++i )
// 		      printf (" %d ",coord[i]);
// 		    printf (")\n");
		    // test periodicity limits
	    if (coordinates_are_not_ok) {
	      // 		      printf ("coordinates are not OK\n");
	      continue;
	    }
	    
	    // add (if necessary) the nearliest node
	    Node *tmp = (Node*)
	      test_by_coordinates( coord, J_MAX ); // ( coord,tmp,id_of_added_amr, front_of_added );
	    if ( tmp == 0 ) {
	      printf ("Error: origin node of the cell to be bisected does not exist !\n");
	      exit (1);
	    }
// 		    add_by_coordinates( coord, J_MAX, tmp, id_of_added_amr, front_of_added );
	    // bisect the nearliest cell
	    bisect_amr_cell( tmp,current_size>>(1+lev_num),lev_num );
//  		    printf ("bisect_amr_cell( tmp, %d, %d )\n",current_size>>(1+lev_num),lev_num);
	  }
// 		  printf ("\n");
	}
// 	      printf ("\n\n");
    }
  }
}
#endif //TREE_VERSION




#if TREE_VERSION == 0
// ------------------------------------------------------------------------------------------
//  Add central nodes to the cells which should be bisected during AMR creation
//   leaves will be bisected first
//  Set total number of tree nodes to be visualized: stat_nnodes
void Forest::add_central_amr_nodes () {

  int *coord = new int [DIM_NUM];
  int *delta = new int [DIM_NUM];
  vector <bool> ch_typev (DIM_NUM); // binary representation of ch_type
  vector <bool> mask (DIM_NUM);     // permutation vector for nearliest coordinates

  int depth = g->J_MAX - g->J_TREE + 1;                      // max path length
  vector <Node*> path_node (depth, (Node*) 0);               // current path
  vector <int>   path_cindex (depth,  0);                    // current child index
  
  for (int si=0; si < number_of_trees; si++ ) {              // for each tree ----------------------
    int main_index = 0;                                        // to which position path_...[] is filled
    path_node   [0] = trees[si]->get_root();                    // initialize path's beginning
    path_cindex [0] = 0;                                        // ...
    Node *node = path_node [0];

    while (true) {                                  // the main cycle ===============================
      int ilow = path_cindex [main_index];
      int ci;
      int totc = 0;
      if (found_nonzero_child_link (node,ci,ilow,totc)) { //!!!

	if ( !(node->id & id_of_bisected_amr) ) {
	  add_central_amr_nodes_aux (node,coord,delta,ch_typev,mask);
	  node->id |= id_of_bisected_amr;
	}

	main_index ++;
	path_node [main_index] = node->leaves [ci];
	path_cindex [main_index-1] = ci+1;
	path_cindex [main_index]   = 0;
	node = node->leaves [ci];

      } else {                                      // =============================================

	main_index --;                              // =============================================
	if (main_index < 0) break;

	path_node   [main_index+1] = NULL;
	path_cindex [main_index+1] = 0;
	node = path_node [main_index];

      }
    }                                               // the main cycle ===============================
    
    for (int i=1; i< depth; i++) {                              // clear the path
      path_node   [i] = NULL;                                   // ...
      path_cindex [i] = 0;                                      // ...
    }
  }                                                          // for each tree ----------------------
  delete [] coord;
  delete [] delta;
}
#endif //TREE_VERSION





// // ------------------------------------------------------------------------------------------
// //  Make the grid look like an AMR grid
// //   without introducing additional nodes,
// //   all the roots assumed to exist
// void Forest::make_amr_noadd () {
   
//   Node* c;
//   bool is_boundary, not_ok;
//   const int chi = g->LEAVES;
  
//   for ( int i = number_of_trees-1; i >= 0; --i ) {                 // for each tree
//     c = trees[i]->get_root();
//     //printf ("tree %d %d\n",c->ii[0],c->ii[1]);
//     is_boundary = false;                                             // make sure
//     for ( int dim=0; dim<DIM_NUM; ++dim )                            //  that this tree
//       if ( c->ii[dim] == FINEST_MESH[dim] ) {                        //  is not at the boundary
// 	is_boundary = true;
// 	break;
//       }
//     if ( ( ! is_boundary ) &&
// 	 ( c->num_leaves > chi ) &&                                  // test if the
// 	 ( c->leaves[chi] ) &&                                       //  central significant
// 	 ( c->leaves[chi]->id & significant_id ) ) {                 //  node is present
//       not_ok = false;
//       for ( int ln=0; ln<chi; ++ln )
// 	if ( ( ! c->leaves[ln] ) ||
// 	     ( ! (c->leaves[ln]->id & significant_id )) ) {            // make sure that
// 	  not_ok = true;                                               //  all other nodes
// 	  break;                                                       //  present too
// 	}
//       if ( ! not_ok ) {                                                // make sure that neighboring nodes present too
// 	// it could be done in AMR::create_box_tree ()
//       }

//       if ( not_ok )                                                    // clear the significance
// 	c->leaves[chi]->id = zero_id;                        //  of the central node
//     }
//   }
//   printf ("stop\n");
//   fflush (stdout);
//   exit(0);
// }




// ------------------------------------------------------------------------------------------
//  Add nodes and make the grid look like an AMR grid
//   using depth-first search and adding all the roots
void Forest::make_amr () {
#if TREE_VERSION == 0
  if (trees==NULL) return;  // there are no trees
  
  join_sig_path_for_amr ();
  add_central_amr_nodes ();
#else //TREE_VERSION
  terminate ("Forest::make_amr()");
#endif //TREE_VERSION
}




#if TREE_VERSION == 0
// get coordinates for the given child of the given node
//  0 <= index < LEAVES*n
void Forest::get_coord_by_child_index (const Node* node, const int index, int coord[]) {
  int ch_level = index / g->LEAVES;
  int wlt_type = index % g->LEAVES;
  int delta = x_space>>(node->level+1+ch_level);  //  halfsize = x_space>>(level+1);
  int bittest = 1;
  for (int i=0; i<DIM_NUM; ++i) {
    coord[i] = node->ii[i];
    if ( bittest & (wlt_type+1) )
      coord[i] += delta;
    bittest <<= 1;
  }
}
#endif //TREE_VERSION




#if TREE_VERSION == 0
// ------------------------------------------------------------------------------------------
//  Change node's ID to sig if it has at least one sig child of any generation,
//  add sig children of the same wlt_type for all coarser levels
//   If "MAKE_SIG_PATH" is defined  - ID is changed to sig and the node is added into sig list.
//   Otherwise, ID |= id_of_added_amr and the node is not in any lists.
void Forest::join_sig_path_for_amr () {

#define MAKE_SIG_PATH
  
  int num_sig_added = 0;
  Node *tmp;
  int *coord = new int [g->DIM_NUM];
  int depth = g->J_MAX - g->J_TREE + 1;                      // max path length
  vector <Node*> path_node (depth, (Node*) 0);               // current path
  vector <int>   path_cindex (depth,  0);                    // current child index
  
  for (int si=0; si < number_of_trees; si++ ) {              // for each tree ----------------------
    int main_index = 0;                                        // to which position path_...[] is filled
    path_node   [0] = trees[si]->get_root();                    // initialize path's beginning
    path_cindex [0] = 0;                                        // ...
    Node *node = path_node [0];
    if ( (node->id == intermediate_id) ||                           // treat tree roots
	 (node->id == zero_id) ) {
      trees[si]->
	add_by_coordinates( node->ii, tmp,                          // add root as a normal node
			    id_of_added_amr, front_of_added );
      num_sig_added ++;
    }

    while (true) {                                  // the main cycle ===============================
      int ilow = path_cindex [main_index];
      int ci;
      int totc = 0;
      if (found_nonzero_child_link (node,ci,ilow,totc)) {
	main_index ++;
	path_node [main_index] = node->leaves [ci];
	path_cindex [main_index-1] = ci+1;
	path_cindex [main_index]   = 0;
	node = node->leaves [ci];
      } else {                                      // =============================================
	if ( totc > 0 ) {                              // there are some children for that node
	  // make significant path
	  // ... check if there are significant children (or if there are any amr_added nodes)
	  bool there_are_sig_children = false;
	  for (int cn=0; cn<node->num_leaves; ++cn) {
	    if ( ( node->leaves[cn] ) && 
		 ((node->leaves[cn]->id & significant_id) 
#ifndef MAKE_SIG_PATH
		  || (node->leaves[cn]->id & id_of_added_amr)
#endif
		  )
		 ) {
	      there_are_sig_children = true;
	      break;
	    }
	  }
	  // ... add as sig to the cosrser level the same wlt_type
	  if ( there_are_sig_children )
	    for (int ch_lev=node->num_leaves/g->LEAVES-1; ch_lev > 0; --ch_lev) {  // for all higher levels
// 	      printf ("node->num_leaves=%d, ch_lev=%d\n",node->num_leaves,ch_lev);
	      for (int wlt_type=0; wlt_type<g->LEAVES-1; ++wlt_type) {   // for all noncentral types
		int test_index = ch_lev*g->LEAVES + wlt_type;
		if ( (node->leaves[test_index]) &&
		     (node->leaves[test_index]->id & significant_id) ) { // add sig of other levels
		  for (int add_lev=0; add_lev<ch_lev; ++add_lev) {       //  for all other levels
		    int add_index = add_lev*g->LEAVES + wlt_type;
		    if ( (!(node->leaves[add_index])) ||
			 (!(node->leaves[add_index]->id & significant_id)) ) {
		      get_coord_by_child_index (node,add_index,coord);
		      add_by_coordinates( coord,J_MAX,tmp,significant_id,false );
// 		      printf ("sig_path added: %d %d, index=%d\n",coord[0],coord[1],add_index);
		      num_sig_added ++;
		    }
		  }
		}
	      }
	    }
	  // ... change the node's ID to sig
	  if ( (there_are_sig_children) && (!(node->id & significant_id)) ) {
#ifdef MAKE_SIG_PATH
	    Type_level *node_tlp = node->get_tlp(),                 // ...
	      *sig_tlp = &f_sig;                                    // ...
	    
	    if (node_tlp == 0) {
	      set_type_level (node, trees[si]->get_proc());
	    } else {
	      node_tlp->tlp_node_exclude (node);                  // than move it into 
	    }
	    sig_tlp->tlp_node_include_b (node);                   // sig_adj link-list
	    node->id  |= significant_id;
#else
	    node->id  |= id_of_added_amr;
#endif
	    num_sig_added ++;
	  }
	}
	main_index --;                              // =============================================
	if (main_index < 0) break;

	path_node   [main_index+1] = NULL;
	path_cindex [main_index+1] = 0;
	node = path_node [main_index];
	
      }
    }                                               // the main cycle ===============================
    
    for (int i=1; i< depth; i++) {                              // clear the path
      path_node   [i] = NULL;                                   // ...
      path_cindex [i] = 0;                                      // ...
    }
  }                                                          // for each tree ----------------------
  delete [] coord;
  printf ("Forest::join_sig_path_for_amr: changed ID for %d nodes\n",num_sig_added);

#ifdef MAKE_SIG_PATH
#undef MAKE_SIG_PATH
#endif
  
}
#endif //TREE_VERSION




void Forest::count_nodes_to_visualize () {
  
  stat_nnodes = 0;          // total number of nodes to visualize

  int *coord = new int [DIM_NUM];
  int *delta = new int [DIM_NUM];
  
  int depth = g->J_MAX - g->J_TREE + 1;                      // max path length
  vector <Node*> path_node (depth, (Node*) 0);               // current path
  vector <int>   path_cindex (depth,  0);                    // current child index
  
  for (int si=0; si < number_of_trees; ++si ) {              // for each tree ----------------------
    int main_index = 0;                                        // to which position path_...[] is filled
    path_node   [0] = trees[si]->get_root();                    // initialize path's beginning
    path_cindex [0] = 0;                                        // ...
    Node *node = path_node [0];
    stat_nnodes ++;

//     printf ("root position, node = (");
//     for (int i=0; i<DIM_NUM; ++i) printf (" %d ",node->ii[i]);
//     printf (")\n");

    while (true) {                                  // the main cycle ===============================
      int ilow = path_cindex [main_index];
      int ci;
      int totc = 0;
      if (found_nonzero_child_link (node,ci,ilow,totc)) {
	
// 	printf ("position 0, node = (");
// 	for (int i=0; i<DIM_NUM; ++i) printf (" %d ",node->ii[i]);
// 	printf (")\n");

	main_index ++;
	path_node [main_index] = node->get_child( ci );
	path_cindex [main_index-1] = ci+1;
	path_cindex [main_index]   = 0;
	node = node->get_child( ci );
	
// 	printf ("position 1, node = (");
// 	for (int i=0; i<DIM_NUM; ++i) printf (" %d ",node->ii[i]);
// 	printf (")\n");
	
	if ( ( node->id & significant_id ) ||      // count all
	     ( node->id & id_of_added_amr ) )
	  stat_nnodes ++;

      } else {                                      // =============================================


// 	if (totc>0) {
// 	  printf ("position 2, node = (");
// 	  for (int i=0; i<DIM_NUM; ++i) printf (" %d ",node->ii[i]);
// 	  printf (")\n");
// 	}

	main_index --;                              // =============================================
	if (main_index < 0) break;

	path_node   [main_index+1] = NULL;
	path_cindex [main_index+1] = 0;
	node = path_node [main_index];
	
// 	if (totc>0) {
// 	  printf ("position 3, node = (");
// 	  for (int i=0; i<DIM_NUM; ++i) printf (" %d ",node->ii[i]);
// 	  printf (")\n");
// 	}

      }
    }                                               // the main cycle ===============================
    
    for (int i=1; i< depth; i++) {                              // clear the path
      path_node   [i] = NULL;                                   // ...
      path_cindex [i] = 0;                                      // ...
    }
  }                                                          // for each tree ----------------------
  delete [] coord;
  delete [] delta;
//   printf ("stat_nnodes = %d\n",stat_nnodes);
}




void Forest::terminate (const char* s) {
  printf ("%s\n",s);
  printf ("is not ready for that database, try using DB=db_tree instead\n");
  exit(1);
}




#if TREE_VERSION == 1
//
//  db_tree1 specific function, used in amr and print_info
//  returns node's level in the range of [0,J_MAX-J_TREE]
//
int Forest::find_node_tree_level (int* coord) {
  
  for (int lev = J_MAX-J_TREE, test = 1; lev>0; --lev) {
    bool one_present = 0;
    for (int dim=0; dim<DIM_NUM; ++dim)
      one_present |= test & coord[dim];
    if (one_present)
      return lev;
    test <<=1;
  }
  return 0;
}
#endif //TREE_VERSION




//---------------------------------------------------------------------------------//
// MEMORY
//---------------------------------------------------------------------------------//
void Forest::reshuffle () {
#ifdef system_MM
  return;
#endif
  return;

  for (int si=0; si<number_of_trees; ++si)
    trees[si]->reshuffle(trees[si]->get_root());
}

void Tree::reshuffle (Node* root) {
#ifdef system_MM
  return;
#endif
  
//   int nl = 0;                               // number of children
//   for (int i=0; i< root->num_leaves; i++) {
//     nl += (bool) root->leaves[i];
// //     if (root->leaves[i]) {
// //       cout<<" leaf:"<<root->leaves[i]
// // 	  <<" "<< root->leaves[i]->id
// // 	  <<" "<<root->leaves[i]->ii[0]<<" "<<root->leaves[i]->ii[1]<<" "<<root->leaves[i]->ii[2]<<endl;
// //     }
//   }
  
//   if (nl < LEAVES) return;                  // no reshuffling

//   //   Node* n_ar = new Node [nl];               // allocate all children together
//   //   int* ii_ar = new int [nl*DIM_NUM];
//   //   Value* val_ar = new Value [nl*VAL_NUM];
//   //   int* ival_ar = new int [nl*VAL_NUM_I];
  
//   //   cout << "reshuffling " << nl << " leaves ...\n";
//   //   cout.flush();

//   int num = 0;
//   for (int i=0; i<root->num_leaves; i++) {  // copy the children
//     if ((bool)root->leaves[i]) {
//       //cout << " moving "<<root->leaves[i]<<" to "<<&n_ar[num]<<endl;

//       Node* n_ar = new Node;
//       int* ii_ar = new int [DIM_NUM];
//       Value* val_ar = new Value [VAL_NUM];
//       int* ival_ar = new int [VAL_NUM_I];
      
//       root->leaves[i]->move_node (&n_ar[num], &ii_ar[num*DIM_NUM], &val_ar[num*VAL_NUM], &ival_ar[num*VAL_NUM_I],
// 				  VAL_NUM, VAL_NUM_I, DIM_NUM, m);
//       root->leaves[i] = &n_ar[num];
//       //cout << " done for "<<root->leaves[i]<<" "<<root->leaves[i]->id
//       //	   <<" "<<root->leaves[i]->ii[0]<<" "<<root->leaves[i]->ii[1]<<" "<<root->leaves[i]->ii[2]<<endl;
//       //exit(1);
//       //num ++;
//     }
//   }
//   return;
}



// void Node::move_node (Node* n_new, int* ii_new, Value* val_new, int* ival_new,
// 			     int VAL_NUM, int VAL_NUM_I, int DIM_NUM, Memory_management* m)
//   /*  move this node to n_new  */
// {
// #if TREE_VERSION == 0
  
// #ifdef system_MM
//   return;
// #endif
//   if ((val==0)||(ival==0)||(ii==0)) {
//     cout << " noninitialized array of val or ival or ii: " << val << " " << ival << " " << ii << endl;
//     exit(1);
//   }
  
//   n_new->val = val_new;                                      // copy separately allocated data
//   int i;
//   for ( i=0; i<VAL_NUM; i++) n_new->val[i] = val[i];
//   n_new->ival = ival_new;
//   for ( i=0; i<VAL_NUM_I; i++) n_new->ival[i] = ival[i];
//   n_new->ii = ii_new;
//   for ( i=0; i<DIM_NUM; i++) n_new->ii[i] = ii[i];        // ...
  
//   n_new->id = id;                                            // copy internal to node variables
//   n_new->level = level;

//   Type_level* old_node_tlp = tlp;
//   if (tlp != 0) {
//     tlp->tlp_node_exclude (this);             // exclude and set tlp=0
//     old_node_tlp->tlp_node_include_b (n_new); // include into required TLP
//   }

//   if (num_leaves) {
//     n_new->num_leaves = num_leaves;
//     n_new->leaves = new Node* [num_leaves];
//     int i = num_leaves;
//     do {
//       i --;
//       n_new->leaves[i] = leaves[i];
//     } while (i);
//   }                                                          // ...
  
//   m->deallocate_node (this);
// #else //TREE_VERSION
//   terminate ("Node::move_node()");
// #endif //TREE_VERSION
// }


/*---------------------------------------------------------------------------------------------------*/
/*  Global memory management class                                                                   */
/*---------------------------------------------------------------------------------------------------*/
//=====================================================================================================
#ifdef childclustering_MM
//=====================================================================================================

#if TREE_VERSION == 0

Memory_management::Memory_management (const int& dim, const int& f_num, const int& if_num,
				      const int& a_tmp, const int& b_tmp) :
  DIM_NUM(dim), VAL_NUM(f_num), VAL_NUM_I(if_num) {
#ifdef VERB_MM
  printf ("# MEMORY MANAGEMENT: childclustering_MM\n");
#endif
}
Memory_management::~Memory_management () {}
inline Node* Memory_management::allocate_node () {}
inline void Memory_management::deallocate_node (Node* n) {}

#elif TREE_VERSION == 1

Memory_management::Memory_management (const int& dim, const int& f_num, const int& if_num,
				      const int& jtree, const int& jmax) :
  DIM_NUM(dim), QUAD(1<<dim), VAL_NUM(f_num), VAL_NUM_I(if_num),
  IS_ODD((jmax - jtree + 1) & 1) {
#ifdef VERB_MM
  printf ("# MEMORY MANAGEMENT: childclustering_MM\n");
#endif
#ifdef EBUG_MM
  printf ("$\tsizeof(Node) = %d\n",sizeof(Node));
  Node *n = new Node[5];
  printf ("n - %x\n",n);
  printf ("n[0] - %x\n",&n[0]);
  printf ("n[1] - %x\n",&n[1]);
  Node* ptr = &n[0];
  printf ("ptr - %x\n",ptr);
  printf ("ptr+1 - %x\n",ptr+1);
  delete [] n;
//   exit(1);
#endif //EBUG_MM
}

Memory_management::~Memory_management () {}

// allocate tree root (Forest usage only!)
inline Node* Memory_management::allocate_node () {
  int num_of_nodes = 1;                            // allocate standalone tree root
  if (!IS_ODD)
    num_of_nodes += QUAD;                          // allocate tree root + QUAD first level nodes
  return allocate_group_of_nodes (num_of_nodes);
}

// internal function to allocate selected number of nodes
inline Node* Memory_management::allocate_group_of_nodes (const int& num_of_nodes) {
  Node* n = new Node [num_of_nodes];
  for (int ch=0; ch<num_of_nodes; ++ch) {
    n[ch].id = intermediate_id;
#ifndef FIX_COORD_DATABASE_TREE
    n[ch].ii = new int [DIM_NUM];
#endif //FIX_COORD_DATABASE_TREE
    n[ch].val = new Value [VAL_NUM];
    n[ch].ival = new int [VAL_NUM_I];
    int i;
    for ( i=0; i<VAL_NUM;   ++i) n[ch].val [i] = 0.0;
    for ( i=0; i<VAL_NUM_I; ++i) n[ch].ival [i] = 0;
  }
#ifdef EBUG_MM
  printf ("$\tallocated : Nodes[%d] at %x\n",num_of_nodes,n);
#endif //EBUG_MM
  return n;
}


inline Node* Memory_management::allocate_nodes () { return 0; } // dummy function for childclustering_MM

inline Node* Memory_management::allocate_nodes  // allocate regular group of the nodes
( const int& root_lev,                          // level of the root for the children to be allocated
  Node* & root,                                 // the root ...
  const int root_ch_index                       // the child index of the root itself
  )
{
  if (IS_ODD == (root_lev & 1)) {
#ifdef EBUG_MM
    printf ("$\tallocate_nodes () : storage for children of %x has been already reserved\n",root);


    if (!root_lev) printf ("$\tRET = root+1 = %x\n",root+1);
    else printf ("$\tRET = root+%d-%d = %d = %x\n",QUAD*(root_ch_index+1),root_ch_index,
		 QUAD*(root_ch_index+1) - root_ch_index,root + QUAD*(root_ch_index+1) - root_ch_index);
#endif //EBUG_MM
    // -----------------------------------------------------------------------------------------------------
    // IS_ODD == true : 0 12 34 56 : root_lev&1 == true <--> storage for children has been already reserved
    // IS_OLD == false : 01 23 45 : root_lev&1 == false ..........
    // -----------------------------------------------------------------------------------------------------
    if (!root_lev) // root is the tree root
      //              which is allocated as [root][ ][ ][ ][ ]
      //              so return pointer to the first node in QUAD array of the next level nodes
      return root + 1;
    //                root in a regular root (e.g. the marked one of level 4) : [01] [23] [45]
    //                which is allocated as [ |x| | ][ | | | ] [ | | | ][ | | | ][ | | | ]
    //                (child index of the marked root is root_ch_index == summ == 1 in this example)
    //                so return the position of the first node in the respective QUAD array
    return root + QUAD*(root_ch_index+1) - root_ch_index;
  } else {
#ifdef EBUG_MM
    printf ("$\tallocate_nodes () : children of %x has to be allocated as [%d]\n$\tRET = ...\n",
	    root,QUAD*QUAD+QUAD);
#endif //EBUG_MM
    // -----------------------------------------------------------------------------------------------------
    // children has to be allocated
    // -----------------------------------------------------------------------------------------------------
    if (!root_lev) // root is the tree root
      //              which is allocated as [root]
      //              so we need to reserve the next pair of levels : QUAD*(QUAD+1) nodes
      return allocate_group_of_nodes (QUAD*QUAD+QUAD);
    //                root in a regular root (e.g. of level 3) : [01] [23],
    //                so we need to reserve space for [45]
    //                as [4444][5555][5555][5555][5555]
    return allocate_group_of_nodes (QUAD*QUAD+QUAD);
  }
  return 0;
}

inline void Memory_management::deallocate_nodes   // delete regular nodes (clean_forest usage only!)
( Node* n,                                        // the main node to delete
  const int& n_lev                                // the level of the node to delete
  )
{
  if (IS_ODD == (n_lev & 1)) {
#ifdef EBUG_MM
    printf ("$\tdeallocate_nodes () : %x : remove it from link-lists only\n",n);
    printf ("$\tn->ptr = %x, n->data_ptr=%d\n",n->ptr, n->data_ptr);
#endif //EBUG_MM
    // -----------------------------------------------------------------------------------------------------
    // we are not allowed to delete that node with space reserved for its children
    // e.g. one of level 4, which was allocated as [44x4][5555][5555][5555][5555]
    // -----------------------------------------------------------------------------------------------------
    n->ptr = 0;                     // set flag, so it will be treated as newly created without children
    
    if ( n->tlp )                   // remove it from lists
      n->tlp->tlp_node_exclude (n);
    
    n->data_ptr = 0;  // set flag, so it will be treated as newly created, so
    //                   its coordinates will be redundantly rewritten in add_by_coordinates
  } else {
#ifdef EBUG_MM
    printf ("$\tdeallocate_nodes () : %x : delete children and remove it from link-lists\n",n);
    printf ("$\tn->ptr = %x, n->data_ptr=%d\n",n->ptr, n->data_ptr);
#endif //EBUG_MM
    // -----------------------------------------------------------------------------------------------------
    // it is a child attached to parent remove it from link-lists and remove space for its own children
    // e.g. one of level 5, which was allocated as [4444][5555][5555][5555][55x5]
    // -----------------------------------------------------------------------------------------------------
    if (n->ptr) {                   // delete node's own children
      delete [] n->ptr;
      n->ptr = 0;
    }
    
    if ( n->tlp )                   // remove it from lists
      n->tlp->tlp_node_exclude (n);
    
    n->data_ptr = 0;                // set flag, so it will be treated as newly created, so its
    //                                 coordinates will be redundantly rewritten in add_by_coordinates
  }
}

inline void Memory_management::deallocate_nodes (Node* n) {}  // dummy function for childclustering_MM
inline void Memory_management::deallocate_node (Node* n) { delete [] n; }  // delete tree root (~Tree usage only!)


#endif //TREE_VERSION


//=====================================================================================================
#elif defined dynamicarray_MM
//=====================================================================================================

#if TREE_VERSION == 0

Memory_management::Memory_management (const int& dim, const int& f_num, const int& if_num,
				      const int& a_tmp, const int& b_tmp) :
  DIM_NUM(dim), VAL_NUM(f_num), VAL_NUM_I(if_num), NODES_IN_CHUNK(20),
  running_edge_free(0), running_edge_filled(0)
{
#ifdef VERB_MM
  printf ("# MEMORY MANAGEMENT: dynamicarray_MM\n");
#endif
  // set maximum number of nodes in the internal storage
  // initialize running edges
  
  ival = new int [if_num<<NODES_IN_CHUNK];   // allocate internal storage
  val = new Value [f_num<<NODES_IN_CHUNK];
  ii = new int      [dim<<NODES_IN_CHUNK];
  node = new Node     [1<<NODES_IN_CHUNK];
  free = new int      [1<<NODES_IN_CHUNK];   // allocate list of free elements

  int delta_test,
    delta = (int) ( (pointer_t) &node[1] - (pointer_t) &node[0] );
  for (int i=2; i<1<<NODES_IN_CHUNK; i++) {       // validate node array allignment
    delta_test = (pointer_t) &node[i] - (pointer_t) &node[i-1];
    if ((delta<0)||(delta_test != delta)) {
      std::cout << "WARNING! Alignment of node array look strange, "
		<< "deallocate_node() will work unproperly.\nProgram terminated\n";
      exit(1);
    }
  }
}
Memory_management::~Memory_management () {
  delete [] ival;
  delete [] val;
  delete [] ii;
  delete [] node;
  delete [] free;
}
inline Node* Memory_management::allocate_node () {
  int index;
  if (running_edge_free) {                                               // take space from free list
    running_edge_free --;
    index = free [running_edge_free];                                    // index of an empty cell
  } else {                                                               // take space from filled list
    if (running_edge_filled==(1<<NODES_IN_CHUNK)) {                            // check overflow
      std::cout << "More than " << 1<<NODES_IN_CHUNK
		<< " nodes allocated.\nProgram terminated\n";
      exit(1);
    }
    index = running_edge_filled;
  }
  Node* n = &node[index];
  n->id = intermediate_id;
  n->ii = &ii[index*DIM_NUM];
  n->val = &val[index*VAL_NUM];
  n->ival = &ival[index*VAL_NUM_I];
  running_edge_filled ++;
  int i;
  for ( i=0; i<VAL_NUM;   i++)  n->val [i] = 0;
  for ( i=0; i<VAL_NUM_I; i++) n->ival [i] = 0;
  return n;
}
inline void Memory_management::deallocate_node (Node* n) {
  int index = (int) ((pointer_t) n - (pointer_t) &node[0]);              // array index of the node
  free [running_edge_free] = index;                                      // store the index
  running_edge_free ++;
}


#elif TREE_VERSION == 1

Memory_management::Memory_management (const int& dim, const int& f_num, const int& if_num,
				      const int& j_tree, const int& j_max) :
  DIM_NUM(dim), VAL_NUM(f_num), VAL_NUM_I(if_num), NODES_IN_CHUNK(20),
  running_edge_free(0), running_edge_filled(0)
{
#ifdef VERB_MM
  printf ("# MEMORY MANAGEMENT: dynamicarray_MM\n");
#endif
  // set maximum number of nodes in the internal storage
  // initialize running edges
  
  ival = new int [if_num<<NODES_IN_CHUNK];   // allocate internal storage
  val = new Value [f_num<<NODES_IN_CHUNK];
#ifndef FIX_COORD_DATABASE_TREE
  ii = new int      [dim<<NODES_IN_CHUNK];
#endif //FIX_COORD_DATABASE_TREE
  node = new Node     [1<<NODES_IN_CHUNK];
  free = new int      [1<<NODES_IN_CHUNK];   // allocate list of free elements

  int delta_test,
    delta = (int) ( (pointer_t) &node[1] - (pointer_t) &node[0] );
  for (int i=2; i<1<<NODES_IN_CHUNK; i++) {       // validate node array allignment
    delta_test = (pointer_t) &node[i] - (pointer_t) &node[i-1];
    if ((delta<0)||(delta_test != delta)) {
      std::cout << "WARNING! Alignment of node array look strange, "
		<< "deallocate_node () will work unproperly.\nProgram terminated\n";
      exit(1);
    }
  }
}
Memory_management::~Memory_management () {
  delete [] ival;
  delete [] val;
#ifndef FIX_COORD_DATABASE_TREE
  delete [] ii;
#endif //FIX_COORD_DATABASE_TREE
  delete [] node;
  delete [] free;
}
inline Node* Memory_management::allocate_node () {
  int index;
  if (running_edge_free) {                                               // take space from free list
    running_edge_free --;
    index = free [running_edge_free];                                    // index of an empty cell
  } else {                                                               // take space from filled list
    if (running_edge_filled==(1<<NODES_IN_CHUNK)) {                            // check overflow
      std::cout << "More than " << 1<<NODES_IN_CHUNK
		<< " nodes allocated.\nProgram terminated\n";
      exit(1);
    }
    index = running_edge_filled;
  }
  Node* n = &node[index];
  n->id = intermediate_id;
#ifndef FIX_COORD_DATABASE_TREE
  n->ii = &ii[index*DIM_NUM];
#endif //FIX_COORD_DATABASE_TREE
  n->val = &val[index*VAL_NUM];
  n->ival = &ival[index*VAL_NUM_I];
  running_edge_filled ++;
  int i;
  for ( i=0; i<VAL_NUM;   i++)  n->val [i] = 0;
  for ( i=0; i<VAL_NUM_I; i++) n->ival [i] = 0;
  return n;
}
inline void Memory_management::deallocate_node (Node* n) {
  int index = (int) ((pointer_t) n - (pointer_t) &node[0]);              // array index of the node
  free [running_edge_free] = index;                                      // store the index
  running_edge_free ++;
}


#endif //TREE_VERSION


//=====================================================================================================
#elif defined system_MM
//=====================================================================================================


#if TREE_VERSION == 0

Memory_management::Memory_management (const int& dim, const int& f_num, const int& if_num,
				      const int& a_tmp, const int& b_tmp) :
  DIM_NUM(dim), VAL_NUM(f_num), VAL_NUM_I(if_num) {
#ifdef VERB_MM
  printf ("# MEMORY MANAGEMENT: system_MM\n");
#endif
}
Memory_management::~Memory_management () {
}
inline Node* Memory_management::allocate_node () {
  Node* n = new Node;
  n->id = intermediate_id;
  n->ii = new int [DIM_NUM];
  n->val = new Value [VAL_NUM];
  n->ival = new int [VAL_NUM_I];
  int i;
  for ( i=0; i<VAL_NUM;   i++)  n->val [i] = 0.0;
  for ( i=0; i<VAL_NUM_I; i++) n->ival [i] = 0;
  return n;
}

inline void Memory_management::deallocate_node (Node* n) {
  delete n;
}


#elif TREE_VERSION == 1


Memory_management::Memory_management (const int& dim, const int& f_num, const int& if_num,
				      const int& j_tree, const int& j_max) :
  DIM_NUM(dim), QUAD(1<<dim), VAL_NUM(f_num), VAL_NUM_I(if_num) {
#ifdef VERB_MM
  printf ("# MEMORY MANAGEMENT: system_MM\n");
#endif
}

Memory_management::~Memory_management () {}

inline Node* Memory_management::allocate_node () {   // used to allocate tree roots in Forest()
  Node* n = new Node;
  n->id = intermediate_id;
#ifndef FIX_COORD_DATABASE_TREE
  n->ii = new int [DIM_NUM];
#endif //FIX_COORD_DATABASE_TREE
  n->val = new Value [VAL_NUM];
  n->ival = new int [VAL_NUM_I];
  int i;
  for ( i=0; i<VAL_NUM;   i++)  n->val [i] = 0.0;
  for ( i=0; i<VAL_NUM_I; i++) n->ival [i] = 0;
  return n;
}

inline Node* Memory_management::allocate_nodes () {
  Node* n = new Node [QUAD];
  for (int ch=0; ch<QUAD; ++ch) {
    n[ch].id = intermediate_id;
#ifndef FIX_COORD_DATABASE_TREE
    n[ch].ii = new int [DIM_NUM];
#endif //FIX_COORD_DATABASE_TREE
    n[ch].val = new Value [VAL_NUM];
    n[ch].ival = new int [VAL_NUM_I];
    int i;
    for ( i=0; i<VAL_NUM;   ++i) n[ch].val [i] = 0.0;
    for ( i=0; i<VAL_NUM_I; ++i) n[ch].ival [i] = 0;
  }
  return n;
}

inline void Memory_management::deallocate_node (Node* n) { delete n; }
inline void Memory_management::deallocate_nodes (Node* n) {
  //printf ("n->ptr = %x, n->data_ptr=%d\n",n->ptr, n->data_ptr);
  if (n->ptr) {                      // delete QUAD children of the node
    delete [] n->ptr;
    n->ptr = 0;
  }
  
  n->data_ptr = 0;  // set flag, so it will be treated as newly created, so
  //                   its coordinates will be redundantly rewritten in add_by_coordinates
  //                   and its values be zeroed there.
  
  if ( n->tlp )                      // remove from lists
    n->tlp->tlp_node_exclude (n);
}
#endif //TREE_VERSION


//=====================================================================================================
#else
#error "Please define memory management style"
#endif
//=====================================================================================================
