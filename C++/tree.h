/*----------------------------------------------------------------*/
/*     Tree Structured Database                                   */
/*                                                                */
/*     Developed by Alexei Vezolainen                             */
/*                                                                */
/*     Please respect all the time and work                       */
/*     that went into the development of the code.                */
/*                                                                */
/*----------------------------------------------------------------*/
#ifndef DATABASE_TREE_H
#define DATABASE_TREE_H

#define MIN(a,b) (((a)<(b)) ? (a) : (b))

#ifdef PTR_LONGLONG                         //  P_SIZE should be equal to pointer size
typedef unsigned long long int  P_SIZE;     //  integer*8
#else                                       //
typedef unsigned long int P_SIZE;           //  integer*4 on 32 bits, integer*8 on 64 bits
#endif                                      //

//#include <stdlib.h>  // JDR 
#include <cstdlib> // JDR 
#include <cstdio> // JDR 

#ifdef EBUG_SAFE
#include <iostream>
#endif

#include <vector>
#include <bitset>
using std::vector;

#include <execinfo.h>



// Clean ID values: ghost, significant, additional, etc.
// IDs also can be mixed, e.g. significant+adjacent, etc.
// ID is intermediate for the nodes which are not in link lists
// and used only for mantaining the tree structure
typedef int ID;
const ID zero_id = 0;               /* 0000 0000 */
const ID significant_id = 1;        /* 0000 0001 */
const ID adjacent_id = 2;           /* 0000 0010 */
const ID ghost_id = 4;              /* 0000 0100 */
const ID old_id = 8;                /* 0000 1000 */
const ID msk_id = 16;               /* 0001 0000 */

const ID id_of_bisected_amr = 32;    // ID of an AMR node, whose cell has been bisected
const ID intermediate_id = 64;       // ID of internal to tree structure node
const ID id_of_added_amr = 128;      // ID of additional AMR nodes, or added for parallel ghost addition
const bool front_of_added = false;   // will add all AMR nodes like that

typedef double Value;           // precision of function values (REAL*8)
#define IWLT_I_P 0              // iwlt stored as ival[IWLT_I_P] for amr::interpolate




/*-----------------------------------------------------------------------------------------*/
/*  Various optimization parameters                                                        */
/*-----------------------------------------------------------------------------------------*/
#define DIM_MAX 4                                   // Maximum possible dimension
//#define FIX_COORD_DATABASE_TREE                   // Static coordinate array Node.ii[]
//#define FIX_FINEST_MESH_DATABASE_TREE             // Static Global_variables and Forest.FINEST_MESH
//#define FIX_OPT1_FACTOR_DATABASE_TREE             // Static Forest.opt1_factor
//#define FIX_INDEX_ON_FINEST_MESH_DATABASE_TREE    // Static Forest.index_on_finest_mesh
// //#define FIX_DIM_LOOPS_DATABASE_TREE          // Set loops limits to 0:DIM_MAX




/*-----------------------------------------------------------------------------------------*/
/*  Global memory management class                                                         */
/*-----------------------------------------------------------------------------------------*/
//#define dynamicarray_MM //system_MM//childclustering_MM //dynamicarray_MM //system_MM//
#define system_MM
// system_MM              -  at the will of the operator new                               
// dynamicarray_MM        -  put nodes into huge array of a fixed size                     
// childclustering_MM     -  cluster parent with its children     
//#define EBUG_MM                     // DEBUG Memory management operations
//#define VERB_MM                     // PRINT Memory management info



/*-----------------------------------------------------------------------------------------*/
/* Node                                                                                    */
/*-----------------------------------------------------------------------------------------*/
class Node;
typedef unsigned long pointer_t;
class Memory_management
{
 private:
  const int DIM_NUM, VAL_NUM, VAL_NUM_I;   // ... local copy of Global DIM_NUM, LEAVES
#if TREE_VERSION == 1
  const int QUAD;                          // ... == 2**DIM_NUM
#endif
  
#ifdef dynamicarray_MM
  const int NODES_IN_CHUNK;            // log2 of maximum number of nodes in a single chunk of memory
  Node* node;                        // internal storage
  int* ii;                           // ...
  Value* val;                        // ...
  int* ival;                         // ...
  int running_edge_filled;      // the next after the last filled element of the internal storage
  int running_edge_free;        // the next after the last element of free array
  int* free;                         // internal list of free storage
#endif
#ifdef childclustering_MM
  bool IS_ODD;                 // == (J_MAX - J_TREE + 1) & 1
#endif
 public:
  
  inline void deallocate_node (Node*);
  inline Node* allocate_node ();
  Memory_management (const int&, const int&, const int&, const int&, const int&);
  ~Memory_management ();

#if TREE_VERSION == 1
 public:
  inline void deallocate_nodes (Node*);
  inline Node* allocate_nodes ();
#ifdef childclustering_MM
  inline void deallocate_nodes (Node*, const int&);
  inline Node* allocate_nodes (const int&, Node*&, const int root_ch_index=0);
#endif
 private:
  inline Node* allocate_group_of_nodes (const int&);
#endif
};

/*-----------------------------------------------------------------------------------------*/
/*  Global_variables class                                                                 */
/*-----------------------------------------------------------------------------------------*/
class Global_variables
{
 public:
#if TREE_VERSION == 0
  int* FINEST_MESH;          // m_x * 2^{J_max-1}
  int* PERIODIC;             // periodic boundary conditions marker
  const int DIM_NUM,         // number of space dimensions (1 or more)
    LEAVES,                  // number of leaves of -1 level
    VAL_NUM,                 // number of real function values
    VAL_NUM_I,               // number of integer function values
    J_MAX,                   // max level
    J_TREE,                  // from that level trees are growing
    TYPES_NUM,               // number of types (4,8,etc)
    par_rank,                // rank of the processor
    par_size;                // total number of the processors
  
  
#elif TREE_VERSION == 1
  
#ifndef FIX_FINEST_MESH_DATABASE_TREE
  int* FINEST_MESH;            // m_x * 2^{J_max-1}
#else
  int FINEST_MESH[DIM_MAX];
#endif
  int* PERIODIC,               // periodic boundary conditions marker
    DIM_NUM,                 // number of space dimensions (1 or more)
    VAL_NUM,                 // number of real function values
    VAL_NUM_I,               // number of integer function values
    J_MAX,                   // max level
    J_TREE,                  // from that level trees are growing
    TYPES_NUM,               // number of wavelet types (4,8,etc)
    QUAD,
    par_rank,                // rank of the processor
    par_size;                // total number of the processors
  
#endif

  Global_variables (const int*&, const int*&,
		    const int&, const int&,
		    const int&, const int&,
		    const int&, const int&, const int&);
  ~Global_variables ();

};


/*-----------------------------------------------------------------------------------------*/
/*  Node class (octal tree node (Node) and node_data (Node_data)                           */
/*-----------------------------------------------------------------------------------------*/
/* class Node */
/* { */
/*   Node* ptr;              // pointer to the other node */
/*   Node_data* data_ptr;    // pointer to the node's data */
/* }; */



/*-----------------------------------------------------------------------------------------*/
/*  Node class                                                                             */
/*-----------------------------------------------------------------------------------------*/
class Type_level;
class Node
{
 public:
  
#if TREE_VERSION == 0
  
#ifdef childclustering_MM
  int storage_array_marker;
#endif
  Value *val;                // function values
  int *ii;                   // coordinates: ix, iy, iz

  int *ival;                 // integer function values
  Type_level* tlp;           // The type-level list the node belongs to
  //union {         // NOTE: union causes problems with DB_finalize calling tlp_exclude
  //                // we may introduce flag not to use tlp at all for stand-alone visualization
  int tlp_index;           // index in level - wavelet type - facetype tlp array
  int vid;                 // VTK point number (for visualization only)
  //};
  ID id;                     // significance ID
  Node **leaves;             // array of pointers to node's children
  int num_leaves;            // maximum number of leaves
  Node* next_tlp;            // type-level pointers to the previous list element
  Node* prev_tlp;            // type-level pointers to the next list element
  int level;                 // internal level (root internal level is 0)

  Node (const ID id_in, const int DIM_NUM, const int coord_in[]);
  
  void set_value_vector (const int& VAL_NUM, const int& VAL_NUM_I);
  void rearrange_leaves (const int&, const int&);
  void zero_value_vector (const int& VAL_NUM, const int& VAL_NUM_I);
  
  
#elif TREE_VERSION == 1
  
  Node* ptr;              // pointer to the other nodes
  int data_ptr;           // if data has been assigned
  Value *val;                // function values
#ifndef FIX_COORD_DATABASE_TREE
  int *ii;                   // coordinates: ix, iy, iz
#else
  int ii[DIM_MAX];
#endif
  
  Type_level* tlp;                // The type-level list the node belongs to
  //  union {
  int tlp_index;                // index in level - wavelet type - facetype tlp array
  int vid;                      // VTK point number (for visualization only)
  //  };
  Node* next_tlp;                 // type-level pointers to the previous list element
  Node* prev_tlp;                 // type-level pointers to the next list element

  int *ival;                 // integer function values
  ID id;                     // significance ID


  

#endif
  
  Node ();
  ~Node ();
  inline void set_element_of_value_vector (const int& pos, const Value& val_in) { val [pos] = val_in; }
  inline void set_element_of_ivalue_vector (const int& pos, const int& val_in) { ival [pos] = val_in; }
  inline Value* get_values () { return val; }
  inline int* get_ivalues () { return ival; }
  inline Type_level* get_tlp () { return tlp; }
  inline void set_tlp (Type_level* q) { tlp=q; }
  inline Node* get_child (const int ch) const {     //  return pointer to node's child
#if TREE_VERSION == 0
    return leaves[ch];
#else
    return ptr + ch;
#endif
  }
  
  //void move_node (Node*, int*, Value*, int*, int, int, int, Memory_management*);
};



/*-----------------------------------------------------------------------------------------*/
/*  Tree class                                                                             */
/*-----------------------------------------------------------------------------------------*/
class Forest;
class Tree
{
 public:
  static int tsize;                           // largest spacing (in finest_mesh units)
  static int tsize_pos;                       // ...
  
  Tree (Node*, const int, Global_variables*, Forest*, Memory_management*, const int&);
  ~Tree ();
  int add_by_coordinates (const int[], Node*&, const int&, const int);
  P_SIZE test_by_coordinates_1 (const int[]);
  Value* test_by_coordinates_nonzero (const int* const);
  
  inline Node* get_root () const { return root; }
  inline int get_proc () const { return proc; }
  inline void set_proc (const int p) { proc = p; }
  
  
  void reshuffle (Node*);

 private:
  static Forest *f;
  static Global_variables *g;
  static Memory_management *m;
  Node *root;                      // root of the tree
  int proc;                        // which processor current tree belongs to

#if TREE_VERSION == 1
  static int DIM_NUM, J_MAX, J_TREE, VAL_NUM, VAL_NUM_I;
#endif
};


/*-----------------------------------------------------------------------------------------*/
/*  Type-level link system:                                                                */
/*   nodes are organized into link-lists                                                   */
/*   according to their wavelet types and levels                                           */
/*-----------------------------------------------------------------------------------------*/
class Type_level
{
 private:
  const Global_variables* const g;
  int pow3,                                     // FACETYPE_NUM                      (3^dim)
    pow4,                                       // FACETYPE_NUM * TYPES_NUM          (3^dim * 2^dim)
    pow5;                                       // FACETYPE_NUM * TYPES_NUM * J_MAX
 public:
  vector <Node*> tlp_begin;                     // first elements of type-level list
  vector <Node*> tlp_end;                       // current last element of the list
  vector <int> tlp_number_of_elements;          // number of elements in the list
  
  Type_level (const Global_variables* const glob)
    : g(glob), pow3(1) {
    for (int dim=0; dim<g->DIM_NUM; ++dim)
      pow3 *= 3;
    pow4 = pow3*g->TYPES_NUM;
    pow5 = pow4*g->J_MAX;
    tlp_begin.             resize (pow5 * g->par_size, (Node*) 0);
    tlp_end.               resize (pow5 * g->par_size, (Node*) 0);
    tlp_number_of_elements.resize (pow5 * g->par_size,         0);
    // hence the total number of link-lists is
    // J_MAX * TYPES_NUM * FACETYPES_NUM * PAR_SIZE
    //
  }
  void tlp_node_include_f (Node*);          // include node into tlp list (front)
  void tlp_node_include_b (Node*);          // include node into tlp list (back)
  void tlp_node_exclude (Node*);            // exclude node from tlp list
  

  inline int tlp_index                      // 1D index <--> (face_type,type,level)
    (const int& face_type, const int& type, const int& level, const int& proc)
    {
#ifdef EBUG_SAFE
      if ( (level<1) || (level > g->J_MAX) || (type<0) || (type>=g->TYPES_NUM) ||
          (proc<0) || (proc>=g->par_size) || (face_type<0) || (face_type>=pow3) ) {
        std::cout << "  DEBUG: error in Type_level::tlp_index( "
          << "type (EXPECT[0," << g->TYPES_NUM << "])=" << type 
          << ", level (EXPECT [1," << g->J_MAX << "])=" << level 
          << ", face_type (EXPECT [0," << pow3 - 1 << "])=" << face_type
          << ", proc (EXPECT [0," << g->par_size - 1 << "] ) =" << proc << " )"
          << std::endl << "  wrong parameter value" << std::endl;
          // BACKTRACE stuff
          void *array[10];
          size_t size;
          size = backtrace(array, 10);// get void*'s for all entries on the stack
          backtrace_symbols_fd(array, size, STDERR_FILENO);

        exit(1);
      }
      test_index( face_type + type*pow3 + (level-1)*pow4 + proc*pow5, "tlp_index()" );
#endif
      return face_type + type*pow3 + (level-1) * pow4 + proc*pow5;
    }

  inline int get_pow3 () const {return pow3;}
  inline int get_pow5 () const {return pow5;}
  
  inline void decr_tlp_number_of_elements(int i) {
#ifdef EBUG_SAFE
    test_index( i, "decr_tlp_number_of_elements()" );
#endif
    tlp_number_of_elements[i] --;
  }
  inline void incr_tlp_number_of_elements(int i) {
#ifdef EBUG_SAFE
    test_index( i, "incr_tlp_number_of_elements()" );
#endif
    tlp_number_of_elements[i] ++;
  }
  inline int get_tlp_number_of_elements(int i) {
#ifdef EBUG_SAFE
    test_index( i, "get_tlp_number_of_elements()" );
#endif
    return tlp_number_of_elements[i];
  }
  
#ifdef EBUG_SAFE
 private:
  inline void test_index(int i, char* str) {
    if (i>=tlp_number_of_elements.size() || i<0) {
      printf ("%s: wrong index %d for size %d",str,i,tlp_number_of_elements.size());
      exit(1);
    }
  }
#endif
};

/*-----------------------------------------------------------------------------------------*/
/*  Forest class                                                                           */
/*-----------------------------------------------------------------------------------------*/
class Forest
{
 public:
  Type_level f_sig, f_gho;        // Type-level information lists
  Value* zero_val;                // Array of zero values
  
  Forest (const int*const, Global_variables*, Memory_management*, const int*const);
  ~Forest ();


  // parallel related functions --------------
  inline int get_proc () const { return par_rank; }
  void count_tree_nodes (const int*const, const int, int*, long*, Value*, const int);       //!AR 2/27/2011!  int*  changed to  long*
  int get_proc_by_coordinates (const int[], const int) const;
  void update_proc_info (const int*const);
  inline bool get_processor_of_the_node (const Node* const n) const
    {
      // return the node's processor based on its tlp index,
      // assumes that tlp index has been set for the node by f->set_type_level()
      return n->tlp_index / f_sig.get_pow5();
    }
  
  // -----------------------------------------

  int add_by_coordinates (const int[], const int, Node*&, const int&, const bool);
  P_SIZE test_by_coordinates (const int[], const int);
  Node* get_root_by_jmax_coordinates (const int []);
  Node* get_initial_type_level_node (const int&, const int&, const int&, const int&, int*, const int&);
  Node* get_next_type_level_node (const int&, const int&, const int&, const int&, const Node*, const int&);


  void get_function_by_jmax_coordinates (const int* const x,
					 const int*const i_low, const int*const i_high,
					 Value* f_out);

  //  void add_locally(const int[], Node*&, const int&, const int&, const bool);

  int calc_face_type_index (const int coord []) {
    //    non-periodic:      periodic for X:
    //
    //    Y   6  7  8          7 7 .
    //    |   3  4  5          4 4 .
    //    |   0  1  2          1 1 .
    //    |
    //    +------X
    int i,
      factor = 1,
      new_index = 0;
    for (i=0; i < DIM_NUM; ++i) {
      new_index += PERIODIC[i] * factor + (1-PERIODIC[i]) * factor *
	( MIN(1,coord[i]) + coord[i]/FINEST_MESH[i] );
      factor += factor<<1;
    }

    return new_index;  // from [0 .. 3^dim-1]
  }
  

  int set_nwlt_per_tree (int*, const int);
  void reshuffle ();
  void test_tlp (Type_level*);
  void clean_the_forest ();
  void delete_the_forest (int proc_to_exempt=-1);

  void print_info (bool verb=0, int print_tree_number=-1);

  inline int get_stat_nnodes () const { return stat_nnodes; }
  void count_nodes_to_visualize ();
  inline int get_number_of_trees () const { return number_of_trees; }
  inline Tree** get_trees () const { return trees; }
  inline int get_x_space () const { return x_space; }

  bool found_nonzero_child_link (const Node*, int&, const int&, int&);
  bool found_nonzero_child_link_bnd (const Node*, int&, const int&, int&);
  
#if TREE_VERSION == 1
  int find_node_tree_level (int*);  // 0 .. J_MAX-J_TREE
#endif
  

 private:
  const int DIM_NUM, J_MAX, J_TREE, VAL_NUM, VAL_NUM_I, 
    par_rank,
    par_size;
#if TREE_VERSION == 1
  const int QUAD;
#endif
  int *PERIODIC;
  

#ifndef FIX_INDEX_ON_FINEST_MESH_DATABASE_TREE
  int *index_on_finest_mesh;                               // ... local buffer for finest coordinates
#else
  int index_on_finest_mesh[DIM_MAX];
#endif
#ifndef FIX_OPT1_FACTOR_DATABASE_TREE
  int *opt1_factor;                                        // ... optimized factor array
#else
  int opt1_factor[DIM_MAX];
#endif
#ifndef FIX_FINEST_MESH_DATABASE_TREE
  int* FINEST_MESH;
#else
  int FINEST_MESH[DIM_MAX];
#endif
  
  
  int x_space_pos,                // ...
    x_space;                      // ... spacing in the grid of tree roots
  Global_variables *g;
  Memory_management *m;
  int number_of_trees;
  Tree **trees;                   // the trees of the forest be stored here
  int stat_nnodes;                // total number of nodes to visualize
  
  
  // ------ interpolation related -----------------
#if TREE_VERSION == 0
 private:
  void bisect_amr_cell
    (Node*, const int, const int);
  void add_central_amr_nodes
    ();
  void add_central_amr_nodes_aux
    (Node* node, int *coord, int *delta, vector <bool> &ch_typev, vector <bool> &mask);
  void join_sig_path_for_amr
    ();
  void get_coord_by_child_index
    (const Node*, const int, int[]);
  void make_amr_box
    (const int, Node**,
     const std::vector< int >&, const std::vector< int >&);
  bool search_smallest_amr
    (Node**, int&,
     const std::vector< float >&, const std::vector< std::vector<float> >&,
     const int);
#endif
  
 public:
  void make_amr
    ();
  void interpolate 
    (const Value*const, Value*, const int, const int*const, const int,
     const vector< vector<float> >&, const int);
  void interpolate_to_box
    (const Value*const u, const int nwlt, const int add_var_marker,
     Node**, float*,const int*const vars, const int vsize,
     const std::vector< float >& fxyz, const std::vector< std::vector<float> >&,
     int&, Value*);
  void interpolate_to_box_h
    (const Value*const u, const Value*const du, const int nwlt, const int add_var_marker,
     Node**, float*, float*,const int*const vars, const int vsize,
     const std::vector< float >&, const std::vector< std::vector<float> >&,
     int&, Value*);
  // ------ interpolation related -----------------
  
  
 public:
  // set tlp_index for the node;
  void set_type_level (Node*, const int&);
  
  // change tlp_index according to the new owner processor of the node
  inline void update_type_level (Node* node, const int& old_p, const int& new_p) {
    node->tlp_index += f_sig.get_pow5()*(new_p-old_p);
  }
  
  
  
#ifdef EBUG
 private:
  void print_box (Node**);
 public:
  int test_amr_box (Node**,
		    const int,
		    const std::vector< float >&,
		    const std::vector< std::vector<float> >&,
		    const bool verb=0);
#endif
  
 private:
  // terminate the code with error "try using DB=db_tree instead"
  static void terminate (const char*);
};

#endif /* DATABASE_TREE_H */
