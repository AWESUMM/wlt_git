/*----------------------------------------------------------------*/
/*     Tree Structured Database                                   */
/*                                                                */
/*     Developed by Alexei Vezolainen                             */
/*                                                                */
/*     Please respect all the time and work                       */
/*     that went into the development of the code.                */
/*                                                                */
/*----------------------------------------------------------------*/
#include <fstream>
#include <iostream>
#include <string.h>

// #if (TREE_VERSION == 1)
// #include "tree1.h"
// #else
// #include "tree.h"
// #endif

#include "tree.h"
#include "interface.h"
#include "amr.h"

#ifdef xgraphics  
#include "xx.h"
Xgraphics * XG;
#endif

// ---------------------------- global variables
Global_variables *g;
Forest *f;
Memory_management *m;
AMR *amr;
// ---------------------------- static members
Forest* Tree::f = 0;
Global_variables* Tree::g = 0;
Memory_management* Tree::m = 0;
int Tree::tsize = 0, Tree::tsize_pos = 0;


// //#define monitor
// #ifdef monitor
// std::ofstream access_file;
// void access (Node *node) {
//   if (node != 0) {
// //     for (int i=0; i<g->DIM_NUM; i++)
// //       access_file << node->ii[i] <<" ";
//     access_file << node->level << " ";
// // << "\n";
//     access_file.flush();
//   }
// }
// #endif
// extern "C" {
//   void db_xclear_all () {
// #ifdef xgraphics
//     XG->xclear_all ();
//     XG->xprint_mesh (g->FINEST_MESH[0],
// 		     g->FINEST_MESH[1],
// 		     g->J_MAX,
// 		     g->J_TREE);                   // print mesh at root level
//     XG->xprint_scale (g->J_MAX);                   // print color scale by levels
// #endif
//   }
//   void db_xprint (const P_SIZE* const ptr, const int * const scale)
//   {
// #ifdef xgraphics
//     Node *a = (Node*) *ptr;
//     if (a->level) // non-root node, maybe an intermediate one
//       XG->xprint_node (a->ii, a->level+g->J_TREE, *scale);
//     else          // tree root, internal level == 0
//       XG->xprint_node (a->ii, a->my_level, *scale);
// #endif
//   }
// } // of extern "C"

// DEBUG only, does not need to be included into lowercase.h
#define DB_PRINT_INFO db_print_info_
extern "C" void DB_PRINT_INFO () {
#ifdef EBUG_SAFE
  f->print_info (true);
  std::cout.flush ();
#endif
}
extern "C" void db1_ (const P_SIZE* p_in) {
  Node* n = (Node*) *p_in;
  printf ("testing node [%d %d] at %d, tlp=%p, id=%d\n",
	  n->ii[0],n->ii[1],g->par_rank,n->get_tlp(),n->id);
  if ( n->get_tlp() == & f->f_sig ) printf ("of SIG list\n");
  if ( n->get_tlp() == & f->f_gho ) printf ("of GHO list\n");
}

/*----------------------------------------------------------------------
  ---   MODE==1   ---
  Count/write boundary nodes for the requested trees
  tree_num      - array of (tree number, direction) pairs
  tree_num_size - size
  out_num       - returned array - number of nodes in each tree
  nodes         - returned array - 1D coordinates of the boundary nodes
  values        - returned array - function values of the boundary nodes
  ---   MODE==0   ---
  Count/write nodes for the requested trees
  tree_num      - array of tree numbers
  tree_num_size - size
  out_num       - returned array - number of nodes in each tree
  nodes         - returned array of (1D coordinate, ID) pairs of the boundary nodes
  values        - returned array - function values of the boundary nodes
  ----------------------------------------------------------------------*/
void DB_COUNT_TREE_NODES
(const int*const tree_num, const int*const tree_num_size, int* out_num, const int*const mode) {
  f->count_tree_nodes (tree_num, *tree_num_size, out_num, 0, 0, *mode);           // count tree nodes
}
void DB_WRITE_TREE_NODES
(const int*const tree_num, const int*const tree_num_size, int* out_num, long* nodes, Value* values, const int*const mode) {  //!AR 2/27/2011!  int* nodes  changed to  long* nodes
  f->count_tree_nodes (tree_num, *tree_num_size, out_num, nodes, values, *mode);  // write 1D coordinate and values
}

/*----------------------------------------------------------------------
  Return 1 if node is in the list of significant/adjacent points,
  return 0 otherwise. This function to be used from db_tree
  indices subroutine to order wavelets similarly to db_wrk version.
  p_in       - pointer to the node
  out        - the returned output
  ----------------------------------------------------------------------*/
void DB_TEST_IF_IN_SIG_LIST_BY_POINTER( const P_SIZE* p_in, int* out ) {
  
  *out = 0;
  Node* n = (Node*) *p_in;
  if ( n->get_tlp() == & f->f_sig )
    *out = 1;
}

// /*----------------------------------------------------------------------
//   Write the database into the file
//   ----------------------------------------------------------------------*/
// void DB_WRITE_DB ()
// {
//   const char filename[] = "db_tree.dat";
  
//   std::ofstream ofile (filename, std::ios::out|std::ios::binary);
//   if (!ofile) {
//     std::cout << "ERROR: while writing the file " << filename << "\n";
//     exit (1);
//   } else {
//     std::cout << "writing ... ";
//   }
//   // write start marker
//   const char start[] = "%tree_db";
//   ofile.write (start, 8*sizeof(char));
//   // write global variables
//   ofile.write ((char*) &g->DIM_NUM, sizeof(int));               // dimension
//   ofile.write ((char*) &g->J_TREE, sizeof(int));                // j_tree
//   ofile.write ((char*) &g->J_MAX, sizeof(int));                 // j_max
//   ofile.write ((char*) &g->VAL_NUM, sizeof(int));               // f_num
//   ofile.write ((char*) &g->VAL_NUM_I, sizeof(int));             // if_num
//   int *m = new int [g->DIM_NUM];
//   int *p = new int [g->DIM_NUM];
//   int factor = 1 << (g->J_MAX-1);
//   for (int i=0; i<g->DIM_NUM; i++) {
//     m[i] = g->FINEST_MESH[i] / factor;
//     p[i] = g->PERIODIC[i];
//   }
//   ofile.write ((char*) m, g->DIM_NUM *sizeof(int));             // m[]
//   ofile.write ((char*) p, g->DIM_NUM *sizeof(int));             // p[]
//   delete [] m;
//   delete [] p;
//   // write all the nodes
//   int total_nodes = 0;
//   long int total_nodes_position = ofile.tellp ();
//   ofile.write ((char*) &total_nodes, sizeof(int));              // write total number of nodes

//   for (short int facetype=0; facetype < f->f_sig.get_pow3(); facetype++)
//     for (short int type=0; type < g->TYPES_NUM; type ++)
//       for (short int level=1; level <= g->J_MAX; level++) {
// 	int num = 0;
// 	Node *next_node, *first_node =
// 	  f->get_initial_type_level_node (type, level, facetype, &num, 3);
// 	total_nodes += num;
// 	while (first_node != 0) {
// 	  // write the node's data ------
// 	  int id = first_node->id;
// 	  ofile.write ((char*) first_node->ii, g->DIM_NUM *sizeof(int));  // coord []
// 	  ofile.write ((char*) &id, sizeof(ID));                                          // ID
// 	  // ----------------------------
// 	  next_node =
// 	    f->get_next_type_level_node (type, level, facetype, first_node, 3);
// 	  first_node = next_node;
// 	}
//       }
//   // write end marker
//   ofile.write (start, 8*sizeof(char));
//   // update total number of nodes
//   ofile.seekp (total_nodes_position);
//   ofile.write ((char*) &total_nodes, sizeof(int));
//   ofile.close ();
//   std::cout << "done. " << total_nodes << " nodes\n";
//   std::cout.flush ();
// }

// /*----------------------------------------------------------------------
//   Read the database from the file and initialize the database
//   ----------------------------------------------------------------------*/
// void DB_READ_DB()
// {

//   const char filename[] = "db_tree.dat";
  
//   std::ifstream ifile (filename, std::ios::in|std::ios::binary);
//   if (!ifile) {
//     std::cout << "ERROR: while reading the file " << filename << "\n";
//     exit (1);
//   } else {
//     std::cout << "reading ... ";
//   }
//   // read start marker
//   char start[] = "%tree_db";
//   ifile.read (start, 8*sizeof(char));
//   if (!ifile.good()) {
//     std::cout << "ERROR: while reading start marker from the file " << filename << "\n";
//     exit (1);
//   }
//   // read global variables
//   int dim, j_tree, j_max, val, ival, *p, *m;
//   ifile.read ((char*) &dim, sizeof(int));                // dimension
//   ifile.read ((char*) &j_tree, sizeof(int));             // j_tree
//   ifile.read ((char*) &j_max, sizeof(int));              // j_max
//   ifile.read ((char*) &val, sizeof(int));                // f_num
//   ifile.read ((char*) &ival, sizeof(int));               // if_num
//   p = new int [dim];
//   m = new int [dim];
//   ifile.read ((char*) m, dim*sizeof(int));               // m[]
//   ifile.read ((char*) p, dim*sizeof(int));               // p[]
//   // initialize the database
// //   cout << dim << " " << j_tree << " " << j_max << " " << val << " " << ival << "\n";
// //   for (int i=0; i<dim; i++) cout << m[i] << " " << p[i] << "\n";
//   DB_DECLARE_GLOBALS (m, p, &j_tree, &j_max, &dim, &val, &ival);
//   delete [] p;
//   int total_nodes;
//   ifile.read ((char*) &total_nodes, sizeof(int));   // total number of nodes
//   std::cout << total_nodes << " nodes ... ";
//   ID id;
//   for (int i = 0; i < total_nodes; i++) {
//     ifile.read ((char*) m, dim*sizeof(int));  // coord []
//     if (!ifile.good()) {
//       std::cout << "ERROR: while reading node " << i << "\n";
//       exit (1);
//     }
//     ifile.read ((char*) &id, sizeof(ID));     // ID
//     if (!ifile.good()) {
//       std::cout << "ERROR: while reading node " << i << "\n";
//       exit (1);
//     }
// //     std::cout <<"#"<<i<<" "<<m[0]<<" "<<m[1]<< " "<<id<<"\n";
//     // add the node to the database
//     Node *a;
//     f->add_by_coordinates (m, j_max, a, id, 0);
//   }
//   delete [] m;
//   std::cout << " done.\n";
// //   // read start marker
// //   ifile.read (start, 8*sizeof(char));
// //   cout << "*" << start << "*\n";

//   ifile.close ();
//   std::cout.flush ();
// }

/*----------------------------------------------------------------------
  Extend the grid to an AMR grid by adding extra nodes
  ----------------------------------------------------------------------*/
void DB_MAKE_AMR () {
  f->make_amr ();
}
/*----------------------------------------------------------------------
  Write the comment line for AMR output file (less than 256 characters).
  May be called once or any number of times.
  ----------------------------------------------------------------------*/
void DB_WRITE_AMR_COMMENT (const char* c, const int* const clen, const int* const ir, const int* const ii) {
  // c - string from FORTRAN c(1:clen)
  char* name=new char[*clen+1];
  strncpy ( name, c, *clen );
  name[*clen]='\0';
  
  amr->set_string_header (name);
  amr->set_number_of_vars_to_output (*ir, *ii);
  delete [] name;
}
/*----------------------------------------------------------------------
  Set variable names (to be used in VTK writing)
  nvar - variable number   \in [1..n_var]
  c    - variable name     TRIM(u_variable_names(i))
  clen - name length       LEN(TRIM(u_variable_names(i)))
  ----------------------------------------------------------------------*/
void DB_AMR_SET_VARIABLE_NAME (const int* nvar, const char* c, const int* const clen)
{
  char* name=new char[*clen+1];
  strncpy ( name, c, *clen );
  name[*clen]='\0';
  
  for (int i=0; i<*clen; ++i)  // substitute spaces inside the variable name
    if (name[i]==' ')          // (VTK will not take spaces, for example)
      name[i] = '_';
  
  amr->set_point_data_names ( *nvar, name );
  delete [] name;
}

/*----------------------------------------------------------------------
  Set coordinate vector, similar to xx(0:MAXVAL(nxyz),1:dim)
  for a fixed dimension dim <-> xx(0:len,dim)
  c_i - 1(integer coordinates) 0(real)
  ----------------------------------------------------------------------*/
void DB_AMR_SET_COORDINATES (const int* c_i, const int* len, const int* dim, const Value* xx)
{
  amr->set_coord_integer (*c_i);
  if ( ! *c_i )
    amr->set_coord_vector ( *len, *dim, xx );
}
/*----------------------------------------------------------------------
  Write the AMR grid into a file in VTK format
  (readable by Paraview or VisIt) for 2 or 3 dimensional case,
  and into a simple text file (readable by XMGR) for 1D.
  DB_MAKE_AMR () assumed to be called before that function.
  File type (ft) is 'b' or 'a' for binary and ASCI respectively.
  Use tetrahedra if (u_t) is 1, use blocks if it is 0.
  ----------------------------------------------------------------------*/
void DB_AMR_VTK_WRITE (const char* c, const int* const clen,  const char* ft, const int* u_t)
{
  // ft  - assumed to be a single character
  // c - string from FORTRAN c(1:clen)
  char* name=new char[*clen+1];
  strncpy ( name, c, *clen );
  name[*clen]='\0';
  
  amr->set_param (*u_t);
  amr->write_amr (name, ft);
  delete [] name;
}
/*----------------------------------------------------------------------
  Write the AMR grid into a file in EnSight Gold format
  (readable by Ensight) for 2 or 3 dimensional case,
  and into a simple text file (readable by XMGR) for 1D.
  DB_MAKE_AMR () assumed to be called before that function.
  File type (ft) is 'b' or 'a' for binary and ASCI respectively.
  Use tetrahedra if (u_t) is 1, use blocks if it is 0.
  ----------------------------------------------------------------------*/
void DB_AMR_ENSGHT_WRITE (const char* c, const int* const clen,  const char* ft, const int* u_t)
{
  // ft  - assumed to be a single character
  // c - string from FORTRAN c(1:clen)
  char* name=new char[*clen+1];
  strncpy ( name, c, *clen );
  name[*clen]='\0';
  
  amr->set_param (*u_t);
  amr->write_amr_ensght (name, ft);
  delete [] name;
}
/*----------------------------------------------------------------------
  Write the AMR grid into a file in plot3d format
  (readable by Tecplot) for 2 or 3 dimensional case,
  and into a simple text file (readable by XMGR) for 1D.
  DB_MAKE_AMR () assumed to be called before that function.
  File type (ft) is 'b' or 'a' for binary and ASCI respectively.
  Use tetrahedra if (u_t) is 1, use blocks if it is 0.
  ----------------------------------------------------------------------*/
void DB_AMR_PLOT3D_WRITE (const char* c, const int* const clen, const char* ft, const int* u_t)
{
  // ft  - assumed to be a single character
  // c - string from FORTRAN c(1:clen)
  char* name=new char[*clen+1];
  strncpy ( name, c, *clen );
  name[*clen]='\0';

  amr->set_param (*u_t);
  amr->write_amr_plot3d (name, ft);
  delete [] name;
}

/*----------------------------------------------------------------------
  Write the AMR grid into a file in CGNS format
  (readable by Tecplot) for 2 or 3 dimensional case,
  and into a simple text file (readable by XMGR) for 1D.
  ----------------------------------------------------------------------*/
void DB_AMR_CGNS_WRITE (const char* c, const int* const clen, const char* ft, const int* u_t)
{
  char* name=new char[*clen+1];
  strncpy ( name, c, *clen );
  name[*clen]='\0';
  
  amr->set_param (*u_t);
  amr->write_amr_cgns (name);
  delete [] name;
}

/*----------------------------------------------------------------------
  Interpolate function values to the given locations
  u         - u(1:nwlt, 1:n_var) - use that array if avm==1
  nwlt      - ...
  n_var     - ...
  du        - du(1:n_var,1:nwlt,1:dim)
  avm       - 0-functions are stored in DB, 1-iwlt is stored in DB as integer
  x         - array of points, FORTRAN x(1:x_dim,1:x_size)
  f         - returned array of interpolated function values, FORTRAN f(1:vars_size,1:x_size)
  x_dim     - first size of x array
  x_size    - second size of x and f arrays
  vars      - array of numbers of variables to interpolate for, FORTRAN vars(1:vars_size)
  vars_size - length of the array of numbers of variables, first size of f array
  mode      - 0 - direct interpolation, good for small arrays of x ( general approach )
  ...       - 1 - use preprocessing, good for large arrays ( uniform coordinates only )
  ...       - 2 - use preprocessing, good for large arrays ( general approach )
  order     - order of interpolation (0-linear)
  vtr       - variable # to put into .ren file for the volume renderer
  vtn       - name of the .ren file
  vtl       - length of the .ren file
  ----------------------------------------------------------------------*/
void DB_INTERPOLATE (const Value*const u, const int*const nwlt, const int*const n_var,
		     const Value*const du, const int*const avm,
		     const Value*const x, Value *f, const int*const x_dim, const int* const x_size,
		     const int*const vars, const int*const vars_size,
		     const int* const mode, const int* const order,
		     const int* const vtr, const char*const vtn, const int* const vtl)
{
  if (*x_dim != g->DIM_NUM) {
    fprintf (stderr, "Error: DB_interpolate ()\n");
    fprintf (stderr, "Coordinate dimension %d is different from the database dimension %d\n",
	     *x_dim, g->DIM_NUM);
    exit (1);
  }
  if (( *order !=0 )&&( *order != 1 )) {
    fprintf (stderr, "Error: DB_interpolate ()\n");
    fprintf (stderr, "Use order of interpolation 0(linear) or 1(third order Hermit), not %d\n",
	     *order);
    exit (1);
  }
  
#ifdef EBUG  
  printf ("vars[%d] = ",*vars_size);
  for (int i=0; i<*vars_size; ++i)
    printf ("%d ",vars[i]);
  printf ("\n");
#endif
  
  switch (*mode) {
  case 0:
    // slow direct interpolation (1-st order only)
    if ( *avm != 0 ) {
      fprintf (stderr, "direct interpolation Forest::interpolate\n%s",
	       "has been designed for functions (not iwlt index) stored in DB\n");
      exit (1);
    }
    
    printf ("STOP: in DB_interpolate\n");
    exit(1);
    //::f->interpolate ( x, f, *x_size, vars, *vars_size, amr->xx, *order );
    break;
  case 1:
    // preprocess, then interpolate, grid assumed to be uniform
    amr->interpolate ( u, du, *nwlt, *avm,
		       x, f, *x_size, vars, *vars_size, true, *order );
    break;
  case 2:
    // preprocess, then interpolate
    amr->interpolate ( u, du, *nwlt, *avm,
		       x, f, *x_size, vars, *vars_size, false, *order );
    break;
  case 3: {
    // preprovess .res -> .ren for the volume renderer
    char* name=new char[*vtl+1];
    strncpy ( name, vtn, *vtl );
    name[*vtl]='\0';
    amr->interpolate ( u, du, *nwlt, *avm,
		       x, f, *x_size, vars, *vars_size, false, *order, name, *vtr-1 );
    delete [] name;
    break;
  }
  default:
    fprintf (stderr, "Error: DB_interpolate ()\n");
    fprintf (stderr, "use mode 0, 1, or 2, not %d\n",*mode);
    exit (1);
  }
}

/*----------------------------------------------------------------------
  Delete nodes marked as if from other processors from link lists and db
  ----------------------------------------------------------------------*/
void DB_CLEAN_OTHER_PROC_NODES () {
  f->delete_the_forest (f->get_proc());
}
#define DB_CLEAN_ALL_PROC_NODES db_clean_all_proc_nodes_
extern "C" void DB_CLEAN_ALL_PROC_NODES () {
  f->delete_the_forest ();
}
/*----------------------------------------------------------------------
  Clean the nodes marked by ID==to_be_deleted from link lists and db
  ----------------------------------------------------------------------*/
void DB_UPDATE_DB ()
{
  f->clean_the_forest ();
}



/*----------------------------------------------------------------------
  Write Nwlt_per_Tree array for the current processor (to be used in load balancing)
  Nwlt_per_Tree     - number of significant+adjacent nodes per tree
  number_of_trees   - number of trees
  err               - zero if no errors
  ----------------------------------------------------------------------*/
void DB_SET_NWLT_PER_TREE (int *Nwlt_per_Tree, const int* const number_of_trees, int* err)
{
  *err = f->set_nwlt_per_tree (Nwlt_per_Tree, *number_of_trees);
}

/*----------------------------------------------------------------------
  Update tree-proc info
  ----------------------------------------------------------------------*/
void DB_UPDATE_PROC_INFO (const int*const procs)
{
  f->update_proc_info (procs);
}


/*----------------------------------------------------------------------
  Redistribute the nodes in type-level lists to make sure that all "significant"
  or "adjacent" are in one list (sig) and all "ghost" are in another (gho).
  The nodes which are none of the above will be deleted from the lists (not roots - in old version)
  ----------------------------------------------------------------------*/
void DB_UPDATE_TYPE_LEVEL ()
{
  short int vindex,                                         // 1D index in tlp array
    vindexmax = g->TYPES_NUM*g->J_MAX*f->f_sig.get_pow3()* g->par_size;
  
  for ( vindex=0; vindex < vindexmax; vindex ++) { // ---- cycle tlp vector ----
    // first elements in the lists before any amendments
    Node *current_in_sig_list = f->f_sig.tlp_begin [vindex];
    Node *current_in_gho_list = f->f_gho.tlp_begin [vindex];
    Node *next_current;
    
    // (1) sweep through all the nodes in sig_adj list
    while (0 != current_in_sig_list) {
      next_current = current_in_sig_list->next_tlp;
      
      if ((current_in_sig_list->id & significant_id)
	  || (current_in_sig_list->id & adjacent_id)) {
	// do nothing
      } else {
	if (current_in_sig_list->id & ghost_id) {
	  // put the node into gho list (front)
	  f->f_sig.tlp_node_exclude (current_in_sig_list);
	  f->f_gho.tlp_node_include_f (current_in_sig_list);
	} else {
	  // exclude it from all the lists
	  // put it into del list
	  //if (current_in_sig_list->level != 0)
	  f->f_sig.tlp_node_exclude (current_in_sig_list);
	  current_in_sig_list->zero_value_vector (g->VAL_NUM, g->VAL_NUM_I);
	}
      }
      current_in_sig_list = next_current;
    }
    
    
    // (2) sweep through the nodes of gho list
    while (0 != current_in_gho_list) {
      next_current = current_in_gho_list->next_tlp;
      
      if ((current_in_gho_list->id & significant_id)
	  || (current_in_gho_list->id & adjacent_id)) {
	// put it into sig_adj list (front)
	f->f_gho.tlp_node_exclude (current_in_gho_list);
	f->f_sig.tlp_node_include_f (current_in_gho_list);
      } else {
	if (current_in_gho_list->id & ghost_id) {
	  // do nothing
	} else {
	  // exclude it from all the lists
	  // put it into del list
	  //if (current_in_gho_list->level != 0)
	  f->f_gho.tlp_node_exclude (current_in_gho_list);
	  current_in_gho_list->zero_value_vector (g->VAL_NUM, g->VAL_NUM_I);
	}
      }
      current_in_gho_list = next_current;
    }
  } // ---- cycle tlp vector ----
  
  
// #ifdef EBUG_SAFE
//   {
//     std::cout << "leaving  db_update_type_level : \n";
//     int num_sigvt=0, num_ghovt=0, num_delvt=0;    
//     for ( facetype=0; facetype < f->f_sig_adj.size(); facetype++)
//       for ( type=0; type < g->TYPES_NUM; type ++)
// 	for ( level=1; level <= g->J_MAX; level++) {
// 	  int tlp_index = type + (level-1) * g->TYPES_NUM;
// 	  num_sigvt += f->f_sig_adj[facetype].tlp_number_of_elements [tlp_index];
// 	  num_ghovt += f->f_gho[facetype].    tlp_number_of_elements [tlp_index];
// 	  //num_delvt += f->f_del[facetype].    tlp_number_of_elements [tlp_index];
// 	}
//     std::cout << "all the nodes : (sig,gho,del)=("
// 	 << num_sigvt << "," << num_ghovt << "," << num_delvt
// 	 << ")\n";
//     std::cout.flush();
//   }
// #endif
// #ifdef monitor
// //   access_file << "\n#\t db_update_type_level\n";
// #endif
}

/*----------------------------------------------------------------------
  Remove the node from its link-list
  ----------------------------------------------------------------------*/
void DB_REMOVE_FROM_LIST_BY_POINTER (P_SIZE* p_in) {
  Node* node = (Node*) *p_in;
  if (node->get_tlp()) {
    node->get_tlp()->tlp_node_exclude (node);
    node->zero_value_vector (g->VAL_NUM, g->VAL_NUM_I);
    node->id = intermediate_id;
  }
}

/*----------------------------------------------------------------------
  Move points with ZERO (or OLD) ID from sig list to gho
  (changed to remove nodes from the lists and the database completely)
  ----------------------------------------------------------------------*/
void DB_MOVE_ZERO_SIG_TO_GHO ()
{
  int vindexmax = g->TYPES_NUM*g->J_MAX*f->f_sig.get_pow3()* g->par_size;

  // int num = 0;
  for (int vindex=0; vindex < vindexmax; ++vindex) {  // ---- 1D index in tlp vector ----
    // first elements in the list before any amendments
    Node *current_in_sig_list = f->f_sig.tlp_begin [vindex];
    Node *next_current;
    
    while (0 != current_in_sig_list) {                         // sweep through all the nodes in sig_adj list
      next_current = current_in_sig_list->next_tlp;
      
      if (!(current_in_sig_list->id & (significant_id | adjacent_id))) {
	f->f_sig.tlp_node_exclude (current_in_sig_list);
	
	//Oleg 05.02.2011: For scalar code, n_update /= 0, thus ghost values are used in adapt grid
#ifdef MULTIPROC //NOTE: make sure that the conditonal compiler is active for parallel case
    //current_in_sig_list->zero_value_vector (g->VAL_NUM, g->VAL_NUM_I); // we should not do it since this point can become sig again
    current_in_sig_list->id = intermediate_id;
    //f->f_gho.tlp_node_include_b (current_in_sig_list); 
    //printtfPARALLEL();   // This erroneous line is intentionally added. By uncommenting it, you can make sure whether "#ifdef MULTIPROC" is working properly.
#else  
    f->f_gho.tlp_node_include_b (current_in_sig_list);
    //printfSERIAL();      // This erroneous line is intentionally added. By uncommenting it, you can make sure whether "#ifdef MULTIPROC" is working properly.
#endif
      }
      current_in_sig_list = next_current;
    }
  }                                                               // ---- 1D index in tlp vector ----
  //  printf ("DB_move_zero_sig_to_gho: moved %d nodes\n",num);
}

/*----------------------------------------------------------------------
  Access to the nodes of the given type and level

  Returns: num, ptr
  [which is the number of nodes in the list]
  [the pointer to the first node]
  ----------------------------------------------------------------------*/
void DB_GET_INITIAL_TYPE_LEVEL_NODE (const int*const proc, const int*const type, const int*const level, const int*const facetype,
				     int* num, P_SIZE* ptr, const int*const marker)
{
  Node* first_node =
    f->get_initial_type_level_node (*proc, *type, *level, *facetype, num, *marker);
  *ptr = (P_SIZE) first_node;

#ifdef monitor
  access (first_node);
#endif
}

/*----------------------------------------------------------------------
  Access to the nodes of the given type and level
  previous_element - previous node of that type and level
  
  Returns: next_element
  [which is the pointer to the next node from the list
  of the nodes of given type and level, or 0 for the last element]
  ----------------------------------------------------------------------*/
void DB_GET_NEXT_TYPE_LEVEL_NODE (const int*const proc, const int*const type, const int*const level, const int*const facetype,
				  const P_SIZE* previous_element, P_SIZE* next_element,
				  const int*const marker)
{
  Node *previous_node, *next_node;
  previous_node = (Node*) *previous_element;
  next_node = f->get_next_type_level_node (*proc, *type, *level, *facetype, previous_node, *marker);
  *next_element = (P_SIZE) next_node;


#ifdef monitor
  access (next_node);
#endif
}

/*----------------------------------------------------------------------
  Set values for the part of the vector of function values [1:VAL_NUM]
  p_in    - pointer to the node
  i_low   - starting index for the output vector
  i_high  - ending index
  f_in    - input vector of function values of size (i_high-i_low+!)
  ----------------------------------------------------------------------*/
void DB_SET_FUNCTION_BY_POINTER (const P_SIZE*const p_in,
				 const int*const i_low, const int*const i_high,
				 const Value*const f_in)
{
  Node* node = (Node*) *p_in;
  for (int i=*i_low-1; i<*i_high; i++) {
    ((Node*) *p_in)->set_element_of_value_vector(i,*(f_in + i - *i_low + 1));
    //printf ("%e ",*(f_in + i - *i_low + 1));
  }
  //printf ("\n");


#ifdef monitor
  access (node);
#endif
}
void DB_SET_IFUNCTION_BY_POINTER (const P_SIZE*const p_in,
				  const int*const i_low, const int*const i_high,
				  const int*const f_in)
{
  for (int i=*i_low-1; i<*i_high; i++)
    ((Node*) *p_in)->set_element_of_ivalue_vector (i,*(f_in + i - *i_low + 1));
  

#ifdef monitor
  access ((Node*) *p_in);
#endif
}

/*----------------------------------------------------------------------
  Get part of the vector of function values [1:VAL_NUM] for the node
  p_in    - pointer to the node
  i_low   - starting index for the output vector
  i_high  - ending index
  
  Returns: f_out
  [vector of function values of size (i_high-i_low+1) ]
  If p_in == 0, return 0 vector of function values (DEBUG_SAFE only)
  ----------------------------------------------------------------------*/
void DB_GET_FUNCTION_BY_POINTER (const P_SIZE*const p_in,
				 const int*const i_low, const int*const i_high,
				 Value* f_out)
{

// !AR 2/26/2011!  The following 6 lines are added by AR
//	printf ("DB_get_function_by_pointer()   P_SIZE= %d     sizeof(unsigned long long int)= %d     sizeof(unsigned long int)= %d     sizeof(log)= %d     sizeof(int)= %d \n", sizeof(P_SIZE), sizeof(unsigned long long int), sizeof(unsigned long int), sizeof(long), sizeof(int));
//#ifdef PTR_LONGLONG
//	printf ("DB_get_function_by_pointer()   PTR_LONGLONG   \n");
//#else                                       //
//	printf ("DB_get_function_by_pointer()   PTR_LONGLONG  NOT  Defined   \n");
//#endif

#ifdef EBUG_SAFE
  if (*p_in == 0) {                                                 // return 0 vector if input pointer is 0
    for (int pos=*i_low-1; pos < *i_high; pos++)
      *(f_out + pos - *i_low + 1) = (Value) 0.0;
    printf ("WARNING: DB_get_function_by_pointer()\n");
    printf ("DEBUG_SAFE forces zeroes into the function values for a zero pointer\n");
  } else {                                                          // return function values
#endif
    // ----- normal operational mode ------
    Value* vals = ((Node*) *p_in)->get_values();
    for (int pos=*i_low-1; pos < *i_high; pos++)
      *(f_out + pos - *i_low + 1) = vals [pos];
#ifdef EBUG_SAFE
  }
#endif


#ifdef monitor
  access ((Node*) *p_in);
#endif
}
void DB_GET_FUNCTION_BY_JMAX_COORDINATES (const int*const x,
					  const int*const i_low, const int*const i_high,
					  Value* f_out)
{
  f->get_function_by_jmax_coordinates (x, i_low, i_high, f_out);
}

void DB_GET_IFUNCTION_BY_POINTER (const P_SIZE*const p_in,
				  const int*const i_low, const int*const i_high,
				  int* f_out)
{
  if (*p_in == 0) {
    for (int pos=*i_low-1; pos < *i_high; pos++) {
      *(f_out + pos - *i_low + 1) = 0;
    }
    return;
  } else {
    Node* node = (Node*) *p_in;
    int* vals = node->get_ivalues();
    
    for (int pos=*i_low-1; pos < *i_high; pos++) {
      *(f_out + pos - *i_low + 1) = vals [pos];
    }
  }


#ifdef monitor
  access ((Node*) *p_in);
#endif
}

/*------------------------------------------------------------------------
  Set node ID to be equal to the given new ID. If the new ID corresponds
  to sig/adj list then move the node into sig/adj list.
  
  p_in  - stored in FORTRAN pointer
  id_in - new ID for the node
  ------------------------------------------------------------------------*/
void DB_SET_ID_BY_POINTER (const P_SIZE*const p_in, const int*const id_in)
{
  Node* node = (Node*) *p_in;
  int id = *id_in;

  //printf(" 1  id: %d \n", id);                                                                               //!AR 2/20/2011! added by AR

  if ((id & adjacent_id) || (id & significant_id)) {        // if ID is changed to sig/adj
    Type_level *node_tlp = node->get_tlp(),                 // ...
      *sig_tlp = &f->f_sig;                                 // ...
    
    if ( node_tlp != sig_tlp ) {                            // and if node was not in sig_adj list
      if (node_tlp == 0) {
	
	printf ("ERROR: in DB_set_id_by_pointer()\nplease contact the developers\n");
	exit(1);
	//f->set_type_level (node); // - UNCOMMENT THIS !!!!!! --------------------



      } else {
	node_tlp->tlp_node_exclude (node);                  // than move it into 
      }
      sig_tlp->tlp_node_include_b (node);                   // sig_adj link-list
    }
  }
  
  //printf(" 2  id: %d \n", id);                                                                               //!AR 2/20/2011! added by AR
  node->id  = id;
  

#ifdef monitor
  access (node);
#endif
}

/*--------------------------------------------------------------------------
  Get node ID
  p_in - stored in FORTRAN pointer
  
  Returns: id_out
  [node ID, integer]
  --------------------------------------------------------------------------*/
void DB_GET_ID_BY_POINTER (const P_SIZE*const p_in, int* id_out)
{
  *id_out = ((Node*) *p_in)->id;


#ifdef monitor
  access ((Node*) *p_in);
#endif
}


/*----------------------------------------------------------------------
  Get node ID
  x[dim] - coordinates at j_max level
  
  Returns: id_out
  [which is -1 if node does not exist]
  ----------------------------------------------------------------------*/
void DB_GET_ID_BY_JMAX_COORD (const int*const x, int* id_out)
{
  *id_out = -1;
  P_SIZE ptr = f->test_by_coordinates (x, g->J_MAX);
  if (ptr)
    *id_out = ((Node*) ptr)->id;
}
void DB_GET_ID_BY_JMAX_COORD_OR_ZERO (const int*const x, int* id_out)
{
  *id_out = 0;
  P_SIZE ptr = f->test_by_coordinates (x, g->J_MAX);
  if (ptr)
    *id_out = ((Node*) ptr)->id;
}
/*--------------------------------------------------------------------------
  Get vector of coordinates
  p_in   -  stored in FORTRAN pointer
  
  Returns: x
  [the vector of coordinates, in the units of the finest level J_max]
  --------------------------------------------------------------------------*/
void DB_GET_COORDINATES_BY_POINTER (const P_SIZE*const p_in, int *x)
{
  for (int i=0; i<g->DIM_NUM; ++i)
    *(x+i) = ((Node*) *p_in)->ii [i];
  
//   memcpy( x ,(((Node*) *p_in)->ii), sizeof(int)*g->DIM_NUM );
  
  
#ifdef monitor
  access ((Node*) *p_in);
#endif
}

/*----------------------------------------------------------------------
  Test the presence of a node
  x[dim] - coordinates, in the units corresponding to node's level
  level  - the level
  
  Returns: ptr
  [which is the pointer to the existing node, or 0]
  ----------------------------------------------------------------------*/
void DB_TEST_NODE (const int*const x, const int*const level,
		   P_SIZE* ptr)
{
  *ptr = f->test_by_coordinates (x, *level);

#ifdef monitor
  access (a);
#endif
}
// extern "C" void db_test_node_1 (const int *x, const int *level,
// 		P_SIZE* ptr)
// {
//   Node *a;
//   int out = f->test_by_coordinates_1 (x, *level, a);
//   *ptr = (P_SIZE) a;
// }

// /*----------------------------------------------------------------------
//   Delete the node
//   x[dim] - coordinates, in the units corresponding to node's level
//   level  - the level
//   Will not delete: 1) tree root, 2) node with children,
//   3) any node which ID != zero_id.
//   Returns: out
//   [which is the number of nodes deleted]
//   ----------------------------------------------------------------------*/
// void db_delete_node (const int *x, const int *level,
// 		  int* out)
// {
//   Node *a = 0;
//   *out = f->action_by_coordinates(x, *level, del_1_action, a, 0);
// }

/*----------------------------------------------------------------------
  Add the node
  x[dim] - coordinates, in the units corresponding to node's level
  level  - the level
  id_in  - ID for the node created

  Returns: out, ptr
  [which is the number of nodes created, including all new intermediate nodes]
  [integer pointer to the node created, or existing already, or 0 if error]
  ----------------------------------------------------------------------*/
void DB_ADD_NODE (const int*const x, const int*const level,
		  int* out, P_SIZE* ptr, const int*const id_in, const int*const front)
{
  Node *a;
  *out = f->add_by_coordinates (x, *level, a, *id_in, (bool) *front);
  *ptr = (P_SIZE) a;

#ifdef monitor
  access (a);
#endif
}


/*----------------------------------------------------------------------
  Return processor's rank the node belongs to.
  x[dim] - coordinates, in the units corresponding to node's level
  level  - the level
  
  Returns: proc
  ----------------------------------------------------------------------*/
void DB_GET_PROC_BY_COORDINATES (const int* const x, const int *const level, int* proc)
{
  *proc = f->get_proc_by_coordinates(x, *level);
}

/*----------------------------------------------------------------------
  Cleans the memory occupied by the database, completely
  ----------------------------------------------------------------------*/
void DB_FINALIZE ()
{
//   printf ("Warninig: DB_finalize has not been tested,\n");
//   printf ("          memory leaks might occur!\n");

  if( amr ) delete amr;           // OK
  amr = NULL;
  if( f ) delete f;             // OK, tree roots are deleted through stack
  f = NULL;
#ifdef xgraphics
  if( XG ) delete XG;            // OK
  XG = NULL;
#endif
  if( m ) delete m;             // OK
  m = NULL;
  if( g ) delete g;             // OK
  g = NULL;
}

/*----------------------------------------------------------------------
  Returns the value of the parameter VAL_NUM (number of values in DB) */
void DB_GET_N_VALUES (int* n_vals ) {
  *n_vals = g->VAL_NUM;
}

/*----------------------------------------------------------------------
  Initialization of the database.
  m[dim] - number of points at the coarsest level, which is level 1
  p[dim] - periodicity marker (1 - periodic)
  j_tree - tree roots are the nodes of that level
  j_max  - maximum level of the mesh
  dim    - space dimension of the mesh (1,2,3,etc)
  f_num  - the size of real function vector
  if_num - the size of integer function vector
  p_size - total number of processors
  p_rank - rank of the current processor
  proc   - which processor each tree belongs to
  ----------------------------------------------------------------------*/
void DB_DECLARE_GLOBALS (const int* m, const int* p,
			 const int* j_tree, const int* j_max,
			 const int* dim,
			 const int* f_num, const int* if_num,
			 const int* p_size, const int* p_rank, const int*const procs)
{
  g = new Global_variables (m, p, *j_tree, *j_max, *f_num, *if_num, *dim, *p_size, *p_rank);
  ::m = new Memory_management( *dim, *f_num, *if_num,   // system_MM
			       *j_tree, *j_max );       // childclustering_MM
  
  
// #ifdef xgraphics
//   {
//     int rad = 8;
    
//     XG = new (Xgraphics) (g->FINEST_MESH[0]*rad,  // x-resolution
// 			  g->FINEST_MESH[1]*rad,  // y-resolution
// 			  true,                        // filled
// 			  rad,                         // radius
// 			  g->J_MAX);                   // number of colors
    
//     XG->xprint_mesh (g->FINEST_MESH[0],
// 		     g->FINEST_MESH[1],
// 		     g->J_MAX,
// 		     g->J_TREE);                   // print mesh at root level
//     XG->xprint_scale (g->J_MAX);                   // print colored scale
//   }
// #endif
  
  int *x = new int [*dim];                         // find grid dimensions
  for (int i=0; i<*dim; i++)                       // at the level of
    x[i] = m[i] << (*j_tree-1);                    // the tree roots

  f = new Forest(x,g,::m,procs);
  delete [] x;
  
  amr = new AMR (f,g);
  
  // #ifdef monitor
  //   access_file.open ("access.dat");
  //   if (!access_file) {
  //     std::cout << "Cannot open access.dat\n";
  //     exit(1);
  //   }
  // #endif
#ifdef EBUG
  printf ("done");
#endif
}

/*----------------------------------------------------------------------
  Reshuffle functions
  ----------------------------------------------------------------------*/
extern "C" void BD_RESHUFFLE_1 ()
{
  f->reshuffle();
}
