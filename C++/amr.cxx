/*----------------------------------------------------------------*/
/*     Tree Structured Database                                   */
/*                                                                */
/*     Developed by Alexei Vezolainen                             */
/*                                                                */
/*     Please respect all the time and work                       */
/*     that went into the development of the code.                */
/*                                                                */
/*----------------------------------------------------------------*/
#include <stdlib.h>
#include <stdio.h>
#include <algorithm>
#include <string>
#include "amr.h"
#include <sys/stat.h>    //for mkdir in ensight format
#include <string.h>      //for mkdir in ensight format
#include <iostream>      //for str to int

#ifdef USE_TLL_TREE
#include "tll_tree.h"    // for AMR::interpolate ()
#endif

#ifdef CGNS_LIB
#include "cgnslib.h"     // for res2vis CGNS output
#endif

/* internal debugging flag:
   define for additional output */
//#define EBUG_AMR

/* create_box_tree() internal flag:
   define for inlined interpolation,
   undefine for stand-alone visualization */
//#define PERIODIC_AMR


/*
  Data will be written in VTK binary or ASCI
  unstructured mesh format (*.vtk) to be
  used with ParaView, VisIt or similar software.
*/
//-----------------------------------------------------------------------------------
// Set initial state variables
AMR::AMR (Forest *f_, const Global_variables *g_ ) :
  f(f_), g(g_), string_header("#"), size(0), cells(0), COORD_INTEGER(0), IS_TETRAHEDRA(0),
  GRID_IS_UNIFORM(1), ielem(0), val_num(g->VAL_NUM), val_num_i(g->VAL_NUM_I)
{
  switch (g->DIM_NUM) {
  case 1:
  case 2:
  case 3:
    {
      // allocate storage for data names
      data_names.resize (val_num + val_num_i);
      int i;
      for ( i=0; i<val_num + val_num_i; ++i)
	data_names[i] = "";
      
      // check SN_MAX
      int sn = 1;
      for ( i=0; i<SN_MAX; ++i, sn *= 10 );
      if (val_num + val_num_i >= sn) {
	printf ("AMR::SN_MAX is too small to hold the number of variables, change in amr.h:45 and recompile\n");
	exit(1);
      }
      // initialize IS_BIG_ENDIAN
      short int word = 0x0001;
      char *byte = (char *) &word;
      IS_BIG_ENDIAN = ! byte[0];

      // check DIM_MAX
      if (g->DIM_NUM > DIM_MAX) {
	printf ("Parameter DIM_MAX=%d seems to be smaller than the dimension of the problem %d\n",DIM_MAX,g->DIM_NUM);
	exit(1);
      }
      // allocate coordinates
      // xx.resize(g->DIM_NUM);
      for ( i=0; i<g->DIM_NUM; ++i) xx[i] = NULL;
      break;
    }
  default:
    return;
  }
}

//-----------------------------------------------------------------------------------
AMR::~AMR () {
  for (int i=0; i<g->DIM_NUM; ++i) {
    if( xx[i] ) {
      delete [] xx[i];
      xx[i] = NULL;
    }
  }

  if (ielem) delete [] ielem;
  ielem = NULL;
}

//-----------------------------------------------------------------------------------
// set maximum number of variables to print into vtk/cgns/plot3d/xmgr file
void AMR::set_number_of_vars_to_output (int ireal, int iint) {
  val_num   = MIN (g->VAL_NUM, ireal);
  val_num_i = MIN (g->VAL_NUM_I, iint);
}

//-----------------------------------------------------------------------------------
// Set coordinate vector xx[a1,a2]
// which corresponds to xx(0:MAXVAL(nxyz),1:dim)
// for a fixed dimension dim <-> xx(0:len,1:dim)
// (assumed that FINEST_MESH==nxyz)
void AMR::set_coord_vector (const int len_, const int dim_, const Value* xx_) {
  
  if ((dim_<1)||(dim_>g->DIM_NUM)) {
    fprintf (stderr, "AMR::set_coord_vector () called for dimension %d of %d\n",
	     dim_,g->DIM_NUM);
    exit (1);
  }

  if (len_ != g->FINEST_MESH[dim_-1]) {
    fprintf (stderr, "len=%d, FINEST_MESH[%d]=%d\n",len_,dim_-1,g->FINEST_MESH[dim_-1]);
    fprintf (stderr, "This is a bug. Try integer coordinates -i instead.\n");
    exit(1);
  }
  
  if (xx[dim_-1]) delete [] xx[dim_-1];
  
  xx[dim_-1] = new float [len_+1];
  for (int i=0; i<len_+1; ++i)
    xx[dim_-1][i] = (float) xx_[i];
}

//-----------------------------------------------------------------------------------
// Set coordinate vector xx_tree for a quick search
// void AMR::arrange_xx_tree () {
//   fprintf (stderr, "AMR::arrange_xx_tree () is not ready");
//   exit (2);
//   xx_tree.resize (g->DIM_NUM);
//   for (int dim=0; dim<g->DIM_NUM; ++dim) {
//     printf ("xx[%d] = ",g->FINEST_MESH[dim]);
//     for (int i=0; i<g->FINEST_MESH[dim]; ++i)
//       printf ();
//     printf ("\n");
//   }
// }

//-----------------------------------------------------------------------------------
// transform integer coordinates into required format,
// a replacement for node->ii[i] statement
inline float AMR::get_coord (const int* const ii, const int& i) {
  if (COORD_INTEGER)
    return (float) ii[i];
  else
    return xx[i][ii[i]];
}
inline float AMR::get_fcoord (int*ii, int i) {
    return xx[i][ii[i]];
}

//-----------------------------------------------------------------------------------
// Write data in CGNS format
// using CGNS library
void AMR::write_amr_cgns (const char* c) {

#ifndef CGNS_LIB
  printf ("The code has not been compiled to support CGNS format.\n");
  printf (" Please update machine specific makefile to provide\n");
  printf (" the correct path to CGNS library and recompile the code.\n");
  exit (1);
#else // CGNS_LIB
  
  switch (g->DIM_NUM) {
  case 1:
    write_amr_1d_cgns ( c );
    return;
  case 2:
  case 3: 
    break;
  default:
    printf ("CGNS format is not supported for dimensionality %d\n",g->DIM_NUM);
    return;
  }
  
  std::string cgns_name = c;       // generic file name
  cgns_name += ".cgns";
  int index_file,                  // CGNS file index
    index_base,                    // CGNS base index
    index_zone,                    // CGNS zone index
    index_coord,                   // CGNS coordinate index
    index_coord_y,                 // CGNS coordinate index
    index_section,                 // CGNS section index
    index_solution,                // CGNS solution index
    cell_dim,                      // Dimension of the cells; 3 for volume cells, 2 for surface cells.
    phys_dim;                      // Number of coordinates required to define a vector in the field
  char basename[] = "Base",
    zonename[] = "Zone",
    c_x[] = "CoordinateX",
    c_y[] = "CoordinateY",
    c_z[] = "CoordinateZ";
  
  printf ("Opening file %s ... ",cgns_name.c_str());
  fflush (stdout);
  if (cg_open( cgns_name.c_str(), MODE_WRITE, &index_file ))
    cg_error_exit();
  
  cell_dim = phys_dim = g->DIM_NUM;
  if (cg_base_write( index_file, basename, cell_dim, phys_dim, &index_base ))
    cg_error_exit();

  // count the number of points
  set_points_plot3d (0);
#ifdef EBUG_AMR
  printf ("%d ",size);
#endif
  printf ("points ... ");
  fflush (stdout);

  float *arrayf = (float*) calloc ( size*3, sizeof(float) );
  float *variables = (float*) malloc ( sizeof(float)*size*(val_num+val_num_i) );
  if ( (arrayf == 0) || (variables == 0) ) {
    fprintf(stderr,  "Error: not enougth memory for CGNS writing\n");
    exit (1);
  }
  // write coordinates and function values into temporary arrays
  set_points_plot3d (1,arrayf,variables);
  
  
  // compute and write connectivity information
  set_cells (0,0,2);                      // CGNS format
  
  int *isize = (int*) calloc ( 3*g->DIM_NUM, sizeof(int) );
  int i;
  for ( i=0; i<g->DIM_NUM; ++i) {
    isize[3*i+0] = size;
    isize[3*i+1] = cells;
    isize[3*i+2] = 0;
  }
#ifdef EBUG_AMR
  printf ("%d ",cells);
#endif
  printf ("cells ... ");
  fflush (stdout);
  

  if (cg_zone_write( index_file, index_base, zonename, isize, Unstructured, &index_zone ))
    cg_error_exit();
  if (cg_coord_write( index_file, index_base, index_zone, RealSingle, c_x, &arrayf[0], &index_coord ))
    cg_error_exit();
  if (cg_coord_write( index_file, index_base, index_zone, RealSingle, c_y, &arrayf[size], &index_coord ))
    cg_error_exit();
  if (g->DIM_NUM == 3)
    if (cg_coord_write( index_file, index_base, index_zone, RealSingle, c_z, &arrayf[size+size], &index_coord ))
      cg_error_exit();
  
  ElementType_t element_type;
  if (g->DIM_NUM == 3)
    if (IS_TETRAHEDRA)
      element_type = TETRA_4;
    else
      element_type = HEXA_8;
  else
    if (IS_TETRAHEDRA)
      element_type = TRI_3;
    else
      element_type = QUAD_4; 

  if (cg_section_write( index_file, index_base, index_zone, "Cell", element_type, 1, cells, 0, ielem, &index_section ))
    cg_error_exit();
  
  if (cg_sol_write( index_file, index_base, index_zone, "Solution", Vertex, &index_solution ))
    cg_error_exit();

  int index_field;
  for ( i=0; i<val_num+val_num_i; ++i)
    if (cg_field_write( index_file, index_base, index_zone, index_solution, RealSingle, data_names[i].c_str(), &variables[size*i], &index_field ))
      cg_error_exit();

  
  if (cg_close( index_file ))
    cg_error_exit();
  
  // deallocate lists
  final_box_list.clear ();
  box_list.clear ();
  next_box_list.clear ();
  
  printf ("done\n");
  fflush (stdout);
  // Set internal AMR state ready for another writing
  size = 0;
  cells = 0;
  free (isize);
  free (arrayf);
  free (variables);
#endif // CGNS_LIB
}


//-----------------------------------------------------------------------------------
// Write data in PLOT3D ASCI or binary file (2 or 3D) or XMGR (1D)
// Set internal AMR state ready for another writing
void AMR::write_amr_plot3d (const char* c, const char* ft) {
  
  switch (g->DIM_NUM) {
  case 1:
    printf ("PLOT3D format is not supported for 1D (Use General Text Loader to read XMGR file in Tecplot, or use CGNS)\n");
    write_amr_xmgr ( c );
    return;
  case 2:
  case 3: 
    break;
  default:
    printf ("PLOT3D format is not supported for dimensionality %d\n",g->DIM_NUM);
    return;
  }

  // make file names
  std::string xyz_file = c;
  std::string q_file = c;
  xyz_file += ".xyz";
  q_file += ".q";
  
  const bool
    is_binary = ( ft[0] == 'b' );
  
  FILE *f2, *f3;
  if ((f2 = fopen (xyz_file.c_str(),"w")) == 0) {
    fprintf(stderr,  "Error: opening file: %s\n", xyz_file.c_str());
    exit(1);
  }
  if ((f3 = fopen (q_file.c_str(),"w")) == 0) {
    fprintf(stderr,  "Error: opening file: %s\n", q_file.c_str());
    exit(1);
  }
 
  if ( g->par_rank == 0 ) { 
    printf ("Opening ");
    if ( is_binary ) printf ("binary ");
    else             printf ("ASCI ");
    printf ("files %s.[xyz,q] ... ",c);
    fflush (stdout);
  }
  
  // count the number of points
  set_points_plot3d (0);
#ifdef EBUG_AMR
  printf ("%d ",size);
#endif
  printf ("points & data ... ");
  fflush (stdout);
  
  // allocate all the required variables
  float *arrayf = (float*) calloc ( size*3, sizeof(float) );
  float *variables = (float*) malloc ( sizeof(float)*size*(val_num+val_num_i) );
  if ( (arrayf == 0) || (variables == 0) ) {
    fprintf(stderr,  "Warning: not enougth memory for PLOT3D writing, trying VTK ...\n");
    fclose (f2);
    fclose (f3);
    write_amr ( c,ft );
    return;
  }
    
  // write data into temporary arrays
  set_points_plot3d (1,arrayf,variables);

  // write grid and data files
  int tri_num = 0, tetra_num = 0;
  int h[3] = { size, tri_num, tetra_num };
  int q[4] = { size, 1, 1, val_num+val_num_i };
  if ( is_binary ) {
    fwrite ( h, sizeof(int), 3, f2 );
    fwrite ( q, sizeof(int), 4, f3 );
    fwrite ( arrayf, sizeof(float), size*3, f2 );
    fwrite ( variables, sizeof(float), size*(val_num+val_num_i), f3 );
  } else {
    fprintf ( f2, "%d %d %d                                                   \n",
	      h[0],h[1],h[2] );
    fprintf ( f3, "%d %d %d %d\n", q[0],q[1],q[2],q[3] );
    int i;
    if (COORD_INTEGER)
      for ( i=0; i<size*3; ++i )
	fprintf (f2, "%d ", (int) arrayf[i]);
    else
      for ( i=0; i<size*3; ++i )
	fprintf (f2, "%f ",  arrayf[i]);
    fprintf (f2, "\n");
    for ( i=0; i<size*(val_num+val_num_i); ++i )
      fprintf ( f3, "%f ", variables[i]);
    fprintf ( f3, "\n" );
  }
  fclose (f3);
  free (arrayf);
  free (variables);
  
  

  // compute and write connectivity information
  set_cells (f2, is_binary, 1);                     // in PLOT3D format
#ifdef EBUG_AMR
  printf ("%d ",cells);
#endif
  printf ("cells ... ");
  fflush (stdout);

  // set grid file header
  int hf[3];
  hf[0] = size;
  if (g->DIM_NUM == 3) {
    hf[1] = 0;
    if (IS_TETRAHEDRA)
      hf[2] = cells;      // 3D, cells == number_of_tetrahedra
    else
      hf[2] = cells*6;    // 3D, cells == number_of_boxes
  } else {
    hf[2] = 0;
    if (IS_TETRAHEDRA)
      hf[1] = cells;      // 2D, cells == number_of_triangles
    else
      hf[1] = cells*2;    // 2D, cells == number_of_boxes
  }
  
  // write Randy's magic numbers
  int rmn = RANDYSNUMBER;
  if ( is_binary ) {
    for ( int i=hf[g->DIM_NUM-1]; i>0; --i )
      fwrite ( &rmn, sizeof(int), 1, f2 );
  } else {
    for ( int i=hf[g->DIM_NUM-1]; i>0; --i )
      fprintf ( f2,"%d ", rmn);
    fprintf ( f2,"\n");
  }
  
  // correct the header of the grid file
  rewind (f2);
  if ( is_binary )
    fwrite ( hf, sizeof(int), 3, f2 );
  else
    fprintf ( f2, "%d %d %d", hf[0],hf[1],hf[2] );
  fclose (f2);
  
  // deallocate lists
  final_box_list.clear ();
  box_list.clear ();
  next_box_list.clear ();
  
  printf ("done\n");
  fflush (stdout);
  // Set internal AMR state ready for another writing
  size = 0;
  cells = 0;
}

//-----------------------------------------------------------------------------------
// Write 1D data into XMGR ASCI file
// Set internal AMR state ready for another writing
void AMR::write_amr_xmgr (const char* c) {
  
  if (g->DIM_NUM != 1) {
    printf ("XMGR here ASCI format is not supported for dimensionality %d\n",g->DIM_NUM);
    return;
  }
  
  
  std::string filename = c;
  filename += ".dat";                         // 1D extension for XMGR's ASCI format
  
  FILE *f;
  if ((f = fopen (filename.c_str(),"w")) == 0) {
    fprintf(stderr,  "Error: opening file: %s\n", filename.c_str());
    exit(1);
  }
  printf ("Opening XMGR data file %s ... ",filename.c_str());
  fprintf (f,  "#  ASCI file to be open with XMGR or similar software.\n");
  fprintf (f,  "#  Sorted by 1D coordinate data vector:\n");
  fprintf (f,  "#  %s\n",string_header.c_str());
  
  printf ("allocating ... ");
  fill_record_vector ();
  printf ("sorting ... ");
  std::sort (rec.begin(), rec.end());
  printf ("writing ... ");
  
  fprintf (f,  "#Coordinate\t");                           // write data names
  int i;
  for ( i=0; i<val_num; ++i)
    fprintf (f,  "%s\t",data_names[i].c_str());
  for ( i=0; i<val_num_i; ++i)
    fprintf (f,  "%s\t",data_names[i+val_num].c_str());
  fprintf (f,  "\n");
  
  for (itr = rec.begin(); itr != rec.end(); ++itr) {
    if (COORD_INTEGER)
      fprintf (f,  "%d\t", itr->node->ii[0]);             // integer coordinate
    else
      fprintf (f,  "%12.12e\t",get_coord ( itr->node->ii,0 )); // real coordinate
    int i;
    for ( i=0; i<val_num; ++i)                        // write real function
      fprintf (f,  "%12.12e\t",itr->node->val[i]);
    for ( i=0; i<val_num_i; ++i)                      // write integer function
      fprintf (f,  "%d\t",itr->node->ival[i]);
    fprintf (f,  "\n");
  }
  fclose (f);
  printf ("deallocating ... ");
  rec.clear ();
  printf ("done\n");
}


//-----------------------------------------------------------------------------------
// Write data in VTK ASCI or binary file (1, 2, or 3D)
// Set internal AMR state ready for another writing
void AMR::write_amr (const char* c, const char* file_type) {

  switch (g->DIM_NUM) {
  case 1:
    write_amr_1d_vtk ( c, file_type );
    return;
  case 2:
  case 3:
    break;
  default:
    // nothing to do
    printf ("VTK format is not supported for dimensionality %d\n",g->DIM_NUM);
    return;
  }



  std::string filename ( c );
  filename += ".vtk";                         // VTK extension
  const bool
    is_binary = ( file_type[0] == 'b' );
  
  FILE *f;
  if ((f = fopen (filename.c_str(),"w")) == 0) {
    fprintf(stderr,  "Error: opening file: %s\n", filename.c_str());
    exit(1);
  }
  if ( g->par_rank == 0 ) { 
    printf ("Opening ");
    if ( is_binary ) printf ("binary ");
    else             printf ("ASCI ");
    printf ("file %s ... ",filename.c_str());
#ifdef MULTIPROC
    printf(" (%d processes). Proc0: ", g->par_size );
#endif
    fflush (stdout);
  }
  
  fprintf (f, "# vtk DataFile Version 3.0\n");            // write VTK header
  fprintf (f,  "# %s\n",string_header.c_str());           // ...
  if ( is_binary )
    fprintf (f,  "BINARY\n");                             // ...
  else
    fprintf (f, "ASCII\n");                               // ...
  
  fprintf (f, "DATASET UNSTRUCTURED_GRID\n");             // ...
  fpos_t points_pos, end_pos, cells_pos;
  fgetpos (f,&points_pos);
  
  if (COORD_INTEGER)
    fprintf (f,  "POINTS %10d int\n", size);              // Assume integer coordinates
  else
    fprintf (f,  "POINTS %10d float\n", size);            // Assume real coordinates
  set_points (f, is_binary);                              // write VTK points, set size
#ifdef EBUG_AMR
  if ( g->par_rank == 0 ) printf ("%d ",size);
#endif
  if ( g->par_rank == 0 ) printf ("points ... ");
  fflush (stdout);
  fgetpos (f,&end_pos);
  fsetpos (f,&points_pos);
  if (COORD_INTEGER)
    fprintf (f,  "POINTS %10d int\n", size);              // print actual size
  else
    fprintf (f,  "POINTS %10d float\n", size);
  fsetpos (f,&end_pos);                                   // continue
  fgetpos (f,&cells_pos);
  
  int MULT;
  if (g->DIM_NUM == 2)
    if (IS_TETRAHEDRA)
      MULT = 4;             // VTK_TRIANGLE
    else
      MULT = 5;             // VTK_PIXEL
  else
    if (IS_TETRAHEDRA)
      MULT = 5;             // VTK_TETRA
    else
      MULT = 9;             // VTK_VOXEL
  
  fprintf (f,  "\nCELLS %10d %10d\n", cells, MULT*cells);
printf("about to set cells on proc %d/n", g->par_rank);
  set_cells (f, is_binary);                               // write VTK cells, count them
printf("FINISH set cells on proc %d", g->par_rank);
  fgetpos (f,&end_pos);
  fsetpos (f,&cells_pos);
  fprintf (f,  "\nCELLS %10d %10d\n", cells, MULT*cells);    // print actual number of cells
  fsetpos (f,&end_pos);                                      // continue
#ifdef EBUG_AMR
  if ( g->par_rank == 0 ) printf ("%d ",cells);
#endif
  if ( g->par_rank == 0 ) printf ("cells ... ");
  fflush (stdout);

  if ( g->par_rank == 0 ) printf ("data ... ");
  fflush (stdout);
  set_point_data (f, is_binary);
  
  fclose (f);
  // deallocate lists
  final_box_list.clear ();
  box_list.clear ();
  next_box_list.clear ();

  printf ("Proc %d/%d wrote %d cells\n", g->par_rank, g->par_size, cells );
  
  fflush (stdout);
  // Set internal AMR state ready for another writing
  size = 0;
  cells = 0;
}


//----------------------------------------------------------------
// Write data in ENSIGHT GOLD ASCI or binary file ( 2 or 3D)
// Set internal AMR state ready for another writing
void AMR::write_amr_ensght (const char* c, const char* file_type) {

  switch (g->DIM_NUM) {
  case 1:
    printf ("ENSIGHT GOLD format is not supported for dimensionality %d\n",g->DIM_NUM);
    return;
  case 2:
  case 3:
    break;
  default:
    // nothing to do
    printf ("ENSIGHT GOLD format is not supported for dimensionality %d\n",g->DIM_NUM);
    return;
  }

  //Write order:
  //geometry
  //variables
  //case file

  //****Create directory for files associated with this case*****
  std::string filename = c;
  std::string station_number = filename.substr(filename.length()-4,4);
  int istation_number = atoi(station_number.c_str());
  static int istation_start = 0;  
  static int istation_count = 0;
  static int istation_step = 1;

    istation_count++;
    if (istation_count == 1) {  
      istation_start = istation_number;
    } else{
      istation_step = (istation_number-istation_start)/(istation_count-1);
    }

  int i_position;
  for (i_position = filename.length()-1; i_position >= 0; --i_position) {
    if ( filename[i_position]=='/' )
      break; 
  }
   
  std::string filename_root = filename.substr(i_position + 1,filename.length()-i_position-6);
  std::string results_directory = filename.substr(0,i_position + 1);
  //mkdir((results_directory + filename_root).c_str(),S_IRWXU);    

  const bool
    is_binary = ( file_type[0] == 'b' );

  printf ("Opening ");
  if ( is_binary ) printf ("binary ");
  else             printf ("ASCII ");
  printf ("file %s%s/*%s ... ",results_directory.c_str(),filename_root.c_str(),station_number.c_str());
  fflush (stdout);

  //*****Geometry File****
  const int ONE = 1;
  filename = results_directory + filename_root + "/" + filename_root + "." + station_number + ".geo";        // geometry extension
  
  FILE *f;
  if ((f = fopen (filename.c_str(),"w")) == 0) {
    fprintf(stderr,  "Error: opening file: %s\n", filename.c_str());
    exit(1);
  }

  
  if ( is_binary )
    fprintf (f,  "%-79s\n","C BINARY");                             // Denote binary file
      
  fprintf (f, "%-79s\n","Ensight Gold: DATASET UNSTRUCTURED GRID");               // Ensight header
  fprintf (f, "%-79s\n","GEOMETRY FILE: Beta");                                   // ...
  
  fprintf (f, "%-79s\n","node id off");                             // ...
  fprintf (f, "%-79s\n","element id off");                          // ...
  
  fprintf (f, "%-79s\n","part");                                    // Number of parts
  if ( is_binary )
    fwrite  (&ONE, sizeof(int), 1, f);
  else
    fprintf (f, "%10d\n", ONE);                                     // ...  full domain as a single part

  fprintf (f, "%-79s\n","Full domain");                             // ...  part description
  fprintf (f, "%-79s\n","coordinates");                             // Node coordinates

  fpos_t points_pos, end_pos, cells_pos;        
  fgetpos (f,&points_pos);
  if ( is_binary )                                          // Write placeholder for number of nodes
    fwrite  (&size, sizeof(int), 1, f);
  else
    fprintf (f, "%10d\n", size);     
                           
  for ( int idim = 0; idim < 3; ++idim ) {                  // write node points, each component seperately
    size = 0;
    set_points_ensght (f, is_binary, -1, idim);             
  }

#ifdef EBUG_AMR
  printf ("%d ",size);
#endif
  printf ("points ... ");
  fflush (stdout);
  fgetpos (f,&end_pos);
  fsetpos (f,&points_pos);

  if ( is_binary )                                          // Write total number of nodes
    fwrite  (&size, sizeof(int), 1, f);
  else
    fprintf (f, "%10d", size);      

  fsetpos (f,&end_pos);                                     // continue
  
  if (g->DIM_NUM == 2)
    if (IS_TETRAHEDRA){
      fprintf (f,  "%-79s\n","tri3");
    }
    else {
      fprintf (f,  "%-79s\n","quad4");
    }
  else
    if (IS_TETRAHEDRA) {
      fprintf (f,  "%-79s\n","tetra4");
    }
    else {
      fprintf (f,  "%-79s\n","hexa8");
      }
  fgetpos (f,&cells_pos);
  if ( is_binary )                                          // Write number of elements placeholder
    fwrite  (&size, sizeof(int), 1, f);
  else
    fprintf (f, "%10d\n", size);      

  set_cells (f, is_binary, 4);                              // write cell connectivities


#ifdef EBUG_AMR
  printf ("%d ",cells);
#endif
  printf ("cells ... ");
  fflush (stdout);
  fgetpos (f,&end_pos);
  fsetpos (f,&cells_pos);


  if ( is_binary )                                          // Write total number of elements
    fwrite  (&cells, sizeof(int), 1, f);
  else
    fprintf (f, "%10d", cells);      

  fsetpos (f,&end_pos);                                     // continue

  fclose (f);

  //*****************



  //****Variable Files****
  int i;
  for ( i=0; i<val_num+val_num_i; i++){                //write each variable in a different file
      filename = results_directory + filename_root + "/" + data_names[i] + "." + station_number + ".scl";                     // variable file to write 
      
      if ((f = fopen (filename.c_str(),"w")) == 0) {
	fprintf(stderr,  "Error: opening file: %s\n", filename.c_str());
	exit(1);
      }
      
      fprintf (f, "%-79s\n","# Ensight Gold DataFile for scalar variable");                // write variable file header

      fprintf (f,  "%-79s\n","part");                                                      // Write information for this part
      if ( is_binary)                                                                      // ...  full domain is single part 
	fwrite  (&ONE, sizeof(int), 1, f);
      else
	fprintf (f, "%10d\n",ONE);                               
      
      fprintf (f, "%-79s\n","coordinates");                             

      set_points_ensght (f, is_binary, i, 0);                                              //write variable data
      
      fclose (f);
    }
  printf ("data ... ");
  fflush (stdout);
  //*****************


  //*******Case******
  filename = results_directory + filename_root + "/" + filename_root + ".case";        // Casefile, overwrites everytime a station is written
  
  
  if ((f = fopen (filename.c_str(),"w")) == 0) {
    fprintf(stderr,  "Error: opening file: %s\n", filename.c_str());
    exit(1);
  }

  fprintf (f, "FORMAT\n");            
  fprintf (f, "type: ensight gold\n\n");            

  fprintf (f, "GEOMETRY\n");            
  fprintf (f, "model:\t%d\t%s.****.geo\n\n",1,filename_root.c_str());            

  fprintf (f, "VARIABLE\n");
  for (i=0; i<val_num+val_num_i; ++i)
    fprintf (f, "scalar per node:\t%d\t%s\t%s.****.scl\n", 1, data_names[i].c_str(), data_names[i].c_str());            
  //    fprintf (f, "scalar per node:\t%d\t%s****.%s.scl\n",1,filename_root.c_str(),data_names[i].c_str());            
          
  fprintf (f, "\nTIME\n");            
  fprintf (f, "time set:\t%d\n",1);            
  fprintf (f, "number of steps:\t%d\n",istation_count);            
  //fprintf (f, "filename start number:\t%4.4d\n",istation_start);            
  //fprintf (f, "filename increment:\t%d\n",istation_step);            
  //fprintf (f, "time values:");
  fprintf (f, "filename numbers file:\t%s.fn.dat\n",filename_root.c_str());            
  fprintf (f, "time values file:\t%s.tv.dat",filename_root.c_str());            
  fclose (f);
  fflush (stdout);

  filename = results_directory + filename_root + "/" + filename_root + ".fn.dat";        // Casefile, overwrites everytime a station is written
  if ((f = fopen (filename.c_str(),"w")) == 0) {
    fprintf(stderr,  "Error: opening file: %s\n", filename.c_str());
    exit(1);
  }

  int k;
  k=1;
  for (int i = istation_start; i <= istation_number; i = i+istation_step) 
  {
    if (k % 8 == 0)  
    {
    fprintf (f, "\n");
    }
    fprintf (f, "\t%4d", i);
    k++;
  }

  fclose (f);
  fflush (stdout);

  filename = results_directory + filename_root + "/" + filename_root + ".tv.dat";        // Casefile, overwrites everytime a station is written
  if ((f = fopen (filename.c_str(),"w")) == 0) {
    fprintf(stderr,  "Error: opening file: %s\n", filename.c_str());
    exit(1);
  }

  k=1;
  for (int i = istation_start; i <= istation_number; i = i+istation_step) 
  {
    if (k % 8 == 0)  
    {
    fprintf (f, "\n");
    }
    fprintf (f, "\t%5.1f", (float) i);
    k++;
  }

  fclose (f);

  printf ("casefile ... ");
  fflush (stdout);
  //*****************


  // deallocate lists
  final_box_list.clear ();
  box_list.clear ();
  next_box_list.clear ();

  printf ("done\n");
  fflush (stdout);
  // Set internal AMR state ready for another writing
  size = 0;
  cells = 0;
}



//-----------------------------------------------------------------------------------
// Write point data into VTK file (2 or 3D)
// All data are treated to be scalars
void AMR::set_point_data (FILE *file, const bool binary) {
  // called from 2-3D section only
  if ((g->DIM_NUM != 2) && (g->DIM_NUM != 3))
    return;

  fprintf (file,  "POINT_DATA %d", size);
  char sn[SN_MAX];
  int i;
  for ( i=0; i<val_num; ++i) {                // write real function
    std::string s = data_names[i];
    if ( s.size() == 0 ) {
      sprintf (sn,"%d",i+1);
      s = "data_" + std::string(sn);
    }
    fprintf (file,  "\nSCALARS %s float\n",s.c_str());
    fprintf (file,  "LOOKUP_TABLE default\n");
    set_points ( file, binary, i );
  }
  for ( i=0; i<val_num_i; ++i) {              // write integer function
    std::string s = data_names[i+val_num];
    if ( s.size() == 0 ) {
      sprintf (sn,"%d",i+1+val_num);
      s = "data_" + std::string(sn);
    }
    fprintf (file,  "\nSCALARS %s int\n",s.c_str());
    fprintf (file,  "LOOKUP_TABLE default\n");
    set_points ( file, binary, i+val_num );
  }
  fprintf (file,  "\n");
}


//-----------------------------------------------------------------------------------
// We may try to get data names from the comment,
//  or ask user to provide them
void AMR::set_point_data_names (const int nvar, char* name) {
  if (nvar <= data_names.size() )
    data_names[nvar-1] = name;
}


//-----------------------------------------------------------------------------------
// Float htonl() function
inline float AMR::FloatSwap( const float& f ) {
  union {
    float f;
    unsigned char b[4];
  } dat1, dat2;  
  dat1.f = f;
  dat2.b[0] = dat1.b[3];
  dat2.b[1] = dat1.b[2];
  dat2.b[2] = dat1.b[1];
  dat2.b[3] = dat1.b[0];
  return dat2.f;
}

#ifndef NETINET_PRESENT_AMR // netinet/in.h has not been included
//-----------------------------------------------------------------------------------
// Integer htonl() function
inline int AMR::htonl( int a ) {
  unsigned char b1, b2, b3, b4;
  b1 = a & 255;
  b2 = ( a >> 8  ) & 255;
  b3 = ( a >> 16 ) & 255;
  b4 = ( a >> 24 ) & 255;
  return ((int)b1 << 24) + ((int)b2 << 16) + ((int)b3 << 8) + b4;
}
#endif

//-----------------------------------------------------------------------------------
// Count all the cells and
// write them into VTK file
// in VTK (format==0) or Plot3D (format==1) file format, or
// allocate CGNS array (format==2)
// call from AMR::interpolate (format==3)
// Ensight Gold format (format==4)
void AMR::set_cells (FILE *file, const bool binary, const int format) {

  if ((g->DIM_NUM != 2) && (g->DIM_NUM != 3))           // called from 2-3D section only
    return;
  
  final_box_list.clear ();
  box_list.clear ();
  next_box_list.clear ();
  if (format == 3)
    create_box_tree(1);                                  // compute tll_info
  else
    create_box_tree();                                   // create boxes, set AMR::cells
  box_list.clear ();
  next_box_list.clear ();

#ifdef EBUG_AMR
  int a=0;
  for ( it = final_box_list.begin (); it != final_box_list.end (); ++it, ++a ){
    printf ("box: [%d %d] - [%d %d]",it->p[0]->ii[0],it->p[0]->ii[1],it->p[3]->ii[0],it->p[3]->ii[1]);
  }
  printf ("AMR::set_cells: a=%d\n",a); //exit(0);
#endif  

  switch (format) {
  case 0:                      // VTK format
    
    // write the boxes
    if (binary)
      for ( it = final_box_list.begin (); it != final_box_list.end (); ++it ) {
	
#ifndef P_                                                   //
#define P_(a) ( htonl( (it->p[(a)]->vid) ) )                 // Some typing acceleration
#else                                                        //
#error Please rename already defined local macros P_         //
#endif                                                       //

	int point_index [30] =
	  { htonl(4),
	    P_(0), P_(1), P_(2), P_(3),
	    0,0,0,0,0, 0,0,0,0,0, 0,0,0,0,0, 0,0,0,0,0, 0,0,0,0,0};

	
	if (g->DIM_NUM == 3) {
	  if (IS_TETRAHEDRA) {                              // six VTK_TETRA cells
	    point_index [4] = P_(5);                        // for each 3D box
	    point_index [5] = htonl(4);
	    point_index [6] = P_(1);
	    point_index [7] = P_(3);
	    point_index [8] = P_(2);
	    point_index [9] = P_(5);
	    point_index [10] = htonl(4);
	    point_index [11] = P_(0);
	    point_index [12] = P_(2);
	    point_index [13] = P_(4);
	    point_index [14] = P_(5);
	    point_index [15] = htonl(4); 
	    point_index [16] = P_(2);
	    point_index [17] = P_(6);
	    point_index [18] = P_(4);
	    point_index [19] = P_(5);
	    point_index [20] = htonl(4);
	    point_index [21] = P_(3);
	    point_index [22] = P_(7);
	    point_index [23] = P_(2);
	    point_index [24] = P_(5);
	    point_index [25] = htonl(4); 
	    point_index [26] = P_(7);
	    point_index [27] = P_(6);
	    point_index [28] = P_(2);
	    point_index [29] = P_(5);
	    fwrite (point_index, sizeof(int), 30, file);
	  } else {
	    point_index [0] = htonl(8);                  // one VTK_VOXEL cell
	    point_index [5] = P_(4);                     // for each 3D box
	    point_index [6] = P_(5);
	    point_index [7] = P_(6);
	    point_index [8] = P_(7);
	    fwrite (point_index, sizeof(int), 9, file);
	  }
	} else {
	  if (IS_TETRAHEDRA) {                             // two VTK_TRIANGLE cell
	    point_index [0] = htonl(3);                    // for each 2D box
	    point_index [4] = htonl(3);
	    point_index [5] = P_(2);
	    point_index [6] = P_(3);
	    point_index [7] = P_(1);
	    fwrite (point_index, sizeof(int), 8, file);
	  } else {                                         // one VTK_PIXEL cell
	    fwrite (point_index, sizeof(int), 5, file);    // for each 2D box
	  }
	}
#undef P_                                                    // Some typing acceleration
	
      }
    else
      for ( it = final_box_list.begin (); it != final_box_list.end (); ++it ) {
	
#ifndef P_                                                   //
#define P_(a) (it->p[(a)]->vid)                              // Some typing acceleration
#else                                                        //
#error Please rename already defined local macros P_         //
#endif                                                       //
	
	if (g->DIM_NUM == 2)
	  if (IS_TETRAHEDRA) {
	    fprintf (file,  "3 %d %d %d 3 %d %d %d ", P_(0),P_(1),P_(2),P_(2),P_(3),P_(1) );
	  } else {
	    fprintf (file,  "4 %d %d %d %d ", P_(0),P_(1),P_(2),P_(3) );
	    printf("%d",P_(1));
	  }
	else
	  if (IS_TETRAHEDRA) {
	    fprintf (file,  "4 %d %d %d %d ", P_(0), P_(1), P_(2), P_(5) );
	    fprintf (file,  "4 %d %d %d %d ", P_(1), P_(3), P_(2), P_(5) );
	    fprintf (file,  "4 %d %d %d %d ", P_(0), P_(2), P_(4), P_(5) );
	    fprintf (file,  "4 %d %d %d %d ", P_(2), P_(6), P_(4), P_(5) );
	    fprintf (file,  "4 %d %d %d %d ", P_(3), P_(7), P_(2), P_(5) );
	    fprintf (file,  "4 %d %d %d %d ", P_(7), P_(6), P_(2), P_(5) );
	  } else {
	    fprintf (file,  "8 %d %d %d %d %d %d %d %d ",
		     P_(0), P_(1), P_(2), P_(3), P_(4), P_(5), P_(6), P_(7) );
	  }
	
	
      }
#undef P_                                                    // Some typing acceleration
    

    // write cell types
    {
      fprintf (file,  "\nCELL_TYPES %d\n", cells);
      int cell_type[2], cell_type_a[2];
      if (IS_TETRAHEDRA) {
	cell_type[0] = htonl(5);                             // 2D and 3D cell types, binary
	cell_type[1] = htonl(10);
	cell_type_a[0] = 5;                                  // ...                 , ASCI
	cell_type_a[1] = 10;
      } else {
	cell_type[0] = htonl(8);                             // 2D and 3D cell types, binary
	cell_type[1] = htonl(11);
	cell_type_a[0] = 8;                                  // ...                 , ASCI
	cell_type_a[1] = 11;
      }
      if (binary)
	for (int i=0; i<cells; ++i )
	  fwrite (&cell_type[g->DIM_NUM-2], sizeof(int), 1, file);
      else
	for (int i=0; i<cells; ++i )
	  fprintf (file,  "%d ",cell_type_a[g->DIM_NUM-2]);
      // write cell data
      fprintf (file,  "\nCELL_DATA %d\n", cells);
    }
    break;
    
  case 1: // PLOT3D format
    

#ifdef WRITE_AMRTREE_FILE
    // open the file for the connectivity record
    FILE *fb2;
    if ((fb2 = fopen (AMRTREE_FILE_NAME2,"w")) == 0) {
      fprintf(stderr,  "Error: opening file: %s\n", AMRTREE_FILE_NAME2);
      exit(1);
    }
#endif


    if (binary)
      for ( it = final_box_list.begin (); it != final_box_list.end (); ++it ) {
	
	if (g->DIM_NUM == 3) {
	  int h[24] = { it->p[0]->vid+1, it->p[1]->vid+1, it->p[2]->vid+1, it->p[5]->vid+1,
			it->p[1]->vid+1, it->p[3]->vid+1, it->p[2]->vid+1, it->p[5]->vid+1,
			it->p[0]->vid+1, it->p[2]->vid+1, it->p[4]->vid+1, it->p[5]->vid+1,
			it->p[2]->vid+1, it->p[6]->vid+1, it->p[4]->vid+1, it->p[5]->vid+1,
			it->p[3]->vid+1, it->p[7]->vid+1, it->p[2]->vid+1, it->p[5]->vid+1,
			it->p[7]->vid+1, it->p[6]->vid+1, it->p[2]->vid+1, it->p[5]->vid+1 };
	  fwrite ( h, sizeof(int), 24, file );
#ifdef WRITE_AMRTREE_FILE
	  int hh [8] = { it->p[0]->vid, it->p[1]->vid, it->p[2]->vid, it->p[3]->vid,
			 it->p[4]->vid, it->p[5]->vid, it->p[6]->vid, it->p[7]->vid };
	  fwrite ( hh, sizeof(int), 8, fb2 );
#endif
	} else {
	  int h[6] = { it->p[0]->vid+1, it->p[1]->vid+1, it->p[2]->vid+1,
		       it->p[1]->vid+1, it->p[3]->vid+1, it->p[2]->vid+1 };
	  fwrite ( h, sizeof(int), 6, file );
	}
      }
    else
      for ( it = final_box_list.begin (); it != final_box_list.end (); ++it ) {
	if (g->DIM_NUM == 3) {
	  int h[8] = { it->p[0]->vid+1, it->p[1]->vid+1, it->p[2]->vid+1, it->p[3]->vid+1,
		       it->p[4]->vid+1, it->p[5]->vid+1, it->p[6]->vid+1, it->p[7]->vid+1 };
	  fprintf (file,  "%d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d ",
		   h[0], h[1], h[2], h[5],
		   h[1], h[3], h[2], h[5],
		   h[0], h[2], h[4], h[5],
		   h[2], h[6], h[4], h[5],
		   h[3], h[7], h[2], h[5],
		   h[7], h[6], h[2], h[5] );		 
	} else {
	  int h[4] = { it->p[0]->vid+1, it->p[1]->vid+1, it->p[2]->vid+1, it->p[3]->vid+1 };
	  fprintf (file,  "%d %d %d %d %d %d\n", h[0], h[1], h[2], h[1], h[3], h[2] );
	}
      }
#ifdef WRITE_AMRTREE_FILE
    fclose (fb2);
#endif
    break;
    
  case 2:  // CGNS format
    {
      int size_of_ielem;        // number of vertexices per box
      if (g->DIM_NUM == 3)
	if (IS_TETRAHEDRA)
	  size_of_ielem = 24;
	else
	  size_of_ielem = 8;
      else
	if (IS_TETRAHEDRA)
	  size_of_ielem = 6;
	else
	  size_of_ielem = 4;
      ielem = new int [size_of_ielem*cells];
      if (!ielem) {
	fprintf(stderr,  "Error: not enougth memory for CGNS connectivity\n");
	exit (1);
      }
      int ielemnum = 0;
      for ( it = final_box_list.begin (); it != final_box_list.end (); ++it, ++ielemnum ) {
	if (g->DIM_NUM == 3)
	  if (IS_TETRAHEDRA) {
	    ielem[size_of_ielem*ielemnum   ] = it->p[0]->vid+1;
	    ielem[size_of_ielem*ielemnum+1 ] = it->p[1]->vid+1;
	    ielem[size_of_ielem*ielemnum+2 ] = it->p[2]->vid+1;
	    ielem[size_of_ielem*ielemnum+3 ] = it->p[5]->vid+1;
	    ielem[size_of_ielem*ielemnum+4 ] = it->p[1]->vid+1;
	    ielem[size_of_ielem*ielemnum+5 ] = it->p[3]->vid+1;
	    ielem[size_of_ielem*ielemnum+6 ] = it->p[2]->vid+1;
	    ielem[size_of_ielem*ielemnum+7 ] = it->p[5]->vid+1;
	    ielem[size_of_ielem*ielemnum+8 ] = it->p[0]->vid+1;
	    ielem[size_of_ielem*ielemnum+9 ] = it->p[2]->vid+1;
	    ielem[size_of_ielem*ielemnum+10] = it->p[4]->vid+1;
	    ielem[size_of_ielem*ielemnum+11] = it->p[5]->vid+1;
	    ielem[size_of_ielem*ielemnum+12] = it->p[2]->vid+1;
	    ielem[size_of_ielem*ielemnum+13] = it->p[6]->vid+1;
	    ielem[size_of_ielem*ielemnum+14] = it->p[4]->vid+1;
	    ielem[size_of_ielem*ielemnum+15] = it->p[5]->vid+1;
	    ielem[size_of_ielem*ielemnum+16] = it->p[3]->vid+1;
	    ielem[size_of_ielem*ielemnum+17] = it->p[7]->vid+1;
	    ielem[size_of_ielem*ielemnum+18] = it->p[2]->vid+1;
	    ielem[size_of_ielem*ielemnum+19] = it->p[5]->vid+1;
	    ielem[size_of_ielem*ielemnum+20] = it->p[7]->vid+1;
	    ielem[size_of_ielem*ielemnum+21] = it->p[6]->vid+1;;
	    ielem[size_of_ielem*ielemnum+22] = it->p[2]->vid+1;
	    ielem[size_of_ielem*ielemnum+23] = it->p[5]->vid+1;
	  } else {
	    ielem[(ielemnum<<3)  ] = it->p[0]->vid+1;
	    ielem[(ielemnum<<3)+1] = it->p[1]->vid+1;
	    ielem[(ielemnum<<3)+2] = it->p[3]->vid+1;
	    ielem[(ielemnum<<3)+3] = it->p[2]->vid+1;
	    ielem[(ielemnum<<3)+4] = it->p[4]->vid+1;
	    ielem[(ielemnum<<3)+5] = it->p[5]->vid+1;
	    ielem[(ielemnum<<3)+6] = it->p[7]->vid+1;
	    ielem[(ielemnum<<3)+7] = it->p[6]->vid+1;
	  }
	else
	  if (IS_TETRAHEDRA) {
	    ielem[size_of_ielem*ielemnum  ] = it->p[0]->vid+1;
	    ielem[size_of_ielem*ielemnum+1] = it->p[1]->vid+1;
	    ielem[size_of_ielem*ielemnum+2] = it->p[2]->vid+1;
	    ielem[size_of_ielem*ielemnum+3] = it->p[1]->vid+1;
	    ielem[size_of_ielem*ielemnum+4] = it->p[3]->vid+1;
	    ielem[size_of_ielem*ielemnum+5] = it->p[2]->vid+1;
	  } else {
	    ielem[(ielemnum<<2)  ] = it->p[0]->vid+1;
	    ielem[(ielemnum<<2)+1] = it->p[1]->vid+1;
	    ielem[(ielemnum<<2)+2] = it->p[3]->vid+1;
	    ielem[(ielemnum<<2)+3] = it->p[2]->vid+1;
	  }
      }
      break;
    }
  case 3: // call from AMR::interpolate
    {
#ifdef WRITE_AMRTREE_FILE
      // open the file for the connectivity record
      FILE *fb2;
      if ((fb2 = fopen (AMRTREE_FILE_NAME2,"w")) == 0) {
	fprintf(stderr,  "Error: opening file: %s\n", AMRTREE_FILE_NAME2);
	exit(1);
      }
#endif
      tll_cell.reserve ( final_box_list.size ()*(g->TYPES_NUM) );
      for ( it = final_box_list.begin (); it != final_box_list.end (); ++it ) {
	if (g->DIM_NUM == 3) {
	  tll_cell.push_back ( it->p[0]->vid );
	  tll_cell.push_back ( it->p[1]->vid );
	  tll_cell.push_back ( it->p[2]->vid );
	  tll_cell.push_back ( it->p[3]->vid );
	  tll_cell.push_back ( it->p[4]->vid );
	  tll_cell.push_back ( it->p[5]->vid );
	  tll_cell.push_back ( it->p[6]->vid );
	  tll_cell.push_back ( it->p[7]->vid );
#ifdef WRITE_AMRTREE_FILE
	  int hh [8] = { it->p[0]->vid, it->p[1]->vid, it->p[2]->vid, it->p[3]->vid,
			 it->p[4]->vid, it->p[5]->vid, it->p[6]->vid, it->p[7]->vid };
	  fwrite ( hh, sizeof(int), 8, fb2 );
#endif
	} else {
	  tll_cell.push_back ( it->p[0]->vid );
	  tll_cell.push_back ( it->p[1]->vid );
	  tll_cell.push_back ( it->p[2]->vid );
	  tll_cell.push_back ( it->p[3]->vid );
#ifdef WRITE_AMRTREE_FILE
	  int hh [4] = { it->p[0]->vid, it->p[1]->vid, it->p[2]->vid, it->p[3]->vid };
	  fwrite ( hh, sizeof(int), 4, fb2 );
#endif
	}
      }
#ifdef WRITE_AMRTREE_FILE
    fclose (fb2);
#endif
      break;
    }
  case 4:                      // Ensight Gold Format
    {
    // write the boxes
    if (binary)
      for ( it = final_box_list.begin (); it != final_box_list.end (); ++it ) {
	
#ifndef P_                                                   //
#define P_(a) (  (it->p[(a)]->vid) + 1  )                    // Some typing acceleration  
#else                                                        //
#error Please rename already defined local macros P_         //
#endif                                                       //

	int point_index [24] =
	  { 
	    P_(0), P_(1), P_(3), P_(2),  
	    0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0};

	
	if (g->DIM_NUM == 3) {
	  if (IS_TETRAHEDRA) {                              // six Ensight tetra4 cells
	    point_index [3] = P_(5);                        // for each 3D box
	    point_index [4] = P_(1);
	    point_index [5] = P_(2);
	    point_index [6] = P_(3);
	    point_index [7] = P_(5);
	    point_index [8] = P_(0);
	    point_index [9] = P_(3);
	    point_index [10] = P_(4);
	    point_index [11] = P_(5);
	    point_index [12] = P_(3);
	    point_index [13] = P_(7);
	    point_index [14] = P_(4);
	    point_index [15] = P_(5);
	    point_index [16] = P_(2);
	    point_index [17] = P_(6);
	    point_index [18] = P_(3);
	    point_index [19] = P_(5);
	    point_index [20] = P_(6);
	    point_index [21] = P_(7);
	    point_index [22] = P_(3);
	    point_index [23] = P_(5);
	    fwrite (point_index, sizeof(int), 24, file);  
	  } else {
	    point_index [4] = P_(4);                      // one Ensight hexa8 cell
	    point_index [5] = P_(5);                     // for each 3D box
	    point_index [6] = P_(7);
	    point_index [7] = P_(6);  
	    fwrite (point_index, sizeof(int), 8, file);
	  }
	} else {
	  if (IS_TETRAHEDRA) {                             // two Ensight tri3 cell
	    point_index [3] = P_(3);                       // for each 2D box 
	    point_index [4] = P_(2);
	    point_index [5] = P_(1);
	    fwrite (point_index, sizeof(int), 6, file);
	  } else {                                         // one Ensight quad4 cell
	    fwrite (point_index, sizeof(int), 4, file);    // for each 2D box
	  }
	}
#undef P_                                                    // Some typing acceleration
	
      }
    else
      for ( it = final_box_list.begin (); it != final_box_list.end (); ++it ) {
	
#ifndef P_                                                   //
#define P_(a)  ((it->p[(a)]->vid) + 1 )                      // Some typing acceleration 
#else                                                        //
#error Please rename already defined local macros P_         //
#endif                                                       //
	
	if (g->DIM_NUM == 2)
	  if (IS_TETRAHEDRA) {
	    fprintf (file,  "%10d%10d%10d\n", P_(0),P_(1),P_(3) );
	    fprintf (file,  "%10d%10d%10d\n", P_(3),P_(2),P_(1) );  //Ensight tri3
	  } else {
	    fprintf (file,  "%10d%10d%10d%10d\n", P_(0),P_(1),P_(3),P_(2) );  // Ensight quad4
	  }
	else
	  if (IS_TETRAHEDRA) {
	    fprintf (file,  "%10d%10d%10d%10d\n", P_(0), P_(1), P_(3), P_(5) );
	    fprintf (file,  "%10d%10d%10d%10d\n", P_(1), P_(2), P_(3), P_(5) );
	    fprintf (file,  "%10d%10d%10d%10d\n", P_(0), P_(3), P_(4), P_(5) );
	    fprintf (file,  "%10d%10d%10d%10d\n", P_(3), P_(7), P_(4), P_(5) );
	    fprintf (file,  "%10d%10d%10d%10d\n", P_(2), P_(6), P_(3), P_(5) );
	    fprintf (file,  "%10d%10d%10d%10d\n", P_(6), P_(7), P_(3), P_(5) );  //Ensigh hex8
	  } else {
	    fprintf (file,  "%10d%10d%10d%10d%10d%10d%10d%10d\n",
		     P_(0), P_(1), P_(3), P_(2), P_(4), P_(5), P_(7), P_(6) );  //Ensight quad4
	  }
	
	
      }
#undef P_                                                    // Some typing acceleration
    

    break;
    }
  } // of switch
}


//-----------------------------------------------------------------------------------
// Create tree hierarchy of boxes and count them
// Assumes 2 or 3D, non-periodic mesh
// Final box hierarchy is stored into final_bix_list
// flag == 0 by default
// flag == 1 for call from AMR::interpolate
void AMR::create_box_tree (const bool flag) {

#ifdef EBUG_AMR
  f->print_info(1,0);//printf("STOP: in AMR::create_box_tree\n");exit(1);
#endif

  // called from 2-3D section only
  if ((g->DIM_NUM != 2) && (g->DIM_NUM != 3))
    return;
  
#ifndef PERIODIC_AMR
  // --- non-periodic mesh only
  if ( g->PERIODIC[0] == 1 || g->PERIODIC[1] == 1 ||
       (g->DIM_NUM == 3 && g->PERIODIC[2] == 1 ) ) {
    fprintf (stderr, "\nAMR::create_box_tree: periodic mesh - cells have not been created !\n");
    exit (1);
  }
#endif
  
  // create boxes at tree root level and
  // insert them into list for consideration
  //  in direct order
  int b_s = f->get_x_space ();                                    // tree size
  Node* c [8];                                                    // box corners, 2 or 3-D
  for ( int i = f->get_number_of_trees()-1; i >= 0; --i ) {
    
    c[0] = (f->get_trees())[i]->get_root();                   // node 0 or the box:
    int  c0c[3] = { c[0]->ii[0], c[0]->ii[1], 0 };            // node c[0] coordinates
    int upl[3] = { g->FINEST_MESH[0], g->FINEST_MESH[1], 1 }; // upper domain limits
    if (g->DIM_NUM == 3) {
      c0c[2] = c[0]->ii[2];
      upl[2] = g->FINEST_MESH[2];
    }

#ifdef EBUG_AMR
    printf ("tree %d, tree_rank=%d, par_rank=%d\n",i,f->get_proc_by_coordinates (c0c, g->J_MAX),g->par_rank);
#endif
    
    if ( (c0c[0] != upl[0]) &&      // node c[0]:
	 (c0c[1] != upl[1]) &&      // 1) is not the tree root laying on the upper boundary
	 (c0c[2] != upl[2]) &&      // 2) belongs to the current processor
	 //(f->get_processor_of_the_node(c[0]) == g->par_rank)             // assumes node's tlp_index set
	 (f->get_proc_by_coordinates (c0c, g->J_MAX) == g->par_rank)       // assumes nothing
	 ) {
      
#ifndef P_                                                   //
#define P_(a) ( c[0]->ii[(a)] )                              // Some typing acceleration
#else                                                        //
#error Please rename already defined local macros P_         //
#endif                                                       //
      
      if (g->DIM_NUM == 2) {
	
#ifndef PERIODIC_AMR
	// --- non-periodic version
	int c1xy [2] = { P_(0) + b_s, P_(1)            };            // VKT order:
	int c2xy [2] = { P_(0),            P_(1) + b_s };            //   2 3
	int c3xy [2] = { c1xy[0], c2xy[1] };                         //   0 1
#else
	// --- periodic version
	int c1xy [2] = { g->PERIODIC[0]    * (P_(0) + b_s)%g->FINEST_MESH[0] + 
			 (1-g->PERIODIC[0])* (P_(0) + b_s),
			 P_(1) };
	int c2xy [2] = { P_(0),
			 g->PERIODIC[0]    * (P_(1) + b_s)%g->FINEST_MESH[1] +
			 (1-g->PERIODIC[0])* (P_(1) + b_s) };
	int c3xy [2] = { c1xy[0], c2xy[1] };
#endif

// 	c[1] = (Node*) f->test_by_coordinates( c1xy, g->J_MAX );
// 	c[2] = (Node*) f->test_by_coordinates( c2xy, g->J_MAX );
// 	c[3] = (Node*) f->test_by_coordinates( c3xy, g->J_MAX );
	// use get_root_by_jmax_coordinates() since test_by_coordinates() returns zero for intermediate nodes
	c[1] = f->get_root_by_jmax_coordinates( c1xy );
	c[2] = f->get_root_by_jmax_coordinates( c2xy );
	c[3] = f->get_root_by_jmax_coordinates( c3xy );
	if (IS_TETRAHEDRA)
	  AMR::cells += 2;
	else
	  AMR::cells ++;
	
	box_list.push_front ( Box(c[0],c[1],c[2],c[3]) );
#ifdef EBUG_AMR
	printf ( "root box [%x %x %x %x]\n",c[0],c[1], c[2],c[3]);
	printf ("\t(%d %d)(%d %d)(%d %d)(%d %d)\n",c[0]->ii[0],c[0]->ii[1], 
		c1xy[0],c1xy[1], c2xy[0],c2xy[1], c3xy[0],c3xy[1] );
	//printf ( "root box (%d %d ) - \n",c[0]->ii[0],c[0]->ii[1]);
#endif
      } else {
	
#ifndef PERIODIC_AMR
	// --- non-periodic version
	int c1xy [3] = { P_(0) + b_s, P_(1)      , P_(2)       };   // VKT order:
	int c2xy [3] = { P_(0)      , P_(1) + b_s, P_(2)       };   //   2 3
	int c3xy [3] = { P_(0) + b_s, P_(1) + b_s, P_(2)       };   //   0 1 (z=0)
	int c4xy [3] = { P_(0)      , P_(1)      , P_(2) + b_s };   //
	int c5xy [3] = { P_(0) + b_s, P_(1)      , P_(2) + b_s };   //   6 7
	int c6xy [3] = { P_(0)      , P_(1) + b_s, P_(2) + b_s };   //   4 5 (z>0)
	int c7xy [3] = { c1xy[0]    , c2xy[1]    , c4xy[2]     };   //
#else
 	// --- periodic version
	int c1xy [3] = { g->PERIODIC[0]*(P_(0) + b_s)%g->FINEST_MESH[0] + (1-g->PERIODIC[0])*(P_(0) + b_s),
			 P_(1),
			 P_(2) };
	int c2xy [3] = { P_(0),
			 g->PERIODIC[1]*(P_(1) + b_s)%g->FINEST_MESH[1] + (1-g->PERIODIC[1])*(P_(1) + b_s),
			 P_(2) };
	int c3xy [3] = { c1xy[0],
			 c2xy[1],
			 P_(2) };
	int c4xy [3] = { P_(0),
			 P_(1),
			 g->PERIODIC[2]*(P_(2) + b_s)%g->FINEST_MESH[2] + (1-g->PERIODIC[2])*(P_(2) + b_s) };
	int c5xy [3] = { c1xy[0],
			 P_(1),
			 c4xy[2] };
	int c6xy [3] = { P_(0),
			 c2xy[1],
			 c4xy[2] };
	int c7xy [3] = { c1xy[0],
			 c2xy[1],
			 c4xy[2] };
#endif

	c[1] = (Node*) f->test_by_coordinates( c1xy, g->J_MAX );
	c[2] = (Node*) f->test_by_coordinates( c2xy, g->J_MAX );
	c[3] = (Node*) f->test_by_coordinates( c3xy, g->J_MAX );
	c[4] = (Node*) f->test_by_coordinates( c4xy, g->J_MAX );
	c[5] = (Node*) f->test_by_coordinates( c5xy, g->J_MAX );
	c[6] = (Node*) f->test_by_coordinates( c6xy, g->J_MAX );
	c[7] = (Node*) f->test_by_coordinates( c7xy, g->J_MAX );
	if (IS_TETRAHEDRA)
	  AMR::cells += 6;
	else
	  AMR::cells += 1;
	box_list.push_front ( Box(c[0],c[1],c[2],c[3],c[4],c[5],c[6],c[7]) );
      }
      
#undef P_                                                    // Some typing acceleration
      
    }
  }
  

  
  //   for (it = box_list.begin (); it != box_list.end (); ++it)
  //     printf ( "box (%d %d %d)",it->p[0]->ii[0],it->p[0]->ii[1],it->p[0]->ii[2] );
  //   printf ("\n");
  
#ifdef WRITE_AMRTREE_FILE
  // open the file for the bisection record
  FILE *fb1;
  if ((fb1 = fopen (AMRTREE_FILE_NAME1,"w")) == 0) {
    fprintf(stderr,  "Error: opening file: %s\n", AMRTREE_FILE_NAME1);
    exit(1);
  }
  // print globals
  int namrt[3] = { g->FINEST_MESH[0] >> (g->J_MAX-g->J_TREE),
		   g->FINEST_MESH[1] >> (g->J_MAX-g->J_TREE),
		   1 };
  if (g->DIM_NUM == 3)
    namrt[2] = g->FINEST_MESH[2] >> ( g->J_MAX-g->J_TREE );
  
  fprintf ( fb1,"%d %d %d %d %d\n",
	    g->DIM_NUM,                       // dimension
	    namrt[0],                         // Mx, My, Mz
	    namrt[1],                         //  corrected for
	    namrt[2],                         //  J_TREE level
	    g->J_MAX - g->J_TREE              // max number of bisections
	    );
#endif

  
  int tll_info_level = 0;                     // current first index of tll_info[][]

#ifdef EBUG_AMR
  printf ("finished root level boxes: %d\n",cells);
#endif

  // sweep the list for each level of possible subdivision
  // and bisect each box if necessary
  for ( int lev = 0; lev < g->J_MAX - g->J_TREE; ++lev ) {
    b_s       // if the current box to be bisected
      >>= 1;  // this will be the size of its child
    
#ifdef EBUG_AMR
    printf ("lev=%d, b_s=%d\n",lev,b_s);
#endif    

    it = box_list.begin ();
    bool something_bisected = false;
    while ( it != box_list.end () )
      {
	Node *r = it->p[0];
	
#ifdef EBUG_AMR
	printf ( "box (%d %d ) - \n",r->ii[0],r->ii[1]); //,r->ii[2] );
#endif
#if (TREE_VERSION == 1)
	int chi = (g->TYPES_NUM-1);
	bool  bisect_the_box =
	  ( ( r->ptr ) &&                                     // if has children
	    ( r->ptr+chi ) &&                                 // if there is a central child
	    ( ( (r->ptr+chi)->id & significant_id ) ||        //  which is significant
	      ( (r->ptr+chi)->id & id_of_added_amr ) )        //  or added
	    );                                                //  then, bisect the box
#else
	int chi =                             // index of the central child to test
	  (g->TYPES_NUM-1) *                  //  one level difference with the root
	  (lev - r->level + 1) - 1;
	bool  bisect_the_box =
	  ( ( r->num_leaves > chi ) &&                        // if there are enough leaves
	    ( r->leaves[chi] ) &&                             // if there is a central child
	    ( ( r->leaves[chi]->id & significant_id ) ||      //  which is significant
	      ( r->leaves[chi]->id & id_of_added_amr ) )      //  or added
	    );                                                //  then, bisect the box
#endif


	if ( bisect_the_box ) {
#ifdef EBUG_AMR
 	  printf ("bisected to :");
#endif
#ifndef P_                                                   //
#define P_(a) ( r->ii[(a)] )                                 // Some typing acceleration
#else                                                        //
#error Please rename already defined local macros P_         //
#endif                                                       //
	  int ar_size;
	  Node* ar[19];
	  
	  if (g->DIM_NUM == 2) {

#ifndef PERIODIC_AMR
	    // --- non-periodic version                               // VTK order: 2 e 3
 	    int d_xy [2] = { P_(0) + (b_s<<1), P_(1) + b_s      };    //            b c d
 	    int e_xy [2] = { P_(0) + b_s     , P_(1) + (b_s<<1) };    //            0 a 1
#else
	    // --- periodic version
	    int d_xy [2] = { g->PERIODIC[0]*(P_(0) + (b_s<<1))%g->FINEST_MESH[0] + (1-g->PERIODIC[0])*(P_(0) + (b_s<<1)),
			     P_(1) + b_s      };
	    int e_xy [2] = { P_(0) + b_s     ,
			     g->PERIODIC[1]*(P_(1) + (b_s<<1))%g->FINEST_MESH[1] + (1-g->PERIODIC[1])*(P_(1) + (b_s<<1)) };
#endif	    
	    ar_size = 5;

#if (TREE_VERSION == 1)
	    // before processing children we should descend to the node of the correct level,
	    //  since 'r' is the upper representation of the box corner node (with id and coordinates)
	    //  and the required ar[] might be direct child of another representation (with intermediate id)
	    // So, find such representation that 'b_s' is the distance to its children
	    int x_level = f->find_node_tree_level (r->ii);   // node's level
	    int x_b_s = f->get_x_space () >> (x_level+1);    // b_s at node's child level
	    Node *rr = r;
	    //printf ("x_level=%d, x_b_s=%d, b_s=%d\n",x_level,x_b_s,b_s);
	    while (x_b_s > b_s) {
	      x_b_s >>= 1;
	      rr = rr->ptr;
	      if (rr == 0) break;
	    }
	    if ((rr)&&(rr->ptr)) {
	      ar[0] = rr->ptr + chi-2;
	      ar[1] = rr->ptr + chi-1;
	      ar[2] = rr->ptr + chi;
	    } else
	      ar[0] = ar[1] = ar[2] = 0;
#else
	    /*0 a*/  ar[0] = r->leaves[chi-2];
	    /*1 b*/  ar[1] = r->leaves[chi-1];
	    /*2 c*/  ar[2] = r->leaves[chi];
#endif

	    /*3 d*/  ar[3] = (Node*) f->test_by_coordinates ( d_xy, g->J_MAX );
	    /*4 e*/  ar[4] = (Node*) f->test_by_coordinates ( e_xy, g->J_MAX );
	    
#ifdef EBUG_AMR
	    //printf ("chi=%d\n",chi);
	    for (int i=0; i<ar_size; ++i) {
	      //printf (" %x: ", ar[i]);
	      if (ar[i]) printf ("%d %d, id=%d\n",ar[i]->ii[0],ar[i]->ii[1], ar[i]->id);
	      else printf ("\n");
	    }
	    //exit(1);
#endif
	  } else {

#ifndef PERIODIC_AMR
	    // --- non-periodic version
	    int b120_ [3] = { P_(0) + b_s     , P_(1) + (b_s<<1), P_(2) };        // VTK order:
	    int b210_ [3] = { P_(0) + (b_s<<1), P_(1) + b_s     , P_(2) };        //  a1-a7 for
	    int b201_ [3] = { P_(0) + (b_s<<1), P_(1)           , P_(2) + b_s };   //  children of 0
	    int b021_ [3] = { P_(0)           , P_(1) + (b_s<<1), P_(2) + b_s };   //  (7 new nodes)
	    int b211_ [3] = { P_(0) + (b_s<<1), P_(1) + b_s     , P_(2) + b_s };   //
	    int b121_ [3] = { P_(0) + b_s     , P_(1) + (b_s<<1), P_(2) + b_s };   //
	    int b221_ [3] = { P_(0) + (b_s<<1), P_(1) + (b_s<<1), P_(2) + b_s };   //
	    int b102_ [3] = { P_(0) + b_s     , P_(1)           , P_(2) + (b_s<<1) };  //  bABC, (A,B,C = 0,1,2)
	    int b012_ [3] = { P_(0)           , P_(1) + b_s     , P_(2) + (b_s<<1) };  //  for all others
	    int b112_ [3] = { P_(0) + b_s,      P_(1) + b_s     , P_(2) + (b_s<<1) };  //  (12 new nodes)
	    int b212_ [3] = { P_(0) + (b_s<<1), P_(1) + b_s     , P_(2) + (b_s<<1) };  //
	    int b122_ [3] = { P_(0) + b_s,      P_(1) + (b_s<<1), P_(2) + (b_s<<1) };  //
#else
	    // --- periodic version
	    int b120_ [3] = { P_(0) + b_s,
			      g->PERIODIC[1]*(P_(1) + (b_s<<1))%g->FINEST_MESH[1]+(1-g->PERIODIC[1])*(P_(1) + (b_s<<1)), // b120_[1]
			      P_(2) };
	    int b210_ [3] = { g->PERIODIC[0]*(P_(0) + (b_s<<1))%g->FINEST_MESH[0]+(1-g->PERIODIC[0])*(P_(0) + (b_s<<1)), // b210_[0]
			      P_(1) + b_s,
			      P_(2) };
	    int b201_ [3] = { b210_[0],
			      P_(1),
			      P_(2) + b_s };
	    int b021_ [3] = { P_(0),
			      b120_[1],
			      P_(2) + b_s };
	    int b211_ [3] = { b210_[0],
			      P_(1) + b_s,
			      P_(2) + b_s };
	    int b121_ [3] = { P_(0) + b_s ,
			      b120_[1],
			      P_(2) + b_s };
	    int b221_ [3] = { b210_[0],
			      b120_[1],
			      P_(2) + b_s };
	    int b102_ [3] = { P_(0) + b_s,
			      P_(1),
			      g->PERIODIC[2]*(P_(2) + (b_s<<1))%g->FINEST_MESH[2]+(1-g->PERIODIC[2])*(P_(2) + (b_s<<1)) };  // b102_[2]
	    int b012_ [3] = { P_(0),
			      P_(1) + b_s,
			      b102_[2] };
	    int b112_ [3] = { P_(0) + b_s,
			      P_(1) + b_s,
			      b102_[2] };
	    int b212_ [3] = { b210_[0],
			      P_(1) + b_s,
			      b102_[2] };
	    int b122_ [3] = { P_(0) + b_s,
			      b120_[1],
			      b102_[2] };
#endif
	    ar_size = 19;

#if (TREE_VERSION ==1)
	    // before processing children we should descend to the node of the correct level,
	    //  since 'r' is the upper representation of the box corner node (with id and coordinates)
	    //  and the required ar[] might be direct child of another representation (with intermediate id)
	    // So, find such representation that 'b_s' is the distance to its children
	    int x_level = f->find_node_tree_level (r->ii);   // node's level
	    int x_b_s = f->get_x_space () >> (x_level+1);    // b_s at node's child level
	    Node *rr = r;
	    //printf ("x_level=%d, x_b_s=%d, b_s=%d\n",x_level,x_b_s,b_s);
	    while (x_b_s > b_s) {
	      x_b_s >>= 1;
	      rr = rr->ptr;
	      if (rr == 0) break;
	    }
	    if ((rr)&&(rr->ptr)) {
	      /* 0  a1 */  ar[0] = rr->ptr + chi-6;
	      /* 1  a2 */  ar[1] = rr->ptr + chi-5;
	      /* 2  a3 */  ar[2] = rr->ptr + chi-4;
	      /* 3  a4 */  ar[3] = rr->ptr + chi-3;
	      /* 4  a5 */  ar[4] = rr->ptr + chi-2;
	      /* 5  a6 */  ar[5] = rr->ptr + chi-1;
	      /* 6  a7 */  ar[6] = rr->ptr + chi;
	    } else
	      ar[0] = ar[1] = ar[2] = ar[3] = ar[4] = ar[5] = ar[6] = 0;
#else
	    /* 0  a1 */  ar[0] = r->leaves[chi-6];
	    /* 1  a2 */  ar[1] = r->leaves[chi-5];
	    /* 2  a3 */  ar[2] = r->leaves[chi-4];
	    /* 3  a4 */  ar[3] = r->leaves[chi-3];
	    /* 4  a5 */  ar[4] = r->leaves[chi-2];
	    /* 5  a6 */  ar[5] = r->leaves[chi-1];
	    /* 6  a7 */  ar[6] = r->leaves[chi];
#endif

	    /* 7  b120 */  ar[7] = (Node*) f->test_by_coordinates ( b120_, g->J_MAX );
	    /* 8  b210 */  ar[8] = (Node*) f->test_by_coordinates ( b210_, g->J_MAX );
	    /* 9  b201 */  ar[9] = (Node*) f->test_by_coordinates ( b201_, g->J_MAX );
	    /* 10 b021 */  ar[10] = (Node*) f->test_by_coordinates ( b021_, g->J_MAX );
	    /* 11 b211 */  ar[11] = (Node*) f->test_by_coordinates ( b211_, g->J_MAX );
	    /* 12 b121 */  ar[12] = (Node*) f->test_by_coordinates ( b121_, g->J_MAX );
	    /* 13 b221 */  ar[13] = (Node*) f->test_by_coordinates ( b221_, g->J_MAX );
	    /* 14 b102 */  ar[14] = (Node*) f->test_by_coordinates ( b102_, g->J_MAX );
	    /* 15 b012 */  ar[15] = (Node*) f->test_by_coordinates ( b012_, g->J_MAX );
	    /* 16 b112 */  ar[16] = (Node*) f->test_by_coordinates ( b112_, g->J_MAX );
	    /* 17 b212 */  ar[17] = (Node*) f->test_by_coordinates ( b212_, g->J_MAX );
	    /* 18 b122 */  ar[18] = (Node*) f->test_by_coordinates ( b122_, g->J_MAX );
	  }
	  if ( test_presence_and_significance ( ar, ar_size ) ) {
	    bisect_the_box = false;                         // early termination if some nodes are NULL
	  } else {



	    if (g->DIM_NUM == 2) {                                                  // 4 new boxes in 2-D
	      next_box_list.push_back ( Box(r    , ar[0]   , ar[1]   , ar[2]    ) );
	      next_box_list.push_back ( Box(ar[0], it->p[1], ar[2]   , ar[3]    ) );
	      next_box_list.push_back ( Box(ar[1], ar[2]   , it->p[2], ar[4]    ) );
	      next_box_list.push_back ( Box(ar[2], ar[3]   , ar[4]   , it->p[3] ) );
	      if (IS_TETRAHEDRA)
		AMR::cells += 6;
	      else
		AMR::cells += 3;
	    } else {
	      next_box_list.push_back ( Box(r,    ar[0],   ar[1],   ar[2],   ar[3],   ar[4],   ar[5],   ar[6] ) );
	      next_box_list.push_back ( Box(ar[0],it->p[1],ar[2],   ar[8],   ar[4],   ar[9],   ar[6],   ar[11]) );
	      next_box_list.push_back ( Box(ar[1],ar[2]   ,it->p[2],ar[7],   ar[5],   ar[6],   ar[10],  ar[12]) ); // in
	      next_box_list.push_back ( Box(ar[2],ar[8]   ,ar[7]   ,it->p[3],ar[6],   ar[11],  ar[12],  ar[13]) ); // 3D
	      next_box_list.push_back ( Box(ar[3],ar[4]   ,ar[5]   ,ar[6]   ,it->p[4],ar[14],  ar[15],  ar[16]) ); // 8 new
	      next_box_list.push_back ( Box(ar[4],ar[9]   ,ar[6]   ,ar[11]  ,ar[14]  ,it->p[5],ar[16],  ar[17]) ); // boxes
	      next_box_list.push_back ( Box(ar[5],ar[6]   ,ar[10]  ,ar[12]  ,ar[15]  ,ar[16]  ,it->p[6],ar[18]) );
	      next_box_list.push_back ( Box(ar[6],ar[11]  ,ar[12]  ,ar[13]  ,ar[16]  ,ar[17]  ,ar[18]  ,it->p[7]) );
	      
	      // 	  printf ( "(%d %d %d) ...  (%d %d %d), pushed back to next_box_list\n",
	      // 		   a7->ii[0],a7->ii[1],a7->ii[2],  r->ii[0],r->ii[1],r->ii[2] );
	      
	      if (IS_TETRAHEDRA)
		AMR::cells += 42;
	      else
		AMR::cells += 7;
	    }
#undef P_                                                    // Some typing acceleration
	    
	    box_list.erase (it--);
	    
	    something_bisected = true;
#ifdef WRITE_AMRTREE_FILE
	    char cc = '1';     // bisection marker
	    fwrite ( &cc, sizeof(char), 1, fb1 );
#endif
	    if (flag)
	      tll_info[tll_info_level].push_back (true);
	    
	  }
	}
	if ( ! bisect_the_box) {
	  //printf ("not bisected, push_back to final list\n");
	  // box is not to be bisected, so move it into the final list
	  final_box_list.push_back (*it);
	  box_list.erase (it--);
#ifdef WRITE_AMRTREE_FILE
	  char cc = '0';
	  fwrite ( &cc, sizeof(char), 1, fb1 );
#endif
	  if (flag)
	    tll_info[tll_info_level].push_back (false);
	  
	}
	it++;
      } //while ( it != box_list.end () ) ...
    
    box_list.splice ( box_list.end(), next_box_list );
    next_box_list.clear (); // just in case
    
    //     printf ("box_list:\n");
    //     for (it = box_list.begin (); it != box_list.end (); ++it)
    //       printf ( "box (%d %d %d)",it->p[0]->ii[0],it->p[0]->ii[1],it->p[0]->ii[2] );
    //     printf ("\n");
    
    //     printf ("next_box_list:\n");
    //     for (it = next_box_list.begin (); it != next_box_list.end (); ++it)
    //       printf ( "box (%d %d %d)",it->p[0]->ii[0],it->p[0]->ii[1],it->p[0]->ii[2] );
    //     printf ("\n");
    //exit(1);
    

    if (flag)
      tll_info_len[tll_info_level] = tll_info[tll_info_level].size();
    
    //printf ("lev=%d, length=%d\n",tll_info_level, tll_info[tll_info_level].size());
    
    if ( ! something_bisected ) {
      //printf ("-- break --\n");
      tll_info_level ++;
      break;
    }
#ifdef WRITE_AMRTREE_FILE
    //printf ("'next level marker'\n");
    char cc = '2';    // next level marker
    fwrite ( &cc, sizeof(char), 1, fb1 );
#endif
    if (flag)
      tll_info_level ++;
    
  } //for ( int lev = 0; lev < g->J_MAX - g->J_TREE; ++lev ) ...
  
  // add the nodes of the finest level present to the final list
#ifdef WRITE_AMRTREE_FILE
  char c0 = '0';
  for (int i=box_list.size(); i>0; --i)
    fwrite ( &c0, sizeof(char), 1, fb1 );
  char cc = '2';    // next level marker
  fwrite ( &cc, sizeof(char), 1, fb1 );
  fclose (fb1);
#endif
  if (flag) {
    tll_info_len[tll_info_level] = box_list.size();
    //     tll_info[tll_info_level].reserve(box_list.size());
    //     for (int i=box_list.size(); i>0; --i)
    //       tll_info[tll_info_level].push_back (false);
  }

  //   it = box_list.begin ();
  //   while ( it != box_list.end () ) {
  //     final_box_list.push_back (*it);
  //     it++;
  //   }
  final_box_list.splice ( final_box_list.end(), box_list );

}

//-----------------------------------------------------------------------------------
// return 0 if all the given nodes exist and significant
bool AMR::test_presence_and_significance (Node* n[], int length) {
#ifdef SIMPLE_AMR
  // show incomplete AMR cells
  for ( int i=0; i<length; ++i )
    if ( (n[i] == 0) || ( ! (n[i]->id & significant_id) ) ) {
#ifdef EBUG_AMR
      printf ("AMR::test_presence and significance()= TRUE\n");
#endif
      return true;
    }
#ifdef EBUG_AMR
  printf ("AMR::test_presence and significance()= FALSE\n");
#endif
  return false;
#else
  // db_make_amr assumed to run before, so all the cells are complete
  return false;
#endif
}

//-----------------------------------------------------------------------------------
// Number all the nodes only, set AMR::size            (action_flag == 0)
// Write data into temporary arrays xyz[] and var[]    (action_flag == 1)
// Write node pointers into temporary array nodes[]    (action_flag == 2)
// Write iwlt into temporary array inodes[]            (action_flag == 3)
// 2 or 3D
void AMR::set_points_plot3d (const int action_flag, float*xyz, float*var, Node** nodes, int* inodes) {
  //
  // size = 0 by AMR constructor (for the first call only)
  //
  Node *tmp;
  int coord [DIM_MAX];
  int delta [DIM_MAX];
  
  int depth = g->J_MAX - g->J_TREE + 1;
  vector <Node*> path_node (depth, (Node*) 0);
  vector <int>   path_cindex (depth,  0);
  
  for (int si=f->get_number_of_trees()-1; si >= 0; --si ) {
    //for (int si=0; si<f->get_number_of_trees(); ++si ) {
    int main_index = 0;
    path_node   [0] = (f->get_trees())[si]->get_root();
    path_cindex [0] = 0;
    Node *node = path_node [0];

    // added in order to count only significant tree roots
    if ( ( node->id & significant_id )||( node->id & id_of_added_amr ) ) {
      switch (action_flag) {
      case 0:
	node->vid = size;                   // set node's VTK point number
	size++;                             // count that point
	break;
      case 1:
	xyz [node->vid]           = get_coord ( node->ii,0 );     // write coordinates
	xyz [node->vid+size]      = get_coord ( node->ii,1 );
	if (g->DIM_NUM == 3)
	  xyz [node->vid+size+size] = get_coord ( node->ii,2 );
	
	int i;
	for ( i=0; i<val_num; ++i)                     // write variables
	  var [node->vid + size*i] = node->val[i];
	for ( i=0; i<val_num_i; ++i)
	  var [node->vid + size*(i+val_num)] = (float) node->ival[i];
	break;
      case 2:
	nodes [node->vid] = node;
	break;
      case 3:
	inodes [node->vid] = node->ival[IWLT_I_P]; // iwlt stored as an integer variable
	break;
      }
    }

    while (true) {
      int ilow = path_cindex [main_index];
      int ci;
      int totc = 0;
      if (f->found_nonzero_child_link (node,ci,ilow,totc)) {
	
	main_index ++;
	path_cindex [main_index-1] = ci+1;
	path_cindex [main_index]   = 0;

#if (TREE_VERSION == 1)
	path_node [main_index] = node->ptr + ci;
	node = node->ptr + ci;
#else
	path_node [main_index] = node->leaves [ci];
	node = node->leaves [ci];
#endif
	
	if ( ( node->id & significant_id ) ||
	     ( node->id & id_of_added_amr ) )
	  switch (action_flag) {
	  case 0:
	    node->vid = size;                   // set node's VTK point number
	    size++;                             // count that point
	    break;
	  case 1:
	    xyz [node->vid]           = get_coord ( node->ii,0 );     // write coordinates
	    xyz [node->vid+size]      = get_coord ( node->ii,1 );
	    if (g->DIM_NUM == 3)
	      xyz [node->vid+size+size] = get_coord ( node->ii,2 );
	    
	    int i;
	    for ( i=0; i<val_num; ++i)                      // write variables
	      var [node->vid + size*i] = node->val[i];
	    for ( i=0; i<val_num_i; ++i)
	      var [node->vid + size*(i+val_num)] = (float) node->ival[i];
	    break;
	  case 2:
	    nodes [node->vid] = node;
	    break;
	  case 3:
	    inodes [node->vid] = node->ival[IWLT_I_P]; // iwlt stored as an integer variable
	    break;
	  }
      } else {
	
	main_index --;
	if (main_index < 0) break;
	
	path_node   [main_index+1] = NULL;
	path_cindex [main_index+1] = 0;
	node = path_node [main_index];
      }
    }
    
    for (int i=1; i< depth; i++) {
      path_node   [i] = NULL;
      path_cindex [i] = 0;
    }
  }
  //
  // size = actual number of points (for the first call only)
  //
}

//-----------------------------------------------------------------------------------
// Number all the nodes and write the
// correspondent points into VTK file ( first call, with val_index < 0 )
// Write function values into VTK file ( second call, with val_index >= 0 )
// 2 or 3-D
void AMR::set_points (FILE *file, const bool binary, const int val_index) {
  //
  // size = 0 by AMR constructor (for the first call only)
  //
  // --------------- tree search for all the nodes -----------------------
  Node *tmp;
  int coord [DIM_MAX];
  int delta [DIM_MAX];
  
  int depth = g->J_MAX - g->J_TREE + 1;
  vector <Node*> path_node (depth, (Node*) 0);
  vector <int>   path_cindex (depth,  0);
  
  for (int si=f->get_number_of_trees()-1; si >= 0; --si ) {
    int main_index = 0;
    path_node   [0] = (f->get_trees())[si]->get_root();
    path_cindex [0] = 0;
    Node *node = path_node [0];

    // added in order to count only significant tree roots
    if ( ( node->id & significant_id )||( node->id & id_of_added_amr ) ) {
      if ( val_index < 0 ) {
	node->vid = size;                   // set node's VTK point number
	size++;                             // count that point
	vtk_print_point (node,file,binary); // print this point to VTK file
      } else                                // print point's data to VTK file
	vtk_print_point_data (node,file,binary,val_index);
    }
    
    while (true) {
      int ilow = path_cindex [main_index];
      int ci;
      int totc = 0;
      if (f->found_nonzero_child_link (node,ci,ilow,totc)) {
	
	main_index ++;
	path_cindex [main_index-1] = ci+1;
	path_cindex [main_index]   = 0;
	
#if (TREE_VERSION == 1)
	path_node [main_index] = node->ptr + ci;
	node = node->ptr + ci;
#else
	path_node [main_index] = node->leaves [ci];
	node = node->leaves [ci];
#endif
	
	if ( ( node->id & significant_id ) ||
	     ( node->id & id_of_added_amr ) )
	  {
	    if ( val_index < 0 ) {
	      node->vid = size;
	      size++;
	      vtk_print_point (node,file,binary);
	    } else
	      vtk_print_point_data (node,file,binary,val_index);
	  }
      } else {
	
	main_index --;
	if (main_index < 0) break;
	
	path_node   [main_index+1] = NULL;
	path_cindex [main_index+1] = 0;
	node = path_node [main_index];
      }
    }
    
    for (int i=1; i< depth; i++) {
      path_node   [i] = NULL;
      path_cindex [i] = 0;
    }
  }
  //
  // size = actual number of points (for the first call only)
  //
}

//-----------------------------------------------------------------------------------
// Number all the nodes and write the
// correspondent points into Ensight file ( first call, with val_index < 0 )
// Write function values into ensight file ( second call, with val_index >= 0 )
// 2 or 3-D
void AMR::set_points_ensght (FILE *file, const bool binary, const int val_index, int idim) {
  //
  // size = 0 by AMR constructor (for the first call only)
  //
  // --------------- tree search for all the nodes -----------------------
  Node *tmp;
  int coord [DIM_MAX];
  int delta [DIM_MAX];
  
  int depth = g->J_MAX - g->J_TREE + 1;
  vector <Node*> path_node (depth, (Node*) 0);
  vector <int>   path_cindex (depth,  0);
  
  for (int si=f->get_number_of_trees()-1; si >= 0; --si ) {
    int main_index = 0;
    path_node   [0] = (f->get_trees())[si]->get_root();
    path_cindex [0] = 0;
    Node *node = path_node [0];

    // added in order to count only significant tree roots
    if ( ( node->id & significant_id )||( node->id & id_of_added_amr ) ) {
      if ( val_index < 0 ) {
	node->vid = size;                   // set node's Ensight point number
	size++;                             // count that point
	ensght_print_point (node,file,binary, idim); // print this point to Ensight file
      } else                                // print point's data to Ensight file
	ensght_print_point_data (node,file,binary,val_index);
    }
    
    while (true) {
      int ilow = path_cindex [main_index];
      int ci;
      int totc = 0;
      if (f->found_nonzero_child_link (node,ci,ilow,totc)) {
	
	main_index ++;
	path_cindex [main_index-1] = ci+1;
	path_cindex [main_index]   = 0;
	
#if (TREE_VERSION == 1)
	path_node [main_index] = node->ptr + ci;
	node = node->ptr + ci;
#else
	path_node [main_index] = node->leaves [ci];
	node = node->leaves [ci];
#endif
	
	if ( ( node->id & significant_id ) ||
	     ( node->id & id_of_added_amr ) )
	  {
	    if ( val_index < 0 ) {
	      node->vid = size;
	      size++;
	      ensght_print_point (node,file,binary, idim);
	    } else
	      ensght_print_point_data (node,file,binary,val_index);
	  }
      } else {
	
	main_index --;
	if (main_index < 0) break;
	
	path_node   [main_index+1] = NULL;
	path_cindex [main_index+1] = 0;
	node = path_node [main_index];
      }
    }
    
    for (int i=1; i< depth; i++) {
      path_node   [i] = NULL;
      path_cindex [i] = 0;
    }
  }
  //
  // size = actual number of points (for the first call only)
  //
}


//-----------------------------------------------------------------------------------
// Print VTK point into the file
// Assumes integer or float point coordinates
// 2 or 3-D
void AMR::vtk_print_point (Node* n, FILE* f, const bool b) {
  if (COORD_INTEGER)
    if (b) {
      int tmp [3] = { htonl ( n->ii[0] ), htonl ( n->ii[1] ), 0 };
      if (g->DIM_NUM == 3)
	tmp[2] = htonl ( n->ii[2] );
      
      fwrite (tmp, sizeof(int), 3, f);
    } else {
      int n3 = 0;
      if (g->DIM_NUM == 3)
	n3 = n->ii[2];
      fprintf (f,  "%d %d %d ", n->ii[0], n->ii[1], n3 );
    }
  else
    if (b) {
      float tmp[3];
      if (IS_BIG_ENDIAN) {
	tmp [0] = get_coord( n->ii,0 );
	tmp [1] = get_coord( n->ii,1 );
	tmp [2] = 0.0;
	if (g->DIM_NUM == 3)
	  tmp[2] = get_coord ( n->ii,2 );
      } else {
	tmp [0] = FloatSwap( get_coord( n->ii,0 ) );  
	tmp [1] = FloatSwap( get_coord( n->ii,1 ) );
	tmp [2] = 0.0;
	if (g->DIM_NUM == 3)
	  tmp[2] = FloatSwap( get_coord ( n->ii,2 ) );
      }
      
      fwrite (tmp, sizeof(float), 3, f);
    } else {
      float tmp [3] = { get_coord( n->ii,0 ), get_coord( n->ii,1 ), 0.0 };
      if (g->DIM_NUM == 3)
	tmp[2] = get_coord( n->ii,2 );
      fprintf (f,  "%f %f %f ", tmp[0], tmp[1], tmp[2] );
    }
}


//-----------------------------------------------------------------------------------
// Print VTK point's data  into the file
// Called from set_points ( ... , data_index )
// 2 or 3-D
void AMR::vtk_print_point_data (Node *n, FILE *f, const bool b, const int i) {
  int ival;
  float val;
  if (b)
    {
      if ( i < val_num ) {                 // this is real value
	if (IS_BIG_ENDIAN) {
	  val = n->val[i];
	} else {
	  val = FloatSwap ( n->val[i] );
	}
	fwrite (&val, sizeof(float), 1, f);
      } else {                                // this is integer value
	ival = htonl ( n->ival[i - val_num] );
	fwrite (&ival, sizeof(int), 1, f);
      }
    }
  else
    {
      if ( i < val_num )                   // this is real value
	fprintf (f,  "%f ",n->val[i]);
      else                                   // this is integer value
	fprintf (f,  "%d ",n->ival[i - val_num]);
    }
}


//-----------------------------------------------------------------------------------
// Print ENSIGHT point into the file
// Assumes integer or float point coordinates
// 2 or 3-D
void AMR::ensght_print_point (Node* n, FILE* f, const bool b, int idim) {     


  if (COORD_INTEGER)        //integer coordinates
    if (b) {
      float tmp [3] = { float (n->ii[0]), float (n->ii[1]), 0.0 };
      if (g->DIM_NUM == 3)
	tmp[2] =  float (n->ii[2]);     

      fwrite (&tmp[idim], sizeof(float), 1, f);

    } else {
      float n3 [3] = {  float (n->ii[0]), float ( n->ii[1]), 0.0 };
      if (g->DIM_NUM == 3)
	n3 [2] = float (n->ii[2]);
      else
	n3 [2] = 0.0;

      fprintf (f,  "%12.5e\n", n3[idim] );
    }
  else                     //real coordinates 
    if (b) {
      float tmp[3] = { get_coord( n->ii,0 ), get_coord( n->ii,1 ), 0.0 };
      if (g->DIM_NUM == 3)
	tmp[2] = get_coord ( n->ii,2 );
      
      fwrite (&tmp[idim], sizeof(float), 1, f);

    } else {
      float tmp [3] = { get_coord( n->ii,0 ), get_coord( n->ii,1 ), 0.0 };
      if (g->DIM_NUM == 3)
	tmp[2] = get_coord( n->ii,2 );

      fprintf (f,  "%12.5e\n", tmp[idim] );
    }
}


//-----------------------------------------------------------------------------------
// Print ENSIGHT point's data  into the file
// Called from set_points ( ... , data_index )
// 2 or 3-D
void AMR::ensght_print_point_data (Node *n, FILE *f, const bool b, const int i) {
  float ival;
  float val;
  if (b)
    {
      if ( i < val_num ) {                 // this is real value
	val = n->val[i];
	fwrite (&val, sizeof(float), 1, f);
      } else {                                // this is integer value  
	ival =  float (n->ival[i - val_num]) ;
	fwrite (&ival, sizeof(float), 1, f);
      }
    }
  else
    {
      if ( i < val_num )                   // this is real value
	fprintf (f,  "%12.5e\n",n->val[i]);
      else                                   // this is integer value
	fprintf (f,  "%12.5e\n", float (n->ival[i - val_num]) );
    }
}




//-----------------------------------------------------------------------------------
void AMR::set_string_header (const char* c) {
  string_header = c;
  if (string_header.size () > 250) {
    printf ("AMR comment exceeded 250 characters and has been truncated.\n");
    string_header.erase(251);
  }
}

//-----------------------------------------------------------------------------------
// fill 1D records
void AMR::fill_record_vector () {
  rec.clear ();
  f->count_nodes_to_visualize ();
  size = f->get_stat_nnodes ();
  if (size > 0) {
    rec.reserve (size);
    // --------------- tree search for all the nodes -----------------------
    Node *tmp;
    int *coord = new int [g->DIM_NUM];
    int *delta = new int [g->DIM_NUM];
    
    int depth = g->J_MAX - g->J_TREE + 1;
    vector <Node*> path_node (depth, (Node*) 0);
    vector <int>   path_cindex (depth,  0);
    
    for (int si=f->get_number_of_trees()-1; si >= 0; --si ) {
      int main_index = 0;
      path_node   [0] = (f->get_trees())[si]->get_root();
      path_cindex [0] = 0;
      Node *node = path_node [0];

      // added in order to count only significant tree roots
      if ( ( node->id & significant_id )||( node->id & id_of_added_amr ) ) {
	rec.push_back (Record (node->ii[0],node));
      }
      
      while (true) {
	int ilow = path_cindex [main_index];
	int ci;
	int totc = 0;
	if (f->found_nonzero_child_link (node,ci,ilow,totc)) {
	  
	  main_index ++;
	  path_cindex [main_index-1] = ci+1;
	  path_cindex [main_index]   = 0;
	  
#if (TREE_VERSION == 1)
	  path_node [main_index] = node->ptr + ci;
	  node = node->ptr + ci;
#else
	  path_node [main_index] = node->leaves [ci];
	  node = node->leaves [ci];
#endif
	  
	  if ( ( node->id & significant_id ) ||
	       ( node->id & id_of_added_amr ) )
	    {
	      rec.push_back (Record (node->ii[0],node));
	    }
	  
	} else {
	  
	  main_index --;
	  if (main_index < 0) break;
	  
	  path_node   [main_index+1] = NULL;
	  path_cindex [main_index+1] = 0;
	  node = path_node [main_index];
	}
      }
      
      for (int i=1; i< depth; i++) {
	path_node   [i] = NULL;
	path_cindex [i] = 0;
      }
    }
    delete [] coord;
    delete [] delta;

    if ( size != rec.size() ) {
      fprintf (stderr,
	       "AMR::fill_record_vector: precomputed length = %d, rec.size() = %zu\n",
	       size,rec.size());
      exit(1);
    }
  }
}

//------------------------------------------------------------------------------------------------------
// Interpolate function values to the given locations
// default values: renname="" and renvar=-1
// use renvar>=0 to write .ren file for the volume rendering for that variable
// add_var_marker: 0-functions stored inside DB, 1-iwlt stored inside DB
//------------------------------------------------------------------------------------------------------
void AMR::interpolate (const Value*const u, const Value*const du, const int nwlt, const int add_var_marker,
		       const Value*const x, Value* f_out, const int xsize, const int*const vars, const int vsize,
		       const bool grid_is_uniform, const int order, const char* renname, const int renvar)
{
#ifdef DATABASE_TLL_TREE_H
  if ((g->DIM_NUM != 2)&&(g->DIM_NUM != 3)) {
    fprintf (stderr, "AMR::interpolate() has been designed for dimensions 2 and 3 only\n");
    exit (1);
  }

  // open .ren file for the renderer if required
  FILE *fren;
  if (renvar >= 0)
    if ((fren = fopen (renname,"w")) == 0) {
      fprintf(stderr,  "Error: opening file: %s\n", renname);
      exit(1);
    }
  
  // count the number of points
  set_points_plot3d (0);
  Node **narray;
//  int *inarray;
  
//   if (add_var_marker==0) {
  narray = new Node* [size];           // some_node == narray[some_node->vid]
  if ( narray == 0 ) {
    fprintf(stderr,  "Error: not enougth memory in AMR::interpolate ()\n");
    exit (1);
  }
  // write pointers to nodes into the temporary array
  set_points_plot3d (2,0,0,narray,0);

//   } else {
//     inarray = new int [size];            // some_node == narray[some_node->vid]
//     if ( inarray == 0 ) {
//       fprintf(stderr,  "Error: not enougth memory in AMR::interpolate ()\n");
//       exit (1);
//     }
//     // write iwlt of the nodes into the temporary array
//     set_points_plot3d (3,0,0,0,inarray);
//   }

  
  int                                             // set Tll parameters
    namrt[DIM_MAX],                               // mxyz for Tll tree
    jmax,                                         // j_max for Tll tree
    dim,
    i_p[DIM_MAX];
  jmax = g->J_MAX - g->J_TREE;
#ifdef EBUG
  printf ("namrt = ");
#endif
  for ( dim=0; dim<g->DIM_NUM; ++dim ) {
    namrt[dim] = g->FINEST_MESH[dim] >> jmax;
#ifdef EBUG
    printf ("%d ",namrt[dim]);
#endif
  }
#ifdef EBUG
  printf ("\nFINEST_MESH[1..%d] = ",g->DIM_NUM);
  for ( dim=0; dim<g->DIM_NUM; ++dim ) printf ("%d ",g->FINEST_MESH[dim]);
  printf ("\nxx[1..%d].size() = ",g->DIM_NUM);
  for ( dim=0; dim<g->DIM_NUM; ++dim ) printf ("%d ",xx[dim].size());
  printf ("\nJ_TREE = %d\nJ_MAX = %d\n",g->J_TREE,g->J_MAX);
#endif

  std::vector< std::vector<float> > mx(g->DIM_NUM);  // xx array for level 0 only
#ifdef EBUG
  printf ("mx = ");
#endif
  for ( dim=0; dim<g->DIM_NUM; ++dim ) {
#ifdef EBUG
    printf ("( ");
#endif
    mx[dim].reserve(namrt[dim]+1);
    int x_space = 1 << jmax;
    for (int i=0; i<=namrt[dim]; ++i) {
      mx[dim].push_back (xx[dim][i*x_space]);
#ifdef EBUG
      printf ("%f ",mx[dim][i]);
#endif
    }
#ifdef EBUG
    printf (")");
#endif
  }
#ifdef EBUG
  printf ("\n");
#endif

  i_p[0] = 1;                                                     // i_p for Tll box tree
#ifdef EBUG
  printf ("i_p = 1 ");
#endif
  for ( dim=1; dim<g->DIM_NUM; ++dim ) {                          // which is
    i_p[dim] = i_p[dim-1]*                                        // 1,Mx,MxMy,etc
      ( namrt[dim-1] );
#ifdef EBUG
    printf ("%d ",i_p[dim]);
#endif
  }
#ifdef EBUG
  printf ("\n");
#endif

  tll_info.resize(jmax+1);                        // tll_info (as if read from tll_info file)
  tll_info_len.resize(jmax+1);

  set_cells (0,0,3);                              // compute tll_info, tll_cell

  int nolt = 0;
  for (int i=0; i<jmax+1; ++i) {
    nolt += tll_info_len[i];
#ifdef EBUG1
    printf ("tll_info[%d of %d][ ... of %d] =\t",i,jmax+1,tll_info_len[i]);
    if (i != jmax ) {
      for (int j=0; j<tll_info_len[i]; ++j)
	printf ("%d",(int) tll_info[i][j]);
    } else
      printf ("skipped zeros");
    printf ("\n");
    if (i == jmax) {  // printf whole tll_cell once
      printf ("\ntll_cell = [ ");
      for (int ji=0; ji<tll_cell.size()/g->TYPES_NUM; ++ji) {
	printf ("(#%d ",ji);
	for (int jj=0; jj<g->TYPES_NUM; ++jj) {
	  printf ("%d ",tll_cell[ji*g->TYPES_NUM+jj]);
	  printf ("{.");
	  for (int in=0; in<g->DIM_NUM; ++in)
	    printf ("%d.",narray[tll_cell[ji*g->TYPES_NUM+jj]]->ii[in]);
	  printf ("}");
	}
	printf (")");
      }
      printf ("\n");
    }                 // printf whole tll_cell once
#endif
  }

#ifdef EBUG
  printf ("nolt = %d, jmax = %d\n",nolt,jmax);
  printf ("size=%d, cells=%d\n",size,cells);
  printf ("tll_cell.size = %d\n",tll_cell.size());
#endif  
  
  
  // allocate AMR tree for a quick search
  bool allocate_iblock = 1;
  if (renvar >= 0)
    allocate_iblock = 0;
  Tll_tree *tll_tree = new Tll_tree( tll_info, tll_info_len, tll_cell, g->DIM_NUM, allocate_iblock );
  //  Node** box = new Node* [g->TYPES_NUM];           // bounding box for a point
  float* f_tmp, *df_tmp;
  if (order ==0)
    f_tmp = new float [g->TYPES_NUM];                  // temporal output for linear interpolation [2^{dim}-1]
  else {
    f_tmp = new float [g->TYPES_NUM*vsize];               // temporal output for high order interpolation
    df_tmp = new float [g->TYPES_NUM*vsize*g->DIM_NUM];   // temporal output for high order interpolation
  }
  int xyz[DIM_MAX];                                   // corresponding to fxyz integer coordinates
  std::vector< float > fxyz(g->DIM_NUM);              // real coordinates to interpolate into

  // deallocate some lists 
  tll_info.clear ();
  tll_info_len.clear ();
  final_box_list.clear ();
  box_list.clear ();
  next_box_list.clear ();
  
  // write .ren file for the renderer
  if (renvar >= 0) {
    char const c[] = "REN";
    fwrite ( c, sizeof(char), 3, fren );                    // 'REN'
    fwrite ( &g->DIM_NUM, sizeof(int), 1, fren );           //  dim
    fwrite ( namrt, sizeof(int), g->DIM_NUM, fren );        //  mxyz[dim]
    fwrite ( &nolt, sizeof(int), 1, fren );                 //  tree_length
    fwrite ( &cells, sizeof(int), 1, fren );                //  data_length/TYPES_NUM
    fwrite ( &jmax, sizeof(int), 1, fren );                 //  jmax
    fwrite ( tll_tree->cell, sizeof(Tll), nolt, fren );     //  tree[tree_length]
    float* fdata = new float [tll_cell.size()];
    float fmin, fmax, div=1.0;
    if (add_var_marker==0)
      fmin = fmax =
	narray[ tll_cell[0] ]->val[renvar];                          // functions stored in DB
    else
      fmin = fmax =
	u[ narray[tll_cell[0]]->ival[IWLT_I_P] + renvar*nwlt ];      // iwlt stored in DB
    
    for (int i=0; i<tll_cell.size(); ++i) {
      if (add_var_marker==0)
	fdata[i] = narray[ tll_cell[i] ]->val[renvar];
      else
	fdata[i] = u[ narray[tll_cell[i]]->ival[IWLT_I_P] + renvar*nwlt ];
      
      if (fdata[i] > fmax) fmax = fdata[i];
      if (fdata[i] < fmin) fmin = fdata[i];
    }
    div /= (fmax-fmin);
    for (int i=0; i<tll_cell.size(); ++i)
      fdata[i] = (fdata[i] - fmin)*div;
    fwrite ( fdata, sizeof(float), tll_cell.size(), fren ); //  data[data_length]
    delete [] fdata;
  }

  
  // skip the interpolation part for .ren preprocessing
  if (renvar < 0)
    {
      int tree_size = 1 << jmax;
      
      int ii = 0, iii = 0, tmp;
      float
	inv_spacing [DIM_MAX],                                   // uniform mesh spacing
	zero[DIM_MAX];                                           // '0' corner real coordinates
      for ( dim=0; dim<g->DIM_NUM; ++dim ) {
	inv_spacing[dim] =  1.0 / ( xx[dim][1] - xx[dim][0] );
	zero[dim] = xx[dim][0];
      }
      
#ifdef EBUG
      printf ("\tgrid_is_uniform == %d\n",grid_is_uniform);
#endif
      
      for (int ixsize=0; ixsize<xsize; ++ixsize) {
	
	int tll_tree_number = 0;
	
#ifdef EBUG
	printf ("x = ");
#endif
	for ( dim=0; dim<g->DIM_NUM; ++dim, ++ii ) {
	  fxyz[dim] = x[ii];                                          // copy coordinate vector
	  if (grid_is_uniform)
	    xyz[dim] = (int) (inv_spacing[dim] * (x[ii]-zero[dim]));    // get uniform integer coordinate
	  else                                                          // non-uniform ...
	    xyz[dim] = locate_in_array 
	      ( fxyz[dim], xx[dim], g->FINEST_MESH[dim] );
#ifdef EBUG
	  printf ("%f(%d <-- %f) ",x[ii],xyz[dim],inv_spacing[dim] * (x[ii]-zero[dim]));
#endif
	  
	  // locate tree for the current dimension
	  tmp = 0;
	  for (int i=1; i<mx[dim].size(); ++i) {
	    if (x[ii] <= mx[dim][i]) {
#ifdef EBUG
	      printf ("{mx=%f}",mx[dim][i]);
#endif
	      tmp = i-1;
	      break;
	    }
	  }
	  tll_tree_number += tmp * i_p[dim];
	  // ammend integer coordinate for the case of upper tree boundary
	  if ( (tmp+1)*tree_size == xyz[dim] ) {
	    xyz[dim] --;
#ifdef EBUG
	    printf ("/ammended for the upper boundary to %d\n",xyz[dim]);
#endif
	  }
	}
#ifdef EBUG
	printf ("\n\ttll_tree_number = %d\n",tll_tree_number);
#endif
	int ci;
	ci = tll_tree->get_cell ( tll_tree_number, xyz );
	
	
#ifdef EBUG
	printf ("\tdata index  %d <-> %d\n",ci,tll_tree->iblock [ci*g->TYPES_NUM]);
#endif
	Node* box[1<<DIM_MAX];
	for (int i=0; i<g->TYPES_NUM; ++i) {
	  box[i] = narray [ tll_tree->iblock [ci*g->TYPES_NUM + i] ];
#ifdef EBUG
	  printf ("/%d+%d/",tll_tree->iblock [ci*g->TYPES_NUM + i], box[i]->ii[0]);
#endif
	}
	
	
#ifdef EBUG
	printf ("\n");
	printf ("box: [ ");
	for (int i=0; i<g->TYPES_NUM; ++i) {
	  printf ("( ");
	  for (int dim=0; dim<g->DIM_NUM; ++dim)
	    printf ("%d ",box[i]->ii[dim]);
	  printf (")");
	}
	printf ("]\n");
	for (int i=0; i<g->TYPES_NUM; ++i)
	  printf ("%d ",box[i]->ival[IWLT_I_P]);
	printf ("\n");
	printf ("f->test_amr_node=%d\n",f->test_amr_box ( box, box[1]->ii[0]-box[0]->ii[0], fxyz, xx, true));
	printf ("u(1:%d,1) = [ ",nwlt);
// 	 for (int i=0; i<g->TYPES_NUM; ++i) {
// 	  if (
// 	  printf ("%f ",u[box[i]->ival[IWLT_I_P]]);
// 	}
	printf ("] \n");
	printf ("nwlt=%d\n",nwlt);
#endif
	
	// second variable to be deleted from f->interpolate_to_box()
	if (order == 0)
	  f->interpolate_to_box ( u, nwlt, add_var_marker, box, f_tmp, vars, vsize, fxyz, xx,
				  iii, f_out );
	else
	  f->interpolate_to_box_h (u, du, nwlt, add_var_marker, box, f_tmp, df_tmp, vars, vsize, fxyz, xx,
				   iii, f_out);
	
	
	//exit (1);
      } // of for (int ixsize=0; ixsize<xsize; ++ixsize)
      
      delete [] f_tmp;
      if (order != 0)
	delete [] df_tmp;
      
    } // of if (renvar < 0)
  
  
  // deallocate all the lists
  //  delete [] box;
  tll_cell.clear ();
  delete [] narray;
  delete tll_tree;
  size = 0;
  cells = 0;

  // close .ren file for the renderer
  if (renvar >= 0)
    fclose (fren);

#else /* DATABASE_TLL_TREE_H */
  fprintf (stderr, "To use AMR::interpolate () define USE_TLL_TREE while compiling\n");
  exit (1);
#endif /* DATABASE_TLL_TREE_H */
}

// -----------------------------------------------------------------------------------------
// return index in array
int AMR::locate_in_array (const float& f, const std::vector<float>& xx, const int& xxmax) {

#ifdef EBUG1
  printf ("xx[0..%d] = ",xxmax);
  for (int i=0; i<=xxmax; ++i)
    printf ("%f ",xx[i]);
#endif

  int min=0, max=xxmax;
  while ( max - min > 1) {
    int middle = (max+min)/2;
    if ( f < xx[middle] )
      max = middle;
    else
      min = middle;
  }
#ifdef EBUG1
  printf ("\n%f gives # %d\n",f,min);
#endif
  return min;
}

// -----------------------------------------------------------------------------------------
// Write 1D VTK file
void AMR::write_amr_1d_vtk (const char* c, const char* file_type) {
  std::string filename = c;
  filename += ".vtk";                         // VTK extension
  const bool
    is_binary = ( file_type[0] == 'b' );
  
  FILE *f;
  if ((f = fopen (filename.c_str(),"w")) == 0) {
    fprintf(stderr,  "Error: opening file: %s\n", filename.c_str());
    exit(1);
  }
  printf ("Opening ");
  if ( is_binary ) printf ("binary ");
  else             printf ("ASCI ");
  printf ("file %s ... ",filename.c_str());
  fflush (stdout);
  
  fprintf (f, "# vtk DataFile Version 3.0\n");            // write VTK header
  fprintf (f,  "# %s\n",string_header.c_str());           // ...
  if ( is_binary )
    fprintf (f,  "BINARY\n");                             // ...
  else
    fprintf (f, "ASCII\n");                               // ...
  
  // this part is 1D specific ----------
  
  printf ("allocating ... ");
  fill_record_vector ();
  printf ("sorting ... ");
  std::sort (rec.begin(), rec.end());
  printf ("writing ... ");
  cells = (val_num + val_num_i);                    // one line per each variable,
  size = rec.size();                                      // each line contains all the points
  

  fprintf (f, "DATASET UNSTRUCTURED_GRID\n");             // ...  
  printf ("points ... ");
  fflush (stdout);
  fprintf (f,  "POINTS %10d float\n", size*cells);        // Assume real coordinates for 1D
  
  set_points_1d_vtk (f, is_binary);                       // write 1D VTK points
  
  printf ("line ... ");
  fflush (stdout);
  
  set_cells_1d_vtk (f, is_binary);                        // write VTK cells (polygonal line)
  
  // -----------------------------------
  
  fclose (f);
  printf ("done\n");
  fflush (stdout);
  // Set internal AMR state ready for another writing
  size = 0;
  cells = 0;
}


//
// write VTK points for 1D data
//
void AMR::set_points_1d_vtk (FILE *f, const bool b) {
  int i;
  float tmp[3] = {0.0};                                  // zero Z
  
  for ( i=0; i<cells; ++i)
    for (itr = rec.begin(); itr != rec.end(); ++itr) {
      
      if (COORD_INTEGER)
	tmp[0] = (float) itr->node->ii[0];               // integer coord X
      else
	tmp[0] = get_coord ( itr->node->ii,0 );          // real coord X
      if ( i < val_num )
	tmp[1] = itr->node->val[i];                      // real function Y
      else
	tmp[1] = (float) itr->node->ival[i-val_num];  // integer function Y
      
      if (b) {
	if ( ! IS_BIG_ENDIAN) {
	  tmp[0] = FloatSwap( tmp[0] );
	  tmp[1] = FloatSwap( tmp[1] );
	}
	fwrite (tmp, sizeof(float), 3, f);
      } else
	fprintf (f,  "%f %f %f ", tmp[0], tmp[1], tmp[2] );
    }
}


//
// write VTK cells and lookup_table for 1D
//
void AMR::set_cells_1d_vtk (FILE *f, const bool b) {
  
  int i, // variable number
    j;   // point number
  

  fprintf (f,  "\nCELLS %10d %10d\n", cells, cells*(size+1));
  
  int bt = htonl(4),      // VTK_POLY_LINE
    len = htonl(size);    // number of points in a line
  
  for ( i=0; i<cells; ++i) {                      // one line per each variable
    if (b)                                        // print number of points in the line
      fwrite (&len, sizeof(int), 1, f);
    else
      fprintf (f, "%d ",size);
    
    for ( j=0; j<size; ++j )                      // print the points
      if (b) {
	int num = htonl(j);
	fwrite (&j, sizeof(int), 1, f);
      } else
	fprintf (f, "%d ",j);
  }

  fprintf (f,  "\nCELL_TYPES %d\n", cells);       // write cell types
  if (b)
    for ( i=0; i<cells; ++i )
      fwrite (&bt, sizeof(int), 1, f);
  else
    for ( i=0; i<cells; ++i )
      fprintf (f, "4 ");
  
  fprintf (f,  "\nCELL_DATA %d\n", cells);
  fprintf (f,  "POINT_DATA %d", size);            // write lookup_table
  fprintf (f,  "\nSCALARS data_1D int\n");
  fprintf (f,  "LOOKUP_TABLE default\n");
  if (b)
    for ( i=0; i<cells; ++i ) {                   // each cell will be of
      int bi = htonl(i);                          // its own color
      for ( j=0; j<size; ++j )
	fwrite (&bi, sizeof(int), 1, f);
    }
  else
    for ( i=0; i<cells; ++i )
      for ( j=0; j<size; ++j )
	fprintf (f, "%d ",i);
  
  
}


//
// Write 1D CGNS file
//
void AMR::write_amr_1d_cgns (const char* c) {
  
#ifndef CGNS_LIB
  printf ("The code has not been compiled to support CGNS format.\n");
  printf (" Please update machine specific makefile to provide\n");
  printf (" the correct path to CGNS library and recompile the code.\n");
  exit (1);
#else // CGNS_LIB
  
  std::string cgns_name = c;       // generic file name
  cgns_name += ".cgns";
  int index_file,                  // CGNS file index
    index_base,                    // CGNS base index
    index_zone,                    // CGNS zone index
    index_coord,                   // CGNS coordinate index
    index_section,                 // CGNS section index
    index_solution,                // CGNS solution index
    cell_dim,                      // Dimension of the cells; 3 for volume cells, 2 for surface cells.
    phys_dim;                      // Number of coordinates required to define a vector in the field
  char *basename = "Base",
    *zonename = "Zone";
  
  printf ("Opening file %s ... ",cgns_name.c_str());
  fflush (stdout);
  if (cg_open( cgns_name.c_str(), MODE_WRITE, &index_file ))
    cg_error_exit();
  
  cell_dim = phys_dim = g->DIM_NUM;
  if (cg_base_write( index_file, basename, cell_dim, phys_dim, &index_base ))
    cg_error_exit();

  printf ("allocating ... ");
  fill_record_vector ();
  printf ("sorting ... ");
  std::sort (rec.begin(), rec.end());
  printf ("writing ... ");
  cells = (val_num + val_num_i);                    // one line per each variable,
  size = rec.size();                                      // each line contains all the points

  float *arrayf = (float*) calloc ( size, sizeof(float) );
  float *variables = (float*) malloc ( sizeof(float)*size*cells );
  if ( (arrayf == 0) || (variables == 0) ) {
    fprintf(stderr,  "Error: not enougth memory for CGNS writing\n");
    exit (1);
  }
  
  // write coordinates and function values into temporary arrays
  int i = 0;
  for (itr = rec.begin(); itr != rec.end(); ++itr, ++i) {
    if (COORD_INTEGER)
      arrayf[i] = (float) itr->node->ii[0];               // integer coord X
    else
      arrayf[i] = get_coord ( itr->node->ii,0 );          // real coord X
    for (int varnum=0; varnum<cells; ++varnum)
      if ( varnum < val_num )
	variables[i+size*varnum] = itr->node->val[varnum];          // real function Y
      else
	variables[i+size*varnum] = (float) itr->node->ival[varnum-val_num];  // integer function Y
  }
  

  // compute and write connectivity information
  ielem = new int [2*(size-1)];
  if (!ielem) {
    fprintf(stderr,  "Error: not enougth memory for CGNS connectivity\n");
    exit (1);
  }
  for ( i=1; i<size; ++i) {
    ielem[2*i-2]   = i;
    ielem[2*i-1] = i+1;
  }
  

  int *isize = (int*) calloc ( 3*g->DIM_NUM, sizeof(int) );
  for ( i=0; i<g->DIM_NUM; ++i) {
    isize[3*i+0] = size;
    isize[3*i+1] = size-1;
    isize[3*i+2] = 0;
  }
  
  if (cg_zone_write( index_file, index_base, zonename, isize, Unstructured, &index_zone ))
    cg_error_exit();
  
  if (cg_coord_write( index_file, index_base, index_zone, RealSingle, "CoordinateX", &arrayf[0], &index_coord ))
    cg_error_exit();
  
  ElementType_t element_type = BAR_2;
  
  if (cg_section_write( index_file, index_base, index_zone, "Cell", element_type, 1, size-1, 0, ielem, &index_section ))
    cg_error_exit();
  
  if (cg_sol_write( index_file, index_base, index_zone, "Solution", Vertex, &index_solution ))
    cg_error_exit();

  for ( i=0; i<val_num+val_num_i; ++i) {
    int index_field;
    if (cg_field_write( index_file, index_base, index_zone, index_solution, RealSingle, data_names[i].c_str(), variables, &index_field ))
      cg_error_exit();
  }
  
  if (cg_close( index_file ))
    cg_error_exit();
  
  
  printf ("done\n");
  fflush (stdout);
  // Set internal AMR state ready for another writing
  size = 0;
  cells = 0;
  free (isize);
  free (arrayf);
  free (variables);
  delete [] ielem;
  ielem = 0;
#endif // CGNS_LIB
}
