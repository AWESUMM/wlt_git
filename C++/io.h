/*------------------------------------------------------*/
/*                                                      */
/*   Endian independent I/O library for AWCM code       */
/*                                                      */
/*   by Alexei Vezolainen                               */
/*   Jan 2007                                           */
/*------------------------------------------------------*/
#ifndef AWCM_RES_IO
#define AWCM_RES_IO

#ifdef _MSC_VER                // it is MSVC compiler --------
typedef signed __int64 int64_t;
typedef int int32_t;
typedef short int16_t;
typedef char int8_t;
typedef unsigned char uint8_t;
#define snprintf _snprintf
#else                          // a normal compiler ----------
#include "stdint.h"
#endif                         // ----------------------------

typedef double r64_t;           // REAL*8        'R8'
typedef float r32_t;            // REAL*4        'R4'
typedef int64_t i64_t;          // INTEGER*8     'I8'
typedef int32_t i32_t;          // INTEGER*4     'I4'
typedef int16_t i16_t;          // INTEGER*2     'I2'
typedef int8_t i8_t;            // INTEGER*1     'I1'
typedef uint8_t ui8_t;          // CHARACTER     'I1'


// uppend underscore or set lowercase if necessary
#include "lowercase.h"


#define INITIAL_HDR_LEN 512     // initial header length
#define RES_VERSION 1           // version to be used as char header_g_t::VERSION [2]
#define RES_MAGIC_STRING "RES"  // file type marker, char header_g_t::MAGIC_STRING [4]
#define RES_MAGIC_STRING_LEN 3  // length of the marker

/*---------------------------------------------------------------------------*/
/*   BEGIN of functions for FORTRAN                                          */
/*                                                                           */
extern "C" {
  /* open file for reading/writing
     CALL RES_OPEN( mode, filename, LEN(filename), ierror )
     mode          -  CHARACTER*1   - read/write mode ('r', 'R', 'w', or 'W')
     filename      -  CHARACTER*(*) - some name, i.e. 'output.res'
     LEN(filename) -  INTEGER*4     - length of FILENAME, LEN(FILENAME)
     ierror        -  INTEGER*4     - output (nonzero if errors happened)
  */
  void RES_OPEN (const char*, const char*, const i32_t*, i32_t*);
  
  
  /* read data from the file
     CALL RES_READ( type, name, count )
     type          - CHARACTER*2    - variable type ('I1', 'i1', ..., 'R4', 'r4', ...)
     name          -                - variable name to read
     count         - INTEGER*4      - number of variables (1 for single, >1 for array)
  */
  void RES_READ (const unsigned char*, void*, const i32_t*);

  
  /* write data into the file
     CALL RES_WRITE( type, name, count )
     type          - CHARACTER*2    - variable type ('I1', 'i1', ..., 'R4', 'r4', ...)
     name          -                - variable name to read
     count         - INTEGER*4      - number of variables (1 for single, >1 for array)
  */
  void RES_WRITE (const unsigned char*, const void*, const i32_t*);


  /* close the file, clean the memory
     CALL RES_CLOSE( ierror )
     ierror        -  INTEGER*4     - output (nonzero if errors happened)
  */
  void RES_CLOSE (i32_t*);
}
/*   END of functions for FORTRAN                                            */
/*---------------------------------------------------------------------------*/


class awcm_res_io {
 public:
  bool OPEN_FOR_WRITE;                    // if open for write or read
  bool FILE_IS_OPEN;                      // if all initializations passed
  bool SWAP_FILE_DATA;                    // if the file data has to be swapped
  bool IS_BIG_ENDIAN;                     // if the native system is big endian
  FILE* f;
  i64_t header_after;                     // position of the header info
  
  awcm_res_io ();
  ~awcm_res_io ();
  i32_t rw_header (const unsigned char);    // called from RES_OPEN
  i32_t w_header ();                        // called from RES_CLOSE
  void add_header (const unsigned char, const i64_t&, const i64_t&);        // write variable header token
  void compare_to_header (const unsigned char, const i64_t&, const i64_t&); // compare to variable header token
 private:
  union header_g_t {
    char all [32];
    struct {                              // file header: general section
      char MAGIC_STRING [4];
      char VERSION [2];
      char IS_BIG [2];
      char SIZEOF_I8     [4];
      char SIZEOF_I16    [4];
      char SIZEOF_I32    [4];
      char SIZEOF_I64    [4];
      char SIZEOF_FLOAT  [4];
      char SIZEOF_DOUBLE [4];
    } part;
  } header_g, header_g_loaded;               // native and from file
  unsigned char* header_c, *header_c_loaded; //              data descriptor
  i64_t*  header_i, *header_i_loaded;          //              data length
  i64_t header_len, header_len_loaded;         //              maximum length
  i64_t header_cur, header_cur_loaded;         //              current length
};


#endif // AWCM_RES_IO
