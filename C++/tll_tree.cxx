/*----------------------------------------------------------------*/
/*     Tree Structured Database                                   */
/*                                                                */
/*     Developed by Alexei Vezolainen                             */
/*                                                                */
/*     Please respect all the time and work                       */
/*     that went into the development of the code.                */
/*                                                                */
/*----------------------------------------------------------------*/
#include <string>
#include "stdio.h"
#include "stdlib.h"
#include "tll_tree.h"


// -------------------------------------------------------------------------------------
// (uniform grid)
// return data index for the cell containing the given coordinates
int Tll_tree::get_cell ( const int& b_, const int* xyz ) {
  
  // which cell of level 0 we are in
  int root = cell [b_].get_p ();
  if ( cell [b_].is_data() )
    return root;
  
  int unit = 1 << jmax;

  // walk along the tree
  for (int i=jmax-1; i>=0; --i) {
    unit >>= 1;                     // we are at the beginning of the next level group

#ifdef EBUG1
    printf ( "\t / UNIFORM / i=%d, root=%d, unit=%d\n",i,root,unit);
    int chi=0;
#endif

    for (int dim=0; dim<dimension; ++dim) {       //  find child index
      root += ((bool)( xyz[dim] & unit )) << dim;
      //root += ( xyz[dim] & unit ) >> (i-dim);

#ifdef EBUG1
      chi += ((bool)( xyz[dim] & unit )) << dim;
      //chi += ( xyz[dim] & unit ) >> (i-dim);
      printf (" + %d +",xyz[dim]);
#endif
    }
    
#ifdef EBUG1
    printf ( "\nroot+chi=%d, chi=%d\n",root,chi);
#endif    

    if ( cell [root].is_data() )
      return cell [root].get_p ();
    
    root = cell [root].get_p ();
  }

#ifdef EBUG
  printf ("error in get_cell()\n");
  exit (1);
#endif
  return 0;
}

// //
// // alocate all, read _t.dat  _d.dat
// //
// Tll_tree::Tll_tree (const char*fn1, const char*fn2) {
//   FILE *of1, *of2;
//   if ( VERB )
//     printf ("reading ...");
//   if ((of1 = fopen (fn1,"rb")) == 0) err ("Error: reading: ",fn1 );
//   if ((of2 = fopen (fn2,"rb")) == 0) err ("Error: reading: ",fn2 );
//   err ("*");

//   // read globals
//   fread ( m,sizeof(int),3,of1 );          // Mx, My, Mz
//   fread ( &nolt,sizeof(int),1,of1 );      // total tree storage length
//   fread ( &nd,sizeof(int),1,of1 );        // data storage length
//   int nols_r;
//   fread ( &nols_r,sizeof(int),1,of1 );    // size of nol array
//   if (nols_r != nols) err ("Error: nols array size mismatch");
//   err ("*");
  
//   // allocate
//   cell = (Tll*) malloc ( sizeof(Tll)*nolt );
//   block = (Bll*) malloc ( sizeof(Bll)*nd );
  
//   // read nol array, tree and data
//   fread ( &nol,sizeof(int),nols,of1 );    // nol array
//   fread ( cell,sizeof(Tll),nolt,of1 );    // tree
//   fread ( block,sizeof(Bll),nd,of2 );     // data
//   fclose (of1);
//   fclose (of2);
  
//   if ( VERB )
//     printf ("\ntree length = %d, data length = %d\n",nolt,nd);
// }

//
// alocate all, read _t.dat  _d.dat
//
Tll_tree::Tll_tree (const std::vector< std::vector<bool> > & tll_info,
		    const std::vector< int > & tll_info_len,
		    const std::vector< int > & tll_cell,
		    const int dim, const bool allocate_iblock ) :
  nolt(0), nd(0), chnum(2), dimension(dim), cell(0), iblock(0)
{
  int i;
  for ( i=1; i<dim; ++i)                              // number of children
    chnum <<= 1;
  
  for ( i=0; i<tll_info.size(); ++i)                  // tree length
    nolt += tll_info_len[i];

#ifdef EBUG
  printf ("Tll_tree(): nolt=%d, chnum=%d, dim=%d\n",nolt,chnum,dim);
#endif
  cell = (Tll*) malloc ( sizeof(Tll)*nolt );             // allocate AMR tree
  
  int ng = 0;                        // cell under consideration, global index
  int l = 0;                         // level under consideration
  int nn = tll_info_len[0];          // index in the next level array
  int l_min_1 = tll_info.size()-1;
  for (int lev=0; lev<l_min_1; ++lev) {
    //printf ("lev=%d of %d\n ",lev,tll_info.size());
    for (int pos=0; pos<tll_info_len[lev]; ++pos) {
      //printf ("c[%d][%d] = %d\n",lev,pos,tll_info[lev][pos]);
      switch (tll_info[lev][pos])
	{
	case false:       // no subdivision
	  cell [ng].set_p (nd);
	  cell [ng].set_data (true);                // this cell points to data
	  ng++;
	  nd++;
	  break;
	case true:       // create children
	  cell [ng].set_p (nn);
	  cell [ng].set_data (false);               // this cell has children
	  ng++;
	  nn += chnum;
	  break;
	}
    }
    l++;                // next level
  }
 
  jmax = l_min_1;

  for (int pos=0; pos<tll_info_len[l_min_1]; ++pos) {    // all the cells
    cell [ng].set_p (nd++);                                // of the last level
    cell [ng++].set_data (true);                           // point to data
  }
  
#ifdef EBUG1
  printf ("\nTree:\n");
  int count=0;
  int beg = 0;
  int lev = 0;
  for (int lev=0; lev<tll_info.size(); ++lev) {
    printf ( " %d level, %d nodes\n",lev,tll_info_len[lev] );
    for ( int i=0; i<tll_info_len[lev]; ++i ) {
      if ( cell [i+beg].is_data () )
	printf ( " %d(d %d)",count,cell [i+beg].get_p () );
      else
	printf ( " %d(* %d)",count,cell [i+beg].get_p () );
      count++;
    }
    beg += tll_info_len[lev];
    printf ("\n");
  }
  printf ("\n");
#endif

  if (allocate_iblock) {
    iblock = (int*) malloc ( sizeof(int)*nd*chnum );    // allocate data blocks
    
    // copy data for rendering according to cell info
    int ind_max = nd*chnum;
    for ( int ind=0; ind<ind_max; ++ind )                // loop trough each data block
      iblock [ind] = tll_cell[ind];
  }

}

//
// deallocate all
//
Tll_tree::~Tll_tree () {
  //free (block);
  free (iblock);
  free (cell);
}




// //
// // write AMR tree into two files: tree, data
// //
// void Tll_tree::write_tree (char*fn1, char*fn2) const {
//   FILE *of1, *of2;
//   if ( VERB )
//     printf ("writing ...");
//   if ((of1 = fopen (fn1,"wb")) == 0) err ("Error: writing: ",fn1 );
//   if ((of2 = fopen (fn2,"wb")) == 0) err ("Error: writing: ",fn2 );
//   err ("*");
  
//   // globals
//   fwrite ( m,sizeof(int),3,of1 );          // Mx, My, Mz
//   fwrite ( &nolt,sizeof(int),1,of1 );      // total tree storage length
//   fwrite ( &nd,sizeof(int),1,of1 );        // data storage length
//   int nols_w = nols;
//   fwrite ( &nols_w,sizeof(int),1,of1 );    // size of nol array
//   fwrite ( &nol,sizeof(int),nols,of1 );    // nol array
//   // tree and data
//   fwrite ( cell,sizeof(Tll),nolt,of1 );    // tree
//   fwrite ( block,sizeof(Bll),nd,of2 );     // data
//   fclose (of1);
//   fclose (of2);
  
//   if ( VERB )
//     printf ("\ntree length = %d, data length = %d\n",nolt,nd);
// }

// //
// // print real data
// //
// void Tll_tree::print_real_data () const {
//   printf ("Data:\n");
//   for ( int i=0; i<np; ++i )
//     printf ( " %f",data[i] );
//   printf ("\n");
// }

// //
// // print AMR data
// //
// void Tll_tree::print_data () const {
//   printf ("Data:\n");
//   for ( int i=0; i<nd; ++i ) {
//     printf ( " %d (",i );
//     for ( int j=0; j<CHNUM; ++j )
//       printf ( " %f",block[i].b[j] );
//     printf (" )\n");
//   }
//   printf ("\n");
// }


// print the tree structure
// void Tll_tree::print_tree () const {
//   printf ("\nTree:\n");
//   int count=0;
//   int beg = 0;
//   int lev = 0;
//   while ( nol[lev] ) {
//     printf ( " %d level, %d nodes\n",lev,nol[lev] );
//     for ( int i=0; i<nol[lev]; ++i ) {
//       if ( cell [i+beg].is_data () )
// 	printf ( " %d(d %d)",count,cell [i+beg].get_p () );
//       else
// 	printf ( " %d(* %d)",count,cell [i+beg].get_p () );
//       count++;
//     }
//     beg += nol[lev];
//     lev++;
//     printf ("\n");
//   }
//   printf ("\n");
// }

// //
// // set nolt value, print statistics if necessary
// //
// void Tll_tree::set_statistics () {
//   int lev = 0;
//   nolt = 0;
//   if ( VERB ) {
//     while ( nol[lev] ) {
//       nolt += nol[lev];
//       printf ( "%d ", nol[lev++] );
//     }
//     printf ("\n");
//   } else
//     while ( nol[lev] )
//       nolt += nol[lev++];
// }

// //
// // print error message, terminate the program if necessary
// //
// void Tll_tree::err (const char c[], const char c1[]) const {
//   static int err_num = 0;
//   if (c[0]=='*') {
//     if (err_num) exit(1);
//   } else {
//     fprintf (stderr, "%s%s\n",c,c1);
//     err_num ++;
//   }
// }
