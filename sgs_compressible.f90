!
! SGS models for compressible flows
!

MODULE SGS_compressible

  USE precision
  USE wlt_vars
  USE wavelet_filters_mod
  USE wlt_trns_mod
  USE wlt_trns_vars
  USE wlt_trns_util_mod
  USE io_3d_vars
  USE util_mod
  USE util_vars
  USE share_consts
  USE pde
  USE sizes
  USE share_kry
  USE vector_util_mod
  USE input_file_reader
  USE equations_compressible 
  USE sgs_compressible_keqn 
  USE SGS_compressible_SM
  USE SGS_compressible_AMD
  USE sgs_util 
  USE error_handling

  PUBLIC :: &
    sgs_compress_read_input, &
    sgs_compress_setup_pde, &              ! Set up the variable counts
    sgs_compress_finalize_setup_pde, &     ! Set up the pde variable mappings (adapt, interpolate, save, etc.)
    sgs_compress_model_setup, &
    sgs_compress_rhs, &
    sgs_compress_drhs, &
    sgs_compress_drhs_diag, &
    sgs_compress_additional_vars, &
    sgs_compress_force

  PRIVATE :: &
    calc_fixed_eddy_visc, &              ! Calculate fixed coefficient smagorinsky eddy viscosity 
    calc_RBEVM_eddy_visc, &              ! Calculate residual-based eddy viscosity 
    calc_NS_momentum_residual 

  ! Eddy viscosity
  LOGICAL, PRIVATE :: save_eddy_visc = .FALSE., save_SGSD = .FALSE., save_diss_ratio = .FALSE., save_delta = .FALSE.
  INTEGER, PRIVATE :: n_var_mu_t, n_var_mu_ratio, n_var_kk_ratio, n_var_bD_ratio, n_var_delta 

  ! Constant Smagorinsky Coefficients
  REAL (pr), PRIVATE :: &
    Pra_t_in        = 0.0_pr, &
    Sc_t_in         = 0.0_pr

  ! Residual-based eddy viscosity 
  REAL (pr), PRIVATE :: Kolmog_const, modeled_grid_ratio

! Model numbers
  INTEGER, PARAMETER :: sgsmodel_compress_RBEVM  = -1 ! 2 - Residual based eddy viscosity model (Oberai 2014), not ready 
  INTEGER, PROTECTED :: sgsmodel_compress = 0
  INTEGER, PARAMETER :: sgsmodel_compress_none   = 0 ! 0 - none 
  INTEGER, PARAMETER :: sgsmodel_compress_fixSM  = 1 ! 1 - fixed parameter Smagorinsky (fixSM)
  INTEGER, PARAMETER :: sgsmodel_compress_AMD    = 2 ! 2 - Anisotropic Minimum Dissipation (Rozema 2015) 
  INTEGER, PARAMETER :: sgsmodel_compress_ksgs   = 3 ! 3 - Dynamic k-equation (Chai 2012) 

CONTAINS

  !
  ! Read input from "case_name"_pde.inp file
  ! case_name is in string file_name read from command line
  ! in read_command_line_input()
  ! 
  SUBROUTINE sgs_compress_read_input()
    IMPLICIT NONE

    IF (par_rank.EQ.0) THEN
       PRINT *, '********************** Reading Compressible SCALES Parameters *****************************'
    END IF

    sgsmodel_compress = 0
    call input_integer ('sgsmodel_compress',sgsmodel_compress,'default',' sgsmodel_compress: Selection of compressible SGS model to use.  See sgs_compressible.f90 for options.') 

    IF ( dim .LT. 3 .AND. sgsmodel_compress .NE. sgsmodel_compress_none ) CALL error("Cannot use compressible LES models for dimensionality other than 3")

    ! Set flag from compressible equations module over whether to use the eddy viscosity ERIC: is there a more elegant way to deal with this?
    IF ( sgsmodel_compress .EQ. sgsmodel_compress_fixSM .OR. & 
          sgsmodel_compress .EQ. sgsmodel_compress_RBEVM .OR. & 
          sgsmodel_compress .EQ. sgsmodel_compress_AMD .OR. & 
          sgsmodel_compress .EQ. sgsmodel_compress_ksgs ) THEN
      use_comp_eddy_visc = .TRUE.
    END IF

    IF ( sgsmodel_compress .GT. 0 ) THEN

      ! All Eddy Viscosity 
      IF( use_comp_eddy_visc ) THEN 
        call input_logical ('save_eddy_visc',save_eddy_visc,'stop','save_eddy_visc: save the eddy viscosity (mu_T)') 
        call input_logical ('save_SGSD',save_SGSD,'stop','save_SGSD: save the subgrid scale dissipations (SijTij, QkRk, DkSk)') 
        call input_logical ('save_diss_ratio',save_diss_ratio,'stop','save_diss_radio: save modeled to physical dissipation ratios')
        call input_logical ('save_delta',save_delta,'stop','save_delta: save filter width for post processing')
      END IF

      ! Fixed Smagorinsky
      IF ( sgsmodel_compress .EQ. sgsmodel_compress_fixSM ) THEN
        CALL fixSM_compress_read_input 
      END IF

      ! Anisotropic Minimum Dissipation 
      IF ( sgsmodel_compress .EQ. sgsmodel_compress_AMD ) THEN
        CALL AMD_compress_read_input 
      END IF

      ! RBEVM 
      IF ( sgsmodel_compress .EQ. sgsmodel_compress_RBEVM ) THEN
        call input_real ('Kolmog_const',Kolmog_const,'stop',' Kolmog_const: Kolmogorev constant in the modeled coefficient computation' ) 
        call input_real ('modeled_grid_ratio',modeled_grid_ratio,'stop','modeled_grid_ratio : Ratio of resolution to resolution where u fluctuations are calculated' ) 

        ! Coefficients for condictivity and diffusivity
        call input_real ('Pra_t_in',Pra_t_in,'stop', &
          ' Pra_t_in ! Turbulent Prandtl number (for set coefficient)')

        call input_real ('Sc_t_in',Sc_t_in,'stop', &
          ' Sc_t_in ! Turbulent Schmidt number (for set coefficient)')
      END IF
      
      ! k-eqn based model 
      IF ( sgsmodel_compress .EQ. sgsmodel_compress_ksgs  ) THEN
        CALL keqn_compress_read_input 
      END IF


    END IF
    IF (par_rank.EQ.0) THEN
       PRINT *, '****************************************************************************************'
    END IF
  END SUBROUTINE sgs_compress_read_input

   !  Setup the compressible pde equations, registering all of the variables
   SUBROUTINE  sgs_compress_setup_pde () 
      USE variable_mapping
      IMPLICIT NONE
      
      IF (par_rank.EQ.0) THEN
        PRINT * ,''
         PRINT *, '******************* Setting up PDE ******************'
         PRINT * ,'            USING COMPRESSIBLE SCALES MODEL'
         PRINT *, '*****************************************************'
      END IF

      IF( use_comp_eddy_visc .AND. save_eddy_visc ) THEN 
        CALL register_var( 'Mu_turb',  integrated=.FALSE.,  adapt=(/.FALSE.,.FALSE./),   interpolate=(/.FALSE.,.FALSE./), &
            exact=(/.FALSE.,.FALSE./),   saved=.TRUE., req_restart=.FALSE. )
      END IF

      IF( use_comp_eddy_visc .AND. save_delta ) THEN 
        CALL register_var( 'Delta',  integrated=.FALSE.,  adapt=(/.FALSE.,.FALSE./),   interpolate=(/.FALSE.,.FALSE./), &
            exact=(/.FALSE.,.FALSE./),   saved=.TRUE., req_restart=.FALSE. )
      END IF

      IF ( sgsmodel_compress .EQ. sgsmodel_compress_ksgs ) THEN
        ! k_sgs 
        CALL keqn_compress_setup_pde  
      END IF

      ! Save eddy viscosity dissipation ratios.  Not applicable non eddy-viscosity models.
      IF( use_comp_eddy_visc .AND. save_diss_ratio ) THEN
        CALL register_var( 'Mu_ratio',  integrated=.FALSE.,  adapt=(/.FALSE.,.FALSE./),   interpolate=(/.FALSE.,.FALSE./), &
            exact=(/.FALSE.,.FALSE./),   saved=.TRUE., req_restart=.FALSE. )
        CALL register_var( 'Kappa_ratio',  integrated=.FALSE.,  adapt=(/.FALSE.,.FALSE./),   interpolate=(/.FALSE.,.FALSE./), &
            exact=(/.FALSE.,.FALSE./),   saved=.TRUE., req_restart=.FALSE. )
        IF (Nspec .GT. 1) CALL register_var( 'SpecDiff_ratio',  integrated=.FALSE.,  adapt=(/.FALSE.,.FALSE./),   interpolate=(/.FALSE.,.FALSE./), &
            exact=(/.FALSE.,.FALSE./),   saved=.TRUE., req_restart=.FALSE. )
      END IF

   END SUBROUTINE  sgs_compress_setup_pde



   !  Setup the compressible pde equations: set mappings (indices, adapt, interpolate, save)
   !    start_integrated: indice in u-array to start the contiguous block of integrated compressible equations 
   !    start_additional: indice in u-array to start the contiguous block of additional compressible variables 
   SUBROUTINE  sgs_compress_finalize_setup_pde()
      USE variable_mapping
      IMPLICIT NONE

      n_var_mu_t = get_index('Mu_turb')
      IF ( sgsmodel_compress .EQ. sgsmodel_compress_ksgs ) THEN
        CALL keqn_compress_finalize_setup_pde  
      END IF
   
      n_var_mu_ratio = get_index( 'Mu_ratio' )
      n_var_kk_ratio = get_index( 'Kappa_ratio' )
      IF(Nspec .GT. 1)n_var_bD_ratio = get_index( 'SpecDiff_ratio' )


      n_var_delta = get_index( 'Delta' )

   END SUBROUTINE  sgs_compress_finalize_setup_pde

    !
    ! Intialize sgs model
    ! This routine is called once in the first
    ! iteration of the main time integration loop.
    ! weights and model filters have been setup for first loop when this routine is called.
    !
  SUBROUTINE sgs_compress_model_setup( nlocal )
    USE wlt_trns_vars      ! indx_DB 
    USE wlt_trns_mod       ! delta_from_mask
    USE curvilinear
    USE parallel
    USE equations_compressible_vars
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: nlocal
    
    INTEGER, SAVE        :: nlocal_last = -1
    INTEGER              :: i, k, ii, j, j_df, wlt_type, face_type, idim
    REAL (pr)            :: tmp1, tmp2

    ! Set up Ksgs model: ERIC: some of these might not be needed since we have turb dissipation
    IF ( nlocal /= nlocal_last) THEN
      IF( ALLOCATED(mu_turb) )         DEALLOCATE( mu_turb )
                                       ALLOCATE(   mu_turb(1:nlocal) )
      mu_turb = 0.0_pr

      IF( ALLOCATED(kk_turb) )         DEALLOCATE( kk_turb )
                                       ALLOCATE(   kk_turb(1:nlocal) )
      kk_turb = 0.0_pr

      IF( ALLOCATED(bD_turb) )         DEALLOCATE( bD_turb )
                                       ALLOCATE(   bD_turb(1:nlocal) )
      bD_turb = 0.0_pr
    END IF

    ! Set up Ksgs model
    IF ( sgsmodel_compress .EQ. sgsmodel_compress_ksgs ) THEN
      CALL keqn_compress_model_setup(nlocal)
    END IF

    ! Setup filter widths 
    CALL sgs_delta_setup( nlocal )
    
    nlocal_last = nlocal
  END SUBROUTINE  sgs_compress_model_setup


  SUBROUTINE sgs_compress_rhs(sgs_rhs,u_integrated, meth )
    IMPLICIT NONE
    REAL (pr), DIMENSION (ng,ne), INTENT(IN) :: u_integrated !1D flat version of integrated variables without BC 
    REAL (pr), DIMENSION (ng*ne), INTENT(INOUT) :: sgs_rhs
    INTEGER, INTENT(IN) :: meth 

    IF ( sgsmodel_compress .EQ. sgsmodel_compress_fixSM ) THEN
      CALL fixSM_compress_rhs(sgs_rhs,u_integrated, meth )
    END IF

    IF ( sgsmodel_compress .EQ. sgsmodel_compress_ksgs ) THEN
      CALL keqn_compress_rhs(sgs_rhs,u_integrated, meth )
    END IF

  END SUBROUTINE sgs_compress_rhs


  SUBROUTINE sgs_compress_Drhs(sgs_Drhs, u_p, u_prev, meth )
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: meth
    REAL (pr), DIMENSION (ng,ne), INTENT(IN) :: u_p, u_prev
    REAL (pr), DIMENSION (ng*ne), INTENT(INOUT) :: sgs_Drhs

    IF ( sgsmodel_compress .EQ. sgsmodel_compress_fixSM ) THEN
      CALL fixSM_compress_Drhs(sgs_Drhs, u_p, u_prev, meth )
    END IF

    IF ( sgsmodel_compress .EQ. sgsmodel_compress_ksgs ) THEN
      CALL keqn_compress_Drhs(sgs_Drhs, u_p, u_prev, meth )
    END IF

  END SUBROUTINE sgs_compress_Drhs

  SUBROUTINE sgs_compress_Drhs_diag(sgs_diag,u_prev,meth)
    USE variable_mapping
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: meth
    REAL (pr), DIMENSION (ng*ne), INTENT(INOUT) :: sgs_diag
    REAL (pr), DIMENSION (ng,ne), INTENT(IN) :: u_prev

    IF ( sgsmodel_compress .EQ. sgsmodel_compress_fixSM ) THEN
      CALL fixSM_compress_Drhs_diag(sgs_diag,u_prev,meth)
    END IF

    IF ( sgsmodel_compress .EQ. sgsmodel_compress_ksgs ) THEN
      CALL keqn_compress_Drhs_diag(sgs_diag,u_prev,meth)
    END IF

  END SUBROUTINE sgs_compress_Drhs_diag

  !
  ! calculate any additional variables
  ! 
  ! arg 
  ! flag  - 0 calledwhile adapting to IC, 1 - called in main time integration loop
  !
  ! These additiona variables are calculated and left in real space.
  !
  SUBROUTINE sgs_compress_additional_vars( t_local, flag )
    USE field
    USE equations_compressible_vars
    IMPLICIT NONE

    REAL (pr), INTENT (IN) ::  t_local
    INTEGER , INTENT(IN) :: flag ! 0- called during adaption to IC, 1 called during main integration loop

    IF( use_comp_eddy_visc .AND. save_eddy_visc ) THEN 
      IF( ALLOCATED(mu_turb) ) u(:,n_var_mu_t) = mu_turb(:) 
    END IF

    IF( use_comp_eddy_visc .AND. save_diss_ratio ) THEN
      IF( ALLOCATED(mu_turb) ) u(:,n_var_mu_ratio) = mu_turb(:) / calc_dynamic_viscosity(u)  
      IF( ALLOCATED(kk_turb) ) u(:,n_var_kk_ratio) = kk_turb(:) / calc_conductivity(u)  
      IF(Nspec .GT. 1 .AND. ALLOCATED(bD_turb)) u(:,n_var_bD_ratio) = bD_turb(:) / calc_spec_diffusion(u)  

    END IF

    IF( use_comp_eddy_visc .AND. save_delta ) THEN 
      IF( ALLOCATED(delta) ) u(:,n_var_delta) = delta(:) 
    END IF

    IF ( sgsmodel_compress .EQ. sgsmodel_compress_ksgs ) THEN
      CALL keqn_compress_additional_vars(t_local,flag)
    END IF

  END SUBROUTINE sgs_compress_additional_vars


  ! Pre processing: any calculation of terms or coefficients static for the whole timestep
  SUBROUTINE  sgs_compress_force ( u_in, nlocal)
    USE util_vars        ! dA
    USE parallel
    USE variable_mapping 
    IMPLICIT NONE

    INTEGER,                                    INTENT (IN)    :: nlocal
    REAL (pr), DIMENSION (nlocal,n_integrated), INTENT (INOUT) :: u_in

    LOGICAL,   SAVE                      :: init_flag = .TRUE. !true if we are initializing the model

    ! Any initializations involving delta(:), which is not set up in time for the initial conditions
    IF ( sgsmodel_compress .EQ. sgsmodel_compress_ksgs .AND. init_flag ) THEN
      CALL initialize_ksgs( u_in ) 
    END IF

    ! Any stepwise pre_process
    IF ( sgsmodel_compress .EQ. sgsmodel_compress_ksgs ) THEN
      CALL keqn_compress_pre_process( u_in, nlocal )
    END IF

    ! Calculation of forcing terms
    IF ( use_comp_eddy_visc ) THEN 
       CALL calc_eddy_viscosity( u_in ) 
    END IF

    init_flag = .FALSE.

  END SUBROUTINE sgs_compress_force

  ! Calc eddy viscosity
  SUBROUTINE calc_eddy_viscosity( u_in ) 
    USE equations_compressible 
    USE variable_mapping 
    USE equations_compressible_vars
    IMPLICIT NONE
    REAL (pr), DIMENSION (nwlt,n_integrated), INTENT (IN)    :: u_in

    REAL (pr), DIMENSION(nwlt)               :: cp_glob 
    INTEGER :: l

    ! Calculate the eddy viscosity
    ! ERIC: better interfaces
    IF ( sgsmodel_compress .EQ. sgsmodel_compress_fixSM ) THEN 
      CALL calc_fixSM_eddy_visc( mu_turb, kk_turb, bd_turb, u_in ) 
    ELSE IF ( sgsmodel_compress .EQ. sgsmodel_compress_AMD ) THEN 
      CALL calc_AMD_eddy_visc( mu_turb, kk_turb, bd_turb, u_in ) 
    ELSE IF ( sgsmodel_compress .EQ. sgsmodel_compress_RBEVM ) THEN 
      mu_turb(:) = calc_RBEVM_eddy_visc( u_in ) 
      ! Calculation of conductivity. Could use an averaging function 
      cp_glob(:) = cp_in(Nspec) !cp_Nspec 
      IF (Nspec>1) cp_glob(:) = (1.0_pr-SUM(u_in(:,n_spc(1):n_spc(Nspecm)),DIM=2)/u_in(:,n_den))*cp_in(Nspec) !cp_Nspec
      DO l=1,Nspecm
        cp_glob(:) = cp_glob(:) + u_in(:,n_spc(l))/u_in(:,n_den)*cp_in(l) !cp
      END DO

      kk_turb(:) = cp_glob(:) * mu_turb(:) / Pra_t_in
      IF(Nspec>1) bD_turb(:) = mu_turb(:) / Sc_t_in
    ELSE IF ( sgsmodel_compress .EQ. sgsmodel_compress_ksgs ) THEN
      ! ERIC: keqn approach is slightly different than const prandtl numbers
      CALL calc_keqn_eddy_visc( mu_turb, kk_turb, u_in ) 
      IF(Nspec>1) bD_turb(:) = mu_turb(:) / Sc_t_in
    END IF

    CALL print_extrema ( mu_turb, nwlt, "mu_turb" )
    CALL print_extrema ( kk_turb, nwlt, "kk_turb" )
    IF(Nspec>1)CALL print_extrema ( bD_turb, nwlt, "bd_turb" )

  END SUBROUTINE calc_eddy_viscosity

  ! Residual Based Eddy Viscosity model 
  FUNCTION calc_RBEVM_eddy_visc( u_in ) 
    USE equations_compressible 
    USE variable_mapping 
    IMPLICIT NONE
    REAL (pr), DIMENSION (nwlt,n_integrated), INTENT (IN)    :: u_in
    REAL (pr), DIMENSION(nwlt)               :: calc_RBEVM_eddy_visc

    REAL (pr), DIMENSION(nwlt)               :: VMS_fine_scale
    REAL (pr), DIMENSION(1:nwlt,1:4)         :: spatial_avg     ! Spatially averaged quantities (here we use local top-hat filtering) 
    REAL (pr), DIMENSION(1:nwlt,1:3)         :: res             ! Momentum equation residual 
    REAL (pr), DIMENSION(nwlt)               :: tau, lam, lam2, inv_mach 
    INTEGER :: i,l, idim
    REAL (pr) :: coeff

    ! Modeled fine scale fluctuation magnitude: |u'|

    
    spatial_avg(:,1) = SUM( u_in(:,n_mom(1:dim))**2, DIM=2 )/(u_in(:,n_den)*u_in(:,n_den))  !|u|^2
    spatial_avg(:,2) = u_in(:,n_den) 
    CALL fraction_avg1( spatial_avg(:,3), mu_in(1:Nspec), u_in )
    spatial_avg(:,4) = 1.0_pr !calc_speed_of_sound  ! ERIC: use helper function 
    ! ERIC: does this need to be positive preserving?
    CALL print_extrema (spatial_avg(:,1), nwlt, "|u_prime|" )
    CALL print_extrema (spatial_avg(:,2), nwlt, "den" )
    CALL print_extrema (spatial_avg(:,3), nwlt, "mu" )
    CALL print_extrema (spatial_avg(:,4), nwlt, "sos" )

    CALL mdl_filt_by_type( spatial_avg(:,1:4),4,TESTfilter)
    CALL print_extrema (spatial_avg(:,1), nwlt, "|u_prime|" )
    CALL print_extrema (spatial_avg(:,2), nwlt, "den" )
    CALL print_extrema (spatial_avg(:,3), nwlt, "mu" )
    CALL print_extrema (spatial_avg(:,4), nwlt, "sos" )

    ! Tau calculation
    inv_mach(:) = 1.0_pr / ( SQRT( spatial_avg(:,1) ) / spatial_avg(:,4) +1.0e-8_pr )
    lam2(:) = SQRT( 4.0_pr / (delta(:)*delta(:)) * spatial_avg(:,1) )  ! 4/h^2 <|u|^2>
    lam(:) = lam2(:) * SQRT( 1.0_pr + 2.0_pr*(inv_mach(:)*inv_mach(:)) + inv_mach(:)*SQRT(4.0_pr + (inv_mach(:)*inv_mach(:))) )
    lam(:) = ( 1.0_pr - EXP(-1.0_pr/inv_mach(:)) )/lam(:) + ( EXP(-1.0_pr/inv_mach(:)) )/lam2(:)   ! 1/lam, reuse
    lam(:) = 1.0_pr / lam(:)
    tau(:) = ( lam(:)*lam(:) + Kolmog_const * ( 4.0_pr * spatial_avg(:,3) / ( delta(:)*delta(:)*spatial_avg(:,2) ) )**2.0_pr )**-0.5_pr 
    CALL print_extrema ( tau, nwlt, "tau" )

    ! Residual calculation
    CALL calc_NS_momentum_residual( res(:,1:dim), u_in ) 

    ! In-place calculation to then take the norm of this vector
    DO idim = 1,dim
      res(:,idim) = - tau(:) / u_in(:,n_den) !* res(:,idim) ! ERIC: what about the P operator? 
      CALL print_extrema ( res(:,idim), nwlt, "res" )
    END DO
    VMS_fine_scale(:) = SQRT( SUM( res(:,1:dim)**2, DIM=2 ) )
    CALL print_extrema ( VMS_fine_scale, nwlt, "|u_prime|" )

    ! Coefficient
    coeff = 2.0_pr / ( 3.0_pr * SQRT( 3.0_pr ) * PI ) * Kolmog_const**(-1.5) / SQRT(1.0_pr - modeled_grid_ratio**(-0.6666666666667_pr) )
    IF ( par_rank .EQ. 0 ) PRINT *, 'RBEVM coefficient:', coeff

    ! Eddy viscosity
    calc_RBEVM_eddy_visc(:) = coeff * delta(:) * VMS_fine_scale
    CALL print_extrema ( calc_RBEVM_eddy_visc, nwlt, "RBEV" )

  END FUNCTION calc_RBEVM_eddy_visc

  ! Calculate NS momentum equation residual 
  SUBROUTINE calc_NS_momentum_residual( residual, u_in ) 
    USE equations_compressible 
    USE variable_mapping 
    IMPLICIT NONE
    REAL (pr), DIMENSION (nwlt,n_integrated), INTENT (IN)    :: u_in
    REAL (pr), DIMENSION(1:nwlt,1:dim), INTENT(OUT)          :: residual 

    REAL (pr), DIMENSION (nwlt,n_integrated) :: u_mom_prev

    REAL (pr), DIMENSION (nwlt*n_integrated) :: NS_rhs
    INTEGER :: idim, ie, shift

    REAL (pr), SAVE :: dt_prev = -1.0_pr
    

    ! zero out eddy viscosities for proper calculation of the Navier-stokes residual ERIC: for safety, cache the data somewhere and replace?
    mu_turb = 0.0_pr
    kk_turb = 0.0_pr
    IF(Nspec>1) bD_turb = 0.0_pr

    ! Calculate residual from RHS, high order ERIC: would low order be sufficient?
print *, 'CHECK: ',size(u_in), shape(u_in)
    ! better way to do this?
    n = nwlt*n_integrated
    ne = n_integrated
    ng = nwlt
    CALL navier_stokes_rhs ( NS_rhs,u_in, 1 ) !ERIC: this is largely incompatible with Brinkman (also will cause large residual with Freund) and inflow BCs 

    ! Populate output array
    DO idim = 1,dim
      shift = (n_mom(idim)-1)*nwlt   
      residual(:,idim) = NS_rhs(shift+1:shift+nwlt)
    END DO

    ! Temporal derivative ! ERIC: is this low order approximation sufficient?
    ! ERIC: estimation of residual is flawed, especially for restart (what is dt and prev timestep)
    DO idim = 1,dim
      IF( dt_prev .LT. 0.0_pr ) THEN ! first timestep 
        u_mom_prev(1:nwlt,idim) = u_in(:,n_mom(idim)) 
      ELSE
        u_mom_prev(1:nwlt,idim) = u_prev_timestep(shift+1:shift+nwlt)
      END IF
    END DO
    ! Initialize dt_prev
    IF( dt_prev .LT. 0.0_pr ) THEN !dt_prev = dt  ! ERIC: this could cause issues with an improper restart
      DO idim = 1,dim
        residual(:,idim) = 0.0_pr 
      END DO
    ELSE
      DO idim = 1,dim
        residual(:,idim) = ( u_in(:,n_mom(idim)) - u_mom_prev(:,idim) ) / dt_prev - residual(:,idim) 
      END DO
    END IF
    CALL print_extrema ( mu_turb, nwlt, "mu_turb" )

    dt_prev = dt

  END SUBROUTINE calc_NS_momentum_residual 


END MODULE SGS_compressible
