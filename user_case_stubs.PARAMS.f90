MODULE user_case_db

  !
  ! n_var_db must be set to
  !   Count( Union (n_var_adapt, n_var_interpolate))
  ! This is the total number of variable that either are used for adaptation
  ! criteria or need to be interpolated to new grid at each time step
  ! or that are needed for vector derivatives.
  !
  INTEGER , PARAMETER :: n_var_db =  7  ! 7-3D
  INTEGER , PARAMETER :: max_dim  = 3   ! maximum dimension in DB

END MODULE user_case_db
