MODULE wlt_trns_vars
  USE wlt_vars                 ! DB_TYPE_...
  USE db_tree_vars             ! pointer_pr, node, node_ptr, etc
  IMPLICIT NONE
  
  
  !=========================== TYPE structure for indx_DB ===============================================================
  TYPE, PUBLIC :: indx_list_DB
     INTEGER :: ixyz                                ! 1-D global coordinate (at level j_lev <--> nxyz)
     INTEGER :: i                                   ! location in 1...nwlt index 
#if ((TREE_VERSION == 0) || (TREE_VERSION == 1))
     INTEGER(pointer_pr) :: nptr                    ! pointer to node of tree structure (integer in C/C++)
#elif ((TREE_VERSION == 2) || (TREE_VERSION == 3))
     TYPE(node), POINTER :: nptr                    ! (Fortran's native pointer)
#endif                                              ! %nptr --- used in read/write_DB only
  END TYPE indx_list_DB
  
  
  ! one cell of j_df x FACE x LEVEL x j matrix
  TYPE, PUBLIC :: indx_type_DB 
     TYPE ( indx_list_DB ), POINTER :: p(:)
     INTEGER :: length                           ! used as a counter
     INTEGER :: real_length                      ! real size of the allocated array p(:)
     INTEGER :: shift                            ! used for finding coordinates of bounary points in V cycle, when j < j_lev
  END TYPE indx_type_DB

  ! all p(:) point into contiguous_p
  TYPE ( indx_list_DB ), ALLOCATABLE, TARGET, PUBLIC :: contiguous_p(:)
  ! all nptr are accessed through contiguous_nptr(iwlt)
  INTEGER(pointer_pr), ALLOCATABLE, TARGET, PUBLIC :: contiguous_nptr(:)

  TYPE (indx_type_DB), ALLOCATABLE, DIMENSION(:,:,:,:), TARGET, PUBLIC :: indx_DB
  !indx_DB(j_df,wlt_type,face_type,j) j_df level of derivative everywhere except %shift
  !Special interpretation of %shift
  !indx_DB(j,wlt_type,face_type,jj)%shift - first index has meaning of level of resolution j < j_lev used for multigrid calculations
  
  
#if ((TREE_VERSION == 0) || (TREE_VERSION == 1))
  INTEGER ,PARAMETER, PUBLIC :: TYPE_DB = DB_TYPE_TREE_C ! so main knows which db module is being used.
#elif ((TREE_VERSION == 2) || (TREE_VERSION == 3))
  INTEGER ,PARAMETER, PUBLIC :: TYPE_DB = DB_TYPE_TREE_F
#endif

  
END MODULE wlt_trns_vars
