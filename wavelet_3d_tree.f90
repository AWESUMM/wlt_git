!-------------------------------------------------------!
! define whether to use C or Fortran style node pointer !
#ifdef TREE_NODE_POINTER_STYLE_C
#undef TREE_NODE_POINTER_STYLE_C
#endif
#ifdef TREE_NODE_POINTER_STYLE_F
#undef TREE_NODE_POINTER_STYLE_F
#endif
#if ((TREE_VERSION == 0) || (TREE_VERSION == 1))
#define TREE_NODE_POINTER_STYLE_C
#elif ((TREE_VERSION == 2) || (TREE_VERSION == 3))
#define TREE_NODE_POINTER_STYLE_F
#else
#error Please define correct TREE_VERSION
#endif
! define whether to use C or Fortran style node pointer !
!-------------------------------------------------------!



! tree
MODULE wlt_trns_mod
  USE wlt_trns_vars
  USE shared_functions

#if ((TREE_VERSION == 2) || (TREE_VERSION == 3))
  USE tree_database_f      ! use Fortran database instead of C/C++ tree
#endif

  IMPLICIT NONE

  PRIVATE
  ! defined when nxyz is calculated, Needed because some complier do not support intrinsics in declarations
  INTEGER :: maxval_nxyz
  LOGICAL :: i_c_init    ! tell significant_wlt() to redefine xx if necessary
  INTEGER :: n_var_to_transfer ! maximum number of variables to transfer in parallel mode

  INTEGER :: n_var_trns_mask ! used by c_wlt_trns_mask and adapt_grid
  INTEGER, DIMENSION(:), ALLOCATABLE :: i_var_trns_mask


  ! in addition to debug_level, define ADAPT_GRIDDEBUG, INIT_DBDEBUG, or WEIGHTSDEBUG
!!$#define ADAPT_GRIDDEBUG
!!$#define INIT_DBDEBUG
!!$#define WEIGHTSDEBUG
!!$#define INTEGER8_DEBUG_AR   


  !==========================================================================================================================

  PUBLIC :: adapt_grid, &
       c_diff_diag, &
       !c_diff_diag_bnd, &
       c_diff_fast,  &
       c_diff_fast_db, &
       c_diff_fast_vector, &    !ERIC: wrapper to use a vectr of methods for c_diff_fast
       c_wlt_trns, &
       c_wlt_trns_interp, &
       c_wlt_trns_interp_free, & 
       c_wlt_trns_interp_setup, &
       c_wlt_trns_mask, &
       get_indices_by_coordinate,&
       init_DB, &
       pre_init_DB, &                   ! set the tree and the domain decomposition
       mdl_filt_EPSplus, &
       release_memory_DB, &
       update_db_from_u, &
       update_u_from_db, &
       update_u_from_db_noidices,  &
       weights, &
       wlt_filt, &
       wlt_interpolate, &
       debug_print_u, &                     ! DEBUG print U values
       set_filter_window_values, &
       delta_from_mask


CONTAINS


  !**************************************************************************
  ! Begin wavelet transform subroutines
  !**************************************************************************
  !
  ! modified to have variable level input-output capability
  !
  !  u_in       1D data array input to be transformed
  !  u_out      1D data array that is the result of the transform
  !  nwlt_in    Number of active points in input 1D array
  !  nwlt_out   Number of active points in output 1D array
  !  j_in       Forward transform is done from level j_in to  1
  !  j_out      Inverse transform is done from level  1 to  j_outc
  !  j_lev_in   Seems to be functionally the same as j_in  ????
  !  j_lev_out  Seems to be functionally the same as j_out ????
  !  wlt_fmly   Wavelet transform family based on order or type, e.g. Sweldens wavelts of differnt order, Haar, etc.
  !  i_coef     If 1 then do forward transform, If -1 then do inverse transform
  !

  !
  ! Top level call to do a forward or inverse wlt transform on
  ! components in u(1:nwlt_in,:)  whose position in mask is .TRUE.
  ! mask_index locates masked vars in uin/u_out
  !
!!!!!!!!!!!!!!!  OLEG:  Need to add vector transform capability
  !??????????????????????????????????????????????????????????????
  !
  SUBROUTINE c_wlt_trns_mask(u_in, u_out, ie, mask, mask2, use_second_mask, old2new, &
       nwlt_in, nwlt_out, j_lev_in, j_lev_out, wlt_fmly, flag, &
       do_update_db_from_u_mask, do_update_u_from_db_mask, SKIP_TRANSFORM)
    USE precision
    USE sizes
    USE pde
    USE variable_mapping 
    USE db_tree
    USE wlt_vars
    USE debug_vars
    USE parallel
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: nwlt_in, nwlt_out, j_lev_in, j_lev_out, wlt_fmly, flag
    INTEGER, INTENT(IN) ::  ie
    LOGICAL, INTENT(IN) :: mask(1:ie) , mask2(1:ie)! transform elements in u whose position in mask is .TRUE.
    LOGICAL, INTENT(IN) :: use_second_mask, old2new
    LOGICAL, INTENT(IN) :: do_update_db_from_u_mask, do_update_u_from_db_mask
    LOGICAL, INTENT(IN), OPTIONAL :: SKIP_TRANSFORM
    REAL (pr), DIMENSION (1:nwlt_in,ie), INTENT (INOUT) :: u_in
    REAL (pr), DIMENSION (1:nwlt_out,ie), INTENT (INOUT) :: u_out
    REAL (pr), DIMENSION (1:nwlt_out,ie) :: u_tmp
    INTEGER :: i
    LOGICAL :: var_mask(n_var), do_skip_transform

    !
    ! defaults to 
    ! do_update_db_from_u_mask = .true. , do_update_u_from_db_mask = .true.
    do_skip_transform = .FALSE.
    IF (PRESENT(SKIP_TRANSFORM)) do_skip_transform = SKIP_TRANSFORM
    
    IF(.NOT.ALLOCATED(i_var_trns_mask)) ALLOCATE(i_var_trns_mask(n_var))
    n_var_trns_mask = 0
    DO i = 1,ie
       IF( mask(i) .OR. (use_second_mask .AND. mask2(i)) ) THEN
          n_var_trns_mask = n_var_trns_mask + 1
          i_var_trns_mask(n_var_trns_mask) = n_var_index(i)
       END IF
    END DO
    IF (do_update_db_from_u_mask) CALL  write_DB (u_in(:,i_var_trns_mask(1:n_var_trns_mask)), nwlt_in, 1, n_var_trns_mask, j_lev_in)
    
    
#ifdef MULTIPROC
    !var_mask = .FALSE. ! only n_var_trns_mask entries are used in request_known_list() anyway
    var_mask(1:n_var_trns_mask) = .TRUE.
    ! request the required nodes from lists already created in init_DB or adapt_grid
    ! (provide the mask which variables are required)
    CALL request_known_list (var_mask, n_var_trns_mask, list_sig, j_mx)
#endif
    
    
    ! make the transform inside the DB, using type-level list of DB,
    ! then copy the transform result into U_TMP from the DB
    CALL wlt_trns_DB (u_tmp(:,1:n_var_trns_mask), nwlt_out, 1, n_var_trns_mask, j_lev_out, wlt_fmly, NORMAL, &
         flag, SKIP_TRANSFORM = do_skip_transform)
    
    DO i=1,n_var_trns_mask
       u_out(:,i_var_trns_mask(i)) = u_tmp(:,i)
    END DO
    
    
  END SUBROUTINE c_wlt_trns_mask
  
  !
  ! Top level call to do a forward or inverse wlt transform on
  ! components mn_var,mx_var of u_in and put the result in u_out
  !
  !PUBLIC
  !Oleg: the code outside DB does not need to use indx_old, it alwasy does transform on j_lev
  !thus the high-level transfrom syntax is changed to reflect this
  !OLD SYNTAX: c_wlt_trns(u_in, u_out, ie, mn_var,mx_var, indx_in, indx_out,&
  !                       nwlt_in, nwlt_out, j_in, j_out, j_lev_in, j_lev_out, i_coef)
  SUBROUTINE c_wlt_trns (u, ie, mn_var, mx_var, wlt_fmly, flag)
    USE precision
    USE sizes
    USE wlt_vars
    USE db_tree          ! ..._DB
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: wlt_fmly, flag
    INTEGER, INTENT(IN) :: mn_var,mx_var, ie
    REAL (pr), DIMENSION (1:nwlt,ie), INTENT (INOUT) :: u
    INTEGER :: i
    INTEGER, PARAMETER :: trnsf_type = NORMAL
    LOGICAL :: var_mask(ie)
    
    ! taken from inside wlt_trns_DB (and setting update_db_from_u flag to FALSE)
    ! index in u array will be computed from indx_DB inside write_DB
    CALL write_DB (u, nwlt, mn_var, mx_var, j_lev)
#ifdef MULTIPROC
    var_mask = .FALSE.; var_mask(mn_var:mx_var) = .TRUE.
    CALL request_known_list (var_mask, ie, list_sig, j_lev)
#endif
    
    CALL wlt_trns_DB (u(1:nwlt,mn_var:mx_var), nwlt, mn_var, mx_var, j_lev, wlt_fmly, trnsf_type, flag)
    
  END SUBROUTINE c_wlt_trns

  !**************************************************************************
  ! Begin wavelet transform subroutines
  !**************************************************************************
  !PUBLIC
  SUBROUTINE wlt_filt(u, ilow, ihigh, nwlt, j_in, j_filt, regime)
    USE precision
    USE util_vars
    USE db_tree          ! ..._DB
    USE parallel         ! par_size
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: nwlt, j_in, j_filt, regime, ilow, ihigh
    REAL (pr), DIMENSION (1:nwlt), INTENT (INOUT) :: u
    INTEGER :: i, j, ii, iwlt, ierr, face_type, wlt_type, ixyz(1:dim), proc
    
#ifdef TREE_NODE_POINTER_STYLE_C
    INTEGER(pointer_pr) :: c_pointer, c_pointer1      ! pointer to node of tree structure (integer in C/C++)
#elif defined TREE_NODE_POINTER_STYLE_F
    TYPE(node), POINTER :: c_pointer, c_pointer1           ! (Fortran's native pointer)
#endif

    REAL (pr), DIMENSION (1:ihigh-ilow+1) :: u_clean
    LOGICAL :: in_zone
    
    u_clean = 0.0_pr
    
    IF(regime == 0 .AND. j_filt < j_in ) THEN
       DO proc = 0, par_size-1
          DO j = j_filt+1, j_in
             DO wlt_type = MIN(j-1,1),2**dim - 1
                DO face_type = 0, 3**dim - 1
                   CALL DB_get_initial_type_level_node (proc, wlt_type,j,face_type,ii,c_pointer,list_sig)
                   IF(ii > 0) THEN
                      
                      DO WHILE (is_ok(c_pointer))
#ifdef TREE_NODE_POINTER_STYLE_C
                         CALL DB_set_function_by_pointer( c_pointer, ilow, ihigh, u_clean )
                         CALL DB_get_next_type_level_node (proc,wlt_type,j,face_type,c_pointer,c_pointer1,list_sig)
                         c_pointer = c_pointer1
#elif defined TREE_NODE_POINTER_STYLE_F
                         CALL DB_set_function_by_pointer( c_pointer, ilow, ihigh, u_clean )
                         CALL DB_get_next_type_level_node (c_pointer)
#endif
                      END DO
                      
                   END IF
                END DO
             END DO
          END DO
       END DO
       
    ELSE IF(regime == 1 .AND. j_filt < j_in ) THEN ! zonal filtering (filtering outside of the zone)
       DO proc = 0, par_size-1
          DO j = j_filt+1, j_in
             DO wlt_type = MIN(j-1,1),2**dim - 1
                DO face_type = 0, 3**dim - 1
                   CALL DB_get_initial_type_level_node (proc,wlt_type,j,face_type,ii,c_pointer,list_sig)
                   IF(ii > 0) THEN
                      
                      DO WHILE (is_ok(c_pointer))
#ifdef TREE_NODE_POINTER_STYLE_C
                         CALL DB_get_ifunction_by_pointer( c_pointer, nvarI_nwlt, nvarI_nwlt, iwlt )
                         IF ( COUNT (xyzzone(1,1:dim) > x(iwlt,1:dim)) + &
                              COUNT (xyzzone(2,1:dim) < x(iwlt,1:dim)) .GT. 0 ) &
                              CALL DB_set_function_by_pointer( c_pointer, ilow, ihigh, u_clean )
                         CALL DB_get_next_type_level_node (proc,wlt_type,j,face_type,c_pointer,c_pointer1,list_sig)
                         c_pointer = c_pointer1
#elif defined TREE_NODE_POINTER_STYLE_F
                         iwlt = c_pointer%ival( nvarI_nwlt )
                         IF ( COUNT (xyzzone(1,1:dim) > x(iwlt,1:dim)) + &
                              COUNT (xyzzone(2,1:dim) < x(iwlt,1:dim)) .GT. 0 ) &
                              CALL DB_set_function_by_pointer( c_pointer, ilow, ihigh, u_clean )
                         CALL DB_get_next_type_level_node (c_pointer)
#endif
                      END DO
                      
                   END IF
                END DO
             END DO
          END DO
       END DO
    END IF

  END SUBROUTINE wlt_filt

  SUBROUTINE mdl_filt_EPSplus( filt_mask, u_in, scl, leps, ne_local) 
    USE precision
    USE main_vars
    USE share_consts
    USE pde
    USE sizes
    USE field
    USE db_tree          ! ..._DB
    USE parallel         ! par_size
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: ne_local  
    REAL (pr), DIMENSION (1:nwlt,1:ne_local) , INTENT (IN) :: u_in
    REAL (pr), DIMENSION (1:nwlt) , INTENT (INOUT) :: filt_mask
    REAL (pr) , INTENT (IN)   :: scl(1:ne_local)
    REAL (pr) , INTENT (IN)   :: leps ! local epsilon
    INTEGER :: i, iwlt, j, wlt_type, face_type, ii, j_lev_tmp, id
    LOGICAL, DIMENSION(1:ne_local) :: var_array

    LOGICAL, DIMENSION(1:ne_local) :: var_array_adapt_eps            ! Spatial Adaptive Epsilon
    INTEGER, PARAMETER :: tmp_mask_size = 1
    LOGICAL, PARAMETER :: tmp_mask(tmp_mask_size) = .TRUE.


#ifdef TREE_NODE_POINTER_STYLE_C
    INTEGER(pointer_pr) :: c_pointer, c_pointer1      ! pointer to node of tree structure (integer in C/C++)
#elif defined TREE_NODE_POINTER_STYLE_F
    TYPE(node), POINTER :: c_pointer, c_pointer1           ! (Fortran's native pointer)
#endif

    var_array_adapt_eps = .TRUE.                                     ! Spatial Adaptive Epsilon
    
    
    var_array = .TRUE.
    j_lev_tmp = j_lev
    
    ! set pos_msk bits (without modifying the lists)
    CALL significant_wlt_DB (scl(1:ne_local), leps, var_array, 1, ne_local, &
         j_lev_tmp, do_Adp_Eps_Spatial, nwlt, &
         eps_in_SpatialSpace(1:nwlt), var_array_adapt_eps(1:ne_local), &
         in_adapt_grid = .FALSE.)   ! Spatial Adaptive Epsilon
    
    IF (j_lev_tmp.NE.j_lev) THEN
       PRINT *, 'looks like some error: j_lev should not change here.'
       CALL parallel_finalize
       STOP 'in mdl_filt_EPSplus'
    END IF
    
    ! set 'adjacents' as pos_msk to already present pos_msk
    CALL adjacent_wlt_DB (ij_adj, adj_type, in_adapt_grid = .FALSE.)
    
    ! again, add as pos_msk nodes required for the transform
    CALL reconstr_check_DB (HIGH_ORDER, NORMAL, pos_msk, pos_msk)
    
#ifdef MULTIPROC
    ! all current processor nodes marked as from other processors in sig_list
    ! will be added to other processors as pos_msk via DB_add_...
    CALL make_list_and_inform (1, list_sig, pos_msk)
    ! make list and request the required nodes (marked as from other processors)
    ! (provide the mask which variables are required)
    CALL make_list_and_request( tmp_mask, tmp_mask_size, list_sig, j_lev, IDMODE=.TRUE. )
    ! ---- WARNING ----
    ! more efficient solution is possible here: e.g, add maxlev and/or msk modes
    !  ----WARNING ----
#endif

    DO j = 1, j_lev
       DO wlt_type = MIN(j-1,1),2**dim - 1
          DO face_type = 0, 3**dim - 1
             CALL DB_get_initial_type_level_node (par_rank,wlt_type,j,face_type,ii,c_pointer,list_sig)
             IF(ii > 0) THEN

                DO WHILE (is_ok(c_pointer))
#ifdef TREE_NODE_POINTER_STYLE_C
                   CALL DB_get_ifunction_by_pointer( c_pointer, nvarI_nwlt, nvarI_nwlt, iwlt )
                   CALL DB_get_id_by_pointer( c_pointer, id )
                   IF(BTEST(id,pos_msk)) filt_mask(iwlt) = 1.0           
                   CALL DB_get_next_type_level_node (par_rank,wlt_type,j,face_type,c_pointer,c_pointer1,list_sig)
                   c_pointer = c_pointer1
#elif defined TREE_NODE_POINTER_STYLE_F
                   iwlt = c_pointer%ival( nvarI_nwlt )
                   CALL DB_get_id_by_pointer( c_pointer, id )
                   IF(BTEST(id,pos_msk)) filt_mask(iwlt) = 1.0           
                   CALL DB_get_next_type_level_node (c_pointer)
#endif

                END DO

             END IF
          END DO
       END DO
    END DO
   
    
  END SUBROUTINE mdl_filt_EPSplus
  !
  ! 
  !**************************************************************************
  !
  ! Find spacial derivatives, vector version
  ! Calls the old c_diff_fast for each value in second arg of u
  !
  !
  ! Arguments
  !u, du, d2u, j_in, nlocal, meth, meth1, 
  ! ID Sets which derivative is done 
  !    10 - do first derivative
  !    11 - do first and secont derivative
  !    01 - do second derivative
  !    if ID > 0 then the second derivative is central difference D_central
  !    if ID < 0 then the second derivative is D_backward_bias followed by D_forward_bias
  ! meth  - order of the method, high low,
  !
  !  dimensions of u are u(1:nlocal,1:ie) 
  !  derivative is done on  u(1:nlocal,mn_var:mx_var)
  !PUBLIC
  SUBROUTINE c_diff_fast(u, du, d2u, j_in, nlocal, meth, id, ie, mn_var, mx_var, FORCE_RECTILINEAR, METH_VECT)
    USE precision
    USE sizes
    USE wlt_vars    ! dim
    USE user_case_db      ! max_dim, n_var_db
    USE db_tree 
    USE debug_vars
    USE parallel
    USE curvilinear 
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: j_in, nlocal, meth, id
    INTEGER, INTENT(IN) :: ie ! number of dimensions of u(:,1:ie)  we are doing derivative for (forced to 1 for now)
    INTEGER, INTENT(IN) ::  mn_var, mx_var   ! min and max of second index of u that we are doing the 1st derivatives for.
    !  INTEGER, INTENT(IN) ::  mn_varD2, mx_varD2 ! min and max of second index of u that we are doing the 2nd derivatives for.
    INTEGER :: i, idim, ii
    INTEGER :: n_diff_cycles, diff_cycle, diff_size, diff_l, diff_h
    REAL (pr), DIMENSION (1:nlocal, ie), INTENT (IN) :: u
    REAL (pr), DIMENSION (ie, 1:nlocal,dim), INTENT (INOUT) :: du,d2u
    REAL (pr), DIMENSION (ie*dim, 1:nlocal,dim) :: du_dim,d2u_dim
    REAL (pr), DIMENSION (1:nlocal, ie*dim) :: v
    LOGICAL :: var_mask(ie)
    INTEGER, DIMENSION(:), INTENT(IN), OPTIONAL :: METH_VECT

    LOGICAL, INTENT (IN), OPTIONAL :: FORCE_RECTILINEAR        ! In the case where curvilinear mapping is specified, force a rectilinear derivative (skip transform)
    LOGICAL :: skip_transform  
 
    REAL (pr), DIMENSION(1:ie, 1:dim) :: d_tmp  

    IF( ANY(transform_dir(1:dim) .EQ. 1) .AND. (MOD(id,10) .NE. 0)) THEN
       !OLEG:  WARNING perhaps we have to put biasing to avoid ODD-EVEN decoupling
       CALL c_diff_fast_curv(u, du, d2u, j_in, nlocal, meth, 10, ie, mn_var, mx_var, FORCE_RECTILINEAR, METH_VECT)
       DO i = 1,dim
          DO ii = 1, ie
             v(:,ie*(i-1)+ii) = du(ii,:,i)
          END DO
       END DO
       CALL c_diff_fast_curv(v, du_dim, d2u_dim, j_in, nlocal, meth, 10, ie*dim, mn_var, mx_var, FORCE_RECTILINEAR, METH_VECT)
       DO i = 1,dim
          DO ii = 1, ie
             d2u(ii,:,i) = du_dim(ie*(i-1)+ii,:,i)
          END DO
       END DO
   ELSE
      CALL c_diff_fast_curv(u, du, d2u, j_in, nlocal, meth, id, ie, mn_var, mx_var, FORCE_RECTILINEAR, METH_VECT)
   END IF

  END SUBROUTINE c_diff_fast

  SUBROUTINE c_diff_fast_curv(u, du, d2u, j_in, nlocal, meth, id, ie, mn_var, mx_var, FORCE_RECTILINEAR, METH_VECT)
    USE precision
    USE sizes
    USE wlt_vars    ! dim
    USE user_case_db      ! max_dim, n_var_db
    USE db_tree 
    USE debug_vars
    USE parallel
    USE curvilinear 
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: j_in, nlocal, meth, id
    INTEGER, INTENT(IN) :: ie ! number of dimensions of u(:,1:ie)  we are doing derivative for (forced to 1 for now)
    INTEGER, INTENT(IN) ::  mn_var, mx_var   ! min and max of second index of u that we are doing the 1st derivatives for.
    !  INTEGER, INTENT(IN) ::  mn_varD2, mx_varD2 ! min and max of second index of u that we are doing the 2nd derivatives for.
    INTEGER :: i, idim, ii, i_map, i_phys
    INTEGER :: j, j_df, wlt_type, face_type, iloc
    INTEGER :: n_diff_cycles, diff_cycle, diff_size, diff_l, diff_h
    REAL (pr), DIMENSION (1:nlocal, ie), INTENT (IN) :: u
    REAL (pr), DIMENSION (ie, 1:nlocal,dim), INTENT (INOUT) :: du,d2u
    LOGICAL :: var_mask(ie)
    INTEGER, DIMENSION(:), INTENT(IN), OPTIONAL :: METH_VECT

    LOGICAL, INTENT (IN), OPTIONAL :: FORCE_RECTILINEAR        ! In the case where curvilinear mapping is specified, force a rectilinear derivative (skip transform)
    LOGICAL :: skip_transform  
 
    REAL (pr), DIMENSION(1:ie, 1:dim) :: d_tmp  
 
    IF( debug_c_diff_fast>=1) THEN
       PRINT *,'    c_diff_fast() enter ===' !debug
       DO i = 1,ie
          WRITE(*,'( "     minmax u(:,",i2.2,") ", 2(G20.12,1X) )') &
               i, MINVAL(u(:,i)),MAXVAL(u(:,i)) !debug 
       END DO
    END IF

    !Oleg 05.02.2011: if ie > n_var_db, cycle through derivative calcualtions)
    IF(MOD(ie,n_var_db) == 0) THEN
       n_diff_cycles = ie/n_var_db
    ELSE
       n_diff_cycles = (ie - MOD(ie,n_var_db))/n_var_db+1
    END IF

   DERIVATIVE_LOOP: DO diff_cycle = 1, n_diff_cycles
      diff_size = MIN(ie - (diff_cycle-1)*n_var_db,n_var_db)
      diff_l = (diff_cycle-1)*n_var_db+1
      diff_h = (diff_cycle-1)*n_var_db+diff_size
      
      CALL  write_DB (u(:,diff_l:diff_h), nlocal, 1, diff_size, j_in)
      
      IF( debug_c_diff_fast>=2) THEN
         PRINT *, 'in c_diff_fast before wlt_diff_DB'
         CALL count_DB( MAXLIST=list_gho, FILE=6, VERB=.TRUE.)
      END IF
      IF( debug_c_diff_fast>=3) THEN
         DO i=1,nlocal
            WRITE (*,'("iwlt=",I3,", u=",G20.12,", du=",2G20.12)') i,u(i,1), du(1,i,:)
         END DO
         PAUSE 'before wlt_diff_DB'
      END IF
      
      IF( PRESENT(METH_VECT) ) THEN
         CALL wlt_diff_DB (u(:,diff_l:diff_h), du(diff_l:diff_h,:,:), d2u(diff_l:diff_h,:,:), j_in, nlocal, 1, diff_size, meth, id, METH_VECT=METH_VECT)          
      ELSE
         CALL wlt_diff_DB (u(:,diff_l:diff_h), du(diff_l:diff_h,:,:), d2u(diff_l:diff_h,:,:), j_in, nlocal, 1, diff_size, meth, id)
      END IF
   END DO DERIVATIVE_LOOP
   
   skip_transform = .FALSE.
   IF( PRESENT(FORCE_RECTILINEAR) ) skip_transform = FORCE_RECTILINEAR
   IF( ANY( transform_dir(1:dim) .EQ. 1 ) .AND. .NOT.(skip_transform) ) THEN ! Curvilinear transform
!!$      IF(MOD(ID,10) == 1 ) THEN
!!$         PRINT *, 'CANNOT DO DIRECT SECOND DERIVATIVE WITH CURVILINEAR TRANSFORM.  EXITING...'; STOP
!!$      END IF
      IF(j_in == j_lev) THEN
         DO i = 1,nlocal
            d_tmp(1:ie,1:dim)=0.0_pr 
            DO i_phys = 1,dim
               DO i_map = 1, dim
                  d_tmp(1:ie,i_phys) =   d_tmp(1:ie,i_phys) + curvilinear_jacobian(i_map,i_phys,i) * du(1:ie,i,i_map)
               END DO
            END DO
            du(1:ie,i,1:dim) = d_tmp(1:ie,1:dim)
         END DO
      ELSE
         DO j = 1,j_in
            DO wlt_type = MIN(j-1,1),2**dim-1
               DO face_type = 0, 3**dim - 1
                  DO j_df = j, j_lev
                     DO ii = 1, indx_DB(j_df,wlt_type,face_type,j)%length  
                        i = indx_DB(j_df,wlt_type,face_type,j)%p(ii)%i
                        iloc = indx_DB(j_df,wlt_type,face_type,j)%p(ii)%i + indx_DB(j_in,wlt_type,face_type,j)%shift
                        d_tmp(1:ie,1:dim)=0.0_pr 
                        DO i_phys = 1,dim
                           DO i_map = 1, dim
                              d_tmp(1:ie,i_phys) =   d_tmp(1:ie,i_phys) + curvilinear_jacobian(i_map,i_phys,i) * du(1:ie,iloc,i_map)
                           END DO
                        END DO
                        du(1:ie,iloc,1:dim) = d_tmp(1:ie,1:dim)
                     END DO
                  END DO
               END DO
            END DO
         END DO
      END IF
   END IF
   
    IF( debug_c_diff_fast>=1) THEN
       DO i = 1,mx_var-mn_var+1
          DO idim = 1,dim
             WRITE(*,'( "     minmax du(",i2.2,":,",i2.2,") ", 2(G20.12,1X) )') &
  	          i,idim,MINVAL(du(i,:,idim)),MAXVAL(du(i,:,idim)) !debug 
          END DO
       END DO
       IF(MOD(id,10) == 1 ) THEN ! du2 was calculated, so print it out also
          DO i = 1,mx_var-mn_var+1
             DO idim = 1,dim
                WRITE(*,'( "     minmax d2u(",i2.2,":,",i2.2,") ", 2(G20.12,1X) )') &
                     i,idim,MINVAL(d2u(i,:,idim)),MAXVAL(d2u(i,:,idim)) !debug 
             ENDDO
          END DO
       END IF
       PRINT *,'    c_diff_fast() exit ===' !debug
    END IF
    IF( debug_c_diff_fast>=2) THEN
       PRINT *, 'in c_diff_fast after wlt_diff_DB'
       CALL count_DB( MAXLIST=list_gho, FILE=6, VERB=.TRUE.)
    END IF
    IF( debug_c_diff_fast>=3) THEN
       DO i=1,nlocal
          WRITE (*,'("iwlt=",I3,", u=",G20.12,", du=",2G20.12)') i,u(i,1), du(1,i,:)
       END DO
       PAUSE 'after wlt_diff_DB'
    END IF
    
  END SUBROUTINE c_diff_fast_curv
  

  !
  ! 
  !**************************************************************************
  !
  ! Wrapper for spacial derivatives with multiple methods for backwards compatibility
  ! Calls the old c_diff_fast for each value in second arg of u
  !
  !
  ! Arguments
  !u, du, d2u, j_in, nlocal, meth, meth1, 
  ! ID Sets which derivative is done 
  !    10 - do first derivative
  !    11 - do first and secont derivative
  !    01 - do second derivative
  !    if ID > 0 then the second derivative is central difference D_central
  !    if ID < 0 then the second derivative is D_backward_bias followed by D_forward_bias
  ! meth  - order of the method, high/low: vector for multiple methods
  ! nlocal = nwlt*size(meth_vector)
  !
  !  dimensions of u are u(1:nlocal,1:ie) 
  !  derivative is done on  u(1:nlocal,mn_var:mx_var)
  !  
  !PUBLIC
  SUBROUTINE c_diff_fast_vector(u, du, d2u, j_in, nlocal, nmeth, meth, id, ie, mn_var, mx_var, FORCE_RECTILINEAR )
    !ERIC: which modules to use?
    USE precision
    USE sizes
    USE wlt_vars    ! dim
    USE user_case_db      ! max_dim, n_var_db
    USE db_tree 
    USE debug_vars
    USE curvilinear 
    !USE parallel
    !ERIC: remove unused integers for memory efficiency
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: j_in, nlocal, nmeth, id
    INTEGER, INTENT(IN) :: ie ! number of dimensions of u(:,1:ie)  we are doing derivative for (forced to 1 for now)
    INTEGER, INTENT(IN) ::  mn_var, mx_var   ! min and max of second index of u that we are doing the 1st derivatives for.
    !  INTEGER, INTENT(IN) ::  mn_varD2, mx_varD2 ! min and max of second index of u that we are doing the 2nd derivatives for.
    INTEGER, DIMENSION(nmeth), INTENT(IN) :: meth
    INTEGER :: i, idim, ii
    INTEGER :: n_diff_cycles, diff_cycle, diff_size, diff_l, diff_h
    REAL (pr), DIMENSION (1:nlocal, ie), INTENT (IN) :: u                           
    REAL (pr), DIMENSION (ie, 1:nlocal,dim), INTENT (INOUT) :: du,d2u          
    LOGICAL :: var_mask(ie)

    LOGICAL, INTENT (IN), OPTIONAL :: FORCE_RECTILINEAR        ! In the case where curvilinear mapping is specified, force a rectilinear derivative (skip transform)

    INTEGER, DIMENSION(nmeth) :: order_check
    DO i =1,nmeth
       order_check(i) = MOD(meth(i),2)
    END DO
    IF( MOD(SUM(order_check),nmeth) .NE. 0) THEN
       PRINT *, 'c_diff_fast_vector methods not of the same order'
       STOP
    END IF
    
    IF( PRESENT(FORCE_RECTILINEAR) ) THEN
       CALL c_diff_fast(u, du, d2u, j_in, nlocal, order_check(1), id, ie, mn_var, mx_var, FORCE_RECTILINEAR = FORCE_RECTILINEAR, METH_VECT=meth )  
    ELSE
       CALL c_diff_fast(u, du, d2u, j_in, nlocal, order_check(1), id, ie, mn_var, mx_var, METH_VECT=meth )  
    END IF

  END SUBROUTINE c_diff_fast_vector


  !
  !**************************************************************************
  !
  ! Arguments
  !u, du, d2u, j_in, nlocal, meth, meth1, 
  ! ID Sets which derivative is done 
  !    10 - do first derivative
  !    11 - do first and secont derivative
  !    01 - do second derivative
  !    if ID > 0 then the second derivative is central difference D_central
  !    if ID < 0 then the second derivative is D_backward_bias followed by D_forward_bias
  !
  SUBROUTINE c_diff_diag( du, d2u, j_in, nlocal, meth, meth1, ID, FORCE_RECTILINEAR )
    USE precision
    USE sizes
    USE wlt_vars   ! dim
    USE db_tree
    USE curvilinear

    IMPLICIT NONE
    INTEGER, INTENT(IN) :: j_in, nlocal, meth, meth1, id
    REAL (pr), DIMENSION (1:nlocal,dim) :: du_comp_meth, du_comp_meth1, d2u_comp
    REAL (pr), DIMENSION (1:nlocal,dim), INTENT (INOUT) :: du, d2u
    INTEGER :: i, j, i_phys, i_map
    INTEGER :: ibndr
    INTEGER :: ii, ij, ik, j_df, wlt_type, face_type, iloc
    INTEGER :: meth2

    LOGICAL, INTENT (IN), OPTIONAL :: FORCE_RECTILINEAR        ! In the case where curvilinear mapping is specified, force a rectilinear derivative (skip transform)
    LOGICAL :: skip_transform
    REAL (pr), DIMENSION(1:dim)    :: d_tmp 


    meth2 = meth1 
    IF(ID < 0) meth2 = meth
    !
    !----------------  inverse transform ----------------------
    !
    DO j = 1,MIN(j_in,j_lev) !@
!!$    DO j = MIN(j_in,j_full),MIN(j_in,j_lev) !@
       !
       !----------------- Calculation of first derivative ---------------
       !
       IF(INT(ABS(ID)/10) == 1) THEN
          CALL diff_diag_aux_DB ( du_comp_meth,  nlocal, j, meth,  meth,  1) !Nwlt_lev(j,1)
          CALL diff_diag_aux_DB ( du_comp_meth1, nlocal, j, meth2, meth2, 1) !Nwlt_lev(j,1)
       END IF
       !
       !----------------- Calculation of second derivative D^2 ---------------
       !
       IF(MOD(ID,10) == 1 .AND. ID > 0) THEN
          CALL diff_diag_aux_DB ( d2u_comp, nlocal, j, meth, meth, 2) !Nwlt_lev(j,1)
       END IF
       !
       !----------------- Calculation of second derivative DD ---------------
       !
       IF(MOD(-ID,10) == 1 .AND. ID < 0) THEN
          CALL diff_diag_aux_DB ( d2u_comp, nlocal, j, meth, meth2, 3) !Nwlt_lev(j,1)
       END IF

    END DO

    skip_transform = .FALSE.
    IF( PRESENT(FORCE_RECTILINEAR) ) skip_transform = FORCE_RECTILINEAR
    IF( ANY( transform_dir(1:dim) .EQ. 1 ) .AND. .NOT.(skip_transform) ) THEN ! Curvilinear transform
!!$       IF(MOD(ID,10) == 1 ) THEN
!!$          PRINT *, 'CANNOT DO DIRECT SECOND DERIVATIVE (DIAG) WITH CURVILINEAR TRANSFORM.  EXITING...'; STOP
!!$       END IF
       IF(j_in == j_lev) THEN
          IF(INT(ABS(ID)/10) == 1) THEN
             DO i = 1,nlocal
                DO i_phys =1,dim
                   du(i,i_phys) =    SUM(curvilinear_jacobian(1:dim,i_phys,i) * du_comp_meth(i,1:dim),DIM=1)  
                END DO
             END DO
          END IF
          IF( (MOD(ID,10) == 1 .AND. ID > 0) .OR. (MOD(-ID,10) == 1 .AND. ID < 0) ) THEN
             DO i = 1,nlocal
                DO i_phys =1,dim
                   d2u(i,i_phys) = SUM(curvilinear_jacobian(1:dim,i_phys,i)**2 * d2u_comp(i,1:dim),DIM=1) 
!!$                   !OLEG: off diagonal derivative term seem to cause convergnece problems, thus commented out until more accurate representaion is available
!!$                   DO  ij = 1, dim
!!$                      DO  ik = 1, dim
!!$                         d2u(i,i_phys) = d2u(i,i_phys) + (curvilinear_jacobian(ij,i_phys,i)*du_comp_meth(i,ij))*(curvilinear_jacobian(ik,i_phys,i)*du_comp_meth1(i,ik))
!!$                      END DO
!!$                      d2u(i,i_phys) = d2u(i,i_phys) - curvilinear_jacobian(ij,i_phys,i)**2*du_comp_meth(i,ij)*du_comp_meth1(i,ij)
!!$                   END DO
                END DO
             END DO
          END IF
       ELSE
          DO j = 1,j_in
             DO wlt_type = MIN(j-1,1),2**dim-1
                DO face_type = 0, 3**dim - 1
                   DO j_df = j, j_lev
                      DO ii = 1, indx_DB(j_df,wlt_type,face_type,j)%length  
                         i = indx_DB(j_df,wlt_type,face_type,j)%p(ii)%i
                         iloc = indx_DB(j_df,wlt_type,face_type,j)%p(ii)%i + indx_DB(j_in,wlt_type,face_type,j)%shift
                         IF(INT(ABS(ID)/10) == 1) THEN
                            DO i_phys =1,dim
                               du(iloc,i_phys) =    SUM(curvilinear_jacobian(1:dim,i_phys,i) * du_comp_meth(iloc,1:dim),DIM=1)  
                            END DO
                         END IF
                         IF( (MOD(ID,10) == 1 .AND. ID > 0) .OR. (MOD(-ID,10) == 1 .AND. ID < 0) ) THEN
                            DO i_phys =1,dim
                               d2u(iloc,i_phys) = SUM(curvilinear_jacobian(1:dim,i_phys,i)**2 * d2u_comp(iloc,1:dim),DIM=1)
!!$                   !OLEG: off diagonal derivative term seem to cause convergnece problems, thus commented out until more accurate representaion is available!!$                               DO  ij = 1, dim
!!$                                  DO  ik = 1, dim
!!$                                     d2u(iloc,i_phys) = d2u(iloc,i_phys) + (curvilinear_jacobian(ij,i_phys,i)*du_comp_meth(iloc,ij))*(curvilinear_jacobian(ik,i_phys,i)*du_comp_meth1(iloc,ik))
!!$                                  END DO
!!$                                  d2u(iloc,i_phys) = d2u(iloc,i_phys) - curvilinear_jacobian(ij,i_phys,i)**2*du_comp_meth(iloc,ij)*du_comp_meth1(iloc,ij)
!!$                               END DO
                            END DO
                         END IF
                      END DO
                   END DO
                END DO
             END DO
          END DO
       END IF
    ELSE
      du = du_comp_meth
      d2u = d2u_comp
    END IF

  END SUBROUTINE c_diff_diag

!!$  !
!!$  !**************************************************************************
!!$  !
!!$  SUBROUTINE c_diff_diag_bnd(du, d2u, j_in, nlocal, meth)
!!$    USE precision
!!$    USE sizes
!!$    USE wlt_vars   ! dim
!!$    IMPLICIT NONE
!!$    INTEGER, INTENT(IN) :: j_in, nlocal, meth
!!$    REAL (pr), DIMENSION (1:nlocal,dim), INTENT (INOUT) :: du,d2u
!!$    REAL (pr), DIMENSION (1:nwlt,dim) :: duh
!!$    INTEGER :: j
!!$    INTEGER :: ibndr, trnsf_type
!!$
!!$
!!$  trnsf_type = 0
!!$
!!$
!!$  DO j = MIN(j_in,j_full),MIN(j_in,j_lev)
!!$     !
!!$     !----------------- Calculation of first derivative ---------------
!!$     !
!!$        IF(j < j_in)  CALL diff_diag_bnd_aux (MAXVAL(nxyz), duh, wgh_df, wgh_d2f, lv, nlv, nlvD, nwlt, j, meth)
!!$        IF(j == j_in) CALL diff_diag_bnd_aux (MAXVAL(nxyz), duh, wgh_df, wgh_d2f, lvj, nlvj, nlvjD, nwltj, j, meth)
!!$     !
!!$  END DO
!!$
!!$  du(1:lv_intrnl(j_in),1:dim)=duh(1:lv_intrnl(j_in),1:dim)
!!$  DO ibndr=1,2*dim
!!$     du(lv_bnd(2,ibndr,j_in):lv_bnd(3,ibndr,j_in),1:dim)  = duh(lv_bnd(0,ibndr,1):lv_bnd(1,ibndr,j_in),1:dim)
!!$  END DO
!!$
!!$  END SUBROUTINE c_diff_diag_bnd



  !
  !**************************************************************************
  ! trnsf_type = 0 = NORMAL - regular transform
  ! trnsf_type = 1 = INTERNAL - internal transform and along boundaries  
  !
  SUBROUTINE wlt_interpolate(u, ie,  mn_var,mx_var, j_in, j_out, nlocal, wlt_fmly, trnsf_type)
    USE precision
    USE sizes
    USE share_consts
    USE db_tree
    !USE pde            ! n_var
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: nlocal, j_in, j_out, wlt_fmly, trnsf_type,ie, mn_var,mx_var
    REAL (pr), DIMENSION (1:nlocal,ie), INTENT (INOUT) :: u
    INTEGER :: j, jmin
    LOGICAL :: var_mask(ie)
    REAL (pr) :: u_zero(1,1:ie)
    
#ifdef MULTIPROC
    var_mask = .TRUE.
#endif
    
    
    IF(j_out > j_in) THEN
       ! u_zero is not used, optional ZERO will enforce writing zeroes into DB
       ! between optional j_in+1 (default 1) and j_out
       u_zero = 0.0_pr
       CALL  write_DB (u_zero, 1, 1, ie, j_out, JMIN=j_in+1, ZERO=.FALSE.)
    END IF
    
    
    ! taken from inside wlt_trns_DB (and setting update_db_from_u flag to FALSE)
    ! index in u array will be computed from indx_DB inside write_DB
    CALL write_DB (u, nlocal, 1, ie, j_in)
#ifdef MULTIPROC
    CALL request_known_list (var_mask, ie, list_sig, MAX(j_in,j_out))
#endif
    
    
    jmin = MIN (j_in, j_out) + 1 
    
    
    DO j = j_in, jmin, -1
       CALL wlt_trns_aux_DB (j, 1, ie, wlt_fmly, trnsf_type, list_sig, WLT_TRNS_FWD)
    END DO
    
    DO j = jmin, j_out
       CALL wlt_trns_aux_DB (j, 1, ie, wlt_fmly, trnsf_type, list_sig, WLT_TRNS_INV)
    END DO
    CALL  read_DB (u, nlocal, 1, ie, j_out)
    
    
  END SUBROUTINE wlt_interpolate
  


  !**************************************************************************
  ! Version of  wavelet transform used to do an inverse transform 
  ! from the adapted grid to the whole grid, thus interpolating to a regular grid.
  !**************************************************************************
  !**************************************************************************
  ! Begin wavelet transform subroutines
  ! This routine does an inverse wlt tranform from the c's on the
  ! adapted grid to the full field
  !**************************************************************************
  SUBROUTINE c_wlt_trns_interp (u_out, c,  nxyz_out, nwlt, j_in, j_out, wlt_fmly, leps) 
    USE precision
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: nwlt,  j_in, j_out, wlt_fmly, nxyz_out(1:3)
    REAL (pr), DIMENSION (1:nwlt), INTENT (IN) :: c
    REAL (pr), DIMENSION (0:nxyz_out(1)+1,0:nxyz_out(2),0:nxyz_out(3)), INTENT (INOUT) :: u_out
    REAL (pr), INTENT (IN) :: leps

    PRINT *, 'c_wlt_trns_interp() is not ready for db_tree'
    STOP

  END SUBROUTINE c_wlt_trns_interp

  !
  !-------- set weights for wavelet transform
  !
  SUBROUTINE c_wlt_trns_interp_setup(j_out, nxyz_out, max_nxyz_out, xx_out )
    USE precision
    USE wlt_vars   ! dim
    IMPLICIT NONE
    INTEGER , INTENT(IN) :: j_out, nxyz_out(1:3), max_nxyz_out
    REAL (pr) , INTENT(IN) ::  xx_out(0:max_nxyz_out,1:dim)

    PRINT *, 'c_wlt_trns_interp_setup() is not ready for db_tree'
    STOP

  END SUBROUTINE c_wlt_trns_interp_setup

  !free weights arrays for wavelet interpolation
  SUBROUTINE c_wlt_trns_interp_free()
    USE precision
    IMPLICIT NONE

    PRINT *, 'c_wlt_trns_interp_free() is not ready for db_tree'
    STOP

  END SUBROUTINE c_wlt_trns_interp_free


  !***********************************************************
  !* Do grid adaptation 
  ! 
  ! arguments
  ! flag  - 0 = in startup code, 1 in main loop
  !     0 implies multiple calls in adaptation to initial conditions
  !     1 implies single call after init_DB for a simple (or IC) restart
  !                 or single call during each timestep
  !***********************************************************
  SUBROUTINE adapt_grid( flag , leps , j_lev_old , j_lev_a, j_mn, j_mx, nxyz_a, ij_adj, &
       adj_type, scl ,new_grid)
    USE precision
    USE share_consts
    USE pde
    USE variable_mapping 
    USE sizes            ! nwlt
    USE field
    USE db_tree          ! ..._DB
    USE debug_vars
    USE parallel         ! par_size
    IMPLICIT NONE

    INTEGER , INTENT (IN)     :: j_mn, j_mx
    LOGICAL , INTENT (INOUT) :: new_grid                   ! had to be computed for flag==0 only
    INTEGER , INTENT (INOUT ) :: j_lev_a , j_lev_old
    INTEGER , INTENT (IN)     :: flag  
    INTEGER , INTENT (INOUT ) , DIMENSION(3) :: nxyz_a
    INTEGER, DIMENSION(-1:1)  , INTENT (IN) :: ij_adj, adj_type
    REAL (pr) , INTENT (IN)   :: leps 
    REAL (pr) , INTENT (IN)   :: scl(1:n_var)
    
    REAL(pr) :: tmp1, tmp2               ! for min/max value parallel synchronization
    LOGICAL :: mask(1:n_var), &
               domain_change                   ! T if domain is repartitioned or initialy partitioned

    INTEGER :: i, ii, j, ie, k, &
         nwlt_new, wlt_fmly, &
         trnsf_type, wlt_type, &
         face_type, id, id1, &
         proc, ixyz(dim)
    LOGICAL, PARAMETER :: tmp_void(1) = .TRUE.
    
#ifdef TREE_NODE_POINTER_STYLE_C
    INTEGER(pointer_pr) :: c_pointer, c_pointer1      ! pointer to node of tree structure (integer in C/C++)
#elif defined TREE_NODE_POINTER_STYLE_F
    TYPE(node), POINTER :: c_pointer, c_pointer1           ! (Fortran's native pointer)
#endif

    INTEGER, PARAMETER :: &
         DU = 553, &            ! DEBUG FILE, set 6 for screen output
         PARTITION = 0,   &     !
         REPARTITION = 1, &     ! Zoltan partitioning input parameter
         REFINE = 2             !
    
    REAL (pr), DIMENSION (:,:), ALLOCATABLE :: u_tmp

    !ERIC: for updated domain partitioning methods
    INTEGER :: number_of_trees
    INTEGER, ALLOCATABLE :: tree_stats(:,:)
    INTEGER  :: ONE(1:dim)


!!$    INTEGER, SAVE :: called = 0
!!$    called = called + 1
    
    
    !CALL DB_update_DB    
    !#define ADAPT_GRIDDEBUG
    !CALL count_DB (MAXLIST=list_gho, FILE=6)
    
    CALL timer_start(3)
    IF ( BTEST(debug_level,1) ) PRINT *, 'adapt_grid:'
    

#ifdef MULTIPROC
    ! delete nodes marked as from other processors from link lists and db
    CALL DB_clean_other_proc_nodes
#endif
    
    
    !***********************************************************
    !* Start grid adaptation criteria
    !***********************************************************
#ifdef ADAPT_GRIDDEBUG
    OPEN (UNIT=DU, FILE='debug.adapt_grid'//par_rank_str)
    IF ( BTEST(debug_level,2) ) THEN
       WRITE (DU,*) 'proc=',par_rank
       WRITE (DU,*) 'entering ADAPT_GRID: j_lev=',j_lev,'j_lev_a=',j_lev_a,'j_lev_old=',j_lev_old, 'proc=',par_rank
       WRITE (DU,*) 'nwlt=',nwlt
       WRITE (DU,*) 'n_var_adapt=',n_var_adapt
       CALL count_DB (MAXLIST=list_gho, FILE=DU)!, VERB=.TRUE.)
    END IF
#endif
    

    j_lev_old = j_lev
    nwlt_old = nwlt
    
    
    IF(read_geometry) THEN
       CALL readwrite_i_in (1) ! write i_in
    END IF


    
    ! Set ID significant bit
    ! passing through the type-level list of the DB
    ! Set j_lev and the correspondent nxyz, xx, h
    
    IF( (do_Adp_Eps_Spatial .EQV. .TRUE.) .OR. (do_Adp_Eps_Spatial_Evol .EQV. .TRUE.) ) THEN
       ! Spatial Adaptive Epsilon
       CALL significant_wlt_DB (scl(1:n_var_adapt_max), leps, n_var_adapt(1:n_var_adapt_max,flag), 1, n_var_adapt_max, &
            j_lev_old, .TRUE., nwlt, &
            eps_in_SpatialSpace(1:nwlt), n_var_adapt_eps(1:n_var,flag), par_rank, par_rank)
    ELSE
       CALL significant_wlt_DB (scl(1:n_var_adapt_max), leps, n_var_adapt(1:n_var_adapt_max,flag), 1, n_var_adapt_max, &
            j_lev_old, .FALSE., nwlt, proc_l=par_rank, proc_h=par_rank)
    END IF
    
    
    !CALL DB_PRINT_INFO
#ifdef ADAPT_GRIDDEBUG
    IF( BTEST(debug_level,2) ) THEN
       WRITE (DU,'(" ")')
       WRITE (DU,'("==================== after significant ================")')
       CALL count_DB (MAXLIST=list_gho, FILE=DU)!, VERB=.TRUE.)
       WRITE (DU,'("j_lev=",I3)') j_lev
       WRITE (DU,'("j_lev_a=",I3)') j_lev_a
       WRITE (DU,'("j_lev_old=",I3)') j_lev_old
       WRITE (DU, *) 'scl(1:n_var), leps=',scl(1:n_var), leps
       WRITE (DU,'("=======================================================")')
       WRITE (DU,'(" ")')
    END IF
#endif
    
    IF (ANY( dosymm == 1 )) THEN
       CALL symmetry_wlt_DB (dosymm, par_rank, par_rank)
       CALL make_list_and_inform (1, list_sig, pos_sig) 
    END IF   
    
    CALL adjacent_wlt_DB (ij_adj, adj_type, par_rank, par_rank)
    
    
    !CALL DB_PRINT_INFO
#ifdef ADAPT_GRIDDEBUG
    IF( BTEST(debug_level,2) ) THEN
       WRITE (DU,'(" ")')
       WRITE (DU,'("==================== after adjacent ===================")')
       CALL count_DB (MAXLIST=list_gho, FILE=DU)!, VERB=.TRUE.)
       WRITE (DU,'("j_lev=",I3)') j_lev
       WRITE (DU,'("j_lev_a=",I3)') j_lev_a
       WRITE (DU,'("j_lev_old=",I3)') j_lev_old
       WRITE (DU,'("=======================================================")')
       WRITE (DU,'(" ")')
    END IF
!!$    PAUSE 'in adapt_grid after adjacent_wlt_DB'
#endif

    CALL add_nodes_DB (j_lev)   !Oleg&Alireza changes :: Oleg needs to check for Acoustic Analogy
    
    !***********************************************************
    !* Add additional planes
    !***********************************************************
    IF(additional_planes_active) THEN
       DO i = 1, n_additional_planes
          CALL add_plane_DB(xyz_planes(:,i),dir_planes(i),j_additional_planes)
       END DO
    END IF
    !***********************************************************
    !* Add additional lines
    !***********************************************************
    IF(additional_lines_active) THEN
       DO i = 1, n_additional_lines
          CALL add_line_DB(xyz_lines(:,i),dir_lines(i),j_additional_lines)
       END DO
    END IF

    !***********************************************************
    !* Add additional points
    !***********************************************************
    IF(additional_points_active) THEN
       DO i = 1, n_additional_points
          CALL add_point_DB(xyz_points(:,i),j_additional_points)
       END DO
    END IF

#ifdef ADAPT_GRIDDEBUG
    IF( BTEST(debug_level,2) ) THEN
       WRITE (DU,'(" ")')
       WRITE (DU,'("==================== after add_nodes ==================")')
       CALL count_DB (MAXLIST=list_gho, FILE=DU)
       WRITE (DU,'("=======================================================")')
       WRITE (DU,'(" ")')
    END IF
#endif       
       
!#ifdef MULTIPROC
!    !***********************************************************
!    !* START Repartitioning the domain
!    !*  (in timestep; while adapting to IC 
!    !*   the domain partitioning is unchanged,
!    !*   unless allowed by IC_repartition (default=T))
!    !***********************************************************
!    IF (flag.NE.0.OR.IC_repartition) THEN
!       CALL timer_start(5)
!       !
!       ! First, synchronize sig/adj list: all current processor nodes marked as
!       ! from other processors will be added to other processors as adjacent (coord+ID)
!       ! start from j_mn+1 since j_mn and lower levels have been added anyway in adjacent_DB
!       ! (otherwise some such nodes could be lost after repartitioning and DB_clean_others)
!       CALL make_list_and_inform (1, list_sig, pos_adj)
!       !
!       !
!       ! Second, repartition.
!       ! This is the second call of domain partitioning,
!       ! the first one is in init_DB_tree_aux before DB_declare_globals with FIRSTCALL=.TRUE.
!       ! (sig+adj nodes with their function values will be moved to the correct procesors
!       i = 0
!       IF (par_rank.EQ.0) i=verb_level
!
!       !-------------------
!       ONE(1:dim) = 1
!       number_of_trees = PRODUCT( mxyz(1:dim)*2**(j_tree_root-1) + ONE(1:dim) - prd(1:dim) )
!       IF(ALLOCATED(tree_stats)) DEALLOCATE(tree_stats)
!       ALLOCATE(tree_stats(0:dim,number_of_trees))
!       !ERIC: to get tree statistics for domain decompose; called here because of compilation order
!       CALL check_active_nodes_per_tree_DB(number_of_trees,tree_stats(0:dim,1:number_of_trees)) 
!       !O+A 04.05.2011 - Need to count sig + adj + old 
!
!       !----------------------------------
!       CALL parallel_domain_decompose (domain_change, &
!            VERBLEVEL=i, &
!            METH=domain_meth, TREE_STATS=tree_stats)
!       
!       IF (domain_change) THEN
!          !
!          ! Delete nodes marked as from other processors from link lists and db
!          ! (to clean the trees passed to other processors)
!
!          CALL DB_clean_other_proc_nodes
!
!          !O+A 04.05.2011 - Need to count sig + adj + old and compare with the umber before decompose, should be idetical 
!
!          
!          !  The cleaning above might unnecessary clean some of the adjacent nodes
!          !  laying close to the current processor domain. Therefore,
!         !  we may need to re-mark adjacent nodes from the other processors:
!         !  It seems to be redundant.
! !O&A!          CALL adjacent_wlt_DB (ij_adj, adj_type, par_rank, par_rank)
! !O&A!          CALL add_nodes_DB (j_lev)                                       Oleg&Alireza changes :: Oleg needs to check for Acoustic Analogy
!          !
!       END IF
!       CALL timer_stop(5)
!    END IF
!    !***********************************************************
!    !* END Repartitioning the domain
!    !***********************************************************
!#endif
    
    
#ifdef ADAPT_GRIDDEBUG
    IF( BTEST(debug_level,2) ) THEN
       WRITE (DU,'(" ")')
       WRITE (DU,'("==================== after repartitioning of the domain =========")')
       CALL count_DB (MAXLIST=list_gho, FILE=DU)
       WRITE (DU,'(" ")')
    END IF
#endif
    
!Oleg: 06.30.2012 - moved it was in wrong location before make_list and infrom, which could result in partition depending mesh
    IF(BNDzone) THEN
       CALL bnd_zone_DB (j_lev)
    !CALL DB_PRINT_INFO
#ifdef ADAPT_GRIDDEBUG
    IF( BTEST(debug_level,2) ) THEN
       WRITE (DU,'(" ")')
       WRITE (DU,'("==================== after bnd_zone =========")')
       CALL count_DB (MAXLIST=list_gho, FILE=DU, VERB=.TRUE.)
       WRITE (DU,'("=======================================================")')
       WRITE (DU,'(" ")')
    END IF
#endif
    END IF
    !***********************************************************
    !* End grid adaptation criteria
    !***********************************************************

    !***********************************************************
    !* Special Grid adaptation criteria within zone
    !***********************************************************
    CALL zone_wlt_DB (j_lev,j_zn)   !ERIC: copy and edit this supbroutine from db_tree.f90 for J_mx_fluid, also modify for reverse zone control (adaptive outside)
    
    !CALL DB_PRINT_INFO
#ifdef ADAPT_GRIDDEBUG
    IF( BTEST(debug_level,2) ) THEN
       WRITE (DU,'(" ")')
       WRITE (DU,'("==================== after zone_wlt before rc =========")')
       CALL count_DB (MAXLIST=list_gho, FILE=DU, VERB=.TRUE.)
       WRITE (DU,'("=======================================================")')
       WRITE (DU,'(" ")')
    END IF
#endif
    !***********************************************************
    !* End grid adaptation criteria
    !***********************************************************    
    
    !------------- Performing a reconstruction check  ----------
    ! through all the domain, including the current processor boundary zone
    DO trnsf_type =0,n_trnsf_type
       DO wlt_fmly = 0, n_wlt_fmly
          CALL reconstr_check_DB (wlt_fmly, trnsf_type, pos_sig, pos_adj)!, par_rank, par_rank)
       END DO
    END DO
    
#ifdef ADAPT_GRIDDEBUG
    IF( BTEST(debug_level,2) ) THEN
       WRITE (DU,'(" ")')
       WRITE (DU,'("==================== right after rc =========")')
       CALL count_DB (MAXLIST=list_gho, FILE=DU)!, VERB=.TRUE.)
       WRITE (DU,'("=============================================================")')
       WRITE (DU,'(" ")')
    END IF
#endif
    
#ifdef MULTIPROC
    ! all current processor nodes (from list_sig) marked as from other processors
    ! will be added to other processors as adjacent (coord+ID)
    ! start from j_mn+1 since j_mn and lower levels have been added anyway in adjacent_DB
    CALL make_list_and_inform (1, list_sig, pos_adj)
    

    !Oleg+Eric 10.08.2013 ??????? it appears that it is better to move domain_decompose here, after all grid is adapted on and zones are incorporated.

!#ifdef MULTIPROC
    !***********************************************************
    !* START Repartitioning the domain
    !*  (in timestep; while adapting to IC 
    !*   the domain partitioning is unchanged,
    !*   unless allowed by IC_repartition (default=T))
    !***********************************************************
    IF (flag.NE.0.OR.IC_repartition) THEN
       CALL timer_start(5)
       !
       ! First, synchronize sig/adj list: all current processor nodes marked as
       ! from other processors will be added to other processors as adjacent (coord+ID)
       ! start from j_mn+1 since j_mn and lower levels have been added anyway in adjacent_DB
       ! (otherwise some such nodes could be lost after repartitioning and DB_clean_others)

       !ERIC: this subroutine just called
!       CALL make_list_and_inform (1, list_sig, pos_adj)
       !
       !
       ! Second, repartition.
       ! This is the second call of domain partitioning,
       ! the first one is in init_DB_tree_aux before DB_declare_globals with FIRSTCALL=.TRUE.
       ! (sig+adj nodes with their function values will be moved to the correct procesors
       i = 0
       IF (par_rank.EQ.0) i=verb_level

       !-------------------
       ONE(1:dim) = 1
       number_of_trees = PRODUCT( mxyz(1:dim)*2**(j_tree_root-1) + ONE(1:dim) - prd(1:dim) )
       IF(ALLOCATED(tree_stats)) DEALLOCATE(tree_stats)
       ALLOCATE(tree_stats(0:dim,number_of_trees))
       !ERIC: to get tree statistics for domain decompose; called here because of compilation order
       CALL check_active_nodes_per_tree_DB(number_of_trees,tree_stats(0:dim,1:number_of_trees)) 
       !O+A 04.05.2011 - Need to count sig + adj + old 

       !----------------------------------
       CALL parallel_domain_decompose (domain_change, &
            VERBLEVEL=i, &
            METH=domain_meth, TREE_STATS=tree_stats)
       
       IF (domain_change) THEN
          !
          ! Delete nodes marked as from other processors from link lists and db
          ! (to clean the trees passed to other processors)

          !ERIC: this subroutine called immediately afterwards, regardless of domain change
!          CALL DB_clean_other_proc_nodes

          !O+A 04.05.2011 - Need to count sig + adj + old and compare with the umber before decompose, should be idetical 

          
          !  The cleaning above might unnecessary clean some of the adjacent nodes
          !  laying close to the current processor domain. Therefore,
          !  we may need to re-mark adjacent nodes from the other processors:
          !  It seems to be redundant.
!O&A!          CALL adjacent_wlt_DB (ij_adj, adj_type, par_rank, par_rank)
!O&A!          CALL add_nodes_DB (j_lev)                                       Oleg&Alireza changes :: Oleg needs to check for Acoustic Analogy
          !
       END IF
       CALL timer_stop(5)
    END IF
    !***********************************************************
    !* END Repartitioning the domain
    !***********************************************************
!#endif

    !O+A 04.05.2011 - clean adjacent other processr nodes to elliminate adjacent points that are not required for the current processor
    CALL DB_clean_other_proc_nodes !O+A 04.05.2011

!Oleg: begin changes after talking with Alexei 11/02/2010
    !------------- !Performing a reconstruction check  ----------
    ! through all the domain, including the current processor boundary zone
    DO trnsf_type =0,n_trnsf_type
       DO wlt_fmly = 0, n_wlt_fmly
          CALL reconstr_check_DB (wlt_fmly, trnsf_type, pos_sig, pos_adj)!, par_rank, par_rank)
       END DO
    END DO
!Oleg: end changes changes after talking with Alexei 11/02/2010
#endif
    
#ifdef ADAPT_GRIDDEBUG
    IF( BTEST(debug_level,2) ) THEN
       WRITE (DU,'(" ")')
       WRITE (DU,'("==================== after make_list_and_inform & rc =========")')
       CALL count_DB (MAXLIST=list_gho, FILE=DU)
       WRITE (DU,'("=============================================================")')
       WRITE (DU,'(" ")')
    END IF
#endif

    
    
    ! indices_tree() will be sweeping through sig list
    ! so, move zero (or old) id from sig_list to ghost
    ! (for all the processors)
    CALL DB_move_zero_sig_to_gho
!!$ 05.06.2011    
!!$ Before "Revision 1032", while debugging the code, it was thought that it might be necessary to add this forced Barrier. 
!!$ Because, in parallel (np=2), when the code hanged in IC, after killing the code, one of the ranks was in  reconstr_check_DB(pos_sig, pos_gho)  and the other in  make_list_and_request(list_gho).
!!$#ifdef MULTIPROC
!!$    ii=1
!!$    CALL parallel_global_sum( INTEGER=ii )
!!$    IF (ii /= par_size) THEN
!!$       WRITE(*,'("ERROR: ii /= par_size")')
!!$       STOP
!!$    END IF
!!$#endif    

    ! OLEG: some old_id, former sig, have been moved to gho list while their values
    ! are still in the database; for a scaler code, when n_update \= 0, these values are used for inverse transform. 
    ! For parallel code n_update = 0, so the old points are never used by any processor, thus can be removed from the list.

    ! WARNING - this is for testing only, we zero all the old_id nodes and compare parallel vs. single proc run
!!$     CALL clean_DB(list_gho,.TRUE.,.TRUE.)
!!$     CALL DB_update_DB
    ! WARNING
    !OLEG: 04.15.2011: If we remove old and ghost points for parallel vesion, the ghost list is empty at this point, no need for this call
#ifdef MULTIPROC
    !CALL make_list_and_request (n_var_interpolate(1,flag), n_var, list_gho, j_mx)
#endif

    
    !CALL DB_PRINT_INFO
#ifdef ADAPT_GRIDDEBUG
    IF( BTEST(debug_level,2) ) THEN
       WRITE (DU,'(" ")')
       WRITE (DU,'("================= after rc before indices_tree ========")')
       CALL count_DB (MAXLIST=list_gho, FILE=DU)!, VERB=.TRUE.)
       WRITE (DU,'("j_lev=",I3)') j_lev
       WRITE (DU,'("j_lev_a=",I3)') j_lev_a
       WRITE (DU,'("j_lev_old=",I3)') j_lev_old
       WRITE (DU,'("=======================================================")')
       WRITE (DU,'(" ")')
    END IF
#endif
    
    
    
    !    Set inverse mapping (ifunction at nvarI_nwlt) for points in sig list (sig+adj)
    !    of the DB (internal then boundary, in the order of type-level cycle);
    !    count nwlt, set Nwlt_lev, reallocate x(nwlt) and set its values (using inverse mapping)
    !    test if the grid is new: set new_grid
    !    set j_full
    !    (lists are not changed in indices_tree)
    CALL indices_tree (nwlt_old, nwlt, new_grid, j_lev_old, j_lev, j_mn, j_mx, &
         1, &                             ! it is not a restart (-1) or first initialization (0)
         COMPUTE_new_grid = (flag.EQ.0) ) ! set new_grid only for flag == 0 (for adapt to IC loop)



#ifdef ADAPT_GRIDDEBUG
    IF( BTEST(debug_level,2) ) THEN
       WRITE (DU,'(" ")')
       WRITE (DU,'("==================== after indices_tree =======")')
       CALL count_DB (MAXLIST=list_gho, FILE=DU)!, VERB=.TRUE.)
       WRITE (DU,*) 'after indices, j_lev    =',j_lev
       WRITE (DU,*) 'after indices, j_lev_a  =',j_lev_a
       WRITE (DU,*) 'after indices, j_lev_old=',j_lev_old
       WRITE (DU,*) 'after indices, new_grid =',new_grid
       WRITE (DU,*) 'nwlt_old=',nwlt_old,'nwlt=',nwlt
       WRITE (DU,'("=======================================================")')
       WRITE (DU,'(" ")')
    END IF
#endif
    
    
    
    ! add ghosts (parallel version requires some preparation and cleaning)
#ifdef MULTIPROC
    CALL add_ghost_DB_parallel_antea (par_rank, par_rank)
    IF( BTEST(debug_level,2) ) THEN
       WRITE (DU,'(" ")')
       WRITE (DU,'("==================== after add_ghost_DB_parallel_antea =======")')
       CALL count_DB (MAXLIST=list_gho, FILE=DU)
    END IF
#endif
    CALL add_ghost_DB (par_rank, par_rank)
    
    
!!$#ifdef MULTIPROC
!!$    ! --- WARNING ---
!!$    ! it seems that we might not have to inform others about ghosts and
!!$    ! the only reason of that is the conformity with serial version
!!$    ! currently ghosts values will be requested in c_diff_fast
!!$    IF( BTEST(debug_level,0) ) CALL add_ghost_DB_parallel_postea ( CLEAN=.FALSE. )
!!$#endif



    ! rewrite/create indx_DB from the nodes of sig list of the DB
    ! (take inverse mapping, etc, from the DB)
    CALL indices_DB (par_rank, par_rank)


    !CALL DB_PRINT_INFO
#ifdef ADAPT_GRIDDEBUG
    IF( BTEST(debug_level,2) ) THEN
       WRITE (DU,'(" ")')
       WRITE (DU,'("==================== after add_ghost ==================")')
       CALL count_DB (MAXLIST=list_gho, FILE=DU)!, VERB=.TRUE.)
       WRITE (DU,'("j_lev=",I3)') j_lev
       WRITE (DU,'("j_lev_a=",I3)') j_lev_a
       WRITE (DU,'("j_lev_old=",I3)') j_lev_old
       WRITE (DU,'("=======================================================")')
       WRITE (DU,'(" ")')
    END IF
#endif
    

    !
    !------------- Performing a reconstruction check  ----------
    ! through all the domain, including the current processor boundary zone
    DO trnsf_type =0,n_trnsf_type
       DO wlt_fmly = 0, n_wlt_fmly
          CALL reconstr_check_DB (wlt_fmly, trnsf_type, pos_sig, pos_gho)!, par_rank, par_rank)
       END DO
    END DO

    
#ifdef MULTIPROC
!!$    ! we do not need these extra ghosts for derivatives and they will not affect the transform
!!$    ! in the already added ghosts, so we do not need that extra synchronization
!!$    ! Nov 2010, apply similar to adj treatment for the gho -------------------------------
!!$    ! all current processor nodes (from list_gho) marked as from other processors
!!$    ! will be added to other processors as gho (coord+ID)

!!$    !O+A: logically  we do not need that. Once the bugs are fixed, this line need to be commented out
!!OLEG 04.15.2011: if the line below is uncommented, ALL ghost points are added to all processors. 
!!OLEG 04.15.2011: NOTE(***) Moreover, the ghost points contain adjacent points
!!$    CALL make_list_and_inform (1, list_all, pos_gho)

!!$    Prior "Revision 1032", the following 6 Stabilizer were added in order to decompose the above one.
!!$    And it was observed that only the 6th one [make_list_and_inform (1, list_gho, pos_gho)] stabilize the code

!!$    Prior "Revision 1032",  Either [ make_list_and_inform (1, list_all, pos_gho) ], 
!!$                                   [ make_list_and_inform (1, list_gho, pos_gho) ],  or  
!!$                                   [ DB_update_DB (after  DB_move_zero_sig_to_gho) +  "f->f_gho.tlp_node_include_b (current_in_sig_list)" inside DB_move_zero_sig_to_gho ]
!!$                            could stabilize the code. 

!!$

!!$ ******************** THIS LINE STABILIZES THE CODE *********************   
!!$    WRITE(*,'("Before the code is stabilized")')
!!$    CALL diagnostics_list_id(list_sig, pos_sig, pos_adj, .FALSE.)
!!$    CALL diagnostics_list_id(list_gho, pos_sig, pos_adj, .TRUE.)
!!$
!!$    CALL make_list_and_inform (1, list_sig, pos_sig) ! STABILIZER
!!$
!!$    WRITE(*,'("After the code is stabilized list_sig - pos_sig")')
!!$    CALL diagnostics_list_id(list_sig, pos_sig, pos_adj, .FALSE.)
!!$    CALL diagnostics_list_id(list_gho, pos_sig, pos_adj, .TRUE.)
!!$
!!$    CALL make_list_and_inform (1, list_sig, pos_adj) ! STABILIZER
!!$
!!$    WRITE(*,'("After the code is stabilized list_sig - pos_adj")')
!!$    CALL diagnostics_list_id(list_sig, pos_sig, pos_adj, .FALSE.)
!!$    CALL diagnostics_list_id(list_gho, pos_sig, pos_adj, .TRUE.)
!!$
!!$    CALL make_list_and_inform (1, list_sig, pos_gho) ! STABILIZER
!!$
!!$    WRITE(*,'("After the code is stabilized list_sig - pos_gho")')
!!$    CALL diagnostics_list_id(list_sig, pos_sig, pos_adj, .FALSE.)
!!$    CALL diagnostics_list_id(list_gho, pos_sig, pos_adj, .TRUE.)
!!$
!!$    CALL make_list_and_inform (1, list_gho, pos_sig) ! STABILIZER
!!$
!!$    WRITE(*,'("After the code is stabilized list_gho - pos_sig")')
!!$    CALL diagnostics_list_id(list_sig, pos_sig, pos_adj, .FALSE.)
!!$    CALL diagnostics_list_id(list_gho, pos_sig, pos_adj, .TRUE.)
!!$
!!$    CALL make_list_and_inform (1, list_gho, pos_adj) ! STABILIZER
!!$
!!$    WRITE(*,'("After the code is stabilized list_gho - pos_adj")')
!!$    CALL diagnostics_list_id(list_sig, pos_sig, pos_adj, .FALSE.)
!!$    CALL diagnostics_list_id(list_gho, pos_sig, pos_adj, .TRUE.)
!!$
!!$    CALL make_list_and_inform (1, list_gho, pos_gho) ! STABILIZER     
!!$
!!$    WRITE(*,'("After the code is stabilized list_gho - pos_gho")')
!!$    CALL diagnostics_list_id(list_sig, pos_sig, pos_adj, .FALSE.)
!!$    CALL diagnostics_list_id(list_gho, pos_sig, pos_adj, .TRUE.)
!!$ ************************************************************************
!!$    DO trnsf_type =0,n_trnsf_type
!!$       DO wlt_fmly = 0, n_wlt_fmly
!!$          CALL reconstr_check_DB (wlt_fmly, trnsf_type, pos_sig, pos_gho)
!!$       END DO
!!$    END DO
!!$    ! ------------------------------------------------------------------------------------


    ! request IDs of ghosts in the boundary zone of the current processor
    ! from the processor of their origin in order to catch thouse gho which may fall on a sig
    !OLEG 04.15.2011: we need to go through the complete list up to level j_lev instead of j_lev+1 (or conservatively j_mx)
    CALL make_list_and_request( tmp_void, 1, list_gho, j_mx, IDMODE=.TRUE., IGNORENONEXISTENT=.TRUE. ) ! SEPT 2010
!!$    ! synchronize ghosts (they may be added to all the lists)
!!$    ! so, search list_all and add required nodes as ghosts
!!$    IF( BTEST(debug_level,0) ) CALL make_list_and_inform (1, list_all, pos_gho)
#endif
    

#ifdef ADAPT_GRIDDEBUG
    IF ( BTEST(debug_level,2) ) THEN
       WRITE (DU,'(" ")')
       WRITE (DU,'("============== after reconstruction_check ghost =======")')
       CALL count_DB (MAXLIST=list_gho, FILE=DU)!, VERB=.TRUE.)
       WRITE (DU,'("j_lev    =",I3)') j_lev
       WRITE (DU,'("j_lev_a  =",I3)') j_lev_a
       WRITE (DU,'("j_lev_old=",I3)') j_lev_old
       WRITE (DU,'("=======================================================")')
       WRITE (DU,'(" ")')
       DO i=1,n_var
!!$          WRITE (DU,'("MAXVAL(u_old/u)=",E17.10,"/",E17.10)') MAXVAL(ABS(u_old(:,i))), MAXVAL(ABS(u(:,i)))
          WRITE (DU,'("MAXVAL(u)=",E17.10)') MAXVAL(ABS(u(:,i)))
       END DO
       WRITE (DU,'("nwlt/nwlt_old=",I8,"/",I8)') nwlt, nwlt_old
    END IF
#endif
    
    IF (nwlt /= nwlt_old) THEN
       DEALLOCATE (u)  
       ALLOCATE   (u(1:nwlt,1:n_var)) 
       u = 0.0_pr
       
       IF( flag == 0 ) THEN !adapting to initial condition
          IF( ALLOCATED(f) ) DEALLOCATE (f)  
          ALLOCATE   (f(1:nwlt,1:n_integrated))  !startup case  
          f = 0.0_pr !startup case 
       END IF
    END IF
    
!!$  PRINT *, 'before DB_update_type_level'
!!$  CALL count_DB
    CALL DB_update_type_level
!!$  PRINT *, 'after DB_update_type_level'
!!$  CALL count_DB


!!$    IF (par_rank.EQ.1) THEN
!!$       PRINT *, '------------------------------ #1 terminated --------------------------------'
!!$       CALL parallel_finalize; STOP
!!$    END IF
!!$    PAUSE

!!$    CALL DB_PRINT_INFO

!OLEG 04.15.2011:  NEED TO CLARIFY with ALEXEY why we need it.  It seam redundunat, since we already checked the ghost list after rec
!OLEG 04.15.2011: IF CALL make_list_and_inform (1, list_all, pos_gho) (SEE NOTE(***)) is present, perfrming resonstruction check makes each processor having communicating all ghost points to corresponding processors
!OLEG 04.15.2011: I belive the reconstruction check below shoudl be commented
#ifdef MULTIPROC
    ! add nodes which are required for the transform inside the current processor
    ! (some of these nodes may belong to other processors)
    DO trnsf_type =0, n_trnsf_type !OLEG 04.19.2011: this ensures that significant are working correctly.
       DO wlt_fmly = 0, n_wlt_fmly
          CALL reconstr_check_DB (wlt_fmly, trnsf_type, pos_sig, pos_adj)!, par_rank, par_rank)
       END DO
    END DO
    ! make list and request the required nodes (marked as from other processors)
    ! (provide the mask which variables are required)
    CALL make_list_and_request (n_var_interpolate(1,flag), n_var, list_sig, j_mx)   !OLEG 04.15.2011: changed j_mx to j_lev, since there are no significant points above j_lev
#endif

!!$    CALL DB_PRINT_INFO
    

#ifdef ADAPT_GRIDDEBUG
    WRITE (DU,*) 'before transform: j_lev=',j_lev,'i_var_trns_mask',i_var_trns_mask(1:n_var_trns_mask)
    DO i=1,n_var
       WRITE (DU,'("MAXVAL(ABS(u_in(:,",I2,")))=",E22.15)') i, MAXVAL(ABS(u(:,i)))
    END DO
#else
    IF ( BTEST(debug_level,2) ) THEN
       CALL read_DB (u, nwlt, 1, n_var, j_lev)
       IF (par_rank.EQ.0) PRINT *, 'adapt_grid: before transform: j_lev=',j_lev,'i_var_trns_mask',i_var_trns_mask(1:n_var_trns_mask)
       DO i=1,n_var
          tmp1 = MINVAL(u(:,i)); tmp2 = MAXVAL(u(:,i))
          CALL parallel_global_sum( REALMINVAL=tmp1 )
          CALL parallel_global_sum( REALMAXVAL=tmp2 )
          IF (par_rank.EQ.0) WRITE (*,'("MINMAX(u_in(:,",I2,"))=",2E22.15)') i, tmp1, tmp2
       END DO
    END IF
#endif

    
    IF(read_geometry) THEN
       CALL readwrite_i_in (-1) ! reads i_in
    END IF


    !!! OLEG: 07.07.2013  not very efficient way of calucaling, wlt_trns_DB is called multiple times
    !!!!OLD WRONG WAY
!!S    mask = n_var_interpolate(:,flag)
!!$    DO i = 1,n_var !modify later with flag write_DB ....
!!$       IF( mask(i) ) THEN
!!$          CALL wlt_trns_DB (u(:,n_var_index(i)), nwlt, i, i, j_lev, HIGH_ORDER, NORMAL, WLT_TRNS_INV)
!!$       END IF
!!$    END DO
    !!! NEW WAY
    ALLOCATE(u_tmp(nwlt,n_var_trns_mask))
    CALL wlt_trns_DB (u_tmp, nwlt, 1, n_var_trns_mask, j_lev, HIGH_ORDER, NORMAL, WLT_TRNS_INV)
    DO i=1,n_var_trns_mask
       u(:,i_var_trns_mask(i)) = u_tmp(:,i)
    END DO
    DEALLOCATE(u_tmp)

#ifdef ADAPT_GRIDDEBUG
    DO i=1,n_var
       WRITE (DU,'("MAXVAL(ABS(u_out(:,",I2,")))=",E17.10)') i, MAXVAL(ABS(u(:,i)))
    END DO
#else
    IF ( BTEST(debug_level,2) ) THEN
       IF (par_rank.EQ.0) PRINT *, 'adapt_grid: after transform:'
       DO i=1,n_var
          tmp1 = MINVAL(u(:,i)); tmp2 = MAXVAL(u(:,i))
          CALL parallel_global_sum( REALMINVAL=tmp1 )
          CALL parallel_global_sum( REALMAXVAL=tmp2 )
          IF (par_rank.EQ.0) WRITE (*,'("MINMAX(u_out(:,",I2,"))=",2E22.15)') i, tmp1, tmp2
       END DO
    END IF
#endif
    
    

    IF (n_var_exact > 0) THEN
       IF( ALLOCATED(u_ex).AND.(SIZE(u_ex,DIM=1).LT.nwlt.OR.SIZE(u_ex,DIM=1).GT.ISHFT(nwlt,1)) ) DEALLOCATE (u_ex)
       IF( .NOT.ALLOCATED(u_ex) ) ALLOCATE( u_ex(1:nwlt,1:n_var_exact) )
       u_ex = 0.0_pr
    END IF
    
    
    
    
    
    ! delete nodes with zero (or old) id from all link-lists (and from db if possible)       
    CALL DB_update_DB
    
    ! sweep the given list and set function values to 0 and 0.0_pr  
    CALL clean_DB(list_gho,.TRUE.,.TRUE.)
    
    ! set all id bits to zero excepting pos_old bit
    CALL clean_id_DB(list_gho)
    

    ! set list of significant nodes marked as from other processors
    ! (whose values to be received from the others)
    ! and set list of the nodes whose values to be sent to others
    ! set maximum number of variables to transfer in init_DB
    CALL make_lists_for_communications (.TRUE., n_var_to_transfer)



    LOOP_J: DO proc = par_rank, par_rank !0, par_size-1
       DO j = 1, j_mx
          DO wlt_type = MIN(j-1,1),2**dim - 1
             DO face_type = 0, 3**dim - 1
                CALL DB_get_initial_type_level_node (proc,wlt_type,j,face_type,ii,c_pointer,list_sig)
                IF(ii > 0) THEN
                   
                   DO WHILE (is_ok(c_pointer))
#ifdef TREE_NODE_POINTER_STYLE_C
                      CALL DB_get_id_by_pointer (c_pointer,id)
                      id1=IBSET(id,pos_old)
                      CALL DB_set_id_by_pointer (c_pointer,id1)
                      CALL DB_get_next_type_level_node (proc,wlt_type,j,face_type,c_pointer,c_pointer1,list_sig)
                      c_pointer = c_pointer1
#elif defined TREE_NODE_POINTER_STYLE_F
                      CALL DB_get_id_by_pointer (c_pointer,id)
                      id1=IBSET(id,pos_old)
                      CALL DB_set_id_by_pointer (c_pointer,id1)
                      CALL DB_get_next_type_level_node (c_pointer)
#endif      
                   END DO
                   
                END IF
             END DO
          END DO
       END DO
    END DO LOOP_J
    

#ifdef ADAPT_GRIDDEBUG
    IF( BTEST(debug_level,2) ) THEN
       WRITE (DU,'(" ")')
       WRITE (DU,'("==================== end of adapt_grid ================")')
       CALL count_DB (MAXLIST=list_gho, FILE=DU)
       WRITE (DU,'("j_lev      = ", I6)') j_lev
       WRITE (DU,'("j_lev_a    = ", I6)') j_lev_a
       WRITE (DU,'("j_lev_old  = ", I6)') j_lev_old
       WRITE (DU,*) 'new_grid=', new_grid
       WRITE (DU,'("=======================================================")')
       WRITE (DU,'(" ")')
       !CALL DB_print_info
    END IF
#endif

    
    ! number of ghosts will be different for parallel code
    ! (external ghosts are not propagated to their processors)
    ! set used in main global variable nwlt_p_ghost
    ! without printing of output in count_DB
    CALL count_DB( OUTPUT=.FALSE. )
    
    
!!$    IF( BTEST(debug_level,2) ) THEN
!!$       OPEN (UNIT=DU2, FILE='debug.short_adapt_grid'//par_rank_str,POSITION='APPEND')
!!$       CALL count_DB (MAXLIST=list_gho, FILE=DU2)
!!$       CLOSE (DU2)
!!$    END IF
    


#ifdef ADAPT_GRIDDEBUG
    CLOSE (DU)
#undef ADAPT_GRIDDEBUG
#endif
    
    
    CALL timer_stop(3)
    
    IF ( BTEST(debug_level,1) ) PRINT *, 'leaving adapt_grid:'
    !CALL count_DB (MAXLIST=list_gho, FILE=6); PAUSE
    
    !CALL count_DB_InternalBoundary( OUTPUT=.TRUE., FILE=666, VERB=.TRUE. )
    
  END SUBROUTINE adapt_grid


  !--------------------------------------------------------------------------------
  ! set the tree and the domain decomposition
  SUBROUTINE pre_init_DB
    USE user_case_db            ! max_dim, n_var_db
    USE db_tree                 ! init_DB_tree_aux
    
    CALL init_DB_tree_aux( n_var_db ) !use max_dim in the future
    
  END SUBROUTINE pre_init_DB

  
  !--------------------------------------------------------------------------------
  !
  ! Initialize new Field data base
  !
  RECURSIVE SUBROUTINE init_DB (nwlt_old_local, nwlt_local, nwltj_old, nwltj, new_grid, &
       j_lev_old, j_lev_a, j_mn_l, j_mx_l, max_nxyz, nxyz_a, IC_from_restart_file, &
       SECOND_CALL, POST_PROCESS)
    USE precision
    USE user_case_db            ! max_dim, n_var_db
    USE pde                     ! n_var, nwlt
    USE variable_mapping 
    USE share_consts
    USE field
    USE wlt_vars                ! nxyz, n_prdct
    USE io_3D_vars              ! IC_from_file
    USE additional_nodes        ! initialize_additional_nodes
    USE db_tree                 ! ..._DB
    USE parallel                ! par_size
    USE util_vars               ! x
!!$    USE debug_vars ! debug_print_u()


    IMPLICIT NONE
    INTEGER, INTENT (IN) :: j_mn_l, j_mx_l, max_nxyz, nxyz_a(1:3) 
    INTEGER, INTENT (INOUT) :: j_lev_old, nwlt_local, nwlt_old_local, nwltj, nwltj_old, j_lev_a
    LOGICAL, INTENT (INOUT) :: new_grid
    LOGICAL, INTENT (IN)    ::  IC_from_restart_file  ! true if restart or IC from restart file
    LOGICAL, INTENT (IN), OPTIONAL :: SECOND_CALL     ! true if n_prdct has to be changed
    LOGICAL, INTENT (IN), OPTIONAL :: POST_PROCESS    ! true if called from from res2vis 
    INTEGER :: wlt_fmly, trnsf_type, ireg, i, jmn
    LOGICAL :: mask(1:n_var), is_second_call, is_post_process
    INTEGER :: ii, j, ie, k
    INTEGER :: flag, wlt_type, face_type, id, id1, proc

#ifdef TREE_NODE_POINTER_STYLE_C
    INTEGER(pointer_pr) :: c_pointer, c_pointer1      ! pointer to node of tree structure (integer in C/C++)
#elif defined TREE_NODE_POINTER_STYLE_F
    TYPE(node), POINTER :: c_pointer, c_pointer1           ! (Fortran's native pointer)
#endif

    REAL(pr) :: tmp1, tmp2
    INTEGER :: ixyz(dim), nwlt_tmp, &
         n_prdct_inp(0:n_wlt_fmly), &                 ! temporal storage for resetting
         n_updt_inp(0:n_wlt_fmly)                     ! n_prdct and n_updt
    LOGICAL, PARAMETER :: tmp_void(1) = .TRUE.        !06.20.2011!O+AR!

#ifdef INIT_DBDEBUG
    INTEGER, PARAMETER :: DU = 553
    OPEN (UNIT=DU, FILE='debug.init_db'//par_rank_str)
#endif


    ! To ensure that set_max_assymetry is properly set up for visualization from restart file 
    is_post_process = .FALSE.
    IF (PRESENT(POST_PROCESS)) is_post_process = POST_PROCESS
    
    ! n_prdct change during restart will cause the second call of init_DB
    is_second_call = .FALSE.
    IF (PRESENT(SECOND_CALL)) is_second_call = SECOND_CALL
    ! store .inp values and set current to .res
    IF (IC_from_restart_file) THEN
       IF( BTEST(debug_level,2) ) PRINT *,'is_second_call = ',is_second_call
       n_prdct_inp = n_prdct
       n_updt_inp = n_updt
       n_prdct = n_prdct_resfile
       n_updt = n_updt_resfile
       ! redefine initialized before values (set it as a function)
       maxval_n_updt = maxval(n_updt)
       maxval_n_prdct = maxval(n_prdct)
       ! set n_assym_prdct/prdct_bnd/updt/updt_bnd
       !CALL set_max_assymetry(is_post_porcess)
       CALL set_max_assymetry_from_resfile(is_post_process)
    END IF
    
    
    
    SELECT CASE (IC_restart_mode) ! new run / hard restart / soft restart / restart from IC
    CASE (0)
       flag = 0
       jmn  = j_mn_init
    CASE (1)
       flag = 1          ! Set flag which n_var_interpolate(:,:) variables to use.
       jmn  = j_mn_l     ! If we are restarting we use jmn as the lowest level instead of j_mn_init.
    CASE (2)
       flag = 1
       jmn  = j_mn_l
    CASE (3)
       flag = 0
       jmn  = j_mn_l
    END SELECT
    
    
    
    ! initialize lh bounds
    IF (is_second_call) THEN
       CALL setup_lh_bounds_DB (RESET=5) ! reset lh_wlt (1) and wgh_DB (4) only as dependent on n_updt/prdct
    ELSE
       CALL setup_lh_bounds_DB           ! set_xx() and all: lh_wlt, lh_diff, wgh_DB, wgh_df_DB
       CALL initialize_additional_nodes  ! could be called once for all the parameters
    END IF


    ! Set flag for indices, ireg = 0 normal, or ireg=-1 for restart
    ! IC_file_fmt == 0 means native restart file format
    ireg = 0
    IF( IC_from_restart_file ) THEN
       ireg = -1
       IF (is_second_call) ireg = 1
    END IF
    
    
    ! 1) add nwlt nodes, with U values, to the database from indx_DB, allocate x(nwlt) (ireg=-1)
    !    or set initial regular grid (ireg=0), or do nothing (ireg=1)
    ! 2) set inverse mapping for points in sig list (if ireg != -1), count nwlt,
    !    reallocate x(nwlt), xx, etc (if necessary), set new_grid
    CALL indices_tree (nwlt_old_local, nwlt_local, new_grid, j_lev_old, j_lev_a, jmn, j_mx_l, ireg)

#ifdef MULTIPROC
    CALL add_ghost_DB_parallel_antea (par_rank, par_rank)
#endif

    CALL add_ghost_DB (par_rank, par_rank)
!!$#ifdef MULTIPROC
!!$    IF( BTEST(debug_level,0) ) CALL add_ghost_DB_parallel_postea ( CLEAN=.FALSE. )
!!$#endif
#ifdef INIT_DBDEBUG
    IF( BTEST(debug_level,2) ) THEN
       WRITE (DU,'(" ")')
       WRITE (DU,'("================= init_DB: after add_ghosts_DB ========")')
       CALL count_DB (MAXLIST=list_gho, FILE=DU)
       WRITE (DU,'("j_lev=",I3)') j_lev
       WRITE (DU,'(" ")')
    END IF
#endif
    
    
    ! rewrite/create indx_DB from the nodes of sig list of the current processor
    ! of the database (take inverse mapping, etc, from the database)
    CALL indices_DB (par_rank, par_rank)

    IF(read_geometry) THEN
       CALL readwrite_i_in (0) ! initialize i_in
    END IF
    
    ! allocate field variables (not restart)
    ! or write existing U into the database (restart)
    IF( .NOT. IC_from_restart_file )  THEN
       ALLOCATE (  u(1:nwlt_local,1:n_var), f(1:nwlt_local,1:n_integrated))
       f = 0.0_pr ; u = 0.0_pr
!!$    ELSE
!!$       ! moved inside indices_tree(ireg=-1)
!!$!!!!!!!!!!!!!!!! OLEG: test if I can onlywrite it to DB while INV transform
!!$       CALL update_db_from_u(  u , nwlt_local ,n_var  , 1, n_var, 1  )
    END IF
    
    
    ! add nodes which are required for the transform inside the current processor
    ! some of these nodes may belong to other processors.
    ! ( This is required for c_diff_fast(), which may be called right after
    !   init_DB() in user_initial_conditions(), before adapt_grid().
    !   Value synchronization will be done directly in c_diff_fast()
    !   after the value of u is known )
#ifdef MULTIPROC
    !OLEG: 06/28/2011 - this loop is required if the mesh is changed due to additional adaptation
    DO trnsf_type =0,n_trnsf_type
       DO wlt_fmly = 0, n_wlt_fmly
          CALL reconstr_check_DB (wlt_fmly, trnsf_type, pos_sig, pos_adj)
       END DO
    END DO
#ifdef INIT_DBDEBUG
    IF( BTEST(debug_level,4) ) THEN
       WRITE (DU,'("=======================================================")')
       WRITE (DU,*) 'after adding neighbours in init_DB at proc', par_rank
       CALL count_DB (MAXLIST=list_gho, FILE=DU)
       WRITE (DU,'(" ")')
    END IF
#endif
#endif
    DO trnsf_type =0,n_trnsf_type
       DO wlt_fmly = 0, n_wlt_fmly
          CALL reconstr_check_DB (wlt_fmly, trnsf_type, pos_sig, pos_gho)!, par_rank, par_rank)
       END DO
    END DO

!06.20.2011!O+AR!
! Analogous to adapt_grid  
#ifdef MULTIPROC
    CALL make_list_and_request( tmp_void, 1, list_gho, j_mx, IDMODE=.TRUE., IGNORENONEXISTENT=.TRUE. )       
#endif

#ifdef INIT_DBDEBUG
    IF( BTEST(debug_level,2) ) THEN
       WRITE (DU,'("=== init_DB: after indices_DB and reconstruction check ghosts ==")')
       CALL count_DB (MAXLIST=list_gho, FILE=DU)
       WRITE (DU,'("j_lev=",I3)') j_lev
       WRITE (DU,'(" ")')
    END IF
#endif

    
    ! make sure all sig and adj are in sig list
    ! and all gho and others are in gho list
    ! (  indices_tree() may set some IDs to 0  )
    CALL DB_update_type_level

#ifdef INIT_DBDEBUG
    IF ( BTEST(debug_level,4) ) THEN
       WRITE (DU,*) 'after DB_update_type_level'
       CALL count_DB (MAXLIST=list_gho, FILE=DU)
       WRITE (DU,'(" ")')
    END IF
#endif    
    
    
    
    !
    ! make inverse transform in case of restart
    IF (IC_from_restart_file) THEN
       ! Add nodes which are required for the transform inside the current processor
       ! (some of these nodes may belong to other processors)
       IF (par_size.GT.1) THEN
          DO trnsf_type =0,n_trnsf_type
             DO wlt_fmly = 0, n_wlt_fmly
                CALL reconstr_check_DB (wlt_fmly, trnsf_type, pos_sig, pos_adj)!, par_rank, par_rank)
             END DO
          END DO
#ifdef INIT_DBDEBUG
          IF( BTEST(debug_level,4) ) THEN
             WRITE (DU,'("=======================================================")')
             WRITE (DU,*) 'after adding neighbours in init_DB at proc', par_rank
             CALL count_DB (MAXLIST=list_gho, FILE=DU)
             WRITE (DU,'(" ")')
          END IF
#endif
          ! make list and request the required nodes
          ! (provide the mask which variables are required)
#ifdef MULTIPROC
          CALL make_list_and_request (n_var_interpolate(1,flag), n_var, list_sig, j_mx)
#endif
       END IF
       
       
       
       ! recompute wavelet coefficients if the coefficients are stored in .res file
       ! and wavelet transform update/predict order in .inp differs from that one in .res
       IF ( (.NOT.is_second_call) )  THEN
          IF ( BTEST(debug_level,2) ) THEN
             IF (par_rank.EQ.0) PRINT *, 'init_DB: before transform: j_lev=',j_lev,'mask', n_var_interpolate(:,flag)
             DO i=1,n_var
                tmp1 = MINVAL(u(:,i)); tmp2 = MAXVAL(u(:,i))
                CALL parallel_global_sum( REALMINVAL=tmp1 )
                CALL parallel_global_sum( REALMAXVAL=tmp2 )
                IF (par_rank.EQ.0) WRITE (*,'("MINMAX(u_in(:,",I2,"))=",2E22.15)') i, tmp1, tmp2
             END DO
          END IF
!!$       CALL debug_print_u (u, nwlt_local, n_var)
          
          !************ Inverse Wavelet Transform ****************
          ! Do inverse wlt transform on n_var_interpolate variables (logical mask)
          ! (use nodes of all the processors (since some were added during the reconstruction
          !  and may belong to other processors)
          mask = n_var_interpolate(:,flag)

          !!! OLEG: 07.07.2013  not very efficient way of calucaling, wlt_trns_DB is called multiple times
          DO i = 1,n_var !modify later with flag write_DB ....
             IF( mask(i) ) THEN
                CALL wlt_trns_DB (u(:,n_var_index(i)), nwlt_local, i, i, j_lev, HIGH_ORDER, NORMAL, WLT_TRNS_INV, &
                     SKIP_TRANSFORM = .NOT.res_save_coeff_IC )
             END IF
          END DO
          !************ Inverse Wavelet Transform ****************
          
          IF ( BTEST(debug_level,2) ) THEN
             IF (par_rank.EQ.0) PRINT *, 'init_DB: after transform:'
             DO i=1,n_var
                tmp1 = MINVAL(u(:,i)); tmp2 = MAXVAL(u(:,i))
                CALL parallel_global_sum( REALMINVAL=tmp1 )
                CALL parallel_global_sum( REALMAXVAL=tmp2 )
                IF (par_rank.EQ.0) WRITE (*,'("MINMAX(u_out(:,",I2,"))=",2E22.15)') i, tmp1, tmp2
             END DO
          END IF
!!$       CALL debug_print_u (u, nwlt_local, n_var)
       END IF


       ! real values are in U and the database, set .inp values for the computing to follow
       ! if it is the first call and the values are different
       IF ( .NOT.is_second_call .AND. &
            ANY( n_prdct_inp(0:n_wlt_fmly).NE.n_prdct_resfile(0:n_wlt_fmly)) .OR. &
            ANY( n_updt_inp(0:n_wlt_fmly).NE.n_updt_resfile(0:n_wlt_fmly)) ) THEN
          n_prdct = n_prdct_inp
          n_updt = n_updt_inp
          n_prdct_resfile = n_prdct_inp
          n_updt_resfile = n_updt_inp
          CALL init_DB (nwlt_old_local, nwlt_local, nwltj_old, nwltj, new_grid, &
               j_lev_old, j_lev_a, j_mn_l, j_mx_l, max_nxyz, nxyz_a, IC_from_restart_file, &
               SECOND_CALL=.TRUE., POST_PROCESS = is_post_process)
          IF ( BTEST(debug_level,2) ) PRINT *, 'init_DB: return after second call completion'
          RETURN
       END IF
       

    END IF

    
    IF(n_var_exact > 0) THEN
       IF( ALLOCATED(u_ex) ) THEN
          IF ( SIZE(u_ex,DIM=1).LT.nwlt_local.OR.SIZE(u_ex,DIM=1).GT.ISHFT(nwlt_local,1) ) DEALLOCATE (u_ex)
       END IF
       IF( .NOT.ALLOCATED(u_ex) ) ALLOCATE( u_ex(1:nwlt_local,1:n_var_exact) )
       u_ex = 0.0_pr
    END IF
    


!!$    IF( BTEST(debug_level,2) ) THEN
!!$       PRINT *, 'before DB_update_DB'
!!$       CALL count_DB
!!$    END IF

    
    ! delete nodes with zero id from all link-lists (and from db if possible)
    CALL DB_update_DB
#ifdef INIT_DBDEBUG
    IF( BTEST(debug_level,4) ) THEN
       WRITE (DU,'("=======================================================")')
       WRITE (DU,'("after DB_update_DB")')
       CALL count_DB (MAXLIST=list_gho, FILE=DU)
       WRITE (DU,'(" ")')
    END IF
#endif
    
    
    ! sweep the given list and set function values to 0 and 0.0_pr
    CALL clean_DB(list_gho,.TRUE.,.TRUE.)
#ifdef INIT_DBDEBUG
    IF( BTEST(debug_level,4) ) THEN
       WRITE (DU,'("=======================================================")')
       WRITE (DU,'("after clean_DB")')
       CALL count_DB (MAXLIST=list_gho, FILE=DU)
    END IF
#endif        
    
    
    ! set all id bits to zero excepting pos_old bit
    CALL clean_id_DB(list_gho)
#ifdef INIT_DBDEBUG
    IF( BTEST(debug_level,4) ) THEN
       WRITE (DU,'("=======================================================")')
       WRITE (DU,'("after clean_id_DB")')
       CALL count_DB (MAXLIST=list_gho, FILE=DU)
       WRITE (DU,'(" ")')
    END IF
#endif


    ! set list of significant nodes marked as from other processors
    ! (whose values to be received from the others)
    ! and set list of the nodes whose values to be sent to others
    ! set maximum number of variables to transfer here, in init_DB
    !Oleg 02.05.2011: For problems that require derivatives of large vector funciton n_var_db could be larger than n_var, 
    !                 resulting in larger size of DB, larger communication cost in adapt grid, 
    !                 yet the derivatives will be calcualted faster.  
    n_var_to_transfer = n_var_db                                           ! at least, n_var
    IF (sgsmodel.NE.0) n_var_to_transfer = MAX( n_var_to_transfer, 6 )  ! tau_ij(6) in Smagorinsky model
    CALL make_lists_for_communications (.TRUE., n_var_to_transfer)

    
    ! mark nodes in significant list of the database
    ! as OLD
    LOOP_J: DO proc = par_rank, par_rank
       DO j = 1, j_mx
          DO wlt_type = MIN(j-1,1),2**dim - 1
             DO face_type = 0, 3**dim - 1
                CALL DB_get_initial_type_level_node (proc,wlt_type,j,face_type,ii,c_pointer,list_sig)
                IF(ii > 0) THEN
                   
                   DO WHILE (is_ok(c_pointer))
#ifdef TREE_NODE_POINTER_STYLE_C
                      CALL DB_get_id_by_pointer (c_pointer,id)
                      id1=IBSET(id,pos_old)
                      CALL DB_set_id_by_pointer (c_pointer,id1)
                      CALL DB_get_next_type_level_node (proc,wlt_type,j,face_type,c_pointer,c_pointer1,list_sig)
                      c_pointer = c_pointer1
#elif defined TREE_NODE_POINTER_STYLE_F
                      CALL DB_get_id_by_pointer (c_pointer,id)
                      id1=IBSET(id,pos_old)
                      CALL DB_set_id_by_pointer (c_pointer,id1)
                      CALL DB_get_next_type_level_node (c_pointer)
#endif                      
                   END DO
                   
                END IF
             END DO
          END DO
       END DO
    END DO LOOP_J
    
#ifdef INIT_DBDEBUG
    IF( BTEST(debug_level,2) ) THEN
       WRITE (DU,'(" ")')
       WRITE (DU,'("==================== end of init_DB ================")')
       CALL count_DB (MAXLIST=list_gho, FILE=DU, VERB=.TRUE.)
       WRITE (DU,'("j_lev      = ", I6)') j_lev
       WRITE (DU,'("j_lev_a    = ", I6)') j_lev_a
       WRITE (DU,'("j_lev_old  = ", I6)') j_lev_old
       WRITE (DU,*) 'new_grid=', new_grid
       WRITE (DU,'("=======================================================")')
       WRITE (DU,'(" ")')
    END IF
    CLOSE (DU)
#undef INIT_DBDEBUG
#endif

    ! number of ghosts will be different for parallel code
    ! (external ghosts are not propagated to their processors)
    ! set used in main global variable nwlt_p_ghost
    ! without printing of output in count_DB
    CALL count_DB( OUTPUT=.FALSE. )
    IF( BTEST(debug_level,2) ) PRINT *, 'init_DB: finished'
    !CALL count_DB (MAXLIST=list_gho, FILE=6)
    
  END SUBROUTINE init_DB
  

  SUBROUTINE indices_tree (nwlt_old_l, nwlt_l, new_grid, j_lev_old, j_lev_a, j_mn_local, j_mx_local, ireg, &
       COMPUTE_new_grid )
    USE precision
    USE sizes             ! nwlt
    USE wlt_vars          ! dim,n nxyz
    USE user_case_db      ! max_dim, n_var_db
    USE util_vars
    USE share_consts
    USE db_tree
    USE wlt_trns_util_mod
    USE debug_vars
    USE pde               ! n_var, nwlt
    USE variable_mapping 
    USE field             ! U
    USE parallel          ! par_size
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: j_mn_local, j_mx_local, ireg
    INTEGER, INTENT (INOUT) :: nwlt_l, nwlt_old_l, j_lev_old, j_lev_a
    LOGICAL, INTENT (INOUT) :: new_grid
    LOGICAL, INTENT(IN), OPTIONAL :: COMPUTE_new_grid             ! do set output new_grid value
    LOGICAL :: lchk
    INTEGER :: i,ii,ih,j,k,l,meth,nD,wlt_fmly,trnsf_type                                                    
    INTEGER :: ix,ix_l,ix_h,ixpm,ix_even,ix_odd
    INTEGER :: iy,iy_l,iy_h
    INTEGER :: iz,iz_l,iz_h
    INTEGER :: jxyz(dim), jxyz_max, ixyz(dim), ixyz2
    INTEGER , DIMENSION(6)::  ibch
    INTEGER :: idim
    REAL (pr) :: dxyz(dim)
    !------------ arrays used for indx_DB --------------------
    INTEGER :: j_df, j_lv, wlt_type, face_type, ishift, ibndr, proc
    INTEGER :: face_dim, face_val
    INTEGER, DIMENSION(dim) :: wlt, face
    INTEGER*8, DIMENSION(0:dim) :: i_p, i_p_prd, i_p_face, i_p_wlt             
    !!                                              !!!!!!!!!!!!!!!!!!!!!! N O T E !!!!!!!!!!!!!!!!!!!!!!!!!      !AR 2/21/2011! added by AR
    !!                                              !!! AR IS NOT SURE WHETHER                           !!!      !AR 2/21/2011! added by AR
    !!                                              !!!   i_p_prd, i_p_face, i_p_wlt   must be INTEGER*8 !!!      !AR 2/21/2011! added by AR
    !!                                              !!!   to be conservative, all of them are changed    !!!      !AR 2/21/2011! added by AR
    !!                                              !!!   Tests showed that they are very large.         !!!      !AR 2/22/2011! added by AR
    !!                                              !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!      !AR 2/21/2011! added by AR
    
    
!!$    LOGICAL, DIMENSION(:,:), ALLOCATABLE :: face_assigned
    !-----------   DB specific variables -------------------------------
    INTEGER :: id,ierr, count, i0, cproc
    REAL (pr), DIMENSION (1:n_var) :: tmp_u           ! temporal extract from u (to shut up compiler warning)
    LOGICAL :: do_COMPUTE_new_grid                    ! do set output new_grid value

#ifdef TREE_NODE_POINTER_STYLE_C
    INTEGER(pointer_pr) :: c_pointer, c_pointer1      ! pointer to node of tree structure (integer in C/C++)
#elif defined TREE_NODE_POINTER_STYLE_F
    TYPE(node), POINTER :: c_pointer, c_pointer1      ! (Fortran's native pointer)
#endif


    do_COMPUTE_new_grid = .TRUE.
    IF (PRESENT(COMPUTE_new_grid)) do_COMPUTE_new_grid = COMPUTE_new_grid
    

    !------------------------------------------------------------------    
    
    IF (ireg == -1 ) THEN !Restart version

       ! pass through the loaded indx_DB list and insert the nodes
       ! into the database as significant and old
       ! (add U values to the database)
       i_p(0) = 1
       DO i=1,dim
          i_p(i) = i_p(i-1)*(1+nxyz(i))
       END DO
       ! coordinates indx_DB()%p()%ixyz <--> nxyz <--> j_lev
       ! (during creation of indx_DB in indices_DB subroutine)


       !O 06.28.2011: begin changes
       CALL DB_update_DB ! all ghost points that are not significant or adjacent are deleted
       !O 06.28.2011: end changes

       count = 0
       DO j = 1, j_lev
          DO wlt_type = MIN(j-1,1),2**dim-1
             DO face_type = 0, 3**dim - 1
                DO j_df = j, j_lev
                   DO k = 1, indx_DB(j_df,wlt_type,face_type,j)%length
                      i = indx_DB(j_df,wlt_type,face_type,j)%p(k)%i             ! nwlt index of the node
                      ixyz(1:dim) = INT(MOD(indx_DB(j_df,wlt_type,face_type,j)%p(k)%ixyz-1,i_p(1:dim))/i_p(0:dim-1))                             
                      CALL DB_get_proc_by_coordinates (ixyz, j_lev, cproc)
                      IF (cproc.EQ.par_rank) THEN
                         count = count + 1
                         CALL DB_add_node( ixyz, j_lev, ierr, c_pointer, idp(pos_sig)+idp(pos_old), end_list )
                         
                         tmp_u = u(i,1:n_var)
                         CALL DB_set_function_by_pointer( c_pointer, 1, n_var, tmp_u )            ! add U values

!***************************************************************************************************************
!O 06.28.2011: begin changes                         
! the inverse mapping is wrong at this point due to wrong order, will be done later, no need to do it here.
!#ifdef TREE_NODE_POINTER_STYLE_C
!                         CALL DB_set_ifunction_by_pointer( c_pointer, nvarI_nwlt, nvarI_nwlt, count ) 
!#elif defined TREE_NODE_POINTER_STYLE_F
!                         c_pointer%ival( nvarI_nwlt ) = count
!#endif
!O 06.28.2011: end changes                        
 !***************************************************************************************************************
                      END IF
                   END DO
                END DO
             END DO
          END DO
       END DO
       IF( BTEST(debug_level,2) ) PRINT *,'nxyz=',nxyz, 'nwlt=',nwlt_l, '#set_id=',count
       
       ! reallocate U for the nodes of the current processor only
       ! (read_solution could read some extra nodes during domain re-decomposition)
       IF (count.NE.nwlt_l) THEN
          IF (ALLOCATED(u)) DEALLOCATE (u)
          ALLOCATE ( u(1:count,1:n_var) )
          u = 0.0_pr
          nwlt_l = count
       END IF
       IF (ALLOCATED(h)) DEALLOCATE (h)
       IF (ALLOCATED(x)) DEALLOCATE (x)
       IF (ALLOCATED(Nwlt_lev)) DEALLOCATE (Nwlt_lev)
       ALLOCATE( h(1:j_lev,1:dim), x(1:nwlt_l,1:dim), Nwlt_lev(1:j_mx_local,0:1) )
       x = 0.0_pr
       h = 0.0_pr

       ! ------------ define new grid coordinates ---------------------------------
       CALL set_xx (xx(0:MAXVAL(nxyz(1:dim)),1:dim),h(1:j_lev,1:dim),nxyz(1:dim),MAXVAL(nxyz(1:dim)),j_lev)
      
       j_lev_old=j_lev
       nwlt_old_l = nwlt_l
       
    ELSE IF (ireg == 0) THEN !first initialization call

       j_lev = MAX(j_mn_local,j_lev_init) ! set current wavelet transform level to at least minimum level
       j_lev = MIN( j_lev, j_mx_local ) ! set current wavelet transform level to below the max level
       nxyz(1:dim)=mxyz(1:dim)*2**(j_lev-1) !calculate non-adaptive resolution from levels and M
       maxval_nxyz = MAXVAL(nxyz)
       i_p_prd(0) = 1
       i_p_prd(1:dim) = nxyz(1:dim)+1-prd(1:dim)
       DO i =1, dim
          i_p_prd(i)  = i_p_prd(i-1) *i_p_prd(i)
       END DO

       ! add initial regular grid nodes (belonging to the current processor)
       DO i=1,i_p_prd(dim)
          ixyz(1:dim) = INT(MOD(i-1,i_p_prd(1:dim))/i_p_prd(0:dim-1))
          CALL DB_get_proc_by_coordinates (ixyz, j_lev, cproc)
          IF (cproc.EQ.par_rank) &
               CALL set_id (ixyz, j_lev, pos_sig, end_list)
       END DO
       nwlt_l = PRODUCT((1-prd(1:dim)+mxyz(1:dim)*2**(j_lev-1)))
       !------------------ setting id=0 to points that are above j_lev
       id = 0
       proc = par_rank
       DO j = j_lev+1,j_tree_root  ! sets id=0 for all points for j: j_lev < j <= j_tree_root, if j_lev < j_tree_root
          DO wlt_type = MIN(j-1,1),2**dim-1
             DO face_type = 0, 3**dim - 1
                CALL DB_get_initial_type_level_node (proc,wlt_type,j,face_type,ii,c_pointer,list_sig)
                IF(ii > 0) THEN
                   
                   DO WHILE (is_ok(c_pointer))
#ifdef TREE_NODE_POINTER_STYLE_C
                      CALL DB_set_id_by_pointer ( c_pointer, id )
                      CALL DB_get_next_type_level_node (proc,wlt_type,j,face_type,c_pointer,c_pointer1,list_sig)
                      c_pointer = c_pointer1
#elif defined TREE_NODE_POINTER_STYLE_F
                      CALL DB_set_id_by_pointer ( c_pointer, id )
                      CALL DB_get_next_type_level_node (c_pointer)
#endif
                   END DO
                   
                END IF
             END DO
          END DO
       END DO
       
       CALL DB_MOVE_ZERO_SIG_TO_GHO  !moves zero id from sig_list to ghost list
       
       IF (ALLOCATED (Nwlt_lev)) DEALLOCATE (Nwlt_lev)
       ALLOCATE(Nwlt_lev(1:j_mx_local,0:1)) ! allocates only once for all levels up to j_mx_local
       nwlt_old_l=0
       j_lev_old=0
    ELSE
       j_lev_old = j_lev
       nwlt_old_l = nwlt_l  ! nwlt before that subroutine has been call
    END IF

    !
    ! ----- Setting nwlt, Nwlt_lev, and inverse mapping ----------------------
    !
    nwlt_l = 0
    Nwlt_lev = 0
    !-------------- ALL internal points 
    face_type = (3**dim - 1)/2
    DO proc = par_rank, par_rank !0, par_size-1
       DO j = 1, j_mx
          DO wlt_type = MIN(j-1,1),2**dim-1
             CALL DB_get_initial_type_level_node (proc,wlt_type,j,face_type,ii,c_pointer,list_sig)
             IF(ii > 0) THEN
                
                DO WHILE (is_ok(c_pointer))
#ifdef TREE_NODE_POINTER_STYLE_C
                   nwlt_l = nwlt_l + 1
                   !IF (ireg.NE.-1)
                   CALL DB_set_ifunction_by_pointer( c_pointer, nvarI_nwlt, nvarI_nwlt, nwlt_l ) ! setting indx for inverse mapping
                   CALL DB_get_next_type_level_node (proc,wlt_type,j,face_type,c_pointer,c_pointer1,list_sig)
                   c_pointer = c_pointer1
#elif defined TREE_NODE_POINTER_STYLE_F
                   nwlt_l = nwlt_l + 1
                   !IF (ireg.NE.-1)
                   c_pointer%ival( nvarI_nwlt ) = nwlt_l  ! setting indx for inverse mapping
                   CALL DB_get_next_type_level_node (c_pointer)
#endif
                END DO
                
             END IF
             Nwlt_lev(j,0) = Nwlt_lev(j,0)+ii !internal points only on level j
             Nwlt_lev(j,1) = Nwlt_lev(j,1)+ii !all points on level j
          END DO
       END DO
    END DO
    
    !------------- ALL face (external) points
    DO proc = par_rank, par_rank !0, par_size-1
       DO j = 1, j_mx
          DO face_type = 0, 3**dim - 1
             IF( face_type /= (3**dim-1)/2 ) THEN
                DO wlt_type = MIN(j-1,1),2**dim-1
                   CALL DB_get_initial_type_level_node (proc,wlt_type,j,face_type,ii,c_pointer,list_sig)
                   IF(ii > 0) THEN
                      
                      DO WHILE (is_ok(c_pointer))
                         nwlt_l = nwlt_l + 1
#ifdef TREE_NODE_POINTER_STYLE_C
                         CALL DB_set_ifunction_by_pointer( c_pointer, nvarI_nwlt, nvarI_nwlt, nwlt_l )
                         CALL DB_get_next_type_level_node (proc,wlt_type,j,face_type,c_pointer,c_pointer1,list_sig)
                         c_pointer = c_pointer1
#elif defined TREE_NODE_POINTER_STYLE_F
                         c_pointer%ival( nvarI_nwlt ) = nwlt_l
                         CALL DB_get_next_type_level_node (c_pointer)
#endif
                      END DO
                      
                   END IF
                   Nwlt_lev(j,1) = Nwlt_lev(j,1)+ii !all points on level j
                END DO
             END IF
          END DO
       END DO
    END DO
    
    DO j=2, j_mx
       Nwlt_lev(j,:) = Nwlt_lev(j-1,:) + Nwlt_lev(j,:) !points on level j and below, (j,0) - internal, (j,1) - all 
    END DO
    
    nwlt_intrnl = Nwlt_lev(j_lev,0)
    nwlt_bnd = nwlt_l - Nwlt_lev(j_lev,0)

    !* IMPORTANT - works only prd(1:dim)=1, does not reorder if at least one direction is non-periodic
    IF (debug_force_wrk_wlt_order .AND. MINVAL(prd(1:dim)) == 1 .AND. ireg /= -1 .AND. par_size.EQ.1) THEN
       PRINT *, '-------------------------------------------------------'
       PRINT *, 'WARNING: for testing only'
       PRINT *, '( db_tree wavelet ordering set similar to db_wrk one )'
       PRINT *, '-------------------------------------------------------'
       i_p_prd(0) = 1
       PRINT *, 'nwlt_before=', nwlt_l
       nwlt_l=0
       DO j =1, j_lev
          i_p_prd(1:dim) = mxyz(1:dim)*2**(j-1) + 1-prd(1:dim)
          DO i =1, dim
             i_p_prd(i)  = i_p_prd(i-1) *i_p_prd(i)
          END DO
          PRINT *, 'j=',j,'grid=',mxyz(1:dim)*2**(j-1), 'nxyz=',nxyz,'ip=',i_p_prd(dim)
          DO i=1,i_p_prd(dim)
             ixyz(1:dim) = INT(MOD(i-1,i_p_prd(1:dim))/i_p_prd(0:dim-1))
             IF(MAXVAL(MOD(ixyz(1:dim),2)) == 1 .OR. j==1 ) THEN
                CALL DB_test_node(ixyz(:),j,c_pointer)

                IF (is_ok(c_pointer)) THEN
#ifdef TREE_NODE_POINTER_STYLE_C
                   CALL DB_test_if_in_sig_list_by_pointer( c_pointer, ii )
                   IF (ii.NE.0) THEN
                      nwlt_l=nwlt_l+1
                      !PRINT *, nwlt_l, ixyz(1:dim)*2**(j_lev-j)
                      CALL DB_set_ifunction_by_pointer( c_pointer, nvarI_nwlt, nvarI_nwlt, nwlt_l ) ! setting indx for inverse mapping
                   END IF
#elif defined TREE_NODE_POINTER_STYLE_F
                   CALL DB_test_if_in_sig_list_by_pointer( c_pointer, ii )
                   IF (ii.NE.0) THEN
                      nwlt_l=nwlt_l+1
                      !PRINT *, nwlt_l, ixyz(1:dim)*2**(j_lev-j)
                      c_pointer%ival( nvarI_nwlt ) = nwlt_l  ! setting indx for inverse mapping
                   END IF
#endif
                END IF

             END IF
          END DO
          PRINT *, 'nwlt_lev=', nwlt_l
       END DO
       PRINT *, 'nwlt_after=', nwlt_l
    END IF
    !
    !-------------- mesh reassignment ---------------------------
    !
    IF(j_lev_old /= j_lev .OR. ireg == 0) THEN 
       nxyz(1:dim)=mxyz(1:dim)*2**(j_lev-1)
       maxval_nxyz = MAXVAL(nxyz)
       IF (ALLOCATED(xx)) DEALLOCATE(xx); ALLOCATE(xx(0:MAXVAL(nxyz),1:dim))
       IF (ALLOCATED(h)) DEALLOCATE(h); ALLOCATE(h(1:j_lev,1:dim) )
       xx=0.0_pr; h = 0.0_pr;
       CALL set_xx (xx,h,nxyz,MAXVAL(nxyz(1:dim)),j_lev)
    END IF

    IF(nwlt_l /= nwlt_old_l ) then
       IF(ALLOCATED(x)) DEALLOCATE(x)
       ALLOCATE(x(1:nwlt_l,1:dim))
       x=0.0_pr  
    END IF

    DO proc = par_rank, par_rank !0, par_size-1
       DO j = 1, j_mx
          DO face_type = 0, 3**dim - 1
             DO wlt_type = MIN(j-1,1),2**dim-1
                CALL DB_get_initial_type_level_node (proc,wlt_type,j,face_type,ii,c_pointer,list_sig)
                IF(ii > 0) THEN
                   
                   DO WHILE (is_ok(c_pointer))
#ifdef TREE_NODE_POINTER_STYLE_C
                      CALL DB_get_ifunction_by_pointer( c_pointer, nvarI_nwlt, nvarI_nwlt, i ) ! indx for inverse mapping
                      CALL DB_get_coordinates_by_pointer ( c_pointer, ixyz )
                      DO idim =1,dim
                         x(i,idim) = xx_DB(ixyz(idim),idim)
                      END DO
                      CALL DB_get_next_type_level_node (proc,wlt_type,j,face_type,c_pointer,c_pointer1,list_sig)
                      c_pointer = c_pointer1
#elif defined TREE_NODE_POINTER_STYLE_F
                      i = c_pointer%ival( nvarI_nwlt )                                     ! indx for inverse mapping
                      CALL DB_get_coordinates_by_pointer ( c_pointer, ixyz )
                      DO idim =1,dim
                         x(i,idim) = xx_DB(ixyz(idim),idim)
                      END DO
                      CALL DB_get_next_type_level_node (c_pointer)
#endif
                   END DO
                   
                END IF
             END DO
          END DO
       END DO
    END DO
    
    
    
    IF (do_COMPUTE_new_grid) THEN
       !
       ! test if the grid is new
       !
       new_grid = .TRUE.                              ! default case of nwlt_old != nwlt
       
       IF (nwlt_old_l == nwlt_l .AND. ireg /= 0) THEN     ! otherwise test pos_old bit for each node
!!$       IF (ireg /= 0) THEN
          new_grid=.FALSE.
          LOOP_J: DO proc = par_rank, par_rank !0, par_size-1
             DO j = 1, j_mx
                DO wlt_type = MIN(j-1,1),2**dim - 1
                   DO face_type = 0, 3**dim - 1
                      CALL DB_get_initial_type_level_node (proc,wlt_type,j,face_type,ii,c_pointer,list_sig)
                      IF(ii > 0) THEN
                         
                         DO WHILE (is_ok(c_pointer))
#ifdef TREE_NODE_POINTER_STYLE_C
                            CALL DB_get_id_by_pointer (c_pointer,id)
                            IF(.NOT.BTEST(id,pos_old)) THEN
                               new_grid = .TRUE.
                               EXIT LOOP_J
                            END IF
                            CALL DB_get_next_type_level_node (proc,wlt_type,j,face_type,c_pointer,c_pointer1,list_sig)
                            c_pointer = c_pointer1
#elif defined TREE_NODE_POINTER_STYLE_F
                            CALL DB_get_id_by_pointer (c_pointer,id)
                            IF(.NOT.BTEST(id,pos_old)) THEN
                               new_grid = .TRUE.
                               EXIT LOOP_J
                            END IF
                            CALL DB_get_next_type_level_node (c_pointer)
#endif
                         END DO
                         
                      END IF
                   END DO
                END DO
             END DO
          END DO LOOP_J
       END IF
       !
       ! synchronize status of the grid
       CALL parallel_global_sum( LOGICALOR=new_grid )
    END IF
    
    
    ! set global number of wavelets to be used elsewhere
    nwlt_global = nwlt_l
    CALL parallel_global_sum( INTEGER=nwlt_global )
    
    
    IF(j_lev > j_mx) THEN
       WRITE(*,'("ERROR: j_lev>j_mx")')
       STOP
    END IF
    
    j_full=j_mn_local                                                        ! set j_full (for derivative calculations)
    IF (ireg.NE.-1) THEN
       DO j = j_mn_local, j_lev
          count = Nwlt_lev(j,1)
#ifdef INTEGER8_DEBUG_AR                                                                                              
           WRITE (*,'(A, I4, A, I2, A, I12, A)') &                                                                    
                    'indices_tree  par_rank=', par_rank, ' j=', j, 'Nwlt_lev=', count, '    CHECK !!!!!!!!!!!!!!!!!!' 
#endif                                                                                                                
          CALL parallel_global_sum( INTEGER=count )
          IF ( count == PRODUCT(mxyz(1:dim)*2**(j-1)+1-prd(1:dim)) )  THEN
             j_full = j
          ELSE
             EXIT
          END IF
       END DO
    END IF
    !j_full = 1
    
    
    IF( BTEST(debug_level,2) ) WRITE (*,'("proc",I3,": ireg=",I2,", j_lev=",I3,", nwlt/old=",I8,"/",I8)') &
         par_rank,ireg, j_lev, nwlt_l, nwlt_old_l
    !PRINT *, 'proc=',par_rank,'Nwlt_lev=',Nwlt_lev
    !CALL check_active_nodes
    !PAUSE

#ifdef INTEGER8_DEBUG_AR
    WRITE (*,'(A, I4, A, 4I12, A, 4I12, A, 4I12, A, 4I12, A)')    'indices_tree  par_rank=', par_rank, &
             ' i_p=', i_p, ' i_p_prd=', i_p_prd, ' i_p_face=', i_p_face, ' i_p_wlt=', i_p_wlt, '    CHECK !!!!!!!!!!!!!!!!!!'
#endif
 
  END SUBROUTINE indices_tree

  SUBROUTINE check_active_nodes
    USE precision
    USE util_vars
    USE share_consts
    USE db_tree
    USE parallel
    IMPLICIT NONE
    INTEGER   :: i, j, id, ixyz(dim),                 nproc      
    INTEGER*8 :: i_p_prd(0:dim)                                                                                   !AR 2/23/2011! added by AR
    !!                                              !!!!!!!!!!!!!!!!!!!!!! N O T E !!!!!!!!!!!!!!!!!!!!!!!!!      !AR 2/23/2011! added by AR
    !!                                              !!! AR IS NOT SURE WHETHER                           !!!      !AR 2/23/2011! added by AR
    !!                                              !!!   i_p_prd   must be INTEGER*8                    !!!      !AR 2/23/2011! added by AR
    !!                                              !!!   But it seems, i_p_prd can be very large        !!!      !AR 2/23/2011! added by AR
    !!                                              !!!   To be conservative, it is changed              !!!      !AR 2/23/2011! added by AR
    !!                                              !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!      !AR 2/23/2011! added by AR


#ifdef TREE_NODE_POINTER_STYLE_C
    INTEGER(pointer_pr) :: c_pointer                  ! pointer to node of tree structure (integer in C/C++)
#elif defined TREE_NODE_POINTER_STYLE_F
    TYPE(node), POINTER :: c_pointer                       ! (Fortran's native pointer)
#endif

    
    PRINT *, 'Checking active nodes'
    i_p_prd(0)=1
    i_p_prd(1:dim) = nxyz(1:dim)+1-prd(1:dim)
    DO i =1, dim
       i_p_prd(i)  = i_p_prd(i-1) *i_p_prd(i)
    END DO
    DO i=1,i_p_prd(dim)
       ixyz(1:dim) = INT(MOD(i-1,i_p_prd(1:dim))/i_p_prd(0:dim-1))
       
       CALL DB_get_proc_by_coordinates (ixyz(:), j_lev, nproc)
       IF (par_rank.EQ.nproc) THEN
          CALL DB_test_node(ixyz(:),j_lev,c_pointer)
          
          IF (is_ok(c_pointer)) THEN
             CALL DB_get_id_by_pointer ( c_pointer, id )
             !IF(.NOT.BTEST(id,3).AND.id/=0) PRINT *, id, ixyz(1:dim)
             !IF(BTEST(id,0) .OR. BTEST(id,1) ) PRINT *, ixyz(1:dim)
             PRINT *, 'id=',id,'ixyz=',ixyz
          END IF
       END IF
    END DO

#ifdef INTEGER8_DEBUG_AR
    WRITE (*,'(A, I4, A, 4I12, A)')    'check_active_nodes  par_rank=', par_rank, ' i_p_prd=', i_p_prd, '    CHECK !!!!!!!!!!!!!!!!!!'
#endif

  END SUBROUTINE check_active_nodes

  SUBROUTINE readwrite_i_in (readwrite) 
    USE precision
    USE wlt_vars
    USE db_tree
    USE sizes
    USE field ! u
    USE pde ! n_var
    USE threedobject 
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: readwrite ! readwrite: -1 - reads from DB_tree inot i_in, 0 initilizes i_in to .FALSE.,  1 - writes from i_in into DB_tree, 
    INTEGER :: i, inum, idenom, ie, id, id1 
    INTEGER :: j, wlt_type, face_type, j_df, k
    LOGICAL,  DIMENSION(:,:,:,:), ALLOCATABLE :: i_in_tmp ! 1 - in/out, 2 - old/new
#ifdef TREE_NODE_POINTER_STYLE_C
    INTEGER(pointer_pr) :: c_pointer      ! pointer to node of tree structure (integer in C/C++)
#elif defined TREE_NODE_POINTER_STYLE_F
    TYPE(node), POINTER :: c_pointer      ! (Fortran's native pointer)
#endif
     

    IF(readwrite == -1) THEN ! read from DB_tree to i_in
       IF(nwlt /= nwlt_old) THEN 
          IF(ALLOCATED(i_in)) DEALLOCATE(i_in)
          ALLOCATE(i_in(2,nwlt))
       END IF
       i_in = 0

       DO j = 1, j_lev
          DO wlt_type = MIN(j-1,1),2**dim-1
             DO face_type = 0, 3**dim - 1
                DO j_df = j, j_lev
                   DO k = 1, indx_DB(j_df,wlt_type,face_type,j)%length  
                      c_pointer = indx_DB(j_df,wlt_type,face_type,j)%p(k)%nptr
                      i = indx_DB(j_df,wlt_type,face_type,j)%p(k)%i
                      CALL DB_get_id_by_pointer( c_pointer, id)
                      i_in(1,i) = BTEST(id,pos_in) 
                      i_in(2,i) = BTEST(id,pos_in_old) 
                      IF( (BTEST(id,pos_old) .AND. .NOT.BTEST(id,pos_in_old)) .OR. (.NOT.BTEST(id,pos_old) .AND. BTEST(id,pos_in_old)) ) THEN !checks if pos_old dos not correspond to pos_in_old
                         WRITE(*,'("WARNING (read): pos_old /= pos_in_old")')
                         PAUSE
                      END IF
                   END DO
                END DO
             END DO
          END DO
       END DO
    ELSE IF(readwrite == 0) THEN ! initializes
       IF(ALLOCATED(i_in)) DEALLOCATE(i_in)
       ALLOCATE(i_in(2,nwlt))
       i_in = .FALSE.
    ELSE IF(readwrite == 1) THEN ! write from i_in to db_tree
       DO j = 1, j_lev
          DO wlt_type = MIN(j-1,1),2**dim-1
             DO face_type = 0, 3**dim - 1
                DO j_df = j, j_lev
                   DO k = 1, indx_DB(j_df,wlt_type,face_type,j)%length  
                      c_pointer = indx_DB(j_df,wlt_type,face_type,j)%p(k)%nptr
                      i = indx_DB(j_df,wlt_type,face_type,j)%p(k)%i
                      CALL DB_get_id_by_pointer( c_pointer, id)
#ifdef GFORTRAN_COMPILE
                      id1=(1-logical2int(i_in(1,i)))*IBCLR(id,pos_in) + logical2int(i_in(1,i))*IBSET(id,pos_in)
                      id =(1-logical2int(i_in(2,i)))*IBCLR(id1,pos_in_old) + logical2int(i_in(2,i))*IBSET(id1,pos_in_old)
#else
                      id1=(1-i_in(1,i))*IBCLR(id,pos_in) + i_in(1,i)*IBSET(id,pos_in)
                      id =(1-i_in(2,i))*IBCLR(id1,pos_in_old) + i_in(2,i)*IBSET(id1,pos_in_old)
#endif
                      CALL DB_set_id_by_pointer (c_pointer,id)
                      IF( (BTEST(id,pos_old) .AND. .NOT.BTEST(id,pos_in_old)) .OR. (.NOT.BTEST(id,pos_old) .AND. BTEST(id,pos_in_old)) ) THEN !checks if pos_old dos not correspond to pos_in_old
                         WRITE(*,'("WARNING (write): pos_old /= pos_in_old")')
                         PAUSE
                      END IF
                   END DO
                END DO
             END DO
          END DO
       END DO
    END IF
 
  END SUBROUTINE readwrite_i_in

  !
  ! In this routine we update the values in u from the db
  SUBROUTINE update_db_from_u(  u , nlocal , ie, n_var_min, n_var_max, db_offset )
    USE precision
    USE wlt_vars
    USE db_tree
    IMPLICIT NONE
    INTEGER,   INTENT(IN) :: nlocal, ie
    INTEGER,   INTENT(IN) :: n_var_min, n_var_max, db_offset
    REAL (pr), INTENT(IN) :: u(1:nlocal,1:n_var_max-n_var_min+1)

    CALL write_DB (u, nlocal, n_var_min, n_var_max, j_lev)

  END SUBROUTINE update_db_from_u

  SUBROUTINE update_u_from_db(  u , nlocal , ie, n_var_min, n_var_max, db_offset )
    USE precision
    USE wlt_vars
    USE db_tree
    IMPLICIT NONE
    INTEGER,   INTENT(IN) :: nlocal, ie
    INTEGER,   INTENT(IN) :: n_var_min, n_var_max, db_offset
    REAL (pr), INTENT(INOUT) :: u(1:nlocal,1:n_var_max-n_var_min+1)

    CALL read_DB (u, nlocal, n_var_min, n_var_max, j_lev)

  END SUBROUTINE update_u_from_db

  !
  ! update_u_from_db_noidices 
  ! In this routine we update the values in u from the db
  ! No indices are changed. Only use this routine if the 
  ! grid has not changed since the last call to update_u_from_db()
  !(This is a dummy routine for wrk version 
  SUBROUTINE update_u_from_db_noidices(  u , nwlt , ie, n_var_min, n_var_max, db_offset )
    USE precision
    IMPLICIT NONE
    INTEGER,   INTENT(IN) :: nwlt, ie
    REAL (pr), INTENT(INOUT) :: u(1:nwlt,1:ie)
    INTEGER,   INTENT(IN) :: n_var_min, n_var_max, db_offset

  END SUBROUTINE update_u_from_db_noidices

  !**************************************************************************
  !
  ! Find spacial derivatives of components already in db
  !
  !
  ! Arguments
  !u, du, d2u, j_in, nlocal, meth, meth1, 
  ! ID Sets which derivative is done 
  !    10 - do first derivative
  !    11 - do first and secont derivative
  !    01 - do second derivative
  !    if ID > 0 then the second derivative is central difference D_central
  !    if ID < 0 then the second derivative is D_backward_bias followed by D_forward_bias
  ! meth  - order of the method, high low,
  !
  !  dimensions of u are u(1:nlocal,1:ie) 
  !  derivative is done on  u(1:nlocal,mn_var:mx_var)
  !(This is a dummy routine for wrk version
  SUBROUTINE c_diff_fast_db(du, d2u, j_in, nlocal, meth, id, ie, mn_var, mx_var, mn_varD, mx_varD , mn_varD2, mx_varD2)
    USE precision
    USE sizes
    USE wlt_vars   ! dim
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: j_in, nlocal, meth, id
    INTEGER, INTENT(IN) :: ie ! number of dimensions of u(:,1:ie)  we are doing derivative for (forced to 1 for now)
    INTEGER, INTENT(IN) ::  mn_var, mx_var ! min and max of second index of u that we are doing the derivatives for.
    INTEGER, INTENT(IN) ::  mn_varD, mx_varD , mn_varD2, mx_varD2 ! min/max extents in db to do 1st/2nd derivative
    REAL (pr), DIMENSION (mn_varD :mx_varD , 1:nlocal,dim), INTENT (INOUT) :: du
    REAL (pr), DIMENSION (mn_varD2:mx_varD2, 1:nlocal,dim), INTENT (INOUT) :: d2u

    PRINT *, 'c_diff_fast_db() is not ready for db_tree'
    STOP

  END SUBROUTINE c_diff_fast_db

  
  SUBROUTINE weights 
    !--Calculates weights for volume integration based on the Trapezoid
    !--integration rule (second-order accurate).
    USE precision
    USE sizes
    USE wlt_vars
    USE util_vars                 ! dA
    USE pde
    USE db_tree 
    USE wlt_trns_vars             ! indx_DB
    USE parallel
    USE curvilinear 
    USE debug_vars
!!$    USE field ! --------- DEBUG
    IMPLICIT NONE

    INTEGER :: i, j, k, m, idim
    INTEGER, DIMENSION(dim) :: ixyz
    INTEGER, DIMENSION(3,2) :: i_k
    REAL (pr), DIMENSION(dim) :: lxyz
    REAL (pr) :: shft(dim,2), dAh, tmp_wgh
    INTEGER :: i1(1), ii, jj, j_df, wlt_type, face_type, dir_type, step, step2, iloc, nproc, &
         ierr, &   
         iveclocal(dim), dir(dim), ixyz1(dim), wlt(dim), i_l(dim), i_h(dim), &
         ZERO(dim,1), num, proc, size_node, num_recv, nwlt_tmp


    INTEGER*8              :: i_p(0:dim)    
    INTEGER                :: i_p_cube(0:dim)                                                                     !AR 2/21/2011! added by AR
    !!                                              !!!!!!!!!!!!!!!!!!!!!! N O T E !!!!!!!!!!!!!!!!!!!!!!!!!      !AR 2/21/2011! added by AR
    !!                                              !!! AR IS NOT SURE WHETHER                           !!!      !AR 2/21/2011! added by AR
    !!                                              !!!   i_p_cube(0:dim)   must be INTEGER*8            !!!      !AR 2/21/2011! added by AR
    !!                                              !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!      !AR 2/21/2011! added by AR

#ifdef TREE_NODE_POINTER_STYLE_C
    INTEGER(pointer_pr) :: c_pointer, c_pointer1      ! pointer to node of tree structure (integer in C/C++)
#elif defined TREE_NODE_POINTER_STYLE_F
    TYPE(node), POINTER :: c_pointer, c_pointer1      ! (Fortran's native pointer)
#endif
    INTEGER, PARAMETER :: nvarI_wgh = 1               ! position to store dA inside the database
    REAL(pr), ALLOCATABLE :: valuebuffer(:)           ! values to transfer
    REAL(pr), POINTER :: valuebuffer_recv(:)

    INTEGER*8, ALLOCATABLE :: nodebuffer(:)             ! store 1D node index  
    INTEGER*8, POINTER     :: nodebuffer_recv(:)                               

#ifdef INTEGER8_DEBUG_AR
     INTEGER               :: max_ipcube                                       
#endif

#ifdef WEIGHTSDEBUG
    !INTEGER, PARAMETER :: DU = 553
    INTEGER            :: FILE_1, FILE_2, io_status, id             
#endif

    REAL (pr), DIMENSION(1:nwlt) :: jacobian_determinant
    LOGICAL :: curvilinear_transform = .FALSE.

    NULLIFY(valuebuffer_recv, nodebuffer_recv)

#ifdef MULTIPROC
    ! nullify real entries on other processor nodes
    ! (function at nvarI_wgh will store hanging out weights)
    CALL clean_DB(list_sig,.TRUE.,.FALSE.)
#endif

#ifdef WEIGHTSDEBUG
    OPEN (UNIT=DU, FILE='debug.weights'//par_rank_str)
    WRITE (DU,*) 'proc=',par_rank, 'lists in the beginning:'
    CALL count_DB (MAXLIST=list_gho, FILE=DU, VERB=.TRUE.)
    !PAUSE 'in the beginnning of weights'
#endif
    
#ifdef INTEGER8_DEBUG_AR
     max_ipcube = 0                                                                                         
#endif


#ifdef WEIGHTSDEBUG
     FILE_1 = 101010
     FILE_2 = 101011
     OPEN (UNIT=FILE_1, FILE='Zero_i1.log',       FORM='formatted', STATUS='UNKNOWN', POSITION='append', IOSTAT=io_status)
     OPEN (UNIT=FILE_2, FILE='NodeNotExist.log',  FORM='formatted', STATUS='UNKNOWN', POSITION='append', IOSTAT=io_status)
#endif                                                                                                                



!!$#ifdef WEIGHTSDEBUG
!!$    WRITE (DU,*) 'link-list:'
!!$    DO proc = 0, par_size-1
!!$       DO j = 1, j_mx
!!$          DO wlt_type = MIN(j-1,1),2**dim - 1
!!$             DO face_type = 0, 3**dim - 1
!!$                CALL DB_get_initial_type_level_node (proc,wlt_type,j,face_type,ii,c_pointer,list_sig)
!!$                IF(ii > 0) THEN
!!$                   DO WHILE (is_ok(c_pointer))
!!$                      CALL DB_get_function_by_pointer( c_pointer, nvarI_wgh, nvarI_wgh, tmp_wgh )
!!$                      CALL DB_get_coordinates_by_pointer ( c_pointer, ixyz )
!!$                      CALL DB_get_id_by_pointer ( c_pointer, ii )
!!$                      WRITE (DU,'(A,'//CHAR(ICHAR('0')+dim)//'I3,X,A,E12.5,A,I3)') 'ixyz=', ixyz,', f=', tmp_wgh, ', id=', ii
!!$#ifdef TREE_NODE_POINTER_STYLE_C
!!$                      CALL DB_get_next_type_level_node (proc,wlt_type,j,face_type,c_pointer,c_pointer1,list_sig)
!!$                      c_pointer = c_pointer1  
!!$#elif defined TREE_NODE_POINTER_STYLE_F
!!$                      CALL DB_get_next_type_level_node (c_pointer)
!!$#endif                    
!!$                   END DO
!!$                END IF
!!$             END DO
!!$          END DO
!!$       END DO
!!$    END DO
!!$    WRITE (DU,*) 'nonadaptive array:'
!!$    i_p(0) = 1
!!$    DO i =1, dim
!!$       i_p(i) = i_p(i-1)*(mxyz(i)*2**(j_mx-1)+1)
!!$    END DO
!!$    DO i = 1,i_p(dim)
!!$       ixyz = INT(MOD(i-1,i_p(1:dim))/i_p(0:dim-1))
!!$       CALL DB_get_proc_by_coordinates (ixyz, j_mx, nproc)
!!$       CALL DB_test_node (ixyz, j_mx, c_pointer)
!!$       IF (is_ok(c_pointer)) THEN
!!$          CALL DB_get_function_by_pointer( c_pointer, nvarI_wgh, nvarI_wgh, tmp_wgh )
!!$          CALL DB_get_id_by_pointer ( c_pointer, ii )
!!$          WRITE (DU,'(A,'//CHAR(ICHAR('0')+dim)//'I3,X,A,E12.5,A,I3,A,I3)') 'ixyz=', ixyz, ', f=', tmp_wgh, ', id=', ii, ', belongs to',nproc
!!$       END IF
!!$    END DO
!!$    WRITE (DU,'("=======================================================")')
!!$#endif

!!$    CALL debug_print_u( u, nwlt, 1)
    !IF ( par_rank .EQ. 0 ) PRINT *, 'Calculating dA'
    IF ( ANY( transform_dir(1:dim) .EQ. 1 ) )  curvilinear_transform = .TRUE. 
    IF ( curvilinear_transform ) jacobian_determinant(:) = calc_jacobian_determinant( curvilinear_jacobian(1:dim,1:dim,1:nwlt), dim, nwlt)
    
    !=============================================================================================
    IF( Weights_Meth ==   0  ) THEN ! # Weights_Meth, 0: dA=area/nwlt, 1: normal
       !==========================================================================================
       IF (verb_level.GT.0.AND.par_rank.EQ.0) THEN
          WRITE(*,'(A)')  " "
          WRITE(*,'(A)')  "************************************************************"
          WRITE(*,'(A,I3)')  'Setting dA = area/nwlt for Weights Method = ',Weights_Meth
          WRITE(*,'(A)')  "************************************************************"
          WRITE(*,'(A)')  " "
       END IF
              
!!$       nwlt_global = nwlt
!!$#ifdef MULTIPROC
!!$       CALL parallel_global_sum (INTEGER=nwlt_global)
!!$#endif
       dA = PRODUCT(xyzlimits(2,1:dim)- xyzlimits(1,1:dim))/nwlt_global
       dA_level = 0.0_pr
       DO j = 1, j_lev
          nwlt_tmp = Nwlt_lev(j,1)
          CALL parallel_global_sum (INTEGER=nwlt_tmp)
          dA_level(1:Nwlt_lev(j,1),j) = PRODUCT(xyzlimits(2,1:dim)- xyzlimits(1,1:dim))/nwlt_tmp
       END DO
       IF ( curvilinear_transform ) THEN
          DO j = 1,j_lev
             DO jj = 1, j
                DO wlt_type = MIN(jj-1,1),2**dim-1
                   DO face_type = 0, 3**dim - 1
                      DO j_df = jj, j_lev
                         DO ii = 1, indx_DB(j_df,wlt_type,face_type,jj)%length  
                            i = indx_DB(j_df,wlt_type,face_type,jj)%p(ii)%i
                            iloc = indx_DB(j_df,wlt_type,face_type,jj)%p(ii)%i + indx_DB(j,wlt_type,face_type,jj)%shift
                            dA_level(iloc,j)= ABS(dA_level(iloc,j)/jacobian_determinant(i))
                         END DO
                      END DO
                   END DO
                END DO
             END DO          
          END DO
       END IF

       !==========================================================================================
    ELSE
       !==========================================================================================
       ZERO = 0
       dA = 0.0_pr
       i_k = 0
       lxyz(1:dim) = xyzlimits(2,1:dim)-xyzlimits(1,1:dim)
       i_p(0) = 1
       i_p_cube(0) = 1
       DO i=1,dim
          i_p(i) = i_p(i-1)*(1+nxyz(i))
       END DO
       DO i = 1,dim
          iveclocal(i) = dim-i+1
       END DO
#ifdef WEIGHTSDEBUG
       WRITE (DU,*) 'i_p=',i_p
#endif
       ! first go through coarse levels and assign areas to coarse levels only
       ! (first level weights, dA and dA_level, do not require synchronization
       !  in multiprocessor case)
       j = 1
       step = 2**(j_lev-j)
       DO wlt_type = MIN(j-1,1),2**dim-1
          DO face_type = 0, 3**dim - 1
             DO j_df = j, j_lev
                DO k = 1, indx_DB(j_df,wlt_type,face_type,j)%length  

#ifdef TREE_NODE_POINTER_STYLE_C
                   c_pointer = indx_DB(j_df,wlt_type,face_type,j)%p(k)%nptr
#elif defined TREE_NODE_POINTER_STYLE_F
                   c_pointer => indx_DB(j_df,wlt_type,face_type,j)%p(k)%nptr
#endif

                   CALL DB_get_coordinates_by_pointer ( c_pointer, ixyz )
                   ixyz = ixyz/2**(j_mx-j_lev)
                   i = indx_DB(j_df,wlt_type,face_type,j)%p(k)%i
                   DO dir_type = 0,2**dim-1
                      dir = 2*IBITS(dir_type,iveclocal(:)-1,1)-1
                      shft = 0.0_pr
                      ixyz1=ixyz+step*dir
                      WHERE(prd(1:dim) == 1 .AND. ixyz1(1:dim) < 0) &
                           shft(1:dim,1)=lxyz(1:dim)                ! left shift for periodic case
                      WHERE(prd(1:dim) == 1 .AND. ixyz1(1:dim) > nxyz(1:dim)-prd(1:dim)) &
                           shft(1:dim,2)=prd(1:dim)*lxyz(1:dim)  ! right shift for periodic case
                      ixyz1(1:dim) = (1-prd(1:dim))*ixyz1(1:dim)+prd(1:dim)*MOD(ixyz1(1:dim)+9*nxyz(1:dim),nxyz(1:dim))
                      !checks if subcube is in the domain
                      IF(.NOT.(ANY(ixyz1(1:dim) > nxyz(1:dim) - prd(1:dim)) .OR. ANY(ixyz1(1:dim) < 0)) )  THEN 
                         dAh=1.0_pr
                         DO idim=1,dim
                            dAh=dAh*( REAL(dir(idim),pr)*(xx(ixyz1(idim),idim)-xx(ixyz(idim),idim))+shft(idim,1)+shft(idim,2) )
                         END DO
                         dA(i) = dA(i) + dAh/2**dim
                      END IF
                   END DO
#ifdef WEIGHTSDEBUG
                   WRITE (DU,'(A,I1,A,'//CHAR(ICHAR('0')+dim)//'I3,A,I3,A,E12.5)') 'j=',j,', ixyz=',ixyz*2**(j_mx-j_lev),', i=',i,', dA(i)=',dA(i)
#endif
                END DO
             END DO
          END DO
       END DO

       DO wlt_type = MIN(j-1,1),2**dim-1
          DO face_type = 0, 3**dim - 1
             DO j_df = j, j_lev
                DO ii = 1, indx_DB(j_df,wlt_type,face_type,j)%length  
                   i = indx_DB(j_df,wlt_type,face_type,j)%p(ii)%i
                   iloc = indx_DB(j_df,wlt_type,face_type,j)%p(ii)%i + indx_DB(j,wlt_type,face_type,j)%shift
                   IF ( curvilinear_transform )  THEN
                      dA_level(iloc,j)=ABS(dA(i)/jacobian_determinant(i))
                   ELSE
                      dA_level(iloc,j)=dA(i)
                   END IF

#ifdef WEIGHTSDEBUG
                   ixyz(1:dim) = INT(MOD(indx_DB(j_df,wlt_type,face_type,j)%p(ii)%ixyz-1,i_p(1:dim))/i_p(0:dim-1))
                   WRITE (*,'(A,I3,A,I3,E12.5,2I3)') 'iloc=',iloc,', i,dA(i)=',i,dA(i),ixyz
#endif
                END DO
             END DO
          END DO
       END DO
       
       
       !--------------------------------------------------------------------------------
       !Use machinerey of reconstruction check procedure
       DO j = 2,j_lev
          step = 2**(j_lev-j)
          step2 = 2*step
          DO wlt_type = MIN(j-1,1),2**dim-1
             wlt(1:dim) = IBITS(wlt_type,iveclocal(1:dim)-1,1)
             DO face_type = 0, 3**dim - 1
                DO j_df = j, j_lev
                   DO k = 1, indx_DB(j_df,wlt_type,face_type,j)%length  

#ifdef TREE_NODE_POINTER_STYLE_C
                      c_pointer = indx_DB(j_df,wlt_type,face_type,j)%p(k)%nptr
#elif defined TREE_NODE_POINTER_STYLE_F
                      c_pointer => indx_DB(j_df,wlt_type,face_type,j)%p(k)%nptr
#endif

                      CALL DB_get_coordinates_by_pointer ( c_pointer, ixyz )
                      ixyz = ixyz/2**(j_mx-j_lev)
                      i = indx_DB(j_df,wlt_type,face_type,j)%p(k)%i
                      IF(dA(i) /= 0.0_pr) PRINT *, i,'before dA=',dA(i)
                      DO dir_type = 0,2**dim-1
                         dir = 2*IBITS(dir_type,iveclocal(:)-1,1)-1
                         shft = 0.0_pr
                         ixyz1=ixyz+step*dir
                         WHERE(prd(1:dim) == 1 .AND. ixyz1 < 0) &
                              shft(1:dim,1)=lxyz(:)                ! left shift for periodic case
                         WHERE(prd(1:dim) == 1 .AND. ixyz1 > nxyz(1:dim)-prd(1:dim)) &
                              shft(1:dim,2)=prd(1:dim)*lxyz(1:dim)  ! right shift for periodic case
                         ixyz1 = (1-prd(1:dim))*ixyz1+prd(1:dim)*MOD(ixyz1+9*nxyz(1:dim),nxyz(1:dim))
                         !checks if subcube is in the domain
                         IF(.NOT.(ANY(ixyz1 > nxyz(1:dim) - prd(1:dim)) .OR. ANY(ixyz1 < 0)) )  THEN 
                            dAh=1.0_pr
                            DO idim=1,dim
                               dAh=dAh*( REAL(dir(idim),pr)*(xx(ixyz1(idim),idim)-xx(ixyz(idim),idim))+shft(idim,1)+shft(idim,2) )
                            END DO
                            dA(i) = dA(i) + dAh/2**dim
                         END IF
                      END DO
#ifdef WEIGHTSDEBUG
                      WRITE (DU,'(A,I2,A,'//CHAR(ICHAR('0')+dim)//'I4,A,I3,A,E12.5)') &
                           'j=',j,', ixyz=',ixyz*2**(j_mx-j_lev),', i=',i,', initial dA(i)=',dA(i)
#endif

                      IF(dA(i) <= 0.0_pr) PRINT *, i,'after dA=',dA(i)
                      i_l(:) = MAX(ixyz(:)-step,ZERO(:,1))
                      i_h(:) = MIN(i_l(:)+step2,nxyz(1:dim)) 
                      i_l(:) = MAX(i_h(:)-step2,ZERO(:,1))
                      i_l(1:dim) = (1-prd(1:dim))*i_l(1:dim) + prd(1:dim)*( ixyz - step )
                      i_h(1:dim) = (1-prd(1:dim))*i_h(1:dim) + prd(1:dim)*( ixyz + step )
                      i_l(:) = ixyz(:) + ( i_l(:) - ixyz(:) ) * wlt(:)
                      i_h(:) = ixyz(:) + ( i_h(:) - ixyz(:) ) * wlt(:)
                      !getting i based on location
                      i_p_cube(1:dim) = (i_h-i_l)/step2 + 1
#ifdef INTEGER8_DEBUG_AR
!                     WRITE (*,'(A, I4, A, 4I12)')  ' weights >> par_rank >> ',par_rank,'>> i_p_cube = ',i_p_cube
                      max_ipcube = MAX(max_ipcube, MAXVAL(i_p_cube(:)))        
#endif
                      DO idim =1, dim
                         i_p_cube(idim) = i_p_cube(idim-1)*i_p_cube(idim)
                      END DO
                      DO m = 1,i_p_cube(dim)
                         ixyz1 = i_l + INT(MOD(m-1,i_p_cube(1:dim))/i_p_cube(0:dim-1))*step2
                         ixyz1(1:dim) = (1-prd(1:dim))*ixyz1(1:dim) + prd(1:dim)*MOD(ixyz1(:)+9*nxyz(1:dim),nxyz(1:dim))
                         
                         CALL DB_get_proc_by_coordinates (ixyz1(:), j_lev, nproc)
#ifdef WEIGHTSDEBUG
                         WRITE (DU,'(A,'//CHAR(ICHAR('0')+dim)//'I3,2(A,I3))') '  ixyz1=',ixyz1,', j_lev=',j_lev !,', nproc=',nproc
#endif
                         IF (nproc.EQ.par_rank) THEN
                            CALL DB_test_node (ixyz1(:), j_lev, c_pointer)
                            CALL DB_get_ifunction_by_pointer( c_pointer, nvarI_nwlt, nvarI_nwlt, i1 )
#ifdef WEIGHTSDEBUG
                            WRITE (DU,*) '   previous tmp_wgh=',dA(i1(1))
#endif
                            IF (is_ok(c_pointer)) THEN ! normal behaviour of a local node
                               dA(i1(1)) = dA(i1(1)) - dA(i)/i_p_cube(dim)
!!$                               WRITE (DU,*) '   i1,dA(i1)=',i1,dA(i1)
#ifdef WEIGHTSDEBUG
                               IF ( i1(1) .LE. 0 ) THEN
                                  CALL DB_get_id_by_pointer ( c_pointer, id )
                               !!!WRITE (*,'(A, I6, A, I6, A, 3I6, A, I3, A, I6, A, I3, A, I3, A, I12)') & 
                               !!!         'weights  par_rank=', par_rank, ' i1(1)=', i1(1), ' ixyz1(1:dim)=', ixyz1(1:dim), ' j_lev=', j_lev, ' nproc=', nproc, ' nvarI_nwlt=', nvarI_nwlt, ' id=', id, ' c_pointer=', c_pointer
                                  WRITE (UNIT=FILE_1, ADVANCE='YES', FMT='(A, I6, A, I6, A, 3I6, A, I3, A, I6, A, I3, A, I3, A, I12)') &
                                         'weights  par_rank=', par_rank, ' i1(1)=', i1(1), ' ixyz1(1:dim)=', ixyz1(1:dim), ' j_lev=', j_lev, ' nproc=', nproc, ' nvarI_nwlt=', nvarI_nwlt, ' id=', id, ' c_pointer=', c_pointer                               
                               END IF
#endif
                                  
                            ELSE                       ! it would give an error in old code
                               WRITE (*,'(A,'//CHAR(ICHAR('0')+dim)//'I3,A)') 'ixyz1=',ixyz1*2**(j_mx-j_lev),' -- node not present'
                               CALL parallel_finalize; STOP 'in weights'
                            END IF
#ifdef WEIGHTSDEBUG
                            WRITE (DU,'(A,I3,A,E12.5)') '    wrote locally to i1(1)=',i1(1),', dA=',dA(i1(1))
#endif

                         ELSE                          ! node from another processor written to sig list
                            CALL DB_add_node (ixyz1(:), j_lev, ierr, c_pointer, idp(pos_adj), end_list)
                            CALL DB_get_function_by_pointer( c_pointer, nvarI_wgh, nvarI_wgh, tmp_wgh )
#ifdef WEIGHTSDEBUG
                            WRITE (DU,*) '   previous tmp_wgh=',tmp_wgh
#endif
                            tmp_wgh = tmp_wgh - dA(i)/i_p_cube(dim)
                            CALL DB_set_function_by_pointer( c_pointer, nvarI_wgh, nvarI_wgh, tmp_wgh )
#ifdef WEIGHTSDEBUG
                            WRITE (DU,'(A,'//CHAR(ICHAR('0')+dim)//'I3,A,I2,A,E12.5)') '    wrote ',ixyz1*2**(j_mx-j_lev),' to #',nproc,' as', tmp_wgh
#endif
                         END IF
                      END DO
                   END DO
                END DO
             END DO
          END DO
          
          ! --------- dA has to be synchronized for multiprocessor case to ensure dA_level correctness after each level ---------
#ifdef MULTIPROC
          ! weights to be substracted from nodes of other processors stored in sig list at nvarI_wgh
          ! count these nodes to inform the other processors (up to the current level j)
          procbuffer(0:par_size-1) = 0
          DO proc = 0, par_size-1
             IF (proc.NE.par_rank) THEN
                DO jj = 1, j
                   DO wlt_type = MIN(jj-1,1),2**dim - 1
                      DO face_type = 0, 3**dim - 1
                         CALL DB_get_initial_type_level_node (proc,wlt_type,jj,face_type,ii,c_pointer,list_sig)
                         IF(ii > 0) THEN
                            DO WHILE (is_ok(c_pointer))
                               procbuffer(proc) = procbuffer(proc) + 1
#ifdef TREE_NODE_POINTER_STYLE_C
                               CALL DB_get_next_type_level_node (proc,wlt_type,jj,face_type,c_pointer,c_pointer1,list_sig)
                               c_pointer = c_pointer1  
#elif defined TREE_NODE_POINTER_STYLE_F
                               CALL DB_get_next_type_level_node (c_pointer)
#endif                    
                            END DO
                         END IF
                      END DO
                   END DO
                END DO
             END IF
          END DO
          ! allocate space and write 1D global index and weights for nodes to send
          size_node = SUM( procbuffer(0:par_size-1) )
          ALLOCATE( nodebuffer(size_node) )
          ALLOCATE( valuebuffer(size_node) )
          i_p(0) = 1
          DO jj=1,dim
             i_p(jj) = i_p(jj-1)*(mxyz(jj)*2**(j_mx-1) + 1)
          END DO
          num = 0
          DO proc = 0, par_size-1
             IF (proc.NE.par_rank) THEN
                DO jj = 1, j
                   DO wlt_type = MIN(jj-1,1),2**dim - 1
                      DO face_type = 0, 3**dim - 1
                         CALL DB_get_initial_type_level_node (proc,wlt_type,jj,face_type,ii,c_pointer,list_sig)
                         IF(ii > 0) THEN
                            DO WHILE (is_ok(c_pointer))
                               num = num + 1
                               CALL DB_get_coordinates_by_pointer ( c_pointer, ixyz )
                               nodebuffer(num) = 1+SUM(ixyz(1:dim)*i_p(0:dim-1))
!!$                               PRINT *, 'ixyz=',ixyz,'index=',nodebuffer(num)
!!$                               PRINT *, 'back=', INT(MOD(nodebuffer(num)-1,i_p(1:dim))/i_p(0:dim-1))
                               CALL DB_get_function_by_pointer ( c_pointer, nvarI_wgh, nvarI_wgh, valuebuffer(num) )
#ifdef TREE_NODE_POINTER_STYLE_C
                               CALL DB_get_next_type_level_node (proc,wlt_type,jj,face_type,c_pointer,c_pointer1,list_sig)
                               c_pointer = c_pointer1  
#elif defined TREE_NODE_POINTER_STYLE_F
                               CALL DB_get_next_type_level_node (c_pointer)
#endif                    
                            END DO
                         END IF
                      END DO
                   END DO
                END DO
             END IF
          END DO
!!$       PRINT *, 'proc',par_rank,'size=',num,'procbuffer=',procbuffer
!!$       DO jj=1,num
!!$          WRITE (*,'(A,2I4,A,E12.5)') &
!!$               'nodebuffer=',INT(MOD(nodebuffer(jj)-1,i_p(1:dim))/i_p(0:dim-1)),', valuebuffer=',valuebuffer(jj)
!!$       END DO
          ! transfer the data
          ! (nodebuffer_recv(:) and valuebuffer_recv(:) will be allocated inside)
          CALL parallel_comm_wgh (nodebuffer, valuebuffer, procbuffer, num, &
               nodebuffer_recv, valuebuffer_recv, num_recv)
          ! write the obtained values
!!$       PRINT *, 'proc',par_rank,'num_recv=',num_recv
!!$          ierr = 0
          DO i = 1, num_recv
             ixyz(1:dim) = INT(MOD(nodebuffer_recv(i)-1,i_p(1:dim))/i_p(0:dim-1))
             CALL DB_test_node ( ixyz, j_mx, c_pointer )
             CALL DB_get_ifunction_by_pointer( c_pointer, nvarI_nwlt, nvarI_nwlt, ii )
             IF (ii.NE.0) THEN
!!$             PRINT *, 'adding',ixyz
                dA(ii) = dA(ii) + valuebuffer_recv(i)
             ELSE
                CALL DB_get_proc_by_coordinates (ixyz, j_mx, ierr)
             !!!WRITE (*,'(A, I6, A, 3I6, A, I12, A, I3)') & 
             !!!         'weights proc=', par_rank, ' : node', ixyz, 'does not exist     c_pointer=', c_pointer, ' get_proc_by_coordinates ierr= ', ierr
#ifdef WEIGHTSDEBUG
                WRITE (UNIT=FILE_2, ADVANCE='YES', FMT='(A, I6, A, 3I6, A, I12, A, I3)') &
                       'weights proc=', par_rank, ' : node', ixyz, 'does not exist     c_pointer=', c_pointer, ' get_proc_by_coordinates ierr= ', ierr
#endif 
                
!!$                ierr = 1
!06.20.2011!AR!  CALL parallel_finalize; STOP 1
             END IF
          END DO
!!$          IF (ierr.NE.0) THEN
!!$             CALL parallel_finalize; STOP 1
!!$          END IF

          ! clean the buffers
          IF (num_recv.NE.0) DEALLOCATE( nodebuffer_recv, valuebuffer_recv)
          DEALLOCATE( nodebuffer, valuebuffer)
!!$       ! clean nodes marked as from other processors from link lists and db
!!$       CALL DB_clean_other_proc_nodes
          
          ! nullify real entries on other processor nodes
          CALL clean_DB(list_sig,.TRUE.,.FALSE.)
#endif
          ! ---------------------------------------------------------------------------------------------------------------------
       

          DO jj = 1, j
             DO wlt_type = MIN(jj-1,1),2**dim-1
                DO face_type = 0, 3**dim - 1
                   DO j_df = jj, j_lev
                      DO ii = 1, indx_DB(j_df,wlt_type,face_type,jj)%length  
                         i = indx_DB(j_df,wlt_type,face_type,jj)%p(ii)%i
                         iloc = indx_DB(j_df,wlt_type,face_type,jj)%p(ii)%i + indx_DB(j,wlt_type,face_type,jj)%shift
                         IF ( curvilinear_transform ) THEN
                            dA_level(iloc,j)=ABS(dA(i)/jacobian_determinant(i))
                         ELSE
                            dA_level(iloc,j)=dA(i)
                         END IF

#ifdef WEIGHTSDEBUG
                         ixyz(1:dim) = INT(MOD(indx_DB(j_df,wlt_type,face_type,jj)%p(ii)%ixyz-1,i_p(1:dim))/i_p(0:dim-1))
                         WRITE (*,'(A,3I3,E12.5,2I3)') 'iloc,j,i,dA(i)',iloc,j,i,dA(i),ixyz
#endif
                      END DO
                   END DO
                END DO
             END DO
          END DO
          
       END DO
       
       
       !==========================================================================================
    END IF !IF( Scale_Meth == 1 .OR. Scale_Meth == 2 ) THEN
    !=============================================================================================
    
    !-------------------------------------------------------------------------------
    !-------------------------------------------------------------------------------
    IF( ANY(dA < 0) ) THEN 
       PRINT *,'WARNING: dA is negative'
       !STOP
    END IF

    sumdA_computational = sum(dA)                         ! calculate here to use in varies places (local)
    sumdA_computational_global = sumdA_computational      !                                        (global)

    IF ( curvilinear_transform )  dA(:) = ABS( dA(:)/jacobian_determinant(:) ) ! Monotonic, negative jacobians valid

    sumdA = sum(dA)           ! calculate here to use in varies places (local)
    sumdA_global = sumdA      !                                        (global)

#ifdef MULTIPROC
    CALL parallel_global_sum (REAL=sumdA_computational_global)
    CALL parallel_global_sum (REAL=sumdA_global)
#endif
    
#ifdef WEIGHTSDEBUG
    WRITE (DU,*) 'sumdA(local)=',sumdA,'sumdA(global)=',sumdA_global,PRODUCT(xyzlimits(2,1:dim)- xyzlimits(1,1:dim))
    WRITE (DU,*) 'sumdA_comp(local)=',sumdA_computational,'sumdA_comp(global)=',sumdA_computational_global,PRODUCT(xyzlimits(2,1:dim)- xyzlimits(1,1:dim))
#endif
    DO j = 1, j_lev
       sumdA = sum(dA_level(1:Nwlt_lev(j,1),j)) ! calculate here to use in varies places
       sumdA_global = sumdA 
#ifdef MULTIPROC
       CALL parallel_global_sum (REAL=sumdA_global)
#endif
#ifdef WEIGHTSDEBUG
       WRITE (DU,*) 'j=',j,'sumdA(j)(local)=',sumdA,'sumdA(j)(global)=',sumdA_global,PRODUCT(xyzlimits(2,1:dim)- xyzlimits(1,1:dim))
#endif
    END DO
    
    
    
#ifdef WEIGHTSDEBUG
    WRITE (DU,'("=======================================================")')
    WRITE (DU,*) 'finish calculating weights at proc', par_rank
    CALL count_DB (MAXLIST=list_gho, FILE=DU, VERB=.TRUE.)
    WRITE (DU,'("=======================================================")')
    CLOSE (DU)
#undef WEIGHTSDEBUG
#endif
    !-------------------------------------------------------------------------------
    !-------------------------------------------------------------------------------



#ifdef INTEGER8_DEBUG_AR
    WRITE (*,'(A, I4, A,  I12, A)')             'weights  par_rank=', par_rank, ' max_ipcube=', max_ipcube, '    CHECK !!!!!!!!!!!!!!!!!!'
    WRITE (*,'(A, I4, A, 4I12)')                'weights  par_rank=', par_rank, ' i_p=', i_p
    WRITE (*,'(A, I4, A, 3I5, A, 3I4, A, 3I5)') 'weights  par_rank=', par_rank, ' ixyz=', ixyz, ' mxyz=', mxyz, ' nxyz=', nxyz  

!   IF(ALLOCATED(nodebuffer))      WRITE (*,'(A, I4, A, I12)')  'weights  par_rank=', par_rank, ' MAXVAL(nodebuffer(:))      =', MAXVAL(nodebuffer(:))
!                                  WRITE (*,'(A, I4, A, I12)')  'weights  par_rank=', par_rank, ' MAXVAL(nodebuffer_recv(:)) =', MAXVAL(nodebuffer_recv(:))
#endif

#ifdef WEIGHTSDEBUG
    CLOSE (FILE_1)
    CLOSE (FILE_2)
#endif                                                                                                                


  END SUBROUTINE weights
  
  
  !************************** Auxliary functions that are DB specific
  SUBROUTINE get_indices_by_coordinate(nvec,ixyz_vec,j_vec,i_vec,ireg)
    USE precision
    USE db_tree
    USE wlt_vars   ! dim
    USE parallel
    IMPLICIT NONE
    INTEGER , INTENT(IN) :: nvec,ireg,j_vec
    INTEGER, DIMENSION(dim,nvec), INTENT(IN) :: ixyz_vec !ixyz coordinates for points needed
    INTEGER, DIMENSION(nvec), INTENT(INOUT) :: i_vec       !i_vec is the vector of indices in 

    INTEGER :: i, ii, j, k, kk, j_df, wlt_type, face_type, id
    INTEGER, DIMENSION(dim) :: ixyz
    INTEGER*8, DIMENSION(0:dim) :: i_p     

#ifdef TREE_NODE_POINTER_STYLE_C
    INTEGER(pointer_pr) :: c_pointer                  ! pointer to node of tree structure (integer in C/C++)
#elif defined TREE_NODE_POINTER_STYLE_F
    TYPE(node), POINTER :: c_pointer                       ! (Fortran's native pointer)
#endif


    ! ireg = +-1 dummy call for compatibility with wrk_DB
    IF(ireg == 0) THEN !extracting inverse index
       DO i = 1,nvec
          CALL DB_test_node(ixyz_vec(:,i),j_vec,c_pointer)
          i_vec(i) = 0
          
          IF (is_ok(c_pointer)) THEN
             CALL DB_get_id_by_pointer( c_pointer, id )
             IF(BTEST(id,pos_sig).OR.BTEST(id,pos_adj)) &
                  CALL DB_get_ifunction_by_pointer( c_pointer, nvarI_nwlt, nvarI_nwlt, i_vec(i) )
          END IF

       END DO
    END IF


  END SUBROUTINE get_indices_by_coordinate
  
  ! used from local_lowpass_filt (wavelet_filters.f90) in parallel
  ! instead of u(get_indices_by_coordinate()) construct
  SUBROUTINE set_filter_window_values (minvar, maxvar, ixyz, j_ixyz, mask_out, u_out)
    USE precision
    USE db_tree
    USE wlt_vars   ! dim
    USE parallel
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: minvar, maxvar, ixyz(dim), j_ixyz
    LOGICAL, INTENT(INOUT) :: mask_out
    REAL(pr), INTENT(INOUT) :: u_out(1:1+maxvar-minvar)
    INTEGER :: id
    
#ifdef TREE_NODE_POINTER_STYLE_C
    INTEGER(pointer_pr) :: c_pointer                  ! pointer to node of tree structure (integer in C/C++)
#elif defined TREE_NODE_POINTER_STYLE_F
    TYPE(node), POINTER :: c_pointer                       ! (Fortran's native pointer)
#endif
    
    CALL DB_test_node(ixyz,j_ixyz,c_pointer)
    mask_out = .FALSE.
    u_out = 0.0_pr
    IF (is_ok(c_pointer)) THEN
       CALL DB_get_id_by_pointer( c_pointer, id )
       IF(BTEST(id,pos_sig).OR.BTEST(id,pos_adj)) THEN
          mask_out = .TRUE.
          CALL DB_get_function_by_pointer( c_pointer, minvar, maxvar, u_out )
       END IF
    END IF
    
  END SUBROUTINE set_filter_window_values

  SUBROUTINE release_memory_DB
    USE precision
    USE field
    USE db_tree
    USE util_vars               ! x
    USE additional_nodes        ! free_additional_nodes
    IMPLICIT NONE
    
    ! clean the database
    CALL DB_finalize
    
    ! clean database related variables
    IF (ALLOCATED(u)) DEALLOCATE(u)                 ! from init_DB/adapt_grid
    IF (ALLOCATED(f)) DEALLOCATE(f)                 ! from init_DB/adapt_grid
    IF (ALLOCATED(u_ex)) DEALLOCATE(u_ex)           ! from init_DB/adapt_grid
    IF (ALLOCATED(h)) DEALLOCATE(h)                 ! indices_tree
    IF (ALLOCATED(x)) DEALLOCATE(x)                 ! indices_tree
    IF (ALLOCATED(Nwlt_lev)) DEALLOCATE(Nwlt_lev)   ! indices_tree
    IF (ALLOCATED(xx)) DEALLOCATE(xx)               ! indices_tree
    
    ! clean other modules
    CALL free_additional_nodes
    CALL clean_db_tree

    !--- WARNING ---
    ! there are still some variables to clean
    ! (it could be nice to have a cleaning inside each module)
    
  END SUBROUTINE release_memory_DB

  !------------------------------------------------------------------------
  ! print U values
  SUBROUTINE debug_print_u (u, nwlt, n_var)
    USE db_tree
    USE wlt_vars                  ! dim
    USE wlt_trns_vars             ! indx_DB, xx
    USE parallel
    INTEGER, INTENT(IN) :: nwlt, n_var
    REAL(pr), INTENT(IN) :: u(1:nwlt,1:n_var)
    INTEGER   :: i, ixyz(dim), j, wlt_type, face_type, j_df, k, iwlt   
    INTEGER*8 :: i_p(0:dim)                                            
    INTEGER*8 :: ii                                                    
    
    i_p(0) = 1
    DO i=1,dim
       i_p(i) = i_p(i-1)*(1+nxyz(i))
    END DO

    PRINT *,'--------------- proc=', par_rank
    DO j = 1, j_lev
       DO wlt_type = MIN(j-1,1),2**dim - 1
          DO face_type = 0, 3**dim - 1
             DO j_df = 1, j_lev
                DO k=1,indx_DB(j_df,wlt_type,face_type,j)%length
                   iwlt = indx_DB(j_df,wlt_type,face_type,j)%p(k)%i
                   ii = indx_DB(j_df,wlt_type,face_type,j)%p(k)%ixyz                       ! j_lev
                   ixyz(1:dim) = (INT(MOD(ii-1,i_p(1:dim))/i_p(0:dim-1)))*2**(j_mx-j_lev)  ! j_mx
                   WRITE (*, '(", ixyz=",I4"[",'//CHAR(ICHAR('0')+dim)//'I5"], u(",I4,")=",'//CHAR(ICHAR('0')+n_var)//'E21.14,", s/l/rl=",3I5)') &
                        ii, ixyz(1:dim), iwlt, u(iwlt,1:n_var), &
                        indx_DB(j_df,wlt_type,face_type,j)%shift,&
                        indx_DB(j_df,wlt_type,face_type,j)%length, &
                        indx_DB(j_df,wlt_type,face_type,j)%real_length
                END DO
             END DO
          END DO
       END DO
    END DO
    PRINT *,'---------------'
    
  END SUBROUTINE debug_print_u
  
  SUBROUTINE delta_from_mask(delta_local,mask_local,nlocal)
    !--Calculates weights for volume integration based on the Trapezoid
    !--integration rule (second-order accurate).
    USE precision
    USE sizes
    USE wlt_vars
    USE util_vars
    USE pde
    USE db_tree 
    IMPLICIT NONE

    INTEGER, INTENT(IN) :: nlocal 
    REAL (pr), DIMENSION(nlocal), INTENT(IN)::    mask_local
    REAL (pr), DIMENSION(nlocal), INTENT(INOUT):: delta_local
    INTEGER :: i, j, k, m, idim, id, id1
    REAL (pr), DIMENSION(j_lev) :: delta_lev
    INTEGER, DIMENSION(dim) :: ixyz, ixyz1
    REAL (pr), DIMENSION(dim) :: lxyz
    INTEGER :: i1(1),     j_df, wlt_type, face_type, dir_type, step, step_cube    
    INTEGER*8 :: ii                                                               
    INTEGER*8, DIMENSION(0:dim) :: i_p                                            
    INTEGER,   DIMENSION(0:dim) :: i_p_cube                                                                       !AR 2/21/2011! added by AR
    !!                                              !!!!!!!!!!!!!!!!!!!!!! N O T E !!!!!!!!!!!!!!!!!!!!!!!!!      !AR 2/21/2011! added by AR
    !!                                              !!! AR IS NOT SURE WHETHER                           !!!      !AR 2/21/2011! added by AR
    !!                                              !!!   i_p_cube(0:dim)   must be INTEGER*8            !!!      !AR 2/21/2011! added by AR
    !!                                              !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!      !AR 2/21/2011! added by AR
    INTEGER, DIMENSION(dim) :: i_l, i_h
    INTEGER, DIMENSION(dim,1) :: ZERO
#ifdef TREE_NODE_POINTER_STYLE_C
    INTEGER(pointer_pr) :: c_pointer      ! pointer to node of tree structure (integer in C/C++)
#elif defined TREE_NODE_POINTER_STYLE_F
    TYPE(node), POINTER :: c_pointer      ! (Fortran's native pointer)
#endif

    ZERO = 0
    ixyz = 1 ! set up the last dimensions if 2-D case
    ixyz1 = 1 ! set up the last dimensions if 2-D case
    DO j = 1, j_lev
       delta_lev(j)   =  PRODUCT(h(j,1:dim))**(1.0_pr/REAL(dim,pr)) !seting delta for each level of resolution
    END DO

    delta_local =  PRODUCT(h(j_mn,1:dim))**(1.0_pr/REAL(dim,pr)) !seting delta to the coarsest level

    i_p_cube(0) = 1
    i_p(0) = 1
    DO i=1,dim
       i_p(i) = i_p(i-1)*(1+nxyz(i))
    END DO
    DO j = 1, j_lev
       DO wlt_type = MIN(j-1,1),2**dim-1
          DO face_type = 0, 3**dim - 1
             DO j_df = j, j_lev
                DO k = 1, indx_DB(j_df,wlt_type,face_type,j)%length  
                   c_pointer = indx_DB(j_df,wlt_type,face_type,j)%p(k)%nptr
                   i = indx_DB(j_df,wlt_type,face_type,j)%p(k)%i
                   CALL DB_get_id_by_pointer( c_pointer, id )
                   IF(mask_local(i) == 1.0_pr) THEN               ! ALL grid points on the mask
                      id1=IBSET(id,pos_msk)
                   ELSE
                      id1=IBCLR(id,pos_msk)
                   END IF
                   CALL DB_set_id_by_pointer (c_pointer,id1)
                END DO
             END DO
          END DO
       END DO
    END DO
 
    !----------------- Calculate local delta by level ----------------------
    step_cube = 1
    DO j = j_mn+1,j_lev
       step = 2**(j_lev-j)
       DO wlt_type = MIN(j-1,1),2**dim-1
          DO face_type = 0, 3**dim - 1
             DO j_df = j, j_lev
                DO k = 1, indx_DB(j_df,wlt_type,face_type,j)%length  
                   c_pointer = indx_DB(j_df,wlt_type,face_type,j)%p(k)%nptr
                   ii = indx_DB(j_df,wlt_type,face_type,j)%p(k)%ixyz
                   ixyz(1:dim) = INT(MOD(ii-1,i_p(1:dim))/i_p(0:dim-1))
                   CALL DB_get_id_by_pointer( c_pointer, id )
                   IF(BTEST(id,pos_msk)) THEN
                      i_l(:) = MAX(ixyz(:)-step,ZERO(:,1))
                      i_h(:) = MIN(i_l(:)+2*step,nxyz(1:dim)) 
                      i_l(:) = MAX(i_h(:)-2*step,ZERO(:,1))
                      i_l(:) = (1-prd(:))*i_l(:) + prd(:)*( ixyz - step )
                      i_h(:) = (1-prd(:))*i_h(:) + prd(:)*( ixyz + step )
                      !getting i based on location
                      i_p_cube(1:dim) = (i_h-i_l)/step_cube + 1
                      DO idim =1, dim
                         i_p_cube(idim) = i_p_cube(idim-1)*i_p_cube(idim)
                      END DO
                      DO m = 1,i_p_cube(dim)
                         ixyz1 = i_l + INT(MOD(m-1,i_p_cube(1:dim))/i_p_cube(0:dim-1))*step_cube
                         ixyz1(1:dim) = (1-prd(1:dim))*ixyz1(1:dim) + prd(1:dim)*MOD(ixyz1(:)+9*nxyz(1:dim),nxyz(1:dim))
                         CALL DB_test_node ( ixyz1, j_lev, c_pointer )
                         IF (is_ok(c_pointer)) THEN
#ifdef TREE_NODE_POINTER_STYLE_C
                               CALL DB_get_ifunction_by_pointer( c_pointer, nvarI_nwlt, nvarI_nwlt, i ) ! indx for inverse mapping
#elif defined TREE_NODE_POINTER_STYLE_F
                               i = c_pointer%ival( nvarI_nwlt )                                     ! indx for inverse mapping
#endif
                               IF(i > 0) delta_local(i) = delta_lev(j)
                         END IF
                      END DO
                   END IF
                END DO
             END DO
          END DO
       END DO
    END DO


#ifdef INTEGER8_DEBUG_AR
    WRITE (*,'(A, A, 4I12)')       'delta_from_mask  ', ' i_p     =', i_p
    WRITE (*,'(A, A, 4I12, A)')    'delta_from_mask  ', ' i_p_cube=', i_p_cube, '    CHECK !!!!!!!!!!!!!!!!!!'
#endif

  END SUBROUTINE delta_from_mask

END MODULE wlt_trns_mod
!**************************************************************************
! End wavelet transform subroutines
!**************************************************************************



!-------------------------------------------------------!
! define whether to use C or Fortran style node pointer !
#ifdef TREE_NODE_POINTER_STYLE_C
#undef TREE_NODE_POINTER_STYLE_C
#endif
#ifdef TREE_NODE_POINTER_STYLE_F
#undef TREE_NODE_POINTER_STYLE_F
#endif
! define whether to use C or Fortran style node pointer !
!-------------------------------------------------------!
