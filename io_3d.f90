MODULE io_3D
  USE precision
  USE variable_name_vars
  USE error_handling
  USE share_consts
  IMPLICIT NONE

  PUBLIC :: open_single_file, &          ! test if _log or _user_stats file can be opened
       write_log_header,      &          ! Write header for stats _log file
       write_log,             &          ! Write stats _log file
       check_inputs,                  &
       clean_write,                   &  ! set small elements of the given array to zero
       open_file,                     &  ! open .res file
       read_command_line_input,       &
       read_input,                    &
       read_input_finalize,           &
       read_solution,                 &  ! read .res file
       read_solution_dim,             &  ! get dimension and number of processors from .res file
       save_solution,                 &  ! write .res file
       error_opening,                 &  ! terminate program due to file opening error
       deallocate_indx_DB,            &  ! deallocate indx_DB
       read_solution_free                ! clean allocated in read_solution()


  PRIVATE :: open_file_aux,           &  ! add "XXXX.res" to the generic filename
       open_file_aux_common,          &  ! add "XXXX.com.res" to the generic filename
       assign_string_to_string,       &  ! assign string='some_string' with checking
       read_solution_aux_0,           &  ! obsolete 01/31/2007 format, do NOT edit, for backward compatibility only
       read_solution_aux_1,           &  ! 02/01/2007 format
       read_solution_aux_2,           &  ! 09/01/2011 format
       read_solution_version,         &  ! get version from .res file
       read_solution_aux_2common,     &  ! 09/01/2011 format
       read_solution_dim_aux_0,       &  ! obsolete 01/31/2007 format, do NOT edit, for backward compatibility only
       read_solution_hdr,             &  ! obsolete 01/31/2007 format, auxiliary function, do NOT edit
       save_solution_aux_1,           &  ! 02/01/2007 format
       save_solution_aux_2,           &  ! 09/01/2011 format
       save_solution_aux_2common,     &  ! 09/01/2011 format
       serious_warning_message,       &  ! warning if the format is obsolete
       set_default_types,             &  ! find the storage size of default integer and real
       set_zero_n_updt_for_multiproc, &  ! set n_updt to zero
       close_res_file,                &
       allocate_u,                    &  ! allocate u for multiple .res files
       allocate_indx_DB,              &  ! allocate indx_DB for multiple .res files
       rearrange_indx_DB_iu,          &  ! set correct iwlt index in indx_DB and u for multiple .res files
       io_verb_flag,                  &  ! transfer verb_flag into 0,1,2 for I/O subroutines
       directory_create                  ! create directory (may be system dependent)
  
  LOGICAL, PRIVATE, PARAMETER :: ok_to_pause = .FALSE.   ! turn on/off pauses inside io_3D module

  CHARACTER (LEN=u_variable_name_len), PRIVATE ::  tmp_name_str  ! Temporary storage for variables

  ! Flag to write each timestep into a separate subdirectory.
  ! System dependent. Use with caution.
  LOGICAL, PRIVATE :: allow_subdir_timestep

  INTEGER, PRIVATE :: i_default                          ! default INTEGER for RES_READ/WRITE
  REAL(pr), PRIVATE :: r_default                         ! default REAL for RES_READ/WRITE
  ! These two variables are used to determine the byte size of INTEGER and REAL(pr)
  ! The size of i_default is the one which is passed into RES_WRITE/READ as the first parameter.
  ! If .res file has been written on a system with INTEGER == INTEGER*4 [ by RES_WRITE('I4',...) ]
  ! to read it on a different system with INTEGER == INTEGER*8 user will have to provide
  ! i_default of type INTEGER*4.
  ! As for the real types, currently only REAL*4 and REAL*8 are supported.

  INTEGER, PRIVATE :: VERSION_STRING_LENGTH = 256        ! I/O version in .res file
  ! Use that string for whatever version information you need
  ! for future releases backward compatibility.
  ! Make sure to adjust save_solution_aux_1 and read_solution_aux_1 respectively
  ! in order to utilize that version information.


  INTEGER, PRIVATE   :: dot_res_file_version   ! save_solution() writes that version of .res file

  INTEGER, PRIVATE :: i_l_inp , i_h_inp             ! i_l and i_h that are read in from input file
  INTEGER, PRIVATE :: curparrank             ! current processor we are on

 ! Global values of  n_var_rst_file, card_n_var_save, save_vort  
 !   which are assigned in  read_solution_aux_2common(...)  
 !   and used in            read_solution_aux_2(...) 
  INTEGER, PRIVATE ::     len_uvars__aux_2__global, n_var_rst_file__aux_2__global, card_n_var_save__aux_2__global, save_vort__aux_2__global, n_integrated_tmp__aux_2__global

!#define INTEGER8_DEBUG_AR                                                                                         !AR 2/24/2011! added by AR
!#define RES_READ_INTEGER4_ixyz_AR                                                                                 !AR 2/27/2011! added by AR
!                          We need this variable in order to read the  32bit integer .res files.                   !AR 3/14/2011! added by Scott&AR
!                          But hereafter,  the user must add it to the FFLAGS in the machine specific makefile.    !AR 3/14/2011! added by Scott&AR
!#define RES_WRITE_INTEGER4_ixyz_AR                                                                                !AR 3/14/2011! added by Scott&AR
!                          We need this variable in order to write the  32bit integer .res files.                  !AR 3/14/2011! added by Scott&AR
!                          But hereafter,  the user must add it to the FFLAGS in the machine specific makefile.    !AR 3/14/2011! added by Scott&AR
!                          NOTE::                                                                                  !AR 3/14/2011! added by Scott&AR
!                                 If you use this, you have to make sure that                                      !AR 3/14/2011! added by Scott&AR
!                                 indx_DB_ptr%p(...)%ixyz   is within 32bit integer limit.                         !AR 3/14/2011! added by Scott&AR



CONTAINS
  

  !--------------------------------------------------------------------------------------------
  ! Write header for stats _log file
  ! IF you change what is written to the log file header MAKE SURE you change the log file 
  ! below in SUBROUTINE write_log
  SUBROUTINE write_log_header( j_max, dim_local )
    USE parallel
    USE io_3D_vars
    USE debug_vars         ! timers
    INTEGER, INTENT(IN) :: j_max, &        ! j_mx
         dim_local                         ! dim
    CHARACTER (LEN = 12) :: DATE_TIME_ZONE(3)
    INTEGER      :: DATE_TIME(8)           ! Date and Time   
    INTEGER      :: io_status, i, tnum
    CHARACTER(8) :: name, timername
    

    IF ( par_rank.EQ.0 ) THEN
       OPEN (UNIT=UNIT_LOG, FILE=file_name_log,  FORM='formatted', STATUS='old', POSITION='APPEND', IOSTAT=io_status)
       IF( io_status.EQ.0 ) THEN 
    
          CALL DATE_AND_TIME( DATE_TIME_ZONE(1), DATE_TIME_ZONE(2), DATE_TIME_ZONE(3), DATE_TIME )
          WRITE (UNIT_LOG, FMT='(A, I6, 2A, 2(I2.2,A), I4.4, A, I6, 2A, 3(I2.2,A), I3.3)') & 
                                                    '% Run on ', par_size, ' CPUs', &
                                                    '     on ',DATE_TIME(2),slash,DATE_TIME(3),slash,DATE_TIME(1),' (UTC:',DATE_TIME(4),')', & 
                                                    '     at ',DATE_TIME(5),':',DATE_TIME(6),':',DATE_TIME(7),'.',     DATE_TIME(8) 
          
          ! new log file for a (0) new run
          ! or for a (3) restart with initial conditions
          IF ( (IC_restart_mode .EQ. 0) .OR. (IC_restart_mode .EQ. 3) ) THEN
             
             ! columns  1 : 4+dim
             WRITE (UNIT=UNIT_LOG, ADVANCE='NO', FMT='("% t,  dt,  j_lev,  nwlt")') 
             DO i=1,dim_local
                WRITE(name ,FMT='( i3 )' ) i
                WRITE (UNIT=UNIT_LOG, ADVANCE='NO', FMT='(",  nxyz(", A, ")")') TRIM(ADJUSTL(name))
             END DO
             ! columns  5+dim : 5+dim+j_mx
             WRITE (UNIT=UNIT_LOG, ADVANCE='NO', FMT='(",  eps")')
             DO i = 1,j_max
                WRITE(name ,FMT='( i3 )' ) i
                WRITE (UNIT=UNIT_LOG, ADVANCE='NO', FMT='(",  nwlt(j=", A, ")")') TRIM(ADJUSTL(name))
             END DO
           
#ifdef USE_DEBUG_TIMERS_S
             ! columns  6+dim+j_mx : 6+dim+j_mx + (4*4+NUMBER_OF_TIMERS)
             WRITE (UNIT=UNIT_LOG, ADVANCE='NO', FMT='(",  (min, max, mean, stdev)NWLT,  (min, max, mean, stdev)GHO,  (min, max, mean, stdev)SEND,  (min, max, mean, stdev)RECV")')
             DO tnum=1,NUMBER_OF_TIMERS
                WRITE(timername ,FMT='( i5 )' ) tnum
                WRITE (UNIT=UNIT_LOG, ADVANCE='NO', FMT='(",  TIMER(", A, ")")') TRIM(ADJUSTL(timername))
             END DO
             WRITE (UNIT=UNIT_LOG, FMT='(" ")')
             WRITE (UNIT=UNIT_LOG, FMT='(A,A)') '%  ', '------------------------------------'
             DO tnum=1,NUMBER_OF_TIMERS
                WRITE (UNIT=UNIT_LOG, FMT='(A,A)') '%  ', TRIM(timer_header(tnum))
             END DO
             WRITE (UNIT=UNIT_LOG, FMT='(A,A)') '%  ', '------------------------------------'
#elif USE_DEBUG_TIMERS
             ! columns  6+dim+j_mx : 6+dim+j_mx + par_size*(4+NUMBER_OF_TIMERS)
             DO i=0, par_size-1
                WRITE(name ,FMT='( i5 )' ) i
                WRITE (UNIT=UNIT_LOG, ADVANCE='NO', FMT='(",  [pr=", A, "]  NWLT")') TRIM(ADJUSTL(name))
                WRITE (UNIT=UNIT_LOG, ADVANCE='NO', FMT='(",  GHO,  SEND,  RECV")')
                DO tnum=1,NUMBER_OF_TIMERS
                   WRITE(timername ,FMT='( i5 )' ) tnum
                   WRITE (UNIT=UNIT_LOG, ADVANCE='NO', FMT='(",  TIMER(", A, ")")') TRIM(ADJUSTL(timername))
                END DO
             END DO
             WRITE (UNIT=UNIT_LOG, FMT='(" ")')
             WRITE (UNIT=UNIT_LOG, FMT='(A,A)') '%  ', '------------------------------------'
             DO tnum=1,NUMBER_OF_TIMERS
                WRITE (UNIT=UNIT_LOG, FMT='(A,A)') '%  ', TRIM(timer_header(tnum))
             END DO
             WRITE (UNIT=UNIT_LOG, FMT='(A,A)') '%  ', '------------------------------------'
#endif
           
          END IF
          CLOSE (UNIT_LOG)
       END IF
    END IF
    
    CALL parallel_broadcast_int( io_status )    
    IF( io_status.NE.0 ) CALL error_opening (file_name_log)


  END SUBROUTINE write_log_header

  
  !--------------------------------------------------------------------------------------------
  ! Write stats _log file
  ! IF you change what is written to the log file MAKE SURE you change the log file header
  ! above in SUBROUTINE write_log_header
  SUBROUTINE write_log( j_max, j_loc, dim_local, t_loc, dt_loc, nxyz_loc, Nwlt_lev_local, nwlt_tot, nwlt_local, epsilon, &
       n_transfer_send_loc, n_transfer_recv_loc, nwlt_p_ghost_local )
    USE parallel
    USE io_3D_vars
    USE debug_vars         ! timers
    INTEGER, INTENT(IN) :: j_max, &              ! j_mx
         j_loc, &                                ! j_lev
         dim_local, &                            ! dim
         nxyz_loc(1:dim_local), &                ! nxyz
         Nwlt_lev_local(1:j_max), &              ! Nwlt_lev(:,1)
         nwlt_tot, &                             ! nwlt_global
         nwlt_local, &                           ! nwlt
         n_transfer_send_loc, &                      ! number of sig/adj nodes to be sent to others by the current processor
         n_transfer_recv_loc, &                      ! number of sig/adj nodes to be received by the current processor
         nwlt_p_ghost_local                      ! number of ghost points on the current processor
         
    REAL(pr), INTENT(IN) :: epsilon, &           ! eps
         t_loc, &                                ! t
         dt_loc                                  ! dt
    INTEGER :: io_status, i, tmp(1:j_loc), &
         tnum, tnum_all
#ifdef USE_DEBUG_TIMERS_S
    INTEGER  :: j
    REAL(pr) :: num_array(1:4*par_size), &       ! n_sig, n_gho, n_send, n_recv
         timers_all_proc(1:NUMBER_OF_TIMERS), &  ! Only for par_rank=0
         num_array_stats(1:16)                   ! (min,max,mean,stdev)[NWLT,GHO,SEND,RECV]
#elif USE_DEBUG_TIMERS
    REAL(pr) :: num_array(1:4*par_size), &       ! n_sig, n_gho, n_send, n_recv
         timers_all_proc(1:NUMBER_OF_TIMERS*par_size)
#endif
    
    IF ( par_rank.EQ.0 ) THEN
       OPEN (UNIT=UNIT_LOG, FILE=file_name_log, FORM='formatted', STATUS='UNKNOWN', POSITION='append', IOSTAT=io_status)
       IF (io_status.NE.0) CALL error_opening (file_name_log)
       ! columns  1 : 4+dim
       WRITE (UNIT=UNIT_LOG,ADVANCE='NO', &
            FMT='(es11.4,1x, es10.4,1x, i2,1x, i10,1x)') &
            t_loc, dt_loc, j_loc, nwlt_tot
       DO i=1,dim_local
          WRITE (UNIT=UNIT_LOG, ADVANCE='NO', FMT='(i6,1x)') nxyz_loc(i)
       END DO
       ! columns  5+dim : 5+dim+j_mx
       WRITE (UNIT=UNIT_LOG, ADVANCE='NO', FMT='(es10.4,1x)') epsilon
    END IF
    DO i=1,j_loc
       tmp(i) = Nwlt_lev_local(i)
       CALL parallel_global_sum( INTEGER=tmp(i) )
    END DO
    IF ( par_rank.EQ.0 ) THEN
       DO i=1,j_loc
          WRITE (UNIT=UNIT_LOG,ADVANCE='NO', FMT='(i10,1x)') tmp(i)
       END DO
       DO i = j_loc+1,j_max
          WRITE (UNIT=UNIT_LOG,ADVANCE='NO', FMT='(i10,1x)') 0
       END DO
    END IF
    
#ifdef USE_DEBUG_TIMERS_S
    ! columns  6+dim+j_mx : 6+dim+j_mx + (4*4+NUMBER_OF_TIMERS)
    IF ( par_rank.EQ.0 ) THEN
       DO tnum=1,NUMBER_OF_TIMERS
          timers_all_proc(tnum) = timer_val(tnum)
       END DO
    END IF
    
    num_array(1) = 1.0_pr* nwlt_local
    num_array(2) = 1.0_pr* nwlt_p_ghost_local
    num_array(3) = 1.0_pr* n_transfer_send_loc
    num_array(4) = 1.0_pr* n_transfer_recv_loc
    CALL parallel_gather( num_array, num_array, 4 )
    
    IF ( par_rank.EQ.0 ) THEN !(min, max, mean, stdev)
       j = 4*(par_size-1)
       DO i=0, 3
          num_array_stats(1+4*i) =    MINVAL( num_array(1+i:1+i+j:4))
          num_array_stats(2+4*i) =    MAXVAL( num_array(1+i:1+i+j:4))
          num_array_stats(3+4*i) =       SUM( num_array(1+i:1+i+j:4)) / REAL(par_size)
          num_array_stats(4+4*i) = SQRT( SUM((num_array(1+i:1+i+j:4)-num_array_stats(3+4*i))**2) / REAL(par_size))
       END DO
       WRITE (UNIT=UNIT_LOG, ADVANCE='NO', FMT='(16(i10,1x))') INT(num_array_stats(1:16))
       i=0
       DO tnum=1,NUMBER_OF_TIMERS
          tnum_all = i*NUMBER_OF_TIMERS+tnum
          WRITE (UNIT=UNIT_LOG, ADVANCE='NO', FMT='(es8.2,1x)') timers_all_proc(tnum_all)
       END DO
    END IF
#elif USE_DEBUG_TIMERS
    ! columns  6+dim+j_mx : 6+dim+j_mx + par_size*(4+NUMBER_OF_TIMERS)
    DO tnum=1,NUMBER_OF_TIMERS
       timers_all_proc(tnum) = timer_val(tnum)
    END DO
    CALL parallel_gather( timers_all_proc, timers_all_proc, NUMBER_OF_TIMERS )
    
    num_array(1) = 1.0_pr* nwlt_local
    num_array(2) = 1.0_pr* nwlt_p_ghost_local
    num_array(3) = 1.0_pr* n_transfer_send_loc
    num_array(4) = 1.0_pr* n_transfer_recv_loc
    CALL parallel_gather( num_array, num_array, 4 )
    
    IF ( par_rank.EQ.0 ) THEN
       DO i=0, par_size-1
          WRITE (UNIT=UNIT_LOG, ADVANCE='NO', FMT='(4(i10,1x))') INT(num_array(1+4*i:4+4*i))
          DO tnum=1,NUMBER_OF_TIMERS
             tnum_all = i*NUMBER_OF_TIMERS+tnum
             WRITE (UNIT=UNIT_LOG, ADVANCE='NO', FMT='(es8.2,1x)') timers_all_proc(tnum_all)
          END DO
       END DO
    END IF
#endif

    IF ( par_rank.EQ.0 ) THEN
       WRITE (UNIT=UNIT_LOG,ADVANCE='YES', FMT='(1x)')
       CLOSE(UNIT_LOG)
    END IF
    
  END SUBROUTINE write_log


  !--------------------------------------------------------------------------------------------
  ! Open _log or _user_stats file at processor #0
  ! STATUS='old'     - try to append to the existing _log or _user_stats file (or create a new one)
  ! STATUS='replace' - create new file
  SUBROUTINE open_single_file( FILE, STATUS )
    USE io_3D_vars
    USE debug_vars
    USE parallel
    USE error_handling
    INTEGER :: io_status
    CHARACTER(LEN=*) :: FILE, STATUS
    
    IF ( par_rank.NE.0 ) RETURN
    
    IF ( ADJUSTL(FILE).EQ.'user_stats' ) THEN
       OPEN  (UNIT=UNIT_USER_STATS, FILE = file_name_user_stats, FORM='formatted', STATUS=TRIM(STATUS), IOSTAT=io_status) 
       IF( io_status /= 0 ) OPEN  (UNIT=UNIT_USER_STATS, FILE = file_name_user_stats, FORM='formatted', STATUS='new', IOSTAT=io_status)
       IF( io_status /= 0 ) CALL error_opening(file_name_user_stats)
       CLOSE (UNIT_USER_STATS)
    ELSE IF ( ADJUSTL(FILE).EQ.'log' ) THEN
       OPEN  (UNIT=UNIT_LOG, FILE=file_name_log, FORM='formatted', STATUS=TRIM(STATUS), IOSTAT=io_status) 
       IF( io_status /= 0 ) OPEN  (UNIT=UNIT_LOG, FILE=file_name_log, FORM='formatted', STATUS='new', IOSTAT=io_status)
       IF( io_status /= 0 ) CALL error_opening(file_name_log)
       CLOSE (UNIT_LOG)
    ELSE IF ( ADJUSTL(FILE).EQ.'user_statz' ) THEN     !by Giuliano Nov 2012
       OPEN  (UNIT=UNIT_USER_STATZ, FILE = file_name_user_statz, FORM='formatted', STATUS=TRIM(STATUS), IOSTAT=io_status)
       IF( io_status /= 0 ) OPEN  (UNIT=UNIT_USER_STATZ, FILE = file_name_user_statz, FORM='formatted', STATUS='new', IOSTAT=io_status)
       IF( io_status /= 0 ) CALL error_opening(file_name_user_statz)
       CLOSE (UNIT_USER_STATZ)
    ELSE
       CALL error ( 'open_single_file: illegal FILE argument', IW=ignore_warnings )
       
    END IF
    
  END SUBROUTINE open_single_file


  !--------------------------------------------------------------------------------------------
  ! Open file:
  !   add "XXXX.pXXX.res" to the generic filename if open_file is NOT called
  !   with a constant filename parameter, i.e. open_file('something', ...)
  ! In parallel, each processors opens its own file.
  ! If filename is constant, it is the same file for each processor.
  ! form    - true for formatted output
  ! append  - true is we want to append to file
  SUBROUTINE open_file( filename, it, unit, form, append )
    USE io_3D_vars
    USE debug_vars
    USE error_handling
    INTEGER, INTENT(IN), OPTIONAL :: it, unit
    LOGICAL, INTENT(IN), OPTIONAL :: form, append 
    CHARACTER (LEN=*), INTENT(IN) :: filename
    
    CHARACTER (LEN=LEN(filename)) :: FILE_OUT
    INTEGER :: io_status

    ! add "XXXX.res" if filename is not a constant parameter
    CALL open_file_aux (filename, it, FILE_OUT)

    IF( form ) THEN 
       IF( append ) THEN
          OPEN (UNIT, FILE=FILE_OUT, STATUS='UNKNOWN',FORM='FORMATTED',&
               POSITION='append', ERR=1)
       ELSE 
          OPEN (UNIT, FILE=FILE_OUT, STATUS='UNKNOWN',FORM='FORMATTED', ERR=1)
       END IF
    ELSE
       IF( append ) THEN
          OPEN (UNIT, FILE=FILE_OUT, STATUS='UNKNOWN',FORM='UNFORMATTED',&
               POSITION='append', ERR=1)
       ELSE 
          OPEN (UNIT, FILE=FILE_OUT, STATUS='UNKNOWN',FORM='UNFORMATTED', ERR=1)
       END IF
    END IF
    RETURN

1   WRITE (*,'("Cannot open the file: ",A)') FILE_OUT
    CALL error

  END SUBROUTINE open_file


  !--------------------------------------------------------------------------------------------
  ! PUBLIC
  ! terminate program due to file opening error
  SUBROUTINE error_opening( fn )
    USE error_handling
    CHARACTER (LEN=*), INTENT(IN) :: fn
    
    PRINT *,' Error opening file : ', TRIM(fn)
    CALL error ('check if directory exists and is accessible')
    
  END SUBROUTINE error_opening


  !--------------------------------------------------------------------------------------------
  ! PRIVATE
  ! transfer verb_flag into 0,1,2 verb which are the debug levels of I/O subroutines
  ! (and which is to be tested as ( (verb.GE.1.AND.par_rank.EQ.0).OR.(verb.GE.2) )
  !
  ! possible outputs: 0,1,2,3
  !--------------------------------------------------------------------------------------------
  PURE FUNCTION io_verb_flag( bitwise_verb_flag )
    INTEGER, INTENT(IN) :: bitwise_verb_flag
    INTEGER :: io_verb_flag
    
    io_verb_flag = IBITS( bitwise_verb_flag, 0, 2 )
    
  END FUNCTION io_verb_flag

  !--------------------------------------------------------------------------------------------
  ! PUBLIC
  ! write .res file
  SUBROUTINE save_solution( nlocal, ieq, scl, it_local, VERBLEVEL, NAME, EXACT )
    USE pde                
    USE variable_mapping   ! n_var
    USE field              ! u 
    USE wlt_trns_mod       ! update_u_from_db_noidices()
    USE wlt_trns_vars      ! TYPE_DB
    USE io_3D_vars         ! datafile_formatted, file_wlt
    USE parallel           ! par_rank
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: nlocal, ieq
    REAL (pr), DIMENSION (1:n_var), INTENT (IN) :: scl
    INTEGER , INTENT (IN) :: it_local                           ! current iteration
    CHARACTER(LEN=LEN(file_wlt)), INTENT(IN), OPTIONAL :: NAME  ! exact name to use (instead of generic one)
    INTEGER, INTENT(IN), OPTIONAL :: VERBLEVEL                  ! verbose level 0,2 (instead of 1)
    LOGICAL, INTENT(IN), OPTIONAL :: EXACT                      ! write exact data without any assumptions
    CHARACTER(LEN=LEN(file_wlt)) name_tmp
    INTEGER :: i, save_vort, verb_level_local, name_len
    LOGICAL :: do_exact


    !============== GENERAL SECTION BEGIN =========================================================
    verb_level_local = 1                           ! default - do print short output at rank 0
    IF (PRESENT(VERBLEVEL)) &
         verb_level_local = io_verb_flag (VERBLEVEL)
    
    name_len = -1                            ! default - no NAME, so use generic
    name_tmp = ''
    IF (PRESENT(NAME)) name_tmp = TRIM(ADJUSTL(NAME))
    IF (PRESENT(NAME)) name_len = LEN(TRIM(ADJUSTL(NAME)))
    do_exact = .FALSE.                       ! default - assumptions and optimizations are allowed
    IF (PRESENT(EXACT)) do_exact = EXACT


    ! Update the values in u from the db in wlt space. This is done 
    ! so the values can be saved if needed.
    IF ( TYPE_DB == DB_TYPE_LINES ) CALL update_u_from_db_noidices(  u , nlocal ,n_var  , 1, n_var, 1 )
    

    ! Note for backwards compatibilty we set save_vort == 0. Vorticity is now saved as an additional 
    save_vort = 0

    IF( datafile_formatted ) THEN
       CALL clean_write (dim*(MAXVAL(nxyz(1:dim))+1), xx(0:MAXVAL(nxyz(1:dim)),1:dim), 1.0e-12_pr)
       CALL clean_write (nlocal*ieq, u(1:nlocal,1:ieq), 1.0e-12_pr)
    END IF
    !============== GENERAL SECTION END ===========================================================
    
    
    
    IF (( (dot_res_file_version.EQ.5).OR.(dot_res_file_version.EQ.7) ) .AND. ( par_size.GT.1 )) THEN
       !------------------------
       ! v7 - 05/20/2018
       ! v5 - 09/01/2011 .res endian independent format
       ! this is the recommended format for parallel
       IF (par_rank .EQ. 0 ) &
            CALL save_solution_aux_2common( nlocal, ieq, scl, it_local, save_vort, verb_level_local, name_tmp, name_len, do_exact )
       CALL save_solution_aux_2      ( nlocal, ieq, scl, it_local, save_vort, verb_level_local, name_tmp, name_len, do_exact )
    ELSE
       !------------------------
       ! v6 - 05/20/2018
       ! v4 - 09/01/2011 .res endian independent format
       ! this is the recommended format
       ! for sequential or single processor run
       CALL save_solution_aux_1( nlocal, ieq, scl, it_local, save_vort, verb_level_local, name_tmp, name_len, do_exact )
    END IF
    
  END SUBROUTINE save_solution


  !--------------------------------------------------------------------------------------------
  ! PRIVATE
  ! write .res file in a format as of 02/01/2007 .res endian independent
  !
  ! This is the only recommended format to use.
  ! Make sure that any RES_WRITE in save_solution_aux_1()
  ! has a correspondent RES_READ in read_solution_aux_1()
  !
  SUBROUTINE  save_solution_aux_1( nlocal, ieq, scl, it_local, save_vort, verb, name, name_len, exact )
    USE PRECISION
    USE pde                ! it, iwrite, dt
    USE variable_mapping   ! n_var
    USE field              ! u
    USE wlt_vars           ! dim, j_lev, j_mx
    USE wlt_trns_vars      ! TYPE_DB, indx_DB, xx
    USE io_3D_vars         ! file_wlt
    USE parallel           ! par_rank
    USE unused
    USE error_handling
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: nlocal, ieq, save_vort
    REAL (pr), DIMENSION (1:n_var) , INTENT (IN) :: scl
    INTEGER , INTENT (IN) :: it_local, &                          ! current iteration
         verb, &                                                  ! local verbose level (0,1,2)
         name_len                                                 ! length of name
    CHARACTER(LEN=LEN(file_wlt)), INTENT(IN) :: name              ! exact name to use
    LOGICAL, INTENT(IN) :: exact                                  ! write without any assumptions
    CHARACTER (LEN=LEN(file_wlt)) :: res_file_out                 ! .res filename
    INTEGER (4) :: ierr_4                                         ! number of errors
    INTEGER (4) :: num_4                                          ! number of elements for RES_WRITE
    CHARACTER (LEN=VERSION_STRING_LENGTH) :: version_string       ! I/O version in .res file
    CHARACTER (LEN=2) :: it_2, rt_2                           ! default types for RES_WRITE
    INTEGER :: j, wlt_type, face_type, &
         j_df, length, ie, i
    REAL(pr) :: tmp1, tmp2                                        ! temp for parallel printing


!!$    INTEGER*8, ALLOCATABLE :: ixyz_INTEGER8(:)                  !added by SR on 04/06/2011 
    INTEGER              :: alloc_stat                            ! allocation status                              !AR 3/30/2011! added by AR 
    INTEGER (4)          :: version_len_4                         ! Length of version_string                       !AR 3/30/2011! added by AR   

    
    IF (name_len.GE.0) THEN
       ! output file name is provided exactly by variable name
       res_file_out = TRIM(name)
    ELSE
       ! add "XXXX.res" to the generic filename file_wlt (global)
       ! put the file into a separate subdirectory XXXX/ (if .inp parameter results_dir_timestep=T)
       CALL open_file_aux (file_wlt, it_local, res_file_out, SUBDIR=allow_subdir_timestep)
    END IF
    
    
    
    ! generate no output for verb=0,
    ! some at rank 0 for verb=1
    ! more for all the processors for verb=2
    IF (verb.LE.1) THEN                ! (verb==1)
       IF (par_rank.EQ.0) THEN
          WRITE (*,'(" ")')
          IF (par_size.EQ.1) THEN
             WRITE (*,'(" Saving Solution file: ",A)') TRIM(res_file_out)
          ELSE
             WRITE (*,'(" Saving Solution files: ",A,", ... for ", I4," processors")') TRIM(res_file_out), par_size
          END IF
       END IF
    ELSE IF (verb.GE.2) THEN           ! (verb>=2)
       WRITE (*,'(" Saving Solution file: ",A)') TRIM(res_file_out)
       DO i=1,n_var
          WRITE (*,'(I4,A,F15.5,A,3(F15.5,1X) )') &
               i,' scl=',scl(i),' COEFminmax=',MINVAL(u(1:nlocal,i)),MAXVAL(u(1:nlocal,i)),MAXVAL(ABS(u(1:nlocal,i)))
       END DO
    END IF
    
    ! open .res file for writing
    ! check tree-c/io.h for details
    ierr_4 = 0
    CALL res_open( 'w', TRIM(res_file_out), LEN(TRIM(res_file_out)), ierr_4 )
    IF (ierr_4.NE.0_4) THEN
       WRITE (*,'("Cannot open the file: ",A)') TRIM(res_file_out)
       CALL error
    END IF

    ! save the data
    ! check tree-c/io.h for details

    ! set integer and real types for RES_WRITE
    CALL set_default_types( it_2, rt_2 )

    version_string = 'VERSION=6 @'                                                                                !AR 3/30/2011! added by AR   

    num_4 = LEN(TRIM(version_string))                 !  saving the string itself
    version_len_4 = num_4                                                                                         !AR 3/30/2011! added by AR   
    CALL RES_WRITE( 'i4', num_4, 1_4 )                ! here we save string's length
    CALL RES_WRITE( 'i1', version_string, num_4 )     ! here we save the string
    CALL RES_WRITE( it_2, dim, 1_4 )

    CALL RES_WRITE( it_2, par_size, 1_4 )             ! save total number of processors (non-MPI 1)
    num_4 = SIZE(par_proc_tree)                       !  and the domain decomposition vector
    CALL RES_WRITE( it_2, num_4, 1_4 )                !  (non-MPI par_proc_tree(1)==0)
    CALL RES_WRITE( it_2, par_proc_tree, num_4 )      !  ...
    CALL RES_WRITE( it_2, Nwlt_per_Tree, num_4 )      ! OLEG: 07.14.2011 for beter load balancing of restart files
    num_4 = dim
    CALL RES_WRITE( it_2, mxyz, num_4 )               ! ...
    CALL RES_WRITE( it_2, prd, num_4 )                ! ...

    CALL RES_WRITE( it_2, n_wlt_fmly, 1_4 )
    num_4 = n_wlt_fmly + 1                            ! the arrays are (0:n_wlt_fmly)
    CALL RES_WRITE( it_2, n_prdct, num_4 )            ! ...
    CALL RES_WRITE( it_2, n_updt, num_4 )             ! ...
    CALL RES_WRITE( it_2, n_assym_prdct, num_4 )      ! the arrays are (0:n_wlt_fmly,0) ( (0,0), (1,0), (2,0), ... )
    CALL RES_WRITE( it_2, n_assym_updt, num_4 )       ! ...
    CALL RES_WRITE( it_2, n_assym_prdct_bnd, num_4 )  ! ...
    CALL RES_WRITE( it_2, n_assym_updt_bnd, num_4 )   ! ...

    i = COUNT(n_var_save)
    CALL RES_WRITE( it_2, i, 1_4 )
    CALL RES_WRITE( it_2, save_vort, 1_4 )

    num_4 = dim
    CALL RES_WRITE( it_2, nxyz, num_4 )               ! the arrays are (1:dim)
    CALL RES_WRITE( it_2, grid, num_4 )               ! ...

    CALL RES_WRITE( it_2, j_mn, 1_4 )
    CALL RES_WRITE( it_2, j_mx, 1_4 )
    CALL RES_WRITE( it_2, j_lev, 1_4 )
    CALL RES_WRITE( it_2, nlocal, 1_4 )
    CALL RES_WRITE( it_2, it, 1_4 )
    CALL RES_WRITE( it_2, iwrite, 1_4 )
    CALL RES_WRITE( it_2, n_var, 1_4 )
    CALL RES_WRITE( it_2, n_integrated, 1_4 )
    CALL RES_WRITE( it_2, n_time_levels, 1_4 ) !No longer in use, left for backward compatibility 

    IF (verb.GE.2) THEN            ! (verb==2)
       PRINT *, TRIM(version_string)
       WRITE (*,'("# save_solution: proc #",I5)') par_rank
       PRINT *, 'dim=', dim
       PRINT *, 'n_wlt_fmly=', n_wlt_fmly
       PRINT *, 'n_prdct(:)            =', n_prdct(0:n_wlt_fmly)
       PRINT *, 'n_updt(:)             =', n_updt(0:n_wlt_fmly)
       PRINT *, 'n_assym_prdct(:,0)    =', n_assym_prdct(0:n_wlt_fmly,0)
       PRINT *, 'n_assym_updt(:,0)     =', n_assym_updt(0:n_wlt_fmly,0)
       PRINT *, 'n_assym_prdct_bnd(:,0)=', n_assym_prdct_bnd(0:n_wlt_fmly,0)
       PRINT *, 'n_assym_updt_bnd(:,0) =', n_assym_updt_bnd(0:n_wlt_fmly,0)
       PRINT *, 'COUNT(n_var_save)=', COUNT(n_var_save)
       PRINT *, 'save_vort=', save_vort
       PRINT *, 'nxyz=',nxyz(1:dim)
       PRINT *, 'mxyz=',mxyz(1:dim)
       PRINT *, 'prd =', prd(1:dim)
       PRINT *, 'grid=',grid(1:dim)
       PRINT *,'j_mn ',j_mn
       PRINT *,'j_mx ',j_mx
       PRINT *,'j_lev',j_lev
       PRINT *,'nwlt ',nlocal
       PRINT *,'it',it
       PRINT *,'iwrite',iwrite
       PRINT *,'n_var',n_var
       PRINT *,'n_integrated',n_integrated
       PRINT *, ' number_of_trees = ',  SIZE(par_proc_tree)
       PRINT *, ' par_proc_tree   : ',  par_proc_tree
       PRINT *, ' Nwlt_per_Tree   : ',  Nwlt_per_Tree
    END IF

    CALL RES_WRITE( rt_2, t, 1_4 )
    CALL RES_WRITE( rt_2, dt, 1_4 )
    
    IF (verb.GE.2) THEN            ! (verb==2)
       PRINT *, 't =',t
       PRINT *, 'dt=',dt
    END IF
    
    
    ! res_save_coeff=T - save wavelet coefficients in .res file
    num_4 = 0; IF (res_save_coeff) num_4 = 1
    CALL RES_WRITE( it_2, num_4, 1_4 )
    
    
    
    DO i = 1,dim
       num_4 = nxyz(i) + 1                            ! xx(0:nxyz(i),1:dim)
       CALL RES_WRITE( rt_2, xx(0,i), num_4 )         ! ...
    END DO
    
    DO j = 1, j_lev
       DO wlt_type = MIN(j-1,1),2**dim-1
          DO face_type = 0, 3**dim - 1
             DO j_df = j, j_lev
                length = indx_DB(j_df,wlt_type,face_type,j)%length
                CALL RES_WRITE( it_2, length, 1_4 )
                IF( length > 0 ) THEN
                   num_4 = length
                   CALL RES_WRITE( it_2, indx_DB(j_df,wlt_type,face_type,j)%p(1:length)%i,    num_4 )
                   CALL RES_WRITE( 'i8', indx_DB(j_df,wlt_type,face_type,j)%p(1:length)%ixyz, num_4 )

!!$                   IF( ALLOCATED(ixyz_INTEGER8) ) DEALLOCATE(ixyz_INTEGER8)                                      !added by SR on 04/06/2011       
!!$                   ALLOCATE( ixyz_INTEGER8(1:length), STAT=alloc_stat )                                          !added by SR on 04/06/2011
!!$                   ixyz_INTEGER8(1:length) = indx_DB(j_df,wlt_type,face_type,j)%p(1:length)%ixyz                 !added by SR on 04/06/2011
!!$                   CALL RES_WRITE( 'i8', ixyz_INTEGER8(1:length), num_4 )                                        !added by SR on 04/06/2011   
!!$                   IF( ALLOCATED(ixyz_INTEGER8) ) DEALLOCATE(ixyz_INTEGER8)                                      !added by SR on 04/06/2011       
!!$                   
!!$!AR 2/24/2011!  The following 9 lines are added by AR
!!$#ifdef INTEGER8_DEBUG_AR
!!$    WRITE (*,'(A, A, A, I, A, I, A, I2, A, I6, A, I6)')  &
!!$             'RES_WRITE     it_2=',                                   it_2, & 
!!$             '   indx_DB(j_df,wlt_type,face_type,j)%p(1)%ixyz=',      indx_DB(j_df,wlt_type,face_type,j)%p(1)%ixyz, &
!!$             '   indx_DB(j_df,wlt_type,face_type,j)%p(length)%ixyz=', indx_DB(j_df,wlt_type,face_type,j)%p(length)%ixyz, &
!!$             '   KIND(indx_DB_ptr%p()%ixyz)=',                        KIND(indx_DB(j_df,wlt_type,face_type,j)%p(length)%ixyz), &
!!$             '   num_4=',                                             num_4, &
!!$             '   length=',                                            length
!!$#endif
                   
                END IF
             END DO
          END DO
       END DO
    END DO

    ! save the scale array for this time step
    num_4 = n_var
    CALL RES_WRITE( rt_2, scl, num_4 )                ! scl(1:n_var)
    num_4 = n_var
    CALL RES_WRITE( it_2, n_var_wlt_fmly, num_4 )     ! n_var_wlt_fmly(1:n_var)
    
    IF (verb.GE.2) THEN            ! (verb==2)
       PRINT *, 'scl=',scl
       PRINT *, 'n_var_wlt_fmly=',n_var_wlt_fmly
    END IF
    
    
    ! --- WARNING ---
    ! n_var_wlt_fmly is not defined in save/read_solution
    



    ! save variables based on n_var_save_index
    DO ie = 1, n_var
       IF( n_var_save(ie) ) THEN
          
          IF (verb.GE.1) THEN
             tmp1 = MINVAL(u(:,n_var_index(ie)))
             tmp2 = MAXVAL(u(:,n_var_index(ie)))
             IF (verb.EQ.1) THEN            ! (verb==1) - print at rank 0 only
                CALL parallel_global_sum( REALMINVAL=tmp1 )
                CALL parallel_global_sum( REALMAXVAL=tmp2 )
                IF (par_rank.EQ.0) THEN
                   WRITE(*,'("Saving variable: ", A ,"(ie=",i2.2,")")') TRIM(u_variable_names(ie)), ie
                   WRITE(*,'(" MINMAX(u_i) ", A, 2(es15.8,1X) )' ) u_variable_names(ie), tmp1, tmp2
                END IF
             ELSE                           ! (verb>=2) - print at all processors
                WRITE(*,'("Saving variable: ", A ,"(ie=",i2.2,")")') TRIM(u_variable_names(ie)), ie
                WRITE(*,'(" MINMAX(u_i) ", A, 2(es15.8,1X) )' ) u_variable_names(ie), tmp1, tmp2
             END IF
          END IF
          
          num_4 = LEN(u_variable_names(ie))
          CALL RES_WRITE( 'i1', u_variable_names(ie), num_4 )  ! u_variable_names(ie)
          CALL RES_WRITE( it_2, ie, 1_4 )                      ! ie
          num_4 = nlocal
          CALL RES_WRITE( rt_2, u(1,n_var_index(ie)), num_4 )  ! u(1:nlocal, 1:n_var)
       END IF
    END DO

    CALL close_res_file( res_file_out )

  END SUBROUTINE save_solution_aux_1





  !--------------------------------------------------------------------------------------------
  ! PRIVATE
  ! write .res file in a format as of 09/01/2011 .res endian independent
  !
  ! This is the only recommended format to use in parallel.
  ! Make sure that any RES_WRITE in save_solution_aux_2common()
  ! has a correspondent RES_READ in read_solution_aux_2common()
  !
  SUBROUTINE  save_solution_aux_2common( nlocal, ieq, scl, it_local, save_vort, verb, name, name_len, exact )
    USE PRECISION
    USE pde                ! n_var, it, iwrite, dt
    USE variable_mapping   ! n_var
    USE field              ! u
    USE wlt_vars           ! dim, j_lev, j_mx
    USE wlt_trns_vars      ! TYPE_DB, indx_DB, xx
    USE io_3D_vars         ! file_wlt
    USE parallel           ! par_rank
    USE unused
    USE error_handling
    IMPLICIT NONE
    INTEGER ,                        INTENT (IN) :: nlocal, ieq, save_vort
    REAL (pr), DIMENSION (1:n_var) , INTENT (IN) :: scl
    INTEGER ,                        INTENT (IN) :: it_local, &   ! current iteration
                                                    verb, &       ! local verbose level (0,1,2)
                                                    name_len      ! length of name
    CHARACTER(LEN=LEN(file_wlt)),    INTENT(IN) :: name           ! exact name to use
    LOGICAL,                         INTENT(IN) :: exact          ! write without any assumptions
    CHARACTER(LEN=LEN(file_wlt)) :: res_file_out                  ! .res filename
    INTEGER (4) :: ierr_4                                         ! number of errors
    INTEGER (4) :: num_4                                          ! number of elements for RES_WRITE
    CHARACTER (LEN=VERSION_STRING_LENGTH) :: version_string       ! I/O version in .res file
    CHARACTER (LEN=2) :: it_2, rt_2                               ! default types for RES_WRITE
    INTEGER     :: j, wlt_type, face_type, &
                   j_df, length, ie, i
    REAL(pr)    :: tmp1, tmp2                                     ! temp for parallel printing

    INTEGER     :: alloc_stat                                     ! allocation status                           
    INTEGER (4) :: version_len_4                                  ! Length of version_string                    

    
    IF (name_len.GE.0) THEN
       ! output file name is provided exactly by variable name
       res_file_out = TRIM(name)
    ELSE
       ! add "XXXX.res" to the generic filename file_wlt (global)
       CALL open_file_aux_common (file_wlt, it_local, res_file_out, SUBDIR=allow_subdir_timestep)
    END IF
    
    
    
    ! generate no output for verb=0,
    ! some at rank 0 for verb=1
    ! more for all the processors for verb=2
    IF (verb.GE.1) THEN                ! (verb>=1)
       WRITE (*,'(" Saving Common Solution file: ",A)') TRIM(res_file_out)
!       DO i=1,n_var
!          WRITE (*,'(I4,A,F15.5,A,3(F15.5,1X) )') &
!               i,' scl=',scl(i),' COEFminmax=',MINVAL(u(1:nlocal,i)),MAXVAL(u(1:nlocal,i)),MAXVAL(ABS(u(1:nlocal,i)))
!       END DO
    END IF
    
    ! open .res file for writing
    ! check tree-c/io.h for details
    ierr_4 = 0
    CALL res_open( 'w', TRIM(res_file_out), LEN(TRIM(res_file_out)), ierr_4 )
    IF (ierr_4.NE.0_4) THEN
       WRITE (*,'("Cannot open the file: ",A)') TRIM(res_file_out)
       CALL error
    END IF

    ! save the data
    ! check tree-c/io.h for details

    ! set integer and real types for RES_WRITE
    CALL set_default_types( it_2, rt_2 )

    ! 
    ! VERSION=5 is the first parallel version which saves the common information in a separate file called .com.res
    ! VERSION=4 and VERSION=5 are the same in terms of the information which are saved; however,
    ! VERSION=4 is for serial run       and only one .res file    is saved for each station   or
    !              for old parallel run and .p*.res              are saved for each station   but
    ! VERSION=5 is for parallel run     and  .com.res & .p*.res  are saved for each station
    !
    version_string = 'VERSION=7 @'                                                                            

    num_4 = LEN(TRIM(version_string))                 !  saving the string itself
    version_len_4 = num_4      
    CALL RES_WRITE( 'i4', num_4, 1_4 )                ! here we save string's length
    CALL RES_WRITE( 'i1', version_string, num_4 )     ! here we save the string
    CALL RES_WRITE( it_2, dim, 1_4 )

    CALL RES_WRITE( it_2, par_size, 1_4 )             ! save total number of processors (non-MPI 1)
    num_4 = SIZE(par_proc_tree)                       !  and the domain decomposition vector
    CALL RES_WRITE( it_2, num_4, 1_4 )                !  (non-MPI par_proc_tree(1)==0)
    CALL RES_WRITE( it_2, par_proc_tree, num_4 )      !  ...
    CALL RES_WRITE( it_2, Nwlt_per_Tree, num_4 )      ! OLEG: 07.14.2011 for beter load balancing of restart files
    num_4 = dim
    CALL RES_WRITE( it_2, mxyz, num_4 )               ! ...
    CALL RES_WRITE( it_2, prd, num_4 )                ! ...

    CALL RES_WRITE( it_2, n_wlt_fmly, 1_4 )
    num_4 = n_wlt_fmly + 1                            ! the arrays are (0:n_wlt_fmly)
    CALL RES_WRITE( it_2, n_prdct, num_4 )            ! ...
    CALL RES_WRITE( it_2, n_updt, num_4 )             ! ...
    CALL RES_WRITE( it_2, n_assym_prdct, num_4 )      ! the arrays are (0:n_wlt_fmly,0) ( (0,0), (1,0), (2,0), ... )
    CALL RES_WRITE( it_2, n_assym_updt, num_4 )       ! ...
    CALL RES_WRITE( it_2, n_assym_prdct_bnd, num_4 )  ! ...
    CALL RES_WRITE( it_2, n_assym_updt_bnd, num_4 )   ! ...

    i = COUNT(n_var_save)
    CALL RES_WRITE( it_2, i, 1_4 )
    CALL RES_WRITE( it_2, save_vort, 1_4 )

    num_4 = dim
    CALL RES_WRITE( it_2, nxyz, num_4 )               ! the arrays are (1:dim)
    CALL RES_WRITE( it_2, grid, num_4 )               ! ...

    CALL RES_WRITE( it_2, j_mn, 1_4 )
    CALL RES_WRITE( it_2, j_mx, 1_4 )
    CALL RES_WRITE( it_2, j_lev, 1_4 )
!!  CALL RES_WRITE( it_2, nlocal, 1_4 )
    CALL RES_WRITE( it_2, it, 1_4 )
    CALL RES_WRITE( it_2, iwrite, 1_4 )
    CALL RES_WRITE( it_2, n_var, 1_4 )
    CALL RES_WRITE( it_2, n_integrated, 1_4 )
    CALL RES_WRITE( it_2, n_time_levels, 1_4 ) !No longer in use, left for backward compatibility 

    IF (verb.GE.2) THEN            ! (verb==2)
       PRINT *, TRIM(version_string)
       WRITE (*,'("# save_solution: proc #",I5)') par_rank
       PRINT *, 'dim=', dim
       PRINT *, 'n_wlt_fmly=', n_wlt_fmly
       PRINT *, 'n_prdct(:)            =', n_prdct(0:n_wlt_fmly)
       PRINT *, 'n_updt(:)             =', n_updt(0:n_wlt_fmly)
       PRINT *, 'n_assym_prdct(:,0)    =', n_assym_prdct(0:n_wlt_fmly,0)
       PRINT *, 'n_assym_updt(:,0)     =', n_assym_updt(0:n_wlt_fmly,0)
       PRINT *, 'n_assym_prdct_bnd(:,0)=', n_assym_prdct_bnd(0:n_wlt_fmly,0)
       PRINT *, 'n_assym_updt_bnd(:,0) =', n_assym_updt_bnd(0:n_wlt_fmly,0)
       PRINT *, 'COUNT(n_var_save)=', COUNT(n_var_save)
       PRINT *, 'save_vort=', save_vort
       PRINT *, 'nxyz=',nxyz(1:dim)
       PRINT *, 'mxyz=',mxyz(1:dim)
       PRINT *, 'prd =', prd(1:dim)
       PRINT *, 'grid=',grid(1:dim)
       PRINT *,'j_mn ',j_mn
       PRINT *,'j_mx ',j_mx
       PRINT *,'j_lev',j_lev
!!     PRINT *,'nwlt ',nlocal
       PRINT *,'it',it
       PRINT *,'iwrite',iwrite
       PRINT *,'n_var',n_var
       PRINT *,'n_integrated',n_integrated
       PRINT *, ' number_of_trees = ',  SIZE(par_proc_tree)
       PRINT *, ' par_proc_tree   : ',  par_proc_tree
       PRINT *, ' Nwlt_per_Tree   : ',  Nwlt_per_Tree
    END IF

    CALL RES_WRITE( rt_2, t, 1_4 )
    CALL RES_WRITE( rt_2, dt, 1_4 )
    
    IF (verb.GE.2) THEN            ! (verb==2)
       PRINT *, 't =',t
       PRINT *, 'dt=',dt
    END IF


    ! res_save_coeff=T - save wavelet coefficients in .res file
    num_4 = 0; IF (res_save_coeff) num_4 = 1
    CALL RES_WRITE( it_2, num_4, 1_4 )
    
    

    CALL close_res_file( res_file_out )

  END SUBROUTINE save_solution_aux_2common





  !--------------------------------------------------------------------------------------------
  ! PRIVATE
  ! write .res file in a format as of 09/01/2011 .res endian independent
  !
  ! This is the only recommended format to use in parallel.
  ! Make sure that any RES_WRITE in save_solution_aux_2()
  ! has a correspondent RES_READ in read_solution_aux_2()
  !
  SUBROUTINE  save_solution_aux_2( nlocal, ieq, scl, it_local, save_vort, verb, name, name_len, exact )
    USE PRECISION
    USE pde                ! it, iwrite, dt
    USE variable_mapping   ! n_var
    USE field              ! u
    USE wlt_vars           ! dim, j_lev, j_mx
    USE wlt_trns_vars      ! TYPE_DB, indx_DB, xx
    USE io_3D_vars         ! file_wlt
    USE parallel           ! par_rank
    USE error_handling
    IMPLICIT NONE
    INTEGER ,                        INTENT (IN) :: nlocal, ieq, save_vort
    REAL (pr), DIMENSION (1:n_var) , INTENT (IN) :: scl
    INTEGER ,                        INTENT (IN) :: it_local, &   ! current iteration
                                                    verb, &       ! local verbose level (0,1,2)
                                                    name_len      ! length of name
    CHARACTER(LEN=LEN(file_wlt)),    INTENT(IN) :: name           ! exact name to use
    LOGICAL,                         INTENT(IN) :: exact          ! write without any assumptions
    CHARACTER(LEN=LEN(file_wlt)) :: res_file_out                  ! .res filename
    INTEGER (4) :: ierr_4                                         ! number of errors
    INTEGER (4) :: num_4                                          ! number of elements for RES_WRITE
    CHARACTER (LEN=VERSION_STRING_LENGTH) :: version_string       ! I/O version in .res file
    CHARACTER (LEN=2) :: it_2, rt_2                               ! default types for RES_WRITE
    INTEGER     :: j, wlt_type, face_type, &
                   j_df, length, ie, i
    REAL(pr)    :: tmp1, tmp2                                     ! temp for parallel printing

    INTEGER     :: alloc_stat                                     ! allocation status                           
    INTEGER (4) :: version_len_4                                  ! Length of version_string                    

    
    IF (name_len.GE.0) THEN
       ! output file name is provided exactly by variable name
       res_file_out = TRIM(name)
    ELSE
       ! add "XXXX.res" to the generic filename file_wlt (global)
       CALL open_file_aux (file_wlt, it_local, res_file_out, SUBDIR=allow_subdir_timestep)
    END IF
    
    
    
    ! generate no output for verb=0,
    ! some at rank 0 for verb=1
    ! more for all the processors for verb=2
    IF (verb.LE.1) THEN           ! (verb<=1)
       IF (par_rank.EQ.0) THEN
          WRITE (*,'(" ")')
          IF (par_size.EQ.1) THEN
             WRITE (*,'(" Saving Solution file: ",A)') TRIM(res_file_out)
          ELSE
             WRITE (*,'(" Saving Solution files: ",A,".p*.res, ... for ", I4," processors")') TRIM( res_file_out( 1:INDEX(res_file_out(1:LEN_TRIM(res_file_out)-4),'.',BACK=.TRUE.)-1 )), par_size
          END IF
       END IF
    ELSE                          ! (verb>=2)
       WRITE (*,'(" Saving Solution file: ",A)') TRIM(res_file_out)
       DO i=1,n_var
          WRITE (*,'(I4,A,F15.5,A,3(F15.5,1X) )') &
               i,' scl=',scl(i),' COEFminmax=',MINVAL(u(1:nlocal,i)),MAXVAL(u(1:nlocal,i)),MAXVAL(ABS(u(1:nlocal,i)))
       END DO
    END IF
    
    ! open .res file for writing
    ! check tree-c/io.h for details
    ierr_4 = 0
    CALL res_open( 'w', TRIM(res_file_out), LEN(TRIM(res_file_out)), ierr_4 )
    IF (ierr_4.NE.0_4) THEN
       WRITE (*,'("Cannot open the file: ",A)') TRIM(res_file_out)
       CALL error
    END IF

    ! save the data
    ! check tree-c/io.h for details

    ! set integer and real types for RES_WRITE
    CALL set_default_types( it_2, rt_2 )

    ! 
    ! VERSION=5 is the first parallel version which saves the common information in a separate file called .com.res
    ! VERSION=4 and VERSION=5 are the same in terms of the information which are saved; however,
    ! VERSION=4 is for serial run       and only one .res file    is saved for each station   or
    !              for old parallel run and .p*.res              are saved for each station   but
    ! VERSION=5 is for parallel run     and  .com.res & .p*.res  are saved for each station
    !
    version_string = 'VERSION=7 @'                                                                          

    num_4 = LEN(TRIM(version_string))                 !  saving the string itself
    version_len_4 = num_4                                                                              
    CALL RES_WRITE( 'i4', num_4, 1_4 )                ! here we save string's length
    CALL RES_WRITE( 'i1', version_string, num_4 )     ! here we save the string


    CALL RES_WRITE( it_2, nlocal, 1_4 )
    IF (verb.GE.2) PRINT *,' nwlt =', nlocal



    DO i = 1,dim
       num_4 = nxyz(i) + 1                            ! xx(0:nxyz(i),1:dim)
       CALL RES_WRITE( rt_2, xx(0,i), num_4 )         ! ...
    END DO

    DO j = 1, j_lev
       DO wlt_type = MIN(j-1,1),2**dim-1
          DO face_type = 0, 3**dim - 1
             DO j_df = j, j_lev
                length = indx_DB(j_df,wlt_type,face_type,j)%length
                CALL RES_WRITE( it_2, length, 1_4 )
                IF( length > 0 ) THEN
                   num_4 = length
                   CALL RES_WRITE( it_2, indx_DB(j_df,wlt_type,face_type,j)%p(1:length)%i,    num_4 )
                   CALL RES_WRITE( 'i8', indx_DB(j_df,wlt_type,face_type,j)%p(1:length)%ixyz, num_4 )    
                   
#ifdef INTEGER8_DEBUG_AR
    WRITE (*,'(A, A, A, I, A, I, A, I2, A, I6, A, I6)')  &
             'RES_WRITE     it_2=',                                   it_2, & 
             '   indx_DB(j_df,wlt_type,face_type,j)%p(1)%ixyz=',      indx_DB(j_df,wlt_type,face_type,j)%p(1)%ixyz, &
             '   indx_DB(j_df,wlt_type,face_type,j)%p(length)%ixyz=', indx_DB(j_df,wlt_type,face_type,j)%p(length)%ixyz, &
             '   KIND(indx_DB_ptr%p()%ixyz)=',                        KIND(indx_DB(j_df,wlt_type,face_type,j)%p(length)%ixyz), &
             '   num_4=',                                             num_4, &
             '   length=',                                            length
#endif
                   
                END IF
             END DO
          END DO
       END DO
    END DO

    ! save the scale array for this time step
    num_4 = n_var
    CALL RES_WRITE( rt_2, scl, num_4 )                ! scl(1:n_var)
    num_4 = n_var
    CALL RES_WRITE( it_2, n_var_wlt_fmly, num_4 )     ! n_var_wlt_fmly(1:n_var)
    
    IF (verb.GE.2) THEN            ! (verb==2)
       PRINT *, 'scl=',scl
       PRINT *, 'n_var_wlt_fmly=',n_var_wlt_fmly
    END IF
    
    
    ! --- WARNING ---
    ! n_var_wlt_fmly is not defined in save/read_solution
    



    ! save variables based on n_var_save_index
    DO ie = 1, n_var
       IF( n_var_save(ie) ) THEN
          
          IF (verb.GE.1) THEN
             tmp1 = MINVAL(u(:,n_var_index(ie)))
             tmp2 = MAXVAL(u(:,n_var_index(ie)))
             IF (verb.EQ.1) THEN            ! (verb==1) - print at rank 0 only
                CALL parallel_global_sum( REALMINVAL=tmp1 )
                CALL parallel_global_sum( REALMAXVAL=tmp2 )
                IF (par_rank.EQ.0) THEN
                   WRITE(*,'("Saving variable: ", A ,"(ie=",i2.2,")")') TRIM(u_variable_names(ie)), ie
                   WRITE(*,'(" MINMAX(u_i) ", A, 2(es15.8,1X) )' ) u_variable_names(ie), tmp1, tmp2
                END IF
             ELSE                           ! (verb>=2) - print at all processors
                WRITE(*,'("Saving variable: ", A ,"(ie=",i2.2,")")') TRIM(u_variable_names(ie)), ie
                WRITE(*,'(" MINMAX(u_i) ", A, 2(es15.8,1X) )' ) u_variable_names(ie), tmp1, tmp2
             END IF
          END IF
          
          num_4 = LEN(u_variable_names(ie))
          CALL RES_WRITE( 'i1', u_variable_names(ie), num_4 )  ! u_variable_names(ie)
          CALL RES_WRITE( it_2, ie, 1_4 )                      ! ie
          num_4 = nlocal
          CALL RES_WRITE( rt_2, u(1,n_var_index(ie)), num_4 )  ! u(1:nlocal, 1:n_var)
       END IF
    END DO

    CALL close_res_file( res_file_out )

  END SUBROUTINE save_solution_aux_2







  !--------------------------------------------------------------------------------------------
  !
  ! Read in solution file. Used for restarting 
  ! and in seperate post_processing codes
  !
  ! Arguments
  ! scl - the scl array (will be allocated inside if not associated already)
  ! filename    - the solution file name
  ! do_read_all_vars - If true a special mode is forced that reads all field variables
  !                    in this solution file. This is used for the compare_solutions
  !                    code which is used for checking test cases
  !
  !ie test_iso_0001.res is station_num = 1
  !
  ! VERBLEVEL (default 1) = 0, 1, 2 - verbosity level
  !
  ! EXACT (default F) is introduced to allow use of read_solution from
  ! a stand-alone application to read exactly what is inside .res file,
  ! without making assumptions if it is a restart or initial condition.
  ! In a stand alone application we don't care, we need exact data.
  !
  ! APPEND (default T) is introduced to allow reading multiple .res files.
  ! The data from all files will be combined together.
  !
  ! IGNOREINIT (default F) ignores old values, e.g. from .inp file
  ! No error checking performed if IGNOREINIT is set.
  !
  ! CLEAN_IC_D (default T) deallocate IC_par_proc_tree() anf IC_Nwlt_per_Tree. Normally we deallocate
  !                        and set a new repartitioning, except for called with
  !                        domain_meth=-1 res2vis and n2m
  !
  ! ZERO_UPDATE (default T) set n_update to zero (n2m does not need that)
  !                         for parallel code only, otherwise - ignored
  !
  ! POSTPROCESS_PRE_INIT_DB (default F)    CALL read_solution_aux_1  with PART_READ=2  [partial read - read all but u and indx_DB]
  !                                        This can be used for post-processing tools (such as Adaptive Spectrum)
  !                                        in order to have the required parameters to   CALL pre_init_DB   prior to reading the solutions in loop
  !
  ! PART_READ (default 0)
  SUBROUTINE read_solution ( scl, filename, READ_ALL_VARS, READ_COMMONFILE, VERBLEVEL, EXACT, APPEND, IGNOREINIT, &
                                            CLEAN_IC_DD, ZERO_UPDATE, POSTPROCESS_PRE_INIT_DB, PART_READ, NUM_VARS )
    USE PRECISION   ! (pr)
    USE pde         ! u_variable_names
    USE variable_mapping ! u_variable_names
    USE sizes       ! nwlt
    USE io_3D_vars  ! IC _restart _mode
    USE debug_vars  ! error
    USE parallel
    USE error_handling
!!$    USE field              ! u ---DEBUG
!!$    USE wlt_trns_mod       ! ---DEBUG
    IMPLICIT NONE
    CHARACTER (LEN=*) , INTENT(IN) ::  filename
    REAL (pr) ,  DIMENSION (:), POINTER :: scl
    LOGICAL, INTENT(IN), OPTIONAL :: READ_ALL_VARS             ! if true read all vars
    LOGICAL, INTENT(IN), OPTIONAL :: READ_COMMONFILE           ! if true read .com.res file and exit
    INTEGER, INTENT(IN), OPTIONAL :: VERBLEVEL                 ! verbouse reading
    LOGICAL, INTENT(IN), OPTIONAL :: EXACT                     ! get data without any assumptions
    LOGICAL, INTENT(IN), OPTIONAL :: APPEND                    ! append data from multiple .res files
    LOGICAL, INTENT(IN), OPTIONAL :: IGNOREINIT                ! ingore .inp values
    LOGICAL, INTENT(IN), OPTIONAL :: CLEAN_IC_DD               ! deallocate IC_par_proc_tree() & IC_Nwlt_per_Tree
    LOGICAL, INTENT(IN), OPTIONAL :: ZERO_UPDATE               ! set n_update to zero (n2m does not need that)
    LOGICAL, INTENT(IN), OPTIONAL :: POSTPROCESS_PRE_INIT_DB   ! set PART_READ=2  [partial read - read all but u and indx_DB]
    INTEGER, INTENT(IN), OPTIONAL :: PART_READ                 ! partial reading
    INTEGER, INTENT(OUT), OPTIONAL :: NUM_VARS                 ! number of variables inside .res file
    INTEGER :: verb_level_local,                      & ! 0, 1, 2
         len_uvars, ierr,                       &
         IC_par_rank, i, &                              ! counter of files to read
         do_part_read, &
         alloc_stat                                     ! allocation status
    LOGICAL :: do_exact, do_read_all_vars, &
         do_append, do_ignoreinit, do_clean_ic_dd, &
         do_zero_update, do_postprocess_preinitdb, &
         do_read_commonfile
    CHARACTER (LEN=par_LENSTR1) :: IC_par_rank_str,  & ! par_rank_str of .res file
         tmp_str
    CHARACTER (LEN=par_LENSTR1) :: IC_par_rank_read_string
    CHARACTER (LEN=1024)        :: fntr_read_string
    LOGICAL, POINTER :: fntr(:)                        ! of length IC_par_size - which files to read
    REAL (pr), POINTER :: utmp(:,:), uold(:,:)         ! buffers for u array rearranging
    INTEGER, POINTER :: olen(:,:,:,:)        ! old length buffer for index_DB
    
    
    !============== GENERAL SECTION BEGIN =========================================================
    do_read_all_vars    = .FALSE.          ! read some variables only (e.g. in restart)
    do_read_commonfile  = .FALSE.          ! do not exit after commonfile reading
    verb_level_local = 1                   ! do print short output
    do_exact = .FALSE.                     ! allow assumptions while reading
    do_append = .TRUE.                     ! append data from multiple .res files
    do_ignoreinit = .FALSE.                ! do not ignore .inp file
    do_clean_ic_dd = .TRUE.
    do_zero_update = .TRUE.
    do_postprocess_preinitdb = .FALSE.
    do_part_read = 0                       ! read all
    
    
    IF (PRESENT(READ_ALL_VARS)) do_read_all_vars = READ_ALL_VARS
    IF (PRESENT(READ_COMMONFILE)) do_read_commonfile = READ_COMMONFILE
    IF (PRESENT(VERBLEVEL)) verb_level_local = io_verb_flag (VERBLEVEL)
    IF (PRESENT(EXACT)) do_exact = EXACT
    IF (PRESENT(APPEND)) do_append = APPEND
    IF (PRESENT(IGNOREINIT)) do_ignoreinit = IGNOREINIT
    IF (PRESENT(CLEAN_IC_DD)) do_clean_ic_dd = CLEAN_IC_DD
    IF (PRESENT(ZERO_UPDATE)) do_zero_update = ZERO_UPDATE
    IF (PRESENT(POSTPROCESS_PRE_INIT_DB)) do_postprocess_preinitdb = POSTPROCESS_PRE_INIT_DB
    IF (PRESENT(PART_READ)) do_part_read = PART_READ
    
    
    NULLIFY (fntr)
    NULLIFY (utmp)
    NULLIFY (uold)
    NULLIFY (olen)
    
    
    !
    ! read .res file version
    ! initialize IC_RES_VERSION
    !
    CALL read_solution_version( filename )
    
    
    ! reset flags and check arguments
    IF (do_exact) THEN
       do_read_all_vars = .TRUE.
    END IF
    len_uvars = 0; IF (ALLOCATED(u_variable_names)) len_uvars = SIZE(u_variable_names(:))
    
    
    ! set default for old .res file versions
    ! wavelet coefficients are stored in .res file
    res_save_coeff_IC = .TRUE.
    
    
    !============== GENERAL SECTION END ===========================================================
    
    
    !-----------------------------------------
    ! Read domain decomposition related data:
    ! dim, IC_par_size, IC_par_proc_tree(:), IC_Nwlt_per_Tree allocate and set fntr(:)
    ! from the file which exact name is provided in .inp (it could be from some other processor)
    ! First, try reading in 02/01/2007 .res endian independent format
    
    
    ierr = 0
       
    IF ((IC_RES_VERSION .EQ. 5).OR.(IC_RES_VERSION .EQ. 7)) THEN
       !
       ! read common .res file v5 or v7 with PART_READ=2 (read all the file, similar to PART_READ=0)
       !
       CALL read_solution_aux_2common( scl, &
            TRIM( filename( 1:INDEX(filename(1:LEN_TRIM(filename)-4),'.',BACK=.TRUE.)-1 )//'.com.res' ), &
            .FALSE., len_uvars, ierr, verb_level_local, EXACT=do_exact, PART_READ=2, FILENUMTOREAD=fntr, &
            IGNOREINIT=do_ignoreinit, CLEAN_IC_DD=do_clean_ic_dd, ZERO_UPDATE=do_zero_update )
    ELSE
       !
       ! read .res file v4 or v6 with PART_READ=2 to mimics read_solution_aux_2common behaviour
       !
       CALL read_solution_aux_1( scl, &
            filename, &
            .FALSE., len_uvars, ierr, verb_level_local, EXACT=do_exact, PART_READ=2, FILENUMTOREAD=fntr, &
            IGNOREINIT=do_ignoreinit, CLEAN_IC_DD=do_clean_ic_dd, ZERO_UPDATE=do_zero_update )
    END IF
    IF (ierr.NE.0 ) THEN
       !
       ! warn users and 
       ! try reading old 01/31/2007 .res format
       CALL serious_warning_message
       CALL read_solution_aux_0( scl, .FALSE. , filename, do_read_all_vars, len_uvars ) ! the 2nd variable - dummy
       IF (PRESENT(NUM_VARS)) NUM_VARS = len_uvars
       RETURN
    END IF

    
    !PRINT *, '3: u_variable_names of size',SIZE(u_variable_names), u_variable_names;STOP 5
    
    IF (do_postprocess_preinitdb .OR. do_read_commonfile)  RETURN
    
    
    
    
    !-----------------------------------------
    ! CALL parallel_set_files_to_read(...) :
    ! read_solution_aux_2common(...)  does not CALL parallel_set_files_to_read(...)
    ! since par_proc_tree() is not allocated  before  CALL read_solution_aux_2common(...)
    ! par_proc_tree() is now allocated after first  CALL parallel_domain_decompose(...)  via  CALL pre_init_DB.

    
    ! define which files to read
    IF (.NOT.ASSOCIATED(fntr)) THEN
       ALLOCATE( fntr(IC_par_size), STAT=alloc_stat )
       CALL test_alloc( alloc_stat, 'fntr in read_solution', IC_par_size )
       ! set fntr - file names to read
       CALL parallel_set_files_to_read (fntr, IC_mxyz_global, IC_prd_global)
       
!!$       ! clean allocated
!!$       IF (do_clean_ic_dd.AND.ALLOCATED(IC_par_proc_tree)) DEALLOCATE (IC_par_proc_tree)
!!$       IF (do_clean_ic_dd.AND.ALLOCATED(IC_Nwlt_per_Tree)) DEALLOCATE (IC_Nwlt_per_Tree)
    END IF
    
!!$    ! prepare for readings from multiple stations
!!$    IF(ALLOCATED(xx) ) DEALLOCATE(xx)
!!$    IF(ALLOCATED(u) ) DEALLOCATE(u)
    !-----------------------------------------

    
    
    
!!$    IF (verb_level_local.GE.2) PRINT *, IC_RES_VERSION, do_postprocess_preinitdb, IC_par_size, IC_restart_mode
    
    IF (verb_level_local.GE.1) THEN
       fntr_read_string = ""
       DO IC_par_rank = 0,IC_par_size-1
          !!WRITE (*, ADVANCE='NO', '("par_rank:", I6, "   Reading Solution file(s): ",A,"   from  IC_par_rank=")') par_rank, TRIM( filename( 1:INDEX(filename(1:LEN_TRIM(filename)-4),'.',BACK=.TRUE.)-1 )
          IF (fntr(IC_par_rank+1)) THEN
             WRITE(IC_par_rank_read_string,'(I6)') IC_par_rank
             fntr_read_string = TRIM(ADJUSTL(fntr_read_string))//IC_par_rank_read_string
             !!WRITE (*, ADVANCE='NO', '(I6)') IC_par_rank
          END IF
       END DO
       WRITE (*, '("par_rank:", I6, "   Reading Solution file(s): ",A,"   from  IC_par_rank=",A)') par_rank, TRIM( filename( 1:INDEX(filename(1:LEN_TRIM(filename)-4),'.',BACK=.TRUE.)-1 )), fntr_read_string
       !!WRITE (*, ADVANCE='YES', '(" ")')   
    ELSE
       IF (par_rank.EQ.0)   WRITE (*, '("Reading Solution file(s):        ",A,".p*.res   by ",I6," processes")') TRIM( filename( 1:INDEX(filename(1:LEN_TRIM(filename)-4),'.',BACK=.TRUE.)-1 )), par_size
    END IF

    
    
    !-------------- parallel reading --------------
    ! compute which files to read,
    ! rearrange names and
    ! read files in cycle

    ! initialize addible variables
    nwlt = 0

    ! insert .p0 into a single processor file name (will be removed in do_append mode later)
    IF (do_append) THEN
       IC_filename = ADJUSTL(IC_filename)
       IF (IC_par_size.EQ.1) IC_filename = IC_filename(1:LEN_TRIM(IC_filename)-4)//'.p0.res'
    END IF


    DO IC_par_rank = 0,IC_par_size-1
       curparrank = IC_par_rank
       IF (fntr(IC_par_rank+1)) THEN
          IF (do_append) THEN ! <<---------------------------------- multiple files mode
             ! set the processor string for every restart file
             IC_par_rank_str = ''       
             IF (IC_par_size.GT.1) THEN
                WRITE( tmp_str,'(I'//CHAR(ICHAR('0')+par_LENSTR1)//')', ERR=1 ) IC_par_rank
                IC_par_rank_str = '.p'//TRIM(ADJUSTL(tmp_str))
             END IF
             ! replace the processor string in the restart filename before .res extension
             ! (the original one could be anything between 0 and IC_par_size-1)
             IF (verb_level_local.GE.2) WRITE (*,'("reading at ",I5,": ", A)') par_rank, &
                  TRIM( &
                  IC_filename( 1:INDEX(IC_filename(1:LEN_TRIM(IC_filename)-4),'.',BACK=.TRUE.)-1 )//TRIM(IC_par_rank_str)//'.res' &
                  )
             
             IF (( IC_RES_VERSION .EQ. 5 ).OR.( IC_RES_VERSION .EQ. 7 )) THEN
             !!!!IF ( IC_COMMONFILE ) THEN
                CALL read_solution_aux_2( scl, &
                  TRIM( IC_filename( 1:INDEX(IC_filename(1:LEN_TRIM(IC_filename)-4),'.',BACK=.TRUE.)-1 )//TRIM(IC_par_rank_str)//'.res' ), &
                  do_read_all_vars, len_uvars, ierr, verb_level_local, EXACT=do_exact, APPEND=do_append, &
                  UTMP=utmp, UOLD=uold, OLEN=olen, IGNOREINIT=do_ignoreinit, ZERO_UPDATE=do_zero_update )
             ELSE
                CALL read_solution_aux_1( scl, &
                  TRIM( IC_filename( 1:INDEX(IC_filename(1:LEN_TRIM(IC_filename)-4),'.',BACK=.TRUE.)-1 )//TRIM(IC_par_rank_str)//'.res' ), &
                  do_read_all_vars, len_uvars, ierr, verb_level_local, EXACT=do_exact, APPEND=do_append, &
                  UTMP=utmp, UOLD=uold, OLEN=olen, IGNOREINIT=do_ignoreinit, ZERO_UPDATE=do_zero_update )
             END IF
             
          ELSE                ! <<---------------------------------- single file mode
             IF (verb_level_local.GE.2) WRITE (*,'("reading at ",I5,": ", A)') par_rank, TRIM(filename)
             IF (( IC_RES_VERSION .EQ. 5 ).OR.( IC_RES_VERSION .EQ. 7 )) THEN
             !!!!IF ( IC_COMMONFILE ) THEN
                CALL read_solution_aux_2( scl, &
                  filename, &
                  do_read_all_vars, len_uvars, ierr, verb_level_local, EXACT=do_exact, APPEND=do_append, &
                  UTMP=utmp, UOLD=uold, OLEN=olen, IGNOREINIT=do_ignoreinit, ZERO_UPDATE=do_zero_update )
             ELSE
                CALL read_solution_aux_1( scl, &
                  filename, &
                  do_read_all_vars, len_uvars, ierr, verb_level_local, EXACT=do_exact, APPEND=do_append, &
                  UTMP=utmp, UOLD=uold, OLEN=olen, IGNOREINIT=do_ignoreinit, ZERO_UPDATE=do_zero_update )
             END IF
             EXIT
          END IF
          IF (ierr.NE.0 ) CALL error ('Error while reading solution file')
       END IF
    END DO
    
    
    ! display the total number of variables inside .res file
    IF (PRESENT(NUM_VARS)) NUM_VARS = len_uvars
    
    
#ifdef VIS_IO3D        
    ! For res2vis, in case of  ADD_DOMAIN, we need IC_par_proc_tree(:) and it should not be DEALLOCATED  here.
    ! SUBROUTINE read_solution_free(...), which is called at end of the loop through data files, will DEALLOCATE both IC_par_proc_tree(:) & IC_Nwlt_per_Tree(:).
!!$PRINT *,' #ifdef VIS_IO3D'
#else
    IF (ALLOCATED(IC_par_proc_tree)) DEALLOCATE (IC_par_proc_tree)
    IF (ALLOCATED(IC_Nwlt_per_Tree)) DEALLOCATE (IC_Nwlt_per_Tree)
#endif
    IF (ASSOCIATED(utmp)) DEALLOCATE (utmp)
    IF (ASSOCIATED(uold)) DEALLOCATE (uold)
    IF (ASSOCIATED(olen)) DEALLOCATE (olen)
    IF (ASSOCIATED(fntr)) DEALLOCATE (fntr)
    RETURN
    ! error in IC_par_rank conversion
1   CALL error ('Error in IC_par_rank conversion: set an appropriate value for par_LENSTR1 in parallel.f90 and recompile')

  END SUBROUTINE read_solution


  !--------------------------------------------------------------------------------------------
  ! read .res file in a format as of 02/01/2007 .res endian independent
  !
  ! This is the only recommended format to use.
  ! Make sure that any RES_WRITE in save_solution_aux_1()
  ! has a correspondent RES_READ in read_solution_aux_1()
  !
  !!! ========= IMPORTANT FOR CODE DEVELOPERS ==================== 
  ! NOTE, then this subroutine can be only called if and only if 
  ! IC_restart .OR. (IC_from_file .AND. IC_file_fmt == 0),
  ! the subroutine is called from main with do_restart = IC_restart,
  ! thus, if IC_restart=.FALSE., that means that it is initial condition from 
  ! file in native format
  !
  ! EXACT (false by default) is introduced to allow use of read_solution from
  ! a stand-alone application to read exactly what is inside .res file,
  ! without making assumptions if it is a restart or initial condition.
  ! In a stand alone application we don't care, we need exact data, set EXACT=T.
  !
  ! PART_READ:  - 0 - default; read all the file
  !             - 1 -          read dimension and exit
  !             - 4 -          read domain decomposition and exit
  !             - 2 -          mimic read_solution_aux_2common behaviour
  !
  ! IGNOREINIT (false by default) - if true, ignore initial data, e.g. from .inp
  !                                 file, and do not check for errors
  !===============================================================
  SUBROUTINE read_solution_aux_1( scl, filename, do_read_all_vars, len_uvars, ierr, verb, &
       EXACT, PART_READ, FILENUMTOREAD, APPEND, UTMP, UOLD, OLEN, IGNOREINIT, CLEAN_IC_DD, ZERO_UPDATE )
    USE PRECISION
    USE pde                ! it, iwrite, dt
    USE variable_mapping   ! n_var
    USE sizes              ! nwlt
    USE field              ! u
    USE wlt_vars           ! dim, j_lev, j_mx
    USE wlt_trns_mod
    USE wlt_trns_vars      ! TYPE_DB, indx_DB, xx
    USE io_3D_vars         ! IC_restart_mode
    USE debug_vars         ! test_alloc, timers, error
    USE parallel           ! par_size
    USE unused
    USE error_handling
    IMPLICIT NONE
    CHARACTER (LEN=*) , INTENT(IN) ::  filename
    REAL (pr), DIMENSION(:), POINTER :: scl, scl_rst_file
    LOGICAL, INTENT(IN) :: do_read_all_vars               ! if true read all vars
    INTEGER, INTENT(INOUT) :: len_uvars
    INTEGER, INTENT(OUT) :: ierr                          ! number of errors
    INTEGER, INTENT(IN) :: verb                           ! verbouse reading (0,1,2)
    LOGICAL, INTENT(IN), OPTIONAL :: EXACT                ! read without assumptions
    LOGICAL, INTENT(IN), OPTIONAL :: IGNOREINIT           ! check for .inp errors
    INTEGER, INTENT(IN), OPTIONAL :: PART_READ            ! read the beginning of the file only
    LOGICAL, POINTER, OPTIONAL :: FILENUMTOREAD(:)        ! which .res files to read
    LOGICAL, OPTIONAL :: APPEND                           ! append data from multiple .res files
    REAL (pr), POINTER, OPTIONAL :: UTMP(:,:), UOLD(:,:)  ! buffers for u array rearranging
    INTEGER, POINTER, OPTIONAL :: OLEN(:,:,:,:)           ! old length buffer for index_DB
    LOGICAL, INTENT(IN), OPTIONAL :: CLEAN_IC_DD          ! deallocate IC_par_proc_tree() and IC_Nwlt_per_Tree()
    LOGICAL, INTENT(IN), OPTIONAL :: ZERO_UPDATE          ! set n_update to zero for multiproc run
    INTEGER :: do_part_read
    INTEGER*4 :: ierr_4, num_4, version_len_4
    CHARACTER (LEN=2) :: it_2, rt_2                               ! default types for RES_READ
    CHARACTER (LEN=VERSION_STRING_LENGTH) :: version_string       ! I/O version in .res file

    INTEGER*8 :: i_INTEGER8                                          !AR 2/22/2011! added by AR
    INTEGER*4, ALLOCATABLE :: ixyz_INTEGER4(:)             !AR 2/27/2011! added by AR
    INTEGER*8, ALLOCATABLE :: ixyz_INTEGER8(:)             !AR 2/27/2011! added by AR

    INTEGER   :: n_wlt_fmly_tmp, j_mn_read, j_mx_read,    &
         n_var_rst_file, n_integrated_tmp,  &
         j, j_df, wlt_type, face_type, card_n_var_save,   &
         save_vort, idim, ie, ie2, u_slot, num_found, i, &                                                       !AR 2/22/2011! added by AR
         iwrite_read, &
         nwlt_tmp, len_tmp, len_old                               ! current .res file nwlt
    !                                                             ! current u -> u_ex
    !                                                             ! current indx_DB -> indx_DB_tmp
    TYPE (indx_type_DB), POINTER :: indx_DB_ptr                   ! pointer to indx_DB element
    REAL (pr) :: dt_tmp
    REAL (pr), DIMENSION(:), ALLOCATABLE   :: u_dummy
    LOGICAL :: done, found, ie_found(1:n_var), do_exact, &
         do_append, do_ignoreinit, do_clean_ic_dd, &
         do_zero_update, &
         isfirst                                         ! true if reading the very first .res file
    INTEGER :: alloc_stat                                ! allocation status
    INTEGER, POINTER :: mxyz_tmp(:), prd_tmp(:)
!#ifdef INTEGER8_DEBUG_AR
    INTEGER :: counter_tmp                               ! loop counter for 'read indx_DB'
!#endif
    
   INTEGER, DIMENSION(:), ALLOCATABLE :: n_var_wlt_fmly_local ! wavelet transform family for each variable

    NULLIFY(scl_rst_file)
    NULLIFY(indx_DB_ptr)
    NULLIFY(mxyz_tmp, prd_tmp)

    ! set default values:
    do_part_read = 0       ! read all the file
    do_exact = .FALSE.     ! make assumptions as if it is a restart
    do_append = .TRUE.     ! append data from multiple .res files;
    do_ignoreinit = .FALSE.! check for errors and compare with .inp data
    do_clean_ic_dd = .TRUE.! deallocate IC_par_proc_tree() and IC_Nwlt_per_tree while reading .res files
    do_zero_update = .TRUE.! normally, set n_update to zero (n2m does not need that)
    IF (PRESENT(PART_READ)) do_part_read = PART_READ
    IF (PRESENT(EXACT)) do_exact = EXACT
    IF (PRESENT(APPEND)) do_append = APPEND
    IF (PRESENT(IGNOREINIT)) do_ignoreinit = IGNOREINIT
    IF (PRESENT(CLEAN_IC_DD)) do_clean_ic_dd = CLEAN_IC_DD
    IF (PRESENT(ZERO_UPDATE)) do_zero_update = ZERO_UPDATE
    

    
    ! open .res file for reading
    ! check tree-c/io.h for details
    CALL RES_OPEN( 'r', TRIM(filename), LEN(TRIM(filename)), ierr_4 )
    IF (ierr_4.NE.0_4) THEN
       WRITE (*,'("Cannot open the file: ",A)') TRIM(filename)
       ierr = 1
       RETURN
    END IF
    ierr = 0

    ! set integer and real types for RES_READ
    CALL set_default_types( it_2, rt_2 )
    
    ! each RES_READ here should have a correspondent
    ! RES_WRITE in save_solution_aux_1
    CALL RES_READ( 'i4', version_len_4, 1_4 )                   ! version string size
    CALL RES_READ( 'i1', version_string, version_len_4 )        ! version string
    
    ! if required, include version control here
    IF( verb.GE.1 ) WRITE (*,'(A)') version_string(1:version_len_4)
    
    ! read dimension; for restart it should coincide with .inp
    CALL RES_READ( it_2, i, 1_4 )
    IF (.NOT.(do_exact.OR.do_ignoreinit) ) THEN
       IF (i.NE.dim) CALL error ('unsupported: different dimension in .res and .inp files')
    END IF
    dim = i
    
    ! partial read - read dimension ------------------------------------------------------------------------
    IF (do_part_read.EQ.1) THEN
       CALL close_res_file( filename )
       RETURN
    END IF

    ! mxyz and prd from .res file
    IF (.NOT.ASSOCIATED(mxyz_tmp)) THEN
       ALLOCATE(mxyz_tmp(dim), STAT=alloc_stat )
       CALL test_alloc( alloc_stat, 'mxyz_tmp in read_solution_aux_1', dim )
    END IF
    IF (.NOT.ASSOCIATED(prd_tmp)) THEN
       ALLOCATE(prd_tmp(dim), STAT=alloc_stat )
       CALL test_alloc( alloc_stat, 'prd_tmp in read_solution_aux_1', dim )
    END IF
    
    ! read number of processors
    IF (version_string(1:version_len_4) .EQ. 'VERSION=1 @') THEN
       IF (IC_par_size.EQ.0) THEN
          ! If the value of IC_par_size (.inp file parameter 'IC_processors') is zero, by default,
          ! that parameter has not been provided in .inp file. We set it to the current number of processors.
          IF (par_rank.EQ.0) THEN
             WRITE (*,'(" ")')
             WRITE (*,'(" WARNING: The file does not contain the total number of processors it was created by.")')
             WRITE (*,'("          And IC_processors has not been defined in your input parameter file.")')
             WRITE (*,'("          Most probably, that file of yours is an old one and therefore")')
             WRITE (*,'("          it might be safe to assume that IC_processors=1.")')
             WRITE (*,'("          If this is incorrect, set input parameter IC_processors")')
             WRITE (*,'("          to the correct number of processors and restart.")')
             CALL error (IW=ignore_warnings)
          END IF
          IC_par_size = 1
       END IF
       
    ELSE IF (version_string(1:version_len_4) .EQ. 'VERSION=2 @' .OR. & 
             version_string(1:version_len_4) .EQ. 'VERSION=3 @' .OR. &
             version_string(1:version_len_4) .EQ. 'VERSION=4 @' .OR. &
             version_string(1:version_len_4) .EQ. 'VERSION=6 @' ) THEN
       ! file has been written after the addition of NtoM transform to the main code
       ! (for old files of 'VERSION=1 @' use the standalone NtoMonP tool)
       CALL RES_READ( it_2, IC_par_size, 1_4 )                          ! par_size of .res file
       CALL RES_READ( 'i4', num_4, 1_4 )                                ! size of par_proc_tree(:)
       
!!$       IF (ALLOCATED(IC_par_proc_tree)) THEN
!!$          IF (SIZE(IC_par_proc_tree).LT.num_4) DEALLOCATE (IC_par_proc_tree)
!!$       END IF
!!$       IF (.NOT.ALLOCATED(IC_par_proc_tree)) THEN
       IF (ALLOCATED(IC_par_proc_tree)) DEALLOCATE (IC_par_proc_tree)
       ALLOCATE( IC_par_proc_tree(num_4), STAT=alloc_stat )
       CALL test_alloc( alloc_stat, 'IC_par_proc_tree in read_solution_aux_1', num_4 )
!!$       END IF
       
       CALL RES_READ( it_2, IC_par_proc_tree, num_4 )                   ! ...
       !OLEG: 07.14.2011 BEGIN CHANGES for beter load balancing of restart files
       IF (ALLOCATED(IC_Nwlt_per_Tree)) THEN
          IF (SIZE(IC_Nwlt_per_Tree).LT.num_4) DEALLOCATE (IC_Nwlt_per_Tree)
       END IF
       IF (.NOT.ALLOCATED(IC_Nwlt_per_Tree)) THEN
          ALLOCATE( IC_Nwlt_per_Tree(num_4), STAT=alloc_stat )
          CALL test_alloc( alloc_stat, 'IC_Nwlt_per_Tree in read_solution_aux_1', num_4 )
       END IF
       IF (version_string(1:version_len_4) .EQ. 'VERSION=4 @' .OR. &
           version_string(1:version_len_4) .EQ. 'VERSION=6 @' ) THEN
          CALL RES_READ( it_2, IC_Nwlt_per_Tree, num_4 )                   ! ...
       END IF
       !OLEG: 07.14.2011 END CHANGES for beter load balancing of restart files
       num_4 = dim
       CALL RES_READ( it_2, mxyz_tmp, num_4 )               ! IC_mxyz - may be different in .res and .inp
       CALL RES_READ( it_2, prd_tmp, num_4 )                ! IC_prd  - ...
    ELSE
       WRITE (*,'("unsupported version: ",A)') TRIM(version_string)
       CALL error
    END IF

    
    IF (.NOT.ASSOCIATED(IC_mxyz_global)) ALLOCATE( IC_mxyz_global(1:dim), STAT=alloc_stat )
    IF (.NOT.ASSOCIATED(IC_prd_global))  ALLOCATE( IC_prd_global(1:dim),  STAT=alloc_stat )    
    IC_mxyz_global(1:dim) =  mxyz_tmp(1:dim)
    IC_prd_global(1:dim)  =  prd_tmp(1:dim)
    
    
    ! partial read - read domain decomposition -------------------------------------------------------------
    IF (do_part_read.EQ.4) THEN
       CALL close_res_file( filename )
       RETURN
    END IF
    
    IF ( (verb.GE.1.AND.par_rank.EQ.0).OR.(verb.GE.2) ) WRITE (*,'("#",I6," processor(s)")') IC_par_size
    
    
!!$    ! partial read - initial read of parallel restart
!!$    IF (do_part_read.EQ.2) THEN
!!$       !
!!$       ! moved inside read_solution ()
!!$       !
!!$       ! define which files to read
!!$       IF (.NOT.ASSOCIATED(FILENUMTOREAD)) THEN
!!$          ALLOCATE( FILENUMTOREAD(IC_par_size), STAT=alloc_stat )
!!$          CALL test_alloc( alloc_stat, 'fntr in read_solution_aux_1', IC_par_size )
!!$          ! set FILENUMTOREAD - which files to read
!!$          CALL parallel_set_files_to_read (FILENUMTOREAD, mxyz_tmp, prd_tmp)
!!$
!!$          ! clean allocated
!!$          IF (do_clean_ic_dd.AND.ALLOCATED(IC_par_proc_tree)) DEALLOCATE (IC_par_proc_tree)
!!$          IF (do_clean_ic_dd.AND.ALLOCATED(IC_Nwlt_per_Tree)) DEALLOCATE (IC_Nwlt_per_Tree)
!!$       END IF
!!$       ! prepare for readings from multiple stations
!!$       IF(ALLOCATED(xx) ) DEALLOCATE(xx)
!!$       IF(ALLOCATED(u) ) DEALLOCATE(u)
!!$       CALL close_res_file( filename )
!!$       RETURN
!!$    END IF
    
    
    CALL RES_READ( it_2, n_wlt_fmly_tmp, 1_4 )

    !
    ! --- WARNING ---
    ! n_wlt_fmly is currently a parameter in wavelet_3d_vars.f90
    ! as soon as it will be changed some IF(do_exact) to be introduced
    ! here, maybe with n_updt(:) and other arrays re-allocation
    !
    IF (do_exact.AND.(n_wlt_fmly_tmp.NE.n_wlt_fmly)) THEN
       CALL error ('unsupported - exact read requested: n_wlt_fmly_tmp != n_wlt_fmly')
    END IF
    
    
    IF(n_wlt_fmly_tmp > n_wlt_fmly) THEN
       CALL error ('ERROR: n_wlt_fmly in .res file > n_wlt_fmly')
    END IF
    
    
    ! --------------------------------------------------------
    ! WARNING
    ! as long as our arrays are (1:3) instead of (1:dim)
    ! the extra dimensions have to be initialized !
    IF (SIZE(nxyz).EQ.3) THEN
       DO i=dim+1, 3
          nxyz(i) = 1
          mxyz(i) = 1
          prd(i) = 1
          grid(i) = 1
       END DO
    END IF
    ! --------------------------------------------------------

    num_4 = n_wlt_fmly_tmp + 1                       ! the arrays are (0:n_wlt_fmly_tmp)
    CALL RES_READ( it_2, n_prdct_resfile, num_4 )
    CALL RES_READ( it_2, n_updt_resfile, num_4 )
    IF ( par_size.GT.1 .AND. ANY( n_updt_resfile(0:n_wlt_fmly).NE.0) ) &
         CALL error ( &
         '  Attempt to restart from .res file with N_update>0 on several processors.', &
         '  Please use single processor for nonzero N_update or N_update_low_order. ', &
         '  Otherwise, set all N_update to zero in your input parameter file, then  ', &
         '  restart on a single processor (it will read your .res file and save it).', &
         '  The saved file has N_update=0 and can be used for a multiprocessor run. ', &
         '                                                                          ', &
         '       The reason is that the code automatically remaps wavelets          ', &
         '       if N_update or N_predict changes on single processor only.         ', &
         '       In multi-processor runs wavelet transform always uses zero         ', &
         '       N_update values and therefore the code is not able to remap        ', &
         '       wavelets correctly if N_update changes on several processors.      ', &
         '       N_predict modifications will be remaped correctly in any case.     ' )
    
!!$    IF (do_part_read.EQ.0) THEN                      ! Only if the whole .res file is read:
       IF (do_exact.OR.do_ignoreinit) THEN
          n_prdct = n_prdct_resfile                            !   Set equal to prevent
          n_updt = n_updt_resfile                              !   renormalization in init_DB.
       END IF
       IF (do_zero_update) CALL set_zero_n_updt_for_multiproc  !   N update -> 0 for a multiprocessor run.
       
       maxval_n_prdct = maxval(n_prdct)
       maxval_n_updt = maxval(n_updt)
!!$    END IF
    

    
    CALL RES_READ( it_2, n_assym_prdct_resfile, num_4 )      ! the arrays are (0:n_wlt_fmly_tmp,0) ( (0,0), (1,0), (2,0), ... )
    CALL RES_READ( it_2, n_assym_updt_resfile, num_4 )       ! ...
    CALL RES_READ( it_2, n_assym_prdct_bnd_resfile, num_4 )  ! ...
    CALL RES_READ( it_2, n_assym_updt_bnd_resfile, num_4 )   ! ...
    DO i = 0, n_trnsf_type !0 - regular, 1 - inernal
       n_assym_prdct(:,i)     = n_assym_prdct_resfile(:,0)
       n_assym_prdct_bnd(:,i) = n_assym_prdct_bnd_resfile(:,0)
       n_assym_updt(:,i)      = n_assym_updt_resfile(:,0)
       n_assym_updt_bnd(:,i)  = n_assym_updt_bnd_resfile(:,0)
    END DO 

    CALL RES_READ( it_2, card_n_var_save, 1_4 )
    CALL RES_READ( it_2, save_vort, 1_4 )

    num_4 = dim
    CALL RES_READ( it_2, nxyz, num_4 )               ! the arrays are (1:dim)
    
    IF (version_string(1:version_len_4) .EQ. 'VERSION=1 @') THEN ! it is moved to domain decomposition section
       CALL RES_READ( it_2, mxyz_tmp, num_4 )                    ! for higher versions
       CALL RES_READ( it_2, prd_tmp, num_4 )                         ! ...
    END IF
    
    ! assign periodicity
    IF ( .NOT.(do_exact.OR.do_ignoreinit) ) THEN
       IF ( ANY(prd(1:dim).NE.prd_tmp(1:dim)) ) &
            CALL error ('unsupported: different periodicity in .res and .inp files')
    END IF
    prd(1:dim) = prd_tmp(1:dim)

    CALL RES_READ( it_2, grid, num_4 )               ! ...

    CALL RES_READ( it_2, j_mn_read, 1_4 )
    CALL RES_READ( it_2, j_mx_read, 1_4 )
    CALL RES_READ( it_2, j_lev, 1_4 )

    CALL RES_READ( it_2, nwlt_tmp, 1_4 )             ! number of wavelets in that particular file
    nwlt = nwlt + nwlt_tmp

    
    CALL RES_READ( it_2, it, 1_4 )
    
    
    CALL RES_READ( it_2, iwrite_read, 1_4 )
    SELECT CASE (IC_restart_mode)
    CASE (0)                          ! new run
       iwrite = 0
    CASE (1)                          ! hard restart
       iwrite = iwrite_read + 1
    CASE (2:3)                        ! soft restart and restart from IC
       iwrite = IC_restart_station
    END SELECT
    
    
    CALL RES_READ( it_2, n_var_rst_file, 1_4 )
    CALL RES_READ( it_2, n_integrated_tmp, 1_4 )
    CALL RES_READ( it_2, n_time_levels, 1_4 ) !No longer in use, left for backward compatibility 

    
!!$    ! partial read - read all but u and indx_DB
!!$    IF (do_part_read.EQ.3) THEN
!!$       mxyz(1:dim) = mxyz_tmp(1:dim)
!!$       j_mn = j_mn_read
!!$       j_mx = j_mx_read
!!$       CALL close_res_file( filename )
!!$       RETURN
!!$    END IF

    
    IF ( (verb.GE.1.AND.par_rank.EQ.0).OR.(verb.GE.2) ) THEN
       WRITE (*,'("# read_solution: proc #",I5)') par_rank
       IF (verb.GE.2) THEN
          PRINT *, 'dim=', dim
          PRINT *, 'IC_par_size=',IC_par_size
          PRINT *, 'n_wlt_fmly=', n_wlt_fmly_tmp
          PRINT *, 'n_prdct(:)            =', n_prdct(0:n_wlt_fmly_tmp)
          PRINT *, 'n_updt(:)             =', n_updt(0:n_wlt_fmly_tmp)
          PRINT *, 'n_assym_prdct(:,0)    =', n_assym_prdct(0:n_wlt_fmly_tmp,0)
          PRINT *, 'n_assym_updt(:,0)     =', n_assym_updt(0:n_wlt_fmly_tmp,0)
          PRINT *, 'n_assym_prdct_bnd(:,0)=', n_assym_prdct_bnd(0:n_wlt_fmly_tmp,0)
          PRINT *, 'n_assym_updt_bnd(:,0) =', n_assym_updt_bnd(0:n_wlt_fmly_tmp,0)
          PRINT *, 'card_n_var_save=', card_n_var_save
          PRINT *, 'save_vort=', save_vort
       END IF
       PRINT *,'nxyz =',nxyz(1:dim)
       PRINT *,'mxyz =',mxyz_tmp(1:dim)
       PRINT *,'prd  =', prd(1:dim)
       PRINT *,'grid =',grid(1:dim)
       PRINT *,'j_mn =',j_mn_read
       PRINT *,'j_mx =',j_mx_read
       PRINT *,'j_lev=',j_lev
       PRINT *,'nwlt =',nwlt_tmp, nwlt
       IF (verb.GE.2) THEN
          PRINT *,'it',it
          PRINT *,'iwrite (in .res file) = ', iwrite_read, ', iwrite = ', iwrite
          PRINT *,'n_var_rst_file',n_var_rst_file
          PRINT *,'n_integrated_tmp',n_integrated_tmp
       END IF
    END IF


!!$    IF (do_exact) THEN ! ERIC: will be set later when vars registered
!!$      !CALL error ( 'Cannot do_exact and directly set n_integrated (nothing else being set)' )
!!$      !CALL set_n_integrated ( n_integrated_tmp )  ! Instead, the first n_integrated_tmp variables will be set as integrated !ERIC: but names are only populated with read_all_vars
!!$    END IF
    
    
    ! this is a call from code1/nmp/exact
    IF( do_read_all_vars .OR. do_exact ) THEN
       len_uvars = n_var_rst_file
       CALL reset_mapping()                    ! clear registry and mappings !ERIC: will this mess up the allocation of uTMP? 
    END IF

    
    ! this is pure restart
    IF ( (IC_restart_mode.EQ.1) .OR. (IC_restart_mode.EQ.2) ) THEN
       IF ( ANY(mxyz_tmp(1:dim).NE.mxyz(1:dim)) ) THEN
          PRINT *, 'Mxyz from input file  :' ,mxyz
          PRINT *, 'Mxyz from restart file:' ,mxyz_tmp
          CALL error ( &
               'Not Supported: Pure restart with different Mxyz.', &
               'Instead, use restart with initial conditions: set (IC_restart_mode=3)', &
               'and increase maximum level of resolution J_MX.')
       END IF
       ! Check that if we are doing a restart, j_mn(from input file) = j_mn_read (from restart file)
       ! Actually this is more restrictive than is necessary. It would work to have 
       ! j_mn(from input file) <= j_mn_read but to support this we need to change init_lines_db().
       !   
       IF ( j_mn /= j_mn_read .OR. j_mx /= j_mx_read ) THEN
          PRINT *, 'j_mn, j_mx from input file:' ,j_mn, j_mx
          PRINT *, 'j_mn, j_mx from restart file:' ,j_mn_read, j_mx_read
          CALL error ( &
               'Not Supported: Pure restart with j_mn or j_mx different from restart file.', &
               'Instead, use restart with initial conditions: set (IC_restart_mode=3)', &
               'in you input parameter file.')
       END IF
    END IF
    
    
    ! ---------------------------------------------------------------------------------------
    ! For post processing we are doing a restart but we do want to take the value of j_mx and j_mn 
    !  from the input (IC_restart to be set to .FALSE. for postprocessing)
    !
    !IF( (j_mx == 0).OR.do_exact ) THEN       ! j_mx = 0 in main before reading .inp file
    IF (do_exact.OR.do_ignoreinit) THEN
       j_mx = j_mx_read
       j_mn = j_mn_read
    END IF
    !
    ! it is restart as IC
    IF ( IC_restart_mode.EQ.3 .AND. j_mx < j_lev) THEN
       WRITE(*,'("j_mx=",I2,", j_lev=",I2)') j_mx, j_lev
       CALL error ( &
            'not supported: j_mx (from .inp) < j_lev (from .res)' )
    END IF
    !
    ! In non-restart cases that the IC file is read from a restart file
    ! we use j_mx and j_mn from the input file
    ! ---------------------------------------------------------------------------------------

    mxyz(1:dim) =  mxyz_tmp(1:dim)
    !DG CHANGE TO RESTART WITH DIFFERENT Mxyz:     !if we are doing restart we want to use the Mxyz values from the inp file
    !DG CHANGE TO RESTART WITH DIFFERENT Mxyz:     IF(.NOT. IC_restart ) mxyz(1:3) = mxyz_tmp(1:3)

    CALL RES_READ( rt_2, t, 1_4 )
    CALL RES_READ( rt_2, dt_tmp, 1_4 )

    !
    ! Use dt from input file if we are doing a restart
    !
    IF ( IC_restart_mode.EQ.1 .OR. &          !
         IC_restart_mode.EQ.2 .OR. &          !
         do_exact.OR.do_ignoreinit ) THEN     ! pure restart or postprocessing
       dt = dt_tmp
    ELSE                                                   ! restart as IC
       IF( dt /= dt_tmp) THEN
          IF ( (verb.GE.1.AND.par_rank.EQ.0).OR.(verb.GE.2) ) THEN
             IF (verb.GE.2) THEN
                PRINT *, 't     =',t
                PRINT *, 'dt_tmp=',dt_tmp
             END IF
             PRINT *,''
             PRINT *,'***************************************************'
             PRINT *,'Restarting using dt from input file:',dt
             PRINT *,'Not using dt from restart file:', dt_tmp
             PRINT *,'***************************************************'
             PRINT *,''
          END IF
       END IF
    END IF
    dt = MIN(dt, dt_original)
    
    
    !
    ! res_save_coeff=T - save wavelet coefficients in .res file (given in .inp file)
    ! res_save_coeff_IC - (given in .res file)
    !
    res_save_coeff_IC = .TRUE.  ! all previous versions have coefficients in .res file
    num_4 = 1
    IF (version_string(1:version_len_4) .EQ. 'VERSION=6 @') &
         CALL RES_READ( it_2, num_4, 1_4 )
    IF ( num_4.EQ.0 ) res_save_coeff_IC = .FALSE.
    
    
    
    ! mimic read_solution_aux_2common behaviour
    ! Make  n_var_rst_file, card_n_var_save, save_vort  global  in order to use them in  SUBROUTINE read_solution_aux_2(...)
    len_uvars__aux_2__global        = len_uvars
    n_var_rst_file__aux_2__global   = n_var_rst_file
    card_n_var_save__aux_2__global  = card_n_var_save
    save_vort__aux_2__global        = save_vort
    n_integrated_tmp__aux_2__global = n_integrated_tmp
    
    IF (do_part_read.EQ.2) THEN
       CALL close_res_file( filename )
       RETURN
    END IF



    ! ------------------------------------------------------
    ! prepare for single file reading
    IF (.NOT.do_append) THEN
       IF( ALLOCATED(xx) ) DEALLOCATE(xx)
       IF( ALLOCATED(u) ) DEALLOCATE(u)
       IF( ALLOCATED(indx_DB) ) CALL deallocate_indx_DB
    END IF
    ! this is for the indx_DB for all the data (from all .res files)
    IF(.NOT.ALLOCATED(indx_DB)) THEN
       ALLOCATE(indx_DB(1:j_mx,0:2**dim-1,0:3**dim-1,1:j_mx), STAT=alloc_stat)
       CALL test_alloc( alloc_stat, 'indx_DB in read_solution_aux_1',j_mx*(2**dim)*(3**dim)*j_mx )
       DO j = 1, j_mx
          DO wlt_type = MIN(j-1,1),2**dim - 1
             DO face_type = 0, 3**dim - 1
                DO j_df = 1, j_mx
                   NULLIFY (indx_DB(j_df,wlt_type,face_type,j)%p)
                END DO
             END DO
          END DO
       END DO
       indx_DB(:,:,:,:)%real_length = 0
       indx_DB(:,:,:,:)%length = 0
!!$       indx_DB(:,:,:,:)%shift = 0
    END IF
    ! ------------------------------------------------------
    
    
    IF ( (IC_restart_mode.EQ.1).OR.(IC_restart_mode.EQ.2) ) THEN
       ! pure restart parameter check
       IF( (n_var > n_var_rst_file).AND.(verb.GE.1) ) THEN
          CALL error ( &
               '*************************************************************************', &
               'Restarting with n_var > n_var in restart file! Caution this might be wrong!', &
               '*************************************************************************', &
               IW=ignore_warnings, F=.TRUE.)
       END IF
       IF( n_var < n_var_rst_file ) &
            CALL error ('Restart with n_var <  n_var in restart file! This is not supported.')
       IF( (n_integrated_tmp /= n_integrated).AND.(verb.GE.1) ) THEN
          CALL error ( &
               '*************************************************************************', &
               'Restart with n_integrated /= n_integrated in restart file! Possible Error !!!', &
               '*************************************************************************', &
               IW=ignore_warnings, F=.TRUE.)
       END IF
       ! u_dummy for the skipped variables
       ALLOCATE( u_dummy(1:nwlt_tmp), STAT=alloc_stat )
       CALL test_alloc( alloc_stat, 'u_dummy in read_solution_aux_1', nwlt_tmp )
       ! allocate u (or increase the size for multiple .res files)
       CALL allocate_u( OLDSIZE=nwlt-nwlt_tmp, NEWSIZE=nwlt, NVAR=n_var, UTMP=UTMP, UOLD=UOLD, ISFIRST=isfirst )
       
    ELSE
       ! restart as IC
       CALL allocate_u( OLDSIZE=nwlt-nwlt_tmp, NEWSIZE=nwlt, NVAR=len_uvars, UTMP=UTMP, UOLD=UOLD, ISFIRST=isfirst )
       IF ( (len_uvars .GT. card_n_var_save).AND.(verb.GE.1).AND.par_rank.EQ.0 ) THEN
          PRINT *,'WARNING: possible error. Number of requested variables (',len_uvars,')'
          PRINT *,'is greater then number of variables saved in this data file (',card_n_var_save,')'
          CALL error (IW=ignore_warnings, F=.TRUE.)
       END IF
    END IF
    
    
    ! read grid coordinates
    IF (.NOT.ALLOCATED(xx)) THEN
       ALLOCATE (xx(0:MAXVAL(nxyz(1:dim)),1:dim), STAT=alloc_stat)
       CALL test_alloc( alloc_stat, 'xx in read_solution_aux_1', (MAXVAL(nxyz(1:dim))+1)*dim )
    END IF
    DO idim = 1,dim
       num_4 = nxyz(idim) + 1                          ! xx(0:nxyz(idim),1:dim)
       CALL RES_READ( rt_2, xx(0,idim), num_4 )
    END DO
    
    ! allocate counter of old lengths (once for all the reads)
    !IF (.NOT.ASSOCIATED(OLEN)) WRITE (*,'(A, I6, A, 4I12)') ' rank', par_rank, ' OLEN is .NOT.ASSOCIATED     SHAPE(OLEN)=', SHAPE(OLEN)
    IF (.NOT.ASSOCIATED(OLEN)) ALLOCATE( OLEN(1:j_lev,0:2**dim-1,0:3**dim - 1,1:j_lev) )

!#ifdef INTEGER8_DEBUG_AR
    counter_tmp = 0
!#endif
    ! read indx_DB and store old real_length in OLEN
    DO j = 1, j_lev
       DO wlt_type = MIN(j-1,1),2**dim-1
          DO face_type = 0, 3**dim - 1
             DO j_df = j, j_lev
                
                indx_DB_ptr => indx_DB(j_df,wlt_type,face_type,j)
                len_old = indx_DB_ptr%real_length;                      ! old size of multiple files data
                OLEN(j_df,wlt_type,face_type,j) = len_old;              ! (store it for future u/indx_DB remapping)
                CALL RES_READ( it_2, len_tmp, 1_4 )                     ! size of the current .res file data
                
                ! allocate indx_DB (or increase the size for multiple .res files)
                CALL allocate_indx_DB ( indx_DB_ptr, OLDLEN=len_old, NEWLEN=len_old+len_tmp ) !, IWLTADD=nwlt-nwlt_tmp )
                
!#ifdef INTEGER8_DEBUG_AR
                counter_tmp = counter_tmp + 1
!#endif
                IF( len_tmp  > 0) THEN
                   num_4 = len_tmp
                   CALL RES_READ( it_2, indx_DB_ptr%p(1+len_old:len_old+len_tmp)%i,    num_4 )
                   IF ( (version_string(1:version_len_4) .EQ. 'VERSION=1 @') .OR. (version_string(1:version_len_4) .EQ. 'VERSION=2 @') ) THEN   !added by SR on 04/06/2011
                      IF( ALLOCATED(ixyz_INTEGER4) ) DEALLOCATE(ixyz_INTEGER4)                             !added by SR on 04/06/2011
                      ALLOCATE( ixyz_INTEGER4(len_tmp), STAT=alloc_stat )                                  !added by SR on 04/06/2011
                      CALL RES_READ( 'i4', ixyz_INTEGER4(1:len_tmp), num_4 )                               !added by SR on 04/06/2011
                      indx_DB_ptr%p(1+len_old:len_old+len_tmp)%ixyz = ixyz_INTEGER4(1:len_tmp)             !added by SR on 04/06/2011
                      IF( ALLOCATED(ixyz_INTEGER4) ) DEALLOCATE(ixyz_INTEGER4)                             !added by SR on 04/06/2011
                   ELSE IF ( (version_string(1:version_len_4) .EQ. 'VERSION=3 @') .OR. &
                             (version_string(1:version_len_4) .EQ. 'VERSION=4 @') .OR. &
                             (version_string(1:version_len_4) .EQ. 'VERSION=6 @') )   THEN                 !added by SR on 04/06/2011
                      IF( ALLOCATED(ixyz_INTEGER8) ) DEALLOCATE(ixyz_INTEGER8)                             !added by SR on 04/06/2011
                      ALLOCATE( ixyz_INTEGER8(len_tmp), STAT=alloc_stat )                                  !added by SR on 04/06/2011
                      CALL RES_READ( 'i8', ixyz_INTEGER8(1:len_tmp), num_4 )                               !added by SR on 04/06/2011
                      indx_DB_ptr%p(1+len_old:len_old+len_tmp)%ixyz = ixyz_INTEGER8(1:len_tmp)             !added by SR on 04/06/2011
                      IF( ALLOCATED(ixyz_INTEGER8) ) DEALLOCATE(ixyz_INTEGER8)                             !added by SR on 04/06/2011
                   ELSE                                                                                    !added by SR on 04/06/2011
                      IF (par_rank.EQ.0) WRITE (*,'("unsupported version: ",A)') TRIM(version_string)      !added by SR on 04/06/2011
                      IF (par_rank.EQ.0) PRINT *, 'Exiting ...'                                            !added by SR on 04/06/2011   
                      CALL parallel_finalize; STOP                                                         !added by SR on 04/06/2011 
                   END IF

                   i_INTEGER8 = indx_DB_ptr%p(1+len_old)%ixyz

!AR 2/24/2011!  The following 6 lines are added by AR
#ifdef INTEGER8_DEBUG_AR
    WRITE (*,'(A, A, A, I, A, I2, A, I2, A, I6, A, I6)')  'RES_READ    it_2=', it_2, '  i_INTEGER8=', i_INTEGER8, '   KIND(i_INTEGER8)=',KIND(i_INTEGER8), '   KIND(indx_DB_ptr%p()%ixyz)=', KIND(indx_DB_ptr%p(len_old+len_tmp)%ixyz), '     num_4=', num_4, '   len_tmp=', len_tmp
#endif

                END IF
                
             END DO
          END DO
       END DO
    END DO
    
!#ifdef INTEGER8_DEBUG_AR
    IF (par_rank .EQ. 0) WRITE (*,'(A, I6, A, I12)')  '  par_rank=', par_rank, '     read indx_DB counter=', counter_tmp
!#endif

    !read the scl array for this time step
    IF( .NOT. ASSOCIATED(scl_rst_file)   ) THEN
       ALLOCATE ( scl_rst_file(1:n_var_rst_file), STAT=alloc_stat)  ! 04/03/2006: Oleg's change
       CALL test_alloc( alloc_stat, 'scl_rst_file in read_solution_aux_1', n_var_rst_file)
    END IF
    IF( .NOT. ASSOCIATED(scl)            ) THEN
       ALLOCATE ( scl(1:n_var), STAT=alloc_stat)  ! 04/03/2006: Oleg's change
       CALL test_alloc( alloc_stat, 'scl in read_solution_aux_1', n_var )
    END IF
    IF( .NOT. ASSOCIATED(scl_global)     ) THEN
       ALLOCATE ( scl_global(1:n_var), STAT=alloc_stat)  ! 04/03/2006: Oleg's change
       CALL test_alloc( alloc_stat, 'scl_global in read_solution_aux_1', n_var )
    END IF
    !IF( .NOT. ALLOCATED(n_var_wlt_fmly)  ) THEN
    !   ALLOCATE ( n_var_wlt_fmly(1:n_var_rst_file), STAT=alloc_stat )  ! 04/03/2006: Oleg's change
    !   CALL test_alloc( alloc_stat, 'n_var_wlt_fmly in read_solution_aux_1', n_var_rst_file )
    !END IF
    IF( .NOT. ALLOCATED(n_var_wlt_fmly_local)  ) THEN
       ALLOCATE ( n_var_wlt_fmly_local(1:n_var_rst_file), STAT=alloc_stat )  ! ERIC: updated to reflect deprecation 
       CALL test_alloc( alloc_stat, 'n_var_wlt_fmly_local in read_solution_aux_1', n_var_rst_file )
    END IF

    scl = 1.0_pr
    num_4 = n_var_rst_file
    CALL RES_READ( rt_2, scl_rst_file, num_4 )                ! scl(1:n_var_rst_file)
    CALL RES_READ( it_2, n_var_wlt_fmly_local, num_4 )     ! n_var_wlt_fmly(1:n_var_rst_file)


    IF( (verb.EQ.1.AND.par_rank.EQ.0).OR.(verb.GE.2) ) WRITE(*,'( "Reading in iteration:", I3 ,",  t = ", E12.5 )') it, t
    IF (verb.GE.2) THEN
       PRINT *, 'scl_rst_file=',scl_rst_file
       PRINT *, 'n_var_wlt_fmly=',n_var_wlt_fmly_local
    END IF
    
    
    ! --- WARNING ---
    ! n_var_wlt_fmly is not defined in save/read_solution


    IF (do_exact.OR.do_ignoreinit) THEN
       scl = scl_rst_file
    END IF
    

    ! -----------------------------------------------------------
    ! ---------------------------- U ----------------------------
    ! -------------------------- begin --------------------------
    IF ( (IC_restart_mode.EQ.1).OR.(IC_restart_mode.EQ.2) ) THEN
       ie_found(:) = .FALSE.
       DO ie = 1, card_n_var_save

          num_4 = LEN(tmp_name_str)
          CALL RES_READ( 'i1', tmp_name_str, num_4 )
          CALL RES_READ( it_2, u_slot, 1_4 )

          found = .FALSE.
          !PRINT *, 'u_variable_names 1:',u_variable_names( : ), tmp_name_str

          DO ie2 = 1,  n_var
             !IF( INDEX(TRIM(ADJUSTL(u_variable_names(ie2))),     TRIM(ADJUSTL(tmp_name_str)))==1 .AND. &
             !      LEN_TRIM(ADJUSTL(u_variable_names(ie2)))==LEN_TRIM(ADJUSTL(tmp_name_str)) ) THEN
             IF( TRIM(ADJUSTL(u_variable_names(ie2))) .EQ.     TRIM(ADJUSTL(tmp_name_str))) THEN
                num_4 = nwlt_tmp
                CALL RES_READ( rt_2, UTMP(1, ie2), num_4 )
                scl(ie2) = scl_rst_file(u_slot)
                ie_found(ie2) = .TRUE.
                found = .TRUE.
                IF ( (verb.GE.1.AND.par_rank.EQ.0).OR.(verb.GE.2) ) PRINT *, 'Reading var  : ', tmp_name_str
                exit ! exit loop
             END IF
          END DO
          IF( .NOT. found ) THEN ! Seach for IC mapped names
             DO ie2 = 1,  n_var
                IF( TRIM(ADJUSTL(u_IC_names(ie2))) .EQ.     TRIM(ADJUSTL(tmp_name_str))) THEN
                   num_4 = nwlt_tmp
                   CALL RES_READ( rt_2, UTMP(1, ie2), num_4 )
                   scl(ie2) = scl_rst_file(u_slot)
                   ie_found(ie2) = .TRUE.
                   found = .TRUE.
                   IF ( (verb.GE.1.AND.par_rank.EQ.0).OR.(verb.GE.2) ) PRINT *, 'Reading var  : ', tmp_name_str,' into ', u_variable_names(ie2)
                   exit ! exit loop
                END IF
             END DO
          END IF
          IF( .NOT. found ) THEN
             num_4 = nwlt_tmp
             CALL RES_READ( rt_2, u_dummy, num_4 )
             IF( (verb.GE.1.AND.par_rank.EQ.0).OR.(verb.GE.2) ) PRINT *, 'Skipping var  : ', tmp_name_str
          END IF
          !PRINT *, 'u_variable_names 2:',u_variable_names( : ), tmp_name_str
       END DO
       scl_global = scl
       DO ie = 1,  n_var
          IF(  n_var_req_restart(ie) .AND. .NOT. ie_found(ie) ) THEN
             PRINT *, 'Error restarting, required restart variable: ', u_variable_names(ie)
             CALL error ('Variable has not been found in restart file.')
          END IF
       END DO
       !PRINT *, 'u_variable_names 3:',u_variable_names( : ), tmp_name_str

    ELSE !not restart
       ! This check is not necessary. It does not take into account additional variables
       ! that were saved
       !IF( len_uvars > n_integrated +1 ) THEN
       !   PRINT *,'ERROR to many variables names in u_variable_names.'
       !   DO ie=1,len_uvars
       !      PRINT *,' u_variable_names(',ie,') =',u_variable_names(ie)
       !   END DO
       !   PRINT *,'Exiting ...'
       !   STOP 1
       !END IF

       done = .FALSE.
       num_found = 0 !number of variables found
       ie = 1
       !PRINT *,' card_n_var_save ', card_n_var_save
       ! if we saved vorticity then add dim to card_n_var_save 
       IF( save_vort == 1 ) card_n_var_save = card_n_var_save + dim

       DO WHILE ( ie <= card_n_var_save .AND. .NOT. done )
          num_4 = LEN(tmp_name_str)
          CALL RES_READ( 'i1', tmp_name_str, num_4 )  ! variable name to be extracted
          CALL RES_READ( it_2, u_slot, 1_4 )          ! location of variable in file
          !PRINT *, 'Read var name header: ', tmp_name_str 
          !find if this variable is requested in u_variable_names
          i=1; found = .FALSE.
          DO WHILE ( .NOT. found .AND. i <= len_uvars )  ! Cycle through all currently setup vars, breaking when the one in the resfile (u_slot) is found 
             !PRINT *,' cmp ', u_variable_names( i ), tmp_name_str
             !PRINT *, 'u_variable_names 4:',u_variable_names( : ), tmp_name_str

             IF( do_read_all_vars ) THEN !.OR. &
                found = .TRUE. 
                num_found = num_found+1
             !ELSE IF ( ( INDEX(TRIM(ADJUSTL(u_variable_names(i))),     TRIM(ADJUSTL(tmp_name_str)))==1 .AND. &   ! Strings match at the beginning
             !        LEN_TRIM(ADJUSTL(u_variable_names(i)))==LEN_TRIM(ADJUSTL(tmp_name_str)) ) ) THEN      ! Lengths match
             ELSE IF ( TRIM(ADJUSTL(u_variable_names(i))) .EQ.     TRIM(ADJUSTL(tmp_name_str)) ) THEN
                found = .TRUE. 
                num_found = num_found+1
             ELSE
                i = i+1
             END IF
             !PRINT *,' found i', found, i
             !PRINT *, 'u_variable_names 5:',u_variable_names( : ), tmp_name_str
          END DO
          IF( .NOT. found ) THEN ! Seach for IC mapped names
             i=1; found = .FALSE.
             DO WHILE ( .NOT. found .AND. i <= len_uvars )  ! Cycle through all currently setup vars, breaking when the one in the resfile (u_slot) is found 
                IF ( TRIM(ADJUSTL(u_IC_names(i))) .EQ.     TRIM(ADJUSTL(tmp_name_str)) ) THEN
                   found = .TRUE. 
                   num_found = num_found+1
                ELSE
                   i = i+1
                END IF
             END DO
          END IF
          IF( found ) THEN  ! If the variable is found
             IF(u_slot /= i .AND. .NOT. do_read_all_vars ) THEN      ! Diagnostic
                IF ( (verb.GE.1.AND.par_rank.EQ.0).OR.(verb.GE.2) ) THEN
                   WRITE(*,'("i=",I8," u_slot=",I8)') i,u_slot
                   WRITE(*,'("WARNING: read_solution - possible error u_slot /= i")')
                   CALL error (IW=ignore_warnings, F=.TRUE.)
                END IF
             END IF
             IF( do_read_all_vars ) THEN
                u_slot = num_found ! if getting all the vars we just put it in the next slot
             ELSE
                u_slot = i !put var in same place as its name in u_variable_names
             END IF
             IF ( (verb.GE.1.AND.par_rank.EQ.0).OR.(verb.GE.2) ) PRINT *, 'Reading var: ', tmp_name_str , i
          ELSE
             !Not found so we need to read over this var
             !
             u_slot = len_uvars
             IF ( (verb.GE.1.AND.par_rank.EQ.0).OR.(verb.GE.2) ) PRINT *, 'Skipping var: ', tmp_name_str
             !IF ( (verb.GE.1.AND.par_rank.EQ.0).OR.(verb.GE.2) ) PRINT *, 'u_variable_names2  : ', u_variable_names
          END IF

          num_4 = nwlt_tmp
          CALL RES_READ( rt_2, UTMP(1, u_slot), num_4 )

          !
          ! Save the variable name if we are doing do_read_all_vars
          !

          IF( do_read_all_vars ) THEN !CALL set_var_name( u_slot, tmp_name_str ) !u_variable<<_names( u_slot ) = tmp_name_str
            ! Since all vars are being read in order (see above), we just register them here ERIC: we can consolidate some of this logic with above
            !ERIC: n_integrated
            CALL register_var( tmp_name_str, integrated=(u_slot .LE. n_integrated_tmp), adapt=(/.FALSE.,.FALSE./), interpolate=(/.FALSE.,.FALSE./), exact=(/.FALSE.,.FALSE./), saved=.FALSE., req_restart=.FALSE. )
          END IF

          IF( num_found == len_uvars ) THEN
             done = .TRUE.
          ELSE
             ie = ie +1
          END IF
          !PRINT *,' ie ', ie
          !PRINT *,' done ', done
          !PRINT *,' num_found ', num_found
          !PRINT *,' len_uvars ', len_uvars
       END DO
       
       !check that we got all the variables requested
       IF( .NOT. done .AND. &
            ((IC_restart_mode.EQ.1).OR.(IC_restart_mode.EQ.2)) ) &
            CALL error ('Error in reading in some variables')
       
       IF( do_read_all_vars ) CALL setup_mapping()
       
    END IF
    ! --------------------------- end ---------------------------
    ! ---------------------------- U ----------------------------
    ! -----------------------------------------------------------
    

    CALL close_res_file( filename )
    
    ! set correct iwlt index in indx_DB and u
    CALL rearrange_indx_DB_iu (UTMP=UTMP, UOLD=UOLD, ISFIRST=isfirst, OLEN=OLEN )
    
    ! clean memory
    IF (ASSOCIATED(scl_rst_file)) DEALLOCATE (scl_rst_file)
    IF (ASSOCIATED(mxyz_tmp)) DEALLOCATE (mxyz_tmp)
    IF (ASSOCIATED(prd_tmp)) DEALLOCATE (prd_tmp)
    IF (ALLOCATED(u_dummy) ) DEALLOCATE ( u_dummy )

  END SUBROUTINE read_solution_aux_1
  
  
  
  
    
  

  !--------------------------------------------------------------------------------------------
  ! read .res file in a format as of 09/01/2011 .res endian independent
  !
  ! This is the only recommended format to use in parallel.
  ! Make sure that any RES_WRITE in save_solution_aux_2common()
  ! has a correspondent RES_READ in read_solution_aux_2common()
  !
  !!! ========= IMPORTANT FOR CODE DEVELOPERS ==================== 
  ! NOTE, then this subroutine can be only called if and only if 
  ! IC_restart .OR. (IC_from_file .AND. IC_file_fmt == 0),
  ! the subroutine is called from main with do_restart = IC_restart,
  ! thus, if IC_restart=.FALSE., that means that it is initial condition from 
  ! file in native format
  !
  ! EXACT (false by default) is introduced to allow use of read_solution from
  ! a stand-alone application to read exactly what is inside .res file,
  ! without making assumptions if it is a restart or initial condition.
  ! In a stand alone application we don't care, we need exact data, set EXACT=T.
  !
  ! PART_READ:  - 0 - default; read all the file
  !             - 1 -          read dimension and exit
  !             - 4 -          read domain decomposition and exit
  !
  ! IGNOREINIT (false by default) - if true, ignore initial data, e.g. from .inp
  !                                 file, and do not check for errors
  !===============================================================
  SUBROUTINE read_solution_aux_2common( scl, filename, do_read_all_vars, len_uvars, ierr, verb, &
                                        EXACT, PART_READ, FILENUMTOREAD, APPEND, UTMP, UOLD, OLEN, IGNOREINIT, CLEAN_IC_DD, ZERO_UPDATE )
    USE PRECISION
    USE pde                ! it, iwrite, dt
    USE variable_mapping   ! n_var
    USE sizes              ! nwlt
    USE field              ! u
    USE wlt_vars           ! dim, j_lev, j_mx
    USE wlt_trns_mod
    USE wlt_trns_vars      ! TYPE_DB, indx_DB, xx
    USE io_3D_vars         ! IC_restart_mode
    USE debug_vars         ! test_alloc, timers, error
    USE parallel           ! par_size
    USE unused
    USE error_handling
    IMPLICIT NONE
    CHARACTER (LEN=*),       INTENT(IN)           ::  filename
    REAL (pr), DIMENSION(:), POINTER              :: scl
    LOGICAL,                 INTENT(IN)           :: do_read_all_vars     ! if true read all vars
    INTEGER,                 INTENT(INOUT)        :: len_uvars
    INTEGER,                 INTENT(OUT)          :: ierr                 ! number of errors
    INTEGER,                 INTENT(IN)           :: verb                 ! verbouse reading (0,1,2)
    LOGICAL,                 INTENT(IN), OPTIONAL :: EXACT                ! read without assumptions
    LOGICAL,                 INTENT(IN), OPTIONAL :: IGNOREINIT           ! check for .inp errors
    INTEGER,                 INTENT(IN), OPTIONAL :: PART_READ            ! read the beginning of the file only
    LOGICAL,                 POINTER,    OPTIONAL :: FILENUMTOREAD(:)     ! which .res files to read
    LOGICAL,                             OPTIONAL :: APPEND               ! append data from multiple .res files
    REAL (pr),               POINTER,    OPTIONAL :: UTMP(:,:), UOLD(:,:) ! buffers for u array rearranging
    INTEGER,                 POINTER,    OPTIONAL :: OLEN(:,:,:,:)        ! old length buffer for index_DB
    LOGICAL,                 INTENT(IN), OPTIONAL :: CLEAN_IC_DD          ! deallocate IC_par_proc_tree() and IC_Nwlt_per_Tree()
    LOGICAL,                 INTENT(IN), OPTIONAL :: ZERO_UPDATE          ! set n_update to zero for multiproc run

    INTEGER                               :: do_part_read
    INTEGER*4                             :: ierr_4, num_4, version_len_4
    CHARACTER (LEN=2)                     :: it_2, rt_2                             ! default types for RES_READ
    CHARACTER (LEN=VERSION_STRING_LENGTH) :: version_string                         ! I/O version in .res file

    INTEGER*8 :: i_INTEGER8                                     
    INTEGER*8, ALLOCATABLE :: ixyz_INTEGER8(:)            

    INTEGER   :: n_wlt_fmly_tmp, j_mn_read, j_mx_read,    &
                 n_var_rst_file, n_integrated_tmp,  &
                 j, j_df, wlt_type, face_type, card_n_var_save,   &
                 save_vort, idim, ie, ie2, u_slot, num_found, i, &                                                      
                 iwrite_read, &
                 nwlt_tmp, len_tmp, len_old                       ! current .res file nwlt
    !                                                             ! current u -> u_ex
    !                                                             ! current indx_DB -> indx_DB_tmp
    REAL (pr)                              :: dt_tmp
    REAL (pr), DIMENSION(:), ALLOCATABLE   :: u_dummy
    LOGICAL :: done, found, ie_found(1:n_var), do_exact, &
               do_append, do_ignoreinit, do_clean_ic_dd, &
               do_zero_update, &
               isfirst                                            ! true if reading the very first .res file
    INTEGER           :: alloc_stat                               ! allocation status
    INTEGER, POINTER  :: mxyz_tmp(:), prd_tmp(:)
!#ifdef INTEGER8_DEBUG_AR
    INTEGER           :: counter_tmp                              ! loop counter for 'read indx_DB'
!#endif
    IF (ASSOCIATED(IC_mxyz_global)) DEALLOCATE( IC_mxyz_global)
    IF (ASSOCIATED(IC_prd_global))  DEALLOCATE( IC_prd_global) 
    NULLIFY(IC_mxyz_global)  
    NULLIFY(IC_prd_global)

    NULLIFY(mxyz_tmp, prd_tmp) 

    ! set default values:
    do_part_read    = 0        ! read all the file
    do_exact        = .FALSE.  ! make assumptions as if it is a restart
    do_append       = .TRUE.   ! append data from multiple .res files;
    do_ignoreinit   = .FALSE.  ! check for errors and compare with .inp data
    do_clean_ic_dd  = .TRUE.   ! deallocate IC_par_proc_tree() and IC_Nwlt_per_tree while reading .res files
    do_zero_update  = .TRUE.   ! normally, set n_update to zero (n2m does not need that)
    IF (PRESENT(PART_READ))    do_part_read = PART_READ
    IF (PRESENT(EXACT))        do_exact = EXACT
    IF (PRESENT(APPEND))       do_append = APPEND
    IF (PRESENT(IGNOREINIT))   do_ignoreinit = IGNOREINIT
    IF (PRESENT(CLEAN_IC_DD))  do_clean_ic_dd = CLEAN_IC_DD
    IF (PRESENT(ZERO_UPDATE))  do_zero_update = ZERO_UPDATE

    

    ! open .res file for reading
    ! check tree-c/io.h for details
    IF (par_rank.EQ.0) &
       CALL RES_OPEN( 'r', TRIM(filename), LEN(TRIM(filename)), ierr_4 )
    CALL parallel_broadcast_int( ierr_4 )
    IF (ierr_4.NE.0_4) THEN
       WRITE (*,'("Cannot open the file: ",A)') TRIM(filename)
       ierr = 1
       RETURN
    END IF
    ierr = 0

    IF (par_rank.EQ.0) THEN
       ! set integer and real types for RES_READ
       CALL set_default_types( it_2, rt_2 )
    
       ! each RES_READ here should have a correspondent
       ! RES_WRITE in save_solution_aux_1
       CALL RES_READ( 'i4', version_len_4, 1_4 )                   ! version string size
       CALL RES_READ( 'i1', version_string, version_len_4 )        ! version string
    
       ! if required, include version control here
       IF( verb.GE.1 ) WRITE (*,'(A)') version_string(1:version_len_4)
    
       ! read dimension; for restart it should coincide with .inp
       CALL RES_READ( it_2, i, 1_4 )
    END IF
    CALL parallel_broadcast_int( i )    
    CALL parallel_broadcast_int( version_len_4 )    
    CALL parallel_broadcast_char( version_string, version_len_4 )    
    !!WRITE (*,'(A,I6,2A)') 'par_rank=',par_rank,'   version_string=', version_string(1:version_len_4)
    IF (.NOT.(do_exact.OR.do_ignoreinit) ) THEN
       IF (i.NE.dim) CALL error ('unsupported: different dimension in .res and .inp files')
    END IF
    dim = i
    
    ! partial read - read dimension ------------------------------------------------------------------
    IF (do_part_read.EQ.1) THEN
       IF (par_rank.EQ.0) &
          CALL close_res_file( filename )
       RETURN
    END IF

    ! mxyz and prd from .res file
    IF (.NOT.ASSOCIATED(mxyz_tmp)) THEN
       ALLOCATE(mxyz_tmp(dim), STAT=alloc_stat )
       CALL test_alloc( alloc_stat, 'mxyz_tmp in read_solution_aux_1', dim )
    END IF
    IF (.NOT.ASSOCIATED(prd_tmp)) THEN
       ALLOCATE(prd_tmp(dim), STAT=alloc_stat )
       CALL test_alloc( alloc_stat, 'prd_tmp in read_solution_aux_1', dim )
    END IF
    
    ! read number of processors
    IF ( (version_string(1:version_len_4) .EQ. 'VERSION=5 @') .OR. &
         (version_string(1:version_len_4) .EQ. 'VERSION=7 @') ) THEN
       ! 
       ! VERSION=5 is the first parallel version which saves the common information in a separate file called .com.res
       ! VERSION=4 and VERSION=5 are the same in terms of the information which are saved; however,
       ! VERSION=4 is for serial run       and only one .res file    is saved for each station   or
       !              for old parallel run and .p*.res              are saved for each station   but
       ! VERSION=5 is for parallel run     and  .com.res & .p*.res  are saved for each station
       !
       IF (par_rank.EQ.0) THEN
          CALL RES_READ( it_2, IC_par_size, 1_4 )                          ! par_size of .res file
          CALL RES_READ( 'i4', num_4, 1_4 )                                ! size of par_proc_tree(:)
       END IF
       CALL parallel_broadcast_int( IC_par_size )    
       CALL parallel_broadcast_int( num_4 )    
       
       IF (ALLOCATED(IC_par_proc_tree)) DEALLOCATE (IC_par_proc_tree)
       ALLOCATE( IC_par_proc_tree(num_4), STAT=alloc_stat )
       CALL test_alloc( alloc_stat, 'IC_par_proc_tree in read_solution_aux_1', num_4 )
       
       IF (par_rank.EQ.0) &
          CALL RES_READ( it_2, IC_par_proc_tree, num_4 )                   ! ...
       CALL parallel_broadcast_int_1D_array( IC_par_proc_tree, num_4 )   

       IF (ALLOCATED(IC_Nwlt_per_Tree)) THEN
          IF (SIZE(IC_Nwlt_per_Tree).LT.num_4) DEALLOCATE (IC_Nwlt_per_Tree)
       END IF
       IF (.NOT.ALLOCATED(IC_Nwlt_per_Tree)) THEN
          ALLOCATE( IC_Nwlt_per_Tree(num_4), STAT=alloc_stat )
          CALL test_alloc( alloc_stat, 'IC_Nwlt_per_Tree in read_solution_aux_1', num_4 )
       END IF
   
       IF (par_rank.EQ.0) &
          CALL RES_READ( it_2, IC_Nwlt_per_Tree, num_4 )                   ! ...
       CALL parallel_broadcast_int_1D_array( IC_Nwlt_per_Tree, num_4 )   

       IF (par_rank.EQ.0) THEN
          num_4 = dim
          CALL RES_READ( it_2, mxyz_tmp, num_4 )               ! IC_mxyz - may be different in .res and .inp
          CALL RES_READ( it_2, prd_tmp, num_4 )                ! IC_prd  - ...
       END IF
       CALL parallel_broadcast_int( num_4 )    
       CALL parallel_broadcast_int_1D_array( mxyz_tmp, num_4 )   
       CALL parallel_broadcast_int_1D_array( prd_tmp, num_4 )   
    ELSE
       WRITE (*,'("[read_solution_aux_2common]  unsupported version: ",A)') TRIM(version_string)
       CALL error
    END IF
    
    
    IF (.NOT.ASSOCIATED(IC_mxyz_global)) ALLOCATE( IC_mxyz_global(1:dim), STAT=alloc_stat )
    IF (.NOT.ASSOCIATED(IC_prd_global))  ALLOCATE( IC_prd_global(1:dim),  STAT=alloc_stat )    
    IC_mxyz_global(1:dim) =  mxyz_tmp(1:dim)
    IC_prd_global(1:dim)  =  prd_tmp(1:dim)

    
    ! partial read - read domain decomposition --------------------------------------------------------
    IF (do_part_read.EQ.4) THEN
       IF (par_rank.EQ.0) &
          CALL close_res_file( filename )
       RETURN
    END IF

    IF ( (verb.GE.1.AND.par_rank.EQ.0).OR.(verb.GE.2) ) WRITE (*,'("#",I6," processor(s)")') IC_par_size
    
!!$    ! partial read - initial read of parallel restart
!!$    IF ( (do_part_read.EQ.2).OR.(do_part_read.EQ.0) ) THEN
!!$       ! define which files to read
!!$       IF (.NOT.ASSOCIATED(FILENUMTOREAD)) THEN
!!$          ALLOCATE( FILENUMTOREAD(IC_par_size), STAT=alloc_stat )
!!$          CALL test_alloc( alloc_stat, 'fntr in read_solution_aux_1', IC_par_size )
!!$          ! set FILENUMTOREAD - which files to read
!!$          CALL parallel_set_files_to_read (FILENUMTOREAD, mxyz_tmp, prd_tmp)
!!$          
!!$          ! clean allocated
!!$          IF (do_clean_ic_dd.AND.ALLOCATED(IC_par_proc_tree)) DEALLOCATE (IC_par_proc_tree)
!!$          IF (do_clean_ic_dd.AND.ALLOCATED(IC_Nwlt_per_Tree)) DEALLOCATE (IC_Nwlt_per_Tree)
!!$       END IF
!!$       IF (par_rank.EQ.0) &
!!$            CALL close_res_file( filename )
!!$       ! prepare for readings from multiple stations
!!$       IF(ALLOCATED(xx) ) DEALLOCATE(xx)
!!$       IF(ALLOCATED(u) ) DEALLOCATE(u)
!!$       RETURN
!!$    END IF
    
    
    IF (par_rank.EQ.0) &
       CALL RES_READ( it_2, n_wlt_fmly_tmp, 1_4 )
    CALL parallel_broadcast_int( n_wlt_fmly_tmp )

    !
    ! --- WARNING ---
    ! n_wlt_fmly is currently a parameter in wavelet_3d_vars.f90
    ! as soon as it will be changed some IF(do_exact) to be introduced
    ! here, maybe with n_updt(:) and other arrays re-allocation
    !
    IF (do_exact.AND.(n_wlt_fmly_tmp.NE.n_wlt_fmly)) THEN
       CALL error ('unsupported - exact read requested: n_wlt_fmly_tmp != n_wlt_fmly')
    END IF
    
    
    IF(n_wlt_fmly_tmp > n_wlt_fmly) THEN
       CALL error ('ERROR: n_wlt_fmly in .res file > n_wlt_fmly')
    END IF
    
    
    ! --------------------------------------------------------
    ! WARNING
    ! as long as our arrays are (1:3) instead of (1:dim)
    ! the extra dimensions have to be initialized !
    IF (SIZE(nxyz).EQ.3) THEN
       DO i=dim+1, 3
          nxyz(i) = 1
          mxyz(i) = 1
          prd(i) = 1
          grid(i) = 1
       END DO
    END IF
    ! --------------------------------------------------------

    num_4 = n_wlt_fmly_tmp + 1                       ! the arrays are (0:n_wlt_fmly_tmp)
    IF (par_rank.EQ.0) THEN
       CALL RES_READ( it_2, n_prdct_resfile, num_4 )
       CALL RES_READ( it_2, n_updt_resfile,  num_4 )
    END IF
    CALL parallel_broadcast_int_1D_array( n_prdct_resfile, num_4 )
    CALL parallel_broadcast_int_1D_array( n_updt_resfile,  num_4 )
    IF ( par_size.GT.1 .AND. ANY( n_updt_resfile(0:n_wlt_fmly).NE.0) ) &
         CALL error ( &
         '  Attempt to restart from .res file with N_update>0 on several processors.', &
         '  Please use single processor for nonzero N_update or N_update_low_order. ', &
         '  Otherwise, set all N_update to zero in your input parameter file, then  ', &
         '  restart on a single processor (it will read your .res file and save it).', &
         '  The saved file has N_update=0 and can be used for a multiprocessor run. ', &
         '                                                                          ', &
         '       The reason is that the code automatically remaps wavelets          ', &
         '       if N_update or N_predict changes on single processor only.         ', &
         '       In multi-processor runs wavelet transform always uses zero         ', &
         '       N_update values and therefore the code is not able to remap        ', &
         '       wavelets correctly if N_update changes on several processors.      ', &
         '       N_predict modifications will be remaped correctly in any case.     ' )
    
!!$    IF (do_part_read.EQ.0) THEN                      ! Only if the whole .res file is read:
       IF (do_exact.OR.do_ignoreinit) THEN
          n_prdct = n_prdct_resfile                            !   Set equal to prevent
          n_updt = n_updt_resfile                              !   renormalization in init_DB.
       END IF
       IF (do_zero_update) CALL set_zero_n_updt_for_multiproc  !   N update -> 0 for a multiprocessor run.
       
       maxval_n_prdct = maxval(n_prdct)
       maxval_n_updt = maxval(n_updt)
!!$    END IF
    

    
    IF (par_rank.EQ.0) THEN
       CALL RES_READ( it_2, n_assym_prdct_resfile,     num_4 )  ! the arrays are (0:n_wlt_fmly_tmp,0) ( (0,0), (1,0), (2,0), ... )
       CALL RES_READ( it_2, n_assym_updt_resfile,      num_4 )  ! ...
       CALL RES_READ( it_2, n_assym_prdct_bnd_resfile, num_4 )  ! ...
       CALL RES_READ( it_2, n_assym_updt_bnd_resfile,  num_4 )  ! ...

       CALL RES_READ( it_2, card_n_var_save, 1_4 )
       CALL RES_READ( it_2, save_vort, 1_4 )
    END IF
    CALL parallel_broadcast_int_array( n_assym_prdct_resfile,     num_4 )   
    CALL parallel_broadcast_int_array( n_assym_updt_resfile,      num_4 )   
    CALL parallel_broadcast_int_array( n_assym_prdct_bnd_resfile, num_4 )   
    CALL parallel_broadcast_int_array( n_assym_updt_bnd_resfile,  num_4 )   
    CALL parallel_broadcast_int( card_n_var_save )    
    CALL parallel_broadcast_int( save_vort )    
    DO i = 0, n_trnsf_type !0 - regular, 1 - inernal
       n_assym_prdct(:,i)     = n_assym_prdct_resfile(:,0)
       n_assym_prdct_bnd(:,i) = n_assym_prdct_bnd_resfile(:,0)
       n_assym_updt(:,i)      = n_assym_updt_resfile(:,0)
       n_assym_updt_bnd(:,i)  = n_assym_updt_bnd_resfile(:,0)
    END DO 

    num_4 = dim
    IF (par_rank.EQ.0) &
       CALL RES_READ( it_2, nxyz, num_4 )               ! the arrays are (1:dim)
    CALL parallel_broadcast_int_1D_array( nxyz, num_4 )   
    
    IF (version_string(1:version_len_4) .EQ. 'VERSION=1 @') THEN ! it is moved to domain decomposition section
       IF (par_rank.EQ.0) THEN
          CALL RES_READ( it_2, mxyz_tmp, num_4 )                    ! for higher versions
          CALL RES_READ( it_2, prd_tmp,  num_4 )                    ! ...
       END IF
       CALL parallel_broadcast_int_1D_array( mxyz_tmp, num_4 )   
       CALL parallel_broadcast_int_1D_array( prd_tmp,  num_4 )   
    END IF
    
    ! assign periodicity
    IF ( .NOT.(do_exact.OR.do_ignoreinit) ) THEN
       IF ( ANY(prd(1:dim).NE.prd_tmp(1:dim)) ) &
            CALL error ('unsupported: different periodicity in .res and .inp files')
    END IF
    prd(1:dim) = prd_tmp(1:dim)

    IF (par_rank.EQ.0) THEN
       CALL RES_READ( it_2, grid, num_4 )               ! ...

       CALL RES_READ( it_2, j_mn_read, 1_4 )
       CALL RES_READ( it_2, j_mx_read, 1_4 )
       CALL RES_READ( it_2, j_lev,     1_4 )

       !! nwlt is local and moved to .res file
       !!  CALL RES_READ( it_2, nwlt_tmp, 1_4 )             ! number of wavelets in that particular file
       !!  nwlt = nwlt + nwlt_tmp
    
       CALL RES_READ( it_2, it, 1_4 )    
    
       CALL RES_READ( it_2, iwrite_read, 1_4 )
       
       CALL RES_READ( it_2, n_var_rst_file,   1_4 )
       CALL RES_READ( it_2, n_integrated_tmp, 1_4 )
       CALL RES_READ( it_2, n_time_levels,    1_4 ) !No longer in use, left for backward compatibility 
    END IF
    CALL parallel_broadcast_int_1D_array( grid, num_4 )    
    CALL parallel_broadcast_int( j_mn_read )    
    CALL parallel_broadcast_int( j_mx_read )    
    CALL parallel_broadcast_int( j_lev )    
    CALL parallel_broadcast_int( it )    
    CALL parallel_broadcast_int( iwrite_read )    
    CALL parallel_broadcast_int( n_var_rst_file )    
    CALL parallel_broadcast_int( n_integrated_tmp )    
    CALL parallel_broadcast_int( n_time_levels ) !No longer in use, left for backward compatibility 

    SELECT CASE (IC_restart_mode)
    CASE (0)                          ! new run
       iwrite = 0
    CASE (1)                          ! hard restart
       iwrite = iwrite_read + 1
    CASE (2:3)                        ! soft restart and restart from IC
       iwrite = IC_restart_station
    END SELECT
    
    
    
    
!!$    ! partial read - read all but u and indx_DB
!!$    IF (do_part_read.EQ.3) THEN
!!$       mxyz(1:dim) = mxyz_tmp(1:dim)
!!$       j_mn = j_mn_read
!!$       j_mx = j_mx_read
!!$       IF (par_rank.EQ.0) &
!!$            CALL close_res_file( filename )
!!$       RETURN
!!$    END IF
    
    
    IF ( (verb.GE.1.AND.par_rank.EQ.0).OR.(verb.GE.2) ) THEN
       WRITE (*,'("# read_solution: proc #",I5)') par_rank
       IF (verb.GE.2) THEN
          PRINT *, 'dim=', dim
          PRINT *, 'IC_par_size=',IC_par_size
          PRINT *, 'n_wlt_fmly=', n_wlt_fmly_tmp
          PRINT *, 'n_prdct(:)            =', n_prdct(0:n_wlt_fmly_tmp)
          PRINT *, 'n_updt(:)             =', n_updt(0:n_wlt_fmly_tmp)
          PRINT *, 'n_assym_prdct(:,0)    =', n_assym_prdct(0:n_wlt_fmly_tmp,0)
          PRINT *, 'n_assym_updt(:,0)     =', n_assym_updt(0:n_wlt_fmly_tmp,0)
          PRINT *, 'n_assym_prdct_bnd(:,0)=', n_assym_prdct_bnd(0:n_wlt_fmly_tmp,0)
          PRINT *, 'n_assym_updt_bnd(:,0) =', n_assym_updt_bnd(0:n_wlt_fmly_tmp,0)
          PRINT *, 'card_n_var_save=', card_n_var_save
          PRINT *, 'save_vort=', save_vort
       END IF
       PRINT *,'nxyz =',nxyz(1:dim)
       PRINT *,'mxyz =',mxyz_tmp(1:dim)
       PRINT *,'prd  =', prd(1:dim)
       PRINT *,'grid =',grid(1:dim)
       PRINT *,'j_mn =',j_mn_read
       PRINT *,'j_mx =',j_mx_read
       PRINT *,'j_lev=',j_lev
!!     PRINT *,'nwlt =',nwlt_tmp, nwlt
       IF (verb.GE.2) THEN
          PRINT *,'it',it
          PRINT *,'iwrite (in .res file) = ', iwrite_read, ', iwrite = ', iwrite
          PRINT *,'n_var_rst_file',n_var_rst_file
          PRINT *,'n_integrated_tmp',n_integrated_tmp
       END IF
    END IF


!!$    IF (do_exact) THEN  ! ERIC: will be set later when vars registered
!!$      !CALL set_n_integrated ( n_integrated_tmp )
!!$      !CALL error ( 'Cannot do_exact and directly set n_integrated (nothing else being set)' )
!!$    END IF
    
    ! this is a call from code1/nmp/exact
    IF( do_read_all_vars .OR. do_exact ) THEN
       len_uvars = n_var_rst_file
       CALL reset_mapping()                    ! clear registry and mappings !ERIC: will this mess up the allocation of uTMP? 
    END IF

    
    ! this is pure restart
    IF ( (IC_restart_mode.EQ.1) .OR. (IC_restart_mode.EQ.2) ) THEN
       IF ( ANY(mxyz_tmp(1:dim).NE.mxyz(1:dim)) ) THEN
          IF (par_rank.EQ.0) PRINT *, 'Mxyz from input   file:' ,mxyz
          IF (par_rank.EQ.0) PRINT *, 'Mxyz from restart file:' ,mxyz_tmp
          CALL error ( &
               'Not Supported: Pure restart with different Mxyz.', &
               'Instead, use restart with initial conditions: set (IC_restart_mode=3)', &
               'and increase maximum level of resolution J_MX.')
       END IF
       ! Check that if we are doing a restart, j_mn(from input file) = j_mn_read (from restart file)
       ! Actually this is more restrictive than is necessary. It would work to have 
       ! j_mn(from input file) <= j_mn_read but to support this we need to change init_lines_db().
       !
       IF ( j_mn /= j_mn_read .OR. j_mx /= j_mx_read ) THEN
          IF (par_rank.EQ.0) PRINT *, 'j_mn, j_mx from input   file:', j_mn,      j_mx
          IF (par_rank.EQ.0) PRINT *, 'j_mn, j_mx from restart file:', j_mn_read, j_mx_read
          CALL error ( &
               'Not Supported: Pure restart with j_mn or j_mx different from restart file.', &
               'Instead, use restart with initial conditions: set (IC_restart_mode=3)', &
               'in you input parameter file.')
       END IF
    END IF
    
    
    ! ---------------------------------------------------------------------------------------
    ! For post processing we are doing a restart but we do want to take the value of j_mx and j_mn 
    !  from the input (IC_restart to be set to .FALSE. for postprocessing)
    !
    !IF( (j_mx == 0).OR.do_exact ) THEN       ! j_mx = 0 in main before reading .inp file
    IF (do_exact.OR.do_ignoreinit) THEN
       j_mx = j_mx_read
       j_mn = j_mn_read
    END IF
    !
    ! it is restart as IC
    IF ( IC_restart_mode.EQ.3 .AND. j_mx < j_lev) THEN
       IF (par_rank.EQ.0) WRITE(*,'("j_mx=",I2,", j_lev=",I2)') j_mx, j_lev
       CALL error ( &
            'Not Supported: j_mx (from .inp) < j_lev (from .res)' )
    END IF
    !
    ! In non-restart cases that the IC file is read from a restart file
    ! we use j_mx and j_mn from the input file
    ! ---------------------------------------------------------------------------------------

    mxyz(1:dim) =  mxyz_tmp(1:dim)
    !DG CHANGE TO RESTART WITH DIFFERENT Mxyz:     !if we are doing restart we want to use the Mxyz values from the inp file
    !DG CHANGE TO RESTART WITH DIFFERENT Mxyz:     IF(.NOT. IC_restart ) mxyz(1:3) = mxyz_tmp(1:3)

    IF (par_rank.EQ.0) THEN
       CALL RES_READ( rt_2, t,      1_4 )
       CALL RES_READ( rt_2, dt_tmp, 1_4 )
    END IF
    CALL parallel_broadcast( t )    
    CALL parallel_broadcast( dt_tmp )    

    !
    ! Use dt from input file if we are doing a restart
    !
    IF ( IC_restart_mode.EQ.1 .OR. &          !
         IC_restart_mode.EQ.2 .OR. &          !
         do_exact.OR.do_ignoreinit ) THEN     ! pure restart or postprocessing
       dt = dt_tmp
    ELSE                                                   ! restart as IC
       IF( dt /= dt_tmp) THEN
          IF ( (verb.GE.1.AND.par_rank.EQ.0).OR.(verb.GE.2) ) THEN
             IF (verb.GE.2) THEN
                PRINT *, 't     =',t
                PRINT *, 'dt_tmp=',dt_tmp
             END IF
             PRINT *,''
             PRINT *,'***************************************************'
             PRINT *,'Restarting using dt from input file:',dt
             PRINT *,'Not using dt from restart file:', dt_tmp
             PRINT *,'***************************************************'
             PRINT *,''
          END IF
       END IF
    END IF
    dt = MIN(dt, dt_original)      
    
    
    !
    ! res_save_coeff=T - save wavelet coefficients in .res file (given in .inp file)
    ! res_save_coeff_IC - (given in .res file)
    !
    res_save_coeff_IC = .TRUE.  ! all previous versions have coefficients in .res file
    num_4 = 1
    IF ( (par_rank.EQ.0) .AND. (version_string(1:version_len_4) .EQ. 'VERSION=7 @') ) &
         CALL RES_READ( it_2, num_4, 1_4 )
    CALL parallel_broadcast_int (num_4)
    IF ( num_4.EQ.0 ) res_save_coeff = .FALSE.
    
    
    
    ! Make  n_var_rst_file, card_n_var_save, save_vort  global  in order to use them in  SUBROUTINE read_solution_aux_2(...)
    len_uvars__aux_2__global        = len_uvars
    n_var_rst_file__aux_2__global   = n_var_rst_file
    card_n_var_save__aux_2__global  = card_n_var_save
    save_vort__aux_2__global        = save_vort
    n_integrated_tmp__aux_2__global = n_integrated_tmp

    IF (verb.GE.2) THEN
       PRINT *, 'read_solution_aux_2common  len_uvars       : ', len_uvars 
       PRINT *, 'read_solution_aux_2common  n_var_rst_file  : ', n_var_rst_file 
       PRINT *, 'read_solution_aux_2common  card_n_var_save : ', card_n_var_save 
       PRINT *, 'read_solution_aux_2common  save_vort       : ', save_vort 
    END IF



    IF (par_rank.EQ.0) &
       CALL close_res_file( filename )
    
    ! clean memory
    IF (ASSOCIATED(mxyz_tmp)) DEALLOCATE (mxyz_tmp)
    IF (ASSOCIATED(prd_tmp))  DEALLOCATE (prd_tmp)
 
  END SUBROUTINE read_solution_aux_2common
  
      
  
   

  !--------------------------------------------------------------------------------------------
  ! read .res file in a format as of 09/01/2011 .res endian independent
  !
  ! This is the only recommended format to use in parallel.
  ! Make sure that any RES_WRITE in save_solution_aux_2()
  ! has a correspondent RES_READ in read_solution_aux_2()
  !
  !!! ========= IMPORTANT FOR CODE DEVELOPERS ==================== 
  ! NOTE, then this subroutine can be only called if and only if 
  ! IC_restart .OR. (IC_from_file .AND. IC_file_fmt == 0),
  ! the subroutine is called from main with do_restart = IC_restart ,
  ! thus, if IC_restart =.FALSE., that means that it is initial condition from 
  ! file in native format
  !
  ! EXACT (false by default) is introduced to allow use of read_solution from
  ! a stand-alone application to read exactly what is inside .res file,
  ! without making assumptions if it is a restart or initial condition.
  ! In a stand alone application we don't care, we need exact data, set EXACT=T.
  !
  ! IGNOREINIT (false by default) - if true, ignore initial data, e.g. from .inp
  !                                 file, and do not check for errors
  !===============================================================
  SUBROUTINE read_solution_aux_2( scl, filename, do_read_all_vars, len_uvars, ierr, verb, &
                                  EXACT, APPEND, UTMP, UOLD, OLEN, IGNOREINIT, CLEAN_IC_DD, ZERO_UPDATE )
    USE PRECISION
    USE pde                ! it, iwrite, dt
    USE variable_mapping   ! n_var
    USE sizes              ! nwlt
    USE field              ! u
    USE wlt_vars           ! dim, j_lev, j_mx
    USE wlt_trns_mod
    USE wlt_trns_vars      ! TYPE_DB, indx_DB, xx
    USE io_3D_vars         ! IC_restart_mode
    USE debug_vars         ! test_alloc, timers, error
    USE parallel           ! par_size
    USE error_handling
    IMPLICIT NONE
    CHARACTER (LEN=*),       INTENT(IN)           ::  filename
    REAL (pr), DIMENSION(:), POINTER              :: scl, scl_rst_file
    LOGICAL,                 INTENT(IN)           :: do_read_all_vars     ! if true read all vars
    INTEGER,                 INTENT(INOUT)        :: len_uvars
    INTEGER,                 INTENT(OUT)          :: ierr                 ! number of errors
    INTEGER,                 INTENT(IN)           :: verb                 ! verbouse reading (0,1,2)
    LOGICAL,                 INTENT(IN), OPTIONAL :: EXACT                ! read without assumptions
    LOGICAL,                 INTENT(IN), OPTIONAL :: IGNOREINIT           ! check for .inp errors
    LOGICAL,                             OPTIONAL :: APPEND               ! append data from multiple .res files
    REAL (pr),               POINTER,    OPTIONAL :: UTMP(:,:), UOLD(:,:) ! buffers for u array rearranging
    INTEGER,                 POINTER,    OPTIONAL :: OLEN(:,:,:,:)        ! old length buffer for index_DB
    LOGICAL,                 INTENT(IN), OPTIONAL :: CLEAN_IC_DD          ! deallocate IC_par_proc_tree() and IC_Nwlt_per_Tree()
    LOGICAL,                 INTENT(IN), OPTIONAL :: ZERO_UPDATE          ! set n_update to zero for multiproc run

    INTEGER*4                             :: ierr_4, num_4, version_len_4
    CHARACTER (LEN=2)                     :: it_2, rt_2                             ! default types for RES_READ
    CHARACTER (LEN=VERSION_STRING_LENGTH) :: version_string                         ! I/O version in .res file

    INTEGER*8 :: i_INTEGER8                                      
    INTEGER*8, ALLOCATABLE :: ixyz_INTEGER8(:)            

    INTEGER   :: n_wlt_fmly_tmp, j_mn_read, j_mx_read,    &
                 n_var_rst_file, n_integrated_tmp,  &
                 j, j_df, wlt_type, face_type, card_n_var_save,   &
                 save_vort, idim, ie, ie2, u_slot, num_found, i, &                                                      
                 iwrite_read, &
                 nwlt_tmp, len_tmp, len_old                       ! current .res file nwlt
    !                                                             ! current u -> u_ex
    !                                                             ! current indx_DB -> indx_DB_tmp
    TYPE (indx_type_DB), POINTER           :: indx_DB_ptr         ! pointer to indx_DB element
    REAL (pr)                              :: dt_tmp
    REAL (pr), DIMENSION(:), ALLOCATABLE   :: u_dummy
    LOGICAL :: done, found, ie_found(1:n_var), do_exact, &
               do_append, do_ignoreinit, do_clean_ic_dd, &
               do_zero_update, &
               isfirst                                            ! true if reading the very first .res file
    INTEGER           :: alloc_stat                               ! allocation status
!#ifdef INTEGER8_DEBUG_AR
    INTEGER           :: counter_tmp                              ! loop counter for 'read indx_DB'
!#endif
   INTEGER, DIMENSION(:), ALLOCATABLE :: n_var_wlt_fmly_local ! wavelet transform family for each variable
    
    NULLIFY(scl_rst_file)
    NULLIFY(indx_DB_ptr)
    
    ! set default values:
    do_exact        = .FALSE.  ! make assumptions as if it is a restart
    do_append       = .TRUE.   ! append data from multiple .res files;
    do_ignoreinit   = .FALSE.  ! check for errors and compare with .inp data
    do_clean_ic_dd  = .TRUE.   ! deallocate IC_par_proc_tree() and IC_Nwlt_per_tree while reading .res files
    do_zero_update  = .TRUE.   ! normally, set n_update to zero (n2m does not need that)
    IF (PRESENT(EXACT))        do_exact = EXACT
    IF (PRESENT(APPEND))       do_append = APPEND
    IF (PRESENT(IGNOREINIT))   do_ignoreinit = IGNOREINIT
    IF (PRESENT(CLEAN_IC_DD))  do_clean_ic_dd = CLEAN_IC_DD
    IF (PRESENT(ZERO_UPDATE))  do_zero_update = ZERO_UPDATE

    
    ! Load global values of  n_var_rst_file, card_n_var_save, save_vort  which were assigned in read_solution_aux_2common(...)
    len_uvars       = len_uvars__aux_2__global
    n_var_rst_file  = n_var_rst_file__aux_2__global  
    card_n_var_save = card_n_var_save__aux_2__global   
    save_vort       = save_vort__aux_2__global   
    n_integrated_tmp=n_integrated_tmp__aux_2__global



    ! open .res file for reading
    ! check tree-c/io.h for details
    CALL RES_OPEN( 'r', TRIM(filename), LEN(TRIM(filename)), ierr_4 )
    IF (ierr_4.NE.0_4) THEN
       WRITE (*,'("Cannot open the file: ",A)') TRIM(filename)
       ierr = 1
       RETURN
    END IF
    ierr = 0

    ! set integer and real types for RES_READ
    CALL set_default_types( it_2, rt_2 )
    
    ! each RES_READ here should have a correspondent
    ! RES_WRITE in save_solution_aux_1
    CALL RES_READ( 'i4', version_len_4, 1_4 )                   ! version string size
    CALL RES_READ( 'i1', version_string, version_len_4 )        ! version string
    
    CALL RES_READ( it_2, nwlt_tmp, 1_4 )                        ! number of wavelets in that particular file
    nwlt = nwlt + nwlt_tmp

    ! if required, include version control here
    IF( verb.GE.1 ) WRITE (*,'(A)') version_string(1:version_len_4)
    IF( verb.GE.1 ) PRINT *,'nwlt =', nwlt_tmp, nwlt
      
    ! ------------------------------------------------------
    ! prepare for single file reading
    IF (.NOT.do_append) THEN
       IF( ALLOCATED(xx) )      DEALLOCATE(xx)
       IF( ALLOCATED(u) )       DEALLOCATE(u)
       IF( ALLOCATED(indx_DB) ) CALL deallocate_indx_DB
    END IF
    ! this is for the indx_DB for all the data (from all .res files)
    IF(.NOT.ALLOCATED(indx_DB)) THEN
       ALLOCATE(indx_DB(1:j_mx,0:2**dim-1,0:3**dim-1,1:j_mx), STAT=alloc_stat)
       CALL test_alloc( alloc_stat, 'indx_DB in read_solution_aux_1',j_mx*(2**dim)*(3**dim)*j_mx )
       DO j = 1, j_mx
          DO wlt_type = MIN(j-1,1),2**dim - 1
             DO face_type = 0, 3**dim - 1
                DO j_df = 1, j_mx
                   NULLIFY (indx_DB(j_df,wlt_type,face_type,j)%p)
                END DO
             END DO
          END DO
       END DO
       indx_DB(:,:,:,:)%real_length = 0
       indx_DB(:,:,:,:)%length = 0
!!$       indx_DB(:,:,:,:)%shift = 0
    END IF
    ! ------------------------------------------------------
    
    
    IF ( (IC_restart_mode.EQ.1).OR.(IC_restart_mode.EQ.2) ) THEN
       ! pure restart parameter check
       IF( (n_var > n_var_rst_file).AND.(verb.GE.1) ) THEN
          CALL error ( &
               '*************************************************************************', &
               'Restarting with n_var > n_var in restart file! Caution this might be wrong!', &
               '*************************************************************************', &
               IW=ignore_warnings, F=.TRUE.)
       END IF
       IF( n_var < n_var_rst_file ) &
            CALL error ('Restart with n_var <  n_var in restart file! This is not supported.')
       IF( (n_integrated_tmp /= n_integrated).AND.(verb.GE.1) ) THEN
          CALL error ( &
               '*************************************************************************', &
               'Restart with n_integrated /= n_integrated in restart file! Possible Error !!!', &
               '*************************************************************************', &
               IW=ignore_warnings, F=.TRUE.)
       END IF
       ! u_dummy for the skipped variables
       ALLOCATE( u_dummy(1:nwlt_tmp), STAT=alloc_stat )
       CALL test_alloc( alloc_stat, 'u_dummy in read_solution_aux_1', nwlt_tmp )
       ! allocate u (or increase the size for multiple .res files)
       CALL allocate_u( OLDSIZE=nwlt-nwlt_tmp, NEWSIZE=nwlt, NVAR=n_var, UTMP=UTMP, UOLD=UOLD, ISFIRST=isfirst )
       
    ELSE
       ! restart as IC
       CALL allocate_u( OLDSIZE=nwlt-nwlt_tmp, NEWSIZE=nwlt, NVAR=len_uvars, UTMP=UTMP, UOLD=UOLD, ISFIRST=isfirst )
       IF ( (len_uvars .GT. card_n_var_save).AND.(verb.GE.1).AND.par_rank.EQ.0 ) THEN
          PRINT *,'WARNING: possible error. Number of requested variables (',len_uvars,')'
          PRINT *,'is greater then number of variables saved in this data file (',card_n_var_save,')'
          CALL error (IW=ignore_warnings, F=.TRUE.)
       END IF
    END IF
    
    
    ! read grid coordinates
    IF (.NOT.ALLOCATED(xx)) THEN
       ALLOCATE (xx(0:MAXVAL(nxyz(1:dim)),1:dim), STAT=alloc_stat)
       CALL test_alloc( alloc_stat, 'xx in read_solution_aux_1', (MAXVAL(nxyz(1:dim))+1)*dim )
    END IF
    DO idim = 1,dim
       num_4 = nxyz(idim) + 1                          ! xx(0:nxyz(idim),1:dim)
       CALL RES_READ( rt_2, xx(0,idim), num_4 )
    END DO
    
    ! allocate counter of old lengths (once for all the reads)
    !IF (.NOT.ASSOCIATED(OLEN)) WRITE (*,'(A, I6, A, 4I12)') ' rank', par_rank, ' OLEN is .NOT.ASSOCIATED     SHAPE(OLEN)=', SHAPE(OLEN)
    IF (.NOT.ASSOCIATED(OLEN)) ALLOCATE( OLEN(1:j_lev,0:2**dim-1,0:3**dim - 1,1:j_lev) )

!#ifdef INTEGER8_DEBUG_AR
!    counter_tmp = 0
!#endif
    ! read indx_DB and store old real_length in OLEN
    DO j = 1, j_lev
       DO wlt_type = MIN(j-1,1),2**dim-1
          DO face_type = 0, 3**dim - 1
             DO j_df = j, j_lev
                
                indx_DB_ptr => indx_DB(j_df,wlt_type,face_type,j)
                len_old = indx_DB_ptr%real_length;                      ! old size of multiple files data
                OLEN(j_df,wlt_type,face_type,j) = len_old;              ! (store it for future u/indx_DB remapping)
                CALL RES_READ( it_2, len_tmp, 1_4 )                     ! size of the current .res file data
                
                ! allocate indx_DB (or increase the size for multiple .res files)
                CALL allocate_indx_DB ( indx_DB_ptr, OLDLEN=len_old, NEWLEN=len_old+len_tmp ) !, IWLTADD=nwlt-nwlt_tmp )
                
!#ifdef INTEGER8_DEBUG_AR
!                counter_tmp = counter_tmp + 1
!#endif
                IF( len_tmp  > 0) THEN

                   num_4 = len_tmp
                   CALL RES_READ( it_2, indx_DB_ptr%p(1+len_old:len_old+len_tmp)%i,    num_4 )

#ifdef DBWRK_IO3D
                      IF( ALLOCATED(ixyz_INTEGER8) ) DEALLOCATE(ixyz_INTEGER8)                             !added by SR on 04/06/2011
                      ALLOCATE( ixyz_INTEGER8(len_tmp), STAT=alloc_stat )                                  !added by SR on 04/06/2011
                      CALL RES_READ( 'i8', ixyz_INTEGER8(1:len_tmp), num_4 )                               !added by SR on 04/06/2011
                      indx_DB_ptr%p(1+len_old:len_old+len_tmp)%ixyz = ixyz_INTEGER8(1:len_tmp)             !added by SR on 04/06/2011
                      IF( ALLOCATED(ixyz_INTEGER8) ) DEALLOCATE(ixyz_INTEGER8)                             !added by SR on 04/06/2011
#else
                   CALL RES_READ( 'i8', indx_DB_ptr%p(1+len_old:len_old+len_tmp)%ixyz, num_4 )                              
#endif
                   i_INTEGER8 = indx_DB_ptr%p(1+len_old)%ixyz

#ifdef INTEGER8_DEBUG_AR
    WRITE (*,'(A, A, A, I, A, I2, A, I2, A, I6, A, I6)')  'RES_READ    it_2=', it_2, '  i_INTEGER8=', i_INTEGER8, '   KIND(i_INTEGER8)=',KIND(i_INTEGER8), '   KIND(indx_DB_ptr%p()%ixyz)=', KIND(indx_DB_ptr%p(len_old+len_tmp)%ixyz), '     num_4=', num_4, '   len_tmp=', len_tmp
#endif
                END IF
                
             END DO
          END DO
       END DO
    END DO
    
!#ifdef INTEGER8_DEBUG_AR
!    IF (par_rank .EQ. 0) WRITE (*,'(A, I6, A, I12)')  '  par_rank=', par_rank, '     read indx_DB counter=', counter_tmp
!#endif

    !read the scl array for this time step
    IF( .NOT. ASSOCIATED(scl_rst_file)   ) THEN
       ALLOCATE ( scl_rst_file(1:n_var_rst_file), STAT=alloc_stat)                  ! 04/03/2006: Oleg's change
       CALL test_alloc( alloc_stat, 'scl_rst_file in read_solution_aux_1', n_var_rst_file)
    END IF
    IF( .NOT. ASSOCIATED(scl)            ) THEN
       ALLOCATE ( scl(1:n_var), STAT=alloc_stat)                                    ! 04/03/2006: Oleg's change
       CALL test_alloc( alloc_stat, 'scl in read_solution_aux_1', n_var )
    END IF
    IF( .NOT. ASSOCIATED(scl_global)     ) THEN
       ALLOCATE ( scl_global(1:n_var), STAT=alloc_stat)                             ! 04/03/2006: Oleg's change
       CALL test_alloc( alloc_stat, 'scl_global in read_solution_aux_1', n_var )
    END IF
    !IF( .NOT. ALLOCATED(n_var_wlt_fmly)  ) THEN
    !   ALLOCATE ( n_var_wlt_fmly(1:n_var_rst_file), STAT=alloc_stat )               ! 04/03/2006: Oleg's change
    !   CALL test_alloc( alloc_stat, 'n_var_wlt_fmly in read_solution_aux_1', n_var_rst_file )
    !END IF
    IF( .NOT. ALLOCATED(n_var_wlt_fmly_local)  ) THEN
       ALLOCATE ( n_var_wlt_fmly_local(1:n_var_rst_file), STAT=alloc_stat )               ! ERIC: updated to reflect deprection 
       CALL test_alloc( alloc_stat, 'n_var_wlt_fmly_local in read_solution_aux_1', n_var_rst_file )
    END IF
    
    scl = 1.0_pr
    num_4 = n_var_rst_file
    CALL RES_READ( rt_2, scl_rst_file, num_4 )                  ! scl(1:n_var_rst_file)
    CALL RES_READ( it_2, n_var_wlt_fmly_local, num_4 )     ! n_var_wlt_fmly(1:n_var_rst_file)


    IF( (verb.EQ.1.AND.par_rank.EQ.0).OR.(verb.GE.2) ) WRITE(*,'( "Reading in iteration:", I3 ,",  t = ", E12.5 )') it, t
    IF (verb.GE.2) THEN
       PRINT *, 'scl_rst_file=',scl_rst_file
       PRINT *, 'n_var_wlt_fmly=',n_var_wlt_fmly_local
    END IF
    
    
    ! --- WARNING ---
    ! n_var_wlt_fmly is not defined in save/read_solution


    IF (do_exact.OR.do_ignoreinit) THEN
       scl = scl_rst_file
    END IF
    
    
    ! -----------------------------------------------------------
    ! ---------------------------- U ----------------------------
    ! -------------------------- begin --------------------------
    IF ( (IC_restart_mode.EQ.1).OR.(IC_restart_mode.EQ.2) ) THEN
       ie_found(:) = .FALSE.
       DO ie = 1, card_n_var_save

          num_4 = LEN(tmp_name_str)
          CALL RES_READ( 'i1', tmp_name_str, num_4 )
          CALL RES_READ( it_2, u_slot, 1_4 )

          found = .FALSE.
          DO ie2 = 1,  n_var
             !IF( INDEX(TRIM(ADJUSTL(u_variable_names(ie2))),     TRIM(ADJUSTL(tmp_name_str)))==1 .AND. &
             !      LEN_TRIM(ADJUSTL(u_variable_names(ie2)))==LEN_TRIM(ADJUSTL(tmp_name_str)) ) THEN
             !PRINT *, 'u_variable_names 7:',u_variable_names( : ), tmp_name_str
             IF( TRIM(ADJUSTL(u_variable_names(ie2))) .EQ.    TRIM(ADJUSTL(tmp_name_str)) ) THEN
                num_4 = nwlt_tmp
                CALL RES_READ( rt_2, UTMP(1, ie2), num_4 )
                scl(ie2) = scl_rst_file(u_slot)
                ie_found(ie2) = .TRUE.
                found = .TRUE.
                IF ( (verb.GE.1.AND.par_rank.EQ.0).OR.(verb.GE.2) ) PRINT *, 'Reading var  : ', tmp_name_str
                exit ! exit loop
             END IF
          END DO
          IF( .NOT. found ) THEN  ! Search for variable mapping to read into another var 
             DO ie2 = 1,  n_var
                IF( TRIM(ADJUSTL(u_IC_names(ie2))) .EQ.    TRIM(ADJUSTL(tmp_name_str)) ) THEN
                   num_4 = nwlt_tmp
                   CALL RES_READ( rt_2, UTMP(1, ie2), num_4 )
                   scl(ie2) = scl_rst_file(u_slot)
                   ie_found(ie2) = .TRUE.
                   found = .TRUE.
                   IF ( (verb.GE.1.AND.par_rank.EQ.0).OR.(verb.GE.2) ) PRINT *, 'Reading var  : ', tmp_name_str
                   exit ! exit loop
                END IF
             END DO
          END IF
          IF( .NOT. found ) THEN
             num_4 = nwlt_tmp
             CALL RES_READ( rt_2, u_dummy, num_4 )
             !PRINT *, 'u_variable_names 8:',u_variable_names( : ), tmp_name_str
          END IF
       END DO
       scl_global = scl
       DO ie = 1,  n_var
          IF(  n_var_req_restart(ie) .AND. .NOT. ie_found(ie) ) THEN
             PRINT *, 'Error restarting, required restart variable: ', u_variable_names(ie)
             CALL error ('Variable has not been found in restart file.')
          END IF
       END DO
       !PRINT *, 'u_variable_names 9:',u_variable_names( : )

    ELSE !not restart
       ! This check is not necessary. It does not take into account additional variables
       ! that were saved
       !IF( len_uvars > n_integrated +1 ) THEN
       !   PRINT *,'ERROR to many variables names in u_variable_names.'
       !   DO ie=1,len_uvars
       !      PRINT *,' u_variable_names(',ie,') =',u_variable_names(ie)
       !   END DO
       !   PRINT *,'Exiting ...'
       !   STOP 1
       !END IF

       done = .FALSE.
       num_found = 0 !number of variables found
       ie = 1
       !PRINT *,' card_n_var_save ', card_n_var_save
       ! if we saved vorticity then add dim to card_n_var_save 
       IF( save_vort == 1 ) card_n_var_save = card_n_var_save + dim
       !!PRINT *, 'card_n_var_save & len_uvars & dim:', card_n_var_save, len_uvars, dim 

       DO WHILE ( ie <= card_n_var_save .AND. .NOT. done )
          num_4 = LEN(tmp_name_str)
          CALL RES_READ( 'i1', tmp_name_str, num_4 )
          CALL RES_READ( it_2, u_slot, 1_4 )
          !!PRINT *, 'Read var name header: ', tmp_name_str 
          !find if this variable is requested in u_variable_names
          i=1; found = .FALSE.
          DO WHILE ( .NOT. found .AND. i <= len_uvars ) 
             !!PRINT *,' cmp ', u_variable_names( i ), tmp_name_str
             IF( do_read_all_vars ) THEN !.OR. &
                found = .TRUE. 
                num_found = num_found+1
                 !( INDEX(TRIM(ADJUSTL(u_variable_names(i))),     TRIM(ADJUSTL(tmp_name_str)))==1 .AND. &
                 !    LEN_TRIM(ADJUSTL(u_variable_names(i)))==LEN_TRIM(ADJUSTL(tmp_name_str)) ) )  THEN
             ELSE IF ( ( TRIM(ADJUSTL(u_variable_names(i))).EQ.   TRIM(ADJUSTL(tmp_name_str)) ) )  THEN
                found = .TRUE. 
                num_found = num_found+1
             ELSE
                i = i+1
             END IF
             !PRINT *,' found i', found, i
          END DO
          !PRINT *, 'u_variable_names 10:',u_variable_names( : ), tmp_name_str

          IF( .NOT. found ) THEN
             i=1; found = .FALSE.
             DO WHILE ( .NOT. found .AND. i <= len_uvars ) 
                IF ( ( TRIM(ADJUSTL(u_IC_names(i))).EQ.   TRIM(ADJUSTL(tmp_name_str)) ) )  THEN
                   found = .TRUE. 
                   num_found = num_found+1
                ELSE
                   i = i+1
                END IF
             END DO
          END IF

          IF( found ) THEN
             IF(u_slot /= i .AND. .NOT. do_read_all_vars ) THEN
                IF ( (verb.GE.1.AND.par_rank.EQ.0).OR.(verb.GE.2) ) THEN
                   WRITE(*,'("i=",I8," u_slot=",I8)') i,u_slot
                   WRITE(*,'("WARNING: read_solution - possible error u_slot /= i")')
                   CALL error (IW=ignore_warnings, F=.TRUE.)
                END IF
             END IF
             IF( do_read_all_vars ) THEN
                u_slot = num_found ! if getting all the vars we just put it in the next slot
             ELSE
                u_slot = i !put var in same place as its name in u_variable_names
             END IF
             IF ( (verb.GE.1.AND.par_rank.EQ.0).OR.(verb.GE.2) ) PRINT *, 'Reading var: ', tmp_name_str , i
          ELSE
             !Not found so we need to read over this var
             !
             u_slot = len_uvars
          END IF
          num_4 = nwlt_tmp
          CALL RES_READ( rt_2, UTMP(1, u_slot), num_4 )
          IF( ALLOCATED( u_variable_names ) ) THEN
            IF  ( INDEX(TRIM(ADJUSTL('DOMAINFORMATLAB')), TRIM(ADJUSTL(u_variable_names(1))))==1 .AND. LEN_TRIM(ADJUSTL('DOMAINFORMATLAB'))==LEN_TRIM(ADJUSTL(u_variable_names(1))) )   THEN
              UTMP(1:nwlt_tmp,u_slot) = REAL(curparrank,pr)
            END IF
          END IF
          !PRINT *, 'u_variable_names 11:',u_variable_names( : ), tmp_name_str

          !
          ! Save the variable name if we are doing do_read_all_vars
          !
          ! ERIC: why is this subroutine called twice?  This is attempting to register vars again
          IF( do_read_all_vars .AND. .NOT. ALLOCATED(u_variable_names) ) THEN !CALL set_var_name( u_slot, tmp_name_str ) !u_variable_names( u_slot ) = tmp_name_str
             !CALL register_var( tmp_name_str, .FALSE., adapt=(/.FALSE.,.FALSE./), interpolate=(/.FALSE.,.FALSE./), exact=(/.FALSE.,.FALSE./), saved=.FALSE., req_restart=.FALSE. )
            CALL register_var( tmp_name_str, integrated=(u_slot .LE. n_integrated_tmp), adapt=(/.FALSE.,.FALSE./), interpolate=(/.FALSE.,.FALSE./), exact=(/.FALSE.,.FALSE./), saved=.FALSE., req_restart=.FALSE. )
          END IF

          IF( num_found == len_uvars ) THEN
             done = .TRUE.
          ELSE
             ie = ie +1
          END IF
          !PRINT *,' ie ', ie
          !PRINT *,' done ', done
          !PRINT *,' num_found ', num_found
          !PRINT *,' len_uvars ', len_uvars
       END DO
       
       !check that we got all the variables requested
       IF( .NOT. done .AND. &
            ((IC_restart_mode.EQ.1).OR.(IC_restart_mode.EQ.2)) ) &
            CALL error ('Error in reading in some variables')
       
       IF( do_read_all_vars .AND. .NOT. ALLOCATED(u_variable_names) ) CALL setup_mapping()

       IF (verb.GE.2) THEN
          DO i = 1,  len_uvars
             WRITE(*,'( A, I6, A, I3, 2A )') ' par_rank ', par_rank, ' u_variable_names(', i, ')', u_variable_names(i)
          END DO
       END IF
    END IF
    ! --------------------------- end ---------------------------
    ! ---------------------------- U ----------------------------
    ! -----------------------------------------------------------
    

    CALL close_res_file( filename )
    
    ! set correct iwlt index in indx_DB and u
    CALL rearrange_indx_DB_iu (UTMP=UTMP, UOLD=UOLD, ISFIRST=isfirst, OLEN=OLEN )
    
    ! clean memory
    IF (ASSOCIATED(scl_rst_file)) DEALLOCATE (scl_rst_file)
    IF (ALLOCATED(u_dummy) ) DEALLOCATE ( u_dummy )

  END SUBROUTINE read_solution_aux_2
  








  
  
  
  


  !--------------------------------------------------------------------------------------------
  ! PUBLIC
  ! Read version in solution file. 
  ! Called from main and separate post_processing codes
  ! Used to identify whether   read_solution( ..., DO_READ_COMMONFILE_TRUE, ...)   can be called prior to calling  pre_init_DB
  SUBROUTINE read_solution_version( filename )
    USE PRECISION
    USE pde                ! n_var, it, iwrite, dt
    USE sizes              ! nwlt
    USE field              ! u
    USE wlt_vars           ! dim, j_lev, j_mx
    USE wlt_trns_mod
    USE wlt_trns_vars      ! TYPE_DB, indx_DB, xx
    USE io_3D_vars         ! IC_restart_mode
    USE debug_vars         ! test_alloc, timers, error
    USE parallel           ! par_size
    IMPLICIT NONE
    CHARACTER (LEN=*) , INTENT(IN) ::  filename

    INTEGER*4                             :: ierr_4, num_4, version_len_4
    CHARACTER (LEN=2)                     :: it_2, rt_2                               ! default types for RES_READ
    CHARACTER (LEN=VERSION_STRING_LENGTH) :: version_string                           ! I/O version in .res file


    INTEGER   :: ierr
    

  
    ierr = 0

    IF (par_rank .EQ. 0) THEN
      
       ! open .res file for reading
       ! check tree-c/io.h for details
       CALL RES_OPEN( 'r', TRIM(filename), LEN(TRIM(filename)), ierr_4 )
       IF (ierr_4 .EQ. 0_4) THEN

          ! set integer and real types for RES_READ
          CALL set_default_types( it_2, rt_2 )
    
          ! each RES_READ here should have a correspondent
          ! RES_WRITE in save_solution_aux_1
          CALL RES_READ( 'i4', version_len_4, 1_4 )                   ! version string size
          CALL RES_READ( 'i1', version_string, version_len_4 )        ! version string
    

          SELECT CASE (version_string(1:version_len_4))   
             CASE ('VERSION=1 @')    
                IC_RES_VERSION = 1
             CASE ('VERSION=2 @')    
                IC_RES_VERSION = 2
             CASE ('VERSION=3 @')    
                IC_RES_VERSION = 3
             CASE ('VERSION=4 @')    
                IC_RES_VERSION = 4
             CASE ('VERSION=5 @')    
                IC_RES_VERSION = 5 
             CASE ('VERSION=6 @')      
                IC_RES_VERSION = 6
             CASE ('VERSION=7 @')    
                IC_RES_VERSION = 7
             CASE DEFAULT        ! something else
                ierr = 2
          END SELECT

       ELSE
          ierr = 1
       END IF
       CALL close_res_file( filename )
    END IF



    CALL parallel_broadcast_int( ierr )
   
    IF (ierr .NE. 0)  THEN
       SELECT CASE (ierr)   
          CASE (1)    
             IF (par_rank.EQ.0) WRITE (*,'("Cannot open the file: ",A)') TRIM(filename)
             CALL error ('Error while reading solution file')
          CASE (2)    
             IF (par_rank.EQ.0) WRITE (*,'("unsupported version: ",A)') TRIM(version_string)      
             IF (par_rank.EQ.0) PRINT *, 'Exiting ...'                                            
       END SELECT
       CALL parallel_finalize; STOP
    END IF

    CALL parallel_broadcast_int( IC_RES_VERSION )
 
    
  END SUBROUTINE read_solution_version


  
  
  !--------------------------------------------------------------------------------------------
  ! PUBLIC
  ! Read dimension in solution file. Used for restarting and in separate post_processing codes
  ! if VERB - print some output
  ! if PAR  - read domain decomposition related variables
  SUBROUTINE read_solution_dim( filename, VERBLEVEL, EXACT, IGNOREINIT, PARTREAD, CLEAN_IC_DD )
    USE io_3D_vars   ! IC_RES_VERSION
    USE parallel
    IMPLICIT NONE
    CHARACTER (LEN=*) , INTENT(IN) ::  filename              ! default values:
    LOGICAL, OPTIONAL, INTENT(IN) :: EXACT                   ! .FALSE.
    LOGICAL, OPTIONAL, INTENT(IN) :: IGNOREINIT              ! .FALSE.
    INTEGER, OPTIONAL, INTENT(IN) :: VERBLEVEL               ! .FALSE.
    INTEGER, OPTIONAL, INTENT(IN) :: PARTREAD                ! 1 - dim, 2 - partition
    LOGICAL, optiONAL, INTENT(IN) :: CLEAN_IC_DD             ! deallocate IC_par_proc_tree() and IC_Nwlt_per_Tree()
    INTEGER :: ierr, len_uvars, do_partread, do_verblevel
    REAL (pr), DIMENSION(:), POINTER :: scl
    LOGICAL :: do_exact, do_ignoreinit, do_clean_ic_dd
   
    NULLIFY(scl)
 
    do_verblevel    = 0
    do_exact        = .FALSE.  ! make assumptions as if it is a restart
    do_ignoreinit   = .FALSE.  ! check for errors and compare with .inp data
    do_partread     = 1        ! 0 - read all the file, 1 - dim, 2 - partition
    do_clean_ic_dd  = .TRUE.   ! deallocate IC_par_proc_tree() and IC_Nwlt_per_tree while reading .res files
    IF (PRESENT(VERBLEVEL))    do_verblevel = io_verb_flag (VERBLEVEL)
    IF (PRESENT(EXACT))        do_exact = EXACT
    IF (PRESENT(IGNOREINIT))   do_ignoreinit = IGNOREINIT
    IF (PRESENT(PARTREAD))     do_partread = PARTREAD
    IF (PRESENT(CLEAN_IC_DD))  do_clean_ic_dd = CLEAN_IC_DD

    ierr = 0
    
    IF ( (do_verblevel.GE.1.AND.par_rank.EQ.0).OR.(do_verblevel.GE.2) ) &
         WRITE (*,'("Reading Solution file dimension:",A)') TRIM (filename)
    
    ! read .res file version 
    CALL read_solution_version( filename )

    IF ((IC_RES_VERSION .EQ. 5).OR.(IC_RES_VERSION .EQ. 7)) THEN
       ! read common file only at processor 0 and broadcast
       CALL read_solution_aux_2common( &
            scl, &
            TRIM( filename( 1:INDEX(filename(1:LEN_TRIM(filename)-4),'.',BACK=.TRUE.)-1 )//'.com.res' ), &
            .FALSE., len_uvars, ierr, do_verblevel, EXACT=do_exact,  &
            IGNOREINIT=do_ignoreinit, CLEAN_IC_DD=do_clean_ic_dd, PART_READ=do_partread)
    ELSE
       
       ! try reading in 02/01/2007 .res endian independent format
       ! at each processor
       CALL read_solution_aux_1( scl, filename, .FALSE., len_uvars, ierr, do_verblevel, &
            PART_READ=do_partread, EXACT=do_exact, IGNOREINIT=do_ignoreinit )
    END IF
    
    
    CALL parallel_global_sum( INTEGER=ierr )
    IF (ierr.EQ.0 ) RETURN
    
    ! warn users
    CALL serious_warning_message
    
    ! try reading old 01/31/2007 .res format
    CALL read_solution_dim_aux_0( filename )
  END SUBROUTINE read_solution_dim
!!$
!!$    CALL parallel_broadcast_int( ierr )
!!$   
!!$    IF (ierr .NE. 0)  THEN
!!$       SELECT CASE (ierr)   
!!$          CASE (1)    
!!$             IF (par_rank.EQ.0) WRITE (*,'("Cannot open the file: ",A)') TRIM(filename)
!!$             CALL error ('Error while reading solution file')
!!$          CASE (2)    
!!$             IF (par_rank.EQ.0) WRITE (*,'("unsupported version: ",A)') TRIM(version_string)      
!!$             IF (par_rank.EQ.0) PRINT *, 'Exiting ...'                                            
!!$       END SELECT
!!$       CALL parallel_finalize; STOP
!!$    END IF

  !--------------------------------------------------------------------------------------------
  ! PRIVATE
  ! obsolete 01/31/2007 format, do NOT edit, for backward compatibility only
  SUBROUTINE read_solution_dim_aux_0( filename )
    USE PRECISION
    USE wlt_vars           ! dim
    USE debug_vars         ! test_alloc
    IMPLICIT NONE
    CHARACTER (LEN=*) , INTENT(IN) ::  filename
    CHARACTER , DIMENSION(:), ALLOCATABLE  :: header2
    LOGICAL   ::  form
    INTEGER   ::  hdr_len !length of actual header in the file
    INTEGER   ::  version ! file format version number
    INTEGER :: alloc_stat

    !read in header and see if this file is formatted or unformatted.
    OPEN (10, FILE = filename, FORM='formatted', STATUS='old')

    !-- Read in the ascii header
    CALL read_solution_hdr(10,form, version, hdr_len)

    IF( form) THEN
       READ (10,'(1(I8,1X))') dim
    ELSE
       CLOSE(10)

       OPEN (10, FILE = filename, FORM='unformatted', STATUS='old')

       ! This is an unformatted file so the record started after the 4 byte
       ! record header
       hdr_len = hdr_len -4
       PRINT *,'READING Data file header, hdr_len = ', hdr_len
       ALLOCATE(header2(hdr_len), STAT=alloc_stat)
       CALL test_alloc( alloc_stat, 'header2 in read_solution_dim_aux_0', hdr_len )
       READ (10) header2
       READ (10) dim
       DEALLOCATE (header2)
    END IF
    CLOSE(10)
  END SUBROUTINE read_solution_dim_aux_0


  !--------------------------------------------------------------------------------------------
  ! PRIVATE
  ! Read in solution header.
  ! obsolete 01/31/2007 format, do NOT edit, for backward compatibility only
  SUBROUTINE read_solution_hdr(funit, form, version, hdr_len)
    USE PRECISION
    USE pde
    USE io_3D_vars
    IMPLICIT NONE
    INTEGER   :: funit !inout file unit
    LOGICAL   , INTENT(INOUT) ::  form
    INTEGER   , INTENT(OUT)   :: version  ! file format version # of solution file
    CHARACTER (LEN=hdr_buf_len) :: header ! file header with ascii info
    INTEGER   ::  hdr_len                 ! length of actual header in the file

    ! read in ascii header
    READ (funit,'(A)') header
    IF(INDEX(header,'FORMAT=UNFORMATTED') /= 0  )THEN
       form = .FALSE.
    ELSEIF(INDEX(header,'FORMAT=FORMATTED') /= 0  )THEN
       form = .TRUE.
    ELSE
       PRINT*,'Corrupt data file header, The header should have one of the '
       PRINT*,'following two strings at the beginning:'
       PRINT*,' ''FORMAT=UNFORMATTED'' or ''FORMAT=FORMATTED'' '
       PRINT*,'Exiting...'
       STOP 1
    END IF

    !
    ! Find version in header
    version = 0
!!$  IF(INDEX(header,'VERSION=') /= 0  ) THEN
!!$     IF( INDEX(header,'VERSION=1') /= 0 ) THEN
!!$		version = 1
!!$      ELSE
!!$        PRINT *,'unknown version # in Solution file'
!!$        STOP 1
!!$	 END IF
!!$  ELSE
!!$     PRINT*,'Corrupt data file header, no version # in Solution file'
!!$     !STOP 1
!!$  END IF

    hdr_len =  0
    PRINT *,'Data file header: '
    IF( INDEX(header,'@') /= 0 ) THEN
       !multiline header, keep reading until we hit a line with the @
       DO WHILE ( INDEX(header,'@') == 0 )
          IF( .NOT. form  .AND. hdr_len == 0 ) THEN
             ! do not print record header
             PRINT *, header(5:hdr_buf_len)
          ELSE
             PRINT *, header
          END IF
          hdr_len = hdr_len + hdr_buf_len ! XXXX
          READ (funit,'(A)') header
       END DO
    END IF
    hdr_len = hdr_len + INDEX(header,'@')

    IF( .NOT. form ) THEN
       !print from record header  to @ sign
       PRINT *, header(5:INDEX(header,'@'))
    ELSE
       PRINT *, header(1:INDEX(header,'@'))
    END IF

  END SUBROUTINE read_solution_hdr


  !--------------------------------------------------------------------------------------------
  ! Read input from "case_name"_wlt.inp file
  ! case_name is in string file_name read from command line
  ! in read_command_line_input()
  !
  SUBROUTINE read_input(eps_init_loc,eps_run_loc,eps_adapt_steps_loc, &
       scl_fltwt, &
       cflmax_loc,cflmin_loc,dtwrite,  &
       j_mn_loc, j_mx_loc, &
       j_filt, ij_adj, adj_type, VERB )
    USE PRECISION
    USE io_3D_vars
    USE wlt_vars
    USE io_3D_vars
    USE pde
    USE share_kry
    USE elliptic_vars
    USE debug_vars                    ! test_alloc, error, verb_level, debug_level
    USE additional_nodes
    USE penalization
    USE input_file_reader
    USE wavelet_filters_vars
    USE parallel                      ! par_size
    USE distance 
    USE error_handling
    IMPLICIT NONE
    REAL (pr) , INTENT (INOUT) :: eps_init_loc,eps_run_loc,scl_fltwt, cflmax_loc,cflmin_loc,dtwrite
    INTEGER   , INTENT (INOUT) :: eps_adapt_steps_loc 
    INTEGER   , INTENT (INOUT) :: j_mn_loc, j_mx_loc, j_filt
    !INTEGER   , INTENT (INOUT) :: IC_file_fmt
    INTEGER   , INTENT (INOUT), DIMENSION(-1:1) :: ij_adj, adj_type
    LOGICAL, INTENT(IN), OPTIONAL :: VERB
    LOGICAL :: verb_, allow_dir_creation
    INTEGER :: alloc_stat, io_status, tmp_int, seqrun_status
    INTEGER :: i
    CHARACTER (LEN=4) :: tmp_str                             
    REAL (pr) :: IMEXgamma, IMEXdelta, xhi, xlo, xnu, fhi, flo, fnu, curerr
    
100 FORMAT (D16.7,T26, A) !format to echo real data
101 FORMAT (I10,T26, A) !format to echo integer data
102 FORMAT (L3,T26, A) !format to echo logical data
103 FORMAT (A,'  ', A) !format to echo logical data
    
    
    ! set default verbouse level                         ! normaly:
    verb_ = .FALSE.                                      ! nothing at others,
    IF (PRESENT(VERB)) verb_ = VERB                      ! print at processor 0
    
    
    IF ( verb_ ) THEN
       WRITE (*,'("Reading input file:",A)') TRIM(file_name)
       WRITE (*,'("********************************************************************")')
    END IF
    
    ! initialize general file reader
    ! (set output on processor 0 only if verb_>0)
    call start_reader ( TRIM(file_name), VERB=verb_ )
    
    
    call input_string ('file_gen',file_gen,'stop', '  base file name')
        
    ! set the directory to write results into,
    ! remove spaces around and add slash (/ as defined in io_3D_vars)
#ifdef USE_USER_CLI
    res_path = '.'//slash
#else
    res_path = 'results'//slash
#endif
    call input_string ('results_dir',res_path,'default')
    res_path = TRIM(ADJUSTL(res_path))
    IF (res_path(LEN_TRIM(res_path):LEN_TRIM(res_path)) .NE. slash) res_path = TRIM(ADJUSTL(res_path))//slash
    
    
    ! create the directory to write results into
    ! (system dependent, use with caution)
    !  we may need to use preprocessor here if Fortran's SYSTEM is not supported
    allow_dir_creation = .FALSE.
    call input_logical ('results_dir_create',allow_dir_creation,'default')
    IF (allow_dir_creation) CALL directory_create( TRIM(res_path), RANK_ZERO_ONLY=.TRUE. )
    
    
    
    ! allow creation of subdirectories for each timestep .res files
    ! (system dependent, use with caution)
    allow_subdir_timestep = .FALSE.
    call input_logical ('results_dir_timestep',allow_subdir_timestep,'default')
    
    ! set the directory to write each timestep into (default - the same)
    res_path_timestep = TRIM(ADJUSTL(res_path))
    
    
    call input_integer ('dimension',dim,'stop', &
         '  dim (2,3), # of dimensions')
    

    !--------------------------- WARNING ------------------- 
    ! for transition to IC_restart_mode
    IC_restart_mode = -1 !not activated value
    call input_integer ('IC_restart_mode',IC_restart_mode,'default', & !'stop', &
         '  0-new run; 1-hard restart; 2-soft restart; 3-restart from IC')


    IC_restart = .FALSE.   ! for backward compatibility only
    IC_from_file = .FALSE. ! 


    IF (IC_restart_mode.EQ.-1) THEN ! IC_restart_mode is not used in .inp file
       call input_logical ('IC_restart',IC_restart,'default', &
            '  T or F, restart a previous run. (multually exclusive with IC_from_file)')
       call input_logical ('IC_from_file',IC_from_file,'default', &
            '  T read from IC file, F use built in IC, IC_restart determines if we will restart from it.')
    ELSE
       IF (par_rank.EQ.0) THEN
          PRINT *, ' '
          PRINT *, 'Thank you for using IC_restart_mode instead of IC_restart/IC_from_file'
          PRINT *, ' '
       END IF
       SELECT CASE (IC_restart_mode)
       CASE (1:2)                          ! hard/soft restart
          IC_restart = .TRUE.
       CASE (3)                            ! restart from IC
          IC_from_file = .TRUE.
       END SELECT
    END IF

   !--------------------------- WARNING -------------------    
    
    IC_file_fmt = 0
    IC_adapt_grid = .TRUE.
    IC_SINGLE_LOOP = .FALSE.
    IC_filename = ''
    IC_restart_station = 0
    IF (IC_restart.OR.IC_from_file) THEN
       ! IC_restart and IC_from_file are multually exclusive
       IF (IC_restart.AND.IC_from_file) CALL error ('IC_restart and IC_from_file are multually exclusive')
       
       call input_integer ('IC_file_fmt',IC_file_fmt,'stop', &
            ' IC file format (0 - native restart file format, 1-netcdf, 2-A.Wray in fourier space, 3-simple binary) next line is IC filename')
       
       call input_logical ('IC_adapt_grid',IC_adapt_grid,'default', &
            ' IC_adapt_grid: if .FALSE., no grid addaptation is perfromed, activated if IC_from_File = .T. and IC_file_fmt = 0')
       
       call input_logical ('IC_SINGLE_LOOP',IC_SINGLE_LOOP,'default', &
            ' IC_SINGLE_LOOP: if .TRUE., adapts grid on initial conditions once')

       call input_string ('IC_filename',IC_filename,'stop', 'name of the file to restart with')
       IC_filename = ADJUSTL(IC_filename)
       
       call input_integer ('IC_restart_station',IC_restart_station,'stop', &
            ' IC_restart_station, after restart the next station number to use')
    END IF

    !
    !do_Sequential_run
    !
    do_Sequential_run = .FALSE.
    call input_logical ('do_Sequential_run', do_Sequential_run, 'default', ' do_Sequential_run')
    IF (do_Sequential_run) THEN
       seqrun_status = 0
       IF (par_rank.EQ.0) THEN
          OPEN  (UNIT=UNIT_SEQUENTIAL_RUN, FILE = TRIM(file_gen)//'sequentialrun.log', FORM='formatted', STATUS='old', IOSTAT=io_status)
          IF( io_status == 0 ) THEN
             READ (UNIT_SEQUENTIAL_RUN, FMT='(I12)')  tmp_int
             IF (tmp_int .LE. IC_restart_station) THEN
                seqrun_status = 1
             ELSE
                seqrun_status = 2
             END IF
          ELSE
             seqrun_status = 3
          END IF
          CLOSE (UNIT_SEQUENTIAL_RUN)
       END IF
       
       CALL parallel_broadcast_int( seqrun_status )    
       SELECT CASE (seqrun_status)
       CASE (1)   
          CALL error ('iwrite in  .sequentialrun.log   <=   IC_restart_station in .inp file')
       CASE (2)   
          CALL parallel_broadcast_int( tmp_int )    
          IC_restart_station = tmp_int+1
          IC_restart_mode    = 1
          IC_restart         = .TRUE.
          IC_from_file       = .FALSE.
          IC_adapt_grid      = .FALSE.             
          !tmp_int = tmp_int-1
          tmp_str =    char(ichar('0') + mod(tmp_int/1000,10)) &
                    // char(ichar('0') + mod(tmp_int/100,10))  &
                    // char(ichar('0') + mod(tmp_int/10,10))   &
                    // char(ichar('0') + mod(tmp_int,10)) 
          
          IF ( allow_subdir_timestep ) res_path_timestep =  TRIM(res_path)//slash//TRIM(ADJUSTL(tmp_str))//slash
          
          IF (par_size.GT.1) THEN
             IC_filename = TRIM(res_path_timestep)//TRIM(file_gen)//TRIM(ADJUSTL(tmp_str))//'.p0.res'
          ELSE
             IC_filename = TRIM(res_path_timestep)//TRIM(file_gen)//TRIM(ADJUSTL(tmp_str))//'.res'
          END IF
       END SELECT
    END IF
  
    ! datafile_formatted is obsolete
    datafile_formatted = .FALSE.
    call input_logical ('Data_file_format',datafile_formatted,'default', &
         '  T-formatted data file, F- unformatted data file')


    call input_real_vector ('coord_min',xyzlimits(1,:),dim,'stop', &
         '  XMIN, YMIN, ZMIN, etc')
    call input_real_vector ('coord_max',xyzlimits(2,:),dim,'stop', &
         '  XMAX, YMAX, ZMAX, etc')

    call input_real ('eps_init',eps_init_loc,'stop', &
         '  EPS used to adapt initial grid')

    call input_real ('eps_run',eps_run_loc,'stop', &
         '  EPS used in run')

    eps_expl = eps_run_loc 
    call input_real ('eps_expl',eps_expl,'default', &
         '  EPSexpl used in explicit wavelet threshold filteirng')
    
    IF ( (par_rank.EQ.0) .AND. (IC_restart_mode.GT.0) ) THEN
       WRITE(*,'("*************************************************************************")')
       WRITE(*,'("*                                                                       *")')
       WRITE(*,'("* !!! WARNING !!!                                                       *")')
       WRITE(*,'("* EPS is read from *.inp file and is reset to:                          *")')
       WRITE(*,'("* eps_init =",E12.5,", eps_run =",E12.5,", eps_expl =",E12.5," *")') &
            eps_init_loc, eps_run_loc, eps_expl
       WRITE(*,'("*                                                                       *")')
       WRITE(*,'("*************************************************************************")')
       CALL error (IW=ignore_warnings, F=.TRUE.) ! forced ignore of the warning
    END IF
    
    call input_integer ('eps_adapt_steps',eps_adapt_steps_loc,'stop', &
         ' how many time steps to adapt from eps_init to eps_run')
    
    call input_integer ('Scale_Meth',Scale_Meth,'stop', &
         ' Scale_Meth !1- Linf, 2-L2')

    Weights_Meth = 1 ! normal method
    call input_integer ('Weights_Meth',Weights_Meth,'default', &
         ' Weights_Meth ! Weights_Meth, 0: dA=area/nwlt, 1: normal')

    call input_real ('scl_fltwt',scl_fltwt,'stop', &
         ' scl temporal filter weight, scl_new = scl_fltwt*scl_old + ( 1-scl_fltwt)*scl_new')

    !  set in user_setup_pde for now because we  do not know n_var at this point
    !  call input_real_vector ('scaleCoeff',scaleCoeff, n_var, 'stop', &
    !       ' Scale coefficient, scl(1:n_var) = scaleCoeff(1:n_var) * scl(1:n_var) ')

    call input_integer ('j_mn_init',j_mn_init,'stop', &
         ' J_mn_init, force J_mn == J_INIT while adapting to inital conditions')


    call input_integer ('j_lev_init',j_lev_init,'stop', &
         ' j_lev_init, force j_lev == j_lev_init when initializing')

    call input_integer ('J_MN',j_mn_loc,'stop', &
         ' J_MN')
         j_mn_evol = j_mn ! set j_mn_evol to j_mn value, since j_mn value will be reassigned doring IC

    call input_integer ('J_MX',j_mx_loc,'stop', &
         ' J_MX')

    call input_integer ('J_FILT',j_filt,'stop', &
         ' J_FILT')

    j_tree_root = j_mn_loc
    call input_integer ('J_TREE_ROOT',j_tree_root,'default', &
         ' optional level of tree roots for DB_tree')
    
    j_IC = j_mx_loc
    call input_integer ('j_IC',j_IC,'default', &
         ' J_IC if the IC data does not have dimensions in it then mxyz(:)*2^(j_IC-1) is used')
    
    j_zn = j_mn
    call input_integer ('j_zn',j_zn,'default', &
         ' j_zn, set level of resoluiton outised the zone to j_zn')
    j_zn = MIN(j_zn, j_mx_loc)  !sanity check
    xyzzone(1,:) = -1.0e12 ! default zone outside of which only j_zn points are kept
    xyzzone(2,:) =  1.0e12
    call input_real_vector ('coord_zone_min',xyzzone(1,:),dim,'default', &
         '  XMINzone, etc, Resolution is fized OUTSIDE the zone.')
    call input_real_vector ('coord_zone_max',xyzzone(2,:),dim,'default', &
         '  XMAXzone, etc, Resolution is fized OUTSIDE the zone.')
    
    j_lev_init=MAX(j_lev_init,j_zn-1,j_mn_init)
    
    IF(ANY(xyzzone(1,:) > xyzlimits(1,:)) .OR. ANY(xyzzone(2,:) < xyzlimits(2,:)) ) THEN
       IF (par_rank.EQ.0) THEN
          WRITE(*,'(" ***************************************************************************")')
          WRITE(*,'(" *  WARNING: wlt_zone will be enforced at j = j_mn outise of the wlt_zone  *")')
          IF(3*j_mn <= j_mx) THEN
!!$          WRITE(*,'(" *           j_mn = ",I2," is too low, restart the simulations with j_mn > ",I2,"  *")') &
!!$               j_mn, INT(j_mx/3)
!!$          WRITE(*,'(" ***************************************************************************")')
!!$          CALL parallel_finalize; STOP
          ELSE
             WRITE(*,'(" ***************************************************************************")')
          END IF
       END IF
       CALL error (IW=ignore_warnings, F=.TRUE.) ! forced ignore of the warning
    END IF
         
    !***********************************************************
    !* Add additional planes
    !***********************************************************
    additional_planes_active = .FALSE.
    call input_logical ('additional_planes_active',additional_planes_active,'default', &
         ' additional_planes_active: if T adds nodes for additional user specified planes and activates the module')

    IF(additional_planes_active) THEN

       call input_integer ('j_additional_planes',j_additional_planes,'stop', &
         ' j_additional_planes - level at which points on the planes are added')
       j_additional_planes = MIN(j_additional_planes,j_mx)
       
       call input_integer ('n_additional_planes',n_additional_planes,'stop', &
         ' n_additional_planes - number of additional planes to be added')

       ALLOCATE(xyz_planes(1:dim,1:n_additional_planes),dir_planes(1:n_additional_planes))

       call input_real_vector ('x_planes',xyz_planes(1,:), n_additional_planes,'stop', &
         '  x-coordinates for additional planes')

       IF(dim>1) call input_real_vector ('y_planes',xyz_planes(2,:), n_additional_planes,'stop', &
         '  y-coordinates for additional planes')

       IF(dim>2) call input_real_vector ('z_planes',xyz_planes(3,:), n_additional_planes,'stop', &
         '  z-coordinates for additional planes')
       
       call input_integer_vector ('dir_planes',dir_planes,n_additional_planes,'stop', &
         '  directions of additional planes')

       DO i=1,n_additional_planes
          IF(dir_planes(i) > dim) THEN
             IF (par_rank.EQ.0) WRITE(*,'("ERROR: dir_planes > dim")')
             STOP
          END IF
       END DO
    END IF

    !***********************************************************
    !* Add additional lines
    !***********************************************************
    additional_lines_active = .FALSE.
    call input_logical ('additional_lines_active',additional_lines_active,'default', &
         ' additional_lines_active: if T adds nodes for additional user specified line and activates the module')

    IF(additional_lines_active) THEN

       call input_integer ('j_additional_lines',j_additional_lines,'stop', &
         ' j_additional_lines - level at which points on the lines are added')
       j_additional_lines = MIN(j_additional_lines,j_mx)
       
       call input_integer ('n_additional_lines',n_additional_lines,'stop', &
         ' n_additional_lines - number of additional lines to be added')

       ALLOCATE(xyz_lines(1:dim,1:n_additional_lines),dir_lines(1:n_additional_lines))

       call input_real_vector ('x_lines',xyz_lines(1,:), n_additional_lines,'stop', &
         '  x-coordinates (intersect) for additional lines')

       IF(dim>1) call input_real_vector ('y_lines',xyz_lines(2,:), n_additional_lines,'stop', &
         '  y-coordinates (intersect) for additional lines')

       IF(dim>2) call input_real_vector ('z_lines',xyz_lines(3,:), n_additional_lines,'stop', &
         '  z-coordinates (intersect) for additional lines')
       
       call input_integer_vector ('dir_lines',dir_lines,n_additional_lines,'stop', &
         '  directions of additional lines')

       DO i=1,n_additional_lines
          IF(dir_lines(i) > dim) THEN
             IF (par_rank.EQ.0) WRITE(*,'("ERROR: dir_lines > dim")')
             STOP
          END IF
       END DO
    END IF

    !***********************************************************
    !* Add additional points
    !***********************************************************
    additional_points_active = .FALSE.
    call input_logical ('additional_points_active',additional_points_active,'default', &
         ' additional_points_active: if T adds nodes for additional user specified points and activates the module')

    IF(additional_points_active) THEN

       call input_integer ('j_additional_points',j_additional_points,'stop', &
         ' j_additional_points - level at which points are added')
       j_additional_points = MIN(j_additional_points,j_mx)
       
       call input_integer ('n_additional_points',n_additional_points,'stop', &
         ' n_additional_points - number of additional points to be added')

       ALLOCATE(xyz_points(1:dim,1:n_additional_points))

       call input_real_vector ('x_points',xyz_points(1,:), n_additional_points,'stop', &
         '  x-coordinates (intersect) for additional points')

       IF(dim>1) call input_real_vector ('y_points',xyz_points(2,:), n_additional_points,'stop', &
         '  y-coordinates (intersect) for additional points')

       IF(dim>2) call input_real_vector ('z_points',xyz_points(3,:), n_additional_points,'stop', &
         '  z-coordinates (intersect) for additional points')
       
    END IF

    call input_integer_vector ('M_vector',mxyz,dim,'stop', &
         '  Mx, etc')

    call input_integer_vector ('periodic',prd,dim,'stop', &
         '  prd(:) (0/1) 0: non-periodic; 1: periodic')

    call input_integer_vector ('uniform',grid,dim,'stop', &
         '  grid(:) (0/1) 0: uniform; 1: non-uniform')

    call input_integer ('i_h',i_h_inp,'stop', &
         ' order of boundaries (1-xmin,2-xmax,3-ymin,4-ymax,5-zmin,6-zmax)')

    call input_integer ('i_l',i_l_inp,'stop', &
         ' algebraic/evolution (1/0) BC order: (lrbt)')

    call input_integer ('N_predict',n_prdct(1),'stop', &
         ' N_predict High_order')

    n_prdct(0) = n_prdct(1) ! default low order interpolation
    ! set to match high order
    call input_integer ('N_predict_low_order',n_prdct(0),'default', &
         ' N_predict Low_order (default is to match N_predict High_orde) ')

    maxval_n_prdct = maxval(n_prdct)


    call input_integer ('N_update',n_updt(1),'stop', &
         ' N_update')

    n_updt(0) = n_updt(1) ! default low order interpolation
    ! set to match high order

    call input_integer ('N_update_low_order',n_updt(0),'default', &
         ' N_update Low_order (default is to match N_update High_orde) ')

    ! --- WARNING ---
    ! n_updt transformation is required if we want
    ! to read data with nonzero one in parallel code
    CALL set_zero_n_updt_for_multiproc
    
    maxval_n_updt = maxval(n_updt)

    call input_integer ('N_diff',n_diff,'stop', &
         ' N_diff')

    call input_integer_vector ('IJ_ADJ',ij_adj(-1:1),3,'stop', &
         ' IJ_ADJ(-1:1) = (coarser level), (same level), (finer level)')

    call input_integer_vector ('ADJ_type',adj_type(-1:1),3,'stop', &
         '  ADJ_type(-1:1) = (coarser level), (same level), (finer level) # (0 - less conservative, 1 - more conservative)')

    dosymm(:) = 0 
    call input_integer_vector ('dosymm',dosymm(1:3),3,'default', &
         '  dosymm(1:3) => set to 1 to enforce symmetric grid in each direction')

    call input_logical ('BNDzone',BNDzone,'stop', &
         ' BNDzone')

    call input_integer ('time_integration_method',time_integration_method, 'stop', &
         '  0=meth2, 1=krylov, 2=Crank Nicolson, 3=RK, 4=IMEX ')

    RKtype = 3
    IMEXtype = 7
    call input_integer ('RKtype',RKtype, 'default', '  1=RK2, 2=RK2TVD, 3=RK3TVD, 4=RK4 ')
    call input_integer ('IMEXtype',IMEXtype, 'default', &
         '  1=(111)L, 2=(121)L, 3=(122)A(1.0), 4=(233)A(0.732), 5=(232)L, 6=(222)L, 7=(343)L, 8=(443)L, 9=(122)A(1.0)TVD, 10=(232)A(0.5)TVD, 11=(332)LTVD')
            ! (srp) => s=# of implicit steps, r=# of explicit steps, p=order of IMEX method
            ! A = A-stable method (R_inf follows in parentheses); L = L-stable method (R_inf = 0)
            ! TVD = explicit scheme is TVD
    aex = 0.0_pr; aim = 0.0_pr; bex = 0.0_pr; bim = 0.0_pr
    IF (IMEXtype == 1) THEN
       imsteps = 1
       exsteps = 1
       aim(1,1) = 1.0_pr
       aex(1,1) = 1.0_pr
       bim(1)   = aim(1,1)
       bex(1)   = aex(1,1)
    ELSEIF (IMEXtype == 2) THEN
       imsteps = 1
       exsteps = 2
       aim(1,1) = 1.0_pr
       aex(1,1) = 1.0_pr
       bim(1)   = aim(1,1)
       bex(1)   = 0.0_pr
       bex(2)   = bim(1)
    ELSEIF (IMEXtype == 3) THEN
       imsteps = 1
       exsteps = 2
       aim(1,1) = 1.0_pr/2.0_pr
       aex(1,1) = 1.0_pr/2.0_pr
       bim(1)   = 1.0_pr
       bex(1)   = 0.0_pr
       bex(2)   = bim(1)
    ELSEIF (IMEXtype == 4) THEN
       IMEXgamma = (3.0_pr+SQRT(3.0_pr))/6.0_pr
       imsteps = 2
       exsteps = 3
       aim(1,1) = IMEXgamma
       aim(2,1) = 1.0_pr-2.0_pr*IMEXgamma
       aim(2,2) = IMEXgamma
       aex(1,1) = IMEXgamma
       aex(2,1) = IMEXgamma-1.0_pr
       aex(2,2) = 2.0_pr*(1.0_pr-IMEXgamma)
       bim(1:2) = 1.0_pr/2.0_pr
       bex(1)   = 0.0_pr
       bex(2:3) = bim(1:2)
    ELSEIF (IMEXtype == 5) THEN
       IMEXgamma = (2.0_pr-SQRT(2.0_pr))/2.0_pr
       IMEXdelta = -2.0_pr*SQRT(2.0_pr)/3.0_pr
       imsteps = 2
       exsteps = 3
       aim(1,1) = IMEXgamma
       aim(2,1) = 1.0_pr-IMEXgamma
       aim(2,2) = IMEXgamma
       aex(1,1) = IMEXgamma
       aex(2,1) = IMEXdelta
       aex(2,2) = 1.0_pr-IMEXdelta
       bim(1:2) = aim(2,1:2)
       bex(1)   = 0.0_pr
       bex(2:3) = bim(1:2)
    ELSEIF (IMEXtype == 6) THEN
       IMEXgamma = (2.0_pr-SQRT(2.0_pr))/2.0_pr
       IMEXdelta = 1.0_pr-1.0_pr/2.0_pr/IMEXgamma
       imsteps = 2
       exsteps = 2
       aim(1,1) = IMEXgamma
       aim(2,1) = 1.0_pr-IMEXgamma
       aim(2,2) = IMEXgamma
       aex(1,1) = IMEXgamma
       aex(2,1) = IMEXdelta
       aex(2,2) = 1.0_pr-IMEXdelta
       bim(1:2) = aim(2,1:2)
       bex(1:2) = aex(2,1:2)
    ELSEIF (IMEXtype == 7) THEN
       xlo = 1.0_pr-SQRT(2.0_pr)/2.0_pr
       xhi = 1.0_pr+SQRT(2.0_pr)/2.0_pr
       flo = 6.0_pr*xlo**3.0_pr-18.0_pr*xlo**2.0_pr+9.0_pr*xlo-1.0_pr
       fhi = 6.0_pr*xhi**3.0_pr-18.0_pr*xhi**2.0_pr+9.0_pr*xhi-1.0_pr
       curerr=1.0_pr
       DO WHILE (curerr>1.0e-12_pr)
          xnu=xlo-flo*(xhi-xlo)/(fhi-flo);
          fnu = 6.0_pr*xnu**3.0_pr-18.0_pr*xnu**2.0_pr+9.0_pr*xnu-1.0_pr
          IF (fnu>0.0_pr) THEN
             curerr=ABS(xnu-xlo) 
             xlo=xnu
             flo=fnu
          ELSE
             curerr=ABS(xnu-xhi) 
             xhi=xnu
             fhi=fnu
          END IF
       END DO
       IMEXgamma = xnu 
       imsteps = 3
       exsteps = 4
       aim(1,1) = IMEXgamma
       aim(2,1) = (1.0_pr-IMEXgamma)/2.0_pr
       aim(2,2) = IMEXgamma
       aim(3,1) = -3.0_pr/2.0_pr*IMEXgamma**2.0_pr+4.0_pr*IMEXgamma-1.0_pr/4.0_pr
       aim(3,2) =  3.0_pr/2.0_pr*IMEXgamma**2.0_pr-5.0_pr*IMEXgamma+5.0_pr/4.0_pr
       aim(3,3) = IMEXgamma
       aex(1,1) = IMEXgamma
       aex(3,2) = 1.0_pr/2.0_pr
       aex(3,3) = 1.0_pr/2.0_pr
       aex(3,1) = 1.0_pr - aex(3,2) - aex(3,3) 
       aex(2,1) = (1.0_pr-9.0_pr/2.0_pr*IMEXgamma+3.0_pr/2.0_pr*IMEXgamma**2.0_pr)*aex(3,2) + &
                  (11.0_pr/4.0_pr-21.0_pr/2.0_pr*IMEXgamma+15.0_pr/4.0_pr*IMEXgamma**2.0_pr)*aex(3,3) - &
                  7.0_pr/2.0_pr + 13.0_pr*IMEXgamma-9.0_pr/2.0_pr*IMEXgamma**2.0_pr
       aex(2,2) = -(1.0_pr-9.0_pr/2.0_pr*IMEXgamma+3.0_pr/2.0_pr*IMEXgamma**2)*aex(3,2) - &
                  (11.0_pr/4.0_pr-21.0_pr/2.0_pr*IMEXgamma+15.0_pr/4.0_pr*IMEXgamma**2.0_pr)*aex(3,3) + &
                  4.0_pr-25.0_pr/2.0_pr*IMEXgamma+9.0_pr/2.0_pr*IMEXgamma**2
       bim(1:3) = aim(3,1:3)
       bex(1)   = 0.0_pr
       bex(2:4) = bim(1:3)
    ELSEIF (IMEXtype == 8) THEN
       imsteps = 4
       exsteps = 4
       aim(1,1) = 1.0_pr/2.0_pr
       aim(2,1) = 1.0_pr/6.0_pr
       aim(2,2) = 1.0_pr/2.0_pr
       aim(3,1) =-1.0_pr/2.0_pr
       aim(3,2) = 1.0_pr/2.0_pr
       aim(3,3) = 1.0_pr/2.0_pr
       aim(4,1) = 3.0_pr/2.0_pr
       aim(4,2) =-3.0_pr/2.0_pr
       aim(4,3) = 1.0_pr/2.0_pr
       aim(4,4) = 1.0_pr/2.0_pr
       aex(1,1) = 1.0_pr/2.0_pr
       aex(2,1) =11.0_pr/18.0_pr
       aex(2,2) = 1.0_pr/18.0_pr
       aex(3,1) = 5.0_pr/6.0_pr
       aex(3,2) =-5.0_pr/6.0_pr
       aex(3,3) = 1.0_pr/2.0_pr
       aex(4,1) = 1.0_pr/4.0_pr
       aex(4,2) = 7.0_pr/4.0_pr
       aex(4,3) = 3.0_pr/4.0_pr
       aex(4,4) =-7.0_pr/4.0_pr
       bim(1:4) = aim(4,1:4)
       bex(1:4) = aex(4,1:4)
    ELSEIF (IMEXtype == 9) THEN
       imsteps = 2
       exsteps = 2
       aim(1,1) = 0.0_pr
       aim(2,1) = 1.0_pr/2.0_pr
       aim(2,2) = 1.0_pr/2.0_pr
       aex(1,1) = 0.0_pr
       aex(2,1) = 0.0_pr
       aex(2,2) = 1.0_pr
       bim(1:2) = aim(2,1:2)
       bex(1:2) = aex(2,1:2)
    ELSEIF (IMEXtype == 10) THEN
       imsteps = 2
       exsteps = 3
       aim(1,1) = 1.0_pr
       aim(2,1) = -1.0_pr/2.0_pr
       aim(2,2) = 1.0_pr
       aex(1,1) = 1.0_pr
       aex(2,1) = 1.0_pr/4.0_pr
       aex(2,2) = 1.0_pr/4.0_pr
       bim(1)   = 0.0_pr
       bim(2)   = 1.0_pr
       bex(1:2) = 1.0_pr/6.0_pr
       bex(3)   = 2.0_pr/3.0_pr 
    ELSEIF (IMEXtype == 11) THEN
       imsteps = 3
       exsteps = 3
       aim(1,1) = 1.0_pr
       aim(2,1) = -1.0_pr/2.0_pr
       aim(2,2) = 1.0_pr
       aim(3,1) = -1.0_pr
       aim(3,2) = 1.0_pr
       aim(3,3) = 1.0_pr
       aex(1,1) = 1.0_pr
       aex(2,1) = 1.0_pr/4.0_pr
       aex(2,2) = 1.0_pr/4.0_pr
       aex(3,1) = 1.0_pr/6.0_pr
       aex(3,2) = 1.0_pr/6.0_pr
       aex(3,3) = 2.0_pr/3.0_pr
       bim(1:3) = aim(3,1:3)
       bex(1:3) = aex(3,1:3)
    ELSE
       IF (par_rank.EQ.0) PRINT *, 'INVALID IMEXtype:', IMEXtype
       IF (par_rank.EQ.0) PRINT *, 'Exiting ...'                                            
       CALL parallel_finalize; STOP                 
    END IF

    call input_real ('t_begin',t_begin,'stop', &
         ' tbeg')

    call input_real ('t_end',t_end,'stop', &
         ' tend')

    !
    !IC_SINGLE_LOOP   for   do_Sequential_run=T & t_begin<t_end (Time Integration Problems)
    !
    IF ( do_Sequential_run .AND. (t_begin < t_end) ) THEN
       seqrun_status = 4
       IF (par_rank.EQ.0) THEN
          OPEN  (UNIT=UNIT_SEQUENTIAL_RUN, FILE = TRIM(file_gen)//'sequentialrun.log', FORM='formatted', STATUS='old', IOSTAT=io_status)
          IF( io_status == 0 )  seqrun_status = 5
          CLOSE (UNIT_SEQUENTIAL_RUN)
       END IF
       CALL parallel_broadcast_int( seqrun_status )    
       IF (seqrun_status .EQ. 5)  IC_SINGLE_LOOP = .TRUE.
    END IF

    call input_real ('dt',dt,'stop', &
         ' dt')
    dt_original = dt

    call input_real ('dtmax',dtmax,'stop', &
         ' dtmax')

    call input_real ('dtmin',dtmin,'stop', &
         ' dtmin-if dt < dtmin then exection stops(likely blowing up)')

    call input_real ('dtwrite',dtwrite,'stop', &
         ' dtwrite')

    call input_real ('t_adapt',t_adapt,'stop', &
         ' t_adapt, when t > t_adapt use an adaptive time step if possible')

    call input_real ('cflmax',cflmax_loc,'stop', &
         ' cflmax')

    call input_real ('cflmin',cflmin_loc,'stop', &
         ' cflmin')

    call input_logical ('Zero_Mean',zero_mean,'stop', &
         '  T- enforce zero mean for 1:dim first variables (velocity usually), F- do nothing')

    eta_chi = 0.0_pr ! default value of penalization parameter
    call input_real ('eta_chi',eta_chi,'default')

    call input_logical ('GMRESflag',GMRESflag,'stop', &
         ' GMRESflag')

    call input_logical ('BiCGSTABflag',BiCGSTABflag,'stop', &
         ' BiCGSTABflag ')

    call input_integer ('kry_p',kry_p,'stop', &
         ' kry_p')

    call input_integer ('kry_p_coarse',kry_p_coarse,'stop', &
         ' kry_p_coarse')

    call input_integer ('len_bicgstab',len_bicgstab,'stop', &
         ' len_bicgstab')

    call input_integer ('len_bicgstab_coarse',len_bicgstab_coarse,'stop', &
         ' len_bicgstab_coarse')

    call input_integer ('len_iter',len_iter,'stop', &
         ' len_iter ')

    call input_real ('W0',w0 ,'stop', &
         ' w0 !underrelaxation factor for inner V-cycle')

    call input_real ('W1',w1 ,'stop', &
         ' w1 !underrelaxation factor on the finest level')

    call input_real ('W2',w2 ,'stop', &
         ' w2 !underrelaxation factor for weighted Jacoby (inner points)')

    call input_real ('W3',w3 ,'stop', &
         ' w3 !underrelaxation factor for weighted Jacoby (boundary points)')

    w_GS = w2
    call input_real ('W_GS',w_GS ,'default', &
         ' w_GS !underrelaxation factor for the Gauss-Sidel irerative algorithm')

    w_penal = w2
    call input_real ('W_penal',w_penal ,'default', &
         ' w_pena !underrelaxation factor for weighted Jacoby (for penalized points)')

    call input_real ('W_min',w_min ,'stop', &
         ' w_min !correction factor')

    call input_real ('W_max',w_max ,'stop', &
         ' w_max !correction factor')

    call input_real ('tol1',tol1,'stop', &
         '  tol1    ! used to set tolerence for non-solenoidal half-step')

    call input_real ('tol2',tol2,'stop', &
         ' ! used to set tolerence for solenoidal half-step')

    call input_real ('tol3',tol3,'stop', &
         ' ! used to set tolerence for time step')

    call input_real ('tol_gmres',tol_gmres,'stop', &
         ' ! used to set tolerence for gmres iterative solver')

    call input_logical ('tol_gmres_stop_if_larger',tol_gmres_stop_if_larger,'stop', &
         ' If true stop iterating in gmres solver if error of last iteration was smaller ')  

    wlog = .FALSE.
    call input_logical ('wlog',wlog,'default', &
         ' wlog: If T log wavelet stats')

    ALLOCATE(elliptic_zone(2,dim))
    elliptic_zone = 0
    elliptic_zone_active = .FALSE.
    call input_integer_vector ('elliptic_zone_min',elliptic_zone(1,:),dim,'default', &
         '  elliptic_zone thickness on MIN boundaries')
    call input_integer_vector ('elliptic_zone_max',elliptic_zone(2,:),dim,'default', &
         '  elliptic_zone thickness on MAX boundaries')
    DO i = 1,2
       elliptic_zone(i,1:dim) = elliptic_zone(i,1:dim)*(1-prd(1:dim))
    END DO
    IF(ANY(MAXVAL(elliptic_zone,DIM=2) > 0)) elliptic_zone_active = .TRUE.

    elliptic_project = .FALSE.
    call input_logical ('elliptic_project',elliptic_project,'default', &
         ' if T, perform additional zero-mean projection every iteration ad on every level')


    GS_ACTIVE = .FALSE.
    call input_logical ('GS_ACTIVE',GS_ACTIVE,'default', &
         ' if T, use Gauss-Siedel iterations based on level of resolution instead of Jacoby iterations')

    set_to_zero_if_diverged = .FALSE.
    call input_logical ('set_to_zero_if_diverged',set_to_zero_if_diverged,'default', &
         'set_to_zero_if_diverged: T - set the solution of elliptic problem to zero if diverged, F - do not do anything')

    diagnostics_elliptic = .FALSE.  !default
    call input_logical ('diagnostics_elliptic',diagnostics_elliptic,'default', &
         ' diagnostics_elliptic: If T print full diagnostic for elliptic solver')

    display_Vcycle = .FALSE.
    call input_logical ('display_Vcycle',display_Vcycle,'default', &
         ' if T displays residual information within V-cycle')

    call input_logical ('Jacoby_correction',Jacoby_correction,'stop', &
         ' Jacoby_correction')

    call input_logical ('multigrid_correction',multigrid_correction,'stop', &
         ' multigrid_correction')

    call input_logical ('GMRES_correction',GMRES_correction,'stop', &
         ' GMRES_correction')

    !============== Inputs to control timing and debugging related output =========================
    flag_timing = .FALSE.
    call input_logical ('flag_timing',flag_timing,'default', &
         'flag_timing, if true extra timing calls are used.')

    debug_level = 0 ! no debugging statements
    call input_integer ('debug_level',debug_level,'default', &
         ' debug_level ! 0- no debugging output, 1- some debugging, 2- more debugging')

    debug_zero_allocated_mem = .FALSE. ! default to not explicitly zero allocated memory
    call input_logical ('debug_zero_allocated_mem',debug_zero_allocated_mem,'default', &
         'debug_zero_allocated_mem, if true allocated memory in some critical places is zeroed.')

    debug_c_diff_fast = 0 ! no debug printouts
    call input_integer ('debug_c_diff_fast',debug_c_diff_fast,'default', &
         'debug_c_diff_fast, 0- nothing,1- derivative MIN/MAXVALS,2- low level, are printed in c_diff_fast.')

    debug_force_wrk_wlt_order = .FALSE. ! db_tree will use its own ordering of wavelets
    call input_logical ('debug_force_wrk_wlt_order',debug_force_wrk_wlt_order,'default', &
         'sort db_tree wavelets in db_wrk order (slow, use for debugging only)')

    verb_level = 1 ! no screen output except for I/O subroutines
    call input_integer ('verb_level',verb_level,'default', &
         'bitwise flag to control screen output')
    
    ignore_warnings = .FALSE. ! warnings terminate the program
    call input_logical ('ignore_warnings',ignore_warnings,'default', &
         'flag to ignore non-fatal warnings, use with caution')

    !============================ additional nodes ========================================
    additional_nodes_active = .FALSE. ! default value
    call input_logical ('additional_nodes_active',additional_nodes_active,'default',' include additional points  (Y/N) -> (T/F)')
    IF(additional_nodes_active) THEN
       call input_integer ('j_additional_nodes',j_additional_nodes,'stop',' j_additional_nodes')
       call input_integer ('n_additional_patches',n_additional_patches,'stop',' n_additional_patches')
       
       ALLOCATE(x_additional_vec(n_additional_patches,1:dim), STAT=alloc_stat)
       CALL test_alloc( alloc_stat, 'x_additional_vec in read_input', n_additional_patches*dim )

       call input_real_vector ('x_patches', x_additional_vec(:,1),n_additional_patches,'stop',&
            ' x-coordinates for additioanl patches')
       call input_real_vector ('y_patches', x_additional_vec(:,2),n_additional_patches,'stop',&
            ' y-coordinates for additioanl patches')
    ELSE
       j_additional_nodes = 1
    END IF

    !======================================================================================
    !            READ filter definitions
    !======================================================================================

    ExplicitFilter = .FALSE.
    call input_logical ('ExplicitFilter', ExplicitFilter, 'default', &
         ' ExplicitFilter !apply grid filter as an explicit filter each time step')

    ExplicitFilterNL = .FALSE.
    call input_logical ('ExplicitFilterNL', ExplicitFilterNL, 'default', &
         ' ExplicitFilterNL !apply grid filter as an explicit filter to the non-linear term')

    mdl_filt_type_grid = 0
    call input_integer ('mdl_filt_type_grid', mdl_filt_type_grid, 'default', &
         ' mdl_filt_type_grid ! dyn mdl grid filter (defined in make_mdl_filts() )')

    mdl_filt_type_test = 1
    call input_integer ('mdl_filt_type_test', mdl_filt_type_test, 'default', &
         ' mdl_filt_type_test ! dyn mdl test filter(defined in make_mdl_filts() )')

    lowpass_filt_type = 0
    call input_integer ('lowpass_filt_type', lowpass_filt_type, 'default', &
         ' lowpass_filt_type: 0 - volume averged, 1 - trapezoidal ')

    tensorial_filt = .FALSE.
    call input_logical ('tensorial_filt',tensorial_filt,'default', &
         ' tensorial_filt: if .TRUE. uses tensorial filter for comparison with lines')

    lowpass_filt_type_GRID = 0
    call input_integer ('lowpass_filt_type_GRID', lowpass_filt_type_GRID, 'default', &
         ' lowpass_filt_type_GRID - filter type for grid-filter level: 0 - volume averged, 1 - trapezoidal ')

    lowpass_filt_type_TEST = 0
    call input_integer ('lowpass_filt_type_TEST', lowpass_filt_type_TEST, 'default', &
         ' lowpass_filt_type_TEST -  filter type for test-filter level: 0 - volume averged, 1 - trapezoidal ')

    lowpassfilt_support = 1
    call input_integer ('lowpassfilt_support', lowpassfilt_support, 'default', &
         ' lowpassfilt_support: -lowpassfilt_support:lowpassfilt_support')
    IF(lowpass_filt_type == 1) lowpassfilt_support = MIN(lowpassfilt_support,1) ! trapezoidal filter

    lowpassfilt_support_GRID = 0
    call input_integer ('lowpassfilt_support_GRID', lowpassfilt_support_GRID, 'default', &
         ' lowpassfilt_support_GRID: filter support for grid-filter level')
    IF(lowpass_filt_type_GRID == 1) lowpassfilt_support_GRID = MIN(lowpassfilt_support_GRID,1) ! trapezoidal filter

    lowpassfilt_support_TEST = 1
    call input_integer ('lowpassfilt_support_TEST', lowpassfilt_support_TEST, 'default', &
         ' lowpassfilt_support_TEST: filter support for test-filter level')
    IF(lowpass_filt_type_TEST == 1) lowpassfilt_support_TEST = MIN(lowpassfilt_support_TEST,1) ! trapezoidal filter

    !======================================================================================

    sgsmodel = 0 !no SGS force by default
    call input_integer ('SGS_model',sgsmodel,'default', &
         ' sgsmodel  !0=no model, >0 chooses a sgs model')

    hypermodel = 0 !no hyperbolic solver by default
    call input_integer ('hypermodel',hypermodel,'default', &
         ' hypermodel  !0=no module, >0 uses hyperbolic module')
 
    IF(hypermodel /= 0) THEN
       savevisc = .FALSE.
       call input_logical ('savevisc',savevisc,'default', &
       ' savevisc! TRUE - save numerical vicosity')
       J_hyper = J_mx
       call input_integer ('J_hyper',J_hyper,'default', &
         'j_hyper: level at which the viscosity is added')
    END IF

    imask_obstacle = .FALSE.
    call input_logical ('obstacle',imask_obstacle,'default', &
       ' imask_obstacle! TRUE - there is an obstacle defined')

    CALL distance_input() 

    stationary_obstacle = .TRUE.
    call input_logical ('stationary_obstacle',stationary_obstacle,'default', &
         ' stationary_obstacle! TRUE - if obstacle does not move and does not deform in time')
    
    Dpenal_factor = 2.0_pr
    IF(stationary_obstacle) Dpenal_factor = 1.0_pr
    
    read_geometry = .FALSE.
    call input_logical ('read_geometry',read_geometry,'default', &
         ' read_geometry! TRUE - the geometry for the obstacle is read from IGS geometry file')
    
    IF(read_geometry) &
         call input_string ('file_geom',file_geom,'stop',' file_geom! geometry file name in local directory geometry')
    
    !*********************************************************************************
    !                       unused variables 
    !*********************************************************************************

!!$ 
!!$  call input_real ('u0',u0,'stop', &
!!$       ' u0 :  imposed mean flow')
!!$
!!$  call input_real ('theta',theta,'stop', &
!!$       ' thet ')
!!$  
!!$  call input_real ('nu1',nu1,'stop', &
!!$       ' nu1 ')
!!$
!!$  call input_real ('theta1',theta1,'stop', &
!!$       ' theta1 (in degrees) (angle of 2D Burgers equation)')
!!$ 
!!$  call input_real_vector ('obstacle_X',Xo(1:dim),dim,'stop', &
!!$       ' Xo(:) ! Location of obstacle')
!!$
!!$  call input_real_vector ('obstacle_U',Uo(1:dim),dim,'stop', &
!!$       ' Uo(:) ! Velocity of obstacle')
!!$
!!$  call input_integer_vector ('obstacle_move',imv(1:dim),dim,'stop', &
!!$       ' imv(1) ! 1- Obstacle allowed to move in that direction, else == 0')
!!$
!!$  call input_real ('diameter',diameter,'stop', &
!!$       ' diameter')
!!$
!!$  call input_real ('k_star',k_star,'stop', &
!!$       ' k_star')
!!$
!!$  call input_real ('m_star',m_star,'stop', &
!!$       ' m_star')
!!$
!!$  call input_real ('b_star',b_star,'stop', &
!!$       ' b_star')
!!$  call input_real ('dtforce',dtforce,'stop', &
!!$       '  dtforce ! time interval for calculating the lift force')
!!$  
!!$  call input_real_vector ('Pressure_force',Pforce(1:dim),dim,'stop', &
!!$       ' Pforce(1) !pressure forcing term in x,y,z-dir')
    !*********************************************************************************

!**************** Lagrangian Module input
    ! read in variables for the lagrangian module (flag and input file name) (Ryan)
    use_lagrangian = .FALSE. !set default value to false in case is not in the input file
    CALL input_logical ('use_lagrangian',use_lagrangian,'default', &
         'T/F, choose to use lagrangian particle tracking')
    IF (use_lagrangian) THEN
       call input_string ('lagrangian_file',lagrangian_file,'stop', &
            'relative path and name of initilization file for the lagrangian module')
       lagrangian_file = TRIM(ADJUSTL(lagrangian_file))
    END IF
!****************

    
#ifdef MULTIPROC
    ! flag to allow domain splitting in the given directions (x,y,z,etc)
    ! (nonzero - allow, 0 - do not split that direction)
    ! the only usage of that flag occurs in domain decomposition
    domain_split = 1
    call input_integer_vector ('domain_split',domain_split,dim,'default')
    
    ! flag to suggest number of splitting  (x,y,z,etc) directions
    ! instead of (3D example)  nproc^(1/3), ^(1/2), etc. for z,y,x
    ! (nonzero - suggestion, 0 - compute)
    ! the only usage of that flag occurs in domain decomposition
    ! normally, the last division is computed, i.e. for domain_split = 0
    ! the value of domain_div(1) will be ignored
    domain_div = 0
    call input_integer_vector ('domain_div',domain_div,dim,'default')
    
    ! flag to terminate execution of the program
    ! inside the domain decompoisition subroutine
    ! (use it to choose the optimal values of domain_split and domain_div)
    domain_debug = .FALSE.
    call input_logical ('domain_debug',domain_debug,'default')
    
    ! flag to set domain decomposition method, see the manual
    ! (it is constant zero for a serial case)
    domain_meth = 0
    call input_integer ('domain_meth',domain_meth,'default')
    
    ! Imbalance = MIN / MAX, where MIN and MAX are the numbers of   !
    ! wavelets per procesor (local nwlt). Domain repartition        !
    ! might be forced if the correcpondent Imbalance < tolerance:   !
    !    Higly imbalanced data will be partitioned, moderately      !
    !    imbalanced - repartitioned, and not so imbalanced          !
    !    will be refined, depending on the Imbalance value.         !
    !                [1]              [2]          [3]              !
    !  0 ------- par_tol -------- rep_tol ---- ref_tol ---------- 1 !
    !                                                               !
    !  <-PARTITION-> <- REPARTITION-> <- REFINE -> <- DO NOTHING -> !
    domain_imbalance_tol = (/ 0.1_pr, 0.5_pr, 0.75_pr /)
    call input_real_vector ('domain_imbalance_tol',domain_imbalance_tol,3,'default')
    ! make sure that par_tol <= rep_tol <= ref_tol by overwriting
    ! left and write by min and max values
    CALL parallel_check_domain_imbalance_tol
    
    ! Zoltan internal debug level (0-10)
    domain_zoltan_verb = 0
    call input_integer ('domain_zoltan_verb',domain_zoltan_verb,'default')

    ! Flag to allow repartition during adaptation to initial conditions.
    ! Beware that repartitioning is changing the grid and therefore adapt grid will not
    ! detect (new_grid -> false) termination of adaptation to IC cycle and run extra
    ! iterations. If it is an issue, override default TRUE for that flag in your .inp file.
    IC_repartition = .TRUE.
    call input_logical ('IC_repartition',IC_repartition,'default')

    ! parameter to control the interpolation zone setup at j_mn in adjacent_wlt_DB for a parallel run
    !  0  - (default) adj points at j_mn are only added to the processor itself and not to the boundary zone
    !  1  - ... are added at upper-rightmost side of every adj point at j_mn belonging to the current processor
    !  2  - ... at all sides (+1 and -1) from point belonging to the current processor
    !  3  - ... (+2 +1, -1)
    !  4  - ... (+2, +1, -1, -2), etc.
    ! -1  - all the points are added at j_mn level as adjacent
    interpolation_adj = 0
    call input_integer ('interpolation_adj',interpolation_adj,'default')

#endif

    
    ! for old .res files IC_processors might need to be manually set
    ! as a number of processors the .res files were created on
    ! (in a serial case we may also read multiple .res files)
    IC_par_size = 0
    call input_integer ('IC_processors',IC_par_size,'default')


    ! Set version to save solution .res files.
    ! Used in save_solution()
    ! In short, in a parallel run v4 stores partition info at each processor's .res file
    ! while v5 stores partition info into a separate .com file.
    ! In a sequential run v4 and v5 .res files are identical.
    ! v6 and v7 can store coefficients or function values
    dot_res_file_version = 5
    call input_integer ( 'dot_res_file_version',dot_res_file_version,'default')
    IF ( (dot_res_file_version.NE.4).AND.(dot_res_file_version.NE.5) ) dot_res_file_version = 5
    
    
    ! Function or wavelet coefficient (TRUE - default) are stored in .res file
    res_save_coeff = .TRUE.
    call input_logical ('res_save_coeff', res_save_coeff, 'default')
    ! the parameter to be stored in ,res file, therefore
    ! .res file versions are 6 and 7
    IF ( (dot_res_file_version.EQ.4).OR.(dot_res_file_version.EQ.5) ) &
         dot_res_file_version = dot_res_file_version + 2

  END SUBROUTINE read_input
  
  
  !--------------------------------------------------------------------------------------------
  !
  ! After reading in any user or special module inputs.
  !
  SUBROUTINE read_input_finalize(eps_init_loc,eps_run_loc,eps_adapt_steps_loc, &
       scl_fltwt, &
       cflmax_loc,cflmin_loc,dtwrite,  &
       j_mn_loc, j_mx_loc, &
       j_filt,ij_adj, adj_type, VERB )
    USE PRECISION
    USE io_3D_vars
    USE wlt_vars
    USE wlt_trns_util_mod
    USE share_kry
    USE pde
    USE input_file_reader
    USE io_3d_vars            ! file_name_log, IT_STRING_LEN
    USE debug_vars            ! error
    USE parallel              ! par_rank
    IMPLICIT NONE
    REAL (pr) , INTENT (INOUT) :: eps_init_loc,eps_run_loc,scl_fltwt, cflmax_loc,cflmin_loc,dtwrite
    INTEGER   , INTENT (INOUT) :: eps_adapt_steps_loc 
    INTEGER   , INTENT (INOUT) :: j_mn_loc, j_mx_loc, j_filt
    INTEGER   , INTENT (INOUT), DIMENSION(-1:1) :: ij_adj, adj_type
    LOGICAL, INTENT(IN), OPTIONAL :: VERB
    LOGICAL :: verb_
    
    
    ! set default verbouse level
    verb_ = .FALSE.
    IF (PRESENT(VERB)) verb_ = VERB
    
    ! clean reader's memory
    call stop_reader
    
    IF (verb_) THEN
       WRITE (*,'("********************************************************************")')
       WRITE (*,'("Done Reading input file: ",A)') TRIM(file_name)
       WRITE (*,'(" ")')
       WRITE (*,'(" ")')
    END IF
    

    ! Predefine maximum assymetry allowed (wlt_vars module)
    CALL set_max_assymetry
    

    nbnd = 0
    ibnd=imp6(i_h_inp)
    ibc=imp6(i_l_inp)
    !PRINT *,'ibnd:',ibnd,'ibc:',ibc
    tol =MAX(1.e-3_pr*eps_run_loc,1.e-12_pr)
    IF(prd(1) == 1) THEN
       ibc(1)=0
       ibc(2)=0
       grid(1)=0
    END IF
    IF(prd(2) == 1) THEN
       ibc(3)=0
       ibc(4)=0
       grid(2)=0
    END IF
    IF(prd(3) == 1) THEN
       ibc(5)=0
       ibc(6)=0
       grid(3)=0
    END IF

    
    !******** INFO FOR SAVING FILES ***********
    IF ( LEN(TRIM(res_path)//TRIM(file_gen)) + 2*IT_STRING_LEN+1 + LEN(par_rank_str) .GT. LEN(file_wlt) ) THEN
       CALL error ( &
            '   Filename '//TRIM(res_path)//TRIM(file_gen)//' ... might be too long', &
            '   Try decreasing the length of file_gen in your input (.inp) file', &
            '   or increase parameter FLEN of io_3d_vars module and recompile.')
    ELSE
       IF ( allow_subdir_timestep ) THEN
          file_wlt = TRIM(res_path) // IT_STRING_PLACEHOLDER // slash // TRIM(file_gen)
       ELSE
          file_wlt = TRIM(res_path)//TRIM(file_gen)
       END IF
    END IF


    IF (LEN_TRIM(par_rank_str).GT.2) THEN
       file_name_user_stats = TRIM(res_path)//TRIM(file_gen)//TRIM(par_rank_str(2:LEN_TRIM(par_rank_str)))//'_user_stats'
       file_name_user_statz = TRIM(res_path)//TRIM(file_gen)//TRIM(par_rank_str(2:LEN_TRIM(par_rank_str)))//'_user_statz'    !by Giuliano
       file_name_log = TRIM(res_path)//TRIM(file_gen)//TRIM(par_rank_str(2:LEN_TRIM(par_rank_str)))//'_log'
    ELSE
       file_name_user_stats = TRIM(res_path)//TRIM(file_gen)//'_user_stats'
       file_name_log = TRIM(res_path)//TRIM(file_gen)//'_log'
    END IF


    
    ! set eps to eps init to begin with
    eps = eps_init_loc
    
    !check inputs
    CALL check_inputs(scl_fltwt, eps_run_loc,j_mn, j_mx, VERB=verb_ )
    

  END SUBROUTINE read_input_finalize
  !--************************************************************************
  !--End input/output routines
  !--************************************************************************



  !--------------------------------------------------------------------------------------------
  !
  !  Get command line arguments.  
  !  Uses string file_name defined in module io_3D_vars
  !
  SUBROUTINE read_command_line_input
    USE PRECISION
    USE io_3D_vars
    USE parallel
    USE debug_vars  ! error
#ifdef _MSC_VER
    USE DFLIB
#endif
    IMPLICIT NONE
    INTEGER(4) :: iargc
#ifdef _MSC_VER
     INTEGER(2) :: i
#else
    INTEGER(4) :: i
#endif
    
    ! Use the first argument as the input file name if is exists
    IF( iargc() >= 1 ) THEN
       i = 1
       CALL getarg( i, file_name )
	   !file_name = 'INPUT.inp' !OLEG: temporarily fix for CMA
    ELSE
       CALL error ( &
            'parameter input file name must be specified as first argument.' )
    END IF

  END SUBROUTINE read_command_line_input

  !--------------------------------------------------------------------------------------------
  !  
  ! do some basic checks on input values 
  !
  SUBROUTINE check_inputs(scl_fltwt, leps,j_mn_loc, j_mx_loc, VERB)
    USE precision
    USE share_consts
    USE pde
    USE io_3D_vars
    USE wlt_vars
    USE io_3D_vars
    USE wlt_trns_vars
    USE debug_vars     ! error
    USE parallel
    IMPLICIT NONE
    REAL (pr) ,  INTENT(IN) ::scl_fltwt !weight for temporal filter on scl
    REAL (pr) , INTENT (IN) :: leps
    INTEGER   , INTENT (IN) ::j_mn_loc, j_mx_loc
    LOGICAL, INTENT(IN), OPTIONAL :: VERB
    INTEGER :: wlt_fmly
    INTEGER :: idim
    LOGICAL :: badinp, verb_
    REAL (pr) :: ZERO
    
    ZERO = 1.0e-10
    badinp = .FALSE.
    
    verb_ = .FALSE.
    IF (PRESENT(VERB)) verb_ = VERB

    !
    ! Check that if restart is requested the IC file format is restart (0)
    !
    IF( IC_restart  .AND. IC_file_fmt /= 0  ) &
         CALL error ( 'IC_restart requested but IC_file_fmt not set for a restart file!' )
    



    !PRINT  *,' TEST in check_inputs, dim = ', dim
    DO idim =1,dim
       IF( (xyzlimits(2,idim) - xyzlimits(1,idim) ) <= ZERO ) THEN
          WRITE(*,'( ''Error: xyzlimits(2,'', I4 , '') - xyzlimits(1,'',I4,'') ) <= ZERO'')') &
               xyzlimits(2,idim) , xyzlimits(1,idim) 
          badinp = .TRUE.
       END IF
    END DO
    IF( badinp ) CALL error ('Exiting due to bad input values ')

    
    !check that j_mn <= j_mn_init  <= j_mx  
    IF( j_mn_loc > j_mx_loc) CALL error ('Error: j_mn > j_mx ')
    
    IF( j_mn_loc > j_mn_init) CALL error ('Error: j_mn > j_mn_init ')
    
    IF( j_mn_init > j_mx_loc) CALL error ('Error: j_mn_init > j_mx ')
    
    
    !check temporal filter weight for scl
    IF( scl_fltwt > 1.0 .OR. scl_fltwt < 0.0 ) THEN
       PRINT*, 'Error: temporal filter weight for scl, scl_fltwt = ', scl_fltwt
       PRINT*, 'But scl_fltwt needs to be between 0.0 and 1.0 '
       PRINT*, 'If scl_fltwt = 0.0 no temporal filtering of scl will be done '
       CALL error
    END IF

    !check tolerances for time integration meth 2
    IF( tol1 < 0.0 ) THEN
       IF( Scale_Meth == 1 ) THEN !Linf
          IF (verb_) PRINT * ,'**************************************************'
          IF (verb_) PRINT * ,'*Autosetting tol1,  tol1 in inputfile is < 0:  ', tol1 
          IF (verb_) PRINT * ,'* Scales method is Linf'
          tol1 = MAX(leps* 0.01_pr, 0.0001_pr);
          IF (verb_) PRINT * ,'*Use tol1 = MAX(leps* 0.01_pr, 1e-4) = ', tol1
          IF (verb_) PRINT * ,'**************************************************'
       ELSE IF(Scale_Meth == 2 ) THEN !L2
          IF (verb_) PRINT * ,'**************************************************'
          IF (verb_) PRINT * ,'*Autosetting tol1,  tol1 in inputfile is < 0:  ', tol1 
          IF (verb_) PRINT * ,'* Scales method is L2'
          tol1 = MAX(leps* 0.001_pr, 0.0001_pr);
          IF (verb_) PRINT * ,'*Use tol1 = MAX(leps* 0.001_pr, 1e-4) = ', tol1
          IF (verb_) PRINT * ,'**************************************************'

       ELSE
          PRINT *, 'Error in check_inputs(), Unknown Scale_Meth type: ', Scale_Meth
          CALL error ( 'Known Scale_Meth types: 1 = Linf scale, 2 = L2 scale' )
       END IF
    END IF
    
    IF( tol2 < 0.0 ) THEN
       IF( Scale_Meth == 1 ) THEN !Linf
          IF (verb_) PRINT * ,'**************************************************'
          IF (verb_) PRINT * ,'*Autosetting tol2,  tol2 in inputfile is < 0:  ', tol2 
          IF (verb_) PRINT * ,'* Scales method is Linf'
          tol2 = MAX(leps , 0.0001_pr);
          IF (verb_) PRINT * ,'*Use tol2 = MAX(leps, 1e-4) = ', tol2
          IF (verb_) PRINT * ,'**************************************************'
       ELSE IF(Scale_Meth == 2 ) THEN !L2
          IF (verb_) PRINT * ,'**************************************************'
          IF (verb_) PRINT * ,'*Autosetting tol2,  tol2 in inputfile is < 0:  ', tol2 
          IF (verb_) PRINT * ,'* Scales method is L2'
          tol2 = MAX(leps*0.1_pr , 0.0001_pr);
          IF (verb_) PRINT * ,'*Use tol2 = MAX(leps*0.1_pr, 1e-4) = ', tol2
          IF (verb_) PRINT * ,'**************************************************'

       ELSE
          PRINT *, 'Error in check_inputs(), Unknown Scale_Meth type: ', Scale_Meth
          CALL error ( 'Known Scale_Meth types: 1 = Linf scale, 2 = L2 scale' )
       END IF
    END IF

    !
    ! Make minumum number of scaling functions = 2
    !
    mxyz(:)=MAX(mxyz(:),2)

    !
    !
    !

    DO wlt_fmly = 0, n_wlt_fmly
       IF(n_updt(wlt_fmly) == 0) THEN
          n_assym_updt(wlt_fmly,:) = 0
          n_assym_updt_bnd(wlt_fmly,:) = 0
       END IF
    END DO

    !
    ! check dimensions
    !
    IF( (TYPE_DB == DB_TYPE_LINES .OR. TYPE_DB == DB_TYPE_WRK) .AND. (dim == 1 .OR. dim > 3) ) THEN
       PRINT *, ' ERROR Dimensionality other then 2 or 3. Dimension input as : ', dim
       CALL error
    ENDIF

    !
    !set up some things for doing 2D
    !
    jd = 1
    IF(dim==2) THEN
       IF (SIZE(mxyz).EQ.3) mxyz(3) = 1
       IF (SIZE(prd).EQ.3) prd(3) = 1
       jd = 0 
    END IF
    


    !
    ! Set time scale to 1.0. This might get reset in case specific user_pde()
    !
    timescl=1.0_pr

  END SUBROUTINE check_inputs
  !--------------------------------------------------------------------------------------------
  ! PUBLIC, used in post_process/c_wlt_inter.out
  SUBROUTINE clean_write (n, f, tol)
    USE PRECISION
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: n
    REAL (pr), INTENT (IN) :: tol   
    REAL (pr), DIMENSION (n), INTENT (INOUT) :: f

    WHERE (ABS(f) < tol) f = 0.0_pr   

  END SUBROUTINE clean_write


  !--------------------------------------------------------------------------------------------
  ! PRIVATE
  ! check if filename had no blanks at the end; add "XXXX.res" to the generic filename
  ! (NOTE, LEN(filename).EQ.LEN(file_out), therefore if open_file called with a constant
  ! parameter, i.e. open_file('something', ...), this subroutine does nothing
  SUBROUTINE open_file_aux (filename, it, file_out, SUBDIR)
    USE parallel
    USE io_3d_vars            ! res_path
    CHARACTER (LEN=*), INTENT (IN) :: filename
    INTEGER, INTENT (IN) :: it
    CHARACTER (LEN=LEN(filename)), INTENT (OUT) :: file_out
    CHARACTER (LEN=4) :: it_string_local
    LOGICAL, INTENT(IN), OPTIONAL :: SUBDIR
    LOGICAL :: do_subdir
    INTEGER :: pos                           ! '####' position (IT_STRING_PLACEHOLDER)
    CHARACTER (LEN=LEN(filename)) :: tmp_str

    do_subdir = .FALSE.
    IF(PRESENT(SUBDIR)) do_subdir = SUBDIR
    
    ! default behavior if called open_file('something', ...)
    file_out = filename
    
    ! iteration addition for a .res file
    WRITE(it_string_local,'(I4.4)') it
    IF ( LEN_TRIM( TRIM(ADJUSTL(filename))//it_string_local//TRIM(par_rank_str)//'.res') &
         .LE. LEN(filename) ) THEN
       file_out = TRIM(ADJUSTL(filename))//it_string_local//TRIM(par_rank_str)//'.res'
    END IF
    
    ! create subdirectory XXXX for a timestep
    ! and adjust the subdirectory name in filename ####
    IF (do_subdir) THEN
       CALL directory_create( TRIM(res_path)//slash//it_string_local )
       pos = INDEX(file_out,IT_STRING_PLACEHOLDER,BACK=.TRUE.)
       tmp_str = file_out
       file_out = TRIM( tmp_str(1:pos-1) //it_string_local// tmp_str(pos+IT_STRING_LEN:LEN(tmp_str)) )
    END IF
    
  END SUBROUTINE open_file_aux


  !--------------------------------------------------------------------------------------------
  ! PRIVATE
  ! check if filename had no blanks at the end; add "XXXX.res" to the generic filename
  ! (NOTE, LEN(filename).EQ.LEN(file_out), therefore if open_file called with a constant
  ! parameter, i.e. open_file('something', ...), this subroutine does nothing
  SUBROUTINE open_file_aux_common (filename, it, file_out, SUBDIR)
    USE parallel
    USE io_3d_vars            ! res_path
    CHARACTER (LEN=*), INTENT (IN) :: filename
    INTEGER, INTENT (IN) :: it
    CHARACTER (LEN=LEN(filename)), INTENT (OUT) :: file_out
    CHARACTER (LEN=4) :: it_string_local
    LOGICAL, INTENT(IN), OPTIONAL :: SUBDIR
    LOGICAL :: do_subdir
    INTEGER :: pos                           ! '####' position (IT_STRING_PLACEHOLDER)
    CHARACTER (LEN=LEN(filename)) :: tmp_str

    do_subdir = .FALSE.
    IF(PRESENT(SUBDIR)) do_subdir = SUBDIR
    
    ! default behavior if called open_file('something', ...)
    file_out = filename
    
    ! iteration addition for a .res file
    WRITE(it_string_local,FILE_NUM_FMT) it
    IF ( LEN_TRIM( TRIM(ADJUSTL(filename))//it_string_local//'.com.res') &
         .LE. LEN(filename) ) THEN
       file_out = TRIM(ADJUSTL(filename))//it_string_local//'.com.res'
    END IF
    
    ! create subdirectory XXXX for a timestep
    ! and adjust the subdirectory name in filename ####
    IF (do_subdir) THEN
       CALL directory_create( TRIM(res_path)//slash//it_string_local )
       pos = INDEX(file_out,IT_STRING_PLACEHOLDER,BACK=.TRUE.)
       tmp_str = file_out
       file_out = TRIM( tmp_str(1:pos-1) //it_string_local// tmp_str(pos+IT_STRING_LEN:LEN(tmp_str)) )
    END IF
    
  END SUBROUTINE open_file_aux_common
  
  !--------------------------------------------------------------------------------------------
  ! PRIVATE
  ! assign L_STR = R_STR with boundary checking and print error message regarding ERR_STR
  SUBROUTINE assign_string_to_string (l_str,r_str,err_str)
    USE parallel
    USE debug_vars  ! error
    CHARACTER (LEN=*), INTENT (IN) :: r_str, err_str
    CHARACTER (LEN=*), INTENT (INOUT) :: l_str
    
    IF (LEN(l_str).GE.LEN(r_str)) THEN
       l_str = r_str
    ELSE
       WRITE (*,'(4A,2(A,I4))') ' ', err_str,' is too long: writing "', TRIM(r_str), &
            ' of length ', LEN(TRIM(r_str)), &
            ' into a string of length ', LEN(l_str)
       CALL error
    END IF
    
  END SUBROUTINE assign_string_to_string

  !--------------------------------------------------------------------------------------------
  ! PRIVATE
  SUBROUTINE serious_warning_message
    USE debug_vars ! ignore_warnings
    USE error_handling
    
    CALL error ( &
         '*******************************************************', &
         '*******************************************************', &
         '**                                                   **', &
         '**  POZOR !   WARNING !   ACHTUNG !   ATENCION !     **', &
         '**  You are reading obsolete 01/31/2007 .res format  **', &
         '**  Keep praying it will work for you this time ...  **', &
         '**                                                   **', &
         '*******************************************************', &
         '*******************************************************', &
         IW=ignore_warnings, F=.TRUE. ) ! forced ignore
    
  END SUBROUTINE serious_warning_message
  
  
  
  !--------------------------------------------------------------------------------------------
  ! 01/31/2007 .res format
  ! this format is NOT the recommended one
  !
  ! do NOT edit, this is an obsolete format !
  !
  ! The recommended format is in read_solution_aux_1
  !
  SUBROUTINE read_solution_aux_0( scl, dummy, filename, do_read_all_vars, len_uvars )
    USE PRECISION
    USE pde
    USE variable_mapping   ! n_var
    USE sizes              ! nwlt
    USE field
    USE wlt_vars
    USE wlt_trns_mod
    USE wlt_trns_vars
    USE debug_vars         ! test_alloc, error
    USE io_3D_vars         ! IC_restart_mode
    USE parallel           ! par_size
    USE unused
    IMPLICIT NONE
    CHARACTER (LEN=*) , INTENT(IN) ::  filename
    REAL (pr), DIMENSION(:), POINTER :: scl
    LOGICAL, INTENT(IN) :: dummy                 ! for backward compatibility only
    LOGICAL, INTENT(IN) :: do_read_all_vars      ! if true read all vars
    INTEGER, INTENT(INOUT) :: len_uvars

    LOGICAL :: do_restart                        ! for backward compatibility only
    CHARACTER , DIMENSION(:), ALLOCATABLE  :: header2
    INTEGER   ::  hdr_len !length of actual header in the file
    INTEGER   :: card_n_var_save !number of field variables saved in file
    INTEGER   ::  save_vort !if 1 we saved vorticity in the file, else 0
    INTEGER   :: idim, ie,ie2,i
    INTEGER   ::  u_slot, num_found
    INTEGER   :: mxyz_tmp(1:3)
    LOGICAL   :: form, done, found, ie_found(1:n_var)
    INTEGER   :: n_var_rst_file, n_integrated_tmp
    REAL (pr) , DIMENSION(:), ALLOCATABLE   :: u_dummy
    INTEGER   :: j, j_df, wlt_type, face_type
    INTEGER   :: j_mn_read, j_mx_read
    REAL (pr) :: dt_tmp
    INTEGER   :: n_wlt_fmly_tmp
    INTEGER   ::  version ! file format version number
    INTEGER :: alloc_stat
   
    INTEGER, DIMENSION(:), ALLOCATABLE :: n_var_wlt_fmly_local ! wavelet transform family for each variable
    
    
    do_restart = .FALSE.
    IF ( (IC_restart_mode.EQ.1).OR.(IC_restart_mode.EQ.2) ) do_restart = .TRUE.
    
    !read in header and see if this file is formatted or unformatted.
    OPEN (10, FILE = filename, FORM='formatted', STATUS='old')


    !
    !-- Read in the ascii header
    CALL read_solution_hdr(10,form, version, hdr_len)

    !
    ! read a given solution file byversion
    !


    !CALL read_solution_V1()

    ! --------------------------------------------------------
    ! as long as our arrays are (1:3) instead of (1:dim)
    ! the extra dimensions have to be initialized !
    nxyz = 1
    mxyz_tmp = 1
    prd = 1
    grid = 1
    ! --------------------------------------------------------

    IF( form ) THEN !formatted data


       PRINT *,'FORMATTED NOT FINISHED...'
       STOP 1
    ELSE !unformatted

       CLOSE(10)

       OPEN (10, FILE = filename, FORM='unformatted', STATUS='old')

       ! This is an unformatted file so the record started after the 4 byte
       ! record header
       hdr_len = hdr_len -4
       PRINT *,'READING Data file header, hdr_len = ', hdr_len
       ALLOCATE(header2(hdr_len), STAT=alloc_stat)
       CALL test_alloc( alloc_stat, 'header2 in read_solution_aux_0', hdr_len )
       READ (10) header2
       !PRINT *,'Data file header: '
       !PRINT *, header2

       READ (10) dim, n_wlt_fmly_tmp ! 04/03/2006: Oleg's change 
       IF(n_wlt_fmly_tmp > n_wlt_fmly) THEN
          WRITE(*,'("ERROR: n_wlt_fmly_tmp > w_wlt_fmly")')
          STOP 1
       END IF
       READ (10) n_prdct(0:n_wlt_fmly_tmp), n_updt(0:n_wlt_fmly_tmp), n_assym_prdct_resfile(0:n_wlt_fmly_tmp,0),&
            n_assym_updt_resfile(0:n_wlt_fmly_tmp,0),n_assym_prdct_bnd_resfile(0:n_wlt_fmly_tmp,0),&
            n_assym_updt_bnd_resfile(0:n_wlt_fmly_tmp,0), card_n_var_save, save_vort, &
            nxyz(1:dim), mxyz_tmp(1:dim),&
            prd(1:dim), &
            grid(1:dim),&
            j_mn_read, j_mx_read, j_lev, nwlt, it, iwrite,&
            n_var_rst_file, n_integrated_tmp, n_time_levels
           !WARNING: n_time_levels no longer in use, left for backward compatibility        

       DO i = 0, n_trnsf_type !0 - regular, 1 - inernal
          n_assym_prdct(:,i)     = n_assym_prdct_resfile(:,0)
          n_assym_prdct_bnd(:,i) = n_assym_prdct_bnd_resfile(:,0)
          n_assym_updt(:,i)      = n_assym_updt_resfile(:,0)
          n_assym_updt_bnd(:,i)  = n_assym_updt_bnd_resfile(:,0)
       END DO 

       ! --- WARNING ---
       ! n_updt transformation is required if we want
       ! to read data with nonzero one in parallel code
       CALL set_zero_n_updt_for_multiproc
       
       PRINT *,'read_solution:'
       PRINT *,'j_mn_read ',j_mn_read
       PRINT *,'j_mx_read',j_mx_read
       PRINT *,'j_lev',j_lev
       PRINT *,'nwlt',nwlt
       PRINT *,'it',it
       PRINT *,'iwrite',iwrite
       PRINT *,'n_var_rst_file',n_var_rst_file
       PRINT *,'n_integrated_tmp',n_integrated_tmp
       IF(do_read_all_vars) THEN
          len_uvars = n_var_rst_file
          CALL reset_mapping()                    ! clear registry and mappings !ERIC: will this mess up the allocation of uTMP?, ERIC: and this is only for do_read_all_vars 
       END IF

       IF( do_restart .AND. (mxyz_tmp(1) /= mxyz(1) .OR. &
            mxyz_tmp(2) /= mxyz(2) .OR. &
            mxyz_tmp(3) /= mxyz(3))  ) THEN
          PRINT *, 'Error, Attempt to restart with Mxyz different from '
          PRINT *, 'restart file.'
          PRINT *, 'Mxyz from input file:' ,mxyz
          PRINT *, 'Mxyz from restart file:' ,mxyz_tmp
          PRINT *, 'This is not currently supported. No reason'
          PRINT *, 'it can be supported in the future. DG. Exiting ...'
          STOP 1
       END IF

       !
       ! Check that if we are doing a restart, j_mn(from input file) = j_mn_read (from restart file)
       ! Actually this is more restrictive than is necessary. It would work to have 
       ! j_mn(from input file) <= j_mn_read but to support this we need to change init_lines_db().
       !   
       IF( do_restart .AND. ( j_mn /= j_mn_read .OR. j_mx /= j_mx_read) )THEN
          PRINT *, 'Error, Attempt to restart with j_mn or j_mx different from '
          PRINT *, 'restart file.'
          PRINT *, 'This is not currently supported.'
          PRINT *, 'This can be done currently by using the restart file as the IC file by setting:' 
          PRINT *, 'IC_restart_mode = 3'
          PRINT *, 'DG. Exiting ...'
          STOP 1
       END IF

       !
       ! For post processing we are doing a restart but we do want to take the value of j_mx and j_mn 
       !  from the input.
       !
       IF( j_mx == 0 ) THEN
          j_mx = j_mx_read
          j_mn = j_mn_read
       END IF

       !
       ! In non-restart cases that the IC file is read from a restart file
       ! we use j_mx and j_mn from the input file
       !

       mxyz(1:3) =  mxyz_tmp(1:3)
       !DG CHANGE TO RESTART WITH DIFFERENT Mxyz:     !if we are doing restart we want to use the Mxyz values from the inp file
       !DG CHANGE TO RESTART WITH DIFFERENT Mxyz:     IF(.NOT. do_restart ) mxyz(1:3) = mxyz_tmp(1:3)
       READ (10) t, dt_tmp

       !
       ! Use dt from input file if we are doing a restart
       !
       IF( do_restart ) THEN
          IF( dt /= dt_tmp) THEN
             PRINT *,''
             PRINT *,'***************************************************'
             PRINT *,'Restarting using dt from input file:',dt
             PRINT *,'Not using dt from restart file:', dt_tmp
             PRINT *,'***************************************************'
             PRINT *,''
          END IF
       ELSE
          dt = dt_tmp
       END IF


       IF(ALLOCATED(xx) ) DEALLOCATE(xx)
       ALLOCATE (xx(0:MAXVAL(nxyz),1:dim), STAT=alloc_stat)
       CALL test_alloc( alloc_stat, 'xx in read_solution_aux_0', (MAXVAL(nxyz)+1)*dim )
       IF(.NOT.ALLOCATED(indx_DB)) THEN
          ALLOCATE(indx_DB(1:j_mx,0:2**dim-1,0:3**dim-1,1:j_mx), STAT=alloc_stat)
          CALL test_alloc( alloc_stat, 'indx_DB in read_solution_aux_0', j_mx*(2**dim)*(3**dim)*j_mx )
          DO j = 1, j_mx
             DO wlt_type = MIN(j-1,1),2**dim - 1
                DO face_type = 0, 3**dim - 1
                   DO j_df = 1, j_mx
                      NULLIFY (indx_DB(j_df,wlt_type,face_type,j)%p)
                      indx_DB(j_df,wlt_type,face_type,j)%real_length = 0
                   END DO
                END DO
             END DO
          END DO
       END IF
       IF(allocated(u) ) DEALLOCATE(u)
       IF( do_restart ) THEN
          IF( n_var > n_var_rst_file ) THEN
             PRINT *,' '
             PRINT *,'*************************************************************************'
             PRINT *,'Restarting with n_var > n_var in restart file! Caution this might be wrong!'
             PRINT *,'*************************************************************************'
             PRINT *,' '
          END IF
          IF( n_var < n_var_rst_file ) THEN
             PRINT *,' '
             PRINT *,'*************************************************************************'
             IF( (IC_restart_mode.EQ.1).OR.(IC_restart_mode.EQ.2) ) THEN
                PRINT *,'Restart with n_var <  n_var in restart file! This is not supported. Exiting...'
                PRINT *,'*************************************************************************'
                PRINT *,' '
                STOP 1
             ELSE
                PRINT *,'WARNING: Restart with n_var <  n_var in restart file! Possible Error !!!'
                PRINT *,'*************************************************************************'
                PRINT *,' '
             END IF
          END IF
          IF( n_integrated_tmp /= n_integrated) THEN
             PRINT *,' '
             PRINT *,'*************************************************************************'
             PRINT *,'Restart with n_integrated /= n_integrated in restart file! Possible Error !!!'
             PRINT *,'*************************************************************************'
             PRINT *,' '
          END IF
          ALLOCATE (u(1:nwlt,n_var), u_dummy(1:nwlt), STAT=alloc_stat) 
          CALL test_alloc( alloc_stat, 'u in read_solution_aux_0', nwlt*(n_var+1) )
       ELSE
          ALLOCATE (u(1:nwlt,1:len_uvars), STAT=alloc_stat)
          CALL test_alloc( alloc_stat, 'u in read_solution_aux_0', nwlt*n_var )
          IF( len_uvars <= card_n_var_save )THEN
             !ALLOCATE (u(1:nwlt,1:len_uvars))
          ELSE
             PRINT *,'Error in read_solution(). Number of requested variables(',len_uvars,')'
             PRINT *,'is greater then number of variables saved in this data'
             PRINT *,'file (',card_n_var_save,'). Exiting ...' 
             !STOP 1
          END IF
       END IF
       u = 0.0_pr 
       DO idim = 1,dim
          READ (10) xx(0:nxyz(idim),idim)
       END DO
       DO j = 1, j_lev
          DO wlt_type = MIN(j-1,1),2**dim-1
             DO face_type = 0, 3**dim - 1
                DO j_df = j, j_lev
                   READ (10) indx_DB(j_df,wlt_type,face_type,j)%length
                   IF( indx_DB(j_df,wlt_type,face_type,j)%length  /= indx_DB(j_df,wlt_type,face_type,j)%real_length ) THEN
                      ! eventually make smarter allocation algorithm, so 
                      ! if the size is bigger, increasie it by 25% and if smaller than 50% decrease by 25%
                      ! or something like this 
                      IF (indx_DB(j_df,wlt_type,face_type,j)%real_length > 0) THEN
                         DEALLOCATE(indx_DB(j_df,wlt_type,face_type,j)%p)
                         indx_DB(j_df,wlt_type,face_type,j)%real_length = 0
                      END IF
                      IF (indx_DB(j_df,wlt_type,face_type,j)%length  > 0) THEN
                         ALLOCATE(indx_DB(j_df,wlt_type,face_type,j)%p(1:indx_DB(j_df,wlt_type,face_type,j)%length), STAT=alloc_stat)
                         CALL test_alloc( alloc_stat, 'indx_DB()%p in read_solution_aux_0', indx_DB(j_df,wlt_type,face_type,j)%length )
                         indx_DB(j_df,wlt_type,face_type,j)%real_length = indx_DB(j_df,wlt_type,face_type,j)%length
                      END IF
                   END IF
                   IF( indx_DB(j_df,wlt_type,face_type,j)%length  > 0) THEN
                      READ (10) indx_DB(j_df,wlt_type,face_type,j)%p(1:indx_DB(j_df,wlt_type,face_type,j)%length)%i
                      READ (10) indx_DB(j_df,wlt_type,face_type,j)%p(1:indx_DB(j_df,wlt_type,face_type,j)%length)%ixyz
                   END IF
                END DO
             END DO
          END DO
       END DO
       !read the scl array for this time step
       IF( .NOT. ASSOCIATED(scl)            ) THEN
          ALLOCATE ( scl(1:n_var_rst_file), STAT=alloc_stat)  ! 04/03/2006: Oleg's change
          CALL test_alloc( alloc_stat, 'scl in read_solution_aux_0', n_var_rst_file )
       END IF
       IF( .NOT. ASSOCIATED(scl_global)     ) THEN
          ALLOCATE ( scl_global(1:n_var_rst_file), STAT=alloc_stat)  ! 04/03/2006: Oleg's change
          CALL test_alloc( alloc_stat, 'scl_global in read_solution_aux_0', n_var_rst_file )
       END IF
       !IF( .NOT. ALLOCATED(n_var_wlt_fmly) ) THEN
       !   ALLOCATE ( n_var_wlt_fmly(1:n_var_rst_file), STAT=alloc_stat )  ! 04/03/2006: Oleg's change
       !   CALL test_alloc( alloc_stat, 'n_var_wlt_fmly in read_solution_aux_0', n_var_rst_file )
       !END IF
       IF( .NOT. ALLOCATED(n_var_wlt_fmly_local) ) THEN
          ALLOCATE ( n_var_wlt_fmly_local(1:n_var_rst_file), STAT=alloc_stat )  ! ERIC: updated to reflect deprecation 
          CALL test_alloc( alloc_stat, 'n_var_wlt_fmly_local in read_solution_aux_0', n_var_rst_file )
       END IF
       READ (10)  scl(1:n_var_rst_file); scl_global = scl
       READ (10)  n_var_wlt_fmly_local(1:n_var_rst_file)  ! 04/03/2006: Oleg's change

       WRITE(*,'( "Reading in iteration:", I3 ,",  t = ", E12.5 )') it, t

       IF(do_restart ) THEN
          ie_found(:) = .FALSE.
          DO ie = 1, card_n_var_save 
             READ (10) tmp_name_str, u_slot ! 04/03/2006: Oleg's change - need this to put variable to the original ie location

             found = .FALSE.
             DO ie2 = 1,  n_var
                IF( INDEX(u_variable_names(ie2),TRIM(ADJUSTL(tmp_name_str))) /= 0 ) THEN
                   READ (10) u(1:nwlt,u_slot)
                   ie_found(ie2) = .TRUE.
                   found = .TRUE.
                   PRINT *, 'Reading var: ', tmp_name_str
                   exit ! exit loop
                END IF
             END DO
             IF( .NOT. found ) THEN
                DO ie2 = 1,  n_var
                   IF( INDEX(u_IC_names(ie2),TRIM(ADJUSTL(tmp_name_str))) /= 0 ) THEN
                      READ (10) u(1:nwlt,u_slot)
                      ie_found(ie2) = .TRUE.
                      found = .TRUE.
                      PRINT *, 'Reading var: ', tmp_name_str
                      exit ! exit loop
                   END IF
                END DO
             END IF
             IF( .NOT. found ) THEN
                READ (10) u_dummy(1:nwlt) !we didn't need this variable so we will skip it
             END IF
          END DO
          DO ie = 1,  n_var
             IF(  n_var_req_restart(ie) .AND. .NOT. ie_found(ie) ) THEN
                PRINT *, 'Error restarting, required restart variable: ', u_variable_names(ie)
                PRINT *, 'not found in restart file. Exiting...'
                STOP 1
             END IF
          END DO



       ELSE !not restart
          ! This check is not necessary. It does not take into account additional variables
          ! that were saved
          !IF( len_uvars > n_integrated +1 ) THEN
          !   PRINT *,'ERROR to many variables names in u_variable_names.'
          !   DO ie=1,len_uvars
          !      PRINT *,' u_variable_names(',ie,') =',u_variable_names(ie)
          !   END DO
          !   PRINT *,'Exiting ...'
          !   STOP 1
          !END IF

          done = .FALSE.
          num_found = 0 !number of variables found
          ie = 1
          !PRINT *,' card_n_var_save ', card_n_var_save
          ! if we saved vorticity then add dim to card_n_var_save 
          if( save_vort == 1 ) card_n_var_save = card_n_var_save + dim
          DO WHILE ( ie <= card_n_var_save .AND. .NOT. done )
             READ (10) tmp_name_str, u_slot! 04/03/2006: Oleg's change - need this to put variable to the original ie location
             !PRINT *, 'Read var name header: ', tmp_name_str 
             !find if this variable is requested in u_variable_names
             i=1; found = .FALSE.
             DO WHILE ( .NOT. found .AND. i <= len_uvars ) 
                !PRINT *,' cmp ', u_variable_names( i ), tmp_name_str
                IF( do_read_all_vars .OR. &
                    ( INDEX(u_variable_names(i),TRIM(ADJUSTL(tmp_name_str))) /= 0 ) ) THEN
                   found = .TRUE. 
                   num_found = num_found+1
                ELSE
                   i = i+1
                END IF
                !PRINT *,' found i', found, i
             END DO
             IF( .NOT. found ) THEN
                i=1; found = .FALSE.
                DO WHILE ( .NOT. found .AND. i <= len_uvars ) 
                   IF ( INDEX(u_IC_names(i),TRIM(ADJUSTL(tmp_name_str))) /= 0 ) THEN
                      found = .TRUE. 
                      num_found = num_found+1
                   ELSE
                      i = i+1
                   END IF
                END DO
             END IF

             IF( found ) THEN
                IF(u_slot /= i .AND. .NOT. do_read_all_vars ) THEN
                   WRITE(*,'("i=",I8," u_slot=",I8)') i,u_slot
                   WRITE(*,'("WARNING: read_solution - possible error u_slot /= i")') 
                END IF
                IF( do_read_all_vars ) THEN
                   u_slot = num_found ! if getting all the vars we just put it in the next slot
                ELSE
                   u_slot = i !put var in same place as its name in u_variable_names
                END IF
                PRINT *, 'Reading var: ', tmp_name_str , i
             ELSE
                !Not found so we need to read over this var
                !
                u_slot = len_uvars
             END IF
             READ (10) u(1:nwlt,u_slot)

             !
             ! Save the variable name if we are doing do_read_all_vars
             !
             IF( do_read_all_vars ) THEN !CALL set_var_name( u_slot, tmp_name_str ) !u_variable_names( u_slot ) = tmp_name_str
                !CALL register_var( tmp_name_str, .FALSE., adapt=(/.FALSE.,.FALSE./), interpolate=(/.FALSE.,.FALSE./), exact=(/.FALSE.,.FALSE./), saved=.FALSE., req_restart=.FALSE. )
                CALL register_var( tmp_name_str, integrated=(u_slot .LE. n_integrated_tmp), adapt=(/.FALSE.,.FALSE./), interpolate=(/.FALSE.,.FALSE./), exact=(/.FALSE.,.FALSE./), saved=.FALSE., req_restart=.FALSE. )
             END IF 

             IF( num_found == len_uvars ) THEN
                done = .TRUE.
             ELSE
                ie = ie +1
             END IF
             !PRINT *,' ie ', ie
             !PRINT *,' done ', done
             !PRINT *,' num_found ', num_found
             !PRINT *,' len_uvars ', len_uvars
          END DO

          !check that we got all the variables requested
          IF( .NOT. done .AND. &
               ( (IC_restart_mode.EQ.1).OR.(IC_restart_mode.EQ.2) ) ) THEN
             PRINT *,'Error in reading in some variables. Exiting ...'
             STOP 1
          END IF

       IF( do_read_all_vars ) CALL setup_mapping()

       END IF

       CLOSE(10)

    END IF

    IF(ALLOCATED(u_dummy) ) DEALLOCATE ( u_dummy )
  END SUBROUTINE read_solution_aux_0


  !--------------------------------------------------------------------------------------------
  ! PRIVATE
  ! find the storage size of default integer and real
  ! (which could be something like integer*4 and real*8 on Intel)
  SUBROUTINE set_default_types( it_2, rt_2 )
    CHARACTER (LEN=2), INTENT(OUT) :: it_2, rt_2        ! default types

    i_default = KIND(i_default)                                 ! number of bytes in INTEGER
    WRITE(it_2,'("I",I1)') i_default                            ! RES_READ/WRITE format is 'Ii'
    i_default = KIND(r_default)                                 ! number of bytes in REAL
    WRITE(rt_2,'("R",I1)') i_default                            ! RES_READ/WRITE format is 'Ri'
    IF ((i_default.NE.4).AND.(i_default.NE.8)) THEN
       WRITE (*,'("Default real is of KIND ",I4)') i_default
       WRITE (*,'("Currently supported KIND 4 and 8")')
       WRITE (*,'("You will have to port I/O subroutines")')
       CALL error ( &
            'Type size related problems.', &
            'Contact the developers or fix it by yourself.' )
    END IF

!AR 2/24/2011!  The following 3 lines are added by AR
#ifdef INTEGER8_DEBUG_AR
    WRITE (*,'(A, I2, A, I2, A, A, A, A)')  'set_default_types     KIND(i_default)=', KIND(i_default), '   KIND(r_default)=', KIND(r_default), '   it_2=', it_2, '   rt_2=', rt_2
#endif

  END SUBROUTINE set_default_types
  
  !--------------------------------------------------------------------------------------------
  ! PRIVATE
  ! serial: do nothing
  ! parallel: if run on several processors: set n_updt to zero,    issue warning
  !           single processor run:         leave n_updt as it is, issue warning (forced ignore)
  SUBROUTINE set_zero_n_updt_for_multiproc
#ifdef MULTIPROC
    USE wlt_vars   ! n_updt
    USE parallel   ! par_size
    USE debug_vars ! ignore_warnings
    
    !
    ! N_UPDT=0 FOR PARALLEL CODE
    ! IN MULTIPROCESSOR RUN ONLY
    !
    IF ( MAXVAL(n_updt).NE.0 ) THEN

       IF (par_size.GT.1) n_updt = 0
       CALL error ( &
            '-------------------------------------------------------------------------------------', &
            '-- This is a parallel compilation of the code (probably, with MP=1 gmake argument) --', &
            '-- and N_update or N_update_low_order parameters are not zero in your input file.  --', &
            '--                                                                                 --', &
            '-- In a run on a SINGLE processor your INPUT FILE VALUES will be used, although    --', &
            '-- in a run on SEVERAL processors both N_update parameters will be FORCED TO ZERO. --', &
            '-------------------------------------------------------------------------------------', &
            IW=ignore_warnings, F=.TRUE.) ! forced ignore
       
    END IF
#endif
  END SUBROUTINE set_zero_n_updt_for_multiproc
  
  
  
  !--------------------------------------------------------------------------------------------
  ! PRIVATE
  ! close .res file, check tree-c/io.h for details
  SUBROUTINE close_res_file (filename)
    USE parallel
    USE debug_vars  ! error
    CHARACTER (LEN=*), INTENT(IN) :: filename
    INTEGER (4) :: ierr_4
    
    CALL RES_CLOSE( ierr_4 )
    IF (ierr_4.NE.0_4) THEN
       WRITE (*,'("Cannot close the file: ",A)') TRIM(filename)
       CALL error
    END IF
    
  END SUBROUTINE close_res_file
  
  
  !--------------------------------------------------------------------------------------------
  ! PRIVATE
  ! allocate u for multiple .res files
  SUBROUTINE allocate_u (OLDSIZE, NEWSIZE, NVAR, UTMP, UOLD, ISFIRST)
    USE precision
    USE field               ! u, u_ex
    USE debug_vars          ! test_alloc
    INTEGER, INTENT(IN), OPTIONAL :: OLDSIZE, NEWSIZE, NVAR
    REAL (pr), POINTER, OPTIONAL :: UTMP(:,:), UOLD(:,:)     ! buffers for u array rearranging
    LOGICAL, INTENT(INOUT), OPTIONAL :: ISFIRST
    INTEGER :: alloc_stat
    
    IF (ALLOCATED(u)) THEN  !---------- continuing multiple files reading
       !                    ! uold - a copy of old u
       !                    ! utmp - buffer to read new u
       !                    ! u - reallocated and ready to be written
       IF (ASSOCIATED(UOLD)) THEN
          IF (SIZE(UOLD).LT.NVAR*OLDSIZE) DEALLOCATE (UOLD)
       END IF
       IF (.NOT.ASSOCIATED(UOLD)) THEN
          ALLOCATE( UOLD(OLDSIZE,NVAR), STAT=alloc_stat )
          CALL test_alloc( alloc_stat, 'UOLD in allocate_u', OLDSIZE*NVAR )
       END IF
       IF (ASSOCIATED(UTMP)) THEN
          IF (SIZE(UTMP).LT.NVAR*NEWSIZE) DEALLOCATE (UTMP)
       END IF
       IF (.NOT.ASSOCIATED(UTMP)) THEN
          ALLOCATE( UTMP(NEWSIZE,NVAR), STAT=alloc_stat )
          CALL test_alloc( alloc_stat, 'UTMP in allocate_u', NEWSIZE*NVAR )
       END IF
       UOLD(1:OLDSIZE,1:NVAR) = u(1:OLDSIZE,1:NVAR)
       DEALLOCATE (u)
       ALLOCATE( u(NEWSIZE,NVAR), STAT=alloc_stat )
       CALL test_alloc( alloc_stat, 'u in allocate_u', NEWSIZE*NVAR )
       ISFIRST = .FALSE.
    ELSE                    !---------- first file reading
       !                    ! uold => 0
       !                    ! utmp => u
       !                    ! u - allocated and ready to be written
       ALLOCATE( u(1:NEWSIZE,1:NVAR), STAT=alloc_stat )
       CALL test_alloc( alloc_stat, 'u in allocate_u', NEWSIZE*NVAR )
       UTMP => u
       ISFIRST = .TRUE.
    END IF
!!$    IF (ALLOCATED(u)) THEN
!!$       IF (ALLOCATED(u_ex)) THEN
!!$          IF (SIZE(u_ex,DIM=1).LT.OLDSIZE) DEALLOCATE (u_ex)
!!$       END IF
!!$       IF (.NOT.ALLOCATED(u_ex)) THEN
!!$          ALLOCATE( u_ex(1:OLDSIZE,1:NVAR), STAT=alloc_stat )
!!$          CALL test_alloc( alloc_stat, 'u_ex in allocate_u', OLDSIZE*NVAR )
!!$       END IF
!!$       u_ex(1:OLDSIZE,1:NVAR) = u(1:OLDSIZE,1:NVAR)
!!$       DEALLOCATE( u )
!!$       ALLOCATE( u(1:NEWSIZE,1:NVAR), STAT=alloc_stat )
!!$       CALL test_alloc( alloc_stat, 'u in in allocate_u', NEWSIZE*NVAR )
!!$       u(1:OLDSIZE,1:NVAR) = u_ex(1:OLDSIZE,1:NVAR)
!!$       u(OLDSIZE+1:NEWSIZE,1:NVAR) = 0.0_pr
!!$       DEALLOCATE( u_ex )
!!$       PRINT *, 'allocate_u: EXT: old/new=', OLDSIZE, NEWSIZE
!!$    ELSE
!!$       PRINT *, 'allocate_u: NEW: old/new=', OLDSIZE, NEWSIZE
!!$       ALLOCATE(u(1:NEWSIZE,1:NVAR), STAT=alloc_stat )
!!$       CALL test_alloc( alloc_stat, 'u in allocate_u', NEWSIZE*NVAR )
!!$       u(1:NEWSIZE,1:NVAR) = 0.0_pr
!!$    END IF
  END SUBROUTINE allocate_u
  
  !--------------------------------------------------------------------------------------------
  ! PRIVATE
  ! allocate indx_DB (or increase the length for multiple .res files)
  SUBROUTINE allocate_indx_DB (indx_DB_ptr, OLDLEN, NEWLEN, IWLTADD)
    USE wlt_trns_vars       ! indx_type_DB
    USE debug_vars          ! test_alloc
    INTEGER, INTENT(IN), OPTIONAL :: OLDLEN, NEWLEN, & ! old/new sizes of p array
         IWLTADD                                       ! correctional addition to iwlt index
    TYPE (indx_type_DB), POINTER :: indx_DB_ptr        ! element of indx_DB(...) array
    INTEGER :: alloc_stat, i
    TYPE (indx_list_DB), ALLOCATABLE :: p_tmp(:)       ! copy buffer
    
    
    IF (OLDLEN .NE.indx_DB_ptr%real_length) THEN
       PRINT *, 'OLDLEN                =', OLDLEN
       PRINT *, 'indx_DB...%real_length=', indx_DB_ptr%real_length
       CALL error ('internal error in allocate_indx_DB()', &
            'please contact the developers.')
    END IF

    
    IF( NEWLEN  /= OLDLEN ) THEN
       ! eventually make smarter allocation algorithm, so 
       ! if the size is bigger, increase it by 25% and if smaller than 50% decrease by 25%
       ! or something like this 
       IF ( OLDLEN > 0 ) THEN
          ! read the old data
          ALLOCATE ( p_tmp(OLDLEN) )
          p_tmp(1:OLDLEN)%ixyz = indx_DB_ptr%p(1:OLDLEN)%ixyz
          p_tmp(1:OLDLEN)%i    = indx_DB_ptr%p(1:OLDLEN)%i
          ! deallocate the old data
          DEALLOCATE ( indx_DB_ptr%p )
       END IF
       IF ( NEWLEN > 0 ) THEN
          ALLOCATE ( indx_DB_ptr%p(1:NEWLEN), STAT=alloc_stat )
          CALL test_alloc( alloc_stat, 'indx_DB()%p in read_solution_aux_1', NEWLEN )
          ! write the old data
          IF (OLDLEN.GT.0) THEN
             indx_DB_ptr%p(1:OLDLEN)%ixyz = p_tmp(1:OLDLEN)%ixyz
             indx_DB_ptr%p(1:OLDLEN)%i    = p_tmp(1:OLDLEN)%i
          END IF
       END IF
       IF (ALLOCATED(p_tmp)) DEALLOCATE (p_tmp)
    END IF
    
    indx_DB_ptr%real_length = NEWLEN
    indx_DB_ptr%length = NEWLEN
    indx_DB_ptr%shift = 0

  END SUBROUTINE allocate_indx_DB

  !--------------------------------------------------------------------------------------------
  ! PRIVATE
  ! set correct iwlt index in indx_DB
  ! and arrange u in order: internal, external points
  SUBROUTINE rearrange_indx_DB_iu (UTMP, UOLD, ISFIRST, OLEN)
    USE field               ! u
    USE wlt_trns_vars       ! indx_DB
    USE wlt_vars            ! dim, j_lev, j_mx
    REAL (pr), POINTER, OPTIONAL :: UTMP(:,:), UOLD(:,:)     ! buffers for u array rearranging
    LOGICAL, INTENT(IN), OPTIONAL :: ISFIRST
    INTEGER, POINTER, OPTIONAL :: OLEN(:,:,:,:)              ! old real_length for index_DB
    INTEGER :: j, wlt_type, face_type, j_df, i, &
         iwlt
    
    IF (ISFIRST) THEN    !---------------- UTMP=>u, UOLD=>0
       NULLIFY (UTMP)
       
    ELSE                 !---------------- UTMP-new, UOLD-old
       iwlt = 0                                                                    ! new running iwlt index
       ! INTERNAL POINTS:
       face_type = (3**dim - 1)/2
       DO j = 1, j_lev
          DO wlt_type = MIN(j-1,1),2**dim-1
             DO j_df = j, j_lev
                !                                                                  ! pass through uold values ...
                DO i=1,OLEN(j_df,wlt_type,face_type,j)
                   iwlt = iwlt + 1
                   u(iwlt,:) = UOLD( indx_DB(j_df,wlt_type,face_type,j)%p(i)%i, :) ! and insert them into u
                   indx_DB(j_df,wlt_type,face_type,j)%p(i)%i = iwlt                ! and correct iwlt index
                END DO
                !                                                                  ! pass through utmp values ...
                DO i=OLEN(j_df,wlt_type,face_type,j)+1,indx_DB(j_df,wlt_type,face_type,j)%length
                   iwlt = iwlt + 1
                   u(iwlt,:) = UTMP( indx_DB(j_df,wlt_type,face_type,j)%p(i)%i, :) ! and insert them into u
                   indx_DB(j_df,wlt_type,face_type,j)%p(i)%i = iwlt                ! and correct iwlt index
                END DO
                !
             END DO
          END DO
       END DO
       ! EXTERNAL POINTS:
       DO j = 1, j_lev
          DO wlt_type = MIN(j-1,1),2**dim-1
             DO face_type = 0, 3**dim - 1
                IF( face_type /= (3**dim-1)/2 ) THEN
                   DO j_df = j, j_lev
                      !                                                                  ! pass through uold values ...
                      DO i=1,OLEN(j_df,wlt_type,face_type,j)
                         iwlt = iwlt + 1
                         u(iwlt,:) = UOLD( indx_DB(j_df,wlt_type,face_type,j)%p(i)%i, :) ! and insert them into u
                         indx_DB(j_df,wlt_type,face_type,j)%p(i)%i = iwlt                ! and correct iwlt index
                      END DO
                      !                                                                  ! pass through utmp values ...
                      DO i=OLEN(j_df,wlt_type,face_type,j)+1,indx_DB(j_df,wlt_type,face_type,j)%length
                         iwlt = iwlt + 1
                         u(iwlt,:) = UTMP( indx_DB(j_df,wlt_type,face_type,j)%p(i)%i, :) ! and insert them into u
                         indx_DB(j_df,wlt_type,face_type,j)%p(i)%i = iwlt                ! and correct iwlt index
                      END DO
                      !
                   END DO
                END IF
             END DO
          END DO
       END DO
    END IF
    
  END SUBROUTINE rearrange_indx_DB_iu
  
  !--------------------------------------------------------------------------------------------
  ! PRIVATE
  ! deallocate indx_DB
  SUBROUTINE deallocate_indx_DB
    USE wlt_trns_vars       ! indx_DB
    USE wlt_vars            ! dim, j_lev, j_mx
    INTEGER :: j, wlt_type, face_type, j_df
    DO j = 1, j_mx
       DO wlt_type = MIN(j-1,1),2**dim - 1
          DO face_type = 0, 3**dim - 1
             DO j_df = 1, j_mx
                IF (ASSOCIATED(indx_DB(j_df,wlt_type,face_type,j)%p)) &
                     DEALLOCATE (indx_DB(j_df,wlt_type,face_type,j)%p)
             END DO
          END DO
       END DO
    END DO
    DEALLOCATE (indx_DB)
    
  END SUBROUTINE deallocate_indx_DB
  

  !--------------------------------------------------------------------------------------------
  ! clean allocated in read_solution()
  SUBROUTINE read_solution_free (scl, KEEP_U, KEEP_NAMES)
    USE pde                       ! u_variable_names, scl_global, n_var_wlt_fmly
    USE variable_mapping          ! n_var 
    USE field                     ! u
    USE wlt_trns_vars             ! indx_DB, xx
    USE io_3D_vars
    USE parallel                  ! IC_par_proc_tree & IC_Nwlt_per_Tree
    REAL (pr) ,  DIMENSION (:), POINTER :: scl
    LOGICAL, INTENT(IN), OPTIONAL :: KEEP_U, KEEP_NAMES
    LOGICAL :: do_keep_u, do_keep_names
    
    do_keep_u = .FALSE.
    do_keep_names = .FALSE.
    IF (PRESENT(KEEP_U)) do_keep_u = KEEP_U
    IF (PRESENT(KEEP_NAMES)) do_keep_names = KEEP_NAMES
    
    IF ( ASSOCIATED(scl) ) DEALLOCATE(scl)
    IF ( ASSOCIATED(scl_global) ) DEALLOCATE(scl_global)
    !IF ( ALLOCATED(n_var_wlt_fmly) ) DEALLOCATE(n_var_wlt_fmly)
    IF (ALLOCATED(xx)) DEALLOCATE (xx)
    IF( ALLOCATED(indx_DB) ) CALL deallocate_indx_DB
    
    IF (.NOT.do_keep_u.AND.ALLOCATED(u)) &
         DEALLOCATE (u)
    !IF (.NOT.do_keep_names.AND.ALLOCATED(u_variable_names)) &  ! ERIC: what to do, since this is used as a marker whether or not things have been set up
    !     DEALLOCATE (u_variable_names)

    IF (ALLOCATED(par_proc_tree))    DEALLOCATE( par_proc_tree )
    IF (ALLOCATED(Nwlt_per_Tree))    DEALLOCATE( Nwlt_per_Tree )
    IF (ALLOCATED(IC_par_proc_tree)) DEALLOCATE( IC_par_proc_tree )
    IF (ALLOCATED(IC_Nwlt_per_Tree)) DEALLOCATE( IC_Nwlt_per_Tree ) 

    IF ( ASSOCIATED( IC_mxyz_global ) ) DEALLOCATE( IC_mxyz_global )
    IF ( ASSOCIATED( IC_prd_global ) ) DEALLOCATE( IC_prd_global )

  END SUBROUTINE read_solution_free
  
  
  !--------------------------------------------------------------------------------------------
  ! create directory (may be system and/or compiler dependent)
  SUBROUTINE directory_create ( dirname, RANK_ZERO_ONLY )
    USE parallel                      ! par_rank
#ifdef IFORT
    USE IFPORT                        ! ifort SYSTEM call
#endif
    CHARACTER (LEN=*), INTENT(IN) :: dirname            ! directory to create
    LOGICAL, INTENT(IN), OPTIONAL :: RANK_ZERO_ONLY     ! run on 0 proc only
    LOGICAL                       :: do_rank_zero_only
    INTEGER                       :: ierr               ! SYSTEM call status / sync flag
    
    do_rank_zero_only = .FALSE.
    IF (PRESENT(RANK_ZERO_ONLY)) do_rank_zero_only = RANK_ZERO_ONLY
    
    ierr = 0
    IF (do_rank_zero_only) THEN
       IF (par_rank .EQ. 0) THEN
#ifdef IFORT
          ierr =  SYSTEM('mkdir -p '//TRIM(dirname))  ! Intel fortran
#else
          CALL SYSTEM('mkdir -p '//TRIM(dirname))     ! PGI fortran
#endif
       END IF
       ! make procs to wait for proc #0
       CALL parallel_broadcast_int( ierr )

    ELSE
       
#ifdef IFORT
       ierr =  SYSTEM('mkdir -p '//TRIM(dirname))  ! Intel fortran
#else
       CALL SYSTEM('mkdir -p '//TRIM(dirname))     ! PGI fortran
#endif 
       
    END IF
    
  END SUBROUTINE directory_create
  
  
END MODULE io_3D
