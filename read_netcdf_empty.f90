MODULE read_netcdf_mod
  IMPLICIT NONE
  
  PUBLIC :: read_netcdf_field_real8, &
       write_netcdf_error_to_stderr
  PRIVATE :: err
  
CONTAINS
  
  
  SUBROUTINE read_netcdf_field_real8(field, dim1, dim2, dim3, filename,fieldname,the_viscosity) 
    IMPLICIT NONE 
    INTEGER dim1, dim2, dim3 
    REAL*8 field(dim1,dim2,dim3) 
    CHARACTER (*) filename, fieldname 
    REAL*8 the_viscosity
    CALL err
  END SUBROUTINE read_netcdf_field_real8
  
  
  SUBROUTINE write_netcdf_error_to_stderr( status , routinename ) 
    INTEGER status 
    CHARACTER (*) routinename 
    INTEGER  error_status
    CHARACTER*80 error_message 
    CALL err
  END SUBROUTINE write_netcdf_error_to_stderr

  
  SUBROUTINE err
    PRINT *,' ERROR, The netcdf library has not been compiled into this executable'
    PRINT *,' ERROR, please install netcdf libraries and change makefile to include'
    PRINT *,' ERROR, netcdf support. Exiting ...'
    STOP
  END SUBROUTINE err


END MODULE read_netcdf_mod
