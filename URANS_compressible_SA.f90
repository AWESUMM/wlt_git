MODULE URANS_compressible_SA
  !
  ! Spalart-Alamaras URANS model for compressible turbulence
  ! Moscow, August 24, 2017
  ! 
  USE precision
  USE input_file_reader
  USE io_3D_vars
  USE pde
  USE variable_mapping 
  USE share_consts
  USE share_kry
  USE util_mod
  USE vector_util_mod
  USE spectra_module
  USE SGS_util 
  USE equations_compressible 
  USE compressible_mean_vars
  USE curvilinear
  USE curvilinear_mesh !physical coordinates
  USE field !u

  PUBLIC :: &
       SA_compressible_additional_vars, &
       SA_compressible_setup, &
       SA_compressible_finalize_setup, &
       SA_compressible_read_input, &
       SA_compressible_rhs, &
       SA_compressible_Drhs, &
       SA_compressible_Drhs_diag, &
       DDES_length_scale, &
       cal_fd, &
       cal_psi, &
       cal_fd_tilde_fe, &
       IDDES_length_scale
  

  REAL (pr), PROTECTED, PUBLIC :: mutil_init
  
  INTEGER, PROTECTED, PUBLIC :: n_var_distw                  ! location of nearest wall distance field in u array

  ! Spalart-Allmaras based DDES model
  LOGICAL, PROTECTED, PUBLIC :: user_DDES
  !INTEGER :: n_var_rd_tmp
  INTEGER, PROTECTED, PUBLIC :: n_var_fd    

  !REAL (pr), DIMENSION (:) , ALLOCATABLE, PUBLIC:: distw_t !true wall distance. It is allocated and calcualted in the user_case module.
  REAL (pr), DIMENSION (:) , ALLOCATABLE, PUBLIC:: h_max   !max(delta_x, delta_y, delta_z). It is allocated and calcualted in the user_case module.


  ! Spalart-Allmaras based IDDES model
  LOGICAL, PROTECTED, PUBLIC :: user_IDDES
  INTEGER, PROTECTED, PUBLIC :: n_var_fdt   
  INTEGER, PROTECTED, PUBLIC :: n_var_fB    
  INTEGER, PROTECTED, PUBLIC :: n_var_fe    
  INTEGER, PROTECTED, PUBLIC :: n_var_fd_tilde 

  ! Reynolds-averaged variables
  INTEGER, PROTECTED, PUBLIC :: n_var_MT_avg

  INTEGER, PROTECTED, PUBLIC, ALLOCATABLE :: &
       n_stress_mdl(:), &   ! Reynolds stresses modeled
       n_stress_avg_mdl(:)  ! Reynolds stresses modeled - Ensemble (Reynolds) Average

  PRIVATE

  LOGICAL   :: adaptMT

  REAL (pr) :: mutil_min
  REAL (pr) :: C0_RANS, C1_RANS, C2_RANS, C3_RANS, C4_RANS, C5_RANS, C6_RANS, C7_RANS, C8_RANS, C9_RANS, C10_RANS     ! constant parameters

  REAL (pr), DIMENSION (:) , ALLOCATABLE :: nuK
  REAL (pr), DIMENSION (:) , ALLOCATABLE :: ASTAR, BSTAR, CSTAR            
  REAL (pr), DIMENSION (:) , ALLOCATABLE :: nutK 

  REAL(pr):: Psgs_diss                 ! Percent SGS dissipation

  REAL (pr) :: floor

CONTAINS

  SUBROUTINE SA_compressible_additional_vars( t_local, flag )
    !USE field
    IMPLICIT NONE

    REAL (pr), INTENT (IN) ::  t_local
    INTEGER , INTENT(IN) :: flag ! 0- called during adaption to IC, 1 called during main integration loop
    LOGICAL, SAVE :: first_call = .TRUE.

    INTEGER :: idim, nS
    REAL (pr) :: dt_avg, relax_avg

    REAL (pr), DIMENSION(nwlt,dim) :: v
    REAL (pr), DIMENSION(dim, nwlt, dim) :: du, d2u
    REAL (pr), DIMENSION (nwlt,dim*(dim+1)/2) :: S_ij, S0_ij
    REAL (pr), DIMENSION(nwlt) :: temp, props, chi, fv1, Smod
    INTEGER :: i
    INTEGER, PARAMETER :: meth=1 ! used meth=1 to match calc_vort()

    !CALL nearest_wall_dist( nwlt, u(:,n_var_distw) ) 
    ! move to case file

    temp(:) = (u(:,n_eng)-0.5_pr*SUM(u(:,n_mom(1):n_mom(dim))**2,DIM=2)/u(:,n_den))/cv_in(Nspec)/u(:,n_den)/F_compress
    ! constant viscosity
    props(:) = mu_in(Nspec)  ! Nspec=1
    ! variable viscosity
    IF ( viscmodel .EQ. viscmodel_sutherland ) THEN  ! mu_in*(1.0_pr+sutherland_const)/(v(:,dimp1)+sutherland_const)*(v(:,dimp1))**(1.5_pr)
       props(:) = mu_in(Nspec) * calc_nondim_sutherland_visc( temp(:), nwlt, sutherland_const )
    END IF
    chi(:) = MAX(0.0_pr,u(:,n_var_MT)) / props(:)
    fv1(:) = chi(:)**3/(chi(:)**3+C6_RANS**3)
    u(:, n_var_MTB) = MAX(0.0_pr, u(:,n_var_MT))*fv1(:)
    
    IF ( saveReyStrMdl ) THEN
       DO i = 1, dim
          v(:,i) = u(:,n_mom(i)) / u(:,n_den)       ! u_i
       END DO
       CALL c_diff_fast( v(:,1:dim), du(1:dim,:,:), d2u(1:dim,:,:), j_lev, nwlt, meth, 10, dim, 1, dim )  
       CALL Sij_velgrad( du(1:dim,:,1:dim), nwlt, S_ij, Smod, .FALSE.)
       nS = dim*(dim+1)/2               ! velocity gradient

       S0_ij = S_ij
       Smod = SUM( S0_ij(:,1:dim), DIM=2 ) / 3.0_pr
       DO idim = 1, dim	
          S0_ij(:,idim) = S0_ij(:,idim) - Smod 
       END DO

       Smod =  SQRT( 2.0_pr*SUM( S0_ij(:,1:dim)**2, DIM=2) + &
                     4.0_pr*SUM( S0_ij(:,dim+1:nS)**2, DIM=2) )

       ! Reynolds stresses (rho*tau_ij)
       DO idim = 1, nS
          u(:,n_stress_mdl(idim)) = 2.0_pr*u(:,n_var_MTB)*S0_ij(:,idim)
       END DO
    END IF !saveReyStrMdl

    ! Reynolds averaged vars

    dt_avg = ABS( t_local - avg_timestart )
    IF (exp_avg) THEN
      relax_avg = dt / avg_timescale
    ELSE
      relax_avg = dt / (dt_avg + dt)
    END IF

    IF (saveReyAvgRANS ) THEN
      IF( (restart_ReyAvg .OR. IC_restart_mode==0) .AND. first_call ) THEN

        IF (saveCfyPlus) THEN 
        u(:,n_var_cf_avg) = u(:,n_var_cf)
        u(:,n_var_yPlus_avg) = u(:,n_var_yPlus)
        END IF

        u(:,n_var_MT_avg) = u(:,n_var_MT)
        u(:,n_var_MTB_avg) = u(:,n_var_MTB)
      END IF

      IF (saveCfyPlus) THEN 
      u(:,n_var_cf_avg) =    ( 1.0_pr - relax_avg ) * u(:,n_var_cf_avg)    + ( relax_avg ) * u(:,n_var_cf) 
      u(:,n_var_yPlus_avg) = ( 1.0_pr - relax_avg ) * u(:,n_var_yPlus_avg) + ( relax_avg ) * u(:,n_var_yPlus)
      END IF

      u(:,n_var_MT_avg) =  ( 1.0_pr - relax_avg ) * u(:,n_var_MT_avg)  + ( relax_avg ) * u(:,n_var_MT)   
      u(:,n_var_MTB_avg) = ( 1.0_pr - relax_avg ) * u(:,n_var_MTB_avg) + ( relax_avg ) * u(:,n_var_MTB) 
    END IF !saveReyAvgRANS

    IF ( saveReyStrMdl ) THEN
      IF( (restart_ReyAvg .OR. IC_restart_mode==0) .AND. first_call ) THEN
        DO idim = 1, nS
           u(:,n_stress_avg_mdl(idim)) = u(:,n_stress_mdl(idim)) 
        END DO
      END IF

      DO idim = 1, nS
         u(:,n_stress_avg_mdl(idim)) = ( 1.0_pr - relax_avg ) * u(:,n_stress_avg_mdl(idim)) + ( relax_avg )  * u(:,n_stress_mdl(idim)) 
      END DO
    END IF !saveReyStrMdl


   first_call = .FALSE.

  END SUBROUTINE SA_compressible_additional_vars

  
  SUBROUTINE  SA_compressible_setup ( floor_local )
  ! In SGS_setup() we setup all the varaibles necessary for SGS 
  ! The following variables must be setup in this routine: n_var_SGS
    USE parallel
    IMPLICIT NONE

    REAL (pr), INTENT(IN) :: floor_local

    floor = floor_local

    !register_var( var_name, integrated, adapt, interpolate, exact, saved, req_restart, FRONT_LOAD )

    CALL register_var( 'Distw', integrated=.FALSE., adapt=(/.FALSE.,.FALSE./), interpolate=(/.TRUE.,.TRUE./), exact=(/.FALSE.,.FALSE./), saved=.TRUE., req_restart=.TRUE. )
    IF (saveCfyPlus) THEN 
      CALL register_var( 'Cf', integrated=.FALSE., adapt=(/.FALSE.,.FALSE./), interpolate=(/.TRUE.,.TRUE./), exact=(/.FALSE.,.FALSE./), saved=.TRUE., req_restart=.TRUE. )
      CALL register_var( 'Yplus', integrated=.FALSE., adapt=(/.FALSE.,.FALSE./), interpolate=(/.TRUE.,.TRUE./), exact=(/.FALSE.,.FALSE./), saved=.TRUE., req_restart=.TRUE. )
      ! Reynolds Averaged integrated variables
      IF (saveReyAvgRANS ) THEN
        CALL register_var( 'Cf_AVG', integrated=.FALSE., adapt=(/.FALSE.,.FALSE./), interpolate=(/.TRUE.,.TRUE./), &
                             exact=(/.FALSE.,.FALSE./), saved=.TRUE., req_restart=.NOT.restart_ReyAvg )
        CALL register_var( 'Yplus_AVG', integrated=.FALSE., adapt=(/.FALSE.,.FALSE./), interpolate=(/.TRUE.,.TRUE./), &
                             exact=(/.FALSE.,.FALSE./), saved=.TRUE., req_restart=.NOT.restart_ReyAvg )
      END IF
    END IF


    IF (saveReyStrMdl ) THEN
    ! Reynolds stresses modeled
      CALL register_var( 'XX_Stress_MDL  ',  integrated=.FALSE.,  adapt=(/.FALSE.,.FALSE./),   interpolate=(/.TRUE.,.TRUE./), &  
          exact=(/.FALSE.,.FALSE./),   saved=.FALSE., req_restart=.NOT.restart_ReyAvg )
      CALL register_var( 'XX_Stress_AVG_MDL  ',  integrated=.FALSE.,  adapt=(/.FALSE.,.FALSE./),   interpolate=(/.TRUE.,.TRUE./), &  
          exact=(/.FALSE.,.FALSE./),   saved=.TRUE., req_restart=.NOT.restart_ReyAvg )
      IF( dim .GE. 2 ) THEN
        CALL register_var( 'XY_Stress_MDL  ',  integrated=.FALSE.,  adapt=(/.FALSE.,.FALSE./),   interpolate=(/.TRUE.,.TRUE./), &  
            exact=(/.FALSE.,.FALSE./),   saved=.FALSE., req_restart=.NOT.restart_ReyAvg )
        CALL register_var( 'XY_Stress_AVG_MDL  ',  integrated=.FALSE.,  adapt=(/.FALSE.,.FALSE./),   interpolate=(/.TRUE.,.TRUE./), &  
            exact=(/.FALSE.,.FALSE./),   saved=.TRUE., req_restart=.NOT.restart_ReyAvg )
        CALL register_var( 'YY_Stress_MDL  ',  integrated=.FALSE.,  adapt=(/.FALSE.,.FALSE./),   interpolate=(/.TRUE.,.TRUE./), &  
            exact=(/.FALSE.,.FALSE./),   saved=.FALSE., req_restart=.NOT.restart_ReyAvg )
        CALL register_var( 'YY_Stress_AVG_MDL  ',  integrated=.FALSE.,  adapt=(/.FALSE.,.FALSE./),   interpolate=(/.TRUE.,.TRUE./), &  
            exact=(/.FALSE.,.FALSE./),   saved=.TRUE., req_restart=.NOT.restart_ReyAvg )
      END IF
      IF( dim .GE. 3 ) THEN
        CALL register_var( 'XZ_Stress_MDL  ',  integrated=.FALSE.,  adapt=(/.FALSE.,.FALSE./),   interpolate=(/.TRUE.,.TRUE./), &  
            exact=(/.FALSE.,.FALSE./),   saved=.FALSE., req_restart=.NOT.restart_ReyAvg )
        CALL register_var( 'XZ_Stress_AVG_MDL  ',  integrated=.FALSE.,  adapt=(/.FALSE.,.FALSE./),   interpolate=(/.TRUE.,.TRUE./), &  
            exact=(/.FALSE.,.FALSE./),   saved=.TRUE., req_restart=.NOT.restart_ReyAvg )
        CALL register_var( 'YZ_Stress_MDL  ',  integrated=.FALSE.,  adapt=(/.FALSE.,.FALSE./),   interpolate=(/.TRUE.,.TRUE./), &  
            exact=(/.FALSE.,.FALSE./),   saved=.FALSE., req_restart=.NOT.restart_ReyAvg )
        CALL register_var( 'YZ_Stress_AVG_MDL  ',  integrated=.FALSE.,  adapt=(/.FALSE.,.FALSE./),   interpolate=(/.TRUE.,.TRUE./), &  
            exact=(/.FALSE.,.FALSE./),   saved=.TRUE., req_restart=.NOT.restart_ReyAvg )
        CALL register_var( 'ZZ_Stress_MDL  ',  integrated=.FALSE.,  adapt=(/.FALSE.,.FALSE./),   interpolate=(/.TRUE.,.TRUE./), &  
            exact=(/.FALSE.,.FALSE./),   saved=.FALSE., req_restart=.NOT.restart_ReyAvg )
        CALL register_var( 'ZZ_Stress_AVG_MDL  ',  integrated=.FALSE.,  adapt=(/.FALSE.,.FALSE./),   interpolate=(/.TRUE.,.TRUE./), &  
            exact=(/.FALSE.,.FALSE./),   saved=.TRUE., req_restart=.NOT.restart_ReyAvg )
      END IF
    END IF

    n_var_SGS = 1 ! one additional scalar rho*nutil
    CALL register_var( 'mutil', integrated=.TRUE., adapt=(/adaptMT,adaptMT/), interpolate=(/.TRUE.,.TRUE./), exact=(/.FALSE.,.FALSE./), saved=.TRUE., req_restart=.TRUE. )
    CALL register_var( 'mut', integrated=.FALSE., adapt=(/.FALSE.,.FALSE./), interpolate=(/.TRUE.,.TRUE./), exact=(/.FALSE.,.FALSE./), saved=.TRUE., req_restart=.FALSE. )
    IF(user_DDES) THEN
       !CALL register_var( 'rd', integrated=.FALSE., adapt=(/.FALSE.,.FALSE./), interpolate=(/.TRUE.,.TRUE./), exact=(/.FALSE.,.FALSE./), saved=.TRUE., req_restart=.FALSE. )
       CALL register_var( 'fd', integrated=.FALSE., adapt=(/.FALSE.,.FALSE./), interpolate=(/.TRUE.,.TRUE./), exact=(/.FALSE.,.FALSE./), saved=.TRUE., req_restart=.FALSE. )
    END IF
    
    IF(user_IDDES) THEN
       CALL register_var( 'fdt', integrated=.FALSE., adapt=(/.FALSE.,.FALSE./), interpolate=(/.TRUE.,.TRUE./), exact=(/.FALSE.,.FALSE./), saved=.TRUE., req_restart=.FALSE. )
       CALL register_var( 'fB', integrated=.FALSE., adapt=(/.FALSE.,.FALSE./), interpolate=(/.TRUE.,.TRUE./), exact=(/.FALSE.,.FALSE./), saved=.TRUE., req_restart=.FALSE. )
       CALL register_var( 'fe', integrated=.FALSE., adapt=(/.FALSE.,.FALSE./), interpolate=(/.TRUE.,.TRUE./), exact=(/.FALSE.,.FALSE./), saved=.TRUE., req_restart=.FALSE. )
       CALL register_var( 'fd_tilde', integrated=.FALSE., adapt=(/.FALSE.,.FALSE./), interpolate=(/.TRUE.,.TRUE./), exact=(/.FALSE.,.FALSE./), saved=.TRUE., req_restart=.FALSE. )
    END IF
    
    
    ! Reynolds Averaged integrated variables
    IF (saveReyAvgRANS ) THEN
       CALL register_var( 'mutil_AVG', integrated=.FALSE., adapt=(/.FALSE.,.FALSE./), interpolate=(/.TRUE.,.TRUE./), &
            exact=(/.FALSE.,.FALSE./), saved=.TRUE., req_restart=.NOT.restart_ReyAvg )
       CALL register_var( 'mut_AVG', integrated=.FALSE., adapt=(/.FALSE.,.FALSE./), interpolate=(/.TRUE.,.TRUE./), &
            exact=(/.FALSE.,.FALSE./), saved=.TRUE., req_restart=.NOT.restart_ReyAvg )
    END IF
    
  END SUBROUTINE SA_compressible_setup

  ! get the indices alloted to sgs variables
  SUBROUTINE  SA_compressible_finalize_setup ()
    USE parallel
    IMPLICIT NONE
    INTEGER :: i

    n_var_distw = get_index('Distw           ' )

    IF (saveCfyPlus) THEN 
       n_var_cf = get_index('Cf           ' )
       n_var_yPlus = get_index('Yplus           ' )
       IF ( saveReyAvgRANS ) THEN
          n_var_cf_avg = get_index('Cf_AVG')
          n_var_yPlus_avg = get_index('Yplus_AVG')
       END IF
    END IF

    IF ( saveReyStrMdl ) THEN
      ! Reynolds stresses modeled
      IF (ALLOCATED(n_stress_mdl)) DEALLOCATE(n_stress_mdl)
      IF (ALLOCATED(n_stress_avg_mdl)) DEALLOCATE(n_stress_avg_mdl)
      ALLOCATE( n_stress_mdl(dim*(dim+1)/2) )
      ALLOCATE( n_stress_avg_mdl(dim*(dim+1)/2) )
      IF( dim .EQ. 2 )THEN
          n_stress_mdl(1)  = get_index('XX_Stress_MDL') 
          n_stress_mdl(2)  = get_index('YY_Stress_MDL') 
          n_stress_mdl(3)  = get_index('XY_Stress_MDL') 
          n_stress_avg_mdl(1)  = get_index('XX_Stress_AVG_MDL') 
          n_stress_avg_mdl(2)  = get_index('YY_Stress_AVG_MDL') 
          n_stress_avg_mdl(3)  = get_index('XY_Stress_AVG_MDL') 
      END IF
      IF( dim .EQ. 3 )THEN
          n_stress_mdl(1)  = get_index('XX_Stress_MDL') 
          n_stress_mdl(2)  = get_index('YY_Stress_MDL') 
          n_stress_mdl(3)  = get_index('ZZ_Stress_MDL') 
          n_stress_mdl(4)  = get_index('XY_Stress_MDL') 
          n_stress_mdl(5)  = get_index('XZ_Stress_MDL') 
          n_stress_mdl(6)  = get_index('YZ_Stress_MDL') 
          n_stress_avg_mdl(1)  = get_index('XX_Stress_AVG_MDL') 
          n_stress_avg_mdl(2)  = get_index('YY_Stress_AVG_MDL') 
          n_stress_avg_mdl(3)  = get_index('ZZ_Stress_AVG_MDL') 
          n_stress_avg_mdl(4)  = get_index('XY_Stress_AVG_MDL') 
          n_stress_avg_mdl(5)  = get_index('XZ_Stress_AVG_MDL') 
          n_stress_avg_mdl(6)  = get_index('YZ_Stress_AVG_MDL') 
      END IF
    END IF

    n_var_MT = get_index('mutil           ' )
    n_var_MTB = get_index('mut           ' )
    IF(user_DDES) THEN
       !n_var_rd_tmp = get_index('rd           ' )
       n_var_fd = get_index('fd           ' )
    END IF
    IF(user_IDDES) THEN
       n_var_fdt = get_index('fdt           ' )
       n_var_fB = get_index('fB           ' )
       n_var_fe = get_index('fe           ' )
       n_var_fd_tilde = get_index('fd_tilde           ' )
    END IF
    IF ( saveReyAvgRANS ) THEN
       n_var_MT_avg = get_index('mutil_AVG')
       n_var_MTB_avg = get_index('mut_AVG')
    END IF

    scaleCoeff(n_var_MT)  = 1.0_pr
    scaleCoeff(n_var_MTB)  = 1.0_pr

  END SUBROUTINE SA_compressible_finalize_setup


  SUBROUTINE  SA_compressible_read_input()
  !
  ! read variables from input file for this user case 
  !
    USE parallel
    IMPLICIT NONE
    
    INTEGER   :: io_status, eps_status
    REAL (pr) :: tmp_eps     
    
    
    adaptMT = .FALSE.
    call input_logical ('adaptMT',adaptMT,'default', &
         'adaptMT, Adapt on mutil')

    saveCfyPlus = .FALSE.
    call input_logical ('saveCfyPlus',saveCfyPlus,'default','saveCfyPlus: if T adds and saves Cf and yPlus fields')
    
    !SA specific constants
    ! DDES model
    user_DDES = .FALSE.
    call input_logical ('user_DDES', user_DDES, 'default', & 
         'user_DDES: default is F. T to switch on Spalart-Allmaras based DDES model' )
    
    ! IDDES model
    user_IDDES = .FALSE.
    call input_logical ('user_IDDES', user_IDDES, 'default', & 
         'user_IDDES: default is F. T to switch on Spalart-Allmaras based IDDES model' )
    
    mutil_init = 0.00001_pr
    call input_real ('mutil_init_SA',mutil_init,'default', &
         'mutil_init, initial positive value for mutil')
    
    mutil_min = 0.000001_pr
    call input_real ('mutil_min_SA',mutil_min,'default', &
         'mutil_min, minimum value for mutil')
    
    C0_RANS = 0.1355_pr
    call input_real ('C0_RANS_SA',C0_RANS,'default', &
         'C_b1, multiplicative ceoff. in the source and sink terms of mutil equation')
    
    C1_RANS = 0.622_pr
    call input_real ('C1_RANS_SA',C1_RANS,'default', &
         'C_b2, multiplicative coeff. for the cross-diffusion of mutil equation')
    
    C2_RANS = 2.0_pr/3.0_pr
    call input_real ('C2_RANS_SA',C2_RANS,'default', &
         'sigma, constant parameter for the diffusion and cross-diffusion of mutil  equation')
    
    C3_RANS = 0.41_pr
    call input_real ('C3_RANS_SA',C3_RANS,'default', &
         'kappa, constant parameter in S_hat, f_w and multiplicative coeff. for f_t2 in the source and sink terms of mutil equation')
    
    C4_RANS = 0.3_pr
    call input_real ('C4_RANS_SA',C4_RANS,'default', &
         'C_w2, constant parameter in function g for function f_w in the sink term of mutil equation')
    
    C5_RANS = 2.0_pr
    call input_real ('C5_RANS_SA',C5_RANS,'default', &
         'C_w3, constant parameter in function f_w in the sink term of mutil equation')
    
    C6_RANS = 7.1_pr
    call input_real ('C6_RANS_SA',C6_RANS,'default', &
         'C_v1, constant parameter in function f_v1 for f_v2 in S_hat and for definition of eddy viscosity')
    
    C7_RANS = 1.2_pr
    call input_real ('C7_RANS_SA',C7_RANS,'default', &
         'C_t3, constant parameter in function f_t2 in the source and sink terms of mutil equation')
    
    C8_RANS = 0.5_pr
    call input_real ('C8_RANS_SA',C8_RANS,'default', &
         'C_t4, constant parameter in function f_t2 in the source and sink terms of mutil equation')
    
    C9_RANS = 8.0_pr/9.0_pr
    call input_real ('C9_RANS_SA',C9_RANS,'default', &
         'Pr_T, turbulent Prandtl number')
    
    C10_RANS = 0.09_pr
    call input_real ('C10_RANS_SA',C10_RANS,'default', &
         'C_mu, constant parameter in definition of turbulence dissipation rate for energy equation')
    
  END SUBROUTINE  SA_compressible_read_input
   
  SUBROUTINE SA_compressible_rhs ( int_rhs, u_integrated, meth )
    !
    !-- Form RHS of SA model equations
    !
    IMPLICIT NONE
    REAL (pr), DIMENSION (ng*ne), INTENT(INOUT) :: int_rhs
    REAL (pr), DIMENSION (ng,ne), INTENT(INOUT)    :: u_integrated ! 1D flat version of integrated variables without BC 
    INTEGER, INTENT(IN) :: meth 

    INTEGER                                 :: i, j, k, idim, shift, tempintd, jshift
    INTEGER                                 :: dimp1, dimp2, dimp3, nS
    INTEGER, DIMENSION(dim,dim)             :: ij2k
    REAL (pr), DIMENSION (ng*ne)            :: inc_rhs          ! incremental rhs
    REAL (pr), DIMENSION (ne*dim,ng,dim)    :: du, d2u
    REAL (pr), DIMENSION (ng,ne*dim)        :: F
    REAL (pr), DIMENSION (ng)               :: Smod, Wmod, cross1, cross2, chi, mu_t, ft2
    REAL (pr), DIMENSION (ng)               :: S_hat, fv1, fv2
    REAL (pr), DIMENSION (ng)               :: fw, gfunc, rfunc
    REAL (pr), DIMENSION (ng,dim*(dim+1)/2) :: S_ij, S0_ij, tau_ij
    REAL (pr), DIMENSION (ng,dim+3)         :: v               ! velocity components + T + rho + nu_til
    REAL (pr), DIMENSION (ng)               :: props           ! nondimensional dynamic viscosity (1/Re*Sutherland)

    REAL (pr)                               :: tmp1, tmp2, C_w1      ! print
    LOGICAL, SAVE :: head_flag = .TRUE. 

    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc
    INTEGER :: wlt_type, j_df, imin, i_bnd

    LOGICAL, SAVE :: header_flag = .TRUE.
    REAL (pr), ALLOCATABLE, DIMENSION(:,:), SAVE :: bnd_coords_btm
    INTEGER, SAVE :: n_coords_btm = 0
    REAL (pr), SAVE :: R1=0.0_pr, R2=0.0_pr
    REAL (pr), DIMENSION(dim) :: normal_dir !inward unit normal vector at a certain points of bnd_coords
    REAL (pr), DIMENSION(dim) :: tang_dir   !unit tangential vector in flow direction at a certain points of bnd_coords
    REAL (pr), DIMENSION (dim, ng, dim) :: dux
    REAL (pr), DIMENSION (dim, ng, dim) :: dux_dummy

    ! ============= SANITY CHECKS =======================================
    IF(Nspec > 1) THEN
       IF (par_rank.EQ.0) PRINT*,'ERROR: the URANS module is not set up for multiple species, requires modification'
       STOP
    END IF 
    IF(MAXVAL(ABS(int_rhs(n_eng*ng+1:ne*ng))) > 0.0_pr) THEN
      IF(par_rank .EQ. 0) PRINT *,'ERROR URANS compressible rhs: int_rhs /= 0'
      STOP
    END IF
    !===================================================================
    
    dimp1 = dim + 1                  ! temperature
    dimp2 = dim + 2                  ! density
    dimp3 = dim + 3                  ! nu_til
    nS = dim*(dim+1)/2               ! velocity gradient

    u_integrated(:,n_var_MT) = MAX(0.0_pr,u_integrated(:,n_var_MT))

    DO i = 1, dim
        v(:,i) = u_integrated(:,n_mom(i)) / u_integrated(:,n_den)       ! u_i
    END DO

    ! temperature
    v(:,dimp1) = (u_integrated(:,n_eng)-0.5_pr*SUM(u_integrated(:,n_mom(1):n_mom(dim))**2,DIM=2)/u_integrated(:,n_den))/cv_in(Nspec)/u_integrated(:,n_den)/F_compress


    ! density - only needed in cross2
    v(:,dimp2) = u_integrated(:,n_den) 

    ! nu_til
    v(:,dimp3) = MAX(0.0_pr,u_integrated(:,n_var_MT)) / u_integrated(:,n_den) ! clipping negative nu_til

    ! constant viscosity
    props(:) = mu_in(Nspec)  ! Nspec=1
    ! variable viscosity
    IF ( viscmodel .EQ. viscmodel_sutherland ) THEN  ! mu_in*(1.0_pr+sutherland_const)/(v(:,dimp1)+sutherland_const)*(v(:,dimp1))**(1.5_pr)
       props(:) = mu_in(Nspec) * calc_nondim_sutherland_visc( v(:,dimp1), nwlt, sutherland_const )
    END IF

!    DO i=1,ne
!       PRINT *, 'rhs_in:', MINVAL(ABS(int_rhs((i-1)*ng+1:i*ng))), MAXVAL(ABS(int_rhs((i-1)*ng+1:i*ng)))
!    END DO
!    PRINT *, 'URANS coeffs:', C0_RANS, C1_RANS, C2_RANS, C3_RANS, C4_RANS, C5_RANS, C6_RANS, C7_RANS
!    PRINT *, 'props:', mu_in(Nspec), cp_in(Nspec), cv_in(Nspec), R_in(Nspec), kk_in(Nspec)
!    WRITE(*,'("min(k)=",E12.5," max(k)=",E12.5," min(omega)=",E12.5," max(omega)=",E12.5)') MINVAL(v(:,dimp2)),MAXVAL(v(:,dimp2)),MINVAL(v(:,dimp3)),MAXVAL(v(:,dimp3))
!    WRITE(*,'("min(T)=    ",E12.5," max(T)=    ",E12.5)') MINVAL(v(:,dimp1)),MAXVAL(v(:,dimp1))
!    PRINT *, 'min|T|=',MINVAL(ABS(v(:,dimp1))), 'max|T|=',MAXVAL(ABS(v(:,dimp1)))


    !IF (verb_level.GT.0) THEN
    IF (BTEST(verb_level,4)) THEN
       tmp1 = MINVAL( v(:,dimp3) )
       tmp2 = MAXVAL( v(:,dimp3) )
       CALL parallel_global_sum( REALMINVAL=tmp1 )
       CALL parallel_global_sum( REALMAXVAL=tmp2 )
       IF (par_rank.EQ.0) PRINT *,'MINMAX nu_til(:)=', tmp1, tmp2
    END IF


    CALL c_diff_fast( v(:,1:dimp3), du(1:dimp3,:,:), d2u(1:dimp3,:,:), j_lev, ng, meth, 10, dimp3, 1, dimp3 )  

    CALL Sij_velgrad( du(1:dim,:,1:dim), ng, S_ij,  Smod, .FALSE.)      ! strain-rate         

    S0_ij = S_ij
    Smod = SUM( S0_ij(:,1:dim), DIM=2 ) / 3.0_pr
    DO idim = 1, dim	
       S0_ij(:,idim) = S0_ij(:,idim) - Smod 
    END DO

    Smod =  SQRT( 2.0_pr*SUM( S0_ij(:,1:dim)**2, DIM=2) + &
                  4.0_pr*SUM( S0_ij(:,dim+1:nS)**2, DIM=2) )

    CALL cal_magvort_velgrad( du(1:dim,:,1:dim), Wmod, ng )            ! magnitude of rotation-rate or vorticity

    ! cross terms in mu_til equation
    cross1(:) = C1_RANS/C2_RANS*SUM(du(dimp3,:,1:dim)*du(dimp3,:,1:dim),DIM=2)*u_integrated(:,n_den)
    cross2(:) = (props(:)+u_integrated(:,n_var_MT))/(C2_RANS*u_integrated(:,n_den))*SUM(du(dimp2,:,1:dim)*du(dimp3,:,1:dim),DIM=2)

    ! chi, distw, fv1, fv2, S_hat, rfunc, gfunc, fw,  ft2, mu_t
    
    chi(:) = v(:,dimp3)/props(:)*u_integrated(:,n_den)

    !u(:,n_var_distw) = SQRT( SUM( u(:,n_map(1:dim))**2, DIM=2 ) ) - xyzlimits(1,1) ! Xuan - only works for circular cylinder geometry!

    u(:,n_var_distw) = MAX ( u(:,n_var_distw), floor )

    fv1(:) = chi(:)**3/(chi(:)**3+C6_RANS**3)

    fv2(:) = 1.0_pr - chi(:)/(1.0_pr+chi(:)*fv1(:))

    S_hat(:) = MAX(0.3_pr*Wmod(:) + floor, Wmod(:) + v(:,dimp3)/(C3_RANS*u(:,n_var_distw))**2*fv2(:))

    rfunc(:) = MIN( v(:,dimp3)/(S_hat(:)*(C3_RANS*u(:,n_var_distw))**2), 10.0_pr )

    gfunc(:) = rfunc(:) + C4_RANS*(rfunc(:)**6-rfunc(:))

    fw(:) = gfunc(:) * ( (1.0_pr+C5_RANS**6)/(gfunc(:)**6+C5_RANS**6) )**(1.0_pr/6.0_pr)

    ft2(:) = C7_RANS*exp(-C8_RANS*chi(:)**2)

    mu_t(:) = u_integrated(:,n_den)*v(:,dimp3)*fv1(:)

!------------------- Begin special treatments for inviscid top and adiabatic bottom BCs, not a generalized implementation. Needs improvement!------------------
       IF (inviscidTop) THEN
          !Imposing inviscid condition at the top wall
          i_p_face(0) = 1
          DO i=1,dim
             i_p_face(i) = i_p_face(i-1)*3
          END DO
          DO face_type = 0, 3**dim - 1
             face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
             IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
                CALL get_all_indices_by_face (face_type, j_lev, nloc, iloc)
                IF(nloc > 0 ) THEN 
                  IF( face(2) == 1 ) THEN ! inviscid top wall
                         mu_t(iloc(1:nloc)) = 0.0_pr
                  END IF
                END IF
             END IF
          END DO
       END IF

       !Modify temperature derivatives to impose adiabatic BC dT/dn =0. The current version is ONLY valid for 2D geometry.
       IF (adiabaticBtm .AND. time_integration_method /= TIME_INT_Crank_Nicholson) THEN

          !!NOTE: this jobcobian matrix dux = dx_i/dxi_i is incorrect for H-type periodic boundary condition
          !CALL c_diff_fast (u(1:ng, n_map(1:dim)), dux(1:dim,1:ng,1:dim), dux_dummy, j_lev, ng, meth, 10, dim, 1, dim, FORCE_RECTILINEAR = .TRUE.)
          !DO idim = 1, dim
          !  IF( transform_dir(idim) .NE. 1 ) THEN  ! account for non-transformed directions
          !    dux(1:dim,:,idim) = 0.0_pr
          !    dux(idim,:,1:dim) = 0.0_pr
          !    dux(idim,:,idim)  = 1.0_pr
          !  END IF
          !END DO

          DO j = 1, j_lev
             i_p_face(0) = 1
             DO i=1,dim
                i_p_face(i) = i_p_face(i-1)*3
             END DO
             DO face_type = 0, 3**dim - 1
                face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
                IF( face(2) == -1 ) THEN                !adiabatic bottom wall
                   DO wlt_type = MIN(j-1,1),2**dim-1
                      DO j_df = j, j_lev
                         DO k = 1, indx_DB(j_df,wlt_type,face_type,j)%length
                            i_bnd = indx_DB(j_df,wlt_type,face_type,j)%p(k)%i

                              normal_dir = 0.0_pr
                              tang_dir = 0.0_pr
                              tang_dir(1) = curvilinear_coordgrad(1,1,i_bnd)/sqrt(curvilinear_coordgrad(1,1,i_bnd)**2+curvilinear_coordgrad(2,1,i_bnd)**2) !normalized dx/dxi
                              tang_dir(2) = curvilinear_coordgrad(2,1,i_bnd)/sqrt(curvilinear_coordgrad(1,1,i_bnd)**2+curvilinear_coordgrad(2,1,i_bnd)**2) !normalized dy/dxi
                              normal_dir(1) = - tang_dir(2)
                              normal_dir(2) =   tang_dir(1)
                              du(dimp1,i_bnd,2) = -du(dimp1,i_bnd,1)*normal_dir(1)/SIGN(MAX(floor,ABS(normal_dir(2))), normal_dir(2)) !nx*dT/dx+ny*dT/dy=0

                         END DO
                      END DO
                   END DO
                END IF
             END DO
          END DO
       END IF
!------------------- End special treatments for inviscid top and adiabatic bottom BCs, not a generalized implementation. Needs improvement!------------------

    !u(:,n_var_MTB) = mu_t(:)

    !IF (verb_level.GT.0) THEN
    IF (BTEST(verb_level,4)) THEN
       tmp1 = MINVAL( mu_t(:) )
       tmp2 = MAXVAL( mu_t(:) )
       CALL parallel_global_sum( REALMINVAL=tmp1 )
       CALL parallel_global_sum( REALMAXVAL=tmp2 )
       IF (par_rank.EQ.0) PRINT *,'MINMAX mu_t(:)=', tmp1, tmp2
       CALL cpu_time(tmp1)
       IF (par_rank.EQ.0) PRINT *,'cpu time=', tmp1
    END IF


    ! Reynolds stresses (rho*tau_ij)
    DO idim = 1, nS
       tau_ij(:,idim) = 2.0_pr*mu_t(:)*S0_ij(:,idim)
    END DO
    !DO idim = 1, dim ! Xuan - this part is igored in the SA model since k is
    !not available
!       tau_ij(:,idim) = tau_ij(:,idim) - 2.0_pr/REAL(dim,pr)*u_integrated(:,n_var_K)
    !   tau_ij(:,idim) = tau_ij(:,idim) - 2.0_pr/REAL(dim,pr)*MAX(0.0_pr,u_integrated(:,n_var_K))
    !END DO

    ! mapping from (i,j) to 1:nS
    k = dim
    DO i = 1, dim
       ij2k(i,i) = i 
       DO j = i+1, dim
          k = k+1
          ij2k(i,j) = k
          ij2k(j,i) = k
       END DO
    END DO

    ! fluxes components
    F(:,:) = 0.0_pr
    DO j = 1, dim
       jshift = ne*(j-1)

       DO i = 1, dim
          F(:,n_mom(i)+jshift) = tau_ij(:,ij2k(i,j))  ! rho*tau_ij 
       END DO

       !F(:,n_eng+jshift)   =  cp_in(Nspec)*mu_t(:)/C9_RANS*F_compress*du(dimp1,:,j)  ! F_compress*Cp*mu_t/Pr_t*dT/dx_j
       DO i = 1, dim
          F(:,n_eng+jshift) = F(:,n_eng+jshift) + v(:,i) * tau_ij(:,ij2k(i,j))  !u_i*rho*tau_ij 
       END DO
       F(:,n_eng+jshift)   = F(:,n_eng+jshift) + cp_in(Nspec)*mu_t(:)/C9_RANS*F_compress*du(dimp1,:,j)  ! F_compress*Cp*mu_t/Pr_t*dT/dx_j

       F(:,n_var_MT+jshift) =-u_integrated(:,n_var_MT )*v(:,j) + 1.0_pr/C2_RANS*( props(:) + u_integrated(:,n_var_MT) )*du(dimp3,:,j)
    END DO

    ! spatial derivatives of fluxes components
    tempintd = ne*dim
    CALL c_diff_fast(F(:,1:tempintd), du(1:tempintd,:,:), d2u(1:tempintd,:,:), j_lev, ng, meth, 10, tempintd, 1, tempintd)  ! du(i,:,k)=dF_ij/dx_k

    ! divergence of fluxes
    inc_rhs(:) = 0.0_pr
    DO j = 1, dim
       jshift = ne*(j-1)
       DO i = 1, ne
          shift = (i-1)*ng
          inc_rhs(shift+1:shift+ng) = inc_rhs(shift+1:shift+ng) + du(i+jshift,:,j)  ! dF_ij/dx_j
       END DO
    END DO

    ! source terms for the one model equation
    C_w1 = C0_RANS/C3_RANS**2 + (1.0_pr+C1_RANS)/C2_RANS
    shift=(n_var_MT-1)*ng
    inc_rhs(shift+1:shift+ng) = inc_rhs(shift+1:shift+ng) + C0_RANS*(1.0_pr-ft2(:))*S_hat(:)*u_integrated(:,n_var_MT) &
        - ( C_w1*fw(:) - C0_RANS/C3_RANS**2*ft2(:) )*(u_integrated(:,n_var_MT)/u(:,n_var_distw))**2/u_integrated(:,n_den) + cross1(:) - cross2(:)

    ! finalizing int_rhs
    int_rhs = int_rhs + inc_rhs


  END SUBROUTINE SA_compressible_rhs

  SUBROUTINE SA_compressible_Drhs (int_Drhs, u_p, u_prev, meth )
    !
    !-- Form DRHS of SA model equations
    !
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: meth
    REAL (pr), DIMENSION (ng*ne), INTENT(INOUT) :: int_Drhs
    REAL (pr), DIMENSION (ng,ne), INTENT(IN) :: u_p, u_prev         ! u_prev           - previous, u_p                         - perturbation                            

    REAL (pr), DIMENSION (ng*ne) :: inc_Drhs                        ! incremental Drhs
    REAL (pr), DIMENSION (2*ne*dim,ng,dim) :: du, d2u               ! 1...ne*dim       - previous, ne*dim+1...2*ne*dim         - perturbation
    REAL (pr), DIMENSION (ng,2*ne*dim) :: F                         ! 1...ne*dim       - previous, ne*dim+1...2*ne*dim         - perturbation                                                
    REAL (pr), DIMENSION (ng,dim*(dim+1)) :: S0_ij, tau_ij, S_ij    ! 1...dim(dim+1)/2 - previous, dim(dim+1)/2+1...dim(dim+1) - perturbation
    REAL (pr), DIMENSION (ng,2*(dim+3)) :: v                        ! 1...dim+3)       - previous, dim+3+1...2(dim+3)          - perturbation

    REAL (pr), DIMENSION (ng,2) :: Smod, Wmod, cross1, cross2, chi, mu_t, ft2, psiSqr          ! 1                - previous, 2                           - perturbation
    REAL (pr), DIMENSION (ng,2) :: S_hat, fv1, fv2                               ! 1                - previous, 2                           - perturbation
    REAL (pr), DIMENSION (ng,2) :: fw, gfunc, rfunc                              ! 1                - previous, 2                           - perturbation
    REAL (pr), DIMENSION (ng,2) :: fd, rd, gradUsqr, tmpf                        ! 1                - previous, 2                           - perturbation
    REAL (pr), DIMENSION (ng) :: props, props_prev                  ! nondimensional dynamic viscosity (1/Re*Sutherland)
    REAL (pr), DIMENSION (ng) :: distw_p                            ! perturbaion of the DDES length scale

    REAL (pr)                               :: C_w1, floor, tmp1, C_DES = 0.65_pr, f_w_star = 0.424_pr

    INTEGER :: i, j, k, idim, shift, tempintd, jshift, vshift, Fshift
    INTEGER :: dimp1, dimp2, dimp3, nS
    INTEGER, DIMENSION(dim,dim) :: ij2k

    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc
    INTEGER :: wlt_type, j_df, imin, i_bnd

    LOGICAL, SAVE :: header_flag = .TRUE.
    REAL (pr), ALLOCATABLE, DIMENSION(:,:), SAVE :: bnd_coords_btm
    INTEGER, SAVE :: n_coords_btm = 0
    REAL (pr), SAVE :: R1=0.0_pr, R2=0.0_pr
    REAL (pr), DIMENSION(dim) :: normal_dir !inward unit normal vector at a certain points of bnd_coords
    REAL (pr), DIMENSION(dim) :: tang_dir   !unit tangential vector in flow direction at a certain points of bnd_coords
    REAL (pr), DIMENSION (dim, ng, dim) :: dux
    REAL (pr), DIMENSION (dim, ng, dim) :: dux_dummy

     ! ============= SANITY CHECKS =======================================
    IF(Nspec > 1) THEN
       IF (par_rank.EQ.0) PRINT*,'ERROR: the URANS module is not set up for multple species, requires modification'
       STOP
    END IF 
    IF(MAXVAL(ABS(int_Drhs(n_eng*ng+1:ne*ng))) > 0.0_pr) THEN
      IF(par_rank .EQ. 0) PRINT *,'ERROR URANS compressible Drhs: int_Drhs /= 0'
      STOP
    END IF
    !===================================================================

    floor = 1.e-12_pr
    C_w1 = C0_RANS/C3_RANS**2 + (1.0_pr+C1_RANS)/C2_RANS

    dimp1 = dim + 1                  ! temperature
    dimp2 = dim + 2                  ! density
    dimp3 = dim + 3                  ! nu_til
    nS = dim*(dim+1)/2               ! velocity gradient

    vshift = dimp3
    Fshift = ne*dim

    ! velocity vector components
    DO i = 1, dim
       v(:,i) = u_prev(:,n_mom(i))/u_prev(:,n_den)  ! u_i
       v(:,vshift+i) = u_p(:,n_mom(i))/u_prev(:,n_den) - u_p(:,n_den)*u_prev(:,n_mom(i))/u_prev(:,n_den)**2  ! u'_i
    END DO

    ! temperature
    v(:,dimp1) = (u_prev(:,n_eng)-0.5_pr*SUM(u_prev(:,n_mom(1):n_mom(dim))**2,DIM=2)/u_prev(:,n_den))/cv_in(Nspec)/u_prev(:,n_den)/F_compress  ! T
    v(:,vshift+dimp1) = (u_p(:,n_eng)-(u_prev(:,n_eng)-SUM(u_prev(:,n_mom(1):n_mom(dim))**2,DIM=2)/u_prev(:,n_den))/u_prev(:,n_den)*u_p(:,n_den) &  ! T'
                      - SUM(u_prev(:,n_mom(1):n_mom(dim))*u_p(:,n_mom(1):n_mom(dim)),DIM=2)/u_prev(:,n_den))/cv_in(Nspec)/u_prev(:,n_den)/F_compress  

    ! density - only needed in cross2
    v(:,dimp2) = u_prev(:,n_den) ! rho
    v(:,vshift+dimp2) = u_p(:,n_den) ! rho'

    ! nu_til
    v(:,dimp3) = MAX(0.0_pr,SIGN(1.0_pr,u_prev(:,n_var_MT)))*u_prev(:,n_var_MT)/u_prev(:,n_den) ! nu_til 
    v(:,vshift+dimp3) = MAX(0.0_pr,SIGN(1.0_pr,u_prev(:,n_var_MT)))*( u_p(:,n_var_MT)/u_prev(:,n_den) - u_p(:,n_den)*v(:,dimp3)/u_prev(:,n_den) )  ! nu_til'

    ! constant viscosity
    props_prev(:) = mu_in(Nspec)  ! Nspec=1
    props(:) = 0.0_pr
    ! variable viscosity
    IF ( viscmodel .EQ. viscmodel_sutherland ) THEN  ! mu_in*(1.0_pr+sutherland_const)/(v(:,dimp1)+sutherland_const)*(v(:,dimp1))**(1.5_pr)
       props_prev(:) = mu_in(Nspec) * calc_nondim_sutherland_visc( v(:,dimp1), nwlt, sutherland_const ) !mu_variable
       props(:) = mu_in(Nspec) * (1.0_pr+sutherland_const)/(v(:,dimp1)+sutherland_const)*v(:,dimp1)**0.5_pr*(1.5_pr - v(:,dimp1)/(v(:,dimp1)+sutherland_const))*v(:,vshift+dimp1) !mu_variable'
    END IF

    ! first spatial derivatives
    tempintd = 2*dimp3
    CALL c_diff_fast(v(:,1:tempintd), du(1:tempintd,:,:), d2u(1:tempintd,:,:), j_lev, ng, meth, 10, tempintd, 1, tempintd)  ! du(i,:,j)=dv_i/dx_j

    ! strain rate
    CALL Sij_velgrad( du(1:dim,:,1:dim), ng, S_ij(:,1:nS), Smod(:,1), .FALSE.)    !S_ij

    S0_ij(:,1:nS) = S_ij(:,1:nS)
    Smod(:,1) = SUM( S0_ij(:,1:dim), DIM=2 ) / 3.0_pr
    DO idim = 1, dim	
       S0_ij(:,idim) = S0_ij(:,idim) - Smod(:,1)
    END DO

    Smod(:,1) =  SQRT( 2.0_pr*SUM( S0_ij(:,1:dim)**2, DIM=2) + &
                  4.0_pr*SUM( S0_ij(:,dim+1:nS)**2, DIM=2) )

    CALL Sij_velgrad( du(vshift+1:vshift+dim,:,1:dim), ng, S_ij(:,nS+1:2*nS), Smod(:,2), .FALSE.)    !S'_ij

    S0_ij(:,nS+1:2*nS) = S_ij(:,nS+1:2*nS)
    Smod(:,2) = SUM( S0_ij(:,nS+1:nS+dim), DIM=2 ) / 3.0_pr
    DO idim = 1, dim	
       S0_ij(:,nS+idim) = S0_ij(:,nS+idim) - Smod(:,2)
    END DO

!    Smod(:,2) = 2.0_pr*(SUM(S0_ij(:,1:dim)*S0_ij(:,nS+1:nS+dim), DIM=2) + 2.0_pr*SUM(S0_ij(:,dim+1:nS)*S0_ij(:,nS+dim+1:2*nS), DIM=2))/Smod(:,1) ! |S0|' = 2*S0_ij*S0'_ij/sqrt(2*S0_ij*S0_ij)

    CALL cal_magvort_velgrad( du(1:dim,:,1:dim), Wmod(:,1), ng )            ! |W| magnitude of rotation-rate or vorticity
    CALL cal_magvort_velgrad( du(vshift+1:vshift+dim,:,1:dim), Wmod(:,2), ng )            ! |W|' magnitude of rotation-rate or vorticity

    ! cross1 term for mu_til equation
    cross1(:,1) = SUM(du(dimp3,:,1:dim)*du(dimp3,:,1:dim),DIM=2)
    cross1(:,2) = C1_RANS/C2_RANS*(u_p(:,n_den)*cross1(:,1)+2.0_pr*u_prev(:,n_den)*SUM(du(vshift+dimp3,:,1:dim)*du(dimp3,:,1:dim), DIM=2))
    cross1(:,1) = C1_RANS/C2_RANS*cross1(:,1)*u_prev(:,n_den)

    !cross2 term for mu_til equation
    cross2(:,1) = SUM(du(dimp2,:,1:dim)*du(dimp3,:,1:dim), DIM=2)
    cross2(:,2) = 1.0_pr/(u_prev(:,n_den)*C2_RANS) &
        *( (props(:) + u_p(:,n_var_MT) - (props_prev(:)+u_prev(:,n_var_MT))*u_p(:,n_den)/u_prev(:,n_den))*cross2(:,1) &
        + (props_prev(:) + u_prev(:,n_var_MT))*(SUM(du(vshift+dimp2,:,1:dim)*du(dimp3,:,1:dim), DIM=2) + SUM(du(dimp2,:,1:dim)*du(vshift+dimp3,:,1:dim), DIM=2)) )
    cross2(:,1) = ( props_prev(:) + u_prev(:,n_var_MT))/(u_prev(:,n_den)*C2_RANS)*cross2(:,1)

    ! chi, distw, fv1, fv2, S_hat, rfunc, gfunc, fw,  ft2, mu_t
    
    chi(:,1) = u_prev(:,n_den)*v(:,dimp3)/props_prev(:)
    chi(:,2) = MAX(0.0_pr,SIGN(1.0_pr,u_prev(:,n_var_MT)))*(u_p(:,n_var_MT) - u_prev(:,n_var_MT)*props(:)/props_prev(:))/props_prev(:)

    !u(:,n_var_distw) = SQRT( SUM( u(:,n_map(1:dim))**2, DIM=2 ) ) - xyzlimits(1,1) ! Xuan - only works for circular cylinder geometry!
    u(:,n_var_distw) = MAX ( u(:,n_var_distw), floor )

    fv1(:,1) = chi(:,1)**3/(chi(:,1)**3+C6_RANS**3)
    fv1(:,2) = 3.0_pr*C6_RANS**3*chi(:,1)**2*chi(:,2)/(chi(:,1)**3+C6_RANS**3)**2

    fv2(:,1) = 1.0_pr - chi(:,1)/(1.0_pr+chi(:,1)*fv1(:,1))
    fv2(:,2) = (chi(:,1)**2*fv1(:,2) - chi(:,2))/(1.0_pr+chi(:,1)*fv1(:,1))**2

    ft2(:,1) = C7_RANS*exp(-C8_RANS*chi(:,1)**2)
    ft2(:,2) = ft2(:,1)*(-C8_RANS*2.0_pr*chi(:,1)*chi(:,2))

    mu_t(:,1) = u_prev(:,n_den)*v(:,dimp3)*fv1(:,1) ! mu_t
    mu_t(:,2) = MAX(0.0_pr,SIGN(1.0_pr,u_prev(:,n_var_MT)))*(u_p(:,n_var_MT)*fv1(:,1) + u_prev(:,n_var_MT)*fv1(:,2)) ! mu_t'

       !perturbation of the DDES length scale
       IF (.NOT. user_DDES) THEN
         distw_p = 0.0_pr
       ELSE 

         gradUsqr = 0.0_pr
         DO idim = 1, dim
           DO i = 1,dim
             gradUsqr(:,1) = gradUsqr(:,1) + du(idim,:,i)*du(idim,:,i)
             gradUsqr(:,2) = gradUsqr(:,2) + 2.0_pr*du(idim,:,i)*du(vshift+idim,:,i)
           END DO
         END DO

         rd(:,1) = ( mu_t(:,1) + props_prev(:) ) / MAX( u_prev(:,n_den)*SQRT(gradUsqr(:,1))*C3_RANS**2*distw_t(:)**2, floor )
         fd(:,1) = 1.0_pr - TANH((8.0_pr*rd(:,1))**3)

         rd(:,2) = ( (mu_t(:,2) - u_p(:,n_den)*v(:,dimp3)*fv1(:,1) + props(:) - u_p(:,n_den)*props_prev(:)/u_prev(:,n_den)) / u_prev(:,n_den) * SQRT(gradUsqr(:,1)) &
                   - (mu_t(:,1) + props_prev(:)) / u_prev(:,n_den) * gradUsqr(:,2)/2.0_pr/MAX(SQRT(gradUsqr(:,1)), floor) ) &
                   / MAX(gradUsqr(:,1), floor**2) / (C3_RANS**2*distw_t(:)**2)
         fd(:,2) = -(1.0_pr - TANH((8.0_pr*rd(:,1))**3)**2) * (8.0_pr**3*3.0_pr*rd(:,1)**2) * rd(:,2)


         tmpf(:,1) = (1.0_pr-C0_RANS/(C_w1*C3_RANS**2*f_w_star)*(ft2(:,1)+(1.0_pr-ft2(:,1))*fv2(:,1)))
         psiSqr(:,1) = tmpf(:,1) / MAX(fv1(:,1)*MAX(1.0e-10_pr,1.0_pr-ft2(:,1)), floor)

         tmpf(:,2) = - C0_RANS/(C_w1*C3_RANS**2*f_w_star) * (ft2(:,2)-ft2(:,2)*fv2(:,1)+(1.0_pr-ft2(:,1))*fv2(:,2))
         WHERE(1.0_pr-ft2(:,1) < 1.0e-10_pr)
           psiSqr(:,2) = (tmpf(:,2)*fv1(:,1) - tmpf(:,1)*fv1(:,2)) / MAX(fv1(:,1), floor) **2
         ELSEWHERE
           psiSqr(:,2) = (tmpf(:,2)*fv1(:,1)*(1.0_pr-ft2(:,1)) - tmpf(:,1)*(fv1(:,2)*(1.0_pr-ft2(:,1)) - fv1(:,1)*ft2(:,2))) / MAX(fv1(:,1),floor)**2*(1.0_pr-ft2(:,1))**2
         ENDWHERE
         psiSqr(:,2) = MAX(0.0_pr, SIGN(1.0_pr, (100.0_pr-psiSqr(:,1)))) * psiSqr(:,2)
         psiSqr(:,1) = MIN(100.0_pr, psiSqr(:,1))
         
         WHERE(distw_t(:) < C_DES*SQRT(psiSqr(:,1))*h_max(:))
           distw_p(:) = 0.0_pr
         ELSEWHERE
           distw_p(:) = -fd(:,2)*(distw_t - C_DES*SQRT(psiSqr(:,1))*h_max(:)) - fd(:,1)*(-C_DES*psiSqr(:,2)/2.0_pr/MAX(SQRT(psiSqr(:,1)), floor)*h_max(:))
         ENDWHERE

         !CALL print_extrema(rd(:,1), ng, 'rd_prev')
         !CALL print_extrema(rd(:,2), ng, 'rd_p')
         !CALL print_extrema(psiSqr(:,1), ng, 'psiSqr_prev')
         !CALL print_extrema(psiSqr(:,2), ng, 'psiSqr_p')
         !CALL print_extrema(distw_p(:), ng, 'distw_p')
       
       END IF

    S_hat(:,1) = Wmod(:,1) + v(:,dimp3)/(C3_RANS*u(:,n_var_distw))**2*fv2(:,1)
    WHERE( S_hat(:,1) >= 0.3*Wmod(:,1) )
        S_hat(:,2) = Wmod(:,2) + (v(:,vshift+dimp3)*fv2(:,1) + v(:,dimp3)*fv2(:,2))/(C3_RANS*u(:,n_var_distw))**2 - (v(:,dimp3)*fv2(:,1))*2.0_pr*distw_p(:)/(C3_RANS**2*u(:,n_var_distw)**3)
    ELSEWHERE
        S_hat(:,2) = 0.3_pr*Wmod(:,2)
    END WHERE
    S_hat(:,1) = MAX(0.3_pr*Wmod(:,1) + floor, S_hat(:,1))

    rfunc(:,1) = v(:,dimp3)/(S_hat(:,1)*(C3_RANS*u(:,n_var_distw))**2)
    rfunc(:,2) = MAX(0.0_pr, SIGN(1.0_pr,10.0_pr-rfunc(:,1)))*((v(:,vshift+dimp3) - v(:,dimp3)*S_hat(:,2)/S_hat(:,1))/S_hat(:,1)/(C3_RANS*u(:,n_var_distw))**2 - v(:,dimp3)/S_hat(:,1)*2.0_pr*distw_p(:)/(C3_RANS**2*u(:,n_var_distw)**3))
    rfunc(:,1) = MIN(rfunc(:,1), 10.0_pr)

    gfunc(:,1) = rfunc(:,1) + C4_RANS*(rfunc(:,1)**6-rfunc(:,1))
    gfunc(:,2) = rfunc(:,2) + C4_RANS*(6.0_pr*rfunc(:,1)**5*rfunc(:,2)-rfunc(:,2))

    fw(:,1) = ( (1.0_pr+C5_RANS**6)/(gfunc(:,1)**6+C5_RANS**6) )**(1.0_pr/6.0_pr)
    fw(:,2) = gfunc(:,2) * fw(:,1) * (C5_RANS**6/(gfunc(:,1)**6+C5_RANS**6))
    fw(:,1) = gfunc(:,1) * fw(:,1)

!------------------- Begin special treatments for inviscid top and adiabatic bottom BCs, not a generalized implementation. Needs improvement!------------------
       IF (inviscidTop) THEN
          !Imposing inviscid condition at the top wall
          i_p_face(0) = 1
          DO i=1,dim
             i_p_face(i) = i_p_face(i-1)*3
          END DO
          DO face_type = 0, 3**dim - 1
             face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
             IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
                CALL get_all_indices_by_face (face_type, j_lev, nloc, iloc)
                IF(nloc > 0 ) THEN 
                  IF( face(2) == 1 ) THEN ! inviscid top wall
                         mu_t(iloc(1:nloc),1) = 0.0_pr
                         mu_t(iloc(1:nloc),2) = 0.0_pr
                  END IF
                END IF
             END IF
          END DO
       END IF

       !Modify temperature derivatives to impose adiabatic BC dT/dn =0. The current version is ONLY valid for 2D geometry.
       IF (adiabaticBtm .AND. time_integration_method /= TIME_INT_Crank_Nicholson) THEN

          !!NOTE: this jobcobian matrix dux = dx_i/dxi_i is incorrect for H-type periodic boundary condition
          !CALL c_diff_fast (u(1:ng, n_map(1:dim)), dux(1:dim,1:ng,1:dim), dux_dummy, j_lev, ng, meth, 10, dim, 1, dim, FORCE_RECTILINEAR = .TRUE.)
          !DO idim = 1, dim
          !  IF( transform_dir(idim) .NE. 1 ) THEN  ! account for non-transformed directions
          !    dux(1:dim,:,idim) = 0.0_pr
          !    dux(idim,:,1:dim) = 0.0_pr
          !    dux(idim,:,idim)  = 1.0_pr
          !  END IF
          !END DO

          DO j = 1, j_lev
             i_p_face(0) = 1
             DO i=1,dim
                i_p_face(i) = i_p_face(i-1)*3
             END DO
             DO face_type = 0, 3**dim - 1
                face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
                IF( face(2) == -1 ) THEN                !adiabatic bottom wall
                   DO wlt_type = MIN(j-1,1),2**dim-1
                      DO j_df = j, j_lev
                         DO k = 1, indx_DB(j_df,wlt_type,face_type,j)%length
                            i_bnd = indx_DB(j_df,wlt_type,face_type,j)%p(k)%i

                              normal_dir = 0.0_pr
                              tang_dir = 0.0_pr
                              tang_dir(1) = curvilinear_coordgrad(1,1,i_bnd)/sqrt(curvilinear_coordgrad(1,1,i_bnd)**2+curvilinear_coordgrad(2,1,i_bnd)**2) !normalized dx/dxi
                              tang_dir(2) = curvilinear_coordgrad(2,1,i_bnd)/sqrt(curvilinear_coordgrad(1,1,i_bnd)**2+curvilinear_coordgrad(2,1,i_bnd)**2) !normalized dy/dxi
                              normal_dir(1) = - tang_dir(2)
                              normal_dir(2) =   tang_dir(1)
                              du(dimp1,i_bnd,2) = -du(dimp1,i_bnd,1)*normal_dir(1)/SIGN(MAX(floor,ABS(normal_dir(2))), normal_dir(2)) !nx*dT/dx+ny*dT/dy=0
                              du(vshift+dimp1,i_bnd,2) = -du(vshift+dimp1,i_bnd,1)*normal_dir(1)/SIGN(MAX(floor,ABS(normal_dir(2))), normal_dir(2)) !nx*dT'/dx+ny*dT'/dy=0

                         END DO
                      END DO
                   END DO
                END IF
             END DO
          END DO
       END IF
!------------------- End special treatments for inviscid top and adiabatic bottom BCs, not a generalized implementation. Needs improvement!------------------

    ! turbulent stresses
    DO idim = 1, nS
       tau_ij(:,idim) = 2.0_pr*mu_t(:,1)*S0_ij(:,idim)
       tau_ij(:,nS+idim) = 2.0_pr*(mu_t(:,2)*S0_ij(:,idim)+mu_t(:,1)*S0_ij(:,nS+idim))
    END DO
    !DO idim = 1, dim
    !   tau_ij(:,idim) = tau_ij(:,idim) - 2.0_pr/REAL(dim,pr)*MAX(0.0_pr,u_prev(:,n_var_K))                                           !tau_ij
    !   tau_ij(:,nS+idim) = tau_ij(:,nS+idim) - 2.0_pr/REAL(dim,pr)*MAX(0.0_pr,SIGN(1.0_pr,u_prev(:,n_var_K)))*u_p(:,n_var_K)         !tau_ij'
    !END DO

    
    k = dim
    DO i = 1, dim
       ij2k(i,i) = i 
       DO j = i+1, dim
          k = k+1
          ij2k(i,j) = k
          ij2k(j,i) = k
       END DO
    END DO

    F(:,:) = 0.0_pr
    DO j = 1, dim
       jshift=ne*(j-1)
       !
       !fluxes at previous time step
       !
       DO i = 1, dim
          F(:,n_mom(i)+jshift) = tau_ij(:,ij2k(i,j))  ! rho*tau_ij 
       END DO
       !F(:,n_eng  +jshift) =  cp_in(Nspec)*mu_t(:,1)/C9_RANS*F_compress*du(dimp1,:,j)
       DO i = 1, dim
          F(:,n_eng+jshift) = F(:,n_eng+jshift) + v(:,i) * tau_ij(:,ij2k(i,j))  !u_i*rho*tau_ij 
       END DO
       F(:,n_eng+jshift)   = F(:,n_eng+jshift) + cp_in(Nspec)*mu_t(:,1)/C9_RANS*F_compress*du(dimp1,:,j)  ! F_compress*Cp*mu_t/Pr_t*dT/dx_j
       F(:,n_var_MT+jshift) = -u_prev(:,n_var_MT)*v(:,j)  + 1.0_pr/C2_RANS*( props_prev(:) + u_prev(:,n_var_MT) ) * du(dimp3,:,j)
       !
       ! perturbation of fluxes
       !
       DO i = 1, dim
          F(:,Fshift+n_mom(i)+jshift) = tau_ij(:,nS+ij2k(i,j))  ! (rho*tau_ij)' 
       END DO
       !F(:,Fshift+n_eng+jshift) = cp_in(Nspec)/C9_RANS*F_compress*(mu_t(:,2)*du(dimp1,:,j)+mu_t(:,1)*du(vshift+dimp1,:,j)) ! F*(Cp mu_t/Pr_t*dT/dx_j)'
       DO i = 1, dim
          F(:,Fshift+n_eng+jshift) = F(:,Fshift+n_eng+jshift) + v(:,vshift+i) * tau_ij(:,ij2k(i,j)) + v(:,i) * tau_ij(:,nS+ij2k(i,j))  !(u_i*rho*tau_ij)'
       END DO
       F(:,Fshift+n_eng+jshift) = F(:,Fshift+n_eng+jshift) + cp_in(Nspec)/C9_RANS*F_compress*(mu_t(:,2)*du(dimp1,:,j)+mu_t(:,1)*du(vshift+dimp1,:,j)) ! F*(Cp mu_t/Pr_t*dT/dx_j)'

       F(:,Fshift+n_var_MT+jshift)  = -u_p(:,n_var_MT)*v(:,j)-u_prev(:,n_var_MT)*v(:,vshift+j)  &
        + 1.0_pr/C2_RANS*( props(:) + u_p(:,n_var_MT)*du(dimp3,:,j) + (props_prev(:) + u_prev(:,n_var_MT))*du(vshift+dimp3,:,j) )
    END DO

    tempintd=2*ne*dim
    CALL c_diff_fast(F(:,1:tempintd), du(1:tempintd,:,:), d2u(1:tempintd,:,:), j_lev, ng, meth, 10, tempintd, 1, tempintd)  !du(i,:,k)=dF_ij/dx_k

    inc_Drhs(:) = 0
    DO j = 1, dim
       jshift=ne*(j-1)
       DO i = 1, ne
          shift=(i-1)*ng
          inc_Drhs(shift+1:shift+ng) = inc_Drhs(shift+1:shift+ng) + du(i+jshift,:,j)        ! dF_ij/dx_j previous
       END DO
    END DO

    !jshift=(n_eng-1)*ng
    !DO i = 1, dim
    !   shift=(n_mom(i)-1)*ng
    !   int_Drhs(jshift+1:jshift+ng) = int_Drhs(jshift+1:jshift+ng) + inc_Drhs(shift+1:shift+ng)*v(:,vshift+i)    ! u'_i d(rho tau_ij)/dx_j 
    !END DO

    ! adding the divergence of fluxes perturbation
    inc_Drhs(:) = 0
    DO j = 1, dim
       jshift=ne*(j-1)
       DO i = 1, ne
          shift=(i-1)*ng
          inc_Drhs(shift+1:shift+ng) = inc_Drhs(shift+1:shift+ng) + du(Fshift+i+jshift,:,j)        ! dF_ij/dx_j preturbation
       END DO
    END DO

    !! temperature
    !jshift=(n_eng-1)*ng
    !DO i = 1, dim
    !   shift=(n_mom(i)-1)*ng
    !   inc_Drhs(jshift+1:jshift+ng) = inc_Drhs(jshift+1:jshift+ng) + inc_Drhs(shift+1:shift+ng)*v(:,i)    ! u_i d(rho tau_ij)'/dx_j
    !END DO
    !inc_Drhs(jshift+1:jshift+ng) = inc_Drhs(jshift+1:jshift+ng) + 0.25_pr*C10_RANS/MAX(mu_t(:,1),floor) * (2.0_pr*SUM(tau_ij(:,1:dim),DIM=2)*SUM(tau_ij(:,nS+1:nS+dim),DIM=2) &
    !    -(SUM(tau_ij(:,1:dim),DIM=2))**2/MAX(mu_t(:,1),floor)*mu_t(:,2))   ! (rho*epsilon)' perturbation of turbulence dissipation

    !tmp1 = SUM(inc_Drhs(jshift+1:jshift+ng))
    !IF(verb_level .GT. 0) PRINT *,'tmp1 = ', tmp1

    ! mu_til
    shift=(n_var_MT-1)*ng
    inc_Drhs(shift+1:shift+ng) = inc_Drhs(shift+1:shift+ng) &
        + C0_RANS*( (-ft2(:,2)*S_hat(:,1)+(1.0_pr-ft2(:,1))*S_hat(:,2))*u_prev(:,n_var_MT) + (1.0_pr-ft2(:,1))*S_hat(:,1)*u_p(:,n_var_MT) ) &
        - ((C_w1*fw(:,2)-C0_RANS/C3_RANS**2*ft2(:,2))*(u_prev(:,n_var_MT)/u(:,n_var_distw))**2 + (C_w1*fw(:,1)-C0_RANS/C3_RANS**2*ft2(:,1)) &
        * (2.0_pr/u(:,n_var_distw)**2*u_prev(:,n_var_MT)*u_p(:,n_var_MT) - (u_prev(:,n_var_MT)/u(:,n_var_distw))**2/u_prev(:,n_den)*u_p(:,n_den) - u_prev(:,n_var_MT)**2*2.0_pr*distw_p(:)/u(:,n_var_distw)**3))/u_prev(:,n_den) &
        + cross1(:,2) - cross2(:,2)

    ! finalizing int_Drhs
    int_Drhs = int_Drhs + inc_Drhs


  END SUBROUTINE SA_compressible_Drhs

  SUBROUTINE SA_compressible_Drhs_diag (int_Drhs_diag,u_prev,meth)
    !-- Form DRHS_diag of SA model equations
    !
    USE variable_mapping
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: meth
    REAL (pr), DIMENSION (ng*ne), INTENT(INOUT) :: int_Drhs_diag
    REAL (pr), DIMENSION (ng,ne), INTENT(IN) :: u_prev
    REAL (pr), DIMENSION (ng,dim) :: du_diag, d2u_diag
    REAL (pr), DIMENSION (dim+3,ng,dim) :: du, d2u   
    !REAL (pr), DIMENSION (ng,dim*(dim+1)) :: S0_ij, S_ij   ! 1...dim(dim+1)/2 - previous, dim(dim+1)/2+1...dim(dim+1) - perturbation
    REAL (pr), DIMENSION (ng, dim+3) :: v_prev
    !REAL (pr), DIMENSION (ng,2) :: mu_t        ! mu_t(:,1) previous time; mu_t(:,2)= d mu_t/d (rho k) or mu_t(:,2)= d (omega mu_t/k) /d (rho omega)
    REAL (pr), DIMENSION (ng) :: Wmod, mu_t        ! mu_t(:,1) previous time; 

    REAL (pr), DIMENSION (ng,2) :: cross1, cross2, chi, ft2, psiSqr  ! 1                - previous, 2                           - perturbation_diag
    REAL (pr), DIMENSION (ng,2) :: S_hat, fv1, fv2                   ! 1                - previous, 2                           - perturbation_diag
    REAL (pr), DIMENSION (ng,2) :: fw, gfunc, rfunc                  ! 1                - previous, 2                           - perturbation_diag
    REAL (pr), DIMENSION (ng,2) :: fd, rd, tmpf                      ! 1                - previous, 2                           - perturbation
    REAL (pr), DIMENSION (ng) :: distw_p, gradUsqr                   ! perturbaion of the DDES length scale

    REAL (pr)                               :: C_w1, floor, tmp1, C_DES = 0.65_pr, f_w_star = 0.424_pr

    REAL (pr), DIMENSION (ng) :: props_prev     !, props
    INTEGER :: i, shift, idim
    INTEGER :: dimp1, dimp2, dimp3, nS

    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc
    INTEGER :: wlt_type, j, k, j_df, imin, i_bnd

    LOGICAL, SAVE :: header_flag = .TRUE.
    REAL (pr), ALLOCATABLE, DIMENSION(:,:), SAVE :: bnd_coords_btm
    INTEGER, SAVE :: n_coords_btm = 0
    REAL (pr), SAVE :: R1=0.0_pr, R2=0.0_pr
    REAL (pr), DIMENSION(dim) :: normal_dir !inward unit normal vector at a certain points of bnd_coords
    !REAL (pr), DIMENSION(dim) :: tang_dir   !unit tangential vector in flow direction at a certain points of bnd_coords

     ! ============= SANITY CHECKS =======================================
    IF(Nspec > 1) THEN
       IF (par_rank.EQ.0) PRINT*,'ERROR: the URANS module is not set up for multple species, requires modification'
       STOP
    END IF 
    IF(MAXVAL(ABS(int_Drhs_diag(n_eng*ng+1:ne*ng))) > 0.0_pr) THEN
      IF(par_rank .EQ. 0) PRINT *,'ERROR URANS compressible Drhs_diag: int_Drhs_diag /= 0'
      STOP
    END IF
    !===================================================================

    floor = 1.e-12_pr
    C_w1 = C0_RANS/C3_RANS**2 + (1.0_pr+C1_RANS)/C2_RANS

    dimp1 = dim + 1                  ! temperature
    dimp2 = dim + 2                  ! density
    dimp3 = dim + 3                  ! nu_til
    nS = dim*(dim+1)/2               ! velocity gradient

    CALL c_diff_diag(du_diag, d2u_diag, j_lev, ng, meth, meth, -11)

    ! velocity vector components
    DO i = 1, dim
       v_prev(:,i) = u_prev(:,n_mom(i))/u_prev(:,n_den)  ! u_i
    END DO

    ! temperature
    v_prev(:,dimp1) = (u_prev(:,n_eng)-0.5_pr*SUM(u_prev(:,n_mom(1):n_mom(dim))**2,DIM=2)/u_prev(:,n_den))/cv_in(Nspec)/u_prev(:,n_den)/F_compress  !T

    ! density - only needed in cross2
    v_prev(:,dimp2) = u_prev(:,n_den) ! rho

    ! nu_til
    v_prev(:,dimp3) = MAX(0.0_pr,SIGN(1.0_pr,u_prev(:,n_var_MT)))*u_prev(:,n_var_MT)/u_prev(:,n_den) ! nu_til 


    ! constant viscosity
    props_prev(:) = mu_in(Nspec)  ! Nspec=1
!    props(:) = 0.0_pr  ! d (mu) / dT
    ! variable viscosity
    IF ( viscmodel .EQ. viscmodel_sutherland ) THEN  ! mu_in*(1.0_pr+sutherland_const)/(v(:,dimp1)+sutherland_const)*(v(:,dimp1))**(1.5_pr)
       props_prev(:) = mu_in(Nspec) * calc_nondim_sutherland_visc( v_prev(:,dimp1), nwlt, sutherland_const )
!       props(:) = mu_in(Nspec) * (1.0_pr+sutherland_const)/(v_prev(:,dimp1)+sutherland_const)*v_prev(:,dimp1)**0.5_pr*(1.5_pr-v_prev(:,dimp1)/(v_prev(:,dimp1)+sutherland_const))
    END IF

    ! first spatial derivatives at previous time
    CALL c_diff_fast( v_prev(:,1:dimp3), du(1:dimp3,:,:), d2u(1:dimp3,:,:), j_lev, ng, meth, 10, dimp3, 1, dimp3 )

    ! strain rate
    !CALL Sij_velgrad( du(1:dim,:,1:dim), ng, S_ij(:,1:nS),  Smod(:), .FALSE.)   ! S_ij at previous time 
    !CALL Sij_velgrad( du(1:dim,:,1:dim), ng, S0_ij(:,1:nS), Smod(:), .TRUE.)    ! S0_ij at previous time

    CALL cal_magvort_velgrad( du(1:dim,:,1:dim), Wmod, ng )            ! |W| magnitude of rotation-rate or vorticity

    ! cross1 term for mu_til equation
    cross1(:,1) = SUM(du(dimp3,:,1:dim)*du(dimp3,:,1:dim),DIM=2)
    cross1(:,2) = C1_RANS/C2_RANS*2.0_pr*SUM(du_diag(:,1:dim)*du(dimp3,:,1:dim), DIM=2)
    cross1(:,1) = C1_RANS/C2_RANS*cross1(:,1)*u_prev(:,n_den)

    !cross2 term for mu_til equation
    cross2(:,1) = SUM(du(dimp2,:,1:dim)*du(dimp3,:,1:dim), DIM=2)
    cross2(:,2) = 1.0_pr/(u_prev(:,n_den)*C2_RANS)*( cross2(:,1) &
        + (props_prev(:) + u_prev(:,n_var_MT))*(SUM(du(dimp2,:,1:dim)*du_diag(:,1:dim), DIM=2))/u_prev(:,n_den) )
    cross2(:,1) = ( props_prev(:) + u_prev(:,n_var_MT) )/(u_prev(:,n_den)*C2_RANS)*cross2(:,1)

    ! chi, distw, fv1, fv2, S_hat, rfunc, gfunc, fw,  ft2, mu_t
    
    chi(:,1) = u_prev(:,n_den)*v_prev(:,dimp3)/props_prev(:)
    chi(:,2) = 1.0_pr/props_prev(:)

    !u(:,n_var_distw) = SQRT( SUM( u(:,n_map(1:dim))**2, DIM=2 ) ) - xyzlimits(1,1) ! Xuan - only works for circular cylinder geometry!
    u(:,n_var_distw) = MAX ( u(:,n_var_distw), floor )

    fv1(:,1) = chi(:,1)**3/(chi(:,1)**3+C6_RANS**3)
    fv1(:,2) = 3.0_pr*C6_RANS**3*chi(:,1)**2*chi(:,2)/(chi(:,1)**3+C6_RANS**3)**2

    fv2(:,1) = 1.0_pr - chi(:,1)/(1.0_pr+chi(:,1)*fv1(:,1))
    fv2(:,2) = (chi(:,1)**2*fv1(:,2) - chi(:,2))/(1.0_pr+chi(:,1)*fv1(:,1))**2

    ft2(:,1) = C7_RANS*exp(-C8_RANS*chi(:,1)**2)
    ft2(:,2) = ft2(:,1)*(-C8_RANS*2.0_pr*chi(:,1)*chi(:,2))

    mu_t(:) = u_prev(:,n_den)*v_prev(:,dimp3)*fv1(:,1) ! mu_t

       !perturbation of the DDES length scale
       IF (.NOT. user_DDES) THEN
         distw_p = 0.0_pr
       ELSE 

         gradUsqr = 0.0_pr
         DO idim = 1, dim
           DO i = 1,dim
             gradUsqr(:) = gradUsqr(:) + du(idim,:,i)*du(idim,:,i)
           END DO
         END DO

         rd(:,1) = ( mu_t(:) + props_prev(:) ) / MAX( u_prev(:,n_den)*SQRT(gradUsqr(:))*C3_RANS**2*distw_t(:)**2, floor )
         fd(:,1) = 1.0_pr - TANH((8.0_pr*rd(:,1))**3)

         rd(:,2) = ( (fv1(:,1) + fv1(:,2)*u_prev(:,n_var_MT)) / u_prev(:,n_den) ) &
                   / MAX( SQRT(gradUsqr(:)), floor ) / (C3_RANS**2*distw_t(:)**2)
         fd(:,2) = -(1.0_pr - TANH((8.0_pr*rd(:,1))**3)**2) * (8.0_pr**3*3.0_pr*rd(:,1)**2) * rd(:,2)


         tmpf(:,1) = (1.0_pr-C0_RANS/(C_w1*C3_RANS**2*f_w_star)*(ft2(:,1)+(1.0_pr-ft2(:,1))*fv2(:,1)))
         psiSqr(:,1) = tmpf(:,1) / MAX(fv1(:,1)*MAX(1.0e-10_pr,1.0_pr-ft2(:,1)), floor)

         tmpf(:,2) = - C0_RANS/(C_w1*C3_RANS**2*f_w_star) * (ft2(:,2)-ft2(:,2)*fv2(:,1)+(1.0_pr-ft2(:,1))*fv2(:,2))
         WHERE(1.0_pr-ft2(:,1) < 1.0e-10_pr)
           psiSqr(:,2) = (tmpf(:,2)*fv1(:,1) - tmpf(:,1)*fv1(:,2)) / MAX(fv1(:,1), floor)**2
         ELSEWHERE
           psiSqr(:,2) = (tmpf(:,2)*fv1(:,1)*(1.0_pr-ft2(:,1)) - tmpf(:,1)*(fv1(:,2)*(1.0_pr-ft2(:,1)) - fv1(:,1)*ft2(:,2))) / MAX(fv1(:,1), floor)**2*(1.0_pr-ft2(:,1))**2
         ENDWHERE
         psiSqr(:,2) = MAX(0.0_pr, SIGN(1.0_pr, (100.0_pr-psiSqr(:,1)))) * psiSqr(:,2)
         psiSqr(:,1) = MIN(100.0_pr, psiSqr(:,1))
         
         WHERE(distw_t(:) < C_DES*SQRT(psiSqr(:,1))*h_max(:))
           distw_p(:) = 0.0_pr
         ELSEWHERE
           distw_p(:) = -fd(:,2)*(distw_t - C_DES*SQRT(psiSqr(:,1))*h_max(:)) - fd(:,1)*(-C_DES*psiSqr(:,2)/2.0_pr/MAX(SQRT(psiSqr(:,1)), floor)*h_max(:))
         ENDWHERE
       
         !CALL print_extrema(rd(:,1), ng, 'rd_prev')
         !CALL print_extrema(rd(:,2), ng, 'rd_p')
         !CALL print_extrema(psiSqr(:,1), ng, 'psiSqr_prev')
         !CALL print_extrema(psiSqr(:,2), ng, 'psiSqr_p')
         !CALL print_extrema(distw_p(:), ng, 'distw_p_diag')
       
       END IF

    S_hat(:,1) = Wmod(:) + v_prev(:,dimp3)/(C3_RANS*u(:,n_var_distw))**2*fv2(:,1)
    WHERE( S_hat(:,1) >= 0.3*Wmod(:) )
        S_hat(:,2) = (fv2(:,1)/u_prev(:,n_den) + v_prev(:,dimp3)*fv2(:,2))/(C3_RANS*u(:,n_var_distw))**2 - (v_prev(:,dimp3)*fv2(:,1))*2.0_pr*distw_p(:)/(C3_RANS**2*u(:,n_var_distw)**3)
    ELSEWHERE
        S_hat(:,2) = 0.0_pr
    END WHERE
    S_hat(:,1) = MAX(0.3_pr*Wmod(:) + floor, S_hat(:,1))

    rfunc(:,1) = v_prev(:,dimp3)/(S_hat(:,1)*(C3_RANS*u(:,n_var_distw))**2)
    rfunc(:,2) = MAX(0.0_pr, SIGN(1.0_pr,10.0_pr-rfunc(:,1)))*((1.0_pr/u_prev(:,n_den) - v_prev(:,dimp3)*S_hat(:,2)/S_hat(:,1))/S_hat(:,1)/(C3_RANS*u(:,n_var_distw))**2 &
                - v_prev(:,dimp3)/S_hat(:,1)*2.0_pr*distw_p(:)/(C3_RANS**2*u(:,n_var_distw)**3))
    rfunc(:,1) = MIN(rfunc(:,1), 10.0_pr)

    gfunc(:,1) = rfunc(:,1) + C4_RANS*(rfunc(:,1)**6-rfunc(:,1))
    gfunc(:,2) = rfunc(:,2) + C4_RANS*(6.0_pr*rfunc(:,1)**5*rfunc(:,2)-rfunc(:,2))

    fw(:,1) = ( (1.0_pr+C5_RANS**6)/(gfunc(:,1)**6+C5_RANS**6) )**(1.0_pr/6.0_pr)
    fw(:,2) = gfunc(:,2) * fw(:,1) * (C5_RANS**6/(gfunc(:,1)**6+C5_RANS**6))
    fw(:,1) = gfunc(:,1) * fw(:,1)


!------------------- Begin special treatments for inviscid top, not a generalized implementation. Needs improvement!------------------
       IF (inviscidTop) THEN
          !Imposing inviscid condition at the top wall
          i_p_face(0) = 1
          DO i=1,dim
             i_p_face(i) = i_p_face(i-1)*3
          END DO
          DO face_type = 0, 3**dim - 1
             face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
             IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
                CALL get_all_indices_by_face (face_type, j_lev, nloc, iloc)
                IF(nloc > 0 ) THEN 
                  IF( face(2) == 1 ) THEN ! inviscid top wall
                         mu_t(iloc(1:nloc)) = 0.0_pr
                  END IF
                END IF
             END IF
          END DO
       END IF
!------------------- End special treatments for inviscid top, not a generalized implementation. Needs improvement!------------------

    ! momentum
    DO i = 1, dim
       shift=(n_mom(i)-1)*ng
       int_Drhs_diag(shift+1:shift+ng) = int_Drhs_diag(shift+1:shift+ng) &
                                       !+ mu_t(:)/u_prev(:,n_den)*( SUM(d2u_diag(:,:),DIM=2) + d2u_diag(:,i) - 2.0_pr/REAL(dim,pr)*d2u_diag(:,i))     
                                       + mu_t(:)/u_prev(:,n_den)*( SUM(d2u_diag(:,:),DIM=2) + d2u_diag(:,i) - 2.0_pr/3.0_pr*d2u_diag(:,i))     
    END DO

    ! energy
    shift=(n_eng-1)*ng
    int_Drhs_diag(shift+1:shift+ng) = int_Drhs_diag(shift+1:shift+ng) &
                                    + cp_in(Nspec)/cv_in(Nspec)/C9_RANS/u_prev(:,n_den)*mu_t(:)*SUM(d2u_diag(:,:),DIM=2)

    ! mu_til
    shift=(n_var_MT-1)*ng
    int_Drhs_diag(shift+1:shift+ng) = int_Drhs_diag(shift+1:shift+ng) &
                - SUM(du_diag(:,1:dim)*v_prev(:,1:dim),DIM=2) &
                + 1.0_pr/C2_RANS*( SUM(du_diag(:,1:dim)*du(dimp3,:,1:dim),DIM=2) &
                + (props_prev(:) + u_prev(:,n_var_MT))/u_prev(:,n_den)*SUM(d2u_diag(:,:),DIM=2) ) &
                + C0_RANS*((-ft2(:,2)*S_hat(:,1) + (1.0_pr-ft2(:,1))*S_hat(:,2))*u_prev(:,n_var_MT) + (1-ft2(:,1))*S_hat(:,1)) &
                - ( (C_w1*fw(:,2)-C0_RANS/C3_RANS**2*ft2(:,2))*(u_prev(:,n_var_MT)/u(:,n_var_distw))**2 + (C_w1*fw(:,1)-C0_RANS/C3_RANS**2*ft2(:,1)) &
                * 2.0_pr/u(:,n_var_distw)**2*u_prev(:,n_var_MT) - u_prev(:,n_var_MT)**2*2.0_pr*distw_p(:)/u(:,n_var_distw)**3 )/u_prev(:,n_den) &
                + cross1(:,2) - cross2(:,2)

    !tmp1 = SUM(int_Drhs_diag(shift+1:shift+ng))
    !IF(verb_level .GT. 0) PRINT *,'tmp1: int_Drhs_diag', tmp1
   
  END SUBROUTINE SA_compressible_Drhs_diag

  SUBROUTINE cal_fd(nlocal, distance_t, mut, u_local, ne_local, fd, props)
    IMPLICIT NONE

    INTEGER, INTENT(IN)  :: nlocal, ne_local
    REAL (pr), DIMENSION (nlocal), INTENT(IN) :: distance_t
    REAL (pr), DIMENSION (nlocal), INTENT(IN)    :: mut
    REAL (pr), DIMENSION (nlocal,ne_local), INTENT(IN)    :: u_local

    INTEGER :: i, idim
    REAL (pr), DIMENSION (nlocal,dim)         :: v               ! velocity components
    REAL (pr), DIMENSION (dim,nlocal,dim)     :: du, d2u         ! velocity gradients
    REAL (pr), DIMENSION (nlocal)         :: temp                ! temperature
    REAL (pr), DIMENSION (nlocal)         :: rd, fd
    REAL (pr), DIMENSION (nlocal)         :: props               ! nondimensional dynamic viscosity (1/Re*Sutherland)
    REAL (pr), DIMENSION (nlocal)         :: gradUsqr            ! grad_u_ij && grad_u_ij
    REAL (pr) :: sumv, tmp1, tmp2
    REAL (pr) :: floor

    floor = 1.e-12_pr

    DO i = 1, dim
        v(:,i) = u_local(:,n_mom(i)) / u_local(:,n_den)       ! u_i
    END DO

    temp(:) = (u_local(:,n_eng)-0.5_pr*SUM(u_local(:,n_mom(1):n_mom(dim))**2,DIM=2)/u_local(:,n_den))/cv_in(Nspec)/u_local(:,n_den)/F_compress
    ! constant viscosity
    props(:) = mu_in(Nspec)  ! Nspec=1
    ! variable viscosity
    IF ( viscmodel .EQ. viscmodel_sutherland ) THEN  ! mu_in*(1.0_pr+sutherland_const)/(v(:,dimp1)+sutherland_const)*(v(:,dimp1))**(1.5_pr)
       props(:) = mu_in(Nspec) * calc_nondim_sutherland_visc( temp(:), nwlt, sutherland_const )
    END IF

    CALL c_diff_fast( v(:,1:dim), du(1:dim,:,:), d2u(1:dim,:,:), j_lev, nlocal, HIGH_ORDER, 10, dim, 1, dim )  
    gradUsqr = 0.0_pr
    DO idim = 1, dim
      DO i = 1,dim
        gradUsqr(:) = gradUsqr(:) + du(idim,:,i)*du(idim,:,i)
      END DO
    END DO

    rd(:) = ( mut(:) + props(:) ) / MAX( u_local(:,n_den)*SQRT(gradUsqr(:))*C3_RANS**2*distance_t(:)**2, floor )
    fd(:) = 1.0_pr - TANH((8.0_pr*rd(:))**3)

!    sumv = SUM( rd(:) )
!    CALL parallel_global_sum( REAL=sumv )
!    IF (par_rank .EQ. 0) PRINT *, 'sum(rd) =', sumv

  END SUBROUTINE cal_fd

  SUBROUTINE DDES_length_scale(nlocal, distance, distance_t, u_local, ne_local, fd, h_phys, h_phys_max, props)
    IMPLICIT NONE

    INTEGER, INTENT(IN)  :: nlocal, ne_local
    REAL (pr), DIMENSION (nlocal) :: distance
    REAL (pr), DIMENSION (nlocal), INTENT(IN) :: distance_t ! true wall distance
    REAL (pr), DIMENSION (nlocal,ne_local), INTENT(IN)    :: u_local
    REAL (pr), DIMENSION (nlocal), INTENT(IN)             :: fd
    REAL (pr), DIMENSION(1:nlocal,1:dim), INTENT(IN)      :: h_phys
    REAL (pr), DIMENSION (nlocal), INTENT(IN)             :: h_phys_max          ! max(delta_x, delta_y, delta_z)
    REAL (pr), DIMENSION (nlocal), INTENT(IN)             :: props               ! nondimensional dynamic viscosity (1/Re*Sutherland)

    REAL (pr), DIMENSION (nlocal)         :: chi, fv1, fv2, ft2, psi
    REAL (pr), DIMENSION (nlocal)         :: length_LES
    REAL (pr) :: C_w1, C_DES = 0.65_pr, f_w_star = 0.424_pr, psi_max, psi_min, chi_max, chi_min, tmp1, tmp2

    IF(dim .EQ. 3) THEN

      C_w1 = C0_RANS/C3_RANS**2 + (1.0_pr+C1_RANS)/C2_RANS

      chi(:) = MAX(0.0_pr,u_local(:,n_var_MT)) / props(:)

      fv1(:) = chi(:)**3/(chi(:)**3+C6_RANS**3)

      fv2(:) = 1.0_pr - chi(:)/(1.0_pr+chi(:)*fv1(:))

      ft2(:) = C7_RANS*exp(-C8_RANS*chi(:)**2)

      psi(:) = SQRT( MIN(100.0_pr, (1.0_pr-C0_RANS/(C_w1*C3_RANS**2*f_w_star)*(ft2(:)+(1.0_pr-ft2)*fv2))/MAX(fv1(:)*MAX(1.0e-10_pr,1.0_pr-ft2(:)), 1.0e-12) ) )

!      chi_max = MAXVAL( chi(:) )
!      chi_min = MINVAL( chi(:) )
!      CALL parallel_global_sum( REALMAXVAL=chi_max )
!      CALL parallel_global_sum( REALMINVAL=chi_min )       
!      IF (par_rank .EQ. 0) PRINT *, 'max(chi) =', chi_max, 'min(chi) =', chi_min

!      psi_max = MAXVAL( psi(:) )
!      psi_min = MINVAL( psi(:) )
!      CALL parallel_global_sum( REALMAXVAL=psi_max )
!      CALL parallel_global_sum( REALMINVAL=psi_min )       
!      IF (par_rank .EQ. 0) PRINT *, 'max(psi) =', psi_max, 'min(psi) =', psi_min

      distance(:) = distance_t(:) - fd(:)*MAX(0.0_pr, distance_t(:) - C_DES*psi(:)*h_phys_max(:))
!      length_LES(:) = C_DES*psi(:)*(fd(:)*PRODUCT(h_phys(:,1:dim), DIM=2)**(1.0_pr/REAL(dim,pr)) + (1.0_pr - fd(:))*h_phys_max(:))
!      distance(:) = distance_t(:) - fd(:)*MAX(0.0_pr, distance_t(:) - length_LES(:))
    ELSE 
      CALL error( 'DDES is only applicable for 3D, i.e. dim =3.' )
    END IF
      
  END SUBROUTINE DDES_length_scale

  FUNCTION cal_psi(nlocal, u_local, ne_local, props)
    IMPLICIT NONE

    INTEGER, INTENT(IN)  :: nlocal, ne_local
    REAL (pr), DIMENSION (nlocal,ne_local), INTENT(IN)    :: u_local
    REAL (pr), DIMENSION (nlocal), INTENT(IN)             :: props               ! nondimensional dynamic viscosity (1/Re*Sutherland)

    REAL (pr), DIMENSION (nlocal)         :: chi, fv1, fv2, ft2, cal_psi
    REAL (pr) :: C_w1, f_w_star = 0.424_pr, psi_max, psi_min, chi_max, chi_min

      C_w1 = C0_RANS/C3_RANS**2 + (1.0_pr+C1_RANS)/C2_RANS

      chi(:) = MAX(0.0_pr,u_local(:,n_var_MT)) / props(:)

      fv1(:) = chi(:)**3/(chi(:)**3+C6_RANS**3)

      fv2(:) = 1.0_pr - chi(:)/(1.0_pr+chi(:)*fv1(:))

      ft2(:) = C7_RANS*exp(-C8_RANS*chi(:)**2)

      cal_psi(:) = SQRT( MIN(100.0_pr, (1.0_pr-C0_RANS/(C_w1*C3_RANS**2*f_w_star)*(ft2(:)+(1.0_pr-ft2)*fv2))/MAX(fv1(:)*MAX(1.0e-10_pr,1.0_pr-ft2(:)), 1.0e-12) ) )

!      chi_max = MAXVAL( chi(:) )
!      chi_min = MINVAL( chi(:) )
!      CALL parallel_global_sum( REALMAXVAL=chi_max )
!      CALL parallel_global_sum( REALMINVAL=chi_min )       
!      IF (par_rank .EQ. 0) PRINT *, 'max(chi) =', chi_max, 'min(chi) =', chi_min

!      psi_max = MAXVAL( psi(:) )
!      psi_min = MINVAL( psi(:) )
!      CALL parallel_global_sum( REALMAXVAL=psi_max )
!      CALL parallel_global_sum( REALMINVAL=psi_min )       
!      IF (par_rank .EQ. 0) PRINT *, 'max(psi) =', psi_max, 'min(psi) =', psi_min
  END FUNCTION cal_psi

  SUBROUTINE cal_fd_tilde_fe(nlocal, distance_t, mut, u_local, ne_local, h_phys_max, psi, fdt, fB, fd_tilde, fe)
    IMPLICIT NONE

    INTEGER, INTENT(IN)  :: nlocal, ne_local
    REAL (pr), DIMENSION (nlocal), INTENT(IN) :: distance_t
    REAL (pr), DIMENSION (nlocal), INTENT(IN)    :: mut
    REAL (pr), DIMENSION (nlocal,ne_local), INTENT(IN)    :: u_local
    REAL (pr), DIMENSION (nlocal), INTENT(IN)             :: h_phys_max          ! max(delta_x, delta_y, delta_z)

    INTEGER :: i, k, j, j_df, wlt_type, face_type, idim
    REAL (pr), DIMENSION (nlocal,dim)         :: v               ! velocity components
    REAL (pr), DIMENSION (dim,nlocal,dim)     :: du, d2u         ! velocity gradients
    REAL (pr), DIMENSION (nlocal)         :: temp                ! temperature
    REAL (pr), DIMENSION (nlocal)         :: rdt, rdl, fdt, fB, fd_tilde, fe1, fe2, fe, psi
    REAL (pr), DIMENSION (nlocal)         :: props               ! nondimensional dynamic viscosity (1/Re*Sutherland)
    REAL (pr), DIMENSION (nlocal)         :: gradUsqr            ! grad_u_ij && grad_u_ij
    REAL (pr), DIMENSION (nlocal)         :: alpha_tmp
    REAL (pr) :: C_t = 1.63_pr, C_l = 3.55_pr
    REAL (pr) :: sumv, tmp1, tmp2
    REAL (pr) :: floor

    floor = 1.e-12_pr

    DO i = 1, dim
        v(:,i) = u_local(:,n_mom(i)) / u_local(:,n_den)       ! u_i
    END DO

    temp(:) = (u_local(:,n_eng)-0.5_pr*SUM(u_local(:,n_mom(1):n_mom(dim))**2,DIM=2)/u_local(:,n_den))/cv_in(Nspec)/u_local(:,n_den)/F_compress
    ! constant viscosity
    props(:) = mu_in(Nspec)  ! Nspec=1
    ! variable viscosity
    IF ( viscmodel .EQ. viscmodel_sutherland ) THEN  ! mu_in*(1.0_pr+sutherland_const)/(v(:,dimp1)+sutherland_const)*(v(:,dimp1))**(1.5_pr)
       props(:) = mu_in(Nspec) * calc_nondim_sutherland_visc( temp(:), nwlt, sutherland_const )
    END IF

    CALL c_diff_fast( v(:,1:dim), du(1:dim,:,:), d2u(1:dim,:,:), j_lev, nlocal, HIGH_ORDER, 10, dim, 1, dim )  
    gradUsqr = 0.0_pr
    DO idim = 1, dim
      DO i = 1,dim
        gradUsqr(:) = gradUsqr(:) + du(idim,:,i)*du(idim,:,i)
      END DO
    END DO

!    sumv = SUM( gradUsqr(:) )
!    CALL parallel_global_sum( REAL=sumv )
!    IF (par_rank .EQ. 0) PRINT *, 'sum(gradUsqr) =', sumv

    rdt(:) = mut(:) / (MAX( SQRT(gradUsqr(:)), 1.0e-10_pr )*u_local(:,n_den)*C3_RANS**2*MAX(distance_t(:),floor)**2)
    rdl(:) = props(:) / (MAX( SQRT(gradUsqr(:)), 1.0e-10_pr )*u_local(:,n_den)*C3_RANS**2*MAX(distance_t(:),floor)**2)
    fdt(:) = 1.0_pr - TANH((8.0_pr*rdt(:))**3)

    fe2(:) = 1.0_pr - MAX( TANH((C_t**2*rdt(:))**3), TANH(C_l**2*rdl(:))**3 )

    !alpha_tmp(:) = 0.25_pr - distance_t(:)/h_phys_max(:)
    alpha_tmp(:) = 0.25_pr - distance_t(:)/0.1_pr
    fB(:) = MIN( 2.0_pr*EXP(-9.0_pr*(alpha_tmp(:))**2), 1.0_pr ) 

    fd_tilde(:) = MAX( 1.0_pr - fdt(:), fB(:))

    DO j = 1, j_lev
       DO wlt_type = MIN(j-1,1),2**dim-1
          DO face_type = 0, 3**dim - 1
             DO j_df = j, j_lev
                DO k = 1, indx_DB(j_df,wlt_type,face_type,j)%length  
                   i = indx_DB(j_df,wlt_type,face_type,j)%p(k)%i

                   IF (alpha_tmp(i) .GE. 0.0_pr) THEN 
                     fe1(i) = 2.0_pr*EXP(-11.09_pr*(alpha_tmp(i))**2)
                   ELSE
                     fe1(i) = 2.0_pr*EXP(-9.0_pr*(alpha_tmp(i))**2)
                   END IF

                END DO
             END DO
          END DO
       END DO
    END DO
    
    psi(:) = cal_psi(nlocal, u_local, ne_local, props)
    fe(:) = MAX( fe1(:) - 1.0_pr, 0.0_pr ) * psi(:) * fe2(:)

    !tmp1 = MAXVAL( psi(:) )
    !tmp2 = MINVAL( psi(:) )
    !CALL parallel_global_sum( REALMAXVAL=tmp1 )
    !CALL parallel_global_sum( REALMINVAL=tmp2 )
    !IF (par_rank .EQ. 0) PRINT *, 'max(psi) =', tmp1, 'min(psi) = ', tmp2

    !tmp1 = MAXVAL( fe1(:) )
    !tmp2 = MINVAL( fe1(:) )
    !CALL parallel_global_sum( REALMAXVAL=tmp1 )
    !CALL parallel_global_sum( REALMINVAL=tmp2 )
    !IF (par_rank .EQ. 0) PRINT *, 'max(fe1) =', tmp1, 'min(fe1) = ', tmp2

    !tmp1 = MAXVAL( fe2(:) )
    !tmp2 = MINVAL( fe2(:) )
    !CALL parallel_global_sum( REALMAXVAL=tmp1 )
    !CALL parallel_global_sum( REALMINVAL=tmp2 )
    !IF (par_rank .EQ. 0) PRINT *, 'max(fe2) =', tmp1, 'min(fe2) = ', tmp2

  END SUBROUTINE cal_fd_tilde_fe

  SUBROUTINE IDDES_length_scale(nlocal, distance, distance_t, h_phys, h_phys_max, nwall_dir, psi, fd_tilde, fe)
    IMPLICIT NONE

    INTEGER, INTENT(IN)  :: nlocal, nwall_dir
    REAL (pr), DIMENSION (nlocal) :: distance
    REAL (pr), DIMENSION (nlocal), INTENT(IN) :: distance_t ! true wall distance
    REAL (pr), DIMENSION(1:nlocal,1:dim), INTENT(IN)      :: h_phys
    REAL (pr), DIMENSION (nlocal), INTENT(IN)             :: h_phys_max          ! max(delta_x, delta_y, delta_z)
    REAL (pr), DIMENSION (nlocal), INTENT(IN)             :: psi, fd_tilde, fe

    REAL (pr), DIMENSION (nlocal) :: length_LES
    REAL (pr) :: C_DES = 0.65_pr, C_w = 0.15_pr

    IF(dim .EQ. 3) THEN
      length_LES(:) = C_DES*psi(:)*MIN( MAX( C_w*distance_t(:), C_w*h_phys_max(:), h_phys(:, nwall_dir) ), h_phys_max(:) )
      distance(:) = fd_tilde(:)*(1.0_pr + fe(:))*distance_t(:) + (1.0_pr - fd_tilde(:))*length_LES(:)
    ELSE 
      CALL error( 'IDDES is only applicable for 3D, i.e. dim =3.' )
    END IF
      
  END SUBROUTINE IDDES_length_scale

END MODULE URANS_compressible_SA
