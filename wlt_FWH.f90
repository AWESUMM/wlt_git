MODULE wlt_FWH

  
  USE precision
  USE wlt_vars
  USE wlt_trns_mod
  USE util_vars
  USE additional_nodes
  USE io_3D_vars
  USE pde
  USE parallel


CONTAINS

  !  Qianlong, modify these subroutnes acordingly

  SUBROUTINE FWH_stats ( u_limited , nlocal, n_limited_var, dtau, startup_flag)

    IMPLICIT NONE
    INTEGER , INTENT (IN) :: nlocal, n_limited_var, startup_flag
    REAL (pr), DIMENSION (nlocal,1:n_limited_var), INTENT (IN) :: u_limited
    REAL (pr), INTENT(IN) :: dtau
    LOGICAL :: oldfile_flag
    INTEGER, DIMENSION(dim,1) :: ZERO
    INTEGER :: i1(1), ipatch
    CHARACTER (LEN=4) :: string

!
! Generate statisitics with variables previously defined in this module
!
    IF( additional_nodes_initialized) THEN
       IF(par_rank .EQ.0) THEN
       DO ipatch = 1,n_additional_patches
          WRITE(string,'(I4.4)') ipatch
          
          !queary flow variable files
          INQUIRE(FILE = TRIM(res_path)//TRIM(file_gen)//'/patch'//string//'.dat', OPENED=oldfile_flag )
          IF(oldfile_flag .EQV. .FALSE.) THEN !first run, clear file - either all patch files are new or all are old

             OPEN(UNIT=100+(2*ipatch-1), FILE = TRIM(res_path)//TRIM(file_gen)//'/patch'//string//'.dat', STATUS = 'REPLACE', form="FORMATTED") !flow variable files
          ELSE !append to current file
             OPEN(UNIT=100+(2*ipatch-1), FILE = TRIM(res_path)//TRIM(file_gen)//'/patch'//string//'.dat', STATUS = 'UNKNOWN', form="FORMATTED") !flow variable files
          END IF

          OPEN(UNIT=100+2*ipatch    , FILE = TRIM(res_path)//TRIM(file_gen)//'/grid'//string//'.dat', STATUS = 'UNKNOWN', form="FORMATTED")
       END DO
       END IF
       ZERO = 0

       CALL FWH_save_time(u_limited,nlocal, n_limited_var, dtau,startup_flag)
    END IF

  END SUBROUTINE FWH_stats



  SUBROUTINE FWH_save_time (u_limited, nlocal, n_limited_var, FWH_dtwrite, startup_flag)
    USE share_kry
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: nlocal, n_limited_var, startup_flag
    REAL (pr), DIMENSION (nlocal,1:n_limited_var), INTENT (IN) :: u_limited
    REAL (pr), INTENT(IN) :: FWH_dtwrite

    INTEGER, DIMENSION(n_additional_nodes) :: i_patches
    INTEGER :: numpts,filesize,npts_total
    INTEGER :: j,i, k, l ,m , ipatch, unit,idim
    CHARACTER (LEN=4) :: string

    INTEGER, DIMENSION(dim) :: ixyz_dum,header
    INTEGER, DIMENSION(n_additional_nodes) :: cproc, i_FWH_local
    INTEGER, DIMENSION(dim,n_additional_nodes) :: ixyz_add_local
    REAL(pr), DIMENSION(n_additional_nodes,dim) :: x_FWH
    REAL(pr), DIMENSION(n_additional_nodes,n_limited_var) :: u_FWH,u_FWH_write
    REAL(pr), DIMENSION(n_additional_nodes*par_size,n_limited_var) :: u_FWH_long
    REAL(pr), DIMENSION(n_additional_nodes*par_size,dim) :: x_FWH_long
    REAL(pr), DIMENSION(n_additional_nodes,par_size) :: u_FWH_wide
    REAL(pr), DIMENSION(n_additional_nodes,par_size) :: x_FWH_wide

    REAL(pr), SAVE :: FWH_twrite, t_prev
    REAL(pr), SAVE, ALLOCATABLE, DIMENSION(:,:) :: u_FWH_prev



    IF(startup_flag .LE. 0) THEN !first call, initialize

       FWH_twrite = FWH_dtwrite  !set first save point
       t_prev = 0.0_pr    
       

    ELSE IF (startup_flag > 0) THEN
       IF( additional_nodes_initialized  ) THEN



          !Determine if stats should be passed to proc0 (for interpolation or writing)
          IF(t .GE. t_adapt .OR. t .GE. FWH_twrite) THEN  !adaptive timestep (need for interpolation) or ready to write
          
             !------------------------------------
             !Arrange indices for points on process
             ixyz_dum(:) = 0


#ifdef MULTIPROC
             !identify locally held points for parallel runs
             DO i = 1, n_additional_nodes
                CALL DB_get_proc_by_coordinates( ixyz_additional_nodes(1:dim,i), j_lev, cproc(i) )
                IF(cproc(i) .EQ. par_rank) ixyz_dum(:) = ixyz_additional_nodes(1:dim,i) !placeholder of points that are on local processor for retreiving indices
             END DO

             DO i=1,dim
                WHERE(cproc .EQ. par_rank)
                   ixyz_add_local(i,:) = ixyz_additional_nodes(i,:)
                ELSEWHERE
                   ixyz_add_local(i,:) = ixyz_dum(i)
                END WHERE
             END DO
#else
             !serial run
             cproc = 0
             ixyz_add_local(:,:) = ixyz_additional_nodes(:,:)
#endif          

             
             CALL get_indices_by_coordinate(n_additional_nodes,ixyz_add_local(1:dim,1:n_additional_nodes),j_lev,i_FWH_local,0)
             
             !insert place-holding index for all non-local points
             WHERE(cproc .NE. par_rank)
                i_FWH_local = 1  
             END WHERE
             
             !initailize FWH stats arrays
             x_FWH  = 0.0_pr
             u_FWH = 0.0_pr
             
             !populate stats arrays 
             DO i = 1,n_additional_nodes
                x_FWH(i,:) = x(i_FWH_local(i),:)
                u_FWH(i,:) = u_limited(i_FWH_local(i),:)      
             END DO
             
             !zero out nonlocal values for global sum
             DO i=1,dim
                WHERE(cproc .NE. par_rank)
                   x_FWH(:,i) = 0.0_pr
                   !following are for debugging and not used anywhere else in FWH stats
                   i_FWH_local = 0  
                   ixyz_add_local(i,:) = 0
                END WHERE
             END DO
             
             DO i=1,n_limited_var  !zero-out values for points not on processor
                WHERE(cproc .NE. par_rank)
                   u_FWH(:,i) = 0.0_pr
                END WHERE
             END DO
             
             
             
             !global sum - consolidate using a global vector sum
             DO i=1,dim
                CALL parallel_vector_sum(REAL=x_FWH(:,i),LENGTH=n_additional_nodes)
             END DO
             
             DO i = 1,n_limited_var
                CALL parallel_vector_sum(REAL=u_FWH(:,i),LENGTH=n_additional_nodes)
             END DO

!Use standard parallel calls 
!!$             DO i=1,dim
!!$                CALL parallel_gather(x_FWH(:,i),x_FWH_long(:,i),n_additional_nodes)
!!$                x_FWH_wide=RESHAPE(x_FWH_long(:,i),(/n_additional_nodes,par_size/))
!!$                x_FWH(:,i) = SUM(x_FWH_wide,DIM=2)
!!$             END DO
!!$             
!!$             DO i=1,n_limited_var
!!$                CALL parallel_gather(u_FWH(:,i),u_FWH_long(:,i),n_additional_nodes)
!!$                u_FWH_wide=RESHAPE(u_FWH_long(:,i),(/n_additional_nodes,par_size/))
!!$                u_FWH(:,i) = SUM(u_FWH_wide,DIM=2)
!!$             END DO
             
             
             
             !---------------------------------------------------------
             !             IF (par_rank.EQ.0 .AND. dim .EQ. 2) THEN   !Write files using process 0
             IF (par_rank.EQ.0) THEN   !Interpolate and/or Write files - process 0

                !allocate and verify previous timestep variables
                IF(.NOT. ALLOCATED(u_FWH_prev)) THEN
                   ALLOCATE(u_FWH_prev(n_additional_nodes,n_limited_var))
                   u_FWH_prev(:,:) = u_FWH(:,:)
                   
                ELSE IF ( ( SIZE(u_FWH_prev,1).NE. n_additional_nodes ) .OR. ( SIZE(u_FWH_prev,2).NE. n_limited_var ) ) THEN !verify proper allocation
                   DEALLOCATE(u_FWH_prev)  !reallocate with proper dimensions
                   ALLOCATE(u_FWH_prev(n_additional_nodes,n_limited_var))
                   u_FWH_prev(:,:) = u_FWH(:,:)
                END IF
                
                IF (t .GE. FWH_twrite ) THEN !Interpolate and write stats
                
                   !********INTERPOLATE HERE************
                   IF(t.GE.t_adapt) THEN !interpolate
                      u_FWH_write = u_FWH - (t-FWH_twrite)*(u_FWH-u_FWH_prev)/(t-t_prev)
                   ELSE  !no interpolation required, just write
                      u_FWH_write = u_FWH 
                   END IF

                   !***********************************

                   !startup flag old location
                   !         IF( additional_nodes_initialized  ) THEN
                   IF(dim ==2 .AND. n_limited_var .NE. 4) PRINT *, 'WARNING: incorrect number of variables, please check calls for FWH stats'
                   IF(dim ==3 .AND. n_limited_var .NE. 5) PRINT *, 'WARNING: incorrect number of variables, please check calls for FWH stats'
                   
                   !   CALL get_indices_by_coordinate(n_additional_nodes,ixyz_additional_nodes(1:2,1:n_additional_nodes),j_lev,i_patches,0)         
                   
                   !----------WRITE PATCH FILE------
                   
                   !write log of actual save times
                   OPEN(UNIT=75    , FILE = TRIM(res_path)//TRIM(file_gen)//'/twrite_log.dat', STATUS = 'UNKNOWN', form="FORMATTED")
                   WRITE(75,'(2(F12.10))')t,FWH_twrite
                   
                   l = 0
                   !ipatch - patch
                   DO ipatch = 1,n_additional_patches  !write a separate file for each patch
                      WRITE(string,'(I4.4)') ipatch  
                      unit = 	100+(2*ipatch-1)               
                      INQUIRE(unit, SIZE=filesize)
                      IF (filesize<5 .AND. dim ==2) THEN ! this is the inital write, write a header
                         numpts = n_patch(ipatch,1)               
                         WRITE(unit,'(3(I4.4) ,1(E20.12))') numpts, n_limited_var, FLOOR((t_end-t_begin)/FWH_dtwrite), FWH_dtwrite !write the header, which has the timing information       
                      ELSE IF (filesize<5 .AND. dim ==3 ) THEN ! this is the inital write, write a header
                         idim = (ipatch +1)/2
                         header(idim             ) = 1
                         header(MOD(idim,dim)+1  ) = n_patch(ipatch,1)
                         header(MOD(idim+1,dim)+1) = n_patch(ipatch,2)               
                         WRITE(unit,'(5(I4.4) ,1(E20.12))') header, n_limited_var, FLOOR((t_end-t_begin)/FWH_dtwrite), FWH_dtwrite !write the header, which has the timing information       
                      END IF
                      !Append flow data in file
                      IF(dim ==2) THEN
                         DO k = l+1, l+n_patch(ipatch,1)
                            l = l+1
                            WRITE(unit,'(4(E24.16))') u_FWH_write(k,1), u_FWH_write(k,2), u_FWH_write(k,3), u_FWH_write(k,4)
                         END DO
                      ELSE IF(dim ==3)THEN
                         DO k = l+1, l+n_patch(ipatch,1)*n_patch(ipatch,2)
                            l = l+1
                            WRITE(unit,'(5(E24.16))') u_FWH_write(k,1), u_FWH_write(k,2), u_FWH_write(k,3), u_FWH_write(k,4), u_FWH_write(k,5)
                         END DO
                      END IF
                   END DO
                   
                   !-----WRITE GRID FILE-----
                   
                   ! for fixed contour at every timestep
                   ! only need to store once

                   PRINT *, 'FWH additional nodes:',SHAPE(ixyz_additional_nodes)
                   
                   !ipatch - patch
                   DO ipatch = 1,n_additional_patches !Write file for each patch
                      write(string,'(I4.4)') ipatch
                      unit = 	100+2*ipatch				
                      IF(dim .EQ. 2) THEN
                         numpts = n_patch(ipatch,1) !number of points, used in the header
                         header = numpts
                         write(unit,'(1(I4))') header(1)  !write the number of points as the file header

                      ELSE IF(dim .EQ. 3) THEN
                         idim = (ipatch +1)/2
                         header(idim             ) = 1
                         header(MOD(idim,dim)+1  ) = n_patch(ipatch,1)
                         header(MOD(idim+1,dim)+1) = n_patch(ipatch,2)
                         numpts = n_patch(ipatch,1)*n_patch(ipatch,2)
                         write(unit,'(3(I4))') header(1:3)  !write the number of points as the file header

                      END IF
                      
!!$                   DO k = l+1, l+n_patch(ipatch,1) !write points in each patch                      
!!$                      write(unit,'(2(E24.16))') x_FWH(k,1), x_FWH(k,2)
!!$                      l = l+1
!!$                   END DO
                      l = 0
                      DO idim = 1,dim !reuse index
                         DO k = 1,numpts
                            write(unit,'(E24.16)') x_FWH(k+l,idim)
                            !write(unit,'(I4)') ixyz_additional_nodes(idim,k+l)
                         END DO
                      END DO
                      l = l + numpts
                      CLOSE(unit)
                   END DO
                   
                   
                   FWH_twrite = FWH_twrite + FWH_dtwrite !set new write point
                   
                   
                END IF
                
                
                !ELSE IF(dim .EQ. 3) THEN
!!$             IF(par_rank .EQ. 0 .AND. dim .EQ. 3) THEN
!!$
!!$                !-----WRITE GRID FILE-----
!!$                l = 0
!!$                DO ipatch = 1,n_additional_patches !Write file for each patch                   
!!$                   write(string,'(I4.4)') ipatch
!!$                   unit = 	100+2*ipatch				
!!$                   idim = (ipatch +1)/2
!!$                   header(idim             ) = 1
!!$                   header(MOD(idim,dim)+1  ) = n_patch(ipatch,1)
!!$                   header(MOD(idim+1,dim)+1) = n_patch(ipatch,2)
!!$                   
!!$                   write(unit,'(I4)') header  !write the patch dimensions as the header
!!$                   
!!$                   numpts = n_patch(ipatch,1)*n_patch(ipatch,2)
!!$                   
!!$                   DO idim = 1,dim !reuse index
!!$                      PRINT *, idim
!!$                      DO k = 1,numpts
!!$                         write(unit,'(2(E24.16))') x_FWH(k+l,idim)
!!$                         !write(unit,'(I4)') ixyz_additional_nodes(idim,k+l)
!!$                      END DO
!!$                   END DO
!!$                   
!!$                   l = l + numpts
!!$                   CLOSE(unit)
!!$                END DO           
!!$             END IF
                
                !save current variables in previous slots
                t_prev = t
                u_FWH_prev(:,:) =  u_FWH(:,:) 
                
             END IF
             
          END IF
       END IF
    END IF
  END SUBROUTINE FWH_save_time

END MODULE wlt_FWH
