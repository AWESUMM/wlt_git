MODULE kry_mod
  USE precision
  USE share_kry
  USE sizes
  USE elliptic_mod
  USE pde
  USE share_consts
  USE user_case

  IMPLICIT NONE
CONTAINS

!--************************************************************************ 
!--Begin Krylov time step subroutines
!--************************************************************************
SUBROUTINE kry_time_step (u, p,  nwlt_in, ddt, t_local)
  USE debug_vars
  USE parallel
  USE variable_mapping
  IMPLICIT NONE
  
  INTEGER, INTENT (IN) :: nwlt_in
  REAL (pr) :: ddt, t_local
  REAL (pr), DIMENSION (nwlt_in*n_integrated), INTENT(INOUT) :: u  !1D flat version of integrated variables without BC
  REAL (pr), DIMENSION (nwlt_in), INTENT (INOUT) :: p   

  INTEGER                                      :: j, j_p, m, nttl
  REAL (pr)                                    :: err, mod_cj, mod_rhs_U, mod_Ub
  REAL (pr), DIMENSION (nwlt_in*n_integrated)                  :: rhs_U, u_aux1, u_aux2, zsum, zsum_err
  REAL (pr), DIMENSION (n_grid)                :: tau_p
  REAL (pr), DIMENSION (nwlt_in*n_integrated, n_grid)          :: R_taup, u_err, u_tau
  REAL (pr), DIMENSION (nwlt_in*n_integrated, kry_l)           :: v
  REAL (pr), DIMENSION (nwlt_in*n_integrated, j_it)            :: c_j
  REAL (pr), DIMENSION (kry_l, kry_l)          :: hess 
  REAL (pr), DIMENSION (nwlt_in*n_integrated, kry_m, j_it)     :: v_m
  REAL (pr), DIMENSION (kry_m, kry_m, j_it)    :: h_m 
  INTEGER, PARAMETER :: meth = 1
  LOGICAL                                      :: ireturn
  LOGICAL, PARAMETER                           :: l2=.TRUE.
  REAL(pr) :: tmp1, tmp2
  INTEGER :: alloc_stat

  told = t_local
  nttl = nwlt_in*n_integrated
  
  IF (ALLOCATED(u_prev_timestep)) DEALLOCATE(u_prev_timestep)
  ALLOCATE (u_prev_timestep(1:nttl),STAT=alloc_stat)
  CALL test_alloc( alloc_stat, 'u_prev_timestep in kry_time_step',nttl )
  u_prev_timestep = u

  n = nttl
  ne = n_integrated
  ng = nttl/ne

  mod_Ub = DOT_PRODUCT (u_prev_timestep, u_prev_timestep)
  CALL parallel_global_sum( REAL=mod_Ub )
  mod_Ub = SQRT (mod_Ub)
  
  
  !--Generate orthonormal basis for Krylov subspace v and approximate operator h
  rhs_U = Kry_rhs (u_prev_timestep,p)
  !rhs of the equation
  mod_rhs_U = DOT_PRODUCT (rhs_U, rhs_U)
  CALL parallel_global_sum( REAL=mod_rhs_U )
  mod_rhs_U = SQRT (mod_rhs_U)
  
  !routine arnoldi calls Drhs()
  CALL arnoldi (kry_l, rhs_U, mod_rhs_U, v, hess)  
  
  m = 1	
  ireturn = .TRUE.			
  DO WHILE (ireturn)
     !--Chebychev grid
     DO j_p = 1, n_grid
        tau_p(j_p) = 0.5_pr  * ddt * ( 1.0_pr - COS ( j_p * pi / n_grid ))
        
        !--Linear solution at each grid point
        u_tau(:,j_p) = MATMUL (v, MATMUL (exp_mat (hess, tau_p(j_p), 1), u_vec (kry_l, mod_rhs_U)))
        u_err(:,j_p) = MATMUL (v, MATMUL (exp_mat (hess, tau_p(j_p), 2), u_vec (kry_l, mod_rhs_U)))

        !--Remainder term	
        !   note: routine rmd_t calls Drhs()
        R_taup(:,j_p) = rmd_t (u_tau(:,j_p))
     END DO

     !--Obtain the coefficients C_j
     CALL least_square (n, n_grid, j_it, tau_p, R_taup, c_j)

     !--Form krylov subspace for each polynomial

     DO j = 1, j_it
        mod_cj = DOT_PRODUCT (c_j(:,j), c_j(:,j))
        CALL parallel_global_sum( REAL=mod_cj )
        mod_cj = SQRT (mod_cj)
        CALL arnoldi (kry_m, c_j(:,j), mod_cj, v_m(:,:,j), h_m(:,:,j))
     END DO
     
     !--Calcul de la somme non lineaire pour chaque point de la grille
     DO j_p = 1, n_grid
        zsum     = u_tau(:, j_p)
        zsum_err = u_err(:, j_p)
        DO j = 1, j_it
           mod_cj = DOT_PRODUCT (c_j(:,j), c_j(:,j))
           CALL parallel_global_sum( REAL=mod_cj )
           mod_cj = SQRT (mod_cj)
           u_aux1 = MATMUL (v_m(:,:,j), MATMUL (integ (h_m(:,:,j), j+1, tau_p(j_p), 1), u_vec(kry_m, mod_cj)))
           u_aux2 = MATMUL (v_m(:,:,j), MATMUL (integ (h_m(:,:,j), j+1, tau_p(j_p), 2), u_vec(kry_m, mod_cj))) &
                  + c_j(:,j) * tau_p(j_p)**(j+1)
           zsum     =	zsum     + u_aux1
           zsum_err =	zsum_err + u_aux2
        END DO

        !--Final form for u at each grid-point
        u_tau(:,j_p) = zsum
     END DO



     !--Estimate l2 or linf error
     IF (l2) THEN
        tmp1 = SUM((zsum_err - RESHAPE( Kry_rhs(u_prev_timestep+u_tau(:,n_grid),p), (/nttl/) ) )**2)
        tmp2 = REAL(n,pr)
        CALL parallel_global_sum( REAL=tmp1 )
        CALL parallel_global_sum( REAL=tmp2 )
        err = tau_p(n_grid) * SQRT(tmp1/tmp2) !--L2 error
     ELSE
        tmp1 = MAXVAL(ABS(zsum_err - RESHAPE( Kry_rhs(u_prev_timestep+u_tau(:,n_grid),p), (/nttl/) ) ))
        CALL parallel_global_sum( REALMAXVAL=tmp1 )
        err = tau_p(n_grid) * tmp1 !--Linf error
     END IF

     IF (err<tol3 .OR. t_local-t_begin < t_adapt) THEN
        u = u_prev_timestep + u_tau(:,n_grid)
        t_local = t_local + tau_p(n_grid)
        IF (err<tol3/4.0_pr .AND. t_local-t_begin >= t_adapt) THEN !--Assume third-order error (verified)
           ddt = MIN(dtmax,tau_p(n_grid) * 1.5_pr)
        ELSE
           ddt = tau_p(n_grid)
        END IF
        ireturn = .FALSE.
     ELSE IF(err>=tol3 .AND. t_local-t_begin >= t_adapt) THEN
        ddt = ddt * 0.75_pr !--Decrease error by 1/2 assuming third-order error
        ireturn = .TRUE.
        m = m+1
     END IF
     IF (par_rank.EQ.0) THEN
        PRINT *, '***********************************************'
        PRINT *, '*            t_local =',t_local
        PRINT *, '***********************************************'
     END IF
  END DO

  !--Projection step
  CALL user_project (u, p, nwlt_in, meth)

  !setting right boundary conditions
  CALL project_BC (u, meth, .FALSE.)

END SUBROUTINE kry_time_step

FUNCTION exp_mat (hess, ddt, flag)
  IMPLICIT NONE
  INTEGER, INTENT (IN) :: flag
  REAL (pr), INTENT (IN) :: ddt
  REAL (pr), DIMENSION (kry_l, kry_l), INTENT (IN) :: hess

  INTEGER :: i, j
  REAL (pr), DIMENSION (kry_l, kry_l) :: exp_mat
  COMPLEX (pr), DIMENSION (kry_l) :: eval
  COMPLEX (pr), DIMENSION (kry_l, kry_l) :: evec, evec_inv

  !--Variables for linear algebra routines
  INTEGER :: info
  INTEGER, PARAMETER :: lwork = 34*kry_l, lwork1 = kry_l
  INTEGER, DIMENSION (kry_l) :: ipvt
  REAL (pr), DIMENSION (lwork) :: work
  REAL (pr), DIMENSION (kry_l) :: wr, wi
  REAL (pr), DIMENSION (kry_l, kry_l) :: h_tmp, vl, vr
  COMPLEX (pr), DIMENSION (2) :: det
  COMPLEX (pr), DIMENSION (lwork1) :: work1

  h_tmp = hess

  CALL dgeev ('N', 'V', kry_l, h_tmp, kry_l, wr, wi, vl, 1, vr, kry_l, work, lwork, info)
  eval = CMPLX (wr, wi)

  DO i = 1, kry_l
    IF (wi(i) == 0.0_pr) THEN
      evec(:,i) = CMPLX (vr(:,i), 0.0_pr)
    ELSE IF (wi(i) > 0.0_pr) THEN
      evec(:,i)   = CMPLX (vr(:,i),  vr(:,i+1))
      evec(:,i+1) = CMPLX (vr(:,i), -vr(:,i+1))
    END IF
  END DO

  evec_inv = evec
  CALL zgetrf (kry_l, kry_l, evec_inv, kry_l, ipvt, info)
  CALL zgetri (kry_l, evec_inv, kry_l, ipvt, work1, lwork, info)

  !--Calculate exponential matrix   
  IF (flag == 1) exp_mat = REAL (MATMUL (evec, MATMUL (diag_mat (kry_l, (EXP (ddt * eval) - 1.0_pr) / eval), evec_inv)), pr)  
  IF (flag == 2) exp_mat = REAL (MATMUL (evec, MATMUL (diag_mat (kry_l,  EXP (ddt * eval)), evec_inv)), pr)
END FUNCTION exp_mat

SUBROUTINE arnoldi (kry, u, mod_u, v, hess)
  USE parallel
  IMPLICIT NONE
  INTEGER :: i, j
  INTEGER, INTENT (IN) :: kry
  REAL (pr), INTENT (IN) :: mod_u
  REAL (pr), DIMENSION (n) :: Av_j, w
  REAL (pr), DIMENSION (n), INTENT (IN) :: u
  REAL (pr), DIMENSION (n, kry), INTENT (OUT) :: v 
  REAL (pr), DIMENSION (kry, kry), INTENT (OUT) :: hess


	  	
  hess = 0.0_pr
  w    = 0.0_pr
  v    = 0.0_pr
  v(:,1) = u / mod_u 
  DO j = 1, kry
     Av_j = RESHAPE( Kry_Drhs(v(:,j), u_prev_timestep, 1), (/n/) ) 
     w = Av_j
     DO i = 1, j
        hess (i,j) = DOT_PRODUCT (v(:,i), Av_j)
        CALL parallel_global_sum( REAL=hess(i,j) )
        w = w - v(:,i) * hess(i,j) 
     END DO
     IF (j < kry) THEN
        hess (j+1,j) = DOT_PRODUCT (w, w)
        CALL parallel_global_sum( REAL=hess(j+1,j) )
        hess (j+1,j) = SQRT (hess(j+1,j))
        v(:,j+1) = w / hess(j+1,j)
     END IF
  END DO
END SUBROUTINE arnoldi

SUBROUTINE integral (Int, lambda, j, ddt)
  IMPLICIT NONE
  INTEGER :: i
  INTEGER , INTENT (IN) :: j
  REAL (pr), INTENT (IN) :: ddt
  COMPLEX (pr), INTENT (OUT) :: Int
  COMPLEX (pr),INTENT (IN) :: lambda
  COMPLEX (pr), DIMENSION (0:j) :: Rec

  Rec(0) = 1.0_pr/lambda * (EXP (lambda * ddt) - 1.0_pr)
  DO i = 1, j
     Rec(i)= 1.0_pr/lambda * (REAL(i, pr) * Rec(i-1)- ddt**i)
  END DO
  Int = Rec(j)
END SUBROUTINE integral

SUBROUTINE  least_square (Ndim, Nobs, NCOF, tau, REM, BB)
  IMPLICIT NONE
  INTEGER :: i, nn, Ndeg
  INTEGER , INTENT (IN) :: Ndim, Nobs, NCOF
  REAL (pr), DIMENSION (Nobs), INTENT(IN) :: tau
  REAL (pr), DIMENSION (Ndim,Nobs), INTENT (IN) :: REM
  REAL (pr), DIMENSION (Ndim, Ncof), INTENT (OUT) :: BB
  REAL (pr), DIMENSION (Nobs) :: Xdata
  REAL (pr), DIMENSION (Nobs) :: Ydata
  REAL (pr) :: det, det1, det2

  Ndeg = ncof+1

  DO i = 1, Ndim
     DO nn = 1, Nobs
        Xdata(nn) = tau(nn)
        Ydata(nn) = REM(i,nn)
     END DO
     det  = xdata(1)**2 * xdata(2)**3 - xdata(2)**2 * xdata(1)**3
     det1 = ydata(1)    * xdata(2)**3 - ydata(2)    * xdata(1)**3
     det2 = xdata(1)**2 * ydata(2)    - xdata(2)**2 * ydata(1)

     BB(i,1) = det1/det
     BB(i,2) = det2/det
  END DO
END SUBROUTINE least_square


FUNCTION integ (hess, j, ddt, flag)
  IMPLICIT NONE
  INTEGER :: i
  INTEGER, INTENT (IN) :: flag, j
  REAL (pr), INTENT (IN) :: ddt
  REAL (pr), DIMENSION (kry_m, kry_m), INTENT (IN) :: hess
  REAL (pr), DIMENSION (kry_m, kry_m) :: integ
  COMPLEX (pr), DIMENSION (kry_m, kry_m) :: evec, evec_inv
  COMPLEX (pr), DIMENSION (kry_m)   :: eval, stok

  !--Variables for linear algebra routines
  INTEGER :: info
  INTEGER, PARAMETER :: lwork = 34*kry_m, lwork1 = kry_m
  INTEGER, DIMENSION (kry_m) :: ipvt
  REAL (pr), DIMENSION (lwork) :: work
  REAL (pr), DIMENSION (kry_m) :: wr, wi
  REAL (pr), DIMENSION (kry_m, kry_m) :: h_tmp, vl, vr
  COMPLEX (pr), DIMENSION (2) :: det
  COMPLEX (pr), DIMENSION (lwork1) :: work1



  h_tmp = hess
  CALL dgeev ('N', 'V', kry_m, h_tmp, kry_m, wr, wi, vl, 1, vr, kry_m, work, lwork, info)
  eval = CMPLX (wr, wi)

  DO i = 1, kry_m
    IF (wi(i) == 0.0_pr) THEN
      evec(:,i) = CMPLX (vr(:,i), 0.0_pr)
    ELSE IF (wi(i) > 0.0_pr) THEN
      evec(:,i)   = CMPLX (vr(:,i),  vr(:,i+1))
      evec(:,i+1) = CMPLX (vr(:,i), -vr(:,i+1))
    END IF
  END DO	  

  DO i = 1, kry_m
     CALL integral (stok(i), eval(i), j, ddt)
  END DO

  evec_inv = evec
  CALL zgetrf (kry_m, kry_m, evec_inv, kry_m, ipvt, info)
  CALL zgetri (kry_m, evec_inv, kry_m, ipvt, work1, lwork, info)

  !--Calculate exponential matrix 
  IF (flag == 1) integ = REAL (MATMUL (evec, MATMUL (diag_mat (kry_m, stok), evec_inv)), pr)  
  IF (flag == 2) integ = REAL (MATMUL (evec, MATMUL (diag_mat (kry_m, eval*stok), evec_inv)), pr)
END FUNCTION integ

FUNCTION rmd_t (u)
  IMPLICIT NONE
  REAL (pr), DIMENSION (n), INTENT (IN) :: u
  REAL (pr), DIMENSION (n)              :: rmd_t
  REAL (pr)                             :: p(1) ! empty pressrue array for compatibility calling Kry_rhs()

  
  rmd_t =  Kry_rhs(u + u_prev_timestep,p) - (Kry_rhs(u_prev_timestep,p) + Kry_Drhs(u, u_prev_timestep, 1))
END FUNCTION rmd_t

FUNCTION diag_mat (m, val)
  IMPLICIT NONE
  INTEGER :: i
  INTEGER, INTENT (IN) :: m
  COMPLEX (pr), DIMENSION (m), INTENT (IN)   :: val
  COMPLEX (pr), DIMENSION (m,m) :: diag_mat

  diag_mat = CMPLX (0.0_pr, 0.0_pr)
  DO i = 1, m
     diag_mat(i,i) = val(i)
  END DO
END FUNCTION diag_mat

FUNCTION u_vec (m, val)
  IMPLICIT NONE
  INTEGER :: i
  INTEGER, INTENT (IN) :: m
  REAL (pr), INTENT (IN) :: val
  REAL (pr), DIMENSION (m) :: u_vec
	
  u_vec (1) = val
  u_vec (2:m) = 0.0_pr
END FUNCTION u_vec

FUNCTION Kry_rhs (Uvec,scalar)
  IMPLICIT NONE
  REAL (pr), DIMENSION (n), INTENT(IN) :: Uvec 
  REAL (pr), DIMENSION (ng), INTENT(IN) :: scalar
  REAL (pr), DIMENSION (n) :: Kry_rhs
  REAL (pr), DIMENSION (n) :: Uvec_aux
  INTEGER, PARAMETER :: meth=1
  
  !create Uvec_aux that satisfy boundary conditions
  Uvec_aux = Uvec
  IF(MINVAL(ABS(prd(1:dim))) == 0) THEN
     CALL project_BC (Uvec_aux, meth, .FALSE.)
  END IF
  Kry_rhs = user_rhs (Uvec_aux,scalar)

  !setting Kry_rhs=0 at locations of user_algebraic_BC
  Uvec_aux = 0.0_pr
  CALL user_algebraic_BC_diag (Uvec_aux, nwlt, ne, j_lev, meth)
  WHERE ( Uvec_aux /= 0.0_pr ) Kry_rhs = 0.0_pr

END FUNCTION Kry_rhs

FUNCTION Kry_Drhs (Uvec, Uvec_prev, meth)
  IMPLICIT NONE
  REAL (pr), DIMENSION (n), INTENT(IN) :: Uvec, Uvec_prev 
  REAL (pr), DIMENSION (n) :: Uvec_aux, Uvec_prev_aux 
  INTEGER, INTENT(IN) :: meth
  REAL (pr), DIMENSION (n) :: Kry_Drhs
  
  !create Uvec_aux & Uvec_prev_aux that satisfy boundary conditions
  Uvec_prev_aux = Uvec_prev
  Uvec_aux = Uvec
  IF(MINVAL(ABS(prd(1:dim))) == 0) THEN
     CALL project_BC (Uvec_Prev_aux, meth, .FALSE.) 
     CALL project_BC (Uvec_aux, meth, .TRUE.)  !perturbation, thus homogeneous
  END IF
  Kry_Drhs = user_Drhs (Uvec_aux, Uvec_prev_aux, meth)

  !setting Kry_Drhs=0 at locations of user_algebraic_BC
  Uvec_aux = 0.0_pr
  CALL user_algebraic_BC_diag (Uvec_aux, nwlt, ne, j_lev, meth)
  WHERE ( Uvec_aux /= 0.0_pr ) Kry_Drhs = 0.0_pr

  
END FUNCTION Kry_drhs

SUBROUTINE project_BC (Uvec, meth, homogeneous)
  USE variable_mapping
  IMPLICIT NONE
  REAL (pr), DIMENSION (ng,ne), INTENT(INOUT) :: Uvec
  INTEGER, INTENT(IN) :: meth
  LOGICAL, INTENT(IN) :: homogeneous
  REAL (pr), DIMENSION (n) :: rhs, rhs_aux
  REAL (pr), DIMENSION (nwlt_bnd*ne):: Uvec_bnd, Lkry_bnd_rhs 
  INTEGER :: ie, shift, shift_bnd
  INTEGER, PARAMETER :: project_BC_iter_meth = 0
  INTEGER, DIMENSION (n_integrated) :: clip

  clip = 0
  rhs = 0.0_pr  
  IF(.NOT.homogeneous) CALL user_algebraic_BC_rhs (rhs, ne, ng, j_lev)
  rhs_aux = 0.0_pr
  CALL user_algebraic_BC (rhs_aux, Uvec, ng, ne, j_lev, meth)
  DO ie = 1, ne
     shift_bnd = nwlt_bnd*(ie-1)
     shift = nwlt*(ie-1)
     Lkry_bnd_rhs(shift_bnd+1:shift_bnd+nwlt_bnd) = rhs(shift+nwlt_intrnl+1:shift+nwlt) - rhs_aux(shift+nwlt_intrnl+1:shift+nwlt)
  END DO
  Uvec_bnd = 0.0_pr
  IF(project_BC_iter_meth == 0) THEN !assumes that diagonal term is correct and there is no coupling
     Uvec_bnd = Lkry_bnd_rhs/Lkry_bnd_diag(j_lev, nwlt_bnd, ne, meth)
  ELSE
     CALL BiCGSTAB (j_lev, nwlt_bnd, ne, 1000, meth, clip, tol1, Uvec_bnd, Lkry_bnd_rhs, Lkry_bnd, Lkry_bnd_diag, SCL=scl_global(1:ne)) 
  END IF
  DO ie = 1, ne
     shift_bnd = nwlt_bnd*(ie-1)
     shift = nwlt*(ie-1)
     Uvec(nwlt_intrnl+1:nwlt,ie)  = Uvec(nwlt_intrnl+1:nwlt,ie) + Uvec_bnd(shift_bnd+1:shift_bnd+nwlt_bnd) !add incriment
  END DO
  
END SUBROUTINE project_BC

FUNCTION Lkry_bnd (jlev, Uvec, nlocal_bnd, ne_local, meth)
  IMPLICIT NONE
  INTEGER, INTENT (IN) :: jlev, nlocal_bnd, ne_local, meth
  REAL (pr), DIMENSION (nlocal_bnd*ne_local), INTENT(INOUT) :: Uvec
  REAL (pr), DIMENSION (nlocal_bnd*ne_local) :: Lkry_bnd
  REAL (pr), DIMENSION (nwlt*ne_local) :: Uvec_aux, Lkry_aux
  INTEGER :: nlocal_intrnl, ie, shift, shift_bnd

  ! jlev is assumed to be j_lev
  Uvec_aux = 0.0_pr
  DO ie = 1, ne_local
     shift_bnd = nlocal_bnd*(ie-1)
     shift = nwlt*(ie-1)
     Uvec_aux(shift+nwlt_intrnl+1:shift+nwlt) = Uvec(shift_bnd+1:shift_bnd+nlocal_bnd)
     Lkry_aux(shift+nwlt_intrnl+1:shift+nwlt) = Uvec_aux(shift+nwlt_intrnl+1:shift+nwlt) 
  END DO
  !--Set operator on boundaries
  CALL user_algebraic_BC (Lkry_aux, Uvec_aux, nwlt, ne_local, j_lev, meth)
  DO ie = 1, ne_local
     shift_bnd = nlocal_bnd*(ie-1)
     shift = nwlt*(ie-1)
     Lkry_bnd(shift_bnd+1:shift_bnd+nlocal_bnd) = Lkry_aux(shift+nwlt_intrnl+1:shift+nwlt)
  END DO
  
END FUNCTION Lkry_bnd

FUNCTION Lkry_bnd_diag (jlev, nlocal_bnd, ne_local, meth)
  IMPLICIT NONE
  INTEGER, INTENT (IN) :: jlev, nlocal_bnd, ne_local, meth
  REAL (pr), DIMENSION (nlocal_bnd*ne_local) :: Lkry_bnd_diag
  REAL (pr), DIMENSION (nwlt*ne_local) :: Lkry_diag_aux
  INTEGER :: nlocal_intrnl, ie, shift, shift_bnd
  
  ! jlev is assumed to be j_lev
  DO ie = 1, ne_local
     shift = nwlt*(ie-1)
     Lkry_diag_aux(shift+nwlt_intrnl+1:shift+nwlt) = 1.0_pr
  END DO
  !--Set operator on boundaries
  CALL user_algebraic_BC_diag (Lkry_diag_aux, nwlt, ne_local, j_lev, meth)
  DO ie = 1, ne_local
     shift_bnd = nlocal_bnd*(ie-1)
     shift = nwlt*(ie-1)
     Lkry_bnd_diag(shift_bnd+1:shift_bnd+nlocal_bnd) = Lkry_diag_aux(shift+nwlt_intrnl+1:shift+nwlt)
  END DO
  
END FUNCTION Lkry_bnd_diag


!--********************************
!--End Krylov time step subroutines
!--********************************

END MODULE kry_mod
