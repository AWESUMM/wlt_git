! --------------------------------------------------------------------
!      Tree database (fortran version of db_tree)
!
!      These global functions are to be used
!      as a substitution for C++ tree database:
!      C++/[interface,tree,tree1,amr].[cxx,h]
!
!      Alex Vezolainen
! --------------------------------------------------------------------
MODULE tree_database_f
  USE precision
  USE db_tree_vars      ! pointer_pr, node, node_ptr, etc
  IMPLICIT NONE
  
  !|=================================================================
  !|
  !|    PUBLIC STRUCTURE OF THE DATABASE
  !|
  !|=================================================================
  PUBLIC :: db_declare_globals, &        ! initialize the database
       db_finalize, &                    ! destroy the database
       db_add_node, &                      ! add the node
       db_test_node, &                     ! test the node
       db_get_coordinates_by_pointer, &  ! get vector of coordinates
       db_get_id_by_pointer, &           ! get ID
       db_set_id_by_pointer, &           ! set ID
       db_get_function_by_pointer, &               !
       db_set_function_by_pointer, &               !
       db_get_ifunction_by_pointer, &              !
       db_set_ifunction_by_pointer, &              !
       db_get_function_by_jmax_coordinates, &      !
       db_get_initial_type_level_node, & !
       db_get_next_type_level_node, &    !
       db_update_db, &                       !
       db_update_type_level, &               !
       db_move_zero_sig_to_gho, &            !
       db_test_if_in_sig_list_by_pointer, &  !
       db_amr_set_variable_name, &             !--------------------------!
       db_amr_set_coordinates, &               !                          !
       db_write_amr_comment, &                 !                          !
       db_amr_vtk_write, &                     !   AMR related functions  !
       db_amr_ensght_write, &                  !                          !
       db_amr_plot3d_write, &                  !                          !
       db_amr_cgns_write, &                    !--------------------------!
       db_interpolate, &   ! old C++ db_tree style interpolation
       db_print_info               ! debug


  
  !|=================================================================
  !|
  !|    PRIVATE STRUCTURE OF THE DATABASE
  !|
  !|=================================================================
  !
  ! Clean ID values: ghost, significant, additional, etc.
  ! IDs also can be mixed, e.g. significant+adjacent
  ! ID is intermediate for the nodes which are not in link lists
  ! and used only for mantaining the tree structure
  !
  INTEGER, PRIVATE, PARAMETER :: pos_significant_id = 0        ! 0001
  INTEGER, PRIVATE, PARAMETER :: pos_adjacent_id = 1           ! 0010
  INTEGER, PRIVATE, PARAMETER :: pos_ghost_id = 2              ! 0100
  INTEGER, PRIVATE, PARAMETER :: pos_old_id = 3                ! 1000
  
  INTEGER, PRIVATE, PARAMETER :: may_be_deleted_id = 0
  INTEGER, PRIVATE, PARAMETER :: old_id = 8                    ! 2**pos_old_id
  INTEGER, PRIVATE, PARAMETER :: intermediate_id = 64
  !
  ! Global variables
  !
  INTEGER, PRIVATE :: DIM_NUM                       ! dimension of the domain
  INTEGER, PRIVATE :: VAL_NUM                       ! number of real function values
  INTEGER, PRIVATE :: VAL_NUM_I                     ! number of integer function values
  INTEGER, PRIVATE :: J_MAX                         ! maximum level
  INTEGER, PRIVATE :: J_TREE                        ! from that level trees are growing
  INTEGER, PRIVATE :: LEAVES                        ! number of leaves of -1 level, 2**dim-1
  INTEGER, PRIVATE :: TYPES_NUM                     ! number of types (4,8,etc), 2**dim

#if (TREE_VERSION == 2)
  INTEGER, PRIVATE, PARAMETER :: DIM_MAX = 4        ! maximum dimension
  INTEGER, PRIVATE :: FINEST_MESH (DIM_MAX)         ! m_x * 2^{J_max-1}
  INTEGER, PRIVATE :: PERIODIC (DIM_MAX)            ! periodic boundary conditions marker
#else
  INTEGER, PRIVATE :: FINEST_MESH (max_dim)         ! m_x * 2^{J_max-1}
  INTEGER, PRIVATE :: PERIODIC (max_dim)            ! periodic boundary conditions marker
#endif
  !
  ! Forest internal variables
  !
  TYPE, PRIVATE :: type_level                            ! Type-level information lists
     TYPE (node), POINTER :: tlp_begin                   ! ...
     TYPE (node), POINTER :: tlp_end                     ! ...
     INTEGER :: tlp_number_of_elements                   ! ...
  END TYPE type_level                                    ! ...
  TYPE(type_level), PRIVATE, POINTER :: f_sig(:,:,:)     ! ( face_type,  wlt_type,   level  )
  TYPE(type_level), PRIVATE, POINTER :: f_gho(:,:,:)     ! ( 0:3**dim-1, 0:2**dim-1, 1:jmax )
  INTEGER, PRIVATE :: current_list_marker                ! ... 1(f_sig), 2(f_gho), 3(all)
  INTEGER, PRIVATE :: current_list_processed             ! ... 1(f_sig), 2(f_gho)
  TYPE(node), POINTER, PRIVATE :: current_list_node      ! ...
  INTEGER, PRIVATE :: clt, clft, cll                     ! ... current t, ft, l
  
  INTEGER, PRIVATE :: x_space                       ! coarse grid spacing
  INTEGER, PRIVATE :: number_of_trees               ! number of trees

#if (TREE_VERSION == 2)
  INTEGER, PRIVATE :: gopt1_factor (DIM_MAX)
#else
  INTEGER, PRIVATE :: gopt1_factor (max_dim)
#endif

  !
  ! Control parameters
  !
!!$  LOGICAL, PRIVATE :: verb = .TRUE.                 ! verbose Forest initialization  
  LOGICAL, PRIVATE :: verb = .FALSE.                 ! verbose Forest initialization  
  !
  ! Tree class
  !
  TYPE, PRIVATE ::tree
     TYPE (node) :: root
  END TYPE tree
  TYPE(tree), PRIVATE, POINTER :: trees(:)   ! array of trees
  !
  ! Private functions
  !
  PRIVATE :: create_tree_root, &
       tlp_node_include_b, &
       tlp_node_include_f, &
       tlp_node_exclude, &
       rearrange_leaves, &
       found_nonzero_child_link, &
       delete_node, &
       delete_root
CONTAINS
  
  !-----------------------------------------------------------------
  !|----------------------------------------------------------------
  !|
  !| PUBLIC STRUCTURE OF THE DATABASE
  !|
  !|----------------------------------------------------------------
  !-----------------------------------------------------------------
  
  SUBROUTINE db_amr_set_variable_name (var_number, var_name, var_name_len)
    INTEGER, INTENT(IN) :: var_number
    INTEGER, INTENT(IN) :: var_name_len
    CHARACTER*(*), INTENT(IN) :: var_name
    
    PRINT *, 'not ready for that database'
    STOP 'db_tree_f.90: db_amr_set_variable_name'
  END SUBROUTINE db_amr_set_variable_name
  SUBROUTINE db_write_amr_comment (input_file_name, input_file_name_len)
    INTEGER, INTENT(IN) :: input_file_name_len
    CHARACTER*(*), INTENT(IN) :: input_file_name
    
    PRINT *, 'not ready for that database'
    STOP 'db_tree_f.90: db_write_amr_comment'
  END SUBROUTINE db_write_amr_comment
  SUBROUTINE db_amr_set_coordinates (int_marker, coord_len, coord_dim, coord)
    INTEGER, INTENT(IN) :: int_marker           ! 1 for integer, 0 for real coordinates
    INTEGER, INTENT(IN) :: coord_len            ! length of coordinate array
    INTEGER, INTENT(IN) :: coord_dim            ! dimension of the current coordinate
    REAL(pr), INTENT(IN) :: coord(0:coord_len)  ! <--> xx(0:nxyz(dim),1:dim)
    
    PRINT *, 'not ready for that database'
    STOP 'db_tree_f.90: db_amr_set_coordinates'
  END SUBROUTINE db_amr_set_coordinates
  SUBROUTINE db_amr_vtk_write (filename, filename_len, asci_or_binary, make_tetrahedra)
    INTEGER, INTENT(IN) :: filename_len
    CHARACTER*(*), INTENT(IN) :: filename
    CHARACTER, INTENT(IN) :: asci_or_binary  ! 'a' for ASCII, 'b' for binary
    INTEGER, INTENT(IN) :: make_tetrahedra   ! 1 for tetrahedra, 0 for blocks
    
    PRINT *, 'not ready for that database'
    STOP 'db_tree_f.90: db_amr_vtk_write'
  END SUBROUTINE db_amr_vtk_write
  SUBROUTINE db_amr_ensght_write (filename, filename_len, asci_or_binary, make_tetrahedra)
    INTEGER, INTENT(IN) :: filename_len
    CHARACTER*(*), INTENT(IN) :: filename
    CHARACTER, INTENT(IN) :: asci_or_binary  ! 'a' for ASCII, 'b' for binary
    INTEGER, INTENT(IN) :: make_tetrahedra   ! 1 for tetrahedra, 0 for blocks
    
    PRINT *, 'not ready for that database'
    STOP 'db_tree_f.90: db_amr_ensght_write'
  END SUBROUTINE db_amr_ensght_write
  SUBROUTINE db_amr_plot3d_write (filename, filename_len, asci_or_binary, make_tetrahedra)
    INTEGER, INTENT(IN) :: filename_len
    CHARACTER*(*), INTENT(IN) :: filename
    CHARACTER, INTENT(IN) :: asci_or_binary  ! 'a' for ASCII, 'b' for binary
    INTEGER, INTENT(IN) :: make_tetrahedra   ! 1 for tetrahedra, 0 for blocks
    
    PRINT *, 'not ready for that database'
    STOP 'db_tree_f.90: db_amr_plot3d_write'
  END SUBROUTINE db_amr_plot3d_write
  SUBROUTINE db_amr_cgns_write (filename, filename_len, asci_or_binary, make_tetrahedra)
    INTEGER, INTENT(IN) :: filename_len
    CHARACTER*(*), INTENT(IN) :: filename
    CHARACTER, INTENT(IN) :: asci_or_binary  ! 'a' for ASCII, 'b' for binary
    INTEGER, INTENT(IN) :: make_tetrahedra   ! 1 for tetrahedra, 0 for blocks
    
    PRINT *, 'not ready for that database'
    STOP 'db_tree_f.90: db_amr_cgns_write'
  END SUBROUTINE db_amr_cgns_write


  !---------------------------------------------------------------------
  SUBROUTINE db_interpolate (u, nwlt, n_var, du, avm, &
       x, f, x_dim, x_size, vars, vars_size, mode, order, &
       vtr, vtn, vtl)
    ! Interpolate function values to the given locations
    ! (old C++ interface)
    INTEGER, INTENT(IN) ::  nwlt       !- number of wavelets
    INTEGER, INTENT(IN) ::  n_var      !- number of variables
    INTEGER, INTENT(IN) ::  x_dim      !- dimension of x array
    INTEGER, INTENT(IN) ::  x_size     !- length of x and f arrays
    INTEGER, INTENT(IN) ::  vars_size  !- length of the array of numbers of variables, first size of f array
    REAL(pr), INTENT(IN) ::   u(1:nwlt, 1:n_var)          !- function (use that array if avm==1)
    REAL(pr), INTENT(IN) ::   du(1:n_var,1:nwlt,1:x_dim)  !- derivatives
    REAL(pr), INTENT(IN) ::   x(1:x_dim,1:x_size)         !- array of points to interpolate into
    REAL(pr), INTENT(OUT) ::  f(1:vars_size,1:x_size)     !- returned array of interpolated function values
    INTEGER, INTENT(IN) ::    vars(1:vars_size)           !- array of numbers of variables to interpolate for
    INTEGER, INTENT(IN) :: avm   !- 0-functions are stored in DB, 1-iwlt is stored in DB as integer
    INTEGER, INTENT(IN) :: mode  !- 0 - direct interpolation, good for small arrays of x ( general approach )
    !...                         !- 1 - use preprocessing, good for large arrays ( uniform coordinates only )
    !...                         !- 2 - use preprocessing, good for large arrays ( general approach )
    !...                         !- 3 .res -> .ren for the volume renderer for the variable # VARIABLE_TO_RENDER  
    INTEGER, INTENT(IN) :: order !- order of interpolation (0-linear, 1-3rd order)
    INTEGER, INTENT(IN) ::       vtr       !- variable # to put into .ren file for the volume renderer
    CHARACTER*(*), INTENT(IN) :: vtn       !- name of the .ren file
    INTEGER, INTENT(IN) ::       vtl       !- length of the .ren file
    
    PRINT *, 'ERROR: not ready for that database'
    PRINT *, 'consider using C++ db_tree DB_INTERPOLATE or database independent INTERPOLATE instead'
    STOP 'db_tree_f.90: db_interpolate()'
    
    IF (mode.EQ.0) THEN                       ! direct interpolation
    ELSEIF ((mode.EQ.1).OR.(mode.EQ.2)) THEN  ! preprocess
    ELSEIF (mode.EQ.3) THEN                   ! rendering
    ELSE
       PRINT *, 'ERROR: wrong interpolation mode', mode
       PRINT *, 'use 0,1,2 for direct or preprocessed (uniform or nonuniform)'
       STOP 'in db_interpolate'
    END IF
    f = 0.0_pr ! to avoid compiler warning
    
  END SUBROUTINE db_interpolate


  !---------------------------------------------------------------------
  SUBROUTINE db_print_info
    !  DEBUG
    !  print tree data
    INTEGER :: j, wlt_type, face_type, dim, j_mx, list, count, ii
    !TYPE(node_ptr) :: c_pointer
    TYPE(node), POINTER :: c_pointer
    
    NULLIFY (c_pointer)
    dim = DIM_NUM
    j_mx = J_MAX
    
    DO list = 1,3
       PRINT *, 'list=', list
       DO j = 1, j_mx
          DO wlt_type = MIN(j-1,1),2**dim - 1
             DO face_type = 0, 3**dim - 1
                CALL DB_get_initial_type_level_node (wlt_type,j,face_type,ii,c_pointer,list)
                IF(ii > 0) THEN
                   
                   WRITE (*,'("(ft,t,l, ii) = ",3(I2,X),", #=",I3)') face_type, wlt_type, j, ii
                   count = 0
                   
                   DO WHILE(ASSOCIATED(c_pointer))  ! Fortran's pointer
                      PRINT *, 'ii=',c_pointer%ii,'id=',c_pointer%id
                      count = count + 1
                      CALL DB_get_next_type_level_node (c_pointer)
                   END DO

                   IF (count.NE.ii) THEN
                      PRINT *, 'ERROR----------------- count=',count
                      !STOP
                   END IF
                   
                END IF
             END DO
          END DO
       END DO
    END DO

  END SUBROUTINE db_print_info
  
  
  !-----------------------------------------------------------------
  SUBROUTINE db_finalize
    !  Completely clean the memory occupied by the database
    INTEGER :: depth                                            ! max path length
    TYPE(node_ptr) :: path_node (0:J_MAX - J_TREE)              ! current path
    INTEGER :: path_cindex (0:J_MAX - J_TREE)                   ! current child index
    INTEGER :: i, si, main_index, ci, ilow, totc
    TYPE(node), POINTER :: ptr

    depth = J_MAX - J_TREE
    path_cindex(:) = 0
    DO i=0,depth
       NULLIFY(path_node(i)%ptr)
    END DO
    
    DO si=1,number_of_trees
       
       main_index = 0                                        ! to which position path_...[] is filled
       path_node(0)%ptr => trees(si)%root                    ! initialize path's beginning
       path_cindex(0) = 1                                    ! ...
       ptr => path_node(0)%ptr
       
       DO                                               ! the main cycle ===============================
          ilow = path_cindex(main_index)
          totc = 0
          
          IF (found_nonzero_child_link (ptr,ci,ilow,totc)) THEN
             !PRINT *, 'found nonzero child for',ptr%ii
             main_index  = main_index + 1
             path_node (main_index)%ptr => ptr%leaves (ci)%ptr
             path_cindex (main_index-1) = ci + 1
             path_cindex (main_index)   = 1
             ptr => ptr%leaves (ci)%ptr
          ELSE
             IF (totc .EQ. 0) THEN
                IF (ptr%level.NE.0) THEN
                   !PRINT *,'detete_node',ptr%ii
                   CALL delete_node (ptr)
                   NULLIFY(path_node (main_index-1)%ptr%leaves (path_cindex(main_index-1)-1)%ptr)
                ELSE
                   !PRINT *,'detete_root',ptr%ii
                   CALL delete_root (ptr)
                END IF
             END IF
             
             main_index = main_index - 1
             IF (main_index.LT.0) EXIT
             
             NULLIFY(path_node(main_index+1)%ptr)
             path_cindex (main_index+1) = 1
             ptr => path_node (main_index)%ptr
             
          END IF
       END DO                                           ! the main cycle ===============================
       
       DO i=1,depth                                             ! clear the path
          NULLIFY(path_node(i)%ptr)                             ! ...
       END DO                                                   ! ...
       path_cindex(:) = 0                                       ! ...
    END DO

    DEALLOCATE (trees)
    DEALLOCATE (f_sig)
    DEALLOCATE (f_gho)
    
  END SUBROUTINE db_finalize

  !---------------------------------------------------------------------
  SUBROUTINE db_test_if_in_sig_list_by_pointer ( ptr, out )
    !  Return 1 if node is in the list of significant/adjacent points,
    !  return 0 otherwise. This function to be used from db_tree
    !  indices subroutine to order wavelets similarly to db_wrk version.
    TYPE(node), POINTER :: ptr                              ! pointer to the node
    INTEGER, INTENT(OUT) :: out                             ! the returned output
    
    out = 0
    IF ( ptr%sig.EQ.1 ) out = 1
    
  END SUBROUTINE db_test_if_in_sig_list_by_pointer
  
  !-----------------------------------------------------------------
  SUBROUTINE db_update_db
    ! Depth first tree search
    ! Delete, if possible, the nodes marked by ID==to_be_deleted,
    ! or ID==intermediate_id, or ID=old_id
    ! Deleted tree roots will be marked as intermediate and deleted from link lists
    INTEGER :: depth                                            ! max path length
    TYPE(node_ptr) :: path_node (0:J_MAX - J_TREE)              ! current path
    INTEGER :: path_cindex (0:J_MAX - J_TREE)                   ! current child index
    INTEGER :: i, si, main_index, ci, ilow, totc
    TYPE(node), POINTER :: ptr
    
    !PRINT *, 'db_update_db'!; STOP
    
    depth = J_MAX - J_TREE
    path_cindex(:) = 0
    DO i=0,depth
       NULLIFY(path_node(i)%ptr)
    END DO
    
    DO si=1,number_of_trees
       IF ((trees(si)%root%id.EQ.may_be_deleted_id).OR. &    ! treat tree roots
            (trees(si)%root%id.EQ.intermediate_id).OR. &
            (trees(si)%root%id.EQ.old_id)) THEN
          ptr => trees(si)%root
          CALL delete_root (ptr)
       END IF

       main_index = 0                                        ! to which position path_...[] is filled
       path_node(0)%ptr => trees(si)%root                    ! initialize path's beginning
       path_cindex(0) = 1                                    ! ...
       ptr => path_node(0)%ptr
       
       DO                                               ! the main cycle ===============================
          ilow = path_cindex(main_index)
          totc = 0
          
          IF (found_nonzero_child_link (ptr,ci,ilow,totc)) THEN
             main_index  = main_index + 1
             path_node (main_index)%ptr => ptr%leaves (ci)%ptr
             path_cindex (main_index-1) = ci + 1
             path_cindex (main_index)   = 1
             ptr => ptr%leaves (ci)%ptr
          ELSE
             IF ((totc.EQ.0).AND. &
                  ( (ptr%id.EQ.may_be_deleted_id).OR. &
                  (ptr%id.EQ.intermediate_id).OR.   &
                  (ptr%id.EQ.old_id) )) THEN
                IF (ptr%level.NE.0) THEN
                   CALL delete_node (ptr)
                   NULLIFY(path_node (main_index-1)%ptr%leaves (path_cindex(main_index-1)-1)%ptr)
                ELSE
                   PRINT *,'it looks like some logical error had happened in db_update_db()'
                   STOP
                   CALL delete_root (ptr)
                END IF
             END IF

             main_index = main_index - 1
             IF (main_index.LT.0) EXIT
             
             NULLIFY(path_node(main_index+1)%ptr)
             path_cindex (main_index+1) = 1
             ptr => path_node (main_index)%ptr

          END IF
       END DO                                           ! the main cycle ===============================
       
       DO i=1,depth                                             ! clear the path
          NULLIFY(path_node(i)%ptr)                             ! ...
       END DO                                                   ! ...
       path_cindex(:) = 0                                       ! ...
    END DO
    
  END SUBROUTINE db_update_db

  !-----------------------------------------------------------------
  SUBROUTINE db_update_type_level
    ! Redistribute the nodes in type-level lists
    ! to make sure that all "significant" and "adjacent" are in one list
    ! and all "ghost" are in another
    ! The nodes which are none of the above will be deleted from the lists.
    INTEGER :: t, ft, l
    TYPE(node), POINTER :: current_in_sig_list, current_in_gho_list, next_current
    
    !PRINT *, 'db_update_type_level'!; STOP

    DO l=1,J_MAX
       DO t=0,2**DIM_NUM-1
          DO ft=0,3**DIM_NUM-1
             current_in_sig_list => f_sig(ft,t,l)%tlp_begin    ! first elements before any amendments
             current_in_gho_list => f_gho(ft,t,l)%tlp_begin
             
             DO WHILE (ASSOCIATED(current_in_sig_list))            ! (1) sweep through sig_adj list
                next_current => current_in_sig_list%next_tlp
                IF (BTEST(current_in_sig_list%id, pos_adjacent_id).OR.&
                     BTEST(current_in_sig_list%id, pos_significant_id)) THEN
                   ! do nothing
                ELSE
                   ! exclude it from the list
                   CALL tlp_node_exclude (f_sig(ft,t,l),current_in_sig_list)
                   current_in_sig_list%sig = 0
                   ! put the node into gho list
                   IF (BTEST(current_in_sig_list%id,pos_ghost_id)) THEN
                      CALL tlp_node_include_f (f_gho(ft,t,l),current_in_sig_list)
                      current_in_sig_list%sig = 2
                   END IF
                END IF
                current_in_sig_list => next_current
             END DO
      
             DO WHILE (ASSOCIATED(current_in_gho_list))            ! (2) sweep through gho list
                next_current => current_in_gho_list%next_tlp
                IF (BTEST(current_in_gho_list%id, pos_adjacent_id).OR.&
                     BTEST(current_in_gho_list%id, pos_significant_id)) THEN
                   ! put it into sig_adj list
                   CALL tlp_node_exclude (f_gho(ft,t,l),current_in_gho_list)
                   CALL tlp_node_include_b (f_sig(ft,t,l),current_in_gho_list)
                   current_in_gho_list%sig = 1
                ELSE
                   IF (BTEST(current_in_gho_list%id, pos_ghost_id)) THEN
                      ! do nothing
                   ELSE 
                      ! exclude it from the lists
                      CALL tlp_node_exclude (f_gho(ft,t,l),current_in_gho_list)
                      current_in_gho_list%sig = 0
                   END IF
                END IF
                current_in_gho_list => next_current
             END DO

          END DO
       END DO
    END DO

  END SUBROUTINE db_update_type_level

  !-----------------------------------------------------------------
  SUBROUTINE db_move_zero_sig_to_gho
    ! move nodes with zero or old ID from sig/adj to ghost type-level list
    INTEGER :: t, ft, l
    TYPE(node), POINTER :: current_in_sig_list, next_current

    DO l=1,J_MAX
       DO t=0,2**DIM_NUM-1
          DO ft=0,3**DIM_NUM-1
             current_in_sig_list => f_sig(ft,t,l)%tlp_begin        ! first before any amendments
             DO WHILE (ASSOCIATED(current_in_sig_list))                ! sweep through sig_adj list
                next_current => current_in_sig_list%next_tlp
                
                IF ( (current_in_sig_list%id.EQ.may_be_deleted_id).OR. &     ! put the node
                     (current_in_sig_list%id.EQ.old_id) ) THEN               ! into gho list
                   CALL tlp_node_exclude (f_sig(ft,t,l),current_in_sig_list)
                   CALL tlp_node_include_b (f_gho(ft,t,l),current_in_sig_list)
                   current_in_sig_list%sig = 2
                END IF
                current_in_sig_list => next_current
             END DO
          END DO
       END DO
    END DO

  END SUBROUTINE db_move_zero_sig_to_gho

  !-----------------------------------------------------------------
  SUBROUTINE db_get_initial_type_level_node ( t, l, ft, noe, ptr, marker )
    ! Access to the nodes of the given type and level
    INTEGER, INTENT(IN) :: marker         ! indicates which type-level list to access
    !                                       1 (significant and adjacent nodes only)
    !                                       2 (ghost nodes only)
    !                                       3 (significant, adjacent, and ghost)
    INTEGER, INTENT(IN) :: ft,t,l         ! type, level, facetype
    INTEGER, INTENT(OUT) :: noe           ! number of nodes in the list
    TYPE(node), POINTER :: ptr            ! the pointer to the first node

    current_list_marker = marker
    clt = t; clft = ft; cll = l;
!!$    PRINT *, 'db_get_initial_type_level_node'
!!$    PRINT *, 'current_list_marker=',current_list_marker
!!$    PRINT *, 'clt,clft,cll',clt,clft,cll
    
    list_type: SELECT CASE (marker)
    CASE (1)
       noe = f_sig (ft,t,l)%tlp_number_of_elements
       IF (noe.EQ.0) THEN
          NULLIFY (ptr)
       ELSE
          ptr => f_sig (ft,t,l)%tlp_begin
          current_list_node => f_sig (ft,t,l)%tlp_begin
          current_list_processed = 1
       END IF

    CASE (2)
       noe = f_gho (ft,t,l)%tlp_number_of_elements
       IF (noe.EQ.0) THEN
          NULLIFY (ptr)
       ELSE
          ptr => f_gho (ft,t,l)%tlp_begin
          current_list_node => f_gho (ft,t,l)%tlp_begin
          current_list_processed = 2
       END IF
       
    CASE (3)
       noe = f_sig (ft,t,l)%tlp_number_of_elements + &
            f_gho (ft,t,l)%tlp_number_of_elements
       IF (noe.EQ.0) THEN
          NULLIFY (ptr)
       ELSE ! noe>0
          IF (f_sig (ft,t,l)%tlp_number_of_elements.EQ.0) THEN
             ptr => f_gho (ft,t,l)%tlp_begin
             current_list_node => f_gho (ft,t,l)%tlp_begin
             current_list_processed = 2
          ELSE
             ptr => f_sig (ft,t,l)%tlp_begin
             current_list_node => f_sig (ft,t,l)%tlp_begin
             current_list_processed = 1
          END IF
       END IF
    END SELECT list_type
    
  END SUBROUTINE db_get_initial_type_level_node

  !-----------------------------------------------------------------
  SUBROUTINE db_get_next_type_level_node ( ptr )
    ! Access to the nodes of the given type and level
    ! It is assumed that get_initial_... has been called first
    TYPE(node), POINTER :: ptr
    ! Returns the pointer to the next node from the list
    ! of the nodes of given type and level, or NULL for the last element
    
    IF (ASSOCIATED(current_list_node%next_tlp)) THEN   ! normal flow
       ptr => current_list_node%next_tlp            ! return result
       current_list_node => ptr
    ELSE                                               ! end of a list
       IF (current_list_marker.NE.3) THEN                ! real end of the current list
          NULLIFY (ptr)
       ELSE                                              ! may need to look at another one
          IF (current_list_processed.EQ.2) THEN            ! real end of gho list
             NULLIFY (ptr)
          ELSE                                             ! end of sig_adj list
             IF (f_gho (clft,clt,cll)%tlp_number_of_elements.EQ.0) THEN ! the real end
                NULLIFY (ptr)
             ELSE                                                       ! jump to gho list
                ptr => f_gho (clft,clt,cll)%tlp_begin
                current_list_node => ptr
                current_list_processed = 2
             END IF
          END IF
       END IF
    END IF

  END SUBROUTINE db_get_next_type_level_node

  !-----------------------------------------------------------------
  SUBROUTINE db_set_function_by_pointer ( ptr, ilow, ihigh, u )
    !  Set values for the part [ilow;ihigh]
    !  of the vector of function values [1:VAL_NUM]
    TYPE(node), POINTER :: ptr                  ! pointer to the node
    INTEGER, INTENT(IN) :: ilow                 ! starting index for the output vector
    INTEGER, INTENT(IN) :: ihigh                ! ending index
    REAL(pr), INTENT(IN) :: u(ilow:ihigh)       ! input vector of function values
    
    ptr%val(ilow:ihigh) = u(ilow:ihigh)
    
  END SUBROUTINE db_set_function_by_pointer
  
  !-----------------------------------------------------------------
  SUBROUTINE db_set_ifunction_by_pointer ( ptr, ilow, ihigh, iu )
    !  Set values for the part [ilow;ihigh]
    !  of the vector of function values [1:VAL_NUM_I]
    TYPE(node), POINTER :: ptr                  ! pointer to the node
    INTEGER, INTENT(IN) :: ilow                 ! starting index for the output vector
    INTEGER, INTENT(IN) :: ihigh                ! ending index
    INTEGER, INTENT(IN) :: iu(ilow:ihigh)       ! input vector of function values
    
    ptr%ival(ilow:ihigh) = iu(ilow:ihigh)
    
  END SUBROUTINE db_set_ifunction_by_pointer

  !-----------------------------------------------------------------
  SUBROUTINE db_get_function_by_pointer ( ptr, ilow, ihigh, u )
    !  Get part [ilow:ihigh] of the vector of function values [1:VAL_NUM] for the node
    !  NOT SAFE, zero pointer causes error
    TYPE(node), POINTER :: ptr                  ! pointer to the node
    INTEGER, INTENT(IN) :: ilow                 ! starting index for the output vector
    INTEGER, INTENT(IN) :: ihigh                ! ending index
    REAL(pr), INTENT(OUT) :: u(ilow:ihigh)      ! output vector of function values
    
    u(ilow:ihigh) = ptr%val(ilow:ihigh)
    
  END SUBROUTINE db_get_function_by_pointer
  
  !-----------------------------------------------------------------
  SUBROUTINE db_get_function_by_jmax_coordinates ( coord, ilow, ihigh, u )
    !  Get part [ilow:ihigh] of the vector of function values [1:VAL_NUM] for the node
    !  SAFE, if ptr==0, return zeroes in the vector of function values
    INTEGER, INTENT(IN) :: coord(DIM_NUM)       ! coordinates, in the units corresponding to J_MAX
    INTEGER, INTENT(IN) :: ilow                 ! starting index for the output vector
    INTEGER, INTENT(IN) :: ihigh                ! ending index
    REAL(pr), INTENT(OUT) :: u(ilow:ihigh)      ! output vector of function values
    TYPE(node), POINTER :: ptr                  ! pointer to the node
    
    CALL db_test_node(coord,J_MAX,ptr)
    IF (ASSOCIATED(ptr)) THEN
       u(ilow:ihigh) = ptr%val(ilow:ihigh)
    ELSE
       u(ilow:ihigh) = 0.0_pr
    END IF
    
  END SUBROUTINE db_get_function_by_jmax_coordinates
  
  !-----------------------------------------------------------------
  SUBROUTINE db_get_ifunction_by_pointer ( ptr, ilow, ihigh, iu )
    !  Get part of the vector of function values [1:VAL_NUM_I] for the node
    !  If ptr==0, return zeroes in the vector of function values
    TYPE(node), POINTER :: ptr                  ! pointer to the node
    INTEGER, INTENT(IN) :: ilow                 ! starting index for the output vector
    INTEGER, INTENT(IN) :: ihigh                ! ending index
    INTEGER, INTENT(OUT) :: iu(ilow:ihigh)      ! output vector of function values
    
    IF (ASSOCIATED(ptr)) THEN
       iu(ilow:ihigh) = ptr%ival(ilow:ihigh)
    ELSE
       iu(ilow:ihigh) = 0
    END IF
    
  END SUBROUTINE db_get_ifunction_by_pointer

  !-----------------------------------------------------------------
  SUBROUTINE db_set_id_by_pointer ( ptr, id )
    !  Set node ID
    !  If the new ID corresponds to sig/adj list then move the node into sig/adj list.
    TYPE(node), POINTER :: ptr
    INTEGER, INTENT(IN) :: id
    INTEGER :: ft,t,l, id_tmp

    id_tmp = id

    IF (BTEST(id_tmp,pos_adjacent_id).OR.BTEST(id_tmp,pos_significant_id)) THEN ! if ID is changed to sig/adj
       CALL set_type_level (ptr, ft, t, l)
       IF (ptr%sig.NE.1) THEN
          IF (ptr%sig.EQ.0) THEN
             NULLIFY (ptr%prev_tlp, ptr%next_tlp)
          ELSE
             CALL tlp_node_exclude (f_gho(ft,t,l), ptr)
          END IF
          CALL tlp_node_include_b (f_sig(ft,t,l), ptr)
          ptr%sig = 1
       END IF
    END IF

    ptr%id = id_tmp

  END SUBROUTINE db_set_id_by_pointer

  !-----------------------------------------------------------------
  SUBROUTINE db_get_id_by_pointer ( ptr, id )
    !  Get node ID
    TYPE(node), POINTER :: ptr
    INTEGER, INTENT(OUT) :: id
    
    id = ptr%id

  END SUBROUTINE db_get_id_by_pointer

  !-----------------------------------------------------------------
  SUBROUTINE db_get_coordinates_by_pointer ( ptr, coord )
    !  Get vector of coordinates
    TYPE(node), POINTER :: ptr
    INTEGER, INTENT(OUT) :: coord(DIM_NUM) ! coordinates, in the units of J_MAX

    coord(1:DIM_NUM) = ptr%ii(1:DIM_NUM)
    
  END SUBROUTINE db_get_coordinates_by_pointer

  !-----------------------------------------------------------------
  SUBROUTINE db_test_node ( coord, level, ptr )
    !  Test the presence of a node
    !  Returns pointer to the existing node, or NULL
    INTEGER, INTENT(IN) :: coord(DIM_NUM)    ! coordinates, in the units corresponding to node's level
    INTEGER, INTENT(IN) :: level             ! the level
    TYPE(node), POINTER :: ptr               ! pointer to the node, or NULL if error
    INTEGER :: index_on_finest_mesh(DIM_NUM), &
         index_on_coarse_mesh(DIM_NUM), &
         index_in_tree_array, i, factor, &
         opt_rel_coord (DIM_NUM), &
         summ_bin, summ, current_size, leaf_index
    
    TYPE(node), POINTER :: current_root, new_child

    DO i=1,DIM_NUM                                                  ! coordinates at the finest level
       index_on_finest_mesh(i) = &
            (1 - PERIODIC(i)) * ( coord(i)*2**(J_MAX-level) ) + &
            PERIODIC(i) * IAND((coord(i)*2**(J_MAX-level)),(FINEST_MESH(i)-1))
    END DO
    
    index_on_coarse_mesh(:) = index_on_finest_mesh(:) / x_space     ! index in the trees(:) array
    index_in_tree_array = 1
    factor = 1
    DO i=1,DIM_NUM
       index_in_tree_array  = index_in_tree_array + factor * index_on_coarse_mesh(i)
       factor  = factor * (FINEST_MESH(i)/x_space + 1 - PERIODIC(i))
    END DO
    
    !IF (verb) WRITE (*,'(A,'//CHAR(ICHAR('0')+DIM_NUM)//'I4,A,I4)') &
    !     'testing the node: (',index_on_finest_mesh,') from the tree #',index_in_tree_array
    
    opt_rel_coord(:) = index_on_finest_mesh(:) - trees(index_in_tree_array)%root%ii(:)
    summ = 0
    DO i=1,DIM_NUM
       summ = IOR(summ,opt_rel_coord(i))
    END DO
    IF (summ.EQ.0) THEN ! ----------------------------------------- ! it is the root
       IF (trees(index_in_tree_array)%root%id.EQ.intermediate_id) THEN
          NULLIFY (ptr)
       ELSE
          ptr => trees(index_in_tree_array)%root
       END IF
       RETURN
    END IF ! -------------------- end of tree root handling -------
    
    current_root => trees(index_in_tree_array)%root
    current_size = x_space/2
    leaf_index = 0                                                  ! leaf index, where the next root should be
    DO
       IF (current_root%num_leaves.EQ.0) THEN
          NULLIFY (ptr)
          RETURN
       END IF
       summ = 0
       summ_bin = 0
       DO i=1,DIM_NUM
          IF (IAND(opt_rel_coord(i),current_size).NE.0) summ = summ + 2**(i-1)
          opt_rel_coord(i) = IAND(opt_rel_coord(i),current_size-1)
          summ_bin = IOR(summ_bin,opt_rel_coord(i))
       END DO
       
       IF (summ.EQ.0) THEN
          leaf_index = leaf_index + LEAVES
          current_size = current_size/2
          CYCLE
       END IF
       leaf_index = leaf_index + summ

       IF (leaf_index.GT.current_root%num_leaves) THEN
          NULLIFY (ptr)
          RETURN
       END IF
       IF (summ_bin.EQ.0) THEN
          IF ((ASSOCIATED(current_root%leaves(leaf_index)%ptr)).AND. &
               (current_root%leaves(leaf_index)%ptr%id.NE.intermediate_id)) THEN
             ptr => current_root%leaves(leaf_index)%ptr
          ELSE
             NULLIFY (ptr)
          END IF
          RETURN
       END IF
       
       current_root => current_root%leaves(leaf_index)%ptr
       leaf_index = 0
       IF (.NOT.ASSOCIATED(current_root)) THEN
          NULLIFY (ptr)
          RETURN
       END IF
       current_size = current_size/2                                ! length of the step to the next root
    END DO

  END SUBROUTINE db_test_node

  !-----------------------------------------------------------------
  SUBROUTINE db_add_node ( coord, level, out, ptr, id_in, front )
    !  Add the node (or find a pointer to the existing node)
    !  set its ID to IOR(ID, id_in)
    !  and include into type-level link list system
    !  all the "significant" and "adjacent" nodes.
    INTEGER, INTENT(IN) :: coord(DIM_NUM) ! coordinates, in the units corresponding to node's level
    INTEGER, INTENT(IN) :: level          ! the level
    INTEGER, INTENT(IN) :: id_in          ! ID for the node created
    INTEGER, INTENT(IN) :: front          ! if 1 add to front of the list, 0-end
    INTEGER, INTENT(OUT) :: out   ! the number of nodes created, including all new intermediate nodes
    TYPE(node), POINTER :: ptr    ! pointer to the node created, or existing already, or NULL if error
    INTEGER :: i, factor, t, ft, l, &
         index_on_finest_mesh(DIM_NUM), &
         index_on_coarse_mesh(DIM_NUM), &
         index_in_tree_array, &
         opt_rel_coord (DIM_NUM), &
         summ_bin, &
         current_size, &
         nodes_created, &
         current_root_level, &
         leaf_index, &
         summ, &
         path (DIM_NUM)
    
    TYPE(node), POINTER :: current_root, new_child
    
    DO i=1,DIM_NUM                                                  ! coordinates at the finest level
       index_on_finest_mesh(i) = &
            (1 - PERIODIC(i)) * ( coord(i)*2**(J_MAX-level) ) + &
            PERIODIC(i) * IAND((coord(i)*2**(J_MAX-level)),(FINEST_MESH(i)-1))
    END DO

    index_on_coarse_mesh(:) = index_on_finest_mesh(:) / x_space     ! index in the trees(:) array
    index_in_tree_array = 1
    factor = 1
    DO i=1,DIM_NUM
       index_in_tree_array  = index_in_tree_array + factor * index_on_coarse_mesh(i)
       factor  = factor * (FINEST_MESH(i)/x_space + 1 - PERIODIC(i))
    END DO
    
    !IF (verb) WRITE (*,'(A,I1,A,'//CHAR(ICHAR('0')+DIM_NUM)//'I4,A,I4)') &
    !     'adding the node:',id_in,'(',index_on_finest_mesh,') to the tree #',index_in_tree_array

    opt_rel_coord(:) = index_on_finest_mesh(:) - trees(index_in_tree_array)%root%ii(:)
    summ_bin = 0
    DO i=1,DIM_NUM
       summ_bin = IOR(summ_bin,opt_rel_coord(i))
    END DO
    IF (summ_bin.EQ.0) THEN ! ---------------------- ! tree root, which could be
       ptr => trees(index_in_tree_array)%root        ! deleted like a simple node
       IF (ptr%id.EQ.intermediate_id) THEN           ! that root was deleted from all lists
          ptr%id = id_in
          out = 1
          CALL set_type_level (ptr, ft, t, l)
          IF (BTEST(id_in, pos_adjacent_id).OR.BTEST(id_in, pos_significant_id)) THEN ! sig_adj list
             IF (front) THEN
                CALL tlp_node_include_f (f_sig(ft,t,l), ptr)
             ELSE
                CALL tlp_node_include_b (f_sig(ft,t,l), ptr)
             END IF
             ptr%sig = 1
          ELSE                                                            ! gho list
             IF (front) THEN
                CALL tlp_node_include_f (f_gho(ft,t,l), ptr)
             ELSE
                CALL tlp_node_include_b (f_gho(ft,t,l), ptr)
             END IF
             ptr%sig = 2
          END IF
       ELSE                                            ! that root can be in some list
          ptr%id = IOR(ptr%id, id_in)
          out = 0
          IF ( (ptr%sig.NE.1).AND.&
               (BTEST(ptr%id, pos_adjacent_id).OR. &
               BTEST(ptr%id, pos_significant_id)) ) THEN
             CALL set_type_level (ptr, ft, t, l)
             CALL tlp_node_exclude (f_gho(ft,t,l), ptr)
             IF (front) THEN
                CALL tlp_node_include_f (f_sig(ft,t,l), ptr)
             ELSE
                CALL tlp_node_include_b (f_sig(ft,t,l), ptr)
             END IF
             ptr%sig = 1
          END IF
       END IF
       RETURN
    END IF ! ---------------------- end of tree root handling
    
    current_root => trees(index_in_tree_array)%root
    current_size = x_space/2
    nodes_created = 0
    current_root_level = 0
    leaf_index = 0
    DO
       summ = 0
       summ_bin = 0
       path(:) = 0
       DO i=1,DIM_NUM
          IF (IAND(opt_rel_coord(i),current_size).NE.0) path(i) = 1
          summ = summ + path(i)*2**(i-1)
          opt_rel_coord(i) = IAND(opt_rel_coord(i),current_size-1)
          summ_bin = IOR(summ_bin,opt_rel_coord(i))
       END DO
       
       IF (summ.EQ.0) THEN                                                 ! it is a higher order leaf
          leaf_index = leaf_index + LEAVES
          current_size = current_size/2
          current_root_level  = current_root_level + 1
          CYCLE
       END IF
       
       IF (current_root%num_leaves.LE.leaf_index) &
            CALL rearrange_leaves (current_root, leaf_index)
       leaf_index = leaf_index + summ
       
       new_child =>  current_root%leaves(leaf_index)%ptr
       IF (.NOT.ASSOCIATED(new_child)) THEN                      !add the new child ----------------
          ALLOCATE (new_child)

#if (TREE_VERSION == 2)
          ALLOCATE (new_child%ii(DIM_NUM))
          NULLIFY (new_child%val)
          NULLIFY (new_child%ival)   
#endif
          NULLIFY (new_child%next_tlp, new_child%prev_tlp)

          nodes_created = nodes_created + 1
          current_root%leaves(leaf_index)%ptr => new_child
          
          new_child%id = intermediate_id
          new_child%sig = 0
          new_child%num_leaves = 0
          new_child%level = current_root_level + 1                                    ! set internal level
          new_child%ii(:) = current_root%ii(:) + &
               current_size * path(:)                                                 ! set coordinates
         
          IF (summ_bin.EQ.0) THEN                              ! it's the target node !!!
             new_child%id = id_in                                                     !  set ID
             ptr => new_child

#if (TREE_VERSION == 2)
             ALLOCATE (new_child%val(VAL_NUM))                                        ! values
             ALLOCATE (new_child%ival(VAL_NUM_I))
#endif

             new_child%val(:) = 0.0_pr
             new_child%ival(:) = 0
             CALL set_type_level (new_child, ft, t, l)
             
             IF (BTEST(id_in, pos_adjacent_id).OR.BTEST(id_in, pos_significant_id)) THEN ! sig_adj list
                IF (front) THEN
                   CALL tlp_node_include_f (f_sig(ft,t,l),new_child)
                ELSE
                   CALL tlp_node_include_b (f_sig(ft,t,l),new_child)
                END IF
                new_child%sig = 1
             ELSE                                                            ! gho list
                !PRINT *, 'adding to ghost list'
                IF (front) THEN
                   CALL tlp_node_include_f (f_gho(ft,t,l),new_child)
                ELSE
                   CALL tlp_node_include_b (f_gho(ft,t,l),new_child)
                END IF
                new_child%sig = 2
             END IF
             
             out = nodes_created
             RETURN
          END IF
       ELSE                                                    ! do not add a child ----------------
          IF (summ_bin.EQ.0) THEN                                 ! it's the target node !!!
             IF (new_child%id.EQ.intermediate_id) THEN                       ! an intermediate
                CALL set_type_level (new_child, ft, t, l)

#if (TREE_VERSION == 2)
                ALLOCATE (new_child%val(VAL_NUM))                                        ! values
                ALLOCATE (new_child%ival(VAL_NUM_I))
#endif

                new_child%val(:) = 0.0_pr
                new_child%ival(:) = 0
                new_child%id = id_in                                                     !  ID
                nodes_created = nodes_created + 1

                IF (BTEST(id_in, pos_adjacent_id).OR.BTEST(id_in, pos_significant_id)) THEN
                   IF (front) THEN
                      CALL tlp_node_include_f (f_sig(ft,t,l),new_child)
                   ELSE
                      CALL tlp_node_include_b (f_sig(ft,t,l),new_child)
                   END IF
                   new_child%sig = 1
                ELSE
                   IF (front) THEN
                      CALL tlp_node_include_f (f_gho(ft,t,l),new_child)
                   ELSE
                      CALL tlp_node_include_b (f_gho(ft,t,l),new_child)
                   END IF
                   new_child%sig = 2
                END IF

                ptr => new_child
                out = nodes_created
                RETURN
             END IF

             new_child%id = IOR(new_child%id, id_in)

             IF ( (new_child%sig.EQ.2).AND.&
                  (BTEST(new_child%id, pos_adjacent_id).OR. &
                  BTEST(new_child%id, pos_significant_id)) ) THEN
                CALL set_type_level (new_child, ft, t, l)
                CALL tlp_node_exclude (f_gho(ft,t,l),new_child)
                IF (front) THEN
                   CALL tlp_node_include_f (f_sig(ft,t,l),new_child)
                ELSE
                   CALL tlp_node_include_b (f_sig(ft,t,l),new_child)
                END IF
                new_child%sig = 1
             END IF
             ptr => new_child
             out = nodes_created
             RETURN
          END IF
       END IF
       leaf_index = 0
       current_root_level = current_root_level + 1
       current_root => new_child
       current_size = current_size/2
    END DO
    
  END SUBROUTINE db_add_node

  !-----------------------------------------------------------------
  SUBROUTINE db_declare_globals (m, p, jtree, jmax, dim, f_num, if_num)
    ! Initialization of the database.
    INTEGER, INTENT(IN) :: dim         ! space dimension of the mesh (1,2,3,etc)
    INTEGER, INTENT(IN) :: f_num       ! the size of real function vector
    INTEGER, INTENT(IN) :: if_num      ! the size of integer function vector
    INTEGER, INTENT(IN) :: jtree       ! tree roots are the nodes of that level
    INTEGER, INTENT(IN) :: jmax        ! maximum level of the mesh
    INTEGER, INTENT(IN) :: m(dim)      ! number of points at the coarsest level, which is level 1
    INTEGER, INTENT(IN) :: p(dim)      ! periodicity marker (1 - periodic)
    INTEGER :: i, products(0:dim), &
         i_tmp, j, coord(dim), k
    
#if (TREE_VERSION == 2)
    ! test maximum dimension DIM_MAX
    IF (dim.GT.DIM_MAX) THEN
       WRITE(*,'("Maximum dimension DIM_MAX=",I1," is smaller than the requested dimension ",I1)') DIM_MAX, dim
       WRITE (*,'("Please increase the parameter DIM_MAX at db_tree_f.f90:60 and recompile")')
       STOP 'in db_declare_globals'
    END IF
#else
    ! test parameters from user_case_db module
    IF (dim.GT.max_dim) THEN
       WRITE(*,'("Maximum dimension max_dim=",I1," is smaller than the requested dimension ",I1)') max_dim, dim
       WRITE (*,'("Please increase the parameter max_dim in user_case_db module and recompile")')
       STOP 1
    END IF
    IF (f_num.GT.n_var_db) THEN
       WRITE(*,'("Maximum number of real variables n_var_db=",I1," is smaller than the requested ",I1)') n_var_db, f_num
       WRITE (*,'("Please increase the parameter n_var_db in user_case_db module and recompile")')
       STOP 'in db_declare_globals'
    END IF
    IF (if_num.GT.n_ivar_db) THEN
       WRITE(*,'("Maximum number of integer variables n_ivar_db=",I1," is smaller than the requested ",I1)') n_ivar_db, if_num
       WRITE (*,'("Please increase the parameter n_ivar_db in user_case_db module and recompile")')
       STOP 'in db_declare_globals'
    END IF
#endif
    
    ! Initialize Globals
    J_TREE = jtree
    J_MAX = jmax
    VAL_NUM = f_num
    VAL_NUM_I = if_num
    DIM_NUM = dim
    TYPES_NUM = 2**dim
    LEAVES = 2**dim - 1
    PERIODIC(:) = 0
    PERIODIC(1:dim) = p(1:dim)
    FINEST_MESH(:) = 0
    FINEST_MESH(1:dim) = m(1:dim)*2**(jmax-1)

    ! Initialize TL lists
    ! ( face_type, wlt_type, level )
    ALLOCATE (f_sig( 0:3**dim-1, 0:2**dim-1, 1:jmax ))
    ALLOCATE (f_gho( 0:3**dim-1, 0:2**dim-1, 1:jmax ))
    DO i=0,3**dim-1
       DO j=0,2**dim-1
          DO k=1,jmax
             NULLIFY(f_sig(i,j,k)%tlp_begin)
             NULLIFY(f_sig(i,j,k)%tlp_end)
             f_sig(i,j,k)%tlp_number_of_elements = 0
             NULLIFY(f_gho(i,j,k)%tlp_begin)
             NULLIFY(f_gho(i,j,k)%tlp_end)
             f_gho(i,j,k)%tlp_number_of_elements = 0
          END DO
       END DO
    END DO

    x_space = FINEST_MESH(1) / (m(1)*2**(jtree-1))                  ! Initialize the Forest
    products(0) = 1
    DO i=1,dim
       products(i) = products(i-1) * (m(i)*2**(jtree-1) + 1 - p(i))
    END DO
    number_of_trees = products(dim)
    IF (verb) WRITE (*,'(A,I8)') &
         'total number of trees:',number_of_trees

    ALLOCATE (trees(number_of_trees))
    
    DO i=1,number_of_trees
       i_tmp = i
       DO j=dim,1,-1                                                ! find N-dim coordinates from 1-dim index
          coord(j) = (i_tmp-1) / products(j-1)                          !
          i_tmp = MOD(i_tmp-1,products(j-1)) + 1
       END DO                                                       !
       coord(:) = coord(:)*2**(jmax-jtree)
       CALL create_tree_root (coord, i)
    END DO
    
    ! set gopt1_factor: to be used in add_node only
    gopt1_factor(:) = 1
    DO i=1,dim-1
       gopt1_factor(i+1) = gopt1_factor(i) * &
            ((FINEST_MESH(i)/2**(jmax-jtree)) + 1 - PERIODIC(i))
    END DO
    
  END SUBROUTINE db_declare_globals


  !-----------------------------------------------------------------
  !|----------------------------------------------------------------
  !|
  !|    PRIVATE STRUCTURE OF THE DATABASE
  !|
  !|----------------------------------------------------------------
  !-----------------------------------------------------------------


  SUBROUTINE delete_root ( ptr )
    ! delete root from link lists only
    ! set ID to intermediate
    TYPE(node), POINTER :: ptr
    INTEGER :: t, ft, l
    
    !IF (verb) WRITE (*,'(A,'//CHAR(ICHAR('0')+DIM_NUM)//'I3)') 'deleting root ', ptr%ii
    
    CALL set_type_level (ptr,ft,t,l)
    list_type: SELECT CASE (ptr%sig)         ! TLP
    CASE (1)
       CALL tlp_node_exclude (f_sig(ft,t,l), ptr)
    CASE (2)
       CALL tlp_node_exclude (f_gho(ft,t,l), ptr)
    END SELECT list_type
    ptr%sig = 0
    
    ptr%id = intermediate_id

  END SUBROUTINE delete_root

  !-----------------------------------------------------------------
  SUBROUTINE delete_node ( ptr )
    ! delete node from link lists and
    ! deallocate all the memory associated to the node
    TYPE(node), POINTER :: ptr
    INTEGER :: t, ft, l
    
    !IF (verb) WRITE (*,'(A,'//CHAR(ICHAR('0')+DIM_NUM)//'I3)') 'deleting ', ptr%ii
    
    CALL set_type_level (ptr,ft,t,l)
    list_type: SELECT CASE (ptr%sig)         ! TLP
    CASE (1)
       CALL tlp_node_exclude (f_sig(ft,t,l), ptr)
    CASE (2)
       CALL tlp_node_exclude (f_gho(ft,t,l), ptr)
    END SELECT list_type
    ptr%sig = 0
    
#if (TREE_VERSION == 2)
    IF (ASSOCIATED(ptr%val)) &               ! values
         DEALLOCATE (ptr%val, ptr%ival)      ! ivalues
    DEALLOCATE (ptr%ii)                      ! coord
#endif

    IF (ptr%num_leaves.NE.0) &
         DEALLOCATE (ptr%leaves)             ! leaves
    DEALLOCATE (ptr)                         ! the node itself
    
  END SUBROUTINE delete_node

  !-----------------------------------------------------------------
  FUNCTION found_nonzero_child_link ( ptr, link_index, lower_limit_index, total_children )
    LOGICAL :: found_nonzero_child_link
    TYPE(node), POINTER :: ptr
    INTEGER, INTENT(IN) :: lower_limit_index
    INTEGER, INTENT(INOUT) :: total_children
    INTEGER, INTENT(OUT) :: link_index
    INTEGER :: i
    
    DO i=1,ptr%num_leaves
       IF (ASSOCIATED(ptr%leaves(i)%ptr)) &
            total_children = total_children + 1
    END DO
    
    DO i=lower_limit_index,ptr%num_leaves
       IF (ASSOCIATED(ptr%leaves(i)%ptr)) THEN
          link_index = i
          found_nonzero_child_link = .TRUE.
          RETURN
       END IF
    END DO
    found_nonzero_child_link = .FALSE.
    RETURN
    
  END FUNCTION found_nonzero_child_link

  !-----------------------------------------------------------------
  SUBROUTINE tlp_node_exclude (list, anode)              ! exclude from TLP system
    TYPE(type_level), INTENT(INOUT) :: list              ! the list
    TYPE(node), INTENT(INOUT), TARGET :: anode           ! the node to be excluded
    TYPE(node), POINTER :: prev_node, next_node

    !PRINT *, 'tlp_node_exclude', anode%ii, 'id=',anode%id,'j=',anode%level

    IF (list%tlp_number_of_elements.GT.0) THEN
       prev_node => anode%prev_tlp
       next_node => anode%next_tlp
      
       IF (ASSOCIATED(prev_node)) THEN
          prev_node%next_tlp => next_node      ! set the pointer to the new next node
       ELSE
          list%tlp_begin => next_node          ! anode was the first node in the sequence
       END IF
      
       IF (ASSOCIATED(next_node)) THEN
          next_node%prev_tlp => prev_node
       ELSE
          list%tlp_end => prev_node            ! anode was the last element
       END IF

       list%tlp_number_of_elements = list%tlp_number_of_elements - 1   ! decrease the counter
       NULLIFY (anode%prev_tlp, anode%next_tlp)                        ! nullify the pointers
    END IF

  END SUBROUTINE tlp_node_exclude

  !-----------------------------------------------------------------
  SUBROUTINE set_type_level (n,ft,t,l)                   ! calculate facetype, type, and level
    TYPE(node), INTENT(IN) :: n                          ! for the given node;
    INTEGER, INTENT(OUT) :: t, ft, l                     ! wlt_types are 0 for the level 1
    INTEGER :: coord(DIM_NUM), space, lv, i, &
         node_belongs, factor
    
    coord(:) = n%ii(:)                                         ! finest mesh coordinates
    space = 2**J_MAX
    DO lv=1,J_MAX
       space = space/2
       node_belongs = 0
       DO i=1,DIM_NUM
          node_belongs = node_belongs + IAND(coord(i),space-1)
       END DO
       IF (node_belongs.NE.0) CYCLE
       l = lv                                                  ! that is the node's level
       EXIT
    END DO
    t = 0                                                      ! that will be the node's type
    IF (l.NE.1) THEN
       factor = 1
       DO i=DIM_NUM,1,-1
          t = t + factor * ((IAND(coord(i),2*space-1))/space)
          factor = factor * 2
       END DO
    END IF
    ft = 0                                                  ! that will be the node's facetype
    factor = 1
    DO i=1,DIM_NUM
       ft = ft + PERIODIC(i)*factor + (1-PERIODIC(i))*factor* &
            ( MIN(1,coord(i)) + coord(i)/FINEST_MESH(i) )
       factor = factor*3 
    END DO
    
    !WRITE(*,'("set_type_level(ft,t,l)=",3(I2,X))') ft,t,l
  END SUBROUTINE set_type_level

  !-----------------------------------------------------------------
  SUBROUTINE rearrange_leaves (n, li)                    ! rearrange leaves
    TYPE(node), INTENT(INOUT) :: n                       ! for that node
    INTEGER, INTENT(IN) :: li                            ! in that quantity
    TYPE(node_ptr), POINTER :: newleaves(:)
    INTEGER :: oldli, newli, i
    

    oldli = n%num_leaves                  ! old number of leaves
    newli = LEAVES + li                   ! new number of leaves
    
    ALLOCATE (newleaves(newli))           ! create new array of node_ptr
    DO i=1,newli
       NULLIFY (newleaves(i)%ptr)         ! and make it empty
    END DO
    
    IF (oldli.NE.0) THEN                  ! copy old data, if required
       DO i=1,oldli
          newleaves(i)%ptr => n%leaves(i)%ptr
       END DO
       DEALLOCATE (n%leaves)
    END IF
    
    n%num_leaves = newli
    n%leaves => newleaves

  END SUBROUTINE rearrange_leaves

  !-----------------------------------------------------------------
  SUBROUTINE tlp_node_include_f (list, anode)            ! include into TLP system
    TYPE(type_level), INTENT(INOUT) :: list              ! the list
    TYPE(node), INTENT(INOUT), TARGET :: anode           ! the node to be included
    TYPE(node), POINTER :: old_first_element

    !PRINT *, 'tlp_node_include_f', anode%ii, 'id=',anode%id,'j=',anode%level,"num=",list%tlp_number_of_elements

    IF (list%tlp_number_of_elements.EQ.0) THEN           ! It's the first one,
       list%tlp_begin => anode                           ! so start a new sequence
       list%tlp_end => anode
       NULLIFY(anode%prev_tlp)
       NULLIFY(anode%next_tlp)
    ELSE                                                 ! There are some already
       old_first_element => list%tlp_begin               ! old first element
       list%tlp_begin => anode                           ! new first element
       
       old_first_element%prev_tlp => anode               !  links
       anode%next_tlp => old_first_element               ! ...
       NULLIFY(anode%prev_tlp)                           ! ...
    END IF
    list%tlp_number_of_elements = list%tlp_number_of_elements + 1

  END SUBROUTINE tlp_node_include_f

  !-----------------------------------------------------------------
  SUBROUTINE tlp_node_include_b (list, anode)            ! include into TLP system
    TYPE(type_level), INTENT(INOUT) :: list              ! the list
    TYPE(node), INTENT(INOUT), TARGET :: anode           ! the node to be included
    TYPE(node), POINTER :: old_last_element
    
    !PRINT *, 'tlp_node_include_b', anode%ii, 'id=',anode%id,'j=',anode%level,"num=",list%tlp_number_of_elements

    IF (list%tlp_number_of_elements.EQ.0) THEN           ! It's the first one,
       list%tlp_begin => anode                           ! so start a new sequence
       list%tlp_end => anode
       NULLIFY(anode%prev_tlp)
       NULLIFY(anode%next_tlp)
    ELSE                                                 ! There are some already
       old_last_element => list%tlp_end                  ! old last element
       list%tlp_end => anode                             ! new last element
       
       old_last_element%next_tlp => anode                ! links
       anode%prev_tlp => old_last_element                ! ...
       NULLIFY(anode%next_tlp)                           ! ...
    END IF
    list%tlp_number_of_elements = list%tlp_number_of_elements + 1
    
  END SUBROUTINE tlp_node_include_b

  !-----------------------------------------------------------------
  SUBROUTINE create_tree_root (coord, treenum)
    INTEGER, INTENT(IN) :: coord(DIM_NUM)           ! root's coordinates
    INTEGER, INTENT(IN) :: treenum                  ! element of trees(:)
    TYPE(node), POINTER :: root                    ! current root
    INTEGER :: i, lv, space, node_belongs, &
         my_type, my_level, factor, ft !, a,b,c
    
    !ALLOCATE (root)                                 ! allocate the space
    root => trees(treenum)%root

#if (TREE_VERSION == 2)
    ALLOCATE (root%ii(DIM_NUM))
    ALLOCATE (root%val(VAL_NUM))
    ALLOCATE (root%ival(VAL_NUM_I))
#endif
    
    root%ii(:) = coord(:)
    root%val(:) = 0.0_pr
    root%ival(:) = 0
    root%id = IBSET(0,pos_significant_id)
    root%level = 0                                  ! internal root's level
    root%num_leaves = 0                             ! size of leaves(:)
    
    NULLIFY (root%next_tlp, root%prev_tlp)                  ! set type and level
    NULLIFY (root%leaves)
    DO lv=1,J_TREE
       space = 2**(J_MAX-lv)
       node_belongs = 0
       DO i=1,DIM_NUM
          node_belongs = node_belongs + IAND(coord(i),space-1)
       END DO
       IF (node_belongs.NE.0) CYCLE
       my_level = lv                                        ! that is the node's level
       EXIT
    END DO
    my_type = 0                                             ! that will be the node's type
    IF (my_level.NE.1) THEN
       factor = 1
       DO i=DIM_NUM,1,-1
          my_type = my_type + factor * &
               ((IAND(coord(i),2*space-1))/space)
          factor = factor * 2
       END DO
    END IF
    ft = 0                                                  ! that will be the node's facetype
    factor = 1
    DO i=1,DIM_NUM
       ft = ft + PERIODIC(i)*factor + (1-PERIODIC(i))*factor* &
            ( MIN(1,coord(i)) + coord(i)/FINEST_MESH(i) )
       factor = factor*3
    END DO
  
    !trees(treenum)%root => root                             ! set trees(:) pointer


    IF (verb) WRITE (*,'(A,'//CHAR(ICHAR('0')+DIM_NUM)//'I5,A,3I3)') &
         'root coord=',coord, &
         ', type-ftype-level=',my_type, ft, my_level

    CALL tlp_node_include_b (f_sig(ft,my_type,my_level), root)  ! include into TLP system
    root%sig = 1
    
  END SUBROUTINE create_tree_root

END MODULE tree_database_f
