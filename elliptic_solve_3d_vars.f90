!****************** variables used for elliptic solver
MODULE elliptic_vars
  USE precision
  PUBLIC
  REAL (pr) :: w0, w1, w2, w3, w_GS, w_penal, w_min, w_max, alph  
  INTEGER :: kry_p, kry_perturb, len_bicgstab, len_iter
  INTEGER :: kry_p_coarse, len_bicgstab_coarse
  INTEGER, DIMENSION(:,:), ALLOCATABLE :: elliptic_zone
  INTEGER, DIMENSION(:,:,:), ALLOCATABLE :: i_elliptic_zone, i_GS
  INTEGER, DIMENSION(:,:), ALLOCATABLE :: n_elliptic_zone, n_GS
  INTEGER, DIMENSION(:,:), ALLOCATABLE :: i_gmres_zone
  INTEGER, DIMENSION(:), ALLOCATABLE :: n_gmres_zone
  INTEGER :: thickness_elliptic_zone
  LOGICAL :: elliptic_project ! if T, perform additional zero-mean projection every iteration ad on every level
  LOGICAL :: elliptic_zone_active
  LOGICAL :: BC_zone
  LOGICAL :: GMRESflag, BiCGSTABflag, IterMeth, diagnostics_elliptic, display_Vcycle
  LOGICAL :: Jacoby_correction, multigrid_correction, GMRES_correction
  LOGICAL :: zero_mean ! T- enforce zero mean for 1:dim first variables (velocity usually), F- do nothing
  LOGICAL :: set_to_zero_if_diverged ! T - set the solution of elliptic problem to zero if diverged, F - do not do anything
  LOGICAL :: interpolate_internal = .TRUE. ! T regular, F then uses normal interpolation including boundaries
  LOGICAL :: GS_ACTIVE = .FALSE.
END MODULE elliptic_vars
