 !***************************************
MODULE wavelet_filters_vars
  USE precision
  PUBLIC

  LOGICAL :: ExplicitFilter   !apply grid filter as an explicit filter each time step
  LOGICAL :: ExplicitFilterNL !apply grid filter as an explicit filter to the non-linear term
  INTEGER :: mdl_filt_type_grid, mdl_filt_type_test !type of mdl filters (input )
  REAL (pr), DIMENSION (:), ALLOCATABLE :: u_g_mdl_gridfilt_mask, u_g_mdl_testfilt_mask

  INTEGER :: lowpass_filt_type ! 0 - top-hat volume averged, 1 - trapezoidal
  INTEGER :: lowpass_filt_type_GRID ! filter type for grid-filter level: 0 - top-hat volume averged, 1 - trapezoidal
  INTEGER :: lowpass_filt_type_TEST ! filter type for test-filet level: 0 - top-hat volume averged, 1 - trapezoidal
  INTEGER :: lowpassfilt_support ! filter support, if used in other places like LDM
  INTEGER :: lowpassfilt_support_GRID ! filter support if used for grid-filter level
  INTEGER :: lowpassfilt_support_TEST ! filter support if used for test-filter level
  INTEGER, PARAMETER:: lowpassFilterType_TOPHAT = 0, lowpassFilterType_TRAPEZOIDAL = 1
  LOGICAL :: tensorial_filt    ! if .TRUE. uses tensorial filter for comparison with lines
  
 INTEGER, PARAMETER:: GRIDfilter = 0, TESTfilter = 1
  
 INTEGER, PARAMETER:: MDL_FILT_NONE         = 0  ! no filter (mask all 1.0_pr)
 INTEGER, PARAMETER:: MDL_FILT_EPS          = 1  ! u > eps filter
 INTEGER, PARAMETER:: MDL_FILT_2EPS         = 2  ! u > 2eps filter
 INTEGER, PARAMETER:: MDL_FILT_jm1          = 3  ! retain all collocation pts up to level j-1 (LES)
 INTEGER, PARAMETER:: MDL_FILT_jm2          = 4  ! retain all collocation pts up to level j-2 (LES)
 INTEGER, PARAMETER:: MDL_FILT_2EPSplus     = 5  ! u > 2eps filter + adjecent zone 
 INTEGER, PARAMETER:: MDL_FILT_LPlocal      = 6  ! u > Local low-pass filter
 INTEGER, PARAMETER:: MDL_FILT_EPSexpl      = 7  ! u > eps_explicit filter  
 INTEGER, PARAMETER:: MDL_FILT_EPSexplplus  = 8  ! u > eps_explicit filter + adjecent zone 
 INTEGER, PARAMETER:: MDL_FILT_2EPSexpl     = 9  ! u > 2eps_explicit filter  
 INTEGER, PARAMETER:: MDL_FILT_2EPSexplplus = 10 ! u > 2eps_explicit filter + adjecent zone 

END MODULE wavelet_filters_vars

