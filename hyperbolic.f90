MODULE hyperbolic_solver
  !
  ! hyperbolic  module for compressible flow problems
  ! Jonathan Regele 7/25/07
  !
  USE precision
  USE wlt_vars
  USE wavelet_filters_mod
  USE wlt_trns_mod
  USE wlt_trns_vars
  USE io_3d_vars
  USE share_consts
  USE pde
    USE variable_mapping 
  USE sizes
  USE share_kry
  USE vector_util_mod
  USE field
  USE input_file_reader
  USE wavelet_filters_mod
  USE penalization
  IMPLICIT NONE
  PRIVATE

  REAL (pr) :: nu, hyper_mode, eps_high, eps_low, q_wght, visc_tail_thickness
  INTEGER :: n_visc
  INTEGER :: limiter_type
  INTEGER, DIMENSION(:), ALLOCATABLE, PUBLIC :: n_var_mom_hyper
  INTEGER, PUBLIC :: n_var_den_hyper
  REAL (pr), DIMENSION (:,:), ALLOCATABLE :: v_back, v_for
  REAL (pr), DIMENSION (:,:), ALLOCATABLE :: vMask
  REAL (pr), DIMENSION (:), ALLOCATABLE, PUBLIC :: visc
  INTEGER, DIMENSION (:), ALLOCATABLE :: min_wall_type, max_wall_type
  REAL (pr), DIMENSION(:), ALLOCATABLE, PUBLIC :: c_sound 
  INTEGER, DIMENSION(:,:), ALLOCATABLE :: face_wall_type
  PUBLIC :: hyper_read_input, hyper_setup, hyper_finalize_setup, hyper_additional_vars, hyper_pre_process, &
       hyperbolic, hyperbolic_diag, shift_by

  REAL (pr), PRIVATE :: visc_scale

  ! ERIC: visc zone is just a local hack, do not merge
  REAL (pr), DIMENSION(:,:), ALLOCATABLE, PRIVATE :: visc_zone 
  LOGICAL, PRIVATE :: use_visc_zone
  PRIVATE :: set_visc_zone
  
CONTAINS
  
  !
  ! In hyper_setup() we setup all the varaibles necessary for SGS 
  !
  ! The following variables must be setup in this routine:
  !
  !
  !
  SUBROUTINE  hyper_setup () 
    USE PDE
    USE parallel
    USE equations_compressible
    IMPLICIT NONE
    
    IF (par_rank.EQ.0) THEN
       PRINT * ,''
       PRINT *, '************** Setting up hyperbolic module ****************'
       PRINT * ,' Hyperbolic module'
       PRINT *, '*****************************************************'
    END IF

    IF(ALLOCATED(n_var_mom_hyper))DEALLOCATE(n_var_mom_hyper)
    ALLOCATE(n_var_mom_hyper(1:dim))
    n_var_mom_hyper = 0
    n_var_den_hyper = 0

    ! TODO: are all saved variables required for restart?
    CALL register_var( 'visc            ', .FALSE., adapt=(/.FALSE.,.FALSE./), interpolate=(/.TRUE.,.TRUE./), exact=(/.FALSE.,.FALSE./), saved=savevisc, req_restart=.FALSE. )

  END SUBROUTINE hyper_setup

  ! Called after all variables and mappings and counts are established
  SUBROUTINE  hyper_finalize_setup () 
    USE PDE
    USE parallel
    IMPLICIT NONE
    
     n_visc = get_index('visc            ')
     IF(.NOT.ALLOCATED(n_var_hyper)) THEN
        ALLOCATE(n_var_hyper(1:n_var))
        n_var_hyper = .FALSE.
     END IF
     IF(.NOT.ANY(n_var_hyper)) n_var_hyper(1:n_integrated) = .TRUE. 
     IF(.NOT.ALLOCATED(n_var_hyper_active)) THEN
        ALLOCATE(n_var_hyper_active(1:n_integrated))
        n_var_hyper_active = .FALSE.
     END IF
     IF(.NOT.ANY(n_var_hyper_active)) n_var_hyper_active(1:n_integrated) = .TRUE. 

  END SUBROUTINE hyper_finalize_setup
  !
  ! read variables from input file for this user case 
  !
  SUBROUTINE  hyper_read_input()
    IMPLICIT NONE

    IF(ALLOCATED(min_wall_type)) DEALLOCATE(min_wall_type)
    ALLOCATE(min_wall_type(1:dim))
    IF(ALLOCATED(max_wall_type)) DEALLOCATE(max_wall_type)
    ALLOCATE(max_wall_type(1:dim))

    call input_real ('eps_high',eps_high,'stop',' eps_high: error upper bound')
    call input_real ('eps_low',eps_low,'stop',' eps_low: error lower bound')
    call input_real ('hyper_mode',hyper_mode,'stop',' hyper_mode: hyper_mode used by hyperbolic solver')
    ! Default limiter type is van Leer
    limiter_type = 1
    call input_integer ('limiter_type',limiter_type,'default','limiter_type: Minbee=0,van Leer=1 (default), Superbee=2')
    call input_integer_vector ('min_wall_type',min_wall_type(1:dim),dim,'stop',&
         ' min_wall_type: Specifies if using reflecting or evolutionary walls with hyperbolic solver')
    call input_integer_vector ('max_wall_type',max_wall_type(1:dim),dim,'stop',&
         ' max_wall_type: Specifies if using reflecting or evolutionary walls with hyperbolic solver')

    visc_scale = 1.0_pr
    call input_real ('visc_scale',visc_scale,'default',' visc_scale: Scaling coefficient for controlling the computed viscosity')

    ! Use of a viscous zone on the computational domain
    use_visc_zone = .FALSE.
    call input_logical ('use_visc_zone', use_visc_zone,'default',' use_visc_zone: define a computational coordinate zone over which the hyperbolic viscosity is applied')
    IF ( use_visc_zone ) THEN
      IF ( ALLOCATED( visc_zone ) ) DEALLOCATE( visc_zone )
      ALLOCATE(visc_zone(1:2,1:dim))
      call input_real_vector ('min_visc_zone', visc_zone(1,1:dim),dim,'stop',&
         ' min_visc_zone: Specifies the minimum values of the viscous zone, over which to apply hyperbolic viscosity') 

      call input_real_vector ('max_visc_zone', visc_zone(2,1:dim),dim,'stop',&
         ' max_visc_zone: Specifies the maximum values of the viscous zone, over which to apply hyperbolic viscosity') 
    END IF

  END SUBROUTINE hyper_read_input

  !******************************************************************************************
  !******************************** HYPERBOLIC MODULE ROUTINES ******************************
  !******************************************************************************************

  SUBROUTINE hyper_pre_process (u,n,neq,c)
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: n, neq
    REAL (pr), DIMENSION (n,neq), INTENT(IN) :: u !1D flat version of integrated variables without BC 
    REAL (pr), DIMENSION (n) :: c

!!$    IF(ALLOCATED(n_var_mtm))DEALLOCATE(n_var_mtm)
!!$    ALLOCATE(n_var_mtm(1:dim))
!!$    n_var_mtm(1:dim) = n_var_mom_hyper

    IF(ALLOCATED(vMask)) DEALLOCATE(vMask)
    ALLOCATE(vMask(1:n,1:dim))

    IF(ALLOCATED(v_back)) DEALLOCATE(v_back)
    ALLOCATE(v_back(1:n,1:dim))
    IF(ALLOCATED(v_for)) DEALLOCATE(v_for)
    ALLOCATE(v_for(1:n,1:dim))

    IF(ALLOCATED(visc)) DEALLOCATE(visc)
    ALLOCATE(visc(n))

    SELECT CASE (hypermodel)
    CASE(1) ! Wavelet based diffusion
       CALL set_diffusion(u, n, neq, c)
    CASE(2) ! Flux limited method
       CALL set_diffusion2(u, n, neq, c)
    END SELECT

    CALL add_bc(n, j_lev)

  END SUBROUTINE hyper_pre_process

  SUBROUTINE hyper_additional_vars (u,nwlt,c,flag)
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: nwlt,flag
    REAL (pr), DIMENSION (nwlt) :: c
    REAL (pr), DIMENSION (nwlt,n_var), INTENT(INOUT) :: u 

    IF(flag.EQ.0) THEN
       CALL hyper_pre_process (u, nwlt, n_var,c)
    END IF

    u(1:nwlt,n_visc) = visc(1:nwlt)

  END SUBROUTINE hyper_additional_vars

!
! Set flux limiter based artifical diffusion
!
  SUBROUTINE set_diffusion2 (u, n, neq, c) !--Set Localized diffusion
    USE PDE
    USE parallel
    IMPLICIT NONE
    INTEGER, INTENT (IN)  :: n, neq
    REAL (pr), DIMENSION (n, neq), INTENT (IN) :: u
    INTEGER :: i, ieq, idim
    ! --------------- Viscosity Modifications ------------------------
    INTEGER :: adj_type 
    REAL (pr), DIMENSION (n) :: uTemp, vMask_loc
    REAL (pr) :: tmp1
    REAL (pr), DIMENSION(n) :: c, one
    REAL (pr), DIMENSION (dim,n) :: h_arr
    REAL (pr), DIMENSION (n,dim) :: phiTemp
    LOGICAL :: tmpl
    REAL (pr), DIMENSION (n,dim) :: r_back, r_for, phi, phi_tmp
    REAL (pr), DIMENSION (n) :: phiMin, vMax
    REAL (pr) :: tmp2

    IF(ALLOCATED(visc)) THEN
       IF(SIZE(visc) /= n) THEN
          DEALLOCATE(visc)
          ALLOCATE(visc(1:n))
       END IF
    ELSE
       ALLOCATE(visc(1:n))
    END IF

    one = 1.0_pr

    ! Create flux limiter function
    phi=2.0_pr
    phiMin=2.0_pr
    DO idim=1,dim
       DO ieq=1,neq 
          ! Don't base flux limiter on momentum equations
          if ( (ieq.ge.minval(n_var_mom_hyper)) .and. (ieq.le.maxval(n_var_mom_hyper)) ) cycle

          ! Calculate r values
          CALL calc_r (u(1:n,ieq), n, r_back, r_for)
          
          ! Take minimum limiter of different integrated variables
          phi_tmp(1:n,idim) = min(limiter(r_for(1:n,idim),n), limiter(r_back(1:n,idim),n))
          tmp2 = MAXVAL(phi_tmp)
          tmp2 = abs(maxval(u(1:n,ieq))-minval(u(1:n,ieq)))
          CALL parallel_global_sum( REALMAXVAL=tmp2 )
          IF (tmp2.GT.1E-6) THEN
             phi(1:n,idim) = min(phi(1:n,idim),phi_tmp(1:n,idim))
          END IF
       END DO
       phiMin = min(phiMin,phi(1:n,idim))
   END DO

    ! For debugging
    !visc = 1.0_pr-phiMin

    ! Setup vMask
    ! Maximum characteristic speed x (1-phi)
    ! Use (1-phi) because we are adding viscosity instead of subtracting it
    ! as conventionally done in flux limited methods
    vMask = 0.0_pr
    ! Use different viscosity in different directions
    DO i=1,dim
       vMask(1:n,i) = (abs(u(1:n,n_var_mom_hyper(i))/u(1:n,1))+c)*(1.0_pr-phi(1:n,i))
    END DO
    ! Use maximum viscosity in all directions
!!$    DO i=1,dim
!!$       uTemp = (abs(u(1:n,n_var_mom_hyper(i))/u(1:n,1))+c)*(1.0_pr-phi(1:n,i))
!!$       vMask(1:n,i) = max(vMask(1:n,i),uTemp(1:n))
!!$    END DO

    
    
    ! Test directional instability
    ! x-direction
!    vMask(1:n,2) = 0.0_pr
!    visc = vMask(:,1)!v_back(:,1)
!!$    ! y-direction
!!$!    vMask(1:n,1) = 0.0_pr
!!$    visc = vMask(:,2)!v_back(:,1)


    ! Must take maximum viscosity for i+1/2, i-1/2 locations
    CALL shift_by(vMask, phiTemp, n, 1)
    v_for(1:n,1:dim) = MAX(vMask(1:n,1:dim), phiTemp(1:n,1:dim))
    CALL shift_by(vMask, phiTemp, n, -1)
    v_back(1:n,1:dim) = MAX(vMask(1:n,1:dim), phiTemp(1:n,1:dim))

!!$    ! Take average viscosity for i+1/2, i-1/2 locations
!!$    CALL shift_by(vMask, phiTemp, n, 1)
!!$    v_for(1:n,1:dim) = (vMask(1:n,1:dim) + phiTemp(1:n,1:dim))*0.5_pr
!!$    CALL shift_by(vMask, phiTemp, n, -1)
!!$    v_back(1:n,1:dim) = (vMask(1:n,1:dim) + phiTemp(1:n,1:dim))*0.5_pr

  END SUBROUTINE set_diffusion2

!
! Calculate flux limiter based on r+ and r-
!
  FUNCTION limiter (r_, n)
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: n
    REAL (pr), DIMENSION (n), INTENT (IN) :: r_
    REAL (pr), DIMENSION (n) :: limiter
    REAL (pr), DIMENSION (n) :: zero, one

    zero = 0.0_pr
    one = 1.0_pr
    
    SELECT CASE(limiter_type)
       CASE(0)    ! First order accurate
          limiter = zero 
       CASE(1)    ! Min Bee
          limiter = max(min(r_,one),zero)
       CASE(2) ! van Leer
          limiter = max(2_pr*r_/(1+r_),zero)
       CASE(3) ! Super Bee
          limiter = max(zero,min(max(min(2.0_pr*r_,one),r_),2.0_pr*one))
       CASE(4) ! Ultra Bee
          limiter = max(min(2_pr*r_,2.0_pr*one),zero)
       CASE(5) ! van Albada 2
          limiter = max(zero,max(2.0_pr*r_/(r_**2+one),min(r_,one)))
!          limiter = max(zero,2.0_pr*r_/(r_**2+one))
!          limiter = 2.0_pr*r_/(r_**2+one)
       END SELECT
  END FUNCTION limiter

!
! Calculate r+ and r- for flux limiter calculation
!
  SUBROUTINE calc_r (u, n, r_back, r_for)
    IMPLICIT NONE
    INTEGER :: i, n, idim
    REAL (pr), DIMENSION (n), INTENT (IN) :: u
    REAL (pr), DIMENSION (n,dim) :: du_back,du_for, d2u
    REAL (pr), DIMENSION (dim,n) :: h_arr
    REAL (pr), DIMENSION (n,dim), INTENT (OUT) :: r_back, r_for
    REAL (pr) :: epsR

    ! Get grid spacing
    CALL get_all_local_h (h_arr)
    
    ! Set tolerance for r+ and r- calculations
    epsR = 1.0E-06_pr

    ! -----------------------------
    ! Calculate rBack and rFor
    ! Start with calculating derivatives in forward and backward directions
    ! First test with only total energy as signaling variable
    CALL c_diff_fast(u(1:n), du_for(1:n,1:dim), d2u(1:n,1:dim), j_lev, n, 4, 10, 1, 1, 1 )   
    CALL c_diff_fast(u(1:n), du_back(1:n,1:dim), d2u(1:n,1:dim), j_lev, n, 2, 10, 1, 1, 1 ) 
     
    ! Apply corrections to prevent overflow
    ! First start with single direction dim=1
    DO idim=1,dim
       DO i=1,n

          ! r_back
          IF ( ABS(du_for(i,idim)) .LT. epsR/h_arr(idim,i) ) THEN
             r_back(i,idim) = SIGN(1.0_pr,du_for(i,idim))*du_back(i,idim)*h_arr(idim,i)/epsR
          ELSE
             r_back(i,idim) = du_back(i,idim)/du_for(i,idim)
          END IF
          
          ! r_for
          IF ( ABS(du_back(i,idim)) .LT. epsR/h_arr(idim,i) ) THEN
             r_for(i,idim) = SIGN(1.0_pr,du_back(i,idim))*du_for(i,idim)*h_arr(idim,i)/epsR
          ELSE
             r_for(i,idim) = du_for(i,idim)/du_back(i,idim)
          END IF
          
       END DO
    END DO
 
  END SUBROUTINE calc_r

!
! Calculate wavelet based artificial viscosity
!
  SUBROUTINE set_diffusion (u, nlocal, ieq, c) !--Set Localized diffusion
    USE PDE
    USE parallel
    USE curvilinear
    USE curvilinear_mesh
    IMPLICIT NONE
    INTEGER, INTENT (IN)  :: nlocal, ieq
    REAL (pr), DIMENSION (nlocal, ieq), INTENT (IN) :: u
    REAL (pr), DIMENSION (nlocal), INTENT(IN) :: c
    INTEGER :: i
    ! --------------- Viscosity Modifications ------------------------
    INTEGER :: adj_type 
    REAL (pr), DIMENSION (nlocal) :: uTemp, vMask_loc
    REAL (pr) :: tmp1
    REAL (pr), DIMENSION(nlocal,dim) :: v, stretch
    REAL (pr), DIMENSION(nlocal) :: c_comp
    REAL (pr), DIMENSION (dim,nlocal) :: h_arr
    REAL (pr), DIMENSION (nlocal,dim) :: phiTemp
    LOGICAL :: tmpl

    IF(ALLOCATED(visc)) THEN
       IF(SIZE(visc) /= nlocal) THEN
          DEALLOCATE(visc)
          ALLOCATE(visc(1:nlocal))
       END IF
    ELSE
       ALLOCATE(visc(1:nlocal))
    END IF

    ! --------------- Add diffusion on the finest scale
    adj_type = 1

    visc = 0.0_pr

    IF(j_lev >= J_hyper) THEN

       uTemp = 0.0_pr
       
       DO i=1,n_var
          IF(n_var_hyper(i)) THEN
             visc(1:nlocal)=u(1:nlocal,i)
             CALL c_wlt_trns (visc, 1, 1, 1, 1, 1)
             
             ! Try volume filter
             vMask_loc = ABS(visc)
             CALL hyper_filter(vMask_loc, j_lev, nlocal, 1)
             uTemp = MAX(uTemp,(vMask_loc-eps_low*scl_global(i))/((eps_high-eps_low)*scl_global(i)))
          END IF
       END DO
       vMask_loc = ABS(uTemp)
       
       DO i=1,nlocal
          vMask_loc(i) = MIN(vMask_loc(i),2.0_pr)
       END DO
       
       c_comp = c
       IF ( ANY( transform_dir(1:dim) .EQ. 1 ) ) THEN  ! Transform acoustic characteristic 
          c_comp = 0.0_pr   ! Temporary usage
          IF(.NOT.ALLOCATED(n_var_mom_hyper) .OR. (ALLOCATED(n_var_mom_hyper) .AND. MAXVAL(ABS(n_var_mom_hyper    )) == 0) .OR. n_var_den_hyper .EQ. 0) THEN  ! Compressible variables not defined
             phiTemp(:,1:dim) = 1.0_pr
          ELSE
             phiTemp(:,1:dim) = u(:,n_var_mom_hyper(1:dim))
          END IF
          CALL calc_mesh_stretching( v, stretch, phiTemp, MESH_STRETCHING_PHYS ) ! Find contravarient momentum vector
          DO i=1,dim
             c_comp = MAX( c_comp, c * stretch(:,i) )
          END DO
       END IF
       !NOTE:  if used for non-conserved variables or other purposes, need to be twicked
       !       the form below assumes that u(:,1)=density and u(:,n_var_mom_hyper) is momentum
       tmp1 = MAXVAL(c_comp)
       CALL parallel_global_sum( REALMAXVAL=tmp1 )
       IF(.NOT.ALLOCATED(n_var_mom_hyper)     .OR. (ALLOCATED(n_var_mom_hyper)     .AND. MAXVAL(ABS(n_var_mom_hyper    )) == 0) .OR. n_var_den_hyper .EQ. 0) THEN  ! Compressible variables not defined
          !visc = MAX(c_comp, tmp1*vMask_loc)*vMask_loc
          visc = c_comp*vMask_loc
       ELSE
          visc = 0.0_pr
          IF ( ANY( transform_dir(1:dim) .EQ. 1 ) ) THEN
             !visc = MAX(SQRT(SUM(v(:,1:dim)**2,DIM=2)/u(:,n_var_den_hyper)**2)+c_comp, tmp1*vMask_loc)*vMask_loc
             DO i=1,dim
                !visc = MAX( visc, ABS(v(:,i)/u(:,n_var_den_hyper)) )
                visc = MAX( visc, ABS(v(:,i)/u(:,n_var_den_hyper)) + c * stretch(:,i) )  
             END DO
          ELSE
             DO i=1,dim
                !visc = MAX( visc, ABS(u(:,n_var_mom_hyper(i))/u(:,n_var_den_hyper)))
                visc = MAX( visc, ABS(u(:,n_var_mom_hyper(i))/u(:,n_var_den_hyper)) + c )  
             END DO
          END IF
          !visc = MAX(visc+c_comp, tmp1*vMask_loc)*vMask_loc
          !visc = MAX(visc, tmp1*vMask_loc)*vMask_loc
          visc = visc*vMask_loc
       END IF
       
       visc = visc*2.0_pr**MAX(0,j_lev-J_hyper) !increase viscosity to accomodate coarseing of effective resolution
       
       ! Scale the viscosity function
       visc = visc_scale * visc
       ! Smooth the viscosity function
       visc = MAX(0.0_pr*visc,visc)
       DO i=1,2
          CALL local_lowpass_filt (visc(1:nlocal), j_lev, nlocal, 1, 1, 1, 1, 1, .FALSE. )
       END DO
       
       ! Time average viscosity
       !Oleg: visc_tail_thickness controlls the thickness of the in viscosity. The larger it is, the thiner is the tail. 
       !      we need to play with this parameter to make it as large as possible, yet have quality results.
       visc_tail_thickness = 2.0_pr
       q_wght = MIN( visc_tail_thickness*MAXVAL(c_comp*dt/MINVAL(h(j_lev,1:dim))),0.5_pr)
       CALL parallel_global_sum( REALMAXVAL=q_wght )
       
       tmpl = (MAXVAL(u(1:nlocal,n_visc)) > 0.0_pr)
       CALL parallel_global_sum( LOGICALOR=tmpl )
       IF(tmpl) THEN
          visc(1:nlocal) = MAX(exp(-q_wght)*u(1:nlocal,n_visc),visc)
          IF ( use_visc_zone ) visc(:) = visc(:) * set_visc_zone(nlocal)
          IF (imask_obstacle) visc(:) = visc(:)*(1.0_pr - penal) ! Nurlybek, make mask 0 inside the obstacle
       END IF

    END IF

    DO i=1,dim
       vMask(1:nlocal,i) = visc(1:nlocal)
    END DO

    CALL shift_by(vMask, phiTemp, nlocal, 1)
    v_for(1:nlocal,1:dim) = MAX(vMask(1:nlocal,1:dim), phiTemp(1:nlocal,1:dim))
    CALL shift_by(vMask, phiTemp, nlocal, -1)
    v_back(1:nlocal,1:dim) = MAX(vMask(1:nlocal,1:dim), phiTemp(1:nlocal,1:dim))

!08.15.2012, Oleg: I commented this, because it is non-conservative
!!$    ! Non-conservative formulation
!!$    DO i=1,dim
!!$       v_for(1:nlocal,i) = visc(1:nlocal)
!!$       v_back(1:nlocal,i) = visc(1:nlocal)
!!$    END DO

  END SUBROUTINE set_diffusion

  ! Create a visc zone by defining a bounding box.  There is no hyperbolic
  ! viscosity outside of the box
  FUNCTION set_visc_zone( nlocal )
    USE util_vars
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: nlocal
    REAL (pr), DIMENSION(nlocal) :: set_visc_zone
    INTEGER :: idim

    DO idim = 1,dim 
      WHERE( x(:,idim) .GT. visc_zone(2,idim) .OR. x(:,idim) .LT. visc_zone(1,idim) )  ! The boundary regions
        set_visc_zone = 0.0_pr 
      ELSEWHERE
        set_visc_zone = 1.0_pr  ! Apply hyperbolic viscosity only within the bounds 
      END WHERE
    END DO

  END FUNCTION set_visc_zone

  SUBROUTINE add_BC (nlocal, jlev)
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: jlev,nlocal
    INTEGER :: i, idim
    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc

    !--Go through all Boundary points that are specified
    i_p_face(0) = 1
    DO i=1,dim
       i_p_face(i) = i_p_face(i-1)*3
    END DO
    !NOTE: this conditions are not perfect, since they overrde each other and do not distinguish 
    !      between corners, edges, faces.  This can be modified in the future to make it more flexible
    !      using face_type information.  In this ces the input will hae to be modified.

    !****************************************************************************************************
    !Oleg: the input can be later extended to deal with corners, edges, faces, the set-up will be the same.
    IF(.NOT.ALLOCATED(face_wall_type)) THEN
       ALLOCATE(face_wall_type(0:3**dim-1,dim))
       face_wall_type = 0
       DO face_type = 0, 3**dim - 1
          face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
          IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
             DO idim=1,dim
                IF(face(idim) == -1) face_wall_type(face_type,idim) = min_wall_type(idim)
                IF(face(idim) ==  1) face_wall_type(face_type,idim) = max_wall_type(idim)
             END DO
          END IF
       END DO
    END IF
    !****************************************************************************************************

    !Oleg: the following senction is commented out because it has been repalced by the option not to 
    !      include wall-notmal derivative based on face_walt_tipe
!!$    DO face_type = 0, 3**dim - 1
!!$       face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
!!$       IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
!!$          CALL get_all_indices_by_face (face_type, jlev, nloc, iloc)
!!$          IF(nloc > 0 ) THEN 
!!$             DO idim=1,dim
!!$                IF( face(idim) == -1  ) THEN                          ! min face (entire face) 
!!$                   IF( min_wall_type(idim).NE.0 ) THEN
!!$                      v_back(iloc(1:nloc),idim) = 0.0_pr
!!$                      !                      v_for(iloc(1:nloc),idim) = 0.0_pr
!!$                   END IF
!!$                ELSE IF( face(idim) == 1  ) THEN                      ! max face (entire face) 
!!$                   IF( max_wall_type(idim).NE.0 ) THEN
!!$                      !                      v_back(iloc(1:nloc),idim) = 0.0_pr
!!$                      v_for(iloc(1:nloc),idim) = 0.0_pr
!!$                   END IF
!!$                END IF
!!$             END DO
!!$          END IF
!!$       END IF
!!$    END DO

  END SUBROUTINE add_BC

  SUBROUTINE add_obstacle (nwlt,mask)
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: nwlt
    REAL (pr), DIMENSION (nwlt) :: mask
    INTEGER :: idim

    !Oleg: this thing can be changed later similaryl to normal wall, but the normal vector inside the obstacle needs to be known.
    DO idim=1,dim
       v_back(1:nwlt,idim) = v_back(1:nwlt,idim)*(1.0_pr-mask(1:nwlt))
       v_for(1:nwlt,idim) = v_for(1:nwlt,idim)*(1.0_pr-mask(1:nwlt))
    END DO
    visc(1:nwlt) = visc(1:nwlt)*(1.0_pr-mask(1:nwlt))


  END SUBROUTINE add_obstacle

  SUBROUTINE hyper_post_process (u,n,neq,c)
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: n, neq
    REAL (pr), DIMENSION (n,neq), INTENT(INOUT) :: u !1D flat version of integrated variables without BC 
    REAL (pr), DIMENSION (n), INTENT(IN) :: c
    REAL (pr), DIMENSION (n,neq) :: u_new
    REAL (pr), DIMENSION (neq,ng,dim) :: du_back, du_for
    REAL (pr), DIMENSION (n) :: coef
    INTEGER :: meth
    REAL (pr), DIMENSION (n) :: highpass,delta_k, mask

  END SUBROUTINE hyper_post_process

  SUBROUTINE shift_by (phiFlux, newPhi, n, dir) !--Maintain Monotonicity
    IMPLICIT NONE
    INTEGER, INTENT (IN)  :: n, dir
    REAL (pr), DIMENSION (n, dim), INTENT (IN) :: phiFlux
    REAL (pr), DIMENSION (n, dim), INTENT (OUT) :: newPhi
    REAL (pr), DIMENSION (n,dim) :: du, d2u
    INTEGER :: i
    REAL (pr), DIMENSION(dim,n) :: h_arr

    CALL get_all_local_h (h_arr)

    ! Shift in positive direction
    IF ( dir == 1) THEN
       DO i=1,dim
          CALL c_diff_fast(phiFlux(:,i), du(:,:), d2u(:,:), j_lev, n, 4, 10, 1, 1, 1, FORCE_RECTILINEAR=.TRUE. ) 
          newPhi(:,i) = phiFlux(:,i) + h_arr(i,:)*du(:,i)
       END DO
    ELSE IF ( dir == -1 ) THEN
       DO i=1,dim
          CALL c_diff_fast(phiFlux(:,i), du(:,:), d2u(:,:), j_lev, n, 2, 10, 1, 1, 1, FORCE_RECTILINEAR=.TRUE. ) 
          newPhi(:,i) = phiFlux(:,i) - h_arr(i,:)*du(:,i)
       END DO
    ELSE
       newPhi = phiFlux
    END IF

  END SUBROUTINE shift_by

  SUBROUTINE hyper_filter(u, j_in, nlocal, filter_type)
    USE precision
    USE sizes
    USE wlt_vars
    !  USE share_vars
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: j_in, nlocal, filter_type
    REAL (pr), DIMENSION (1:nlocal), INTENT (INOUT) :: u
    REAL (pr), DIMENSION (1:nlocal) :: u_tmp
    INTEGER :: i,ii,j,k

    INTEGER, DIMENSION (0:dim) :: i_p
    INTEGER*8, DIMENSION (0:dim) :: i_p_nxyz
    INTEGER, DIMENSION (1:dim) :: ixyz,lxyz, ixyz_l, ixyz_h
    INTEGER :: j_df, wlt_type, face_type, step, jj
!!$    INTEGER, DIMENSION(:), ALLOCATABLE :: i_nbhd
!!$    INTEGER, DIMENSION(:,:), ALLOCATABLE :: ixyz_nbhd
!!$    REAL(pr), DIMENSION(:), ALLOCATABLE :: v_tmp
    
    INTEGER, DIMENSION(1:3**dim) :: i_nbhd
    INTEGER, DIMENSION(1:dim,1:3**dim) :: ixyz_nbhd
    REAL(pr), DIMENSION(1:3**dim) :: v_tmp

    
!!$    IF (.NOT. ALLOCATED(ixyz_nbhd)) ALLOCATE(ixyz_nbhd(1:dim,1:(2**(j_in-j_hyper+1)+1)**dim))
!!$    IF (.NOT. ALLOCATED(i_nbhd)) ALLOCATE(i_nbhd(1:(2**(j_in-j_hyper+1)+1)**dim))
!!$    IF (.NOT. ALLOCATED(v_tmp)) ALLOCATE(v_tmp(1:(2**(j_in-j_hyper+1)+1)**dim))

    u = ABS(u)

    !  ZERO = 0
    !  CALL get_indices_by_coordinate(1,ZERO,j_lev,i1,1) !preparing iwrk

    !---- Note, that u is assumed on level j_lev (highest level), i.e. j_in = j_lev
    IF(j_in /= j_lev) THEN
       PRINT *, 'ERROR: j_in /= j_lev'
       STOP
    END IF

    i_p(0) = 1
    i_p_nxyz(0) = 1
    DO i=1,dim
       i_p_nxyz(i) = i_p_nxyz(i-1)*(1+nxyz(i))
    END DO


!!$ u_tmp = 0.0_pr
!!$  testpoint: DO i=1,nlocal
!!$        ixyz(1:dim) = indx(i,1:dim)
!!$        jxyz=0
!!$        DO idim=1,dim
!!$           jxyz(idim) = ij2j(ixyz(idim), j_in)
!!$        END DO
!!$        j=MAXVAL(jxyz)
!!$        IF(j==j_in .AND. MINVAL(ixyz(1:dim)) > 3 .AND. MINVAL(nxyz(1:dim) - ixyz(1:dim)) > 3) THEN
!!$           WRITE(*,'("ixyz=",3(I8,1x))') ixyz(1:dim)
!!$           WRITE(*,'("Do you want to test this point? (1/0)",$)')
!!$           READ *, ii
!!$           IF(ii == 1) THEN
!!$              u_tmp(i)=1.0_pr
!!$              ix = i
!!$              print*, 'test point on level ',j, j_in
!!$              exit testpoint
!!$           END IF
!!$        END IF
!!$     END DO testpoint

    !******************* Filtering  ******************************************

    !---------- filtering only on levels j>=j_J_hyper
    DO jj = j_in, J_hyper, -1
       u_tmp =  u
       ! Filter wavelets on j<jj levels of resolution
       DO j = 1,jj-1
          DO wlt_type = MIN(j-1,1),2**dim-1
             DO face_type = 0, 3**dim - 1
                DO j_df = j, j_in
                   DO k = 1, indx_DB(j_df,wlt_type,face_type,j)%length  
                      u_tmp(indx_DB(j_df,wlt_type,face_type,j)%p(k)%i)=0.0_pr
                   END DO
                END DO
             END DO
          END DO
       END DO
       step = 2**(j_in-jj)
       DO j = jj, 1, -1
          DO wlt_type = MIN(j-1,1),2**dim-1
             DO face_type = 0, 3**dim - 1
                DO j_df = j, j_in
                   DO k = 1, indx_DB(j_df,wlt_type,face_type,j)%length  
                      i = indx_DB(j_df,wlt_type,face_type,j)%p(k)%i
                      ixyz(1:dim) = INT(MOD(indx_DB(j_df,wlt_type,face_type,j)%p(k)%ixyz-1,i_p_nxyz(1:dim))/i_p_nxyz(0:dim-1))
                      ! check if point on the boundary
                      ixyz_l=0; ixyz_h=0
                      ixyz_l(1:dim) = MAX(0,ixyz(1:dim)-step)
                      ixyz_h(1:dim) = MIN(ixyz(1:dim)+step, nxyz(1:dim))
                      ixyz_l(1:dim) = (1-prd(1:dim))*ixyz_l(1:dim) + prd(1:dim)*(ixyz(1:dim)-step)
                      ixyz_h(1:dim) = (1-prd(1:dim))*ixyz_h(1:dim) + prd(1:dim)*(ixyz(1:dim)+step)
                      
                      i_p(1:dim) = ( ixyz_h(1:dim) - ixyz_l(1:dim) )/step + 1
                      DO ii=1,dim
                         i_p(ii) = i_p(ii-1)*i_p(ii)
                      END DO
                      
                      DO ii=1,i_p(dim)
                         lxyz(1:dim) = INT(MOD(ii-1,i_p(1:dim))/i_p(0:dim-1))*step+ixyz_l(1:dim)                 
                         ixyz_nbhd(1:dim,ii)=(1-prd(1:dim))*lxyz(1:dim) &
                              + prd(1:dim)*MOD(lxyz(1:dim)+9*nxyz(1:dim),nxyz(1:dim))
                      END DO
                      CALL get_indices_by_coordinate(i_p(dim),ixyz_nbhd,j_lev,i_nbhd,0)
                      FORALL(ii = 1:i_p(dim), i_nbhd(ii)/=0) v_tmp(ii) = u_tmp(i_nbhd(ii))
                      u(i) = MAXVAL(v_tmp(1:i_p(dim)),1,i_nbhd(1:i_p(dim))/=0)
                   END DO
                END DO
             END DO
          END DO
       END DO
    END DO
    
!!$  DO i=1,nlocal
!!$     IF(u(i) > 0.0_pr) THEN
!!$        PRINT *, 'ixyz=', indx(i,1:dim), u(i)
!!$     END IF
!!$  END DO
    
    !******************** Cleaning iwrk ***************************************
    ! CALL get_indices_by_coordinate(1,ZERO,j_lev,i1,-1) !cleaning the iwrk
    !**************************************************************************
  END SUBROUTINE hyper_filter

  SUBROUTINE hyperbolic(uh, ng, rhs)
    USE parallel
    IMPLICIT NONE
    INTEGER :: ng, ieq,idim
    REAL (pr), DIMENSION (ng,n_integrated) :: uh
    REAL (pr), DIMENSION (ng*n_integrated), INTENT (INOUT) :: rhs
    REAL (pr), DIMENSION (ng,n_integrated) :: F
    REAL (pr), DIMENSION (n_integrated,ng,dim) :: du_back, du_for, d2u
    INTEGER :: shift, i
    REAL (pr), DIMENSION(dim,ng) :: h_arr
    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc
    
    !--Go through all Boundary points that are specified
    i_p_face(0) = 1
    DO i=1,dim
       i_p_face(i) = i_p_face(i-1)*3
    END DO

    IF (hyper_mode == 1) THEN
       F(1:ng,1:n_integrated) = uh(1:ng,1:n_integrated)
       CALL c_diff_fast(F(1:ng,1:n_integrated), du_back(1:n_integrated,1:ng,1:dim), d2u(1:n_integrated,1:ng,1:dim), j_lev, ng, 2, 10, n_integrated, 1, n_integrated, FORCE_RECTILINEAR=.TRUE. ) 
       CALL c_diff_fast(F(1:ng,1:n_integrated),du_for(1:n_integrated,1:ng,1:dim),d2u(1:n_integrated,1:ng,1:dim), j_lev, ng, 4, 10, n_integrated, 1, n_integrated, FORCE_RECTILINEAR=.TRUE. ) 

       DO face_type = 0, 3**dim - 1
          face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
          CALL get_all_indices_by_face (face_type, j_lev, nloc, iloc)
          IF(nloc > 0 ) THEN 
             DO ieq=1,n_integrated
                IF(n_var_hyper_active(ieq)) THEN
                   shift=(ieq-1)*ng
                   DO idim=1,dim
                      rhs(shift+iloc(1:nloc)) = rhs(shift+iloc(1:nloc)) + (1.0_pr-face_wall_type(face_type,idim)) * &
                                            ( - 0.5_pr*v_back(iloc(1:nloc),idim)*du_back(ieq,iloc(1:nloc),idim) &
                                              + 0.5_pr*v_for(iloc(1:nloc),idim)*du_for(ieq,iloc(1:nloc),idim) )
                   END DO
                END IF
             END DO
          END IF
       END DO
    ELSE ! Non-conservative formulation: visc -> visc * h(i)
       F(1:ng,1:n_integrated) = uh(1:ng,1:n_integrated)
       CALL c_diff_fast(F(1:ng,1:n_integrated), du_back(1:n_integrated,1:ng,1:dim), &
            d2u(1:n_integrated,1:ng,1:dim), j_lev, ng, 0, 01, n_integrated, 1, n_integrated, FORCE_RECTILINEAR=.TRUE. ) 
       DO face_type = 0, 3**dim - 1
          face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
          CALL get_all_indices_by_face (face_type, j_lev, nloc, iloc)
          IF(nloc > 0 ) THEN 
             DO ieq=1,n_integrated
                IF(n_var_hyper_active(ieq)) THEN
                   shift=(ieq-1)*ng
                   DO idim=1,dim
                      rhs(shift+iloc(1:nloc)) = rhs(shift+iloc(1:nloc)) + (1.0_pr-face_wall_type(face_type,idim)) * &
                                                0.5_pr*h(j_lev,idim)*visc(iloc(1:nloc))*d2u(ieq,iloc(1:nloc),idim)
                   END DO
                END IF
             END DO
          END IF
       END DO
    END IF
  END SUBROUTINE hyperbolic

  SUBROUTINE hyperbolic_diag(rhs, ng )
    IMPLICIT NONE
    INTEGER :: ng, ieq, idim
    REAL (pr), DIMENSION (ng*n_integrated), INTENT (INOUT) :: rhs
    REAL (pr), DIMENSION (ng,dim) :: du_back, du_for, d2u
    INTEGER :: shift
    REAL (pr), DIMENSION(dim,ng) :: h_arr

    IF (hyper_mode == 1) THEN
       CALL c_diff_diag(du_back(1:ng,1:dim),d2u(1:ng,1:dim),j_lev,ng,2,2,10, FORCE_RECTILINEAR=.TRUE. ) 
       CALL c_diff_diag(du_for(1:ng,1:dim),d2u(1:ng,1:dim),j_lev,ng,4,4,10, FORCE_RECTILINEAR=.TRUE. ) 

       DO ieq=1,n_integrated
          IF(n_var_hyper_active(ieq)) THEN
             DO idim=1,dim
                shift=(ieq-1)*ng
                rhs(shift+1:shift+ng) = rhs(shift+1:shift+ng) &
                     - 0.5_pr*v_back(1:ng,idim)*du_back(1:ng,idim) &
                     + 0.5_pr*v_for(1:ng,idim)*du_for(1:ng,idim)
             END DO
          END IF
       END DO
    ELSE ! Non-conservative formulation

       CALL c_diff_diag(du_back(1:ng,1:dim),d2u(1:ng,1:dim),j_lev,ng,0,0,01, FORCE_RECTILINEAR=.TRUE. ) 

       DO ieq=1,n_integrated
          IF(n_var_hyper_active(ieq)) THEN
             DO idim=1,dim
                shift=(ieq-1)*ng
                rhs(shift+1:shift+ng) = rhs(shift+1:shift+ng) &
                     + 0.5_pr*h(j_lev,idim)*visc(1:ng)*d2u(1:ng,idim)
             END DO
          END IF
       END DO

    END IF

  END SUBROUTINE hyperbolic_diag

END MODULE hyperbolic_solver
