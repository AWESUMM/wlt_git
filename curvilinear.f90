! This module provides general coordinate system transforms and operations
MODULE curvilinear
  USE precision
  USE user_case_db
  PUBLIC
  REAL (pr), DIMENSION (:,:,:), ALLOCATABLE ::  curvilinear_coordgrad ! curvilinear_coordgrad(i,j,:) = d x_i/d xi_j
  REAL (pr), DIMENSION (:,:,:), ALLOCATABLE ::  curvilinear_jacobian  ! curvilinear_jacobian(i,j,:)  = d xi_i/d x_j
  INTEGER, DIMENSION ( max_dim )            ::  transform_dir
  INTEGER, PARAMETER :: MESH_STRETCHING_PHYS = 0, & ! based on d x_i/d xi_j (coordgrad)
                        MESH_STRETCHING_COMP = 1    ! based on d xi_j/d x_i (Jacobian)

  PUBLIC :: calc_jacobian_determinant

  CONTAINS 

  FUNCTION calc_jacobian_determinant( jacobian_matrix, dim_local, nlocal )
    IMPLICIT NONE 
    INTEGER, INTENT(IN) :: dim_local, nlocal
    REAL (pr), DIMENSION(1:dim_local,1:dim_local,1:nlocal) :: jacobian_matrix
    REAL (pr), DIMENSION(1:nlocal)             :: calc_jacobian_determinant 
    
    calc_jacobian_determinant(:) = 1.0_pr 
    ! Apply curvilinear transform by multiplying by the jacobian determinant - move this and everything below out into util_3d.f90 for generality
    IF( ANY( transform_dir(1:dim_local) .EQ. 1 ) ) THEN 
      IF ( dim_local .EQ. 3 ) THEN
        calc_jacobian_determinant(:) = (   jacobian_matrix(1,1,:) * ( jacobian_matrix(2,2,:) * jacobian_matrix(3,3,:) - jacobian_matrix(2,3,:) * jacobian_matrix(3,2,:) ) &
                                         - jacobian_matrix(1,2,:) * ( jacobian_matrix(2,1,:) * jacobian_matrix(3,3,:) - jacobian_matrix(2,3,:) * jacobian_matrix(3,1,:) ) &
                                         + jacobian_matrix(1,3,:) * ( jacobian_matrix(2,1,:) * jacobian_matrix(3,2,:) - jacobian_matrix(2,2,:) * jacobian_matrix(3,1,:) ) )
      ELSE IF ( dim_local .EQ. 2 ) THEN
        calc_jacobian_determinant(:) = (   jacobian_matrix(1,1,:) * jacobian_matrix(2,2,:) &
                                         - jacobian_matrix(1,2,:) * jacobian_matrix(2,1,:) )
      ELSE IF ( dim_local .EQ. 1 ) THEN
        calc_jacobian_determinant(:) = jacobian_matrix(1,1,:)
      END IF
    END IF    

  END FUNCTION calc_jacobian_determinant


END MODULE curvilinear
