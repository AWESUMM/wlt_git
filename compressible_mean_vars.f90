MODULE compressible_mean_vars  

  USE precision
  USE equations_compressible

  INTEGER,              PROTECTED :: &
    n_den_avg   = 0, &       ! Density Reynolds Average
    n_den_flt_rms   = 0, &   ! Density Reynolds Fluctuation root mean square
    n_eng_avg   = 0, &       ! Energy Reynolds Average
    n_eng_flt_rms   = 0, &      ! Energy Reynolds Fluctuation root mean square
    n_prs_avg   = 0, &       ! Pressure Reynolds Average
    n_prs_flt_rms   = 0, &   ! Pressure Reynolds Fluctuation root mean square
    n_temp_avg   = 0, &      ! Temperature Reynolds Average
    n_temp_flt_rms   = 0  ! Temperature Reynolds Fluctuation root mean square
  INTEGER, ALLOCATABLE, PROTECTED :: &
    n_mom_avg(:), &          ! Momentum vector Reynolds Average
    n_mom_flt(:), &          ! Momemtum vector Reynolds Fluctuation components
    n_spc_avg(:), &          ! Species mass fraction array
    n_spc_flt_rms(:), &      ! Species mass fraction array
    n_stress_avg(:)          ! Reynolds stresses

  INTEGER, PRIVATE :: adaptmom_avg = 0, adaptden_avg = 0, adapteng_avg = 0, adaptspc_avg = 0, adaptprs_avg = 0, adapttmp_avg = 0
  INTEGER, PRIVATE :: adaptmom_flt = 0, adaptden_flt = 0, adapteng_flt = 0, adaptspc_flt = 0, adaptprs_flt = 0, adapttmp_flt = 0

  REAL (pr), PRIVATE :: momScale_avg=1.0_pr, denScale_avg=1.0_pr, engScale_avg=1.0_pr, specScale_avg=1.0_pr, prsScale_avg=1.0_pr, tempScale_avg=1.0_pr
  REAL (pr), PRIVATE :: momScale_flt=1.0_pr, denScale_flt=1.0_pr, engScale_flt=1.0_pr, specScale_flt=1.0_pr, prsScale_flt=1.0_pr, tempScale_flt=1.0_pr

  LOGICAL, PROTECTED :: saveReyAvgInt = .FALSE.   ! Save Reynolds averaged integrated variables
  LOGICAL, PROTECTED :: saveReyAvgFlt = .FALSE.   ! Save Reynolds averaged fluctuation variables
  LOGICAL, PROTECTED :: saveReyAvgPrs = .FALSE.   ! Save Reynolds averaged pressure
  LOGICAL, PROTECTED :: saveReyAvgTmp = .FALSE.   ! Save Reynolds averaged temperature
  LOGICAL, PROTECTED :: saveReyStrRsv = .FALSE.   ! Save resolved Reynolds stresses
  LOGICAL, PROTECTED :: saveReyStrMdl = .FALSE.   ! Save modeled Reynolds stresses (used in SGS and URANS modules)
  LOGICAL, PROTECTED :: saveReyAvgRANS = .FALSE.  ! Save Reynolds averaged URANS variables (used in URANS modules)

  REAL (pr), PROTECTED :: avg_timestart = 0.0_pr, avg_timescale
  LOGICAL, PROTECTED   :: restart_ReyAvg = .FALSE., exp_avg = .FALSE.

  PUBLIC :: &
    compressible_mean_read_input, &
    compressible_setup_mean, &
    compressible_finalize_setup_mean, &
    compressible_cal_mean, &
    compressible_mean_scales

CONTAINS

  SUBROUTINE compressible_mean_read_input()
    IMPLICIT NONE
    LOGICAL :: fail

    call input_logical ('saveReyAvgInt',saveReyAvgInt,'stop',         'saveReyAvgInt: if T adds and saves Reynolds Averaged Integrated vars')
    call input_logical ('saveReyAvgFlt',saveReyAvgFlt,'stop',         'saveReyAvgFlt: if T adds and saves Reynolds Fluctuation vars')
    call input_logical ('saveReyAvgPrs',saveReyAvgPrs,'stop',         'saveReyAvgPrs: if T adds and saves Reynolds averaged pressure                  ')
    IF (savetmp)  call input_logical ('saveReyAvgTmp',saveReyAvgTmp,'stop',         'saveReyAvgTmp: if T adds and saves Reynolds averaged temperature               ')
    call input_logical ('saveReyStrRsv',saveReyStrRsv,'stop',         'saveReyStrRsv: if T adds and saves resolved Reynolds stresses                  ')
    call input_logical ('saveReyStrMdl',saveReyStrMdl,'stop',         'saveReyStrMdl: if T adds and saves modeled Reynolds stresses                   ')
    call input_logical ('saveReyAvgRANS',saveReyAvgRANS,'stop',      'saveReyAvgRANS: if T adds and saves Reynolds averaged URANS variables           ')

    call input_real    ('avg_timestart',avg_timestart,'stop',       '   avg_timestart: starting time for Reynolds averaging') 
    call input_logical ('restart_ReyAvg',restart_ReyAvg,'stop',       '   restart_ReyAvg: restart Reynolds averaging') 
    call input_logical ('exp_avg',exp_avg,'default',       '   exp_avg: if T uses exponential moving average (Reynolds Avg)')
    IF (exp_avg) call input_real    ('avg_timescale',avg_timescale,'stop',       '   avg_timescale: exponential moving average timescale (Reynolds Avg)')

    call input_integer ('adaptden_avg',adaptden_avg,'default',         'adapt on density avg: 0 - off, 1 - const scale, 2 - dynamic scale')
    call input_integer ('adaptden_flt',adaptden_flt,'default',         'adapt on density flt: 0 - off, 1 - const scale, 2 - dynamic scale')
    call input_real    ('denScale_avg',denScale_avg,'default',         '   denScale_avg: scale/coefficient for density avg')
    call input_real    ('denScale_flt',denScale_flt,'default',         '   denScale_flt: scale/coefficient for density flt')
    call input_integer ('adaptmom_avg',adaptmom_avg,'default',         'adapt on momentum avg: 0 - off, 1 - const scale, 2 - dynamic scale')
    call input_integer ('adaptmom_flt',adaptmom_flt,'default',         'adapt on momentum flt: 0 - off, 1 - const scale, 2 - dynamic scale')
    call input_real    ('momScale_avg',momScale_avg,'default',         '   momScale_avg: scale/coefficient for momentum avg')
    call input_real    ('momScale_flt',momScale_flt,'default',         '   momScale_flt: scale/coefficient for momentum flt')
    call input_integer ('adapteng_avg',adapteng_avg,'default',         'adapt on total engery avg: 0 - off, 1 - const scale, 2 - dynamic scale')
    call input_integer ('adapteng_flt',adapteng_flt,'default',         'adapt on total engery flt: 0 - off, 1 - const scale, 2 - dynamic scale')
    call input_real    ('engScale_avg',engScale_avg,'default',         '   engScale_avg: scale/coefficient for total energy avg')
    call input_real    ('engScale_flt',engScale_flt,'default',         '   engScale_flt: scale/coefficient for total energy flt')
    call input_integer ('adaptspc_avg',adaptspc_avg,'default',         'adapt on species avg: 0 - off, 1 - const scale, 2 - dynamic scale')
    call input_integer ('adaptspc_flt',adaptspc_flt,'default',         'adapt on species flt: 0 - off, 1 - const scale, 2 - dynamic scale')
    call input_real   ('spcScale_avg',specScale_avg,'default',         '   specScale_avg: scale/coefficient for species avg')
    call input_real   ('spcScale_flt',specScale_flt,'default',         '   specScale_flt: scale/coefficient for species flt')
    call input_integer ('adaptprs_avg',adaptprs_avg,'default',         'adapt on pressure avg: 0 - off, 1 - const scale, 2 - dynamic scale')
    call input_integer ('adaptprs_flt',adaptprs_flt,'default',         'adapt on pressure flt: 0 - off, 1 - const scale, 2 - dynamic scale')
    call input_real    ('prsScale_avg',prsScale_avg,'default',         '   prsScale_avg: scale/coefficient for pressure avg')
    call input_real    ('prsScale_flt',prsScale_flt,'default',         '   prsScale_flt: scale/coefficient for pressure flt')
    call input_integer ('adapttmp_avg',adapttmp_avg,'default',         'adapt on temperature avg: 0 - off, 1 - const scale, 2 - dynamic scale')
    call input_integer ('adapttmp_flt',adapttmp_flt,'default',         'adapt on temperature flt: 0 - off, 1 - const scale, 2 - dynamic scale')
    call input_real  ('tempScale_avg',tempScale_avg,'default',         '   tempScale_avg: scale/coefficient for temperature avg')
    call input_real  ('tempScale_flt',tempScale_flt,'default',         '   tempScale_flt: scale/coefficient for temperature flt')

    ! Sanity checks
    fail = .FALSE.
    IF ( adaptden_avg .LT. 0 .OR. adaptden_avg .GT. 2 )     fail = .TRUE. 
    IF ( adaptmom_avg .LT. 0 .OR. adaptmom_avg .GT. 2 )     fail = .TRUE. 
    IF ( adapteng_avg .LT. 0 .OR. adapteng_avg .GT. 2 )     fail = .TRUE. 
    IF ( adaptspc_avg .LT. 0 .OR. adaptspc_avg .GT. 2 )     fail = .TRUE. 
    IF ( adaptprs_avg .LT. 0 .OR. adaptprs_avg .GT. 2 )     fail = .TRUE. 
    IF ( adapttmp_avg .LT. 0 .OR. adapttmp_avg .GT. 2 )     fail = .TRUE. 
    IF ( adaptden_flt .LT. 0 .OR. adaptden_flt .GT. 2 )     fail = .TRUE. 
    IF ( adaptmom_flt .LT. 0 .OR. adaptmom_flt .GT. 2 )     fail = .TRUE. 
    IF ( adapteng_flt .LT. 0 .OR. adapteng_flt .GT. 2 )     fail = .TRUE. 
    IF ( adaptspc_flt .LT. 0 .OR. adaptspc_flt .GT. 2 )     fail = .TRUE. 
    IF ( adaptprs_flt .LT. 0 .OR. adaptprs_flt .GT. 2 )     fail = .TRUE. 
    IF ( adapttmp_flt .LT. 0 .OR. adapttmp_flt .GT. 2 )     fail = .TRUE. 
    IF ( fail ) THEN
       IF (par_rank .EQ. 0) PRINT *, "ERROR: bad value for one of the adapt flags.  Pleach check that all are between 0 and 2.  Exiting..."
       CALL parallel_finalize()
       STOP
    END IF
  END SUBROUTINE compressible_mean_read_input

  SUBROUTINE compressible_setup_mean
    USE variable_mapping
    IMPLICIT NONE
    INTEGER :: i, l   !4extra
    CHARACTER(LEN=8) :: specnum  !4extra
    CHARACTER (LEN=u_variable_name_len) :: specname 
    LOGICAL :: tmp_adapt, tmp_save

    ! Reynolds Averaged integrated variables
    IF (saveReyAvgInt ) THEN

      ! Density AVG
      tmp_adapt = (adaptden_avg .GT. 0) 
      CALL register_var( 'Density_rho_AVG  ',  integrated=.FALSE.,  adapt=(/tmp_adapt,tmp_adapt/),   interpolate=(/.TRUE.,.TRUE./), &
          exact=(/.FALSE.,.FALSE./),   saved=.TRUE., req_restart=.NOT.restart_ReyAvg )

      ! Momentum AVG
      tmp_adapt = (adaptmom_avg .GT. 0) .AND. .NOT. adaptvelonly
      CALL register_var( 'X_Momentum_AVG  ',  integrated=.FALSE.,  adapt=(/tmp_adapt,tmp_adapt/),   interpolate=(/.TRUE.,.TRUE./), &  
          exact=(/.FALSE.,.FALSE./),   saved=.TRUE., req_restart=.NOT.restart_ReyAvg )
      IF( dim .GE. 2 ) CALL register_var( 'Y_Momentum_AVG  ',  integrated=.FALSE.,  adapt=(/tmp_adapt,tmp_adapt/),   interpolate=(/.TRUE.,.TRUE./), &  
          exact=(/.FALSE.,.FALSE./),   saved=.TRUE., req_restart=.NOT.restart_ReyAvg )
      IF( dim .GE. 3 ) CALL register_var( 'Z_Momentum_AVG  ',  integrated=.FALSE.,  adapt=(/tmp_adapt,tmp_adapt/),   interpolate=(/.TRUE.,.TRUE./), &
          exact=(/.FALSE.,.FALSE./),   saved=.TRUE., req_restart=.NOT.restart_ReyAvg )

      ! Energy AVG
      tmp_adapt = (adapteng_avg .GT. 0) .AND. .NOT. adaptengonly
      CALL register_var( 'E_Total_AVG  ',  integrated=.FALSE.,  adapt=(/tmp_adapt,tmp_adapt/),    interpolate=(/.TRUE.,.TRUE./), &
          exact=(/.FALSE.,.FALSE./),   saved=.TRUE., req_restart=.NOT.restart_ReyAvg )

      ! Species AVG
      tmp_adapt = (adaptspc_avg .GT. 0) .AND. .NOT. adaptspconly
      IF (Nspec>1) THEN
        DO l=1,Nspecm      
            WRITE (specnum,'(I8)') l          
            WRITE (specname, u_variable_names_fmt) TRIM('Species_Scalar_'//ADJUSTL(TRIM(specnum)))//'_AVG  '
            CALL register_var( specname,  integrated=.FALSE.,  adapt=(/tmp_adapt,tmp_adapt/),   interpolate=(/.TRUE.,.TRUE./), &
                exact=(/.FALSE.,.FALSE./),   saved=.TRUE., req_restart=.NOT.restart_ReyAvg )
        END DO
      END IF

      IF (saveReyAvgFlt ) THEN

        ! Density FLT root mean square
        tmp_adapt = (adaptden_flt .GT. 0) 
        CALL register_var( 'Density_rho_FLT_RMS  ',  integrated=.FALSE.,  adapt=(/tmp_adapt,tmp_adapt/),   interpolate=(/.TRUE.,.TRUE./), &
            exact=(/.FALSE.,.FALSE./),   saved=.TRUE., req_restart=.NOT.restart_ReyAvg )

        ! Momentum FLT
        tmp_adapt = (adaptmom_flt .GT. 0) .AND. .NOT. adaptvelonly
        CALL register_var( 'X_Momentum_FLT  ',  integrated=.FALSE.,  adapt=(/tmp_adapt,tmp_adapt/),   interpolate=(/.TRUE.,.TRUE./), &  
            exact=(/.FALSE.,.FALSE./),   saved=.TRUE., req_restart=.NOT.restart_ReyAvg )
        IF( dim .GE. 2 ) CALL register_var( 'Y_Momentum_FLT  ',  integrated=.FALSE.,  adapt=(/tmp_adapt,tmp_adapt/),   interpolate=(/.TRUE.,.TRUE./), &  
            exact=(/.FALSE.,.FALSE./),   saved=.TRUE., req_restart=.NOT.restart_ReyAvg )
        IF( dim .GE. 3 ) CALL register_var( 'Z_Momentum_FLT  ',  integrated=.FALSE.,  adapt=(/tmp_adapt,tmp_adapt/),   interpolate=(/.TRUE.,.TRUE./), &
            exact=(/.FALSE.,.FALSE./),   saved=.TRUE., req_restart=.NOT.restart_ReyAvg )

        ! Energy FLT root mean square
        tmp_adapt = (adapteng_flt .GT. 0) .AND. .NOT. adaptengonly
        CALL register_var( 'E_Total_FLT_RMS  ',  integrated=.FALSE.,  adapt=(/tmp_adapt,tmp_adapt/),   interpolate=(/.TRUE.,.TRUE./), &
            exact=(/.FALSE.,.FALSE./),   saved=.TRUE., req_restart=.NOT.restart_ReyAvg )

        ! Species FLT root mean square
        tmp_adapt = (adaptspc_flt .GT. 0) .AND. .NOT. adaptspconly
        IF (Nspec>1) THEN
          DO l=1,Nspecm      
              WRITE (specnum,'(I8)') l          
              WRITE (specname, u_variable_names_fmt) TRIM('Species_Scalar_'//ADJUSTL(TRIM(specnum)))//'_FLT_RMS  '
              CALL register_var( specname,  integrated=.FALSE.,  adapt=(/tmp_adapt,tmp_adapt/),   interpolate=(/.TRUE.,.TRUE./), &
                  exact=(/.FALSE.,.FALSE./),   saved=.TRUE., req_restart=.NOT.restart_ReyAvg )
          END DO
        END IF

      END IF ! saveReyAvgFlt

    END IF ! saveReyAvgInt

    ! <p> - pressrue
    IF (saveReyAvgPrs ) THEN
      ! Pressure AVG
      tmp_adapt = (adaptprs_avg .GT. 0) 
      CALL register_var( 'Pressure_AVG  ',  integrated=.FALSE.,  adapt=(/tmp_adapt,tmp_adapt/),   interpolate=(/.TRUE.,.TRUE./), &
          exact=(/.FALSE.,.FALSE./),   saved=.TRUE., req_restart=.NOT.restart_ReyAvg )

      IF (saveReyAvgFlt ) THEN
        ! Pressure FLT mean square
        tmp_adapt = (adaptprs_flt .GT. 0) 
        CALL register_var( 'Pressure_FLT_RMS  ',  integrated=.FALSE.,  adapt=(/tmp_adapt,tmp_adapt/),   interpolate=(/.TRUE.,.TRUE./), &
            exact=(/.FALSE.,.FALSE./),   saved=.TRUE., req_restart=.NOT.restart_ReyAvg )
      END IF ! saveReyAvgFlt
      
    END IF ! saveReyAvgPrs

    ! <T> - temperature
    IF (savetmp) THEN 
    IF (saveReyAvgTmp ) THEN
      ! Temperature AVG
      tmp_adapt = (adapttmp_avg .GT. 0) 
      CALL register_var( 'Temperature_AVG  ',  integrated=.FALSE.,  adapt=(/tmp_adapt,tmp_adapt/),   interpolate=(/.TRUE.,.TRUE./), &
          exact=(/.FALSE.,.FALSE./),   saved=.TRUE., req_restart=.NOT.restart_ReyAvg )

      IF (saveReyAvgFlt ) THEN
        ! Temperature FLT mean square
        tmp_adapt = (adapttmp_flt .GT. 0) 
        CALL register_var( 'Temperature_FLT_RMS  ',  integrated=.FALSE.,  adapt=(/tmp_adapt,tmp_adapt/),   interpolate=(/.TRUE.,.TRUE./), &
            exact=(/.FALSE.,.FALSE./),   saved=.TRUE., req_restart=.NOT.restart_ReyAvg )
      END IF ! saveReyAvgFlt
      
    END IF ! saveReyAvgTmp
    END IF ! savetmp

    ! <rho*u_i*u_j> - Reynolds stresses
    IF (saveReyStrRsv ) THEN
      CALL register_var( 'XX_Stress_AVG  ',  integrated=.FALSE.,  adapt=(/.FALSE.,.FALSE./),   interpolate=(/.TRUE.,.TRUE./), &  
          exact=(/.FALSE.,.FALSE./),   saved=.TRUE., req_restart=.NOT.restart_ReyAvg )
      IF( dim .GE. 2 ) THEN
        CALL register_var( 'XY_Stress_AVG  ',  integrated=.FALSE.,  adapt=(/.FALSE.,.FALSE./),   interpolate=(/.TRUE.,.TRUE./), &  
            exact=(/.FALSE.,.FALSE./),   saved=.TRUE., req_restart=.NOT.restart_ReyAvg )
        CALL register_var( 'YY_Stress_AVG  ',  integrated=.FALSE.,  adapt=(/.FALSE.,.FALSE./),   interpolate=(/.TRUE.,.TRUE./), &  
            exact=(/.FALSE.,.FALSE./),   saved=.TRUE., req_restart=.NOT.restart_ReyAvg )
      END IF
      IF( dim .GE. 3 ) THEN
        CALL register_var( 'XZ_Stress_AVG  ',  integrated=.FALSE.,  adapt=(/.FALSE.,.FALSE./),   interpolate=(/.TRUE.,.TRUE./), &  
            exact=(/.FALSE.,.FALSE./),   saved=.TRUE., req_restart=.NOT.restart_ReyAvg )
        CALL register_var( 'YZ_Stress_AVG  ',  integrated=.FALSE.,  adapt=(/.FALSE.,.FALSE./),   interpolate=(/.TRUE.,.TRUE./), &  
            exact=(/.FALSE.,.FALSE./),   saved=.TRUE., req_restart=.NOT.restart_ReyAvg )
        CALL register_var( 'ZZ_Stress_AVG  ',  integrated=.FALSE.,  adapt=(/.FALSE.,.FALSE./),   interpolate=(/.TRUE.,.TRUE./), &  
            exact=(/.FALSE.,.FALSE./),   saved=.TRUE., req_restart=.NOT.restart_ReyAvg )
      END IF
    END IF ! saveReyStrRsv

  END SUBROUTINE compressible_setup_mean

  SUBROUTINE compressible_finalize_setup_mean
    USE variable_mapping
    IMPLICIT NONE
    INTEGER :: i, l   !4extra
    CHARACTER(LEN=8) :: specnum  !4extra
    CHARACTER (LEN=u_variable_name_len) :: specname 

    ! Allocate variable indices
    IF (ALLOCATED(n_mom_avg)) DEALLOCATE(n_mom_avg)
    IF (ALLOCATED(n_mom_flt)) DEALLOCATE(n_mom_flt)
    IF (ALLOCATED(n_spc_avg)) DEALLOCATE(n_spc_avg)
    IF (ALLOCATED(n_spc_flt_rms)) DEALLOCATE(n_spc_flt_rms)
    ALLOCATE( n_mom_avg(dim) )
    ALLOCATE( n_mom_flt(dim) )
    IF( Nspec .GT. 1 ) ALLOCATE(n_spc_avg(Nspecm), n_spc_flt_rms(Nspecm))  ! Only allocate species fields if the total number of species is greater than one

    ! Integrated Variables - Reynolds averaging 
    n_den_avg                     = get_index('Density_rho_AVG') 
    n_den_flt_rms                 = get_index('Density_rho_FLT_RMS') 
    n_mom_avg(1)                  = get_index('X_Momentum_AVG') 
    IF( dim .GE. 2 )n_mom_avg(2)  = get_index('Y_Momentum_AVG') 
    IF( dim .GE. 3 )n_mom_avg(3)  = get_index('Z_Momentum_AVG') 
    n_mom_flt(1)                  = get_index('X_Momentum_FLT') 
    IF( dim .GE. 2 )n_mom_flt(2)  = get_index('Y_Momentum_FLT') 
    IF( dim .GE. 3 )n_mom_flt(3)  = get_index('Z_Momentum_FLT') 
    n_eng_avg = get_index('E_Total_AVG') 
    n_eng_flt_rms = get_index('E_Total_FLT_RMS') 
    IF (Nspec>1) THEN
      DO i=1,Nspecm
        WRITE (specnum,'(I8)') l          
        WRITE (specname, u_variable_names_fmt) TRIM('Species_Scalar_'//ADJUSTL(TRIM(specnum)))//'_AVG '
        n_spc_avg(i) = get_index(specname) 
        WRITE (specname, u_variable_names_fmt) TRIM('Species_Scalar_'//ADJUSTL(TRIM(specnum)))//'_FLT_RMS '
        n_spc_flt_rms(i) = get_index(specname) 
      END DO
    END IF

    IF (adaptden_avg .GT. 0) scaleCoeff(n_den_avg)    = denScale_avg
    IF (adaptden_flt .GT. 0) scaleCoeff(n_den_flt_rms)    = denScale_flt

    IF (adaptmom_avg .GT. 0) scaleCoeff(n_mom_avg(1):n_mom_avg(dim))    = momScale_avg
    IF (adaptmom_flt .GT. 0) scaleCoeff(n_mom_flt(1):n_mom_flt(dim))    = momScale_flt

    IF (adapteng_avg .GT. 0) scaleCoeff(n_eng_avg)    = engScale_avg
    IF (adapteng_flt .GT. 0) scaleCoeff(n_eng_flt_rms)    = engScale_flt

    IF (adaptspc_avg .GT. 0 .AND. Nspec .GT. 1) scaleCoeff(n_spc_avg(1):n_spc_avg(Nspecm)) = specScale_avg
    IF (adaptspc_flt .GT. 0 .AND. Nspec .GT. 1) scaleCoeff(n_spc_flt_rms(1):n_spc_flt_rms(Nspecm)) = specScale_flt

    ! Pressure
    n_prs_avg                  = get_index('Pressure_AVG') 
    n_prs_flt_rms               = get_index('Pressure_FLT_RMS') 
    IF (adaptprs_avg .GT. 0) scaleCoeff(n_prs_avg)    = prsScale_avg
    IF (adaptprs_flt .GT. 0) scaleCoeff(n_prs_flt_rms)    = prsScale_flt

    ! Temperature
    n_temp_avg                  = get_index('Temperature_AVG') 
    n_temp_flt_rms               = get_index('Temperature_FLT_RMS') 
    IF (adapttmp_avg .GT. 0) scaleCoeff(n_temp_avg)    = tempScale_avg
    IF (adapttmp_flt .GT. 0) scaleCoeff(n_temp_flt_rms)    = tempScale_flt

    ! Reynolds stresses 
    IF (ALLOCATED(n_stress_avg)) DEALLOCATE(n_stress_avg)
    ALLOCATE( n_stress_avg(dim*(dim+1)/2) )
    n_stress_avg(1)      = get_index('XX_Stress_AVG') 
    IF( dim .GE. 2 )THEN
        n_stress_avg(2)  = get_index('XY_Stress_AVG') 
        n_stress_avg(3)  = get_index('YY_Stress_AVG') 
    END IF
    IF( dim .GE. 3 )THEN
        n_stress_avg(4)  = get_index('XZ_Stress_AVG') 
        n_stress_avg(5)  = get_index('YZ_Stress_AVG') 
        n_stress_avg(6)  = get_index('ZZ_Stress_AVG') 
    END IF

  END SUBROUTINE compressible_finalize_setup_mean

  SUBROUTINE compressible_cal_mean( t_local, flag )
    USE field
    IMPLICIT NONE

    REAL (pr), INTENT (IN) ::  t_local
    INTEGER , INTENT(IN) :: flag ! 0- called during adaption to IC, 1 called during main integration loop
    INTEGER :: i
    REAL (pr) :: relax_avg, dt_avg
    LOGICAL, SAVE :: first_call = .TRUE.

    dt_avg = ABS( t_local - avg_timestart )
    IF (exp_avg) THEN
      relax_avg = dt / avg_timescale
    ELSE
      relax_avg = dt / (dt_avg + dt)
    END IF

    ! Reynolds Averaged integrated variables
    IF (saveReyAvgInt ) THEN

      IF( (restart_ReyAvg .OR. IC_restart_mode==0) .AND. first_call ) THEN

        ! Density AVG
        u(:,n_den_avg) = u(:,n_den)
        ! Momentum AVG
        DO i = 1,dim
          u(:,n_mom_avg(i)) = u(:,n_mom(i))
        END DO
        ! Energy AVG
        u(:,n_eng_avg) = u(:,n_eng)
        ! Species AVG
        DO i=1,Nspecm
          u(:,n_spc_avg(i)) = u(:,n_spc(i))
        END DO

        IF (saveReyAvgFlt ) THEN
          ! Density FLT root mean square
          u(:,n_den_flt_rms) = 0.0_pr 
          ! Momentum FLT 
          DO i = 1,dim
            u(:,n_mom_flt(i)) = 0.0_pr !u(:,n_mom(i)) - u(:,n_mom_avg(i))
          END DO
          ! Energy FLT root mean square
          u(:,n_eng_flt_rms) = 0.0_pr 
          ! Species FLT root mean square
          DO i=1,Nspecm
            u(:,n_spc_flt_rms(i)) = 0.0_pr
          END DO
        END IF ! saveReyAvgFlt

      END IF !restart_ReyAvg

      ! Density AVG
      u(:,n_den_avg) = ( 1.0_pr - relax_avg ) * u(:,n_den_avg) + ( relax_avg ) * u(:,n_den)

      ! Momentum AVG
      DO i = 1,dim
        u(:,n_mom_avg(i)) = ( 1.0_pr -  relax_avg ) * u(:,n_mom_avg(i)) + ( relax_avg ) * u(:,n_mom(i))
      END DO

      ! Energy AVG
      u(:,n_eng_avg) = ( 1.0_pr -  relax_avg ) * u(:,n_eng_avg) + ( relax_avg ) * u(:,n_eng)

      ! Species AVG
      DO i=1,Nspecm
        u(:,n_spc_avg(i)) = ( 1.0_pr -  relax_avg ) * u(:,n_spc_avg(i)) + ( relax_avg ) * u(:,n_spc(i))
      END DO

      IF (saveReyAvgFlt ) THEN

        ! Density FLT root mean square
        u(:,n_den_flt_rms) = sqrt( ( 1.0_pr - relax_avg ) * u(:,n_den_flt_rms)**2 + ( relax_avg ) * (u(:,n_den) - u(:,n_den_avg))**2 )

        ! Momentum FLT
        DO i = 1,dim
          u(:,n_mom_flt(i)) = u(:,n_mom(i)) - u(:,n_mom_avg(i))
        END DO

        ! Energy FLT root mean square
        u(:,n_eng_flt_rms) = sqrt( ( 1.0_pr - relax_avg ) * u(:,n_eng_flt_rms)**2 + ( relax_avg ) * (u(:,n_eng) - u(:,n_eng_avg))**2 )

        ! Species FLT root mean square
        DO i=1,Nspecm
          u(:,n_spc_flt_rms(i)) = sqrt( ( 1.0_pr - relax_avg ) * u(:,n_spc_flt_rms(i))**2 + ( relax_avg ) * (u(:,n_spc(i)) - u(:,n_spc_avg(i)))**2 )
        END DO

      END IF ! saveReyAvgFlt

    END IF ! saveReyAvgInt

    ! <p> - pressrue
    IF (saveReyAvgPrs ) THEN

      IF( (restart_ReyAvg .OR. IC_restart_mode==0) .AND. first_call ) THEN
        ! Pressure Reynolds Average
        u(:,n_prs_avg) = u(:,n_prs)

        IF (saveReyAvgFlt ) THEN
          ! Pressure FLT mean square
          u(:,n_prs_flt_rms) = 0.0_pr 
        END IF ! saveReyAvgFlt
      END IF !restart_ReyAvg

      ! Pressure AVG
      u(:,n_prs_avg) = ( 1.0_pr - relax_avg ) * u(:,n_prs_avg) + ( relax_avg ) * u(:,n_prs) 

      IF (saveReyAvgFlt ) THEN
        ! Pressure FLT mean square
        u(:,n_prs_flt_rms) = sqrt( ( 1.0_pr - relax_avg ) * u(:,n_prs_flt_rms)**2 + ( relax_avg ) * (u(:,n_prs) - u(:,n_prs_avg))**2 )
      END IF ! saveReyAvgFlt

    END IF ! saveReyAvgPrs

    ! <T> - temperature
    IF (savetmp) THEN 
    IF (saveReyAvgTmp ) THEN

      IF( (restart_ReyAvg .OR. IC_restart_mode==0) .AND. first_call ) THEN
        ! Temperature Reynolds Average
        u(:,n_temp_avg) = u(:,n_temp)

        IF (saveReyAvgFlt ) THEN
          ! Temperature FLT mean square
          u(:,n_temp_flt_rms) = 0.0_pr 
        END IF ! saveReyAvgFlt
      END IF !restart_ReyAvg

      ! Temperature AVG
      u(:,n_temp_avg) = ( 1.0_pr - relax_avg ) * u(:,n_temp_avg) + ( relax_avg ) * u(:,n_temp) 

      IF (saveReyAvgFlt ) THEN
        ! Temperature FLT mean square
        u(:,n_temp_flt_rms) = sqrt( ( 1.0_pr - relax_avg ) * u(:,n_temp_flt_rms)**2 + ( relax_avg ) * (u(:,n_temp) - u(:,n_temp_avg))**2 )
      END IF ! saveReyAvgFlt
      
    END IF ! saveReyAvgTmp
    END IF ! savetmp

    ! <rho*u_i*u_j> - Reynolds stresses
    IF (saveReyStrRsv ) THEN

      IF( (restart_ReyAvg .OR. IC_restart_mode==0) .AND. first_call ) THEN
        ! Reynolds stresses
          u(:,n_stress_avg(1)) = u(:,n_mom(1))*u(:,n_mom(1))/u(:,n_den)
        IF( dim .GE. 2 ) THEN
          u(:,n_stress_avg(2)) = u(:,n_mom(1))*u(:,n_mom(2))/u(:,n_den)
          u(:,n_stress_avg(3)) = u(:,n_mom(2))*u(:,n_mom(2))/u(:,n_den)
        END IF
        IF( dim .GE. 3 ) THEN
          u(:,n_stress_avg(4)) = u(:,n_mom(1))*u(:,n_mom(3))/u(:,n_den)
          u(:,n_stress_avg(5)) = u(:,n_mom(2))*u(:,n_mom(3))/u(:,n_den)
          u(:,n_stress_avg(6)) = u(:,n_mom(3))*u(:,n_mom(3))/u(:,n_den)
        END IF
      END IF !restart_ReyAvg

      ! Reynolds stresses
        u(:,n_stress_avg(1)) =  ( 1.0_pr - relax_avg ) * u(:,n_stress_avg(1)) + ( relax_avg ) * u(:,n_mom(1))*u(:,n_mom(1))/u(:,n_den) 
      IF( dim .GE. 2 ) THEN
        u(:,n_stress_avg(2)) =  ( 1.0_pr - relax_avg ) * u(:,n_stress_avg(2)) + ( relax_avg ) * u(:,n_mom(1))*u(:,n_mom(2))/u(:,n_den) 
        u(:,n_stress_avg(3)) =  ( 1.0_pr - relax_avg ) * u(:,n_stress_avg(3)) + ( relax_avg ) * u(:,n_mom(2))*u(:,n_mom(2))/u(:,n_den) 
      END IF
      IF( dim .GE. 3 ) THEN
        u(:,n_stress_avg(4)) =  ( 1.0_pr - relax_avg ) * u(:,n_stress_avg(4)) + ( relax_avg ) * u(:,n_mom(1))*u(:,n_mom(3))/u(:,n_den) 
        u(:,n_stress_avg(5)) =  ( 1.0_pr - relax_avg ) * u(:,n_stress_avg(5)) + ( relax_avg ) * u(:,n_mom(2))*u(:,n_mom(3))/u(:,n_den) 
        u(:,n_stress_avg(6)) =  ( 1.0_pr - relax_avg ) * u(:,n_stress_avg(6)) + ( relax_avg ) * u(:,n_mom(3))*u(:,n_mom(3))/u(:,n_den) 
      END IF

    END IF ! saveReyStrRsv

    first_call = .FALSE.
  END SUBROUTINE compressible_cal_mean
 
  SUBROUTINE compressible_mean_scales(flag,use_default, u_loc, nlocal, ne_local, l_n_var_adapt , l_n_var_adapt_index, &
       scl, scl_fltwt) !add
    USE variable_mapping
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: flag ! 0 during initial adaptation, then 1 during time advancement
    LOGICAL , INTENT(INOUT) :: use_default
    INTEGER, INTENT (IN) :: nlocal, ne_local
    REAL (pr), DIMENSION (1:nlocal,1:ne_local), INTENT (IN) :: u_loc
    LOGICAL , INTENT (IN) :: l_n_var_adapt(ne_local)
    INTEGER , INTENT (IN) :: l_n_var_adapt_index(1:ne_local)
    REAL (pr), DIMENSION (1:ne_local), INTENT (INOUT) :: scl
    REAL (pr) ,  INTENT(IN) ::scl_fltwt !weight for temporal filter on scl
    REAL (pr) :: floor, tmp
    INTEGER :: i, ie
    REAL (pr), SAVE, ALLOCATABLE, DIMENSION (:) :: scl_old
    LOGICAL, SAVE :: startup_init = .TRUE.

    !
    ! Ignore the output of this routine and use default scales routine
    !
    use_default = .FALSE. 

    IF( .NOT. use_default ) THEN
       !IF (par_rank.EQ.0) PRINT *,'Use user_scales() 
       !
       ! ALLOCATE scl_old if it was not done previously in user_scales
       !
       IF( startup_init .AND. .NOT. ALLOCATED(scl_old) ) THEN
          ALLOCATE( scl_old(1:ne_local) )
          scl_old = 0.0_pr
       END IF

       floor = 1.0e-12_pr
  
      IF (saveReyAvgInt) THEN
        !
        ! take appropriate vector norm over scl(1:dim)
        ! the vector norm method defaults to global Scale_Meth, but can be
        ! overridden by an optional input parameter: vel_vect_scale
        IF( vel_vect_scale == 1 ) THEN ! Use Linf scale
           scl(n_mom_avg(1):n_mom_avg(dim)) = MAXVAL(scl(n_mom_avg(1):n_mom_avg(dim)))
        ELSE IF ( vel_vect_scale == 2 ) THEN ! Use L2 scale
           scl(n_mom_avg(1):n_mom_avg(dim)) = SQRT(SUM(scl(n_mom_avg(1):n_mom_avg(dim))**2)) !now velocity vector length, rather than individual component
        ELSE IF ( vel_vect_scale == 3 ) THEN ! Use L2 scale using dA
           scl(n_mom_avg(1):n_mom_avg(dim)) = SQRT(SUM(scl(n_mom_avg(1):n_mom_avg(dim))**2)) !now velocity vector length, rather than individual component
        END IF
        IF (saveReyAvgFlt) THEN
          IF( vel_vect_scale == 1 ) THEN ! Use Linf scale
             scl(n_mom_flt(1):n_mom_flt(dim)) = MAXVAL(scl(n_mom_flt(1):n_mom_flt(dim)))
          ELSE IF ( vel_vect_scale == 2 ) THEN ! Use L2 scale
             scl(n_mom_flt(1):n_mom_flt(dim)) = SQRT(SUM(scl(n_mom_flt(1):n_mom_flt(dim))**2)) !now velocity vector length, rather than individual component
          ELSE IF ( vel_vect_scale == 3 ) THEN ! Use L2 scale using dA
             scl(n_mom_flt(1):n_mom_flt(dim)) = SQRT(SUM(scl(n_mom_flt(1):n_mom_flt(dim))**2)) !now velocity vector length, rather than individual component
          END IF
        END IF !saveReyAvgFlt

        IF ( adaptden_avg .EQ. 1 ) scl(n_den_avg) = 1.0_pr 
        IF ( adaptmom_avg .EQ. 1 ) THEN
           DO i = 1,dim
              scl(n_mom_avg(i)) = 1.0_pr
           END DO
        END IF
        IF ( adapteng_avg .EQ. 1 ) scl(n_eng_avg) = 1.0_pr 
        IF ( adaptspc_avg .EQ. 1 ) THEN
           DO i = 1,Nspecm
              scl(n_spc_avg(i)) = 1.0_pr
           END DO
        END IF

        IF (saveReyAvgFlt) THEN
          IF ( adaptden_flt .EQ. 1 ) scl(n_den_flt_rms) = 1.0_pr 
          IF ( adaptmom_flt .EQ. 1 ) THEN
             DO i = 1,dim
                scl(n_mom_flt(i)) = 1.0_pr
             END DO
          END IF
          IF ( adapteng_flt .EQ. 1 ) scl(n_eng_flt_rms) = 1.0_pr 
          IF ( adaptspc_flt .EQ. 1 ) THEN
             DO i = 1,Nspecm
                scl(n_spc_flt_rms(i)) = 1.0_pr
             END DO
          END IF
        END IF !saveReyAvgFlt
      END IF !saveReAvgInt

      IF (saveReyAvgPrs) THEN
        IF ( adaptprs_avg .EQ. 1 ) scl(n_prs_avg) = 1.0_pr 
        IF (saveReyAvgFlt) THEN 
          IF ( adaptprs_flt .EQ. 1 ) scl(n_prs_flt_rms) = 1.0_pr 
        END IF
      END IF !saveReyAvgPrs

      IF (savetmp) THEN 
      IF (saveReyAvgTmp) THEN
        IF ( adapttmp_avg .EQ. 1 ) scl(n_temp_avg) = 1.0_pr 
        IF (saveReyAvgFlt) THEN 
          IF ( adapttmp_flt .EQ. 1 ) scl(n_temp_flt_rms) = 1.0_pr 
        END IF
      END IF !saveReyAvgTmp
      END IF ! savetmp

      !
      ! Print out new scl
      !
      DO ie=1,ne_local
         !IF(l_n_var_adapt(ie)) THEN
         !this is done if one of the variable is exactly zero inorder not to adapt to the noise
         IF(scl(ie) .LE. floor) scl(ie)=1.0_pr 
         tmp = scl(ie)
         ! temporally filter scl
         IF(.NOT. startup_init ) scl(ie) = scl_fltwt * scl_old(ie) + (1.0_pr - scl_fltwt) * scl(ie)
         scl_old(ie) = scl(ie)           !save scl for this time step
         scl(ie) = scaleCoeff(ie) * scl(ie)
         IF (par_rank.EQ.0 .AND. verb_level .GT. 0) THEN
            WRITE (6,'("Scale factor eqn", I4 ," = ", es11.4 ," scalecoeff = ",es11.4 )') &
                    ie, scl(ie), scaleCoeff(ie)
            WRITE (6,'("(before filtering = ",es11.4,")" )') tmp
         END IF
         !END IF
      END DO
      
      !scl_old = scl !save scl for this time step
      startup_init = .FALSE.

    END IF !use_default

    IF (par_rank.EQ.0) THEN
       WRITE(*,'("************************ SCALES **************************")')
       DO ie=1,ne_local
          WRITE(*,'("***", A,E12.5,"        ***")') u_variable_names(ie), scl(ie)
       END DO
       WRITE(*,'("**********************************************************")')
    END IF
  END SUBROUTINE compressible_mean_scales

END MODULE compressible_mean_vars
