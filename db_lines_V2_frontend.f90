!
! This module defines the database that contains all collocation points.
! all routines that directly access the data base are contained in this
! module
!
! Daniel Goldstein January 2004
!

!
! Uncomment define below to force points to be sorted to match wrk version
!
!     #define SORT_TO_MATCH_WRK
MODULE db_mod
  USE  PRECISION
  USE debug_vars
  USE  sizes
  USE  pde
  USE user_case_db !defines n_var_db
  USE wlt_trns_util_mod
  USE wlt_vars
  USE wlt_trns_vars
  USE lines_database ! backend for liens db that does actual data base management
  USE util_vars
  USE db_lines_vars
! Lines starting with !$ are only compiled if for openMP
!$  USE OMP_LIB

  IMPLICIT NONE




CONTAINS


!
! Initialize a new grid data base
! 
! Points at j_mn and below are markes active to begin with
  SUBROUTINE init_lines_db(nwlt, j_mn , j_mx, restart)
    IMPLICIT NONE
    INTEGER ,INTENT(INOUT):: nwlt
    INTEGER ,INTENT(IN) :: j_mn, j_mx
    LOGICAL ,INTENT(IN) :: restart !true if restarting
    INTEGER i,ix,iy,iz , ixyz_db(1:dim), j,k
    INTEGER (KIND_INT_J)           :: tmp_j
    INTEGER (KIND_INT_FLAGS)       :: tmp_j_flag 
    INTEGER (KIND_INT_J_LEV_FLAGS) :: tmp_j_lev_flag
    INTEGER (KIND_INT_BND)         :: tmp_BND
    INTEGER :: tmp_int
    INTEGER, DIMENSION(nwlt,1:dim) :: indx_loc
    INTEGER, DIMENSION(nwlt)       ::  i_arr,deriv_lev_loc
 
    !
    ! A quick check to be sure that the KIND used for representing levels in DB is big enough
    !
    PRINT *,'10**RANGE(tmp_j)-1 :', 10**RANGE(tmp_j)-1
    if( 10**RANGE(tmp_j)-1 < j_mx) THEN
       PRINT *,'ERROR, db_lines, KIND_INT_J too small to represent j_mx requested...'
       STOP
    END IF
    PRINT *,'BIT_SIZE(tmp_j_flag) :', BIT_SIZE(tmp_j_flag)
    IF( BIT_SIZE(tmp_j_flag) < MAX_FLAG ) THEN
       PRINT *,'ERROR, db_lines, KIND_INT_FLAGS too small to represent flags requested... MAX_FLAG=',MAX_FLAG
       STOP     
    END IF
    PRINT *,'BIT_SIZE(tmp_j_lev_flag) :', BIT_SIZE(tmp_j_lev_flag)
    IF( BIT_SIZE(tmp_j_lev_flag) < j_mx ) THEN
       PRINT *,'ERROR, db_lines, KIND_INT_LEV_FLAGS too small to represent flags requested...'
       STOP     
    END IF

    !
    ! Shift must be able to represent an int as big ass all the boundary pts. This is the worst case that all
    ! the possible boundary pts are used.
    !
    !badidea  tmp_int = 0.0_pr
    !badidea  DO i=1,dim-1
    !badidea     tmp_int = tmp_int + mxyz(i)*2**(j_mx-1)  * mxyz(i+1)*2**(j_mx-1) !one face
    !badidea  END DO
    !badidea  IF( 10**RANGE(tmp_j_lev_bnd_shift)-1 < tmp_int ) THEN
    !badidea     PRINT *,'ERROR, db_lines, KIND_INT_LEV_BND_SHIFT too small to represent worst case total boundary resolution...'
    !badidea	 STOP     
    !badidea  END IF
    !
    ! Print a banner showing limits of representation
    !
    PRINT *,'******************************************************************************'
    PRINT *,'*                                                                            *'
    PRINT *,'*  KIND types (KIND_INT_J,KIND_INT_LEV_FLAGS) defined in db_lines_var.f90    *'
    PRINT *,'*  limit j_mx <= ', MIN( INT(BIT_SIZE(tmp_j_lev_flag),4), 10**RANGE(tmp_j)-1 ) 
    PRINT *,'*                                                                            *'
    PRINT *,'******************************************************************************'





    dbmax(1) = mxyz(1)*2**(j_mx-1)
    dbmax(2) = mxyz(2)*2**(j_mx-1)
    dbmax(3) = mxyz(3)*2**( jd*(j_mx-1) ) !jd==0 for dim=2
    j_mx_db = j_mx ! save the highest level represented in db
    j_mn_db = j_mn ! save the lowest  level represented in db


#ifdef SORT_TO_MATCH_WRK
    PRINT *,'******************************************************************************'
    PRINT *,'*                                                                            *'
    PRINT *,'*  TESTING, points sorted in lines db access routines to match wrk version   *'
    PRINT *,'*                                                                            *'
    PRINT *,'*****************************************************************************'
#endif


    ! 
    ! setup map between location and level for the idim dimensions
    ! based on internal max db level j_mx_db
    IF (ALLOCATED(lvl_db)) DEALLOCATE (lvl_db)
    ALLOCATE( lvl_db(0:MAXVAL(dbmax) ))
    DO i=0,MAXVAL(dbmax)
       lvl_db(i) = ij2j(i, j_mx)     
    END DO

    !
    ! IF we are restarting we need to add points to DB that are in u on levels jmn+1 to j_lev
    ! 
    IF( restart ) THEN

       !
       ! Intitalize DB backend
       ! Here we initialize just level 1
	   !
	   ! Use tmp variable of type INTEGER(KIND_INT_FLAGS) set with
	   ! proper flags to get correct
	   ! type when passing flag argument to init_lines_db_backend()
	   ! First call to init_lines_db_backend only sets up DB but doesn't
	   ! add points (controlled by first arg)
       tmp_j_flag = IBSET( INT(0,KIND_INT_FLAGS), ACTIVE )
       db_nwlt =  init_lines_db_backend( INIT_DONT_ADD_PTS, &
            mxyz(1:dim),&           ! grid at the level 1
            prd     ,&           ! periodic (1), non-periodic (0)
            j_mn    ,&           ! minimum level of initialization
            j_mx    ,&           ! maximum level
            n_var_db,&           ! number of  real values inside a node
            0       ,&           ! number of integer values inside a node
            dim     ,&           ! dimension     
            tmp_j_flag ,&        ! flag to set initially for internal points
            BND     ,&           ! flag to set  for boundary points
            j_lev )! ,&         ! j_deriv_lev_int set to  initial j_lev
       !face_type_internal ) ! face # that marks internal pts

       !
       ! Get indx that maps flat indices (1:nwlt) to coordinates from indxDB 
       ! NOTE: at thsi point the shift var in indxDB is NOT initialized!!!
	   ! because of this i_arr is garbage. We ignore it and go through
	   ! indx_loc directly because the order doesn't matter here.
	   ! get levels 1 through j_lev
       CALL get_indices_and_coordinates_by_types ( (/0,2**dim-1/), (/0,3**dim-1/), &
            (/1,j_lev/), j_lev, nwlt, i_arr, indx_loc, deriv_lev_loc)


       DO i = 1,nwlt

          ixyz_db = indx_loc(i,:)*2**(j_mx-j_lev)

          !IF( MAXVAL(lvl_db(ixyz_db(1:dim)))   > j_mn  ) THEN

             !
             ! Add the node to the DB
             !
             !  SUBROUTINE add_node_lines_db_backend (3Dlevel, coord, 
             !  flags, deriv_lev, truncated_deriv_lev, indx)
			 !
			 ! Use tmp variable of type INTEGER(KIND_INT_FLAGS) set with
			 ! proper flags to get correct
			 ! type when passing flag argument to add_node_lines_db_backend()
			 ! 
			 tmp_j_flag = IBSET( INT(0,KIND_INT_FLAGS), ACTIVE)
             CALL add_node_lines_db_backend ( &
                  lvl_db(ixyz_db(1:dim)), &
                  ixyz_db, &
                  tmp_j_flag,  & ! flags
                  deriv_lev_loc(i), &         ! deriv_lev 
                  j_lev, &                    ! truncated_deriv_lev
                  0)                          ! indx
             db_nwlt = db_nwlt+1
          !END IF
       END DO
    ELSE
       !
       ! Intitalize DB backend
       !
	   !
	   ! Use tmp variable of type INTEGER(KIND_INT_FLAGS) set with
	   ! proper flags to get correct
	   ! type when passing flag argument to init_lines_db_backend()
	   ! 
       !j_lev = MIN(j_mx,j_mn+1)
	   tmp_j_flag = IBSET( INT(0,KIND_INT_FLAGS), ACTIVE)
       db_nwlt =  init_lines_db_backend(INIT_ADD_PTS, &
            mxyz    ,&           ! grid at the level 1
            prd     ,&           ! periodic (1), non-periodic (0)
            j_lev   ,&           ! minimum level of initialization
            j_mx    ,&           ! maximum level
            n_var_db,&           ! number of  real values inside a node
            0       ,&           ! number of integer values inside a node
            dim     ,&           ! dimension     
            tmp_j_flag ,&    ! flag to set initially for internal points
            BND     ,&           ! flag to set  for boundary points
            j_lev  )! ,&           ! j_deriv_lev_int set to  initial j_lev
       !face_type_internal ) ! face # that marks internal pts
    END IF

    !
    ! call delete_some_backend to sweep through new DB and set line_flags for each line
    !
   	!
	! Use tmp variable of type INTEGER(KIND_INT_FLAGS) set with
	! proper flags to get correct
	! type when passing flag argument to delete_some_backend()
    ! 
    tmp_j_flag = IBSET( INT(0,KIND_INT_FLAGS), DELETE )
    CALL delete_some_backend( tmp_j_flag  , j_lev)




    !  N_bnd_db = 0

    db_ghost = 0
    db_n_bnd_total = 0
    !len_u_ptrs_to_db_bnd = 0


    !
    ! Setup parameters for u_ptrs_to_db array
    len_u_ptrs_to_db = 0 !so it gets allocated in first call to update_u_from_db



    ! initalize the lines index array 
    ! This is initialized to the maximum possible number of active lines on any face.
    ! This can be done  less brute force later
    !ALLOCATE( lines_indx_backend( (MAXVAL(dbmax-prd)+1 )**2) )

    nwlt = db_nwlt ! pass back the number of collocation points created



  END SUBROUTINE init_lines_db


!
! This routine updates the u array from the db.
!
! NOTE make nlv local to this routine (eventually it goes away)
!  nlvj can go away now
! lv and lvj need to stay accessible for now. Thet are used in diff_aux routines
!
SUBROUTINE update_u_from_db(  u , nwlt , ie, n_var_min, n_var_max, db_offset, j_mx, j_out, max_nxyz)

  IMPLICIT NONE
  INTEGER,   INTENT(IN) :: nwlt , ie
  REAL (pr), DIMENSION (1:nwlt,1:ie) , INTENT (INOUT) :: u
  INTEGER,   INTENT(IN)    ::  n_var_min, n_var_max, db_offset, j_mx,max_nxyz, j_out

  !local vars  
  TYPE (grid_pt) ,POINTER  :: dbpt
  INTEGER :: i,j
  INTEGER :: ibndr
  INTEGER :: curr_pt !current point 
  LOGICAL lchk
  INTEGER :: indx_line !index into current line being processed
  REAL(pr)             :: line_u(db_offset:db_offset+n_var_max-n_var_min,0:max_nxyz) 
  INTEGER(KIND_INT_FLAGS):: line_flags(0:max_nxyz)
  TYPE (grid_pt_ptr)   :: line_ptrs(0:max_nxyz)
  INTEGER              :: line_coords(0:max_nxyz,dim)
  INTEGER :: n_active_pts_odd,active_pts_odd(max_nxyz+1)
  INTEGER :: n_active_pts_even,active_pts_even(max_nxyz+1)
  INTEGER :: n_delete_pts,delete_pts(max_nxyz+1)
  INTEGER :: n_j_above,j_above(max_nxyz+1)
  INTEGER :: idim, ixyz_loc(3), ixyz_db(3)
  INTEGER :: odim(1:dim-1) ! the face coordinates of the dimensions other then idim 
  !  INTEGER :: ln
  INTEGER :: db_scale !scaleing from local to database indexing
  INTEGER :: db_n_bnd_total_save
  INTEGER :: k, ih, l
  integer :: ii
  INTEGER :: nlv(1:4,1:nwlt) ! nlv is still needed locally in this routine to build indx_DB, In the future indx_DB
                             ! will be build directly from db and nlv will not be needed D.G. 
  INTEGER ::tst1
  INTEGER , DIMENSION(:,:) ,SAVE, ALLOCATABLE :: indx ! local to this routine,indx from indx (1-nwlt) to coordinate (1:dim)
  TYPE (grid_pt_ptr)   ,SAVE , DIMENSION(:) , ALLOCATABLE :: u_ptrs_to_db_bnd
  INTEGER :: plocal_numlines_indx_backend ! number of active lines indexed in lines_indx_backend
  TYPE (element_of_face_ptr) ,SAVE, DIMENSION(:), POINTER :: plocal_lines_indx_backend !  list of active line faces
  INTEGER :: plocal_lines_indx_current ! current active line to be returned 

  !REAL(pr) , dimension(:), ALLOCATABLE :: test1
  !------------ arrays used for indx_DB --------------------
  TYPE :: indx_DB_info_type
     INTEGER :: icoord ! points to global contracted coordinate on wrk array
     INTEGER :: wlt_type, face_type, j_deriv   
  END TYPE indx_DB_info_type
  TYPE   (indx_DB_info_type) :: indx_db_info(1:nwlt)

  INTEGER :: j_df, j_lv, wlt_type, face_type, ishift!, ibndr
  INTEGER :: face_dim, face_val, ixyz(1:dim)
  INTEGER, DIMENSION(dim) :: wlt, face, ivec
  INTEGER, DIMENSION(0:dim) :: i_p, i_p_face, i_p_wlt
  LOGICAL, DIMENSION(:,:), SAVE, ALLOCATABLE :: face_assigned
  LOGICAL :: face_type_mask(0:3**dim-1)
  INTEGER :: face_type_count, bnd_faces(0:2*dim), face_bnd, iibnd
  !---------------------------------------------------------
  LOGICAL :: tmp_found(nwlt)

!$OMP threadprivate(u_ptrs_to_db_bnd,plocal_lines_indx_backend,indx,face_assigned)
  n_delete_pts = 0 ! delete points list not used in this routine

  CALL check_db_access( n_var_min, n_var_max, db_offset , "update_u_from_db()")

  db_scale = (2**(j_mx_db-j_lev))
  ! Make correct size u to take new data
  IF( db_nwlt >  nwlt ) THEN
     PRINT *,'ERROR In update_u_from_db, db_nwlt >  nwlt'
     PRINT *,'Exit ...'
     stop

  END IF
  
  IF( ALLOCATED(indx) ) DEALLOCATE( indx )
  ALLOCATE( indx(1:nwlt,1:3) )  !NOTE needs to be 1:dim when rest of code is updated

  ! realloc u_ptrs_to_db array if its current length is less then the lenght of u
  !  We allocate u_ptrs_to_db as dimension  1:db_nwlt+u_ptrs_to_db_buf so it will not keep getting
  ! reallocated as it grows in small increments
  IF( len_u_ptrs_to_db < nwlt ) THEN
     IF( ALLOCATED(u_ptrs_to_db ) ) DEALLOCATE( u_ptrs_to_db )
     ALLOCATE( u_ptrs_to_db(1:nwlt+u_ptrs_to_db_buf ) )
     len_u_ptrs_to_db = nwlt+u_ptrs_to_db_buf
  END IF

  IF (ALLOCATED(u_ptrs_to_db_bnd)) DEALLOCATE (u_ptrs_to_db_bnd)
  ALLOCATE( u_ptrs_to_db_bnd(1:nwlt ) )
  ! realloc u_ptrs_to_db_bnd array if its current length is less then the lenght of u (bwlt)
  !  We allocate u_ptrs_to_db_bnd as dimension  1:nwlt+u_ptrs_to_db_buf_bnd so it will not keep getting
  ! reallocated as it grows in small increments
  ! NEED A BETTER WAY TO DOTHIS DG.
  !Print*,'len_u_ptrs_to_db_bnd , db_n_bnd_total',len_u_ptrs_to_db_bnd , db_n_bnd_total
  !IF( len_u_ptrs_to_db_bnd < nwlt ) THEN
  !   IF( ALLOCATED(u_ptrs_to_db_bnd ) ) DEALLOCATE( u_ptrs_to_db_bnd )
  !!   len_u_ptrs_to_db_bnd = nwlt+u_ptrs_to_db_buf_bnd
  !   ALLOCATE( u_ptrs_to_db_bnd(1:len_u_ptrs_to_db_bnd ) )

  !END IF

  !----
  !   indx_DB(:,:,:,:)%length = 0
  ! indx_DB(:,:,:,:)%shift  = 1
  IF( .NOT.ALLOCATED(indx_DB)) THEN
     ALLOCATE(indx_DB(1:j_mx_DB,0:2**dim-1,0:3**dim-1,1:j_mx_DB))

     !clear out indx_DB
     DO j = 1, j_mx_DB
        DO wlt_type = MIN(j-1,1),2**dim - 1
           !        DO wlt_type = 0,2**dim - 1
           DO face_type = 0, 3**dim - 1
              DO j_df = 1, j_mx_DB
                 NULLIFY (indx_DB(j_df,wlt_type,face_type,j)%p)
                 indx_DB(j_df,wlt_type,face_type,j)%real_length = 0
                 indx_DB(j_df,wlt_type,face_type,j)%length = 0
                 indx_DB(j_df,wlt_type,face_type,j)%shift = 1
              END DO
           END DO
        END DO
     END DO
  END IF

  !
  ! Copy new data to u
  ! It is important that the data is copied in the following order
  ! first internal points by level starting with level 1
  ! then boundary points by level starting with level 1
  ! This ordering is used in the solver part of the code.
  !
  ! In this section following index arrays are updated that are
  ! used in solver part of code.
  ! lv_intrnl  -
  ! indx       - index that associated location in u with domain location
  ! Only if the data base has changed!!!
  !
  !IF ( db_dirty ) THEN

  !
  curr_pt = 0
  lv_intrnl = 0
  db_n_bnd_total = 0
  !---------- go through internal points level by level (Building indx() array )
  ! Field is traversed starting at j=1 up to j=j_lev. Points
  ! picked up on level j are then ignored when they are hit on higher levels.
  ! Boundary points are skipped in this loop. They are done below.
  idim = 1

  DO j =1,j_out
     lv_intrnl(j) = lv_intrnl(j-1)

     CALL get_active_lines_indx( idim, j, 1, GET_ALL_DERIV_LEV, &
           plocal_lines_indx_backend, plocal_numlines_indx_backend,&
		   plocal_lines_indx_current )

     line_flags = 0  !We have to zero line_flags because we never 
                     ! call put_active_line() in this routine

     ! Get all active pts on this line.
     ! We call get_active_line() with sort_pts=.TRUE. so that the order 
     ! in u matches the old wrk version. This is also critical for restart 
     ! to work!!
     !
     DO WHILE ( get_active_line( idim, j, j_lev ,max_nxyz, db_offset, &
          db_offset+n_var_max-n_var_min, &
          line_u, line_flags, line_ptrs, line_coords, odim, &
          active_pts_odd, n_active_pts_odd, &
          active_pts_even, n_active_pts_even,&
          j_above, n_j_above, delete_pts, n_delete_pts, NO_DELETE ,NO_GET_J_ABOVE ,&
		   LINE_ONE, NO_GHOST_PTS,GET_ALL_PTS, SORT_PTS_TRUE, &
           plocal_lines_indx_backend, plocal_numlines_indx_backend,&
		   plocal_lines_indx_current) )  



        DO i=1,n_active_pts_even
           indx_line = active_pts_even(i)

           ixyz_db = line_coords(indx_line,:)
           ixyz_loc  = ixyz_db / db_scale 
           !ixyz_loc(idim)   = indx_line
           !ixyz_db(idim)   = indx_line* db_scale 



           !
           ! for each active point check its level and 
           ! if it is for the current level we are indexing add it to the index
           !


           IF ( BTEST( line_flags(indx_line), ACTIVE ) ) THEN ! NOT NEEDED???, TESTING ONLY


              !
              ! Debug check for GHOST pts
              !
              !IF( BTEST( line_flags(indx_line), GHOST ) )THEN
              !   PRINT  *, 'SSupdate_u_from_db error, ghost pt found, ', ixyz_db, char_flags(line_flags(indx_line))
              !   stop
              !END IF

              !IF( j == db_find_lvl_3d( ixyz_db  ) ) THEN
              IF( j == MAXVAL( lvl_db(ixyz_db(1:dim)))  ) THEN

                 IF(  BTEST( line_flags(indx_line), BND ) ) THEN
                    !
                    ! save boundary points 
                    !
                    db_n_bnd_total = db_n_bnd_total + 1 
                    !PRINT *,'in update_u_from_db db_n_bnd_total', db_n_bnd_total
                    u_ptrs_to_db_bnd(db_n_bnd_total )%ptr => line_ptrs(indx_line)%ptr !set ptr back in db
                 ELSE

                    curr_pt = curr_pt + 1
                    !PRINT *,ixyz_db, line_flags(indx_line), curr_pt, 'TESTPT add new'

                    u(curr_pt,n_var_min:n_var_max) = line_u(:,indx_line) 
                    indx(curr_pt,1:3) = ixyz_loc

                    lv_intrnl(j) = lv_intrnl(j)+1

                    u_ptrs_to_db(curr_pt)%ptr => line_ptrs(indx_line)%ptr !set ptr back in db 
                    u_ptrs_to_db(curr_pt)%ptr%indx = curr_pt !added DG 020405
                 END IF
              END IF
           ELSE ! NOT NEEDED, TESTING ONLY
	      PRINT *,'TEST in update_u_from_db() non-active points found, Exiting'  ! NOT NEEDED, TESTING ONLY
              stop  ! NOT NEEDED, TESTING ONLY
           END IF  ! NOT NEEDED, TESTING ONLY


!NOTE we need to only zer flags that have been touched..

          line_flags(indx_line) = 0  !We have to zero line_flags because we 
                                     ! never  call put_active_line() in this
                                     ! routine

        END DO ! i=1,n_active_pts_even

     END DO

  END DO

  ! Sanity check
  ! check that we get all the bnd points in the first sweep 
  !!IF(db_n_bnd_total_save /= db_n_bnd_total ) THEN
  !!	PRINT *, 'ERROR in update_u_from_db(),  db_n_bnd_total_save /= db_n_bnd_total, Exiting...'
  !!	stop
  !!END IF

  ! sanity check
  IF( curr_pt == 0 ) THEN
     PRINT *,'Error, no active points to return in update_u_from_db(), Exiting...'
     STOP
  END IF
tmp_found = .FALSE.!TEST
  !
  !
  !---------- go through boundary points (Building indx() array )
  lv_bnd = 0 !TEST
  DO i=1,2*dim
     lchk=.FALSE.
     nbnd(ibnd(i))=0
     DO j=1,j_out  !---
        lv_bnd(0,i,j) = curr_pt+1 
        lv_bnd(1,i,j) = curr_pt   
        IF(ibnd(i) == 1 .AND. prd(1) == 0) THEN 
           !traverse left  boundary (xmin)
		   idim = 1 
           ibndr = 0
           lchk=.TRUE.
        ELSE IF(ibnd(i) == 2 .AND. prd(1) == 0) THEN 
           !traverse right boundary (xmax)
		   idim = 1
           ibndr = nxyz(idim)*db_scale
           lchk=.TRUE.
        ELSE IF(ibnd(i) == 3 .AND. prd(2) == 0) THEN 
           !traverse bottom boundary (ymin)
		   idim = 2
           ibndr = 0
           lchk=.TRUE.
        ELSE IF(ibnd(i) == 4 .AND. prd(2) == 0) THEN 
           !traverse top boundary (ymax)
		   idim = 2
           ibndr = nxyz(idim)*db_scale
           lchk=.TRUE.
        ELSE IF(dim == 3 .and. ibnd(i) == 5 .AND. prd(3) == 0) THEN 
           !traverse bottom boundary (zmin)
		   idim = 3
           ibndr = 0
           lchk=.TRUE.
        ELSE IF(dim == 3 .and. ibnd(i) == 6 .AND. prd(3) == 0) THEN 
           !traverse top boundary (zmax)
		   idim = 3
           ibndr = nxyz(idim)*db_scale
           lchk=.TRUE.
        END IF

        IF( lchk ) THEN
           !
           !  Go through the list of boundary points we saved above
           !

           DO ii = 1,db_n_bnd_total
              ixyz_db = u_ptrs_to_db_bnd(ii)%ptr%reg_grid_index
              IF ( j == MAXVAL( lvl_db(ixyz_db(1:dim))) .AND. ixyz_db(idim) == ibndr .AND. .NOT. tmp_found(ii) ) THEN


				tmp_found(ii)=.TRUE.

                 curr_pt = curr_pt + 1
                 u(curr_pt,n_var_min:n_var_max) = &
                      u_ptrs_to_db_bnd(ii)%ptr%u(db_offset:db_offset+n_var_max-n_var_min)
                 u_ptrs_to_db(curr_pt)%ptr =>  u_ptrs_to_db_bnd(ii)%ptr !set ptr back in db 
                 u_ptrs_to_db(curr_pt)%ptr%indx = curr_pt !added DG 020405
                 indx(curr_pt,1:dim) = &
                      u_ptrs_to_db_bnd(ii )%ptr%reg_grid_index(1:dim)/ (2**(j_mx-j_lev))

                 nbnd(ibnd(i))= nbnd(ibnd(i))+1
                 lv_bnd(1,i,j) = lv_bnd(1,i,j) +1

              END IF

           END DO
        END IF
     END DO
  END DO



  ! up date the lv_bnd(2:3, , ) 
  ! the index 2:3 of this array is the mapping needed to put the bnd pnts after level j
  ! in the flat u array. This is used in diff_aux_db()

  DO j=1,j_lev
     i = 1
     lv_bnd(2,i,j) = lv_intrnl(j) + 1
     lv_bnd(3,i,j) = lv_bnd(2,i,j) + lv_bnd(1,i,j)-lv_bnd(0,i,1)
     DO i=2,2*dim
        lv_bnd(2,i,j) = lv_bnd(3,i-1,j) + 1
        lv_bnd(3,i,j) = lv_bnd(2,i,j) + lv_bnd(1,i,j)-lv_bnd(0,i,1)
     END DO
  END DO

  !
  ! Set face on the actual node in the DB.
  !
  DO ibndr=1,2*dim
     DO i=lv_bnd(0,ibndr,1),lv_bnd(1,ibndr,j_lev)
        u_ptrs_to_db(i)%ptr%ibndr = ibndr
	 END DO
  END DO



  IF(ALLOCATED(x)) DEALLOCATE(x)
  ALLOCATE(x(1:nwlt,1:dim))
  x=0.0_pr  

  DO idim =1,dim
     DO i=1,nwlt
        x(i,idim) = xx(indx(i,idim),idim)
     END DO
  END DO

  !
  ! Setup arrays that index into flat array structure by derivative level.
  !
  ! NOTE: this should really be done in set_ghost_pts_db() where the derivative levels are defined.
  ! this way the data is accessed from far memory one less time. DG
  ! CUrrently this is called before the first call to set_ghost_pts_db() so
  ! deriv_lev is undefined!!
  !
  lv(0) = 0
  DO j = 1, j_lev
     lv(j) = lv(j-1)
     DO i = 1, nwlt
        ! If the neighborhood contains a point at level j then we add this point to the
        ! list of points for which the derivative will be done at level j
        IF ( u_ptrs_to_db(i)%ptr%deriv_lev == j) THEN ! 
           lv(j) = lv(j)+1  ! keep track of the # of points who's derivative is done at this level
           nlv(1,lv(j)) = i ! add this point to the list of pts who's derivative is done at this level
           nlv(2:4,lv(j)) =  u_ptrs_to_db(i)%ptr%reg_grid_index / db_scale
		   !WRITE(*,'( "XXderiv_lev ", 6(I5.5 , 1X ) )' ) &
		   ! u_ptrs_to_db(i)%ptr%reg_grid_index / db_scale, j ,lv(j) ,i  !TESTTEST
        END IF
     END DO
  END DO


  !
  ! for each level j (j_in in c_diff_fast ) find all points whos 
  ! deriviative level is greater or equal to j and whos phsycal 3d level is j or less
  !
  nwltj = 0
  lvj = 0
  DO j = 1, j_lev
     DO k  = lv(j-1)+1, lv(j_lev) ! lv(j-1)+1, lv(j) derivative needs to calculated on level j
        i  = nlv(1,k) ! map into the actual u vector
        lchk=.FALSE.
        IF(i <= lv_intrnl(j)) lchk = .TRUE. ! belongs to physcal 3d level j or less
        DO ih=1,2*dim ! check if not on bndry
           IF(lv_bnd(0,ih,1)<= i .AND. i <= lv_bnd(1,ih,j)) lchk = .TRUE.
        END DO
        IF(lchk) nwltj = nwltj + 1 !belongs to my level j
     END DO
     lvj(j) = nwltj
  END DO

!OLD  IF(allocated(nlvj)) DEALLOCATE(nlvj)
!OLD  ALLOCATE(nlvj(1:4,1:nwltj))
!OLD  nlvj = 0

!OLD  !Now we go through and fill nlvj( 1 , l) == index into u
!OLD  l = 0
!OLD  DO j = 1, j_lev
!OLD     DO k  = lv(j-1)+1, lv(j_lev)
!OLD        i  = nlv(1,k)
!OLD        lchk=.FALSE.
!OLD        IF(i <= lv_intrnl(j)) lchk = .TRUE.
!OLD        DO ih=1,2*dim
!OLD           IF(lv_bnd(0,ih,1)<= i .AND. i <= lv_bnd(1,ih,j)) lchk = .TRUE.
!OLD        END DO
!OLD        IF(lchk) THEN 
!OLD           l = l + 1
!OLD           nlvj(1,l) = i
!OLD           nlvj(2:4,l) =  u_ptrs_to_db(i)%ptr%reg_grid_index / db_scale
!OLD        END IF
!OLD    END DO
!OLD  END DO


  !
  ! Set j_full
  ! j_full is used to determine the minimum level for derivatives
  j_full=j_lev
  DO WHILE(lv(j_full) /= 0 .AND. j_full>1)
     j_full=j_full-1
  END DO
  ! PRINT *,'j_full:',j_full,j_lev
  ! PRINT *,'lv: ',lv
  j_full = MAX(1,j_full-1)
  ! PRINT *,'j_full:',j_full,j_lev

  !
  ! Consistency check
  !
  IF( curr_pt /= nwlt ) THEN
     PRINT *,' Error, curr_pt /= nwlt in update_u_from_db'
     PRINT *,'curr_pt = ', curr_pt 
     PRINT *,'nwlt = ', nwlt
     PRINT *, 'Exiting ...'
     stop
  END IF

  !*********************************************************************************************
  !* Setup indx_DB
  !* This part should be the same for all DB types with few modifications related to DB maping *  
  !*********************************************************************************************

  IF( .NOT. ALLOCATED(Nwlt_lev) )  ALLOCATE(Nwlt_lev(1:j_mx,0:1)) ! allocates only once for all levels up to j_mx

  Nwlt_lev(:,:) = 0
  DO j = 1, j_lev
     Nwlt_lev(j,0) = lv_intrnl(j)      ! # of internal points on level j and below
     Nwlt_lev(j,1) = lv_bnd(3,2*dim,j) ! # of all points on level j and below
  END DO
  !
  !================== Set up list of pointes based on wlt_type, face_type, j, j_diff, 
  !                   where j_diff is the level at which derivative is taken
  !
  !
  !initializing
  !
  IF(j_lev > j_mx_DB) THEN
     WRITE(*,'("ERROR: j_lev>j_mx_DB")')
     STOP
  END IF
  ! creatig forward and backward mapping, in future has to go thrhough wavelt families and levels to create DB own indx.
  i_p(0) = 1
  i_p_face(0) = 1
  i_p_wlt(0) = 1
  DO i=1,dim
     i_p(i) = i_p(i-1)*(1+nxyz(i))
     i_p_face(i) = i_p_face(i-1)*3
     i_p_wlt(i) = i_p_wlt(i-1)*2
  END DO
  DO i = 1,dim
     ivec(i) = dim-i+1
  END DO

  face = 0 
  face_type_internal = SUM((face+1)*i_p_face(0:dim-1))


  !----
  !    IF(.NOT.ALLOCATED(indx_DB)) THEN
  !       ALLOCATE(indx_DB(1:j_mx_DB,0:2**dim-1,0:3**dim-1,1:j_mx_DB))
  !       DO j = 1, j_mx_DB
  !          DO wlt_type = MIN(j-1,1),2**dim - 1
  !             DO face_type = 0, 3**dim - 1
  !                DO j_df = 1, j_mx_DB
  !                   NULLIFY (indx_DB(j_df,wlt_type,face_type,j)%p)
  !                   indx_DB(j_df,wlt_type,face_type,j)%real_length = 0
  !                END DO
  !             END DO
  !          END DO
  !       END DO
  !       face = 0 
  !       face_type_internal = SUM((face+1)*i_p_face(0:dim-1))
  !    END IF
  !
  ! seting up the indx_DB list
  !
  indx_DB(:,:,:,:)%length = 0
  indx_DB(:,:,:,:)%shift = 1
  !
  ! count number of each type of wavelet type
  ! and keep track of this number in %length for each type
  !
  DO j_df = 1, j_lev
     DO k = lv(j_df-1)+1, lv(j_df)
        i = nlv(1,k)
        ixyz(1:dim) = indx(i,1:dim)
        face = (-1 + 2*INT(ixyz(1:dim)/nxyz(1:dim))+ MIN(1,MOD(ixyz(1:dim),nxyz(1:dim)))) * (1 - prd(1:dim))
        j = ixyz2j(ixyz(1:dim),j_lev)
        wlt = MIN(MIN(1,j-1),MOD(ixyz(1:dim),2**(j_lev-j+1)))
        face_type = SUM((face+1)*i_p_face(0:dim-1))
        wlt_type = SUM(wlt(ivec)*i_p_wlt(0:dim-1))
        indx_DB(j_df,wlt_type,face_type,j)%length = indx_DB(j_df,wlt_type,face_type,j)%length + 1
     !WRITE(*,'("---------------")')
	!WRITE(*,'( 4(i4.4,1x),a )' ) j_df,wlt_type,face_type,j,"l1abcd"
	!WRITE(*,'( i )' ) indx_DB(j_df,wlt_type,face_type,j)%length
	!WRITE(*,'( i )' ) indx_DB(j_df,wlt_type,face_type,j)%real_length

     END DO
  END DO
  ! allocating arrays
  DO j = 1, j_lev
     DO wlt_type = MIN(j-1,1),2**dim-1
        DO face_type = 0, 3**dim - 1
           DO j_df = j, j_lev

!PRINT THE types here and see why 2 2 13 3 is never being allocated even though length
!is on-zero????
	!WRITE(*,'("---------------")')
	!WRITE(*,'( 4(i4.4,1x),a )' ) j_df,wlt_type,face_type,j,"l2"
	!WRITE(*,'( i )' ) indx_DB(j_df,wlt_type,face_type,j)%length
	!WRITE(*,'( i )' ) indx_DB(j_df,wlt_type,face_type,j)%real_length

              IF( indx_DB(j_df,wlt_type,face_type,j)%length  /= indx_DB(j_df,wlt_type,face_type,j)%real_length ) THEN
                 ! eventually make smarter allocation algorithm, so 
                 ! if the size is bigger, increasie it by 25% and if smaller than 50% decrease by 25%
                 ! or something like this 
                 IF (indx_DB(j_df,wlt_type,face_type,j)%real_length > 0) THEN
                    DEALLOCATE(indx_DB(j_df,wlt_type,face_type,j)%p)
                    indx_DB(j_df,wlt_type,face_type,j)%real_length = 0
                 END IF
                 IF (indx_DB(j_df,wlt_type,face_type,j)%length  > 0) THEN
                    ALLOCATE(indx_DB(j_df,wlt_type,face_type,j)%p(1:indx_DB(j_df,wlt_type,face_type,j)%length))
                    indx_DB(j_df,wlt_type,face_type,j)%real_length = indx_DB(j_df,wlt_type,face_type,j)%length
                 END IF
!WRITE(*,'("aLLOCATE---------------")')
              END IF
           END DO
        END DO
     END DO
  END DO
  !--------------- assigning indx
  !  indx_DB(j_df,wlt_type,face_type,j)%p(k)%ixyz coresponds to level j_lev
  !
  indx_DB(:,:,:,:)%length = 0
  DO j_df = 1, j_lev
     DO k = lv(j_df-1)+1, lv(j_df)
        i = nlv(1,k)
        ixyz(1:dim) = indx(i,1:dim)
        face = (-1 + 2*INT(ixyz(1:dim)/nxyz(1:dim))+ MIN(1,MOD(ixyz(1:dim),nxyz(1:dim)))) * (1 - prd(1:dim))
        j = ixyz2j(ixyz(1:dim),j_lev)
        wlt = MIN(MIN(1,j-1),MOD(ixyz(1:dim),2**(j_lev-j+1)))
        face_type = SUM((face+1)*i_p_face(0:dim-1))
        wlt_type = SUM(wlt(ivec)*i_p_wlt(0:dim-1))
        ii = 1+SUM(ixyz(1:dim)*i_p(0:dim-1)) ! index on j_lev level
        ! ixyz can be exctracted using the follwing command: ixyz(1:dim) = INT(MOD(ii-1,i_p(1:dim))/i_p(0:dim-1))

		indx_DB(j_df,wlt_type,face_type,j)%length = indx_DB(j_df,wlt_type,face_type,j)%length + 1
    !WRITE(*,'("---------------")')
	!WRITE(*,'( 4(i4.4,1x),a )' ) j_df,wlt_type,face_type,j,"l3"
	!WRITE(*,'( i )' ) indx_DB(j_df,wlt_type,face_type,j)%length
	!WRITE(*,'( i )' ) indx_DB(j_df,wlt_type,face_type,j)%real_length

	IF( indx_DB(j_df,wlt_type,face_type,j)%length <= 0 ) THEN
		   PRINT *,'ERROR1 in update_u_from_db()'
		   stop
    endif
	IF( indx_DB(j_df,wlt_type,face_type,j)%length > indx_DB(j_df,wlt_type,face_type,j)%real_length)then
		   PRINT *,'ERROR2 in update_u_from_db()'
		   stop
    endif

		indx_DB(j_df,wlt_type,face_type,j)%p(indx_DB(j_df,wlt_type,face_type,j)%length)%ixyz = ii
        indx_DB(j_df,wlt_type,face_type,j)%p(indx_DB(j_df,wlt_type,face_type,j)%length)%i = i
     END DO
  END DO
  ! ensures compatibility with the old code
  ! setting shift for internal points to 0
  face = 0 
  face_type = SUM((face+1)*i_p_face(0:dim-1))
  indx_DB(1:j_lev,0:2**dim-1,face_type,1:j_lev)%shift = 0 ! internal points
  DO ibndr=1,2*dim
     DO i=lv_bnd(0,ibndr,1),lv_bnd(1,ibndr,j_lev)
        ixyz(1:dim) = indx(i,1:dim)
        face = (-1 + 2*INT(ixyz(1:dim)/nxyz(1:dim))+ MIN(1,MOD(ixyz(1:dim),nxyz(1:dim)))) * (1 - prd(1:dim))
        j = ixyz2j(ixyz,j_lev)
        wlt = MIN(MIN(1,j-1),MOD(ixyz,2**(j_lev-j+1)))
        face_type = SUM((face+1)*i_p_face(0:dim-1))
        wlt_type = SUM(wlt(ivec)*i_p_wlt(0:dim-1))
        DO j_lv = 1, j_lev ! j_lv - level of resolution for which u is supplied.
           ishift = lv_bnd(2,ibndr,j_lv)-lv_bnd(0,ibndr,1)
           IF(indx_DB(j_lv,wlt_type,face_type,j)%shift == 1) indx_DB(j_lv,wlt_type,face_type,j)%shift = ishift
        END DO
     END DO
  END DO

  !*********************************************************************************************
  !* END Setup indx_DB
  !*********************************************************************************************
!TEST
!  DO j = 1, j_lev
!     DO wlt_type = MIN(j-1,1),2**dim-1
!        DO face_type = 0, 3**dim - 1
!           DO j_df = j, j_lev
!              WRITE(*,'( "INDX_DB ", 5(i2, 1X) )')  j_df,wlt_type,face_type,j,indx_DB(j_df,wlt_type,face_type,j)%length
!			  IF( indx_DB(j_df,wlt_type,face_type,j)%length > 0 ) THEN
!			     DO i=1,indx_DB(j_df,wlt_type,face_type,j)%length
!			        WRITE(*,'( "p() ",(i6, 1X) )' ) indx_DB(j_df,wlt_type,face_type,j)%p(i)%i
!			     END DO
!			  END IF
!		   END DO
!		END DO
!	END DO
! END DO

 DEALLOCATE( indx,u_ptrs_to_db_bnd)
 CALL deallocate_active_lines_indx(plocal_lines_indx_backend)
END SUBROUTINE update_u_from_db



!
! update_u_from_db_noidices 
! In this routine we update the values in u from the db
! No indices are changed. Only use this routine if the 
! grid has not changed since the last call to update_u_from_db()
!
SUBROUTINE update_u_from_db_noidices(  u , nwlt , ie, n_var_min, n_var_max , db_offset)
  IMPLICIT NONE
  INTEGER,   INTENT(IN) :: nwlt, ie
  REAL (pr), INTENT(INOUT) :: u(1:nwlt,1:ie)
  INTEGER,   INTENT(IN) :: n_var_min, n_var_max, db_offset
  INTEGER :: i

  CALL check_db_access( n_var_min, n_var_max, db_offset , "update_u_from_db_noidices()")

  DO i = 1,nwlt
    u(i,n_var_min:n_var_max) = u_ptrs_to_db(i)%ptr%u(db_offset:db_offset+n_var_max-n_var_min)
	u_ptrs_to_db(i)%ptr%indx = i !added DG S  
  END DO
 
 
END SUBROUTINE update_u_from_db_noidices




!
! update_db_from_u 
! In this routine we update the values in db from u 
!
SUBROUTINE update_db_from_u(  u , nwlt , ie, n_var_min, n_var_max, db_offset )
  IMPLICIT NONE
  INTEGER,   INTENT(IN) :: nwlt, ie
  REAL (pr), INTENT(IN) :: u(1:nwlt,1:ie)
  INTEGER,   INTENT(IN) :: n_var_min, n_var_max, db_offset
  INTEGER :: i

  !IF( debug_zero_db ) CALL zero_all_db_backend( )
  CALL check_db_access( n_var_min, n_var_max, db_offset , "update_db_from_u()")

  DO i = 1,nwlt
    u_ptrs_to_db(i)%ptr%u(db_offset:db_offset+n_var_max-n_var_min) =  u(i,n_var_min:n_var_max)
	u_ptrs_to_db(i)%ptr%indx = i ! save linear index 
  END DO
 
 
END SUBROUTINE update_db_from_u


! update_db_from_u_mask 
! In this routine we update the values in db from u if they are masked
!
SUBROUTINE update_db_from_u_mask(  u , nwlt , ie, mask, mask2, use_second_mask, db_offset )
  IMPLICIT NONE
  INTEGER,   INTENT(IN) :: nwlt, ie
  REAL (pr), INTENT(IN) :: u(1:nwlt,1:ie)
  LOGICAL,   INTENT(In) :: mask(1:ie), mask2(1:ie)
  LOGICAL,   INTENT(IN) :: use_second_mask
  INTEGER,   INTENT(IN) :: db_offset
  INTEGER :: i, n_var_min, n_var_max, data_indx(1:ie)

  !IF( debug_zero_db ) CALL zero_all_db_backend( )

  !
  ! Make integer array to access data
  !
  n_var_min = 1
  n_var_max = 0
  DO i = 1,ie
	IF( mask(i) ) THEN
		n_var_max = n_var_max + 1
		data_indx(n_var_max) = i
	END IF
  END DO

  IF( use_second_mask ) THEN
	DO i = 1,ie
		IF( mask2(i) .AND. .NOT. mask(i) ) THEN
			n_var_max = n_var_max + 1
			data_indx(n_var_max) = i
		END IF
	END DO

  END IF

  CALL check_db_access( n_var_min, n_var_max, db_offset , "update_db_from_u()")

  DO i = 1,nwlt
    u_ptrs_to_db(i)%ptr%u(db_offset:db_offset+n_var_max-n_var_min) =  u(i,data_indx(1:n_var_max))
	u_ptrs_to_db(i)%ptr%indx = i ! save linear index 
  END DO
 
 
END SUBROUTINE update_db_from_u_mask

! update_u_from_db_mask 
! In this routine we update the values in u from db if they are masked
!
SUBROUTINE update_u_from_db_mask(  u , nwlt , ie, mask, mask2, use_second_mask, db_offset )
  IMPLICIT NONE
  INTEGER,   INTENT(IN) :: nwlt, ie
  REAL (pr), INTENT(INOUT) :: u(1:nwlt,1:ie)
  LOGICAL,   INTENT(In) :: mask(1:ie), mask2(1:ie)
  LOGICAL,   INTENT(IN) :: use_second_mask
  INTEGER,   INTENT(IN) :: db_offset
  INTEGER :: i, n_var_min, n_var_max, data_indx(1:ie)

  !
  ! Make integer array to access data
  !
  n_var_min = 1
  n_var_max = 0
  DO i = 1,ie
	IF( mask(i) ) THEN
		n_var_max = n_var_max + 1
		data_indx(n_var_max) = i
	END IF
  END DO

  IF( use_second_mask ) THEN
	DO i = 1,ie
		IF( mask2(i) .AND. .NOT. mask(i) ) THEN
			n_var_max = n_var_max + 1
			data_indx(n_var_max) = i
		END IF
	END DO

  END IF

  CALL check_db_access( n_var_min, n_var_max, db_offset , "update_db_from_u()")

  DO i = 1,nwlt
    u(i,data_indx(1:n_var_max)) = u_ptrs_to_db(i)%ptr%u(db_offset:db_offset+n_var_max-n_var_min) 
	!u_ptrs_to_db(i)%ptr%indx = i ! save linear index 
  END DO
 
 
END SUBROUTINE update_u_from_db_mask


!
! Make input args:
! lv_intrnl
! lv_bnd
!
! called from wlt_interpolate() in wavelet_3d_lines.f90
!
SUBROUTINE update_db_from_u_interpolate(  u , nlocal , ie, mn_var, mx_var, j_in, j_out )
  IMPLICIT NONE
  INTEGER,   INTENT(IN) :: nlocal, ie, mn_var, mx_var, j_in, j_out
  REAL (pr), INTENT(IN) :: u(1:nlocal,1:ie)
  INTEGER :: i, ibndr, ishift

  !IF( debug_zero_db ) CALL zero_all_db_backend( )

  DO i=1,lv_intrnl(j_in)
  		u_ptrs_to_db(i)%ptr%u(mn_var:mx_var) =  u(i,mn_var:mx_var)
		u_ptrs_to_db(i)%ptr%indx = i ! save linear index 
  END DO
  DO ibndr=1,2*dim
     ishift = lv_bnd(2,ibndr,j_in)-lv_bnd(0,ibndr,1)
     DO i=lv_bnd(0,ibndr,1),lv_bnd(1,ibndr,j_in)
  		u_ptrs_to_db(i)%ptr%u(mn_var:mx_var) =  u(i+ishift,mn_var:mx_var)
     END DO
  END DO

  !
  ! Zero out from j_in to j_out
  !
  IF( j_in < j_out ) THEN

	  DO i=lv_intrnl(j_in)+1,lv_intrnl(j_out)
  			u_ptrs_to_db(i)%ptr%u(mn_var:mx_var) =  0.0_pr
	  END DO
	  DO ibndr=1,2*dim
		 DO i=lv_bnd(0,ibndr,j_in)+1,lv_bnd(1,ibndr,j_out)
  			u_ptrs_to_db(i)%ptr%u(mn_var:mx_var) =  0.0_pr
		 END DO
	  END DO

  END IF ! ( j_in < j_out )

END SUBROUTINE update_db_from_u_interpolate


!
! Make input args:
! lv_intrnl
! lv_bnd
!
! called from wlt_interpolate() in wavelet_3d_lines.f90
!
SUBROUTINE update_u_from_db_interpolate(  u , nwlt , ie, mn_var, mx_var, j_in, j_out )
  IMPLICIT NONE
  INTEGER,   INTENT(IN) :: nwlt, ie, mn_var, mx_var, j_in, j_out
  REAL (pr), INTENT(INOUT) :: u(1:nwlt,1:ie)
  INTEGER :: i,ibndr, ishift

  DO i=1,lv_intrnl(j_out)

    u(i,mn_var:mx_var) = 	u_ptrs_to_db(i)%ptr%u(mn_var:mx_var) 

  END DO
  DO ibndr=1,2*dim
     ishift = lv_bnd(2,ibndr,j_out)-lv_bnd(0,ibndr,1)
     DO i=lv_bnd(0,ibndr,1),lv_bnd(1,ibndr,j_out)
  
      u(i+ishift,mn_var:mx_var) = 	u_ptrs_to_db(i)%ptr%u(mn_var:mx_var) 

     END DO
  END DO

END SUBROUTINE update_u_from_db_interpolate

!
! check we are using legal values to access db
!
SUBROUTINE check_db_access( n_var_min, n_var_max, db_offset , func_name)
  IMPLICIT NONE
  INTEGER,   INTENT(IN) :: n_var_min, n_var_max, db_offset
  CHARACTER (LEN=*)     :: func_name ! name of function we are called from
  LOGICAL               :: error


  error = .FALSE.
  IF( db_offset > n_var_db ) THEN
	 WRITE( * ,'( "Error in ", A , " db_offset > n_var_db, Exiting..." )') TRIM(func_name)
	 PRINT *, 'db_offset = ', db_offset
	 error = .TRUE.
  END IF
  IF( db_offset+n_var_max-n_var_min > n_var_db ) THEN
	 WRITE( * ,'( "Error in ", A , " db_offset+n_var_max-n_var_min > n_var_db" )') TRIM(func_name)
	 PRINT *, 'db_offset+n_var_max-n_var_min = ', db_offset+n_var_max-n_var_min
	 error = .TRUE.
  END IF
  IF( n_var_min < 0 .OR. n_var_min > n_var_db ) THEN
	 WRITE( * ,'( A,A ,A )') &
             "Error in ", &
             TRIM(func_name), &
             " n_var_min < 0 .OR. n_var_min > n_var_db"
	 PRINT *, 'n_var_min = ', n_var_min
	 error = .TRUE.
  END IF
  IF( n_var_max < n_var_min .OR. n_var_max > n_var_db ) THEN
	 WRITE( * ,'( A,A,A )') &
             "Error in ", &
             TRIM(func_name) , &
             " n_var_max < n_var_min .OR. n_var_max > n_var_db"
	 PRINT *, 'n_var_min = ', n_var_min
	 PRINT *, 'n_var_max = ', n_var_max
	 error = .TRUE.
  END IF

  IF( error ) THEN
     PRINT *,'n_var_db = ', n_var_db
	 PRINT *,'Exiting..'
     STOP
  END IF
 
END SUBROUTINE check_db_access

!
! Find level based on max level in each direction
! PURE Temporaily not pure so bounds checking works with absoft compiler 
FUNCTION  db_find_lvl_3d( reg_grid)
  IMPLICIT NONE 
  INTEGER, INTENT (IN) :: reg_grid(1:3)
  INTEGER :: db_find_lvl_3d

!PRINT *,reg_grid
  ! check valid grid indices
  IF( reg_grid(1) > dbmax(1)-prd(1) .OR. reg_grid(1) < 0 .OR. & 
      reg_grid(2) > dbmax(2)-prd(2) .OR. reg_grid(2) < 0 .OR. &
      reg_grid(3) > dbmax(3)-prd(3) .OR. reg_grid(3) < 0 )  THEN
      PRINT *, 'Error in db_find_lvl_3d. Incorrect reg_grid.. '
      PRINT *, 'reg_grid = ', reg_grid
      PRINT *, 'Exiting...'
      stop 
   END IF 
 


  db_find_lvl_3d = MAX( lvl_db(reg_grid(1)), lvl_db(reg_grid(2)))
  IF( dim == 3 ) THEN
     db_find_lvl_3d = MAX( db_find_lvl_3d, lvl_db(reg_grid(3)))
  END IF

END FUNCTION  db_find_lvl_3d

!
! This routine returns a list of lines in the idim direction that are
! required for the level j transform that have some active points on them.
!
! (3d level <= j+1)
!
SUBROUTINE get_active_lines_indx(idim, j, j_df_min, j_df_max, &
           plocal_lines_indx_backend, plocal_numlines_indx_backend,&
		   plocal_lines_indx_current )
  IMPLICIT NONE 
  INTEGER ,INTENT(IN) :: idim, j
  INTEGER ,INTENT(IN) :: j_df_min, j_df_max    !  only get line if pts with j_df_min<deriv_lev<j_df_max exist on line 
  INTEGER ,INTENT(INOUT) :: plocal_numlines_indx_backend ! number of active lines indexed in lines_indx_backend
  TYPE (element_of_face_ptr) , DIMENSION(:), POINTER :: plocal_lines_indx_backend !  list of active line faces
  INTEGER ,INTENT(INOUT) :: plocal_lines_indx_current ! current active line to be returned 
  INTEGER :: ixyz_db(1:3), ii,jj, iidim,iiidim
  INTEGER IAM_OMP, N_Threads_OMP

  ! Check we are not called with idim ==3 if we are only doing 2D
  IF( idim > dim ) THEN
     PRINT *,'Error in get_active_lines_indx(), idim > dim, Exiting ...'
     stop
  END IF

  ! initalize the lines index array 
  ! This is initialized to the maximum possible number of active lines on any face.
  ! This can be done  less brute force later
  IF( .NOT. ASSOCIATED(plocal_lines_indx_backend) ) THEN
     ALLOCATE( plocal_lines_indx_backend( (MAXVAL(dbmax-prd)+1 )**2) )
  END IF

!$  IAM_OMP = OMP_GET_THREAD_NUM()
!$  N_Threads_OMP = OMP_GET_NUM_THREADS()
!$  PRINT *,'In get_active_lines_indx(), IAM_OMP, N_Threads_OMP', IAM_OMP, N_Threads_OMP
!!!PRINT *,idim, j, j_df_min, j_df_max,plocal_numlines_indx_backend


  CALL get_active_lines_indx_backend (idim, j, j_df_min, j_df_max, &
          plocal_lines_indx_backend, plocal_numlines_indx_backend)
  

!@ $  IF( IAM_OMP /= 0 ) THEN
!@ $   plocal_numlines_indx_backend = 0 ! force zero for all other than proc zero to test
!@ $ END IF 
  plocal_lines_indx_current = 0 !zero lines_indx_current counter

! different strategies for load balancing.
! 1) divide # lines (brute force)
!    in this first try we call get_active_lines_indx_backend
! for each thread and then divide the results
!$  plocal_numlines_indx_backend = plocal_numlines_indx_backend/N_Threads_OMP
!$  plocal_lines_indx_current = IAM_OMP*plocal_numlines_indx_backend
!$  plocal_numlines_indx_backend = plocal_lines_indx_current + plocal_numlines_indx_backend
!$ PRINT *,'get_active_lines_indx ',IAM_OMP,plocal_numlines_indx_backend,plocal_lines_indx_current

!$OMP BARRIER

END SUBROUTINE get_active_lines_indx

SUBROUTINE deallocate_active_lines_indx(plocal_lines_indx_backend)
  IMPLICIT NONE
  TYPE (element_of_face_ptr) , DIMENSION(:), POINTER :: plocal_lines_indx_backend

  IF( ASSOCIATED(plocal_lines_indx_backend) ) THEN
     DEALLOCATE(plocal_lines_indx_backend)
  END IF

END SUBROUTINE deallocate_active_lines_indx


!
! Get the active points on a given line for a given level transform
!
! j - get pts on line associeted with level "j" and below (1D level)
! j_lev_out - current max level in solver (j_lev) which is different then the
!             max level internal to the db. 
! get_del   - ! Flag to define whether to get DELETE points in get_active_line()
!             1 NO_DELETE=0, GET_DEL_PTS=1, GET_DEL_INLST=2 
! get_j_above ! flag to get pts on level above (j+1)
!             ! GET_ABOVE_PTS=0, NO_GET_J_ABOVE=1
! get_odd_even !  LINE_ONE=0, LINE_EVEN_ODD=1
!
! get_ghost
!
! get_BND     ! GET_INT_PTS = 0, GET_ALL_PTS=1
!
! current ln pointer lines_indx_current reset in call to get_active_lines_indx()
!

! performance TODO
! cp data to curr_node (and save curr_nod_ptr) 
! then cp of data from far memory is done only once.
FUNCTION get_active_line( idim, j, j_lev_out, max_nxyz_out,   mn_var,mx_var, &
     line_u, line_flags, line_ptrs, line_coords, odim,  &
     active_pts_odd, n_active_pts_odd, &
     active_pts_even, n_active_pts_even, &
     j_above, n_j_above, delete_pts, n_delete_pts, get_del , &
	 get_j_above, get_odd_even, get_ghost, get_BND, sort_pts, &
     plocal_lines_indx_backend, plocal_numlines_indx_backend,&
     plocal_lines_indx_current) 
  IMPLICIT NONE
  INTEGER, INTENT(IN) ::  max_nxyz_out
  INTEGER , INTENT(IN)   :: idim,mn_var,mx_var, j,j_lev_out
  INTEGER , INTENT(INOUT):: active_pts_odd(max_nxyz_out+1), n_active_pts_odd
  INTEGER , INTENT(INOUT):: active_pts_even(max_nxyz_out+1), n_active_pts_even
  INTEGER , INTENT(INOUT):: delete_pts(max_nxyz_out+1), n_delete_pts
  INTEGER , INTENT(INOUT):: j_above(max_nxyz_out+1), n_j_above
  REAL(pr), INTENT(INOUT):: line_u(mn_var:mx_var,0:max_nxyz_out)
  INTEGER(KIND_INT_FLAGS), INTENT(INOUT):: line_flags(0:max_nxyz_out) 
  INTEGER , INTENT(INOUT):: line_coords(0:max_nxyz_out,dim)  ! coordinates of returned points in terms of j_mx
  INTEGER , INTENT(OUT)  :: odim(1:dim-1) ! the face coordinates of the dimensions other then idim
  INTEGER , INTENT(IN)   :: get_del ! NO_DELETE     - get no pts marked DELETE
  ! GET_DEL_PTS   - put DELETE points in line but not in odd/even pts list (for inverse transform after adapt_grid)
  ! GET_DEL_INLST - put DELETE points in line AND in odd/even pts list (for db_update_grid )
  INTEGER , INTENT(IN)   :: get_j_above  !NO_GET_J_ABOVE=0, GET_ABOVE_PTS=1
  INTEGER , INTENT(IN)   :: get_odd_even ! LINE_ONE=0, LINE_EVEN_ODD=1
  INTEGER , INTENT(IN)   :: get_ghost    ! GET_GHOST_PTS = 0, NO_GHOST_PTS=1
  INTEGER , INTENT(IN)   :: get_BND      ! GET_INT_PTS = 0, GET_ALL_PTS=1
  LOGICAL , INTENT(IN)   :: sort_pts      ! When called from update_u_from_db we need to sort the points
  LOGICAL                :: get_active_line ! return true as long as there is another line to return
  INTEGER ,INTENT(INOUT) :: plocal_numlines_indx_backend ! number of active lines indexed in plocal_lines_indx_backend
  TYPE (element_of_face_ptr) , DIMENSION(:), POINTER :: plocal_lines_indx_backend !  list of active line faces
  INTEGER ,INTENT(INOUT) :: plocal_lines_indx_current ! current active line to be returned 
  INTEGER :: get_j_above_loc
  TYPE (grid_pt_ptr) , INTENT(INOUT)  :: line_ptrs(0:max_nxyz_out)
  INTEGER :: ix,iy,iz
  INTEGER :: ixyz_db(1:3), ii, i, lvl, db_scale
  INTEGER ::  ext_indx ! index into this line used external to the db routines
  INTEGER (KIND_INT_FLAGS) :: flags_loc, flags_mask
  LOGICAL :: get_one_start 
  LOGICAL :: is_deleted
  TYPE (grid_pt) , POINTER :: curr_node_ptr  !pointer to current node in db 
  INTEGER :: current_level ! used in get_1_backend to keep track of the current level
  

!  !PRINT *,'DEBUG get_active_line() check line_flags(0:max_nxyz_out)'
!  IF( MAXVAL(ABS(line_flags(0:max_nxyz_out))) /=0 ) THEN
!     PRINT *,'ERROR MAXVAL(ABS(line_flags(0:max_nxyz_out))) /=0 '
!	 DO i=0,max_nxyz_out
!        PRINT *,'i, line_flags(i) ', i, line_flags(i)
!	 END DO
!	 STOP
!  END IF

  n_active_pts_odd  = 0
  n_active_pts_even = 0
  n_j_above         = 0
  n_delete_pts      = 0
  db_scale = (2**(j_mx_db-j_lev_out))
  ! check we dont get a level above j_lev_out
  get_j_above_loc = get_j_above
  IF(j+get_j_above > j_lev_out ) THEN
     get_j_above_loc = 0
  END IF

  !
  ! Make mask for nodes we want...
  !
  flags_mask = 0
  DO i=ACTIVE,ACTIVE+dim+1 ! we want active points
     flags_mask = IBSET(flags_mask, i )
  END DO
  IF( get_del > 0 )                flags_mask = IBSET(flags_mask, DELETE )
  IF( get_ghost == GET_GHOST_PTS ) flags_mask = IBSET(flags_mask, GHOST  )


  !
  ! Return u and flags into  line_u and line_flags. 
  ! ALso return integer array that lists points passed back (and # of points passed back)
  !( then I can use the exisiting logic to build the index arrays I pass back..
  

  !
  !
  ! look at points on level j+get_j_above_loc and below (1D)
  !
  plocal_lines_indx_current = plocal_lines_indx_current + 1
  IF( plocal_lines_indx_current > plocal_numlines_indx_backend ) THEN ! check we have more lines to do
     get_active_line = .FALSE. ; RETURN
  END IF
!@ $ PRINT *,'get_active_line()', OMP_GET_THREAD_NUM(),plocal_lines_indx_current,plocal_numlines_indx_backend


  get_one_start = .TRUE. !reset start to get first point on line
  curr_node_ptr => NULL()

  ! Get the first point on the line
  IF( .NOT. get_1_backend (plocal_lines_indx_backend(plocal_lines_indx_current), idim, &
      j+get_j_above_loc, curr_node_ptr, get_one_start,current_level) )THEN
     !There should always be atleast one point on a line chosen previously by a call to 
	 ! get_active_line_indx() so this is an error condition
	 PRINT *,'Error in get_active_line(), Line is empty...Exiting...'
	 STOP
  END IF

  !
  ! Set the face coordinates of the dimensions other then idim
  !
  ixyz_db = curr_node_ptr%reg_grid_index ! index relative to j_mx_db
  ii = 1
  DO i = 1,dim
	 IF( i /= idim) THEN
	   odim(ii) = ixyz_db(i)
	   ii = ii+1
	 END IF
  END DO


   DO     
     
     flags_loc = curr_node_ptr%flags
        
     IF( IAND(flags_mask,flags_loc) /= 0  .AND. &
         (ABS(IBITS(flags_loc, BND,1 )) <= get_BND )) THEN

        ! Note on if above, I changed the test for the Bnd points above,
		! It should work as if this is a boundary pt and get_BND == 1 then we proceed,
		! if it is a boundary pt and get_BND == 0 then we do not. The ABS() was put to ensure 
		! correct operation if bit may produce negative integer. DG.
		! old line: (IBITS(flags_loc, BND,1 ) <= get_BND )) THEN

		ixyz_db = curr_node_ptr%reg_grid_index ! index relative to j_mx_db
        lvl = lvl_db( ixyz_db(idim)) 

        !
        ! Get data for this line to pass back
        ! Data is passed back in a line nxyz(idim) long based on the
        ! current max level j   / (2**(j_mx-j_lev_out))
        ext_indx = ixyz_db(idim)/ db_scale
        line_coords(ext_indx,:) = ixyz_db
        
        line_u( mn_var:mx_var, ext_indx ) = curr_node_ptr%u(mn_var:mx_var)!  u_in(mn_var:mx_var,  db(ixyz_db(1),ixyz_db(2),ixyz_db(3))%u_indx ) 
        
        !
        ! Add this point to the appropriat index array
        !
		is_deleted = BTEST(flags_loc, DELETE )
        IF( get_del == GET_DEL_PTS .AND. is_deleted ) THEN
           !
           ! We add this point to the delete list so it can be cleared in put_active_line()
           !
           n_delete_pts = n_delete_pts +1
           delete_pts(n_delete_pts) = ext_indx
           
        END IF
        
       
        IF( .NOT. (get_del /= GET_DEL_INLST .AND. is_deleted)  ) THEN
           
           ! use seperate even/ odd points lists
           
           IF( get_odd_even == LINE_EVEN_ODD ) THEN
              IF(  lvl   == j+1 ) THEN
                 n_j_above = n_j_above +1
                 j_above(n_j_above) =  ext_indx
              ELSE IF(  lvl == j ) THEN
                 n_active_pts_odd = n_active_pts_odd +1
                 active_pts_odd(n_active_pts_odd) = ext_indx
              ELSE IF(lvl < j  ) THEN
                 n_active_pts_even = n_active_pts_even +1
                 active_pts_even(n_active_pts_even) = ext_indx
              END IF
           ELSE ! use one line
             
              ! put all points in the active_pts_even list
			  IF( lvl <= j ) THEN
                 n_active_pts_even = n_active_pts_even +1
                 active_pts_even(n_active_pts_even) = ext_indx
			  ELSE ! must be on level above
                 n_j_above = n_j_above +1
                 j_above(n_j_above) =  ext_indx
			  END IF

           END IF
        END IF
        
        !
        ! Note should the pointer only be saves if this line is in a list (in if block above)
        !
        !
        ! Save pointer to find this point when doing the corresponding 
        ! put_active_line()
        !
        line_ptrs(ext_indx)%ptr => curr_node_ptr    
        line_flags(ext_indx) = flags_loc
        
        
     END IF

	      !
	 ! Look for next point
	 ! On same line..
	 IF( ASSOCIATED(curr_node_ptr%next(idim)%ptr)) THEN
	     curr_node_ptr => curr_node_ptr%next(idim)%ptr
		 
	 !If no more pts on this line, then call get_1_backend() to try to get a pt on the next line	  
	 ELSE IF( .NOT. get_1_backend (plocal_lines_indx_backend(plocal_lines_indx_current), idim, &
	          j+get_j_above_loc, curr_node_ptr, get_one_start,current_level)  ) THEN
!		 ! Not more points
         EXIT !exit the do loop
	 END IF

  END DO
  


  !PRINT *,'LEaving get_active_line ', n_active_pts_odd, n_active_pts_even
  !PRINT *,''

  ! sort active arrays when called from update_u_from_db()
  IF( sort_pts) THEN
     CALL sort_pick(active_pts_odd, n_active_pts_odd)
     CALL sort_pick(active_pts_even, n_active_pts_even)
  END IF

!PRINT *,'odd  ', active_pts_odd(1:n_active_pts_odd)
!PRINT *,'even ', active_pts_even(1:n_active_pts_even)
!PRINT *,'---------------------------'
!PRINT *,''
  get_active_line = .TRUE.
  RETURN


END FUNCTION get_active_line

#ifdef FALSE
!
! Get the active points on a given line for a given level transform
!
! j - get pts on line associeted with level "j" and below (1D level)
! j_lev_out - current max level in solver (j_lev) which is different then the
!             max level internal to the db. 
! get_del   - ! Flag to define whether to get DELETE points in get_active_line()
!             1 NO_DELETE=0, GET_DEL_PTS=1, GET_DEL_INLST=2 
! get_j_above ! flag to get pts on level above (j+1)
!             ! GET_ABOVE_PTS=0, NO_GET_J_ABOVE=1
! get_odd_even !  LINE_ONE=0, LINE_EVEN_ODD=1
!
! get_ghost
!
! get_BND     ! GET_INT_PTS = 0, GET_ALL_PTS=1
!
! current ln pointer lines_indx_current reset in call to get_active_lines_indx()
!
FUNCTION get_active_line2(lines_indx_local, idim, j, j_lev_out, max_nxyz_out,   mn_var,mx_var, &
     line_u, line_flags, line_ptrs, line_coords, odim,  &
     active_pts_odd, n_active_pts_odd, &
     active_pts_even, n_active_pts_even, &
     j_above, n_j_above, delete_pts, n_delete_pts, get_del , get_j_above, &
	 get_odd_even, get_ghost, get_BND, sort_pts, &
     plocal_lines_indx_backend, plocal_numlines_indx_backend,&
     plocal_lines_indx_current) 
  IMPLICIT NONE
  INTEGER , INTENT(IN)   :: lines_indx_local ! index into lines_index_backend
  INTEGER , INTENT(IN)   ::  max_nxyz_out
  INTEGER , INTENT(IN)   :: idim,mn_var,mx_var, j,j_lev_out
  INTEGER , INTENT(INOUT):: active_pts_odd(max_nxyz_out+1), n_active_pts_odd
  INTEGER , INTENT(INOUT):: active_pts_even(max_nxyz_out+1), n_active_pts_even
  INTEGER , INTENT(INOUT):: delete_pts(max_nxyz_out+1), n_delete_pts
  INTEGER , INTENT(INOUT):: j_above(max_nxyz_out+1), n_j_above
  REAL(pr), INTENT(INOUT):: line_u(mn_var:mx_var,0:max_nxyz_out)
  INTEGER(KIND_INT_FLAGS), INTENT(INOUT):: line_flags(0:max_nxyz_out) 
  INTEGER , INTENT(INOUT):: line_coords(0:max_nxyz_out,dim)  ! coordinates of returned points in terms of j_mx
  INTEGER , INTENT(OUT)  :: odim(1:dim-1) ! the face coordinates of the dimensions other then idim
  INTEGER , INTENT(IN)   :: get_del ! NO_DELETE     - get no pts marked DELETE
  ! GET_DEL_PTS   - put DELETE points in line but not in odd/even pts list (for inverse transform after adapt_grid)
  ! GET_DEL_INLST - put DELETE points in line AND in odd/even pts list (for db_update_grid )
  INTEGER , INTENT(IN)   :: get_j_above  !NO_GET_J_ABOVE=0, GET_ABOVE_PTS=1
  INTEGER , INTENT(IN)   :: get_odd_even ! LINE_ONE=0, LINE_EVEN_ODD=1
  INTEGER , INTENT(IN)   :: get_ghost    ! GET_GHOST_PTS = 0, NO_GHOST_PTS=1
  INTEGER , INTENT(IN)   :: get_BND      ! GET_INT_PTS = 0, GET_ALL_PTS=1
  LOGICAL , INTENT(IN)   :: sort_pts      ! When called from update_u_from_db we need to sort the points
  LOGICAL                :: get_active_line2 ! return true as long as there is another line to return
  INTEGER ,INTENT(INOUT) :: plocal_numlines_indx_backend ! number of active lines indexed in lines_indx_backend
  TYPE (element_of_face_ptr) , DIMENSION(:), POINTER :: plocal_lines_indx_backend !  list of active line faces
  INTEGER ,INTENT(INOUT) :: plocal_lines_indx_current ! current active line to be returned 
  INTEGER :: get_j_above_loc
  TYPE (grid_pt_ptr) , INTENT(INOUT)  :: line_ptrs(0:max_nxyz_out)
  INTEGER :: ix,iy,iz
  INTEGER :: ixyz_db(1:3), ii, i, lvl, db_scale
  INTEGER ::  ext_indx ! index into this line used external to the db routines
  INTEGER (KIND_INT_FLAGS) :: flags_loc, flags_mask
  LOGICAL :: get_one_start 
  LOGICAL :: is_deleted
  TYPE (grid_pt) , POINTER :: curr_node_ptr  !pointer to current node in db 
  INTEGER :: current_level ! used in get_1_backend to keep track of the current level

!  !PRINT *,'DEBUG get_active_line() check line_flags(0:max_nxyz_out)'
!  IF( MAXVAL(ABS(line_flags(0:max_nxyz_out))) /=0 ) THEN
!     PRINT *,'ERROR MAXVAL(ABS(line_flags(0:max_nxyz_out))) /=0 '
!	 DO i=0,max_nxyz_out
!        PRINT *,'i, line_flags(i) ', i, line_flags(i)
!	 END DO
!	 STOP
!  END IF
!$PRINT *,'in get_active_line2(), lines_indx_local',lines_indx_local,OMP_GET_THREAD_NUM()
!$PRINT *,'in get_active_line2(), numlines_indx_backend',numlines_indx_backend,OMP_GET_THREAD_NUM()

  n_active_pts_odd  = 0
  n_active_pts_even = 0
  n_j_above         = 0
  n_delete_pts      = 0
  db_scale = (2**(j_mx_db-j_lev_out))
  ! check we dont get a level above j_lev_out
  get_j_above_loc = get_j_above
  IF(j+get_j_above > j_lev_out ) THEN
     get_j_above_loc = 0
  END IF

  !
  ! Make mask for nodes we want...
  !
  flags_mask = 0
  DO i=ACTIVE,ACTIVE+dim+1 ! we want active points
     flags_mask = IBSET(flags_mask, i )
  END DO
  IF( get_del > 0 )                flags_mask = IBSET(flags_mask, DELETE )
  IF( get_ghost == GET_GHOST_PTS ) flags_mask = IBSET(flags_mask, GHOST  )


  !
  ! Return u and flags into  line_u and line_flags. 
  ! ALso return integer array that lists points passed back (and # of points passed back)
  !( then I can use the exisiting logic to build the index arrays I pass back..
  

  !
  !
  ! look at points on level j+get_j_above_loc and below (1D)
  !
  !lines_indx_local = lines_indx_local + 1
  IF( plocal_lines_indx_local > plocal_numlines_indx_backend ) THEN ! check we have more lines to do
     get_active_line2 = .FALSE. ; RETURN
  END IF


  get_one_start = .TRUE. !reset start to get first point on line
  curr_node_ptr => NULL()

  ! Get the first point on the line
  IF( .NOT. get_1_backend (plocal_lines_indx_backend(plocal_lines_indx_local), idim, &
      j+get_j_above_loc, curr_node_ptr, get_one_start,current_level) )THEN
     !There should always be atleast one point on a line chosen previously by a call to 
	 ! get_active_line_indx() so this is an error condition
	 PRINT *,'Error in get_active_line2(), Line is empty...Exiting...'
	 STOP
  END IF

  !
  ! Set the face coordinates of the dimensions other then idim
  !
  ixyz_db = curr_node_ptr%reg_grid_index ! index relative to j_mx_db
  ii = 1
  DO i = 1,dim
	 IF( i /= idim) THEN
	   odim(ii) = ixyz_db(i)
	   ii = ii+1
	 END IF
  END DO


   DO     
     
     flags_loc = curr_node_ptr%flags
        
     IF( IAND(flags_mask,flags_loc) /= 0  .AND. &
         (ABS(IBITS(flags_loc, BND,1 )) <= get_BND )) THEN

        ! Note on if above, I changed the test for the Bnd points above,
		! It should work as if this is a boundary pt and get_BND == 1 then we proceed,
		! if it is a boundary pt and get_BND == 0 then we do not. The ABS() was put to ensure 
		! correct operation if bit may produce negative integer. DG.


		ixyz_db = curr_node_ptr%reg_grid_index ! index relative to j_mx_db
        lvl = lvl_db( ixyz_db(idim)) 

        !
        ! Get data for this line to pass back
        ! Data is passed back in a line nxyz(idim) long based on the
        ! current max level j   / (2**(j_mx-j_lev_out))
        ext_indx = ixyz_db(idim)/ db_scale
        line_coords(ext_indx,:) = ixyz_db
        
        line_u( mn_var:mx_var, ext_indx ) = curr_node_ptr%u(mn_var:mx_var)!  u_in(mn_var:mx_var,  db(ixyz_db(1),ixyz_db(2),ixyz_db(3))%u_indx ) 
        
        !
        ! Add this point to the appropriat index array
        !
		is_deleted = BTEST(flags_loc, DELETE )
        IF( get_del == GET_DEL_PTS .AND. is_deleted ) THEN
           !
           ! We add this point to the delete list so it can be cleared in put_active_line()
           !
           n_delete_pts = n_delete_pts +1
           delete_pts(n_delete_pts) = ext_indx
           
        END IF
        
       
        IF( .NOT. (get_del /= GET_DEL_INLST .AND. is_deleted)  ) THEN
           
           ! use seperate even/ odd points lists
           
           IF( get_odd_even == LINE_EVEN_ODD ) THEN
              IF(  lvl   == j+1 ) THEN
                 n_j_above = n_j_above +1
                 j_above(n_j_above) =  ext_indx
              ELSE IF(  lvl == j ) THEN
                 n_active_pts_odd = n_active_pts_odd +1
                 active_pts_odd(n_active_pts_odd) = ext_indx
              ELSE IF(lvl < j  ) THEN
                 n_active_pts_even = n_active_pts_even +1
                 active_pts_even(n_active_pts_even) = ext_indx
              END IF
           ELSE ! use one line
             
              ! put all points in the active_pts_even list
			  IF( lvl <= j ) THEN
                 n_active_pts_even = n_active_pts_even +1
                 active_pts_even(n_active_pts_even) = ext_indx
			  ELSE ! must be on level above
                 n_j_above = n_j_above +1
                 j_above(n_j_above) =  ext_indx
			  END IF

           END IF
        END IF
        
        !
        ! Note should the pointer only be saves if this line is in a list (in if block above)
        !
        !
        ! Save pointer to find this point when doing the corresponding 
        ! put_active_line()
        !
        line_ptrs(ext_indx)%ptr => curr_node_ptr    
        line_flags(ext_indx) = flags_loc
        
        
     END IF
!$     PRINT *,'gal2(),ixyz_db ',ixyz_db,OMP_GET_THREAD_NUM()

	      !
	 ! Look for next point
	 ! On same line..
	 IF( ASSOCIATED(curr_node_ptr%next(idim)%ptr)) THEN
	     curr_node_ptr => curr_node_ptr%next(idim)%ptr
		 
	 !If no more pts on this line, then call get_1_backend() to try to get a pt on the next line	  
	 ELSE IF( .NOT. get_1_backend (plocal_lines_indx_backend(plocal_lines_indx_local), idim, &
	          j+get_j_above_loc, curr_node_ptr, get_one_start,current_level)  ) THEN
!		 ! Not more points
         EXIT !exit the do loop
	 END IF

  END DO
  


  !PRINT *,'LEaving get_active_line ', n_active_pts_odd, n_active_pts_even
  !PRINT *,''

  ! sort active arrays when called from update_u_from_db()
  IF( sort_pts) THEN
     CALL sort_pick(active_pts_odd, n_active_pts_odd)
     CALL sort_pick(active_pts_even, n_active_pts_even)
  END IF

!PRINT *,'odd  ', active_pts_odd(1:n_active_pts_odd)
!PRINT *,'even ', active_pts_even(1:n_active_pts_even)
!PRINT *,'---------------------------'
!PRINT *,''
  get_active_line2 = .TRUE.
  RETURN


END FUNCTION get_active_line2
#endif

!
! Get the active points on a given line for a given level transform
!
! j - get pts on line associeted with level "j" and below (1D level)
! j_lev_out - current max level in solver (j_lev) which is different then the
!             max level internal to the db. 
! We get delete points in the main lists
! 
! 
! get_odd_even !  LINE_ONE=0, LINE_EVEN_ODD=1
!
! current ln pointer lines_indx_current reset in call to get_active_lines_indx()
!
FUNCTION get_active_line_flags( idim, j, j_lev_out, dbscale, max_nxyz_out,   mn_var,mx_var, &
     line_u, line_flags, line_ptrs, line_coords, odim,  &
     active_pts_odd, n_active_pts_odd, &
     active_pts_even, n_active_pts_even, &
     j_above, n_j_above, delete_pts, n_delete_pts, get_del ,&
	 get_j_above, get_odd_even, get_ghost, &
     plocal_lines_indx_backend, plocal_numlines_indx_backend,&
     plocal_lines_indx_current) 
  IMPLICIT NONE
  INTEGER, INTENT(IN) ::  max_nxyz_out
  INTEGER , INTENT(IN)   :: idim,mn_var,mx_var, j,j_lev_out, dbscale
  INTEGER , INTENT(INOUT):: active_pts_odd(max_nxyz_out+1), n_active_pts_odd
  INTEGER , INTENT(INOUT):: active_pts_even(max_nxyz_out+1), n_active_pts_even
  INTEGER , INTENT(INOUT):: delete_pts(max_nxyz_out+1), n_delete_pts
  INTEGER , INTENT(INOUT):: j_above(max_nxyz_out+1), n_j_above
  REAL(pr), INTENT(INOUT):: line_u(mn_var:mx_var,0:max_nxyz_out)
  INTEGER(KIND_INT_FLAGS), INTENT(INOUT):: line_flags(0:max_nxyz_out) 
  INTEGER , INTENT(INOUT):: line_coords(0:max_nxyz_out,dim)  ! coordinates of returned points in terms of j_mx
  INTEGER , INTENT(INOUT):: odim(1:dim-1) ! the face coordinates of the dimensions other then idim
  INTEGER , INTENT(IN)   :: get_del ! NO_DELETE     - get no pts marked DELETE
  ! GET_DEL_PTS   - put DELETE points in line but not in odd/even pts list (for inverse transform after adapt_grid)
  ! GET_DEL_INLST - put DELETE points in line AND in odd/even pts list (for db_update_grid )
  INTEGER , INTENT(IN)   :: get_j_above  !NO_GET_J_ABOVE=0, GET_ABOVE_PTS=1
  INTEGER , INTENT(IN)   :: get_odd_even ! LINE_ONE=0, LINE_EVEN_ODD=1
  INTEGER , INTENT(IN)   :: get_ghost    ! GET_GHOST_PTS = 0, NO_GHOST_PTS=1
  INTEGER ,INTENT(INOUT) :: plocal_numlines_indx_backend ! number of active lines indexed in lines_indx_backend
  TYPE (element_of_face_ptr) , DIMENSION(:), POINTER :: plocal_lines_indx_backend !  list of active line faces
  INTEGER ,INTENT(INOUT) :: plocal_lines_indx_current ! current active line to be returned 
  LOGICAL                :: get_active_line_flags ! return true as long as there is another line to return
  INTEGER :: get_j_above_loc
  TYPE (grid_pt_ptr) , INTENT(INOUT)  :: line_ptrs(0:max_nxyz_out)
  !INTEGER :: ix,iy,iz
  INTEGER :: ixyz_db(1:3), ii, lvl
  INTEGER ::  ext_indx ! index into this line used external to the db routines
  INTEGER(KIND_INT_FLAGS):: curr_node_flags

  LOGICAL :: get_one_start ,first_pt
  TYPE (grid_pt) , POINTER :: curr_node_ptr  !pointer to current node in db 
  INTEGER :: current_level ! used in get_1_backend to keep track of the current level

!debug
  character :: tmpcharflags(0:MAX_FLAG+1)
  logical   :: tmpl1
  integer   :: tmpi1
!  !PRINT *,'DEBUG get_active_line_flags() check line_flags(0:max_nxyz_out)'
!  IF( MAXVAL(ABS(line_flags(0:max_nxyz_out))) /=0 ) THEN
!     PRINT *,'ERROR MAXVAL(ABS(line_flags(0:max_nxyz_out))) /=0 '
!	 DO i=0,max_nxyz_out
!        PRINT *,'i, line_flags(i) ', i, line_flags(i)
!	 END DO
!	 STOP
!  END IF

  n_active_pts_odd  = 0
  n_active_pts_even = 0
  n_j_above         = 0
  n_delete_pts      = 0


  !
  ! Return  flags into   line_flags. 
  ! ALso return integer array that lists points passed back (and # of points passed back)
  !( then I can use the exisiting logic to build the index arrays I pass back..
  

  !
  !
  ! look at points on level j+get_j_above_loc and below (1D)
  !
  plocal_lines_indx_current = plocal_lines_indx_current + 1
  IF( plocal_lines_indx_current > plocal_numlines_indx_backend ) THEN ! check we have more lines to do
     get_active_line_flags = .FALSE. ; RETURN
  END IF

  get_one_start = .TRUE. !reset start to get first point on line
  curr_node_ptr => NULL()

    ! Get the first point on the line
  IF( .NOT. get_1_backend (plocal_lines_indx_backend(plocal_lines_indx_current), idim, j , &
      curr_node_ptr, get_one_start,current_level) )THEN
     !There should always be atleast one point on a line chosen previously by a call to 
	 ! get_active_line_indx() so this is an error condition
	 PRINT *,'Error in get_active_line_flags(), Line is empty...Exiting...'
	 STOP
  END IF

!  DO WHILE ( get_1_backend (lines_indx_backend(lines_indx_current), idim, j, curr_node_ptr, get_one_start)  )
  DO     
     
     ixyz_db         = curr_node_ptr%reg_grid_index ! index relative to j_mx_db
     curr_node_flags = curr_node_ptr%flags

     lvl = lvl_db( ixyz_db(idim))
     tmpcharflags = char_flags(curr_node_flags)

    !IF( curr_node_flags == 32 ) THEN
    ! WRITE(*,'("get_active_line_flags ",10A)') char_flags(curr_node_flags)
    ! WRITE(*,'("g_a_l_f get_ghost,GET_GHOST_PTS ",i,1x,i)') &
    !   get_ghost, GET_GHOST_PTS
    ! WRITE(*,&
     ! '("get_active_line_flags IBITS(curr_node_flags ,ACTIVE,dim+1 ) ",i)')&
     !  IBITS(curr_node_flags ,ACTIVE,dim+1 )
     !WRITE(*,'("get_active_line_flags BTEST(curr_node_flags, DELETE ) ",L)')&
     !  BTEST(curr_node_flags, DELETE )
    ! WRITE(*,'("get_active_line_flags BTEST(curr_node_flags, GHOST ) ",L)')&
    !   BTEST(curr_node_flags, GHOST )

      !IF( &
      !(get_ghost == GET_GHOST_PTS .AND. BTEST(curr_node_flags, GHOST )))THEN
      !  WRITE(*,'("get_ghost == ")' ) 
      !  WRITE(*,'("GET_GHOST_PTS .AND. BTEST(curr_node_flags, GHOST )")')
      !END IF
    ! END IF

     IF( ( IBITS(curr_node_flags ,ACTIVE,dim+1 ) /= 0  .OR. &
        (get_del > 0 .AND. BTEST(curr_node_flags, DELETE ) ) .OR. &
        (get_ghost == GET_GHOST_PTS .AND. BTEST(curr_node_flags, GHOST )) ) &
        ) THEN
!NOTE we are not getting points that are get_ghost==0 and curr_node_flags=32
! on cobalt..
        !IF( curr_node_flags == 32 ) THEN
        ! WRITE(*,'("g_a_l_f ch curr_node_flags == 32",3(i4,1x))') ixyz_db
		   
	    !END IF
        !
        ! Get data for this line to pass back
        ! Data is passed back in a line nxyz(idim) long based on the
        ! current max level j   / (2**(j_mx-j_lev_out))
        ext_indx = ixyz_db(idim)/ dbscale
        line_coords(ext_indx,:) = ixyz_db
        
 	    IF(  lvl == j ) THEN
          n_active_pts_odd = n_active_pts_odd +1
          active_pts_odd(n_active_pts_odd) = ext_indx
        ELSE !IF(lvl < j  ) THEN
          n_active_pts_even = n_active_pts_even +1
          active_pts_even(n_active_pts_even) = ext_indx
        END IF
        
        !
        ! Save pointer to find this point when doing the corresponding 
        ! put_active_line()
        !
        line_ptrs(ext_indx)%ptr => curr_node_ptr     
        line_flags(ext_indx)    =  curr_node_flags
        
        
     END IF

	 ! Look for next point
	 ! On same line..
	 IF( ASSOCIATED(curr_node_ptr%next(idim)%ptr)) THEN
	     curr_node_ptr => curr_node_ptr%next(idim)%ptr
		 
	 !If no more pts on this line, then call get_1_backend() to try to get a pt on the next line	  
	 ELSE IF( .NOT. get_1_backend (plocal_lines_indx_backend(plocal_lines_indx_current), idim, &
	           j, curr_node_ptr, get_one_start,current_level)  ) THEN
!		 ! Not more points
         EXIT !exit the do loop
	 END IF
  END DO
  
  get_active_line_flags = .TRUE.
  RETURN

#ifdef SORT_TO_MATCH_WRK
CALL sort_pick(active_pts_odd, n_active_pts_odd)
CALL sort_pick(active_pts_even, n_active_pts_even)
#endif

END FUNCTION get_active_line_flags

! debugging routine to sort active lists
SUBROUTINE sort_pick(arr,n)
  IMPLICIT NONE
  INTEGER , INTENT(IN) :: n
  INTEGER , DIMENSION(1:n), INTENT(INOUT) :: arr
  !Sorts an array arr into ascending numerical order, by straight insertion. arr is replaced
  !on output by its sorted rearrangement.
  INTEGER :: i,j
  INTEGER :: a

  do j=2,n !Pick out each element in turn.
	a=arr(j)
	do i=j-1,1,-1 !Look for the place to insert it.
		if (arr(i) <= a) exit
		arr(i+1)=arr(i)
	end do
	arr(i+1)=a !Insert it.
  end do

END SUBROUTINE sort_pick
!
! Get the active points on a given line 
! with 1D lvl in idim direction <= j
!
! j_mx - get pts on line associeted with level "j_mx" and below (1D level) .AND.
! j_mn - get points associated with level j_mn and above (1D level)
! j_lev_out - current max level in solver (j_lev) which is different then the
!             max level internal to the db. 

!NOTE so far j_mn arg is not used. 

FUNCTION get_active_line_all_lvl_le_j( idim, j_mn, j_mx, j_lev_out, max_nxyz_out,   mn_var,mx_var, &
     line_u, line_flags,  line_deriv_lev, line_flat_indx ,line_ptrs, line_coords, &
     active_pts, n_active_pts, get_ghost, &
     plocal_lines_indx_backend, plocal_numlines_indx_backend,&
     plocal_lines_indx_current) 
  IMPLICIT NONE
  INTEGER, INTENT(IN) ::  max_nxyz_out
  INTEGER , INTENT(IN)   :: idim,mn_var,mx_var, j_mx,j_lev_out,j_mn
  INTEGER , INTENT(INOUT):: active_pts(max_nxyz_out+1), n_active_pts
  REAL(pr), INTENT(INOUT):: line_u(mn_var:mx_var,0:max_nxyz_out)
  INTEGER(KIND_INT_FLAGS), INTENT(INOUT):: line_flags(0:max_nxyz_out)
  INTEGER , INTENT(INOUT):: line_deriv_lev(0:max_nxyz_out)
  INTEGER , INTENT(INOUT):: line_flat_indx(0:max_nxyz_out)
  INTEGER , INTENT(INOUT):: line_coords(0:max_nxyz_out,dim)  ! coordinates of returned points in terms of j_mx
  INTEGER , INTENT(IN)   :: get_ghost    ! GET_GHOST_PTS = 0, NO_GHOST_PTS=1
  INTEGER ,INTENT(INOUT) :: plocal_numlines_indx_backend ! number of active lines indexed in lines_indx_backend
  TYPE (element_of_face_ptr) , DIMENSION(:), POINTER :: plocal_lines_indx_backend !  list of active line faces
  INTEGER ,INTENT(INOUT) :: plocal_lines_indx_current ! current active line to be returned 
  LOGICAL                :: get_active_line_all_lvl_le_j ! return true as long as there is another line to return
  LOGICAL :: get_one_start 

  TYPE (grid_pt_ptr) , INTENT(INOUT)  :: line_ptrs(0:max_nxyz_out)
  TYPE (grid_pt) , POINTER :: curr_node_ptr  !pointer to current node in db 

  !INTEGER :: ix,iy,iz
  INTEGER :: ixyz_db(1:3), ii, lvl
  INTEGER ::  ext_indx ! index into this line used external to the db routines
!INTEGER ::tmp, tmp2 !TEST
  INTEGER :: current_level ! used in get_1_backend to keep track of the current level


!Shouldn't need to do this, But we do for now because we pull deleted points
! into line arrays for inv transform and these are not cleared in put_active_line()
! because they are not in any of the lists (active_odd/even, new_pts, above pts...
! I guess I need another list deleted pts, just to be able to clear them in
! put_active_line()...
!
!NONEEDED! line_u = 0.0_pr  
!NONEEDED!line_flags = 0

!  PRINT *,'DEBUG get_active_line_all_lvl_le_j() check line_flags(0:max_nxyz_out)'
!  IF( MAXVAL(ABS(line_flags(0:max_nxyz_out))) /=0 ) THEN
!     PRINT *,'ERROR MAXVAL(ABS(line_flags(0:max_nxyz_out))) /=0 '
!	 DO i=0,max_nxyz_out
!        PRINT *,'i, line_flags(i) ', i, line_flags(i)
!	 END DO
!	 STOP
!  END IF

n_active_pts=0

!new
  !
  ! look at points on level j+get_j_above_loc and below (1D)
  !
  plocal_lines_indx_current = plocal_lines_indx_current + 1
  IF( plocal_lines_indx_current > plocal_numlines_indx_backend ) THEN ! check we have more lines to do
      get_active_line_all_lvl_le_j = .FALSE. ; RETURN
  END IF
!  PRINT *,'get_active_line_all_lvl_le_j face coord', lines_indx_backend(lines_indx_current)%ptr%face_coord
!  PRINT *,'j , face lvl ', j_mx , lvl_db(lines_indx_backend(lines_indx_current)%ptr%face_coord)
  curr_node_ptr => NULL()
  get_one_start = .TRUE. !reset start to get first point on line


  ! Get the first point on the line
  IF( .NOT. get_1_backend (plocal_lines_indx_backend(plocal_lines_indx_current), idim, j_mx, &
       curr_node_ptr, get_one_start,current_level) )THEN
     !There should always be atleast one point on a line chosen previously by a call to 
	 ! get_active_line_indx() so this is an error condition
	 PRINT *,'Error in get_active_line_all_lvl_le_j(), Line is empty...Exiting...'
	 STOP
  END IF

!OLD  DO WHILE ( get_1_backend (lines_indx_backend(lines_indx_current), idim, j_mx, curr_node_ptr, get_one_start)  )
   DO
   		 ixyz_db = curr_node_ptr%reg_grid_index ! index relative to j_mx_db

 
		 lvl = lvl_db(curr_node_ptr%reg_grid_index(idim))
		 IF(  (IBITS(curr_node_ptr%flags ,ACTIVE,dim+1 ) /= 0  .OR. &
			   (get_ghost == GET_GHOST_PTS .AND. BTEST(curr_node_ptr%flags, GHOST )) )  &
			  .AND. lvl >= j_mn .AND. lvl <= j_mx ) THEN


			!
			! Get data for this line to pass back
			! Data is passed back in a line nxyz(idim) long based on the
			! current max level j   / (2**(j_mx-j_lev_out))
			ext_indx = ixyz_db(idim)/ (2**(j_mx_db-j_lev_out))
            line_coords(ext_indx,:) = ixyz_db
 
			IF( ext_indx > max_nxyz_out ) THEN
				PRINT *,'ERROR ext_indx > max_nxyz_out'
				stop
			END IF 
			line_u( mn_var:mx_var, ext_indx ) = curr_node_ptr%u(mn_var:mx_var)!  u_in(mn_var:mx_var,  db(ixyz_db(1),ixyz_db(2),ixyz_db(3))%u_indx ) 



			!
			! add to list of active points being returned
			n_active_pts = n_active_pts +1
			active_pts(n_active_pts) = ext_indx


			!
			! Save pointer to find this point when doing the corresponding 
			! put_active_line()
			!
			line_ptrs(ext_indx)%ptr => curr_node_ptr

			line_flags(ext_indx) = curr_node_ptr%flags
			line_deriv_lev( ext_indx ) = curr_node_ptr%deriv_lev
			line_flat_indx( ext_indx ) = curr_node_ptr%indx

		 END IF

     !
	 ! Look for next point
	 ! On same line..
	 IF( ASSOCIATED(curr_node_ptr%next(idim)%ptr)) THEN
	     curr_node_ptr => curr_node_ptr%next(idim)%ptr
		 
	 !If no more pts on this line, then call get_1_backend() to try to get a pt on the next line	  
	 ELSE IF( .NOT. get_1_backend (plocal_lines_indx_backend(plocal_lines_indx_current), &
	                idim, j_mx, curr_node_ptr, get_one_start,current_level)  ) THEN
!		 ! Not more points
         EXIT !exit the do loop
	 END IF


  END DO 






! sort active arrays for debugging only
#ifdef SORT_TO_MATCH_WRK
CALL sort_pick(active_pts, n_active_pts)
#endif

!PRINT *,'Leaving get_active_line_all_lvl_le_j '
!PRINT *,'even ', active_pts(1:n_active_pts)
!PRINT *,'---------------------------'
!PRINT *,''
!PRINT *, ''
  get_active_line_all_lvl_le_j = .TRUE.
  RETURN
END FUNCTION get_active_line_all_lvl_le_j


!
! Get the active points on a given line 
! with 1D lvl in idim direction <= j
!
! j_mx - get pts on line associeted with level "j_mx" and below (1D level) .AND.
! j_mn - get points associated with level j_mn and above (1D level)
! j_lev_out - current max level in solver (j_lev) which is different then the
!             max level internal to the db. 

!NOTE so far j_mn arg is not used. 
!
FUNCTION get_active_line_diff_aux( idim, j_mn, j_mx, j_lev_out, max_nxyz_out,   mn_var,mx_var, &
     line_u, line_flags,  line_deriv_lev, line_flat_indx ,line_ptrs, line_coords, &
     active_pts, n_active_pts, get_ghost, shift, &
     plocal_lines_indx_backend, plocal_numlines_indx_backend,&
     plocal_lines_indx_current) 
  IMPLICIT NONE
  INTEGER, INTENT(IN) ::  max_nxyz_out
  INTEGER , INTENT(IN)   :: idim,mn_var,mx_var, j_mx,j_lev_out,j_mn
  INTEGER , INTENT(INOUT):: active_pts(max_nxyz_out+1), n_active_pts
  REAL(pr), INTENT(INOUT):: line_u(mn_var:mx_var,0:max_nxyz_out)
  INTEGER(KIND_INT_FLAGS), INTENT(INOUT):: line_flags(0:max_nxyz_out)
  INTEGER , INTENT(INOUT):: line_deriv_lev(0:max_nxyz_out)
  INTEGER , INTENT(INOUT):: line_flat_indx(0:max_nxyz_out)
  INTEGER , INTENT(INOUT):: line_coords(0:max_nxyz_out,dim)  ! coordinates of returned points in terms of j_mx
  INTEGER , INTENT(IN)   :: get_ghost    ! GET_GHOST_PTS = 0, NO_GHOST_PTS=1  INTEGER :: shift(0:2**dim), ibndr
  INTEGER , INTENT(IN)   :: shift(0:2*dim)
  INTEGER ,INTENT(INOUT) :: plocal_numlines_indx_backend ! number of active lines indexed in lines_indx_backend
  TYPE (element_of_face_ptr) , DIMENSION(:), POINTER :: plocal_lines_indx_backend !  list of active line faces
  INTEGER ,INTENT(INOUT) :: plocal_lines_indx_current ! current active line to be returned 
  LOGICAL                :: get_active_line_diff_aux ! return true as long as there is another line to return
  LOGICAL :: get_one_start 

  TYPE (grid_pt_ptr) , INTENT(INOUT)  :: line_ptrs(0:max_nxyz_out)
  TYPE (grid_pt) , POINTER :: curr_node_ptr  !pointer to current node in db 

  INTEGER :: ix,iy,iz
  INTEGER :: ixyz_db(1:3), ii, lvl
  INTEGER ::  ext_indx! index into this line used external to the db routines
  INTEGER :: current_level ! used in get_1_backend to keep track of the current level

  !PRINT *,'DEBUG get_active_line_diff_aux() check line_flags(0:max_nxyz_out)'
  !IF( MAXVAL(ABS(line_flags(0:max_nxyz_out))) /=0 ) THEN
  !   PRINT *,'ERROR MAXVAL(ABS(line_flags(0:max_nxyz_out))) /=0 '
!	 DO i=0,max_nxyz_out
  !      PRINT *,'i, line_flags(i) ', i, line_flags(i)
!	 END DO
!	 STOP
!  END IF


n_active_pts=0

!new
  !
  ! look at points on level j+get_j_above_loc and below (1D)
  !
  plocal_lines_indx_current = plocal_lines_indx_current + 1
  IF( plocal_lines_indx_current > plocal_numlines_indx_backend ) THEN ! check we have more lines to do
      get_active_line_diff_aux = .FALSE. ; RETURN
  END IF
!  PRINT *,'get_active_line_all_lvl_le_j face coord', lines_indx_backend(lines_indx_current)%ptr%face_coord
!  PRINT *,'j , face lvl ', j_mx , lvl_db(lines_indx_backend(lines_indx_current)%ptr%face_coord)
  curr_node_ptr => NULL()
  get_one_start = .TRUE. !reset start to get first point on line


  ! Get the first point on the line
  IF( .NOT. get_1_backend (plocal_lines_indx_backend(plocal_lines_indx_current), idim, j_mx, &
      curr_node_ptr, get_one_start,current_level) )THEN
     !There should always be atleast one point on a line chosen previously by a call to 
	 ! get_active_line_indx() so this is an error condition
	 PRINT *,'Error in get_active_line_diff_aux(), Line is empty...Exiting...'
	 STOP
  END IF

!OLD  DO WHILE ( get_1_backend (lines_indx_backend(lines_indx_current), idim, j_mx, curr_node_ptr, get_one_start)  )
   DO
   		 ixyz_db = curr_node_ptr%reg_grid_index ! index relative to j_mx_db

 
		 lvl = lvl_db(curr_node_ptr%reg_grid_index(idim))
		 IF(  (IBITS(curr_node_ptr%flags ,ACTIVE,dim+1 ) /= 0  .OR. &
			   (get_ghost == GET_GHOST_PTS .AND. BTEST(curr_node_ptr%flags, GHOST )) )  &
			  .AND. lvl >= j_mn .AND. lvl <= j_mx ) THEN


			!
			! Get data for this line to pass back
			! Data is passed back in a line nxyz(idim) long based on the
			! current max level j   / (2**(j_mx-j_lev_out))
			ext_indx = ixyz_db(idim)/ (2**(j_mx_db-j_lev_out))
            line_coords(ext_indx,:) = ixyz_db
 
			IF( ext_indx > max_nxyz_out ) THEN
				PRINT *,'ERROR ext_indx > max_nxyz_out'
				stop
			END IF 
			line_u( mn_var:mx_var, ext_indx ) = curr_node_ptr%u(mn_var:mx_var)!  u_in(mn_var:mx_var,  db(ixyz_db(1),ixyz_db(2),ixyz_db(3))%u_indx ) 



			!
			! add to list of active points being returned
			n_active_pts = n_active_pts +1
			active_pts(n_active_pts) = ext_indx


			!
			! Save pointer to find this point when doing the corresponding 
			! put_active_line()
			!
			line_ptrs(ext_indx)%ptr => curr_node_ptr

			line_flags(ext_indx) = curr_node_ptr%flags
			line_deriv_lev( ext_indx ) = curr_node_ptr%deriv_lev

!no shift
!			line_flat_indx( ext_indx ) = curr_node_ptr%indx

!w/shift
! need wlt_type? or can it be just 1, 
! face type from this node..
             line_flat_indx( ext_indx ) = curr_node_ptr%indx + shift( curr_node_ptr%ibndr) 
!
		 END IF

     !
	 ! Look for next point
	 ! On same line..
	 IF( ASSOCIATED(curr_node_ptr%next(idim)%ptr)) THEN
	     curr_node_ptr => curr_node_ptr%next(idim)%ptr
		 
	 !If no more pts on this line, then call get_1_backend() to try to get a pt on the next line	  
	 ELSE IF( .NOT. get_1_backend (plocal_lines_indx_backend(plocal_lines_indx_current), &
	                idim, j_mx, curr_node_ptr, get_one_start,current_level)  ) THEN
!		 ! Not more points
         EXIT !exit the do loop
	 END IF


  END DO 






! sort active arrays for debugging only
#ifdef SORT_TO_MATCH_WRK
CALL sort_pick(active_pts, n_active_pts)
#endif

!PRINT *,'Leaving get_active_line_all_lvl_le_j '
!PRINT *,'even ', active_pts(1:n_active_pts)
!PRINT *,'---------------------------'
!PRINT *,''
!PRINT *, ''
  get_active_line_diff_aux = .TRUE.
  RETURN
END FUNCTION get_active_line_diff_aux

!
! Get the active points on a given line 
! with 1D lvl in idim direction <= j
!
! j - get pts on line associeted with level "j" and below (1D level)
! j_mn -get points associated with level j_mn and above (3D level)
! j_lev_out - current max level in solver (j_lev) which is different then the
!             max level internal to the db.

!NOTE arg j_mn is not used and can be removed..D.G. 
FUNCTION get_active_line_flags_deriv_lev( idim, j, j_mn, j_lev_out, max_nxyz_out,    &
      line_flags, line_deriv_lev, line_ptrs, line_coords, &
     active_pts, n_active_pts,get_del , get_ghost, &
     plocal_lines_indx_backend, plocal_numlines_indx_backend,&
     plocal_lines_indx_current) 
  IMPLICIT NONE
  INTEGER, INTENT(IN) ::  max_nxyz_out
  INTEGER , INTENT(IN)   :: idim, j,j_lev_out,j_mn
  INTEGER , INTENT(INOUT):: active_pts(max_nxyz_out+1), n_active_pts
 
  INTEGER(KIND_INT_FLAGS), INTENT(INOUT):: line_flags(0:max_nxyz_out)
  INTEGER , INTENT(INOUT):: line_deriv_lev(0:max_nxyz_out)
  INTEGER , INTENT(INOUT):: line_coords(0:max_nxyz_out,dim)  ! coordinates of returned points in terms of j_mx
  INTEGER , INTENT(IN)   :: get_ghost    ! GET_GHOST_PTS = 0, NO_GHOST_PTS=1
  INTEGER , INTENT(IN)   :: get_del ! 0- get no pts marked DELETE
                                    ! 1- put DELETE points in line but not in odd/even pts list (for inverse transform after adapt_grid)
                                    ! 2- put DELETE points in line AND in  pts list 
  INTEGER ,INTENT(INOUT) :: plocal_numlines_indx_backend ! number of active lines indexed in lines_indx_backend
  TYPE (element_of_face_ptr) , DIMENSION(:), POINTER :: plocal_lines_indx_backend !  list of active line faces
  INTEGER ,INTENT(INOUT) :: plocal_lines_indx_current ! current active line to be returned 
  LOGICAL                :: get_active_line_flags_deriv_lev ! return true as long as there is another line to return
  LOGICAL :: get_one_start 

  TYPE (grid_pt_ptr) , INTENT(INOUT)  :: line_ptrs(0:max_nxyz_out)
  TYPE (grid_pt) , POINTER :: curr_node_ptr  !pointer to current node in db 

  INTEGER :: ix,iy,iz
  INTEGER :: ixyz_db(1:3), ii, lvl
  INTEGER ::  ext_indx! index into this line used external to the db routines
  INTEGER :: current_level ! used in get_1_backend to keep track of the current level


!  !PRINT *,'DEBUG get_active_line_flags_deriv_lev() check line_flags(0:max_nxyz_out)'
!  IF( MAXVAL(ABS(line_flags(0:max_nxyz_out))) /=0 ) THEN
!     PRINT *,'ERROR MAXVAL(ABS(line_flags(0:max_nxyz_out))) /=0 '
!	 DO i=0,max_nxyz_out
!        PRINT *,'i, line_flags(i) ', i, line_flags(i)
!	 END DO
!	 STOP
!  END IF

!Shouldn't need to do this, But we do for now because we pull deleted points
! into line arrays for inv transform and these are not cleared in put_active_line()
! because they are not in any of the lists (actove_odd/even, new_pts, above pts...
! I guess I need another list deleted pts, just to be able to clear them in
! put_active_line()...
!

line_flags = 0  !DELETE THIS 
n_active_pts=0

!new
  !
  ! look at points on level j+get_j_above_loc and below (1D)
  !
  plocal_lines_indx_current = plocal_lines_indx_current + 1
  IF( plocal_lines_indx_current > plocal_numlines_indx_backend ) THEN ! check we have more lines to do
      get_active_line_flags_deriv_leV = .FALSE. ; RETURN
  END IF

!  PRINT *,'get_active_line_flags_deriv_lev face coord', lines_indx_backend(lines_indx_current)%ptr%face_coord
!  PRINT *,'j , face lvl ', j , lvl_db(lines_indx_backend(lines_indx_current)%ptr%face_coord)
  get_one_start = .TRUE. !reset start to get first point on line
  curr_node_ptr => NULL()

  ! Get the first point on the line
  IF( .NOT. get_1_backend (plocal_lines_indx_backend(plocal_lines_indx_current), idim, j, &
      curr_node_ptr, get_one_start,current_level) )THEN
     !There should always be atleast one point on a line chosen previously by a call to 
	 ! get_active_line_indx() so this is an error condition
	 PRINT *,'Error in get_active_line_flags_deriv_lev(), Line is empty...Exiting...'
	 STOP
  END IF

!  DO WHILE ( get_1_backend (lines_indx_backend(lines_indx_current), idim, j, curr_node_ptr, get_one_start)  )
  DO     
	 ixyz_db = curr_node_ptr%reg_grid_index ! index relative to j_mx_db


     lvl = lvl_db(curr_node_ptr%reg_grid_index(idim)) 
     IF(  (IBITS(curr_node_ptr%flags ,ACTIVE,dim+1 ) /= 0  .OR. &
           (get_del > 0 .AND. BTEST(curr_node_ptr%flags, DELETE ) ) .OR. &
		   (get_ghost == GET_GHOST_PTS .AND. BTEST(curr_node_ptr%flags, GHOST )) )  &
!not used		   .AND. db_find_lvl_3d(db(ixyz_db(1),ixyz_db(2),ixyz_db(3))%reg_grid_index ) > j_mn &
          .AND. lvl <= j ) THEN

        !
        ! Get data for this line to pass back
        ! Data is passed back in a line nxyz(idim) long based on the
        ! current max level j   / (2**(j_mx-j_lev_out))
        ext_indx = ixyz_db(idim)/ (2**(j_mx_db-j_lev_out))
		line_coords(ext_indx,:) = ixyz_db

		IF( ext_indx > max_nxyz_out ) THEN
			PRINT *,'ERROR ext_indx > max_nxyz_out'
			stop
		END IF 


		!return the active points in the line and include the delete points in pts list
		! if called for
        IF( IBITS(curr_node_ptr%flags ,ACTIVE,dim+1 ) /= 0  .OR. &
             (get_del == 2 .AND. BTEST(curr_node_ptr%flags, DELETE ) ) .OR. &
		   (get_ghost == GET_GHOST_PTS .AND. BTEST(curr_node_ptr%flags, GHOST )) )THEN

			!
			! add to list of active points being returned
			n_active_pts = n_active_pts +1
			active_pts(n_active_pts) = ext_indx

	    END IF

		!
		! Save pointer to find this point when doing the corresponding 
		! put_active_line()
		!
		line_ptrs(ext_indx)%ptr => curr_node_ptr

		!get values
		line_flags(ext_indx) = curr_node_ptr%flags
		line_deriv_lev( ext_indx ) = curr_node_ptr%deriv_lev

    END IF

		 ! Look for next point
	 ! On same line..
	 IF( ASSOCIATED(curr_node_ptr%next(idim)%ptr)) THEN
	     curr_node_ptr => curr_node_ptr%next(idim)%ptr
		 
	 !If no more pts on this line, then call get_1_backend() to try to get a pt on the next line	  
	 ELSE IF( .NOT. get_1_backend (plocal_lines_indx_backend(plocal_lines_indx_current), idim, &
	                                j, curr_node_ptr, get_one_start,current_level)  ) THEN
!		 ! Not more points
         EXIT !exit the do loop
	 END IF

 END DO

! sort active arrays for debugging only
#ifdef SORT_TO_MATCH_WRK
CALL sort_pick(active_pts, n_active_pts)
#endif

!PRINT *,'Leaving get_active_line_flags_deriv_lev '
!PRINT *,'even ', active_pts(1:n_active_pts)
!PRINT *,'---------------------------'
!PRINT *,''
!PRINT *, ''
  get_active_line_flags_deriv_lev = .TRUE.
  RETURN

END FUNCTION get_active_line_flags_deriv_lev



!
! Put the active points on a given line for a given level transform
! This routine will zero out the locations in line that were used so in the
! end the line is all zeros and ready for reuse
!
!

!$$$$$$$$$$$  j not used in args..

SUBROUTINE put_active_line( idim, j, j_lev_out, max_nxyz_out,  mn_var,mx_var, &
     line_u, do_update_u , line_flags, line_ptrs,  &
     active_pts_odd, n_active_pts_odd, &
     active_pts_even, n_active_pts_even, &
     j_above, n_j_above, new_pts, n_new_pts , delete_pts, n_delete_pts, delete_flag ) 
  IMPLICIT NONE
  INTEGER , INTENT(IN)    ::  max_nxyz_out
  INTEGER , INTENT(IN)    :: idim,mn_var,mx_var, j
  INTEGER , INTENT(IN)    :: j_lev_out
  INTEGER , INTENT(IN)    :: active_pts_odd(max_nxyz_out+1), n_active_pts_odd
  INTEGER , INTENT(IN)    :: active_pts_even(max_nxyz_out+1), n_active_pts_even
  INTEGER , INTENT(IN)    :: j_above(max_nxyz_out+1), n_j_above
  INTEGER , INTENT(IN)    :: new_pts(max_nxyz_out+1), n_new_pts
  INTEGER , INTENT(IN)    :: delete_pts(max_nxyz_out+1), n_delete_pts
  INTEGER , INTENT(IN)    :: delete_flag ! DELETE_FROM_DB, or DELETE_ZERO_ONLY, DELETE_UPDATE (used to undelete points)
  REAL(pr), INTENT(INOUT) :: line_u(mn_var:mx_var,0:max_nxyz_out)
  INTEGER(KIND_INT_FLAGS), INTENT(INOUT) :: line_flags(0:max_nxyz_out)
!  INTEGER , INTENT(IN)    :: line_coords(0:max_nxyz_out,dim)  ! coordinates of returned points in terms of j_mx
  INTEGER , INTENT(IN)    :: do_update_u
  INTEGER                 :: face_coord(1:dim)
  TYPE (grid_pt_ptr) , INTENT(INOUT)  :: line_ptrs(0:max_nxyz_out)
  INTEGER :: ix,iy,iz
  INTEGER :: ixyz_db(1:3), ii,  lvl
  INTEGER ::  dbscale ! scale for indices between db and external index

  integer tmp !test

  !
  ! Update active odd points from list
  !
  DO ii=1,n_active_pts_odd



     ! put result back into u
	 IF( do_update_u == UPDATE_U ) THEN
		line_ptrs(active_pts_odd(ii) )%ptr%u(mn_var:mx_var)  = &
          line_u(mn_var:mx_var, active_pts_odd(ii))
     END IF
     line_u(mn_var:mx_var, active_pts_odd(ii)) = 0.0_pr


     ! Update flags
     line_ptrs(active_pts_odd(ii) )%ptr%flags  = &
          line_flags( active_pts_odd(ii) )

     line_flags( active_pts_odd(ii) ) = 0 !zero out (all false) flags

  END DO

  !
  ! Update active even points from list
  !
  DO ii=1,n_active_pts_even

     ! put result back into u
 	 IF( do_update_u == UPDATE_U ) THEN
		line_ptrs(active_pts_even(ii) )%ptr%u(mn_var:mx_var)  = &
          line_u(mn_var:mx_var, active_pts_even(ii))
	END IF
     line_u(mn_var:mx_var, active_pts_even(ii)) = 0.0_pr
     ! Update flags
     line_ptrs(active_pts_even(ii) )%ptr%flags  = &
          line_flags( active_pts_even(ii) )
     line_flags( active_pts_even(ii) ) = 0 !zero out (all false) flags
  END DO

  !
  ! Update active j_above points from list
  !
  DO ii=1,n_j_above


     ! put result back into u
	 IF( do_update_u == UPDATE_U ) THEN
		line_ptrs(j_above(ii) )%ptr%u(mn_var:mx_var)  = &
          line_u(mn_var:mx_var, j_above(ii))
	 END IF
     line_u(mn_var:mx_var, j_above(ii)) = 0.0_pr

     ! Update flags
     line_ptrs(j_above(ii) )%ptr%flags  = &
          line_flags( j_above(ii) )
     line_flags( j_above(ii) ) = 0 !zero out (all false) flags

  END DO

  !
  ! Add any new points on the line
  ! 
  !
  dbscale = (2**(j_mx_db-j_lev_out)) 

  !
  !find a face coordinate
  ! This relies on assumption that if there are new_pts or delete_pts then there must be
  ! some active point on this line..
  !
  IF( n_new_pts > 0) THEN
     IF(n_active_pts_even>0) THEN
	    face_coord = line_ptrs(active_pts_even(ii) )%ptr%reg_grid_index(:)
     ELSE IF(n_active_pts_odd>0) THEN
	    face_coord = line_ptrs(active_pts_odd(ii) )%ptr%reg_grid_index(:)
     ELSE IF(n_j_above>0) THEN
	    face_coord = line_ptrs(j_above(ii) )%ptr%reg_grid_index(:)
     ELSE
	    PRINT *,'ERROR in put_active_line(), No face coorinate found..Exiting..'
		STOP
	 END IF
  END IF



  !ixyz_db =  line_indx
  DO ii=1,n_new_pts


     ! index in the direction of idim
     ! new_pts is external (local) indexing so we multiply by dbscale
!	 ixyz_db = lines_indx_backend(lines_indx_current)%ptr%face_coord !lines_indx(ln,:)
     ixyz_db = face_coord
     ixyz_db(idim) = new_pts(ii) * dbscale


     !
	 ! Add the node to the DB
	 !
	 !  SUBROUTINE add_node_lines_db_backend (3Dlevel, coord, 
     !  flags, deriv_lev, truncated_deriv_lev, indx)
	 !
	 ! Set deriv_lev and indx to -1. In the case of an active point these will get reset
	 ! before use. In this way ghost points will end up with -1 for these values
	 CALL add_node_lines_db_backend ( &
		lvl_db(ixyz_db(1:dim)), &
		ixyz_db, &
        line_flags(  new_pts(ii) ),  &
		-1, &
		-1, &
		-1)





     line_flags( new_pts(ii) ) = 0 !zero out (all false) flags

     line_u(mn_var:mx_var, new_pts(ii)) = 0.0_pr

  END DO

  !
  ! delete old points on the line
  ! 
  !
  DO ii=1,n_delete_pts
	 IF( delete_flag == DELETE_UPDATE ) THEN ! update flags

		line_ptrs(delete_pts(ii) )%ptr%flags = line_flags(  delete_pts(ii) )
		line_ptrs(delete_pts(ii) )%ptr%ibndr  = 0 !incase this point comes back as an interior point (ibndr==0)
	 END IF

     !
     ! for all cases we clear line_flags and line_u for this point.
	 !
     line_flags( delete_pts(ii) ) = 0 !zero out (all false) flags

     line_u(mn_var:mx_var, delete_pts(ii)) = 0.0_pr

  END DO


!if( SUM(line_flags) /= 0 ) THEN
! print *,''
!END IF
END SUBROUTINE put_active_line

!
! Put the active points on a given line for a given level transform
! This routine will zero out the locations in line that were used so in the
! end the line is all zeros and ready for reuse
!
!
SUBROUTINE put_active_line_deriv_lev(  max_nxyz_out, line_deriv_lev, line_ptrs, &
     active_pts, n_active_pts) 
  IMPLICIT NONE
  INTEGER, INTENT(IN) ::  max_nxyz_out
  INTEGER , INTENT(INOUT):: active_pts(max_nxyz_out+1), n_active_pts
  INTEGER , INTENT(INOUT):: line_deriv_lev(0:max_nxyz_out)
  TYPE (grid_pt_ptr) , INTENT(INOUT)  :: line_ptrs(0:max_nxyz_out)
  INTEGER :: ii



  DO ii=1,n_active_pts

     ! Update deriv_lev
     line_ptrs(active_pts(ii) )%ptr%deriv_lev  = &
          line_deriv_lev( active_pts(ii) )

     line_deriv_lev( active_pts(ii) ) = 0 !zero out (all false) flags

  END DO



END SUBROUTINE put_active_line_deriv_lev



SUBROUTINE db_update_grid(j_lev_new,j_out,j_mn, max_nxyz_out)
  USE precision
  IMPLICIT NONE
  INTEGER, INTENT (IN) ::  j_mn

  INTEGER, INTENT (IN) :: j_out, j_lev_new
  INTEGER, INTENT (IN) :: max_nxyz_out
  INTEGER :: i, ie, ii
  INTEGER :: ixyz_db(3),ixyz_loc(3)
  INTEGER :: idim, j_tmp, db_scale
  !integer:: ix,iy,iz !test
  ! arguments for db routines (have to allocate these....
  INTEGER :: n_active_pts_odd,n_active_pts_even, n_new_pts, n_j_above, n_delete_pts
  INTEGER :: indx_line !index into current line being processed
  INTEGER :: j_lev_new_chk ! j_lev returned after  deleting nodes..
  INTEGER (KIND_INT_FLAGS)       :: tmp_j_flag 
  INTEGER :: plocal_numlines_indx_backend ! number of active lines indexed in lines_indx_backend
  TYPE (element_of_face_ptr) , DIMENSION(:), POINTER :: plocal_lines_indx_backend !  list of active line faces
  INTEGER :: plocal_lines_indx_current ! current active line to be returned 

  !SHOULD BE ON STACK!!!
  INTEGER ,ALLOCATABLE ,SAVE ,DIMENSION(:) :: active_pts_odd
  INTEGER ,ALLOCATABLE ,SAVE ,DIMENSION(:) :: active_pts_even
  INTEGER ,ALLOCATABLE ,SAVE ,DIMENSION(:) :: j_above
  INTEGER ,ALLOCATABLE ,SAVE ,DIMENSION(:) :: new_pts
  INTEGER ,ALLOCATABLE ,SAVE ,DIMENSION(:) :: delete_pts
  REAL(pr),ALLOCATABLE ,SAVE ,DIMENSION(:,:) :: line_u
  INTEGER ,ALLOCATABLE ,SAVE ,DIMENSION(:,:) :: line_coords
  INTEGER(KIND_INT_FLAGS),ALLOCATABLE ,SAVE ,DIMENSION(:) :: line_flags
  TYPE (grid_pt_ptr) ,ALLOCATABLE  ,SAVE ,DIMENSION(:)  :: line_ptrs
  INTEGER :: odim(1:dim-1) ! the face coordinates of the dimensions other then idim 

!$OMP threadprivate(active_pts_odd,active_pts_even,j_above,new_pts,delete_pts,line_u,line_coords)
!$OMP threadprivate(line_flags,line_ptrs)
  db_scale = (2**(j_mx_db-j_out))


  PRINT *,'Entering db_update_grid(), db_nwlt,j_out,max_nxyz_out,j_mn = ', db_nwlt,j_out,max_nxyz_out,j_mn
  !CALL info_check_backend (db_nwlt, db_ghost, IBSET(0,GHOST), j_out)
  !PRINT *,'info_check db_nwlt, db_ghost', db_nwlt, db_ghost
  !CALL info_check_backend (db_nwlt, db_ghost, IBSET(0,DELETE), j_out)
  !PRINT *,'info_check total, delete', db_nwlt, db_ghost

  db_nwlt = 0 
  db_ghost = 0

  ! allocate the arrays used in db access routines
  ALLOCATE( active_pts_odd(max_nxyz_out+1)  )
  ALLOCATE( active_pts_even(max_nxyz_out+1) )
  ALLOCATE( j_above(max_nxyz_out+1) )
  ALLOCATE( new_pts(max_nxyz_out+1)       )
  ALLOCATE( delete_pts(max_nxyz_out+1)    )
  ALLOCATE( line_u(1:n_var,0:max_nxyz_out))
  ALLOCATE( line_flags(0:max_nxyz_out)    )
  ALLOCATE( line_coords(0:max_nxyz_out,dim)   )
  ALLOCATE( line_ptrs(0:max_nxyz_out)     )

  line_u = 0.0_pr ! make sure the line array is zeroed out
  line_flags = 0  ! make sure the line flags array is zeroed out

  n_new_pts = 0 ! we add no new points so we can do this once outside of get_active_line loop
  n_delete_pts = 0 

  !
  ! find deleted wlts (may be able to do in one loop below??
  !
  !  j = j_out-1 ! we want to get all points in this loop, so we get 
            ! points for level j_out (current maximum level)
  idim = 1  ! only look from first dimension here.
  j_tmp = 0  !
  CALL get_active_lines_indx( idim, j_out ,1 , GET_ALL_DERIV_LEV, &
           plocal_lines_indx_backend, plocal_numlines_indx_backend,&
		   plocal_lines_indx_current ) !4/18/05 DG changed from j_out+1 to j_out

  !DO ln = 1, nlines

	!
	!  get all active pts on this line
	!  
	!ln = 1 !TEST only
  DO WHILE ( get_active_line( idim, j_out, j_out ,max_nxyz_out,  1, n_var, &
		line_u, line_flags, line_ptrs, line_coords, odim, &
		active_pts_odd, n_active_pts_odd, &
		active_pts_even, n_active_pts_even,&
		j_above, n_j_above, delete_pts, n_delete_pts, GET_DEL_INLST ,NO_GET_J_ABOVE ,&
		LINE_ONE, GET_GHOST_PTS,GET_ALL_PTS, SORT_PTS_TRUE, &
           plocal_lines_indx_backend, plocal_numlines_indx_backend,&
		   plocal_lines_indx_current) )

	!ixyz_db(:)  = lines_indx(ln,:)
	!ixyz_loc(:) = ixyz_db(:) /db_scale 


	DO i=1,n_active_pts_even
        indx_line = active_pts_even(i)
		ixyz_db   = line_coords(indx_line,:)
		ixyz_loc  = ixyz_db / db_scale 

        !ixyz_loc(idim) = indx_line
        !ixyz_db(idim)  = ixyz_loc(idim) * db_scale 



        !
        ! review points,
		! if ghost clear delete flag
        ! if they are to be deleted then put them in delete list
		! else if they are to be kept then we need to clear all but the ACTIVE flag
		!
        IF( BTEST(line_flags(indx_line), GHOST )  ) THEN

			! ghost points can have delete flag true because they were deleted as active or adjacent
			! but have now become ghost. THis is necessary so these points will get
			! used in the inverse transform done between the adding ghost points and
			! this call to update_grid()
			line_flags(indx_line) = IBCLR( line_flags(indx_line) , DELETE )

			line_u(:,indx_line) = 0.0_pr ! zero out any ghost pts here so they are ready for deriviative.
			db_ghost = db_ghost+1	

           ! track highest level of significant+adjacent+ghost
           j_tmp = MAX(MAXVAL(lvl_db(ixyz_db(1:dim))),j_tmp) 

	
		ELSE

			IF( BTEST(line_flags(indx_line),DELETE )  .AND. MAXVAL(lvl_db(ixyz_db(1:dim)))> j_mn ) THEN

			    !
				! Add this point to delet list so it will get deleted in put_active_line() below
				!
				n_delete_pts = n_delete_pts + 1
				delete_pts(n_delete_pts) = indx_line

				!
				IF( BTEST(line_flags(indx_line), BND)  ) THEN
					db_n_bnd_total = db_n_bnd_total - 1 ! keep track of number of active boundary points
				END IF



			ELSE

				! keep this point
				! count all none ghost pts
!WRITE(6,'( 3( I3.3 , 1X ) ,"GridPt Flags:", I3.3 )' ) ixyz_db,line_flags(indx_line) 
				!we know we are nto ghost because of if above.. IF( .NOT. BTEST(line_flags(indx_line), GHOST ) ) 
				db_nwlt =db_nwlt +1

				!NOTE we can clear all flags except active here is we do not use the bnd flag, but
				! check about ghost pts first.

				line_flags(indx_line) = IBSET( line_flags(indx_line) , ACTIVE )

				!clear the adjacent flags
				line_flags(indx_line) = &
                         IBCLR( line_flags(indx_line) , ADJACENT_X )
				line_flags(indx_line) = &
                         IBCLR( line_flags(indx_line) , ADJACENT_Y )
				IF( dim == 3 ) line_flags(indx_line) = &
                         IBCLR( line_flags(indx_line) , ADJACENT_Z )
				line_flags(indx_line) = &
                         IBCLR( line_flags(indx_line) , DELETE )
				line_flags(indx_line) = &
                         IBCLR( line_flags(indx_line) , SIG_FLAG )

	            ! track highest level of significant+adjacent+ghost
                j_tmp = MAX(MAXVAL(lvl_db(ixyz_db(1:dim))),j_tmp) 

			END IF
		END IF
	END DO


	!
	! put line back and update points that have been deleted
	!
	CALL put_active_line( idim, j_out, j_out, max_nxyz_out, 1,n_var, &
		line_u, UPDATE_U, line_flags, line_ptrs, &
		active_pts_odd, n_active_pts_odd, &
		active_pts_even, n_active_pts_even, &
		j_above, n_j_above, new_pts, n_new_pts, delete_pts, n_delete_pts, DELETE_FROM_DB )


  END DO !DO ln = 1,nlines

  ! deallocate the arrays used in db access routines
  DEALLOCATE( active_pts_odd )
  DEALLOCATE( active_pts_even)
  DEALLOCATE( j_above        )
  DEALLOCATE( new_pts        )
  DEALLOCATE( delete_pts     )
  DEALLOCATE( line_u         )
  DEALLOCATE( line_flags     )
  DEALLOCATE( line_coords    )
  DEALLOCATE( line_ptrs      )
  CALL deallocate_active_lines_indx(plocal_lines_indx_backend)

  !
  ! traverse db and delete all points with delete flag still set.
  !
  !
  ! Use tmp variable of type INTEGER(KIND_INT_FLAGS) set with
  ! proper flags to get correct
  ! type when passing flag argument to delete_some_backend()
  ! 
  tmp_j_flag = IBSET( INT(0,KIND_INT_FLAGS), DELETE )
!NOTE NEED TO FOLD WHAT IS DONE IN SWEEP ABOVE INTO delete_some_backend() dg
  
  CALL delete_some_backend(  tmp_j_flag , j_lev_new_chk)

  !check
  IF( j_lev_new_chk /= j_tmp ) THEN
    Print *,' in db_update_grid, j_lev_new_chk /= j_lev_new '
	    Print *,' in db_update_grid, j_lev_new_chk /= j_lev_new '
	    Print *,'j_lev_new_chk:',  j_lev_new_chk , ' j_lev_new:',j_lev_new
	    Print *,'Exiting...'
	STOP
  END IF


  PRINT *,'Leaving db_update_grid(), db_nwlt            = ', db_nwlt
  PRINT *,'Leaving db_update_grid(), db_ghost           = ', db_ghost
  PRINT *,'Leaving db_update_grid(), db_ghost + db_nwlt = ', db_ghost + db_nwlt
  PRINT *,'Leaving db_update_grid(), j_out,max_nxyz_out,j_mn = ', j_out,max_nxyz_out,j_mn

  !CALL info_check_backend (db_nwlt, db_ghost, IBSET(0,GHOST), j_out)
  !PRINT *,'info_check db_nwlt, db_ghost', db_nwlt, db_ghost

END SUBROUTINE db_update_grid



!
! This is a utility routine that is used to print the status of the db
! flags for debugging
!
FUNCTION char_flags(flags)
  IMPLICIT NONE
  CHARACTER :: char_flags(0:MAX_FLAG+1)
  INTEGER (KIND_INT_FLAGS):: flags,i

  DO i=0,MAX_FLAG
     IF(BTEST( flags, i ) ) THEN
        char_flags(i) = 'T'
     ELSE
        char_flags(i) = 'F'
     END IF
  END DO
char_flags(MAX_FLAG+1) = '.'
END FUNCTION char_flags

END MODULE db_mod
