!--**********************************    
!--Subroutines to solve Lu(u)=f
!--**********************************


MODULE elliptic_mod
  USE debug_vars  
  USE wlt_trns_mod
  USE wlt_vars
  USE elliptic_vars

  LOGICAL :: OLEG_CHANGE = .FALSE.
  ! this is related to xlf compiler
  ! (see math.f90 for details)
#ifdef OLOCAL_MATMUL
#define MATMUL(A,B) (LMATMUL(A,B,SIZE(A,DIM=1),SIZE(A,DIM=2),SIZE(B)))
  USE LOCAL_MATH
#endif

!!$ DEBUG
!!$#define DEBUG_GMRES
!!$#define DEBUG_JACOBY
#define NO_wlog_diagnostics_elliptic     

CONTAINS

  !--**********************************    
  !--Begin subroutines to solve Lu(u)=f
  !--**********************************
  SUBROUTINE Linsolve (u_local, f_local, tol, nlocal, ne_local, clip, Lulocal, Lulocal_diag, SCL)
    !--Solves equation Lu(u) = f for u.
    USE precision
    USE wlt_vars
    USE elliptic_vars
    USE util_vars       ! dA
    USE pde
    USE util_vars
    USE parallel
    IMPLICIT NONE

    INTEGER,                          INTENT (IN)    :: nlocal, ne_local
    INTEGER, DIMENSION(ne_local),           INTENT (IN)    :: clip
    REAL (pr),                        INTENT (IN)    :: tol
    REAL (pr), DIMENSION (nlocal, ne_local), INTENT (INOUT) :: u_local
    REAL (pr), DIMENSION (nlocal, ne_local), INTENT (INOUT) :: f_local
    REAL (pr), DIMENSION (ne_local), INTENT (IN), OPTIONAL :: SCL
    REAL (pr) :: u_clip


    INTEGER, DIMENSION(ne_local)                           :: noclip
    INTEGER, PARAMETER  :: j_up = 0, j_down = 1
    INTEGER             :: j, jmin, jmax, k, m, ie, repeart_corrections, ii
    INTEGER             :: kmax, trnsf_type
    INTEGER,   DIMENSION (j_lev)           :: iclip
    REAL (pr), DIMENSION (nlocal, ne_local,j_lev) :: res, v
    REAL (pr), DIMENSION (ne_local)              :: error, scl_u, scl_f, scl_local, tolh
    REAL (pr)                              :: floor_rhs
    REAL (pr), DIMENSION (ne_local) :: w_correction
    INTERFACE
       FUNCTION Lulocal (jlev, u_local, nlocal, ne_local, meth)
         USE precision
         USE sizes
         USE pde
         IMPLICIT NONE
         INTEGER, INTENT (IN) :: jlev, nlocal, ne_local, meth
         REAL (pr), DIMENSION (nlocal*ne_local), INTENT (INOUT) :: u_local
         REAL (pr), DIMENSION (nlocal*ne_local) :: Lulocal
       END FUNCTION Lulocal
       FUNCTION Lulocal_diag (jlev,  nlocal, ne_local, meth)
         USE precision
         USE sizes
         USE pde
         IMPLICIT NONE
         INTEGER, INTENT (IN) :: jlev, nlocal, ne_local, meth
         REAL (pr), DIMENSION (nlocal*ne_local) :: Lulocal_diag
       END FUNCTION Lulocal_diag
    END INTERFACE
    
    INTEGER ::nlocal_global, m_global
    REAL(pr) :: tmp1, tmp2, tmp3
    
    IF(diagnostics_elliptic) THEN
       PRINT *,' '
       PRINT *,'------------------------'
       PRINT *,'Linsolve: solve Lu(u)=f '
    END IF

   IF(elliptic_zone_active) THEN
      CALL setup_elliptic_zone(nlocal)
   END IF

   IF(GS_active) THEN
      CALL setup_GS(nlocal)
   END IF

    v = 0.0_pr
    res = 0.0_pr

    noclip = 0
    floor_rhs = 1.e-12_pr
    
    !--Clipping
    DO j = 1, j_lev
       iclip(j) = i_clip(j)
    END DO

    DO ie = 1, ne_local 
       IF(clip(ie) == 1 .AND. .NOT.elliptic_project) THEN
          u_clip = u_local(iclip(j_lev),ie) 
          CALL parallel_broadcast( REAL=u_clip )
          u_local(1:nlocal,ie)= u_local(1:nlocal,ie)-u_clip
       ELSE IF(clip(ie) == 1 .AND. elliptic_project) THEN
          u_clip = 0.0_pr
          u_clip = SUM(u_local(1:nlocal,ie)*dA_level(1:nlocal,j_lev))
          CALL parallel_global_sum( REAL=u_clip )
          u_clip = u_clip/sumdA_global
          u_local(1:nlocal,ie)= u_local(1:nlocal,ie)-u_clip
       END IF
    END DO
    !  
    !--Solve problem Lu(u)=f
    DO ie = 1, ne_local  !L2-norm
       tmp1 = SUM(f_local(:,ie)**2*dA)
       tmp2 = SUM(u_local(:,ie)**2*dA)
       CALL parallel_global_sum( REAL=tmp1 )
       CALL parallel_global_sum( REAL=tmp2 )
       scl_f(ie) =  SQRT( tmp1 )
       scl_u(ie) =  SQRT( tmp2 )
    END DO
    scl_local = MAX(scl_u,scl_f)

    IF( PRESENT(SCL) ) scl_local = MAX(scl_local, SCL)
    WHERE( scl_local < floor_rhs ) scl_local = 1.0_pr ! if scl_local is small, it becomes absolute error
    
    tolh = tol * scl_local
    
    !---------- Standard multigrid strategy ------------------------
#ifdef MULTIPROC                                   !05.18.2011!O+AR!
    jmin = j_tree_root
    IF ( j_tree_root .GT. j_lev ) THEN
       IF (par_rank.EQ.0)  THEN
          WRITE (*,'(A)')                ' ERROR: In Linsolve() [elliptic_solve_3d.f90]:  j_tree_root  must be <  j_lev '
          WRITE (*,'(A, A, I3, A, I3)')  ' j_tree_root is high, set it at the most equal to j_lev', '     j_tree_root=',j_tree_root, '   j_lev=',j_lev
          PRINT *,                       ' Exiting ...'
       END IF
       CALL parallel_finalize; STOP                   
    END IF
#else
    jmin = 1  
#endif
    jmax = j_lev

    
    IF ( ANY( Nwlt_lev(jmin:jmax,1) .LE. 0 ))   THEN
       DO j = jmin, jmax
         IF ( Nwlt_lev(j,1) .LE. 0 ) WRITE (*,'(A, I6, A, I3, A, I9)')  'rank=',par_rank, '   Nwlt_lev(j=',j,',1)=',Nwlt_lev(j,1)
       END DO
       IF (par_rank.EQ.0)  THEN
          WRITE (*,'(A)')                ' ERROR: In Linsolve() [elliptic_solve_3d.f90]:  nlocal=Nwlt_lev(j,1)=0  must be >  0 '
          WRITE (*,'(A)')                ' j_tree_root is high, reduce it (not less than j_min)'
          PRINT *,                       ' Exiting ...'
       END IF
       CALL parallel_finalize; STOP                   
    END IF
    

    
    IF(diagnostics_elliptic) THEN
       PRINT *,'Initial iterative solver'
       PRINT *, jmax, kry_p, nlocal, ne_local, HIGH_ORDER, clip
       PRINT *, MINVAL(u_local), MAXVAL(u_local), MINVAL(f_local), MAXVAL(f_local)
       !PAUSE  'in Linsolve before gmres'
    END IF

    IF(BiCGSTABflag) CALL BiCGSTAB (jmax, nlocal, ne_local, kry_p, HIGH_ORDER, clip, tol_gmres, u_local, f_local, Lulocal, Lulocal_diag, SCL=scl_local)
    IF(GMRESflag)    CALL    gmres (jmax, kry_p, nlocal, ne_local, HIGH_ORDER, clip, u_local, f_local, Lulocal, Lulocal_diag)

    IF(diagnostics_elliptic) THEN
       PRINT *, 'after gmres MIN/MAX(u)=', MINVAL(u_local), MAXVAL(u_local), &
	                        'MIN/MAX(f)=', MINVAL(f_local), MAXVAL(f_local)
       !PAUSE  'in Linsolve after gmres'
    END IF
    
    res(:,:,j_lev) = f_local - RESHAPE( Lulocal (j_lev, u_local, nlocal, ne_local, HIGH_ORDER), (/nlocal, ne_local/) ) 


    
    IF(diagnostics_elliptic .OR. display_Vcycle) THEN
       nlocal_global = nlocal
       tmp1 = SUM(SUM(res(:,:,j_lev)**2,DIM=2))
       tmp2 = MAXVAL(ABS(res(:,:,j_lev)))
       tmp3 = SUM(SUM(res(:,:,j_lev)**2,DIM=2)*dA)
#ifdef MULTIPROC
       CALL parallel_global_sum( INTEGER=nlocal_global )
       CALL parallel_global_sum( REAL=tmp1 )
       CALL parallel_global_sum( REALMAXVAL=tmp2 )
       CALL parallel_global_sum( REAL=tmp3 )
#endif
       IF (par_rank.EQ.0)  THEN
!!$       PRINT *, 'sumdA_global=',sumdA_global
          WRITE(*,'("after exactsolver: ||res||=", 3(2x,E15.8))') SQRT( tmp1/REAL(nlocal_global*ne_local,pr) ), &
               tmp2, SQRT( tmp3/sumdA_global )
       END IF
    END IF

    DO ie = 1, ne_local
       tmp1 = SUM(res(:,ie,j_lev)**2*dA)
#ifdef MULTIPROC
       CALL parallel_global_sum( REAL=tmp1 )
#endif

       error(ie) = SQRT( tmp1/sumdA_global ) !--l2 error
!!$     error(ie) = MAXVAL(ABS(res(:,ie,j_lev))) !L_infty error
    END DO

    k = 1
    repeart_corrections = 0

#ifdef NO_wlog_diagnostics_elliptic                                                                            
#else                                                                                                          
    IF (wlog) WRITE (6,'("Initial error = ", es21.10)') error
    IF (wlog) WRITE (6,'("scl_local = ", es21.10)') scl_local
    IF (wlog) WRITE (6,'("absolute tol = ", es21.10," (stopping criteria)")') tolh
#endif                                                                                                         

    IF(diagnostics_elliptic) THEN
       PRINT *, 'MIN/MAX(u)=',MINVAL(u_local), MAXVAL(u_local), &
	            'MIN/MAX(f)=',MINVAL(f_local), MAXVAL(f_local)
       !PAUSE  'in Linsolve before V-cycle'
    END IF

#ifdef OLEG_CONV
    !OLEG: convergence test
    kmax = 7
#else
    kmax = 20
#endif
    DO WHILE ( ANY(error > tolh) .AND. k <= kmax)  !Oleg:  I do not like fixing # of iterations
#ifdef NO_wlog_diagnostics_elliptic                                                                                         
#else                                                                                                                       
       IF (wlog) WRITE (6,'("----------------------- V-cycle Iteration ", i3, " (",i1,") -----------------------")') k, repeart_corrections 
#endif                                                                                                                      
       !-- Down V-cycle ----------------------------------------------------------------------------------------------------
       IF(diagnostics_elliptic) PRINT *,'Down V-cycle'

       DO j = jmax, jmin+1, -1
          m  = Nwlt_lev(j,1) ! OLEG: modified to be DB neutral
          v(1:m,1:ne_local,j) = 0.0_pr
          
          IF(elliptic_zone_active) THEN
                BC_zone = .TRUE.
!!$                CALL BiCGSTAB (j, m, ne_local, len_bicgstab_coarse, HIGH_ORDER, clip, tol_gmres, v(1:m,1:ne_local,j), res(1:m,1:ne_local,j), Lulocal, Lulocal_diag, SCL=scl_local)
                CALL    GMRES (j, len_bicgstab_coarse, m, ne_local, HIGH_ORDER, clip, v(1:m,1:ne_local,j), res(1:m,1:ne_local,j), Lulocal, Lulocal_diag)

                CALL Jacoby(v(1:m,1:ne_local,j), res(1:m,1:ne_local,j), j, m, ne_local, 1, LOW_ORDER, clip, Lulocal, Lulocal_diag)

                BC_zone = .FALSE.
          ELSE
             CALL Jacoby(v(1:m,1:ne_local,j), res(1:m,1:ne_local,j), j, m, ne_local, len_iter, LOW_ORDER, clip, Lulocal, Lulocal_diag)
          END IF
          
          res(1:m,1:ne_local,j-1) = res(1:m,1:ne_local,j) - RESHAPE (Lulocal (j, v(1:m,1:ne_local,j), m, ne_local, LOW_ORDER), (/m, ne_local/) ) 
          
          !PRINT *, 'res1:', MINVAL(ABS(res(1:m,1:ne_local,j-1))), MAXVAL(ABS(res(1:m,1:ne_local,j-1)))
          
          IF(diagnostics_elliptic .OR. display_Vcycle) THEN
             m_global = m
             tmp1 = SUM(SUM(res(1:m,1:ne_local,j-1)**2,DIM=2))
             tmp2 = MAXVAL (ABS(res(1:m,1:ne_local,j-1)))
             CALL parallel_global_sum( INTEGER=m_global )
             CALL parallel_global_sum( REAL=tmp1 )
             CALL parallel_global_sum( REALMAXVAL=tmp2 )
             IF (par_rank.EQ.0) PRINT *, 'j=',j,' ||res||=', SQRT( tmp1/REAL(m_global*ne_local,pr) ), tmp2
          END IF
          
!!$        IF(diagnostics_elliptic) PRINT *, 'Low pass filtering'
!!$        CALL local_lowpass_filt(res(1:m,1:ne_local,j-1), j, m, ne_local, 1, ne_local )
!!$          IF(diagnostics_elliptic) PRINT *, 'j=',j,' ||res||=',SQRT(SUM(SUM(res(1:m,1:ne_local,j-1)**2,DIM=2))/REAL(m*ne_local,pr)), MAXVAL (ABS(res(1:m,1:ne_local,j-1)))
!!$          IF(diagnostics_elliptic) PRINT *, 'j=',j, &
!!$               'res=',res(1:nlocal,1:ne_local,j-1), 'ne_local/nlocal=', ne_local, nlocal
!!$          PRINT *,'SIZE(res)=',SIZE(res)
          
          IF(interpolate_internal) THEN
             trnsf_type = INTERNAL !INTERNAL = 1
          ELSE
             trnsf_type = NORMAL !NORMAL = 0
          END IF
          IF(MINVAL(clip) == MAXVAL(clip)) THEN ! transfer all variables at once
             CALL wlt_interpolate(res(1:nlocal,1:ne_local,j-1), ne_local,  1, ne_local, j, j-1, nlocal, HIGH_ORDER, MIN(trnsf_type,1-MINVAL(clip)))!original 
          ELSE
             DO ie=1,ne_local
                CALL wlt_interpolate(res(1:nlocal,ie,j-1), 1,  1, 1, j, j-1, nlocal, HIGH_ORDER, MIN(trnsf_type,1-clip(ie))) !original 
             END DO
          END IF


          IF(diagnostics_elliptic) PRINT *, 'after wlt_interpolate() MIN/MAX(res(:,:,j-1))=', &
               MINVAL(res(1:nlocal,1:ne_local,j-1)), MAXVAL(res(1:nlocal,1:ne_local,j-1))
          
       END DO
       
       !-- Coarsest level ----------------------------------------------------------------------------------------------------
       j = jmin
       m  = Nwlt_lev(j,1) ! OLEG: modified to be DB neutral
       v(1:m,1:ne_local,j) = 0.0_pr
       
       IF(diagnostics_elliptic) PRINT *,'Coarsest Level iterative Solver'
!!$     CALL BiCGSTAB (j, m, ne_local, len_bicgstab_coarse, low_order, clip, tol_gmres, v(1:m,1:ne_local,j), res(1:m,1:ne_local,j), Lulocal, Lulocal_diag, SCL=scl_local)
!!$     CALL Jacoby (v(1:m,1:ne_local,j), res(1:m,1:ne_local,j), j, m, ne_local, len_iter, LOW_ORDER, clip, Lulocal, Lulocal_diag)
       
       !PRINT *, j, kry_p_coarse, 'm/ne_local=',m, ne_local, LOW_ORDER, clip, v(1:m,1:ne_local,j), res(1:m,1:ne_local,j)

       IF(BiCGSTABflag) CALL BiCGSTAB (j, m, ne_local, kry_p_coarse, LOW_ORDER, clip, tol_gmres, v(1:m,1:ne_local,j), res(1:m,1:ne_local,j), Lulocal, Lulocal_diag)
       IF(GMRESflag)    CALL    GMRES (j, kry_p_coarse, m, ne_local, LOW_ORDER, clip, v(1:m,1:ne_local,j), res(1:m,1:ne_local,j), Lulocal, Lulocal_diag)
       
       IF(diagnostics_elliptic .OR. display_Vcycle) THEN
          res(1:m,1:ne_local,j) = res(1:m,1:ne_local,j) - RESHAPE (Lulocal (j, v(1:m,1:ne_local,j), m, ne_local, LOW_ORDER), (/m, ne_local/) )
          m_global = m
          tmp1 = SUM(SUM(res(1:m,1:ne_local,j)**2,DIM=2))
          tmp2 = MAXVAL (ABS(res(1:m,1:ne_local,j)))
          CALL parallel_global_sum( INTEGER=m_global )
          CALL parallel_global_sum( REAL=tmp1 )
          CALL parallel_global_sum( REALMAXVAL=tmp2 )
          IF (par_rank.EQ.0) PRINT *, 'j=',j,' ||res||=', SQRT( tmp1/REAL(m_global*ne_local,pr) ), tmp2
       END IF

       !-- Up V-cycle ------------------
       IF(diagnostics_elliptic) THEN
          PRINT *,'Up V-cycle'
          DO j = jmin+1, jmax
             PRINT *, 'j, Nwlt_lev(j,1), nlocal', j, Nwlt_lev(j,1), nlocal
          END DO
!!$          PRINT *, 'interpolate here1:', 'res',v(:,1:ne_local,:)
!!$          DO j = 1, j_lev
!!$             print *, 'before:', j, MINVAL(v(:,1:ne_local,j)), MAXVAL(v(:,1:ne_local,j))
!!$          END DO
!!$          PRINT *, '=================================================='
       END IF
       
       DO j = jmin+1, jmax
          m  = Nwlt_lev(j,1) ! OLEG: modified to be DB neutral
          IF(diagnostics_elliptic) &
               PRINT * ,'before wlt_interp MIN/MAX(v(:,1:ne_local,j-1))=', &
               MINVAL(v(:,1:ne_local,j-1)), MAXVAL(v(:,1:ne_local,j-1))

          CALL wlt_interpolate(v(:,1:ne_local,j-1), ne_local, 1, ne_local, j-1, j, nlocal, HIGH_ORDER, NORMAL)
          
          IF(diagnostics_elliptic) THEN
             PRINT * ,'UPV after interp j = ', j
             PRINT * ,'MIN/MAX(v(:,1:ne_local,j-1))=', MINVAL(v(:,1:ne_local,j-1)), MAXVAL(v(:,1:ne_local,j-1))
             PRINT * ,'--------------------'
          END IF
          
          !PRINT *,'before Jacoby: j=', j, 'jmin=', jmin
          !PRINT *,'before Jacoby: v(1:m,1:ne_local,j-1)=', v(1:m,1:ne_local,j-1)
          
          v(1:m,1:ne_local,j) = v(1:m,1:ne_local,j) + w0 * v(1:m,1:ne_local,j-1)
          
          IF(Jacoby_correction) THEN
             res(1:m,1:ne_local,j-1) = res(1:m,1:ne_local,j) - RESHAPE (Lulocal (j, v(1:m,1:ne_local,j), m, ne_local, LOW_ORDER), (/m, ne_local/) )
             IF(diagnostics_elliptic) PRINT *, 'Up V-cycle'
             CALL correction_factor (res(1:m,1:ne_local,j), res(1:m,1:ne_local,j-1), dA_level(1:m,j), w_min*w0, w0, w_correction, m, ne_local)
             DO ie =1, ne_local
                v(1:m,1:ne_local,j) = v(1:m,1:ne_local,j) + (w_correction(ie)-w0) * v(1:m,1:ne_local,j-1)
             END DO
!!$           mh = Nwlt_lev(j,0)
!!$           !internal points
!!$           IF(diagnostics_elliptic) PRINT *, 'internal points'
!!$           mh = Nwlt_lev(j,0)
!!$           CALL correction_factor (res(1:mh,1:ne_local,j), res(1:mh,1:ne_local,j-1), dA_level(1:mh,j), w_min*w0, w0, w_correction, mh, ne_local)
!!$           DO ie =1, ne_local
!!$              v(1:mh,1:ne_local,j) = v(1:mh,1:ne_local,j) + (w_correction(ie)-w0) * v(1:mh,1:ne_local,j-1)
!!$           END DO
!!$           !boundary points
!!$           IF(diagnostics_elliptic) PRINT *, 'boundary points'
!!$           IF( mh < m) THEN
!!$              CALL correction_factor (res(mh+1:m,1:ne_local,j), res(mh+1:m,1:ne_local,j-1), dA_level(mh+1:m,j), w_min*w0, w0, w_correction, m-mh, ne_local)
!!$              DO ie =1, ne_local
!!$                 v(mh+1:m,1:ne_local,j) = v(mh+1:m,1:ne_local,j) + (w_correction(ie)-w0) * v(mh+1:m,1:ne_local,j-1)
!!$              END DO
!!$           END IF
          END IF


          IF(diagnostics_elliptic) &
               PRINT *,'before Jacoby: v=', MINVAL(v(1:m,1:ne_local,j)), MAXVAL(v(1:m,1:ne_local,j)), &
               'res=', MAXVAL(res(1:m,1:ne_local,j)), MINVAL(res(1:m,1:ne_local,j))
          
          IF(elliptic_zone_active) THEN
                BC_zone = .TRUE.
!!$                CALL BiCGSTAB (j, m, ne_local, len_bicgstab_coarse, HIGH_ORDER, clip, tol_gmres, v(1:m,1:ne_local,j), res(1:m,1:ne_local,j), Lulocal, Lulocal_diag, SCL=scl_local)
                CALL    GMRES (j, len_bicgstab_coarse, m, ne_local, HIGH_ORDER, clip, v(1:m,1:ne_local,j), res(1:m,1:ne_local,j), Lulocal, Lulocal_diag)
                CALL Jacoby(v(1:m,1:ne_local,j), res(1:m,1:ne_local,j), j, m, ne_local, 1, LOW_ORDER, clip, Lulocal, Lulocal_diag)

                BC_zone = .FALSE.
          ELSE
             CALL Jacoby (v(1:m,1:ne_local,j), res(1:m,1:ne_local,j), j, m, ne_local, len_iter, LOW_ORDER, clip, Lulocal, Lulocal_diag)
          END IF
          IF(diagnostics_elliptic) &
               PRINT *,'after Jacoby: v=', MINVAL(v(1:m,1:ne_local,j)), MAXVAL(v(1:m,1:ne_local,j)), &
               'res=', MAXVAL(res(1:m,1:ne_local,j)), MINVAL(res(1:m,1:ne_local,j))
          
          
!!!!!!!!!!!! need to add correction here
          
          res(1:m,1:ne_local,j) = res(1:m,1:ne_local,j) - RESHAPE (Lulocal (j, v(1:m,1:ne_local,j), m, ne_local, LOW_ORDER), (/m, ne_local/) )

          IF(diagnostics_elliptic .OR. display_Vcycle) THEN
             m_global = m
             tmp1 = SUM(SUM(res(1:m,1:ne_local,j)**2,DIM=2))
             tmp2 = MAXVAL (ABS(res(1:m,1:ne_local,j)))
             CALL parallel_global_sum( INTEGER=m_global )
             CALL parallel_global_sum( REAL=tmp1 )
             CALL parallel_global_sum( REALMAXVAL=tmp2 )
             
             IF (par_rank.EQ.0)  THEN
                PRINT *, 'j=', j, ' ||res||=', SQRT( tmp1/REAL(m_global*ne_local,pr) ), &
                     tmp2!, ' m=', m
             END IF
          END IF
          
       END DO

       
       IF(diagnostics_elliptic) THEN
          PRINT *, 'MIN/MAX(u)=',MINVAL(u_local), MAXVAL(u_local), &
		           'MIN/MAX(f)=',MINVAL(f_local), MAXVAL(f_local)
          !PAUSE  'in Linsolve after V-cycle 1'
       END IF

       IF(multigrid_correction) res(:,:,j_lev-1) = f_local - RESHAPE(Lulocal (j_lev, u_local, nlocal, ne_local, HIGH_ORDER), (/m, ne_local/) )
       
       u_local = u_local + w1 * v(:,:,jmax) 

       IF(diagnostics_elliptic) THEN
          PRINT *, 'MIN/MAX(u)=',MINVAL(u_local), MAXVAL(u_local), &
		           'MIN/MAX(f)=',MINVAL(f_local), MAXVAL(f_local)
          !PAUSE  'in Linsolve after V-cycle 2'
       END IF
       
       IF(multigrid_correction) THEN
          res(:,:,j_lev) = f_local - RESHAPE(Lulocal (j_lev, u_local, nlocal, ne_local, HIGH_ORDER), (/m, ne_local/) )

          IF(diagnostics_elliptic) PRINT *, 'update after multi-grid'
          CALL correction_factor (res(1:m,:,j_lev-1), res(1:m,:,j_lev), dA_level(1:m,j_lev), w_min*w1, w_max*w1, w_correction, m, ne_local)


          DO ie =1, ne_local
             u_local(1:m,ie) = u_local(1:m,ie) + (w_correction(ie)-w1) * v(1:m,ie,jmax) 
          END DO
!!$        mh = Nwlt_lev(j_lev,0)
!!$        !internal points
!!$        IF(diagnostics_elliptic) PRINT *, 'internal points'
!!$        CALL correction_factor (res(1:mh,:,j_lev-1), res(1:mh,:,j_lev), dA_level(1:mh,j_lev), w_min*w1, w_max*w1, w_correction, mh, ne_local)
!!$        DO ie =1, ne_local
!!$           u_local(1:mh,ie) = u_local(1:mh,ie) + (w_correction(ie)-w1) * v(1:mh,ie,jmax) 
!!$        END DO
!!$        !boundary points
!!$        IF(diagnostics_elliptic) PRINT *, 'boundary points'
!!$        IF( mh < m) THEN
!!$           CALL correction_factor (res(mh+1:m,:,j_lev-1), res(mh+1:m,:,j_lev), dA_level(mh+1:m,j_lev), w_min*w1, w_max*w1, w_correction, m-mh, ne_local)
!!$           DO ie =1, ne_local
!!$              u_local(mh+1:m,ie) = u_local(mh+1:m,ie) + (w_correction(ie)-w1) * v(mh+1:m,ie,jmax) 
!!$           END DO
!!$        END IF
       END IF

       res(:,:,j_lev) = f_local - RESHAPE(Lulocal (j_lev, u_local, nlocal, ne_local, HIGH_ORDER), (/nlocal, ne_local/) )


!!$       DO ie = 1, ne_local
!!$          PRINT *,res(:,ie,j_lev)
!!$          PRINT *,dA
!!$          PRINT *,sumdA
!!$       END DO
       
       DO ie = 1, ne_local
          tmp1 = SUM(res(:,ie,j_lev)**2*dA)
          CALL parallel_global_sum( REAL=tmp1 )
          error(ie) = SQRT( tmp1/sumdA_global )     !--l2 error
!!$        error(ie) = MAXVAL(ABS(res(:,ie,j_lev))) !L_infty error
       END DO

#ifdef NO_wlog_diagnostics_elliptic                                                                            
#else                                                                                                          
       IF(diagnostics_elliptic .OR. wlog) THEN
          nlocal_global = nlocal
          tmp1 = SUM(SUM(res(:,:,j_lev)**2,DIM=2))
          tmp2 = MAXVAL (ABS(res(:,:,j_lev)))
          tmp3 = SUM(SUM(res(:,:,j_lev)**2,DIM=2)*dA)
          CALL parallel_global_sum( INTEGER=nlocal_global )
          CALL parallel_global_sum( REAL=tmp1 )
          CALL parallel_global_sum( REALMAXVAL=tmp2 )
          CALL parallel_global_sum( REAL=tmp3 )

          IF (par_rank.EQ.0)  THEN
             PRINT *, 'before exact solver: ||res||=', &
                  SQRT( tmp1/REAL(nlocal_global*ne_local,pr) ), tmp2, &
                  SQRT( tmp3/sumdA_global )
          END IF
       END IF
#endif                                                                                                         
       

       IF(diagnostics_elliptic) THEN
          PRINT *, 'MIN/MAX(u)=',MINVAL(u_local), MAXVAL(u_local), &
		           'MIN/MAX(f)=',MINVAL(f_local), MAXVAL(f_local)
          !PAUSE  'in Linsolve before exact solver'
       END IF

       !--------- Exact Solver ---------
       
       IF(diagnostics_elliptic) PRINT *,'Exact iterative Solver, kry_p=', kry_p
       IF(GMRES_correction .AND. kry_p > 0  )  THEN
          v(:,:,j_lev) = u_local         
          res(:,:,j_lev-1 ) = res(:,:,j_lev ) 
       END IF

       IF(diagnostics_elliptic) THEN
          PRINT *, 'MIN/MAX(u)=',MINVAL(u_local), MAXVAL(u_local),  &
	               'MIN/MAX(f)=',MINVAL(f_local), MAXVAL(f_local)
          !PAUSE  'in Linsolve before GMRES correction 0'
       END IF

       IF( kry_p > 0) THEN
          IF(BiCGSTABflag) CALL BiCGSTAB (j_lev, nlocal, ne_local, kry_p, HIGH_ORDER, clip, tol_gmres, u_local, f_local, Lulocal, Lulocal_diag, SCL=scl_local)
          IF(GMRESflag)    CALL    GMRES (j_lev, kry_p, nlocal, ne_local, HIGH_ORDER, clip, u_local, f_local, Lulocal, Lulocal_diag)
       END IF

       IF(diagnostics_elliptic) THEN
          PRINT *, 'MIN/MAX(u)=',MINVAL(u_local), MAXVAL(u_local), &
	               'MIN/MAX(f)=',MINVAL(f_local), MAXVAL(f_local)
          !PAUSE  'in Linsolve before GMRES correction 1'
       END IF


       res(:,:,j_lev) = f_local - RESHAPE(Lulocal (j_lev, u_local, nlocal, ne_local, HIGH_ORDER), (/nlocal, ne_local/) )

       IF(diagnostics_elliptic .OR. display_Vcycle) THEN
          nlocal_global = nlocal
          tmp1 = SUM(SUM(res(:,:,j_lev)**2,DIM=2))
          tmp2 = MAXVAL (ABS(res(:,:,j_lev)))
          tmp3 = SUM(SUM(res(:,:,j_lev)**2,DIM=2)*dA)
          CALL parallel_global_sum( INTEGER=nlocal_global )
          CALL parallel_global_sum( REAL=tmp1 )
          CALL parallel_global_sum( REALMAXVAL=tmp2 )
          CALL parallel_global_sum( REAL=tmp3 )
          
          IF (par_rank.EQ.0)  THEN
             WRITE(*,'("After exact solver: ||res||=",3(1x,E15.8))') &
                  SQRT( tmp1/REAL(nlocal_global*ne_local,pr) ), tmp2, &
                  SQRT( tmp3/sumdA_global )
          END IF
       END IF

       IF(diagnostics_elliptic) THEN
          PRINT *, 'MIN/MAX(u)=',MINVAL(u_local), MAXVAL(u_local), &
	               'MIN/MAX(f)=',MINVAL(f_local), MAXVAL(f_local)
          !PAUSE  'in Linsolve before GMRES correction'
       END IF

       IF(GMRES_correction) THEN
          IF(diagnostics_elliptic) PRINT *, 'GMRES correction'
          CALL correction_factor (res(:,:,j_lev-1), res(:,:,j_lev), dA_level(:,j_lev),w_min, w_max, w_correction, nlocal, ne_local)
          IF(diagnostics_elliptic) PRINT *, 'w_correction=',w_correction
          DO ie =1, ne_local
             u_local(:,ie) = v(:,ie,j_lev) + w_correction(ie) * (u_local(:,ie)-v(:,ie,j_lev)) 
          END DO
!!$        mh = Nwlt_lev(j_lev,0)
!!$        m=nlocal
!!$        !internal points
!!$        IF(diagnostics_elliptic) PRINT *, 'internal points'
!!$        CALL correction_factor (res(1:mh,:,j_lev-1), res(1:mh,:,j_lev), dA_level(1:mh,j_lev), w_min, w_max, w_correction, nlocal, ne_local)
!!$        DO ie =1, ne_local
!!$           u_local(1:mh,ie) = v(1:mh,ie,j_lev) + w_correction(ie) * (u_local(1:mh,ie)-v(:,ie,j_lev)) 
!!$        END DO
!!$        !boundary points
!!$        IF(diagnostics_elliptic) PRINT *, 'boundary points'
!!$        IF( mh < m) THEN
!!$           CALL correction_factor (res(mh+1:m,:,j_lev-1), res(mh+1:m,:,j_lev), dA_level(mh+1:m,j_lev), w1, w1, w_correction, nlocal-mh, ne_local)
!!$           DO ie =1, ne_local
!!$              u_local(mh+1:m,ie) = v(mh+1:m,ie,j_lev) + w_correction(ie) * (u_local(mh+1:m,ie)-v(mh+1:m,ie,j_lev)) 
!!$           END DO
!!$        END IF
          res(:,:,j_lev) = f_local - RESHAPE(Lulocal (j_lev, u_local, nlocal, ne_local, HIGH_ORDER), (/nlocal, ne_local/) )

          IF(diagnostics_elliptic .OR. display_Vcycle) THEN
             tmp1 = SUM(SUM(res(:,:,j_lev)**2,DIM=2))
             tmp2 = MAXVAL (ABS(res(:,:,j_lev)))
             tmp3 = SUM(SUM(res(:,:,j_lev)**2,DIM=2)*dA)
             CALL parallel_global_sum( REAL=tmp1 )
             CALL parallel_global_sum( REALMAXVAL=tmp2 )
             CALL parallel_global_sum( REAL=tmp3 )
             IF (par_rank.EQ.0)  THEN
                WRITE(*,'("After GMRES correction: ||res||=",3(1x,E15.8))') &
                     SQRT( tmp1/REAL(nlocal_global*ne_local,pr) ), tmp2, &
                     SQRT( tmp3/sumdA_global )
             END IF
          END IF
          
          IF (ALL(w_correction==w_min) ) THEN
             repeart_corrections = repeart_corrections + 1
          ELSE
             repeart_corrections = 0
          END IF
#ifdef MULTIPROC
          CALL parallel_global_sum( INTEGERMAXVAL=repeart_corrections )
#endif

       END IF

       IF(diagnostics_elliptic) THEN
          PRINT *, 'MIN/MAX(u)=',MINVAL(u_local), MAXVAL(u_local), &
	               'MIN/MAX(f)=',MINVAL(f_local), MAXVAL(f_local)
          !PAUSE  'in Linsolve after GMRES correction'
       END IF

       !OLEG: I put this change to stabilize solutoin when f = 0 and  guess is 0
       DO ie = 1, ne_local  !L2-norm
          tmp1 = SUM(u_local(:,ie)**2*dA)
          CALL parallel_global_sum( REAL=tmp1 )
          scl_u(ie) =  SQRT( tmp1 )
       END DO
       scl_local = MAX(scl_u,scl_f)
       IF( PRESENT(SCL) ) scl_local = MAX(scl_local, SCL)
       WHERE( scl_local < floor_rhs ) scl_local = 1.0_pr ! if scl_local is small, it becomes absolute error
       tolh = tol * scl_local
       
       IF(diagnostics_elliptic) THEN
          PRINT *, 'scl_u=',scl_u
          PRINT *, 'scl_f=',scl_f
          IF( PRESENT(SCL) ) PRINT *, 'SCL=',SCL
          PRINT *, 'scl_local=',scl_local
       END IF
       
       DO ie = 1, ne_local
          tmp1 = SUM(res(:,ie,j_lev)**2*dA)
          CALL parallel_global_sum( REAL=tmp1 )
          error(ie) = SQRT( tmp1/sumdA_global )     !--l2 error
!!$        error(ie) = MAXVAL(ABS(res(:,ie,j_lev))) !L_infty error
       END DO

#ifdef NO_wlog_diagnostics_elliptic                                                                                         
#else                                                                                                                       
       IF (wlog) WRITE (6,'("      After V-cycle", i4, ": relative L2 residue = ",20(es21.11,1x))') k,error/scl_local !was es11.4 format DG
       IF (wlog) WRITE (6,'("      After V-cycle", i4, ": absolute L2 residue = ",20(es21.11,1x))') k, error
#endif                                                                                                                      

       k = k+1
       IF( repeart_corrections > 4) THEN
          IF(par_rank.EQ.0) WRITE(*,'("WARNING: the iterations stopped converging, and started diverging")')
          k =k + 100
       END IF

       IF(k >kmax .AND. set_to_zero_if_diverged) THEN
          WRITE(*,'("WARNING: the solution of eliptic problem did not converge and set to zero")')
          DO ie =1, ne_local
             u_local(:,ie) = 0.0_pr
          END DO
       END IF

!       PAUSE 'in Linsolve'
    END DO

    !Now, once solution is converge, shift back to clipped value
    DO ie = 1, ne_local 
       IF(clip(ie) == 1) THEN
          u_clip = u_local(iclip(j_lev),ie) 
          CALL parallel_broadcast( REAL=u_clip )
          u_local(1:nlocal,ie)= u_local(1:nlocal,ie)-u_clip
       END IF
    END DO



#ifdef NO_wlog_diagnostics_elliptic                                                                                         
#else                                                                                                                       
    IF (wlog) WRITE (6,'(" ")')
#endif                                                                                                                      

  END SUBROUTINE Linsolve
  
  SUBROUTINE WLinsolve (u_local, f_local, tol, nlocal, ne_local, clip, Lulocal, Lulocal_diag, Lulocal_Wdiag, SCL)
    !--Solves equation Lu(u) = f for u.
    USE precision
    USE wlt_vars
    USE elliptic_vars
    USE util_vars       ! dA
    USE pde
    USE parallel
    IMPLICIT NONE

    INTEGER,                          INTENT (IN)    :: nlocal, ne_local
    INTEGER, DIMENSION(ne_local),           INTENT (IN)    :: clip
    REAL (pr),                        INTENT (IN)    :: tol
    REAL (pr), DIMENSION (nlocal, ne_local), INTENT (INOUT) :: u_local
    REAL (pr), DIMENSION (nlocal, ne_local), INTENT (INOUT) :: f_local
    REAL (pr), DIMENSION (ne_local), INTENT (IN), OPTIONAL :: SCL


    INTEGER, DIMENSION(ne_local)                           :: noclip
    INTEGER, PARAMETER                     :: j_up = 0, j_down = 1
    INTEGER             :: j, jmin, jmax, k, m, ie, repeart_corrections, ii, trnsf_type
    INTEGER,   DIMENSION (j_lev)           :: iclip
    REAL (pr), DIMENSION (nlocal, ne_local,j_lev) :: res, v
    REAL (pr), DIMENSION (ne_local)              :: error, scl_u, scl_f, scl_local, tolh
    REAL (pr)                              :: floor_rhs
    REAL (pr), DIMENSION (ne_local) :: w_correction
    INTERFACE
       FUNCTION Lulocal (jlev, u_local, nlocal, ne_local, meth)
         USE precision
         USE sizes
         USE pde
         IMPLICIT NONE
         INTEGER, INTENT (IN) :: jlev, nlocal, ne_local, meth
         REAL (pr), DIMENSION (nlocal*ne_local), INTENT (INOUT) :: u_local
         REAL (pr), DIMENSION (nlocal*ne_local) :: Lulocal
       END FUNCTION Lulocal
       FUNCTION Lulocal_diag (jlev,  nlocal, ne_local, meth)
         USE precision
         USE sizes
         USE pde
         IMPLICIT NONE
         INTEGER, INTENT (IN) :: jlev, nlocal, ne_local, meth
         REAL (pr), DIMENSION (nlocal*ne_local) :: Lulocal_diag
       END FUNCTION Lulocal_diag
       FUNCTION Lulocal_Wdiag (jlev,  nlocal, ne_local, meth)
         USE precision
         USE sizes
         USE pde
         IMPLICIT NONE
         INTEGER, INTENT (IN) :: jlev, nlocal, ne_local, meth
         REAL (pr), DIMENSION (nlocal*ne_local) :: Lulocal_Wdiag
       END FUNCTION Lulocal_Wdiag
    END INTERFACE
    
    INTEGER ::nlocal_global, m_global
    REAL(pr) :: tmp1, tmp2, tmp3
    
    IF(diagnostics_elliptic) THEN
       PRINT *,' '
       PRINT *,'------------------------'
       PRINT *,'Linsolve: solve Lu(u)=f '
    END IF

!!$   IF(elliptic_zone_active) THEN
!!$      CALL insert_elliptic_zone(u_local, u_local, j_lev, nlocal, ne_local, -1)
!!$   END IF

    
    v = 0.0_pr
    res = 0.0_pr

    noclip = 0
    floor_rhs = 1.e-12_pr
    
    !--Clipping
    DO j = 1, j_lev
       iclip(j) = i_clip(j)
    END DO

    !  
    !--Solve problem Lu(u)=f
    DO ie = 1, ne_local  !L2-norm
       tmp1 = SUM(f_local(:,ie)**2*dA)
       tmp2 = SUM(u_local(:,ie)**2*dA)
       CALL parallel_global_sum( REAL=tmp1 )
       CALL parallel_global_sum( REAL=tmp2 )
       scl_f(ie) =  SQRT( tmp1 )
       scl_u(ie) =  SQRT( tmp2 )
    END DO
    scl_local = MAX(scl_u,scl_f)

    IF( PRESENT(SCL) ) scl_local = MAX(scl_local, SCL)
    WHERE( scl_local < floor_rhs ) scl_local = 1.0_pr ! if scl_local is small, it becomes absolute error
    
    tolh = tol * scl_local
    
    !---------- Standard multigrid strategy ------------------------
#ifdef MULTIPROC                                   !05.18.2011!O+AR!
    jmin = j_tree_root
    IF ( j_tree_root .GT. j_lev ) THEN
       IF (par_rank.EQ.0)  THEN
          WRITE (*,'(A)')                ' ERROR: In WLinsolve() [elliptic_solve_3d.f90]:  j_tree_root  must be <  j_lev '
          WRITE (*,'(A, A, I3, A, I3)')  ' j_tree_root is high, set it at the most equal to j_lev', '     j_tree_root=',j_tree_root, '   j_lev=',j_lev
          PRINT *,                       ' Exiting ...'
       END IF
       CALL parallel_finalize; STOP                   
    END IF
#else
    jmin = 1  
#endif
    jmax = j_lev

    
    IF ( ANY( Nwlt_lev(jmin:jmax,1) .LE. 0 ))   THEN
       DO j = jmin, jmax
         IF ( Nwlt_lev(j,1) .LE. 0 ) WRITE (*,'(A, I6, A, I3, A, I9)')  'rank=',par_rank, '   Nwlt_lev(j=',j,',1)=',Nwlt_lev(j,1)
       END DO
       IF (par_rank.EQ.0)  THEN
          WRITE (*,'(A)')                ' ERROR: In WLinsolve() [elliptic_solve_3d.f90]:  nlocal=Nwlt_lev(j,1)=0  must be >  0 '
          WRITE (*,'(A)')                ' j_tree_root is high, reduce it (not less than j_min)'
          PRINT *,                       ' Exiting ...'
       END IF
       CALL parallel_finalize; STOP                   
    END IF
    

    
    IF(diagnostics_elliptic) THEN
       PRINT *,'Initial iterative solver'
       PRINT *, jmax, kry_p, nlocal, ne_local, HIGH_ORDER, clip
       PRINT *, MINVAL(u_local), MAXVAL(u_local), MINVAL(f_local), MAXVAL(f_local)
       !PAUSE  'in Linsolve before gmres'
    END IF

    CALL Wgmres (jmax, kry_p, nlocal, ne_local, HIGH_ORDER, clip, u_local, f_local, Lulocal, Lulocal_diag, Lulocal_Wdiag)

    IF(diagnostics_elliptic) THEN
       PRINT *, 'after gmres MIN/MAX(u)=', MINVAL(u_local), MAXVAL(u_local), &
	                        'MIN/MAX(f)=', MINVAL(f_local), MAXVAL(f_local)
       !PAUSE  'in Linsolve after gmres'
    END IF
    
    res(:,:,j_lev) = f_local - RESHAPE( Lulocal (j_lev, u_local, nlocal, ne_local, HIGH_ORDER), (/nlocal, ne_local/) ) 


    
    IF(diagnostics_elliptic .OR. display_Vcycle) THEN
       nlocal_global = nlocal
       tmp1 = SUM(SUM(res(:,:,j_lev)**2,DIM=2))
       tmp2 = MAXVAL(ABS(res(:,:,j_lev)))
       tmp3 = SUM(SUM(res(:,:,j_lev)**2,DIM=2)*dA)
#ifdef MULTIPROC
       CALL parallel_global_sum( INTEGER=nlocal_global )
       CALL parallel_global_sum( REAL=tmp1 )
       CALL parallel_global_sum( REALMAXVAL=tmp2 )
       CALL parallel_global_sum( REAL=tmp3 )
#endif
       IF (par_rank.EQ.0)  THEN
          PRINT *, 'after exactsolver: ||res||=', SQRT( tmp1/REAL(nlocal_global*ne_local,pr) ), &
               tmp2, SQRT( tmp3/sumdA_global )
       END IF
    END IF

    DO ie = 1, ne_local
       tmp1 = SUM(res(:,ie,j_lev)**2*dA)
#ifdef MULTIPROC
       CALL parallel_global_sum( REAL=tmp1 )
#endif

       error(ie) = SQRT( tmp1/sumdA_global ) !--l2 error
!!$     error(ie) = MAXVAL(ABS(res(:,ie,j_lev))) !L_infty error
    END DO

    k = 1
    repeart_corrections = 0

#ifdef NO_wlog_diagnostics_elliptic                                                                                         
#else                                                                                                                       
    IF (wlog) WRITE (6,'("Initial error = ", es21.10)') error
    IF (wlog) WRITE (6,'("scl_local = ", es21.10)') scl_local
    IF (wlog) WRITE (6,'("absolute tol = ", es21.10," (stopping criteria)")') tolh
#endif                                                                                                                      

    IF(diagnostics_elliptic) THEN
       PRINT *, 'MIN/MAX(u)=',MINVAL(u_local), MAXVAL(u_local), &
	            'MIN/MAX(f)=',MINVAL(f_local), MAXVAL(f_local)
       !PAUSE  'in Linsolve before V-cycle'
    END IF

#ifdef OLEG_CONV 
    !OLEG: convergence test
    DO WHILE ( ANY(error > tolh) .AND. k <= 7)  !Oleg:  I do not like fixing # of iterations
#else
    DO WHILE ( ANY(error > tolh) .AND. k <= 7)  !Oleg:  I do not like fixing # of iterations
#endif
#ifdef NO_wlog_diagnostics_elliptic                                                                                         
#else                                                                                                                       
       IF (wlog) WRITE (6,'("----------------------- V-cycle Iteration ", i3, " (",i1,") -----------------------")') k, repeart_corrections 
#endif                                                                                                                      
       !-- Down V-cycle ----------------------------------------------------------------------------------------------------
       IF(diagnostics_elliptic) PRINT *,'Down V-cycle'

       DO j = jmax, jmin+1, -1
          m  = Nwlt_lev(j,1) ! OLEG: modified to be DB neutral
          v(1:m,1:ne_local,j) = 0.0_pr
          
          IF(elliptic_zone_active) THEN
             DO ii=1, len_iter
                BC_zone = .TRUE.
!!$                CALL BiCGSTAB (j, m, ne_local, len_bicgstab_coarse, LOW_ORDER, clip, tol_gmres, v(1:m,1:ne_local,j), res(1:m,1:ne_local,j), Lulocal, Lulocal_diag, SCL=scl_local)
                CALL Wgmres (j, len_bicgstab_coarse, m, ne_local, LOW_ORDER, clip, v(1:m,1:ne_local,j), res(1:m,1:ne_local,j), Lulocal, Lulocal_diag, Lulocal_Wdiag)
                CALL Jacoby(v(1:m,1:ne_local,j), res(1:m,1:ne_local,j), j, m, ne_local, 1, LOW_ORDER, clip, Lulocal, Lulocal_diag)
                BC_zone = .FALSE.
             END DO
          ELSE
             CALL Jacoby(v(1:m,1:ne_local,j), res(1:m,1:ne_local,j), j, m, ne_local, len_iter, LOW_ORDER, clip, Lulocal, Lulocal_diag)
          END IF
          
          res(1:m,1:ne_local,j-1) = res(1:m,1:ne_local,j) - RESHAPE (Lulocal (j, v(1:m,1:ne_local,j), m, ne_local, LOW_ORDER), (/m, ne_local/) ) 
          
          !PRINT *, 'res1:', MINVAL(ABS(res(1:m,1:ne_local,j-1))), MAXVAL(ABS(res(1:m,1:ne_local,j-1)))
          
          IF(diagnostics_elliptic .OR. display_Vcycle) THEN
             m_global = m
             tmp1 = SUM(SUM(res(1:m,1:ne_local,j-1)**2,DIM=2))
             tmp2 = MAXVAL (ABS(res(1:m,1:ne_local,j-1)))
             CALL parallel_global_sum( INTEGER=m_global )
             CALL parallel_global_sum( REAL=tmp1 )
             CALL parallel_global_sum( REALMAXVAL=tmp2 )
   
             IF (par_rank.EQ.0) PRINT *, 'j=',j,' ||res||=', SQRT( tmp1/REAL(m_global*ne_local,pr) ), tmp2
          END IF
          
!!$        IF(diagnostics_elliptic) PRINT *, 'Low pass filtering'
!!$        CALL local_lowpass_filt(res(1:m,1:ne_local,j-1), j, m, ne_local, 1, ne_local )
!!$          IF(diagnostics_elliptic) PRINT *, 'j=',j,' ||res||=',SQRT(SUM(SUM(res(1:m,1:ne_local,j-1)**2,DIM=2))/REAL(m*ne_local,pr)), MAXVAL (ABS(res(1:m,1:ne_local,j-1)))
!!$          IF(diagnostics_elliptic) PRINT *, 'j=',j, &
!!$               'res=',res(1:nlocal,1:ne_local,j-1), 'ne_local/nlocal=', ne_local, nlocal
!!$          PRINT *,'SIZE(res)=',SIZE(res)
          
          IF(interpolate_internal) THEN
             trnsf_type = INTERNAL !INTERNAL = 1
          ELSE
             trnsf_type = NORMAL !NORMAL = 0
          END IF
          IF(MINVAL(clip) == MAXVAL(clip)) THEN ! transfer all variables at once
             CALL wlt_interpolate(res(1:nlocal,1:ne_local,j-1), ne_local,  1, ne_local, j, j-1, nlocal, LOW_ORDER, MIN(trnsf_type,1-MINVAL(clip)))!original 
          ELSE
             DO ie=1,ne_local
                CALL wlt_interpolate(res(1:nlocal,ie,j-1), 1,  1, 1, j, j-1, nlocal, LOW_ORDER, MIN(trnsf_type,1-clip(ie))) !original 
             END DO
          END IF

          IF(diagnostics_elliptic) PRINT *, 'after wlt_interpolate() MIN/MAX(res(:,:,j-1))=', &
               MINVAL(res(1:nlocal,1:ne_local,j-1)), MAXVAL(res(1:nlocal,1:ne_local,j-1))
          
       END DO
       
       !-- Coarsest level ----------------------------------------------------------------------------------------------------
       j = jmin
       m  = Nwlt_lev(j,1) ! OLEG: modified to be DB neutral
       v(1:m,1:ne_local,j) = 0.0_pr
       
       IF(diagnostics_elliptic) PRINT *,'Coarsest Level iterative Solver'
!!$     CALL BiCGSTAB (j, m, ne_local, len_bicgstab_coarse, low_order, clip, tol_gmres, v(1:m,1:ne_local,j), res(1:m,1:ne_local,j), Lulocal, Lulocal_diag, SCL=scl_local)
!!$     CALL Jacoby (v(1:m,1:ne_local,j), res(1:m,1:ne_local,j), j, m, ne_local, len_iter, LOW_ORDER, clip, Lulocal, Lulocal_diag)
       
       !PRINT *, j, kry_p_coarse, 'm/ne_local=',m, ne_local, LOW_ORDER, clip, v(1:m,1:ne_local,j), res(1:m,1:ne_local,j)

       IF(BiCGSTABflag) CALL BiCGSTAB (j, m, ne_local, kry_p_coarse, LOW_ORDER, clip, tol_gmres, v(1:m,1:ne_local,j), res(1:m,1:ne_local,j), Lulocal, Lulocal_diag)
       IF(GMRESflag)    CALL    gmres (j, kry_p_coarse, m, ne_local, LOW_ORDER, clip, v(1:m,1:ne_local,j), res(1:m,1:ne_local,j), Lulocal, Lulocal_diag)
        
       IF(diagnostics_elliptic .OR. display_Vcycle) THEN
          res(1:m,1:ne_local,j) = res(1:m,1:ne_local,j) - RESHAPE (Lulocal (j, v(1:m,1:ne_local,j), m, ne_local, LOW_ORDER), (/m, ne_local/) )
          m_global = m
          tmp1 = SUM(SUM(res(1:m,1:ne_local,j)**2,DIM=2))
          tmp2 = MAXVAL (ABS(res(1:m,1:ne_local,j)))
          CALL parallel_global_sum( INTEGER=m_global )
          CALL parallel_global_sum( REAL=tmp1 )
          CALL parallel_global_sum( REALMAXVAL=tmp2 )
          
          IF (par_rank.EQ.0)  PRINT *, 'j=',j,' ||res||=', SQRT( tmp1/REAL(m_global*ne_local,pr) ), tmp2
       END IF

       !-- Up V-cycle ------------------
       IF(diagnostics_elliptic) THEN
          PRINT *,'Up V-cycle'
          DO j = jmin+1, jmax
             PRINT *, 'j, Nwlt_lev(j,1), nlocal', j, Nwlt_lev(j,1), nlocal
          END DO
!!$          PRINT *, 'interpolate here1:', 'res',v(:,1:ne_local,:)
!!$          DO j = 1, j_lev
!!$             print *, 'before:', j, MINVAL(v(:,1:ne_local,j)), MAXVAL(v(:,1:ne_local,j))
!!$          END DO
!!$          PRINT *, '=================================================='
       END IF
       
       DO j = jmin+1, jmax
          m  = Nwlt_lev(j,1) ! OLEG: modified to be DB neutral
          IF(diagnostics_elliptic) &
               PRINT * ,'before wlt_interp MIN/MAX(v(:,1:ne_local,j-1))=', &
               MINVAL(v(:,1:ne_local,j-1)), MAXVAL(v(:,1:ne_local,j-1))

          CALL wlt_interpolate(v(:,1:ne_local,j-1), ne_local, 1, ne_local, j-1, j, nlocal, LOW_ORDER, NORMAL)
          
          IF(diagnostics_elliptic) THEN
             PRINT * ,'UPV after interp j = ', j
             PRINT * ,'MIN/MAX(v(:,1:ne_local,j-1))=', MINVAL(v(:,1:ne_local,j-1)), MAXVAL(v(:,1:ne_local,j-1))
             PRINT * ,'--------------------'
          END IF
          
          !PRINT *,'before Jacoby: j=', j, 'jmin=', jmin
          !PRINT *,'before Jacoby: v(1:m,1:ne_local,j-1)=', v(1:m,1:ne_local,j-1)
          
          v(1:m,1:ne_local,j) = v(1:m,1:ne_local,j) + w0 * v(1:m,1:ne_local,j-1)
          
          IF(Jacoby_correction) THEN
             res(1:m,1:ne_local,j-1) = res(1:m,1:ne_local,j) - RESHAPE (Lulocal (j, v(1:m,1:ne_local,j), m, ne_local, LOW_ORDER), (/m, ne_local/) )
             IF(diagnostics_elliptic) PRINT *, 'Up V-cycle'
             CALL correction_factor (res(1:m,1:ne_local,j), res(1:m,1:ne_local,j-1), dA_level(1:m,j), w_min*w0, w0, w_correction, m, ne_local)
             DO ie =1, ne_local
                v(1:m,1:ne_local,j) = v(1:m,1:ne_local,j) + (w_correction(ie)-w0) * v(1:m,1:ne_local,j-1)
             END DO
!!$           mh = Nwlt_lev(j,0)
!!$           !internal points
!!$           IF(diagnostics_elliptic) PRINT *, 'internal points'
!!$           mh = Nwlt_lev(j,0)
!!$           CALL correction_factor (res(1:mh,1:ne_local,j), res(1:mh,1:ne_local,j-1), dA_level(1:mh,j), w_min*w0, w0, w_correction, mh, ne_local)
!!$           DO ie =1, ne_local
!!$              v(1:mh,1:ne_local,j) = v(1:mh,1:ne_local,j) + (w_correction(ie)-w0) * v(1:mh,1:ne_local,j-1)
!!$           END DO
!!$           !boundary points
!!$           IF(diagnostics_elliptic) PRINT *, 'boundary points'
!!$           IF( mh < m) THEN
!!$              CALL correction_factor (res(mh+1:m,1:ne_local,j), res(mh+1:m,1:ne_local,j-1), dA_level(mh+1:m,j), w_min*w0, w0, w_correction, m-mh, ne_local)
!!$              DO ie =1, ne_local
!!$                 v(mh+1:m,1:ne_local,j) = v(mh+1:m,1:ne_local,j) + (w_correction(ie)-w0) * v(mh+1:m,1:ne_local,j-1)
!!$              END DO
!!$           END IF
          END IF


          IF(diagnostics_elliptic) &
               PRINT *,'before Jacoby: v=', MINVAL(v(1:m,1:ne_local,j)), MAXVAL(v(1:m,1:ne_local,j)), &
               'res=', MAXVAL(res(1:m,1:ne_local,j)), MINVAL(res(1:m,1:ne_local,j))
          
          IF(elliptic_zone_active) THEN
             DO ii=1, len_iter
                BC_zone = .TRUE.
!!$                CALL BiCGSTAB (j, m, ne_local, len_bicgstab_coarse, LOW_ORDER, clip, tol_gmres,v(1:m,1:ne_local,j), res(1:m,1:ne_local,j), Lulocal, Lulocal_diag,SCL=scl_local)
                CALL Wgmres (j, len_bicgstab_coarse, m, ne_local, LOW_ORDER, clip, v(1:m,1:ne_local,j), res(1:m,1:ne_local,j), Lulocal, Lulocal_diag, Lulocal_Wdiag)
                CALL Jacoby(v(1:m,1:ne_local,j), res(1:m,1:ne_local,j), j, m, ne_local, 1, LOW_ORDER, clip, Lulocal, Lulocal_diag)
                BC_zone = .FALSE.
             END DO
          ELSE
             CALL Jacoby (v(1:m,1:ne_local,j), res(1:m,1:ne_local,j), j, m, ne_local, len_iter, LOW_ORDER, clip, Lulocal, Lulocal_diag)
          END IF
          IF(diagnostics_elliptic) &
               PRINT *,'after Jacoby: v=', MINVAL(v(1:m,1:ne_local,j)), MAXVAL(v(1:m,1:ne_local,j)), &
               'res=', MAXVAL(res(1:m,1:ne_local,j)), MINVAL(res(1:m,1:ne_local,j))
          
          
!!!!!!!!!!!! need to add correction here
          
          res(1:m,1:ne_local,j) = res(1:m,1:ne_local,j) - RESHAPE (Lulocal (j, v(1:m,1:ne_local,j), m, ne_local, LOW_ORDER), (/m, ne_local/) )

          IF(diagnostics_elliptic .OR. display_Vcycle) THEN
             m_global = m
             tmp1 = SUM(SUM(res(1:m,1:ne_local,j)**2,DIM=2))
             tmp2 = MAXVAL (ABS(res(1:m,1:ne_local,j)))
             CALL parallel_global_sum( INTEGER=m_global )
             CALL parallel_global_sum( REAL=tmp1 )
             CALL parallel_global_sum( REALMAXVAL=tmp2 )
             
             IF (par_rank.EQ.0)  THEN
                PRINT *, 'j=', j, ' ||res||=', SQRT( tmp1/REAL(m_global*ne_local,pr) ), &
                     tmp2!, ' m=', m
             END IF
          END IF
          
       END DO

       
       IF(diagnostics_elliptic) THEN
          PRINT *, 'MIN/MAX(u)=',MINVAL(u_local), MAXVAL(u_local), &
		           'MIN/MAX(f)=',MINVAL(f_local), MAXVAL(f_local)
          !PAUSE  'in Linsolve after V-cycle 1'
       END IF

       IF(multigrid_correction) res(:,:,j_lev-1) = f_local - RESHAPE(Lulocal (j_lev, u_local, nlocal, ne_local, HIGH_ORDER), (/m, ne_local/) )
       
       u_local = u_local + w1 * v(:,:,jmax) 

       IF(diagnostics_elliptic) THEN
          PRINT *, 'MIN/MAX(u)=',MINVAL(u_local), MAXVAL(u_local), &
		           'MIN/MAX(f)=',MINVAL(f_local), MAXVAL(f_local)
          !PAUSE  'in Linsolve after V-cycle 2'
       END IF
       
       IF(multigrid_correction) THEN
          res(:,:,j_lev) = f_local - RESHAPE(Lulocal (j_lev, u_local, nlocal, ne_local, HIGH_ORDER), (/m, ne_local/) )

          IF(diagnostics_elliptic) PRINT *, 'update after multi-grid'
          CALL correction_factor (res(1:m,:,j_lev-1), res(1:m,:,j_lev), dA_level(1:m,j_lev), w_min*w1, w_max*w1, w_correction, m, ne_local)


          DO ie =1, ne_local
             u_local(1:m,ie) = u_local(1:m,ie) + (w_correction(ie)-w1) * v(1:m,ie,jmax) 
          END DO
!!$        mh = Nwlt_lev(j_lev,0)
!!$        !internal points
!!$        IF(diagnostics_elliptic) PRINT *, 'internal points'
!!$        CALL correction_factor (res(1:mh,:,j_lev-1), res(1:mh,:,j_lev), dA_level(1:mh,j_lev), w_min*w1, w_max*w1, w_correction, mh, ne_local)
!!$        DO ie =1, ne_local
!!$           u_local(1:mh,ie) = u_local(1:mh,ie) + (w_correction(ie)-w1) * v(1:mh,ie,jmax) 
!!$        END DO
!!$        !boundary points
!!$        IF(diagnostics_elliptic) PRINT *, 'boundary points'
!!$        IF( mh < m) THEN
!!$           CALL correction_factor (res(mh+1:m,:,j_lev-1), res(mh+1:m,:,j_lev), dA_level(mh+1:m,j_lev), w_min*w1, w_max*w1, w_correction, m-mh, ne_local)
!!$           DO ie =1, ne_local
!!$              u_local(mh+1:m,ie) = u_local(mh+1:m,ie) + (w_correction(ie)-w1) * v(mh+1:m,ie,jmax) 
!!$           END DO
!!$        END IF
       END IF

       res(:,:,j_lev) = f_local - RESHAPE(Lulocal (j_lev, u_local, nlocal, ne_local, HIGH_ORDER), (/nlocal, ne_local/) )


!!$       DO ie = 1, ne_local
!!$          PRINT *,res(:,ie,j_lev)
!!$          PRINT *,dA
!!$          PRINT *,sumdA
!!$       END DO
       
       DO ie = 1, ne_local
          tmp1 = SUM(res(:,ie,j_lev)**2*dA)
          CALL parallel_global_sum( REAL=tmp1 )
          error(ie) = SQRT( tmp1/sumdA_global )     !--l2 error
!!$        error(ie) = MAXVAL(ABS(res(:,ie,j_lev))) !L_infty error
       END DO

#ifdef NO_wlog_diagnostics_elliptic                                                                            
#else                                                                                                          
       IF(diagnostics_elliptic .OR. wlog) THEN
          nlocal_global = nlocal
          tmp1 = SUM(SUM(res(:,:,j_lev)**2,DIM=2))
          tmp2 = MAXVAL (ABS(res(:,:,j_lev)))
          tmp3 = SUM(SUM(res(:,:,j_lev)**2,DIM=2)*dA)
          CALL parallel_global_sum( INTEGER=nlocal_global )
          CALL parallel_global_sum( REAL=tmp1 )
          CALL parallel_global_sum( REALMAXVAL=tmp2 )
          CALL parallel_global_sum( REAL=tmp3 )

          IF (par_rank.EQ.0)  THEN
             PRINT *, 'before exact solver: ||res||=', &
                  SQRT( tmp1/REAL(nlocal_global*ne_local,pr) ), tmp2, &
                  SQRT( tmp3/sumdA_global )
          END IF
       END IF
#endif                                                                                                         
       

       IF(diagnostics_elliptic) THEN
          PRINT *, 'MIN/MAX(u)=',MINVAL(u_local), MAXVAL(u_local), &
		           'MIN/MAX(f)=',MINVAL(f_local), MAXVAL(f_local)
          !PAUSE  'in Linsolve before exact solver'
       END IF

       !--------- Exact Solver ---------
       
       IF(diagnostics_elliptic) PRINT *,'Exact iterative Solver, kry_p=', kry_p
       IF(GMRES_correction .AND. kry_p > 0  )  THEN
          v(:,:,j_lev) = u_local         
          res(:,:,j_lev-1 ) = res(:,:,j_lev ) 
       END IF

       IF(diagnostics_elliptic) THEN
          PRINT *, 'MIN/MAX(u)=',MINVAL(u_local), MAXVAL(u_local),  &
	               'MIN/MAX(f)=',MINVAL(f_local), MAXVAL(f_local)
          !PAUSE  'in Linsolve before GMRES correction 0'
       END IF

       IF( kry_p > 0) THEN
          CALL Wgmres (j_lev, kry_p, nlocal, ne_local, HIGH_ORDER, clip, u_local, f_local, Lulocal, Lulocal_diag, Lulocal_Wdiag)
       END IF

       IF(diagnostics_elliptic) THEN
          PRINT *, 'MIN/MAX(u)=',MINVAL(u_local), MAXVAL(u_local), &
	               'MIN/MAX(f)=',MINVAL(f_local), MAXVAL(f_local)
          !PAUSE  'in Linsolve before GMRES correction 1'
       END IF


       res(:,:,j_lev) = f_local - RESHAPE(Lulocal (j_lev, u_local, nlocal, ne_local, HIGH_ORDER), (/nlocal, ne_local/) )

       IF(diagnostics_elliptic .OR. wlog) THEN
          nlocal_global = nlocal
          tmp1 = SUM(SUM(res(:,:,j_lev)**2,DIM=2))
          tmp2 = MAXVAL (ABS(res(:,:,j_lev)))
          tmp3 = SUM(SUM(res(:,:,j_lev)**2,DIM=2)*dA)
          CALL parallel_global_sum( INTEGER=nlocal_global )
          CALL parallel_global_sum( REAL=tmp1 )
          CALL parallel_global_sum( REALMAXVAL=tmp2 )
          CALL parallel_global_sum( REAL=tmp3 )
          
          IF (par_rank.EQ.0)  THEN
             WRITE(*,'("After exact solver: ||res||=",3(1x,E15.8))') &
                  SQRT( tmp1/REAL(nlocal_global*ne_local,pr) ), tmp2, &
                  SQRT( tmp3/sumdA_global )
          END IF
       END IF

       IF(diagnostics_elliptic) THEN
          PRINT *, 'MIN/MAX(u)=',MINVAL(u_local), MAXVAL(u_local), &
	               'MIN/MAX(f)=',MINVAL(f_local), MAXVAL(f_local)
          !PAUSE  'in Linsolve before GMRES correction'
       END IF

       IF(GMRES_correction) THEN
          IF(diagnostics_elliptic) PRINT *, 'GMRES correction'
          CALL correction_factor (res(:,:,j_lev-1), res(:,:,j_lev), dA_level(:,j_lev),w_min, w_max, w_correction, nlocal, ne_local)
          IF(diagnostics_elliptic) PRINT *, 'w_correction=',w_correction
          DO ie =1, ne_local
             u_local(:,ie) = v(:,ie,j_lev) + w_correction(ie) * (u_local(:,ie)-v(:,ie,j_lev)) 
          END DO
!!$        mh = Nwlt_lev(j_lev,0)
!!$        m=nlocal
!!$        !internal points
!!$        IF(diagnostics_elliptic) PRINT *, 'internal points'
!!$        CALL correction_factor (res(1:mh,:,j_lev-1), res(1:mh,:,j_lev), dA_level(1:mh,j_lev), w_min, w_max, w_correction, nlocal, ne_local)
!!$        DO ie =1, ne_local
!!$           u_local(1:mh,ie) = v(1:mh,ie,j_lev) + w_correction(ie) * (u_local(1:mh,ie)-v(:,ie,j_lev)) 
!!$        END DO
!!$        !boundary points
!!$        IF(diagnostics_elliptic) PRINT *, 'boundary points'
!!$        IF( mh < m) THEN
!!$           CALL correction_factor (res(mh+1:m,:,j_lev-1), res(mh+1:m,:,j_lev), dA_level(mh+1:m,j_lev), w1, w1, w_correction, nlocal-mh, ne_local)
!!$           DO ie =1, ne_local
!!$              u_local(mh+1:m,ie) = v(mh+1:m,ie,j_lev) + w_correction(ie) * (u_local(mh+1:m,ie)-v(mh+1:m,ie,j_lev)) 
!!$           END DO
!!$        END IF
          res(:,:,j_lev) = f_local - RESHAPE(Lulocal (j_lev, u_local, nlocal, ne_local, HIGH_ORDER), (/nlocal, ne_local/) )

          IF(diagnostics_elliptic .OR. display_Vcycle) THEN
             tmp1 = SUM(SUM(res(:,:,j_lev)**2,DIM=2))
             tmp2 = MAXVAL (ABS(res(:,:,j_lev)))
             tmp3 = SUM(SUM(res(:,:,j_lev)**2,DIM=2)*dA)
             CALL parallel_global_sum( REAL=tmp1 )
             CALL parallel_global_sum( REALMAXVAL=tmp2 )
             CALL parallel_global_sum( REAL=tmp3 )
             
             IF (par_rank.EQ.0)  THEN
                WRITE(*,'("After GMRES correction: ||res||=",3(1x,E15.8))') &
                     SQRT( tmp1/REAL(nlocal_global*ne_local,pr) ), tmp2, &
                     SQRT( tmp3/sumdA_global )
             END IF
          END IF
          IF (ALL(w_correction==w_min) ) THEN
             repeart_corrections = repeart_corrections + 1
          ELSE
             repeart_corrections = 0
          END IF
#ifdef MULTIPROC
          CALL parallel_global_sum( INTEGERMAXVAL=repeart_corrections )
#endif

       END IF

       IF(diagnostics_elliptic) THEN
          PRINT *, 'MIN/MAX(u)=',MINVAL(u_local), MAXVAL(u_local), &
	               'MIN/MAX(f)=',MINVAL(f_local), MAXVAL(f_local)
          !PAUSE  'in Linsolve after GMRES correction'
       END IF

       !OLEG: I put this change to stabilize solutoin when f = 0 and initial guess is 0
       DO ie = 1, ne_local  !L2-norm
          tmp1 = SUM(u_local(:,ie)**2*dA)
          CALL parallel_global_sum( REAL=tmp1 )
          scl_u(ie) =  SQRT( tmp1 )
       END DO
       scl_local = MAX(scl_u,scl_f)
       IF( PRESENT(SCL) ) scl_local = MAX(scl_local, SCL)
       WHERE( scl_local < floor_rhs ) scl_local = 1.0_pr ! if scl_local is small, it becomes absolute error
       tolh = tol * scl_local
       
       IF(diagnostics_elliptic) THEN
          PRINT *, 'scl_u=',scl_u
          PRINT *, 'scl_f=',scl_f
          IF( PRESENT(SCL) ) PRINT *, 'SCL=',SCL
          PRINT *, 'scl_local=',scl_local
       END IF
       
       DO ie = 1, ne_local
          tmp1 = SUM(res(:,ie,j_lev)**2*dA)
          CALL parallel_global_sum( REAL=tmp1 )
          error(ie) = SQRT( tmp1/sumdA_global )     !--l2 error
!!$        error(ie) = MAXVAL(ABS(res(:,ie,j_lev))) !L_infty error
       END DO
#ifdef NO_wlog_diagnostics_elliptic                                                                                         
#else                                                                                                                       
       IF (wlog) WRITE (6,'("      After V-cycle", i4, ": relative L2 residue = ",20(es21.11,1x))') k,error/scl_local !was es11.4 format DG
       IF (wlog) WRITE (6,'("      After V-cycle", i4, ": absolute L2 residue = ",20(es21.11,1x))') k, error
#endif                                                                                                                      
       k = k+1
       IF( repeart_corrections > 4) THEN
          IF(par_rank.EQ.0) WRITE(*,'("WARNING: the iterations stopped converging, and started diverging")')
          k =k + 100
       END IF

!       PAUSE 'in WLinsolve'
    END DO

#ifdef NO_wlog_diagnostics_elliptic                                                                                         
#else                                                                                                                       
    IF (wlog) WRITE (6,'(" ")')
#endif                                                                                                                      

  END SUBROUTINE WLinsolve

  SUBROUTINE correction_factor (res_old, res_new, dA_local, w_min, w_max, w_correction, nlocal, ne_local)
    !-- correction factor for U_new = U_old  + w * dU
    USE precision
    USE parallel
    IMPLICIT NONE

    INTEGER,                          INTENT (IN) :: nlocal, ne_local
    REAL (pr), DIMENSION (nlocal, ne_local), INTENT (IN) :: res_old, res_new
    REAL (pr), DIMENSION (nlocal), INTENT (IN) :: dA_local
    REAL (pr), INTENT (IN) :: w_min, w_max
    REAL (pr), DIMENSION (ne_local), INTENT (INOUT) :: w_correction
    REAL (pr), DIMENSION (nlocal, ne_local) :: del_res
    REAL (pr) :: Rold_Rold, Rnew_Rnew, dR_dR, dR_Rold, sumdA_local
	REAL (pr) :: Emax_old, Emax_new, Emax_corrected
    INTEGER :: ie

    IF(diagnostics_elliptic) PRINT *, '    correction_factor()'!: sum dA=',sumdA_local
    del_res = res_new - res_old
    DO ie =1, ne_local
       dR_dR = DOT_PRODUCT(del_res(:,ie),del_res(:,ie))
       dR_Rold = DOT_PRODUCT(del_res(:,ie),res_old(:,ie))
#ifdef MULTIPROC
       CALL parallel_global_sum( REAL=dR_dR )
       CALL parallel_global_sum( REAL=dR_Rold )
#endif
       IF(diagnostics_elliptic) THEN
          Rold_Rold = DOT_PRODUCT(res_old(:,ie),res_old(:,ie))
          Rnew_Rnew = DOT_PRODUCT(res_new(:,ie),res_new(:,ie))
#ifdef MULTIPROC
       CALL parallel_global_sum( REAL=Rold_Rold )
       CALL parallel_global_sum( REAL=Rnew_Rnew )
#endif
       END IF
!!$     dR_dR = DOT_PRODUCT(del_res(:,ie),del_res(:,ie)*dA_local(:))/sumdA_local
!!$     dR_Rold = DOT_PRODUCT(del_res(:,ie),res_old(:,ie)*dA_local(:))/sumdA_local
!!$     IF(diagnostics_elliptic) THEN
!!$        Rold_Rold = DOT_PRODUCT(res_old(:,ie),res_old(:,ie)*dA_local(:))/sumdA_local
!!$        Rnew_Rnew = DOT_PRODUCT(res_new(:,ie),res_new(:,ie)*dA_local(:))/sumdA_local
!!$     END IF
       w_correction(ie) = -dR_Rold/dR_dR
       IF(diagnostics_elliptic) THEN
          WRITE(*,'("     w_correction=",E12.5," res: ",3(E12.5,1x))') &
               w_correction(ie), SQRT(Rold_Rold), SQRT(Rnew_Rnew), &
               SQRT(ABS(Rold_Rold+2.0_pr*w_correction(ie)*dR_Rold+w_correction(ie)**2*dR_dR)) 
       END IF
       w_correction(ie) = MIN(w_max,MAX(w_min, -dR_Rold/dR_dR ))
       IF(diagnostics_elliptic) THEN
          WRITE(*,'("     w_correction=",E12.5," res: ",3(E12.5,1x))') &
               w_correction(ie), SQRT(Rold_Rold), SQRT(Rnew_Rnew), &
               SQRT(ABS(Rold_Rold+2.0_pr*w_correction(ie)*dR_Rold+w_correction(ie)**2*dR_dR)) 
       END IF
    END DO

  END SUBROUTINE correction_factor

  PURE FUNCTION i_clip (jlev) 
    USE wlt_trns_vars       ! indx_DB
    IMPLICIT NONE 
    INTEGER, INTENT (IN):: jlev
    INTEGER :: i_clip

    IF (MINVAL(prd) == 1) THEN  ! all directions periodic 
       i_clip = 1
    ELSE ! atleast one direction is non-periodic
       i_clip = 1   !05.17.2011!Oleg!  Nwlt_lev(jlev,0) + 1! + mxyz(2)/2
    END IF


  END FUNCTION i_clip

  SUBROUTINE BiCGSTAB (jlev, nlocal, ne_local, len, meth, clip, tol, u_local, f_local, Lulocal, Lulocal_diag, SCL)
    !--Solves the linear equation L(u) = f using Bi-CGSTAB algorithm (van der Vorst 1992)
    !--This is a conjugate gradient type algorithm
    USE precision
    USE elliptic_vars
    USE util_vars         ! dA
    USE parallel
    IMPLICIT NONE
    INTEGER, INTENT (IN)        :: jlev, nlocal, ne_local, len, meth
    INTEGER, DIMENSION(ne_local), INTENT(IN):: clip
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (IN) :: f_local
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (INOUT) :: u_local
    REAL (pr), DIMENSION (ne_local), INTENT (IN), OPTIONAL :: SCL
    REAL (pr), INTENT (IN)  :: tol

    INTEGER :: i, ie, shift
    REAL (pr) :: alpha, b1, error, error_scale, omega1, ro, ro_old, floor
    REAL (pr) :: minmax_error_scale(0:1) , u_clip
    REAL (pr), DIMENSION (nlocal*ne_local) :: diag, p, res, res0, s1, t_local, v, res_h
    INTEGER :: iclip
    REAL(pr) :: tmp1, tmp2, tmp3
    LOGICAL :: tmpl

    INTERFACE
       FUNCTION Lulocal (jlev, u_local, nlocal, ne_local, meth)
         USE precision
         USE sizes
         USE pde
         IMPLICIT NONE
         INTEGER, INTENT (IN) :: jlev, nlocal, ne_local, meth
         REAL (pr), DIMENSION (nlocal*ne_local), INTENT (INOUT) :: u_local
         REAL (pr), DIMENSION (nlocal*ne_local) :: Lulocal
       END FUNCTION Lulocal
       FUNCTION Lulocal_diag (jlev,  nlocal, ne_local, meth)
         USE precision
         USE sizes
         USE pde
         IMPLICIT NONE
         INTEGER, INTENT (IN) :: jlev, nlocal, ne_local, meth
         REAL (pr), DIMENSION (nlocal*ne_local) :: Lulocal_diag
       END FUNCTION Lulocal_diag
    END INTERFACE

    iclip = i_clip(jlev)

    floor = 1.e-12_pr


    ! DO itmp=0, ne_local-1
    !   PRINT *,' MINMAX(f) itmp+1 ', MINVAL(f_local(itmp*nlocal+1:(1+itmp)*nlocal)),MAXVAL(f_local(itmp*nlocal+1:(1+itmp)*nlocal))
    !   PRINT *,' MINMAX(u) itmp+1 ', MINVAL(u_local(itmp*nlocal+1:(1+itmp)*nlocal)),MAXVAL(u_local(itmp*nlocal+1:(1+itmp)*nlocal))
    !END DO

    IF(diagnostics_elliptic) THEN
       PRINT *, 'calling Lulocal_diag() from BiCGSTAB(), meth = ', meth
       PRINT *,'MINMAX(f)',MINVAL(f_local),MAXVAL(f_local)
       PRINT *,'MINMAX(u)',MINVAL(u_local),MAXVAL(u_local)
    END IF

    diag = Lulocal_diag(jlev, nlocal, ne_local, meth)
    
    IF(diagnostics_elliptic) PRINT *, 'MINMAX(diag)=', MINVAL(ABS(diag)), MINVAL(diag), MAXVAL(diag)
    
    
    DO ie=1, ne_local
       shift=(ie-1)*nlocal
       !clipping the solution for ie variable
       IF(clip(ie) == 1  .AND. .NOT.elliptic_project) THEN
          u_clip = u_local(shift+iclip)
          CALL parallel_broadcast( REAL=u_clip )
          u_local(shift+1:ie*nlocal)= u_local(shift+1:ie*nlocal)- u_clip
       ELSE IF(clip(ie) == 1 .AND. elliptic_project) THEN
          u_clip = 0.0_pr
          u_clip = SUM(u_local(shift+1:ie*nlocal)*dA_level(1:nlocal,jlev))
          CALL parallel_global_sum( REAL=u_clip )
          u_clip = u_clip/sumdA_global
          u_local(shift+1:ie*nlocal)= u_local(shift+1:ie*nlocal)-u_clip
       END IF
    END DO
    
    res = 0.0_pr
    IF(diagnostics_elliptic) PRINT *, jlev, MINVAL(u_local), MAXVAL(u_local), nlocal, ne_local, meth

    res = (f_local - Lulocal (jlev, u_local, nlocal, ne_local, meth) )/diag

    IF(BC_zone) THEN
       res_h = res
       res = 0.0_pr
       DO ie=1, ne_local
          shift=(ie-1)*nlocal
          res(shift+i_gmres_zone(1:n_gmres_zone(jlev),jlev)) = res_h(shift+i_gmres_zone(1:n_gmres_zone(jlev),jlev))
       END DO
    END IF

    IF(diagnostics_elliptic) PRINT *,'MINMAX(res)',MINVAL(res),MAXVAL(res)
    !PAUSE ' in BiCGSTAB after Lcn'
    
    res0 = res

    ro   = 1.0_pr
    alpha = 1.0_pr
    omega1 = 1.0_pr

    v     = 0.0_pr
    p     = 0.0_pr

    i = 0
    DO
       !--------------------- checking the error --------------------
       error = 0.0_pr
       minmax_error_scale = (/ 1.E24_pr, 0.0_pr /)

       DO ie = 1, ne_local
          shift = (ie-1) * nlocal
          !original    error = error + SUM((diag(shift+1:shift+nlocal)*res(shift+1:shift+nlocal))**2*dA)
!!$        error_scale = MAX( SUM((f_local(shift+1:shift+nlocal))**2*dA), SUM((diag(shift+1:shift+nlocal)*u_local(shift+1:shift+nlocal))**2*dA) )

          tmp1 = SUM( u_local(shift+1:shift+nlocal)**2*dA )
          CALL parallel_global_sum( REAL=tmp1 )
          error_scale = tmp1/sumdA_global
          IF(error_scale < floor) error_scale = 1.0_pr
          
          !use of optional scaling argument.  Need to ensure since one variable can be exactly zer like in Poiseulle flow
          IF( PRESENT(SCL) ) error_scale = MAX(error_scale, SCL(ie)**2)
          minmax_error_scale(0) = MIN(minmax_error_scale(0),error_scale)
          minmax_error_scale(1) = MAX(minmax_error_scale(1),error_scale)
!!$        error = error + SUM((diag(shift+1:shift+nlocal)*res(shift+1:shift+nlocal))**2*dA)/ error_scale
          !note res is already normalized by diag, so it measures error in u
          tmp1 = SUM(res(shift+1:shift+nlocal)**2*dA)
          CALL parallel_global_sum( REAL=tmp1 )
          error = error + tmp1/ error_scale 
          
       END DO
       
       error = SQRT( error/REAL(ne_local) )
       
       !PRINT *,'Using Relative Error'
       
       IF(diagnostics_elliptic) &
            WRITE (6,'("BICGSTAB iteration ",i3,1x," l2 error = ",es11.4," min_error_scale= ",es11.4," max_error_scale= ",es11.4)') &
            i, error, minmax_error_scale
       !PAUSE
       
       !IF ( ALL( errors(1:ne_local) <= tol * error_scales(1:ne_local) ) .OR. i==len) EXIT
       ! tmpl == do continue
       tmpl = .NOT.( (error <= tol .OR. i==len) .AND. (i > 0 .OR. len == 0) )
       CALL parallel_global_sum( LOGICALOR=tmpl )
       IF ( .NOT.tmpl ) EXIT
       !-------------------------------------------------------------

       i = i+1

       ro_old = ro
       !PRINT *,'MINMAX(res0(1:nlocal) ', MINVAL(res0(1:nlocal)), MAXVAL(res0(1:nlocal))
       !PRINT *,'MINMAX(res0(nlocal+1:2*nlocal) ', MINVAL(res0(nlocal+1:2*nlocal)), MAXVAL(res0(nlocal+1:2*nlocal))
       !PRINT *,'MINMAX(res0(2*nlocal+1:3*nlocal) ', MINVAL(res0(2*nlocal+1:3*nlocal)), MAXVAL(res0(2*nlocal+1:3*nlocal))
       !PRINT *,'MINMAX(res0(3*nlocal+1:4*nlocal) ', MINVAL(res0(3*nlocal+1:4*nlocal)), MAXVAL(res0(3*nlocal+1:4*nlocal))
       !PRINT *,'MINMAX(res0(4*nlocal+1:5*nlocal) ', MINVAL(res0(4*nlocal+1:5*nlocal)), MAXVAL(res0(4*nlocal+1:5*nlocal))
       !PRINT *,'MINMAX(res(1:nlocal) ', MINVAL(res(1:nlocal)), MAXVAL(res(1:nlocal))
       !PRINT *,'MINMAX(res(nlocal+1:2*nlocal) ', MINVAL(res(nlocal+1:2*nlocal)), MAXVAL(res(nlocal+1:2*nlocal))
       !PRINT *,'MINMAX(res(2*nlocal+1:3*nlocal) ', MINVAL(res(2*nlocal+1:3*nlocal)), MAXVAL(res(2*nlocal+1:3*nlocal))
       !PRINT *,'MINMAX(res(3*nlocal+1:4*nlocal) ', MINVAL(res(3*nlocal+1:4*nlocal)), MAXVAL(res(3*nlocal+1:4*nlocal))
       !PRINT *,'MINMAX(res(4*nlocal+1:5*nlocal) ', MINVAL(res(4*nlocal+1:5*nlocal)), MAXVAL(res(4*nlocal+1:5*nlocal))
       !PRINT *,'MINMAX(res0 ', MINVAL(res0), MAXVAL(res0)
       !PRINT *,'MINMAX(res ', MINVAL(res), MAXVAL(res)

       !PRINT *,'DOT_PRODUCT (res0(1:nlocal), res(1:nlocal)) ', DOT_PRODUCT (res0(1:nlocal), res(1:nlocal))
       !PRINT *,'DOT_PRODUCT (res0(nlocal+1:2*nlocal), res(nlocal+1:2*nlocal)) ', &
       !         DOT_PRODUCT (res0(nlocal+1:2*nlocal), res(nlocal+1:2*nlocal))
       !PRINT *,'DOT_PRODUCT (res0(2*nlocal+1:3*nlocal), res(2*nlocal+1:3*nlocal)) ', &
       !         DOT_PRODUCT (res0(2*nlocal+1:3*nlocal), res(2*nlocal+1:3*nlocal))
       !PRINT *,'DOT_PRODUCT (res0(3*nlocal+1:4*nlocal), res(3*nlocal+1:4*nlocal)) ', &
       !         DOT_PRODUCT (res0(3*nlocal+1:4*nlocal), res(3*nlocal+1:4*nlocal))
       !PRINT *,'DOT_PRODUCT (res0(4*nlocal+1:5*nlocal), res(4*nlocal+1:5*nlocal)) ', &
       !         DOT_PRODUCT (res0(4*nlocal+1:5*nlocal), res(4*nlocal+1:5*nlocal))


       ro = DOT_PRODUCT (res0, res) 
#ifdef MULTIPROC
       CALL parallel_global_sum( REAL=ro )
#endif
       !!DO itmp=1,n
       !!   ro = ro + res(itmp) * res0(itmp)
       !!   print *, itmp, ro
       !!END DO

       b1 = (ro/ro_old) * (alpha/omega1)

       p = res + b1 * (p - omega1 * v)
!!$       PRINT *,'MINMAX(p)',MINVAL(p),MAXVAL(p)

       DO ie=1, ne_local
          shift=(ie-1)*nlocal
          !clipping the solution for ie variable
          !Oleg: this feature seem to cause problem with convergence, since it changes the orthogolanilzation
          !IF(clip(ie) == 1) THEN
          !   u_clip = p(shift+iclip)
          !   CALL parallel_broadcast( REAL=u_clip )
          !   p(shift+1:ie*nlocal)= p(shift+1:ie*nlocal)-u_clip
          !END IF
       END DO

       v = Lulocal (jlev, p, nlocal, ne_local, meth)/diag

       IF(BC_zone) THEN
          res_h = v
          v = 0.0_pr
          DO ie=1, ne_local
             shift=(ie-1)*nlocal
             v(shift+i_gmres_zone(1:n_gmres_zone(jlev),jlev)) = res_h(shift+i_gmres_zone(1:n_gmres_zone(jlev),jlev))
          END DO
       END IF

!!$       PRINT *, 'MINMAX(v)=', MINVAL(v), MAXVAL(v)
       !PAUSE 'in BiCGSTAB after Lcn'

       !**** note if v = 0, then p is null space of operator Lulocal
       !PRINT *,'MINMAX(v)',MINVAL(v),MAXVAL(v)

       tmp1 = DOT_PRODUCT (res0, v)
#ifdef MULTIPROC
       CALL parallel_global_sum( REAL=tmp1 )
#endif
       IF(ABS(ro) > 0.0_pr) THEN
          alpha = ro / tmp1
       ELSE
          alpha = ro
       END IF

       s1 = res - alpha * v
!!$       PRINT *,'MINMAX(s1)',MINVAL(s1),MAXVAL(s1)
!!$       PRINT *,'MINMAX(res)',MINVAL(res),MAXVAL(res)
!!$       PRINT *,'MINMAX(v)',MINVAL(v),MAXVAL(v)
       !PRINT *,'MINMAX(s1(1:nlocal) ', MINVAL(s1(1:nlocal)), MAXVAL(s1(1:nlocal))
       !PRINT *,'MINMAX(s1(nlocal+1:2*nlocal) ', MINVAL(s1(nlocal+1:2*nlocal)), MAXVAL(s1(nlocal+1:2*nlocal))
       !PRINT *,'MINMAX(s1(2*nlocal+1:3*nlocal) ', MINVAL(s1(2*nlocal+1:3*nlocal)), MAXVAL(s1(2*nlocal+1:3*nlocal))
       !PRINT *,'MINMAX(s1(3*nlocal+1:4*nlocal) ', MINVAL(s1(3*nlocal+1:4*nlocal)), MAXVAL(s1(3*nlocal+1:4*nlocal))
       !PRINT *,'MINMAX(s1(4*nlocal+1:5*nlocal) ', MINVAL(s1(4*nlocal+1:5*nlocal)), MAXVAL(s1(4*nlocal+1:5*nlocal))
       !PAUSE

       DO ie=1, ne_local
          shift=(ie-1)*nlocal
          !clipping the solution for ie variable
          !Oleg: this feature seem to cause problem with convergence, since it changes the orthogolanilzation
          !IF(clip(ie) == 1) THEN
          !   u_clip = s1(shift+iclip)
          !   CALL parallel_broadcast( REAL=u_clip )
          !   s1(shift+1:ie*nlocal)= s1(shift+1:ie*nlocal)-u_clip
          !END IF
       END DO

       t_local = Lulocal (jlev, s1, nlocal, ne_local, meth)/diag

       IF(BC_zone) THEN
          res_h = t_local
          t_local = 0.0_pr
          DO ie=1, ne_local
             shift=(ie-1)*nlocal
             t_local(shift+i_gmres_zone(1:n_gmres_zone(jlev),jlev)) = res_h(shift+i_gmres_zone(1:n_gmres_zone(jlev),jlev))
          END DO
       END IF

!!$       PRINT *,'MINMAX(t_local)',MINVAL(t_local),MAXVAL(t_local)
       !PAUSE

       tmpl = (MAXVAL(ABS(t_local)) >= floor)
#ifdef MULTIPROC
       CALL parallel_global_sum( LOGICALOR=tmpl )
#endif
       IF (tmpl) THEN
          tmp1 = DOT_PRODUCT (t_local, s1)
          tmp2 = DOT_PRODUCT (t_local, t_local)
#ifdef MULTIPROC
       CALL parallel_global_sum( REAL=tmp1 )
       CALL parallel_global_sum( REAL=tmp2 )
#endif
          omega1 = tmp1 / tmp2
       ELSE
          omega1 = 0.0_pr
       END IF
!!$       PRINT *,'omega1 ',omega1

       u_local = u_local + alpha*p + omega1*s1
!!$       PRINT *,'MINMAX(u_local)',MINVAL(u_local),MAXVAL(u_local)
       res = s1 - omega1 * t_local
    END DO

    DO ie=1, ne_local
       shift=(ie-1)*nlocal
       !clipping the solution for ie variable
       IF(clip(ie) == 1  .AND. .NOT.elliptic_project) THEN
          u_clip = u_local(shift+iclip)
          CALL parallel_broadcast( REAL=u_clip )
          u_local(shift+1:ie*nlocal)= u_local(shift+1:ie*nlocal)- u_clip
       ELSE IF(clip(ie) == 1 .AND. elliptic_project) THEN
          u_clip = 0.0_pr
          u_clip = SUM(u_local(shift+1:ie*nlocal)*dA_level(1:nlocal,jlev))
          CALL parallel_global_sum( REAL=u_clip )
          u_clip = u_clip/sumdA_global
          u_local(shift+1:ie*nlocal)= u_local(shift+1:ie*nlocal)-u_clip
       END IF
    END DO


  END SUBROUTINE BiCGSTAB


  SUBROUTINE gmres (jlev, kry, nlocal, ne_local, meth, clip, psi, b_loc, Lulocal, Lulocal_diag)
    !--Solves a linear system of the form Apsi = b, using initial guess psi0 
    USE precision
    USE elliptic_vars
    USE pde
    USE util_vars
    USE parallel
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: jlev, kry, nlocal, ne_local, meth
    INTEGER, DIMENSION(ne_local), INTENT(IN):: clip
    INTEGER :: i, j, info, kryh, ntotal, ie, shift
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (IN) :: b_loc
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (INOUT) :: psi
    REAL (pr), DIMENSION (nlocal*ne_local) :: w, res, res_h
    REAL (pr), DIMENSION (nlocal*ne_local, kry+1) :: v 
    REAL (pr), DIMENSION (kry+1, kry) :: hess
    REAL (pr)                         :: hess_last
    REAL (pr), DIMENSION (kry+1) :: e1
    REAL (pr) :: beta_loc, scl_local, u_clip
    REAL (pr), DIMENSION (kry+(kry+1)*32) :: work
    REAL (pr), DIMENSION (nlocal*ne_local) :: Lulocal_diag_tmp ! local result of Lulocal_diag
    !  REAL (pr), DIMENSION (nlocal*ne_local) :: Lulocal_tmp ! temp array
    INTEGER :: iclip
    INTEGER :: nlocal_global
    REAL(pr) :: tmp1, tmp2, tmp3

    INTERFACE 
       FUNCTION Lulocal (jlev, u_local, nlocal, ne_local, meth)
         USE precision
         USE sizes
         USE pde
         IMPLICIT NONE
         INTEGER, INTENT (IN) :: jlev, nlocal, ne_local, meth
         REAL (pr), DIMENSION (nlocal*ne_local), INTENT (INOUT) :: u_local
         REAL (pr), DIMENSION (nlocal*ne_local) :: Lulocal
       END FUNCTION Lulocal
       FUNCTION Lulocal_diag (jlev,  nlocal, ne_local, meth)
         USE precision
         USE sizes
         USE pde
         IMPLICIT NONE
         INTEGER, INTENT (IN) :: jlev, nlocal, ne_local, meth
         REAL (pr), DIMENSION (nlocal*ne_local) :: Lulocal_diag
       END FUNCTION Lulocal_diag
    END INTERFACE

    iclip = i_clip(jlev)

#ifdef DEBUG_GMRES
    WRITE (*,'(A, I6, A, F, A, F)')                  'rank=',par_rank, '   gmres 0: MIN/MAX(psi)=',MINVAL(psi),'   ',MAXVAL(psi)
    WRITE (*,'(A, I6, A, I9, A, I3, A, I3, A, I3)')  'rank=',par_rank, '   nlocal=',nlocal, '   ne_local=',ne_local, '   iclip=',iclip, '   clip=',clip
#endif

    DO ie=1, ne_local
       shift=(ie-1)*nlocal
       !clipping the solution for ie variable
       IF(clip(ie) == 1  .AND. .NOT.elliptic_project) THEN
          u_clip = psi(shift+iclip)
          CALL parallel_broadcast( REAL=u_clip )
          psi(shift+1:ie*nlocal)= psi(shift+1:ie*nlocal)- u_clip
       ELSE IF(clip(ie) == 1 .AND. elliptic_project) THEN
          u_clip = 0.0_pr
          u_clip = SUM(psi(shift+1:ie*nlocal)*dA_level(1:nlocal,jlev))
          CALL parallel_global_sum( REAL=u_clip )
          u_clip = u_clip/sumdA_global
          psi(shift+1:ie*nlocal)= psi(shift+1:ie*nlocal)-u_clip
       END IF
    END DO
    
#ifdef DEBUG_GMRES
    PRINT *, 'gmres 1: MIN/MAX(psi)=',MINVAL(psi),MAXVAL(psi)
#endif

    nlocal_global = nlocal
    tmp1 = SUM(psi(:)**2)
    tmp2 = SUM(b_loc(:)**2)
    CALL parallel_global_sum( INTEGER=nlocal_global )
    CALL parallel_global_sum( REAL=tmp1 )
    CALL parallel_global_sum( REAL=tmp2 )
    scl_local =  MAX(SQRT(tmp1/REAL(nlocal_global*ne_local,pr)),SQRT(tmp2/REAL(nlocal_global*ne_local,pr)),1.e-12_pr)
    
    ntotal = nlocal*ne_local
    !kryh = MIN(kry,ntotal)
    kryh = kry
    hess_last = HUGE(hess_last) !set hass_last to max possile value
    
    
#ifdef DEBUG_GMRES
    PRINT *, '=========== gmres==============='
    PRINT *, 'scl_local=',scl_local,'kryh=',kryh,'hess_last=',hess_last
#endif

    !
    ! Call Lulocal_diag once and store result 
    !
    Lulocal_diag_tmp = Lulocal_diag(jlev, nlocal, ne_local, meth)

#ifdef DEBUG_GMRES
    !  Lulocal_tmp = Lulocal (jlev, psi, nlocal, ne_local, meth) !TEST
    !Lulocal_diag_tmp = 1.0_pr
    PRINT *,'gmres Lulocal_diag_tmp', MINVAL(Lulocal_diag_tmp), MAXVAL(Lulocal_diag_tmp)
    !, SQRT(SUM(Lulocal_diag_tmp*Lulocal_diag_tmp))!TESTDG
    !PRINT *,'gmres ', MINVAL(Lulocal_tmp), MAXVAL(Lulocal_tmp), SQRT(SUM(Lulocal_tmp*Lulocal_tmp))!TESTDG
    PRINT *,'gmres b_loc', MINVAL(b_loc), MAXVAL(b_loc) !, SQRT(SUM(b_loc*b_loc))!TESTDG
    PRINT *, 'gmres 1.1: MIN/MAX(psi)=',MINVAL(psi),MAXVAL(psi)
    !PRINT *, 'calling Lulocal_diag() from gmres(), meth = ', meth
    !PRINT *, 'MINMAX(b_loc=)', MINVAL(b_loc), MAXVAL(b_loc)
#endif


    res = 0.0_pr

    res = ( b_loc - Lulocal (jlev, psi, nlocal, ne_local, meth) ) / Lulocal_diag_tmp

    IF(BC_zone) THEN
       res_h = res
       res = 0.0_pr
       DO ie=1, ne_local
          shift=(ie-1)*nlocal
          res(shift+i_gmres_zone(1:n_gmres_zone(jlev),jlev)) = res_h(shift+i_gmres_zone(1:n_gmres_zone(jlev),jlev))
       END DO
    END IF

    IF (debug_level > 0) PRINT *, 'res:', MINVAL(res), MAXVAL(res), MINVAL(ABS(res)), MAXVAL(ABS(res))


#ifdef DEBUG_GMRES
    !PRINT *, res
    !PAUSE 'in gmres'
    !PRINT *, b_loc
    PRINT *,'gmres res=', MINVAL(res), MAXVAL(res)!, SQRT(SUM(res*res))!TESTDG
    PRINT *, 'gmres 2: MIN/MAX(psi)=',MINVAL(psi),MAXVAL(psi)
    !TESTDG  PRINT *,'TEST L2norm psi(:,ie) (real space, in gmres )1 ' , SUM( psi(:)**2 )
    !PRINT *,'gmres ', MINVAL(res), MAXVAL(res), SQRT(SUM(res*res))!TESTDG
#endif



    tmp1 = DOT_PRODUCT (res, res)
#ifdef MULTIPROC
    CALL parallel_global_sum( REAL=tmp1 )
#endif
    beta_loc = SQRT ( tmp1 )

#ifdef DEBUG_GMRES
    WRITE(*,'("beta_loc = ", e21.11 )') beta_loc !TESTDG
#endif


    hess = 0.0_pr
    w    = 0.0_pr
    v    = 0.0_pr
    v(:,1) = res / MAX(beta_loc,1.0e-24_pr) 
    j = 1 

    DO WHILE( j <= kryh ) 

       DO ie=1, ne_local
          shift=(ie-1)*nlocal
          !clipping the solution for ie variable
          !Oleg: this feature seem to cause problem with convergence, since it changes the orthogolanilzation
          !IF(clip(ie) == 1) THEN
          !   u_clip = v(shift+iclip,j) 
          !   CALL parallel_broadcast( REAL=u_clip )
          !   v(shift+1:ie*nlocal,j)= v(shift+1:ie*nlocal,j)-u_clip
          !END IF
          IF(clip(ie) == 1 .AND. elliptic_project) THEN
             u_clip = 0.0_pr
             u_clip = SUM(v(shift+1:ie*nlocal,j)*dA_level(1:nlocal,jlev))
             CALL parallel_global_sum( REAL=u_clip )
             u_clip = u_clip/sumdA_global
             v(shift+1:ie*nlocal,j)= v(shift+1:ie*nlocal,j)-u_clip
          END IF
       END DO
       !PRINT *, 'calling Lulocal_diag() 2 from gmres(), meth = ', meth


       w = Lulocal(jlev, v(:,j), nlocal, ne_local, meth) / Lulocal_diag_tmp

       IF(BC_zone) THEN
          res_h = w
          w = 0.0_pr
          DO ie=1, ne_local
             shift=(ie-1)*nlocal
             w(shift+i_gmres_zone(1:n_gmres_zone(jlev),jlev)) = res_h(shift+i_gmres_zone(1:n_gmres_zone(jlev),jlev))
          END DO
       END IF

#ifdef DEBUG_GMRES
       PRINT *,'    gmres w', MINVAL(w), MAXVAL(w), SQRT(SUM(w*w))!TESTDG
       !PRINT *,'TEST gmres j, w', j,w
#endif


       DO i = 1, j
          hess (i,j) = DOT_PRODUCT (v(:,i), w)
#ifdef MULTIPROC
          CALL parallel_global_sum( REAL=hess (i,j) )
#endif

#ifdef DEBUG_GMRES
          PRINT *,'    TEST gmres i, v(:,i)',i, MINVAL(v(:,i)), MAXVAL(v(:,i))
#endif
          
          w = w - v(:,i) * hess(i,j) 
       END DO
       
       tmp1 = DOT_PRODUCT (w, w)
#ifdef MULTIPROC
       CALL parallel_global_sum( REAL=tmp1 )
#endif
       hess(j+1,j) = SQRT ( tmp1 )

       IF(hess(j+1,j) > tol_gmres * scl_local .AND. .NOT. ( tol_gmres_stop_if_larger .AND. hess(j+1,j) > hess_last)  )  THEN
          v(:,j+1) = w / hess (j+1,j)
       ELSE
          IF( hess(j+1,j) > hess_last ) PRINT *,'Exiting gmres iteration hess(j+1,j) > hess_last'
          v(:,j+1) = 0.0_pr
          kryh=j
       END IF
       hess_last = hess(j+1,j) ! save last error criterion

       IF(diagnostics_elliptic) WRITE (*,'("GMRES it = ",I4 ," Err = ",  es21.11 )') j, hess(j+1,j)
       j = j+1

    END DO
    e1 = 0.0_pr ; e1(1) = beta_loc
    
    
#ifdef DEBUG_GMRES
    !PRINT *,'gmres before DGLES kryh, info', kryh, info
    !PRINT *,'    gmres before DGLES hess ', hess
    !PRINT *,'    gmres before DGLES e1 ', e1
    !PRINT *,'    gmres before DGLES work ', par_rank, MINVAL(work), MAXVAL(work)
#endif

    
    CALL DGELS ('N', kryh+1, kryh, 1, hess, kry+1, e1, kry+1, work, kry+(kry+1)*32, info)

    
#ifdef DEBUG_GMRES
    PRINT *,'    gmres after  DGLES e1(1:kryh) ,', e1(1:kryh)
#endif



    IF (info /=0) WRITE (6,'(i3)') info

#ifdef DEBUG_GMRES
    PRINT *, 'gmres 3: MIN/MAX(psi)=',MINVAL(psi),MAXVAL(psi)
    PRINT *, 'gmres 3: MIN/MAX(v)=',MINVAL(v),MAXVAL(v)
    PRINT *, 'gmres 3: MIN/MAX(e1)=',MINVAL(e1),MAXVAL(e1)
#endif

    psi = psi + MATMUL (v(1:nlocal*ne_local,1:kryh), e1(1:kryh))

#ifdef DEBUG_GMRES
    PRINT *, 'gmres 4: MIN/MAX(psi)=',MINVAL(psi),MAXVAL(psi)
    PRINT *, 'gmres 3: MIN/MAX(v)=',MINVAL(v),MAXVAL(v)
    PRINT *, 'gmres 3: MIN/MAX(e1)=',MINVAL(e1),MAXVAL(e1)
#endif

    
    DO ie=1, ne_local
       shift=(ie-1)*nlocal
       !clipping the solution for ie variable
       IF(clip(ie) == 1  .AND. .NOT.elliptic_project) THEN
          u_clip = psi(shift+iclip)
          CALL parallel_broadcast( REAL=u_clip )
          psi(shift+1:ie*nlocal)= psi(shift+1:ie*nlocal)- u_clip
       ELSE IF(clip(ie) == 1 .AND. elliptic_project) THEN
          u_clip = 0.0_pr
          u_clip = SUM(psi(shift+1:ie*nlocal)*dA_level(1:nlocal,jlev))
          CALL parallel_global_sum( REAL=u_clip )
          u_clip = u_clip/sumdA_global
          psi(shift+1:ie*nlocal)= psi(shift+1:ie*nlocal)-u_clip
       END IF
    END DO
#ifdef DEBUG_GMRES
    PRINT *, 'gmres 5: MIN/MAX(psi)=',MINVAL(psi),MAXVAL(psi)
#endif

  END SUBROUTINE gmres

  SUBROUTINE Wgmres (jlev, kry, nlocal, ne_local, meth, clip, psi, b_loc, Lulocal, Lulocal_diag, Lulocal_Wdiag)
    !--Solves a linear system of the form Apsi = b, using initial guess psi0 
    USE precision
    USE elliptic_vars
    USE pde
    USE util_vars         ! dA
    USE parallel
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: jlev, kry, nlocal, ne_local, meth
    INTEGER, DIMENSION(ne_local), INTENT(IN):: clip
    INTEGER :: i, j, info, kryh, ntotal, ie, shift
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (IN) :: b_loc
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (INOUT) :: psi
    REAL (pr), DIMENSION (nlocal*ne_local) :: w, res
    REAL (pr), DIMENSION (nlocal*ne_local, kry+1) :: v 
    REAL (pr), DIMENSION (kry+1, kry) :: hess
    REAL (pr)                         :: hess_last
    REAL (pr), DIMENSION (kry+1) :: e1
    REAL (pr) :: beta_loc, scl_local, u_clip
    REAL (pr), DIMENSION (kry+(kry+1)*32) :: work
    REAL (pr), DIMENSION (nlocal*ne_local) :: Lulocal_diag_tmp ! local result of Lulocal_diag
    REAL (pr), DIMENSION (nlocal*ne_local) :: Lulocal_Wdiag_tmp ! local result of Lulocal_Wdiag
    !  REAL (pr), DIMENSION (nlocal*ne_local) :: Lulocal_tmp ! temp array
    INTEGER :: iclip
    INTEGER :: nlocal_global
    REAL(pr) :: tmp1, tmp2, tmp3

    INTERFACE 
       FUNCTION Lulocal (jlev, u_local, nlocal, ne_local, meth)
         USE precision
         USE sizes
         USE pde
         IMPLICIT NONE
         INTEGER, INTENT (IN) :: jlev, nlocal, ne_local, meth
         REAL (pr), DIMENSION (nlocal*ne_local), INTENT (INOUT) :: u_local
         REAL (pr), DIMENSION (nlocal*ne_local) :: Lulocal
       END FUNCTION Lulocal
       FUNCTION Lulocal_diag (jlev,  nlocal, ne_local, meth)
         USE precision
         USE sizes
         USE pde
         IMPLICIT NONE
         INTEGER, INTENT (IN) :: jlev, nlocal, ne_local, meth
         REAL (pr), DIMENSION (nlocal*ne_local) :: Lulocal_diag
       END FUNCTION Lulocal_diag
       FUNCTION Lulocal_Wdiag (jlev,  nlocal, ne_local, meth)
         USE precision
         USE sizes
         USE pde
         IMPLICIT NONE
         INTEGER, INTENT (IN) :: jlev, nlocal, ne_local, meth
         REAL (pr), DIMENSION (nlocal*ne_local) :: Lulocal_Wdiag
       END FUNCTION Lulocal_Wdiag
    END INTERFACE

    iclip = i_clip(jlev)

#ifdef DEBUG_GMRES
    PRINT *, 'Wgmres 0: MIN/MAX(psi)=',MINVAL(psi),MAXVAL(psi)
    PRINT *,'nlocal=',nlocal,'ne_local=',ne_local,'iclip=',iclip,'clip=',clip
#endif

    DO ie=1, ne_local
       shift=(ie-1)*nlocal
       !clipping the solution for ie variable
       IF(clip(ie) == 1  .AND. .NOT.elliptic_project) THEN
          u_clip = psi(shift+iclip)
          CALL parallel_broadcast( REAL=u_clip )
          psi(shift+1:ie*nlocal)= psi(shift+1:ie*nlocal)- u_clip
       ELSE IF(clip(ie) == 1 .AND. elliptic_project) THEN
          u_clip = 0.0_pr
          u_clip = SUM(psi(shift+1:ie*nlocal)*dA_level(1:nlocal,jlev))
          CALL parallel_global_sum( REAL=u_clip )
          u_clip = u_clip/sumdA_global
          psi(shift+1:ie*nlocal)= psi(shift+1:ie*nlocal)-u_clip
       END IF
    END DO
    
#ifdef DEBUG_GMRES
    PRINT *, 'Wgmres 1: MIN/MAX(psi)=',MINVAL(psi),MAXVAL(psi)
#endif

    nlocal_global = nlocal
    tmp1 = SUM(psi(:)**2)
    tmp2 = SUM(b_loc(:)**2)
    CALL parallel_global_sum( INTEGER=nlocal_global )
    CALL parallel_global_sum( REAL=tmp1 )
    CALL parallel_global_sum( REAL=tmp2 )
    scl_local =  MAX(SQRT(tmp1/REAL(nlocal_global*ne_local,pr)),SQRT(tmp2/REAL(nlocal_global*ne_local,pr)),1.e-12_pr)
    
    ntotal = nlocal*ne_local
    !kryh = MIN(kry,ntotal)
    kryh = kry
    hess_last = HUGE(hess_last) !set hass_last to max possile value
    
    
#ifdef DEBUG_GMRES
    PRINT *, '=========== Wgmres==============='
    PRINT *, 'scl_local=',scl_local,'kryh=',kryh,'hess_last=',hess_last
#endif

    !
    ! Call Lulocal_diag once and store result 
    !
    Lulocal_diag_tmp  = Lulocal_diag(jlev, nlocal, ne_local, meth)
    Lulocal_Wdiag_tmp = Lulocal_Wdiag(jlev, nlocal, ne_local, meth)



#ifdef DEBUG_GMRES
    !  Lulocal_tmp = Lulocal (jlev, psi, nlocal, ne_local, meth) !TEST
    !Lulocal_diag_tmp = 1.0_pr
    PRINT *,'Wgmres Lulocal_diag_tmp', MINVAL(Lulocal_diag_tmp), MAXVAL(Lulocal_diag_tmp)
    !, SQRT(SUM(Lulocal_diag_tmp*Lulocal_diag_tmp))!TESTDG
    !PRINT *,'Wgmres ', MINVAL(Lulocal_tmp), MAXVAL(Lulocal_tmp), SQRT(SUM(Lulocal_tmp*Lulocal_tmp))!TESTDG
    PRINT *,'Wgmres b_loc', MINVAL(b_loc), MAXVAL(b_loc) !, SQRT(SUM(b_loc*b_loc))!TESTDG
    PRINT *, 'Wgmres 1.1: MIN/MAX(psi)=',MINVAL(psi),MAXVAL(psi)
    !PRINT *, 'calling Lulocal_diag() from Wgmres(), meth = ', meth
    !PRINT *, 'MINMAX(b_loc=)', MINVAL(b_loc), MAXVAL(b_loc)
#endif


    res = 0.0_pr

    res = ( b_loc - Lulocal (jlev, psi, nlocal, ne_local, meth) ) / Lulocal_diag_tmp / Lulocal_Wdiag_tmp

	IF (debug_level > 0) PRINT *, 'res:', MINVAL(res), MAXVAL(res), MINVAL(ABS(res)), MAXVAL(ABS(res))


#ifdef DEBUG_GMRES
    !PRINT *, res
    !PAUSE 'in Wgmres'
    !PRINT *, b_loc
    PRINT *,'Wgmres res=', MINVAL(res), MAXVAL(res)!, SQRT(SUM(res*res))!TESTDG
    PRINT *, 'Wgmres 2: MIN/MAX(psi)=',MINVAL(psi),MAXVAL(psi)
    !TESTDG  PRINT *,'TEST L2norm psi(:,ie) (real space, in Wgmres )1 ' , SUM( psi(:)**2 )
    !PRINT *,'Wgmres ', MINVAL(res), MAXVAL(res), SQRT(SUM(res*res))!TESTDG
#endif



    tmp1 = DOT_PRODUCT (res, res)
#ifdef MULTIPROC
    CALL parallel_global_sum( REAL=tmp1 )
#endif
    beta_loc = SQRT ( tmp1 )


#ifdef DEBUG_GMRES
    WRITE(*,'("beta_loc = ", e21.11 )') beta_loc !TESTDG
#endif


    hess = 0.0_pr
    w    = 0.0_pr
    v    = 0.0_pr
    v(:,1) = res / MAX(beta_loc,1.0e-24_pr) 
    j = 1 

    DO WHILE( j <= kryh ) 

       DO ie=1, ne_local
          shift=(ie-1)*nlocal
          !clipping the solution for ie variable
          !Oleg: this feature seem to cause problem with convergence, since it changes the orthogolanilzation
          !IF(clip(ie) == 1) THEN
          !   u_clip = v(shift+iclip,j) 
          !   CALL parallel_broadcast( REAL=u_clip )
          !   v(shift+1:ie*nlocal,j)= v(shift+1:ie*nlocal,j)-u_clip
          !END IF
          IF(clip(ie) == 1 .AND. elliptic_project) THEN
             u_clip = 0.0_pr
             u_clip = SUM(v(shift+1:ie*nlocal,j)*dA_level(1:nlocal,jlev))
             CALL parallel_global_sum( REAL=u_clip )
             u_clip = u_clip/sumdA_global
             v(shift+1:ie*nlocal,j)= v(shift+1:ie*nlocal,j)-u_clip
       END IF
       END DO
       !PRINT *, 'calling Lulocal_diag() 2 from Wgmres(), meth = ', meth


       w = Lulocal(jlev, v(:,j), nlocal, ne_local, meth) / Lulocal_diag_tmp / Lulocal_Wdiag_tmp


#ifdef DEBUG_GMRES
       PRINT *,'    Wgmres w', MINVAL(w), MAXVAL(w), SQRT(SUM(w*w))!TESTDG
       !PRINT *,'TEST Wgmres j, w', j,w
#endif


       DO i = 1, j
          hess (i,j) = DOT_PRODUCT (v(:,i), w)
#ifdef MULTIPROC
          CALL parallel_global_sum( REAL=hess (i,j) )
#endif

#ifdef DEBUG_GMRES
          PRINT *,'    TEST Wgmres i, v(:,i)',i, MINVAL(v(:,i)), MAXVAL(v(:,i))
#endif
          
          w = w - v(:,i) * hess(i,j) 
       END DO
       
       tmp1 = DOT_PRODUCT (w, w)
#ifdef MULTIPROC
       CALL parallel_global_sum( REAL=tmp1 )
#endif
       hess(j+1,j) = SQRT ( tmp1 )

       !OLD IF(hess(j+1,j) > 1.e-12_pr )  THEN
       IF(hess(j+1,j) > tol_gmres * scl_local .AND. .NOT. ( tol_gmres_stop_if_larger .AND. hess(j+1,j) > hess_last)  )  THEN
          v(:,j+1) = w / hess (j+1,j)
       ELSE
          IF( hess(j+1,j) > hess_last ) PRINT *,'Exiting gmres iteration hess(j+1,j) > hess_last'
          v(:,j+1) = 0.0_pr
          kryh=j
       END IF
       hess_last = hess(j+1,j) ! save last error criterion

       IF(diagnostics_elliptic) WRITE (*,'("WGMRES it = ",I4 ," Err = ",  es21.11 )') j, hess(j+1,j)
       j = j+1

    END DO
    e1 = 0.0_pr ; e1(1) = beta_loc
    
    
#ifdef DEBUG_GMRES
    !PRINT *,'Wgmres before DGLES kryh, info', kryh, info
    !PRINT *,'    Wgmres before DGLES hess ', hess
    !PRINT *,'    Wgmres before DGLES e1 ', e1
    !PRINT *,'    Wgmres before DGLES work ', par_rank, MINVAL(work), MAXVAL(work)
#endif

    
    CALL DGELS ('N', kryh+1, kryh, 1, hess, kry+1, e1, kry+1, work, kry+(kry+1)*32, info)

    
#ifdef DEBUG_GMRES
    PRINT *,'    Wgmres after  DGLES e1(1:kryh) ,', e1(1:kryh)
#endif



    IF (info /=0) WRITE (6,'(i3)') info

#ifdef DEBUG_GMRES
    PRINT *, 'Wgmres 3: MIN/MAX(psi)=',MINVAL(psi),MAXVAL(psi)
    PRINT *, 'Wgmres 3: MIN/MAX(v)=',MINVAL(v),MAXVAL(v)
    PRINT *, 'Wgmres 3: MIN/MAX(e1)=',MINVAL(e1),MAXVAL(e1)
#endif

    psi = psi + MATMUL (v(1:nlocal*ne_local,1:kryh), e1(1:kryh))

#ifdef DEBUG_GMRES
    PRINT *, 'Wgmres 4: MIN/MAX(psi)=',MINVAL(psi),MAXVAL(psi)
    PRINT *, 'Wgmres 3: MIN/MAX(v)=',MINVAL(v),MAXVAL(v)
    PRINT *, 'Wgmres 3: MIN/MAX(e1)=',MINVAL(e1),MAXVAL(e1)
#endif

    
    DO ie=1, ne_local
       shift=(ie-1)*nlocal
       !clipping the solution for ie variable
       IF(clip(ie) == 1  .AND. .NOT.elliptic_project) THEN
          u_clip = psi(shift+iclip)
          CALL parallel_broadcast( REAL=u_clip )
          psi(shift+1:ie*nlocal)= psi(shift+1:ie*nlocal)- u_clip
       ELSE IF(clip(ie) == 1 .AND. elliptic_project) THEN
          u_clip = 0.0_pr
          u_clip = SUM(psi(shift+1:ie*nlocal)*dA_level(1:nlocal,jlev))
          CALL parallel_global_sum( REAL=u_clip )
          u_clip = u_clip/sumdA_global
          psi(shift+1:ie*nlocal)= psi(shift+1:ie*nlocal)-u_clip
       END IF
    END DO
#ifdef DEBUG_GMRES
    PRINT *, 'Wgmres 5: MIN/MAX(psi)=',MINVAL(psi),MAXVAL(psi)
#endif

  END SUBROUTINE Wgmres

  SUBROUTINE WGMRES_OLD (jlev, kry, nlocal, ne_local, meth, clip, psi, b_loc, Lulocal, Lulocal_diag, Lulocal_Wdiag)
    !--Solves a linear system of the form Apsi = b, using initial guess psi0 
    USE precision
    USE wlt_vars
    USE wlt_trns_mod
    USE wlt_trns_vars
    USE elliptic_vars
    USE pde
    USE util_vars         ! dA
    USE parallel
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: jlev, kry, nlocal, ne_local, meth
    INTEGER, DIMENSION(ne_local), INTENT(IN):: clip
    INTEGER :: i, j, info, kryh, ntotal, ie, shift
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (IN) :: b_loc
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (INOUT) :: psi
    REAL (pr), DIMENSION (nlocal*ne_local)                 :: dpsi
    REAL (pr), DIMENSION (nlocal*ne_local) :: w, res
    REAL (pr), DIMENSION (nlocal*ne_local, kry+1) :: v 
    REAL (pr), DIMENSION (kry+1, kry) :: hess
    REAL (pr)                         :: hess_last
    REAL (pr), DIMENSION (kry+1) :: e1
    REAL (pr) :: beta_loc, scl_local, u_clip
    REAL (pr), DIMENSION (kry+(kry+1)*32) :: work
    REAL (pr), DIMENSION (nlocal*ne_local) :: Lulocal_diag_tmp ! local result of Lulocal_diag
    REAL (pr), DIMENSION (nlocal*ne_local) :: Lulocal_Wdiag_tmp ! local result of Lulocal_diag
    !  REAL (pr), DIMENSION (nlocal*ne_local) :: Lulocal_tmp ! temp array
    INTEGER :: iclip
    INTEGER :: nlocal_global
    REAL(pr) :: tmp1, tmp2, tmp3

    INTERFACE 
       FUNCTION Lulocal (jlev, u_local, nlocal, ne_local, meth)
         USE precision
         USE sizes
         USE pde
         IMPLICIT NONE
         INTEGER, INTENT (IN) :: jlev, nlocal, ne_local, meth
         REAL (pr), DIMENSION (nlocal*ne_local), INTENT (INOUT) :: u_local
         REAL (pr), DIMENSION (nlocal*ne_local) :: Lulocal
       END FUNCTION Lulocal
       FUNCTION Lulocal_diag (jlev,  nlocal, ne_local, meth)
         USE precision
         USE sizes
         USE pde
         IMPLICIT NONE
         INTEGER, INTENT (IN) :: jlev, nlocal, ne_local, meth
         REAL (pr), DIMENSION (nlocal*ne_local) :: Lulocal_diag
       END FUNCTION Lulocal_diag
       FUNCTION Lulocal_Wdiag (jlev,  nlocal, ne_local, meth)
         USE precision
         USE sizes
         USE pde
         IMPLICIT NONE
         INTEGER, INTENT (IN) :: jlev, nlocal, ne_local, meth
         REAL (pr), DIMENSION (nlocal*ne_local) :: Lulocal_Wdiag
       END FUNCTION Lulocal_Wdiag
    END INTERFACE

    iclip = i_clip(jlev)

    !
    ! Call Lulocal_diag once and store result 
    !
    Lulocal_diag_tmp = Lulocal_diag(jlev, nlocal, ne_local, meth)
    Lulocal_Wdiag_tmp = Lulocal_Wdiag(jlev, nlocal, ne_local, meth)

#ifdef DEBUG_GMRES
    PRINT *, 'WGMRES_OLD 0: MIN/MAX(psi)=',MINVAL(psi),MAXVAL(psi)
    PRINT *,'nlocal=',nlocal,'ne_local=',ne_local,'iclip=',iclip,'clip=',clip
#endif

    DO ie=1, ne_local
       shift=(ie-1)*nlocal
       !clipping the solution for ie variable
       IF(clip(ie) == 1  .AND. .NOT.elliptic_project) THEN
          u_clip = psi(shift+iclip)
          CALL parallel_broadcast( REAL=u_clip )
          psi(shift+1:ie*nlocal)= psi(shift+1:ie*nlocal)- u_clip
       ELSE IF(clip(ie) == 1 .AND. elliptic_project) THEN
          u_clip = 0.0_pr
          u_clip = SUM(psi(shift+1:ie*nlocal)*dA_level(1:nlocal,jlev))
          CALL parallel_global_sum( REAL=u_clip )
          u_clip = u_clip/sumdA_global
          psi(shift+1:ie*nlocal)= psi(shift+1:ie*nlocal)-u_clip
       END IF
    END DO
    
#ifdef DEBUG_GMRES
    PRINT *, 'WGMRES_OLD 1: MIN/MAX(psi)=',MINVAL(psi),MAXVAL(psi)
#endif

    dpsi = psi
!!    CALL c_wlt_trns (dpsi, ne_local, 1, ne_local, HIGH_ORDER, WLT_TRNS_FWD)
!!    dpsi = dpsi * Lulocal_Wdiag_tmp
    
    nlocal_global = nlocal
    tmp1 = SUM(psi(:)**2)
    tmp2 = SUM(b_loc(:)**2)
    CALL parallel_global_sum( INTEGER=nlocal_global )
    CALL parallel_global_sum( REAL=tmp1 )
    CALL parallel_global_sum( REAL=tmp2 )
    scl_local =  MAX(SQRT(tmp1/REAL(nlocal_global*ne_local,pr)),SQRT(tmp2/REAL(nlocal_global*ne_local,pr)),1.e-12_pr)
    
    ntotal = nlocal*ne_local
    !kryh = MIN(kry,ntotal)
    kryh = kry
    hess_last = HUGE(hess_last) !set hass_last to max possile value
    
    
#ifdef DEBUG_GMRES
    PRINT *, '=========== WGMRES_OLD==============='
    PRINT *, 'scl_local=',scl_local,'kryh=',kryh,'hess_last=',hess_last
#endif


#ifdef DEBUG_GMRES
    ! Lulocal_tmp = Lulocal (jlev, psi, nlocal, ne_local, meth) !TEST
    ! Lulocal_diag_tmp = 1.0_pr
    PRINT *,'WGMRES_OLD Lulocal_diag_tmp', MINVAL(Lulocal_diag_tmp), MAXVAL(Lulocal_diag_tmp)
    !, SQRT(SUM(Lulocal_diag_tmp*Lulocal_diag_tmp))!TESTDG
    !PRINT *,'WGMRES_OLD ', MINVAL(Lulocal_tmp), MAXVAL(Lulocal_tmp), SQRT(SUM(Lulocal_tmp*Lulocal_tmp))!TESTDG
    PRINT *,'WGMRES_OLD b_loc', MINVAL(b_loc), MAXVAL(b_loc) !, SQRT(SUM(b_loc*b_loc))!TESTDG
    PRINT *, 'WGMRES_OLD 1.1: MIN/MAX(psi)=',MINVAL(psi),MAXVAL(psi)
    !PRINT *, 'calling Lulocal_diag() from WGMRES_OLD(), meth = ', meth
    !PRINT *, 'MINMAX(b_loc=)', MINVAL(b_loc), MAXVAL(b_loc)
#endif


    res = 0.0_pr

    res = ( b_loc - Lulocal (jlev, psi, nlocal, ne_local, meth) ) / Lulocal_diag_tmp / Lulocal_Wdiag_tmp

    IF (debug_level > 0) PRINT *, 'res:', MINVAL(res), MAXVAL(res), MINVAL(ABS(res)), MAXVAL(ABS(res))


#ifdef DEBUG_GMRES
    !PRINT *, res
    !PAUSE 'in WGMRES_OLD'
    !PRINT *, b_loc
    PRINT *,'WGMRES_OLD res=', MINVAL(res), MAXVAL(res)!, SQRT(SUM(res*res))!TESTDG
    PRINT *, 'WGMRES_OLD 2: MIN/MAX(psi)=',MINVAL(psi),MAXVAL(psi)
    !TESTDG  PRINT *,'TEST L2norm psi(:,ie) (real space, in WGMRES_OLD )1 ' , SUM( psi(:)**2 )
    !PRINT *,'WGMRES_OLD ', MINVAL(res), MAXVAL(res), SQRT(SUM(res*res))!TESTDG
#endif



    tmp1 = DOT_PRODUCT (res, res)
#ifdef MULTIPROC
    CALL parallel_global_sum( REAL=tmp1 )
#endif
    beta_loc = SQRT ( tmp1 )


#ifdef DEBUG_GMRES
    WRITE(*,'("beta_loc = ", e21.11 )') beta_loc !TESTDG
#endif

    hess = 0.0_pr
    w    = 0.0_pr
    v    = 0.0_pr
    v(:,1) = res / MAX(beta_loc,1.0e-24_pr) 
    j = 1 

    DO WHILE( j <= kryh ) 

       DO ie=1, ne_local
          shift=(ie-1)*nlocal
          !clipping the solution for ie variable
          !Oleg: this feature seem to cause problem with convergence, since it changes the orthogolanilzation
          !IF(clip(ie) == 1) THEN
          !   u_clip = v(shift+iclip,j) 
          !   CALL parallel_broadcast( REAL=u_clip )
          !   v(shift+1:ie*nlocal,j)= v(shift+1:ie*nlocal,j)-u_clip
          !END IF
       END DO
       !PRINT *, 'calling Lulocal_diag() 2 from WGMRES_OLD(), meth = ', meth

	   v(:,j) = v(:,j) / Lulocal_Wdiag_tmp	
!!       CALL c_wlt_trns (v(:,j), ne_local, 1, ne_local, HIGH_ORDER, WLT_TRNS_INV)
       w = Lulocal(jlev, v(:,j), nlocal, ne_local, meth) / Lulocal_diag_tmp / Lulocal_Wdiag_tmp
!!       CALL c_wlt_trns (v(:,j), ne_local, 1, ne_local, HIGH_ORDER, WLT_TRNS_FWD)
	   v(:,j) = v(:,j) * Lulocal_Wdiag_tmp	


#ifdef DEBUG_GMRES
       PRINT *,'    WGMRES_OLD w', MINVAL(w), MAXVAL(w), SQRT(SUM(w*w))!TESTDG
       !PRINT *,'TEST WGMRES_OLD j, w', j,w
#endif


       DO i = 1, j
          hess (i,j) = DOT_PRODUCT (v(:,i), w)
#ifdef MULTIPROC
          CALL parallel_global_sum( REAL=hess (i,j) )
#endif

#ifdef DEBUG_GMRES
          PRINT *,'    TEST WGMRES_OLD i, v(:,i)',i, MINVAL(v(:,i)), MAXVAL(v(:,i))
#endif
          
          w = w - v(:,i) * hess(i,j) 
       END DO
       
       tmp1 = DOT_PRODUCT (w, w)
#ifdef MULTIPROC
       CALL parallel_global_sum( REAL=tmp1 )
#endif
       hess(j+1,j) = SQRT ( tmp1 )

       !OLD IF(hess(j+1,j) > 1.e-12_pr )  THEN
       IF(hess(j+1,j) > tol_GMRES * scl_local .AND. .NOT. ( tol_GMRES_stop_if_larger .AND. hess(j+1,j) > hess_last)  )  THEN
          v(:,j+1) = w / hess (j+1,j)
       ELSE
          IF( hess(j+1,j) > hess_last ) PRINT *,'Exiting WGMRES_OLD iteration hess(j+1,j) > hess_last'
          v(:,j+1) = 0.0_pr
          kryh=j
       END IF
       hess_last = hess(j+1,j) ! save last error criterion

       IF(diagnostics_elliptic) WRITE (*,'("WGMRES_OLD it = ",I4 ," Err = ",  es21.11 )') j, hess(j+1,j)
       j = j+1

    END DO
    e1 = 0.0_pr ; e1(1) = beta_loc
    
    
#ifdef DEBUG_GMRES
    !PRINT *,'WGMRES_OLD before DGLES kryh, info', kryh, info
    !PRINT *,'    WGMRES_OLD before DGLES hess ', hess
    !PRINT *,'    WGMRES_OLD before DGLES e1 ', e1
    !PRINT *,'    WGMRES_OLD before DGLES work ', par_rank, MINVAL(work), MAXVAL(work)
#endif

    
    CALL DGELS ('N', kryh+1, kryh, 1, hess, kry+1, e1, kry+1, work, kry+(kry+1)*32, info)

    
#ifdef DEBUG_GMRES
    PRINT *,'    WGMRES_OLD after  DGLES e1(1:kryh) ,', e1(1:kryh)
#endif



    IF (info /=0) WRITE (6,'(i3)') info

#ifdef DEBUG_GMRES
    PRINT *, 'WGMRES_OLD 3: MIN/MAX(psi)=',MINVAL(psi),MAXVAL(psi)
    PRINT *, 'WGMRES_OLD 3: MIN/MAX(v)=',MINVAL(v),MAXVAL(v)
    PRINT *, 'WGMRES_OLD 3: MIN/MAX(e1)=',MINVAL(e1),MAXVAL(e1)
#endif

    dpsi = dpsi + MATMUL (v(1:nlocal*ne_local,1:kryh), e1(1:kryh))
    psi = psi / Lulocal_Wdiag_tmp
	
!!    CALL c_wlt_trns (psi, ne_local, 1, ne_local, HIGH_ORDER, WLT_TRNS_INV)

#ifdef DEBUG_GMRES
    PRINT *, 'WGMRES_OLD 4: MIN/MAX(psi)=',MINVAL(psi),MAXVAL(psi)
    PRINT *, 'WGMRES_OLD 3: MIN/MAX(v)=',MINVAL(v),MAXVAL(v)
    PRINT *, 'WGMRES_OLD 3: MIN/MAX(e1)=',MINVAL(e1),MAXVAL(e1)
#endif

    
    DO ie=1, ne_local
       shift=(ie-1)*nlocal
       !clipping the solution for ie variable
       IF(clip(ie) == 1  .AND. .NOT.elliptic_project) THEN
          u_clip = psi(shift+iclip)
          CALL parallel_broadcast( REAL=u_clip )
          psi(shift+1:ie*nlocal)= psi(shift+1:ie*nlocal)- u_clip
       ELSE IF(clip(ie) == 1 .AND. elliptic_project) THEN
          u_clip = 0.0_pr
          u_clip = SUM(psi(shift+1:ie*nlocal)*dA_level(1:nlocal,jlev))
          CALL parallel_global_sum( REAL=u_clip )
          u_clip = u_clip/sumdA_global
          psi(shift+1:ie*nlocal)= psi(shift+1:ie*nlocal)-u_clip
       END IF
    END DO
#ifdef DEBUG_GMRES
    PRINT *, 'WGMRES_OLD 5: MIN/MAX(psi)=',MINVAL(psi),MAXVAL(psi)
#endif

END SUBROUTINE WGMRES_OLD

#ifdef DEBUG_GMRES
#undef DEBUG_GMRES
#endif





  !
  !**************************************************************************
  !
  SUBROUTINE Jacoby(u_local, f_local, jlev, nlocal, ne_local, len, meth, clip, Lulocal, Lulocal_diag)
    USE precision
    USE penalization
    USE sizes
    USE elliptic_vars
    USE share_consts
    USE parallel
    USE pde
    USE util_vars
    IMPLICIT NONE
!!$     END IF
    INTEGER, INTENT(IN) :: nlocal, ne_local, jlev, len, meth
    INTEGER, DIMENSION(ne_local), INTENT(IN) :: clip
    REAL (pr), DIMENSION (1:nlocal*ne_local), INTENT (INOUT) :: u_local
    REAL (pr), DIMENSION (1:nlocal*ne_local), INTENT (IN) :: f_local
    REAL (pr), DIMENSION (1:nlocal*ne_local)              :: res
    INTEGER :: i,k,ie,shift, len_h, i_ez, i_ez_h, i_type
    INTEGER :: iclip
    REAL(pr) :: u_clip
    REAL (pr), DIMENSION (nlocal*ne_local) :: Lulocal_diag_tmp
    LOGICAL, PARAMETER :: print_diag_Jacoby = .FALSE.
    LOGICAL :: update_bnd_points
    INTERFACE
!!$     SUBROUTINE wlt_projection(u_local, j_in, j_out, nlocal, type)
!!$       USE precision
!!$       USE sizes
!!$       USE elliptic_vars
!!$       USE share_consts
!!$       IMPLICIT NONE
!!$       INTEGER, INTENT(IN) :: nlocal, j_in, j_out, type
!!$       REAL (pr), DIMENSION (1:nlocal), INTENT (INOUT) :: u_local
!!$     END SUBROUTINE wlt_projection
       FUNCTION Lulocal (jlev, u_local, nlocal, ne_local, meth)
         USE precision
         USE sizes
         USE pde
         IMPLICIT NONE
         INTEGER, INTENT (IN) :: jlev, nlocal, ne_local, meth
         REAL (pr), DIMENSION (nlocal*ne_local), INTENT (INOUT) :: u_local
         REAL (pr), DIMENSION (nlocal*ne_local) :: Lulocal
       END FUNCTION Lulocal
       FUNCTION Lulocal_diag (jlev,  nlocal, ne_local, meth)
         USE precision
         USE sizes
         USE pde
         IMPLICIT NONE
         INTEGER, INTENT (IN) :: jlev, nlocal, ne_local, meth
         REAL (pr), DIMENSION (nlocal*ne_local) :: Lulocal_diag
       END FUNCTION Lulocal_diag
    END INTERFACE

    iclip = i_clip(jlev)

#ifdef DEBUG_JACOBY
    WRITE (*,'( "Entering Jacoby MinMAxVAL u ", 2(es15.7), " " )') MINVAL(u_local) , MAXVAL(u_local)
    WRITE (*,'( "Entering Jacoby MinMAxVAL f ", 2(es15.7), " " )') MINVAL(f_local) , MAXVAL(f_local)
!!$    PRINT *, 'u=', u
#endif

    !
    ! call Lulocal_diag once anc save result
    !
    Lulocal_diag_tmp = 0.0_pr
    Lulocal_diag_tmp = Lulocal_diag (jlev, nlocal, ne_local, meth)
!!$    Lulocal_diag_tmp = 1.0_pr
    DO ie=1, ne_local
       shift=(ie-1)*nlocal
       !clipping the solution for ie variable
       IF(clip(ie) == 1 .AND. .NOT.elliptic_project) THEN
          u_clip = u_local(shift+iclip) 
          CALL parallel_broadcast( REAL=u_clip )
          u_local(shift+1:ie*nlocal)= u_local(shift+1:ie*nlocal)-u_clip
       ELSE IF(clip(ie) == 1 .AND. elliptic_project) THEN
          u_clip = 0.0_pr
          u_clip = SUM(u_local(shift+1:ie*nlocal)*dA_level(1:nlocal,jlev))
          CALL parallel_global_sum( REAL=u_clip )
          u_clip = u_clip/sumdA_global
          u_local(shift+1:ie*nlocal)= u_local(shift+1:ie*nlocal)-u_clip
       END IF
    END DO

    
#ifdef DEBUG_JACOBY
    PRINT *, 'nlocal=',nlocal,'len=',len
    PRINT *, 'u0:', MINVAL(ABS(u_local)), MAXVAL(ABS(u_local))
    PRINT *, 'MINMAX_diag', jlev, nlocal, Nwlt_lev(jlev,1),MINVAL(ABS(Lulocal_diag_tmp)),MAXVAL(ABS(Lulocal_diag_tmp))
    WRITE (*,'( "1 MinMAxVAL u ", 2(es15.7), " " )') MINVAL(u_local) , MAXVAL(u_local)
#endif


    DO k = 1,len

       IF(BC_zone) THEN
!!$          CALL BiCGSTAB (jlev, nlocal, ne_local, len_bicgstab_coarse, meth, clip, tol_gmres, u_local, f_local, Lulocal, Lulocal_diag)
!!$          CALL    GMRES (jlev, len_bicgstab_coarse, nlocal, ne_local, meth, clip, u_local, f_local, Lulocal, Lulocal_diag)

!!$          DO i_ez = 1, thickness_elliptic_zone
!!$             
!!$             res = ( f_local - Lulocal (jlev, u_local, nlocal, ne_local, meth) ) / Lulocal_diag_tmp
!!$             DO ie=1, ne_local
!!$                shift=(ie-1)*nlocal
!!$                DO i_ez_h = 1, i_ez
!!$                   u_local(shift+i_elliptic_zone(1:n_elliptic_zone(i_ez_h,jlev),i_ez_h,jlev))=u_local(shift+i_elliptic_zone(1:n_elliptic_zone(i_ez_h,jlev),i_ez_h,jlev)) + &
!!$                                                                                               w3*res(shift+i_elliptic_zone(1:n_elliptic_zone(i_ez_h,jlev),i_ez_h,jlev))
!!$                END DO
!!$             END DO
!!$
!!$          END DO

       END IF

       !PRINT *, 'calling Lulocal_diag() 2 from Jacoby(), meth = ', meth


       IF(GS_ACTIVE) THEN !GS iterations based on wlt_type family

          DO i_type = 0, 2**dim-1
             
             res = ( f_local - Lulocal (jlev, u_local, nlocal, ne_local, meth) ) / Lulocal_diag_tmp
             DO ie=1, ne_local
                shift=(ie-1)*nlocal
                u_local(shift+i_GS(1:n_GS(i_type,jlev),i_type,jlev))=u_local(shift+i_GS(1:n_GS(i_type,jlev),i_type,jlev)) + &
                                                                           w_GS*res(shift+i_GS(1:n_GS(i_type,jlev),i_type,jlev))
             END DO

          END DO

       ELSE ! regular weighted Jacoby

          res = ( f_local - Lulocal (jlev, u_local, nlocal, ne_local, meth) ) / Lulocal_diag_tmp
          
          
!!$       PRINT *, 'f0:', MINVAL(ABS(f)), MAXVAL(ABS(f_local))
          !PRINT *, 'Lulocal0:', MINVAL(ABS(Lulocal (jlev, u_local, nlocal, ne_local, meth))), MAXVAL(ABS(Lulocal (jlev, u_local, nlocal, ne_local, meth)))
!!$       PRINT *, 'f0:', MINVAL(ABS(Lulocal_diag_tmp)), MAXVAL(ABS(Lulocal_diag_tmp))
!!$       PRINT *, 'res0:', MINVAL(ABS(res)), MAXVAL(ABS(res))
          
          DO ie=1, ne_local
             shift=(ie-1)*nlocal
             ! updating internal points
             u_local(shift+1:shift+Nwlt_lev(jlev,0))=u_local(shift+1:shift+Nwlt_lev(jlev,0)) + w2*res(shift+1:shift+Nwlt_lev(jlev,0))
             IF(imask_obstacle) THEN
                WHERE( penal(1:Nwlt_lev(jlev,0)) == 1.0_pr) 
                   u_local(shift+1:shift+Nwlt_lev(jlev,0))=u_local(shift+1:shift+Nwlt_lev(jlev,0)) &
                        + (w_penal-w2)*res(shift+1:shift+Nwlt_lev(jlev,0))
                END WHERE
             END IF
          END DO

       END IF

!!$       WRITE (*,'( "k=",I3,", MinMAxVAL u ", 2(es15.7), " " )') k,MINVAL(u_local) , MAXVAL(u_local)

#ifdef DEBUG_JACOBY
       PRINT *, 'Nwlt_lev(j_lev,0:1)=',Nwlt_lev(j_lev,0:1)
#endif

       ! updating boundary points
       update_bnd_points = ( Nwlt_lev(j_lev,1) >  Nwlt_lev(j_lev,0) )
#ifdef MULTIPROC
       CALL parallel_global_sum ( LOGICALOR=update_bnd_points )
#endif
       IF( update_bnd_points ) THEN
          DO i = 1, 1
             !PRINT *, 'calling Lulocal_diag() 1 from Jacoby(), meth = ', meth

             !PRINT *, 'f:', MINVAL(ABS(f_local)), MAXVAL(ABS(f_local))
             !PRINT *, jlev, nlocal, ne_local, meth, size(u_local)

             IF(BC_zone) THEN
!!$                CALL BiCGSTAB (jlev, nlocal, ne_local, len_bicgstab_coarse, meth, clip, tol_gmres, u_local, f_local, Lulocal, Lulocal_diag)
!!$                CALL    GMRES (jlev, len_bicgstab_coarse, nlocal, ne_local, meth, clip, u_local, f_local, Lulocal, Lulocal_diag)
!!$                res = ( f_local - Lulocal (jlev, u_local, nlocal, ne_local, meth) ) / Lulocal_diag_tmp
             END IF

             res = ( f_local - Lulocal (jlev, u_local, nlocal, ne_local, meth) ) / Lulocal_diag_tmp
!!$
!!$             PRINT *, 'f1:', MINVAL(ABS(f_local)), MAXVAL(ABS(f_local))
!!$             PRINT *, 'u1:', MINVAL(ABS(u_local)), MAXVAL(ABS(u_local))
!!$             PRINT *, 'Lulocal1:', MINVAL(ABS(Lulocal (jlev, u_local, nlocal, ne_local, meth))), MAXVAL(ABS(Lulocal (jlev, u_local, nlocal, ne_local, meth)))
!!$             PRINT *, 'f1:', MINVAL(ABS(Lulocal_diag_tmp)), MAXVAL(ABS(Lulocal_diag_tmp))
!!$             PRINT *, 'res1:', MINVAL(ABS(res)), MAXVAL(ABS(res))
       
             DO ie=1, ne_local
                shift=(ie-1)*nlocal
                ! updating boundary points
                u_local(shift+Nwlt_lev(jlev,0)+1:ie*nlocal)=u_local(shift+Nwlt_lev(jlev,0)+1:ie*nlocal) + w3*res(shift+Nwlt_lev(jlev,0)+1:ie*nlocal)
             END DO
!!$             PRINT *, 'u:', MINVAL(ABS(u_local)), MAXVAL(ABS(u_local))

             DO ie=1, ne_local
                shift=(ie-1)*nlocal
                !clipping the solution for ie variable
                IF(clip(ie) == 1 .AND. .NOT.elliptic_project) THEN
                   u_clip = u_local(shift+iclip) 
                   CALL parallel_broadcast( REAL=u_clip )
                   u_local(shift+1:ie*nlocal)= u_local(shift+1:ie*nlocal)-u_clip
                ELSE IF(clip(ie) == 1 .AND. elliptic_project) THEN
                   u_clip = 0.0_pr
                   u_clip = SUM(u_local(shift+1:ie*nlocal)*dA_level(1:nlocal,jlev))
                   CALL parallel_global_sum( REAL=u_clip )
                   u_clip = u_clip/sumdA_global
                   u_local(shift+1:ie*nlocal)= u_local(shift+1:ie*nlocal)-u_clip
                END IF
             END DO

#ifdef DEBUG_JACOBY
             IF (par_rank.EQ.0 .AND. print_diag_Jacoby) THEN
                PRINT *, 'k,i=',k,i
                DO ie=1, ne_local
                   shift=(ie-1)*nlocal
                   PRINT *, 'Internal: ie = ',ie
                   WRITE(*,'("  ||res||_inf=",E12.5," ||res||_2=",E12.5)') MAXVAL(ABS(res(shift+1:shift+Nwlt_lev(jlev,0)))),&
                        SQRT(DOT_PRODUCT(res(shift+1:shift+Nwlt_lev(jlev,0)),res(shift+1:shift+Nwlt_lev(jlev,0)))/nlocal)
                   PRINT *, 'Boundary: '
                   WRITE(*,'("  ||res||_inf=",E12.5," ||res||_2=",E12.5)') MAXVAL(ABS(res(shift+Nwlt_lev(jlev,0)+1:ie*nlocal))),&
                        SQRT(DOT_PRODUCT(res(shift+Nwlt_lev(jlev,0)+1:ie*nlocal),res(shift+Nwlt_lev(jlev,0)+1:ie*nlocal))/nlocal)
                   PRINT *,' '
                END DO
             END IF
#endif

          END DO
       END IF

       DO ie=1, ne_local
          shift=(ie-1)*nlocal
          !clipping the solution for ie variable
          IF(clip(ie) == 1 .AND. .NOT.elliptic_project) THEN
             u_clip = u_local(shift+iclip) 
             CALL parallel_broadcast( REAL=u_clip )
             u_local(shift+1:ie*nlocal)= u_local(shift+1:ie*nlocal)-u_clip
          ELSE IF(clip(ie) == 1 .AND. elliptic_project) THEN
             u_clip = 0.0_pr
             u_clip = SUM(u_local(shift+1:ie*nlocal)*dA_level(1:nlocal,jlev))
             CALL parallel_global_sum( REAL=u_clip )
             u_clip = u_clip/sumdA_global
             u_local(shift+1:ie*nlocal)= u_local(shift+1:ie*nlocal)-u_clip
          END IF
       END DO


!!$     IF(.TRUE.) THEN 
!!$        DO ie=1, ne_local
!!$           CALL wlt_projection(u_local((ie-1)*nlocal+1:ie*nlocal), jlev, jlev, nlocal, NORMAL)
!!$       END DO
!!$     END IF

    END DO

#ifdef DEBUG_JACOBY
    WRITE (*,'( "LEAVING Jacoby MinMAxVAL u ", 2(es15.7), " " )') MINVAL(u_local) , MAXVAL(u_local)
    WRITE (*,'( "LEAVING Jacoby MinMAxVAL f ", 2(es15.7), " " )') MINVAL(f_local) , MAXVAL(f_local)
!!$    PRINT *, 'U=', u
#undef DEBUG_JACOBY
#endif
    
  END SUBROUTINE Jacoby

SUBROUTINE setup_elliptic_zone(nlocal)
  USE elliptic_vars
  USE wlt_trns_vars
  IMPLICIT NONE
  INTEGER, INTENT(IN) :: nlocal
  INTEGER*8, DIMENSION(0:dim) :: i_p
  INTEGER :: i, ii, j, jj, iloc, wlt_type, face_type, j_df, i_ez, idim
  INTEGER, DIMENSION(dim) :: ixyz


  thickness_elliptic_zone = MAXVAL(elliptic_zone)
  IF(ALLOCATED(i_elliptic_zone)) DEALLOCATE(i_elliptic_zone)
  IF(ALLOCATED(n_elliptic_zone)) DEALLOCATE(n_elliptic_zone)
  ALLOCATE(i_elliptic_zone(nlocal,thickness_elliptic_zone,j_lev),n_elliptic_zone(thickness_elliptic_zone,j_lev))
  IF(ALLOCATED(i_gmres_zone)) DEALLOCATE(i_gmres_zone)
  IF(ALLOCATED(n_gmres_zone)) DEALLOCATE(n_gmres_zone)
  ALLOCATE(i_gmres_zone(nlocal,j_lev),n_gmres_zone(j_lev))

  !--Boundary points
  i_p(0) = 1
  DO jj=1,dim
     i_p(jj) = i_p(jj-1)*(mxyz(jj)*2**(j_mx-1) + 1)
  END DO
  DO j = 1, j_lev
     DO i_ez = 1, thickness_elliptic_zone
        n_elliptic_zone(i_ez,j) = 0
        DO jj = 1, j
           DO wlt_type = MIN(jj-1,1),2**dim-1
              DO face_type = 0, 3**dim - 1
                 DO j_df = jj, j_lev
                    DO ii = 1, indx_DB(j_df,wlt_type,face_type,jj)%length  
                       i = indx_DB(j_df,wlt_type,face_type,jj)%p(ii)%i
                       iloc = indx_DB(j_df,wlt_type,face_type,jj)%p(ii)%i + indx_DB(j,wlt_type,face_type,jj)%shift
                       ixyz(1:dim) = INT(MOD(indx_DB(j_df,wlt_type,face_type,jj)%p(ii)%ixyz-1,i_p(1:dim))/i_p(0:dim-1))
!!$                       IF(ALL(ixyz(1:dim) > MIN(elliptic_zone(1,1:dim),i_ez)-1) .AND. ALL(ixyz(1:dim) < nxyz(1:dim) - prd(1:dim) - MIN(elliptic_zone(2,1:dim),i_ez)-1)) THEN
                       DO idim = 1, dim
                          IF(  (ixyz(idim) == 2**(j_lev-j)*(i_ez -1) .AND. i_ez <= elliptic_zone(1,idim)) .OR. &
                               (ixyz(idim) == nxyz(idim) - prd(idim) - 2**(j_lev-j)*(i_ez - 1) .AND. i_ez <= elliptic_zone(2,idim)) ) THEN
                             n_elliptic_zone(i_ez,j) = n_elliptic_zone(i_ez,j)+1
                             i_elliptic_zone(n_elliptic_zone(i_ez,j),i_ez,j) = iloc
                          END IF
                       END DO
!!$                       END IF
                    END DO
                 END DO
              END DO
           END DO
        END DO
     END DO
  END DO

  DO j = 1, j_lev
     n_gmres_zone(j) = 0
     DO i_ez = 1, thickness_elliptic_zone
        i_gmres_zone(n_gmres_zone(j)+1:n_gmres_zone(j)+n_elliptic_zone(i_ez,j),j) =  i_elliptic_zone(1:n_elliptic_zone(i_ez,j),i_ez,j)
        n_gmres_zone(j) = n_gmres_zone(j) + n_elliptic_zone(i_ez,j)
     END DO
  END DO

END SUBROUTINE setup_elliptic_zone

SUBROUTINE setup_GS(nlocal)
  USE elliptic_vars
  USE wlt_trns_vars
  IMPLICIT NONE
  INTEGER, INTENT(IN) :: nlocal
  INTEGER*8, DIMENSION(0:dim) :: i_p
  INTEGER :: i, ii, j, jj, iloc, wlt_type, face_type, j_df, idim, i_type
  INTEGER, DIMENSION(dim) :: ixyz


  IF(ALLOCATED(i_GS)) DEALLOCATE(i_GS)
  IF(ALLOCATED(n_GS)) DEALLOCATE(n_GS)
  ALLOCATE(i_GS(nlocal,0:2**dim-1,j_lev),n_GS(0:2**dim-1,j_lev))

  !--GS points based on wavelet families for a given level of resolution
  i_p(0) = 1
  DO jj=1,dim
     i_p(jj) = i_p(jj-1)*(mxyz(jj)*2**(j_mx-1) + 1)
  END DO
  DO j = 1, j_lev
     DO i_type = 0, 2**dim-1
        n_GS(i_type,j) = 0
        DO jj = 1, j
           DO wlt_type = MIN(jj-1,1),2**dim-1
              DO face_type = 0, 3**dim - 1
                 DO j_df = jj, j_lev
                    DO ii = 1, indx_DB(j_df,wlt_type,face_type,jj)%length  
                       i = indx_DB(j_df,wlt_type,face_type,jj)%p(ii)%i
                       iloc = indx_DB(j_df,wlt_type,face_type,jj)%p(ii)%i + indx_DB(j,wlt_type,face_type,jj)%shift
                       ixyz(1:dim) = INT(MOD(indx_DB(j_df,wlt_type,face_type,jj)%p(ii)%ixyz-1,i_p(1:dim))/i_p(0:dim-1))
                       !internal to BC_Zone points only
                       IF(ALL(ixyz(1:dim) > 2**(j_lev-j)*(elliptic_zone(1,1:dim)-1)) .AND. ALL(ixyz(1:dim) < nxyz(1:dim) - prd(1:dim) - 2**(j_lev-j)*(elliptic_zone(2,1:dim)-1))) THEN 
                          IF( (jj == j .AND. wlt_type == i_type) .OR. (jj < j .AND. i_type == 0) ) THEN
                             n_GS(i_type,j) = n_GS(i_type,j)+1
                             i_GS(n_GS(i_type,j),i_type,j) = iloc
                          END IF
                       END IF
                    END DO
                 END DO
              END DO
           END DO
        END DO
     END DO
  END DO
  
END SUBROUTINE setup_GS

  !--********************************
  !--End subroutines for Lu(u)=f solver
  !--********************************

END MODULE elliptic_mod
