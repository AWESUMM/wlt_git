!
! SGS models for compressible flows
!

MODULE SGS_compressible_keqn

  USE precision
  USE wlt_vars
  USE wavelet_filters_mod
  USE wlt_trns_mod
  USE wlt_trns_vars
  USE wlt_trns_util_mod
  USE io_3d_vars
  USE util_mod
  USE util_vars
  USE share_consts
  USE pde
  USE sizes
  USE share_kry
  USE vector_util_mod
  USE input_file_reader
  USE equations_compressible 
  USE sgs_util 
  USE error_handling

  PUBLIC :: &
    keqn_compress_read_input, &
    keqn_compress_setup_pde, &              ! Set up the variable counts
    keqn_compress_finalize_setup_pde, &     ! Set up the pde variable mappings (adapt, interpolate, save, etc.)
    keqn_compress_model_setup, &
    keqn_compress_pre_process, &
    keqn_compress_rhs, &
    keqn_compress_drhs, &
    keqn_compress_drhs_diag, &
    keqn_compress_additional_vars, &
    keqn_compress_cal_cfl, &
    calc_keqn_eddy_visc, &
    initialize_ksgs

  PRIVATE :: calcStrainRateTensors, calc_u_tau, calc_sgs_production, &
      calc_keqn_momentum_closure, calc_keqn_thermal_closure, &
      calc_keqn_momentum_closure_OPT, &
      calc_keqn_triple_corr_closure, calc_keqn_dissipation_closure, &
      calc_keqn_p_dilat, symm_tensor_contract3D   

  REAL (pr), PRIVATE :: ksgs_compress_scale, background_ksgs, init_ksts_scale

  ! Constant Smagorinsky Coefficients
  REAL (pr), PRIVATE :: &
    Cs_in           = 0.0_pr, &
    Pra_t_in        = 0.0_pr, &
    Sc_t_in         = 0.0_pr, & 
    C_f_in          = 0.0_pr, &
    C_diss_sol_in   = 0.0_pr, &
    C_diss_dilat_in = 0.0_pr, &
    C_pi_in         = 0.0_pr

  ! Ksgs-based model
  REAL (pr), DIMENSION(:), ALLOCATABLE, PRIVATE :: Cs, Pra_t, C_f, C_diss_sol, C_diss_dilat, C_pi

  REAL (pr), DIMENSION(1:2), PRIVATE :: &
    Cs_limit           = (/0.0_pr,0.0_pr/), &
    Pra_t_limit        = (/0.0_pr,0.0_pr/), &
    Sc_t_limit         = (/0.0_pr,0.0_pr/), & 
    C_f_limit          = (/0.0_pr,0.0_pr/), &
    C_diss_sol_limit   = (/0.0_pr,0.0_pr/), &
    C_diss_dilat_limit = (/0.0_pr,0.0_pr/), &
    C_pi_limit        = (/0.0_pr,0.0_pr/)

  INTEGER, PARAMETER, PRIVATE :: &
    coeff_alt       =-1, &  ! Alternate formulation 
    coeff_off       = 0, &  ! Coefficient is off
    coeff_const     = 1, &  ! Coefficient is constant (fixed)
    coeff_Ger_clip  = 2, &  ! Coefficient is dynamic (Germano), but clipped
    coeff_Ger_dyn   = 3, &  ! Coefficient is dynamic (Germano), unclipped
    coeff_Bar_clip  = 4, &  ! Coefficient is dynamic (Bardina), but clipped
    coeff_Bar_dyn   = 5     ! Coefficient is dynamic (Bardina), unclipped

  INTEGER, PRIVATE :: &
    Cs_type           = 0, &
    Pra_t_type        = 0, & 
    q_coeff_type      = 1, & 
    Sc_t_type         = 0, & 
    C_f_type          = 0, & 
    C_diss_sol_type   = 0, & 
    C_diss_dilat_type = 0, & 
    C_pi_type        = 0

  LOGICAL, PRIVATE :: save_dyn_coeffs = .FALSE. 
  LOGICAL, PRIVATE :: filt_C = .FALSE., preclip_K = .FALSE.

  INTEGER, PRIVATE :: n_var_cs,n_var_pra_t,n_var_c_f, n_var_c_diss_sol, n_var_c_diss_dilat,n_var_c_pi     
  
CONTAINS

  !
  ! Read input from "case_name"_pde.inp file
  ! case_name is in string file_name read from command line
  ! in read_command_line_input()
  ! 
  SUBROUTINE keqn_compress_read_input()
    IMPLICIT NONE

    ! ERIC: make sure these scales are added
    call input_logical ('adaptk', adaptk,'stop','adaptk: T to adapt on compressible rho*ksgs')
    call input_real ('ksgs_compress_scale',ksgs_compress_scale,'stop','ksgs_compress_scale: Scale coefficient if ksgs is to be adapted on')

    call input_real ('init_ksts_scale',init_ksts_scale,'stop','init_ksts_scale: Scaling for ksts = krts+background_ksgs to initialize Ksgs') 
    call input_real ('background_ksgs',background_ksgs,'stop','background_ksgs: backround value used with krts to initialize field') 

    ! Coefficient types
    call input_integer ('Cs_type',Cs_type,'stop', &
      ' Cs_type ! 0 - 3, Type (off, constant, dynamic/clipped, dynamic) of coefficient')
    IF(Cs_type .LT. 0 .OR. Cs_type .GT. 3) CALL error('Invalid selection for Cs_type')

    call input_integer ('Pra_t_type',Pra_t_type,'stop', &
      ' Pra_t_type ! 0 - 3, Type (off, constant, dynamic/clipped, dynamic) of coefficient')
    IF(Pra_t_type.LT. 0 .OR. Pra_t_type .GT. 3) CALL error('Invalid selection for Pra_t_type')

    call input_integer ('q_coeff_type',q_coeff_type,'stop', &
      ' q_coeff_type ! 1 - 3, coefficient for turbulent heat flux model (Pra_t,1/Pra_t, kappa_s)')
    IF(q_coeff_type .LT. 1 .OR. q_coeff_type.GT. 3) CALL error('Invalid selection for q_coeff_type')

    call input_integer ('Sc_t_type',Sc_t_type,'stop', &
      ' Sc_t_type ! 0 - 3, Type (off, constant, dynamic/clipped, dynamic) of coefficient')
    IF( Sc_t_type.LT. 0 .OR. Sc_t_type .GT. 1) CALL error('Invalid selection for Sc_t_type (0-off or 1-const)')

    call input_integer ('C_f_type',C_f_type,'stop', &
      ' C_f_type ! 0 - 5, Type (off, constant, dynamic/clipped, dynamic) of coefficient, 10 for DSM')
    IF(C_f_type.LT. -1 .OR. C_f_type .GT. 5 ) CALL error('Invalid selection for C_f_type')

    call input_integer ('C_diss_sol_type',C_diss_sol_type,'stop', &
      ' C_diss_sol_type ! 0 - 5, Type (off, constant, dynamic/clipped, dynamic) of coefficient')
    IF(C_diss_sol_type .LT. 0 .OR. C_diss_sol_type .GT. 5) CALL error('Invalid selection for C_diss_sol_type')

    call input_integer ('C_diss_dilat_type',C_diss_dilat_type,'stop', &
      ' C_diss_dilat_type ! 0 - 5, Type (off, constant, dynamic/clipped, dynamic) of coefficient')
    IF(C_diss_dilat_type.LT. 0 .OR. C_diss_dilat_type .GT. 5) CALL error('Invalid selection for C_diss_dilat_type')

    call input_integer ('C_pi_type',C_pi_type,'stop', &
      ' C_pi_type ! 0 - 3, Type (off, constant, dynamic/clipped, dynamic) of coefficient, 10 for YBIII')
    IF(C_pi_type .LT. 0 .OR. C_pi_type .GT. 1) CALL error('Invalid selection for c_pi_type')

    ! Fixed coefficients
    call input_real ('Cs_in',Cs_in,'stop', &
      ' Cs_in ! Eddy viscosity coefficient (for set coefficient)')

    call input_real ('Pra_t_in',Pra_t_in,'stop', &
      ' Pra_t_in ! Turbulent Prandtl number (for set coefficient)')

    call input_real ('Sc_t_in',Sc_t_in,'stop', &
      ' Sc_t_in ! Turbulent Schmidt number (for set coefficient)')

    call input_real ('C_f_in',C_f_in,'stop', &
      ' C_f_in ! SGS transport coefficient (for set coefficient)')

    call input_real ('C_diss_sol_in',C_diss_sol_in,'stop', &
      ' C_diss_sol_in ! SGS dissipation coefficient (for set coefficient)')

    call input_real ('C_diss_dilat_in',C_diss_dilat_in,'stop', &
      ' C_diss_dilat_in ! SGS dilatational dissipation coefficient (for set coefficient)')

    call input_real ('C_pi_in',C_pi_in,'stop', &
      ' C_pi_in ! SGS pressure dilatation coefficient (for set coefficient)')
          
    ! Limits on dynamic coefficients
    call input_real_vector ('Cs_limit',           Cs_limit(1:2),2,'stop',           ' Cs_limit: limits on the dynamic coefficient Cs')  
    call input_real_vector ('Pra_t_limit',        Pra_t_limit(1:2),2,'stop',        ' Pra_t_limit: limits on the dynamic coefficient Pra_t')  
    call input_real_vector ('Sc_t_limit',         Sc_t_limit(1:2),2,'stop',         ' Sc_t_limit: limits on the dynamic coefficient Sc_t')  
    call input_real_vector ('C_f_limit',          C_f_limit(1:2),2,'stop',          ' C_f_limit: limits on the dynamic coefficient C_f')  
    call input_real_vector ('C_diss_sol_limit',   C_diss_sol_limit(1:2),2,'stop',   ' C_diss_sol_limit: limits on the dynamic coefficient C_diss_sol')  
    call input_real_vector ('C_diss_dilat_limit', C_diss_dilat_limit(1:2),2,'stop', ' C_diss_dilat_limit: limits on the dynamic coefficient C_diss_dilat')  
    call input_real_vector ('C_pi_limit',         C_pi_limit(1:2),2,'stop',         ' C_pi_limit: limits on the dynamic coefficient C_pi')  

    IF ( Cs_limit(1) .GT. Cs_limit(2) ) CALL error('Invalid limits on Cs')
    IF ( Pra_t_limit(1) .GT. Pra_t_limit(2) ) CALL error('Invalid limits on Pra_t')
    IF ( Sc_t_limit(1) .GT. Sc_t_limit(2) ) CALL error('Invalid limits on Sc_t')
    IF ( C_f_limit(1) .GT. C_f_limit(2) )  CALL error('Invalid limits on C_f')
    IF ( C_diss_sol_limit(1) .GT. C_diss_sol_limit(2) ) CALL error('Invalid limits on C_diss_sol')
    IF ( C_diss_dilat_limit(1) .GT. C_diss_dilat_limit(2) ) CALL error('Invalid limits on C_diss_dilat')
    IF ( C_pi_limit(1) .GT. C_pi_limit(2) ) CALL error('Invalid limits on C_pi')


    call input_logical ('filt_C', filt_C, 'stop', &
      ' filt_C ! use lowpass filtering for pseudo-volume averaging of coefficients')

    call input_logical ('preclip_K', preclip_K, 'stop', &
      ' preclip_K ! clip Ksgs to positive values in pre-processing')

    call input_logical ('save_dyn_coeffs', save_dyn_coeffs, 'stop', &
      ' save_dyn_coeffs: save any dynamic coefficients')

    call input_logical ('deltaMij',deltaMij,'stop', &
      '  deltaMij, Mij definition including delta^2')

    call input_real ('Mcoeff', Mcoeff, 'stop', &
        ' Mcoeff ! Mij = Mcoeff * |S>2eps| Sij>2eps - (|S|Sij )>2eps')


  END SUBROUTINE keqn_compress_read_input

   !  Setup the compressible pde equations, registering all of the variables
   SUBROUTINE  keqn_compress_setup_pde () 
      USE variable_mapping
      IMPLICIT NONE
      CHARACTER (LEN=u_variable_name_len) :: tmp_name 
      
      IF (par_rank.EQ.0) THEN
        PRINT * ,''
         PRINT *, '******************* Setting up PDE ******************'
         PRINT * ,'            USING COMPRESSIBLE K-EQN LES MODEL'
         PRINT *, '*****************************************************'
      END IF

      CALL register_var( 'Rho_Ksgs',  integrated=.TRUE.,  adapt=(/adaptk, adaptk/),   interpolate=(/.TRUE.,.TRUE./), &
          exact=(/.FALSE.,.FALSE./),   saved=.TRUE., req_restart=.TRUE. )

      ! Save any dynamic coefficients as a diagnostic
      IF( save_dyn_coeffs ) THEN
        
        IF (Cs_type .EQ. coeff_Ger_clip .OR. Cs_type .EQ. coeff_Ger_dyn &
          .OR. Cs_type .EQ. coeff_Bar_clip .OR. Cs_type .EQ. coeff_Bar_dyn ) THEN
          CALL register_var( 'Cs',  integrated=.FALSE.,  adapt=(/.FALSE., .FALSE./),   interpolate=(/.FALSE.,.FALSE./), &
            exact=(/.FALSE.,.FALSE./),   saved=.TRUE., req_restart=.FALSE. )
        END IF
        
        IF (Pra_t_type .EQ. coeff_Ger_clip .OR. Pra_t_type .EQ. coeff_Ger_dyn &
          .OR. Pra_t_type .EQ. coeff_Bar_clip .OR. Pra_t_type .EQ. coeff_Bar_dyn ) THEN
          IF ( q_coeff_type .EQ. 1 ) THEN
            WRITE (tmp_name, u_variable_names_fmt) 'Pra_t' 
          ELSE IF ( q_coeff_type .EQ. 2 ) THEN
            WRITE (tmp_name, u_variable_names_fmt) 'inv_Pra_t' 
          ELSE IF ( q_coeff_type .EQ. 3 ) THEN
            WRITE (tmp_name, u_variable_names_fmt) 'C_kappa' 
          END IF
          CALL register_var( tmp_name,  integrated=.FALSE.,  adapt=(/.FALSE., .FALSE./),   interpolate=(/.FALSE.,.FALSE./), &
            exact=(/.FALSE.,.FALSE./),   saved=.TRUE., req_restart=.FALSE. )
        END IF


        IF (Sc_t_type .EQ. coeff_Ger_clip .OR. Sc_t_type .EQ. coeff_Ger_dyn &
          .OR. Sc_t_type .EQ. coeff_Bar_clip .OR. Sc_t_type .EQ. coeff_Bar_dyn ) THEN
          CALL register_var( 'Sc_t',  integrated=.FALSE.,  adapt=(/.FALSE., .FALSE./),   interpolate=(/.FALSE.,.FALSE./), &
            exact=(/.FALSE.,.FALSE./),   saved=.TRUE., req_restart=.FALSE. )
        END IF


        IF (C_f_type .EQ. coeff_Ger_clip .OR. C_f_type .EQ. coeff_Ger_dyn &
          .OR. C_f_type .EQ. coeff_Bar_clip .OR. C_f_type .EQ. coeff_Bar_dyn ) THEN
          CALL register_var( 'C_f',  integrated=.FALSE.,  adapt=(/.FALSE., .FALSE./),   interpolate=(/.FALSE.,.FALSE./), &
            exact=(/.FALSE.,.FALSE./),   saved=.TRUE., req_restart=.FALSE. )
        END IF


        IF (C_diss_sol_type .EQ. coeff_Ger_clip .OR. C_diss_sol_type .EQ. coeff_Ger_dyn &
          .OR. C_diss_sol_type .EQ. coeff_Bar_clip .OR. C_diss_sol_type .EQ. coeff_Bar_dyn ) THEN
          CALL register_var( 'C_diss_sol',  integrated=.FALSE.,  adapt=(/.FALSE., .FALSE./),   interpolate=(/.FALSE.,.FALSE./), &
            exact=(/.FALSE.,.FALSE./),   saved=.TRUE., req_restart=.FALSE. )
        END IF

        IF (C_diss_dilat_type .EQ. coeff_Ger_clip .OR. C_diss_dilat_type .EQ. coeff_Ger_dyn &
          .OR. C_diss_dilat_type .EQ. coeff_Bar_clip .OR. C_diss_dilat_type .EQ. coeff_Bar_dyn ) THEN
          CALL register_var( 'C_diss_dilat',  integrated=.FALSE.,  adapt=(/.FALSE., .FALSE./),   interpolate=(/.FALSE.,.FALSE./), &
            exact=(/.FALSE.,.FALSE./),   saved=.TRUE., req_restart=.FALSE. )
        END IF


        IF (C_pi_type .EQ. coeff_Ger_clip .OR. C_pi_type .EQ. coeff_Ger_dyn &
          .OR. C_pi_type .EQ. coeff_Bar_clip .OR. C_pi_type .EQ. coeff_Bar_dyn ) THEN
          CALL register_var( 'C_pi',  integrated=.FALSE.,  adapt=(/.FALSE., .FALSE./),   interpolate=(/.FALSE.,.FALSE./), &
            exact=(/.FALSE.,.FALSE./),   saved=.TRUE., req_restart=.FALSE. )
        END IF

      END IF

   END SUBROUTINE  keqn_compress_setup_pde



   !  Setup the compressible pde equations: set mappings (indices, adapt, interpolate, save)
   !    start_integrated: indice in u-array to start the contiguous block of integrated compressible equations 
   !    start_additional: indice in u-array to start the contiguous block of additional compressible variables 
   SUBROUTINE  keqn_compress_finalize_setup_pde()
      USE variable_mapping
      IMPLICIT NONE
      CHARACTER (LEN=u_variable_name_len) :: tmp_name 

      n_var_K = get_index('Rho_Ksgs')
  
      IF( adaptk ) scaleCoeff( n_var_k ) = ksgs_compress_scale
 
      n_var_cs            = get_index( 'Cs' )
      IF ( q_coeff_type .EQ. 1 ) THEN
        WRITE (tmp_name, u_variable_names_fmt) 'Pra_t' 
      ELSE IF ( q_coeff_type .EQ. 2 ) THEN
        WRITE (tmp_name, u_variable_names_fmt) 'inv_Pra_t' 
      ELSE IF ( q_coeff_type .EQ. 3 ) THEN
        WRITE (tmp_name, u_variable_names_fmt) 'C_kappa' 
      END IF
      n_var_pra_t         = get_index( tmp_name )
      n_var_c_f           = get_index( 'C_f' )
      n_var_c_diss_sol    = get_index( 'C_diss_sol' )
      n_var_c_diss_dilat  = get_index( 'C_diss_dilat' )
      n_var_c_pi          = get_index( 'C_pi' )

   END SUBROUTINE  keqn_compress_finalize_setup_pde

    !
    ! Intialize sgs model
    ! This routine is called once in the first
    ! iteration of the main time integration loop.
    ! weights and model filters have been setup for first loop when this routine is called.
    !
  SUBROUTINE keqn_compress_model_setup( nlocal )
    USE wlt_trns_vars      ! indx_DB 
    USE wlt_trns_mod       ! delta_from_mask
    USE curvilinear
    USE parallel
    USE equations_compressible_vars
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: nlocal
    
    INTEGER, SAVE        :: nlocal_last = 0
    INTEGER              :: i, k, ii, j, j_df, wlt_type, face_type, idim
    REAL (pr)            :: tmp1, tmp2

    ! Set up Ksgs model: ERIC: some of these might not be needed since we have turb dissipation
    IF ( nlocal /= nlocal_last) THEN
      IF( ALLOCATED(Cs) )             DEALLOCATE( Cs )
                                      ALLOCATE(   Cs(1:nlocal) )

      IF( ALLOCATED(Pra_t) )          DEALLOCATE( Pra_t )
                                      ALLOCATE(   Pra_t(1:nlocal) )

      IF( ALLOCATED(C_f) )            DEALLOCATE( C_f )
                                      ALLOCATE(   C_f(1:nlocal) )

      IF( ALLOCATED(C_diss_sol) )     DEALLOCATE( C_diss_sol )
                                      ALLOCATE(   C_diss_sol(1:nlocal) )

      IF( ALLOCATED(C_diss_dilat) )   DEALLOCATE( C_diss_dilat )
                                      ALLOCATE(   C_diss_dilat(1:nlocal) )

      IF( ALLOCATED(C_pi) )           DEALLOCATE( C_pi )
                                      ALLOCATE(   C_pi(1:nlocal) )

    END IF

    nlocal_last = nlocal

  END SUBROUTINE  keqn_compress_model_setup

  SUBROUTINE keqn_compress_pre_process( u_int, nlocal )
    USE variable_mapping 
    USE equations_compressible_vars
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: nlocal
    REAL (pr), DIMENSION( nlocal,n_integrated ), INTENT(INOUT) :: u_int

    ! Clip ksgs to positive, realizable values
    IF(preclip_K)u_int(:,n_var_k) = MAX(0.0_pr,u_int(:,n_var_k)) 

  END SUBROUTINE keqn_compress_pre_process

  SUBROUTINE keqn_compress_rhs( keqn_rhs,u_int, meth )
    USE error_handling 
    USE variable_mapping
    IMPLICIT NONE
    REAL (pr), DIMENSION (nwlt,n_integrated), INTENT(IN) :: u_int ! integrated variables
    REAL (pr), DIMENSION (nwlt*n_integrated), INTENT(INOUT) :: keqn_rhs
    INTEGER, INTENT(IN) :: meth 

    REAL (pr), DIMENSION (nwlt,dim+4)         ::  v          !velocity + Temperature + Pressure
    REAL (pr), DIMENSION (dim+4,nwlt,dim)       :: du, d2u
    REAL (pr), DIMENSION (1:nwlt,1:6)         :: tau_ij, s_ij, s_ij_traceless, tau_over_rho
    REAL (pr), DIMENSION (1:nwlt,1:dim)       :: div_tau
    REAL (pr), DIMENSION (1:nwlt,dim)         :: u_tau, q_k, f_correlation
    REAL (pr), DIMENSION (1:nwlt)             :: diss_sol, diss_dilat, sgs_production, p_dilatation
    REAL (pr), DIMENSION (1:nwlt)             :: R_field, cp_field 
    REAL (pr), DIMENSION (1:nwlt)             :: dyn_visc_loc 

    REAL (pr), DIMENSION (nwlt,dim*2)           :: F  !fluxes: energy and k-eqn
    REAL (pr), DIMENSION (dim*2,nwlt,dim)       :: dF, d2F
    REAL (pr), DIMENSION (6,nwlt,dim)           :: dtau, d2tau

    INTEGER :: i, j, l, jshift, shift

   !Solanoidal dissipation
    diss_sol = C_diss_sol * MAX(0.0_pr,u_int(:,n_var_k))**1.5_pr / SQRT(u_int(:,n_den))
    IF(deltaMij) diss_sol = diss_sol/delta(:)

    !Dilatational dissipation  ! ERIC: using the local sgs turbulent mach number instead of avg (does avg have meaning?) 
    diss_dilat = C_diss_dilat*2.0_pr/( calc_speed_of_sound( u_int, nwlt, n_integrated )**2 )*MAX(0.0_pr,u_int(:,n_var_k))**2.5_pr/u_int(:,n_den)**1.5_pr
    IF(deltaMij) diss_dilat = diss_dilat/delta(:)

    !CALL print_extrema( diss_sol, nwlt, 'LDKM RHS: diss_sol (should probably be positive for stability)')
    !CALL print_extrema( diss_dilat, nwlt, 'LDKM RHS: diss_dilat (should probably be positive for stability)')

    !calculate native quantities  ! ERIC: put in helper functions 
    DO i = 1,dim
       v(:,i) = u_int(:,n_mom(i))/u_int(:,n_den)  !compute velocity
    END DO

    ! ERIC: wrapper function
    cp_field(:) = cp_in(Nspec)
    R_field(:) = R_in(Nspec)
    IF (Nspec>1) cp_field(:) = (1.0_pr-SUM(u_int(:,n_spc(1):n_spc(Nspecm)),DIM=2)/u_int(:,n_den))*cp_in(Nspec) !cp_Nspec
    IF (Nspec>1) cp_field(:) = (1.0_pr-SUM(u_int(:,n_spc(1):n_spc(Nspecm)),DIM=2)/u_int(:,n_den))*R_in(Nspec) !R_Nspec
    DO l=1,Nspecm
       cp_field(:) = cp_field(:) + u_int(:,n_spc(l))/u_int(:,n_den)*cp_in(l) !cp
       R_field(:) = R_field(:) + u_int(:,n_spc(l))/u_int(:,n_den)*R_in(l) !R
    END DO
    v(:,2+dim) = (u_int(:,n_eng)-0.5_pr*SUM(u_int(:,n_mom(1):n_mom(dim))**2,DIM=2)/u_int(:,n_den))/(cp_field(:)/R_field(:)-1.0_pr) / F_compress  !pressure
    v(:,1+dim) = v(:,2+dim)/( R_field(:)*u_int(:,n_den) ) !Temperature
    v(:,3+dim) = MAX(0.0_pr,u_int(:,n_var_k))/u_int(:,n_den)    !ksgs
    v(:,4+dim) = MAX(0.0_pr,u_int(:,n_var_k))                   ! rhp * ksgs, isotropic part of tau needed for momentum

    CALL c_diff_fast(v(:,1:dim+4), du(1:dim+4,:,:), d2u(1:dim+4,:,:), j_lev, nwlt, meth, 10, dim+4, 1, dim+4)


    !Calculate S_ij and S_ij-traceless from the velocity derivatives
    !******************************
    CALL calcStrainRateTensors( du(1:dim, :, 1:dim), nwlt, s_ij, TRACELESS = S_ij_traceless )

    ! deltaMij already accounted for in nu_t()
    DO i = 1,6
       tau_ij(:,i) = -2.0_pr*mu_turb(:)*s_ij_traceless(:,i)
    END DO
    DO i = 1,3 !Isotropic part
       tau_ij(:,i) = tau_ij(:,i) + 2.0_pr/3.0_pr*MAX(0.0_pr,u_int(:,n_var_k))
    END DO

    u_tau(:,:) = calc_u_tau( v(:,1:dim), tau_ij, nwlt)

    DO i = 1,6
       tau_over_rho(:,i) = tau_ij(:,i)/u_int(:,n_den)
    END DO

    ! ERIC: check all derivative calls to ensure nothing is improperly
    ! overwritten
    CALL c_diff_fast(tau_over_rho(:,1:6), dtau(1:6,:,:), d2tau(1:6,:,:), j_lev, nwlt, meth, 10, 6, 1, 6)
    div_tau(:,1) = dtau(1,:,1)+dtau(4,:,2)+dtau(5,:,3)
    div_tau(:,2) = dtau(4,:,1)+dtau(2,:,2)+dtau(6,:,3)
    div_tau(:,3) = dtau(5,:,1)+dtau(6,:,2)+dtau(3,:,3)


    DO j = 1,dim
       IF(q_coeff_type .EQ. 1) q_k(:,j) = -1.0_pr/Pra_t(:) * mu_turb(:) * du(dim+1,:,j)
       IF(q_coeff_type .EQ. 2) q_k(:,j) = -Pra_t(:) * mu_turb * du(dim+1,:,j)
       IF(q_coeff_type .EQ. 3) q_k(:,j) = -Pra_t(:)* u_int(:,n_den) * SQRT(MAX(0.0_pr,u_int(:,n_var_k))/u_int(:,n_den) ) * du(dim+1,:,j)
       !IF(q_coeff_type .EQ. 3) q_k(:,j) = -Pra_t(:) * du(dim+1,:,j) !ERIC: reconcile q_coeff_type == 3 

       IF(deltaMij .AND. q_coeff_type .EQ. 3) q_k(:,j) = q_k(:,j)*delta(:)
    END DO

    f_correlation = 0.0_pr
    IF ( C_f_type .NE. coeff_alt ) THEN
       DO j = 1,dim
          f_correlation(:,j) = C_f(:) * u_int(:,n_den)*SQRT(MAX(0.0_pr,u_int(:,n_var_k)/u_int(:,n_den)  ) ) * du(dim+3,:,j)
          IF(deltaMij) f_correlation(:,j) = f_correlation(:,j)*delta(:)
       END DO
    END IF 


   !SGS_production
    sgs_production(:) = calc_sgs_production(tau_ij,S_ij, nwlt)

    !PRESSURE DILATATION
    IF( C_pi_type .EQ. coeff_off ) THEN 
      p_dilatation = 0.0_pr
    ELSE
      p_dilatation = calc_keqn_p_dilat( du(1:dim,:,1:dim), du(2+dim,:,1:dim), nwlt, meth ) 
    END IF 
  

    F(:,:) = 0.0_pr ! Variable is fastest varying, then direction
    dyn_visc_loc(:) = calc_dynamic_viscosity( u_int )
    DO j = 1,dim
       jshift = (j - 1)*2  ! Only 2 fields: E_resolved and Ksgs

       !ENERGY
       !F(:,jshift+1) =  F(:,jshift+1) + (cp_field + R_field)*q_k(:,j) 
       F(:,jshift+1) =  F(:,jshift+1) + R_field*q_k(:,j)  ! From subtraction of dk/dt to get resolved energy field, Cp*q already accounted for by eddy viscosity 
       F(:,jshift+1) =  F(:,jshift+1) + u_tau(:,j)

       !KSGS  
       F(:,jshift+2) =  F(:,jshift+2) +  f_correlation(:,j)  ! check the divergence of this term for stability
       F(:,jshift+2) =  F(:,jshift+2) + MAX(0.0_pr,u_int(:,n_var_k)) * u_int(:,n_mom(j))/u_int(:,n_den) - R_field(:)*q_k(:,j) - u_tau(:,j) &
                     - dyn_visc_loc(:) * div_tau(:,j)   !ERIC: check with diagnostic

       IF ( C_f_type .EQ. coeff_alt ) THEN
          F(:,jshift+2) =  F(:,jshift+2) - MAX(0.0_pr, dyn_visc_loc(:) + mu_turb(:) ) * du(dim+3,:,j)  ! Use mu_t to model the triple correlation term (aligns eddy diffusions)
       ELSE
          F(:,jshift+2) =  F(:,jshift+2) - dyn_visc_loc(:) * du(dim+3,:,j) 
       END IF
    END DO


    CALL c_diff_fast(F(:,1:dim*2 ), dF(1:dim*2,:,:), d2F(1:dim*2,:,:), j_lev, nwlt, meth, 10, dim*2, 1, dim*2)  ! dF/dx_j
    !CALL print_extrema( df(1,:,1), nwlt, 'RHS: df_11')
    !CALL print_extrema( df(2,:,1), nwlt, 'RHS: df_21')
    !CALL print_extrema( df(3,:,2), nwlt, 'RHS: df_32')
    !CALL print_extrema( df(4,:,2), nwlt, 'RHS: df_43')
    !CALL print_extrema( df(5,:,3), nwlt, 'RHS: df_53')
    !CALL print_extrema( df(6,:,3), nwlt, 'RHS: df_63')

    !FLUX TERMS 
    DO j = 1,dim
       jshift = (j - 1)*2  ! Only 2 fields: E_resolved and Ksgs

       ! MOMENTUM isotropic part of tau 
       shift=(n_mom(j)-1)*nwlt
       keqn_rhs(shift+1:shift+nwlt) = keqn_rhs(shift+1:shift+nwlt) - 2.0_pr / 3.0_pr * du(dim+4,:,j)

       ! ENERGY
       shift=(n_eng-1)*nwlt
       keqn_rhs(shift+1:shift+nwlt) = keqn_rhs(shift+1:shift+nwlt) - dF(jshift+1,:,j)

       ! KSGS 
       shift=(n_var_k-1)*nwlt
       keqn_rhs(shift+1:shift+nwlt) = keqn_rhs(shift+1:shift+nwlt) - dF(jshift+2,:,j)
    END DO

    !ADDITIONAL TERMS
    ! ENERGY
    shift=(n_eng-1)*nwlt
    keqn_rhs(shift+1:shift+nwlt) = keqn_rhs(shift+1:shift+nwlt) + diss_sol
    keqn_rhs(shift+1:shift+nwlt) = keqn_rhs(shift+1:shift+nwlt) - SGS_production - p_dilatation


    ! KSGS 
    shift=(n_var_k-1)*nwlt
    keqn_rhs(shift+1:shift+nwlt) = keqn_rhs(shift+1:shift+nwlt) - diss_sol - diss_dilat
    keqn_rhs(shift+1:shift+nwlt) = keqn_rhs(shift+1:shift+nwlt) + SGS_production + p_dilatation

    CALL print_extrema( keqn_rhs(shift+1:shift+nwlt), nwlt, 'RHS: k eqn' )

    CALL print_extrema( diss_sol, nwlt, 'RHS: diss_sol')
    CALL print_extrema( diss_dilat, nwlt, 'RHS: diss_dilat')
    CALL print_extrema( sgs_production, nwlt, 'RHS: sgs_production')
    CALL print_extrema( p_dilatation, nwlt, 'RHS: p_dilatation')


  END SUBROUTINE keqn_compress_rhs


  SUBROUTINE keqn_compress_Drhs(keqn_Drhs, u_p, u_prev, meth )
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: meth
    REAL (pr), DIMENSION (ng,ne), INTENT(IN) :: u_p, u_prev
    REAL (pr), DIMENSION (ng*ne), INTENT(INOUT) :: keqn_Drhs

  END SUBROUTINE keqn_compress_Drhs

  SUBROUTINE keqn_compress_Drhs_diag(keqn_diag,u_prev,meth)
    USE variable_mapping
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: meth
    REAL (pr), DIMENSION (ng*ne), INTENT(INOUT) :: keqn_diag
    REAL (pr), DIMENSION (ng,ne), INTENT(IN) :: u_prev

  END SUBROUTINE keqn_compress_Drhs_diag

  SUBROUTINE keqn_compress_additional_vars( t_local, flag )
    USE field
    USE equations_compressible_vars
    IMPLICIT NONE

    REAL (pr), INTENT (IN) ::  t_local
    INTEGER , INTENT(IN) :: flag ! 0- called during adaption to IC, 1 called during main integration loop

    ! Save any dynamic coefficients as a diagnostic
    IF( save_dyn_coeffs ) THEN
        
      IF (Cs_type .EQ. coeff_Ger_clip .OR. Cs_type .EQ. coeff_Ger_dyn &
        .OR. Cs_type .EQ. coeff_Bar_clip .OR. Cs_type .EQ. coeff_Bar_dyn ) THEN
        IF( ALLOCATED(Cs) ) u(:,n_var_cs) = Cs
      END IF
        
      IF (Pra_t_type .EQ. coeff_Ger_clip .OR. Pra_t_type .EQ. coeff_Ger_dyn &
        .OR. Pra_t_type .EQ. coeff_Bar_clip .OR. Pra_t_type .EQ. coeff_Bar_dyn ) THEN
        IF( ALLOCATED(Pra_t) ) u(:,n_var_pra_t) = Pra_t
      END IF

      IF (C_f_type .EQ. coeff_Ger_clip .OR. C_f_type .EQ. coeff_Ger_dyn &
        .OR. C_f_type .EQ. coeff_Bar_clip .OR. C_f_type .EQ. coeff_Bar_dyn ) THEN
        IF( ALLOCATED(C_f) ) u(:,n_var_c_f) = C_f
      END IF

      IF (C_diss_sol_type .EQ. coeff_Ger_clip .OR. C_diss_sol_type .EQ. coeff_Ger_dyn &
        .OR. C_diss_sol_type .EQ. coeff_Bar_clip .OR. C_diss_sol_type .EQ. coeff_Bar_dyn ) THEN
        IF( ALLOCATED(C_diss_sol) ) u(:,n_var_c_diss_sol) = C_diss_sol
      END IF

      IF (C_diss_dilat_type .EQ. coeff_Ger_clip .OR. C_diss_dilat_type .EQ. coeff_Ger_dyn &
        .OR. C_diss_dilat_type .EQ. coeff_Bar_clip .OR. C_diss_dilat_type .EQ. coeff_Bar_dyn ) THEN
        IF( ALLOCATED(C_diss_dilat) ) u(:,n_var_c_diss_dilat) = C_diss_dilat
      END IF

      IF (C_pi_type .EQ. coeff_Ger_clip .OR. C_pi_type .EQ. coeff_Ger_dyn &
        .OR. C_pi_type .EQ. coeff_Bar_clip .OR. C_pi_type .EQ. coeff_Bar_dyn ) THEN
        IF( ALLOCATED(C_pi) ) u(:,n_var_c_pi) = C_pi
      END IF

    END IF


  END SUBROUTINE keqn_compress_additional_vars


  FUNCTION keqn_compress_cal_cfl()
    USE variable_mapping
    USE sizes 
    USE curvilinear 
    USE field
    USE wlt_trns_util_mod 
    IMPLICIT NONE
    REAL (pr) :: keqn_compress_cal_cfl

    REAL (pr) :: flor 
    REAL (pr), DIMENSION(dim,nwlt) :: h_arr
    INTEGER idim, ibnd

    flor = 1e-12_pr

    !IF ( ) THEN
      ! Calculate the CFL in computational space
      CALL get_all_local_h (h_arr)
      IF ( ANY( transform_dir(1:dim) .EQ. 1 ) ) THEN
        keqn_compress_cal_cfl = MAXVAL( 2.0_pr*dt*C_f(:) * u(:,n_den) * delta * SQRT( MAX(0.0_pr,u(:,n_var_k))/u(:,n_den)) *MAXVAL(( SUM(curvilinear_jacobian(1:dim,1:dim,:)**2, DIM=2 ) )/(h_arr(:,:)),DIM=1)**2  )        
      ELSE
        keqn_compress_cal_cfl = MAXVAL( 2.0_pr*dt*C_f(:) * u(:,n_den) * delta * SQRT( MAX(0.0_pr,u(:,n_var_k))/u(:,n_den)) *MAXVAL(1.0_pr/(h_arr(:,:)),DIM=1)**2  )
      END IF
      CALL parallel_global_SUM(REALMAXVAL=keqn_compress_cal_cfl)
      keqn_compress_cal_cfl = MAX( flor, keqn_compress_cal_cfl )
    !END IF



  END FUNCTION keqn_compress_cal_cfl


  ! Keqn based eddy viscosity: possibly need 
  SUBROUTINE calc_keqn_eddy_visc( mu_t_out, kk_t_out, u_int ) 
    USE equations_compressible 
    USE variable_mapping 
    IMPLICIT NONE
    REAL (pr), DIMENSION (nwlt), INTENT (OUT)   :: mu_t_out, kk_t_out 
    REAL (pr), DIMENSION (nwlt,n_integrated), INTENT (IN)    :: u_int

    ! ERIC: generic?
    INTEGER, PARAMETER :: meth = HIGH_ORDER

    REAL (pr), DIMENSION (1:nwlt)             :: R_field, cp_field, ksts 
    REAL (pr), DIMENSION (nwlt,dim+3)         :: v          !velocity + Temperature + Pressure
    REAL (pr), DIMENSION (1:nwlt,1:6)         :: s_ij, s_ij_traceless
    REAL (pr), DIMENSION (dim+3,nwlt,dim)     :: du, d2u

    REAL (pr), DIMENSION (1:nwlt,1:2)         :: tmp_array
    REAL (pr), DIMENSION (1:nwlt,1:6)         :: Leonard_stress, modeled_tensor ! LHS and RHS of Germano equality for momentum
    REAL (pr), DIMENSION (1:nwlt,1:3)         :: Leonard_vect, modeled_vect     ! LHS and RHS of Germano equality for temperature conduction triple correlation closure

    INTEGER :: i, l

    REAL (pr), PARAMETER :: condition_eps = 1.0e-6_pr ! ERIC: Conditioning for dynamic coefficient division

    CALL print_extrema( u_int(:,n_var_k), nwlt, 'ksgs (calc eddy visc)')

    !calculate native quantities  ! ERIC: put in helper functions 
    DO i = 1,dim
       v(:,i) = u_int(:,n_mom(i))/u_int(:,n_den)  !compute velocity
    END DO

    ! ERIC: wrapper function  ERIC: do we need the temp, press
    cp_field(:) = cp_in(Nspec)
    R_field(:) = R_in(Nspec)
    IF (Nspec>1) cp_field(:) = (1.0_pr-SUM(u_int(:,n_spc(1):n_spc(Nspecm)),DIM=2)/u_int(:,n_den))*cp_in(Nspec) !cp_Nspec
    IF (Nspec>1) cp_field(:) = (1.0_pr-SUM(u_int(:,n_spc(1):n_spc(Nspecm)),DIM=2)/u_int(:,n_den))*R_in(Nspec) !R_Nspec
    DO l=1,Nspecm
       cp_field(:) = cp_field(:) + u_int(:,n_spc(l))/u_int(:,n_den)*cp_in(l) !cp
       R_field(:) = R_field(:) + u_int(:,n_spc(l))/u_int(:,n_den)*R_in(l) !R
    END DO
    v(:,2+dim) = (u_int(:,n_eng)-0.5_pr*SUM(u_int(:,n_mom(1):n_mom(dim))**2,DIM=2)/u_int(:,n_den))/(cp_field(:)/R_field(:)-1.0_pr) / F_compress  !pressure
    v(:,1+dim) = v(:,2+dim)/R_field(:)/u_int(:,n_den)  !Temperature
    v(:,3+dim) = MAX(0.0_pr,u_int(:,n_var_k))/u_int(:,n_den)    !ksgs

    CALL c_diff_fast(v(:,1:dim+3), du(1:dim+3,:,:), d2u(1:dim+3,:,:), j_lev, nwlt, meth, 10, dim+3, 1, dim+3)

    CALL calcStrainRateTensors( du(1:dim, :, 1:dim), nwlt, s_ij, TRACELESS = S_ij_traceless )


    !-----------------------------------Calculate tau_ij (modeled)--------------------------------------------------- 

    !
    ! find dynCs = <LijMij>/<MijMij> 
    ! 
    IF(Cs_type .EQ. coeff_off ) THEN
       Cs(:) = 0.0_pr
    ELSE IF(Cs_type .EQ. coeff_const) THEN
       Cs(:) = Cs_in
    ELSE IF (Cs_type .EQ. coeff_Ger_clip .OR. Cs_type .EQ. coeff_Ger_dyn ) THEN

      !Coefficient numerator/denominator calculated in SUBROUTINE GM_terms
      !-------------------------------------- Calculate Lij and Sij and Ksts-------------------------------------------------

      ! Lij and Mij
      !CALL calc_keqn_momentum_closure( Leonard_stress, Modeled_tensor, ksts, s_ij_traceless, u_int ) 
      CALL calc_keqn_momentum_closure_OPT( Leonard_stress, Modeled_tensor, ksts, s_ij_traceless, u_int ) 


      tmp_array(:,1) = symm_tensor_contract3D( Leonard_stress, Modeled_tensor, nwlt )  ! Lij * Mij
      tmp_array(:,2) = symm_tensor_contract3D( Modeled_tensor, Modeled_tensor, nwlt )  ! Mij * Mij

       IF (filt_C) THEN  !local volume averaging (Chai 2010)  
          !CALL mdl_filt_by_type(filt_in(:,1:2),2,TESTfilter)  !ERIC: adjust for change of variables
          CALL local_lowpass_filt( tmp_array(:,1:2), j_lev, nwlt, 2, 1, 2,&
               lowpassfilt_support, lowpass_filt_type, tensorial_filt)
       END IF

       !WHERE (tmp_array(:,2) == 0.0_pr) !Clip nu_t to avoid Nan !ERIC: is there any guaranteed positivity?
       !   Cs(:) = 0.0_pr
       !ELSEWHERE
       !   Cs(:) = 0.5_pr*tmp_array(:,1)/tmp_array(:,2)  !local value
       !END WHERE
       WHERE (tmp_array(:,2) .GE. 0.0_pr) !Clip nu_t to avoid Nan !ERIC: is there any guaranteed positivity?
          Cs(:) = 0.5_pr*tmp_array(:,1)/( tmp_array(:,2) + condition_eps )  !local value
       ELSEWHERE
          Cs(:) = 0.5_pr*tmp_array(:,1)/( tmp_array(:,2) - condition_eps )  !local value
       END WHERE

       !Limiting
       IF (Cs_type .EQ.coeff_Ger_clip) THEN
          Cs(:) = MAX(Cs, Cs_limit(1))
          Cs(:) = MIN(Cs, Cs_limit(2))
       END IF

    END IF 

    CALL print_extrema( delta, nwlt, 'LDKM: delta') 
    mu_t_out = Cs * u_int(:,n_den) * SQRT( MAX(0.0_pr,u_int(:,n_var_k)) / u_int(:,n_den) ) 
    IF (deltaMij) mu_t_out = mu_t_out * delta
    CALL print_extrema( mu_t_out, nwlt, 'LDKM: mu_t_out') 

    IF(Pra_t_type .EQ. coeff_off) THEN
       Pra_t(:) = 0.0_pr
    ELSE IF(Pra_t_type .EQ. coeff_const) THEN
       Pra_t(:) = Pra_t_in
    ELSE IF (Pra_t_type .EQ. coeff_Ger_clip .OR. Pra_t_type .EQ. coeff_Ger_dyn ) THEN

      ! Kj and Nj
      CALL calc_keqn_thermal_closure( Leonard_vect, Modeled_vect, u_int, cp_field - R_field, ksts ) 

      tmp_array(:,1) = SUM( Leonard_vect(:,1:3)*Modeled_vect(:,1:3), DIM=2  ) 
      tmp_array(:,2) = SUM( Modeled_vect(:,1:3)*Modeled_vect(:,1:3), DIM=2  ) 

      ! ERIC: should this be averaged to isolate Pra_t before deriving tensors, or is it consistent to assume Cs is unafected by filter in -all- expressions?
      IF(q_coeff_type .EQ. 1 .OR. q_coeff_type .EQ. 2) tmp_array(:,2)= Cs*tmp_array(:,2)  !needed here before filtering, accounts for cancellation in division

       IF (filt_C) THEN
          ! CALL mdl_filt_by_type(filt_in(:,1:2),2,TESTfilter)
          CALL local_lowpass_filt( tmp_array(:,1:2), j_lev, nwlt, 2, 1, 2,&
               lowpassfilt_support, lowpass_filt_type, tensorial_filt)
       END IF

       IF(q_coeff_type .EQ.1)  THEN  !Pra_t 
          !WHERE ( tmp_array(:,1) .EQ. 0.0_pr)  !clip to avoid NaN
          !   pra_t(:) = 0.0_pr
          !ELSEWHERE
          !   pra_t(:) = tmp_array(:,2)/tmp_array(:,1)   !Pr_t 
          !END WHERE
          WHERE ( tmp_array(:,1) .GE. 0.0_pr)  !clip to avoid NaN
             pra_t(:) = tmp_array(:,2)/(tmp_array(:,1) + condition_eps )  !Pr_t 
          ELSEWHERE
             pra_t(:) = tmp_array(:,2)/(tmp_array(:,1) - condition_eps )   !Pr_t 
          END WHERE
          
       ELSE IF(q_coeff_type .EQ. 2 .OR. q_coeff_type .EQ. 3) THEN  ! 1/Pra_t OR kappa_t = Cs/Pra_t
          !WHERE (tmp_array(:,2) .EQ. 0.0_pr)  !clip to avoid NaN
          !   pra_t(:) = 0.0_pr
          !ELSEWHERE
          !   pra_t(:) = tmp_array(:,1)/tmp_array(:,2)  ! kappa_t
          !END WHERE
          WHERE (tmp_array(:,2) .GE. 0.0_pr)  !clip to avoid NaN
             pra_t(:) = tmp_array(:,1)/(tmp_array(:,2) + condition_eps ) ! kappa_t
          ELSEWHERE
             pra_t(:) = tmp_array(:,1)/(tmp_array(:,2) - condition_eps )  ! kappa_t
          END WHERE
       END IF

       !Limiting
       IF (Pra_t_type .EQ. 2) THEN  !limited dynamic model
          Pra_t(:) = MAX(Pra_t, Pra_t_limit(1))
          Pra_t(:) = MIN(Pra_t, Pra_t_limit(2))
       END IF
       CALL print_extrema( Pra_t, nwlt, 'LDKM: Pra_t (should probably be positive for realizability, unless type=3, Cs/Pr )')
    END IF


    kk_t_out = cp_field(:) * u_int(:,n_den) * SQRT( MAX(0.0_pr,u_int(:,n_var_k)) / u_int(:,n_den) ) 
    IF(q_coeff_type .EQ.1 .AND. Pra_t_type .EQ. coeff_off ) kk_t_out = 0.0_pr 
    IF(q_coeff_type .EQ.1 .AND. Pra_t_type .NE. coeff_off ) kk_t_out = Cs/Pra_t * kk_t_out
    IF(q_coeff_type .EQ.2) kk_t_out = Cs*Pra_t * kk_t_out
    IF(q_coeff_type .EQ.3) kk_t_out = Pra_t * kk_t_out
    IF (deltaMij) kk_t_out = kk_t_out * delta
    


    ! ------------------- Calculate Residual coefficients ------------------------


    IF(C_f_type .EQ. coeff_off .OR. C_f_type .EQ. coeff_alt ) THEN  ! Alternate formulation is by using nu_t dk/dx for triple correlation model (align with momentum model)
       C_f = 0.0_pr
    ELSE IF (C_f_type .EQ. coeff_const) THEN
       C_f = C_f_in
    ELSE IF (C_f_type .EQ. coeff_Ger_clip .OR. C_f_type .EQ. coeff_Ger_dyn &
        .OR. C_f_type .EQ. coeff_Bar_clip .OR. C_f_type .EQ. coeff_Bar_dyn ) THEN
  
       CALL calc_keqn_triple_corr_closure( Leonard_vect, Modeled_vect, u_int, ksts, du(dim+3,:,:) ) 

       !Calculate model coefficient (C_f*delta if .NOT. deltaMij)
       tmp_array(:,1) = SUM( Leonard_vect(:,1:3)*Modeled_vect(:,1:3), DIM=2  ) 
       tmp_array(:,2) = SUM( Modeled_vect(:,1:3)*Modeled_vect(:,1:3), DIM=2  ) 

       IF (filt_C) THEN
          !CALL mdl_filt_by_type(filt_in(:,1:2),2,TESTfilter)
          CALL local_lowpass_filt( tmp_array(:,1:2), j_lev, nwlt, 2, 1, 2,&
                  lowpassfilt_support, lowpass_filt_type, tensorial_filt)
       END IF

       !WHERE ( tmp_array(:,2) .EQ. 0.0_pr)
       !   C_f = 0.0_pr
       !ELSEWHERE
       !   C_f = tmp_array(:,1)/tmp_array(:,2)
       !END WHERE
       WHERE ( tmp_array(:,2) .GE. 0.0_pr)
          C_f = tmp_array(:,1)/(tmp_array(:,2) + condition_eps )
       ELSEWHERE
          C_f = tmp_array(:,1)/(tmp_array(:,2) - condition_eps )
       END WHERE

       IF (C_f_type .EQ. coeff_Ger_clip .OR. C_f_type .EQ. coeff_Bar_clip ) THEN
          C_f(:) = MAX(C_f, C_f_limit(1))
          C_f(:) = MIN(C_f, C_f_limit(2))
       END IF
    END IF

    ! Solonoidal and Dilatational dissipation
    IF (C_diss_sol_type .EQ. coeff_Ger_clip .OR. C_diss_sol_type .EQ. coeff_Ger_dyn &
        .OR. C_diss_sol_type .EQ. coeff_Bar_clip .OR. C_diss_sol_type .EQ. coeff_Bar_dyn &
        .OR. C_diss_dilat_type .EQ. coeff_Ger_clip .OR. C_diss_dilat_type .EQ. coeff_Ger_dyn &
        .OR. C_diss_dilat_type .EQ. coeff_Bar_clip .OR. C_diss_dilat_type .EQ. coeff_Bar_dyn ) THEN

      ! Leonard/Modeled(:,1) -> solonoidal dissipation coefficient
      ! Leonard/Modeled(:,2) -> dilatational dissipation coefficient
      ! ERIC: check the velocity gradient
      CALL calc_keqn_dissipation_closure( Leonard_vect(:,1), Modeled_vect(:,1), Leonard_vect(:,2), Modeled_vect(:,2), u_int, ksts, du(1:dim,:,1:dim), S_ij_traceless ) 
    END IF

    ! Solonoidal coefficient
    IF( C_diss_sol_type .EQ. coeff_off ) THEN
       C_diss_sol = 0.0_pr
    ELSE IF (C_diss_sol_type .EQ. coeff_const ) THEN
       C_diss_sol = C_diss_sol_in
    ELSE IF (C_diss_sol_type .EQ. coeff_Ger_clip .OR. C_diss_sol_type .EQ. coeff_Ger_dyn &
        .OR. C_diss_sol_type .EQ. coeff_Bar_clip .OR. C_diss_sol_type .EQ. coeff_Bar_dyn ) THEN 

        ! Closed equation, since these are scalars, so do not need least squares approach
        tmp_array(:,1) = Leonard_vect(:,1)
        tmp_array(:,2) = Modeled_vect(:,1)

        !Model Coefficient:
        IF (filt_C) THEN  !ERIC: reduce to single filter call
          !CALL mdl_filt_by_type(E_s,1,TESTfilter)
          !CALL mdl_filt_by_type(F_s,1,TESTfilter)
          CALL local_lowpass_filt( tmp_array(:,1:2), j_lev, nwlt, 2, 1, 2,&
               lowpassfilt_support, lowpass_filt_type, tensorial_filt)
        END IF

        !WHERE ( tmp_array(:,2) .LE. 0.0_pr)  !Clipping to avoid NaN and negative (unrealizable) values
        !  C_diss_sol = 0.0_pr
        !ELSEWHERE
        !  C_diss_sol = tmp_array(:,1)/tmp_array(:,2)  
        !END WHERE
        WHERE ( tmp_array(:,2) .GE. 0.0_pr)  !Clipping to avoid NaN and negative (unrealizable) values
          C_diss_sol = tmp_array(:,1)/( tmp_array(:,2)  + condition_eps )
        ELSEWHERE
          C_diss_sol = tmp_array(:,1)/( tmp_array(:,2)   - condition_eps )
        END WHERE

       IF (C_diss_sol_type .EQ. coeff_Ger_clip .OR. C_diss_sol_type .EQ. coeff_Bar_clip ) THEN
          C_diss_sol(:) = MAX(0.0_pr, MAX(C_diss_sol, C_diss_sol_limit(1)))
          C_diss_sol(:) = MIN(C_diss_sol, C_diss_sol_limit(2))
       END IF

    END IF

    ! Dilatational coefficient
    IF( C_diss_dilat_type .EQ. coeff_off ) THEN
       C_diss_dilat = 0.0_pr
    ELSE IF (C_diss_dilat_type .EQ. coeff_const ) THEN
       C_diss_dilat = C_diss_dilat_in
    ELSE IF (C_diss_dilat_type .EQ. coeff_Ger_clip .OR. C_diss_dilat_type .EQ. coeff_Ger_dyn &
        .OR. C_diss_dilat_type .EQ. coeff_Bar_clip .OR. C_diss_dilat_type .EQ. coeff_Bar_dyn ) THEN 

        ! Closed equation, since these are scalars, so do not need least squares approach
        tmp_array(:,1) = Leonard_vect(:,2)
        tmp_array(:,2) = Modeled_vect(:,2)

        !Model Coefficient:
        IF (filt_C) THEN  !ERIC: reduce to single filter call
          !CALL mdl_filt_by_type(E_s,1,TESTfilter)
          !CALL mdl_filt_by_type(F_s,1,TESTfilter)
          CALL local_lowpass_filt( tmp_array(:,1:2), j_lev, nwlt, 2, 1, 2,&
               lowpassfilt_support, lowpass_filt_type, tensorial_filt)
        END IF

        !WHERE ( tmp_array(:,2) .LE. 0.0_pr)  !Clipping to avoid NaN and negative (unrealizable) values
        !  C_diss_dilat = 0.0_pr
        !ELSEWHERE
        !  C_diss_dilat = tmp_array(:,1)/tmp_array(:,2)  
        !END WHERE
        WHERE ( tmp_array(:,2) .GE. 0.0_pr)  !Clipping to avoid NaN and negative (unrealizable) values
          C_diss_dilat = tmp_array(:,1)/(tmp_array(:,2) + condition_eps )
        ELSEWHERE
          C_diss_dilat = tmp_array(:,1)/(tmp_array(:,2) - condition_eps )
        END WHERE

       IF (C_diss_dilat_type .EQ. coeff_Ger_clip .OR. C_diss_sol_type .EQ. coeff_Bar_clip ) THEN
          C_diss_dilat(:) = MAX(0.0_pr, MAX(C_diss_dilat, C_diss_dilat_limit(1)))
          C_diss_dilat(:) = MIN(C_diss_dilat, C_diss_dilat_limit(2))
       END IF

    END IF


    ! Pressure coefficient
    IF( C_pi_type .EQ. coeff_off ) THEN
       C_pi = 0.0_pr
    ELSE IF (C_pi_type .EQ. coeff_const ) THEN
       C_pi = C_pi_in
    END IF

    CALL print_extrema( Cs, nwlt, 'LDKM Cs: ')
    CALL print_extrema( Pra_t, nwlt, 'LDKM Pra_t: ')
    CALL print_extrema( C_f, nwlt, 'LDKM: c_f (should probably be positive for stability)')
    CALL print_extrema( C_diss_sol, nwlt, 'LDKM: c_diss_sol (should probably be positive for realizability)')
    CALL print_extrema( C_diss_dilat, nwlt, 'LDKM: c_diss_sol (should probably be positive for realizability)')
    CALL print_extrema( C_pi, nwlt, 'LDKM: c_pi')

  END SUBROUTINE calc_keqn_eddy_visc

  ! Initialize Ksgs by krts
  SUBROUTINE initialize_ksgs( u_int ) 
    USE equations_compressible 
    USE variable_mapping 
    IMPLICIT NONE
    REAL (pr), DIMENSION (nwlt,n_integrated), INTENT (INOUT)    :: u_int
    REAL (pr), DIMENSION (1:nwlt,1:4)         :: filt_in  
    REAL (pr), DIMENSION (1:nwlt,1:3)         :: Leonard 
    INTEGER :: idim

    IF ( dim .NE. 3 ) CALL error('Can only initialize ksgs <- krts for dim .EQ. 3')

    !Test filter to calculate L
    filt_in(:,1:3) = u_int(:,n_mom(1:3))  !rho*u
    filt_in(:,4)   = u_int(:,n_den)       !rho

    CALL mdl_filt_by_type(filt_in(:,1:4),4,TESTfilter)

    !Ordering corresponds with Sij 
    Leonard(:,1) = -filt_in(:,1)*filt_in(:,1)/filt_in(:,4)  !(rho*u)(rho*u)/(rho)
    Leonard(:,2) = -filt_in(:,2)*filt_in(:,2)/filt_in(:,4)  !(rho*v)(rho*v)/(rho)
    Leonard(:,3) = -filt_in(:,3)*filt_in(:,3)/filt_in(:,4)  !(rho*w)(rho*w)/(rho)

    filt_in(:,1) = u_int(:,n_mom(1))*u_int(:,n_mom(1))/u_int(:,n_den)  !rho*u*u 
    filt_in(:,2) = u_int(:,n_mom(2))*u_int(:,n_mom(2))/u_int(:,n_den)  !rho*v*v 
    filt_in(:,3) = u_int(:,n_mom(3))*u_int(:,n_mom(3))/u_int(:,n_den)  !rho*w*w 
    
    CALL mdl_filt_by_type(filt_in(:,1:3),3,TESTfilter)

    DO idim = 1,3
      Leonard(:,idim) = Leonard(:,idim) + filt_in(:,idim)
    END DO
    !L_ij diagonal terms determined

    ! Modeled tensor for Germano equality
    ! ksts = Krts + background
    u_int(:,n_var_k) = 0.5_pr*SUM( Leonard(:,1:3), DIM=2 ) + u_int(:,n_den)*background_ksgs
    CALL mdl_filt_by_type(u_int(:,n_var_k),1,TESTfilter)
    u_int(:,n_var_k) = init_ksts_scale * u_int(:,n_var_k)

    CALL print_extrema( u_int(:,n_var_k), nwlt, 'INITIALIZE: ksgs')

  END SUBROUTINE initialize_ksgs 

  ! Germano dynamic method for determining momentum closure terms 
  SUBROUTINE calc_keqn_momentum_closure( Leonard_out, Modeled_out, ksts_out, s_ij_traceless, u_int ) 
    USE equations_compressible 
    USE variable_mapping 
    IMPLICIT NONE
    REAL (pr), DIMENSION (nwlt,1:6), INTENT (OUT)   :: Leonard_out,Modeled_out  ! Traceless quantities 
    REAL (pr), DIMENSION (1:nwlt),   INTENT (OUT)   :: ksts_out
    REAL (pr), DIMENSION (nwlt,n_integrated), INTENT (IN)    :: u_int
    REAL (pr), DIMENSION (nwlt,1:6), INTENT (IN)             :: s_ij_traceless 

    REAL (pr), DIMENSION (1:nwlt,1:7)         :: filt_in  ! Increased size to allow for an extra field to filter
    REAL (pr), DIMENSION (1:nwlt)             :: rho_test
    REAL (pr), DIMENSION (1:nwlt,1:6)         :: S2_ij, smod2

    INTEGER :: i,j

    !Coefficient numerator/denominator calculated in SUBROUTINE GM_terms
    !-------------------------------------- Calculate Lij and Sij and Ksts-------------------------------------------------
    !ERIC: check all sign conventions       
    IF ( dim .NE. 3 ) CALL error('Can only calculate dynamic coefficients for dim .EQ. 3')

    !Test filter to calculate L
    filt_in(:,1) = u_int(:,n_den)  !rho 
    filt_in(:,2:4) = u_int(:,n_mom(1:3))  !rho*u
    !filt_in(:,5) = u_in(:,n_var_k)/u_in(:,1)  !possibly filter rho*k_sgs if ksgs2 is no longer needed

    CALL mdl_filt_by_type(filt_in(:,1:4),4,TESTfilter)
    rho_test(:)  = filt_in(:,1) !need this variable in many other calculations

    ! An intermediate quantity, this prevents function dedicated to Lij
    ! Favre average velocities, and compute Sij_test, Smod_test ERIC: (does this commute across differential operator)
    DO i=1,3
      Leonard_out(:,i) = filt_in(:,i+1)/rho_test
    END DO
    CALL Sij( Leonard_out(:,1:3), nwlt, S2_ij, Smod2, .TRUE. ) !calculate both Sij* and |S*|, assume correct 


    !Ordering corresponds with Sij 
    Leonard_out(:,1) = -filt_in(:,2)*filt_in(:,2)/filt_in(:,1)  !(rho*u)(rho*u)/(rho)
    Leonard_out(:,2) = -filt_in(:,3)*filt_in(:,3)/filt_in(:,1)  !(rho*v)(rho*v)/(rho)
    Leonard_out(:,3) = -filt_in(:,4)*filt_in(:,4)/filt_in(:,1)  !(rho*w)(rho*w)/(rho)
    Leonard_out(:,4) = -filt_in(:,2)*filt_in(:,3)/filt_in(:,1)  !(rho*u)(rho*v)/(rho)
    Leonard_out(:,5) = -filt_in(:,2)*filt_in(:,4)/filt_in(:,1)  !(rho*u)(rho*w)/(rho)
    Leonard_out(:,6) = -filt_in(:,3)*filt_in(:,4)/filt_in(:,1)  !(rho*v)(rho*w)/(rho)


    filt_in(:,1) = u_int(:,n_mom(1))*u_int(:,n_mom(1))/u_int(:,n_den)  !rho*u*u 
    filt_in(:,2) = u_int(:,n_mom(2))*u_int(:,n_mom(2))/u_int(:,n_den)  !rho*v*v 
    filt_in(:,3) = u_int(:,n_mom(3))*u_int(:,n_mom(3))/u_int(:,n_den)  !rho*w*w 
    filt_in(:,4) = u_int(:,n_mom(1))*u_int(:,n_mom(2))/u_int(:,n_den)  !rho*u*v 
    filt_in(:,5) = u_int(:,n_mom(1))*u_int(:,n_mom(3))/u_int(:,n_den)  !rho*u*w 
    filt_in(:,6) = u_int(:,n_mom(2))*u_int(:,n_mom(3))/u_int(:,n_den)  !rho*v*w 
    
    filt_in(:,7) = MAX(u_int(:,n_var_k),0.0_pr)   ! Ride along to minimize filtering operations
    CALL mdl_filt_by_type(filt_in(:,1:7),7,TESTfilter)

    DO j = 1,6
      Leonard_out(:,j) = Leonard_out(:,j) + filt_in(:,j)
    END DO


    ! Modeled tensor for Germano equality
    ! Ksts
    ksts_out = (0.5_pr*SUM( Leonard_out(:,1:3), DIM=2 ) + filt_in(:,7))/rho_test(:)
    ksts_out = MAX(ksts_out,0.0_pr) 

    ! Lij and Mij traceless  ERIC:  traceless helper function?  
    DO i = 1,3
      Leonard_out(:,i) = Leonard_out(:,i) - SUM(Leonard_out(:,1:3),DIM=2) / 3.0_pr 
    END DO
    !L_ij is determined, Verified 8/18/13 

    DO j = 1,6
      filt_in(:,j) = u_int(:,n_den)*SQRT(MAX(0.0_pr,u_int(:,n_var_k)/u_int(:,n_den)))*(S_ij_traceless(:,j)  ) !rho sqrt(k) S_ij   
    END DO
    IF(deltaMij)  THEN  !Mirrored off of Guliano's code
      DO j=1,6
        filt_in(:,j) = delta(:)*filt_in(:,j)
      END DO
    END IF

    CALL mdl_filt_by_type(filt_in(:,1:6),6,TESTfilter)

    IF (deltaMij) THEN
      DO i=1,6
        Modeled_out(:,i) = filt_in(:,i) - Mcoeff*delta(:)*rho_test(:)*SQRT( MAX(0.0_pr,ksts_out) )*(S2_ij(:,i)  )  !M_ij  2.0_pr factor not implicit in Cs
      END DO
    ELSE
      DO i=1,6
        Modeled_out(:,i) = filt_in(:,i) - Mcoeff*rho_test(:)*SQRT( MAX(0.0_pr,ksts_out) )*(S2_ij(:,i)  )  !M_ij  (no delta_ij terms)
      END DO
    END IF

  END SUBROUTINE calc_keqn_momentum_closure

  ! Germano dynamic method for determining momentum closure terms 
  SUBROUTINE calc_keqn_momentum_closure_OPT( Leonard_out, Modeled_out, ksts_out, s_ij_traceless, u_int ) 
    USE equations_compressible 
    USE variable_mapping 
    IMPLICIT NONE
    REAL (pr), DIMENSION (nwlt,1:6), INTENT (OUT)   :: Leonard_out,Modeled_out 
    REAL (pr), DIMENSION (1:nwlt),   INTENT (OUT)   :: ksts_out
    REAL (pr), DIMENSION (nwlt,n_integrated), INTENT (IN)    :: u_int
    REAL (pr), DIMENSION (nwlt,1:6), INTENT (IN)             :: s_ij_traceless 

    REAL (pr), DIMENSION (1:nwlt,1:17)         :: filt_in  ! Increased size to allow for an extra field to filter
    !REAL (pr), DIMENSION (1:nwlt)             :: rho_test
    REAL (pr), DIMENSION (1:nwlt,1:6)         :: S2_ij, smod2

    INTEGER :: i,j, pos

    !Coefficient numerator/denominator calculated in SUBROUTINE GM_terms
    !-------------------------------------- Calculate Lij and Sij and Ksts-------------------------------------------------
    !ERIC: check all sign conventions       
    IF ( dim .NE. 3 ) CALL error('Can only calculate dynamic coefficients for dim .EQ. 3')

    ! Do all filtering at the same time

    !Test filter to calculate L
    filt_in(:,1) = u_int(:,n_den)                 !rho 
    filt_in(:,2) = MAX(0.0_pr,u_int(:,n_var_k))   !rho ksgs ! ERIC: clip after filter? 
    filt_in(:,3:5) = u_int(:,n_mom(1:3))          !rho*u

    ! Leonard terms
    filt_in(:,6)  = u_int(:,n_mom(1))*u_int(:,n_mom(1))/u_int(:,n_den)  !rho*u*u 
    filt_in(:,7)  = u_int(:,n_mom(2))*u_int(:,n_mom(2))/u_int(:,n_den)  !rho*v*v 
    filt_in(:,8)  = u_int(:,n_mom(3))*u_int(:,n_mom(3))/u_int(:,n_den)  !rho*w*w 
    filt_in(:,9)  = u_int(:,n_mom(1))*u_int(:,n_mom(2))/u_int(:,n_den)  !rho*u*v 
    filt_in(:,10) = u_int(:,n_mom(1))*u_int(:,n_mom(3))/u_int(:,n_den)  !rho*u*w 
    filt_in(:,11) = u_int(:,n_mom(2))*u_int(:,n_mom(3))/u_int(:,n_den)  !rho*v*w 
    pos = 11
    DO j = 1,6
      filt_in(:,j+pos) = u_int(:,n_den)*SQRT(MAX(0.0_pr,u_int(:,n_var_k)/u_int(:,n_den)))*(S_ij_traceless(:,j)  ) !rho sqrt(k) S_ij   
    END DO
    IF(deltaMij)  THEN  !Mirrored off of Guliano's code
      DO j=1,6
        filt_in(:,j+pos) = delta(:)*filt_in(:,j+pos)
      END DO
    END IF

    CALL mdl_filt_by_type(filt_in(:,1:6+pos),6+pos,TESTfilter)

    ! An intermediate quantity, this prevents function dedicated to Lij
    ! Favre average velocities, and compute Sij_test, Smod_test ERIC: (does this commute across differential operator)
    DO i=1,3
      Leonard_out(:,i) = filt_in(:,i+2)/filt_in(:,1)    ! ()/(rho)
    END DO
    CALL Sij( Leonard_out(:,1:3), nwlt, S2_ij, Smod2, .TRUE. ) !calculate both Sij* and |S*|, assume correct 

    !Ordering corresponds with Sij 
    Leonard_out(:,1) = -filt_in(:,3)*filt_in(:,3)/filt_in(:,1)  !(rho*u)(rho*u)/(rho)
    Leonard_out(:,2) = -filt_in(:,4)*filt_in(:,4)/filt_in(:,1)  !(rho*v)(rho*v)/(rho)
    Leonard_out(:,3) = -filt_in(:,5)*filt_in(:,5)/filt_in(:,1)  !(rho*w)(rho*w)/(rho)
    Leonard_out(:,4) = -filt_in(:,3)*filt_in(:,4)/filt_in(:,1)  !(rho*u)(rho*v)/(rho)
    Leonard_out(:,5) = -filt_in(:,3)*filt_in(:,5)/filt_in(:,1)  !(rho*u)(rho*w)/(rho)
    Leonard_out(:,6) = -filt_in(:,4)*filt_in(:,5)/filt_in(:,1)  !(rho*v)(rho*w)/(rho)
    
    DO j = 1,6
      Leonard_out(:,j) = Leonard_out(:,j) + filt_in(:,j+5)
    END DO
    !L_ij is determined, Verified 8/18/13 

    ! Modeled tensor for Germano equality
    ! Ksts
    ksts_out = (0.5_pr*SUM( Leonard_out(:,1:3), DIM=2 ) + filt_in(:,2))/filt_in(:,1)
    ksts_out = MAX(ksts_out,0.0_pr) 

    ! Lij and Mij traceless  ERIC:  traceless helper function?  
    DO i = 1,3
      Leonard_out(:,i) = Leonard_out(:,i) - SUM(Leonard_out(:,1:3),DIM=2) / 3.0_pr 
    END DO

    IF (deltaMij) THEN
      DO i=1,6
        Modeled_out(:,i) = filt_in(:,i+pos) - Mcoeff*delta(:)*filt_in(:,1)*SQRT( MAX(0.0_pr,ksts_out) )*(S2_ij(:,i)  )  !M_ij  2.0_pr factor not implicit in Cs
      END DO
    ELSE
      DO i=1,6
        Modeled_out(:,i) = filt_in(:,i+pos) - Mcoeff*filt_in(:,1)*SQRT( MAX(0.0_pr,ksts_out) )*(S2_ij(:,i)  )  !M_ij  (no delta_ij terms)
      END DO
    END IF

  END SUBROUTINE calc_keqn_momentum_closure_OPT


  ! Keqn: Kj and Nj, closure terms for dynamic coefficient [Pra_t, 1/Pra_t, C/Pra_t] 
  SUBROUTINE calc_keqn_thermal_closure( Leonard_out, Modeled_out, u_int, cv_field, ksts ) 
    USE equations_compressible 
    USE variable_mapping 
    IMPLICIT NONE
    REAL (pr), DIMENSION (nwlt,1:3), INTENT (OUT)   :: Leonard_out,Modeled_out 
    REAL (pr), DIMENSION (nwlt,n_integrated), INTENT (IN)    :: u_int
    REAL (pr), DIMENSION (1:nwlt), INTENT(IN)                :: cv_field, ksts 

    ! ERIC: make option
    INTEGER, PARAMETER:: meth = HIGH_ORDER

    REAL (pr), DIMENSION (1:nwlt,1:6)         :: filt_in  ! ERIC: need to account for Species 
    REAL (pr), DIMENSION (1:nwlt)             :: rho_test, tmptr, tmptr_test 
    REAL (pr), DIMENSION (2,nwlt,dim)         :: dTemp, d2Temp

    INTEGER :: i,j

    !Coefficient numerator/denominator calculated in SUBROUTINE GM_terms
    !-------------------------------------- Calculate Lij and Sij and Ksts-------------------------------------------------
    !ERIC: check all sign conventions       
    IF ( dim .NE. 3 ) CALL error('Can only calculate dynamic coefficients for dim .EQ. 3')

    filt_in(:,1) = u_int(:,n_den) 
    filt_in(:,2:4) = u_int(:,n_mom(1:3)) 
    filt_in(:,5) = u_int(:,n_eng) 
    CALL mdl_filt_by_type( filt_in(:,1:5),5,TESTfilter)

    ! ERIC: how to compute cp_field?  Filter?
    rho_test = filt_in(:,1)
    ! ERIC: F_compress
    tmptr      =  (u_int(:,n_eng) - 0.5_pr*SUM(u_int(:,n_mom(1:3))**2.0_pr,DIM=2)/u_int(:,n_den))/( cv_field * u_int(:,n_den) ) / F_compress 
    tmptr_test =  (filt_in(:,5)   - 0.5_pr*SUM(filt_in(:,2:4)**2.0_pr,DIM=2)/filt_in(:,1))/( cv_field * filt_in(:,1) ) / F_compress

    !Temperature derivatives
    filt_in(:,1) = Tmptr_test
    filt_in(:,2) = Tmptr
    CALL c_diff_fast(filt_in(:,1:2), dTemp(1:2,:,:), d2Temp(1:2,:,:), j_lev, nwlt, meth, 10, 2, 1, 2)

    !Filtering for N_k and K_k
    !K_k term
    filt_in(:,1) = u_int(:,n_mom(1))*Tmptr(:) !rho*u*T
    filt_in(:,2) = u_int(:,n_mom(2))*Tmptr(:) !rho*v*T
    filt_in(:,3) = u_int(:,n_mom(3))*Tmptr(:) !rho*w*T
    !N_k term: staggering to reduce filtering calls
    filt_in(:,4) = u_int(:,n_den) * SQRT(MAX(0.0_pr, u_int(:,n_var_k)/u_int(:,n_den)  )) * dTemp(2,:,1)    !rho*sqrt(k)* dT/dx_1
    filt_in(:,5) = u_int(:,n_den) * SQRT(MAX(0.0_pr, u_int(:,n_var_k)/u_int(:,n_den)  )) * dTemp(2,:,2)    !rho*sqrt(k)* dT/dx_2
    filt_in(:,6) = u_int(:,n_den) * SQRT(MAX(0.0_pr, u_int(:,n_var_k)/u_int(:,n_den)  )) * dTemp(2,:,3)    !rho*sqrt(k)* dT/dx_3

    IF(deltaMij) THEN
       DO j=4,6
          filt_in(:,j) = delta(:)*filt_in(:,j)
       END DO
    END IF

    CALL mdl_filt_by_type(filt_in(:,1:6),6,TESTfilter)

    Leonard_out(:,1) =  filt_in(:,1)  !(rho*u*T)
    Leonard_out(:,2) =  filt_in(:,2)  !(rho*v*T)
    Leonard_out(:,3) =  filt_in(:,3)  !(rho*w*T)
    IF(deltaMij) THEN
       Modeled_out(:,1) = -Mcoeff*delta(:)*rho_test*SQRT(MAX(0.0_pr, ksts ))*dTemp(1,:,1) + filt_in(:,4)  !M * (rho2) * sqrt(Ksts)*(dT/dx_1)-(rho*|S|*dT/dx_1) 
       Modeled_out(:,2) = -Mcoeff*delta(:)*rho_test*SQRT(MAX(0.0_pr, ksts ))*dTemp(1,:,2) + filt_in(:,5)  !M * (rho2) * sqrt(Ksts)*(dT/dx_2)-(rho*|S|*dT/dx_2) 
       Modeled_out(:,3) = -Mcoeff*delta(:)*rho_test*SQRT(MAX(0.0_pr, ksts ))*dTemp(1,:,3) + filt_in(:,6)  !M * (rho2) * sqrt(Ksts)*(dT/dx_3)-(rho*|S|*dT/dx_3) 
    ELSE
       Modeled_out(:,1) = -Mcoeff         *rho_test*SQRT(MAX(0.0_pr, ksts ))*dTemp(1,:,1) + filt_in(:,4)  !M * (rho2) * sqrt(Ksts)*(dT/dx_1)-(rho*|S|*dT/dx_1) 
       Modeled_out(:,2) = -Mcoeff         *rho_test*SQRT(MAX(0.0_pr, ksts ))*dTemp(1,:,2) + filt_in(:,5)  !M * (rho2) * sqrt(Ksts)*(dT/dx_1)-(rho*|S|*dT/dx_1) 
       Modeled_out(:,3) = -Mcoeff         *rho_test*SQRT(MAX(0.0_pr, ksts ))*dTemp(1,:,3) + filt_in(:,6)  !M * (rho2) * sqrt(Ksts)*(dT/dx_1)-(rho*|S|*dT/dx_1) 
    END IF

    !final terms for K_k
    filt_in(:,1) = u_int(:,n_mom(1))           !rho*u
    filt_in(:,2) = u_int(:,n_mom(2))           !rho*v
    filt_in(:,3) = u_int(:,n_mom(3))           !rho*w
    filt_in(:,4) = u_int(:,n_den)*Tmptr(:)     !rho*T
    CALL mdl_filt_by_type(filt_in(:,1:4),4,TESTfilter)
    Leonard_out(:,1) = Leonard_out(:,1) - filt_in(:,1)*Tmptr_test(:)  !(rho*u)(rho*T)/(rho2)
    Leonard_out(:,2) = Leonard_out(:,2) - filt_in(:,2)*Tmptr_test(:)  !(rho*v)(rho*T)/(rho2)
    Leonard_out(:,3) = Leonard_out(:,3) - filt_in(:,3)*Tmptr_test(:)  !(rho*w)(rho*T)/(rho2)
    !N_k and K_k are complete

  END SUBROUTINE calc_keqn_thermal_closure

  ! Triple correlation 
  SUBROUTINE calc_keqn_triple_corr_closure( Leonard_out, Modeled_out, u_int, ksts, d_ksgs_in ) 
    USE equations_compressible 
    USE variable_mapping 
    IMPLICIT NONE
    REAL (pr), DIMENSION (nwlt,1:3), INTENT (OUT)            :: Leonard_out,Modeled_out 
    REAL (pr), DIMENSION (nwlt,n_integrated), INTENT (IN)    :: u_int
    REAL (pr), DIMENSION (1:nwlt), INTENT(IN)                :: ksts 
    REAL (pr), DIMENSION (1:nwlt,1:dim), INTENT(IN)          :: d_ksgs_in 

    ! ERIC: make option
    INTEGER, PARAMETER:: meth = HIGH_ORDER

    REAL (pr), DIMENSION (1:nwlt,1:7)         :: filt_in  
    REAL (pr), DIMENSION (1:nwlt)             :: rho_test
    REAL (pr), DIMENSION (1,nwlt,dim)         :: dksts, d2ksts

    INTEGER :: idim

    !Coefficient numerator/denominator calculated in SUBROUTINE GM_terms
    !-------------------------------------- Calculate Lij and Sij and Ksts-------------------------------------------------
    IF ( dim .NE. 3 ) CALL error('Can only calculate dynamic coefficients for dim .EQ. 3')

    !Calculate W_j (Modeled term)
    filt_in(:,1) = ksts
    CALL c_diff_fast(filt_in(:,1), dksts(1,:,:), d2ksts(1,:,:), j_lev, nwlt, meth, 10, 1, 1, 1)

    filt_in(:,4) = u_int(:,n_den)
    IF (C_f_type .EQ. coeff_Ger_clip  .OR. C_f_type .EQ. coeff_Ger_dyn ) THEN  !Germano scaling, else Menon/Bardina scaling

      DO idim = 1,3
        filt_in(:,idim) = u_int(:,n_den)*SQRT(MAX(0.0_pr,u_int(:,n_var_k)/u_int(:,n_den) ))*d_ksgs_in(:,idim)  ! rho * sqrt(k_sgs) *d ksgs/dx
      END DO

      IF(deltaMij)  THEN  !include filter width in test filter (considering adaptive grid), Mirrored off of Guliano's code
        DO idim=1,3
          filt_in(:,idim) = delta(:)*filt_in(:,idim)                                          ! rho * delta *sqrt(k_sgs) *d ksgs/dx
        END DO
      END IF

      CALL mdl_filt_by_type(filt_in(:,1:4),4,TESTfilter)                                 ! [ rho * delta *sqrt(k_sgs) *d ksgs/dx ]
    ELSE
      CALL mdl_filt_by_type(filt_in(:,4),1,TESTfilter)                                 ! [ rho * delta *sqrt(k_sgs) *d ksgs/dx ]
      filt_in(:,1:3) = 0.0_pr
    END IF
    rho_test = filt_in(:,4)


    IF(deltaMij)  THEN  !include filter width in test filter (considering adaptive grid), Mirrored off of Guliano's code
      DO idim=1,3
        Modeled_out(:,idim) = rho_test*Mcoeff*delta(:)*SQRT(MAX(0.0_pr,ksts))*dksts(1,:,idim) - filt_in(:,idim)  ! Need to calc ksgs
      END DO
    ELSE
      DO idim=1,3
        Modeled_out(:,idim) = rho_test*Mcoeff         *SQRT(MAX(0.0_pr,ksts))*dksts(1,:,idim) - filt_in(:,idim)
      END DO
    END IF

    !Calculate G_j (Leonard_term)
    filt_in(:,4) = MAX(u_int(:,n_var_k),0.0_pr)
    DO idim = 1,3
      filt_in(:,4) = filt_in(:,4) + 0.5_pr*u_int(:,n_mom(idim))**2.0_pr/u_int(:,n_den)    !1/2 * (rho u_k u_k) = rho*k + 1/2 * rho u_k u_k
    END DO

    DO idim = 1,3
      filt_in(:,idim) = filt_in(:,4) * u_int(:,n_mom(idim))/ u_int(:,n_den)          ! 1/2*(rho u_k u_k)*(u_j)
      filt_in(:,4+idim) = u_int(:,n_mom(idim))/ u_int(:,n_den)                       ! (u_j) 
    END DO

    CALL mdl_filt_by_type(filt_in(:,1:4),4,TESTfilter)                ! [1/2*(rho u_k u_k)*(u_j)] , [ rho/2 u_k u_k]

    !ERIC:should total (molecular + eddy) viscosity be used in f_correlation (Menon doesn't use these residual terms)
    DO idim = 1,3
      !Leonard_out(:,j) = filt_in(:,idim) - uu_tilde*v_testfilt(:,j)                    !  [u_j] [ rho/2 u_k u_k] - [1/2*(rho u_k u_k)*(u_j)]
      Leonard_out(:,idim) = filt_in(:,idim) - filt_in(:,4)*filt_in(:,4+idim)                    !  [u_j] [ rho/2 u_k u_k] - [1/2*(rho u_k u_k)*(u_j)]
    END DO

  END SUBROUTINE calc_keqn_triple_corr_closure 

  ! SGS dissipation closure coefficients
  SUBROUTINE calc_keqn_dissipation_closure( Leonard_sol_out, Modeled_sol_out, Leonard_dilat_out, Modeled_dilat_out, u_int, ksts, vel_grad, Sij_traceless ) 
    USE equations_compressible 
    USE variable_mapping 
    IMPLICIT NONE
    REAL (pr), DIMENSION (nwlt), INTENT (OUT)                :: Leonard_sol_out, Modeled_sol_out, Leonard_dilat_out, Modeled_dilat_out 
    REAL (pr), DIMENSION (nwlt,n_integrated), INTENT (IN)    :: u_int
    REAL (pr), DIMENSION (1:nwlt), INTENT(IN)                :: ksts 
    REAL (pr), DIMENSION (1:dim,1:nwlt,1:dim), INTENT(IN)          :: vel_grad
    REAL (pr), DIMENSION (1:nwlt,1:6),         INTENT(IN)           :: Sij_traceless 

    ! ERIC: make option
    INTEGER, PARAMETER:: meth = HIGH_ORDER

    REAL (pr), DIMENSION (1:nwlt,1:6)           :: filt_in  
    REAL (pr), DIMENSION (1:dim,1:nwlt,1:dim)   :: vel_grad_testfilt, vel_grad_loc
    REAL (pr), DIMENSION (1:nwlt,1:6)           :: S2ij_traceless 
    REAL (pr), DIMENSION (1:nwlt)               :: rho_test, visc_loc, sos_loc

    INTEGER :: idim, jdim

    !Coefficient numerator/denominator calculated in SUBROUTINE GM_terms
    !-------------------------------------- Calculate Lij and Sij and Ksts-------------------------------------------------
    IF ( dim .NE. 3 ) CALL error('Can only calculate dynamic coefficients for dim .EQ. 3')

    DO idim = 1,3
      filt_in(:,idim) = u_int(:,n_mom(idim))/u_int(:,n_den)
    END DO
    filt_in(:,4) = u_int(:,n_den)
    CALL mdl_filt_by_type(filt_in(:,1:4),4,TESTfilter)
    rho_test(:) = filt_in(:,4)

    ! vel_grad_loc used as a dummy argument
    CALL c_diff_fast(filt_in(:,1:3), vel_grad_testfilt(1:3,:,:), vel_grad_loc(1:3,:,:), j_lev, nwlt, meth, 10, 3, 1, 3)

    CALL calcStrainRateTensors( vel_grad_testfilt(1:dim, :, 1:dim), nwlt, filt_in, TRACELESS = S2ij_traceless ) ! using filt_in as a dummy array

    visc_loc = calc_dynamic_viscosity( u_int )
    ! ERIC: note_we're using a local turb mach number instead of an average  (filter?)
    sos_loc = calc_speed_of_sound( u_int, nwlt, n_integrated ) 

    ! Be careful with the order, since the solonoidal dissipation uses in-place calculations on vel_grad and vel_grad_testfilt

    IF (C_diss_dilat_type .EQ. coeff_Ger_clip .OR. C_diss_dilat_type .EQ. coeff_Ger_dyn &
        .OR. C_diss_dilat_type .EQ. coeff_Bar_clip .OR. C_diss_dilat_type .EQ. coeff_Bar_dyn ) THEN
    
       ! Be aware of order, so that in-place calcs don't overwrite
       filt_in(:,1) = vel_grad(1,:,1) * vel_grad(1,:,1)
       filt_in(:,2) = vel_grad_testfilt(1,:,1) * vel_grad_testfilt(1,:,1)
       DO idim = 2,3
          filt_in(:,1) = filt_in(:,1) + vel_grad(idim,:,idim) * vel_grad(idim,:,idim)
          filt_in(:,2) = filt_in(:,2) + vel_grad_testfilt(idim,:,idim) * vel_grad_testfilt(idim,:,idim)
       END DO
       CALL mdl_filt_by_type(filt_in(:,1),1,TESTfilter)
       Leonard_dilat_out = visc_loc*5.0_pr/3.0_pr*( filt_in(:,1) - filt_in(:,2) )

       Modeled_dilat_out = 2.0_pr/(sos_loc*sos_loc)*rho_test*MAX(ksts,0.0_pr)**2.5_pr/Mcoeff
       IF(deltaMij) Modeled_dilat_out = Modeled_dilat_out/delta

       IF((C_diss_dilat_type .EQ. 2) .OR. (C_diss_dilat_type .EQ. 3)) THEN !Germano - like scaling, otherwise, Bardina (Chai and Mahesh 2012) 
          filt_in(:,1) = MAX(u_int(:,n_var_k),0.0_pr)**2.5_pr/u_int(:,n_den)**1.5_pr
          IF(deltaMij) filt_in(:,1) = filt_in(:,1)/delta
          CALL mdl_filt_by_type(filt_in(:,1),1,TESTfilter)
          Modeled_dilat_out = Modeled_dilat_out - 2.0_pr/(sos_loc*sos_loc)* filt_in(:,1)
       END IF

    END IF

    !-------------------------------------- Calculate diss sol Lij and Sij -------------------------------------------------
    

    IF (C_diss_sol_type .EQ. coeff_Ger_clip .OR. C_diss_sol_type .EQ. coeff_Ger_dyn &
        .OR. C_diss_sol_type .EQ. coeff_Bar_clip .OR. C_diss_sol_type .EQ. coeff_Bar_dyn ) THEN

        !Make traceless
        filt_in(:,1) = vel_grad(1,:,1)
        filt_in(:,2) = vel_grad_testfilt(1,:,1)
        DO idim = 1,3
          filt_in(:,1) = filt_in(:,1) + vel_grad(idim,:,idim)
          filt_in(:,2) = filt_in(:,2) + vel_grad_testfilt(idim,:,idim)
        END DO
        vel_grad_loc = vel_grad  ! ERIC: better way to do this?
        DO idim = 1,3
          ! Use In place to save memory
          vel_grad_loc(idim,:,idim)      = vel_grad(idim,:,idim)          - filt_in(:,1)/3.0_pr
          vel_grad_testfilt(idim,:,idim) = vel_grad_testfilt(idim,:,idim) - filt_in(:,2)/3.0_pr
        END DO

       filt_in(:,1) = Sij_traceless(:,1)*vel_grad_loc(1,:,1) + Sij_traceless(:,2)*vel_grad_loc(2,:,2)  + Sij_traceless(:,3)*vel_grad_loc(3,:,3) &
                + Sij_traceless(:,4)*(vel_grad_loc(1,:,2)+vel_grad_loc(2,:,1)) + Sij_traceless(:,5)*(vel_grad_loc(1,:,3)+vel_grad_loc(3,:,1))  &
                + Sij_traceless(:,6)*(vel_grad_loc(2,:,3)+vel_grad_loc(3,:,2))

       filt_in(:,1) = 2.0_pr*visc_loc *filt_in(:,1)
       CALL mdl_filt_by_type(filt_in(:,1),1,TESTfilter)

       !DO NOT FILTER 
       filt_in(:,2) = S2ij_traceless(:,1)*vel_grad_testfilt(1,:,1) + S2ij_traceless(:,2)*vel_grad_testfilt(2,:,2)  + S2ij_traceless(:,3)*vel_grad_testfilt(3,:,3) &
                + S2ij_traceless(:,4)*(vel_grad_testfilt(1,:,2)+vel_grad_testfilt(2,:,1)) + S2ij_traceless(:,5)*(vel_grad_testfilt(1,:,3)+vel_grad_testfilt(3,:,1)) &
                + S2ij_traceless(:,6)*(vel_grad_testfilt(2,:,3)+vel_grad_testfilt(3,:,2))
       filt_in(:,2) = 2.0_pr*visc_loc *filt_in(:,2)

       Leonard_sol_out = filt_in(:,1) - filt_in(:,2)

       Modeled_sol_out          = rho_test     * MAX(ksts,0.0_pr)**1.5_pr/Mcoeff
       IF(deltaMij) Modeled_sol_out = Modeled_sol_out/delta

       IF (C_diss_sol_type .EQ. coeff_Ger_clip .OR. C_diss_sol_type .EQ. coeff_Ger_dyn) THEN !Germano - like scaling, otherwise, Bardina/Menon (Chai and Mahesh 2012) 
          filt_in(:,1) = u_int(:,n_den) * MAX(u_int(:,n_var_k)/u_int(:,n_den),0.0_pr)**1.5_pr  !rho * ksgs^1.5
          IF(deltaMij) filt_in(:,1) = filt_in(:,1)/delta
          CALL mdl_filt_by_type(filt_in(:,1),1,TESTfilter)
          Modeled_sol_out = Modeled_sol_out - filt_in(:,1)
       END IF
    END IF


  END SUBROUTINE calc_keqn_dissipation_closure 



    ! ERIC: local helper function: wrap many of these into their own helper module
   SUBROUTINE calcStrainRateTensors( du, nlocal, s_ij, TRACELESS )
      IMPLICIT NONE
      INTEGER, INTENT(IN) :: nlocal
      REAL (pr), DIMENSION( 3,nlocal, 3), INTENT(IN) :: du 
      REAL (pr), DIMENSION(nlocal,6), INTENT(OUT) :: s_ij
      REAL (pr), DIMENSION(nlocal,6), OPTIONAL, INTENT(OUT) :: TRACELESS 

      REAL (pr), DIMENSION(nlocal) :: tmp_array
      INTEGER :: i, j, k 

      IF( dim .NE. 3 )  CALL error( 'ERROR in keqn_compressible module: CalcStrainRateTensor.  Only valid in 3D')

      k = 0
      DO i = 1, dim
        s_ij(:,i)      = du(i,:,i) ! s_ii
        DO j = i+1, dim
           k= k + 1
           s_ij(:,dim+k)      = 0.5_pr * ( du(i,:,j) + du(j,:,i) ) ! 0.5* (dui/dxj + dui/dxi)
        END DO
      END DO

      IF( PRESENT( TRACELESS )) THEN
        TRACELESS(:,1:6) = s_ij(:,1:6)
        tmp_array = SUM( S_ij(:,1:dim), DIM=2 ) / REAL(dim,pr) ! trace
        DO i = 1, dim 
          TRACELESS(:,i) = TRACELESS(:,i) - tmp_array ! makes it traceless
        END DO
      END IF
   END SUBROUTINE calcStrainRateTensors


   ! Tensor-vector muliplication with 3D symmetric tensor
   PURE FUNCTION calc_u_tau ( vel_loc, tau_ij_loc, nloc ) 
     IMPLICIT NONE 
     INTEGER, INTENT(IN) :: nloc
     REAL (pr), DIMENSION(1:nloc,1:6), INTENT(IN) :: tau_ij_loc
     REAL (pr), DIMENSION(1:nloc,1:3), INTENT(IN) :: vel_loc 
     REAL (pr), DIMENSION(1:nloc,1:3)             :: calc_u_tau
   
     INTEGER :: j  

     !tau_i1*ui = tau_11*u1 + tau_21*u2 + tau_31*u3
     calc_u_tau(:,1) = tau_ij_loc(:,1) * vel_loc(:,1) + tau_ij_loc(:,4) * vel_loc(:,2) + tau_ij_loc(:,5) * vel_loc(:,3) 

     !tau_i2*ui = tau_12*u1 + tau_22*u2 + tau_32*u3
     calc_u_tau(:,2) = tau_ij_loc(:,4) * vel_loc(:,1)  + tau_ij_loc(:,2) * vel_loc(:,2) + tau_ij_loc(:,6) * vel_loc(:,3)
     
     !tau_i3*ui = tau_13*u1 + tau_23*u2 + tau_33*u3
     calc_u_tau(:,3) = tau_ij_loc(:,5) * vel_loc(:,1)  + tau_ij_loc(:,6) * vel_loc(:,2) + tau_ij_loc(:,3) * vel_loc(:,3)

   END FUNCTION calc_u_tau 


   ! - tau_ij * S_ij
   ! Symmetric tensor contraction
   PURE FUNCTION calc_sgs_production ( tau_ij_loc, S_ij_loc, nloc ) 
     IMPLICIT NONE 
     INTEGER, INTENT(IN) :: nloc
     REAL (pr), DIMENSION(nloc,6), INTENT(IN) :: tau_ij_loc, S_ij_loc
     REAL (pr), DIMENSION(nloc)               :: calc_sgs_production 
   
     INTEGER :: j  
     
     calc_sgs_production(:) = - tau_ij_loc(:,1)*S_ij_loc(:,1)     
     DO j = 2,3
        calc_sgs_production(:) = calc_sgs_production(:) - tau_ij_loc(:,j)*S_ij_loc(:,j)     
     END DO
     DO j = 4,6 !symmetric tensor
        calc_sgs_production(:) = calc_sgs_production(:) - 2.0_pr*tau_ij_loc(:,j)*S_ij_loc(:,j)     
     END DO

   END FUNCTION calc_sgs_production


   ! Calculate the keqn modeled p_dilatation 
   FUNCTION calc_keqn_p_dilat ( grad_v, dp, nloc, meth ) 
     IMPLICIT NONE 
     INTEGER, INTENT(IN) :: nloc, meth
     REAL (pr), DIMENSION(dim,nloc,dim), INTENT(IN) :: grad_v 
     REAL (pr), DIMENSION(nloc,dim),     INTENT(IN) :: dp 
     REAL (pr), DIMENSION(nloc)               :: calc_keqn_p_dilat 

     REAL (pr), DIMENSION(nloc,1)       :: v
     REAL (pr), DIMENSION(1,nloc,dim) :: du, d2u 

   
     INTEGER :: idim 
    
     ! divergence of v 
     v(:,1) = grad_v(1,:,1) 
     DO idim = 2,3
       v(:,1)  = v(:,1) + grad_v(idim,:,idim)
     END DO
     !CALL print_extrema(v(:,1), nloc,'P_DILAT: div velocity')
     CALL c_diff_fast(v(:,1), du(1,:,:), d2u(1,:,:), j_lev, nloc, meth, 10, 1, 1, 1)
     calc_keqn_p_dilat(:) = C_pi(:)*dp(:,1)*du(1,:,1)
     DO idim = 2,3
       calc_keqn_p_dilat(:) = calc_keqn_p_dilat(:) + C_pi(:)*dp(:,idim)*du(1,:,idim)
     END DO
     IF(deltaMij) calc_keqn_p_dilat(:) = calc_keqn_p_dilat(:)*delta(:)*delta(:)
     !CALL print_extrema(calc_keqn_p_dilat, nloc,'P_DILAT: out')

   END FUNCTION calc_keqn_p_dilat 

   ! Contract 3D symmetric tensors 
   PURE FUNCTION symm_tensor_contract3D( tensor1, tensor2, nloc ) 
     IMPLICIT NONE 
     INTEGER, INTENT(IN) :: nloc
     REAL (pr), DIMENSION(nloc,6),     INTENT(IN) :: tensor1, tensor2 
     REAL (pr), DIMENSION(nloc)                   :: symm_tensor_contract3D 

      symm_tensor_contract3D(:) = tensor1(:,1)*tensor2(:,1) + tensor1(:,2)*tensor2(:,2) + tensor1(:,3)*tensor2(:,3) &
                + 2.0_pr*tensor1(:,4)*tensor2(:,4) +2.0_pr*tensor1(:,5)*tensor2(:,5)   +2.0_pr*tensor1(:,6)*tensor2(:,6)  

   END FUNCTION symm_tensor_contract3D

END MODULE SGS_compressible_keqn

