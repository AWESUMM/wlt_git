! Module to automatically generate and an optimized order mapping of variables
! in the solution arrays.  Provides a clean and robust interfaces for multiple
! modules to add variables.
MODULE variable_mapping
  USE pde
  USE parallel
  USE variable_name_vars
  IMPLICIT NONE

!  INTEGER ,PARAMETER :: u_variable_name_len = 32snames2
  CHARACTER (LEN=u_variable_name_len) , DIMENSION(:), ALLOCATABLE, PROTECTED :: u_variable_names, u_IC_names
  CHARACTER (LEN=10), PROTECTED ::  u_variable_names_fmt 

  ! external globals TODO: make protected once integrated into the modules
  INTEGER, PROTECTED  :: n_integrated !--number of equations integrated in time
  INTEGER, PROTECTED  :: n_var_additional !--number of variables not integrated in time (e.g. pressure, viscosity, etc.)
  INTEGER, PROTECTED  :: n_var !-- total number of component variables
  INTEGER, PROTECTED  :: n_var_adapt_max !-- number of last component variables for which the code adapts.
  INTEGER, PROTECTED  :: n_var_exact !--number of variables with exact solution available

  LOGICAL, DIMENSION(:,:), ALLOCATABLE, PROTECTED  :: n_var_adapt       ! Logical map into Variables in u(,) used in 
                                                            ! wavelet adaptation
                                                            ! thresholding to determine new adaptive grid

  LOGICAL, DIMENSION(:,:), ALLOCATABLE, PROTECTED  :: n_var_interpolate ! Logical map into Variables in u(,)
                                                            ! that must be interpolated to
                                                            ! the new grid each time step.

  LOGICAL, DIMENSION(:,:), ALLOCATABLE, PROTECTED  :: n_var_exact_soln  ! Logical map into Variables in u(,)
                                                            ! that should be checked against an exact solution

  LOGICAL, DIMENSION(:), ALLOCATABLE, PROTECTED  ::  n_var_save         ! Logical map into Variables in u(,)
                                                            ! that are saved to restart/result file

  LOGICAL, DIMENSION(:), ALLOCATABLE, PROTECTED  ::  n_var_req_restart  ! Logical map into Variables in u(,)
                                                            ! that are required for a restart from a result file

  ! ERIC: could these just be moved to the hyperbolic module?
  LOGICAL, DIMENSION(:), ALLOCATABLE :: n_var_hyper        ! Logical map into Variables in u(,)
                                                           ! that should be used for calculation of numerical
                                                           ! viscosity if hypermodel == 1 

  LOGICAL, DIMENSION(:), ALLOCATABLE :: n_var_hyper_active ! Logical map into Variables in u(,)
                                                           ! that indicates for which evolution equations apply numerical
                                                           ! viscosity if hypermodel == 1 
  ! These are indices that map the locations of variables at each time step TODO: make protected.  How to mesh with io_3d? 
  ! ERIC: check these
  !INTEGER, DIMENSION(:), ALLOCATABLE :: n_var_index

  !INTEGER, DIMENSION(:), ALLOCATABLE, PROTECTED :: n_var_wlt_fmly ! wavelet transform family for each variable


  ! Variable register
  TYPE, PRIVATE :: VARIABLE_REGISTER 
    CHARACTER (LEN=u_variable_name_len) :: var_name     ! Variable name
    CHARACTER (LEN=u_variable_name_len) :: OPT_var_IC_name  ! OPTIONAL: variable name to map from for restart from IC
    LOGICAL :: integrated
    LOGICAL :: adapt(0:1)
    LOGICAL :: interpolate(0:1)
    LOGICAL :: exact(0:1)
    LOGICAL :: saved
    LOGICAL :: req_restart
    LOGICAL :: front_load          ! Send the variable to the front of its resepected block
  END TYPE VARIABLE_REGISTER

  ! Private variables for registering variables
  TYPE(VARIABLE_REGISTER), ALLOCATABLE, DIMENSION(:), PRIVATE :: variableRegisterArray
  INTEGER, PRIVATE :: n_vars_registered = 0   ! Number of variables registered in the array
  INTEGER, PRIVATE :: array_size = 0          ! Total allocated array size


  LOGICAL, PRIVATE :: indices_ready = .FALSE.  ! Flag determining the current state of the mapping, if new variables are being accepted

  PUBLIC :: &
    register_var, &              ! Register a new AWCM variable
    setup_mapping, &             ! Setup the mappings and arrays and lock variable registration
    get_index, &                 ! Get the corresponding index for a variable
    set_variable_name_format, &  ! ensure that the variable_name_fmt is set correctly
    set_adapt, &                 ! Override, to be able to adjust adapt flags on the fly 
    set_interpolate, &           ! Override, to be able to adjust interpolate flags on the fly 
    set_exact, &                 ! Override, to be able to adjust if an equation has an exact solution 
    reset_mapping, &             ! Reset the mapping.  ERIC: should this be exposed, or do we enforce single reading? 
    print_variable_registery, &  ! Diagnostic
    get_n_var                    ! Returns the total number of variables
    

  PRIVATE :: &
    check_size, &        ! Check the size of the variable register array and re-allocate as necessary
    search_registery, &  ! search the variable registery array by name for a given variable 
    sort_registery,       & ! sort the registry array for optimized interaction with the database 
    alloc_variable_mappings, &
    check_setup_pde, &  
    reset_variable_mapping, &
    reset_registry

CONTAINS

  ! Register a new AWCM variable.  The mapping are formed from the registry
  SUBROUTINE register_var( var_name, integrated, adapt, interpolate, exact, saved, req_restart, FRONT_LOAD, IC_MAP_NAME )
    USE error_handling
    IMPLICIT NONE
    CHARACTER (LEN=*), INTENT(IN) :: var_name
    LOGICAL, INTENT(IN) :: integrated, adapt(0:1), interpolate(0:1), exact(0:1), saved, req_restart
    LOGICAL,           OPTIONAL, INTENT(IN) :: FRONT_LOAD
    CHARACTER (LEN=*), OPTIONAL, INTENT(IN) :: IC_MAP_NAME 

    LOGICAL :: do_front_load

    do_front_load = .FALSE.
    IF( PRESENT(FRONT_LOAD) ) do_front_load = FRONT_LOAD



    ! Ensure that the mapping has not already been set
    IF( indices_ready ) CALL error('WARNING: AWCM variable mappings have already been set, so no new variables can be registered') 

    IF( PRESENT( IC_MAP_NAME ) ) THEN ! Avoid conflicting variable names with IC mapped names
      IF( search_registery( IC_MAP_NAME ) .NE. 0 ) THEN 
        CALL error('IC_MAP_NAME: Variable name has already been registered.  Check for errors or pick a unique name. ', IC_MAP_NAME ) 
      END IF
      IF( search_registery( IC_MAP_NAME, IC_MAP = .TRUE. ) .NE. 0 &
        .OR.  TRIM(ADJUSTL(var_name)) .EQ. TRIM(ADJUSTL(''))   ) THEN  ! Only valid mapped names are not empty 
print *, search_registery( IC_MAP_NAME, IC_MAP = .TRUE. )
        CALL error('IC_MAP_NAME: IC mapping name has already been registered.  Check for errors or pick a unique name. ', IC_MAP_NAME) 
      END IF
    END IF

    IF( search_registery( var_name ) .EQ. 0 ) THEN  ! variable name currently not registered
 
      CALL check_size

      ! make format for writing 
      WRITE(u_variable_names_fmt,  '( ''( A'', I3.3 , '')'' )') u_variable_name_len

      ! put a new variable in the register array
      ! TODO: make sure that adapted are also interpolated, etc.
      n_vars_registered = n_vars_registered + 1
      WRITE(variableRegisterArray(n_vars_registered)%var_name, u_variable_names_fmt ) var_name 
      IF( PRESENT(IC_MAP_NAME) ) THEN
         WRITE(variableRegisterArray(n_vars_registered)%OPT_var_IC_name, u_variable_names_fmt ) IC_MAP_NAME 
      ELSE
         WRITE(variableRegisterArray(n_vars_registered)%OPT_var_IC_name, u_variable_names_fmt ) ''
      END IF
      variableRegisterArray(n_vars_registered)%integrated       =     integrated
      variableRegisterArray(n_vars_registered)%adapt(0:1)       =     adapt(0:1)
      variableRegisterArray(n_vars_registered)%interpolate(0:1) =     interpolate(0:1)
      variableRegisterArray(n_vars_registered)%exact(0:1)       =     exact(0:1)
      variableRegisterArray(n_vars_registered)%saved            =     saved
      variableRegisterArray(n_vars_registered)%req_restart      =     req_restart
      variableRegisterArray(n_vars_registered)%front_load       =     do_front_load 
    ELSE
      CALL error('WARNING: Variable name has already been registered.  Check for errors or pick a unique name. ', var_name ) 
    END IF

  END SUBROUTINE register_var

  ! Get the index corresponding to a variable.  Leading and trailing whitespace is ignored
  FUNCTION get_index( var_name )
    USE error_handling
    IMPLICIT NONE
    INTEGER :: get_index
    CHARACTER (LEN=*), INTENT(IN) :: var_name
    

    ! Ensure that the mapping has been set
    IF( .NOT. indices_ready ) CALL error( 'AWCM variable mappings have not yet been set, so the indices are currently undetermined' )

    get_index = search_registery( var_name )

  END FUNCTION get_index

  ! Accessor for overriding current map to manually set or modify adapt flags
  SUBROUTINE set_adapt( i_var, adapt ) 
    USE error_handling
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: i_var        ! Variable index to change  
    LOGICAL, INTENT(IN) :: adapt(0:1)   ! Flag to change to

    !! Bounds and validity checking
    IF( .NOT. indices_ready ) CALL error('WARNING: Cannot override adapt.  Variable mapping not set up yet') 
    IF( i_var .LT. 1 .OR. i_var .GT. n_var ) CALL error('WARNING: Cannot override adapt: index out of bounds') 

    n_var_adapt(i_var,0:1)          = adapt 

  END SUBROUTINE set_adapt

  ! Helper function to make sure that the variable_names_fmt ( a protected variable ) has been set
  SUBROUTINE set_variable_name_format()
    USE variable_name_vars
    IMPLICIT NONE
    WRITE (u_variable_names_fmt,  '( ''( A'', I3.3 , '')'' )') u_variable_name_len
  END SUBROUTINE set_variable_name_format

  ! Accessor for overriding current map to manually set or modify interpolate flags
  SUBROUTINE set_interpolate( i_var, interpolate ) 
    USE error_handling
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: i_var        ! Variable index to change  
    LOGICAL, INTENT(IN) :: interpolate(0:1)   ! Flag to change to

    !! Bounds and validity checking
    IF( .NOT. indices_ready ) CALL error('WARNING: Cannot override interpolate.  Variable mapping not set up yet') 
    IF( i_var .LT. 1 .OR. i_var .GT. n_var ) CALL error('WARNING: Cannot override interpolate: index out of bounds') 

    n_var_interpolate(i_var,0:1)          = interpolate 

  END SUBROUTINE set_interpolate

  ! Accessor for overriding current map to manually set or modify exact flags
  SUBROUTINE set_exact( i_var, exact ) 
    USE error_handling
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: i_var        ! Variable index to change  
    LOGICAL, INTENT(IN) :: exact(0:1)   ! Flag to change to

    !! Bounds and validity checking
    IF( .NOT. indices_ready ) CALL error('WARNING: Cannot override exact_soln.  Variable mapping not set up yet') 
    IF( i_var .LT. 1 .OR. i_var .GT. n_var ) CALL error('WARNING: Cannot override interpolate: index out of bounds') 

    n_var_exact_soln(i_var,0:1)          = exact 
    n_var_exact = MAX( COUNT(n_var_exact_soln(:,0)),COUNT(n_var_exact_soln(:,1)) )

  END SUBROUTINE set_exact


  ! Sort the registry (Stable sort).  Variables with similar attributes are grouped together for proper functioning of the code.
  ! Within each group, variables marked for front loading are moved to the initial spaces (Stable sort).
  !   1) Integrated
  !   1) Adapted 
  !   2) Interpolated 
  !   3) Non-interpolated
  SUBROUTINE sort_registery() 
    USE error_handling
    IMPLICIT NONE
    TYPE(VARIABLE_REGISTER), ALLOCATABLE, DIMENSION(:) :: sorted_registers
    INTEGER :: i, j, block_start
    INTEGER :: n_var_next, n_var_backfill
    INTEGER :: block_size(1:4)

    ! Sort the registered variables  ! TODO: make this a stable sort.  The backfilling is reversing the order
    ALLOCATE( sorted_registers( n_vars_registered ) )

    ! Separate integrated from additional
    n_var_next = 1
    n_var_backfill = n_vars_registered
    DO i = 1,n_vars_registered
      IF ( variableRegisterArray(i)%integrated ) THEN
        sorted_registers(n_var_next) = variableRegisterArray(i)  
        n_var_next = n_var_next + 1 
      ELSE
        sorted_registers(n_var_backfill) = variableRegisterArray(i)  
        n_var_backfill = n_var_backfill - 1
      END IF    
    END DO
    block_size(1) = n_var_next - 1 

    variableRegisterArray(1:n_vars_registered) = sorted_registers(1:n_vars_registered)  ! Copy back
    DO i = n_var_next,n_vars_registered
      variableRegisterArray(i) = sorted_registers(n_vars_registered - i + n_var_next)  ! correct order from backfill 
    END DO

    ! sort adapted from non-adapted additional
    n_var_backfill = n_vars_registered
    DO i = n_var_next,n_vars_registered
      IF ( COUNT( variableRegisterArray(i)%adapt(0:1) ) .GT. 0 ) THEN
        sorted_registers(n_var_next) = variableRegisterArray(i)  
        n_var_next = n_var_next + 1 
      ELSE
        sorted_registers(n_var_backfill) = variableRegisterArray(i)  
        n_var_backfill = n_var_backfill - 1
      END IF    
    END DO
    variableRegisterArray(1:n_vars_registered) = sorted_registers(1:n_vars_registered)  ! Copy back
    DO i = n_var_next,n_vars_registered
      variableRegisterArray(i) = sorted_registers(n_vars_registered - i + n_var_next)  ! correct order from backfill 
    END DO
    block_size(2) = n_var_next - 1 - SUM( block_size(1:1) )               ! Calculate number of adapted vars

    ! sort interpolated from non-interpolated additional
    n_var_backfill = n_vars_registered
    DO i = n_var_next,n_vars_registered
      IF ( COUNT( variableRegisterArray(i)%interpolate(0:1) ) .GT. 0 ) THEN
        sorted_registers(n_var_next) = variableRegisterArray(i)  
        n_var_next = n_var_next + 1 
      ELSE
        sorted_registers(n_var_backfill) = variableRegisterArray(i)  
        n_var_backfill = n_var_backfill - 1
      END IF    
    END DO
    variableRegisterArray(1:n_vars_registered) = sorted_registers(1:n_vars_registered)  ! Copy back
    DO i = n_var_next,n_vars_registered
      variableRegisterArray(i) = sorted_registers(n_vars_registered - i + n_var_next)  ! correct order from backfill 
    END DO
    block_size(3) = n_var_next - 1 - SUM( block_size(1:2) )               ! Calculate number of adapted vars

    block_size(4) = n_vars_registered - n_var_next + 1                    ! Final block of additional vars

    ! Check
    IF( SUM(block_size(1:4)) .NE. n_vars_registered ) CALL error('ERROR: failed counts in sort registry')

    ! Front load each section by flag
    block_start = 1
    n_var_next = 1
    DO j = 1,4
      DO i = block_start, block_start + block_size(j) - 1
        IF ( variableRegisterArray(i)%front_load ) THEN
          sorted_registers(n_var_next) = variableRegisterArray(i)  
          n_var_next = n_var_next + 1 
        END IF    
      END DO
      DO i = block_start, block_start + block_size(j) - 1
        IF ( .NOT. variableRegisterArray(i)%front_load ) THEN
          sorted_registers(n_var_next) = variableRegisterArray(i)  
          n_var_next = n_var_next + 1 
        END IF    
      END DO
      block_start = block_start + block_size(j)
    END DO
    variableRegisterArray(1:n_vars_registered) = sorted_registers(1:n_vars_registered)  ! Copy back

  END SUBROUTINE sort_registery

  ! Setup the mappings and allocate all AWCM adaptation/save arrays
  ! Checks are also performed
  SUBROUTINE setup_mapping
    USE error_handling
    IMPLICIT NONE
    INTEGER :: i
    
    
    IF ( indices_ready ) THEN
       CALL error('WARNING: AWCM variable mappings have already been setup, cannot do a second time') 
    ELSE
       ! First, sort the registry into proper order for mappings    
       CALL sort_registery() 

       ! Get counts to allocate mappings and set globals
       n_var = n_vars_registered
       n_integrated = 0
       n_var_additional = 0
       n_var_exact = 0
       DO i = 1,n_vars_registered
         IF ( variableRegisterArray(i)%integrated ) THEN
           n_integrated = n_integrated + 1
         ELSE
           n_var_additional = n_var_additional + 1 
         END IF
         IF ( COUNT( variableRegisterArray(i)%exact(0:1) ) .GT. 0 ) THEN
           n_var_exact = n_var_exact + 1
         END IF
       END DO

       ! Allocate the mappings
       CALL alloc_variable_mappings

       ! Initialize the mappings
       n_var_adapt(:,0:1)          = .FALSE. 
       n_var_interpolate(:,0:1)    = .FALSE.
       n_var_exact_soln(:,0:1)     = .FALSE. 
       n_var_save                  = .FALSE. 
       n_var_req_restart           = .FALSE. 

       ! Set up the maps from the sorted registry
       DO i = 1,n_vars_registered
          WRITE (u_variable_names(i), u_variable_names_fmt) ADJUSTL(variableRegisterArray(i)%var_name)
          WRITE (u_IC_names(i), u_variable_names_fmt) ADJUSTL(variableRegisterArray(i)%OPT_var_IC_name)
          n_var_adapt(i,0:1)          = variableRegisterArray(i)%adapt 
          n_var_interpolate(i,0:1)    = variableRegisterArray(i)%interpolate
          n_var_exact_soln(i,0:1)     = variableRegisterArray(i)%exact
          n_var_save(i)               = variableRegisterArray(i)%saved
          n_var_req_restart(i)        = variableRegisterArray(i)%req_restart
       END DO

       n_var_exact = MAX( COUNT(n_var_exact_soln(:,0)),COUNT(n_var_exact_soln(:,1)) )

       ! Run some checks
       CALL check_setup_pde
       
    END IF
    ! Lock in the variables
    indices_ready = .TRUE.
    
  END SUBROUTINE setup_mapping

  ! Helper function to check the size of the variable register array and re-allocate as necessary
  SUBROUTINE check_size()
    IMPLICIT NONE
    TYPE(VARIABLE_REGISTER), ALLOCATABLE, DIMENSION(:) :: tmpRegisters
    INTEGER, PARAMETER :: grow_size = 10      ! Number of elements to add when growing array


    ! If the array is full, increase the size
    IF( n_vars_registered .EQ. array_size ) THEN 

      IF( ALLOCATED( variableRegisterArray ) ) THEN
        IF( ALLOCATED( tmpRegisters ) ) DEALLOCATE( tmpRegisters )
        ALLOCATE( tmpRegisters( array_size + grow_size ) )

        tmpRegisters(1:array_size) = variableRegisterArray(1:array_size)

        DEALLOCATE( variableRegisterArray ) 
        ALLOCATE( variableRegisterArray( array_size + grow_size ) )

        variableRegisterArray(1:array_size) =tmpRegisters(1:array_size)
        array_size = array_size + grow_size
      ELSE ! initial allocation
        ALLOCATE( variableRegisterArray( array_size + grow_size ) )
        array_size = array_size + grow_size
      END IF

    END IF

  END SUBROUTINE

  ! Search the register array by name for a given variable.  Return zero ('0') if variable not found 
  FUNCTION search_registery( var_name, IC_MAP )
    IMPLICIT NONE
    CHARACTER (LEN=*), INTENT(IN) :: var_name
    LOGICAL, OPTIONAL, INTENT(IN) :: IC_MAP
    INTEGER :: search_registery

    LOGICAL :: search_ic_map
    INTEGER :: i

    search_ic_map = .FALSE.
    IF( PRESENT( IC_MAP ) ) search_ic_map = IC_MAP

    search_registery = 0  ! Default value indicating failure
    ! Do a linear search through all vars
    DO i = 1, n_vars_registered
      IF( search_ic_map ) THEN ! Search for name in the IC mapping variables
        IF( TRIM(ADJUSTL(var_name)) .EQ. TRIM(ADJUSTL(variableRegisterArray(i)%OPT_var_ic_name)) ) THEN  ! TODO: check comparing of strings, and how to deal with whitespace
          search_registery = i
          EXIT
        END IF
      ELSE ! Search the variable names
        IF( TRIM(ADJUSTL(var_name)) .EQ. TRIM(ADJUSTL(variableRegisterArray(i)%var_name)) ) THEN  ! TODO: check comparing of strings, and how to deal with whitespace
          search_registery = i
          EXIT
        END IF
      END IF
    END DO

  END FUNCTION

  ! Allocate the variable mappings
  SUBROUTINE alloc_variable_mappings
    use user_case_db
    USE pde
    USE debug_vars
    USE penalization
    USE wlt_vars
    USE unused
    IMPLICIT NONE

!!$    n_var_time_levels = n_integrated !no longer in use, set up to the correct value for backward compatibility        

    ! Spatial Adaptive Epsilon
    IF (verb_level.GT.1) THEN
       PRINT *, 'n_var               (( alloc_variable_mappings  )) ::::  ',    n_var 
       PRINT *, 'n_var_additional    (( alloc_variable_mappings  )) ::::  ',    n_var_additional 
    END IF

    ! ERIC: check those that are not owned by this module? 
    IF( .NOT. ALLOCATED(n_var_adapt) )        ALLOCATE (n_var_adapt(1:n_var,0:1))
    IF( .NOT. ALLOCATED(n_var_interpolate) )  ALLOCATE (n_var_interpolate(1:n_var,0:1))
    IF( .NOT. ALLOCATED(n_var_exact_soln) )   ALLOCATE (n_var_exact_soln(1:n_var,0:1))
    IF( .NOT. ALLOCATED(n_var_save) )         ALLOCATE (n_var_save(1:n_var))
    IF( .NOT. ALLOCATED(n_var_index) )        ALLOCATE (n_var_index(1:n_var))
    IF( .NOT. ALLOCATED(n_var_req_restart) )  ALLOCATE (n_var_req_restart(1:n_var))
    IF( .NOT. ALLOCATED(n_var_wlt_fmly) )     ALLOCATE (n_var_wlt_fmly(1:n_var))
    n_var_wlt_fmly = 0
    n_var_adapt = .FALSE. ;n_var_interpolate = .FALSE. ; n_var_exact_soln = .FALSE. ; n_var_save = .FALSE.
    n_var_req_restart = .TRUE. ! require all by default
    n_var_index = 0 
    IF( .NOT. ALLOCATED(u_variable_names) )   ALLOCATE (u_variable_names(1:n_var) )
    IF( .NOT. ALLOCATED(u_IC_names) )   ALLOCATE (u_IC_names(1:n_var) )
    u_variable_names(:) = ' '
    u_IC_names(:) = ' '

    IF ( ALLOCATED( scaleCoeff ) ) DEALLOCATE( scaleCoeff )
    ALLOCATE ( scaleCoeff(1:n_var) )
    scaleCoeff = 1.0_pr


  END SUBROUTINE alloc_variable_mappings

  !
  ! check that the pde defined by the user can be solved by this code
  !
  SUBROUTINE check_setup_pde
    USE precision
    USE share_consts
    USE pde
    USE user_case_db         ! n_var_db
    USE parallel             ! par_rank
    USE debug_vars           ! ignore_warnings
    IMPLICIT NONE
    INTEGER chk_n_var_db, i, ii
    
    
    n_var_save = (n_var_req_restart .OR. n_var_save)
    DO i = 1, n_var
       n_var_interpolate(i,0)=n_var_interpolate(i,0) .OR. n_var_save(i)
       n_var_interpolate(i,1)=n_var_interpolate(i,1) .OR. n_var_save(i)
    END DO


    !Find the maximum index of the variable for which the code adaptes: n_var_adapt_max
    n_var_adapt_max = 0
    DO i = 1,n_var
       IF( COUNT( n_var_adapt(i,0:1) ) > 0 ) THEN
          n_var_adapt_max = i 
       END IF
    END DO

    ! Set the variable that is not used for adaptation, and is not interpolated, but is lower than n_var_adapt_max to be interpolated variable to avoid logical conflicts
    ! in significant_wlt
    DO ii=0,1
     DO i = 1,n_var_adapt_max
        IF( .NOT.(n_var_adapt(i,ii) .OR. n_var_interpolate(i,ii)) ) THEN
           n_var_interpolate(i,ii) = .TRUE.
           IF ( (par_rank.EQ.0) .AND. .NOT. ignore_warnings ) THEN
              PRINT *, '********************************************************************************'
              IF(ii == 0) PRINT *, ' WARNING: non-optimal placement of the variable for initial condition.          '
              IF(ii == 1) PRINT *, ' WARNING: non-optimal placement of the variable for time integration.           '
              PRINT *, ' variable ', i, ' is not used for adaptation                       '
              PRINT *, ' For optimal placemnt non-adaptive additional variables should be indexed       '
              PRINT *, ' after ALL additional variables used for grid adaptation.                       '
              PRINT *, '********************************************************************************'
           END IF
        END IF
     END DO
    END DO

    !
    ! CHeck that the user case module set n_var_db =
    !  Count( Union (n_var_adapt, n_var_interpolate, n_var_save))
    !
    chk_n_var_db = 0
    DO i = 1,n_var
       IF( ANY( n_var_adapt(i,0:1) ) .OR. ANY( n_var_interpolate(i,0:1) ) .OR. n_var_save(i)   ) THEN
          chk_n_var_db = chk_n_var_db +1 
       END IF
    END DO

    IF( chk_n_var_db > n_var_db ) THEN
       IF (par_rank.EQ.0) THEN
          PRINT *, ''
          PRINT *, '*******************************************************************************'
          PRINT *, ' ERROR: n_var_db  < COUNT( Union( n_var_adapt, n_var_interpolate))'
          PRINT *, ' This is set  as a parameter in [user_case].PARAMS.f90 so that'
          PRINT *, ' the u array in the db node can be statically allocated to the right length.'
          PRINT *, ' Edit [user_case].PARAMS.f90 and make n_var_db >= ', chk_n_var_db
          PRINT *, ' according to the values set for n_var_adapt, n_var_interpolate in the current'
          PRINT *, ' case in user_setup_pde (). Exiting now...'
          PRINT *, '*******************************************************************************'
          PRINT *, ''
       END IF
       CALL parallel_finalize; STOP
    END IF

    
    !
    ! Here we must check that n_var <= n_var_db
    ! n_var_db is set as a parameter in user_case_db_defs.f90
    ! so that the u array in the db node can be statically allocated to the right length
    !
    IF( n_var_adapt_max > n_var_db ) THEN
       IF ( par_rank.EQ.0 ) THEN
          PRINT *, ''
          PRINT *, '*******************************************************************************'
          PRINT *, 'ERROR: n_var_adapt_max > n_var_db'
          PRINT *, 'n_var_db is set as a parameter in user_case.PARAMS.f90 so that'
          PRINT *, 'the u array in the db node can be statically allocated to the right length.'
          PRINT *, 'Edit [user_case].PARAMS.f90 so n_var_db matches the value setup in the current'
          PRINT *, 'case in user_setup_pde (). Currently n_var_adapt_max = ', n_var_adapt_max
          PRINT *, 'n_var_db = ', n_var_db
          PRINT *, 'We will Exit now...'
          PRINT *, '*******************************************************************************'
          PRINT *, ''
       END IF
       CALL parallel_finalize; STOP
    END IF
    
    !
    ! Warn user if n_var < n_var_db .
    ! There is a reason this can happen because the derivatives can use more data_base slots then the
    ! total number of variables. But if this is not the case then memory is just being wasted'
    !
    IF( n_var < n_var_db ) THEN
       IF ( (par_rank.EQ.0) .AND. .NOT. ignore_warnings ) THEN
          PRINT *, ''
          PRINT *, '*******************************************************************************'
          PRINT *, 'WARNING: Possible ineficient use of memory, n_var < n_var_db'
          PRINT *, 'There is a reason this can happen because the derivatives can use more data_base slots than the'
          PRINT *, 'total number of variables. But if this is not the case then memory is just being wasted'
          PRINT *, ''
          PRINT *, ''
          PRINT *, 'n_var_db is set as a parameter in [user_case].PARAMS.f90 so that'
          PRINT *, 'the u array in the db node can be statically allocated to the right length.'
          PRINT *, 'To change this edit [user_case].PARAMS.f90 so n_var_db matches the value setup in the current'
          PRINT *, 'case in user_setup_pde (). Currently n_var = ', n_var
          PRINT *, 'n_var_db = ', n_var_db
          PRINT *, ''
          PRINT *, '*******************************************************************************'
          PRINT *, ''
       END IF
    END IF
    
    !Checks sanity of n_var_save and n_var_req_restart


  END SUBROUTINE check_setup_pde

  ! Clean slate approach to get around issues in io_3d
  SUBROUTINE reset_mapping( )
    IMPLICIT NONE

    CALL reset_registry
    CALL reset_variable_mapping
    indices_ready = .FALSE.

  END SUBROUTINE reset_mapping 

  ! Clean slate approach to get around issues in io_3d
  SUBROUTINE reset_registry( )
    IMPLICIT NONE
    IF( ALLOCATED( variableRegisterArray ) ) DEALLOCATE( variableRegisterArray ) 
    n_vars_registered = 0 
    array_size = 0 
  END SUBROUTINE reset_registry

  ! Helper function to reset the variable mappings
  SUBROUTINE reset_variable_mapping( )
    USE unused
    IMPLICIT NONE

    n_var = 0
    n_integrated = 0
    n_var_additional = 0

    n_var_Epsilon  = 0      ! will get reset in alloc_variable_mappings 
    n_var_EpsilonEvol  = 0  ! will get reset in alloc_variable_mappings ERIC: ? 

!!$    ! unsure
!!$    n_var_time_levels = 0 

    IF( ALLOCATED (n_var_adapt)       ) DEALLOCATE (n_var_adapt)
    IF( ALLOCATED (n_var_interpolate) ) DEALLOCATE (n_var_interpolate)
    IF( ALLOCATED (n_var_exact_soln)  ) DEALLOCATE (n_var_exact_soln)
    IF( ALLOCATED (n_var_save)        ) DEALLOCATE (n_var_save)
    IF( ALLOCATED (n_var_index)       ) DEALLOCATE (n_var_index)
    IF( ALLOCATED (n_var_req_restart) ) DEALLOCATE (n_var_req_restart)
    IF( ALLOCATED (n_var_wlt_fmly)    ) DEALLOCATE (n_var_wlt_fmly)
    IF( ALLOCATED (u_variable_names)  ) DEALLOCATE (u_variable_names)
    IF( ALLOCATED (u_IC_names)  ) DEALLOCATE (u_IC_names)

    ! Spatial Adaptive Epsilon
    IF( ALLOCATED (n_var_adapt_eps)   ) DEALLOCATE ( n_var_adapt_eps )

  END SUBROUTINE reset_variable_mapping 

  ! Diagnostic, to print out the current variable register array
  !     FULL: Print out all array information
  SUBROUTINE print_variable_registery( FULL )
    IMPLICIT NONE
    LOGICAL, OPTIONAL, INTENT(IN) :: FULL

    LOGICAL :: print_full = .FALSE.
    INTEGER :: i

    IF ( PRESENT(FULL) ) print_full = FULL

    IF ( par_rank .EQ. 0 ) THEN
      PRINT *, '*************** Variables Registered ***************'
      IF( print_full ) WRITE(*,'(A32, 7A6, A12 )') 'Name','INT', 'ADPT', 'INTER', 'EXACT', 'SAVED', 'REST', 'FRONT', 'ICMAP' 
      DO i = 1,n_vars_registered
        IF( print_full ) THEN     ! TODO: correct format for alignment with fields 
          WRITE (*, u_variable_names_fmt,ADVANCE="NO")  variableRegisterArray(i)%var_name
          WRITE (*, '(1L6)', ADVANCE="NO")  variableRegisterArray(i)%integrated
          WRITE (*, '(6L3)', ADVANCE="NO")  variableRegisterArray(i)%adapt, variableRegisterArray(i)%interpolate, variableRegisterArray(i)%exact
          WRITE (*, '(3L6)', ADVANCE="NO") variableRegisterArray(i)%saved, variableRegisterArray(i)%req_restart, variableRegisterArray(i)%front_load
          WRITE (*, u_variable_names_fmt,ADVANCE="YES")  variableRegisterArray(i)%OPT_var_IC_name
        ELSE
          WRITE (*, u_variable_names_fmt) variableRegisterArray(i)%var_name 
        END IF
      END DO
      PRINT *, '****************************************************'
    END IF

  END SUBROUTINE print_variable_registery


  ! Returns the total number of variables
  FUNCTION get_n_var
    INTEGER get_n_var
    
    get_n_var = n_var
    
  END FUNCTION get_n_var
  
  
END MODULE variable_mapping


