MODULE rk_mod
  USE precision
  USE share_kry
  USE sizes
  USE elliptic_mod
  USE pde
  USE share_consts
  USE user_case

  IMPLICIT NONE
CONTAINS

!--************************************************************************ 
!--Begin RK time step subroutines
!--************************************************************************
SUBROUTINE time_adv_rk2 (u, p,  nwlt_in, ddt, t_local)
  USE debug_vars
  USE parallel
  USE variable_mapping
  IMPLICIT NONE
  
  INTEGER, INTENT (IN) :: nwlt_in
  REAL (pr) :: ddt, t_local
  REAL (pr), DIMENSION (nwlt_in*n_integrated), INTENT(INOUT) :: u  !1D flat version of integrated variables without BC
  REAL (pr), DIMENSION (nwlt_in), INTENT (INOUT) :: p   

  INTEGER :: j, j_p, m, nttl
  REAL (pr), DIMENSION (nwlt_in*n_integrated) :: currhs, curu
  INTEGER, PARAMETER :: meth = 1
  INTEGER :: alloc_stat

  told = t_local
  nttl = nwlt_in*n_integrated
  n = nttl
  ne = n_integrated
  ng = nttl/ne
  
  IF (ALLOCATED(u_prev_timestep)) DEALLOCATE(u_prev_timestep)
  ALLOCATE (u_prev_timestep(1:nttl),STAT=alloc_stat)
  CALL test_alloc( alloc_stat, 'u_prev_timestep',nttl )
  IF (ALLOCATED(u_for_BCs)) DEALLOCATE(u_for_BCs)
  ALLOCATE (u_for_BCs(1:nttl),STAT=alloc_stat)
  CALL test_alloc( alloc_stat, 'u_for_BCs',nttl )

  u_prev_timestep = u

  currhs = RK_rhs (u_prev_timestep,p)  !f_n
  curu   = u_prev_timestep + ddt/2.0_pr*currhs !u*
  u_for_BCs = curu
  CALL project_BC (curu, meth)

  currhs = RK_rhs (curu, p) !f*
  u      = u_prev_timestep + ddt*currhs !u_n+1  
  u_for_BCs = u
  CALL project_BC (u, meth)
  
  t_local = t_local + ddt
  CALL user_project (u, p, nwlt_in, meth)
END SUBROUTINE time_adv_rk2

SUBROUTINE time_adv_rk4 (u, p,  nwlt_in, ddt, t_local)
  USE debug_vars
  USE parallel
  USE variable_mapping
  IMPLICIT NONE
  
  INTEGER, INTENT (IN) :: nwlt_in
  REAL (pr) :: ddt, t_local
  REAL (pr), DIMENSION (nwlt_in*n_integrated), INTENT(INOUT) :: u  !1D flat version of integrated variables without BC
  REAL (pr), DIMENSION (nwlt_in), INTENT (INOUT) :: p   

  INTEGER :: j, j_p, m, nttl
  REAL (pr), DIMENSION (nwlt_in*n_integrated) :: currhs, curu
  INTEGER, PARAMETER :: meth = 1
  INTEGER :: alloc_stat

  told = t_local
  nttl = nwlt_in*n_integrated
  n = nttl
  ne = n_integrated
  ng = nttl/ne
  
  IF (ALLOCATED(u_prev_timestep)) DEALLOCATE(u_prev_timestep)
  ALLOCATE (u_prev_timestep(1:nttl),STAT=alloc_stat)
  CALL test_alloc( alloc_stat, 'u_prev_timestep',nttl )
  IF (ALLOCATED(u_for_BCs)) DEALLOCATE(u_for_BCs)
  ALLOCATE (u_for_BCs(1:nttl),STAT=alloc_stat)
  CALL test_alloc( alloc_stat, 'u_for_BCs',nttl )


  u_prev_timestep = u

  currhs = RK_rhs (u_prev_timestep,p)  !f_n
  curu   = u_prev_timestep + ddt/2.0_pr*currhs !u*
  u_for_BCs = curu
  CALL project_BC (curu, meth)
  u      = currhs !f_n  

  currhs = RK_rhs (curu, p) !f*
  curu   = u_prev_timestep + ddt/2.0_pr*currhs !u**
  u_for_BCs = curu
  CALL project_BC (curu, meth)
  u      = u + 2*currhs !f_n + 2f*
 
  currhs = RK_rhs (curu, p) !f**
  curu   = u_prev_timestep + ddt*currhs !u***
  u_for_BCs = curu
  CALL project_BC (curu, meth)
  u      = u + 2*currhs !f_n + 2f* + 2f**

  currhs = RK_rhs (curu, p) !f***
  u      = u_prev_timestep + ddt/6.0_pr*(u + currhs) !u_n + dt/6*(f_n + 2f* + 2f** + f***)
  u_for_BCs = u
  CALL project_BC (u, meth)
  
  t_local = t_local + ddt
  CALL user_project (u, p, nwlt_in, meth)
END SUBROUTINE time_adv_rk4

SUBROUTINE time_adv_rk2tvd (u, p,  nwlt_in, ddt, t_local)
  USE debug_vars
  USE parallel
  USE variable_mapping
  IMPLICIT NONE
  
  INTEGER, INTENT (IN) :: nwlt_in
  REAL (pr) :: ddt, t_local
  REAL (pr), DIMENSION (nwlt_in*n_integrated), INTENT(INOUT) :: u  !1D flat version of integrated variables without BC
  REAL (pr), DIMENSION (nwlt_in), INTENT (INOUT) :: p   

  INTEGER :: j, j_p, m, nttl
  REAL (pr), DIMENSION (nwlt_in*n_integrated) :: currhs, curu
  INTEGER, PARAMETER :: meth = 1
  INTEGER :: alloc_stat

  told = t_local
  nttl = nwlt_in*n_integrated
  n = nttl
  ne = n_integrated
  ng = nttl/ne
  
  IF (ALLOCATED(u_prev_timestep)) DEALLOCATE(u_prev_timestep)
  ALLOCATE (u_prev_timestep(1:nttl),STAT=alloc_stat)
  CALL test_alloc( alloc_stat, 'u_prev_timestep',nttl )
  IF (ALLOCATED(u_for_BCs)) DEALLOCATE(u_for_BCs)
  ALLOCATE (u_for_BCs(1:nttl),STAT=alloc_stat)
  CALL test_alloc( alloc_stat, 'u_for_BCs',nttl )

 
  u_prev_timestep = u

  currhs = RK_rhs (u_prev_timestep,p)  !f_n
  curu   = u_prev_timestep + ddt*currhs !u*
  u_for_BCs = curu
  CALL project_BC (curu, meth)

  currhs = RK_rhs (curu, p) !f*
  u      = (u_prev_timestep + curu + ddt*currhs)/2.0_pr  !(u_n + u* + dt.f*)/2
  u_for_BCs = u
  CALL project_BC (u, meth)
  
  t_local = t_local + ddt
  CALL user_project (u, p, nwlt_in, meth)
END SUBROUTINE time_adv_rk2tvd

SUBROUTINE time_adv_rk3tvd (u, p,  nwlt_in, ddt, t_local)
  USE debug_vars
  USE parallel
  USE variable_mapping
  IMPLICIT NONE
  
  INTEGER, INTENT (IN) :: nwlt_in
  REAL (pr) :: ddt, t_local
  REAL (pr), DIMENSION (nwlt_in*n_integrated), INTENT(INOUT) :: u  !1D flat version of integrated variables without BC
  REAL (pr), DIMENSION (nwlt_in), INTENT (INOUT) :: p   

  INTEGER :: j, j_p, m, nttl
  REAL (pr), DIMENSION (nwlt_in*n_integrated) :: currhs, curu
  INTEGER, PARAMETER :: meth = 1
  INTEGER :: alloc_stat

  told = t_local
  nttl = nwlt_in*n_integrated
  n = nttl
  ne = n_integrated
  ng = nttl/ne
  
  IF (ALLOCATED(u_prev_timestep)) DEALLOCATE(u_prev_timestep)
  ALLOCATE (u_prev_timestep(1:nttl),STAT=alloc_stat)
  CALL test_alloc( alloc_stat, 'u_prev_timestep',nttl )
  IF (ALLOCATED(u_for_BCs)) DEALLOCATE(u_for_BCs)
  ALLOCATE (u_for_BCs(1:nttl),STAT=alloc_stat)
  CALL test_alloc( alloc_stat, 'u_for_BCs',nttl )


  u_prev_timestep = u

  currhs = RK_rhs (u_prev_timestep,p)  !f_n
  curu   = u_prev_timestep + ddt*currhs !u*
  u_for_BCs = curu
  CALL project_BC (curu, meth)

  currhs = RK_rhs (curu, p) !f*
  curu   = (3.0_pr*u_prev_timestep + curu + ddt*currhs)/4.0_pr !u** = 3/4.u_n + 1/4.u* + dt/4.f*
  u_for_BCs = curu
  CALL project_BC (curu, meth)

  currhs = RK_rhs (curu, p) !f**
  u      = (u_prev_timestep + 2.0_pr*(curu + ddt*currhs))/3.0_pr !1/3.u_n + 2/3.u** + 2/3.dt.f**
  u_for_BCs = u
  CALL project_BC (u, meth)
  
  t_local = t_local + ddt
  CALL user_project (u, p, nwlt_in, meth)
END SUBROUTINE time_adv_rk3tvd

SUBROUTINE time_adv_euler (u, p,  nwlt_in, ddt, t_local)
  USE debug_vars
  USE parallel
  USE variable_mapping
  IMPLICIT NONE
  
  INTEGER, INTENT (IN) :: nwlt_in
  REAL (pr) :: ddt, t_local
  REAL (pr), DIMENSION (nwlt_in*n_integrated), INTENT(INOUT) :: u  !1D flat version of integrated variables without BC
  REAL (pr), DIMENSION (nwlt_in), INTENT (INOUT) :: p   

  INTEGER :: j, j_p, m, nttl
  REAL (pr), DIMENSION (nwlt_in*n_integrated) :: currhs, curu
  INTEGER, PARAMETER :: meth = 1
  INTEGER :: alloc_stat

  told = t_local
  nttl = nwlt_in*n_integrated
  
  IF (ALLOCATED(u_prev_timestep)) DEALLOCATE(u_prev_timestep)
  ALLOCATE (u_prev_timestep(1:nttl),STAT=alloc_stat)
  CALL test_alloc( alloc_stat, 'u_prev_timestep in rk2_time_step',nttl )
  u_prev_timestep = u

  n = nttl
  ne = n_integrated
  ng = nttl/ne

  ! first order accurte in time Explicit Euler
  currhs = RK_rhs (u_prev_timestep,p)  !f_n
  u      = u_prev_timestep + ddt*currhs !u_n+1  
  
  t_local = t_local + dt

  !--Projection step
  CALL user_project (u, p, nwlt_in, meth)

  !setting right boundary conditions
  CALL project_BC (u, meth)

END SUBROUTINE time_adv_euler

FUNCTION RK_rhs (Uvec,scalar)
  IMPLICIT NONE
  REAL (pr), DIMENSION (n), INTENT(IN) :: Uvec 
  REAL (pr), DIMENSION (ng), INTENT(IN) :: scalar
  REAL (pr), DIMENSION (n) :: RK_rhs
  RK_rhs = user_rhs (Uvec,scalar)
END FUNCTION RK_rhs

SUBROUTINE project_BC (Uvec, meth)
  IMPLICIT NONE
  REAL (pr), DIMENSION (n), INTENT(INOUT) :: Uvec
  INTEGER, INTENT(IN) :: meth
  REAL (pr), DIMENSION (n) :: rhs, RK_diag
  INTEGER :: ie, shift

  rhs = 0.0_pr  
  CALL user_algebraic_BC_rhs (rhs, ne, ng, j_lev)
  RK_diag = 0.0_pr
  CALL user_algebraic_BC (RK_diag, Uvec, ng, ne, j_lev, meth)
  rhs = rhs - RK_diag !f-Lu

  DO ie = 1, ne
     shift = nwlt*(ie-1)
     RK_diag(shift+nwlt_intrnl+1:shift+nwlt) = 1.0_pr
  END DO
  CALL user_algebraic_BC_diag (RK_diag, nwlt, ne, j_lev, meth)
  DO ie = 1, ne
     shift = nwlt*(ie-1)
     Uvec(shift+nwlt_intrnl+1:shift+nwlt)  = Uvec(shift+nwlt_intrnl+1:shift+nwlt) + rhs(shift+nwlt_intrnl+1:shift+nwlt)/RK_diag(shift+nwlt_intrnl+1:shift+nwlt)  
     !u_new = u_old + (f-Lu)/diag ... assumes that diagonal term is correct and there is no coupling
  END DO
  
END SUBROUTINE project_BC

!--********************************
!--End RK time step subroutines
!--********************************

END MODULE rk_mod
