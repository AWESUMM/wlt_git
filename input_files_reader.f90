!  General File Reader
!  by Alexei Vezolainen
!  Oct 2005
MODULE input_file_reader
  USE precision
  USE debug_vars

  IMPLICIT NONE  

  PUBLIC :: start_reader, stop_reader,            & ! global user functions
       input_string,      input_logical,          & ! of the reader
       input_integer,     input_real,             & !
       input_integer_vector, input_real_vector,   & !
       check_highlimit_real, check_lowlimit_real, & ! Check that inputs are rational 
       check_highlimit_int, check_lowlimit_int      !
  
  LOGICAL, PRIVATE  :: verbose ! print on screen while reading the data from file (set by start_reader() optional parameter VERB)
  LOGICAL, PRIVATE, PARAMETER  :: overwrite_similar = .TRUE.   ! if similar entries are found in the file, use the last one
  LOGICAL, PRIVATE, PARAMETER  :: name_case_sensitive = .TRUE. ! variable name is case-sensitive
  CHARACTER (LEN=*), PRIVATE, PARAMETER :: output_real_format = 'E14.7'            !  ... with such formats
  CHARACTER (LEN=*), PRIVATE, PARAMETER :: output_int_format = 'I8'                !  ...
  CHARACTER (LEN=*), PRIVATE, PARAMETER :: comment_start_position = '50'   ! comments will start from that position
!  CHARACTER (LEN=*), PRIVATE, PARAMETER :: vector_comment_format = 'T70'  ! recommended for some MS' compilers
  CHARACTER (LEN=*), PRIVATE, PARAMETER :: vector_comment_format = '10X'  ! recommended for IBM PC,SP, or SGI
  CHARACTER (LEN=*), PRIVATE, PARAMETER :: input_real_format = 'E14.7'         ! input formats; sometimes 'E' and 'I' work also,
  CHARACTER (LEN=*), PRIVATE, PARAMETER :: input_int_format = 'I8'             ! although some compilers don't like 'E'

  INTEGER, PRIVATE, PARAMETER   :: MAX_NAME_LENGTH = 256               ! variable name
  INTEGER, PRIVATE, PARAMETER   :: MAX_VALUE_LENGTH = 256              ! variable value(s)
  INTEGER, PRIVATE, PARAMETER   :: MAX_LINE_LENGTH = 256               ! input file line
  INTEGER, PRIVATE, PARAMETER   :: UNIT_NUMBER = 12345                 ! filename unit number
  CHARACTER (LEN=*), PRIVATE, PARAMETER :: char_set = &            ! allowed characters in var.names
       'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz_'
  CHARACTER (LEN=*), PRIVATE, PARAMETER :: num_set = &             ! allowed numbers in var.values
       '0123456789.dDeE+-"' // "'"
  INTEGER, PRIVATE, PARAMETER   :: HASH_LENGTH = 64              ! hash table size - choose optimal
  LOGICAL (KIND=1), PRIVATE     :: module_initialized = .FALSE.  ! initialize test marker - don't change
  TYPE, PRIVATE :: var                                           ! hash table cell internal structure
     CHARACTER (LEN=MAX_NAME_LENGTH) :: name_str                   
     CHARACTER (LEN=MAX_VALUE_LENGTH) :: val_str
     TYPE (var), POINTER :: next
  END TYPE var
  TYPE (var), PRIVATE, TARGET, ALLOCATABLE :: hash_table (:)     ! the hash table
  PRIVATE :: error_num, hash_calculate, put_into_table, &        ! some local functions
       locate, dot_add, initialize_hash_table, &                 ! ...
       to_upper                                                  ! ...
CONTAINS
  !-----------------------------------------------------------------
  SUBROUTINE input_real_vector (name_in, out_value, dim, key, comment)
    CHARACTER (LEN=*), INTENT(IN) :: name_in
    CHARACTER (LEN=MAX_NAME_LENGTH) :: name
    INTEGER, INTENT(IN) :: dim                ! vector length
    REAL (pr), INTENT(OUT) :: out_value (1:dim)
    CHARACTER (LEN=*), INTENT(IN) :: key                !'stop' or 'default' marker
    CHARACTER (LEN=*), INTENT(IN), OPTIONAL :: comment  ! to be printed at the end of the line
    CHARACTER (LEN=MAX_VALUE_LENGTH) :: val, c_val
    INTEGER :: i, & ! dimension
         cb_pos,  & ! beginning of value field
         ce_pos     ! end of value field

    name = ADJUSTL(name_in)                           ! remove left spaces
    IF (.NOT.locate (name, val, key) ) RETURN         ! get value string

    if (verbose) WRITE (*,FMT='(2A)',ADVANCE='NO') TRIM(name),' = '

    cb_pos = 1
    ce_pos = 1
    do i=1,dim
       if (i.eq.dim) then
          ce_pos = MAX_VALUE_LENGTH
          IF ( SCAN(val(cb_pos:MAX_VALUE_LENGTH),',') .GT. 0 ) &
               ce_pos = MIN (ce_pos, cb_pos -1 + SCAN(val(cb_pos:MAX_VALUE_LENGTH),',') ) !gfortran related
          IF ( SCAN(val(cb_pos:MAX_VALUE_LENGTH),' ') .GT. 0 ) &
               ce_pos = MIN (ce_pos, cb_pos -1 + SCAN(val(cb_pos:MAX_VALUE_LENGTH),' ') ) !gfortran related
          IF ( SCAN(val(cb_pos:MAX_VALUE_LENGTH),'#') .GT. 0 ) &
               ce_pos = MIN (ce_pos, cb_pos -1 + SCAN(val(cb_pos:MAX_VALUE_LENGTH),'#') ) !gfortran related
       else
          ce_pos = cb_pos -1 + SCAN(val(cb_pos:MAX_VALUE_LENGTH),',')
       end if
       c_val = val(cb_pos:ce_pos-1)
       call dot_add(c_val,name)                                                  ! add decimal dot
       READ(UNIT=c_val,FMT='('//input_real_format//')',ERR=1) out_value(i)       ! transform to real
       cb_pos = ce_pos -1 + SCAN(val(ce_pos:MAX_VALUE_LENGTH),'-+0123456789')

       if (verbose) then                                                         ! verbose output
          WRITE (*,'('//output_real_format//')',ADVANCE='NO') out_value(i)
          if (i .lt. dim) WRITE (*,'(A)',ADVANCE='NO') ','
       end if
    end do

    if (verbose) then
       if (PRESENT(comment)) then
          WRITE (*,'('//vector_comment_format//',2A)') ' #', comment
       else
          WRITE (*,'(A)') ' '
       end if
    end if

    RETURN
1   call error_num(3,name)
  END SUBROUTINE input_real_vector
  !-----------------------------------------------------------------
  SUBROUTINE input_real (name_in, out_value, key, comment)
    CHARACTER (LEN=*), INTENT(IN) :: name_in
    CHARACTER (LEN=MAX_NAME_LENGTH) :: name
    REAL (pr), INTENT(OUT) :: out_value
    CHARACTER (LEN=*), INTENT(IN) :: key                !'stop' or 'default' marker
    CHARACTER (LEN=*), INTENT(IN), OPTIONAL :: comment  ! to be printed at the end of the line
    CHARACTER (LEN=MAX_VALUE_LENGTH) :: val

    name = ADJUSTL(name_in)                           ! remove left spaces
    IF (.NOT.locate (name, val, key) ) RETURN                                   ! get value string
    call dot_add (val,name)                                                     ! add decimal dot
    READ(UNIT=val,FMT='('//input_real_format//')',ERR=1) out_value              ! transform to real

    if (verbose) then                                                           ! verbose output
       if (PRESENT(comment)) then
          WRITE (*,FMT='(2A,'//output_real_format//',T'//comment_start_position//',2A)') &
               TRIM(name),' = ', out_value, ' #', comment
       else
          WRITE (*,FMT='(2A,'//output_real_format//')') &
               TRIM(name),' = ', out_value
       end if
    end if

    RETURN
1   call error_num(3,name)
  END SUBROUTINE input_real
  !-----------------------------------------------------------------
  SUBROUTINE dot_add (val,name_in)                            ! add decimal dot to the
    CHARACTER (LEN=*), INTENT(IN) :: name_in                      ! number, if necessary
    CHARACTER (LEN=MAX_VALUE_LENGTH),INTENT(INOUT) :: val
    INTEGER :: last_pos, E_pos

    if (SCAN(val,'.').eq.0) then ! decimal dot does not present
       E_pos = SCAN(val,'Ee')                              ! exponent sign position,
       if (E_pos.eq.0) &
            E_pos = SCAN(val,'Dd')                         ! ... or that one,
       last_pos = SCAN(val,'0123456789',BACK=.TRUE.)       ! the last digit

       if (last_pos.eq.MAX_VALUE_LENGTH) then              ! check length limit
          PRINT *, 'Value string is too long :', TRIM (ADJUSTL(name_in))
          STOP 1
       end if

       if (E_pos.eq.0) then      ! insert dot at the end
          val(last_pos+1:last_pos+1) = '.'
       else                      ! insert dot before the exponent
          val(E_pos+1:last_pos+1) = val(E_pos:last_pos)
          val(E_pos:E_pos) = '.'
       end if
    end if

  END SUBROUTINE dot_add
  !-----------------------------------------------------------------
  SUBROUTINE input_integer_vector (name_in, out_value, dim, key, comment)
    CHARACTER (LEN=*), INTENT(IN) :: name_in
    CHARACTER (LEN=MAX_NAME_LENGTH) :: name
    INTEGER, INTENT(IN) :: dim                ! vector length
    INTEGER, INTENT(OUT) :: out_value(1:dim)
    CHARACTER (LEN=*), INTENT(IN) :: key                !'stop' or 'default' marker
    CHARACTER (LEN=*), INTENT(IN), OPTIONAL :: comment  ! to be printed at the end of the line
    CHARACTER (LEN=MAX_VALUE_LENGTH) :: val
    INTEGER :: i, & ! dimension
         cb_pos,  & ! beginning of value field
         ce_pos     ! end of value field

    name = ADJUSTL(name_in)                           ! remove left spaces
    IF (.NOT.locate (name, val, key) ) RETURN         ! get value string

    if (verbose) WRITE (*,FMT='(2A)',ADVANCE='NO') TRIM(name),' = '

    cb_pos = 1
    ce_pos = 1
    do i=1,dim
       if (i.eq.dim) then
          ce_pos = MAX_VALUE_LENGTH
          IF ( SCAN(val(cb_pos:MAX_VALUE_LENGTH),',') .GT. 0 ) &
               ce_pos = MIN (ce_pos, cb_pos -1 + SCAN(val(cb_pos:MAX_VALUE_LENGTH),',') ) !gfortran related
          IF ( SCAN(val(cb_pos:MAX_VALUE_LENGTH),' ') .GT. 0 ) &
               ce_pos = MIN (ce_pos, cb_pos -1 + SCAN(val(cb_pos:MAX_VALUE_LENGTH),' ') ) !gfortran related
          IF ( SCAN(val(cb_pos:MAX_VALUE_LENGTH),'#') .GT. 0 ) &
               ce_pos = MIN (ce_pos, cb_pos -1 + SCAN(val(cb_pos:MAX_VALUE_LENGTH),'#') ) !gfortran related
       else
          ce_pos = cb_pos -1 + SCAN(val(cb_pos:MAX_VALUE_LENGTH),',')
       end if
       
       
       READ(UNIT=val(cb_pos:ce_pos-1),FMT='('//input_int_format//')',ERR=1) &
            out_value(i)                                     ! transform to integer

       cb_pos = ce_pos -1 + SCAN(val(ce_pos:MAX_VALUE_LENGTH),'-+0123456789')

       if (verbose) then
          WRITE (*,'('//output_int_format//')',ADVANCE='NO') out_value(i)
          if (i.lt.dim) WRITE (*,'(A)',ADVANCE='NO') ','
       end if
    end do

    if (verbose) then
       if (PRESENT(comment)) then
          WRITE (*,'('//vector_comment_format//',2A)') ' #', comment
       else
          WRITE (*,'(A)') ' '
       end if
    end if
    
    RETURN
1   call error_num(3,name)
  END SUBROUTINE input_integer_vector
  !-----------------------------------------------------------------
  SUBROUTINE input_integer (name_in, out_value, key, comment)
    CHARACTER (LEN=*), INTENT(IN) :: name_in
    CHARACTER (LEN=MAX_NAME_LENGTH) :: name
    INTEGER, INTENT(OUT) :: out_value
    CHARACTER (LEN=*), INTENT(IN) :: key                !'stop' or 'default' marker
    CHARACTER (LEN=*), INTENT(IN), OPTIONAL :: comment  ! to be printed at the end of the line
    CHARACTER (LEN=MAX_VALUE_LENGTH) :: val

    name = ADJUSTL(name_in)                           ! remove left spaces
    IF (.NOT.locate (name, val, key) ) RETURN                        ! get value string
    READ(UNIT=val,FMT='('//input_int_format//')',ERR=1) out_value    ! transform to integer

    if (verbose) then                                                ! verbose output
       if (PRESENT(comment)) then
          WRITE (*,FMT='(2A,'//output_int_format//',T'//comment_start_position//',2A)') &
               TRIM(name),' = ',out_value,' #', comment
       else
          WRITE (*,FMT='(2A,'//output_int_format//')') &
               TRIM(name),' = ',out_value
       end if
    end if
    
    RETURN
1   call error_num(3,name)
  END SUBROUTINE input_integer
  !-----------------------------------------------------------------
  SUBROUTINE input_logical (name_in, out_value, key, comment)
    CHARACTER (LEN=*), INTENT(IN) :: name_in
    CHARACTER (LEN=MAX_NAME_LENGTH) :: name
    LOGICAL, INTENT(OUT) :: out_value
    CHARACTER (LEN=*), INTENT(IN) :: key                !'stop' or 'default' marker
    CHARACTER (LEN=*), INTENT(IN), OPTIONAL :: comment  ! to be printed at the end of the line
    CHARACTER (LEN=MAX_VALUE_LENGTH) :: val
    INTEGER :: lpos, rpos
    CHARACTER (LEN=1) :: output_printed             ! 'T' or 'F' - for the output only

    name = ADJUSTL(name_in)                           ! remove left spaces
    IF (.NOT.locate (name, val, key) ) RETURN         ! get value string
    if ((val(1:1).eq.'"').or.(val(1:1).eq."'")) then          ! value in quotes
       rpos = SCAN(val,val(1:1),BACK=.TRUE.)
       if ((rpos.eq.1).or.(rpos.eq.0)) then                   ! right quote absent
          PRINT *, 'Strange quotes, while reading :', TRIM(name)
          STOP 1
       end if
       lpos = 2; rpos = rpos - 1
    else
       lpos = 1; rpos = LEN(TRIM(val))                        ! clean string
    end if
    
    if ((val(lpos:rpos).eq.'1').or. &                         ! analize all cases
         (val(lpos:rpos).eq.'on').or. &
         (val(lpos:rpos).eq.'ON').or. &
         (val(lpos:rpos).eq.'T').or. &
         (val(lpos:rpos).eq.'t') ) then
       out_value = .TRUE.
    else
       if ((val(lpos:rpos).eq.'0').or. &
            (val(lpos:rpos).eq.'off').or. &
            (val(lpos:rpos).eq.'OFF').or. &
            (val(lpos:rpos).eq.'F').or. &
            (val(lpos:rpos).eq.'f') ) then
          out_value = .FALSE.
       else
          PRINT *, 'Strange value, while reading :', TRIM(name)
          STOP 1
       end if
    end if

    if (verbose) then                               ! verbose output
       if (out_value) then
          output_printed = 'T'
       else
          output_printed = 'F'
       end if
       if (PRESENT(comment)) then
          WRITE (*,FMT='(3A,T'//comment_start_position//',2A)') &
               TRIM(name),' = ',output_printed,' #', comment
       else
          WRITE (*,FMT='(3A)') &
               TRIM(name),' = ',output_printed
       end if
    end if

  END SUBROUTINE input_logical
  !-----------------------------------------------------------------
  SUBROUTINE input_string (name_in, out_value, key, comment)
    CHARACTER (LEN=*), INTENT(IN) :: name_in
    CHARACTER (LEN=MAX_NAME_LENGTH) :: name
    CHARACTER (LEN=*), INTENT(OUT) :: out_value
    CHARACTER (LEN=*), INTENT(IN) :: key                !'stop' or 'default' marker
    CHARACTER (LEN=*), INTENT(IN), OPTIONAL :: comment  ! to be printed at the end of the line
    CHARACTER (LEN=MAX_VALUE_LENGTH) :: val
    INTEGER :: pos

    name = ADJUSTL(name_in)                           ! remove left spaces
    IF (.NOT.locate (name, val, key) ) RETURN         ! get value string
    if ((val(1:1).ne.'"').and.(val(1:1).ne."'")) then         ! left quote test
       PRINT *, 'Left quote seems to be absent :', TRIM(name)
       STOP 1
    end if

    pos = SCAN(val,val(1:1),BACK=.TRUE.)                      ! right quote position
    if ((pos.eq.0).or.(pos.eq.1)) then
       PRINT *, 'Right quote seems to be absent :', TRIM(name)
       STOP 1
    end if

    if (LEN(out_value) .lt. pos-2) then                       ! test length limit
       PRINT *, 'String too long :', TRIM(name)
       STOP 1
    end if
    out_value = val(2:pos-1)                                  ! set output string

    if (verbose) then
       if (PRESENT(comment)) then
          WRITE (*,FMT='(4A,T'//comment_start_position//',2A)') &
               TRIM(name),' = "',TRIM(out_value),'"',' #', comment
       else
          WRITE (*,FMT='(4A)') &
               TRIM(name),' = "',TRIM(out_value),'"'
       end if
    end if
  END SUBROUTINE input_string
  !-----------------------------------------------------------------
  LOGICAL*1 FUNCTION locate (name_in, val, key)  ! TRUE if table entry is found
    CHARACTER (LEN=*), INTENT(IN) :: name_in        ! string for the name
    CHARACTER (LEN=MAX_NAME_LENGTH) :: name
    CHARACTER (LEN=*), INTENT(IN) :: key             !'stop' or 'default' marker
    CHARACTER (LEN=*), INTENT(OUT) :: val           ! string for the values
    TYPE (var), POINTER :: p
    
    if (.NOT.module_initialized) call error_num (1,'')    ! test initialization
    CALL to_upper (name_in, name)                     ! make upper case, if required
    
    p => hash_table (hash_calculate(name))                  ! find the entry
    do while (p % name_str .ne. name)
       if (ASSOCIATED(p % next)) then
          p => p % next
       else                  ! name not found
          select case (key)
          case ('stop')
             PRINT *, 'Name not found :', TRIM(name)
             STOP 1
          case ('default')
             locate = .FALSE.
             RETURN
          case default
             PRINT *, 'Wrong default/stop key value :', TRIM(name)
          end select
       end if
    end do
    val = p % val_str                                       ! return value string
    locate = .TRUE.

  END FUNCTION locate
  !-----------------------------------------------------------------
  SUBROUTINE stop_reader
    INTEGER :: pos
    TYPE (var), POINTER :: p, p1
    
    if (.NOT.module_initialized) call error_num (1,'')
    do pos=1,HASH_LENGTH                                    ! clean collisions for
       p => hash_table (pos)                                ! each table entry
       do while (ASSOCIATED(p % next))
          p1 => p % next
          if (.NOT.ASSOCIATED(p,hash_table (pos))) DEALLOCATE(p)
          p => p1
       end do
       if (.NOT.ASSOCIATED(p,hash_table (pos))) DEALLOCATE(p)
    end do
    DEALLOCATE (hash_table)                                 ! clean the table

  END SUBROUTINE stop_reader
  !-----------------------------------------------------------------
  SUBROUTINE put_into_table (name, val)
    CHARACTER (LEN=*), INTENT(IN) :: name, val
    TYPE (var), POINTER :: p
    
    p => hash_table (hash_calculate(name))                      ! current insert position
    
    do while (p % name_str .ne. '')
       
       if (overwrite_similar .AND.(TRIM(p % name_str).EQ.TRIM(name))) exit
       
       if (.NOT.ASSOCIATED(p % next)) then                      !  insert position in case of hash
          ALLOCATE(p % next)                                    !  collision
          p % next % name_str = ''                              ! initialize the new node
          NULLIFY(p % next % next)                              ! ...
       end if
       p => p % next
       
    end do
    
    p % name_str = name                                         ! insert
    p % val_str = val                                           ! ...

  END SUBROUTINE put_into_table
  !-----------------------------------------------------------------
  INTEGER FUNCTION hash_calculate (name)
    CHARACTER (LEN=*), INTENT(IN) :: name
    INTEGER i
    
    hash_calculate = 0
    do i=1,LEN(TRIM(name))
       hash_calculate = hash_calculate + IACHAR(name(i:i))    ! summ as hash
    end do
    hash_calculate = MOD(hash_calculate, HASH_LENGTH) + 1
    
  END FUNCTION hash_calculate
  !-----------------------------------------------------------------
  SUBROUTINE error_num (in, name)                                  ! some error handling
    integer, intent(in) :: in
    CHARACTER (LEN=*) :: name

    select case (in)
    case (1)
       print *,'Reader has to be initialized first'
    case (2)
       print *,'Reader has been initialized already'
    case (3)
       print *,'Error while reading value for :', TRIM(name)
    case default
       print *,'Reader internal error'
    end select
    STOP 1

  END SUBROUTINE error_num
  !-----------------------------------------------------------------
  SUBROUTINE start_reader (filename, VERB)
    CHARACTER (LEN=*), INTENT(IN) :: filename          ! filename to be read
    LOGICAL, OPTIONAL, INTENT(IN) :: VERB              ! verbose level
    CHARACTER (LEN=MAX_LINE_LENGTH) :: fline           ! whole single line of the file
    CHARACTER (LEN=MAX_NAME_LENGTH) :: vname           ! name string
    CHARACTER (LEN=MAX_VALUE_LENGTH) :: vval           ! value string
    CHARACTER (LEN=MAX_NAME_LENGTH) :: name_upper      ! name to be placed into hash table
    INTEGER posnb, posne, poseq, l_end, pos, pose
    
    verbose = .FALSE.
    IF (PRESENT(VERB)) verbose = VERB


    if (.NOT.module_initialized) call initialize_hash_table
    
    OPEN (FILE=filename,ACTION='READ',STATUS='OLD', &         ! open the file
         UNIT=UNIT_NUMBER,ERR=1,POSITION='REWIND')
    do
       READ (UNIT_NUMBER,ERR=3,END=4,FMT="(A)") fline         ! raw file line with all the blanks
       posnb = SCAN(fline,'#')
       if (posnb.eq.0) then
          l_end = MAX_LINE_LENGTH                             ! end of string to analize
       else 
          l_end = posnb
       end if
       posnb = SCAN(fline,char_set//num_set)                  ! beginning of the string
       if ((posnb.eq.0).or.(posnb.ge.l_end)) CYCLE
       poseq = SCAN(fline(posnb:l_end-1),'=')                 ! position of '=' is posnb+poseq-1
       if (poseq.eq.0) then
          PRINT *, 'Symbol "=" not found in the line: ', TRIM(fline)
          STOP 1
       else
          posne = SCAN(fline(posnb:posnb+poseq-1), &
               char_set//num_set,BACK=.TRUE.)                 ! ending of the string is posnb+posne-1
          vname = fline(posnb:posnb+posne-1)                  ! the name
       end if
       pos = SCAN(fline(posnb+poseq-1:l_end-1),char_set//num_set)     ! value string begins at 
       if (pos.eq.0) then                                             !  posnb+poseq-1 + pos -1
          PRINT *, 'Error while reading the line: ', TRIM(fline)
          STOP 1
       else
          pose = SCAN(fline(posnb+poseq-1:l_end-1), &
               char_set//num_set,BACK=.TRUE.)                       ! value string ending
          vval = fline(posnb+poseq-2+pos:posnb+poseq-2+pose)        ! the values
       end if
       
       CALL to_upper (vname, name_upper)                      ! make upper case, if required
       CALL put_into_table(name_upper,vval)                   ! put into the table
    end do
    
4   CLOSE (UNIT=UNIT_NUMBER,ERR=2)                            ! close the file
    RETURN
1   PRINT *, 'File not found: ', filename
    STOP 1
2   PRINT *, 'File cannot be closed: ', filename
    STOP 1
3   PRINT *, 'Error while reading the file: ', filename
    STOP 1
  END SUBROUTINE start_reader
  !-----------------------------------------------------------------
  SUBROUTINE to_upper (name_in, name)
    CHARACTER (LEN=*), INTENT(IN)  :: name_in    ! input string
    CHARACTER (LEN=*), INTENT(OUT) :: name       ! upper case output, sometimes
    INTEGER i
    
    name = name_in                     ! case sensitive case
    
    IF (.NOT.name_case_sensitive) THEN ! make upper case
       DO i=1,LEN(TRIM(name))
          IF ( LGE(name(i:i),'a') .AND. LLE(name(i:i),'z') ) THEN
             name(i:i) = ACHAR(IACHAR( name(i:i) ) - 32 )
          END IF
       END DO
    END IF
  END SUBROUTINE to_upper
  !-----------------------------------------------------------------
  SUBROUTINE initialize_hash_table
    INTEGER pos, alloc_stat

    if (module_initialized) call error_num (2,'')       ! test initialization
    module_initialized = .TRUE.

    ALLOCATE (hash_table(HASH_LENGTH),STAT=alloc_stat)        ! new[] hash table
    if (alloc_stat.ne.0) then                                 ! test allocation
       PRINT *, 'Error during hash table allocation'
       STOP 1
    end if
    
    do pos=1,HASH_LENGTH                                      ! initialize hash to ''
       hash_table(pos) % name_str = '';                       ! (just in case)
       NULLIFY(hash_table(pos) % next)
    end do

  END SUBROUTINE initialize_hash_table
  !-----------------------------------------------------------------

  ! Check the upper limit on a real scalar, throwing an error and exiting upon fail
  ! input - scalar to check
  ! name_in - the variable name
  ! limit - value to compare agains
  ! comment - comment to print in error message
  SUBROUTINE check_highlimit_real( name_in, input, limit, comment )
    USE parallel
    IMPLICIT NONE
    REAL (pr), INTENT(IN) :: input, limit
    CHARACTER (LEN=*), INTENT(IN) :: name_in
    CHARACTER (LEN=*), INTENT(IN) :: comment 

    IF ( input .GT. limit ) THEN
       WRITE (*,FMT='(A,'//output_int_format//',3A,'//output_real_format//',1A,'//output_real_format//',2A)') &
               'ERROR on Proc ', par_rank, ': ',  TRIM(name_in), ' = ', input , ' > ', limit, '; ', comment
       WRITE (*,FMT='(1A)') &
              'Exiting...'
       CALL parallel_finalize
       STOP
    END IF

  END SUBROUTINE check_highlimit_real

  ! Check the lower limit on a real scalar, throwing an error and exiting upon fail
  ! input - scalar to check
  ! name_in - the variable name
  ! limit - value to compare agains
  ! comment - comment to print in error message
  SUBROUTINE check_lowlimit_real( name_in, input, limit, comment )
    USE parallel
    IMPLICIT NONE
    REAL (pr), INTENT(IN) :: input, limit
    CHARACTER (LEN=*), INTENT(IN) :: name_in
    CHARACTER (LEN=*), INTENT(IN) :: comment 

    IF ( input .LT. limit ) THEN
       WRITE (*,FMT='(A,'//output_int_format//',3A,'//output_real_format//',1A,'//output_real_format//',2A)') &
               'ERROR on Proc ', par_rank, ': ',  TRIM(name_in), ' = ', input , ' < ', limit, '; ', comment
       WRITE (*,FMT='(1A)') &
              'Exiting...'
       CALL parallel_finalize
       STOP
    END IF

  END SUBROUTINE check_lowlimit_real
     
  ! Check the upper limit on an integer scalar, throwing an error and exiting upon fail
  ! input - scalar to check
  ! name_in - the variable name
  ! limit - value to compare agains
  ! comment - comment to print in error message
  SUBROUTINE check_highlimit_int( name_in, input, limit, comment )
    USE parallel
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: input, limit
    CHARACTER (LEN=*), INTENT(IN) :: name_in
    CHARACTER (LEN=*), INTENT(IN) :: comment 


    IF ( input .GT. limit ) THEN
       WRITE (*,FMT='(A,'//output_int_format//',3A,'//output_int_format//',1A,'//output_int_format//',2A)') &
               'ERROR on Proc ', par_rank, ': ',  TRIM(name_in), ' = ', input , ' > ', limit, '; ', comment
       WRITE (*,FMT='(1A)') &
              'Exiting...'
       CALL parallel_finalize
       STOP
    END IF

  END SUBROUTINE check_highlimit_int

  ! Check the lower limit on an integer scalar, throwing an error and exiting upon fail
  ! input - scalar to check
  ! name_in - the variable name
  ! limit - value to compare agains
  ! comment - comment to print in error message
  SUBROUTINE check_lowlimit_int( name_in, input, limit, comment )
    USE parallel
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: input, limit
    CHARACTER (LEN=*), INTENT(IN) :: name_in
    CHARACTER (LEN=*), INTENT(IN) :: comment 


    IF ( input .LT. limit ) THEN
       WRITE (*,FMT='(A,'//output_int_format//',3A,'//output_int_format//',1A,'//output_int_format//',2A)') &
               'ERROR on Proc ', par_rank, ': ',  TRIM(name_in), ' = ', input , ' < ', limit, '; ', comment
       WRITE (*,FMT='(1A)') &
              'Exiting...'
       CALL parallel_finalize
       STOP
    END IF

  END SUBROUTINE check_lowlimit_int


END MODULE input_file_reader
