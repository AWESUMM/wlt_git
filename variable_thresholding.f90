!
! variable thresholding
!
MODULE VT !variable_thresholding



  USE precision
  USE wlt_vars
  USE wavelet_filters_mod
  USE wlt_trns_mod
  USE wlt_trns_vars
  USE wlt_trns_util_mod
  USE io_3d_vars
  USE util_mod
  USE util_vars
  USE share_consts
  USE pde
  USE variable_mapping 
  USE sizes
  USE share_kry
  USE vector_util_mod
  USE input_file_reader
  USE SGS_util

! #define DEBUG_INTERPOL
! #define DEBUG_VT
  
  
  
  
  
  
  !
  ! Specific Variables for
  ! Spatial Adaptive Epsilon
  !
  REAL (pr)          :: Psgs_diss_goal__AdpEpsSp   ! Percent SGS Dissipation we want to model      ( after Performing  Spatial-Adaptive-Epsilon )   
  REAL (pr)          :: eps_IC__AdpEpsSp           ! Allowable Minimum Value for Epsilon           ( after Performing  Spatial-Adaptive-Epsilon )   
  REAL (pr)          :: eps_min__AdpEpsSp          ! Allowable Minimum Value for Epsilon           ( after Performing  Spatial-Adaptive-Epsilon )   
  REAL (pr)          :: eps_max__AdpEpsSp          ! Allowable Maximum Value for Epsilon           ( after Performing  Spatial-Adaptive-Epsilon )   
  REAL (pr)          :: max_eps_scale__AdpEpsSp    ! Allowable Maximum Chang in (eps/eps_old)**2 - 1.0                                              
  REAL (pr)          :: Forcing_Factor__AdpEpsSp   ! Factor   of   Epsilon Change   due to   Forcing Term
  REAL (pr)          :: C_nu_art_EpsEvol           ! Artificial Diffusion Coefficient for  "Lagrangian Path-Line Diffusive Averaging Evolution Equation"  of  Epsilon   DefaultValue=0.0
  
  LOGICAL            :: initial_conditions__beforeTimeIntegration__IC_restart, post_process__beforeTimeIntegration__IC_restart, IC__AdpEpsSp__from_file

  INTEGER            :: Allocate_Status
  INTEGER            :: Interpolation_Order
  INTEGER            :: Choice__AdpEpsSp          ! DefaultValue = -1
                                                  ! Only in case that both  do_Adp_Eps_Spatial & do_Adp_Eps_Spatial_Evol  are .TRUE.  
                                                  ! 1- Adapt Grid based on  do_Adp_Eps_Spatial 
                                                  ! 2- Adapt Grid based on  do_Adp_Eps_Spatial_Evol     
                                                  
  INTEGER            :: Forcing_Type__AdpEpsSp    ! DefaultValue = -1 
                                                  !  1- TestingInterpolationModule   
                                                  !  5- ExternalFlow    
                                                  !  7- SCALES|LKM|RD|SGSD| Time-Average Forcing_Norm_Denom__av | Forcing_Factor__AdpEpsSp    
                                                  !  9- SCALES|LKM|RD|SGSD| Time-Average Forcing_Norm_Denom__av | Time-Scale based on Rate-of-Strain
                                                  ! 11- SCALES|LKM|RD|SGSD| SGSD/(SGSD+RD) - Goal               | Time-Scale based on Rate-of-Strain


  LOGICAL                              ::  do_varying_Goal
  INTEGER                              ::  teddy_interval_numbers
  INTEGER,   ALLOCATABLE, DIMENSION(:) ::  teddy_interval_end
  REAL (pr), ALLOCATABLE, DIMENSION(:) ::  Psgs_diss_goal__AdpEpsSp__varying, eps_min__AdpEpsSp__varying, eps_max__AdpEpsSp__varying


  !
  ! Other Specific Global Variables for
  ! Spatial Adaptive Epsilon
  ! which are Moved To  "MODULE pde"   (In order to be Global)
  !
  !LOGICAL                  :: do_Adp_Eps_Spatial         ! Perform  Spatial Adaptation  for  Epsilon
  !INTEGER                  :: n_var_Epsilon              ! Start Index of Epsilon (defined as an Additional Variable)  in u array 
  !REAL (pr), ALLOCATABLE   :: eps_in_SpatialSpace(:)     ! A Global Array for  eps_in_SpatialSpace(:)  [ NOTE :: This is in addition to  u(:,n_var_Epsilon) ]
  !LOGICAL, DIMENSION(:,:), ALLOCATABLE :: n_var_adapt_eps  ! Logical map into Variables in u(,) used in 
                                                            ! wavelet adaptation
                                                            ! using  spatial-variable-thresholding  to determine new adaptive grid

  !
  ! Case Dependent Variables for
  ! Initialization  of  Epsilon  
  !
  INTEGER                   :: IC_Type         ! Initial Condition Type  
                                               ! (0: Constant Everywhere,    1: Spot,    2: Sin,    3: Sin + Spot) 

  REAL (pr)                 :: X_Center__Spot
  REAL (pr)                 :: Y_Center__Spot
  REAL (pr)                 :: Z_Center__Spot
  REAL (pr)                 :: Radius__Spot  
  
  REAL (pr), DIMENSION(3,3) :: Vertex__Triangle    
  
  REAL (pr)                 :: Forcing_Norm_Denom__av, Forcing_Norm_Denom__av_Evol
  INTEGER                   :: Number__av, Number__av_Evol

  INTEGER, PARAMETER        :: UNIT_VT_STATS      = 1007   ! .p0_vt_stats
  INTEGER, PARAMETER        :: UNIT_VT_STATS_EVOL = 1008   ! .p0_vt_stats_Evol
  
    
  REAL (pr)                 :: tau_eps_min, tau_eps_max


  !Sptial Adaptive Epsilon variables based on S-A DDES/IDDES model
  LOGICAL   :: do_Adp_Eps_DDES
  LOGICAL   :: do_Adp_Eps_IDDES
  REAL (pr) :: eps_RANS, eps_LES

  !Sptial Adaptive Epsilon variables used for inviscid wall boundary
  INTEGER, DIMENSION(2,3) :: inviscid_wall_zone
  REAL (pr), DIMENSION(2,3) :: inviscid_zone_thickness
  REAL (pr) :: eps_uni, eps_r_h
  LOGICAL :: uni_base


CONTAINS





  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  SUBROUTINE user_read_input__VT ()
    IMPLICIT NONE
    INTEGER, DIMENSION(2*dim) :: zone_active  ! Temporary storage for parsing inputting inviscid zone flag
    REAL (pr), DIMENSION(2*dim) :: zone_input  ! Temporary storage for parsing inputting inviscid zone thickness
    INTEGER :: idim, shift


    ! Spatial Adaptive Epsilon
    do_Adp_Eps_Spatial = .FALSE. !default
    call input_logical ('do_Adp_Eps_Spatial',          do_Adp_Eps_Spatial,             'default',  ' do_Adp_Eps_Spatial')

    ! Spatial Adaptive Epsilon   Evolution
    do_Adp_Eps_Spatial_Evol = .FALSE. !default
    call input_logical ('do_Adp_Eps_Spatial_Evol',     do_Adp_Eps_Spatial_Evol,        'default',  ' do_Adp_Eps_Spatial_Evol')

    ! Spatial Adaptive Epsilon based on S-A DDES model
    do_Adp_Eps_DDES = .FALSE. !default
    call input_logical ('do_Adp_Eps_DDES',          do_Adp_Eps_DDES,             'default',  ' do_Adp_Eps_DDES')

    ! Spatial Adaptive Epsilon based on S-A IDDES model
    do_Adp_Eps_IDDES = .FALSE. !default
    call input_logical ('do_Adp_Eps_IDDES',          do_Adp_Eps_IDDES,             'default',  ' do_Adp_Eps_IDDES')

    !Sptial Adaptive Epsilon variables used for inviscid wall boundary
    zone_active = 0
    call input_integer_vector ('inviscid_wall_zone',zone_active(1:2*dim),2*dim,'default',' inviscid_wall_zone')
    inviscid_wall_zone = 0
    DO idim = 1,dim
      shift = (idim - 1)*2
      inviscid_wall_zone(1:2,idim) = zone_active(shift+1:shift+2)
    END DO
    zone_input = 0.0_pr
    call input_real_vector ('inviscid_zone_thickness',zone_input(1:2*dim),2*dim,'default',' inviscid_zone_thickness: inviscid zone thickness (xmin,xmax,ymin etc.) in computational domain')
    inviscid_zone_thickness = 0.0_pr
    DO idim = 1,dim
      shift = (idim - 1)*2
      inviscid_zone_thickness(1:2,idim) = zone_input(shift+1:shift+2)
    END DO
    uni_base = .FALSE. 
    call input_logical ('uni_base',  uni_base,  'default',     ' uni_base: T to use uniform eps as the base field')
    eps_r_h = 1.0e02_pr
    call input_real    ('eps_r_h',  eps_r_h,  'default',     ' eps_r_h: upper bound of relative eps (eps/epe_base)')

    Forcing_Type__AdpEpsSp = -1       !default
    do_varying_Goal        = .FALSE.  !default
    IF ( do_Adp_Eps_Spatial .OR. do_Adp_Eps_Spatial_Evol ) THEN
    call input_real    ('eps_IC__AdpEpsSp ',         eps_IC__AdpEpsSp ,         'stop',     ' eps_IC__AdpEpsSp ')
    IF (do_Adp_Eps_DDES .OR. do_Adp_Eps_IDDES) THEN
    call input_real    ('eps_RANS',        eps_RANS,         'stop',     ' eps_RANS')
    call input_real    ('eps_LES ',        eps_LES ,         'stop',     ' eps_LES ')
    ELSE IF (uni_base) THEN
    eps_uni = 1.0e-3_pr
    call input_real    ('eps_uni',  eps_uni,  'default',     ' eps_uni: uniform eps as the base field')
    ELSE
    call input_logical ('do_varying_Goal',           do_varying_Goal,           'default',  ' do_varying_Goal')
    call input_real    ('Psgs_diss_goal__AdpEpsSp',  Psgs_diss_goal__AdpEpsSp,  'stop',     ' Psgs_diss_goal__AdpEpsSp')
    call input_real    ('eps_min__AdpEpsSp',         eps_min__AdpEpsSp,         'stop',     ' eps_min__AdpEpsSp')
    call input_real    ('eps_max__AdpEpsSp ',        eps_max__AdpEpsSp ,        'stop',     ' eps_max__AdpEpsSp ')
    call input_real    ('max_eps_scale__AdpEpsSp',   max_eps_scale__AdpEpsSp,   'stop',     ' max_eps_scale__AdpEpsSp')
    call input_real    ('Forcing_Factor__AdpEpsSp',  Forcing_Factor__AdpEpsSp,  'stop',     ' Forcing_Factor__AdpEpsSp')
    call input_integer ('Forcing_Type__AdpEpsSp',    Forcing_Type__AdpEpsSp,    'stop',     ' Forcing_Type__AdpEpsSp')
    ENDIF
    ENDIF

    Interpolation_Order    = -1       
    !IF ( do_Adp_Eps_Spatial ) THEN
    IF ( do_Adp_Eps_Spatial .AND. .NOT. (do_Adp_Eps_DDES .OR. do_Adp_Eps_IDDES .OR. uni_base)) THEN
    call input_integer ('Interpolation_Order',    Interpolation_Order,    'stop',     ' Interpolation_Order')
    ENDIF


    IF ( do_varying_Goal ) THEN
    call input_integer ('teddy_interval_numbers',    teddy_interval_numbers,    'stop',     ' teddy_interval_numbers')

    IF( ALLOCATED(teddy_interval_end) )                DEALLOCATE (teddy_interval_end)
    IF( ALLOCATED(Psgs_diss_goal__AdpEpsSp__varying) ) DEALLOCATE (Psgs_diss_goal__AdpEpsSp__varying)
    IF( ALLOCATED(eps_min__AdpEpsSp__varying) )        DEALLOCATE (eps_min__AdpEpsSp__varying)
    IF( ALLOCATED(eps_max__AdpEpsSp__varying) )        DEALLOCATE (eps_max__AdpEpsSp__varying)
    ALLOCATE  (teddy_interval_end(1:teddy_interval_numbers),                STAT=Allocate_Status)
    ALLOCATE  (Psgs_diss_goal__AdpEpsSp__varying(1:teddy_interval_numbers), STAT=Allocate_Status)
    ALLOCATE  (eps_min__AdpEpsSp__varying(1:teddy_interval_numbers),        STAT=Allocate_Status)
    ALLOCATE  (eps_max__AdpEpsSp__varying(1:teddy_interval_numbers),        STAT=Allocate_Status)
    
    call input_integer_vector ('teddy_interval_end',                 teddy_interval_end(:),                teddy_interval_numbers,    'stop',     ' teddy_interval_end')
    call input_real_vector    ('Psgs_diss_goal__AdpEpsSp__varying',  Psgs_diss_goal__AdpEpsSp__varying(:), teddy_interval_numbers,    'stop',     ' Psgs_diss_goal__AdpEpsSp__varying')
    call input_real_vector    ('eps_min__AdpEpsSp__varying',         eps_min__AdpEpsSp__varying(:),        teddy_interval_numbers,    'stop',     ' eps_min__AdpEpsSp__varying')
    call input_real_vector    ('eps_max__AdpEpsSp__varying',         eps_max__AdpEpsSp__varying(:),        teddy_interval_numbers,    'stop',     ' eps_max__AdpEpsSp__varying')
    ENDIF


    C_nu_art_EpsEvol  = 0.0_pr !default
    IF ( do_Adp_Eps_Spatial_Evol ) THEN
    call input_real    ('C_nu_art_EpsEvol',          C_nu_art_EpsEvol,          'stop',     ' C_nu_art_EpsEvol')
    ENDIF


    Choice__AdpEpsSp  = -1 !default
    IF ( do_Adp_Eps_Spatial .AND. do_Adp_Eps_Spatial_Evol ) THEN
    call input_integer ('Choice__AdpEpsSp',          Choice__AdpEpsSp,          'stop',     ' Choice__AdpEpsSp')
    ENDIF


    IF ( do_Adp_Eps_Spatial .OR. do_Adp_Eps_Spatial_Evol ) THEN
    call input_integer ('IC_Type',     IC_Type,        'stop',  ' IC_Type')
    ! IC_Type ::  Initial Condition Type  (0: Constant Everywhere,    1: Spot,    2: Sin,    3: Sin + Spot,    4: Triangle + Spot) 

    IF ( (IC_Type == 1) .OR. (IC_Type == 4) ) THEN  
       call input_real ('X_Center__Spot', X_Center__Spot, 'stop',  ' X_Center__Spot')
       call input_real ('Y_Center__Spot', Y_Center__Spot, 'stop',  ' Y_Center__Spot')
       call input_real ('Z_Center__Spot', Z_Center__Spot, 'stop',  ' Z_Center__Spot')
       call input_real ('Radius__Spot',   Radius__Spot,   'stop',  ' Radius__Spot')
    ENDIF

    IF ( IC_Type == 4 ) THEN  
       call input_real_vector ('Vertex1__Triangle', Vertex__Triangle(1,:), dim, 'stop',  ' Vertex1__Triangle')
       call input_real_vector ('Vertex2__Triangle', Vertex__Triangle(2,:), dim, 'stop',  ' Vertex2__Triangle')
       call input_real_vector ('Vertex3__Triangle', Vertex__Triangle(3,:), dim, 'stop',  ' Vertex3__Triangle')
    ENDIF

    IF ( IC_restart_mode.EQ.3 ) & 
    call input_logical ('IC__AdpEpsSp__from_file',     IC__AdpEpsSp__from_file,        'stop',  ' IC__AdpEpsSp__from_file')

    IF ( (Forcing_Type__AdpEpsSp == 13) .OR. (Forcing_Type__AdpEpsSp == 15) ) &
       call input_real ('tau_eps_min', tau_eps_min, 'stop',  ' tau_eps_min')
    IF ( (Forcing_Type__AdpEpsSp == 14) .OR. (Forcing_Type__AdpEpsSp == 15) ) &
       call input_real ('tau_eps_max', tau_eps_max, 'stop',  ' tau_eps_max')

    ENDIF



    IF (IC_restart)  THEN
       initial_conditions__beforeTimeIntegration__IC_restart = .TRUE.
       post_process__beforeTimeIntegration__IC_restart       = .TRUE.
    ELSE
       initial_conditions__beforeTimeIntegration__IC_restart = .FALSE.
       post_process__beforeTimeIntegration__IC_restart       = .FALSE.
    ENDIF

  END SUBROUTINE user_read_input__VT










  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  SUBROUTINE  setup_VT () 
    USE parallel
    IMPLICIT NONE

    IF(do_Adp_Eps_Spatial)CALL register_var( 'Epsilon  ', &
      integrated=.FALSE., &  ! ERIC?
      adapt=(/.FALSE.,.FALSE./), &
      interpolate=(/.TRUE.,.TRUE./), &
      exact=(/.FALSE.,.FALSE./), &
      saved=.TRUE., &
      req_restart=.TRUE. )

    IF(do_Adp_Eps_Spatial_Evol)CALL register_var( 'Evolution  ', &
      integrated=.TRUE., &           ! ERIC?
      adapt=(/.FALSE.,.FALSE./), &   ! ERIC?
      interpolate=(/.TRUE.,.TRUE./), &
      exact=(/.FALSE.,.FALSE./), &
      saved=.TRUE., &
      req_restart=.FALSE. )


    !IF( do_Adp_Eps_Spatial ) THEN
    IF ( do_Adp_Eps_Spatial .AND. .NOT. (do_Adp_Eps_DDES .OR. do_Adp_Eps_IDDES .OR. uni_base)) THEN
       IF ( (Interpolation_Order .NE. 1) .AND. (Interpolation_Order .NE. 3) ) THEN
          IF (par_rank.EQ.0) THEN
          PRINT *, 'Error ::   Unknown  Interpolation_Order'
          PRINT *, '           Interpolation_Order = ', Interpolation_Order
          PRINT *,' Exiting ...'
          END IF
          CALL parallel_finalize
          STOP
       END IF
    END IF     



  END SUBROUTINE  setup_VT
  
  
  
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  SUBROUTINE  finalize_setup_VT () 
    USE parallel
    IMPLICIT NONE

    IF(do_Adp_Eps_Spatial)      n_var_epsilon = get_index('Epsilon  ')
    IF(do_Adp_Eps_Spatial_Evol) n_var_epsilonEvol = get_index('Evolution  ')

    !
    ! Allocate Logical map  for  spatial-variable-thresholding
    !
    ! n_var_adapt_eps = .FALSE.       ! intialize     Do NOT initialize.   Because if   do_Adp_Eps_Spatial=F    Then   n_var_adapt_eps   is NOT  Allocated  at all !!!! 
    IF( do_Adp_Eps_Spatial .OR. do_Adp_Eps_Spatial_Evol ) THEN 
      ALLOCATE (n_var_adapt_eps(1:n_var,0:1))
      n_var_adapt_eps = .TRUE.
    END IF

#ifdef DEBUG_VT
    IF( do_Adp_Eps_Spatial .OR. do_Adp_Eps_Spatial_Evol ) THEN  
    IF (par_rank.EQ.0) &
    PRINT *, 'n_var_adapt_eps       ',  n_var_adapt_eps
    ENDIF
#endif

    IF (verb_level.GT.1) THEN 
      IF( do_Adp_Eps_Spatial )      PRINT *, 'n_var_Epsilon       ::  ',    n_var_Epsilon
      IF( do_Adp_Eps_Spatial_Evol ) PRINT *, 'n_var_EpsilonEvol   ::  ',    n_var_EpsilonEvol 
    END IF

  END SUBROUTINE  finalize_setup_VT
  
  
  
  
  
  
  
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  SUBROUTINE user_initial_conditions__VT (u_local, nlocal, ne_local)!, t_local, scl, scl_fltwt, iter)
    USE parallel
    IMPLICIT NONE
    
    INTEGER,                                INTENT (IN)     ::  nlocal, ne_local
   !INTEGER  ,                              INTENT (IN)     ::  iter                   ! iteration of call while adapting initial grid
    REAL (pr), DIMENSION (nlocal,ne_local), INTENT (INOUT)  ::  u_local
   !REAL (pr), DIMENSION (1:n_var),         INTENT (IN)     ::  scl
   !REAL (pr),                              INTENT (IN)     ::  scl_fltwt
   !REAL (pr),                              INTENT (IN)     ::  t_local

    INTEGER                   ::  i    
    REAL (pr), DIMENSION (3)  ::  xx, yy, zz
    REAL (pr)                 ::  Area_V1V2V3, Area_PV1V2, Area_PV2V3, Area_PV3V1


    



    IF ( do_Adp_Eps_Spatial .OR. do_Adp_Eps_Spatial_Evol ) THEN

       IF( ALLOCATED(eps_in_SpatialSpace) ) DEALLOCATE (eps_in_SpatialSpace)
                                            ALLOCATE   (eps_in_SpatialSpace(1:nwlt), STAT=Allocate_Status)
    ENDIF






!IF (IC_restart .AND. it==0) THEN   !in the case of restart
!IF (  (IC_restart .OR. (IC_from_file .AND. IC_file_fmt == 0))  .AND. it==0)  THEN
!IF (  (IC_restart .OR. (IC_from_file .AND. IC_file_fmt == 0))  .AND. iwrite==IC_restart_station)  THEN
!IF (IC_restart .AND. iwrite==IC_restart_station)  THEN
IF (initial_conditions__beforeTimeIntegration__IC_restart .OR. post_process__beforeTimeIntegration__IC_restart)  THEN

    initial_conditions__beforeTimeIntegration__IC_restart = .FALSE.
    ! Spatial Adaptive Epsilon
    !IF( do_Adp_Eps_Spatial==.TRUE. ) THEN 
    !   eps_in_SpatialSpace(:)  =   u_local(:,n_var_Epsilon)
    !END IF
    


    !IF( do_Adp_Eps_Spatial_Evol==.TRUE. ) THEN 
    !   eps_in_SpatialSpace(:)  =   u_local(:,n_var_EpsilonEvol)
    !END IF    


!#ifdef DEBUG_VT
     IF (par_rank.EQ.0)  WRITE (*,'(A, I6, A, I6, A)')   'it = ', it,' iwrite = ', iwrite, ' IC_restart ... ::: user_initial_conditions__VT'
!#endif
   
        
ELSE 

   IF ( IC_restart_mode.EQ.3 .AND. IC__AdpEpsSp__from_file) THEN
!#ifdef DEBUG_VT
    IF (par_rank.EQ.0)  WRITE (*,'(A, I6, A, I6, A)')   'it = ', it,' iwrite = ', iwrite, ' IC__AdpEpsSp__from_file ... ::: user_initial_conditions__VT'
!#endif
    
   ELSE
!#ifdef DEBUG_VT
    IF (par_rank.EQ.0)  PRINT *, 'it= ', it, '  Perform  user_initial_conditions__VT'
!#endif


    
    IF( do_Adp_Eps_Spatial ) THEN  
    
    
       IF ( IC_Type == 0 ) THEN  
          DO i =1, nlocal
                u_local(i,n_var_Epsilon) = eps_IC__AdpEpsSp
          END DO
       END IF
    
    
    
       IF ( IC_Type == 1 ) THEN  
          DO i =1, nlocal
       
             IF       ( dim == 2 )  THEN
                  IF ( ((x(i,1)-X_Center__Spot)**2 + (x(i,2)-Y_Center__Spot)**2) <= Radius__Spot**2 ) THEN
                       u_local(i,n_var_Epsilon) = eps_min__AdpEpsSp
                  ELSE
                       u_local(i,n_var_Epsilon) = eps_max__AdpEpsSp
                  ENDIF
             ELSE IF  ( dim == 3 )  THEN
                  IF ( ((x(i,1)-X_Center__Spot)**2 + (x(i,2)-Y_Center__Spot)**2 + (x(i,3)-Z_Center__Spot)**2) <= Radius__Spot**2 ) THEN
                      u_local(i,n_var_Epsilon) = eps_min__AdpEpsSp
                  ELSE
                       u_local(i,n_var_Epsilon) = eps_max__AdpEpsSp
                  END IF
             END IF    
       
          END DO
       END IF
    
    
    
       IF ( IC_Type == 4 ) THEN  
    
                  ! Vertex1, Vertex2, Vertex3
                  xx = (/ Vertex__Triangle(1,1) , Vertex__Triangle(2,1) , Vertex__Triangle(3,1) /)
                  yy = (/ Vertex__Triangle(1,2) , Vertex__Triangle(2,2) , Vertex__Triangle(3,2) /)
           
                  Area_V1V2V3 = 0.5_pr * abs( xx(1)*yy(2) - xx(1)*yy(3) - xx(2)*yy(1) + xx(2)*yy(3) + xx(3)*yy(1) - xx(3)*yy(2) )
     
    
          DO i =1, nlocal
       
           ! IF   Area(Point, Vertex1, Vertex2) + Area(Point, Vertex2, Vertex3) + Area(Point, Vertex3, Vertex1) > Area(Vertex1, Vertex2, Vertex3)
           ! THEN Point is outside Traingle(Vertex1, Vertex2, Vetrex3)
           ! IF   < 
           ! THEN Point is inside  Traingle(Vertex1, Vertex2, Vetrex3)
           ! IF   = 
           ! THEN Point is on      Traingle(Vertex1, Vertex2, Vetrex3)
           !
           !                      |x1  y1  1|  
           ! Area(v1,v2,v3) = 1/2 |x2  y2  1|  =  1/2 ( x1.y2 - x1.y3 - x2.y1 + x2.y3 + x3.y1 - x3.y2 ) 
           !                      |x3  y3  1|
           !
                    
             IF       ( dim == 2 )  THEN
          
                  ! P, Vertex1, Vertex2
                  xx = (/ x(i,1) , Vertex__Triangle(1,1) , Vertex__Triangle(2,1) /)
                  yy = (/ x(i,2) , Vertex__Triangle(1,2) , Vertex__Triangle(2,2) /)
           
                  Area_PV1V2 = 0.5_pr * abs( xx(1)*yy(2) - xx(1)*yy(3) - xx(2)*yy(1) + xx(2)*yy(3) + xx(3)*yy(1) - xx(3)*yy(2) )
               
                  ! P, Vertex2, Vertex3
                  xx = (/ x(i,1) , Vertex__Triangle(2,1) , Vertex__Triangle(3,1) /)
                  yy = (/ x(i,2) , Vertex__Triangle(2,2) , Vertex__Triangle(3,2) /)
           
                  Area_PV2V3 = 0.5_pr * abs( xx(1)*yy(2) - xx(1)*yy(3) - xx(2)*yy(1) + xx(2)*yy(3) + xx(3)*yy(1) - xx(3)*yy(2) )
               
                  ! P, Vertex1, Vertex3
                  xx = (/ x(i,1) , Vertex__Triangle(1,1) , Vertex__Triangle(3,1) /)
                  yy = (/ x(i,2) , Vertex__Triangle(1,2) , Vertex__Triangle(3,2) /)
           
                  Area_PV3V1 = 0.5_pr * abs( xx(1)*yy(2) - xx(1)*yy(3) - xx(2)*yy(1) + xx(2)*yy(3) + xx(3)*yy(1) - xx(3)*yy(2) )
               
               
                  IF      ( ( Area_PV1V2 + Area_PV2V3 + Area_PV3V1 ) <= Area_V1V2V3 ) THEN
                              u_local(i,n_var_Epsilon) = eps_min__AdpEpsSp
                  ELSE IF ( ((x(i,1)-X_Center__Spot)**2 + (x(i,2)-Y_Center__Spot)**2) <= Radius__Spot**2 ) THEN
                              u_local(i,n_var_Epsilon) = eps_min__AdpEpsSp
                  ELSE             
                              u_local(i,n_var_Epsilon) = eps_max__AdpEpsSp
                  END IF
             ELSE IF  ( dim == 3 )  THEN
                    !!!!!!!!!!!!!!!!!!!
             END IF    
       
          END DO
       END IF


    END IF ! IF( do_Adp_Eps_Spatial )






    IF( do_Adp_Eps_Spatial_Evol ) THEN  

    
       IF ( IC_Type == 0 ) THEN  
          DO i =1, nlocal
                u_local(i,n_var_EpsilonEvol) = eps_IC__AdpEpsSp
          END DO
       END IF
    
    
    
       IF ( IC_Type == 1 ) THEN  
          DO i =1, nlocal
       
             IF       ( dim == 2 )  THEN
                  IF ( ((x(i,1)-X_Center__Spot)**2 + (x(i,2)-Y_Center__Spot)**2) <= Radius__Spot**2 ) THEN
                       u_local(i,n_var_EpsilonEvol) = eps_min__AdpEpsSp
                  ELSE
                       u_local(i,n_var_EpsilonEvol) = eps_max__AdpEpsSp
                  ENDIF
             ELSE IF  ( dim == 3 )  THEN
                  IF ( ((x(i,1)-X_Center__Spot)**2 + (x(i,2)-Y_Center__Spot)**2 + (x(i,3)-Z_Center__Spot)**2) <= Radius__Spot**2 ) THEN
                      u_local(i,n_var_EpsilonEvol) = eps_min__AdpEpsSp
                  ELSE
                       u_local(i,n_var_EpsilonEvol) = eps_max__AdpEpsSp
                  END IF
             END IF    
       
          END DO
       END IF



    END IF ! IF( do_Adp_Eps_Spatial_Evol )




   END IF !IF (IC_restart_mode .eq. 3) 
END IF  !IF (  (IC_restart  ....






    IF      ( do_Adp_Eps_Spatial .EQV. .TRUE.  .AND. do_Adp_Eps_Spatial_Evol .EQV. .FALSE. ) THEN  
                                                                                       eps_in_SpatialSpace(:) = u_local(:,n_var_Epsilon)
    ELSE IF ( do_Adp_Eps_Spatial .EQV. .FALSE. .AND. do_Adp_Eps_Spatial_Evol .EQV. .TRUE. )  THEN  
                                                                                       eps_in_SpatialSpace(:) = u_local(:,n_var_EpsilonEvol)
    ELSE IF ( do_Adp_Eps_Spatial .EQV. .TRUE.  .AND. do_Adp_Eps_Spatial_Evol .EQV. .TRUE. )  THEN  
                                                                                   SELECT CASE(Choice__AdpEpsSp)
                                                                                          CASE(1)
                                                                                             eps_in_SpatialSpace(:) = u_local(:,n_var_Epsilon)
                                                                                          CASE(2)
                                                                                             eps_in_SpatialSpace(:) = u_local(:,n_var_EpsilonEvol)
                                                                                          CASE DEFAULT
                                                                                             IF (par_rank.EQ.0) THEN
                                                                                             PRINT *, 'Error ::   Unknown  Variable Thresholding   Choice__AdpEpsSp  in  user_initial_conditions__VT()'
                                                                                             PRINT *, '           Choice__AdpEpsSp = ', Choice__AdpEpsSp
                                                                                             PRINT *,' Exiting ...'
                                                                                             END IF
                                                                                             CALL parallel_finalize
                                                                                             STOP
                                                                                          END SELECT
    END IF

    IF( do_Adp_Eps_Spatial ) THEN      
       Forcing_Norm_Denom__av  = 0.0_pr
       Number__av              = 0
    ENDIF
    IF( do_Adp_Eps_Spatial_Evol ) THEN      
       Forcing_Norm_Denom__av_Evol  = 0.0_pr
       Number__av_Evol              = 0
    ENDIF


  END SUBROUTINE user_initial_conditions__VT









  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  SUBROUTINE  user_post_process__VT (n_var__AdpEpsSp, Cf)
    USE parallel
!?!    USE db_tree 
    USE db_tree 
    USE field            ! In order to have access to global   u(:,1:dim) & u(:,n_var_Epsilon) & u(:,n_var_EpsilonEvol)
    USE error_handling

    IMPLICIT NONE

    INTEGER,                      INTENT (IN)            ::  n_var__AdpEpsSp
    REAL (pr),                    INTENT (IN), OPTIONAL  ::  Cf

    
    ! Spatial Adaptive Epsilon
    REAL (pr), DIMENSION (1, nwlt, dim)            ::  d__eps_in_SpatialSpace_old   ! 1st derivatives  of  epsilon  @ previous time_step
    REAL (pr), DIMENSION (1, nwlt, dim)            ::  d2__eps_in_SpatialSpace_old  ! 2nd derivatives  of  epsilon  @ previous time_step

    REAL (pr), DIMENSION (dim, nwlt)               ::  x_interpolation              ! Interpolation Location ::  ( x-u*dt ; y-v*dt ; z-w*dt )

    INTEGER,   DIMENSION (2)                       ::  var_array_interpolation
  
   !REAL (pr), DIMENSION (1, nwlt, dim)            ::  d__n_var_AdpEpsSp   ! 1st derivatives  of  u(n_var__AdpEpsSp)
   !REAL (pr), DIMENSION (1, nwlt, dim)            ::  d2__n_var_AdpEpsSp  ! 2nd derivatives  of  u(n_var__AdpEpsSp)
   !REAL (pr), DIMENSION (nwlt)                    ::  du_AdpEpsSp         ! sqrt( d__n_var_AdpEpsSp(1,:,1)**2  +  d__n_var_AdpEpsSp(1,:,2)**2 )

    REAL (pr)        ::  tmp1, tmp2, tmp3, tmp4, tmp5, tmp6, tmp7, tmp8, teddy_interval, nu_2
    INTEGER          ::  i, j, idim
    REAL (pr), DIMENSION (nwlt) ::  distance
  
!?! LOGICAL          :: var_mask(1:n_var)
    LOGICAL          :: var_mask(1:n_var)
    LOGICAL , SAVE   :: start = .TRUE.
    








    IF ( do_Adp_Eps_Spatial .OR. do_Adp_Eps_Spatial_Evol ) THEN

       IF( ALLOCATED(eps_in_SpatialSpace) ) DEALLOCATE (eps_in_SpatialSpace)
                                            ALLOCATE   (eps_in_SpatialSpace(1:nwlt), STAT=Allocate_Status)
    ENDIF







    ! Spatial Adaptive Epsilon
    IF( do_Adp_Eps_Spatial .EQV. .TRUE. ) THEN 
    
    
      !IF (t_local <= 0.01) THEN
      !IF (iwrite <= 1)     THEN
      !IF (it == 0 .OR. iwrite==IC_restart_station)     THEN
       IF (it == 0 .OR. post_process__beforeTimeIntegration__IC_restart)  THEN
!#ifdef DEBUG_VT
         IF (par_rank.EQ.0)  PRINT *, 'it= ', it, '  CALL user_initial_conditions__VT again for Eps'
!#endif
         CALL user_initial_conditions__VT(u, nwlt, n_var)        

         IF (do_Adp_Eps_DDES) THEN

           u(:,n_var_Epsilon) = eps_RANS + u(:,n_var__AdpEpsSp)*(eps_LES - eps_RANS)

           ! Bounding Epsilon 
           u(:,n_var_Epsilon) = MAX( eps_RANS,  MIN( eps_LES, u(:,n_var_Epsilon) ) )

         ELSE IF (do_Adp_Eps_IDDES) THEN

           u(:,n_var_Epsilon) = u(:,n_var__AdpEpsSp)*eps_RANS + (1.0_pr - u(:,n_var__AdpEpsSp))*eps_LES

           ! Bounding Epsilon 
           u(:,n_var_Epsilon) = MAX( eps_RANS,  MIN( eps_LES, u(:,n_var_Epsilon) ) )

         ELSE  IF (uni_base) THEN
         
           !uniform base field before eps filtering in the inviscid wall zone
           u(:,n_var_Epsilon) = eps_uni

         END IF
        
       ELSE    ! IF (it == 0)   ! IF (iwrite <= 1)   ! IF (t_local <= 0.01)
!#ifdef DEBUG_VT
         IF (par_rank.EQ.0)  PRINT *, 'it= ', it, '  Perform  user_post_process__VT  for Eps'
!#endif

       IF (do_Adp_Eps_DDES) THEN

       u(:,n_var_Epsilon) = eps_RANS + u(:,n_var__AdpEpsSp)*(eps_LES - eps_RANS)

       ! Bounding Epsilon 
       u(:,n_var_Epsilon) = MAX( eps_RANS,  MIN( eps_LES, u(:,n_var_Epsilon) ) )

       ELSE IF (do_Adp_Eps_IDDES) THEN

       u(:,n_var_Epsilon) = u(:,n_var__AdpEpsSp)*eps_RANS + (1.0_pr - u(:,n_var__AdpEpsSp))*eps_LES

       ! Bounding Epsilon 
       u(:,n_var_Epsilon) = MAX( eps_RANS,  MIN( eps_LES, u(:,n_var_Epsilon) ) )

       ELSE  IF (uni_base) THEN
       
       !uniform base field before eps filtering in the inviscid wall zone
       u(:,n_var_Epsilon) = eps_uni

       ELSE

       eps_in_SpatialSpace(:)     =  u(:,n_var_Epsilon)       
       var_array_interpolation(1) = 1




       ! Find 1st and 2nd  deriviative of  epsilon 
       CALL c_diff_fast(eps_in_SpatialSpace, &
!?!       CALL c_diff_fast(u(:,n_var_Epsilon), &
                        d__eps_in_SpatialSpace_old(1,:,:), &
                        d2__eps_in_SpatialSpace_old(1,:,:), &
                        j_lev, nwlt, 1, 11, &                      !meth=1
                        1 , 1, 1)
                        !ne , 1:dim, ne )
                        
       !Examples of How to Call  c_diff_fast(...)
       !CALL c_diff_fast(u_integrated, du, d2u, j_lev, ng, 1, 10, ne, 1, ne)
       !CALL c_diff_fast(u, du(1:ne,:,:), d2u(1:ne,:,:), j_lev, ng, meth, 10, ne , 1, ne )




       ! Finding Interpolation Location ::  ( x-u*dt ; y-v*dt ; z-w*dt )
       !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
       !!! N O T E ::   
       !!!                                   Dimension of  x_interpolation      is(1:dim,1:nwlt)
       !!!                                                                                        Although
       !!!                                   Dimension of  x ((global x array)) is(1:nwlt,1:dim)
       DO i =1, dim
          Do j=1, nwlt
             IF       ( (x(j, i) - dt*u(j, i)) < xyzlimits(1,i) )     THEN
                  x_interpolation(i, j) = xyzlimits(2,i) - abs( xyzlimits(1,i) - (x(j, i) - dt*u(j, i)) )
             ELSE IF  ( (x(j, i) - dt*u(j, i)) > xyzlimits(2,i) )     THEN
                  x_interpolation(i, j) = xyzlimits(1,i) + abs( xyzlimits(2,i) - (x(j, i) - dt*u(j, i)) )
             ELSE
                  x_interpolation(i, j) = x(j, i) - dt*u(j, i)
             END IF
          END DO
       
          !!!!!!!!   I M P L E M E N T    T H I S 
          !x_interpolation(i, j) = xyzlimits(1,:)+MOD(x(j, :)-xyzlimits(1,:)- dt*u(j, :), xyzlimits(2,:)-xyzlimits(1,:))

       END DO
       !!! N O T E ::   (( In Parallel ))
       !!!                                
       !!!                                 Question ::   -dt*u(:, 1:dim)   may point to the locations not on myRank ?
       !!!                                
       !!!                                 Answer   ::   very unlikely !!
       !!!
       !!!                                               User(s) must make sure that   -dt*u(:, 1:dim)  is small enough
       !!!                                               so that it lay within the  Boundary_Zone of  myRank
       !!!
       !!!                                               In general, 1) CFL has been checked by the "Highest Level of Resolution"  (i.e. smallest dx);
       !!!                                                              consequently,  dt*u(:, 1:dim)  should be small enough.
       !!!
       !!!                                                           2) Each processes has its own  Boundary_Zone  and  J_tree nodes;
       !!!                                                              therefore,  x - dt*u(:, 1:dim)  should lay within the  Boundary_Zone
       !!!                                                                          provided that  dt*u(:, 1:dim)  is small enough.
       !!! 
       !!!                                               Otherwise ....  :(
       !!!                                
#ifdef DEBUG_INTERPOL
       !IF (par_rank.EQ.0) THEN
       PRINT *, ' ?????????????????? Before Interpolation ??????????????? '
       PRINT *, ' Process  ::: ', par_rank
       IF     (dim==2) THEN
       WRITE (*,'( " MinVAL  x - dt*u(:, 1:dim) ", 2(es15.7), " " )') MINVAL(x_interpolation(1,:)), MINVAL(x_interpolation(2,:))
       WRITE (*,'( " MAxVAL  x - dt*u(:, 1:dim) ", 2(es15.7), " " )') MAXVAL(x_interpolation(1,:)), MAXVAL(x_interpolation(2,:))
       PRINT *, ' ??????????????????????????????????????????????????????? '
       ELSEIF (dim==3) THEN
       WRITE (*,'( " MinVAL  x - dt*u(:, 1:dim) ", 3(es15.7), " " )') MINVAL(x_interpolation(1,:)), MINVAL(x_interpolation(2,:)), MINVAL(x_interpolation(3,:))
       WRITE (*,'( " MAxVAL  x - dt*u(:, 1:dim) ", 3(es15.7), " " )') MAXVAL(x_interpolation(1,:)), MAXVAL(x_interpolation(2,:)), MAXVAL(x_interpolation(3,:))
       PRINT *, ' ??????????????????????????????????????????????????????? '
       ENDIF
       !ENDIF
       !#undef DEBUG_INTERPOL
#endif




       
       
       ! Bounding Epsilon 
       !Do i=1, nwlt
       !   eps_in_SpatialSpace(i) = MAX( eps_min__AdpEpsSp,  MIN( eps_max__AdpEpsSp, eps_in_SpatialSpace(i) ) )
       !END DO

!?!        var_mask  = .TRUE.
!?!        CALL request_known_list (var_mask, n_var, list_sig, j_lev)


       ! Interpolate Epsilon_Old   At the   Location (x �u*dt; y-v*dt; z-w*dt)
       ! interpolate ( u, du, nwlt, n_var, x_size, x, interpolation_order, var_size, var, res )   in   wavelet_filters.f90
       CALL interpolate ( eps_in_SpatialSpace, &                                   ! u(1:nwlt, 1:n_var)                        ! function
!?!       CALL interpolate ( u(:,n_var_Epsilon), &                                   ! u(1:nwlt, 1:n_var)                        ! function
                          d__eps_in_SpatialSpace_old, &                            ! du(1:n_var,1:nwlt,1:dim)                  ! derivatives
                          nwlt, 1, nwlt, &                                         ! nwlt, n_var, x_size
                          x_interpolation, &                                       ! x(1:dim,1:x_size)                         ! points to interpolate into
                          Interpolation_Order, &                                   ! interpolation_order                       ! (order: 0, 1, 3) 
                          1, &                                                     ! var_size
                          var_array_interpolation, &                               ! var(1:var_size)                           ! variables to interpolate for
                          eps_in_SpatialSpace)                                     ! res(1:var_size,1:x_size)                  ! interpolation result





       IF (do_varying_Goal) THEN
           teddy_interval =  1.0_pr / ( 3.0_pr * Cf )  ! OR   10t_eddy= 10.0_pr / ( 3.0_pr * Cf )   OR   5t_eddy= 5.0_pr / ( 3.0_pr * Cf )
           
           IF ( floor(t / teddy_interval) > teddy_interval_end(teddy_interval_numbers) ) THEN
              IF (par_rank.EQ.0) THEN
                 PRINT *, 'Error ::   Unknown   time   for   varying_Goal   in   user_post_process__VT()'
                 WRITE(*,'(3(A,F25.16))') 't=', t, '   teddy_interval=', teddy_interval, '   Cf=', Cf
                 PRINT *,' Exiting ...'
              END IF
              CALL parallel_finalize
              STOP
           END IF

           i = MAXVAL(MINLOC(teddy_interval_end , mask = teddy_interval_end .GT. floor(t / teddy_interval) ))
           Psgs_diss_goal__AdpEpsSp = Psgs_diss_goal__AdpEpsSp__varying(i)
           eps_min__AdpEpsSp        = eps_min__AdpEpsSp__varying(i)
           eps_max__AdpEpsSp        = eps_max__AdpEpsSp__varying(i)
           IF (par_rank.EQ.0) WRITE(*,'(" VT:  PSGSD_Goal/eps_min/eps_max/teddy_interval/(#)    = ",4F12.5,A,I2,A)') &
                              Psgs_diss_goal__AdpEpsSp__varying(i), eps_min__AdpEpsSp__varying(i), eps_max__AdpEpsSp__varying(i), teddy_interval, '   (',i,')'
           
       END IF





       ! To Prevent  "Buildup of Scales"  OR  "Loosing some Scales" ::
       ! Track the eps within a Lagrangian frame ::
       !
       ! The "Lagrangian Path-Line Diffusive Averaging Evolution" equation for eps can be expresses as follow:
       
       SELECT CASE(Forcing_Type__AdpEpsSp)
              CASE(0)
              !!!!  For case ::   /Simulations/TestingInterpolationModule/2D3D_Evolution_AdpEpsSpatial__Parallel/
              !!!!
              u(:,n_var_Epsilon)       =   eps_in_SpatialSpace(:)  -  eps_in_SpatialSpace(:) * dt * Forcing_Factor__AdpEpsSp 
!?!              u(:,n_var_Epsilon)       =   u(:,n_var_Epsilon)  -  u(:,n_var_Epsilon) * dt * Forcing_Factor__AdpEpsSp 


              CASE(1)
              !!!!  For case ::   /Simulations/TestingInterpolationModule/2Evolution_AdpEpsSpatial_6__changes_in_MainCode/6_1__MODULE_variable_thresholding/
              !!!!  For case ::   /Simulations/TestingInterpolationModule/3Evolution_AdpEpsSpatial_6__changes_in_MainCode/
              !!!!
              tmp1 = MAXVAL(u(:,n_var__AdpEpsSp))
              CALL parallel_global_sum( REALMAXVAL=tmp1 )
#ifdef DEBUG_VT
              IF (par_rank.EQ.0) PRINT *, 'VT:  MAX( u(:,n_var__AdpEpsSp) )  = ', tmp1
#endif           
                
             !u(:,n_var_Epsilon)       =   eps_in_SpatialSpace(:)  -  eps_in_SpatialSpace(:) * dt * Forcing_Factor__AdpEpsSp * ( abs(u(:,n_var__AdpEpsSp)/MAXVAL(u(:,n_var__AdpEpsSp))) - Psgs_diss_goal__AdpEpsSp ) !* (Psgs_diss__Global - Psgs_diss_goal__AdpEpsSp)
              u(:,n_var_Epsilon)       =   eps_in_SpatialSpace(:)  -  eps_in_SpatialSpace(:) * dt * Forcing_Factor__AdpEpsSp * ( abs(u(:,n_var__AdpEpsSp)/tmp1) - Psgs_diss_goal__AdpEpsSp ) 


              CASE(2)
              !!!!  For case ::   /Simulations/2D_varThreshold/ExternalFlow__AdpEpsSpatial/
              !!!!u(:,n_var_Epsilon)       =   eps_in_SpatialSpace(:)  -  eps_in_SpatialSpace(:) * dt * Forcing_Factor__AdpEpsSp * ( abs(u(:,n_var_modvort)/MAXVAL(u(:,n_var_modvort))) - Psgs_diss_goal__AdpEpsSp ) 


              CASE(3)
              !!!!  For any case 
              !u(:,n_var_Epsilon)       =   eps_in_SpatialSpace(:)  -  eps_in_SpatialSpace(:) * dt * Forcing_Factor__AdpEpsSp * ( abs(u(:,n_var__AdpEpsSp))/MAXVAL(abs(u(:,n_var__AdpEpsSp))) - Psgs_diss_goal__AdpEpsSp ) 
              !u(:,n_var_Epsilon)       =   eps_in_SpatialSpace(:)  -  eps_in_SpatialSpace(:) * dt * Forcing_Factor__AdpEpsSp * ( abs(u(:,n_var__AdpEpsSp)/MAXVAL(u(:,n_var__AdpEpsSp))) - Psgs_diss_goal__AdpEpsSp ) 


              CASE(4)
              ! Find 1st and 2nd  deriviative of  n_var__AdpEpsSp 
              !CALL c_diff_fast(u(:,n_var__AdpEpsSp), &
              !                 d__n_var_AdpEpsSp(1,:,:), &
              !                 d2__n_var_AdpEpsSp(1,:,:), &
              !                 j_lev, nwlt, 1, 11, &                      !meth=1
              !                 1 , 1, 1)                                  !ne , 1:dim, ne )
                        
              !du_AdpEpsSp(:) = sqrt( d__n_var_AdpEpsSp(1,:,1)**2  +  d__n_var_AdpEpsSp(1,:,2)**2 )
              !PRINT *, 'MAXVAL(du_AdpEpsSp(:))  ::::  ',    MAXVAL(du_AdpEpsSp(:)) 

              !u(:,n_var_Epsilon)       =   eps_in_SpatialSpace(:)  -  eps_in_SpatialSpace(:) * dt * Forcing_Factor__AdpEpsSp * ( abs(du_AdpEpsSp(:)/MAXVAL(du_AdpEpsSp(:))) - Psgs_diss_goal__AdpEpsSp ) 
              !u(:,6) = du_AdpEpsSp(:)


              CASE(5)
              !!!!  For case ::   External Flow
              !Forcing_Norm_Denom__av = ( Forcing_Norm_Denom__av * REAL(Number__av,pr) + MAXVAL(abs(u(:,n_var__AdpEpsSp))) )  /  REAL(Number__av+1,pr)
              !Number__av = Number__av + 1
              !u(:,n_var_Epsilon)       =   eps_in_SpatialSpace(:)  -  eps_in_SpatialSpace(:) * dt * Forcing_Factor__AdpEpsSp * ( abs(u(:,n_var__AdpEpsSp))/Forcing_Norm_Denom__av - Psgs_diss_goal__AdpEpsSp ) 
              !#ifdef DEBUG_VT
              !IF (par_rank.EQ.0)
              !PRINT *, 'Number__av  ::::  ',                           Number__av 
              !PRINT *, 'Forcing_Norm_Denom__av  ::::  ',               Forcing_Norm_Denom__av 
              !PRINT *, 'MAXVAL(abs(u(:,n_var__AdpEpsSp)))  ::::  ',    MAXVAL(abs(u(:,n_var__AdpEpsSp))) 
              !ENDIF
              !#endif


              !CASE(66)
              !IF(total_sgs_diss__Global > 0.0_pr) THEN
              !u(:,n_var_Epsilon)      =   eps_in_SpatialSpace(:) - dt *Psgs_diss_goal__AdpEpsSp !* (Psgs_diss__Global - Psgs_diss_goal__AdpEpsSp)
              !
              !  NOTE :: The following two Global variables  are declared and assigned in  "sgs_incompressible.f90"   so that we can have access to them here.
              !  REAL(pr), PUBLIC :: Psgs_diss__Global
              !  REAL(pr), PUBLIC :: total_sgs_diss__Global

              !u(:,n_var_Epsilon)      =   eps_in_SpatialSpace(:) - dt *( ( u(:,n_var_ExtraAdpt_Der) - MAX(u(:,n_var_ExtraAdpt_Der) ) / MAX(u(:,n_var_ExtraAdpt_Der) ) !* (Psgs_diss__Global - Psgs_diss_goal__AdpEpsSp)


              CASE(6)
              !!!!  For case ::   /Simulations/ForcedHomogeneousTurbulence/SCALES__AdpEpsSpatial/GDM/
              !Forcing_Norm_Denom__av = ( Forcing_Norm_Denom__av * REAL(Number__av,pr) + MAXVAL(abs(u(:,n_var__AdpEpsSp))) )  /  REAL(Number__av+1,pr)
              !Number__av = Number__av + 1
              !u(:,n_var_Epsilon)       =   eps_in_SpatialSpace(:)  -  eps_in_SpatialSpace(:) * dt * Forcing_Factor__AdpEpsSp * ( abs(u(:,n_var__AdpEpsSp))/Forcing_Norm_Denom__av - Psgs_diss_goal__AdpEpsSp ) 
              !#ifdef DEBUG_VT
              !IF (par_rank.EQ.0)
              !PRINT *, 'Number__av  ::::  ',                           Number__av 
              !PRINT *, 'Forcing_Norm_Denom__av  ::::  ',               Forcing_Norm_Denom__av 
              !PRINT *, 'MAXVAL(abs(u(:,n_var__AdpEpsSp)))  ::::  ',    MAXVAL(abs(u(:,n_var__AdpEpsSp))) 
              !ENDIF
              !#endif


              CASE(7)
              !!!!  For case ::   /Simulations/ForcedHomogeneousTurbulence/SCALES__AdpEpsSpatial/LKM/RSGSD/
              !!!!  SCALES|LKM|RD|SGSD| Time-Average Forcing_Norm_Denom__av | Forcing_Factor__AdpEpsSp
              !!!!
              tmp1 = MAXVAL(abs( u(:,n_var__AdpEpsSp) - u(:,n_var_RD)*Psgs_diss_goal__AdpEpsSp/(1-Psgs_diss_goal__AdpEpsSp) ))
              CALL parallel_global_sum( REALMAXVAL=tmp1 )
#ifdef DEBUG_VT
              IF (par_rank.EQ.0) PRINT *, 'VT:  Max| SGSD - RD*Goal/(1-Goal) |  = ', tmp1
#endif             
              
             !Forcing_Norm_Denom__av = ( Forcing_Norm_Denom__av * REAL(Number__av,pr) + MAXVAL(abs( u(:,n_var__AdpEpsSp) - u(:,n_var_RD)*Psgs_diss_goal__AdpEpsSp/(1-Psgs_diss_goal__AdpEpsSp) )) )  /  REAL(Number__av+1,pr)
              Forcing_Norm_Denom__av = ( Forcing_Norm_Denom__av * REAL(Number__av,pr) + tmp1 )  /  REAL(Number__av+1,pr)
              Number__av = Number__av + 1
              u(:,n_var_Epsilon)       =   eps_in_SpatialSpace(:)  -  eps_in_SpatialSpace(:) * dt * Forcing_Factor__AdpEpsSp * ( ( u(:,n_var__AdpEpsSp) - u(:,n_var_RD)*Psgs_diss_goal__AdpEpsSp/(1-Psgs_diss_goal__AdpEpsSp) )  /  Forcing_Norm_Denom__av )

#ifdef DEBUG_VT
              tmp1 = MINVAL( ( u(:,n_var__AdpEpsSp) - u(:,n_var_RD)*Psgs_diss_goal__AdpEpsSp/(1-Psgs_diss_goal__AdpEpsSp) )  /  Forcing_Norm_Denom__av )
              tmp2 = MAXVAL( ( u(:,n_var__AdpEpsSp) - u(:,n_var_RD)*Psgs_diss_goal__AdpEpsSp/(1-Psgs_diss_goal__AdpEpsSp) )  /  Forcing_Norm_Denom__av )
              CALL parallel_global_sum( REALMINVAL=tmp1 )
              CALL parallel_global_sum( REALMAXVAL=tmp2 )

              IF (par_rank.EQ.0) PRINT *, 'VT:  (Min,Max)( SGSD - RD*Goal/(1-Goal) )/Time-Average  ::::   ', tmp1, tmp2
#endif
           
              !u(:,n_var_Epsilon)       =   eps_in_SpatialSpace(:)  -  eps_in_SpatialSpace(:) * dt * Forcing_Factor__AdpEpsSp * ( u(:,n_var__AdpEpsSp) - u(:,n_var_RD)*Psgs_diss_goal__AdpEpsSp/(1-Psgs_diss_goal__AdpEpsSp) ) 
              !PRINT *, 'MAXVAL( SGSD - RD*Goal/(1-Goal) )  ::::   ', MAXVAL( u(:,n_var__AdpEpsSp) - u(:,n_var_RD)*Psgs_diss_goal__AdpEpsSp/(1-Psgs_diss_goal__AdpEpsSp) )
              !PRINT *, 'MINVAL( SGSD - RD*Goal/(1-Goal) )  ::::   ', MINVAL( u(:,n_var__AdpEpsSp) - u(:,n_var_RD)*Psgs_diss_goal__AdpEpsSp/(1-Psgs_diss_goal__AdpEpsSp) )

              ! u(:,n_var_Epsilon)       =   eps_in_SpatialSpace(:)  -  eps_in_SpatialSpace(:) * 0.03 * sign(1.0_pr ,  u(:,n_var__AdpEpsSp) - u(:,n_var_RD)*Psgs_diss_goal__AdpEpsSp/(1-Psgs_diss_goal__AdpEpsSp)) 
              ! 0.03 ~=  t_eddy / 2
  
  


              CASE(9)
              !!!!  For case ::   /Simulations/ForcedHomogeneousTurbulence/SCALES__AdpEpsSpatial/LKM/RSGSD/
              !!!!  SCALES|LKM|RD|SGSD| Time-Average Forcing_Norm_Denom__av | Time-Scale based on Rate-of-Strain
              !!!!
              tmp1 = MAXVAL(abs( u(:,n_var__AdpEpsSp) - u(:,n_var_RD)*Psgs_diss_goal__AdpEpsSp/(1-Psgs_diss_goal__AdpEpsSp) ))
              CALL parallel_global_sum( REALMAXVAL=tmp1 )
#ifdef DEBUG_VT
              IF (par_rank.EQ.0) PRINT *, 'VT:  Max| SGSD - RD*Goal/(1-Goal) |  = ', tmp1
#endif             
              
             !Forcing_Norm_Denom__av = ( Forcing_Norm_Denom__av * REAL(Number__av,pr) + MAXVAL(abs( u(:,n_var__AdpEpsSp) - u(:,n_var_RD)*Psgs_diss_goal__AdpEpsSp/(1-Psgs_diss_goal__AdpEpsSp) )) )  /  REAL(Number__av+1,pr)
              Forcing_Norm_Denom__av = ( Forcing_Norm_Denom__av * REAL(Number__av,pr) + tmp1 )  /  REAL(Number__av+1,pr)
              Number__av = Number__av + 1
              u(:,n_var_Epsilon)       =   eps_in_SpatialSpace(:)  -  eps_in_SpatialSpace(:) * dt* ( u(:,n_var__AdpEpsSp) - u(:,n_var_RD)*Psgs_diss_goal__AdpEpsSp/(1-Psgs_diss_goal__AdpEpsSp) ) / EpsSpatial_Forcing_TimeScale


              tmp1 = MINVAL( u(:,n_var__AdpEpsSp) / ( u(:,n_var__AdpEpsSp) + u(:,n_var_RD) ) )
              tmp2 = MAXVAL( u(:,n_var__AdpEpsSp) / ( u(:,n_var__AdpEpsSp) + u(:,n_var_RD) ) )
              tmp3 = SUM(    u(:,n_var__AdpEpsSp) / ( u(:,n_var__AdpEpsSp) + u(:,n_var_RD) ) ) / nwlt
              CALL parallel_global_sum( REALMINVAL=tmp1 )
              CALL parallel_global_sum( REALMAXVAL=tmp2 )
              CALL parallel_global_sum( REAL=tmp3 )
              IF (par_rank.EQ.0) THEN
              PRINT *,  ' VT:  EpsSpatial_Forcing_TimeScale       = ', EpsSpatial_Forcing_TimeScale
              PRINT *,  ' VT:  Forcing_Norm_Denom__av             = ', Forcing_Norm_Denom__av
              WRITE(*,'(" VT:  (Min,Max,Mean)( SGSD/(SGSD+RD) )   = ",E12.5,E12.5,E12.5)') tmp1, tmp2, tmp3
              END IF


#ifdef DEBUG_VT
              tmp1 = MINVAL( ( u(:,n_var__AdpEpsSp) - u(:,n_var_RD)*Psgs_diss_goal__AdpEpsSp/(1-Psgs_diss_goal__AdpEpsSp) )  /  Forcing_Norm_Denom__av )
              tmp2 = MAXVAL( ( u(:,n_var__AdpEpsSp) - u(:,n_var_RD)*Psgs_diss_goal__AdpEpsSp/(1-Psgs_diss_goal__AdpEpsSp) )  /  Forcing_Norm_Denom__av )
              CALL parallel_global_sum( REALMINVAL=tmp1 )
              CALL parallel_global_sum( REALMAXVAL=tmp2 )

              IF (par_rank.EQ.0) PRINT *, 'VT:  (Min,Max)( SGSD - RD*Goal/(1-Goal) )/Time-Average  ::::   ', tmp1, tmp2
#endif

  


              CASE(11)
              !!!!  For case ::   /Simulations/ForcedHomogeneousTurbulence/SCALES__AdpEpsSpatial/LKM/RSGSD/
              !!!!  SCALES|LKM|RD|SGSD| SGSD/(SGSD+RD) - Goal | Time-Scale based on Rate-of-Strain
              !!!!
              
              u(:,n_var_Epsilon)       =   eps_in_SpatialSpace(:)  -  eps_in_SpatialSpace(:) * dt* ( u(:,n_var_SGSD)/(u(:,n_var_SGSD)+u(:,n_var_RD))  -  Psgs_diss_goal__AdpEpsSp ) * EpsSpatial_Forcing_TimeScale


#ifdef DEBUG_VT
              tmp1 = MINVAL( u(:,n_var_SGSD) / ( u(:,n_var_SGSD) + u(:,n_var_RD) ) )
              tmp2 = MAXVAL( u(:,n_var_SGSD) / ( u(:,n_var_SGSD) + u(:,n_var_RD) ) )
              tmp3 = SUM(    u(:,n_var_SGSD) / ( u(:,n_var_SGSD) + u(:,n_var_RD) ) ) !/ nwlt
              tmp4 = SUM(    u(:,n_var_SGSD) / ( u(:,n_var_SGSD) + u(:,n_var_RD) ) * dA(:) )
              CALL parallel_global_sum( REALMINVAL=tmp1 )
              CALL parallel_global_sum( REALMAXVAL=tmp2 )
              CALL parallel_global_sum( REAL=tmp3 )
              CALL parallel_global_sum( REAL=tmp4 )
              tmp3 = tmp3 / nwlt_global
              tmp4 = tmp4 / sumdA_global
              IF (par_rank.EQ.0) THEN
              WRITE(*,'(" VT:  EpsSpatial_Forcing_TimeScale                     = ",F12.5)')                   EpsSpatial_Forcing_TimeScale
              WRITE(*,'(" VT:  (Min,Max,Mean,VolumeAverage)( SGSD/(SGSD+RD) )   = ",F12.5,F12.5,F12.5,F12.5)') tmp1, tmp2, tmp3, tmp4
              END IF
#endif

  


              CASE(12)
              !!!!  For case ::   /Simulations/ForcedHomogeneousTurbulence/SCALES__AdpEpsSpatial/LKM/RSGSD/
              !!!!  SCALES|LKM|RD|SGSD| SGSD/(SGSD+RD) - Goal | Local Time-Scale based on Local Rate-of-Strain
              !!!!
              
              nu_2 = sqrt(0.5_pr/nu)
              u(:,n_var_Epsilon)       =   eps_in_SpatialSpace(:)  -  eps_in_SpatialSpace(:) * dt* ( u(:,n_var_SGSD)/(u(:,n_var_SGSD)+u(:,n_var_RD))  -  Psgs_diss_goal__AdpEpsSp ) * nu_2 * sqrt(u(:,n_var_RD))
              

#ifdef DEBUG_VT
              tmp1 = MINVAL( u(:,n_var_SGSD) / ( u(:,n_var_SGSD) + u(:,n_var_RD) ) )
              tmp2 = MAXVAL( u(:,n_var_SGSD) / ( u(:,n_var_SGSD) + u(:,n_var_RD) ) )
              tmp3 = SUM(    u(:,n_var_SGSD) / ( u(:,n_var_SGSD) + u(:,n_var_RD) ) ) !/ nwlt
              tmp4 = SUM(    u(:,n_var_SGSD) / ( u(:,n_var_SGSD) + u(:,n_var_RD) ) * dA(:) )
              CALL parallel_global_sum( REALMINVAL=tmp1 )
              CALL parallel_global_sum( REALMAXVAL=tmp2 )
              CALL parallel_global_sum( REAL=tmp3 )
              CALL parallel_global_sum( REAL=tmp4 )
              tmp3 = tmp3 / nwlt_global
              tmp4 = tmp4 / sumdA_global
              IF (par_rank.EQ.0) THEN
              WRITE(*,'(" VT:  EpsSpatial_Forcing_TimeScale                     = ",F12.5)')                   EpsSpatial_Forcing_TimeScale
              WRITE(*,'(" VT:  (Min,Max,Mean,VolumeAverage)( SGSD/(SGSD+RD) )   = ",F12.5,F12.5,F12.5,F12.5)') tmp1, tmp2, tmp3, tmp4
              END IF
#endif
           
  




              CASE(13)
              !!!!  For case ::   /Simulations/ForcedHomogeneousTurbulence/SCALES__AdpEpsSpatial/LKM/RSGSD/
              !!!!  SCALES|LKM|RD|SGSD| SGSD/(SGSD+RD) - Goal | Local Time-Scale based on Local Rate-of-Strain   +  Cap \tau_{{\epsilon}_{min}}  to  tau_eps_min * \tau_{\rm eddy}
              !!!!
              
              nu_2 = sqrt(0.5_pr/nu)
              u(:,n_var_Epsilon)       =   eps_in_SpatialSpace(:)  -  eps_in_SpatialSpace(:) * dt* ( u(:,n_var_SGSD)/(u(:,n_var_SGSD)+u(:,n_var_RD))  -  Psgs_diss_goal__AdpEpsSp ) * max( tau_eps_min*Cf, nu_2 * sqrt(u(:,n_var_RD)) )
           
  




              CASE(14)
              !!!!  For case ::   /Simulations/ForcedHomogeneousTurbulence/SCALES__AdpEpsSpatial/LKM/RSGSD/
              !!!!  SCALES|LKM|RD|SGSD| SGSD/(SGSD+RD) - Goal | Local Time-Scale based on Local Rate-of-Strain   +  Cap \tau_{{\epsilon}_{max}}  to  tau_eps_max * \tau_{\rm eddy}
              !!!!
              
              nu_2 = sqrt(0.5_pr/nu)
              u(:,n_var_Epsilon)       =   eps_in_SpatialSpace(:)  -  eps_in_SpatialSpace(:) * dt* ( u(:,n_var_SGSD)/(u(:,n_var_SGSD)+u(:,n_var_RD))  -  Psgs_diss_goal__AdpEpsSp ) * min( tau_eps_max*Cf, nu_2 * sqrt(u(:,n_var_RD)) )
           
  




              CASE(15)
              !!!!  For case ::   /Simulations/ForcedHomogeneousTurbulence/SCALES__AdpEpsSpatial/LKM/RSGSD/
              !!!!  SCALES|LKM|RD|SGSD| SGSD/(SGSD+RD) - Goal | Local Time-Scale based on Local Rate-of-Strain   +  Cap \tau_{\epsilon}  to  [tau_eps_min, tau_eps_max] * \tau_{\rm eddy}
              !!!!
              
              nu_2 = sqrt(0.5_pr/nu)
              u(:,n_var_Epsilon)       =   eps_in_SpatialSpace(:)  -  eps_in_SpatialSpace(:) * dt* ( u(:,n_var_SGSD)/(u(:,n_var_SGSD)+u(:,n_var_RD))  -  Psgs_diss_goal__AdpEpsSp ) * max( tau_eps_min*Cf, min( tau_eps_max*Cf, nu_2 * sqrt(u(:,n_var_RD)) ))
           
  




              CASE(20)
              !!!!  For case ::   external_flow_box_mod.f90
              !!!!  SCALES|LKM|RD|SGSD| SGSD/(SGSD+RD) - Goal | Local Time-Scale based on Local Rate-of-Strain
              !!!!
              
              nu_2 = sqrt(0.5_pr/nu)
             !u(:,n_var_Epsilon)       =   eps_in_SpatialSpace(:)  -  eps_in_SpatialSpace(:) * dt* ( u(:,n_var_SGSD)/(u(:,n_var_SGSD)+u(:,n_var_RD))  -  Psgs_diss_goal__AdpEpsSp ) * nu_2 * sqrt(u(:,n_var_RD))
              Do j=1, nwlt
             !DO i =1, dim
                !IF ( (u(j,n_var_SGSD)+u(j,n_var_RD)) .EQ. 0.0_pr ) PRINT *, 'Error ::   (u(j,n_var_SGSD)+u(j,n_var_RD)) = 0.0'
                 IF (  ANY( x(j, :)-xyzlimits(1,:) .GT. 0 )  .AND.  ANY( x(j, :)-xyzlimits(2,:) .LT. 0 )  .AND.  ((u(j,n_var_SGSD)+u(j,n_var_RD)) .NE. 0.0_pr)  )   THEN
                    u(j,n_var_Epsilon)       =   eps_in_SpatialSpace(j)  -  eps_in_SpatialSpace(j) * dt* ( u(j,n_var_SGSD)/(u(j,n_var_SGSD)+u(j,n_var_RD))  -  Psgs_diss_goal__AdpEpsSp ) * nu_2 * sqrt(u(j,n_var_RD))
                 ELSE
                    u(j,n_var_Epsilon)       =   eps_in_SpatialSpace(j) 
                 END IF
             !END DO
              END DO
              
  
  
  
              CASE DEFAULT
              IF (par_rank.EQ.0) THEN
              PRINT *, 'Error ::   Unknown  Variable Thresholding Forcing Type  in  user_post_process__VT()'
              PRINT *, '           Forcing_Type__AdpEpsSp = ', Forcing_Type__AdpEpsSp
              PRINT *,' Exiting ...'
              END IF
              CALL parallel_finalize
              STOP

  
       END SELECT
  







       IF ( (Forcing_Type__AdpEpsSp .GE. 11) .AND. (Forcing_Type__AdpEpsSp .LE. 15) ) THEN
           tmp1 = MINVAL( u(:,n_var_SGSD) / ( u(:,n_var_SGSD) + u(:,n_var_RD) ) )
           tmp2 = MAXVAL( u(:,n_var_SGSD) / ( u(:,n_var_SGSD) + u(:,n_var_RD) ) )
           tmp3 = SUM(    u(:,n_var_SGSD) / ( u(:,n_var_SGSD) + u(:,n_var_RD) ) ) !/ nwlt
           tmp4 = SUM(    u(:,n_var_SGSD) / ( u(:,n_var_SGSD) + u(:,n_var_RD) ) * dA(:) )
           CALL parallel_global_sum( REALMINVAL=tmp1 )
           CALL parallel_global_sum( REALMAXVAL=tmp2 )
           CALL parallel_global_sum( REAL=tmp3 )
           CALL parallel_global_sum( REAL=tmp4 )
           tmp3 = tmp3 / nwlt_global
           tmp4 = tmp4 / sumdA_global
           
           tmp5 = MINVAL( nu_2 * sqrt(u(:,n_var_RD)) )
           tmp6 = MAXVAL( nu_2 * sqrt(u(:,n_var_RD)) )
           tmp7 = SUM(    nu_2 * sqrt(u(:,n_var_RD)) ) !/ nwlt
           tmp8 = SUM(    nu_2 * sqrt(u(:,n_var_RD)) * dA(:) )
           CALL parallel_global_sum( REALMINVAL=tmp5 )
           CALL parallel_global_sum( REALMAXVAL=tmp6 )
           CALL parallel_global_sum( REAL=tmp7 )
           CALL parallel_global_sum( REAL=tmp8 )
           tmp7 = tmp7 / nwlt_global
           tmp8 = tmp8 / sumdA_global           
           
           IF (par_rank.EQ.0) THEN
              OPEN  (UNIT=UNIT_VT_STATS, FILE = TRIM(file_gen)//'p0_vt_stats', FORM='formatted', STATUS='unknown', POSITION='append')
              IF ( start ) THEN
                 WRITE(UNIT=UNIT_VT_STATS,ADVANCE='YES', FMT='( a )' ) &
                      '%Time          Nwlt    % VT:  EpsSpatial_Forcing_TimeScale          (Min,Max,Mean,VolumeAverage)( SGSD/(SGSD+RD) )          (Min,Max,Mean,VolumeAverage)( sqrt(0.5*RD/nu) ) [Local Time-Scale] '
              END IF
              start = .FALSE.
              WRITE (UNIT_VT_STATS,'(es13.6,5x, I10,5x, F12.5,18x, 4(F12.5,1x), 7x, 4(F12.5,1x))') t, nwlt_global, EpsSpatial_Forcing_TimeScale, tmp1, tmp2, tmp3, tmp4, tmp5, tmp6, tmp7, tmp8
              CLOSE (UNIT=UNIT_VT_STATS)
           END IF
       END IF









       ! Bounding Epsilon 
       !Do i=1, nwlt
            u(:,n_var_Epsilon) = MAX( eps_min__AdpEpsSp,  MIN( eps_max__AdpEpsSp, u(:,n_var_Epsilon) ) )
       !    u(i,n_var_Epsilon) = MAX( eps_min__AdpEpsSp,  MIN( eps_max__AdpEpsSp, u(i,n_var_Epsilon) ) )
       !END DO
       !u(:,n_var_Epsilon) = MAX( eps_min__AdpEpsSp, u(:,n_var_Epsilon) )
#ifdef DEBUG_VT
       tmp1 = MAXVAL( u(:,n_var_Epsilon) )
       CALL parallel_global_sum( REALMAXVAL=tmp1 )
       IF (par_rank.EQ.0) &
       WRITE(*,'(" VT:  (Max)( Epsilon )   = ",E12.5)') tmp1
#endif

      !#ifdef DEBUG_VT
      !IF (par_rank.EQ.0)
      !PRINT *, ' ??????????????????????????????????????????????????????? '
      !PRINT *, ' ????? n_var_Epsilon              ::::  ',  n_var_Epsilon
      !PRINT *, ' ????? MAXVAL u(i,n_var_Epsilon)  ::::  ',  MAXVAL(u(:,n_var_Epsilon))
      !PRINT *, ' ????? MINVAL u(i,n_var_Epsilon)  ::::  ',  MINVAL(u(:,n_var_Epsilon))
      !PRINT *, ' ??????????????????????????????????????????????????????? '
      !ENDIF
      !#endif
           
           

           
       !eps_in_SpatialSpace(:)  =   u(:,n_var_Epsilon)
            
       END IF    ! IF (do_Adp_Eps_DDES)

       END IF    ! IF (it == 0)        

       IF(ANY(inviscid_wall_zone .GT. 0)) THEN
         DO idim = 1,dim
           DO i = 1,2
              IF (inviscid_zone_thickness(i,idim) .GT. 1.0e-10_pr .AND. inviscid_wall_zone(i,idim) .GT. 0) THEN
                distance(:) = ABS( (xyzlimits(i,idim) - x(:,idim)) / inviscid_zone_thickness(i,idim) ) ! distance from the boundary edge, normalized and unsigned
                WHERE( distance(:) .LE. 1.0_pr )
                  !reuse distnace
                  distance(:) = 0.5_pr + 0.5_pr * TANH( 4.0_pr * ( distance(:) - 0.5_pr  ) ) / ( TANH( 0.5_pr * 4.0_pr  ) + 1.0e-10_pr ) !clustering of type tanh and normalizing into range from 0 and 1
                ELSEWHERE
                  distance(:) = 1.0_pr
                END WHERE
              END IF
           END DO
         END DO
         distance(:) = distance(:) * (1.0_pr - eps_r_h) + eps_r_h !rescaling clustering                 
         u(:,n_var_Epsilon) = u(:,n_var_Epsilon)*distance(:)
       END IF ! inviscid_wall_zone

    END IF   ! IF( do_Adp_Eps_Spatial==.TRUE. ) 







    IF( do_Adp_Eps_Spatial_Evol .EQV. .TRUE. ) THEN 

      !IF (it == 0 .OR. iwrite==IC_restart_station)     THEN
       IF (it == 0 .OR. post_process__beforeTimeIntegration__IC_restart)  THEN
#ifdef DEBUG_VT
         IF (par_rank.EQ.0)  PRINT *, 'it= ', it, '  CALL user_initial_conditions__VT again for Eps_Evol'
#endif
         CALL user_initial_conditions__VT(u, nwlt, n_var)        
        
       ELSE
#ifdef DEBUG_VT
         IF (par_rank.EQ.0)  PRINT *, 'it= ', it, '  Perform  user_post_process__VT  for Eps_Evol'
#endif

         ! Bounding Epsilon 
         !Do i=1, nwlt
              u(:,n_var_EpsilonEvol) = MAX( eps_min__AdpEpsSp,  MIN( eps_max__AdpEpsSp, u(:,n_var_EpsilonEvol) ) )
         !    u(i,n_var_EpsilonEvol) = MAX( eps_min__AdpEpsSp,  MIN( eps_max__AdpEpsSp, u(i,n_var_EpsilonEvol) ) )
         !END DO
         !u(:,n_var_EpsilonEvol) = MAX( eps_min__AdpEpsSp,  u(:,n_var_EpsilonEvol) )
!#ifdef DEBUG_VT
         !tmp1 = MAXVAL( u(:,n_var_EpsilonEvol) )
         !CALL parallel_global_sum( REALMAXVAL=tmp1 )
         !IF (par_rank.EQ.0) &
         !WRITE(*,'(" VT:  (Max)( EpsilonEvolution )   = ",E12.5)') tmp1
!#endif
       
       
          !IF( do_Adp_Eps_Spatial==.FALSE. )   eps_in_SpatialSpace(:)  =   u(:,n_var_EpsilonEvol)

       END IF    ! IF (it == 0)
    END IF   ! IF( do_Adp_Eps_Spatial_Evol==.TRUE. ) 

    





    IF      ( do_Adp_Eps_Spatial .EQV. .TRUE.  .AND. do_Adp_Eps_Spatial_Evol .EQV. .FALSE. ) THEN  
                                                                                       eps_in_SpatialSpace(:) = u(:,n_var_Epsilon)
                                                                                       !PRINT *,' TRUE FALSE'
    ELSE IF ( do_Adp_Eps_Spatial.EQV. .FALSE. .AND. do_Adp_Eps_Spatial_Evol.EQV. .TRUE. )  THEN  
                                                                                       eps_in_SpatialSpace(:) = u(:,n_var_EpsilonEvol)
                                                                                       !PRINT *,' FALSE TRUE'
    ELSE IF ( do_Adp_Eps_Spatial.EQV. .TRUE.  .AND. do_Adp_Eps_Spatial_Evol .EQV. .TRUE. )  THEN  
                                                                                   SELECT CASE(Choice__AdpEpsSp)
                                                                                          CASE(1)
                                                                                             eps_in_SpatialSpace(:) = u(:,n_var_Epsilon)
                                                                                             !PRINT *,' TRUE TRUE 1'
                                                                                          CASE(2)
                                                                                             eps_in_SpatialSpace(:) = u(:,n_var_EpsilonEvol)
                                                                                             !PRINT *,' TRUE TRUE 2'
                                                                                          CASE DEFAULT
                                                                                             IF (par_rank.EQ.0) THEN
                                                                                             PRINT *, 'Error ::   Unknown  Variable Thresholding   Choice__AdpEpsSp  in  user_post_process__VT()'
                                                                                             PRINT *, '           Choice__AdpEpsSp = ', Choice__AdpEpsSp
                                                                                             PRINT *,' Exiting ...'
                                                                                             END IF
                                                                                             CALL parallel_finalize
                                                                                             STOP
                                                                                          END SELECT
    END IF


    
    post_process__beforeTimeIntegration__IC_restart = .FALSE.

    
  END SUBROUTINE user_post_process__VT









  SUBROUTINE user_rhs__VT (user_rhs, u_integrated, du, d2u, Cf)
    USE parallel
    IMPLICIT NONE
    
    REAL (pr), DIMENSION (ng,ne),     INTENT(IN)               :: u_integrated  ! 1D flat version of integrated variables without BC 
    REAL (pr), DIMENSION (n),         INTENT(INOUT)            :: user_rhs
    REAL (pr), DIMENSION (ne,ng,dim), INTENT(INOUT)            :: du, d2u
    REAL (pr),                        INTENT(IN),    OPTIONAL  :: Cf

    REAL (pr), DIMENSION (ng)  :: Forcing_AdpEpsSpEvol               ! Forcing Term in RHS of Evolution Equation for "Lagrangian Variable Thresholding"
    REAL (pr), DIMENSION (ng)  :: nu_EpsEvol  
    REAL (pr)                  :: tmp1, tmp2, tmp3, tmp4, nu_2
    INTEGER                    :: ie, shift, i
    INTEGER,   PARAMETER       :: meth=1



    IF( do_Adp_Eps_Spatial_Evol ) THEN  
    
        ie    =  n_var_EpsilonEvol
        shift = (n_var_EpsilonEvol-1)*ng
        
        CALL Calculate__Forcing_AdpEpsSpEvol ( Forcing_AdpEpsSpEvol, Cf )
    
    
        !
        ! Artificial Viscosity for  epsilon  Evolution
        !
        !  nuI(:) = CI*delta(:)**2*Smod(:)
        !  Smod(:) = nu_t = SQRT( twoSijSij ) =  sqrt(u(:,nvar__RD) / nu)
        
        ! u(:,n_var_RD) is not accessible here  B/C  in this Subroutine,  only u_integrated(:,:) is accessible.
        ! Therefore, the following CALL will assign Local_Resolved_Dissipation, u(:,n_var_RD), to  nu_EpsEvol(:)
        CALL Get_Local_RD( nu_EpsEvol, ng )  
  
        nu_2 = sqrt(0.5_pr/nu)
        nu_EpsEvol(:) = C_nu_art_EpsEvol * delta(:)**2 * nu_2 * sqrt(nu_EpsEvol(:))  
        !nu_EpsEvol(:) = nu + nu_EpsEvol(:)
        
!#ifdef DEBUG_VT
        tmp1 = MINVAL( nu_EpsEvol(:) )
        tmp2 = MAXVAL( nu_EpsEvol(:) )
        tmp3 = SUM(    nu_EpsEvol(:) ) !/ nwlt
        tmp4 = SUM(    nu_EpsEvol(:) * dA(:) )
        CALL parallel_global_sum( REALMINVAL=tmp1 )
        CALL parallel_global_sum( REALMAXVAL=tmp2 )
        CALL parallel_global_sum( REAL=tmp3 )
        CALL parallel_global_sum( REAL=tmp4 )
        tmp3 = tmp3 / nwlt_global
        tmp4 = tmp4 / sumdA_global
        IF (par_rank.EQ.0) WRITE(*, FMT='( a, 5(F12.5,1x) )' ) &
                                        '% VT:  user_rhs__VT          nu, (Min,Max,Mean,VolumeAverage)( nu_EpsEvol )', nu, tmp1, tmp2, tmp3, tmp4
        tmp1 = MINVAL( delta(:) )
        tmp2 = MAXVAL( delta(:) )
        tmp3 = SUM(    delta(:) ) !/ nwlt
        tmp4 = SUM(    delta(:) * dA(:) )
        CALL parallel_global_sum( REALMINVAL=tmp1 )
        CALL parallel_global_sum( REALMAXVAL=tmp2 )
        CALL parallel_global_sum( REAL=tmp3 )
        CALL parallel_global_sum( REAL=tmp4 )
        tmp3 = tmp3 / nwlt_global
        tmp4 = tmp4 / sumdA_global
        IF (par_rank.EQ.0) WRITE(*, FMT='( a, 5(F12.5,1x) )' ) &
                                        '% VT:  user_rhs__VT          nu, (Min,Max,Mean,VolumeAverage)( delta )     ', nu, tmp1, tmp2, tmp3, tmp4
!#endif
        
        
        

        user_rhs(shift+1:shift+ng) = - u_integrated(:,1)*du(ie,:,1) - u_integrated(:,2)*du(ie,:,2) - u_integrated(:,3)*du(ie,:,3) &
                                     - Forcing_AdpEpsSpEvol(:) &
                                     + 1.0_pr*nu_EpsEvol(:) * SUM(d2u(ie,:,:),2)
                                    !+ ( nu + C_nu_art_EpsEvol*sqrt(u(:,n_var_RD) / nu) ) * SUM(d2u(ie,:,:),2)
                                     
                                     

        !DO i =shift+1, shift+ng
        !   user_rhs(i) = 0.0_pr
        !enddo

    END IF

  END SUBROUTINE user_rhs__VT










  SUBROUTINE user_Drhs__VT (user_Drhs, u, u_prev, du, d2u, meth)     !(user_Drhs, u, u_prev, du, d2u, meth)
    IMPLICIT NONE
    
    INTEGER,                             INTENT(IN)    :: meth    
    REAL (pr), DIMENSION (ng,ne),        INTENT(IN)    :: u, u_prev  ! u_prev_timestep passed local to cast it into 2dim array to work with vector derivatives
    REAL (pr), DIMENSION (n),            INTENT(INOUT) :: user_Drhs
    REAL (pr), DIMENSION (2*ne,ng,dim),  INTENT(INOUT) :: du         ! 1st derivatives for u (in du(1:ne,:,:)) and u_prev_timestep (in du(ne+1:2*ne,:,:))
    REAL (pr), DIMENSION (ne  ,ng,dim),  INTENT(INOUT) :: d2u        ! 2nd derivatives for u 

    REAL (pr), DIMENSION (ng)  :: nu_EpsEvol  
    REAL (pr)                  :: nu_2
    INTEGER                    :: ie, shift, i


    IF( do_Adp_Eps_Spatial_Evol ) THEN  
    
        ie    =  n_var_EpsilonEvol
        shift = (n_var_EpsilonEvol-1)*ng

        CALL Get_Local_RD( nu_EpsEvol, ng )  

        nu_2 = sqrt(0.5_pr/nu)
        nu_EpsEvol(:) = C_nu_art_EpsEvol * delta(:)**2 * nu_2 * sqrt(nu_EpsEvol(:))  
        !nu_EpsEvol(:) = nu + nu_EpsEvol(:)

        user_Drhs(shift+1:shift+ng) = - u_prev(:,1)*du(ie,:,1)    - u_prev(:,2)*du(ie,:,2)    - u_prev(:,3)*du(ie,:,3)     &
                                      -      u(:,1)*du(ne+ie,:,1) -      u(:,2)*du(ne+ie,:,2) -      u(:,3)*du(ne+ie,:,3) &
                                      + 1.0_pr*nu_EpsEvol(:) * SUM(d2u(ie,:,:),2)
                                     !+ ( nu + C_nu_art_EpsEvol*sqrt(u(:,n_var_RD) / nu) ) * SUM(d2u(ie,:,:),2)
                                     !- Forcing_AdpEpsSpEvol(:) &

        !DO i =shift+1, shift+ng
        !   user_Drhs(i) = 0.0_pr
        !enddo

    END IF

  END SUBROUTINE user_Drhs__VT










  SUBROUTINE user_Drhs_diag__VT (user_Drhs_diag, du_prev_timestep, du, d2u, meth)
    IMPLICIT NONE
    
    INTEGER,                          INTENT(IN)    :: meth
    REAL (pr), DIMENSION (n),         INTENT(INOUT) :: user_Drhs_diag
    REAL (pr), DIMENSION (ne,ng,dim), INTENT(INOUT) :: du_prev_timestep
    REAL (pr), DIMENSION (ng,dim),    INTENT(INOUT) :: du, d2u

    REAL (pr), DIMENSION (ng)  :: nu_EpsEvol  
    REAL (pr)                  :: nu_2
    INTEGER                    :: ie, shift, i


    IF( do_Adp_Eps_Spatial_Evol ) THEN  
    
        ie    =  n_var_EpsilonEvol
        shift = (n_var_EpsilonEvol-1)*ng

        CALL Get_Local_RD( nu_EpsEvol, ng )  

        nu_2 = sqrt(0.5_pr/nu)
        nu_EpsEvol(:) = C_nu_art_EpsEvol * delta(:)**2 * nu_2 * sqrt(nu_EpsEvol(:))  
        !nu_EpsEvol(:) = nu + nu_EpsEvol(:)

        user_Drhs_diag(shift+1:shift+ng) = - u_prev_timestep(1:ng)*du(:,1) - u_prev_timestep(ng+1:2*ng)*du(:,2) - u_prev_timestep(2*ng+1:3*ng)*du(:,3) &
                                           + 1.0_pr*nu_EpsEvol(:) * SUM(d2u,2)            
                                          !+ ( nu + C_nu_art_EpsEvol*sqrt(u(:,n_var_RD) / nu) ) * SUM(d2u,2)            
                                          !- Forcing_AdpEpsSpEvol(:) &

        !DO i =shift+1, shift+ng
        !   user_Drhs_diag(i) = 1.0_pr
        !enddo        

    END IF

  END SUBROUTINE user_Drhs_diag__VT



















!******************** Boundary conditions for additional Epsilon Evolution equation  *****************
  SUBROUTINE VT_algebraic_BC (Lu, u_in, du_in, nlocal, ne_local, jlev, meth)
    IMPLICIT NONE
    INTEGER,                                      INTENT (IN)    :: jlev, meth, ne_local, nlocal
    REAL (pr), DIMENSION (nlocal*ne_local),       INTENT (INOUT) :: Lu
    REAL (pr), DIMENSION (nlocal*ne_local),       INTENT (IN)    :: u_in
    REAL (pr), DIMENSION (ne_local, nlocal, dim), INTENT (IN)    :: du_in

    INTEGER                   :: ie, i, shift
    INTEGER                   :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim)   :: face
    INTEGER, DIMENSION(nwlt)  :: iloc


    IF ( do_Adp_Eps_Spatial_Evol ) THEN
       IF( Forcing_Type__AdpEpsSp == 20 ) THEN
          ie    =  n_var_EpsilonEvol
          shift = (n_var_EpsilonEvol-1)*ng
          !--Go through all Boundary points that are specified
          i_p_face(0) = 1
          DO i=1,dim
             i_p_face(i) = i_p_face(i-1)*3
          END DO
          DO face_type = 0, 3**dim - 1
             face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
             IF( ANY( face(1:dim) /= 0) ) THEN                           ! goes only through boundary points
                CALL get_all_indices_by_face (face_type, jlev, nloc, iloc)
                IF(nloc > 0) THEN 
                   IF( face(1) == -1  ) THEN                             ! Xmin face (entire face) 
                      Lu(shift+iloc(1:nloc)) = u_in(shift+iloc(1:nloc))     ! Dirichlet conditions
                   END IF
                   IF(  ABS(face(2)) == 1 .AND. face(1) /= -1 ) THEN     ! Ymin & Ymax face (entire face) except inflow points
                      Lu(shift+iloc(1:nloc)) = du_in(ie,iloc(1:nloc),2)     ! Neuman conditions
                   END IF
                END IF
             END IF
          END DO
       END IF
    END IF

  END SUBROUTINE VT_algebraic_BC





  SUBROUTINE VT_algebraic_BC_diag (Lu_diag, du_in, nlocal, ne_local, jlev, meth)
    IMPLICIT NONE
    INTEGER,                                INTENT (IN)    :: jlev, meth, ne_local, nlocal
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (INOUT) :: Lu_diag
    REAL (pr), DIMENSION (nlocal,dim),      INTENT (IN)    :: du_in

    INTEGER                   :: ie, i, shift
    INTEGER                   :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim)   :: face
    INTEGER, DIMENSION(nwlt)  :: iloc


    IF ( do_Adp_Eps_Spatial_Evol ) THEN
       IF( Forcing_Type__AdpEpsSp == 20 ) THEN
          ie    =  n_var_EpsilonEvol
          shift = (n_var_EpsilonEvol-1)*ng
          !--Go through all Boundary points that are specified
          i_p_face(0) = 1
          DO i=1,dim
             i_p_face(i) = i_p_face(i-1)*3
          END DO
          DO face_type = 0, 3**dim - 1
             face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
             IF( ANY( face(1:dim) /= 0) ) THEN                             ! goes only through boundary points
                CALL get_all_indices_by_face (face_type, jlev, nloc, iloc)
                IF(nloc > 0 ) THEN 
                   IF( face(1) == -1  ) THEN                               ! Xmin face (entire face) 
                      Lu_diag(shift+iloc(1:nloc)) = 1.0_pr                 ! Dirichlet conditions
                   END IF
                   IF(  ABS(face(2)) == 1 .AND. face(1) /= -1 ) THEN       ! Ymin & Ymax face (entire face) except inflow points
                      Lu_diag(shift+iloc(1:nloc)) = du_in(iloc(1:nloc),2)  ! Newman conditions
                   END IF
                END IF
             END IF
          END DO
       END IF
    END IF

  END SUBROUTINE VT_algebraic_BC_diag 






  SUBROUTINE VT_algebraic_BC_rhs (rhs, ne_local, nlocal, jlev)
    IMPLICIT NONE
    INTEGER,                                INTENT (IN)    :: ne_local, nlocal, jlev
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (INOUT) :: rhs

    INTEGER                   :: ie, i, shift
    INTEGER                   :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim)   :: face
    INTEGER, DIMENSION(nwlt)  :: iloc



    IF ( do_Adp_Eps_Spatial_Evol ) THEN
       IF( Forcing_Type__AdpEpsSp == 20 ) THEN
          ie    =  n_var_EpsilonEvol
          shift = (n_var_EpsilonEvol-1)*ng
          !--Go through all Boundary points that are specified
          i_p_face(0) = 1
          DO i=1,dim
             i_p_face(i) = i_p_face(i-1)*3
          END DO
          DO face_type = 0, 3**dim - 1
             face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
             IF( ANY( face(1:dim) /= 0) ) THEN                        ! goes only through boundary points
                CALL get_all_indices_by_face (face_type, jlev, nloc, iloc)
                IF(nloc > 0 ) THEN 
                   IF( face(1) == -1  ) THEN                          ! Xmin face (entire face) 
                      rhs(shift+iloc(1:nloc)) = eps_min__AdpEpsSp     ! Dirichlet conditions  ( constant minimum threshold  imposed at input )
                   END IF
                   IF(  ABS(face(2)) == 1 .AND. face(1) /= -1) THEN   ! Ymin & Ymax face (entire face) except inflow points
                      rhs(shift+iloc(1:nloc)) = 0.0_pr                ! Neuman conditions
                   END IF
                END IF
             END IF
          END DO
       END IF
    END IF
  
  END SUBROUTINE VT_algebraic_BC_rhs









  SUBROUTINE Calculate__Forcing_AdpEpsSpEvol ( Forcing_AdpEpsSpEvol_out, Cf )
    USE parallel
    USE field            ! In order to have access to global   u(:,1:dim) & u(:,n_var_Epsilon) & u(:,n_var_EpsilonEvol)

    IMPLICIT NONE
    
    REAL (pr), DIMENSION (ng), INTENT(INOUT)   :: Forcing_AdpEpsSpEvol_out        ! Forcing Term in RHS of Evolution Equation for "Lagrangian Variable Thresholding"
    REAL (pr),                 INTENT(IN)      :: Cf

    REAL (pr)        :: Goal_Ratio, tmp1, tmp2, tmp3, tmp4,  tmp5, tmp6, tmp7, tmp8, teddy_interval, nu_2
    INTEGER          :: i, j
    LOGICAL , SAVE   :: start = .TRUE.
    
    

    IF( do_Adp_Eps_Spatial_Evol ) THEN  



       IF (do_varying_Goal) THEN
           teddy_interval =  1.0_pr / ( 3.0_pr * Cf )  ! OR   10t_eddy= 10.0_pr / ( 3.0_pr * Cf )   OR   5t_eddy= 5.0_pr / ( 3.0_pr * Cf )
           
           IF ( floor(t / teddy_interval) > teddy_interval_end(teddy_interval_numbers) ) THEN
              IF (par_rank.EQ.0) THEN
                 PRINT *, 'Error ::   Unknown   time   for   varying_Goal   in   user_post_process__VT()'
                 WRITE(*,'(3(A,F25.16))') 't=', t, '   teddy_interval=', teddy_interval, '   Cf=', Cf
                 PRINT *,' Exiting ...'
              END IF
              CALL parallel_finalize
              STOP
           END IF

           i = MAXVAL(MINLOC(teddy_interval_end , mask = teddy_interval_end .GT. floor(t / teddy_interval) ))
           Psgs_diss_goal__AdpEpsSp = Psgs_diss_goal__AdpEpsSp__varying(i)
           eps_min__AdpEpsSp        = eps_min__AdpEpsSp__varying(i)
           eps_max__AdpEpsSp        = eps_max__AdpEpsSp__varying(i)
           IF (par_rank.EQ.0) WRITE(*,'(" VT:  PSGSD_Goal/eps_min/eps_max/teddy_interval/(#)    = ",4F12.5,A,I2,A)') &
                              Psgs_diss_goal__AdpEpsSp__varying(i), eps_min__AdpEpsSp__varying(i), eps_max__AdpEpsSp__varying(i), teddy_interval, '   (',i,')'
           
       END IF


       


       SELECT CASE(Forcing_Type__AdpEpsSp)
              CASE(7)
    
    
              Goal_Ratio                    = Psgs_diss_goal__AdpEpsSp/(1-Psgs_diss_goal__AdpEpsSp)           


              tmp1                          = MAXVAL(abs( u(:,n_var_SGSD) - u(:,n_var_RD)*Goal_Ratio ))
              CALL parallel_global_sum( REALMAXVAL=tmp1 )
#ifdef DEBUG_VT
              IF (par_rank.EQ.0) PRINT *, 'VT:  Max| SGSD - RD*Goal/(1-Goal) |  = ', tmp1
#endif             

                      
              Forcing_Norm_Denom__av_Evol   = (  Forcing_Norm_Denom__av_Evol * REAL(Number__av_Evol,pr) + tmp1 )  /  REAL(Number__av_Evol+1,pr)
           
              Number__av_Evol               = Number__av_Evol + 1
           
           

              Forcing_AdpEpsSpEvol_out      =    Forcing_Factor__AdpEpsSp &
                                              * ( u(:,n_var_SGSD) - u(:,n_var_RD)*Goal_Ratio ) &
                                              /  Forcing_Norm_Denom__av_Evol


#ifdef DEBUG_VT
              tmp1 = MINVAL( Forcing_AdpEpsSpEvol_out )
              tmp2 = MAXVAL( Forcing_AdpEpsSpEvol_out )
              CALL parallel_global_sum( REALMINVAL=tmp1 )
              CALL parallel_global_sum( REALMAXVAL=tmp2 )

              IF (par_rank.EQ.0) THEN
                  PRINT *, '*****Calculate__Forcing_AdpEpsSpEvol***** '
                 !PRINT *, 'MAXVAL( SGSD - RD*Goal/(1-Goal) )  ::::   ', MAXVAL( ( u(:,n_var_SGSD) - u(:,n_var_RD)*Goal_Ratio ) / Forcing_Norm_Denom__av_Evol )
                 !PRINT *, 'MINVAL( SGSD - RD*Goal/(1-Goal) )  ::::   ', MINVAL( ( u(:,n_var_SGSD) - u(:,n_var_RD)*Goal_Ratio ) / Forcing_Norm_Denom__av_Evol )
                  PRINT *, '  MinVal & MaxVal   of   Forcing_AdpEpsSpEvol '
                  PRINT *, 'VT:  (Min,Max)INVAL( Fc * (SGSD - RD*Goal/(1-Goal))/TimeAverage(MaxVal...) )  ::::   ', tmp1, tmp2
                  PRINT *, '***************************************** '
              END IF
#endif
           





              CASE(11)
              !!!!  For case ::   /Simulations/ForcedHomogeneousTurbulence/SCALES__AdpEpsSpatial/LKM/RSGSD/
              !!!!  SCALES|LKM|RD|SGSD| SGSD/(SGSD+RD) - Goal | Time-Scale based on Rate-of-Strain
              !!!!
              
              Forcing_AdpEpsSpEvol_out       =  0.0_pr * & 
                                                 ( u(:,n_var_SGSD)/(u(:,n_var_SGSD)+u(:,n_var_RD))  -  Psgs_diss_goal__AdpEpsSp ) &
                                               * EpsSpatial_Forcing_TimeScale


#ifdef DEBUG_VT
              tmp1 = MINVAL( u(:,n_var_SGSD) / ( u(:,n_var_SGSD) + u(:,n_var_RD) ) )
              tmp2 = MAXVAL( u(:,n_var_SGSD) / ( u(:,n_var_SGSD) + u(:,n_var_RD) ) )
              tmp3 = SUM(    u(:,n_var_SGSD) / ( u(:,n_var_SGSD) + u(:,n_var_RD) ) ) !/ nwlt
              CALL parallel_global_sum( REALMINVAL=tmp1 )
              CALL parallel_global_sum( REALMAXVAL=tmp2 )
              CALL parallel_global_sum( REAL=tmp3 )
              tmp3 = tmp3 / nwlt_global
              IF (par_rank.EQ.0) THEN
              PRINT *,  ' VT:  EpsSpatial_Forcing_TimeScale       = ', EpsSpatial_Forcing_TimeScale
              WRITE(*,'(" VT:  (Min,Max,Mean)( SGSD/(SGSD+RD) )   = ",E12.5,E12.5,E12.5)') tmp1, tmp2, tmp3
              END IF
#endif
           
  




              CASE(12)
              !!!!  For case ::   /Simulations/ForcedHomogeneousTurbulence/SCALES__AdpEpsSpatial/LKM/RSGSD/
              !!!!  SCALES|LKM|RD|SGSD| SGSD/(SGSD+RD) - Goal | Local Time-Scale based on Local Rate-of-Strain
              !!!!
              
              nu_2 = sqrt(0.5_pr/nu)
              Forcing_AdpEpsSpEvol_out       =   ( u(:,n_var_SGSD)/(u(:,n_var_SGSD)+u(:,n_var_RD))  -  Psgs_diss_goal__AdpEpsSp ) * nu_2 * sqrt(u(:,n_var_RD))


#ifdef DEBUG_VT
              tmp1 = MINVAL( u(:,n_var_SGSD) / ( u(:,n_var_SGSD) + u(:,n_var_RD) ) )
              tmp2 = MAXVAL( u(:,n_var_SGSD) / ( u(:,n_var_SGSD) + u(:,n_var_RD) ) )
              tmp3 = SUM(    u(:,n_var_SGSD) / ( u(:,n_var_SGSD) + u(:,n_var_RD) ) ) !/ nwlt
              tmp4 = SUM(    u(:,n_var_SGSD) / ( u(:,n_var_SGSD) + u(:,n_var_RD) ) * dA(:) )
              CALL parallel_global_sum( REALMINVAL=tmp1 )
              CALL parallel_global_sum( REALMAXVAL=tmp2 )
              CALL parallel_global_sum( REAL=tmp3 )
              CALL parallel_global_sum( REAL=tmp4 )
              tmp3 = tmp3 / nwlt_global
              tmp4 = tmp4 / sumdA_global
              IF (par_rank.EQ.0) THEN
              WRITE(*,'(" VT:  EpsSpatial_Forcing_TimeScale                     = ",F12.5)')                   EpsSpatial_Forcing_TimeScale
              WRITE(*,'(" VT:  (Min,Max,Mean,VolumeAverage)( SGSD/(SGSD+RD) )   = ",F12.5,F12.5,F12.5,F12.5)') tmp1, tmp2, tmp3, tmp4
              END IF
#endif
           
  




              CASE(13)
              !!!!  For case ::   /Simulations/ForcedHomogeneousTurbulence/SCALES__AdpEpsSpatial/LKM/RSGSD/
              !!!!  SCALES|LKM|RD|SGSD| SGSD/(SGSD+RD) - Goal | Local Time-Scale based on Local Rate-of-Strain   +  Cap \tau_{{\epsilon}_{min}}  to  tau_eps_min * \tau_{\rm eddy}
              !!!!
              
              nu_2 = sqrt(0.5_pr/nu)
              Forcing_AdpEpsSpEvol_out       =   ( u(:,n_var_SGSD)/(u(:,n_var_SGSD)+u(:,n_var_RD))  -  Psgs_diss_goal__AdpEpsSp ) * max( tau_eps_min*Cf, nu_2 * sqrt(u(:,n_var_RD)) )
             
  




              CASE(14)
              !!!!  For case ::   /Simulations/ForcedHomogeneousTurbulence/SCALES__AdpEpsSpatial/LKM/RSGSD/
              !!!!  SCALES|LKM|RD|SGSD| SGSD/(SGSD+RD) - Goal | Local Time-Scale based on Local Rate-of-Strain   +  Cap \tau_{{\epsilon}_{max}}  to  tau_eps_max * \tau_{\rm eddy}
              !!!!
              
              nu_2 = sqrt(0.5_pr/nu)
              Forcing_AdpEpsSpEvol_out       =   ( u(:,n_var_SGSD)/(u(:,n_var_SGSD)+u(:,n_var_RD))  -  Psgs_diss_goal__AdpEpsSp ) * min( tau_eps_max*Cf, nu_2 * sqrt(u(:,n_var_RD)) )
             
  




              CASE(15)
              !!!!  For case ::   /Simulations/ForcedHomogeneousTurbulence/SCALES__AdpEpsSpatial/LKM/RSGSD/
              !!!!  SCALES|LKM|RD|SGSD| SGSD/(SGSD+RD) - Goal | Local Time-Scale based on Local Rate-of-Strain   +  Cap \tau_{\epsilon}  to  [tau_eps_min, tau_eps_max]* \tau_{\rm eddy}
              !!!!
              
              nu_2 = sqrt(0.5_pr/nu)
              Forcing_AdpEpsSpEvol_out       =   ( u(:,n_var_SGSD)/(u(:,n_var_SGSD)+u(:,n_var_RD))  -  Psgs_diss_goal__AdpEpsSp ) * max( tau_eps_min*Cf, min( tau_eps_max*Cf, nu_2 * sqrt(u(:,n_var_RD)) ))
           
  




              CASE(20)
              !!!!  For case ::   external_flow_box_mod.f90
              !!!!  SCALES|LKM|RD|SGSD| SGSD/(SGSD+RD) - Goal | Local Time-Scale based on Local Rate-of-Strain
              !!!!
              
              nu_2 = sqrt(0.5_pr/nu)
             !Forcing_AdpEpsSpEvol_out       =   ( u(:,n_var_SGSD)/(u(:,n_var_SGSD)+u(:,n_var_RD))  -  Psgs_diss_goal__AdpEpsSp ) * nu_2 * sqrt(u(:,n_var_RD))
              Do j=1, nwlt
             !DO i =1, dim
                !IF ( (u(j,n_var_SGSD)+u(j,n_var_RD)) .EQ. 0.0_pr ) PRINT *, 'Error ::   (u(j,n_var_SGSD)+u(j,n_var_RD)) = 0.0'
                 IF (  ANY( x(j, :)-xyzlimits(1,:) .GT. 0 )  .AND.  ANY( x(j, :)-xyzlimits(2,:) .LT. 0 )  .AND.  ((u(j,n_var_SGSD)+u(j,n_var_RD)) .NE. 0.0_pr)  )   THEN
                !IF (  ANY( x(j, :)-xyzlimits(1,:) .GT. 0 )  .AND.  ANY( x(j, :)-xyzlimits(2,:) .LT. 0 )  .AND.  (u(j,n_var_RD) .NE. 0.0_pr)  )   THEN
                   !IF ( (u(j,n_var_SGSD)+u(j,n_var_RD)) .EQ. 0.0_pr ) PRINT *, 'Error ::   (u(j,n_var_SGSD)+u(j,n_var_RD)) = 0.0'
                    Forcing_AdpEpsSpEvol_out(j)   =   ( u(j,n_var_SGSD)/(u(j,n_var_SGSD)+u(j,n_var_RD))  -  Psgs_diss_goal__AdpEpsSp ) * nu_2 * sqrt(u(j,n_var_RD))
                 ELSE
                    Forcing_AdpEpsSpEvol_out(j)   =   0.0_pr
                 END IF
             !END DO
              END DO

             !i_p_face(0) = 1
             !DO i=1,dim
             !   i_p_face(i) = i_p_face(i-1)*3
             !END DO
             !DO face_type = 0, 3**dim - 1
             !   face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
             !   IF( ANY( face(1:dim) .EQ. 0) ) THEN                            ! goes only through internal points
             !      CALL get_all_indices_by_face (face_type, jlev, nloc, iloc)
             !      IF(nloc > 0 ) THEN 
             !      
             !      END IF
             !   END IF
             !END DO
                
  
  
  
              CASE DEFAULT
              IF (par_rank.EQ.0) THEN
              PRINT *, 'Error ::   Unknown  Variable Thresholding Forcing Type  in  Calculate__Forcing_AdpEpsSpEvol()'
              PRINT *, '           Forcing_Type__AdpEpsSp = ', Forcing_Type__AdpEpsSp
              PRINT *,' Exiting ...'
              END IF
              CALL parallel_finalize
              STOP
  
       END SELECT
  







       IF ( (Forcing_Type__AdpEpsSp .GE. 11) .AND. (Forcing_Type__AdpEpsSp .LE. 15) ) THEN
           tmp1 = MINVAL( u(:,n_var_SGSD) / ( u(:,n_var_SGSD) + u(:,n_var_RD) ) )
           tmp2 = MAXVAL( u(:,n_var_SGSD) / ( u(:,n_var_SGSD) + u(:,n_var_RD) ) )
           tmp3 = SUM(    u(:,n_var_SGSD) / ( u(:,n_var_SGSD) + u(:,n_var_RD) ) ) !/ nwlt
           tmp4 = SUM(    u(:,n_var_SGSD) / ( u(:,n_var_SGSD) + u(:,n_var_RD) ) * dA(:) )
           CALL parallel_global_sum( REALMINVAL=tmp1 )
           CALL parallel_global_sum( REALMAXVAL=tmp2 )
           CALL parallel_global_sum( REAL=tmp3 )
           CALL parallel_global_sum( REAL=tmp4 )
           tmp3 = tmp3 / nwlt_global
           tmp4 = tmp4 / sumdA_global
           
           tmp5 = MINVAL( nu_2 * sqrt(u(:,n_var_RD)) )
           tmp6 = MAXVAL( nu_2 * sqrt(u(:,n_var_RD)) )
           tmp7 = SUM(    nu_2 * sqrt(u(:,n_var_RD)) ) !/ nwlt
           tmp8 = SUM(    nu_2 * sqrt(u(:,n_var_RD)) * dA(:) )
           CALL parallel_global_sum( REALMINVAL=tmp5 )
           CALL parallel_global_sum( REALMAXVAL=tmp6 )
           CALL parallel_global_sum( REAL=tmp7 )
           CALL parallel_global_sum( REAL=tmp8 )
           tmp7 = tmp7 / nwlt_global
           tmp8 = tmp8 / sumdA_global           
           
           IF (par_rank.EQ.0) THEN
              OPEN  (UNIT=UNIT_VT_STATS_EVOL, FILE = TRIM(file_gen)//'p0_vt_stats_Evol', FORM='formatted', STATUS='unknown', POSITION='append')
              IF ( start ) THEN
                 WRITE(UNIT=UNIT_VT_STATS_EVOL,ADVANCE='YES', FMT='( a )' ) &
                      '%Time          Nwlt    % VT:  EpsSpatial_Forcing_TimeScale          (Min,Max,Mean,VolumeAverage)( SGSD/(SGSD+RD) )          (Min,Max,Mean,VolumeAverage)( sqrt(0.5*RD/nu) ) [Local Time-Scale] '
              END IF
              start = .FALSE.
              WRITE (UNIT_VT_STATS_EVOL,'(es13.6,5x, I10,5x, F12.5,18x, 4(F12.5,1x), 7x, 4(F12.5,1x))') t, nwlt_global, EpsSpatial_Forcing_TimeScale, tmp1, tmp2, tmp3, tmp4, tmp5, tmp6, tmp7, tmp8
              CLOSE (UNIT=UNIT_VT_STATS_EVOL)
           END IF
       END IF


    END IF   ! IF( do_Adp_Eps_Spatial_Evol==.TRUE. ) 

  END SUBROUTINE Calculate__Forcing_AdpEpsSpEvol


       


  !****************************************************************************************
  !*    Getting   Local_Resolved_Dissipation(:)                                           *
  !*    From      u(:,n_var_RD)                                                           *
  !*                                                                                      *
  !*    This Subroutine is called from                                                    *
  !*         Subroutines              user_rhs__VT, user_Drhs__VT, user_Drhs_diag__VT     *
  !*    B/C  in  these subroutines,   u(:,n_var_RD) is NOT  accessible                    *
  !****************************************************************************************
  SUBROUTINE  Get_Local_RD ( Local_RD_out, nlocal)
    USE field            ! In order to have access to global u(:,n_var_RD)
    USE parallel
    IMPLICIT NONE

    INTEGER,                       INTENT (IN)  :: nlocal
    REAL (pr), DIMENSION (nlocal), INTENT (OUT) :: Local_RD_out


    ! Spatial Adaptive Epsilon
    IF( do_Adp_Eps_Spatial .OR. do_Adp_Eps_Spatial_Evol ) THEN     
         Local_RD_out(:) = u(:,n_var_RD)
    ENDIF
    
   END SUBROUTINE Get_Local_RD



END MODULE VT

  
