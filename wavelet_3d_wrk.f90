!*********************************************************
! nxyz(3) - # of points at the finest level at coresponding directions
! indx, indx_old - indicies of active points on non-adaptive grid (old for the previous time step)
! lv(0:j_lev) - show the boundaries of grid points in the 1-d vector of the solution
! lvj(0:j_lev) - the same as above but regrouped for use in V-cycle calculation
! nlv(1:4,1:nwlt) - indecies of the points in 1%-d array corresponding to j level, for faster wavelet transform
!           i = nlv(1,k)
!           ix   = nlv(2,k)
!           iy   = nlv(3,k)
!           iz   = nlv(4,k)
! note that points are stored level by level with boundary points stored after all internal points are stored 
! nlvj(1:4,1:nwlt) - analagous to nlv, but used for V-cycle calculations
! lvx_****, lvy_*** boundaries for points to do wavelet transform, analogous to lv
! nlvx_****, nlvy_*** boundaries for points to do wavelet transform, analogous to nlv
! h - grid spacing for different levels an directions, used only if grid is uniform
! wgh** - different weights for interpolation and derivative calculations
!   wgh_updt(-2*n_updt:2*n_updt,0:MAXVAL(nxyz),j_lev-1,0:n_wlt_fmly,0:n_trnsf_type,1:3) 0:1 low and high order method
!   wgh_prdct(-2*n_prdct:2*n_prdct,0:MAXVAL(nxyz),j_lev-1,0:n_wlt_fmly,0:n_trnsf_type,1:3)
!   wgh_df(-2*n_diff-1:2*n_diff+1,0:MAXVAL(nxyz),j_lev,0:n_df,1:3)
!   wgh_d2f(-2*n_diff-1:2*n_diff+1,0:MAXVAL(nxyz),j_lev,0:n_df,1:3) 
! xx(:,3) - coordinates on non-adaptive grid in corresponding directions
! x(1:nwlt,1:3) are the adaptive grid point coordinates in real space
! wrk - working array (use 3d working array in this version )
! i_c, i_d - logical array for mask
! jd - if 2D jd = 0, if 3D jd = 1 used to force loops to one pass in 2D
!--------------- Poisson solver ----------------------------
! lv_intrnl - analogous to lv, but contains only internal points
! lv_bnd - analogous to lv but contains only boundary points, lv_bnd(i ,bnd,level) 
!          where bnd = (1=xmin,2=xmax,3=ymin,4=ymax,5=zmin,6=zmax )
!          where i = (0=start of bnd pts for this bndry at this level,
!                     1=end of bnd pts for this bndry at this level,
!                     2=
!                     3=
MODULE wlt_trns_mod
  USE precision
  USE debug_vars
  USE wlt_vars
  USE wavelet_filters_vars
  USE wlt_trns_util_mod      ! set_xx()
  USE wlt_trns_vars
  USE io_3D_vars
  USE sizes
  USE additional_nodes 
  IMPLICIT NONE
  PRIVATE
  INTEGER :: xuj1, xuj2

  INTEGER :: maxval_nxyz ! defined when nxyz is calculated, Needed because some complier do not support intrinsics in declarations
  INTEGER, DIMENSION (:,:), ALLOCATABLE:: indx, indx_old
  INTEGER, DIMENSION (:), ALLOCATABLE :: lv,lvj
  INTEGER, DIMENSION (:,:), ALLOCATABLE :: nlv,nlvj
  INTEGER, DIMENSION (:,:,:,:), ALLOCATABLE :: lvxyz_odd,lvxyz_even
  INTEGER, DIMENSION (:,:,:,:,:), ALLOCATABLE :: nlvxyz_odd,nlvxyz_even
  INTEGER, DIMENSION (:,:,:), ALLOCATABLE :: nlvD,nlvjD
  INTEGER, DIMENSION (:,:,:), ALLOCATABLE :: lvdxyz_odd,lvdxyz_even
  INTEGER, DIMENSION (:,:,:,:), ALLOCATABLE :: nlvdxyz_odd,nlvdxyz_even
  REAL (pr), DIMENSION (:,:,:,:,:,:), ALLOCATABLE :: wgh_updt, wgh_prdct !wlt transform weights
  REAL (pr), DIMENSION (:,:,:,:,:,:), ALLOCATABLE :: wgh_updt_interp, wgh_prdct_interp !wlt transform weights for c_wlt_trns_interp()
  REAL (pr), DIMENSION (:,:,:,:,:), ALLOCATABLE :: wgh_df, wgh_d2f
  REAL (pr), DIMENSION (:,:,:), ALLOCATABLE :: wrk
  INTEGER, DIMENSION (:), ALLOCATABLE :: iwrk
  LOGICAL  , DIMENSION (:,:,:), ALLOCATABLE :: i_c, i_d
  LOGICAL :: i_c_init
  INTEGER, PARAMETER :: dim_tmp = 3

  !==========================================================================================================================

  !===================================================================!
  !               PUBLIC FUNCTIONS AND SUBROUTINES                    !  
  !===================================================================!

  PUBLIC :: adapt_grid, c_diff_fast, c_diff_diag, c_diff_diag_bnd, c_wlt_trns, c_wlt_trns_mask, &
       init_DB, update_db_from_u, update_u_from_db, update_u_from_db_noidices,  weights, wlt_filt, wlt_interpolate, &
       c_wlt_trns_interp, c_wlt_trns_interp_setup, c_wlt_trns_interp_free, & 
       release_memory_DB, c_diff_fast_db, get_indices_by_coordinate,&
       mdl_filt_EPSplus, &
       indx, &                   ! need this as public for convert_output (See if they break others..)
       pre_init_DB, &              ! EMPTY
       delta_from_mask, rearrange_i_in

CONTAINS


  !**************************************************************************
  ! Begin wavelet transform subroutines
  !**************************************************************************
  !
  ! modified to have variable level input-output capability
  !
  !  u_in       1D data array input to be transformed
  !  u_out      1D data array that is the result of the transform
  !  indx_in    Index between wrk array and 1D u_in() array:  wrk(indx_in(i,1),indx_in(i,2)) = u_in(i)
  !  indx_out   Index between wrk array and 1D u_out() array:  u_out(i)=wrk(indx_out(i,1),indx_out(i,2))
  !  nwlt_in    Number of active points in input 1D array
  !  nwlt_out   Number of active points in output 1D array
  !  j_in       Forward transform is done from level j_in to  1
  !  j_out      Inverse transform is done from level  1 to  j_outc
  !  j_lev_in   Seems to be functionally the same as j_in  ????
  !  j_lev_out  Seems to be functionally the same as j_out ????
  !  wlt_fmly   Wavelet transform family based on order or type, e.g. Sweldens wavelts of differnt order, Haar, etc.
  !  i_coef     If 1 then do forward transform, If -1 then do inverse transform
  !
  !PRIVATE
  SUBROUTINE c_wlt_trns_wrk(u_in, u_out,  indx_in, indx_out, nwlt_in, nwlt_out, j_in, j_out, j_lev_in, j_lev_out, wlt_fmly, i_coef)
    USE precision
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: nwlt_in, nwlt_out, j_in, j_out, j_lev_in, j_lev_out, wlt_fmly, i_coef
    REAL (pr), DIMENSION (1:nwlt_in), INTENT (IN) :: u_in
    REAL (pr), DIMENSION (1:nwlt_out), INTENT (OUT) :: u_out
    INTEGER, DIMENSION (1:nwlt_in,1:3), INTENT(IN) :: indx_in
    INTEGER, DIMENSION (1:nwlt_out,1:3), INTENT(IN) :: indx_out
    INTEGER :: i,j,inum,idenom,ibndr
    INTEGER :: trnsf_type
    INTEGER :: ix
    INTEGER :: iy
    INTEGER :: iz

    trnsf_type = 0 !order of method, used in wgh_ arrays

    IF (i_coef == 1) THEN       ! forward transform
       IF(j_lev_in > j_lev_out ) THEN
          WRITE(*,'("!!!! ERROR 1, SUBROUTINE c_wlt_trns !!!!")')
          STOP
       END IF
       IF(j_in > j_lev_in ) THEN
          WRITE(*,'("!!!! ERROR 2, SUBROUTINE c_wlt_trns !!!!")')
          STOP
       END IF

       inum=1
       idenom=1
       IF(j_lev_out > j_lev_in) inum=2**(j_lev_out-j_lev_in)
       IF(j_lev_out < j_lev_in) idenom=2**(j_lev_in-j_lev_out)
       IF(j_lev_in == j_lev_out) THEN
          DO i=lv_intrnl(0)+1,lv_intrnl(j_in)
             wrk(indx_in(i,1),indx_in(i,2),indx_in(i,3)) = u_in(i)
          END DO
          DO ibndr=1,2*dim
             DO i=lv_bnd(0,ibndr,1),lv_bnd(1,ibndr,j_in)
                wrk(indx_in(i,1),indx_in(i,2),indx_in(i,3)) = u_in(i)
             END DO
          END DO
       ELSE 
          DO i=1,nwlt_in
             !DG populate working array
             wrk(indx_in(i,1)*inum/idenom,indx_in(i,2)*inum/idenom,indx_in(i,3)*inum/idenom) = u_in(i)
          END DO
       END IF

       ! do transform from j_in-1 to 1 levels
       DO j = j_in-1, 1, -1

          !
          ! Do the wlt transform for level j
          !
          call c_wlt_trns_aux(MAXVAL(nxyz), dim , j, j_lev_in, nxyz,&
               lvxyz_odd(:,:,wlt_fmly,trnsf_type), nlvxyz_odd(:,:,:,wlt_fmly,trnsf_type), &
               lvxyz_even(:,:,wlt_fmly,trnsf_type), nlvxyz_even(:,:,:,wlt_fmly,trnsf_type), &
               n_prdct, n_updt, wgh_prdct, wgh_updt, wlt_fmly, trnsf_type,  wrk ,jd, prd,i_coef )

       END DO
       DO i=1,nwlt_out
          u_out(i)=wrk(indx_out(i,1),indx_out(i,2),indx_out(i,3))
       END DO
       !
       !*********** Cleaning the wrk array **********************************************
       !
       inum=1
       idenom=1
       IF(j_lev_out > j_lev_in) inum=2**(j_lev_out-j_lev_in)
       IF(j_lev_out < j_lev_in) idenom=2**(j_lev_in-j_lev_out)
       IF(j_lev_in == j_lev_out) THEN
          DO i=lv_intrnl(0)+1,lv_intrnl(j_in)
             wrk(indx_in(i,1),indx_in(i,2),indx_in(i,3)) = 0.0_pr
          END DO
          DO ibndr=1,2*dim
             DO i=lv_bnd(0,ibndr,1),lv_bnd(1,ibndr,j_in)
                wrk(indx_in(i,1),indx_in(i,2),indx_in(i,3)) = 0.0_pr
             END DO
          END DO
       ELSE 
          DO i=1,nwlt_in
             wrk(indx_in(i,1)*inum/idenom,indx_in(i,2)*inum/idenom,indx_in(i,3)*inum/idenom) = 0.0_pr
          END DO
       END IF
    ELSE IF (i_coef == -1) THEN  !  inverse transform
       inum=1
       idenom=1
       IF(j_lev_out > j_lev_in) inum=2**(j_lev_out-j_lev_in)
       IF(j_lev_out < j_lev_in) idenom=2**(j_lev_in-j_lev_out)
       IF(j_lev_in == j_lev_out) THEN
          DO i=1,nwlt_in
             ix = indx_in(i,1)
             iy = indx_in(i,2)
             iz = indx_in(i,3)
             wrk(ix,iy,iz) = u_in(i)
          END DO
       ELSE
          DO i=1,nwlt_in
             ix = indx_in(i,1)
             iy = indx_in(i,2)
             iz = indx_in(i,3)
             IF (MOD(ix,idenom)==0 .AND. MOD(iy,idenom)==0 .AND. MOD(iz,idenom)==0) THEN
                wrk(ix*inum/idenom,iy*inum/idenom,iz*inum/idenom) = u_in(i)
             END IF
          END DO
       END IF
       DO j = 1, j_out-1

          !
          ! Do the wlt transform for level j
          !
          call c_wlt_trns_aux(MAXVAL(nxyz), dim , j, j_lev_out, &
               nxyz,lvxyz_odd(:,:,wlt_fmly,trnsf_type), nlvxyz_odd(:,:,:,wlt_fmly,trnsf_type), &
               lvxyz_even(:,:,wlt_fmly,trnsf_type), nlvxyz_even(:,:,:,wlt_fmly,trnsf_type), &
               n_prdct, n_updt, wgh_prdct, wgh_updt, wlt_fmly, trnsf_type,  wrk ,jd, prd,i_coef )
       END DO

       !put result into u_out
       DO i=1,nwlt_out
          IF( (indx_out(i,1).GT.nxyz(1)).OR.(indx_out(i,1).LT.0)) STOP 1
          IF( (indx_out(i,2).GT.nxyz(2)).OR.(indx_out(i,2).LT.0)) STOP 2
          IF( (indx_out(i,3).GT.jd*nxyz(3)).OR.(indx_out(i,3).LT.0)) STOP 3
          !PRINT *, indx_out(i,1),indx_out(i,2),indx_out(i,3), wrk(indx_out(i,1),indx_out(i,2),indx_out(i,3))
          u_out(i)=wrk(indx_out(i,1),indx_out(i,2),indx_out(i,3))
       END DO
       !
       !******************** Cleaning wrk ***************************************
       !
       inum=1
       idenom=1
       IF(j_lev_out > j_lev_in) inum=2**(j_lev_out-j_lev_in)
       IF(j_lev_out < j_lev_in) idenom=2**(j_lev_in-j_lev_out)
       IF(j_lev_in == j_lev_out) THEN
          DO i=1,nwlt_in
             wrk(indx_in(i,1),indx_in(i,2),indx_in(i,3)) = 0.0_pr
          END DO
       ELSE
          DO i=1,nwlt_in
             ix = indx_in(i,1)
             iy = indx_in(i,2)
             iz = indx_in(i,3)
             IF (MOD(ix,idenom)==0 .AND. MOD(iy,idenom)==0.AND. MOD(iz,idenom)==0) THEN
                wrk(ix*inum/idenom,iy*inum/idenom,iz*inum/idenom) = 0.0_pr
             END IF
          END DO
       END IF
       DO i=1,nwlt_out
          wrk(indx_out(i,1),indx_out(i,2),indx_out(i,3)) = 0.0_pr
       END DO
       !**************************************************************************
    ELSE
       print *,'ERROR, c_wlt_trns() called with i_coeff = ', i_coef
       print *,'ERROR  Correct values are 1 -fwd trns or -1 inv trns. Exiting ....'
       stop
    END IF

  END SUBROUTINE c_wlt_trns_wrk
  
  
  !
  ! Top level call to do a forward or inverse wlt transform on
  ! components in u(1:nwlt_in,:)  whose position in mask is .TRUE.
  ! mask_index locates masked vars in uin/u_out
  !
  !OLEG: this subroutine uses either indx_old or indx for indx_in 
  !the functionality changed for high-level subroutine to avoid using indx outside of DB
  !OLD SYNTAX: c_wlt_trns_mask(u_in, u_out, ie, mask, mask2, use_second_mask, indx_in, indx_out, nwlt_in, &
  !                            nwlt_out, j_in, j_out, j_lev_in, j_lev_out, i_coef,do_update_db_from_u_mask, do_update_u_from_db_mask)
  SUBROUTINE c_wlt_trns_mask(u_in, u_out, ie, mask, mask2, use_second_mask, old2new, &
       nwlt_in, nwlt_out, j_lev_in, j_lev_out, wlt_fmly, i_coef, &
       do_update_db_from_u_mask, do_update_u_from_db_mask, SKIP_TRANSFORM)
    USE precision
    USE sizes
    USE pde
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: nwlt_in, nwlt_out, j_lev_in, j_lev_out, wlt_fmly, i_coef
    INTEGER, INTENT(IN) ::  ie
    LOGICAL, INTENT(IN) :: mask(1:ie) , mask2(1:ie)! transform elements in u whose position in mask is .TRUE.
    LOGICAL, INTENT(IN) :: use_second_mask, old2new
    LOGICAL, INTENT(IN) :: do_update_db_from_u_mask, do_update_u_from_db_mask
    LOGICAL, INTENT(IN), OPTIONAL :: SKIP_TRANSFORM
    REAL (pr), DIMENSION (1:nwlt_in,ie), INTENT (INOUT) :: u_in
    REAL (pr), DIMENSION (1:nwlt_out,ie), INTENT (INOUT) :: u_out
    INTEGER :: i
    LOGICAL :: do_skip_transform
    !
    ! wrk version of c_wlt_trns_mask()
    ! defaults to 
    ! do_update_db_from_u_mask = .true. , do_update_u_from_db_mask = .true.
    do_skip_transform = .FALSE.
    IF (PRESENT(SKIP_TRANSFORM)) do_skip_transform = SKIP_TRANSFORM

    IF (.NOT.do_skip_transform) THEN
       DO i = 1,ie
          IF( mask(i) .OR. (use_second_mask .AND. mask2(i)) ) THEN
             !PRINT *, MAXVAL(ABS(u_in(:,n_var_index(i))))
             IF(old2new) THEN
                CALL c_wlt_trns_wrk(u_in(1:nwlt_in,n_var_index(i)), u_out(1:nwlt_out,n_var_index(i)),&
                     indx_old, indx, nwlt_in, nwlt_out,&
                     j_lev_in, j_lev_out, j_lev_in, j_lev_out, wlt_fmly, i_coef)
             ELSE
                CALL c_wlt_trns_wrk(u_in(1:nwlt_in,n_var_index(i)), u_out(1:nwlt_out,n_var_index(i)),&
                     indx, indx, nwlt_in, nwlt_out,&
                     j_lev_in, j_lev_out, j_lev_in, j_lev_out, wlt_fmly, i_coef)
             END IF
          END IF
       END DO
    ELSE
       u_out = u_in
    END IF

  END SUBROUTINE c_wlt_trns_mask


  !
  ! Top level call to do a forward or inverse wlt transform on
  ! components mn_var,mx_var of u_in and put the result in u_out
  !
  !PUBLIC
  !Oleg: the code outside DB does not need to use indx_old, it alwasy does transform on j_lev
  !thus the high-level transfrom syntax is changed to reflect this
  !OLD SYNTAX: c_wlt_trns(u_in, u_out, ie, mn_var,mx_var, indx_in, indx_out,&
  !                       nwlt_in, nwlt_out, j_in, j_out, j_lev_in, j_lev_out, i_coef)
  SUBROUTINE c_wlt_trns (u, ie, mn_var, mx_var, wlt_fmly, i_coef)
    USE precision
    USE sizes
    USE wlt_vars
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: wlt_fmly, i_coef
    INTEGER, INTENT(IN) :: mn_var,mx_var, ie
    REAL (pr), DIMENSION (1:nwlt,ie), INTENT (INOUT) :: u
    INTEGER :: i

    DO i = mn_var,mx_var
       CALL c_wlt_trns_wrk(u(1:nwlt,i), u(1:nwlt,i),  indx, indx, nwlt, nwlt,&
            j_lev, j_lev, j_lev, j_lev, wlt_fmly, i_coef)
    END DO

  END SUBROUTINE c_wlt_trns

  !**************************************************************************
  ! Begin wavelet transform subroutines
  !**************************************************************************
  !PUBLIC
  SUBROUTINE wlt_filt(u, nwlt, j_in, j_filt, regime)
    USE precision
    USE util_vars
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: nwlt, j_in, j_filt, regime
    REAL (pr), DIMENSION (1:nwlt), INTENT (INOUT) :: u
    INTEGER :: i,ibndr


    IF(regime == 0 .AND. j_filt < j_in ) THEN
       u(lv_intrnl(j_filt)+1:lv_intrnl(j_in))= 0.0_pr
       DO ibndr=1,2*dim
          u(lv_bnd(0,ibndr,j_filt):lv_bnd(1,ibndr,j_in)) = 0.0_pr
       END DO
    ELSE IF(regime == 1 .AND. j_filt < j_in ) THEN ! zonal filtering (filtering outside of the zone)
       DO i = lv_intrnl(j_filt)+1,lv_intrnl(j_in)
          IF( .NOT.(ALL(xyzzone(1,1:dim)<=x(i,1:dim)).AND.ALL(x(i,1:dim)<=xyzzone(2,1:dim)))) THEN
!!$          IF( .NOT.(xyzzone(1,1)<=x(i,1).AND.x(i,1)<=xyzzone(2,1).AND. &
!!$               xyzzone(1,2)<=x(i,2).AND.x(i,2)<=xyzzone(2,2).AND. &
!!$               xyzzone(1,3)<=x(i,3).AND.x(i,3)<=xyzzone(2,3))) THEN 
             u(i)=0.0_pr 
          END IF
       END DO
       DO ibndr=1,2*dim
          DO i = lv_bnd(0,ibndr,j_filt),lv_bnd(1,ibndr,j_in)
             IF( .NOT.(ALL(xyzzone(1,1:dim)<=x(i,1:dim)).AND.ALL(x(i,1:dim)<=xyzzone(2,1:dim)))) THEN
!!$             IF( .NOT.(xyzzone(1,1)<=x(i,1).AND.x(i,1)<=xyzzone(2,1).AND. &
!!$                  xyzzone(1,2)<=x(i,2).AND.x(i,2)<=xyzzone(2,2).AND. &
!!$                  xyzzone(1,3)<=x(i,3).AND.x(i,3)<=xyzzone(2,3))) THEN 
                u(i)=0.0_pr
             END IF
          END DO
       END DO
    END IF
  END SUBROUTINE wlt_filt

  SUBROUTINE mdl_filt_EPSplus( filt_mask, u_in, scl, leps, ne_local) 
    USE precision
    USE main_vars
    USE share_consts
    USE pde
    USE sizes
    USE field
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: ne_local  
    REAL (pr), DIMENSION (1:nwlt,1:ne_local) , INTENT (IN) :: u_in
    REAL (pr), DIMENSION (1:nwlt) , INTENT (INOUT) :: filt_mask
    REAL (pr) , INTENT (IN)   :: scl(1:ne_local)
    REAL (pr) , INTENT (IN)   :: leps ! local epsilon
    INTEGER :: i,ie

    i_c_init = .FALSE. ! tell significant_wlt() to redefine new grid for i_c if necessary
    i_c = .FALSE.
    DO ie = 1, ne_local
       CALL significant_wlt (u_in(1:nwlt,ie), scl(ie)*leps, nwlt, j_lev, j_lev, j_mx, j_mn, nxyz)
    END DO

    DO ie = 1, ne_local
       CALL adjacent_wlt (u_in(1:nwlt,ie), scl(ie)*leps, nwlt, &
            ij_adj, adj_type, j_lev, j_lev, j_mn, j_mx, ie, ne_local )
    END DO

    CALL reconstr_check (i_c, HIGH_ORDER, NORMAL, nxyz, j_lev)
    
    FORALL (i=1:nwlt, i_c(indx(i,1),indx(i,2),indx(i,3)) )
       filt_mask(i) = 1.0_pr
    END FORALL
    
  END SUBROUTINE mdl_filt_EPSplus
  !
  ! 
  !**************************************************************************
  !
  ! Find spacial derivatives, vector version
  ! Calls the old c_diff_fast for each value in second arg of u
  !
  !
  ! Arguments
  !u, du, d2u, j_in, nlocal, meth, meth1, 
  ! ID Sets which derivative is done 
  !    10 - do first derivative
  !    11 - do first and secont derivative
  !    01 - do second derivative
  !    if ID > 0 then the second derivative is central difference D_central
  !    if ID < 0 then the second derivative is D_backward_bias followed by D_forward_bias
  ! meth  - order of the method, high low,
  !
  !  dimensions of u are u(1:nlocal,1:ie) 
  !  derivative is done on  u(1:nlocal,mn_var:mx_var)
  !PUBLIC
  SUBROUTINE c_diff_fast(u, du, d2u, j_in, nlocal, meth, id, ie, mn_var, mx_var, FORCE_RECTILINEAR )
    USE precision
    USE sizes
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: j_in, nlocal, meth, id
    INTEGER, INTENT(IN) :: ie ! number of dimensions of u(:,1:ie)  we are doing derivative for (forced to 1 for now)
    INTEGER, INTENT(IN) ::  mn_var, mx_var   ! min and max of second index of u that we are doing the 1st derivatives for.
    !  INTEGER, INTENT(IN) ::  mn_varD2, mx_varD2 ! min and max of second index of u that we are doing the 2nd derivatives for.
    INTEGER :: i, idim
    REAL (pr), DIMENSION (1:nlocal, ie), INTENT (IN) :: u
    REAL (pr), DIMENSION (ie, 1:nlocal,dim), INTENT (INOUT) :: du,d2u

    LOGICAL, INTENT (IN), OPTIONAL :: FORCE_RECTILINEAR        ! In the case where curvilinear mapping is specified, force a rectilinear derivative (skip transform)

    IF( debug_c_diff_fast>=1) THEN
       PRINT *,'c_diff_fast() enter' !debug
       DO i = 1,ie
          WRITE(*,'( "minmax u(:,",i2.2,") ", 2(G20.12,1X) )') &
               i, MINVAL(u(:,i)),MAXVAL(u(:,i)) !debug 
       END DO
    END IF
    IF( debug_c_diff_fast>=3) THEN
       DO i=1,nlocal
          WRITE (*,'("iwlt=",I3,", u=",G20.12,", du=",2G20.12)') i,u(i,1), du(1,i,:)
       END DO
       PAUSE 'before wlt_diff_DB'
    END IF
    
    
    DO i = mn_var, mx_var
       CALL c_diff_fast_wrk(i, u, du , d2u, j_in, nlocal, ie, meth, id)
    END DO
    
    
    IF( debug_c_diff_fast>=1) THEN
       PRINT *,'c_diff_fast()' !debug
       DO i = 1,mx_var-mn_var+1
          DO idim = 1,dim
             WRITE(*,'( "minmax du(",i2.2,":,",i2.2,") ", 2(G20.12,1X) )') &
                  i,idim,MINVAL(du(i,:,idim)),MAXVAL(du(i,:,idim)) !debug 
          ENDDO
       END DO
       IF(MOD(id,10) == 1 ) THEN ! du2 was calculated, so print it out also
          DO i = 1,mx_var-mn_var+1
             DO idim = 1,dim
                WRITE(*,'( "minmax d2u(",i2.2,":,",i2.2,") ", 2(G20.12,1X) )') &
                     i,idim,MINVAL(d2u(i,:,idim)),MAXVAL(d2u(i,:,idim)) !debug 
             ENDDO
          END DO
       END IF
    END IF
    IF( debug_c_diff_fast>=3) THEN
       DO i=1,nlocal
          WRITE (*,'("iwlt=",I3,", u=",G20.12,", du=",2G20.12)') i,u(i,1), du(1,i,:)
       END DO
       PAUSE 'after wlt_diff_DB'
    END IF


  END SUBROUTINE c_diff_fast

  !
  ! 
  !**************************************************************************
  !
  ! Find spacial derivatives
  !
  !
  ! Arguments
  !u, du, d2u, j_in, nlocal, meth, meth1, 
  ! ID Sets which derivative is done 
  !    10 - do first derivative
  !    11 - do first and secont derivative
  !    01 - do second derivative
  !    if ID > 0 then the second derivative is central difference D_central
  !    if ID < 0 then the second derivative is D_backward_bias followed by D_forward_bias
  ! meth  - order of the method, high low,
  SUBROUTINE c_diff_fast_wrk(do_ie, u, du, d2u, j_in, nlocal, ie, meth, id)
    USE precision
    USE sizes
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: j_in, nlocal, meth, id
    INTEGER, INTENT(IN) ::   ie  ! number of equations in u
    INTEGER, INTENT(IN) :: do_ie ! which of the ie equations we will do the deriviative for
    REAL (pr), DIMENSION (1:nlocal,ie), INTENT (IN) :: u
    REAL (pr), DIMENSION (ie,1:nlocal,dim), INTENT (INOUT) :: du,d2u
    REAL (pr), DIMENSION (1:nwlt,dim) :: duh,d2uh
    INTEGER :: i,j,idim
    INTEGER :: ibndr, ishift
    INTEGER :: wlt_fmly, trnsf_type, cntr

!!$    PRINT *, 'j_full set to 1'
!!$    j_full = 1

    wlt_fmly = MOD(meth,2) 
    trnsf_type = 0

    DO i=1,lv_intrnl(j_in)
       wrk(indx(i,1),indx(i,2),indx(i,3)) = u(i,do_ie)
    END DO
    cntr = lv_intrnl(j_in)
    DO ibndr=1,2*dim
       ishift = lv_bnd(2,ibndr,j_in)-lv_bnd(0,ibndr,1)
       DO i=lv_bnd(0,ibndr,1),lv_bnd(1,ibndr,j_in)
          wrk(indx(i,1),indx(i,2),indx(i,3)) = u(i+ishift,do_ie)
          cntr = cntr + 1
       END DO
    END DO

!!$    PRINT *, 'j_in=', j_in, 'j_lev=', j_lev, 'j_full=', j_full
!!$    PRINT *, '# wrk values set:', cntr

    !
    !---------- forward transform -----------------------------
    !
!!$    DO j = j_in-1,2,-1 !@
    DO j = MIN(j_in,j_lev)-1,  MIN(j_in,j_full), -1 !@
!!$       PRINT *, 'j=',j, 'calling c_wlt_trns_aux()'
       !
       ! Do the wlt transform for level j
       !
!!$       PRINT *, 'MIN/MAXVAL(ABS(wrk))=', MINVAL(ABS(wrk)), MAXVAL(ABS(wrk))
!!$       PRINT *, 'MIN/MAXVAL(u))=', MINVAL(ABS(u(:,do_ie))), MAXVAL(ABS(u(:,do_ie)))
       call c_wlt_trns_aux(MAXVAL(nxyz), dim , j, j_lev, nxyz,&
            lvxyz_odd(:,:,wlt_fmly,trnsf_type),  nlvxyz_odd(:,:,:,wlt_fmly,trnsf_type), &
            lvxyz_even(:,:,wlt_fmly,trnsf_type), nlvxyz_even(:,:,:,wlt_fmly,trnsf_type), &
            n_prdct, n_updt, wgh_prdct, wgh_updt, wlt_fmly, trnsf_type,  wrk ,jd, prd, WLT_TRNS_FWD )
!!$       PRINT *, 'MIN/MAXVAL(ABS(wrk))=', MINVAL(ABS(wrk)), MAXVAL(ABS(wrk))
!!$       PRINT *, 'MIN/MAXVAL(u))=', MINVAL(ABS(u(:,do_ie))), MAXVAL(ABS(u(:,do_ie)))
    END DO

    !
    !----------------  inverse transform ----------------------
    !
!!$    DO j = 1,MIN(j_in,j_lev) !@
    DO j = MIN(j_in,j_full),MIN(j_in,j_lev) !@
       
       !
       ! calculate some of the derivatives at each level as we do the inverse transform
       !
       !----------------- Calculation of first derivative ---------------
       !
       IF(INT(ID/10) == 1 ) THEN
          IF(j < j_in)  CALL diff_aux (MAXVAL(nxyz), duh, wgh_df, lv, nlv, nlvD, nwlt, j, meth, 1)

          !calculate finest level derivative
          IF(j == j_in) CALL diff_aux (MAXVAL(nxyz), duh, wgh_df, lvj, nlvj, nlvjD, nwltj, j, meth, 1)
       END IF
       !
       !----------------- Calculation of second derivative ---------------
       !
       IF(MOD(ID,10) == 1 ) THEN
          IF(j < j_in)  CALL diff_aux (MAXVAL(nxyz), d2uh, wgh_d2f, lv, nlv, nlvD, nwlt, j, meth, 2)

          !calculate finest level derivative
          IF(j == j_in) CALL diff_aux (MAXVAL(nxyz), d2uh, wgh_d2f, lvj, nlvj, nlvjD, nwltj, j, meth, 2)
       END IF
       !iz = 0 
       !
       !*********** transform in y-direction *********************
       !        
       IF( j < j_in ) THEN

          call c_wlt_trns_aux(MAXVAL(nxyz), dim , j, j_lev, nxyz,&
               lvdxyz_odd(:,:,wlt_fmly),  nlvdxyz_odd(:,:,:,wlt_fmly), &
               lvdxyz_even(:,:,wlt_fmly), nlvdxyz_even(:,:,:,wlt_fmly), &
               n_prdct, n_updt, wgh_prdct, wgh_updt, wlt_fmly, trnsf_type,  wrk ,jd, prd, WLT_TRNS_INV )

       END IF

    END DO


    !put result of the deriviative calculations into du and d2u
    IF(INT(ID/10) == 1 ) THEN
       du(do_ie,1:lv_intrnl(j_in),1:dim)=duh(1:lv_intrnl(j_in),1:dim)
       DO ibndr=1,2*dim
          du(do_ie,lv_bnd(2,ibndr,j_in):lv_bnd(3,ibndr,j_in),1:dim)  = duh(lv_bnd(0,ibndr,1):lv_bnd(1,ibndr,j_in),1:dim)
       END DO
    END IF
    IF(MOD(ID,10) == 1 ) THEN
       d2u(do_ie,1:lv_intrnl(j_in),1:dim)=d2uh(1:lv_intrnl(j_in),1:dim)
       DO ibndr=1,2*dim
          d2u(do_ie,lv_bnd(2,ibndr,j_in):lv_bnd(3,ibndr,j_in),1:dim) = d2uh(lv_bnd(0,ibndr,1):lv_bnd(1,ibndr,j_in),1:dim)
       END DO
    END IF

    !
    !******************** Cleaning wrk ***************************************
    !
    IF( dim == 3 ) THEN
       idim = 3
       DO i=1,lvdxyz_even(MIN(j_in-1,j_lev),idim,wlt_fmly)
          wrk(nlvdxyz_even(1,idim,i,wlt_fmly),nlvdxyz_even(2,idim,i,wlt_fmly),nlvdxyz_even(4,idim,i,wlt_fmly)) = 0.0_pr
       END DO
       DO i=1,lvdxyz_odd(MIN(j_in-1,j_lev),idim,wlt_fmly)
          wrk(nlvdxyz_odd(1,idim,i,wlt_fmly),nlvdxyz_odd(2,idim,i,wlt_fmly),nlvdxyz_odd(4,idim,i,wlt_fmly)) = 0.0_pr
       END DO
    END IF
    idim = 2
    DO i=1,lvdxyz_even(MIN(j_in-1,j_lev),idim,wlt_fmly)
       wrk(nlvdxyz_even(1,idim,i,wlt_fmly),nlvdxyz_even(4,idim,i,wlt_fmly),nlvdxyz_even(3,idim,i,wlt_fmly)) = 0.0_pr
    END DO
    DO i=1,lvdxyz_odd(MIN(j_in-1,j_lev),idim,wlt_fmly)
       wrk(nlvdxyz_odd(1,idim,i,wlt_fmly),nlvdxyz_odd(4,idim,i,wlt_fmly),nlvdxyz_odd(3,idim,i,wlt_fmly)) = 0.0_pr
    END DO
    idim = 1
    DO i=1,lvdxyz_even(MIN(j_in-1,j_lev),idim,wlt_fmly)
       wrk(nlvdxyz_even(4,idim,i,wlt_fmly),nlvdxyz_even(2,idim,i,wlt_fmly),nlvdxyz_even(3,idim,i,wlt_fmly)) = 0.0_pr
    END DO
    DO i=1,lvdxyz_odd(MIN(j_in-1,j_lev),idim,wlt_fmly)
       wrk(nlvdxyz_odd(4,idim,i,wlt_fmly),nlvdxyz_odd(2,idim,i,wlt_fmly),nlvdxyz_odd(3,idim,i,wlt_fmly)) = 0.0_pr
    END DO

    !**************************************************************************

    !Added by Oleg feb 20,2003. Not all the points were zeroed on the coarsest level
    IF(j_in == 1) THEN
       DO i=1,lv_intrnl(j_in)
          wrk(indx(i,1),indx(i,2),indx(i,3)) = 0.0_pr
       END DO
       DO ibndr=1,2*dim
          DO i=lv_bnd(0,ibndr,1),lv_bnd(1,ibndr,j_in)
             wrk(indx(i,1),indx(i,2),indx(i,3)) = 0.0_pr
          END DO
       END DO
    END IF
    wrk = 0.0_pr

  END SUBROUTINE c_diff_fast_wrk


  !
  !**************************************************************************
  !
  ! Arguments
  !u, du, d2u, j_in, nlocal, meth, meth1, 
  ! ID Sets which derivative is done 
  !    10 - do first derivative
  !    11 - do first and secont derivative
  !    01 - do second derivative
  !    if ID > 0 then the second derivative is central difference D_central
  !    if ID < 0 then the second derivative is D_backward_bias followed by D_forward_bias
  !
  SUBROUTINE c_diff_diag( du, d2u, j_in, nlocal, meth, meth1, ID, FORCE_RECTILINEAR)
    USE precision
    USE sizes
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: j_in, nlocal, meth, meth1, id
    REAL (pr), DIMENSION (1:nlocal,dim), INTENT (INOUT) :: du,d2u
    REAL (pr), DIMENSION (1:nwlt,dim) :: duh,d2uh
    INTEGER :: j
    INTEGER :: ibndr, trnsf_type

    LOGICAL, INTENT (IN), OPTIONAL :: FORCE_RECTILINEAR        ! In the case where curvilinear mapping is specified, force a rectilinear derivative (skip transform)


    trnsf_type = 0

    !
    !----------------  inverse transform ----------------------
    !
!!$    DO j = 1,MIN(j_in,j_lev) !@
    DO j = MIN(j_in,j_full),MIN(j_in,j_lev) !@
       !
       !----------------- Calculation of first derivative ---------------
       !
       IF(INT(ABS(ID)/10) == 1) THEN
          IF(j < j_in)  CALL diff_diag_aux (MAXVAL(nxyz), duh, wgh_df, lv, nlv, nlvD, nwlt, j, meth, meth, 1)
          IF(j == j_in) CALL diff_diag_aux (MAXVAL(nxyz), duh, wgh_df, lvj, nlvj, nlvjD, nwltj, j, meth, meth, 1)
       END IF
       !
       !----------------- Calculation of second derivative D^2 ---------------
       !
       IF(MOD(ID,10) == 1 .AND. ID > 0) THEN
          IF(j < j_in)  CALL diff_diag_aux (MAXVAL(nxyz), d2uh, wgh_d2f, lv, nlv, nlvD, nwlt, j, meth, meth, 2)
          IF(j == j_in) CALL diff_diag_aux (MAXVAL(nxyz), d2uh, wgh_d2f, lvj, nlvj, nlvjD, nwltj, j, meth, meth, 2)
       END IF
       !
       !----------------- Calculation of second derivative DD ---------------
       !
       IF(MOD(-ID,10) == 1 .AND. ID < 0) THEN
          IF(j < j_in)  CALL diff_diag_aux (MAXVAL(nxyz), d2uh, wgh_df, lv, nlv, nlvD, nwlt, j, meth, meth1, 3)
          IF(j == j_in) CALL diff_diag_aux (MAXVAL(nxyz), d2uh, wgh_df, lvj, nlvj, nlvjD, nwltj, j, meth, meth1, 3)
       END IF
    END DO

    IF(INT(ABS(ID)/10) == 1 ) THEN
       du(1:lv_intrnl(j_in),1:dim)=duh(1:lv_intrnl(j_in),1:dim)
       DO ibndr=1,2*dim
          du(lv_bnd(2,ibndr,j_in):lv_bnd(3,ibndr,j_in),1:dim)  = duh(lv_bnd(0,ibndr,1):lv_bnd(1,ibndr,j_in),1:dim)
       END DO
    END IF
    IF(MOD(ID,10) == 1 .AND. ID > 0) THEN
       d2u(1:lv_intrnl(j_in),1:dim)=d2uh(1:lv_intrnl(j_in),1:dim)
       DO ibndr=1,2*dim
          d2u(lv_bnd(2,ibndr,j_in):lv_bnd(3,ibndr,j_in),1:dim) = d2uh(lv_bnd(0,ibndr,1):lv_bnd(1,ibndr,j_in),1:dim)
       END DO
    END IF
    IF(MOD(-ID,10) == 1 .AND. ID < 0) THEN
       d2u(1:lv_intrnl(j_in),1:dim)=d2uh(1:lv_intrnl(j_in),1:dim)
       DO ibndr=1,2*dim
          d2u(lv_bnd(2,ibndr,j_in):lv_bnd(3,ibndr,j_in),1:dim) = d2uh(lv_bnd(0,ibndr,1):lv_bnd(1,ibndr,j_in),1:dim)
       END DO
    END IF

  END SUBROUTINE c_diff_diag

  !
  !**************************************************************************
  !
  SUBROUTINE c_diff_diag_bnd(du, d2u, j_in, nlocal, meth)
    USE precision
    USE sizes
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: j_in, nlocal, meth
    REAL (pr), DIMENSION (1:nlocal,dim), INTENT (INOUT) :: du,d2u
    REAL (pr), DIMENSION (1:nwlt,dim) :: duh
    INTEGER :: j
    INTEGER :: ibndr, trnsf_type

    trnsf_type = 0


!!$    DO j = 1,MIN(j_in,j_lev) !@
    DO j = MIN(j_in,j_full),MIN(j_in,j_lev) !@
       !
       !----------------- Calculation of first derivative ---------------
       !
       IF(j < j_in)  CALL diff_diag_bnd_aux (MAXVAL(nxyz), duh, wgh_df, wgh_d2f, lv, nlv, nlvD, nwlt, j, meth)
       IF(j == j_in) CALL diff_diag_bnd_aux (MAXVAL(nxyz), duh, wgh_df, wgh_d2f, lvj, nlvj, nlvjD, nwltj, j, meth)
       !
    END DO

    du(1:lv_intrnl(j_in),1:dim)=duh(1:lv_intrnl(j_in),1:dim)
    DO ibndr=1,2*dim
       du(lv_bnd(2,ibndr,j_in):lv_bnd(3,ibndr,j_in),1:dim)  = duh(lv_bnd(0,ibndr,1):lv_bnd(1,ibndr,j_in),1:dim)
    END DO

  END SUBROUTINE c_diff_diag_bnd


  SUBROUTINE diff_aux (max_nxyz,du, wghd_in, lv_in, nlv_in, nlvD_in, len_nlv, j_in, meth, order)

    USE precision
    USE sizes
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: len_nlv, j_in, meth, order,max_nxyz
    INTEGER, DIMENSION (0:j_lev), INTENT (IN) :: lv_in
    INTEGER, DIMENSION (1:4,1:len_nlv), INTENT (IN) :: nlv_in
    INTEGER, DIMENSION (1:4*dim,1:len_nlv,0:n_df), INTENT (IN) :: nlvD_in
    REAL (pr), DIMENSION (1:nwlt,dim),   INTENT (INOUT) :: du
    REAL (pr), DIMENSION (-2*n_diff-1:2*n_diff+1,0:max_nxyz,j_lev,0:n_df,1:3), INTENT (IN) :: wghd_in
    INTEGER :: i,k,ix,iy,iz,ixp,ixpm,iyp,iypm,izp,izpm,iD, idim 
    INTEGER :: iD3d

    INTEGER ,SAVE :: itest = 0 !DEBUG ONLY
    character (len=3) :: testchar !DEBUG ONLY

    IF( debug_c_diff_fast>=4) THEN
       WRITE(*,'(A,i6,A,i6)') "diff_aux(): nlocal =",nwlt, " max_nxyz = ",max_nxyz
       WRITE(*,'(A,i2,A,i2,A,i2)') "j_in = ",j_in, "meth = ",meth, "order = ",order
       WRITE(*,'(A,i2,A,i2,A)') "mn_var = ", 1, "mx_var = ", 1

       itest = itest +1
       write(testchar,'(i3.3)') itest
       OPEN(UNIT=111,FILE='tmpdu.wrk'//testchar,FORM='formatted')
       WRITE(*,'(A,A,A)') "Opening: ",'tmpdu.wrk'//testchar,"for debugging output"
    END IF

    !!PRINT *,'diff_aux j_in =', j_in
    iD=0
    IF(ORDER == 2) iD=4


    DO k = lv_in(j_in-1)+1, lv_in(j_in)
       i = nlv_in(1,k)
       ix   = nlv_in(2,k)
       iy   = nlv_in(3,k)
       iz   = nlv_in(4,k)
       du(i,1)=0.e0_pr
       idim = 1
!!$PRINT*,'TEST idim==1 loop bounds', nlvD_in(iD+1,k,meth),nlvD_in(iD+2,k,meth),'ID ', ID
       DO ixpm=nlvD_in(iD+1,k,meth),nlvD_in(iD+2,k,meth)
          ixp = (1-prd(idim))*(ix+ixpm*2**(j_lev-j_in))+prd(idim)*MOD(ix+ixpm*2**(j_lev-j_in)+9*nxyz(idim),nxyz(idim))

          du(i,1)=du(i,1)+wghd_in(ixpm,ix,j_in,meth,idim)*wrk(ixp,iy,iz)
          !PRINT *,'du = ',du(i,1),j_in
          ! TEST only, This will slow things down... D.G.
          !        IF( ABS(du(i,1)) > 1.e12) THEN
          !           PRINT *, 'WARNING:',i,du(i,1),wghd_in(ixpm,ix,j_in,meth,idim),wrk(ixp,iy,iz)
          !        END IF
          IF( debug_c_diff_fast>=4) THEN
             !WRITE(*,'(A,i,A,i)') & 
             !	 "indx_line_db = ",ix, "ixp = ",ixp "ixp = ",ixp
             !WRITE(*,'(A,G20.12,A,G20.12)') &
             !	"du =",du(i,1), &
             !	" line_u ",wrk(ixp,iy,iz)
             WRITE(111,'(i6,1X,i6,1X,i6,1X,G20.12,1X,G20.12)') i,ix/2**(j_mx-j_in), ixp/2**(j_mx-j_in), &
                  du(i,1), &
                  wrk(ixp,iy,iz)
          END IF

       END DO
       du(i,2)=0.e0_pr
       idim = 2
!!$PRINT*,'TEST idim==2 loop bounds', nlvD_in(iD+3,k,meth),nlvD_in(iD+4,k,meth),'ID ', ID

       DO iypm=nlvD_in(iD+3,k,meth),nlvD_in(iD+4,k,meth)
          iyp = (1-prd(idim))*(iy+iypm*2**(j_lev-j_in))+prd(idim)*MOD(iy+iypm*2**(j_lev-j_in)+9*nxyz(idim),nxyz(idim))
          !problem here in 3D, 
          !PRINT*,'TEST','i',i,'iypm',iypm,'iy',iy,'j_in',j_in,'meth',meth,'ID',ID
          !PRINT*,'TEST','idim',idim,'ix',ix,'iyp',iyp,'iz',iz,'j_lev',j_lev,'j_in',j_in
          du(i,2)=du(i,2)+wghd_in(iypm,iy,j_in,meth,idim)*wrk(ix,iyp,iz)
          !PRINT *,'du = ',du(i,1),j_in
          ! 		WRITE (*, '( 3(i5," ") ,  2( es15.7, " " )," wrk" )' ) &
          !				ix, iyp , iz , wrk(ix,iyp,iz),wghd_in(izpm,iy,j_in,meth,idim)
       END DO

       IF( dim == 3 ) THEN
          du(i,3)=0.e0_pr
          idim = 3
          iD3d = 0
          IF(ORDER == 2) iD3d=2 
          DO izpm=nlvD_in(iD3d+9,k,meth),nlvD_in(iD3d+10,k,meth)
             izp = (1-prd(idim))*(iz+izpm*2**(j_lev-j_in))+prd(idim)*MOD(iz+izpm*2**(j_lev-j_in)+9*nxyz(idim),nxyz(idim))

             du(i,3)=du(i,3)+wghd_in(izpm,iz,j_in,meth,idim)*wrk(ix,iy,izp)
             !PRINT *,'du = ',du(i,1),j_in
             !! 		WRITE (*, '( 4(i5," ") ,  3( es15.7, " " ) )' ) &
             !!				ix, iy , iz, izp , wrk(ix,iy,izp),wghd_in(izpm,iz,j_in,meth,idim),du(i,3)

          END DO
       ENDIF

    END DO

    IF( debug_c_diff_fast>=4) THEN
       CLOSE(UNIT=111)
    END IF
  END SUBROUTINE diff_aux

  SUBROUTINE diff_diag_aux (max_nxyz, du, wghd_in, lv_in, nlv_in, nlvD_in, len_nlv, j_in, meth, meth1, order)

    USE precision
    USE sizes
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: len_nlv, j_in, meth, meth1, order, max_nxyz
    INTEGER, DIMENSION (0:j_lev), INTENT (IN) :: lv_in
    INTEGER, DIMENSION (1:4,1:len_nlv), INTENT (IN) :: nlv_in
    INTEGER, DIMENSION (1:4*dim,1:len_nlv,0:n_df), INTENT (IN) :: nlvD_in
    REAL (pr), DIMENSION (1:nwlt,dim),   INTENT (INOUT) :: du
    REAL (pr), DIMENSION (-2*n_diff-1:2*n_diff+1,0:max_nxyz,j_lev,0:n_df,1:3), INTENT (IN) :: wghd_in
    INTEGER :: i,k,iD,nD,idim 
    INTEGER :: ih,ix_l,ix_h,l,nxj,ixl
    INTEGER, DIMENSION (dim) :: ixyz
    iD = 0
    ixyz = 0
    IF(ORDER == 2) iD=4

    IF(ORDER < 3) THEN
       DO k = lv_in(j_in-1)+1, lv_in(j_in)
          i = nlv_in(1,k)
          ixyz(1:dim) = nlv_in(2:1+dim,k)
          DO idim = 1, dim
             du(i,idim) = wghd_in(0,ixyz(idim),j_in,meth,idim)
          END DO
       END DO
    ELSE IF(ORDER == 3) THEN
       nD=n_diff
       DO k = lv_in(j_in-1)+1, lv_in(j_in)
          i = nlv_in(1,k)
          ixyz(1:dim) = nlv_in(2:1+dim,k)
          DO idim = 1,dim
             nxj = mxyz(idim)*2**(j_in-1)
             ! some weight are zero by definition in [-2nD-1,2nD+1]
             ih   = ixyz(idim)/2**(j_lev-j_in)
             ix_l=(1-prd(idim))*MAX(-2*nD-1,-ih) + prd(idim)*(-2*nD-1)
             ix_h=(1-prd(idim))*MIN(2*nD+1,nxj-ih) + prd(idim)*(2*nD+1)
             du(i,idim) = 0.0_pr
             DO l = ix_l, ix_h
                ixl = ixyz(idim)+l*2**(j_lev-j_in)
                ixl = (1-prd(idim))*ixl + prd(idim)*MOD(ixl+9*nxyz(idim),nxyz(idim))
                du(i,idim) = du(i,idim)+ wghd_in(l,ixyz(idim),j_in,meth,idim)*wghd_in(-l,ixl,j_in,meth1,idim)
             END DO
          END DO

!!$        idim = 1 
!!$        ih   = ix/2**(j_lev-j_in)
!!$        ix_l=(1-prd(idim))*MAX(-2*nD-1,-ih) + prd(idim)*(-2*nD-1)
!!$        ix_h=(1-prd(idim))*MIN(2*nD+1,nxj-ih) + prd(idim)*(2*nD+1)
!!$        du(i,1) = 0.0_pr
!!$        DO l = ix_l, ix_h
!!$           ixl = ix+l*2**(j_lev-j_in)
!!$           ixl = (1-prd(idim))*ixl + prd(idim)*MOD(ixl+9*nxyz(idim),nxyz(idim))
!!$           du(i,1) = du(i,1)+ wghd_in(l,ix,j_in,meth,idim)*wghd_in(-l,ixl,j_in,meth1,idim)
!!$        END DO
!!$        !
!!$        idim = 2
!!$        ih   = iy/2**(j_lev-j_in)
!!$        iy_l=(1-prd(idim))*MAX(-2*nD-1,-ih) + prd(idim)*(-2*nD-1)
!!$        iy_h=(1-prd(idim))*MIN(2*nD+1,nyj-ih) + prd(idim)*(2*nD+1)
!!$        du(i,2) = 0.0_pr
!!$        DO l = iy_l, iy_h 
!!$           iyl = iy+l*2**(j_lev-j_in)
!!$           iyl = (1-prd(idim))*iyl + prd(idim)*MOD(iyl+9*nxyz(idim),nxyz(idim))
!!$           dum = wghd_in(-l,iyl,j_in,meth1,idim)
!!$           du(i,2) = du(i,2)+ wghd_in(l,iy,j_in,meth,idim)*wghd_in(-l,iyl,j_in,meth1,idim)
!!$        END DO
       END DO
    END IF

  END SUBROUTINE diff_diag_aux

  !
  !
  !
  ! apply the weights to the wlt coefficients to calculate a derivative.
  !
  SUBROUTINE diff_diag_bnd_aux (max_nxyz, du, wghd_in, wghd2_in, lv_in, nlv_in, nlvD_in, len_nlv, j_in, meth)

    USE precision
    USE sizes
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: len_nlv, j_in, meth,max_nxyz
    INTEGER, DIMENSION (0:j_lev), INTENT (IN) :: lv_in
    INTEGER, DIMENSION (1:4,1:len_nlv), INTENT (IN) :: nlv_in
    INTEGER, DIMENSION (1:4*dim,1:len_nlv,0:n_df), INTENT (IN) :: nlvD_in
    REAL (pr), DIMENSION (1:nwlt,dim),   INTENT (INOUT) :: du
    REAL (pr), DIMENSION (-2*n_diff-1:2*n_diff+1,0:max_nxyz,j_lev,0:n_df,1:3), INTENT (IN) :: wghd_in, wghd2_in
    INTEGER :: i,k,idim 
    INTEGER :: ix_l,ix_h
    INTEGER ::ixyz(3)


    DO k = lv_in(j_in-1)+1, lv_in(j_in)
       i = nlv_in(1,k)
       ixyz(1:3) = nlv_in(2:4,k)

       DO idim = 1,dim
          IF(ixyz(idim) > 0 .AND. ixyz(idim) < nxyz(idim)) THEN
             ix_l=MIN(ixyz(idim)/2**(j_lev-j_in),2*n_diff+1)
             ix_h=MIN((nxyz(idim)-ixyz(idim))/2**(j_lev-j_in),2*n_diff+1)
             du(i,idim) = -wghd2_in(-ix_l,ixyz(idim),j_in,meth,idim)* &
                  wghd_in(ix_l,0,j_in,meth,idim)/wghd_in(0,0,j_in,meth,idim) &
                  -wghd2_in(ix_h,ixyz(idim),j_in,meth,idim)* &
                  wghd_in(-ix_h,nxyz(idim),j_in,meth,idim)/wghd_in(0,nxyz(idim),j_in,meth,idim)
          ELSE
             du(i,idim) = 0.0_pr
          END IF
       END DO

    END DO

  END SUBROUTINE diff_diag_bnd_aux

  !
  !************ Calculating Maximum Level ***************************
  !
  ! 
  SUBROUTINE max_level(coef, leps, nwlt, j_in, j_out, j_mx, j_mn)
    USE precision
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: j_in, j_mx, j_mn, nwlt
    INTEGER, INTENT (OUT) :: j_out
    REAL (pr), INTENT (IN) :: leps
    REAL (pr), DIMENSION (1:nwlt), INTENT (IN) :: coef
    INTEGER :: i


    IF(i_c_init) THEN
       j_out=1
       i_c_init=.FALSE.
    END IF

    ! Find the maximum level of all the collocation points with wlt coefficients >=  leps
    DO i = 1, nwlt
       IF (ABS (coef(i)) >= leps) THEN
          j_out = MAX (j_out, ij2j(indx(i,1),j_in), ij2j(indx(i,2),j_in))
          IF( dim == 3 ) j_out = MAX (j_out, ij2j(indx(i,3),j_in) )
       END IF
    END DO

  END SUBROUTINE max_level

  
  !************ Calculating Significant wavelets ***************************
  !
  ! coef - 1D adaptive array of wlt coefficients currently active
  ! leps  - theshold
  ! nwlt - number of wavalets
  ! j_in             -  Current number of levels
  ! j_out            -  New number of levels, Max_level called right before this routine
  !                     sets the currrently highest active level and it is passed in as j_out,
  !                      Then if i_c_init==TRUE then j_out = j_out +1 to add one more level above
  !                      the highest active level
  ! j_mn             -   all points are kept below this level
  ! j_mx             -  maximum level we will use
  !

  SUBROUTINE significant_wlt (coef, leps, nwlt, j_in, j_out, j_mx, j_mn, nxyz_out)
    USE precision
    USE share_consts
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: j_in, j_mx, j_mn, nwlt
    INTEGER, INTENT (INOUT) :: j_out
    INTEGER, DIMENSION(3) :: nxyz_out
    REAL (pr), INTENT (IN) :: leps
    REAL (pr), DIMENSION (1:nwlt), INTENT (IN) :: coef
    INTEGER :: i, inum, idenom
    IF(i_c_init) THEN
       nxyz_out(1) = mxyz(1)*2**(j_out-1)
       nxyz_out(2) = mxyz(2)*2**(j_out-1)
       nxyz_out(3) = mxyz(3)*2**(jd*(j_out-1))

       IF(j_out /= j_lev) THEN

          DEALLOCATE(i_c,i_d,xx,h) 

          !note we allocate the third dim as 0:0 in the case of 2D
          ALLOCATE(i_c(0:nxyz_out(1),0:nxyz_out(2),0:jd*nxyz_out(3)),i_d(0:nxyz_out(1),0:nxyz_out(2),0:jd*nxyz_out(3)) )
          ALLOCATE(xx(0:MAXVAL(nxyz_out),1:dim)) 
          ALLOCATE(h(1:j_out,1:dim))
          ! ------------ define new grid coordinates ---------------------------------
          CALL set_xx (xx(0:MAXVAL(nxyz_out(1:dim)),1:dim),h(1:j_out,1:dim),nxyz_out(1:dim),MAXVAL(nxyz_out(1:dim)),j_out)
          
       END IF
       i_c =.FALSE. ; i_d = .FALSE.
       i_c_init=.FALSE.
    END IF !  IF(i_c_init) THEN


    inum=1
    idenom=1
    IF(j_out > j_in) inum=2**(j_out-j_in)
    IF(j_out < j_in) idenom=2**(j_in-j_out)

    FORALL (i=1:nwlt, ABS (coef(i)) >= leps &
         .AND. ij2j(indx(i,1),j_in) <= j_out  &
         .AND. ij2j(indx(i,2),j_in) <= j_out  &
         .AND. jd*ij2j(indx(i,3),j_in) <= j_out ) &
         i_c(indx(i,1)*inum/idenom,indx(i,2)*inum/idenom,indx(i,3)*inum/idenom) = .TRUE.
    
    
    !test
    !do i=1,nwlt
    !   IF( i_c(indx(i,1)*inum/idenom,indx(i,2)*inum/idenom,indx(i,3)*inum/idenom) ) THEN
    !	   PRINT *, indx(i,1)*inum/idenom,indx(i,2)*inum/idenom,indx(i,3)*inum/idenom , coef(i) , i, ' wrk'
    !   END IF
    ! END DO

    PRINT *,'WRK # n_sig significant  = ', COUNT( i_c )

  END SUBROUTINE significant_wlt
  !
  !************ Calculating Adjacent wavelets ***************************
  !
  ! This routine makes adjacent points true on the i_c logical mask
  ! 
  ! coef - 1D adaptive array of wlt coefficients currently active
  ! leps  - theshold
  ! nwlt - number of wavalets
  ! ij_adj(-1,0,1)   - Number of adjacent wavelets to retain in level below, current, and above 
  ! adj_type(-1,0,1) - for level below,current,above (0 - less conservative, 1 - more conservative)
  ! j_in             -
  ! j_out            -
  ! j_mn             -
  ! j_mx             -
  !
  !
  ! 
  SUBROUTINE adjacent_wlt (coef, leps, nwlt, ij_adj, adj_type, j_in, j_out, j_mn, j_mx, ie_loc,n_var_loc)
    USE precision
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: j_in, j_mn, j_mx, j_out, nwlt,ie_loc,n_var_loc
    INTEGER, DIMENSION (-1:1) :: ij_adj, adj_type
    INTEGER, DIMENSION(3) :: nxyz_out
    INTEGER :: inum,idenom,j,ij_step
    INTEGER :: i
    INTEGER :: ix_l,ix_h,iy_l,iy_h,iz_l,iz_h
    INTEGER :: lx,lxh,ly,lyh,lz,lzh
    INTEGER :: ix_shift,iy_shift,iz_shift
    INTEGER :: ix,iy,iz
    INTEGER :: jx,jy,jz
    REAL (pr), DIMENSION (1:nwlt) :: coef
    REAL (pr), INTENT (IN) :: leps
    REAL (pr):: eps_bnd
 
    eps_bnd = 0.1_pr*leps
    nxyz_out(1) = mxyz(1)*2**(j_out-1)
    nxyz_out(2) = mxyz(2)*2**(j_out-1)
    nxyz_out(3) = mxyz(3)*2**(jd*(j_out-1))

    inum=1
    idenom=1
    IF(j_out > j_in) inum=2**(j_out-j_in)
    IF(j_out < j_in) idenom=2**(j_in-j_out)
    DO i = 1, nwlt
!!$     ix = indx(i,1)*inum/idenom
!!$     iy = indx(i,2)*inum/idenom
!!$     jx = ij2j(ix, j_out)
!!$     jy = ij2j(iy, j_out)
!!$     j = MAX( jx, jy )
!!$     epsh = (leps-eps_bnd)
!!$     IF(prd(1) == 0) epsh = epsh &
!!$          *0.5_pr*(TANH(1.E2_pr*(ix/2**(j_lev-j)-n_prdct))+TANH(1.E2_pr*((nxyz(1)-ix)/2**(j_lev-j)-n_prdct)))
!!$     IF(prd(2) == 0) epsh = epsh &
!!$          *0.5_pr*(TANH(1.E2_pr*(iy/2**(j_lev-j)-n_prdct))+TANH(1.E2_pr*((nxyz(2)-iy)/2**(j_lev-j)-n_prdct)))
!!$     epsh = epsh+eps_bnd

       !IF( indx(i,1) == 15 .AND. indx(i,2) == 2 .AND. indx(i,3)== 0 ) THEN
       !PRINT *, ''
       !END IF

       IF (  ABS(coef(i)) >= leps &
            .AND. ij2j(indx(i,1),j_in) <= j_out & 
            .AND. ij2j(indx(i,2),j_in) <= j_out & 
            .AND. jd*ij2j(indx(i,3),j_in) <= j_out ) THEN  
          ix = indx(i,1)*inum/idenom
          iy = indx(i,2)*inum/idenom
          iz = indx(i,3)*inum/idenom
          jx = ij2j(ix, j_out)
          jy = ij2j(iy, j_out)
          if( DIM == 3 ) THEN
             jz = ij2j(iz, j_out)
          ELSE
             jz = 0
          END IF
          j = MAX( jx, jy )
          if( DIM == 3 )  j = MAX( j, jz )
!!$          PRINT *, 'working pt ', ix,iy,iz
!!$          PRINT *, 'j,j_mn', j,j_mn
          !PRINT *, 'levels ', jx,jy,jz
          !PRINT *, 'j_out = ', j_out ,' j = ', j

          !IF( ix == 30 .AND. iy == 4 .AND. iz == 0 ) THEN
          !PRINT *, ''
          !END IF

          ! Check if the level for this point is >= j_mn. If it is below this
          ! All the points with level <= j_mn will be in the grid anyway.
          ! 
          IF (j >= j_mn ) THEN !ADDED check for j_mn OLEG CHECK

             !****************************************************
             ! same level
             !****************************************************
             IF (j > j_mn ) THEN 
                ij_step = 2**(j_out-j) ! number of points to step to previous/next pt of same level

                ! Find previous (ix_l) and next (ix_h) point in each direction
                ix_l = MAX(0, ix-ij_adj(0)*ij_step)
                ix_h = MIN(ix+ij_adj(0)*ij_step, nxyz_out(1))
                iy_l = MAX(0, iy-ij_adj(0)*ij_step)
                iy_h = MIN(iy+ij_adj(0)*ij_step, nxyz_out(2))
                IF( DIM == 3 ) THEN
                   iz_l = MAX(0, iz-ij_adj(0)*ij_step)
                   iz_h = MIN(iz+ij_adj(0)*ij_step, nxyz_out(3))
                ELSE
                   iz_l = 0
                   iz_h = 0
                END IF

                ! account for boundary in periodic or non-periodic case
                ix_l = (1-prd(1))*ix_l + prd(1)*(ix-ij_adj(0)*ij_step)
                ix_h = (1-prd(1))*ix_h + prd(1)*(ix+ij_adj(0)*ij_step)
                iy_l = (1-prd(2))*iy_l + prd(2)*(iy-ij_adj(0)*ij_step)
                iy_h = (1-prd(2))*iy_h + prd(2)*(iy+ij_adj(0)*ij_step)
                IF( DIM == 3 ) THEN
                   iz_l = (1-prd(3))*iz_l + prd(3)*(iz-ij_adj(0)*ij_step)
                   iz_h = (1-prd(3))*iz_h + prd(3)*(iz+ij_adj(0)*ij_step)
                END IF

                If(adj_type(0) == 0) THEN ! less conservative
                   IF( jx > jy .and. jx > jz ) THEN ! psi phi phi
                      ix_l = ix
                      ix_h = ix
                   END IF
                   IF( jy > jx .and. jy > jz ) THEN ! phi psi phi
                      iy_l = iy
                      iy_h = iy
                   END IF
                   IF( jz > jx .and. jz > jy ) THEN ! phi phi psi 
                      iz_l = iz
                      iz_h = iz
                   END IF
                   ! What about psi psi phi ...???

                   DO lz = iz_l, iz_h, ij_step
                      lzh=(1-prd(3))*lz + prd(3)*MOD(lz+9*nxyz_out(3),nxyz_out(3))
                      i_c(ix,iy,lzh) = .TRUE.
                      !PRINT *, ix +   iy*64    +   lzh *64*64   ,ix,iy,lzh ,j_out                 

!!$                      write(*,'( ''add same level pt:'', 3(I4 , 1X ),''wlt fmly:'',3(I4 , 1X ) )')&
!!$                           ix,iy,lzh,&
!!$                           ij2j(ix , j_out),ij2j(iy , j_out),ij2j(lzh , j_out)
                   END DO
                   DO ly = iy_l, iy_h, ij_step
                      lyh=(1-prd(2))*ly + prd(2)*MOD(ly+9*nxyz_out(2),nxyz_out(2))
                      !PRINT *, ix +   lyh*64    +   iz *64*64   ,ix,lyh,iz ,j_out                 
                      i_c(ix,lyh,iz) = .TRUE.
!!$                      write(*,'( ''add same level pt:'', 3(I4 , 1X ),''wlt fmly:'',3(I4 , 1X ) )')&
!!$                           ix,lyh,iz,&
!!$                           ij2j(ix , j_out),ij2j(lyh , j_out),ij2j(iz , j_out)
                   END DO
                   DO lx = ix_l, ix_h, ij_step
                      lxh=(1-prd(1))*lx + prd(1)*MOD(lx+9*nxyz_out(1),nxyz_out(1))
                      !PRINT *, lxh +   iy*64    +   iz *64*64   ,lxh,iy,iz ,j_out                 
                      i_c(lxh,iy,iz) = .TRUE.
!!$                      write(*,'( ''add same level pt:'', 3(I4 , 1X ),''wlt fmly:'',3(I4 , 1X ) )')&
!!$                           lxh,iy,iz,&
!!$                           ij2j(lxh , j_out),ij2j(iy , j_out),ij2j(iz , j_out)
                   END DO
                ELSE IF(adj_type(0) == 1) THEN !more conservative
                   DO lz = iz_l, iz_h, ij_step
                      lzh=(1-prd(3))*lz + prd(3)*MOD(lz+9*nxyz_out(3),nxyz_out(3))
                      DO ly = iy_l, iy_h, ij_step
                         lyh=(1-prd(2))*ly + prd(2)*MOD(ly+9*nxyz_out(2),nxyz_out(2))
                         DO lx = ix_l, ix_h, ij_step
                            lxh=(1-prd(1))*lx + prd(1)*MOD(lx+9*nxyz_out(1),nxyz_out(1))
                            i_c(lxh,lyh,lzh) = .TRUE.
                            !PRINT *, lxh +   lyh*64    +   lzh *64*64   ,lxh,lyh,lzh ,j_out                 

!!$                            write(*,'( ''add same level pt:'', 3(I4 , 1X ),''wlt fmly:'',3(I4 , 1X ) )')&
!!$                                 lxh,lyh,lzh,&
!!$                                 ij2j(lxh , j_out),ij2j(lyh , j_out),ij2j(lzh , j_out)
                         END DO
                      END DO
                   END DO
                END IF
             END IF


             !****************************************************
             ! level below
             !****************************************************

             IF (j >= max(2,j_mn+2) ) THEN  !level below  
                ij_step = 2**(j_out-j+1)
                ix_shift= ij_step/2
                iy_shift= ij_step/2
                iz_shift= ij_step/2

                ! change i_shift depending on wvlt family
                IF( jx > jz .and. jx == jy ) iz_shift=0 ! psi psi phi
                IF( jz > jy .and. jz == jx ) iy_shift=0 ! psi phi psi
                IF( jz > jx .and. jz == jy ) ix_shift=0 ! phi psi psi
                IF( jx > jy .and. jx > jz ) THEN        ! psi phi phi
                   iy_shift=0 ; iz_shift=0 
                END IF
                IF( jy > jz .and. jy > jx ) THEN        ! phi psi phi
                   iz_shift=0 ; ix_shift=0 
                END IF
                IF( jz > jx .and. jz > jy ) THEN        ! phi phi psi
                   ix_shift=0 ; iy_shift=0 
                END IF


                ! Find previous (ix_l) and next (ix_h) point in each direction
                ix_l = MAX(0, ix+ix_shift-ij_adj(-1)*ij_step)
                ix_h = MIN(   ix-ix_shift+ij_adj(-1)*ij_step, nxyz_out(1))
                iy_l = MAX(0, iy+iy_shift-ij_adj(-1)*ij_step)
                iy_h = MIN(   iy-iy_shift+ij_adj(-1)*ij_step, nxyz_out(2))
                IF( DIM == 3 ) THEN
                   iz_l = MAX(0, iz+iz_shift-ij_adj(-1)*ij_step)
                   iz_h = MIN(   iz-iz_shift+ij_adj(-1)*ij_step, nxyz_out(3))
                ELSE
                   iz_l = 0
                   iz_h = 0
                END IF
                !write(*,'( ''1)ix_l,ix_h,iy_l,iy_h,iz_l,iz_h '', 6(I3 , 1X ) )')&
                !     ix_l,ix_h,iy_l,iy_h,iz_l,iz_h

                ! account for boundary in periodic or non-periodic case
                ix_l = (1-prd(1))*ix_l + prd(1)*(ix+ix_shift-ij_adj(-1)*ij_step)
                ix_h = (1-prd(1))*ix_h + prd(1)*(ix-ix_shift+ij_adj(-1)*ij_step)
                iy_l = (1-prd(2))*iy_l + prd(2)*(iy+iy_shift-ij_adj(-1)*ij_step)
                iy_h = (1-prd(2))*iy_h + prd(2)*(iy-iy_shift+ij_adj(-1)*ij_step)
                IF( DIM == 3 ) THEN
                   iz_l = (1-prd(3))*iz_l + prd(3)*(iz+iz_shift-ij_adj(-1)*ij_step)
                   iz_h = (1-prd(3))*iz_h + prd(3)*(iz-iz_shift+ij_adj(-1)*ij_step)
                END IF

                !write(*,'( ''2)ix_l,ix_h,iy_l,iy_h,iz_l,iz_h '', 6(I3 , 1X ) )')&
                !     ix_l,ix_h,iy_l,iy_h,iz_l,iz_h

                IF(adj_type(-1) == 0) THEN ! less conservative

                   IF( jx > jz .and. jx == jy ) THEN ! psi psi phi 
                      iz_l = iz; iz_h = iz
                   END IF
                   IF( jz > jy .and. jz == jx ) THEN ! psi phi psi
                      iy_l = iy; iy_h = iy
                   END IF
                   IF( jz > jx .and. jz == jy ) THEN ! phi psi psi
                      ix_l = ix; ix_h = ix
                   END IF
                   IF( jx > jy .and. jx > jz ) THEN  ! psi phi phi
                      iy_l = iy; iy_h = iy; iz_l = iz; iz_h = iz
                   END IF
                   IF( jy > jz .and. jy > jx ) THEN  ! phi psi phi
                      iz_l = iz; iz_h = iz; ix_l = ix; ix_h = ix;
                   END IF
                   IF( jz > jx .and. jz > jy ) THEN  ! phi phi psi
                      ix_l = ix; ix_h = ix; iy_l = iy; iy_h = iy
                   END IF

                END IF
                DO lz = iz_l, iz_h, ij_step !more conservative
                   lzh=(1-prd(3))*lz + prd(3)*MOD(lz+9*nxyz_out(3),nxyz_out(3))
                   DO ly = iy_l, iy_h, ij_step 
                      lyh=(1-prd(2))*ly + prd(2)*MOD(ly+9*nxyz_out(2),nxyz_out(2))
                      DO lx = ix_l, ix_h, ij_step
                         lxh=(1-prd(1))*lx + prd(1)*MOD(lx+9*nxyz_out(1),nxyz_out(1))
                         i_c(lxh,lyh,lzh) = .TRUE.
                         !PRINT *, lxh +   lyh*64    +   lzh *64*64   ,lxh,lyh,lzh ,j_out                 
!!$                         write(*,'( ''add level below pt:'', 3(I4 , 1X ),''wlt fmly:'',3(I4 , 1X ) )')&
!!$                              lxh,lyh,lzh,&
!!$                              ij2j(lxh , j_out),ij2j(lyh , j_out),ij2j(lzh , j_out)
                      END DO
                   END DO
                END DO
             END IF


             !****************************************************
             ! level above
             !****************************************************

             IF (j <= j_out-1) THEN               ! level above
                ij_step = 2**(j_out-j-1)

                ! Find previous (ix_l) and next (ix_h) point in each direction
                ix_l = MAX(0, ix-ij_adj(1)*ij_step)
                ix_h = MIN(   ix+ij_adj(1)*ij_step, nxyz_out(1))
                iy_l = MAX(0, iy-ij_adj(1)*ij_step)
                iy_h = MIN(   iy+ij_adj(1)*ij_step, nxyz_out(2))
                IF( DIM == 3 ) THEN
                   iz_l = MAX(0, iz-ij_adj(1)*ij_step)
                   iz_h = MIN(   iz+ij_adj(1)*ij_step, nxyz_out(3))
                ELSE
                   iz_l = 0
                   iz_h = 0
                END IF

                ! account for boundary in periodic or non-periodic case
                ix_l = (1-prd(1))*ix_l + prd(1)*(ix-ij_adj(1)*ij_step)
                ix_h = (1-prd(1))*ix_h + prd(1)*(ix+ij_adj(1)*ij_step)
                iy_l = (1-prd(2))*iy_l + prd(2)*(iy-ij_adj(1)*ij_step)
                iy_h = (1-prd(2))*iy_h + prd(2)*(iy+ij_adj(1)*ij_step)
                IF( DIM == 3 ) THEN
                   iz_l = (1-prd(3))*iz_l + prd(3)*(iz-ij_adj(1)*ij_step)
                   iz_h = (1-prd(3))*iz_h + prd(3)*(iz+ij_adj(1)*ij_step)
                END IF

                IF(adj_type(1) == 0) THEN!less conservative

                   IF( jx > jz .and. jx == jy ) THEN ! psi psi phi 
                      iz_l = iz; iz_h = iz
                   END IF
                   IF( jz > jy .and. jz == jx ) THEN ! psi phi psi
                      iy_l = iy; iy_h = iy
                   END IF
                   IF( jz > jx .and. jz == jy ) THEN ! phi psi psi
                      ix_l = ix; ix_h = ix
                   END IF
                   IF( jx > jy .and. jx > jz ) THEN  ! psi phi phi
                      iy_l = iy; iy_h = iy; iz_l = iz; iz_h = iz
                   END IF
                   IF( jy > jz .and. jy > jx ) THEN  ! phi psi phi
                      iz_l = iz; iz_h = iz; ix_l = ix; ix_h = ix;
                   END IF
                   IF( jz > jx .and. jz > jy ) THEN  ! phi phi psi
                      ix_l = ix; ix_h = ix; iy_l = iy; iy_h = iy
                   END IF


                   DO lx = ix_l, ix_h, ij_step
                      lxh=(1-prd(1))*lx + prd(1)*MOD(lx+9*nxyz_out(1),nxyz_out(1))
                      i_c(lxh,iy,iz) = .TRUE.
                      !PRINT *, lxh +   iy*64    +   iz *64*64   ,lxh,iy,iz ,j_out                 
!!$                      write(*,'( ''add above level pt:'', 3(I4 , 1X ),''wlt fmly:'',3(I4 , 1X ) )')&
!!$                           lxh,iy,iz,&
!!$                           ij2j(lxh , j_out),ij2j(iy , j_out),ij2j(iz , j_out)
                   END DO
                   DO ly = iy_l, iy_h, ij_step
                      lyh=(1-prd(2))*ly + prd(2)*MOD(ly+9*nxyz_out(2),nxyz_out(2))
                      i_c(ix,lyh,iz) = .TRUE.
                      !PRINT *, ix +   lyh*64    +   iz *64*64   ,ix,lyh,iz ,j_out                 
!!$                      write(*,'( ''add above level pt:'', 3(I4 , 1X ),''wlt fmly:'',3(I4 , 1X ) )')&
!!$                           ix,lyh,iz,&
!!$                           ij2j(ix , j_out),ij2j(lyh , j_out),ij2j(iz , j_out)
                   END DO
                   DO lz = iz_l, iz_h, ij_step
                      lzh=(1-prd(3))*lz + prd(3)*MOD(lz+9*nxyz_out(3),nxyz_out(3))
                      i_c(ix,iy,lzh) = .TRUE.
                      !PRINT *, ix +   iy*64    +   lzh *64*64   ,ix,iy,lzh ,j_out                 
!!$                      write(*,'( ''add above level pt:'', 3(I4 , 1X ),''wlt fmly:'',3(I4 , 1X ) )')&
!!$                           ix,iy,lzh,&
!!$                           ij2j(ix , j_out),ij2j(iy , j_out),ij2j(lzh , j_out)
                   END DO
                ELSE IF (adj_type(1) == 1) THEN !more conservative
                   DO lz = iz_l, iz_h, ij_step
                      lzh=(1-prd(3))*lz + prd(3)*MOD(lz+9*nxyz_out(3),nxyz_out(3))
                      DO ly = iy_l, iy_h, ij_step
                         lyh=(1-prd(2))*ly + prd(2)*MOD(ly+9*nxyz_out(2),nxyz_out(2))
                         DO lx = ix_l, ix_h, ij_step
                            lxh=(1-prd(1))*lx + prd(1)*MOD(lx+9*nxyz_out(1),nxyz_out(1))
                            i_c(lxh,lyh,lzh) = .TRUE.
                            !PRINT *, lxh +   lyh*64    +   lzh *64*64   ,lxh,lyh,lzh ,j_out                 
!!$                            write(*,'( ''add level above pt:'', 3(I4 , 1X ),''wlt fmly:'',3(I4 , 1X ) )')&
!!$                                 lxh,lyh,lzh,&
!!$                                 ij2j(lxh , j_out),ij2j(lyh , j_out),ij2j(lzh , j_out)
                         END DO
                      END DO
                   END DO
                END IF
             END IF


          END IF !IF (j >= j_mn )
       END IF !IF (  ABS(coef(i)) >= leps & ...
    END DO
    PRINT *,'sig+adj =', COUNT( i_c)
    !
    !---------- include all points  j<= j_mn on call for last component ------------------
    !

    PRINT *, 'Adjacent_wlt before include j<= j_mn , COUNT(i_c) = ', COUNT(i_c) , j_out, j_mn
    PRINT *, 'ie_loc/n_var_loc', ie_loc, n_var_loc

    DO lz = 0, nxyz_out(3) - prd(3), 2**(j_out-j_mn)
       DO ly = 0, nxyz_out(2) - prd(2), 2**(j_out-j_mn)
          DO lx = 0, nxyz_out(1) - prd(1), 2**(j_out-j_mn)
             i_c(lx,ly,lz) = .TRUE.
          END DO
       END DO
    END DO
    PRINT *,'leaving adjacent_wlt() COUNT(i_c) j_in, j_out =', COUNT(i_c), j_in, j_out

  END SUBROUTINE adjacent_wlt
  !
  !************ Calculating bnd_zone wavelets ***************************
  !
  SUBROUTINE bnd_zone(i_mask,  nxyz_out, j_out)
    USE precision
    USE debug_vars     ! experimental_feature_stop
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: j_out
    INTEGER, DIMENSION(1:3), INTENT (IN) :: nxyz_out
    LOGICAL, DIMENSION (0:nxyz_out(1),0:nxyz_out(2),0:jd*nxyz_out(3)), INTENT (INOUT) :: i_mask
    INTEGER :: i, j 
    !----------------- BNDzone variables
    INTEGER :: m,  idim, step_j, num
    INTEGER :: ii
    INTEGER :: n_BNDzone, s(0:1), i_l(dim), i_h(dim), face(1:dim), wlt(1:dim), ZERO(1:dim)
    INTEGER, DIMENSION(3) :: ixyz, ixyz1                     !this is hardwired, only for DB_wrk
    INTEGER, DIMENSION(0:dim) :: i_p, i_p_cube
    LOGICAL :: check, check1(1:dim)
    LOGICAL, DIMENSION (0:nxyz_out(1),0:nxyz_out(2),0:jd*nxyz_out(3)) :: i_mask_h
    
    
    ! that feature is experimental, warn the user and terminate
    CALL experimental_feature_stop('bnd_zone, wlt_trns_mod, wavelet_3d_wrk.f90 ')
    
    
    ZERO = 0
    num=0

    i_mask_h = i_mask
    !n_BNDzone = MAX(2*n_diff,MAXVAL(n_prdct+n_updt))
    n_BNDzone = 2*MAX(MAXVAL(n_prdct),n_diff)+1 !Oleg: this seem to work fine with minimum overhead.
    i_p(0) = 1
    i_p_cube(0) = 1
    IF(dim == 2 ) ixyz(3) = jd
    IF(dim == 2 ) ixyz1(3) = jd
    DO j = j_out, 1, -1
       step_j = 2**(j_out - j)
       DO i=1,dim
          i_p(i) = i_p(i-1)*(nxyz_out(i)/step_j+1-prd(i))
       END DO
       DO ii = 1, i_p(dim)
          ixyz(1:dim) = INT(MOD(ii-1,i_p(1:dim))/i_p(0:dim-1))*step_j 
          face = (-1 + 2*INT(ixyz(1:dim)/nxyz_out(1:dim))+ MIN(1,MOD(ixyz(1:dim),nxyz_out(1:dim)))) * (1 - prd(1:dim))
          wlt = MIN(MIN(1,j-1),MOD(ixyz(1:dim),2**(j_out-j+1)))
          IF(i_mask(ixyz(1),ixyz(2),ixyz(3)) .AND. ixyz2j(ixyz(1:dim),j_out) == j  ) THEN 
             check1 = .FALSE. !.TRUE. if point is in bnd_zone, .FALSE. otherwise
             DO idim = 1, dim ! idim - direction
                s(0) = ( 1 - MIN(INT(ixyz(idim)/(step_j*(n_BNDzone+1))),1) ) * ( 1 - prd(idim) )                    ! low  BNDzone 
                s(1) = ( 1 - MIN(INT((nxyz_out(idim) - ixyz(idim)) /(step_j*(n_BNDzone+1))),1) ) * (1 - prd(idim) ) ! high BNDzone
                i_l(idim) = MIN( 0*s(0)+ixyz(idim)*(1-s(0)), MAX(0,(nxyz_out(idim)-n_BNDzone*step_j))*s(1)+ixyz(idim)*(1-s(1)) )
                i_h(idim) = MAX( MIN(step_j*n_BNDzone,nxyz_out(idim))*s(0)+ixyz(idim)*(1-s(0)), nxyz_out(idim)*s(1)+ixyz(idim)*(1-s(1)) )
                check1(idim) = ANY( s(0:1) /= 0)
             END DO
             IF( ANY(check1(1:dim)) ) THEN
                WHERE (.NOT.check1(1:dim) ) 
!!$                 i_l(1:dim) = MIN( i_l(1:dim),ixyz(1:dim)-step_j )
!!$                 i_h(1:dim) = MAX( i_h(1:dim),ixyz(1:dim)+step_j )
                   i_l(1:dim) = MIN( i_l(1:dim),0 )
                   i_h(1:dim) = MAX( i_h(1:dim),nxyz_out(1:dim)-prd(1:dim) )
                END WHERE
                i_l(1:dim)=(1-prd(1:dim))*MAX(ZERO(1:dim),i_l(1:dim)) + prd(1:dim)*i_l(1:dim)
                i_h(1:dim)=(1-prd(1:dim))*MIN(nxyz_out(1:dim),i_h(1:dim)) + prd(1:dim)*i_h(1:dim)
                i_p_cube(1:dim) = (i_h-i_l)/step_j + 1
                DO idim =1, dim
                   i_p_cube(idim) = i_p_cube(idim-1)*i_p_cube(idim)
                END DO
                check = .FALSE.
                DO m = 1,i_p_cube(dim)
                   ixyz1(1:dim) = i_l + INT(MOD(m-1,i_p_cube(1:dim))/i_p_cube(0:dim-1))*step_j
                   ixyz1(1:dim) = (1-prd(1:dim))*ixyz1(1:dim)+prd(1:dim)*MOD(ixyz1(1:dim)+9*nxyz_out(1:dim),nxyz_out(1:dim))
                   IF(i_mask(ixyz1(1),ixyz1(2),ixyz1(3)) .AND. ixyz2j(ixyz1(1:dim),j_out) == j ) check = .TRUE. 
                   !IF(i_mask(ixyz1(1),ixyz1(2),ixyz1(3)) .AND. ANY(ixyz(1:dim) /= ixyz1(1:dim)) ) check = .TRUE. 
                END DO
                IF(check) THEN
                   DO m = 1,i_p_cube(dim)
                      ixyz1(1:dim) = i_l + INT(MOD(m-1,i_p_cube(1:dim))/i_p_cube(0:dim-1))*step_j
                      ixyz1(1:dim) = (1-prd(1:dim))*ixyz1(1:dim)+prd(1:dim)*MOD(ixyz1(1:dim)+9*nxyz_out(1:dim),nxyz_out(1:dim))
                      i_mask(ixyz1(1),ixyz1(2),ixyz1(3)) = .TRUE.
                      num = num + 1
                   END DO
                END IF
             END IF
          END IF
       END DO
    END DO
!!$  !-------------------- go through faces one more time & coarsen the grid
!!$  DO k =1, 2
!!$     DO j = j_out, 1, -1
!!$        step_j = 2**(j_out - j)
!!$        DO idim = 1, dim ! go through faces perpendicular to idim direction
!!$           IF( prd(idim) == 0) THEN !only if not periodic in idim direction
!!$              step(1:dim) = step_j
!!$              step(idim) = nxyz_out(idim)
!!$              i_p(1:dim) = (nxyz_out(1:dim)/step(1:dim)+1-prd(1:dim))
!!$              DO i=1,dim
!!$                 i_p(i) = i_p(i-1)*(nxyz_out(i)/step(i)+1-prd(i))
!!$              END DO
!!$              DO ii = 1, i_p(dim)
!!$                 ixyz(1:dim) = INT(MOD(ii-1,i_p(1:dim))/i_p(0:dim-1))*step(1:dim) 
!!$                 IF(i_mask(ixyz(1),ixyz(2),ixyz(3)) ) THEN 
!!$                    face = (-1 + 2*INT(ixyz(1:dim)/nxyz_out(1:dim))+ MIN(1,MOD(ixyz(1:dim),nxyz_out(1:dim)))) * (1 - prd(1:dim))
!!$                    wlt = MIN(MIN(1,j-1),MOD(ixyz(1:dim),2**(j_out-j+1)))
!!$                    check = .FALSE.
!!$                    IF( ixyz2j(ixyz(1:dim),j_out) == j ) THEN
!!$                       check = .TRUE.
!!$                    ELSE
!!$                       i_l(1:dim) = ixyz(1:dim)-step_j
!!$                       i_h(1:dim) = ixyz(1:dim)+step_j
!!$                       i_l(idim) = MAX(0,MIN(ixyz(idim), ixyz(idim)-face(idim)*n_BNDzone*step_j))
!!$                       i_h(idim) = MIN(nxyz_out(idim),MAX(ixyz(idim), ixyz(idim)-face(idim)*n_BNDzone*step_j))
!!$                       i_l(1:dim)=(1-prd(1:dim))*MAX(ZERO(1:dim),i_l(1:dim)) + prd(1:dim)*i_l(1:dim)
!!$                       i_h(1:dim)=(1-prd(1:dim))*MIN(nxyz_out(1:dim),i_h(1:dim)) + prd(1:dim)*i_h(1:dim)
!!$                       i_p_cube(1:dim) = (i_h-i_l)/step_j + 1
!!$                       DO m =1, dim
!!$                          i_p_cube(m) = i_p_cube(m-1)*i_p_cube(m)
!!$                       END DO
!!$                       DO m = 1,i_p_cube(dim)
!!$                          ixyz1(1:dim) = i_l + INT(MOD(m-1,i_p_cube(1:dim))/i_p_cube(0:dim-1))*step_j
!!$                          ixyz1(1:dim) = (1-prd(1:dim))*ixyz1(1:dim)+prd(1:dim)*MOD(ixyz1(1:dim)+9*nxyz_out(1:dim),nxyz_out(1:dim))
!!$                          IF(i_mask(ixyz1(1),ixyz1(2),ixyz1(3)) .AND. ANY(ixyz1(1:dim) /= ixyz(1:dim)) .AND. ixyz2j(ixyz1(1:dim),j_out) == j ) check = .TRUE. 
!!$                       END DO
!!$                    END IF
!!$                    IF(check) THEN
!!$                       i_l(idim) = MAX(0,MIN(ixyz(idim), ixyz(idim)-face(idim)*n_BNDzone*step_j))
!!$                       i_h(idim) = MIN(nxyz_out(idim),MAX(ixyz(idim), ixyz(idim)-face(idim)*n_BNDzone*step_j))
!!$                       i_p_cube(1:dim) = (i_h-i_l)/step_j + 1
!!$                       DO m =1, dim
!!$                          i_p_cube(m) = i_p_cube(m-1)*i_p_cube(m)
!!$                       END DO
!!$                       DO m = 1,i_p_cube(dim)
!!$                          ixyz1(1:dim) = i_l + INT(MOD(m-1,i_p_cube(1:dim))/i_p_cube(0:dim-1))*step_j
!!$                          ixyz1(1:dim) = (1-prd(1:dim))*ixyz1(1:dim)+prd(1:dim)*MOD(ixyz1(1:dim)+9*nxyz_out(1:dim),nxyz_out(1:dim))
!!$                          i_mask(ixyz1(1),ixyz1(2),ixyz1(3)) = .TRUE.
!!$                       END DO
!!$                    END IF
!!$                 END IF
!!$              END DO
!!$           END IF
!!$        END DO
!!$     END DO
!!$  END DO
!!$  
!!$  DO j = j_out, 1, -1
!!$     step_j = 2**(j_out - j)
!!$     DO idim = 1, dim ! go through faces perpendicular to idim direction
!!$        IF( prd(idim) == 0) THEN !only if not periodic in idim direction
!!$           step(1:dim) = step_j
!!$           step(idim) = nxyz_out(idim)
!!$           i_p(1:dim) = (nxyz_out(1:dim)/step(1:dim)+1-prd(1:dim))
!!$           DO i=1,dim
!!$              i_p(i) = i_p(i-1)*(nxyz_out(i)/step(i)+1-prd(i))
!!$           END DO
!!$           check = .FALSE.
!!$           DO ii = 1, i_p(dim)
!!$              ixyz(1:dim) = INT(MOD(ii-1,i_p(1:dim))/i_p(0:dim-1))*step(1:dim) 
!!$              face = (-1 + 2*INT(ixyz(1:dim)/nxyz_out(1:dim))+ MIN(1,MOD(ixyz(1:dim),nxyz_out(1:dim)))) * (1 - prd(1:dim))
!!$              IF( ixyz2j(ixyz(1:dim),j_out) == j ) check = .TRUE.
!!$           END DO
!!$           IF(check) THEN
!!$              i_l(1:dim) = 0
!!$              i_h(1:dim) = nxyz_out(1:dim) - prd(1:dim)
!!$              i_l(idim) = MAX(0,MIN(ixyz(idim), ixyz(idim)-face(idim)*n_BNDzone*step_j))
!!$              i_h(idim) = MIN(nxyz_out(idim),MAX(ixyz(idim), ixyz(idim)-face(idim)*n_BNDzone*step_j))
!!$              i_p_cube(1:dim) = (i_h-i_l)/step_j + 1
!!$              DO m =1, dim
!!$                 i_p_cube(m) = i_p_cube(m-1)*i_p_cube(m)
!!$              END DO
!!$              DO m = 1,i_p_cube(dim)
!!$                 ixyz1(1:dim) = i_l + INT(MOD(m-1,i_p_cube(1:dim))/i_p_cube(0:dim-1))*step_j
!!$                 !ixyz1(1:dim) = (1-prd(1:dim))*ixyz1(1:dim)+prd(1:dim)*MOD(ixyz1(1:dim)+9*nxyz_out(1:dim),nxyz_out(1:dim))
!!$                 i_mask(ixyz1(1),ixyz1(2),ixyz1(3)) = .TRUE.
!!$              END DO
!!$           END IF
!!$        END IF
!!$     END DO
!!$  END DO

    PRINT *,'leaving bnd_zone() COUNT(i_c)=', COUNT(i_mask),'j_out =', j_out
    PRINT *,'in bnd_zone: number of nodes added =',num

  END SUBROUTINE bnd_zone

  !************ Calculating zone wavelets ***************************
  !
  ! Zone is defined by input file parameter coord_zone_min/max
  ! outside of this zone all wavelets up to the parameter j_zone (called with argument j_mn)
  ! are retained, (nothing higher than j_zone).
  !
  !
  SUBROUTINE zone_wlt (i_mask,  nxyz_out, j_out, j_zone)
    USE precision
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: j_out, j_zone
    INTEGER, DIMENSION(1:3), INTENT (IN) :: nxyz_out
    LOGICAL, DIMENSION (0:nxyz_out(1),0:nxyz_out(2),0:jd*nxyz_out(3)), INTENT (INOUT) :: i_mask
    INTEGER :: lx, ly, lz, &
          num1,    &          ! total number of points tested
         num2,    &          ! all points j <= j_zone
         num1p               ! ajacent points set
    LOGICAL :: check

    num1 = 0
    num2 = 0
    num1p = 0
    !---------- include all points inside of the zone ------------------
    !Oleg, resolution is fixed outside of the zone, so I changed it to be right
    !it shuould be .AND. instead of .OR.
    DO lz = 0, nxyz_out(3) - prd(3)
       DO ly = 0, nxyz_out(2) - prd(2)
          DO lx = 0, nxyz_out(1) - prd(1)
             num1 = num1 + 1
             IF( dim==3) THEN
                i_mask(lx,ly,lz) = i_mask(lx,ly,lz) .AND. &
                     (xyzzone(1,1)<=xx(lx,1).AND.xx(lx,1)<=xyzzone(2,1).AND.&
                     xyzzone(1,2)<=xx(ly,2).AND.xx(ly,2)<=xyzzone(2,2).AND.&
                     xyzzone(1,3)<=xx(lz,3).AND.xx(lz,3)<=xyzzone(2,3))
                IF (i_mask(lx,ly,lz)) num1p = num1p + 1
             ELSE
                i_mask(lx,ly,lz) = i_mask(lx,ly,lz) .AND. &
                     (xyzzone(1,1)<=xx(lx,1).AND.xx(lx,1)<=xyzzone(2,1).AND.&
                     xyzzone(1,2)<=xx(ly,2).AND.xx(ly,2)<=xyzzone(2,2))
                IF (i_mask(lx,ly,lz)) num1p = num1p + 1
             END IF
          END DO
       END DO
    END DO

    ! 
    !---------- include all points  j <= j_zone ------------------
    DO lz = 0, nxyz_out(3) - prd(3), MAX(1,2**(j_out-j_zone))
       DO ly = 0, nxyz_out(2) - prd(2), MAX(1,2**(j_out-j_zone))
          DO lx = 0, nxyz_out(1) - prd(1), MAX(1,2**(j_out-j_zone))
             IF( dim==3) THEN
                check = &
                     (xyzzone(1,1)<=xx(lx,1).AND.xx(lx,1)<=xyzzone(2,1).AND.&
                     xyzzone(1,2)<=xx(ly,2).AND.xx(ly,2)<=xyzzone(2,2).AND.&
                     xyzzone(1,3)<=xx(lz,3).AND.xx(lz,3)<=xyzzone(2,3))
             ELSE
                check = &
                     (xyzzone(1,1)<=xx(lx,1).AND.xx(lx,1)<=xyzzone(2,1).AND.&
                     xyzzone(1,2)<=xx(ly,2).AND.xx(ly,2)<=xyzzone(2,2))
             END IF
             IF(.NOT.check) THEN
               i_mask(lx,ly,lz) = .TRUE.
               num2 = num2 + 1
             END IF
          END DO
       END DO
    END DO

    PRINT *, 'ajacent points set            =', num1p
    PRINT *, 'all points j <= j_zone        =', num2
    PRINT *, 'total number of points tested =', num1
    PRINT *, 'in zone_wlt COUNT(i_c)=',COUNT(i_c)

  END SUBROUTINE zone_wlt
  !
  !************ Adding additional nodes ***************************
  !
  SUBROUTINE add_nodes (i_mask,  nxyz_out, j_out)
    USE precision
    !  USE user_case
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: j_out
    INTEGER, DIMENSION(1:3), INTENT (IN) :: nxyz_out
    LOGICAL, DIMENSION (0:nxyz_out(1),0:nxyz_out(2),0:jd*nxyz_out(3)), INTENT (INOUT) :: i_mask
    INTEGER :: i

    IF(additional_nodes_active) THEN 
       CALL add_additional_nodes ( j_out )
       !---------- include all points specified by user ------------------
       IF( n_additional_nodes > 0 .AND. ALLOCATED(ixyz_additional_nodes) ) THEN
          IF( dim==3) THEN
             DO i = 1, n_additional_nodes
                i_mask(ixyz_additional_nodes(1,i),ixyz_additional_nodes(2,i),ixyz_additional_nodes(3,i)) = .TRUE.
             END DO
          ELSE IF( dim == 2) THEN
             DO i = 1, n_additional_nodes
                i_mask(ixyz_additional_nodes(1,i),ixyz_additional_nodes(2,i),0) = .TRUE.
             END DO
          ELSE
             PRINT *, 'ERROR: regime is unspecified'
             STOP
          END IF
       END IF
    END IF

    !print *, 'in additional_nodes COUNT(i_c) ',COUNT(i_mask)

  END SUBROUTINE add_nodes
  !
  !************ Performing the Reconstruction Check ***************************
  !
  SUBROUTINE reconstr_check (i_mask, wlt_fmly, trnsf_type, nxyz_out, j_out)
    USE precision
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: wlt_fmly, trnsf_type, j_out
    INTEGER, DIMENSION(1:3), INTENT (IN) :: nxyz_out

    LOGICAL, DIMENSION (0:nxyz_out(1),0:nxyz_out(2),0:jd*nxyz_out(3)), INTENT (INOUT) :: i_mask
    INTEGER :: i, j, idim
    INTEGER , DIMENSION(3) ::  jxyz, ixyz, ixyzp
    INTEGER ::  ix,ix_l,ix_h,ix_oe,ixpm
    INTEGER ::  iy,iy_oe
    INTEGER ::  iz, iz_oe
    INTEGER :: ifmly, wlt_type, intrnl_shft
    INTEGER :: dum_vec(1:dim), i_p(0:dim), wlt_type_fmly(dim_tmp,0:2**dim-1)
    INTEGER, DIMENSION(dim) :: face, intrnl_trnsf

    wlt_type_fmly = 0
    i_p(0) = 1
    DO i=1,dim
       i_p(i) = i_p(i-1)*2
    END DO
    DO wlt_type = 0,2**dim-1
       wlt_type_fmly(1:dim,wlt_type) = INT(MOD(wlt_type,i_p(1:dim))/i_p(0:dim-1)) !set up wlt_type family, not ordered yet
    END DO
    !---- ordering for wlt_type family, 1 - odd location, 0 - even location 
    !ordering  wlt_type_fmly starting from edges, then faces, then cell centers.
    j = -1
    DO i = 0,dim
       DO wlt_type = 0,2**dim-1
          IF(SUM(wlt_type_fmly(1:dim,wlt_type)) == i) THEN
             j = j + 1 
             dum_vec(1:dim) = wlt_type_fmly(1:dim,j)
             wlt_type_fmly(1:dim,j) = wlt_type_fmly(1:dim,wlt_type)
             wlt_type_fmly(1:dim,wlt_type) = dum_vec(1:dim)
          END IF
       END DO
    END DO

!!$  !OLD Dan's hardwired way of doing it.
!!$  !---- ordering for wavelet family, 1 - odd location, 0 - even location 
!!$  fmly = RESHAPE(& 
!!$       (/1,1,1,& ! psi psi psi (all wlts or odd odd odd)
!!$       0,1,1,& ! phi psi psi (scaling func, wlt, wlt  or even odd odd)
!!$       1,0,1,& ! psi phi psi
!!$       1,1,0,& ! psi psi phi
!!$       0,0,1,& ! phi phi psi
!!$       0,1,0,& ! phi psi phi
!!$       1,0,0/)& ! psi phi phi
!!$       ,(/3,7/))

    DO j = j_out-1, 1, -1
       !new
       !loop though 2**dim-1 families of wlts 
       DO ifmly = 2**dim-1,1,-1
          ! add one to jxyz for each psi wlt func
          DO idim = 1,dim
             jxyz(idim) = j + (1-wlt_type_fmly(idim,ifmly))
          END DO
          !     DO ix = 0, mxyz(1)*2**(jxyz(1)-1)-prd(1)
          !        ix_even = ix*2**(j_out-jxyz(1))
          !     DO iy = 1, mxyz(2)*2**(jxyz(2)-1)
          !        iy_odd = (2*iy-1)*2**(j_out-1-jxyz(2))

          IF (jd.EQ.0) THEN
             xuj1=0; xuj2=0
          ELSE
             xuj1=wlt_type_fmly(3,ifmly);  xuj2=(mxyz(3)*2**(jxyz(3)-1) - prd(3)*(1-wlt_type_fmly(3,ifmly)))
          END IF
          DO iz = xuj1, xuj2
             iz_oe = 0
             IF (jd.NE.0) &
                  iz_oe = jd*((iz + wlt_type_fmly(3,ifmly)*iz- wlt_type_fmly(3,ifmly))*2**(j_out-wlt_type_fmly(3,ifmly)-jxyz(3)) )
             DO iy = wlt_type_fmly(2,ifmly), mxyz(2)*2**(jxyz(2)-1) - prd(2)*(1-wlt_type_fmly(2,ifmly))
                iy_oe = (iy + wlt_type_fmly(2,ifmly)*iy- wlt_type_fmly(2,ifmly))*2**(j_out-wlt_type_fmly(2,ifmly)-jxyz(2))
                DO ix = wlt_type_fmly(1,ifmly), mxyz(1)*2**(jxyz(1)-1) - prd(1)*(1-wlt_type_fmly(1,ifmly))
                   ix_oe = (ix + wlt_type_fmly(1,ifmly)*ix- wlt_type_fmly(1,ifmly))*2**(j_out-wlt_type_fmly(1,ifmly)-jxyz(1))
                   ixyz = (/ix,iy,iz/)
                   ixyzp = (/ix_oe,iy_oe,iz_oe/)

                   IF (i_mask(ix_oe,iy_oe,iz_oe)) THEN
                      DO idim = 1,dim
                         IF( wlt_type_fmly(idim,ifmly) == 1 ) THEN !odd or psi
                            face = (-1 + 2*INT(ixyzp(1:dim)/nxyz_out(1:dim))+ MIN(1,MOD(ixyzp(1:dim),nxyz_out(1:dim)))) * (1 - prd(1:dim))
                            intrnl_trnsf = ABS(face)
                            intrnl_trnsf(idim) = 0 ! no internal transform along faces and edges
                            intrnl_shft=(1-MAXVAL(intrnl_trnsf))*trnsf_type ! to use instead of ibc
                            ix_l   = -MIN(ixyz(idim)-intrnl_shft,n_prdct(wlt_fmly)) !lower bndry for this dim 
                            ix_h   = ix_l+2*n_prdct(wlt_fmly)-1
                            ix_h   = MIN(ix_h,mxyz(idim)*2**(jxyz(idim)-1)-ixyz(idim)-intrnl_shft)!upper bndry for this dim
                            ix_l   = ix_h-2*n_prdct(wlt_fmly)+1
                            ix_l = &
                                 (1-prd(idim))*MAX(ix_l,-ixyz(idim)+intrnl_shft) + prd(idim)*(-n_prdct(wlt_fmly))
                            ix_h = &
                                 (1-prd(idim))*MIN(ix_h,mxyz(idim)*2**(jxyz(idim)-1)- &
                                 ixyz(idim)-intrnl_shft) + prd(idim)*(n_prdct(wlt_fmly) - 1)

                            IF  (ix_l+ix_h+1 < 0 .AND. ix_h >= 0 .AND. prd(idim) ==0 ) THEN      ! the right boundary
                               ix_l = MAX(ix_l,-(ix_h+1)-n_assym_prdct(wlt_fmly,trnsf_type),-ixyz(idim)+intrnl_shft)
                            ELSE IF  (ix_l+ix_h+1 < 0 .AND. ix_h == -1 .AND. prd(idim) ==0 ) THEN!right on the right bndry
                               ix_l = MAX(ix_l,-1-n_assym_prdct_bnd(wlt_fmly,trnsf_type),-ixyz(idim)+intrnl_shft)

                            ELSE IF  (ix_l+ix_h+1 > 0 .AND. ix_l < 0 .AND. prd(idim) ==0 ) THEN  !  the left boundary
                               ix_h = MIN(ix_h,-ix_l-1+n_assym_prdct(wlt_fmly,trnsf_type),mxyz(idim)*2**(jxyz(idim)-1)-ixyz(idim)-intrnl_shft)
                            ELSE IF  (ix_l+ix_h+1 > 0 .AND. ix_l == 0 .AND. prd(idim) ==0 ) THEN !right on the left bndry
                               ix_h = MIN(ix_h,n_assym_prdct_bnd(wlt_fmly,trnsf_type),mxyz(idim)*2**(jxyz(idim)-1)-ixyz(idim)-intrnl_shft)   
                            END IF
                            ixyzp = (/ix_oe,iy_oe,iz_oe/)
                            DO ixpm = ix_l, ix_h
                               ixyzp(idim) = (ixyz(idim)+ixpm)*2**(j_out-jxyz(idim))
                               ixyzp(idim) = (1-prd(idim))*ixyzp(idim)+prd(idim)* &
                                    MOD(ixyzp(idim)+9*nxyz_out(idim),nxyz_out(idim))
                               i_mask(ixyzp(1),ixyzp(2),ixyzp(3)) = .TRUE.
                               !PRINT *,'add ', ixyzp
                               !IF( ixyzp(1) == 20 .AND. ixyzp(2) == 28 .AND. ixyzp(3) == 6 ) THEN
                               !PRINT *,'recon add 20 28 6 '
                               !END IF
                            END DO
                         ENDIF
                      END DO
                   END IF
                END DO
             END DO
          END DO
       END DO


    END DO


  END SUBROUTINE reconstr_check


  !
  !* INDX - maps 1..NWLT active grid points to nonadaptive grid 
  !*         at J_LEV level of resolution
  !
  ! 

  SUBROUTINE indices (nwlt_old, nwlt, nwltj_old, nwltj, new_grid, j_lev_old, j_lev_new, j_mn, j_mx, ireg)
    USE precision
    USE util_vars
    USE share_consts
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: j_mn, j_mx, ireg
    INTEGER, INTENT (INOUT) :: j_lev_new, j_lev_old, nwlt, nwlt_old, nwltj, nwltj_old
    LOGICAL, INTENT (INOUT) :: new_grid
    LOGICAL :: lchk
    INTEGER :: i,ii,ih,j,k,l,meth,nD,wlt_fmly,trnsf_type
    INTEGER :: ix,ix_l,ix_h,ixpm,ix_even,ix_odd
    INTEGER :: iy,iy_l,iy_h
    INTEGER :: iz,iz_l,iz_h
    INTEGER :: jxyz_max, ixyz2
    INTEGER, DIMENSION(3) :: jxyz, ixyz

    INTEGER :: idim
     !------------ arrays used for indx_DB --------------------
    INTEGER :: j_df, j_lv, wlt_type, face_type, ishift, ibndr
    INTEGER :: face_dim, face_val
    INTEGER, DIMENSION(dim) :: wlt, face, ivec
    INTEGER, DIMENSION(0:dim) :: i_p, i_p_face, i_p_wlt
    LOGICAL, DIMENSION(:,:), ALLOCATABLE :: face_assigned
    !---------------------------------------------------------
    INTEGER j_full_tmp
    INTEGER :: i1(1)


    IF (ireg == -1 ) THEN !Restart version

       ! put into create_indices_from_indx_DB()
       ! code below intil end comment D.G.

       !
       !Creating lv_intrnl, lv_bnd, & indx from indx_DB
       i_p(0) = 1
       i_p_face(0) = 1
       DO i=1,dim
          i_p(i) = i_p(i-1)*(1+nxyz(i))
          i_p_face(i) = i_p_face(i-1)*3
       END DO
       IF(ALLOCATED(lv_intrnl)) DEALLOCATE(lv_intrnl)
       ALLOCATE(lv_intrnl(0:j_lev))
       face = 0 
       face_type = SUM((face+1)*i_p_face(0:dim-1))
       lv_intrnl(0) = 0
       DO j = 1, j_lev
          lv_intrnl(j) = lv_intrnl(j-1)
          DO wlt_type = MIN(j-1,1),2**dim-1
             DO j_df = j, j_lev
                lv_intrnl(j) = lv_intrnl(j) + indx_DB(j_df,wlt_type,face_type,j)%length  
             END DO
          END DO
       END DO
       nwlt = lv_intrnl(j_lev)
       IF(ALLOCATED(lv_bnd)) DEALLOCATE(lv_bnd) !changed
       ALLOCATE(lv_bnd(0:3,1:2*dim,0:j_lev)) !changed
       IF(ALLOCATED(face_assigned)) DEALLOCATE(face_assigned)
       ALLOCATE(face_assigned(0:3**dim-1,1:j_mx))
       face_assigned = .FALSE.
       face_assigned((3**dim-1)/2,:) = .TRUE. !internal points
       IF( MINVAL(prd(1:dim)) == 0 ) THEN !non-periodic at least in one direction
          DO i=1,2*dim
             face_dim = INT((ibnd(i)-1)/2)+1
             face_val = (2*MOD(ibnd(i)-1,2)-1)*(1-prd(face_dim))

             DO j=1,j_lev  !--- the following do-loop changed to set up indicies for multilevel calculations.
                lv_bnd(0,i,j) = nwlt+1 
                lv_bnd(1,i,j) = nwlt   
                DO face_type = 0, 3**dim - 1
                   face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
                   IF( face(face_dim) == face_val .AND. .NOT.face_assigned(face_type,j) ) THEN
                      face_assigned(face_type,j) = .TRUE.
                      DO wlt_type = MIN(j-1,1),2**dim-1
                         DO j_df = j, j_lev
                            nwlt = nwlt + indx_DB(j_df,wlt_type,face_type,j)%length
                            lv_bnd(1,i,j) = lv_bnd(1,i,j) + indx_DB(j_df,wlt_type,face_type,j)%length
                         END DO
                      END DO
                   END IF
                END DO
             END DO
          END DO
       END IF
       DEALLOCATE(face_assigned)
       ! update the lv_bnd(2:3, , ) 
       ! The 2:3 indices are for mappign the bnd points to the end of a lower level 
       ! in the flat u array. Used in diff_aux()
       DO j=1,j_lev
          i = 1
          lv_bnd(2,i,j) = lv_intrnl(j) + 1
          lv_bnd(3,i,j) = lv_bnd(2,i,j) + lv_bnd(1,i,j)-lv_bnd(0,i,1)
          DO i=2,2*dim
             lv_bnd(2,i,j) = lv_bnd(3,i-1,j) + 1
             lv_bnd(3,i,j) = lv_bnd(2,i,j) + lv_bnd(1,i,j)-lv_bnd(0,i,1)
          END DO
       END DO
       !end put into create_indices_from_indx_DB()
       IF(ALLOCATED(indx)) DEALLOCATE(indx)
       ALLOCATE(indx(1:nwlt,1:3)); indx = 0
       DO j = 1, j_lev
          DO wlt_type = MIN(j-1,1),2**dim-1
             DO face_type = 0, 3**dim - 1
                DO j_df = j, j_lev
                   DO k = 1, indx_DB(j_df,wlt_type,face_type,j)%length  
                      i = indx_DB(j_df,wlt_type,face_type,j)%p(k)%i
                      indx(i,1:dim) = INT(MOD(indx_DB(j_df,wlt_type,face_type,j)%p(k)%ixyz-1,i_p(1:dim))/i_p(0:dim-1))
                   END DO
                END DO
             END DO
          END DO
       END DO
       nwltj = 2*nwlt

       IF (ALLOCATED(Nwlt_lev)) DEALLOCATE (Nwlt_lev)
       ALLOCATE(Nwlt_lev(1:j_mx,0:1)) ! allocates only once for all levels up to j_mx
       !

       IF (ALLOCATED(nlvj)) DEALLOCATE (nlvj)
       IF (ALLOCATED(nlvjD)) DEALLOCATE (nlvjD)
       IF (ALLOCATED(lvj)) DEALLOCATE (lvj)
       ALLOCATE(nlvj(1:4,1:nwltj),nlvjD(1:4*dim,1:nwltj,0:n_df),lvj(0:j_lev))
       IF (ALLOCATED(lvxyz_odd)) DEALLOCATE (lvxyz_odd)
       IF (ALLOCATED(lvxyz_even)) DEALLOCATE (lvxyz_even)
       ALLOCATE(lvxyz_odd(0:j_lev,1:3,0:n_wlt_fmly,0:n_trnsf_type),       lvxyz_even(0:j_lev,1:3,0:n_wlt_fmly,0:n_trnsf_type))
       IF (ALLOCATED(nlvxyz_odd)) DEALLOCATE (nlvxyz_odd)
       IF (ALLOCATED(nlvxyz_even)) DEALLOCATE (nlvxyz_even)
       ALLOCATE(nlvxyz_odd(1:5,1:3,1:2*nwlt,0:n_wlt_fmly,0:n_trnsf_type),nlvxyz_even(1:5,1:3,1:2*nwlt,0:n_wlt_fmly,0:n_trnsf_type))
       IF (ALLOCATED(lvdxyz_odd)) DEALLOCATE (lvdxyz_odd)
       IF (ALLOCATED(lvdxyz_even)) DEALLOCATE (lvdxyz_even)
       ALLOCATE(lvdxyz_odd(0:j_lev,1:3,0:n_wlt_fmly),       lvdxyz_even(0:j_lev,1:3,0:n_wlt_fmly))
       IF (ALLOCATED(nlvdxyz_odd)) DEALLOCATE (nlvdxyz_odd)
       IF (ALLOCATED(nlvdxyz_even)) DEALLOCATE (nlvdxyz_even)
       ALLOCATE(nlvdxyz_odd(1:6,1:3,1:4*nwlt,0:n_wlt_fmly),nlvdxyz_even(1:6,1:3,1:4*nwlt,0:n_wlt_fmly))
       IF (ALLOCATED(x)) DEALLOCATE (x)
       ALLOCATE(x(1:nwlt,1:dim))
       IF (ALLOCATED(h)) DEALLOCATE (h)
       IF (ALLOCATED(wrk)) DEALLOCATE (wrk)
       ALLOCATE(h(1:j_lev,1:dim),wrk(0:nxyz(1),0:nxyz(2),0:jd*nxyz(3)))
       IF (ALLOCATED(iwrk)) DEALLOCATE (iwrk)
       ALLOCATE(iwrk(1:PRODUCT(nxyz(1:dim)+1)))
       IF (ALLOCATED(wgh_updt)) DEALLOCATE (wgh_updt)
       ALLOCATE(wgh_updt(-2*MAXVAL(n_updt):2*MAXVAL(n_updt),0:MAXVAL(nxyz),j_lev-1,0:n_wlt_fmly,0:n_trnsf_type,1:3))
       IF (ALLOCATED(wgh_prdct)) DEALLOCATE (wgh_prdct)
       ALLOCATE(wgh_prdct(-2*MAXVAL(n_prdct):2*MAXVAL(n_prdct),0:MAXVAL(nxyz),j_lev-1,0:n_wlt_fmly,0:n_trnsf_type,1:3) )
       IF (ALLOCATED(wgh_df)) DEALLOCATE (wgh_df)
       ALLOCATE(wgh_df(-2*n_diff-1:2*n_diff+1,0:MAXVAL(nxyz),j_lev,0:n_df,1:3))
       IF (ALLOCATED(wgh_d2f)) DEALLOCATE (wgh_d2f)
       ALLOCATE(wgh_d2f(-2*n_diff-1:2*n_diff+1,0:MAXVAL(nxyz),j_lev,0:n_df,1:3) )
       IF (ALLOCATED(i_c)) DEALLOCATE (i_c)
       IF (ALLOCATED(i_d)) DEALLOCATE (i_d)
       ALLOCATE(i_c(0:nxyz(1),0:nxyz(2),0:jd*nxyz(3)), i_d(0:nxyz(1),0:nxyz(2),0:jd*nxyz(3)))
       IF (ALLOCATED(nlv)) DEALLOCATE (nlv)
       IF (ALLOCATED(nlvD)) DEALLOCATE (nlvD)
       IF (ALLOCATED(lv)) DEALLOCATE (lv)
       ALLOCATE(nlv(1:4,1:nwlt)  ,nlvD(1:4*dim,1:nwlt,0:n_df)  ,lv(0:j_lev))

        wgh_updt = 0.0_pr;       wgh_prdct = 0.0_pr
       wgh_df = 0.0_pr;         wgh_d2f = 0.0_pr
       
       nlv = 0
       nlvD = 0; lv = 0
       lvxyz_odd = 0; lvxyz_even = 0
       lvdxyz_odd = 0; lvdxyz_even = 0
       i_c = .FALSE.; i_d = .FALSE.
       DO i=1,nwlt
          IF(dim==3) THEN
             i_c(indx(i,1),indx(i,2),indx(i,3)) = .TRUE. 
          ELSE IF (dim==2) THEN
             i_c(indx(i,1),indx(i,2),:) = .TRUE.
          ELSE
             PRINT*, "error of dimension..."
          END IF
       END DO
       x=0.0_pr; wrk=0.0_pr; iwrk = 0; h = 0.0_pr
       !nwlt_old=0      !old version
       !j_lev_old=0     !old version
       !indx_old = 0    !old version
       !nwltj_old =0    !old version
       j_lev_new=j_lev
       j_lev_old=j_lev
       nwlt_old=nwlt
       nwltj_old =nwltj
       IF (ALLOCATED(indx_old)) DEALLOCATE (indx_old)
       ALLOCATE(indx_old(1:nwlt_old,1:3))
       indx_old = indx !prepare for rearrange_u
    ELSE IF (ireg == 0) THEN !first initialization call
       j_lev = MAX(j_mn,j_lev_init) ! set current wavelet transform level to atleast minimum level
       j_lev = MIN( j_lev, j_mx ) ! set current wavelet transform level to below the max level
       nwlt=(1-prd(1)+mxyz(1)*2**(j_lev-1))*(1-prd(2)+mxyz(2)*2**(j_lev-1))

       IF (ALLOCATED (Nwlt_lev)) DEALLOCATE (Nwlt_lev)
       ALLOCATE(Nwlt_lev(1:j_mx,0:1)) ! allocates only once for all levels up to j_mx
       if( dim == 3 ) nwlt=nwlt*(1-prd(3)+mxyz(3)*2**(j_lev-1))  
       nwltj = 2*nwlt 
       nxyz(1:2)=mxyz(1:2)*2**(j_lev-1) !calculate non-adaptive resolution from levels and M
       nxyz(3)=mxyz(3)*2**(jd*(j_lev-1)) !calculate non-adaptive resolution from levels and M
       maxval_nxyz = MAXVAL(nxyz)

       IF (ALLOCATED (indx)) DEALLOCATE (indx)
       IF (ALLOCATED (indx_old)) DEALLOCATE (indx_old)
       ALLOCATE(indx(1:nwlt,1:3),indx_old(1:nwlt,1:3))
       IF (ALLOCATED (nlv)) DEALLOCATE (nlv)
       IF (ALLOCATED (nlvD)) DEALLOCATE (nlvD)
       IF (ALLOCATED (lv)) DEALLOCATE (lv)
       ALLOCATE(nlv(1:4,1:nwlt)  ,nlvD(1:4*dim,1:nwlt,0:n_df)  ,lv(0:j_lev))
       IF (ALLOCATED (nlvj)) DEALLOCATE (nlvj)
       ALLOCATE(nlvj(1:4,1:nwltj) )
       !Oleg, need to keep it for elliptic solver option
       !DG not needed during intial grid adaptation.   ALLOCATE(nlvjD(1:4*dim,1:nwltj,0:n_df)
       IF (ALLOCATED (lvj)) DEALLOCATE (lvj)
       ALLOCATE(lvj(0:j_lev))
       IF (ALLOCATED (lv_intrnl)) DEALLOCATE (lv_intrnl)
       IF (ALLOCATED (lv_bnd)) DEALLOCATE (lv_bnd)
       ALLOCATE(lv_intrnl(0:j_lev),lv_bnd(0:3,1:2*dim,0:j_lev)) !changed
       IF (ALLOCATED (lvxyz_odd)) DEALLOCATE (lvxyz_odd)
       IF (ALLOCATED (lvxyz_even)) DEALLOCATE (lvxyz_even)
       ALLOCATE(lvxyz_odd(0:j_lev,1:3,0:n_wlt_fmly,0:n_trnsf_type),       lvxyz_even(0:j_lev,1:3,0:n_wlt_fmly,0:n_trnsf_type) )
       IF (ALLOCATED (nlvxyz_odd)) DEALLOCATE (nlvxyz_odd)
       IF (ALLOCATED (nlvxyz_even)) DEALLOCATE (nlvxyz_even)
       ALLOCATE(nlvxyz_odd(1:5,1:3,1:2*nwlt,0:n_wlt_fmly,0:n_trnsf_type),nlvxyz_even(1:5,1:3,1:2*nwlt,0:n_wlt_fmly,0:n_trnsf_type))
       IF (ALLOCATED (lvdxyz_odd)) DEALLOCATE (lvdxyz_odd)
       IF (ALLOCATED (lvdxyz_even)) DEALLOCATE (lvdxyz_even)
       ALLOCATE(lvdxyz_odd(0:j_lev,1:3,0:n_wlt_fmly),       lvdxyz_even(0:j_lev,1:3,0:n_wlt_fmly))
       !Oleg, need to keep it for elliptic solver option
       !DG not needed during intial grid adaptation.  ALLOCATE(nlvdxyz_odd(1:6,1:3,1:4*nwlt),nlvdxyz_even(1:6,1:3,1:4*nwlt))
       IF (ALLOCATED (x)) DEALLOCATE (x)
       ALLOCATE(x(1:nwlt,1:dim))
       IF (ALLOCATED (i_c)) DEALLOCATE (i_c)
       IF (ALLOCATED (i_d)) DEALLOCATE (i_d)
       ALLOCATE(i_c(0:nxyz(1),0:nxyz(2),0:jd*nxyz(3)),i_d(0:nxyz(1),0:nxyz(2),0:jd*nxyz(3)))
       IF (ALLOCATED (xx)) DEALLOCATE (xx)
       IF (ALLOCATED (h)) DEALLOCATE (h)
       IF (ALLOCATED (wrk)) DEALLOCATE (wrk)
       ALLOCATE(xx(0:MAXVAL(nxyz),1:dim), h(1:j_lev,1:dim), wrk(0:nxyz(1),0:nxyz(2),0:jd*nxyz(3)))
       IF (ALLOCATED (iwrk)) DEALLOCATE (iwrk)
       ALLOCATE(iwrk(1:PRODUCT(nxyz(1:dim)+1)))
       IF (ALLOCATED (wgh_updt)) DEALLOCATE (wgh_updt)
       ALLOCATE(wgh_updt(-2*MAXVAL(n_updt):2*MAXVAL(n_updt),0:MAXVAL(nxyz),j_lev-1,0:n_wlt_fmly,0:n_trnsf_type,1:3))
       IF (ALLOCATED (wgh_prdct)) DEALLOCATE (wgh_prdct)
       ALLOCATE(wgh_prdct(-2*MAXVAL(n_prdct):2*MAXVAL(n_prdct),0:MAXVAL(nxyz),j_lev-1,0:n_wlt_fmly,0:n_trnsf_type,1:3) )
       IF (ALLOCATED (wgh_df)) DEALLOCATE (wgh_df)
       ALLOCATE(wgh_df(-2*n_diff-1:2*n_diff+1,0:MAXVAL(nxyz),j_lev,0:n_df,1:3))
       IF (ALLOCATED (wgh_d2f)) DEALLOCATE (wgh_d2f)
       ALLOCATE(wgh_d2f(-2*n_diff-1:2*n_diff+1,0:MAXVAL(nxyz),j_lev,0:n_df,1:3) )

       wgh_updt = 0.0_pr;       wgh_prdct = 0.0_pr
       wgh_df = 0.0_pr;         wgh_d2f = 0.0_pr

       indx = 0; indx_old = 0; nlv = 0
       lvxyz_odd = 0; lvxyz_even = 0; 
       lvdxyz_odd = 0; lvdxyz_even = 0; 
       i_c = .FALSE.; i_c(0:nxyz(1)-prd(1),0:nxyz(2)-prd(2),0:nxyz(3)-prd(3)) = .TRUE.; i_d = .FALSE.
       x=0.0_pr; xx=0.0_pr; wrk=0.0_pr; iwrk = 0; h = 0.0_pr
       nwlt_old=0
       j_lev_old=0
       j_lev_new=j_lev
       indx_old = 0
       nwltj_old =0
    ELSE
       j_lev_old = j_lev

       !
       ! Allocate large index arrays on the first time step. 
       ! (This was moved from the inititlaization call (ireg == 0) above
       !
       IF( .NOT.  ALLOCATED(nlvjD)) ALLOCATE(nlvjD(1:4*dim,1:nwltj,0:n_df) )
       IF( .NOT.  ALLOCATED(nlvdxyz_odd )) ALLOCATE(nlvdxyz_odd(1:6,1:3,1:4*nwlt,0:n_wlt_fmly) )
       IF( .NOT.  ALLOCATED(nlvdxyz_even )) ALLOCATE(nlvdxyz_even(1:6,1:3,1:4*nwlt,0:n_wlt_fmly))
       IF(nwlt /= nwlt_old ) then
          IF(allocated(indx_old)) DEALLOCATE(indx_old)
          ALLOCATE(indx_old(1:nwlt,1:3))
          indx_old = 0
       END IF
       nwlt_old = nwlt
       indx_old(1:nwlt_old,1:dim) = indx(1:nwlt_old,1:dim)
       j_lev = j_lev_new
       PRINT *,'TEST 1 nwlt, nwlt_old ', nwlt, nwlt_old
       nwlt = COUNT( &
            i_c(0:mxyz(1)*2**(j_lev - 1) - prd(1),0:mxyz(2)*2**(j_lev - 1) - prd(2),0:mxyz(3)*2**(jd*(j_lev - 1)) - prd(3)))
       PRINT *,'TEST 1.5 nwlt, nwlt_old ', nwlt, nwlt_old ,&
            mxyz(1)*2**(j_lev - 1) - prd(1),mxyz(2)*2**(j_lev - 1) - prd(2),&
            mxyz(3)*2**(jd*(j_lev - 1)) - prd(3)

       IF(nwlt /= nwlt_old ) then
          IF(allocated(indx)) DEALLOCATE(indx)
          IF(allocated(nlv)) DEALLOCATE(nlv)
          IF(allocated(nlvD)) DEALLOCATE(nlvD)
          ALLOCATE(indx(1:nwlt,1:3),nlv(1:4,1:nwlt),nlvD(1:4*dim,1:nwlt,0:n_df))
          indx = 0; nlv = 0
       END IF
       IF( j_lev /= j_lev_old ) then
          IF(allocated(lv_intrnl)) DEALLOCATE(lv_intrnl) !changed
          IF(allocated(lv_bnd)) DEALLOCATE(lv_bnd) !changed
          ALLOCATE(lv_intrnl(0:j_lev),lv_bnd(0:3,1:2*dim,0:j_lev)) !changed
       END IF
    END IF

    !
    ! ----- Build indx() array
    ! this part of the subroutine is changed to account for boundary points
    !
    nwlt = 0
    lv_intrnl(0) = 0
    !---------- go through internal points level by level (Building indx() array )
    ! Field is traversed starting at j=1 up to j=j_lev. Points
    ! picked up on level j are then ignored when they are hit on higher levels.
    ! Boundary points are skipped in this loop. They are done below.
    DO j =1,j_lev
       !WRITE (6,'("MK INDX j_lev = ", i3.3, "  j = ", i3.3, "   2**(j_lev-j) = ", i3.3 )') &
       !     j_lev,j,2**(j_lev-j)
       lv_intrnl(j) = lv_intrnl(j-1)
       ix_l=(1-prd(1))*2**(j_lev-j); ix_h=mxyz(1)*2**(j_lev - 1) - 2**(j_lev-j)
       iy_l=(1-prd(2))*2**(j_lev-j); iy_h=mxyz(2)*2**(j_lev - 1) - 2**(j_lev-j)
       IF( dim == 3 ) THEN
          iz_l=(1-prd(3))*2**(j_lev-j); iz_h=mxyz(3)*2**(j_lev - 1) - 2**(j_lev-j)
       ELSE
          iz_l = 0 ; iz_h = 0
       ENDIF

       !?????????????????????????? ERRROR in ordering, not DB neutral  ??????????????????????????

       DO iz = iz_l, iz_h,2**(j_lev-j)
          DO iy = iy_l, iy_h,2**(j_lev-j)
             DO ix = ix_l, ix_h,2**(j_lev-j)
                IF (i_c(ix,iy,iz)) THEN
                   nwlt = nwlt + 1
                   indx(nwlt,1) = ix
                   indx(nwlt,2) = iy
                   IF(dim==3) indx(nwlt,3) = iz 
                   !PRINT *, nwlt, indx(nwlt,1:dim)              
                   i_c(ix,iy,iz) = .FALSE.
                   lv_intrnl(j) = lv_intrnl(j)+1
                END IF
             END DO
          END DO
       END DO
    END DO
    !---------- go through boundary points (Building indx() array )
    DO i=1,2*dim
       lchk=.FALSE.
       nbnd(ibnd(i))=0
       DO j=1,j_lev  !--- the folowing do-loop changed to set up indicies for multilevel calculations.
          lv_bnd(0,i,j) = nwlt+1 
          lv_bnd(1,i,j) = nwlt   
          IF(ibnd(i) == 1 .AND. prd(1) == 0) THEN 
             !left  boundary (xmin)
             lchk=.TRUE.
             ix_l=0; ix_h=0
             iy_l=0; iy_h=mxyz(2)*2**(j_lev - 1) - prd(2)*2**(j_lev-j)
             iz_l=0; iz_h=jd*( mxyz(3)*2**(j_lev - 1) - prd(3)*2**(j_lev-j) )
          ELSE IF(ibnd(i) == 2 .AND. prd(1) == 0) THEN 
             !right boundary (xmax)
             lchk=.TRUE.
             ix_l=mxyz(1)*2**(j_lev - 1); ix_h=mxyz(1)*2**(j_lev - 1)
             iy_l=0; iy_h=mxyz(2)*2**(j_lev - 1) - prd(2)*2**(j_lev-j)
             iz_l=0; iz_h=jd*( mxyz(3)*2**(j_lev - 1) - prd(3)*2**(j_lev-j) )
          ELSE IF(ibnd(i) == 3 .AND. prd(2) == 0) THEN 
             !bottom boundary (ymin)
             lchk=.TRUE.
             ix_l=0; ix_h=mxyz(1)*2**(j_lev - 1) - prd(1)*2**(j_lev-j)
             iy_l=0; iy_h=0
             iz_l=0; iz_h=jd*( mxyz(3)*2**(j_lev - 1) - prd(3)*2**(j_lev-j) )
          ELSE IF(ibnd(i) == 4 .AND. prd(2) == 0) THEN 
             !top boundary (ymax)
             lchk=.TRUE.
             ix_l=0; ix_h=mxyz(1)*2**(j_lev - 1) - prd(1)*2**(j_lev-j)
             iy_l=mxyz(2)*2**(j_lev - 1); iy_h=mxyz(2)*2**(j_lev - 1)
             iz_l=0; iz_h=jd*( mxyz(3)*2**(j_lev - 1) - prd(3)*2**(j_lev-j) )
          ELSE IF(dim == 3 .and. ibnd(i) == 5 .AND. prd(3) == 0) THEN 
             !bottom boundary (zmin)
             lchk=.TRUE.
             ix_l=0; ix_h=mxyz(1)*2**(j_lev - 1) - prd(1)*2**(j_lev-j)
             iy_l=0; iy_h=mxyz(2)*2**(j_lev - 1) - prd(2)*2**(j_lev-j)
             iz_l=0; iz_h=0
          ELSE IF(dim == 3 .and. ibnd(i) == 6 .AND. prd(3) == 0) THEN 
             !top boundary (zmax)
             lchk=.TRUE.
             ix_l=0; ix_h=mxyz(1)*2**(j_lev - 1) - prd(1)*2**(j_lev-j)
             iy_l=0; iy_h=mxyz(2)*2**(j_lev - 1) - prd(2)*2**(j_lev-j)
             iz_l=mxyz(3)*2**(j_lev - 1); iz_h=mxyz(3)*2**(j_lev - 1)
          END IF
          IF(lchk) THEN
             DO iz = iz_l, iz_h, 2**(j_lev-j)
                DO iy = iy_l, iy_h, 2**(j_lev-j)
                   DO ix = ix_l, ix_h, 2**(j_lev-j)
                      IF (i_c(ix,iy,iz)) THEN
                         nwlt = nwlt + 1
                         nbnd(ibnd(i))=nbnd(ibnd(i))+1
                         indx(nwlt,1) = ix
                         indx(nwlt,2) = iy
                         indx(nwlt,3) = iz
                         i_c(ix,iy,iz) = .FALSE.
                         lv_bnd(1,i,j) = lv_bnd(1,i,j) +1 
                      END IF
                   END DO
                END DO
             END DO
          END IF
       END DO
    END DO
    ! up date the lv_bnd(2:3, , ) 
    ! The 2:3 indices are for mappign the bnd points to the end of a lower level 
    ! in the flat u array. Used in diff_aux()
    DO j=1,j_lev
       i = 1
       lv_bnd(2,i,j) = lv_intrnl(j) + 1
       lv_bnd(3,i,j) = lv_bnd(2,i,j) + lv_bnd(1,i,j)-lv_bnd(0,i,1)
       DO i=2,2*dim
          lv_bnd(2,i,j) = lv_bnd(3,i-1,j) + 1
          lv_bnd(3,i,j) = lv_bnd(2,i,j) + lv_bnd(1,i,j)-lv_bnd(0,i,1)
       END DO
    END DO
    DO i=1,nwlt
       IF(dim==3) THEN
          i_c(indx(i,1),indx(i,2),indx(i,3))=.TRUE.
       ELSE IF (dim==2) THEN
          i_c(indx(i,1),indx(i,2),:)=.TRUE.
       ELSE
          PRINT *, 'Error dimension'
          STOP
       END IF
    END DO
    !PRINT *,'TEST 2 COUNT(i_c) ',  COUNT(i_c), nwlt
    !
    !-------------- mesh reassignment ---------------------------
    !
!!$PRINT *,'TEST j_lev_old j_lev = ', j_lev_old ,j_lev
    IF(j_lev_old /= j_lev .OR. ireg /= 1) THEN 
!!$PRINT *,'TEST doing mesh reasignment '
       nxyz(1:2)=mxyz(1:2)*2**(j_lev-1)
       nxyz(3)=mxyz(3)*2**(jd*(j_lev-1))
       maxval_nxyz = MAXVAL(nxyz)
       !
       IF (ALLOCATED(xx)) DEALLOCATE(xx)
       IF (ALLOCATED(h)) DEALLOCATE(h)
       IF (ALLOCATED(wrk)) DEALLOCATE(wrk)
       IF (ALLOCATED(iwrk)) DEALLOCATE(iwrk)
       IF (ALLOCATED(lv)) DEALLOCATE(lv)
       IF (ALLOCATED(lvj)) DEALLOCATE(lvj)
       IF (ALLOCATED(lvxyz_odd)) DEALLOCATE(lvxyz_odd)
       IF (ALLOCATED(lvxyz_even)) DEALLOCATE(lvxyz_even)
       IF (ALLOCATED(lvdxyz_odd)) DEALLOCATE(lvdxyz_odd)
       IF (ALLOCATED(lvdxyz_even)) DEALLOCATE(lvdxyz_even)
       !    DEALLOCATE(wgh_updt,wgh_prdct,wgh_df,wgh_d2f)
       IF (ALLOCATED(wgh_updt)) DEALLOCATE(wgh_updt)
       IF (ALLOCATED(wgh_prdct)) DEALLOCATE(wgh_prdct)
       IF (ALLOCATED(wgh_df)) DEALLOCATE(wgh_df)
       IF (ALLOCATED(wgh_d2f)) DEALLOCATE(wgh_d2f) 
       ALLOCATE(xx(0:MAXVAL(nxyz),1:dim),h(1:j_lev,1:dim) )
       ALLOCATE(wrk(0:nxyz(1),0:nxyz(2),0:jd*nxyz(3)))
       ALLOCATE(iwrk(1:PRODUCT(nxyz(1:dim)+1)))
       ALLOCATE(lv(0:j_lev),lvj(0:j_lev))
       ALLOCATE(wgh_updt (-2*MAXVAL(n_updt) :2*MAXVAL(n_updt) ,0:MAXVAL(nxyz),j_lev-1,0:n_wlt_fmly,0:n_trnsf_type,1:3))
       ALLOCATE(wgh_prdct(-2*MAXVAL(n_prdct):2*MAXVAL(n_prdct),0:MAXVAL(nxyz),j_lev-1,0:n_wlt_fmly,0:n_trnsf_type,1:3))
       ALLOCATE(wgh_df(-2*n_diff-1:2*n_diff+1,0:MAXVAL(nxyz),j_lev,0:n_df,1:3))
       ALLOCATE(wgh_d2f(-2*n_diff-1:2*n_diff+1,0:MAXVAL(nxyz),j_lev,0:n_df,1:3) )
       ALLOCATE(lvxyz_odd(0:j_lev,1:3,0:n_wlt_fmly,0:n_trnsf_type), lvxyz_even(0:j_lev,1:3,0:n_wlt_fmly,0:n_trnsf_type))
       ALLOCATE(lvdxyz_odd(0:j_lev,1:3,0:n_wlt_fmly), lvdxyz_even(0:j_lev,1:3,0:n_wlt_fmly))
       
       wgh_updt = 0.0_pr;   wgh_prdct = 0.0_pr
       wgh_df = 0.0_pr;     wgh_d2f = 0.0_pr
       
       lvxyz_odd = 0;       lvxyz_even = 0; 
       lvdxyz_odd = 0;      lvdxyz_even = 0;

       xx=0.0_pr; wrk=0.0_pr; iwrk = 0; h = 0.0_pr; lv =0
       
       CALL set_xx (xx(0:MAXVAL(nxyz(1:dim)),1:dim),h(1:j_lev,1:dim),nxyz(1:dim),MAXVAL(nxyz(1:dim)),j_lev)

       !
       !-------- setting weights for wavelet transform
       !
       DO trnsf_type = 0,n_trnsf_type
          DO wlt_fmly = 0, n_wlt_fmly
             DO j = 1, j_lev-1
                DO idim =1,dim
                   DO ix = 1, mxyz(idim)*2**(j-1)
                      ix_odd = (2*ix-1)*2**(j_lev-1-j)
                      ix_l   = -MIN(ix-trnsf_type,n_prdct(wlt_fmly))
                      ix_h   = ix_l+2*n_prdct(wlt_fmly)-1
                      ix_h   = MIN(ix_h,mxyz(idim)*2**(j-1)-ix-trnsf_type)
                      ix_l   = ix_h-2*n_prdct(wlt_fmly)+1
                      ix_l = (1-prd(idim))*MAX(ix_l,-ix+trnsf_type) + prd(idim)*(-n_prdct(wlt_fmly))
                      ix_h = (1-prd(idim))*MIN(ix_h,mxyz(idim)*2**(j-1)-ix-trnsf_type) + prd(idim)*(n_prdct(wlt_fmly) - 1)
                      IF  (ix_l+ix_h+1 < 0 .AND. ix_h >= 0 .AND. prd(idim) ==0 ) THEN        ! the right boundary
                         ix_l = MAX(ix_l,-(ix_h+1)-n_assym_prdct(wlt_fmly,trnsf_type),-ix+trnsf_type)
                      ELSE IF  (ix_l+ix_h+1 < 0 .AND. ix_h == -1 .AND. prd(idim) ==0 ) THEN  ! right on the right boundary
                         ix_l = MAX(ix_l,-1-n_assym_prdct_bnd(wlt_fmly,trnsf_type),-ix+trnsf_type )
                      ELSE IF  (ix_l+ix_h+1 > 0 .AND. ix_l < 0 .AND. prd(idim) ==0 ) THEN    !  the left boundary
                         ix_h = MIN(ix_h,-ix_l-1+n_assym_prdct(wlt_fmly,trnsf_type),mxyz(idim)*2**(j-1)-ix-trnsf_type )
                      ELSE IF  (ix_l+ix_h+1 > 0 .AND. ix_l == 0 .AND. prd(idim) ==0 ) THEN   ! right on the left boundary
                         ix_h = MIN(ix_h,n_assym_prdct_bnd(wlt_fmly,trnsf_type),mxyz(idim)*2**(j-1)-ix-trnsf_type)   
                      END IF
                      !PRINT *, '@', trnsf_type, wlt_fmly, idim, ix_odd*2**(j_mx-j_lev), ix_l, ix_h, &
                      !     wgh_prdct(ix_l:ix_h, ix_odd,j,wlt_fmly,trnsf_type,idim)
                      DO ixpm = ix_l, ix_h
                         wgh_prdct(ixpm,ix_odd,j,wlt_fmly,trnsf_type,idim)= &
                              wgh (ixpm, ix_odd, ix_l, ix_h, 2**(j_lev-1-j), prd(idim), nxyz(idim), xx(0:nxyz(idim),idim))
                         !@@@@
                      END DO
                   END DO
                   DO ix = trnsf_type, mxyz(idim)*2**(j-1)-prd(idim) - trnsf_type
                      ix_even = ix*2**(j_lev-j)
                      ix_l = - MIN (ix, n_updt(wlt_fmly))
                      ix_h = ix_l + 2*n_updt(wlt_fmly) - 1
                      ix_h = MIN (ix_h, mxyz(idim)*2**(j-1)-ix-1)
                      ix_l = ix_h - 2*n_updt(wlt_fmly) + 1
                      ix_l = (1-prd(idim))*MAX(ix_l,-ix) + prd(idim)*(-n_updt(wlt_fmly))
                      ix_h = (1-prd(idim))*MIN(ix_h,mxyz(idim)*2**(j-1)-ix-1) + prd(idim)*(n_updt(wlt_fmly) - 1)
                      IF  (ix_l+ix_h+1 < 0 .AND. ix_h >= 0 .AND. prd(idim) == 0) THEN                               ! the right boundary
                         ix_l = MAX(ix_l,-(ix_h+1)-n_assym_updt(wlt_fmly,trnsf_type),-ix)
                      ELSE IF  (ix_l+ix_h+1 < 0 .AND. ix_h == -1 .AND. prd(idim) == 0) THEN                        ! right on the right boundary
                         ix_l = MAX(ix_l,-1-n_assym_updt_bnd(wlt_fmly,trnsf_type),-ix)
                      ELSE IF  (ix_l+ix_h+1 > 0 .AND. ix_l < 0 .AND. prd(idim) == 0) THEN                          !  the left boundary
                         ix_h = MIN(ix_h,-ix_l-1+n_assym_updt(wlt_fmly,trnsf_type),mxyz(idim)*2**(j-1)-ix-1)
                      ELSE IF  (ix_l+ix_h+1 > 0 .AND. ix_l == 0 .AND. prd(idim) == 0) THEN                         ! right on the left boundary
                         ix_h = MIN(ix_h,n_assym_updt_bnd(wlt_fmly,trnsf_type),mxyz(idim)*2**(j-1)-ix-1)   
                      END IF
                      DO ixpm = ix_l, ix_h
                         wgh_updt(ixpm,ix_even,j,wlt_fmly,trnsf_type,idim) = &
                              wgh (ixpm, ix_even, ix_l, ix_h, 2**(j_lev-1-j), prd(idim), nxyz(idim), xx(0:nxyz(idim),idim))
                         !PRINT *, 'wgh_updt', wgh_updt(ixpm,ix_even,j,wlt_fmly,trnsf_type,idim)
                         !PRINT *, ixpm, ix_even, ix_l, ix_h, 2**(j_lev-1-j), prd(idim), nxyz(idim), xx(0:nxyz(idim),idim)
                      END DO
                   END DO
                END DO
             END DO
          END DO
       END DO
       !PRINT *,'TEST 4 COUNT(i_c) ',  COUNT(i_c), nwlt


       !
       !-------- setting weights for derivatives calculation
       !
       DO meth =0, n_df
          IF(meth == 0) THEN 
             nD=1
          ELSE IF(meth == 1) THEN 
             nD=n_diff
          ELSE IF(meth == 2) THEN 
             nD=1
          ELSE IF(meth == 3) THEN 
             nD=n_diff
          ELSE IF(meth == 4) THEN 
             nD=1
          ELSE IF(meth == 5) THEN 
             nD=n_diff
          END IF
          DO j = 1, j_lev
             DO idim =1,dim
                DO ih = 0,  mxyz(idim)*2**(j-1)-prd(idim)
                   ix=ih*2**(j_lev-j)
                   ix_l =-MIN(ih,nD)
                   ix_h = ix_l+2*nD
                   ix_h = MIN(ix_h,  mxyz(idim)*2**(j-1)-ih)
                   ix_l=ix_h-2*nD
                   ix_l=(1-prd(idim))*MAX(MIN(ix_l,0),-ih) + prd(idim)*(-nD)
                   ix_h=(1-prd(idim))*MIN(MAX(ix_h,0),mxyz(idim)*2**(j-1)-ih) + prd(idim)*(nD)

!!$                 !========================================= OLD version ============================================
!!$                 IF       (ix_l+ix_h == 0) THEN                                        ! the middle of the domain
!!$                    ix_l = ix_l+forward_bias(meth)
!!$                    ix_h = ix_h-backward_bias(meth)
!!$                 ELSE IF  (ix_l+ix_h < 0 .AND. backward_bias(meth)==1 .AND. ix_h > 0) THEN    ! the right boundary
!!$                    ix_l = MIN(MAX(ix_l,-ix_h-nd_assym(meth),-ih),ix_h-1)           
!!$                 ELSE IF  (ix_l+ix_h < 0 .AND. forward_bias(meth)==1 .AND. ix_h > 0) THEN  !  the right boundary
!!$                    ix_l = MIN(MAX(-ix_h+1,-ih),ix_h-1)
!!$                 ELSE IF  (ix_l+ix_h < 0 .AND. ix_h == 0) THEN                         ! right on the right boundary
!!$                    ix_l = MIN(MAX(ix_l,-nd_assym_bnd(meth),-ih),ix_h-1)
!!$                 ELSE IF  (ix_l+ix_h > 0 .AND. backward_bias(meth)==1 .AND. ix_l < 0) THEN    !  the left boundary
!!$                    ix_h = MAX(MIN(-ix_l-1,mxyz(idim)*2**(j-1)-ih),ix_l+1)
!!$                 ELSE IF  (ix_l+ix_h > 0 .AND. forward_bias(meth)==1 .AND. ix_l < 0 ) THEN !  the left boundary
!!$                    ix_h = MAX(MIN(ix_h,-ix_l+nd_assym(meth),mxyz(idim)*2**(j-1)-ih),ix_l+1)
!!$                 ELSE IF  (ix_l+ix_h > 0 .AND. ix_l == 0 ) THEN                        ! right on the left boundary
!!$                    ix_h = MAX(MIN(ix_h,nd_assym_bnd(meth),mxyz(idim)*2**(j-1)-ih),ix_l+1)  !comment if do not want reduce the order of derivative 
!!$                 END IF

!!$                 !======================== symmetric stencil - order drops close to the wall ======================= 
!!$                 IF       (ix_l+ix_h == 0) THEN                                        ! the middle of the domain
!!$                    ix_l = ix_l+forward_bias(meth)
!!$                    ix_h = ix_h-backward_bias(meth)
!!$                 ELSE IF  (ix_l+ix_h < 0  .AND. ix_h > 0) THEN    ! the right boundary
!!$                    ix_l = MIN(MAX(ix_l,-ix_h-nd_assym(meth),-ih),ix_h-1)           
!!$                    ix_l = ix_l+forward_bias(meth)
!!$                    ix_h = ix_h-backward_bias(meth)
!!$                 ELSE IF  (ix_l+ix_h < 0 .AND. ix_h == 0) THEN                         ! right on the right boundary
!!$                    ix_l = MIN(MAX(ix_l,-nd_assym_bnd(meth),-ih),ix_h-1)
!!$                 ELSE IF  (ix_l+ix_h > 0 .AND. ix_l < 0 ) THEN !  the left boundary
!!$                    ix_h = MAX(MIN(ix_h,-ix_l+nd_assym(meth),mxyz(idim)*2**(j-1)-ih),ix_l+1)
!!$                    ix_l = ix_l+forward_bias(meth)
!!$                    ix_h = ix_h-backward_bias(meth)
!!$                 ELSE IF  (ix_l+ix_h > 0 .AND. ix_l == 0 ) THEN                        ! right on the left boundary
!!$                    ix_h = MAX(MIN(ix_h,nd_assym_bnd(meth),mxyz(idim)*2**(j-1)-ih),ix_l+1)  !comment if do not want reduce the order of derivative 
!!$                 END IF

                   !======================== swapping backward_bias direction if close to the  ======================= 
                   IF       (ix_l+ix_h == 0) THEN                                        ! the middle of the domain
                      ix_l = ix_l+forward_bias(meth)
                      ix_h = ix_h-backward_bias(meth)
                   ELSE IF  (ix_l+ix_h < 0  .AND. ix_h > 0) THEN    ! the right boundary
                      !swapping the backward_bias <-> forward_bias 
                      ix_l = MIN(MAX(ix_l,-ix_h-nd_assym(meth),-ih),ix_h-1)           
!OLD                      ix_l = ix_l+backward_bias(meth) !swapping the rigth boundary only
!OLD                      ix_h = ix_h-forward_bias(meth)
                      ix_l = ix_l+forward_bias(meth) !swapping the rigth boundary only
                      ix_h = ix_h-backward_bias(meth)
                   ELSE IF  (ix_l+ix_h < 0 .AND. ix_h == 0) THEN                         ! right on the right boundary
                      ix_l = MIN(MAX(ix_l,-nd_assym_bnd(meth),-ih),ix_h-1)
                   ELSE IF  (ix_l+ix_h > 0 .AND. ix_l < 0 ) THEN !  the left boundary
                      ix_h = MAX(MIN(ix_h,-ix_l+nd_assym(meth),mxyz(idim)*2**(j-1)-ih),ix_l+1)
                      ix_l = ix_l+forward_bias(meth)
                      ix_h = ix_h-backward_bias(meth)
                   ELSE IF  (ix_l+ix_h > 0 .AND. ix_l == 0 ) THEN                        ! right on the left boundary
                      ix_h = MAX(MIN(ix_h,nd_assym_bnd(meth),mxyz(idim)*2**(j-1)-ih),ix_l+1)  !comment if do not want reduce the order of derivative 
                   END IF
                   DO ixpm = ix_l, ix_h
                      wgh_df(ixpm,ix,j,meth,idim) = &
                           diff_wgh(ixpm, ix, ix_l, ix_h, 2**(j_lev-j), prd(idim), nxyz(idim), xx(0:nxyz(idim),idim), h(j,idim),1)
                   END DO
                END DO
             END DO
             DO idim =1,dim
                DO ih = 0,  mxyz(idim)*2**(j-1)-prd(idim)
                   ix=ih*2**(j_lev-j)
                   ix_l =-MIN(ih,nD)
                   ix_h = ix_l+2*nD
                   ix_h = MIN(ix_h,  mxyz(idim)*2**(j-1)-ih)
                   ix_l=ix_h-2*nD
                   IF (ix_l > -nD) ix_h = ix_h+1
                   IF (ix_h <  nD) ix_l = ix_l-1
                   ix_l=(1-prd(idim))*MAX(ix_l,-ih) + prd(idim)*(-nD)
                   ix_h=(1-prd(idim))*MIN(ix_h,mxyz(idim)*2**(j-1)-ih) + prd(idim)*(nD)
                   IF(ix_l+ix_h<0) ix_l = MIN(MAX(ix_l,-ix_h-nd2_assym(meth)),ix_h-2)
                   IF(ix_l+ix_h>0) ix_h = MAX(MIN(ix_h,-ix_l+nd2_assym(meth)),ix_l+2)
                   IF(ix_l+ix_h < 0 .AND. ix_h == 0 ) ix_l = MIN(MAX(ix_l,-nd2_assym_bnd(meth),-ih),ix_h-2)
                   IF(ix_l+ix_h > 0 .AND. ix_l == 0 ) ix_h = MAX(MIN(ix_h,nd2_assym_bnd(meth),mxyz(idim)*2**(j-1)-ih),ix_l+2)
                   DO ixpm = ix_l, ix_h
                      wgh_d2f(ixpm,ix,j,meth,idim) = &
                           diff_wgh(ixpm, ix, ix_l, ix_h, 2**(j_lev-j), prd(idim), nxyz(idim), xx(0:nxyz(idim),idim), h(j,idim), 2)
                   END DO
                END DO
             END DO
          END DO
       END DO
    END IF
    IF(nwlt /= nwlt_old .OR. ireg == -1) then
       !IF(nwlt /= nwlt_old ) then
       IF(ALLOCATED(x)) DEALLOCATE(x)
       ALLOCATE(x(1:nwlt,1:dim))
       x=0.0_pr  
    END IF
    DO idim =1,dim
       DO i=1,nwlt
          x(i,idim) = xx(indx(i,idim),idim)
       END DO
    END DO
    !PRINT *,'TEST 5 COUNT(i_c) ',  COUNT(i_c), nwlt


    !
    !----  Setting the points for derivative calculation at different levels
    !
    lv(0) = 0
    !PRINT *, 'TEST deriv_lev IN INDICES '
    ixyz = 0 ! this needs to be initialized to zero to deal with multi-dimensioanlity
    DO j = 1, j_lev
       lv(j) = lv(j-1)
       DO i = 1, nwlt

          ixyz(1:dim)=indx(i,1:dim) ! point for which we will check neighborhood

          jxyz_max = ixyz2j(ixyz(1:dim),j_lev)  !finds a level corresponding to a point.

          jxyz(1:dim) = j_lev+1 ! set to max level
          jxyz(3) = jd*(j_lev+1) ! set to max level

          DO idim = 1,dim 
             lchk =.TRUE.
             ixyz2 = ixyz(idim)

             ! check  neighborhood of point ixyz(:) for presence of the scale jxyz(idim) in idim-dir
             ! find the closet point to this point and then find out what level it is at.
             DO WHILE(lchk .AND. jxyz(idim) > 1)
                jxyz(idim) = jxyz(idim) -1
                ix_l = (1-prd(idim))*&
                     MAX(0,&
                     ixyz2-nbhd*2**(j_lev-jxyz(idim))) + prd(idim)*(ixyz2-nbhd*2**(j_lev-jxyz(idim)))
                ix_h = (1-prd(idim))*&
                     MIN(nxyz(idim), &
                     ixyz2+nbhd*2**(j_lev-jxyz(idim))) + prd(idim)*(ixyz2+nbhd*2**(j_lev-jxyz(idim)))
                DO ixpm=ix_l,ix_h,2**(j_lev-jxyz(idim))
                   ixyz(idim) = (1-prd(idim))*ixpm+prd(idim)*MOD(ixpm+9*nxyz(idim),nxyz(idim))
                   ! if this point is active and this point is not the point ixyz(idim) that we are looking at
                   IF(i_c(ixyz(1),ixyz(2),ixyz(3)) .AND. ixyz(idim)/=ixyz2) lchk = .FALSE.
                END DO
             END DO
             ixyz(idim) = ixyz2
          END DO

          ! If the neighborhood contains a point at level j then we add this point to the
          ! list of points for which the derivative will be done at level j
          !Oleg: Important, there was a logicla bug before, j_df >= jxyz_max
          IF ( MAX(MAXVAL(jxyz(1:dim)),jxyz_max) == j ) THEN !compare to derivative level
             lv(j) = lv(j)+1  ! keep track of the # of points who's derivative is done at this level
             nlv(1,lv(j)) = i ! add this point to the list of pts who's derivative is done at this level
             !WRITE(*,'( "XXderiv_lev ", 6(I5.5 , 1X ) )' ) ixyz, j ,lv(j) ,i  !TESTTEST
          END IF
       END DO
    END DO

    !PRINT *,'TEST 6 COUNT(i_c) ',  COUNT(i_c), nwlt

    !
    !----------- Setting Mask For Derivative Calculations on truncated levels ------------
    ! nwltj is the number of truncated points when j_in == j (see c_diff_fast )
    nwltj_old = nwltj
    nwltj = 0
    lvj(0)=0
    !
    ! for each level j (j_in in c_diff_fast ) find all points whos 
    ! deriviative level is greater or equal to j and whos phsycal level is j or less
    !
    DO j = 1, j_lev
       DO k  = lv(j-1)+1, lv(j_lev) ! lv(j-1)+1, lv(j) derivative needs to calculated on level j
          i  = nlv(1,k) ! map into the actual u vector
          nlv(2:dim+1,k) = indx(i,1:dim) !Oleg: that line was missing
          lchk=.FALSE.
          IF(i <= lv_intrnl(j)) lchk = .TRUE. ! belongs to physcal level j or less
          DO ih=1,2*dim ! check if on bndry
             IF(lv_bnd(0,ih,1)<= i .AND. i <= lv_bnd(1,ih,j)) lchk = .TRUE.
          END DO
          IF(lchk) nwltj = nwltj + 1 !belongs to my level j
       END DO
       lvj(j) = nwltj
    END DO
    IF((ireg == 1 .AND. nwltj /= nwltj_old) .OR. ireg == -1) THEN
       !IF(ireg == 1 .AND. nwltj /= nwltj_old) THEN
       IF( ALLOCATED(nlvj) ) DEALLOCATE(nlvj)
       IF( ALLOCATED(nlvjD) ) DEALLOCATE(nlvjD)
       ALLOCATE(nlvj(1:4,1:nwltj),nlvjD(1:4*dim,1:nwltj,0:n_df))
       nlvj = 0
    END IF

    !Now we go through and fill nlvj( 1 , l) == index into u
    l = 0
    DO j = 1, j_lev
       DO k  = lv(j-1)+1, lv(j_lev)
          i  = nlv(1,k)
          lchk=.FALSE.
          IF(i <= lv_intrnl(j)) lchk = .TRUE.
          DO ih=1,2*dim
             IF(lv_bnd(0,ih,1)<= i .AND. i <= lv_bnd(1,ih,j)) lchk = .TRUE.
          END DO
          IF(lchk) THEN 
             l = l + 1
             nlvj(1,l) = i
          END IF
       END DO
    END DO
    
    !
    !----------- Setting Mask For Derivative Calculations ------------
    ! We do not setup the deriviative mask while we are adapting the 
    ! grid to the IC or a read in field
    !
    IF(ireg /= 0 ) THEN
       i_d = i_c

       CALL diff_mask (i_d, lv, nlv, nlvD, nwlt)
       PRINT * ,'Total # of points in i_d mask before:', &
            COUNT(i_d(0:mxyz(1)*2**(j_lev - 1) - prd(1),0:mxyz(2)*2**(j_lev - 1) - prd(2),0:mxyz(3)*2**(jd*(j_lev - 1)) - prd(3)))


       CALL diff_mask (i_d, lvj, nlvj, nlvjD, nwltj)

       PRINT * ,'Total # of points in i_d mask after: ', &
            COUNT(i_d(0:mxyz(1)*2**(j_lev - 1) - prd(1),0:mxyz(2)*2**(j_lev - 1) - prd(2),0:mxyz(3)*2**(jd*(j_lev - 1)) - prd(3)))

    END IF


    IF ((nwlt_old == nwlt .AND. ireg /= 0) .OR. ireg == -1) THEN
       new_grid=.FALSE.
       DO i = 1, nwlt
          IF (indx(i,1) /= indx_old(i,1) .OR. indx(i,2) /= indx_old(i,2) &
               .OR. indx(i,3) /= indx_old(i,3)) new_grid = .TRUE.
       END DO
    ELSE
       new_grid = .TRUE.
    END IF
    ! PRINT *,'TEST 8 COUNT(i_c) ',  COUNT(i_c), nwlt

    !*********************************************************************************************
    !* This part should be the same for all DB types with few modifications related to DB maping *  
    !*********************************************************************************************
    IF(ireg >= 0) THEN
       Nwlt_lev(:,:) = 0
       DO j = 1, j_lev
          Nwlt_lev(j,0) = lv_intrnl(j)      ! # of internal points on level j and below
          Nwlt_lev(j,1) = lv_bnd(3,2*dim,j) ! # of all points on level j and below
       END DO
       nwlt_intrnl = Nwlt_lev(j_lev,0)
       nwlt_bnd = nwlt - Nwlt_lev(j_lev,0)
       !
       !================== Set up list of pointes based on wlt_type, face_type, j, j_diff, 
       !                   where j_diff is the level at which derivative is taken
       !
       !
       !initializing
       !
       IF(j_lev > j_mx) THEN
          WRITE(*,'("ERROR in indices: j_lev>j_mx")')
          STOP
       END IF
       ! creatig forward and backward mapping, in future has to go thrhough wavelt families and levels to create DB own indx.
       i_p(0) = 1
       i_p_face(0) = 1
       i_p_wlt(0) = 1
       DO i=1,dim
          i_p(i) = i_p(i-1)*(1+nxyz(i))
          i_p_face(i) = i_p_face(i-1)*3
          i_p_wlt(i) = i_p_wlt(i-1)*2
       END DO
       DO i = 1,dim
          ivec(i) = dim-i+1
       END DO
       !----
       IF(.NOT.ALLOCATED(indx_DB)) THEN
          ALLOCATE(indx_DB(1:j_mx,0:2**dim-1,0:3**dim-1,1:j_mx))
          DO j = 1, j_mx
             DO wlt_type = MIN(j-1,1),2**dim - 1
                DO face_type = 0, 3**dim - 1
                   DO j_df = 1, j_mx
                      NULLIFY (indx_DB(j_df,wlt_type,face_type,j)%p)
                      indx_DB(j_df,wlt_type,face_type,j)%real_length = 0
                   END DO
                END DO
             END DO
          END DO
          face = 0 
          face_type_internal = SUM((face+1)*i_p_face(0:dim-1))
       END IF
       !
       CALL get_indices_by_coordinate(1,ixyz,j_lev_old,i1,-1) !cleaning the iwrk
       !
       !
       ! seting up the indx_DB list
       !
       indx_DB(:,:,:,:)%length = 0
       indx_DB(:,:,:,:)%shift  = 1
       DO j_df = 1, j_lev
          DO k = lv(j_df-1)+1, lv(j_df)
             i = nlv(1,k)
             ixyz(1:dim) = indx(i,1:dim)
             face = (-1 + 2*INT(ixyz(1:dim)/nxyz(1:dim))+ MIN(1,MOD(ixyz(1:dim),nxyz(1:dim)))) * (1 - prd(1:dim))
             j = ixyz2j(ixyz(1:dim),j_lev)
             wlt = MIN(MIN(1,j-1),MOD(ixyz(1:dim),2**(j_lev-j+1)))
             face_type = SUM((face+1)*i_p_face(0:dim-1))
             wlt_type = SUM(wlt(ivec)*i_p_wlt(0:dim-1))
             indx_DB(j_df,wlt_type,face_type,j)%length = indx_DB(j_df,wlt_type,face_type,j)%length + 1
             IF(j > j_df) THEN
                PRINT *,'error, j > j_df in setting up the indx_DB list'
                STOP
             END IF
          END DO
       END DO
       ! allocating arrays
       DO j = 1, j_lev
          DO wlt_type = MIN(j-1,1),2**dim-1
             DO face_type = 0, 3**dim - 1
                DO j_df = j, j_lev
                   IF( indx_DB(j_df,wlt_type,face_type,j)%length  /= indx_DB(j_df,wlt_type,face_type,j)%real_length ) THEN
                      ! eventually make smarter allocation algorithm, so 
                      ! if the size is bigger, increasie it by 25% and if smaller than 50% decrease by 25%
                      ! or something like this 
                      IF (indx_DB(j_df,wlt_type,face_type,j)%real_length > 0) THEN
                         DEALLOCATE(indx_DB(j_df,wlt_type,face_type,j)%p)
                         indx_DB(j_df,wlt_type,face_type,j)%real_length = 0
                      END IF
                      IF (indx_DB(j_df,wlt_type,face_type,j)%length  > 0) THEN
                         ALLOCATE(indx_DB(j_df,wlt_type,face_type,j)%p(1:indx_DB(j_df,wlt_type,face_type,j)%length))
                         indx_DB(j_df,wlt_type,face_type,j)%real_length = indx_DB(j_df,wlt_type,face_type,j)%length
                      END IF
                   END IF
                END DO
             END DO
          END DO
       END DO
       !--------------- assigning indx
       !  indx_DB(j_df,wlt_type,face_type,j)%p(k)%ixyz coresponds to level j_lev
       !
       indx_DB(:,:,:,:)%length = 0
       DO j_df = 1, j_lev
          DO k = lv(j_df-1)+1, lv(j_df)
             i = nlv(1,k)
             ixyz(1:dim) = indx(i,1:dim)
             face = (-1 + 2*INT(ixyz(1:dim)/nxyz(1:dim))+ MIN(1,MOD(ixyz(1:dim),nxyz(1:dim)))) * (1 - prd(1:dim))
             j = ixyz2j(ixyz(1:dim),j_lev)
             wlt = MIN(MIN(1,j-1),MOD(ixyz(1:dim),2**(j_lev-j+1)))
             face_type = SUM((face+1)*i_p_face(0:dim-1))
             wlt_type = SUM(wlt(ivec)*i_p_wlt(0:dim-1))
             ii = 1+SUM(ixyz(1:dim)*i_p(0:dim-1)) ! index on j_lev level
             ! ixyz can be exctracted using the follwing command: ixyz(1:dim) = INT(MOD(ii-1,i_p(1:dim))/i_p(0:dim-1))
             indx_DB(j_df,wlt_type,face_type,j)%length = indx_DB(j_df,wlt_type,face_type,j)%length + 1
             indx_DB(j_df,wlt_type,face_type,j)%p(indx_DB(j_df,wlt_type,face_type,j)%length)%ixyz = ii
             indx_DB(j_df,wlt_type,face_type,j)%p(indx_DB(j_df,wlt_type,face_type,j)%length)%i = i
          END DO
       END DO
       ! ensures compatibility with the old code
       ! setting shift for internal points to 0
       face = 0 
       face_type = SUM((face+1)*i_p_face(0:dim-1))
       indx_DB(1:j_lev,0:2**dim-1,face_type,1:j_lev)%shift = 0 ! internal points
       DO ibndr=1,2*dim
          DO i=lv_bnd(0,ibndr,1),lv_bnd(1,ibndr,j_lev)
             ixyz(1:dim) = indx(i,1:dim)
             face = (-1 + 2*INT(ixyz(1:dim)/nxyz(1:dim))+ MIN(1,MOD(ixyz(1:dim),nxyz(1:dim)))) * (1 - prd(1:dim))
             j = ixyz2j(ixyz,j_lev)
             wlt = MIN(MIN(1,j-1),MOD(ixyz,2**(j_lev-j+1)))
             face_type = SUM((face+1)*i_p_face(0:dim-1))
             wlt_type = SUM(wlt(ivec)*i_p_wlt(0:dim-1))
             DO j_lv = 1, j_lev ! j_lv - level of resolution for which u is supplied.
                ishift = lv_bnd(2,ibndr,j_lv)-lv_bnd(0,ibndr,1)
                IF(indx_DB(j_lv,wlt_type,face_type,j)%shift == 1) indx_DB(j_lv,wlt_type,face_type,j)%shift = ishift
             END DO
          END DO
       END DO
       
    END IF  ! ireg >= 0
    
    ! set j_full (for derivative calculations)
    j_full=j_lev
    DO WHILE(lv(j_full) /= 0 .AND. j_full>1)
       j_full=j_full-1
    END DO
    j_full = MAX(1,j_full-1)

    !j_full = j_mn - 1

!!$    ! ----------------------------------
!!$    j_full_tmp=j_mn
!!$    DO j = j_mn, j_lev
!!$       IF( Nwlt_lev(j,1) == PRODUCT(mxyz(1:dim)*2**(j-1)+1-prd(1:dim)) )  j_full_tmp = j
!!$    END DO
!!$    !j_full = j_full_tmp
!!$
!!$    IF (j_full.NE.j_full_tmp) THEN
!!$       PRINT *, 'j_lev=',j_lev,'j_mn=',j_mn
!!$       DO j=1,j_lev
!!$          PRINT *,'j=',j,'lv(j)=',lv(j),'Nwlt_lev(j,0:1)=',Nwlt_lev(j,0:1)
!!$       END DO
!!$       PRINT *, 'WARNING: j_full=', j_full, 'j_full_tmp=',j_full_tmp
!!$       !PAUSE !STOP
!!$    END IF
    ! ----------------------------------

    WRITE (*,'("ireg=",I2,", j_lev=",I3,", nwlt/old=",I8,"/",I8)') &
         ireg, j_lev, nwlt, nwlt_old
    !PAUSE

    CALL get_indices_by_coordinate(1,ixyz,j_lev,i1,1) !preparing iwrk


  END SUBROUTINE indices


  SUBROUTINE check_active_nodes
    USE precision
    USE util_vars
    USE share_consts
    IMPLICIT NONE
    INTEGER :: i, ixyz(3), i_p_prd(0:dim)

    i_p_prd(0)=1
    i_p_prd(1:dim) = nxyz(1:dim)+1-prd(1:dim)
    DO i =1, dim
       i_p_prd(i)  = i_p_prd(i-1) *i_p_prd(i)
    END DO
    DO i=1,i_p_prd(dim)
       ixyz(1:dim) = INT(MOD(i-1,i_p_prd(1:dim))/i_p_prd(0:dim-1))
       IF(i_c(ixyz(1),ixyz(2),ixyz(3))) PRINT *, ixyz(1:dim)
    END DO
 
  END SUBROUTINE check_active_nodes

  !
  ! setup nlv_in and nlvD_in 
  !
  !
  ! nlvD_in( 1:2, :, : )  - first derivative in x-dir
  ! nlvD_in( 3:4, :, : )  - first derivative in y-dir
  ! nlvD_in( 5:6, :, : )   - second derivative in x-dir
  ! nlvD_in( 7:8, :, : )  - second derivative in y-dir
  ! nlvD_in( 9:10, :, : )  - first derivative in z-dir
  ! nlvD_in( 11:12, :, : ) - second derivative in z-dir
  !
  SUBROUTINE diff_mask (i_mask, lv_in, nlv_in, nlvD_in, len_nlv)

    USE precision

    IMPLICIT NONE

    INTEGER, INTENT (IN) :: len_nlv
    !  LOGICAL, DIMENSION (0:nxyz(1),0:nxyz(2),0:jd*nxyz(3)), INTENT (INOUT) :: i_mask
    LOGICAL, DIMENSION (0:nxyz(1),0:nxyz(2),0:nxyz(3)), INTENT (INOUT) :: i_mask
    INTEGER, DIMENSION (0:j_lev), INTENT (IN) :: lv_in
    INTEGER, DIMENSION (1:4,1:len_nlv), INTENT (INOUT) :: nlv_in
    INTEGER, DIMENSION (1:4*dim,1:len_nlv,0:n_df), INTENT (INOUT) :: nlvD_in
    INTEGER, DIMENSION (1:3) :: ixyz, ixyz1, shift1, shift2

    INTEGER :: j,k,l,meth,nD
    INTEGER :: ix,ix_l,ix_h,ixpm
    INTEGER :: idim

    DO j = 1, j_lev
       DO l = lv_in(j-1) + 1, lv_in(j)
          k = nlv_in(1,l)  
          ixyz(1:3) = indx(k,1:3)
          nlv_in(2:4,l)=ixyz(1:3)

          !092404 PRINT *,'wkr lk ghost around: ', ix,iy,iz
          !TEST chk deriv_lev (j_mx = 5 !!!)
          !IF( db(ix*2**(5-j_lev), iy*2**(5-j_lev),iz*2**(5-j_lev))% deriv_lev /= j )THEN
          !  PRINT *,'Error in deriv lev, ixyz_db = ', ix*2**(5-j_lev), iy*2**(5-j_lev),iz*2**(5-j_lev)
          !  PRINT *,'--- j, deriv_lev ', j, db(ix*2**(5-j_lev), iy*2**(5-j_lev),iz*2**(5-j_lev))% deriv_lev 
          !END IF
          !PRINT *,'wkrlk ghost ixyz_db = ', ix*2**(5-j_lev), iy*2**(5-j_lev),iz*2**(5-j_lev)

          DO meth = 0, n_df
             IF(meth == 0) THEN 
                nD=1
             ELSE IF(meth == 1) THEN 
                nD=n_diff
             ELSE IF(meth == 2) THEN 
                nD=1
             ELSE IF(meth == 3) THEN 
                nD=n_diff
             ELSE IF(meth == 4) THEN 
                nD=1
             ELSE IF(meth == 5) THEN 
                nD=n_diff
             END IF
             shift1 = (/0,2,8/)
             shift2 = (/4,6,10/)
             !---------------------- 1-st derivative in Xi-direction ---------------
             DO idim = 1, dim
                ix = ixyz(idim)
                ix_l =-MIN(ix/2**(j_lev-j),nD)
                ix_h = ix_l+2*nD
                ix_h = MIN(ix_h, (nxyz(idim)-ix)/2**(j_lev-j))
                ix_l=ix_h-2*nD
                ix_l=(1-prd(idim))*MAX(MIN(ix_l,0),-ix/2**(j_lev-j)) + prd(idim)*(-nD)
                ix_h=(1-prd(idim))*MIN(MAX(ix_h,0),(nxyz(idim)-ix)/2**(j_lev-j)) + prd(idim)*(nD)

!!$              !======================================= OLD version =================================
!!$              IF       (ix_l+ix_h == 0) THEN                                        ! the middle of the domain
!!$                 ix_l = ix_l+forward_bias(meth)
!!$                 ix_h = ix_h-backward_bias(meth)
!!$              ELSE IF  (ix_l+ix_h < 0 .AND. backward_bias(meth)==1 .AND. ix_h > 0) THEN    ! the right boundary
!!$                 ix_l = MIN(MAX(ix_l-backward_bias(meth),-ix_h-nd_assym(meth),-ix/2**(j_lev-j)),ix_h-1)
!!$              ELSE IF  (ix_l+ix_h < 0 .AND. forward_bias(meth)==1 .AND. ix_h > 0) THEN  !  the right boundary
!!$                 ix_l = MIN(MAX(-ix_h+1,-ix/2**(j_lev-j)),ix_h-1)
!!$              ELSE IF  (ix_l+ix_h < 0 .AND. ix_h == 0) THEN                         ! right on the right boundary
!!$                 ix_l = MIN(MAX(ix_l,-nd_assym_bnd(meth),-ix/2**(j_lev-j)),ix_h-1)
!!$              ELSE IF  (ix_l+ix_h > 0 .AND. backward_bias(meth)==1 .AND. ix_l < 0) THEN    !  the left boundary
!!$                 ix_h = MAX(MIN(-ix_l-1,(nxyz(idim)-ix)/2**(j_lev-j)),ix_l+1)
!!$              ELSE IF  (ix_l+ix_h > 0 .AND. forward_bias(meth)==1 .AND. ix_l < 0 ) THEN !  the left boundary
!!$                 ix_h = MAX(MIN(ix_h,-ix_l+nd_assym(meth),(nxyz(idim)-ix)/2**(j_lev-j)),ix_l+1)
!!$              ELSE IF  (ix_l+ix_h > 0 .AND. ix_l == 0 ) THEN                        ! right on the left boundary
!!$                 ix_h = MAX(MIN(ix_h,nd_assym_bnd(meth),(nxyz(idim)-ix)/2**(j_lev-j)),ix_l+1)   !comment if do not want reduce the order of derivative 
!!$              END IF

!!$                 !======================== symmetric stencil - order drops close to the wall ======================= 
!!$              IF       (ix_l+ix_h == 0) THEN                                        ! the middle of the domain
!!$                 ix_l = ix_l+forward_bias(meth)
!!$                 ix_h = ix_h-backward_bias(meth)
!!$              ELSE IF  (ix_l+ix_h < 0  .AND. ix_h > 0) THEN    ! the right boundary
!!$                 ix_l = MIN(MAX(ix_l-backward_bias(meth),-ix_h-nd_assym(meth),-ix/2**(j_lev-j)),ix_h-1)
!!$                 ix_l = ix_l+forward_bias(meth)
!!$                 ix_h = ix_h-backward_bias(meth)
!!$              ELSE IF  (ix_l+ix_h < 0 .AND. ix_h == 0) THEN                         ! right on the right boundary
!!$                 ix_l = MIN(MAX(ix_l,-nd_assym_bnd(meth),-ix/2**(j_lev-j)),ix_h-1)
!!$              ELSE IF  (ix_l+ix_h > 0 .AND. ix_l < 0) THEN    !  the left boundary
!!$                 ix_h = MAX(MIN(ix_h,-ix_l+nd_assym(meth),(nxyz(idim)-ix)/2**(j_lev-j)),ix_l+1)
!!$                 ix_l = ix_l+forward_bias(meth)
!!$                 ix_h = ix_h-backward_bias(meth)
!!$               ELSE IF  (ix_l+ix_h > 0 .AND. ix_l == 0 ) THEN                        ! right on the left boundary
!!$                 ix_h = MAX(MIN(ix_h,nd_assym_bnd(meth),(nxyz(idim)-ix)/2**(j_lev-j)),ix_l+1)   !comment if do not want reduce the order of derivative 
!!$              END IF

                !======================== swapping backward_bias direction if close to the  ======================= 
                IF       (ix_l+ix_h == 0) THEN                                        ! the middle of the domain
                   ix_l = ix_l+forward_bias(meth)
                   ix_h = ix_h-backward_bias(meth)
                ELSE IF  (ix_l+ix_h < 0  .AND. ix_h > 0) THEN    ! the right boundary
                   !swapping the backward_bias <-> forward_bias 
                   ix_l = MIN(MAX(ix_l,-ix_h-nd_assym(meth),-ix/2**(j_lev-j)),ix_h-1)
!OLD                   ix_l = ix_l+backward_bias(meth) 
!OLD                   ix_h = ix_h-forward_bias(meth) 
                   ix_l = ix_l+forward_bias(meth) 
                   ix_h = ix_h-backward_bias(meth) 
                ELSE IF  (ix_l+ix_h < 0 .AND. ix_h == 0) THEN                         ! right on the right boundary
                   ix_l = MIN(MAX(ix_l,-nd_assym_bnd(meth),-ix/2**(j_lev-j)),ix_h-1)
                ELSE IF  (ix_l+ix_h > 0 .AND. ix_l < 0) THEN    !  the left boundary
                   ix_h = MAX(MIN(ix_h,-ix_l+nd_assym(meth),(nxyz(idim)-ix)/2**(j_lev-j)),ix_l+1)
                   ix_l = ix_l+forward_bias(meth)
                   ix_h = ix_h-backward_bias(meth)
                ELSE IF  (ix_l+ix_h > 0 .AND. ix_l == 0 ) THEN                        ! right on the left boundary
                   ix_h = MAX(MIN(ix_h,nd_assym_bnd(meth),(nxyz(idim)-ix)/2**(j_lev-j)),ix_l+1)   !comment if do not want reduce the order of derivative 
                END IF
                !========================================================================================
                nlvD_in(shift1(idim)+1,l,meth)=ix_l
                nlvD_in(shift1(idim)+2,l,meth)=ix_h

                !---------------------- 2-nd derivative in x-direction ---------------
                ix_l =-MIN(ix/2**(j_lev-j),nD)
                ix_h = ix_l+2*nD
                ix_h = MIN(ix_h, (nxyz(idim)-ix)/2**(j_lev-j))
                ix_l=ix_h-2*nD
                IF (ix_l > -nD) ix_h = ix_h+1
                IF (ix_h <  nD) ix_l = ix_l-1
                ix_l = (1-prd(idim))*MAX(ix_l,-ix/2**(j_lev-j)) + prd(idim)*(-nD)
                ix_h = (1-prd(idim))*MIN(ix_h,(nxyz(idim)-ix)/2**(j_lev-j)) + prd(idim)*(nD)
                IF(ix_l+ix_h<0) ix_l = MIN(MAX(ix_l,-ix_h-nd2_assym(meth)),ix_h-2)
                IF(ix_l+ix_h>0) ix_h = MAX(MIN(ix_h,-ix_l+nd2_assym(meth)),ix_l+2)
                IF(ix_l+ix_h < 0 .AND. ix_h == 0 ) ix_l = MIN(MAX(ix_l,-nd2_assym_bnd(meth),-ix/2**(j_lev-j)),ix_h-2)
                IF(ix_l+ix_h > 0 .AND. ix_l == 0 ) ix_h = MAX(MIN(ix_h,nd2_assym_bnd(meth),(nxyz(idim)-ix)/2**(j_lev-j)),ix_l+2)
                nlvD_in(shift2(idim)+1,l,meth)=ix_l
                nlvD_in(shift2(idim)+2,l,meth)=ix_h

                ! add ghost pts from min pt on left needed by 1st or 2nd deriviative to
                !  max pt on right needed by 1st or 2nd deriviative
                ixyz1 = ixyz
                DO ixpm = MIN(nlvD_in(shift1(idim)+1,l,meth),nlvD_in(shift2(idim)+1,l,meth)), &
                     MAX(nlvD_in(shift1(idim)+2,l,meth),nlvD_in(shift2(idim)+2,l,meth))
                   ixyz1(idim) = (1-prd(idim))*(ix+ixpm*2**(j_lev-j))+prd(idim)*MOD(ix+ixpm*2**(j_lev-j)+9*nxyz(idim),nxyz(idim))
                   i_d(ixyz1(1),ixyz1(2),ixyz1(3)) = .TRUE.
                END DO
             END DO
          END DO
       END DO
    END DO

  END SUBROUTINE diff_mask

  SUBROUTINE accel_size(lvxyz_odd,lvxyz_even,nxyz,mxyz,n_prdct,n_updt,prd,trnsf_type,ibc,j_lev,i_c,jd,dim)
    USE precision
    IMPLICIT NONE
    INTEGER, INTENT (IN) ::  trnsf_type,j_lev
    INTEGER, DIMENSION(0:n_wlt_fmly), INTENT (IN) ::  n_prdct,n_updt
    INTEGER , DIMENSION(3), INTENT (IN) ::  nxyz,mxyz,prd
    INTEGER , DIMENSION(6), INTENT (IN) ::  ibc
    INTEGER, DIMENSION (0:j_lev,1:3), INTENT (INOUT) :: lvxyz_odd,lvxyz_even
    INTEGER, INTENT (IN) :: jd, dim
    LOGICAL, DIMENSION (0:nxyz(1),0:nxyz(2),0:jd*nxyz(3)),  INTENT (IN) :: i_c
    INTEGER :: j
    INTEGER :: ix,ix_even,ix_odd
    INTEGER :: iy,iy_even,iy_odd
    INTEGER :: iz,iz_even,iz_odd
    INTEGER :: idim, intrnl_shft
    INTEGER, DIMENSION(3) :: ixyz
    INTEGER, DIMENSION(dim) :: face, intrnl_trnsf

    !
    !----  Setting the points for wavelet transform at different levels
    !
    lvxyz_odd(0,1)  = 0
    lvxyz_even(0,1) = 0
    lvxyz_odd(0,2)  = 0
    lvxyz_even(0,2) = 0
    lvxyz_odd(0,3)  = 0
    lvxyz_even(0,3) = 0
    DO j = 1,j_lev-1
       lvxyz_odd(j,1)  = lvxyz_odd(j-1,1)  
       lvxyz_even(j,1) = lvxyz_even(j-1,1)
       lvxyz_odd(j,2)  = lvxyz_odd(j-1,2)
       lvxyz_even(j,2) = lvxyz_even(j-1,2)
       lvxyz_odd(j,3)  = lvxyz_odd(j-1,3)
       lvxyz_even(j,3) = lvxyz_even(j-1,3)

       !
       !*********** transform in x-direction *********************
       !
       idim = 1
       DO iz=0,mxyz(3)*2**(jd*(j_lev-1))-prd(3),2**(j_lev-j-1)
          DO iy=0,mxyz(2)*2**(j_lev-1)-prd(2),2**(j_lev-j-1)
             DO ix = 1, mxyz(1)*2**(j-1)
                ix_odd = (2*ix-1)*2**(j_lev-1-j)
                IF (i_c(ix_odd,iy,iz)) THEN
                   lvxyz_odd(j,1)=lvxyz_odd(j,1)+1
                END IF
             END DO
             ixyz=(/0,iy,iz/)
             face = (-1 + 2*INT(ixyz(1:dim)/nxyz(1:dim))+ MIN(1,MOD(ixyz(1:dim),nxyz(1:dim)))) * (1 - prd(1:dim))
             intrnl_trnsf = ABS(face)
             intrnl_trnsf(idim) = 0 ! no internal transform along faces and edges
             intrnl_shft=(1-MAXVAL(intrnl_trnsf))*trnsf_type ! to use instead of ibc
             DO ix = intrnl_shft, mxyz(1)*2**(j-1)-prd(1) - intrnl_shft
                ix_even = ix*2**(j_lev-j)
                IF (i_c(ix_even,iy,iz)) THEN
                   lvxyz_even(j,1)=lvxyz_even(j,1)+1
                END IF
             END DO
          END DO
       END DO


       !
       !*********** transform in y-direction *********************
       !       
       idim = 2 
       DO iz=0,mxyz(3)*2**(jd*(j_lev-1))-prd(3),2**(j_lev-j-1)
          DO ix=0,mxyz(1)*2**(j_lev-1)-prd(1),2**(j_lev-j-1)
             DO iy = 1, mxyz(2)*2**(j-1)
                iy_odd = (2*iy-1)*2**(j_lev-1-j)
                IF (i_c(ix,iy_odd,iz)) THEN
                   lvxyz_odd(j,2)=lvxyz_odd(j,2)+1
                END IF
             END DO
             ixyz=(/ix,0,iz/)
             face = (-1 + 2*INT(ixyz(1:dim)/nxyz(1:dim))+ MIN(1,MOD(ixyz(1:dim),nxyz(1:dim)))) * (1 - prd(1:dim))
             intrnl_trnsf = ABS(face)
             intrnl_trnsf(idim) = 0 ! no internal transform along faces and edges
             intrnl_shft=(1-MAXVAL(intrnl_trnsf))*trnsf_type ! to use instead of ibc
             DO iy = intrnl_shft, mxyz(2)*2**(j-1)-prd(2) - intrnl_shft
                iy_even = iy*2**(j_lev-j)
                IF (i_c(ix,iy_even,iz)) THEN
                   lvxyz_even(j,2)=lvxyz_even(j,2)+1
                END IF
             END DO
          END DO
       END DO

       !
       !*********** transform in z-direction *********************
       ! 
       IF ( dim == 3 ) THEN !3D
          idim = 3
          DO iy=0,mxyz(2)*2**(j_lev-1)-prd(2),2**(j_lev-j-1)
             DO ix=0,mxyz(1)*2**(j_lev-1)-prd(1),2**(j_lev-j-1)
                DO iz = 1, mxyz(3)*2**(j-1)
                   iz_odd = (2*iz-1)*2**(j_lev-1-j)
                   IF (i_c(ix,iy,iz_odd)) THEN
                      lvxyz_odd(j,3)=lvxyz_odd(j,3)+1
                   END IF
                END DO
                ixyz=(/ix,iy,0/)
                face = (-1 + 2*INT(ixyz(1:dim)/nxyz(1:dim))+ MIN(1,MOD(ixyz(1:dim),nxyz(1:dim)))) * (1 - prd(1:dim))
                intrnl_trnsf = ABS(face)
                intrnl_trnsf(idim) = 0 ! no internal transform along faces and edges
                intrnl_shft=(1-MAXVAL(intrnl_trnsf))*trnsf_type ! to use instead of ibc

                DO iz = intrnl_shft, mxyz(3)*2**(j-1)-prd(3) - intrnl_shft
                   iz_even = iz*2**(j_lev-j)
                   IF (i_c(ix,iy,iz_even)) THEN
                      lvxyz_even(j,3)=lvxyz_even(j,3)+1
                   END IF
                END DO
             END DO
          END DO
       END IF

    END DO !j = 1,j_lev-1
!!$  print *,'j_lev=',j_lev
!!$  PRINT *,'lvx_odd:  ',lvx_odd
!!$  PRINT *,'lvx_even: ',lvx_even
!!$  PRINT *,'lvy_odd:  ',lvy_odd
!!$  PRINT *,'lvy_even: ',lvy_even

  END SUBROUTINE accel_size


  ! 
  SUBROUTINE accel_indices(lvxyz_odd,lvxyz_even, & 
       nlvxyz_odd,nlvxyz_even, & 
       nxyz,mxyz,n_prdct,n_updt,&
       n_assym_prdct,n_assym_updt,n_assym_prdct_bnd,n_assym_updt_bnd,& !changed
       prd,trnsf_type,ibc,j_lev,i_c,jd,dim) 
    USE precision
    IMPLICIT NONE
    INTEGER, INTENT (IN) ::  n_prdct,n_updt,trnsf_type,j_lev
    INTEGER, INTENT (IN) ::  n_assym_prdct,n_assym_updt,n_assym_prdct_bnd,n_assym_updt_bnd
    INTEGER , DIMENSION(3), INTENT (IN) ::  nxyz,mxyz,prd
    INTEGER , DIMENSION(6), INTENT (IN) ::  ibc
    INTEGER, DIMENSION (0:j_lev,1:3), INTENT (INOUT) :: lvxyz_odd,lvxyz_even
    INTEGER, DIMENSION (1:6,1:3,*), INTENT (INOUT) :: nlvxyz_odd,nlvxyz_even
    INTEGER, INTENT (IN) :: jd,dim
    LOGICAL, DIMENSION (0:nxyz(1),0:nxyz(2),0:jd*nxyz(3)),  INTENT (IN) :: i_c
    INTEGER :: i,j
    INTEGER :: ix,ix_l,ix_h,ix_even,ix_odd
    INTEGER :: iy,iy_l,iy_h,iy_even,iy_odd
    INTEGER :: iz,iz_l,iz_h,iz_even,iz_odd

    INTEGER, DIMENSION(3) :: ixyz
    INTEGER, DIMENSION(dim) :: face, intrnl_trnsf
    INTEGER :: idim, intrnl_shft

    !
    !----  Setting the points for wavelet transform at different levels
    !
    lvxyz_odd(0,1)  = 0
    lvxyz_even(0,1) = 0
    lvxyz_odd(0,2)  = 0
    lvxyz_even(0,2) = 0
    lvxyz_odd(0,3)  = 0
    lvxyz_even(0,3) = 0
    DO j = 1,j_lev-1
       lvxyz_odd(j,1)  = lvxyz_odd(j-1,1)  
       lvxyz_even(j,1) = lvxyz_even(j-1,1)
       lvxyz_odd(j,2)  = lvxyz_odd(j-1,2)
       lvxyz_even(j,2) = lvxyz_even(j-1,2)
       lvxyz_odd(j,3)  = lvxyz_odd(j-1,3)
       lvxyz_even(j,3) = lvxyz_even(j-1,3)


       !
       !*********** transform in x-direction *********************
       ! 
       idim = 1
       DO iz=0,mxyz(3)*2**(jd*(j_lev-1))-prd(3),2**(j_lev-j-1)
          DO iy=0,mxyz(2)*2**(j_lev-1)-prd(2),2**(j_lev-j-1)
             ixyz=(/0,iy,iz/)
             face = (-1 + 2*INT(ixyz(1:dim)/nxyz(1:dim))+ MIN(1,MOD(ixyz(1:dim),nxyz(1:dim)))) * (1 - prd(1:dim))
             intrnl_trnsf = ABS(face)
             intrnl_trnsf(idim) = 0 ! no internal transform along faces and edges
             intrnl_shft=(1-MAXVAL(intrnl_trnsf))*trnsf_type ! to use instead of ibc
             DO ix = 1, mxyz(1)*2**(j-1)
                ! ix is consecutive from 1 to max pts on this level
                ix_odd = (2*ix-1)*2**(j_lev-1-j) 
                ! ix_odd is the actual index of the point in the 3D field (i_c)
                IF (i_c(ix_odd,iy,iz)) THEN
                   lvxyz_odd(j,idim)=lvxyz_odd(j,idim)+1
                   i=lvxyz_odd(j,idim)
                   ix_l   = -MIN(ix-intrnl_shft,n_prdct)
                   ix_h   = ix_l+2*n_prdct-1
                   ix_h   = MIN(ix_h,mxyz(1)*2**(j-1)-ix-intrnl_shft)
                   ix_l   = ix_h-2*n_prdct+1
                   ix_l = (1-prd(1))*MAX(ix_l,-ix+intrnl_shft) + prd(1)*(-n_prdct)
                   ix_h = (1-prd(1))*MIN(ix_h,mxyz(1)*2**(j-1)-ix-intrnl_shft) + prd(1)*(n_prdct - 1)
                   IF  (ix_l+ix_h+1 < 0 .AND. ix_h >= 0 .AND. prd(1) ==0 ) THEN      ! the right boundary
                      ix_l = MAX(ix_l,-(ix_h+1)-n_assym_prdct,-ix+intrnl_shft)
                   ELSE IF  (ix_l+ix_h+1 < 0 .AND. ix_h == -1 .AND. prd(1) ==0 ) THEN!right on the right bndry
                      ix_l = MAX(ix_l,-1-n_assym_prdct_bnd,-ix+intrnl_shft)
                   ELSE IF  (ix_l+ix_h+1 > 0 .AND. ix_l < 0 .AND. prd(1) ==0 ) THEN  !  the left boundary
                      ix_h = MIN(ix_h,-ix_l-1+n_assym_prdct,mxyz(1)*2**(j-1)-ix-intrnl_shft )
                   ELSE IF  (ix_l+ix_h+1 > 0 .AND. ix_l == 0 .AND. prd(1) ==0 ) THEN !right on the left bndry
                      ix_h = MIN(ix_h,n_assym_prdct_bnd,mxyz(1)*2**(j-1)-ix-intrnl_shft)   
                   END IF
                   nlvxyz_odd(1,idim,i)=ix
                   nlvxyz_odd(2,idim,i)=iy
                   nlvxyz_odd(3,idim,i)=iz
                   nlvxyz_odd(4,idim,i)=ix_odd
                   nlvxyz_odd(5,idim,i)=ix_l
                   nlvxyz_odd(6,idim,i)=ix_h
                   !write(*,'(''nlvxyz_odd '', 7(I8, 1X ) )') idim, nlvxyz_odd(:,idim,i)
                END IF
             END DO
             DO ix = intrnl_shft, mxyz(1)*2**(j-1)-prd(1) - intrnl_shft
                ix_even = ix*2**(j_lev-j)
                IF (i_c(ix_even,iy,iz)) THEN
                   lvxyz_even(j,idim)=lvxyz_even(j,idim)+1
                   i= lvxyz_even(j,idim)
                   ix_l = - MIN (ix, n_updt)
                   ix_h = ix_l + 2*n_updt - 1
                   ix_h = MIN (ix_h, mxyz(1)*2**(j-1)-ix-1)
                   ix_l = ix_h - 2*n_updt + 1
                   ix_l = (1-prd(1))*MAX(ix_l,-ix) + prd(1)*(-n_updt)
                   ix_h = (1-prd(1))*MIN(ix_h,mxyz(1)*2**(j-1)-ix-1) + prd(1)*(n_updt - 1)
                   IF  (ix_l+ix_h+1 < 0 .AND. ix_h >= 0 .AND. prd(1) ==0 ) THEN      ! the right boundary
                      ix_l = MAX(ix_l,-(ix_h+1)-n_assym_updt,-ix)
                   ELSE IF  (ix_l+ix_h+1 < 0 .AND. ix_h == -1 .AND. prd(1) ==0 ) THEN!right on the right bndry
                      ix_l = MAX(ix_l,-1-n_assym_updt_bnd,-ix)
                   ELSE IF  (ix_l+ix_h+1 > 0 .AND. ix_l < 0 .AND. prd(1) ==0 ) THEN  !  the left boundary
                      ix_h = MIN(ix_h,-ix_l-1+n_assym_updt,mxyz(1)*2**(j-1)-ix-1)
                   ELSE IF  (ix_l+ix_h+1 > 0 .AND. ix_l == 0 .AND. prd(1) ==0 ) THEN ! right on the left bndry
                      ix_h = MIN(ix_h,n_assym_updt_bnd,mxyz(1)*2**(j-1)-ix-1)   
                   END IF
                   nlvxyz_even(1,idim,i)=ix
                   nlvxyz_even(2,idim,i)=iy
                   nlvxyz_even(3,idim,i)=iz
                   nlvxyz_even(4,idim,i)=ix_even
                   nlvxyz_even(5,idim,i)=ix_l
                   nlvxyz_even(6,idim,i)=ix_h
                   !write(*,'(''nlvxyz_even '', 7(I8, 1X ) )') idim, nlvxyz_odd(:,idim,i)
                END IF
             END DO
          END DO
       END DO

       !
       !*********** transform in y-direction *********************
       !     
       idim = 2
       DO iz=0,mxyz(3)*2**(jd*(j_lev-1))-prd(3),2**(j_lev-j-1)
          DO ix=0,mxyz(1)*2**(j_lev-1)-prd(1),2**(j_lev-j-1)
             ixyz=(/ix,0,iz/)
             face = (-1 + 2*INT(ixyz(1:dim)/nxyz(1:dim))+ MIN(1,MOD(ixyz(1:dim),nxyz(1:dim)))) * (1 - prd(1:dim))
             intrnl_trnsf = ABS(face)
             intrnl_trnsf(idim) = 0 ! no internal transform along faces and edges
             intrnl_shft=(1-MAXVAL(intrnl_trnsf))*trnsf_type ! to use instead of ibc
             DO iy = 1, mxyz(2)*2**(j-1)
                iy_odd = (2*iy-1)*2**(j_lev-1-j)
                IF (i_c(ix,iy_odd,iz)) THEN
                   lvxyz_odd(j,idim)=lvxyz_odd(j,idim)+1
                   i=lvxyz_odd(j,idim)
                   iy_l   = -MIN(iy-intrnl_shft,n_prdct)
                   iy_h   = iy_l+2*n_prdct-1
                   iy_h   = MIN(iy_h,mxyz(2)*2**(j-1)-iy-intrnl_shft)
                   iy_l   = iy_h-2*n_prdct+1
                   iy_l = (1-prd(2))*MAX(iy_l,-iy+intrnl_shft) + prd(2)*(-n_prdct)
                   iy_h = (1-prd(2))*MIN(iy_h,mxyz(2)*2**(j-1)-iy-intrnl_shft) + prd(2)*(n_prdct - 1)
                   IF  (iy_l+iy_h+1 < 0 .AND. iy_h >= 0 .AND. prd(2) ==0 ) THEN      ! the right boundary
                      iy_l = MAX(iy_l,-(iy_h+1)-n_assym_prdct,-iy+intrnl_shft)
                   ELSE IF  (iy_l+iy_h+1 < 0 .AND. iy_h == -1 .AND. prd(2) ==0 ) THEN!right on the right bndry
                      iy_l = MAX(iy_l,-1-n_assym_prdct_bnd,-iy+intrnl_shft )
                   ELSE IF  (iy_l+iy_h+1 > 0 .AND. iy_l < 0 .AND. prd(2) ==0 ) THEN  !  the left boundary
                      iy_h = MIN(iy_h,-iy_l-1+n_assym_prdct,mxyz(2)*2**(j-1)-iy-intrnl_shft )
                   ELSE IF  (iy_l+iy_h+1 > 0 .AND. iy_l == 0 .AND. prd(2) ==0 ) THEN ! right on the left bndry
                      iy_h = MIN(iy_h,n_assym_prdct_bnd,mxyz(2)*2**(j-1)-iy-intrnl_shft)   
                   END IF
                   nlvxyz_odd(1,idim,i)=ix
                   nlvxyz_odd(2,idim,i)=iy
                   nlvxyz_odd(3,idim,i)=iz
                   nlvxyz_odd(4,idim,i)=iy_odd
                   nlvxyz_odd(5,idim,i)=iy_l
                   nlvxyz_odd(6,idim,i)=iy_h
                   !write(*,'(''nlvxyz_odd '', 7(I8, 1X ) )') idim, nlvxyz_odd(:,idim,i)
                END IF
             END DO
             DO iy = intrnl_shft, mxyz(2)*2**(j-1)-prd(2) - intrnl_shft
                iy_even = iy*2**(j_lev-j)
                IF (i_c(ix,iy_even,iz)) THEN
                   lvxyz_even(j,idim)=lvxyz_even(j,idim)+1
                   i=lvxyz_even(j,idim)
                   iy_l = - MIN (iy, n_updt)
                   iy_h = iy_l + 2*n_updt - 1
                   iy_h = MIN (iy_h, mxyz(2)*2**(j-1)-iy-1)
                   iy_l = iy_h - 2*n_updt + 1
                   iy_l = (1-prd(2))*MAX(iy_l,-iy) + prd(2)*(-n_updt)
                   iy_h = (1-prd(2))*MIN(iy_h,mxyz(2)*2**(j-1)-iy-1) + prd(2)*(n_updt - 1)
                   IF  (iy_l+iy_h+1 < 0 .AND. iy_h >= 0 .AND. prd(2) ==0 ) THEN      ! the right boundary
                      iy_l = MAX(iy_l,-(iy_h+1)-n_assym_updt,-iy)
                   ELSE IF  (iy_l+iy_h+1 < 0 .AND. iy_h == -1 .AND. prd(2) ==0 ) THEN!right on the right bndry
                      iy_l = MAX(iy_l,-1-n_assym_updt_bnd,-iy)
                   ELSE IF  (iy_l+iy_h+1 > 0 .AND. iy_l < 0 .AND. prd(2) ==0 ) THEN  !  the left boundary
                      iy_h = MIN(iy_h,-iy_l-1+n_assym_updt,mxyz(2)*2**(j-1)-iy-1)
                   ELSE IF  (iy_l+iy_h+1 > 0 .AND. iy_l == 0 .AND. prd(2) ==0 ) THEN ! right on the left bndry
                      iy_h = MIN(iy_h,n_assym_updt_bnd,mxyz(2)*2**(j-1)-iy-1)   
                   END IF
                   nlvxyz_even(1,idim,i)=ix
                   nlvxyz_even(2,idim,i)=iy
                   nlvxyz_even(3,idim,i)=iz
                   nlvxyz_even(4,idim,i)=iy_even
                   nlvxyz_even(5,idim,i)=iy_l
                   nlvxyz_even(6,idim,i)=iy_h
                   !write(*,'(''nlvxyz_even '', 7(I8, 1X ) )') idim, nlvxyz_odd(:,idim,i)
                END IF
             END DO
          END DO

       END DO ! iz=0,mxyz(3)*2**(jd*(j_lev-1))-prd(3),2**(j_lev-j-1)

       !
       !*********** transform in z-direction *********************
       !  
       idim = 3
       IF( dim == 3 ) THEN 
          DO iy=0,mxyz(2)*2**(j_lev-1)-prd(2),2**(j_lev-j-1)
             DO ix=0,mxyz(1)*2**(j_lev-1)-prd(1),2**(j_lev-j-1)
                ixyz=(/ix,iy,0/)
                face = (-1 + 2*INT(ixyz(1:dim)/nxyz(1:dim))+ MIN(1,MOD(ixyz(1:dim),nxyz(1:dim)))) * (1 - prd(1:dim))
                intrnl_trnsf = ABS(face)
                intrnl_trnsf(idim) = 0 ! no internal transform along faces and edges
                intrnl_shft=(1-MAXVAL(intrnl_trnsf))*trnsf_type ! to use instead of ibc
                DO iz = 1, mxyz(3)*2**(j-1)
                   iz_odd = (2*iz-1)*2**(j_lev-1-j)
                   !PRINT *,' iz_odd = ',iz_odd 
                   IF (i_c(ix,iy,iz_odd)) THEN
                      lvxyz_odd(j,idim)=lvxyz_odd(j,idim)+1
                      i=lvxyz_odd(j,idim)
                      iz_l   = -MIN(iz-intrnl_shft,n_prdct)
                      iz_h   = iz_l+2*n_prdct-1
                      iz_h   = MIN(iz_h,mxyz(3)*2**(j-1)-iz-intrnl_shft)
                      iz_l   = iz_h-2*n_prdct+1
                      iz_l = (1-prd(3))*MAX(iz_l,-iz+intrnl_shft) + prd(3)*(-n_prdct)
                      iz_h = (1-prd(3))*MIN(iz_h,mxyz(3)*2**(j-1)-iz-intrnl_shft) + prd(3)*(n_prdct - 1)
                      IF  (iz_l+iz_h+1 < 0 .AND. iz_h >= 0 .AND. prd(3) ==0 ) THEN      ! the right boundary
                         iz_l = MAX(iz_l,-(iz_h+1)-n_assym_prdct,-iz+intrnl_shft)
                      ELSE IF  (iz_l+iz_h+1 < 0 .AND. iz_h == -1 .AND. prd(3) ==0 ) THEN!right on the right bndry
                         iz_l = MAX(iz_l,-1-n_assym_prdct_bnd,-iz+intrnl_shft )
                      ELSE IF  (iz_l+iz_h+1 > 0 .AND. iz_l < 0 .AND. prd(3) ==0 ) THEN  !  the left boundary
                         iz_h = MIN(iz_h,-iz_l-1+n_assym_prdct,mxyz(3)*2**(j-1)-iz-intrnl_shft )
                      ELSE IF  (iz_l+iz_h+1 > 0 .AND. iz_l == 0 .AND. prd(3) ==0 ) THEN ! right on the left bndry
                         iz_h = MIN(iz_h,n_assym_prdct_bnd,mxyz(3)*2**(j-1)-iz-intrnl_shft)   
                      END IF
                      nlvxyz_odd(1,idim,i)=ix
                      nlvxyz_odd(2,idim,i)=iy
                      nlvxyz_odd(3,idim,i)=iz
                      nlvxyz_odd(4,idim,i)=iz_odd
                      nlvxyz_odd(5,idim,i)=iz_l
                      nlvxyz_odd(6,idim,i)=iz_h
                      !write(*,'(''nlvxyz_odd '', 7(I8, 1X ) )') idim, nlvxyz_odd(:,idim,i)
                   END IF
                END DO
                DO iz = intrnl_shft, mxyz(3)*2**(j-1)-prd(3) - intrnl_shft
                   iz_even = iz*2**(j_lev-j)
                   IF (i_c(ix,iy,iz_even)) THEN
                      lvxyz_even(j,idim)=lvxyz_even(j,idim)+1
                      i=lvxyz_even(j,idim)
                      iz_l = - MIN (iz, n_updt)
                      iz_h = iz_l + 2*n_updt - 1
                      iz_h = MIN (iz_h, mxyz(3)*2**(j-1)-iz-1)
                      iz_l = iz_h - 2*n_updt + 1
                      iz_l = (1-prd(3))*MAX(iz_l,-iz) + prd(3)*(-n_updt)
                      iz_h = (1-prd(3))*MIN(iz_h,mxyz(3)*2**(j-1)-iz-1) + prd(3)*(n_updt - 1)
                      IF  (iz_l+iz_h+1 < 0 .AND. iz_h >= 0 .AND. prd(3) ==0 ) THEN      ! the right boundary
                         iz_l = MAX(iz_l,-(iz_h+1)-n_assym_updt,-iz)
                      ELSE IF  (iz_l+iz_h+1 < 0 .AND. iz_h == -1 .AND. prd(3) ==0 ) THEN!right on the right bndry
                         iz_l = MAX(iz_l,-1-n_assym_updt_bnd,-iz)
                      ELSE IF  (iz_l+iz_h+1 > 0 .AND. iz_l < 0 .AND. prd(3) ==0 ) THEN  !  the left boundary
                         iz_h = MIN(iz_h,-iz_l-1+n_assym_updt,mxyz(3)*2**(j-1)-iz-1)
                      ELSE IF  (iz_l+iz_h+1 > 0 .AND. iz_l == 0 .AND. prd(3) ==0 ) THEN ! right on the left bndry
                         iz_h = MIN(iz_h,n_assym_updt_bnd,mxyz(3)*2**(j-1)-iz-1)   
                      END IF
                      nlvxyz_even(1,idim,i)=ix
                      nlvxyz_even(2,idim,i)=iy
                      nlvxyz_even(3,idim,i)=iz
                      nlvxyz_even(4,idim,i)=iz_even
                      nlvxyz_even(5,idim,i)=iz_l
                      nlvxyz_even(6,idim,i)=iz_h
                      !write(*,'(''nlvxyz_even '', 7(I8, 1X ) )') idim, nlvxyz_odd(:,idim,i)
                   END IF
                END DO
             END DO
          END DO ! iy=0,mxyz(2)*2**(j_lev-1)-prd(2),2**(j_lev-j-1)
       END IF

    END DO ! j = 1,j_lev-1
!!$  print *,'j_lev=',j_lev
!!$  PRINT *,'lvx_odd:  ',lvx_odd
!!$  PRINT *,'lvx_even: ',lvx_even
!!$  PRINT *,'lvy_odd:  ',lvy_odd


  END SUBROUTINE accel_indices


  
  !
  ! do a forward wlt transform for dim dimensions at level j
  ! 
  ! 
  SUBROUTINE c_wlt_trns_aux(max_nxyz, dim , j, j_lev, nxyz,lvxyz_odd, nlvxyz_odd, lvxyz_even, &
       nlvxyz_even, nn_prdct, nn_updt,  wgh_prdct, wgh_updt, wlt_fmly, trnsf_type,  wrk ,jd, prd,  i_coef )
    USE precision
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: dim, j , j_lev, wlt_fmly, trnsf_type,i_coef , jd,max_nxyz
    INTEGER, DIMENSION (0:j_lev,1:3), INTENT (IN) :: lvxyz_odd,lvxyz_even
    INTEGER, DIMENSION (1:6,1:3,*), INTENT (IN) :: nlvxyz_odd,nlvxyz_even
    INTEGER, DIMENSION(3) , INTENT (IN) ::  nxyz
    INTEGER, DIMENSION(3) , INTENT (IN) ::  prd 
    INTEGER, DIMENSION(0:n_wlt_fmly), INTENT (IN) :: nn_prdct, nn_updt
    REAL (pr), DIMENSION ( -2*maxval_n_updt:2*maxval_n_updt  ,0:max_nxyz,j_lev-1,0:n_wlt_fmly,0:n_trnsf_type,1:3), INTENT (IN) :: wgh_updt
    REAL (pr), DIMENSION ( -2*maxval_n_prdct:2*maxval_n_prdct,0:max_nxyz,j_lev-1,0:n_wlt_fmly,0:n_trnsf_type,1:3), INTENT (IN) :: wgh_prdct
    REAL(pr) ,DIMENSION(0:nxyz(1),0:nxyz(2),0:jd*nxyz(3)), INTENT (INOUT):: wrk
    INTEGER :: ixpm,ix_l,ix_h
    INTEGER :: ixyz(3),ixyzp(3), ixyz2
    INTEGER :: idim, i
    REAL (pr) :: c_predict
!!$    REAL(pr) :: max_ug, min_ug, max_us, min_us, max_rm, max_ra, min_rm, min_ra

!!$    max_ug = 0.0_pr; max_us = 0.0_pr
!!$    min_ug = 0.0_pr; min_us = 0.0_pr
!!$    max_rm= 0.0_pr; max_ra= 0.0_pr; min_rm= 0.0_pr; min_ra= 0.0_pr;

!!$    PRINT *,'inside c_wlt_trns_aux'
    !PRINT *, wgh_updt
    !STOP '-----aa----'

    ! Do real to wlt transform
    IF( i_coef == 1 ) THEN
       !
       !*********** transform in idim -direction *********************
       !       
       DO idim = 1,dim
          !!CALL CPU_TIME (t0)
          ! do predict stage for odd points
          ! we loop through the range (i) of the index array nlvxyz_odd(1,1:3,i)  that maps to the
          ! active points at level j in the work array wrk()
          !$OMP PARALLEL DO SHARED(lvxyz_odd,nlvxyz_odd,prd,nxyz,wrk) PRIVATE(i,ixyz,ixyz2,ix_l,ix_h,ixpm,ixyzp,c_predict)
          DO i=lvxyz_odd(j-1,idim)+1,lvxyz_odd(j,idim) 
             ixyz(1)=nlvxyz_odd(1,idim,i) ! get x index in working array of point we will do predict stage
             ixyz(2)=nlvxyz_odd(2,idim,i) ! get y index in working array of point we will do predict stage
             ixyz(3)=nlvxyz_odd(3,idim,i) ! get z index in working array of point we will do predict stage
             ixyz2 = ixyz(idim)  !save the value from nlvxyz_odd(idim,idim,i) to use below
             ixyz(idim)=nlvxyz_odd(4,idim,i)!ix_odd the point in wrk ( in the curent direction idim)

!!$             PRINT *, 'trns odd  bfr', ixyz, wrk(ixyz(1),ixyz(2),ixyz(3))
             ! that we will do predict for
             ix_l=nlvxyz_odd(5,idim,i) 
             ix_h=nlvxyz_odd(6,idim,i)
             c_predict = 0.0_pr
             ixyzp = ixyz !save the original value of ixyz(:)

!!$PRINT *, ' '
!!$PRINT *, 'indx_line, idim,j',ixyz(idim), idim,j

             DO ixpm = ix_l, ix_h
                ixyzp(idim) = (ixyz2+ixpm)*2**(j_lev-j)
                ixyzp(idim) = (1-prd(idim))*ixyzp(idim)+prd(idim)*MOD(ixyzp(idim)+9*nxyz(idim),nxyz(idim))
                c_predict = c_predict + wgh_prdct(ixpm,ixyz(idim),j,wlt_fmly,trnsf_type,idim) * wrk(ixyzp(1),ixyzp(2),ixyzp(3))
!!$                PRINT *, '  GET ',ixyzp(1),ixyzp(2),ixyzp(3), wrk(ixyzp(1),ixyzp(2),ixyzp(3))

                
!!$                max_ra = MAX(max_ra,wrk(ixyzp(1),ixyzp(2),ixyzp(3)))
!!$                min_ra = MIN(min_ra,wrk(ixyzp(1),ixyzp(2),ixyzp(3)))

!!$PRINT *,'wrkixpm,j,trnsf_type,idim',ixpm,j,trnsf_type,idim
!!$WRITE(*,'("fwdodd    indx ",I3.3 ,",",I3.3,",",I3.3 ," wgh_prdct ",e15.6,", line(indx)", e15.6)')&
!!$     ixyzp(1),ixyzp(2),ixyzp(3), wgh_prdct(ixpm,ixyz(idim),j,trnsf_type,idim), wrk(ixyzp(1),ixyzp(2),ixyzp(3))
             END DO
             wrk(ixyz(1),ixyz(2),ixyz(3)) = 0.5_pr*(wrk(ixyz(1),ixyz(2),ixyz(3)) - c_predict)

!!$             IF (min_ug > wrk(ixyz(1),ixyz(2),ixyz(3))) min_ug = wrk(ixyz(1),ixyz(2),ixyz(3))
!!$             IF (max_ug < wrk(ixyz(1),ixyz(2),ixyz(3))) max_ug = wrk(ixyz(1),ixyz(2),ixyz(3))

!!$             PRINT *, 'trns odd aftr', ixyz,  wrk(ixyz(1),ixyz(2),ixyz(3)) !,c_predict
!!$             PRINT *, ' '
          END DO
          !$OMP END PARALLEL DO
          !!CALL CPU_TIME (t1)
          !!PRINT *, 'c_wlt_trns_aux fwd trns elapse time ODD,  j: ', t1-t0,  j
          !!CALL CPU_TIME (t0)
          !do update for even points
          !$OMP PARALLEL DO SHARED(lvxyz_even,nlvxyz_even,prd,nxyz,wrk) PRIVATE(i,ixyz,ixyz2,ix_l,ix_h,ixpm,ixyzp,c_predict)
          DO i=lvxyz_even(j-1,idim)+1,lvxyz_even(j,idim)
             ixyz(1)=nlvxyz_even(1,idim,i)
             ixyz(2)=nlvxyz_even(2,idim,i)
             ixyz(3)=nlvxyz_even(3,idim,i)
             ixyz2 = ixyz(idim)  !save the value from nlvxyz_even(idim,idim,i) to use below
             ixyz(idim)=nlvxyz_even(4,idim,i)!ix_even the point in wrk ( in the curent direction idim)
             ! that we will do update for
!!$             PRINT *, 'trns even  bfr', ixyz, wrk(ixyz(1),ixyz(2),ixyz(3))
             ix_l=nlvxyz_even(5,idim,i)
             ix_h=nlvxyz_even(6,idim,i)
             c_predict = 0.0_pr
             ixyzp = ixyz !save the original value of ixyz(:)
             DO ixpm = ix_l, ix_h
                ixyzp(idim) = (2*(ixyz2+ixpm)+1)*2**(j_lev-1-j)
                ixyzp(idim) = (1-prd(idim))*ixyzp(idim)+prd(idim)*MOD(ixyzp(idim)+9*nxyz(idim),nxyz(idim))
                c_predict = c_predict + wgh_updt(ixpm,ixyz(idim),j,wlt_fmly,trnsf_type,idim) * wrk(ixyzp(1),ixyzp(2),ixyzp(3))
!!$                PRINT *, '  GET ',ixyzp(1),ixyzp(2),ixyzp(3), wrk(ixyzp(1),ixyzp(2),ixyzp(3))

!!$                max_ra = MAX(max_ra,wrk(ixyzp(1),ixyzp(2),ixyzp(3)))
!!$                min_ra = MIN(min_ra,wrk(ixyzp(1),ixyzp(2),ixyzp(3)))


!!$WRITE(*,'("fwdeven    indx ",I3.3  ,",",I3.3,",",I3.3," wgh_updt ",f10.4,", line(indx)",f10.4 )')&
!!$     ixyzp(1),ixyzp(2),ixyzp(3), wgh_updt(ixpm,ixyz(idim),j,trnsf_type,idim), wrk(ixyzp(1),ixyzp(2),ixyzp(3))  

             END DO
             wrk(ixyz(1),ixyz(2),ixyz(3)) = wrk(ixyz(1),ixyz(2),ixyz(3)) + c_predict

!!$             IF (min_us > wrk(ixyz(1),ixyz(2),ixyz(3))) min_us = wrk(ixyz(1),ixyz(2),ixyz(3))
!!$             IF (max_us < wrk(ixyz(1),ixyz(2),ixyz(3))) max_us = wrk(ixyz(1),ixyz(2),ixyz(3))

!!$             PRINT *, 'trns even aftr', ixyz, wrk(ixyz(1),ixyz(2),ixyz(3)) !, c_predict
!!$             PRINT *, ' '
          END DO
          !$OMP END PARALLEL DO
          !!CALL CPU_TIME (t1)
          !!PRINT *, 'c_wlt_trns_aux fwd trns elapse time EVEN,  j: ', t1-t0,  j 
       END DO

    ELSE IF (i_coef == -1) THEN ! Do wlt to real transform
       !!CALL CPU_TIME (t0)

       DO idim = dim,1,-1
          !XX! PRINT *, ''
          !XX! PRINT *, 'wrkidim, j',idim, j
          !$OMP PARALLEL DO SHARED(lvxyz_even,nlvxyz_even,prd,nxyz,wrk) PRIVATE(i,ixyz,ixyz2,ix_l,ix_h,ixpm,ixyzp,c_predict)
          !do update for even points
          DO i=lvxyz_even(j-1,idim)+1,lvxyz_even(j,idim)
             ixyz(1)=nlvxyz_even(1,idim,i)
             ixyz(2)=nlvxyz_even(2,idim,i)
             ixyz(3)=nlvxyz_even(3,idim,i)
             ixyz2 = ixyz(idim)  !save the value from nlvxyz_even(idim,idim,i) to use below
             ixyz(idim)=nlvxyz_even(4,idim,i)!ix_even the point in wrk ( in the curent direction idim)

             !XX! PRINT *,'Do inv pt even = ', ixyz


             ! that we will do update for
             !PRINT *, 'invtrns even  bfr', ixyz, wrk(ixyz(1),ixyz(2),ixyz(3))
             ix_l=nlvxyz_even(5,idim,i)
             ix_h=nlvxyz_even(6,idim,i)
             c_predict = 0.0_pr
             ixyzp = ixyz !save the original value of ixyz(:)
             !PRINT *, 'ix_l ix_h',  ix_l,ix_h
             !PRINt *, 'ixyz=', ixyz
             DO ixpm = ix_l, ix_h
                ixyzp(idim) = (2*(ixyz2+ixpm)+1)*2**(j_lev-1-j)
                ixyzp(idim) = (1-prd(idim))*ixyzp(idim)+prd(idim)*MOD(ixyzp(idim)+9*nxyz(idim),nxyz(idim))
                c_predict = c_predict + wgh_updt(ixpm,ixyz(idim),j,wlt_fmly,trnsf_type,idim) * wrk(ixyzp(1),ixyzp(2),ixyzp(3))
                !PRINT *, ixpm,ixyz(idim),j,wlt_fmly,trnsf_type,idim
                !PRINT *, 'wgh_updt', wgh_updt(ixpm,ixyz(idim),j,wlt_fmly,trnsf_type,idim)
                !XX! WRITE(*,'("inveven    indx ", I3.3  ," wgh_updt ",f10.4,", line(indx)",f10.4 )')&
                !XX!      ixyzp(idim), wgh_updt(ixpm,ixyz(idim),j,trnsf_type,idim), wrk(ixyzp(1),ixyzp(2),ixyzp(3))  
                !IF(  wrk(ixyzp(1),ixyzp(2),ixyzp(3))  > .0000001 ) PRINT *,'XCXC'
             END DO

             !test
!!$IF( idim ==3  ) PRINT *, '%%%% c_wlt_trns_aux inv ixyz, wrk(ixyz), new wrk(ixyz) ',&
!!$     ixyz,wrk(ixyz(1),ixyz(2),ixyz(3)),  wrk(ixyz(1),ixyz(2),ixyz(3)) - c_predict 
!!$!end test

             wrk(ixyz(1),ixyz(2),ixyz(3)) = wrk(ixyz(1),ixyz(2),ixyz(3)) - c_predict
             !PRINT *, '-1-', wrk(ixyz(1),ixyz(2),ixyz(3))
             !XX! PRINT *, 'invtrns even aftr', ixyz, wrk(ixyz(1),ixyz(2),ixyz(3))
             !XX! IF(  ABS(wrk(ixyz(1),ixyz(2),ixyz(3)))  > .0000001 ) PRINT *,'XCXC'
          END DO
          !STOP '-----q-----'
          !$OMP END PARALLEL DO
          ! do predict stage for odd points
          ! we loop through the range (i) of the index array nlvxyz_odd(1,1:3,i)  that maps to the
          ! active points at level j in the work array wrk()
          !$OMP PARALLEL DO SHARED(lvxyz_odd,nlvxyz_odd,prd,nxyz,wrk) PRIVATE(i,ixyz,ixyz2,ix_l,ix_h,ixpm,ixyzp,c_predict)
          DO i=lvxyz_odd(j-1,idim)+1,lvxyz_odd(j,idim) 
             ixyz(1)=nlvxyz_odd(1,idim,i) ! get x index in working array of point we will do predict stage
             ixyz(2)=nlvxyz_odd(2,idim,i) ! get y index in working array of point we will do predict stage
             ixyz(3)=nlvxyz_odd(3,idim,i) ! get z index in working array of point we will do predict stage
             ixyz2 = ixyz(idim)  !save the value from nlvxyz_odd(idim,idim,i) to use below
             ixyz(idim)=nlvxyz_odd(4,idim,i)!ix_odd the point in wrk ( in the curent direction idim)
             ! that we will do predict for
             !PRINT *, 'invtrns odd  bfr', ixyz, wrk(ixyz(1),ixyz(2),ixyz(3))
             ix_l=nlvxyz_odd(5,idim,i) 
             ix_h=nlvxyz_odd(6,idim,i)
             c_predict = 0.0_pr
             ixyzp = ixyz !save the original value of ixyz(:)
             DO ixpm = ix_l, ix_h
                ixyzp(idim) = (ixyz2+ixpm)*2**(j_lev-j)
                ixyzp(idim) = (1-prd(idim))*ixyzp(idim)+prd(idim)*MOD(ixyzp(idim)+9*nxyz(idim),nxyz(idim))
                c_predict = c_predict + wgh_prdct(ixpm,ixyz(idim),j,wlt_fmly,trnsf_type,idim) * wrk(ixyzp(1),ixyzp(2),ixyzp(3))

                !XX! WRITE(*,'("invodd    indx ",I3.3 ,",",I3.3,",",I3.3 ," wgh_prdct ",f10.4,", line(indx)",f10.4 )')&
                !XX!      ixyzp(1),ixyzp(2),ixyzp(3), wgh_prdct(ixpm,ixyz(idim),j,trnsf_type,idim), wrk(ixyzp(1),ixyzp(2),ixyzp(3))
             END DO

             !PRINT *, '%%%% c_wlt_trns_aux inv ixyz, wrk(ixyz), new wrk(ixyz) ',&
             !    ixyz,wrk(ixyz(1),ixyz(2),ixyz(3)),  2.0_pr*wrk(ixyz(1),ixyz(2),ixyz(3)) + c_predict
             !test
!!$IF( idim ==3  ) PRINT *, '%%%% c_wlt_trns_aux inv ixyz, wrk(ixyz), new wrk(ixyz) ',&
!!$     ixyz,wrk(ixyz(1),ixyz(2),ixyz(3)), 2.0_pr*wrk(ixyz(1),ixyz(2),ixyz(3)) + c_predict 
             !end test

             wrk(ixyz(1),ixyz(2),ixyz(3)) = 2.0_pr*wrk(ixyz(1),ixyz(2),ixyz(3)) + c_predict
             !PRINT *, '-2-', wrk(ixyz(1),ixyz(2),ixyz(3))
             !XX! PRINT *, 'invtrns odd aftr', ixyz, wrk(ixyz(1),ixyz(2),ixyz(3))
             !XX! IF(  ABS(wrk(ixyz(1),ixyz(2),ixyz(3)))  > .0000001 ) PRINT *,'XCXC'
          END DO
          !$OMP END PARALLEL DO

       END DO
       !! CALL CPU_TIME (t1)
       !!PRINT *, 'c_wlt_trns_aux inv trns elapse time,  j: ', t1-t0,  j 

       !STOP '---a---'

    ELSE
       print *,'ERROR, c_wlt_trns_aux() called with i_coeff = ', i_coef
       print *,'ERROR  Correct values are 1 -fwd trns or -1 inv trns. Exiting ....'
       stop
    ENDIF
    !PRINT *,'LEAVING Entering c_wlt_trns_aux'
!!$    PRINT *,'  inside c_wlt_trns_aux: flag=',i_coef
!!$    IF (i_coef < 0) THEN
!!$       PRINT *, '  min/max update_inv  =', min_us, max_us
!!$       PRINT *, '  min/max predict_inv =', min_ug, max_ug
!!$    ELSE
!!$       PRINT *, '  min/max predict =', min_ug, max_ug
!!$       PRINT *, '  min/max update  =', min_us, max_us
!!$       PRINT *, '   (prdct) GET main min/max = ', min_rm, max_rm
!!$       PRINT *, '   (prdct) GET aux  min/max = ', min_ra, max_ra
!!$    END IF

  END SUBROUTINE c_wlt_trns_aux

  !
  !**************************************************************************
  ! trnsf_type = 0 - regular transform
  ! trnsf_type = 1 - internal transform and along boundaries 
  !
  SUBROUTINE wlt_projectionUNUSED(u, j_in, j_out, nlocal, wlt_fmly, trnsf_type)
    USE precision
    USE sizes
    USE share_consts
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: nlocal, j_in, j_out, wlt_fmly, trnsf_type
    REAL (pr), DIMENSION (1:nlocal), INTENT (INOUT) :: u
    REAL (pr) :: c_predict, mean
    INTEGER :: i,j,kk
    INTEGER :: ix,ix_even,ix_odd,ixp,ixpm,ix_l,ix_h
    INTEGER :: iy,iy_even,iy_odd,iyp,iypm,iy_l,iy_h
    INTEGER :: iz
    INTEGER :: ibndr
    INTEGER :: ishift,jmin,jmax, idim



    !  PRINT *,'BEG:',', MAXval=',MAXVAL(ABS(wrk))

    DO i=1,lv_intrnl(j_in)
       wrk(indx(i,1),indx(i,2),indx(i,3)) = u(i)
    END DO
    DO ibndr=1,2*dim
       ishift = lv_bnd(2,ibndr,j_in)-lv_bnd(0,ibndr,1)
       DO i=lv_bnd(0,ibndr,1),lv_bnd(1,ibndr,j_in)
          wrk(indx(i,1),indx(i,2),indx(i,3)) = u(i+ishift)
       END DO
    END DO

    jmin = 1
    jmax = MAX(j_in,j_out)
    !
    !---------- forward transform -----------------------------
    !
    DO j = j_in-1, jmin, -1


       !
       !*********** transform in x-direction *********************
       !    
       idim = 1
       DO i=lvxyz_odd(j-1,idim,wlt_fmly,trnsf_type)+1,lvxyz_odd(j,idim,wlt_fmly,trnsf_type)
          ix=nlvxyz_odd(1,idim,i,wlt_fmly,trnsf_type)
          iy=nlvxyz_odd(2,idim,i,wlt_fmly,trnsf_type)
          iz=nlvxyz_odd(3,idim,i,wlt_fmly,trnsf_type)
          ix_odd=nlvxyz_odd(4,idim,i,wlt_fmly,trnsf_type)
          ix_l=nlvxyz_odd(5,idim,i,wlt_fmly,trnsf_type)
          ix_h=nlvxyz_odd(6,idim,i,wlt_fmly,trnsf_type)
          c_predict = 0.0_pr
          DO ixpm = ix_l, ix_h
             ixp = (ix+ixpm)*2**(j_lev-j)
             ixp = (1-prd(idim))*ixp+prd(idim)*MOD(ixp+3*nxyz(idim),nxyz(idim))
             c_predict = c_predict + wgh_prdct(ixpm,ix_odd,j,wlt_fmly,trnsf_type,idim) * wrk(ixp,iy,iz)
          END DO
          wrk(ix_odd,iy,iz) = 0.5_pr*(wrk(ix_odd,iy,iz) - c_predict)
       END DO
       DO i=lvxyz_even(j-1,idim,wlt_fmly,trnsf_type)+1,lvxyz_even(j,idim,wlt_fmly,trnsf_type)
          ix=nlvxyz_even(1,idim,i,wlt_fmly,trnsf_type)
          iy=nlvxyz_even(2,idim,i,wlt_fmly,trnsf_type)
          iz=nlvxyz_even(3,idim,i,wlt_fmly,trnsf_type)
          ix_even=nlvxyz_even(4,idim,i,wlt_fmly,trnsf_type)
          ix_l=nlvxyz_even(5,idim,i,wlt_fmly,trnsf_type)
          ix_h=nlvxyz_even(6,idim,i,wlt_fmly,trnsf_type)
          c_predict = 0.0_pr
          DO ixpm = ix_l, ix_h
             ixp = (2*(ix+ixpm)+1)*2**(j_lev-1-j)
             ixp = (1-prd(idim))*ixp+prd(idim)*MOD(ixp+3*nxyz(idim),nxyz(idim))
             c_predict = c_predict + wgh_updt(ixpm,ix_even,j,wlt_fmly,trnsf_type,idim) * wrk(ixp,iy,iz)
          END DO
          wrk(ix_even,iy,iz) = wrk(ix_even,iy,iz) + c_predict
       END DO
       !
       !*********** transform in y-direction *********************
       !        
       idim = 2
       DO i=lvxyz_odd(j-1,idim,wlt_fmly,trnsf_type)+1,lvxyz_odd(j,idim,wlt_fmly,trnsf_type)
          ix=nlvxyz_odd(1,idim,i,wlt_fmly,trnsf_type)
          iy=nlvxyz_odd(2,idim,i,wlt_fmly,trnsf_type)
          iz=nlvxyz_odd(3,idim,i,wlt_fmly,trnsf_type)
          iy_odd=nlvxyz_odd(4,idim,i,wlt_fmly,trnsf_type)
          iy_l=nlvxyz_odd(5,idim,i,wlt_fmly,trnsf_type)
          iy_h=nlvxyz_odd(6,idim,i,wlt_fmly,trnsf_type)
          c_predict = 0.0_pr
          DO iypm = iy_l, iy_h
             iyp = (iy+iypm)*2**(j_lev-j)
             iyp = (1-prd(idim))*iyp+prd(idim)*MOD(iyp+3*nxyz(idim),nxyz(idim))
             c_predict = c_predict + wgh_prdct(iypm,iy_odd,j,wlt_fmly,trnsf_type,idim) * wrk(ix,iyp,iz)
          END DO
          wrk(ix,iy_odd,iz) = 0.5_pr*(wrk(ix,iy_odd,iz) - c_predict)
       END DO
       DO i=lvxyz_even(j-1,idim,wlt_fmly,trnsf_type)+1,lvxyz_even(j,idim,wlt_fmly,trnsf_type)
          ix=nlvxyz_even(1,idim,i,wlt_fmly,trnsf_type)
          iy=nlvxyz_even(2,idim,i,wlt_fmly,trnsf_type)
          iz=nlvxyz_even(3,idim,i,wlt_fmly,trnsf_type)
          iy_even=nlvxyz_even(4,idim,i,wlt_fmly,trnsf_type)
          iy_l=nlvxyz_even(5,idim,i,wlt_fmly,trnsf_type)
          iy_h=nlvxyz_even(6,idim,i,wlt_fmly,trnsf_type)
          c_predict = 0.0_pr
          DO iypm = iy_l, iy_h
             iyp = (2*(iy+iypm)+1)*2**(j_lev-1-j)
             iyp = (1-prd(idim))*iyp+prd(idim)*MOD(iyp+3*nxyz(idim),nxyz(idim))
             c_predict = c_predict + wgh_updt(iypm,iy_even,j,wlt_fmly,trnsf_type,idim) * wrk(ix,iyp,iz)
          END DO
          wrk(ix,iy_even,iz) = wrk(ix,iy_even,iz) + c_predict
       END DO
    END DO
    !---------------- Setting to zero the mean 
    !DG  Why do you make mean zero?
    mean = 0.0_pr
    kk = 0
    DO iy = trnsf_type,nxyz(2)-prd(2)-trnsf_type,2**(j_lev-jmin)
       DO ix = trnsf_type,nxyz(1)-prd(1)-trnsf_type,2**(j_lev-jmin)
          kk = kk +1
          mean = mean + wrk(ix,iy,iz)
       END DO
    END DO
    mean = mean/REAL(kk,pr)
    DO iy = trnsf_type,nxyz(2)-prd(2)-trnsf_type,2**(j_lev-jmin)
       DO ix = trnsf_type,nxyz(1)-prd(1)-trnsf_type,2**(j_lev-jmin)
          wrk(ix,iy,iz) = wrk(ix,iy,iz) - mean
       END DO
    END DO

    !
    !----------------  inverse transform ----------------------
    !
    DO j = jmin, j_out-1

       !
       !*********** transform in y-direction *********************
       !        
       idim = 2
       DO i=lvxyz_even(j-1,idim,wlt_fmly,trnsf_type)+1,lvxyz_even(j,idim,wlt_fmly,trnsf_type)
          ix=nlvxyz_even(1,idim,i,wlt_fmly,trnsf_type)
          iy=nlvxyz_even(2,idim,i,wlt_fmly,trnsf_type)
          iz=nlvxyz_even(3,idim,i,wlt_fmly,trnsf_type)
          iy_even=nlvxyz_even(4,idim,i,wlt_fmly,trnsf_type)
          iy_l=nlvxyz_even(5,idim,i,wlt_fmly,trnsf_type)
          iy_h=nlvxyz_even(6,idim,i,wlt_fmly,trnsf_type)
          c_predict = 0.0_pr
          DO iypm = iy_l, iy_h
             iyp = (2*(iy+iypm)+1)*2**(j_lev-1-j)
             iyp = (1-prd(idim))*iyp+prd(idim)*MOD(iyp+3*nxyz(idim),nxyz(idim))
             c_predict = c_predict + wgh_updt(iypm,iy_even,j,wlt_fmly,trnsf_type,idim) * wrk(ix,iyp,iz)
          END DO
          wrk(ix,iy_even,iz) = wrk(ix,iy_even,iz) - c_predict
       END DO
       DO i=lvxyz_odd(j-1,idim,wlt_fmly,trnsf_type)+1,lvxyz_odd(j,idim,wlt_fmly,trnsf_type)
          ix=nlvxyz_odd(1,idim,i,wlt_fmly,trnsf_type)
          iy=nlvxyz_odd(2,idim,i,wlt_fmly,trnsf_type)
          iz=nlvxyz_odd(3,idim,i,wlt_fmly,trnsf_type)
          iy_odd=nlvxyz_odd(4,idim,i,wlt_fmly,trnsf_type)
          iy_l=nlvxyz_odd(5,idim,i,wlt_fmly,trnsf_type)
          iy_h=nlvxyz_odd(6,idim,i,wlt_fmly,trnsf_type)
          c_predict = 0.0_pr
          DO iypm = iy_l, iy_h
             iyp = (iy+iypm)*2**(j_lev-j)
             iyp = (1-prd(idim))*iyp+prd(idim)*MOD(iyp+3*nxyz(idim),nxyz(idim))
             c_predict = c_predict + wgh_prdct(iypm,iy_odd,j,wlt_fmly,trnsf_type,idim) * wrk(ix,iyp,iz)
          END DO
          wrk(ix,iy_odd,iz) = 2.0_pr*wrk(ix,iy_odd,iz) + c_predict
       END DO
       !
       !*********** transform in x-direction *********************
       !        
       idim = 1
       DO i=lvxyz_even(j-1,idim,wlt_fmly,trnsf_type)+1,lvxyz_even(j,idim,wlt_fmly,trnsf_type)
          ix=nlvxyz_even(1,idim,i,wlt_fmly,trnsf_type)
          iy=nlvxyz_even(2,idim,i,wlt_fmly,trnsf_type)
          iz=nlvxyz_even(3,idim,i,wlt_fmly,trnsf_type)
          ix_even=nlvxyz_even(4,idim,i,wlt_fmly,trnsf_type)
          ix_l=nlvxyz_even(5,idim,i,wlt_fmly,trnsf_type)
          ix_h=nlvxyz_even(6,idim,i,wlt_fmly,trnsf_type)
          c_predict = 0.0_pr
          DO ixpm = ix_l, ix_h
             ixp = (2*(ix+ixpm)+1)*2**(j_lev-1-j)
             ixp = (1-prd(idim))*ixp+prd(idim)*MOD(ixp+3*nxyz(idim),nxyz(idim))
             c_predict = c_predict +  wgh_updt(ixpm,ix_even,j,wlt_fmly,trnsf_type,idim) * wrk(ixp,iy,iz)
          END DO
          wrk(ix_even,iy,iz) = wrk(ix_even,iy,iz) - c_predict
       END DO
       DO i=lvxyz_odd(j-1,idim,wlt_fmly,trnsf_type)+1,lvxyz_odd(j,idim,wlt_fmly,trnsf_type)
          ix=nlvxyz_odd(1,i,idim,wlt_fmly,trnsf_type)
          iy=nlvxyz_odd(2,i,idim,wlt_fmly,trnsf_type)
          iz=nlvxyz_odd(3,i,idim,wlt_fmly,trnsf_type)
          ix_odd=nlvxyz_odd(4,i,idim,wlt_fmly,trnsf_type)
          ix_l=nlvxyz_odd(5,i,idim,wlt_fmly,trnsf_type)
          ix_h=nlvxyz_odd(6,i,idim,wlt_fmly,trnsf_type)
          c_predict = 0.0_pr
          DO ixpm = ix_l, ix_h
             ixp = (ix+ixpm)*2**(j_lev-j)
             ixp = (1-prd(idim))*ixp+prd(idim)*MOD(ixp+3*nxyz(idim),nxyz(idim))
             c_predict = c_predict + wgh_prdct(ixpm,ix_odd,j,wlt_fmly,trnsf_type,idim) * wrk(ixp,iy,iz)
          END DO
          wrk(ix_odd,iy,iz) = 2.0_pr*wrk(ix_odd,iy,iz) + c_predict
       END DO
    END DO

    DO i=1,lv_intrnl(j_out)
       u(i) = wrk(indx(i,1),indx(i,2),indx(i,3)) 
    END DO
    DO ibndr=1,2*dim
       ishift = lv_bnd(2,ibndr,j_out)-lv_bnd(0,ibndr,1)
       DO i=lv_bnd(0,ibndr,1),lv_bnd(1,ibndr,j_out)
          u(i+ishift) = wrk(indx(i,1),indx(i,2),indx(i,3))  
       END DO
    END DO

    !
    !******************** Cleaning wrk ***************************************
    !
    DO i=1,lv_intrnl(jmax)
       wrk(indx(i,1),indx(i,2),indx(i,3)) = 0.0_pr
    END DO
    DO ibndr=1,2*dim
       DO i=lv_bnd(0,ibndr,1),lv_bnd(1,ibndr,jmax)
          wrk(indx(i,1),indx(i,2),indx(i,3)) = 0.0_pr
       END DO
    END DO
    !**************************************************************************
  END SUBROUTINE wlt_projectionUNUSED

  !
  !**************************************************************************
  ! trnsf_type = 0 - regular transform
  ! trnsf_type = 1 - internal transform and along boundaries  
  !
  SUBROUTINE wlt_interpolate(u, ie,  mn_var,mx_var, j_in, j_out, nlocal, wlt_fmly, trnsf_type)
    USE precision
    USE sizes
    USE share_consts
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: nlocal, j_in, j_out, wlt_fmly, trnsf_type,ie, mn_var,mx_var
    REAL (pr), DIMENSION (1:nlocal,ie), INTENT (INOUT) :: u
    INTEGER :: i,j
    INTEGER :: ibndr
    INTEGER :: ishift,jmin,jmax
    INTEGER :: ii ! component being worked on

!!$    PRINT *,'j_in/j_out=',j_in, j_out,'nlocal=',nlocal

    DO ii = mn_var,mx_var
       DO i=1,lv_intrnl(j_in)
          wrk(indx(i,1),indx(i,2),indx(i,3)) = u(i,ii)
       END DO
       DO ibndr=1,2*dim
          ishift = lv_bnd(2,ibndr,j_in)-lv_bnd(0,ibndr,1)
          DO i=lv_bnd(0,ibndr,1),lv_bnd(1,ibndr,j_in)
             wrk(indx(i,1),indx(i,2),indx(i,3)) = u(i+ishift,ii)
          END DO
       END DO

       jmin = MIN(j_in,j_out)
       jmax = MAX(j_in,j_out)

!!$       PRINT *,'1: MIN/MAXVAL(u)=', MINVAL(wrk), MAXVAL(wrk)

       !
       !---------- forward transform -----------------------------
       !
       DO j = j_in-1, jmin, -1
          call c_wlt_trns_aux(MAXVAL(nxyz), dim , j, j_lev, &
               nxyz,lvxyz_odd(:,:,wlt_fmly,trnsf_type), nlvxyz_odd(:,:,:,wlt_fmly,trnsf_type), &
               lvxyz_even(:,:,wlt_fmly,trnsf_type), nlvxyz_even(:,:,:,wlt_fmly,trnsf_type), &
               n_prdct , n_updt , wgh_prdct, wgh_updt, wlt_fmly, trnsf_type,  wrk ,jd, prd, WLT_TRNS_FWD )
       END DO

!!$       PRINT *,'2: MIN/MAXVAL(u)=', MINVAL(wrk), MAXVAL(wrk)
       !
       !----------------  inverse transform ----------------------
       !
       DO j = jmin, j_out-1
          call c_wlt_trns_aux(MAXVAL(nxyz), dim , j, j_lev, &
               nxyz,lvxyz_odd(:,:,wlt_fmly,trnsf_type), nlvxyz_odd(:,:,:,wlt_fmly,trnsf_type), &
               lvxyz_even(:,:,wlt_fmly,trnsf_type), nlvxyz_even(:,:,:,wlt_fmly,trnsf_type), &
               n_prdct , n_updt , wgh_prdct, wgh_updt, wlt_fmly, trnsf_type,  wrk ,jd, prd, WLT_TRNS_INV )
       END DO

!!$       PRINT *,'3: MIN/MAXVAL(u)=', MINVAL(wrk), MAXVAL(wrk)

       DO i=1,lv_intrnl(j_out)
          u(i,ii) = wrk(indx(i,1),indx(i,2),indx(i,3)) 
       END DO
       DO ibndr=1,2*dim
          ishift = lv_bnd(2,ibndr,j_out)-lv_bnd(0,ibndr,1)
          DO i=lv_bnd(0,ibndr,1),lv_bnd(1,ibndr,j_out)
             u(i+ishift,ii) = wrk(indx(i,1),indx(i,2),indx(i,3))  
          END DO
       END DO

       !
       !******************** Cleaning wrk ***************************************
       !
       DO i=1,lv_intrnl(jmax)
          wrk(indx(i,1),indx(i,2),indx(i,3)) = 0.0_pr
       END DO
       DO ibndr=1,2*dim
          DO i=lv_bnd(0,ibndr,1),lv_bnd(1,ibndr,jmax)
             wrk(indx(i,1),indx(i,2),indx(i,3)) = 0.0_pr
          END DO
       END DO
       !**************************************************************************
    END DO

!!$    PRINT *,'3.5: MIN/MAXVAL(u)=', MINVAL(u), MAXVAL(u)
!!$    PAUSE 'leaving wlt_interpolate'

  END SUBROUTINE wlt_interpolate


  !**************************************************************************
  ! Version of  wavelet transform used to do an inverse transform 
  ! from the adapted grid to the whole grid, thus interpolating to a regular grid.
  !**************************************************************************
  !**************************************************************************
  ! Begin wavelet transform subroutines
  ! This routine does an inverse wlt tranform from the c's on the
  ! adapted grid to the full field
  !**************************************************************************
  SUBROUTINE c_wlt_trns_interp (u_out, c,  nxyz_out, nwlt, j_in, j_out, wlt_fmly, leps) 
    USE precision
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: nwlt,  j_in, j_out, wlt_fmly, nxyz_out(1:3)
    REAL (pr), DIMENSION (1:nwlt), INTENT (IN) :: c
    REAL (pr), DIMENSION (0:nxyz_out(1)+1,0:nxyz_out(2),0:nxyz_out(3)), INTENT (INOUT) :: u_out
    REAL (pr), INTENT (IN) :: leps
    REAL (pr) :: c_predict
    INTEGER :: i,j,inum,idenom
    INTEGER :: ix,ix_even,ix_odd,ixp,ixpm,ix_l,ix_h
    INTEGER :: iy,iy_even,iy_odd,iyp,iypm,iy_l,iy_h
    INTEGER :: iz,iz_even,iz_odd,izp,izpm,iz_l,iz_h

    jd = 0
    IF(dim == 3) jd = 1

    u_out = 0.0_pr
    inum=1
    idenom=1
    IF(j_out > j_in) inum=2**(j_out-j_in)
    IF(j_out < j_in) idenom=2**(j_in-j_out)
    iz = 0
    !!PRINT *,'in wlt_trns_interp'
    !!PRINT *,'j_in, j_out ',j_in, j_out
    !!PRINT *,'inum , idenom ', inum , idenom
    !!PRINT *,'nwlt ', nwlt
    !!PRINT *,'nxyz_out ', nxyz_out
    DO i=1,nwlt
       ix = indx(i,1)
       iy = indx(i,2)
       IF(dim == 3) iz = indx(i,3)
       IF (MOD(ix,idenom)==0 .AND. MOD(iy,idenom)==0 .AND. ABS(c(i)) >= leps ) THEN
          !!PRINT *, i, ix*inum/idenom,iy*inum/idenom,iz*inum/idenom
          u_out(ix*inum/idenom,iy*inum/idenom,iz*inum/idenom) = c(i)
       END IF
    END DO

    DO j = 1, j_out-1

       IF( dim == 3 ) THEN 
          DO iy=0,mxyz(2)*2**(j_out-1)-prd(2),2**(j_out-j-1)
             DO ix=0,mxyz(1)*2**(j_out-1)-prd(1),2**(j_out-j-1)
                DO iz = 0, mxyz(3)*2**(j-1)-prd(3)
                   iz_even = iz*2**(j_out-j)
                   c_predict = 0.0_pr
                   iz_l = - MIN (iz, n_updt(wlt_fmly))
                   iz_h = iz_l + 2*n_updt(wlt_fmly) - 1
                   iz_h = MIN (iz_h, mxyz(3)*2**(j-1)-iz-1)
                   iz_l = iz_h - 2*n_updt(wlt_fmly) + 1
                   iz_l = (1-prd(3))*MAX(iz_l,-iz) + prd(3)*(-n_updt(wlt_fmly))
                   iz_h = (1-prd(3))*MIN(iz_h,mxyz(3)*2**(j-1)-iz-1) + prd(3)*(n_updt(wlt_fmly) - 1)
                   IF  (iz_l+iz_h+1 < 0 .AND. iz_h >= 0 .AND. prd(3) == 0) THEN                               ! the right boundary
                      iz_l = MAX(iz_l,-(iz_h+1)-n_assym_updt(wlt_fmly,0),-iz)
                   ELSE IF  (iz_l+iz_h+1 < 0 .AND. iz_h == -1 .AND. prd(3) == 0) THEN                         ! right on the right boundary
                      iz_l = MAX(iz_l,-1-n_assym_updt_bnd(wlt_fmly,0),-iz)
                   ELSE IF  (iz_l+iz_h+1 > 0 .AND. iz_l < 0 .AND. prd(3) == 0) THEN                          !  the left boundary
                      iz_h = MIN(iz_h,-iz_l-1+n_assym_updt(wlt_fmly,0),mxyz(3)*2**(j-1)-iz-1)
                   ELSE IF  (iz_l+iz_h+1 > 0 .AND. iz_l == 0 .AND. prd(3) == 0) THEN                         ! right on the left boundary
                      iz_h = MIN(iz_h,n_assym_updt_bnd(wlt_fmly,0),mxyz(3)*2**(j-1)-iz-1)   
                   END IF
                   DO izpm = iz_l, iz_h
                      izp = (2*(iz+izpm)+1)*2**(j_out-1-j)
                      izp = (1-prd(3))*izp+prd(3)*MOD(izp+3*nxyz_out(3),nxyz_out(3))
                      c_predict = c_predict + wgh_updt_interp(izpm,iz_even,j,wlt_fmly,1,3) * u_out(ix,iy,izp)
                   END DO
                   u_out(ix,iy,iz_even) = u_out(ix,iy,iz_even) - c_predict
                END DO
                DO iz = 1, mxyz(3)*2**(j-1)
                   iz_odd = (2*iz-1)*2**(j_out-1-j)
                   iz_l   = -MIN(iz,n_prdct(wlt_fmly))
                   iz_h   = iz_l+2*n_prdct(wlt_fmly)-1
                   iz_h   = MIN(iz_h,mxyz(3)*2**(j-1)-iz)
                   iz_l   = iz_h-2*n_prdct(wlt_fmly)+1
                   iz_l = (1-prd(3))*MAX(iz_l,-iz) + prd(3)*(-n_prdct(wlt_fmly))
                   iz_h = (1-prd(3))*MIN(iz_h,mxyz(3)*2**(j-1)-iz) + prd(3)*(n_prdct(wlt_fmly) - 1)
                   IF  (iz_l+iz_h+1 < 0 .AND. iz_h >= 0 .AND. prd(3) ==0 ) THEN                             ! the right boundary
                      iz_l = MAX(iz_l,-(iz_h+1)-n_assym_prdct(wlt_fmly,0),-iz)
                   ELSE IF  (iz_l+iz_h+1 < 0 .AND. iz_h == -1 .AND. prd(3) ==0 ) THEN                       ! right on the right boundary
                      iz_l = MAX(iz_l,-1-n_assym_prdct_bnd(wlt_fmly,0),-iz )
                   ELSE IF  (iz_l+iz_h+1 > 0 .AND. iz_l < 0 .AND. prd(3) ==0 ) THEN                         !  the left boundary
                      iz_h = MIN(iz_h,-iz_l-1+n_assym_prdct(wlt_fmly,0),mxyz(3)*2**(j-1)-iz )
                   ELSE IF  (iz_l+iz_h+1 > 0 .AND. iz_l == 0 .AND. prd(3) ==0 ) THEN                        ! right on the left boundary
                      iz_h = MIN(iz_h,n_assym_prdct_bnd(wlt_fmly,0),mxyz(3)*2**(j-1)-iz)   
                   END IF
                   c_predict = 0.0_pr
                   DO izpm = iz_l, iz_h
                      izp = (iz+izpm)*2**(j_out-j)
                      izp = (1-prd(3))*izp+prd(3)*MOD(izp+3*nxyz_out(3),nxyz_out(3))
                      c_predict = c_predict + wgh_prdct_interp(izpm,iz_odd,j,wlt_fmly,1,3) * u_out(ix,iy,izp)
                   END DO
                   u_out(ix,iy,iz_odd) = 2.0_pr*u_out(ix,iy,iz_odd) + c_predict

                END DO
             END DO
          END DO
       END IF
       !
       !*********** transform in y-direction *********************
       !        
       DO iz=0,mxyz(3)*2**(jd*(j_out-1))-prd(3),2**(j_out-j-1)
          DO ix=0,mxyz(1)*2**(j_out-1)-prd(1),2**(j_out-j-1)
             DO iy = 0, mxyz(2)*2**(j-1)-prd(2)
                iy_even = iy*2**(j_out-j)
                c_predict = 0.0_pr
                iy_l = - MIN (iy, n_updt(wlt_fmly))
                iy_h = iy_l + 2*n_updt(wlt_fmly) - 1
                iy_h = MIN (iy_h, mxyz(2)*2**(j-1)-iy-1)
                iy_l = iy_h - 2*n_updt(wlt_fmly) + 1
                iy_l = (1-prd(2))*MAX(iy_l,-iy) + prd(2)*(-n_updt(wlt_fmly))
                iy_h = (1-prd(2))*MIN(iy_h,mxyz(2)*2**(j-1)-iy-1) + prd(2)*(n_updt(wlt_fmly) - 1)
                IF  (iy_l+iy_h+1 < 0 .AND. iy_h >= 0 .AND. prd(2) ==0) THEN                               ! the right boundary
                   iy_l = MAX(iy_l,-(iy_h+1)-n_assym_updt(wlt_fmly,0),-iy)
                ELSE IF  (iy_l+iy_h+1 < 0 .AND. iy_h == -1 .AND. prd(2) ==0) THEN                         ! right on the right boundary
                   iy_l = MAX(iy_l,-1-n_assym_updt_bnd(wlt_fmly,0),-iy)
                ELSE IF  (iy_l+iy_h+1 > 0 .AND. iy_l < 0  .AND. prd(2) ==0) THEN                          !  the left boundary
                   iy_h = MIN(iy_h,-iy_l-1+n_assym_updt(wlt_fmly,0),mxyz(2)*2**(j-1)-iy-1)
                ELSE IF  (iy_l+iy_h+1 > 0 .AND. iy_l == 0  .AND. prd(2) ==0) THEN                         ! right on the left boundary
                   iy_h = MIN(iy_h,n_assym_updt_bnd(wlt_fmly,0),mxyz(2)*2**(j-1)-iy-1)   
                END IF
                DO iypm = iy_l, iy_h
                   iyp = (2*(iy+iypm)+1)*2**(j_out-1-j)
                   iyp = (1-prd(2))*iyp+prd(2)*MOD(iyp+3*nxyz_out(2),nxyz_out(2))
                   c_predict = c_predict + wgh_updt_interp(iypm,iy_even,j,wlt_fmly,1,2) * u_out(ix,iyp,iz)
                END DO
                u_out(ix,iy_even,iz) = u_out(ix,iy_even,iz) - c_predict
             END DO
             DO iy = 1, mxyz(2)*2**(j-1)
                iy_odd = (2*iy-1)*2**(j_out-1-j)
                iy_l   = -MIN(iy,n_prdct(wlt_fmly))
                iy_h   = iy_l+2*n_prdct(wlt_fmly)-1
                iy_h   = MIN(iy_h,mxyz(2)*2**(j-1)-iy)
                iy_l   = iy_h-2*n_prdct(wlt_fmly)+1
                iy_l = (1-prd(2))*MAX(iy_l,-iy) + prd(2)*(-n_prdct(wlt_fmly))
                iy_h = (1-prd(2))*MIN(iy_h,mxyz(2)*2**(j-1)-iy) + prd(2)*(n_prdct(wlt_fmly) - 1)
                IF  (iy_l+iy_h+1 < 0 .AND. iy_h >= 0 .AND. prd(2) ==0 ) THEN                             ! the right boundary
                   iy_l = MAX(iy_l,-(iy_h+1)-n_assym_prdct(wlt_fmly,0),-iy)
                ELSE IF  (iy_l+iy_h+1 < 0 .AND. iy_h == -1 .AND. prd(2) ==0 ) THEN                       ! right on the right boundary
                   iy_l = MAX(iy_l,-1-n_assym_prdct_bnd(wlt_fmly,0),-iy )
                ELSE IF  (iy_l+iy_h+1 > 0 .AND. iy_l < 0 .AND. prd(2) ==0 ) THEN                         !  the left boundary
                   iy_h = MIN(iy_h,-iy_l-1+n_assym_prdct(wlt_fmly,0),mxyz(2)*2**(j-1)-iy )
                ELSE IF  (iy_l+iy_h+1 > 0 .AND. iy_l == 0 .AND. prd(2) ==0 ) THEN                        ! right on the left boundary
                   iy_h = MIN(iy_h,n_assym_prdct_bnd(wlt_fmly,0),mxyz(2)*2**(j-1)-iy)   
                END IF
                c_predict = 0.0_pr
                DO iypm = iy_l, iy_h
                   iyp = (iy+iypm)*2**(j_out-j)
                   iyp = (1-prd(2))*iyp+prd(2)*MOD(iyp+3*nxyz_out(2),nxyz_out(2))
                   c_predict = c_predict + wgh_prdct_interp(iypm,iy_odd,j,wlt_fmly,1,2) * u_out(ix,iyp,iz)
                END DO
                u_out(ix,iy_odd,iz) = 2.0_pr*u_out(ix,iy_odd,iz) + c_predict
             END DO
          END DO
       END DO
       !
       !*********** transform in x-direction *********************
       !        
       DO iz=0,mxyz(3)*2**(jd*(j_out-1))-prd(3),2**(j_out-j-1)
          DO iy=0,mxyz(2)*2**(j_out-1)-prd(2),2**(j_out-j-1)
             DO ix = 0, mxyz(1)*2**(j-1)-prd(1)
                ix_even = ix*2**(j_out-j)
                c_predict = 0.0_pr
                ix_l = - MIN (ix, n_updt(wlt_fmly))
                ix_h = ix_l + 2*n_updt(wlt_fmly) - 1
                ix_h = MIN (ix_h, mxyz(1)*2**(j-1)-ix-1)
                ix_l = ix_h - 2*n_updt(wlt_fmly) + 1
                ix_l = (1-prd(1))*MAX(ix_l,-ix) + prd(1)*(-n_updt(wlt_fmly))
                ix_h = (1-prd(1))*MIN(ix_h,mxyz(1)*2**(j-1)-ix-1) + prd(1)*(n_updt(wlt_fmly) - 1)
                IF  (ix_l+ix_h+1 < 0 .AND. ix_h >= 0 .AND. prd(1) == 0) THEN                               ! the right boundary
                   ix_l = MAX(ix_l,-(ix_h+1)-n_assym_updt(wlt_fmly,0),-ix)
                ELSE IF  (ix_l+ix_h+1 < 0 .AND. ix_h == -1 .AND. prd(1) == 0) THEN                         ! right on the right boundary
                   ix_l = MAX(ix_l,-1-n_assym_updt_bnd(wlt_fmly,0),-ix)
                ELSE IF  (ix_l+ix_h+1 > 0 .AND. ix_l < 0 .AND. prd(1) == 0) THEN                          !  the left boundary
                   ix_h = MIN(ix_h,-ix_l-1+n_assym_updt(wlt_fmly,0),mxyz(1)*2**(j-1)-ix-1)
                ELSE IF  (ix_l+ix_h+1 > 0 .AND. ix_l == 0 .AND. prd(1) == 0) THEN                         ! right on the left boundary
                   ix_h = MIN(ix_h,n_assym_updt_bnd(wlt_fmly,0),mxyz(1)*2**(j-1)-ix-1)   
                END IF
                DO ixpm = ix_l, ix_h
                   ixp = (2*(ix+ixpm)+1)*2**(j_out-1-j)
                   ixp = (1-prd(1))*ixp+prd(1)*MOD(ixp+3*nxyz_out(1),nxyz_out(1))
                   c_predict = c_predict + wgh_updt_interp(ixpm,ix_even,j,wlt_fmly,1,1) * u_out(ixp,iy,iz)
                END DO
                u_out(ix_even,iy,iz) = u_out(ix_even,iy,iz) - c_predict
             END DO
             DO ix = 1, mxyz(1)*2**(j-1)
                ix_odd = (2*ix-1)*2**(j_out-1-j)
                ix_l   = -MIN(ix,n_prdct(wlt_fmly))
                ix_h   = ix_l+2*n_prdct(wlt_fmly)-1
                ix_h   = MIN(ix_h,mxyz(1)*2**(j-1)-ix)
                ix_l   = ix_h-2*n_prdct(wlt_fmly)+1
                ix_l = (1-prd(1))*MAX(ix_l,-ix) + prd(1)*(-n_prdct(wlt_fmly))
                ix_h = (1-prd(1))*MIN(ix_h,mxyz(1)*2**(j-1)-ix) + prd(1)*(n_prdct(wlt_fmly) - 1)
                IF  (ix_l+ix_h+1 < 0 .AND. ix_h >= 0 .AND. prd(1) ==0 ) THEN                             ! the right boundary
                   ix_l = MAX(ix_l,-(ix_h+1)-n_assym_prdct(wlt_fmly,0),-ix)
                ELSE IF  (ix_l+ix_h+1 < 0 .AND. ix_h == -1 .AND. prd(1) ==0 ) THEN                       ! right on the right boundary
                   ix_l = MAX(ix_l,-1-n_assym_prdct_bnd(wlt_fmly,0),-ix )
                ELSE IF  (ix_l+ix_h+1 > 0 .AND. ix_l < 0 .AND. prd(1) ==0 ) THEN                         !  the left boundary
                   ix_h = MIN(ix_h,-ix_l-1+n_assym_prdct(wlt_fmly,0),mxyz(1)*2**(j-1)-ix )
                ELSE IF  (ix_l+ix_h+1 > 0 .AND. ix_l == 0 .AND. prd(1) ==0 ) THEN                        ! right on the left boundary
                   ix_h = MIN(ix_h,n_assym_prdct_bnd(wlt_fmly,0),mxyz(1)*2**(j-1)-ix)   
                END IF
                c_predict = 0.0_pr
                DO ixpm = ix_l, ix_h
                   ixp = (ix+ixpm)*2**(j_out-j)
                   ixp = (1-prd(1))*ixp+prd(1)*MOD(ixp+3*nxyz_out(1),nxyz_out(1))
                   c_predict = c_predict + wgh_prdct_interp(ixpm,ix_odd,j,wlt_fmly,1,1) * u_out(ixp,iy,iz)
                END DO
                u_out(ix_odd,iy,iz) = 2.0_pr*u_out(ix_odd,iy,iz) + c_predict
             END DO
          END DO
       END DO
    END DO


    IF( prd(1) == 1 ) u_out(nxyz_out(1),:,:)=u_out(0,:,:);
    IF( prd(2) == 1 ) u_out(:,nxyz_out(2),:)=u_out(:,0,:); 
    IF( prd(3) == 1 ) u_out(:,:,nxyz_out(3))=u_out(:,:,0); 

  END SUBROUTINE c_wlt_trns_interp

  !
  !-------- set weights for wavelet transform
  !
  SUBROUTINE c_wlt_trns_interp_setup(j_out, nxyz_out, max_nxyz_out, xx_out )
    USE precision
    IMPLICIT NONE
    INTEGER , INTENT(IN) :: j_out, nxyz_out(1:3), max_nxyz_out
    REAL (pr) , INTENT(IN) ::  xx_out(0:max_nxyz_out,1:dim)
    INTEGER :: wlt_fmly
    INTEGER :: i, k, ii, j, ix, ix_l, ix_h , ix_odd, ix_even, idim, ixpm

    !------------ arrays used for indx_DB --------------------
    INTEGER :: j_df, wlt_type, face_type
    INTEGER :: face_dim, face_val
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(0:dim) :: i_p, i_p_face
    LOGICAL, DIMENSION(:,:), ALLOCATABLE :: face_assigned
    !---------------------------------------------------------

    !------------ copied from indices, IREG=-1
    !Creating lv_intrnl, lv_bnd, & indx from indx_DB
    i_p(0) = 1
    i_p_face(0) = 1
    DO i=1,dim
       i_p(i) = i_p(i-1)*(1+nxyz(i))
       i_p_face(i) = i_p_face(i-1)*3
    END DO
    IF(ALLOCATED(lv_intrnl)) DEALLOCATE(lv_intrnl)
    ALLOCATE(lv_intrnl(0:j_lev))
    face = 0 
    face_type = SUM((face+1)*i_p_face(0:dim-1))
    lv_intrnl(0) = 0
    DO j = 1, j_lev
       lv_intrnl(j) = lv_intrnl(j-1)
       DO wlt_type = MIN(j-1,1),2**dim-1
          DO j_df = j, j_lev
             lv_intrnl(j) = lv_intrnl(j) + indx_DB(j_df,wlt_type,face_type,j)%length  
          END DO
       END DO
    END DO
    nwlt = lv_intrnl(j_lev)
    IF(ALLOCATED(lv_bnd)) DEALLOCATE(lv_bnd) !changed
    ALLOCATE(lv_bnd(0:3,1:2*dim,0:j_lev)) !changed
    IF(ALLOCATED(face_assigned)) DEALLOCATE(face_assigned)
    ALLOCATE(face_assigned(0:3**dim-1,1:j_mx))
    face_assigned = .FALSE.
    face_assigned((3**dim-1)/2,:) = .TRUE. !internal points
    IF( MINVAL(prd(1:dim)) == 0 ) THEN !non-periodic at least in one direction
       DO i=1,2*dim
          ibnd(i) = i
          face_dim = 0
          IF(ibnd(i) == 1 .AND. prd(1) == 0) THEN 
             !left  boundary (xmin)
             face_dim = 1
             face_val =-1
          ELSE IF(ibnd(i) == 2 .AND. prd(1) == 0) THEN 
             !right boundary (xmax)
             face_dim = 1
             face_val = 1
          ELSE IF(ibnd(i) == 3 .AND. prd(2) == 0) THEN 
             !bottom boundary (ymin)
             face_dim = 2
             face_val =-1
          ELSE IF(ibnd(i) == 4 .AND. prd(2) == 0) THEN 
             !top boundary (ymax)
             face_dim = 2
             face_val = 1
          ELSE IF(dim == 3 .and. ibnd(i) == 5 .AND. prd(3) == 0) THEN 
             !bottom boundary (zmin)
             face_dim = 3
             face_val =-1
          ELSE IF(dim == 3 .and. ibnd(i) == 6 .AND. prd(3) == 0) THEN 
             !top boundary (zmax)
             face_dim = 3
             face_val = 1
          END IF
          IF(face_dim /= 0) THEN
             DO j=1,j_lev  !--- the following do-loop changed to set up indicies for multilevel calculations.
                lv_bnd(0,i,j) = nwlt+1 
                lv_bnd(1,i,j) = nwlt   
                DO face_type = 0, 3**dim - 1
                   face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
                   IF( face(face_dim) == face_val .AND. .NOT.face_assigned(face_type,j) ) THEN
                      face_assigned(face_type,j) = .TRUE.
                      DO wlt_type = MIN(j-1,1),2**dim-1
                         DO j_df = j, j_lev
                            nwlt = nwlt + indx_DB(j_df,wlt_type,face_type,j)%length
                            lv_bnd(1,i,j) = lv_bnd(1,i,j) + indx_DB(j_df,wlt_type,face_type,j)%length
                         END DO
                      END DO
                   END IF
                END DO
             END DO
          END IF
       END DO
    END IF
	nwlt_global = nwlt
    DEALLOCATE(face_assigned)
    ! update the lv_bnd(2:3, , ) 
    ! The 2:3 indices are for mappign the bnd points to the end of a lower level 
    ! in the flat u array. Used in diff_aux()
    DO j=1,j_lev
       i = 1
       lv_bnd(2,i,j) = lv_intrnl(j) + 1
       lv_bnd(3,i,j) = lv_bnd(2,i,j) + lv_bnd(1,i,j)-lv_bnd(0,i,1)
       DO i=2,2*dim
          lv_bnd(2,i,j) = lv_bnd(3,i-1,j) + 1
          lv_bnd(3,i,j) = lv_bnd(2,i,j) + lv_bnd(1,i,j)-lv_bnd(0,i,1)
       END DO
    END DO
    IF(ALLOCATED(indx)) DEALLOCATE(indx)
    ALLOCATE(indx(1:nwlt,1:3))
    DO j = 1, j_lev
       DO wlt_type = MIN(j-1,1),2**dim-1
          DO face_type = 0, 3**dim - 1
             DO j_df = j, j_lev
                DO k = 1, indx_DB(j_df,wlt_type,face_type,j)%length  
                   i = indx_DB(j_df,wlt_type,face_type,j)%p(k)%i
                   indx(i,1:dim) = INT(MOD(indx_DB(j_df,wlt_type,face_type,j)%p(k)%ixyz-1,i_p(1:dim))/i_p(0:dim-1))
                END DO
             END DO
          END DO
       END DO
    END DO

    !-------------------------------------------------------------

    IF( ALLOCATED(wgh_updt_interp) ) DEALLOCATE(wgh_updt_interp)
    IF( ALLOCATED(wgh_prdct_interp) ) DEALLOCATE(wgh_prdct_interp)
    ALLOCATE( wgh_updt_interp(-2*MAXVAL(n_updt):2*MAXVAL(n_updt),0:MAXVAL(nxyz_out),j_out-1,0:n_wlt_fmly,1,1:3))
    wgh_updt=0.0_pr
    ALLOCATE( wgh_prdct_interp(-2*MAXVAL(n_prdct):2*MAXVAL(n_prdct),0:MAXVAL(nxyz_out),j_out-1,0:n_wlt_fmly,1,1:3) )   
    !--Initialize arrays
    wgh_updt_interp  = 0.0_pr 
    wgh_prdct_interp = 0.0_pr

    DO wlt_fmly = 0, n_wlt_fmly
       DO j = 1, j_out-1
          DO idim =1,dim
             DO ix = 1, mxyz(idim)*2**(j-1)
                ix_odd = (2*ix-1)*2**(j_out-1-j)
                ix_l   = -MIN(ix,n_prdct(wlt_fmly))
                ix_h   = ix_l+2*n_prdct(wlt_fmly)-1
                ix_h   = MIN(ix_h,mxyz(idim)*2**(j-1)-ix)
                ix_l   = ix_h-2*n_prdct(wlt_fmly)+1
                ix_l = (1-prd(idim))*MAX(ix_l,-ix) + prd(idim)*(-n_prdct(wlt_fmly))
                ix_h = (1-prd(idim))*MIN(ix_h,mxyz(idim)*2**(j-1)-ix) + prd(idim)*(n_prdct(wlt_fmly) - 1)
                IF  (ix_l+ix_h+1 < 0 .AND. ix_h >= 0 .AND. prd(idim) ==0 ) THEN! the right boundary
                   ix_l = MAX(ix_l,-(ix_h+1)-n_assym_prdct(wlt_fmly,0),-ix)
                ELSE IF  (ix_l+ix_h+1 < 0 .AND. ix_h == -1 .AND. prd(idim) ==0 ) THEN! right on the right boundary
                   ix_l = MAX(ix_l,-1-n_assym_prdct_bnd(wlt_fmly,0),-ix )
                ELSE IF  (ix_l+ix_h+1 > 0 .AND. ix_l < 0 .AND. prd(idim) ==0 ) THEN!  the left boundary
                   ix_h = MIN(ix_h,-ix_l-1+n_assym_prdct(wlt_fmly,0),mxyz(idim)*2**(j-1)-ix)
                ELSE IF  (ix_l+ix_h+1 > 0 .AND. ix_l == 0 .AND. prd(idim) ==0 ) THEN! right on the left boundary
                   ix_h = MIN(ix_h,n_assym_prdct_bnd(wlt_fmly,0),mxyz(idim)*2**(j-1)-ix)   
                END IF
                DO ixpm = ix_l, ix_h
                   wgh_prdct_interp(ixpm,ix_odd,j,wlt_fmly,1,idim)= &
                        wgh (ixpm, ix_odd, ix_l, ix_h, 2**(j_out-1-j), prd(idim), &
                        nxyz_out(idim), xx_out(0:nxyz_out(idim),idim))
                END DO
             END DO
             DO ix = 0, mxyz(idim)*2**(j-1)-prd(idim) 
                ix_even = ix*2**(j_out-j)
                ix_l = - MIN (ix, n_updt(wlt_fmly))
                ix_h = ix_l + 2*n_updt(wlt_fmly) - 1
                ix_h = MIN (ix_h, mxyz(idim)*2**(j-1)-ix-1)
                ix_l = ix_h - 2*n_updt(wlt_fmly) + 1
                ix_l = (1-prd(idim))*MAX(ix_l,-ix) + prd(idim)*(-n_updt(wlt_fmly))
                ix_h = (1-prd(idim))*MIN(ix_h,mxyz(idim)*2**(j-1)-ix-1) + prd(idim)*(n_updt(wlt_fmly) - 1)
                IF  (ix_l+ix_h+1 < 0 .AND. ix_h >= 0 .AND. prd(idim) == 0) THEN                               ! the right boundary
                   ix_l = MAX(ix_l,-(ix_h+1)-n_assym_updt(wlt_fmly,0),-ix)
                ELSE IF  (ix_l+ix_h+1 < 0 .AND. ix_h == -1 .AND. prd(idim) == 0) THEN                        ! right on the right boundary
                   ix_l = MAX(ix_l,-1-n_assym_updt_bnd(wlt_fmly,0),-ix)
                ELSE IF  (ix_l+ix_h+1 > 0 .AND. ix_l < 0 .AND. prd(idim) == 0) THEN                          !  the left boundary
                   ix_h = MIN(ix_h,-ix_l-1+n_assym_updt(wlt_fmly,0),mxyz(idim)*2**(j-1)-ix-1)
                ELSE IF  (ix_l+ix_h+1 > 0 .AND. ix_l == 0 .AND. prd(idim) == 0) THEN                         ! right on the left boundary
                   ix_h = MIN(ix_h,n_assym_updt_bnd(wlt_fmly,0),mxyz(idim)*2**(j-1)-ix-1)   
                END IF
                DO ixpm = ix_l, ix_h
                   wgh_updt_interp(ixpm,ix_even,j,wlt_fmly,1,idim) = &
                        wgh (ixpm, ix_even, ix_l, ix_h, 2**(j_out-1-j), prd(idim), nxyz_out(idim), xx_out(0:nxyz_out(idim),idim))
                END DO
             END DO
          END DO
       END DO
    END DO


  END SUBROUTINE c_wlt_trns_interp_setup

  !free weights arrays for wavelet interpolation
  SUBROUTINE c_wlt_trns_interp_free()
    USE precision
    IMPLICIT NONE
    DEALLOCATE (wgh_updt_interp, wgh_prdct_interp)

  END SUBROUTINE c_wlt_trns_interp_free


  !***********************************************************
  !* Do grid adaptation 
  ! 
  ! arguments
  ! flag  -  -1 = restart, 0 = in startup code, 1 in main loop
  !***********************************************************
  SUBROUTINE adapt_grid( flag ,  leps , j_lev_old , j_lev_a, j_mn, j_mx, nxyz_a, ij_adj,&
       adj_type, scl ,new_grid)
    USE precision
    USE io_3d_vars
    USE share_consts
    USE pde
    USE sizes
    USE field
    USE debug_vars
    USE variable_mapping
    IMPLICIT NONE
    INTEGER , INTENT (IN)     :: j_mn, j_mx
    LOGICAL , INTENT (INOUT) :: new_grid
    INTEGER , INTENT (INOUT ) :: j_lev_a , j_lev_old
    INTEGER , INTENT (IN)     :: flag  
    INTEGER , INTENT (INOUT ) , DIMENSION(3) :: nxyz_a
    INTEGER, DIMENSION(-1:1)  , INTENT (IN) :: ij_adj, adj_type
    REAL (pr) , INTENT (IN)   :: leps 
    REAL (pr) , INTENT (IN)   :: scl(1:n_var)
    INTEGER :: wlt_fmly, trnsf_type
    !  LOGICAL :: startup ! true is we are inthe startup part of the code.
    LOGICAL :: mask(1:n_var) ! 
    INTEGER :: ie

    !***********************************************************
    !* Start grid adaptation criteria
    !***********************************************************
    
    
    CALL timer_start(3)
    
    
    IF ( BTEST(debug_level,2) ) THEN
       PRINT *, ' '
       PRINT *, 'ADAPT_GRID: j_lev=',j_lev,'j_lev_a=',j_lev_a,'j_lev_old=',j_lev_old
       PRINT *, 'nwlt=',nwlt
       PRINT *, 'n_var_adapt=',n_var_adapt
    END IF
    i_c_init = .TRUE.
    DO ie = 1, n_var
       IF(n_var_adapt(ie,flag)) THEN 
          CALL max_level (u_old(1:nwlt,n_var_index(ie)), &
               scl(ie)*leps, nwlt, j_lev, j_lev_a, j_mx, j_mn)
       END IF
    END DO
    PRINT *, 'in max_level after the cycle: j_lev_a=',j_lev_a
    j_lev_a = MAX (j_lev_a, j_mn-1) ! We set to j_mn-1 because a adjacent level is needed 1 above, added 5/22/03 DanG
    j_lev_a = MIN (j_lev_a+1, j_mx, j_mx_adj)
    IF (j_lev_a <= j_lev .AND. j_lev_a < j_additional_nodes) THEN
       j_lev_a = MIN( j_lev+1, j_additional_nodes, j_mx, j_mx_adj)
    END IF

    i_c_init = .TRUE. ! tell significant_wlt() to redefine new grid for i_c if necessary
    
    IF ( BTEST(debug_level,2) ) THEN
       WRITE (*,'(" ")')
       WRITE (*,'("==================== after max_level ==================")')
       WRITE (*,'("j_lev=",I3)') j_lev
       WRITE (*,'("j_lev_a=",I3)') j_lev_a
       WRITE (*,'("j_lev_old=",I3)') j_lev_old
       WRITE (*,'("MAXVAL(ABS(u_old)) = ", f30.20 )')  MAXVAL(ABS(u_old)) 
       WRITE (*,'("=======================================================")')
       WRITE (*,'(" ")')
    END IF
    
    DO ie = 1, n_var 
       IF(n_var_adapt(ie,flag)) THEN

          PRINT * , 'here: ie=',ie
          IF (wlog) WRITE (6,'("Calling significant_wlt for eqn ", I4,", ", I12," active wlts" )')ie, COUNT(i_c)
          CALL significant_wlt (u_old(1:nwlt,n_var_index(ie)), &
               scl(ie)*leps, nwlt, &
               j_lev, j_lev_a, j_mx, j_mn, nxyz_a)
          IF (wlog) WRITE (6,'("After significant_wlt there are now, ", I12," active wlts" )') COUNT(i_c)
       END IF
    END DO

    IF ( BTEST(debug_level,2) ) THEN
       WRITE (*,'(" ")')
       WRITE (*,'("==================== after significant_wlt ============")')
       WRITE (*,'("# significant wlts = ", I12)') COUNT(i_c)
       WRITE (*,'("j_lev              = ", I6)') j_lev
       WRITE (*,'("j_lev_a            = ", I6)') j_lev_a
       WRITE (*,'("j_lev_old            = ", I6)') j_lev_old
       WRITE (*,'("=======================================================")')
       WRITE (*,'(" ")')
    END IF
    
    DO ie = 1, n_var
       IF(n_var_adapt(ie,flag)) THEN 
          CALL adjacent_wlt (u_old(1:nwlt,n_var_index(ie)), &
               scl(ie)*leps, nwlt, &
               ij_adj, adj_type, j_lev, j_lev_a, j_mn, j_mx, ie,COUNT( n_var_adapt(1:n_var,flag)) )
          IF (wlog) WRITE (6,'("After adjacent_wlt for eqn ", I4,", ", I12," active wlts" )')ie, COUNT(i_c)
       END IF
    END DO

    IF ( BTEST(debug_level,2) ) THEN
       WRITE (*,'(" ")')
       WRITE (*,'("=============== after adjacent_wlt ====================")')
       WRITE (*,'("# significant+adjacent wlts = ", I12)') COUNT(i_c)
       WRITE (*,'("j_lev                       = ", I6)') j_lev
       WRITE (*,'("j_lev_a                     = ", I6)') j_lev_a
       WRITE (*,'("j_lev_old                   = ", I6)') j_lev_old
       WRITE (*,'("MAXVAL(ABS(u_old)) = ", f30.20 )')  MAXVAL(ABS(u_old))
       WRITE (*,'("=======================================================")')
       WRITE (*,'(" ")')
    END IF
!!$    PAUSE 'in adapt_grid after adjacent_wlt'
    
    CALL add_nodes (i_c,  nxyz_a, j_lev_a)

    IF ( BTEST(debug_level,2) ) THEN
       WRITE (*,'(" ")')
       WRITE (*,'("================== after add_nodes ====================")')
       WRITE (*,'("# wlts    = ", I12)') COUNT(i_c)
       WRITE (*,'("=======================================================")')
       WRITE (*,'(" ")')
    END IF
    
    IF(BNDzone) THEN
       CALL bnd_zone(i_c,  nxyz_a, j_lev_a) 
       
       IF ( BTEST(debug_level,2) ) THEN
          WRITE (*,'(" ")')
          WRITE (*,'("================= after bnd_zone ======================")')
          WRITE (*,'("# wlts    = ", I12)') COUNT(i_c)
          WRITE (*,'("j_lev_a   = ", I6)') j_lev_a
          WRITE (*,'("=======================================================")')
          WRITE (*,'(" ")')
       END IF
    END IF
    !***********************************************************
    !* End grid adaptation criteria
    !***********************************************************
    
    !***********************************************************
    !* Special Grid adaptation criteria within zone
    !***********************************************************
    
    CALL zone_wlt (i_c,  nxyz_a, j_lev_a, j_zn)
    
    IF ( BTEST(debug_level,2) ) THEN
       WRITE (*,'(" ")')
       WRITE (*,'("=============== after zone_wlt ========================")')
       WRITE (*,'("# wlts    = ", I12)') COUNT(i_c)
       WRITE (*,'("j_lev_new = ", I6)') j_lev_a
       WRITE (*,'("=======================================================")')
       WRITE (*,'(" ")')
    END IF
    
    !
    !------------- Performing a reconstruction check  ----------
    PRINT *, shape(i_c)
    
    DO trnsf_type =0,n_trnsf_type
       DO wlt_fmly = 0, n_wlt_fmly   
          CALL reconstr_check (i_c, wlt_fmly, trnsf_type, nxyz_a, j_lev_a)
          IF (wlog) PRINT *,'wlt_fmly=',wlt_fmly,'trnsf_type=',trnsf_type
          IF (wlog) WRITE (6,'("After reconstr_check , ", I12," active wlts" )') COUNT(i_c)
       END DO
    END DO
    
    IF( BTEST(debug_level,2) ) THEN
       WRITE (*,'(" ")')
       WRITE (*,'("=========== after reconstr_check ======================")')
       WRITE (*,'("# wlts    = ", I12)') COUNT(i_c)
       WRITE (*,'("j_lev     = ", I6)') j_lev
       WRITE (*,'("j_lev_a   = ", I6)') j_lev_a
       WRITE (*,'("j_lev_old   = ", I6)') j_lev_old
       WRITE (*,'("=======================================================")')
       WRITE (*,'(" ")')
    END IF
    
    ! Note must call indices again with last arg 1 to initialize deriviatives
    ! for elliptic solver (analytic solution). This needs to be fixed to work
    ! in both cases.
    
    IF( BTEST(debug_level,2) ) PRINT *, 'COUNT(i_c), COUNT(i_d) = ', COUNT(i_c), COUNT(i_d)

    CALL indices (nwlt_old, nwlt, nwltj_old, nwltj, new_grid, j_lev_old, j_lev_a, j_mn, j_mx, 1)
    
    IF( BTEST(debug_level,2) ) THEN
       WRITE (*,'(" ")')
       WRITE (*,'("==================== after indices(set ghost) =========")')
       PRINT *, 'j_lev    =',j_lev
       PRINT *, 'j_lev_a  =',j_lev_a
       PRINT *, 'j_lev_old=',j_lev_old
       PRINT *, 'new_grid =',new_grid
       PRINT *, 'nwlt_old=',nwlt_old,'nwlt=',nwlt
       WRITE (*,'(" ")')
       WRITE (*,'("# wlts+ghost = ", I12)') COUNT(i_d)
       WRITE (*,'("j_lev_a      = ", I6)') j_lev_a
       WRITE (*,'("j_lev_old      = ", I6)') j_lev_old
       WRITE (*,'("=======================================================")')
       WRITE (*,'(" ")')
    END IF
    
    !  IF( .NOT. startup ) THEN
    DO trnsf_type = 0,n_trnsf_type
       DO wlt_fmly = 0, n_wlt_fmly   
          CALL accel_size(lvxyz_odd(:,:,wlt_fmly,trnsf_type),lvxyz_even(:,:,wlt_fmly,trnsf_type), &
               nxyz,mxyz,n_prdct,n_updt,prd,trnsf_type,ibc,j_lev,i_c,jd,dim)
          !Print *, 'after accel_size ,   nxyz,nwlt, j_lev, j_lev_a, j_mx = ',   nxyz,nwlt, j_lev, j_lev_a, j_mx!DG TEST
       END DO
    END DO
    DEALLOCATE(nlvxyz_odd,nlvxyz_even)
    ALLOCATE(  nlvxyz_odd(1:6,1:3,1:MAXVAL(lvxyz_odd (j_lev-1,1:3,0:n_wlt_fmly,0:n_trnsf_type)),0:n_wlt_fmly,0:n_trnsf_type) )
    ALLOCATE( nlvxyz_even(1:6,1:3,1:MAXVAL(lvxyz_even(j_lev-1,1:3,0:n_wlt_fmly,0:n_trnsf_type)),0:n_wlt_fmly,0:n_trnsf_type) )
    DO trnsf_type = 0,n_trnsf_type
       DO wlt_fmly = 0, n_wlt_fmly   
          CALL accel_indices(lvxyz_odd(:,:,wlt_fmly,trnsf_type),lvxyz_even(:,:,wlt_fmly,trnsf_type), &
               nlvxyz_odd(:,:,:,wlt_fmly,trnsf_type),nlvxyz_even(:,:,:,wlt_fmly,trnsf_type), &
               nxyz,mxyz,n_prdct(wlt_fmly),n_updt(wlt_fmly),&
               n_assym_prdct(wlt_fmly,trnsf_type),n_assym_updt(wlt_fmly,trnsf_type),n_assym_prdct_bnd(wlt_fmly,trnsf_type),n_assym_updt_bnd(wlt_fmly,trnsf_type), &
               prd,trnsf_type,ibc,j_lev,i_c,jd,dim)
       END DO
    END DO
    !

    WRITE (6,'("Before reconstruction check on ghost points, COUNT(i_d) = ", I10.10 )' ) COUNT(i_d)     
    DO trnsf_type =0, 0
       DO wlt_fmly = 0, n_wlt_fmly   
          CALL reconstr_check (i_d, wlt_fmly, trnsf_type, nxyz_a, j_lev_a)
       END DO
    END DO
    WRITE (6,'("After  reconstruction check on ghost points, COUNT(i_d)  = ", I10.10 )' ) COUNT(i_d)
    
    IF( BTEST(debug_level,2) ) THEN
       WRITE (*,'(" ")')
       WRITE (*,'("========== after reconstr_check (on ghost pts) ========")')
       WRITE (*,'("# wlts    = ", I12)') COUNT(i_c)
       WRITE (*,'("j_lev      = ", I6)') j_lev
       WRITE (*,'("j_lev_a    = ", I6)') j_lev_a
       WRITE (*,'("j_lev_old  = ", I6)') j_lev_old
       WRITE (*,'("=======================================================")')
       WRITE (*,'(" ")')
    END IF
    
    DO wlt_fmly = 0, n_wlt_fmly   
       CALL accel_size(lvdxyz_odd(:,:,wlt_fmly),lvdxyz_even(:,:,wlt_fmly),&
            nxyz,mxyz,n_prdct,n_updt,prd,0,ibc,j_lev,i_d,jd,dim)
    END DO
    DEALLOCATE(nlvdxyz_odd,nlvdxyz_even)
    ALLOCATE( nlvdxyz_odd(1:6,1:3,1:MAXVAL(lvdxyz_odd(j_lev-1,1:3,0:n_wlt_fmly)),0:n_wlt_fmly) )
    ALLOCATE( nlvdxyz_even(1:6,1:3,1:MAXVAL(lvdxyz_even(j_lev-1,1:3,0:n_wlt_fmly)),0:n_wlt_fmly) )
    
    DO trnsf_type = 0,0 !becuse derivatives are only using trnsf_type = 0
       DO wlt_fmly = 0, n_wlt_fmly   
          CALL accel_indices(lvdxyz_odd(:,:,wlt_fmly),lvdxyz_even(:,:,wlt_fmly), &  
               nlvdxyz_odd(:,:,:,wlt_fmly),nlvdxyz_even(:,:,:,wlt_fmly), &  
               nxyz,mxyz,n_prdct(wlt_fmly),n_updt(wlt_fmly),&
               n_assym_prdct(wlt_fmly,trnsf_type),n_assym_updt(wlt_fmly,trnsf_type), &
               n_assym_prdct_bnd(wlt_fmly,trnsf_type),n_assym_updt_bnd(wlt_fmly,trnsf_type), &
               prd,trnsf_type,ibc,j_lev,i_d,jd,dim)
       END DO
    END DO
    

    IF(read_geometry) THEN
       CALL rearrange_i_in (j_lev_old)
    END IF
    IF (nwlt /= nwlt_old) THEN
       DEALLOCATE (u)  
       ALLOCATE   (u(1:nwlt,1:n_var)) 
       u = 0.0_pr
       
       
       
       IF( flag == 0 ) THEN !adapting to initial condition
          IF( ALLOCATED(f) ) DEALLOCATE (f)  
          ALLOCATE   (f(1:nwlt,1:n_integrated))  !startup case  
          f = 0.0_pr !startup case 
       END IF
    END IF

    !************ Inverse Wavelet Transform ****************
    ! Do inverse wlt transform on n_var_interpolate variables (logical mask)
    mask = n_var_interpolate(:,flag)
    IF( BTEST(debug_level,2) ) THEN
       PRINT *, 'before transform: j_lev=',j_lev,'j_lev_a=',j_lev_a,'mask', mask
       DO ie=1,n_var
          WRITE (*,'("MINMAX(u_in(:,",I2,"))=",2E22.15)') ie, MINVAL(u_old(:,ie)), MAXVAL(u_old(:,ie))
       END DO
    END IF
    
    CALL c_wlt_trns_mask (u_old, u , n_var, mask ,mask, .FALSE., &
         .TRUE., nwlt_old, nwlt, j_lev_old, j_lev, HIGH_ORDER, WLT_TRNS_INV, .TRUE., .TRUE.)
    
    IF( BTEST(debug_level,2) ) THEN
!!$    PRINT *, 'u-u_old:', j_lev, j_lev_old, nwlt, nwlt_old
!!$    IF(j_lev_old == j_lev .AND. nwlt_old == nwlt) THEN
!!$       PRINT *, '|u - u_old|=',MAXVAL(ABS(u(1:nwlt,1)-u_old(1:nwlt,1)))
!!$    END IF
!!$    PAUSE
       DO ie=1,n_var
          WRITE (*,'("MINMAX(u_out(:,",I2,"))=",2E22.15)') ie, MINVAL(u(:,ie)), MAXVAL(u(:,ie))
       END DO
    END IF
    
    IF(n_var_exact > 0) THEN
       IF( ALLOCATED(u_ex) ) DEALLOCATE (u_ex)  
       ALLOCATE   (u_ex(1:nwlt,1:n_var_exact)) 
       u_ex = 0.0_pr
    END IF
    
    IF( BTEST(debug_level,2) ) THEN
       WRITE (*,'(" ")')
       WRITE (*,'("==================== end of adapt_grid ================")')
       WRITE (*,'("j_lev      = ", I6)') j_lev
       WRITE (*,'("j_lev_a    = ", I6)') j_lev_a
       WRITE (*,'("j_lev_old  = ", I6)') j_lev_old
       PRINT *, 'new_grid =', new_grid
       WRITE (*,'("=======================================================")')
       WRITE (*,'(" ")')
    END IF
    
    ! parallel artifact
    nwlt_global = nwlt

    
    nwlt_p_ghost = COUNT(i_d )         ! # total active wavelets plus ghost points
    
    
    CALL timer_stop(3)
    
    
  END SUBROUTINE adapt_grid



  !
  ! Initialize new Field data base
  !
  RECURSIVE SUBROUTINE init_DB (nwlt_old, nwlt, nwltj_old, nwltj, new_grid,&
       j_lev_old, j_lev_a, j_mn_local, j_mx_local, max_nxyz, nxyz_a, IC_from_restart_file, &
       SECOND_CALL)
    USE precision
    USE pde
    USE share_consts
    USE field
    USE wlt_vars                ! j_mn_init
    USE variable_mapping
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: j_mn_local, j_mx_local, max_nxyz, nxyz_a(1:3) 
    INTEGER, INTENT (INOUT) :: j_lev_old, nwlt, nwlt_old, nwltj, nwltj_old, j_lev_a
    LOGICAL, INTENT (INOUT) :: new_grid
    LOGICAL, INTENT (IN)    ::  IC_from_restart_file ! true if restart or IC from restart file
    LOGICAL, INTENT (IN), OPTIONAL :: SECOND_CALL    ! true if n_prdct has to be changed
    INTEGER :: wlt_fmly, trnsf_type, ireg, &
         n_prdct_inp(0:n_wlt_fmly), &                ! temporal storage for resetting
         n_updt_inp(0:n_wlt_fmly),  &                ! n_prdct and n_updt
         ie, flag
    LOGICAL :: mask(1:n_var), is_second_call

    ! n_prdct change during restart will cause the second call of init_DB
    is_second_call = .FALSE.
    IF (PRESENT(SECOND_CALL)) is_second_call = SECOND_CALL
    ! store .inp values and set current to .res
    IF (IC_from_restart_file) THEN
       PRINT *,'is_second_call = ',is_second_call
       n_prdct_inp = n_prdct
       n_updt_inp = n_updt
       n_prdct = n_prdct_resfile
       n_updt = n_updt_resfile
       ! redefine initialized before values (set it as a function)
       maxval_n_updt = maxval(n_updt)
       maxval_n_prdct = maxval(n_prdct)
       ! set n_assym_prdct/prdct_bnd/updt/updt_bnd
       CALL set_max_assymetry
    END IF
    
    flag = 0
    IF(IC_restart) flag = 1
    
    !
    ! If we are restarting we use j_mn as the lowest level instead of j_mn_init.
    !
    IF((IC_restart .OR. (IC_from_file .AND. IC_file_fmt == 0)) ) THEN
       j_mn = j_mn_local
    ELSE
       j_mn = j_mn_init
    END IF

    CALL initialize_additional_nodes

    !
    ! Set flag for indices, ireg = 0 normal, or ireg=-1 for restart
    ! IC_file_fmt == 0 means native restart file format
    ireg = 0
    IF( IC_from_restart_file ) ireg=-1
    IF( is_second_call ) ireg = 1

    CALL indices (nwlt_old, nwlt, nwltj_old, nwltj, new_grid,&
         j_lev_old, j_lev_a, j_mn, j_mx, ireg)
    ! Note must call indices again with last arg 1 to initialize deriviatives
    ! for elliptic solver (analytic solution). This needs to be fixed to work
    ! in both cases.
    
    IF ( IC_restart.OR. &
         (IC_from_file .AND. IC_file_fmt == 0).OR. &
         IC_from_restart_file ) &                               ! case of post-processing call
         CALL rearrange_u (j_lev_old)
    ! this is done insure correct orderiing in u(1:nwlt,:) array
    !   (i) the order of the array is DB dependent
    !  (ii) u is saved in the order specific to database


    CALL indices (nwlt_old, nwlt, nwltj_old, nwltj, new_grid, j_lev_old, j_lev_a, j_mn, j_mx, 1)

    not_IC_restart: IF( .NOT. IC_from_restart_file )  THEN
       ! If not restarting 

       !Allocate field variables
       ALLOCATE (  u(1:nwlt,1:n_var), f(1:nwlt,1:n_integrated))
       f = 0.0_pr ; u = 0.0_pr

       IF(n_var_exact > 0) THEN
          IF( ALLOCATED(u_ex) ) DEALLOCATE (u_ex)
          ALLOCATE ( u_ex(1:nwlt,1:n_var_exact) )
          u_ex = 0.0_pr
       END IF

    END IF not_IC_restart

    ALLOCATE (   u_old(1:nwlt,1:n_var))
    u_old = u ! we start with u_old and then in adapt grid the adapted data will be put in u

    DO wlt_fmly = 0, n_wlt_fmly
       DO trnsf_type = 0,n_trnsf_type
          CALL accel_size(lvxyz_odd(:,:,wlt_fmly,trnsf_type),lvxyz_even(:,:,wlt_fmly,trnsf_type), &
               nxyz,mxyz,n_prdct(wlt_fmly),n_updt(wlt_fmly),prd,trnsf_type,ibc,j_lev,i_c,jd,dim)
       END DO
    END DO
    ! 
    IF(ALLOCATED(nlvxyz_odd)) DEALLOCATE(nlvxyz_odd)
    IF(ALLOCATED(nlvxyz_even)) DEALLOCATE(nlvxyz_even)

    ALLOCATE(  nlvxyz_odd(1:6,1:3,1:MAXVAL(lvxyz_odd(j_lev-1,1:3,0:n_wlt_fmly,0:n_trnsf_type)),0:n_wlt_fmly,0:n_trnsf_type) )

    ALLOCATE( nlvxyz_even(1:6,1:3,1:MAXVAL(lvxyz_even(j_lev-1,1:3,0:n_wlt_fmly,0:n_trnsf_type)),0:n_wlt_fmly,0:n_trnsf_type) )
    DO wlt_fmly = 0, n_wlt_fmly
       DO trnsf_type = 0,n_trnsf_type
          CALL accel_indices(lvxyz_odd(:,:,wlt_fmly,trnsf_type), lvxyz_even(:,:,wlt_fmly,trnsf_type),&
               nlvxyz_odd(:,:,:,wlt_fmly,trnsf_type), nlvxyz_even(:,:,:,wlt_fmly,trnsf_type), nxyz &
               ,mxyz,n_prdct(wlt_fmly),n_updt(wlt_fmly), n_assym_prdct(wlt_fmly,trnsf_type) &
               ,n_assym_updt(wlt_fmly,trnsf_type),n_assym_prdct_bnd(wlt_fmly,trnsf_type) &
               ,n_assym_updt_bnd(wlt_fmly,trnsf_type), prd,trnsf_type,ibc,j_lev,i_c,jd,dim)
       END DO
    END DO



    !*** do a reconstruction check after indices if we are restarting ****
    IF(IC_from_restart_file ) THEN
       DO trnsf_type =0, 0
          DO wlt_fmly = 0, n_wlt_fmly   
             CALL reconstr_check (i_d, wlt_fmly, trnsf_type, nxyz_a, j_lev_a)
          END DO
       END DO
    END IF

    !
    !----------- for derivatives ----------------------------
    !
    DO wlt_fmly = 0, n_wlt_fmly   
       CALL accel_size(lvdxyz_odd(:,:,wlt_fmly),lvdxyz_even(:,:,wlt_fmly),&
            nxyz,mxyz,n_prdct,n_updt,prd,0,ibc,j_lev,i_d,jd,dim)
    END DO
    IF(ALLOCATED(nlvdxyz_odd))  DEALLOCATE(nlvdxyz_odd)
    IF(ALLOCATED(nlvdxyz_even))  DEALLOCATE(nlvdxyz_even)
    ALLOCATE( nlvdxyz_odd(1:6,1:3,1:MAXVAL(lvdxyz_odd(j_lev-1,1:3,0:n_wlt_fmly)),0:n_wlt_fmly) )
    ALLOCATE( nlvdxyz_even(1:6,1:3,1:MAXVAL(lvdxyz_even(j_lev-1,1:3,0:n_wlt_fmly)),0:n_wlt_fmly) )

    DO trnsf_type = 0,0
       DO wlt_fmly = 0, n_wlt_fmly 
          CALL accel_indices(lvdxyz_odd(:,:,wlt_fmly),lvdxyz_even(:,:,wlt_fmly), &
               nlvdxyz_odd(:,:,:,wlt_fmly),nlvdxyz_even(:,:,:,wlt_fmly), &
               nxyz,mxyz,n_prdct(wlt_fmly),n_updt(wlt_fmly), n_assym_prdct(wlt_fmly,trnsf_type),n_assym_updt(wlt_fmly,trnsf_type),&
               n_assym_prdct_bnd(wlt_fmly,trnsf_type),n_assym_updt_bnd(wlt_fmly,trnsf_type), prd,trnsf_type,ibc,j_lev,&
               i_d,jd,dim)

       END DO
    END DO

    IF ( BTEST(debug_level,2) ) THEN
       WRITE (*,'(" ")')
       WRITE (*,'("==================== at the end of init_DB ============")')
       WRITE (*,'("# significant wlts = ", I12, I12)') COUNT(i_c)
       WRITE (*,'("j_lev              = ", I6)') j_lev
       WRITE (*,'("j_lev_a            = ", I6)') j_lev_a
       WRITE (*,'("=======================================================")')
       WRITE (*,'(" ")')
    END IF

!?????????????????????

    IF(IC_from_restart_file ) THEN ! if restart or IC from restart file
       !************ Inverse Wavelet Transform ****************
       ! Do inverse wlt transform on n_var_interpolate variables (logical mask)
       mask = n_var_interpolate(:,flag)
       IF (.NOT.is_second_call) THEN
          IF( BTEST(debug_level,2) ) THEN
             PRINT *, 'before transform: j_lev=',j_lev,'j_lev_a=',j_lev_a,'mask', mask
             DO ie=1,n_var
                WRITE (*,'("MINMAX(u_in(:,",I2,"))=",2E22.15)') ie, MINVAL(u_old(:,ie)), MAXVAL(u_old(:,ie))
             END DO
          END IF
          
          CALL c_wlt_trns_mask (u_old, u , n_var, mask ,mask, .FALSE., &
               .TRUE., nwlt_old, nwlt, j_lev_old, j_lev, HIGH_ORDER, WLT_TRNS_INV, .TRUE., .TRUE.)
          
          IF( BTEST(debug_level,2) ) THEN
!!$    PRINT *, 'u-u_old:', j_lev, j_lev_old, nwlt, nwlt_old
!!$    IF(j_lev_old == j_lev .AND. nwlt_old == nwlt) THEN
!!$       PRINT *, '|u - u_old|=',MAXVAL(ABS(u(1:nwlt,1)-u_old(1:nwlt,1)))
!!$    END IF
!!$    PAUSE
             DO ie=1,n_var
                WRITE (*,'("MINMAX(u_out(:,",I2,"))=",2E22.15)') ie, MINVAL(u(:,ie)), MAXVAL(u(:,ie))
             END DO
          END IF

          ! real values are in U and the database, set .inp values for the computing to follow
          ! if it is the first call and the values are different
          IF ( ANY( n_prdct_inp(0:n_wlt_fmly).NE.n_prdct_resfile(0:n_wlt_fmly)) .OR. &
               ANY( n_updt_inp(0:n_wlt_fmly).NE.n_updt_resfile(0:n_wlt_fmly)) ) THEN
             n_prdct = n_prdct_inp
             n_updt = n_updt_inp
             n_prdct_resfile = n_prdct_inp
             n_updt_resfile = n_updt_inp
             CALL init_DB ( nwlt_old, nwlt, nwltj_old, nwltj, new_grid,&
                  j_lev_old, j_lev_a, j_mn_local, j_mx_local, max_nxyz, nxyz_a, IC_from_restart_file, &
                  SECOND_CALL=.TRUE.)
             RETURN
          END IF
          
       END IF
    END IF
    
    IF(n_var_exact > 0) THEN
       IF( ALLOCATED(u_ex) ) DEALLOCATE (u_ex)  
       ALLOCATE   (u_ex(1:nwlt,1:n_var_exact)) 
       u_ex = 0.0_pr
    END IF
    
    IF( BTEST(debug_level,2) ) THEN
       WRITE (*,'(" ")')
       WRITE (*,'("==================== end of init_DB ================")')
       WRITE (*,'("j_lev      = ", I6)') j_lev
       WRITE (*,'("j_lev_a    = ", I6)') j_lev_a
       WRITE (*,'("j_lev_old  = ", I6)') j_lev_old
       PRINT *, 'new_grid =', new_grid
       WRITE (*,'("=======================================================")')
       WRITE (*,'(" ")')
    END IF
    
    nwlt_p_ghost = COUNT(i_d )         ! # total active wavelets plus ghost points
    
  END SUBROUTINE init_DB


  ! this is done insure correct orderiing in u(1:nwlt,:) array
  !   (i) the order of the array is DB dependent
  !  (ii) u is saved in the order specific to database
  SUBROUTINE rearrange_u (j_lev_old) 
    USE precision
    USE threedobject
    USE wlt_vars
    USE sizes
    USE field ! u
    USE pde ! n_var
    USE variable_mapping
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: j_lev_old
    INTEGER, DIMENSION(3) :: ixyz
    INTEGER :: i, inum, idenom, ie
    REAL(pr), DIMENSION(:,:), ALLOCATABLE :: u_tmp

    IF(ALLOCATED(i_in)) THEN
       WRITE (*,'("rearrange_u: j_lev=",I3,", j_lev_old=",I3)') j_lev, j_lev_old

       ALLOCATE ( u_tmp(1:nwlt,1:n_var))
       u_tmp = u
       inum=1
       idenom=1
       ixyz = 0
       IF(j_lev > j_lev_old) inum=2**(j_lev-j_lev_old)
       IF(j_lev < j_lev_old) idenom=2**(j_lev_old-j_lev)
       DO ie = 1, n_var
          IF(j_lev_old <= j_lev) THEN
             DO i=1, nwlt_old
                ixyz(1:dim) = indx_old(i,1:dim)*inum/idenom
                wrk(ixyz(1),ixyz(2),ixyz(3)) = u_tmp(i,ie)
             END DO
          ELSE 
             DO i=1, nwlt_old
                ixyz(1:dim) = indx_old(i,1:dim)
                IF(ixyz2j(ixyz(1:dim),j_lev_old) <= j_lev) THEN
                   ixyz(1:dim) = ixyz(1:dim)*inum/idenom
                   wrk(ixyz(1),ixyz(2),ixyz(3)) = u_tmp(i,ie)
                END IF
             END DO
          END IF
          DO i=1, nwlt
             ixyz(1:dim) = indx(i,1:dim)
             u(i,ie)=wrk(ixyz(1),ixyz(2),ixyz(3))
          END DO
       END DO
       DO i=1, nwlt
          ixyz(1:dim) = indx(i,1:dim)
          wrk(ixyz(1),ixyz(2),ixyz(3))=0.0_pr
       END DO

       DEALLOCATE(u_tmp)
    ELSE
       ALLOCATE(i_in(2,nwlt))
       i_in = .FALSE.
    END IF

  END SUBROUTINE rearrange_u


  SUBROUTINE rearrange_i_in (j_lev_old) 
    USE precision
    USE wlt_vars
    USE sizes
    USE field ! u
    USE pde ! n_var
    USE threedobject 
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: j_lev_old
    INTEGER, DIMENSION(3) :: ixyz
    INTEGER :: i, inum, idenom, ie
    LOGICAL,  DIMENSION(:,:,:,:), ALLOCATABLE :: i_in_tmp ! 1 - in/out, 2 - old/new
     
    WRITE (*,'("rearrange_i_in: j_lev=",I3,", j_lev_old=",I3)') j_lev, j_lev_old

   
    ALLOCATE(i_in_tmp(2,0:nxyz(1),0:nxyz(2),0:jd*nxyz(3)))
    i_in_tmp = .FALSE.

    inum=1
    idenom=1
    ixyz = 0
    IF(j_lev > j_lev_old) inum=2**(j_lev-j_lev_old)
    IF(j_lev < j_lev_old) idenom=2**(j_lev_old-j_lev)
    IF(j_lev_old <= j_lev) THEN
       DO i=1, nwlt_old
          ixyz(1:dim) = indx_old(i,1:dim)*inum/idenom
          i_in_tmp(1,ixyz(1),ixyz(2),ixyz(3)) = i_in(1,i)
          i_in_tmp(2,ixyz(1),ixyz(2),ixyz(3)) = .TRUE.
       END DO
    ELSE 
       DO i=1, nwlt_old
          ixyz(1:dim) = indx_old(i,1:dim)
          IF(ixyz2j(ixyz(1:dim),j_lev_old) <= j_lev) THEN
             ixyz(1:dim) = ixyz(1:dim)*inum/idenom
             i_in_tmp(1,ixyz(1),ixyz(2),ixyz(3)) = i_in(1,i)
             i_in_tmp(2,ixyz(1),ixyz(2),ixyz(3)) = .TRUE.
          END IF
       END DO
    END IF
    IF(nwlt /= nwlt_old) THEN 
       IF(ALLOCATED(i_in)) DEALLOCATE(i_in)
       ALLOCATE(i_in(2,nwlt))
    END IF
    i_in = .FALSE.
    DO i=1, nwlt
       ixyz(1:dim) = indx(i,1:dim)
       i_in(:,i)=i_in_tmp(:,ixyz(1),ixyz(2),ixyz(3))
    END DO

    DEALLOCATE(i_in_tmp)

  END SUBROUTINE rearrange_i_in




  !
  ! update_db_from_u 
  ! In this routine we update the values in u from the db
  ! we also delete points flagged for deletion from the db
  ! and update j_lev and db_nwlt to refelect any changes.
  ! (This is a dummy routine for wrk version 
  SUBROUTINE update_db_from_u(  u , nwlt , ie, n_var_min, n_var_max, db_offset )
    IMPLICIT NONE
    INTEGER,   INTENT(IN) :: nwlt, ie
    REAL (pr), INTENT(IN) :: u(1:nwlt,1:ie)
    INTEGER,   INTENT(IN) :: n_var_min, n_var_max, db_offset

  END SUBROUTINE update_db_from_u
  SUBROUTINE update_u_from_db(  u , nwlt , ie, n_var_min, n_var_max, db_offset )
    IMPLICIT NONE
    INTEGER,   INTENT(IN) :: nwlt, ie
    REAL (pr), INTENT(IN) :: u(1:nwlt,1:ie)
    INTEGER,   INTENT(IN) :: n_var_min, n_var_max, db_offset

  END SUBROUTINE update_u_from_db

  !
  ! update_u_from_db_noidices 
  ! In this routine we update the values in u from the db
  ! No indices are changed. Only use this routine if the 
  ! grid has not changed since the last call to update_u_from_db()
  !(This is a dummy routine for wrk version 
  SUBROUTINE update_u_from_db_noidices(  u , nwlt , ie, n_var_min, n_var_max, db_offset )
    IMPLICIT NONE
    INTEGER,   INTENT(IN) :: nwlt, ie
    REAL (pr), INTENT(INOUT) :: u(1:nwlt,1:ie)
    INTEGER,   INTENT(IN) :: n_var_min, n_var_max, db_offset

  END SUBROUTINE update_u_from_db_noidices

  !**************************************************************************
  !
  ! Find spacial derivatives of components already in db
  !
  !
  ! Arguments
  !u, du, d2u, j_in, nlocal, meth, meth1, 
  ! ID Sets which derivative is done 
  !    10 - do first derivative
  !    11 - do first and secont derivative
  !    01 - do second derivative
  !    if ID > 0 then the second derivative is central difference D_central
  !    if ID < 0 then the second derivative is D_backward_bias followed by D_forward_bias
  ! meth  - order of the method, high low,
  !
  !  dimensions of u are u(1:nlocal,1:ie) 
  !  derivative is done on  u(1:nlocal,mn_var:mx_var)
  !(This is a dummy routine for wrk version
  SUBROUTINE c_diff_fast_db(du, d2u, j_in, nlocal, meth, id, ie, mn_var, mx_var, mn_varD, mx_varD , mn_varD2, mx_varD2)
    USE precision
    USE sizes
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: j_in, nlocal, meth, id
    INTEGER, INTENT(IN) :: ie ! number of dimensions of u(:,1:ie)  we are doing derivative for (forced to 1 for now)
    INTEGER, INTENT(IN) ::  mn_var, mx_var ! min and max of second index of u that we are doing the derivatives for.
    INTEGER, INTENT(IN) ::  mn_varD, mx_varD , mn_varD2, mx_varD2 ! min/max extents in db to do 1st/2nd derivative
    REAL (pr), DIMENSION (mn_varD :mx_varD , 1:nlocal,dim), INTENT (INOUT) :: du
    REAL (pr), DIMENSION (mn_varD2:mx_varD2, 1:nlocal,dim), INTENT (INOUT) :: d2u
  END SUBROUTINE c_diff_fast_db


  SUBROUTINE weights 
    !--Calculates weights for volume integration based on the Trapezoid
    !--integration rule (second-order accurate).
    USE precision
    USE sizes
    USE wlt_vars
    USE util_vars
    USE pde
    IMPLICIT NONE

    INTEGER :: i, j, k, m, idim
    INTEGER, DIMENSION(dim) :: ixyz
    INTEGER, DIMENSION(3,2) :: i_k
    REAL (pr), DIMENSION(dim) :: lxyz
    REAL (pr) :: shft(dim,2), dAh 
    INTEGER :: i1(1), ii, j_df, wlt_type, face_type, dir_type, step, step2, ibndr
    INTEGER, DIMENSION(0:dim) :: i_p, i_p_cube
    INTEGER, DIMENSION(dim) :: ivec, dir, ixyz1, wlt, i_l, i_h
    INTEGER, DIMENSION(dim,1) :: ZERO


    IF( Weights_Meth ==   0  ) THEN ! # Weights_Meth, 0: dA=area/nwlt, 1: normal
       WRITE(*,'(A)')  " "
       WRITE(*,'(A)')  "************************************************************"
       WRITE(*,'(A,I3)')  'Setting dA = area/nwlt for Weights Method = ',Weights_Meth
       WRITE(*,'(A)')  "************************************************************"
       WRITE(*,'(A)')  " "
       dA = PRODUCT(xyzlimits(2,1:dim)- xyzlimits(1,1:dim))/nwlt
       dA_level = 0.0_pr
       DO j = 1, j_lev
          dA_level(1:Nwlt_lev(j,1),j) = PRODUCT(xyzlimits(2,1:dim)- xyzlimits(1,1:dim))/Nwlt_lev(j,1)
       END DO
    ELSE


       ZERO = 0
       dA = 0.0_pr
       i_k = 0
       lxyz(:) = xyzlimits(2,:)-xyzlimits(1,:)
       i_p(0) = 1
       i_p_cube(0) = 1
       DO i=1,dim
          i_p(i) = i_p(i-1)*(1+nxyz(i))
       END DO
       DO i = 1,dim
          ivec(i) = dim-i+1
       END DO


       !first go through coarse levels and assign areas to coarse levels only
       j = 1
       step = 2**(j_lev-j)
       DO wlt_type = MIN(j-1,1),2**dim-1
          DO face_type = 0, 3**dim - 1
             DO j_df = j, j_lev
                DO k = 1, indx_DB(j_df,wlt_type,face_type,j)%length  
                   ixyz(1:dim) = INT(MOD(indx_DB(j_df,wlt_type,face_type,j)%p(k)%ixyz-1,i_p(1:dim))/i_p(0:dim-1))
                   i = indx_DB(j_df,wlt_type,face_type,j)%p(k)%i
                   DO dir_type = 0,2**dim-1
                      dir = 2*IBITS(dir_type,ivec(:)-1,1)-1
                      shft = 0.0_pr
                      ixyz1=ixyz+step*dir
                      WHERE(prd(1:dim) == 1 .AND. ixyz1(1:dim) < 0) &
                           shft(1:dim,1)=lxyz(1:dim)                ! left shift for periodic case
                      WHERE(prd(1:dim) == 1 .AND. ixyz1(1:dim) > nxyz(1:dim)-prd(1:dim)) &
                           shft(1:dim,2)=prd(1:dim)*lxyz(1:dim)  ! right shift for periodic case
                      ixyz1(1:dim) = (1-prd(1:dim))*ixyz1(1:dim)+prd(1:dim)*MOD(ixyz1(1:dim)+9*nxyz(1:dim),nxyz(1:dim))
                      !checks if subcube is in the domain
                      IF(.NOT.(ANY(ixyz1(1:dim) > nxyz(1:dim) - prd(1:dim)) .OR. ANY(ixyz1(1:dim) < 0)) )  THEN 
                         dAh=1.0_pr
                         DO idim=1,dim
                            dAh=dAh*( REAL(dir(idim),pr)*(xx(ixyz1(idim),idim)-xx(ixyz(idim),idim))+shft(idim,1)+shft(idim,2) )
                         END DO
                         dA(i) = dA(i) + dAh/2**dim
                      END IF
                   END DO

!!$                   WRITE (*,'(A,I1,A,'//CHAR(ICHAR('0')+dim)//'I3,A,I3,A,E12.5)') 'j=',j,', ixyz=',ixyz*2**(j_mx-j_lev),', i=',i,', dA(i)=',dA(i)
                   
                END DO
             END DO
          END DO
       END DO
       
       !----------------- Calculate integration weights by level ----------------------
       dA_level(1:lv_intrnl(j),j)=dA(1:lv_intrnl(j))
       DO ibndr=1,2*dim
          dA_level(lv_bnd(2,ibndr,j):lv_bnd(3,ibndr,j),j)  = dA(lv_bnd(0,ibndr,1):lv_bnd(1,ibndr,j))
       END DO
!!$       PRINT *, 'after level 1, dA_level = ', dA_level
!!$       PRINT *, 'dA=',dA
       
       !Use machinerey of reconstruction check procedure
       
       DO j = 2,j_lev
          step = 2**(j_lev-j)
          step2 = 2*step
          DO wlt_type = MIN(j-1,1),2**dim-1
             wlt(1:dim) = IBITS(wlt_type,ivec(1:dim)-1,1)
             DO face_type = 0, 3**dim - 1
                DO j_df = j, j_lev
                   DO k = 1, indx_DB(j_df,wlt_type,face_type,j)%length  
                      ixyz(1:dim) = INT(MOD(indx_DB(j_df,wlt_type,face_type,j)%p(k)%ixyz-1,i_p(1:dim))/i_p(0:dim-1))
                      i = indx_DB(j_df,wlt_type,face_type,j)%p(k)%i
                      IF(dA(i) /= 0.0_pr) PRINT *, i,'before dA=',dA(i)
                      DO dir_type = 0,2**dim-1
                         dir = 2*IBITS(dir_type,ivec(:)-1,1)-1
                         shft = 0.0_pr
                         ixyz1=ixyz+step*dir
                         WHERE(prd(1:dim) == 1 .AND. ixyz1 < 0) &
                              shft(1:dim,1)=lxyz(:)                ! left shift for periodic case
                         WHERE(prd(1:dim) == 1 .AND. ixyz1 > nxyz(1:dim)-prd(1:dim)) &
                              shft(1:dim,2)=prd(1:dim)*lxyz(1:dim)  ! right shift for periodic case
                         ixyz1 = (1-prd(1:dim))*ixyz1+prd(1:dim)*MOD(ixyz1+9*nxyz(1:dim),nxyz(1:dim))
                         !checks if subcube is in the domain
                         IF(.NOT.(ANY(ixyz1 > nxyz(1:dim) - prd(1:dim)) .OR. ANY(ixyz1 < 0)) )  THEN 
                            dAh=1.0_pr
                            DO idim=1,dim
                               dAh=dAh*( REAL(dir(idim),pr)*(xx(ixyz1(idim),idim)-xx(ixyz(idim),idim))+shft(idim,1)+shft(idim,2) )
                            END DO
                            dA(i) = dA(i) + dAh/2**dim
                         END IF
                      END DO

!!$                      WRITE (*,'(A,I2,A,'//CHAR(ICHAR('0')+dim)//'I4,A,I3,A,E12.5)') &
!!$                           'j=',j,', ixyz=',ixyz*2**(j_mx-j_lev),', i=',i,', dA(i)=',dA(i)

                      IF(dA(i) <= 0.0_pr) PRINT *, i,'after dA=',dA(i)
                      i_l(:) = MAX(ixyz(:)-step,ZERO(:,1))
                      i_h(:) = MIN(i_l(:)+step2,nxyz(1:dim)) 
                      i_l(:) = MAX(i_h(:)-step2,ZERO(:,1))
                      i_l(:) = (1-prd(:))*i_l(:) + prd(:)*( ixyz - step )
                      i_h(:) = (1-prd(:))*i_h(:) + prd(:)*( ixyz + step )
                      i_l(:) = ixyz(:) + ( i_l(:) - ixyz(:) ) * wlt(:)
                      i_h(:) = ixyz(:) + ( i_h(:) - ixyz(:) ) * wlt(:)
                      !getting i based on location
                      i_p_cube(1:dim) = (i_h-i_l)/step2 + 1
                      DO idim =1, dim
                         i_p_cube(idim) = i_p_cube(idim-1)*i_p_cube(idim)
                      END DO
                      DO m = 1,i_p_cube(dim)
                         ixyz1 = i_l + INT(MOD(m-1,i_p_cube(1:dim))/i_p_cube(0:dim-1))*step2
                         ixyz1(1:dim) = (1-prd(1:dim))*ixyz1(1:dim) + prd(1:dim)*MOD(ixyz1(:)+9*nxyz(1:dim),nxyz(1:dim))
                         CALL get_indices_by_coordinate(1,ixyz1(:),j_lev,i1(1),0)
                         dA(i1(1)) = dA(i1(1)) - dA(i)/i_p_cube(dim)
!!$                         WRITE (*,'(A,'//CHAR(ICHAR('0')+dim)//'I3,2(A,I3))') '  ixyz1=',ixyz1,', j_lev=',j_lev
!!$                         WRITE (*,*) '   i1,dA(i1)=',i1,dA(i1)
                      END DO
                   END DO
                END DO
             END DO
          END DO
          !----------------- Calculate integration weights by level ----------------------
          dA_level(1:lv_intrnl(j),j)=dA(1:lv_intrnl(j))
          DO ibndr=1,2*dim
             dA_level(lv_bnd(2,ibndr,j):lv_bnd(3,ibndr,j),j)  = dA(lv_bnd(0,ibndr,1):lv_bnd(1,ibndr,j))
          END DO
          !----------------------------------------------------------------------------
          
       END DO
       
    END IF !IF( Scale_Meth == 1 .OR. Scale_Meth == 2 ) THEN
    IF( ANY(dA < 0) ) PRINT *,'WARNING: dA is negative'
    
    sumdA = sum(dA) ! calculate here to use in varies places
    sumdA_global = sumdA      !                                        (global)
    
    PRINT *, 'A=',sumdA,PRODUCT(xyzlimits(2,1:dim)- xyzlimits(1,1:dim))
!!$  PRINT *, 'finish claculating weights'
  END SUBROUTINE weights  
  
  !************************** Auxliary functions that are DB specific
  !this subroutine uses working array iwrk.  It is not memory efficient this way, but in actual DBs it is more efficient.
  SUBROUTINE get_indices_by_coordinate(nvec,ixyz_vec,j_vec,i_vec,ireg)
    USE precision
    IMPLICIT NONE
    INTEGER , INTENT(IN) :: nvec,ireg, j_vec
    INTEGER, DIMENSION(dim,nvec), INTENT(IN) :: ixyz_vec !ixyz coordinates for points needed
    INTEGER, DIMENSION(nvec), INTENT(OUT) :: i_vec       !i_vec is the vector of indices in 

    INTEGER :: i, ii, j, k, kk, j_df, wlt_type, face_type
    INTEGER, DIMENSION(dim) :: ixyz
    INTEGER, DIMENSION(0:dim) :: i_p

    IF(j_vec /= j_lev .AND. ireg .NE. -1) THEN !in wrk_DB j_vec is assumed to be j_lev
       WRITE(*,'("ERROR in SUBROUTINE get_indices_by_coordinate: j_vec /= j_lev")')
       STOP
    END  IF

    i_p(0) = 1
    DO i=1,dim
       i_p(i) = i_p(i-1)*(1+nxyz(i))
    END DO

    IF(ireg == 1) THEN !putting inverse information to iwrk
       DO j = 1, j_lev
          DO wlt_type = MIN(j-1,1),2**dim-1
             DO face_type = 0, 3**dim - 1
                DO j_df = j, j_lev
                   DO k = 1, indx_DB(j_df,wlt_type,face_type,j)%length  
                      ixyz(1:dim) = INT(MOD(indx_DB(j_df,wlt_type,face_type,j)%p(k)%ixyz-1,i_p(1:dim))/i_p(0:dim-1))
					  IF(ii > size(iwrk)) THEN
						 PRINT *, 'ERROR1: ii=',ii,size(iwrk)
						 STOP
					  END IF
                      iwrk(ii) = indx_DB(j_df,wlt_type,face_type,j)%p(k)%i
                   END DO
                END DO
             END DO
          END DO
       END DO
    ELSE IF(ireg == 0) THEN !extracting inverse index
       DO i = 1,nvec
          kk = 1+SUM(ixyz_vec(1:dim,i)*i_p(0:dim-1)) ! index on j_lev level
          i_vec(i) = iwrk(kk)
       END DO
    ELSE IF(ireg == -1 .AND. j_vec == j_lev) THEN !cleaning iwrk
       DO j = 1, j_vec
          DO wlt_type = MIN(j-1,1),2**dim-1
             DO face_type = 0, 3**dim - 1
                DO j_df = j, j_vec
                   DO k = 1, indx_DB(j_df,wlt_type,face_type,j)%length  
                      ii = indx_DB(j_df,wlt_type,face_type,j)%p(k)%ixyz
					  IF(ii > size(iwrk)) THEN
						 PRINT *, 'ERROR2: ii=',ii,size(iwrk), j, j_vec, j_lev
						 STOP
					  END IF
                      iwrk(ii) = 0
                   END DO
                END DO
             END DO
          END DO
       END DO
    ELSE IF(ireg == -1 .AND. j_vec /= j_lev) THEN !cleaning iwrk
	   iwrk = 0
    END IF

  END SUBROUTINE get_indices_by_coordinate

  SUBROUTINE release_memory_DB
    USE precision
    IMPLICIT NONE

    ! Not sure why these are not defined here. Gives error in deallocate.
    !DEALLOCATE (iwrk, wrk, indx_old, indx, i_c,  i_d, &
         !            lv_intrnl, lv_bnd, lv, lvj, nlv, nlvj, nlvjD, &
    !            lvxyz_odd, lvxyz_even, nlvxyz_odd, nlvxyz_even,  &
         !            lvdxyz_odd, lvdxyz_even, nlvdxyz_odd, nlvdxyz_even,  &
    !            wgh_updt, wgh_prdct, wgh_df, wgh_d2f,  wgh_updt_interp, wgh_prdct_interp, &
         !            h )

    DEALLOCATE ( xx, Nwlt_lev, indx_DB)

  END SUBROUTINE release_memory_DB

  
!!$  !-------------------------------------------------------------------------------------------
!!$  SUBROUTINE DB_interpolate_wrk (u, du, nwlt, n_var, &
!!$       x_size, x, interpolation_order, var_size, var, res )
!!$    ! Interpolation into array of points x(1:dim,1:x_size),
!!$    ! where dim is the dimension and x_size is the number of points.
!!$    ! Order of interpolation is to be provided (0-nearliest, 1-linear, etc).
!!$    ! Result is to be returned as res(1:nvar,1:x_size),
!!$    ! where var_size is the number of variables to interpolate for, whose global
!!$    ! numbers (1..n_var) are to be provided in the array variables(1:var_size).
!!$    INTEGER, INTENT (IN) :: nwlt, n_var, x_size, var_size   ! sizes
!!$    INTEGER, INTENT (IN) :: interpolation_order             ! order: 0, 1, 3
!!$    REAL(pr), INTENT(IN) :: u(1:nwlt, 1:n_var)              ! function
!!$    REAL(pr), INTENT(IN) :: du(1:n_var,1:nwlt,1:dim)        ! derivatives
!!$    REAL(pr), INTENT (IN) :: x(1:dim,1:x_size)              ! points to interpolate into
!!$    INTEGER, INTENT (IN) :: var(1:var_size)                 ! variables to interpolate for
!!$    REAL(pr), INTENT (OUT) :: res(1:var_size,1:x_size)      ! interpolation result
!!$    INTEGER :: box(2**dim), &   ! bounding box
!!$         point_number, &
!!$         low_index(dim), &      ! index of low corner of the bounding box
!!$         dd, &                  ! size of the box
!!$         i,j,k
!!$         
!!$    LOGICAL :: box_not_complete
!!$    
!!$    DO point_number=1,x_size
!!$       ! find index coordinates of the lower corner of the bounding box for each point
!!$       CALL get_xx_index( x, low_index )
!!$
!!$       ! find complete box starting from j_lev level
!!$       dd = 1
!!$       box_not_complete = .TRUE.
!!$       DO WHILE (box_not_complete)    ! test all larger boxes
!!$          box_not_complete = .FALSE.
!!$          DO i=1,2**dim               ! sweep box points
!!$             coord = low_index
!!$             DO j=1,dim
!!$                
!!$             END DO
!!$          END DO
!!$          dx = dx*2
!!$       END DO
!!$       
!!$    END DO
!!$    PRINT *, i_c
!!$    STOP
!!$    
!!$  END SUBROUTINE DB_interpolate_wrk

  !--------------------------------------------------------------------------------
  ! empty subroutines
  SUBROUTINE pre_init_DB
  END SUBROUTINE pre_init_DB


  SUBROUTINE delta_from_mask(delta_local,mask_local,nlocal)
    !--Calculates weights for volume integration based on the Trapezoid
    !--integration rule (second-order accurate).
    USE precision
    USE sizes
    USE wlt_vars
    USE util_vars
    USE pde
    IMPLICIT NONE

    INTEGER, INTENT(IN) :: nlocal 
    REAL (pr), DIMENSION(nlocal), INTENT(IN)::    mask_local
    REAL (pr), DIMENSION(nlocal), INTENT(INOUT):: delta_local
    INTEGER :: i, j, k, m, idim
    REAL (pr), DIMENSION(j_lev) :: delta_lev
    INTEGER, DIMENSION(3) :: ixyz, ixyz1
    REAL (pr), DIMENSION(dim) :: lxyz
    INTEGER :: i1(1), ii, j_df, wlt_type, face_type, dir_type, step, step_cube
    INTEGER, DIMENSION(0:dim) :: i_p, i_p_cube
    INTEGER, DIMENSION(3) :: i_l, i_h
    INTEGER, DIMENSION(3,1) :: ZERO

    i_c = .FALSE.
    i_d = .FALSE.
    ZERO = 0
    ixyz = 1 ! set up the last dimensions if 2-D case
    ixyz1 = 1 ! set up the last dimensions if 2-D case
    DO j = 1, j_lev
       delta_lev(j)   =  PRODUCT(h(j,1:dim))**(1.0_pr/REAL(dim,pr)) !seting delta for each level of esolution
    END DO

    delta_local =  PRODUCT(h(j_mn,1:dim))**(1.0_pr/REAL(dim,pr)) !seting delta to the coarsest level

    i_p_cube(0) = 1
    i_p(0) = 1
    DO i=1,dim
       i_p(i) = i_p(i-1)*(1+nxyz(i))
    END DO
    DO j = 1, j_lev
       DO wlt_type = MIN(j-1,1),2**dim-1
          DO face_type = 0, 3**dim - 1
             DO j_df = j, j_lev
                DO k = 1, indx_DB(j_df,wlt_type,face_type,j)%length  
                   i = indx_DB(j_df,wlt_type,face_type,j)%p(k)%i
                   ii = indx_DB(j_df,wlt_type,face_type,j)%p(k)%ixyz
                   ixyz(1:dim) = INT(MOD(ii-1,i_p(1:dim))/i_p(0:dim-1))
                   i_c(ixyz(1),ixyz(2),ixyz(3)) = .TRUE.                                  ! ALL active grid points
                   i_d(ixyz(1),ixyz(2),ixyz(3)) = (mask_local(i) == 1.0_pr)               ! ALL grid points on the mask
                END DO
             END DO
          END DO
       END DO
    END DO
 
    !----------------- Calculate local delta by level ----------------------
    step_cube = 1
    DO j = j_mn+1,j_lev
       step = 2**(j_lev-j)
       DO wlt_type = MIN(j-1,1),2**dim-1
          DO face_type = 0, 3**dim - 1
             DO j_df = j, j_lev
                DO k = 1, indx_DB(j_df,wlt_type,face_type,j)%length  
                   i = indx_DB(j_df,wlt_type,face_type,j)%p(k)%i
                   ii = indx_DB(j_df,wlt_type,face_type,j)%p(k)%ixyz
                   ixyz(1:dim) = INT(MOD(ii-1,i_p(1:dim))/i_p(0:dim-1))
                   IF( i_d(ixyz(1),ixyz(2),ixyz(3)) ) THEN
                      i_l(:) = MAX(ixyz(:)-step,ZERO(:,1))
                      i_h(:) = MIN(i_l(:)+2*step,nxyz(1:dim)) 
                      i_l(:) = MAX(i_h(:)-2*step,ZERO(:,1))
                      i_l(:) = (1-prd(:))*i_l(:) + prd(:)*( ixyz - step )
                      i_h(:) = (1-prd(:))*i_h(:) + prd(:)*( ixyz + step )
                      !getting i based on location
                      i_p_cube(1:dim) = (i_h-i_l)/step_cube + 1
                      DO idim =1, dim
                         i_p_cube(idim) = i_p_cube(idim-1)*i_p_cube(idim)
                      END DO
                      DO m = 1,i_p_cube(dim)
                         ixyz1 = i_l + INT(MOD(m-1,i_p_cube(1:dim))/i_p_cube(0:dim-1))*step_cube
                         ixyz1(1:dim) = (1-prd(1:dim))*ixyz1(1:dim) + prd(1:dim)*MOD(ixyz1(:)+9*nxyz(1:dim),nxyz(1:dim))
                         IF( i_c(ixyz1(1),ixyz1(2),ixyz1(3)) ) THEN
                            CALL get_indices_by_coordinate(1,ixyz1(:),j_lev,i1(1),0)
                            delta_local(i1(1)) = delta_lev(j)
                         END IF
                      END DO
                   END IF
                END DO
             END DO
          END DO
       END DO
    END DO

    !========= cleaning i_c and i_d ==================
    DO j = 1, j_lev
       DO wlt_type = MIN(j-1,1),2**dim-1
          DO face_type = 0, 3**dim - 1
             DO j_df = j, j_lev
                DO k = 1, indx_DB(j_df,wlt_type,face_type,j)%length  
                   i = indx_DB(j_df,wlt_type,face_type,j)%p(k)%i
                   ii = indx_DB(j_df,wlt_type,face_type,j)%p(k)%ixyz
                   ixyz(1:dim) = INT(MOD(ii-1,i_p(1:dim))/i_p(0:dim-1))
                   i_c(ixyz(1),ixyz(2),ixyz(3)) = .FALSE.                                  ! ALL active grid points
                   i_d(ixyz(1),ixyz(2),ixyz(3)) = .FALSE.               ! ALL grid points on the mask
                END DO
             END DO
          END DO
       END DO
    END DO

  END SUBROUTINE delta_from_mask


END MODULE wlt_trns_mod
!**************************************************************************
! End wavelet transform subroutines
!**************************************************************************

