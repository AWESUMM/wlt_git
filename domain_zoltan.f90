!
! Zoltan library based domain decomposition module
!
MODULE domain_zoltan
  IMPLICIT NONE
  PRIVATE
  PUBLIC :: zoltan_domain_decomp

CONTAINS
  
  SUBROUTINE zoltan_domain_decomp( dim_loc, Number_Vertices, Mxyz_Vertices, nproc, &
       Nwlt_Vertices, Nwlt_Surface_Vertices, N_Update, N_Predict, &
       proc_to_tree, task, Partitioning_Method, EdgeWeight_Global_SafetyFactor, &
       Vertices_Coordinates, verb_level, error_out, myrank )
    USE wlt_vars 
    USE precision
    IMPLICIT NONE
    INTEGER, INTENT(IN) ::  &
         dim_loc, &                                         ! problem geometric dimension
         Number_Vertices, &                                 ! total number of vertices
         Mxyz_Vertices(dim_loc), &                          ! number of vertices in each direction
         nproc, &                                           ! total number of processors
         Nwlt_Vertices(Number_Vertices), &                  ! nwlt per vertex
         Nwlt_Surface_Vertices(dim_loc,Number_Vertices), &      ! nwlt on min surfaces of vertex
         N_Update, N_Predict, &                             ! n_update, n_predict
         task, &                                            ! 0-partition, 1-repartition, 2-refine
         Partitioning_Method, &                             ! 1-Geometric, 3-HyperGraph
         verb_level, &                                      ! Zoltan debug level (0-10)
         myrank                                             ! processor's rank
    REAL(pr), INTENT(IN) :: &
         EdgeWeight_Global_SafetyFactor, &             ! 0.1 <-> 10% of nodes in boundary zone
         Vertices_Coordinates(Number_Vertices*dim_loc) ! (useg for geometric partitioning)
    INTEGER, INTENT(INOUT) :: &
         proc_to_tree(Number_Vertices), &              ! input/output partition proc_to_tree(:)
         error_out                                     ! should be zero if everything is OK
    
    
    INTEGER :: ii, i, j, i_t, iter_count, &
         Number_Vertices_Neighbors(Number_Vertices), &       ! number of neighbours for each vertice
         Vertices_Global_ID(Number_Vertices), &              ! ID starting from 1 of all vertices in the order or processor rank
         Edges_Vertices_ID(Number_Vertices*2*dim_loc), &     ! ID of neighbours for each vertice
         Edges_Vertices_Nwlt(Number_Vertices*2*dim_loc), &   ! Nwlt on edges for each vertice
         Number_Vertices_OnEachProcess(nproc), &             ! number of vertices on each processor
         tmp_OnEachProcess(0:nproc), &                       ! per processor counter
         Global_Processor_Flag(Number_Vertices), &           ! Zoltan OUTPUT
         i_p(0:dim_loc), &
         face(dim_loc), &
         ixyz(dim_loc), &
         ixyz_t(dim_loc), &
         num_of_ones
    
    INTEGER, PARAMETER :: &     ! Zoltan partitioning input parameter
         PARTITION = 0,   &
         REPARTITION = 1, &
         REFINE = 2
    
    
    ! no errors marker
    error_out = 0
    
    
    ! check available partitioning methods
    IF (Partitioning_Method.NE.1.AND.Partitioning_Method.NE.3.AND.Partitioning_Method.NE.4) THEN
       PRINT *, 'ERROR: this partitioning method is not currently supported'
       error_out = 1
       RETURN
    END IF
    
    
    
    !
    ! set Vertices_Global_ID(:), Number_Vertices_OnEachProcess(:)
    !
    Number_Vertices_OnEachProcess(1:nproc) = 0
    IF (task.EQ.PARTITION) THEN !.OR.Partitioning_Method.EQ.1) THEN
       !
       ! assign initial number of trees per processor
       ! and initial ID distribution (in the order of processor rank)
       !
       ! Partitioning_Method != PARTITION valid for HYPERGRAPH or GRAPH only
       !
       Number_Vertices_OnEachProcess(1) = Number_Vertices
       DO i=1,Number_Vertices
          Vertices_Global_ID(i)=i	          
       END DO
    ELSE
       !
       ! compute initial number of trees per processor
       ! and initial ID distribution (in the order of processor rank)
       ! using proc_to_tree(1:Number_Vertices) array
       ! whose entries \in [0:nproc-1]
       tmp_OnEachProcess(0:nproc) = 0
       DO i=1,Number_Vertices
          tmp_OnEachProcess( proc_to_tree(i)+1 ) = tmp_OnEachProcess( proc_to_tree(i)+1 ) + 1
       END DO
       DO i=1,nproc
          tmp_OnEachProcess(i) = tmp_OnEachProcess(i) + tmp_OnEachProcess(i-1)
          ! counters set: tmp_(i-1)+1 points into the cell to be written
       END DO
       DO i=1,Number_Vertices
          ! sweeping proc_to_tree array:
          ! the current tree processor's rank is proc_to_tree(i),
          ! the current tree ID is i,
          ! the location to write is tmp_(rank)+current_num
          Number_Vertices_OnEachProcess( proc_to_tree(i)+1 ) = &
               Number_Vertices_OnEachProcess( proc_to_tree(i)+1 ) + 1
          Vertices_Global_ID &
               ( &
               tmp_OnEachProcess(proc_to_tree(i)) + &
               Number_Vertices_OnEachProcess( proc_to_tree(i)+1 ) &
               ) = i
       END DO
    END IF
    
    
    IF (myrank.EQ.0.AND.verb_level.GT.2) THEN
       PRINT *, 'DEBUG: zoltan_domain_decomp, before ZOLTAN_DOMAIN_PARTITIONING'
       PRINT *, '       Number_Vertices_OnEachProcess = '
       PRINT *, Number_Vertices_OnEachProcess
       PRINT *, '       Vertices_Global_ID = '
       PRINT *, Vertices_Global_ID
    END IF
    
    
    ! for a regular grid
    ! set Number_Vertices_Neighbors(:), Edges_Vertices_ID(:),
    ! and Vertices_Coordinates(:)
    ! e.g.
    !       7-----8-----9
    !       |     |     |         Number_Vertices_Neighbors = (/2,3,2, 3,4,3, 2,3,2/)
    !       4-----5-----6   ==>   Edges_Vertices_ID(:) =(/2,4, 1,5,3, 2,6, 1,5,7, 2,4,8,6, .../)
    !       |     |     |
    !       1-----2-----3         (above they are in natural tree order, before passing to Zoltan
    !                              we change them to processor rank order, similar to Vertices_Global_ID)
    i_p(0) = 1
    DO i=1,dim_loc
       i_p(i) = i_p(i-1)*Mxyz_Vertices(i)
    END DO
    num_of_ones = 1                ! running index in Edges_Vertices_ID(:)
    Number_Vertices_Neighbors = 0
    Edges_Vertices_ID   = 0
    Edges_Vertices_Nwlt = 0

    DO ii=1,Number_Vertices
       ! changing to processor rank order, similar to Vertices_Global_ID
       i = Vertices_Global_ID(ii)

       ixyz(1:dim_loc) = INT(MOD(i-1,i_p(1:dim_loc))/i_p(0:dim_loc-1))
!!$       ! in case we do not need Edges_Vertices_ID(:)
!!$       face = -1 + INT((ixyz(1:dim_loc)+1)/Mxyz_Vertices(1:dim_loc)) &
!!$            + MIN(1,MOD(ixyz(1:dim_loc),Mxyz_Vertices(1:dim_loc)))
!!$       num_of_ones = SUM(ABS(face(1:dim_loc)))
!!$       Number_Vertices_Neighbors(i) = dim_loc*2 - num_of_ones
       DO j=1,dim_loc   !need check for periodicity
          ixyz_t = ixyz
          ixyz_t(j) = ixyz_t(j) + 1     ! tree on upper boundary
          IF (ixyz_t(j).LT.Mxyz_Vertices(j)) THEN
             i_t = 1+SUM(ixyz_t(1:dim_loc)*i_p(0:dim_loc-1))
             Number_Vertices_Neighbors(ii) = Number_Vertices_Neighbors(ii) + 1
             Edges_Vertices_ID(num_of_ones) = i_t           ! Global ID on max face
             Edges_Vertices_Nwlt(num_of_ones) = Nwlt_Surface_Vertices(j,i_t)
             !if edge is empty and successive tree exists (for case of j_tree > j_mn)
             iter_count = 0  !WHILE loop limiter
             DO WHILE (Nwlt_Surface_Vertices(j,i_t).EQ.0 .AND. ixyz_t(j)+1.LT.Mxyz_Vertices(j).AND. iter_count < (2**(j_tree_root-j_mn)-1)) 
                ixyz_t(j) = ixyz_t(j) + 1     ! tree on upper boundary
                i_t = 1+SUM(ixyz_t(1:dim_loc)*i_p(0:dim_loc-1))
                Edges_Vertices_Nwlt(num_of_ones) = Nwlt_Surface_Vertices(j,i_t)
                iter_count = iter_count + 1
             END DO
             num_of_ones = num_of_ones + 1
          END IF
          ! tree on lower boundary
          ixyz_t(j) = ixyz(j) - 1
          IF (ixyz_t(j).GE.0) THEN      
             i_t = 1+SUM(ixyz_t(1:dim_loc)*i_p(0:dim_loc-1))
             Number_Vertices_Neighbors(ii) = Number_Vertices_Neighbors(ii) + 1
             Edges_Vertices_ID(num_of_ones) = i_t ! Global ID on min face
             Edges_Vertices_Nwlt(num_of_ones) = Nwlt_Surface_Vertices(j,i) !Empty edges are accounted for by hypergraph partitioner by neighboring tree 
             num_of_ones = num_of_ones + 1
          END IF
       END DO
    END DO

    Global_Processor_Flag(:) = proc_to_tree(:) !input in global ID order for querey subroutines

    IF (myrank.EQ.0.AND.verb_level.GT.2) THEN
       PRINT *, '=============================================================='
       PRINT *, 'DEBUG: zoltan_domain_decomp, before ZOLTAN_DOMAIN_PARTITIONING'
       PRINT *, 'Number_Vertices_OnEachProcess = ', Number_Vertices_OnEachProcess
       WRITE (*,'(" ")')
       j = 0
       DO i=1,Number_Vertices
          WRITE (*,'(A,I3)')                    ' Vertices_Global_ID = ', Vertices_Global_ID(i)
          WRITE (*,ADVANCE='NO',FMT='(A,I3,A)') ' Edges_Vertices_ID(',Number_Vertices_Neighbors(i),') = '
          DO ii=1,Number_Vertices_Neighbors(i)
             WRITE (*,ADVANCE='NO',FMT='(I3,X)') Edges_Vertices_ID(ii+j)
          END DO
          WRITE (*,'(" ")'); WRITE (*,'(" ")')
          j = j + Number_Vertices_Neighbors(i)
       END DO
       PRINT *, 'Nwlt_Vertices=', Nwlt_Vertices
       PRINT *, '=============================================================='
    END IF
    
    CALL ZOLTAN_DOMAIN_PARTITIONING ( &
         Number_Vertices, &                   ! total number of vertices on all the proessors
         Number_Vertices_Neighbors, &         ! number of neighbours for each vertex
         Number_Vertices_OnEachProcess, &     ! number of vertices on each processor
         Vertices_Global_ID, &                ! ID starting from 1 of all vertices in the order of processor rank
         Edges_Vertices_ID, &                 ! ID of neighbours for each vertex
         Edges_Vertices_Nwlt, &               ! Nwlt on each edge
         Nwlt_Vertices, &                     ! nwlt per vertex
         N_Predict, &
         N_Update, &
         dim_loc, &
         Vertices_Coordinates, &
         Partitioning_Method, &               ! 1-Geometric, 2-Graph, 3-HyperGraph, 4-HSFC
         EdgeWeight_Global_SafetyFactor, &    ! 0.1 <-> 10% of nodes in boundary zone
         task, &                              ! 0-partition, 1-repartition, 2-refine
         verb_level, &                        ! Zoltan debug level (0-10)
         Global_Processor_Flag, &             ! OUTPUT, new processor for that Global_ID
         error_out )
    
    
    IF (error_out.NE.0) THEN
       WRITE (*,'(A,I4)') 'ERROR: internal Zoltan library error at proc ', myrank
       RETURN
    END IF
    
    ! assign new proc_to_tree(:)
    ! Global_Processor_Flag is in processor rank order, like Vertices_Global_ID
    ! (Vertices_Global_ID is not changing in partitioning subroutines)
    DO i=1,Number_Vertices
       proc_to_tree( Vertices_Global_ID(i) ) = Global_Processor_Flag(i)
    END DO

    
    
  END SUBROUTINE zoltan_domain_decomp

END MODULE domain_zoltan
