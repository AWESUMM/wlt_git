!-------------------------------------------------------!
! define whether to use C or Fortran style node pointer !
#ifdef TREE_NODE_POINTER_STYLE_C
#undef TREE_NODE_POINTER_STYLE_C
#endif
#ifdef TREE_NODE_POINTER_STYLE_F
#undef TREE_NODE_POINTER_STYLE_F
#endif
#if ((TREE_VERSION == 0) || (TREE_VERSION == 1))
#define TREE_NODE_POINTER_STYLE_C
#elif ((TREE_VERSION == 2) || (TREE_VERSION == 3))
#define TREE_NODE_POINTER_STYLE_F
#else
#error Please define correct TREE_VERSION
#endif
! define whether to use C or Fortran style node pointer !
!-------------------------------------------------------!

#ifdef TREE_NODE_POINTER_STYLE_C
#define DECLARE_NODE_POINTER INTEGER(pointer_pr)
#elif defined TREE_NODE_POINTER_STYLE_F
#define DECLARE_NODE_POINTER TYPE(node), POINTER
#endif

MODULE db_tree
  USE precision
!!$  USE wlt_trns_vars

#if ((TREE_VERSION == 2) || (TREE_VERSION == 3))
    USE tree_database_f      ! use Fortran database instead of C/C++ tree
#endif

  IMPLICIT NONE
  PRIVATE
  REAL(pr), DIMENSION(:), ALLOCATABLE :: fvec
  INTEGER, PUBLIC  :: j_mn_init_DB
  INTEGER, DIMENSION(:), ALLOCATABLE :: mxyz_DB, nxyz_DB, prd_DB
  INTEGER, DIMENSION(:), ALLOCATABLE :: ixyz_DB, ixyz1_DB, ixyz_wlt_DB, ixyz_shift
  INTEGER, DIMENSION(:), ALLOCATABLE :: i_l, i_h
  INTEGER, DIMENSION(:), ALLOCATABLE, PUBLIC :: ivec
  INTEGER, DIMENSION(:,:), ALLOCATABLE :: wlt_type_DB
  INTEGER, ALLOCATABLE :: lh_diff(:,:,:,:,:,:), lh_wlt(:,:,:,:,:,:,:)
  REAL (pr), DIMENSION (:,:), ALLOCATABLE, PUBLIC :: xx_DB, h_DB
  REAL(pr), ALLOCATABLE :: wgh_df_DB(:,:,:,:,:,:)
#ifdef COP_1
  ! COP_1 - compilation optimization parameter 1 - define to call wgh_DB() as a function
  !         and save memory for large J_MX .inp parameters
  PRIVATE :: wgh_DB
#else
  !         default is undefined and wgh_DB() is an allocatable array
  REAL(pr), ALLOCATABLE :: wgh_DB(:,:,:,:,:,:)
#endif

  INTEGER, DIMENSION(0:14), PUBLIC :: idp
  INTEGER, PARAMETER, PUBLIC ::   pos_sig  = 0,  & !   /* 0 0001 */
       pos_adj  = 1,  & !   /* 0 0010 */
       pos_gho  = 2,  & !   /* 0 0100 */
       pos_old  = 3,  & !   /* 0 1000 */
       pos_msk  = 4     !   /* 1 0000 */
  INTEGER, PARAMETER, PUBLIC ::   list_sig = 1,  & !   significant + adjacent points
       list_gho = 2,  & !   ghost points only
       list_all = 3     !   significant + adjacent + ghost  points

  INTEGER, PARAMETER, PUBLIC ::   beg_list = 1, end_list = 0 ! parameters to control whehter point is added to the end or beginning of the list
  INTEGER, PARAMETER :: predict_stage = 0, update_stage = 1, low_limit  = 0, high_limit  = 1
  ! index in integer vector in DB:
  INTEGER, PARAMETER, PUBLIC ::  nvarI_nwlt = 1, & ! inverse maping to 1...nwlt
       nvarI_jD   = 2    ! derivative level
  INTEGER, PARAMETER, PRIVATE :: NvecI_DB   = 2    ! # of integer positions per node in tree-structure
  INTEGER, PRIVATE ::            NvecR_DB          !NvecR_DB - # of real(pr) variables per node in tree-structure

  !????????????????????????????
  INTEGER, DIMENSION (:,:), ALLOCATABLE, PUBLIC:: indx
  !????????????????????????????
!!$  LOGICAL :: first_call_of_sig_adapt ! true for the first significant_DB call from adapt_grid


  !===================================================================!
  !               PUBLIC FUNCTIONS AND SUBROUTINES                    !  
  !===================================================================!

  PUBLIC :: add_ghost_DB,      &
       add_nodes_DB,           &
       adjacent_wlt_DB,        &
       bnd_zone_DB,            &
       clean_DB,               &
       clean_id_DB,            &
       compare_DB,             &
       count_DB,               &
       diff_diag_aux_DB,       &
       indices_DB,             &
       init_DB_tree_aux,       & ! set tree and the domain decomposition
       ixyz2j_DB,              &
       read_DB,                &
       reconstr_check_DB,      &
       set_id,                 &
       setup_lh_bounds_DB,     & ! set low/high bounds
       significant_wlt_DB,     &
       test_DB_values,         &
       weights_DB,             &
       wlt_diff_DB,            &
       wlt_trns_DB,            &
       wlt_trns_aux_DB,        &
       write_DB,               &
       zone_wlt_DB,            &
       is_ok,                  & ! tests if node pointer is valid
       make_list_and_request,  & ! make list of the nodes to request from other processors
       request_boundary_nodes, & ! request unknown number of boundary nodes from other processors
       make_list_and_inform,   & ! make list of the nodes and send them to other processors
       make_lists_for_communications         ! the first part of make_list_and_request()
  
#ifdef MULTIPROC
  PUBLIC :: add_ghost_DB_parallel_antea, &
       add_ghost_DB_parallel_postea,     &
       request_known_list                    ! and the second part of it
#endif

!!!!!!!!!!!!!!!! once interface functions are written, these need to be replaced by interface functions
!!!!!!!!!!!!!!!! also variable declared public need to be set proivite and nouse for them in main program.
  
CONTAINS



#ifdef COP_1
  ! COP_1 - compilation optimization parameter 1 - define to call wgh_DB() as a function
  !         and save memory for large J_MX .inp parameters
  !         default is undefined and wgh_DB() is an allocatable array
  FUNCTION wgh_DB (ipm, i, j, wlt_fmly, trnsf_type, idim)
    INTEGER, INTENT(IN) :: &
         ipm,              & ! -2*MAX(MAXVAL(n_prdct),MAXVAL(n_updt)) : 2*MAX(MAXVAL(n_prdct),MAXVAL(n_updt))
         i,                & ! 0:MAXVAL(nxyz_DB)
         j,                & ! 2:j_mx
         wlt_fmly,         & ! 0:n_wlt_fmly
         trnsf_type,       & ! 0:n_trnsf_type
         idim                ! 1:dim
    INTEGER :: step, step2
    REAL(pr) :: wgh_DB
    
    step = 2**(j_mx-j)
    step2 = 2*step
    
!!$                   !****** PREDICT STAGE ****** 
!!$                   DO i = step,nxyz_DB(idim)-prd_DB(idim),step2
!!$                      i_l(idim) = lh_wlt(low_limit, predict_stage,wlt_fmly,trnsf_type,j,i,idim)
!!$                      i_h(idim) = lh_wlt(high_limit,predict_stage,wlt_fmly,trnsf_type,j,i,idim)

!!$                      DO ipm = i_l(idim),i_h(idim)
!!$                         !PRINT *, ipm, i, j, wlt_fmly,trnsf_type,idim
!!$
!!$                         wgh_DB(ipm,i,j,wlt_fmly,trnsf_type,idim) =  &
!!$                              wgh (ipm, i, i_l(idim), i_h(idim), step, prd_DB(idim), nxyz_DB(idim), xx_DB(0:nxyz_DB(idim),idim))

!!$                      END DO
!!$                   END DO
!!$                   !****** UPDATE STAGE ****** 
!!$                   DO i = 0,nxyz_DB(idim)-prd_DB(idim),step2
!!$                      i_l(idim) = lh_wlt(low_limit, update_stage,wlt_fmly,trnsf_type,j,i,idim)
!!$                      i_h(idim) = lh_wlt(high_limit,update_stage,wlt_fmly,trnsf_type,j,i,idim)
!!$                      DO ipm = i_l(idim),i_h(idim)
                         wgh_DB(ipm,i,j,wlt_fmly,trnsf_type,idim) =  &
                              wgh (ipm, i, i_l(idim), i_h(idim), step, prd_DB(idim), nxyz_DB(idim), xx_DB(0:nxyz_DB(idim),idim))
!!$                      END DO
!!$                   END DO

  END FUNCTION wgh_DB
#endif



  !===================================================================!
  !               INTERFACE LAYER PUBLIC SUBROUTINES                  !  
  !===================================================================!


  !===================================================================!
  !               PRIVITE DATABASE FUNCTIONS AND SUBROUTINES          !  
  !===================================================================!

  SUBROUTINE init_DB_tree_aux( nvec )
    USE io_3D_vars              ! IC_restart_mode
    USE wlt_vars                ! j_tree_root
    USE parallel                ! parallel domain decompose()
    USE debug_vars
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: nvec
    INTEGER  :: i
    LOGICAL :: domain_change    ! parallel domain decompose() output parameter

    NvecR_DB = nvec
    
    ! test if previously allocated - for multiple init_DB calls in post-processing
    IF(.NOT.ALLOCATED(mxyz_DB)) ALLOCATE(mxyz_DB(1:dim))
    IF(.NOT.ALLOCATED(nxyz_DB)) ALLOCATE(nxyz_DB(1:dim))
    IF(.NOT.ALLOCATED(prd_DB))  ALLOCATE(prd_DB(1:dim))
    IF(.NOT.ALLOCATED(ixyz_DB)) ALLOCATE(ixyz_DB(1:dim))
    IF(.NOT.ALLOCATED(ixyz1_DB)) ALLOCATE(ixyz1_DB(1:dim))
    IF(.NOT.ALLOCATED(ixyz_wlt_DB)) ALLOCATE(ixyz_wlt_DB(1:dim))
    IF(.NOT.ALLOCATED(ixyz_shift)) ALLOCATE(ixyz_shift(1:dim))
    IF(.NOT.ALLOCATED(ivec)) ALLOCATE(ivec(1:dim))
    IF(.NOT.ALLOCATED(fvec)) ALLOCATE(fvec(1:NvecR_DB))
    IF(.NOT.ALLOCATED(i_l)) ALLOCATE(i_l(1:dim))
    IF(.NOT.ALLOCATED(i_h)) ALLOCATE(i_h(1:dim))

    mxyz_DB(1:dim) = mxyz(1:dim)
    prd_DB(1:dim)  = prd(1:dim)
    nxyz_DB = mxyz_DB * 2**(j_mx-1)
    !------------- node id classes -----------------
    DO i = 0,14
       idp(i) = IBSET(0,i)
    END DO
    DO i = 1,dim
       ivec(i) = dim-i+1
    END DO

    
    !--------------- setup tree structure -----------------------------------------
    ! distribute domains among processors
    ! (j_tree_root will be used in domain decomposition to find the number of trees,
    !   optional integer argument meth - method of decomposition,
    !   par_size, par_rank - set in parallel_init(), par_proc_tree - in domain decomposition)

    !   This is the first call of domain partitioning,
    !   the second one is in adapt_grid after synchronizing all the adjacent.
    CALL parallel_domain_decompose (domain_change, VERB=(par_rank.EQ.0), METH=domain_meth, FIRSTCALL=.TRUE.)
    
    ! initialize the database, inform it about the decomposition
    CALL DB_declare_globals ( mxyz_DB, prd_DB, j_tree_root, j_mx, dim, NvecR_DB, NvecI_DB, &
         par_size, par_rank, par_proc_tree )
    !CALL parallel_finalize; STOP 'in init_DB_tree_aux: 175'

    IF (par_rank.EQ.0.AND. BTEST(debug_level,1) ) THEN
       WRITE(*,'("database is initialized: j_mn =",I3," j_mx =",I3," dim =",I2," NvecR =",I2," NvecI =",I2)') &
            j_mn,j_mx,dim,NvecR_DB,NvecI_DB
       WRITE(*,'("mxyz =(",10(I4,$))') mxyz_DB ; WRITE(*,'(" )")')
       WRITE(*,'("Nxyz =(",10(I8,$))') nxyz_DB ; WRITE(*,'(" )")')
       WRITE(*,'("prd =(",10(I2,$))') prd_DB ; WRITE(*,'(" )")')
    END IF
    
  END SUBROUTINE init_DB_tree_aux

  SUBROUTINE setup_lh_bounds_DB( RESET )
    USE precision
    USE wlt_vars
    USE share_consts
    USE wlt_trns_util_mod
    USE debug_vars
!!$    USE parallel
    IMPLICIT NONE
    INTEGER, INTENT(IN), OPTIONAL :: RESET               ! n_updt/n_prdct has been changed
    INTEGER  :: i, i1, j, jj, ipm 
    INTEGER  :: meth, order
    INTEGER :: idim, wlt_fmly, trnsf_type, step, step2, step_j, step_jj
    INTEGER, DIMENSION(2*dim)::  ibch
    INTEGER, DIMENSION(0:n_df) :: nDD ,ii_l, ii_h, ZERO, ONE
    REAL (pr) :: dxyz(1:dim)
    INTEGER:: do_reset
    INTEGER, PARAMETER :: INI = 15
    
    ! 1111   - 15 - initial call
    ! 0001   -  1 - reset lh_wlt
    ! 0010   -  2 - lh_diff
    ! 0100   -  4 - wgh_DB
    ! 1000   -  8 - wgh_df_DB
    do_reset = INI
    IF (PRESENT(RESET)) do_reset = RESET
    IF( BTEST(debug_level,4) ) PRINT *, 'setup_lh_bounds_DB: n_prdct, n_updt=', n_prdct, n_updt
    
    
    !----------------- setup grid --------------------
    IF (do_reset.EQ.INI) THEN
       !  lh_wlt(low/high,predict/update,wlt_type(internal/external),j,i,idim)
       IF(.NOT.ALLOCATED(xx_DB)) ALLOCATE(xx_DB(0:MAXVAL(nxyz_DB(1:dim)),1:dim)); xx_DB = 0.0_pr
       IF(.NOT.ALLOCATED(h_DB))  ALLOCATE(h_DB(1:j_mx,dim)); h_DB = 0.0_pr
       
       CALL set_xx (xx_DB,h_DB,nxyz_DB,MAXVAL(nxyz_DB),j_mx)
    END IF
    
    !----------------- setup left and right limits for update and predict stage
    IF (.NOT.ALLOCATED(lh_wlt)) &
         ALLOCATE(lh_wlt(0:1,0:1,0:n_wlt_fmly,0:n_trnsf_type,1:j_mx,0:MAXVAL(nxyz_DB(1:dim)-prd_DB(1:dim)),1:dim))
    IF( BTEST(do_reset,0) ) THEN
       IF( BTEST(debug_level,4) ) PRINT *, 'reset lh_wlt'
       lh_wlt = 0
       
       DO wlt_fmly = 0, n_wlt_fmly
          DO trnsf_type = 0,n_trnsf_type 
             ibch(1:2*dim) = 0
             IF(trnsf_type == 1) ibch(1:2*dim) = ibc(1:2*dim)
             DO idim = 1,dim ! transform along idim direction
                DO j=1,j_mx
                   step = 2**(j_mx-j)
                   step2 = 2*step
                   ! PREDICT STAGE
                   DO i = step,nxyz_DB(idim)-prd_DB(idim),step2
                      i_l(idim) = MAX(i-(2*n_prdct(wlt_fmly)-1)*step,ibch(2*idim-1)*step2)
                      i_h(idim) = MIN(i_l(idim)+(2*n_prdct(wlt_fmly)-1)*step2,nxyz_DB(idim)-ibch(2*idim)*step2) 
                      i_l(idim) = MAX(i_h(idim)-(2*n_prdct(wlt_fmly)-1)*step2,ibch(2*idim-1)*step2)
                      i_l(idim) = (1-prd_DB(idim))*i_l(idim) + prd_DB(idim)*( i - (2*n_prdct(wlt_fmly) - 1)*step )
                      i_h(idim) = (1-prd_DB(idim))*i_h(idim) + prd_DB(idim)*( i + (2*n_prdct(wlt_fmly) - 1)*step )
                      i_l(idim) = (i_l(idim) - i - step)/step2
                      i_h(idim) = (i_h(idim) - i - step)/step2
                      IF  (i_l(idim)+i_h(idim)+1 < 0 .AND. i_h(idim) >= 0 .AND. prd(idim) ==0 ) THEN  ! the right boundary
                         i_l(idim) = MAX(i_l(idim),-(i_h(idim)+1)-n_assym_prdct(wlt_fmly,trnsf_type),-(i+step)/step2+ibch(2*idim-1))
                      ELSE IF  (i_l(idim)+i_h(idim)+1 < 0 .AND. i_h(idim) == -1 .AND. prd(idim) ==0 ) THEN  ! right on the right internal transform boundary
                         i_l(idim) = MAX(i_l(idim),-1-n_assym_prdct_bnd(wlt_fmly,trnsf_type),-(i+step)/step2+ibch(2*idim-1) )
                      ELSE IF  (i_l(idim)+i_h(idim)+1 > 0 .AND. i_l(idim) < 0 .AND. prd(idim) ==0 ) THEN    !  the left boundary
                         i_h(idim) = MIN(i_h(idim),-i_l(idim)-1+n_assym_prdct(wlt_fmly,trnsf_type),(nxyz_DB(idim)-i-step)/step2-ibch(2*idim) )
                      ELSE IF  (i_l(idim)+i_h(idim)+1 > 0 .AND. i_l(idim) == 0 .AND. prd(idim) ==0 ) THEN   ! right on the left internal transform boundary
                         i_h(idim) = MIN(i_h(idim),n_assym_prdct_bnd(wlt_fmly,trnsf_type),(nxyz_DB(idim)-i-step)/step2-ibch(2*idim))   
                      END IF
                      lh_wlt(low_limit, predict_stage,wlt_fmly,trnsf_type,j,i,idim) = i_l(idim)
                      lh_wlt(high_limit,predict_stage,wlt_fmly,trnsf_type,j,i,idim) = i_h(idim)
!!$                      IF (par_rank.EQ.0) PRINT *, 'lh_wlt prd',&
!!$                           low_limit, high_limit, predict_stage,wlt_fmly,trnsf_type,j,i,idim, i_l(idim),i_h(idim)
                      
                   END DO
                   ! UPDATE STAGE
                   DO i = 0,nxyz_DB(idim)-prd_DB(idim),step2
                      i_l(idim) = MAX(i-(2*n_updt(wlt_fmly)-1)*step,step)
                      i_h(idim) = MIN(i_l(idim)+(2*n_updt(wlt_fmly)-1)*step2,nxyz_DB(idim)-step) 
                      i_l(idim) = MAX(i_h(idim)-(2*n_updt(wlt_fmly)-1)*step2,step)
                      i_l(idim) = (1-prd_DB(idim))*i_l(idim) + prd_DB(idim)*( i - (2*n_updt(wlt_fmly) - 1)*step )
                      i_h(idim) = (1-prd_DB(idim))*i_h(idim) + prd_DB(idim)*( i + (2*n_updt(wlt_fmly) - 1)*step )
                      i_l(idim) = (i_l(idim) - i - step)/step2
                      i_h(idim) = (i_h(idim) - i - step)/step2
                      IF  (i_l(idim)+i_h(idim)+1 < 0 .AND. i_h(idim) >= 0 .AND. prd(idim) == 0) THEN                              ! the right boundary
                         i_l(idim) = MAX(i_l(idim),-(i_h(idim)+1)-n_assym_updt(wlt_fmly,trnsf_type),-i/step2)
                      ELSE IF  (i_l(idim)+i_h(idim)+1 < 0 .AND. i_h(idim) == -1 .AND. prd(idim) == 0) THEN                        ! right on the right boundary
                         i_l(idim) = MAX(i_l(idim),-1-n_assym_updt_bnd(wlt_fmly,trnsf_type),-i/step2)
                      ELSE IF  (i_l(idim)+i_h(idim)+1 > 0 .AND. i_l(idim) < 0 .AND. prd(idim) == 0) THEN                          !  the left boundary
                         i_h(idim) = MIN(i_h(idim),-i_l(idim)-1+n_assym_updt(wlt_fmly,trnsf_type),(nxyz_DB(idim)-i)/step2-1)
                      ELSE IF  (i_l(idim)+i_h(idim)+1 > 0 .AND. i_l(idim) == 0 .AND. prd(idim) == 0) THEN                         ! right on the left boundary
                         i_h(idim) = MIN(i_h(idim),n_assym_updt_bnd(wlt_fmly,trnsf_type),(nxyz_DB(idim)-i)/step2-1)   
                      END IF
                      lh_wlt(low_limit, update_stage,wlt_fmly,trnsf_type,j,i,idim) = i_l(idim)
                      lh_wlt(high_limit,update_stage,wlt_fmly,trnsf_type,j,i,idim) = i_h(idim)
!!$                      IF (par_rank.EQ.0) PRINT *, 'lh_wlt upd',&
!!$                           low_limit, high_limit, update_stage,wlt_fmly,trnsf_type,j,i,idim, i_l(idim), i_h(idim)
                   END DO
                END DO
             END DO
          END DO
       END DO
    END IF


    !----------------- setup left and right limits for derivative stencils
    !    lh_diff(meth,diff(1/2),low_limit/high_limit(0/1),j,ix_idim,idim)
    IF (.NOT.ALLOCATED(lh_diff) ) &
         ALLOCATE(lh_diff(0:n_df,1:2,0:1,1:j_mx,0:MAXVAL(nxyz_DB(1:dim)-prd_DB(1:dim)),1:dim))
    IF ( BTEST(do_reset,1) ) THEN
       IF( BTEST(debug_level,4) ) PRINT *, 'reset lh_diff'
       lh_diff = 0
       ZERO = 0; ONE = 1
       DO meth = 0, n_df
          IF(meth == 0) THEN 
             nDD(meth)=1
          ELSE IF(meth == 1) THEN 
             nDD(meth)=n_diff
          ELSE IF(meth == 2) THEN 
             nDD(meth)=1
          ELSE IF(meth == 3) THEN 
             nDD(meth)=n_diff
          ELSE IF(meth == 4) THEN 
             nDD(meth)=1
          ELSE IF(meth == 5) THEN 
             nDD(meth)=n_diff
          END IF
       END DO
       DO idim = 1,dim
          DO j=1,j_mx
             step_j = 2**(j_mx-j)
             DO i = 0,nxyz_DB(idim)-prd_DB(idim),step_j
                DO jj = j,j_mx
                   step_jj = 2**(j_mx-jj)
                   !---------------------- 1-st derivative in idim-direction ---------------
                   ii_l =-MIN(ONE*i/step_jj,nDD)
                   ii_h = ii_l+2*nDD
                   ii_h = MIN(ii_h, ONE*(nxyz_DB(idim)-i)/step_jj)
                   ii_l = ii_h-2*nDD
                   ii_l = (1-prd_DB(idim))*MAX(MIN(ii_l,ZERO),-ONE*i/step_jj) + prd_DB(idim)*(-nDD)
                   ii_h = (1-prd_DB(idim))*MIN(MAX(ii_h,ZERO),ONE*(nxyz_DB(idim)-i)/step_jj) + prd_DB(idim)*(nDD)
                   WHERE ( ii_l+ii_h == 0)                                           ! the middle of the domain
                      ii_l = ii_l+forward_bias
                      ii_h = ii_h-backward_bias
                   ELSEWHERE (ii_l+ii_h < 0 .AND. ii_h > 0)        ! the right boundary
                      !swapping the backward_bias <-> forward_bias 
                      ii_l = MIN(MAX(ii_l,-ii_h-nd_assym,-ONE*i/step_jj),ii_h-ONE)           
!OLD                      ii_l = ii_l+backward_bias !swapping the rigth boundary only
!OLD                      ii_h = ii_h-forward_bias
                      ii_l = ii_l+forward_bias !swapping the rigth boundary only
                      ii_h = ii_h-backward_bias
                   ELSEWHERE (ii_l+ii_h < 0 .AND. ii_h == 0)                         ! right on the right boundary
                      ii_l = MIN(MAX(ii_l,-nd_assym_bnd,-ONE*i/step_jj),ii_h-ONE)
                   ELSEWHERE (ii_l+ii_h > 0 .AND. ii_l < 0 )       !  the left boundary
                      ii_h = MAX(MIN(ii_h,-ii_l+nd_assym,ONE*(nxyz_DB(idim)-i)/step_jj),ii_l+ONE)
                      ii_l = ii_l+forward_bias
                      ii_h = ii_h-backward_bias
                   ELSEWHERE (ii_l+ii_h > 0 .AND. ii_l == 0 )                        ! right on the left boundary
                      ii_h = MAX(MIN(ii_h,nd_assym_bnd,ONE*(nxyz_DB(idim)-i)/step_jj),ii_l+ONE)    !comment if do not want reduce the order of derivative 
                   END WHERE
				   IF(ANY(ii_l >= ii_h)) THEN
				      PRINT *, 'ii_l >= ii_h'
				   END IF
				   WHERE ( ii_l >= ii_h )  ! recover unbiased case 
                      ii_l = ii_l-forward_bias
                      ii_h = ii_h+backward_bias
				   END WHERE                                          

                   lh_diff(0:n_df,1,low_limit ,jj,i,idim) = ii_l(0:n_df)
                   lh_diff(0:n_df,1,high_limit,jj,i,idim) = ii_h(0:n_df)
                   !---------------------- 2-nd derivative in x-direction ---------------
                   ii_l =-MIN(ONE*i/step_jj,nDD)
                   ii_h = ii_l+2*nDD
                   ii_h = MIN(ii_h, ONE*(nxyz_DB(idim)-i)/step_jj)
                   ii_l = ii_h-2*nDD
                   WHERE( ii_l > -nDD ) ii_h = ii_h+ONE
                   WHERE( ii_h < nDD ) ii_l = ii_l-ONE
                   ii_l = (1-prd_DB(idim))*MAX(ii_l,-ONE*i/step_jj) + prd_DB(idim)*(-nDD)
                   ii_h = (1-prd_DB(idim))*MIN(ii_h,ONE*(nxyz_DB(idim)-i)/step_jj) + prd_DB(idim)*(nDD)
                   WHERE(ii_l+ii_h<0) &
                        ii_l = MIN(MAX(ii_l,-ii_h-nd2_assym),ii_h-2*ONE)
                   WHERE(ii_l+ii_h>0) &
                        ii_h = MAX(MIN(ii_h,-ii_l+nd2_assym),ii_l+2*ONE)
                   WHERE(ii_l+ii_h < 0 .AND. ii_h == 0 ) &
                        ii_l = MIN(MAX(ii_l,-nd2_assym_bnd,-ONE*i/step_jj),ii_h-2*ONE)
                   WHERE(ii_l+ii_h > 0 .AND. ii_l == 0 ) &
                        ii_h = MAX(MIN(ii_h,nd2_assym_bnd,ONE*(nxyz_DB(idim)-i)/step_jj),ii_l+2*ONE)
                   lh_diff(:,2,low_limit ,jj,i,idim) = ii_l
                   lh_diff(:,2,high_limit,jj,i,idim) = ii_h
                END DO
             END DO
          END DO
       END DO
    END IF
    
    IF ( BTEST(debug_level,4) ) &
         PRINT *,'finish setting lh_bounds'
    

  
    !PRINT *, 'UUUUUUUUUUUUUUUUUUUUUUUUUUu', j_lev
    !----------------- setup weights for wavelet transform
    !    lh_diff(meth,diff(1/2),low_limit/high_limit(0/1),j,ix_idim,idim)

    
    IF ( BTEST(do_reset,2) ) THEN
#ifdef COP_1
  ! COP_1 - compilation optimization parameter 1 - define to call wgh_DB() as a function
  !         and save memory for large J_MX .inp parameters
  !         default is undefined and wgh_DB() is an allocatable array
#else
       IF (ALLOCATED(wgh_DB)) DEALLOCATE( wgh_DB )
       ALLOCATE( wgh_DB(-2*MAX(MAXVAL(n_prdct),MAXVAL(n_updt)) : 2*MAX(MAXVAL(n_prdct),MAXVAL(n_updt)), &
            0:MAXVAL(nxyz_DB), 2:j_mx, 0:wlt_fmly, 0:n_trnsf_type, 1:dim)) 
       
       IF( BTEST(debug_level,4) ) PRINT *, 'reset wgh_DB'
       !no WT on level 1, internal/normal transform
       !PRINT *, -2*MAX(MAXVAL(n_prdct),MAXVAL(n_updt)), &
       !	MAXVAL(nxyz_DB),j_mx, wlt_fmly, n_trnsf_type, dim
!!$       PRINT *, nxyz_DB, prd_DB, low_limit, high_limit, predict_stage,wlt_fmly,trnsf_type
       DO trnsf_type = 0,n_trnsf_type
          DO wlt_fmly = 0,n_wlt_fmly
             DO idim = 1,dim ! transform along idim direction
                DO j = 2,j_mx
                   step = 2**(j_mx-j)
                   step2 = 2*step
                   !****** PREDICT STAGE ****** 
                   DO i = step,nxyz_DB(idim)-prd_DB(idim),step2
                      i_l(idim) = lh_wlt(low_limit, predict_stage,wlt_fmly,trnsf_type,j,i,idim)
                      i_h(idim) = lh_wlt(high_limit,predict_stage,wlt_fmly,trnsf_type,j,i,idim)
                      !PRINT *, i_l(idim),i_h(idim)
                      !IF ( j <= 2 ) &
                           !PRINT *, '@', trnsf_type, wlt_fmly, idim, i, i_l(idim), i_h(idim), &
                           !wgh_DB(i_l(idim):i_h(idim),i,j,wlt_fmly,trnsf_type,idim)
                      DO ipm = i_l(idim),i_h(idim)
                         !PRINT *, ipm, i, j, wlt_fmly,trnsf_type,idim

                         wgh_DB(ipm,i,j,wlt_fmly,trnsf_type,idim) =  &
                              wgh (ipm, i, i_l(idim), i_h(idim), step, prd_DB(idim), nxyz_DB(idim), xx_DB(0:nxyz_DB(idim),idim))
                         !@@@@
                      END DO
                   END DO
                   !****** UPDATE STAGE ****** 
                   DO i = 0,nxyz_DB(idim)-prd_DB(idim),step2
                      i_l(idim) = lh_wlt(low_limit, update_stage,wlt_fmly,trnsf_type,j,i,idim)
                      i_h(idim) = lh_wlt(high_limit,update_stage,wlt_fmly,trnsf_type,j,i,idim)
                      DO ipm = i_l(idim),i_h(idim)
                         wgh_DB(ipm,i,j,wlt_fmly,trnsf_type,idim) =  &
                              wgh (ipm, i, i_l(idim), i_h(idim), step, prd_DB(idim), nxyz_DB(idim), xx_DB(0:nxyz_DB(idim),idim))
                      END DO
                   END DO
                END DO
             END DO
          END DO
       END DO
#endif
    END IF
    
    
    !----------------- setup weights for derivatives
    !    lh_diff(meth,diff(1/2),low_limit/high_limit(0/1),j,ix_idim,idim)
    
    IF ( BTEST(do_reset,3) ) THEN
       IF (ALLOCATED(wgh_df_DB)) DEALLOCATE( wgh_df_DB )
       ALLOCATE(wgh_df_DB(-2*n_diff-1:2*n_diff+1,0:MAXVAL(nxyz_DB),j_mx,0:n_df,1:2,1:dim))
       IF( BTEST(debug_level,4) ) PRINT *, 'reset wgh_df_DB'
       DO order = 1,2
          DO meth = 0, n_df
             DO j = 1, j_mx
                step = 2**(j_mx-j)
                DO idim = 1,dim
                   DO i = 0,nxyz_DB(idim)-prd_DB(idim),step
                      i_l(idim)  = lh_diff(meth, order, low_limit,  j, i, idim)
                      i_h(idim)  = lh_diff(meth, order, high_limit, j, i, idim)
                      !PRINT *, meth, order, high_limit, j, i, idim
                      !PRINT *, i_l(idim),i_h(idim)

                      DO ipm = i_l(idim),i_h(idim)
                         i1 = (1-prd_DB(idim))*(i+ipm*step) &
                              + prd_DB(idim)*MOD(i+ipm*step+9*nxyz_DB(idim),nxyz_DB(idim))
                         wgh_df_DB(ipm,i,j,meth,order,idim) = &
                              diff_wgh(ipm, i, i_l(idim), i_h(idim), step, prd_DB(idim), nxyz_DB(idim), xx_DB(0:nxyz_DB(idim),idim), h_DB(j,idim), order)

                      END DO
                   END DO
                END DO
             END DO
          END DO
       END DO
    END IF

  END SUBROUTINE setup_lh_bounds_DB

!!$  SUBROUTINE initialize_indices_DB(FreshStart)
!!$    USE wlt_vars
!!$    IMPLICIT NONE
!!$    LOGICAL, INTENT(IN) :: FreshStart
!!$    INTEGER :: i, step
!!$
!!$    IF ( .NOT. FreshStart ) THEN !Restart version
!!$       !------------- add here later ------------------
!!$       PRINT *, 'THIS OPTIONS NEEDS TO BE ADDED'
!!$       STOP
!!$    ELSE  !first initialization call
!!$       j_lev = MIN(j_mn_init_DB,j_mx)
!!$       step = 2**(j_mx - j_lev)
!!$       i_p(1:dim) = INT((nxyz_DB(:)-prd_DB(:))/step) + 1
!!$       DO i =1, dim
!!$          i_p(i) = i_p(i-1)*i_p(i)
!!$       END DO
!!$       DO i = 1,i_p(dim)
!!$          ixyz_DB = INT(MOD(i-1,i_p(1:dim))/i_p(0:dim-1))*step
!!$          CALL set_id(ixyz_DB,j_mx,pos_sig,beg_list)
!!$       END DO
!!$    END IF
!!$
!!$  END SUBROUTINE initialize_indices_DB

  SUBROUTINE indices_DB
    USE sizes
    USE wlt_vars
    USE wlt_trns_vars       ! indx_DB
    USE parallel            ! par_size
    IMPLICIT NONE
    DECLARE_NODE_POINTER :: c_pointer, c_pointer1

    INTEGER  :: i, j, j_df, j_lv
    INTEGER, DIMENSION(dim) :: wlt, face
    INTEGER, DIMENSION(0:dim) :: i_p_face, i_p_wlt, i_p
    INTEGER :: step_DB, wlt_type, face_type, ibndr, ishift, ii
    INTEGER :: iaux(nvarI_nwlt:nvarI_jD), total_length
    INTEGER, DIMENSION(dim) :: ixyz

    ! creatig forward and backward mapping, in future has to go thrhough wavelt families and levels to create DB own indx.
    step_DB = 2**(j_mx-j_lev)
    i_p(0) = 1
    i_p_face(0) = 1
    i_p_wlt(0) = 1
    DO i=1,dim
       i_p_face(i) = i_p_face(i-1)*3
       i_p_wlt(i) = i_p_wlt(i-1)*2
!!$       i_p(i) = i_p(i-1)*(1+mxyz(i)*2**(j_lev-1))
       i_p(i) = i_p(i-1)*(1+nxyz(i))
    END DO

    !
    !================== Set up list of pointes based on wlt_type, face_type, j, j_diff, 
    !                   where j_diff is the level at which derivative is taken
    !
    !
    !initializing
    !
    IF(.NOT.ALLOCATED(indx_DB)) THEN
       ALLOCATE(indx_DB(1:j_mx,0:2**dim-1,0:3**dim-1,1:j_mx))
       DO j = 1, j_mx
          DO wlt_type = MIN(j-1,1),2**dim - 1
             DO face_type = 0, 3**dim - 1
                DO j_df = 1, j_mx
                   NULLIFY (indx_DB(j_df,wlt_type,face_type,j)%p)
                   indx_DB(j_df,wlt_type,face_type,j)%real_length = 0
                END DO
             END DO
          END DO
       END DO
    END IF
    total_length = 0
    indx_DB(:,:,:,:)%length = 0
    indx_DB(:,:,:,:)%shift = 1
    !
    ! setting up the indx_DB list
    !
    DO j = 1, j_lev
       DO wlt_type = MIN(j-1,1),2**dim-1
          DO face_type = 0, 3**dim - 1
             CALL DB_get_initial_type_level_node (par_rank, wlt_type, j, face_type, ii, c_pointer, list_sig)
             IF(ii > 0) THEN

                DO WHILE (is_ok(c_pointer))
#ifdef TREE_NODE_POINTER_STYLE_C
                   CALL DB_get_ifunction_by_pointer( c_pointer, nvarI_jD, nvarI_jD, j_df )
                   indx_DB(j_df,wlt_type,face_type,j)%length = indx_DB(j_df,wlt_type,face_type,j)%length + 1
                   total_length = total_length + 1
                   CALL DB_get_next_type_level_node (par_rank, wlt_type, j, face_type, c_pointer, c_pointer1, list_sig)
                   c_pointer = c_pointer1
#elif defined TREE_NODE_POINTER_STYLE_F
                   j_df = c_pointer%ival(nvarI_jD)
                   indx_DB(j_df,wlt_type,face_type,j)%length = indx_DB(j_df,wlt_type,face_type,j)%length + 1
                   total_length = total_length + 1
                   CALL DB_get_next_type_level_node (c_pointer)
#endif
                END DO

             END IF
          END DO
       END DO
    END DO
    
    ! allocating arrays, all p(:) point into contiguous_p(:)
    IF (ALLOCATED(contiguous_p).AND.SIZE(contiguous_p).LT.total_length) DEALLOCATE(contiguous_p)
    IF (.NOT.ALLOCATED(contiguous_p)) ALLOCATE (contiguous_p(total_length))
    total_length = 1 !starting position of next p(:)
    DO j = 1, j_lev
       DO wlt_type = MIN(j-1,1),2**dim-1
          DO face_type = 0, 3**dim - 1
             DO j_df = 1, j_lev

                indx_DB(j_df,wlt_type,face_type,j)%p => &
                     contiguous_p(total_length:total_length+indx_DB(j_df,wlt_type,face_type,j)%length)
                indx_DB(j_df,wlt_type,face_type,j)%real_length = indx_DB(j_df,wlt_type,face_type,j)%length
                total_length = total_length + indx_DB(j_df,wlt_type,face_type,j)%length

!!$                IF( indx_DB(j_df,wlt_type,face_type,j)%length  /= indx_DB(j_df,wlt_type,face_type,j)%real_length ) THEN
!!$                   ! eventually make smarter allocation algorithm, so 
!!$                   ! if the size is bigger, increasie it by 25% and if smaller than 50% decrease by 25%
!!$                   ! or something like this 
!!$                   IF (indx_DB(j_df,wlt_type,face_type,j)%real_length > 0) THEN
!!$                      DEALLOCATE(indx_DB(j_df,wlt_type,face_type,j)%p)
!!$                      indx_DB(j_df,wlt_type,face_type,j)%real_length = 0
!!$                   END IF
!!$                   IF (indx_DB(j_df,wlt_type,face_type,j)%length  > 0) THEN
!!$                      ALLOCATE(indx_DB(j_df,wlt_type,face_type,j)%p(1:indx_DB(j_df,wlt_type,face_type,j)%length))
!!$                      indx_DB(j_df,wlt_type,face_type,j)%real_length = indx_DB(j_df,wlt_type,face_type,j)%length
!!$                   END IF
!!$                END IF

             END DO
          END DO
       END DO
    END DO

    !--------------- assigning indx_DB
    indx_DB(:,:,:,:)%length = 0
    DO j = 1, j_mx
       DO wlt_type = MIN(j-1,1),2**dim-1
          DO face_type = 0, 3**dim - 1
             CALL DB_get_initial_type_level_node (par_rank, wlt_type, j, face_type, ii, c_pointer, list_sig)
             IF(ii > 0) THEN

                DO WHILE (is_ok(c_pointer))
#ifdef TREE_NODE_POINTER_STYLE_C
                   CALL DB_get_ifunction_by_pointer( c_pointer, nvarI_nwlt, nvarI_jD, iaux(nvarI_nwlt:nvarI_jD) )
                   CALL DB_get_coordinates_by_pointer ( c_pointer, ixyz_DB )
                   ixyz = ixyz_DB/2**(j_mx-j_lev)
                   j_df = iaux(nvarI_jD)
                   indx_DB(j_df,wlt_type,face_type,j)%length = indx_DB(j_df,wlt_type,face_type,j)%length + 1
                   indx_DB(j_df,wlt_type,face_type,j)%p(indx_DB(j_df,wlt_type,face_type,j)%length)%nptr = c_pointer
                   indx_DB(j_df,wlt_type,face_type,j)%p(indx_DB(j_df,wlt_type,face_type,j)%length)%i = iaux(nvarI_nwlt)
                   indx_DB(j_df,wlt_type,face_type,j)%p(indx_DB(j_df,wlt_type,face_type,j)%length)%ixyz = &
                        1+SUM(ixyz(1:dim)*i_p(0:dim-1)) ! index on j_lev level
                   CALL DB_get_next_type_level_node (par_rank, wlt_type, j, face_type, c_pointer, c_pointer1, list_sig)
                   c_pointer = c_pointer1
#elif defined TREE_NODE_POINTER_STYLE_F
                   CALL DB_get_ifunction_by_pointer( c_pointer, nvarI_nwlt, nvarI_jD, iaux(nvarI_nwlt:nvarI_jD) )
                   CALL DB_get_coordinates_by_pointer ( c_pointer, ixyz_DB )
                   ixyz = ixyz_DB/2**(j_mx-j_lev)
                   j_df = iaux(nvarI_jD)
                   indx_DB(j_df,wlt_type,face_type,j)%length = indx_DB(j_df,wlt_type,face_type,j)%length + 1
                   indx_DB(j_df,wlt_type,face_type,j)%p(indx_DB(j_df,wlt_type,face_type,j)%length)%nptr => c_pointer
                   indx_DB(j_df,wlt_type,face_type,j)%p(indx_DB(j_df,wlt_type,face_type,j)%length)%i = iaux(nvarI_nwlt)
                   indx_DB(j_df,wlt_type,face_type,j)%p(indx_DB(j_df,wlt_type,face_type,j)%length)%ixyz = &
                        1+SUM(ixyz(1:dim)*i_p(0:dim-1)) ! index on j_lev level
                   CALL DB_get_next_type_level_node (c_pointer)
#endif
                END DO

             END IF
          END DO
       END DO
    END DO

    indx_DB(1:j_mx,0:2**dim-1,:,1:j_mx)%shift = 0 ! initialization
    DO j = 1, j_lev
       DO face_type = 0, 3**dim - 1
          IF(face_type /= (3**dim-1)/2) THEN
             DO wlt_type = MIN(j-1,1),2**dim-1
                DO j_df = 1, j_lev
                   indx_DB(j_df,wlt_type,face_type,j)%shift = Nwlt_lev(j_df,0)-Nwlt_lev(j_lev,0)
                END DO
             END DO
          END IF
       END DO
    END DO

  END SUBROUTINE indices_DB


  SUBROUTINE set_id(ixyz_loc,j,pos,loc_list)
    USE wlt_vars
    USE db_tree_vars ! pointer
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: ixyz_loc(1:dim),j,pos,loc_list
    INTEGER  :: ierr
    DECLARE_NODE_POINTER :: c_pointer

    CALL DB_add_node (ixyz_loc,j,ierr,c_pointer,idp(pos),loc_list)

  END SUBROUTINE set_id

  SUBROUTINE update_DB
    USE debug_vars
    IMPLICIT NONE

    IF ( BTEST(debug_level,3) ) &
         PRINT *,'updating DB'

    CALL DB_update_DB

  END SUBROUTINE update_DB

  SUBROUTINE write_DB (u, nlocal, ilow, ihigh, j_in, JMIN, ZERO)
    USE precision
    USE wlt_trns_vars     ! indx_DB
    USE debug_vars
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: nlocal, ilow, ihigh, j_in
    REAL (pr), DIMENSION (1:nlocal,1:ihigh-ilow+1), INTENT (IN) :: u
    LOGICAL, INTENT(IN), OPTIONAL :: ZERO                               ! if present, treat u as zero
    INTEGER, INTENT(IN), OPTIONAL :: JMIN                               ! start from that level
    DECLARE_NODE_POINTER :: c_pointer, c_pointer1
    
    
    INTEGER :: j_df, wlt_type, face_type, iloc
    INTEGER  :: j, i_type, jminl
    REAL (pr), DIMENSION (1:ihigh-ilow+1) :: tmp_u    ! temporal extract from u (to shut up compiler warning)
!!$    INTEGER :: ixyz(3)
    
    IF (PRESENT(ZERO)) tmp_u = 0.0_pr
    jminl = 1;
    IF (PRESENT(JMIN)) jminl = JMIN
    
    DO j = jminl, j_in
       DO wlt_type = MIN(j-1,1),2**dim-1
          DO face_type = 0, 3**dim - 1
             DO j_df = j, j_lev
                DO i_type = 1, indx_DB(j_df,wlt_type,face_type,j)%length
                   
#ifdef TREE_NODE_POINTER_STYLE_C
                   c_pointer = indx_DB(j_df,wlt_type,face_type,j)%p(i_type)%nptr
#elif defined TREE_NODE_POINTER_STYLE_F
                   c_pointer => indx_DB(j_df,wlt_type,face_type,j)%p(i_type)%nptr
#endif
                   
                   IF (.NOT.PRESENT(ZERO)) THEN
                      iloc = indx_DB(j_df,wlt_type,face_type,j)%p(i_type)%i + indx_DB(j_in,wlt_type,face_type,j)%shift
                      tmp_u = u(iloc,1:ihigh-ilow+1)
                   END IF
                   
                   CALL DB_set_function_by_pointer( c_pointer, ilow, ihigh, tmp_u )
                END DO
             END DO
          END DO
       END DO
    END DO



    IF ( BTEST(debug_level,3) ) &
         WRITE(*,'("Finish writing data to DB")')


  END SUBROUTINE write_DB

  SUBROUTINE read_DB (u, nlocal, ilow, ihigh, j_in)
    USE precision
    USE wlt_trns_vars     ! indx_DB
    USE debug_vars
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: nlocal, ilow, ihigh, j_in
    REAL (pr), DIMENSION (1:nlocal,1:ihigh-ilow+1), INTENT (INOUT) :: u
    REAL (pr), DIMENSION (1:ihigh-ilow+1) :: tmp_u    ! temporal extract from u (to shut up compiler warning)
    DECLARE_NODE_POINTER :: c_pointer

    INTEGER :: j_df, wlt_type, face_type, iloc
    INTEGER  :: j, i_type

    DO j = 1, j_in
       DO wlt_type = MIN(j-1,1),2**dim-1
          DO face_type = 0, 3**dim - 1
             DO j_df = j, j_lev
                DO i_type = 1, indx_DB(j_df,wlt_type,face_type,j)%length  

#ifdef TREE_NODE_POINTER_STYLE_C
                   c_pointer = indx_DB(j_df,wlt_type,face_type,j)%p(i_type)%nptr
#elif defined TREE_NODE_POINTER_STYLE_F
                   c_pointer => indx_DB(j_df,wlt_type,face_type,j)%p(i_type)%nptr
#endif

                   iloc = indx_DB(j_df,wlt_type,face_type,j)%p(i_type)%i + indx_DB(j_in,wlt_type,face_type,j)%shift
                   CALL DB_get_function_by_pointer( c_pointer, ilow, ihigh, tmp_u )
                   u(iloc,1:ihigh-ilow+1) = tmp_u(1:ihigh-ilow+1)
                END DO
             END DO
          END DO
       END DO
    END DO
    
    IF ( BTEST(debug_level,3) ) &
         WRITE(*,'("Finish reading data from DB")')
    
  END SUBROUTINE read_DB

  !============================================
  !       Subroutines for grid adaptaion       
  !============================================


  SUBROUTINE significant_wlt_DB (leps, var_adapt, ilow, ihigh, j_lev_old, proc_l, proc_h)
    USE precision
    USE wlt_vars
    USE db_tree_vars        ! pointer
    USE wlt_trns_util_mod   ! set xx
    USE additional_nodes
    USE debug_vars
    USE parallel            ! par_size
    IMPLICIT NONE
    INTEGER, INTENT(IN), OPTIONAL :: proc_l, proc_h   ! for processors [proc_l, proc_h]
    INTEGER, INTENT (IN) :: ilow, ihigh, j_lev_old
    REAL (pr), DIMENSION(1:ihigh-ilow+1), INTENT (IN) :: leps
    LOGICAL, DIMENSION(1:ihigh-ilow+1), INTENT (IN) :: var_adapt
    DECLARE_NODE_POINTER :: c_pointer, c_pointer1

    INTEGER :: j, inum, inum1, inum2,  ii
    INTEGER  :: id, proc
    INTEGER  :: wlt_type, face_type, lnxyz(1:dim), proc_l_, proc_h_

    IF ( BTEST(debug_level,3) ) &
         PRINT *,'start significant_DB: leps:',leps, 'j_mx_adj=',j_mx_adj

    proc_l_ = 0
    proc_h_ = par_size-1
    IF (PRESENT(proc_l)) proc_l_ = proc_l
    IF (PRESENT(proc_h)) proc_h_ = proc_h

    inum = 0
    inum1 = 0
    inum2 = 0
    !j_lev = MAX(1,j_mn-1) !OLEG: change
    j_lev=1
    DO proc = proc_l_, proc_h_
       DO j = 1, j_mx_adj
          DO wlt_type = MIN(j-1,1),2**dim - 1
             DO face_type = 0, 3**dim - 1
                CALL DB_get_initial_type_level_node (proc, wlt_type, j, face_type, ii, c_pointer, list_sig)
                IF(ii > 0) THEN
                   

                   DO WHILE (is_ok(c_pointer))
                      
                      CALL DB_get_function_by_pointer( c_pointer, ilow, ihigh, fvec(ilow:ihigh) )
                      CALL DB_get_id_by_pointer ( c_pointer, id )
                      IF( ANY(ABS(fvec(ilow:ihigh)) >= leps .AND. var_adapt) ) THEN
                         IF(BTEST(id,pos_old)) THEN
                            id=IBSET(idp(pos_sig),pos_old)
                         ELSE
                            id = idp(pos_sig)
                         END IF
                         inum = inum +1
                         CALL DB_get_coordinates_by_pointer ( c_pointer, ixyz_DB )
                         j_lev = MAX(j,j_lev)
                      ELSE
                         IF(BTEST(id,pos_old)) THEN
                            id=IBSET(0,pos_old)
                         ELSE
                            id = 0
                         END IF
                         inum1 = inum1 +1
                      END IF
                      inum2 = inum2 +1
                      CALL DB_set_id_by_pointer ( c_pointer, id )
                      
#ifdef TREE_NODE_POINTER_STYLE_C
                      CALL DB_get_next_type_level_node (proc, wlt_type, j, face_type, c_pointer, c_pointer1, list_sig)
                      c_pointer = c_pointer1
#elif defined TREE_NODE_POINTER_STYLE_F
                      CALL DB_get_next_type_level_node (c_pointer)
#endif
                   END DO
                   
                END IF
             END DO
          END DO
       END DO
    END DO


#ifdef MULTIPROC
    CALL parallel_global_sum (INTEGERMAXVAL=j_lev)
#endif


    j_lev = MAX( j_mn, MIN (j_lev+1, j_mx, j_mx_adj) )
    IF (j_lev <= j_lev_old .AND. j_lev < j_additional_nodes) THEN
       j_lev = MAX(j_mn, MIN( j_lev_old+1, j_additional_nodes, j_mx, j_mx_adj))
    END IF

    IF ( BTEST(debug_level,3) ) &
         PRINT *,'finish significant_DB: ',inum,inum1,inum2


    IF (j_lev /= j_lev_old) THEN
       nxyz(1:dim)=mxyz(1:dim)*2**(j_lev-1)
       IF (ALLOCATED(xx)) DEALLOCATE(xx); ALLOCATE(xx(0:MAXVAL(nxyz),1:dim))
       IF (ALLOCATED(h)) DEALLOCATE(h); ALLOCATE(h(1:j_lev,1:dim) )
       xx=0.0_pr; h = 0.0_pr;
       CALL set_xx (xx,h,nxyz,MAXVAL(nxyz(1:dim)),j_lev)
    END IF
    
  END SUBROUTINE significant_wlt_DB
  
  !
  !************ Calculating Adjacent wavelets ***************************
  !
  ! This routine makes adjacent points true on the i_c logical mask
  ! 
  ! coef - 1D adaptive array of wlt coefficients currently active
  ! leps  - theshold
  ! nwlt - number of wavalets
  ! ij_adj(-1,0,1)   - Number of adjacent wavelets to retain in level below, current, and above 
  ! adj_type(-1,0,1) - for level below,current,above (0 - less conservative, 1 - more conservative)
  ! adj_type(-1)     - is always set to be conservative (no distiction) 
  ! j_in             -
  ! j_out            -
  ! j_mn             -
  ! j_mx             -

  SUBROUTINE adjacent_wlt_DB (ij_adj, adj_type, proc_l, proc_h)
    USE precision
    USE wlt_vars
    USE db_tree_vars        ! pointer
    USE debug_vars
    USE parallel            ! par_size
    IMPLICIT NONE
    INTEGER, DIMENSION (-1:1), INTENT(IN) :: ij_adj, adj_type
    INTEGER, INTENT(IN), OPTIONAL :: proc_l, proc_h   ! for processors [proc_l, proc_h]
    DECLARE_NODE_POINTER :: c_pointer, c_pointer1
    
    INTEGER :: inum,j,step,step_adj, proc_l_, proc_h_, cproc, &
         wlt_type, face_type, i, ii, jadj, ixyz(dim), idim,   &
         id, proc, i_p(0:dim), size_node, num, num_recv
!!$    INTEGER, ALLOCATABLE :: nodebuffer(:)             ! store 1D node index
!!$    INTEGER, POINTER :: nodebuffer_recv(:)
    
    IF ( BTEST(debug_level,3) ) &
         PRINT *, 'start adjacent_wlt_DB'

!!$    PRINT *,'ij_adj=',ij_adj,'adj_type=',adj_type,'j_mx_adj=',j_mx_adj


    i_p(0) = 1

    proc_l_ = 0
    proc_h_ = par_size-1
    IF (PRESENT(proc_l)) proc_l_ = proc_l
    IF (PRESENT(proc_h)) proc_h_ = proc_h

    inum = 0
    DO proc = proc_l_, proc_h_
       DO j = 1, j_mx_adj
          DO wlt_type = MIN(j-1,1),2**dim-1
             DO face_type = 0, 3**dim - 1
                !---------- this deals with the fact that on the coarsest level Alexei still has multiple wavelet families
                ixyz_wlt_DB(:) = MIN(1,j-1)*IBITS(wlt_type,ivec(:)-1,1) + (1-MIN(1,j-1))
                CALL DB_get_initial_type_level_node (proc, wlt_type, j, face_type, ii, c_pointer, list_sig)
                IF(ii > 0) THEN
!!$                   PRINT *, 'type,level,facetype=',wlt_type, j, face_type,'ii=',ii
                   DO WHILE (is_ok(c_pointer))
                      
                      CALL DB_get_id_by_pointer ( c_pointer, id )
                      IF(BTEST(id,pos_sig)) THEN
                         inum = inum + 1
                         CALL DB_get_coordinates_by_pointer ( c_pointer, ixyz_DB )
!!$                         PRINT *, 'working pt ', ixyz_DB
                         !jadj =  1 -> level above
                         !jadj =  0 -> same level
                         !jadj = -1 -> level below
                         DO jadj = MAX(-1,1-j),MIN(1,j_mx_adj-j)
                            step_adj = 2**(j_mx - j - jadj)
                            ixyz_shift(:) = MAX(0,-jadj)*ixyz_wlt_DB(:)*(step_adj/2)
                            ! Find previous (i_l) and next (i_h) point in each direction
                            i_l(:) = MAX(0, ixyz_DB(:)+ixyz_shift(:)-ij_adj(jadj)*step_adj)
                            i_h(:) = MIN(ixyz_DB(:)-ixyz_shift(:)+ij_adj(jadj)*step_adj, nxyz_DB(:) )
                            i_l(:) = (1-prd_DB(:))*i_l(:) + prd_DB(:)*(ixyz_DB(:)+ixyz_shift(:)-ij_adj(jadj)*step_adj)
                            i_h(:) = (1-prd_DB(:))*i_h(:) + prd_DB(:)*(ixyz_DB(:)-ixyz_shift(:)+ij_adj(jadj)*step_adj)
                            IF(adj_type(jadj) == 0) THEN ! less conservative
                               ixyz1_DB(:) = MAX(0,ABS(jadj)) * ixyz_wlt_DB(:) + &
                                    ( 1-MAX(0, ABS(jadj)) ) * ( 1 - (1 - MIN(ABS(SUM(ixyz_wlt_DB)-1),1)) * ixyz_wlt_DB(:) ) 
                               i_l(:) = ixyz_DB(:) + ( i_l(:)-ixyz_DB(:) ) * ixyz1_DB(:)
                               i_h(:) = ixyz_DB(:) + ( i_h(:)-ixyz_DB(:) ) * ixyz1_DB(:)
                               DO idim = 1,dim
                                  ixyz1_DB = ixyz_DB
                                  DO i = i_l(idim),i_h(idim),step_adj
                                     ixyz1_DB(idim) = i
                                     ixyz1_DB(:) = (1-prd_DB(:))*ixyz1_DB + prd_DB(:)*MOD(ixyz1_DB(:)+9*nxyz_DB(:),nxyz_DB(:))
                                     CALL set_id(ixyz1_DB,j_mx,pos_adj,beg_list)
!!$                                     write (*,'( "add pt:", 3I4)') ixyz1_DB
                                  END DO
                               END DO
                            ELSE IF(adj_type(jadj) == 1) THEN !more conservative
                               i_p(1:dim) = (i_h-i_l)/step_adj + 1
                               DO i =1, dim
                                  i_p(i) = i_p(i-1)*i_p(i)
                               END DO
                               DO i = 1,i_p(dim)
                                  ixyz1_DB = i_l + INT(MOD(i-1,i_p(1:dim))/i_p(0:dim-1))*step_adj
                                  ixyz1_DB(:) = (1-prd_DB(:))*ixyz1_DB(:) + prd_DB(:)*MOD(ixyz1_DB(:)+9*nxyz_DB(:),nxyz_DB(:))
                                  CALL set_id(ixyz1_DB,j_mx,pos_adj,beg_list)
!!$                                  write (*,'( "add pt:", 3I4)') ixyz1_DB
                               END DO
                            END IF
                         END DO
                      END IF
                      
#ifdef TREE_NODE_POINTER_STYLE_C
                      CALL DB_get_next_type_level_node (proc, wlt_type, j, face_type, c_pointer, c_pointer1, list_sig)
                      c_pointer = c_pointer1
#elif defined TREE_NODE_POINTER_STYLE_F
                      CALL DB_get_next_type_level_node (c_pointer)
#endif
                   END DO
                   
                END IF
             END DO
          END DO
       END DO
    END DO

!!$    IF(BNDzone) THEN !--- Adding boundary points, modify this subroutine later
!!$!!!!!!  -------------- Need to add BNDzone capability  ?????????????????????????????
!!$       !Oleg - use corrected version from 2-D
!!$       PRINT *, ' Code to do BNDzone is not updated for 3D yet.'
!!$       STOP
!!$    ENDIF
    
    ! include all points j<= j_mn (belonging to current processor)
    step = 2**(j_mx - j_mn)
    i_p(1:dim) = INT((nxyz_DB(:)-prd_DB(:))/step) + 1
    DO i =1, dim
       i_p(i) = i_p(i-1)*i_p(i)
    END DO
    DO i = 1,i_p(dim)
       ixyz_DB = INT(MOD(i-1,i_p(1:dim))/i_p(0:dim-1))*step
       CALL DB_get_proc_by_coordinates (ixyz_DB, j_mx, cproc)
       IF (cproc.EQ.par_rank) &
            CALL set_id(ixyz_DB,j_mx,pos_adj,beg_list)
    END DO

    
    IF ( BTEST(debug_level,3) ) &
         PRINT *, 'finish adjacent_wlt_DB'
    
  END SUBROUTINE adjacent_wlt_DB

  
  !
  !************ Calculating bnd_zone wavelets ***************************
  !
  SUBROUTINE bnd_zone_DB (j_out)
    USE wlt_vars            ! n_prdct
    USE db_tree_vars        ! pointer
    USE debug_vars          ! experimental_feature_stop
    USE parallel            ! par_size
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: j_out
    INTEGER :: i, j, ix, iy, iz, lx, ly, lz
    !----------------- BNDzone variables
    INTEGER :: j_bnd, jj, ii, k, m,  idim, step(1:dim), step_j, step_jj, ncheck, id, id1, num, &
         n_BNDzone, s(0:1), i_l(dim), i_h(dim), jxyz(1:dim), face(1:dim), wlt(1:dim), ZERO(1:dim), &
         ixyz(1:dim), ixyz1(1:dim), nxyz_out(1:dim), &
         i_p(0:dim), i_p_cube(0:dim), ierr, face_type, wlt_type, proc
    LOGICAL :: check, check1(1:dim)
    DECLARE_NODE_POINTER :: c_pointer, c_pointer1, c_pointer2
    
    
    ! that feature is experimental, warn the user and terminate the program
    CALL experimental_feature_stop('bnd_zone, wlt_trns_mod, wavelet_3d_wrk.f90 ')
    
    
    ZERO = 0
    n_BNDzone = 2*MAX(MAXVAL(n_prdct),n_diff)+1
    nxyz_out(1:dim) = mxyz(1:dim)*2**(j_out-1)
!!$    PRINT *,'in bnd_zone: nxyz_out=',nxyz_out,'j_out=',j_out
    num = 0
    
    i_p(0) = 1
    i_p_cube(0) = 1
    DO proc = 0, par_size-1
       DO j = j_out, 1, -1
          step_j = 2**(j_out - j)
          DO i=1,dim
             i_p(i) = i_p(i-1)*(nxyz_out(i)/step_j+1-prd(i))
          END DO
          
          DO wlt_type = MIN(j-1,1),2**dim - 1
             DO face_type = 0, 3**dim - 1
                CALL DB_get_initial_type_level_node (proc, wlt_type, j, face_type, ii, c_pointer, list_sig)
                IF(ii > 0) THEN
                   
                   DO WHILE (is_ok(c_pointer))
                      
                      CALL DB_get_coordinates_by_pointer( c_pointer, ixyz )
                      ixyz = ixyz / 2**(j_mx-j_out)
                      CALL DB_get_id_by_pointer ( c_pointer, id )
                      IF((BTEST(id,pos_sig).OR.BTEST(id,pos_adj)) .AND. ixyz2j_DB(ixyz(1:dim),j_out) == j  ) THEN
                         check1 = .FALSE. !.TRUE. if point is in bnd_zone, .FALSE. otherwise
                         DO idim = 1, dim ! idim - direction
                            s(0) = ( 1 - MIN(INT(ixyz(idim)/(step_j*(n_BNDzone+1))),1) ) * ( 1 - prd(idim) )                    ! low  BNDzone 
                            s(1) = ( 1 - MIN(INT((nxyz_out(idim) - ixyz(idim)) /(step_j*(n_BNDzone+1))),1) ) * (1 - prd(idim) ) ! high BNDzone
                            i_l(idim) = MIN( 0*s(0)+ixyz(idim)*(1-s(0)), MAX(0,(nxyz_out(idim)-n_BNDzone*step_j))*s(1)+ixyz(idim)*(1-s(1)) )
                            i_h(idim) = MAX( MIN(step_j*n_BNDzone,nxyz_out(idim))*s(0)+ixyz(idim)*(1-s(0)), nxyz_out(idim)*s(1)+ixyz(idim)*(1-s(1)) )
                            check1(idim) = ANY( s(0:1) /= 0)
                         END DO
                         IF( ANY(check1(1:dim)) ) THEN
                            WHERE (.NOT.check1(1:dim) ) 
                               !  i_l(1:dim) = MIN( i_l(1:dim),ixyz(1:dim)-step_j )
                               !  i_h(1:dim) = MAX( i_h(1:dim),ixyz(1:dim)+step_j )
                               i_l(1:dim) = MIN( i_l(1:dim),0 )
                               i_h(1:dim) = MAX( i_h(1:dim),nxyz_out(1:dim)-prd(1:dim) )
                            END WHERE
                            i_l(1:dim)=(1-prd(1:dim))*MAX(ZERO(1:dim),i_l(1:dim)) + prd(1:dim)*i_l(1:dim)
                            i_h(1:dim)=(1-prd(1:dim))*MIN(nxyz_out(1:dim),i_h(1:dim)) + prd(1:dim)*i_h(1:dim)
                            i_p_cube(1:dim) = (i_h-i_l)/step_j + 1
                            DO idim =1, dim
                               i_p_cube(idim) = i_p_cube(idim-1)*i_p_cube(idim)
                            END DO
                            check = .FALSE.
                            DO m = 1,i_p_cube(dim)
                               ixyz1(1:dim) = i_l + INT(MOD(m-1,i_p_cube(1:dim))/i_p_cube(0:dim-1))*step_j
                               ixyz1(1:dim) = (1-prd(1:dim))*ixyz1(1:dim)+prd(1:dim)*MOD(ixyz1(1:dim)+9*nxyz_out(1:dim),nxyz_out(1:dim))
                               CALL DB_test_node( ixyz1(:), j_out, c_pointer1 )
                               
#ifdef TREE_NODE_POINTER_STYLE_C
                               IF (c_pointer1 /= 0) THEN           ! integer C/C++ pointer
#elif defined TREE_NODE_POINTER_STYLE_F
                               IF(ASSOCIATED(c_pointer1)) THEN ! Fortran's native pointer
#endif
                                  CALL DB_get_id_by_pointer ( c_pointer1, id1 )
                                  IF((BTEST(id1,pos_sig).OR.BTEST(id1,pos_adj)) .AND. ixyz2j_DB(ixyz1(1:dim),j_out) == j ) check = .TRUE.
                               END IF
                               
                            END DO
                            IF(check) THEN
                               DO m = 1,i_p_cube(dim)
                                  ixyz1(1:dim) = i_l + INT(MOD(m-1,i_p_cube(1:dim))/i_p_cube(0:dim-1))*step_j
                                  ixyz1(1:dim) = (1-prd(1:dim))*ixyz1(1:dim)+prd(1:dim)*MOD(ixyz1(1:dim)+9*nxyz_out(1:dim),nxyz_out(1:dim))
                                  CALL DB_add_node( ixyz1(1:dim), j_out, ierr, c_pointer1, idp(pos_adj), beg_list )
                                  num = num + 1
                               END DO
                            END IF
                         END IF
                      END IF
                      
#ifdef TREE_NODE_POINTER_STYLE_C
                      CALL DB_get_next_type_level_node (proc, wlt_type, j, face_type, c_pointer, c_pointer2, list_sig)
                      c_pointer = c_pointer2
#elif defined TREE_NODE_POINTER_STYLE_F
                      CALL DB_get_next_type_level_node (c_pointer)
#endif
                   END DO
                   
                END IF
             END DO
          END DO
       END DO
    END DO

    IF ( BTEST(debug_level,3) ) &
         PRINT *,'in bnd_zone: number of nodes added =',num
    
  END SUBROUTINE bnd_zone_DB

  !
  !************ Calculating Adjacent wavelets ***************************
  !
  SUBROUTINE zone_wlt_DB (j_out,j_zone)
    USE precision
    USE wlt_vars
    USE db_tree_vars        ! pointer
    USE debug_vars
    USE parallel            ! par_size
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: j_out,j_zone
    DECLARE_NODE_POINTER :: c_pointer, c_pointer1

    INTEGER :: wlt_type, face_type, proc, i_p(0:dim), &
         i, ii, j, idim, step, id, ixyz(1:dim), &
         num1,    &          ! total number of points tested
         num2,    &          ! all points j <= j_zone
         num1p               ! ajacent points set
    LOGICAL :: check

    IF ( BTEST(debug_level,3) ) &
         PRINT *, 'starting zone_wlt_DB'

    !---------- exclude points outside of the zone ------------------

!!$    PRINT *, 'in zone_wlt_DB: j_out=',j_out, 'j_zone=',j_zone
!!$    PRINT *, 'xx=',xx, 'length(xx)=',SIZE(xx(:,1))
!!$    PRINT *, 'xyzzone=',xyzzone

    num1 = 0
    num2 = 0
    num1p = 0
    DO proc = 0, par_size-1
       DO j = 1,j_out
          DO wlt_type = MIN(1,j-1),2**dim-1
             DO face_type = 0, 3**dim - 1
                CALL DB_get_initial_type_level_node (proc, wlt_type, j, face_type, ii, c_pointer, list_sig)
                IF(ii > 0) THEN
                   
                   DO WHILE (is_ok(c_pointer))
                      
                      CALL DB_get_id_by_pointer ( c_pointer, id )
                      check = BTEST(id,pos_sig).OR.BTEST(id,pos_adj)
                      IF (check) THEN
                         num1 = num1 + 1
                         CALL DB_get_coordinates_by_pointer ( c_pointer, ixyz )
                         ixyz = ixyz / 2**(j_mx-j_out)
                         DO idim = 1, dim
                            check = check .AND. xyzzone(1,idim)<=xx(ixyz(idim),idim) .AND. xx(ixyz(idim),idim) <= xyzzone(2,idim)
                         END DO
                         IF (check) THEN
                            num1p = num1p + 1
                            !PRINT *, 'i_mask==true',ixyz
                         ELSE
                            IF (BTEST(id,pos_old)) THEN
                               id = IBSET(0,pos_old)
                            ELSE
                               id = 0
                            END IF
                            CALL DB_set_id_by_pointer ( c_pointer, id )
!!$                            PRINT *, 'set ID to',id,' at',ixyz
                         END IF
                      END IF
                      
#ifdef TREE_NODE_POINTER_STYLE_C
                      CALL DB_get_next_type_level_node (proc, wlt_type, j, face_type, c_pointer, c_pointer1, list_sig)
                      c_pointer = c_pointer1
#elif defined TREE_NODE_POINTER_STYLE_F
                      CALL DB_get_next_type_level_node (c_pointer)
#endif
                   END DO
                   
                END IF
             END DO
          END DO
       END DO
    END DO

    !---------- include all points  j <= j_zone ------------------
    step = MAX(1,2**(j_mx - j_zone))
    i_p(0) = 1
    i_p(1:dim) = INT((nxyz_DB(:)-prd_DB(:))/step) + 1
    DO i =1, dim
       i_p(i) = i_p(i-1)*i_p(i)
    END DO
    DO i = 1,i_p(dim)
       ixyz = INT(MOD(i-1,i_p(1:dim))/i_p(0:dim-1))*step
       CALL DB_get_proc_by_coordinates (ixyz, j_mx, proc)
       ixyz = ixyz / 2**(j_mx-j_out)
       IF (proc.EQ.par_rank) THEN
	      check=.TRUE.
          DO idim = 1, dim
             check = check .AND. xyzzone(1,idim)<=xx(ixyz(idim),idim) .AND. xx(ixyz(idim),idim) <= xyzzone(2,idim)
          END DO
          IF(.NOT.check) THEN 
		     CALL set_id(ixyz,j_out,pos_adj,end_list)
             num2 = num2 + 1
          END IF
       END IF
    END DO
    
    !CALL DB_MOVE_ZERO_SIG_TO_GHO  !moves zero id from sig_list to ghost list
    
    IF ( BTEST(debug_level,3) ) THEN
       PRINT *, 'ajacent points set            =', num1p
       PRINT *, 'all points j <= j_zone        =', num2
       PRINT *, 'total number of points tested =', num1
       PRINT *, 'finish zone_wlt_DB'
    END IF
    
  END SUBROUTINE zone_wlt_DB
  
  SUBROUTINE reconstr_check_DB (wlt_fmly, trnsf_type, pos_l, pos_h, proc_l, proc_h) !, TEST)
    USE precision
    USE wlt_vars
    USE db_tree_vars        ! pointer
    USE debug_vars
    USE parallel            ! par_size
    IMPLICIT NONE
!!$    LOGICAL, INTENT(IN), OPTIONAL :: TEST
    INTEGER, INTENT (IN) :: wlt_fmly, trnsf_type, pos_l, pos_h
    INTEGER, INTENT(IN), OPTIONAL :: proc_l, proc_h              ! for processors [proc_l, proc_h]
    DECLARE_NODE_POINTER :: c_pointer, c_pointer1, c_tmp
    
    INTEGER :: j, wlt_type, face_type, list, proc, &
         proc_l_, proc_h_, cproc, id_tmp, nproc, &
         i, ii, i_p(0:dim), &
         idim, step, step2, id
    INTEGER, DIMENSION(2*dim)::  ibch
    
    
    i_p(0) = 1

    !Oleg: 04/06/2006 need to fix to accomodate wlt_fmly like it was done in wrk database
    
    IF ( BTEST(debug_level,3) ) &
         PRINT *, 'starting reconstruction_check_DB'
    
    IF(pos_h <= pos_adj) THEN
       list = list_sig
    ELSE IF(pos_l == pos_gho .AND. pos_h == pos_gho) THEN
       list = list_gho
    ELSE IF(pos_l <= pos_adj .AND. pos_h == pos_gho) THEN
       list = list_all
    END IF
    
    ibch(1:2*dim) = 0
    IF(trnsf_type == 1) ibch(1:2*dim) = ibc(1:2*dim)

    
    proc_l_ = 0
    proc_h_ = par_size-1
    IF (PRESENT(proc_l)) proc_l_ = proc_l
    IF (PRESENT(proc_h)) proc_h_ = proc_h
    

!!$    IF (PRESENT(TEST)) THEN
!!$       CALL DB_clean_other_proc_nodes
!!$       PRINT *, 'list=',list, 'pos_h=',pos_h, 'par_rank=',par_rank
!!$       ixyz1_DB(1)=64; ixyz1_DB(2)=64
!!$       CALL DB_get_proc_by_coordinates (ixyz1_DB,j_mx, nproc)
!!$       CALL DB_test_node (ixyz1_DB,j_mx,c_tmp)
!!$       CALL DB_get_id_by_pointer (c_tmp, id_tmp)
!!$       PRINT *, c_tmp, id_tmp,ixyz1_DB, nproc
!!$       CALL count_DB
!!$       STOP
!!$    END IF


    DO j = j_mx,2,-1
       step  = 2**(j_mx-j)
       step2 = 2*step
       DO proc = proc_l_, proc_h_
          DO wlt_type = 2**dim-1,1,-1
             DO face_type = 0, 3**dim - 1
                ixyz_wlt_DB(:) = IBITS(wlt_type,ivec(:)-1,1)
                CALL DB_get_initial_type_level_node (proc, wlt_type, j, face_type, ii, c_pointer, list)
                IF(ii > 0) THEN
                   
                   DO WHILE (is_ok(c_pointer))
                      
                      CALL DB_get_id_by_pointer ( c_pointer, id )
                      IF(IBITS(id,pos_l,pos_h-pos_l+1) /= 0 ) THEN  
                         CALL DB_get_coordinates_by_pointer ( c_pointer, ixyz_DB )


!!$                         IF (PRESENT(TEST)) PRINT *, ' ixyz_DB=', ixyz_DB,'j=',j

                         ! Find previous (i_l) and next (i_h) point in each direction
                         !--------- first goes along the lines
                         DO idim = 1,dim
                            ixyz1_DB = ixyz_DB
                            i_l(idim) = ixyz_DB(idim) &
                                 + ( lh_wlt(low_limit, predict_stage,wlt_fmly,trnsf_type,j,ixyz_DB(idim),idim) * step2 + step ) * ixyz_wlt_DB(idim)
                            i_h(idim) = ixyz_DB(idim) &
                                 + ( lh_wlt(high_limit,predict_stage,wlt_fmly,trnsf_type,j,ixyz_DB(idim),idim) * step2 + step ) * ixyz_wlt_DB(idim)
                            DO i = i_l(idim),i_h(idim),step2
                               ixyz1_DB(idim) = i
                               ixyz1_DB(:) = (1-prd_DB(:))*ixyz1_DB(:) + prd_DB(:)*MOD(ixyz1_DB(:)+9*nxyz_DB(:),nxyz_DB(:))
                               CALL set_id(ixyz1_DB,j_mx,pos_h,end_list)
                               CALL DB_get_proc_by_coordinates (ixyz1_DB,j_mx, nproc)
!!$                               IF (PRESENT(TEST).AND.par_rank.NE.nproc) THEN
!!$                                  CALL DB_test_node (ixyz1_DB,j_mx,c_tmp)
!!$                                  CALL DB_get_id_by_pointer (c_tmp, id_tmp)
!!$                                  PRINT *, 'set ',id_tmp,ixyz1_DB, 'to',nproc
!!$                               ELSE
!!$                                  IF (PRESENT(TEST)) PRINT *, 'set locally',ixyz1_DB
!!$                               END IF
                            END DO
                         END DO
                         !--------- then goes in the volume
                         i_p(1:dim) = (i_h-i_l)/step2 + 1
                         DO i =1, dim
                            i_p(i) = i_p(i-1)*i_p(i)
                         END DO
                         DO i = 1,i_p(dim)
                            ixyz1_DB = i_l + INT(MOD(i-1,i_p(1:dim))/i_p(0:dim-1))*step2
                            ixyz1_DB(:) = (1-prd_DB(:))*ixyz1_DB(:) + prd_DB(:)*MOD(ixyz1_DB(:)+9*nxyz_DB(:),nxyz_DB(:))
                            CALL set_id(ixyz1_DB,j_mx,pos_h,end_list)
                            CALL DB_get_proc_by_coordinates (ixyz1_DB,j_mx, nproc)
!!$                            IF (PRESENT(TEST).AND.par_rank.NE.nproc) THEN
!!$                               CALL DB_test_node (ixyz1_DB,j_mx,c_tmp)
!!$                               CALL DB_get_id_by_pointer (c_tmp, id_tmp)
!!$                               PRINT *, 'set',id_tmp,ixyz1_DB,'to',nproc
!!$                            ELSE
!!$                               IF (PRESENT(TEST)) PRINT *, 'set locally',ixyz1_DB
!!$                            END IF
                         END DO
                      END IF
                      
#ifdef TREE_NODE_POINTER_STYLE_C
                      CALL DB_get_next_type_level_node (proc, wlt_type, j, face_type, c_pointer, c_pointer1, list)
                      c_pointer = c_pointer1
#elif defined TREE_NODE_POINTER_STYLE_F
                      CALL DB_get_next_type_level_node (c_pointer)
#endif
                   END DO
                   
                END IF
             END DO
          END DO
       END DO
    END DO

    IF ( BTEST(debug_level,3) ) &
         PRINT *, 'finish reconstr_check_DB'

  END SUBROUTINE reconstr_check_DB

  SUBROUTINE add_ghost_DB
    USE precision
    USE wlt_vars
    USE db_tree_vars       ! pointer
    USE debug_vars
    USE parallel           ! par_size
    IMPLICIT NONE
    DECLARE_NODE_POINTER :: c_pointer, c_pointer1, c_pointer_tmp

    INTEGER :: j, j_d, jj, step_jj, &
         wlt_type, face_type, &
         ii, ipm, idim, jxyz(dim), &
         id, num, tmp_i(5), id_tmp
    REAL(pr) :: tmp_r(5)
    LOGICAL :: lchk
    INTEGER, PARAMETER :: list = list_sig
    
    
    INTEGER :: j_d_old             ! some could be precomputed in parallel case
#ifndef MULTIPROC
    j_d_old = -1                   ! all has to be computed at once for single processor case
#endif
    
    
    IF ( BTEST(debug_level,3) ) &
         PRINT *, 'starting add_ghost_DB','j_lev=',j_lev
    
    
    num = 0
    DO j = 1,j_lev
       DO face_type = 0, 3**dim - 1
          DO wlt_type = MIN(1,j-1),2**dim-1
             
             CALL DB_get_initial_type_level_node (par_rank, wlt_type, j, face_type, ii, c_pointer, list)
             
             IF(ii > 0) THEN
                num = num + ii
                
                DO WHILE (is_ok(c_pointer))
                   
                   CALL DB_get_coordinates_by_pointer ( c_pointer, ixyz_DB )
#ifdef MULTIPROC
                   CALL DB_get_ifunction_by_pointer( c_pointer, nvarI_jD, nvarI_jD, j_d_old )
#endif
                   IF (j_d_old.EQ.-1) THEN
                      ! it was incomplete in add_ghost_DB_parallel_antea,
                      ! so we will compute it here
                      
!!$                      WRITE (*,'(A,2I3,A)') 'get node ->',ixyz_DB, ' f lev of d'
                      !--------------- find corresponding level of derivative
                      jxyz = j_lev+1
                      DO idim = 1,dim
                         lchk = .TRUE.
                         DO WHILE(lchk .AND. jxyz(idim) > 1)
                            jxyz(idim) = jxyz(idim) -1
                            i_l(idim) = (1-prd_DB(idim))*MAX(0,ixyz_DB(idim)-nbhd*2**(j_mx-jxyz(idim))) &
                                 + prd_DB(idim)*(ixyz_DB(idim)-nbhd*2**(j_mx-jxyz(idim)))
                            i_h(idim) = (1-prd_DB(idim))*MIN(nxyz_DB(idim),ixyz_DB(idim)+nbhd*2**(j_mx-jxyz(idim))) &
                                 + prd_DB(idim)*(ixyz_DB(idim)+nbhd*2**(j_mx-jxyz(idim)))
                            ixyz1_DB = ixyz_DB
                            DO ipm=i_l(idim),i_h(idim),2**(j_mx-jxyz(idim))
                               ixyz1_DB(idim) = (1-prd_DB(idim))*ipm+prd_DB(idim)*MOD(ipm+9*nxyz_DB(idim),nxyz_DB(idim))
                               ! if this point is active and this point is not the point ixyz(idim) that we are looking at
                               CALL DB_test_node ( ixyz1_DB, j_mx, c_pointer1 )
!!$                               PRINT *, '  testing',ixyz1_DB
                               
                               IF( is_ok(c_pointer1) .AND. ixyz1_DB(idim) /= ixyz_DB(idim) )  THEN
                                  CALL DB_get_id_by_pointer ( c_pointer1, id )
!!$                                  PRINT *, '    id=',id
                                  IF(IBITS(id,pos_sig,pos_adj-pos_sig+1) /= 0 ) lchk = .FALSE.
                               END IF
                               
                            END DO
                         END DO
                      END DO
                      j_d = MAXVAL(jxyz(:))
!!$                      PRINT *, ' j_d=', j_d
#ifdef TREE_NODE_POINTER_STYLE_C
                      CALL DB_set_ifunction_by_pointer( c_pointer, nvarI_jD, nvarI_jD, j_d ) ! setting integer in DB to correspond to derivative level
#elif defined TREE_NODE_POINTER_STYLE_F
                      c_pointer%ival( nvarI_jD ) = j_d
#endif
                   ELSE
                      ! as for the others j_d, they have been computed inside
                      ! add_ghost_DB_parallel_antea
                      j_d = j_d_old
                   END IF
                   
!!$                   IF (par_rank.EQ.1) PRINT *,'coord',ixyz_DB,'j_d',j_d
!!$                   IF (ixyz_DB(1).EQ.0.AND.ixyz_DB(2).EQ.16) THEN
!!$                      PRINT *, 'SIG POINT:',ixyz_DB,j_d,par_rank
!!$
!!$                   END IF
                   !num = num + 1
                   DO jj = j, j_d  
                      step_jj  = 2**(j_mx-jj)
                      DO idim = 1, dim
                         ixyz1_DB = ixyz_DB
                         DO ipm = MINVAL(lh_diff(:,1:2,low_limit ,jj,ixyz_DB(idim),idim)),MAXVAL(lh_diff(:,1:2,high_limit,jj,ixyz_DB(idim),idim))
                            ixyz1_DB(idim) = (1-prd_DB(idim))*(ixyz_DB(idim)+ipm*step_jj) &
                                 + prd_DB(idim)*MOD(ixyz_DB(idim)+ipm*step_jj+9*nxyz_DB(idim),nxyz_DB(idim))
                            
!!$                            IF (ixyz1_DB(1).EQ.0.AND.ixyz1_DB(2).EQ.15) THEN
!!$                               PRINT *,'set_id(gho) to',ixyz1_DB,'of',ixyz_DB
!!$                               CALL DB_test_node ( ixyz1_DB, j_mx, c_pointer1 )
!!$                               id_tmp=-1
!!$                               IF (is_ok(c_pointer1)) &
!!$                                    CALL DB_get_id_by_pointer (c_pointer1, id_tmp)
!!$                               PRINT *, 'c_pointer,id',c_pointer1,id_tmp
!!$                            END IF
                            CALL set_id(ixyz1_DB,j_mx,pos_gho,beg_list)
!!$                            IF (ixyz1_DB(1).EQ.0.AND.ixyz1_DB(2).EQ.15) THEN
!!$                               CALL DB_test_node ( ixyz1_DB, j_mx, c_pointer1 )
!!$                               CALL db1(c_pointer1)
!!$                               CALL parallel_finalize; STOP
!!$                            END IF
                         END DO
                      END DO
                   END DO
                   
#ifdef TREE_NODE_POINTER_STYLE_C
                   CALL DB_get_next_type_level_node (par_rank,wlt_type,j,face_type,c_pointer,c_pointer1,list)
                   c_pointer = c_pointer1
#elif defined TREE_NODE_POINTER_STYLE_F
                   CALL DB_get_next_type_level_node (c_pointer)
#endif
                END DO
                
             END IF
          END DO
       END DO
    END DO
    
    IF ( BTEST(debug_level,3) ) THEN
       PRINT *, '# of set ifunctions:', num
       PRINT *, 'finish add_ghost_DB'
    END IF
        
  END SUBROUTINE add_ghost_DB

  !----------------------------------------------------------------------------------
#ifdef MULTIPROC
  ! bring IDs of the required nodes to current processor (prepare for add_ghost_DB)
  SUBROUTINE add_ghost_DB_parallel_antea
    USE precision
    USE wlt_vars
    USE debug_vars
    USE parallel           ! par_size
    IMPLICIT NONE
    DECLARE_NODE_POINTER :: c_pointer, c_pointer1, c_pointer2

    INTEGER, PARAMETER :: list = list_sig, tmp_mask_size = 1
    LOGICAL, PARAMETER :: tmp_mask(tmp_mask_size) = .TRUE.
    INTEGER :: j, j_d, jj, step_jj, ierr,  &
         wlt_type, face_type, cproc, &
         ii, ipm, idim, jxyz(dim), id
    
    INTEGER  :: tmp_i(5)
    REAL(pr) :: tmp_r(5)
    LOGICAL :: lchk, incomplete_j_d

    DO j = 1,j_lev
       DO face_type = 0, 3**dim - 1
          DO wlt_type = MIN(1,j-1),2**dim-1
             CALL DB_get_initial_type_level_node (par_rank, wlt_type, j, face_type, ii, c_pointer, list)
             IF(ii > 0) THEN
                
                DO WHILE (is_ok(c_pointer))
                   
                   CALL DB_get_coordinates_by_pointer ( c_pointer, ixyz_DB )
                   ! if no points from other processors required for j_d calculation
                   ! the marker will remain false
                   incomplete_j_d = .FALSE.
                   
!!$                   WRITE (*,'(A,2I3,A)') 'get node ->',ixyz_DB, ' f lev of d'
                   !--------------- find corresponding level of derivative
                   jxyz = j_lev+1
                   DO idim = 1,dim
                      lchk = .TRUE.
                      DO WHILE(lchk .AND. jxyz(idim) > 1)
                         jxyz(idim) = jxyz(idim) -1
                         i_l(idim) = (1-prd_DB(idim))*MAX(0,ixyz_DB(idim)-nbhd*2**(j_mx-jxyz(idim))) &
                              + prd_DB(idim)*(ixyz_DB(idim)-nbhd*2**(j_mx-jxyz(idim)))
                         i_h(idim) = (1-prd_DB(idim))*MIN(nxyz_DB(idim),ixyz_DB(idim)+nbhd*2**(j_mx-jxyz(idim))) &
                              + prd_DB(idim)*(ixyz_DB(idim)+nbhd*2**(j_mx-jxyz(idim)))
                         ixyz1_DB = ixyz_DB
                         DO ipm=i_l(idim),i_h(idim),2**(j_mx-jxyz(idim))
                            ixyz1_DB(idim) = (1-prd_DB(idim))*ipm+prd_DB(idim)*MOD(ipm+9*nxyz_DB(idim),nxyz_DB(idim))
                            
                            CALL DB_get_proc_by_coordinates( ixyz1_DB, j_mx, cproc )
                            IF (cproc.NE.par_rank) THEN
                               ! we need to test that node, so add it to the list of the nodes from other processors
                               ! (as old) so we will request its ID later and correct the list accordingly
                               CALL DB_add_node( ixyz1_DB, j_mx, ierr, c_pointer2, idp(pos_old), beg_list )
!!$                               IF (par_rank.EQ.0) PRINT *, 'added old',ixyz1_DB
                               ! set incompleteness marker
                               incomplete_j_d = .TRUE.
                            ELSE
                               ! if this point is active and this point is not the point ixyz(idim) that we are looking at
                               CALL DB_test_node ( ixyz1_DB, j_mx, c_pointer1 )
!!$                               PRINT *, '  testing',ixyz1_DB
                               IF( is_ok(c_pointer1) .AND. ixyz1_DB(idim) /= ixyz_DB(idim) )  THEN
                                  CALL DB_get_id_by_pointer ( c_pointer1, id )
!!$                                  PRINT *, '    id=',id
                                  IF(IBITS(id,pos_sig,pos_adj-pos_sig+1) /= 0 ) lchk = .FALSE.
                               END IF
                            END IF
                            
                         END DO
                      END DO
                   END DO
                   IF (incomplete_j_d) THEN
                      ! j_d can not be computed locally, so we will wait till the nodes
                      ! from other processors are delivered
                      j_d = -1
!!$                      PRINT *, '!-- incomplete j_d for',ixyz_DB,'at', par_rank
                   ELSE
                      ! if j_d has been computed we will add
                      ! the required ghosts (some of them may belong to other processors)
                      j_d = MAXVAL(jxyz(:))
!!$                      PRINT *, 'complete j_d=', j_d,'for',ixyz_DB,'at', par_rank
                   END IF
#ifdef TREE_NODE_POINTER_STYLE_C
                   CALL DB_set_ifunction_by_pointer( c_pointer, nvarI_jD, nvarI_jD, j_d ) ! setting integer in DB to correspond to derivative level
                   CALL DB_get_next_type_level_node (par_rank,wlt_type,j,face_type,c_pointer,c_pointer1,list)
                   c_pointer = c_pointer1
#elif defined TREE_NODE_POINTER_STYLE_F
                   c_pointer%ival( nvarI_jD ) = j_d
                   CALL DB_get_next_type_level_node (c_pointer)
#endif
                   
                END DO
                
             END IF
          END DO
       END DO
    END DO
    
!!$    PRINT *, par_rank,'$$2'
!!$    ixyz1_DB(1)=0;ixyz1_DB(2)=15
!!$    CALL DB_test_node ( ixyz1_DB, j_mx, c_pointer1 )
!!$    IF (is_ok(c_pointer1)) CALL db1(c_pointer1)

    ! get IDs of the nodes added as old from other processors (to complete j_d calculations)
    CALL make_list_and_request( tmp_mask, tmp_mask_size, list_gho, j_lev, IDMODE=.TRUE. )

!!$    PRINT *, par_rank,'$$3'
!!$    ixyz1_DB(1)=0;ixyz1_DB(2)=15
!!$    CALL DB_test_node ( ixyz1_DB, j_mx, c_pointer1 )
!!$    IF (is_ok(c_pointer1)) CALL db1(c_pointer1)

  END SUBROUTINE add_ghost_DB_parallel_antea
  
  ! synchronize ghosts and clean nodes marked as from other processors
  SUBROUTINE add_ghost_DB_parallel_postea( CLEAN )
    USE precision
    USE wlt_vars
    USE debug_vars
    USE parallel           ! par_size
    IMPLICIT NONE
    
    LOGICAL, INTENT(IN), OPTIONAL :: CLEAN ! clean other processor lists, TRUE by default
    DECLARE_NODE_POINTER :: c_pointer, c_pointer1, c_pointer_tmp
    
    INTEGER, PARAMETER :: list = list_all ! new ghosts may present in any list
    INTEGER :: i, j, j_d, jj, step_jj, &
         wlt_type, face_type, proc, &
         ii, ipm, idim, jxyz(dim), ixyz(dim), &
         id, tmp_i(5), i_p(0:dim), size_node, num, num_recv, maxlevel
    
    REAL(pr) :: tmp_r(5)
    LOGICAL :: lchk, do_clean
    
    INTEGER, ALLOCATABLE :: nodebuffer(:)             ! store 1D node index
    INTEGER, POINTER :: nodebuffer_recv(:)
    
    do_clean = .TRUE.
    IF (PRESENT( CLEAN )) do_clean = CLEAN
    maxlevel = j_mx

    ! count ghost nodes to send to the other processors
    procbuffer(0:par_size-1) = 0
    DO proc = 0, par_size-1
       IF (proc.NE.par_rank) THEN
          DO j = 1, maxlevel
             DO wlt_type = MIN(j-1,1),2**dim - 1
                DO face_type = 0, 3**dim - 1
                   CALL DB_get_initial_type_level_node (proc,wlt_type,j,face_type,ii,c_pointer,list)
                   IF(ii > 0) THEN
                      DO WHILE (is_ok(c_pointer))
                         CALL DB_get_id_by_pointer (c_pointer, id)
                         IF (BTEST(id,pos_gho)) &
                              procbuffer(proc) = procbuffer(proc) + 1
#ifdef TREE_NODE_POINTER_STYLE_C
                         CALL DB_get_next_type_level_node (proc,wlt_type,j,face_type,c_pointer,c_pointer1,list)
                         c_pointer = c_pointer1  
#elif defined TREE_NODE_POINTER_STYLE_F
                         CALL DB_get_next_type_level_node (c_pointer)
#endif
                      END DO
                   END IF
                END DO
             END DO
          END DO
       END IF
    END DO
    ! allocate space and write 1D global index for ghost nodes to send
    size_node = SUM( procbuffer(0:par_size-1) )
    ALLOCATE( nodebuffer(size_node) )
    i_p(0) = 1
    DO j=1,dim
       i_p(j) = i_p(j-1)*(mxyz(j)*2**(j_mx-1) + 1)
    END DO
    num = 0
    DO proc = 0, par_size-1
       IF (proc.NE.par_rank) THEN
          DO j = 1, maxlevel
             DO wlt_type = MIN(j-1,1),2**dim - 1
                DO face_type = 0, 3**dim - 1
                   CALL DB_get_initial_type_level_node (proc,wlt_type,j,face_type,ii,c_pointer,list)
                   IF(ii > 0) THEN
                      DO WHILE (is_ok(c_pointer))
                         CALL DB_get_id_by_pointer (c_pointer, id)
                         IF (BTEST(id,pos_gho)) THEN
                            num = num + 1
                            CALL DB_get_coordinates_by_pointer ( c_pointer, ixyz )
                            nodebuffer(num) = 1+SUM(ixyz(1:dim)*i_p(0:dim-1))
!!$                            IF (ixyz(1).EQ.0.AND.ixyz(2).EQ.15) THEN
!!$                               PRINT *, '@@ ixyz=',ixyz,'index=',nodebuffer(num)
!!$                            END IF
!!$                            PRINT *, 'back=', INT(MOD(nodebuffer(num)-1,i_p(1:dim))/i_p(0:dim-1))
                         END IF
#ifdef TREE_NODE_POINTER_STYLE_C
                         CALL DB_get_next_type_level_node (proc,wlt_type,j,face_type,c_pointer,c_pointer1,list)
                         c_pointer = c_pointer1  
#elif defined TREE_NODE_POINTER_STYLE_F
                         CALL DB_get_next_type_level_node (c_pointer)
#endif
                      END DO
                   END IF
                END DO
             END DO
          END DO
       END IF
    END DO
!!$    PRINT *, 'proc',par_rank,'size=',num,'procbuffer=',procbuffer
    ! transfer the data
    ! (nodebuffer_recv(:) will be allocated inside)
    CALL parallel_comm_adj (nodebuffer, procbuffer, num, nodebuffer_recv, num_recv)
    ! set ID for the obtained nodes
    DO i = 1, num_recv
       ixyz(1:dim) = INT(MOD(nodebuffer_recv(i)-1,i_p(1:dim))/i_p(0:dim-1))
!!$       PRINT *, nodebuffer_recv(i), ixyz(1:dim)
       CALL set_id( ixyz, j_mx, pos_gho, beg_list )
    END DO
    ! clean the buffers
    IF (num_recv.NE.0) DEALLOCATE( nodebuffer_recv )
    DEALLOCATE( nodebuffer ) 
    
    
    IF (do_clean) &
         ! clean nodes marked as from other processors
         CALL DB_clean_other_proc_nodes
    
  END SUBROUTINE add_ghost_DB_parallel_postea
#endif
  !----------------------------------------------------------------------------------
  
  SUBROUTINE count_DB( OUTPUT, MAXLIST, FILE, VERB )
    USE precision
    USE wlt_vars
    USE sizes
    USE db_tree_vars        ! pointer
    USE parallel            ! par_size
    USE debug_vars
    IMPLICIT NONE
    LOGICAL, INTENT(IN), OPTIONAL :: OUTPUT                  ! if TRUE(default), print output
    INTEGER, INTENT(IN), OPTIONAL :: MAXLIST                 ! maximum list number to print
    INTEGER, INTENT(IN), OPTIONAL :: FILE                    ! output to file unit
    !INTEGER, INTENT(IN), OPTIONAL :: PROC                    ! print for that processor only
    LOGICAL, INTENT(IN), OPTIONAL :: VERB                    ! print all nodes
    LOGICAL :: DO_OUTPUT, do_file
    INTEGER :: maxlist_, itmp
    REAL(pr) :: rtmp
    DECLARE_NODE_POINTER :: c_pointer, c_pointer1

    INTEGER, PARAMETER :: ML=3
    INTEGER :: ii,j,inum,inum1,inum2, ixyz(dim), &
         i_total, &    ! total number of nodes in the list
         inum4, &
         inum5, &
         i_tree, &     ! significant, ghost, or adjacent
         i_new,  &     ! new points (without pos_old flag set)
         i_old,  &     ! old points (with pos_old flag set)
         id,     &
         list ,    &
         wlt_type, &
         face_type, &
         proc, proc_l, proc_h, &
         ig_sig(ML), ig_adj(ML), ig_gho(ML), &   ! sum across all
         ig_adjonly(ML), ig_ghoonly(ML), &       ! the processors
         ig_old(ML), ig_new(ML), &               ! (for each link-list)
         total_this, total_others                ! total number of nodes on the processor
    
    DO_OUTPUT = .TRUE.                                      ! set default value for DO_OUTPUT
    IF ( PRESENT(OUTPUT) ) DO_OUTPUT = OUTPUT
    maxlist_ = list_all
    IF (PRESENT(MAXLIST)) maxlist_ = MAXLIST
    do_file = .FALSE.
    IF (PRESENT(FILE)) do_file = .TRUE.
    
    proc_l = 0                                 ! default - statistics for all processor nodes
    proc_h = par_size-1                        ! ...
    IF (.NOT.DO_OUTPUT) THEN                   ! this is to compute number of ghosts
       proc_l = par_rank                       ! (used in debugging from main)
       proc_h = par_rank
    END IF

    total_this = 0
    total_others = 0
    
    DO proc = proc_l, proc_h
       DO list = list_sig,maxlist_
          inum = 0
          inum1 = 0
          inum2 = 0
          i_total = 0
          inum4 = 0
          inum5 = 0
          i_tree = 0
          i_new = 0; i_old = 0;
          DO j = 1, j_mx
             DO wlt_type = MIN(j-1,1),2**dim-1
                DO face_type = 0, 3**dim - 1
                   CALL DB_get_initial_type_level_node (proc, wlt_type, j, face_type, ii, c_pointer, list)
                   IF(ii > 0) THEN
                      !PRINT *, wlt_type,j,face_type,ii
                      
                      DO WHILE (is_ok(c_pointer))

                         CALL DB_get_id_by_pointer ( c_pointer, id )
                         
                         IF (PRESENT(VERB)) THEN
                            CALL DB_get_coordinates_by_pointer ( c_pointer, ixyz )
                            CALL DB_get_function_by_pointer (c_pointer,1,1,rtmp)
                            CALL DB_get_ifunction_by_pointer (c_pointer,1,1,itmp)

                            WRITE (UNIT=FILE, FMT='(A,'//CHAR(ICHAR('0')+dim)//'I4,A,4I1,A,E12.5)') &
                                 'ixyz=', ixyz, ', id= ', &
                                 IBITS(id,3,1),IBITS(id,2,1),IBITS(id,1,1),IBITS(id,0,1), &
                                 ', f(1)=',rtmp
                         END IF

                         IF(BTEST(id,pos_sig)) inum  = inum  + 1
                         IF(BTEST(id,pos_adj)) inum1 = inum1 + 1
                         IF(BTEST(id,pos_gho)) inum2 = inum2 + 1
                         i_total = i_total + 1
                         IF(BTEST(id,pos_adj) .AND. .NOT.BTEST(id,pos_sig) ) inum4 = inum4 + 1
                         IF(BTEST(id,pos_gho) .AND. .NOT.(BTEST(id,pos_sig) .OR. BTEST(id,pos_adj))  ) inum5 = inum5 + 1
                         IF(BTEST(id,pos_gho) .OR. BTEST(id,pos_sig) .OR. BTEST(id,pos_adj)  ) i_tree = i_tree + 1
                         IF (BTEST(id,pos_old)) THEN
                            i_old = i_old + 1
                         ELSE
                            i_new = i_new + 1
                         END IF
                         
#ifdef TREE_NODE_POINTER_STYLE_C
                         CALL DB_get_next_type_level_node (proc, wlt_type, j, face_type, c_pointer, c_pointer1, list)
                         c_pointer = c_pointer1
#elif defined TREE_NODE_POINTER_STYLE_F
                         CALL DB_get_next_type_level_node (c_pointer)
#endif
                      END DO
                      
                   END IF
                END DO
             END DO
          END DO

          IF (proc.EQ.par_rank) THEN
             total_this = total_this + i_total
          ELSE
             total_others = total_others + i_total
          END IF

          IF (DO_OUTPUT) THEN
             IF(list == list_sig) THEN
                IF (do_file) THEN
                   WRITE( UNIT=FILE, FMT='("DB info: list_sig, proc=",I8)') proc
                ELSE
                   WRITE( *, '("DB info: list_sig, proc=",I8)') proc
                END IF
             ELSE IF(list == list_gho) THEN
                IF (do_file) THEN
                   WRITE( UNIT=FILE, FMT='("DB info: list_gho, proc=",I8)') proc
                ELSE
                   WRITE( *, '("DB info: list_gho, proc=",I8)') proc
                END IF
             ELSE
                IF (do_file) THEN
                   WRITE( UNIT=FILE, FMT='("DB info: list_all, proc=",I8)') proc
                ELSE
                   WRITE( *, '("DB info: list_all, proc=",I8)') proc
                END IF
             END IF
             IF (do_file) THEN
                IF (inum.NE.0.OR.inum1.NE.0.OR.inum2.NE.0.OR.i_tree.NE.0.OR.i_total.NE.0.OR.inum4.NE.0.OR.inum5.NE.0.OR.i_old.NE.0.OR.i_new.NE.0) THEN
                   WRITE( UNIT=FILE, FMT='(" sig=",I8,", adj=",I8,", gho=",I8,/,", tree=",I8,", tot=",I8,", adj_only=",I8,", gho_only=",I8)') &
                        inum, inum1, inum2, i_tree, i_total, inum4, inum5
                   WRITE( UNIT=FILE, FMT='(" old = ",I8,", new = ",I8)') i_old, i_new
                END IF
             ELSE
                WRITE( *, '("Nsig=",I8," Nadj=",I8," Ngho=",I8,/,"Ntree=",I8," Ntot=",I8," Nadj_only=",I8," Ngho_only=",I8)') &
                     inum, inum1, inum2, i_tree, i_total, inum4, inum5
                WRITE( *, '("old = ",I8,", new = ",I8)') i_old, i_new
             END IF
          END IF
          IF (proc.EQ.par_rank) THEN
             ig_sig(list) = inum
             ig_adj(list) = inum1
             ig_gho(list) = inum2
             ig_adjonly(list) = inum4
             ig_ghoonly(list) = inum5
             ig_old(list) = i_old
             ig_new(list) = i_new
          END IF
       END DO
    END DO
    
    nwlt_p_ghost = i_total !store total # total active wavelets plus ghost points
    
#ifdef MULTIPROC
    IF (par_size.GT.1.AND.do_file) THEN
       DO list = list_sig,maxlist_
          CALL parallel_global_sum (INTEGER=ig_sig(list))
          CALL parallel_global_sum (INTEGER=ig_adj(list))
          CALL parallel_global_sum (INTEGER=ig_gho(list))
          CALL parallel_global_sum (INTEGER=ig_adjonly(list))
          CALL parallel_global_sum (INTEGER=ig_ghoonly(list))
          CALL parallel_global_sum (INTEGER=ig_old(list))
          CALL parallel_global_sum (INTEGER=ig_new(list))
       END DO
       WRITE( UNIT=FILE, FMT='(" +--list_sig-------------------------------------------")')
       WRITE( UNIT=FILE, FMT='(" |  sig=",I8,", adj=",I8,", gho=",I8,/," |  old =",I8,", new=",I8,", adj_only=",I8,", gho_only=",I8)') &
            ig_sig(1), ig_adj(1), ig_gho(1), ig_old(1), ig_new(1), ig_adjonly(1), ig_ghoonly(1)
       WRITE( UNIT=FILE, FMT='(" +--list_gho-------------------------------------------")')
       WRITE( UNIT=FILE, FMT='(" |  sig=",I8,", adj=",I8,", gho=",I8,/," |  old =",I8,", new=",I8,", adj_only=",I8,", gho_only=",I8)') &
            ig_sig(2), ig_adj(2), ig_gho(2), ig_old(2), ig_new(2), ig_adjonly(2), ig_ghoonly(2)
       WRITE( UNIT=FILE, FMT='(" +-----------------------------------------------------")')   
    END IF
    IF (debug_level.GT.0) PRINT *, '#$ proc/this/others = ', par_rank, total_this, total_others
    CALL parallel_global_sum (INTEGER=total_this)
    CALL parallel_global_sum (INTEGER=total_others)
    IF (debug_level.GT.0) PRINT *, '#$ all /this/others = ', total_this, total_others
#endif
    
  END SUBROUTINE count_DB

  SUBROUTINE clean_DB( list, Rvec_clean, Ivec_clean,  proc_l, proc_h )
    USE precision
    USE wlt_vars
    USE debug_vars
    USE db_tree_vars        ! pointer
    USE parallel            ! par_size
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: list
    LOGICAL, INTENT(IN) :: Rvec_clean,Ivec_clean
    INTEGER, INTENT(IN), OPTIONAL :: proc_l, proc_h ! for processors [proc_l, proc_h]
    DECLARE_NODE_POINTER :: c_pointer, c_pointer1

    INTEGER :: ii,j
    INTEGER :: wlt_type, face_type, proc, proc_l_, proc_h_
    REAL (pr), DIMENSION (1:NvecR_DB) :: u_clean
    INTEGER, DIMENSION (1:NvecI_DB) :: i_clean

    ! set processor limits
    proc_l_ = 0
    proc_h_ = par_size-1
    IF (PRESENT(proc_l).AND.PRESENT(proc_h)) THEN
       proc_l_ = proc_l
       proc_h_ = proc_h
    END IF
    
    u_clean = 0.0_pr
    i_clean = 0
    DO proc = proc_l_, proc_h_
       DO j = 1, j_mx
          DO wlt_type = MIN(j-1,1),2**dim-1
             DO face_type = 0, 3**dim - 1
                CALL DB_get_initial_type_level_node (proc, wlt_type, j, face_type, ii, c_pointer, list)
                IF(ii > 0) THEN
                   
                   DO WHILE (is_ok(c_pointer))
#ifdef TREE_NODE_POINTER_STYLE_C
                      IF(Rvec_clean)  CALL DB_set_function_by_pointer( c_pointer, 1, NvecR_DB, u_clean )
                      IF(Ivec_clean) CALL DB_set_ifunction_by_pointer( c_pointer, 1, NvecI_DB, i_clean )
                      CALL DB_get_next_type_level_node (proc, wlt_type, j, face_type, c_pointer, c_pointer1, list)
                      c_pointer = c_pointer1
#elif defined TREE_NODE_POINTER_STYLE_F
                      IF(Rvec_clean)  CALL DB_set_function_by_pointer( c_pointer, 1, NvecR_DB, u_clean )
                      IF(Ivec_clean) CALL DB_set_ifunction_by_pointer( c_pointer, 1, NvecI_DB, i_clean )
                      CALL DB_get_next_type_level_node (c_pointer)
#endif
                   END DO
                   
                END IF
             END DO
          END DO
       END DO
    END DO
    
    IF ( BTEST(debug_level,3) ) &
         PRINT *,'DB is cleaned'
    
  END SUBROUTINE clean_DB

  !OLEG: 03/16/2007 - modify, not to clean id for in/out point, once introduced, and flag idicates to do so
  !  make sure that there could be multiple objects
  SUBROUTINE clean_id_DB (list, proc_l, proc_h)
    USE precision
    USE wlt_vars
    USE debug_vars
    USE db_tree_vars        ! pointer
    USE parallel            ! par_size
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: list
    INTEGER, INTENT(IN), OPTIONAL :: proc_l, proc_h ! for processors [proc_l, proc_h]
    DECLARE_NODE_POINTER :: c_pointer, c_pointer1
    
    INTEGER :: ii,j, &
         id, wlt_type, face_type, proc, &
         proc_l_, proc_h_, num
    
    ! set processor limits
    proc_l_ = 0
    proc_h_ = par_size-1
    IF (PRESENT(proc_l).AND.PRESENT(proc_h)) THEN
       proc_l_ = proc_l
       proc_h_ = proc_h
    END IF

    num = 0
    DO proc = proc_l_, proc_h_
       DO j = 1, j_mx
          DO wlt_type = MIN(j-1,1),2**dim-1
             DO face_type = 0, 3**dim - 1
                CALL DB_get_initial_type_level_node (proc, wlt_type, j, face_type, ii, c_pointer, list)
                IF(ii > 0) THEN
                   
                   DO WHILE (is_ok(c_pointer))
                      
                      CALL DB_get_id_by_pointer ( c_pointer, id )
                      IF (BTEST(id,pos_old)) THEN
                         id = IBSET(0,pos_old)
                      ELSE
                         id = 0
                      END IF
                      CALL DB_set_id_by_pointer ( c_pointer, id )
                      num = num + 1
                      
#ifdef TREE_NODE_POINTER_STYLE_C
                      CALL DB_get_next_type_level_node (proc, wlt_type, j, face_type, c_pointer, c_pointer1, list)
                      c_pointer = c_pointer1
#elif defined TREE_NODE_POINTER_STYLE_F
                      CALL DB_get_next_type_level_node (c_pointer)
#endif
                   END DO
                   
                END IF
             END DO
          END DO
       END DO
    END DO

    IF ( BTEST(debug_level,3) ) &
         PRINT *,'DB id is cleaned: num=', num
    
  END SUBROUTINE clean_id_DB


  SUBROUTINE COMPARE_DB (pos_high)
    INTEGER, INTENT (IN) :: pos_high
  END SUBROUTINE COMPARE_DB

  !need function MODVEC(X,Y) where X and Y are vectors of length n

  !*******************************************************************
  !*          SUBROUTINES for wavelet transform                      *
  !*******************************************************************

  SUBROUTINE wlt_trns_DB (ulocal, nlocal, ilow, ihigh, j_in, wlt_fmly, trnsf_type, flag) !, update_db_from_u)
    USE precision
    USE wlt_vars
    USE parallel
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: nlocal, ilow, ihigh, j_in, wlt_fmly, &
         trnsf_type, &       ! NORMAL/INTERNAL transform, internal does not include boundary points
         flag
    !    LOGICAL, INTENT (IN) :: update_db_from_u
    REAL (pr), DIMENSION (1:nlocal,1:ihigh-ilow+1), INTENT (INOUT) :: ulocal
    INTEGER  :: j
    !!INTEGER :: step_in
    INTEGER, DIMENSION(2*dim)::  ibch

!!$    IF (par_rank.EQ.0) PRINT *, 'lh_wlt=',lh_wlt; CALL parallel_finalize; STOP

    ibch(1:2*dim) = 0
    IF(trnsf_type == 1) ibch(1:2*dim) = ibc(1:2*dim)
    !!step_in = 2**(j_mx - j_in)

    !    IF( update_db_from_u ) CALL  write_DB (ulocal(1:nlocal,1:ihigh-ilow+1), nlocal, ilow, ihigh, j_in)

    IF (flag > 0) THEN       ! forward transform
       DO j = j_in,2,-1
          CALL wlt_trns_aux_DB (j, ilow, ihigh, wlt_fmly, trnsf_type, list_sig, flag)
       END DO
    ELSE IF (flag < 0) THEN  ! inverse transform
       DO j = 2,j_in
          CALL wlt_trns_aux_DB (j, ilow, ihigh, wlt_fmly, trnsf_type, list_sig, flag)
       END DO
    END IF

    ! read data from the database
    CALL  read_DB (ulocal(1:nlocal,1:ihigh-ilow+1), nlocal, ilow, ihigh, j_in)

  END SUBROUTINE wlt_trns_DB

  SUBROUTINE wlt_trns_aux_DB (j, ilow, ihigh, wlt_fmly, trnsf_type, list, flag, proc_l, proc_h)
    USE precision
    USE wlt_vars
    USE db_tree_vars        ! pointer
    USE parallel            ! par_size
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: j, ilow, ihigh, wlt_fmly, trnsf_type, list, flag
    INTEGER, INTENT(IN), OPTIONAL :: proc_l, proc_h
    REAL (pr), DIMENSION (ihigh-ilow+1) :: c_predict, dc_predict
    DECLARE_NODE_POINTER :: c_pointer, c_pointer1
    
    INTEGER  :: ii, jj, ixpm, id, i_p_face(0:dim), face(dim)
    INTEGER  :: wlt_type, face_type, proc, proc_l_, proc_h_
    INTEGER :: idim, step_lev, step, step2
    INTEGER, DIMENSION(2*dim)::  ibch
    
    ! this is to be moved outside ------
    i_p_face(0) = 1
    DO idim=1,dim
       i_p_face(idim) = i_p_face(idim-1)*3
    END DO
    ! this is to be moved outside ------


    proc_l_ = 0; proc_h_ = par_size-1
    IF (PRESENT(proc_l)) THEN
       proc_l_ = proc_l
       proc_h_ = proc_h
    END IF
    
    ibch(1:2*dim) = 0
    IF(trnsf_type == 1) ibch(1:2*dim) = ibc(1:2*dim)
    step_lev = 2**(j_mx - j_lev)
    step  = 2**(j_mx-j)
    step2 = 2*step
!!$    PRINT *, 'in wlt_trns_aux_DB: ibch=',ibch(1:2*dim)

    IF (flag > 0) THEN       ! forward transform
       DO idim = 1,dim ! transform along idim direction
          !****** PREDICT STAGE ****** 
          predict: DO jj = j,j
             DO wlt_type = 1,2**dim-1
                ixyz_wlt_DB(:) = IBITS(wlt_type,ivec(:)-1,1)
                IF (ixyz_wlt_DB(idim) == 1) THEN ! odd points in idim-direction
                   DO face_type = 0, 3**dim - 1
                      DO proc = proc_l_, proc_h_ !0, par_size-1
                         CALL DB_get_initial_type_level_node (proc, wlt_type, jj, face_type, ii, c_pointer, list)
                         IF(ii > 0) THEN
                            
                            DO WHILE (is_ok(c_pointer))
                               
                               CALL DB_get_coordinates_by_pointer ( c_pointer, ixyz_DB )
                               ixyz1_DB = ixyz_DB
                               CALL DB_get_function_by_pointer( c_pointer, ilow, ihigh, c_predict )
!!$                               PRINT *, 'trns odd  bfr', ixyz_DB/2**(j_mx-j_lev), c_predict
                               
                               i_l(idim) = lh_wlt(low_limit, predict_stage,wlt_fmly,trnsf_type,j,ixyz_DB(idim),idim)
                               i_h(idim) = lh_wlt(high_limit,predict_stage,wlt_fmly,trnsf_type,j,ixyz_DB(idim),idim)
                               
!!$                            IF (par_rank.EQ.0) PRINT *, '$$1',ixyz_DB, c_predict(1), i_l(idim), i_h(idim)
                               
                               DO ixpm = i_l(idim),i_h(idim)
                                  ixyz1_DB(idim) = ixpm * step2 + ixyz_DB(idim) + step
                                  ixyz1_DB(idim) = (1-prd_DB(idim))*ixyz1_DB(idim) + prd_DB(idim)*MOD(ixyz1_DB(idim)+9*nxyz_DB(idim),nxyz_DB(idim))
                                  
                                  CALL DB_get_function_by_jmax_coordinates(ixyz1_DB,ilow, ihigh, dc_predict) ! 0 if node not present
!!$                               IF (par_rank.EQ.0) PRINT *, '  $$1', ixyz1_DB, dc_predict(1)
!!$                                  PRINT *, '  GET ',ixyz1_DB/2**(j_mx-j_lev), dc_predict
                                  
                                  c_predict(:) = c_predict(:) &
                                       - wgh_DB(ixpm,ixyz_DB(idim),j,wlt_fmly,trnsf_type,idim) * dc_predict(:) 
                               END DO
                               c_predict(:) = 0.5_pr*c_predict(:)
                               CALL DB_set_function_by_pointer( c_pointer, ilow, ihigh, c_predict )
!!$                            IF (par_rank.EQ.0) PRINT *, '  $$1', 'set to',c_predict
!!$                               PRINT *, 'trns odd aftr', ixyz_DB/2**(j_mx-j_lev), c_predict
!!$                               PRINT *, ' '
#ifdef TREE_NODE_POINTER_STYLE_C
                               CALL DB_get_next_type_level_node (proc, wlt_type, jj, face_type, c_pointer, c_pointer1, list)
                               c_pointer = c_pointer1
#elif defined TREE_NODE_POINTER_STYLE_F
                               CALL DB_get_next_type_level_node (c_pointer)
#endif
                            END DO
                            
                         END IF
                      END DO
                   END DO
                END IF
             END DO
          END DO predict

!#ifndef MULTIPROC
          IF (ANY(n_updt(0:n_wlt_fmly).NE.0)) THEN
          !****** UPDATE STAGE ****** 
          update: DO jj = 1,j ! need to change to 1,j inneweer version
             DO wlt_type = MIN(jj-1,1),2**dim-1
                ixyz_wlt_DB(:) = MAX(0,jj+1-j)*IBITS(wlt_type,ivec(:)-1,1) ! points on levels coarser thatn j are of 0 type
!!$                PRINT *, 'wlt_type/ixyz_wlt_DB=',wlt_type, ixyz_wlt_DB(:)

                IF (ixyz_wlt_DB(idim) == 0) THEN                           ! even points in idim-direction
                   DO face_type = 0, 3**dim - 1
                      face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
!!$                      PRINT *, 'face_type=', face_type,'face=', face
                      
                      IF ( (face(idim).EQ.-1.AND.ibch(2*idim-1).EQ.1).OR. &     ! left algebraic bc, skips points
                           (face(idim).EQ.1.AND.ibch(2*idim).EQ.1) ) CYCLE      ! right ...

                      DO proc = proc_l_, proc_h_ !0, par_size-1
                         CALL DB_get_initial_type_level_node (proc, wlt_type, jj, face_type, ii, c_pointer, list)
                         IF (ii > 0) THEN

                            DO WHILE (is_ok(c_pointer))

                               CALL DB_get_coordinates_by_pointer ( c_pointer, ixyz_DB )
                               ixyz1_DB = ixyz_DB
                               CALL DB_get_function_by_pointer( c_pointer, ilow, ihigh, c_predict )
!!$                               PRINT *, 'trns even  bfr', ixyz_DB/2**(j_mx-j_lev), c_predict

                               i_l(idim) = lh_wlt(low_limit, update_stage,wlt_fmly,trnsf_type,j,ixyz_DB(idim),idim)
                               i_h(idim) = lh_wlt(high_limit,update_stage,wlt_fmly,trnsf_type,j,ixyz_DB(idim),idim)

!!$                            IF (par_rank.EQ.0) PRINT *,'   $2',ixyz_DB,c_predict(1), i_l(idim), i_h(idim)

                               DO ixpm = i_l(idim),i_h(idim)
                                  ixyz1_DB(idim) = ixpm * step2 + ixyz_DB(idim) + step
                                  ixyz1_DB(idim) = (1-prd_DB(idim))*ixyz1_DB(idim) + prd_DB(idim)*MOD(ixyz1_DB(idim)+9*nxyz_DB(idim),nxyz_DB(idim))

                                  CALL DB_get_function_by_jmax_coordinates(ixyz1_DB,ilow, ihigh, dc_predict ) ! 0 if node not present
!!$                               IF (par_rank.EQ.0) PRINT *,'     $2',ixyz1_DB,dc_predict(1)
!!$                                  PRINT *, '  GET ',ixyz1_DB/2**(j_mx-j_lev), dc_predict

                                  c_predict(:) = c_predict(:) &
                                       + wgh_DB(ixpm,ixyz_DB(idim),j,wlt_fmly,trnsf_type,idim) * dc_predict(:) 
                               END DO
                               CALL DB_set_function_by_pointer( c_pointer, ilow, ihigh, c_predict )
!!$                               IF (par_rank.EQ.0) PRINT *,'     $2', 'set to',c_predict(1)

#ifdef TREE_NODE_POINTER_STYLE_C
                               CALL DB_get_next_type_level_node (proc, wlt_type, jj, face_type, c_pointer, c_pointer1, list)
                               c_pointer = c_pointer1
#elif defined TREE_NODE_POINTER_STYLE_F
                               CALL DB_get_next_type_level_node (c_pointer)
#endif
!!$                               PRINT *, 'trns even aftr', ixyz_DB/2**(j_mx-j_lev), c_predict
!!$                               PRINT *, ' '
                            END DO

                         END IF
                      END DO

                   END DO
                END IF
             END DO
          END DO update
!#endif
          END IF

       END DO
    ELSE IF (flag < 0) THEN  ! inverse transform
       DO idim = dim,1,-1 ! transform along idim direction

!#ifndef MULTIPROC
          IF (ANY(n_updt(0:n_wlt_fmly).NE.0)) THEN
          !****** UPDATE STAGE ****** 
          update_inv: DO jj = 1,j ! need to change to 1,j inneweer version
             DO wlt_type = MIN(jj-1,1),2**dim-1
                ixyz_wlt_DB(:) = MAX(0,jj+1-j)*IBITS(wlt_type,ivec(:)-1,1) ! points on levels coarser thatn j are of 0 type
                IF (ixyz_wlt_DB(idim) == 0) THEN                           ! even points in idim-direction
                   DO face_type = 0, 3**dim - 1
                      face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
                      
                      IF ( (face(idim).EQ.-1.AND.ibch(2*idim-1).EQ.1).OR. &     ! left algebraic bc, skips points
                           (face(idim).EQ.1.AND.ibch(2*idim).EQ.1) ) CYCLE      ! right ...
                      
                      DO proc = proc_l_, proc_h_ !0, par_size-1
                         CALL DB_get_initial_type_level_node (proc, wlt_type, jj, face_type, ii, c_pointer, list)   
                         IF (ii > 0) THEN

                            DO WHILE (is_ok(c_pointer))

                               CALL DB_get_coordinates_by_pointer ( c_pointer, ixyz_DB )
                               ixyz1_DB = ixyz_DB
                               ! Find previous (i_l) and next (i_h) point in each direction
                               CALL DB_get_function_by_pointer( c_pointer, ilow, ihigh, c_predict )
!!$                            CALL DB_set_id_by_pointer (c_pointer, 123)!@@@

                               i_l(idim) = lh_wlt(low_limit, update_stage,wlt_fmly,trnsf_type,j,ixyz_DB(idim),idim)
                               i_h(idim) = lh_wlt(high_limit,update_stage,wlt_fmly,trnsf_type,j,ixyz_DB(idim),idim)
                               DO ixpm = i_l(idim),i_h(idim)
                                  ixyz1_DB(idim) = ixpm * step2 + ixyz_DB(idim) + step
                                  ! next line is the slowest
                                  ixyz1_DB(idim) = (1-prd_DB(idim))*ixyz1_DB(idim) + prd_DB(idim)*MOD(ixyz1_DB(idim)+9*nxyz_DB(idim),nxyz_DB(idim))
!!$
!!$                               CALL DB_test_node (ixyz1_DB,j_mx,c_pointer1) !@@@
!!$                               IF (is_ok(c_pointer)) THEN
!!$                                  CALL DB_get_id_by_pointer (c_pointer1,id)
!!$                                  IF (id.EQ.123) STOP 'id is 123'
!!$                               END IF !@@@
                                  CALL DB_get_function_by_jmax_coordinates(ixyz1_DB,ilow, ihigh, dc_predict) ! 0 if node not present

                                  c_predict(:) = c_predict(:) &
                                       - wgh_DB(ixpm,ixyz_DB(idim),j,wlt_fmly,trnsf_type,idim) * dc_predict(:) 
                               END DO
                               CALL DB_set_function_by_pointer( c_pointer, ilow, ihigh, c_predict )

#ifdef TREE_NODE_POINTER_STYLE_C
                               CALL DB_get_next_type_level_node (proc,wlt_type,jj,face_type,c_pointer,c_pointer1,list)
                               c_pointer = c_pointer1
#elif defined TREE_NODE_POINTER_STYLE_F
                               CALL DB_get_next_type_level_node (c_pointer)
#endif
                            END DO

                         END IF
                      END DO
                   END DO
                END IF
             END DO
          END DO update_inv
!#endif
          END IF

          !****** PREDICT STAGE ****** 
!!$          IF (list.NE.list_sig) PRINT *, 'predict_inv, jj=',j,'idim=',idim,'-----------------------'
          predict_inv: DO jj = j,j
             DO wlt_type = 1,2**dim-1
                ixyz_wlt_DB(:) = IBITS(wlt_type,ivec(:)-1,1)
                IF (ixyz_wlt_DB(idim) == 1) THEN ! odd points in idim-direction
                   DO face_type = 0, 3**dim - 1
                      DO proc = proc_l_, proc_h_ !0, par_size-1
                         CALL DB_get_initial_type_level_node (proc, wlt_type, jj, face_type, ii, c_pointer, list)
                         IF(ii > 0) THEN
                            
                            DO WHILE (is_ok(c_pointer))
                               
                               CALL DB_get_coordinates_by_pointer ( c_pointer, ixyz_DB )
                               ixyz1_DB = ixyz_DB
                               CALL DB_get_function_by_pointer( c_pointer, ilow, ihigh, c_predict )
!!$                            IF (jj.EQ.4) CALL DB_set_id_by_pointer (c_pointer, 123)!@@@
!!$                               IF (list.NE.list_sig) PRINT *, 'passing through ixyz_DB=',ixyz_DB, 'of', c_predict
                               
                               c_predict(:) = 2.0_pr*c_predict(:)
                               i_l(idim) = lh_wlt(low_limit, predict_stage,wlt_fmly,trnsf_type,j,ixyz_DB(idim),idim)
                               i_h(idim) = lh_wlt(high_limit,predict_stage,wlt_fmly,trnsf_type,j,ixyz_DB(idim),idim)
!!$                            PRINT *, '                i_l/i_h=',i_l(idim),i_h(idim)
                               DO ixpm = i_l(idim),i_h(idim)
                                  ixyz1_DB(idim) = ixpm * step2 + ixyz_DB(idim) + step
                                  ! next line is the slowest
                                  ixyz1_DB(idim) = (1-prd_DB(idim))*ixyz1_DB(idim) + prd_DB(idim)*MOD(ixyz1_DB(idim)+9*nxyz_DB(idim),nxyz_DB(idim))
!!$                               IF (list.NE.list_sig) PRINT *, '       ixyz1_DB=',ixyz1_DB
!!$                               
!!$                               CALL DB_test_node (ixyz1_DB,j_mx,c_pointer1) !@@@
!!$                               IF (jj.EQ.4 .AND. is_ok(c_pointer)) THEN
!!$                                  CALL DB_get_id_by_pointer (c_pointer1,id)
!!$                                  IF (id.EQ.123) THEN
!!$                                     PRINT *, 'jj=',jj,'ixyz1_DB=',ixyz1_DB
!!$                                     STOP 'id is 123'
!!$                                  END IF
!!$                               END IF !@@@
                                  
                                  CALL DB_get_function_by_jmax_coordinates(ixyz1_DB,ilow, ihigh, dc_predict) ! 0 if node not present
!!$                                  IF (list.NE.list_sig) PRINT *, '        of value',dc_predict
                                  c_predict(:) = c_predict(:) &
                                       + wgh_DB(ixpm,ixyz_DB(idim),j,wlt_fmly,trnsf_type,idim) * dc_predict(:) 
                               END DO
                               CALL DB_set_function_by_pointer( c_pointer, ilow, ihigh, c_predict )
!!$                               IF (list.NE.list_sig) PRINT *, '         final value set ==', c_predict

#ifdef TREE_NODE_POINTER_STYLE_C
                               CALL DB_get_next_type_level_node (proc, wlt_type, jj, face_type, c_pointer, c_pointer1, list)
                               c_pointer = c_pointer1
#elif defined TREE_NODE_POINTER_STYLE_F
                               CALL DB_get_next_type_level_node (c_pointer)
#endif
                            END DO
                            
                         END IF
                      END DO
                   END DO
                END IF
             END DO
          END DO predict_inv
       END DO
    END IF
    !IF (list.EQ.list_gho.AND.id.EQ.1) STOP

  END SUBROUTINE wlt_trns_aux_DB


  SUBROUTINE test_DB_values( ilow, ihigh )
    USE precision
    USE wlt_vars
    USE db_tree_vars        ! pointer
    USE parallel            ! par_size
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: ilow, ihigh
    INTEGER :: j, wlt_fmly, trnsf_type, list, flag
    REAL (pr), DIMENSION (ihigh-ilow+1) :: c_predict, dc_predict
    DECLARE_NODE_POINTER :: c_pointer, c_pointer1

    INTEGER  :: ii, jj, ixpm
    INTEGER  :: wlt_type, face_type, proc
    INTEGER :: idim, step_lev, step, step2, inum
    INTEGER, DIMENSION(2*dim)::  ibch

    DO proc = 0, par_size-1
       DO jj = 1,j_lev
          DO wlt_type = MIN(jj-1,1),2**dim-1
             DO face_type = 0, 3**dim - 1
                ixyz_wlt_DB(:) = IBITS(wlt_type,ivec(:)-1,1)
                CALL DB_get_initial_type_level_node (proc, wlt_type, jj, face_type, ii, c_pointer, list_sig)
                
                DO WHILE (is_ok(c_pointer))
#ifdef TREE_NODE_POINTER_STYLE_C
                   CALL DB_get_coordinates_by_pointer ( c_pointer, ixyz_DB )
                   CALL DB_get_function_by_pointer( c_pointer, ilow, ihigh, c_predict )
                   CALL DB_test_node(ixyz_DB,j_mx,c_pointer1)
                   IF(dim == 2) WRITE(*,'(i2,i2,"ixyz=",2(i3,1x),"c_pointer=",i16,I16,e12.5)') ilow,ihigh,ixyz_DB,c_pointer,c_pointer1,c_predict
                   IF(dim ==3 ) WRITE(*,'(i2,i2,"ixyz=",3(i3,1x),"c_pointer=",i16,I16,e12.5)') ilow,ihigh,ixyz_DB,c_pointer,c_pointer1,c_predict
                   CALL DB_get_next_type_level_node (proc, wlt_type, jj, face_type, c_pointer, c_pointer1, list_sig)
                   c_pointer = c_pointer1
#elif defined TREE_NODE_POINTER_STYLE_F
                   CALL DB_get_coordinates_by_pointer ( c_pointer, ixyz_DB )
                   CALL DB_get_function_by_pointer( c_pointer, ilow, ihigh, c_predict )
                   CALL DB_test_node(ixyz_DB,j_mx,c_pointer1)
                   IF(dim == 2) WRITE(*,'(i2,i2,"ixyz=",2(i3,1x),e12.5)') ilow,ihigh,ixyz_DB,c_predict
                   IF(dim ==3 ) WRITE(*,'(i2,i2,"ixyz=",3(i3,1x),e12.5)') ilow,ihigh,ixyz_DB,c_predict
                   CALL DB_get_next_type_level_node (c_pointer)
#endif
                END DO
                
             END DO
          END DO
       END DO
    END DO
  END SUBROUTINE test_DB_values


  SUBROUTINE wlt_diff_DB (u, du, d2u, j_in, nlocal, ilow, ihigh, meth, id)
    USE precision
    USE wlt_vars
    USE parallel
    USE db_tree_vars        ! pointer
    !USE pde                 ! n_var
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: j_in, nlocal,  ilow, ihigh, meth, id
    REAL (pr), DIMENSION (1:nlocal,1:ihigh-ilow+1), INTENT (IN) :: u
    REAL (pr), DIMENSION (1:ihigh-ilow+1,1:nlocal,1:dim), INTENT (INOUT) :: du,d2u
    INTEGER  :: i, j
    INTEGER :: trnsf_type, wlt_fmly
    INTEGER :: wlt_type, face_type, ii, id1
    REAL (pr), DIMENSION (ihigh-ilow+1) :: c_diff
    REAL (pr), DIMENSION (1:nlocal,1:ihigh-ilow+1) :: u_tmp
    LOGICAL :: var_mask(ihigh-ilow+1)
    DECLARE_NODE_POINTER :: c_pointer, c_pointer1
    
    

#ifdef MULTIPROC
    ! The nodes have been already added in init_DB or in adapt_grid
    ! all what we need is value synchronization in sig list
    ! to be able to make forward transform on sig nodes.
    ! The list of significant nodes marked as from other processors
    ! (whose values to be received from the others)
    ! and the list of the nodes whose values to be sent to others
    ! have been already created in make_lists_for_communications()
    var_mask  = .TRUE.
    CALL request_known_list (var_mask, ihigh-ilow+1, list_sig, j_in)
!!$    ! make list and request the required nodes
!!$    ! (provide the mask which variables are required)
!!$    CALL make_list_and_request (var_mask, ihigh-ilow+1, list_sig, j_in)
#endif

    
!!$    IF(INT(ID/10) /= 1 ) du = 0.0_pr
!!$    IF(MOD(ID,10) /= 1 ) d2u = 0.0_pr

    wlt_fmly = MOD(meth,2) 
    trnsf_type = 0
    

    DO j = j_in,2,-1
       !PRINT *, 'j=',j, 'calling wlt_trns_aux_DB(), ilow/ihigh=', ilow, ihigh
       !PRINT *, 'MIN/MAXVAL(ABS(u))=', MINVAL(ABS(u)), MAXVAL(ABS(u))
       !u_tmp = 0.0_pr
       CALL wlt_trns_aux_DB (j, ilow, ihigh, wlt_fmly, trnsf_type, list_sig, 1)
       !CALL read_DB (u_tmp, nlocal, ilow, ihigh, j_in)
       !PRINT *, 'MIN/MAXVAL(ABS(u))=', MINVAL(ABS(u_tmp)), MAXVAL(ABS(u_tmp))
    END DO

    
    DO j = 1,MIN(j_in,j_lev)
       IF( j >= 2  ) CALL wlt_trns_aux_DB (j, ilow, ihigh, wlt_fmly, trnsf_type, list_all, -1)
       IF(INT(ID/10) == 1 ) CALL wlt_diff_aux_DB (du , j, j_in, nlocal, ilow, ihigh, meth, 1) ! 1-st order derivative
       IF(MOD(ID,10) == 1 ) CALL wlt_diff_aux_DB (d2u, j, j_in, nlocal, ilow, ihigh, meth, 2) ! 2-nd order derivative
    END DO

!!$    PRINT *, 'at the end of  wlt_diff_DB'
!!$    CALL count_DB( MAXLIST=list_gho, FILE=6, VERB=.TRUE.)
    
    CALL clean_DB(list_gho,.TRUE.,.FALSE.)
    
  END SUBROUTINE wlt_diff_DB

  SUBROUTINE wlt_diff_aux_DB (du, jj, j_in, nlocal, ilow, ihigh, meth, order)
    USE precision
    USE wlt_vars          ! dim
    USE wlt_trns_vars     ! indx_DB
    USE parallel
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: jj, j_in, nlocal,  ilow, ihigh, meth, order
    REAL (pr), DIMENSION (1:ihigh-ilow+1,1:nlocal,1:dim), INTENT (INOUT) :: du
    REAL (pr), DIMENSION (ihigh-ilow+1) :: c_diff, dc_diff
    DECLARE_NODE_POINTER :: c_pointer, c_pointer1

    INTEGER  :: iloc, ii, ipm
    INTEGER  :: wlt_type, face_type, j, j_df, j_df_l, j_df_h
    INTEGER :: idim, step, step_lev, id, ixyz(dim), ixyz1(dim), i_p(0:dim)

!!$    PRINT *,'before du: ', jj, MINVAL(ABS(du)), MAXVAL(ABS(du))

    i_p(0) = 1
    DO ii=1,dim
       i_p(ii) = i_p(ii-1)*(1+mxyz(ii)*2**(j_lev-1))
    END DO

    
    step_lev  = 2**(j_mx-j_lev)
    step  = 2**(j_mx-jj)
    IF( jj < j_in) THEN
       j_df_l = jj
       j_df_h = jj
    ELSE
       j_df_l = jj
       j_df_h = j_lev
    END IF
    DO j = 1, jj
       DO wlt_type = MIN(j-1,1),2**dim-1
          DO face_type = 0, 3**dim - 1
             DO j_df = j_df_l, j_df_h
                DO ii = 1, indx_DB(j_df,wlt_type,face_type,j)%length  

!!$#ifdef TREE_NODE_POINTER_STYLE_C
!!$                   c_pointer = indx_DB(j_df,wlt_type,face_type,j)%p(ii)%nptr
!!$#elif defined TREE_NODE_POINTER_STYLE_F
!!$                   c_pointer => indx_DB(j_df,wlt_type,face_type,j)%p(ii)%nptr
!!$#endif
                   iloc = indx_DB(j_df,wlt_type,face_type,j)%p(ii)%i + indx_DB(j_in,wlt_type,face_type,j)%shift
!!$                   CALL DB_get_coordinates_by_pointer ( c_pointer, ixyz1 )
                   ixyz = INT(MOD(indx_DB(j_df,wlt_type,face_type,j)%p(ii)%ixyz-1,i_p(1:dim))/i_p(0:dim-1)) *2**(j_mx-j_lev)
!!$                   IF (ANY(ixyz(1:dim)-ixyz1(1:dim).NE.0)) THEN
!!$                      PRINT *, 'j_lev,j_mx=',j_lev,j_mx,'ixyz=',ixyz,'ixyz1=',ixyz1, ', i_p=', i_p
!!$                      STOP 'error'
!!$                   END IF
!!$                   xyz_tmp(1:dim) = INT(MOD(indx_DB(j_df,wlt_type,face_type,j)%p(ii)%ixyz-1,i_p(1:dim))/i_p(0:dim-1))
!!$                   
!!$                   ixyz_tmp2 = 1+SUM(xyz_tmp(1:dim)*i_p(0:dim-1))
!!$                   ixyz_tmp = 1+SUM(ixyz_DB(1:dim)/2**(j_mx-j_lev)*i_p(0:dim-1))
!!$                   IF (ixyz_tmp.NE.indx_DB(j_df,wlt_type,face_type,j)%p(ii)%ixyz.OR.&
!!$                        ixyz_tmp.NE.ixyz_tmp2) THEN
!!$                      PRINT *, 'ixyz_DB=',ixyz_DB,',ixyz_tmp=',ixyz_tmp,', %ixyz=',indx_DB(j_df,wlt_type,face_type,j)%p(ii)%ixyz
!!$                      PRINT *, '%nptr=',c_pointer, '%i=',indx_DB(j_df,wlt_type,face_type,j)%p(ii)%i, 'i_p=',i_p
!!$                      PRINT *, 'xyz_tmp=',xyz_tmp,'ixyz_tmp2=',ixyz_tmp2
!!$                      STOP 'error'
!!$                   END IF

                   DO idim = 1,dim
                      i_l(idim)  = lh_diff(meth, order, low_limit,  jj, ixyz(idim), idim)
                      i_h(idim)  = lh_diff(meth, order, high_limit, jj, ixyz(idim), idim)
                      c_diff = 0.0_pr
                      ixyz1 = ixyz
                      DO ipm = i_l(idim),i_h(idim)
                         ixyz1(idim) = (1-prd_DB(idim))*(ixyz(idim)+ipm*step) &
                              + prd_DB(idim)*MOD(ixyz(idim)+ipm*step+9*nxyz_DB(idim),nxyz_DB(idim))
                         
                         CALL DB_get_function_by_jmax_coordinates(ixyz1, ilow, ihigh, dc_diff)

                         c_diff(:) = c_diff(:) + wgh_df_DB(ipm,ixyz(idim),jj,meth,order,idim) * dc_diff(:)
                       END DO
                      du(:,iloc,idim) = c_diff(:)
                   END DO

                END DO
             END DO
          END DO
       END DO
    END DO

  END SUBROUTINE wlt_diff_aux_DB

  SUBROUTINE diff_diag_aux_DB ( du, nlocal, j_in, meth, meth1, order)
    USE precision
    USE wlt_vars          ! dim
    USE wlt_trns_vars     ! indx_DB
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: nlocal, j_in, meth, meth1, order
    REAL (pr), DIMENSION (1:nlocal,dim),   INTENT (INOUT) :: du
    DECLARE_NODE_POINTER :: c_pointer, c_pointer1

    INTEGER  :: i_p(0:dim), ixyz(dim), iD, &
         wlt_type, face_type, j, jj, j_df, j_df_l, j_df_h, &
         idim, step, step_lev, ipm, ii, iloc
    INTEGER, PARAMETER :: first_order = 1

!!$    PRINT *, 'in =',du,'wgh_df_DB=',wgh_df_DB

    i_p(0) = 1
    DO ii=1,dim
       i_p(ii) = i_p(ii-1)*(1+mxyz(ii)*2**(j_lev-1))
    END DO
    
    DO jj = 1,MIN(j_in,j_lev)
       step_lev  = 2**(j_mx-j_lev)
       step  = 2**(j_mx-jj)
       IF( jj < j_in) THEN
          j_df_l = jj
          j_df_h = jj
       ELSE
          j_df_l = jj
          j_df_h = j_lev
       END IF
       DO j = 1, jj
          DO wlt_type = MIN(j-1,1),2**dim-1
             DO face_type = 0, 3**dim - 1
                DO j_df = j_df_l, j_df_h
                   DO ii = 1, indx_DB(j_df,wlt_type,face_type,j)%length  

!!$#ifdef TREE_NODE_POINTER_STYLE_C
!!$                      c_pointer = indx_DB(j_df,wlt_type,face_type,j)%p(ii)%nptr
!!$#elif defined TREE_NODE_POINTER_STYLE_F
!!$                      c_pointer => indx_DB(j_df,wlt_type,face_type,j)%p(ii)%nptr
!!$#endif
!!$                      CALL DB_get_coordinates_by_pointer ( c_pointer, ixyz )
                      
                      ixyz = INT(MOD(indx_DB(j_df,wlt_type,face_type,j)%p(ii)%ixyz-1,i_p(1:dim))/i_p(0:dim-1)) *2**(j_mx-j_lev)
                      iloc = indx_DB(j_df,wlt_type,face_type,j)%p(ii)%i + indx_DB(j_in,wlt_type,face_type,j)%shift

                      IF(order < 3) THEN
                         DO idim = 1,dim
                            du(iloc,idim) = wgh_df_DB(0,ixyz(idim),jj,meth,order,idim)
                            !PRINT *,' du()=',du(iloc,idim),ixyz(idim),jj,meth,order,idim
                          END DO
                      ELSE IF(order == 3) THEN
                         DO idim = 1,dim
                            i_l(idim)  = MAX(lh_diff(meth, first_order, low_limit,  jj, ixyz(idim), idim), &
                                             -lh_diff(meth1, first_order, high_limit, jj, ixyz(idim), idim) )
                            i_h(idim)  = MIN(lh_diff(meth, first_order, high_limit, jj, ixyz(idim), idim), &
                                            -lh_diff(meth1, first_order, low_limit,  jj, ixyz(idim), idim) )
                            du(iloc,idim) = 0.0_pr
                            DO ipm = i_l(idim),i_h(idim)
                               du(iloc,idim) = du(iloc,idim) + wgh_df_DB( ipm,ixyz(idim),jj,meth, first_order,idim)* &
                                                               wgh_df_DB(-ipm,ixyz(idim),jj,meth1,first_order,idim)
                            END DO
                         END DO
                      END IF
                   END DO
                END DO
             END DO
          END DO
       END DO
    END DO

!!$    PRINT *,'out=',du

  END SUBROUTINE diff_diag_aux_DB


  PURE FUNCTION ixyz2j_DB (ixyz, Jmx) RESULT(j)
    USE precision
    USE wlt_vars
    IMPLICIT NONE 
    INTEGER, INTENT (IN):: Jmx
    INTEGER, DIMENSION(dim), INTENT (IN):: ixyz
    INTEGER :: i, j, k
    j = 1
    DO i = 1,dim
       k = 0
       DO WHILE (MOD(ixyz(i)+2**k, 2**(k+1)) /= 0 .AND. k < Jmx-1)
          k = k + 1
       END DO
       j = MAX(j,Jmx-k)
    END DO

  END FUNCTION ixyz2j_DB

  SUBROUTINE weights_DB (dA,nlocal,loc_list,pos_h)
    !--Calculates weights for volume integration based on the Trapezoid
    !--integration rule (second-order accurate).
    !--loc_list - list of points (significant, adjacent, ghost) used for calculation
    USE precision
    USE wlt_vars
    USE db_tree_vars        ! pointer
    USE parallel            ! par_size
    IMPLICIT NONE

    INTEGER, INTENT(IN) :: nlocal, loc_list, pos_h
    REAL(pr), DIMENSION(1:nlocal), INTENT(INOUT) :: dA
    INTEGER :: i, ii, j, jj, k, idim, case, case1
    INTEGER, DIMENSION(dim,0:1,0:2**dim-1) :: k_arr
    INTEGER, DIMENSION(dim,0:1) :: i_k
    REAL (pr), DIMENSION(dim) :: lxyz
    REAL (pr) :: shft(dim,0:1), dAh

    STOP 'in weights_DB'
!!$#ifdef TREE_NODE_POINTER_STYLE_C
!!$    INTEGER(pointer_pr) :: c_pointer, c_pointer1      ! pointer to node of tree structure (integer in C/C++)
!!$#elif defined TREE_NODE_POINTER_STYLE_F
!!$    TYPE(node), POINTER :: c_pointer, c_pointer1           ! (Fortran's native pointer)
!!$#endif
!!$
!!$    INTEGER :: wlt_type, face_type, proc
!!$    INTEGER :: step, id
!!$    LOGICAL :: check, check_cube
!!$
!!$    ! change to dim loops in accordance to DB plus use DB, not i_c
!!$
!!$    dA = 0.0_pr
!!$    i_k = 0
!!$    lxyz(:) = xyzlimits(2,:)-xyzlimits(1,:)
!!$
!!$    DO case = 0, 2**dim-1 
!!$       ixyz_DB(:) = IBITS(case,ivec(:)-1,1)
!!$       DO idim = 1,dim
!!$          DO k = 0,1
!!$             k_arr(idim,k,case)=((-1)**ixyz_DB(idim)-(-1)**k)/2
!!$          END DO
!!$       END DO
!!$    END DO
!!$
!!$    DO proc = 0, par_size-1
!!$       DO j = 1, j_lev
!!$          DO wlt_type = MIN(j-1,1),2**dim-1
!!$             DO face_type = 0, 3**dim - 1
!!$                CALL DB_get_initial_type_level_node (proc, wlt_type, j, face_type, ii, c_pointer, loc_list)
!!$                IF(ii > 0) THEN
!!$                   
!!$                   DO WHILE (is_ok(c_pointer))
!!$#ifdef TREE_NODE_POINTER_STYLE_C
!!$                      CALL DB_get_coordinates_by_pointer ( c_pointer, ixyz_DB )
!!$                      CALL DB_get_ifunction_by_pointer( c_pointer, nvarI_nwlt, nvarI_nwlt, i ) 
!!$#elif defined TREE_NODE_POINTER_STYLE_F
!!$                      CALL DB_get_coordinates_by_pointer ( c_pointer, ixyz_DB )
!!$                      i = c_pointer%ival( nvarI_nwlt )
!!$#endif
!!$                      
!!$                      !go through all the nodes on the grid
!!$                      DO case = 0,2**dim-1
!!$                         loop_j: DO jj = j_lev, j, -1
!!$                            step = 2**(j_mx-jj)
!!$                            DO idim=1,dim !local cube of size step
!!$                               i_k(idim,:)=ixyz_DB(idim)+step*k_arr(idim,:,case)
!!$                            END DO
!!$                            shft = 0.0_pr
!!$                            check = .TRUE.
!!$                            DO idim = 1,dim
!!$                               IF(prd_DB(idim) == 1 .AND. i_k(idim,0) < 0) shft(idim,0)=lxyz(idim)
!!$                               IF(prd_DB(idim) == 1 .AND. i_k(idim,1) > nxyz_DB(idim)-prd_DB(idim)) shft(idim,1)=prd_DB(idim)*lxyz(idim)
!!$                               i_k(idim,:) = (1-prd_DB(idim))*i_k(idim,:)+prd_DB(idim)*MOD(i_k(idim,:)+9*nxyz_DB(idim),nxyz_DB(idim))
!!$                               check = (check .AND. (.NOT.( ANY(i_k(idim,:) > nxyz_DB(idim) - prd_DB(idim)) .OR. ANY(i_k(idim,:) < 0) ) ) )
!!$                            END DO
!!$                            IF(check) THEN
!!$                               check_cube = .TRUE.
!!$                               !checks if all vertices of the cube exists on the grid with node position types <= pos_h
!!$                               DO case1 = 0, 2**dim-1
!!$                                  ixyz_wlt_DB(:) = IBITS(case1,ivec(:)-1,1)
!!$                                  DO idim=1,dim !local cube of size step
!!$                                     ixyz1_DB(idim)=i_k(idim,ixyz_wlt_DB(idim))
!!$                                  END DO
!!$                                  CALL DB_test_node(ixyz1_DB(:),j_mx,c_pointer1)
!!$                                  
!!$#ifdef TREE_NODE_POINTER_STYLE_C
!!$                                  IF( c_pointer1 == 0 )  THEN                ! integer C/C++ pointer
!!$#elif defined TREE_NODE_POINTER_STYLE_F
!!$                                  IF( .NOT.ASSOCIATED(c_pointer1) ) THEN ! Fortran's native pointer
!!$#endif                      
!!$                                     check_cube = .FALSE.
!!$                                  ELSE
!!$                                     CALL DB_get_id_by_pointer ( c_pointer1, id )
!!$                                     IF(IBITS(id,pos_sig,pos_h-pos_sig+1) == 0 ) check_cube = .FALSE.
!!$                                  END IF
!!$                                  
!!$                               END DO
!!$                               IF(check_cube) THEN
!!$                                  dAh=1.0_pr
!!$                                  DO idim=1,dim
!!$                                     dAh=dAh*( xx_DB(i_k(idim,1),idim)-xx_DB(i_k(idim,0),idim)+shft(idim,0)+shft(idim,1) )
!!$                                  END DO
!!$                                  dA(i) = dA(i) + dAh/2.0_pr**dim
!!$                                  EXIT loop_j
!!$                               END IF
!!$                            END IF
!!$                         END DO loop_j
!!$                      END DO
!!$                      
!!$#ifdef TREE_NODE_POINTER_STYLE_C
!!$                      CALL DB_get_next_type_level_node (proc, wlt_type, j, face_type, c_pointer, c_pointer1, loc_list)
!!$                      c_pointer = c_pointer1
!!$#elif defined TREE_NODE_POINTER_STYLE_F
!!$                      CALL DB_get_next_type_level_node (c_pointer)
!!$#endif
!!$                   END DO
!!$                   
!!$                END IF
!!$             END DO
!!$          END DO
!!$       END DO
!!$    END DO
!!$
!!$    PRINT *, 'WEIGHTS_DB: area=',SUM(dA)
!!$    !PAUSE

  END SUBROUTINE weights_DB

  SUBROUTINE check_active_nodes_DB
    USE precision
    USE util_vars
    USE share_consts
    USE wlt_vars            ! j_mx, dim
    USE db_tree_vars        ! pointer
    USE parallel            ! par_size
    IMPLICIT NONE
    INTEGER :: j, wlt_type, face_type, ii, id, ixyz(dim), proc
    DECLARE_NODE_POINTER :: c_pointer, c_pointer1


    PRINT *, 'Checking active nodes'
    LOOP_J: DO proc = 0, par_size-1
       DO j = 1, j_mx
          DO wlt_type = MIN(j-1,1),2**dim - 1
             DO face_type = 0, 3**dim - 1
                CALL DB_get_initial_type_level_node (proc, wlt_type, j, face_type, ii, c_pointer, list_sig)
                IF(ii > 0) THEN
                   
#ifdef TREE_NODE_POINTER_STYLE_C
                   DO WHILE (is_ok(c_pointer))
                      CALL DB_get_id_by_pointer (c_pointer,id)
                      IF(.NOT.BTEST(id,pos_old)) THEN
                         CALL DB_get_coordinates_by_pointer ( c_pointer, ixyz )
                         PRINT *, id, ixyz(1:dim)
                      END IF
                      CALL DB_get_next_type_level_node (proc, wlt_type,j,face_type,c_pointer,c_pointer1,list_sig)
                      c_pointer = c_pointer1
                   END DO
#elif defined TREE_NODE_POINTER_STYLE_F
                   DO WHILE (is_ok(c_pointer))
                      CALL DB_get_id_by_pointer (c_pointer,id)
                      IF(.NOT.BTEST(id,pos_old)) THEN
                         CALL DB_get_coordinates_by_pointer ( c_pointer, ixyz )
                         PRINT *, id, ixyz(1:dim)
                      END IF
                      CALL DB_get_next_type_level_node (c_pointer)
                   END DO
#endif
                   
                END IF
             END DO
          END DO
       END DO
    END DO LOOP_J

!!$  PAUSE

  END SUBROUTINE check_active_nodes_DB

  SUBROUTINE add_nodes_DB (j_out)
    USE precision
    USE additional_nodes
    USE db_tree_vars        ! pointer
    USE parallel            ! par_rank
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: j_out
    DECLARE_NODE_POINTER :: c_pointer

    INTEGER  :: ierr, i, cproc

    IF(additional_nodes_active) THEN 
       CALL add_additional_nodes ( j_out )
       !---------- include all points specified by user ------------------
       IF( n_additional_nodes > 0 .AND. ALLOCATED(ixyz_additional_nodes) ) THEN
          DO i = 1, n_additional_nodes
             CALL DB_get_proc_by_coordinates( ixyz_additional_nodes(1:dim,i), j_out, cproc )
             IF (cproc.EQ.par_rank) &
                  CALL DB_add_node( ixyz_additional_nodes(1:dim,i), j_out, ierr, c_pointer, idp(pos_adj), beg_list )
          END DO
       END IF
    END IF
    
  END SUBROUTINE add_nodes_DB

  
  !---------------------------------------------------------------------------------------------
  ! test if node pointer is valid
  FUNCTION is_ok( c_pointer )
    USE db_tree_vars        ! pointer
    IMPLICIT NONE
    LOGICAL :: is_ok
    DECLARE_NODE_POINTER :: c_pointer
    
#ifdef TREE_NODE_POINTER_STYLE_C
    is_ok = (c_pointer /= 0)
#elif defined TREE_NODE_POINTER_STYLE_F
    is_ok = (ASSOCIATED(c_pointer))
#endif

  END FUNCTION is_ok



  !---------------------------------------------------------------------------------------------
  ! set list of the nodes marked as from other processors
  ! whose values to be received from the others and
  ! set list of the nodes whose values to be sent to others
  ! (local sig + local gho which sig on other proc)
  RECURSIVE SUBROUTINE make_lists_for_communications( do_ghosts, n_var_comm )
    USE parallel                       ! procbuffer, etc.
    USE wlt_vars                       ! j_mx
    USE db_tree_vars                   ! pointer
    LOGICAL, INTENT(IN) :: do_ghosts   ! true - recursion is allowed (true for calls from adapt_grid, init_db)
    INTEGER, INTENT(IN) :: n_var_comm  ! maximum number of variables to transmit
    
    DECLARE_NODE_POINTER :: c_pointer, c_pointer1, c_pointer2
    INTEGER :: proc, j, wlt_type, face_type, ii, &
         num, num_gs, ixyz(dim), i_p(0:dim),  &
         kk, size_node_to_recv, size_node_to_recv_gho, list, &
         num_gho, num_gho_which_are_sig, i
    
    
#ifdef MULTIPROC

    
    IF (.NOT.ALLOCATED(procbuffer_to_recv) ) THEN
       ALLOCATE (procbuffer_to_recv(j_mx,0:par_size-1))
       ALLOCATE (procbuffer_to_send(j_mx,0:par_size-1))
       ALLOCATE (procbuffer_to_recv_gho(j_mx,0:par_size-1))
       ALLOCATE (procbuffer_to_send_gho(j_mx,0:par_size-1))
    END IF
    procbuffer_to_recv = 0
    procbuffer_to_send = 0
    procbuffer_to_recv_gho = 0
    procbuffer_to_send_gho = 0

    
    ! set maximum numbers of nodes to request from other processors for all levels (both sig and gho)
    DO proc = 0, par_size-1
       IF (proc.NE.par_rank) THEN
          DO j = 1, j_mx
             DO wlt_type = MIN(j-1,1),2**dim - 1
                DO face_type = 0, 3**dim - 1
                   CALL DB_get_initial_type_level_node (proc,wlt_type,j,face_type,ii,c_pointer,list_sig)
                   procbuffer_to_recv(j,proc) = procbuffer_to_recv(j,proc) + ii
                END DO
             END DO
          END DO
       END IF
    END DO
    IF (do_ghosts) THEN
       DO proc = 0, par_size-1
          IF (proc.NE.par_rank) THEN
             DO j = 1, j_mx
                DO wlt_type = MIN(j-1,1),2**dim - 1
                   DO face_type = 0, 3**dim - 1
                      CALL DB_get_initial_type_level_node (proc,wlt_type,j,face_type,ii,c_pointer,list_gho)
                      procbuffer_to_recv_gho(j,proc) = procbuffer_to_recv_gho(j,proc) + ii
                   END DO
                END DO
             END DO
          END IF
       END DO
    END IF
    
    
    ! allocate space for the nodes to request from other processors
    ! (nodebuffer/valuebuffer have to be able to include ghosts which are sig on other proc
    ! and therefore to be added locally as sig)
    size_node_to_recv     = SUM( procbuffer_to_recv    (1:j_mx,0:par_size-1) )
    size_node_to_recv_gho = SUM( procbuffer_to_recv_gho(1:j_mx,0:par_size-1) )


    !WARNING: test
    IF(ALLOCATED(nodebuffer_to_recv)) DEALLOCATE (nodebuffer_to_recv)
    IF(ALLOCATED(valuebuffer_to_recv)) DEALLOCATE (valuebuffer_to_recv)
    IF(ALLOCATED(nodebuffer_to_recv_gho)) DEALLOCATE (nodebuffer_to_recv_gho)
    IF(ALLOCATED(boolbuffer_to_recv_gho)) DEALLOCATE (boolbuffer_to_recv_gho)



    IF ( ALLOCATED(nodebuffer_to_recv) .AND. SIZE(nodebuffer_to_recv).LT.size_node_to_recv+size_node_to_recv_gho ) THEN
       DEALLOCATE( nodebuffer_to_recv )
       DEALLOCATE( valuebuffer_to_recv )
    END IF
    IF ( .NOT.ALLOCATED(nodebuffer_to_recv) ) THEN
       ALLOCATE( nodebuffer_to_recv(size_node_to_recv+size_node_to_recv_gho) )
       ALLOCATE( valuebuffer_to_recv((size_node_to_recv+size_node_to_recv_gho)*n_var_comm) )
    END IF
    nodebuffer_to_recv = 0
    valuebuffer_to_recv = 0.0_pr

    IF ( ALLOCATED(nodebuffer_to_recv_gho) .AND. SIZE(nodebuffer_to_recv_gho).LT.size_node_to_recv_gho ) THEN
       DEALLOCATE( nodebuffer_to_recv_gho )
       DEALLOCATE( boolbuffer_to_recv_gho )
    END IF
    IF ( .NOT.ALLOCATED(nodebuffer_to_recv_gho) ) THEN
       ALLOCATE( nodebuffer_to_recv_gho(size_node_to_recv_gho) )
       ALLOCATE( boolbuffer_to_recv_gho(size_node_to_recv_gho) )
    END IF
    nodebuffer_to_recv_gho = 0
    boolbuffer_to_recv_gho = .FALSE.


    ! write 1D global index for the nodes to request from other processors
    i_p(0) = 1
    DO j=1,dim
       i_p(j) = i_p(j-1)*(mxyz(j)*2**(j_mx-1) + 1)
    END DO
    list = list_sig
    num = 0
    DO proc = 0, par_size-1
       IF (proc.NE.par_rank) THEN
          DO j = 1, j_mx
             DO wlt_type = MIN(j-1,1),2**dim - 1
                DO face_type = 0, 3**dim - 1
                   CALL DB_get_initial_type_level_node (proc,wlt_type,j,face_type,ii,c_pointer,list)
                   IF(ii > 0) THEN
                      DO WHILE (is_ok(c_pointer))

                         CALL DB_get_coordinates_by_pointer ( c_pointer, ixyz )
                         num = num + 1
                         nodebuffer_to_recv(num) = 1+SUM(ixyz(1:dim)*i_p(0:dim-1))
!!$                         PRINT *, 'sig node to recv',nodebuffer_to_recv(num),ixyz(1:dim)

#ifdef TREE_NODE_POINTER_STYLE_C
                         CALL DB_get_next_type_level_node (proc,wlt_type,j,face_type,c_pointer,c_pointer1,list)
                         c_pointer = c_pointer1  
#elif defined TREE_NODE_POINTER_STYLE_F
                         CALL DB_get_next_type_level_node (c_pointer)
#endif                    
                      END DO
                   END IF
                END DO
             END DO
          END DO
       END IF
    END DO
    IF (do_ghosts) THEN
       list = list_gho
       num = 0
       DO proc = 0, par_size-1
          IF (proc.NE.par_rank) THEN
             DO j = 1, j_mx
                DO wlt_type = MIN(j-1,1),2**dim - 1
                   DO face_type = 0, 3**dim - 1
                      CALL DB_get_initial_type_level_node (proc,wlt_type,j,face_type,ii,c_pointer,list)
                      IF(ii > 0) THEN
                         DO WHILE (is_ok(c_pointer))

                            CALL DB_get_coordinates_by_pointer ( c_pointer, ixyz )
                            num = num + 1
                            nodebuffer_to_recv_gho(num) = 1+SUM(ixyz(1:dim)*i_p(0:dim-1))
!!$                            PRINT *, 'gho node to recv',nodebuffer_to_recv_gho(num),ixyz(1:dim)

#ifdef TREE_NODE_POINTER_STYLE_C
                            CALL DB_get_next_type_level_node (proc,wlt_type,j,face_type,c_pointer,c_pointer1,list)
                            c_pointer = c_pointer1  
#elif defined TREE_NODE_POINTER_STYLE_F
                            CALL DB_get_next_type_level_node (c_pointer)
#endif                    
                         END DO
                      END IF
                   END DO
                END DO
             END DO
          END IF
       END DO
    END IF

    
    ! transfer 1D index for sig and gho nodes
    ! (nodebuffer/valuebuffer _to_send(_gho) as well
    ! as boolbuffer_to_send_gho are allocated inside;
    ! do_ghosts=.TRUE. during the first call only)
    CALL parallel_comm_mlfc (do_ghosts, n_var_comm)


    ! add ghosts which are sig on other processors to the local sig list
    ! (modify procbuffer_to_send/recv accordingly)
!!$    PRINT *, par_rank,'##comm_mlfc_gho (send): count/size', COUNT(boolbuffer_to_send_gho), SIZE(boolbuffer_to_send_gho)
!!$    PRINT *, par_rank,'##comm_mlfc_gho (recv): count/size', COUNT(boolbuffer_to_recv_gho), SIZE(boolbuffer_to_recv_gho)
!!$    PRINT *, par_rank,'do_ghosts=',do_ghosts,'COUNT(bool)=',COUNT(boolbuffer_to_recv_gho)

!!$    CALL count_DB( MAXLIST=list_gho, FILE=6, VERB=.TRUE.)
!!$    PAUSE 'after parallel_comm_mlfc'

!!$    num_gho_which_are_sig = COUNT(boolbuffer_to_recv_gho)
!!$    IF (.NOT.do_ghosts.OR.num_gho_which_are_sig.EQ.0) RETURN
    
    IF (.NOT.do_ghosts) THEN
       ! replace coordinates by c_pointers
       i_p(0) = 1
       DO i=1,dim
          i_p(i) = i_p(i-1)*(mxyz(i)*2**(j_mx-1) + 1)
       END DO
       
       ! set global n_transfer_send n_transfer_recv for _log file
       n_transfer_send = SUM( procbuffer_to_send(1:j_mx,0:par_size-1) )
       n_transfer_recv = SUM( procbuffer_to_recv(1:j_mx,0:par_size-1) )
       
       DO i=1,n_transfer_send
          ixyz(1:dim) = INT(MOD(nodebuffer_to_send(i)-1,i_p(1:dim))/i_p(0:dim-1))
          CALL DB_test_node( ixyz, j_mx, c_pointer )
          nodeptrbuffer_to_send(i) = c_pointer
       END DO
       ! it was the second call
       RETURN
    END IF
    
    
    IF (COUNT(boolbuffer_to_recv_gho).GT.0) THEN
       list = list_gho
       num = 0                      ! counter in ...buffer_to_recv_gho array
!!$       num_gs = 0                   ! number of newly added adj
       DO proc = 0, par_size-1
          IF (proc.NE.par_rank) THEN
             DO j = 1, j_mx
                DO wlt_type = MIN(j-1,1),2**dim - 1
                   DO face_type = 0, 3**dim - 1
                      CALL DB_get_initial_type_level_node (proc,wlt_type,j,face_type,ii,c_pointer,list)
                      IF(ii > 0) THEN
                         DO WHILE (is_ok(c_pointer))

                            num = num + 1
                            c_pointer2 = c_pointer
#ifdef TREE_NODE_POINTER_STYLE_C
                            CALL DB_get_next_type_level_node (proc,wlt_type,j,face_type,c_pointer,c_pointer1,list)
                            c_pointer = c_pointer1  
#elif defined TREE_NODE_POINTER_STYLE_F
                            CALL DB_get_next_type_level_node (c_pointer)
#endif                    
                            IF (boolbuffer_to_recv_gho(num)) THEN
                               ! add that ghost as adj locally (since it is sig on some other proc)
                               CALL DB_set_id_by_pointer (c_pointer2, idp(pos_adj)+idp(pos_gho))
!!$                               CALL DB_get_coordinates_by_pointer (c_pointer2, ixyz)
!!$                               PRINT *, par_rank,'set id to',idp(pos_adj)+idp(pos_gho),'at',ixyz
!!$                               num_gs = num_gs + 1
                            END IF

                         END DO
                      END IF
                   END DO
                END DO
             END DO
          END IF
       END DO
    END IF
    
    !PRINT *, par_rank,'num_gs=',num_gs,'of',num
    !CALL count_DB( MAXLIST=list_gho, FILE=6, VERB=.TRUE.)
    ! rearrange sig list for communications
    ! (in future, this has to be done without communications)
    CALL make_lists_for_communications( .FALSE., n_var_comm )

#else
    ! global n_transfer_send n_transfer_recv for _log file
    n_transfer_send = 0
    n_transfer_recv = 0
#endif


  ! to visualize single processor boundary zone to be communicated
  ! PRINT nodes to be transferred for the given processor
!#define EBUG_SINGLE_PROC_ZONE
#ifdef EBUG_SINGLE_PROC_ZONE
    i_p(0) = 1
    DO i=1,dim
       i_p(i) = i_p(i-1)*(mxyz(i)*2**(j_mx-1) + 1)
    END DO
    n_transfer_send = SUM( procbuffer_to_send(1:j_mx,0:par_size-1) )
    
    OPEN (UNIT=123,FILE='nodes'//TRIM(par_rank_str)//'.dat',FORM='unformatted')
    WRITE (123) i_p(0:dim), &                          ! <--> 1D written index
         n_transfer_send                               ! n_transfer_send
    WRITE (123) nodebuffer_to_recv(1:n_transfer_send)  ! 1D index of the nodes
    
!!$    DO i=1,n_transfer_send
!!$       ixyz(1:dim) = INT(MOD(nodebuffer_to_recv(i)-1,i_p(1:dim))/i_p(0:dim-1))
!!$       WRITE (123, *) ixyz, nodebuffer_to_recv(i)
!!$    END DO
    CLOSE (123)
#endif
    
  END SUBROUTINE make_lists_for_communications



#ifdef MULTIPROC
  !---------------------------------------------------------------------------------------------
  ! request given variables from other processors
  SUBROUTINE request_known_list ( var, var_size, list, maxlev )
    USE parallel                       ! procbuffer(1:par_size)
    USE wlt_vars                       ! j_mx
    USE db_tree_vars                   ! pointer
    INTEGER, INTENT(IN) :: var_size, &                ! .LE.NvecR
         list, maxlev                                 ! use that list up to that level
    LOGICAL, INTENT(IN) :: var(1:var_size)            ! which variables to transfer,
    
    DECLARE_NODE_POINTER :: c_pointer, c_pointer1, c_pointer2
    INTEGER :: proc, j, wlt_type, face_type, ii, num, &
         size_node, active_var, kk, &
         tpbr(0:par_size-1), startnum, id, anvar
    REAL(pr), POINTER :: valuebuffer_to_recv_ptr(:)
    
    
    ! numbers of nodes to request from other processors have been set in
    ! make_lists_for_communications as procbuffer_to_send/recv(lev,proc)
    ! and 1D index of that nodes have been stored in nodebuffer_to_send/recv(:)
    ! buffers for transfer values have been allocated as valuebuffer_to_send/recv(:)
    
    ! number of active variables
    anvar = COUNT( var(1:var_size) )

    
    ! transfer the data
    CALL parallel_comm_rkl ( var, var_size, maxlev )
    tpbr(0:par_size-1) = SUM( procbuffer_to_recv(1:j_mx,0:par_size-1), DIM=1 )*anvar !var_size
    valuebuffer_to_recv_ptr => valuebuffer_to_recv
    

    ! write the obtained values
    num = 0
    startnum = 1
    DO proc = 0, par_size-1
       IF (proc.NE.par_rank) THEN
          num = startnum
          
          DO j = 1, maxlev
             DO wlt_type = MIN(j-1,1),2**dim - 1
                DO face_type = 0, 3**dim - 1
                   CALL DB_get_initial_type_level_node (proc,wlt_type,j,face_type,ii,c_pointer,list)
                   IF(ii > 0) THEN
                      DO WHILE (is_ok(c_pointer))
                         
                         ! transfer real function values mode
                         DO kk=1,var_size
                            IF (var(kk)) THEN
                               !PRINT *, 'writen obtained value', num, valuebuffer_to_recv(num)
                               CALL DB_set_function_by_pointer( c_pointer, kk, kk, valuebuffer_to_recv_ptr(num) )
                               num = num + 1
                            END IF
                         END DO
                         
#ifdef TREE_NODE_POINTER_STYLE_C
                         CALL DB_get_next_type_level_node (proc,wlt_type,j,face_type,c_pointer,c_pointer1,list)
                         c_pointer = c_pointer1  
#elif defined TREE_NODE_POINTER_STYLE_F
                         CALL DB_get_next_type_level_node (c_pointer)
#endif
                            
                      END DO
                   END IF
                END DO
             END DO
          END DO
          
          startnum = startnum + tpbr(proc) !*var_size
       END IF
    END DO

    !WRITE (*, '("request_known_list at #",I3,":",I8)') par_rank, num

  END SUBROUTINE request_known_list
#endif


  !---------------------------------------------------------------------------------------------
  ! make list of the nodes in the boundary zone of the current processor
  ! and send them (coord+ID) to other processors;
  ! the other processors will add the nodes with the received ID's
  !
  ! (this subroutine is empty for sequential code)
  SUBROUTINE make_list_and_inform (j_mn_start, list, pos)
    USE parallel                          ! procbuffer(1:par_size)
    USE wlt_vars                          ! j_mx
    USE db_tree_vars                   ! pointer
    INTEGER, INTENT(IN) :: j_mn_start, &  ! count nodes starting with that level
         list,                         &  ! use nodes from that list
         pos                              ! ID of newly added nodes
#ifdef MULTIPROC
    DECLARE_NODE_POINTER :: c_pointer, c_pointer1
    INTEGER :: proc, j, wlt_type, face_type, ii, &
         i_p(0:dim), size_node, num, num_recv, &
         ixyz(dim), i
    INTEGER, ALLOCATABLE :: nodebuffer(:)             ! store 1D node index
    INTEGER, POINTER :: nodebuffer_recv(:)
    
    ! count the nodes to send to the other processors (j_mn_start-1 and lower levels has been added anyway)
    procbuffer(0:par_size-1) = 0
    DO proc = 0, par_size-1
       IF (proc.NE.par_rank) THEN
          DO j = j_mn_start, j_mx
             DO wlt_type = MIN(j-1,1),2**dim - 1
                DO face_type = 0, 3**dim - 1
                   CALL DB_get_initial_type_level_node (proc,wlt_type,j,face_type,ii,c_pointer,list)
                   IF(ii > 0) THEN
                      DO WHILE (is_ok(c_pointer))
                         procbuffer(proc) = procbuffer(proc) + 1
#ifdef TREE_NODE_POINTER_STYLE_C
                         CALL DB_get_next_type_level_node (proc,wlt_type,j,face_type,c_pointer,c_pointer1,list)
                         c_pointer = c_pointer1  
#elif defined TREE_NODE_POINTER_STYLE_F
                         CALL DB_get_next_type_level_node (c_pointer)
#endif                    
                      END DO
                   END IF
                END DO
             END DO
          END DO
       END IF
    END DO
    ! allocate space and write 1D global index for nodes to send
    size_node = SUM( procbuffer(0:par_size-1) )
    ALLOCATE( nodebuffer(size_node) )
    i_p(0) = 1
    DO j=1,dim
       i_p(j) = i_p(j-1)*(mxyz(j)*2**(j_mx-1) + 1)
    END DO
    num = 0
    DO proc = 0, par_size-1
       IF (proc.NE.par_rank) THEN
          DO j = j_mn_start, j_mx
             DO wlt_type = MIN(j-1,1),2**dim - 1
                DO face_type = 0, 3**dim - 1
                   CALL DB_get_initial_type_level_node (proc,wlt_type,j,face_type,ii,c_pointer,list)
                   IF(ii > 0) THEN
                      DO WHILE (is_ok(c_pointer))
                         num = num + 1
                         CALL DB_get_coordinates_by_pointer ( c_pointer, ixyz )
                         nodebuffer(num) = 1+SUM(ixyz(1:dim)*i_p(0:dim-1))
!!$                         PRINT *, 'ixyz=',ixyz,'index=',nodebuffer(num)
!!$                         PRINT *, 'back=', INT(MOD(nodebuffer(num)-1,i_p(1:dim))/i_p(0:dim-1))
#ifdef TREE_NODE_POINTER_STYLE_C
                         CALL DB_get_next_type_level_node (proc,wlt_type,j,face_type,c_pointer,c_pointer1,list)
                         c_pointer = c_pointer1  
#elif defined TREE_NODE_POINTER_STYLE_F
                         CALL DB_get_next_type_level_node (c_pointer)
#endif                    
                      END DO
                   END IF
                END DO
             END DO
          END DO
       END IF
    END DO
!!$    PRINT *, 'proc',par_rank,'size=',num,'procbuffer=',procbuffer
    ! transfer the data
    ! (nodebuffer_recv(:) will be allocated inside)
    CALL parallel_comm_adj (nodebuffer, procbuffer, num, nodebuffer_recv, num_recv)
    ! set ID for the obtained nodes
    DO i = 1, num_recv
       ixyz(1:dim) = INT(MOD(nodebuffer_recv(i)-1,i_p(1:dim))/i_p(0:dim-1))
!!$       PRINT *, 'make_list_and_inform: at', par_rank, ixyz(1:dim)
       CALL set_id( ixyz, j_mx, pos, beg_list )
    END DO
    ! clean the buffers
    IF (num_recv.NE.0) DEALLOCATE( nodebuffer_recv )
    DEALLOCATE( nodebuffer ) 
    
#endif    
  END SUBROUTINE make_list_and_inform

  !---------------------------------------------------------------------------------------------
  ! make list of the nodes and request given variables from other processors
  ! (this subroutine is empty for sequential code)
  SUBROUTINE make_list_and_request( var, var_size, list, maxlev, IDMODE, ACT )
    USE parallel                       ! procbuffer(1:par_size)
    USE wlt_vars                       ! j_mx
    USE db_tree_vars                   ! pointer
    INTEGER, INTENT(IN) :: var_size, &                ! n_var
         list, maxlev                                 ! use that list up to that level
    
    LOGICAL, INTENT(IN) :: var(1:var_size)            ! which variables to transfer
    LOGICAL, OPTIONAL, INTENT(IN) :: IDMODE, ACT      ! transfer IDs instead of real function values
#ifdef MULTIPROC
    DECLARE_NODE_POINTER :: c_pointer, c_pointer1, c_pointer2
    INTEGER, ALLOCATABLE :: nodebuffer(:), &          ! store 1D node index
         ivaluebuffer(:)                              ! integer values to transfer
    REAL(pr), ALLOCATABLE :: valuebuffer(:)           ! values to transfer
    INTEGER :: proc, j, wlt_type, face_type, ii, &
         num, ixyz(dim), i_p(0:dim),  &
         size_node, active_var, kk, &
         mode                                         ! transfer: 0-real, 1-ID
    INTEGER, PARAMETER :: ID_of_nonpresent_node = -1  ! mark non-present nodes with that ID in parallel_comm


    IF (PRESENT(ACT)) THEN
       !DEALLOCATE( nodebuffer, valuebuffer, ivaluebuffer )
       RETURN
    END IF

    mode = 0
    IF (PRESENT(IDMODE)) mode = 1
    
    ! set numbers of nodes to request from other processors
    procbuffer(0:par_size-1) = 0
    DO proc = 0, par_size-1
       IF (proc.NE.par_rank) THEN
          DO j = 1, maxlev
             DO wlt_type = MIN(j-1,1),2**dim - 1
                DO face_type = 0, 3**dim - 1
                   CALL DB_get_initial_type_level_node (proc,wlt_type,j,face_type,ii,c_pointer,list)
                   IF(ii > 0) THEN
                      DO WHILE (is_ok(c_pointer))
                         
                         procbuffer(proc) = procbuffer(proc) + 1

#ifdef TREE_NODE_POINTER_STYLE_C
                         CALL DB_get_next_type_level_node (proc,wlt_type,j,face_type,c_pointer,c_pointer1,list)
                         c_pointer = c_pointer1  
#elif defined TREE_NODE_POINTER_STYLE_F
                         CALL DB_get_next_type_level_node (c_pointer)
#endif                    
                      END DO
                   END IF
                END DO
             END DO
          END DO
       END IF
    END DO

 

    ! allocate space and write 1D global index for that nodes to request
    size_node = SUM( procbuffer(0:par_size-1) )
    active_var = COUNT( var(1:var_size) )
    ALLOCATE( nodebuffer(size_node) )
    IF (mode.EQ.0) THEN
       ! transfer real function values
       ALLOCATE( valuebuffer(active_var*size_node) )
       ALLOCATE( ivaluebuffer(1) )                     ! for compiler
    ELSE IF (mode.EQ.1) THEN
       ! transfer IDs
       ALLOCATE( ivaluebuffer(active_var*size_node) )
       ALLOCATE( valuebuffer(1) )                      ! for compiler
    END IF
    
    i_p(0) = 1
    DO j=1,dim
       i_p(j) = i_p(j-1)*(mxyz(j)*2**(j_mx-1) + 1)
    END DO
    num = 0
    DO proc = 0, par_size-1
       IF (proc.NE.par_rank) THEN
          DO j = 1, maxlev
             DO wlt_type = MIN(j-1,1),2**dim - 1
                DO face_type = 0, 3**dim - 1
                   CALL DB_get_initial_type_level_node (proc,wlt_type,j,face_type,ii,c_pointer,list)
                   IF(ii > 0) THEN
                      DO WHILE (is_ok(c_pointer))

                         CALL DB_get_coordinates_by_pointer ( c_pointer, ixyz )
                         num = num + 1
                         nodebuffer(num) = 1+SUM(ixyz(1:dim)*i_p(0:dim-1))
!!$                         PRINT *,'@@ ixyz=', ixyz
#ifdef TREE_NODE_POINTER_STYLE_C
                         CALL DB_get_next_type_level_node (proc,wlt_type,j,face_type,c_pointer,c_pointer1,list)
                         c_pointer = c_pointer1  
#elif defined TREE_NODE_POINTER_STYLE_F
                         CALL DB_get_next_type_level_node (c_pointer)
#endif                    
                      END DO
                   END IF
                END DO
             END DO
          END DO
       END IF
    END DO


    
!!$    PRINT *, '@@  var_size=',var_size
!!$    PRINT *, '@@  num=',num,', size_node=',size_node, ', active_var=',active_var
    ! transfer the data
    CALL parallel_comm(nodebuffer, valuebuffer, ivaluebuffer, var, size_node, active_var, var_size, mode)
    

    ! write the obtained values
    num = 0
    DO proc = 0, par_size-1
       IF (proc.NE.par_rank) THEN
          DO j = 1, maxlev
             DO wlt_type = MIN(j-1,1),2**dim - 1
                DO face_type = 0, 3**dim - 1
                   CALL DB_get_initial_type_level_node (proc,wlt_type,j,face_type,ii,c_pointer,list)
                   IF(ii > 0) THEN
                      DO WHILE (is_ok(c_pointer))
                         
                         IF (mode.EQ.0) THEN
                            ! transfer real function values mode
                            DO kk=1,var_size
                               IF (var(kk)) THEN
                                  num = num + 1
                                  CALL DB_set_function_by_pointer( c_pointer, kk, kk, valuebuffer(num) )
                               END IF
                            END DO
#ifdef TREE_NODE_POINTER_STYLE_C
                            CALL DB_get_next_type_level_node (proc,wlt_type,j,face_type,c_pointer,c_pointer1,list)
                            c_pointer = c_pointer1  
#elif defined TREE_NODE_POINTER_STYLE_F
                            CALL DB_get_next_type_level_node (c_pointer)
#endif                    
                         ELSE IF (mode.EQ.1) THEN
                            ! transfer IDs mode (ID_of_nonpresent_node corresponds to an absent node)
                            ! these nodes were marked as old in add_ghost_DB_paralle_antea(),
                            ! so we set they real IDs now
                            num = num + 1
                            ! if ID is changed to sig/adj, the node will be moved to sig/adj list,
                            ! so we will store pointer to the node, obtain the next one and
                            ! change ID of the stored one
                            c_pointer2 = c_pointer
#ifdef TREE_NODE_POINTER_STYLE_C
                            CALL DB_get_next_type_level_node (proc,wlt_type,j,face_type,c_pointer,c_pointer1,list)
                            c_pointer = c_pointer1  
#elif defined TREE_NODE_POINTER_STYLE_F
                            CALL DB_get_next_type_level_node (c_pointer)
#endif
                            ! add nodes which were present and
                            ! exclude non-present nodes with marked ID
                            IF (ivaluebuffer(num).NE.ID_of_nonpresent_node) THEN
                               CALL DB_set_id_by_pointer( c_pointer2, ivaluebuffer(num) )
                            ELSE
                               CALL DB_remove_from_list_by_pointer( c_pointer2 )
                            END IF
                            CALL DB_get_coordinates_by_pointer ( c_pointer2, ixyz )
!!$                            IF (par_rank.EQ.0) PRINT *,'@@@',ivaluebuffer(num),'ixyz=',ixyz
                         END IF
                         
                      END DO
                   END IF
                END DO
             END DO
          END DO
       END IF
    END DO
!!$    IF (par_rank.EQ.0) WRITE (*,*) 'proc',par_rank,'make_list_and_request: wrote values:',num

    ! clean the buffers
    DEALLOCATE( nodebuffer, valuebuffer, ivaluebuffer )
#endif
  END SUBROUTINE make_list_and_request
  
  ! this is subroutine for res2vis visualization
  ! it requests unknown number of boundary nodes from other processors
  ! (this subroutine returns zero for sequential code)
  SUBROUTINE request_boundary_nodes( add_coord, add_u, add_size, var_size, jmax, prd_old )
    USE precision
    USE parallel
    USE wlt_vars     ! dim, j_lev
    INTEGER, POINTER :: add_coord(:)   ! 1D coord of boundary nodes from other processors
    REAL(pr), POINTER :: add_u(:,:)    ! u value of the nodes (1:n_var,iwlt)
    INTEGER, INTENT(OUT) :: add_size   ! total number of the nodes
    INTEGER, INTENT(IN) :: var_size, & ! n_var
         jmax,                       & ! maximum level to consider
         prd_old(1:dim)                ! the periodicity of the original grid (to transfer external bourders)
#ifdef MULTIPROC
    INTEGER :: i, ii, dir_type, dm, &
         i_pt(0:dim), iveclocal(1:dim), &
         t_coord(1:dim), t2_coord(1:dim), t_max(1:dim), &
         number_of_trees_to_request, tn
    INTEGER, ALLOCATABLE :: tmp(:), &
         tree_to_recv(:), tree_to_recv_sorted(:)       ! trees to receive (keep face direction)
    INTEGER :: prbuff(0:par_size-1), &                 ! number of trees to receive from each processor
         startbuff(0:par_size-1), cbuff(0:par_size-1)  ! tmp storage for sorting
    
    ! nothing to do in serial mode
    IF (par_size.EQ.1) THEN
       add_size = 0
       RETURN
    END IF
    
    i_pt(0) = 1 ! 1D tree coordinate transform
    DO i=1,dim
       i_pt(i) = i_pt(i-1)*( mxyz(i)*2**(j_tree_root-1) + 1 - prd(i) )
       t_max(i) = mxyz(i)*2**(j_tree_root-1) - prd(i)
       iveclocal(i) = i-1
    END DO
    !!PRINT *, 'i_pt=',i_pt,'iveclocal=',iveclocal
    
    ! pass through the processor trees and mark ones to receive
    ALLOCATE( tmp(1:SIZE(par_proc_tree)) )
    tmp = 0
    DO i=1,SIZE(par_proc_tree)
       IF (par_proc_tree(i).EQ.par_rank) THEN
          t_coord(1:dim) = INT(MOD(i-1,i_pt(1:dim))/i_pt(0:dim-1))          
          DO dir_type=1,dim**2-1
             t2_coord(1:dim) = t_coord(1:dim) + IBITS(dir_type,iveclocal(:),1)
             IF (ALL(t2_coord(1:dim).LE.t_max(1:dim))) THEN
                ii = 1+SUM(t2_coord(1:dim)*i_pt(0:dim-1))
                IF (par_proc_tree(ii).NE.par_rank) THEN
                   tmp(ii) = tmp(ii) + 1
                END IF
             END IF
          END DO
       END IF
    END DO
    !!PRINT *,'tmp=',tmp, 'SUM(tmp)=', SUM(tmp)
    
    ! SUM (tmp) gives maximum possible number of trees to receive
    ALLOCATE( tree_to_recv(1:2*SUM(tmp)) )
    tree_to_recv = 0                           ! array of pairs (tree number, direction)
    tn = 1                                     ! index in tree_to_recv array
    DO i=1,SIZE(par_proc_tree)
       IF (par_proc_tree(i).EQ.par_rank) THEN
          t_coord(1:dim) = INT(MOD(i-1,i_pt(1:dim))/i_pt(0:dim-1))          
          DO dir_type=1,dim**2-1
             t2_coord(1:dim) = t_coord(1:dim) + IBITS(dir_type,iveclocal(:),1)
             IF (ALL(t2_coord(1:dim).LE.t_max(1:dim))) THEN
                ii = 1+SUM(t2_coord(1:dim)*i_pt(0:dim-1))
                IF (par_proc_tree(ii).NE.par_rank) THEN
                   ! this node has been already marked tmp(ii) times
!!$                   IF (par_rank.EQ.0) PRINT *, 't_coord=',t_coord, 't2_coord=',t2_coord,ii
                   
                   ! mark that tree as one to receive
                   !IF (dir_type.EQ.dim**2-1)
                   tree_to_recv(tn) = ii
                   tree_to_recv(tn+1) = dir_type
!!$                   PRINT *, 'tree_to_recv/dir=',tree_to_recv(tn),tree_to_recv(tn+1)
                   tn = tn + 2
                END IF
             END IF
          END DO
       END IF
    END DO
    !!PRINT *,'tree_to_recv=',tree_to_recv,'tn=',tn

    ! --- WARNING ---
    ! we may want to sort tree_to_recv() directions
    ! and eliminate some redundancy, e.g 3 \in (1 \cap 2) for 2D
    ! --- WARNING ---

    
    ! count the trees to receive from other processors
    ! and stack them close to the beginning of the array
    number_of_trees_to_request = 0
    prbuff(0:par_size-1) = 0
    ii = 0
    DO i=1,tn-1,2
       IF (tree_to_recv(i).GT.0) THEN
          prbuff(par_proc_tree(tree_to_recv(i))) = prbuff(par_proc_tree(tree_to_recv(i))) + 1
          number_of_trees_to_request = number_of_trees_to_request + 1
          tree_to_recv(2*number_of_trees_to_request-1) = tree_to_recv(i)
          tree_to_recv(2*number_of_trees_to_request  ) = tree_to_recv(i+1)
       END IF
    END DO
    
    ! sort trees by their processor number
    ALLOCATE( tree_to_recv_sorted(tn-1) )
    startbuff(0) = 1
    DO i=1,par_size-1
       startbuff(i) = startbuff(i-1) + prbuff(i-1)
    END DO
    cbuff = 0
    DO i=1,tn-1,2
       ii = startbuff (par_proc_tree (tree_to_recv(i))) + cbuff (par_proc_tree (tree_to_recv(i)))
       cbuff (par_proc_tree (tree_to_recv(i))) = cbuff (par_proc_tree (tree_to_recv(i))) + 1
       tree_to_recv_sorted(2*ii-1) = tree_to_recv(i)
       tree_to_recv_sorted(2*ii)   = tree_to_recv(i+1)
    END DO
!!$    PRINT *,'tree_to_recv_sorted=',tree_to_recv_sorted, &
!!$         'number_of_trees_to_request=',number_of_trees_to_request, &
!!$         'prbuff=',prbuff
    
    
    ! transfer the data
    CALL parallel_comm_bnd( add_coord, add_u, add_size,      & ! to be allocated
         tree_to_recv_sorted,                                & ! trees to request (tree number,direction)
         number_of_trees_to_request,                         & ! number of pairs
         prbuff,                                             & ! per processor number of trees to request
         var_size )                                            ! n_var

    DEALLOCATE( tree_to_recv_sorted, tree_to_recv, tmp )
#else    
    add_size = 0
#endif
  END SUBROUTINE request_boundary_nodes
END MODULE db_tree



!-------------------------------------------------------!
! define whether to use C or Fortran style node pointer !
#ifdef TREE_NODE_POINTER_STYLE_C
#undef TREE_NODE_POINTER_STYLE_C
#endif
#ifdef TREE_NODE_POINTER_STYLE_F
#undef TREE_NODE_POINTER_STYLE_F
#endif
#ifdef DECLARE_NODE_POINTER
#undef DECLARE_NODE_POINTER
#endif
! define whether to use C or Fortran style node pointer !
!-------------------------------------------------------!
