!
! SGS Anisotropic Minimum Dissipation model 
!

MODULE SGS_compressible_AMD

  USE precision
  USE wlt_vars
  USE wavelet_filters_mod
  USE wlt_trns_mod
  USE wlt_trns_vars
  USE wlt_trns_util_mod
  USE io_3d_vars
  USE util_mod
  USE util_vars
  USE share_consts
  USE pde
  USE sizes
  USE share_kry
  USE vector_util_mod
  USE input_file_reader
  USE equations_compressible 
  USE sgs_util 
  USE error_handling

  PUBLIC :: &
    AMD_compress_read_input, &
    calc_AMD_eddy_visc

  ! AMD model coefficients 
  REAL (pr), PRIVATE :: &
    C_AMD_in        = 0.0_pr, &
    Pra_t_in        = 0.0_pr, &
    Sc_t_in         = 0.0_pr

CONTAINS

  !
  ! Read input from "case_name"_pde.inp file
  ! case_name is in string file_name read from command line
  ! in read_command_line_input()
  ! 
  SUBROUTINE AMD_compress_read_input()
    IMPLICIT NONE

      call input_real ('C_AMD_in',C_AMD_in,'stop', &
        ' C_AMD_in ! AMD_Eddy viscosity coefficient')
      call input_real ('Pra_t_in',Pra_t_in,'stop', &
        ' Pra_t_in ! Turbulent Prandtl number (for set coefficient)')
      call input_real ('Sc_t_in',Sc_t_in,'stop', &
        ' Sc_t_in ! Turbulent Schmidt number (for set coefficient)')

  END SUBROUTINE AMD_compress_read_input


  ! Constant Ceofficient Smagorinsky 
  SUBROUTINE calc_AMD_eddy_visc( mu_t_out, kk_t_out, bd_t_out, u_in ) 
    USE equations_compressible 
    USE variable_mapping 
    USE equations_compressible_vars
    IMPLICIT NONE
    REAL (pr), DIMENSION (nwlt,n_integrated), INTENT (IN)    :: u_in
    REAL (pr), DIMENSION(nwlt),               INTENT (OUT)   :: mu_t_out, kk_t_out, bd_t_out 

    REAL(pr), DIMENSION(nwlt,dim) :: v 
    REAL(pr), DIMENSION(dim,nwlt,dim) :: dv_comp, dv_phys, S_ij_nonsymm 
    REAL (pr), DIMENSION(dim,nwlt) :: h_arr
    REAL (pr), DIMENSION(nwlt,dim*(dim+1)/2) :: s_ij
    REAL (pr), DIMENSION(nwlt)               :: smod
    REAL (pr), DIMENSION(nwlt)               :: cp_field
    INTEGER :: i,j,l

    REAL (pr), DIMENSION(nwlt)               :: num, denom 

    REAL (pr), PARAMETER :: conditioning_factor = 1.0e-6_pr

    CALL get_all_local_h (h_arr)


    ! Calculate modulus of strain rate
    DO i = 1,dim
       v(1:nwlt,i) = u_in(1:nwlt,n_mom(i)) / u_in(1:nwlt,n_den)
    END DO
    ! Calculate derivatives of velocity on the computational grid
    ! ERIC: control over the order should absolutely be reserved to the user (or otherwise controlled)
    CALL c_diff_fast(v(:,1:dim), dv_comp(1:dim,:,:), S_ij_nonsymm(1:dim,:,:), j_lev, nwlt, HIGH_ORDER, 10, dim, 1, dim, FORCE_RECTILINEAR=.TRUE.)
    CALL c_diff_fast(v(:,1:dim), dv_phys(1:dim,:,:), S_ij_nonsymm(1:dim,:,:), j_lev, nwlt, HIGH_ORDER, 10, dim, 1, dim, FORCE_RECTILINEAR=.FALSE.)
    ! ERIC: should this be traceless?
    CALL Sij( v, nwlt, s_ij, smod, .FALSE.) 
    ! map from symmetric tensor to i,j indices
    S_ij_nonsymm(1,:,1) = s_ij(:,1)
    S_ij_nonsymm(1,:,2) = s_ij(:,4)
    S_ij_nonsymm(1,:,3) = s_ij(:,5)
    S_ij_nonsymm(2,:,1) = s_ij(:,4)
    S_ij_nonsymm(2,:,2) = s_ij(:,2)
    S_ij_nonsymm(2,:,3) = s_ij(:,6)
    S_ij_nonsymm(3,:,1) = s_ij(:,5)
    S_ij_nonsymm(3,:,2) = s_ij(:,6)
    S_ij_nonsymm(3,:,3) = s_ij(:,3)

    v(:,:) = 0.0_pr
    DO i = 1,dim
      DO j = 1,dim
        v(:,i) = v(:,i) + h_arr(j,:) * dv_comp(i,:,j)  ! Calculate intermediate term 
      END DO
    END DO

    num   = 0.0_pr
    denom = 0.0_pr
    DO i = 1,dim
      DO j = 1,dim
        denom = denom + dv_phys(i,:,j) * dv_phys(i,:,j) 
        num = num + MAX( -v(:,i) * v(:,j) * S_ij_nonsymm(i,:,j), 0.0_pr)
      END DO
    END DO

    ! Calculate the eddy viscosity
    mu_t_out = C_AMD_in * u_in(:,n_den) * num / ( denom + conditioning_factor ) ! Denom will always be positive


    ! Calculation of conductivity. Could use an averaging function 
    cp_field(:) = cp_in(Nspec) !cp_Nspec 
    IF (Nspec>1) cp_field(:) = (1.0_pr-SUM(u_in(:,n_spc(1):n_spc(Nspecm)),DIM=2)/u_in(:,n_den))*cp_in(Nspec) !cp_Nspec
    DO l=1,Nspecm
       cp_field(:) = cp_field(:) + u_in(:,n_spc(l))/u_in(:,n_den)*cp_in(l) !cp
    END DO

    kk_t_out(:) = cp_field(:) * mu_t_out(:) / Pra_t_in
    IF(Nspec>1) bD_t_out(:) = mu_t_out(:) / Sc_t_in


  END SUBROUTINE calc_AMD_eddy_visc

END MODULE SGS_compressible_AMD

