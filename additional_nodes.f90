! This module is designed to include additional variables 
MODULE additional_nodes
  USE precision
  USE debug_vars
  USE wlt_vars
  USE parallel
  PUBLIC
  INTEGER :: j_additional_nodes, n_additional_nodes
  INTEGER, DIMENSION(:,:), ALLOCATABLE :: ixyz_additional_nodes
  LOGICAL :: additional_nodes_active, additional_nodes_initialized
  INTEGER :: n_additional_patches
  REAL (pr), DIMENSION(:,:), ALLOCATABLE :: x_additional_vec  
  INTEGER, DIMENSION(:,:), ALLOCATABLE :: n_patch  !to allow for 3D patches
  INTEGER, DIMENSION(:,:), ALLOCATABLE:: i_patch
  
  PUBLIC :: initialize_additional_nodes, &
       add_additional_nodes,             &
       free_additional_nodes
  
CONTAINS

  !
  ! Add additional grid points (nodes) specified by user 
  !
  ! additional_points(n_nodes, ixyz_nodes)
  SUBROUTINE initialize_additional_nodes
    IMPLICIT NONE
    INTEGER :: i, ip

    additional_nodes_initialized = .FALSE.
    !----------------- points hardwired here, in future release will be input parameters
    ! 2D case only
    IF( additional_nodes_active ) THEN
       IF (ALLOCATED(n_patch)) DEALLOCATE (n_patch)
       IF (ALLOCATED(i_patch)) DEALLOCATE (i_patch)
       ALLOCATE(n_patch(n_additional_patches,dim-1))
       IF (dim ==2)ALLOCATE(i_patch(1:dim,n_additional_patches)) !equal number of patches and nodes
       IF (dim ==3)ALLOCATE(i_patch(1:dim,8))  !8 nodes used for the six patches

!!$       x_additional_vec(:,1) = (/-1.,2.,2.,1.,1.,-1./) 
!!$       x_additional_vec(:,2) = (/-2.,-2.,0.,0.,1.,1./) 
       !-------------- sanity check
       IF(dim ==2) THEN
          DO i = 1, n_additional_patches
             ip = MOD(i,n_additional_patches) +1
             IF(dim == 2 .AND.  COUNT(x_additional_vec(ip,1:dim) == x_additional_vec(i,1:dim)) /= 1 ) THEN
                PRINT *, 'ERROR: patch is not aligned with principal axes'
                STOP
             END IF
          END DO
       END IF

       !check for proper inputs on 3D
       IF(dim == 3) THEN
          IF(n_additional_patches .NE. 6)THEN
             PRINT *, 'ERROR: 3D requires a cube of 6 patches'
             STOP
          END IF
          DO i = 1, 4
             ip = MOD(i,4) +1
             IF( COUNT(x_additional_vec(ip,1:2) == x_additional_vec(i,1:2)) /= 1 ) THEN
                PRINT *, 'ERROR: patch is not aligned with principal axes '
                STOP
             ELSE IF( (x_additional_vec(i,3) /= -1.0_pr*x_additional_vec(i+4,3)) .OR. (x_additional_vec(i,3) /= x_additional_vec(ip,3))  ) THEN
                PRINT *, 'ERROR: patch inputs are incorrect for 3D: must proceed ccw about z-faces'
                STOP
             ELSE IF(x_additional_vec(i,2) /= x_additional_vec(i+4,2) ) THEN
                PRINT *, 'ERROR: patch inputs are incorrect for 3D: must originate at same point for each z-face'
                STOP
             ELSE IF(x_additional_vec(i,1) /= x_additional_vec(i+4,1) ) THEN
                PRINT *, 'ERROR: patch inputs are incorrect for 3D: must originate at same point for each z-face'
                STOP
             END IF

          END DO
       END IF
 
       additional_nodes_initialized = .TRUE.
       PRINT *, 'Additional Nodes INITIALIZED'
    END IF

  END SUBROUTINE initialize_additional_nodes
    
  SUBROUTINE add_additional_nodes ( j_out )
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: j_out
    INTEGER :: i, ip, k, m, l, idim, step, nxyz_out(1:dim)
    INTEGER :: ONE(1:dim)
    INTEGER :: imaxmin(dim,2)
    

    !PRINT *,'add_additional_nodes: j_out=',j_out

    step = MAX(1,2**(j_out-j_additional_nodes))

    IF( (.NOT. additional_nodes_initialized) .AND. j_out > j_additional_nodes  ) THEN
       additional_nodes_initialized = .TRUE. 
    END IF
 
    IF( additional_nodes_initialized ) THEN
       !you create points on the nearest j_out level
       !calculate new n_additional_nodes
       IF (dim ==2) THEN !1D patches
          ONE = 1
          nxyz_out(1:dim) = mxyz(1:dim) * 2**(j_out -1)
          DO i = 1, n_additional_patches
             DO idim = 1, dim
                i_patch(idim,i) = (MINLOC( ABS(xx(0:nxyz_out(idim):step,idim) - x_additional_vec(i,idim)), DIM = 1  ) - 1 ) !align on nearest j_out level, indices
!!$             PRINT *,  i_patch(idim,i), size(xx(:,1)), size(xx(:,2)), nxyz_out
             END DO
          END DO
          DO i = 1, n_additional_patches
             ip = MOD(i,n_additional_patches)  +1
             !finds locaton of nodes on the mesh closest to pathc nodes
             DO idim = 1,dim !this finds direction which patch is parallel to 
                IF(x_additional_vec(ip,idim) /= x_additional_vec(i,idim) ) EXIT !direction  axes || to patch
             END DO
             !finds the length of the patch on MIN(j_out,j_additional_nodes)
             n_patch(i,1) = ABS( i_patch(idim,ip) - i_patch(idim,i) ) 
          END DO
          n_additional_nodes = SUM(N_patch(1:n_additional_patches,1))
          !PRINT *, 'Number of additional nodes:',n_additional_nodes
          IF(ALLOCATED(ixyz_additional_nodes)) DEALLOCATE(ixyz_additional_nodes)
          ALLOCATE(ixyz_additional_nodes(1:dim,n_additional_nodes))
          
          
          l = 0
          DO i = 1, n_additional_patches
             ip = MOD(i,n_additional_patches)+1
             DO k = 1, n_patch(i,1)
                l = l+1
                ! cooridiantes on level MIN(j_additional_nodes, j_out) 
                ixyz_additional_nodes(1:dim,l) = i_patch(1:dim,i) +   &
                     REAL(k-1,pr) * SIGN(ONE,i_patch(1:dim,ip) - i_patch(1:dim,i) ) &
                     * MIN(ONE,ABS(i_patch(1:dim,ip) - i_patch(1:dim,i)) ) 
             END DO
          END DO
       END IF
       
!!$       PRINT *, 'patches:',n_additional_patches, n_patch
!!$       PRINT *, 'additional_nodes:',l
       !    PRINT *, ixyz_additional_nodes(1:dim,1:n_additional_nodes)
       !PAUSE
       !************************************ 2D patches ***************************
       IF (dim ==3)THEN
          nxyz_out(1:dim) = mxyz(1:dim) * 2**(j_out -1)
          DO i = 1, 8 !go through all nodes
             DO idim = 1, dim
                i_patch(idim,i) = (MINLOC( ABS(xx(0:nxyz_out(idim):step,idim) - x_additional_vec(i,idim)), DIM = 1  ) - 1 )  !locate index of nearest pts to use
!!$             PRINT *,  i_patch(idim,i), size(xx(:,1)), size(xx(:,2)), nxyz_out
             END DO
          END DO

          !find dimensions of each patch
          DO idim = 1,dim !find max and min indexes (planes)
             imaxmin(idim,1) = MINVAL(i_patch(idim,:))
             imaxmin(idim,2) = MAXVAL(i_patch(idim,:))
          END DO

          DO i = 1,n_additional_patches !find dimensions of each patch
          !   IF( MOD(i,2) .EQ. 1 ) plane = MIN(x_additional_vec(:,(i+1)/2 ) !even is minimum plane
          !   IF( MOD(i,2) .EQ. 0 ) plane = MAX(x_additional_vec(:,(i+1)/2 ) !odd is max
             idim = (i+1)/2 !patch normal

             n_patch(i,1) = ABS(imaxmin(MOD(idim,dim)+1,2) - imaxmin(MOD(idim,dim)+1,1)) + 1
             n_patch(i,2) = ABS(imaxmin(MOD(idim+1,dim)+1,2) - imaxmin(MOD(idim+1,dim)+1,1)) + 1 

             !n_patch(i,1) = ABS( MAXVAL( i_patch(MOD(idim,dim)+1,:) ) - MINVAL(i_patch(MOD(idim,dim)+1,:)) ) + 1  !overlap at the edges of the patches
             !n_patch(i,2) = ABS( MAXVAL(i_patch(MOD(idim+1,dim)+1,:)) - MINVAL(i_patch(MOD(idim+1,dim)+1,:)) ) + 1
          END DO

          !Find total additional nodes
          n_additional_nodes = 0 
          DO i = 1,n_additional_patches  
             n_additional_nodes = n_patch(i,1) * n_patch(i,2) + n_additional_nodes
          END DO

          IF(ALLOCATED(ixyz_additional_nodes)) DEALLOCATE(ixyz_additional_nodes)
          ALLOCATE(ixyz_additional_nodes(1:dim,n_additional_nodes))
          
          
          l = 0  !Generate coordinates of nodes to add
          DO i = 1, n_additional_patches
             DO k = 1, n_patch(i,2) !add appropriate numbers of points for the dimension of the patch
                DO m = 1, n_patch(i,1) !add appropriate numbers of points for the dimension of the patch
                   l = l+1 !increment to next point

                   ! cooridiantes on level MIN(j_additional_nodes, j_out) 
                   idim = (i+1)/2 !patch normal
                   ixyz_additional_nodes(idim              ,l) = MOD(i+1,2)*imaxmin(idim,2) + MOD(i,2)*imaxmin(idim,1)
                   ixyz_additional_nodes(MOD(idim,dim)+1   ,l) = imaxmin(MOD(idim,dim)+1,1)  &
                        + REAL(m-1,pr) & 
                        * MIN(1,n_patch(i,1))
                   ixyz_additional_nodes(MOD(idim+1,dim)+1,l) = imaxmin(MOD(idim+1,dim)+1,1) &
                        + REAL(k-1,pr) &
                        * MIN(1,n_patch(i,2))
                END DO
             END DO
          END DO
       END IF
       ixyz_additional_nodes(1:dim,1:n_additional_nodes) = step * ixyz_additional_nodes(1:dim,1:n_additional_nodes)
       !PRINT *, "additional nodes sample", ixyz_additional_nodes(1:dim,1:3)
       
    
       !*******************************************************************************
    END IF
  END SUBROUTINE add_additional_nodes



  ! release memory used in the module
  SUBROUTINE free_additional_nodes
    IMPLICIT NONE
    
    IF (ALLOCATED(n_patch)) DEALLOCATE(n_patch)                                ! initialize_additional_nodes
    IF (ALLOCATED(i_patch)) DEALLOCATE(i_patch)                                ! initialize_additional_nodes
    IF (ALLOCATED(ixyz_additional_nodes)) DEALLOCATE(ixyz_additional_nodes)    ! add_additional_nodes
    IF (ALLOCATED(x_additional_vec)) DEALLOCATE(x_additional_vec)              ! read_input
    
  END SUBROUTINE free_additional_nodes


END MODULE additional_nodes
