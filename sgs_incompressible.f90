MODULE SGS_incompressible
  !
  ! SGS models for turbulence
  ! Boulder, 16 November 2015
  !
  USE precision
  USE input_file_reader
  USE io_3D_vars
  USE pde
  USE variable_mapping 
  USE share_consts
  USE share_kry
  USE util_mod
  USE vector_util_mod
  USE wavelet_filters_vars
  USE wavelet_filters_mod
  USE spectra_module
  USE SGS_util

  INTEGER, PROTECTED :: n_var_Ilm       ! location of Ilm  in u array
  INTEGER, PROTECTED :: n_var_Imm       ! location of Imm  in u array
  INTEGER, PROTECTED :: n_var_KD        ! location of Kdiss/Komeg  in u array (2015)
  

  INTEGER, PROTECTED :: SGS_TimeIntegrationType, N_SGS_TimeIntegration
  INTEGER   :: SGS_Homogeneity                   ! 0 - Incompressible Homogeneous Isotropic Turbulence,   1 - Incompressible External Flow 
  INTEGER   :: Prod_index                        ! 0 - SS (unaltered), 1 - SW (Kato-Launder) , 2 - WW (Menter)
  INTEGER   :: mdl_form
  INTEGER   :: Ceps_model                        ! 0 - fixed coefficient, 1 - dynamic Bardina-like scaling, 2 - dynamic Germano-like scaling
  REAL (pr) :: Cs                                ! Smagorinsky model coefficient
  REAL (PR) :: sgsmodel_coef1                    ! first model coefficient for model set by sgsmodel (see above)
  REAL (pr) :: LDMtheta                          ! time constant for Langrangian model
  REAL (pr) :: CI                                ! CI, langrangian model diffusion scale coefficient
  REAL (pr) :: C_eps_sgs, C_nu_art_k, delta_L2weight
  REAL (pr) :: alpha_k                           ! portion of the SGS kinetic energy
  LOGICAL   :: adaptIlm, adaptImm, SpaceTimeAvg, adaptKD, lowRe_flag
  LOGICAL   :: print_SGS_stats                   ! if true SGS stats will be printed
  REAL (pr), DIMENSION (:) , ALLOCATABLE :: nuI, one_T
  REAL (pr), DIMENSION (:) , ALLOCATABLE :: nuK, kprod_coeff, eps_sgs
  REAL (pr), DIMENSION (:) , ALLOCATABLE :: ASTAR, BSTAR, CSTAR            
  REAL (pr), DIMENSION (:) , ALLOCATABLE :: nutK ! as nu_t for turbulent diffusion in Ksgs equation (by Giuliano Oct 2011)

!------------------ GDM  --------------------------------------------------------
  REAL (pr) :: total_neg_sgs_diss, backscatter_frac 
  REAL (pr) :: total_resolved_diss, total_sgs_diss
  REAL (pr) :: total_residual_diss, total_Leonard_diss     ! by Giuliano
  LOGICAL   :: clip_nu_nut                                 ! if true clip nu + nu_t  in the sgs modeling
  LOGICAL   :: clip_Cs                                     ! if true clip  in the sgs modeling
  LOGICAL   :: Lijtraceless,Mijtraceless                   ! make Lij and Mij traceless in Dyn model
  LOGICAL   :: DynSmodGridFilt                             !if true |S| is based on grid filt in nu_t=Cdyn|S|
  LOGICAL   :: DynSmodGridFiltS                            !if true S is based on grid filt in tauij = -2 nu_t Sij
  LOGICAL   :: DynMdlFiltLijLast                           ! if true Lij=(ugrid ugrid - u_test u_test )_test
                                                           ! else  Lij=(ugrid ugrid)_test - u_test u_test 

!---------------------------------   Ksgs based models  -----------------------------------------
  REAL (pr) :: total_Ksgs, Ksgs_init, total_Kdiss, Kdiss_init, Ksgs_min, Kdiss_min, delta_n
  LOGICAL   :: uniform_C_nu                                ! if true C_nu uniform in LDKM modeling (Giuliano)

!---------------------------------   URANS (KEPS & KOME model )   ------------------------------------------
  REAL (pr) :: C0_RANS, C1_RANS, C2_RANS, C3_RANS, C4_RANS, C5_RANS     ! constant parameters for KEPS/KOME model

!-----------------------------------------------------------------------------------------------------------

! Adaptive eps
  LOGICAL :: do_const_diss   ! implement constant SGS dissipation
  LOGICAL :: do_const_k      ! implement constant k_SGS
  LOGICAL :: model_rescale   ! if .TRUE. - rescale model
  REAL(pr):: Psgs_diss       ! Percent SGS dissipation
  REAL(pr):: Psgs_diss_goal  ! Goal for percent SGS dissipation read from input file
  REAL(PR):: q_eps           ! temporal filtering factor for eps change
  REAL(PR):: eps_min,eps_max !Allowable range for eps to change 
  REAL(PR):: sgs_gain        ! gain for modifying nu_t
  REAL(PR):: Ilm_gain        ! gain for modifying Ilm
  REAL(PR):: max_eps_scale   ! max change in (eps/eps_old)**2 -1.0

CONTAINS

  !
  ! In SGS_setup() we setup all the varaibles necessary for SGS 
  !
  ! The following variables must be setup in this routine:
  !
  !
  ! n_var_SGS        ! 
  !
  !
  !
  !
  SUBROUTINE  SGS_setup ()
    USE parallel
    IMPLICIT NONE
    
    INTEGER :: i
    
    !register_var( var_name, integrated, adapt, interpolate, exact, saved, req_restart, FRONT_LOAD )
    IF( sgsmodel == sgsmodel_LDM ) THEN
      n_var_SGS = 2 ! two scalars Ilm, Imm
      CALL register_var( 'Ilm            ', .TRUE., adapt=(/.FALSE.,.TRUE./), interpolate=(/.TRUE.,.TRUE./), exact=(/.FALSE.,.FALSE./), saved=.TRUE., req_restart=.TRUE. )
      CALL register_var( 'Imm            ', .TRUE., adapt=(/.FALSE.,.TRUE./), interpolate=(/.TRUE.,.TRUE./), exact=(/.FALSE.,.FALSE./), saved=.TRUE., req_restart=.TRUE. )
    ELSE IF( sgsmodel_DSM <= sgsmodel  .AND. sgsmodel <= sgsmodel_LDKMG ) THEN
      n_var_SGS = 1 ! one scalar Ksgs
      CALL register_var( 'Ksgs           ', .TRUE., adapt=(/.FALSE.,adaptK/), interpolate=(/.TRUE.,.TRUE./), exact=(/.FALSE.,.FALSE./), saved=.TRUE., req_restart=.TRUE. )
    ELSE IF( sgsmodel == sgsmodel_KEPS ) THEN
      n_var_SGS = 2 ! two scalars Ksgs, Kdiss
      CALL register_var( 'Ksgs           ', .TRUE., adapt=(/.FALSE.,adaptK/), interpolate=(/.TRUE.,.TRUE./), exact=(/.FALSE.,.FALSE./), saved=.TRUE., req_restart=.TRUE. )
      CALL register_var( 'Kdiss          ', .TRUE., adapt=(/.FALSE.,adaptKD/), interpolate=(/.TRUE.,.TRUE./), exact=(/.FALSE.,.FALSE./), saved=.TRUE., req_restart=.TRUE. )
    ELSE IF( sgsmodel == sgsmodel_KOME .OR. sgsmodel == sgsmodel_KOMET ) THEN
      n_var_SGS = 2 ! two scalars Ksgs, Komeg
      CALL register_var( 'Ksgs           ', .TRUE., adapt=(/.FALSE.,adaptK/), interpolate=(/.TRUE.,.TRUE./), exact=(/.FALSE.,.FALSE./), saved=.TRUE., req_restart=.TRUE. )
      CALL register_var( 'Komeg          ', .TRUE., adapt=(/.FALSE.,adaptKD/), interpolate=(/.TRUE.,.TRUE./), exact=(/.FALSE.,.FALSE./), saved=.TRUE., req_restart=.TRUE. )
    ELSE
          n_var_SGS = 0
    END IF    

    IF( do_Adp_Eps_Spatial .OR. do_Adp_Eps_Spatial_Evol ) THEN   !!!! TODO: req_restart?
      CALL register_var( 'SGSD           ', .FALSE., adapt=(/.FALSE.,.FALSE./), interpolate=(/.TRUE.,.TRUE./), exact=(/.FALSE.,.FALSE./), saved=saveSGSD, req_restart=.FALSE. )
      CALL register_var( 'RD             ', .FALSE., adapt=(/.FALSE.,.FALSE./), interpolate=(/.TRUE.,.TRUE./), exact=(/.FALSE.,.FALSE./), saved=saveSGSD, req_restart=.FALSE. )
    ENDIF
    
    
  END SUBROUTINE SGS_setup

  ! get the indices alloted to sgs variables
  SUBROUTINE  SGS_finalize_setup ()
    USE parallel
    IMPLICIT NONE
    INTEGER :: i

    IF(  sgsmodel == sgsmodel_LDM)   THEN
      n_var_Ilm = get_index('Ilm            ' )
      n_var_Imm = get_index('Imm            ' )
    ELSE IF( sgsmodel_DSM <= sgsmodel .AND. sgsmodel <= sgsmodel_LDKMG ) THEN
      n_var_K = get_index('Ksgs           ' )
    ELSE IF( sgsmodel == sgsmodel_KEPS) THEN
      n_var_K = get_index('Ksgs           ' )
      n_var_KD = get_index('Kdiss           ' )
    ELSE IF( sgsmodel == sgsmodel_KOME .OR. sgsmodel == sgsmodel_KOMET ) THEN
      n_var_K = get_index('Ksgs           ' )
      n_var_KD = get_index('Komeg           ' )
    END IF

    IF( do_Adp_Eps_Spatial .OR. do_Adp_Eps_Spatial_Evol ) THEN   !!!! TODO: req_restart?
      n_var_SGSD = get_index( 'SGSD           ')
      n_var_RD   = get_index( 'RD             ')
    ENDIF

    IF(  sgsmodel == sgsmodel_LDM)   THEN
      scaleCoeff(n_var_Ilm)  = 0.1_pr
      scaleCoeff(n_var_Imm)  = 0.1_pr
    ELSE IF( sgsmodel_DSM <= sgsmodel .AND. sgsmodel <= sgsmodel_LDKMG ) THEN
      scaleCoeff(n_var_K)  = 0.1_pr
    ELSE IF( sgsmodel == sgsmodel_KEPS .OR. sgsmodel == sgsmodel_KOME .OR. sgsmodel == sgsmodel_KOMET ) THEN
      scaleCoeff(n_var_K)  = 1.0_pr
      scaleCoeff(n_var_KD)  = 1.0_pr
    END IF

    ! set up semi-implicit integration: TODO: get Oleg's sign off
    ! this index is used for updating velocity in time integration procedure when SGS force is NOT treated EXPLICITLY
    IF(sgsmodel <= sgsmodel_GDM) mdl_form = 0 
    IF(sgsmodel == sgsmodel_LDM .AND. mdl_form > 1 )  THEN
      N_SGS_TimeIntegration = get_index('Ilm           ' )
    ELSE IF( sgsmodel_DSM <= sgsmodel .AND. sgsmodel <= sgsmodel_LDKMG .AND. mdl_form >= 1 )  THEN
      N_SGS_TimeIntegration = get_index('Ksgs           ' ) ! the last variable with non-zero sgs_force
    ELSE IF( sgsmodel == sgsmodel_KEPS )  THEN
      N_SGS_TimeIntegration = get_index('Kdiss           ' ) ! the last variable with non-zero sgs_force
    ELSE IF( sgsmodel == sgsmodel_KOME .OR. sgsmodel == sgsmodel_KOMET )  THEN
      N_SGS_TimeIntegration = get_index('Komeg           ' ) ! the last variable with non-zero sgs_force
    ELSE
      N_SGS_TimeIntegration = n_integrated
    END IF

    IF (par_rank.EQ.0) THEN
       PRINT * ,''; PRINT * ,''
       PRINT *, 'PRINT   from   SUBROUTINE SGS_setup'
       PRINT *, '************** Setting up SGS module ****************'
       PRINT * ,' SGS module'
       PRINT *, '*****************************************************'
       PRINT *, 'n_var_SGS         = ',  n_var_SGS
       IF( (do_Adp_Eps_Spatial .OR. do_Adp_Eps_Spatial_Evol) .AND. saveSGSD ) THEN
       PRINT *, 'n_var_SGSD        = ',  n_var_SGSD
       PRINT *, 'n_var_RD          = ',  n_var_RD
       END IF
       PRINT *, '- - - - - - - - - - - - - - - - - -'
       PRINT *, 'n_integrated      = ',n_integrated 
       PRINT *, 'n_var_additional  = ',n_var_additional   
       PRINT *, 'n_var             = ',n_var 
       PRINT *, 'n_var_exact       = ',n_var_exact 
       PRINT *, '*******************Variable Names*******************'
       DO i = 1,n_var
          WRITE (*, u_variable_names_fmt) u_variable_names(i)
       END DO
       PRINT *, '****************************************************'
       PRINT *, 'N_SGS_TimeIntegration = ',N_SGS_TimeIntegration 
    END IF

  
  END SUBROUTINE SGS_finalize_setup

  !
  ! read variables from input file for this user case 
  !
  SUBROUTINE  SGS_read_input()
    USE parallel
    IMPLICIT NONE

    INTEGER   :: io_status, eps_status
    REAL (pr) :: tmp_eps     
     

   print_SGS_stats = .FALSE.
   call input_logical ('print_SGS_stats', print_SGS_stats, 'default', &
       ' !if true SGS statisitics will be printed')

   call input_integer ('SGS_Homogeneity', SGS_Homogeneity, 'stop', ' SGS_Homogeneity (SGS problem type):  0 - Incompressible Homogeneous Isotropic Turbulence,   1 - Incompressible External Flow')

   call input_integer ('SGS_mdl_form',mdl_form,'stop', &
       ' SGS_mdl_form,  only relevant to sgsmodel > 3')

   call input_integer ('SGS_TimeIntegrationType',SGS_TimeIntegrationType,'stop', &
       ' SGS_TimeIntegrationType of SGS term: 0 - explicit 1st order, 1 - semi-implicit 1st order, 2 - semi-implicit 2nd order')

   call input_logical ('Lijtraceless', Lijtraceless, 'stop', &
       ' Lijtraceless ! make Lij traceless in dyn sgs model')

   call input_logical ('Mijtraceless', Mijtraceless, 'stop', &
       ' Mijtraceless ! make Mij traceless in dyn sgs model')

   call input_logical ('DynSmodGridFilt', DynSmodGridFilt, 'stop', &
       ' DynSmodGridFilt !if true |S| is based on grid filt in nu_t=Cdyn|S|')

   call input_logical ('DynSmodGridFiltS', DynSmodGridFiltS, 'stop', &
       ' DynSmodGridFiltS !if true S is based on grid filt in tauij = -2 nu_t Sij')

   call input_logical ('DynMdlFiltLijLast', DynMdlFiltLijLast, 'stop', &
       ' ! if true Lij=(ugrid ugrid - u_test u_test )_test ! else  Lij=(ugrid ugrid)_test - u_test u_test')

   call input_real ('Mcoeff', Mcoeff, 'stop', &
        ' Mcoeff ! Mij = Mcoeff * |S>2eps| Sij>2eps - (|S|Sij )>2eps')
        ! also used is LDKMB and LDKMG models 

   do_const_diss = .FALSE. !default
   call input_logical ('do_const_diss',do_const_diss,'default', &
        ' Adjust eps for constant dissipation')

   do_const_k = .FALSE. !default
   call input_logical ('do_const_k',do_const_k,'default', &
        ' Adjust eps for constant k_SGS')

   call input_real ('SGS_model_coef1',sgsmodel_coef1,'stop', &
        ' sgsmodel_coef1 ! first model coefficient for model set by sgsmodel (see above)')
   
   call input_logical ('clip_nu_nut', clip_nu_nut, 'stop', &
        ' clip_nu_nut ! clip nu+ nu_t if true')
   
   call input_logical ('clip_Cs', clip_Cs, 'stop', &
        ' clip_Cs ! clip Cs if true')

   call input_logical ('deltaMij',deltaMij,'stop', &
        '  deltaMij, Mij definition including delta^2')

   delta_L2weight = 0.0_pr
   call input_real ('delta_L2weight',delta_L2weight,'default', &
        '  delta_L2weight, parameter for additional weighting  to find global model coefficient, i.e. <delta^(delta_L2weight) Lij Mij>')
   ! Negative values put more emphasis on small scale region, positive on large scale.  
   ! Since dissipation happens at small scale, it makes sence to use delta_L2weight = -3.0_pr

   !**************** variables specific to LDM model ******************************
   IF( sgsmodel == sgsmodel_LDM) THEN 

      call input_real ('LDMtheta', LDMtheta, 'stop', &
           ' LDMtheta, langrangian model time scale coefficient')

      call input_real ('CI', CI, 'stop', &
           ' CI, langrangian model diffusion scale coefficient')

      call input_logical ('adaptIlm',adaptIlm,'stop', &
           '  adaptIlm, Adapt on the Ilm')

      call input_logical ('adaptImm',adaptImm,'stop', &
           '  adaptImm, Adapt on the Imm')

      call input_logical ('SpaceTimeAvg',SpaceTimeAvg,'stop', &
           '  SpaceTimeAvg, Lagrangian spatial filter')

   END IF
   !************* variables specific to DSM, LKM, LDKMB, & LDKMG  models ****************


   IF( sgsmodel_LDKMB <= sgsmodel .AND. sgsmodel <= sgsmodel_LDKMG ) THEN
      uniform_C_nu = .FALSE.
      call input_logical ('uniform_C_nu',uniform_C_nu,'default', &
           '  uniform_C_nu, uniform C_nu given the global dissipation')
   END IF

   IF( sgsmodel_DSM <= sgsmodel .AND. sgsmodel <= sgsmodel_LDKMG ) THEN

      adaptK = .FALSE.
      call input_logical ('adaptK',adaptK,'default', &
           '  adaptK, Adapt on sgs kinetic energy k')
      
      call input_real ('C_eps_sgs',C_eps_sgs,'stop', &
           '  C_eps_sgs, ksgs dissipation prescribed or max coefficient')

      C_nu_art_k = 0.0_pr
      call input_real ('C_nu_art_k',C_nu_art_k,'default', &
           '  C_nu_art_k, ksgs artificial dissipation coefficient')

      call input_real ('alpha_k', alpha_k, 'stop', &
           ' alpha_k, portion of the SGS kinetic energy')

      call input_integer ('Ceps_model',Ceps_model,'stop', &
           ' Ceps_model: 0 - fixed coefficeint, 1 - dynamic Bardina-like scaling, 2 - dynamic Germano like scaling')

      Ksgs_init = 0.00015_pr
      call input_real ('Ksgs_init',Ksgs_init,'default', &
        '  Ksgs_init, initial positive value for Ksgs')

   END IF

   !************** variables specific to URANS models *****************

   IF( sgsmodel == sgsmodel_KEPS .OR. sgsmodel == sgsmodel_KOME .OR. sgsmodel == sgsmodel_KOMET ) THEN

     adaptK = .FALSE.
     call input_logical ('adaptK',adaptK,'default', &
                         'adaptK, Adapt on sgs kinetic energy Ksgs')

     adaptKD = .FALSE.
     call input_logical ('adaptKD',adaptKD,'default', &
                         'adaptKD, Adapt on Kdiss/Komeg')

     Prod_index = 0
     call input_integer ('Prod_index',Prod_index,'default', &
                         'Index for turbulence production (0:SS,1:SW,2:WW)')

     lowRe_flag = .FALSE.
     call input_logical ('lowRe_flag',lowRe_flag,'default', &
                         'lowRe_flag, low-Reynolds formulation for wall bounded turbulence')

     C_nu_art_k = 0.0_pr
     call input_real ('C_nu_art_k',C_nu_art_k,'default', &
                      'C_nu_art_k, ksgs artificial dissipation coefficient')

     IF( sgsmodel == sgsmodel_KEPS ) THEN

      Ksgs_init = 0.00015_pr
      call input_real ('Ksgs_init_KEPS',Ksgs_init,'default', &
                       'Ksgs_init, initial positive value for Ksgs')

      Ksgs_min = 0.000000001_pr
      call input_real ('Ksgs_min_KEPS',Ksgs_min,'default', &
                       'Ksgs_min, minimum value for Ksgs')

      Kdiss_init = 0.00001_pr
      call input_real ('Kdiss_init_KEPS',Kdiss_init,'default', &
                       'Kdiss_init, initial positive value for Kdiss')

      Kdiss_min = 0.000000001_pr
      call input_real ('Kdiss_min_KEPS',Kdiss_min,'default', &
                       'Kdiss_min, minimum value for Kdiss')

      C0_RANS = 0.09_pr
      call input_real ('C0_RANS_KEPS',C0_RANS,'default', &
                       'C0_RANS, constant parameter for nu_t definition')

      C1_RANS = 1.44_pr
      call input_real ('C1_RANS_KEPS',C1_RANS,'default', &
                       'C1_RANS, first constant parameter at RHS of Kdiss equation')

      C2_RANS = 1.92_pr
      call input_real ('C2_RANS_KEPS',C2_RANS,'default', &
                       'C2_RANS, second constant parameter at RHS of Kdiss equation')

      C3_RANS = 1.0_pr
      call input_real ('C3_RANS_KEPS',C3_RANS,'default', &
                       'C3_RANS, constant parameter for turbulent diffusion of Ksgs')

      C4_RANS = 1.0_pr/1.3_pr
      call input_real ('C4_RANS_KEPS',C4_RANS,'default', &
                       'C4_RANS, constant parameter for turbulent diffusion of Kdiss')

     ELSEIF( sgsmodel == sgsmodel_KOME .OR. sgsmodel == sgsmodel_KOMET ) THEN

      Ksgs_init = 0.0000015_pr
      call input_real ('Ksgs_init_KOME',Ksgs_init,'default', &
        '  Ksgs_init, initial positive value for Ksgs')

      Ksgs_min = 0.000000001_pr
      call input_real ('Ksgs_min_KOME',Ksgs_min,'default', &
                       'Ksgs_min, minimum value for Ksgs')

      Kdiss_init = 0.00001_pr
      call input_real ('Kdiss_init_KOME',Kdiss_init,'default', &
                       'Kdiss_init, initial positive value for Komeg')

      Kdiss_min = 0.0001_pr
      call input_real ('Kdiss_min_KOME',Kdiss_min,'default', &
                       'Kdiss_min, minimum value for Komeg')

      delta_n = MINVAL(h(J_MX,:))
      call input_real ('delta_n',delta_n,'default', &
                       'delta_n: minimum wall-normal spacing')

      C0_RANS = 9.0_pr/100.0_pr
      call input_real ('C0_RANS_KOME',C0_RANS,'default', &
                       'C0_RANS, constant parameter for nu_t definition')

      C1_RANS = 5.0_pr/9.0
      call input_real ('C1_RANS_KOME',C1_RANS,'default', &
                       'C1_RANS, first constant parameter at RHS of Komeg equation')

      C2_RANS = 3.0_pr/40.0_pr
      call input_real ('C2_RANS_KOME',C2_RANS,'default', &
                       'C2_RANS, second constant parameter at RHS of Komeg equation')

      C3_RANS = 0.5_pr
      call input_real ('C3_RANS_KOME',C3_RANS,'default', &
                       'C3_RANS, constant parameter for turbulent diffusion of Ksgs')

      C4_RANS = 0.5_pr
      call input_real ('C4_RANS_KOME',C4_RANS,'default', &
                       'C4_RANS, constant parameter for turbulent diffusion of Komeg')

      C5_RANS = 0.5_pr
      call input_real ('C5_RANS_KOME',C5_RANS,'default', &
                       'C5_RANS, constant parameter for cross-diffusion of Komeg')

     END IF

   END IF

   !************** variables specific to const disipation or k-SGS strategy **************

   IF( do_const_diss .OR. do_const_k) THEN
      Psgs_diss_goal = 0.0 !default
      call input_real ('Psgs_diss_goal', Psgs_diss_goal, 'default', &
           ' Psgs_diss_goal')
      
      q_eps = 1.0 !default
      call input_real ('q_eps', q_eps, 'default', &
           ' Temporal filtering factor for eps change ')
      
      eps_min = 0.0 !default
      call input_real ('eps_min', eps_min, 'default', &
           ' Allowable lower range for eps to change ')
      
      eps_max = 1.0 !default
      call input_real ('eps_max', eps_max, 'default', &
           ' Allowable upper range for eps to change ')
      
      sgs_gain = 1.0 !default
      call input_real ('sgs_gain', sgs_gain, 'default', &
           ' Gain factor for modifying nu_t ')
      
      Ilm_gain = 1.0 !default
      call input_real ('Ilm_gain', Ilm_gain, 'default', &
           ' Gain factor for modifying Ilm and Ilm forcing ')
      
      max_eps_scale = 1.0 !default
      call input_real ('max_eps_scale', max_eps_scale, 'default', &
           ' max chang in (eps/eps_old)**2 -1.0 ')


       eps_status = 0
       IF (par_rank.EQ.0) THEN
          OPEN  (UNIT=UNIT_const_SGS_dissipation, FILE = TRIM(file_gen)//'const_SGS_dissipation.log', FORM='formatted', STATUS='old', IOSTAT=io_status)
          IF( io_status == 0 ) THEN
             READ (UNIT_const_SGS_dissipation, FMT='(F12.6)')  tmp_eps
             IF ((tmp_eps .LT. eps_min) .AND. (tmp_eps .GT. eps_max)) THEN
                eps_status = 1
             ELSE
                eps_status = 2
             END IF
          ELSE
             eps_status = 3
          END IF
          CLOSE (UNIT_const_SGS_dissipation)
       END IF
       
       CALL parallel_broadcast_int( eps_status )    
       SELECT CASE (eps_status)
       CASE (1)   
          IF (par_rank.EQ.0) PRINT *, 'Error, eps in  .const_SGS_dissipation.log    < eps_min  OR  > eps_max    in .inp file'
          IF (par_rank.EQ.0) PRINT *,' Exiting ...'
          CALL parallel_finalize; STOP
       CASE (2)   
          CALL parallel_broadcast( tmp_eps )   
          eps      = tmp_eps
       END SELECT


   END IF
   
   
   ! Spatial Adaptive Epsilon
   !************** variables specific to Spatial Adaptive Epsilon           **************
    IF( do_Adp_Eps_Spatial .OR. do_Adp_Eps_Spatial_Evol ) THEN     
      saveSGSD = .FALSE. !default
      call input_logical ('saveSGSD', saveSGSD, 'default', '  saveSGSD, Adapt on sgs kinetic energy k')
    ENDIF
   

  END SUBROUTINE  SGS_read_input





  SUBROUTINE SGS_initial_conditions (u_in, nlocal, ne_local, t_local, scl, scl_fltwt)
    IMPLICIT NONE
    INTEGER,                                INTENT (IN)    :: nlocal, ne_local
    REAL (pr), DIMENSION (nlocal,ne_local), INTENT (INOUT) :: u_in
    REAL (pr)                                              :: scl(1:n_var),scl_fltwt
    REAL (pr),                              INTENT (IN)    :: t_local


    ! 
    ! Initialize scalars if we are starting from a file, but this is not a restart from a restart file.
    !
    IF ( sgsmodel == sgsmodel_LDM .AND. IC_restart_mode.EQ.3 ) THEN
       u_in(:,n_var_Ilm) = 0.0_pr
       u_in(:,n_var_Imm) = 0.0_pr
    ELSE IF( ( sgsmodel_DSM <= sgsmodel .AND. sgsmodel <= sgsmodel_LDKMG ) .AND. .NOT. IC_restart .AND. IC_from_file) THEN
       u_in(:,n_var_K) = 0.0_pr
    ELSE IF ( sgsmodel == sgsmodel_KEPS .AND. IC_restart_mode.EQ.3 ) THEN
!       u_in(:,n_var_K) = Ksgs_init
!       u_in(:,n_var_KD) = Kdiss_init
    ELSE IF ( sgsmodel == sgsmodel_KOME .AND. IC_restart_mode.EQ.3 ) THEN
!       u_in(:,n_var_K) = Ksgs_init
!       u_in(:,n_var_KD) = Kdiss_init
    ELSE IF ( sgsmodel == sgsmodel_KOMET .AND. IC_restart_mode.EQ.3 ) THEN
!       u_in(:,n_var_K) = Ksgs_init
!       u_in(:,n_var_KD) = Kdiss_init
    END IF
    model_rescale = .FALSE. ! set it to .FALSE. during initial condition or restart, 
                        ! and to .TRUE. during time integartion

  END SUBROUTINE SGS_initial_conditions





!******************** Boundary conditions for additional SGS equations  *****************
  SUBROUTINE SGS_algebraic_BC (Lu, u_in, nlocal, ne_local, jlev, meth)
    !--Defines default SGS boundary condition type
    IMPLICIT NONE
    INTEGER,                                INTENT (IN)    :: jlev, meth, ne_local, nlocal
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (INOUT) :: Lu
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (IN)    :: u_in

    INTEGER :: ie, ii, shift


    !
    ! There are periodic BC conditions
    !

  END SUBROUTINE SGS_algebraic_BC





  SUBROUTINE SGS_algebraic_BC_diag (Lu_diag, nlocal, ne_local, jlev, meth)
    !--Defines default SGS boundary condition type
    IMPLICIT NONE
    INTEGER,                                INTENT (IN)    :: jlev, meth, ne_local, nlocal
    REAL (pr), DIMENSION (nlocal,ne_local), INTENT (INOUT) :: Lu_diag

    INTEGER :: ie, ii, shift


    !
    ! There are periodic BC conditions
    !

  END SUBROUTINE SGS_algebraic_BC_diag





  SUBROUTINE SGS_algebraic_BC_rhs (rhs, ne_local, nlocal, jlev)
    !--Sets rhs for default SGS boundary conditions
    IMPLICIT NONE
    INTEGER,                                INTENT (IN)    :: ne_local, nlocal, jlev
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (INOUT) :: rhs

    INTEGER :: ie, ii, shift


    !
    ! There are periodic BC conditions
    !

  END SUBROUTINE SGS_algebraic_BC_rhs

  SUBROUTINE SGS_rhs (user_rhs, u_integrated, du, d2u)
    IMPLICIT NONE
    REAL (pr), DIMENSION (ng,ne),     INTENT(IN)    :: u_integrated   ! 1D flat version of integrated variables without BC 
    REAL (pr), DIMENSION (n),         INTENT(INOUT) :: user_rhs
    REAL (pr), DIMENSION (ne,ng,dim), INTENT(INOUT) :: du, d2u

    INTEGER            :: ie, shift, i
    INTEGER, PARAMETER :: meth=1

    REAL (pr) :: tmp1, tmp2

    !Note: derivatives are passed to take adantage of vector wavelet transform.  They can be done in this subroutine as well.
    
    !adds SGS force into the equation
    DO ie = 1, dim
!    DO ie = 1, n_integrated-n_var_SGS
       shift=(ie-1)*ng
       user_rhs(shift+1:shift+ng) = user_rhs(shift+1:shift+ng) + sgs_mdl_force(:,ie)
    END DO

    IF( sgsmodel == sgsmodel_LDM ) THEN
       CALL LDM_rhs (user_rhs, u_integrated, du, d2u)
    ELSE IF( sgsmodel_DSM <= sgsmodel .AND. sgsmodel <= sgsmodel_LDKMG ) THEN
       CALL Ksgs_rhs (user_rhs, u_integrated, du, d2u)
    ELSE IF( sgsmodel == sgsmodel_KEPS ) THEN
       CALL KEPS_rhs (user_rhs, u_integrated, du, d2u)
    ELSE IF( sgsmodel == sgsmodel_KOME ) THEN
       CALL KOME_rhs (user_rhs, u_integrated, du, d2u)
    ELSE IF( sgsmodel == sgsmodel_KOMET ) THEN
       CALL KOMET_rhs (user_rhs, u_integrated, du, d2u)
    END IF

  END SUBROUTINE SGS_rhs





  ! find Jacobian of Right Hand Side of the problem
  ! u_prev == u_prev_timestep_loc
  SUBROUTINE SGS_Drhs (user_Drhs, u, u_prev, du, d2u, meth)
    IMPLICIT NONE
    !u_prev_timestep passed local to cast it into 2dim array to work with vector derivatives
    REAL (pr), DIMENSION (ng,ne),       INTENT(IN)    :: u, u_prev
    REAL (pr), DIMENSION (n),           INTENT(INOUT) :: user_Drhs
    REAL (pr), DIMENSION (2*ne,ng,dim), INTENT(INOUT) :: du         ! 1st derivatives for u (in du(1:ne,:,:)) and u_prev_timestep (in du(ne+1:2*ne,:,:))
    REAL (pr), DIMENSION (ne  ,ng,dim), INTENT(INOUT) :: d2u        ! 2nd derivatives for u 
    INTEGER,                            INTENT(IN)    :: meth

    INTEGER :: ie, shift


    !--Form right hand side of Navier--Stokes equations
    IF( sgsmodel == sgsmodel_LDM ) THEN
       CALL LDM_Drhs (user_Drhs, u, u_prev, du, d2u, meth)
    ELSE IF(  sgsmodel_DSM <= sgsmodel .AND. sgsmodel <= sgsmodel_LDKMG ) THEN
       CALL Ksgs_Drhs (user_Drhs, u, u_prev, du, d2u, meth)
    ELSE IF( sgsmodel == sgsmodel_KEPS ) THEN
       CALL KEPS_Drhs (user_Drhs, u, u_prev, du, d2u, meth)
    ELSE IF( sgsmodel == sgsmodel_KOME ) THEN
       CALL KOME_Drhs (user_Drhs, u, u_prev, du, d2u, meth)
    ELSE IF( sgsmodel == sgsmodel_KOMET ) THEN
       CALL KOMET_Drhs (user_Drhs, u, u_prev, du, d2u, meth)
    END IF

  END SUBROUTINE SGS_Drhs





  !
  ! Uses u_prev_timestep, which is global flat (1D ) array set in time_step_cn()
  !
  SUBROUTINE SGS_Drhs_diag (user_Drhs_diag, du_prev_timestep, du, d2u, meth)
    IMPLICIT NONE
    REAL (pr), DIMENSION (n),         INTENT(INOUT) :: user_Drhs_diag
    REAL (pr), DIMENSION (ne,ng,dim), INTENT(INOUT) :: du_prev_timestep
    REAL (pr), DIMENSION (ng,dim),    INTENT(INOUT) :: du, d2u
    INTEGER,                          INTENT (IN)   :: meth

    INTEGER :: ie, shift,shiftIlm,shiftImm
    
    
    IF( sgsmodel == sgsmodel_LDM ) THEN
       CALL LDM_Drhs_diag (user_Drhs_diag, du_prev_timestep, du, d2u, meth)
    ELSE IF( sgsmodel_DSM <= sgsmodel .AND. sgsmodel <= sgsmodel_LDKMG ) THEN
       CALL Ksgs_Drhs_diag (user_Drhs_diag, du_prev_timestep, du, d2u, meth)
    ELSE IF( sgsmodel == sgsmodel_KEPS ) THEN
       CALL KEPS_Drhs_diag (user_Drhs_diag, du_prev_timestep, du, d2u, meth)
    ELSE IF( sgsmodel == sgsmodel_KOME ) THEN
       CALL KOME_Drhs_diag (user_Drhs_diag, du_prev_timestep, du, d2u, meth)
    ELSE IF( sgsmodel == sgsmodel_KOMET ) THEN
       CALL KOMET_Drhs_diag (user_Drhs_diag, du_prev_timestep, du, d2u, meth)
    END IF

  END SUBROUTINE SGS_Drhs_diag





  SUBROUTINE SGS_stats ( u_in ,j_mn, startup_flag)
    USE util_vars        ! dA
    USE parallel
    USE io_3D_vars
   !USE fft_module
   !USE spectra_module
    IMPLICIT NONE
    REAL (pr), DIMENSION (nwlt,1:n_var), INTENT (IN) :: u_in
    INTEGER ,                            INTENT (IN) :: j_mn, startup_flag 
    
    CHARACTER (LEN=256)  :: filename
   !CHARACTER (LEN=256)  :: file_out
    INTEGER              :: outputfileUNIT
    INTEGER :: n_var_mask   ! variable for penalization mask 
    INTEGER              :: count_u_g_mdl_gridfilt_mask, count_u_g_mdl_testfilt_mask
    REAL (pr)            :: ttke, tmp1
    LOGICAL , SAVE       :: start = .TRUE.

    
    ! only proc 0 writes the file
    IF (par_rank.EQ.0) THEN
       filename = TRIM(res_path)//TRIM(file_gen)//'case_turb.log'
      !PRINT *,' SGS_stats, logging at proc #0 to : ', filename
    END IF

    ! Find the TKE and ... if the dA weights have been allocated (and it is
    ! assumed that if they are allocated they have been calculated)

    IF( ALLOCATED(dA) ) THEN
       !
       ! volume-averaged resolved kinetic energy
       !
       ttke = 0.5_pr * SUM( dA(:)*SUM(u_in(:,1:dim)**2,DIM=2) ) /sumdA_global
       CALL parallel_global_sum( REAL=ttke )
 
       !
       ! volume-averaged Ksgs and Kdiss (fluid domain)
       !
       IF( sgsmodel_DSM <= sgsmodel  .AND. sgsmodel <= sgsmodel_LDKMG ) THEN
          total_Ksgs = SUM(dA(:)*u_in(:,n_var_K))/sumdA_global
          CALL parallel_global_sum( REAL=total_Ksgs )
       ELSEIF( sgsmodel == sgsmodel_KEPS .OR. sgsmodel == sgsmodel_KOME .OR. sgsmodel == sgsmodel_KOMET ) THEN
          IF (imask_obstacle) THEN
             n_var_mask      = get_index('Mask    ')  
!             IF (verb_level.GT.0 .AND. par_rank.EQ.0) PRINT *,' n_var_mask = ', n_var_mask, sumdA_global
             total_Ksgs = SUM(dA(:)*u_in(:,n_var_K)*(1.0_pr-u_in(:,n_var_mask)))/(sumdA_global-5.0_pr)   !should be sumdA_global-height*length 
             CALL parallel_global_sum( REAL=total_Ksgs )
             total_Kdiss = SUM(dA(:)*u_in(:,n_var_KD)*(1.0_pr-u_in(:,n_var_mask)))/(sumdA_global-5.0_pr)   !should be sumdA_global-height*length
             CALL parallel_global_sum( REAL=total_Kdiss )
          END IF
       ELSE
          total_Ksgs          = 0.0_pr
          total_Kdiss         = 0.0_pr
          total_neg_sgs_diss  = 0.0_pr
          backscatter_frac    = 0.0_pr 
       END IF
       
       IF (verb_level.GT.0 .AND. par_rank.EQ.0) THEN
          PRINT *,' SGS_STATS'
          PRINT *,'***********************************************'
          WRITE(*,'(" (Approximate) TKE =",E12.5)')  ttke
          IF(sgsmodel >= sgsmodel_DSM .AND. sgsmodel <= sgsmodel_KOMET) &
               WRITE(*,'(" (Approximate) TKsgs =",E12.5," TKsgs/TKE=",E12.5)')  total_Ksgs, total_Ksgs/ttke
          IF(sgsmodel <= sgsmodel_LDM) &
               PRINT *,' Smagorinsky Mdl. Coeff. Cs = ', Cs
          PRINT *,' total_resolved_diss = ',total_resolved_diss
          PRINT *,' total_sgs_diss = ',total_sgs_diss
       END IF
       
       !
       ! percentage of SGS dissipation
       !
       IF((total_resolved_diss+total_sgs_diss) /= 0.0) THEN
          Psgs_diss = 100._pr*total_sgs_diss/(total_resolved_diss+total_sgs_diss)
       ENDIF

       !
       ! active points on masks
       !
       IF( ALLOCATED(u_g_mdl_gridfilt_mask ) ) THEN
          tmp1 = SUM(u_g_mdl_gridfilt_mask)
          CALL parallel_global_sum( REAL=tmp1 )
          count_u_g_mdl_gridfilt_mask = INT( tmp1 )
       ELSE
          count_u_g_mdl_gridfilt_mask = -1
       ENDIF

       IF( ALLOCATED(u_g_mdl_testfilt_mask ) ) THEN
          tmp1 = SUM(u_g_mdl_testfilt_mask)
          CALL parallel_global_sum( REAL=tmp1 )
          count_u_g_mdl_testfilt_mask = INT( tmp1 )
       ELSE
          count_u_g_mdl_testfilt_mask = -1
       ENDIF

       IF (verb_level.GT.0 .AND. par_rank.EQ.0) THEN
          PRINT *,' # points on grid filt mask = ', count_u_g_mdl_gridfilt_mask
          PRINT *,' # points on test filt mask = ', count_u_g_mdl_testfilt_mask
          PRINT *,'***********************************************'
          PRINT *,''
       END IF

       !
       ! output on file (only proc 0 writes the file)
       !
       IF (par_rank.EQ.0) THEN
          outputfileUNIT = 157
          OPEN (UNIT = outputfileUNIT, FILE =filename, STATUS='unknown',&
               POSITION='APPEND')
         !PRINT *,'SGS_stats(), start = ', start 
          IF( start ) THEN
             write(UNIT=outputfileUNIT,ADVANCE='YES', FMT='( a )' ) &
             '%Time       avg_tke            Cs             ttl_res_diss   ttl_sgs_diss   gridfilt_mask  testfilt_mask  Psgs_diss     avg_Ksgs       eps      ttl_neg_sgs_diss  backscatter_frac  ttl_Leonard_diss  ttl_residual_diss  avg_Kdiss'
          END IF
          start = .FALSE.
          write(UNIT=outputfileUNIT,ADVANCE='YES' , &
               FMT='(  5(e14.7 , '' '') , 2(i14 , '' ''), 8(e14.7 , '' ''))' ) &
               t, ttke, Cs, total_resolved_diss, total_sgs_diss, &
               count_u_g_mdl_gridfilt_mask, count_u_g_mdl_testfilt_mask, Psgs_diss, total_Ksgs, eps, total_neg_sgs_diss, &
               backscatter_frac, total_Leonard_diss, total_residual_diss, total_Kdiss
          CLOSE(UNIT = outputfileUNIT)
       END IF

    END IF

  END SUBROUTINE SGS_stats





!!$!
!!$! Calculate any statitics
!!$!
!!$SUBROUTINE other_stats_XXX ( u , filename ,j_mn)
!!$  USE fft_module
!!$  USE spectra_module
!!$  IMPLICIT NONE
!!$  REAL (pr), DIMENSION (nwlt,1:dim), INTENT (IN) :: u
!!$  CHARACTER (LEN=*) , INTENT (IN) :: filename
!!$!  CHARACTER (LEN=256)             :: file_out
!!$  INTEGER :: j_mn 
!!$!  INTEGER :: it_local !iteration
!!$
!!$  LOGICAL , SAVE :: start = .TRUE.
!!$  REAL (pr) ::  pi2
!!$  REAL (pr) :: field(nxyz(1)+2,nxyz(2)+1,nxyz(3)+1,3)
!!$  REAL (pr) :: the_tke, the_int_length,total_diss
!!$  INTEGER   :: i, io_status
!!$  !CHARACTER (LEN=LEN(filename))  :: filename2
!!$
!!$  !* Initialize fft's on the first call
!!$  pi2 = 8.0D0 *atan(1.0D0 )
!!$
!!$  IF( start ) THEN
!!$     print *,' In user_stats  initializing fft''s '
!!$     CALL init_fft(nxyz(1),nxyz(2),nxyz(3),pi2,pi2,pi2, .true. )
!!$  END IF
!!$
!!$  !CALL test_fft()
!!$  !STOP
!!$
!!$  ! Save the  flat u array  into the field 
!!$  field = 0.0_pr
!!$  DO i=1,nwlt
!!$     field(indx(i,1)+1,indx(i,2)+1,indx(i,3)+1,1) = u(i,1)
!!$  END DO
!!$  DO i=1,nwlt
!!$     field(indx(i,1)+1,indx(i,2)+1,indx(i,3)+1,2) = u(i,2)
!!$  END DO
!!$  DO i=1,nwlt
!!$     field(indx(i,1)+1,indx(i,2)+1,indx(i,3)+1,3) = u(i,3)
!!$  END DO
!!$
!!$  CALL rtoft(field(:,:,:,1))
!!$  CALL rtoft(field(:,:,:,2))
!!$  CALL rtoft(field(:,:,:,3))
!!$
!!$  CALL print_spectra2(field(:,:,:,1),field(:,:,:,2),field(:,:,:,3),&
!!$       TRIM(filename)//"it"//it_string//'.spectra',nxyz(1),nxyz(2),nxyz(3),the_tke,total_diss )
!!$
!!$!  CALL tke( field(:,:,:,1),field(:,:,:,2),field(:,:,:,3), the_tke, the_int_length )
!!$  PRINT *,' '
!!$  PRINT *,'******************* Turbulent statistics ******************* '
!!$  PRINT *,'* TKE                = ', the_tke
!!$  PRINT *,'* TKE real space     = ', SUM( Da*(u(:,1)**2 +u(:,2)**2 + u(:,3)**2) )
!!$
!!$  PRINT *,'* Total Dissipation  = ', total_diss
!!$  PRINT *,'******************* End Turbulent statistics *************** '
!!$  PRINT *,' '
!!$
!!$  ! make sure that any log files exist, and if not create them.
!!$  !filename2 = TRIM(filename)//'.turb.stats'
!!$  CALL open_file(TRIM(filename)//'.turb.stats', it, 13, .TRUE., .TRUE.)
!!$
!!$  IF( start ) WRITE(13,'("% time iteration tke total_dissipation ")')
!!$
!!$  WRITE(13,'(E12.5,1x,I10,1x,3(E12.5,1x) )') t,it,the_tke,total_diss
!!$  CLOSE (13)
!!$     
!!$  start = .FALSE.
!!$
!!$END SUBROUTINE other_stats_XXX










  !******************************************************************************************
  !************************************* SGS MODEL ROUTINES *********************************
  !******************************************************************************************


  !
  ! Intialize sgs model
  ! This routine is called once in the first
  ! iteration of the main time integration loop.
  ! weights and model filters have been setup for first loop when this routine is called.
  !
  SUBROUTINE sgs_model_setup( nlocal )
    USE wlt_trns_vars      ! indx_DB 
    USE wlt_trns_mod       ! delta_from_mask
    USE curvilinear
    USE parallel
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: nlocal
    
    INTEGER, SAVE        :: nlocal_last = 0
    INTEGER              :: i, k, ii, j, j_df, wlt_type, face_type, idim
    REAL (pr)            :: tmp1, tmp2


    model_rescale = .TRUE.  ! set it to .TRUE. during time integartion 
                            ! and to .FALSE. during initial condition or restart,


    IF( nlocal /= nlocal_last) THEN
       IF (verb_level.GT.0 .AND. par_rank.EQ.0) &
           PRINT *,' Reallocating sgs_mdl_force(1:nlocal, n_integrated) ', nlocal,n_integrated

       IF( ALLOCATED(sgs_mdl_force) )  DEALLOCATE( sgs_mdl_force )
                                       ALLOCATE(   sgs_mdl_force(1:nlocal, n_integrated) )
   
       IF(sgsmodel == sgsmodel_LDM) THEN

          IF( ALLOCATED(nuI) )         DEALLOCATE( nuI )
                                       ALLOCATE(   nuI(1:nlocal) )
                                 
          IF( ALLOCATED(one_T) )       DEALLOCATE( one_T )
                                       ALLOCATE(   one_T(1:nlocal) )
       END IF

       IF( sgsmodel_DSM <= sgsmodel .AND. sgsmodel <= sgsmodel_LDKMG ) THEN

          IF( ALLOCATED(nutK) )        DEALLOCATE( nutK )                          !by Giuliano Oct 2011
                                       ALLOCATE(   nutK(1:nlocal) ); nutK = 0.0_pr

          IF( ALLOCATED(nuK) )         DEALLOCATE( nuK )
                                       ALLOCATE(   nuK(1:nlocal) ); nuK = 0.0_pr
                                 
          IF ( ALLOCATED(kprod_coeff)) DEALLOCATE( kprod_coeff )
                                       ALLOCATE(   kprod_coeff(1:nlocal) ); kprod_coeff = 0.0_pr
                                       
          IF ( ALLOCATED(eps_sgs))     DEALLOCATE( eps_sgs )
                                       ALLOCATE(   eps_sgs(1:nlocal) ); eps_sgs = 0.0_pr
       END IF

       IF( sgsmodel == sgsmodel_KEPS .OR. sgsmodel == sgsmodel_KOME .OR. sgsmodel == sgsmodel_KOMET ) THEN

          IF( ALLOCATED(nutK) )        DEALLOCATE( nutK )
                                       ALLOCATE(   nutK(1:nlocal) ); nutK = 0.0_pr

          IF( ALLOCATED(nuK) )         DEALLOCATE( nuK )
                                       ALLOCATE(   nuK(1:nlocal) ); nuK = 0.0_pr

                                       IF( ALLOCATED(ASTAR) )       DEALLOCATE( ASTAR )
                                       ALLOCATE(   ASTAR(1:nlocal) ); ASTAR = 1.0_pr

          IF( ALLOCATED(BSTAR) )       DEALLOCATE( BSTAR )
                                       ALLOCATE(   BSTAR(1:nlocal) ); BSTAR = 1.0_pr

          IF( ALLOCATED(CSTAR) )       DEALLOCATE( CSTAR )
                                       ALLOCATE(   CSTAR(1:nlocal) ); CSTAR = 1.0_pr
       END IF

    END IF
    sgs_mdl_force = 0.0_pr ! setting SGS force to zero for different SGS_TimeIntegrationType
    
    ! Setup filter widths 
    CALL sgs_delta_setup( nlocal )
    
  END SUBROUTINE sgs_model_setup





  !
  ! calculate sgs model forcing term
  ! user_sgs_force is called int he beginning of each times step in time_adv_cn().
  ! THE sgs forcign is stored for the time step in sgs_mdl_force(1:nlocal,1:n_integrated)
  ! where nlocal should be nwlt.
  ! 
  ! Accesses u from field module, 
  !          j_lev from wlt_vars module,
  !
!  SUBROUTINE  sgs_force ( u_in, nlocal, wdist)
  SUBROUTINE  sgs_force ( u_in, nlocal)
    USE util_vars        ! dA
    USE parallel
    IMPLICIT NONE

    INTEGER,                                    INTENT (IN)    :: nlocal
    REAL (pr), DIMENSION (nlocal,n_integrated), INTENT (INOUT) :: u_in

    INTEGER                              :: idim, i, j, k,  ie, deriv_meth_sgs
    REAL (pr), DIMENSION (dim*(dim+1)/2,nlocal,dim)  :: du, d2u
    REAL (pr), DIMENSION (1:nlocal,dim*(dim+1)/2)    :: tau_ij 
    REAL (pr), DIMENSION (nlocal)        :: nu_t !turbulent eddy viscosity 
!    REAL (pr), DIMENSION (nlocal,2)      :: wdist
    REAL(pr)                             :: tmp1, tmp2
    LOGICAL,   SAVE                      :: init_flag = .TRUE. !true if we are initializing the model

    ! Spatial Adaptive Epsilon
    REAL (pr), DIMENSION (nlocal)        :: Local_Resolved_Dissipation


    
    !debug
    IF( BTEST(debug_level,1) ) THEN
       IF (par_rank.EQ.0) THEN
          WRITE(*,'(A)') "  "
          WRITE(*,'(A)') "Debug In sgs_force()" 
       END IF
       DO i=1,n_integrated
          tmp1 = MINVAL(u_in(:,i))
          tmp2 = MAXVAL(u_in(:,i))
          CALL parallel_global_sum (REALMINVAL=tmp1)
          CALL parallel_global_sum (REALMAXVAL=tmp2)
          IF (par_rank.EQ.0) WRITE(*,'(A,2(e15.7,1x))') "MINMAXVAL(u_in(:,i)) = ", tmp1, tmp2
       END DO
       IF (par_rank.EQ.0) THEN
          WRITE(*,'(A)') "END Debug In sgs_force()"
          WRITE(*,'(A)') "  "
       END IF
    ENDIF


    !OLEG: functionality, lit IC_init flag needs to be added here, so we can initialise these
!    IF(  IC_restart .OR. IC_from_file) init_flag = .FALSE. !if the same model is used for IC_from_file
    IF(  IC_restart) init_flag = .FALSE.
    deriv_meth_sgs = 1  ! define deriviative method to use (see c_diff_fast )

    !This is commented out to restart from non LDM case...  
    ! IF( IC_restart )  init_flag = .FALSE. ! use save Ilm Imm if we restarted..
    IF(  IC_restart_mode .EQ. 3 ) init_flag = .FALSE.

    !
    ! Find total resolved diss even if we are not using a model
    !

    CALL Sij( u_in(1:nlocal,1:dim) ,nlocal, tau_ij(:,1:dim*(dim+1)/2), nu_t, .TRUE. )
    ! for now:    tau_ij = S_ij    and   nu_t = SQRT( twoSijSij )

    total_resolved_diss = nu*SUM( dA*nu_t**2 )
    CALL parallel_global_sum( REAL=total_resolved_diss )
    IF (par_rank.EQ.0) PRINT *,'total resolved dissipation', total_resolved_diss

    ! Spatial Adaptive Epsilon
    IF( do_Adp_Eps_Spatial .OR. do_Adp_Eps_Spatial_Evol ) THEN     

       EpsSpatial_Forcing_TimeScale = SUM( dA * abs(nu_t)) 
       CALL parallel_global_sum( REAL=EpsSpatial_Forcing_TimeScale)
       EpsSpatial_Forcing_TimeScale = EpsSpatial_Forcing_TimeScale / ( SQRT(2.0_pr) * sumdA_global )

       !u_in(:,n_var_RD) = nu* nu_t**2    This is NOT Working  B/C  in this Subroutine,  u_in(:,:) is accessible up to  n_integrated, i.e.  u_in(nlocal,n_integrated)
       Local_Resolved_Dissipation(:) = nu* nu_t**2
       call Set_Local_RD( Local_Resolved_Dissipation, nlocal )
    END IF

    IF (verb_level.GT.0 .AND. par_rank.EQ.0) &
        PRINT *,'eddy viscosity SGS model = ', sgsmodel
    
    !find nu_t turbulent eddy viscosity
    SELECT CASE (sgsmodel)
    CASE(sgsmodel_fixSM, sgsmodel_dynSM, sgsmodel_GDM)                  ! constant(1) and dynamic(2-3) coefficient Smagorinsky
       CALL sgs_GDM_model( u_in(1:nlocal,1:dim), tau_ij, nu_t,  nlocal )   ! I am passing the tensor, since there is no need to calculate it again. 
       ! output: tau_ij <- tau_ij, 
       !         nu_t <- nu_t
    CASE (sgsmodel_LDM)                                                 ! Lagrangian dynamic coefficient Smagorinsky (LDM)
       CALL sgs_LDM_model ( u_in, tau_ij, nu_t,  nlocal, init_flag ) 
    CASE (sgsmodel_DSM, sgsmodel_LKM, sgsmodel_LDKMB, sgsmodel_LDKMG )  ! Ksgs based models
       CALL sgs_Kbased_model ( u_in, tau_ij, nu_t,  nlocal, init_flag ) 
!       CALL sgs_Kbased_model ( u_in, tau_ij, nu_t,  nlocal, init_flag, wdist ) 
    CASE (sgsmodel_KEPS)                                                ! KEPS model
!       CALL sgs_KEPS_model ( u_in, tau_ij, nu_t,  nlocal, init_flag, wdist ) 
       CALL sgs_KEPS_model ( u_in, tau_ij, nu_t,  nlocal, init_flag, lowRe_flag ) 
    CASE (sgsmodel_KOME)                                                ! KOME model
       CALL sgs_KOME_model ( u_in, tau_ij, nu_t,  nlocal, init_flag, lowRe_flag ) 
    CASE (sgsmodel_KOMET)                                               ! KOMET model
       CALL sgs_KOMET_model ( u_in, tau_ij, nu_t,  nlocal, init_flag, lowRe_flag ) 
    CASE DEFAULT
       IF (par_rank.EQ.0) PRINT *, 'Error, Unknown model type in sgs_mdl_force(), sgsmodel = ', sgsmodel
       IF (par_rank.EQ.0) PRINT *,' Exiting ...'
       CALL parallel_finalize; STOP
    END SELECT

    !find d(tau_ij)/dx_j
    CALL c_diff_fast(tau_ij(:,1:dim*(dim+1)/2), du, d2u, j_lev, nlocal, deriv_meth_sgs, 10, dim*(dim+1)/2, 1, dim*(dim+1)/2)
    sgs_mdl_force(:,1:dim) = 0.0_pr
    k = 0
    DO i = 1, dim
       sgs_mdl_force(:,i) = sgs_mdl_force(:,i) + du(i,:,i)                                 ! d tau_ii/dx_i
       DO j = i+1, dim
          k = k+1
          sgs_mdl_force(:,i) = sgs_mdl_force(:,i) + du(dim+k,:,j)                          ! d tau_ij/dx_j
          sgs_mdl_force(:,j) = sgs_mdl_force(:,j) + du(dim+k,:,i)                          ! d tau_ji/dx_i= d tau_ij/dx_i  
       END DO
    END DO

    init_flag = .FALSE.

  END  SUBROUTINE sgs_force

  SUBROUTINE const_SGS_dissipation (rescale_tau_ij, eps_scale, Smod, nlocal)    !Giuliano
    USE PDE
    USE util_vars        ! dA         by Giuliano
    USE parallel
    IMPLICIT NONE
    REAL (pr), INTENT(OUT) :: rescale_tau_ij
    REAL (pr), INTENT(OUT) :: eps_scale
    REAL (pr), DIMENSION (1:nlocal) :: Smod    !by Giuliano
    INTEGER, INTENT (IN) :: nlocal             !by Giuliano
    
    REAL (pr) :: eps_old, tmp1
    INTEGER   :: io_status


    !
    ! try const SGS dissipation, after initial adaptation
    !
    Psgs_diss = total_sgs_diss/(total_resolved_diss+total_sgs_diss)
    
    IF (verb_level.GT.0 .AND. par_rank.EQ.0) THEN
       WRITE(*,'(A)') 'Adapting epsilon for constant SGS dissipation '
       WRITE(*,'(A, e15.5 )') ' total_resolved_diss :', total_resolved_diss
       WRITE(*,'(A, e15.5 )') ' total_sgs_diss      :', total_sgs_diss
       WRITE(*,'(A, e15.5 )') ' % SGS diss          :', Psgs_diss
       WRITE(*,'(A, e15.5 )') ' % SGS diss (Goal)   :', Psgs_diss_goal
       WRITE(*,'(A, e15.5 )') ' Current Epsilon     :', eps
    END IF
    
    eps_old = eps !save eps old
    IF(total_sgs_diss > 0.0_pr) THEN
       tmp1 = SUM(Smod*dA)
       CALL parallel_global_sum( REAL= tmp1 )
!       q_eps = tmp1 / sumdA_global            ! global time scale based average |S| 
       eps =  eps_old - q_eps * dt * (Psgs_diss - Psgs_diss_goal)*eps_old
    END IF
    
    IF (verb_level.GT.0 .AND. par_rank.EQ.0) THEN
       WRITE(*,'(A, e15.5 )')       ' New Epsilon                  : ', eps
       WRITE(*,'(A, 2(e15.5,1x) )') ' Clip eps between eps min/max : ', eps_min, eps_max
    END IF
    
    eps = MAX( eps_min, MIN(eps_max, eps) )
    IF (verb_level.GT.0 .AND. par_rank.EQ.0) &
       WRITE(*,'(A, e15.5 )') ' New Epsilon (Clipped) : ', eps
    
    eps_scale = (eps / eps_old)**2
    IF (verb_level.GT.0 .AND. par_rank.EQ.0) &
       WRITE(*,'(A, e15.5 )') ' eps**2/eps_old**2     : ', eps_scale
    
    ! limit eps_scale
    eps_scale =  1.0_pr + SIGN( MIN( ABS(eps_scale - 1.0_pr ), max_eps_scale),eps_scale - 1.0_pr)
    IF (verb_level.GT.0 .AND. par_rank.EQ.0) &
       WRITE(*,'(A, e15.5 )') ' limited eps_scale     : ', eps_scale
    
    rescale_tau_ij = sgs_gain*(eps_scale -1.0_pr ) + 1.0_pr
    IF (verb_level.GT.0 .AND. par_rank.EQ.0) THEN
       PRINT*,' sgs_gain = ',sgs_gain
       PRINT*,' rescale_tau_ij = sgs_gain(eps_scale -1.0_pr ) + 1.0_pr  = ',rescale_tau_ij
    END IF

  END SUBROUTINE const_SGS_dissipation





  !****************************************************************************************
  !*                              Smagorinsky-type Models                                 *
  !****************************************************************************************
  ! this subroutine is common for all Smagorinsky type models: 1-4
  
  SUBROUTINE sgs_Smagorinsky_model(u_in,  tau_ij, L, S_ij, nu_t, Smod, Smod2, nlocal) 
    USE parallel
    IMPLICIT NONE
    INTEGER,                           INTENT (IN)    :: nlocal
    REAL (pr), DIMENSION (nlocal,dim), INTENT (IN)    :: u_in
    REAL (pr), DIMENSION (1:nlocal,6), INTENT (INOUT) :: tau_ij, L, S_ij
    REAL (pr), DIMENSION (1:nlocal),   INTENT (INOUT) :: nu_t, Smod, Smod2
    
    REAL (pr), DIMENSION (1:nlocal)    :: trace
    REAL (pr), DIMENSION (1:nlocal,6)  :: SM 
    REAL(pr)                           :: tmp1, tmp2, tmp3
    INTEGER                            :: i
    
    
    tmp1 = SUM(u_g_mdl_gridfilt_mask)
    tmp2 = SUM(u_g_mdl_testfilt_mask)
    tmp3 = SUM(u_g_mdl_gridfilt_mask-u_g_mdl_testfilt_mask)
    CALL parallel_global_sum( REAL=tmp1 )
    CALL parallel_global_sum( REAL=tmp2 )
    CALL parallel_global_sum( REAL=tmp3 )
    IF (verb_level.GT.0 .AND. par_rank.EQ.0) THEN
       PRINT *,'SUM(u_g_mdl_gridfilt_mask)                        = ', tmp1
       PRINT *,'SUM(u_g_mdl_testfilt_mask)                        = ', tmp2
       PRINT *,'SUM(u_g_mdl_gridfilt_mask-u_g_mdl_testfilt_mask)  = ',tmp3
    END IF
 
    L(:,4:6) = u_in(:,1:3)  
!    IF(mdl_filt_type_grid ==0) THEN
    IF(mdl_filt_type_grid ==0 .OR. ExplicitFilterNL ) THEN          !by Giuliano
       !Sij are alredy calculated if no additional filtering is done
       S_ij = tau_ij
       Smod(:)=nu_t(:)
    ELSE
       !
       ! grid filter u_in, Save it in L(:,4:6)
       !
       CALL mdl_filt_by_type(L(:,4:6),3,GRIDfilter)
       !OLD syntax-CALL wlt_filt_wmask( L(:,4:6),3,u_g_mdl_gridfilt_mask)
       ! find Sij (based on grid filtered u) & ||S_gridfilt||
       CALL Sij( L(:,4:6),nlocal, s_ij, Smod, .TRUE. )
    END IF

    ! find ||S_gridfilt|| Sij_gridfilt (6 components)
    DO i=1,6
       tau_ij(:,i) =  Smod(:) * s_ij(:,i)
    END DO
    !
    ! test filter ||S_gridfilt|| Sij_gridfilt 
    !
    IF ( ExplicitFilterNL ) CALL mdl_filt_by_type(tau_ij(:,1:6),6,GRIDfilter)     !by Giuliano
    CALL mdl_filt_by_type(tau_ij(:,1:6),6,TESTfilter)
    !OLD syntax-CALL wlt_filt_wmask(tau_ij,6,u_g_mdl_testfilt_mask)

    ! now tau_ij == ( ||S_gridfilt|| Sij_gridfilt )_testfilt

    ! 
    !  test filter u and save in L(:,1:3)
    ! 
    L(:,1:3) = u_in(:,1:3)
    CALL mdl_filt_by_type(L(:,1:3),3,TESTfilter)
    !OLD syntax-CALL wlt_filt_wmask(L(:,1:3) ,3,u_g_mdl_testfilt_mask) 

    ! find Sij based on the test filtered  u
    CALL Sij( L(:,1:3) ,nlocal, SM, Smod2, .TRUE. )
    ! now SM is S_ij
    ! Smod2 is ||S_testfilt||

    ! find ||S>2eps|| Sij>2eps (6 components)
    DO i=1,6
       SM(:,i) = Smod2(:) * SM(:,i)
    END DO
    !now SM == ||S>2eps|| Sij>2eps

    IF ( ExplicitFilterNL ) THEN                             !by Giuliano
       CALL mdl_filt_by_type(SM(:,1:6),6,GRIDfilter)
       CALL mdl_filt_by_type(SM(:,1:6),6,TESTfilter)
    END IF

    !
    ! find Mij
    DO i=1,6
       !IF (verb_level.GT.0 .AND. par_rank.EQ.0) &
       !    PRINT *,' Mij =',Mcoeff ,'*  (|S|Sij )>2eps - |S>2eps| Sij>2eps '
       SM(:,i) =  tau_ij(:,i) - Mcoeff* SM(:,i) 
    END DO

    !
    ! Subtract trace for Mij (in SM)
    ! only makes sense in dim=3 case
    !
    trace(:) = (SM(:,1) + SM(:,2) + SM(:,3)) 
    IF( Mijtraceless ) THEN
       DO i=1,nlocal
          trace(i) =  trace(i)/3.0_pr
          SM(i,1:3) = SM(i,1:3) - trace(i)
       END DO
       trace(:) = (SM(:,1) + SM(:,2) + SM(:,3)) 
    END IF

    !
    ! find u_gridfilt u_gridfilt
    !note   L(:,4:6) = u_in(:,1:3)_gridfilt
    tau_ij(:,1) = L(:,4) * L(:,4)   ! u_in(:,1) * u_in(:,1)
    tau_ij(:,2) = L(:,5) * L(:,5)   ! u_in(:,2) * u_in(:,2)
    tau_ij(:,3) = L(:,6) * L(:,6)   ! u_in(:,3) * u_in(:,3)
    tau_ij(:,4) = L(:,4) * L(:,5)   ! u_in(:,1) * u_in(:,2)
    tau_ij(:,5) = L(:,4) * L(:,6)   ! u_in(:,1) * u_in(:,3)
    tau_ij(:,6) = L(:,5) * L(:,6)   ! u_in(:,2) * u_in(:,3)

    !
    ! The first version is Lij = (ugrid ugrid)_test - u_test u_test
    !
    IF( .NOT. DynMdlFiltLijLast ) THEN
       CALL mdl_filt_by_type(tau_ij(:,1:6),6,TESTfilter)
       !OLD syntax-CALL wlt_filt_wmask(tau_ij(:,1:6) ,6,u_g_mdl_testfilt_mask) !testfilt Lij 
    END IF

    !
    ! find Lij, Note that u_test is in L(:,1:3)
    ! so the order is important here.
    !
    L(:,4) = tau_ij(:,4) - L(:,1) * L(:,2)
    L(:,5) = tau_ij(:,5) - L(:,1) * L(:,3)
    L(:,6) = tau_ij(:,6) - L(:,2) * L(:,3)
    L(:,1) = tau_ij(:,1) - L(:,1) * L(:,1)
    L(:,2) = tau_ij(:,2) - L(:,2) * L(:,2)
    L(:,3) = tau_ij(:,3) - L(:,3) * L(:,3)

    !
    ! The second  version is Lij = (ugrid ugrid - u_test u_test )_test
    ! TO BE CHECKED BEFORE REMOVING COMMENTS!     !by Giuliano
    !
!    IF(  DynMdlFiltLijLast ) THEN
!       CALL mdl_filt_by_type(L(:,1:6),6,TESTfilter)
!       !OLD syntax-CALL wlt_filt_wmask(L(:,1:6) ,6,u_g_mdl_testfilt_mask) !testfilt Lij
!    END IF

    IF ( ExplicitFilterNL ) THEN                             !by Giuliano
       CALL mdl_filt_by_type(L(:,1:6),6,GRIDfilter)
       CALL mdl_filt_by_type(L(:,1:6),6,TESTfilter)
    END IF

    !
    ! Subtract trace for Lij (in SM)
    ! only makes sense in dim=3 case
    !
    trace(:) = (L(:,1) + L(:,2) + L(:,3)) 

    IF (verb_level.GT.0) THEN
       tmp1 = MINVAL(trace)
       tmp2 = MAXVAL(trace)
       CALL parallel_global_sum (REALMINVAL=tmp1)
       CALL parallel_global_sum (REALMAXVAL=tmp2)
       IF (par_rank.EQ.0) PRINT *,'Lij(), MINMAX( trace(Lij) )  = ', tmp1, tmp2
    END IF

    IF( Lijtraceless ) THEN
       DO i=1,nlocal
          trace(i) =  trace(i)/3.0_pr
          L(i,1:3) = L(i,1:3) - trace(i)
       END DO
       trace(:) = (L(:,1) + L(:,2) + L(:,3)) 
       
       IF (verb_level.GT.0) THEN
          tmp1 = MINVAL(trace)
          tmp2 = MAXVAL(trace)
          CALL parallel_global_sum (REALMINVAL=tmp1)
          CALL parallel_global_sum (REALMAXVAL=tmp2)
       IF (par_rank.EQ.0) PRINT *,'Lij()_traceless, MINMA( trace(Lij) )  = ', tmp1, tmp2
       END IF
    END IF
    !
    ! find find LijMij and MijMij
    ! (store LijMij in tau_ij(:,1) and
    ! store MijMij in tau_ij(:,2)
    !
    L(:,1) =            L(:,1)*SM(:,1) + L(:,2)*SM(:,2) + L(:,3)*SM(:,3) &
            + 2.0_pr *( L(:,4)*SM(:,4) + L(:,5)*SM(:,5) + L(:,6)*SM(:,6))

    L(:,2) =            SM(:,1)*SM(:,1) + SM(:,2)*SM(:,2) + SM(:,3)*SM(:,3) &
            + 2.0_pr *( SM(:,4)*SM(:,4) + SM(:,5)*SM(:,5) + SM(:,6)*SM(:,6))

    !  print *,'no.times LijMij<0', count( L:,1) < 0.0_pr)
    !  PRINT *,'MINMAX LijMij', MINVAL( L(:,1) ), MAXVAL( L(:,1) )
    !  PRINT *,'clipping LijMij < 0.0 to 0.0 '
    !  L(:,1) = MAX( L(:,1) , 0.0_pr )


    !
    ! Mij definition including delta^2
    !
    IF ( deltaMij ) THEN
       L(:,1) = delta(:)**2 * L(:,1)
       L(:,2) = delta(:)**4 * L(:,2)
    END IF

  END SUBROUTINE sgs_Smagorinsky_model










  !****************************************************************************************
  !*                           Global Dynamic Smagorinsky Model (GDM)                     *
  !****************************************************************************************
  !!
  ! find turbulent eddy viscosity for dynamic smagorinski  model
  !
  ! dynCs -result 
  ! s_ij  - sij based on u>eps+adj
  ! nwlt
  ! u_in - u on current adaptive grid (u>eps+adj)
  !
  ! Globals accessed
  !  Cs - Cs = the Averaged Dynamic Coeff (For either sgsmodel case)
  !  sgsmodel_coef1 - Cs is multiplied by sgsmodel_coef1 (if sgsmodel_coef1 /= 0.0)
  !
  !comiing in: tau_ij <- Sij, nu_t <- twoSijSij
  SUBROUTINE sgs_GDM_model( u_in, tau_ij, nu_t,  nlocal) 
    USE util_vars        ! dA
    USE parallel
    USE field            ! Spatial Adaptive Epsilon       In order to have access to global u(:,n_var_SGSD)
    
    IMPLICIT NONE
    INTEGER,                             INTENT (IN)    :: nlocal
    REAL (pr), DIMENSION (1:nlocal,dim), INTENT (IN)    :: u_in
    REAL (pr), DIMENSION (1:nlocal,6),   INTENT (INOUT) :: tau_ij
    REAL (pr), DIMENSION (1:nlocal),     INTENT (INOUT) :: nu_t     ! dynamic smagorinski coeffient
    
    REAL (pr), DIMENSION (1:nlocal,6) :: S_ij
    REAL (pr), DIMENSION (1:nlocal)   :: Smod, Smod2
    REAL (pr), DIMENSION (1:nlocal,6) :: L 
    REAL (pr)                         :: rescale_tau_ij, eps_scale, tmp1, tmp2, tmp3, tmp4
    INTEGER                           :: i, ie, itmp1
    
    REAL (pr), DIMENSION (1:nlocal)   :: trace          !by Giuliano
    
    
    !debug
    IF( BTEST(debug_level,1) ) THEN
       IF (par_rank.EQ.0) THEN
          IF (par_rank.EQ.0) WRITE(*,'(A)') "  "
          IF (par_rank.EQ.0) WRITE(*,'(A)') "Debug In sgs_LDM_model()" 
       END IF
       DO i=1,dim
          tmp1 = MINVAL(u_in(:,i))
          tmp2 = MAXVAL(u_in(:,i))
          CALL parallel_global_sum (REALMINVAL=tmp1)
          CALL parallel_global_sum (REALMAXVAL=tmp2)
          IF (par_rank.EQ.0) WRITE(*,'(A,2(e15.7,1x))') "MINMAX( u_in(:,i) )  = ", tmp1, tmp2
       END DO
       IF (par_rank.EQ.0) THEN
          IF (par_rank.EQ.0) WRITE(*,'(A)') "END Debug In sgs_LDM_model()"
          IF (par_rank.EQ.0) WRITE(*,'(A)') "  "
       END IF
    ENDIF
    
    IF( dim /= 3 ) THEN
       IF (par_rank.EQ.0) PRINT *, 'Error, Dynamic smagorinski model is only defined for 3D simulations'
       IF (par_rank.EQ.0) PRINT *, 'Exiting...'
       CALL parallel_finalize; STOP
    END IF
    
    IF(sgsmodel > sgsmodel_GDM ) THEN !problem
       IF (par_rank.EQ.0) PRINT *,'sgsmodel should be <4 to call sgs_GDM_model()'
       IF (par_rank.EQ.0) PRINT *, 'Exiting...'
       CALL parallel_finalize; STOP
    END IF
    
    IF (verb_level.GT.0 .AND. par_rank.EQ.0)  PRINT *,'In sgs_GDM_model() '
    
    CALL sgs_Smagorinsky_model(u_in(1:nlocal,1:dim), tau_ij, L, S_ij, nu_t, Smod, Smod2, nlocal)
    IF(sgsmodel == sgsmodel_fixSM ) THEN
       Cs = sgsmodel_coef1
    ELSE IF(sgsmodel == sgsmodel_dynSM .OR. sgsmodel == sgsmodel_GDM) THEN
       !
       ! find dynCs = <LijMij>/<MijMij> 
       ! 
       ! in either case calculate the averaged value of Cs
       ! to be logged in __.case_turb.log
       
       !possible different weigting delta(:)^(-n) to put more weight where small scales are, i.e. where dissipation is.
       
       IF (verb_level.GT.0) THEN
          tmp1 = maxval(ABS(dA))
          tmp2 = maxval(ABS(delta))
          tmp3 = maxval(ABS(L(:,1)))
          tmp4 = maxval(ABS(L(:,2)))
          CALL parallel_global_sum( REALMAXVAL=tmp1 )
          CALL parallel_global_sum( REALMAXVAL=tmp2 )
          CALL parallel_global_sum( REALMAXVAL=tmp3 )
          CALL parallel_global_sum( REALMAXVAL=tmp4 )
          IF (par_rank.EQ.0) THEN
             PRINT *, 'here 1'
             PRINT *, 'maxval(ABS(dA))     = ', tmp1
             PRINT *, 'maxval(ABS(delta))  = ', tmp2
             PRINT *, 'delta_L2weight      = ', delta_L2weight
             PRINT *, 'maxval(ABS(L(:,1))) = ', tmp3
             PRINT *, 'maxval(ABS(L(:,2))) = ', tmp4
          END IF
       END IF

       tmp1 = SUM(dA*delta(:)**delta_L2weight*L(:,1))
       tmp2 = SUM(dA*delta(:)**delta_L2weight*L(:,2))
       CALL parallel_global_sum( REAL=tmp1 )
       CALL parallel_global_sum( REAL=tmp2 )
       Cs = 0.5_pr* tmp1 / tmp2
       IF (verb_level.GT.0 .AND. par_rank.EQ.0) THEN
          PRINT *, 'here 2'      
          PRINT *, 'Cs=', Cs
          WRITE(*,'(A,1x,e15.7)') "Dynamic Smagorinski Coeff = ", Cs
       END IF

       IF( sgsmodel_coef1 /= 0.0_pr ) THEN 
          Cs =  Cs * sgsmodel_coef1 !damping of global dynamic coefficient
          IF (verb_level.GT.0 .AND. par_rank.EQ.0) THEN
              WRITE(*,'(A)')       "sgs_GDM_model() Setting  Cs =  Cs * sgsmodel_coef1 "
              WRITE(*,'(A,f15.7)') "sgs_GDM_model()  sgsmodel_coef1 =", sgsmodel_coef1
              WRITE(*,'(A,f15.7)') "sgs_GDM_model()  new Cs  =", Cs  
          END IF
       END IF
       IF( clip_Cs ) THEN
          Cs =  MAX(Cs , 0.0_pr )
          IF (verb_level.GT.0 .AND. par_rank.EQ.0) PRINT *, 'Cs =  MAX(Cs , 0.0_pr ) '
       END IF
    END IF

    !
    ! here we need |S_(on adaptive grid)|
    ! if we need |S| with Sij based on u_in
    ! IF( mdl_filt_type_grid == 0 ) then Smod has what we need from
    ! above. If not we use already calculated one.
    IF( mdl_filt_type_grid /= 0 .AND. .NOT. DynSmodGridFilt ) THEN
       Smod(:) = nu_t(:)
    END IF

    ! Now calculate nu_t
    !  Cs * ||S||
    IF( sgsmodel == sgsmodel_fixSM .OR. sgsmodel == sgsmodel_GDM ) THEN 
       nu_t(:) =  Cs * Smod(:)
    ELSE IF( sgsmodel == sgsmodel_dynSM ) THEN      ! find local Cs
       nu_t(:) = 0.5_pr *L(:,1)/ L(:,2)             ! Oleg: changed the sign to be consistent with LDM 09/27/2006
       IF( sgsmodel_coef1 /= 0.0_pr ) THEN
          nu_t(:) =   nu_t(:) * sgsmodel_coef1
          IF (verb_level.GT.0 .AND. par_rank.EQ.0) PRINT *, ' sgs_dynSM_model() Setting  Clocal(:) =  Clocal(:) * sgsmodel_coef1 '
       END IF
       IF( clip_Cs ) THEN
          nu_t(:) =  MAX( nu_t(:) , 0.0_pr )
          IF (verb_level.GT.0) THEN
             itmp1 = COUNT( nu_t(:) < 0.0_pr )
             CALL parallel_global_sum( INTEGER=itmp1 )
             IF (par_rank.EQ.0) THEN
                PRINT *, ' Number of points in Clocal to be clipped:', itmp1
                PRINT *, ' Cs_local =  MAX(Cs_local , 0.0_pr ) '
             END IF
          END IF
       END IF
       
       nu_t(:) =  nu_t(:) * Smod(:)
       IF (verb_level.GT.0) THEN
          tmp1 = MINVAL (nu_t(:) )
          tmp2 = MAXVAL (nu_t(:) )
          CALL parallel_global_sum (REALMINVAL=tmp1)
          CALL parallel_global_sum (REALMAXVAL=tmp2)
          IF (par_rank.EQ.0) THEN 
              PRINT *, ' nu_t before clipping: MINMAX (nu_t(:) ) = ', tmp1, tmp2
              PRINT *, ' Dynamic Smagorinski Coeff               = ', Cs
          END IF
       END IF
    ENDIF

    IF( deltaMij ) nu_t(:) =  delta(:)**2 * nu_t(:) !Oleg: added this option to be consistent with LDM 09/27/2006
    !
    ! clip nu + nu_t(:) if called for
    !
    IF( clip_nu_nut ) THEN !Oleg: this is moved, to ensure that clipping is done even for global model.
       nu_t(:) =  MAX( nu_t(:), -nu )

       IF (verb_level.GT.0) THEN
          itmp1 = COUNT( nu + nu_t(:) < 0.0_pr )
          CALL parallel_global_sum( INTEGER=itmp1 )

          tmp1 = MINVAL (nu_t(:) )
          tmp2 = MAXVAL (nu_t(:) )
          CALL parallel_global_sum( REALMINVAL=tmp1 )
          CALL parallel_global_sum( REALMAXVAL=tmp2 )
          IF (par_rank.EQ.0) THEN
             PRINT *, ' Dyn_Smag number of points of (nu +  nu_t(:)) < 0.0   = ', itmp1
             PRINT *, ' nu_t after clipping: MINMAXVAL (nu_t(:) )            = ', tmp1, tmp2
          END IF
       END IF
    END IF

    DO ie = 1, 6
       tau_ij(:,ie) = 2.0_pr * nu_t(:) * S_ij(:,ie) !SGS stress
    END DO

    !
    ! find SGS dissipation: -tauij Sij and store in smod2(:)
    !
    smod2(:) =            tau_ij(:,1)*s_ij(:,1) + tau_ij(:,2)*s_ij(:,2) + tau_ij(:,3)*s_ij(:,3) &
              + 2.0_pr *( tau_ij(:,4)*s_ij(:,4) + tau_ij(:,5)*s_ij(:,5) + tau_ij(:,6)*s_ij(:,6))

    !
    ! calculate total SGS diss
    !
    total_sgs_diss = SUM( dA* smod2 ) 
    CALL parallel_global_sum( REAL=total_sgs_diss )

    !
    ! adding dissipation by resolved Leonard stress -Lij Sij (by Giuliano)
    !
    IF( ExplicitFilterNL )THEN
       IF (par_rank.EQ.0) PRINT *,'sgs_GDM_model: adding Leonard stress dissipation'
       L(:,4) = u_in(:,1)*u_in(:,2)
       L(:,5) = u_in(:,1)*u_in(:,3)
       L(:,6) = u_in(:,2)*u_in(:,3)
       L(:,1) = u_in(:,1)*u_in(:,1)
       L(:,2) = u_in(:,2)*u_in(:,2)
       L(:,3) = u_in(:,3)*u_in(:,3)
       CALL mdl_filt_by_type(L(:,1:6),6,GRIDfilter)
       L(:,4) = u_in(:,1)*u_in(:,2) - L(:,4)
       L(:,5) = u_in(:,1)*u_in(:,3) - L(:,5)
       L(:,6) = u_in(:,2)*u_in(:,3) - L(:,6)
       L(:,1) = u_in(:,1)*u_in(:,1) - L(:,1)
       L(:,2) = u_in(:,2)*u_in(:,2) - L(:,2)
       L(:,3) = u_in(:,3)*u_in(:,3) - L(:,3)
       IF( Lijtraceless ) THEN
         trace(:) = (L(:,1) + L(:,2) + L(:,3))
         DO i=1,nlocal
            trace(i) =  trace(i)/3.0_pr
            L(i,1:3) = L(i,1:3) - trace(i)
         END DO
       END IF
       smod2(:) =   smod2(:) + L(:,1)*s_ij(:,1) + L(:,2)*s_ij(:,2) + L(:,3)*s_ij(:,3) &
                   + 2.0_pr *( L(:,4)*s_ij(:,4) + L(:,5)*s_ij(:,5) + L(:,6)*s_ij(:,6))
    !
    !  calculate total residual & Leonard dissipation
    !
       total_residual_diss = SUM( dA* smod2 )
       CALL parallel_global_sum( REAL=total_residual_diss )
       total_Leonard_diss = total_residual_diss - total_sgs_diss
       total_sgs_diss = total_residual_diss
       total_residual_diss = total_sgs_diss - total_Leonard_diss
    END IF


    ! Spatial Adaptive Epsilon
    IF( do_Adp_Eps_Spatial .OR. do_Adp_Eps_Spatial_Evol ) THEN     
        u(:,n_var_SGSD) = smod2(:)                              ! we can access global u(:,:) here  B/C  we  USE field   in this Subroutine
    ENDIF



    IF( do_const_diss .AND. model_rescale) THEN
       CALL const_SGS_dissipation (rescale_tau_ij, eps_scale, Smod, nlocal)  !modified by Giuliano
       
       nu_t(:) = rescale_tau_ij * nu_t(:)
       tau_ij = rescale_tau_ij * tau_ij
       !
       ! find SGS dissipation: -tauij Sij and store in smod2(:)
       !
       smod2(:) =            tau_ij(:,1)*s_ij(:,1) + tau_ij(:,2)*s_ij(:,2) + tau_ij(:,3)*s_ij(:,3) &
                 + 2.0_pr *( tau_ij(:,4)*s_ij(:,4) + tau_ij(:,5)*s_ij(:,5) + tau_ij(:,6)*s_ij(:,6))
       
       ! calculate total SGS diss
       total_sgs_diss = SUM( dA* smod2 ) 
       CALL parallel_global_sum( REAL=total_sgs_diss )
       
       IF (verb_level.GT.0 .AND. par_rank.EQ.0) THEN
           WRITE(*,'(A)')         ' Rescaling nu_t & tau_ij'
           WRITE(*,'(A, e15.5 )') ' Re-Calculate total_sgs_diss:', total_sgs_diss
           WRITE(*,'(A, e15.5 )') ' % SGS diss (new)           :', total_sgs_diss/(total_resolved_diss+total_sgs_diss)
       END IF
     END IF

    IF(.FALSE.) THEN !this part of the code calculates the low-pass contribution of SGS dissipation
       L = tau_ij
       CALL LP_sgs_dissipation(tmp1, L, S_ij, Smod2, nlocal, MAX(1,j_mx-4))
       
       IF (verb_level.GT.0 .AND. par_rank.EQ.0)&
           WRITE(*,'(" SGS_DISS_total=",E12.5," LP_SGS_DISS_total=",E12.5)') total_sgs_diss, tmp1
    END IF

  END SUBROUTINE sgs_GDM_model

  !========================================================================================
  !=                                    END of GDM                                        =
  !========================================================================================










  !****************************************************************************************
  !*                    Lagrangian Dynamic Smagorinsky Model (LDM)                        *
  !****************************************************************************************
  ! LDM Model form
  ! 0 - pure convection of Ilm & Imm
  ! 1 - explicit integration of Ilm & Imm
  ! 2 - implicit (except for 1/T, LijMij and MijMij which are calculated once explicitly)
  ! 3 - implicit (except for LijMij and MijMij which are calculated once explicitly)


  SUBROUTINE LDM_rhs (user_rhs, u_integrated, du, d2u)
    IMPLICIT NONE
    REAL (pr), DIMENSION (ng,ne),     INTENT(IN)    :: u_integrated   ! 1D flat version of integrated variables without BC 
    REAL (pr), DIMENSION (n),         INTENT(INOUT) :: user_rhs
    REAL (pr), DIMENSION (ne,ng,dim), INTENT(INOUT) :: du, d2u

    INTEGER            :: ie, shift, i
    INTEGER, PARAMETER :: meth=1


    !Note: derivatives are passed to take adantage of vector wavelet transform.  
    !They can be done in this subroutine as well.

    IF( mdl_form == 0 ) THEN        !pure convection of Ilm and Imm
       !
       ! Ilm
       !
       shift=(n_var_Ilm-1)*ng
       ie = n_var_Ilm
       user_rhs(shift+1:shift+ng) = - u_integrated(:,1)*du(ie,:,1) &
                                    - u_integrated(:,2)*du(ie,:,2) &
                                    - u_integrated(:,3)*du(ie,:,3)  + nuI(:)*SUM(d2u(n_var_Ilm,:,:),2) 
       !
       ! Imm
       !
       shift=(n_var_Imm-1)*ng
       ie = n_var_Imm
       user_rhs(shift+1:shift+ng) = - u_integrated(:,1)*du(ie,:,1) &
                                    - u_integrated(:,2)*du(ie,:,2) &
                                    - u_integrated(:,3)*du(ie,:,3)  + nuI(:)*SUM(d2u(n_var_Imm,:,:),2) 
    ELSE IF( mdl_form == 1 ) THEN  !EXPLICIT
       !
       ! Ilm
       !
       shift=(n_var_Ilm-1)*ng
       ie = n_var_Ilm
       user_rhs(shift+1:shift+ng) = - u_integrated(:,1)*du(ie,:,1) &
                                    - u_integrated(:,2)*du(ie,:,2) &
                                    - u_integrated(:,3)*du(ie,:,3)  + sgs_mdl_force(:,ie) + nuI(:)*SUM(d2u(ie,:,:),2) 
       !
       ! Imm
       !
       shift=(n_var_Imm-1)*ng
       ie = n_var_Imm
       user_rhs(shift+1:shift+ng) = - u_integrated(:,1)*du(ie,:,1) &
                                    - u_integrated(:,2)*du(ie,:,2) &
                                    - u_integrated(:,3)*du(ie,:,3)  + sgs_mdl_force(:,ie) + nuI(:)*SUM(d2u(ie,:,:),2) 
    ELSE IF ( mdl_form == 2 ) THEN  !implicit with 1/T=const 
       ! sgs_mdl_force(:,n_var_Ilm) == LijMij
       ! sgs_mdl_force(:,n_var_Imm) == MijMi
       ! one_T(:) = 1/(delta^(1/4) *theta) * (Ilm Imm )^1/8 ( = 1/T ) for deltaMij=.FALSE.
       ! one_T(:) = 1/(delta*theta) * (Ilm Imm )^1/8 ( = 1/T )        for deltaMij=.TRUE.
       !
       ! Ilm
       !
       shift=(n_var_Ilm-1)*ng
       ie = n_var_Ilm
       user_rhs(shift+1:shift+ng) = - u_integrated(:,1)*du(ie,:,1) &
                                    - u_integrated(:,2)*du(ie,:,2) &
                                    - u_integrated(:,3)*du(ie,:,3) &
                                    +  one_T(:) * (sgs_mdl_force(:,ie) - u_integrated(:,ie) ) + nuI(:)*SUM(d2u(ie,:,:),2) 
       !
       ! Imm
       !
       shift=(n_var_Imm-1)*ng
       ie = n_var_Imm
       user_rhs(shift+1:shift+ng) = - u_integrated(:,1)*du(ie,:,1) &
                                    - u_integrated(:,2)*du(ie,:,2) &
                                    - u_integrated(:,3)*du(ie,:,3) &
                                    + one_T(:) * (sgs_mdl_force(:,ie) - u_integrated(:,ie) ) + nuI(:)*SUM(d2u(ie,:,:),2) 
    ELSE IF ( mdl_form == 3 ) THEN  !implicit with 1/T variable
       ! sgs_mdl_force(:,n_var_Ilm) == LijMij
       ! sgs_mdl_force(:,n_var_Imm) == MijMij
       ! one_T(:) = 1/(delta^(1/4) *theta)  or  = 1/(delta*theta)
       !
       ! Ilm
       !
       shift=(n_var_Ilm-1)*ng
       user_rhs(shift+1:shift+ng) = - u_integrated(:,1)*du(n_var_Ilm,:,1) &
                                    - u_integrated(:,2)*du(n_var_Ilm,:,2) &
                                    - u_integrated(:,3)*du(n_var_Ilm,:,3) &
                                    + one_T(:) * MAX(0.0_pr, u_integrated(:,n_var_Ilm)*u_integrated(:,n_var_Imm) ) ** (0.125_pr) &
                                    * (sgs_mdl_force(:,n_var_Ilm) - u_integrated(:,n_var_Ilm) ) + nuI(:)*SUM(d2u(n_var_Ilm,:,:),2) 
       !
       ! Imm
       !
       shift=(n_var_Imm-1)*ng
       user_rhs(shift+1:shift+ng) = - u_integrated(:,1)*du(n_var_Imm,:,1) &
                                    - u_integrated(:,2)*du(n_var_Imm,:,2) &
                                    - u_integrated(:,3)*du(n_var_Imm,:,3) &
                                    + one_T(:) * MAX(0.0_pr,u_integrated(:,n_var_Ilm)*u_integrated(:,n_var_Imm) ) ** (0.125_pr) &
                                    * (sgs_mdl_force(:,n_var_Imm) - u_integrated(:,n_var_Imm) ) + nuI(:)*SUM(d2u(n_var_Imm,:,:),2)
    END IF

  END SUBROUTINE LDM_rhs





  ! find Jacobian of Right Hand Side of the problem
  ! u_prev == u_prev_timestep_loc

  SUBROUTINE LDM_Drhs (user_Drhs, u, u_prev, du, d2u, meth)
    IMPLICIT NONE
    INTEGER,                            INTENT(IN)    ::  meth
    !u_prev_timestep passed local to cast it into 2dim array to work with vector derivatives
    REAL (pr), DIMENSION (ng,ne),       INTENT(IN)    :: u, u_prev
    REAL (pr), DIMENSION (n),           INTENT(INOUT) :: user_Drhs
    REAL (pr), DIMENSION (2*ne,ng,dim), INTENT(INOUT) :: du         ! 1st derivatives for u (in du(1:ne,:,:)) and u_prev_timestep (in du(ne+1:2*ne,:,:))
    REAL (pr), DIMENSION (ne  ,ng,dim), INTENT(INOUT) :: d2u        ! 2nd derivatives for u 

    INTEGER :: ie, shift
    !--Form right hand side of Navier--Stokes equations


    IF( mdl_form == 0 .OR. mdl_form == 1  ) THEN
       !
       ! Ilm
       !
       shift=(n_var_Ilm-1)*ng
       ie = n_var_Ilm
       ! Drhs = 
       user_Drhs(shift+1:shift+ng) = - u_prev(:,1)*du(ie,:,1) - u_prev(:,2)*du(ie,:,2) - u_prev(:,3)*du(ie,:,3) &
                                     - u(:,1)*du(ne+ie,:,1)   - u(:,2)*du(ne+ie,:,2)   - u(:,3)*du(ne+ie,:,3) &
                                     + nuI(:)*SUM(d2u(n_var_Ilm,:,:),2)
       !
       ! Imm
       !
       shift=(n_var_Imm-1)*ng
       ie = n_var_Imm
       ! Drhs = 
       user_Drhs(shift+1:shift+ng) = - u_prev(:,1)*du(ie,:,1) - u_prev(:,2)*du(ie,:,2) - u_prev(:,3)*du(ie,:,3) &
                                     - u(:,1)*du(ne+ie,:,1)   - u(:,2)*du(ne+ie,:,2)   - u(:,3)*du(ne+ie,:,3) &
                                     + nuI(:)*SUM(d2u(n_var_Imm,:,:),2)
    ELSE IF ( mdl_form == 2 ) THEN  !implicit
       ! sgs_mdl_force(:,n_var_Ilm) == LijMij
       ! sgs_mdl_force(:,n_var_Imm) == MijMij
       ! one_T(:) = 1/(delta^(1/4) *theta) * (Ilm Imm )^1/8 or
       ! one_T(:) = 1/(delta*theta) * (Ilm Imm )^1/8
       !
       ! Drhs(Ilm) = - u_prev d Ilm/dx_i - u dIlm_prev/ Dx_i 
       !             + ( IlmImm)^(1/8)  /( 8*theta*delta^(1/4) )  &
       !             * ( LijMij*( Ilm/Ilm_prev + Imm/Imm_prev) - 9*Ilm - Ilm_prev*Imm/Imm_prev )         
       !
       ! where, _prev is value from prev time step and all non_prev are perterbations
       ! du(ne+ie,:,1) is du_prev(ie)/dx_1
       
       shift=(n_var_Ilm-1)*ng
       
       ! Drhs = 
       user_Drhs(shift+1:shift+ng) = - u_prev(:,1)*du(n_var_Ilm,:,1) - u_prev(:,2)*du(n_var_Ilm,:,2) - u_prev(:,3)*du(n_var_Ilm,:,3) &
                                     - u(:,1)*du(ne+n_var_Ilm,:,1)   - u(:,2)*du(ne+n_var_Ilm,:,2)   - u(:,3)*du(ne+n_var_Ilm,:,3) &
                                     - one_T(:) * u(:,n_var_Ilm) &
                                     + nuI(:)*SUM(d2u(n_var_Ilm,:,:),2)
       !
       ! Drhs(Imm) = - u_prev d Imm/dx_i - u dImm_prev/ Dx_i 
       !             + Imm*( Imm_prev Imm_prev)^(1/8)  /( 8*theta*delta^(1/4) )  &
       !             * ( 2.0*MijMij/Imm_prev - 10 )         
       !
       ! where, _prev is value from prev time step and all non_prev are perterbations
       ! du(ne+ie,:,1) is du_prev(ie)/dx_1
       shift=(n_var_Imm-1)*ng
       ! Drhs = 
       user_Drhs(shift+1:shift+ng) = - u_prev(:,1)*du(n_var_Imm,:,1) - u_prev(:,2)*du(n_var_Imm,:,2) - u_prev(:,3)*du(n_var_Imm,:,3) &
                                     - u(:,1)*du(ne+n_var_Imm,:,1)   - u(:,2)*du(ne+n_var_Imm,:,2)   - u(:,3)*du(ne+n_var_Imm,:,3) &
                                     - one_T(:) * u(:,n_var_Imm) &
                                     + nuI(:)*SUM(d2u(n_var_Imm,:,:),2)
    ELSE IF ( mdl_form == 3 ) THEN  !implicit
       ! sgs_mdl_force(:,n_var_Ilm) == LijMij
       ! sgs_mdl_force(:,n_var_Imm) == MijMij
       ! one_T(:) = 1/(delta^(1/4) *theta) or 1/(delta*theta)  
       !
       ! Drhs(Ilm) = - u_prev d Ilm/dx_i - u dIlm_prev/ Dx_i 
       !             + ( IlmImm)^(1/8)  /( 8*theta*delta^(1/4) )  &
       !             * ( LijMij*( Ilm/Ilm_prev + Imm/Imm_prev) - 9*Ilm - Ilm_prev*Imm/Imm_prev )         
       !
       ! where, _prev is value from prev time step and all non_prev are perterbations
       ! du(ne+ie,:,1) is du_prev(ie)/dx_1
       shift=(n_var_Ilm-1)*ng
       ! Drhs = 
       user_Drhs(shift+1:shift+ng) = - u_prev(:,1)*du(n_var_Ilm,:,1) - u_prev(:,2)*du(n_var_Ilm,:,2) - u_prev(:,3)*du(n_var_Ilm,:,3) &
                                     - u(:,1)*du(ne+n_var_Ilm,:,1)   - u(:,2)*du(ne+n_var_Ilm,:,2)   - u(:,3)*du(ne+n_var_Ilm,:,3) &
                                     + one_T(:)/8.0_pr*MAX(0.0_pr,u_prev(:,n_var_Ilm)*u_prev(:,n_var_Imm))**(0.125_pr) &
                                     * (  sgs_mdl_force(:,n_var_Ilm)*( u(:,n_var_Ilm)/u_prev(:,n_var_Ilm) + u(:,n_var_Imm)/u_prev(:,n_var_Imm) ) &
                                     - 9.0_pr * u(:,n_var_Ilm) - u_prev(:,n_var_Ilm) * u(:,n_var_Imm)/u_prev(:,n_var_Imm) ) &
                                     + nuI(:)*SUM(d2u(n_var_Ilm,:,:),2)
       !
       ! Drhs(Imm) = - u_prev d Imm/dx_i - u dImm_prev/ Dx_i 
       !             + Imm*( Imm_prev Imm_prev)^(1/8)  /( 8*theta*delta^(1/4) )  &
       !             * ( 2.0*MijMij/Imm_prev - 10 )         
       !
       ! where, _prev is value from prev time step and all non_prev are perterbations
       ! du(ne+ie,:,1) is du_prev(ie)/dx_1
       shift=(n_var_Imm-1)*ng
       ! Drhs = 
       user_Drhs(shift+1:shift+ng) = - u_prev(:,1)*du(n_var_Imm,:,1) - u_prev(:,2)*du(n_var_Imm,:,2) - u_prev(:,3)*du(n_var_Imm,:,3) &
                                     - u(:,1)*du(ne+n_var_Imm,:,1)   - u(:,2)*du(ne+n_var_Imm,:,2)   - u(:,3)*du(ne+n_var_Imm,:,3) &
                                     + one_T(:)/8.0_pr*MAX(0.0_pr, u_prev(:,n_var_Ilm)*u_prev(:,n_var_Imm))**(0.125_pr)  &
                                     * (  sgs_mdl_force(:,n_var_Imm)*( u(:,n_var_Ilm)/u_prev(:,n_var_Ilm) + u(:,n_var_Imm)/u_prev(:,n_var_Imm) ) &
                                     - 9.0_pr * u(:,n_var_Imm) - u_prev(:,n_var_Imm) * u(:,n_var_Ilm)/u_prev(:,n_var_Ilm) ) &
                                     + nuI(:)*SUM(d2u(n_var_Imm,:,:),2)
    END IF
  END SUBROUTINE LDM_Drhs





  !
  ! Uses u_prev_timestep, which is global flat (1D ) array set in time_step_cn()
  !
  SUBROUTINE LDM_Drhs_diag (user_Drhs_diag, du_prev_timestep, du, d2u, meth)
    IMPLICIT NONE
    INTEGER,                          INTENT(IN)    :: meth
    REAL (pr), DIMENSION (n),         INTENT(INOUT) :: user_Drhs_diag
    REAL (pr), DIMENSION (ne,ng,dim), INTENT(INOUT) :: du_prev_timestep
    REAL (pr), DIMENSION (ng,dim),    INTENT(INOUT) :: du, d2u

    INTEGER :: ie, shift,shiftIlm,shiftImm
    
    
    IF( mdl_form == 0 .OR. mdl_form == 1 ) THEN
       !
       ! Ilm
       !
       shift=(n_var_Ilm-1)*ng
       ie = n_var_Ilm
       ! Drhs = 
       user_Drhs_diag(shift+1:shift+ng) = - u_prev_timestep(1:ng)*du(:,1) &
                                          - u_prev_timestep(ng+1:2*ng)*du(:,2) &
                                          - u_prev_timestep(2*ng+1:3*ng)*du(:,3) &
                                          + nuI(:)*SUM(d2u,2)
       !
       ! Imm
       !
       shift=(n_var_Imm-1)*ng
       ie = n_var_Imm
       ! Drhs = 
       user_Drhs_diag(shift+1:shift+ng) = - u_prev_timestep(1:ng)*du(:,1) &
                                          - u_prev_timestep(ng+1:2*ng)*du(:,2) &
                                          - u_prev_timestep(2*ng+1:3*ng)*du(:,3) &
                                          + nuI(:)*SUM(d2u,2)
    ELSE IF ( mdl_form == 2 ) THEN  !explicit
       ! sgs_mdl_force(:,n_var_Ilm) == LijMij
       ! sgs_mdl_force(:,n_var_Imm) == MijMij
       ! one_T(:) = 1/(delta^(1/4) *theta) * (Ilm Imm )^1/8
       shiftIlm=(n_var_Ilm-1)*ng
       shiftImm=(n_var_Imm-1)*ng
       !
       ! Ilm
       !
       user_Drhs_diag(shiftIlm+1:shiftIlm+ng) = - u_prev_timestep(1:ng)*du(:,1) &
                                                - u_prev_timestep(ng+1:2*ng)*du(:,2) &
                                                - u_prev_timestep(2*ng+1:3*ng)*du(:,3) &
                                                - one_T(:) + nuI(:)*SUM(d2u,2)
       !
       ! Ilm
       !
       user_Drhs_diag(shiftImm+1:shiftImm+ng) = - u_prev_timestep(1:ng)*du(:,1) &
                                                - u_prev_timestep(ng+1:2*ng)*du(:,2) &
                                                - u_prev_timestep(2*ng+1:3*ng)*du(:,3) &
                                                - one_T(:) + nuI(:)*SUM(d2u,2)
    ELSE IF ( mdl_form == 3 ) THEN  !explicit
       ! sgs_mdl_force(:,n_var_Ilm) == LijMij
       ! sgs_mdl_force(:,n_var_Imm) == MijMij
       ! one_T(:) = 1/(delta^(1/4) *theta) 
       shiftIlm=(n_var_Ilm-1)*ng
       shiftImm=(n_var_Imm-1)*ng
       !
       ! Ilm
       !
       ! -u_i*d_diag + 1/(8*theta*delta^1/4) (IlmImm)^1/8(LijMij/Ilm - 9 )
       !
       user_Drhs_diag(shiftIlm+1:shiftIlm+ng) = - u_prev_timestep(1:ng)*du(:,1) &
                                                - u_prev_timestep(ng+1:2*ng)*du(:,2) &
                                                - u_prev_timestep(2*ng+1:3*ng)*du(:,3) &
                                                + one_T(:)/8.0_pr &
                                                * MAX(0.0_pr, u_prev_timestep(shiftIlm+1:shiftIlm+ng)*u_prev_timestep(shiftImm+1:shiftImm+ng))**(0.125_pr)  &
                                                * (  sgs_mdl_force(:,n_var_Ilm)/u_prev_timestep(shiftIlm+1:shiftIlm+ng)  - 9.0_pr ) &
                                                + nuI(:)*SUM(d2u,2)
       !
       ! Ilm
       !
       ! -u_i*d_diag + 1/(8*theta*delta^1/4) (IlmImm)^1/8( 2 MijMij/Imm - 10 )
       !
       user_Drhs_diag(shiftImm+1:shiftImm+ng) = - u_prev_timestep(1:ng)*du(:,1) &
                                                - u_prev_timestep(ng+1:2*ng)*du(:,2) &
                                                - u_prev_timestep(2*ng+1:3*ng)*du(:,3) &
                                                + one_T(:)/8.0_pr &
                                                * MAX(0.0_pr, u_prev_timestep(shiftIlm+1:shiftIlm+ng)*u_prev_timestep(shiftImm+1:shiftImm+ng))**(0.125_pr)  &
                                                * (  sgs_mdl_force(:,n_var_Imm)/u_prev_timestep(shiftImm+1:shiftImm+ng)  - 9.0_pr ) &
                                                + nuI(:)*SUM(d2u,2)
    END IF
  END SUBROUTINE LDM_Drhs_diag





  !
  ! find turbulent eddy viscosity for dynamic Smagorinsky  model using 
  ! Meneveau's langrangian model avereraging
  !
  ! In this routine we calculate nu_t based on the current value of Ilm, Imm which are convected scalars
  ! and calculate the RHS forcing for the convection of Ilm, Imm for the next time step.
  !
  ! dynCs -result 
  ! s_ij  - sij based on u>eps+adj
  ! nwlt
  ! init_flag - true if we are initializing the model
  !
  ! Globals accessed
  !  Cs - Cs = the Averaged Dynamic Coeff (For either sgsmodel case)
  !  sgsmodel_coef1 - Cs is multiplied by sgsmodel_coef1 (if sgsmodel_coef1 /= 0.0)
  !  u(:,n_var) field variables, uvw, Ilm, Imm, pressure
  !
  SUBROUTINE sgs_LDM_model(u_in,  tau_ij, nu_t,  nlocal, init_flag)
    USE util_vars        ! dA 
    USE parallel
    IMPLICIT NONE
    INTEGER,                                    INTENT (IN)    :: nlocal
    LOGICAL,                                    INTENT (IN)    :: init_flag  ! true if we are initializing the model
    REAL (pr), DIMENSION (nlocal,n_integrated), INTENT (INOUT) :: u_in
    REAL (pr), DIMENSION (1:nlocal,6),          INTENT (INOUT) :: tau_ij
    REAL (pr), DIMENSION (1:nlocal),            INTENT (INOUT) :: nu_t       ! dynamic Smagorinsky coeffient

    REAL (pr), DIMENSION (1:nlocal,6)  :: s_ij 
    REAL (pr), DIMENSION (1:nlocal)    :: Smod, Smod2
    REAL (pr), DIMENSION (1:nlocal,6)  :: SM, L 
    REAL (pr)                          :: rescale_tau_ij, eps_scale, rescale_Ilm, tmp1, tmp2, tmp3, tmp4
    INTEGER                            :: i, ie, ii, itmp1, itmp2
    INTEGER, PARAMETER                 :: n_filt_repeat = 1             ! Oleg, I was experimenting with this at CTR and forgot to change to 1


    !debug
    IF( BTEST(debug_level,1) ) THEN
       IF (par_rank.EQ.0) THEN
          IF (par_rank.EQ.0) WRITE(*,'(A)') "  "
          IF (par_rank.EQ.0) WRITE(*,'(A)') "Debug In sgs_LDM_model()" 
       END IF
       DO i=1,n_integrated
          tmp1 = MINVAL(u_in(:,i)); tmp2 = MAXVAL(u_in(:,i))
          CALL parallel_global_sum (REALMINVAL=tmp1)
          CALL parallel_global_sum (REALMAXVAL=tmp2)
          IF (par_rank.EQ.0) WRITE(*,'(A,2(e15.7,1x))') "MINMAXVAL(u_in(:,i)) = ", tmp1, tmp2
       END DO
       IF (par_rank.EQ.0) THEN
          IF (par_rank.EQ.0) WRITE(*,'(A)') "END Debug In sgs_LDM_model()"
          IF (par_rank.EQ.0) WRITE(*,'(A)') "  "
       END IF
    ENDIF

    IF( dim /= 3 ) THEN
       IF (par_rank.EQ.0) PRINT *, 'Error, Dynamic Sagorinsky del is only defined for 3D simulations'
       IF (par_rank.EQ.0) PRINT *, 'Exiting...'
       CALL parallel_finalize; STOP
    END IF

    IF(sgsmodel /= sgsmodel_LDM ) THEN !problem
       IF (par_rank.EQ.0) PRINT *,'sgsmodel should be 4 to call sgs_LDM_model()'
       IF (par_rank.EQ.0) PRINT *, 'Exiting...'
       CALL parallel_finalize; STOP
    END IF

    IF (verb_level.GT.0 .AND. par_rank.EQ.0) THEN
       PRINT *,' sgs_LDM_model(), LDMtheta = ', LDMtheta
       PRINT *,' In sgs_LDM_model() '
    END IF

    CALL sgs_Smagorinsky_model(u_in(1:nlocal,1:dim), tau_ij, L, S_ij, nu_t, Smod, Smod2, nlocal)
    !
    ! initialize Ilm = LijMij and Imm = MijMij
    !
    IF( init_flag ) THEN
       u_in(:,n_var_Ilm) = L(:,1) !initialize Ilm = LijMij only to calculate Cs average for first time step
       ! below Ilm is set to Ilm = Cs * Imm 
       IF (verb_level.GT.0 .AND. par_rank.EQ.0) PRINT *, '....... initialized Imm to a its average'
!       u_in(:,n_var_Imm) = L(:,2) ! initialize Imm = MijMij
       tmp1 = SUM(dA*L(:,2))
       CALL parallel_global_sum( REAL= tmp1 )
       u_in(:,n_var_Imm) = tmp1 / sumdA_global ! initialize Imm to a its average
    END IF

    !
    ! find dynCs = Ilm/ Imm 
    ! 
    ! in either case calculate the averaged value of Cs
    ! to be logged in __.case_turb.log
    
    ! In first iteration this is really LijMij/MijMij
    ! but in subsequent iteration it will be Ilm/Imm
    tmp1 = SUM(dA*u_in(:,n_var_Ilm) )
    tmp2 = SUM(dA*u_in(:,n_var_Imm))
    CALL parallel_global_sum( REAL=tmp1 )
    CALL parallel_global_sum( REAL=tmp2 )
    Cs =  0.5_pr* tmp1 / tmp2

    IF (verb_level.GT.0 .AND. par_rank.EQ.0) WRITE(*,'(A,1x,e15.7)') " Dynamic Smagorinski Coeff = ", Cs
    
    IF(  init_flag .AND. sgsmodel_coef1 /= 0.0_pr ) THEN
       Cs =   sgsmodel_coef1
       IF (verb_level.GT.0 .AND. par_rank.EQ.0) THEN
           WRITE(*,'(A)')       " sgs_LDM_model()  Setting  Cs     =   sgsmodel_coef1 "
           WRITE(*,'(A,e15.7)') " sgs_LDM_model()  sgsmodel_coef1  = ", sgsmodel_coef1
           WRITE(*,'(A,e15.7)') " sgs_LDM_model()  new Cs          = ", Cs  
       END IF
    END IF
    
    IF( clip_Cs ) THEN
       Cs =  MAX(Cs , 0.0_pr )
       IF (verb_level.GT.0 .AND. par_rank.EQ.0) PRINT *, 'Cs =  MAX(Cs , 0.0_pr ) '
    END IF

    IF( init_flag ) THEN
       ! initialize Ilm = Cseps^2 * Imm 
       u_in(:,n_var_Ilm) = 2.0_pr* Cs * u_in(:,n_var_Imm)
       !TEST
       !Cs_tmp =  0.5_pr* SUM(dA*u_in(:,n_var_Ilm) ) / SUM(dA*u_in(:,n_var_Imm))
       !PRINT *,'Dynamic SmagorinskyCoeff, based on  u_in(:,n_var_Ilm) = Cs * u_in(:,n_var_Imm) = ', Cs_tmp
    END IF

    !
    ! clipping Ilm < 0.0 to 0.0
    !
    IF (verb_level.GT.0) THEN
       tmp1 = MINVAL( u_in(:,n_var_Ilm) )
       tmp2 = MAXVAL( u_in(:,n_var_Ilm) )
       CALL parallel_global_sum( REALMINVAL=tmp1 )
       CALL parallel_global_sum( REALMAXVAL=tmp2 )
       IF (par_rank.EQ.0) PRINT *,' MINMAX Ilm  = ', tmp1, tmp2
    
       tmp1 = MINVAL( u_in(:,n_var_Imm) )
       tmp2 = MAXVAL( u_in(:,n_var_Imm) )
       CALL parallel_global_sum( REALMINVAL=tmp1 )
       CALL parallel_global_sum( REALMAXVAL=tmp2 )
       IF (par_rank.EQ.0) PRINT *,' MINMAX Imm  = ', tmp1, tmp2
    
       itmp1 = count(u_in(:,n_var_Ilm) < 0.0_pr)
       itmp2 = count(u_in(:,n_var_Imm) < 0.0_pr)
       CALL parallel_global_sum( INTEGER=itmp1 )
       CALL parallel_global_sum( INTEGER=itmp2 )
       IF (par_rank.EQ.0) PRINT *, ' no.times Ilm<0  = ', itmp1
       IF (par_rank.EQ.0) PRINT *, ' no.times Imm<0  = ', itmp2
    END IF

    !
    ! here we need |S_(on adaptive grid)|
    ! if we need |S| with Sij based on u_in
    ! IF( mdl_filt_type_grid == 0 ) then Smod has what we need from
    ! above. If not we use already calculated one.
    IF( mdl_filt_type_grid /= 0 .AND. .NOT. DynSmodGridFilt ) THEN
       Smod(:) = nu_t(:)
    END IF

    !  PRINT *,'clipping Ilm < 0.0 to 0.0 '
    !  u_in(:,n_var_Ilm) = MAX( u_in(:,n_var_Ilm) , 0.0_pr ) ! clip all LijMij < 0.0 to 0.0 

    nu_t(:) =  0.5_pr * u_in(:,n_var_Ilm)/ u_in(:,n_var_Imm) !- is different from GDM since definition of Mij is different
    !       IF( sgsmodel_coef1 /= 0.0_pr ) THEN
    !          nu_t(:) =   nu_t(:) * sgsmodel_coef1
    !          PRINT *, 'sgs_GDM_model() Setting  Clocal(:) =  Clocal(:) * sgsmodel_coef1 '
    !       END IF
    IF( clip_Cs ) THEN
       nu_t(:) =  MAX( nu_t(:) , 0.0_pr )

       IF (verb_level.GT.0) THEN
          itmp1 = COUNT( nu_t(:) < 0.0_pr )
          CALL parallel_global_sum( INTEGER=itmp1 )
          IF (par_rank.EQ.0) THEN
             PRINT *, ' Number of points in Clocal to be clipped : ', itmp1       
             PRINT *, ' Cs_local =  MAX(Cs_local , 0.0_pr ) '
          END IF   
       END IF
    END IF
    

    ! Now calculate nu_t
    !  Cs * ||S||
    IF( init_flag ) Then
       IF (verb_level.GT.0 .AND. par_rank.EQ.0) PRINT * ,'>>>Averaged Cs used<<<'
       nu_t(:) =  Cs * Smod(:) !averaged nu_t
       IF( deltaMij ) nu_t(:) =  delta(:)**2 * nu_t(:)
    ELSE
       ! only for output
       tmp1 = SUM(dA*nu_t(:))
       CALL parallel_global_sum( REAL=tmp1 )
       Cs = tmp1 / sumdA_global; 

       IF (verb_level.GT.0) THEN
          ! non-averaged Cs case
          tmp1 = MINVAL (nu_t(:) )
          tmp2 = MAXVAL (nu_t(:) )
          CALL parallel_global_sum( REALMINVAL=tmp1 )
          CALL parallel_global_sum( REALMAXVAL=tmp2 )       
          IF (par_rank.EQ.0) THEN
             PRINT * ,'>>>Local Cs used<<<'
             PRINT *, ' Averaged nu_t = ', Cs
             PRINT *, ' MINVAL local Cs', tmp1, tmp2
          END IF
       END IF
       
       nu_t(:) =  nu_t(:) * Smod(:)
       IF( deltaMij ) nu_t(:) =  delta(:)**2 * nu_t(:)
    END IF
    
    !
    ! clip nu + nu_t(:) if called for
    !
    IF( clip_nu_nut ) THEN
       nu_t(:) =  MAX( nu_t(:), -nu )

       IF (verb_level.GT.0) THEN
          itmp1 = COUNT( nu + nu_t(:) < 0.0_pr )
          CALL parallel_global_sum( INTEGER=itmp1 )
          IF (par_rank.EQ.0) PRINT *, ' Dyn_Smag number of points of (nu +  nu_t(:)) < 0.0  = ', itmp1
       
          tmp1 = MINVAL (nu_t(:) )
          tmp2 = MAXVAL (nu_t(:) )
          CALL parallel_global_sum( REALMINVAL=tmp1 )
          CALL parallel_global_sum( REALMAXVAL=tmp2 )
          IF (par_rank.EQ.0) PRINT *, ' nu_t after clipping: MINMAXVAL (nu_t(:) )           = ', tmp1, tmp2
       END IF
    END IF

    DO ie = 1, 6
       tau_ij(:,ie) = 2.0_pr * nu_t(:) * S_ij(:,ie) !SGS stress
    END DO

    !
    ! find SGS dissipation: -tauij Sij and store in smod2(:)
    !
    smod2(:) =            tau_ij(:,1)*s_ij(:,1) + tau_ij(:,2)*s_ij(:,2) + tau_ij(:,3)*s_ij(:,3) &
              + 2.0_pr *( tau_ij(:,4)*s_ij(:,4) + tau_ij(:,5)*s_ij(:,5) + tau_ij(:,6)*s_ij(:,6))

    ! calculate total SGS diss
    total_sgs_diss = SUM( dA* smod2 )
    CALL parallel_global_sum( REAL=total_sgs_diss )
! to use rescaling this code needs to be moved below!!! or modified...
    IF( do_const_diss .AND. model_rescale) THEN
       CALL const_SGS_dissipation (rescale_tau_ij, eps_scale, Smod, nlocal)    !Giuliano

       nu_t(:) = rescale_tau_ij * nu_t(:)
       tau_ij = rescale_tau_ij * tau_ij
       !
       ! find SGS dissipation: -tauij Sij and store in smod2(:)
       !
       smod2(:) =            tau_ij(:,1)*s_ij(:,1) + tau_ij(:,2)*s_ij(:,2) + tau_ij(:,3)*s_ij(:,3) &
                 + 2.0_pr *( tau_ij(:,4)*s_ij(:,4) + tau_ij(:,5)*s_ij(:,5) + tau_ij(:,6)*s_ij(:,6))
       
       ! calculate total SGS diss
       total_sgs_diss = SUM( dA* smod2 ) 
       CALL parallel_global_sum( REAL=total_sgs_diss )

       rescale_Ilm = Ilm_gain*(eps_scale -1.0_pr ) + 1.0_pr
       
       ! rescale Ilm
       u_in(:,n_var_Ilm) = rescale_Ilm * u_in(:,n_var_Ilm)
          
       sgs_mdl_force(:,n_var_Ilm) = rescale_Ilm * sgs_mdl_force(:,n_var_Ilm)

       IF (verb_level.GT.0 .AND. par_rank.EQ.0) THEN
           WRITE(*,'(A)')         ' Rescaling nu_t & tau_ij'
           WRITE(*,'(A, e15.5 )') ' Re-Calculate total_sgs_diss = ', total_sgs_diss
           WRITE(*,'(A, e15.5 )') ' % SGS diss (new)            = ', total_sgs_diss/(total_resolved_diss+total_sgs_diss)
           PRINT*,                ' Ilm_gain                    = ',Ilm_gain
           PRINT*,                ' rescale_Ilm = Ilm_gain(eps_scale -1.0_pr ) + 1.0_pr  = ',rescale_Ilm
           WRITE(*,'(A)')         ' Rescaling u_in(:,n_var_Ilm)'
           WRITE(*,'(A)')         ' Rescaling sgs_mdl_force(:,n_var_Ilm)'
       END IF
     END IF

    !
    ! artificial viscosity for Ilm & Imm evolution
    !
    nuI(:) = CI*delta(:)**2*Smod(:)
    
    IF (verb_level.GT.0) THEN
       tmp1 = MINVAL( nuI(:) )
       tmp2 = MAXVAL( nuI(:) )
       CALL parallel_global_sum( REALMINVAL=tmp1 )
       CALL parallel_global_sum( REALMAXVAL=tmp2 )    
       IF (par_rank.EQ.0) PRINT *,'MINMAX nuI  = ', tmp1, tmp2
    END IF


    ! find 1/T = 1/(delta^(1/4)*theta) or ...  (store in delta)
    IF ( deltaMij ) THEN
       one_T(:) =  1.0_pr/(LDMtheta*delta(:))
    ELSE
       one_T(:) =  1.0_pr/(LDMtheta*delta(:)**0.25_pr)
    END IF
    
    IF (verb_level.GT.0) THEN
       tmp1 = MINVAL( one_T(:) )
       tmp2 = MAXVAL( one_T(:) )
       CALL parallel_global_sum( REALMINVAL=tmp1 )
       CALL parallel_global_sum( REALMAXVAL=tmp2 )    
       IF (par_rank.EQ.0) PRINT *,'MINMAX 1/T  = ', tmp1, tmp2
    END IF

    !************************
    !     
    ! Set RHS forcing for evolution of model scalars Ilm Imm based on current LijMij and MijMij
    !
    IF( mdl_form == 1 ) THEN

       ! RHS( Ilm ) = 1/(delta^(1/4) *theta) *(Ilm Imm)^(1/8) (LijMij - Ilm)
       !
       sgs_mdl_force(:,n_var_Ilm) =   one_T(:) &
                                    * (MAX(u_in(:,n_var_Ilm)*u_in(:,n_var_Imm),0.0_pr))**(0.125_pr) &
                                    *( L(:,1)-u_in(:,n_var_Ilm))
       
       ! RHS( Imm ) = 1/(delta^(1/4)*theta) *(Imm Imm)^(1/8)  (MijMij - Imm)
       !
       sgs_mdl_force(:,n_var_Imm) =   one_T(:) &
                                    * (MAX(u_in(:,n_var_Ilm)*u_in(:,n_var_Imm),0.0_pr))**(0.125_pr) &
                                    * ( L(:,2)-u_in(:,n_var_Imm))
       
    ELSE IF ( mdl_form == 2 ) THEN
       
       ! 1/(delta^(1/4) *theta) * (Ilm Imm )^1/8
       one_T(:) = one_T(:) * MAX(0.0_pr,u_in(:,n_var_Ilm)*u_in(:,n_var_Imm))**(0.125_pr)
       

       IF (verb_level.GT.0) THEN
          itmp1 = COUNT(one_T(:)==0.0_pr)
          itmp2 = COUNT(one_T(:)>0.0_pr)
          CALL parallel_global_sum( INTEGER=itmp1 )
          CALL parallel_global_sum( INTEGER=itmp2 )
          IF (par_rank.EQ.0) PRINT *, ' COUNT 1/T  = ', itmp1, itmp2
       
          tmp1 = MINVAL( one_T(:) )
          tmp2 = MAXVAL( one_T(:) )
          CALL parallel_global_sum( REALMINVAL=tmp1 )
          CALL parallel_global_sum( REALMAXVAL=tmp2 )
          IF (par_rank.EQ.0) WRITE(*,'("(atg set RHS forcing) minmaxval delta ",2(e15.7 , 1X))' ) tmp1, tmp2            
       END IF
       
       
       sgs_mdl_force(:,n_var_Ilm) = L(:,1) ! LijMij
       sgs_mdl_force(:,n_var_Imm) = L(:,2) ! MijMij
       WHERE( u_in(:,n_var_Ilm) <= 0.0_pr ) sgs_mdl_force(:,n_var_Ilm) = MAX(0.0_pr,sgs_mdl_force(:,n_var_Ilm))
       
       
       IF(SpaceTimeAvg) THEN
          IF (verb_level.GT.0) THEN
             tmp1 = MINVAL( sgs_mdl_force(:,n_var_Ilm) )
             tmp2 = MAXVAL( sgs_mdl_force(:,n_var_Ilm) )
             tmp3 = MINVAL( sgs_mdl_force(:,n_var_Imm) )
             tmp4 = MAXVAL( sgs_mdl_force(:,n_var_Imm) )
             CALL parallel_global_sum( REALMINVAL=tmp1 )
             CALL parallel_global_sum( REALMAXVAL=tmp2 )             
             CALL parallel_global_sum( REALMINVAL=tmp3 )
             CALL parallel_global_sum( REALMAXVAL=tmp4 )             
             IF (par_rank.EQ.0) THEN
                PRINT *,'------------------------- Before Filtering -----------------------------------'
                PRINT *,'MINMAX sgs_mdl_force(:,n_var_Ilm): ', tmp1, tmp2
                  !MINVAL( sgs_mdl_force(:,n_var_Ilm) ), MAXVAL( sgs_mdl_force(:,n_var_Ilm) )
                PRINT *,'MINMAX sgs_mdl_force(:,n_var_Imm): ', tmp3, tmp4
                  !MINVAL( sgs_mdl_force(:,n_var_Imm) ), MAXVAL( sgs_mdl_force(:,n_var_Imm) )
                PRINT *,'------------------------------------------------------------------------------'; PRINT *,''
             END IF
          END IF
          
         
          DO i =1 , n_filt_repeat
             CALL local_lowpass_filt(sgs_mdl_force(:,n_var_Ilm:n_var_Imm), j_lev, nlocal, 2, 1, 2, &
                  lowpassfilt_support,lowpass_filt_type, tensorial_filt )
          END DO
         
          
          IF (verb_level.GT.0) THEN
             tmp1 = MINVAL( sgs_mdl_force(:,n_var_Ilm) )
             tmp2 = MAXVAL( sgs_mdl_force(:,n_var_Ilm) )
             tmp3 = MINVAL( sgs_mdl_force(:,n_var_Imm) )
             tmp4 = MAXVAL( sgs_mdl_force(:,n_var_Imm) )
             CALL parallel_global_sum( REALMINVAL=tmp1 )
             CALL parallel_global_sum( REALMAXVAL=tmp2 )             
             CALL parallel_global_sum( REALMINVAL=tmp3 )
             CALL parallel_global_sum( REALMAXVAL=tmp4 )             
             IF (par_rank.EQ.0) THEN
                PRINT *,'------------------------- After  Filtering -----------------------------------'
                PRINT *,'MINMAX sgs_mdl_force(:,n_var_Ilm): ', tmp1, tmp2
                  !MINVAL( sgs_mdl_force(:,n_var_Ilm) ), MAXVAL( sgs_mdl_force(:,n_var_Ilm) )
                PRINT *,'MINMAX sgs_mdl_force(:,n_var_Imm): ', tmp3, tmp4
                  !MINVAL( sgs_mdl_force(:,n_var_Imm) ), MAXVAL( sgs_mdl_force(:,n_var_Imm) )
                PRINT *,'------------------------------------------------------------------------------'; PRINT *,''
             END IF
          END IF
          
       END IF
       
       
    ELSE IF ( mdl_form == 3 ) THEN
       sgs_mdl_force(:,n_var_Ilm) = tau_ij(:,1) ! LijMij
       sgs_mdl_force(:,n_var_Imm) = tau_ij(:,2) ! MijMij
       WHERE( u_in(:,n_var_Ilm) <= 0.0_pr ) sgs_mdl_force(:,n_var_Ilm) = MAX(0.0_pr,sgs_mdl_force(:,n_var_Ilm))
       
       IF(SpaceTimeAvg) THEN
          IF (verb_level.GT.0) THEN
             tmp1 = MINVAL( sgs_mdl_force(:,n_var_Ilm) )
             tmp2 = MAXVAL( sgs_mdl_force(:,n_var_Ilm) )
             tmp3 = MINVAL( sgs_mdl_force(:,n_var_Imm) )
             tmp4 = MAXVAL( sgs_mdl_force(:,n_var_Imm) )
             CALL parallel_global_sum( REALMINVAL=tmp1 )
             CALL parallel_global_sum( REALMAXVAL=tmp2 )             
             CALL parallel_global_sum( REALMINVAL=tmp3 )
             CALL parallel_global_sum( REALMAXVAL=tmp4 )             
             IF (par_rank.EQ.0) THEN
                PRINT *,'------------------------- Before Filtering -----------------------------------'
                PRINT *,'MINMAX sgs_mdl_force(:,n_var_Ilm): ', tmp1, tmp2
                  !MINVAL( sgs_mdl_force(:,n_var_Ilm) ), MAXVAL( sgs_mdl_force(:,n_var_Ilm) )
                PRINT *,'MINMAX sgs_mdl_force(:,n_var_Imm): ', tmp3, tmp4
                  !MINVAL( sgs_mdl_force(:,n_var_Imm) ), MAXVAL( sgs_mdl_force(:,n_var_Imm) )
                PRINT *,'------------------------------------------------------------------------------'; PRINT *,''
             END IF
          END IF
          
          
          DO i =1 , n_filt_repeat
             CALL local_lowpass_filt(sgs_mdl_force(:,n_var_Ilm:n_var_Imm), j_lev, nlocal, 2, 1, 2, &
                  lowpassfilt_support,lowpass_filt_type, tensorial_filt )
          END DO
          
          
          IF (verb_level.GT.0) THEN
             tmp1 = MINVAL( sgs_mdl_force(:,n_var_Ilm) )
             tmp2 = MAXVAL( sgs_mdl_force(:,n_var_Ilm) )
             tmp3 = MINVAL( sgs_mdl_force(:,n_var_Imm) )
             tmp4 = MAXVAL( sgs_mdl_force(:,n_var_Imm) )
             CALL parallel_global_sum( REALMINVAL=tmp1 )
             CALL parallel_global_sum( REALMAXVAL=tmp2 )             
             CALL parallel_global_sum( REALMINVAL=tmp3 )
             CALL parallel_global_sum( REALMAXVAL=tmp4 )             
             IF (par_rank.EQ.0) THEN
                PRINT *,'------------------------- After  Filtering -----------------------------------'
                PRINT *,'MINMAX sgs_mdl_force(:,n_var_Ilm): ', tmp1, tmp2
                  !MINVAL( sgs_mdl_force(:,n_var_Ilm) ), MAXVAL( sgs_mdl_force(:,n_var_Ilm) )
                PRINT *,'MINMAX sgs_mdl_force(:,n_var_Imm): ', tmp3, tmp4
                  !MINVAL( sgs_mdl_force(:,n_var_Imm) ), MAXVAL( sgs_mdl_force(:,n_var_Imm) )
                PRINT *,'------------------------------------------------------------------------------'; PRINT *,''
             END IF
          END IF
          
       END IF
    END IF
 
  END SUBROUTINE sgs_LDM_model
  !****************************************************************************************
  !*                                   END of LDM                                         *
  !****************************************************************************************





  !****************************************************************************************
  !*                                 KOMET Model (11:KOMET)
  !****************************************************************************************

  SUBROUTINE KOMET_rhs (user_rhs, u_integrated, du, d2u)
    !--Form rhs for both k and ktau equations
    IMPLICIT NONE
    REAL (pr), DIMENSION (ng,ne),     INTENT(IN)    :: u_integrated !1D flat version of integrated variables without BC 
    REAL (pr), DIMENSION (n),         INTENT(INOUT) :: user_rhs
    REAL (pr), DIMENSION (ne,ng,dim), INTENT(INOUT) :: du, d2u
    REAL (pr), DIMENSION (ng,dim)                   :: duk

    INTEGER            :: shift, shift1, idim
    INTEGER, PARAMETER :: meth=1

    REAL (pr)  :: tmp1, tmp2

    ! Note: derivatives are passed to take adantage of vector wavelet transform
    ! They can be done in this subroutine as well

    ! evaluating nu_t = Ksgs*Ktau
    nutK(:) = u_integrated(:,n_var_K)*u_integrated(:,n_var_KD)

    IF (verb_level.GT.0) THEN
!       tmp1 = MINVAL( sgs_mdl_force(:,n_var_K) )
!       tmp2 = MAXVAL( sgs_mdl_force(:,n_var_K) )
!       CALL parallel_global_sum( REALMINVAL=tmp1 )
!       CALL parallel_global_sum( REALMAXVAL=tmp2 )
!       IF (par_rank.EQ.0) PRINT *,'MINMAX_sgs_mdl_force(:,n_var_K) inKOMET_rhs=', tmp1, tmp2
!       tmp1 = MINVAL( sgs_mdl_force(:,n_var_KD) )
!       tmp2 = MAXVAL( sgs_mdl_force(:,n_var_KD) )
!       CALL parallel_global_sum( REALMINVAL=tmp1 )
!       CALL parallel_global_sum( REALMAXVAL=tmp2 )
!       IF (par_rank.EQ.0) PRINT *,'MINMAX_sgs_mdl_force(:,n_var_KD) inKOMET_rhs=', tmp1, tmp2
!       tmp1 = MINVAL( u_integrated(:,n_var_K) )
!       tmp2 = MAXVAL( u_integrated(:,n_var_K) )
!       CALL parallel_global_sum( REALMINVAL=tmp1 )
!       CALL parallel_global_sum( REALMAXVAL=tmp2 )
!       IF (par_rank.EQ.0) PRINT *,'MINMAX_Ksgs(:) inKOMET_rhs=', tmp1, tmp2
!       tmp1 = MINVAL( u_integrated(:,n_var_KD) )
!       tmp2 = MAXVAL( u_integrated(:,n_var_KD) )
!       CALL parallel_global_sum( REALMINVAL=tmp1 )
!       CALL parallel_global_sum( REALMAXVAL=tmp2 )
!       IF (par_rank.EQ.0) PRINT *,'MINMAX_Komeg(:) inKOMET_rhs=', tmp1, tmp2
       tmp1 = MINVAL( nutK(:) )
       tmp2 = MAXVAL( nutK(:) )
       CALL parallel_global_sum( REALMINVAL=tmp1 )
       CALL parallel_global_sum( REALMAXVAL=tmp2 )
       IF (par_rank.EQ.0) PRINT *,'MINMAX_nutK(:) inKOMET_rhs=', tmp1, tmp2
!       tmp1 = MINVAL( nuK(:) )
!       tmp2 = MAXVAL( nuK(:) )
!       CALL parallel_global_sum( REALMINVAL=tmp1 )
!       CALL parallel_global_sum( REALMAXVAL=tmp2 )
!       IF (par_rank.EQ.0) PRINT *,'MINMAX_nuK(:)/nu inKOMET_rhs=', tmp1/nu, tmp2/nu
    END IF

     ! Ksgs equation
     shift=(n_var_K-1)*ng
     DO idim = 1, dim
        duk(:,idim) = nutK(:)*du(n_var_K,:,idim)*C3_RANS 
     END DO
     user_rhs(shift+1:shift+ng) = -SUM(u_integrated(:,1:dim)*du(n_var_K,:,1:dim),DIM=2) &
                                + ( nu + nuK(:) )*SUM(d2u(n_var_K,:,:),2) &
                                + div(duk,ng,j_lev,meth)

     ! Ktau equation
     shift1=(n_var_KD-1)*ng
     DO idim = 1, dim
        duk(:,idim) = nutK(:)*du(n_var_KD,:,idim)*C4_RANS
     END DO
     user_rhs(shift1+1:shift1+ng) = -SUM(u_integrated(:,1:dim)*du(n_var_KD,:,1:dim),DIM=2) &
                                  + ( nu + nuK(:) )*SUM(d2u(n_var_KD,:,:),2) &
                                  + div(duk,ng,j_lev,meth) 

     IF( mdl_form == 0 ) THEN  ! Frozen forcing

      ! sgs_mdl_force(:,n_var_K) = nu_t(:)*smod2(:) - C0_RANS*u_in(:,n_var_K)/u_in(:,n_var_KD) 
      ! sgs_mdl_force(:,n_var_KD) = - C1_RANS*smod2(:)*u_in(:,n_var_KD)**2 + C2_RANS                    

       user_rhs(shift+1:shift+ng) = user_rhs(shift+1:shift+ng) + sgs_mdl_force(:,n_var_K)
       user_rhs(shift1+1:shift1+ng) = user_rhs(shift1+1:shift1+ng) + sgs_mdl_force(:,n_var_KD)

     ELSEIF ( mdl_form == 1 ) THEN  ! Frozen strain-rate

       ! sgs_mdl_force(:,n_var_K) = smod2(:)
       ! sgs_mdl_force(:,n_var_KD) = smod2(:)

       user_rhs(shift+1:shift+ng) = user_rhs(shift+1:shift+ng) &
                                  + nutK(:)*sgs_mdl_force(:,n_var_K) &
                                  - C0_RANS*u_integrated(:,n_var_K)/u_integrated(:,n_var_KD)

       user_rhs(shift1+1:shift1+ng) = user_rhs(shift1+1:shift1+ng) &
                                    - C1_RANS*sgs_mdl_force(:,n_var_KD)*u_integrated(:,n_var_KD)**2 + C2_RANS

    END IF

    ! additional diffusion term for Ktau equation (Kok 2000)
    nutK(:) = SUM(du(n_var_KD,:,1:dim)*du(n_var_KD,:,1:dim),DIM=2)
    user_rhs(shift1+1:shift1+ng) = user_rhs(shift1+1:shift1+ng) - 2.0_pr*(nu/u_integrated(:,n_var_KD)+C4_RANS*u_integrated(:,n_var_K))*nutK(:)

    ! cross-diffusion term for Ktau equation (Kok 2000)
    nutK(:) = SUM(du(n_var_K,:,1:dim)*du(n_var_KD,:,1:dim),DIM=2)
    user_rhs(shift1+1:shift1+ng) = user_rhs(shift1+1:shift1+ng) + C5_RANS*MIN(0.0_pr,nutK(:))*u_integrated(:,n_var_KD)

    IF (verb_level.GT.0) THEN
       tmp1 = MINVAL( nutK(:) )
       tmp2 = MAXVAL( nutK(:) )
       CALL parallel_global_sum( REALMINVAL=tmp1 )
       CALL parallel_global_sum( REALMAXVAL=tmp2 )
       IF (par_rank.EQ.0) PRINT *,'MINMAX_cross(:) inKOMET_rhs=', tmp1, tmp2
    END IF


  END SUBROUTINE KOMET_rhs

  ! find Jacobian of Right Hand Side of the problem
  ! u_prev == u_prev_timestep_loc

  SUBROUTINE KOMET_Drhs (user_Drhs, u, u_prev, du, d2u, meth)
    !--Form Drhs for both k and ktau equations
    IMPLICIT NONE
    INTEGER,                            INTENT(IN)    ::  meth
    !u_prev_timestep passed local to cast it into 2dim array to work with vector derivatives
    REAL (pr), DIMENSION (ng,ne),       INTENT(IN)    :: u, u_prev
    REAL (pr), DIMENSION (n),           INTENT(INOUT) :: user_Drhs
    REAL (pr), DIMENSION (2*ne,ng,dim), INTENT(INOUT) :: du     ! 1st derivatives for u (in du(1:ne,:,:)) and u_prev_timestep (in du(ne+1:2*ne,:,:))
    REAL (pr), DIMENSION (ne  ,ng,dim), INTENT(INOUT) :: d2u    ! 2nd derivatives for u 
    REAL (pr), DIMENSION (ng,dim)                     :: duk
    REAL (pr), DIMENSION (1:ng)                       :: nut_prev

    INTEGER            :: ie, shift, shift1, idim

    REAL (pr)  :: tmp1, tmp2

    ! evaluating nu_t = Ksgs*Ktau at previous time
    nut_prev(:) = u_prev(:,n_var_K)*u_prev(:,n_var_KD)
    ! evaluating nu_t variation 
    nutK(:) = u(:,n_var_K)*u_prev(:,n_var_KD)+u(:,n_var_KD)*u_prev(:,n_var_K)

    IF (verb_level.GT.0) THEN
!       tmp1 = MINVAL( sgs_mdl_force(:,n_var_K) )
!       tmp2 = MAXVAL( sgs_mdl_force(:,n_var_K) )
!       CALL parallel_global_sum( REALMINVAL=tmp1 )
!       CALL parallel_global_sum( REALMAXVAL=tmp2 )
!       IF (par_rank.EQ.0) PRINT *,'MINMAX sgs_mdl_force(:,n_var_K) inKOME_Drhs=', tmp1, tmp2
!       tmp1 = MINVAL( sgs_mdl_force(:,n_var_KD) )
!       tmp2 = MAXVAL( sgs_mdl_force(:,n_var_KD) )
!       CALL parallel_global_sum( REALMINVAL=tmp1 )
!       CALL parallel_global_sum( REALMAXVAL=tmp2 )
!       IF (par_rank.EQ.0) PRINT *,'MINMAX sgs_mdl_force(:,n_var_KD) inKOME_Drhs=', tmp1, tmp2
       tmp1 = MINVAL( nut_prev(:) )
       tmp2 = MAXVAL( nut_prev(:) )
       CALL parallel_global_sum( REALMINVAL=tmp1 )
       CALL parallel_global_sum( REALMAXVAL=tmp2 )
       IF (par_rank.EQ.0) PRINT *,'MINMAX nut_prev(:) inKOME_Drhs=', tmp1, tmp2
!       tmp1 = MINVAL( nutK(:) )
!       tmp2 = MAXVAL( nutK(:) )
!       CALL parallel_global_sum( REALMINVAL=tmp1 )
!       CALL parallel_global_sum( REALMAXVAL=tmp2 )
!       IF (par_rank.EQ.0) PRINT *,'MINMAX nutK(:) inKOME_Drhs=', tmp1, tmp2
    END IF

     shift=(n_var_K-1)*ng
     ie = n_var_K
     DO idim = 1, dim
        duk(:,idim) = ( nut_prev(:)*du(ie,:,idim) + nutK(:)*du(ne+ie,:,idim) )*C3_RANS
     END DO
     user_Drhs(shift+1:shift+ng) = - SUM(u_prev(:,1:dim)*du(ie,:,1:dim),DIM=2) - SUM(u(:,1:dim)*du(ne+ie,:,1:dim),DIM=2) & 
                                   + ( nu + nuK(:) )*SUM(d2u(ie,:,:),2) + div(duk,ng,j_lev,meth)

     shift1=(n_var_KD-1)*ng
     ie = n_var_KD
     DO idim = 1, dim
        duk(:,idim) = ( nut_prev(:)*du(ie,:,idim) + nutK(:)*du(ne+ie,:,idim) )*C4_RANS
     END DO
     user_Drhs(shift1+1:shift1+ng) = - SUM(u_prev(:,1:dim)*du(ie,:,1:dim),DIM=2) - SUM(u(:,1:dim)*du(ne+ie,:,1:dim),DIM=2) & 
                                     + ( nu + nuK(:) )*SUM(d2u(ie,:,:),2) + div(duk,ng,j_lev,meth)

     IF( mdl_form == 0 ) THEN  ! Frozen forcing

       ! sgs_mdl_force(:,n_var_K) = nu_t(:)*smod2(:) - C0_RANS*u_in(:,n_var_K)/u_in(:,n_var_KD) 
       ! sgs_mdl_force(:,n_var_KD) = - C1_RANS*smod2(:)*u_in(:,n_var_KD)**2 + C2_RANS                    

     ELSEIF ( mdl_form == 1 ) THEN  ! Frozen strain-rate

       ! sgs_mdl_force(:,n_var_K) = smod2(:)
       ! sgs_mdl_force(:,n_var_KD) = smod2(:)

       user_Drhs(shift+1:shift+ng) = user_Drhs(shift+1:shift+ng) & 
                                   + nutK(:)*sgs_mdl_force(:,n_var_K) &
                                   - C0_RANS*( u(:,n_var_K)*u_prev(:,n_var_KD) - u_prev(:,n_var_K)*u(:,n_var_KD) )/u_prev(:,n_var_KD)**2

       user_Drhs(shift1+1:shift1+ng) = user_Drhs(shift1+1:shift1+ng) & 
                                     - C1_RANS*sgs_mdl_force(:,n_var_KD)*2.0_pr*u_prev(:,n_var_KD)*u(:,n_var_KD)

     END IF

    ! additional diffusion term for Ktau equation (Kok 2000)

    user_Drhs(shift1+1:shift1+ng) = user_Drhs(shift1+1:shift1+ng)  &
       - 2.0_pr*(C4_RANS*nutK(:)*SUM(du(ne+n_var_KD,:,1:dim)*du(ne+n_var_KD,:,1:dim),DIM=2)  &
       + 2.0_pr*(nu+C4_RANS*nut_prev(:))*SUM(du(ne+n_var_KD,:,1:dim)*du(n_var_KD,:,1:dim),DIM=2)  &
       - (nu+C4_RANS*nut_prev(:))*SUM(du(ne+n_var_KD,:,1:dim)*du(ne+n_var_KD,:,1:dim),DIM=2)*u(:,n_var_KD)/u_prev(:,n_var_KD))/u_prev(:,n_var_KD)

    ! cross-diffusion term for Ktau equation (Kok 2000)

    nutK(:) = SUM(du(ne+n_var_K,:,1:dim)*du(ne+n_var_KD,:,1:dim),DIM=2)

    IF (verb_level.GT.0) THEN
       tmp1 = MINVAL( nutK(:) )
       tmp2 = MAXVAL( nutK(:) )
       CALL parallel_global_sum( REALMINVAL=tmp1 )
       CALL parallel_global_sum( REALMAXVAL=tmp2 )
       IF (par_rank.EQ.0) PRINT *,'MINMAX_cross(:) inKOME_Drhs=', tmp1, tmp2
    END IF

    WHERE ( nutK(:) < 0.0_pr )  & 
       user_Drhs(shift1+1:shift1+ng) = user_Drhs(shift1+1:shift1+ng)  &
       + C5_RANS*(SUM(du(ne+n_var_KD,:,1:dim)*du(n_var_K,:,1:dim),DIM=2) + SUM(du(n_var_KD,:,1:dim)*du(ne+n_var_K,:,1:dim),DIM=2))*u_prev(:,n_var_KD)  &
       + C5_RANS*nutK(:)*u(:,n_var_KD)

  END SUBROUTINE KOMET_Drhs
  !
  ! Uses u_prev_timestep, which is global flat (1D ) array set in time_step_cn()
  !
  SUBROUTINE KOMET_Drhs_diag (user_Drhs_diag, du_prev_timestep, du, d2u, meth)
    !--Form Drhs_diag for both k and ktau equations
    IMPLICIT NONE
    INTEGER,                          INTENT(IN)    :: meth
    REAL (pr), DIMENSION (n),         INTENT(INOUT) :: user_Drhs_diag
    REAL (pr), DIMENSION (ne,ng,dim), INTENT(INOUT) :: du_prev_timestep
    REAL (pr), DIMENSION (ng,dim),    INTENT(INOUT) :: du, d2u
    REAL (pr), DIMENSION (ng,dim)                   :: duk
    INTEGER :: ie, shift, shift1, idim

    REAL (pr)  :: tmp1, tmp2

    shift=(n_var_K-1)*ng
    shift1=(n_var_KD-1)*ng

    ! evaluating nu_t at prev_timestep = Ksgs*Ktau
    nutK(:) = u_prev_timestep(shift+1:shift+ng)*u_prev_timestep(shift1+1:shift1+ng)

    IF (verb_level.GT.0) THEN
!       tmp1 = MINVAL( sgs_mdl_force(:,n_var_K) )
!       tmp2 = MAXVAL( sgs_mdl_force(:,n_var_K) )
!       CALL parallel_global_sum( REALMINVAL=tmp1 )
!       CALL parallel_global_sum( REALMAXVAL=tmp2 )
!       IF (par_rank.EQ.0) PRINT *,'MINMAX_sgs_mdl_force(:,n_var_K) inKOME_Drhs_diag=', tmp1, tmp2
!       tmp1 = MINVAL( sgs_mdl_force(:,n_var_KD) )
!       tmp2 = MAXVAL( sgs_mdl_force(:,n_var_KD) )
!       CALL parallel_global_sum( REALMINVAL=tmp1 )
!       CALL parallel_global_sum( REALMAXVAL=tmp2 )
!       IF (par_rank.EQ.0) PRINT *,'MINMAX_sgs_mdl_force(:,n_var_KD) inKOME_Drhs_diag=', tmp1, tmp2
       tmp1 = MINVAL( nutK(:) )
       tmp2 = MAXVAL( nutK(:) )
       CALL parallel_global_sum( REALMINVAL=tmp1 )
       CALL parallel_global_sum( REALMAXVAL=tmp2 )
       IF (par_rank.EQ.0) PRINT *,'MINMAX_nutK(:) inKOME_Drhs_diag=', tmp1, tmp2
       tmp1 = MINVAL( du_prev_timestep(n_var_K,:,1) )
       tmp2 = MAXVAL( du_prev_timestep(n_var_K,:,1) )
       CALL parallel_global_sum( REALMINVAL=tmp1 )
       CALL parallel_global_sum( REALMAXVAL=tmp2 )
       IF (par_rank.EQ.0) PRINT *,'MINMAX_du_prev_timestep(n_var_K,:,1) inKOME_Drhs_diag=', tmp1, tmp2
       tmp1 = MINVAL( du_prev_timestep(n_var_K,:,2) )
       tmp2 = MAXVAL( du_prev_timestep(n_var_K,:,2) )
       CALL parallel_global_sum( REALMINVAL=tmp1 )
       CALL parallel_global_sum( REALMAXVAL=tmp2 )
       IF (par_rank.EQ.0) PRINT *,'MINMAX_du_prev_timestep(n_var_K,:,2) inKOME_Drhs_diag=', tmp1, tmp2
       tmp1 = MINVAL( du_prev_timestep(n_var_KD,:,1) )
       tmp2 = MAXVAL( du_prev_timestep(n_var_KD,:,1) )
       CALL parallel_global_sum( REALMINVAL=tmp1 )
       CALL parallel_global_sum( REALMAXVAL=tmp2 )
       IF (par_rank.EQ.0) PRINT *,'MINMAX_du_prev_timestep(n_var_KD,:,1) inKOME_Drhs_diag=', tmp1, tmp2
       tmp1 = MINVAL( du_prev_timestep(n_var_KD,:,2) )
       tmp2 = MAXVAL( du_prev_timestep(n_var_KD,:,2) )
       CALL parallel_global_sum( REALMINVAL=tmp1 )
       CALL parallel_global_sum( REALMAXVAL=tmp2 )
       IF (par_rank.EQ.0) PRINT *,'MINMAX_du_prev_timestep(n_var_KD,:,2) inKOME_Drhs_diag=', tmp1, tmp2
    ENDIF

    CALL c_diff_diag(du, duk, j_lev, ng, meth, meth, -01)
    DO idim = 1, dim
       duk(:,idim) = nutK(:)*duk(:,idim)*C3_RANS
    END DO
    user_Drhs_diag(shift+1:shift+ng) = ( nu + nuK(:) )*SUM(d2u,2) + SUM(duk(:,1:dim),DIM=2)

    duk = duk*C4_RANS/C3_RANS   ! the diagonal form is the same up to the constant multiplier
    user_Drhs_diag(shift1+1:shift1+ng) =  ( nu + nuK(:) )*SUM(d2u,2) + SUM(duk(:,1:dim),DIM=2)

    DO idim = 1, dim
        user_Drhs_diag(shift+1:shift+ng) = user_Drhs_diag(shift+1:shift+ng) - u_prev_timestep((idim-1)*ng+1:idim*ng)*du(:,idim) 
        user_Drhs_diag(shift1+1:shift1+ng) = user_Drhs_diag(shift1+1:shift1+ng) - u_prev_timestep((idim-1)*ng+1:idim*ng)*du(:,idim) 
    END DO

    IF( mdl_form == 0 ) THEN  ! Frozen forcing

       ! sgs_mdl_force(:,n_var_K) = nu_t(:)*smod2(:) - C0_RANS*u_in(:,n_var_K)/u_in(:,n_var_KD) 
       ! sgs_mdl_force(:,n_var_KD) = - C1_RANS*smod2(:)*u_in(:,n_var_KD)**2 + C2_RANS                    

    ELSEIF ( mdl_form == 1 ) THEN  ! Frozen strain-rate

       ! sgs_mdl_force(:,n_var_K) = smod2(:)
       ! sgs_mdl_force(:,n_var_KD) = smod2(:)

       user_Drhs_diag(shift+1:shift+ng) = user_Drhs_diag(shift+1:shift+ng)  &
                                        + sgs_mdl_force(:,n_var_K)*u_prev_timestep(shift1+1:shift1+ng)  & 
                                        - C0_RANS/u_prev_timestep(shift1+1:shift1+ng) 

       user_Drhs_diag(shift1+1:shift1+ng) = user_Drhs_diag(shift1+1:shift1+ng)  &
                                          - C1_RANS*sgs_mdl_force(:,n_var_KD)*2.0_pr*u_prev_timestep(shift1+1:shift1+ng) 

    END IF

    ! additional diffusion term for Ktau equation (Kok 2000)

    user_Drhs_diag(shift1+1:shift1+ng) = user_Drhs_diag(shift1+1:shift1+ng)  &
      -2.0_pr*(C4_RANS*u_prev_timestep(shift+1:shift+ng)*SUM(du_prev_timestep(n_var_KD,:,1:dim)*du_prev_timestep(n_var_KD,:,1:dim),DIM=2)  &
      +2.0_pr*(nu+C4_RANS*nutK(:))*SUM(du_prev_timestep(n_var_KD,:,1:dim)*du(:,1:dim),DIM=2)  &
      -(nu+C4_RANS*nutK(:))*SUM(du_prev_timestep(n_var_KD,:,1:dim)*du_prev_timestep(n_var_KD,:,1:dim),DIM=2)/u_prev_timestep(shift1+1:shift1+ng))/u_prev_timestep(shift1+1:shift1+ng)

    ! cross-diffusion term for Ktau equation (Kok 2000)
    nutK(:) = SUM(du_prev_timestep(n_var_K,:,1:dim)*du_prev_timestep(n_var_KD,:,1:dim),DIM=2)

!    IF (verb_level.GT.0) THEN
!       tmp1 = MINVAL( nutK(:) )
!       tmp2 = MAXVAL( nutK(:) )
!       CALL parallel_global_sum( REALMINVAL=tmp1 )
!       CALL parallel_global_sum( REALMAXVAL=tmp2 )
!       IF (par_rank.EQ.0) PRINT *,'MINMAX_cross(:) inKOME_Drhs_diag=', tmp1, tmp2
!    END IF

    WHERE ( nutK(:) < 0.0_pr )  &
         user_Drhs_diag(shift1+1:shift1+ng) = user_Drhs_diag(shift1+1:shift1+ng)  &
         + C5_RANS*SUM(du_prev_timestep(n_var_K,:,1:dim)*du(:,1:dim),DIM=2)*u_prev_timestep(shift1+1:shift1+ng)  &
         + C5_RANS*nutK(:)

  END SUBROUTINE KOMET_Drhs_diag

  !
  ! In this routine we calculate nu_t (k-omega model) based on the current values of Ksgs and Komeg
  ! and calculate the RHS forcing for the convection of Ksgs and Komeg for the next time step.
  ! init_flag - true if we are initializing the model
  !
  ! INPUT:  tau_ij = Sij (based on u>eps+adj)   and    nu_t = Smod = sqrt(2SijSij)
  !
  SUBROUTINE sgs_KOMET_model(u_in, tau_ij, nu_t, nlocal, init_flag, lowRe_flag)
    USE util_vars        ! dA 
    USE parallel
    IMPLICIT NONE
    INTEGER,                                    INTENT (IN)    :: nlocal
    LOGICAL,                                    INTENT (IN)    :: init_flag  ! true if we are initializing the model
    LOGICAL,                                    INTENT (IN)    :: lowRe_flag ! true if low-Reynolds correction
    REAL (pr), DIMENSION (nlocal,n_integrated), INTENT (INOUT) :: u_in
    REAL (pr), DIMENSION (1:nlocal,dim*(dim+1)/2), INTENT (INOUT) :: tau_ij
    REAL (pr), DIMENSION (1:nlocal),            INTENT (INOUT) :: nu_t

    REAL (pr), DIMENSION (1:nlocal,3-MOD(dim,3))  :: W
    REAL (pr), DIMENSION (1:nlocal)    :: smod2, Wmod, Re_t
    REAL (pr)                          :: tmp1, tmp2
    INTEGER                            :: i, ie

    !constant parameters for damping functions (low-Re correction)
    REAL (pr)  :: RB = 8.0_pr, RK = 6.0_pr, RW = 2.61_pr
    !constant parameter for stress-limiter = sqrt(0.09)*8/7 (Wilcox)
    REAL (pr)  :: Clim = 0.34285714_pr
    REAL (pr)  :: Clim2 = 10.0_pr

    !debug
    IF( BTEST(debug_level,1) ) THEN
       IF (par_rank.EQ.0) THEN
          IF (par_rank.EQ.0) WRITE(*,'(A)') "  "
          IF (par_rank.EQ.0) WRITE(*,'(A)') "Debug In sgs_KOMET_model()" 
       END IF
       DO i=1,n_integrated
          tmp1 = MINVAL(u_in(:,i)); tmp2 = MAXVAL(u_in(:,i))
          CALL parallel_global_sum (REALMINVAL=tmp1)
          CALL parallel_global_sum (REALMAXVAL=tmp2)
          IF (par_rank.EQ.0) WRITE(*,'(A,2(e15.7,1x))') "MINMAXVAL(u_in(:,i)) = ", tmp1, tmp2
       END DO
       IF (par_rank.EQ.0) THEN
          IF (par_rank.EQ.0) WRITE(*,'(A)') "END Debug In sgs_KOMET_model()"
          IF (par_rank.EQ.0) WRITE(*,'(A)') "  "
       END IF
    ENDIF

    IF(sgsmodel /= sgsmodel_KOMET ) THEN !problem
       IF (par_rank.EQ.0) PRINT *,'sgsmodel should be 11 to call sgs_KOMET_model()'
       IF (par_rank.EQ.0) PRINT *, 'Exiting...'
       CALL parallel_finalize; STOP
    END IF

    IF( init_flag ) THEN
       u_in(:,n_var_K) = Ksgs_init
       u_in(:,n_var_KD) = Kdiss_init
       IF (verb_level.GT.0 .AND. par_rank.EQ.0) PRINT *, 'Initialized Ksgs and Ktau at...', Ksgs_init, Kdiss_init
    END IF

    !
    ! save Smod^2 (input nu_t was Smod)
    !
    smod2(:) = nu_t(:)**2

    IF (verb_level.GT.0) THEN
       tmp1 = MINVAL( smod2(:) )
       tmp2 = MAXVAL( smod2(:) )
       CALL parallel_global_sum( REALMINVAL=tmp1 )
       CALL parallel_global_sum( REALMAXVAL=tmp2 )
       IF (par_rank.EQ.0) PRINT *,'MINMAX input smod2(:)=', tmp1, tmp2
    END IF

    !
    ! artificial diffusion coefficient for Ksgs & Ktau evolution
    !
    nuK(:) = C_nu_art_k*delta(:)**2*SQRT(smod2(:))

    !
    ! inferior limits for Ksgs & Ktau
    !
    u_in(:,n_var_K) = MAX(u_in(:,n_var_K),0.0_pr)
    u_in(:,n_var_KD) = MAX(u_in(:,n_var_KD),Kdiss_min)

    !
    ! superior limit for Ktau
    !
    u_in(:,n_var_KD) = MIN(u_in(:,n_var_KD),Clim/SQRT(smod2(:)))

    !
    ! Low-Reynolds formulation: apply wall damping as a function of Re_t = Ksgs/(nu*Komeg)
    !
!    IF ( lowRe_flag ) THEN
!      Re_t(:) = u_in(:,n_var_K)*u_in(:,n_var_KD)/nu
!      ASTAR(:) = ( C2_RANS/3.0_pr +  Re_t(:)/RK     ) / ( 1.0_pr +  Re_t(:)/RK ) 
!      BSTAR(:) = ( C2_RANS*100.0_pr/27.0_pr + (Re_t(:)/RB)**4 ) / ( 1.0_pr + (Re_t(:)/RB)**4 ) 
!      CSTAR(:) = ( 1.0_pr/9.0_pr +  Re_t(:)/RW     ) / ( 1.0_pr +  Re_t(:)/RW ) 
!    END IF

    !
    ! find turbulent eddy viscosity nu_t = Ksgs*Ktau with stress limiter (Wilcox)
    !
    nu_t(:) = u_in(:,n_var_K)*u_in(:,n_var_KD)

    IF (verb_level.GT.0) THEN
       tmp1 = MINVAL( nu_t(:) )
       tmp2 = MAXVAL( nu_t(:) )
       CALL parallel_global_sum( REALMINVAL=tmp1 )
       CALL parallel_global_sum( REALMAXVAL=tmp2 )
       IF (par_rank.EQ.0) PRINT *,'MINMAX nu_t=', tmp1, tmp2, '  MINMAX Re_t = nu_t/nu=', tmp1/nu, tmp2/nu
    END IF

!    nu_t(:) = ASTAR(:)*u_in(:,n_var_K)/MAX(Clim*SQRT(smod2(:)),u_in(:,n_var_KD))
!    nu_t(:) = ASTAR(:)*u_in(:,n_var_K)*u_in(:,n_var_KD)

!    IF (verb_level.GT.0) THEN
!       tmp1 = MINVAL( nu_t(:) )
!       tmp2 = MAXVAL( nu_t(:) )
!       CALL parallel_global_sum( REALMINVAL=tmp1 )
!       CALL parallel_global_sum( REALMAXVAL=tmp2 )
!       IF (par_rank.EQ.0) PRINT *,'lowRe MINMAX nu_t=', tmp1, tmp2, '  lowRe MINMAX Re_t = nu_t/nu=', tmp1/nu, tmp2/nu
!    END IF

    !
    ! calculate total dissipations
    !
    total_resolved_diss = nu * SUM( dA * smod2 )
    CALL parallel_global_sum( REAL=total_resolved_diss )
    total_sgs_diss = SUM( dA * nu_t * smod2 )
    CALL parallel_global_sum( REAL=total_sgs_diss )
    IF (par_rank.EQ.0) PRINT *,'total mod/res dissipation', total_sgs_diss, total_resolved_diss, total_sgs_diss/total_resolved_diss

    !
    ! find (-)Reynolds stresses (tau_ij was S_ij in input)
    !
    DO ie = 1, dim*(dim+1)/2
       tau_ij(:,ie) = 2.0_pr * nu_t(:) * tau_ij(:,ie)
    END DO
    DO ie = 1, dim
       tau_ij(:,ie) = tau_ij(:,ie) - 2.0_pr/3.0_pr*u_in(:,n_var_K)
!       tau_ij(:,ie) = tau_ij(:,ie) - 2.0_pr/REAL(dim,pr)*MAX(0.0_pr,u_in(:,n_var_K))
    END DO

    !
    ! Kato-Launder (1) or Menter (2) modification of the production term
    ! Wmod = SQRT( twoWijWij )
    !
    IF ( Prod_index == 1 .OR. Prod_index == 2) THEN
       CALL Wij( u_in(1:nlocal,1:dim) ,nlocal, W, Wmod )
       smod2(:) = sqrt(smod2(:))**(2-Prod_index) * Wmod(:)**Prod_index
       IF (verb_level.GT.0) THEN
          tmp1 = MINVAL( smod2(:) )
          tmp2 = MAXVAL( smod2(:) )
          CALL parallel_global_sum( REALMINVAL=tmp1 )
          CALL parallel_global_sum( REALMAXVAL=tmp2 )
          IF (par_rank.EQ.0) PRINT *,'MINMAX altered smod2(:)=', tmp1, tmp2
       END IF
    ENDIF

    !     
    ! Set RHS forcing for evolution of model scalars Ksgs and Ktau based on current variables
    !
    IF( mdl_form == 0 ) THEN    !Frozen forcing

       sgs_mdl_force(:,n_var_K) = nu_t(:)*smod2(:) - C0_RANS*BSTAR(:)*u_in(:,n_var_K)/u_in(:,n_var_KD) 
       sgs_mdl_force(:,n_var_KD) = - C1_RANS*smod2(:)*u_in(:,n_var_KD)**2 + C2_RANS                    

    ELSEIF( mdl_form == 1 ) THEN    !Frozen strain-rate

       sgs_mdl_force(:,n_var_K) = smod2(:)
       sgs_mdl_force(:,n_var_KD) = smod2(:)

    END IF

    ! Controlled decay for external flows (Menter)     
    sgs_mdl_force(:,n_var_K) = sgs_mdl_force(:,n_var_K) + C0_RANS*BSTAR(:)*Ksgs_init/Kdiss_init
    sgs_mdl_force(:,n_var_KD) = sgs_mdl_force(:,n_var_KD) + C1_RANS*smod2(:)*Kdiss_init**2

    IF (verb_level.GT.0) THEN
!       tmp1 = MINVAL( delta(:)**2*SQRT(smod2(:)) )
!       tmp2 = MAXVAL( delta(:)**2*SQRT(smod2(:)) )
!       CALL parallel_global_sum( REALMINVAL=tmp1 )
!       CALL parallel_global_sum( REALMAXVAL=tmp2 )
!       IF (par_rank.EQ.0) PRINT *,'MINMAX delta2S/nu in KOMET_model=', tmp1/nu, tmp2/nu
!       IF (par_rank.EQ.0) PRINT *,'MINMAX nuK/nu in KOMET_model=', C_nu_art_k*tmp1/nu, C_nu_art_k*tmp2/nu
!       tmp1 = MINVAL( sgs_mdl_force(:,n_var_K) )
!       tmp2 = MAXVAL( sgs_mdl_force(:,n_var_K) )
!       CALL parallel_global_sum( REALMINVAL=tmp1 )
!       CALL parallel_global_sum( REALMAXVAL=tmp2 )
!       IF (par_rank.EQ.0) PRINT *,'MINMAX sgs_mdl_force(:,n_var_K) in KOMET_model=', tmp1, tmp2
!       tmp1 = MINVAL( sgs_mdl_force(:,n_var_KD) )
!       tmp2 = MAXVAL( sgs_mdl_force(:,n_var_KD) )
!       CALL parallel_global_sum( REALMINVAL=tmp1 )
!       CALL parallel_global_sum( REALMAXVAL=tmp2 )
!       IF (par_rank.EQ.0) PRINT *,'MINMAX sgs_mdl_force(:,n_var_KD) in KOMET_model=', tmp1, tmp2
!       tmp1 = MINVAL( ASTAR(:) )
!       tmp2 = MAXVAL( ASTAR(:) )
!       CALL parallel_global_sum( REALMINVAL=tmp1 )
!       CALL parallel_global_sum( REALMAXVAL=tmp2 )
!       IF (par_rank.EQ.0) PRINT *,'MINMAX_ASTAR(:) inKOMET_model=', tmp1, tmp2
!       tmp1 = MINVAL( BSTAR(:) )
!       tmp2 = MAXVAL( BSTAR(:) )
!       CALL parallel_global_sum( REALMINVAL=tmp1 )
!       CALL parallel_global_sum( REALMAXVAL=tmp2 )
!       IF (par_rank.EQ.0) PRINT *,'MINMAX_BSTAR(:) inKOMET_model=', tmp1, tmp2
!       tmp1 = MINVAL( CSTAR(:) )
!       tmp2 = MAXVAL( CSTAR(:) )
!       CALL parallel_global_sum( REALMINVAL=tmp1 )
!       CALL parallel_global_sum( REALMAXVAL=tmp2 )
!       IF (par_rank.EQ.0) PRINT *,'MINMAX_CSTAR(:) inKOMET_model=', tmp1, tmp2
       tmp1 = MINVAL( u_in(:,n_var_K) )
       tmp2 = MAXVAL( u_in(:,n_var_K) )
       CALL parallel_global_sum( REALMINVAL=tmp1 )
       CALL parallel_global_sum( REALMAXVAL=tmp2 )
       IF (par_rank.EQ.0) PRINT *,'MINMAX_K(:) inKOMET_model=', tmp1, tmp2
       tmp1 = MINVAL( u_in(:,n_var_KD) )
       tmp2 = MAXVAL( u_in(:,n_var_KD) )
       CALL parallel_global_sum( REALMINVAL=tmp1 )
       CALL parallel_global_sum( REALMAXVAL=tmp2 )
       IF (par_rank.EQ.0) PRINT *,'MINMAX_KTAU(:) inKOMET_model=', tmp1, tmp2
    END IF

  END SUBROUTINE sgs_KOMET_model

  !****************************************************************************************
  !*                                   END of KOMET model                                  *
  !****************************************************************************************



  !****************************************************************************************
  !*                                 KOME Model (10:KOME)
  !****************************************************************************************

  SUBROUTINE KOME_rhs (user_rhs, u_integrated, du, d2u)
    !--Form rhs for both k and omega equations
    IMPLICIT NONE
    REAL (pr), DIMENSION (ng,ne),     INTENT(IN)    :: u_integrated !1D flat version of integrated variables without BC 
    REAL (pr), DIMENSION (n),         INTENT(INOUT) :: user_rhs
    REAL (pr), DIMENSION (ne,ng,dim), INTENT(INOUT) :: du, d2u
    REAL (pr), DIMENSION (ng,dim)                   :: duk

    INTEGER            :: shift, shift1, idim
    INTEGER, PARAMETER :: meth=1
    !constant parameter for stress-limiter 7/8 * sqrt(100/9) (Wilcox)
    REAL (pr)  :: Clim = 2.91666667_pr

    REAL (pr)  :: tmp1, tmp2

    ! Note: derivatives are passed to take adantage of vector wavelet transform
    ! They can be done in this subroutine as well

    ! evaluating nu_t = ASTAR*Ksgs/Komeg
    nutK(:) = ASTAR(:)*u_integrated(:,n_var_K)/u_integrated(:,n_var_KD)

!    IF (verb_level.GT.0) THEN
!       tmp1 = MINVAL( sgs_mdl_force(:,n_var_K) )
!       tmp2 = MAXVAL( sgs_mdl_force(:,n_var_K) )
!       CALL parallel_global_sum( REALMINVAL=tmp1 )
!       CALL parallel_global_sum( REALMAXVAL=tmp2 )
!       IF (par_rank.EQ.0) PRINT *,'MINMAX_sgs_mdl_force(:,n_var_K) inKOME_rhs=', tmp1, tmp2
!       tmp1 = MINVAL( sgs_mdl_force(:,n_var_KD) )
!       tmp2 = MAXVAL( sgs_mdl_force(:,n_var_KD) )
!       CALL parallel_global_sum( REALMINVAL=tmp1 )
!       CALL parallel_global_sum( REALMAXVAL=tmp2 )
!       IF (par_rank.EQ.0) PRINT *,'MINMAX_sgs_mdl_force(:,n_var_KD) inKOME_rhs=', tmp1, tmp2
!       tmp1 = MINVAL( u_integrated(:,n_var_K) )
!       tmp2 = MAXVAL( u_integrated(:,n_var_K) )
!       CALL parallel_global_sum( REALMINVAL=tmp1 )
!       CALL parallel_global_sum( REALMAXVAL=tmp2 )
!       IF (par_rank.EQ.0) PRINT *,'MINMAX_Ksgs(:) inKOME_rhs=', tmp1, tmp2
!       tmp1 = MINVAL( u_integrated(:,n_var_KD) )
!       tmp2 = MAXVAL( u_integrated(:,n_var_KD) )
!       CALL parallel_global_sum( REALMINVAL=tmp1 )
!       CALL parallel_global_sum( REALMAXVAL=tmp2 )
!       IF (par_rank.EQ.0) PRINT *,'MINMAX_Komeg(:) inKOME_rhs=', tmp1, tmp2
!       tmp1 = MINVAL( nutK(:) )
!       tmp2 = MAXVAL( nutK(:) )
!       CALL parallel_global_sum( REALMINVAL=tmp1 )
!       CALL parallel_global_sum( REALMAXVAL=tmp2 )
!       IF (par_rank.EQ.0) PRINT *,'MINMAX_nutK(:) inKOME_rhs=', tmp1, tmp2
!    END IF

     ! Ksgs equation
     shift=(n_var_K-1)*ng
     DO idim = 1, dim
        duk(:,idim) = nutK(:)*du(n_var_K,:,idim)*C3_RANS 
     END DO
     user_rhs(shift+1:shift+ng) = -SUM(u_integrated(:,1:dim)*du(n_var_K,:,1:dim),DIM=2) &
                                + ( nu + nuK(:) )*SUM(d2u(n_var_K,:,:),2) &
                                + div(duk,ng,j_lev,meth)

     ! Komeg equation
     shift1=(n_var_KD-1)*ng
     DO idim = 1, dim
        duk(:,idim) = nutK(:)*du(n_var_KD,:,idim)*C4_RANS
     END DO
     user_rhs(shift1+1:shift1+ng) = -SUM(u_integrated(:,1:dim)*du(n_var_KD,:,1:dim),DIM=2) &
                                  + ( nu + nuK(:) )*SUM(d2u(n_var_KD,:,:),2) &
                                  + div(duk,ng,j_lev,meth) 

     IF( mdl_form == 0 ) THEN  ! Frozen forcing

      ! sgs_mdl_force(:,n_var_K) = nu_t(:)*smod2(:) - C0_RANS*BSTAR(:)*u_in(:,n_var_K)*u_in(:,n_var_KD)
      ! sgs_mdl_force(:,n_var_KD) = C1_RANS*CSTAR(:)*smod2(:) - C2_RANS*u_in(:,n_var_KD)**2

       user_rhs(shift+1:shift+ng) = user_rhs(shift+1:shift+ng) + sgs_mdl_force(:,n_var_K)
       user_rhs(shift1+1:shift1+ng) = user_rhs(shift1+1:shift1+ng) + sgs_mdl_force(:,n_var_KD)

     ELSEIF ( mdl_form == 1 ) THEN  ! Frozen strain-rate

       ! sgs_mdl_force(:,n_var_K) = smod2(:)
       ! sgs_mdl_force(:,n_var_KD) = C1_RANS*CSTAR(:)*smod2(:)

       user_rhs(shift+1:shift+ng) = user_rhs(shift+1:shift+ng) &
                                  + nutK(:)*sgs_mdl_force(:,n_var_K) &
                                  - C0_RANS*BSTAR(:)*u_integrated(:,n_var_K)*u_integrated(:,n_var_KD)

       user_rhs(shift1+1:shift1+ng) = user_rhs(shift1+1:shift1+ng) &
                                    + sgs_mdl_force(:,n_var_KD) - C2_RANS*u_integrated(:,n_var_KD)**2

    END IF

    ! cross-diffusion term for Komeg equation (Wilcox 2008)
    !
    nutK(:) = SUM(du(n_var_K,:,1:dim)*du(n_var_KD,:,1:dim),DIM=2)
    user_rhs(shift1+1:shift1+ng) = user_rhs(shift1+1:shift1+ng) + C5_RANS*MAX(0.0_pr,nutK(:))/u_integrated(:,n_var_KD)

  END SUBROUTINE KOME_rhs


  ! find Jacobian of Right Hand Side of the problem
  ! u_prev == u_prev_timestep_loc

  SUBROUTINE KOME_Drhs (user_Drhs, u, u_prev, du, d2u, meth)
    !--Form Drhs for both k and omega equations
    IMPLICIT NONE
    INTEGER,                            INTENT(IN)    ::  meth
    !u_prev_timestep passed local to cast it into 2dim array to work with vector derivatives
    REAL (pr), DIMENSION (ng,ne),       INTENT(IN)    :: u, u_prev
    REAL (pr), DIMENSION (n),           INTENT(INOUT) :: user_Drhs
    REAL (pr), DIMENSION (2*ne,ng,dim), INTENT(INOUT) :: du     ! 1st derivatives for u (in du(1:ne,:,:)) and u_prev_timestep (in du(ne+1:2*ne,:,:))
    REAL (pr), DIMENSION (ne  ,ng,dim), INTENT(INOUT) :: d2u    ! 2nd derivatives for u 
    REAL (pr), DIMENSION (ng,dim)                     :: duk
    REAL (pr), DIMENSION (1:ng)                       :: nut_prev

    INTEGER            :: ie, shift, shift1, idim

    REAL (pr)  :: tmp1, tmp2

    ! evaluating nu_t = ASTAR*Ksgs/Komeg
    nut_prev(:) = ASTAR(:)*u_prev(:,n_var_K)/u_prev(:,n_var_KD)
    ! evaluating nu_t variation 
    nutK(:) = ASTAR(:)*(u(:,n_var_K)*u_prev(:,n_var_KD)-u(:,n_var_KD)*u_prev(:,n_var_K))/u_prev(:,n_var_KD)**2

!     IF (verb_level.GT.0) THEN
!       tmp1 = MINVAL( sgs_mdl_force(:,n_var_K) )
!       tmp2 = MAXVAL( sgs_mdl_force(:,n_var_K) )
!       CALL parallel_global_sum( REALMINVAL=tmp1 )
!       CALL parallel_global_sum( REALMAXVAL=tmp2 )
!       IF (par_rank.EQ.0) PRINT *,'MINMAX sgs_mdl_force(:,n_var_K) inKOME_Drhs=', tmp1, tmp2
!       tmp1 = MINVAL( sgs_mdl_force(:,n_var_KD) )
!       tmp2 = MAXVAL( sgs_mdl_force(:,n_var_KD) )
!       CALL parallel_global_sum( REALMINVAL=tmp1 )
!       CALL parallel_global_sum( REALMAXVAL=tmp2 )
!       IF (par_rank.EQ.0) PRINT *,'MINMAX sgs_mdl_force(:,n_var_KD) inKOME_Drhs=', tmp1, tmp2
!       tmp1 = MINVAL( nut_prev(:) )
!       tmp2 = MAXVAL( nut_prev(:) )
!       CALL parallel_global_sum( REALMINVAL=tmp1 )
!       CALL parallel_global_sum( REALMAXVAL=tmp2 )
!       IF (par_rank.EQ.0) PRINT *,'MINMAX nut_prev(:) inKOME_Drhs=', tmp1, tmp2
!       tmp1 = MINVAL( nutK(:) )
!       tmp2 = MAXVAL( nutK(:) )
!       CALL parallel_global_sum( REALMINVAL=tmp1 )
!       CALL parallel_global_sum( REALMAXVAL=tmp2 )
!       IF (par_rank.EQ.0) PRINT *,'MINMAX nutK(:) inKOME_Drhs=', tmp1, tmp2
!     END IF

     shift=(n_var_K-1)*ng
     ie = n_var_K
     DO idim = 1, dim
        duk(:,idim) = ( nut_prev(:)*du(ie,:,idim) + nutK(:)*du(ne+ie,:,idim) )*C3_RANS
     END DO
     user_Drhs(shift+1:shift+ng) = - SUM(u_prev(:,1:dim)*du(ie,:,1:dim),DIM=2) - SUM(u(:,1:dim)*du(ne+ie,:,1:dim),DIM=2) & 
                                   + ( nu + nuK(:) )*SUM(d2u(ie,:,:),2) + div(duk,ng,j_lev,meth)

     shift1=(n_var_KD-1)*ng
     ie = n_var_KD
     DO idim = 1, dim
        duk(:,idim) = ( nut_prev(:)*du(ie,:,idim) + nutK(:)*du(ne+ie,:,idim) )*C4_RANS
     END DO
     user_Drhs(shift1+1:shift1+ng) = - SUM(u_prev(:,1:dim)*du(ie,:,1:dim),DIM=2) - SUM(u(:,1:dim)*du(ne+ie,:,1:dim),DIM=2) & 
                                     + ( nu + nuK(:) )*SUM(d2u(ie,:,:),2) + div(duk,ng,j_lev,meth)

     IF( mdl_form == 0 ) THEN  ! Frozen forcing

       ! sgs_mdl_force(:,n_var_K) = nu_t(:)*smod2(:) - C0_RANS*BSTAR(:)*u_in(:,n_var_K)*u_in(:,n_var_KD)
       ! sgs_mdl_force(:,n_var_KD) = C1_RANS*CSTAR(:)*smod2(:) - C2_RANS*u_in(:,n_var_KD)**2

     ELSEIF ( mdl_form == 1 ) THEN  ! Frozen strain-rate

       ! sgs_mdl_force(:,n_var_K) = smod2(:)
       ! sgs_mdl_force(:,n_var_KD) = C1_RANS*CSTAR(:)*smod2(:)

       user_Drhs(shift+1:shift+ng) = user_Drhs(shift+1:shift+ng) & 
                                   + nutK(:)*sgs_mdl_force(:,n_var_K) &
                                   - C0_RANS*BSTAR(:)*( u(:,n_var_K)*u_prev(:,n_var_KD) + u_prev(:,n_var_K)*u(:,n_var_KD) )

       user_Drhs(shift1+1:shift1+ng) = user_Drhs(shift1+1:shift1+ng) & 
                                     - C2_RANS*2.0_pr*u_prev(:,n_var_KD)*u(:,n_var_KD)

     END IF

    ! cross-diffusion term for Komeg equation (Wilcox 2008)
    !
    nutK(:) = SUM(du(ne+n_var_K,:,1:dim)*du(ne+n_var_KD,:,1:dim),DIM=2)

    WHERE ( nutK(:) > 0.0_pr )  & 
       user_Drhs(shift1+1:shift1+ng) = user_Drhs(shift1+1:shift1+ng)  &
                       + C5_RANS*(SUM(du(ne+n_var_KD,:,1:dim)*du(n_var_K,:,1:dim),DIM=2) + SUM(du(n_var_KD,:,1:dim)*du(ne+n_var_K,:,1:dim),DIM=2))/u_prev(:,n_var_KD)  &
                       - C5_RANS*u(:,n_var_KD)*nutK(:)/u_prev(:,n_var_KD)**2 

  END SUBROUTINE KOME_Drhs


  !
  ! Uses u_prev_timestep, which is global flat (1D ) array set in time_step_cn()
  !
  SUBROUTINE KOME_Drhs_diag (user_Drhs_diag, du_prev_timestep, du, d2u, meth)
    !--Form Drhs_diag for both k and omega equations
    IMPLICIT NONE
    INTEGER,                          INTENT(IN)    :: meth
    REAL (pr), DIMENSION (n),         INTENT(INOUT) :: user_Drhs_diag
    REAL (pr), DIMENSION (ne,ng,dim), INTENT(INOUT) :: du_prev_timestep
    REAL (pr), DIMENSION (ng,dim),    INTENT(INOUT) :: du, d2u
    REAL (pr), DIMENSION (ng,dim)                   :: duk

    INTEGER :: ie, shift, shift1, idim

    REAL (pr)  :: tmp1, tmp2

    shift=(n_var_K-1)*ng
    shift1=(n_var_KD-1)*ng

    ! evaluating nu_t_prev_timestep = ASTAR*Ksgs/Komeg
    nutK(:) = ASTAR(:)*u_prev_timestep(shift+1:shift+ng)/u_prev_timestep(shift1+1:shift1+ng)

    IF (verb_level.GT.0) THEN
!       tmp1 = MINVAL( sgs_mdl_force(:,n_var_K) )
!       tmp2 = MAXVAL( sgs_mdl_force(:,n_var_K) )
!       CALL parallel_global_sum( REALMINVAL=tmp1 )
!       CALL parallel_global_sum( REALMAXVAL=tmp2 )
!       IF (par_rank.EQ.0) PRINT *,'MINMAX_sgs_mdl_force(:,n_var_K) inKOME_Drhs_diag=', tmp1, tmp2
!       tmp1 = MINVAL( sgs_mdl_force(:,n_var_KD) )
!       tmp2 = MAXVAL( sgs_mdl_force(:,n_var_KD) )
!       CALL parallel_global_sum( REALMINVAL=tmp1 )
!       CALL parallel_global_sum( REALMAXVAL=tmp2 )
!       IF (par_rank.EQ.0) PRINT *,'MINMAX_sgs_mdl_force(:,n_var_KD) inKOME_Drhs_diag=', tmp1, tmp2
       tmp1 = MINVAL( du_prev_timestep(n_var_K,:,1) )
       tmp2 = MAXVAL( du_prev_timestep(n_var_K,:,1) )
       CALL parallel_global_sum( REALMINVAL=tmp1 )
       CALL parallel_global_sum( REALMAXVAL=tmp2 )
       IF (par_rank.EQ.0) PRINT *,'MINMAX_du_prev_timestep(n_var_K,:,1) inKOME_Drhs_diag=', tmp1, tmp2
       tmp1 = MINVAL( du_prev_timestep(n_var_K,:,2) )
       tmp2 = MAXVAL( du_prev_timestep(n_var_K,:,2) )
       CALL parallel_global_sum( REALMINVAL=tmp1 )
       CALL parallel_global_sum( REALMAXVAL=tmp2 )
       IF (par_rank.EQ.0) PRINT *,'MINMAX_du_prev_timestep(n_var_K,:,2) inKOME_Drhs_diag=', tmp1, tmp2
       tmp1 = MINVAL( du_prev_timestep(n_var_KD,:,1) )
       tmp2 = MAXVAL( du_prev_timestep(n_var_KD,:,1) )
       CALL parallel_global_sum( REALMINVAL=tmp1 )
       CALL parallel_global_sum( REALMAXVAL=tmp2 )
       IF (par_rank.EQ.0) PRINT *,'MINMAX_du_prev_timestep(n_var_KD,:,1) inKOME_Drhs_diag=', tmp1, tmp2
       tmp1 = MINVAL( du_prev_timestep(n_var_KD,:,2) )
       tmp2 = MAXVAL( du_prev_timestep(n_var_KD,:,2) )
       CALL parallel_global_sum( REALMINVAL=tmp1 )
       CALL parallel_global_sum( REALMAXVAL=tmp2 )
       IF (par_rank.EQ.0) PRINT *,'MINMAX_du_prev_timestep(n_var_KD,:,2) inKOME_Drhs_diag=', tmp1, tmp2
    ENDIF

    CALL c_diff_diag(du, duk, j_lev, ng, meth, meth, -01)
    DO idim = 1, dim
       duk(:,idim) = nutK(:)*duk(:,idim)*C3_RANS
    END DO
    user_Drhs_diag(shift+1:shift+ng) = ( nu + nuK(:) )*SUM(d2u,2) + SUM(duk(:,1:dim),DIM=2)

    duk = duk*C4_RANS/C3_RANS   ! the diagonal form is the same up to the constant multiplier
    user_Drhs_diag(shift1+1:shift1+ng) =  ( nu + nuK(:) )*SUM(d2u,2) + SUM(duk(:,1:dim),DIM=2)

    DO idim = 1, dim
        user_Drhs_diag(shift+1:shift+ng) = user_Drhs_diag(shift+1:shift+ng) - u_prev_timestep((idim-1)*ng+1:idim*ng)*du(:,idim) 
        user_Drhs_diag(shift1+1:shift1+ng) = user_Drhs_diag(shift1+1:shift1+ng) - u_prev_timestep((idim-1)*ng+1:idim*ng)*du(:,idim) 
    END DO

    IF( mdl_form == 0 ) THEN  ! Frozen forcing

      ! sgs_mdl_force(:,n_var_K) = nu_t(:)*smod2(:) - C0_RANS*BSTAR(:)*u_in(:,n_var_K)*u_in(:,n_var_KD)
      ! sgs_mdl_force(:,n_var_KD) = C1_RANS*CSTAR(:)*smod2(:) - C2_RANS*u_in(:,n_var_KD)**2

    ELSEIF ( mdl_form == 1 ) THEN  ! Frozen strain-rate

       ! sgs_mdl_force(:,n_var_K) = smod2(:)
       ! sgs_mdl_force(:,n_var_KD) = C1_RANS*CSTAR(:)*smod2(:)

       user_Drhs_diag(shift+1:shift+ng) = user_Drhs_diag(shift+1:shift+ng)  &
                                        + ASTAR(:)*sgs_mdl_force(:,n_var_K)/u_prev_timestep(shift1+1:shift1+ng)  & 
                                        - C0_RANS*BSTAR(:)*u_prev_timestep(shift1+1:shift1+ng) 

       user_Drhs_diag(shift1+1:shift1+ng) = user_Drhs_diag(shift1+1:shift1+ng)  &
                                          - C2_RANS*2.0_pr*u_prev_timestep(shift1+1:shift1+ng) 

    END IF

    ! cross-diffusion term for Komeg equation (Wilcox 2008)
    !
    nutK(:) = SUM(du_prev_timestep(n_var_K,:,1:dim)*du_prev_timestep(n_var_KD,:,1:dim),DIM=2)
    WHERE ( nutK(:) > 0.0_pr )  &
       user_Drhs_diag(shift1+1:shift1+ng) = user_Drhs_diag(shift1+1:shift1+ng)  &
                                          + C5_RANS*SUM(du_prev_timestep(n_var_K,:,1:dim)*du(:,1:dim),DIM=2)/u_prev_timestep(shift1+1:shift1+ng)  &
                                          - C5_RANS*nutK(:)/u_prev_timestep(shift1+1:shift1+ng)**2 

  END SUBROUTINE KOME_Drhs_diag


  !
  ! In this routine we calculate nu_t (k-omega model) based on the current values of Ksgs and Komeg
  ! and calculate the RHS forcing for the convection of Ksgs and Komeg for the next time step.
  ! init_flag - true if we are initializing the model
  !
  ! INPUT:  tau_ij = Sij (based on u>eps+adj)   and    nu_t = Smod = sqrt(2SijSij)
  !
  SUBROUTINE sgs_KOME_model(u_in, tau_ij, nu_t, nlocal, init_flag, lowRe_flag)
    USE util_vars        ! dA 
    USE parallel
    IMPLICIT NONE
    INTEGER,                                    INTENT (IN)    :: nlocal
    LOGICAL,                                    INTENT (IN)    :: init_flag  ! true if we are initializing the model
    LOGICAL,                                    INTENT (IN)    :: lowRe_flag ! true if low-Reynolds correction
    REAL (pr), DIMENSION (nlocal,n_integrated), INTENT (INOUT) :: u_in
    REAL (pr), DIMENSION (1:nlocal,dim*(dim+1)/2), INTENT (INOUT) :: tau_ij
    REAL (pr), DIMENSION (1:nlocal),            INTENT (INOUT) :: nu_t

    REAL (pr), DIMENSION (1:nlocal,3-MOD(dim,3))  :: W
    REAL (pr), DIMENSION (1:nlocal)    :: smod2, Wmod, Re_t
    REAL (pr)                          :: tmp1, tmp2
    INTEGER                            :: i, ie

    !constant parameters for damping functions (low-Re correction)
    REAL (pr)  :: RB = 8.0_pr, RK = 6.0_pr, RW = 2.61_pr
    !constant parameter for stress-limiter 7/8 * sqrt(100/9) (Wilcox)
    REAL (pr)  :: Clim = 2.91666667_pr
    REAL (pr)  :: Clim2 = 10.0_pr

    !debug
    IF( BTEST(debug_level,1) ) THEN
       IF (par_rank.EQ.0) THEN
          IF (par_rank.EQ.0) WRITE(*,'(A)') "  "
          IF (par_rank.EQ.0) WRITE(*,'(A)') "Debug In sgs_KOME_model()" 
       END IF
       DO i=1,n_integrated
          tmp1 = MINVAL(u_in(:,i)); tmp2 = MAXVAL(u_in(:,i))
          CALL parallel_global_sum (REALMINVAL=tmp1)
          CALL parallel_global_sum (REALMAXVAL=tmp2)
          IF (par_rank.EQ.0) WRITE(*,'(A,2(e15.7,1x))') "MINMAXVAL(u_in(:,i)) = ", tmp1, tmp2
       END DO
       IF (par_rank.EQ.0) THEN
          IF (par_rank.EQ.0) WRITE(*,'(A)') "END Debug In sgs_KOME_model()"
          IF (par_rank.EQ.0) WRITE(*,'(A)') "  "
       END IF
    ENDIF

    IF(sgsmodel /= sgsmodel_KOME ) THEN !problem
       IF (par_rank.EQ.0) PRINT *,'sgsmodel should be 10 to call sgs_KOME_model()'
       IF (par_rank.EQ.0) PRINT *, 'Exiting...'
       CALL parallel_finalize; STOP
    END IF

    IF( init_flag ) THEN
       u_in(:,n_var_K) = Ksgs_init
       u_in(:,n_var_KD) = Kdiss_init
       IF (verb_level.GT.0 .AND. par_rank.EQ.0) PRINT *, 'Initialized Ksgs and Komeg at...', Ksgs_init, Kdiss_init
    END IF

    !
    ! save Smod^2 (input nu_t was Smod)
    !
    smod2(:) = nu_t(:)**2

    IF (verb_level.GT.0) THEN
       tmp1 = MINVAL( smod2(:) )
       tmp2 = MAXVAL( smod2(:) )
       CALL parallel_global_sum( REALMINVAL=tmp1 )
       CALL parallel_global_sum( REALMAXVAL=tmp2 )
       IF (par_rank.EQ.0) PRINT *,'MINMAX input smod2(:)=', tmp1, tmp2
       IF (par_rank.EQ.0) PRINT *,'MINMAX Clim|S|=', Clim*SQRT(tmp1), Clim*SQRT(tmp2), 80.0_pr*nu/delta_n**2
    END IF

    !
    ! inferior limits for Ksgs & Komeg
    !
    u_in(:,n_var_K) = MAX(u_in(:,n_var_K),0.0_pr)
    u_in(:,n_var_KD) = MAX(u_in(:,n_var_KD),MAX(Kdiss_min,Clim*SQRT(smod2(:))))

    !
    ! superior limit for Komeg
    !
    u_in(:,n_var_KD) = MIN(u_in(:,n_var_KD),80.0_pr*nu/delta_n**2)

    !
    ! Low-Reynolds formulation: apply wall damping as a function of Re_t = Ksgs/(nu*Komeg)
    !
    IF ( lowRe_flag ) THEN
      Re_t(:) = u_in(:,n_var_K)/(nu*u_in(:,n_var_KD))
      ASTAR(:) = ( C2_RANS/3.0_pr +  Re_t(:)/RK     ) / ( 1.0_pr +  Re_t(:)/RK ) 
      BSTAR(:) = ( C2_RANS*100.0_pr/27.0_pr + (Re_t(:)/RB)**4 ) / ( 1.0_pr + (Re_t(:)/RB)**4 ) 
      CSTAR(:) = ( 1.0_pr/9.0_pr +  Re_t(:)/RW     ) / ( 1.0_pr +  Re_t(:)/RW ) 
    END IF

    !
    ! find turbulent eddy viscosity nu_t = ASTAR*Ksgs/Komeg with stress limiter (Wilcox)
    !
    nu_t(:) = u_in(:,n_var_K)/MAX(Clim*SQRT(smod2(:)),u_in(:,n_var_KD))

    IF (verb_level.GT.0) THEN
       tmp1 = MINVAL( nu_t(:) )
       tmp2 = MAXVAL( nu_t(:) )
       CALL parallel_global_sum( REALMINVAL=tmp1 )
       CALL parallel_global_sum( REALMAXVAL=tmp2 )
       IF (par_rank.EQ.0) PRINT *,'MINMAX nu_t=', tmp1, tmp2, '  MINMAX Re_t = nu_t/nu=', tmp1/nu, tmp2/nu
    END IF

!    nu_t(:) = ASTAR(:)*u_in(:,n_var_K)/MAX(Clim*SQRT(smod2(:)),u_in(:,n_var_KD))

!    IF (verb_level.GT.0) THEN
!       tmp1 = MINVAL( nu_t(:) )
!       tmp2 = MAXVAL( nu_t(:) )
!       CALL parallel_global_sum( REALMINVAL=tmp1 )
!       CALL parallel_global_sum( REALMAXVAL=tmp2 )
!       IF (par_rank.EQ.0) PRINT *,'lowRe MINMAX nu_t=', tmp1, tmp2, '  lowRe MINMAX Re_t = nu_t/nu=', tmp1/nu, tmp2/nu
!    END IF

    !
    ! artificial diffusion coefficient for Ksgs & Komeg evolution
    !
    nuK(:) = C_nu_art_k*delta(:)**2*SQRT(smod2(:))

    !
    ! calculate total dissipations
    !
    total_resolved_diss = nu * SUM( dA * smod2 )
    CALL parallel_global_sum( REAL=total_resolved_diss )
    total_sgs_diss = SUM( dA * nu_t * smod2 )
    CALL parallel_global_sum( REAL=total_sgs_diss )
    IF (par_rank.EQ.0) PRINT *,'total diss: res, sgs, ratio', total_resolved_diss, total_sgs_diss, total_resolved_diss/total_sgs_diss

    !
    ! find (-)Reynolds stresses (tau_ij was S_ij in input)
    !
    DO ie = 1, dim*(dim+1)/2
       tau_ij(:,ie) = 2.0_pr * nu_t(:) * tau_ij(:,ie)
    END DO
    DO ie = 1, dim
       tau_ij(:,ie) = tau_ij(:,ie) - 2.0_pr*u_in(:,n_var_K)/3.0_pr
!       tau_ij(:,ie) = tau_ij(:,ie) - 2.0_pr/REAL(dim,pr)*MAX(0.0_pr,u_in(:,n_var_K))
    END DO

    !
    ! Kato-Launder (1) or Menter (2) modification of the production term
    ! Wmod = SQRT( twoWijWij )
    !
    IF ( Prod_index == 1 .OR. Prod_index == 2) THEN
       CALL Wij( u_in(1:nlocal,1:dim) ,nlocal, W, Wmod )
       smod2(:) = sqrt(smod2(:))**(2-Prod_index) * Wmod(:)**Prod_index
       IF (verb_level.GT.0) THEN
          tmp1 = MINVAL( smod2(:) )
          tmp2 = MAXVAL( smod2(:) )
          CALL parallel_global_sum( REALMINVAL=tmp1 )
          CALL parallel_global_sum( REALMAXVAL=tmp2 )
          IF (par_rank.EQ.0) PRINT *,'MINMAX altered smod2(:)=', tmp1, tmp2
       END IF
    ENDIF

    !     
    ! Set RHS forcing for evolution of model scalars Ksgs and Komeg based on current variables
    !
    IF( mdl_form == 0 ) THEN    !Frozen forcing

       sgs_mdl_force(:,n_var_K) = nu_t(:)*smod2(:) - C0_RANS*BSTAR(:)*u_in(:,n_var_K)*u_in(:,n_var_KD) 
       sgs_mdl_force(:,n_var_KD) = C1_RANS*CSTAR(:)*smod2(:) - C2_RANS*u_in(:,n_var_KD)**2
       ! Production limiter (Menter)     
!       sgs_mdl_force(:,n_var_K) = MIN(sgs_mdl_force(:,n_var_K), (Clim2-1.0_pr)*C0_RANS*BSTAR(:)*u_in(:,n_var_K)*u_in(:,n_var_KD))
!       sgs_mdl_force(:,n_var_KD) = MIN(sgs_mdl_force(:,n_var_KD), (Clim2*C0_RANS*BSTAR(:)*C1_RANS*CSTAR(:)/ASTAR(:)-C2_RANS*u_in(:,n_var_KD)**2)

    ELSEIF( mdl_form == 1 ) THEN    !Frozen strain-rate

       sgs_mdl_force(:,n_var_K) = smod2(:)
       sgs_mdl_force(:,n_var_KD) = C1_RANS*CSTAR(:)*smod2(:)
       ! Production limiter (Menter)     
!       sgs_mdl_force(:,n_var_KD) = MIN(sgs_mdl_force(:,n_var_KD),CSTAR(:)*Clim2*(C0_RANS*BSTAR(:)/ASTAR(:))*u_in(:,n_var_KD)**2)

    END IF

    ! Controlled decay for external flows (Menter)     
    sgs_mdl_force(:,n_var_K) = sgs_mdl_force(:,n_var_K) + C0_RANS*BSTAR(:)*Ksgs_init*Kdiss_init
    sgs_mdl_force(:,n_var_KD) = sgs_mdl_force(:,n_var_KD) + C2_RANS*Kdiss_init**2

    IF (verb_level.GT.0) THEN
       tmp1 = MINVAL( nuK(:) )
       tmp2 = MAXVAL( nuK(:) )
       CALL parallel_global_sum( REALMINVAL=tmp1 )
       CALL parallel_global_sum( REALMAXVAL=tmp2 )
       IF (par_rank.EQ.0) PRINT *,'MINMAX nuK/nu in KOME_model=', tmp1/nu, tmp2/nu
!       tmp1 = MINVAL( sgs_mdl_force(:,n_var_K) )
!       tmp2 = MAXVAL( sgs_mdl_force(:,n_var_K) )
!       CALL parallel_global_sum( REALMINVAL=tmp1 )
!       CALL parallel_global_sum( REALMAXVAL=tmp2 )
!       IF (par_rank.EQ.0) PRINT *,'MINMAX sgs_mdl_force(:,n_var_K) in KOME_model=', tmp1, tmp2
!       tmp1 = MINVAL( sgs_mdl_force(:,n_var_KD) )
!       tmp2 = MAXVAL( sgs_mdl_force(:,n_var_KD) )
!       CALL parallel_global_sum( REALMINVAL=tmp1 )
!       CALL parallel_global_sum( REALMAXVAL=tmp2 )
!       IF (par_rank.EQ.0) PRINT *,'MINMAX sgs_mdl_force(:,n_var_KD) in KOME_model=', tmp1, tmp2
!       tmp1 = MINVAL( ASTAR(:) )
!       tmp2 = MAXVAL( ASTAR(:) )
!       CALL parallel_global_sum( REALMINVAL=tmp1 )
!       CALL parallel_global_sum( REALMAXVAL=tmp2 )
!       IF (par_rank.EQ.0) PRINT *,'MINMAX_ASTAR(:) inKOME_model=', tmp1, tmp2
!       tmp1 = MINVAL( BSTAR(:) )
!       tmp2 = MAXVAL( BSTAR(:) )
!       CALL parallel_global_sum( REALMINVAL=tmp1 )
!       CALL parallel_global_sum( REALMAXVAL=tmp2 )
!       IF (par_rank.EQ.0) PRINT *,'MINMAX_BSTAR(:) inKOME_model=', tmp1, tmp2
!       tmp1 = MINVAL( CSTAR(:) )
!       tmp2 = MAXVAL( CSTAR(:) )
!       CALL parallel_global_sum( REALMINVAL=tmp1 )
!       CALL parallel_global_sum( REALMAXVAL=tmp2 )
!       IF (par_rank.EQ.0) PRINT *,'MINMAX_CSTAR(:) inKOME_model=', tmp1, tmp2
       tmp1 = MINVAL( u_in(:,n_var_K) )
       tmp2 = MAXVAL( u_in(:,n_var_K) )
       CALL parallel_global_sum( REALMINVAL=tmp1 )
       CALL parallel_global_sum( REALMAXVAL=tmp2 )
       IF (par_rank.EQ.0) PRINT *,'MINMAX_K(:) inKOME_model=', tmp1, tmp2
       tmp1 = MINVAL( u_in(:,n_var_KD) )
       tmp2 = MAXVAL( u_in(:,n_var_KD) )
       CALL parallel_global_sum( REALMINVAL=tmp1 )
       CALL parallel_global_sum( REALMAXVAL=tmp2 )
       IF (par_rank.EQ.0) PRINT *,'MINMAX_KOMEG(:) inKOME_model=', tmp1, tmp2
    END IF

  END SUBROUTINE sgs_KOME_model

  !****************************************************************************************
  !*                                   END of KOMEGA model                                  *
  !****************************************************************************************





  !****************************************************************************************
  !*                                 KEPS Model (9:KEPS)
  !****************************************************************************************

  SUBROUTINE KEPS_rhs (user_rhs, u_integrated, du, d2u)
    IMPLICIT NONE
    REAL (pr), DIMENSION (ng,ne),     INTENT(IN)    :: u_integrated !1D flat version of integrated variables without BC 
    REAL (pr), DIMENSION (n),         INTENT(INOUT) :: user_rhs
    REAL (pr), DIMENSION (ne,ng,dim), INTENT(INOUT) :: du, d2u
    REAL (pr), DIMENSION (ng,dim)                   :: duk

    INTEGER            :: ie, shift, shift1, i, idim
    INTEGER, PARAMETER :: meth=1

    REAL (pr)            :: tmp1, tmp2

    IF (verb_level.GT.0) THEN
       tmp1 = MINVAL( sgs_mdl_force(:,n_var_K) )
       tmp2 = MAXVAL( sgs_mdl_force(:,n_var_K) )
       CALL parallel_global_sum( REALMINVAL=tmp1 )
       CALL parallel_global_sum( REALMAXVAL=tmp2 )
       IF (par_rank.EQ.0) PRINT *,'MINMAX sgs_mdl_force(K) inKEPS_rhs=', tmp1, tmp2
       tmp1 = MINVAL( sgs_mdl_force(:,n_var_KD) )
       tmp2 = MAXVAL( sgs_mdl_force(:,n_var_KD) )
       CALL parallel_global_sum( REALMINVAL=tmp1 )
       CALL parallel_global_sum( REALMAXVAL=tmp2 )
       IF (par_rank.EQ.0) PRINT *,'MINMAX sgs_mdl_force(KD) inKEPS_rhs=', tmp1, tmp2
    END IF

    !
    !Note: derivatives are passed to take adantage of vector wavelet transform
    !They can be done in this subroutine as well
    !

    shift=(n_var_K-1)*ng
    shift1=(n_var_KD-1)*ng

    ! evaluating nu_t = C0*Ksgs^2/Kdiss for the turbulent diffusion term
    nutK(:) = C0_RANS*u_integrated(:,n_var_K)**2/u_integrated(:,n_var_KD)

    ! Ksgs evolution: common part
    ie = n_var_K
    DO idim = 1, dim
       duk(:,idim) = nutK(:)*du(ie,:,idim)*C3_RANS 
    END DO
    user_rhs(shift+1:shift+ng) = -SUM(u_integrated(:,1:dim)*du(ie,:,1:dim),DIM=2) &
                                 + ( nu + nuK(:) )*SUM(d2u(ie,:,:),2) &
                                 + div(duk,ng,j_lev,meth)
       
    ! Kdiss evolution: common part
    !
    ie = n_var_KD
    DO idim = 1, dim
       duk(:,idim) = nutK(:)*du(ie,:,idim)*C4_RANS
    END DO
    user_rhs(shift1+1:shift1+ng) = -SUM(u_integrated(:,1:dim)*du(ie,:,1:dim),DIM=2) &
                                 + ( nu + nuK(:) )*SUM(d2u(ie,:,:),2) &
                                 + div(duk,ng,j_lev,meth)

    ! Ksgs & Kdiss evolution: part depending on sgs_mdl_force definition
    !
    IF ( mdl_form == 0 .OR. mdl_form == 1 ) THEN  ! explicit formulation (1: Chien version)         

       user_rhs(shift+1:shift+ng) = user_rhs(shift+1:shift+ng) + sgs_mdl_force(:,n_var_K)
       user_rhs(shift1+1:shift1+ng) = user_rhs(shift1+1:shift1+ng) + sgs_mdl_force(:,n_var_KD)

    ELSE IF( mdl_form == 2 ) THEN  ! semi-implicit formulation

       user_rhs(shift+1:shift+ng) = user_rhs(shift+1:shift+ng) &
                                  + sgs_mdl_force(:,n_var_K) - u_integrated(:,n_var_KD)         
!                                  + C0_RANS*AUX(:)*u_integrated(:,n_var_K)**2/u_integrated(:,n_var_KD) - u_integrated(:,n_var_KD)         
       user_rhs(shift1+1:shift1+ng) = user_rhs(shift1+1:shift1+ng) &
                                    + sgs_mdl_force(:,n_var_KD) - C2_RANS*u_integrated(:,n_var_KD)**2/u_integrated(:,n_var_K)
!                                    + C0_RANS*AUX(:)*C1_RANS*u_integrated(:,n_var_K) - C2_RANS*u_integrated(:,n_var_KD)**2/u_integrated(:,n_var_K)

    ELSE IF( mdl_form == 3 ) THEN  ! SHIH 1994

       user_rhs(shift+1:shift+ng) = user_rhs(shift+1:shift+ng) &
                                  + sgs_mdl_force(:,n_var_K) - u_integrated(:,n_var_KD)         
       user_rhs(shift1+1:shift1+ng) = user_rhs(shift1+1:shift1+ng) &
                                    + sgs_mdl_force(:,n_var_KD)*u_integrated(:,n_var_KD) &
                                    - C2_RANS*u_integrated(:,n_var_KD)**2/(u_integrated(:,n_var_K)+SQRT(nu*u_integrated(:,n_var_KD)))
    END IF


  END SUBROUTINE KEPS_rhs



  ! find Jacobian of Right Hand Side of the problem
  ! u_prev == u_prev_timestep_loc

  SUBROUTINE KEPS_Drhs (user_Drhs, u, u_prev, du, d2u, meth)
    IMPLICIT NONE
    INTEGER,                            INTENT(IN)    ::  meth
    !u_prev_timestep passed local to cast it into 2dim array to work with vector derivatives
    REAL (pr), DIMENSION (ng,ne),       INTENT(IN)    :: u, u_prev
    REAL (pr), DIMENSION (n),           INTENT(INOUT) :: user_Drhs
    REAL (pr), DIMENSION (2*ne,ng,dim), INTENT(INOUT) :: du     ! 1st derivatives for u (in du(1:ne,:,:)) and u_prev_timestep (in du(ne+1:2*ne,:,:))
    REAL (pr), DIMENSION (ne  ,ng,dim), INTENT(INOUT) :: d2u    ! 2nd derivatives for u 
    REAL (pr), DIMENSION (ng,dim)                     :: duk
    REAL (pr), DIMENSION (ng)                         :: nutK_prev

    INTEGER :: ie, shift, shift1,  idim

    REAL (pr)            :: tmp1, tmp2

    IF (verb_level.GT.0) THEN
       tmp1 = MINVAL( sgs_mdl_force(:,n_var_K) )
       tmp2 = MAXVAL( sgs_mdl_force(:,n_var_K) )
       CALL parallel_global_sum( REALMINVAL=tmp1 )
       CALL parallel_global_sum( REALMAXVAL=tmp2 )
       IF (par_rank.EQ.0) PRINT *,'MINMAX sgs_mdl_force(K) inKEPS_Drhs=', tmp1, tmp2
       tmp1 = MINVAL( sgs_mdl_force(:,n_var_KD) )
       tmp2 = MAXVAL( sgs_mdl_force(:,n_var_KD) )
       CALL parallel_global_sum( REALMINVAL=tmp1 )
       CALL parallel_global_sum( REALMAXVAL=tmp2 )
       IF (par_rank.EQ.0) PRINT *,'MINMAX sgs_mdl_force(KD) inKEPS_Drhs=', tmp1, tmp2
    END IF

    shift=(n_var_K-1)*ng
    shift1=(n_var_KD-1)*ng

    ! evaluating nu_t prev = C0*Ksgs^2/Komeg for the turbulent diffusion term
    nutK_prev(:) = C0_RANS*u_prev(:,n_var_K)**2/u_prev(:,n_var_KD)
    ! evaluating nu_t variation
    nutK(:) = C0_RANS*(2.0_pr*u(:,n_var_K)*u_prev(:,n_var_K)/u_prev(:,n_var_KD)-u(:,n_var_KD)*(u_prev(:,n_var_K)/u_prev(:,n_var_KD))**2)

    ! Ksgs evolution: common part
    !
    ie = n_var_K
    DO idim = 1, dim
       duk(:,idim) = ( nutK_prev(:)*du(ie,:,idim) + nutK(:)*du(ne+ie,:,idim) )*C3_RANS
    END DO
    user_Drhs(shift+1:shift+ng) = - SUM(u_prev(:,1:dim)*du(ie,:,1:dim),DIM=2) - SUM(u(:,1:dim)*du(ne+ie,:,1:dim),DIM=2) & 
                                     + ( nu + nuK(:) )*SUM(d2u(ie,:,:),2) &
                                     +  div(duk,ng,j_lev,meth)

    ! Kdiss evolution: common part
    !
    ie = n_var_KD
    DO idim = 1, dim
       duk(:,idim) = ( nutK_prev(:)*du(ie,:,idim) + nutK(:)*du(ne+ie,:,idim) )*C4_RANS
    END DO
    user_Drhs(shift1+1:shift1+ng) = - SUM(u_prev(:,1:dim)*du(ie,:,1:dim),DIM=2) - SUM(u(:,1:dim)*du(ne+ie,:,1:dim),DIM=2) & 
                                     + ( nu + nuK(:) )*SUM(d2u(ie,:,:),2) &
                                     + div(duk,ng,j_lev,meth)

    ! Ksgs & Kdiss evolution: part depending on sgs_mdl_force definition
    !
    IF ( mdl_form == 0 .OR. mdl_form == 1 ) THEN  ! explicit formulation (1: Chien version)         

!       user_Drhs(shift+1:shift+ng) = user_Drhs(shift+1:shift+ng) + 0.0_pr
!       user_Drhs(shift1+1:shift1+ng) = user_Drhs(shift1+1:shift1+ng) + 0.0_pr

    ELSE IF( mdl_form == 2 ) THEN  ! semi-implicit formulation

       user_Drhs(shift+1:shift+ng) = user_Drhs(shift+1:shift+ng) - u(:,n_var_KD)
       user_Drhs(shift1+1:shift1+ng) = user_Drhs(shift1+1:shift1+ng) &
                                     - C2_RANS*(2.0_pr*u(:,n_var_KD)*u_prev(:,n_var_KD)/u_prev(:,n_var_K)-u(:,n_var_K)*(u_prev(:,n_var_KD)/u_prev(:,n_var_K))**2)

    ELSE IF( mdl_form == 3 ) THEN  ! SHIH 1994

!       nutK_prev(:) = u_prev(:,n_var_K)+SQRT(nu*u_prev(:,n_var_KD))
       user_Drhs(shift+1:shift+ng) = user_Drhs(shift+1:shift+ng) - u(:,n_var_KD)
       user_Drhs(shift1+1:shift1+ng) = user_Drhs(shift1+1:shift1+ng) + sgs_mdl_force(:,n_var_KD)*u(:,n_var_KD)
!       user_Drhs(shift1+1:shift1+ng) = user_Drhs(shift1+1:shift1+ng) + sgs_mdl_force(:,n_var_KD)*u(:,n_var_KD) &                 
!                                     + C2_RANS*u(:,n_var_K)*(u_prev(:,n_var_KD)/nutK_prev(:))**2 &
!                                     - C2_RANS*u(:,n_var_KD)*( 2.0_pr*u_prev(:,n_var_KD)/nutK_prev(:) - 0.5_pr*u_prev(:,n_var_KD)**1.5_pr*nu**0.5_pr/nutK_prev(:)**2 )
    END IF

  END SUBROUTINE KEPS_Drhs

  !
  ! Uses u_prev_timestep, which is global flat (1D ) array set in time_step_cn()
  !
  SUBROUTINE KEPS_Drhs_diag (user_Drhs_diag, du_prev_timestep, du, d2u, meth)
    IMPLICIT NONE
    INTEGER,                          INTENT(IN)    :: meth
    REAL (pr), DIMENSION (n),         INTENT(INOUT) :: user_Drhs_diag
    REAL (pr), DIMENSION (ne,ng,dim), INTENT(INOUT) :: du_prev_timestep
    REAL (pr), DIMENSION (ng,dim),    INTENT(INOUT) :: du, d2u
    REAL (pr), DIMENSION (ng,dim)                   :: duk

    INTEGER :: ie, shift, idim, shift1

    REAL (pr)            :: tmp1, tmp2

    IF (verb_level.GT.0) THEN
       tmp1 = MINVAL( sgs_mdl_force(:,n_var_K) )
       tmp2 = MAXVAL( sgs_mdl_force(:,n_var_K) )
       CALL parallel_global_sum( REALMINVAL=tmp1 )
       CALL parallel_global_sum( REALMAXVAL=tmp2 )
       IF (par_rank.EQ.0) PRINT *,'MINMAX sgs_mdl_force(K) inKEPS_Drhs_diag=', tmp1, tmp2
       tmp1 = MINVAL( sgs_mdl_force(:,n_var_KD) )
       tmp2 = MAXVAL( sgs_mdl_force(:,n_var_KD) )
       CALL parallel_global_sum( REALMINVAL=tmp1 )
       CALL parallel_global_sum( REALMAXVAL=tmp2 )
       IF (par_rank.EQ.0) PRINT *,'MINMAX sgs_mdl_force(KD) inKEPS_Drhs_diag=', tmp1, tmp2
    END IF

    shift=(n_var_K-1)*ng
    shift1=(n_var_KD-1)*ng

    ! evaluating nu_t = C0*Ksgs^2/Kdiss for the turbulent diffusion term
    nutK(:) = C0_RANS*u_prev_timestep(shift+1:shift+ng)**2/u_prev_timestep(shift1+1:shift1+ng)

    ! Kdiss evolution: common part
    !
    ie = n_var_K
    CALL c_diff_diag(du, duk, j_lev, ng, meth, meth, -01)
    DO idim = 1, dim
       duk(:,idim) = nutK(:)*duk(:,idim)*C3_RANS
    END DO
    user_Drhs_diag(shift+1:shift+ng) = ( nu + nuK(:) )*SUM(d2u,2) + SUM(duk(:,1:dim),DIM=2)
    DO idim = 1, dim
       user_Drhs_diag(shift+1:shift+ng) = user_Drhs_diag(shift+1:shift+ng) - u_prev_timestep((idim-1)*ng+1:idim*ng)*du(:,idim) 
    END DO

    ! Kdiss evolution: common part
    !
    ie = n_var_KD
    duk = duk*C4_RANS/C3_RANS   ! the diagonal form is the same up to the constant multiplier
    user_Drhs_diag(shift1+1:shift1+ng) =  ( nu + nuK(:) )*SUM(d2u,2) + SUM(duk(:,1:dim),DIM=2)
    DO idim = 1, dim
       user_Drhs_diag(shift1+1:shift1+ng) = user_Drhs_diag(shift1+1:shift1+ng) - u_prev_timestep((idim-1)*ng+1:idim*ng)*du(:,idim) 
    END DO

    ! Ksgs & Kdiss evolution: part depending on sgs_mdl_force definition
    !
    IF ( mdl_form == 0 .OR. mdl_form == 1 ) THEN  ! explicit formulation (1: Chien version)         

!       user_Drhs_diag(shift+1:shift+ng) = user_Drhs_diag(shift+1:shift+ng) + 0.0_pr
!       user_Drhs_diag(shift1+1:shift1+ng) = user_Drhs_diag(shift1+1:shift1+ng) + 0.0_pr

    ELSE IF( mdl_form == 2 ) THEN  ! implicit formulation with nut constant

!!$       user_Drhs_diag(shift+1:shift+ng) = user_Drhs_diag(shift+1:shift+ng) &
!!$                                        + C0_RANS*AUX(:)*2.0_pr*u_prev_timestep(shift+1:shift+ng)/u_prev_timestep(shift1+1:shift1+ng)                         
       user_Drhs_diag(shift1+1:shift1+ng) = user_Drhs_diag(shift1+1:shift1+ng) &
                                          - C2_RANS*2.0_pr*u_prev_timestep(shift1+1:shift1+ng)/u_prev_timestep(shift+1:shift+ng)

    ELSE IF( mdl_form == 2 ) THEN  ! semi-implicit formulation

!       user_Drhs_diag(shift+1:shift+ng) = user_Drhs_diag(shift+1:shift+ng) &
!                                        + C0_RANS*AUX(:)*2.0_pr*u_prev_timestep(shift+1:shift+ng)/u_prev_timestep(shift1+1:shift1+ng)                         
       user_Drhs_diag(shift1+1:shift1+ng) = user_Drhs_diag(shift1+1:shift1+ng) &
                                          - C2_RANS*2.0_pr*u_prev_timestep(shift1+1:shift1+ng)/u_prev_timestep(shift+1:shift+ng)

    ELSE IF( mdl_form == 3 ) THEN  ! SHIH 1994

!    nutK(:) = u_prev_timestep(shift+1:shift+ng)+SQRT(nu*u_prev_timestep(shift1+1:shift1+ng))
       user_Drhs_diag(shift+1:shift+ng) = user_Drhs_diag(shift+1:shift+ng) + 0.0_pr
       user_Drhs_diag(shift1+1:shift1+ng) = user_Drhs_diag(shift1+1:shift1+ng) + sgs_mdl_force(:,n_var_KD)
!       user_Drhs_diag(shift1+1:shift1+ng) = user_Drhs_diag(shift1+1:shift1+ng) + sgs_mdl_force(:,n_var_KD) &                 
!                                     - C2_RANS*( 2.0_pr*u_prev_timestep(shift1+1:shift1+ng)/nutK(:)  &
!                                     - 0.5_pr*u_prev_timestep(shift1+1:shift1+ng)**1.5_pr*nu**0.5_pr/nutK(:)**2 ) 
    END IF

  END SUBROUTINE KEPS_Drhs_diag


  !
  ! In this routine we calculate nu_t (k-epsilon model) based on the current values of Ksgs and Kdiss
  ! and calculate the RHS forcing for the convection of Ksgs and Kdiss for the next time step.
  ! init_flag - true if we are initializing the model
  !
  ! INPUT:  tau_ij = Sij (based on u>eps+adj)   and    nu_t = Smod = sqrt(2SijSij)
  !
!  SUBROUTINE sgs_KEPS_model(u_in, tau_ij, nu_t, nlocal, init_flag, ddplus )
  SUBROUTINE sgs_KEPS_model(u_in, tau_ij, nu_t, nlocal, init_flag, lowRe_flag )
    USE util_vars        ! dA 
    USE parallel
    IMPLICIT NONE
    INTEGER,                                    INTENT (IN)    :: nlocal
    LOGICAL,                                    INTENT (IN)    :: init_flag  ! true if we are initializing the model
    LOGICAL,                                    INTENT (IN)    :: lowRe_flag ! true if low-Reynolds correction
    REAL (pr), DIMENSION (nlocal,n_integrated), INTENT (INOUT) :: u_in
    REAL (pr), DIMENSION (1:nlocal,dim*(dim+1)/2), INTENT (INOUT) :: tau_ij
    REAL (pr), DIMENSION (1:nlocal),            INTENT (INOUT) :: nu_t

!    REAL (pr), DIMENSION (nlocal,2)    :: ddplus          ! Chien version (2015)       

    REAL (pr), DIMENSION (1:nlocal,3-MOD(dim,3))  :: W
    REAL (pr), DIMENSION (1:nlocal)    :: smod2, Wmod, f2_chien, eta
    REAL (pr)                          :: tmp1, tmp2
    INTEGER                            :: i, ie

    !debug
    IF( BTEST(debug_level,1) ) THEN
       IF (par_rank.EQ.0) THEN
          IF (par_rank.EQ.0) WRITE(*,'(A)') "  "
          IF (par_rank.EQ.0) WRITE(*,'(A)') "Debug In sgs_KEPS_model()" 
       END IF
       DO i=1,n_integrated
          tmp1 = MINVAL(u_in(:,i)); tmp2 = MAXVAL(u_in(:,i))
          CALL parallel_global_sum (REALMINVAL=tmp1)
          CALL parallel_global_sum (REALMAXVAL=tmp2)
          IF (par_rank.EQ.0) WRITE(*,'(A,2(e15.7,1x))') "MINMAXVAL(u_in(:,i)) = ", tmp1, tmp2
       END DO
       IF (par_rank.EQ.0) THEN
          IF (par_rank.EQ.0) WRITE(*,'(A)') "END Debug In sgs_KEPS_model()"
          IF (par_rank.EQ.0) WRITE(*,'(A)') "  "
       END IF
    ENDIF

    IF(sgsmodel /= sgsmodel_KEPS ) THEN !problem
       IF (par_rank.EQ.0) PRINT *,'sgsmodel should be 9 to call sgs_KEPS_model()'
       IF (par_rank.EQ.0) PRINT *, 'Exiting...'
       CALL parallel_finalize; STOP
    END IF

    IF( init_flag ) THEN
       u_in(:,n_var_K) = Ksgs_init
       u_in(:,n_var_KD) = Kdiss_init
       IF (verb_level.GT.0 .AND. par_rank.EQ.0) PRINT *, 'Initialized Ksgs and Kdiss at...', Ksgs_init, Kdiss_init
    END IF

    !
    ! save Smod^2 (nu_t was Smod)
    !
    smod2(:) = nu_t(:)**2

    IF (verb_level.GT.0) THEN
       tmp1 = MINVAL( smod2(:) )
       tmp2 = MAXVAL( smod2(:) )
       CALL parallel_global_sum( REALMINVAL=tmp1 )
       CALL parallel_global_sum( REALMAXVAL=tmp2 )
       IF (par_rank.EQ.0) PRINT *,'INPUT MINMAX Smod2=', tmp1, tmp2
    END IF

    !
    ! inferior limits for Ksgs & Kdiss
    !
    u_in(:,n_var_K) = MAX(u_in(:,n_var_K),Ksgs_min)
    u_in(:,n_var_KD) = MAX(u_in(:,n_var_KD),Kdiss_min)
!    u_in(:,n_var_K) = MAX(u_in(:,n_var_K),0.0_pr)
!    u_in(:,n_var_KD) = MAX(u_in(:,n_var_KD),0.0_pr)

    !
    ! find turbulent eddy viscosity
    !
    nu_t(:) = C0_RANS*u_in(:,n_var_K)**2/u_in(:,n_var_KD)

    IF (verb_level.GT.0) THEN
       tmp1 = MINVAL( nu_t(:) )
       tmp2 = MAXVAL( nu_t(:) )
       CALL parallel_global_sum( REALMINVAL=tmp1 )
       CALL parallel_global_sum( REALMAXVAL=tmp2 )
       IF (par_rank.EQ.0) PRINT *,'MINMAX nu_t=', tmp1, tmp2, '  MINMAX nu_t/nu=', tmp1/nu, tmp2/nu
    END IF

    !
    ! apply wall damping
    !
!    IF ( lowRe_flag ) THEN
!      nu_t(:) = nu_t(:)*exp( -2.5_pr / ( 1.0_pr + 0.02_pr*u_in(:,n_var_K)**2/(nu*u_in(:,n_var_KD)) ) )
!      nu_t(:) = nu_t(:)*exp( -3.4_pr / ( 1.0_pr + 0.02_pr*u_in(:,n_var_K)**2/(nu*u_in(:,n_var_KD)) )**2 )
!    IF ( mdl_form == 1 ) nu_t(:) = (1.0_pr-exp(-0.0115_pr*ddplus(:,2))) * nu_t(:)         ! CHIEN
!    END IF

!    IF (verb_level.GT.0) THEN
!       tmp1 = MINVAL( nu_t(:) )
!       tmp2 = MAXVAL( nu_t(:) )
!       CALL parallel_global_sum( REALMINVAL=tmp1 )
!       CALL parallel_global_sum( REALMAXVAL=tmp2 )
!       IF (par_rank.EQ.0) PRINT *,'MINMAX altered nu_t=', tmp1, tmp2, '  MINMAX nu_t/nu=', tmp1/nu, tmp2/nu
!    END IF

    !
    ! calculate total SGS diss
    !
    total_sgs_diss = SUM( dA * nu_t * smod2 )
    CALL parallel_global_sum( REAL=total_sgs_diss )

    !
    ! find (-)Reynolds stresses (tau_ij was S_ij)
    !
    DO ie = 1, dim*(dim+1)/2
       tau_ij(:,ie) = 2.0_pr * nu_t(:) * tau_ij(:,ie)
    END DO
    DO ie = 1, dim
       tau_ij(:,ie) = tau_ij(:,ie) - 2.0_pr/3.0_pr*u_in(:,n_var_K)
!       tau_ij(:,ie) = tau_ij(:,ie) - 2.0_pr/REAL(dim,pr)*MAX(0.0_pr,u_in(:,n_var_K))
    END DO

    !
    ! Kato-Launder (1) or Menter (2) modification of the production term
    ! Wmod = SQRT( twoWijWij )
    !
    IF ( Prod_index == 1 .OR. Prod_index == 2) THEN
       CALL Wij( u_in(1:nlocal,1:dim) ,nlocal, W, Wmod )
       smod2(:) = sqrt(smod2(:))**(2-Prod_index) * Wmod(:)**Prod_index
       IF (verb_level.GT.0) THEN
          tmp1 = MINVAL( smod2(:) )
          tmp2 = MAXVAL( smod2(:) )
          CALL parallel_global_sum( REALMINVAL=tmp1 )
          CALL parallel_global_sum( REALMAXVAL=tmp2 )
          IF (par_rank.EQ.0) PRINT *,'MINMAX altered Prod=', tmp1, tmp2
       END IF
    ENDIF

    !     
    ! Set RHS forcing for evolution of model scalars Ksgs and Kdiss based on current vaiables
    !
    IF( mdl_form == 0 ) THEN

      sgs_mdl_force(:,n_var_K) = nu_t(:)*smod2(:) - u_in(:,n_var_KD)                               
      sgs_mdl_force(:,n_var_KD) = C1_RANS*C0_RANS*smod2(:)*u_in(:,n_var_K) - C2_RANS*u_in(:,n_var_KD)**2/u_in(:,n_var_K) 

    ELSE IF ( mdl_form == 1 ) THEN         ! CHIEN
       
!      ! wall damping factor
!      f2_chien(:) = 1.0_pr - 0.22_pr * exp ( -1.0_pr * ( u_in(:,n_var_K)**2/(6.0_pr*nu*u_in(:,n_var_KD)) )**2 )
!
!      ! as if nu_t were constant, while explicitly treating Ksgs and Kdiss,
!      sgs_mdl_force(:,n_var_K) = nu_t(:)*smod2(:) - u_in(:,n_var_KD) - 2.0_pr*nu*u_in(:,n_var_K)/ddplus(:,1)**2                               
!      sgs_mdl_force(:,n_var_KD) = u_in(:,n_var_KD)/u_in(:,n_var_K)*(C1_RANS*nu_t(:)*smod2(:)-C2_RANS*f2_chien(:)*u_in(:,n_var_KD)) &
!                                           - exp(-0.5_pr*ddplus(:,2))*2.0_pr*nu*u_in(:,n_var_KD)/ddplus(:,1)**2 

    ELSE IF ( mdl_form == 2 ) THEN
      
      sgs_mdl_force(:,n_var_K) = nu_t(:)*smod2(:)
      sgs_mdl_force(:,n_var_KD) = C1_RANS*C0_RANS*smod2(:)*u_in(:,n_var_K)

    ELSE IF ( mdl_form == 3 ) THEN         ! SHIH 1994
      
      sgs_mdl_force(:,n_var_K) = nu_t(:)*smod2(:)
      eta(:) = SQRT(smod2(:))*u_in(:,n_var_K)/u_in(:,n_var_KD)  
      sgs_mdl_force(:,n_var_KD) = MAX(0.43_pr,eta(:)/(5.0_pr+eta(:)))*SQRT(smod2(:))
    END IF

!    IF (verb_level.GT.0) THEN
!       tmp1 = MINVAL( ddplus(:,1) )
!       tmp2 = MAXVAL( ddplus(:,1) )
!       CALL parallel_global_sum( REALMINVAL=tmp1 )
!       CALL parallel_global_sum( REALMAXVAL=tmp2 )
!       IF (par_rank.EQ.0) PRINT *,'MINMAX kepsd=', tmp1, tmp2
!       tmp1 = MINVAL( ddplus(:,2) )
!       tmp2 = MAXVAL( ddplus(:,2) )
!       CALL parallel_global_sum( REALMINVAL=tmp1 )
!       CALL parallel_global_sum( REALMAXVAL=tmp2 )
!       IF (par_rank.EQ.0) PRINT *,'MINMAX kepsdplus', tmp1, tmp2
!    END IF

    IF (verb_level.GT.0) THEN
       tmp1 = MINVAL( sgs_mdl_force(:,n_var_K) )
       tmp2 = MAXVAL( sgs_mdl_force(:,n_var_K) )
       CALL parallel_global_sum( REALMINVAL=tmp1 )
       CALL parallel_global_sum( REALMAXVAL=tmp2 )
       IF (par_rank.EQ.0) PRINT *,'MINMAX sgs_mdl_force(K)=', tmp1, tmp2
       tmp1 = MINVAL( sgs_mdl_force(:,n_var_KD) )
       tmp2 = MAXVAL( sgs_mdl_force(:,n_var_KD) )
       CALL parallel_global_sum( REALMINVAL=tmp1 )
       CALL parallel_global_sum( REALMAXVAL=tmp2 )
       IF (par_rank.EQ.0) PRINT *,'MINMAX sgs_mdl_force(KD)=', tmp1, tmp2
    END IF
 
  END SUBROUTINE sgs_KEPS_model


  !****************************************************************************************
  !*                                   END of KEPS model                                  *
  !****************************************************************************************





  !****************************************************************************************
  !*                Kintetic Energy Based Models (5:DSM, 6:LKM, 7:LDKMB, 8:LDKMG)
  !****************************************************************************************

  ! Common subroutines:
  SUBROUTINE Ksgs_rhs (user_rhs, u_integrated, du, d2u)
    IMPLICIT NONE
    REAL (pr), DIMENSION (ng,ne),     INTENT(IN)    :: u_integrated !1D flat version of integrated variables without BC 
    REAL (pr), DIMENSION (n),         INTENT(INOUT) :: user_rhs
    REAL (pr), DIMENSION (ne,ng,dim), INTENT(INOUT) :: du, d2u
    REAL (pr), DIMENSION (ng,dim)                   :: duk
    
    INTEGER            :: ie, shift, i, idim
    INTEGER, PARAMETER :: meth=1
    
    
    !
    !Note: derivatives are passed to take adantage of vector wavelet transform
    !They can be done in this subroutine as well
    !
    ie = n_var_K
    shift=(n_var_K-1)*ng
    IF( mdl_form == 0 ) THEN  ! explicit formulation
       ! sgs_mdl_force(:,n_var_k) = tauijsij(:) 
       user_rhs(shift+1:shift+ng) = - u_integrated(:,1)*du(ie,:,1) &
                                    - u_integrated(:,2)*du(ie,:,2) &
                                    - u_integrated(:,3)*du(ie,:,3) &
                                    + nu*SUM(d2u(ie,:,:),2) - eps_sgs(:) + sgs_mdl_force(:,ie) 
                                    
    ELSE IF( mdl_form == 1 ) THEN  ! implicit formulation
       ! sgs_mdl_force(:,n_var_k) =  0.0_pr 
       user_rhs(shift+1:shift+ng) = - u_integrated(:,1)*du(ie,:,1) &
                                    - u_integrated(:,2)*du(ie,:,2) &
                                    - u_integrated(:,3)*du(ie,:,3) &
                                    + nu*SUM(d2u(ie,:,:),2) &
                                    - eps_sgs(:)*MAX(0.0_pr,u_integrated(:,ie))**(1.5_pr) &
                                    - kprod_coeff(:)*u_integrated(:,ie)
                                    
    ELSE IF ( mdl_form == 2 ) THEN  ! hybrid formulation (implicitly only eps_sgs) + artificial diffusion
       ! sgs_mdl_force(:,n_var_k) =  tauijsij(:)
       user_rhs(shift+1:shift+ng) = - u_integrated(:,1)*du(ie,:,1) &
                                    - u_integrated(:,2)*du(ie,:,2) &
                                    - u_integrated(:,3)*du(ie,:,3) &
                                    + ( nu + nuK(:) )*SUM(d2u(ie,:,:),2) &
                                    - eps_sgs(:)*MAX(0.0_pr,u_integrated(:,ie))**(1.5_pr) &
                                    + sgs_mdl_force(:,ie)

    ELSE IF ( mdl_form == 3 ) THEN  ! as mdl_form=2 + turbulent diffusion (by Giuliano & Oleg Nov 2012)
       ! sgs_mdl_force(:,n_var_k) =  tauijsij(:)
       DO idim = 1, dim
          duk(:,idim) = nutK(:)*du(ie,:,idim) 
       END DO
       user_rhs(shift+1:shift+ng) = - u_integrated(:,1)*du(ie,:,1) &
                                    - u_integrated(:,2)*du(ie,:,2) &
                                    - u_integrated(:,3)*du(ie,:,3) &
                                    + nu*SUM(d2u(ie,:,:),2) + div(duk,ng,j_lev,meth) &
                                    - eps_sgs(:)*MAX(0.0_pr,u_integrated(:,ie))**(1.5_pr) &
                                    + sgs_mdl_force(:,ie)

  END IF
  END SUBROUTINE Ksgs_rhs





  ! find Jacobian of Right Hand Side of the problem
  ! u_prev == u_prev_timestep_loc

  SUBROUTINE Ksgs_Drhs (user_Drhs, u, u_prev, du, d2u, meth)
    IMPLICIT NONE
    INTEGER,                            INTENT(IN)    ::  meth
    !u_prev_timestep passed local to cast it into 2dim array to work with vector derivatives
    REAL (pr), DIMENSION (ng,ne),       INTENT(IN)    :: u, u_prev
    REAL (pr), DIMENSION (n),           INTENT(INOUT) :: user_Drhs
    REAL (pr), DIMENSION (2*ne,ng,dim), INTENT(INOUT) :: du         ! 1st derivatives for u (in du(1:ne,:,:)) and u_prev_timestep (in du(ne+1:2*ne,:,:))
    REAL (pr), DIMENSION (ne  ,ng,dim), INTENT(INOUT) :: d2u        ! 2nd derivatives for u 
    REAL (pr), DIMENSION (ng,dim)                     :: duk

    INTEGER :: ie, shift, idim
    !--Form right hand side of Navier--Stokes equations


    ie = n_var_K
    shift=(n_var_K-1)*ng

    IF( mdl_form == 0 ) THEN  ! explicit formulation
       ! sgs_mdl_force(:,n_var_k) =  tauijsij(:) 
       user_Drhs(shift+1:shift+ng) = - u_prev(:,1)*du(ie,:,1) - u_prev(:,2)*du(ie,:,2) - u_prev(:,3)*du(ie,:,3) &
                                     - u(:,1)*du(ne+ie,:,1)   - u(:,2)*du(ne+ie,:,2)   - u(:,3)*du(ne+ie,:,3) &
                                     + nu*SUM(d2u(ie,:,:),2)
                                     
    ELSE IF( mdl_form == 1 ) THEN  ! implicit formulation
       ! sgs_mdl_force(:,n_var_k) =  0.0_pr 
       user_Drhs(shift+1:shift+ng) = - u_prev(:,1)*du(ie,:,1) - u_prev(:,2)*du(ie,:,2) - u_prev(:,3)*du(ie,:,3) &
                                     - u(:,1)*du(ne+ie,:,1)   - u(:,2)*du(ne+ie,:,2)   - u(:,3)*du(ne+ie,:,3) &
                                     + nu*SUM(d2u(ie,:,:),2) - kprod_coeff(:) * u(:,n_var_k) &
                                    - 1.5_pr*eps_sgs(:)*MAX(0.0_pr,u_prev(:,n_var_k))**(0.5_pr) * u(:,n_var_k)
                                    
    ELSE IF ( mdl_form == 2 ) THEN  ! hybrid formulation (implicitly only eps_sgs) + artificial diffusion
       ! sgs_mdl_force(:,n_var_k) =  tauijsij(:)
       user_Drhs(shift+1:shift+ng) = - u_prev(:,1)*du(ie,:,1) - u_prev(:,2)*du(ie,:,2) - u_prev(:,3)*du(ie,:,3) &
                                     - u(:,1)*du(ne+ie,:,1)   - u(:,2)*du(ne+ie,:,2)   - u(:,3)*du(ne+ie,:,3) &
                                     + ( nu + nuK(:) )*SUM(d2u(ie,:,:),2) &
                                     - 1.5_pr*eps_sgs(:)*MAX(0.0_pr,u_prev(:,n_var_k))**(0.5_pr) * u(:,n_var_k)

    ELSE IF ( mdl_form == 3 ) THEN  ! as mdl_form=2 + turbulent diffusion (by Giuliano & Oleg Nov 2012)
       ! sgs_mdl_force(:,n_var_k) =  tauijsij(:)
       DO idim = 1, dim
          duk(:,idim) = nutK(:)*du(ie,:,idim)
       END DO
       user_Drhs(shift+1:shift+ng) = - u_prev(:,1)*du(ie,:,1) - u_prev(:,2)*du(ie,:,2) - u_prev(:,3)*du(ie,:,3) &
                                     - u(:,1)*du(ne+ie,:,1)   - u(:,2)*du(ne+ie,:,2)   - u(:,3)*du(ne+ie,:,3) &
                                     + nu*SUM(d2u(ie,:,:),2) + div(duk,ng,j_lev,meth) &
                                     - 1.5_pr*eps_sgs(:)*MAX(0.0_pr,u_prev(:,n_var_k))**(0.5_pr) * u(:,n_var_k)

    END IF

  END SUBROUTINE Ksgs_Drhs





  !
  ! Uses u_prev_timestep, which is global flat (1D ) array set in time_step_cn()
  !
  SUBROUTINE Ksgs_Drhs_diag (user_Drhs_diag, du_prev_timestep, du, d2u, meth)
    IMPLICIT NONE
    INTEGER,                          INTENT(IN)    :: meth
    REAL (pr), DIMENSION (n),         INTENT(INOUT) :: user_Drhs_diag
    REAL (pr), DIMENSION (ne,ng,dim), INTENT(INOUT) :: du_prev_timestep
    REAL (pr), DIMENSION (ng,dim),    INTENT(INOUT) :: du, d2u
    REAL (pr), DIMENSION (ng,dim)                   :: duk

    INTEGER :: ie, shift, idim
    
    
    ie = n_var_K
    shift=(n_var_K-1)*ng

    IF( mdl_form == 0 ) THEN  ! explicit formulation
       ! sgs_mdl_force(:,n_var_k) = tauijsij(:) 
      user_Drhs_diag(shift+1:shift+ng) = - u_prev_timestep(1:ng)*du(:,1) &
                                         - u_prev_timestep(ng+1:2*ng)*du(:,2) &
                                         - u_prev_timestep(2*ng+1:3*ng)*du(:,3) &
                                         + nu*SUM(d2u,2)
                                         
    ELSE IF( mdl_form == 1 ) THEN  ! implicit formulation
       ! sgs_mdl_force(:,n_var_k) =  0.0_pr 
       user_Drhs_diag(shift+1:shift+ng) = - u_prev_timestep(1:ng)*du(:,1) &
                                          - u_prev_timestep(ng+1:2*ng)*du(:,2) &
                                          - u_prev_timestep(2*ng+1:3*ng)*du(:,3) &
                                          + nu*SUM(d2u,2) &
                                          - 1.5_pr*eps_sgs(:)*MAX(0.0_pr,u_prev_timestep(shift+1:shift+ng))**(0.5_pr) &
                                          - kprod_coeff(:)
                                          
    ELSE IF ( mdl_form == 2 ) THEN  ! hybrid formulation (implicitly only eps_sgs) + artificial diffusion
       ! sgs_mdl_force(:,n_var_k) =  tauijsij(:)
       user_Drhs_diag(shift+1:shift+ng) = - u_prev_timestep(1:ng)*du(:,1) &
                                          - u_prev_timestep(ng+1:2*ng)*du(:,2) &
                                          - u_prev_timestep(2*ng+1:3*ng)*du(:,3) &
                                          + ( nu + nuK(:) )*SUM(d2u,2) &
                                          - 1.5_pr*eps_sgs(:)*MAX(0.0_pr,u_prev_timestep(shift+1:shift+ng))**(0.5_pr)

    ELSE IF ( mdl_form == 3 ) THEN  ! as mdl_form=2 + turbulent diffusion (by Giuliano & Oleg Nov 2012)
       ! sgs_mdl_force(:,n_var_k) =  tauijsij(:)
       CALL c_diff_diag(du, duk, j_lev, ng, meth, meth, -01)
!       CALL c_diff_diag(du, duk, j_lev, ng, meth, meth, -11)
       DO idim = 1, dim
          duk(:,idim) = nutK(:)*duk(:,idim)
       END DO
       user_Drhs_diag(shift+1:shift+ng) = - u_prev_timestep(1:ng)*du(:,1) &
                                          - u_prev_timestep(ng+1:2*ng)*du(:,2) &
                                          - u_prev_timestep(2*ng+1:3*ng)*du(:,3) &
                                          + nu*SUM(d2u,2) + SUM(duk(:,1:dim),DIM=2) &
                                          - 1.5_pr*eps_sgs(:)*MAX(0.0_pr,u_prev_timestep(shift+1:shift+ng))**(0.5_pr)

   END IF

  END SUBROUTINE Ksgs_Drhs_diag





  !
  ! Modeling of Kinetic Energy Dissipation
  !
  SUBROUTINE Ksgs_dissipation(u_in,SM,S,S2,L,SMmod,Smod,Smod2,trace,W,W2,Wmod,Wmod2,ttke,nlocal,init_flag) 
    USE util_vars        ! dA
    USE parallel
    IMPLICIT NONE
    INTEGER,                                    INTENT(IN)    :: nlocal
    LOGICAL,                                    INTENT(IN)    :: init_flag !true if we are initializing the model
    REAL (pr), DIMENSION (nlocal,n_integrated), INTENT(INOUT) :: u_in
    REAL (pr), DIMENSION (1:nlocal,6),          INTENT(INOUT) :: SM, S, S2, L
    REAL (pr), DIMENSION (1:nlocal,3),          INTENT(INOUT) :: W, W2
    REAL (pr), DIMENSION (1:nlocal),            INTENT(INOUT) :: SMmod, Smod, Smod2, trace, Wmod, Wmod2  !SMmod=Smod
    REAL (PR),                                  INTENT(INOUT) :: ttke
    
    REAL(pr) :: tmp1, tmp2, tmp3


    IF (verb_level.GT.0) THEN
        tmp1 = MINVAL(nutK)
        tmp2 = MAXVAL(nutK)
        CALL parallel_global_sum( REALMINVAL=tmp1 )
        CALL parallel_global_sum( REALMAXVAL=tmp2 )
        IF (par_rank.EQ.0) PRINT *, ' (MIN,MAX)( nutK )  in Ksgs_dissipation = ', tmp1, tmp2
    END IF

    IF (Ceps_model == 1) THEN                                                  ! Bardina-like scaling

       IF (verb_level.GT.0 .AND. par_rank.EQ.0) &
          PRINT *, ' Bardina-like model for Ksgs_dissipation: max C_eps_sgs   =  ', C_eps_sgs
       
       SM(:,1) = Smod(:)**2 + Wmod(:)**2                                       ! ||Sbar||^2 + ||Wbar||^2 = 2 Sbarij Sbarij + 2Wbarij Wbarij
       CALL mdl_filt_by_type(SM(:,1),1,TESTfilter)
       IF( mdl_form == 3 ) THEN
          SM(:,1) = MAX( 0.0_pr, 0.5_pr*MAX(0.0_pr,nu+nutK(:))*(SM(:,1)-Smod2(:)**2-Wmod2(:)**2) )    ! eps_rts    (by Giuliano Nov 2011)
       ELSE
          SM(:,1) = MAX( 0.0_pr, 0.5_pr*nu*(SM(:,1)-Smod2(:)**2-Wmod2(:)**2) )    ! eps_rts
       END IF
       SM(:,2) = MAX( 0.0_pr, 0.5_pr*trace(:))**1.5_pr                         ! Ktest^(3/2)
       
       ! clipping eps_sgs = C_eps/delta
!       WHERE( C_eps_sgs*SM(:,2) < SM(:,1)*Mcoeff*delta(:) )
       WHERE( C_eps_sgs*SM(:,2) < SM(:,1)*Mcoeff*delta(:) .OR. SM(:,2)==0.0_pr )   ! by Giuliano (Oct 2011)
           eps_sgs = C_eps_sgs/delta(:)                                        ! maximum allowable coefficient
       ELSEWHERE
           eps_sgs = Mcoeff*SM(:,1)/SM(:,2)
       END WHERE
      
       IF (verb_level.GT.0) THEN
          tmp1 = MINVAL(eps_sgs*delta)
          tmp2 = MAXVAL(eps_sgs*delta) 
          CALL parallel_global_sum( REALMINVAL=tmp1 )
          CALL parallel_global_sum( REALMAXVAL=tmp2 )       
          IF (par_rank.EQ.0) PRINT *, ' (MIN,MAX)( dyn C_eps )  = ', tmp1, tmp2 
       END IF
      
       IF( mdl_form == 0 ) THEN                                                ! explicit formulation
          eps_sgs(:) = eps_sgs * MAX(0.0_pr,u_in(:,n_var_k))**(1.5_pr)
!       ELSE                      ! implicit formulation
!          eps_sgs(:) = eps_sgs(:)
       END IF



    ELSE IF (Ceps_model == 2) THEN                                             ! Germano-like scaling

       IF (verb_level.GT.0 .AND. par_rank.EQ.0) &
          PRINT *, ' Germano-like model for Ksgs_dissipation: max C_eps_sgs   =  ', C_eps_sgs
       
       SM(:,1) = Smod(:)**2 + Wmod(:)**2                                       ! ||Sbar||^2 + ||Wbar||^2 = 2 Sbarij Sbarij + 2Wbarij Wbarij
       SM(:,2) = MAX( 0.0_pr, u_in(:,n_var_k) )                                ! Ksgs
       SM(:,3) = SM(:,2)**(1.5_pr)/delta(:)                                    ! Ksgs^(3/2)/Delta
       CALL mdl_filt_by_type(SM(:,1:3),3,TESTfilter)
       IF( mdl_form == 3 ) THEN
          SM(:,1) = MAX( 0.0_pr, 0.5_pr*MAX(0.0_pr,nu+nutK(:))*(SM(:,1)-Smod2(:)**2-Wmod2(:)**2) )    ! eps_rts    (by Giuliano Nov 2011)
       ELSE
          SM(:,1) = MAX( 0.0_pr, 0.5_pr*nu*(SM(:,1)-Smod2(:)**2-Wmod2(:)**2) )    ! eps_rts
       END IF
       SM(:,2) = MAX( 0.0_pr, (SM(:,2)+0.5_pr*trace(:)) )**1.5_pr / (Mcoeff*Delta) - SM(:,3)

       ! clipping eps_sgs = C_eps
!       WHERE( C_eps_sgs*ABS(SM(:,2)) < ABS(SM(:,1)) )
       WHERE( C_eps_sgs*ABS(SM(:,2)) < ABS(SM(:,1)) .OR. SM(:,2)==0.0_pr )     ! by Giuliano (Oct 2011)
          eps_sgs = C_eps_sgs                                                  ! maximum allowable coefficient
       ELSEWHERE
          eps_sgs = MAX(SM(:,1)/SM(:,2),0.0_pr)                                ! by Giuliano (Nov 2011)
       END WHERE
    
       IF (verb_level.GT.0) THEN
          tmp1 = MINVAL(eps_sgs)
          tmp2 = MAXVAL(eps_sgs) 
          CALL parallel_global_sum( REALMINVAL=tmp1 )
          CALL parallel_global_sum( REALMAXVAL=tmp2 )       
          IF (par_rank.EQ.0) PRINT *, ' (MIN,MAX)( dyn C_eps )  = ', tmp1, tmp2
       END IF
    
       IF( mdl_form == 0 ) THEN                                                ! explicit formulation
          eps_sgs(:) =  MAX(eps_sgs,0.0_pr)*(MAX(0.0_pr,u_in(:,n_var_k))**(1.5_pr))/delta(:)
       ELSE                                                                    ! implicit formulation
          eps_sgs(:) =  MAX(eps_sgs,0.0_pr)/delta(:)
       END IF


    ELSE

       IF (verb_level.GT.0 .AND. par_rank.EQ.0) &
           PRINT *, 'no model for Ksgs_dissipation: prescribed C_eps_sgs=', C_eps_sgs
       IF( mdl_form == 0 ) THEN                                                ! explicit formulation
          eps_sgs(:) = C_eps_sgs*(MAX(0.0_pr,u_in(:,n_var_k))**(1.5_pr))/delta(:)
       ELSE
          eps_sgs(:) = C_eps_sgs/delta(:)
       END IF

    END IF
    
    
    IF (verb_level.GT.0) THEN
       tmp1 = MINVAL(eps_sgs)
       tmp2 = MAXVAL(eps_sgs)
       tmp3 = SUM(dA*eps_sgs(:))/sumdA_global
       CALL parallel_global_sum( REALMINVAL=tmp1 )
       CALL parallel_global_sum( REALMAXVAL=tmp2 )
       CALL parallel_global_sum( REAL=tmp3 )
       IF (par_rank.EQ.0) PRINT *,' C_eps_sgs/delta (MINVAL,MAXVAL,Average)  = ', tmp1, tmp2, tmp3
    END IF

  END SUBROUTINE Ksgs_dissipation





  SUBROUTINE sgs_Kbased_model(u_in,  tau_ij, nu_t,  nlocal, init_flag) 
!  SUBROUTINE sgs_Kbased_model(u_in,  tau_ij, nu_t,  nlocal, init_flag, wdist) 
    USE util_vars        ! dA
    USE parallel
    USE field            ! Spatial Adaptive Epsilon       In order to have access to global u(:,n_var_SGSD)
    IMPLICIT NONE
    INTEGER,                                    INTENT(IN)    :: nlocal
    LOGICAL,                                    INTENT(IN)    :: init_flag  ! true if we are initializing the model
    REAL (pr), DIMENSION (nlocal,n_integrated), INTENT(INOUT) :: u_in
    REAL (pr), DIMENSION (1:nlocal,6),          INTENT(INOUT) :: tau_ij
    REAL (pr), DIMENSION (1:nlocal),            INTENT(INOUT) :: nu_t       ! dynamic Smagorinsky coeffient
    
    REAL (pr), DIMENSION (1:nlocal,6) :: S, S2, L 
    REAL (pr), DIMENSION (1:nlocal,3) :: W, W2 
    REAL (pr), DIMENSION (1:nlocal)   :: Smod, Smod2, trace, Wmod, Wmod2
    REAL (pr)                         :: rescale_tau_ij, eps_scale
    REAL (pr)                         :: ttke, ttKsgs, trace_ave, tmp1, tmp2, tmp3
    INTEGER                           :: i, itmp1

!    REAL (pr), DIMENSION (1:nlocal)   :: wdist

    IF( dim /= 3 ) THEN
       IF (par_rank.EQ.0) PRINT *, 'Error, Dynamic SGS model is only defined for 3D simulations'
       IF (par_rank.EQ.0) PRINT *,' Exiting ...'
!       CALL parallel_finalize; STOP
    END IF

    IF(sgsmodel < sgsmodel_DSM ) THEN !problem
       IF (par_rank.EQ.0) PRINT *,' sgsmodel should be k-based model'
       IF (par_rank.EQ.0) PRINT *,' Exiting ...'
       CALL parallel_finalize; STOP
    END IF


    IF (verb_level.GT.0) THEN
       tmp1 = SUM(u_g_mdl_gridfilt_mask)
       tmp2 = SUM(u_g_mdl_testfilt_mask)
       tmp3 = SUM(u_g_mdl_gridfilt_mask-u_g_mdl_testfilt_mask)
       CALL parallel_global_sum( REAL=tmp1 )
       CALL parallel_global_sum( REAL=tmp2 )
       CALL parallel_global_sum( REAL=tmp3 )
       IF (par_rank.EQ.0) THEN
          PRINT *,' SUM(u_g_mdl_gridfilt_mask)                       = ', tmp1
          PRINT *,' SUM(u_g_mdl_testfilt_mask)                       = ', tmp2
          PRINT *,' SUM(u_g_mdl_gridfilt_mask-u_g_mdl_testfilt_mask) = ', tmp3
       END IF
    END IF
 
    !================== Sbar_ij and ||Sbar|| ==========================
    !================== Wbar_ij and ||Wbar|| ==========================
    L(:,4:6) = u_in(:,1:3)  
    IF(mdl_filt_type_grid == 0) THEN
       !Sij are alredy calculated if no additional filtering is done
       S = tau_ij
       Smod = nu_t
    ELSE
       ! grid filter u_in, save it in L(:,4:6)
       CALL mdl_filt_by_type(L(:,4:6),3,GRIDfilter)
       
       ! find Sij & Wij (based on grid filtered u)
       CALL Sij( L(:,4:6),nlocal, S, Smod, .TRUE. )       ! now Smod is  ||S_gridfilt||
    END IF
    CALL Wij( L(:,4:6),nlocal, W, Wmod )       ! now Wmod is  ||W_gridfilt||
    
    !================== Sbarhat_ij and ||Sbarhat|| ==========================
    ! 
    ! test filter u and save in L(:,1:3)
    ! 
    L(:,1:3) = u_in(:,1:3)  !Oleg: in older version we had L(:,4:6), 
    !this makes sence if test filteris viewed as convolution of both and 
    !not as one that contains the effect of both
    !PAUSE 'before'
    CALL mdl_filt_by_type(L(:,1:3),3,TESTfilter)

    !WRITE (*,'("01 MIN/MAXVAL(L)=",2(E17.10,X))') MINVAL(L(:,1:3)), MAXVAL(L(:,1:3))
    !debug_c_diff_fast = 3

    !
    ! find rate-of-strain and rate-of-rotation based on test-filtered u
    !
    CALL Sij( L(:,1:3), nlocal, S2, Smod2, .TRUE. )
    CALL Wij( L(:,1:3), nlocal, W2, Wmod2 )

    !========================== Lij ==========================
    !
    ! Now find Lij using grid_filtered velocity 
    !
    tau_ij(:,1) = L(:,4) * L(:,4)  ! u_in(:,1) * u_in(:,1)
    tau_ij(:,2) = L(:,5) * L(:,5)  ! u_in(:,2) * u_in(:,2)
    tau_ij(:,3) = L(:,6) * L(:,6)  ! u_in(:,3) * u_in(:,3)
    tau_ij(:,4) = L(:,4) * L(:,5)  ! u_in(:,1) * u_in(:,2)
    tau_ij(:,5) = L(:,4) * L(:,6)  ! u_in(:,1) * u_in(:,3)
    tau_ij(:,6) = L(:,5) * L(:,6)  ! u_in(:,2) * u_in(:,3)
    !
    ! The first version is Lij = (ugrid ugrid)_test - u_test u_test
    !
    IF( .NOT. DynMdlFiltLijLast ) THEN
       CALL mdl_filt_by_type(tau_ij(:,1:6),6,TESTfilter)
    END IF
    !
    ! find Lij, Note that u_test is in L(:,1:3)
    ! so the order is important here
    !
    L(:,4) = tau_ij(:,4) - L(:,1) * L(:,2)
    L(:,5) = tau_ij(:,5) - L(:,1) * L(:,3)
    L(:,6) = tau_ij(:,6) - L(:,2) * L(:,3)
    L(:,1) = tau_ij(:,1) - L(:,1) * L(:,1)
    L(:,2) = tau_ij(:,2) - L(:,2) * L(:,2)
    L(:,3) = tau_ij(:,3) - L(:,3) * L(:,3)
    !
    ! The second version is Lij = (ugrid ugrid - u_test u_test )_test
    !
    IF ( DynMdlFiltLijLast ) THEN
       IF (par_rank.EQ.0) PRINT *, ' STOPPING! ' !; STOP
       CALL parallel_finalize; STOP
       CALL mdl_filt_by_type(L(:,1:6),6,TESTfilter)
    END IF
    
    !================ Calculating other quantities  =============
    !
    ! kinetic energy (based on unfiltered u_in)
    !
    ttke = 0.5_pr * SUM( (dA(:)/sumdA_global)*SUM(u_in(:,1:dim)**2,DIM=2) )
    CALL parallel_global_sum( REAL=ttke )
    IF (verb_level.GT.0 .AND. par_rank.EQ.0) PRINT *, 'resolved KE =', ttke
 
 
    
    !
    ! artificial diffusion coefficient for k evolution
    !
    nuK(:) = C_nu_art_k*delta(:)**2*Smod(:)


    trace = SUM(L(:,1:dim),DIM=2) ! Lii


    IF (verb_level.GT.0) THEN
       tmp1 = MINVAL(nuK(:))
       tmp2 = MAXVAL(nuK(:))
       CALL parallel_global_sum( REALMINVAL=tmp1 )
       CALL parallel_global_sum( REALMAXVAL=tmp2 )
       IF (par_rank.EQ.0) PRINT *,' MINMAXVAL( nuK )         = ', tmp1, tmp2  !MINVAL( nuK(:) ), MAXVAL( nuK(:) )
    
       tmp1 = MINVAL(trace)
       tmp2 = MAXVAL(trace)
       CALL parallel_global_sum( REALMINVAL=tmp1 )
       CALL parallel_global_sum( REALMAXVAL=tmp2 )
       IF (par_rank.EQ.0) PRINT *,' MINMAXVAL( trace(Lij) )  = ', tmp1, tmp2  ! MINVAL(trace), MAXVAL(trace)
    
       itmp1 = COUNT(trace(:) < 0.0_pr)
       CALL parallel_global_sum( INTEGER=itmp1 )
    
       trace_ave = SUM(dA*trace(:))/sumdA_global
       CALL parallel_global_sum( REAL=trace_ave )
       
       IF (par_rank.EQ.0) THEN
          PRINT *,' no.times trace<0  = ', itmp1 
          PRINT *,' sgs_Kbased_model:  Lii_ave = ', trace_ave
       END IF
    END IF
    
    
    
    !
    ! initialization for Ksgs
    !
    IF( init_flag ) THEN
!       u_in(:,n_var_k) = 0.5_pr*trace(:)
       u_in(:,n_var_k) = MAX(Ksgs_init,0.5_pr*trace(:))      
       
       IF (verb_level.GT.0) THEN
          itmp1 = COUNT(u_in(:,n_var_k) < 0.0_pr)
          CALL parallel_global_sum( INTEGER=itmp1 )
          IF (par_rank.EQ.0) PRINT *,' no.times Ksgs<0  = ', itmp1
       END IF
       
       u_in(:,n_var_k) = MAX(0.0_pr,u_in(:,n_var_k))
       ttKsgs = SUM( (dA(:)/sumdA_global)*u_in(:,n_var_k) )
       CALL parallel_global_sum( REAL=ttKsgs )
       IF (verb_level.GT.0 .AND. par_rank.EQ.0) PRINT *, ' Total Ksgs ALTERED=', ttKsgs, ttKsgs/ttke
       
       IF( eps > 0.0_pr ) u_in(:,n_var_k) = u_in(:,n_var_k)/ttKsgs*( eps**2*ttke )
       IF( alpha_k > 0.0_pr) THEN 
          !rescaling, so Ksgs has prescribed value = alpha_k*(ttke+Ksgs)
          IF( ttKsgs > 0.0_pr) THEN
             u_in(:,n_var_k) = u_in(:,n_var_k)*( alpha_k*ttke/((1._pr-alpha_k)*ttKsgs) )
          ELSE
             IF (par_rank.EQ.0) PRINT *, 'ERROR: total Ksgs = 0'
             IF (par_rank.EQ.0) PRINT *,' Exiting ...'
             CALL parallel_finalize; STOP
          END IF
       END IF
       IF (verb_level.GT.0 .AND. par_rank.EQ.0) PRINT *, '....... initialized Ksgs to its average'
    END IF
    
    ttKsgs = SUM(dA*u_in(:,n_var_k))/sumdA_global
    CALL parallel_global_sum( REAL=ttKsgs )
    IF (verb_level.GT.0 .AND. par_rank.EQ.0) PRINT *, '  average Ksgs and Ksgs/Kres = ', ttKsgs, ttKsgs/ttke

    CALL Ksgs_dissipation(u_in,tau_ij,S,S2,L,nu_t,Smod,Smod2,trace,W,W2,Wmod,Wmod2,ttke,nlocal,init_flag) 

    !==================================================================================!
    !                   select differnet Ksgs-based models                             !
    !==================================================================================!
    !
    SELECT CASE (sgsmodel)
    CASE (sgsmodel_DSM )  !dynamic structure model
       CALL sgs_DSM_model(u_in, tau_ij, S, S2, L, nu_t, Smod, Smod2, trace, nlocal) 
    CASE (sgsmodel_LKM)   !"static" k-based eddy-viscosity model (LKM)
       CALL sgs_LKM_model(u_in, tau_ij, S, S2, L, nu_t, Smod, Smod2, trace, nlocal) 
!       CALL sgs_LKM_model(u_in, tau_ij, S, S2, L, nu_t, Smod, Smod2, trace, nlocal, wdist) 
    CASE (sgsmodel_LDKMB) !dynamic k-based eddy-viscosity model based Bardina scaling (LDKM-B)
       CALL sgs_LDKMB_model(u_in, tau_ij, S, S2, L, nu_t, Smod, Smod2, trace, nlocal) 
    CASE (sgsmodel_LDKMG) !dynamic k-based eddy-viscosity model based on Geramno identity (LDKM-G)
       CALL sgs_LDKMG_model(u_in, tau_ij, S, S2, L, nu_t, Smod, Smod2, trace, nlocal) 
    CASE DEFAULT
       IF (par_rank.EQ.0) PRINT *, 'Error, Unknown model type in sgs_Kbased_model(), sgsmodel = ', sgsmodel
       IF (par_rank.EQ.0) PRINT *, 'Exiting...'
       CALL parallel_finalize; STOP
    END SELECT


    !==================================================================================!
    !
    ! find SGS dissipation: -tauij Sij and store in smod(:)
    !
    smod(:) =      tau_ij(:,1)*S(:,1) + tau_ij(:,2)*S(:,2) + tau_ij(:,3)*S(:,3) + &
         2.0_pr *( tau_ij(:,4)*S(:,4) + tau_ij(:,5)*S(:,5) + tau_ij(:,6)*S(:,6))

  
    !
    ! find total negative SGS dissipation: -tauij Sij < 0 (by Giuliano on 20 Apr 2009)
    !
    total_neg_sgs_diss = 0.0_pr
    DO i = 1, nlocal
      IF ( smod(i) < 0.0_pr ) total_neg_sgs_diss = total_neg_sgs_diss + smod(i)*dA(i)
    END DO
    CALL parallel_global_sum( REAL=total_neg_sgs_diss )
    
    IF (verb_level.GT.0) THEN
       tmp1 = MINVAL( smod(:) )
       tmp2 = MAXVAL( smod(:) )
       CALL parallel_global_sum( REALMINVAL=tmp1 )
       CALL parallel_global_sum( REALMAXVAL=tmp2 )

       IF (par_rank.EQ.0) THEN
          PRINT *,' Total Negative SGS Dissipation = ', total_neg_sgs_diss
          PRINT *,' MINMAX( tauij^*sij )           = ', tmp1, tmp2 !MINVAL( smod(:) ), MAXVAL( smod(:)  )
       END IF
    END IF
 
   
    
    ! calculate total SGS diss
    total_sgs_diss = SUM( dA* smod ) 
    CALL parallel_global_sum( REAL=total_sgs_diss )




    ! Spatial Adaptive Epsilon
    IF( do_Adp_Eps_Spatial .OR. do_Adp_Eps_Spatial_Evol ) THEN     
        u(:,n_var_SGSD) = smod(:)                                  ! we can access global u(:,:) here  B/C  we  USE field   in this Subroutine
    ENDIF




    IF( do_const_diss .AND. model_rescale) THEN
       CALL const_SGS_dissipation (rescale_tau_ij, eps_scale, Smod, nlocal)    !Giuliano

       nu_t(:) = rescale_tau_ij * nu_t(:)
       tau_ij  = rescale_tau_ij * tau_ij

       !
       ! find SGS dissipation: -tauij Sij and store in smod(:)
       ! 
       smod(:) =             tau_ij(:,1)*S(:,1) + tau_ij(:,2)*S(:,2) + tau_ij(:,3)*S(:,3) &
                 + 2.0_pr *( tau_ij(:,4)*S(:,4) + tau_ij(:,5)*S(:,5) + tau_ij(:,6)*S(:,6))
       
       ! calculate total SGS diss
       total_sgs_diss = SUM( dA* smod ) 
       CALL parallel_global_sum( REAL=total_sgs_diss )
       IF (verb_level.GT.0 .AND. par_rank.EQ.0) THEN
           WRITE(*,'(A)')         ' Rescaling Ksgs & tau_ij'
           WRITE(*,'(A, e15.5 )') ' Re-Calculate total_sgs_diss:', total_sgs_diss
           WRITE(*,'(A, e15.5 )') ' % SGS diss (new)           :', total_sgs_diss/(total_resolved_diss+total_sgs_diss)
           ! rescale Ksgs
           WRITE(*,'(A)')         ' Rescaling u_in(:,n_var_K)'
       END IF
       u_in(:,n_var_k) = rescale_tau_ij**2 * u_in(:,n_var_k)
     END IF

    IF(.FALSE.) THEN !this part of the code calculates the low-pass contribution of SGS dissipation
       L = tau_ij
       S2 = S
       CALL LP_sgs_dissipation(tmp1, L, S2, Smod2, nlocal, MAX(1,j_mx-4))
       IF (verb_level.GT.0 .AND. par_rank.EQ.0) WRITE(*,'("SGS_DISS_total=",E12.5," LP_SGS_DISS_total=",E12.5)') total_sgs_diss, tmp1
    END IF

    !     
    ! Set RHS forcing for evolution of model scalar k based on current values
    ! modelling sgs energy dissipation, clipping ON/OFF?
    !

    IF( mdl_form == 0 ) THEN                   ! explicit formulation
       sgs_mdl_force(:,n_var_k) =  smod(:)  
    ELSE IF( mdl_form == 1 ) THEN              ! implicit formulation
       sgs_mdl_force(:,n_var_k) =  0.0_pr 
    ELSE IF ( mdl_form == 2 ) THEN             ! hybrid formulation (implicitly only eps_sgs) + artificial diffusion
       sgs_mdl_force(:,n_var_k) =  smod(:)
    ELSE IF ( mdl_form == 3 ) THEN             ! as mdl_form=2 + turbulent diffusion     (by Giuliano Oct 2011)
       sgs_mdl_force(:,n_var_k) =  smod(:)
       nutK(:) = MAX(0.0_pr, nu_t(:))                            ! clipped for ksgs-equation
       tmp1 = MINVAL( nutK(:) )
       tmp2 = MAXVAL( nutK(:) )
       CALL parallel_global_sum( REALMINVAL=tmp1 )
       CALL parallel_global_sum( REALMAXVAL=tmp2 )
       IF (par_rank.EQ.0) PRINT *,' MINMAX nutK                     = ', tmp1, tmp2
    END IF

    
    IF (verb_level.GT.0) THEN
       tmp1 = MINVAL( u_in(:,n_var_k) )
       tmp2 = MAXVAL( u_in(:,n_var_k) )
       CALL parallel_global_sum( REALMINVAL=tmp1 )
       CALL parallel_global_sum( REALMAXVAL=tmp2 )
       IF (par_rank.EQ.0) PRINT *,' MINMAX k                        = ', tmp1, tmp2 !MINVAL( u_in(:,n_var_k) ), MAXVAL( u_in(:,n_var_k) )
    
       tmp1 = MINVAL( sgs_mdl_force(:,n_var_k) )
       tmp2 = MAXVAL( sgs_mdl_force(:,n_var_k) )
       CALL parallel_global_sum( REALMINVAL=tmp1 )
       CALL parallel_global_sum( REALMAXVAL=tmp2 )
       IF (par_rank.EQ.0) PRINT *,' MINMAX sgs_mdl_force(:,n_var_k) = ', tmp1, tmp2 !MINVAL( sgs_mdl_force(:,n_var_k) ), MAXVAL( sgs_mdl_force(:,n_var_k) )
    END IF

  END SUBROUTINE sgs_Kbased_model





  SUBROUTINE LP_sgs_dissipation(total_lowpass_sgs_diss, tau_ij, S, Smod, nlocal, j_lowpass) 
    USE util_vars        ! dA
    USE parallel
    IMPLICIT NONE
    INTEGER,                           INTENT(IN)    :: nlocal, j_lowpass
    REAL (pr),                         INTENT(INOUT) :: total_lowpass_sgs_diss
    REAL (pr), DIMENSION (1:nlocal,6), INTENT(INOUT) :: S, tau_ij 
    REAL (pr), DIMENSION (1:nlocal),   INTENT(INOUT) :: Smod
    
    REAL (pr) :: tmp


    CALL wlt_lowpass_filt(S,6,MAX(1,j_lowpass))
    CALL wlt_lowpass_filt(tau_ij,6,MAX(1,j_lowpass))
    !
    ! find SGS dissipation: -tauij Sij and store in smod(:)
    ! 
    Smod(:) =            tau_ij(:,1)*S(:,1) + tau_ij(:,2)*S(:,2) + tau_ij(:,3)*S(:,3) &
             + 2.0_pr *( tau_ij(:,4)*S(:,4) + tau_ij(:,5)*S(:,5) + tau_ij(:,6)*S(:,6))
    
    ! calculate total SGS diss
    tmp = SUM( dA*Smod ) 
    CALL parallel_global_sum( REAL=tmp )
    total_lowpass_sgs_diss = tmp
    
  END SUBROUTINE LP_sgs_dissipation
  
  
  
  
  
  !****************************************************************************************
  !*    SGS Dynamic Structure k-based model (DSM)  with artificial diffusion for Ksgs     *
  !****************************************************************************************
  SUBROUTINE sgs_DSM_model(u_in, tau_ij, S, S2, L, nu_t, Smod, Smod2, trace, nlocal) 
    USE parallel
    IMPLICIT NONE
    INTEGER,                                    INTENT(IN)    :: nlocal
    REAL (pr), DIMENSION (nlocal,n_integrated), INTENT(INOUT) :: u_in
    REAL (pr), DIMENSION (1:nlocal,6),          INTENT(INOUT) :: tau_ij, S, S2, L
    REAL (pr), DIMENSION (1:nlocal),            INTENT(INOUT) :: nu_t, Smod, Smod2, trace
    
    INTEGER :: i


    IF(sgsmodel /= sgsmodel_DSM ) THEN !problem
       IF (par_rank.EQ.0) PRINT *,' sgsmodel should be 5 to call sgs_DSM_model()'
       IF (par_rank.EQ.0) PRINT *,' Exiting ...'
       CALL parallel_finalize; STOP
    END IF

    nu_t(:) = 0.0_pr

    !
    ! modelling sgs stresses (-tau_ij) with DS model
    ! find  kprod_coeff = (2LijSij)/Lmm   or   (2LijSij)/|L|
    !
    !
    ! DS model based upon |L| or trace or TKE_test (old values)
    !
    DO i = 1, nlocal
       IF (trace(i) .le. 0.0_pr) THEN
         tau_ij(i,:) =  0.0_pr
       ELSE
         tau_ij(i,:) = -2.0_pr*L(i,:)/trace(i)
       ENDIF  
    END DO

    kprod_coeff(:) =         -( tau_ij(:,1)*S(:,1) + tau_ij(:,2)*S(:,2) + tau_ij(:,3)*S(:,3) &
                    + 2.0_pr *( tau_ij(:,4)*S(:,4) + tau_ij(:,5)*S(:,5) + tau_ij(:,6)*S(:,6)) )

    DO i = 1, 6
         tau_ij(:,i) = u_in(:,n_var_k) * tau_ij(:,i)
    END DO

   END SUBROUTINE sgs_DSM_model





  !****************************************************************************************
  !*    SGS Dynamic eddy-viscosity model based on Ksgs scaling  ("static" coeffcients)    *
  !****************************************************************************************
  SUBROUTINE sgs_LKM_model(u_in, tau_ij, S, S2, L, nu_t, Smod, Smod2, trace, nlocal) 
!  SUBROUTINE sgs_LKM_model(u_in, tau_ij, S, S2, L, nu_t, Smod, Smod2, trace, nlocal, ddplus) 
    USE parallel
    IMPLICIT NONE
    INTEGER,                                    INTENT(IN)    :: nlocal
    REAL (pr), DIMENSION (nlocal,n_integrated), INTENT(INOUT) :: u_in
    REAL (pr), DIMENSION (1:nlocal,6),          INTENT(INOUT) :: tau_ij, S, S2, L
    REAL (pr), DIMENSION (1:nlocal),            INTENT(INOUT) :: nu_t, Smod, Smod2, trace
    
    REAL (pr) :: tmp1, tmp2
    INTEGER   :: i

!    REAL (pr), DIMENSION (nlocal,2) :: ddplus

    IF(sgsmodel /= sgsmodel_LKM ) THEN !problem
       IF (par_rank.EQ.0) PRINT *,' sgsmodel should be 6 to call sgs_LKM_model()'
       IF (par_rank.EQ.0) PRINT *,' Exiting ...'
       CALL parallel_finalize; STOP
    END IF
    
    ! SGS_Homogeneity ( SGS Problem Type )
    SELECT CASE (SGS_Homogeneity)        
    CASE (0)                                                                ! Incompressible Homogeneous Isotropic Turbulence         
        nu_t(:) = 0.060_pr*delta(:)*SQRT(MAX(0.0_pr,u_in(:,n_var_k)))       ! HT case
        
    CASE (1)                                                                ! Incompressible External Flow       
        nu_t(:) = 0.070_pr*delta(:)*SQRT(MAX(0.0_pr,u_in(:,n_var_k)))       ! EF case    
        ! nu_t(:) = 0.085_pr*delta(:)*SQRT(MAX(0.0_pr,u_in(:,n_var_k)))       ! EF case (Sohankar Re=22000)
        
    CASE (2)                                                                ! No modeling       
        nu_t(:) = 0.0_pr

!    CASE (3)                                                                ! as (1) with Van Driest damping       
!        nu_t(:) = 0.070_pr*MIN(delta(:),ddplus(:,1))*SQRT(MAX(0.0_pr,u_in(:,n_var_k)))

!    IF (verb_level.GT.0) THEN
!       tmp1 = MINVAL(  ddplus(:,1) )
!       tmp2 = MAXVAL(  ddplus(:,1) )
!       CALL parallel_global_sum( REALMINVAL=tmp1 )
!       CALL parallel_global_sum( REALMAXVAL=tmp2 )
!       IF (par_rank.EQ.0) PRINT *, ' LKM:  van driest delta  = ', tmp1, tmp2
!    END IF

    CASE DEFAULT
       IF (par_rank.EQ.0) PRINT *, 'Error, Unknown SGS_Homogeneity (SGS problem type) in sgs_LKM_model(), SGS_Homogeneity = ', SGS_Homogeneity
       IF (par_rank.EQ.0) PRINT *,' Exiting ...'
       CALL parallel_finalize; STOP
    END SELECT

        
    IF (verb_level.GT.0) THEN
       tmp1 = MINVAL(  nu_t(:) )
       tmp2 = MAXVAL(  nu_t(:) )
       CALL parallel_global_sum( REALMINVAL=tmp1 )
       CALL parallel_global_sum( REALMAXVAL=tmp2 )
       IF (par_rank.EQ.0) PRINT *, ' LKM:  MINMAXVAL( nu_t )  = ', tmp1, tmp2 !MINVAL(  nu_t(:) ), MAXVAL(  nu_t(:) )
    END IF
    
!    DO i = 1, nlocal
!      tau_ij(i,:) = 2.0_pr*nu_t(i)*S(i,:)
!    ENDDO
    DO i = 1, 6
       tau_ij(:,i) = 2.0_pr*nu_t(:)*S(:,i)
    ENDDO
!    DO i = 1, 3       !ksgs contribute to -tauij (2014)
!       tau_ij(:,i) = tau_ij(:,i) - 2.0_pr/3.0_pr*MAX(0.0_pr,u_in(:,n_var_k))
!    END DO

   END SUBROUTINE sgs_LKM_model





  !****************************************************************************************
  !*    SGS Dynamic eddy-viscosity model based on Ksgs scaling  (dynamic coeffcients)     *
  !*    Dynamic model based on Bardina-like scaling arguments (LDKMB)                     *
  !****************************************************************************************
 SUBROUTINE sgs_LDKMB_model(u_in, tau_ij, S, S2, L, nu_t, Smod, Smod2, trace, nlocal) 
    USE util_vars        ! dA
    USE parallel
    IMPLICIT NONE
    INTEGER,                                    INTENT(IN) :: nlocal
    REAL (pr), DIMENSION (nlocal,n_integrated), INTENT(INOUT) :: u_in
    REAL (pr), DIMENSION (1:nlocal,6),          INTENT(INOUT) :: tau_ij, S, S2, L
    REAL (pr), DIMENSION (1:nlocal),            INTENT(INOUT) :: nu_t, Smod, Smod2, trace
    
    REAL (pr) :: trace_ave, tmp1, tmp2, tmp3
    INTEGER   :: i, itmp1


    IF(sgsmodel /= sgsmodel_LDKMB ) THEN !problem
       IF (par_rank.EQ.0) PRINT *,' sgsmodel should be 7 to call sgs_LDKMB_model()'
       IF (par_rank.EQ.0) PRINT *,' Exiting ...'
       CALL parallel_finalize; STOP
    END IF
    
    !
    ! find S2=sigma_ij and Smod2=||sigma_ij|| (based on test filtered S_ij)
    ! note that |sigma_ij||^2 = 2 sigma_ij sigma_ij
    !
    DO i = 1, nlocal
       ! \hat{\Delta} = Mcoeff \Delta
       S2(i,:) = -Mcoeff*delta(i)*SQRT(0.5_pr*MAX(0.0_pr,trace(i)))*S2(i,:)
       Smod2(i)= ABS(-Mcoeff*delta(i))*SQRT(0.5_pr*MAX(0.0_pr,trace(i)))*Smod2(i)
    END DO
    
    !
    ! Subtract trace for Lij (in tau_ij)
    ! only makes sense in dim=3 case
    !
    IF( Lijtraceless ) THEN
       DO i=1,nlocal
          trace(i) =  trace(i)/3.0_pr
          L(i,1:3) = L(i,1:3) - trace(i)
       END DO
       trace(:) = L(:,1) + L(:,2) + L(:,3)   
            
       IF (verb_level.GT.0) THEN
          tmp1 = MINVAL(trace) 
          tmp2 = MAXVAL(trace)
          CALL parallel_global_sum( REALMINVAL=tmp1 )
          CALL parallel_global_sum( REALMAXVAL=tmp2 )       
          IF (par_rank.EQ.0) PRINT *,' LDKMB:  Lii^*(), MINMAXVAL( trace(Lij) ) = ', tmp1, tmp2 !MINVAL(trace), MAXVAL(trace)
       END IF
    ELSE
       IF (par_rank.EQ.0) PRINT *,' PAY ATTENTION: Lijtraceless EXPECTED to be TRUE for LDKMB!'
       IF (par_rank.EQ.0) PRINT *,' Exiting ...'
       CALL parallel_finalize; STOP
    END IF
    
    trace_ave = SUM(dA*trace(:))/sumdA_global
    CALL parallel_global_sum( REAL=trace_ave )
    IF (verb_level.GT.0 .AND. par_rank.EQ.0) PRINT *, 'LDKMB:  trace_ave=', trace_ave
    
    
    
    !
    ! find  = sigma_ij sigma_ij
    !
    tau_ij(:,1) = Smod2(:)**2 ! 2 sigma_ij sigma_ij
    IF (verb_level.GT.0) THEN
       tmp1 = MINVAL(tau_ij(:,1)) 
       tmp2 = MAXVAL(tau_ij(:,1))
       tmp3 = SUM(dA*tau_ij(:,1))
       CALL parallel_global_sum( REALMINVAL=tmp1 )
       CALL parallel_global_sum( REALMAXVAL=tmp2 )
       CALL parallel_global_sum( REAL=tmp3 )
       IF (par_rank.EQ.0) PRINT *, ' LDKMB:  (MIN,MAX,Average)(2 sigma_ij sigma_ij)  = ', tmp1, tmp2, tmp3/sumdA_global
    END IF
    
    
    !
    ! find Lij^*Sij and store in C_nu(:), then 1/2 C_nu/(sigma_ij sigma_ij)
    !
    tau_ij(:,2) =           L(:,1)*S2(:,1) + L(:,2)*S2(:,2) + L(:,3)*S2(:,3) &
                  + 2.0_pr*(L(:,4)*S2(:,4) + L(:,5)*S2(:,5) + L(:,6)*S2(:,6))
    !
    WHERE( tau_ij(1:nlocal,1) == 0.0_pr )
       nu_t(1:nlocal) = 0.0_pr
    ELSEWHERE
       nu_t(1:nlocal) = tau_ij(1:nlocal,2)/ tau_ij(1:nlocal,1) !Note 2 is factored into tau_ij(:,1)=2 sigma_ij sigme_ij
    END WHERE
    
    !nu_t is C_nu now
    IF (verb_level.GT.0) THEN
       tmp1 = MINVAL( nu_t(:) )
       tmp2 = MAXVAL( nu_t(:) )
       tmp3 = SUM(dA*nu_t(:))
       CALL parallel_global_sum( REALMINVAL=tmp1 )
       CALL parallel_global_sum( REALMAXVAL=tmp2 )
       CALL parallel_global_sum( REAL=tmp3 )
       IF (par_rank.EQ.0) PRINT *, ' LDKMB:  (MIN,MAX,Average)C_nu  = ', tmp1, tmp2, tmp3/sumdA_global
    END IF
    
    
    !
    !number of points with negative C_nu  
    !backscatter_frac = float(COUNT( nu_t(:) < 0.0_pr ))/nlocal 
    !IF (par_rank.EQ.0) PRINT *, 'LDKMB:  no. backscatter points', COUNT( nu_t(:) < 0.0_pr ), ' of', nlocal, backscatter_frac
    itmp1 = COUNT( nu_t(:) < 0.0_pr )
    CALL parallel_global_sum( INTEGER=itmp1 )
    backscatter_frac = float(itmp1) / nwlt_global


    !uniform C_nu given the total dissipation (by Giuliano on Oct 2011)
    IF( uniform_C_nu ) THEN
      tmp1 = SUM(delta(:)*SQRT(MAX(0.0_pr,u_in(:,n_var_k)))*Smod(:)*Smod(:)*nu_t(:)*dA)      !/sumdA_global (total_sgs_diss)
      tmp2 = SUM(delta(:)*SQRT(MAX(0.0_pr,u_in(:,n_var_k)))*Smod(:)*Smod(:)*dA)              !/sumdA_global
      CALL parallel_global_sum( REAL=tmp1 )
      CALL parallel_global_sum( REAL=tmp2 )
      IF( tmp2 /= 0.0_pr ) nu_t(:) = tmp1/tmp2
      IF (verb_level.GT.0 .AND. par_rank.EQ.0) &
         PRINT *, ' LDKMB: <C_nu*delta*k^0.5*|S|^2> <delta*k^0.5*|S|^2> C_nu = ', tmp1, tmp2, tmp1/tmp2
    END IF


    nu_t(:) = nu_t(:)*delta(:)*SQRT(MAX(0.0_pr,u_in(:,n_var_k)))


    IF( clip_nu_nut ) THEN                  !by Giuliano Oct 2011
       nu_t(:) =  MAX( nu_t(:), -nu )
    END IF

    IF (verb_level.GT.0) THEN
       tmp1 = MINVAL( nu_t(:) )
       tmp2 = MAXVAL( nu_t(:) )
       CALL parallel_global_sum( REALMAXVAL=tmp1 )
       CALL parallel_global_sum( REALMAXVAL=tmp2 )
       IF (par_rank.EQ.0) THEN
          PRINT *, ' LDKMB:  no. backscatter points  = ', itmp1, ' of ', nwlt_global, ' backscatter_frac = ', backscatter_frac
          PRINT *, ' LDKMB:  MINMAXVAL(nu)           = ', tmp1, tmp2
       END IF
    END IF
    
    DO i = 1, 6
       tau_ij(:,i) = 2.0_pr*nu_t(:)*S(:,i)
    ENDDO

   END SUBROUTINE sgs_LDKMB_model





  !****************************************************************************************
  !*    SGS Dynamic eddy-viscosity model based on Ksgs scaling  (dynamic coeffcients)     *
  !*    Dynamic model based on Germano-identity dynamic procedure (LDKMG)                 *
  !****************************************************************************************
 SUBROUTINE sgs_LDKMG_model(u_in, tau_ij, S, S2, L, nu_t, Smod, Smod2, trace, nlocal)
    USE util_vars        ! dA 
    USE parallel
    IMPLICIT NONE
    INTEGER,                                    INTENT(IN)    :: nlocal
    REAL (pr), DIMENSION (nlocal,n_integrated), INTENT(INOUT) :: u_in
    REAL (pr), DIMENSION (1:nlocal,6),          INTENT(INOUT) :: tau_ij, S, S2, L
    REAL (pr), DIMENSION (1:nlocal),            INTENT(INOUT) :: nu_t, Smod, Smod2, trace
    
    REAL (pr) :: trace_ave, tmp1, tmp2, tmp3
    INTEGER   :: i, itmp1


    IF(sgsmodel /= sgsmodel_LDKMG ) THEN !problem
       IF (par_rank.EQ.0) PRINT *,' sgsmodel should be 8 to call sgs_LDKMG_model()'
       IF (par_rank.EQ.0) PRINT *,' Exiting ...'
       CALL parallel_finalize; STOP
    END IF

    !
    ! Subtract trace for Lij (in tau_ij)
    ! only makes sense in dim=3 case
    ! trace(:) is NOT updated as we need k_test=1/2 Lii
    !
    IF( Lijtraceless ) THEN
       DO i=1,nlocal
          L(i,1:3) = L(i,1:3) - trace(i)/3.0_pr
       END DO
    ELSE
       IF (par_rank.EQ.0) PRINT *,' PAY ATTENTION: Lijtraceless EXPECTED to be TRUE for LDKMG!'
       IF (par_rank.EQ.0) PRINT *,' Exiting ...'
       CALL parallel_finalize; STOP
    END IF
    
    IF (verb_level.GT.0) THEN
       tmp1 = MINVAL(trace)
       tmp2 = MAXVAL(trace)
       CALL parallel_global_sum( REALMINVAL=tmp1 )
       CALL parallel_global_sum( REALMAXVAL=tmp2 )
       IF (par_rank.EQ.0) PRINT *,' LDKMG:  MINMAXVAL(Lii) = ', tmp1, tmp2
    END IF
    
    tmp1 = SUM(dA*trace(:))
    CALL parallel_global_sum( REAL=tmp1 )
    trace_ave = tmp1/sumdA_global
    
    IF (verb_level.GT.0) THEN 
       tmp1 = MINVAL(L(:,1)+L(:,2)+L(:,3))
       tmp2 = MAXVAL(L(:,1)+L(:,2)+L(:,3))
       CALL parallel_global_sum( REALMINVAL=tmp1 )
       CALL parallel_global_sum( REALMAXVAL=tmp2 )
       IF (par_rank.EQ.0) THEN
          PRINT *,' LDKMG:  Lii_ave           = ', trace_ave
          PRINT *,' LDKMG:  MINMAXVAL(Lii^*)  = ', tmp1, tmp2
       END IF
    END IF

    !
    ! find ||S_gridfilt|| Sij_gridfilt (6 components)
    !
    DO i=1,6
       tau_ij(:,i) = SQRT(MAX(0.0_pr,u_in(:,n_var_k))) * S(:,i)
    END DO
    IF ( deltaMij ) THEN
       DO i=1,6
          tau_ij(:,i) =  delta(:) * tau_ij(:,i)
       END DO
    END IF
    !
    ! test filter Ksgs^1/2 Sij_gridfilt 
    !
    CALL mdl_filt_by_type(tau_ij(:,1:6),6,TESTfilter)

    ! now tau_ij == ( Ksgs^1/2 Sij_gridfilt )_testfilt or ( delta Ksgs^1/2 Sij_gridfilt )_testfilt depending on deltaMij

    nu_t(:) = u_in(:,n_var_k) ! Ksgs
    CALL mdl_filt_by_type(nu_t(:),1,TESTfilter)
       !now nu_t is (Ksgs)_testfilt

    !
    ! find Mij
    !
    IF ( deltaMij ) THEN
       DO i=1,6
          ! \hat{\Delta} = Mcoeff \Delta
          tau_ij(:,i) =  tau_ij(:,i) - Mcoeff * delta(:) * SQRT(MAX(0.0_pr,nu_t(:)+0.5_pr*trace(:))) * S2(:,i) 
       END DO
    ELSE
       DO i=1,6
          ! \hat{\Delta} = Mcoeff \Delta
          tau_ij(:,i) =  tau_ij(:,i) - Mcoeff * SQRT(MAX(0.0_pr,nu_t(:)+0.5_pr*trace(:))) * S2(:,i) 
       END DO
    END IF

    !
    ! find find LijMij and MijMij
    ! (store LijMij in L(:,1) and
    ! store MijMij in L(:,2)
    !
    L(:,1) =             L(:,1)*tau_ij(:,1) + L(:,2)*tau_ij(:,2) + L(:,3)*tau_ij(:,3) &
             + 2.0_pr *( L(:,4)*tau_ij(:,4) + L(:,5)*tau_ij(:,5) + L(:,6)*tau_ij(:,6))

    L(:,2) =             tau_ij(:,1)*tau_ij(:,1) + tau_ij(:,2)*tau_ij(:,2) + tau_ij(:,3)*tau_ij(:,3) &
             + 2.0_pr *( tau_ij(:,4)*tau_ij(:,4) + tau_ij(:,5)*tau_ij(:,5) + tau_ij(:,6)*tau_ij(:,6))

    WHERE( L(:,2) == 0.0_pr )
       nu_t(:) = 0.0_pr
    ELSEWHERE
       nu_t(:) = 0.5_pr* L(:,1)/ L(:,2)
    END WHERE


    !nu_t is C_nu now
    IF (verb_level.GT.0) THEN
       tmp1 = MINVAL(  nu_t(:) )
       tmp2 = MAXVAL(  nu_t(:) )
       tmp3 = SUM(dA* nu_t(:))/sumdA_global
       CALL parallel_global_sum( REALMINVAL=tmp1 )
       CALL parallel_global_sum( REALMAXVAL=tmp2 )
       CALL parallel_global_sum( REAL=tmp3 )
       IF (par_rank.EQ.0) PRINT *, ' LDKMG:  (MIN,MAX,Average)(C_nu)  = ', tmp1, tmp2, tmp3
    END IF
    
    
    !number of points with negative C_nu  
    !backscatter_frac = float(COUNT( nu_t(:) < 0.0_pr ))/nlocal 
    !IF (par_rank.EQ.0) PRINT *, 'LDKMG: no. backscatter points', COUNT( nu_t(:) < 0.0_pr ), ' of', nlocal, backscatter_frac
    itmp1 = COUNT( nu_t(:) < 0.0_pr )
    CALL parallel_global_sum( INTEGER=itmp1 )
    backscatter_frac = float(itmp1) / nwlt_global
    IF (verb_level.GT.0 .AND. par_rank.EQ.0) &
       PRINT *, ' LDKMG:  no. backscatter points=', itmp1, ' of ', nwlt_global, ' backscatter_frac = ', backscatter_frac


    !uniform C_nu given the total dissipation (by Giuliano on Oct 2011)
    IF( uniform_C_nu ) THEN
      tmp1 = SUM(delta(:)*SQRT(MAX(0.0_pr,u_in(:,n_var_k)))*Smod(:)*Smod(:)*nu_t(:)*dA)      !/sumdA_global (total_sgs_diss)
      tmp2 = SUM(delta(:)*SQRT(MAX(0.0_pr,u_in(:,n_var_k)))*Smod(:)*Smod(:)*dA)              !/sumdA_global
      CALL parallel_global_sum( REAL=tmp1 )
      CALL parallel_global_sum( REAL=tmp2 )
      IF( tmp2 /= 0.0_pr ) nu_t(:) = tmp1/tmp2
      IF (verb_level.GT.0 .AND. par_rank.EQ.0) &
         PRINT *, ' LDKMG: <C_nu*delta*k^0.5*|S|^2> <delta*k^0.5*|S|^2> C_nu = ', tmp1, tmp2, tmp1/tmp2
    END IF


    nu_t(:) = nu_t(:)*SQRT(MAX(0.0_pr,u_in(:,n_var_k)))
    IF ( deltaMij ) nu_t(:) = delta(:) * nu_t(:)
    !nu_t is nu_t now


    IF( clip_nu_nut ) THEN                  !by Giuliano Oct 2011
       nu_t(:) =  MAX( nu_t(:), -nu )
    END IF

    DO i = 1, 6
       tau_ij(:,i) = 2.0_pr * nu_t(:) * S(:,i) !SGS stress
    END DO

   END SUBROUTINE sgs_LDKMG_model

  !****************************************************************************************
  !*                END of Kintetic Energy Based Models (DSM, LKM, LDKMB, LDKMG)          *
  !****************************************************************************************





  ! Spatial Adaptive Epsilon
  !
  !****************************************************************************************
  !*    Setting   Local_Resolved_Dissipation(:)                                           *
  !*    To        u(:,n_var_RD)                                                           *
  !*                                                                                      *
  !*    This Subroutine is called from      "SUBROUTINE  sgs_force ( u, nlocal)"          *
  !*    B/C  in  "SUBROUTINE  sgs_force",   u(:,n_var_RD) is NOT  accessible              *
  !****************************************************************************************
  SUBROUTINE  Set_Local_RD ( Local_RD_in, nlocal)
    USE field            ! In order to have access to global u(:,n_var_RD)
    USE parallel
    IMPLICIT NONE

    INTEGER,                       INTENT (IN) :: nlocal
    REAL (pr), DIMENSION (nlocal), INTENT (IN) :: Local_RD_in


    ! Spatial Adaptive Epsilon
    IF( do_Adp_Eps_Spatial .OR. do_Adp_Eps_Spatial_Evol ) THEN     
         u(:,n_var_RD) = Local_RD_in(:)
    ENDIF
    
   END SUBROUTINE Set_Local_RD




END MODULE SGS_incompressible
