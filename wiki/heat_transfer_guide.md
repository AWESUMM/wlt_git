

## тестовый запуск heat_transfer

## отображение результатов в ParaView

## heat_transfer_2D.inp

* **results_dir** - директория, в которую сохраняются выходные результаты, если данной директории нет, то ее либо нужно создать вручную, либо установить results_dir_create = T.

* **results_dir_create** 

* **IC_restart_mode** 
   1. A
   2. B

* **do_Sequential_run** - если TRUE, то стартует с последнего выходного файла(последний файл указывать не нужно)

* **coord_zone_min, coord_zone_max** Задает область, в пределах которой будет адаптироваться сетка, например, если мы укажем параметры:

`coord_min = 0.25, 0.25            #  XMIN, YMIN, ZMIN, etc`   
`coord_max = 0.75, 0.75   	 #  XMAX, YMAX, ZMAX, etc`

, то адаптация будет происходит только внутри зеленого прямоугольника, вне этой области будет постоянная сетка.
<gallery>
![coord_zone_2](uploads/20ef63398288a8b4ab66feb2c30ccd56/coord_zone_2.png)
![coord_zone_3](uploads/1b1e791521dee1d337f3656b2492724c/coord_zone_3.png)
</gallery>

* **eps_init**

* **eps_run**

* **eps_adapt_steps**

* **Scale_Meth**

* **j_mn_init**

* **j_lev_init**

* **J_FILT** - не используется для heat_transfer

* **M_vector**

* **uniform**

* **j_h, j_l**

* **tol1**

## heat_transfer.f90

$`\frac{dU\left(t\right)}{dt} = F\left((U\right)`$