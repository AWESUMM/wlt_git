[Home](home)

## Tutorial

Note, while svn uses "trunk" to denote the main code, git conventionally uses a branch called "master".  While git does not recognize anything special about "master", it is often a restricted branch and kept to be always production quality.  In the following tutorial "trunk/master" is typically used to refer to this main branch.

A list of frequent commands can be found [here](git-commands)

1.  Once you gain access to a repository, the repository must be cloned onto your local machine for editing and development:
`git clone [repo url]`
( SSH can be used to clone a repo.  Please see GitLab instructions for registering your ssh keys with your account ).
Once a repo is cloned, you will immediately have local machine access to all branches that existed at the time of cloning.

2. You will start off in a new repository on the default branch.  Switch to another branch by
`git checkout [branchname]`
All of the old SVN branches have been imported as git branches.  You can also check the main page on GitLab to see which branches are part of the repo.  When you switch branches via `git checkout`, you do not change directories.  Rather, git will apply all deltas between the current branch, and the one you are checking out.  In this way, you can really only interact with one active branch at a time.

3. Now, to develop a new feature.  You can develop in an existing branch, but often it is best to just create a new branch to correspond with the part of the code you are developing.  Go to GitLab and the main repo page.  On the left sidebar, click on "Commits", and go to the "Branches" tab.  This will list all of the current branches.  Click "New Branch" in the upper right.  Specify the branch that you want to branch from.  Unlike SVN, git views all branches as equal.  Master/trunk branches are only special in how they are treated by the user.  Git makes no differentiation. Give the new branch a unique name.  For this tutorial, we'll use the example of branch "weno" to develop a WENO finite difference scheme for AWESUMM.

4.  You will have to pull the new branch to your local machine to edit.  From the command line, type
`git pull`
You should see a summary of all that was pulled from the repo.

5. Switch to the new branch by
`git checkout weno`
You are now in a clean branch that is identical to whichever branch it was branched from.

6. Create a new file, we'll say `weno.f90`, in the typical fashion.  This file is currently untracked, and you can see that by 
`git status`
In order to add the file to the repo, execute
`git add weno.f90`
Again, check `git status` and it will show the file as now tracked and staged.

7. Now that the branch has been altered, you should commit it:
`git commit -a`
The `-a` flag means that all changes should be part of this commit.  A text editor will automatically open for you to write a commit memo.  Alternatively, you can execute 
`git commit -a -m "Added a file for the weno code"`
to directly add the memo.  Unlike SVN, you should commit often, not just when you have a new version you want to back up.  Commits keep a rolling mark of your work, so that you always have access to the most up to date code, even on other machines.  This is particularly useful when you develop on one machine, but do production runs on clusters. Since branches and commits are so cheap for git, you should use them VERY liberally.

8. Unlike SVN the commit is only on your local clone of the repo.  In order to send your current commits so that they are widely available, call
`git push`
an authenticate your account.  Now the main repo is aware of your commits.  You do not have to push after every commit, but rather you can accumulate several commits.

9.  As you develop branch `weno`, continue to commit frequently.  At some point, you may want to switch branches and work on something else.  If there are uncommited changes currently on your active branch, you will have three options:
  * `git commit -a` Commit the changes.  The changes then become part of the history.
  * `git reset --hard` Throw out the changes and rewind your branch to the last commit.  These changes cannot be retrieved once the branch is reset.
  * `git stash` Stashing drops all of the changes into a dedicated scratch area, and resets the branch to the last commit.  The stashed changes can be reapplied with `git stash apply`.  You can also apply these changes to a different branch, or have multiple stashes.  [This site](http://git-scm.com/book/en/v1/Git-Tools-Stashing) has a good introduction to stashing.

10. Now, lets say that at some point, a bug is fixed in the trunk.  To apply this upstream change, first pull the changes from the repo:
`git pull`
Then, navigate to your branch
`git checkout weno`
and merge the newly updated trunk:
`git merge trunk`
If the merge is successful, then `weno` will automatically be committed.  Any conflicts will be flagged.

11.  At some point, your branch will become stable.  It might not be ready to merge back into the trunk, but you want to use it in your project, say `SupersonicFlow`.  After pulling to make sure your local clone is up to date, navigate to the project destination branch
`git checkout SupersonicFlow`
and merge the weno functionality
`git merge weno`
As you refine `weno`, you can continually merge it to `SupersonicFlow` or any other branch where you want to use it.  This is one of the big advantages over SVN.  Since the functionality is already isolated, it can be developed in a single place, and always be readily available across all projects and for all users.  This prevents a lot of variations of lines of code from proliferating across everybody's work, and prevents code from going stale.  One person fixes a bug or makes an improvement, and everybody can instantly update.

12.  Once the branch is finished and polished, it is ready to merge back into the trunk/master.  Since branch `weno` was always developed independently, there is no staging or unwinding from a more complicated branch. 
With trunk/master as the main root of the code, it is often protected from directly merging into.  Instead, a "Merge Request" is made through the GitLab web portal (this feature is usually called a "Pull request" in most git software packages).  A merge request stages the code until it can be reviewed by someone with write access.  Go to the web portal, click "Commits" on the left sidebar and go to the "Branches" tab.  Find branch `weno`, and click on "Create Merge Request".  Indicate which branch you would like to merge into:  `wlt_git/trunk`.  The code can be reviewed through the web portal, showing all diffs between the new branch and the base.  Once approved, it can be merged with a single click, since it is already staged.

13.  After the `weno` development has finished and it has been merged to the trunk, go and delete branch `weno`.  That functionality is now available by updating through master/trunk.  If further development is necessary, a new branch can be created.

[Home](home)
