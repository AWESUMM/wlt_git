[Home](home)

Project ignore file
==

We all remember what a pain was making sure that any of the unwanted files such as `*.res`, `*.vtk` and etc, didn't snick into the repository at the time of the commit when using svn. Although, it is possible to create a kind of ignore file and provide command line option for the svn, but the procedure wasn't as transparent and easy to do as in the case of git. For such purposes git offers a helper file called `.gitignore` that might be located anywhere in the repository. You can have several such files, but keep in mind that `.gitignore` at the lower (deeper) level overrides same file at the higher (parent) level. Normal practice is to have single ignore file in the root of the project and provide exclusions for the entire branch. Those files will not be tracked in the future, unless file is already being tracked. If the latter is the case, the you need to delete the file, before it will stop being tracked. 

File format
--

Ignore file has a nice and simple format of wildcard based patterns. For example
```
# data files
*.res
*.vtk
*.dat
# results folder, excludes everything in it, at any level of deepness
results
# images and documents
*.png
*.jpg
*.pdf
*.eps
*.ps
*.gif
# archives
*.rar
*.zip
*.tar
*.gz
*.bz2
*.7z
# output, object, module files
*.out
*.o
*.mod
# log file
*.log
*.txt
# some random folder we want to exclude
path1/path2/random_folder
```

As you may see, comments are allowed by using `#` sign at the beginning of the line. All paths are relative to the `.gitignore` file. If you get a lot of auxiliary files during the compilation or runtime add it to the `.gitignore`.

Both svn and git (or any other file versioning piece of software for that matter) advocates having only text data in the repository. Deltas, chunk of data that after applying changes one file to another or, more appropriately changes the file from one version to another, has the smallest size for the text files, for the same reason text files can be compressed better than any other binary type files. Otherwise, you might end up having large and clunky repos. But sometimes, even text files can be large. For example `*.log` files. You probably don't want to include that to the repo. Rule of thumb is always give your log files `*.log` extension and add this pattern to your `.gitignore`. Sample ignore file is below.

[sample.gitignore](https://gitlab.com/AWESUMM/wlt_git/uploads/a69f6e09444f8d3d5cba7e444a364706/sample.gitignore)

More information can be found [here][1].

[1]: http://git-scm.com/docs/gitignore