[Home](home)

Obiovusly, list of software will differ from platform to platform, so below are subjectively most common ones for all three major operating systems (click operating system name, not the software name).

1. [Mac](GUI/mac-git-gui) ![apple_icon_larger](https://gitlab.com/AWESUMM/wlt_git/uploads/bec235dd79266f0dd1f0657df36a3ef5/apple_icon_larger.png) &mdash; [Atlassian Sourcetree][atlassian] ![atlassian](https://gitlab.com/AWESUMM/wlt_git/uploads/1c77412e38bcdd301ee7ddcc0df0376d/atlassian.png)
2. [Lnx](GUI/linux-git-gui) ![297876-linux-logo](https://gitlab.com/AWESUMM/wlt_git/uploads/ac0a6c96531a58ea6e5e9651aed98a24/297876-linux-logo.jpg) &mdash; [Syntevo SmartGit][smartgit] ![home-smartgit](https://gitlab.com/AWESUMM/wlt_git/uploads/cbe80c24a302030c78eb5f6d418f628d/home-smartgit.png)
3. [Win](GUI/win-git-gui) ![Windows_logo_-_2012.svg](https://gitlab.com/AWESUMM/wlt_git/uploads/b800bcd8c0124fa156400077fb655b4c/Windows_logo_-_2012.svg.png) &mdash; [Atlassian Sourcetree][atlassian] ![atlassian](https://gitlab.com/AWESUMM/wlt_git/uploads/1c77412e38bcdd301ee7ddcc0df0376d/atlassian.png)

[atlassian]: https://www.atlassian.com/software/sourcetree/overview
[smartgit]: http://www.syntevo.com/smartgit/

[Home](home)