[Home](home)

## Effective Use of Git When All You Know is SVN

Git is a much more dynamic approach to developing a codebase than svn.  Aside from simply maintaining a backup, git helps to effectively organize code that is being worked on by many people across many machines.  While SVN leaves most of the organizational work to the user and is typically used for checkpointing versions of a relatively small number of branches, Git facilitates all of the heavy lifting internally.

While Git can be used in a fashion that is very similar to SVN, it is much more powerful and has many more features.  Rather than developing a new feature in a larger branch or on a locally controlled copy, a dedicated branch is created so that all intermediate work is tracked through the whole process and easily distributed to other users and uploaded to different machines.  Git all but eliminates the mess of local working copies with names like `trunk_new_updated_solver2`, or losing minor edits that were made during production runs on a cluster.

### Branches are Cheap
Actually, git branches are free, and should be treated as such.  Since only the deltas are logged across the entire repository, there is essentially no overhead to creating a new branch.  Branches should therefore be used to organize code to a fine granularity. They can be created for bugfixes, single feature development, collaborative user cases, etc.  By creating a very large number of branches, new features can be developed in isolation to aid in collaboration, code sharing, and eventual merging back to the main master/trunk.  It also helps in code organization where a developer might be working on several different projects at the same time.

Unlike previous development of AWESUMM on svn, where each branch is a fully contained copy of the code, and each commit is ideally a stable version, git branches are a work in progress on some piece of functionality, a bug, or a project.  The ability to move to a much higher granularity is intended to cut down drastically on manual organization.  Many branches can be merged to add new functionality into a single project.  And since the main git repo should (ideally) be the most up to date code, git is a convenient way of moving versions of the code onto clusters and migrating changes between machines.  

Make lots of branches, commit frequently, and keep updated.


### Working on Code in a Git Repo

The most noticeable difference between working on git and svn, is that with svn there is an individual directory for each branch.  With git there is only a single directory containing the repo.  When you switch between branches with
 
`git checkout [branchname]`

all of the deltas are applied to the version controlled files and subirectories, leaving you with the new branch.  Rather than checking out multiple copies and branches to juggle changes, everything is organized concisely behind the scenes.  For any side-development on a current project, just create a new branch (or a branch from a branch).  The goal is to move from a bunch of working copies of SVN branches

`trunk/`

`trunk_clean/`

`trunk_2/`

`john/`

`john_turbulenceModel/`

`john_turbModel2/`

to a single directory containing the full codebase.  Note that once you have cloned the repository, all of the difference branches are available to you on your local machine.

Another important note is that, since each branch has fewer changes, it becomes much easier to continually update from the trunk and fewer things can go wrong.

Remember: Make lots of branches, commit frequently, and keep updated.

## Example Workflows

Since branches and commits are so cheap and fine grained, there is a lot of flexibility to aid in development and reduce the amount of time that is spent organizing and moving files.

A tutorial, demonstrating a basic workflow with the corresponding git commands can be found here.

* Feature Development
  1. Say a new feature becomes necessary for a project, such as a time integration method.  Rather than develop this in your general project branch, you create a new branch from the main trunk/master since it is a piece of core functionality.  
  2. In the course of writing, testing, and debugging, you find an error that goes back to the trunk.  So you create another branch to fix the bug, and you merge the change back to the trunk so that everyone benefits.  Once done, you delete the branch.  
  3. At some point, the new time integrator is robust and functional, though it might not be ready to merge back to the trunk yet.  You merge it to your project branch to put it into use.  As you refine and refactor and clean up the feature, you can continually merge it to your project.  Additionally, other users can merge the new time integrator into their projects, eliminating the burden from you to clean up, isolate the code, and distribute it.

* Exploratory development
  1.  You get an idea for an improvement.  You create a branch from trunk.  
  2.  Your idea doesn't work.  
  3.  You delete the branch.

* Project Organization
  1.  You have a project to develop: Detonation simulations, turbulence model, tsunami simulations, robust volume penalization approach, etc.  This project will likely require many different features to be developed, and many related user cases.  You create a branch for this project.  This branch is analogous to the svn user branches used previously.
  2.  As discrete features are required (IMEX time integration, weno methods, curvilinear coordinate systems, nonlinear elliptic solvers, etc.), they are developed in individual, dedicated branches whenever possible, and merged into the project branch.  This way they can be co-opted and shared with other projects, and other users, with minimal effort.
  3.  Project branches might have very long lifecycles.  As user cases and features reach maturity, they can be staged in side branches and merged back into the trunk for general use.  A project branch might easily evolve into a special-purpose code that is worth preserving.

* Project Collaboration
  1. To collaborate on a project, you create a new branch.  An example might be a user case that makes use of ongoing research by multiple people.  This provides a common area to develop the code by merging feature branches and direct editing.

[Home](home)