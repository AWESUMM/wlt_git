[Home](home)

### Frequent Git Commands

Descriptions and additional useful information are given.  Operations that are purely local (i.e. do not involve immediate interactions with the repository on GitLab) are noted.  A tutorial demonstrating the use of these commands can be found here.

`git clone [repo address]`
Clone a repo.  This is analogous to `svn checkout`.  It clones a repo onto your local machine, including all of the current branches.  You should only need to clone a repo once on any machine, and just create additional branches for any scratch space or staging.

`git checkout [branch name]`
LOCAL OPERATION. Switch to a different branch.  Cannot be done if there are uncommited changes on the branch.  The changes should first be commited, reset, or stashed for later.  Any untracked files will be retained.  

`git commit`  
LOCAL OPERATION. Typically `git commit -a` to commit all staged changes. Commit any changes to tracked files on the current branch.  The commit is purely local to the machine.  Any commits will need to be pushed to the main repository before they are available.  If the `-m` flag is not used to add a commit message on the command line, a text editor such as vim will typically open to add a message.  Closing the text editor will append the message and finish the commit.  A commit, when used in conjunction with a `git push`, is analogous to `svn ci`. 

`git push`
Push any local commits to the repository.  Usually this command will push any commits from ALL branches, not just the current branch.  Pushing commits is analogous to `svn ci`.

`git pull`
Pull any changes to the repository on to your local machine.

`git merge [source branch]`
LOCAL OPERATION. Merge the source branch into your current branch.  Not that this is a pulling operation; you cannot merge your current branch into another. Any conflicts are flagged.  Conflicting files can be listed by the `git status` command.  Once conflicts are resolved, the (previously) conflicting files must be cleared by `git add [filename]`.  If `merge` is accomplished successfully, the change is automatically committed.  If there are conflicting files, the changes must be committed manually once they are resolved.

`git add [filename]`
LOCAL OPERATION. Start tracking a file.  

`git reset --hard`
LOCAL OPERATION.  Reset all uncommited local changes to the last commit.

`git reset --hard HEAD~1`
LOCAL OPERATION.  Reset all uncommited local changes and rewind by one commit.

`git stash` 
LOCAL OPERATION.Stashing drops all of the changes into a dedicated scratch area, and resets the branch to the last commit.  The stashed changes can be reapplied with `git stash apply`.  You can also apply these changes to a different branch, or have multiple stashes.  [This site](http://git-scm.com/book/en/v1/Git-Tools-Stashing) has a good introduction to stashing.

#### Branches

`git branch [branch name]`
LOCAL OPERATION.  Create a local branch.  The remote of this branch will have to be set manually to become part of the main repository.  Usually, it is just best to create new branches through the GitLab web portal.  Local branches can be added to the main repository at any point.

`git branch -d [branch name]`
LOCAL OPERATION.  Delete a local branch.  If development on a branch has been finished (code feature polished, bug fixed, project done) and the branch has been merged upstream, it is best practice to delete the branch.  Usually this is done through the GitLab web portal.  A local delete is used to keep your machine clean.

[Home](home)