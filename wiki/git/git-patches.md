[Home](home)

## Git Patches: A Way out of Trouble


Git patches are a way of breaking down individual commits in a branch for fine grain control and portability.  This is another primary reason why you should commit often and write descriptive commit messages. A patch is simply all of the changes, additions, deletions, etc. that were made in a single commit.  

You can use it to extract single commits and move them between branches and machines.  Some examples:
1.If there are technical difficulties with a computer, and you are unable to push several commits, you can create a patch, send the files to another computer, and manually apply them and push from that local branch.  
2.If there is a single bugfix in a branch, but you can't/don't want to merge the full branch, you can create a patch from the individual commit and apply it elsewhere.

The basic workflow:
1. Navigate to the branch (`git checkout <branch>`) that contains the commits.  
2. Create patches from all open commits (each commit to an individual, chronologically numbered patch): `git format-patch <branch_to_compare_to>`.  If your branch was created off of the trunk, the command would be `git format-patch trunk`. You can read the contents of the commit in plain text (vim, emacs, Notepad).
3. Move to the desired location to apply the patch (email, sftp, rsync, git checkout...), and checkout the destination branch.
4. Verify the patch.  You can view a summary of the deltas `git apply --stat <patch_filename>`. You can test the application of the patch and check for errors and conflicts by `git check --stat <patch_filename>`
5. Apply the patch:  `git am --signoff < <patch_filename>`

More tutorials and information can be found [here](http://rypress.com/tutorials/git/patch-workflows) and [here](https://ariejan.net/2009/10/26/how-to-create-and-apply-a-patch-with-git/)