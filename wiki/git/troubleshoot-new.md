## Troubleshooting Common Issues Setting Up A Git Repo

Note: This is a living document, and should be added to as difficulties are found

### Setting up Multiple SSH Keys

Often, there are existing ssh keys on a machine.  When you run `ssh-keygen -t rsa -C "[gitlab-email]"`, you will be prompted to overwrite the default `~/.ssh/id_rsa.pub`.  DO NOT DO THIS.  Instead, give it a unique name.  You can find detailed instructions on how to configure multiple ssh keys (with special attention to git) [here](https://coderwall.com/p/7smjkq/multiple-ssh-keys-for-different-accounts-on-github-or-gitlab).

### Checkout a New Branch

`error: pathspec 'branchname' did not match any file(s) known to git.`

Often this error shows up with you have cloned the git repo onto a new machine.  This means that a local branch has not yet been created to correspond with the branch in the git repository (remember, that with git, the local copy IS a fully functional repo that we have connected with the repo on our server, GitLab).  In this case, the workflow, starting from the clone is the following.  If you have already cloned, skip to step 2 or 3:

1. `git clone [repo-url]`
2. `git checkout [my-branchname]`
  * `error: pathspec 'my-branchname' did not match any file(s) known to git.`
3. First try `git show-ref`, and search for your branch.  It usually has a prefix `refs/remotes/origin/my-branchname`
4. Run `git checkout -t -b my-branchname origin/my-branchname`

This should create the local branch and connect it to the corresponding branch on the server.
This fix was found [here](https://coderwall.com/p/zcozuq/git-error-pathspec-develop-did-not-match-any-file-s-known-to-git)