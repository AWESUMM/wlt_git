
[[_TOC_]]

# AWESUMM manual

1. [Introduction](manual/Introduction)

---

# Zoltan library installation

1. [How to setup zoltan library](How to setup zoltan library)

# Visualization

1. [Paraview: automatic parallel VTK](https://gitlab.com/AWESUMM/wlt_git/wikis/paraview-automatic-parallel-vtk-loading)
1. [Paraview: remote visualization](https://gitlab.com/AWESUMM/wlt_git/wikis/paraview-remote-visualization)

---

# Using Git

1. [Git: effective use](git/effective-git)
1. [Git: commands cheat sheet](git/git-commands)
1. [Git: Tutorial](git/git-tutorial)
1. [Git: ignore file](git/git-ignore)
1. [Git: Patches](git/git-patches)

# Useful Application

1. [Git GUIs](git/git-guis)
1. [Git Sourcetree](https://gitlab.com/AWESUMM/wlt_git/wikis/GUI/mac-git-gui)

# Troubleshooting

1. [Troubleshooting Setup on a New Machine](git/troubleshoot-new)

# Reporting Bugs

If you think you have found a bug, please open an entry at the [GitLab issue tracker](https://gitlab.com/AWESUMM/wlt_git/issues).
