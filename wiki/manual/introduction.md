Welcome to the world of numerical modeling using the computationally
efficient, dynamically adaptive, Adaptive Wavelet Environment for Simulating Universal Multiscale problems (AWESUM). The numerical algorithm used in this method is a finite 
difference based scheme which uses an adaptive wavelet transform in 
conjunction with a  prescribed error threshold parameter $` \epsilon `$ 
to determine which grid points are needed in the solution. By reducing 
the number of points needed to solve the numerical solution, the 
computational cost is significantly reduced resulting in large data 
compression. It has been shown that the error within the solution using
this technique is bounded by $`C \epsilon`$, where $`C`$ is usually around 
$`\sim 10`$ \cite{donoho:1992}. The error threshold parameter $`\epsilon`$
is prescribed beforehand and is used throughout the entire simulation.
It is recommended that the equations be non-dimensionalized for the
numerical algorithm even though $`{\mathcal L}_\infty`$ and 
$`{\mathcal L}_2`$ normalization techniques are used  to further scale 
the equations. Typical values for the error threshold parameter 
$`\epsilon`$ range from $`10^{-6}`$ to $`10^{-1}`$, anything below that range
usually decreases the compuational efficiency significantly and anything
above that range has significant error that isn't easily ignored.

The wavelets used in the wavelet transform can be of any even order
of accuracy. Each wavelet's accuracy is determined by its stencil size,
which is typically prescribed in terms of the number of points used on 
each side of the point being calculated. Two stages are used to calculate
the wavelet coefficients; a predict stage and an update stage. The number
of points used on each side in the predict stage are called **n_predict** 
and the number used during the update stage are called **n_update**. 
In order to ensure the wavelet properties are maintained, these two 
parameters should be equal. Similarly, the stencil size for derivative 
calculations is prescribed in the same way. Since the wavelets support is 
naturally centered, the default setting for derivative calculations is 
centered as well. The stencil size for a centered derivative calculation is 
**2*n_diff** where **n_diff** is the number of points on each 
side of the location being calculated. In practice, all three of these 
parameters are normally the same to achieve the highest performance and 
accuracy.

The AWCM is set up to solve two main types of partial differential
equations (PDEs), elliptic and time evolving or a combination of the
two. The elliptic solver can be used to solve for the initial condition
used in a time evolving problem, or can be used solely on its own. The
time evolving portion can also be supplied with an analytical initial
condition and uses either a Krylov or Crank-Nicolson implicit time
integrator. Due to the fact that the time integrators are implicit
algorithms, the only time constraining parameter used in the code is
the CFL condition. As long as $`{\rm CFL} \le 1`$ the time integrator
should function properly and the solution will remain within the 
anticipated error margin. 
 
The grid used with the AWCM can be described as if it had two parts,
a base grid to build upon and several further levels of refinement. The
level of resolution at any point in time is described by the variable
**j_lev**. For any simulation, minimum **j_min** and maximum 
**j_max** levels of resolution must be specified. The base grid
size is prescribed with the **M_vector** variable. Combining all
of these parameters will give a grid with a minimum and maximum grid
sizing. For example, if a 2-D simulation is to be performed using a 
domain of dimension $`2.0 \times 1.0`$ with periodic boundary conditions
in the y-direction and the aspect ratio is desired 
to be unity, the following could be a suitable grid setup:


  - dimension = 2 
  - $`M_{\rm vector} = 8, 4, 0`$ 
  - $`periodic = 0, 1, 0`$ 
  - $`j_{\rm min} = 2`$ 
  - $`j_{\rm max} = 7`$. 


This sets the dimensionality of the simulation to a 2-D simulation so that
the third index in the **M_vector** and **periodic** vector are ignored.
The base grid of $`8 \times 4`$ is on the **j_lev=1** level, but because
the solution is not periodic in the x-direction the base grid is really
$`9 \times 4`$. However, the minimum level of resolution is set to be $`2`$ 
so the coarsest grid possible in this simulation will be 
$`\rm M_{\rm vector}2^{j_{\rm min}-1}+periodic = 17, 8`$ corresponding to
a grid spacing $`h=2/16=1/8=0.125`$ and a total of $`136`$ points. Similarly 
the finest grid possible in this simulation will be
$`\rm M_{\rm vector}2^{j_{\rm max}-1}+periodic = 513, 256`$ corresponding to a 
grid spacing of $`h=2/512=1/256\simeq 3.9 \times 10^{-3}`$ and a total of 
$`131328`$ points. Throughout the solution the resolution will change
such that the finer levels of resolution will be used in the regions
where localized structures are present. If the simulation only uses 
$`10,000`$ points it would only be using $`7.6\%`$ of the points available
and have a compression ratio of $`13.1`$.

The rest of this manual will try to familiarize you with the codes organization
and how a user can create their own simulations. A brief overview of the codes
organization will be given in Section \ref{sec:structure} and point out which 
files need to be modified and others that should be left untouched. Section
\ref{sec:compile} will demonstrate how to obtain the code using subversion, set
up a makefile, compile and run the code. Section \ref{sec:visual} will show
how to compile and use the post processing tools for data visualization. Finally
Section \ref{sec:case} will proceed with the details of the case files, how they 
are used and how to customize them to any particular situation.