
# GIT

`git` is employed as a version-control system.
Please follow these rules for `git` and GitLab:
1. Start from one of [issues](https://gitlab.com/AWESUMM/wlt_git/issues) or create a new one to formulate the problem.  Skip this step for a minor contribution.
1. Minimize the scope of a Merge Request (MR). Split a MR if possible.
1. Keep your MR up-to-date by rebasing your branch on the target branch (usually `trunk`).
1. Follow [Conventional Commits Specification](http://conventionalcommits.org/) for commit messages.
1. Name individual branches as `user/feature`.
1. Comment merge requests instead of individual commits in GitLab.

For maintainers only:
1. Merge into the `trunk` branch only after the approval of at least one maintainer.
1. Preserve a [semi-linear history of commits](https://github.com/isaacs/github/issues/1017).
1. Prefer squashing in case of _messy_ commits.
1. Use "Merge with Merge Commit" as a default option via GitLab interface. Note that there is [no confirmation button in GitLab](https://gitlab.com/gitlab-org/gitlab-ce/issues/19430). Double-check the history of commits and description of the _merge commit_, which usually contains list of improvements/fixes as well as references to the issues addressed.
1. Use fast-forward merges for small features or bug fixes via command line tools.
Double-check the `trunk` branch before pushing it to the GitLab repository. 
