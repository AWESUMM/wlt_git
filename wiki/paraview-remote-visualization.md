If you wish to visualize the data you have on another system (an HPC node, for example) using PARAVIEW and you don't want to transfer the files to your machine, then you have the following two options:
1.  **Combined Server Mode**
The user runs a client (ParaView GUI) on user's computer and use ssh tunneling as described below to connect to remote machine running on a separate remote node. In the first step, you need to submit a job on the remote machine to have access to a node. Once you are on the requested node, run the following commands:

*module load gnu-openmpi*

*mpirun -n 2 pvserver*

The pvserver will start and you will receive a message similar to the following:

Waiting for client...
Connection URL: cs://hpc-tc-2.local:11111
Accepting connection(s): hpc-tc-2.local:11111

Now, launch ParaView GUI on YOUR computer. Make sure you have the same version of the PARAVIEW as the one on the remote server. Then, in a separate terminal, create an ssh tunnel using the command,
*ssh -X -N -L localhost:11111:**<main server node>**:11111 <USER id>@remote_machine*

where in the above example the *<main server node>* is *hpc-tc-2.local*. Then, launch ParaView GUI, click File > Connect > Add server

*Name:your server name (doesn't matter)*

*Server Type: Client/Server*

*Host:localhost*

*Port:11111*

Click Configure and select manual from Startup and click save. Now you are ready to run ParaView in combined server mode where all processing happens on HPC compute nodes and you have access to the remote files as well.

2.  **Reverse Connection Mode**
This mode is easier because all you need to do is to make sure that you have the same version of PARAVIEW on your local machine and remote server. Then, you need to run a simple script on the remote server which is: 

`#Paraview directory
PARAVIEW=/where_PARAVIEW_is_installed/ParaView-5.5.0-Qt5-MPI-Linux-64bit/bin

#Set IP address
if [ "$1" != "" ]; then
        IP=${1}
else
        IP=144.174.182.180
fi

#Set number of cores, if specified
if [ -z "${PBS_NP}" ]; then
        Ncores=1
else
        Ncores=${SLURM_NTASKS}
fi
echo "Running with ${Ncores} tasks"

#Call pvserver
${PARAVIEW}/../lib/mpiexec -n ${Ncores} ${PARAVIEW}/pvserver -rc --client-host=${IP}`

All you need to do is to change the PARAVIEW directory and the number of cores you wish to use (default is 1). Then copy the above code in a file and save it on the server. Make the file executable by running in the bash (Using chmod u+x scriptname make the script executable). On you local machine, open Paraview and click on the Connect>Add a server. Then specify a name and in the Server Type click on the reverse connection. Then click configure it and try connect to the server. If everything is done properly you will be able to connect to the server.

If you have a problem or question about the above guidelines, please do not hesitate to send me an email: maslani@fsu.edu. 