# Overview
Zoltan is a toolkit of parallel services for dynamic, unstructured, and/or adaptive simulations. Zoltan provides parallel dynamic load balancing and related services for a wide variety of applications, including finite element methods, matrix operations, particle methods, and crash simulations. Zoltan also provides parallel graph coloring, matrix ordering, unstructured communication tools, and distributed data directories.
These tools are provided in an easy-to-use toolkit that is callable from C, C++, and Fortran90. In case of wlt_git calling of Zoltan library happens from C++ code.

More detailed information you can find in this [link](http://www.cs.sandia.gov/zoltan/Zoltan_phil.html).

# Installing. 
1. Download library [here](http://www.cs.sandia.gov/Zoltan/) and unzip it. The name of folder will be like __Zoltan v3.83__. In the time of writing this instructions this version was last.
2. Save unzipped folder in convenient place, e.g. in /home/username/ 
3. Create a subfolder BUILD_DIR in  __Zoltan v3.83__.
4. Write a script file, where we pass parameters to execute another file ./configure and compile all together  :
 * Create a file ZoltanWLT.sh   
 * Write next in it:

         ```cmake
           ../configure \
           --enable-f90interface
           make everything
           make install 
          ```

 * Give this file permission to be executed for example using 

```Console 
      chmod 777 ZoltanWLT.sh 
```

5. Run this script ./ZoltanWLT.sh.
6. Wait a minute until it ends.
7. Next step is choose and configure your machine.specific [file](https://gitlab.com/AWESUMM/wlt_git/tree/trunk/makefiles.machine_specific). For this purpose write paths to library and include files.
For Ubuntu it can be next:

 ```cmake 
      SUPPORT_ZOLTAN=YES
      ZOLTANLIB= -L/home/weugene/Zoltan_v3.83/BUILD_DIR/src -lzoltan
      ZOLTANINC= -I/home/weugene/Zoltan_v3.83/BUILD_DIR/include 
 ```

For MacOS:
```cmake
      SUPPORT_ZOLTAN=YES
      ZOLTANLIB= -L/Users/weugene/Zoltan_v3.83/BUILD_DIR/src -lzoltan
      ZOLTANINC= -I/Users/weugene/Zoltan_v3.83/src/include
```
8. Check does it work or is it faulty. Let's write next in the root directory of project wlt_git. 

```cmake
     make clean
     make CASE=PATH_TO_YOUR_CASE DB=db_tree MP=1 all
```
where MP=1 turns on parallel code. If project can be compiled move to next point.
9. Next step is to say execution code use Zoltan method of domain decomposition (2-4). Open your CASE input file *.inp file, change domain decomposition information (or if you don't have such lines copy below piece of code and add it at the end of file).

```cmake
###############################################################################################################################################################
#  P a r a l l e l     D o m a i n      D e c o m p o s i t i o n
#
domain_debug  =F
domain_split  = 1, 0, 0      # Parallel domain decomposition: 1 allows subdivision in the direction (x,y,z,etc) (nonzero - allow, 0 - do not split that direction)

domain_meth  = 3      # domain meth decomposition is based on . . .
                      #       0 (default) geometric simultaneous - based on prime number
                      #       1 geometric sequential (recursive pd N) based on N^(1/D) subdivision
                      #       2 Zoltan library, Geometric
                      #       3 Zoltan library, Hypergraph
                      #       4 Zoltan library, Hilbert Space-Filling Curve
                      #       10 tree number
                      #       11 tree number as if the boundaries are excluded
                      #       -1 read domain decomposition from the restart file
                      #           (during restart or in postprocessing only)
domain_imbalance_tol = 0.1,0.85,0.95
```
10. Run your testcase.out file. 

```Console
mpirun -np NUMBER_OF_PROCESSES ./YOUR_CASE.out YOUR_INPUT_FILE.inp
```
If it is OK, value of 'nwlt_mx' must be almost equal 'nwlt_mn', otherwise domain decomposition does work. Below you see an example of information from running program. Notice that nwlt_mn $`\approx`$ nwlt_mx

```Console
 nwlt_g =        13578   cpu     =   -1.000000
 n_mvec =          128   n%      =   2.584749634502924E-002
 nwlt   =         2527   eps     =   1.000000000000000E-003
 nwlt_mx=         2980   nwlt_av =         2715
 nwlt_mn=         2527
```

# TroubleShooting
Some times the compiler can not find mpi.h include file. To fix this problem it needs to be added the include path of mpi.h file in machine.specific file. 
Notice that your MPI compiler. For instance the path can have next form: 

``` cmake
 -I/opt/intel/compilers_and_libraries_2018.2.199/linux/mpi/intel64/include/
```
to find out the path you can write next command in you terminal 

```Console
locate mpi.h
```
