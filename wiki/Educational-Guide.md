This page provides a list of resources to improve developer skills.

# Git

These sites can be used for mastering git as a distributed version-control system:
- [Atlassian tutorials](https://www.atlassian.com/git/tutorials) for reading-based learning.
- [LearnGitBranching](https://learngitbranching.js.org) for doing-based learning.

Particularly, the following links are recommended for reading:
- [Merging vs Rebasing](https://www.atlassian.com/git/tutorials/merging-vs-rebasing).
- [Always squash and rebase your git commits](https://blog.carbonfive.com/2017/08/28/always-squash-and-rebase-your-git-commits/).
- ["Merge with Merge Commit" and semi-linear history of commits via `git merge --no-ff`](https://nvie.com/posts/a-successful-git-branching-model/#incorporating-a-finished-feature-on-develop).
- ["Squash + Fast-Forward Merge"](https://stackoverflow.com/questions/5308816/how-to-use-git-merge-squash).
- [Recovering lost commits by `git reflog`](http://effectif.com/git/recovering-lost-git-commits).

The following command aliases are recommended for including in `~/.gitconfig` file:
```shell
[alias]
    co = checkout
    up = !git submodule update --init --recursive
    ci = commit
    st = status -suno
    br = branch
    hist = log --graph --abbrev-commit --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset' 
    aa = commit -a --amend --no-edit
```
