[Home](home)

## Sourcetree by [Atlassian](https://www.atlassian.com/software/sourcetree/overview)

Sourcetree is, in [my](https://gitlab.com/u/kaster) opinion, the best graphical frontend for git. It has all the functionality that one normally would type in the console as well as nice graphical representation of all git activity, such as tree base list of commits, fast branch checkout, real-time local/remote branches and much more. It is free afer some easy registration procedure. Below are some screenshots with the very few comments. I suggest to play around with it so you can actually feel its power.

1. Here you can see the main window, where the entire library of all included repositories are shown. You can also group repositories if you wish. All repos are independent form each other, so just double click and git your heart out.

![shot1](https://gitlab.com/AWESUMM/wlt_git/uploads/4f9a2e6d072ed5a3acebd5494de3b73d/Screenshot_2015-04-09_19.22.41.png)

2. This is the main repository window, with my short notes about every major part of it. If you master all elements on this window, you may consider yourself a git expert, or a gexpert for short.

![shot2](https://gitlab.com/AWESUMM/wlt_git/uploads/ffc83b5a0c9cdcbac88443122bd862c6/Screenshot_2015-04-09_19.27.20.png)

### Disclaimer

*I might add more operation specific screehshots if necessary, but I'd strongly recommend simply to play around, pressing different buttons and trust your intuition &mdash; interface, fortunately, allows that generously.*

[Git GUIs](../git-guis)