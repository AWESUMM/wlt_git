MODULE SGS_util
  USE precision
  IMPLICIT NONE

  INTEGER, PUBLIC :: n_var_MT    = 0              ! location of mu_til in u array

  INTEGER, PUBLIC :: n_var_SGS  = 0      ! number of addittional equations solved for SGS model
  INTEGER, PUBLIC :: n_var_K    = 0      ! location of Ksgs  in u array
  INTEGER, PUBLIC :: n_var_W    = 0      ! location of Omega  in u array (in K-Omega URANS)
  
  INTEGER, PUBLIC :: n_var_SGSD = 0      ! Local SGS      Dissipation                ! Spatial Adaptive Epsilon
  INTEGER, PUBLIC :: n_var_RD   = 0      ! Local Resolved Dissipation                ! Spatial Adaptive Epsilon
  LOGICAL, PUBLIC :: saveSGSD   = 0                                                 ! Spatial Adaptive Epsilon
  REAL (pr), PUBLIC :: EpsSpatial_Forcing_TimeScale                              ! Spatial Adaptive Epsilon
  
  LOGICAL   :: adaptk, deltaMij
  REAL (pr) :: Mcoeff                                      ! Mij = Mcoeff * |S>2eps| Sij>2eps - (|S|Sij )>2eps

  !========================  Model specific parameters ===================================
  !OLEG: ???? ask Xuan if we need them as global variables or can have public in SA model
  !Xuan: Variabes indices included here should be here because they are acutally used for both KOME and SA as well as the user case files
  !The others have been moved to SA model and made public
  INTEGER, PUBLIC :: n_var_cf    = 0                 ! location of Cf field in u array (acutally surface field - only non-zero at walls)
  INTEGER, PUBLIC :: n_var_yPlus = 0                 ! location of y_plus field in u array (acutally surface field - only non-zero at walls)
  INTEGER, PUBLIC :: n_var_cf_avg, n_var_yPlus_avg 

  LOGICAL, PUBLIC :: saveCfyPlus   ! Save Cf and yPlus fields

  INTEGER, PUBLIC :: n_var_MTB   = 0               ! location of mu_t (turb. eddy viscosity) in u array
  INTEGER, PUBLIC :: n_var_MTB_avg = 0             

  !=======================================================================================

  REAL (pr), DIMENSION (:) , ALLOCATABLE :: delta

  ! Variable Thresholding:  sgs_incompressible specific
  REAL (pr) :: nu                                ! kinematic viscosity (1/Re) - need this in this module for clipping 

  ! Model numbers
  INTEGER, PARAMETER:: sgsmodel_none  = 0  ! 0 - none 
!
! Incompressible SGS models
!
  INTEGER, PARAMETER:: sgsmodel_fixSM = 1  ! 1 - fixed parameter Smagorinsky (fixSM)
  INTEGER, PARAMETER:: sgsmodel_dynSM = 2  ! 2 - local dynamic Smagorinsky model (dynSM)
  INTEGER, PARAMETER:: sgsmodel_GDM   = 3  ! 3 - global dynamic Smagorinsky (GDM), 
  INTEGER, PARAMETER:: sgsmodel_LDM   = 4  ! 4 - Lagrangian dynamic Smagorinsky model (LDM)
  INTEGER, PARAMETER:: sgsmodel_DSM   = 5  ! 5 - dynamic structure model (DSM), 
  INTEGER, PARAMETER:: sgsmodel_LKM   = 6  ! 6 - localized kinetic energy based model (LKM)
  INTEGER, PARAMETER:: sgsmodel_LDKMB = 7  ! 7 - localized dynamic(based on Bardina) kinetic energy based model (LDKMB)
  INTEGER, PARAMETER:: sgsmodel_LDKMG = 8  ! 8 - localized dynamic(based on Germano) kinetic energy based model (LDKMG)
!
! Incompressible URANS models
!
  INTEGER, PARAMETER:: sgsmodel_KEPS  = 9  !  k/epsilon model (KEPS)   NOT READY             
  INTEGER, PARAMETER:: sgsmodel_KOME  = 10 !  k/omega model (KOME)
  INTEGER, PARAMETER:: sgsmodel_KOMET = 11 !  k/tau model (KOMET)      NOT READY
  INTEGER, PARAMETER:: sgsmodel_max   = 20 ! maximum index for incompressible SGS and URANS models
!
! Compressible URANS models (sgsmodel > sgsmodel_max)
!
  INTEGER, PARAMETER:: compressible_KEPS  = sgsmodel_max + 1   !  k/epsilon model (KEPS)   NOT READY             
  INTEGER, PARAMETER:: compressible_KOME  = sgsmodel_max + 2   !  k/omega model (KOME)
  INTEGER, PARAMETER:: compressible_KOMET = sgsmodel_max + 3   !  k/tau model (KOMET)      NOT READY
  INTEGER, PARAMETER:: compressible_SA    = sgsmodel_max + 4   !  Spalart-Allmaras model

  PUBLIC :: &
       sgs_delta_setup   ! Setup filter width for LES sgs terms

CONTAINS
  !Setup of delta

  !
  ! Intialize filter delta 
  ! This routine is called once in the first
  ! iteration of the main time integration loop.
  ! weights and model filters have been setup for first loop when this routine is called.
  !
  SUBROUTINE sgs_delta_setup( nlocal )
    USE debug_vars
    USE wlt_trns_vars      ! indx_DB 
    USE wlt_trns_mod       ! delta_from_mask
    USE curvilinear
    USE parallel
    USE variable_mapping 
    USE wavelet_filters_vars
    USE wavelet_filters_mod
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: nlocal
    
    INTEGER, SAVE        :: nlocal_last = 0
    INTEGER              :: i, k, ii, j, j_df, wlt_type, face_type, idim
    REAL (pr)            :: tmp1, tmp2


    IF( nlocal /= nlocal_last) THEN
       IF (verb_level.GT.0 .AND. par_rank.EQ.0) &
           PRINT *,' Reallocating sgs_mdl_force(1:nlocal) ', nlocal

       IF( ALLOCATED(delta) )          DEALLOCATE( delta )
                                       ALLOCATE(   delta(1:nlocal) )
   
    END IF
    
    !
    ! this part to find grid-filter local width delta(i) for LDM model
    !
    ! this is for uniform mesh, for streched meshes use DB_get_all_local_h
    IF( COUNT(u_g_mdl_gridfilt_mask==1.0_pr) == nwlt ) THEN
       DO j = 1, j_lev
          DO wlt_type = MIN(j-1,1),2**dim-1
             DO face_type = 0, 3**dim - 1
                DO j_df = j, j_lev
                   DO k = 1, indx_DB(j_df,wlt_type,face_type,j)%length  
                      i = indx_DB(j_df,wlt_type,face_type,j)%p(k)%i
                      delta(i) = PRODUCT(h(j_df,1:dim))**(1.0_pr/REAL(dim,pr))
                   END DO
                END DO
             END DO
          END DO
       END DO
    ELSE
       CALL delta_from_mask(delta,u_g_mdl_gridfilt_mask,nlocal)
    END IF
    
    IF( ANY(transform_dir(1:dim)>0) ) THEN
       delta = calc_jacobian_determinant( curvilinear_jacobian, dim, nwlt)**(-1.0_pr/REAL(dim,pr))*delta
    END IF
    
    IF (verb_level.GT.0) THEN
       tmp1 = MINVAL( delta(:) )
       tmp2 = MAXVAL( delta(:) )
       CALL parallel_global_sum( REALMINVAL=tmp1 )
       CALL parallel_global_sum( REALMAXVAL=tmp2 )
       IF (par_rank.EQ.0) PRINT *,'MINMAX delta  = ', tmp1, tmp2
    END IF
    
    nlocal_last = nlocal

  END SUBROUTINE sgs_delta_setup

END MODULE SGS_util
