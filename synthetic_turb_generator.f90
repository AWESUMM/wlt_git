!Reference: Shur et al. Flow Turbulence Combust (2014) 93:63–92

MODULE synthetic_turb_generator

  USE precision
  USE equations_compressible
  USE URANS_compressible 
  USE field
  USE error_handling

  LOGICAL, PROTECTED :: STG_flag
  REAL (pr), PRIVATE :: alpha_stg, beta_stg
  INTEGER, PRIVATE :: n_modes

  PUBLIC :: STG_read_input, STG_aux_vec_fluc

  PRIVATE :: &
    length_rans, &
    length_cut, &
    set_wave_numbers, &
    random_mode_vector, &
    random_mode_scalar

CONTAINS

  SUBROUTINE STG_read_input()
    IMPLICIT NONE

    STG_flag = .FALSE.
    call input_logical    ('STG_flag',STG_flag,'default',       '   STG_flag: T to turn on synthetic turbulence generator (STG)') 

    IF (STG_flag) THEN
    alpha_stg=0.025_pr
    call input_real    ('alpha_stg',alpha_stg,'default',       '   alpha_stg: parameter for wave number geometric series in STG module') 
    beta_stg=0.5_pr
    call input_real    ('beta_stg',beta_stg,'default',       '   beta_stg: parameter for wave number geometric series in STG module') 

    n_modes=401
    call input_integer    ('n_modes',n_modes,'default',       '   n_modes: total number of Fourier modes in STG module') 
    END IF
    
  END SUBROUTINE STG_read_input

  SUBROUTINE STG_aux_vec_fluc(nlocal, aux_vec_fluc, jlev)
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: nlocal, jlev
    REAL (pr), DIMENSION(nlocal,dim) :: aux_vec_fluc
    REAL (pr), DIMENSION(nlocal,dim) :: pos_vec 
    REAL (pr), DIMENSION(nlocal) :: l_e, l_eta, l_cut
    REAL (pr), DIMENSION(nlocal) :: f_eta, f_cut, eng_spectrum, amp_modes, amp_modes_sum, tmp_array
    REAL (pr) :: tmp, tmp1, tmp2, tmp3

    INTEGER :: ie, i, i_mod, shift
    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nlocal) :: iloc

    LOGICAL, SAVE  :: header_flag = .TRUE.
    REAL (pr), SAVE :: ke_min = 1.0e+05_pr
    REAL (pr), SAVE :: kcut_max = 0.0_pr
    REAL (pr), ALLOCATABLE, DIMENSION(:), SAVE :: kn

    REAL (pr), ALLOCATABLE, DIMENSION(:,:), SAVE :: d_vector, sigma_vector
    REAL (pr), ALLOCATABLE, DIMENSION(:), SAVE :: phi_scalar

    REAL (pr) :: floor

    floor = 1.0e-12

    CALL length_cut(nlocal, l_cut)
    CALL length_rans(nlocal, l_e, l_eta, jlev)

    !CALL print_extrema(l_cut(1:nlocal),nlocal, 'l_cut ')
    !CALL print_extrema(l_e(1:nlocal),nlocal, 'l_e ')
    !CALL print_extrema(l_eta(1:nlocal),nlocal, 'l_eta ')

    IF (header_flag) THEN

      !--Go through all Boundary points that are specified
      i_p_face(0) = 1
      DO i=1,dim
         i_p_face(i) = i_p_face(i-1)*3
      END DO
      DO face_type = 0, 3**dim - 1
         face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
         IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
            CALL get_all_indices_by_face (face_type, jlev, nloc, iloc)
            IF(nloc > 0 ) THEN 
               IF( face(1) == -1 .AND. ABS(face(2)) /= 1 ) THEN  ! inlet except corner points
                 tmp = 2.0_pr*pi/MAXVAL(l_e(iloc(1:nloc)))
                 IF (ke_min .GT. tmp) ke_min = tmp
                 tmp = 2.0_pr*pi/MINVAL(l_cut(iloc(1:nloc)))
                 IF (kcut_max .LT. tmp) kcut_max = tmp
               END IF
            END IF
         END IF
      END DO

      CALL parallel_global_sum (REALMINVAL=ke_min)
      IF (par_rank .EQ. 0) PRINT *, 'ke_min', ke_min 
      CALL set_wave_numbers(kn, ke_min) !ke_min and kn are common for all ranks and not changed with time

      !print estimated kcut_max
      CALL parallel_global_sum (REALMAXVAL=kcut_max)
      IF (par_rank .EQ. 0) PRINT *, 'kcut_max', kcut_max

      CALL random_mode_scalar( phi_scalar, 84567 )
      CALL random_mode_vector( d_vector, 618 )
      CALL random_mode_vector( sigma_vector, 1644 )

      DO i_mod=1,n_modes
        sigma_vector(i_mod,1:dim) = sigma_vector(i_mod,1:dim) - SUM(sigma_vector(i_mod,1:dim)*d_vector(i_mod,1:dim),DIM=1)*d_vector(i_mod,1:dim) !find sigma normal to d
        sigma_vector(i_mod,1:dim) = sigma_vector(i_mod,1:dim)/SQRT(SUM(sigma_vector(i_mod,1:dim)**2,DIM=1)) !normalized sigma
        phi_scalar(i_mod) = phi_scalar(i_mod)*2.0_pr*pi !rescale phi to [0,2pi)
      END DO

      header_flag = .FALSE.
    END IF !header_flag

    !CALL print_extrema(kn(1:n_modes), n_modes, 'kn(n_modes) ')
    !DO i=1,dim
    !  IF(par_rank .EQ. 0) PRINT *, 'dim = ', i
    !  CALL print_extrema(d_vector(1:n_modes,i), n_modes, 'd_vector(n_modes) ')
    !  CALL print_extrema(sigma_vector(1:n_modes,i), n_modes, 'sigma_vector(n_modes) ')
    !END DO
    !CALL print_extrema(phi_scalar(1:n_modes), n_modes, 'phi_scalar(n_modes) ')

    amp_modes_sum = 0.0_pr
    aux_vec_fluc = 0.0_pr
    DO i_mod = 1,n_modes
      !--Go through all Boundary points that are specified
      i_p_face(0) = 1
      DO i=1,dim
         i_p_face(i) = i_p_face(i-1)*3
      END DO
      DO face_type = 0, 3**dim - 1
         face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
         IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
            CALL get_all_indices_by_face (face_type, jlev, nloc, iloc)
            IF(nloc > 0 ) THEN 
               IF( face(1) == -1 .AND. ABS(face(2)) /= 1 ) THEN  ! inlet except corner points

                 DO i=1,dim
                   pos_vec(iloc(1:nloc),i) = u(iloc(1:nloc),n_map(i))
                 END DO
                 pos_vec(iloc(1:nloc),1) = ke_min/kn(i_mod)*(pos_vec(iloc(1:nloc),1) - 1.0_pr*t)
                 f_eta(iloc(1:nloc)) = EXP(-(12.0_pr*kn(i_mod)*l_eta(iloc(1:nloc))/2.0_pr/pi)**2)
                 WHERE( 0.9*2.0_pr*pi/l_cut(iloc(1:nloc)) < kn(i_mod) )
                   f_cut(iloc(1:nloc)) = EXP(-(4.0_pr*(kn(i_mod) - 0.9*2.0_pr*pi/l_cut(iloc(1:nloc)))*l_cut(iloc(1:nloc))/2.0_pr/pi)**3)
                 ELSEWHERE
                   f_cut(iloc(1:nloc)) = 1.0_pr
                 ENDWHERE
                 eng_spectrum(iloc(1:nloc)) = (kn(i_mod)*l_e(iloc(1:nloc))/2.0_pr/pi)**4 &
                                              /(1.0_pr+2.4_pr*(kn(i_mod)*l_e(iloc(1:nloc))/2.0_pr/pi)**2)**(17.0_pr/6.0_pr)*f_eta(iloc(1:nloc))*f_cut(iloc(1:nloc))
                 amp_modes(iloc(1:nloc)) = eng_spectrum(iloc(1:nloc))*kn(i_mod)*alpha_stg
                 amp_modes_sum(iloc(1:nloc)) = amp_modes_sum(iloc(1:nloc)) + amp_modes(iloc(1:nloc))

                 tmp_array = 0.0_pr
                 DO i=1,dim
                   tmp_array(iloc(1:nloc)) = tmp_array(iloc(1:nloc)) + d_vector(i_mod,i)*pos_vec(iloc(1:nloc),i)
                 END DO
                 DO i=1,dim
                   aux_vec_fluc(iloc(1:nloc),i) = aux_vec_fluc(iloc(1:nloc),i) + SQRT(amp_modes(iloc(1:nloc))) &
                                                  *(sigma_vector(i_mod,i)*COS(kn(i_mod)*tmp_array(iloc(1:nloc))+phi_scalar(i_mod)))
                 END DO
                 
               END IF
            END IF
         END IF
      END DO !face_type
    END DO !n_modes

    tmp1 = 0.0_pr
    tmp2 = 0.0_pr
    tmp3 = 0.0_pr
    !--Go through all Boundary points that are specified
    i_p_face(0) = 1
    DO i=1,dim
       i_p_face(i) = i_p_face(i-1)*3
    END DO
    DO face_type = 0, 3**dim - 1
       face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
       IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
          CALL get_all_indices_by_face (face_type, jlev, nloc, iloc)
          IF(nloc > 0 ) THEN 
             IF( face(1) == -1 .AND. ABS(face(2)) /= 1 ) THEN  ! inlet except corner points
               DO i=1,dim
                 aux_vec_fluc(iloc(1:nloc),i) = 2.0_pr*SQRT(1.5_pr)/SQRT(amp_modes_sum(iloc(1:nloc))+floor)*aux_vec_fluc(iloc(1:nloc),i)

                 !tmp1 = tmp1 + SUM( aux_vec_fluc(iloc(1:nloc),i))
               END DO

               !PRINT *, 'par_rank ', par_rank, 'SUM of amp_modes_sum: ', SUM(amp_modes_sum(iloc(1:nloc)))
               !tmp2 = tmp2 + SUM( amp_modes_sum(iloc(1:nloc)))

             END IF
          END IF
       END IF
    END DO

    !CALL parallel_global_sum( REAL = tmp1 )
    !IF(par_rank .EQ. 0) PRINT *, 'aux_vec_fluc inlet sum: ', tmp1

    !DO i=1,dim
    !  IF(par_rank .EQ. 0) PRINT *, 'dim = ', i 
    !  CALL print_extrema(aux_vec_fluc(1:nlocal,i),nlocal, 'aux_vec_fluc ')
    !END DO

    !CALL parallel_global_sum( REAL = tmp2 )
    !IF(par_rank .EQ. 0) PRINT *, 'amp_modes_sum inlet sum: ', tmp2

    !CALL print_extrema(amp_modes_sum(1:nlocal),nlocal, 'amp_modes_sum ')

  END SUBROUTINE STG_aux_vec_fluc

  SUBROUTINE length_rans(nlocal, l_e, l_eta, jlev)
    USE vector_util_mod
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: nlocal, jlev
    REAL (pr), DIMENSION(nlocal,dim) :: v
    REAL (pr), DIMENSION(dim, nlocal, dim) :: du, d2u
    REAL (pr), DIMENSION (nlocal,dim*(dim+1)/2) :: S0_ij
    REAL (pr), DIMENSION(nlocal) :: turb_k, turb_diss, temp, props, local_sutherland
    REAL (pr), DIMENSION(nlocal) :: l_e, l_eta ! l_e: engery-containing length scale; l_eta: Kolmogorov length scale
    INTEGER :: i
    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc

    INTEGER :: wlt_type, j, k, j_df, imin, i_bnd

    LOGICAL, SAVE :: header_flag = .TRUE.
    REAL (pr), ALLOCATABLE, DIMENSION(:,:), SAVE :: bnd_profileNUTB, bnd_profileuv
    INTEGER, SAVE :: n_profile = 0
    REAL (pr), SAVE :: R1=0.0_pr, R2=0.0_pr

    REAL (pr) :: tmp1, tmp2, floor

    floor = 1.0e-12

    IF (header_flag) THEN
      IF (sgsmodel == compressible_SA) THEN
        CALL bnd_coords_input(R1, R2, n_profile, bnd_profileNUTB,'profiles_RANS_nu_t')
      END IF
      CALL bnd_coords_input(R1, R2, n_profile, bnd_profileuv,'profiles_RANS_uv')
      header_flag = .FALSE.
    END IF
    
    IF ( viscmodel .EQ. viscmodel_sutherland ) THEN
      temp(:) = calc_pressure(u(:,1:n_integrated), nlocal, n_integrated ) / u(:,n_den) / R_in(Nspec) 
      local_sutherland(:) = calc_nondim_sutherland_visc( temp(:), nlocal, sutherland_const )
    END IF

    props(:) = mu_in(Nspec)  
    turb_k(:) = 0.0_pr
    turb_diss(:) = 0.0_pr
    l_e(:) = 0.0_pr
    l_eta(:) = 0.0_pr
    IF (sgsmodel .EQ. compressible_SA) THEN
       DO j = 1, jlev
          i_p_face(0) = 1
          DO i=1,dim
             i_p_face(i) = i_p_face(i-1)*3
          END DO
          DO face_type = 0, 3**dim - 1
             face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
             IF( face(1) == -1 .AND. ABS(face(2)) /= 1 ) THEN  !inflow except corner points
                DO wlt_type = MIN(j-1,1),2**dim-1
                   DO j_df = j, jlev
                      DO k = 1, indx_DB(j_df,wlt_type,face_type,j)%length
                         i_bnd = indx_DB(j_df,wlt_type,face_type,j)%p(k)%i

                               imin = MINLOC(ABS(distw_t(i_bnd) - bnd_profileNUTB(1:n_profile,1)), DIM = 1)
                               tmp1 = interpolate_bnd_coords(imin, n_profile, bnd_profileNUTB, distw_t(i_bnd)) !nu_tb: eddy viscosity
                               tmp2 = interpolate_bnd_coords(imin, n_profile, bnd_profileuv, distw_t(i_bnd)) !u'v' = - nu_tb*dydU: turbulent shear stress
                               !turb_k(i_bnd) = -tmp2/0.3_pr !-u'v'/0.3 = nu_tb*|S|/0.3 = nu_tb*dydU/0.3
                               !turb_diss(i_bnd) = tmp2**2/tmp1 ! nu_tb*|S|**2 = (nu_tb*|S|)**2/nu_tb
                               turb_k(i_bnd) = ABS(tmp2)/0.3_pr !-u'v'/0.3 = nu_tb*|S|/0.3 = nu_tb*dydU/0.3
                               turb_diss(i_bnd) = tmp2**2/MAX(floor, tmp1) ! nu_tb*|S|**2 = (nu_tb*|S|)**2/nu_tb

                               l_e(i_bnd) = MIN(2.0_pr*distw_t(i_bnd),3.0_pr*turb_k(i_bnd)**1.5_pr/(turb_diss(i_bnd)+floor))
                               IF ( viscmodel .EQ. viscmodel_sutherland ) THEN
                                  props(i_bnd) = props(i_bnd) * local_sutherland(i_bnd)
                               END IF
                               l_eta(i_bnd) = ((props(i_bnd)/u(i_bnd,n_den))**3/(turb_diss(i_bnd)+floor))**0.25_pr

                      END DO
                   END DO
                END DO
             END IF
          END DO
       END DO
    END IF


  END SUBROUTINE length_rans

  SUBROUTINE length_cut(nlocal, l_cut)
    USE wlt_trns_util_mod
    USE curvilinear
    IMPLICIT NONE

    INTEGER, INTENT(IN) :: nlocal
    REAL (pr), DIMENSION(nlocal) :: l_cut
    REAL (pr), DIMENSION(1:nlocal,1:dim) :: h_phys
    REAL (pr), DIMENSION(dim,nlocal) :: h_arr

    INTEGER:: i

    CALL get_all_local_h (h_arr)

    IF ( ANY( transform_dir(1:dim) .EQ. 1 ) ) THEN

      DO i = 1, dim
      h_phys (:, i) = h_arr(i, :) / SQRT( SUM(curvilinear_jacobian(i,1:dim,:)**2, DIM=1 ) )
      END DO

    ELSE 
      h_phys (:, 1:dim) = h_arr(1:dim, :) 
    END IF !transform_dir

    DO i = 1,nlocal
      l_cut(i) = 2.0_pr * MIN( MAX(h_phys(i,2), h_phys(i,dim), 0.3_pr*h_max(i)) + 0.1*distw_t(i), h_max(i) )
    END DO
  END SUBROUTINE length_cut

  SUBROUTINE set_wave_numbers(kn, ke_min)
    IMPLICIT NONE
    REAL (pr), INTENT (IN) :: ke_min
    REAL (pr), ALLOCATABLE, DIMENSION(:) :: kn
    
    INTEGER:: i
    REAL (pr) :: k_min

    k_min = beta_stg * ke_min
    IF(ALLOCATED(kn)) DEALLOCATE(kn)
    ALLOCATE(kn(n_modes))
    DO i = 1,n_modes 
      kn(i) = k_min * (1.0_pr + alpha_stg)**(i-1)
    END DO
     
  END SUBROUTINE set_wave_numbers

  SUBROUTINE random_mode_vector( mode_vector, seed_input )
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: seed_input
    REAL (pr), ALLOCATABLE, DIMENSION(:,:) :: mode_vector
    REAL (pr) :: theta, phi

    INTEGER :: iseedsize
    INTEGER, ALLOCATABLE, DIMENSION(:) :: seed

    INTEGER :: imodes, idim

    IF(ALLOCATED(mode_vector)) DEALLOCATE(mode_vector)
    ALLOCATE(mode_vector(n_modes,dim))

    CALL RANDOM_SEED(SIZE=iseedsize)  
    
    ALLOCATE(seed(iseedsize))
    seed(:) = seed_input 
    CALL RANDOM_SEED(PUT=seed)
    DEALLOCATE(seed)

       
    DO imodes = 1,n_modes
      ! generate random unit vector uniformly distributed over a sphere
      IF( par_rank .EQ. 0 ) THEN 
        CALL RANDOM_NUMBER(theta)
        CALL RANDOM_NUMBER(phi)
        theta = 2.0_pr * pi * theta
        phi = ACOS(1.0_pr - 2.0_pr * phi)
        mode_vector(imodes,1) = SIN(phi) * COS(theta)
        mode_vector(imodes,2) = SIN(phi) * SIN(theta)
        mode_vector(imodes,3) = COS(phi)
      ELSE
        mode_vector(imodes,1:dim)=0.0_pr
      END IF  
      CALL parallel_vector_sum( REAL=mode_vector(imodes,1:dim), LENGTH=dim)
    END DO

  END SUBROUTINE random_mode_vector

  SUBROUTINE random_mode_scalar( mode_scalar, seed_input )
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: seed_input
    REAL (pr), ALLOCATABLE, DIMENSION(:) :: mode_scalar

    INTEGER :: iseedsize
    INTEGER, ALLOCATABLE, DIMENSION(:) :: seed

    INTEGER :: imodes, idim

    IF(ALLOCATED(mode_scalar)) DEALLOCATE(mode_scalar)
    ALLOCATE(mode_scalar(n_modes))

    CALL RANDOM_SEED(SIZE=iseedsize)  
    
    ALLOCATE(seed(iseedsize))
    seed(:) = seed_input 
    CALL RANDOM_SEED(PUT=seed)
    DEALLOCATE(seed)

       
    DO imodes = 1,n_modes
      ! generate random scalar
      IF( par_rank .EQ. 0 ) THEN 
        CALL RANDOM_NUMBER(mode_scalar(imodes))
      ELSE
        mode_scalar(imodes)=0.0_pr
      END IF  
      CALL parallel_global_sum( REAL=mode_scalar(imodes))
    END DO

  END SUBROUTINE random_mode_scalar

END MODULE synthetic_turb_generator
