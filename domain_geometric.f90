!
! Geometric domain decomposition module
!
! INPUT PARAMETERS:
! dim - dimension of the problem
! P - number of parallel processors
! N(1:dim) - number of tree roots to be geometrically decomposed 
! (face, edge, and corner trees are not included)
! refine(1:dim) - if 1 the direction is refined, 0 - not
! OUTPUT PARAMETERS:
! NP(1:dim) - tensorial geometric decomposition between the procesors
!
MODULE domain_geometric

  IMPLICIT NONE
  PRIVATE
  PUBLIC :: geom_domain_decomp

CONTAINS

  SUBROUTINE geom_domain_decomp(P,N,refine,dim,NP, VERB)
    IMPLICIT NONE 
    INTEGER, INTENT(IN):: dim
    INTEGER, INTENT(IN):: P, N(1:dim)
    INTEGER, INTENT(INOUT):: refine(1:dim), NP(1:dim)
    LOGICAL, INTENT(IN), OPTIONAL :: VERB
    INTEGER :: r(1:dim)
    INTEGER :: Pused,i, idim, Nfac, ifac, max_options, option, imin
    INTEGER :: Ncell(1:dim), Ng(1:dim), NB(1:dim)

    INTEGER, DIMENSION(:), ALLOCATABLE :: factors, Rfactors, options, i_p, ioption
    INTEGER, DIMENSION(:,:), ALLOCATABLE :: NRfactors
    INTEGER, DIMENSION(:,:,:), ALLOCATABLE :: NRfactors_options
    REAL :: Vcell, Vcell_normalized, Vcell_min, cost
    LOGICAL :: do_verb

    do_verb = .FALSE.
    IF (PRESENT(VERB)) do_verb = VERB


    IF(MAXVAL(refine) == 0) THEN
       IF (do_verb) WRITE(*,'("refine is reset to refinement in all directions")')
       refine = 1
    END IF

    Pused = MIN(P,PRODUCT(N))

    IF(Pused < P) THEN
       IF (do_verb) WRITE(*,'(" WARNING: ",I4," processors used out of ",I4)') Pused,P
       i=0
       DO WHILE(P > PRODUCT(N)*2**i)
          i = i+1
       END	DO
       IF (do_verb) WRITE(*,'(" Increase the j_tree_root level by",I2)') i
    END IF

    Nfac = number_prime_factors(Pused)
    IF(ALLOCATED(factors)) DEALLOCATE(factors)
    IF(ALLOCATED(Rfactors)) DEALLOCATE(Rfactors)
    IF(ALLOCATED(options)) DEALLOCATE(options)
    ALLOCATE(factors(1:Nfac),Rfactors(1:Nfac),options(1:Nfac)); options = 0

    CALL prime_factors(Pused,Nfac,factors,Rfactors ) 

    IF(ALLOCATED(NRfactors)) DEALLOCATE(NRfactors)
    ALLOCATE(NRfactors(dim,1:Nfac)); NRfactors = 0

    IF (do_verb) WRITE(*,'(" factors(rank): ",6(1x,I4,"(",I2,")"))')  (factors(i), Rfactors(i), i=1,Nfac)

    ! finding all possible combinations of prime factors distribution between different dimensions
    IF(ALLOCATED(i_p)) DEALLOCATE(i_p)
    ALLOCATE(i_p(0:dim)); i_p = 1
    DO ifac = 1,Nfac
       i_p = 1
       i_p(1:dim)=1+Rfactors(ifac)*refine;
       DO i =1,dim
          i_p(i) = i_p(i-1)*i_p(i);
       END DO
       DO i = 1,i_p(dim)
          r = INT(MOD(i-1,i_p(1:dim))/i_p(0:dim-1))
          IF( SUM(r) == Rfactors(ifac) ) THEN
             options(ifac) = options(ifac)+1
          END IF
       END DO
    END DO
    ! setting up the array of different prime factor distributions
    max_options = MAXVAL(options);
    IF(ALLOCATED(NRfactors_options)) DEALLOCATE(NRfactors_options)
    ALLOCATE(NRfactors_options(dim,max_options,1:Nfac)); NRfactors_options = 0
    options = 0
    DO ifac = 1,Nfac
       i_p = 1
       i_p(1:dim)=1+Rfactors(ifac)*refine;
       DO i =1,dim
          i_p(i) = i_p(i-1)*i_p(i);
       END DO
       DO i = 1,i_p(dim)
          r = INT(MOD(i-1,i_p(1:dim))/i_p(0:dim-1))
          IF( SUM(r) == Rfactors(ifac) ) THEN
             options(ifac) = options(ifac)+1
             Nrfactors_options(:,options(ifac),ifac) = r
          END IF
       END DO
    END DO
!!$   ! displaying all possibilities of factorizaztion
!!$   DO ifac = 1,Nfac
!!$      DO option = 1,options(ifac)
!!$         WRITE(*,'(" factor",I4,", options",I4,": ",$)') factors(ifac),option
!!$         DO idim =1, dim
!!$            WRITE(*,'(I2,1x,$)') Nrfactors_options(idim,option,ifac)
!!$         END DO
!!$         WRITE(*,'(" = ",I3)') Rfactors(ifac)
!!$      END DO
!!$   END DO
    IF(ALLOCATED(i_p)) DEALLOCATE(i_p)
    ALLOCATE(i_p(0:Nfac)); i_p = 1
    i_p(1:Nfac) = options
    DO i =1,Nfac
       i_p(i) = i_p(i-1)*i_p(i);
    END DO
    IF(ALLOCATED(ioption)) DEALLOCATE(ioption)
    ALLOCATE(ioption(1:Nfac)); ioption = 0
    ! optimizing the domian decomposiiton between processors
    Vcell_min = 1.0E15
    DO i = 1,i_p(Nfac)
       ioption = INT(mod(i-1,i_p(1:Nfac))/i_p(0:Nfac-1))+1
!!$      WRITE(*,'(" i=",I4,$)') i
       NP=1
       DO ifac=1,Nfac
!!$         WRITE(*,'(6(I4,1x))') Nrfactors_options(:,ioption(ifac),ifac)
          NP=NP*factors(ifac)**NRfactors_options(:,ioption(ifac),ifac)
       END DO
       Ncell=CEILING(REAL(N)/REAL(NP))
       Ng=CEILING(REAL(N)/REAL(NP))*NP;
       ! cost factor makes exact directional factorization more favorable decomposition
       IF(MOD(PRODUCT(N),Pused) == 0 .AND. PRODUCT(Ng) == PRODUCT(N) ) THEN 
          cost = 1
       ELSE
          cost = 1000
       END IF
       Vcell = PRODUCT(REAL(Ncell)/REAL(MINVAL(Ncell)))*cost
       ! PRODUCT(Ncell/MINVAL(Ncell)) - normalized volume of each cell
       ! PRODUCT(Ng/N) - too big expansion factor is inneficient use of parallel processor CPU
       IF(Vcell < Vcell_min) then
          Vcell_min = Vcell
          imin = i
       END IF
    END DO
    DO i = imin,imin
       ioption = INT(mod(i-1,i_p(1:Nfac))/i_p(0:Nfac-1))+1
!!$      WRITE(*,'(" Optimum factorization per direction:"$)') 
       IF (do_verb) WRITE(*,'(" Optimum factorization per direction:")') 
       NP=1
       DO ifac =1,Nfac
          NP=NP*factors(ifac)**NRfactors_options(:,ioption(ifac),ifac)
       END DO
       IF (do_verb) THEN
          !  Displaying results
          DO idim =1,dim
!!$         WRITE(*,'(" X",I1,": ",$)') idim
             WRITE(*,'(" X",I1,": ")') idim
             DO ifac=1,Nfac
!!$            WRITE(*,'(1x,I4,1x,"(",I2,")",$))') factors(ifac), Nrfactors_options(idim,ioption(ifac),ifac)
                WRITE(*,'(1x,I4,1x,"(",I2,")")') factors(ifac), Nrfactors_options(idim,ioption(ifac),ifac)
             END DO
             WRITE(*,'(" ")')
          END DO
       END IF
    END DO
    Ncell=CEILING(REAL(N)/REAL(NP))
    Ng=Ncell*NP
    Vcell = PRODUCT(REAL(Ncell)/REAL(MINVAL(Ncell)))
    IF (do_verb) WRITE(*,'(" normalized volume: ",E12.5)') Vcell
    IF (do_verb) WRITE(*,'(" domain decomposition, processors per direction: ",4(I5,1x))') NP
    NB=0
    DO idim = 1,dim
       NB(idim) = MAX(0,N(idim)-Ncell(idim)*(NP(idim)-1))
    END DO
    IF (do_verb) WRITE(*,'(" Worst boundary element utilization: ",F6.2,"%")') REAL(PRODUCT(NB))/REAL(PRODUCT(Ncell))*100.00

  END SUBROUTINE geom_domain_decomp
  !
  ! function tests if N is prime number or not
  !
  FUNCTION prime_number(N) RESULT(out)
    IMPLICIT NONE 
    INTEGER, INTENT(IN):: N
    LOGICAL :: out
    INTEGER :: i

    out = .TRUE.
    i = 2
    DO WHILE(i < N .AND. out )
       out = (MOD(N,i) /= 0)
       i = i + 1
    END DO

  END FUNCTION prime_number
  !
  ! outputs prime number factorization
  !
  FUNCTION number_prime_factors(N) RESULT(Nfac)
    IMPLICIT NONE 
    INTEGER, INTENT(IN):: N
    INTEGER :: Nfac
    INTEGER :: i


    Nfac = 0
    DO i = 2,N
       IF(prime_number(i) .AND. mod(N,i) == 0 ) Nfac = Nfac + 1
    END DO

  END FUNCTION number_prime_factors

  SUBROUTINE prime_factors(N,Nfac_in,factors,Rfactors ) 
    IMPLICIT NONE 
    INTEGER, INTENT(IN):: N, Nfac_in
    INTEGER, DIMENSION(Nfac_in), INTENT(INOUT) :: factors, Rfactors
    INTEGER :: i, Nfac, M

    factors = 1
    Nfac = 0
    DO i = 2,N
       IF( prime_number(i) .AND. MOD(N,i) == 0 ) THEN
          Nfac = Nfac + 1
          factors(Nfac) = i
       END IF
    END DO
    IF(Nfac /= Nfac_in) THEN
       WRITE(*,'("ERROR in SUBROUTINE prime_factors: Nfac /= Nfac_in")')
       STOP
    END IF
    M=N
    Rfactors=0
    DO i = 1,Nfac
       DO WHILE( mod(M,factors(i)) == 0 ) 
          M=M/factors(i)
          Rfactors(i) = Rfactors(i) + 1
       END DO
    END DO

  END SUBROUTINE prime_factors


END MODULE domain_geometric
