!-------------------------------------------------------!
! define whether to use C or Fortran style node pointer !
#ifdef TREE_NODE_POINTER_STYLE_C
#undef TREE_NODE_POINTER_STYLE_C
#endif
#ifdef TREE_NODE_POINTER_STYLE_F
#undef TREE_NODE_POINTER_STYLE_F
#endif
#if ((TREE_VERSION == 0) || (TREE_VERSION == 1))
#define TREE_NODE_POINTER_STYLE_C
#elif ((TREE_VERSION == 2) || (TREE_VERSION == 3))
#define TREE_NODE_POINTER_STYLE_F
#else
#error Please define correct TREE_VERSION
#endif
! define whether to use C or Fortran style node pointer !
!-------------------------------------------------------!
#define DECLARE_NODE_POINTER INTEGER(pointer_pr)

#ifdef TREE_NODE_POINTER_STYLE_C
#define DECLARE_NODE_POINTER INTEGER(pointer_pr)
#elif defined TREE_NODE_POINTER_STYLE_F
#define DECLARE_NODE_POINTER TYPE(node), POINTER
#endif

!#define INTEGER8_DEBUG_AR


! -------------------------------------------------------------------------------
! For large problems NWLT and NXYZ^3 could exceed 2^32 integer. It is possible to
! define some related variables to be 64 bit, though I would rather set all the
! integers to 64 bit size for the sake of programming simplicity (AV).
! Particularly in this file, define SET_SOME_TO_64 if you prefer selective integer
! size increasing instead of an obvious solution via a compiler flag.
#define SET_SOME_TO_64
! -------------------------------------------------------------------------------





#ifdef SET_SOME_TO_64
#define INTEGER_V INTEGER*8
#else
#define INTEGER_V INTEGER
#endif


!
! wavelet filtering routines
!
MODULE wavelet_filters_mod

  USE precision
  USE debug_vars
  USE pde
  USE sizes
  USE wlt_vars
  USE wlt_trns_mod
  USE wlt_trns_util_mod
  USE wavelet_filters_vars
  IMPLICIT NONE

  !------------------------------
  ! simplified tree
  TYPE tree_node
     INTEGER :: iwlt             ! index in u(1:nwlt) array, or 0 if node not present
     INTEGER :: ptr              ! pointer to the node's children, or 0 if not present
  END TYPE tree_node

  TYPE group_of_nodes_of_one_level                     !size, integer*4
     TYPE(tree_node), DIMENSION(:), POINTER :: arr     !36     ! space for the nodes
     INTEGER :: arr_len                                !4      ! ALLOCATED LENGTH OF ARR(:)
     INTEGER :: prev_len                               !4      ! CUMULATIVE LENGTH OF PREVIOUSLY ALLOCATED SPACE
     TYPE(group_of_nodes_of_one_level), POINTER :: tptr!4      ! next chunk of the tree
  END TYPE group_of_nodes_of_one_level
  TYPE(group_of_nodes_of_one_level), PRIVATE, TARGET :: tree   ! the tree

  INTEGER, PRIVATE :: j_min, j_max,                    & ! j_mn, j_lev
       QUAD,                                           & ! 2**dim
       total_arr_len                                     ! TOTAL ALLOCATED LENGTH
  INTEGER, PRIVATE, ALLOCATABLE ::   beg_index(:),     & ! GLOBAL START INDEX OF THAT LEVEL
       lnwlt(:),                                       & ! counter of nwlt for that level
       fil_len(:),                                     & ! filled nodes (cumulative)
       fnz_len(:)                                        ! filled by nonzero nodes for that level

  TYPE group_of_nodes_of_one_level_ptr
     TYPE(group_of_nodes_of_one_level), POINTER :: tptr
  END TYPE group_of_nodes_of_one_level_ptr
  TYPE(group_of_nodes_of_one_level_ptr),PRIVATE,ALLOCATABLE :: info_lev(:)  ! WHICH TREE HAS THAT LEVEL NODES
  INTEGER, PRIVATE, ALLOCATABLE :: info_len(:)                              ! CUMULATIVE LENGTH OF THAT TREE

  LOGICAL, PRIVATE, PARAMETER :: OPTIMIZE_NO = .FALSE., OPTIMIZE_YES = .TRUE.     ! flags
  
  ! --- define for additional interpolation related output ---
!#define INTERP_DEBUG
  ! --- define for additional interpolation related output ---
  
!#define INTERP_DEBUG
  
#ifdef INTERP_DEBUG
!!$  LOGICAL, PRIVATE, PARAMETER :: V1 = .TRUE., V2 = .FALSE., V3 = .FALSE.
!!$  LOGICAL, PARAMETER :: V0 = .TRUE., V1 = .TRUE., V2 = .TRUE., V3 = .TRUE.
  LOGICAL, PRIVATE, PARAMETER :: V0 = .TRUE., V1 = .TRUE., V2 = .FALSE., V3 = .FALSE.
#endif  
  
  PRIVATE :: tree_set_iwlt,    & ! set tree()%arr()%iwlt
       tree_get_iwlt,          & ! get tree()%arr()%iwlt
       tree_set_ptr,           & ! set tree()%arr()%ptr
       tree_get_ptr,           & ! get tree()%arr()%ptr
       tree_allocate,          & ! initial tree allocation
       tree_allocate_level,    & ! allocate level of the tree ( .GT.j_mn )
       tree_allocate_extension,& ! allocate tree extension
       tree_deallocate,        & ! delete tree completely
       tree_stat,              & ! output tree statistics
       tree_locate_node,       & ! get exact node location inside the tree
       tree_round_to_allocate    ! round to a nice allocatable number
  ! simplified tree
  !------------------------------

CONTAINS
  !-------------------------------------------------------------------------------------------
  ! PRIVATE tree related functions:
  !----------------------------------------------
  FUNCTION tree_round_to_allocate( num )
    ! round num to some reasonable number for arr(:) allocation
    INTEGER, INTENT(IN) :: num
    INTEGER :: tree_round_to_allocate
    INTEGER, PARAMETER :: BLK = 32
    
    tree_round_to_allocate = (num/BLK)*BLK + BLK
    
  END FUNCTION tree_round_to_allocate
  !----------------------------------------------
  SUBROUTINE tree_locate_node( tree_, index_in, cl, index_out )
    ! get exact node location inside the tree as tree_%arr(index)
    ! returns: tree_ and index_out
    TYPE(group_of_nodes_of_one_level), POINTER :: tree_
    INTEGER, INTENT(IN) :: index_in      ! index from the main tree beginning
    INTEGER, INTENT(IN) :: cl            ! level of the node
    INTEGER, INTENT(OUT) :: index_out    ! output real index in %arr(:)
    INTEGER :: requested
    REAL :: bulge_fraction
    
    
    ! at that tree that level starts
    tree_ => info_lev(cl)%tptr
    
    
    ! transfer index_in (from the main tree beginning)
    !    into index_out (from cl level's tree beginning)
    index_out = index_in - tree_%prev_len
    
    
    DO WHILE( index_out.GT.tree_%arr_len )         ! if index exceed arr(:) size
       index_out = index_out - tree_%arr_len
       
       IF (.NOT.ASSOCIATED(tree_%tptr)) THEN
          ! need to allocate extension
          ! so far, out of lnwlt nodes fnz_len occupied fil_len space
          ! thus we estimate the space required for the rest of the nodes:
          bulge_fraction = ( fil_len(cl)-fil_len(cl-1) )/REAL( fnz_len(cl) )
          requested = MAX(1.0_pr, AINT( (1+lnwlt(cl)-fnz_len(cl))*bulge_fraction) )
          
#ifdef INTERP_DEBUG
          IF (V0) THEN
             PRINT *, '    tree_locate_node() for level ', cl
             PRINT *, '    fil_len = ', fil_len
             PRINT *, '    fnz_len = ', fnz_len
             PRINT *, '    lnwlt   = ', lnwlt
             PRINT *, '    bulge_fraction =', bulge_fraction
             PRINT *, '    requested = ', requested
          END IF
#endif

          IF (requested.LT.tree_%arr_len/4) &              ! bias requested to 25% of previous tree
               requested = (requested+tree_%arr_len/4)/2   ! ...
          CALL tree_allocate_extension( tree_, &
               tree_round_to_allocate(requested) )
          tree_ => tree_%tptr                           ! step to the next tree arr(:)
       ELSE
          tree_ => tree_%tptr                           ! step to the next tree arr(:)
       END IF
    END DO
    
  END SUBROUTINE tree_locate_node
  !----------------------------------------------
  FUNCTION tree_get_iwlt( level, index )
    ! get tree(level)%arr(index)%iwlt
    ! (index starts from the main tree beginning)
    INTEGER, INTENT(IN) :: level, index
    INTEGER :: ind
    INTEGER :: tree_get_iwlt
    TYPE(group_of_nodes_of_one_level), POINTER :: tree_
    
    CALL tree_locate_node( tree_, index, level, ind )
    tree_get_iwlt =  tree_%arr( ind )%iwlt
    
  END FUNCTION tree_get_iwlt
  !----------------------------------------------
  FUNCTION tree_get_ptr( level, index )
    ! get tree(level)%arr(index)%ptr
    ! (index starts from the main tree beginning)
    INTEGER, INTENT(IN) :: level, index
    INTEGER :: ind
    INTEGER :: tree_get_ptr
    TYPE(group_of_nodes_of_one_level), POINTER :: tree_
    
    CALL tree_locate_node( tree_, index, level, ind )
    tree_get_ptr = tree_%arr( ind )%ptr

  END FUNCTION tree_get_ptr
  !----------------------------------------------
  SUBROUTINE tree_stat( j1, j2 )
    ! tree statistics
    INTEGER, INTENT(IN) :: j1, j2 ! j_mn, j_lev
    INTEGER :: j, tree_size, count_nonzero(j1:j2,1:2), &
         i, local_index, fil_len_loc(j1-1:j2)
    TYPE(group_of_nodes_of_one_level), POINTER :: tree_
    
    count_nonzero(j1:j2,1:2) = 0
    fil_len_loc(j1-1) = 0
    fil_len_loc(j1:j2) = fil_len(j1:j2)
    WRITE (*,'(" ")')
    WRITE (*,'(" TREE STATISTICS: ",I4,":",I4,A)') j1, j2, '--------------------------->'
    WRITE (*,'(" level: allocated:    filled:   nonzero:      nwlt:")')   ! statistics by level
    DO j=j1,j2
       IF ( lnwlt(j).GT.0 ) &
            WRITE (*,'(I8,1X,4(3X,I8))') j, &
            info_lev(j)%tptr%arr_len, fil_len_loc(j)-fil_len_loc(j-1), fnz_len(j), lnwlt(j)
       ! count nonzero nodes of that level directly
#ifdef INTERP_DEBUG
       IF (V3) WRITE (*, ADVANCE='NO', FMT='(A)') '(iwlt, ptr)='
#endif
       tree_ => info_lev(j)%tptr
!!$       DO i = 1, beg_index(j+1)-beg_index(j)                ! i - from level beginning
!!$          local_index = i-1+beg_index(j)-tree_%prev_len     ! local_index - from tree beginning
!!$          CALL tree_locate_node( tree_, local_index, j )
       DO i = beg_index(j), beg_index(j+1)-1                   ! i - index from tree beginning
          CALL tree_locate_node( tree_, i, j, local_index )    ! local_index - actual in %arr(:)

          IF (tree_%arr(local_index)%iwlt.GT.0) count_nonzero(j,1) = count_nonzero(j,1) + 1
          IF (tree_%arr(local_index)%ptr.GT.0) count_nonzero(j,2) = count_nonzero(j,2) + 1
#ifdef INTERP_DEBUG
          IF (V3) &
               WRITE (*, ADVANCE='NO', FMT='("(",I8,1X,I8,")")' ) &
               tree_%arr(local_index)%iwlt, tree_%arr(local_index)%ptr
#endif
       END DO
       WRITE (*, ADVANCE='YES', FMT='(" ")')
    END DO
    WRITE (*,'(" total ",4(3X,I8))') &
         total_arr_len, MAXVAL(fil_len(j1:j2)), SUM(fnz_len(j1:j2)), SUM(lnwlt(j1:j2))
    tree_ => tree  ! current tree
    tree_size = 0  ! total tree size
    j = 1          ! current tree number
    WRITE (*,'(" tree:    arr_len:  prev_len:")')                                    ! statistics by tree
    DO WHILE( ASSOCIATED(tree_) )
       WRITE (*,'(I8,4X,I8,3X,I8)') j, tree_%arr_len, tree_%prev_len
       tree_size = tree_size + tree_%arr_len
       j = j + 1
       tree_ => tree_%tptr
    END DO
    WRITE (*,'(" total:   ",I8,", total_arr_len=",I8)') tree_size, total_arr_len
    WRITE (*,'(" tot/nwlt = ", I8," %,  tot/filled = ",I8," %")') &
         (total_arr_len*100)/Nwlt_lev(j2,1), (total_arr_len*100)/fil_len(j2)
    WRITE (*,'(" beg_index=",'//CHAR(ICHAR('0')+j2-j1+2)//'(I8,X))') beg_index(j1:j2+1)
    WRITE (*,'(" nonzero iwlt =",'//CHAR(ICHAR('0')+j2-j1+2)//'(I8,X))') count_nonzero(j1:j2,1)
    WRITE (*,'(" nonzero ptr  =",'//CHAR(ICHAR('0')+j2-j1+2)//'(I8,X))') count_nonzero(j1:j2,2)
    WRITE (*,'(" ")')

  END SUBROUTINE tree_stat
  !----------------------------------------------
  SUBROUTINE tree_set_iwlt ( level, index, iwlt )
    ! set tree(level)%arr(index)%iwlt
    ! (index starts from the main tree beginning)
    INTEGER, INTENT(IN) :: level, index
    INTEGER, INTENT(IN) :: iwlt
    TYPE(group_of_nodes_of_one_level), POINTER :: tree_
    INTEGER :: ind
    
    CALL tree_locate_node( tree_, index, level, ind )
    
    IF (tree_%arr(ind)%iwlt.NE.0) THEN               ! DEBUG
       PRINT *, ' '
       PRINT *, 'ERROR: iwlt has been set already!'
       WRITE (*, '("level=",I8,", index,ind=",I8,1X,I8,", new_iwlt,old_iwlt=",I8,1X,I8)') &
            level,index,ind,iwlt,tree_%arr(ind)%iwlt
#ifdef INTERP_DEBUG
       IF (V0) CALL tree_stat( j_mn, j_lev )
#endif
       STOP 1
    END IF
    
    tree_%arr( ind )%iwlt = iwlt
    
  END SUBROUTINE tree_set_iwlt
  !----------------------------------------------
  SUBROUTINE tree_set_ptr ( level, index, ptr )
    ! set tree(level)%arr(index)%ptr
    ! (index starts from the main tree beginning)
    INTEGER, INTENT(IN) :: level, index
    INTEGER, INTENT(IN) :: ptr
    TYPE(group_of_nodes_of_one_level), POINTER :: tree_
    INTEGER :: ind
    
    CALL tree_locate_node( tree_, index, level, ind )
    
    IF (tree_%arr(ind)%ptr.NE.0) THEN                ! DEBUG
       PRINT *, ' '
       PRINT *, 'ERROR: ptr has been set already!'
       WRITE (*, '("level=",I3,", index,ind=",I8,1X,I8,", new_ptr, old_ptr =",I8,1X,I8)') &
            level,index,ind,ptr,tree_%arr(ind)%ptr
#ifdef INTERP_DEBUG
       IF (V0) CALL tree_stat( j_mn, j_lev )
#endif
       STOP 1
    END IF
    
    tree_%arr( ind )%ptr = ptr
    
  END SUBROUTINE tree_set_ptr
  !----------------------------------------------
  SUBROUTINE tree_allocate_extension( tree_, nwlt_2 )
    ! allocate tree_ extension for extra num nodes
    ! and return the pointer to the new tree
    TYPE(group_of_nodes_of_one_level), POINTER :: tree_
    INTEGER, INTENT(IN) :: nwlt_2
    TYPE(tree_node) :: zero_node
    
    zero_node%iwlt = 0
    zero_node%ptr = 0

#ifdef INTERP_DEBUG
    IF (V0) PRINT *, '++ tree_allocate_extension of ', nwlt_2
#endif
    
    ALLOCATE( tree_%tptr )
    ALLOCATE( tree_%tptr%arr(nwlt_2) )
    tree_%tptr%arr(:) = zero_node                        ! clean the allocated array
    tree_%tptr%arr_len = nwlt_2                          ! and set internal
    tree_%tptr%prev_len = tree_%arr_len + tree_%prev_len ! tree parameters
    NULLIFY( tree_%tptr%tptr )                           ! ...
    total_arr_len = total_arr_len + nwlt_2               ! set global allocation length

#ifdef INTERP_DEBUG
    IF (V0) PRINT *, '++ allocated extension of ', tree_%tptr%arr_len, ', total_arr_len =', total_arr_len
#endif

  END SUBROUTINE tree_allocate_extension
  !----------------------------------------------
  SUBROUTINE tree_allocate_level( j, num, opt_flag )
    ! allocate level of the tree
    INTEGER, INTENT(IN) :: j, &         ! level to allocate
         num                            ! number of nodes in indx_DB at that level
    LOGICAL, INTENT(IN) :: opt_flag     ! .TRUE. for j .NE. j_mn
    TYPE(group_of_nodes_of_one_level), POINTER :: tree_
    INTEGER :: old_total_arr_len, &
         requested, tmp_int
    REAL :: bulge_fraction              ! how much % of space the nodes actually take
    

    IF ( opt_flag ) THEN
       ! for any level .NE. j_mn
       
#ifdef INTERP_DEBUG
       IF (V0) THEN
          PRINT *, ' tree_allocate_level(): allocating level',j,', num=',num
          PRINT *, ' total_arr_len=',total_arr_len
          PRINT *, ' beg_index=',beg_index
          PRINT *, ' Nwlt_lev(:,1)=', Nwlt_lev(:,1)
       END IF
#endif
       
       ! already allocated Nwlt_lev(j-1,1) for levels 1:j-1
       ! and these nodes are occupying beg_index(j)-1 space,
       ! empty space is total_arr_len-beg_index(j)+1,
       ! so the space required for new num nodes to be:
       ! (beg_index(j)-1)/Nwlt_lev(j-1,1) * num
       
       IF ( ASSOCIATED(info_lev(j)%tptr) ) THEN
#ifdef INTERP_DEBUG
          IF (V0) PRINT *, ' info_lev(j)%tptr already associated, exiting tree_allocate_level()'
#endif
          RETURN        ! do nothing if already allocated
       END IF
       
       info_lev(j)%tptr => info_lev(j-1)%tptr
       ! compute bulge_fraction
       ! ( N*bulge_fraction of memory has been already allocated for N nodes )
       bulge_fraction = (beg_index(j)-1)/REAL( Nwlt_lev(j-1,1) )
       !bulge_fraction = num/REAL( Nwlt_lev(j-1,1) )
       
       IF ( AINT(num*bulge_fraction) .LE. total_arr_len-beg_index(j) ) THEN
#ifdef INTERP_DEBUG
          IF (V0) THEN
             PRINT *, ' bulge_fraction=', bulge_fraction
             PRINT *, ' it is enough space, exiting tree_allocate_level()'
          END IF
#endif
          RETURN !  or if it is enough space
       END IF

       ! find in which tree level j starts
       tree_ => info_lev(j-1)%tptr                      ! tree of the previous level
       DO WHILE( ASSOCIATED(tree_%tptr) )               ! find the last tree which has
          tree_ => tree_%tptr                           !  no extensions to it
       END DO                                           ! tree_ is such a tree
       ! allocate extension
       old_total_arr_len = total_arr_len

       requested = MAX( 1.0_pr, AINT(num*bulge_fraction) + 1 +beg_index(j)-total_arr_len )

#ifdef INTERP_DEBUG
       IF (V0) THEN
          WRITE (*,'(" original requested:",I8)') requested   ! .GT. 0
          PRINT *, ' beg_index =', beg_index
          PRINT *, ' Nwlt_lev(:,1)=',Nwlt_lev(:,1)
          PRINT *, ' j=',j
          PRINT *, ' bulge_fraction=', bulge_fraction
          PRINT *, ' total_arr_len = ', total_arr_len
       END IF
#endif
       IF (requested.LT.0) STOP 'interpolation error 350'
       
       
       IF (requested.LT.tree_%arr_len/4) THEN                 ! bias requested to 25% of previous tree
          requested = (requested+tree_%arr_len/4)/2           ! ...
#ifdef INTERP_DEBUG
          IF (V0) WRITE (*,'(" biased requested  :",I8)') requested   ! .GT. 0
#endif
       END IF
       
       
       CALL tree_allocate_extension( tree_, tree_round_to_allocate(requested) )
       
       ! set info_lev(j)
!!$       IF ( beg_index(j).LE.old_total_arr_len ) THEN
!!$          info_lev(j)%tptr => tree_       ! level j starts at old tree
!!$       ELSE
!!$          info_lev(j)%tptr => tree_%tptr  ! ... at new tree
!!$       END IF
       CALL tree_locate_node( info_lev(j)%tptr, beg_index(j), j-1, tmp_int )
       
            
    ELSE
       ! for level j_mn only, num is the regular grid number of nodes
       fil_len(j) = num
       fnz_len(j) = num
       beg_index(j) = 1
       beg_index(j+1) = num+1
       info_lev(j)%tptr => tree      ! level j_mn stored in the main tree
       info_len(j) = tree%arr_len    ! and its length has been already set in tree_allocate
    END IF
  END SUBROUTINE tree_allocate_level
  !----------------------------------------------
  SUBROUTINE tree_add_nonperiodic_j_mn( j_in, i_pjm )
    ! add nonperiodic nodes at j_in level to the tree
    ! by copying the correspondent periodic entries into
    ! the mesh at j_mn which is always nonperiodic
    INTEGER, INTENT(IN) :: i_pjm(0:dim), &   ! i_pjm <--> j_mn, computed in interpolate()
         j_in                                ! nodes of that level to be added
    INTEGER :: ii, ixyz(dim), bnd(dim), i
    
    IF (j_in.EQ.j_mn) THEN
       ! --- regular grid at j_mn -----------
       
       ! set upper boundary coordinate at j_mn
       bnd = mxyz*2**(j_mn-1)
       
       ! look for zero iwlt nodes in the tree%
       DO ii=i_pjm(1),i_pjm(dim)
          IF (tree%arr(ii)%iwlt.EQ.0) THEN
             ixyz(1:dim) = INT(MOD(ii-1,i_pjm(1:dim))/i_pjm(0:dim-1)) ! 3D coord on j_mn
#ifdef INTERP_DEBUG
             IF (V2) PRINT *, 'expanding ii=',ii, ixyz
#endif
             ixyz(1:dim) = (1-prd(1:dim))*ixyz(:) + prd(:)*MOD(ixyz(:)+bnd(:),bnd(:))
             i = 1+SUM(ixyz(1:dim)*i_pjm(0:dim-1))                    ! 1D coord on j_mn
#ifdef INTERP_DEBUG
             IF (V2) PRINT *, '     into i =',i, ixyz
#endif
             tree%arr(ii)%iwlt = tree%arr(i)%iwlt
             tree%arr(ii)%ptr = tree%arr(i)%ptr
          END IF
       END DO
       
       
#ifdef INTERP_DEBUG
       ! check that no zero entries left
       DO ii=1,i_pjm(dim)
          IF (tree%arr(ii)%iwlt.EQ.0) THEN
             PRINT *, 'tree_add_nonperiodic_j_mn(): zero entry left', &
                  ii, INT(MOD(ii-1,i_pjm(1:dim))/i_pjm(0:dim-1))
             STOP 'error'
          END IF
       END DO
#endif
    ELSE
       ! --- j_in > j_mn ---
       PRINT *, 'tree_add_nonperiodic_j_mn(): j.NE.j_mn mode is not ready'
       STOP
    END IF

  END SUBROUTINE tree_add_nonperiodic_j_mn
  !----------------------------------------------
  SUBROUTINE tree_set_ptr_iwlt( level, index, ptr, iwlt )
    ! set tree(level)%arr(index)%ptr and tree(level)%arr(index)%iwlt
    ! for j_mn level only (without boundary testing)
    INTEGER, INTENT(IN) :: level, index
    INTEGER, INTENT(IN) :: ptr, iwlt
    
    tree%arr(index)%ptr = ptr
    tree%arr(index)%iwlt = iwlt

  END SUBROUTINE tree_set_ptr_iwlt
  !----------------------------------------------
  SUBROUTINE tree_allocate( j1, j2, nwlt )
    ! initial tree allocation
    INTEGER, INTENT(IN) :: j1, j2, &   ! j_mn, j_lev
         nwlt                          ! total number of wavelets at level j2 and below
    INTEGER :: nwlt_2, &   ! allocated space
         i                 ! counter
    TYPE(tree_node) :: zero_node
    
    zero_node%iwlt = 0                      ! initialize zero tree node
    zero_node%ptr = 0                       ! ...
    
    ! compute appropriate space to allocate
    nwlt_2 = tree_round_to_allocate( nwlt )
    
    NULLIFY( tree%arr )                     ! reset node array
    NULLIFY( tree%tptr )                    ! reset tree extension pointer
    tree%arr_len = nwlt_2                   ! set allocated length of arr(:)
    tree%prev_len = 0                       ! set previously allocated space
    ALLOCATE( lnwlt(j1:j2),  &
         fil_len(j1:j2),     &
         fnz_len(j1:j2),     &
         beg_index(j1:j2+1), &
         tree%arr(nwlt_2) )
    
    ! clean the allocated array
    tree%arr(:) = zero_node
    
    ! set counters
    lnwlt(j1:j2) = 0          ! nwlt for that level
    fil_len(j1:j2) = 0        ! filled by all the nodes
    fnz_len(j1:j2) = 0        ! filled by nonzero nodes only
    beg_index(j1:j2+1) = 0

    ! set globals
    j_min = j1
    j_max = j2
    total_arr_len = tree%arr_len  ! total allocated space
    
    ! set tree info
    ALLOCATE( info_lev(j1:j2), info_len(j1:j2) )
    DO i=j1,j2
       NULLIFY( info_lev(i)%tptr )
       info_len(i) = 0
    END DO

#ifdef INTERP_DEBUG
    IF (V0) PRINT *, 'allocated main tree of length', tree%arr_len
#endif

  END SUBROUTINE tree_allocate
  !----------------------------------------------
  SUBROUTINE tree_deallocate
    ! deallocate tree completely
    TYPE(group_of_nodes_of_one_level), POINTER :: nexttree, nexttree_
    
    nexttree => tree%tptr                    ! set next pointer
    DEALLOCATE( lnwlt, beg_index, &          ! deallocate main tree
         fil_len, fnz_len, tree%arr )        ! ...
    
#ifdef INTERP_DEBUG
    IF (V0) PRINT *, 'deallocated: main tree of length',tree%arr_len
#endif
    
    tree%arr_len = 0
    
    
    DO WHILE( ASSOCIATED(nexttree) )         ! deallocate tree extensions
       DEALLOCATE( nexttree%arr )
       
#ifdef INTERP_DEBUG
       IF (V0) PRINT *, 'deallocated: tree extension of length',nexttree%arr_len
#endif
       
       nexttree_ => nexttree%tptr
       DEALLOCATE( nexttree )
       nexttree => nexttree_
    END DO
    
    
    total_arr_len = 0                        ! reset globals
    j_min = 0
    j_max = 0
    DEALLOCATE( info_lev, info_len )
    
  END SUBROUTINE tree_deallocate
  !----------------------------------------------
  
  !-------------------------------------------------------------------------------------------
  SUBROUTINE interpolate ( u, du, nwlt, n_var, &
       x_size, x, interpolation_order, var_size, var, res, VERB, &
       SYNC_U)
    ! Database independent interpolation into array of points x(1:dim,1:x_size),
    ! where dim is the dimension and x_size is the number of points.
    ! Order of interpolation is to be provided (0-nearliest, 1-linear, etc).
    ! Result is to be returned as res(1:nvar,1:x_size),
    ! where var_size is the number of variables to interpolate for, whose global
    ! numbers (1..n_var) are to be provided in the array variables(1:var_size).
    !
    ! NOTE: In parallel compilation (with MP defined) the database is db_tree!
    !
    !       It is user responsibility to set real point coordinates x(:,:)
    !       within current processor's domain. Check xyzlimits() and
    !       DB_get_proc_by_coordinates() to ensure the correct placement.
    !
    ! Interpolation orders:
    !   0 - nearest leftmost box corner value
    !   1 - linear in each direction
    !   3 - 3rd order in each direction
    !
    USE wlt_trns_vars    ! indx_DB
    USE parallel
#ifdef MULTIPROC
    USE db_tree_vars     ! c_pointer
    USE db_tree          ! is_ok()
#endif
    INTEGER, INTENT (IN) :: nwlt, &                       !   nwlt, size of U and dU
         n_var, &                                           !   n_var, size of U and dU
         x_size                                             ! number of point to interpolate into
    REAL(pr), INTENT(IN) :: u(1:nwlt, 1:n_var)              ! function, U
    REAL(pr), INTENT(IN) :: du(1:n_var,1:nwlt,1:dim)        ! derivatives, dU
    REAL(pr), INTENT (IN) :: x(1:dim,1:x_size)              ! points to interpolate into (real coordinates)
    INTEGER, INTENT (IN) :: interpolation_order             ! interpolation order: 0, 1, 3
    INTEGER, INTENT (IN) :: var_size                        ! number of variables to interpolate for
    INTEGER, INTENT (IN) :: var(1:var_size)                 ! variables to interpolate for
    REAL(pr), INTENT (OUT) :: res(1:var_size,1:x_size)      ! the result of interpolation
    LOGICAL, INTENT(IN), OPTIONAL :: VERB,                & ! print tree statistics
         SYNC_U                                             ! synchronize U from neighbouring processors
    
    LOGICAL :: do_verb,                                   & ! print tree statistics
         do_sync_u
    LOGICAL, ALLOCATABLE :: var_mask(:)
    INTEGER :: box(2**dim), box_coord(2**dim), box_j(2**dim), &       ! bounding box
         box_tmp(2**dim), box_coord_tmp(2**dim), box_j_tmp(2**dim)
    INTEGER :: point_number, &
         ONE(1:dim), &          ! constant
         i_index_jl(dim), &     ! index of low corner of the bounding box
         i_index(dim)           !
    INTEGER :: n_var_of_db, &   ! DB parameter VAL_NUM
         nwlt_extra, &          ! number of iwlt nodes from other processors
         i, j, k, j_df, iii, &  ! counters
         iwlt_extra, &          ! iwlt index for a processor boundary zone nodes
         var_size_du, &         ! variables to store du
         wlt_type, proc, &      ! counters
         errnum, chi, &
         face_type(0:3**dim-1), &   ! array of face types
         ftc                        ! counter in face types(:)    
    
    INTEGER_V :: i_p(0:dim), i_pj(0:dim), i_pjj(0:dim)
    INTEGER :: i_pjm(0:dim)
    INTEGER ::   ii, ixyz(dim), ixyz_jl(dim), &
         box_marker, j_search, node_wlt_type, &
         powscale(dim),                    & ! 1,2,4 <--> z,y,x
         ivar, idim
    REAL(pr) :: rel_x(dim), rel_dx(dim), &
         ubox(var_size,2**dim), &            ! box of u values
         dubox(var_size,2**dim,dim)          ! box of du values
    LOGICAL :: box_complete, &               ! true if all box nodes exist
         error                               ! if error has happened
    REAL(pr), ALLOCATABLE :: u_extra(:,:)           ! function, U, of the boundary zone
    REAL(pr), ALLOCATABLE :: du_extra(:,:,:)        ! derivatives, dU, of the boundary zone
    REAL(pr) :: tmp_u_du (var_size*(1+dim))  ! tmp buffer for function values

#ifdef MULTIPROC
    DECLARE_NODE_POINTER :: c_pointer, c_pointer1
#endif
    
    !===================================================
    ! test input
    !===================================================
    IF (x_size.LE.0) RETURN ! there are no points to interpolate into
    
    errnum = 0
    IF ((interpolation_order.GT.3).OR.(interpolation_order.LT.0)) THEN
       PRINT *, 'ERROR: interpolation order should be 0,1,or 3'
       errnum = errnum + 1
    END IF
    IF (ANY(var.GT.n_var)) THEN
       PRINT *, 'ERROR: variable numbers should not be greater than n_var'
       errnum = errnum + 1
    END IF
    IF (errnum.NE.0) STOP 'wavelet_filters.f90: interpolate()'

#ifdef INTERP_DEBUG
    IF (V2) PRINT *, 'mxyz=',mxyz, 'nxyz=',nxyz
    IF (V2) PRINT *, 'prd=',prd
    IF (V2) PRINT *, 'j_mn=',j_mn,'j_lev=',j_lev
#endif
    !===================================================
    ! set constants
    !===================================================
    do_verb = .FALSE.
    IF (PRESENT(VERB)) do_verb = VERB
#ifdef MULTIPROC
    do_sync_u = .TRUE.
    IF (PRESENT(SYNC_U)) do_sync_u = SYNC_U
#endif

    QUAD = 2**dim
    ONE = 1
    i_p(0) = 1                           ! i_p <--> nxyz <--> j_lev
    DO i=1,dim
       i_p(i) = i_p(i-1)*(1+nxyz(i))
       powscale(i) = 2**(dim-i)
    END DO
    
    i_pjm(0) = 1                         ! i_pjm <--> j_mn
    DO i=1,dim
       i_pjm(i) = i_pjm(i-1)*(1+nxyz(i)/2**(j_lev-j_mn))
    END DO

    i_pjj(0) = 1                         ! i_pjj <--> j
    i_pj(0:dim) = i_pjm(0:dim)           ! i_pj  <--> j_mn, j_search

    DO ftc=0,3**dim-1                    ! set array of face types
       face_type(ftc) = ftc              ! in a way that internal
    END DO                               ! points come first
    face_type(0) = (3**dim-1)/2          ! ...
    face_type((3**dim-1)/2) = 0          ! ...




    !===================================================
    ! set tree
    !===================================================
    
    ! initial tree allocation
    ! 1) Nwlt_lev(j_lev,1) is the number of internal and
    !    external points at level j_lev and below
    ! 2) lnwlt(:) should be nonperiodic since tree_set_ptr_iwlt()
    !    places node in a nonperiodic indx_DB related ii at j_mn index
    
    CALL tree_allocate ( j_mn, j_lev, &
         MAX( Nwlt_lev(j_lev,1), PRODUCT(mxyz(1:dim)*2**(j_mn-1)+ONE(1:dim)) ) )
    
    
    ! build AMR tree from indx_DB
    ! allocate space for j_mn (regular nonperiodic grid)
    lnwlt(j_mn) = PRODUCT(mxyz(1:dim)*2**(j_mn-1)+ONE(1:dim))
    CALL tree_allocate_level( j_mn, lnwlt(j_mn), OPTIMIZE_NO ) ! allocate space for j_mn level
    


    
    
#ifdef MULTIPROC
    ! 1) test if it is enough storage in the DB to keep there the data required for the interpolation
    CALL DB_GET_N_VALUES (n_var_of_DB)
    error = .FALSE.
    IF (interpolation_order.EQ.3) THEN
       IF (var_size*(1+dim).GT.n_var_DB) error = .TRUE.
    ELSE
       IF (var_size.GT.n_var_DB) error = .TRUE.
    END IF
    IF (error) THEN
       IF (par_rank.EQ.0) THEN
          WRITE (*,'("Increase the number of variables in the database")')
          WRITE (*,'("to store all the data required for the interpolation.")')
          WRITE (*,'("Another option - change parallel interpolation algorithm.")')
       END IF
       CALL parallel_finalize; STOP
    END IF
    ! 2) write U and DU into the DB
    ii = 1                            ! running index of DB variable to write into
    ALLOCATE (var_mask(1:n_var_DB))   ! parallel synchronization mask
    var_mask = .FALSE.
    DO i=1,var_size
       CALL write_DB ( u(1:nwlt, var(i)), nwlt, ii, ii, j_lev )
       ii = ii + 1
    END DO
    var_size_du = 0
    IF (interpolation_order.EQ.3) THEN
       DO i=1,var_size
          DO j=1,dim
             CALL write_DB ( du(var(i), 1:nwlt, j), nwlt, ii, ii, j_lev )
             ii = ii + 1
          END DO
       END DO
       var_size_du = var_size*dim
    END IF
    var_mask(1:var_size+var_size_du) = .TRUE.
    ! 3) synchronize U for all the required variables
    CALL request_known_list (var_mask, n_var_DB, list_sig, j_lev)
    ! 4) allocate U/DU for the boundary zone
    nwlt_extra = SIZE(nodebuffer_to_recv)
    ALLOCATE (u_extra(nwlt+1:nwlt+nwlt_extra,1:var_size))
    IF (interpolation_order.EQ.3) THEN
       ALLOCATE (du_extra(1:var_size,nwlt+1:nwlt+nwlt_extra,1:dim))
    END IF
#endif
    
    
    ! fill tree at j_mn level (by sig/adj nodes of the current processor)
    DO j = 1, j_mn
       DO ftc = 0, 3**dim - 1
          DO wlt_type = MIN(j-1,1),2**dim-1
             DO j_df = j, j_lev
                DO k = 1, indx_DB(j_df,wlt_type,face_type(ftc),j)%length
                   ! set tree nodes at j_mn according to their 1D coordinate
                   ixyz(1:dim) = INT(MOD(indx_DB(j_df,wlt_type,face_type(ftc),j)%p(k)%ixyz-1,i_p(1:dim))/i_p(0:dim-1))     ! 3D coord on j_lev
                   ixyz = ixyz / 2**(j_lev-j_mn)                        ! 3D coord on j_mn
                   ii = 1+SUM(ixyz(1:dim)*i_pjm(0:dim-1))               ! 1D coord on j_mn
                   
                   CALL tree_set_ptr_iwlt( j_mn, ii, 0, indx_DB(j_df,wlt_type,face_type(ftc),j)%p(k)%i )
                   
#ifdef INTERP_DEBUG
                   IF (V2) WRITE(*,'(A,'//CHAR(ICHAR('0')+dim)//'(I8,X),X,A,I8,X,A,I8,X,A,I8)') &
                        'ixyz=',ixyz,'ii=',ii,'indx_DB%ii=',indx_DB(j_df,wlt_type,face_type(ftc),j)%p(k)%ixyz,&
                        'indx_DB%iwlt=',indx_DB(j_df,wlt_type,face_type(ftc),j)%p(k)%i
#endif
                END DO
             END DO
          END DO
       END DO
    END DO
#ifdef MULTIPROC
    ! 4) fill tree at j_mn level (by sig/adj nodes of the boundary zone of the current processor)
    !    write nwlt_extra values for j_mn
    iwlt_extra = nwlt+1
    DO proc = 0, par_size-1
       IF (proc.NE.par_rank) THEN
          DO j = 1, j_mn
             DO wlt_type = MIN(j-1,1),2**dim - 1
                DO ftc = 0, 3**dim - 1
                   CALL DB_get_initial_type_level_node (proc,wlt_type,j,ftc,ii,c_pointer,list_sig)
                   IF(ii > 0) THEN
                      DO WHILE (is_ok(c_pointer))
                         CALL DB_get_coordinates_by_pointer (c_pointer, ixyz)    ! 3D coord on j_mx
                         ixyz = ixyz / 2**(j_mx-j_mn)                         ! 3D coord on j_mn
                         ii = 1+SUM(ixyz(1:dim)*i_pjm(0:dim-1))               ! 1D coord on j_mn
                         
                         CALL tree_set_ptr_iwlt( j_mn, ii, 0, iwlt_extra )
                         CALL DB_get_function_by_pointer( c_pointer, 1,var_size+var_size_du, tmp_u_du )
                         u_extra (iwlt_extra,1:var_size) = tmp_u_du(1:var_size)
                         IF (interpolation_order.EQ.3) THEN
                            DO iii=1,dim
                               du_extra (1:var_size,iwlt_extra,iii) = tmp_u_du(iii*var_size+1:iii*var_size+var_size)
                            END DO
                         END IF
                         iwlt_extra = iwlt_extra + 1
                         
#ifdef TREE_NODE_POINTER_STYLE_C
                         CALL DB_get_next_type_level_node (proc,wlt_type,j,ftc,c_pointer,c_pointer1,list_sig)
                         c_pointer = c_pointer1  
#elif defined TREE_NODE_POINTER_STYLE_F
                         CALL DB_get_next_type_level_node (c_pointer)
#endif
                      END DO
                   END IF
                END DO
             END DO
          END DO
       END IF
    END DO
#endif
    
    
    ! For a periodic case add nonperiodic nodes at j_mn
    ! (the space has been already allocated for regular grid array at j_mn)
    ! This should be done after the layer j_mn+1 is written, i.e. all nonzero ptr
    ! in j_mn table are pointing already to their children. Therefore the next line
    ! to be moved past the creation of the tree for higher levels.
    ! {IF (ANY(prd(1:dim).EQ.1)) CALL tree_add_nonperiodic_j_mn( j_mn, i_pjm )}
    
    
#ifdef INTERP_DEBUG
    IF (V0) CALL tree_stat( j_mn, j_mn ) ! compute tree statistics
#endif
    
    ! build the rest of the tree
    DO j=j_mn+1, j_lev
       ! set i_pjj constant:
       ! i_p   <--> nxyz <--> j_lev
       ! i_pjm <--> nxyz <--> j_mn
       ! i_pjj <--> nxyz <--> j
       DO i=1,dim
          i_pjj(i) = i_pjj(i-1)*(1+nxyz(i)/2**(j_lev-j))
       END DO
       ! count the nodes of the level to allocate
       lnwlt(j) = Nwlt_lev(j,1) - Nwlt_lev(j-1,1)
       
       CALL tree_allocate_level( j, lnwlt(j), OPTIMIZE_YES )      ! allocate space for the next level
       
       ! fill tree at j level
       fil_len(j) = fil_len(j-1)                      ! running index of the node group to be written is (fil_len+1)
       DO ftc = 0, 3**dim - 1                         ! treat internal points first
          DO wlt_type = 1,2**dim-1
             DO j_df = j, j_lev
                DO k = 1, indx_DB(j_df,wlt_type,face_type(ftc),j)%length
                   ! set tree nodes at j in order
                   !  of their appearance in indx_DB
                   ixyz_jl(1:dim) = INT(MOD(indx_DB(j_df,wlt_type,face_type(ftc),j)%p(k)%ixyz-1,i_p(1:dim))/i_p(0:dim-1)) ! 3D coord on j_lev

#ifdef INTERP_DEBUG
                   IF (V2) WRITE(*,'("ixyz=",'//CHAR(ICHAR('0')+dim)//'(I8,X),"indx_DB%ii=",I8,", indx_DB%iwlt=",I8)' ) &
                        ixyz_jl, &
                        indx_DB(j_df,wlt_type,face_type(ftc),j)%p(k)%ixyz, &
                        indx_DB(j_df,wlt_type,face_type(ftc),j)%p(k)%i
#endif


                   ixyz = ixyz_jl / 2**(j_lev-j_mn)                        ! 3D coord on j_mn
                   ii = 1+SUM(ixyz(1:dim)*i_pjm(0:dim-1))                  ! 1D coord on j_mn
                   ! search for the direct parent of the node of level j
                   ! through the already created tree starting from j_mn;
                   ! current level - j_search, current index - ii
                   j_search = j_mn

#ifdef INTERP_DEBUG
                   IF (V1) WRITE (*,'(A,'//CHAR(ICHAR('0')+dim)//'(I8,X),A,'//CHAR(ICHAR('0')+dim)//'(I8,X),A,I8,A,I8)') &
                        'START SEARCH FOR',ixyz_jl, ',ixyz=',ixyz,&
                        ',from ii=',ii,' of wlt_type=',wlt_type
                   IF (ixyz_jl(1)==22.AND.ixyz_jl(2)==39) THEN
                      PAUSE 'here'
                   END IF
#endif

                   DO WHILE( j_search.LT.j )
                      IF ( tree_get_ptr( j_search, ii ).NE.0 ) THEN
                         ! some parent of the node with some children
                         IF (j_search+1.EQ.j) THEN
                            ! this is the direct parent of the node to be added
                            ! write that newly created child
                            ! add nonzero node
                            CALL tree_set_iwlt( j_search+1, &
                                 tree_get_ptr( j_search, ii )+wlt_type, &
                                 indx_DB(j_df,wlt_type,face_type(ftc),j)%p(k)%i )
                            ! count that nonzero node added
                            fnz_len(j) = fnz_len(j) + 1
#ifdef INTERP_DEBUG
                            IF (V1) PRINT *, '  added (1) to tree level/wlt_type',j_search+1,wlt_type,&
                                 'at',tree_get_ptr( j_search, ii )+wlt_type
#endif
                            EXIT
                         ELSE
#ifdef INTERP_DEBUG
                            IF (V1) PRINT *,'-------------------'
                            IF (V1) PRINT *,'j_search=', j_search
#endif
                            ! this is some parent, so we should walk along the tree
                            ! to find the direct parent of the node;
                            ! compute child index of the node of level j_search+1:
                            ixyz_jl = ixyz_jl - ixyz*2**(j_lev-j_search) ! rel to root coord at j_lev
                            ixyz = ixyz_jl / 2**(j_lev-j_search-1)
#ifdef INTERP_DEBUG
                            IF (V1) PRINT *,'new ixyz_jl=',ixyz_jl
                            IF (V1) PRINT *,'new ixyz   =',ixyz
#endif
                            node_wlt_type = SUM(ixyz(1:dim)*powscale(1:dim))
#ifdef INTERP_DEBUG
                            IF (V1) PRINT *, 'node_wlt_type =', node_wlt_type
#endif
                            ! set the index of the next level node - possible parent
                            ii = tree_get_ptr( j_search, ii ) + node_wlt_type
#ifdef INTERP_DEBUG
                            IF (V1) PRINT *,'new ii=',ii
                            !PAUSE '***'
#endif
                            ! step to the next tree level
                            j_search = j_search + 1
                         END IF
                      ELSE
                         ! some parent of the node without childen
                         IF (j_search+1.EQ.j) THEN
                            ! this is the direct parent of the node to be added
                            ! set pointer to its new children
                            CALL tree_set_ptr( j_search, ii, fil_len(j)+1 )
#ifdef INTERP_DEBUG
                            IF (V1) WRITE (*,'("  adding at",I8,"(at",I8,")")') fil_len(j)+1, fil_len(j)+1+wlt_type
#endif
                            ! propagate parent data as child[0]
                            CALL tree_set_iwlt( j_search+1, fil_len(j)+1, tree_get_iwlt( j_search, ii ) )
                            ! write that newly created child
                            CALL tree_set_iwlt( j_search+1, fil_len(j)+1+wlt_type, indx_DB(j_df,wlt_type,face_type(ftc),j)%p(k)%i )
                            ! repoint running index to the next free space
                            fil_len(j) = fil_len(j) + QUAD
                            ! count that newly created child addition
                            fnz_len(j) = fnz_len(j) + 1
#ifdef INTERP_DEBUG
                            IF (V1) PRINT *, '  added (2) to tree level/wlt_type',j_search+1,wlt_type
#endif
                         ELSE
#ifdef INTERP_DEBUG
                            IF (V1) PRINT *, '  skipped non-AMR node'
#endif
                         END IF
                         ! otherwise, if (j_search+1.NE.j), there is an attempt to add
                         ! a non-AMR node of some higher level without all its parents;
                         ! we are not considering such nodes for interpolation purposes
                         EXIT
                      END IF
                   END DO
                   !IF (j.GT.j_mn+1) PAUSE 'going to the next node'

                END DO
             END DO
          END DO
       END DO



!AR 3/10/2011!  The following 5 lines are added by AR
#ifdef INTEGER8_DEBUG_AR
    !IF (par_rank.EQ.0) THEN
       WRITE (*,'(A, I6, A, I12)')   'interpolate  par_rank=', par_rank, ' ii=', ii
    !ENDIF
#endif  


       
#ifdef MULTIPROC
#ifdef INTERP_DEBUG
       IF (V2) WRITE (*,'("PARALLEL SECTION STARTS:")')
#endif
       ! 4) fill tree at j level (by sig/adj nodes of the boundary zone of the current processor)
       !    write nwlt_extra values for j
       DO proc = 0, par_size-1
          IF (proc.NE.par_rank) THEN
             DO wlt_type = MIN(j-1,1),2**dim - 1
                DO ftc = 0, 3**dim - 1
                   CALL DB_get_initial_type_level_node (proc,wlt_type,j,ftc,ii,c_pointer,list_sig)
                   IF(ii > 0) THEN
                      DO WHILE (is_ok(c_pointer))
                         CALL DB_get_coordinates_by_pointer (c_pointer, ixyz)    ! 3D coord on j_mx
                         ixyz_jl = ixyz / 2**(j_mx-j_lev)                     ! 3D coord on j_lev
                         ii = 1+SUM(ixyz(1:dim)*i_p(0:dim-1))                 ! 1D coord on j_lev
                         ixyz = ixyz_jl / 2**(j_lev-j_mn)                        ! 3D coord on j_mn
                         ii = 1+SUM(ixyz(1:dim)*i_pjm(0:dim-1))                  ! 1D coord on j_mn
                         
                         !------------------------
                         ! search for the direct parent of the node of level j
                         ! through the already created tree starting from j_mn;
                         ! current level - j_search, current index - ii
                         j_search = j_mn
#ifdef INTERP_DEBUG
                         IF (V1) WRITE (*,'(A,'//CHAR(ICHAR('0')+dim)//'(I8,X),A,'//CHAR(ICHAR('0')+dim)//'(I8,X),A,I8,A,I8)') &
                              'START SEARCH FOR',ixyz_jl, ',ixyz=',ixyz,&
                              ',from ii=',ii,' of wlt_type=',wlt_type
#endif
                         DO WHILE( j_search.LT.j )
                            IF ( tree_get_ptr( j_search, ii ).NE.0 ) THEN
                               ! some parent of the node with some children
                               IF (j_search+1.EQ.j) THEN
                                  ! this is the direct parent of the node to be added
                                  ! write that newly created child
                                  ! add nonzero node
                                  CALL tree_set_iwlt( j_search+1, &
                                       tree_get_ptr( j_search, ii )+wlt_type, &
                                       iwlt_extra )
                                  ! count that nonzero node added
                                  fnz_len(j) = fnz_len(j) + 1
#ifdef INTERP_DEBUG
                                  IF (V1) PRINT *, '  added (1) to tree level/wlt_type',j_search+1,wlt_type,&
                                       'at',tree_get_ptr( j_search, ii )+wlt_type
#endif
                                  EXIT
                               ELSE
#ifdef INTERP_DEBUG
                                  IF (V1) PRINT *,'-------------------'
                                  IF (V1) PRINT *,'j_search=', j_search
#endif
                                  ! this is some parent, so we should walk along the tree
                                  ! to find the direct parent of the node;
                                  ! compute child index of the node of level j_search+1:
                                  ixyz_jl = ixyz_jl - ixyz*2**(j_lev-j_search) ! rel to root coord at j_lev
                                  ixyz = ixyz_jl / 2**(j_lev-j_search-1)
#ifdef INTERP_DEBUG
                                  IF (V1) PRINT *,'new ixyz_jl=',ixyz_jl
                                  IF (V1) PRINT *,'new ixyz   =',ixyz
#endif
                                  node_wlt_type = SUM(ixyz(1:dim)*powscale(1:dim))
#ifdef INTERP_DEBUG
                                  IF (V1) PRINT *, 'node_wlt_type =', node_wlt_type
#endif
                                  ! set the index of the next level node - possible parent
                                  ii = tree_get_ptr( j_search, ii ) + node_wlt_type
#ifdef INTERP_DEBUG
                                  IF (V1) PRINT *,'new ii=',ii
                                  !PAUSE '***'
#endif
                                  ! step to the next tree level
                                  j_search = j_search + 1
                               END IF
                            ELSE
                               ! some parent of the node without childen
                               IF (j_search+1.EQ.j) THEN
                                  ! this is the direct parent of the node to be added
                                  ! set pointer to its new children
                                  CALL tree_set_ptr( j_search, ii, fil_len(j)+1 )
#ifdef INTERP_DEBUG
                                  IF (V1) WRITE (*,'("  adding at",I8,"(at",I8,")")') fil_len(j)+1, fil_len(j)+1+wlt_type
#endif
                                  ! propagate parent data as child[0]
                                  CALL tree_set_iwlt( j_search+1, fil_len(j)+1, tree_get_iwlt( j_search, ii ) )
                                  ! write that newly created child
                                  CALL tree_set_iwlt( j_search+1, fil_len(j)+1+wlt_type, iwlt_extra )
                                  ! repoint running index to the next free space
                                  fil_len(j) = fil_len(j) + QUAD
                                  ! count that newly created child addition
                                  fnz_len(j) = fnz_len(j) + 1
#ifdef INTERP_DEBUG
                                  IF (V1) PRINT *, '  added (2) to tree level/wlt_type',j_search+1,wlt_type
#endif
                               ELSE
#ifdef INTERP_DEBUG
                                  IF (V1) PRINT *, '  skipped non-AMR node'
#endif
                               END IF
                               ! otherwise, if (j_search+1.NE.j), there is an attempt to add
                               ! a non-AMR node of some higher level without all its parents;
                               ! we are not considering such nodes for interpolation purposes
                               EXIT
                            END IF
                         END DO
                         
                         CALL DB_get_function_by_pointer( c_pointer, 1,var_size+var_size_du, tmp_u_du )
                         u_extra (iwlt_extra,1:var_size) = tmp_u_du(1:var_size)
                         IF (interpolation_order.EQ.3) THEN
                            DO iii=1,dim
                               du_extra (1:var_size,iwlt_extra,iii) = tmp_u_du(iii*var_size+1:iii*var_size+var_size)
                            END DO
                         END IF
                         
                         iwlt_extra = iwlt_extra + 1
                         
#ifdef TREE_NODE_POINTER_STYLE_C
                         CALL DB_get_next_type_level_node (proc,wlt_type,j,ftc,c_pointer,c_pointer1,list_sig)
                         c_pointer = c_pointer1  
#elif defined TREE_NODE_POINTER_STYLE_F
                         CALL DB_get_next_type_level_node (c_pointer)
#endif
                      END DO
                   END IF
                END DO
             END DO
          END IF
       END DO

#endif

       !PRINT *, 'j=',j,'iwlt_extra=', iwlt_extra
       
       ! set beg_index for the next level
       ! (fil_len is cumulative)
       beg_index(j+1) = beg_index(j) + fil_len(j) - fil_len(j-1)
       
#ifdef INTERP_DEBUG
       IF (V0) CALL tree_stat( j_mn, j ) ! compute tree statistics for each level
#endif
    END DO


    
    ! For a periodic case add nonperiodic nodes at j_mn
    ! (the space has been already allocated for regular grid array at j_mn)
    ! This should be done after the layer j_mn+1 is written, i.e. all nonzero ptr
    ! in j_mn table are pointing already to their children. 
    IF (ANY(prd(1:dim).EQ.1)) CALL tree_add_nonperiodic_j_mn( j_mn, i_pjm )

    
    
    ! compute tree statistics for the final tree
    IF (do_verb) CALL tree_stat( j_mn, j_lev )
    
    
    !DO ii=i_pjm(1),i_pjm(dim)
    !   IF (tree%arr(ii)%iwlt.EQ.0)
    !CALL parallel_finalize; STOP
    
#ifdef INTERP_DEBUG
    IF (V3) THEN
       PRINT *, 'NO INTERPOLATION HAS BEEN PERFORMED'
       CALL tree_deallocate
       RETURN
    END IF
#endif
    
#ifdef INTERP_DEBUG
    IF (V2) PAUSE 'START INTERPOLATION'
    IF (V2) PRINT *, 'i_pjm=',i_pjm
#endif

    !===================================================
    ! interpolate
    !===================================================
    DO point_number=1,x_size
       
       ! input x array holds real world coordinates
       ! find respective integer coordinates in [0:nxyz] array
       CALL get_xx_index( x(1:dim,point_number), i_index_jl )    ! index at j_lev <--> nxyz
       
       ! adjust integer coordinates to be \in \[0,nxyz\)
       ! so the upper box boundary .LE. nxyz
       ! (interpolation will use real coordinates anyway)
       DO idim=1,dim
          i_index_jl(idim) = MAX( 0, MIN(i_index_jl(idim),nxyz(idim)-1) ) ! index at j_lev <--> nxyz
       END DO
       
       ! set initial box at j_mn level the point belongs to
       i_index = i_index_jl / 2**(j_lev-j_mn)                    ! index at j_mn level
       
       
#ifdef INTERP_DEBUG
       IF (V2) PRINT *, 'x=',x(1:dim,point_number),'ix_jl=',i_index_jl(1:dim)    ! [0..nxyz]
       IF (V2) PRINT *, 'i_index=',i_index
#endif
       
       box(1) = 1+SUM(i_index(1:dim)*i_pjm(0:dim-1))   ! one dimensional box at j_mn level
       box(2) = box(1) + 1                             !   that point belongs to
       box_marker = 2                                  ! box(2**dim) filled until that point
       DO idim=2,dim                                   ! as ii, ii+1
          DO i=1,box_marker                                 ! ii,ii+1,ii+X,ii+1+X
             box( box_marker+i ) = box( i ) + i_pjm(idim-1)    ! etc.
          END DO
          box_marker = box_marker * 2
       END DO
       ! -------------------------
       ! search for a smaller box
       j_search = j_mn      ! this will be the level of the box
       ! box                ! indices in tree(box_j())%arr(:)
       box_coord = box      ! 1D coord at j_search
       box_j = j_mn         ! levels of the nodes
       i_pjj = i_pjm        ! to equalize treatment of level j_mn nodes
#ifdef INTERP_DEBUG
       IF (V2) WRITE (*,'("initial box[",'//CHAR(ICHAR('0')+2**dim)//'I8"] at j=",'//CHAR(ICHAR('0')+2**dim)//'I4)') &
            box, box_j
#endif


       !CYCLE
       DO WHILE( j_search.LT.j_lev )
          ! i_pjj <--> nxyz <--> j_search+1
          ! i_pj <--> nxyz <--> j_search
          DO i=1,dim
             i_pjj(i) = i_pjj(i-1)*(1+nxyz(i)/2**(j_lev-j_search-1))
             i_pj(i) = i_pj(i-1)*(1+nxyz(i)/2**(j_lev-j_search))
          END DO
          
          i_index = i_index_jl / 2**(j_lev-j_search-1)               ! index at j_search+1 level of the lower box corner
          
#ifdef INTERP_DEBUG
          IF (V2) PRINT *, 'i_pjj=',i_pjj
          IF (V2) PRINT *, 'i_pj =',i_pj
          IF (V2) PRINT *, 'i_index=',i_index
#endif
          
          box_coord_tmp(1) = 1+SUM(i_index(1:dim)*i_pjj(0:dim-1))    ! one dimensional box at j_search+1 level
          box_coord_tmp(2) = box_coord_tmp(1) + 1
          box_marker = 2
          DO idim=2,dim                                                            ! as ii, ii+1
             DO i=1,box_marker                                                     ! ii,ii+1,ii+X,ii+1+X
                box_coord_tmp( box_marker+i ) = box_coord_tmp( i ) + i_pjj(idim-1) ! etc.
             END DO
             box_marker = box_marker * 2
          END DO
          ! box_coord contains 1D coordinates at j_search level
          ! and box_cord_tmp ... at j_search+1 level;
          ! coordinates are to be replaced by index in tree(box_j())%arr(:)
          box_complete = .TRUE.
          i_loop: DO i=1,2**dim
             ! tree walk for each node of the box:
             ! get 3D coordinates for box_coord_tmp(i)
             i_index(1:dim) = INT(MOD(box_coord_tmp(i)-1,i_pjj(1:dim))/i_pjj(0:dim-1))    ! at j_search+1 level
             ! get root 1D coordinate for box_coord_tmp(i) and its child index
             ixyz = i_index / 2                                                           ! at j_search level
             ii = 1+SUM(ixyz(1:dim)*i_pj(0:dim-1))
             chi = SUM(MOD(i_index(1:dim),2)*powscale(1:dim))

#ifdef INTERP_DEBUG
             IF (V2) PRINT *, 'chi=',chi,'ii=',ii
             IF (V2) WRITE (*, '("box_coord_tmp ",I1," [",'//CHAR(ICHAR('0')+2**dim)//'(I8,X),"] is child ",I8," of root #",I8," [",'//CHAR(ICHAR('0')+2**dim)//'(I8,X),"]")') &
                  i, i_index, chi, ii, ixyz
#endif


             ! for each root find its level and index in tree(:)%arr(:)
             ! box_coord(:) has 1D coordinates at j_search level,
             ! box(:) and box_j(:) have indices and levels in tree(:)%arr(:)
             k_loop: DO k=1,2**dim
                IF (ii.EQ.box_coord(k)) THEN
#ifdef INTERP_DEBUG
                   IF (V2) PRINT *, ' transfer to indices: FOUND box_coord(k)/box(k) =',box_coord(k),box(k)
                   IF (V2) PRINT *, ' tree(box_j(k))%arr( box(k) )%iwlt=',tree_get_iwlt( box_j(k), box(k) )
                   IF (V2) PRINT *, ' tree(box_j(k))%arr( box(k) )%ptr=',tree_get_ptr( box_j(k), box(k) )
#endif
                   ! box(k) is the index in tree(box_j(k))%arr(:),
                   ! where box_j(k) <= j_search
                   ! so we can set box_tmp (box at level j_search+1) as index in tree(j_search+1)%arr(:)
                   IF ( tree_get_ptr( j_search, box(k) ).EQ.0 ) THEN
                      ! either it is the same node, or the node does not exist
                      IF ( chi.NE.0 ) THEN
                         box_complete = .FALSE.     ! there is no such child
                         EXIT i_loop
                      ELSE
                         box_tmp(i) = box(i)
                         box_j_tmp(i) = box_j(i)
                         EXIT k_loop
                      END IF
                      ! ----- PROBLEM is HERE with corner nodes --------
                      
                   ELSE
                      ! this will be a node of j_search+1 level
                      box_tmp(i) = tree_get_ptr( j_search, box(k) ) + chi
                      box_j_tmp(i) = j_search+1
                      EXIT k_loop
                   END IF
                END IF
             END DO k_loop
             ! test if the node (at its level, which might be j_search+1 or less) exists
             IF ( tree_get_iwlt( box_j_tmp(i), box_tmp(i) ).EQ.0 ) THEN
                box_complete = .FALSE.
                EXIT i_loop
             END IF
             ! process the next node of the box at j_search+1 level
          END DO i_loop

#ifdef INTERP_DEBUG
          IF (V2) WRITE (*,'("box_coord_tmp[",'//CHAR(ICHAR('0')+2**dim)//'I8"] at j=",I4)') box_coord_tmp,j_search+1
          IF (V2) WRITE (*,'("      box_tmp[",'//CHAR(ICHAR('0')+2**dim)//'I8"] at j=",I4)') box_tmp,j_search+1
#endif

          IF (box_complete) THEN
             j_search = j_search + 1
#ifdef INTERP_DEBUG
             IF (V2) PRINT *, 'box_tmp is complete'
#endif
             box_coord = box_coord_tmp        ! 1D coordinates at j_search+1 level
             box_j = box_j_tmp
             box = box_tmp                    ! indices in tree(box_j())%arr(:)
          ELSE
             ! incomplete box_tmp, box is the final box, terminate search
             !j_search = j_search + 1
#ifdef INTERP_DEBUG
             IF (V2) PRINT *, 'box_tmp is not complete'
#endif
             EXIT
          END IF
          ! continue to the next level
       END DO
       ! finish search for a smaller box
       ! -------------------------    


#ifdef INTERP_DEBUG
       IF (V2) WRITE (*,'("FINAL BOX (coord)  [",'//CHAR(ICHAR('0')+QUAD)//'I8"], j_search=",I4)') box_coord,j_search
       IF (V2) WRITE (*,'("FINAL BOX (indices)[",'//CHAR(ICHAR('0')+QUAD)//'I8"] at j=",'//CHAR(ICHAR('0')+2**dim)//'I4)')&
            box,box_j
#endif

       ! convert box to iwlt indices (nwlt + nwlt_extra)
       DO i=1,2**dim
          box(i) = tree_get_iwlt( box_j(i), box(i) )
       END DO
#ifdef INTERP_DEBUG
       IF (V2) WRITE (*,'("FINAL BOX (iwlt)   [",'//CHAR(ICHAR('0')+2**dim)//'I8"]")') box
#endif
       ! set i_pjj <--> j_search
       DO i=1,dim
          i_pjj(i) = i_pjj(i-1)*(1+nxyz(i)/2**(j_lev-j_search))
       END DO
#ifdef INTERP_DEBUG
       IF (V2) PRINT *, 'i_pjj=',i_pjj
#endif
       ! set box size, box corner coordinates(at j_lev), and
       ! relative to box corner point coordinate \in \[0,1\]
       ixyz(1:dim) = (INT(MOD(box_coord(1)-1,i_pjj(1:dim))/i_pjj(0:dim-1)))*2**(j_lev-j_search)       ! (0,0,...)
       ixyz_jl(1:dim) = (INT(MOD(box_coord(QUAD)-1,i_pjj(1:dim))/i_pjj(0:dim-1)))*2**(j_lev-j_search) ! (1,1,...)
#ifdef INTERP_DEBUG
       IF (V2) WRITE (*, '("corner coordinates:",'//CHAR(ICHAR('0')+QUAD)//'I8,X,'//CHAR(ICHAR('0')+QUAD)//'I8)') &
            ixyz, ixyz_jl
#endif
       DO idim=1,dim
          rel_dx(idim) = xx(ixyz_jl(idim),idim) - xx(ixyz(idim),idim)
          rel_x(idim) = x(idim,point_number) - xx(ixyz(idim),idim)
          !PRINT *,'dim=',idim,'xx=',xx(i_index(idim),idim)
       END DO
       rel_x(1:dim) = rel_x(1:dim) / rel_dx(1:dim)
#ifdef INTERP_DEBUG
       IF (V2) PRINT *,'rel_x=',rel_x
       IF (V2) PAUSE
#endif


       ! write output
       IF (interpolation_order.EQ.0) THEN
          !-----------------------
          ! 0 order interpolation
          ! 
          ! (nearest leftmost box corner)
          !-----------------------
          IF (box(1).LE.nwlt) THEN
             DO i=1,var_size
                res(i,point_number) = u( box(1), var(i) )
             END DO
          ELSE
             res(1:var_size,point_number) = u_extra( box(1), 1:var_size )
          END IF
          
       ELSE IF (interpolation_order.EQ.1) THEN
          !-----------------------
          ! 1 order interpolation
          !
          ! (linear in each direction)
          !-----------------------
          ! write box of u values
          DO i=1,2**dim
             IF (box(i).LE.nwlt) THEN
                DO ivar=1,var_size
                   ubox(ivar,i) = u( box(i), var(ivar) )
                END DO
             ELSE
                !PRINT *, 'box(i)=',box(i), 'nwlt/nwlt_extra=',nwlt,nwlt_extra
                ubox(1:var_size,i) = u_extra( box(i), 1:var_size )
             END IF
          END DO
          CALL interpolate_to_box_1( ubox, res(1,point_number), var_size, var, rel_x )
          
       ELSE
          ! write box of u and du values (rescaled to \[0,1\])
          DO i=1,2**dim
             IF (box(i).LE.nwlt) THEN
                DO ivar=1,var_size
                   ubox(ivar,i) = u( box(i), var(ivar) )
                   dubox(ivar,i,1:dim) = du( var(ivar), box(i), 1:dim ) * rel_dx(1:dim)
                END DO
             ELSE
                ubox(1:var_size,i) = u_extra( box(i), 1:var_size )
                DO ivar=1,var_size
                   dubox(ivar,i,1:dim) = du_extra( ivar, box(i), 1:dim ) * rel_dx(1:dim)
                END DO
             END IF
          END DO
          !-----------------------
          ! 3 order interpolation
          !
          ! (3rd order in each direction)
          !-----------------------
          CALL interpolate_to_box_3( ubox, dubox, res(1,point_number), var_size, var, rel_x )
       END IF
       ! process next point
    END DO
    
    !===================================================
    ! delete tree
    !===================================================
#ifdef MULTIPROC
    IF (ALLOCATED(var_mask)) DEALLOCATE (var_mask)
    IF (ALLOCATED(u_extra)) DEALLOCATE (u_extra)
    IF (ALLOCATED(du_extra))DEALLOCATE (du_extra)
#endif
    CALL tree_deallocate
    
  END SUBROUTINE interpolate





  !-------------------------------------------------------------------------------------------
  !
  ! u is passed in real space
  ! GridTest_filt: 0 - grid level filter, 1 - test level filter
  !
  SUBROUTINE mdl_filt_by_type(u_in,ne_local,GridTest_filt)
    IMPLICIT NONE
    INTEGER,    INTENT (IN) :: ne_local, GridTest_filt
    REAL (pr), DIMENSION (1:nwlt,1:ne_local) , INTENT (INOUT) :: u_in

    
    IF(GridTest_filt ==  GRIDfilter) THEN ! Grid-level filter
       IF(mdl_filt_type_grid == MDL_FILT_LPlocal) THEN
          CALL local_lowpass_filt( u_in(:,1:ne_local), j_lev, nwlt, ne_local, 1, ne_local, &
               lowpassfilt_support_GRID, lowpass_filt_type_GRID, tensorial_filt )
       ELSE
          CALL wlt_filt_wmask( u_in(:,1:ne_local),ne_local,u_g_mdl_gridfilt_mask)
       END IF
    ELSE IF(GridTest_filt == TESTfilter) THEN ! Test-level filter
       IF(mdl_filt_type_test == MDL_FILT_LPlocal) THEN
          CALL local_lowpass_filt( u_in(:,1:ne_local), j_lev, nwlt, ne_local, 1, ne_local, &
               lowpassfilt_support_TEST, lowpass_filt_type_TEST, tensorial_filt )
          IF (verb_level.GT.0) PRINT *, 'mdl_filt_by_type=local_lowpass'
       ELSE
          CALL wlt_filt_wmask( u_in(:,1:ne_local),ne_local,u_g_mdl_testfilt_mask)
       END IF
    END IF

  END SUBROUTINE mdl_filt_by_type


  SUBROUTINE wlt_filt_wmask(u,ie,mask)
    USE parallel
    IMPLICIT NONE
    INTEGER,    INTENT (IN) :: ie
    REAL (pr), DIMENSION (1:nwlt,1:ie) , INTENT (INOUT) :: u
    REAL (PR), DIMENSION (1:nwlt)      ,INTENT (IN) :: mask

    INTEGER i
    REAL (pr) :: temp
    
    IF (verb_level.GT.0) THEN
       PRINT *,'wlt_filt_wmask(), nwlt = ',  nwlt
       PRINT *,'wlt_filt_wmask(), ie = ',  ie
       temp = sum(mask)
       CALL parallel_global_sum( REAL=temp )
       PRINT *,'wlt_filt_wmask(), sum(mask) = ',  temp
    END IF

    CALL c_wlt_trns (u, ie, 1, ie, HIGH_ORDER, WLT_TRNS_FWD)

    DO i = 1, ie
       u(:,i) = u(:,i)*mask
    END DO

    CALL c_wlt_trns (u,  ie, 1, ie, HIGH_ORDER, WLT_TRNS_INV)

  END SUBROUTINE wlt_filt_wmask
  !
  ! setup the model filter masks. (only called if we are using a sgs model)
  ! u - filed components in wlt space
  ! nwlt number of active wlts
  !
  ! u
  ! scl
  ! type_grid   - type of grid filter
  ! type_test   _ type of test filter
  !   - defined types: 
  !         MDL_FILT_NONE         - 0: no filter (mask all 1.0_pr)
  !         MDL_FILT_EPS          - 1:  u > eps filter
  !         MDL_FILT_2EPS         - 2:  u > 2eps filter
  !         MDL_FILT_jm1          - 3:  retain all collocation pts up to level j-1 (LES)
  !         MDL_FILT_jm2          - 4:  retain all collocation pts up to level j-2 (LES)
  !         MDL_FILT_2EPSplus     - 5:  u > 2eps filter + adjecent zone 
  !         MDL_FILT_LPlocal      - 6:  u > local low-pass filter
  !         MDL_FILT_EPSexpl      - 7   u > eps_explicit filter  
  !         MDL_FILT_EPSexplplus  - 8   u > eps_explicit filter + adjecent zone 
  !         MDL_FILT_2EPSexpl     - 9   u > 2eps_explicit filter  
  !         MDL_FILT_2EPSexplplus - 10  u > 2eps_explicit filter + adjecent zone 
  !
  ! 
  SUBROUTINE  make_mdl_filts( u , n_filt, scl , type_grid, type_test) 
    USE variable_mapping 
    USE parallel
    IMPLICIT NONE
    INTEGER  , INTENT(IN) :: type_grid, type_test, n_filt
    REAL (pr), DIMENSION (1:nwlt,1:n_filt) , INTENT (IN) :: u
    REAL (pr), DIMENSION (1:n_var), INTENT (IN) :: scl
    
    REAL (pr), DIMENSION (1:nwlt,1:n_filt)  :: utmp
    INTEGER ,SAVE :: nwlt_saved = -1 ! set to -1 so masks get allocated on first call
    REAL(pr) :: tmp1, tmp2

    IF (verb_level.GT.0) PRINT *,' top make_mdl_filts nwlt =' , nwlt

    !
    ! Allocate masks if necessary
    !

    IF( nwlt /= nwlt_saved ) THEN
       IF( ALLOCATED(u_g_mdl_gridfilt_mask)) DEALLOCATE(u_g_mdl_gridfilt_mask)
       IF( ALLOCATED(u_g_mdl_testfilt_mask)) DEALLOCATE(u_g_mdl_testfilt_mask)
       ALLOCATE(u_g_mdl_gridfilt_mask(1:nwlt))
       ALLOCATE(u_g_mdl_testfilt_mask(1:nwlt))
       nwlt_saved = nwlt
    END IF
    IF( .NOT. ALLOCATED(u_g_mdl_gridfilt_mask)) ALLOCATE(u_g_mdl_gridfilt_mask(1:nwlt))
    IF( .NOT. ALLOCATED(u_g_mdl_testfilt_mask)) ALLOCATE(u_g_mdl_testfilt_mask(1:nwlt))


    u_g_mdl_gridfilt_mask = 0.0_pr
    u_g_mdl_testfilt_mask = 0.0_pr

    utmp(:,1:n_filt) = u(:,1:n_filt) !save a tmp copy of u

    CALL c_wlt_trns (utmp(1:nwlt,1:n_filt), n_filt, 1, n_filt, HIGH_ORDER, WLT_TRNS_FWD)

    ! grid filter

    CALL mdl_filt( u_g_mdl_gridfilt_mask, utmp(1:nwlt,1:n_filt) , scl(1:n_filt) , n_filt, type_grid )

    ! test filter
    CALL mdl_filt( u_g_mdl_testfilt_mask, utmp(1:nwlt,1:n_filt) , scl(1:n_filt) , n_filt, type_test )
    
    tmp1 = SUM(u_g_mdl_gridfilt_mask(1:nwlt))
    tmp2 = SUM(u_g_mdl_testfilt_mask(1:nwlt))
    CALL parallel_global_sum( REAL=tmp1 )
    CALL parallel_global_sum( REAL=tmp2 )
    IF (par_rank.EQ.0.AND.verb_level.GT.0) THEN
       PRINT *, 'in make_mdl_filts, SUM(u_g_mdl_gridfilt_mask) = ', tmp1
       PRINT *, 'in make_mdl_filts, SUM(u_g_mdl_testfilt_mask) = ', tmp2
       PRINT *, 'nwlt, nwlt_saved',nwlt, nwlt_saved
    END IF
    
  END SUBROUTINE make_mdl_filts

  !
  ! set the mdl filt mask
  ! u - in wlt space
  ! scl
  ! type - type of filter
  !
  SUBROUTINE mdl_filt( filt_mask, u , scl , n_filt, type) 
    USE wlt_trns_vars      ! indx_DB
    IMPLICIT NONE
    INTEGER  , INTENT(IN) :: n_filt, type
    REAL (pr), DIMENSION (1:nwlt,1:n_filt) , INTENT (IN) :: u
    REAL (pr), DIMENSION (1:n_filt), INTENT (IN) :: scl
    REAL (pr), DIMENSION (1:nwlt) , INTENT (INOUT) :: filt_mask

    REAL (pr), DIMENSION (1:n_filt) :: m
    INTEGER :: i
    INTEGER :: j, jflt, j_df, wlt_type, face_type, k


    filt_mask(:) = 0.0_pr
    ! no filter
    IF( type == MDL_FILT_NONE ) THEN
       filt_mask(:) = 1.0_pr 
       ! eps or 2eps filter
    ELSE IF( type == MDL_FILT_EPS .or. type == MDL_FILT_2EPS .or. &
         type == MDL_FILT_EPSexpl .or. type == MDL_FILT_2EPSexpl) THEN
       IF( type == MDL_FILT_EPS ) THEN 
          m = eps*scl
          IF (verb_level.GT.0) PRINT *,'m = eps*scl = ', m
       ELSE IF( type == MDL_FILT_2EPS )  THEN 
          m = 2.0_pr * eps*scl
          IF (verb_level.GT.0) PRINT *,'m = 2 * eps*scl = ', m
       ELSE IF( type == MDL_FILT_EPSexpl )  THEN
          m = eps_expl*scl
          IF (verb_level.GT.0) PRINT *,'m = eps_expl*scl = ', m
       ELSE IF( type == MDL_FILT_2EPSexpl )  THEN
          m = 2.0_pr*eps_expl*scl
          IF (verb_level.GT.0) PRINT *,'m = 2*eps_expl*scl = ', m
       END IF
       DO i = 1, nwlt
          IF( ANY(ABS(u(i,1:dim)) >= m) ) filt_mask(i) = 1.0_pr
       END DO
    ELSE IF( type ==  MDL_FILT_jm1 .OR. type ==  MDL_FILT_jm2 ) THEN
       IF( type ==  MDL_FILT_jm1) jflt = j_lev-1 ! LES level filter at j-1
       IF( type ==  MDL_FILT_jm2) jflt = j_lev-2 ! LES level filter at j-2
       DO j = 1, jflt
          DO wlt_type = MIN(j-1,1),2**dim-1
             DO face_type = 0, 3**dim - 1
                DO j_df = j, j_lev
                   DO k = 1, indx_DB(j_df,wlt_type,face_type,j)%length  
                      filt_mask(indx_DB(j_df,wlt_type,face_type,j)%p(k)%i) = 1.0_pr
                   END DO
                END DO
             END DO
          END DO
       END DO
    ELSE IF( type ==  MDL_FILT_2EPSplus ) THEN
       CALL mdl_filt_EPSplus( filt_mask, u, scl, 2.0_pr*eps, dim)
    ELSE IF( type ==  MDL_FILT_EPSexplplus ) THEN
       CALL mdl_filt_EPSplus( filt_mask, u, scl, eps_expl, dim)
    ELSE IF( type ==  MDL_FILT_2EPSexplplus ) THEN
       CALL mdl_filt_EPSplus( filt_mask, u, scl, 2.0_pr*eps_expl, dim)
    ELSE IF( type ==  MDL_FILT_LPlocal ) THEN
       !This option is achieve my different subroutine: local_lowpass_filt
    ELSE
       ! ERROR
       PRINT *,' Error in mdl_filt(), unknown filt type :', type
       PRINT *,' Exiting....'
       stop
    END IF

  END SUBROUTINE mdl_filt

  !
  ! Find local low-pass filtered u, vector version
  !
  ! Arguments
  !u, j_in, nlocal
  !  dimensions of u are u(1:nlocal,1:ie) 
  !  filtering is done on  u(1:nlocal,mn_var:mx_var)
  ! u -> comes in unfiltered -> comes out filtered
  !If fitler coefficeints are positive, filter is positive, since it does not use wavelet transform to interpolate to
  !ghost points.
  SUBROUTINE local_lowpass_filt(u_local, j_in, nlocal, ie, mn_var, mx_var, filter_support, lowpassFilterType, tensorialFilt )
    USE precision
    USE sizes
    USE wlt_trns_vars
    USE parallel
#ifdef MULTIPROC
    USE wlt_trns_mod
    USE db_tree
#endif
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: j_in, nlocal
    INTEGER, INTENT(IN) ::   ie  ! number of equations in u
    INTEGER, INTENT(IN) ::  mn_var, mx_var   ! min and max of second index of u that we are doing the 1st derivatives for.
    REAL (pr), DIMENSION (1:nlocal,ie), INTENT (INOUT) :: u_local
    REAL (pr), DIMENSION (1:nlocal,ie) :: fuh
    INTEGER, INTENT(IN) :: filter_support ! support of low-pass filter -filter_support:filter_support on thelocal scale
    INTEGER, INTENT(IN) :: lowpassFilterType ! 0 - top-hat volume averged, 1 - trapezoidal 
    LOGICAL, INTENT(IN) :: tensorialFilt ! if .TRUE. uses tensorial filter for comparison with lines
    REAL (pr) :: wf_1d(-filter_support:filter_support)
    REAL (pr) :: wf((2*filter_support+1)**dim), wf_loc((2*filter_support+1)**dim), u_loc((2*filter_support+1)**dim,1:ie)
    INTEGER :: n_lowpassfilt, lowpassfilt_shift 
    INTEGER :: i_p(0:dim), i_p_f(0:dim), i_l(dim), i_h(dim), ixyz(dim), ixyz1(3)
    INTEGER :: step, i, j, k, kk, idim                                                                    
    LOGICAL :: mask_loc((2*filter_support+1)**dim)
    INTEGER :: ONE, i1(1), j_df, wlt_type, face_type
    REAL(pr) :: umm(2)

    INTEGER*8 :: ii, i_p_xyz(0:dim)


#ifdef MULTIPROC
    REAL(pr) :: u_tmp(mn_var:mx_var)
    LOGICAL :: var_mask(ie)

!!$    DO i = 1,ie
!!$       umm(1) = MINVAL(u_local(:,i)); umm(2) = MAXVAL(u_local(:,i))
!!$       CALL parallel_global_sum (REALMINVAL=umm(1))
!!$       CALL parallel_global_sum (REALMAXVAL=umm(2))
!!$       IF (par_rank.EQ.0) THEN
!!$          WRITE(*,'( "     minmax u_local(:,",i2.2,") ", 2(G20.12,1X) )') &
!!$               i, umm
!!$       END IF
!!$    END DO


    CALL  write_DB (u_local, nlocal, mn_var, mx_var, MIN(j_in,j_lev))
    var_mask = .TRUE.
    ! any of them could be used here
    CALL request_known_list (var_mask, ie, list_sig, MIN(j_in,j_lev))
!!$    CALL make_list_and_request (var_mask, ie, list_sig, MIN(j_in,j_lev))
#endif

    !WRITE (*,'("1 MIN/MAXVAL(u_loc)=",2(E17.10,X))') MINVAL(u_local(:,mn_var:mx_var)), MAXVAL(u_local(:,mn_var:mx_var))

    ONE = 1
    ixyz = 0
    ixyz1 = 0 ! to ensure that it works with i_c(:,:,:), which is always 3-D

    n_lowpassfilt = 2*filter_support+1
    lowpassfilt_shift = filter_support+1

    wf_1d = 0.0_pr
    IF(lowpassFilterType == lowpassFilterType_TRAPEZOIDAL) THEN ! trapezoidal volume averaged filter
       DO ii = -1,1
          wf_1d(ii) = 1.0_pr/REAL(2*2**ABS(ii),pr) 
       END DO
    ELSE ! top-hat volume averaged filter with filter support filt_support
       wf_1d(-filter_support:filter_support) = 1.0_pr / REAL(n_lowpassfilt,pr)
    END IF

    IF( tensorialFilt ) THEN !tensorial filter
       PRINT *, 'Curently is not implemented'
       PRINT *, 'If you need tensorial filters, then the only way to do it is to do sequantial filtering from i=1:dim'
       CALL parallel_finalize
       STOP
    ELSE    !volume filter
       !--------------- global use ----------------------------------------
       i_p(0) = 1
       i_p_f(0) = 1
       i_p_f(1:dim) = n_lowpassfilt
       DO ii =1, dim
          i_p_f(ii) = i_p_f(ii-1)*i_p_f(ii)
       END DO


       wf = 1.0_pr
       DO ii = 1,i_p_f(dim)
          ixyz(1:dim) = -filter_support + INT(MOD(ii-1,i_p_f(1:dim))/i_p_f(0:dim-1)) 
          DO idim = 1, dim
             wf(ii) = wf(ii)*wf_1d(ixyz(idim))
          END DO
       END DO

       !=============================== new way ==============================================
       i_p_xyz(0) = 1
       DO i=1,dim
          i_p_xyz(i) = i_p_xyz(i-1)*(1+nxyz(i))
       END DO


       !low-pass filtering at j_filt_local level all the points at level of resolution j_filt_local and below
       DO j_df = MIN(j_in,j_lev),  MIN(j_in,j_full), -1 !@
          step = 2**(j_lev-j_df)
          DO j = 1, j_df
             DO wlt_type = MIN(j-1,1),2**dim-1
                DO face_type = 0, 3**dim - 1
                   DO k = 1, indx_DB(j_df,wlt_type,face_type,j)%length  
                      i = indx_DB(j_df,wlt_type,face_type,j)%p(k)%i
                      ixyz(1:dim) = INT(MOD(indx_DB(j_df,wlt_type,face_type,j)%p(k)%ixyz-1,i_p_xyz(1:dim))/i_p_xyz(0:dim-1))
                      i_l = ixyz(1:dim) - filter_support*step
                      i_h = ixyz(1:dim) + filter_support*step
                      i_l = (1-prd(1:dim)) * MAX(0,i_l) + prd(1:dim) * i_l
                      i_h = (1-prd(1:dim)) * MIN(nxyz(1:dim),i_h) + prd(1:dim) * i_h
                      i_l = ( i_l-ixyz(1:dim) ) / step
                      i_h = ( i_h-ixyz(1:dim) ) / step

                      i_p(1:dim) = (i_h-i_l) + 1
                      DO ii =1, dim
                         i_p(ii) = i_p(ii-1)*i_p(ii)
                      END DO
                      DO ii = 1,i_p(dim)
                         ixyz1(1:dim) = i_l + INT(MOD(ii-1,i_p(1:dim))/i_p(0:dim-1)) 
                         kk = 1 + SUM( (filter_support+ixyz1(1:dim))*i_p_f(0:dim-1) ) !how to convert 3-D coordinate to 1-D!
                         ixyz1(1:dim) = ixyz(1:dim)+ step*ixyz1(1:dim)
                         ixyz1(1:dim) = (1-prd(1:dim))*ixyz1(1:dim) + prd(1:dim)*MOD(ixyz1(1:dim)+9*nxyz(1:dim),nxyz(1:dim))
#ifdef MULTIPROC
                         ! u from other processors are in the database, local processor u - in u(nwlt)
                         CALL set_filter_window_values (mn_var, mx_var, ixyz1(1:dim), j_lev, mask_loc(ii), u_tmp(mn_var:mx_var) )
                         u_loc(ii,mn_var:mx_var) = u_tmp(mn_var:mx_var)
#else
                         CALL get_indices_by_coordinate (1, ixyz1(:), j_lev, i1(1), 0)
                         mask_loc(ii) = LOGICAL( i1(1) > 0 )
                         u_loc(ii,mn_var:mx_var)    = u_local(MAX(ONE,i1(1)), mn_var:mx_var) ! if i1 = 0, assign u(1) value, but do not use it in sum
#endif
                         
                         wf_loc(ii)   = wf(kk)
                      END DO
                      DO ii = mn_var, mx_var
                         fuh(i,ii) = SUM( u_loc(1:i_p(dim),ii) * wf_loc(1:i_p(dim)), DIM = 1, MASK = mask_loc(1:i_p(dim)) ) / &
                              SUM( wf_loc(1:i_p(dim)), DIM = 1, MASK = mask_loc(1:i_p(dim)) )
                      END DO
                   END DO
                END DO
             END DO
          END DO
       END DO
    END IF
    
!!$    PRINT *, 'mn_var, mx_var=', mn_var,mx_var
!!$    DO i = 1,ie
!!$       umm(1) = MINVAL(fuh(:,i)); umm(2) = MAXVAL(fuh(:,i))
!!$       CALL parallel_global_sum (REALMINVAL=umm(1))
!!$       CALL parallel_global_sum (REALMAXVAL=umm(2))
!!$       IF (par_rank.EQ.0) THEN
!!$          WRITE(*,'( "     minmax fuh(:,",i2.2,") ", 2(G20.12,1X) )') &
!!$               i, umm
!!$       END IF
!!$    END DO

    u_local(:,mn_var:mx_var) = fuh(:,mn_var:mx_var)

    ixyz = 0

  END SUBROUTINE local_lowpass_filt


  ! wavelet low-pass filter: all wavelet coefficients at levels j>j_lowpass are set to zero
  SUBROUTINE wlt_lowpass_filt(u,ie,j_lowpass)
    USE wlt_trns_vars      ! indx_DB
    IMPLICIT NONE
    INTEGER,    INTENT (IN) :: ie, j_lowpass
    REAL (pr), DIMENSION (1:nwlt,1:ie) , INTENT (INOUT) :: u

    INTEGER :: j, j_df, wlt_type, face_type, k

    IF (verb_level.GT.0) THEN
       PRINT *,'global_lowpass_filt(), nwlt = ',  nwlt
       PRINT *,'global_lowpass_filt(), ie = ',  ie
    END IF

    CALL c_wlt_trns (u, ie, 1, ie, HIGH_ORDER, WLT_TRNS_FWD)

    DO j = j_lowpass+1, j_lev
       DO wlt_type = MIN(j-1,1),2**dim-1
          DO face_type = 0, 3**dim - 1
             DO j_df = j, j_lev
                DO k = 1, indx_DB(j_df,wlt_type,face_type,j)%length  
                   u(indx_DB(j_df,wlt_type,face_type,j)%p(k)%i,1:ie) = 0.0_pr
                END DO
             END DO
          END DO
       END DO
    END DO

    CALL c_wlt_trns (u,  ie, 1, ie, HIGH_ORDER, WLT_TRNS_INV)

  END SUBROUTINE wlt_lowpass_filt

END MODULE wavelet_filters_mod


!------------------------------!
! set some integers to 64 bits !
#ifdef SET_SOME_TO_64
#undef INTEGER_V
#undef SET_SOME_TO_64
#endif
!------------------------------!


!-------------------------------------------------------!
! define whether to use C or Fortran style node pointer !
#ifdef TREE_NODE_POINTER_STYLE_C
#undef TREE_NODE_POINTER_STYLE_C
#endif
#ifdef TREE_NODE_POINTER_STYLE_F
#undef TREE_NODE_POINTER_STYLE_F
#endif
#ifdef DECLARE_NODE_POINTER
#undef DECLARE_NODE_POINTER
#endif
! define whether to use C or Fortran style node pointer !
!-------------------------------------------------------!
