 !
 ! THis is the poisson test from wlt_3d_main.f90
 SUBROUTINE  poisson_solver()

  !  PAUSE
  !---------------------  Test for Poisson Solver
  IF(ifn == -2) THEN 

     CALL IC (u(1:nwlt,1:n_var), nwlt, n_var, t) !set initial conditions
     CALL elliptic_rhs (f(1:nwlt,1:n_integrated), nwlt, n_integrated, t) !
     CALL BC (f(1:nwlt,1:n_integrated), nwlt, n_integrated, t) !set boundary conditions
!!$     IF( n_var_exact > 0 ) CALL Exact_Soln (u_ex(1:nwlt,1:n_var_exact), nwlt, n_var_exact, t)
     u=0.0_pr 
     CALL CPU_TIME (t1)


     !--Set weights for area/volume associated with each wlt collocation point
     IF(ALLOCATED(dA)) DEALLOCATE (dA)
     ALLOCATE (dA(1:nwlt))
     CALL weights

     CALL Linsolve (u(1:nwlt,1:n_integrated), f(1:nwlt,1:n_integrated), tol, nwlt, n_integrated, Lu, Lu_diag)
     CALL CPU_TIME (t2)
     WRITE (6,'("CPU TIME (USING CPU_TIME) = ", es12.5)') t2 - t1

     !Check Exact Solution if it exists
     CALL Check_Exact_Soln(u,  u_ex,  t, scl,n_var_exact_soln_index(:,1))

     dl=0.01_pr; dl1=0.01_pr

     !
     ! Preturb solution slightly and rerun poisson solver a second time
     !
     PRINT *, ''
     PRINT *, '******************************************************************'
     PRINT *, '*Preturb solution slightly and rerun poisson solver a second time*'
     PRINT *, '******************************************************************'
     PRINT *, ''

     x0=x0+dl*COS(theta); y0=y0+dl*SIN(theta); x1=x1+dl1*COS(theta1); y1=y1+dl1*SIN(theta1)

     CALL elliptic_rhs (f(1:nwlt,1:n_integrated), nwlt, n_integrated, t)
     CALL BC (f(1:nwlt,1:n_integrated), nwlt, n_integrated, t)
!!$     IF( n_var_exact > 0 ) CALL Exact_Soln (u_ex(1:nwlt,1:n_var_exact), nwlt, n_var_exact, t)

     CALL CPU_TIME (t1)

     !--Set weights for area/volume associated with each wlt collocation point
     IF(ALLOCATED(dA)) DEALLOCATE (dA)
     ALLOCATE (dA(1:nwlt))
     CALL weights

     CALL Linsolve (u(1:nwlt,1:n_integrated), f(1:nwlt,1:n_integrated), &
          tol, nwlt, n_integrated, Lu, Lu_diag)

     CALL CPU_TIME (t2)
     WRITE (6,'("CPU TIME (USING CPU_TIME) = ", es12.5)') t2 - t1

     !Check Exact Solution if it exists
     CALL Check_Exact_Soln(u,  u_ex,  t, scl,n_var_exact_soln_index(:,1))

     PRINT *,'End of Poisson Solver'
     STOP
  END IF
  !--------------------------------------------------------
