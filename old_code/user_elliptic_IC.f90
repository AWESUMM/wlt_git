!OLEG: this should be moved to case
!
! Lu and Lu_diag define the elliptic equation that are solved 
! to get the initial condition for I_type = == 1:
! "Initial conditions from a set of algebraic equations".
! This is also called for the case called "testing poisson equation"
!
MODULE user_elliptic_IC_mod
  USE wlt_vars
  USE wlt_trns_mod

  IMPLICIT NONE
CONTAINS


FUNCTION Lu (jlev, u, nlocal, ne, order)   
  USE precision
  USE sizes
  USE pde
  IMPLICIT NONE
  INTEGER, INTENT (IN) :: jlev, nlocal, ne, order
  INTEGER :: i, ii, ie, shift
  INTEGER :: meth_central, meth_backward, meth_forward 
  REAL (pr), DIMENSION (nlocal*ne), INTENT (INOUT) :: u
  REAL (pr), DIMENSION (nlocal*ne) :: Lu
  REAL (pr), DIMENSION (nlocal,dim) :: du, d2u !, dum
  INTEGER, PARAMETER :: internal = 1, normal = 0
  INTEGER :: face_type, nloc
  INTEGER, DIMENSION(0:dim) :: i_p_face
  INTEGER, DIMENSION(dim) :: face
  INTEGER, DIMENSION(nwlt) :: iloc
  

  meth_central  = order + BIASING_NONE
  meth_backward = order + BIASING_BACKWARD
  meth_forward  = order + BIASING_FORWARD

  DO ie=1,ne
     shift=(ie-1)*nlocal

!!$     CALL wlt_projection(u((ie-1)*nlocal+1:ie*nlocal), jlev, jlev, nlocal, normal)

     !************* Calculate derivatives *******
  
     !--- Internal points 

!--- div(grad) 
!!$     CALL c_diff_fast (u(shift+1:shift+nlocal), du, d2u, jlev, nlocal, meth_backward, 10)
!!$     CALL c_diff_fast (du(1:nlocal,1), d2u, dum, jlev, nlocal, meth_forward, 10)
!!$     Lu(shift+1:shift+Nwlt_lev(jlev,0))=d2u(1:Nwlt_lev(jlev,0),1) 
!!$     CALL c_diff_fast (du(1:nlocal,2), d2u, dum, jlev, nlocal, meth_forward, 10)
!!$     Lu(shift+1:shift+Nwlt_lev(jlev,0))=Lu(shift+1:shift+Nwlt_lev(jlev,0)) + d2u(1:Nwlt_lev(jlev,0),2)
!--- Laplacian

!NOTE this looks like the last arg must be changed to -01 to get the backward_bias then forward_bias version
! of the second derivative
!
     CALL c_diff_fast (u(shift+1:shift+nlocal), du, d2u, jlev, nlocal, order, 01, 1, 1, 1)
     Lu(shift+1:shift+Nwlt_lev(jlev,0))= d2u(1:Nwlt_lev(jlev,0),1) + d2u(1:Nwlt_lev(jlev,0),2)
     IF (dim==3) THEN
        Lu(shift+1:shift+Nwlt_lev(jlev,0))= Lu(shift+1:shift+Nwlt_lev(jlev,0)) + d2u(1:Nwlt_lev(jlev,0),3)
     END IF
!----
     !--Boundary points
     i_p_face(0) = 1
     DO i=1,dim
        i_p_face(i) = i_p_face(i-1)*3
     END DO
     DO face_type = 0, 3**dim - 1
        face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
        IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
           CALL get_all_indices_by_face (face_type, jlev, nloc, iloc)
           IF(nloc > 0 .AND. dim == 3) THEN ! 3-D case only
              IF( ALL( face == (/-1,0,0/) ) ) THEN                 ! Xmin face, no edges or corners
                 Lu(shift+iloc(1:nloc)) = u(shift+iloc(1:nloc))    !Dirichlet conditions
              ELSE IF( ALL( face == (/1,0,0/) ) ) THEN             ! Xmax face, no edges or corners
                 Lu(shift+iloc(1:nloc)) = u(shift+iloc(1:nloc))    !Dirichlet conditions
!!$                Lu(shift+iloc(1:nloc)) = du(ie,iloc(1:nloc),1)  !Neuman conditions
              ELSE IF( ALL( face(2:dim) == (/-1,0/) ) ) THEN       ! Ymin face, edges || to Z, no corners
                 Lu(shift+iloc(1:nloc)) = u(shift+iloc(1:nloc))    !Dirichlet conditions
!!$                Lu(shift+iloc(1:nloc)) = du(ie,iloc(1:nloc),2)  !Neuman conditions
              ELSE IF( ALL( face(2:dim) == (/1,0/) ) ) THEN        ! Ymax face, edges || to Z, no corners
                 Lu(shift+iloc(1:nloc)) = u(shift+iloc(1:nloc))    !Dirichlet conditions
!!$                Lu(shift+iloc(1:nloc)) = du(ie,iloc(1:nloc),2)  !Neuman conditions
              ELSE IF( face(3) == -1 ) THEN                        ! entire Zmin face
                 Lu(shift+iloc(1:nloc)) = u(shift+iloc(1:nloc))    !Dirichlet conditions
!!$                Lu(shift+iloc(1:nloc)) = du(ie,iloc(1:nloc),3)  !Neuman conditions
              ELSE IF( face(3) == 1 ) THEN                         ! entire Zmax face
                 Lu(shift+iloc(1:nloc)) = u(shift+iloc(1:nloc))    !Dirichlet conditions
!!$                Lu(shift+iloc(1:nloc)) = du(ie,iloc(1:nloc),3)  !Neuman conditions
              END IF
           END IF
        END IF
     END DO
  END DO
END FUNCTION Lu

FUNCTION Lu_diag (jlev, u, nlocal, ne, order)
  USE precision
  USE sizes
  USE pde
  IMPLICIT NONE
  INTEGER, INTENT (IN) :: jlev, nlocal, ne, order
  INTEGER :: meth_central, meth_backward, meth_forward 
  INTEGER :: i, ii, ie, shift
  REAL (pr), DIMENSION (nlocal*ne), INTENT (INOUT) :: u
  REAL (pr), DIMENSION (nlocal*ne) :: Lu_diag
  REAL (pr), DIMENSION (nlocal,dim) :: du, d2u
  INTEGER :: face_type, nloc
  INTEGER, DIMENSION(0:dim) :: i_p_face
  INTEGER, DIMENSION(dim) :: face
  INTEGER, DIMENSION(nwlt) :: iloc

  meth_central  = order + BIASING_NONE
  meth_backward = order + BIASING_BACWARD
  meth_forward  = order + BIASING_FORWARD
 

  DO ie=1,ne
     shift=(ie-1)*nlocal

!!$     IF(SUM(nbnd) > 0 .AND. BCtype == 1) THEN !--- Neuman
!!$        CALL c_diff_diag_bnd(u(shift+1:shift+nlocal), du, d2u, jlev, nlocal, order)
!!$        Lu_diag(shift+1:shift+Nwlt_lev(jlev,0))=du(1:Nwlt_lev(jlev,0),1) + du(1:Nwlt_lev(jlev,0),2)
!!$     ELSE
!!$        Lu_diag(shift+1:shift+Nwlt_lev(jlev,0)) = 0.0_pr
!!$     END IF

     !************* Calculate derivatives *******
!--- div(grad) 
!!$     CALL c_diff_diag (u(shift+1:shift+nlocal), du, d2u, jlev, nlocal, meth_backward, meth_forward, -11)
!!$     Lu_diag(shift+1:shift+Nwlt_lev(jlev,0))=d2u(1:Nwlt_lev(jlev,0),1) + d2u(1:Nwlt_lev(jlev,0),2)
!--- Laplacian
PRINT *,'CAlling c_diff_diag from Lu_diag()'
     CALL c_diff_diag (du, d2u, jlev, nlocal, order, order, 01)
     Lu_diag(shift+1:shift+Nwlt_lev(jlev,0))=d2u(1:Nwlt_lev(jlev,0),1) + d2u(1:Nwlt_lev(jlev,0),2)
     IF( dim == 3) THEN
        Lu_diag(shift+1:shift+Nwlt_lev(jlev,0))= &
             Lu_diag(shift+1:shift+Nwlt_lev(jlev,0)) + d2u(1:Nwlt_lev(jlev,0),3)
     END IF
!---
     !--Boundary points
     i_p_face(0) = 1
     DO i=1,dim
        i_p_face(i) = i_p_face(i-1)*3
     END DO
     DO face_type = 0, 3**dim - 1
        face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
        IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
           CALL get_all_indices_by_face (face_type, jlev, nloc, iloc)
           IF(nloc > 0 .AND. dim == 3) THEN ! 3-D case only
              IF( ALL( face == (/-1,0,0/) ) ) THEN                    ! Xmin face, no edges or corners
                 Lu_diag(shift+iloc(1:nloc)) = 1.0_pr                 !Dirichlet conditions
              ELSE IF( ALL( face == (/1,0,0/) ) ) THEN                ! Xmax face, no edges or corners
                 Lu_diag(shift+iloc(1:nloc)) = 1.0_pr                 !Dirichlet conditions
!!$                Lu_diag(shift+iloc(1:nloc)) = du(iloc(1:nloc),1)   !Neuman conditions
              ELSE IF( ALL( face(2:dim) == (/-1,0/) ) ) THEN          ! Ymin face, edges || to Z, no corners
                 Lu_diag(shift+iloc(1:nloc)) = 1.0_pr                 !Dirichlet conditions
!!$                Lu_diag(shift+iloc(1:nloc)) = du(iloc(1:nloc),2)   !Neuman conditions
              ELSE IF( ALL( face(2:dim) == (/1,0/) ) ) THEN           ! Ymax face, edges || to Z, no corners
                 Lu_diag(shift+iloc(1:nloc)) = 1.0_pr                 !Dirichlet conditions
!!$                Lu_diag(shift+iloc(1:nloc)) = du(iloc(1:nloc),2)   !Neuman conditions
              ELSE IF( face(3) == -1 ) THEN                           ! entire Zmin face
                 Lu_diag(shift+iloc(1:nloc)) = 1.0_pr                 !Dirichlet conditions
!!$                Lu_diag(shift+iloc(1:nloc)) = du(iloc(1:nloc),3)   !Neuman conditions
              ELSE IF( face(3) == 1 ) THEN                            ! entire Zmax face
                 Lu_diag(shift+iloc(1:nloc)) = 1.0_pr                 !Dirichlet conditions
!!$                Lu_diag(shift+iloc(1:nloc)) = du(iloc(1:nloc),3)   !Neuman conditions
              END IF
           END IF
        END IF
     END DO
  END DO


END FUNCTION Lu_diag 

END MODULE user_elliptic_IC_mod
