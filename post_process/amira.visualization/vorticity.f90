module vorticity_mod
! This module containes routines to tranform 
! velocity into vorticity and back again
!
! The  setup_fgrid() from fft_module must be called before any routines in this
! module


  use params
  use fft_module
  
contains

!
!
! calculate the vorticity from the velocity
! Args:
!
!u,v,w     velocity components IN FOURIER SPACE !!
!Wu,Wv,Ww     verticity components   RETURNED IN FOURIER SPACE !!
subroutine veloc_2_vorticity(u,v,w,Wu,Wv,Ww)
implicit none
real*8 u(n1pp,n2p,n3p) ,v(n1pp,n2p,n3p),w(n1pp,n2p,n3p)
real*8 Wu(n1pp,n2p,n3p),Wv(n1pp,n2p,n3p),Ww(n1pp,n2p,n3p)
integer i,j,k

call curl(u,v,w,Wu,Wv,Ww)

end subroutine veloc_2_vorticity


!
! does curl u_i
!
!args in Fourier Space
! u_in
! v_in
! w_in
! u_result
! v_result
! w_result
subroutine curl(u_in,v_in,w_in,u_result,v_result,w_result)
implicit none
real*8 u_in(n1pp,n2p,n3p) ,v_in(n1pp,n2p,n3p),w_in(n1pp,n2p,n3p)
real*8 u_result(n1pp,n2p,n3p),v_result(n1pp,n2p,n3p),w_result(n1pp,n2p,n3p)
integer i,j,k


!
!        -------------------------
!        
!        calculate curl u
!
!        -------------------------
! 
! 
!
!$OMP PARALLEL DO DEFAULT(SHARED) PRIVATE(i,j,k)
         do k = 1, n3
            do j = 1, n2
               do i = 1, n1pp, 2
                  u_result(i,j,k) = fac3(k) * v_in(i+1,j,k) &
                      - fac2(j) * w_in(i+1,j,k)
                  u_result(i+1,j,k) = -fac3(k) * v_in(i,j,k) &
                      + fac2(j) * w_in(i,j,k)
                  v_result(i,j,k) = fac1(i) * w_in(i+1,j,k) &
                      - fac3(k) * u_in(i+1,j,k)
                  v_result(i+1,j,k) = -fac1(i) * w_in(i,j,k) &
                      + fac3(k) * u_in(i,j,k)
                  w_result(i,j,k) = fac2(j) * u_in(i+1,j,k) &
                      - fac1(i) * v_in(i+1,j,k)
                  w_result(i+1,j,k) = -fac2(j) * u_in(i,j,k) &
                      + fac1(i) * v_in(i,j,k)
               enddo
            enddo
         enddo
!$OMP END PARALLEL DO
end subroutine curl


!
! Find velocity from vorticity
! all fields must be in fourier space  and the result
! is returned in fourier space
!
! Wu - x component of vorticity in  fourier space - input
! Wv - y component of vorticity in  fourier space - input
! Ww - z component of vorticity in  fourier space - input
! 
! u - x component of  velocity in  fourier space - result
! v - y component of  velocity in  fourier space - result
! w - z component of  velocity in  fourier space - result
! tmp123 are tmp variables
subroutine vorticity_2_veloc(Wu,Wv,Ww,u,v,w,tmp1,tmp2,tmp3)
implicit none
real*8 u(n1pp,n2p,n3p) ,v(n1pp,n2p,n3p),w(n1pp,n2p,n3p)
real*8 Wu(n1pp,n2p,n3p),Wv(n1pp,n2p,n3p),Ww(n1pp,n2p,n3p)
real*8 tmp1(n1pp,n2p,n3p),tmp2(n1pp,n2p,n3p),tmp3(n1pp,n2p,n3p)
integer i,j,k

real*8 tmp, ksq


!
!        -------------------------
!        
!        calculate velocity ( u v w ) from vorticity
!         w_i = 
!
!        -------------------------
! 
! 
!
! first find curl( W )

	call curl(Wu,Wv,Ww,tmp1,tmp2,tmp3)
! now Wu Wv Ww is the curl of W in Fourier space

!$OMP PARALLEL DO DEFAULT(SHARED) PRIVATE(i,j,k,ksq,tmp)
         do k = 1, n3
            do j = 1, n2
		tmp = (fsq3(k) + fsq2(j) )
               do i = 1, n1pp
		  ksq = ( fsq1(i) + tmp )
                  ! do special case where ksq = 0 (or near zero)
                  if( ( i .eq. 1 .or. i .eq. 2) .and. j .eq.1 .and. k .eq. 1 ) then
                      u(i,j,k) = 0.D0
                      v(i,j,k) = 0.D0
                      w(i,j,k) = 0.D0
                  else
                     u(i,j,k) = tmp1(i,j,k) / ksq
                     v(i,j,k) = tmp2(i,j,k) / ksq
                     w(i,j,k) = tmp3(i,j,k) / ksq
                  end if
               enddo
            enddo
         enddo
!$OMP END PARALLEL DO
end subroutine vorticity_2_veloc



!
!
! calculate the vorticity from the velocity
! same as  veloc_2_vorticity() but this version does calculations in place
! Args:
!
!u,v,w     velocity components IN FOURIER SPACE !!
!
subroutine veloc_2_vorticity_ip(u,v,w)
implicit none
real*8 u(n1pp,n2p,n3p) ,v(n1pp,n2p,n3p),w(n1pp,n2p,n3p)
integer i,j,k

call curl_ip(u,v,w)

end subroutine veloc_2_vorticity_ip


!
! does curl u_i
!! same as  curl() but this version does calculations in place
!args in Fourier Space
! u_in
! v_in
! w_in
! u_result
! v_result
! w_result
subroutine curl_ip(u,v,w)
implicit none
real*8 u(n1pp,n2p,n3p) ,v(n1pp,n2p,n3p),w(n1pp,n2p,n3p)
real*8 ur,ui,vr,vi,wr,wi
integer i,j,k


!
!        -------------------------
!        
!        calculate curl u
!
!        -------------------------
! 
! 
!
         do k = 1, n3
            do j = 1, n2
               do i = 1, n1pp, 2
                  ur= u(i,j,k); ui= u(i+1,j,k) 
                  vr= v(i,j,k); vi= v(i+1,j,k) 
                  wr= w(i,j,k); wi= w(i+1,j,k) 
                  u(i,j,k)   =  fac3(k) * vi - fac2(j) * wi
                  u(i+1,j,k) = -fac3(k) * vr + fac2(j) * wr
                  v(i,j,k)   =  fac1(i) * wi - fac3(k) * ui
                  v(i+1,j,k) = -fac1(i) * wr + fac3(k) * ur
                  w(i,j,k)   =  fac2(j) * ui - fac1(i) * vi
                  w(i+1,j,k) = -fac2(j) * ur + fac1(i) * vr
               enddo
            enddo
         enddo

end subroutine curl_ip


!
! Find velocity from vorticity
! all fields must be in fourier space  and the result
! is returned in fourier space
!same as  vorticity_2_veloc() but this version does calculations in place

! Wu - x component of vorticity in  fourier space - input
! Wv - y component of vorticity in  fourier space - input
! Ww - z component of vorticity in  fourier space - input
! 
! u - x component of  velocity in  fourier space - result
! v - y component of  velocity in  fourier space - result
! w - z component of  velocity in  fourier space - result
! tmp123 are tmp variables
subroutine vorticity_2_veloc_ip(u,v,w)
implicit none
real*8 u(n1pp,n2p,n3p) ,v(n1pp,n2p,n3p),w(n1pp,n2p,n3p)
integer i,j,k

real*8 tmp, ksq


!
!        -------------------------
!        
!        calculate velocity ( u v w ) from vorticity
!         w_i = 
!
!        -------------------------
! 
! 
!
! first find curl( W )

	call curl_ip(u,v,w)

! now Wu Wv Ww is the curl of W in Fourier space

         do k = 1, n3
            do j = 1, n2
		tmp = (fsq3(k) + fsq2(j) )
               do i = 1, n1pp
		  ksq = ( fsq1(i) + tmp )
                  ! do special case where ksq = 0 (or near zero)
                  if( ( i .eq. 1 .or. i .eq. 2) .and. j .eq.1 .and. k .eq. 1 ) then
                      u(i,j,k) = 0.D0
                      v(i,j,k) = 0.D0
                      w(i,j,k) = 0.D0
                  else
                     u(i,j,k) = u(i,j,k) / ksq
                     v(i,j,k) = v(i,j,k) / ksq
                     w(i,j,k) = w(i,j,k) / ksq
                  end if
               enddo
            enddo
         enddo

end subroutine vorticity_2_veloc_ip


! ******************************************
! Now versions of the vorticity to velocity and back 
! done in place using field variables
! ******************************************


!
!
! calculate the vorticity from the velocity
! same as  veloc_2_vorticity() but this version does calculations in place
! Args:
!
!uu     velocity components IN FOURIER SPACE !!
!
subroutine veloc_2_vorticity_field_ip(uu )
implicit none
real*8 uu(n1pp,3,n2p,n3p) 
integer i,j,k

call curl_field_ip(uu)

end subroutine veloc_2_vorticity_field_ip


!
! does curl u_i
!! same as  curl() but this version does calculations in place
!args in Fourier Space
! uu 
!
subroutine curl_field_ip(uu)
implicit none
real*8 uu(n1pp,3,n2p,n3p)
real*8 ur,ui,vr,vi,wr,wi
integer i,j,k


!
!        -------------------------
!        
!        calculate curl u
!
!        -------------------------
! 
! 
!
         do k = 1, n3
            do j = 1, n2
               do i = 1, n1pp, 2
                  ur= uu(i,1,j,k); ui= uu(i+1,1,j,k) 
                  vr= uu(i,2,j,k); vi= uu(i+1,2,j,k) 
                  wr= uu(i,3,j,k); wi= uu(i+1,3,j,k) 
                  uu(i,1,j,k)   =  fac3(k) * vi - fac2(j) * wi
                  uu(i+1,1,j,k) = -fac3(k) * vr + fac2(j) * wr
                  uu(i,2,j,k)   =  fac1(i) * wi - fac3(k) * ui
                  uu(i+1,2,j,k) = -fac1(i) * wr + fac3(k) * ur
                  uu(i,3,j,k)   =  fac2(j) * ui - fac1(i) * vi
                  uu(i+1,3,j,k) = -fac2(j) * ur + fac1(i) * vr
               enddo
            enddo
         enddo

end subroutine curl_field_ip


!
! Find velocity from vorticity
! all fields must be in fourier space  and the result
! is returned in fourier space
!same as  vorticity_2_veloc() but this version does calculations in place

! uu - uvw components of vorticity in  fourier space on input
! and uvw components of velocity on output
!
subroutine vorticity_2_veloc_field_ip(uu )
implicit none
real*8 uu(n1pp,3,n2p,n3p) 
integer i,j,k

real*8 tmp, ksq


!
!        -------------------------
!        
!        calculate velocity ( u v w ) from vorticity
!         w_i = 
!
!        -------------------------
! 
! 
!
! first find curl( W )

	call curl_field_ip(uu)

! now Wu Wv Ww is the curl of W in Fourier space

         do k = 1, n3
            do j = 1, n2
		tmp = (fsq3(k) + fsq2(j) )
               do i = 1, n1pp
		  ksq = ( fsq1(i) + tmp )
                  ! do special case where ksq = 0 (or near zero)
                  if( ( i .eq. 1 .or. i .eq. 2) .and. j .eq.1 .and. k .eq. 1 ) then
                      uu(i,1,j,k) = 0.D0
                      uu(i,2,j,k) = 0.D0
                      uu(i,3,j,k) = 0.D0
                  else
                     uu(i,1,j,k) = uu(i,1,j,k) / ksq
                     uu(i,2,j,k) = uu(i,2,j,k) / ksq
                     uu(i,3,j,k) = uu(i,3,j,k) / ksq
                  end if
               enddo
            enddo
         enddo

end subroutine vorticity_2_veloc_field_ip

end module vorticity_mod 
