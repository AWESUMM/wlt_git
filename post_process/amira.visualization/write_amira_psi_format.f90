module write_amira_psi
  USE precision
  USE share_consts
  USE pde
  USE sizes
  USE wlt_trns_util_mod
contains
  subroutine write_amira_psi_format( c,  filename, j_out , j_lev)
    use precision
    implicit none 
    REAL (pr) ::  c(1:nwlt,1:dim)
    character (LEN=*) filename
    INTEGER :: j_out,j_lev
    integer   i,j,k,total
    integer Lev, LevX,LevY,LevZ

    integer ,parameter :: output_unit = 200



    open(UNIT=output_unit, FILE=filename,FORM='FORMATTED')

    write (output_unit,'(A)') '# PSI Format 1.0'
    write (output_unit,'(A)') '# '
    write (output_unit,'(A)') '# column[0] = "x" ' 
    write (output_unit,'(A)') '# column[1] = "y" '
    write (output_unit,'(A)') '# column[2] = "z" '
    write (output_unit,'(A)') '# column[3] = "Magnitude" ' 
    write (output_unit,'(A)') '# column[4] = "Level" ' 
    write (output_unit,'(A)') '# column[5] = "LevelX" '
    write (output_unit,'(A)') '# column[6] = "LevelY"'
    write (output_unit,'(A)') '# column[7] = "LevelZ" '
    write (output_unit,'(A)') '#'
    write (output_unit,'(A)') '# symbol[3] = "m" '
    write (output_unit,'(A)') '# symbol[4] = "l" '
    write (output_unit,'(A)') '# symbol[5] = "lx" '
    write (output_unit,'(A)') '# symbol[6] = "ly" '
    write (output_unit,'(A)') '# symbol[7] = "lz" '
    write (output_unit,'(A)') '#'
    write (output_unit,'(A)') '# type[3] = float'
    write (output_unit,'(A)') '# type[4] = byte'
    write (output_unit,'(A)') '# type[5] = byte'
    write (output_unit,'(A)') '# type[6] = byte'
    write (output_unit,'(A)') '# type[7] = byte'
    write (output_unit,'(A)') ''
    write (output_unit,'(I10.10,1x,I10.10,1x,I10.10)') nwlt,nwlt,nwlt
    write (output_unit,'(A)') '1.00 0.00 0.00'
    write (output_unit,'(A)') '0.00 1.00 0.00'
    write (output_unit,'(A)') '0.00 0.00 1.00'
    write (output_unit,'(A)') ''

    ! The wavelet level is defined as the maximum of the three (xyz) wavelet levels
    ! associated with a point.

    !WRITE_DEBUG 'Entering make_n_level_mask() to make mask of n levels'
    !     if( .not. do_intersection ) one_level_mask = 0.0_pr


    do i=1,nwlt
       LevX = ij2j(indx(i,1),j_lev)
       LevY = ij2j(indx(i,2),j_lev)
       LevZ = ij2j(indx(i,3),j_lev)
       Lev = max( LevX,max(LevY,LevZ) )
             write (output_unit,'(I5.5,1x,I5.5,1x,I5.5,F15.5,1x,I2.2,1x,I2.2,1x,I2.2,1x,I2.2)') &
                  indx(i,1)*(2**(j_out-j_lev)),indx(i,2)*(2**(j_out-j_lev)),indx(i,3)*(2**(j_out-j_lev)),&
				  MAXVAL(ABS(c(i,1:dim))), Lev, LevX, LevY,LevZ
       
    enddo



   close(UNIT=output_unit )
  end subroutine write_amira_psi_format

end module write_amira_psi
