
PROGRAM post_process_viz
  USE precision
  USE share_consts
  USE pde
  USE sizes
  USE FFT_MODULE
  USE fft_util

  USE write_amira_psi
  USE write_amira_am
  USE io_3D
  USE wlt_trns_mod
  USE wlt_trns_util_mod
  USE vector_util_mod
  USE field
  IMPLICIT NONE

  !--Wavelet transform variables
  REAL (pr), DIMENSION (:,:,:,:), ALLOCATABLE, SAVE :: u_out
!  REAL (pr), SAVE,DIMENSION (:,:), POINTER :: c
  REAL (pr), DIMENSION (:), POINTER , SAVE :: scl
  REAL (pr), SAVE ::  eps_post, eps_sim, dx, dy
  INTEGER   ::  j_out!,frm 
  INTEGER   ::  nxyz_out(1:3) !, nxyz(1:3)
  INTEGER   :: i, j, k,  ie ,llc
  REAL (pr), DIMENSION (:,:), ALLOCATABLE, SAVE ::  xx_out
  INTEGER :: ix,jx,ix_l,ix_h,ixp,ixpm,ix_even,ix_odd,iz
  INTEGER :: iy,jy,iy_l,iy_h,iyp,iypm,iy_even,iy_odd
!  CHARACTER (LEN=64) :: file_gen, file_cwlt
  CHARACTER (LEN=4)    :: name
  CHARACTER (LEN=10)    :: epsname  
  CHARACTER (LEN=256)  :: outputfile
  INTEGER   :: idim!, dim
  LOGICAL :: make_amira_fmt, make_amira_psi_fmt
  INTEGER   :: begin_station_num, end_station_num
  INTEGER   :: station_num !station # in saved solution file name
  !ie. test_iso_0001.res is station_num = 1
  INTEGER :: n_var_saved ! number of vars saved
  INTEGER :: saved_vort  ! 1 if we saved vorticity in this file
  INTEGER , DIMENSION(6)::  ibounds
  REAL (pr), DIMENSION (6) :: bounds
  REAL(pr)       :: pi2
  REAL(pr)       :: the_tke,the_tdiss
  REAL(pr)       :: viscosity




  ! READ ARGUMENTS
  !----------- inputs
  OPEN(1,FILE='inp.dat',STATUS='OLD')
  READ (1,'(A64)')    file_gen
  READ (1,*)   begin_station_num
  READ (1,*)   end_station_num
  READ (1,*)    eps_post
  READ (1,*)    eps_sim
  READ (1,*)    viscosity
  READ (1,*)    j_out
  READ (1,'(L1)') make_amira_fmt
  READ (1,'(L1)') make_amira_psi_fmt
  CLOSE(1)

  PRINT *, 'file_gen: ',file_gen
  PRINT *, 'eps_sim',eps_sim 
  PRINT *, 'eps_post',eps_post 
  PRINT *, 'j_out ', j_out

  IF( .NOT. make_amira_fmt .AND. .NOT. make_amira_psi_fmt ) THEN
     PRINT *, 'nothing to do.. Exiting..'
     STOP
  END IF
  !
  !-- First open the first data file and find out if it is 3D or 2D
  CALL read_solution_dim(  file_gen//' ', begin_station_num)
PRINT *,'after  read_solution_dim , dim = ', dim
  !
  ! -- setup u_variable_names array to tell read_solution which variables to read
  !
  ALLOCATE (u_variable_names(1:3) )
  u_variable_names(:) = ' '
  
  ! make format for reading u_variable_names_fmt
  WRITE(u_variable_names_fmt,  '( ''( A'', I3.3 , '')'' )') u_variable_name_len

  ! In integrated variables (3D)
  WRITE (u_variable_names(1), u_variable_names_fmt) 'Velocity_u_@t  '
  WRITE (u_variable_names(2), u_variable_names_fmt) 'Velocity_v_@t  '
  IF( dim == 3 ) WRITE (u_variable_names(3), u_variable_names_fmt) 'Velocity_w_@t  '

  !
  !-- Loop through each data file 
  !
  DO station_num = begin_station_num, end_station_num
     PRINT *, 'Process station_num ',station_num
     WRITE (name, '(i4.4)') station_num
    !--------------------

     n_var = 10
	 !
	 ! Read the solution file. The wlt coeff's are read into the global u(1:nwlt,1:n_var)
     CALL read_solution( scl , station_num , .FALSE. )
     ! set j_out (the max level in the inverse transform) to j_lev 
     ! (the max level of the simulation) if the j_out given is < 0
     ! else use j_out from input file   
     IF( j_out < 0 ) j_out = j_lev

     nxyz_out(1)=mxyz(1)*2**(j_out-1)
     nxyz_out(2)=mxyz(2)*2**(j_out-1)
     IF( dim == 3 ) THEN
        nxyz_out(3)=mxyz(3)*2**(j_out-1)
     ELSE
        nxyz_out(3) = 0
     END IF

     ALLOCATE( xx_out(0:MAXVAL(nxyz_out),1:dim) )
     IF( make_amira_fmt ) THEN

	ALLOCATE( u_out(0:nxyz_out(1)+1,0:nxyz_out(2),0:nxyz_out(3),3) )     

     	!
     	!-- set the real space coordinates for the interpolated field
    	 !
     	DO idim = 1,dim
            DO i=0,nxyz_out(idim)
           	xx_out(i,idim)=xx(i*2**(j_lev-j_out),idim)
       	    END DO
        END DO

     	!
     	!-------- defining bounds for the regions to be plotted 
     	!
     	DO idim = 1,dim
        	ibounds(2*idim-1) = 0
        	DO WHILE (xx_out(ibounds(2*idim-1),dim) < bounds(2*idim-1) .AND. ibounds(2*idim-1) < nxyz_out(idim))
           		ibounds(2*idim-1) = ibounds(2*idim-1)+1
        	END DO
        	ibounds(2*idim) = nxyz_out(idim)
        	DO WHILE (xx_out(ibounds(2*idim),dim) > bounds(2*idim) .AND. ibounds(2*idim) > 0)
           		ibounds(2*idim) = ibounds(2*idim)-1
        	END DO
     	END DO

     END IF !doing amira output format

PRINT *,' MAXVAL(ABS(u(1:nwlt,1))) = ', MAXVAL(ABS(u(1:nwlt,1)))
PRINT *,' MAXVAL(ABS(u(1:nwlt,2))) = ', MAXVAL(ABS(u(1:nwlt,2)))
PRINT *,' MAXVAL(ABS(u(1:nwlt,3))) = ', MAXVAL(ABS(u(1:nwlt,3)))

     !
     !-- Set output file name. If the results were wavelet filtered above (eps_post > eps_sim(ulation) )
     ! this filter eps_post is added to the file name.
     !
     IF( eps_post == 0.0 ) THEN
        outputfile = file_gen(1:INDEX(file_gen,' ')-1)
     ELSE
        WRITE (epsname, '(f10.6)') eps_post
        ! WRITE leaves initial blanks if the non-decimal part is not big
        !enough. So we first use INDEX to find where the real number begins
        llc = INDEX(epsname,' ',BACK=.TRUE.)+1; 
        outputfile = TRIM(file_gen)//'filt.eps'//epsname(llc:LEN(epsname))//'_st'
     END IF

     !write psi coefficient  file for Amira
     CALL write_amira_psi_format( u(1:nwlt,1:dim), &
          outputfile(1:INDEX(outputfile,' ')-1)//name//'.maxabswltcoeff.psi',&
         j_out )
    

     !
     !-------- setting weights for wavelet transform
     !
     CALL c_wlt_trns_interp_setup(j_out, nxyz_out, maxval(nxyz_out), xx_out )


     !
     !-- Interpolate to full grid based on epsilon. If epsilon is greater then the
     ! epsilon used in the simulation then a filtered version of the results
     ! will be interpolated to the full grid.
     !
     CALL c_wlt_trns_interp (u_out(:,:,:,1), u(1:nwlt,1), nwlt, j_lev, j_out, scl(1)*eps_post)
     CALL c_wlt_trns_interp (u_out(:,:,:,2), u(1:nwlt,2), nwlt, j_lev, j_out, scl(2)*eps_post)
     CALL c_wlt_trns_interp (u_out(:,:,:,3), u(1:nwlt,3), nwlt, j_lev, j_out, scl(3)*eps_post)

     ! write  amira am field file for velocity components
     !!CALL write_amira_am_format(u_out(:,:,:,1), &
     !!      nxyz(1),nxyz(2),nxyz(3),&
     !!      outputfile(1:INDEX(outputfile,' ')-1)//name//'.veloc.u.am') 
     !!CALL write_amira_am_format(u_out(:,:,:,2), &
     !!      nxyz(1),nxyz(2),nxyz(3),&
     !!      outputfile(1:INDEX(outputfile,' ')-1)//name//'.veloc.v.am') 
     !!CALL write_amira_am_format(u_out(:,:,:,3), &
     !!      nxyz(1),nxyz(2),nxyz(3),&
     !!      outputfile(1:INDEX(outputfile,' ')-1)//name//'.veloc.w.am') 

	CALL write_field(u_out(:,:,:,1),u_out(:,:,:,2),u_out(:,:,:,3), &
           nxyz(1),nxyz(2),nxyz(3),outputfile(1:INDEX(outputfile,' ')-1)//name//'.veloc') 



     !
	 ! calculate vorticity on adapted grid
	 !
	 CALL cal_vort (u(:,n0:n0+dim-1), u(:,n_var_vorticity:n_var_vorticity+3-MOD(dim,3)-1), nwlt)

     CALL c_wlt_trns ( &
		  u(:,n_var_vorticity:n_var_vorticity+3-MOD(dim,3)-1), n_var, &
		  n_var_vorticity,n_var_vorticity+3-MOD(dim,3)-1 , 1)



PRINT *,' MAXVAL(ABS(u_out(:,:,:,1))) = ', MAXVAL(ABS(u_out(:,:,:,1)))
PRINT *,' MAXVAL(ABS(u_out(:,:,:,2))) = ', MAXVAL(ABS(u_out(:,:,:,2)))
PRINT *,' MAXVAL(ABS(u_out(:,:,:,3))) = ', MAXVAL(ABS(u_out(:,:,:,3)))

     ! initialize fft's
     ! Last arg true is to zero oddball points in fft.
     pi2 = 8.0D0 *atan(1.0D0 )
     call init_fft(nxyz(1),nxyz(2),nxyz(3),pi2,pi2,pi2, .true. )

     !
     !-- real to fourier space
     !
     call rtoft(u_out(:,:,:,1))
     call rtoft(u_out(:,:,:,2))
     call rtoft(u_out(:,:,:,3))
PRINT *,' MAXVAL(ABS(u_out(:,:,:,1))) = ', MAXVAL(ABS(u_out(:,:,:,1)))
PRINT *,' MAXVAL(ABS(u_out(:,:,:,2))) = ', MAXVAL(ABS(u_out(:,:,:,2)))
PRINT *,' MAXVAL(ABS(u_out(:,:,:,3))) = ', MAXVAL(ABS(u_out(:,:,:,3)))



 
     ! calculate  vorticity in place ( curl of u)
     CALL  curl_ip_fft(u_out(:,:,:,1),u_out(:,:,:,2),u_out(:,:,:,3))
     !
     !--  fourier to  real space
     !
     call ftort(u_out(:,:,:,1))
     call ftort(u_out(:,:,:,2))
     call ftort(u_out(:,:,:,3))


     ! write  amira am field file for velocity components
     !!CALL write_amira_am_format(u_out(:,:,:,1), &
     !!      nxyz(1),nxyz(2),nxyz(3),&
     !!      outputfile(1:INDEX(outputfile,' ')-1)//name//'.vort.u.am') 
     !!CALL write_amira_am_format(u_out(:,:,:,2), &
     ! !     nxyz(1),nxyz(2),nxyz(3),&
     !!      outputfile(1:INDEX(outputfile,' ')-1)//name//'.vort.v.am') 
     !!CALL write_amira_am_format(u_out(:,:,:,3), &
     !!      nxyz(1),nxyz(2),nxyz(3),&
      !!     outputfile(1:INDEX(outputfile,' ')-1)//name//'.vort.w.am') 

	CALL write_field(u_out(:,:,:,1),u_out(:,:,:,2),u_out(:,:,:,3), &
           nxyz(1),nxyz(2),nxyz(3),outputfile(1:INDEX(outputfile,' ')-1)//name//'.vort') 

     CALL  c_wlt_trns_interp_free() !


     DEALLOCATE (u, xx, u_out, xx_out ,lv_intrnl, lv_bnd, indx)

  END DO !loop through stations
END PROGRAM  post_process_viz



