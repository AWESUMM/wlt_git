!
! write in amira .am format 
!
module write_amira_am
  USE share_consts
  USE pde
  USE sizes
  USE wlt_trns_util_mod
!*******************FOR DEBUGGING*******************

! logical debug defined in params.f90 
!******************END FOR DEBUGGING****************

contains 
 


! 
!     ------------ 
!
!     write  data field
! 
!     ------------ 
! 
      subroutine write_field(u,v,w, n1, n2, n3,filename) 

        implicit none 

       
        integer n1, n2, n3 
        real (pr) u(n1,n2,n3) 
        real (pr) v(n1,n2,n3) 
        real (pr) w(n1,n2,n3) 

        character (*) filename
        real (pr) the_viscosity

        
        integer i, j, k
        integer IUNIT  

	    IUNIT = 200


        !write u
        !!write(*,*) 'write_field(): File opened to write output field:',filename
        !!write(*,*)  TRIM(filename)//'_raw_u'
        !!open(unit=IUNIT, FILE=TRIM(filename)//'_raw_u', FORM='unformatted')
        !!WRITE(IUNIT) (((real(u(i,j,k),4),i=1,n1),j=1,n2),k=1,n3)
        !!close(IUNIT)
	    !!write(*,*) 'End  writing output field:', TRIM(filename)//'_raw_u'

        !write v
        !!write(*,*) 'write_field(): File opened to write output field:',filename
        !!write(*,*)  TRIM(filename)//'_raw_v'
        !!open(unit=IUNIT, FILE=TRIM(filename)//'_raw_v', FORM='unformatted')
        !!WRITE(IUNIT) (((real(v(i,j,k),4),i=1,n1),j=1,n2),k=1,n3)
        !!close(IUNIT)
	    !!write(*,*) 'End  writing output field:', TRIM(filename)//'_raw_v'

        !write w
        !!write(*,*) 'write_field(): File opened to write output field:',filename
        !!write(*,*)  TRIM(filename)//'_raw_w'
        !!open(unit=IUNIT, FILE=TRIM(filename)//'_raw_w', FORM='unformatted')
        !!WRITE(IUNIT) (((real(w(i,j,k),4),i=1,n1),j=1,n2),k=1,n3)
        !!close(IUNIT)
	    !!write(*,*) 'End  writing output field:', TRIM(filename)//'_raw_w'

        !write field
        write(*,*) 'write_field(): File opened to write output field:',filename
        write(*,*)  TRIM(filename)//'_raw_field'
        open(unit=IUNIT, FILE=TRIM(filename)//'_raw_field', FORM='unformatted')
        WRITE(IUNIT) ((( (real(u(i,j,k),4),real(v(i,j,k),4),real(w(i,j,k),4))  ,i=1,n1),j=1,n2),k=1,n3)
        close(IUNIT)
	    write(*,*) 'End  writing output field:', TRIM(filename)//'_raw_field'

        !write magnitude
        write(*,*) 'write_field(): File opened to write output field:',filename
        write(*,*)  TRIM(filename)//'_raw_mag'
        open(unit=IUNIT, FILE=TRIM(filename)//'_raw_mag', FORM='unformatted')
        WRITE(IUNIT) (((real( sqrt(u(i,j,k)**2 + v(i,j,k)**2 + w(i,j,k)**2),4),i=1,n1),j=1,n2),k=1,n3)
        close(IUNIT)
	    write(*,*) 'End  writing output field:', TRIM(filename)//'_raw_mag'


        
      end subroutine write_field


! 
!     ------------ 
!
!     write  data scalar field
! 
!     ------------ 
! 
      subroutine write_amira_am_format(u, &
           n1, n2, n3,filename) 

        implicit none 

  
        integer n1, n2, n3 
        REAL (pr), DIMENSION (0:n1+1,0:n2,0:n3), INTENT (INOUT) :: u

        character (*) filename
        integer IUNIT!, IUNIT2
        integer i, j, k   

	IUNIT = 200
	!IUNIT2 = 201
        write(*,*) 'write_scalar_field(): File opened to write output field:',filename
 
        write(*,*) '******************************************************************'
        write(*,*) 'field(1:10,1,1):', u(1:10,1,1)
        write(*,*) 'field(n1,n2,n3-9:n3):', u(n1,n2,n3-9:n3)
        write(*,*) '******************************************************************'


        open(unit=IUNIT, FILE=filename, FORM='formatted')

        WRITE(IUNIT,'(A)') '# AmiraMesh BINARY'
        WRITE(IUNIT,*) '# Dimensions in x, y, z, directions'
        WRITE(IUNIT,*) 'define Lattice ',  n1, n2, n3
        WRITE(IUNIT,*) 'Parameters {'
        WRITE(IUNIT,*) ' CoordType uniform '
        WRITE(IUNIT,*) ' BoundingBox 0 1 0 1 0 1'
        WRITE(IUNIT,*) ' }'
        WRITE(IUNIT,*) 'Lattice { float XXX } = @1'
        WRITE(IUNIT,*) ''
        WRITE(IUNIT,*) '@1'
        !WRITE(IUNIT,*) ''

        
        close(IUNIT)

        open(unit=IUNIT, CONVERT='BIG_ENDIAN', FILE=filename, FORM='unformatted',STATUS="old",POSITION="append")
!STATUS="old",POSITION="append"
        
        WRITE(IUNIT) (((real(u(i,j,k),4),i=1,n1),j=1,n2),k=1,n3)
        close(IUNIT)

 
        write(*,*) 'write_scalar_field(): File opened to write output scalar field:'
        write(*,*)  TRIM(filename)
 
 
        write(*,*) 'End  writing output scalar  field:', TRIM(filename)

      end subroutine write_amira_am_format





  
end module write_amira_am
