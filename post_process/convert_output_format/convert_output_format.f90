MODULE local_share_vars

  INTEGER ,  PARAMETER  ::  vtk_format = 1
  INTEGER ,  PARAMETER  ::  amira_format = 2
  CHARACTER (LEN=256)   ::  file_name2           !input command file name
END MODULE local_share_vars

PROGRAM convert_output_format
  USE precision
  USE local_share_vars
  USE share_consts
  USE pde
  USE sizes
  USE FFT_MODULE
!  USE fft_util

!  USE write_amira_psi
!  USE write_amira_am
  USE write_field_mod
  USE write_vtk_mod
  USE io_3D
  USE wlt_trns_mod
!  USE wlt_trns_util_mod
  USE vector_util_mod
  USE field
  IMPLICIT NONE

  !--Wavelet transform variables
  REAL (pr), DIMENSION (:,:,:,:), ALLOCATABLE, SAVE :: u_out
  REAL (pr), DIMENSION (:,:,:)  , ALLOCATABLE, SAVE :: vort_out

!  REAL (pr), SAVE,DIMENSION (:,:), POINTER :: c
  REAL (pr), DIMENSION (:), POINTER , SAVE :: scl
  REAL (pr), SAVE ::  eps_post, eps_sim, eps_indx, dx, dy
  INTEGER   ::  j_out, j_out_in!,frm 
  INTEGER   ::  nxyz_out(1:3) !, nxyz(1:3)
  INTEGER   :: i, j, k,  ie ,llc
  INTEGER   :: n_vars_read
  REAL (pr), DIMENSION (:,:), ALLOCATABLE, SAVE ::  xx_out, w
  INTEGER  , DIMENSION (:)  , ALLOCATABLE, SAVE ::  active_pts
  INTEGER   :: n_active_pts
  INTEGER   :: ix,jx,ix_l,ix_h,ixp,ixpm,ix_even,ix_odd,iz
  INTEGER   :: iy,jy,iy_l,iy_h,iyp,iypm,iy_even,iy_odd
!  CHARACTER (LEN=64) :: file_gen, file_cwlt
  CHARACTER (LEN=4)    :: name
  CHARACTER (LEN=10)   :: epsname  
  CHARACTER (LEN=256)  :: outputfile, outputfile2, output_dir
  CHARACTER (LEN=64)   :: output_format
  CHARACTER(LEN=FILE_NUM_LEN) :: file_num_str

  INTEGER   :: idim!, dim
  LOGICAL   :: make_non_adaptive_binary, make_non_adaptive_ascii, make_real_space,calc_vorticity
  LOGICAL   :: 	 new_grid , make_adaptive_wcells,found 
  INTEGER   :: 	 j_mn_init     
  INTEGER   :: begin_station_num, end_station_num
  INTEGER   :: station_num !station # in saved solution file name
  !ie. test_iso_0001.res is station_num = 1
  INTEGER :: n_var_saved ! number of vars saved
  INTEGER :: saved_vort  ! 1 if we saved vorticity in this file
  INTEGER , DIMENSION(6)::  ibounds
  INTEGER :: len_write
  !supported formats
  INTEGER               :: format_num
  INTEGER :: wlt_fmly
  INTEGER , DIMENSION(:), ALLOCATABLE :: ij2j_tbl
  INTEGER :: max_nxyz
  INTEGER :: output_vort_mag(1)
  REAL (pr), DIMENSION (6) :: bounds
  REAL(pr)       :: pi2
  REAL(pr)       :: the_tke,the_tdiss
  REAL(pr)       :: viscosity
  REAL(pr) ,DIMENSION(:,:), ALLOCATABLE :: vort
  REAL(pr) ,DIMENSION(:), ALLOCATABLE :: vort_mag, u_tmp !, vort_mag2

! stuff from read_input()
  REAL(pr)       :: eps_init,eps_run
  INTEGER        :: eps_adapt_steps
  REAL(pr)       ::  scl_fltwt !weight for temporal filter on scl

  REAL(pr)       ::     cflmax,cflmin,dtwrite
  INTEGER        ::    j_filt,j_lev_old,j_lev_a, nxyz_a(1:3)
  INTEGER, DIMENSION(-1:1) :: ij_adj, adj_type



!
!  Get command line arguments.  
!  Uses string file_name2 defined in module share_vars_local
!
	CALL  read_command_line_input_local

  ! READ ARGUMENTS
  !----------- inputs
  OPEN(1,FILE=TRIM(file_name2),STATUS='OLD')
  READ (1,'(A64)')    file_gen
  READ (1,'(A256)')   output_dir
  READ (1,*)   begin_station_num
  READ (1,*)   end_station_num
  READ (1,*)    eps_post
  READ (1,*)    eps_sim
  READ (1,*)    eps_indx
  READ (1,*)    viscosity
  READ (1,*)    j_out_in
  READ (1,'(L1)') make_real_space
  READ (1,'(L1)') make_non_adaptive_binary
  READ (1,'(L1)') make_non_adaptive_ascii
  READ (1,'(L1)') make_adaptive_wcells

  READ (1,'(L1)') calc_vorticity
  READ (1,'(A64)')    output_format
  CLOSE(1)

  PRINT *, 'file_gen: ',file_gen
  PRINT *, 'output_dir :', output_dir
  PRINT *, 'eps_sim',eps_sim 
  PRINT *, 'eps_post',eps_post 
  PRINT *, 'j_out ', j_out_in
  PRINT *, 'output_format ', output_format

 
  !
  ! Read input file parameters
  ! case_wlt.inp
  !
  file_name = file_gen(1:INDEX(file_gen,' ')-1)//'.inp'
  CALL read_input(eps_init,eps_run,eps_adapt_steps, &
     scl_fltwt, &
     cflmax,cflmin,dtwrite,  &
     j_mn, j_mx,j_mn_init, &
     j_filt,ij_adj, adj_type  )

 CALL check_inputs(scl_fltwt, eps,j_mn, j_mx,j_mn_init)

  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  !Oleg: need to deal with wlt_fmly from input
   wlt_fmly = 1 ! temporiry fix, need to be read from the file
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  CALL check_format(output_format, format_num)

  

  !
  !-- First open the first data file and find out if it is 3D or 2D
  WRITE(file_num_str,FILE_NUM_FMT) begin_station_num
  CALL read_solution_dim(  &
    'results/'//file_gen(1:INDEX(file_gen//' ' ,' ')-1)//file_num_str//'.res' )
  PRINT *,'after  read_solution_dim , dim = ', dim
  !
  ! -- setup u_variable_names array to tell read_solution which variables to read
  !
  IF( dim == 3 ) THEN
      n_vars_read = 3
  ELSE
      n_vars_read = 2 
  END IF


  !
  !-- Loop through each data file 
  !
  DO station_num = begin_station_num, begin_station_num !end_station_num
     PRINT *, 'Process station_num ',station_num
     WRITE (name, '(i4.4)') station_num
    !--------------------

	 !
	 ! Read the solution file. The wlt coeff's are read into the global u(1:nwlt,1:n_var)
	 ! some of the things set in read_solution that we use below are:
	 ! n_var
	 ! u_variable_names
	 ! j_lev
	 !
     WRITE(file_num_str,FILE_NUM_FMT) station_num
     CALL read_solution( scl, DO_RESTART_FALSE, &
      'results/'//file_gen(1:INDEX(file_gen//' ' ,' ')-1)//file_num_str//'.res' ,& 
	  DO_READ_ALL_VARS_TRUE )

     ALLOCATE(n_var_index(1:n_var))
     DO i=1,n_var
	    n_var_index(i) = i
	 END DO

!-------------------------------
     ! set j_out (the max level in the inverse transform) to j_lev 
     ! (the max level of the simulation) if the j_out given is < 0
     ! else use j_out from input file   
     IF( j_out_in <= 0 ) THEN
		j_out = j_lev
     ELSE
	    j_out = j_out_in
	 END IF

	 IF( j_out > j_lev ) THEN
		PRINT *,'Set j_out <= j_lev '
		stop
	 END IF




	!
	! make table for ij2j 
	!
	max_nxyz = MAXVAL(nxyz)
	IF( ALLOCATED(ij2j_tbl) ) DEALLOCATE(ij2j_tbl)
	ALLOCATE(ij2j_tbl(max_nxyz) )
	DO i=1,max_nxyz
		ij2j_tbl = ij2j(i,j_lev)
	END DO


    ALLOCATE( active_pts(nwlt) )

 
!     END IF !make_non_adaptive
PRINT *,' Wavelet Coefficients of Velocity'
PRINT *,' MINMAXVAL(u(1:nwlt,1)) = ', MINVAL(u(1:nwlt,1)), MAXVAL(u(1:nwlt,1))
PRINT *,' MINMAXVAL(u(1:nwlt,2)) = ', MINVAL(u(1:nwlt,2)), MAXVAL(u(1:nwlt,2))
PRINT *,' MINMAXVAL(u(1:nwlt,3)) = ', MINVAL(u(1:nwlt,3)), MAXVAL(u(1:nwlt,3))
!PRINT *,' MAXVAL(ABS(u(1:nwlt,4))) = ', MAXVAL(ABS(u(1:nwlt,4)))
!PRINT *,' MAXVAL(ABS(u(1:nwlt,5))) = ', MAXVAL(ABS(u(1:nwlt,5)))
!PRINT *,' MAXVAL(ABS(u(1:nwlt,6))) = ', MAXVAL(ABS(u(1:nwlt,6)))

     !
     !-- Set output file name. If the results were wavelet filtered above (eps_post > eps_sim(ulation) )
     ! this filter eps_post is added to the file name.
     !
	 llc = LEN(TRIM(output_dir))
	 IF( output_dir(llc:llc) /= '/' ) output_dir = TRIM(output_dir)//'/'
     IF( eps_post == 0.0 ) THEN
        outputfile = TRIM(output_dir)//TRIM(file_gen)
     ELSE
        WRITE (epsname, '(f10.6)') eps_post
        ! WRITE leaves initial blanks if the non-decimal part is not big
        !enough. So we first use INDEX to find where the real number begins
        llc = INDEX(epsname,' ',BACK=.TRUE.)+1; 
        outputfile = TRIM(output_dir)//TRIM(file_gen)//'filt.eps'//epsname(llc:LEN(epsname))//'_st'
     END IF

     !write psi coefficient  file for Amira
     !CALL write_amira_psi_format( u(1:nwlt,1:dim), &
     !     outputfile(1:INDEX(outputfile,' ')-1)//name//'.veloc.maxabswltcoeff.psi',&
     !    j_out , j_lev)
 

    ! *********************************************************************************
	! VELOVICITY
	! *********************************************************************************


    ! Find active points
	IF( eps_post > 0.0_pr ) THEN
	    n_active_pts = 0
		DO i=1,nwlt
			IF( abs(u(i,1)) > scl(1)*eps_post .OR. &
			    abs(u(i,2)) > scl(2)*eps_post .OR. &
				abs(u(i,3)) > scl(3)*eps_post ) THEN

				n_active_pts = n_active_pts +1
				active_pts(n_active_pts) = i !save index into original flat array
			END IF
		END DO
	ELSE !all points are considered active
		DO i=1,nwlt			
				active_pts(i) = i			
		END DO
		n_active_pts = nwlt
	END IF


    ! THIS NEEDS indx() so it must be called after init_DB
    !
	! write velocity unstructured wlt coefficients based on j_lev 
	! Threshold and only write values > scl()*eps_post
	!
!	IF( format_num == vtk_format ) THEN
!		CALL write_unstructured_vtk(active_pts, n_active_pts, nwlt, nxyz, MAXVAL(nxyz),  u(:,1:3), indx, xx ,it,&
!			dim ,j_out, TRIM(outputfile)//'.'//name,'velocity_wltcoeffs')
!      outputfile2 = TRIM(outputfile)//'.'//name//'velocity_wltcoeffs.vtu'//CHAR(0)
	  
!	  IF(make_adaptive_wcells) &
!	    CALL WRITE_VTK_wDelaunay(dim, MAXVAL(nxyz), n_active_pts, nwlt,  dim, indx, active_pts, xx, &
!	     u(:,1:3), ij2j_tbl, TRIM(outputfile)//'.'//name//'.velocity_wltcoeffs.vtu'//CHAR(0) )
!	     u(:,1:3), vort_mag, .FALSE., ij2j_tbl, TRIM(outputfile)//'.'//name//'.velocity_wltcoeffs.vtu'//CHAR(0) )

!	ELSE IF( format_num == amira_format ) THEN
	
!		CALL write_amira_psi_format( u(:,1:3),  TRIM(outputfile)//'.'//name//'.velocity_wltcoeffs.psi', j_out , j_lev)

!	END IF



 

    ! *********************************************************************************
	!VORTICITY
    ! *********************************************************************************
	IF( calc_vorticity) THEN
	     ALLOCATE(vort(1:nwlt,3)) ! vorticity 
	     ALLOCATE(vort_mag(1:nwlt)) ! vorticity magnitude

	 	 !
		 ! Initialize the field database
		 !
		 new_grid  = .FALSE.
		 nwlt_old  = 0
		 nwltj_old = 0
		 nwltj     = 0
		 j_lev_old = j_lev
		 j_mn_init = j_mn
		 j_lev_a   = j_lev !I do not know what j_lev_a is! but in main it is set to j_lev on startup..
		 nxyz_a    = nxyz  !I do not know what this is!!
		 new_grid = .TRUE.
		 ALLOCATE(n_var_adapt(1:n_var,0:1))
         n_var_adapt(1:n_var,0:1) = .FALSE.
		 !ALLOCATE(n_var_index(1:n_var))
		 DO i=1,n_var
		   n_var_index=i
		 END DO
		 ALLOCATE(n_var_interpolate(1:n_var,0:1))
         n_var_interpolate=.FALSE.
		 n_var_exact = 0
		 CALL init_DB( nwlt_old, nwlt, nwltj_old, nwltj, new_grid,&
					  j_lev_old, j_lev, j_mn_init, j_mn, j_mx , MAXVAL(nxyz) , nxyz, &
					 .TRUE. )

		! flag  -  -1 = restart, 0 = in startup code, 1 in main loop
		!***********************************************************
		CALL adapt_grid( 0 ,  eps , j_lev_old , j_lev_a, j_mn, j_mx, nxyz_a, ij_adj,&
			adj_type, scl ,new_grid)

					     !
		! write indx out for information
		! This has to be done after init_DB, because indx is setup in init_DB()
		!
		OPEN( UNIT=11,FILE=TRIM(outputfile)//'.'//name//'.active_pts.indx',STATUS='UNKNOWN', &
			FORM='FORMATTED', POSITION='rewind')
		DO i = 1,nwlt
			WRITE(11,'( 3(i," "))') indx(i,1),indx(i,2),indx(i,3)
		END DO
		CLOSE(11)


        !
		! the sclale (scl()) saved inthe .res file is already multiplied by eps so to
		! change the epsilon for postprocessing we do scl()*eps_indx/eps
		!

		IF( eps_indx > 0.0 ) THEN
			OPEN( UNIT=11,FILE=TRIM(outputfile)//'.'//name//'.significant_pts.indx',STATUS='UNKNOWN', &
				FORM='FORMATTED', POSITION='rewind')
			PRINT *,'Simultion eps =', eps
            PRINT *,'eps_indx provided for creating indx of significant points =', eps_indx
			PRINT *,'Simulation Scale eqn 1 = ', scl(1)
			PRINT *,'Simulation Scale eqn 2 = ', scl(2)
			PRINT *,'Simulation Scale eqn 3 = ', scl(3)

			j=0
			DO i=1,nwlt
				IF( abs(u(i,1)) > scl(1)*eps_indx .OR. &
					abs(u(i,2)) > scl(2)*eps_indx .OR. &
					abs(u(i,3)) > scl(3)*eps_indx   ) THEN
				   WRITE(11,'( 3(i," "))') indx(i,1),indx(i,2),indx(i,3)
				   j=j+1
				END IF
			END DO
			CLOSE(11)
			PRINT *,'Number of significant points = ',j
		END IF


        !
		! Transform u (velocity) into real space
		!
		CALL c_wlt_trns (u, dim, 1, dim, HIGH_ORDER, WLT_TRNS_INV)

		PRINT *,' '
		PRINT *,' Velocity in real space '
		PRINT *,' MINMAXVAL(u(1:nwlt,1)) = ', MINVAL(u(1:nwlt,1)), MAXVAL(u(1:nwlt,1))
		PRINT *,' MINMAXVAL(u(1:nwlt,2)) = ', MINVAL(u(1:nwlt,2)), MAXVAL(u(1:nwlt,2))
		PRINT *,' MINMAXVAL(u(1:nwlt,3)) = ', MINVAL(u(1:nwlt,3)), MAXVAL(u(1:nwlt,3))

        !
		! Take vorticity magnitude from solution file if it there.
		!

	    DO i = 1,n_var
		   
		   PRINT *,u_variable_names( i ),MINVAL(u(1:nwlt,i)) ,MAXVAL(u(1:nwlt,i)) 
        END DO

		i = 0
		found = .FALSE.
		DO WHILE( i <= n_var .AND.  .NOT. found ) 
		   i = i +1
		   PRINT *,u_variable_names( i ),MINVAL(u(1:nwlt,i)) ,MAXVAL(u(1:nwlt,i)) 

		   if( INDEX(u_variable_names( i ),'Pressure') /= 0 ) found = .TRUE.
        END DO
		!IF( found ) THEN
!
!		   PRINT *,'Taking Vorticity magnitude from the solution file..'
		   !take vorticity from the solution file
!		   vort_mag(:) = u(1:nwlt,i)
!
!		   CALL c_wlt_trns (vort_mag, 1, 1, 1, HIGH_ORDER, WLT_TRNS_FWD)
!
 !          PRINT *,'coeff MINMAXVAL(vort_mag(:)) ',MINVAL(vort_mag(:)) ,MAXVAL(vort_mag(:)) 
!
!		   CALL write_unstructured_vtk_scalar(active_pts, n_active_pts, nwlt, nxyz, MAXVAL(nxyz),  vort_mag, indx, xx ,it,&
!		   		dim ,j_out, TRIM(outputfile)//'.'//name,'vortMag_wltcoeffs_fromSolutionFile')
!
!		   CALL c_wlt_trns (vort_mag, 1, 1, 1, HIGH_ORDER, WLT_TRNS_INV)
 !          PRINT *,'real MINMAXVAL(vort_mag(:)) ',MINVAL(vort_mag(:)) ,MAXVAL(vort_mag(:)) 

		!ELSE

			 !
			 ! calculate vorticity from velocity 
			 ! (starting and ending with u in real space)

			 CALL cal_vort (u(:,1:dim), vort(:,1:dim), nwlt)
			 IF(dim==3) THEN
				vort_mag(:) = SQRT(vort(:,1)**2.0_pr + vort(:,2)**2.0_pr + vort(:,3)**2.0_pr )
			 ELSE IF(dim==2) THEN
				vort_mag(:) = SQRT(vort(:,1)**2.0_pr + vort(:,2)**2.0_pr )
			 END IF	
!			 PRINT *,'MAXVAL(ABS(vort_mag2(1:nwlt) - vort_mag(1:nwlt)) ', &
!			    MAXVAL(ABS(vort_mag2(1:nwlt) - vort_mag(1:nwlt)))
		     CALL write_unstructured_vtk_scalar(active_pts, n_active_pts, nwlt, nxyz, MAXVAL(nxyz),  vort_mag, indx, xx ,it,&
		   		dim ,j_out, TRIM(outputfile)//'.'//name,'vortMag_wltcoeffs_fromSolutionFile')
!
        !END IF
stop
        !
		! Put velocity is in wlt space 
        CALL c_wlt_trns (u, dim, 1, dim, HIGH_ORDER, WLT_TRNS_FWD)

   
		!
		! First put vorticity into wlt space
		!!!CALL c_wlt_trns (vort, dim, 1, dim, HIGH_ORDER, WLT_TRNS_FWD)
  
		!
		! write velocity unstructured wlt coefficients based on j_lev 
		! Threshold and only write values > scl()*eps_post
		!
		!!!IF( format_num == vtk_format ) THEN
		!!!	CALL write_unstructured_vtk(active_pts, n_active_pts, nwlt, nxyz, MAXVAL(nxyz),  vort(:,1:3), indx, xx ,it,&
		!!!		dim ,j_out, TRIM(outputfile)//'.'//name,'vort_wltcoeffs')
		!!!END IF

		!DEALLOCATE(vort)

     END IF ! do vorticity



	IF( make_real_space ) THEN

	

		nxyz_out(1)=mxyz(1)*2**(j_out-1)
		nxyz_out(2)=mxyz(2)*2**(j_out-1)
		IF( dim == 3 ) THEN
			nxyz_out(3)=mxyz(3)*2**(j_out-1)
		ELSE
			nxyz_out(3) = 0
		END IF

		ALLOCATE( xx_out(0:MAXVAL(nxyz_out),1:dim) )

     	!
     	!-- set the real space coordinates for the interpolated field
    	!
     	DO idim = 1,dim
            DO i=0,nxyz_out(idim)-prd(idim)
           	xx_out(i,idim)=xx(i*2**(j_lev-j_out),idim)
       	    END DO
        END DO

		    	!
     	!-------- defining bounds for the regions to be plotted 
     	!
     	DO idim = 1,dim
        	ibounds(2*idim-1) = 0
        	DO WHILE (xx_out(ibounds(2*idim-1),dim) < bounds(2*idim-1) .AND. ibounds(2*idim-1) < nxyz_out(idim))
           		ibounds(2*idim-1) = ibounds(2*idim-1)+1
        	END DO
        	ibounds(2*idim) = nxyz_out(idim)
        	DO WHILE (xx_out(ibounds(2*idim),dim) > bounds(2*idim) .AND. ibounds(2*idim) > 0)
           		ibounds(2*idim) = ibounds(2*idim)-1
        	END DO
     	END DO



		!
		! Save only real space points on adapted grid 
		!
		! First extract data at adapted grid points
		!IF(   j_lev == j_out) THEN
		!	DO i=1,nwlt
		!		u(i,1:3) = u_out(1:3,indx(i,1),indx(i,2),indx(i,3))
		!	END DO
        !
		!	IF( format_num == vtk_format ) THEN
		!		CALL write_unstructured_vtk(active_pts, n_active_pts, nwlt, nxyz, MAXVAL(nxyz),  u(:,1:3), indx, xx ,it,&
		!			dim ,j_out, TRIM(outputfile)//'.'//name,'velocity_OnAdaptedGrid')
		!	!ELSE IF( format_num == amira_format ) THEN
		!	END IF
		!END IF

		!
		!-------- setting weights for wavelet transform 
		!
		CALL c_wlt_trns_interp_setup(j_out, nxyz_out, maxval(nxyz_out), xx_out )


		!
		!-- Interpolate to full grid based on epsilon. If epsilon is greater then the
		! epsilon used in the simulation then a filtered version of the results
		! will be interpolated to the full grid.
		!
	    ALLOCATE( u_out(3,0:nxyz_out(1)+1,0:nxyz_out(2),0:nxyz_out(3)) )     

		PRINT *,'before c_wlt_trns_interp  j_lev , j_out ',j_lev , j_out
		CALL c_wlt_trns_interp (u_out(1,:,:,:), u(1:nwlt,1), nxyz_out, nwlt, j_lev, j_out, wlt_fmly, scl(1)*eps_post)
		CALL c_wlt_trns_interp (u_out(2,:,:,:), u(1:nwlt,2), nxyz_out, nwlt, j_lev, j_out, wlt_fmly, scl(2)*eps_post)
		CALL c_wlt_trns_interp (u_out(3,:,:,:), u(1:nwlt,3), nxyz_out, nwlt, j_lev, j_out, wlt_fmly, scl(3)*eps_post)


		!
		! write real space points on full grid (binary)
		!
		IF( make_real_space .AND. make_non_adaptive_binary ) THEN

			CALL write_field(u_out(1:3,0:nxyz_out(1)-prd(1),0:nxyz_out(2)-prd(2),0:nxyz_out(3)-prd(3)) , &
				nxyz_out(1)-prd(1)+1, nxyz_out(2)-prd(2)+1, nxyz_out(3)-prd(3)+1,&
				TRIM(outputfile)//'.'//name//'_velocity' )
						  
		END IF

		!
		! write real space points on full grid (ascii)
		!
		IF(  make_non_adaptive_ascii ) THEN

			IF( format_num == vtk_format ) THEN
				CALL write_structured_vtk(u_out(1:3,0:nxyz_out(1),0:nxyz_out(2),0:nxyz_out(3)), nwlt,nxyz_out,  MAXVAL(nxyz_out),  xx_out ,it,&
					j_out, TRIM(outputfile)//'.'//name,'velocity')

			ELSE IF( format_num == amira_format ) THEN

			ENDIF
		END IF

            ! 
			! Use fft

            ! initialize fft's
            ! Last arg true is to zero oddball points in fft.
            pi2 = 8.0D0 *atan(1.0D0 )
            call init_fft(nxyz_out(1),nxyz_out(2),nxyz_out(3),pi2,pi2,pi2, .true. )
            call rtoft( u_out(1,:,:,:) )
            call rtoft( u_out(2,:,:,:) )
            call rtoft( u_out(3,:,:,:) )
            CALL curl_ip_fft(u_out(1,:,:,:),u_out(2,:,:,:),u_out(3,:,:,:))
            call ftort( u_out(1,:,:,:) )
            call ftort( u_out(2,:,:,:) )
            call ftort( u_out(3,:,:,:) )
 			CALL write_structured_vtk(u_out(1:3,0:nxyz_out(1),0:nxyz_out(2),&
			    0:nxyz_out(3)), nwlt,nxyz_out,  MAXVAL(nxyz_out),  xx_out ,it,&
				j_out, TRIM(outputfile)//'.'//name,'vort_fromfft')


            !PRINT *,'cmp vort_mag made from cal_vort and fft vort mag (in u_out(1,:,:,:) )'
 			!DO i=1,nwlt
			!	 PRINT *, 'Vi ', vort(i,1) - u_out(1,indx(i,1),indx(i,2),indx(i,3))
			!	 PRINT *, 'Vj ', vort(i,2) - u_out(2,indx(i,1),indx(i,2),indx(i,3))
			!	 PRINT *, 'Vk ', vort(i,3) - u_out(3,indx(i,1),indx(i,2),indx(i,3))
		    !END DO


            ! output magnitude in raw form
			u_out(1,0:nxyz_out(1)-prd(1),0:nxyz_out(2)-prd(2),0:nxyz_out(3)-prd(3)) = &
			  SQRT( &
			  u_out(1,0:nxyz_out(1)-prd(1),0:nxyz_out(2)-prd(2),0:nxyz_out(3)-prd(3))**2.0 + &
			  u_out(2,0:nxyz_out(1)-prd(1),0:nxyz_out(2)-prd(2),0:nxyz_out(3)-prd(3))**2.0 + &
			  u_out(3,0:nxyz_out(1)-prd(1),0:nxyz_out(2)-prd(2),0:nxyz_out(3)-prd(3))**2.0 )


			open(unit=11, FILE=TRIM(outputfile)//'.'//name//'MagVort_fromfft.raw', FORM='unformatted')
			WRITE(11) u_out(1,0:nxyz_out(1)-prd(1),0:nxyz_out(2)-prd(2),0:nxyz_out(3)-prd(3))
			close(11)

            !
			! Compare vort magnitude from the soltuion file to the one generated from
			! velocity using ffts
			!
			ALLOCATE(u_tmp(1:nwlt))

            ! vort magnitude from the ffts
 			DO i=1,nwlt
				 u_tmp(i) = u_out(1,indx(i,1),indx(i,2),indx(i,3))
		    END DO

	  
	        PRINT *,'MAXVAL(ABS(u_tmp(1:nwlt) - vort_mag(1:nwlt)) ', &
			    MAXVAL(ABS(u_tmp(1:nwlt) - vort_mag(1:nwlt)))

            DEALLOCATE(u_tmp)

	  output_vort_mag(1) = 1 ! flag to output vorticity mag
	  IF(make_adaptive_wcells) &
	    CALL WRITE_VTK_wDelaunay(dim, MAXVAL(nxyz), n_active_pts, nwlt,  dim, indx, active_pts, xx, &
	     u(:,1:3), vort_mag, output_vort_mag,ij2j_tbl, TRIM(outputfile)//'.'//name//'.fromfft.vtu'//CHAR(0) )

stop

		!
		! write vorticity real space points on full grid (binary)
		!
		IF( calc_vorticity  ) THEN
           


           !
		   ! Project to a non-adapted gric
		    ALLOCATE(vort_out(0:nxyz_out(1)+1,0:nxyz_out(2),0:nxyz_out(3)))
			CALL c_wlt_trns_interp (vort_out(:,:,:), vort_mag(1:nwlt), nxyz_out, nwlt, j_lev,&
			  j_out, wlt_fmly, 0.0_pr)
			open(unit=11, FILE=TRIM(outputfile)//'.'//name//'MagVort.raw', FORM='unformatted')

			WRITE(11) vort_out(0:nxyz_out(1)-prd(1),0:nxyz_out(2)-prd(2),0:nxyz_out(3)-prd(3))
			close(11)
            DEALLOCATE(vort_out)
		END IF !( calc_vorticity  ) 

		!
		! Save only real space points on adapted grid 
		!
		! First extract data at adapted grid points
		!IF( make_real_space  .AND. j_lev == j_out) THEN
!
!			DO i=1,nwlt
!				u(i,1:3) = u_out(1:3,indx(i,1),indx(i,2),indx(i,3))
!			END DO
!
!			IF( format_num == vtk_format ) THEN
!				CALL write_unstructured_vtk(active_pts, n_active_pts, nwlt, nxyz, MAXVAL(nxyz),  u(:,1:3), indx, xx ,it,&
!					dim ,j_out, TRIM(outputfile)//'.'//name,'vorticity_OnAdaptedGrid')
!
!			ELSE IF( format_num == amira_format ) THEN
!
!			END IF

		CALL  c_wlt_trns_interp_free() !

        DEALLOCATE ( u_out, xx_out )
	END IF !( make_real_space ) 

    !
	! write velocity unstructured wlt  based on j_lev 
	! Threshold and only write values > scl()*eps_post
	!
	IF( format_num == vtk_format ) THEN
		CALL write_unstructured_vtk(active_pts, n_active_pts, nwlt, nxyz, MAXVAL(nxyz),  u(:,1:3), indx, xx ,it,&
			dim ,j_out, TRIM(outputfile)//'.'//name,'velocity')
      !outputfile2 = TRIM(outputfile)//'.'//name//'velocity_wltcoeffs.vtu'//CHAR(0)
	  
	  output_vort_mag(1) = 1 ! flag to output vorticity mag
	  IF(make_adaptive_wcells) &
	    CALL WRITE_VTK_wDelaunay(dim, MAXVAL(nxyz), n_active_pts, nwlt,  dim, indx, active_pts, xx, &
	     u(:,1:3), vort_mag, output_vort_mag,ij2j_tbl, TRIM(outputfile)//'.'//name//'.vtu'//CHAR(0) )

!	ELSE IF( format_num == amira_format ) THEN
	
!		CALL write_amira_psi_format( u(:,1:3),  TRIM(outputfile)//'.'//name//'.velocity_wltcoeffs.psi', j_out , j_lev)

	END IF

IF(.FALSE.) THEN
		 !
		 !-- Interpolate to full grid based on epsilon. If epsilon is greater then the
		 ! epsilon used in the simulation then a filtered version of the results
		 ! will be interpolated to the full grid.
		 !
		 IF( make_real_space ) THEN
			 CALL c_wlt_trns_interp (u_out(1,:,:,:), vort(1:nwlt,1), nxyz_out, nwlt, j_lev, j_out, wlt_fmly, 0.0_pr)
			 CALL c_wlt_trns_interp (u_out(2,:,:,:), vort(1:nwlt,2), nxyz_out, nwlt, j_lev, j_out, wlt_fmly, 0.0_pr)
			 CALL c_wlt_trns_interp (u_out(3,:,:,:), vort(1:nwlt,3), nxyz_out, nwlt, j_lev, j_out, wlt_fmly, 0.0_pr)
		 END IF !( make_real_space ) 


		!
		! write vorticity unstructured wlt coefficients based on j_lev
		!
		IF( format_num == vtk_format ) THEN
			CALL write_unstructured_vtk(active_pts, n_active_pts, nwlt, nxyz, MAXVAL(nxyz),  vort(:,1:3), indx, xx ,it,&
				dim ,j_out, TRIM(outputfile)//'.'//name,'vorticity_wltcoeffs')
	!	ELSE IF( format_num == amira_format ) THEN
			
	!		CALL write_amira_psi_format( u(:,4:6),  TRIM(outputfile)//'.'//name//'.vortcity_wltcoeffs.psi', j_out , j_lev)


		END IF

 
		 IF( make_real_space  .AND. make_non_adaptive_ascii ) THEN
			IF( format_num == vtk_format ) THEN
				CALL write_structured_vtk(u_out(1:3,0:nxyz_out(1),0:nxyz_out(2),0:nxyz_out(3)), nwlt,nxyz_out,  &
					MAXVAL(nxyz_out), xx_out ,it,&
					j_out, TRIM(outputfile)//'.'//name,'vorticity')
			ELSE IF( format_num == amira_format ) THEN
		
			END IF

		 END IF

		 IF( make_non_adaptive_binary ) THEN

		 END IF


		
	END IF !false
 

     DEALLOCATE (u, xx, lv_intrnl, lv_bnd, indx,active_pts)

  END DO !loop through stations
  
END PROGRAM  convert_output_format

SUBROUTINE check_format(output_format, format_num)
	USE local_share_vars
	IMPLICIT NONE
    CHARACTER (LEN=*)        :: output_format
	INTEGER , INTENT(INOUT)  :: format_num 


    IF( output_format(1:3) == 'vtk' .OR. output_format(1:3) == 'VTK' ) THEN
		format_num = vtk_format
	ELSE IF( output_format(1:5) == 'amira' .OR. output_format(1:5) == 'AMIRA' ) THEN
	    format_num = amira_format
	ELSE
		PRINT *,' UNknown output format type:', output_format
		PRINT *,' Exiting...'
		STOP
	END IF

END SUBROUTINE check_format

!
!  Get command line arguments.  
!  Uses string file_name2 defined in module share_vars_local
!
SUBROUTINE read_command_line_input_local
  USE PRECISION
  USE local_share_vars
  IMPLICIT NONE
  INTEGER :: iargc

  ! Use the first argument as the input file name if is exists
  IF( iargc() >= 1 ) THEN

     CALL getarg( 1, file_name2 )
  ELSE
     PRINT *,' Error,  input file name must be specified as first argument.'
	 PRINT *,' Exiting...'
	 STOP
  END IF

END	SUBROUTINE read_command_line_input_local
