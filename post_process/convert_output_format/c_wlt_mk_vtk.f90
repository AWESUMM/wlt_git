!**************************************************************
! Begin shared module 
!**************************************************************
MODULE precision
  IMPLICIT NONE
  INTEGER, PARAMETER :: pr = SELECTED_REAL_KIND (8)
END MODULE precision
MODULE share_vars
  USE precision
  INTEGER ::  n_prdct, n_updt, neq
  INTEGER ::  n_assym_prdct, n_assym_updt,n_assym_prdct_bnd,n_assym_updt_bnd
  INTEGER ::  mxyz(1:3),prd(1:3), grid(1:3)
  INTEGER ::  dim
  INTEGER , DIMENSION(6)::  ibounds
  INTEGER, DIMENSION (:,:), ALLOCATABLE, SAVE :: indx
  REAL (pr), DIMENSION (6) :: bounds
  REAL (pr), DIMENSION (:,:,:,:), ALLOCATABLE, SAVE :: wgh_updt, wgh_prdct
END MODULE share_vars
!**************************************************************
! End shared modules  
!**************************************************************

PROGRAM wavelet_interpolation
  USE precision
  USE share_vars
  IMPLICIT NONE

  !--Wavelet transform variables
  REAL (pr), DIMENSION (:,:,:), ALLOCATABLE, SAVE :: u_out
  REAL (pr), DIMENSION (:,:), ALLOCATABLE, SAVE :: c
!  REAL (pr), DIMENSION (:), ALLOCATABLE, SAVE :: x, y
  REAL (pr), SAVE :: scl, eps, dx, dy
  INTEGER   :: j_lev, j_out,frm 
  INTEGER   :: nwlt, nxyz_out(1:3), nxyz(1:3)
  INTEGER   :: i, j, k, lc, it, ie 
  REAL (pr), DIMENSION (:,:), ALLOCATABLE, SAVE :: xyz, xyz_out
  INTEGER :: ix,jx,ix_l,ix_h,ixp,ixpm,ix_even,ix_odd
  INTEGER :: iy,jy,iy_l,iy_h,iyp,iypm,iy_even,iy_odd
  INTEGER, DIMENSION(:), ALLOCATABLE :: lv_intrnl
  INTEGER, DIMENSION(:,:,:), ALLOCATABLE :: lv_bnd
  CHARACTER (LEN=64) :: file_gen, file_cwlt
  INTEGER   :: idim, dim
  LOGICAL :: form
  INTEGER :: n_var_saved ! number of vars saved
  INTERFACE
     SUBROUTINE c_wlt_trns (u_out, c, nxyz_out, nwlt, j_in, j_out, eps)
       USE precision
       USE share_vars
       IMPLICIT NONE
       INTEGER, INTENT(IN) :: nwlt, nxyz_out(1:3), j_in, j_out
       REAL (pr), INTENT (IN) :: eps
       REAL (pr), DIMENSION (1:nwlt), INTENT (IN) :: c
       REAL (pr), DIMENSION (0:nxyz_out(1),0:nxyz_out(2),0:nxyz_out(3)), INTENT (INOUT) :: u_out
     END SUBROUTINE c_wlt_trns
     FUNCTION wgh (k, i, i_l, i_h, istep, i_prd, nx, x)
       USE precision
       IMPLICIT NONE
       INTEGER, INTENT (IN) :: k, i, i_l, i_h, istep, i_prd, nx
       REAL (pr), DIMENSION(0:nx), INTENT (IN) :: x
       REAL (pr) :: wgh
     END FUNCTION wgh
     SUBROUTINE clean_write (n, f, tol)
       USE precision
       IMPLICIT NONE
       INTEGER, INTENT (IN) :: n
       REAL (pr), INTENT (IN) :: tol   
       REAL (pr), DIMENSION (n), INTENT (INOUT) :: f
     END SUBROUTINE clean_write
     SUBROUTINE open_file (filename, form, it, unit, isec)
       IMPLICIT NONE
       INTEGER, INTENT (IN) :: it, unit, isec
       CHARACTER (LEN=64), INTENT (IN) :: filename
       LOGICAL, INTENT (IN) :: form
     END SUBROUTINE open_file
  END INTERFACE
 
!----------- inputs
  OPEN(1,FILE='inp.dat',STATUS='OLD')
    READ (1,'(A64)')    file_gen
    !READ (1,'(7(E,1X),4(I,1X))') eps, bounds, frm, ie, j_out, it
    READ (1,*) eps, bounds, frm, ie, j_out, it
    !READ (1,'(1(E12.5,1X),4(I7,1X))') eps,  ie, j_out, it
print *, 'eps',eps,  'ie',ie, 'j_out',j_out, 'it',it
print *,'frm = ', frm ,' (frm ==1 => formated, frm == 2 => unformatted data)'

  CLOSE(1)

 
!new  form = .FALSE.
 form = .TRUE. !tmp for testing
   IF(frm == 1) form = .TRUE.
!  PRINT *, file_gen,eps,ie,j_out,it
!--------------------

  file_cwlt  = 'results/'//TRIM(file_gen)

  CALL open_file (file_cwlt, form, it, 1, 1)
  IF(form) THEN !--------------- Formatted output 
     READ (1,'(2(I8,1X))') dim
     READ (1,'(2(I8,1X))') n_prdct, n_updt, n_assym_prdct,&
            n_assym_updt,n_assym_prdct_bnd, &
            n_assym_updt_bnd, n_var_saved
     READ (1,'(3(I8,1X))') nxyz(1:3), mxyz(1:3), prd(1:3), grid(1:3)
     READ (1,'(1(I8,1X))') j_lev, nwlt, neq

!TESTING
     WRITE (*,'(2(I8,1X))') dim
     WRITE (*,'(2(I8,1X))') n_prdct, n_updt, n_assym_prdct,&
            n_assym_updt,n_assym_prdct_bnd,&
            n_assym_updt_bnd
     WRITE (*,'(3(I8,1X))') nxyz, mxyz, prd, grid
     WRITE (*,'(1(I8,1X))') j_lev, nwlt, neq


!END TESTING
  ELSE !--------------- Unformatted output 
     READ (1) n_prdct, n_updt, &
          n_assym_prdct, n_assym_updt,n_assym_prdct_bnd,n_assym_updt_bnd, &
          nxyz(1:3) , mxyz(1:3), &
          prd, grid, j_lev, nwlt, neq
  END IF

     j_out = MIN(j_out,j_lev)
     nxyz_out(1)=mxyz(1)*2**(j_out-1)
     nxyz_out(2)=mxyz(2)*2**(j_out-1)
     IF( dim == 3 ) nxyz_out(3)=mxyz(3)*2**(j_out-1)

     ALLOCATE( lv_intrnl(0:j_lev),lv_bnd(0:1,1:2*dim,0:j_lev) ,  c(1:nwlt,1:n_var_saved) )
     ALLOCATE( wgh_updt(-2*n_updt:2*n_updt,0:MAXVAL(nxyz_out),j_out-1,1:dim))
     ALLOCATE( wgh_prdct(-2*n_prdct:2*n_prdct,0:MAXVAL(nxyz_out),j_out-1,1:dim) )

     ALLOCATE( xyz(0:MAXVAL(nxyz),1:dim), indx(1:nwlt,1:dim) )
     ALLOCATE( xyz_out(0:MAXVAL(nxyz_out),1:dim), u_out(0:nxyz_out(1),0:nxyz_out(2),0:nxyz_out(3)) )
     !--Initialize arrays
     wgh_updt  = 0.0_pr 
     wgh_prdct = 0.0_pr
!old code
!!$
!!$     READ (1,'(10(E12.5,1X))') x
!!$     READ (1,'(10(E12.5,1X))') y !DG for some reason I had to make read x,y into two seperate reads to match poisson_3d.f90?
!!$     DO idim = 1,dim
!!$        READ (1,'(10(I8,1X))')    indx(:,idim)
!!$     END DO
!!$     !READ (1,'(10(I8,1X))')    indx
!!$     READ (1,'(10(E12.5,1X))') c(1:nwlt,1:neq)


     !-----
     IF(form) THEN !--------------- Formatted output 
        READ (1,'(4(I8,1X))') lv_intrnl(0:j_lev)    
        READ (1,'(4(I8,1X))') lv_bnd(0:1,1:2*dim,0:j_lev) 
        DO idim = 1,dim
           READ (1,'(10(E12.5,1X))') xyz(0:nxyz(idim),idim)
        END DO

        DO idim = 1,dim
           READ (1,'(10(I8,1X))')    indx(:,idim)
        END DO
       
        
        DO i = 1,n_var_saved
           READ (1,'(10(E12.5,1X))') c(1:nwlt,i)
        END DO
     ELSE !--------------- Unformatted output 
!!$        READ (1) lv_intrnl(0:j_lev),  &    
!!$                 lv_bnd(0:1,1:4,0:j_lev), & 
!!$                 indx
!!$        READ (1) x, y, c(1:nwlt,1:neq)
     END IF
  !-----
  CLOSE (1)


!!$!TEST
!!$write (*,*) 'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
!!$DO idim = 1,dim
!!$   write (*, '(3(I8,1X))') nxyz(idim),nxyz_out(idim)
!!$ENDDO
!!$DO idim = 1,dim
!!$   write (*, '(10(E12.5,1X))') xyz(0:nxyz(idim),idim)
!!$ENDDO
!!$
!!$stop
!!$!END TEST

 
  !--Set weights
  WRITE (6,'("n_updt = ", i2, "  n_prdct = ", i2)') n_prdct, n_updt
print *, nwlt,ie
  scl= MAXVAL ( ABS( c(1:nwlt,ie) ) )
  IF(scl < 1.E-10_pr) scl=1.0_pr

  DO idim = 1,dim
     DO i=0,nxyz_out(idim)
        xyz_out(i,idim)=xyz(i*2**(j_lev-j_out),idim)
     END DO
  END DO
  !
  !-------- defining bounds for the regions to be plotted 
  !
  DO idim = 1,dim
     ibounds(2*idim-1) = 0
     DO WHILE (xyz_out(ibounds(2*idim-1),dim) < bounds(2*idim-1) .AND. ibounds(2*idim-1) < nxyz_out(1))
        ibounds(2*idim-1) = ibounds(2*idim-1)+1
     END DO
     ibounds(2*idim) = nxyz_out(1)
     DO WHILE (xyz_out(ibounds(2*idim),dim) > bounds(2*idim) .AND. ibounds(2*idim) > 0)
        ibounds(2*idim) = ibounds(2*idim)-1
     END DO
  END DO

  !
  !-------- setting weights for wavelelt transform
  !

  DO j = 1, j_out-1
     DO idim =1,dim
        DO ix = 1, mxyz(idim)*2**(j-1)
           ix_odd = (2*ix-1)*2**(j_out-1-j)
           ix_l   = -MIN(ix,n_prdct)
           ix_h   = ix_l+2*n_prdct-1
           ix_h   = MIN(ix_h,mxyz(idim)*2**(j-1)-ix)
           ix_l   = ix_h-2*n_prdct+1
           ix_l = (1-prd(idim))*MAX(ix_l,-ix) + prd(idim)*(-n_prdct)
           ix_h = (1-prd(idim))*MIN(ix_h,mxyz(idim)*2**(j-1)-ix) + prd(idim)*(n_prdct - 1)
           IF  (ix_l+ix_h+1 < 0 .AND. ix_h >= 0 .AND. prd(idim) ==0 ) THEN                             ! the right boundary
              ix_l = MAX(ix_l,-(ix_h+1)-n_assym_prdct,-ix)
           ELSE IF  (ix_l+ix_h+1 < 0 .AND. ix_h == -1 .AND. prd(idim) ==0 ) THEN                       ! right on the right boundary
              ix_l = MAX(ix_l,-1-n_assym_prdct_bnd,-ix )
           ELSE IF  (ix_l+ix_h+1 > 0 .AND. ix_l < 0 .AND. prd(idim) ==0 ) THEN                         !  the left boundary
              ix_h = MIN(ix_h,-ix_l-1+n_assym_prdct,mxyz(idim)*2**(j-1)-ix)
           ELSE IF  (ix_l+ix_h+1 > 0 .AND. ix_l == 0 .AND. prd(idim) ==0 ) THEN                        ! right on the left boundary
              ix_h = MIN(ix_h,n_assym_prdct_bnd,mxyz(idim)*2**(j-1)-ix)   
           END IF
           DO ixpm = ix_l, ix_h
              wgh_prdct(ixpm,ix_odd,j,idim)= &
                   wgh (ixpm, ix_odd, ix_l, ix_h, 2**(j_out-1-j), prd(idim), &
                   nxyz_out(idim), xyz_out(0:nxyz_out(idim),idim))
           END DO
        END DO
        DO ix = 0, mxyz(idim)*2**(j-1)-prd(idim) 
           ix_even = ix*2**(j_out-j)
           ix_l = - MIN (ix, n_updt)
           ix_h = ix_l + 2*n_updt - 1
           ix_h = MIN (ix_h, mxyz(idim)*2**(j-1)-ix-1)
           ix_l = ix_h - 2*n_updt + 1
           ix_l = (1-prd(idim))*MAX(ix_l,-ix) + prd(idim)*(-n_updt)
           ix_h = (1-prd(idim))*MIN(ix_h,mxyz(idim)*2**(j-1)-ix-1) + prd(idim)*(n_updt - 1)
           IF  (ix_l+ix_h+1 < 0 .AND. ix_h >= 0 .AND. prd(idim) == 0) THEN                               ! the right boundary
              ix_l = MAX(ix_l,-(ix_h+1)-n_assym_updt,-ix)
           ELSE IF  (ix_l+ix_h+1 < 0 .AND. ix_h == -1 .AND. prd(idim) == 0) THEN                        ! right on the right boundary
              ix_l = MAX(ix_l,-1-n_assym_updt_bnd,-ix)
           ELSE IF  (ix_l+ix_h+1 > 0 .AND. ix_l < 0 .AND. prd(idim) == 0) THEN                          !  the left boundary
              ix_h = MIN(ix_h,-ix_l-1+n_assym_updt,mxyz(idim)*2**(j-1)-ix-1)
           ELSE IF  (ix_l+ix_h+1 > 0 .AND. ix_l == 0 .AND. prd(idim) == 0) THEN                         ! right on the left boundary
              ix_h = MIN(ix_h,n_assym_updt_bnd,mxyz(idim)*2**(j-1)-ix-1)   
           END IF
           DO ixpm = ix_l, ix_h
              wgh_updt(ixpm,ix_even,j,idim) = &
                   wgh (ixpm, ix_even, ix_l, ix_h, 2**(j_out-1-j), prd(idim), nxyz_out(idim), xyz_out(0:nxyz_out(idim),idim))
           END DO
        END DO
     END DO
  END DO

  !
  !-------- setting weights for wavelelt transform
  !
!!$  DO j = 1, j_out-1
!!$     DO ix = 1, mxy(1)*2**(j-1)
!!$        ix_odd = (2*ix-1)*2**(j_out-1-j)
!!$        ix_l   = -MIN(ix,n_prdct)
!!$        ix_h   = ix_l+2*n_prdct-1
!!$        ix_h   = MIN(ix_h,mxy(1)*2**(j-1)-ix)
!!$        ix_l   = ix_h-2*n_prdct+1
!!$        ix_l = (1-ix_prd)*MAX(ix_l,-ix) + ix_prd*(-n_prdct)
!!$        ix_h = (1-ix_prd)*MIN(ix_h,mxy(1)*2**(j-1)-ix) + ix_prd*(n_prdct - 1)
!!$        IF  (ix_l+ix_h+1 < 0 .AND. ix_h >= 0 .AND. ix_prd ==0 ) THEN                             ! the right boundary
!!$           ix_l = MAX(ix_l,-(ix_h+1)-n_assym_prdct,-ix)
!!$        ELSE IF  (ix_l+ix_h+1 < 0 .AND. ix_h == -1 .AND. ix_prd ==0 ) THEN                       ! right on the right boundary
!!$           ix_l = MAX(ix_l,-1-n_assym_prdct_bnd,-ix )
!!$        ELSE IF  (ix_l+ix_h+1 > 0 .AND. ix_l < 0 .AND. ix_prd ==0 ) THEN                         !  the left boundary
!!$           ix_h = MIN(ix_h,-ix_l-1+n_assym_prdct,mxy(1)*2**(j-1)-ix )
!!$        ELSE IF  (ix_l+ix_h+1 > 0 .AND. ix_l == 0 .AND. ix_prd ==0 ) THEN                        ! right on the left boundary
!!$           ix_h = MIN(ix_h,n_assym_prdct_bnd,mxy(1)*2**(j-1)-ix)   
!!$        END IF
!!$        DO ixpm = ix_l, ix_h
!!$           wghx_prdct(ixpm,ix_odd,j)=wgh (ixpm, ix_odd, ix_l, ix_h, 2**(j_out-1-j), ix_prd, nxyz_out(1), x_out)
!!$        END DO
!!$     END DO
!!$     DO ix = 0, mxy(1)*2**(j-1)-ix_prd
!!$        ix_even = ix*2**(j_out-j)
!!$        ix_l = - MIN (ix, n_updt)
!!$        ix_h = ix_l + 2*n_updt - 1
!!$        ix_h = MIN (ix_h, mxy(1)*2**(j-1)-ix-1)
!!$        ix_l = ix_h - 2*n_updt + 1
!!$        ix_l = (1-ix_prd)*MAX(ix_l,-ix) + ix_prd*(-n_updt)
!!$        ix_h = (1-ix_prd)*MIN(ix_h,mxy(1)*2**(j-1)-ix-1) + ix_prd*(n_updt - 1)
!!$        IF  (ix_l+ix_h+1 < 0 .AND. ix_h >= 0 .AND. ix_prd == 0) THEN                               ! the right boundary
!!$           ix_l = MAX(ix_l,-(ix_h+1)-n_assym_updt,-ix)
!!$        ELSE IF  (ix_l+ix_h+1 < 0 .AND. ix_h == -1 .AND. ix_prd == 0) THEN                         ! right on the right boundary
!!$           ix_l = MAX(ix_l,-1-n_assym_updt_bnd,-ix)
!!$        ELSE IF  (ix_l+ix_h+1 > 0 .AND. ix_l < 0 .AND. ix_prd == 0) THEN                          !  the left boundary
!!$           ix_h = MIN(ix_h,-ix_l-1+n_assym_updt,mxy(1)*2**(j-1)-ix-1)
!!$        ELSE IF  (ix_l+ix_h+1 > 0 .AND. ix_l == 0 .AND. ix_prd == 0) THEN                         ! right on the left boundary
!!$           ix_h = MIN(ix_h,n_assym_updt_bnd,mxy(1)*2**(j-1)-ix-1)   
!!$        END IF
!!$        DO ixpm = ix_l, ix_h
!!$           wghx_updt(ixpm,ix_even,j)=wgh (ixpm, ix_even, ix_l, ix_h, 2**(j_out-1-j), ix_prd, nxyz_out(1), x_out)
!!$        END DO
!!$     END DO
!!$     DO iy = 1, mxy(2)*2**(j-1)
!!$        iy_odd = (2*iy-1)*2**(j_out-1-j)
!!$        iy_l   = -MIN(iy,n_prdct)
!!$        iy_h   = iy_l+2*n_prdct-1
!!$        iy_h   = MIN(iy_h,mxy(2)*2**(j-1)-iy)
!!$        iy_l   = iy_h-2*n_prdct+1
!!$        iy_l = (1-iy_prd)*MAX(iy_l,-iy) + iy_prd*(-n_prdct)
!!$        iy_h = (1-iy_prd)*MIN(iy_h,mxy(2)*2**(j-1)-iy) + iy_prd*(n_prdct - 1)
!!$        IF  (iy_l+iy_h+1 < 0 .AND. iy_h >= 0 .AND. iy_prd ==0 ) THEN                             ! the right boundary
!!$           iy_l = MAX(iy_l,-(iy_h+1)-n_assym_prdct,-iy)
!!$        ELSE IF  (iy_l+iy_h+1 < 0 .AND. iy_h == -1 .AND. iy_prd ==0 ) THEN                       ! right on the right boundary
!!$           iy_l = MAX(iy_l,-1-n_assym_prdct_bnd,-iy )
!!$        ELSE IF  (iy_l+iy_h+1 > 0 .AND. iy_l < 0 .AND. iy_prd ==0 ) THEN                         !  the left boundary
!!$           iy_h = MIN(iy_h,-iy_l-1+n_assym_prdct,mxy(2)*2**(j-1)-iy )
!!$        ELSE IF  (iy_l+iy_h+1 > 0 .AND. iy_l == 0 .AND. iy_prd ==0 ) THEN                        ! right on the left boundary
!!$           iy_h = MIN(iy_h,n_assym_prdct_bnd,mxy(2)*2**(j-1)-iy)   
!!$        END IF
!!$        DO iypm = iy_l, iy_h
!!$           wghy_prdct(iypm,iy_odd,j)=wgh (iypm, iy_odd, iy_l, iy_h, 2**(j_out-1-j), iy_prd, nxyz(2), y_out)
!!$        END DO
!!$     END DO
!!$     DO iy = 0, mxy(2)*2**(j-1)-iy_prd
!!$        iy_even = iy*2**(j_out-j)
!!$        iy_l = - MIN (iy, n_updt)
!!$        iy_h = iy_l + 2*n_updt - 1
!!$        iy_h = MIN (iy_h, mxy(2)*2**(j-1)-iy-1)
!!$        iy_l = iy_h - 2*n_updt + 1
!!$        iy_l = (1-iy_prd)*MAX(iy_l,-iy) + iy_prd*(-n_updt)
!!$        iy_h = (1-iy_prd)*MIN(iy_h,mxy(2)*2**(j-1)-iy-1) + iy_prd*(n_updt - 1)
!!$        IF  (iy_l+iy_h+1 < 0 .AND. iy_h >= 0 .AND. iy_prd ==0) THEN                               ! the right boundary
!!$           iy_l = MAX(iy_l,-(iy_h+1)-n_assym_updt,-iy)
!!$        ELSE IF  (iy_l+iy_h+1 < 0 .AND. iy_h == -1 .AND. iy_prd ==0) THEN                         ! right on the right boundary
!!$           iy_l = MAX(iy_l,-1-n_assym_updt_bnd,-iy)
!!$        ELSE IF  (iy_l+iy_h+1 > 0 .AND. iy_l < 0  .AND. iy_prd ==0) THEN                          !  the left boundary
!!$           iy_h = MIN(iy_h,-iy_l-1+n_assym_updt,mxy(2)*2**(j-1)-iy-1)
!!$        ELSE IF  (iy_l+iy_h+1 > 0 .AND. iy_l == 0  .AND. iy_prd ==0) THEN                         ! right on the left boundary
!!$           iy_h = MIN(iy_h,n_assym_updt_bnd,mxy(2)*2**(j-1)-iy-1)   
!!$        END IF
!!$        DO iypm = iy_l, iy_h
!!$           wghy_updt(iypm,iy_even,j)=wgh (iypm, iy_even, iy_l, iy_h, 2**(j_out-1-j), iy_prd, nxyz(2), y_out)
!!$        END DO
!!$     END DO
!!$  END DO
 
!TMP  CALL c_wlt_trns (u_out, c(1:nwlt,ie), nxyz_out, nwlt, j_lev, j_out, scl*eps)

  !
  !--------------- Formatted output 
  !
!!$  OPEN(1,FILE='wrk.dat',STATUS='UNKNOWN')
!!$     CALL clean_write (nx_out+1, x_out(0), 1.0e-12_pr)
!!$     CALL clean_write (ny_out+1, y_out(0), 1.0e-12_pr)
!!$     CALL clean_write ((nx_out+1)*(ny_out+1), u_out(0,0), 1.0e-12_pr)
!!$        WRITE (1,'(2(I8,1X))') ibounds(2)-ibounds(1),ibounds(4)-ibounds(3)
!!$        WRITE (1,'(10(E12.5,1X))') x_out(ibounds(1):ibounds(2))
!!$        WRITE (1,'(10(E12.5,1X))') y_out(ibounds(3):ibounds(4))
!!$        WRITE (1,'(10(E12.5,1X))') u_out(ibounds(1):ibounds(2),ibounds(3):ibounds(4))
!!$  CLOSE(1)

  !
  !--------------- Unormatted output of real space data
  !
!!$  OPEN(1,FILE='wrk.dat',STATUS='UNKNOWN',FORM='UNFORMATTED')
!!$     DO idim = 1,dim
!!$        CALL clean_write (nxyz_out(idim)+1, xyz_out(0,idim), 1.0e-12_pr)
!!$     END DO
!!$
!!$     CALL clean_write ((nxyz_out(1)+1)*(nxyz_out(2)+1)*(nxyz_out(3)+1), u_out(0,0,0), 1.0e-12_pr)
!!$        WRITE (1) ibounds(2)-ibounds(1),ibounds(4)-ibounds(3),ibounds(6)-ibounds(5)
!!$        DO idim = 1,dim
!!$           WRITE (1) xyz_out(ibounds(2*idim-1):ibounds(2*idim),idim)
!!$        END DO
!!$
!!$        WRITE (1) u_out(ibounds(1):ibounds(2),ibounds(3):ibounds(4),ibounds(5):ibounds(6))
!!$  CLOSE(1)

  !
  !--------------- Unormatted output of wlt coefficients on full grid (zeros are added 
  ! where there are no wlt coeff's
  !
  !call write_unstructured_vtk( nwlt, nxyz, c, indx,xyz, it,dim ,j_out)

  DO i = 1,dim
     PRINT *, 'Max/Min wlt coefficient value dim=', &
          i, MAXVAL(c(1:nwlt,i)), MINVAL(c(1:nwlt,i))
  END DO
  call write_unstructured_vtk( nwlt, nxyz, SQRT(SUM(c(1:nwlt,1:dim)**2,2)), indx,xyz, it,dim ,j_out)



  DEALLOCATE (c, xyz, u_out, xyz_out ,lv_intrnl, lv_bnd, wgh_updt, wgh_prdct, indx)

END PROGRAM wavelet_interpolation

SUBROUTINE clean_write (n, f, tol)
  USE precision
  IMPLICIT NONE
  INTEGER, INTENT (IN) :: n
  REAL (pr), INTENT (IN) :: tol   
  REAL (pr), DIMENSION (n), INTENT (INOUT) :: f
  
  WHERE (ABS(f) < tol) f = 0.0_pr      
END SUBROUTINE clean_write

!**************************************************************************
! Begin wavelet transform subroutines
! This routine does an inverse wlt tranform from the c's on the
! adapted grid to the full field
!**************************************************************************
SUBROUTINE c_wlt_trns (u_out, c, nxyz,  nwlt, j_in, j_out, eps)
  USE precision
  USE share_vars
  IMPLICIT NONE
  INTEGER, INTENT(IN) :: nwlt, nxyz(1:3),  j_in, j_out
  REAL (pr), DIMENSION (1:nwlt), INTENT (IN) :: c
  REAL (pr), DIMENSION (0:nxyz(1),0:nxyz(2),0:nxyz(3)), INTENT (INOUT) :: u_out
  REAL (pr), INTENT (IN) :: eps
  REAL (pr) :: c_predict
  INTEGER :: i,j,inum,idenom
  INTEGER :: ix,ix_even,ix_odd,ixp,ixpm,ix_l,ix_h
  INTEGER :: iy,iy_even,iy_odd,iyp,iypm,iy_l,iy_h
  INTEGER :: iz,iz_even,iz_odd,izp,izpm,iz_l,iz_h
  INTEGER :: jd

  jd = 0
  IF(dim == 3) jd = 1

  u_out = 0.0_pr
  inum=1
  idenom=1
  IF(j_out > j_in) inum=2**(j_out-j_in)
  IF(j_out < j_in) idenom=2**(j_in-j_out)
  iz = 0
  DO i=1,nwlt
     ix = indx(i,1)
     iy = indx(i,2)
     IF(dim == 3) iz = indx(i,3)
     IF (MOD(ix,idenom)==0 .AND. MOD(iy,idenom)==0 .AND. ABS(c(i)) >= eps ) THEN
        u_out(ix*inum/idenom,iy*inum/idenom,iz*inum/idenom) = c(i)
     END IF
  END DO

  DO j = 1, j_out-1

     IF( dim == 3 ) THEN 
        DO iy=0,mxyz(2)*2**(j_out-1)-prd(2),2**(j_out-j-1)
           DO ix=0,mxyz(1)*2**(j_out-1)-prd(1),2**(j_out-j-1)
              DO iz = 1, mxyz(3)*2**(j-1)
                 iz_even = iz*2**(j_out-j)
                 c_predict = 0.0_pr
                 iz_l = - MIN (iz, n_updt)
                 iz_h = iz_l + 2*n_updt - 1
                 iz_h = MIN (iz_h, mxyz(3)*2**(j-1)-iz-1)
                 iz_l = iz_h - 2*n_updt + 1
                 iz_l = (1-prd(3))*MAX(iz_l,-iz) + prd(3)*(-n_updt)
                 iz_h = (1-prd(3))*MIN(iz_h,mxyz(3)*2**(j-1)-iz-1) + prd(3)*(n_updt - 1)
                 IF  (iz_l+iz_h+1 < 0 .AND. iz_h >= 0 .AND. prd(3) == 0) THEN                               ! the right boundary
                    iz_l = MAX(iz_l,-(iz_h+1)-n_assym_updt,-iz)
                 ELSE IF  (iz_l+iz_h+1 < 0 .AND. iz_h == -1 .AND. prd(3) == 0) THEN                         ! right on the right boundary
                    iz_l = MAX(iz_l,-1-n_assym_updt_bnd,-iz)
                 ELSE IF  (iz_l+iz_h+1 > 0 .AND. iz_l < 0 .AND. prd(3) == 0) THEN                          !  the left boundary
                    iz_h = MIN(iz_h,-iz_l-1+n_assym_updt,mxyz(3)*2**(j-1)-iz-1)
                 ELSE IF  (iz_l+iz_h+1 > 0 .AND. iz_l == 0 .AND. prd(3) == 0) THEN                         ! right on the left boundary
                    iz_h = MIN(iz_h,n_assym_updt_bnd,mxyz(3)*2**(j-1)-iz-1)   
                 END IF
                 DO izpm = iz_l, iz_h
                    izp = (2*(iz+izpm)+1)*2**(j_out-1-j)
                    izp = (1-prd(3))*izp+prd(3)*MOD(izp+3*nxyz(3),nxyz(3))
                    c_predict = c_predict + wgh_updt(izpm,iz_even,j,3) * u_out(ix,iy,izp)
                 END DO
                 u_out(ix,iy,iz_even) = u_out(ix,iy,iz_even) - c_predict
              END DO
              DO iz = 1, mxyz(3)*2**(j-1)
                 iz_odd = (2*iz-1)*2**(j_out-1-j)
                 iz_l   = -MIN(iz,n_prdct)
                 iz_h   = iz_l+2*n_prdct-1
                 iz_h   = MIN(iz_h,mxyz(3)*2**(j-1)-iz)
                 iz_l   = iz_h-2*n_prdct+1
                 iz_l = (1-prd(3))*MAX(iz_l,-iz) + prd(3)*(-n_prdct)
                 iz_h = (1-prd(3))*MIN(iz_h,mxyz(3)*2**(j-1)-iz) + prd(3)*(n_prdct - 1)
                 IF  (iz_l+iz_h+1 < 0 .AND. iz_h >= 0 .AND. prd(3) ==0 ) THEN                             ! the right boundary
                    iz_l = MAX(iz_l,-(iz_h+1)-n_assym_prdct,-iz)
                 ELSE IF  (iz_l+iz_h+1 < 0 .AND. iz_h == -1 .AND. prd(3) ==0 ) THEN                       ! right on the right boundary
                    iz_l = MAX(iz_l,-1-n_assym_prdct_bnd,-iz )
                 ELSE IF  (iz_l+iz_h+1 > 0 .AND. iz_l < 0 .AND. prd(3) ==0 ) THEN                         !  the left boundary
                    iz_h = MIN(iz_h,-iz_l-1+n_assym_prdct,mxyz(3)*2**(j-1)-iz )
                 ELSE IF  (iz_l+iz_h+1 > 0 .AND. iz_l == 0 .AND. prd(3) ==0 ) THEN                        ! right on the left boundary
                    iz_h = MIN(iz_h,n_assym_prdct_bnd,mxyz(3)*2**(j-1)-iz)   
                 END IF
                 c_predict = 0.0_pr
                 DO izpm = iz_l, iz_h
                    izp = (iz+izpm)*2**(j_out-j)
                    izp = (1-prd(3))*izp+prd(3)*MOD(izp+3*nxyz(3),nxyz(3))
                    c_predict = c_predict + wgh_prdct(izpm,iz_odd,j,3) * u_out(ix,iy,izp)
                 END DO
                 u_out(ix,iy,iz_odd) = 2.0_pr*u_out(ix,iy,iz_odd) + c_predict

              END DO
           END DO
        END DO
     END IF
!
!*********** transform in y-direction *********************
!        
     DO iz=0,mxyz(3)*2**(jd*(j_out-1))-prd(3),2**(j_out-j-1)
        DO ix=0,mxyz(1)*2**(j_out-1)-prd(1),2**(j_out-j-1)
           DO iy = 0, mxyz(2)*2**(j-1)-prd(2)
              iy_even = iy*2**(j_out-j)
              c_predict = 0.0_pr
              iy_l = - MIN (iy, n_updt)
              iy_h = iy_l + 2*n_updt - 1
              iy_h = MIN (iy_h, mxyz(2)*2**(j-1)-iy-1)
              iy_l = iy_h - 2*n_updt + 1
              iy_l = (1-prd(2))*MAX(iy_l,-iy) + prd(2)*(-n_updt)
              iy_h = (1-prd(2))*MIN(iy_h,mxyz(2)*2**(j-1)-iy-1) + prd(2)*(n_updt - 1)
              IF  (iy_l+iy_h+1 < 0 .AND. iy_h >= 0 .AND. prd(2) ==0) THEN                               ! the right boundary
                 iy_l = MAX(iy_l,-(iy_h+1)-n_assym_updt,-iy)
              ELSE IF  (iy_l+iy_h+1 < 0 .AND. iy_h == -1 .AND. prd(2) ==0) THEN                         ! right on the right boundary
                 iy_l = MAX(iy_l,-1-n_assym_updt_bnd,-iy)
              ELSE IF  (iy_l+iy_h+1 > 0 .AND. iy_l < 0  .AND. prd(2) ==0) THEN                          !  the left boundary
                 iy_h = MIN(iy_h,-iy_l-1+n_assym_updt,mxyz(2)*2**(j-1)-iy-1)
              ELSE IF  (iy_l+iy_h+1 > 0 .AND. iy_l == 0  .AND. prd(2) ==0) THEN                         ! right on the left boundary
                 iy_h = MIN(iy_h,n_assym_updt_bnd,mxyz(2)*2**(j-1)-iy-1)   
              END IF
              DO iypm = iy_l, iy_h
                 iyp = (2*(iy+iypm)+1)*2**(j_out-1-j)
                 iyp = (1-prd(2))*iyp+prd(2)*MOD(iyp+3*nxyz(2),nxyz(2))
                 c_predict = c_predict + wgh_updt(iypm,iy_even,j,2) * u_out(ix,iyp,iz)
              END DO
              u_out(ix,iy_even,iz) = u_out(ix,iy_even,iz) - c_predict
           END DO
           DO iy = 1, mxyz(2)*2**(j-1)
              iy_odd = (2*iy-1)*2**(j_out-1-j)
              iy_l   = -MIN(iy,n_prdct)
              iy_h   = iy_l+2*n_prdct-1
              iy_h   = MIN(iy_h,mxyz(2)*2**(j-1)-iy)
              iy_l   = iy_h-2*n_prdct+1
              iy_l = (1-prd(2))*MAX(iy_l,-iy) + prd(2)*(-n_prdct)
              iy_h = (1-prd(2))*MIN(iy_h,mxyz(2)*2**(j-1)-iy) + prd(2)*(n_prdct - 1)
              IF  (iy_l+iy_h+1 < 0 .AND. iy_h >= 0 .AND. prd(2) ==0 ) THEN                             ! the right boundary
                 iy_l = MAX(iy_l,-(iy_h+1)-n_assym_prdct,-iy)
              ELSE IF  (iy_l+iy_h+1 < 0 .AND. iy_h == -1 .AND. prd(2) ==0 ) THEN                       ! right on the right boundary
                 iy_l = MAX(iy_l,-1-n_assym_prdct_bnd,-iy )
              ELSE IF  (iy_l+iy_h+1 > 0 .AND. iy_l < 0 .AND. prd(2) ==0 ) THEN                         !  the left boundary
                 iy_h = MIN(iy_h,-iy_l-1+n_assym_prdct,mxyz(2)*2**(j-1)-iy )
              ELSE IF  (iy_l+iy_h+1 > 0 .AND. iy_l == 0 .AND. prd(2) ==0 ) THEN                        ! right on the left boundary
                 iy_h = MIN(iy_h,n_assym_prdct_bnd,mxyz(2)*2**(j-1)-iy)   
              END IF
              c_predict = 0.0_pr
              DO iypm = iy_l, iy_h
                 iyp = (iy+iypm)*2**(j_out-j)
                 iyp = (1-prd(2))*iyp+prd(2)*MOD(iyp+3*nxyz(2),nxyz(2))
                 c_predict = c_predict + wgh_prdct(iypm,iy_odd,j,2) * u_out(ix,iyp,iz)
              END DO
              u_out(ix,iy_odd,iz) = 2.0_pr*u_out(ix,iy_odd,iz) + c_predict
           END DO
        END DO
     END DO
!
!*********** transform in x-direction *********************
!        
     DO iz=0,mxyz(3)*2**(jd*(j_out-1))-prd(3),2**(j_out-j-1)
        DO iy=0,mxyz(2)*2**(j_out-1)-prd(2),2**(j_out-j-1)
           DO ix = 0, mxyz(1)*2**(j-1)-prd(1)
              ix_even = ix*2**(j_out-j)
              c_predict = 0.0_pr
              ix_l = - MIN (ix, n_updt)
              ix_h = ix_l + 2*n_updt - 1
              ix_h = MIN (ix_h, mxyz(1)*2**(j-1)-ix-1)
              ix_l = ix_h - 2*n_updt + 1
              ix_l = (1-prd(1))*MAX(ix_l,-ix) + prd(1)*(-n_updt)
              ix_h = (1-prd(1))*MIN(ix_h,mxyz(1)*2**(j-1)-ix-1) + prd(1)*(n_updt - 1)
              IF  (ix_l+ix_h+1 < 0 .AND. ix_h >= 0 .AND. prd(1) == 0) THEN                               ! the right boundary
                 ix_l = MAX(ix_l,-(ix_h+1)-n_assym_updt,-ix)
              ELSE IF  (ix_l+ix_h+1 < 0 .AND. ix_h == -1 .AND. prd(1) == 0) THEN                         ! right on the right boundary
                 ix_l = MAX(ix_l,-1-n_assym_updt_bnd,-ix)
              ELSE IF  (ix_l+ix_h+1 > 0 .AND. ix_l < 0 .AND. prd(1) == 0) THEN                          !  the left boundary
                 ix_h = MIN(ix_h,-ix_l-1+n_assym_updt,mxyz(1)*2**(j-1)-ix-1)
              ELSE IF  (ix_l+ix_h+1 > 0 .AND. ix_l == 0 .AND. prd(1) == 0) THEN                         ! right on the left boundary
                 ix_h = MIN(ix_h,n_assym_updt_bnd,mxyz(1)*2**(j-1)-ix-1)   
              END IF
              DO ixpm = ix_l, ix_h
                 ixp = (2*(ix+ixpm)+1)*2**(j_out-1-j)
                 ixp = (1-prd(1))*ixp+prd(1)*MOD(ixp+3*nxyz(1),nxyz(1))
                 c_predict = c_predict + wgh_updt(ixpm,ix_even,j,1) * u_out(ixp,iy,iz)
              END DO
              u_out(ix_even,iy,iz) = u_out(ix_even,iy,iz) - c_predict
           END DO
           DO ix = 1, mxyz(1)*2**(j-1)
              ix_odd = (2*ix-1)*2**(j_out-1-j)
              ix_l   = -MIN(ix,n_prdct)
              ix_h   = ix_l+2*n_prdct-1
              ix_h   = MIN(ix_h,mxyz(1)*2**(j-1)-ix)
              ix_l   = ix_h-2*n_prdct+1
              ix_l = (1-prd(1))*MAX(ix_l,-ix) + prd(1)*(-n_prdct)
              ix_h = (1-prd(1))*MIN(ix_h,mxyz(1)*2**(j-1)-ix) + prd(1)*(n_prdct - 1)
              IF  (ix_l+ix_h+1 < 0 .AND. ix_h >= 0 .AND. prd(1) ==0 ) THEN                             ! the right boundary
                 ix_l = MAX(ix_l,-(ix_h+1)-n_assym_prdct,-ix)
              ELSE IF  (ix_l+ix_h+1 < 0 .AND. ix_h == -1 .AND. prd(1) ==0 ) THEN                       ! right on the right boundary
                 ix_l = MAX(ix_l,-1-n_assym_prdct_bnd,-ix )
              ELSE IF  (ix_l+ix_h+1 > 0 .AND. ix_l < 0 .AND. prd(1) ==0 ) THEN                         !  the left boundary
                 ix_h = MIN(ix_h,-ix_l-1+n_assym_prdct,mxyz(1)*2**(j-1)-ix )
              ELSE IF  (ix_l+ix_h+1 > 0 .AND. ix_l == 0 .AND. prd(1) ==0 ) THEN                        ! right on the left boundary
                 ix_h = MIN(ix_h,n_assym_prdct_bnd,mxyz(1)*2**(j-1)-ix)   
              END IF
              c_predict = 0.0_pr
              DO ixpm = ix_l, ix_h
                 ixp = (ix+ixpm)*2**(j_out-j)
                 ixp = (1-prd(1))*ixp+prd(1)*MOD(ixp+3*nxyz(1),nxyz(1))
                 c_predict = c_predict + wgh_prdct(ixpm,ix_odd,j,1) * u_out(ixp,iy,iz)
              END DO
              u_out(ix_odd,iy,iz) = 2.0_pr*u_out(ix_odd,iy,iz) + c_predict
           END DO
        END DO
     END DO
  END DO


  IF( prd(1) == 1 ) u_out(nxyz(1),:,:)=u_out(0,:,:);
  IF( prd(2) == 1 ) u_out(:,nxyz(2),:)=u_out(:,0,:); 
  IF( prd(3) == 1 ) u_out(:,:,nxyz(3))=u_out(:,:,0); 

END SUBROUTINE c_wlt_trns

!
!*************************************************************
!
FUNCTION wgh (k, i, i_l, i_h, istep, i_prd, nx, x)
  USE precision
  IMPLICIT NONE
  INTEGER, INTENT (IN) :: k, i, i_l, i_h, istep, i_prd, nx
  REAL (pr), DIMENSION(0:nx), INTENT (IN) :: x
  REAL (pr) :: wgh
  INTEGER :: iprd1, iprd2, l, ip, ipm, kp
  REAL (pr) :: prd1, prd2

  IF(i_prd == 1) THEN
     iprd1 = 1
     iprd2 = 1
     DO l = i_l, k-1
        iprd1 = iprd1*(2*l+1)
        iprd2 = iprd2*2*(l-k)
     END DO
     DO l = k+1, i_h
        iprd1 = iprd1*(2*l+1)
        iprd2 = iprd2*2*(l-k)
     END DO
     wgh = REAL (iprd1, pr) / REAL (iprd2, pr)
  ELSE
     prd1 = 1.0_pr
     prd2 = 1.0_pr
     kp = i+(2*k+1)*istep
     DO ipm = i_l, k-1
        ip = i+(2*ipm+1)*istep
        prd1 = prd1*(x(i)-x(ip))
        prd2 = prd2*(x(kp)-x(ip))
     END DO
     DO ipm = k+1,i_h
        ip = i+(2*ipm+1)*istep
        prd1 = prd1*(x(i)-x(ip))
        prd2 = prd2*(x(kp)-x(ip))
     END DO
     wgh = prd1/prd2
  END IF
END FUNCTION wgh

!--************************************************************************
!--End input/output routines
!--************************************************************************


SUBROUTINE write_unstructured_vtk( nwlt, nxyz, c, indx, xyz,it,dim ,j_out)
  USE precision
  IMPLICIT NONE
  INTEGER  , INTENT(IN) :: nwlt, nxyz(1:3)
  REAL (pr), DIMENSION (1:nwlt), INTENT (IN) :: c
  INTEGER, DIMENSION (1:nwlt,1:3), INTENT(IN) :: indx
  REAL (pr), DIMENSION (0:MAXVAL(nxyz),1:dim) , INTENT(IN) :: xyz
  INTEGER  ,INTENT(IN) :: it,dim, j_out
  CHARACTER (LEN=3) :: tmpch
  INTEGER   :: i

  INTERFACE
  PURE FUNCTION ij2j (ij, j)
       IMPLICIT NONE 
       INTEGER, INTENT (IN) :: ij, j
       INTEGER :: ij2j
     END FUNCTION ij2j
  END INTERFACE

  PRINT *, 'Writing ', nwlt ,'ABS(wlt coefficents) to vtk file'
  PRINT *, 'Max/Min wlt coefficient value', MAXVAL(c), MINVAL(c)

  
  WRITE(tmpch,FMT='(i3.3)' )  it
  OPEN(1,FILE='results/cwrk.dat.'//tmpch//'.vtk',STATUS='UNKNOWN',FORM='FORMATTED')
        !WRITE (1) u_out(ibounds(1):ibounds(2),ibounds(3):ibounds(4),ibounds(5):ibounds(6))
    WRITE (1,'( "# vtk DataFile Version 3.0" )' )
    WRITE (1,'( "vtk output" )' )
    WRITE (1,'( "ASCII" )' )
    WRITE (1,'( "DATASET UNSTRUCTURED_GRID" )' )
    WRITE (1,'( "POINTS ", i ," float" )' ) nwlt
    DO i=1,nwlt
           WRITE (1,'(3( E12.5,1X))') &
                xyz(indx(i,1),1), xyz(indx(i,2),2),xyz(indx(i,3),3)
    END DO

    WRITE (1,'( "" )' )
    WRITE (1,'( "CELLS "  ,2(i , 1X) )' ) nwlt , 2*nwlt
    DO i=1,nwlt
           WRITE (1,'(2( i,1X))') &
                1, i-1
    END DO

    WRITE (1,'( "" )' )
    WRITE (1,'( "CELL_TYPES "  , i )' ) nwlt
    DO i=1,nwlt
           WRITE (1,'("1")') 
    END DO

!!$    WRITE (1,'( "" )' )
!!$    WRITE (1,'( "CELL_DATA ", i )' ) nwlt
!!$    WRITE (1,'( "SCALARS scalars float" )' )
!!$    WRITE (1,'( "LOOKUP_TABLE default" )' )
!!$    DO i=1,nwlt
!!$           WRITE (1,'( E12.5 )') c(i)
!!$    END DO

    WRITE (1,'( "" )' )
    WRITE (1,'( "POINT_DATA ", i )' ) nwlt
    WRITE (1,'( "SCALARS abs_wlt_coefficent float 1" )' )
    WRITE (1,'( "LOOKUP_TABLE default" )' )
    DO i=1,nwlt
           WRITE (1,'( E12.5 )') ABS(c(i)) 
    END DO

!!$    WRITE (1,'( "" )' )
!!$    WRITE (1,'( "POINT_DATA ", i )' ) nwlt
!!$    WRITE (1,'( "SCALARS level float 1" )' )
!!$    WRITE (1,'( "LOOKUP_TABLE default" )' )
!!$    DO i=1,nwlt
!!$           WRITE (1,'( i)') &
!!$                MAX( ij2j(indx(i,1),j_out), ij2j(indx(i,2),j_out) , ij2j(indx(i,3),j_out))
!!$    END DO

  CLOSE(1)


END SUBROUTINE  write_unstructured_vtk

PURE FUNCTION ij2j (ij, j)
  USE precision
  USE share_vars
  IMPLICIT NONE 
  INTEGER, INTENT (IN) :: ij, j
  INTEGER :: ij2j
  INTEGER :: k
  k = 0
  DO WHILE (MOD(ij+2**k, 2**(k+1)) /= 0 .AND. k < j-1)
     k = k + 1
  END DO
  ij2j = j-k
END FUNCTION ij2j
