!
! write raw data into a file to be read 
! data is written as vectors
!
MODULE write_field_mod
  USE precision
  USE share_consts
  USE pde
  USE sizes
!  USE wlt_trns_util_mod

CONTAINS 
 


! 
!     ------------ 
!
!     write  data field
! 
!     ------------ 
! 
      subroutine write_field(u, n1, n2, n3,filename) 

        implicit none 

       
        integer n1, n2, n3 
        real (pr) u(1:3,n1,n2,n3) 


        character (*) filename
        real (pr) the_viscosity

        
        integer i, j, k
        integer IUNIT  

	    IUNIT = 200



        !write field
        write(*,*) 'write_field(): File opened to write output field:',filename
        write(*,*)  TRIM(filename)//'.raw'

        open(unit=IUNIT, FILE=TRIM(filename)//'.raw', FORM='unformatted')
        WRITE(IUNIT) u
        close(IUNIT)
	    write(*,*) 'End  writing output field:', TRIM(filename)//'.raw'


        
      end subroutine write_field

END MODULE write_field_mod

