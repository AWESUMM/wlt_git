#include <stdio.h>
#include <string.h>
#include "vtkFloatArray.h"
#include "vtkMath.h"
#include "vtkPointData.h"
#include "vtkPoints.h"
#include "vtkUnstructuredGrid.h"
#include "vtkUnstructuredGridWriter.h"
#include "vtkPolyData.h"
#include "vtkPolygon.h"
#include "vtkHexahedron.h"
#include "vtkCellArray.h"
#include "vtkCellTypes.h"
#include "vtkIdList.h"
#include "vtkIdTypeArray.h"
#include <vtkDelaunay3D.h>
#include <vtkDelaunay2D.h>
#include <vtkAppendFilter.h>
#include <vtkXMLUnstructuredGridWriter.h>


/*extern "C" 
{

void  __stdcall WRITE_VTK_WDELAUNAY(const int *Dim, const int * Max_nxyz, const int *Npoints, const int *Nwlt,
				 const int *Numveloc_scalars, const int *indx, const int *active_pts, 
				 const double *xyz, const double *c, const double *vort_mag, 
				 const int *Output_vort_mag, const double *ij2j, char *file_name,
				 const int File_name_len);
}
*/

// compile with # g++ -Wno-deprecated vtkXMLWriter.cxx -I/sw/include/vtk -L/sw/lib/vtk/ -lvtkCommon -lvtkFiltering -lvtkIO -lvtkGraphics


/*
  
	ARGS
   In Fortan
   indx(1:nwlt,1:dim) 
   active_pts(1:nwlt)
   xyz(1:nwlt,1:dim)
   c(1:nwlt,1:Numveloc_scalars)
   ij2j(0:max_nxyz)

  We will access 2D arrays as flat arrays

  Example:
  Fortran: x(1:maxx,1:maxy) so we index x(i,j)
  C      : x[ i + j * maxx] 

*/
extern "C" 
{
#ifdef WIN32
void  __stdcall WRITE_VTK_WDELAUNAY
#else
void write_vtk_wdelaunay_
#endif 
(const int *Dim, const int * Max_nxyz, const int *Npoints, const int *Nwlt,
				 const int *Numveloc_scalars, const int *indx, const int *active_pts, 
				 const double *xyz, const double *c, const double *vort_mag, 
				 const int *Output_vort_mag, const double *ij2j, char *file_name,
				 const int File_name_len)
{
 int npoints = *Npoints;
 int numveloc_scalars = *Numveloc_scalars;
 int nwlt = *Nwlt;
 int dim = *Dim;
 int max_nxyz = *Max_nxyz;
 int output_vort_mag = *Output_vort_mag; // if != 0 then output the vorticity magnitude

 int if3d = 1; //tmp force 3D
 printf("npoints %d, numveloc_scalars %d\n", npoints, numveloc_scalars);
 
// Define structured Grid
 vtkUnstructuredGrid *ugrid = vtkUnstructuredGrid::New();
 vtkCellArray* polygons = vtkCellArray::New();
 vtkPolygon *polygon = vtkPolygon::New();
	
// Set Points in Grid
vtkPoints *points = vtkPoints::New();
points->Allocate(npoints);
//printf("Allocated Points.\n");

// Set Point veloc_scalars
vtkFloatArray *veloc_scalars = vtkFloatArray::New();
veloc_scalars->SetNumberOfComponents(numveloc_scalars);
veloc_scalars->SetNumberOfTuples(npoints); // !!!!!?6? sides for 3d elements?
veloc_scalars->SetName("Velocity");

// Set Point veloc_scalars
vtkFloatArray *vort_mag_scalars = vtkFloatArray::New();
vort_mag_scalars->SetNumberOfComponents(1);
vort_mag_scalars->SetNumberOfTuples(npoints); // !!!!!?6? sides for 3d elements?
vort_mag_scalars->SetName("Vorticity Magnitude");

int count=0;
float * tempscalar = (float *)malloc(numveloc_scalars*sizeof(float));
//printf("Inserting points...\n");
for(int i=0; i<npoints; i++){
	points->InsertPoint(count,   xyz[ indx[ active_pts[i]-1 + 0*nwlt ] + 0*(max_nxyz+1)],
		                         xyz[ indx[ active_pts[i]-1 + 1*nwlt ] + 1*(max_nxyz+1)],
								 xyz[ indx[ active_pts[i]-1 + 2*nwlt ] + 2*(max_nxyz+1)]);
//	printf("indx %d %d %d loc %f %f %f \n", indx[ active_pts[i]-1 + 0*nwlt ],indx[ active_pts[i]-1 + 1*nwlt ],
//		        indx[ active_pts[i]-1 + 2*nwlt ], xyz[ indx[ active_pts[i]-1 + 0*nwlt ] + 0*(max_nxyz+1)],
//                xyz[ indx[ active_pts[i]-1 + 1*nwlt ] + 1*(max_nxyz+1)], xyz[ indx[ active_pts[i]-1 + 2*nwlt ] + 2*(max_nxyz+1)]);
		for(int ii=0; ii<numveloc_scalars; ii++){
			tempscalar[ii] = c[active_pts[i]-1 + ii*nwlt];
			//printf("vals %e \n", tempscalar[ii] );
		}
		veloc_scalars->InsertTuple(count, tempscalar); // xyz[4][], xyz[5][]......
        tempscalar[0] = vort_mag[count];
		vort_mag_scalars->InsertTuple(count, tempscalar);
		count++;
}

ugrid->SetPoints(points);
ugrid->GetPointData()->SetScalars(veloc_scalars);
ugrid->GetPointData()->AddArray(vort_mag_scalars);

if(if3d){
	printf("Starting Delaunay3d...");
	vtkDelaunay3D* delny;
	delny = vtkDelaunay3D::New();
	delny->SetInput(ugrid);
	delny->SetTolerance(0.01);
	//delny->SetAlpha(0.2);
	delny->BoundingTriangulationOff();
	ugrid = delny->GetOutput();
	printf("....done.\n");
	}
 else{
	printf("Starting Delaunay2d...");
	vtkDelaunay2D* delny;
	delny = vtkDelaunay2D::New();
	delny->SetInput(ugrid);
	delny->SetTolerance(0.01);
	//delny->SetAlpha(0.2);
	delny->BoundingTriangulationOff();
	vtkAppendFilter* append;
	append = vtkAppendFilter::New();
	append->AddInput(delny->GetOutput());
	ugrid = append->GetOutput();
	printf("....done.\n");
}
 
points->Delete();
veloc_scalars->Delete();
vort_mag_scalars->Delete();
 
 
 // There is a bug/problem with the legacy vtk writer on windows...
 //printf("Writing testunstruct.vtk\n");
 //vtkUnstructuredGridWriter* writer;
 //writer = vtkUnstructuredGridWriter::New();
 //writer->SetFileName( "C:\dan\research\viz\tst669.gdm\testunstruct.vtk" );
 //writer->SetInput(ugrid);
 //writer->Write();
 //writer->Delete();
 
 printf("Writing testunstruct.vtu\n");
 vtkXMLUnstructuredGridWriter* xmlwriter;
 xmlwriter = vtkXMLUnstructuredGridWriter::New();
 xmlwriter->SetFileName(file_name);
 xmlwriter->SetInput(ugrid);
 xmlwriter->Write();
 xmlwriter->Delete();
 ugrid->Delete();



 return;
}
}

/*

/* END Subroutine prepostvtk, by Kadlec
/*-------------------------------------------------------------------------- 


/*Main subroutine to drive prepostvtk. Comment out if calling prepostvtk through fortran.
int main(int argc, char *argv[]){
	
	int nptx=6;
	int npty=6;
	int nptz=6;
	int ID = 6+1;
	int npoints = (nptx)*(npty)*(nptz);
	float *xyz;
	if( (xyz = (float*) malloc((sizeof(float)*npoints)*(sizeof(float)*ID))) == NULL)
		perror("Error allocating memory for xyz!");

	for(int i=0; i<npoints; i++){
		xyz[i] = i%nptx;  
		xyz[i+(npoints)] = (int)(i/npty)%(nptx); 
		xyz[i+2*(npoints)] = (int)(i/(nptx*npty));
		for(int ii=0; ii<ID-3; ii++){
			xyz[i+(ii+3)*(npoints)] = i%nptx;
		}
	}
	
	printf("Calling prepostvtk_()\n");
	
	int * nptsx = &nptx;
	int * nptsy = &npty;
	int * nptsz = &nptz;
	int if3d = 1;
	int numcomponents = 1;
	prepostvtk_(&if3d, nptsx, nptsy, nptsz, &numcomponents, xyz);
	return 0;
	}
	
	*/																			
//}

