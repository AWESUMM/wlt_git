
MODULE write_vtk_mod

CONTAINS

!
!  WRITE an old style vtk data format file of adaptive points
!
! active points is a map into the flat arrays (c,indx,xx) of the n_active_pts that are active.
!
SUBROUTINE write_unstructured_vtk( active_pts, n_active_pts, nwlt_loc, nxyz_out, max_nxyz, c, indx, xyz,it,dim_loc ,j_out,filename,field_label)
  USE precision
  USE wlt_trns_util_mod
  IMPLICIT NONE
  INTEGER  ,                                   INTENT(IN) ::  nwlt_loc, nxyz_out(1:3)
  INTEGER  ,                                   INTENT(IN) ::  n_active_pts, active_pts(nwlt_loc)
  INTEGER  ,                                   INTENT(IN) :: it,dim_loc, j_out
  REAL (pr), DIMENSION (1:nwlt_loc,1:dim_loc),         INTENT(IN) :: c
  INTEGER  , DIMENSION (1:nwlt_loc,1:3),           INTENT(IN) :: indx
  INTEGER  ,                                   INTENT(IN) :: max_nxyz 
  REAL (pr), DIMENSION (0:max_nxyz,1:dim_loc) , INTENT(IN) :: xyz
  CHARACTER(*),                                   INTENT(IN) :: filename
  CHARACTER(*),                                   INTENT(IN) :: field_label

  CHARACTER (LEN=3)   :: tmpch
  CHARACTER (LEN=256) :: file_name_written
  INTEGER             :: i,j,k
  INTEGER             :: IUNIT  

  IUNIT = 200

  



    write(*,*) 'write_unstructured_vtk(): File opened to write output field:'
    PRINT *, 'Writing ', nwlt_loc ,'wlt coefficents to vtk file'
	file_name_written = TRIM(filename)//"_"//TRIM(field_label)//'.vtk'
    write(*,*)  TRIM(file_name_written)
    open(unit=IUNIT, FILE=TRIM(file_name_written), FORM='formatted',RECL=1000)

        !WRITE (1) u_out(ibounds(1):ibounds(2),ibounds(3):ibounds(4),ibounds(5):ibounds(6))
    WRITE (IUNIT,'( "# vtk DataFile Version 3.0" )' )
    WRITE (IUNIT,'( "vtk output" )' )
    WRITE (IUNIT,'( "ASCII" )' )
 
 
    WRITE (IUNIT,'( "DATASET UNSTRUCTURED_GRID" )' )
    WRITE (IUNIT,'( "POINTS ", i12 ," float" )' ) n_active_pts
    DO i=1,n_active_pts
           WRITE (IUNIT,'(3( E12.5,1X))') &
                xyz(indx(active_pts(i),1),1), xyz(indx(active_pts(i),2),2),xyz(indx(active_pts(i),3),3)
    END DO

    !WRITE (1,'( " " )' )
    !WRITE (1,'( "CELLS "  ,2(i , 1X) )' ) nwlt_loc , 2*nwlt_loc
    !DO i=1,nwlt_loc
    !       WRITE (1,'(2( i,1X))') &
    !            1, i-1
    !END DO

    !WRITE (1,'( " " )' )
    !WRITE (1,'( "CELL_TYPES "  , i12 )' ) nwlt_loc
    !DO i=1,nwlt_loc
    !       WRITE (1,'("1")') 
    !END DO

!!$    WRITE (1,'( " " )' )
!!$    WRITE (1,'( "CELL_DATA ", i12 )' ) nwlt_loc
!!$    WRITE (1,'( "SCALARS scalars float" )' )
!!$    WRITE (1,'( "LOOKUP_TABLE default" )' )
!!$    DO i=1,nwlt_loc
!!$           WRITE (1,'( E12.5 )') c(i)
!!$    END DO

    WRITE (IUNIT,'( " " )' )
    WRITE (IUNIT,'( "POINT_DATA ", i12 )' )  n_active_pts

    WRITE (IUNIT,'( "SCALARS " , A , " float 3" )' ) field_label
    WRITE (IUNIT,'( "LOOKUP_TABLE default" )' )
    DO i=1,n_active_pts
           WRITE (IUNIT,'( 3(E12.5, " ") )') c(active_pts(i),:) 
    END DO

    WRITE (IUNIT,'( " " )' )

    WRITE (IUNIT,'( "SCALARS max_level float 1" )' )
    WRITE (IUNIT,'( "LOOKUP_TABLE default" )' )
    DO i=1,n_active_pts
           WRITE (IUNIT,'( i12 )') &
                MAX( ij2j(indx(active_pts(i),1),j_out), ij2j(indx(active_pts(i),2),j_out) , ij2j(indx(active_pts(i),3),j_out))
    END DO



  CLOSE(IUNIT)


END SUBROUTINE  write_unstructured_vtk

!
!  WRITE an old style vtk data format file of adaptive points
! thsi just writes one scalar variable
!
! active points is a map into the flat arrays (c,indx,xx) of the n_active_pts that are active.
!
SUBROUTINE write_unstructured_vtk_scalar( active_pts, n_active_pts, nwlt_loc, nxyz_out, max_nxyz, c, indx, xyz,it,dim_loc ,j_out,filename,field_label)
  USE precision
  USE wlt_trns_util_mod
  IMPLICIT NONE
  INTEGER  ,                                   INTENT(IN) ::  nwlt_loc, nxyz_out(1:3)
  INTEGER  ,                                   INTENT(IN) ::  n_active_pts, active_pts(nwlt_loc)
  INTEGER  ,                                   INTENT(IN) :: it,dim_loc, j_out
  REAL (pr), DIMENSION (1:nwlt_loc),           INTENT(IN) :: c
  INTEGER  , DIMENSION (1:nwlt_loc,1:3),           INTENT(IN) :: indx
  INTEGER  ,                                   INTENT(IN) :: max_nxyz 
  REAL (pr), DIMENSION (0:max_nxyz,1:dim_loc) , INTENT(IN) :: xyz
  CHARACTER(*),                                   INTENT(IN) :: filename
  CHARACTER(*),                                   INTENT(IN) :: field_label

  CHARACTER (LEN=3)   :: tmpch
  CHARACTER (LEN=256) :: file_name_written
  INTEGER             :: i,j,k
  INTEGER             :: IUNIT  

  IUNIT = 200

  



    write(*,*) 'write_unstructured_vtk(): File opened to write output field:'
    PRINT *, 'Writing ', nwlt_loc ,'wlt coefficents to vtk file'
	file_name_written = TRIM(filename)//"_"//TRIM(field_label)//'.vtk'
    write(*,*)  TRIM(file_name_written)
    open(unit=IUNIT, FILE=TRIM(file_name_written), FORM='formatted',RECL=1000)

        !WRITE (1) u_out(ibounds(1):ibounds(2),ibounds(3):ibounds(4),ibounds(5):ibounds(6))
    WRITE (IUNIT,'( "# vtk DataFile Version 3.0" )' )
    WRITE (IUNIT,'( "vtk output" )' )
    WRITE (IUNIT,'( "ASCII" )' )
 
 
    WRITE (IUNIT,'( "DATASET UNSTRUCTURED_GRID" )' )
    WRITE (IUNIT,'( "POINTS ", i12 ," float" )' ) n_active_pts
    DO i=1,n_active_pts
           WRITE (IUNIT,'(3( E12.5,1X))') &
                xyz(indx(active_pts(i),1),1), xyz(indx(active_pts(i),2),2),xyz(indx(active_pts(i),3),3)
    END DO

    !WRITE (1,'( " " )' )
    !WRITE (1,'( "CELLS "  ,2(i , 1X) )' ) nwlt_loc , 2*nwlt_loc
    !DO i=1,nwlt_loc
    !       WRITE (1,'(2( i,1X))') &
    !            1, i-1
    !END DO

    !WRITE (1,'( " " )' )
    !WRITE (1,'( "CELL_TYPES "  , i12 )' ) nwlt_loc
    !DO i=1,nwlt_loc
    !       WRITE (1,'("1")') 
    !END DO

!!$    WRITE (1,'( " " )' )
!!$    WRITE (1,'( "CELL_DATA ", i12 )' ) nwlt_loc
!!$    WRITE (1,'( "SCALARS scalars float" )' )
!!$    WRITE (1,'( "LOOKUP_TABLE default" )' )
!!$    DO i=1,nwlt_loc
!!$           WRITE (1,'( E12.5 )') c(i)
!!$    END DO

    WRITE (IUNIT,'( " " )' )
    WRITE (IUNIT,'( "POINT_DATA ", i12 )' )  n_active_pts

    WRITE (IUNIT,'( "SCALARS " , A , " float 1" )' ) field_label
    WRITE (IUNIT,'( "LOOKUP_TABLE default" )' )
    DO i=1,n_active_pts
           WRITE (IUNIT,'( E12.5 )') c(active_pts(i)) 
    END DO

    WRITE (IUNIT,'( " " )' )

    WRITE (IUNIT,'( "SCALARS max_level float 1" )' )
    WRITE (IUNIT,'( "LOOKUP_TABLE default" )' )
    DO i=1,n_active_pts
           WRITE (IUNIT,'( i12 )') &
                MAX( ij2j(indx(active_pts(i),1),j_out), ij2j(indx(active_pts(i),2),j_out) , ij2j(indx(active_pts(i),3),j_out))
    END DO



  CLOSE(IUNIT)


END SUBROUTINE  write_unstructured_vtk_scalar


SUBROUTINE write_structured_vtk( u_out, nwlt_loc, nxyz_out, max_nxyz,  xx_out,it,j_out,filename,field_label)
  USE precision
  USE share_consts
  USE wlt_trns_util_mod
  USE wlt_vars ! prd
  IMPLICIT NONE
  INTEGER  ,                                   INTENT(IN) :: nwlt_loc , nxyz_out(1:3) ! dim,
  INTEGER  ,                                   INTENT(IN) :: it, j_out
  REAL (pr), DIMENSION (1:3,0:nxyz_out(1),0:nxyz_out(2),0:nxyz_out(3)), INTENT(IN) :: u_out
!  INTEGER  , DIMENSION (1:nwlt_loc,1:3),           INTENT(IN) :: indx
  INTEGER  ,                                   INTENT(IN) :: max_nxyz 
  REAL (pr), DIMENSION (0:max_nxyz,1:3) , INTENT(IN) :: xx_out
  CHARACTER(*),                                   INTENT(IN) :: filename
  CHARACTER(*),                                   INTENT(IN) :: field_label

  CHARACTER (LEN=3)   :: tmpch
  CHARACTER (LEN=256) :: file_name_written
  INTEGER             :: i,j,k
  INTEGER             :: IUNIT  

  IUNIT = 200

  

 

    write(*,*) 'write_structured_vtk(): File opened to write output field:'
	file_name_written = TRIM(filename)//"_"//TRIM(field_label)//'.vtk'
    write(*,*)  TRIM(file_name_written)
    open(unit=IUNIT, FILE=TRIM(file_name_written), FORM='formatted',RECL=1000)

    WRITE (IUNIT,'( "# vtk DataFile Version 3.0" )' )
    WRITE (IUNIT,'( "vtk output" )' )
    WRITE (IUNIT,'( "ASCII" )' )
 
 
    !
	! Now write a structured data set 
    WRITE (IUNIT,'( " " )' )
    WRITE (IUNIT,'( "DATASET STRUCTURED_POINTS" )' )
    WRITE (IUNIT,'( "DIMENSIONS ", 3( i4.4 ," ") )' ) nxyz_out + 1 - prd !grid dimensions
    WRITE (IUNIT,'( "ORIGIN "  , 3( E12.5 ," ") )' ) xx_out(0,1:3)   !grid origin

	!Assume regular spacing. (We will leave a check)
	IF(xx_out(1,1)-xx_out(0,1) /= xx_out(2,1)-xx_out(1,1) .OR. &
	   xx_out(1,2)-xx_out(0,2) /= xx_out(2,2)-xx_out(1,2) .OR. &
	   xx_out(1,3)-xx_out(0,3) /= xx_out(2,3)-xx_out(1,3)) THEN
	    PRINT *,' ERROR this routine write_structured_vtk() is only for regularly spaced grid, Exiting...'
		pause; stop
	END IF

	!grid spacing between points
    WRITE (IUNIT,'( "SPACING " , 3( E12.5 ," ") )' ) xx_out(1,1)-xx_out(0,1), xx_out(1,2)-xx_out(0,2),xx_out(1,3)-xx_out(0,3)

    WRITE (IUNIT,'( " " )' )

	!# of points on grid
    WRITE (IUNIT,'( "POINT_DATA ", i12 )' ) (nxyz_out(1) + 1 - prd(1) )* (nxyz_out(2) + 1 - prd(2) )*(nxyz_out(3) + 1 - prd(3) )

    ! add label for field that will be shown in ParaView
    WRITE (IUNIT,'( "SCALARS ", A  ," float 3" )' ) field_label
    WRITE (IUNIT,'( "LOOKUP_TABLE default" )' )

	!write actual data as vectors x1 y1 z1 x2 y2 z2 ...
    DO k=0,nxyz_out(3)- prd(3)
	    DO j=0,nxyz_out(2) - prd(2)
		    DO i=0,nxyz_out(1) - prd(1)
				WRITE (IUNIT,'( 3(E12.5, " ") )') u_out(1:3,i,j,k) 
			END DO   
		END DO
    END DO


  CLOSE(IUNIT)


END SUBROUTINE  write_structured_vtk

END MODULE write_vtk_mod
