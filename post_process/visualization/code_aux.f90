!-------------------------------------------------------!
! define whether to use C or Fortran style node pointer !
#ifdef TREE_NODE_POINTER_STYLE_C
#undef TREE_NODE_POINTER_STYLE_C
#endif
#ifdef TREE_NODE_POINTER_STYLE_F
#undef TREE_NODE_POINTER_STYLE_F
#endif
#if ((TREE_VERSION == 0) || (TREE_VERSION == 1))
#define TREE_NODE_POINTER_STYLE_C
#elif ((TREE_VERSION == 2) || (TREE_VERSION == 3))
#define TREE_NODE_POINTER_STYLE_F
#else
#error Please define correct TREE_VERSION
#endif
! define whether to use C or Fortran style node pointer !
!-------------------------------------------------------!

! auxiliary subroutines in FORTRAN
! related to res2vis and int codes
! by Alexei Vezolainen
! Oct 2006
MODULE code_aux
  USE precision

#if ((TREE_VERSION == 2) || (TREE_VERSION == 3))
    USE tree_database_f      ! use Fortran database instead of C/C++ tree
#endif

  IMPLICIT NONE
  
  PUBLIC :: aux__1, &                         ! filter wavelet coefficients before the transform
       aux_0, &                               ! perform inverse wavelet transform
       aux_1, &                               ! add all significant nodes and make the data non-periodic
       usage_res2vis, &                       ! print usage message (res2vis)
       read_file_type_res2vis, &              ! read command line parameters (res2vis)
       usage_int, &                           ! print usage message (int, int2)
       read_file_type_int, &                  ! read command line parameters (int, int2)
       aux_init_vis_db                        ! initialize visualization tree (if required)
  
  PRIVATE :: expand, &                                             ! add periodic node as a non-periodic
       err_int, &                                                  ! print error message
       get_begin_station_num, &                                    ! extract begin_station_num from filename
       switch_ij                                                   ! exchange i-j values of an array
  
  INTEGER, PRIVATE, PARAMETER :: MAXLEN = 512                      ! maximum length of command line argument
  INTEGER, PRIVATE, PARAMETER :: id_sig = 1                        ! ID of significant node
  CHARACTER(LEN=*), PRIVATE, PARAMETER :: EPS_FMT = '(E15.10)'     ! format for the treshold conversion
  CHARACTER(LEN=*), PRIVATE, PARAMETER :: NUM_FMT = '(I5)'         ! format for the variable number conversion
  
  INTEGER, PRIVATE, DIMENSION(-1:1) :: ij_adj, adj_type
  LOGICAL, PRIVATE :: new_grid
  REAL(pr), PRIVATE ::  scl_fltwt, eps_run, eps_init, cflmax, cflmin, dtwrite
  
  INTEGER, PRIVATE :: trnsf_type, &
       j_lev_old, eps_adapt_steps, j_filt, &
       nd_assym_low, nd_assym_bnd_low, nd2_assym_low, nd2_assym_bnd_low, &
       nd_assym_high, nd_assym_bnd_high, nd2_assym_high, nd2_assym_bnd_high, &
       i_h_inp, i_l_inp, wlt_fmly, &
       domain_meth_old                               ! original domain_meth
  
CONTAINS
  !------------------------------------------------------------------------------------------------
  !------------------------------------------------------------------------------------------------
  ! PUBLIC
  ! initialize visualization tree (if required)
  SUBROUTINE aux_init_vis_db ( db_j_max, n_var, n_var_i, TRANSFORM_TO_REAL, SAVE_TREE )
    USE wlt_vars                ! dim, j_lev, j_mx
    USE parallel                ! par_size
    USE debug_vars
    INTEGER, INTENT(IN) :: db_j_max, n_var, n_var_i
    LOGICAL, INTENT(IN) :: SAVE_TREE, TRANSFORM_TO_REAL
    INTEGER, ALLOCATABLE :: par_proc_tree_nonprd(:)
!   INTEGER :: nprd_num_of_trees, ONE(dim), i_p(0:dim), i_p_nprd(0:dim), &                                        !AR 2/23/2011! commented by AR
    INTEGER :: nprd_num_of_trees, ONE(dim),  ii, &                                                                    !AR 2/23/2011! added by AR
         xyz_nprd(dim), i, i_nprd, xyz(dim), t_max_coord(dim)

    INTEGER*8 :: i_p(0:dim), i_p_nprd(0:dim)                                                                      !AR 2/23/2011! added by AR

    LOGICAL :: domain_change   ! parallel domain decompose() output parameter
    
    IF ( SAVE_TREE ) RETURN
    


    ! decomposition uses current prd(:) values
    ! use it as it is for nonperiodic
    ! and remap for periodic grid
    IF (.NOT.TRANSFORM_TO_REAL) &
         CALL parallel_domain_decompose( & ! normally it is done in init_DB
         domain_change, &
         VERBLEVEL=verb_level, &
         METH=-1, &
         FIRSTCALL=.TRUE.)
    

    IF ( ALL(prd(1:dim).EQ.0).OR.par_size.EQ.1 ) THEN
       ! domain decomposition is performed for nonperiodic prd()
       ! similar to nonperiodic visualization tree
       CALL DB_declare_globals ( mxyz, prd*0, j_tree_root, db_j_max, dim, n_var, n_var_i, &
            par_size, par_rank, par_proc_tree )
    ELSE
       ! periodicity of the domain decomposition, PAR_PROC_TREE, differs
       ! from one of nonperiodic visualization tree, stored in the DB
       ONE(1:dim) = 1
       nprd_num_of_trees = PRODUCT( mxyz(1:dim)*2**(j_tree_root-1) + ONE(1:dim) )
       ALLOCATE( par_proc_tree_nonprd(1:nprd_num_of_trees) )
       i_p(0) = 1
       i_p_nprd(0) = 1
       DO i=1,dim
          i_p(i) = i_p(i-1)*( mxyz(i)*2**(j_tree_root-1) + 1 - prd(i) )
          i_p_nprd(i) = i_p_nprd(i-1)*( mxyz(i)*2**(j_tree_root-1) + 1 )
          t_max_coord(i) =  mxyz(i)*2**(j_tree_root-1)
       END DO
       DO i_nprd=1,nprd_num_of_trees
          xyz_nprd(1:dim) = INT(MOD(i_nprd-1,i_p_nprd(1:dim))/i_p_nprd(0:dim-1))
          DO ii = 1,dim
             xyz(ii) = xyz_nprd(ii)
             IF (prd(ii).EQ.1) &
                  xyz(ii) = MOD( xyz_nprd(ii) + t_max_coord(ii), t_max_coord(ii) )
          END DO
          i = 1+SUM(xyz(1:dim)*i_p(0:dim-1))
          par_proc_tree_nonprd(i_nprd) = par_proc_tree(i)
       END DO

!!$       IF (par_rank.EQ.0) THEN
!!$          PRINT *, 'prd=',prd
!!$          PRINT *, 'i_p     =', i_p
!!$          PRINT *, 'i_p_nprd=', i_p_nprd
!!$          PRINT *, 't_max_coord=',t_max_coord
!!$          PRINT *, 'par_proc_tree_nonprd(aux_init_vis_db)=', par_proc_tree_nonprd
!!$          PRINT *, 'par_proc_tree(aux_init_vis_db)=', par_proc_tree
!!$          PRINT *, 'TRANSFORM_TO_REAL=', TRANSFORM_TO_REAL
!!$       END IF
       

       CALL DB_declare_globals ( mxyz, prd*0, j_tree_root, db_j_max, dim, n_var, n_var_i, &
            par_size, par_rank, par_proc_tree_nonprd )
       DEALLOCATE (par_proc_tree)
       ALLOCATE (par_proc_tree(nprd_num_of_trees))
       par_proc_tree = par_proc_tree_nonprd
       DEALLOCATE (par_proc_tree_nonprd)
    END IF

  END SUBROUTINE aux_init_vis_db
  !------------------------------------------------------------------------------------------------
  !------------------------------------------------------------------------------------------------
  ! PUBLIC
  ! prepare and perform inverse transform if required
  SUBROUTINE aux_0 ( TRANSFORM_TO_REAL, SAVE_TREE, RE_PARTITION, verb_level, pre_init_DB_only )
    USE precision
    USE share_consts
    USE pde
    USE variable_mapping 
    USE sizes
    USE wlt_trns_mod       ! init_DB(), c_wlt_trns_mask()
    USE field              ! u
    USE io_3D              ! read_solution()
    USE wlt_vars           ! DB_TYPE_TREE_C,  maxval_n_updt, j_mn
    USE wlt_trns_vars      ! TYPE_DB
    USE wlt_trns_util_mod  ! imp6
    USE parallel           ! par_rank
    LOGICAL, INTENT(IN) :: TRANSFORM_TO_REAL, RE_PARTITION
    LOGICAL, INTENT(INOUT) :: SAVE_TREE
    INTEGER, INTENT(IN), OPTIONAL :: verb_level
    LOGICAL, INTENT(IN), OPTIONAL :: pre_init_DB_only
    INTEGER   :: i, j, k, do_verb_level, n_var_local
    LOGICAL :: new_grid, do_pre_init_DB
    INTEGER :: j_lev_old, j_filt

    LOGICAL, DIMENSION(:,:), ALLOCATABLE :: n_var_adapt_loc       ! Local Logical map into Variables in u(,) used in 
    LOGICAL, DIMENSION(:,:), ALLOCATABLE :: n_var_interpolate_loc ! Local Logical map into Variables in u(,)
    

#ifdef MULTIPROC
    domain_meth_old = domain_meth              ! original domain decomposition
    IF (.NOT.RE_PARTITION) domain_meth = -1    ! use original partition if no repartitioning set
#endif

    
    do_verb_level = 0             ! no output
    do_pre_init_DB = .TRUE.       ! return after pre_init_DB() call

    IF (PRESENT(verb_level))        do_verb_level  = verb_level
    IF (PRESENT (pre_init_DB_only)) do_pre_init_DB = pre_init_DB_only
    

    
    
    ! set pre-init parameters
    
    !-----------------------------------------------------------
    ! CALL read_input(...)
    maxval_n_updt = maxval(n_updt)
    maxval_n_prdct = maxval(n_prdct)
    n_diff = 1

    j_tree_root = j_mn                  ! db_tree related variable (normally set in read_input)
    j_mn_init = j_mn                    ! to make sure j_mn will not change in init_DB
    
    !-----------------------------------------------------------
    !CALL read_input_finalize()
    i_h_inp = 123456
    i_l_inp = 111111


    ! set n_assym_prdct/prdct_bnd/updt/updt_bnd
    !  .TRUE. will not reassign n_assym_prdct, etc.
    !  Instead, it will keep read from .res file values
    CALL set_max_assymetry ( .TRUE. )


    nbnd = 0
    ibnd=imp6(i_h_inp)
    ibc=imp6(i_l_inp)
    IF(prd(1) == 1) THEN
        ibc(1)=0
        ibc(2)=0
        grid(1)=0
    END IF
    IF(prd(2) == 1) THEN
        ibc(3)=0
        ibc(4)=0
        grid(2)=0
    END IF
    IF(prd(3) == 1) THEN
        ibc(5)=0
        ibc(6)=0
        grid(3)=0
    END IF
    
    !-----------------------------------------------------------
    ! CALL check_inputs(...)
    jd = 1
    IF(dim==2) THEN
        mxyz(3) = 1
        prd(3) = 1
        jd = 0 
    END IF
    !-----------------------------------------------------------
    !-----------------------------------------------------------
    ! Initialize tree; compute the domain decomposition; initialize transform related things
    ! pre-init called from main in parallel
    IF (do_pre_init_DB==.TRUE.) THEN
       CALL pre_init_DB
       RETURN
    END IF
    
    
    ! get n_var from variable_mapping module
    n_var_local = get_n_var()    

    IF (n_var_local.EQ.0) THEN
       CALL error ('Something is wrong.','In aux_0 (in code_aux.f90) n_var=0.', &
            'Please contact the developers.')
    END IF

    !-----------------------------------------------------------
    !  CALL alloc_variable_mappings
    !  CALL map_veloc_timesteps
    IF(.NOT.ALLOCATED(n_var_adapt_loc) )       ALLOCATE (n_var_adapt_loc(1:n_var_local,0:1))
    IF(.NOT.ALLOCATED(n_var_interpolate_loc) ) ALLOCATE (n_var_interpolate_loc(1:n_var_local,0:1))
    IF(.NOT.ALLOCATED(n_var_index))            ALLOCATE (n_var_index(1:n_var_local))
    n_var_adapt_loc(:,0) = .TRUE.
    n_var_interpolate_loc(:,0) = .TRUE.
    DO i=1,n_var_local        
       CALL set_adapt( i, (/.TRUE.,.FALSE./) )
       CALL set_interpolate( i, (/.TRUE.,.FALSE./) )
       n_var_index(i) = i        
    END DO


    
    ! For do_pre_init_DB==.TRUE.,  .p*.res files  are not yet read; therefore, xx() array is not allocated/assigned at this point.
    ! That is why this loop is moved to after  IF (do_pre_init_DB==.TRUE.) statement.
    DO i=1,dim
       xyzlimits(1,i) = xx(0,i)
       xyzlimits(2,i) = xx(nxyz(i),i)
    END DO
    
    
    
    IF (.NOT.TRANSFORM_TO_REAL) THEN
        SAVE_TREE = .FALSE.               ! the tree has not been saved
        RETURN
    END IF
    
    
    
    
    
    
    CALL init_DB( nwlt_old, nwlt, nwltj_old, nwltj, new_grid,&
         j_lev_old, j_lev, j_mn, j_mx , MAXVAL(nxyz) , nxyz, .TRUE., POST_PROCESS=.TRUE.)
    
    !WRITE(*,'(A, 2I6)') '   IN aux_0   AFTER init_DB   ---   par_rank  nwlt : ', par_rank, nwlt  
    
    ! this is conditioned by non-updated init_DB subroutine for db_lines,
    ! as soon as it is updated, inverse transform to be removed for all the databases
    IF (TYPE_DB.EQ.DB_TYPE_LINES) &
            CALL c_wlt_trns_mask (u, u, n_var, n_var_interpolate(:,0), n_var_adapt(:,0), .TRUE., .FALSE.,&
            nwlt, nwlt, j_lev, j_lev, HIGH_ORDER, WLT_TRNS_INV, DO_UPDATE_DB_FROM_U, DO_UPDATE_U_FROM_DB)

    ! make sure the tree used for the transform does not interfere 
    !  with the tree which will be used for visualization
    IF ((TYPE_DB.EQ.DB_TYPE_TREE_C).AND.(.NOT.SAVE_TREE )) CALL DB_finalize
    !-----------------------------------------------------------
    
    DEALLOCATE (n_var_adapt_loc)
    DEALLOCATE (n_var_interpolate_loc)
    DEALLOCATE (n_var_index)
    
    IF (par_rank.EQ.0.AND.do_verb_level.GT.0) THEN
        IF (SAVE_TREE) THEN
            PRINT *, 'aux_0: transform to real - done, the database still exists'
        ELSE
            PRINT *, 'aux_0: transform to real - done, the database has been destroyed'
        END IF
    END IF
    

  END SUBROUTINE aux_0
  !------------------------------------------------------------------------------------------------
  !------------------------------------------------------------------------------------------------
  ! PUBLIC
  ! add all significant nodes and make the data non-periodic
  ! assumes that indx_DB has been initialized (through read_solution)
  SUBROUTINE aux_1 (n_var_local, prd_old, eps_max_level, db_j_max, var_add_mode, &
       SAVE_TREE, EXPAND_DATA, ADD_LEVEL, ADD_DOMAIN, P_D, RDL)
    USE wlt_vars                  ! dim, j_lev
    USE sizes                     ! nwlt
    USE field                     ! u
    USE variable_mapping 
    USE wlt_trns_vars             ! indx_DB
    USE db_tree_vars              ! node pointers
    USE parallel                  ! par_size, IC_par_proc_tree, IC_par_size
    USE debug_vars
    USE db_tree                   ! request_boundary_nodes()
!!$#ifdef MULTIPROC
!!$    USE db_tree                   ! request_boundary_nodes()
!!$#endif
    INTEGER, INTENT(IN) :: n_var_local, &                                   ! number of variables
         prd_old(1:dim), &                                            ! *.res file periodicity
         eps_max_level, &                                             ! max level to use
         db_j_max, &                                                  ! j_max for the tree database
         var_add_mode                                                 ! 0 - add variables, 1 - index
    LOGICAL, INTENT(IN) :: EXPAND_DATA,                             & ! make data non-periodic
         SAVE_TREE                                                    ! if tree exists
    LOGICAL, INTENT(IN), OPTIONAL :: ADD_LEVEL,                     & ! add level as integer coordinate
         ADD_DOMAIN,                                                & ! add domain as integer coordinate
         P_D                                                          ! even if read at single processor
    INTEGER, INTENT(IN), OPTIONAL :: RDL                              ! randomize domain number up to that limit
    
!   INTEGER :: j, wlt_type, face_type, j_df, kk, ii, iwlt, &                                                           !AR 2/23/2011! commented by AR
!        i_p(0:dim), i_pm(0:dim), i, xyz(1:dim), ierror, &                                                             !AR 2/23/2011! commented by AR
    INTEGER :: j, k, wlt_type, face_type, j_df, kk,     iwlt, &                                                        !AR 2/23/2011! added by AR
                                  i, xyz(1:dim), ierror, &                                                             !AR 2/23/2011! added by AR
         xyz_e(1:dim), j_max_to_consider, domain, proc, add_size, &
         i_pj_tree(0:dim), num_trees(dim), xyz_tr(dim), SEED(2), &
         do_rdl, mxv                                                  ! max random multiplier for domain numbers
    INTEGER*8 :: ii, i_p(0:dim), i_p_db(0:dim), i_pm(0:dim)                                                            !AR 2/23/2011! added by AR
         
    REAL*8 :: f_in(1:n_var_local)                                           ! single node function values
    INTEGER :: lev_stat(1:j_lev)                                      ! number of points at each level
    INTEGER, ALLOCATABLE :: dom_stat(:), &                            ! number of points at each domain
         dom_rnd_mult(:), &                                           ! multiply domain number at this value
         dom_loc(:)                                                   ! domain number (for colormap)
    INTEGER :: i_level, i_domain                                      ! integer variables positions
    INTEGER*8, POINTER :: add_coord(:)  ! 1D coord of boundary nodes from other processors              !AR 2/23/2011!  INTEGER changed to INTEGER*8
    REAL(pr), POINTER :: add_u(:,:)   ! u value of boundary nodes from other processors (1:n_var_local,iwlt)
    REAL(pr) :: rnd
    LOGICAL :: var_mask(n_var_local), PRESERVE_DECOMPOSITION

    CHARACTER(LEN=256) :: WRITE_NUM_FMT

    INTEGER :: Coord_map(dim), Jij_map(dim,dim)  ! auxiliary variable for periodic curvilinear coordinate adjustment
     
#ifdef TREE_NODE_POINTER_STYLE_C
    INTEGER(pointer_pr) :: c_ptr, c_ptr1                                ! C style node pointer
#elif defined TREE_NODE_POINTER_STYLE_F
    TYPE(node), POINTER :: c_ptr, c_ptr1                           ! (Fortran's native pointer)
#endif
    
    
    ! ----------------------------------------------------------
    ! define EBUG_SINGLE_PROC_ZONE
    ! to visualize single processor boundary zone to be communicated
!#define EBUG_SINGLE_PROC_ZONE
#ifdef EBUG_SINGLE_PROC_ZONE
    INTEGER, ALLOCATABLE :: node_to_recv(:)
    INTEGER :: node_to_recv_len, tmpwrite, xyz_tmpd(dim), ii_tmp, iii
#endif

    NULLIFY(add_coord)
    NULLIFY(add_u)
 
    ! set integer variable numbers
    i_level = 0
    i_domain = 0
    IF (PRESENT(ADD_LEVEL)) THEN
       IF (ADD_LEVEL) i_level = 1
    END IF
    IF (PRESENT(ADD_DOMAIN)) THEN
       IF(ADD_DOMAIN) THEN
          i_domain = i_level + 1
          ! ALLOCATE( dom_stat(0:MAX(IC_par_size,par_size)-1) )
       END IF
    END IF
    ALLOCATE( dom_stat(0:MAX(IC_par_size,par_size)-1) )
    
    PRESERVE_DECOMPOSITION = .FALSE.
    IF (PRESENT(P_D)) PRESERVE_DECOMPOSITION = P_D
    
    do_rdl = 0
    IF (PRESENT(RDL)) do_rdl = RDL
    
    IF (i_domain.GT.0) THEN
       IF (PRESERVE_DECOMPOSITION) THEN
          IF (debug_level.GT.0) PRINT *, 'SIZE(IC_par_proc_tree)=', SIZE(IC_par_proc_tree)
          IF (debug_level.GT.1) PRINT *, 'IC_par_proc_tree=', IC_par_proc_tree
          ! j_tree_root set to j_mn for visualization tree and may differ from that in .res
          ! therefore it has to be computed here; mxyz and prd_old are the same
          i = 1
          DO WHILE (PRODUCT( mxyz(1:dim)*2**(i-1) + 1 - prd_old(1:dim) ) .LT. SIZE(IC_par_proc_tree))
             i = i + 1
          END DO
          ! j_tree_root <--> i
          num_trees(1:dim) = mxyz(1:dim)*2**(i-1) + 1 - prd_old(1:dim)
          i_pj_tree(0) = 1
          DO i=1,dim
             i_pj_tree(i) = i_pj_tree(i-1)*num_trees(i)
          END DO
          IF (PRODUCT(num_trees).NE.SIZE(IC_par_proc_tree)) THEN
             PRESERVE_DECOMPOSITION = .FALSE.
             PRINT *, 'WARNING: Unable to preseve the original domain partritioning on a single'
             PRINT *, '         processor. Try running in parallel if partitioning is required.'
             PRINT *, 'num_trees = ', num_trees
             PRINT *, 'SIZE(IC_par_proc_tree) = ', SIZE(IC_par_proc_tree)
          END IF
       END IF
       
       ! randomize domain numbers (for better visualization)
       mxv = MAXVAL(IC_par_proc_tree)
       IF (debug_level.GT.0) PRINT *, 'MAXVAL(IC_par_proc_tree)=', mxv
       ALLOCATE ( dom_rnd_mult(0:mxv), dom_loc(0:mxv) )
       ! for default value of maximum random domain number, 0,
       ! do not randomize domain numbers
       IF (do_rdl.EQ.0) THEN
          dom_rnd_mult = 1
       ELSE
          SEED = 54321
          CALL RANDOM_SEED(PUT=SEED)
          DO i=0,mxv
             CALL RANDOM_NUMBER (rnd)
             dom_rnd_mult(i) = INT(rnd*do_rdl)
          END DO
       END IF
       DO i=0,mxv
          dom_loc(i) = i
       END DO
       
       !
       ! manual adjustment for domains to look separately with standard paraview colormap
       !
!!$       dom_rnd_mult = 1
!!$       CALL switch_ij (dom_loc, mxv, 4, 5)
!!$       CALL switch_ij (dom_loc, mxv, 6, 7)
!!$       CALL switch_ij (dom_loc, mxv, 0, 1)
    END IF

    ! ----------------------------------------------------------
    ! define EBUG_SINGLE_PROC_ZONE
    ! to visualize single processor boundary zone to be communicated
#ifdef EBUG_SINGLE_PROC_ZONE
    IF (i_level.GT.0) THEN
       OPEN (UNIT=123,FILE='nodes.p0.dat',FORM='unformatted')
       READ (123) i_pm(0:dim), &                          ! <--> 1D written index
            node_to_recv_len                              ! n_transfer_send
       ALLOCATE (node_to_recv(1:node_to_recv_len))
       READ (123) node_to_recv(1:node_to_recv_len)      ! 1D index of the nodes at j_mx
       CLOSE (123)
    END IF
#endif

    ! initialize i_p array for 1d into 3d coordinate transform
    i_p(0)    = 1
    i_p_db(0) = 1
    i_pm(0)   = 1
    DO i=1,dim
       i_p(i)    = i_p(i-1)*(1+nxyz(i))                     ! <--> j_lev
       i_p_db(i) = i_pm(i-1)*(1+mxyz(i)*2**(db_j_max-1))   ! <--> db_j_max
       i_pm(i)   = i_pm(i-1)*(1+mxyz(i)*2**(j_mx-1))       ! <--> j_max
    END DO
    lev_stat(1:j_lev) = 0
    dom_stat = 0
    
    ! find maximum level to consider
    j_max_to_consider = j_lev
    IF (eps_max_level.GT.0) &
         j_max_to_consider = MAX( j_tree_root, MIN( j_lev, eps_max_level ) )
    
    ! ---------------------------------------------------------------------------------
    ! add all the significant nodes of the current processor (stored in indx_DB)
    IF ( (.NOT.SAVE_TREE) .OR. &
         EXPAND_DATA      .OR. &
         (i_level.GT.0)   .OR. &
         (i_domain.GT.0)       ) THEN

       DO j = 1, j_max_to_consider
          DO wlt_type = MIN(j-1,1),2**dim-1
             DO face_type = 0, 3**dim - 1
                DO j_df = j, j_lev
                   DO kk = 1, indx_DB(j_df,wlt_type,face_type,j)%length
                      ii = indx_DB(j_df,wlt_type,face_type,j)%p(kk)%ixyz           ! 1D coordinate
                      xyz(1:dim) = INT(MOD(ii-1,i_p(1:dim))/i_p(0:dim-1))          ! 3D coordinate \in [0..N-1] at j_lev
                      xyz(1:dim) = xyz(1:dim)/2**(j_lev-db_j_max)                  ! ... at db_j_max
                      !CALL DB_get_proc_by_coordinates ( xyz, db_j_max, domain )   ! processor the node belongs to
                      !IF (domain.EQ.par_rank.OR.j.LE.j_tree_root.OR..TRUE.) THEN
                      !
                      ! We can add all the nodes for all the domains here,
                      ! AMR::create_box_tree() will add only those boxes
                      ! which lowest corner belongs to the current processor

                      iwlt = indx_DB(j_df,wlt_type,face_type,j)%p(kk)%i         ! wavelet number

                      IF (var_add_mode.EQ.0) &
                           f_in(1:n_var_local) = u(iwlt,1:n_var_local)

                      CALL DB_add_node ( xyz, db_j_max, ierror, c_ptr, id_sig, 1 )

#ifdef TREE_NODE_POINTER_STYLE_C
                      IF (var_add_mode.EQ.0) THEN
                         CALL DB_set_function_by_pointer ( c_ptr, 1, n_var_local, f_in )
                      ELSE IF (var_add_mode.EQ.1) THEN
                         CALL DB_set_ifunction_by_pointer ( c_ptr, 1, 1, iwlt )
                      END IF
                      IF (i_level.GT.0) THEN
                         ! define EBUG_SINGLE_PROC_ZONE
                         ! to visualize single processor boundary zone to be communicated
#ifdef EBUG_SINGLE_PROC_ZONE
                         ii_tmp = 1+SUM( (xyz(1:dim)*2**(j_mx-db_j_max)) *i_pm(0:dim-1))    ! ... at j_mx
                         tmpwrite = 0
                         DO iii=1,node_to_recv_len
                            IF (node_to_recv(iii).EQ.ii_tmp) tmpwrite = 1
                         END DO
                         CALL DB_set_ifunction_by_pointer ( c_ptr, i_level, i_level, tmpwrite )
#else
                         CALL DB_set_ifunction_by_pointer ( c_ptr, i_level, i_level, j )
                         lev_stat(j) = lev_stat(j) + 1
#endif
                      END IF
                      IF (i_domain.GT.0) THEN
                         IF (PRESERVE_DECOMPOSITION) THEN
                            ! visualize domain decomposition on a single processor
                            domain = IC_par_proc_tree( 1+SUM( (xyz(1:dim)/2**(db_j_max-j_tree_root)) *i_pj_tree(0:dim-1)) )
                            dom_stat(domain) = dom_stat(domain) + 1
                            domain = dom_loc( domain )
                            domain = domain * dom_rnd_mult(domain)
                            
#ifdef EBUG_SINGLE_PROC_ZONE
                            domain = domain + 1
                            IF (domain.EQ.1) THEN
                               domain = 10
                            ELSE
                               domain = 9
                            END IF
#endif
                         ELSE
                            ! get real processor the node belongs to
                            CALL DB_get_proc_by_coordinates ( xyz, db_j_max, domain )
                            dom_stat(domain) = dom_stat(domain) + 1
                         END IF
                         CALL DB_set_ifunction_by_pointer ( c_ptr, i_domain, i_domain, domain )
                      END IF
#elif defined TREE_NODE_POINTER_STYLE_F
                      IF (var_add_mode.EQ.0) THEN
                         c_ptr%val(1:n_var_local) = f_in(1:n_var_local)
                      ELSE IF (var_add_mode.EQ.1) THEN
                         c_ptr%ival(1) = iwlt
                      END IF
                      IF (i_level.GT.0) THEN
                         ! define EBUG_SINGLE_PROC_ZONE
                         ! to visualize single processor boundary zone to be communicated
#ifdef EBUG_SINGLE_PROC_ZONE
                         ii_tmp = 1+SUM( (xyz(1:dim)*2**(j_mx-db_j_max)) *i_pm(0:dim-1))    ! ... at j_mx
                         tmpwrite = 0
                         DO iii=1,node_to_recv_len
                            IF (node_to_recv(iii).EQ.ii_tmp) tmpwrite = 1
                         END DO
                         CALL DB_set_ifunction_by_pointer ( c_ptr, i_level, i_level, tmpwrite )
#else
                         c_ptr%ival(i_level) = j
                         lev_stat(j) = lev_stat(j) + 1
#endif
                      END IF
                      IF (i_domain.GT.0) THEN
                         IF (PRESERVE_DECOMPOSITION) THEN
                            ! visualize domain decomposition on a single processor
                            domain = IC_par_proc_tree( 1+SUM( (xyz(1:dim)/2**(db_j_max-j_tree_root)) *i_pj_tree(0:dim-1)) )
                            dom_stat(domain) = dom_stat(domain) + 1
                            domain = dom_loc( domain )
                            domain = domain * dom_rnd_mult(domain) + 1
                         ELSE
                            ! get real processor the node belongs to
                            CALL DB_get_proc_by_coordinates ( xyz, db_j_max, domain )
                            dom_stat(domain) = dom_stat(domain) + 1
                         END IF
                         c_ptr%ival(i_domain) = domain
                      END IF
#endif

!!$
                      IF ( EXPAND_DATA .AND. ANY(xyz(1:dim).EQ.0) ) THEN ! some non-periodic nodes might need to be added
                         xyz_e(1:dim) = xyz(1:dim)
                         CALL expand ( iwlt, n_var_local, xyz_e, prd_old, db_j_max, var_add_mode, j, i_level, i_domain )
                      END IF

                      !END IF ! of adding the nodes of the current domain
                   END DO
                END DO
             END DO
          END DO
       END DO
    END IF
    
    
    
    
    IF (ALLOCATED(dom_rnd_mult)) DEALLOCATE (dom_rnd_mult)
    IF (ALLOCATED(dom_loc)) DEALLOCATE (dom_loc)

    ! ----------------------------------------------------------
    ! define EBUG_SINGLE_PROC_ZONE
    ! to visualize single processor boundary zone to be communicated
#ifdef EBUG_SINGLE_PROC_ZONE
    IF (i_level.GT.0) DEALLOCATE (node_to_recv)
#endif



    ! communications below use prd(:) to get 1d coordinates of points to transfer
    ! although request_boundary_nodes() requires the original prd(:), prd_old(:),
    ! to transfer the external boundaries
    prd(1:dim) = 0

    ! ---------------------------------------------------------------------------------
    ! request unknown number of boundary nodes from other processors
#ifdef MULTIPROC
    !  All to all communication: j_max_to_consider not used
    !  Returns add_coord in db_j_max coordinates
    CALL request_boundary_nodes( add_coord, add_u, add_size, n_var_local, j_max_to_consider, prd_old )
#else
    ! db_wrk and others do not have that subroutine
    add_size = 0
#endif
    ! add boundary nodes to the current database
    ! (with zero level)
    DO iwlt=1,add_size
       ! ERIC: these could be on the current j_max_db, NEED TO GENERALLY CLEAN UP THE USE OF J_MAX AND GLOBALS
       ! xyz is in db_j_max, and i_pm is in j_mx, though i_p is in j_lev (eq db_j_max?)
       xyz(1:dim) = INT( MOD( add_coord(iwlt)-1,i_p_db(1:dim) )/i_p_db(0:dim-1))        ! 3D coordinate \in [0..N-1]
       
       IF (var_add_mode.EQ.0) &
            f_in(1:n_var_local) = add_u(1:n_var_local,iwlt)
       
       !CALL DB_add_node ( xyz, j_mx, ierror, c_ptr, id_sig, 1 )
       CALL DB_add_node ( xyz, db_j_max, ierror, c_ptr, id_sig, 1 )
       
#ifdef TREE_NODE_POINTER_STYLE_C
       IF (var_add_mode.EQ.0) THEN
          CALL DB_set_function_by_pointer ( c_ptr, 1, n_var_local, f_in )
       ELSE IF (var_add_mode.EQ.1) THEN
          CALL DB_set_ifunction_by_pointer ( c_ptr, 1, 1, iwlt )
       END IF
!!$       IF (i_level.GT.0) THEN
!!$          CALL DB_set_ifunction_by_pointer ( c_ptr, i_level, i_level, j )
!!$          lev_stat(j) = lev_stat(j) + 1
!!$       END IF
       IF (i_domain.GT.0) THEN
          CALL DB_get_proc_by_coordinates ( xyz, db_j_max, domain )    ! processor the node belongs to
          CALL DB_set_ifunction_by_pointer ( c_ptr, i_domain, i_domain, domain )
          dom_stat(domain) = dom_stat(domain) + 1
       END IF
#elif defined TREE_NODE_POINTER_STYLE_F
       IF (var_add_mode.EQ.0) THEN
          c_ptr%val(1:n_var_local) = f_in(1:n_var_local)
       ELSE IF (var_add_mode.EQ.1) THEN
          c_ptr%ival(1) = iwlt
       END IF
!!$       IF (i_level.GT.0) THEN
!!$          c_ptr%ival(i_level) = j
!!$          lev_stat(j) = lev_stat(j) + 1
!!$       END IF
       IF (i_domain.GT.0) THEN
          CALL DB_get_proc_by_coordinates ( xyz, db_j_max, domain )
          c_ptr%ival(i_domain) = domain
          dom_stat(domain) = dom_stat(domain) + 1
       END IF
#endif
!!$       IF ( EXPAND_DATA .AND. ANY(xyz(1:dim).EQ.0) ) THEN ! some non-periodic nodes might need to be added
!!$          xyz_e(1:dim) = xyz(1:dim)
!!$          CALL expand ( iwlt, n_var_local, xyz_e, prd_old, db_j_max, var_add_mode, j, i_level, i_domain )
!!$       END IF       
    END DO
    ! delete boundary nodes
    IF (ASSOCIATED(add_coord)) DEALLOCATE( add_coord )
    IF (ASSOCIATED(add_u)) DEALLOCATE( add_u )
    
    
    
    ! statistics
    IF ( par_rank .EQ. 0 ) THEN
      IF ( par_size .GT. 1 ) PRINT *, 'Proc0 Stats:'
      IF (i_level.GT.0) THEN
         ii = j_lev
         kk = CEILING( LOG(REAL(MAXVAL(lev_stat)+1,pr))/LOG(10.0_pr) ) + 1 ! field
  
         WRITE(WRITE_NUM_FMT,'(I)') ii
         WRITE_NUM_FMT = TRIM(ADJUSTL(WRITE_NUM_FMT))

         WRITE (*,'(A,I6,A,'//WRITE_NUM_FMT//'I'//CHAR(ICHAR('0')+kk)//')') &
        !WRITE (*,'(A,I3,A,'//CHAR(ICHAR('0')+j_lev)//'I8)') &
              '  number of nodes at levels 1:', j_lev, ' is', lev_stat(1:j_lev)
      END IF
      IF (i_domain.GT.0) THEN
         ii = par_size                                                     ! active length of dom_stat
         IF (PRESERVE_DECOMPOSITION) ii = IC_par_size                      ! ...
         kk = CEILING( LOG(REAL(MAXVAL(dom_stat)+1,pr))/LOG(10.0_pr) ) + 1 ! field

         WRITE(WRITE_NUM_FMT,'(I)') ii
         WRITE_NUM_FMT = TRIM(ADJUSTL(WRITE_NUM_FMT))
  
         WRITE (*,'(A,I6,A,'//WRITE_NUM_FMT//'I'//CHAR(ICHAR('0')+kk)//')') &
              '  number of nodes at domain 0 -', ii-1, ' is:', dom_stat(0:ii-1)
      END IF
      IF (add_size.GT.0) THEN
         WRITE (*,'("  number of boundary nodes:",I8)') add_size
      END IF
    END IF
    
!====================================
    Coord_map = 0
    Jij_map = 0
    DO i=1,n_var_local
       IF(TRIM(ADJUSTL(u_variable_names(i))) == 'XCoord') Coord_map(1) = i
       IF(dim > 1 .AND. TRIM(ADJUSTL(u_variable_names(i))) == 'YCoord') Coord_map(2) = i
       IF(dim > 2 .AND. TRIM(ADJUSTL(u_variable_names(i))) == 'ZCoord') Coord_map(3) = i
       IF(TRIM(ADJUSTL(u_variable_names(i))) == 'J11' ) Jij_map(1,1) = i
       IF(dim > 1 .AND. TRIM(ADJUSTL(u_variable_names(i))) == 'J12' ) Jij_map(1,2) = i
       IF(dim > 1 .AND. TRIM(ADJUSTL(u_variable_names(i))) == 'J21' ) Jij_map(2,1) = i
       IF(dim > 1 .AND. TRIM(ADJUSTL(u_variable_names(i))) == 'J22' ) Jij_map(2,2) = i
       IF(dim > 2 .AND. TRIM(ADJUSTL(u_variable_names(i))) == 'J13' ) Jij_map(1,3) = i
       IF(dim > 2 .AND. TRIM(ADJUSTL(u_variable_names(i))) == 'J23' ) Jij_map(2,3) = i
       IF(dim > 2 .AND. TRIM(ADJUSTL(u_variable_names(i))) == 'J31' ) Jij_map(3,1) = i
       IF(dim > 2 .AND. TRIM(ADJUSTL(u_variable_names(i))) == 'J32' ) Jij_map(3,2) = i
       IF(dim > 2 .AND. TRIM(ADJUSTL(u_variable_names(i))) == 'J33' ) Jij_map(3,3) = i
    END DO

    IF( .NOT.ALL(Coord_map(1:dim)==0) .AND. .NOT.ALL(Jij_map(1:dim,1:dim)==0) ) THEN

       DO proc = 0, par_size-1
          DO j = 1, j_lev
             DO wlt_type = MIN(j-1,1),2**dim-1
                DO face_type = 0, 3**dim - 1
                   CALL DB_get_initial_type_level_node (proc, wlt_type, j, face_type, ii, c_ptr, list_sig)
                   IF(ii > 0 ) THEN
                      DO WHILE (is_ok(c_ptr))
#ifdef TREE_NODE_POINTER_STYLE_C
                         CALL DB_get_ifunction_by_pointer( c_ptr, nvarI_nwlt, nvarI_nwlt, iwlt )
                         CALL DB_get_coordinates_by_pointer ( c_ptr, xyz )
                         CALL DB_get_function_by_pointer( c_ptr, 1, n_var_local, f_in )
#elif defined TREE_NODE_POINTER_STYLE_F
                         CALL DB_get_ifunction_by_pointer( c_ptr, nvarI_nwlt, nvarI_nwlt, iwlt )
                         CALL DB_get_coordinates_by_pointer ( c_ptr, xyz )
                         f_in(1:n_var_local) = c_ptr%val(1:n_var_local) 
#endif   
                         xyz = xyz/2**(db_j_max-j_lev)    !Oleg: xyz is on db_j_max level and NOT on j_mx
                         DO i = 1, dim
                            DO k = 1, dim 
                               f_in(Coord_map(i)) =  f_in(Coord_map(i))  +  f_in(Jij_map(i,k)) * ( xx(xyz(k),k) - xyzlimits(1,k) )
                            END DO
                         END DO
#ifdef TREE_NODE_POINTER_STYLE_C
                         CALL DB_set_function_by_pointer ( c_ptr, 1, n_var_local, f_in )
                         CALL DB_get_next_type_level_node (proc, wlt_type, j, face_type, c_ptr, c_ptr1, list_sig)
                         c_ptr = c_ptr1
#elif defined TREE_NODE_POINTER_STYLE_F
                         c_ptr%val(1:n_var_local) = f_in(1:n_var_local)
                         CALL DB_get_next_type_level_node (c_ptr)
#endif   
                      END DO
                   END IF
                END DO
             END DO
          END DO
       END DO
    END IF
       
    ! GO through list by pointers


!=====================================



    ! clean locals
    IF (ALLOCATED(dom_stat)) DEALLOCATE (dom_stat)

  END SUBROUTINE aux_1


  !------------------------------------------------------------------------------------------------
  ! exchange i-j values of an array
  SUBROUTINE switch_ij (arr, len, i, j)
    INTEGER, INTENT(IN) :: i, j, len
    INTEGER, INTENT(INOUT) :: arr(0:len)
    INTEGER :: tmp
    
    IF (MAX(i,j).GT.len.OR.MIN(i,j).LT.0) STOP 'error array bounds in SWITCH_IJ()'

    tmp = arr(i)
    arr(i) = arr(j)
    arr(j) = tmp
    
  END SUBROUTINE switch_ij

  !------------------------------------------------------------------------------------------------
  ! PUBLIC
  ! filter wavelet coefficients before the transform
  SUBROUTINE aux__1 (n_var, prd_old, eps_filter, eps_num, eps_max_level)
    USE wlt_vars                  ! dim, j_lev
    USE sizes                     ! nwlt
    USE field                     ! u
    USE wlt_trns_vars             ! indx_DB
    USE parallel                  ! par_rank

    INTEGER, INTENT(IN) :: n_var, &                                   ! number of variables
         prd_old(1:dim)                                               ! *.res file periodicity
    REAL(pr), INTENT (IN) :: eps_filter                               ! treshold value
    INTEGER, INTENT(IN) :: eps_num, &                                 !  for that variable
         eps_max_level                                                ! filter all above that level
    INTEGER :: proc, j, wlt_type, face_type, j_df, kk, iwlt, &
         i, num, &
         j_max_to_consider

    IF (eps_num.GT.n_var) THEN
       IF (par_rank.EQ.0) THEN
          WRITE (*,'("Requested treshold for the variable ",I5)') eps_num
          WRITE (*,'("although the total number of variables is ",I5)') n_var
          WRITE (*,'("Terminating ...")')
       END IF
       CALL parallel_finalize
       STOP 0
    END IF

    ! find maximum level to consider
    j_max_to_consider = j_lev
    IF (eps_max_level.GT.0) &
         j_max_to_consider = MAX( j_tree_root, MIN( j_lev, eps_max_level ) )

    ! test for early termination
    IF ((eps_filter.LE.0.0_pr).AND.(j_max_to_consider.GE.j_lev)) &
         RETURN
    
    ! filter
    num = 0
    DO j = 1, j_lev
       DO wlt_type = MIN(j-1,1),2**dim-1
          DO face_type = 0, 3**dim - 1
             DO j_df = j, j_lev
                DO kk = 1, indx_DB(j_df,wlt_type,face_type,j)%length
                   iwlt = indx_DB(j_df,wlt_type,face_type,j)%p(kk)%i            ! wavelet number
                   
                   IF (( ABS(u(iwlt,eps_num)).LT.eps_filter ) .OR. & ! ---- additional filtering ----
                        (j.GT.j_max_to_consider)) THEN
                      u(iwlt,eps_num) = 0.0_pr
                   ELSE
                      num = num + 1
                   END IF                                            ! ---- additional filtering ----
                   
                END DO
             END DO
          END DO
       END DO
    END DO
    
  END SUBROUTINE aux__1

  !------------------------------------------------------------------------------------------------
  !------------------------------------------------------------------------------------------------
  ! PUBLIC
  ! print usage message
  SUBROUTINE usage_res2vis
    USE parallel  ! par_rank
    IF (par_rank.EQ.0) THEN
       WRITE (*,'("Usage: res2vis [OPTIONS] [IN_FILE] [OUT_FILE]")')
       WRITE (*,'("       res2vis [OPTIONS] [IN_FILE] [TIME_END] [TIME_STEP]")')
       WRITE (*,'("       [mpirun -np [N_PROC]] res2vis -m [PARTITION METHOD] [OPTIONS] ...")')
       WRITE (*,'(" ")')
       WRITE (*,'(" ")')
       WRITE (*,'("Convert data from .res file(s) to a visualizable format.")')
       WRITE (*,'(" ")')
       WRITE (*,'("Help options:")')
       WRITE (*,'("  -h --help -?           show this message")')
       WRITE (*,'("Data options:")')
       WRITE (*,'("  -t                     apply inverse wavelet transform to show the data values")')
       WRITE (*,'("                         (wavelet coefficients are displayed by default)")')
       WRITE (*,'("  -i                     use integer space coordinates, according to the mesh")')
       WRITE (*,'("                         (coordinates are real by default)")')
       WRITE (*,'("  -g                     output wavelet level as an additional variable")')
       WRITE (*,'("  -d                     output domain number as an additional variable")')
       WRITE (*,'("  -s                     search each timestep in a separate subdirectory")')
       WRITE (*,'("                         (write the correspondent output files into these subdirectories)")')
       WRITE (*,'("  -S                     search each timestep in a separate subdirectory,")')
       WRITE (*,'("                         (write the correspondent output files into one directory)")')
!!$       WRITE (*,'("  -m <method>            REQUIRED FOR PARALLEL EXECUTION, ignored in serial: ")')
!!$       WRITE (*,'("                         from the number of processors, use that method for domain re-decomposition")')
!!$       WRITE (*,'("                         of input data; check main wavelet code manual for the available values")')
       WRITE (*,'("Output formats:")')
       WRITE (*,'("  -v                     VTK file (default)")')
       WRITE (*,'("  -p                     plot3D files")')
       WRITE (*,'("  -c                     CGNS file")')
       WRITE (*,'("  -E                     Ensight Gold file set (in development)")')
       WRITE (*,'("Additional output formats:")')
       WRITE (*,'("  -a                     VTK, plot3D, Ensight(in development): the output file is ASCII (binary by default)")')
       WRITE (*,'("  -r                     VTK, CGNS:   use tetrahedra (blocks will be written by default)")')
       WRITE (*,'("Additional filtering, before the transform:")')
       WRITE (*,'("  -e  <threshold_value> <number_of_variable_for_threshold>")')
       WRITE (*,'("  -l  <maximum_level_to_use>")')
       WRITE (*,'("Files:")')
       WRITE (*,'("  <input_result_file> <output_filename>")')
       WRITE (*,'("  <first_timestep_input_result_file> <last_timestep> <timestep_increment>")')
       WRITE (*,'("Debug options:")')
       WRITE (*,'("  -V  <verb_level>       level of verbosity while reading .res file (0-default, 1, 2)")')
       WRITE (*,'("  -D  <debug_level>      bitwise internal flag for the transform etc. (0-default)")')
       WRITE (*,'("  -P                     preserve original domains even if running on a single processor")')
       WRITE (*,'("  -R  <max_rand>         randomize domain numbers up to that limit (positive integer)")')
       WRITE (*,'(" ")')
       WRITE (*,'("Examples:")')
       WRITE (*,'(" 1) Read single timestep single processor .res file on a single processor:")')
       WRITE (*,'("  res2vis -vpa -e 0.15 4 ../res/input.0001.res  out")')
       WRITE (*,'("    produces both VTK and plot3D ASCII files (out.vtk, out.q, out.xyz)")')
       WRITE (*,'("    and the data will be filtered by applying the threshold 0.15 ")')
       WRITE (*,'("    to the variable number 4 of the result file")')
       WRITE (*,'(" 2) Read many single processor .res files (for several timesteps) on a single processor:")')
       WRITE (*,'("  res2vis res/in.0000.res 6 3 -t")')
       WRITE (*,'("    writes data as VTK files (in.0000.vtk, in.0003.vtk, in.0006.vtk)")')
       WRITE (*,'("    into ./res/ directory for each of the input file from the directory ./res/")')
       WRITE (*,'("    (in.0000.res, in.0003.res, in.0006.res).")')
       WRITE (*,'(" 3) Read several (written by a parallel code) single timestep .res files on a single processor:")')
       WRITE (*,'("  res2vis -vpa -e 0.15 4 ../res/input.0001.p0.res  out")')
       WRITE (*,'("    similar to the example 1; the difference is that the data from all the multi-processor files:")')
       WRITE (*,'("    input.0001.p0.res, input.0001.p1.res, input.0001.p2.res, etc, will be processed")')
       WRITE (*,'(" 4) Read many multi-processor .res files (for several timesteps) on a single processor:")')
       WRITE (*,'("  res2vis res/in.0000.p0.res 6 3 -t")')
       WRITE (*,'("    similar to the example 2; the difference is that each timestep is represented by several,")')
       WRITE (*,'("    multi-processor, files ...p0.res, ...p1.res, etc.")')
       WRITE (*,'("Parallel examples:")')
       WRITE (*,'("    Each processor reads one input and writes one output file.")')
       WRITE (*,'(" 1) Single timestep processing:")')
       WRITE (*,'("  mpirun -np 2 res2vis in.0001.p0.res out")')
       WRITE (*,'("    each processor read its own input (in.0001.p0.res, in.0001.p1.res)")')
       WRITE (*,'("    each processor write its own VTK file (out.p0.vtk, out.p1.vtk)")')
       WRITE (*,'(" 2) Several timesteps processing:")')
       WRITE (*,'("  mpirun -np 2 res2vis results/in.0000.p0.res 5 5 -t")')
       WRITE (*,'("    two processors write: in.0000.p0.vtk, in.0000.p0.vtk, in.0000.p1.vtk, in.0005.p1.vtk")')
       WRITE (*,'("    into ./results/ directory for each of the input file from the directory ./results/")')
       WRITE (*,'("    (in.0000.p0.res, in.0005.p0.res, in.0000.p1.res, in.0005.p1.res) ")')
       WRITE (*,'("Several timesteps:")')
       WRITE (*,'("    -s or -S will search each timestep file(s) in a separate subdirectory, which name is the tumestep number")')
       WRITE (*,'(" ")')
       WRITE (*,'("Warning:")')
       WRITE (*,'("    In a parallel run of res2vis, make sure that the number of processors is equal")')
       WRITE (*,'("    to the number of .res input files. Each processor expects one .res input file!")')
       WRITE (*,'("    Use a separate n2m, or NtoMonP, tool to convers N .res files produced on N processors")')
       WRITE (*,'("    into M .res files for a possible restart or visualization on M processors.")')

    END IF
    CALL parallel_finalize
    STOP 0
  END SUBROUTINE usage_res2vis
  
  !------------------------------------------------------------------------------------------------
  !------------------------------------------------------------------------------------------------
  ! PUBLIC
  ! print usage message
  SUBROUTINE usage_int
    USE parallel   ! par_rank
    IF (par_rank.EQ.0) THEN
       WRITE (*,'("Usage: int [OPTIONS] [.RES_FILE] [.REN_FILE]")')
       WRITE (*,'("Data options:")')
       WRITE (*,'("  -w                   do not apply inverse transform, show coefficients only")')
       WRITE (*,'("                         (the default is to show the real values after the inverse transform)")')
       WRITE (*,'("  -i                   use integer space coordinates, according to the mesh")')
       WRITE (*,'("                         (coordinates are real by default)")')
       WRITE (*,'("")')
       WRITE (*,'("  -p MODE              preprocessing mode:")')
       WRITE (*,'("                         0 - direct, no preprocessing")')
       WRITE (*,'("                         1 - preprocess uniform grid")')
       WRITE (*,'("                         2 - preprocess nonuniform grid")')
       WRITE (*,'("                         3 - (default MODE) preprovess .res -> .ren for the volume renderer")')
       WRITE (*,'("")')
       WRITE (*,'("  -v NUM               create .ren file for that variable:")')
       WRITE (*,'("                         .ren file is created for a single scalar variable")')
       WRITE (*,'("                         default value of NUM is 1")')
       WRITE (*,'("")')
       WRITE (*,'("  -o ORDER             interpolation order:")')
       WRITE (*,'("                         0 - (default ORDER) linear")')
       WRITE (*,'("                         1 - 3-rd order")')
       WRITE (*,'("                         ")')
       WRITE (*,'("  -d FLAG              user provided input flag:")')
       WRITE (*,'("                         0 - user provided format")')
       WRITE (*,'("                         1 - RES_IO format")')
       WRITE (*,'("                         2 - dedug: predefined diagonal with array file writing/reading")')
       WRITE (*,'("                         3 - debug: predefined diagonal without using array file")')
       WRITE (*,'("                         4 - (default FLAG) no input, preprocess for rendering only")')
       WRITE (*,'("")')
       WRITE (*,'("  -f ARRAY_FILE        filename for the user provided array of points to interpolate into")')
       WRITE (*,'("")')
       WRITE (*,'("Additional options:")')
       WRITE (*,'("  -e EPSILON NUM       threshold filtering, before the transform:")')
       WRITE (*,'("                         EPSILON - real threshold value")')
       WRITE (*,'("                         NUM     - number of the variable to threshold for")')
       WRITE (*,'("  -l LEVEL             maximum level filtering, before the transform:")')
       WRITE (*,'("                         LEVEL   - the maximum level to use")')
       WRITE (*,'("")')
       WRITE (*,'("Example:")')
       WRITE (*,'("  int -e 0.15 4 ../res/input.0001.res out.ren -v 2")')
       WRITE (*,'("    The data will be filtered by applying the threshold 0.15 to the")')
       WRITE (*,'("    variable #4 of .res file. For the variable #2 create out.ren")')
       WRITE (*,'("    file for the future use with volume renderer.")')
       WRITE (*,'("    ")')
       WRITE (*,'("  int ../res/input.0001.res -p 2 -d 1 -f array.dat")')
       WRITE (*,'("    Interpolate the real values for the variables and into the points")')
       WRITE (*,'("    as given in the file array.dat written in RES_IO format.")')
       WRITE (*,'("    Preprocess data before the interpolation for the")')
       WRITE (*,'("    general case of nonuniform grid.")')
       WRITE (*,'("    ")')
    END IF
    CALL parallel_finalize
    STOP 0
  END SUBROUTINE usage_int

  !------------------------------------------------------------------------------------------------
  !------------------------------------------------------------------------------------------------
  ! PUBLIC
  ! read command line parameters
  SUBROUTINE read_file_type_res2vis (VTK_OUTPUT, PLOT_3D_OUTPUT, IS_BINARY, eps_filter, eps_num, &
       SINGLE_TIMESTEP, input_file_name, output_file_name, par_name, original_input_file_name, &
       begin_station_num, end_station_num, step_station_num, &
       eps_max_level, TRANSFORM_TO_REAL, COORD_INTEGER, MAKE_TETRAHEDRA, CGNS_OUTPUT, ADD_LEVEL, &
       ADD_DOMAIN, DO_APPEND, io_verb_level, debug_level, PRESERVE_DECOMPOSITION, &
       rand_domain_limit, ENSGHT_OUTPUT, RE_PARTITION, DO_TIMESTEP_SUBDIR, DO_OUTPUT_SUBDIR)
    USE precision
#ifdef _MSC_VER
    USE DFLIB         ! getarg()
#endif
    USE parallel      ! par_rank_str
    LOGICAL, INTENT (INOUT) :: VTK_OUTPUT, PLOT_3D_OUTPUT, IS_BINARY, SINGLE_TIMESTEP, &
         TRANSFORM_TO_REAL, COORD_INTEGER, MAKE_TETRAHEDRA, CGNS_OUTPUT, ADD_LEVEL, &
         ADD_DOMAIN, DO_APPEND, PRESERVE_DECOMPOSITION, ENSGHT_OUTPUT, RE_PARTITION, &
         DO_TIMESTEP_SUBDIR, DO_OUTPUT_SUBDIR
    REAL(pr), INTENT (INOUT) :: eps_filter
    INTEGER, INTENT(INOUT) :: eps_num, begin_station_num, end_station_num, step_station_num, &
         eps_max_level, io_verb_level, debug_level, rand_domain_limit
    CHARACTER(LEN=MAXLEN), INTENT(INOUT) :: input_file_name, output_file_name, par_name, original_input_file_name
    INTEGER, PARAMETER :: MAX_NAMES = 3
    CHARACTER (LEN=MAXLEN) :: tmp, name(1:MAX_NAMES)
    INTEGER :: i, j, name_counter, iargc
   
    LOGICAL :: domain_meth_set = .FALSE.
 
    ! default values -------
    begin_station_num = 1             ! enter the loop once
    end_station_num = 1               !
    step_station_num = 1              !
    eps_filter = 0.0                  ! no additional filtering, so the
    eps_num = 1                       !  variable number is not important
    VTK_OUTPUT = .FALSE.              ! VTK output
    PLOT_3D_OUTPUT = .FALSE.          ! PLOT3D output
    CGNS_OUTPUT = .FALSE.             ! CGNS output
    ENSGHT_OUTPUT = .FALSE.              ! Ensight Gold output
    IS_BINARY = .TRUE.                ! binary output
    SINGLE_TIMESTEP = .TRUE.          ! single input file (single timestep processed)
    eps_max_level = 0                 ! indicator of no filtering by maximum level
    TRANSFORM_TO_REAL = .FALSE.       ! show wavelet coefficients only
    COORD_INTEGER = .FALSE.           ! coordinates are real
    MAKE_TETRAHEDRA = .FALSE.         ! tetrahedra will be written
    ADD_LEVEL = .FALSE.               ! level is not an additional variable
    ADD_DOMAIN = .FALSE.              ! processor's rank as an additional variable
    DO_APPEND = .TRUE.                ! read all relevant .res files and append the data
    io_verb_level = 0                 ! verb level to be passed to read_solution()
    debug_level = 0                   ! debug_level to be passed to the transform etc.
    PRESERVE_DECOMPOSITION = .FALSE.  ! preserve domain data even if read at single processor
    rand_domain_limit = 0             ! no maximum => no randomization of domain numbers
    RE_PARTITION = .FALSE.            ! re-partition a parallel domain 
    DO_TIMESTEP_SUBDIR = .FALSE.      ! all timesteps .res files are in the same directory
    DO_OUTPUT_SUBDIR = .FALSE.        ! output files are in the same directory
    ! default values -------

#ifdef MULTIPROC
    ! Initialize, domain splitting
    domain_split(:) = 1
    domain_split(2) = 0
#endif

    i = 1
    name_counter = 0
    DO WHILE ( i.LE.iargc() )
       CALL getarg( i, tmp )
       IF ( tmp(1:1).EQ.'-' ) THEN ! ---- parameter
          DO j=2,LEN(TRIM(tmp))
             IF ( ( tmp(j:j).EQ.'h' ).OR. &                     ! -h -help
                  ( tmp(j:j).EQ.'-' ).OR. &                     ! --help
                  ( tmp(j:j).EQ.'?' ) ) CALL usage_res2vis      ! -?
             IF ( tmp(j:j).EQ.'i' ) COORD_INTEGER = .TRUE.
             IF ( tmp(j:j).EQ.'v' ) VTK_OUTPUT = .TRUE.
             IF ( tmp(j:j).EQ.'p' ) PLOT_3D_OUTPUT = .TRUE.
             IF ( tmp(j:j).EQ.'c' ) CGNS_OUTPUT = .TRUE.
             IF ( tmp(j:j).EQ.'E' ) ENSGHT_OUTPUT = .TRUE.
             IF ( tmp(j:j).EQ.'a' ) IS_BINARY = .FALSE.
             IF ( tmp(j:j).EQ.'t' ) TRANSFORM_TO_REAL = .TRUE.
             IF ( tmp(j:j).EQ.'r' ) MAKE_TETRAHEDRA = .TRUE.
             IF ( tmp(j:j).EQ.'g' ) ADD_LEVEL = .TRUE.
             IF ( tmp(j:j).EQ.'d' ) ADD_DOMAIN = .TRUE.
             IF ( tmp(j:j).EQ.'P' ) PRESERVE_DECOMPOSITION = .TRUE.
             IF ( ( tmp(j:j).EQ.'s' ) .OR. &
                  ( tmp(j:j).EQ.'S' ) ) &
                  DO_TIMESTEP_SUBDIR = .TRUE.
             IF ( tmp(j:j).EQ.'s' ) DO_OUTPUT_SUBDIR = .TRUE.
#ifdef MULTIPROC
             IF ( tmp(j:j).EQ.'m' ) THEN
                domain_meth_set = .TRUE. 
                i = i + 1
                CALL getarg( i, tmp )
                READ( UNIT=tmp, FMT=NUM_FMT, ERR=3 ) domain_meth
                IF( domain_meth .NE. -1 ) RE_PARTITION = .TRUE. 
             END IF
#endif
             IF ( tmp(j:j).EQ.'e' ) THEN
                i = i + 1
                CALL getarg( i, tmp )
                READ( UNIT=tmp, FMT=EPS_FMT, ERR=1 ) eps_filter
                i = i + 1
                CALL getarg( i, tmp )
                READ( UNIT=tmp, FMT=NUM_FMT, ERR=2 ) eps_num
             END IF
             IF ( tmp(j:j).EQ.'l' ) THEN
                i = i + 1
                CALL getarg( i, tmp )
                READ( UNIT=tmp, FMT=NUM_FMT, ERR=3 ) eps_max_level
             END IF
             IF ( tmp(j:j).EQ.'V' ) THEN
                i = i + 1
                CALL getarg( i, tmp )
                READ( UNIT=tmp, FMT=NUM_FMT, ERR=3 ) io_verb_level
             END IF
             IF ( tmp(j:j).EQ.'D' ) THEN
                i = i + 1
                CALL getarg( i, tmp )
                READ( UNIT=tmp, FMT=NUM_FMT, ERR=3 ) debug_level
             END IF
             IF ( tmp(j:j).EQ.'R' ) THEN
                i = i + 1
                CALL getarg( i, tmp )
                READ( UNIT=tmp, FMT=NUM_FMT, ERR=3 ) rand_domain_limit
             END IF
          END DO
       ELSE ! ---- file name
          name_counter = name_counter + 1
          IF (name_counter.GT.MAX_NAMES) THEN
             WRITE (*,'("Error: too many arguments")')
             WRITE (*,'(" ")')
             CALL usage_res2vis
          END IF
          CALL getarg( i, name(name_counter))
       END IF
       i = i + 1
    END DO
    IF (name_counter.EQ.2) THEN                  ! "input.0001[.p0].res   out"
       SINGLE_TIMESTEP = .TRUE.
       input_file_name = TRIM(name(1))
       original_input_file_name = TRIM(name(1))
       output_file_name = TRIM(name(2))
    ELSE IF (name_counter.EQ.3) THEN             ! "res/in.0000[.p0].res   6   3"
       SINGLE_TIMESTEP = .FALSE.
       input_file_name = TRIM(name(1))
       original_input_file_name = TRIM(name(1))
       CALL get_begin_station_num ( begin_station_num, input_file_name, par_name )
       READ( UNIT=name(2), FMT=NUM_FMT, ERR=3 ) end_station_num
       IF (begin_station_num.GT.end_station_num) THEN
          WRITE (*,'("Error: first timestep should not be greater than the last one")')
          WRITE (*,'(" ")')
          CALL usage_res2vis
       END IF
       READ( UNIT=name(3), FMT=NUM_FMT, ERR=3 ) step_station_num
       step_station_num = MAX(1,step_station_num)
    ELSE
       WRITE (*,'("Error: strange number of arguments")')
       WRITE (*,'(" ")')
       CALL usage_res2vis
    END IF

    ! default values -------
    IF ((.NOT.VTK_OUTPUT).AND.(.NOT.PLOT_3D_OUTPUT).AND.(.NOT.CGNS_OUTPUT).AND.(.NOT.ENSGHT_OUTPUT)) &
         VTK_OUTPUT = .TRUE.
    ! default values -------

    !PRINT *, 'in read: input_file_name = ', par_rank, TRIM(input_file_name)
    ! --- WARNING ---
    IF (par_size.GT.1) THEN
       ! Multiprocessor res2vis run assumes one input file per processor,
       ! though it is called as 'mpirun ... res2vis ... {some.NUM}.p{NN}.res'.
       ! We substitute ... p{NN}.res by the current p{rank} processor string.
       DO_APPEND = .FALSE.
       IF (SINGLE_TIMESTEP) THEN
          input_file_name = input_file_name( 1:INDEX(input_file_name(1:LEN_TRIM(input_file_name)-4),'.',BACK=.TRUE.)-1 ) &
               //TRIM(par_rank_str)//'.res'
       ELSE
          par_name = TRIM(par_rank_str)
       END IF
    END IF
#ifdef MULTIPROC
             !------------------------------------------------------------
             ! WARNING: temporal fix
             domain_meth_set = .TRUE.
             domain_meth = -1
             RE_PARTITION = .FALSE.
             !------------------------------------------------------------

    IF(RE_PARTITION .AND. PRESERVE_DECOMPOSITION ) THEN
       IF( par_rank .EQ. 0 ) THEN
          WRITE (*,'("WARNING: Cannot re-partition the domain AND preserve the original decomposition.")')
          WRITE (*,'("WARNING: Re-partitioning")')
          WRITE (*,'(" ")')
       END IF
       PRESERVE_DECOMPOSITION = .FALSE.
    END IF
    IF( domain_meth_set .EQ. .FALSE. ) THEN
       IF( par_rank .EQ. 0 ) THEN
          WRITE (*,'("Error: Parallel operation requires specification of domain partitioning method.")') 
          WRITE (*,'("  Please set -m flag. ( -m -1 to retain same partitioning as.res files, though requires")') 
          WRITE (*,'("  res2vis to be run on the same number of processors).")')
          WRITE (*,'(" ")')
          WRITE (*,'("  See res2vis --help for other options")')
          WRITE (*,'(" ")')
       END IF
       !CALL usage_res2vis
        CALL parallel_finalize
        STOP 0
    END IF
#endif
    ! --- WARNING ---
    !PRINT *, 'in read: input_file_name = ', par_rank, TRIM(input_file_name)
    

    RETURN
1   WRITE (*,'("Error: strange treshold value")')
    WRITE (*,'("")')
    CALL usage_res2vis
2   WRITE (*,'("Error: strange treshold variable number")')
    WRITE (*,'("")')
    CALL usage_res2vis
3   WRITE (*,'("Error: strange integer value")')
    WRITE (*,'("")')
    CALL usage_res2vis
  END SUBROUTINE read_file_type_res2vis
  !------------------------------------------------------------------------------------------------
  ! PRIVATE
  ! extract begin_station_num from filename
  ! set input_file_name
  SUBROUTINE get_begin_station_num ( begin_station_num, input_file_name, pr_name )
    CHARACTER(LEN=MAXLEN), INTENT(INOUT) :: input_file_name, &       ! IN   e.g. res/in.0005[.p0].res
         pr_name                                                     ! OUT  p000 string
    INTEGER, INTENT(INOUT) :: begin_station_num                      ! OUT  station (e.g. 5)
    CHARACTER(LEN=MAXLEN) :: tmp
    INTEGER :: dpl, dpr                           ! dot position case.0002.p0.res, case.0002.res
    !                                             ! dpl =                 ^      ,     ^
    dpr = LEN_TRIM(input_file_name)-3             ! dpr =                    ^   ,          ^
    dpl = INDEX(input_file_name(1:LEN_TRIM(input_file_name)-4),'.',BACK=.TRUE.)
    pr_name = ''
    IF ( (input_file_name(dpl+1:dpl+1).EQ.'p') .OR. &
         (input_file_name(dpl+1:dpl+1).EQ.'c') ) THEN
       pr_name = input_file_name(dpl:dpr-1)
       dpr = dpl
       dpl = INDEX(input_file_name(1:dpl-1),'.',BACK=.TRUE.)
    END IF
    tmp = input_file_name(dpl+1:dpr-1)
    READ( UNIT=tmp, FMT=NUM_FMT, ERR=1 ) begin_station_num

    input_file_name = TRIM( input_file_name( 1:dpl ) )

    RETURN
1   WRITE (*,'("Error: first timestep strange number")')
    WRITE (*,'("")')
    CALL usage_res2vis
  END SUBROUTINE get_begin_station_num

  !------------------------------------------------------------------------------------------------
  !------------------------------------------------------------------------------------------------
  ! PUBLIC
  ! read command line parameters
  SUBROUTINE read_file_type_int ( eps_filter, eps_num, eps_max_level, &
       input_res_name, input_array_name, output_ren_name, variable_to_render, &
       TRANSFORM_TO_REAL, COORD_INTEGER, &
       preprocess_mode, interpolation_order, debug_flag )
    USE precision
#ifdef _MSC_VER
    USE DFLIB
#endif
    LOGICAL, INTENT (OUT) :: TRANSFORM_TO_REAL, COORD_INTEGER
    REAL(pr), INTENT (OUT) :: eps_filter
    INTEGER, INTENT (OUT) :: eps_num, eps_max_level, preprocess_mode, interpolation_order, debug_flag, &
         variable_to_render
    CHARACTER(LEN=MAXLEN), INTENT(INOUT) :: input_res_name, input_array_name, output_ren_name
    INTEGER, PARAMETER :: MAX_NAMES = 2
    CHARACTER (LEN=MAXLEN) :: tmp, name(1:MAX_NAMES)
    INTEGER :: i,j,name_counter,iargc
    
    ! default values -------
    eps_filter = 0.0                  ! no additional filtering, so the
    eps_num = 1                       !  variable number is not important
    eps_max_level = 0                 ! indicator of no filtering by maximum level
    TRANSFORM_TO_REAL = .TRUE.        ! make wavelet transform
    COORD_INTEGER = .FALSE.           ! coordinates are real
    preprocess_mode = 3               ! preprocess, nonuniform grid
    interpolation_order = 0           ! linear interpolation
    debug_flag = 4                    ! preprocess for rendering only
    variable_to_render = 1            ! create .ren file for that variable
    input_array_name = ' '
    output_ren_name = ' '
    ! default values -------

    i = 1
    name_counter = 0
    DO WHILE ( i.LE.iargc() )
       CALL getarg( i, tmp )
       IF ( tmp(1:1).EQ.'-' ) THEN ! ---- parameter
          DO j=2,LEN(TRIM(tmp))
             IF ( tmp(j:j).EQ.'w' ) TRANSFORM_TO_REAL = .FALSE.
             IF ( tmp(j:j).EQ.'i' ) COORD_INTEGER = .TRUE.
             IF ( tmp(j:j).EQ.'v' ) THEN
                i = i + 1
                CALL getarg( i, tmp )
                READ( UNIT=tmp, FMT=NUM_FMT, ERR=7 ) variable_to_render
             END IF
             IF ( tmp(j:j).EQ.'d' ) THEN
                i = i + 1
                CALL getarg( i, tmp )
                READ( UNIT=tmp, FMT=NUM_FMT, ERR=6 ) debug_flag
             END IF
             IF ( tmp(j:j).EQ.'o' ) THEN
                i = i + 1
                CALL getarg( i, tmp )
                READ( UNIT=tmp, FMT=NUM_FMT, ERR=5 ) interpolation_order
             END IF
             IF ( tmp(j:j).EQ.'p' ) THEN
                i = i + 1
                CALL getarg( i, tmp )
                READ( UNIT=tmp, FMT=NUM_FMT, ERR=3 ) preprocess_mode
             END IF
             IF ( tmp(j:j).EQ.'e' ) THEN
                i = i + 1
                CALL getarg( i, tmp )
                READ( UNIT=tmp, FMT=EPS_FMT, ERR=1 ) eps_filter
                i = i + 1
                CALL getarg( i, tmp )
                READ( UNIT=tmp, FMT=NUM_FMT, ERR=2 ) eps_num
             END IF
             IF ( tmp(j:j).EQ.'l' ) THEN
                i = i + 1
                CALL getarg( i, tmp )
                READ( UNIT=tmp, FMT=NUM_FMT, ERR=4 ) eps_max_level
             END IF
             IF ( tmp(j:j).EQ.'f' ) THEN
                i = i + 1
                CALL getarg( i, input_array_name )
             END IF
          END DO
       ELSE ! ---- file name
          name_counter = name_counter + 1
          IF (name_counter.GT.MAX_NAMES) &
               CALL err_int ("too many names")
          CALL getarg( i, name(name_counter))
       END IF
       i = i + 1
    END DO
    IF (name_counter.EQ.2) THEN         ! INPUT_RES_NAME & OUTPUT_REN_NAME are present
       input_res_name = TRIM(name(1))
       output_ren_name = TRIM(name(2))
    ELSE IF (name_counter.EQ.1) THEN    ! INPUT_RES_NAME is present
       input_res_name = TRIM(name(1))
       output_ren_name = TRIM(name(1))//'.ren'
    ELSE
       CALL err_int ("strange number of names")
    END IF

    ! default values -------
    ! default values -------

    RETURN
1   CALL err_int ("strange treshold value")
2   CALL err_int ("strange treshold variable number")
3   CALL err_int ("strange preprocessing mode")
4   CALL err_int ("strange maximum level to use")
5   CALL err_int ("strange interpolation order")
6   CALL err_int ("strange user provided input flag")
7   CALL err_int ("strange variable number")
  END SUBROUTINE read_file_type_int
  
  !------------------------------------------------------------------------------------------------
  !------------------------------------------------------------------------------------------------
  ! PRIVATE
  ! print error message
  SUBROUTINE err_int ( msg )
    CHARACTER*(*) msg
    WRITE (*,'("Error: ",A)') msg
    WRITE (*,'("")')
    CALL usage_int
  END SUBROUTINE err_int

  !------------------------------------------------------------------------------------------------
  !------------------------------------------------------------------------------------------------
  ! PRIVATE
  ! add some non-periodic nodes
  RECURSIVE SUBROUTINE expand ( iwlt, n_var, xyz, prd_old, db_j_max, var_add_mode, j, i_level, i_domain )
    USE wlt_vars                                             ! dim
    USE field                                                ! u
    USE db_tree_vars                                         ! node pointers
    USE parallel                                             ! par_size
    INTEGER, INTENT(IN) :: iwlt, n_var, &
         xyz(1:dim), prd_old(1:dim), db_j_max, var_add_mode, &
         j, i_level, i_domain
    REAL*8 :: f_in(1:n_var)
    INTEGER :: ierror, i, xyz_e(1:dim), domain
    
#ifdef TREE_NODE_POINTER_STYLE_C
    INTEGER(pointer_pr) :: c_ptr                                ! C style node pointer
#elif defined TREE_NODE_POINTER_STYLE_F
    TYPE(node), POINTER :: c_ptr                                ! (Fortran's native pointer)
#endif
    
    DO i=1,dim
       IF ( (xyz(i).EQ.0).AND.(prd_old(i).EQ.1) ) THEN
          xyz_e(1:dim) = xyz(1:dim)
          xyz_e(i) = nxyz(i)

          IF (var_add_mode.EQ.0) &
               f_in(1:n_var) = u(iwlt,1:n_var)
          CALL DB_add_node ( xyz_e, db_j_max, ierror, c_ptr, id_sig, 1 )

          !CALL DB_get_proc_by_coordinates ( xyz_e, db_j_max, domain )

#ifdef TREE_NODE_POINTER_STYLE_C
          IF (var_add_mode.EQ.0) THEN
             CALL DB_set_function_by_pointer ( c_ptr, 1, n_var, f_in )
          ELSE IF (var_add_mode.EQ.1) THEN
             CALL DB_set_ifunction_by_pointer ( c_ptr, 1, 1, iwlt )
          END IF
          IF (i_level.GT.0) CALL DB_set_ifunction_by_pointer ( c_ptr, i_level, i_level, j )
          IF (i_domain.GT.0) THEN
             CALL DB_get_proc_by_coordinates ( xyz_e, db_j_max, domain )
             CALL DB_set_ifunction_by_pointer ( c_ptr, i_domain, i_domain, domain )
          END IF
#elif defined TREE_NODE_POINTER_STYLE_F
          IF (var_add_mode.EQ.0) THEN
             c_ptr%val (1:n_var) = f_in(1:n_var)
          ELSE IF (var_add_mode.EQ.1) THEN
             c_ptr%ival(1) = iwlt
          END IF
          IF (i_level.GT.0) c_ptr%ival(i_level) = j
          IF (i_domain.GT.0) THEN
             CALL DB_get_proc_by_coordinates ( xyz_e, db_j_max, domain )
             c_ptr%ival(i_domain) = domain
          END IF
#endif

          IF ( ANY(xyz_e(1:dim).EQ.0) ) &
               CALL expand ( iwlt, n_var, xyz_e, prd_old, db_j_max, var_add_mode, j, i_level, i_domain )
       END IF
    END DO
  END SUBROUTINE expand

END MODULE code_aux


!-------------------------------------------------------!
! define whether to use C or Fortran style node pointer !
#ifdef TREE_NODE_POINTER_STYLE_C
#undef TREE_NODE_POINTER_STYLE_C
#endif
#ifdef TREE_NODE_POINTER_STYLE_F
#undef TREE_NODE_POINTER_STYLE_F
#endif
! define whether to use C or Fortran style node pointer !
!-------------------------------------------------------!
