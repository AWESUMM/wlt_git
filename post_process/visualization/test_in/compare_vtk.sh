# compare two binary VTK files
#  Arguments:
#   $1               - first file
#   $2               - second file

if [ $# -ne 2 ]; then exit 1; fi

# Output the status of an executed operation
function print_status() {
    if [ $1 -eq 0 ]; then
	echo -ne "  [OK]     \t"
    elif [ $1 -eq 2 ]; then
	echo -ne "  [WARNING]\t"
    else
	echo -ne "  [FAILED] \t"
    fi
    echo -e "$2"
}

# compare number of points
points1=`\grep -a POINTS $1`
points2=`\grep -a POINTS $2`
ps=0; if [ "$points1" != "$points2" ]; then ps=1; fi
print_status $ps "(VTK files are different) compare number of points"

# compare number of cells
cells1=`\grep -a CELLS $1`
cells2=`\grep -a CELLS $2`
ps=0; if [ "$cells1" != "$cells2" ]; then ps=1; fi
print_status $ps "(VTK files are different) compare number of cells"

exit 0
