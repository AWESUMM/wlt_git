#!/bin/sh
# tests for res2vis visualization tool
TITLE="The testing suite for the code developers"
VERSION="070321"

# set (additional to -t) flags to run res2vis with
# (VTK files in results_good/ did not use any additional flags)
flags=""

function err {
    echo -e "\tRun as \"sh visualization/test_in/RUN.sh\" from the directory post_process/ or"
    echo -e "\tas \"sh test_in/RUN.sh\" from the directory visualization/ or"
    echo -e "\tas \"sh RUN.sh\" from the directory test_in/"
    echo -e "\tTwo optional parameters: output stream name, error stream name"
    exit 1
}

function print_status() {
    if [ $1 -eq 0 ]; then
	echo -ne "  [OK]    \t"
    else
	echo -ne "  [FAILED]\t"
    fi
    echo -e "$2"
}

# set output streams
o1=/dev/null
o2=/dev/null
if [ "$1" ]; then o1=$1; fi
if [ "$2" ]; then o2=$2; fi

# print invitation message
echo -e "\t$TITLE. Version $VERSION"

# check the current directory
if [ -f RUN.sh ]; then cd ../../
elif [ -f test_in/RUN.sh ]; then cd ../
elif [ ! -d visualization ]; then err
fi
cd visualization

echo "#--------------------------"
echo "#     res2vis test [ post_process/visualization/test_in/RUN.sh ]"
echo "#--------------------------"

# source the lists $list_db and $list_ca
source ../../TestCases/util/RUN_lists.txt 1>>$o1 2>>$o2

# initialize database pairs
db_pairs=`\sh ../../TestCases/util/init_pairs.sh $list_db`
np2=`echo $db_pairs|\wc -w`

# create test output directory
\mkdir -p TEST_LOGS

for ca in $list_ca; do
    echo -e "\n# CASE = $ca"
    cd=`echo $ca | \sed -e "s/\/.*//"`
    cn=`echo $ca | \sed -e "s/.*\///"`
    #-------------------------------------------------------------------------------------------
    # set correspondent result file names (without .res extension)
    if [ "$cn" = "case_small_vortex_array" ]; then fres="ccase_small_vortex_array_db_wrk.0005"
    elif [ "$cn" = "case_elliptic_poisson" ]; then fres="ccase_elliptic_poisson_db_wrk.0000"
    elif [ "$cn" = "case_channel_incompressible" ]; then fres=""
    elif [ "$cn" = "channel_compressible" ]; then fres=""
    else echo -e "  [WARNING]\t- testing script itself has to be modified according to the new list_ca"
    fi
    # this part will require modifications if list_ca is changed
    #-------------------------------------------------------------------------------------------

    for da in $list_db; do
	input_file="../../TestCases/${cd}/results_good/${fres}.res"
	output_file="TEST_LOGS/${fres}_${da}"
	# run res2vis tool
	if [ -s $input_file ]; then
	    # wavelet coefficients
	    if [ -s ${output_file}_coeff.vtk ]; then
		echo -e "  [SKIPPED]\trun:           res2vis_${da} ${flags}"
	    else
		../../TestCases/${cd}/res2vis_${da}_${cn}.out $input_file ${output_file}_coeff $flags 1>>$o1 2>>$o2
		print_status $? "run:           res2vis_${da} ${flags}"
	    fi

	    # solution (after inverse wavelet transform)
	    if [ -s ${output_file}.vtk ]; then
		echo -e "  [SKIPPED]\trun:           res2vis_${da} -t ${flags}"
	    else
		../../TestCases/${cd}/res2vis_${da}_${cn}.out $input_file ${output_file} -t $flags 1>>$o1 2>>$o2
		print_status $? "run:           res2vis_${da} -t ${flags}"
	    fi

	    # compare VTK output to saved good outputs
	    saved_file="results_good/${fres}"
	    if [ -s ${saved_file}_coeff.vtk ]; then
		#echo ${saved_file}_coeff.vtk ${output_file}_coeff.vtk
		\sh ../../TestCases/util/compare_exact_solns.sh \
		${saved_file}_coeff.vtk ${output_file}_coeff.vtk \
		"compare VTK files: ${saved_file}_coeff.vtk ${output_file}_coeff.vtk" 0 "no" >> $o1
		ps=$?
		if [ $ps -ne 0 ]; then
		    echo -e "# VTK files seems to be different\n# please examine log files in TEST_LOGS/ directory" >> $o1
		    \sh test_in/compare_vtk.sh ${saved_file}_coeff.vtk ${output_file}_coeff.vtk
		    if [ $? -ne 0 ]; then print_status 1 "internal error in compare_vtk.sh called form RUN.sh"; fi
		else
		    # files are identical
		    print_status $ps "compare wavelet coefficients, diff"
		fi
	    fi
	    if [ -s ${saved_file}.vtk ]; then
		#echo ${saved_file}.vtk ${output_file}.vtk
		\sh ../../TestCases/util/compare_exact_solns.sh \
		${saved_file}.vtk ${output_file}.vtk \
		"compare VTK files: ${saved_file}.vtk ${output_file}.vtk" 0 "no" >> $o1
		ps=$?
		if [ $ps -ne 0 ]; then
		    echo -e "# VTK files seems to be different\n# please examine log files in TEST_LOGS/ directory" >> $o1
		    \sh test_in/compare_vtk.sh ${saved_file}.vtk ${output_file}.vtk
		    if [ $? -ne 0 ]; then print_status 1 "internal error in compare_vtk.sh called form RUN.sh"; fi
		else
		    # files are identical
		    print_status $ps "compare solutions, diff"
		fi
	    fi
	fi
    done


done

exit 0
