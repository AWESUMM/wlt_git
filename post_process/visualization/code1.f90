! Convert *.res file into VTK, PLOT3D or CGNS format for
! processing with ParaView, VisIt, Tecplot or other software.
! by Alexei Vezolainen
! Oct 2006
PROGRAM res2vis
  USE precision
  USE share_consts
  USE pde
  USE sizes
  USE wlt_trns_mod              ! init_DB(), c_wlt_trns_mask()
  USE wlt_trns_vars             ! indx_DB, xx
  USE field                     ! u
  USE io_3D                     ! read solution dim()
  USE io_3D_vars                ! DO_READ_ALL_VARS_TRUE
  USE wlt_vars                  ! dim, j_lev, j_mx
  USE code_aux
  USE debug_vars                ! debug_level
  USE parallel                  ! MPI related subroutines
  USE variable_mapping 
  USE error_handling 
  
#if ((TREE_VERSION == 2) || (TREE_VERSION == 3))
  USE tree_database_f      ! use Fortran database instead of C/C++ tree
#endif
  
  IMPLICIT NONE
  
  REAL (pr), DIMENSION (:), POINTER :: scl               ! used in READ SOLUTION
  INTEGER   :: begin_station_num, end_station_num, &     ! generic numbers in *.res file name
       step_station_num, station_num, &                  ! 
       i, iargc, mult, ii, &
       n_var_i, &                          ! number of integer variables
       db_j_max                            ! maximum level in the tree database
  REAL (pr) :: eps_filter                  ! treshold for the purposes of visualization
  REAL (pr), ALLOCATABLE :: &
       tmp_minval(:), &
       tmp_maxval(:)
  INTEGER :: eps_num, &                    ! treshold is applied to that variable
       eps_max_level, &                    ! consider the wavelets up to that level
       n_var_res                           ! total number of variables in .res file
  
  
  INTEGER, PARAMETER :: MAXLEN = 512                    ! maximum length of command line argument
  CHARACTER(LEN=FILE_NUM_LEN) :: file_num_str           ! generic file number
  CHARACTER(LEN=MAXLEN) :: input_file_name, &           ! file names
       original_input_file_name, &                      !
       output_file_name, tmp, &                         !
       par_name, f_sub_name                                         !
  CHARACTER(LEN=1) :: asci_or_binary                    ! control parameters
  LOGICAL :: VTK_OUTPUT, PLOT_3D_OUTPUT, &              !
       IS_BINARY, SINGLE_TIMESTEP, TRANSFORM_TO_REAL, & !
       COORD_INTEGER, MAKE_TETRAHEDRA, &                !
       CGNS_OUTPUT, ENSGHT_OUTPUT, &
       SAVE_TREE, &                                     ! whether to keep initialization tree or not
       ADD_LEVEL, &                                     ! add level as another integer
       ADD_DOMAIN, &                                    ! add domain as another integer
       DO_APPEND, &                                     ! read all relevant .res files and append the data
       PRESERVE_DECOMPOSITION, &                        ! preserve domain data even if read at single processor
       RE_PARTITION, &                                  ! re-partition the domain
       DO_TIMESTEP_SUBDIR, &                            ! search each timestep file(s) in a separate subdirectory
       DO_OUTPUT_SUBDIR                                 ! write output files into separate subdirectories
  
  INTEGER :: RAND_DOMAIN_LIMIT, &                       ! max of random domain numbers (for better visualization)  
       itmp, &                                          ! temporal variable
       i_level, i_domain, &                             ! integer position of level/domain
       verb_read, &                                     ! verbouse .res file reading
       f_sub_pos

  REAL*8,ALLOCATABLE :: xx_c(:)                         ! array to pass to C subroutines
  CHARACTER (LEN=u_variable_name_len) , DIMENSION(:), ALLOCATABLE :: u_variable_names_tmp
  
  NULLIFY(scl)

  ignore_warnings = .TRUE.
  
  CALL parallel_init
  
  
  
  ! Read command line arguments
  ! Each processor assumed to have an access to the command line arguments
  IF( iargc() < 2 ) THEN
     CALL usage_res2vis
  ELSE
     CALL read_file_type_res2vis (VTK_OUTPUT, PLOT_3D_OUTPUT, IS_BINARY, eps_filter, eps_num, &
          SINGLE_TIMESTEP, input_file_name, output_file_name, par_name, original_input_file_name, &
          begin_station_num, end_station_num, step_station_num, &
          eps_max_level, TRANSFORM_TO_REAL, COORD_INTEGER, MAKE_TETRAHEDRA, CGNS_OUTPUT, ADD_LEVEL, &
          ADD_DOMAIN, DO_APPEND, verb_read, debug_level, PRESERVE_DECOMPOSITION, RAND_DOMAIN_LIMIT, ENSGHT_OUTPUT, &
          RE_PARTITION, DO_TIMESTEP_SUBDIR, DO_OUTPUT_SUBDIR)
  END IF
  
  
  
  ! Test if the input .res files were produced by the same number
  ! of processors we are using for visualization.
  CALL read_solution_dim( original_input_file_name, VERBLEVEL=verb_read, EXACT=.TRUE., IGNOREINIT=.TRUE., PARTREAD=4, CLEAN_IC_DD=.FALSE. )
  
  IF ( (par_size.NE.IC_par_size).AND.(par_size.NE.1) ) THEN
     ! this could be a run at 1 processor with reading multiple parallel files
     ! or currently unsupported internal NtoM option for res2vis
     IF (par_rank.EQ.0) THEN
        WRITE (*,'(" ")')
        WRITE (*,'("    Unsupported processing of ",I6," .res files at ",I6," processors.")') IC_par_size, par_size
        WRITE (*,'(" ")')
        WRITE (*,'("    In a parallel run of res2vis, make sure that the number of processors is equal")')
        WRITE (*,'("    to the number of .res input files. Each processor expects one .res input file!")')
        WRITE (*,'("    Use a separate n2m, or NtoMonP, tool to convert N .res files produced on N processors")')
        WRITE (*,'("    into M .res files for a possible restart or visualization on M processors.")')
     END IF
     CALL parallel_finalize
     STOP
  END IF
  
  
  IC_restart_mode = 0
  SAVE_TREE = (TYPE_DB.EQ.DB_TYPE_TREE_C).AND.ALL(IC_prd_global(1:dim).EQ.0)
  SAVE_TREE = .FALSE.
  
  ! Loop through each timestep
  ! (number of .res files equals to the number of processors for a parallel run)
  DO station_num = begin_station_num, end_station_num, step_station_num
     
     
     IF ( SINGLE_TIMESTEP ) THEN
        !
        ! mpirun -np NP ... res2vis ... ifile.0000.p0.res ofile
        !
        IC_filename = TRIM(input_file_name)
        tmp = TRIM( TRIM(output_file_name)//TRIM(par_rank_str) )
        
     ELSE
        !
        ! mpirun ... res2vis ... ifile.0000.p0.res N1 N2
        !
        WRITE(file_num_str,FILE_NUM_FMT) station_num
        IF (par_rank.EQ.0) PRINT *, 'Process station_num ',station_num
        IF (DO_TIMESTEP_SUBDIR) THEN
           ! 1. locate the position of /XXXX/ subdirectory
           ! 2. amend it for the current timestep
           ! 3. remove subdir for the output files (if necessary)
           f_sub_name = TRIM(input_file_name)
           IF (station_num .EQ. begin_station_num) &
                f_sub_pos = INDEX( f_sub_name(1:INDEX(f_sub_name,slash,BACK=.TRUE.)), file_num_str, BACK=.TRUE. )
           
           IC_filename = TRIM(  TRIM( input_file_name(1:f_sub_pos-1) ) &
                //file_num_str//TRIM( input_file_name(f_sub_pos+IT_STRING_LEN:LEN(input_file_name)) ) &
                //file_num_str//TRIM(par_name)//'.res' )
           
           IF (DO_OUTPUT_SUBDIR) THEN
              tmp = TRIM( TRIM(input_file_name)//file_num_str//TRIM(par_rank_str) )
           ELSE
              tmp = TRIM( TRIM( input_file_name(1:f_sub_pos-1) ) &
                   //     TRIM( input_file_name(f_sub_pos+1+IT_STRING_LEN:LEN(input_file_name)) ) &
                   //file_num_str//TRIM(par_rank_str) )
           END IF
           
        ELSE
           IC_filename = TRIM( TRIM(input_file_name)//file_num_str//TRIM(par_name)//'.res' )
           tmp = TRIM( TRIM(input_file_name)//file_num_str//TRIM(par_rank_str) )
        END IF
     END IF
     

     
     ! single processor processing
     ! read all the data from .res file (and .com, if necessary)
     
     ! parallel processing
     ! read common data from v5 .com file or v4 .res file
     
     CALL read_solution( scl, IC_filename, READ_ALL_VARS=.TRUE., READ_COMMONFILE=.TRUE., VERBLEVEL=verb_read, &
          EXACT=.TRUE., IGNOREINIT = .TRUE., APPEND=DO_APPEND, CLEAN_IC_DD=.FALSE. )
     
     CALL aux_0 ( TRANSFORM_TO_REAL, SAVE_TREE, RE_PARTITION, debug_level, pre_init_DB_only=.TRUE. )
     
     CALL read_solution( scl, IC_filename, READ_ALL_VARS=.TRUE., READ_COMMONFILE=.FALSE., VERBLEVEL=verb_read, &
          EXACT=.TRUE., IGNOREINIT = .TRUE., APPEND=DO_APPEND, CLEAN_IC_DD=.FALSE. )
     
     ! get the number of variables from the module variable_mapping
     n_var_res = get_n_var()
     

     
     ! add domain number and level
     ! as additional integer variables
     ! for .res file data
     n_var_i = 0                           ! all .res file variables are assumed to be real
     i_level = 0
     i_domain = 0
     IF (ADD_LEVEL) THEN
        n_var_i = n_var_i + 1  ! level is added as integer
        i_level = 1
     END IF
     IF (ADD_DOMAIN) THEN
        n_var_i = n_var_i + 1 ! domain is added as integer
        i_domain = i_level + 1
     END IF
     
     ! allow changes in number of variables (n_var+n_var_i)
     IF (ALLOCATED(u_variable_names_tmp)) THEN
        IF (n_var_res+n_var_i.GT.SIZE(u_variable_names_tmp)) DEALLOCATE (u_variable_names_tmp)
     END IF
     IF (.NOT.ALLOCATED(u_variable_names_tmp)) ALLOCATE (u_variable_names_tmp(n_var_res+n_var_i))
     
     u_variable_names_tmp(:) = ' '
     IF (ALLOCATED(u_variable_names)) THEN
        IF (SIZE(u_variable_names).GE.n_var_res) THEN
           u_variable_names_tmp(1:n_var_res) = u_variable_names(1:n_var_res)
        END IF
     END IF
     
     IF (ADD_LEVEL) THEN
        CALL set_variable_name_format()
        !WRITE (u_variable_names_fmt,  '( ''( A'', I3.3 , '')'' )') u_variable_name_len
        WRITE (u_variable_names_tmp(n_var_res+i_level), u_variable_names_fmt) 'Level   '
     END IF
     IF (ADD_DOMAIN) THEN
        CALL set_variable_name_format()
        !WRITE (u_variable_names_fmt,  '( ''( A'', I3.3 , '')'' )') u_variable_name_len
        WRITE (u_variable_names_tmp(n_var_res+i_domain), u_variable_names_fmt) 'Domain  '
     END IF
     
     ! allow changes in grid length (nxyz)
     IF (ALLOCATED(xx_c)) THEN
        IF (MAXVAL(nxyz(1:dim)).GT.SIZE(xx_c)) DEALLOCATE (xx_c)
     END IF
     IF (.NOT.ALLOCATED(xx_c)) ALLOCATE (xx_c(0:MAXVAL(nxyz(1:dim))))
     
     IF (par_rank.EQ.0.AND.debug_level.GT.0) THEN
        WRITE (*,'("------------------------------------ proc #", I5)') par_rank
        DO i=1,n_var_res
           WRITE (*,'("MIN/MAX(u_in (1:nwlt,",I2,")) = ",2E17.10)') i, MINVAL(u(1:nwlt,i)), MAXVAL(u(1:nwlt,i))
        END DO
     END IF
     

     
     
     ! filter wavelet coefficients before the transform
     ! ( operates with indx_DB and u )
     CALL aux__1 ( n_var_res, prd, eps_filter, eps_num, eps_max_level )
 

     ! perform inverse wavelet transform if required
     ! ( make all the required initializations;
     !   init_DB() will initialize it's own database according to gmake's DB parameter;
     !   c_wlt_trns_mask() will perform inverse transform;
     !   DB_finalize() will clean the initialized database if SAVE_TREE is false;
     !   tree to be saved for DB=db_tree and non-periodic case
     CALL aux_0 ( TRANSFORM_TO_REAL, SAVE_TREE, RE_PARTITION, debug_level, pre_init_DB_only=.FALSE. )
     
     u_variable_names_tmp(1:n_var_res) = u_variable_names(1:n_var_res)
     
     
     IF (debug_level .GT. 0) THEN
        ALLOCATE( tmp_minval(1:n_var_res) )
        ALLOCATE( tmp_maxval(1:n_var_res) )
        tmp_minval(1:n_var_res) = MINVAL( u(1:nwlt,1:n_var_res), DIM=2 )
        tmp_maxval(1:n_var_res) = MAXVAL( u(1:nwlt,1:n_var_res), DIM=2 )
        CALL parallel_vector_sum( REALMINVAL=tmp_minval, LENGTH=n_var_res )
        CALL parallel_vector_sum( REALMAXVAL=tmp_maxval, LENGTH=n_var_res )
        IF (par_rank.EQ.0) THEN
           WRITE (*,'("----------------------------------------------------")')
           DO i=1,n_var_res
              WRITE (*,'("MIN/MAX(u_out(1:nwlt,",I2,")) = ",2E17.10," ",A)') &
                   i, tmp_minval(i), tmp_maxval(i), TRIM(u_variable_names(i))
           END DO
        END IF
        DEALLOCATE( tmp_minval )
        DEALLOCATE( tmp_maxval )
     END IF
     
     
     
     
     ! find maximum level of the data to visualize
     ! ( it may be different from j_mx )
     db_j_max = 1
     itmp = mxyz(1)
     DO WHILE (itmp.LT.nxyz(1))
        itmp = itmp * 2
        db_j_max = db_j_max + 1
     END DO
     
     
     
     
     ! initialize tree database for AMR building purposes, if required
     ! ( this tree is the visualization tree,
     !   differrent from the tree for the inverse transform )
     CALL aux_init_vis_db( db_j_max, n_var_res, n_var_i, TRANSFORM_TO_REAL, SAVE_TREE )
     
     
     
     ! set variable names and real coordinates inside visualization tree;
     ! wlt tree has 2 integer values (by NvecI_DB in db_tree.f90) which may be
     ! overwritten by level and domain values in visualization tree
     ! (nxyz() <--> j_lev, though AMR expects coordinates at g->FINEST_MESH[] <--> j_mx,
     !  if tree has been saved, it is wlt tree which has been initialized to j_mx)
     mult = 1
     IF (db_j_max.LT.j_mx.AND.SAVE_TREE) THEN
        mult = 2**(j_mx-db_j_max)
        IF (ALLOCATED(xx_c)) DEALLOCATE (xx_c)
        ALLOCATE( xx_c(0:MAXVAL(nxyz(1:dim))*mult) )
        xx_c = 0.0_pr
     END IF
     itmp = 0
     IF (COORD_INTEGER) itmp = 1
     DO i=1,dim                                  ! xx(0:MAXVAL(nxyz),1:dim)
        xx_c(0:nxyz(i)) = xx(0:nxyz(i),i)
        IF (db_j_max.LT.j_mx) THEN               ! remap nxyz to FINEST_MESH
           DO ii=nxyz(i),1,-1
              xx_c(mult*ii) = xx_c(ii)
           END DO
        END IF
        CALL DB_amr_set_coordinates ( itmp, nxyz(i)*mult, i, xx_c )
     END DO
     DO i=1,n_var_res+n_var_i
        CALL DB_amr_set_variable_name ( i, &
             TRIM(ADJUSTL(u_variable_names_tmp(i))), LEN(TRIM(ADJUSTL(u_variable_names_tmp(i)))) )
     END DO
     
     
     ! add all significant nodes;
     ! make the data non-periodic if EXPAND_DATA is true;
     ! filter high level nodes if required
     ! ( uses indx_DB and visualization tree,
     !   fifth argument: store function values (0) or iwlt number (1) inside visualization tree
     !   seventh argument: store level as integer, and
     !   eigth argument: store domain as integer - used in res2vis )
     CALL aux_1 ( n_var_res, prd, eps_max_level, db_j_max, 0, &
          SAVE_TREE, &
          EXPAND_DATA=ANY(prd(1:dim).EQ.1), &
          ADD_LEVEL=ADD_LEVEL, &
          ADD_DOMAIN=ADD_DOMAIN, &
          P_D=PRESERVE_DECOMPOSITION, &
          RDL=RAND_DOMAIN_LIMIT)
     
     
     ! clear the memory after READ SOLUTION
     ! ( last arguments: store function values (0) or iwlt number (1) inside visualization tree
     !   effects here as KEEP_U=.FALSE. - deallocate u, KEEP_U=.TRUE - do not deallocate u;
     !   u_variable_names are not deallocated)
     CALL read_solution_free ( scl, KEEP_U=.FALSE., KEEP_NAMES=.TRUE. )
     
     ! make AMR by adding the missing nodes with zero values
     ! ( interpolation into the added nodes is required
     !   after the using of that subroutine;
     !   currently, no such interpolation is available, so
     !   the incomplete AMR cells are simply discarded )
     !CALL DB_make_amr
     
     IF (IS_BINARY) THEN
        asci_or_binary = 'b'
     ELSE
        asci_or_binary = 'a'
     END IF
     IF (MAKE_TETRAHEDRA) THEN
        itmp = 1
     ELSE
        itmp = 0
     END IF
     



     ! write input file name as a comment (will not have effect on PLOT3D files)
     ! and set number of variables to print into vtk/cgns/plot3d/xmgr file
     CALL DB_write_amr_comment ( TRIM(ADJUSTL(input_file_name)), LEN(TRIM(ADJUSTL(input_file_name))), n_var_res, n_var_i )
     
     IF (VTK_OUTPUT) &
          CALL DB_amr_vtk_write ( TRIM(ADJUSTL(tmp)), LEN(TRIM(ADJUSTL(tmp))), asci_or_binary, itmp )
     
     IF (PLOT_3D_OUTPUT) &
          CALL DB_amr_plot3d_write ( TRIM(ADJUSTL(tmp)), LEN(TRIM(ADJUSTL(tmp))), asci_or_binary, itmp )
     
     IF (CGNS_OUTPUT) &
          CALL DB_amr_cgns_write ( TRIM(ADJUSTL(tmp)), LEN(TRIM(ADJUSTL(tmp))), asci_or_binary, itmp  )

     IF (ENSGHT_OUTPUT) &
          CALL DB_amr_ensght_write ( TRIM(ADJUSTL(tmp)), LEN(TRIM(ADJUSTL(tmp))), asci_or_binary, itmp  )  
     
     
     ! clean the memory
     CALL parallel_sync()
     IF ( .NOT. SINGLE_TIMESTEP ) CALL DB_finalize

     
  END DO
  CALL release_memory_DB()
  IF( ALLOCATED( xx_c ) ) DEALLOCATE (xx_c)
  IF( ALLOCATED( u_variable_names_tmp ) )DEALLOCATE ( u_variable_names_tmp )

  CALL parallel_finalize
  
END PROGRAM res2vis



