! Stand-alone interpolation test.
! 
! Some subroutines might also be used from inside the main code for interpolation purposes.
!  In short, for db_tree compiled code use 1) DB_amr_set_coordinates(...) 2) set xarray(:,:),
!  farray(:,:), and which_var(:) 3) DB_interpolate(...)
! For other databases, initialize db_tree before each interpolation through DB_declare_globals(...),
!  populate the tree with the required nodes through aux_1(...), perform previously described
!  three stages of interpolation, clean the memory from the db_tree through DB_finalize(...)
! Interpolation for the main code compiled with databases other that db_tree
!  is expected to be very slow.
!
! The stand-alone interpolation mode:
!  For a given result file and a given file with a properly formatted array
!  of coordinate points (see module user_array_format in user_array.local.f90
!  for the format details) it will interpolate function value to these points.
!
! Implementation: like in 'int', although, C tree is initialized with a single
!  integer variable to store iwlt index, as directed by ADD_VAR_MARKER.
!  Currently, int2 is the only supported stand-alone interpolation main code.
!
! by Alexei Vezolainen
! Dec 2006
PROGRAM int2
  USE precision
  USE share_consts
  USE pde
  USE sizes                     ! nwlt
  USE wlt_trns_mod              ! init_DB(), c_wlt_trns_mask(), c_diff_fast()
  USE wlt_trns_vars             ! indx_DB, xx
  USE field                     ! u
  USE io_3D                     ! read_solution_dim()
  USE io_3D_vars                ! DO_READ_ALL_VARS_TRUE
  USE wlt_vars                  ! dim, j_lev, j_mx, LOW/HIGH_ORDER
  USE code_aux                  ! read_file_type(), aux__1(), aux_0(), aux_1()
  USE user_array_format         ! read_input_array(), clean_input_array(),
  ! ...                           xarray(:,:), farray(:,:), which_var(:), XSIZE, VAR_SIZE

#if ((TREE_VERSION == 2) || (TREE_VERSION == 3))
  USE tree_database_f      ! use Fortran database instead of C/C++ tree
#endif

  IMPLICIT NONE
  
  
  !----------------------------------------------------------------------------------------------------
  INTEGER, PARAMETER :: ADD_VAR_MARKER = 1    ! 0-store variables in DB, 1-store iwlt index to DB
  INTEGER, PARAMETER :: MAXLEN = 512          ! maximum length of command line argument
  LOGICAL, PARAMETER :: ALLOW_PRD_AMR_TREE = .TRUE.      ! internal flag (amr.cxx PERIODIC_AMR makro)
  !----------------------------------------------------------------------------------------------------
  
  
  REAL (pr), DIMENSION (:), POINTER :: scl       ! used in READ_SOLUTION
  INTEGER   :: i, j, iargc, &                    ! counters
       itmp, itmp1, itmp2,  &                    ! temporal variables
       n_var_i, &                                ! number of integer variables
       db_j_max                                  ! maximum level in the tree database
  REAL (pr) :: eps_filter                        ! treshold for the purposes of visualization
  INTEGER :: eps_num, &                          ! treshold is applied to that variable
       eps_max_level, &                          ! consider the wavelets up to that level
       variable_to_render, &                     ! variable to put into .ren file
       PREPROCESS_MODE                 ! mode: 0 - direct
  !                                            1 - preprocess uniform grid,
  !                                            2 - preprocess nonuniform grid
  !                                            3 - preprovess .res -> .ren for the volume renderer
  
  INTEGER :: INTERPOLATION_ORDER, &    ! 0-linear, 1-3rd order Hermit
       debug_flag                      ! debug_flag: READ_INPUT_ARRAY in user_array.local.f90 for details
  !                                          ( 0 - user provided format, 1 - RES_IO format,
  !                                            2 - predefined diagonal with file writing/reading,
  !                                            3 - predefined diagonal without reading from file,
  !                                            4 - no array, preprocess for rendering only )

  CHARACTER(LEN=MAXLEN) :: input_res_name, &     ! file names
       input_array_name, output_ren_name         !
  LOGICAL :: SINGLE_FILE, TRANSFORM_TO_REAL, &   ! logical flags
       COORD_INTEGER, MAKE_TETRAHEDRA, &         !
       CGNS_OUTPUT, &
       SAVE_TREE, &                              ! whether to keep initialization tree or not
       EXPAND_DATA                               ! whether to expand data to non-periodic grid
  
  REAL*8,ALLOCATABLE :: xx_c(:)                  ! coordinates to pass to C subroutines
  REAL (pr), POINTER :: du(:,:,:), d2u(:,:,:)    ! derivative du(ie,nwlt,dim)
  LOGICAL :: TREE_EXISTS                                 ! temporal marker
  REAL*4 :: t_1, t0, t1, t2

  IF( iargc() < 1 ) THEN
     CALL usage_int
  ELSE
     CALL read_file_type_int( eps_filter, eps_num, eps_max_level, &
          input_res_name, input_array_name, output_ren_name, variable_to_render, &
          TRANSFORM_TO_REAL, COORD_INTEGER, &
          PREPROCESS_MODE, INTERPOLATION_ORDER, debug_flag )
  END IF



  CALL read_solution( scl, DO_RESTART_FALSE, TRIM(input_res_name), DO_READ_ALL_VARS_TRUE )
  
  n_var = SIZE(u_variable_names(:))     !
  n_var_i = 0                           ! all the variables are considered to be real

  IF ( (PREPROCESS_MODE.EQ.3).AND.&                       ! test if -v flag makes sense
       ((variable_to_render.LE.0).OR.&
       (variable_to_render.GT.n_var)) ) THEN
     WRITE (*,'("strange variable to render:",I3," out of ",I3)') variable_to_render, n_var
     STOP 1
  END IF

  IF (.NOT.ALLOCATED(xx_c)) &
       ALLOCATE(xx_c(0:MAXVAL(nxyz(1:dim))))
  
  WRITE (*,'("-------------------------------------------------")')
  DO i=1,n_var
     WRITE (*,'("MAXVAL(ABS(u_in (1:nwlt,",I2,"))) = ",E17.10)') i, MAXVAL(ABS(u(1:nwlt,i)))
  END DO
  

  ! filter wavelet coefficients before the transform
  ! ( operates with indx_DB and u )
  CALL aux__1( n_var, prd, eps_filter, eps_num, eps_max_level )

  ! perform inverse wavelet transform if required
  ! ( make all the required initializations;
  !   init_DB() will initialize it's own database according to gmake's DB parameter;
  !   c_wlt_trns_mask() will perform inverse transform;
  !   DB_finalize() will clean the initialized database if SAVE_TREE is false;
  !   tree to be saved for DB=db_tree and non-periodic case
  SAVE_TREE = (TYPE_DB.EQ.DB_TYPE_TREE_C).AND.( ALL(prd(1:dim).EQ.0).OR.ALLOW_PRD_AMR_TREE )
  CALL aux_0 ( TRANSFORM_TO_REAL, SAVE_TREE )
  
  ! find first derivative
  ! (required for 3-rd order interpolation)
  IF (INTERPOLATION_ORDER.EQ.1) THEN
     ALLOCATE ( du(1:n_var,1:nwlt,1:dim) )
     ALLOCATE ( d2u(1:n_var,1:nwlt,1:dim) )
     CALL c_diff_fast (u, du, d2u, j_lev, nwlt, HIGH_ORDER, 10, 1, 1, n_var)
!!$     DO i=1,n_var
!!$        u(1:nwlt,i) = du(i,1:nwlt,1) ! u=du/dx
!!$     END DO
!!$     INTERPOLATION_ORDER = 0
  END IF
  
  WRITE (*,'("-------------------------------------------------")')
  DO i=1,n_var
     WRITE (*,'("MAXVAL(ABS(u_out(1:nwlt,",I2,"))) = ",E17.10)') i, MAXVAL(ABS(u(1:nwlt,i)))
  END DO
  
  
  ! find maximum level of the data to visualize
  ! ( it may be different from j_mx )
  db_j_max = 1
  itmp = mxyz(1)
  DO WHILE (itmp.LT.nxyz(1))
     itmp = itmp * 2
     db_j_max = db_j_max + 1
  END DO
  
  !-----------------------------------------------------------------
  !- DB_declare_globals() has to be used from inside the interpolation module
  !- if and only if main code has not been compiled with db_tree
  !-----------------------------------------------------------------
  ! initialize tree database
  ! ( this tree is the interpolation tree,
  !   differrent from the tree for the inverse transform )
  IF (ADD_VAR_MARKER.EQ.0) THEN ! add variables into DB
     itmp = n_var
  ELSE IF (ADD_VAR_MARKER.EQ.1) THEN ! add index into DB
     n_var_i = 1
     itmp = 0
  ELSE
     WRITE (*,'("strange ADD_VAR_MARKER:",I4)') ADD_VAR_MARKER
     STOP 1
  END IF
  CALL CPU_TIME( t_1 )
  IF ( .NOT.SAVE_TREE ) CALL DB_declare_globals( mxyz, prd*0, j_mn, db_j_max, dim, itmp, n_var_i )


  !-----------------------------------------------------------------
  !- This has to be used from inside the interpolation module
  !-----------------------------------------------------------------
  ! set xx arrays
  ! ( inside interpolation tree )
  itmp = 0                                  ! xx(0:MAXVAL(nxyz),1:dim)
  IF (COORD_INTEGER) itmp = 1
  DO i=1,dim
     xx_c(0:nxyz(i)) = xx(0:nxyz(i),i)
     CALL DB_amr_set_coordinates( itmp, nxyz(i), i, xx_c )
  END DO
  
  
  
  !-----------------------------------------------------------------
  !- This might be used from inside the interpolation module
  !-  to set xarray(1:dim,1:XSIZE), farray(1:VAR_SIZE,1:XSIZE), which_var(1:VAR_SIZE)
  !-----------------------------------------------------------------
  ! read user provided array of points to interpolate into:
  !   DEBUG_FLAG=0 - user provided format for data in INPUT_ARRAY_NAME file
  !   DEBUG_FLAG=1 - RES_IO format for data in INPUT_ARRAY_NAME file
  !   DEBUG_FLAG=2 - predefined diagonal with INPUT_ARRAY_NAME file writing/reading
  !   DEBUG_FLAG=3 - predefined diagonal without reading from file
  !   DEBUG_FLAG=4 - no array, preprocess for rendering only
  CALL read_input_array( XSIZE, VAR_SIZE, xarray, farray, which_var, input_array_name, debug_flag )
  
  

  ! add all significant nodes;
  ! make the data non-periodic if EXPAND_DATA is true;
  ! filter high level nodes if required
  ! ( uses indx_DB and visualization tree,
  !   fifth argument: store function values (0) or iwlt number (1) inside visualization tree
  !   seventh argument: store level as integer ival[0] - used in res2vis                     )
  EXPAND_DATA = ANY(prd(1:dim).EQ.1).AND..NOT.ALLOW_PRD_AMR_TREE
  IF ((.NOT.SAVE_TREE).OR.EXPAND_DATA) &
       CALL aux_1 ( n_var, prd, eps_max_level, db_j_max, ADD_VAR_MARKER, EXPAND_DATA, .FALSE. )

  
  ! clear the memory after READ_SOLUTION
  ! ( last argument: store function values (0) or iwlt number (1) inside interpolation tree
  !   effects here as 0 - deallocate u, 1 - do not deallocate u )
  CALL read_solution_free( scl, KEEP_U=(ADD_VAR_MARKER.EQ.1), KEEP_NAMES=.TRUE. )
  


  IF (PREPROCESS_MODE.EQ.3) &
       WRITE (*,'("Writing .ren file: ",A)') TRIM(output_ren_name)



  !-----------------------------------------------------------------
  !- This has to be used from inside the interpolation module
  !-----------------------------------------------------------------
  ! the main subroutine
  ! INTERPOLATION_ORDER=0 - linear interpolation
  ! INTERPOLATION_ORDER=1 - 3rd order Hermit interpolation
  ! PREPROCESS_MODE=0     - direct interpolation to each point
  ! PREPROCESS_MODE=1     - preprocess (uniform grid only)
  ! PREPROCESS_MODE=2     - preprocess (all grid types)
  ! PREPROCESS_MODE=3     - .res -> .ren for the volume renderer for the variable # VARIABLE_TO_RENDER  
  CALL CPU_TIME( t0 )
  CALL DB_interpolate( u, nwlt, n_var, du, ADD_VAR_MARKER, &
       xarray, farray, dim, XSIZE, which_var, VAR_SIZE, &
       PREPROCESS_MODE, INTERPOLATION_ORDER, &
       variable_to_render, TRIM(output_ren_name), LEN(TRIM(output_ren_name)) )
  CALL CPU_TIME( t1 )
  

  IF ((PREPROCESS_MODE.NE.3).AND.(debug_flag.NE.4)) THEN
     ! print the interpolation output
     WRITE (*,*) 'farray(:,:)='
     DO i=1,XSIZE
        WRITE (*,'(E12.5,X,100E12.5)') xarray(1,i), farray(1,i)
     END DO
     WRITE (*, '("CALL DB_interpolate (USING CPU_TIME) = ", es12.5)') t1-t0
     WRITE (*, '("all preprocessing   (USING CPU_TIME) = ", es12.5)') t0-t_1
  END IF
  
  DEALLOCATE (xx_c)
  IF (INTERPOLATION_ORDER.EQ.1) &
       DEALLOCATE ( du )
  CALL clean_input_array
  IF (SAVE_TREE) &
       CALL DB_finalize
  
END PROGRAM int2



