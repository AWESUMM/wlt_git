! standalone interpolation test
!
! The only purpose of that module is to provide user with
! READ_INPUT_ARRAY() subroutine, which can be used to read
! from a file the arrays of points to interpolate into:
!   xarray(1:dim,XSIZE)       - the point to interpolate into
!   farray(1:VAR_SIZE,XSIZE)  - not used, allocated only
!   which_var(1:VAR_SIZE)     - the variables to interpolate for
!   dim                       - dimension
!   XSIZE                     - number of points
!   VAR_SIZE                  - number of variables
!
! Please feel free to adjust that subroutine to the format you like.
!
! Check file tree-c/interface.h for a detailed description
! of DB_... subroutines and their parameters.
!
! Check file tree-c/io.h for the description of endian
! independent I/O library (I am using that AWCM_RES_IO format
! for the user provided file for the test case. Example of reading
! AWCM_RES_IO format can be found in read_input_array() and
! example of writing - in input_test_only_file() subroutine.
! Feel free to change read_input_array() if you
! wish to use a different format.
!
! by Alexei Vezolainen
! Jan 2007
MODULE user_array_format
  USE precision
  IMPLICIT NONE
  
  PUBLIC :: read_input_array, &       ! read user provided points to interpolate into
       clean_input_array              ! clean the memory
  
  REAL*8, PUBLIC, ALLOCATABLE :: xarray(:,:), farray(:,:)     ! arrays to pass to C subroutines
  INTEGER*4, PUBLIC, ALLOCATABLE :: which_var(:)              ! ...
  INTEGER*4, PUBLIC :: XSIZE, VAR_SIZE                        ! length of arrays to pass to C
  

  PRIVATE :: input_test_only, &       ! predefined diagonal without reading from file, for developers only
       input_test_only_file           ! predefined diagonal with file writing/reading, for developers only
       
  INTEGER, PRIVATE, PARAMETER :: SIZE_OF_TEST_ARRAY_FOR_INTERPOLATION = 50   ! input_test_only() parameter
  REAL(pr), PRIVATE, PARAMETER :: MARGIN = 0.3_pr                             ! input_test_only() parameter

CONTAINS


  ! ---------------------------------------------------------------------------
  ! PUBLIC
  ! read user provided points to interpolate into
  SUBROUTINE read_input_array ( XSIZE, VAR_SIZE, xarray, farray, which_var, filename, debug_flag )
    USE wlt_vars            ! dim
    USE wlt_trns_vars       ! xx
    INTEGER*4, INTENT(OUT) :: XSIZE
    INTEGER*4, INTENT(OUT) :: VAR_SIZE
    REAL*8,    ALLOCATABLE :: xarray(:,:)
    REAL*8,    ALLOCATABLE :: farray(:,:)
    INTEGER*4, ALLOCATABLE :: which_var(:)
    CHARACTER (LEN=*), INTENT(IN) :: filename       ! file with user provided array
    INTEGER, INTENT(IN) :: debug_flag               ! check below to see what it does
    INTEGER :: i
    INTEGER*4 :: ierr_4, num_4, dim_local
    
    CALL clean_input_array
    
    IF (debug_flag.EQ.0) THEN
       !
       ! uncomment that or put your format reading here if you wish
       !
!!$       OPEN (123, FILE = filename, FORM='unformatted', STATUS='old' ERR=123)
!!$       READ (123) dim_local, XSIZE, VAR_SIZE
!!$       IF (dim_local.NE.dim) THEN
!!$          WRITE (*,'("dimensions of the field and the array of points are different")')
!!$          STOP 1
!!$       END IF
!!$       ALLOCATE ( xarray(1:dim,XSIZE), farray(1:VAR_SIZE,XSIZE), which_var(1:VAR_SIZE) )
!!$       READ (123) which_var, xarray
!!$       CLOSE (123)       
       WRITE (*,'("This has to be verified by the user first:")')
       WRITE (*,'("subroutine READ_INPUT_ARRAY, file user_array.local.f90")')
       STOP 1
       
123    WRITE (*,'("Error while reading file: ",A)') TRIM(filename)
       IF (LEN(TRIM(filename)).EQ.0) &
            WRITE (*, '("Provide user array filename as: -f ARRAY_FILE")')
       STOP 1
    ELSE IF (debug_flag.EQ.1) THEN
       !
       ! read in RES_IO format
       !
       CALL RES_OPEN( 'r', TRIM(filename), LEN(TRIM(filename)), ierr_4 )
       IF (ierr_4.NE.0_4) THEN
          WRITE (*,'("Cannot open the file: ",A)') TRIM(filename)
          IF (LEN(TRIM(filename)).EQ.0) &
               WRITE (*, '("Provide user array filename as: -f ARRAY_FILE")')
          STOP 1
       END IF
       
       CALL RES_READ('i4', dim_local, 1_4 )
       IF (dim_local.NE.dim) THEN
          WRITE (*,'("dimensions of the field and the array of points are different")')
          STOP 1
       END IF
       
       CALL RES_READ('i4', XSIZE, 1_4 )
       CALL RES_READ('i4', VAR_SIZE, 1_4 )
       
       ALLOCATE ( xarray(1:dim,XSIZE), farray(1:VAR_SIZE,XSIZE), which_var(1:VAR_SIZE) )
       
       CALL RES_READ('i4', which_var, VAR_SIZE )
       num_4 = dim*XSIZE
       CALL RES_READ('r8', xarray, num_4 )
       
       CALL RES_CLOSE( ierr_4 )
       IF (ierr_4.NE.0_4) THEN
          WRITE (*,'("Cannot close the file: ",A)') TRIM(filename)
          STOP 1
       END IF
       RETURN
    ELSE IF (debug_flag.EQ.2) THEN
       !
       ! predefined diagonal with file writing/reading
       !
       CALL input_test_only_file( XSIZE, VAR_SIZE, xarray, farray, which_var, filename )
       RETURN
    ELSE IF (debug_flag.EQ.3) THEN
       !
       ! predefined diagonal without reading from file
       !
       CALL input_test_only( XSIZE, VAR_SIZE, xarray, farray, which_var )
       RETURN
    ELSE IF (debug_flag.EQ.4) THEN
       !
       ! (default FLAG) no input, preprocess for rendering only
       !
       RETURN
    END IF
    
    WRITE (*,'("Unknown user provided input flag")')
    WRITE (*,'("subroutine READ_INPUT_ARRAY, file user_array.local.f90")')
    STOP 1
  END SUBROUTINE read_input_array
  

  ! ---------------------------------------------------------------------------
  ! PUBLIC
  ! clean the memory
  SUBROUTINE clean_input_array
    IF (ALLOCATED(xarray)) DEALLOCATE (xarray)
    IF (ALLOCATED(farray)) DEALLOCATE (farray)
    IF (ALLOCATED(which_var)) DEALLOCATE (which_var)
  END SUBROUTINE clean_input_array


  ! ---------------------------------------------------------------------------
  ! PRIVATE
  ! testing subroutine, for developers only
  ! points are defined on the diagonal, with file writing/reading
  SUBROUTINE input_test_only_file( XSIZE, VAR_SIZE, xarray, farray, which_var, filename )
    USE wlt_vars            ! dim
    USE wlt_trns_vars       ! xx
    INTEGER*4, INTENT(OUT) :: XSIZE
    INTEGER*4, INTENT(OUT) :: VAR_SIZE
    REAL*8,    ALLOCATABLE :: xarray(:,:)
    REAL*8,    ALLOCATABLE :: farray(:,:)
    INTEGER*4, ALLOCATABLE :: which_var(:)
    CHARACTER (LEN=*), INTENT(IN) :: filename       ! file with user provided array
    INTEGER :: i, j
    INTEGER*4 :: num_4, ierr_4
    
    REAL*8, ALLOCATABLE :: xarray_local(:,:), farray_local(:,:)  ! all these has to be
    INTEGER*4, ALLOCATABLE :: which_var_local(:)                 ! provided by the user in
    INTEGER*4 :: XSIZE_local, VAR_SIZE_local, dim_local          ! order to do interpolation
    

    !
    ! write the "provided by user" arrays
    !
    XSIZE_local = SIZE_OF_TEST_ARRAY_FOR_INTERPOLATION
    VAR_SIZE_local = 1
    dim_local = dim
    ALLOCATE( xarray_local(1:dim_local, XSIZE_local),        &
         farray_local(1:VAR_SIZE_local, XSIZE_local),   &
         which_var_local(1:VAR_SIZE_local) )
    DO i=1,VAR_SIZE_local
       which_var_local(i) = i
    END DO
    DO i=1,XSIZE_local
       DO j=1,dim_local ! xx(0:nxyz(:),1:dim)
          xarray_local(j,i) = xx(0,j) + (xx(nxyz(j),j) - xx(0,j))*(i-1)/(1.0*(XSIZE_local-1))
       END DO
    END DO
    WRITE (*,*) "input_test_only_file :------------------------"
    WRITE (*,'("VAR_SIZE=",I3,", XSIZE=",I8)') VAR_SIZE_local, XSIZE_local
    WRITE (*,*) 'which_var=', which_var_local
    WRITE (*,*) 'xarray=', xarray_local
    WRITE (*,*) "----------------------------------------------"

    !
    ! this is an example how to do it in RES_IO format
    !
    CALL RES_OPEN( 'w', TRIM(filename), LEN(TRIM(filename)), ierr_4 )
    IF (ierr_4.NE.0_4) THEN
       WRITE (*,'("Cannot open the file: ",A)') TRIM(filename)
       IF (LEN(TRIM(filename)).EQ.0) &
            WRITE (*, '("Provide user array filename as: -f ARRAY_FILE")')
       STOP 1
    END IF
    CALL RES_WRITE('i4', dim_local, 1_4 )
    CALL RES_WRITE('i4', XSIZE_local, 1_4 )
    CALL RES_WRITE('i4', VAR_SIZE_local, 1_4 )
    num_4 = VAR_SIZE_local
    CALL RES_WRITE('i4', which_var_local, num_4 )    ! which_var_local(1:VAR_SIZE_local)
    num_4 = dim_local*XSIZE_local
    CALL RES_WRITE('r8', xarray_local, num_4 )       ! xarray_local(1:dim_local, XSIZE_local)
    CALL RES_CLOSE( ierr_4 )
    IF (ierr_4.NE.0_4) THEN
       WRITE (*,'("Cannot close the file: ",A)') TRIM(filename)
       STOP 1
    END IF

    DEALLOCATE( xarray_local, farray_local, which_var_local )


    !
    ! read the "provided by user" arrays from the file test_file.dat
    ! this is an example how to do it in RES_IO format
    !
    CALL RES_OPEN( 'r', TRIM(filename), LEN(TRIM(filename)), ierr_4 )
    IF (ierr_4.NE.0_4) THEN
       WRITE (*,'("Cannot open the file: ",A)') TRIM(filename)
       STOP 1
    END IF
    CALL RES_READ('i4', dim_local, 1_4 )
    IF (dim_local.NE.dim) THEN
       WRITE (*,'("dimensions of the field and the array of points are different")')
       STOP 1
    END IF
    CALL RES_READ('i4', XSIZE, 1_4 )
    CALL RES_READ('i4', VAR_SIZE, 1_4 )
    ALLOCATE ( xarray(1:dim,XSIZE), farray(1:VAR_SIZE,XSIZE), which_var(1:VAR_SIZE) )
    CALL RES_READ('i4', which_var, VAR_SIZE )
    num_4 = dim*XSIZE
    CALL RES_READ('r8', xarray, num_4 )
    CALL RES_CLOSE( ierr_4 )
    IF (ierr_4.NE.0_4) THEN
       WRITE (*,'("Cannot close the file: ",A)') TRIM(filename)
       STOP 1
    END IF
    WRITE (*,*) "input_test_only_file :------------------------"
    WRITE (*,'("VAR_SIZE=",I3,", XSIZE=",I8)') VAR_SIZE, XSIZE
    WRITE (*,*) 'which_var=', which_var
    WRITE (*,*) 'xarray=', xarray
    WRITE (*,*) "----------------------------------------------"

  END SUBROUTINE input_test_only_file
  ! ---------------------------------------------------------------------------
  ! PRIVATE
  ! testing subroutine, for developers only
  ! points are defined on the diagonal, without reading from user provided file
  SUBROUTINE input_test_only( XSIZE, VAR_SIZE, xarray, farray, which_var )
    USE wlt_vars            ! dim
    USE wlt_trns_vars       ! xx
    INTEGER*4, INTENT(OUT) :: XSIZE
    INTEGER*4, INTENT(OUT) :: VAR_SIZE
    REAL*8,    ALLOCATABLE :: xarray(:,:)
    REAL*8,    ALLOCATABLE :: farray(:,:)
    INTEGER*4, ALLOCATABLE :: which_var(:)
    INTEGER :: i, j
    
    XSIZE = SIZE_OF_TEST_ARRAY_FOR_INTERPOLATION
    VAR_SIZE = 1
    ALLOCATE ( xarray(1:dim,XSIZE), farray(1:VAR_SIZE,XSIZE), which_var(1:VAR_SIZE) )
    DO i=1,VAR_SIZE
       which_var(i) = i
    END DO
    DO i=1,XSIZE
       DO j=1,dim ! xx(0:nxyz(:),1:dim)
          xarray(j,i) = xx(0,j) + MARGIN + (xx(nxyz(j),j) - xx(0,j) - 2*MARGIN)*(i-1)/(1.0*(XSIZE-1))
       END DO
    END DO

    !xarray(2:3,:) = 0.0_pr

    WRITE (*,*) "input_test_only :----------------------------"
    WRITE (*,*) 'which_var=', which_var
    WRITE (*,*) 'xarray=', xarray 
    WRITE (*,*) "---------------------------------------------" 
  END SUBROUTINE input_test_only
  
END MODULE user_array_format
