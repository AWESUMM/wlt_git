! standalone interpolation test and a preprocessor for volume rendering
!
! For a given result file and a given file with a properly formatted array
! of coordinate points (see module user_array_format in user_array.local.f90
! for the format details) it will interpolate function value to these points.
!
! Implementation: read .res file; initialize complete C tree; inverse transform;
! add all the nodes to C tree; using the tree, build AMR tree of volumes;
! make interpolation using that AMR tree.
! The building of complete C tree in addition to the already excising database
! is an overkill. This is not the best approach for large problems.
!
! by Alexei Vezolainen
! Dec 2006
PROGRAM int
  USE precision
  USE share_consts
  USE pde
  USE sizes                     ! nwlt
  USE wlt_trns_mod              ! init_DB(), c_wlt_trns_mask()
  USE wlt_trns_vars             ! indx_DB, xx
  USE field                     ! u
  USE io_3D                     ! read_solution_dim()
  USE io_3D_vars                ! DO_READ_ALL_VARS_TRUE
  USE wlt_vars                  ! dim, j_lev, j_mx
  USE code_aux                  ! read_file_type(), aux__1(), aux_0(), aux_1()

  USE user_array_format         ! read_input_array(), clean_input_array(),
  ! ...                           xarray(:,:), farray(:,:), which_var(:), XSIZE, VAR_SIZE
  IMPLICIT NONE
  
  REAL (pr), DIMENSION (:), POINTER :: scl               ! used in READ_SOLUTION
  INTEGER   :: i, j, iargc, &
       n_var_i, &                          ! number of integer variables
       db_j_max                            ! maximum level in the tree database
  REAL (pr) :: eps_filter                  ! treshold for the purposes of visualization
  INTEGER :: eps_num, &                    ! treshold is applied to that variable
       eps_max_level, &                    ! consider the wavelets up to that level
       variable_to_render                  ! variable to put into .ren file
  
  
  INTEGER, PARAMETER :: MAXLEN = 256                    ! maximum length of command line argument
  CHARACTER(LEN=MAXLEN) :: input_res_name, &            ! file names
       input_array_name, output_ren_name                !
  LOGICAL :: SINGLE_FILE, TRANSFORM_TO_REAL, &
       COORD_INTEGER, MAKE_TETRAHEDRA, &
       CGNS_OUTPUT, &
       SAVE_TREE, &                             ! whether to keep initialization tree or not
       EXPAND_DATA                              ! whether to expand data to non-periodic grid
  INTEGER :: itmp                                    ! temporal variable
  REAL*8,ALLOCATABLE :: xx_c(:)                      ! coordinates to pass to C subroutines
  REAL (pr), POINTER :: du(:,:,:), d2u(:,:,:)    ! derivative du(ie,nwlt,dim)

  INTEGER :: PREPROCESS_MODE                         ! mode: 0 - direct
  !                                                          1 - preprocess uniform grid,
  !                                                          2 - preprocess nonuniform grid
  !                                                          3 - preprovess .res -> .ren for the volume renderer
  
  INTEGER :: INTERPOLATION_ORDER                     ! 0-linear, 1-3rd order Hermit
  
  INTEGER :: debug_flag                              ! debug_flag: READ_INPUT_ARRAY in user_array.local.f90 for details
  !                                                               ( 0 - user provided format, 1 - RES_IO format,
  !                                                                 2 - predefined diagonal with file writing/reading,
  !                                                                 3 - predefined diagonal without reading from file,
  !                                                                 4 - no array, preprocess for rendering only )
  
  IF( iargc() < 1 ) THEN
     CALL usage_int
  ELSE
     CALL read_file_type_int( eps_filter, eps_num, eps_max_level, &
          input_res_name, input_array_name, output_ren_name, variable_to_render, &
          TRANSFORM_TO_REAL, COORD_INTEGER, &
          PREPROCESS_MODE, INTERPOLATION_ORDER, debug_flag )
  END IF
!!$  PRINT *, 'eps_filter=',eps_filter,', eps_num=',eps_num,', eps_max_level=',eps_max_level, &
!!$       '|res=',TRIM(input_res_name),'|arr=',TRIM(input_array_name),'|ren=',TRIM(output_ren_name),'|', &
!!$       ', v=',variable_to_render
!!$  WRITE (*,'("TRANSFORM_TO_REAL=",L,", COORD_INTEGER=",L,", INTERPOLATION_ORDER=",I2)') &
!!$       TRANSFORM_TO_REAL, COORD_INTEGER, INTERPOLATION_ORDER
!!$  WRITE (*,'("PREPROCESS_MODE=",I2,", debug_flag=",I2)') &
!!$       PREPROCESS_MODE, debug_flag
  
!!$  !-- test parameter compatibility
!!$  IF ( ((LEN(TRIM(input_array_name)).GT.0).AND.(PREPROCESS_MODE.EQ.3)) .OR.&   ! input_array_name is not required
!!$       ((LEN(TRIM(input_array_name)).GT.0).AND.(debug_flag.GT.2))      .OR.&   ! ...
!!$       ((LEN(TRIM(input_array_name)).EQ.0).AND.(PREPROCESS_MODE.NE.3)) .OR.&   ! input_array_name is required
!!$       ((LEN(TRIM(input_array_name)).EQ.0).AND.(debug_flag.LE.2))          &   ! ...
!!$       ) THEN
!!$     WRITE (*,'("incompatible set of parameters")')
!!$     CALL usage_int
!!$  END IF

  

  CALL read_solution( scl, DO_RESTART_FALSE, TRIM(input_res_name), DO_READ_ALL_VARS_TRUE )
  
  n_var = SIZE(u_variable_names(:))     !
  n_var_i = 0                           ! all the variables are considered to be real

  IF ( (PREPROCESS_MODE.EQ.3).AND.&                       ! test if -v flag makes sense
       ((variable_to_render.LE.0).OR.&
       (variable_to_render.GT.n_var)) ) THEN
     WRITE (*,'("strange variable to render:",I3," out of ",I3)') variable_to_render, n_var
     STOP 1
  END IF

  IF (.NOT.ALLOCATED(xx_c)) &
       ALLOCATE(xx_c(0:MAXVAL(nxyz(1:dim))))
  
  WRITE (*,'("-------------------------------------------------")')
  DO i=1,n_var
     WRITE (*,'("MAXVAL(ABS(u_in (1:nwlt,",I2,"))) = ",E17.10)') i, MAXVAL(ABS(u(1:nwlt,i)))
  END DO
  
  
  ! filter wavelet coefficients before the transform
  ! ( operates with indx_DB and u )
  CALL aux__1( n_var, prd, eps_filter, eps_num, eps_max_level )
  
  ! perform inverse wavelet transform if required
  ! ( make all the required initializations;
  !   init_DB() will initialize it's own database according to gmake's DB parameter;
  !   c_wlt_trns_mask() will perform inverse transform;
  !   DB_finalize() will clean the initialized database if SAVE_TREE is false;
  !   tree to be saved for DB=db_tree and non-periodic case
  SAVE_TREE = (TYPE_DB.EQ.DB_TYPE_TREE_C).AND.ALL(prd(1:dim).EQ.0)
  CALL aux_0 ( TRANSFORM_TO_REAL, SAVE_TREE )
  

  WRITE (*,'("-------------------------------------------------")')
  DO i=1,n_var
     WRITE (*,'("MAXVAL(ABS(u_out(1:nwlt,",I2,"))) = ",E17.10)') i, MAXVAL(ABS(u(1:nwlt,i)))
  END DO
  
  
  ! find maximum level of the data to visualize
  ! ( it may be different from j_mx )
  db_j_max = 1
  itmp = mxyz(1)
  DO WHILE (itmp.LT.nxyz(1))
     itmp = itmp * 2
     db_j_max = db_j_max + 1
  END DO

  ! initialize tree database
  ! ( this tree is the interpolation tree,
  !   differrent from the tree for the inverse transform )
  IF ( .NOT.SAVE_TREE ) &
       CALL DB_declare_globals( mxyz, prd*0, j_mn, db_j_max, dim, n_var, n_var_i )
  
  ! write xx arrays into the database
  itmp = 0                                  ! xx(0:MAXVAL(nxyz),1:dim)
  IF (COORD_INTEGER) itmp = 1
  DO i=1,dim
     xx_c(0:nxyz(i)) = xx(0:nxyz(i),i)
     CALL DB_amr_set_coordinates( itmp, nxyz(i), i, xx_c )
  END DO
  
  

  ! read user provided array of points to interpolate into:
  !   DEBUG_FLAG=0 - user provided format for data in INPUT_ARRAY_NAME file
  !   DEBUG_FLAG=1 - RES_IO format for data in INPUT_ARRAY_NAME file
  !   DEBUG_FLAG=2 - predefined diagonal with INPUT_ARRAY_NAME file writing/reading
  !   DEBUG_FLAG=3 - predefined diagonal without reading from file
  !   DEBUG_FLAG=4 - no array, preprocess for rendering only
  CALL read_input_array( XSIZE, VAR_SIZE, xarray, farray, which_var, input_array_name, debug_flag )
  
  
  ! add all significant nodes;
  ! make the data non-periodic if EXPAND_DATA is true;
  ! filter high level nodes if required
  ! ( uses indx_DB and visualization tree,
  !   last argument: store function values (0) or iwlt number (1) inside visualization tree )
  EXPAND_DATA = ANY(prd(1:dim).EQ.1)
  IF ((.NOT.SAVE_TREE).OR.EXPAND_DATA) &
       CALL aux_1 ( n_var, prd, eps_max_level, db_j_max, 0, EXPAND_DATA )
  
  ! clear the memory after READ_SOLUTION
  !   u_variable_names are not deallocated)
  CALL read_solution_free ( scl, KEEP_U=.FALSE., KEEP_NAMES=.TRUE. )
  

  ! the main subroutine
  ! INTERPOLATION_ORDER=0 - linear interpolation
  ! INTERPOLATION_ORDER=1 - 3rd order Hermit interpolation
  ! PREPROCESS_MODE=0     - direct interpolation to each point
  ! PREPROCESS_MODE=1     - preprocess (uniform grid only)
  ! PREPROCESS_MODE=2     - preprocess (all grid types)
  ! PREPROCESS_MODE=3     - .res -> .ren for the volume renderer for the variable # VARIABLE_TO_RENDER
  IF (PREPROCESS_MODE.EQ.3) &
       WRITE (*,'("Writing .ren file: ",A)') TRIM(output_ren_name)
!!$  XSIZE = 1; xarray(:,1) = xarray(:,23)
!!$  PRINT *, 'xarray(:,1)=', xarray(:,1)
  CALL DB_interpolate(  u, nwlt, n_var, du, 0, &
       xarray, farray, dim, XSIZE, which_var, VAR_SIZE, &
       PREPROCESS_MODE, INTERPOLATION_ORDER, &
       variable_to_render, TRIM(output_ren_name), LEN(TRIM(output_ren_name)) )
  
  
  IF ((PREPROCESS_MODE.NE.3).AND.(debug_flag.NE.4)) THEN
     ! print the interpolation output
     WRITE (*,*) 'farray(:,:)='
     DO i=1,XSIZE
        WRITE (*,'(E12.5,X,100E12.5)') xarray(1,i), farray(1,i)
     END DO
  END IF

  DEALLOCATE (xx_c)
  CALL clean_input_array
  
END PROGRAM int



