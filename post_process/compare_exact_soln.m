cd c:\dan\research\wavelet_code\wlt_3d_V2\results

err_meth2 = load('smalltest1_meth2.err.exact.soln');
err_cn    = load('smalltest1_cn_adaptive.err.exact.soln');
err_cn_nonadpt    = load('smalltest1_cn_nonadaptive.err.exact.soln');

hold off;
plot(err_meth2(:,1),(err_meth2(:,3)+err_meth2(:,5))/2,'r+-'); hold on;
plot(err_cn(:,1),(err_cn(:,3)+err_cn(:,5))/2,'b+-');
plot(err_cn_nonadpt(:,1),(err_cn_nonadpt(:,3)+err_cn_nonadpt(:,5))/2,'g+-'); hold off;
legend('meth2 CPU = 1646 sec','CN Adaptive, CPU = 789 sec','CN Non Adaptive, CPU = 769 sec');
title('Vortex Array, L2 Error with analytical Soln');
xlabel('time');
ylabel('L2 Error (ave(u,v))');


