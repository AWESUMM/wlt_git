! read .res file and preprocess data
! for plotting with showme3D.m 'grid' and 'coeff' modes
PROGRAM res2matlab
  USE precision
  USE share_consts
  USE pde                  ! u_variable_names, dt
  USE field                ! u
  USE wlt_trns_vars        ! indx_DB, xx
  USE wlt_trns_mod
  USE sizes                ! nwlt
  USE wlt_vars             ! dim, j_lev
  USE io_3D_vars           ! DO_READ_ALL_VARS_FALSE
  USE io_3D                ! read_solution_dim, read_solution, clean_write
  USE parallel
  USE variable_mapping
  IMPLICIT NONE
  
! INTEGER,   ALLOCATABLE :: i_p(:), xyz(:)                  ! auxiliary arrays for coordinate transform           !AR 2/23/2011! commented by AR
  INTEGER,   ALLOCATABLE :: xyz(:)                          ! auxiliary arrays for coordinate transform           !AR 2/23/2011! added by AR
  INTEGER*8, ALLOCATABLE :: i_p(:)                          ! auxiliary arrays for coordinate transform           !AR 2/23/2011! added by AR
  INTEGER,  ALLOCATABLE :: index_stm(:,:)                   ! to be writted as matlab's stm_indx
  REAL (pr), ALLOCATABLE :: xyz_stm(:,:)                    ! to be written as matlab's xstm, ystm, zstm, cstm
  REAL (pr), DIMENSION (:), POINTER :: scl                  ! required by read_solution
  REAL (pr) ::  leps                                        ! epsilon treshold

  INTEGER   :: j_min_in, j_max_in, &                        ! levels to process
       i, j, idim, wlt_type, face_type, j_df, kk, &               ! cycle variables
!      ii, iwlt, num, &                                     ! auxiliary variables                                 !AR 2/23/2011! commented by AR
           iwlt, num, &                                     ! auxiliary variables                                 !AR 2/23/2011! added by AR
       begin_station_num, end_station_num, &                ! station # in saved solution file name
       station_num, &                                       !  ie. test_iso_0001.res is station_num = 1
       mag, &                                               ! 0 we do one component, >1 we find the magnitude
       do_par_read                                           ! 0-read single .res files, 1-multiple parallel files
  INTEGER, DIMENSION(:), ALLOCATABLE  :: n_map_local

  INTEGER*8 :: ii                                           ! auxiliary variable  for  indx_DB(,,,)%p()%ixyz      !AR 2/23/2011! added by AR

  INTEGER , DIMENSION(6)::  ibounds
  REAL (pr), DIMENSION (6) :: bounds                        ! matlab's bounds
  CHARACTER (LEN=4)    :: name
  CHARACTER (LEN=u_variable_name_len) ::  plot_component
  CHARACTER(LEN=FILE_NUM_LEN) :: file_num_str
  REAL(pr), PARAMETER :: ZERO = 1.0e-12_pr                  ! zero
  INTEGER :: wlt_fmly

  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  !Oleg: need to update this to deal with wlt_fmly
  wlt_fmly = 1 !temporarily fix, need to be read form the input variables
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  ! make format for reading u_variable_names_fmt
  !WRITE(u_variable_names_fmt,  '( ''( A'', I3.3 , '')'' )') u_variable_name_len
  CALL set_variable_name_format()
  
  ! READ ARGUMENTS
  OPEN(1,FILE='inp.dat',STATUS='OLD')
  READ (1,'(A64)')    file_gen
  READ (1,*) leps, bounds,  j_min_in, j_max_in, begin_station_num, end_station_num, do_par_read
  READ (1,'(A)') plot_component
  CLOSE(1)
  
!!$  PRINT *, 'file_gen=',TRIM(file_gen)
!!$  PRINT *, 'leps, bounds =', leps, bounds
!!$  PRINT *, 'j_min_in, j_max_in', j_min_in, j_max_in
!!$  PRINT *, 'begin_station_num, end_station_num', begin_station_num, end_station_num, do_par_read
  
  
  !-- First open the first data file and find out if it is 3D or 2D  
  WRITE(file_num_str,FILE_NUM_FMT) begin_station_num


  IF (do_par_read.EQ.0) THEN
     ! read .res file from a single processor
     CALL read_solution_dim( 'results/'//file_gen(1:INDEX(file_gen//' ' ,' ')-1)//file_num_str//'.res' ,& 
          IGNOREINIT = .TRUE. )
  ELSE
     ! read multiple processor .res files
     CALL read_solution_dim( 'results/'//file_gen(1:INDEX(file_gen//' ' ,' ')-1)//file_num_str//'.p0.res', &
          IGNOREINIT = .TRUE. )
  END IF
  

     mag =  0
     !ALLOCATE (u_variable_names(1) )
     !u_variable_names(:) = ' '
     !u_variable_names(1) = plot_component
     CALL register_var( plot_component, integrated=.FALSE.,  adapt=(/.FALSE.,.FALSE./),   interpolate=(/.FALSE.,.FALSE./),   exact=(/.FALSE.,.FALSE./),   saved=.FALSE., req_restart=.FALSE. )

     CALL setup_mapping()

     CALL print_variable_registery( FULL=.FALSE.)

  ! set global parameters to be used directly in read_solution
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!  IC_restart = .FALSE.
  IC_restart = .FALSE.
	!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  !force just one station for now
  end_station_num =  begin_station_num
  DO station_num = begin_station_num, end_station_num
     
     WRITE(file_num_str,FILE_NUM_FMT) begin_station_num
     ! put plot_component into u(1:nwlt,1)
     
     IF (do_par_read.EQ.0) THEN
        IC_filename = 'results/'//file_gen(1:INDEX(file_gen//' ' ,' ')-1)//file_num_str//'.res'
     ELSE
        IC_filename = 'results/'//file_gen(1:INDEX(file_gen//' ' ,' ')-1)//file_num_str//'.p0.res'
     END IF

     !CALL read_solution_version( IC_filename )

     !IF ( IC_RES_VERSION .EQ. 5 ) CALL read_solution( scl, IC_filename, DO_READ_ALL_VARS_FALSE, DO_READ_COMMONFILE_TRUE, IGNOREINIT = .TRUE. ) !, APPEND=appendit) !, VERBLEVEL=2 )
     !CALL read_solution( scl, IC_filename, DO_READ_ALL_VARS_FALSE, DO_READ_COMMONFILE_FALSE, IGNOREINIT = .TRUE. , APPEND=.TRUE.) !, VERBLEVEL=2 )
     CALL read_solution( scl, IC_filename, DO_READ_ALL_VARS_FALSE, DO_READ_COMMONFILE_FALSE, &
          IGNOREINIT = .TRUE. , EXACT=.FALSE., APPEND=.TRUE., CLEAN_IC_DD=.FALSE., VERBLEVEL=2 )
     
     ! if eps < 0 we reset it and use abs(eps) as percentage based on range of wlt coeff's
     IF (leps.LT.0.0) THEN
        eps = MINVAL(u(1:nwlt,1)) + DABS(leps)*(MAXVAL(u(1:nwlt,1))-MINVAL(u(1:nwlt,1)))
        WRITE (*,'("Using eps = ",E15.10)') leps
     END IF

     ! set matlab's bounds to hold xmin, etc for the output
     IF (DABS(SUM(bounds(1:dim*2))).LT.ZERO) THEN
        DO i=1,dim
           bounds(i*2-1) = xx(0,i)                     ! define xmin as min/max of xx in computational space
           bounds(i*2)   = xx(nxyz(i),i)               ! otherwise leave bounds as they are
        END DO
     END IF

     ! initialize i_p array for 1d into 3d coordinate transform
     ALLOCATE( i_p(0:dim), xyz(1:3) )
     xyz = 0 
     i_p(0) = 1
     DO i=1,dim
        i_p(i) = i_p(i-1)*(1+nxyz(i))
     END DO

     ! find levels to consider
     j_min_in = MAX( j_min_in, 1 )
     j_max_in = MIN( j_max_in, j_lev )

     ! count wavelets that are >= epsilon
     num = 0
     DO j = j_min_in, j_max_in
        DO wlt_type = MIN(j-1,1),2**dim-1
           DO face_type = 0, 3**dim - 1
              DO j_df = j, j_lev
                 DO kk = 1, indx_DB(j_df,wlt_type,face_type,j)%length
                    iwlt = indx_DB(j_df,wlt_type,face_type,j)%p(kk)%i            ! wavelet number
                    
                    IF (MAXVAL(DABS(u(iwlt,1:n_var))).GE.leps) num = num + 1
                    
                 END DO
              END DO
           END DO
        END DO
     END DO

     ! write xstm,ystm,zstm,cstm,stm_indx
     ALLOCATE( xyz_stm(1:num,1:4), index_stm(1:num,1:3) )
     xyz_stm(1:num,1:4) = 0.0_pr
     index_stm = 0
     num = 0
     DO j = j_min_in, j_max_in
        DO wlt_type = MIN(j-1,1),2**dim-1
           DO face_type = 0, 3**dim - 1
              DO j_df = j, j_lev
                 DO kk = 1, indx_DB(j_df,wlt_type,face_type,j)%length
                    iwlt = indx_DB(j_df,wlt_type,face_type,j)%p(kk)%i            ! wavelet number
                    IF (DABS(u(iwlt,1)).GE.leps) THEN
                       num = num + 1
                       ii = indx_DB(j_df,wlt_type,face_type,j)%p(kk)%ixyz        ! 1D coordinate
                       xyz(1:dim) = INT(MOD(ii-1,i_p(1:dim))/i_p(0:dim-1))       ! 3D coordinate \in [0..N-1]
                       DO i=1,dim
                          xyz_stm(num,i) = xx(xyz(i),i)                          ! xstm, ystm, zstm in computational domain
                       END DO
                       index_stm(num,1:dim) = xyz(1:dim)                         ! index_stm
                       xyz_stm(num,4) = u(iwlt,1)                             !  instead of zstm
                    END IF
                 END DO
              END DO
           END DO
        END DO
     END DO
     
     !
     !--------------- Unformatted output to be read from matlab
     !
     OPEN(1,FILE='wrk.dat',STATUS='UNKNOWN',FORM='UNFORMATTED',ACCESS='STREAM')
     WRITE (1) dim, IC_par_size, nxyz(dim), num
     WRITE (1) bounds(1:dim*2), t
     DO i=1,4
        !CALL clean_write (num, xyz_stm(1,i), ZERO)
        WRITE (1) xyz_stm(1:num,i)                           ! xyz coordinates
     END DO

     !IF (dim.EQ.3) THEN
        DO i=1,3
           WRITE (1) index_stm(1:num,i)                      ! wlt coefficients
        END DO
     !END IF

     CLOSE(1)

     ! clear the memory after READ SOLUTION
     ! ( last arguments: store function values (0) or iwlt number (1) inside visualization tree
     !   effects here as KEEP_U=.FALSE. - deallocate u, KEEP_U=.TRUE - do not deallocate u;
     !   u_variable_names are not deallocated)
     CALL read_solution_free ( scl, KEEP_U=.FALSE., KEEP_NAMES=.TRUE. )
 
     IF (ALLOCATED(u)) DEALLOCATE( u )
     IF (ALLOCATED(xx)) DEALLOCATE( xx )
     IF (ALLOCATED(i_p)) DEALLOCATE( i_p )
     IF (ALLOCATED(xyz)) DEALLOCATE( xyz )
     IF (ALLOCATED(xyz_stm)) DEALLOCATE( xyz_stm )
     IF (ALLOCATED(index_stm)) DEALLOCATE( index_stm )
  END DO !loop through stations
  PRINT *,'res2matlab is complete'
END PROGRAM res2matlab
