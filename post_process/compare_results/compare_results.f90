PROGRAM wavelet_interpolation
  USE precision
  USE share_consts
  USE pde
  USE wlt_trns_mod
  USE io_3D
  USE field
  USE sizes
  IMPLICIT NONE

  !--Wavelet transform variables
  REAL (pr), DIMENSION (:,:,:,:), ALLOCATABLE, SAVE :: u_out
  REAL (pr), SAVE,DIMENSION (:,:), POINTER :: c
  REAL (pr), DIMENSION (:), POINTER , SAVE :: scl
  REAL (pr), SAVE ::  leps, dx, dy
  INTEGER   ::  j_out!,frm 
  INTEGER   ::  nxyz_out(1:3) !, nxyz(1:3)
  INTEGER   :: i, j, k,  ie 
  REAL (pr), DIMENSION (:,:), ALLOCATABLE, SAVE ::  xx_out
  INTEGER :: ix,jx,ix_l,ix_h,ixp,ixpm,ix_even,ix_odd,iz
  INTEGER :: iy,jy,iy_l,iy_h,iyp,iypm,iy_even,iy_odd
!  CHARACTER (LEN=64) :: file_gen, file_cwlt
  CHARACTER (LEN=4)    :: name
  CHARACTER (LEN=10)    :: epsname  
  CHARACTER (LEN=256)  :: outputfile
  INTEGER   :: idim!, dim
!  LOGICAL :: form
  INTEGER   :: begin_station_num, end_station_num
  INTEGER   :: station_num !station # in saved solution file name
  !ie. test_iso_0001.res is station_num = 1
  INTEGER :: n_var_saved ! number of vars saved
  INTEGER :: saved_vort  ! 1 if we saved vorticity in this file
  INTEGER :: mag ! 0 we do one component, >1 we find the magnitude
  INTEGER , DIMENSION(6)::  ibounds
!  INTEGER :: nwlt_old !used to see if variables need to be reallocated between stations
  REAL (pr), DIMENSION (6) :: bounds
  REAL(pr)       :: pi2
  REAL(pr)       :: the_tke,the_tdiss
  CHARACTER (LEN=u_variable_name_len) ::  plot_component
  CHARACTER(LEN=FILE_NUM_LEN) :: file_num_str

  ! make format for reading u_variable_names_fmt
  WRITE(u_variable_names_fmt,  '( ''( A'', I3.3 , '')'' )') u_variable_name_len

  ! READ ARGUMENTS
  !----------- inputs
  OPEN(1,FILE='inp.dat',STATUS='OLD')
    READ (1,'(A64)')    file_gen

    READ (1,*) leps, bounds,  j_out, begin_station_num, end_station_num

    !READ (1,u_variable_names_fmt) plot_component
    READ (1,*) plot_component

    PRINT *, 'eps',leps, 'j_out',j_out,  'station_num',begin_station_num

    PRINT *, 'plot_component = ', plot_component
    PRINT *, ' begin_station_num, end_station_num = ', begin_station_num, end_station_num

  CLOSE(1)

  PRINT *, 'file_gen',file_gen
  PRINT *, 'eps',leps 

  !
  !-- First open the first data file and find out if it is 3D or 2D  
  WRITE(file_num_str,FILE_NUM_FMT) begin_station_num
  CALL read_solution_dim(  &
    'results/'//file_gen(1:INDEX(file_gen//' ' ,' ')-1)//file_num_str//'.res' )
!    'results/'//file_gen(1:INDEX(file_gen//' ' ,' ')-1)//'.'//file_num_str//'.res' ) !Oleg: modified because it puts extra . and make it *..0001
  PRINT *,'after  read_solution_dim , dim = ', dim

  !find magnitude of the velocity field
  IF( INDEX(plot_component,'magu') /= 0 ) THEN 
     mag=1
     ALLOCATE (u_variable_names(1:dim) )
     u_variable_names(:) = ' '
     u_variable_names(1) = 'Velocity_u_@t'
     u_variable_names(2) = 'Velocity_v_@t  '
     IF(dim==3)  u_variable_names(3) = 'Velocity_w_@t  '
  ELSE  IF( INDEX(plot_component,'magw') /= 0 ) THEN 
     !find magnitude of the velocity field
     mag=1
     ALLOCATE (u_variable_names(1:dim) )
     u_variable_names(:) = ' '
     u_variable_names(1) = 'Vorticity_u '
     u_variable_names(2) = 'Vorticity_v '
     IF(dim==3)  u_variable_names(3) = 'Vorticity_w '
  ELSE !do not do magnitude
     mag =  0
     ALLOCATE (u_variable_names(1) )
     u_variable_names(:) = ' '
     u_variable_names(1) = plot_component
  END IF

PRINT *,' u_variable_names ', u_variable_names

  !
  !-- Loop through each data file 
  !
  !force just one station for now
  nwlt_old = 0
  end_station_num =  begin_station_num
  DO station_num = begin_station_num, end_station_num
     PRINT *, 'Process station_num ',station_num
     WRITE (name, '(i4.4)') station_num

     !--------------------
     WRITE(file_num_str,FILE_NUM_FMT) begin_station_num
     CALL read_solution( scl, DO_RESTART_FALSE, &
!      'results/'//file_gen(1:INDEX(file_gen//' ' ,' ')-1)//'.'//file_num_str//'.res',& 
      'results/'//file_gen(1:INDEX(file_gen//' ' ,' ')-1)//file_num_str//'.res',& !Oleg: modified because it puts extra . and make it *..0001
	  DO_READ_ALL_VARS_TRUE )
     ! set j_out (the max level in the inverse transform) to j_lev 
     ! (the max level of the simulation)
     j_out = j_lev! MIN(j_out,j_lev)
     nxyz_out(1:dim)=mxyz(1:dim)*2**(j_out-1)
     IF( dim /= 3 ) THEN
        nxyz_out(3) = 0
     END IF

     ALLOCATE( xx_out(0:MAXVAL(nxyz_out),1:dim) )
     ALLOCATE( u_out(0:nxyz_out(1)+1,0:nxyz_out(2),0:nxyz_out(3),3) )
	 !IF( nwlt_old /= nwlt ) THEN
	 !  IF( ALLOCATED(c) ) DEALLOCATE(c)
	  ! ALLOCATE(     

     !
     !-- set the real space coordinates for the interpolated field
     !
     DO idim = 1,dim
        DO i=0,nxyz_out(idim)
           xx_out(i,idim)=xx(i*2**(j_lev-j_out),idim)
        END DO
     END DO

     !
     !-------- defining bounds for the regions to be plotted 
     !
     DO idim = 1,dim
        ibounds(2*idim-1) = 0
        DO WHILE (xx_out(ibounds(2*idim-1),dim) < bounds(2*idim-1) .AND. ibounds(2*idim-1) < nxyz_out(idim))
           ibounds(2*idim-1) = ibounds(2*idim-1)+1
        END DO
        ibounds(2*idim) = nxyz_out(idim)
        DO WHILE (xx_out(ibounds(2*idim),dim) > bounds(2*idim) .AND. ibounds(2*idim) > 0)
           ibounds(2*idim) = ibounds(2*idim)-1
        END DO
     END DO

     !     !-------- setting weights for wavelet transform
     !
     CALL c_wlt_trns_interp_setup(j_out, nxyz_out, MAXVAL(nxyz_out), xx_out )


!!$PRINT *,' MIN/MAX VAL u(:,1) : ', &
!!$     MINVAL( u(:,1)), &
!!$     MAXVAL( u(:,1))
!!$PRINT *,' MIN/MAX VAL u(:,2) : ', &
!!$     MINVAL( u(:,2)), &
!!$     MAXVAL( u(:,2))
!!$PRINT *,' MIN/MAX VAL u(:,3) : ', &
!!$     MINVAL( u(:,3)), &
!!$     MAXVAL( u(:,3))

     !
     !-- Interpolate to full grid based on epsilon. If epsilon is greater then the
     ! epsilon used in the simulation then a filtered version of the results
     ! will be interpolated to the full grid.
     !
     CALL c_wlt_trns_interp (u_out(:,:,:,1), u(1:nwlt,1), nxyz_out, nwlt, j_lev, &
          j_out, scl(1)*leps)
     IF( mag>0) THEN !calculate magnitude
        CALL c_wlt_trns_interp (u_out(:,:,:,2), u(1:nwlt,2), nxyz_out, nwlt, j_lev, &
           j_out, scl(2)*leps)
        IF(dim==2)THEN
           u_out(:,:,:,1) = SQRT(u_out(:,:,:,1)**2 +u_out(:,:,:,2)**2   )
        ELSE !dim == 3
           CALL c_wlt_trns_interp (u_out(:,:,:,3), u(1:nwlt,3), nxyz_out, nwlt, j_lev, &
           j_out, scl(3)*leps)
           u_out(:,:,:,1) = &
                SQRT(u_out(:,:,:,1)**2 + u_out(:,:,:,2)**2 + u_out(:,:,:,3)**2)
        END IF
     END IF

  !
  !--------------- Unformatted output of real space data 
  !
  OPEN(1,FILE='wrk.dat',STATUS='UNKNOWN',FORM='UNFORMATTED')
     DO idim = 1,dim
        CALL clean_write (nxyz_out(idim)+1, xx_out(0,idim), 1.0e-12_pr)
     END DO

     CALL clean_write ((nxyz_out(1)+1)*(nxyz_out(2)+1)*(nxyz_out(3)+1), u_out(0,0,0,1), 1.0e-12_pr)
     WRITE (1) dim
     ! then the new nx,ny,nz for this sub-domain
     WRITE (1)  ibounds(2)-ibounds(1)+1,ibounds(4)-ibounds(3)+1,ibounds(6)-ibounds(5)+1
     DO idim = 1,dim
        WRITE (1) xx_out(ibounds(2*idim-1):ibounds(2*idim),idim)
     END DO

     WRITE (1) u_out(ibounds(1):ibounds(2),ibounds(3):ibounds(4),ibounds(5):ibounds(6),1)
PRINT *,' MIN/MAX VAL u_out : ', &
     MINVAL( u_out(ibounds(1):ibounds(2),ibounds(3):ibounds(4),ibounds(5):ibounds(6),1)), &
     MAXVAL( u_out(ibounds(1):ibounds(2),ibounds(3):ibounds(4),ibounds(5):ibounds(6),1))
  CLOSE(1)

 
     DEALLOCATE (u, xx, u_out, xx_out)
     CALL  c_wlt_trns_interp_free() !
	  PRINT *, 'done'

  END DO !loop through stations
END PROGRAM wavelet_interpolation



