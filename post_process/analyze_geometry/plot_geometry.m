%geom(0/1) - 0 plots outside points, 1 plots outline of the object
function plot_geometry(geom_case,geom)
   global local_dir POSTPROCESS_DIR line_clr
   TRUE = logical(1);
   FALSE = logical(0);
   check=TRUE;
   A=101
   if isunix
       slash = '/';
       eval(['!\rm -f' 'temp' slash 'num.txt' ])
   else
       slash = '\';
       delete(['temp' slash 'num.txt']);
   end
   file = [geom_case '.igs']
   fid = fopen(['temp' slash 'temp.nfo'],'w');
   fprintf(fid,'%s\n',file);
   fclose(fid);
   file = ['geometry' slash geom_case '.igs']
   ans=[];
   while check
       disp(['plot_geometry: geom=',num2str(geom)]);
       fid = fopen([file '.inp'],'w');
       fprintf(fid,'%i\n',geom);
       fclose(fid);
       check = TRUE;
       if isunix
           disp('UNIX'); 
           ANALYZE_GEOM = 'analyze_geometry.2d.out';
       else
           disp('WINDOWS');
           disp('Open Dos Window and Run Program')
           %dos(['start debug\twodobject.exe' ' ' geom_case]);
           ANALYZE_GEOM = ['debug' slash 'analyze_geometry_2d.exe'];
       end
       eval(['!' POSTPROCESS_DIR slash ANALYZE_GEOM ' ' geom_case ])

       if geom==1
           answer=2;
           fid = fopen(['temp' slash 'answer.txt'],'w');
           fprintf(fid,'%i\n',answer);
           fclose(fid);
       elseif geom==0
           answer=0;
       else
           disp('geom must be 0 or 1.  1 corresponds to finding the geometry and 0 to analyzing it')
       end
       %while answer>1
       %    disp('push enter when program finishes')
       %    pause
       %    answer=load(['temp' slash 'answer.txt']);
       %end
       if 1==geom
           %figure(2)
           if isunix
               disp('type ./out in new window.')
               disp(' ')
               disp('answer questions')
           else
               disp('answer questions')
           end
           arr=load(['temp' slash 'cirarcdata.txt']);
           if ~isempty(arr)
               x=arr(:,1);
               y=arr(:,2);
               hold on
               plot(x,y,'o','MarkerEdgeColor',line_clr,'MarkerFaceColor',line_clr,'MarkerSize',2);
               xlabel('x')
               ylabel('y')
               daspect([1 1 1])
               hold on;
           end
           arr=load(['temp' slash 'bsplinedata.txt']);
           if ~isempty(arr)
               x=arr(:,1);
               y=arr(:,2);
               hold on;
               plot(x,y,'o','MarkerEdgeColor',line_clr,'MarkerFaceColor',line_clr,'MarkerSize',2);
               xlabel('x')
               ylabel('y')
               daspect([1 1 1])
               hold on
           end
           arr=load(['temp' slash 'Linedata.txt']);
           if ~isempty(arr)
               x=arr(:,1);
               y=arr(:,2);
               hold on;

               plot(x,y,'o','MarkerEdgeColor',line_clr,'MarkerFaceColor',line_clr,'MarkerSize',2);
               xlabel('x')
               ylabel('y')
               daspect([1 1 1])
           end
           ans =input('Are you satisfied with the geometry? (y/n):','s');
       end
       if 0==geom
           if isunix
               disp('type ./out in new window.')
               disp(' ')
               disp('push enter when ./out is finished running')
           else
               disp('Press enter when the program in a separate window is finished.')
           end
           pause
           arr=load([file '.geom']);
           x=arr(:,1);
           y=arr(:,2);

           plot(x,y,'o','MarkerEdgeColor','r','MarkerFaceColor','r','MarkerSize',2);
           xlabel('x')
           ylabel('y')
           daspect([1 1 1])
           check = FALSE;
       end
       if isunix
           !rm -f *Linedata.txt
           !rm -f *bsplinedata.txt
           !rm -f *cirarcdata.txt
           if 0==geom
               !rm -f *num.txt
           end
       else
           delete(['temp' slash 'Linedata.txt'])
           delete(['temp' slash 'bsplinedata.txt'])
           delete(['temp' slash 'cirarcdata.txt'])
           if 0==geom
               delete(['temp' slash 'num.txt'])
           end
       end
       if ans == 'n'
           check = TRUE;
       else
           check = FALSE;
       end
       if ans == 'n'
           if isunix

               !rm -f *num.txt
           else
               delete(['temp' slash 'num.txt'])
           end
       end
   end
end

