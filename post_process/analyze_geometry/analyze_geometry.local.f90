PROGRAM main
  USE precision
  USE threedobject

  IMPLICIT NONE
  INTEGER :: dimen,n, geom
  LOGICAL, DIMENSION(1000000) :: lp
  LOGICAL :: wrt_flag
  REAL(pr), DIMENSION(3,1000000) :: xp
  INTEGER :: count,nx,ny,nz,ix,iy,iz,j,k
  REAL(pr) xmin,xmax,ymin,ymax,zmin,zmax,dx,dy,dz
  CHARACTER (LEN=256) :: geometry_name, file_name
  INTEGER :: div = 3
  INTEGER(4) :: iargc
#ifdef _MSC_VER
     INTEGER(2) :: i
#else
    INTEGER(4) :: i
#endif

! Use the first argument as the input file name if it exists
  IF( iargc() >= 1 ) THEN
     CALL getarg( 1, geometry_name )
  ELSE
     WRITE(*,'("ERROR: need to provide name for the casefile")')
     STOP
  END IF
  PRINT *, geometry_name

  dimen = 3
  !PRINT *,'temp' //slash // 'temp.nfo'
  !OPEN (UNIT=130,FILE='temp' //slash // 'temp.nfo',FORM='formatted',STATUS= &
  !'old')
  !READ (UNIT=130,FMT='(A20)') geometry_name
  !CLOSE (UNIT=130)
  file_name = TRIM(geometry_name)
  PRINT*,'Using input file:', TRIM(file_name)
  count = 0
  xmin = -0.0
  xmax =  0.6
  ymin = -0.0
  ymax =  0.6
  zmin = -0.25
  zmax =  0.25
  nx   =  16
  ny   =  10
  IF (dimen==2) THEN
     nz   =  0
  ELSE
     nz   =  10
  END IF
  n = (nx+1)*(ny+1)*(nz+1)
  dx = (xmax-xmin)/REAL(nx+1)
  dy = (ymax-ymin)/REAL(ny+1)
  dz = (zmax-zmin)/REAL(nz+1)
  count = 0
  wrt_flag = .TRUE.
  lp = .FALSE.
  xp=0.0_pr

      
  ! case.inp is the file's geometry
  PRINT *, 'reading ...', 'geometry'//slash//TRIM(file_name)//'.inp'
  OPEN(111,FILE='geometry'//slash//TRIM(file_name)//'.inp',FORM='formatted',STATUS='old')
     READ (111,'(I1)') geom
  CLOSE(111)
  PRINT *, 'geom=',geom


  CALL initgeom(dimen,file_name,geom)

  count = 0
  DO iy = 0,ny
     DO ix = 0,nx
        DO iz = 0,nz
            count = count + 1
            xp(1,count) = xmin + dx * ix
            xp(2,count) = ymin + dy * iy
            xp(3,count) = zmin + dz * iz
         END DO
      END DO
   END DO

  CALL surface(xp,dimen,n,lp,wrt_flag,file_name)!lp=.FALSE. -> point out, lp=.TRUE. -> point in
                                                   !wrt_flag = .FALSE. does not write points to the file
           
!         END DO
!      END DO
!   END DO

!PRINT *,xp(1),xp(2),xp(3)
!PRINT *,yp(1),yp(2),yp(3)
  CALL clean_the_memory
  PRINT *,'main is done'

END PROGRAM main
