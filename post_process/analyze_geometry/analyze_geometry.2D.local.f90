PROGRAM main
  USE precision
  USE twodobject

  IMPLICIT NONE
  INTEGER :: n
  LOGICAL, DIMENSION(1000000) :: lp
  LOGICAL :: wrt_flag
  REAL(8), DIMENSION(1000000) :: xp,yp
  INTEGER :: j,count,nx,ny,ix,iy
  REAL(8) xmin,xmax,ymin,ymax,dx,dy
  CHARACTER (LEN=256) :: geometry_name, file_name
  INTEGER :: iargc
#ifdef _MSC_VER
     INTEGER(2) :: i
#else
    INTEGER(4) :: i
#endif

! Use the first argument as the input file name if it exists
  IF( iargc() >= 1 ) THEN
     CALL getarg( 1, geometry_name )
  ELSE
     WRITE(*,'("ERROR: need to provide name for the casefile")')
     STOP
  END IF
  PRINT *, geometry_name
  ! Use the first argument as the input file name if is exists
!  IF( iargc() >= 1 ) THEN
!     CALL getarg( 1, geometry_name )
!  ELSE
!     WRITE(*,'("ERROR: need to provide name for the casefile")')
!     STOP
!  END IF
!  PRINT *,'temp' //slash // 'temp.nfo'
!  OPEN (UNIT=130,FILE='temp' //slash // 'temp.nfo',FORM='formatted',STATUS='old')
!  READ (UNIT=130,FMT='(A256)') geometry_name
!  CLOSE (UNIT=130)
  file_name = TRIM(geometry_name) // '.igs'
  PRINT*,'Using input file:',geometry_name, file_name
  count=0
  xmin =  0.0
  xmax =  0.6
  ymin =  0.0
  ymax =  0.6
  nx   =  8
  ny   =  8
  n = (nx+1)*(ny+1)
  dx = (xmax-xmin)/REAL(nx)
  dy = (ymax-ymin)/REAL(ny)
  count = 0
  DO iy = 0,ny
  DO ix = 0,nx
     count = count + 1
     xp(count) = xmin + dx * ix
     yp(count) = ymin + dy * iy
  END DO
  END DO
!PRINT *,xp(1),xp(2),xp(3)
!PRINT *,yp(1),yp(2),yp(3)
  wrt_flag = .TRUE.

  CALL surface(xp,yp,n,lp,wrt_flag,file_name)	   !lp=.FALSE. -> point out, lp=.TRUE. -> point in
	                                           !wrt_flag = .FALSE. does not write points to the file

  PRINT *,'main is done'

END PROGRAM main
