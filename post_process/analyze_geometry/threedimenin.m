function threedimenin(name,geom2)
%close all
global POSTPROCESS_DIR
if geom2==1
    plot_geometry(name,geom2);
end
hold on;
tol=1e-12;
res=0;
if isunix
    slash = '/';
else
    slash = '\';
end
file = [name '.igs']
fid = fopen(['temp' slash 'temp.nfo'],'w');
fprintf(fid,'%s\n',file);
fclose(fid);
geom = 0
file = ['geometry' slash name '.igs']
fid = fopen([file '.inp'],'w');
fprintf(fid,'%i\n',geom);
fclose(fid);
bound=0;
fid = fopen([file '.splinebound'],'w');
fprintf(fid,'%i\n',bound);
fclose(fid);
if isunix 
    ANALYZE_GEOM = 'analyze_geometry.out';
else
    ANALYZE_GEOM = ['Debug' slash 'analyze_geometry.exe'];
end
eval(['!' POSTPROCESS_DIR slash ANALYZE_GEOM ' ' name '.igs'])

arr=load(['geometry' '\' 'inorout3d.txt']);
if ~isempty(arr)
    sz=size(arr(:,1));
    x=arr(:,1);
	y=arr(:,2);
	z=arr(:,3);
	plot3(x(:,1),y(:,1),z(:,1),'.r');
	xlabel('x')
	ylabel('y')
	zlabel('z')
	daspect([1 1 1])
end
hold on
arr=load(['geometry' '\' 'tie2d.txt']);
if ~isempty(arr)
    sz=size(arr(:,1));
    x=arr(:,1);
	y=arr(:,2);
	z=arr(:,3);
	plot3(x(:,1),y(:,1),z(:,1),'.c');
	xlabel('x')
	ylabel('y')
	zlabel('z')
	daspect([1 1 1])
end