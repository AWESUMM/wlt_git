
global lw
lw = 3;
if isunix 
    DIR1 = 'results.regatta/';
    DIR2 = '/regscratch2/olegv/isoturb.g/results/'
else
    DIR1 = 'results.regatta/';
    DIR2 = 'results.regatta/';
end

fname1=[DIR1 'decay.g128.dns.tst340.'];
%fname2=[DIR1 'decay.g128.dns.tst420.'];
plotDir ='.'
plotfilename = 'TESTTEST'
thelegend = '256^3 SCALES Cdyn eps0.5L2,t=0.08';
plotSPEC( fname1, '0080', '0155',plotDir,plotfilename, thelegend ,'print',11) 
%plotSPEC2( fname1, fname2, '0080', '0155',plotDir,plotfilename, thelegend ,'print',11) 
%340 stations 80, 155
%420 stations 80, 156