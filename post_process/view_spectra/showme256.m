addpath -end /homes/r31/olegv/research/SCALES/src.3d/post_process
 
if isunix 
    DIR1 = 'results.regatta/';
    DIR2 = '/regscratch2/olegv/isoturb.g/results/'
else
    DIR1 = 'results.regatta/';
    DIR2 = 'results.regatta/';
end
disp(['directories ' DIR1 ' ' DIR2]);

global lw; lw =3 ;
global lspec;  lspec = [ 'k  ' ; 'r--' ; 'r: ' ; 'g: ' ; 'm- ' ; 'b-.']; 

%%%%DNS%%%%
%256^3 runs giuliano's DNS
GiulianoDNS_phd(1)

%plotspectra_phd(256^3,[DIR1 'decay.g128.dns.tst278.'],[1 321],0.0,1,2,...
%    'SCALES Cdyn eps0.5L2, consadj','nospectra',0.6) % 256^3 SCALES  cons 

%plotspectra_phd(256^3,[DIR1 'decay.g128.dns.tst140.'],[1 388],0.0,1,3,...
%    'SCALES C=0 eps0.4L2, non-consadj','nospectra',0.6) % CVS  99.8%  

%plotspectra_phd(256^3,[DIR1 'decay.g128.dns.tst141.'],[0 339],0.0,1,3,...
%    'CVS eps0.25L2, non-consadj','nospectra',0.6) % CVS  99.8%  

plotspectra_phd(256^3,[DIR1 'decay.g128.dns.tst250.'],[0 66],0.0,1,2,...
    'scales no mdl, conservative adj','nospectra',0.6) % CVS  99.8%  

%plotspectra_phd(256^3,[DIR2 'decay.g128.dns.tst420.'],[0 271],0.0,1,4,...
%    'SCALES eps0.5L2, consadj','nospectra',0.6) % SCALES conservative  

plotspectra_phd(256^3,[DIR2 'decay.g128.dns.tst340.'],[0 232],0.0,1,3,...
    '??SCALES eps0.5L2, non-consadj','nospectra',0.6) % SCALES non-conservative

plotspectra_phd(256^3,[DIR2 'decay.g128.dns.tst297.'],[0 332],0.0,1,4,...
    'SCALES Cs=1e-4  256^3 eps=0.5L2 mdl =1','nospectra',0.3) % SCALES non-conservative

plotspectra_phd(256^3,[DIR2 'decay.g128.dns.tst430.'],[0 66],0.0,1,5,...
    'SCALES eps0.5L2, modadj','nospectra',0.6) % SCALES moderate-adjacent

%plotspectra_phd(256^3,[DIR2 'decay.g128.dns.tst315.'],...
%    [1 32],0.0,1,4,...
%    'CVS eps0.3L2, modadj',...
%    'nospectra',0.6) 

%plotspectra_phd(256^3,[DIR2 'decay.g128.dns.tst311.'],...
%    [1 174],0.0,1,5,...
%    'SCALES Cdyn eps0.5L2, modadj, |S|eps',...
%    'nospectra',0.6) % SCALES  cons 

%plotspectra_phd(256^3,[DIR2 'decay.g128.dns.tst316.'],...
%    [1 275],0.0,1,6,...
%    'SCALES Cdyn eps0.5L2, modadj, |S|epsplus',...
%    'nospectra',0.6) % SCALES  cons 

