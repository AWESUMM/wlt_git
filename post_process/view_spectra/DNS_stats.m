%
% Plot results from Giuliano's DNS runs
%
function DNS_stats( dofilt,loc )

%DNS data file in loc


lw = 3
     maxtke = 495.21 % nc.0 DNS file
    maxdiss = 0.51915E+04 % nc.0 DNS file
st1tke =   0.25505E+03 %station 1 nc.1000
st2tke =   0.12941E+03 %station 2 nc.2000
    
for i=[1:12]
    figure(i);clf
end

f=load(loc);

setup_fig(1);clf;
title('tke');
%subplot(2,1,1); hold on;
plot(f(:,1)-f(1,1),f(:,3)./maxtke,'k','LineWidth',lw); hold on

%axis([0 .06 300 500 ]);
add2legend('128^3 DNS');
if( dofilt == 1) 
%add circle for filt DNS at two stations
plot([ (f(1001,1) -f(1,1)) (f(2001,1) -f(1,1)) ],[st1tke/maxtke  st2tke/maxtke ],'ko','LineWidth',lw)
add2legend('128^3 DNS, wavelet filtered eps=0.5epsL2');
end


%hold off
%subplot(2,1,2); hold on;
setup_fig(3);clf;
title('dissipation');
plot(f(:,1)-f(1,1),f(:,4)./maxdiss,'k','LineWidth',lw)
%axis([0 .06 3000 5500 ]);
add2legend('128^3 DNS');
hold off
%% skew du
setup_fig(6);clf;
title('skew du');
plot(f(:,1)-f(1,1),f(:,14),'k','LineWidth',lw)
%axis([0 .06 300 500 ]);
add2legend('128^3 DNS');
hold off


function setup_fig(num)
figure(num)
set(gca,'FontSize',24); %24 good for including in 2 col tex output
set(gca,'FontName','Times')