! 
!     -----------------------------------------------------
!     caluclates several quantities from a homogeneous
!     isotropic turbulence field
!     -----------------------------------------------------
!

MODULE turbulence_statistics

    USE precision
    USE fft_module
  CONTAINS

!
!
! Arguments
!
! n1,n2,n3     - field dimensionsions
! u,v,w        -Input velocity field in real space
! vis          -viscosity (input)
! scl(1:3)     - eps scaling used in simulation
! eps_post     - eps used in post processing
! eps_sim      - epsilon used in simulation
! nwlt         - number of active wavelets in simulation
! outputfile   -output file name  
! print_stats  - if true the statistics are printed to stdout

    SUBROUTINE trb_stats( n1,n2,n3, u, v, w, t, vis, scl, &
         eps_post, eps_sim,nwlt,&
         outputfile,print_stats)

      IMPLICIT NONE
      !  
      INTEGER  , INTENT (IN)    ::n1,n2,n3,nwlt
      REAL (pr), INTENT (INOUT) :: u(n1+2,n2+1,n3+1),v(n1+2,n2+1,n3+1),w(n1+2,n2+1,n3+1) 
      REAL (pr), INTENT (IN) :: t 
      REAL (pr), INTENT (IN) :: vis 
      REAL (pr), INTENT (IN) :: scl(1:3)
      REAL (pr), INTENT (IN) :: eps_post, eps_sim
      CHARACTER (LEN=*), INTENT (IN) ::  outputfile
      LOGICAL, INTENT (IN) ::  print_stats

      !
      REAL (pr), DIMENSION(:,:,:) , ALLOCATABLE :: u2,v2,w2
      REAL (pr) :: tke,  int_length
      REAL (pr) :: utmp, vtmp, wtmp
      REAL (pr) :: umag, mag, wmag, smag
      REAL (pr) :: pi_local, pi2, div1
      REAL (pr) :: temp, time
      REAL (pr) :: beta, gradient

      REAL (pr) :: sspvc, scpvc, sfpvc
      REAL (pr) :: uprime, skew_u, kurt_u
      REAL (pr) :: ssfvc
      REAL (pr) :: t11(n1+2), t12(n2+1), t13(n3+1)
      REAL (pr) :: tke2
      REAL (pr) :: e11(n1+2), e12(n2+1), e13(n3+1)
      REAL (pr) :: ssdpvc, scdpvc, sfdpvc
      REAL (pr) :: ssddpvc, scddpvc, sfddpvc
      REAL (pr) :: duprime, skew_du, kurt_du
      REAL (pr) :: dduprime, skew_ddu, kurt_ddu
      REAL (pr) :: diss(n1+2)
      REAL (pr) :: ediss
      REAL (pr) :: eta, lambda, ldiss
      REAL (pr) :: fts, lett, kts
      REAL (pr) :: ret, rei
      REAL (pr) :: ssspc, scspc, sfspc
      REAL (pr) :: sprime, skew_s, kurt_s

      REAL (pr) :: sssdx, sssdy, sssdz, scsdx, scsdy, scsdz
      REAL (pr) :: sfsdx, sfsdy, sfsdz

      REAL (pr) :: sssddx, sssddy, sssddz, scsddx, scsddy, scsddz
      REAL (pr) :: sfsddx, sfsddy, sfsddz

      REAL (pr) :: dsxprime, skew_dsx, kurt_dsx
      REAL (pr) :: dsyprime, skew_dsy, kurt_dsy
      REAL (pr) :: dszprime, skew_dsz, kurt_dsz

      REAL (pr) :: ddsxprime, skew_ddsx, kurt_ddsx
      REAL (pr) :: ddsyprime, skew_ddsy, kurt_ddsy
      REAL (pr) :: ddszprime, skew_ddsz, kurt_ddsz

      REAL (pr) :: scal_int_len

      REAL (pr) :: area
      INTEGER :: n1pp,n2p,n3p, n1h, n2h,n3h, n1hp,  n2hp, n3hp
      INTEGER :: i, j, k, ii
      INTEGER :: outputfileUNIT

	  ALLOCATE(u2(n1+2,n2+1,n3+1),v2(n1+2,n2+1,n3+1),w2(n1+2,n2+1,n3+1))

      !    REAL time_begin, time_end

      n1pp = n1+2
      n2p = n2+1
      n3p = n3+1
      n1h=n1/2
      n2h=n2/2
      n3h=n3/2 
      n1hp=n1h+1
      n2hp=n2h+1
      n3hp=n3h+1 
      !
      !   define mathematical constants
      !
      pi_local=4.0_pr*atan(1.0_pr)
      pi2=2.0_pr*pi_local
      !
      !-------------------------------------------------
      !


      ! 
      ! 
      ! Open file put header
      !

      outputfileUNIT = 157
      OPEN (UNIT = outputfileUNIT, FILE =outputfile, STATUS='unknown',&
           POSITION='APPEND')


      write(UNIT=outputfileUNIT,ADVANCE='NO', &
           FMT='( ''% Time eps_sim, eps_post, scl.u, scl.v, scl.w, nwlt, '')' )       
	  write(UNIT=outputfileUNIT,ADVANCE='NO', &
           FMT='( ''percent.compression uprime tke tke.from.Fourier.Components '')' )
      write(UNIT=outputfileUNIT,ADVANCE='NO', &
           FMT='( '' uprime skew.u kurt.u duprime skew.du kurt.du dduprime skew.dd  kurt.ddu  '')' )
      write(UNIT=outputfileUNIT,ADVANCE='YES', &
           FMT= '( ''  vis ediss  eta  lambda  ldiss int.length duprime/dduprime kts lett  ret rei  '' ) ' )

      write(UNIT=outputfileUNIT,ADVANCE='NO', &
           FMT='(  6(e15.7 , '' ''), i11 , '' '' , 1(e15.7 , '' '') )' ) &
           t, eps_sim, eps_post, scl(1), scl(2), scl(3), nwlt,&
           100.0_pr*(1.0_pr- (REAL(nwlt,pr)/REAL(n1*n2*n3,pr)))
! For some reason the compression does nto always calculate correctly.
! some times it says 0.00 compression when it is not??

      sspvc = SUM( u(1:n1,1:n2,1:n3)**2 + v(1:n1,1:n2,1:n3)**2 + w(1:n1,1:n2,1:n3)**2 )
      tke = 0.5_pr * sspvc/(n1*n2*n3)
      uprime = sqrt(sspvc/(3.0_pr*n1*n2*n3))

      scpvc = SUM( u(1:n1,1:n2,1:n3)**3 + v(1:n1,1:n2,1:n3)**3 + w(1:n1,1:n2,1:n3)**3 )
      sfpvc = SUM( u(1:n1,1:n2,1:n3)**4 + v(1:n1,1:n2,1:n3)**4 + w(1:n1,1:n2,1:n3)**4 )


      skew_u = scpvc/(3.0_pr*n1*n2*n3)/uprime**3
      kurt_u = sfpvc/(3.0_pr*n1*n2*n3)/uprime**4

      if( print_stats ) &
           write(*,'(''*************************************************************************'')')
      if( print_stats ) write(*,'(''* uprime: '', e12.5,50x,''*'')') uprime
      write(UNIT=outputfileUNIT,ADVANCE='NO', FMT='( e15.7   , '' ''  )' )  uprime
      if( print_stats ) &
           write(*,'(''* TKE from physical components: '', e12.5,28x,''*'')') tke
      write(UNIT=outputfileUNIT,ADVANCE='NO', FMT='( e15.7   , '' ''  )' )  tke

      !     -----------------------------------------
      !     calucalte 1D spectrum of velocity squared
      !     -----------------------------------------

      call rtoft(u); call rtoft(v); call rtoft(w)
      !save velocity in Fourier space
      u2=u; v2=v; w2=w

      !

      DO i = 1, n1pp
         t11(i) = SUM( u(i,1:n2,1:n3)**2 + v(i,1:n2,1:n3)**2 + w(i,1:n2,1:n3)**2 )
      END DO
      t11(3:n1pp) = 2.0_pr*t11(3:n1pp)

      DO j = 1, n2
         t12(j) = SUM( u(1:2,j,1:n3)**2 + v(1:2,j,1:n3)**2 + w(1:2,j,1:n3)**2 ) + &
              2.0_pr*SUM( u(3:n1pp,j,1:n3)**2 + v(3:n1pp,j,1:n3)**2 + w(3:n1pp,j,1:n3)**2 )
      END DO

      DO k = 1, n3
         t13(k) = SUM( u(1:2,1:n2,k)**2 + v(1:2,1:n2,k)**2 + w(1:2,1:n2,k)**2 ) + &
              2.0_pr*SUM( u(3:n1pp,1:n2,k)**2 + v(3:n1pp,1:n2,k)**2 + w(3:n1pp,1:n2,k)**2 )
      END DO

      ssfvc = SUM( u(1:2,1:n2,1:n3)**2 + v(1:2,1:n2,1:n3)**2 + w(1:2,1:n2,1:n3)**2 ) +&
           2.0_pr*SUM( u(3:n1pp,1:n2,1:n3)**2 + v(3:n1pp,1:n2,1:n3)**2 + w(3:n1pp,1:n2,1:n3)**2 )

      tke2 = 0.5_pr * ssfvc
      if( print_stats ) &
           write(*,'(''* TKE from fourier components: '', e12.5,29x,''*'')') tke2
      write(UNIT=outputfileUNIT,ADVANCE='NO', FMT='( e15.7   , '' ''  )' )  &
           tke2
      !
      !     reduce data to meaningful 1D arrays:
      !       1) combine real*8 and imaginary parts (x-direction)
      !       2) combine k and -k wavenumbers (y- and z- directions)
      !

      do i = 1, n1hp
         e11(i) = t11(2*i-1) + t11(2*i)
      enddo

      e12(1) = t12(1)
      do j = 2, n2h
         e12(j) = t12(j) + t12(n2-j+2)
      enddo
      e12(n2hp) = t12(n2hp)

      e13(1) = t13(1)
      do k = 2, n3h
         e13(k) = t13(k) + t13(n3-k+2)
      enddo
      e13(n3hp) = t13(n3hp)
      !PRINT *, e11(1), e12(1), e13(1); STOP

      !
      !     derivative statistics
      !

      do k = 1, n3
         do j = 1, n2
            do i = 1, n1pp, 2
               ii = i+1
               utmp = u(i,j,k)
               u(i,j,k) = -fac1(i) * u(ii,j,k)
               u(ii,j,k) = fac1(i) * utmp
               vtmp = v(i,j,k)
               v(i,j,k) = -fac2(j) * v(ii,j,k)
               v(ii,j,k) = fac2(j) * vtmp
               wtmp = w(i,j,k)
               w(i,j,k) = -fac3(k) * w(ii,j,k)
               w(ii,j,k) = fac3(k) * wtmp
            enddo
         enddo
      enddo
      !now uvw are velocity derivative in Fourier space

      call ftort(u); call ftort(v); call ftort(w)
      !now uvw are velocity derivative in real space

      ssdpvc = SUM( u(1:n1,1:n2,1:n3)**2 + v(1:n1,1:n2,1:n3)**2 + w(1:n1,1:n2,1:n3)**2 )
      scdpvc = SUM( u(1:n1,1:n2,1:n3)**3 + v(1:n1,1:n2,1:n3)**3 + w(1:n1,1:n2,1:n3)**3 )
      sfdpvc = SUM( u(1:n1,1:n2,1:n3)**4 + v(1:n1,1:n2,1:n3)**4 + w(1:n1,1:n2,1:n3)**4 )

      u=u2;v=v2;w=w2 !take velocity in Fouier space from uvw2 

      do k = 1, n3
         do j = 1, n2
            do i = 1, n1pp, 2
               ii = i+1
               u(i,j,k) = -fsq1(i) * u(i,j,k)
               u(ii,j,k) = -fsq1(i) * u(ii,j,k)
               v(i,j,k) = -fsq2(j) * v(i,j,k)
               v(ii,j,k) = -fsq2(j) * v(ii,j,k)
               w(i,j,k) = -fsq3(k) * w(i,j,k)
               w(ii,j,k) = -fsq3(k) * w(ii,j,k)
            enddo
         enddo
      enddo
      !now uvw are velocity 2nd derivative in Fourier space

      call ftort(u); call ftort(v); call ftort(w)
      !now uvw are velocity 2nd derivative in real space
      ssddpvc = SUM( u(1:n1,1:n2,1:n3)**2 + v(1:n1,1:n2,1:n3)**2 + w(1:n1,1:n2,1:n3)**2 )
      scddpvc = SUM( u(1:n1,1:n2,1:n3)**3 + v(1:n1,1:n2,1:n3)**3 + w(1:n1,1:n2,1:n3)**3 )
      sfddpvc = SUM( u(1:n1,1:n2,1:n3)**4 + v(1:n1,1:n2,1:n3)**4 + w(1:n1,1:n2,1:n3)**4 )

      duprime = sqrt(ssdpvc/(3.0_pr*n1*n2*n3))
      dduprime = sqrt(ssddpvc/(3.0_pr*n1*n2*n3))

      skew_du = scdpvc/(3.0_pr*n1*n2*n3)/duprime**3
      kurt_du = sfdpvc/(3.0_pr*n1*n2*n3)/duprime**4

      skew_ddu = scddpvc/(3.0_pr*n1*n2*n3)/dduprime**3
      kurt_ddu = sfddpvc/(3.0_pr*n1*n2*n3)/dduprime**4

      if( print_stats ) &
           write(*,'(''* uprime, skew_u, kurt_u: '', 3(1x, e12.5),7x,''*'')')  &
           uprime, skew_u, kurt_u
      if( print_stats ) &
           write(*,'(''* duprime, skew_du, kurt_du: '', 3(1x, e12.5),4x,''*'')')  &
           duprime, skew_du, kurt_du
      if( print_stats ) &
           write(*,'(''* dduprime, skew_ddu, kurt_ddu: '', 3(1x, e12.5),1x,''*'')')  &
           dduprime, skew_ddu, kurt_ddu
      write(UNIT=outputfileUNIT,ADVANCE='NO', &
           FMT='(9(e15.7 , '' '')  )' )  &
           uprime, skew_u, kurt_u ,duprime, skew_du, kurt_du,dduprime, skew_ddu, kurt_ddu 

      ! uvw2 are velocity in Fouier space
      diss = 0.0_pr

      do k = 1, n3
         do j = 1, n2
            temp = fsq3(k) + fsq2(j)
            do i = 1, n1pp
               diss(i) = diss(i) + (temp + fsq1(i)) &
                    * (u2(i,j,k)**2 + v2(i,j,k)**2 + w2(i,j,k)**2)
            enddo
         enddo
      enddo

      ediss = vis * (2.0_pr*SUM( diss(3:n1pp) ) + diss(1) + diss(2))

      if( print_stats ) write(*,'(''* Viscosity: '', e12.5,47x,''*'')') vis
      if( print_stats ) &
           write(*,'(''* Kinetic energy dissipation: '', e12.5,30x,''*'')') ediss
      write(UNIT=outputfileUNIT,ADVANCE='NO', &
           FMT='( 2(e15.7 , '' '')  )' )  &
           vis,ediss

      !
      !     length scales
      !

      !     Kolmogorov length scale

      eta = (vis**3/ediss)**(.25_pr)
      if( print_stats ) &
           write(*,'(''* Kolmogorov length scale: '', e12.5,33x,''*'')') eta

      !     Taylor microscale

      lambda = uprime/duprime
      if( print_stats ) write(*,'(''* Taylor microscale: '', e12.5,39x,''*'')') lambda

      !     Dissipation length scale

      ldiss = uprime**3/ediss
      if( print_stats ) &
           write(*,'(''* Dissipation length scale: '', e12.5,32x,''*'')') ldiss

      !     Integral length scale

      int_length = pi_local/(2.0_pr*uprime**2) * (e11(1) + e12(1) + e13(1)) / 3.0_pr

      if( print_stats ) &
           write(*,'(''* Integral length scale: '', e12.5,35x,''*'')') int_length

      !     Velocity derivative microscale

      if( print_stats ) &
           write(*,'(''* Velocity derivative microscale: '', e12.5,26x,''*'')')  &
           duprime/dduprime
      !
      !     Time scales
      !      

      !     Kolmogorov time scale

      kts = (vis/ediss)**0.5_pr
      if( print_stats ) &
           write(*,'(''* Kolmogorov time scale: '', e12.5,35x,''*'')') kts

      !     Large eddy turnover time

      lett = ldiss/uprime
      if( print_stats ) &
           write(*,'(''* Large-eddy turnover time (diss length scale): '',  e12.5,12x,''*'')') lett

      !
      !     Reynolds numbers
      !

      !     Taylor Re

      ret = uprime * lambda / vis
      if( print_stats ) &
           write(*,'(''* Taylor microscale Reynolds number: '', e12.5,23x,''*'')') ret

      !     Integral Re

      rei = uprime * ldiss / vis
      if( print_stats ) &
           write(*,'(''* Dissipation scale Reynolds number: '', e12.5,23x,''*'')') rei
      if( print_stats ) &
           write(*,'(''*************************************************************************'')')
      write(UNIT=outputfileUNIT,ADVANCE='YES', &
           FMT='( 9(e15.7 , '' '')  )' )  &
           eta, lambda, ldiss,int_length,duprime/dduprime,kts,lett,ret,rei

      CLOSE(UNIT=outputfileUNIT)

      CALL ftort(u);  CALL ftort(v);  CALL ftort(w) ! FFT input data

      DEALLOCATE(u2,v2,w2)

    end subroutine trb_stats

end module turbulence_statistics

