function   add2legend(str)
%
% presentation_type
% 0 screen large plots and text, small legend text to see lots of info
% 1 all large text for overheads
global presentation_type

% default presentation_type to 0
if( isempty(presentation_type) ) 
    presentation_type = 0
end

%first get current legend strings
a =get(legend,'UserData');

if( isempty(a))  
    h=legend(str);
else
    h=legend(strvcat( char(a.lstrings),str));
end

% get current legend strings
a =get(legend,'UserData');
if( presentation_type == 0 )
  set(a.LegendHandle,'FontSize',10)
elseif ( presentation_type == 1 )
  set(a.LegendHandle,'FontSize',24)
end

