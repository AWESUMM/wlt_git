function plot_spec_tst340_newgdm(the_title,fig_num,DNSfull_spectra_file,DNSfilt_spectra_file1,DNSfilt_spectra_file2,DNSlegend_str,...
      RUN_spectra_file,RUN_legend_str,RUN2_spectra_file,RUN2_legend_str,...
      RUN3_spectra_file,RUN3_legend_str,RUN4_spectra_file,RUN4_legend_str,...
      RUN5_spectra_file,RUN5_legend_str,RUN6_spectra_file,RUN6_legend_str)


global lw
global lspec
do_k5_3 = logical(0)
%Plot case for PhD
% SCALES Cdyn cons 256 

%path(path,'/home/dan/research/scales/src.3d/post_process')
%path(path,'/home/dan/research/scales/runs/isoturb/isoturb.g')
%cd /home/dan/research/scales/runs/isoturb/isoturb.g



%%%%%%%%%%%%%%%%%%%%%\
% now run same thing comparing SCALES to filtered and non-filtered DNS
% 


%set parameters  DNS and LES blow up case and some mdl runs
%dt= 0.0001
dt= 0.002
%maxt = 0.2064331E-01
maxt = 0.2
mint = 0
mintke_x = 0; maxtke_x = maxt;  mintke =100 ; maxtke = 500
minspec_x = 0 ; maxspec_x = 128; minspec = 1e-4 ; maxspec =  1e3
maxw= 128;
Ntotal = 256^3 % unadapted total resolution
plotfilename = 'SCALES_Cdyn_consadj_256_filtDNS'
plotDir ='./'

% Plot line width
global lw;

%manually find closest station
%f = load(['C:/dan/research/wavelet_code/results.saved/JOT.SCALES_final340.run/decay.g128.dns.tst340dan..turbstats']);
%f(80,1) %check station 1, t = 0.08
%f(159,1) %check  station 2, t = 0.16
 
figure(fig_num);close(fig_num); 

last=0
%if RUN2_spectra_file == '' 
%   last = 1
%end

manual_spectra(RUN_spectra_file,fig_num,RUN_legend_str,last,do_k5_3,minspec_x, maxspec_x, minspec, maxspec,maxw,plotDir, plotfilename,lspec(4,:),1)
 figure(fig_num);hold on;               

 if exist('RUN2_spectra_file')
     if exist('RUN3_spectra_file') % last Add k^-5/3
         last = 0;
     else
         last = 1;
     end
     manual_spectra(RUN2_spectra_file,fig_num,RUN2_legend_str,last,do_k5_3,minspec_x, maxspec_x, minspec, maxspec,maxw,plotDir, plotfilename,lspec(5,:),1)
     figure(fig_num);hold on;
 end

 if exist('RUN3_spectra_file')
     if exist('RUN4_spectra_file') % last Add k^-5/3
         last = 0;
     else
         last = 1;
     end
     manual_spectra(RUN3_spectra_file,fig_num,RUN3_legend_str,last,do_k5_3,minspec_x, maxspec_x, minspec, maxspec,maxw,plotDir, plotfilename,lspec(6,:),1)
     figure(fig_num);hold on;
 end

 if exist('RUN4_spectra_file')
     if exist('RUN5_spectra_file') % last Add k^-5/3
         last = 0;
     else
         last = 1;
     end
     manual_spectra(RUN4_spectra_file,fig_num,RUN4_legend_str,last,do_k5_3,minspec_x, maxspec_x, minspec, maxspec,maxw,plotDir, plotfilename,lspec(7,:),1)
     figure(fig_num);hold on;
 end

 if exist('RUN5_spectra_file')
     if exist('RUN6_spectra_file') % last Add k^-5/3
         last = 0;
     else
         last = 1;
     end
     manual_spectra(RUN5_spectra_file,fig_num,RUN5_legend_str,last,do_k5_3,minspec_x, maxspec_x, minspec, maxspec,maxw,plotDir, plotfilename,lspec(8,:),1)
     figure(fig_num);hold on;
 end

 if exist('RUN6_spectra_file')
     last = 1;% last Add k^-5/3
%     manual_spectra(RUN6_spectra_file,fig_num,RUN6_legend_str,last,do_k5_3,minspec_x, maxspec_x, minspec, maxspec,maxw,plotDir, plotfilename,lspec(9,:),1)
     figure(fig_num);hold on;
 end

%Add_Spectral_DNS_spectra(station,fig,full_spectra_file,filt_spectra_file,legend_str)
Add_Spectral_DNS_spectra(fig_num,DNSfull_spectra_file,DNSfilt_spectra_file1,DNSfilt_spectra_file2,DNSlegend_str)

set(gca,'FontSize',24); %24 good for including in 2 col tex output
set(gca,'FontName','Times')
title(the_title);
figure(fig_num);hold off; 
   






