function printstats( res, basename )

%res = 128 or 256
figure(1)
eval(['print -depsc2 ' basename '.tke.eps'])
eval(['!gv ' basename '.tke.eps'])
legend off;  title('');
eval(['print -depsc2 ' basename '.nolegend.tke.eps'])

figure(3)
eval(['print -depsc2 ' basename '.diss.eps'])
eval(['!gv ' basename '.diss.eps'])
legend off;  title('');
eval(['print -depsc2 ' basename '.nolegend.diss.eps'])


	figure(4)
	eval(['print -depsc2 ' basename '.comp.eps'])
	eval(['!gv ' basename '.comp.eps'])
	legend off;  title('');
	eval(['print -depsc2 ' basename '.nolegend.comp.eps'])
    
 %if( res == 128 ) %only for 128 runs       
	figure(5)
	eval(['print -depsc2 ' basename '.scale.eps'])
	eval(['!gv ' basename '.scale.eps'])
	legend off; title('');
	eval(['print -depsc2 ' basename '.nolegend.scale.eps'])
    %end     
figure(6)
eval(['print -depsc2 ' basename '.skewnessdu.eps'])
eval(['!gv ' basename '.skewnessdu.eps'])
legend off;  title('');
eval(['print -depsc2 ' basename '.nolegend.skewnessdu.eps'])
    
%if( res == 128 ) %only for 128 runs
	figure(7)
	eval(['print -depsc2 ' basename '.mdlCs.eps'])
	eval(['!gv ' basename '.mdlCs.eps'])
	legend off;  title('');
	eval(['print -depsc2 ' basename '.nolegend.mdlCs.eps'])
	
	figure(8)
	eval(['print -depsc2 ' basename '.totalVisDiss.eps'])
	eval(['!gv ' basename '.mdlCs.eps'])
	legend off;  title('');
	eval(['print -depsc2 ' basename '.nolegend.totalVisDiss.eps'])
	
	figure(9)
	eval(['print -depsc2 ' basename '.totalSGSdiss.eps'])
	eval(['!gv ' basename '.totalSGSdiss.eps'])
	legend off;  title('');
	eval(['print -depsc2 ' basename '.nolegend.totalSGSdiss.eps'])
    %end