function  plot_turb_stats(Ntotal, DIR, file,iter_range_in,eps,eps_simulation, viscity,fig,...
      plotnum,legendstr,do_movie,plotDir,max_x_axis,plot_prepost)
global POSTPROCESS_DIR
global lw
global lspec 
global compression_flag
global file_num
Ntotal = [256^3, 256^3, 256^3, 256^3, 256^3]
% 
compression_flag = logical(1);
usegrid = 1 %put grid on all plots
% for scaling tke plots
%if( Ntotal == 128^3 )
    maxtke = 500  % nc.0 DNS file
    maxdiss = 0.2455519E+04   % nc.0 DNS file

    %elseif( Ntotal == 256^3 )
 %    maxtke =  494.404 % nc.0 DNS file
 %   maxdiss =  3442.19% nc.0 DNS file
 %end
area = 6.283185^3 ;% nc.0 DNS file

if( length(iter_range_in) == 1 )
     iter_range(1) = iter_range_in;
     iter_range(2) = iter_range_in;
 elseif(length(iter_range_in) == 2 )
      iter_range = iter_range_in
  else
      error('iter_range must have 1 or 2 elements ')
 end

 if( strcmpi(do_movie, 'movie') )
     domovie = 1;
 elseif(strcmpi(do_movie, 'nospectra') )
     domovie = -1;
 elseif ( strcmpi(do_movie, '') )
     domovie = -1;
 else
     domovie = 0;
 end
     
%if(plotnum==1) 
%    hold off
%    clf
%else
%    hold on
%end
% Plot line widthfiguire

% BW only for paper
if logical(0)
    if(plotnum==1) ;lclr = 'k--' ;end
    if(plotnum==2) ;lclr = 'k--' ;end
    if(plotnum==3) ;lclr = 'k-.'  ;end
    if(plotnum==4) ;lclr = lspec(4,:);end
    if(plotnum==5) ;lclr = lspec(5,:);end
    if(plotnum==6) ;lclr = lspec(6,:);end
    if(plotnum==7) ;lclr = lspec(7,:);end
    if(plotnum==8) ;lclr = lspec(8,:);end
end

% BW and color for thesis for presentation
if logical(0)
    if(plotnum==1) ;lclr = lspec(1,:);lclr_rts = [lspec(1,1:1), ':']; end
    if(plotnum==2) ;lclr = lspec(2,:);lclr_rts = [lspec(2,1:1), ':'];end
    if(plotnum==3) ;lclr = lspec(3,:);lclr_rts = [lspec(3,1:1), ':'];end
    if(plotnum==4) ;lclr = lspec(4,:);lclr_rts = [lspec(4,1:1), ':'];end
    if(plotnum==5) ;lclr = lspec(5,:);lclr_rts = [lspec(5,1:1), ':'];end
    if(plotnum==6) ;lclr = lspec(6,:);lclr_rts = [lspec(6,1:1), ':'];end
    if(plotnum==7) ;lclr = lspec(7,:);lclr_rts = [lspec(7,1:1), ':'];end
    if(plotnum==8) ;lclr = lspec(8,:);lclr_rts = [lspec(8,1:1), ':'];end
    if(plotnum==9) ;lclr = lspec(9,:);lclr_rts = [lspec(9,1:1), ':'];end
end
%lspec = [ 'k  ' ; 'r--' ; 'b-.' ; 'g: ' ; 'm  ' ; 'k: ' ; 'r: ' ]; %color
lspec
plotnum
lclr = lspec(plotnum,:);
lclr_rts = [lspec(plotnum,1:1), ':'];

% color for presentation
if logical(0)
    if(plotnum==1) ;lclr = 'k--';end
    if(plotnum==2) ;lclr = 'r--';end
    if(plotnum==3) ;lclr = 'b-.';end
    if(plotnum==4) ;lclr = 'g- ';end
    if(plotnum==5) ;lclr = 'm: ';end
    if(plotnum==6) ;lclr = 'k--';end
    if(plotnum==7) ;lclr = 'b-.';end
    if(plotnum==8) ;lclr = 'g-.';end
    if(plotnum==9) ;lclr = 'k';end
    if(plotnum==10) ;lclr = 'r';end
    if(plotnum==11) ;lclr = 'b';end
    if(plotnum==12) ;lclr = 'g';end
end

%clear plots if this is the first data series (plotnum==1) 
if(plotnum == 1)
  for i=1:11
     setup_fig(fig+i,'')
     clf;hold off
   end
end
if( eps == 0.0 )
    spectrafile = [DIR '/' file];
else
    spectrafile = [DIR '/' file 'filt.eps' num2str(eps,'%10.6f') '_st' ];
end

fprintf('Base file name: %s \n', spectrafile )

%for i=iter_range(1):iter_range(2)
%    filename = ['results/' file num2str(i,'%4.4d') '.spectra'] ;
%    if( exist(filename) ~= 2 )
%%        %res =     ['results/' file  num2str(i,'%4.4d') '.res']
 %       ffl=  'inp.dat';
 %       fid = fopen(ffl,'w');
 %       fprintf(fid,'%s\n',file);
 %       fprintf(fid,'%7i %7i  %12.5e ',...
	%        i ,i, eps);
  %      status = fclose(fid);
 %       POSTPROCESS_DIR
 %       eval(['!' POSTPROCESS_DIR '/view_spectra/c_wlt_turbstats.out'])
 %       !\rm inp.dat
 %   end
 %end
 
 %c
 % Biuld spectra is not there Olny if .turbstats file exists. This is an
 %  indicator that the postprocessing has been done..
 %
 if( exist( [ spectrafile 'turbstats'] ) )
     
    if( exist( [ spectrafile num2str( iter_range(1),'%4.4d') '.spectra'] ) ~= 2  | ...
         exist( [ spectrafile num2str( iter_range(2),'%4.4d') '.spectra'] ) ~= 2 ) 
        ffl=  'inp.dat';
        fid = fopen(ffl,'w');
        fprintf(fid,'%s\n',  file );
        fprintf(fid,'%7i %7i  %12.5e %12.5e %12.5e ',...
	    iter_range(1),iter_range(2) , eps , eps_simulation, viscity);
        status = fclose(fid);
        if( isunix ) 
            eval(['!' POSTPROCESS_DIR '/stats_isoturb/c_wlt_turbstats.out'])
        else %assumed to be windows visual fortran
            eval(['!' POSTPROCESS_DIR '/stats_isoturb/Debug/stats_isoturb.exe'])
        end
        !\rm inp.dat
    end 
end

if( exist( [ spectrafile 'turbstats'] ) )
    spectrafile
    fstat = load( [ spectrafile 'turbstats'] );
    %%%%%%%%%%%%%%%%%
    %tke
    setup_fig(fig,'TKE')
    set(gcf,'Position',[0 0   600 500])
    %if(plotnum == 1) ;clf;end
    if(plotnum>1) ;hold on;end
    plot(fstat(:,1),fstat(:,10)./maxtke,[lclr ],'LineWidth',lw);hold on
    %plot([ 0.08 0.08], [0 .80],'k','LineWidth',lw); % station 1
    %plot([ 0.16 0.16], [0 .60],'k','LineWidth',lw); % station 2
    %text( 0.06, .85, 'Station 1','FontSize',24,'FontName','Times');
    %text( 0.14, .65, 'Station 2','FontSize',24,'FontName','Times');
    axis([0  max_x_axis 0 1.5]);
    if(usegrid) grid on; end;
    %title('Total Kinetic Energy')
    ylabel('Resolved Kinetic Energy');
    xlabel('time');
     if(plotnum>1)
        add2legend(legendstr);
    else
        legend(legendstr);
    end
    


    %%%%%%%%%%%%%%%%%
    %dissipation
    setup_fig(fig+2,'Diss')
    %if(plotnum == 1) ;clf;end
    if(plotnum>1) ;hold on;end
    plot(fstat(:,1),fstat(:,22)./maxdiss,[lclr ],'LineWidth',lw)
    axis([0 max_x_axis 0 1]);
    if(usegrid) grid on; end;
    %if(max(f(:,2)) > 1e7 | max(f(:,2)) > 1e7)
    %    axis([min(f(:,1))  max(f(:,1)) 0 1000])
    %end
    xlabel('time');
    ylabel('viscous dissipation');
    if(plotnum>1)
        add2legend(legendstr);
    else
        legend(legendstr);
    end
    %eval(['print -dpsc2 ' file '.tkediss.eps']);

    if compression_flag
        setup_fig(fig+3,'Compression')
    else
        setup_fig(fig+3,'# of grid points')
    end
    %if(plotnum == 1) ;clf;end
    %%%%%%%%%%%%%%%%%
    % compression
    %if(plotnum>1) ;hold on;end
    if compression_flag
        plot(fstat(:,1),100.0* (1.0-fstat(:,7)/Ntotal(file_num)),[lclr ],'LineWidth',lw)
        axis([0 max_x_axis 99.4 100]);
    else
        plot(fstat(:,1),fstat(:,7),[lclr ],'LineWidth',lw)
        axis auto
        set(gca,'xlim',[0 max_x_axis]);
    end
    hold on;
    %axis([0 max_x_axis 0 100]);
    if(usegrid) grid on; end;
    %title('Compression ')
    if compression_flag
        ylabel('% compression (discarded points)');
        set(gca,'Ylim',[99.4 100])
    else
        ylabel('# of grid points points');
    end
    xlabel('time ');
     if(plotnum>1)
        add2legend(legendstr);
    else
        legend(legendstr);
     end
     set(gcf,'Position',[0 0   600 500])
    if compression_flag
        set(gca,'Ylim',[99.4 100])
    end
        %%%%%%%%%%%%%%%%%
    % scl
    setup_fig(fig+4,'Taylor Microscale Reynolds number')
    %if(plotnum == 1) ;clf;hold off;end    
    if(plotnum>1) ;hold on;end
    plot(fstat(:,1),fstat(:,30),[lclr ]); 
    axis auto
    set(gca,'xlim',[0 max_x_axis]);
    if(usegrid) grid on; end;
    title('Taylor Microscale Reynolds number')
    ylabel('Re_\lambda');
    xlabel('time ');
    if(plotnum>1)
        add2legend(legendstr);
    else
        legend(legendstr);
    end
    %%%%%%%%%%%%%%%%%
    % Skewness of velocity
    setup_fig(fig+5,'Skewness du_i/dx_j')
    %if(plotnum == 1) ;clf;hold off;end    
    if(plotnum>1) ;hold on;end
    plot(fstat(:,1),fstat(:,16),[lclr ],'LineWidth',lw); hold on
    set(gca,'XLim',[0 max_x_axis ])
    if(usegrid) grid on; end;
    %axis([0 .61 0 100]);
    title('skewness du_i/dx_j')
    ylabel('skewness du_i/dx_j');
    xlabel('time ');
     if(plotnum>1)
        add2legend(legendstr);
   else
        legend(legendstr);
    end
end

%
% Plot from ._log file
%
if( plot_prepost == 1 ) % plot from pre post proccessing files
if( exist( [ spectrafile '_log'] ) )
    fstat = load( [ spectrafile '_log'] );
    %%%%%%%%%%%%%%%%%
    % compression
    if compression_flag
        setup_fig(fig+3,'Compression');
    else
        setup_fig(fig+3,'# of grid points');
    end

    
    %if(plotnum == 1) ;clf;end
    if(plotnum>1) ;hold on;end
    %HACK becuase giuliano changed column 5 of log file
    if compression_flag
        if fstat(1,5) < 1.0 % already compression
            plot(fstat(:,1),100.0* fstat(:,5),[lclr_rts ],'LineWidth',lw)
        else % col 5 is nwlt
            plot(fstat(:,1),100.0* (1.0-fstat(:,5)/Ntotal(file_num)),[lclr_rts ],'LineWidth',lw)
        end
        axis([0 max_x_axis 99.4 100]);
    else
        if fstat(1,5) < 1.0 % already compression
            plot(fstat(:,1),fstat(:,5)*Ntotal(file_num),[lclr_rts ],'LineWidth',lw)
        else % col 5 is nwlt
            plot(fstat(:,1),fstat(:,5),[lclr_rts ],'LineWidth',lw)
        end
        axis auto
        set(gca,'xlim',[0 max_x_axis]);
    end
    if(usegrid) grid on; end;
    if compression_flag
        title('% wavelet compression');
        ylabel('% compression');
    else
        title('# of grid points');
        ylabel('# of grid points');
    end
    xlabel('time ');
     if(plotnum>1)
        add2legend([legendstr ' from user stats']);
    else
        legend([legendstr ' from user stats']);
    end
end
end
%
% plot Model coeff
if( exist( [ spectrafile 'case_isoturb.log'] )  )
    
    fturb = load( [ spectrafile 'case_isoturb.log'] );


if( plot_prepost == 1 ) % plot from pre post proccessing files
        
    %%%%%%%%%%%%%%%%%
    %tke
    setup_fig(fig,'TKE')
    %if(plotnum == 1) ;clf;end
    if(plotnum>1) ;hold on;end
    plot(fturb(:,1),fturb(:,2)./maxtke,[lclr_rts ],'LineWidth',lw);hold on
    %plot([ 0.08 0.08], [0 .80],'k','LineWidth',lw); % station 1
    %plot([ 0.16 0.16], [0 .60],'k','LineWidth',lw); % station 2
    %text( 0.06, .85, 'Station 1','FontSize',24,'FontName','Times');
    %text( 0.14, .65, 'Station 2','FontSize',24,'FontName','Times');
    axis([0  max_x_axis 0 1]);
    if(usegrid) grid on; end;
    %title('Total Kinetic Energy')
    ylabel('Resolved Kinetic Energy');
    xlabel('time');
     if(plotnum>1)
        add2legend([legendstr ' from user stats']);
    else
        legend([legendstr ' from user stats']);
    end
end


    if( length(fturb(1,:)) >= 3 )
        %%%%%%%%%%%%%%%%%
        % Cs
        setup_fig(fig+6,'SGS model Coeff Cs')
        %if(plotnum == 1) ;clf;hold off;end
	
      
        if(plotnum>1) ;hold on;end
        plot(fturb(:,1),fturb(:,3),[lclr ],'LineWidth',lw)
        set(gca,'XLim',[0 max_x_axis ])
        if(usegrid) grid on; end;
       
	
        %axis([0 .61 0 500]);
        title('SGS model Coeff Cs')
        ylabel('Cs');
        xlabel('time');
         if(plotnum>1)
            add2legend(legendstr);
        else
            legend(legendstr);
        end
        %END Cs 
    end
    %%%%%%%%%%%%%%%%%
    
    %%%%%%%%%%%%%%%%%
    % total_resolved diss
    if( length(fturb(1,:)) >= 4)
        setup_fig(fig+7,'Viscous Dissipation')
        %if(plotnum == 1) ;clf;hold off;end
	
       
        if(plotnum>1) ;hold on;end
        
        % We have to divide by area because in the wlt code I calculate
        % this dissipation using dA instead of dA/sum(dA)
        % area = sum(dA)..DG
        plot(fturb(:,1),fturb(:,4)./(area*maxdiss),[lclr ],'LineWidth',lw)
        
        set(gca,'XLim',[0 max_x_axis ])
        axis([0 max_x_axis 0 1]); 
        if(usegrid) grid on; end;
	
        %grid on
       
        %axis([0 .61 0 500]);
        title('Total resolved Viscous Dissipation')
        ylabel('\nu 2SijSij');
        xlabel('time');
         if(plotnum>1)
            add2legend(legendstr);
        else
            legend(legendstr);
        end
    end
    %END 
    %%%%%%%%%%%%%%%%%
    
       %%%%%%%%%%%%%%%%%
    % total_sgs diss
     if( length(fturb(1,:)) >= 5 )
		setup_fig(fig+8,'SGS Diss')
		%if(plotnum == 1) ;clf;hold off;end
		
		
		if(plotnum>1) ;hold on;end
		plot(fturb(:,1),fturb(:,5)./(area*maxdiss),[lclr ],'LineWidth',lw)
		set(gca,'XLim',[0 max_x_axis ])
        axis([0 max_x_axis 0 1]);       
        if(usegrid) grid on; end;
		
		
		%axis([0 .61 0 500]);
		title('Total SGS Dissipation')
		ylabel('SGS dissipation');
		xlabel('time');
         if(plotnum>1)
            add2legend(legendstr);
		else
            legend(legendstr);
		end
    end
    %END 
    %%%%%%%%%%%%%%%%%
           
    %%%%%%%%%%%%%%%%%
    % viscous plus total_sgs diss
     if(0) %not used for now
        if( length(fturb(1,:)) >= 5 )
		    setup_fig(fig+9,'total_sgs diss')
	        %if(plotnum == 1) ;clf;hold off;end
		
		
		    if(plotnum>1) ;hold on;end
            plot(fstat(:,1),fstat(:,22)./maxdiss,[lclr ],'LineWidth',lw)
		    plot(fturb(:,1),( fturb(:,4) )./(area*maxdiss), 'b-.','LineWidth',lw); hold on
            plot(fturb(:,1),( fturb(:,5) )./(area*maxdiss), 'm--','LineWidth',lw)
            plot(fturb(:,1),( fturb(:,4) + fturb(:,5))./(area*maxdiss), 'g:','LineWidth',lw)
		    set(gca,'XLim',[0 max_x_axis ])
            axis([0 max_x_axis 0 1]);     
            if(usegrid) grid on; end;
		
		    %axis([0 .61 0 500]);
		    title('Total SGS Dissipation')
		    ylabel('total dissipation');
		    xlabel('time');
            if(plotnum>1)
                add2legend(legendstr);
		    else
                legend(legendstr);
		    end
        end
    end
    
    
    
    %%%%%%%%%%%%%%%%%
    % percent sgs diss
     
        if( length(fturb(1,:)) >= 5 )
		    setup_fig(fig+9,'% SGS Diss')
            set(gcf,'Position',[0 0   600 500])
	        %if(plotnum == 1) ;clf;hold off;end
		
		
		    if(plotnum>1) ;hold on;end
            plot(fturb(:,1),( 100.0 * abs(fturb(:,5))./(fturb(:,4) + abs(fturb(:,5)))), [lclr ],'LineWidth',lw)
		    set(gca,'XLim',[0 max_x_axis ])
            axis([0 max_x_axis 0 50]);     
            if(usegrid) grid on; end;
		
		    %axis([0 .61 0 500]);
		    %title('Percent (modeled) SGS Dissipation')
		    ylabel('% SGS Dissipation');
		    xlabel('time');
            
            if(plotnum>1)
                add2legend(legendstr);
		    else
                legend(legendstr);
		    end
        end
        
        
    %END 
    %%%%%%%%%%%%%%%%%
    
    
    
else
    for i=6:8
        setup_fig(fig+i,'')
        if(plotnum == 1) ;clf;hold off;end
    end
end

if( domovie ~= -1 ) 
%plot spectra
setup_fig(fig+1,'E. SPectra')
if(plotnum == 1) ;clf;end

%setup avi movie
if(domovie == 1)
    set(fig,'DoubleBuffer','on');
    %set('NextPlot','replace','Visible','off')
    mov = avifile([ plotDir '/' file 'spectra.avi'],'FPS',3)
    %end setup avi movie
end
for i=iter_range(1):iter_range(2)
    f = load( [ spectrafile num2str(i,'%4.4d') '.spectra'] );
    %%%if(i==iter_range(1));hold off;clf;end
    %if(plotnum>1) ;hold on;end
    %if(plotnum==1) ;hold off;clf;end
    loglog(f(:,1),f(:,2)./max(f(:,2)),lclr); 
    set(gca,'xlim',[0 120],'ylim',[1e-6 1])
    i-iter_range(1)+1

    title(['Energy Spectra, soln file # ' num2str(i) , ' time = ', num2str(fstat(i-iter_range(1)+1,1)) ])
    if( domovie == 0 )
        pause
    end
    %movie frame
    if(domovie == 1)
        F = getframe(gcf);
        mov = addframe(mov,F);
    end
    %end movie frame
    %pause
    %loglog(f(:,1),f(:,2)./max(f(:,2)),'k'); hold on
end
if(domovie == 1)
    mov = close(mov);
end
hold off
end


function setup_fig(num,title)
global compression_flag
figure(num)
set(gca,'FontSize',24); %24 good for including in 2 col tex output
set(gca,'FontName','Times')
set(gcf,'Name',title); %set the window title
