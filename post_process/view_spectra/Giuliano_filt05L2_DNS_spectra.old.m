function Giuliano_filteps0_5_DNS_spectra(station,fig, DNS_DIR)

   global lw


    f = load( [DNS_DIR '/nc.'  num2str(station,'%4.4d ') '.init.spectra'] );
    
    figure(fig)
    set(gca,'FontSize',24); %24 good for including in 2 col tex output
    set(gca,'FontName','Times')
    loglog( f(:,1), f(:,2), 'k','LineWidth',lw); hold on %unfiltered DNS
    add2legend('DNS')
     % plot filtered DNS
    if( station ==1000) 
         f = load( [DNS_DIR '/decay.g128.dns.tst298.nc.1000.filt.0000.spectra' ] );
        loglog( f(:,1), f(:,2), 'r-.','LineWidth',lw);
    end 
      if( station == 2000) 
         f = load( [DNS_DIR '/decay.g128.dns.tst299.nc.2000.filt.0000.spectra' ]  );
        loglog( f(:,1), f(:,2), 'r-.','LineWidth',lw);
    end 
    add2legend('Filtered DNS, eps = 0.5')