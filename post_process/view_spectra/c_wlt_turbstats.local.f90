
PROGRAM wavelet_interpolation
  USE precision
  USE share_consts
  USE pde
  USE sizes
  USE FFT_MODULE
  USE SPECTRA_MODULE
  USE wlt_trns_mod
  USE field
  USE turbulence_statistics
  USE io_3D
  USE io_3D_vars
  USE wlt_vars
  USE input_file_reader
  IMPLICIT NONE

  !--Wavelet transform variables
  REAL (pr), DIMENSION (:,:,:,:), ALLOCATABLE, SAVE :: u_out
!  REAL (pr), SAVE,DIMENSION (:,:), POINTER :: c
  REAL (pr), DIMENSION (:), POINTER , SAVE :: scl
  REAL (pr), SAVE ::  eps_post, eps_sim, dx, dy
  INTEGER   ::  j_out!,frm 
  INTEGER   ::  nxyz_out(1:3) !, nxyz(1:3)
  INTEGER   :: i, j, k,  ie ,llc
  REAL (pr), DIMENSION (:,:), ALLOCATABLE, SAVE ::  xx_out
  INTEGER :: ix,jx,ix_l,ix_h,ixp,ixpm,ix_even,ix_odd,iz
  INTEGER :: iy,jy,iy_l,iy_h,iyp,iypm,iy_even,iy_odd
  CHARACTER (LEN=10)    :: epsname  
  CHARACTER (LEN=256)  :: outputfile
  INTEGER   :: idim!, dim
!  LOGICAL :: form
  INTEGER   :: begin_station_num, end_station_num
  INTEGER   :: station_num !station # in saved solution file name
  !ie. test_iso_0001.res is station_num = 1
  INTEGER :: n_var_saved ! number of vars saved
  INTEGER :: saved_vort  ! 1 if we saved vorticity in this file
  INTEGER , DIMENSION(6)::  ibounds
  REAL (pr), DIMENSION (6) :: bounds
  REAL(pr)       :: pi2
  REAL(pr)       :: the_tke,the_tdiss
  REAL(pr)       :: viscosity
  CHARACTER(LEN=FILE_NUM_LEN) :: file_num_str
  INTEGER :: wlt_fmly




  ! READ ARGUMENTS from stdin
  !----------- inputs
 
  CALL read_command_line_input

  call start_reader(TRIM(file_name))      ! initialize general file reader

  res_path = 'results/'
  call input_string ('results_dir',        res_path,          'default','results_dir')
  res_path = TRIM(ADJUSTL(res_path))
  IF (res_path(LEN_TRIM(res_path):LEN_TRIM(res_path)) .NE. '/') res_path = TRIM(ADJUSTL(res_path))//'/'

  call input_string ('file_name',file_gen,'stop', &
         '  base file name')
  IF (file_gen(LEN_TRIM(file_gen):LEN_TRIM(file_gen)) .NE. '.') file_gen = TRIM(ADJUSTL(file_gen))//'.'

  call input_integer ('begin_station_num',begin_station_num,'stop', &
         '  begin_station_num')

  call input_integer ('end_station_num',end_station_num,'stop', &
         '  end_station_num')

  call input_real ('eps_post',eps_post,'stop', &
         '  post-processing threshold value')

  call input_real ('eps_sim',eps_sim,'stop', &
         '  simulation threshold value')

  call input_real ('viscosity',viscosity,'stop', &
         '  viscosity')

  call stop_reader           ! clean reader's memory
  !
  !-- First open the first data file and find out if it is 3D or 2D
  PRINT *,'FILE_NUM_FMT',FILE_NUM_FMT
  PRINT *,'file_num_str',file_num_str,':::'
  WRITE(file_num_str,FILE_NUM_FMT) begin_station_num  
  IC_filename = TRIM( 'results/'//file_gen(1:INDEX(file_gen//' ' ,' ')-1)//''//file_num_str//'.res' )
  CALL read_solution_dim( IC_filename, EXACT=.TRUE., IGNOREINIT = .TRUE.  )
  !Oleg: possible error with '..'
  PRINT *,'after  read_solution_dim , dim = ', dim
  !
  ! -- setup u_variable_names array to tell read_solution which variables to read
  !
  ALLOCATE (u_variable_names(1:3) )
  u_variable_names(:) = ' '
  
  ! make format for reading u_variable_names_fmt
  WRITE(u_variable_names_fmt,  '( ''( A'', I3.3 , '')'' )') u_variable_name_len

  ! In integrated variables (3D)
  WRITE (u_variable_names(1), u_variable_names_fmt) 'Velocity_u_@t  '
  WRITE (u_variable_names(2), u_variable_names_fmt) 'Velocity_v_@t  '
  IF( dim == 3 ) WRITE (u_variable_names(3), u_variable_names_fmt) 'Velocity_w_@t  '
  ! set n_var - it will be the size of scl(:) allocated within read_solution
  n_var = 3
 
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  !Oleg: need to update this to deal with wlt_fmly
  wlt_fmly = 1 !temporarily fix, need to be read form the input variables
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 
 
  !
  !-- Loop through each data file 
  !
  DO station_num = begin_station_num, end_station_num
     PRINT *, 'Process station_num ',station_num
    !--------------------

     WRITE(file_num_str,FILE_NUM_FMT) station_num
     IC_filename = TRIM( TRIM(res_path)//file_gen(1:INDEX(file_gen//' ' , ' ')-1)//''//file_num_str//'.res' )


     CALL read_solution_version( IC_filename )
     IF ( IC_RES_VERSION .EQ. 5 ) THEN
        WRITE (*,'("unsupported version: ",I6)') IC_RES_VERSION      
        PRINT *, 'ERROR: Non-Adaptive Spectrum code only reads serial .res files.'
        PRINT *, 'Exiting ...'                                            
        STOP
     END IF


     ! IC_restart is assumed to be known so it is not passed as a parameter into new
     ! endien independent format read_solution_aux...
     ! It is a parameter for read_solution for backward compatibility only.
     IC_restart = .FALSE.
     CALL read_solution( scl, IC_filename, DO_READ_COMMONFILE_FALSE, &
	  DO_READ_ALL_VARS_FALSE, EXACT=.TRUE., IGNOREINIT = .TRUE. )


     ! set j_out (the max level in the inverse transform) to j_lev 
     ! (the max level of the simulation)
     j_out = j_lev! MIN(j_out,j_lev)
     nxyz_out(1)=mxyz(1)*2**(j_out-1)
     nxyz_out(2)=mxyz(2)*2**(j_out-1)
     IF( dim == 3 ) THEN
        nxyz_out(3)=mxyz(3)*2**(j_out-1)
     ELSE
        nxyz_out(3) = 0
     END IF
     
     ALLOCATE( xx_out(0:MAXVAL(nxyz_out),1:dim) )
     ALLOCATE( u_out(0:nxyz_out(1)+1,0:nxyz_out(2),0:nxyz_out(3),3) )     

     !
     !-- set the real space coordinates for the interpolated field
     !
     DO idim = 1,dim
        DO i=0,nxyz_out(idim)
           xx_out(i,idim)=xx(i*2**(j_lev-j_out),idim)
        END DO
     END DO

     !
     !-------- defining bounds for the regions to be plotted 
     !
     DO idim = 1,dim
        ibounds(2*idim-1) = 0
        DO WHILE (xx_out(ibounds(2*idim-1),dim) < bounds(2*idim-1) .AND. ibounds(2*idim-1) < nxyz_out(idim))
           ibounds(2*idim-1) = ibounds(2*idim-1)+1
        END DO
        ibounds(2*idim) = nxyz_out(idim)
        DO WHILE (xx_out(ibounds(2*idim),dim) > bounds(2*idim) .AND. ibounds(2*idim) > 0)
           ibounds(2*idim) = ibounds(2*idim)-1
        END DO
     END DO

     PRINT *,' MAXVAL(ABS(u(1:nwlt,1))) = ', MAXVAL(ABS(u(1:nwlt,1)))
     PRINT *,' MAXVAL(ABS(u(1:nwlt,2))) = ', MAXVAL(ABS(u(1:nwlt,2)))
     PRINT *,' MAXVAL(ABS(u(1:nwlt,3))) = ', MAXVAL(ABS(u(1:nwlt,3)))
     
     !
     !-------- setting weights for wavelet transform
     !
     CALL c_wlt_trns_interp_setup(j_out, nxyz_out, MAXVAL(nxyz_out), xx_out )
     

     !
     !-- Interpolate to full grid based on epsilon. If epsilon is greater then the
     ! epsilon used in the simulation then a filtered version of the results
     ! will be interpolated to the full grid.
     !
     CALL c_wlt_trns_interp (u_out(:,:,:,1), u(1:nwlt,1), nxyz_out, nwlt, j_lev, j_out, wlt_fmly, scl(1)*eps_post)
     CALL c_wlt_trns_interp (u_out(:,:,:,2), u(1:nwlt,2), nxyz_out, nwlt, j_lev, j_out, wlt_fmly, scl(2)*eps_post)
     CALL c_wlt_trns_interp (u_out(:,:,:,3), u(1:nwlt,3), nxyz_out, nwlt, j_lev, j_out, wlt_fmly, scl(3)*eps_post)
     
     
     PRINT *,' MAXVAL(ABS(u_out(:,:,:,1))) = ', MAXVAL(ABS(u_out(:,:,:,1)))
     PRINT *,' MAXVAL(ABS(u_out(:,:,:,2))) = ', MAXVAL(ABS(u_out(:,:,:,2)))
     PRINT *,' MAXVAL(ABS(u_out(:,:,:,3))) = ', MAXVAL(ABS(u_out(:,:,:,3)))
     
     ! initialize fft's
     ! Last arg true is to zero oddball points in fft.
     pi2 = 8.0D0 *atan(1.0D0 )
     call init_fft(nxyz(1),nxyz(2),nxyz(3),pi2,pi2,pi2, .true. )

     !
     !-- real to fourier space
     !
     call rtoft(u_out(:,:,:,1))
     call rtoft(u_out(:,:,:,2))
     call rtoft(u_out(:,:,:,3))
PRINT *,' MAXVAL(ABS(u_out(:,:,:,1))) = ', MAXVAL(ABS(u_out(:,:,:,1)))
PRINT *,' MAXVAL(ABS(u_out(:,:,:,2))) = ', MAXVAL(ABS(u_out(:,:,:,2)))
PRINT *,' MAXVAL(ABS(u_out(:,:,:,3))) = ', MAXVAL(ABS(u_out(:,:,:,3)))

     !
     !-- Set output file name. If the results were wavelet filtered above (eps_post > eps_sim(ulation) )
     ! this filter eps_post is added to the file name.
     !
     IF( eps_post == 0.0 ) THEN
        outputfile = 'results/'//TRIM(file_gen)
     ELSE
        WRITE (epsname, '(f10.6)') eps_post
        ! WRITE leaves initial blanks if the non-decimal part is not big
        !enough. So we first use INDEX to find where the real number begins
        llc = INDEX(epsname,' ',BACK=.TRUE.)+1; 
        outputfile = 'results/'//TRIM(file_gen)//'filt.eps'//epsname(llc:LEN(epsname))//'_st.'
     END IF

     ! print spectra and dissipation into a file
     CALL print_spectra2(u_out(:,:,:,1),u_out(:,:,:,2),u_out(:,:,:,3), &
          TRIM(outputfile)//file_num_str//'.spectra' ,&
          nxyz(1),nxyz(2),nxyz(3),the_tke,the_tdiss)

     !write log file
     OPEN(1,FILE=outputfile(1:INDEX(outputfile,' ')-1)//'post.turbstats',&
          STATUS='UNKNOWN',POSITION='APPEND')
     WRITE(1,'("% time tke total_diss u_scaled_eps v_scaled_eps w_scaled_eps")')
     WRITE(1,'( 6(E12.5,1X) )') t, the_tke, the_tdiss ,scl(1)*eps_post,scl(2)*eps_post,scl(3)*eps_post
     CLOSE(1)

     !
     !--  fourier to  real space
     !
     call ftort(u_out(:,:,:,1))
     call ftort(u_out(:,:,:,2))
     call ftort(u_out(:,:,:,3))

     !calculate turbulent stats
     CALL trb_stats( nxyz(1),nxyz(2),nxyz(3),&
          u_out(:,:,:,1),u_out(:,:,:,2),u_out(:,:,:,3), t, viscosity, &
          scl, eps_post, eps_sim,nwlt, &
          outputfile(1:INDEX(outputfile,' ')-1)//'turbstats', .TRUE.)
     DEALLOCATE (u, xx, u_out, xx_out)
     CALL  c_wlt_trns_interp_free() !

     ! clean allocated in read_solution()
     CALL read_solution_free (scl)

  END DO !loop through stations
END PROGRAM wavelet_interpolation



