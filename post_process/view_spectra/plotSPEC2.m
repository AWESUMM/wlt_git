function plotSPEC( fname1, fname2, station1it, station2it , plotDir,plotfilename, thelegend , printPlot, fignum) 

if( strcmpi(printPlot, 'print') ) 
    last = 1
else
    last = 0
end

global lw
global lspec
%Plot case for PhD

%fig 11 is station 2 at t=0.08
%fig 12 is station 3 at t=.16
figure(fignum);clf;
figure(fignum+1);clf;
%Giuliano_DNS_spectra(station,fig)
Giuliano_filt05L2_DNS_spectra(1000,fignum)
Giuliano_filt05L2_DNS_spectra(2000,fignum+1)


%set parameters  DNS and LES blow up case and some mdl runs
%dt= 0.0001
%dt= 0.002
%maxt = 0.2064331E-01
%maxt = 0.2
%mint = 0
%mintke_x = 0; maxtke_x = maxt;  mintke =100 ; maxtke = 500
minspec_x = 0 ; maxspec_x = 128; minspec = 1e-4 ; maxspec =  1e3
maxw= 128;
%Ntotal = 256^3 % unadapted total resolution

%fps = 15 %frames per second for the animation (avi file)
lspec = [ 'k  ' ; 'b--' ; 'b-.' ; 'g: ' ]; %BW
%lspec = [ 'k  ' ; 'r  ' ; 'b  ' ; 'g  ' ]; %color
% Plot line width

%manually find closest station
f1 = load([fname1 '.turbstats']);
f2 = load([fname2 '.turbstats']);
%f(:,1) %check station 1, t = 0.08
%f(159,1) %check  station 2, t = 0.16
 
%station 81 , t = 0.0801

fig= fignum;
thelegendloc = [ thelegend ',t=0.08']
manual_spectra([fname1 station1it '.spectra'],fig,thelegendloc,last,minspec_x, maxspec_x, minspec, maxspec,maxw,plotDir, plotfilename,lspec(2,:),1)
manual_spectra([fname2 station1it '.spectra'],fig,thelegendloc,last,minspec_x, maxspec_x, minspec, maxspec,maxw,plotDir, plotfilename,lspec(4,:),1)
                

%station 159 , t = 0.1600
fig= fignum+1;
thelegendloc = [ thelegend ',t=0.16'] 
manual_spectra([fname1 station2it '.spectra'],fig,thelegendloc,last,minspec_x, maxspec_x, minspec, maxspec,maxw,plotDir, plotfilename,lspec(2,:),2)
manual_spectra([fname2 station2it '.spectra'],fig,thelegendloc,last,minspec_x, maxspec_x, minspec, maxspec,maxw,plotDir, plotfilename,lspec(4,:),2)
   

   






