%
% location of wavelet code 
%
WLTHOME = 'c:/dan/research/wavelet_code/'

%
% location of post processing code
%
global POSTPROCESS_DIR % location of post_processing executables
POSTPROCESS_DIR = [WLTHOME 'wlt_3d_V2/post_process']

%
% add path to the post_processing directorys
%
eval( ['addpath ' POSTPROCESS_DIR])
eval( ['addpath ' POSTPROCESS_DIR '/stats_isoturb' ])

%
% cd to results rirectory
%
eval( ['cd ' WLTHOME 'wlt_3d_V2'])

%
% Directory where the DNS data is stored
%
DNS_DIR = [WLTHOME 'saved.results/DNS.stats.decaying.128']

%
% SCALES run used in JOT paper
%
SCALES_DIR = [WLTHOME 'saved.results/JOT.SCALES_final340.run/']
SCALES_RUN = 'decay.g128.dns.tst340dan.'


%
% plot spectra at two stations and compare to DNS and filtered DNS data at these stations
% This uses the SCALES run that is in our latest JOT paper
%
global lw; lw =2 ; % plot line width
%
% Set line types and colors
% FOR PLOTspec the line specifications are int he following order
% 1- DNS spectra
% 2- current run spectra
% 3- filtered DNS  (prints sparc points (circles here) 
global lspec;  lspec = [ 'k  ' ; 'r--' ; 'go ' ]; 
fname=[SCALES_DIR SCALES_RUN];
plotDir ='.'
plotfilename = 'SCALES_RUN'
thelegend = '256^3 SCALES Cdyn eps0.5L2, ';

plotSPEC( fname, ...  % file name for run being compared to DNS
    DNS_DIR, ...      % Directory to find DNs stats
    '0081', ...       % output file number corresponding to first station (t=0.08) !must be set manaully for each new run
    '0159', ...       % output file number corresponding to second station (t=0.16) !must be set manaully for each new run
    plotDir, ...      % directory to put plots
    plotfilename,...  % Root name for plots printed to file
    thelegend , ...   % legend string for this run
    'print',   ...    % id 'print' output is printed to plot file, if '' no printing
    11 )              % base figure number 



% 
% statistics of multiple runs
global lw; lw =2 ;% plot line width
global lspec;  lspec = [ 'k  ' ; 'r--' ; 'r: ' ; 'g--' ; 'm--' ; 'b--' ; 'b-.']; %line specs per run

%
% plot stats for DNS
DNS_stats(1,[DNS_DIR '/dns.data.longrun2'])



%
% JOT scales run
% tst340dan SCALES dan Cdyn eps0.5L2 Mdl , consadj, gridfile=eps,testfilt=2eps,DynSmodGridFilt, SgridFilt,
%
plot_turb_stats(256^3, ...  % total number of non-adaptive points (for calculating compression
    SCALES_DIR ,...         % directory run is located in  
    SCALES_RUN, ...         % base name of run
    [1 324], ...            % begin and end station numbers (file numbers) for run
    0.0,...                 % eps for wlt filter applied during post processing (NOT USED FOR NOW)
     0.5,...                % eps used in simulation (needed for post_processing code)
    0.09,...                % viscity used in simulation (needed for post_processing code)
    1, ...                  % base figure number 
    2,...                   % plot number for comparing multiple plots. if 1 then allplots are cleared before plotting
    'JOT dyn SCALES run',...              % legend string
    'nospectra',...           % if 'spectra' spectras are plotted for all data files with a pause in between (keep hitting return)
    ...                     % else if 'movie' then make an avi movie of the changing spectra (just one run )
    ...                     % else if 'nospectra' no spectra is plotted
    plotDir,...             % directory to put movie (or plots eventually)
    0.6)                    % maximum x for all stats plots.


% nc.0 test run
plot_turb_stats(256^3, ...  % total number of non-adaptive points (for calculating compression
    './',...                % directory run is located in  
    'nc.0.test.', ...       % base name of run
    [0 1], ...              % begin and end station numbers (file numbers) for run
    0.0,...                 % eps for wlt filter applied during post processing (needed for post_processing code)
    0.5,...                 % eps used in simulation (needed for post_processing code)
    0.09,...                % viscity used in simulation (needed for post_processing code)
    1, ...                  % base figure number 
    3,...                   % plot number for comparing multiple plots. if 1 then allplots are cleared before plotting
    'TEST',...              % legend string
    'nospectra',...           % if 'spectra' spectras are plotted for all data files with a pause in between (keep hitting return)
    ...                     % else if 'movie' then make an avi movie of the changing spectra (just one run )
    plotDir,...             % directory to put movie (or plots eventually)
    0.6)                    % maximum x for all stats plots.




