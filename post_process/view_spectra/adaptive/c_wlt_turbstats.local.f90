!#define AdaptiveSpectrum_DEBUG  ! DEBUG: local output inside   PROGRAM wavelet_interpolation


PROGRAM wavelet_interpolation
  USE precision
  USE share_consts
  USE pde
  USE sizes
  USE wlt_trns_mod            ! init_DB(), c_wlt_trns_mask()
  USE field                   ! u
  USE turbulence_statistics
  USE io_3D                   ! read_solution_dim()
  USE io_3D_vars
  USE ft_aux
  USE util_mod                ! set_weights()
  USE wlt_trns_vars           ! indx_DB
  USE wlt_trns_util_mod       ! imp6
  USE input_file_reader
  USE spectrum_parameters
  USE parallel                ! MPI related subroutines
  IMPLICIT NONE

  REAL (pr), DIMENSION (:), POINTER :: scl
  REAL (pr), SAVE                   :: eps_post, eps_sim

  CHARACTER (LEN=4)            :: station_string
  CHARACTER (LEN=10)           :: epsname  
  CHARACTER (LEN=256)          :: outputfile
  CHARACTER (LEN=FILE_NUM_LEN) :: file_num_str

  INTEGER  :: i, llc
  INTEGER  :: begin_station_num, end_station_num, step_station_num, station_num, NUMBER_OF_BINS_READ
  REAL(pr) :: the_tke,the_tdiss
  REAL(pr) :: enrg_total, diss_total ! estimate of sum through all k != 0 (by print_spectra_a)
  REAL(pr) :: e1d_total              ! ... for "1D spectrum" (by print_spectra_a)
  INTEGER  :: e1d_num                ! ...
  REAL(pr) :: viscosity
  LOGICAL  :: print__trb_stats
  LOGICAL  :: DO_APPEND
  LOGICAL  :: station_subdir
  INTEGER  :: verb_read
  
  ! inverse wavelet transform things
  LOGICAL  :: new_grid
  INTEGER  :: j_lev_old, j_filt
  INTEGER  :: trnsf_type
  INTEGER  :: nd_assym_low, nd_assym_bnd_low, nd2_assym_low, nd2_assym_bnd_low 
  INTEGER  :: nd_assym_high, nd_assym_bnd_high, nd2_assym_high, nd2_assym_bnd_high
  INTEGER  :: i_h_inp, i_l_inp

  ! Temporary variables for Parallel Mathematical Operations (e.g. SUM, MIN, MAX)
  REAL(pr) :: tmp1, tmp2, tmp3
  
  INTEGER  ::  io_status




  ! initialize parallel module
  CALL parallel_init
  
  

    ! READ ARGUMENTS from stdin
     !----------- inputs
     CALL read_command_line_input

     CALL start_reader(TRIM(file_name))      ! initialize general file reader

     res_path = 'results/'
     call input_string ('results_dir',        res_path,          'default','results_dir')
     res_path = TRIM(ADJUSTL(res_path))
     IF (res_path(LEN_TRIM(res_path):LEN_TRIM(res_path)) .NE. '/') res_path = TRIM(ADJUSTL(res_path))//'/'

     CALL input_string  ('file_name',         file_gen,          'stop',  '  base file name')
     IF (file_gen(LEN_TRIM(file_gen):LEN_TRIM(file_gen)) .NE. '.') file_gen = TRIM(ADJUSTL(file_gen))//'.'

     station_subdir = .FALSE.
     CALL input_logical ('station_subdir',    station_subdir,    'default','station_subdir')

     CALL input_integer ('begin_station_num', begin_station_num, 'stop',  '  begin_station_num')
     CALL input_integer ('end_station_num',   end_station_num,   'stop',  '  end_station_num')
     CALL input_integer ('step_station_num',  step_station_num,  'stop',  '  step_station_num')

     CALL input_real    ('eps_post',          eps_post,          'stop',  '  post-processing threshold value')
     CALL input_real    ('eps_sim',           eps_sim,           'stop',  '  simulation threshold value')

     CALL input_real    ('viscosity',         viscosity,         'stop',  '  viscosity')  

     MIN_POINTS_PER_BIN = 100
     CALL input_integer ('MIN_POINTS_PER_BIN',MIN_POINTS_PER_BIN,'default', '  minimum # of points in wavenumber space per shell')

     MAX_POINTS_PER_BIN = 50000
     CALL input_integer ('MAX_POINTS_PER_BIN',MAX_POINTS_PER_BIN,'default', '  maximum # of points in wavenumber space per shell')

     PERCENT_OF_POINTS_PER_BIN_TO_USE = 1.0_pr
     CALL input_real ('PERCENT_OF_POINTS_PER_BIN',PERCENT_OF_POINTS_PER_BIN_TO_USE,'default',  '  percentage of points in a shell if between MIN and MAX_POINTS_PER_BIN')
     
     NUMBER_OF_BINS_READ = 2**12
     CALL input_integer ('NUMBER_OF_BINS',NUMBER_OF_BINS_READ,'default',   '  # of shells in wavenumber space')
 
     CALL stop_reader           ! clean reader's memory
  ! IC_restart is assumed to be known so it is not passed as a parameter into new
  ! endien independent format read_solution_aux...
  ! It is a parameter for read_solution for backward compatibility only.
  IC_restart = .FALSE.
  
  
  
  
  
  

  IF (par_rank.EQ.0) THEN
     OPEN  (UNIT=UNIT_SEQUENTIAL_RUN, FILE = 'spectra.log', FORM='formatted', STATUS='old', IOSTAT=io_status)
     IF( io_status == 0 )   READ (UNIT_SEQUENTIAL_RUN, FMT='(I12)')  begin_station_num
     CLOSE (UNIT_SEQUENTIAL_RUN)
  END IF
  CALL parallel_broadcast_int( begin_station_num )
  end_station_num = begin_station_num
  
  
  
  


  
  
  !
  !-- Loop through each data file 
  !
  FIRST_STATION = .TRUE.
  DO station_num = begin_station_num, end_station_num, step_station_num

     IF (par_rank.EQ.0.AND.begin_station_num.NE.end_station_num)   PRINT *, 'Process station_num ',station_num

     !-----------------------------------------------------------
     WRITE(file_num_str,FILE_NUM_FMT) station_num


     IF ( station_subdir ) THEN    
        WRITE(station_string,'(I4.4)') station_num
#ifdef MULTIPROC
        IC_filename = TRIM( TRIM(res_path)//TRIM(station_string)//'/'//file_gen(1:INDEX(file_gen//' ' , ' ')-1)//''//file_num_str//'.p0'//'.res' )
#else
        IC_filename = TRIM( TRIM(res_path)//TRIM(station_string)//'/'//file_gen(1:INDEX(file_gen//' ' , ' ')-1)//''//file_num_str//'.res' )
#endif     
     ELSE
#ifdef MULTIPROC
        IC_filename = TRIM( TRIM(res_path)//file_gen(1:INDEX(file_gen//' ' , ' ')-1)//''//file_num_str//'.p0'//'.res' )
#else
        IC_filename = TRIM( TRIM(res_path)//file_gen(1:INDEX(file_gen//' ' , ' ')-1)//''//file_num_str//'.res' )
#endif     
    END IF





  
  
  !-begin-------------------------------------------------------------------------------------------------------------------------------------------
  ! partial read  +  pre_init_DB
  


     CALL read_solution_version( IC_filename )
     IF ( IC_RES_VERSION .EQ. 5 ) THEN
        CALL read_solution( scl, IC_filename, DO_READ_ALL_VARS_FALSE, DO_READ_COMMONFILE_TRUE, VERBLEVEL=verb_level, EXACT=.TRUE., IGNOREINIT = .TRUE. )
     ELSE

        ! POSTPROCESS_PRE_INIT_DB=.TRUE.  will   CALL read_solution_aux_1  with PART_READ=3  [partial read - read all but u and indx_DB]
        !                                        so that we have the solution dim, j_mn, j_mx, j_lev, prd(:), mxyz(:), ...
        !                                        in order to have the required parameters to   CALL pre_init_DB   prior to reading the solutions in loop
  
#ifdef MULTIPROC
        verb_read = 0
        DO_APPEND = .TRUE.
        CALL read_solution( scl, &
                            IC_filename, &
                            DO_READ_ALL_VARS_TRUE, &
                            DO_READ_COMMONFILE_FALSE,&! Not read .com.res
                            VERBLEVEL=verb_read, &    ! verbouse reading (0, 1, 2)
                            EXACT=.TRUE., &           ! get data without any assumptions
                                                      !    read_solution from a stand-alone application to read exactly what is inside .res file,
                                                      !    without making assumptions if it is a restart or initial condition.
                                                      !    In a stand alone application we don't care, we need exact data.
                            IGNOREINIT = .TRUE., &    ! ingore .inp values
                            APPEND=DO_APPEND, &       ! append data from multiple .res files
                            CLEAN_IC_DD=.FALSE., &    ! CLEAN_IC_D (default T) deallocate IC_par_proc_tree(). Normally we deallocate
                                                      !    and set a new repartitioning, except for called with
                                                      !    domain_meth=-1 res2vis and n2m
                            POSTPROCESS_PRE_INIT_DB= .TRUE. )
#else
        CALL read_solution( scl, &
                            IC_filename, &
                            DO_READ_ALL_VARS_TRUE, &
                            DO_READ_COMMONFILE_FALSE,&! Not read .com.res
                            EXACT=.TRUE., &
                            IGNOREINIT = .TRUE., &
                            POSTPROCESS_PRE_INIT_DB= .TRUE. )
#endif     

     END IF
     
     !  j_mn = 2
     !  j_mx = 6
     !  j_lev = 5
     !  prd(:)= 1
     !  mxyz(:) = 8
     !  dim = 3

#ifdef MULTIPROC
     domain_meth  = 3
     domain_split = 1
     domain_div   = 0
     domain_debug = .FALSE.
     domain_imbalance_tol = (/ 0.1_pr, 0.5_pr, 0.75_pr /)
     domain_zoltan_verb   = 0
#endif
     



     !-----------------------------------------------------------
     ! CALL read_input(...)
    
     IC_restart_mode= 1          ! In order to have   par_proc_tree(:)=IC_par_proc_tree(:)   in   SUBROUTINE init_DB_tree_aux
     IC_adapt_grid  = .FALSE. 

     maxval_n_updt  = maxval(n_updt)
     maxval_n_prdct = maxval(n_prdct)
     n_diff         = maxval(n_prdct)
!     DO i=1,dim
!        xyzlimits(1,i) = xx(0,i)
!        xyzlimits(2,i) = xx(nxyz(i),i)
!        xyzzone(1,i) =-1.0E+15_pr ! to ensure tha tall points are in the zone
!        xyzzone(2,i) = 1.0E+15_pr ! to ensure tha tall points are in the zone
!     END DO

     Scale_Meth    = 3
     Weights_Meth  = 1
     j_tree_root   = j_mn                ! db_tree related variable (normally set in read_input)
     j_mn_init     = j_mn                ! to make sure j_mn will not change in init_DB
     j_mn_evol     = j_mn
     j_lev_init    = j_lev
     j_IC          = j_mx
     j_filt        = j_mx + 20         

     BNDzone       = .FALSE.
     !-----------------------------------------------------------
     !CALL read_input_finalize()
     i_h_inp = 123456
     i_l_inp = 111111

#ifndef AdaptiveSpectrum_DEBUG
     IF (par_rank.EQ.0) &
#endif     
     WRITE(*,'( A,I6, 5(A,I4), A,3I3, 2(A,3I6), 2(A,2I3) )') &
                         ' par_rank=', par_rank, ' dim=', dim, ' j_mn=', j_mn, ' j_mx=', j_mx, ' j_lev=', j_lev, ' j_tree_root=', j_tree_root, &
                         ' prd(:)=', prd(:), ' mxyz(:)=', mxyz(:), ' nxyz(:)=', nxyz(:), ' n_prdct(:)=', n_prdct(:), ' n_updt(:)=', n_updt(:)

  
     ! In parallel prior to  reading the full solution, pre_init_DB  must be called  in order to have a decomposition 
     ! pre_init_DB  >>  init_DB_tree_aux  >>  parallel_domain_decompose  &  DB_declare_globals 
     CALL pre_init_DB                       
  !                                       
  !-end---------------------------------------------------------------------------------------------------------------------------------------------







#ifdef MULTIPROC
     verb_read = 0
     DO_APPEND = .TRUE.
     CALL read_solution( scl, &
                         IC_filename, &
                         DO_READ_ALL_VARS_TRUE, &
                         DO_READ_COMMONFILE_FALSE,&! Not read .com.res
                         VERBLEVEL=verb_read, &    ! verbouse reading (0, 1, 2)
                         EXACT=.TRUE., &           ! get data without any assumptions
                                                   !    read_solution from a stand-alone application to read exactly what is inside .res file,
                                                   !    without making assumptions if it is a restart or initial condition.
                                                   !    In a stand alone application we don't care, we need exact data.
                         IGNOREINIT = .TRUE., &    ! ingore .inp values
                         APPEND=DO_APPEND, &       ! append data from multiple .res files
                         CLEAN_IC_DD=.FALSE. )     ! CLEAN_IC_D (default T) deallocate IC_par_proc_tree(). Normally we deallocate
                                                   !    and set a new repartitioning, except for called with
                                                   !    domain_meth=-1 res2vis and n2m
#else
     CALL read_solution( scl, &
                         IC_filename, &
                         DO_READ_ALL_VARS_TRUE, &
                         DO_READ_COMMONFILE_FALSE,&! Not read .com.res
                         EXACT=.TRUE., &
                         IGNOREINIT = .TRUE. )
#endif     






     n_var = SIZE(u_variable_names(:))


!*********************** READ # of point per bin *****************************
     NUMBER_OF_BINS = MIN(NUMBER_OF_BINS_READ,MINVAL(nxyz)/2)
 !****************************************************************************

     !----------------------------------------------------------------!
     ! |------------------------------------------------------------| !
     ! | NOTE: All this initialization is required for stand-alone  | !
     ! |       wavelet transform and is the same as in res2vis.     | !
     ! |       It could be nice to create a separate subroutine     | !
     ! |       for that initialization sometimes.                   | !
     ! |------------------------------------------------------------| !
     !----------------------------------------------------------------!

     !-----------------------------------------------------------
     ! CALL read_input(...)
    
     IC_adapt_grid  = .FALSE. 

     maxval_n_updt  = maxval(n_updt)
     maxval_n_prdct = maxval(n_prdct)
     n_diff         = maxval(n_prdct)
     DO i=1,dim
        xyzlimits(1,i) = xx(0,i)
        xyzlimits(2,i) = xx(nxyz(i),i)
        xyzzone(1,i) =-1.0E+15_pr ! to ensure tha tall points are in the zone
        xyzzone(2,i) = 1.0E+15_pr ! to ensure tha tall points are in the zone
     END DO

     Scale_Meth    = 3
     Weights_Meth  = 1
     j_tree_root   = j_mn                ! db_tree related variable (normally set in read_input)
     j_mn_init     = j_mn                ! to make sure j_mn will not change in init_DB
     j_mn_evol     = j_mn
     j_lev_init    = j_lev
     j_IC          = j_mx
     j_filt        = j_mx + 20         

     BNDzone       = .FALSE.
     !-----------------------------------------------------------
     !CALL read_input_finalize()
     i_h_inp = 123456
     i_l_inp = 111111

    ! set n_assym_prdct/prdct_bnd/updt/updt_bnd
    ! (keep values from .res file)
    CALL set_max_assymetry ( .TRUE. )

    nbnd = 0
    ibnd=imp6(i_h_inp)
    ibc=imp6(i_l_inp)
    IF(prd(1) == 1) THEN
       ibc(1)=0
       ibc(2)=0
       grid(1)=0
    END IF
    IF(prd(2) == 1) THEN
       ibc(3)=0
       ibc(4)=0
       grid(2)=0
    END IF
    IF(prd(3) == 1) THEN
       ibc(5)=0
       ibc(6)=0
       grid(3)=0
    END IF

     !-----------------------------------------------------------
     ! CALL check_inputs(...)
     jd = 1
     IF(dim==2) THEN
        mxyz(3) = 1
        prd(3) = 1
        jd = 0 
     END IF
     !-----------------------------------------------------------
     !-----------------------------------------------------------
     !  CALL alloc_variable_mappings
     !  CALL map_veloc_timesteps
     ALLOCATE (n_var_adapt(1:n_var,0:1))
     ALLOCATE (n_var_interpolate(1:n_var,0:1))
     ALLOCATE (n_var_index(1:n_var))
     n_var_adapt(:,0) = .TRUE.
     n_var_interpolate(:,0) = .TRUE.
     DO i=1,n_var        
        n_var_index(i) = i        
     END DO
     
     !-----------------------------------------------------------!
     ! |-------------------------------------------------------| !
     ! | END OF WAVELET TRANSFORM VARIABLES INITIALIZATION     | !
     ! |-------------------------------------------------------| !
     !-----------------------------------------------------------!

     
#ifdef AdaptiveSpectrum_DEBUG
     tmp1 = MAXVAL( ABS(u(1:nwlt,1)) )
     tmp2 = MAXVAL( ABS(u(1:nwlt,2)) )
     tmp3 = MAXVAL( ABS(u(1:nwlt,3)) )
     CALL parallel_global_sum( REALMAXVAL=tmp1 )
     CALL parallel_global_sum( REALMAXVAL=tmp2 )
     CALL parallel_global_sum( REALMAXVAL=tmp3 )
     IF (par_rank.EQ.0) THEN
       PRINT *,' MAXVAL(ABS(u(1:nwlt,1))) = ', tmp1
       PRINT *,' MAXVAL(ABS(u(1:nwlt,2))) = ', tmp2
       PRINT *,' MAXVAL(ABS(u(1:nwlt,3))) = ', tmp3
     END IF
     !!!PRINT *,' MAXVAL(ABS(u(1:nwlt,1))) = ', MAXVAL(ABS(u(1:nwlt,1)))
     !!!PRINT *,' MAXVAL(ABS(u(1:nwlt,2))) = ', MAXVAL(ABS(u(1:nwlt,2)))
     !!!PRINT *,' MAXVAL(ABS(u(1:nwlt,3))) = ', MAXVAL(ABS(u(1:nwlt,3)))
#endif     


     ! initialize fft's and internal i_p array
     CALL init_fft

     !
     !-- Set output file name. If the results were wavelet filtered above (eps_post > eps_sim(ulation) )
     ! this filter eps_post is added to the file name.
     !
     IF( eps_post == 0.0 ) THEN
        outputfile = TRIM(res_path)//TRIM(file_gen)
     ELSE
        WRITE (epsname, '(f10.6)') eps_post
        ! WRITE leaves initial blanks if the non-decimal part is not big
        !enough. So we first use INDEX to find where the real number begins
        llc = INDEX(epsname,' ',BACK=.TRUE.)+1; 
        outputfile = TRIM(res_path)//TRIM(file_gen)//'filt.eps'//epsname(llc:LEN(epsname))//'_st.'
     END IF
     
     ! print spectra and dissipation into a file
     ! Perform Shell Averaging
     CALL print_spectra_a( TRIM(outputfile)//file_num_str//'.spectra' , the_tke, the_tdiss, enrg_total, diss_total, e1d_total, e1d_num )
     
     !write log file
     IF (par_rank.EQ.0) THEN
        OPEN(1,FILE=outputfile(1:INDEX(outputfile,' ')-1)//'post.turbstats', STATUS='UNKNOWN',POSITION='APPEND')
        IF (FIRST_STATION) WRITE(1,'("% time   tke   total_diss   u_scaled_eps   v_scaled_eps   w_scaled_eps")')
        WRITE(1,'( 6(E12.5,1X) )') t, the_tke, the_tdiss ,scl(1)*eps_post,scl(2)*eps_post,scl(3)*eps_post
        CLOSE(1)
#ifdef AdaptiveSpectrum_DEBUG
        WRITE(*,'("% time tke total_diss u_scaled_eps v_scaled_eps w_scaled_eps")')
        WRITE(*,'( 6(E12.5,1X) )') t, the_tke, the_tdiss ,scl(1)*eps_post,scl(2)*eps_post,scl(3)*eps_post
#endif     
     END IF

     !-------------------------------------------------------------------------------------------------
     !--  inverse the database
     !-------------------------------------------------------------------------------------------------     

     !CALL pre_init_DB
     CALL init_DB( nwlt_old, nwlt, nwltj_old, nwltj, new_grid, j_lev_old, j_lev, j_mn, j_mx , MAXVAL(nxyz) , nxyz, .TRUE. )
     IF (par_rank.EQ.0) PRINT *, ' AFTER init_DB' 
     
     CALL set_weights()
     IF (par_rank.EQ.0) PRINT *, ' AFTER set_weights' 

     ! this is conditioned by non-updated init_DB subroutine for db_lines,
     ! as soon as it is updated, inverse transform to be removed for all the databases
     IF (TYPE_DB.EQ.DB_TYPE_LINES) &
          CALL c_wlt_trns_mask (u, u, n_var, n_var_interpolate(:,0), n_var_adapt(:,0), .TRUE., .FALSE.,&
                                nwlt, nwlt, j_lev, j_lev, HIGH_ORDER, WLT_TRNS_INV, DO_UPDATE_DB_FROM_U, DO_UPDATE_U_FROM_DB)
     IF (par_rank.EQ.0 .AND. TYPE_DB.EQ.DB_TYPE_LINES) PRINT *, ' AFTER c_wlt_trns_mask' 
     !-------------------------------------------------------------------------------------------------
     !--  inverse wavelet transform things
     !-------------------------------------------------------------------------------------------------
     
     
#ifdef AdaptiveSpectrum_DEBUG
     tmp1 = MAXVAL( ABS(u(1:nwlt,1)) )
     tmp2 = MAXVAL( ABS(u(1:nwlt,2)) )
     tmp3 = MAXVAL( ABS(u(1:nwlt,3)) )
     CALL parallel_global_sum( REALMAXVAL=tmp1 )
     CALL parallel_global_sum( REALMAXVAL=tmp2 )
     CALL parallel_global_sum( REALMAXVAL=tmp3 )
     IF (par_rank.EQ.0) THEN
       PRINT *,' MAXVAL(ABS(u_out(1:nwlt,1))) = ', tmp1
       PRINT *,' MAXVAL(ABS(u_out(1:nwlt,2))) = ', tmp2
       PRINT *,' MAXVAL(ABS(u_out(1:nwlt,3))) = ', tmp3
     END IF
     !!!PRINT *,' MAXVAL(ABS(u_out(1:nwlt,1))) = ', MAXVAL(ABS(u(1:nwlt,1)))
     !!!PRINT *,' MAXVAL(ABS(u_out(1:nwlt,2))) = ', MAXVAL(ABS(u(1:nwlt,2)))
     !!!PRINT *,' MAXVAL(ABS(u_out(1:nwlt,3))) = ', MAXVAL(ABS(u(1:nwlt,3)))
#endif     

#ifdef AdaptiveSpectrum_DEBUG
    print__trb_stats = .TRUE.
#else
    print__trb_stats = .FALSE.
#endif     


     !calculate turbulent stats
     CALL trb_stats( viscosity, scl, eps_post, eps_sim, &
                    outputfile(1:INDEX(outputfile,' ')-1)//'turbstats', print__trb_stats, &
                    enrg_total, diss_total, e1d_total, e1d_num )


     FIRST_STATION = .FALSE.


#ifdef AdaptiveSpectrum_DEBUG
     PRINT *, ' par_rank', par_rank, '    e1d_total=', e1d_total
     CALL parallel_global_sum( REAL=e1d_total  )  ! Just to synchronize all ranks
     PRINT *, ' par_rank', par_rank, '    e1d_total=', e1d_total
#endif     



     ! allocated in MAIN
     DEALLOCATE (n_var_adapt)
     DEALLOCATE (n_var_interpolate)
     DEALLOCATE (n_var_index)
     ! allocated in READ_SOLUTION()
     IF (ALLOCATED  (u_variable_names) ) DEALLOCATE (u_variable_names)
     IF (ALLOCATED  (xx))                DEALLOCATE (xx)
     IF (ALLOCATED  (indx_DB))           DEALLOCATE (indx_DB)
     IF (ALLOCATED  (u))                 DEALLOCATE (u)
     IF (ASSOCIATED (scl))               DEALLOCATE (scl)
     IF (ASSOCIATED (scl_global))        DEALLOCATE (scl_global)
     IF (ALLOCATED  (n_var_wlt_fmly))    DEALLOCATE (n_var_wlt_fmly)

     CALL release_memory_DB
     !CALL DB_finalize   No need; it is called by  release_memory_DB
     
     ! clean allocated in read_solution()
     CALL read_solution_free (scl)
     
  END DO !loop through stations



  IF (par_rank.EQ.0) THEN
     OPEN  (UNIT=UNIT_SEQUENTIAL_RUN, FILE = 'spectra.log', FORM='formatted', STATUS='replace', IOSTAT=io_status)
     WRITE (UNIT_SEQUENTIAL_RUN, FMT='(I12)')  begin_station_num+step_station_num
     CLOSE (UNIT_SEQUENTIAL_RUN)
  END IF



  CALL parallel_finalize

END PROGRAM wavelet_interpolation



