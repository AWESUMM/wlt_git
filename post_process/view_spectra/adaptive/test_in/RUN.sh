#!/bin/sh
# testing of adaptive spectrum statistics
# assumed to be called from /post_process/view_spectra directory
TITLE="The testing suite for AWCM code developers"
VERSION="061106"

function ie {
    if [ $1 -ne 0 ]; then
	echo -e "ERROR: in post_process/view_spectra/adaptive/test_in/RUN.sh\n$2"
	exit 1
    fi
}

# available datastructures (non-adaptive, db_wrk, db_lines, db_tree)
source ../../TestCases/util/RUN_lists.txt
list_dbs="db_non $list_db"

# set output streams
o1=/dev/null
o2=/dev/null
if [ "$1" ]; then o1=$1; fi
if [ "$2" ]; then o2=$2; fi

# copy ccase_small_vortex_array_db_wrk.000[5,6].res
file_1=../../TestCases/SmallVortexArrayTest1/results_good/ccase_small_vortex_array_db_wrk.0005.res
file_2=../../TestCases/SmallVortexArrayTest1/results_good/ccase_small_vortex_array_db_wrk.0006.res
if [ -s $file_1 -a -s $file_2 ]; then
    \mkdir -p adaptive/TEST_LOGS/results && cd adaptive/TEST_LOGS && \
    \cp -rf ../../$file_1 results/ca.0005.res && \
    \cp -rf ../../$file_2 results/ca.0006.res && \
    \cp -rf ../../c_wlt_turbstats.out ../c_wlt_turbstats.db_non.out
    ie $? 1
else
    ie 1 ".res files not found: $file_1, $file_2"
fi

# initialize database pairs
db_pairs=`\sh ../../../../TestCases/util/init_pairs.sh $list_dbs`
np2=`echo $db_pairs|\wc -w`

# print invitation message
echo -e "\t$TITLE. Version $VERSION"
echo "#--------------------------"
echo "#     spectrum statistics [ post_process/view_spectra/adaptive/test_in/RUN.sh ]"
echo "#--------------------------"


echo "# TEST 1: single file processing"
for da in $list_dbs; do
    \cp -rf results/ca.0005.res results/ca.$da.0005.res && \
    \cp -rf results/ca.0006.res results/ca.$da.0006.res
    ie $? 2$da
    echo -e "a.$da\n5\n5\n0.1\n0.1\n0.1\n" | ../c_wlt_turbstats.$da.out 1>>$o1 2>>$o2
    if [ $? -eq 0 ]; then
	echo -ne "  [OK]    \t"
    else
	echo -ne "  [FAILED]\t"
    fi
    echo -e "post_process/view_spectra/adaptive/c_wlt_turbstats.$da.out"
done


echo "# TEST 2: multiple file processing"
for da in $list_dbs; do
    echo -e "a.$da\n5\n6\n0.1\n0.1\n0.1\n" | ../c_wlt_turbstats.$da.out 1>>$o1 2>>$o2
    if [ $? -eq 0 ]; then
	echo -ne "  [OK]    \t"
    else
	echo -ne "  [FAILED]\t"
    fi
    echo -e "post_process/view_spectra/adaptive/c_wlt_turbstats.$da.out"
done


df=diff.log
echo "# TEST 3: datastructure comparisons:"
for (( i=1; i<=`expr $np2 / 2`; i++ )); do
    i2=`expr $i \* 2`
    i1=`expr $i2 - 1`
    d1=`echo $db_pairs|cut -d " " -f $i1`
    d2=`echo $db_pairs|cut -d " " -f $i2`

    `\diff results/a.$d1*.0005.spectra      results/a.$d2*.0005.spectra      >> $df 2>>$df` && \
    `\diff results/a.$d1*.0006.spectra      results/a.$d2*.0006.spectra      >> $df 2>>$df` && \
    `\diff results/a.$d1*_st.post.turbstats results/a.$d2*_st.post.turbstats >> $df 2>>$df` && \
    `\diff results/a.$d1*_st.turbstats      results/a.$d2*_st.turbstats      >> $df 2>>$df`
    if [ $? -ne 0 -o -s $df ]; then
	# We know that adaptive versions lack derivatives etc.
	echo -ne "  [WARNING]\t"
    else
	echo -ne "  [OK]     \t"
    fi
    echo -e "$d1\tvs.\t$d2"
    \rm -rf $df
done

exit 0
