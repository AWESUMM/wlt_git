! 
!     -----------------------------------------------------
!     caluclates several quantities from a homogeneous
!     isotropic turbulence field
!     -----------------------------------------------------
!     ADAPTIVE VERSION
!
MODULE turbulence_statistics
  USE precision
  USE ft_aux
  IMPLICIT NONE
  
  PUBLIC :: trb_stats

CONTAINS
  !
  !
  ! Arguments
  !
  ! vis          -viscosity (input)
  ! scl(1:3)     - eps scaling used in simulation
  ! eps_post     - eps used in post processing
  ! eps_sim      - epsilon used in simulation
  ! nwlt         - number of active wavelets in simulation
  ! outputfile   -output file name  
  ! print_stats  - if true the statistics are printed to stdout
  ! enrg_total   - estimate of spectral values through the whole area (in PRINT_SPECTRA2, file ft_aux.f90)
  ! diss_total   - ... (assumed that PRINT_SPECTRA2 called before TRB_STATS, file c_wlt_turbstats.f90)

  SUBROUTINE trb_stats( vis, scl, eps_post, eps_sim, &
       outputfile, print_stats, enrg_total, diss_total, e1d_total, e1d_num )
    USE pde
    USE field
    USE share_consts
    USE sizes
    USE wlt_trns_mod
    USE wlt_trns_vars
    USE util_vars

    REAL (pr), INTENT (IN) :: vis 
    REAL (pr), INTENT (IN) :: scl(1:3)
    REAL (pr), INTENT (IN) :: eps_post, eps_sim
    CHARACTER (LEN=*), INTENT (IN) ::  outputfile
    LOGICAL, INTENT (IN) ::  print_stats
    REAL(pr), INTENT(IN) :: enrg_total, diss_total   ! precomputed estimate of spectral values through the whole area
    REAL(pr), INTENT(IN) :: e1d_total                ! ... of 1D spectral energy
    INTEGER, INTENT(IN)  :: e1d_num                  ! ...
    REAL (pr) :: tke,  int_length
    REAL (pr) :: utmp, vtmp, wtmp
    REAL (pr) :: umag, mag, wmag, smag
    REAL (pr) :: pi_local, pi2, div1
    REAL (pr) :: temp, time
    REAL (pr) :: beta, gradient

    REAL (pr) :: sspvc, scpvc, sfpvc
    REAL (pr) :: uprime, skew_u, kurt_u
    REAL (pr) :: ssfvc
    REAL (pr) :: tke2
    REAL (pr) :: e11_1, e12_1, e13_1, e1_sum
    REAL (pr) :: ssdpvc, scdpvc, sfdpvc
    REAL (pr) :: ssddpvc, scddpvc, sfddpvc
    REAL (pr) :: duprime, skew_du, kurt_du
    REAL (pr) :: dduprime, skew_ddu, kurt_ddu
    REAL (pr) :: diss
    REAL (pr) :: ediss
    REAL (pr) :: eta, lambda, ldiss
    REAL (pr) :: fts, lett, kts
    REAL (pr) :: ret, rei
    REAL (pr) :: ssspc, scspc, sfspc
    REAL (pr) :: sprime, skew_s, kurt_s

    REAL (pr) :: sssdx, sssdy, sssdz, scsdx, scsdy, scsdz
    REAL (pr) :: sfsdx, sfsdy, sfsdz

    REAL (pr) :: sssddx, sssddy, sssddz, scsddx, scsddy, scsddz
    REAL (pr) :: sfsddx, sfsddy, sfsddz

    REAL (pr) :: dsxprime, skew_dsx, kurt_dsx
    REAL (pr) :: dsyprime, skew_dsy, kurt_dsy
    REAL (pr) :: dszprime, skew_dsz, kurt_dsz

    REAL (pr) :: ddsxprime, skew_ddsx, kurt_ddsx
    REAL (pr) :: ddsyprime, skew_ddsy, kurt_ddsy
    REAL (pr) :: ddszprime, skew_ddsz, kurt_ddsz

    REAL (pr) :: scal_int_len

    REAL (pr) :: area
    INTEGER :: i, j, k, ii, wlt_type, face_type, j_df, kk, iwlt, xyz(1:dim), i_pe(0:dim)
    INTEGER :: outputfileUNIT, count, ie_arr(1:dim), k_size
    INTEGER, ALLOCATABLE :: kav(:,:)
    COMPLEX, ALLOCATABLE :: res(:,:)
    REAL (pr), ALLOCATABLE :: du(:,:,:), d2u(:,:,:)

    !
    !   define mathematical constants
    !
    pi_local=4.0_pr*atan(1.0_pr)
    pi2=2.0_pr*pi_local

    ! 
    ! 
    ! Open file put header
    !
    outputfileUNIT = 157
    OPEN (UNIT = outputfileUNIT, FILE =outputfile, STATUS='unknown',&
         POSITION='APPEND')


    write(UNIT=outputfileUNIT,ADVANCE='NO', &
         FMT='( ''% Time eps_sim, eps_post, scl.u, scl.v, scl.w, nwlt, '')' )       
    write(UNIT=outputfileUNIT,ADVANCE='NO', &
         FMT='( ''percent.compression uprime tke tke.from.Fourier.Components '')' )
    write(UNIT=outputfileUNIT,ADVANCE='NO', &
         FMT='( '' uprime skew.u kurt.u duprime skew.du kurt.du dduprime skew.dd  kurt.ddu  '')' )
    write(UNIT=outputfileUNIT,ADVANCE='YES', &
         FMT= '( ''  vis ediss  eta  lambda  ldiss int.length duprime/dduprime kts lett  ret rei  '' ) ' )

    write(UNIT=outputfileUNIT,ADVANCE='NO', &
         FMT='(  6(e15.7 , '' ''), i11 , '' '' , 1(e15.7 , '' '') )' ) &
         t, eps_sim, eps_post, scl(1), scl(2), scl(3), nwlt,&
         100.0_pr*(1.0_pr- (REAL(nwlt,pr)/REAL(PRODUCT(nxyz(1:dim)),pr)))
    ! For some reason the compression does not always calculate correctly.
    ! some times it says 0.00 compression when it is not??
    
    sspvc = SUM( SUM( u(:,1:dim)**2, DIM=2 ) * dA ) / sumdA
    scpvc = SUM( SUM( u(:,1:dim)**3, DIM=2 ) * dA ) / sumdA 
    sfpvc = SUM( SUM( u(:,1:dim)**4, DIM=2 ) * dA ) / sumdA
    
    tke = 0.5_pr * sspvc
    uprime = SQRT( sspvc/ 3.0_pr )
    skew_u = scpvc/ ( 3.0_pr * uprime**3 )
    kurt_u = sfpvc/ ( 3.0_pr * uprime**4 )
    
    IF ( print_stats ) THEN
       WRITE(*,'(''*************************************************************************'')')
       WRITE(*,'(''* uprime: '', e12.5,50x,''*'')') uprime
       WRITE(*,'(''* TKE from physical components: '', e12.5,28x,''*'')') tke
    END IF
    WRITE(UNIT=outputfileUNIT,ADVANCE='NO', FMT='( e15.7   , '' ''  )' )  uprime
    WRITE(UNIT=outputfileUNIT,ADVANCE='NO', FMT='( e15.7   , '' ''  )' )  tke

    ! an estimate of TKE from fourier components
    tke2 = 2.0_pr * enrg_total
    
    IF ( print_stats ) THEN
       WRITE(*,'(''* TKE from fourier components: '', e12.5,29x,''*'')') tke2
       WRITE(UNIT=outputfileUNIT,ADVANCE='NO', FMT='( e15.7   , '' ''  )' ) &
            tke2
       WRITE(*,'(''* uprime, skew_u, kurt_u: '', 3(1x, e12.5),7x,''*'')')  &
            uprime, skew_u, kurt_u
    END IF
    

    !     calculate derivative statistics
    !     -----------------------------------------
    ! -----------------------------------------------------------------------------------------------------------
    ! -----------------------------------------------------------------------------------------------------------
    ALLOCATE ( du(n_var,nwlt,dim), d2u(n_var,nwlt,dim) )
    CALL c_diff_fast (u, du, d2u, j_lev, nwlt, HIGH_ORDER, 11, n_var, 1, n_var)
!!$    DO i=1,n_var
!!$       WRITE (*,'("   MAXVAL(ABS(u(1:nwlt,",I2,"))) = ",E17.10)') i, MAXVAL(ABS(u(1:nwlt,i)))
!!$       WRITE (*,'("   MAXVAL(ABS(du(1:nwlt,",I2,"))) = ",E17.10)') i, MAXVAL(ABS(du(i,1:nwlt,1:dim)))
!!$    END DO

    ssdpvc = 0.0_pr
    scdpvc = 0.0_pr
    sfdpvc = 0.0_pr
    ssddpvc = 0.0_pr
    scddpvc = 0.0_pr
    sfddpvc = 0.0_pr
    DO i = 1, dim
       ssdpvc = ssdpvc + SUM( du(i,:,i)**2 * dA ) / sumdA
       scdpvc = scdpvc + SUM( du(i,:,i)**3 * dA ) / sumdA
       sfdpvc = sfdpvc + SUM( du(i,:,i)**4 * dA ) / sumdA
       
       ssddpvc = ssddpvc + SUM( d2u(i,:,i)**2 * dA ) / sumdA
       scddpvc = scddpvc + SUM( d2u(i,:,i)**3 * dA ) / sumdA
       sfddpvc = sfddpvc + SUM( d2u(i,:,i)**4 * dA ) / sumdA
    END DO
    
    ediss = 0.0_pr
    DO i = 1, dim
       DO j = 1, dim
          ediss = ediss + SUM( du(i,:,j)*du(i,:,j) * dA ) / sumdA
       END DO
    END DO

    DEALLOCATE ( du, d2u )
    ! -----------------------------------------------------------------------------------------------------------
    ! -----------------------------------------------------------------------------------------------------------

    duprime = sqrt(ssdpvc/3.0_pr)
    dduprime = sqrt(ssddpvc/3.0_pr)
    
    skew_du = scdpvc/(3.0_pr)/duprime**3
    kurt_du = sfdpvc/(3.0_pr)/duprime**4
    
    skew_ddu = scddpvc/(3.0_pr)/dduprime**3
    kurt_ddu = sfddpvc/(3.0_pr)/dduprime**4
    
    ediss = vis * ediss 

    !------------------------------------------------------------------------------------
    !     calucalte 1D spectrum of velocity
    !------------------------------------------------------------------------------------
    CALL c_wlt_trns_mask (u, u, n_var, n_var_interpolate(:,0), n_var_adapt(:,0), .TRUE., .FALSE.,&
         nwlt, nwlt, j_lev, j_lev, HIGH_ORDER, WLT_TRNS_FWD, DO_UPDATE_DB_FROM_U, DO_NOT_UPDATE_U_FROM_DB)
    CALL compute_1d_spectrum( e1_sum, e1d_total, e1d_num )
    !------------------------------------------------------------------------------------
    
    
    !     -----------------------------------------
    IF ( print_stats ) THEN
       WRITE(*,'(''* duprime, skew_du, kurt_du: '', 3(1x, e12.5),4x,''*'')')  &
            duprime, skew_du, kurt_du
       WRITE(*,'(''* dduprime, skew_ddu, kurt_ddu: '', 3(1x, e12.5),1x,''*'')')  &
            dduprime, skew_ddu, kurt_ddu
       WRITE(*,'(''* Viscosity: '', e12.5,47x,''*'')') vis
       WRITE(*,'(''* Kinetic energy dissipation: '', e12.5,30x,''*'')') ediss
    END IF
    
    WRITE(UNIT=outputfileUNIT,ADVANCE='NO', &
         FMT='(9(e15.7 , '' '')  )' )  &
         uprime, skew_u, kurt_u ,duprime, skew_du, kurt_du,dduprime, skew_ddu, kurt_ddu 
    WRITE(UNIT=outputfileUNIT,ADVANCE='NO', &
         FMT='( 2(e15.7 , '' '')  )' )  &
         vis,ediss

    !
    !     length scales
    !
    
    !     Kolmogorov length scale
    
    eta = (vis**3/ediss)**(.25_pr)
    if( print_stats ) &
         write(*,'(''* Kolmogorov length scale: '', e12.5,33x,''*'')') eta
    
    !     Taylor microscale
    
    lambda = uprime/duprime
    if( print_stats ) write(*,'(''* Taylor microscale: '', e12.5,39x,''*'')') lambda
    
    !     Dissipation length scale
    
    ldiss = uprime**3/ediss
    if( print_stats ) &
         write(*,'(''* Dissipation length scale: '', e12.5,32x,''*'')') ldiss
    
    !     Integral length scale
    !int_length = pi_local/(2.0_pr*uprime**2) * (e11_1 + e12_1 + e13_1) / 3.0_pr
    int_length = pi_local/(2.0_pr*uprime**2) * (e1_sum) / 3.0_pr

    if( print_stats ) &
         write(*,'(''* Integral length scale: '', e12.5,35x,''*'')') int_length

    !     Velocity derivative microscale

    if( print_stats ) &
         write(*,'(''* Velocity derivative microscale: '', e12.5,26x,''*'')')  &
         duprime/dduprime
    !
    !     Time scales
    !      

    !     Kolmogorov time scale

    kts = (vis/ediss)**0.5_pr
    if( print_stats ) &
         write(*,'(''* Kolmogorov time scale: '', e12.5,35x,''*'')') kts

    !     Large eddy turnover time

    lett = ldiss/uprime
    if( print_stats ) &
         write(*,'(''* Large-eddy turnover time (diss length scale): '',  e12.5,12x,''*'')') lett

    !
    !     Reynolds numbers
    !

    !     Taylor Re

    ret = uprime * lambda / vis
    if( print_stats ) &
         write(*,'(''* Taylor microscale Reynolds number: '', e12.5,23x,''*'')') ret

    !     Integral Re

    rei = uprime * ldiss / vis
    if( print_stats ) &
         write(*,'(''* Dissipation scale Reynolds number: '', e12.5,23x,''*'')') rei
    if( print_stats ) &
         write(*,'(''*************************************************************************'')')
    write(UNIT=outputfileUNIT,ADVANCE='YES', &
         FMT='( 9(e15.7 , '' '')  )' )  &
         eta, lambda, ldiss,int_length,duprime/dduprime,kts,lett,ret,rei

    CLOSE(UNIT=outputfileUNIT)



  end subroutine trb_stats

end module turbulence_statistics

