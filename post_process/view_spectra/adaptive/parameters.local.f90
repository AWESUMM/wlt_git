MODULE spectrum_parameters
  USE precision
  
  !----------------------------------------------------------------------------------------------------------------------
  ! Parameters for shell average spectrum computing
  ! used as an input for subroutine PRINT_SPECTRA_A which is called from the main program
  !
  ! Shell average spectrum will be computed in bins
  !
  ! Number_of_bins = MIN(nxyz/2) or larger and percent_of_points_to_use = 100.0
  ! will give the same result as the previous non-adaptive version
  !
  ! Bin parameters are considered as hints, non-reasonable values will be automatically adjusted
  !
  ! Some values to be used in TRB_STATS are precomputed in PRINT_SPECTRA_A, so turning shell
  ! spectra computing off will affect some statistics produced by TRB_STATS
  !
  LOGICAL, PARAMETER, PUBLIC  :: DO_SHELL_SPECTRA = .TRUE.                    ! turns on/off shell spectra computing
  INTEGER, PUBLIC  :: NUMBER_OF_BINS != 64                         ! power of two (other value is OK too)
  REAL(pr), PUBLIC :: PERCENT_OF_POINTS_PER_BIN_TO_USE != 1.0_pr   ! 10 looks OK
  INTEGER, PUBLIC  :: MIN_POINTS_PER_BIN != 100                     ! 10 or so looks OK
  INTEGER, PUBLIC  :: MAX_POINTS_PER_BIN != 50000                   !
  ! --- DEBUG parameters ---
  ! Most probably they should be left as they are unless you are
  ! testing some new statistics and would like to speed up
  ! the process by not recomputing Fourier spectra each time.
  LOGICAL, PARAMETER, PUBLIC       :: FT_RESULT_ON = .FALSE.          ! write or read the result of Fourier transform
  CHARACTER*(*), PARAMETER, PUBLIC :: FT_RESULT_FILE = 'ft.res.data'  !  in/from a file depending on the next parameter
  LOGICAL, PARAMETER, PUBLIC       :: FT_RESULT_READ = .TRUE.         ! (TRUE)<-->read, (FALSE)<-->write
  ! --- more useful DEBUG parameters ---
  ! The first one prints spectral values on the screen as
  ! they are computed, for observing the process of computations.
  ! The second one controls uniqueness of random points
  ! distributed inside the shells. For the shells which numbers are
  ! less or equal to UNIQUE_LIMIT all the points will be unique,
  ! for the other shells some points might be the same, especially
  ! if PERCENT_OF_POINTS_PER_BIN_TO_USE value is quite high.
  ! Non-unique points distribution works faster and consumes
  ! less memory, so choose reasonably.
  LOGICAL, PARAMETER, PUBLIC       :: FT_RESULT_PRINT = .TRUE.   ! useful to observe the speed of the computations
  INTEGER, PARAMETER, PUBLIC       :: UNIQUE_LIMIT = 10          ! unique assignment of k-points for the bins below

  
  !----------------------------------------------------------------------------------------------------------------------
  ! Parameters for 1D spectrum computing [  which is the sum of spectral energy along three planes,
  ! in 3D, (kx=0,:,:), (:,ky=0,:), and (:,:,kz=0)  ]
  ! used as an input for subroutine COMPUTE_1D_SPECTRUM which is called from TRB_STATS
  !
  ! The output of COMPUTE_1D_SPECTRUM is the scalar value e1_sum,
  ! which is used solely for the computing of the integral length scale
  !
  ! At least 10 or so percent of points should be used for the reasonable
  ! comparison to the non-adaptive version.
  ! The subroutine is quite slow, although it is not as slow as PRINT_SPECTRA_A
  !
  ! Non-unique points distribution works faster and consumes less memory.
  !
  LOGICAL, PARAMETER, PUBLIC  :: DO_1D = .TRUE.                             ! turns on/off "1D spectrum" computing
  LOGICAL, PARAMETER, PUBLIC  :: DO_UNIQUE_POINTS = .TRUE.                  ! make unique distribution of points
  REAL(pr), PARAMETER, PUBLIC :: PERCENT_OF_POINTS_TO_USE_1D = 0.001_pr      ! 10 or so is OK
  INTEGER, PARAMETER,  PUBLIC :: MIN_POINTS_1D = 100                        !
  INTEGER, PARAMETER,  PUBLIC :: MAX_POINTS_1D = 10000                      !
  
END MODULE spectrum_parameters
