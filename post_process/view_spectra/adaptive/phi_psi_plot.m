clear all;
% close all;

figure;
phi = load( 'phi.dat' );
%plot( phi(:,1), phi(:,2), 'o', phi(:,1), phi(:,3), '-', phi(:,1), phi(:,4), '--', phi(:,1), phi(:,5), '<', phi(:,1), phi(:,5), 's' )
plot( phi(:,1), phi(:,2), '-', phi(:,1), phi(:,3), '-', phi(:,1), phi(:,4), '-', phi(:,1), phi(:,5), '.', phi(:,1), phi(:,5), '-' )
legend('J=1','J=2','J=3','J=4','J=5', 'Location','North')
title('\phi')


figure;
psi = load( 'psi.dat' );
%plot( psi(:,1), psi(:,2), 'o', psi(:,1), psi(:,3), '-', psi(:,1), psi(:,4), '--', psi(:,1), psi(:,5), '<', psi(:,1), psi(:,5), 's' )
plot( psi(:,1), psi(:,2), '-', psi(:,1), psi(:,3), '-', psi(:,1), psi(:,4), '-', psi(:,1), psi(:,5), '.', psi(:,1), psi(:,5), '-' )
legend('J=1','J=2','J=3','J=4','J=5', 'Location','North')
title('\psi')
