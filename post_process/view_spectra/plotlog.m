

% runs from nc.0 (from Giuliano )

% run eps = 0.0 DNS
% tol = 1e-4
cd /home/dan/research/scales/runs/isoturb/isoturb.g
plotspectra_old('decay.g128.dns.tst1.',[2 34],0.0,1,1,'DNS','movie')

% run eps = 0.0 DNS, zero out top level after readin in initial file
% tol = 1e-4
%%cd /home/dan/research/scales/runs/isoturb/isoturb.g
%%plotspectra('decay.g128.dns.tst2.',[0 45],0.0,1,2,'DNS Init zero Jlev','')


% run eps = 0.0 DNS, artifially high viscosity 10*vis = 10*0.09
% tol = 1e-4
%cd /home/dan/research/scales/runs/isoturb/isoturb.g
%plotspectra('decay.g128.dns.tst3.',[0 11],0.0,1,2,'DNS high viscosity','')

% run eps = 0.0 DNS, artifially high viscosity 2*vis = 2*0.09
% tol = 1e-4
%%cd /home/dan/research/scales/runs/isoturb/isoturb.g
%%plotspectra('decay.g128.dns.tst4.',[0 13],0.0,1,3,'DNS 2* viscosity','')

% run eps = 0.0 DNS, artifially high viscosity 1.5*vis = 1.5*0.09
% tol = 1e-4
cd /home/dan/research/scales/runs/isoturb/isoturb.g
plotspectra('decay.g128.dns.tst5.',[0 40],0.0,1,2,'DNS 1.5* viscosity','movie')

% run eps = 0.0 DNS, vis = 0.09 (correct vis)
% restarted from station 1 of tst5
% tol = 1e-4
cd /home/dan/research/scales/runs/isoturb/isoturb.g
plotspectra('decay.g128.dns.tst5b.',[1 25],0.0,1,3,'DNS high vis first dt','movie')

%giuliano's DNS
f=load('/home/dan/data/giuliano.isoturb.decay/dns.data');
figure(1);clf;
subplot(2,1,1); hold on;
plot(f(:,1)-f(1,1),f(:,3),'k')
%axis([0 .06 300 500 ]);
add2legend('Giuliano''s 128^3 DNS');
hold off
subplot(2,1,2); hold on;
plot(f(:,1)-f(1,1),f(:,4),'k')
%axis([0 .06 3000 5500 ]);
add2legend('Giuliano''s DNS');
hold off


% run eps = 0.0 DNS, artifially high viscosity 1.5*vis = 1.5*0.09
% tol = 1e-4
cd /home/dan/research/scales/runs/isoturb/isoturb.g
plotspectra('results.regatta/decay.g128.dns.tst5.',[0 156],0.0,1,2,'DNS 1.5* viscosity','nospectra')
%plotspectra('decay.g128.dns.tst5.eps5e3.',[0 37],0.0,1,2,'SCALES eps5e-3','')
plotspectra('decay.g128.dns.tst5.eps2.384e2.',[0 68],0.0,1,2,'SCALES eps2.384e-2','nospectra')
plotspectra('decay.g128.dns.tst5.eps3.3e2.',[0 99],0.0,1,3,'SCALES eps3.3e-2','nospectra')
plotspectra('decay.g128.dns.tst5.eps4.236e2.',[0 353],0.0,1,2,'SCALES eps4.236e2','nospectra') %93.5%
plotspectra('decay.g128.dns.tst5.eps4.236e2.filtscl075.',[0 66],0.0,1,9,'SCALES eps4.236e2 filtscl075. ','nospectra') 
plotspectra('decay.g128.dns.tst5.eps5.0e2.',[0 31],0.0,1,5,'SCALES eps5.0e-2','nospectra')
plotspectra(128^3,'results.regatta/decay.g128.dns.tst5.eps5e2.filtscl075.',...
       [0 293],0.0,1,2,'SCALES eps5.0e-2 .filtscl075','nospectra')% 94% call this CVS
plotspectra('decay.g128.dns.tst5.eps6.8e2.',[0 34],0.0,1,2,'SCALES eps6.8e-2','nospectra')
plotspectra('decay.g128.dns.tst5.eps6.8e2.filtscl075.',[0 213],0.0,1,3,'SCALES eps6.8e-2 filtscl075.','nospectra') % 96.5% call this CVS
plotspectra('decay.g128.dns.tst5.eps7.8e2.',[0 186],0.0,1,7,'SCALES eps7.8e-2','nospectra')
plotspectra('decay.g128.dns.tst5.eps7.8e2.filtscl075.',[0 44],0.0,1,1,'SCALES eps7.8e-2 filtscl075.','')
plotspectra('decay.g128.dns.tst5.eps9.2e2.',[0 196],0.0,1,8,'SCALES eps9.2e-2','nospectra')
plotspectra('decay.g128.dns.tst5.eps11439.',[0 40],0.0,1,9,'SCALES eps0.11439','nospectra')
plotspectra('decay.g128.dns.tst5.LES48.',[0 46],0.0,1,4,'LES 48^3','')
plotspectra('decay.g128.dns.tst5.LES50a.',[0 46],0.0,1,4,'LES 50^3','')

% testing conservative form of viscous terms
plotspectra('results.regatta/decay.g128.dns.tst7.eps5.0e2.',[0 7],0.0,1,8,'SCALES eps5.0e-2 ConsForm','spectra')

plotspectra('results.regatta/decay.g128.dns.tst7.eps7.8e2.filtscl075.',[0 135],0.0,1,4,...
    'SCALES eps7.8e-2 ConsForm','nospectra')

% Cons form with sgs model  smag with coeff = 0.0001
%plotspectra('results.regatta/decay.g128.dns.tst8.eps7.8e2.filtscl075.',[0 29],0.0,1,3,...
%    'SCALES eps7.8e-2 ConsForm, Cmdl =0.0001','spectra')

% Cons form with sgs model  smag with coeff = -1e-7
plotspectra('results.regatta/decay.g128.dns.tst9.eps7.8e2.filtscl075.',[0 32],0.0,1,2,...
    'SCALES eps7.8e-2 ConsForm, Cmdl =-1e-7','nospectra') 

%  non-conservative vis terms for comparison form with NO sgs model 
plotspectra('results.regatta/decay.g128.dns.tst10.eps7.8e2.filtscl075.',[0 23],0.0,1,3,...
    'SCALES eps7.8e-2 NON-ConsForm, no sgs mdl','nospectra') 

% Cons form with sgs model  smag with coeff = -2e-7
plotspectra('results.regatta/decay.g128.dns.tst11.eps7.8e2.filtscl075.',[0 302],0.0,1,4,...
    'SCALES eps7.8e-2 ConsForm, Cmdl =-2e-7','nospectra') 

% Cons form with sgs model  
plotspectra('results.regatta/decay.g128.dns.tst12.eps7.8e2.filtscl075.',[0 22],0.0,1,5,...
    'SCALES eps7.8e-2 ConsForm, Cmdl =-4e-7','nospectra') 

% Cons form with sgs model  
plotspectra('results.regatta/decay.g128.dns.tst13.eps7.8e2.filtscl075.',[0 30],0.0,1,6,...
    'SCALES eps7.8e-2 ConsForm, Cmdl =-6e-7','nospectra') 

% Cons form with sgs model  
plotspectra('results.regatta/decay.g128.dns.tst14.eps7.8e2.filtscl075.',[0 26],0.0,1,7,...
    'SCALES eps7.8e-2 ConsForm, Cmdl =-8e-7','nospectra') 

% Cons form with sgs model  
plotspectra('results.regatta/decay.g128.dns.tst15.eps7.8e2.filtscl075.',[0 119],0.0,1,8,...
    'SCALES eps7.8e-2 ConsForm, Cmdl =-1e-6','nospectra') 

% Cons form with sgs model  
plotspectra('results.regatta/decay.g128.dns.tst16.eps7.8e2.filtscl090.',[0 23],0.0,1,4,...
    'SCALES eps7.8e-2 ConsForm, Cmdl =-1e-7','nospectra') 

% Cons form with sgs model  
plotspectra('results.regatta/decay.g128.dns.tst17.eps7.8e2.filtscl095.',[0 27],0.0,1,2,...
    'SCALES eps7.8e-2 ConsForm, Cmdl =-1e-7','nospectra') 

% Cons form with sgs model  
plotspectra('results.sp/decay.g128.dns.tst100.',[0 36],0.0,1,2,...
    'SCALES eps5.0e-1L2 ConsForm, Cmdl = -5e-6','nospectra') 

% Cons form with sgs model  
cd /home/dan/research/scales/runs/isoturb/isoturb.g
plotspectra(128^3,'results.regatta/decay.g128.dns.tst5.',[0 156],0.0,1,1,'DNS 1.5* viscosity','nospectra')
plotspectra(128^3,'results.regatta/decay.g128.dns.tst104.',[0 227],0.0,1,2,...
    'SCALES eps5.0e-1L2 ConsForm, Dyn Mdl','nospectra') 
plotspectra(128^3,'results.regatta/decay.g128.dns.tst106.',[0 76],0.0,1,3,...
    'SCALES eps7.0e-1L2 ConsForm, Dyn Mdl','nospectra')              % 98.4 % compression
plotspectra(128^3,'results.regatta/decay.g128.dns.tst107.',[0 41],0.0,1,4,...
    'SCALES eps9.0e-1L2 ConsForm, Dyn Mdl','nospectra') 
plotspectra(128^3,'results.regatta/decay.g128.dns.tst108.',[0 27],0.0,1,5,...
    'SCALES eps1.1 L2 ConsForm, Dyn Mdl','nospectra') 

plotspectra(128^3,'results.regatta/decay.g128.dns.tst114.',[0 27],0.0,1,2,...
    'SCALES eps7.0e-1L2 ConsForm, Dyn Mdl Cs(M=4)','nospectra') 
plotspectra(128^3,'results.regatta/decay.g128.dns.tst115.',[0 52],0.0,1,2,...
    'SCALES eps7.0e-1L2 ConsForm, Dyn Mdl Cs(M=2,j_mn=3)','nospectra')  % lower M=2 99.8 % compression
plotspectra(128^3,'results.regatta/decay.g128.dns.tst116.',[0 212],0.0,1,5,...
    'SCALES eps5.0e-1L2 ConsForm, 2.0* Dyn Mdl Cs','nospectra') 
plotspectra(128^3,'results.regatta/decay.g128.dns.tst117.',[0 145],0.0,1,4,...
    'SCALES eps7.0e-1L2 ConsForm, 2.0 * Dyn Mdl Cs','nospectra') 
plotspectra(128^3,'results.regatta/decay.g128.dns.tst118.',[0 235],0.0,1,2,...
    'SCALES eps0.5L2 Mdl Cs_dyn, ','nospectra') %SCALES 98.3 DynMdl (same as 104 except j_mninit=4)
plotspectra(128^3,'results.regatta/decay.g128.dns.tst119.',[0 219],0.0,1,3,...
    'SCALES eps0.5L2 Mdl Cs= 5.0e-4','nospectra')  %SCALES 98.3 Cs= 5.0e-4 (j_mninit=4)
plotspectra(128^3,'results.regatta/decay.g128.dns.tst120.',[0 222],0.0,1,4,...
    'SCALES eps0.5L2 Mdl Cs = 4.0e-4','nospectra') %SCALES 98.3 Cs= 4.0e-4 (j_mninit=4)

%%% TEST dyn model coeff with and withotu a NX factor
max_x = .61;
cd /home/dan/research/scales/runs/isoturb/isoturb.g
plotspectra(128^3,'results.regatta/decay.g128.dns.tst5.',[0 156],0.0,1,1,...
    'DNS 1.5* viscosity','nospectra',max_x)
plotspectra(128^3,'results.regatta/decay.g128.dns.tst5.eps5e2.filtscl075.',[0 293],0.0,1,2,...
    'CVS eps5.0e-2 .filtscl075','nospectra',max_x)% 94% call this CVS
plotspectra(128^3,'results.regatta/decay.g128.dns.tst104.',[0 227],0.0,1,3,...
    'SCALES eps5.0e-1L2 ConsForm, Dyn Mdl','nospectra',max_x) 
plotspectra(128^3,'results.regatta/decay.g128.dns.tst116.',[0 212],0.0,1,4,...
    'SCALES eps5.0e-1L2 ConsForm, 2.0* Dyn Mdl Cs','nospectra',max_x) 
plotspectra(128^3,'results.regatta/decay.g128.dns.tst152',[0 126],0.0,1,5,...
    'SCALES eps5.0e-1L2 ConsForm, 4.0* Dyn Mdl Cs','nospectra',max_x) 
%plotspectra(128^3,'results.regatta/decay.g128.dns.tst153.',[0 47],0.0,1,6,...
%    'SCALES eps5.0e-1L2 ConsForm, 8.0* Dyn Mdl Cs','nospectra',max_x) 
plotspectra(128^3,'results.regatta/decay.g128.dns.tst154.',[0 52],0.0,1,7,...
    'SCALES eps5.0e-1L2 ConsForm, 2^{(3/2)}* Dyn Mdl Cs','nospectra',max_x) 
%%% END TEST dyn model coeff with and withotu a NX factor


%%% TEST adapting eps from cvs to scales on start up
cd /home/dan/research/scales/runs/isoturb/isoturb.g
plotspectra(128^3,'results.regatta/decay.g128.dns.tst5.',[0 156],0.0,1,1,...
    'DNS 1.5* viscosity','nospectra',0.1)
plotspectra(128^3,'results.regatta/decay.g128.dns.tst5.eps5e2.filtscl075.',[0 293],0.0,1,2,...
    'CVS eps5.0e-2 .filtscl075','nospectra',0.1)% 94% call this CVS
plotspectra(128^3,'results.regatta/decay.g128.dns.tst104.',[0 227],0.0,1,3,...
    'SCALES eps5.0e-1L2 ConsForm, Dyn Mdl','nospectra',0.1) 
%plotspectra(128^3,'results.regatta/decay.g128.dns.tst132.',[0 32],0.0,1,1,...
 %   'SCALES eps0.5L2 Mdl Cs_dyn adapt eps .2->.5 4 steps','nospectra') 
%plotspectra(128^3,'results.regatta/decay.g128.dns.tst134.',[0 231],0.0,1,4,...
 %   'SCALES eps0.5L2 Mdl Cs_dyn adapt eps .2->.5 10 steps','nospectra') 
%plotspectra(128^3,'results.regatta/decay.g128.dns.tst136.',[0 241],0.0,1,3,...
%    'SCALES eps0.5L2 Mdl Cs_dyn adapt eps .2->.5 20 steps','nospectra') 
%plotspectra(128^3,'results.regatta/decay.g128.dns.tst137.',[0 237],0.0,1,4,...
%    'SCALES eps0.5L2 Mdl Cs_dyn adapt eps .2->.5 40 steps','nospectra') 
plotspectra(128^3,'results.regatta/decay.g128.dns.tst150.',[0 60],0.0,1,4,...
    'SCALES eps0.5L2 Mdl Cs_dyn adapt eps .2->.5 100 steps','nospectra',0.1 )
plotspectra(128^3,'results.regatta/decay.g128.dns.tst151.',[0 34],0.0,1,5,...
    'SCALES eps0.5L2 Mdl Cs_dyn adapt eps .2->.5 200 steps','nospectra',0.1) 
%%% END TEST adapting eps from cvs to scales on start up



%%% TEST use eps(noadj) in dyn model
cd /home/dan/research/scales/runs/isoturb/isoturb.g
plotspectra(128^3,'results.regatta/decay.g128.dns.tst5.',[0 156],0.0,1,1,...
    'DNS 1.5* viscosity','nospectra',0.6)
%plotspectra(128^3,'results.regatta/decay.g128.dns.tst5.eps5e2.filtscl075.',[0 293],0.0,1,2,...
%    'CVS eps5.0e-2 .filtscl075','nospectra',0.6)% 94% call this CVS
plotspectra(128^3,'results.regatta/decay.g128.dns.tst104.',[0 227],0.0,1,7,...
    'SCALES eps5.0e-1L2 ConsForm, Dyn Mdl','nospectra',0.6)  %fixed code since this..
plotspectra(128^3,'results.regatta/decay.g128.dns.tst155.',[0 226],0.0,1,3,...
    'SCALES eps0.5L2 Mdl Cs_dyn use eps(noadj)','nospectra',0.6) 
plotspectra(128^3,'results.regatta/decay.g128.dns.tst157.',[0 69],0.0,1,4,...
    'SCALES eps0.5L2 Mdl Cs_dyn test filt= eps, grid filter = eps+adj','nospectra',0.6) 
plotspectra(128^3,'results.regatta/decay.g128.dns.tst158b.',[0 20],0.0,1,5,...
    'SCALES eps0.5L2 Mdl Cs_dyn test filt= eps, grid filter = eps+adj(took out 4 in Mij def','nospectra',0.6) 
plotspectra(128^3,'results.regatta/decay.g128.dns.tst159.',[0 7],0.0,1,5,...
    'SCALES eps0.5L2 Mdl Cs_dyn test filt= eps, grid filter = eps+adj(took out 4 in Mij def) C=.2','nospectra',0.6) 
plotspectra(128^3,'results.regatta/decay.g128.dns.tst160.',[0 4],0.0,1,6,...
    'SCALES eps0.5L2 Mdl Cs_dyn test filt= eps, grid filter = eps+adj(took out 4 in Mij def) C=.5','nospectra',0.6) 
plotspectra(128^3,'results.regatta/decay.g128.dns.tst165.',[0 33],0.0,1,2,...
    'SCALES eps0.5L2 Mdl Cs_dyn test filt= 2eps, grid filter = none , Mcoef=4','nospectra',0.6) 
plotspectra(128^3,'results.regatta/decay.g128.dns.tst166.',[0 64],0.0,1,3,...
    'SCALES eps0.5L2 Mdl Cs_dyn test filt= 2eps, grid filter = eps  Mcoef=4','spectra',0.6) 
plotspectra(128^3,'results.regatta/decay.g128.dns.tst167.',[0 238],0.0,1,4,...
    'SCALES eps0.5L2 Mdl (-1)Cs_dyn test filt= 2eps, grid filter = none, Mcoef=4','nospectra',0.6) 
plotspectra(128^3,'results.regatta/decay.g128.dns.tst168.',[0 213],0.0,1,5,...
    'SCALES eps0.5L2 Mdl (-1)Cs_dyn test filt= 2eps, grid filter = eps, Mcoef=4','nospectra',0.6) 
plotspectra(128^3,'results.regatta/decay.g128.dns.tst169.',[0 16],0.0,1,6,...
    'SCALES eps0.5L2 Mdl (-1)Cs_dyn test filt= eps, grid filter = none, Mcoef=4','nospectra',0.6) 
plotspectra(128^3,'results.regatta/decay.g128.dns.tst164.',[0 20],0.0,1,4,...
    'SCALES LES Mdl Cs_dyn test filt= j-1 lev, grid filter = none','spectra',0.6) 
plotspectra(128^3,'results.regatta/decay.g128.dns.tst170.',[0 214],0.0,1,6,...
    'SCALES eps0.5L2 Csmag=5.0-4','nospectra',0.6) 
plotspectra(128^3,'results.regatta/decay.g128.dns.tst171.',[0 148],0.0,1,2,...
    'SCALES eps0.5L2 Mdl (-1)Cs_dyn test filt= 2eps, grid filter = eps, Mcoef=8','nospectra',0.6) 



plotspectra(128^3,'results.regatta/decay.g128.dns.tst172.',[0 159],0.0,1,3,...
    'SCALES eps0.5L2 Mdl Csmag=6.0-4 ','nospectra',0.1)  % dissipation is low 
plotspectra(128^3,'results.regatta/decay.g128.dns.tst173.',[0 20],0.0,1,2,...
    'SCALES eps0.5L2 Mdl (-1) Cs_dyn test filt= 2eps, grid filter = none, Mcoef=8','nospectra',0.6)  % blew up
plotspectra(128^3,'results.regatta/decay.g128.dns.tst174.',[0 254],0.0,1,3,...
    'SCALES eps0.5L2 Mdl (-0.5)Cs_dyn test filt= 2eps, grid filter = eps, Mcoef=4','nospectra',0.1) 
plotspectra(128^3,'results.regatta/decay.g128.dns.tst175.',[0 32],0.0,1,2,...
    'SCALES eps0.5L2 Mdl (-0.5)Cs_dyn test filt= 2eps, grid filter = eps, Mcoef=4','nospectra',0.6) 
%updated code to incorporate (-0.5)Cs_dyn


cd /home/dan/research/scales/runs/isoturb/isoturb.g
plotspectra(128^3,'results.regatta/decay.g128.dns.tst5.',[0 156],0.0,1,1,...
    'DNS 1.5* viscosity','nospectra',0.6)
plotspectra(128^3,'results.regatta/decay.g128.dns.tst176.',[0 19],0.0,1,2,...
    'LES eps0.5L2 Mdl (-0.5)Cs_dyn test filt= j-1, grid filter = 32^3, Mcoef=4','nospectra',0.6) 
plotspectra(128^3,'results.regatta/decay.g128.dns.tst177.',[0 16],0.0,1,2,...
    'LES eps0.5L2 Mdl Cs=1e-3 grid filter = 32^3, Mcoef=4','spectra',0.6) 
plotspectra(128^3,'results.regatta/decay.g128.dns.tst178.',[0 22],0.0,1,3,...
    'LES eps0.5L2 Mdl Cs=-1e-3, grid filter = 32^3, Mcoef=4','spectra',0.03) 

plotspectra(128^3,'results.regatta/decay.g128.dns.tst180.',[0 81],0.0,1,3,...
    'LES eps0.5L2 Mdl Cs=-1e-3, grid filter = 32^3, Mcoef=4, 200 steps to eps0.0 LES','spectra',0.6) 

plotspectra(128^3,'results.regatta/decay.g128.dns.tst5.eps5e2.filtscl075.',[0 293],0.0,1,2,...
    'CVS eps5.0e-2 .filtscl075','nospectra',0.6)% 94% call this CVS
plotspectra(128^3,'results.regatta/decay.g128.dns.tst181.',[0 79],0.0,1,4,...
    'eps0.2L2 CVS Mdl none','nospectra',0.6) 
plotspectra(128^3,'results.regatta/decay.g128.dns.tst182b.',[0 72],0.0,1,3,...
    'CVS eps0.2L2 Mdl Cs=0.0, no model with cons model form','nospectra',0.6) 

plotspectra(128^3,'results.regatta/decay.g128.dns.tst183.',[0 36],0.0,1,5,...
    'SCALES eps0.5L2 Mdl Cdyn, grid filter = none, test filt = 2eps,Mcoef=4','nospectra',0.6) 

plotspectra(128^3,'results.regatta/decay.g128.dns.tst184.',[0 131],0.0,1,2,...
    'SCALES eps0.5L2 Mdl -1*Cdyn, grid filter = none, test filt = 2eps,Mcoef=4','nospectra',0.6) 

plotspectra(128^3,'results.regatta/decay.g128.dns.tst185.',[0 214],0.0,1,3,...
    'SCALES eps0.5L2 Mdl -1*Cdyn, gridfilt = none, testfilt = 2eps,Mcoef=4,clip 2*(\nu+\nu_t)','nospectra',0.6) 
plotspectra(128^3,'results.regatta/decay.g128.dns.tst186.',[0 217],0.0,1,2,...
    'SCALES eps0.5L2 Mdl Cdyn, gridfilt = none, testfilt = 2eps,Mcoef=4,clip 2*(\nu+\nu_t)','nospectra',0.6) 

plotspectra(128^3,'results.regatta/decay.g128.dns.tst187.',[0 10],0.0,1,6,...
    'SCALES eps0.5L2 Mdl Cs = 5.0e-4 ','nospectra',0.6) 
plotspectra(128^3,'results.regatta/decay.g128.dns.tst188.',[0 270],0.0,1,5,...
    'SCALES eps0.5L2 Mdl Cs = -5.0e-4 ','nospectra',0.6) 

printstats('feb17.cmp.tstfilt2eps.gridfilteps')
printstats('plots/mar5.04.cmp.DNS.SCALES')
printstats('plots/mar5.04.cmp.DNS.SCALES.minusC')
%%% END TEST use eps(noadj) in dyn model


%%%%%%%% Test mdl at 0.6LS eps
cd /home/dan/research/scales/runs/isoturb/isoturb.g
plotspectra(128^3,'results.regatta/decay.g128.dns.tst5.',[0 156],0.0,1,1,...
    'DNS 1.5* viscosity','nospectra',0.6)

plotspectra(128^3,'results.regatta/decay.g128.dns.tst189.',[0 41],0.0,1,2,...
    'Scales eps0.6L2 (99.1%) Cons form no mdl','nospectra',0.6)
plotspectra(128^3,'results.regatta/decay.g128.dns.tst190.',[0 49],0.0,1,3,...
    'Scales eps0.6L2 (99.1%) nonCons form no mdl','nospectra',0.1)
plotspectra(128^3,'results.regatta/decay.g128.dns.tst191.',[0 15],0.0,1,4,...
    'Scales eps0.6L2 (99.1%) nonCons form  mdl Cs=5e-4','nospectra',0.1)

%%%%%%%% Test mdl at 0.6LS eps
cd /home/dan/research/scales/runs/isoturb/isoturb.g
plotspectra(128^3,'results.regatta/decay.g128.dns.tst5.',[0 156],0.0,1,1,...
    'DNS 1.5* viscosity','nospectra',0.6)

plotspectra(128^3,'results.regatta/decay.g128.dns.tst205.',[0 35],0.0,1,2,...
    'Scales eps0.5L2 (%)  mdl Cs=5e-3','nospectra',0.6)
plotspectra(128^3,'results.regatta/decay.g128.dns.tst206.',[0 22],0.0,1,3,...
    'Scales eps0.5L2 (%)  mdl Cs=0.0','nospectra',0.1)
printstats('plots/mar11.04.cmp.DNS.SCALES.Cs')

%%%%%%%% Test mdl at 0.5LS eps test LES
cd /home/dan/research/scales/runs/isoturb/isoturb.g
plotspectra(128^3,'results.regatta/decay.g128.dns.tst5.',[0 156],0.0,1,1,...
    'DNS 1.5* viscosity','nospectra',0.6)
%plotspectra(128^3,'results.regatta/decay.g128.dns.tst213.',[0 18],0.0,1,2,...
%    'LES 1.5* viscosity, 64^3 dyn mdl(wrong sign)','spectra',0.1)
plotspectra(128^3,'results.regatta/decay.g128.dns.tst221.',[0 19],0.0,1,2,...
    'LES 1.5* viscosity, 32^3 dyn mdl','nospectra',0.1)
plotspectra(128^3,'results.regatta/decay.g128.dns.tst223.',[0 10],0.0,1,3,...
    'LES 1.5* viscosity, 32^3 2Cdyn mdl','nospectra',0.1)
plotspectra(128^3,'results.regatta/decay.g128.dns.tst224.',[0 20],0.0,1,4,...
    'LES 1.5* viscosity, 32^3 4Cdyn mdl','nospectra',0.1)
plotspectra(128^3,'results.regatta/decay.g128.dns.tst225.',[0 25],0.0,1,5,...
    'LES 1.5* viscosity, 32^3 8Cdyn mdl','nospectra',0.1)
plotspectra(128^3,'results.regatta/decay.g128.dns.tst226.',[0 33],0.0,1,6,...
    'LES 1.5* viscosity, 32^3 Cs=2e-3 mdl','nospectra',0.1)
plotspectra(128^3,'results.regatta/decay.g128.dns.tst231.',[0 20],0.0,1,2,...
    'LES 1.5* viscosity, 32^3 Cdyn mdl','nospectra',0.1)
plotspectra(128^3,'results.regatta/decay.g128.dns.tst236.',[0 34],0.0,1,3,...
    'LES 1.5* viscosity, 32^3 Cdyn mdl, cfl=0.1','nospectra',0.1)
plotspectra(128^3,'results.regatta/decay.g128.dns.tst252.',[0 129],0.0,1,5,...
    'LES 1.5* viscosity, 64^3 Cdyn mdl, explicit filt j-1, test filt j-2','spectra',0.3)

%%%%%%%% Test mdl at 0.5LS eps find const Cs that is closest 
cd /home/dan/research/scales/runs/isoturb/isoturb.g
plotspectra(128^3,'results.regatta/decay.g128.dns.tst5.',[0 156],0.0,1,1,...
    'DNS 1.5* viscosity','nospectra',0.6)
plotspectra(128^3,'results.regatta/decay.g128.dns.tst211.',[0 7],0.0,1,2,...
    'Scales eps0.5, jmn=3, Cs=1e-4','nospectra',0.1)
plotspectra(128^3,'results.regatta/decay.g128.dns.tst212.',[0 15],0.0,1,3,...
    'Scales eps0.5, jmn=2, Cs=5e-4','nospectra',0.1)
plotspectra(128^3,'results.regatta/decay.g128.dns.tst214.',[0 22],0.0,1,4,...
    'Scales eps0.5, jmn=2, Cs=3e-4','nospectra',0.1)
plotspectra(128^3,'results.regatta/decay.g128.dns.tst218.',[0 18],0.0,1,2,...
    'Scales eps0.5, jmn=2, Cs=1e-3','nospectra',0.1)
plotspectra(128^3,'results.regatta/decay.g128.dns.tst219.',[0 15 ],0.0,1,3,...
    'Scales eps0.5, jmn=2, Cs=5e-4','nospectra',0.1)
plotspectra(128^3,'results.regatta/decay.g128.dns.tst220.',[0 12],0.0,1,4,...
    'Scales eps0.5, jmn=2, Cs=1e-4','nospectra',0.1)
plotspectra(128^3,'results.regatta/decay.g128.dns.tst227.',[0 8],0.0,1,5,...
    'Scales eps0.5, jmn=2, Cs=1e-3','nospectra',0.1)
%fixed mdl in code....

%%%%%%%%%%%%%%%%%%%%
% test fixed coeff mdl with jmn=2, eps=0.5L2, non_cons adj
cd /home/dan/research/scales/runs/isoturb/isoturb.g
plotspectra(128^3,'results.regatta/decay.g128.dns.tst5.',[0 156],0.0,1,1,...
    'DNS 1.5* viscosity','nospectra',0.6)
plotspectra(128^3,'results.regatta/decay.g128.dns.tst234',[0 84],0.0,1,2,...
    'Scales eps0.5, jmn=2, Cs=1e-3','nospectra',0.1)
plotspectra(128^3,'results.regatta/decay.g128.dns.tst235.',[0 135],0.0,1,3,...
    'Scales eps0.5, jmn=2, Cs=5e-3','nospectra',0.6) %NEW
plotspectra(128^3,'results.regatta/decay.g128.dns.tst244.',[0 37],0.0,1,4,...
    'Scales eps0.5, jmn=2, Cs=0.0','nospectra',0.6) %NEW


%%%%%%%%%%%%%%%%%%%%
% test fixed coeff mdl with jmn=3, eps=0.5L2, non_cons adj
cd /home/dan/research/scales/runs/isoturb/isoturb.g
plotspectra(128^3,'results.regatta/decay.g128.dns.tst5.',[0 156],0.0,1,1,...
    'DNS 1.5* viscosity','nospectra',0.6)
plotspectra(128^3,'results.regatta/decay.g128.dns.tst242.',[0 107],0.0,1,2,...
    'Scales eps0.5, jmn=3, Cs=5e-3','nospectra',0.6) %NEW
plotspectra(128^3,'results.regatta/decay.g128.dns.tst243.',[0 129],0.0,1,3,...
    'Scales eps0.5, jmn=3, Cs=3e-3','nospectra',0.6) %NEW
plotspectra(128^3,'results.regatta/decay.g128.dns.tst245.',[0 51],0.0,1,4,...
    'Scales eps0.5, jmn=3, Cs=0.0','nospectra',0.6) %NEW


%%%%%%%%%%%%%%%%%%%%2
% test fixed coeff mdl with jmn=3, eps=0.5L2, CONSERVATIVE adj
cd /home/dan/research/scales/runs/isoturb/isoturb.g
plotspectra(128^3,'results.regatta/decay.g128.dns.tst5.',[0 156],0.0,1,1,...
    'DNS 1.5* viscosity','nospectra',0.6)
plotspectra(128^3,'results.regatta/decay.g128.dns.tst255.',[0 280],0.0,1,2,...
    'Scales eps0.5, jmn=3, Cs=0.0,cons adj','nospectra',0.6) %
plotspectra(128^3,'results.regatta/decay.g128.dns.tst259.',[0 136],0.0,1,3,...
    'Scales eps0.5, jmn=3, Cs=3e3,cons adj','nospectra',0.6) %NEW
plotspectra(128^3,'results.regatta/decay.g128.dns.tst260.',[0 62],0.0,1,4,...
    'Scales eps0.5, jmn=3, Cs=1e3,cons adj','nospectra',0.6) %NEW
plotspectra(128^3,'results.regatta/decay.g128.dns.tst266.',[0 96],0.0,1,5,...
    'Scales eps0.5, jmn=3, Cs=8e-4,cons adj','nospectra',0.6) %NEW
plotspectra(128^3,'results.regatta/decay.g128.dns.tst267.',[0 71],0.0,1,6,...
    'Scales eps0.5, jmn=3, Cs=6e-4,cons adj','nospectra',0.6) %NEW

%%%%%%%% Test dynamic mdl at 0.5LS eps 
cd /home/dan/research/scales/runs/isoturb/isoturb.g
plotspectra(128^3,'results.regatta/decay.g128.dns.tst5.',[0 156],0.0,1,1,...
    'DNS 1.5* viscosity','nospectra',0.6)
plotspectra(128^3,'results.regatta/decay.g128.dns.tst207.',[0 35],0.0,1,2,...
    'Scales eps0.5, Cdyn, gridfilt=eps, testfilt=2eps','nospectra',0.1)
plotspectra(128^3,'results.regatta/decay.g128.dns.tst208.',[0 33],0.0,1,3,...
    'Scales eps0.5, Cdyn, gridfilt=eps, testfilt=2eps, TracefreeLijMij','nospectra',0.1)
plotspectra(128^3,'results.regatta/decay.g128.dns.tst209.',[0 17],0.0,1,3,...
    'Scales eps0.5, Cdyn, gridfilt=eps, testfilt=2eps, clip_Cs','nospectra',0.1)
plotspectra(128^3,'results.regatta/decay.g128.dns.tst216.',[0 30],0.0,1,3,...
    'Scales eps0.5, Cdyn, gridfilt=eps, testfilt=2eps','nospectra',0.1)
plotspectra(128^3,'results.regatta/decay.g128.dns.tst216.',[0 30],0.0,1,3,...
    'Scales eps0.5, Cdyn, gridfilt=none, testfilt=2eps','nospectra',0.1)
plotspectra(128^3,'results.regatta/decay.g128.dns.tst246.',[0 87],0.0,1,3,...
    'Scales eps0.5, Cdyn,local, Cs clipped, gridfilt=none, testfilt=2eps','nospectra',0.1) %Cdyn,local, Cs clipped,
plotspectra(128^3,'results.regatta/decay.g128.dns.tst253.',[0 237],0.0,1,2,...
    'Scales eps0.5, Cdyn, gridfilt=eps, testfilt=2eps, cons adj','nospectra',0.1) %conservative adjacent zone
plotspectra(128^3,'results.regatta/decay.g128.dns.tst270.',[0 67],0.0,1,4,...
    'Scales eps0.5, Cdyn, gridfilt=eps, testfilt=2eps, cons adj','nospectra',0.1) %conservative adjacent zonesam as 253 with updated code, They are the same so I didn't run 270 any more
plotspectra(128^3,'results.regatta/decay.g128.dns.tst271.',[0 156],0.0,1,3,...
    'Scales eps0.5, Cdyn, gridfilt=eps, testfilt=2eps, cons adj,dynSmodGridFilt = true','nospectra',0.6) %c dynSmodGridFilt = true
plotspectra(128^3,'results.regatta/decay.g128.dns.tst274.',[0 143],0.0,1,4,...
    'Scales eps0.5, Cdyn, gridfilt=eps, testfilt=2eps, cons adj, DynMdlFiltLijLast= true','nospectra',0.6) %c dynSmodGridFilt = true


%%% DYNAMIC MDL
plotspectra(128^3,'results.regatta/decay.g128.dns.tst206.',[0 22],0.0,1,3,...
    'Scales eps0.5L2 jmn=3  (%)  mdl Cs=0.0','nospectra',0.1) % SCALES 0.5L2 no mdl
plotspectra(128^3,'results.regatta/decay.g128.dns.tst233.',[0 100],0.0,1,2,...
    'Scales eps0.5, jmn=2, Cdyn, gridfilt=eps, testfilt=2eps','nospectra',0.1)
plotspectra(128^3,'results.regatta/decay.g128.dns.tst237.',[0 83],0.0,1,3,...
    'Scales eps0.5, jmn=3, Cdyn, gridfilt=eps, testfilt=2eps','spectra',0.1) %closest so far
plotspectra(128^3,'results.regatta/decay.g128.dns.tst239.',[0 44],0.0,1,3,...
    'Scales eps0.5, jmn=2, Cdyn, gridfilt=none, testfilt=2eps','nospectra',0.1) %blew up..
plotspectra(128^3,'results.regatta/decay.g128.dns.tst246.',[0 12],0.0,1,4,...
    'Scales eps0.5, jmn=3, Cdyn local Cs clipped, gridfilt=eps, testfilt=2eps','nospectra',0.1) % 

plotspectra(128^3,'results.regatta/decay.g128.dns.tst257.',[0 29],0.0,1,4,...
    'Scales eps0.5, jmn=3, Cdyn , gridfilt=eps, testfilt=2eps, ExplicitFiltNL','spectra',0.3) % NL term = (u u) bar much to dissapative

%cmp conservative adj DYN MDL
cd /home/dan/research/scales/runs/isoturb/isoturb.g
plotspectra(128^3,'results.regatta/decay.g128.dns.tst5.',[0 156],0.0,1,1,...
    'DNS 1.5* viscosity','nospectra',0.6)
plotspectra(128^3,'results.regatta/decay.g128.dns.tst253.',[0 237],0.0,1,2,...
    'Scales eps0.5, jmn=3, Cdyn , gridfilt=eps, testfilt=2eps, consv adj','nospectra',0.6) % 
plotspectra(128^3,'results.regatta/decay.g128.dns.tst255.',[0 280],0.0,1,3,...
    'Scales eps0.5, jmn=3, NoMdl , gridfilt=eps, testfilt=2eps, consv adj','nospectra',0.3) % 
plotspectra(128^3,'results.regatta/decay.g128.dns.tst261.',[0 235],0.0,1,4,...
    'Scales eps0.55, jmn=3, Cdyn , gridfilt=eps, testfilt=2eps, consv adj','nospectra',0.6) % 
plotspectra(128^3,'results.regatta/decay.g128.dns.tst262.',[0 121],0.0,1,5,...
    'Scales eps0.6, jmn=3, Cdyn , gridfilt=eps, testfilt=2eps, consv adj','nospectra',0.6) % 
plotspectra(128^3,'results.regatta/decay.g128.dns.tst263.',[0 37],0.0,1,6,...
    'Scales eps0.65, jmn=3, Cdyn , gridfilt=eps, testfilt=2eps, consv adj','nospectra',0.3) % 
plotspectra_phd(128^3,'results.regatta/decay.g128.dns.tst275.',[0 105],0.0,1,2,...
    'Scales eps0.5, jmn=3, Cdyn,non-cons adj,ExplicitFilterNL = ubar ubar','nospectra',0.6) % SCALES no mdl not dissapative enough

%256^3 runs
%giuliano's DNS
GiulianoDNS

cd /home/dan/research/scales/runs/isoturb/isoturb.g
plotspectra(256^3,'results.regatta/decay.g128.dns.tst122.',[0 100],0.0,1,2,...
    '256^3 CVS eps0.2L2 ','nospectra',0.3) % CVS 99.2%
plotspectra(256^3,'results.regatta/decay.g128.dns.tst138.',[0 60],0.0,1,3,...
    '256^3 CVS eps0.3L2 ','nospectra',0.3) % CVS 99.2%
plotspectra(256^3,'results.regatta/decay.g128.dns.tst140.',[0 54],0.0,1,4,...
    '256^3 CVS eps0.4L2 ','nospectra',0.6) % CVS  99.8%  
plotspectra(256^3,'results.regatta/decay.g128.dns.tst123.',[0 231],0.0,1,5,...
    'SCALES eps0.5L2 Mdl Cs_dyn','nospectra',0.1) %SCALES  non-cons (not dissapative enough)
plotspectra(256^3,'results.regatta/decay.g128.dns.tst141.',[0 339],0.0,1,6,...
    '256^3 CVS eps0.25L2 ','nospectra',0.2) % CVS  99.8%  
plotspectra(256^3,'results.regatta/decay.g128.dns.tst254.',[0 187 ],0.0,1,7,...
    '64^3 LES Explcit+grid filt =j-1, test filt=j-2 ','nospectra',0.1) % LES nu=.09 
plotspectra(256^3,'results.regatta/decay.g128.dns.tst248.',[0 178],0.0,1,2,...
    'SCALES eps0.5L2 Mdl Cs_dyn, gridfile=eps,testfilt=2eps','nospectra',0.6) %SCALES  cons 
plotspectra(256^3,'results.regatta/decay.g128.dns.tst250.',[0 40],0.0,1,3,...
    'SCALES eps0.5L2 Mdl Cs=0.0','nospectra',0.6) %SCALES  cons 
plotspectra(256^3,'results.regatta/decay.g128.dns.tst277.',[0 306],0.0,1,3,...
    'SCALES eps0.5L2 Mdl Cdyn, consadj, gridfile=eps,testfilt=2eps','nospectra',0.6) %SCALES  cons 
printstats('plots/mar11.04.cmp.DNS.CVS.256.')



%THESIS
global lw; lw =2 ;
global lspec;  lspec = [ 'k  ' ; 'r--' ; 'b-.' ; 'g: ' ]; 
%defense 
global lw; lw =3 ;
global lspec;  lspec = [ 'k  ' ; 'r--' ; 'b-.' ; 'm: ' ]; 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 128 cvs NON-CONS
cd /home/dan/research/scales/runs/isoturb/isoturb.g
%%%%DNS%%%%
plotspectra_phd(128^3,'results.regatta/decay.g128.dns.tst5.',[0 156],0.0,1,1,...
    'DNS 1.5* viscosity','nospectra',0.6) %Replace with tst269 when done
%plotspectra(128^3,'results.regatta/decay.g128.dns.tst269.',[0 15],0.0,1,3,...
%    'DNS 1.5* viscosity','nospectra',0.6) %reran DNS to get .res files
%%%%CVS%%%%
plotspectra_phd(128^3,'results.regatta/decay.g128.dns.tst5.eps5e2.filtscl075.',[0 293],0.0,1,2,...
    'CVS eps5.0e-2 .filtscl075','nospectra',0.6)% 94%  CVS Linf norm, olde code version
%plotspectra_phd(128^3,'results.regatta/decay.g128.dns.tst181.',[0 122],0.0,1,4,...
%    'eps0.2L2 CVS Mdl none','nospectra',0.6)  % old case_isoturb.log file format, must disable in plotspectra(replace with tst276 when done)
%%%%SCALES no mdl%%%%
plotspectra_phd(128^3,'results.regatta/decay.g128.dns.tst5.eps9.2e2.',[0 196],0.0,1,3,'SCALES eps9.2e-2','nospectra',0.6)

%plotspectra_phd(128^3,'results.regatta/decay.g128.dns.tst245.',[0 51],0.0,1,3,...
%    'Scales eps0.5L2, jmn=3, Cs=0.0','nospectra',0.6) % Replace with tst272 when done
%plotspectra_phd(128^3,'results.regatta/decay.g128.dns.tst272.',[0 51],0.0,1,3,...
%    'Scales eps0.5, jmn=3, Cs=0.0','nospectra',0.6) % 

printstats_phd(128,'/home/dan/docs/colorado/phd.thesis/plotsIsoturb/CVS_nonconsadj_128')

% END 128 cvs NON-CONS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 128 cvs NON-CONS cmp to LES no mdl
cd /home/dan/research/scales/runs/isoturb/isoturb.g
%%%%DNS%%%%
plotspectra_phd(128^3,'results.regatta/decay.g128.dns.tst5.',[0 156],0.0,1,1,...
    'DNS 1.5* viscosity','nospectra',0.6) %Replace with tst269 when done

%%%%CVS%%%%
plotspectra_phd(128^3,'results.regatta/decay.g128.dns.tst5.eps5e2.filtscl075.',[0 293],0.0,1,2,...
    'CVS eps5.0e-2 .filtscl075','nospectra',0.6)% 94%  CVS Linf norm, olde code version

plotspectra_phd(128^3,'results.regatta/decay.g128.dns.tst5.eps9.2e2.',[0 196],0.0,1,3,...
    'SCALES eps9.2e-2','nospectra',0.6)
%printstats_phd(128,'/home/dan/docs/colorado/phd.thesis/plotsIsoturb/CVS_nonconsadj_128')

plotspectra_phd(128^3,'results.regatta/decay.g128.dns.tst285.',[0 249],0.0,1,4,...
    'LES no mdl (de-aliased)','nospectra',0.6)
%
%Add line on comp plot 1 0.9473= - 48^3/128^3
figure(4); hold on;
plot([0 .1 .2 .3 .4 .5 .6],[94.73 94.73 94.73 94.73 94.73 94.73 94.73 ],'mo');
add2legend('LES comp 2/3 de-aliase');

printstats_phd(128,'/home/dan/docs/colorado/phd.thesis/plotsIsoturb/CVS_wLES_nonconsadj_128')
% END 128 cvs NON-CONS cmp to LES no mdl
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%not used in thesis
% 128 cvs CONS
cd /home/dan/research/scales/runs/isoturb/isoturb.g
%%%%DNS%%%%
plotspectra_phd(128^3,'results.regatta/decay.g128.dns.tst5.',[0 156],0.0,1,1,...
    'DNS 1.5* viscosity','nospectra',0.6) %Replace with tst269 when done
%plotspectra(128^3,'results.regatta/decay.g128.dns.tst269.',[0 15],0.0,1,3,...
%    'DNS 1.5* viscosity','nospectra',0.6) %reran DNS to get .res files
%%%%CVS%%%%
plotspectra_phd(128^3,'results.regatta/decay.g128.dns.tst268.',[0 30],0.0,1,2,...
    'CVS eps0.35, jmn=3, ,cons adj','spectra',0.6) % CVS
%%%%SCALES no mdl%%%%
plotspectra_phd(128^3,'results.regatta/decay.g128.dns.tst255.',[0 280],0.0,1,3,...
    'Scales eps0.5, jmn=3, Cs=0.0,cons adj','nospectra',0.6) % SCALES no mdl
printstats_phd(128,'/home/dan/docs/colorado/phd.thesis/plotsIsoturb/CVS_consadj_128')

% 128 SCALES CONS fixed mdl
cd /home/dan/research/scales/runs/isoturb/isoturb.g
%%%%DNS%%%%
plotspectra_phd(128^3,'results.regatta/decay.g128.dns.tst5.',[0 156],0.0,1,1,...
    'DNS 1.5* viscosity','nospectra',0.6) %Replace with tst269 when done
%plotspectra(128^3,'results.regatta/decay.g128.dns.tst269.',[0 15],0.0,1,3,...
%    'DNS 1.5* viscosity','nospectra',0.6) %reran DNS to get .res files
%%%%SCALES Cs%%%%
plotspectra_phd(128^3,'results.regatta/decay.g128.dns.tst266.',[0 188],0.0,1,2,...
    'Scales eps0.5, jmn=3, Cs=8e-4,cons adj','nospectra',0.6) %NEW
%%%%SCALES no mdl%%%%
plotspectra_phd(128^3,'results.regatta/decay.g128.dns.tst255.',[0 397],0.0,1,3,...
    'Scales eps0.5, jmn=3, Cs=0.0,cons adj','nospectra',0.6) % SCALES no mdl
printstats_phd(128,'/home/dan/docs/colorado/phd.thesis/plotsIsoturb/SCALES_Cs_consadj_128')
%%plot dissipations
   plotsdiss_phd(128^3,'results.regatta/decay.g128.dns.tst5.',[0 156],0.0,1,1,...
    'DNS 1.5* viscosity','nospectra',0.6) %Replace with tst269 when done
   plotsdiss_phd(128^3,'results.regatta/decay.g128.dns.tst266.',[0 188],0.0,1,2,...
    'Scales eps0.5, jmn=3, Cs=8e-4,cons adj','nospectra',0.6) 
%%end plot dissipations

% 128 SCALES CONS Dyn mdl
cd /home/dan/research/scales/runs/isoturb/isoturb.g
%%%%DNS%%%%
plotspectra_phd(128^3,'results.regatta/decay.g128.dns.tst5.',[0 156],0.0,1,1,...
    'DNS 1.5* viscosity','nospectra',0.6) %Replace with tst269 when done
%plotspectra(128^3,'results.regatta/decay.g128.dns.tst269.',[0 15],0.0,1,3,...
%    'DNS 1.5* viscosity','nospectra',0.6) %reran DNS to get .res files
%%%%SCALES Cdyn %%%%
plotspectra_phd(128^3,'results.regatta/decay.g128.dns.tst253.',[0 237],0.0,1,2,...
    'Scales eps0.5, Cdyn, gridfilt=eps, testfilt=2eps, cons adj','nospectra',0.6) %conservative adjacent zone
%%%%SCALES no mdl%%%%
plotspectra_phd(128^3,'results.regatta/decay.g128.dns.tst255.',[0 397],0.0,1,3,...
    'Scales eps0.5, jmn=3, Cs=0.0,cons adj','nospectra',0.6) % SCALES no mdl
%%%%LES%%%%
plotspectra_phd(128^3,'results.regatta/decay.g128.dns.tst252.',[0 179],0.0,1,4,...
    'LES eps0.0, jmn=3, 64^3 Cdyn, gridfilt=j-1, testfilt=j-2,explicit gridfilt,cons adj','nospectra',0.6) % SCALES no mdl
figure(4); hold on;
plot([0 .1 .2 .3 .4 .5 .6],[94.73 94.73 94.73 94.73 94.73 94.73 94.73 ],'go');
add2legend('LES comp 2/3 de-aliase');


printstats_phd(128,'/home/dan/docs/colorado/phd.thesis/plotsIsoturb/SCALES_Cdyn_consadj_128')

%%plot SCALES SGS + vis dissipations
plotspectra_phd(128^3,'results.regatta/decay.g128.dns.tst5.',[0 156],0.0,1,2,...
    'DNS 1.5* viscosity','nospectra',0.6) %Replace with tst269 when done
plotsdiss_phd(128^3,'results.regatta/decay.g128.dns.tst253.',[0 237],0.0,1,3,...
    'Scales eps0.5, Cdyn, gridfilt=eps, testfilt=2eps, cons adj','nospectra',0.6) %
printdiss_phd(128,'/home/dan/docs/colorado/phd.thesis/plotsIsoturb/SCALES_Cdyn_consadj_128')

%%plot LES SGS + vis dissipations
plotspectra_phd(128^3,'results.regatta/decay.g128.dns.tst5.',[0 156],0.0,1,2,...
    'DNS 1.5* viscosity','nospectra',0.6) %Replace with tst269 when done
plotsdiss_phd(128^3,'results.regatta/decay.g128.dns.tst252.',[0 179],0.0,1,4,...
    'LES eps0.0, jmn=3, 64^3 Cdyn, gridfilt=j-1, testfilt=j-2,explicit gridfilt,cons adj','nospectra',0.66) % SCALES no mdl
printdiss_phd(128,'/home/dan/docs/colorado/phd.thesis/plotsIsoturb/LES_Cdyn_consadj_128')



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%256 non-cons CVS
%%%%DNS%%%%
%256^3 runs giuliano's DNS
GiulianoDNS_phd(0)

cd /home/dan/research/scales/runs/isoturb/isoturb.g
%%%% CVS %%%%
plotspectra_phd(256^3,'results.regatta/decay.g128.dns.tst141.',[0 339],0.0,1,2,...
    '256^3 CVS eps0.25L2 ','nospectra',0.2) % CVS  99.8%  
%%%%SCALES no mdl%%%%
plotspectra_phd(256^3,'results.regatta/decay.g128.dns.tst140.',[0 388],0.0,1,3,...
    '256^3 CVS eps0.4L2 ','nospectra',0.6) % CVS  99.8%  
printstats_phd(256,'/home/dan/docs/colorado/phd.thesis/plotsIsoturb/CVS_nonconsadj_256')
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% not used for  now...
%256 cons Cs= fixed
%%%%DNS%%%%
%256^3 runs giuliano's DNS
cd /home/dan/research/scales/runs/isoturb/isoturb.g
GiulianoDNS_phd(1)

%%%% SCALES  %%%%
plotspectra_phd(256^3,'results.regatta/decay.g128.dns.tst280.',[0 231],0.0,1,2,...
    '256^3 eps=0.5L2 mdl =1, Cs=8e-4 ','nospectra',0.6) %%  
plotspectra_phd(256^3,'results.regatta/decay.g128.dns.tst279.',[0 214],0.0,1,3,...
    '256^3 eps=0.5L2 mdl =1, Cs=1e-3 ','nospectra',0.6) %%  
plotspectra_phd(256^3,'results.regatta/decay.g128.dns.tst294.',[0 157],0.0,1,4,...
    '256^3 eps=0.5L2 mdl =1, Cs=3e-4 DynSmodGridFilt','nospectra',0.6) %%  
plotspectra_phd(256^3,'results.regatta/decay.g128.dns.tst295.',[0 223],0.0,1,5,...
    '256^3 eps=0.5L2 mdl =1, Cs=4e-4 DynSmodGridFilt','nospectra',0.6) %%  
printstats_phd(256,'/home/dan/docs/colorado/phd.thesis/plotsIsoturb/SCALES_Cs_consadj_256')



%%plot SGS + vis dissipations
GiulianoDNS_phd(0)
   plotsdiss_phd(256^3,'results.regatta/decay.g128.dns.tst280.',[0 231],0.0,1,2,...
    '256^3 eps=0.5L2 mdl =1, Cs=8e-4 ','nospectra',0.6) %%  
printdiss_phd(256,'/home/dan/docs/colorado/phd.thesis/plotsIsoturb/SCALES_Cs_consadj_256')

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%256 cons Cdyn

 
%%%%DNS%%%%
%256^3 runs giuliano's DNS
GiulianoDNS_phd(1)

cd /home/dan/research/scales/runs/isoturb/isoturb.g
%%%% SCALES  Cdyn%%%%
%plotspectra_phd(256^3,'results.regatta/decay.g128.dns.tst277.',[0 306],0.0,1,2,...
%    'SCALES eps0.5L2 Mdl Cdyn, consadj, gridfile=eps,testfilt=2eps','nospectra',0.6) %SCALES  cons 
plotspectra_phd(256^3,'results.regatta/decay.g128.dns.tst278.',[0 321],0.0,1,2,...
    'SCALES Cdyn eps0.5L2 Mdl , consadj, gridfile=eps,testfilt=2eps,DynSmodGridFilt = true','nospectra',0.6) % SCALES  cons 
%printstats_phd(256,'/home/dan/docs/colorado/phd.thesis/plotsIsoturb/SCALES_Cdyn_consadj_256')

%%%%%% SCALES Cs=8e-4
%plotspectra_phd(256^3,'results.regatta/decay.g128.dns.tst280.',[0 231],0.0,1,3,...
 %   'SCALES Cs=8e-4  256^3 eps=0.5L2 mdl =1, ','nospectra',0.6) %%  
plotspectra_phd(256^3,'results.regatta/decay.g128.dns.tst297.',[0 332],0.0,1,3,...
    'SCALES Cs=1e-4  256^3 eps=0.5L2 mdl =1, ','nospectra',0.6) %%  TEST better fit!!!

%printstats_phd(256,'/home/dan/docs/colorado/phd.thesis/plotsIsoturb/SCALES_Cdyn_WCs_consadj_256')

%%with CVS NON-CONS (not the same adjacent zone!!)
%%%%SCALES no mdl%%%%
%plotspectra_phd(256^3,'results.regatta/decay.g128.dns.tst140.',[0 388],0.0,1,3,...
%    '256^3 CVS eps0.4L2 ','nospectra',0.6) % CVS  99.8%  
%printstats_phd(256,'/home/dan/docs/colorado/phd.thesis/plotsIsoturb/SCALES_Cdyn_consadj_WCVSnoncons_256')

%%%LES dyn mdl
%plotspectra_phd(256^3,'results.regatta/decay.g128.dns.tst254.',[0 187 ],0.0,1,4,...
%    '64^3 LES Explcit+grid filt =j-1, test filt=j-2 ','nospectra',0.6) % LES nu=.09 
plotspectra_phd(256^3,'results.regatta/decay.g128.dns.tst287.',[0 183],0.0,1,4,...
    '64^3 LES Explcit+grid filt =j-1, test filt=j-2 DynSmodGridFilt ','nospectra',0.6) % LES nu=.09 
%
%Add line on comp plot 1 0.9934= - 48^3/256^3
figure(4); hold on;
plot([0 .1 .2 .3 .4 .5 .6],[99.34 99.34 99.34 99.34 99.34 99.34 99.34 ],'go');
add2legend('LES comp 2/3 de-aliase');

printstats_phd(256,'/home/dan/docs/colorado/phd.thesis/plotsIsoturb/SCALES_Cdyn_WCs_wLES_consadj_256')

%%LES no mdl %%%
%plotspectra_phd(256^3,'results.regatta/decay.g128.dns.tst288.',[0 256 ],0.0,1,4,...
%    '64^3 LES Explcit+grid nomdl ','nospectra',0.6) % LES nu=.09 

%%plot SGS + vis dissipations SCALES CDyn
GiulianoDNS_phd(0)
   plotsdiss_phd(256^3,'results.regatta/decay.g128.dns.tst278.',[0 321],0.0,1,2,...
    'SCALES eps0.5L2 Cdyn, consadj, gridfile=eps,testfilt=2eps,DynSmodGridFilt','nospectra',0.6) %%  
printdiss_phd(256,'/home/dan/docs/colorado/phd.thesis/plotsIsoturb/SCALES_Cdyn_consadj_256')

%%plot SGS + vis dissipations SCALES Cs
GiulianoDNS_phd(0)
   plotsdiss_phd(256^3,'results.regatta/decay.g128.dns.tst297.',[0 332],0.0,1,2,...
    'SCALES Cs=8e-4  256^3 eps=0.5L2 mdl =1, ','nospectra',0.6) %%  
printdiss_phd(256,'/home/dan/docs/colorado/phd.thesis/plotsIsoturb/SCALES_Cs_consadj_256')

%%plot SGS + vis dissipations Cdyn LES
GiulianoDNS_phd(0)
   plotsdiss_phd(256^3,'results.regatta/decay.g128.dns.tst254.',[0 187],0.0,1,2,...
    'LES Cdyn Explcit+grid filt =j-1, test filt=j-2 ','nospectra',0.6) %%  
printdiss_phd(256,'/home/dan/docs/colorado/phd.thesis/plotsIsoturb/LES_Cdyn_consadj_256')

%plot spectra at 2 stations
plot_spec_phd_LES_Cdyn_cons_256
plot_specphdSCALES_Cs_cons256
plot_specphdSCALES_Cdyn_cons256


%END 256 cons Cdyn
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%