model =  1
addpath -end C:\Oleg_Vasilyev\Research\FORTRAN\work_wlt_3d_DBs\trunk\WorkCases\Oleg\HomogeneousTurbulence\results

if isunix 
    DIR1 = 'results.regatta/';
    DIR2 = '/regscratch2/olegv/isoturb.g/results/'
else
    DIR1 = 'results.regatta/';
    DIR2 = 'results.regatta/';
end
disp(['directories ' DIR1 ' ' DIR2]);

global lw; lw =2 ;
global lspec;  lspec = [ 'k  ' ; 'r--' ; 'r: ' ; 'g--' ; 'm--' ; 'b--' ; 'b-.']; 

plotspectra_phd(128^3,[DIR1 'decay.g128.dns.tst5.'],[0 156],0.0,1,1,...
    'DNS 1.5* viscosity','nospectra',0.3)

plotspectra_phd(128^3,[DIR1 'decay.g128.dns.tst5.eps5e2.filtscl075.'],[0 293],0.0,1,2,...
'CVS eps5.0e-2 .filtscl075','nospectra',0.3)% 94%  CVS Linf norm, olde code version

plotspectra_phd(128^3,[DIR1 'decay.g128.dns.tst253.'],[0 237],0.0,1,3,...
    'Scales eps0.5, jmn=3, Cdyn , gridfilt=eps, testfilt=2eps, consv adj','nospectra',0.3) % 

if model == 1
    lw = 3
%    plotspectra_phd(128^3,[DIR2 'decay.g128.dns.tst420.'],[0 130],0.0,1,4,...
%        'Scales eps0.5, jmn=3, NoMdl , gridfilt=eps, testfilt=2eps, consv adj','nospectra',0.3) % 
    plotspectra_phd(128^3,[DIR2 'decay.g128.dns.tst400.'],[0 182],0.0,1,4,...
        'Scales eps0.5, jmn=3, NoMdl , gridfilt=eps, testfilt=2eps, consv adj','nospectra',0.3) % 

    plotspectra_phd(128^3,[DIR2 'decay.g128.dns.tst402.'],[0 75],0.0,1,5,...
        'Scales eps0.5, jmn=3, NoMdl , gridfilt=eps, testfilt=2eps, consv adj','nospectra',0.3) % 
    lw =2
    plotspectra_phd(128^3,[DIR2 'decay.g128.dns.tst404.'],[0 27],0.0,1,6,...
        'Scales eps0.55, jmn=3, Cdyn , gridfilt=eps, testfilt=2eps, consv adj','nospectra',0.3) % 
else
    plotspectra_phd(128^3,[DIR2 'decay.g128.dns.tst401.'],[0 83],0.0,1,4,...
        'Scales eps0.55, jmn=3, Cdyn , gridfilt=eps, testfilt=2eps, consv adj','spectra',0.3) % 

    plotspectra_phd(128^3,[DIR2 'decay.g128.dns.tst404.'],[0 27],0.0,1,5,...
        'Scales eps0.6, jmn=3, Cdyn , gridfilt=eps, testfilt=2eps, consv adj','spectra',0.3) % 
    plotspectra_phd(128^3,[DIR2 'decay.g128.dns.tst405.'],[0 17],0.0,1,6,...
        'Scales eps0.6, jmn=3, Cdyn , gridfilt=eps, testfilt=2eps, consv adj','spectra',0.3) % 
end
%plotspectra_phd(128^3,[DIR2 'decay.g128.dns.tst403.'],[0 6],0.0,1,3,...
%    'Scales eps0.65, jmn=3, Cdyn , gridfilt=eps, testfilt=2eps, consv adj','spectra',0.3) % 
