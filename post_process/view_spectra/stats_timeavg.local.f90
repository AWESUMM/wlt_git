MODULE time_average

CONTAINS
  SUBROUTINE spectra_avg    
  USE precision
  USE io_3D
  USE io_3D_vars
  USE input_file_reader

  IMPLICIT NONE
 
  INTEGER                                :: Nit, k, kk, station_num, s0, status, counter
  INTEGER                                :: Kmax, begin_station_num, end_station_num, step_station_num, K_time_average  
  INTEGER,  DIMENSION(:),   ALLOCATABLE  :: Kmax_arr
  REAL(pr), DIMENSION(:,:), ALLOCATABLE  :: spectra
  REAL (pr)                              :: s1, s2, denom
  LOGICAL                                :: do_Constant_K_TimeAverage
  CHARACTER(LEN=FILE_NUM_LEN)            :: file_num_str  
    
    
    ! READ ARGUMENTS from stdin
    !----------- inputs
    CALL read_command_line_input
    
    call start_reader(TRIM(file_name))      ! initialize general file reader
    
    res_path = 'results/'
    call input_string ('results_dir',        res_path,          'default','results_dir')
    res_path = TRIM(ADJUSTL(res_path))
    IF (res_path(LEN_TRIM(res_path):LEN_TRIM(res_path)) .NE. '/') res_path = TRIM(ADJUSTL(res_path))//'/'

    call input_string ('file_name',file_gen,'stop', '  base file name')
    IF (file_gen(LEN_TRIM(file_gen):LEN_TRIM(file_gen)) .NE. '.') file_gen = TRIM(ADJUSTL(file_gen))//'.'
    
    call input_integer ('Kmax',              Kmax,              'stop',  '  maximum wavenumber for which spectrum is computed')
 
    CALL input_integer ('begin_station_num', begin_station_num, 'stop',  '  begin_station_num')
    CALL input_integer ('end_station_num',   end_station_num,   'stop',  '  end_station_num')
    CALL input_integer ('step_station_num',  step_station_num,  'stop',  '  step_station_num')

    do_Constant_K_TimeAverage = .FALSE.
    CALL input_logical ('do_Constant_K_TimeAverage', do_Constant_K_TimeAverage, 'default', '  do_Constant_K_TimeAverage')
    IF ( do_Constant_K_TimeAverage ) THEN
    CALL input_integer ('K_time_average',    K_time_average,    'stop',  '  Specified fixed wavenumber for calculating the time_averaged spectrum')
    END IF



	Nit = (end_station_num-begin_station_num)/step_station_num + 1
	ALLOCATE(Kmax_arr(Nit))
    
   counter = 0
   DO station_num = begin_station_num, end_station_num, step_station_num
	  WRITE(file_num_str,FILE_NUM_FMT) station_num  
      IC_filename = TRIM( TRIM(res_path)//file_gen(1:INDEX(file_gen//' ' , ' ')-1)//''//file_num_str//'.spectra' )
      OPEN(1,file=IC_filename,STATUS='OLD')
      PRINT *, 'Reading station_num :', station_num
 
      READ(1,*)  
      k=0
      status = 0
      DO WHILE( status == 0 )
          read(1,*, IOSTAT = status) s0, s1, s2
          IF (status == 0) k = k+1 
      END DO
	  counter = counter + 1
	  Kmax_arr(counter) = k
      CLOSE(1)
    END DO


    IF ( do_Constant_K_TimeAverage ) THEN
       Kmax = K_time_average
    ELSE
       Kmax = MAX(Kmax,MAXVAL(Kmax_arr))
    END IF
    ALLOCATE(spectra(Kmax,2))
    spectra(:,:) = 0.0

    
    counter = 0
    DO station_num = begin_station_num, end_station_num, step_station_num
       counter = counter + 1
       IF ( (do_Constant_K_TimeAverage==.FALSE.) .OR. (do_Constant_K_TimeAverage==.TRUE. .AND. Kmax_arr(counter)==Kmax) ) THEN
          WRITE(file_num_str,FILE_NUM_FMT) station_num  
          IC_filename = TRIM( TRIM(res_path)//file_gen(1:INDEX(file_gen//' ' , ' ')-1)//''//file_num_str//'.spectra' )
          OPEN(1,file=IC_filename,STATUS='OLD')
          PRINT *, 'Reading station_num :', station_num
  
          READ(1,'(A256)') file_name  
          DO k=1, Kmax_arr(counter)
             read(1,*) s0, s1, s2
             spectra(k,1) = spectra(k,1) + s1
             spectra(k,2) = spectra(k,2) + s2
          END DO
          CLOSE(1)
       END IF
    END DO
   
   
    OPEN(1,file=TRIM( TRIM(res_path)//file_gen(1:INDEX(file_gen//' ' , ' ')-1)//''//'avg.spectra' ),STATUS='UNKNOWN')
    WRITE(1,*) TRIM(file_name)
    IF ( do_Constant_K_TimeAverage ) THEN
       denom = REAL( COUNT(Kmax_arr .EQ. K_time_average) , pr)
       kk    = K_time_average
    ELSE
       denom = REAL(Nit,pr)
       kk    = Kmax
    END IF    
    DO k = 1, kk
       WRITE (1,*) k,' ',spectra(k,1)/denom,' ',spectra(k,2)/denom
    END DO
    CLOSE(1)
    
  END SUBROUTINE spectra_avg
  
END MODULE time_average


PROGRAM averages
  USE time_average
  CALL spectra_avg
END PROGRAM averages

