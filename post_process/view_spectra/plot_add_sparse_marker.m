%
% This routine is used to add markers to a plot but
% to make sure they are evenly spaced out.
function plot_add_sparse_marker(x,y,marker,spacing)
global lw
plot(x(1:max(1,spacing):end),y(1:max(1,spacing):end),marker,'MarkerSize',8,'LineWidth',lw);

end
