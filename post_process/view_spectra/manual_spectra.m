function  manual_spectra(fname,fig,thelegend,last,do_k5_3,minspec_x, maxspec_x ,minspec, maxspec,maxw,plotDir, plotfilename,lspec,station)
    global lw
    global spectra_type
    f = load(fname  );
    
    figure(fig)
    loglog( f(:,1), f(:,spectra_type), lspec ,'LineWidth',lw);hold on

    set(gca,'FontSize',24); %24 good for including in 2 col tex output
    set(gca,'FontName','Times')

     add2legend( thelegend);
     if( last == 1 )
         if do_k5_3
             y = ( 1:maxw).^(-5/3) * exp(6) ;
             loglog(1:maxw, y , 'k--','LineWidth',lw);hold off
             add2legend( 'K^{-{5/3}}');
         end
         grid on;
         axis([ minspec_x maxspec_x 1.e-4 1.e2 ])
         %axis([ minspec_x maxspec_x minspec maxspec ])

         title( plotfilename );
         xlabel('wavenumber');
         if spectra_type == 2
            ylabel('Energy Spectrum');
         elseif spectra_type == 3
            ylabel('Enstrophy (Dissipation) Spectrum');
         end
         set(gcf,'Position',[0 0   600 500])
         %set(gcf,'PaperPositionMode','auto') %print to match screen size
         %%eval(['print -depsc2 -loose '  plotDir '/' plotfilename  '.spectra.st' num2str(station,'%1.1d') '.eps']);
         %eval(['!gv ' plotDir plotfilename  '.spectra.st' num2str(station,'%1.1d') '.eps'])
         %%legend off; title('');
         %%eval(['print -depsc2 -loose '  plotDir '/' plotfilename  '.spectra.st'   num2str(station,'%1.1d')  '.notitle.eps']);
     end