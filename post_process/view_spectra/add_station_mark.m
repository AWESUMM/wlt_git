function add_station_mark( plot_num, station_t, station_num, min_max)  

    lw = 3
    figure(plot_num)
     hold on;
     plot([ station_t station_t], min_max,'k','LineWidth',lw); % station n
     text( station_t-0.05, min_max(2)+0.05, 'Station 1','FontSize',24,'FontName','Times');
     hold off;
    %plot([ 0.08 0.08], [0 .80],'k','LineWidth',lw); % station 1
    %plot([ 0.16 0.16], [0 .60],'k','LineWidth',lw); % station 2
    %text( 0.06, .85, 'Station 1','FontSize',24,'FontName','Times');
    %text( 0.14, .65, 'Station 2','FontSize',24,'FontName','Times');