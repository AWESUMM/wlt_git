global lw
lw = 2;
if isunix 
    DIR1 = 'results.regatta/';
    DIR2 = '/regscratch2/olegv/isoturb.g/results/'
else
    DIR1 = 'results.regatta/';
    DIR2 = 'results.regatta/';
end

fname=[DIR1 'decay.g128.dns.tst278.'];
plotDir ='.'
plotfilename = 'TESTTEST'
thelegend = '256^3 SCALES Cdyn eps0.5L2,t=0.08';
plotSPEC( fname, '0081', '0159',plotDir,plotfilename, thelegend ,'print' ) 
