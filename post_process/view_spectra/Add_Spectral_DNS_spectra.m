function Add_Spectral_DNS_spectra(fig,full_spectra_file,filt_spectra_file1,filt_spectra_file2,legend_str)
global lw lw_dns;
global lspec; 
global spectra_type;
    
    figure(fig)
    %set(gca,'FontSize',24); %24 good for including in 2 col tex output
    %set(gca,'FontName','Times')
    f = load( full_spectra_file );
    loglog( f(:,1), f(:,spectra_type), lspec(1,:),'LineWidth',lw_dns); hold on %unfiltered DNS
    add2legend(legend_str)

    % plot filtered DNS

    f = load( filt_spectra_file1   );
    %loglog( f(:,1), f(:,2), 'r-.','LineWidth',lw);

%    plot_add_sparse_marker( f(:,1), f(:,spectra_type) ,lspec(2,:),3)
%    %add2legend([legend_str ' Wlt. filtered'])
%    add2legend([legend_str ', \epsilon=0.23'])

    f = load( filt_spectra_file2   );
    %loglog( f(:,1), f(:,2), 'r-.','LineWidth',lw);
    plot_add_sparse_marker( f(:,1), f(:,spectra_type) ,lspec(3,:),3)
    add2legend([legend_str ', \epsilon=0.43'])
