function Giuliano_filt_lev3_DNS_spectra(station,fig)

global lw
if isunix 
    DIR1 = 'results.regatta/';
    DIR2 = '/regscratch2/olegv/isoturb.g/results/'
else
    DIR1 = 'results.regatta/';
    DIR2 = 'results.regatta/';
end

    f = load( [ DIR1 'nc.'  num2str(station,'%4.4d ') '.init.spectra'] );
    
    figure(fig)
    set(gca,'FontSize',24); %24 good for including in 2 col tex output
    set(gca,'FontName','Times')
    loglog( f(:,1), f(:,2), 'k','LineWidth',lw); hold on
    
    if( station ==1000) 
         f = load( 'results.regatta/decay.g128.dns.tst306.nc.1000.lev3.filt.0000.spectra'   );
         loglog( f(:,1), f(:,2), 'r--','LineWidth',lw);
    end 
      if( station == 2000) 
         f = load( 'results.regatta/decay.g128.dns.tst306.nc.2000.lev3.filt.0000.spectra'   );
        loglog( f(:,1), f(:,2), 'r--','LineWidth',lw);
    end 
    