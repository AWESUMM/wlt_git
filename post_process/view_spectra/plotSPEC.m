function plotSPEC( fname, DNS_DIR,station1it, station2it , plotDir,plotfilename, thelegend , printPlot, fignum) 

if( strcmpi(printPlot, 'print') ) 
    last = 1
else
    last = 0
end

global lw
global lspec
%Plot case for PhD

%fig 11 is station 2 at t=0.08
%fig 12 is station 3 at t=.16
figure(fignum);clf;
figure(fignum+1);clf;
%Giuliano_DNS_spectra(station,fig)
Giuliano_filt05L2_DNS_spectra(1000,fignum, DNS_DIR)
Giuliano_filt05L2_DNS_spectra(2000,fignum+1, DNS_DIR)



minspec_x = 0 ; maxspec_x = 128; minspec = 1e-4 ; maxspec =  1e3
maxw= 128;

%lspec = [ 'k  ' ; 'b--' ; 'b-.' ; 'g: ' ]; %BW
%lspec = [ 'k  ' ; 'r  ' ; 'b  ' ; 'g  ' ]; %color
% Plot line width

%manually find closest station
%f = load('/home/dan/research/scales/runs/isoturb/isoturb.g/results.regatta/decay.g128.dns.tst278..turbstats');
%f(81,1) %check station 1, t = 0.08
% f(159,1) %check  station 2, t = 0.16
 
%station 81 , t = 0.0801

fig= fignum;
thelegendloc = [ thelegend ',t=0.08']
manual_spectra([fname station1it '.spectra'],fig,thelegendloc,last,minspec_x, maxspec_x, minspec, maxspec,maxw,plotDir, plotfilename,lspec(2,:),1)
                

%station 159 , t = 0.1600
fig= fignum+1;
thelegendloc = [ thelegend ',t=0.16'] 
manual_spectra([fname station2it '.spectra'],fig,thelegendloc,last,minspec_x, maxspec_x, minspec, maxspec,maxw,plotDir, plotfilename,lspec(2,:),2)
   

   






