function [xstm,ystm,zstm,cstm,stm_indx,dim,nz,xmin,xmax,ymin,ymax,zmin,zmax,time,np] ...
    = res2matlab(file,eps,bounds,plot_component,j_min,j_max,station_num,x0,n0,spm,curv)

global POSTPROCESS_DIR
if ispc
  res2matlab_processor = ['..' slsh 'MVS_P2012' slsh 'Debug' slsh 'res2matlab.exe'];
else
  res2matlab_processor = 'res2matlab.out';
end
if size(bounds,2) ~= 6
  % xmin, etc will be defined by MINVAL(xx) inside res2matlab.f90
  bounds = [0.0  0.0  0.0  0.0  0.0  0.0];
  % otherwise, it will be defined by bounds(1:6), also inside res2matlab.f90
end
do_par_read = 0;  % default - read single .res file only
if nargin > 9  % spm present
  if strcmpi(spm,'p')
    do_par_read = 1; %     - read parallel .res files
  end
end
     
xstm=0; ystm=0; zstm=0; cstm=0; stm_indx=0;
ffl=  'inp.dat';
fid = fopen(ffl,'w');
fprintf(fid,'%s\n',file);
fprintf(fid,'%12.5e %12.5e %12.5e %12.5e %12.5e %12.5e %12.5e %7i %7i %7i %7i %7i\n',...
	eps,bounds,j_min,j_max,station_num,station_num,do_par_read);
fprintf(fid,'%s\n',plot_component);
status = fclose(fid);

%eval(['!' POSTPROCESS_DIR slsh res2matlab_processor])

res2matlab_status_run=system([POSTPROCESS_DIR slsh res2matlab_processor]);
if res2matlab_status_run == 0 
    disp('res2matlab: res2matlab is completed');
else
    disp('WARNING res2matlab: res2matlab is not completed');
end

if ispc
  delete('inp.dat')
else
  %!\rm inp.dat
  res2matlab_status_delinp=system('rm inp.dat');
  if res2matlab_status_delinp == 0 
    disp('res2matlab: file inp.dat is deleted');
  else
    disp('WARNING res2matlab: file inp.dat is not deleted');
  end

end

%------------------- reading unformated file written by res2matlab.f90
ffl = 'wrk.dat';
fid = fopen(ffl,'rb','n');
arr = fread(fid,4,'int32');           % dim,np,num,nxyz(dim)
dim = arr(1);
np = arr(2);
nz = arr(3);
stm_num = arr(4);
arr = fread(fid,dim*2+1,'double');           % bounds(1:dim*2), time
xmin = arr(1);
xmax = arr(2);
ymin = arr(3);
ymax = arr(4);
if dim == 3
  zmin = arr(5);
  zmax = arr(6);
  time = arr(7);
else
  zmin = 1;
  zmax = 1;
  nz = 1;
  time = arr(5);
end

arr = fread(fid,stm_num,'double');         % xyz_stm(1:num,1)
xstm = transpose(arr);

arr = fread(fid,stm_num,'double');        % xyz_stm(1:num,2)
ystm = transpose(arr);

arr = fread(fid,stm_num,'double');         % xyz_stm(1:num,3)
zstm = transpose(arr);

arr = fread(fid,stm_num,'double');         % xyz_stm(1:num,4)
cstm = transpose(arr);

%if dim == 3
  stm_indx(1:stm_num,1:dim) = 0.0;
  
  arr = fread(fid,stm_num,'int32');       % index_stm(1:num,1)
  stm_indx(1:stm_num,1) = arr(1:stm_num);
  
  arr = fread(fid,stm_num,'int32'); % index_stm(1:num,2)
  stm_indx(1:stm_num,2) = arr(1:stm_num);

  arr = fread(fid,stm_num,'int32'); % index_stm(1:num,3)
  stm_indx(1:stm_num,3) = arr(1:stm_num);
  
%end
clear arr
status = fclose(fid);


if ispc
  delete('wrk.dat') 
else
 %!\rm wrk.dat  
 res2matlab_status_delwrk=system('rm wrk.dat');
  if res2matlab_status_delwrk == 0 
    disp('res2matlab: file wrk.dat is deleted');
  else
    disp('WARNING res2matlab: file wrk.dat is not deleted');
  end
end

istm=0;
%if dim == 3 & exist('x0') & exist('n0') % find the grid points between planes
if exist('x0') & exist('n0') % find the grid points between planes
  nplanes=size(n0,2);
  for i =1:nplanes
    n0(:,i)=n0(:,i)./norm(n0(:,i),2);
  end
  igrd=zeros(size(xstm));
  if curv == 1 
      szstm = size(xstm);
      ii = linspace(szstm(1),szstm(2),szstm(2));
  else
      for i = 1:nplanes
        [ii]=find ( abs((xstm-x0(1)).*n0(1,i)+(ystm-x0(2)).*n0(2,i)+(zstm-x0(3)).*n0(3,i)) <= x0(4));
        igrd(ii)=1;
      end
      [ii]=find( igrd == 1);
  end
  xstm=xstm(ii);
  ystm=ystm(ii);
  zstm=zstm(ii);
  cstm=cstm(ii);
  stm_indx=stm_indx(ii,1:3);
  istm=length(xstm);
end
if  istm  == 0 
  fprintf(1,'Zero wlt Coefficients > eps, Raise the value of eps. exiting...\n')
  return
end
fprintf(1,'# coeff''s above eps = %d \n', length(xstm))

clear ii
