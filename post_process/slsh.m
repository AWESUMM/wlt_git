function [pl] = slsh
if isunix
    pl = '/'
else
    pl = '\';
end