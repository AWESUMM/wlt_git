
! Post-Processing Type: inside   PROGRAM wlt3d_user_post_process
!#define userpostprocess_AdpEpsSpatialEvolution
#define userpostprocess_parallel_misbalance

! DEBUG: local output inside   PROGRAM wlt3d_user_post_process
!!#define userpostprocess_DEBUG



PROGRAM wlt3d_user_post_process
  USE precision
  USE share_consts
  USE pde
  USE sizes
  USE wlt_trns_mod              ! init_DB(), c_wlt_trns_mask()
  USE field                     ! u
  USE io_3D                     ! read_solution_dim()
  USE io_3D_vars                ! DO_READ_ALL_VARS_TRUE
  USE util_mod                  ! set_weights()
  USE wlt_trns_vars             ! indx_DB
  USE wlt_trns_util_mod         ! imp6
  USE input_file_reader
  USE parallel                  ! MPI related subroutines
  USE util_vars                 ! dA

  IMPLICIT NONE
!#ifdef MULTIPROC
!  INCLUDE 'mpif.h'                  ! MPI subroutines
!#endif

  REAL (pr), DIMENSION (:), POINTER  :: scl
  REAL (pr), SAVE                    ::  eps_post, eps_sim

  CHARACTER(LEN=10)           :: epsname  
  CHARACTER(LEN=256)          :: outputfile
  CHARACTER(LEN=FILE_NUM_LEN) :: file_num_str
! CHARACTER(LEN=MAXLEN)       :: tmp

  LOGICAL  :: SINGLE_FILE
  LOGICAL  :: DO_APPEND

  INTEGER  :: i, llc
  INTEGER  :: begin_station_num, end_station_num, step_station_num, station_num, NUMBER_OF_BINS_READ
  INTEGER  :: e1d_num                ! ...
  INTEGER  :: Allocate_Status
  INTEGER  :: verb_read

  REAL(pr) :: the_tke,the_tdiss
  REAL(pr) :: enrg_total, diss_total ! estimate of sum through all k != 0 (by print_spectra_a)
  REAL(pr) :: e1d_total              ! ... for "1D spectrum" (by print_spectra_a)
  REAL(pr) :: viscosity

  
  ! inverse wavelet transform things
  LOGICAL  :: new_grid
  INTEGER  :: j_lev_old, j_filt
  INTEGER  :: trnsf_type
  INTEGER  :: nd_assym_low, nd_assym_bnd_low, nd2_assym_low, nd2_assym_bnd_low 
  INTEGER  :: nd_assym_high, nd_assym_bnd_high, nd2_assym_high, nd2_assym_bnd_high
  INTEGER  :: i_h_inp, i_l_inp

 


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! CUSTOM  POST  PROCESS  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!      MisBalance Calculations      !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
#ifdef userpostprocess_parallel_misbalance
  INTEGER, ALLOCATABLE ::  MyRank_nwlt(:)!, send_check(:)
#endif
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! CUSTOM  POST  PROCESS  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! SCALES__AdpEpsSpatialEvolution  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
#ifdef userpostprocess_AdpEpsSpatialEvolution

  INTEGER   :: nvar__RD, nvar__SGSD

  REAL (pr) :: total_resolved_diss, total_sgs_diss , fracSGS_DISS
  REAL (pr) :: F__SGSD_RD__VolumeAverage,  F__SGSD_RD__Min, F__SGSD_RD__Max, F__SGSD_RD__Mean
  REAL (pr) :: Forcing_TimeScale_Min,Forcing_TimeScale_Max,Forcing_TimeScale_Mean       
  REAL (pr) :: Forcing_NormDenom_Min,Forcing_NormDenom_Max,Forcing_NormDenom_Mean     

  REAL (pr) :: Forcing_Norm_Denom__av, Forcing_Norm_Denom__av_Evol
  INTEGER   :: Number__av, Number__av_Evol

  REAL (pr) :: EpsSpatial_Forcing_TimeScale
  
  REAL (pr) :: Psgs_diss_goal__AdpEpsSp, Goal_Ratio, Forcing_Factor__AdpEpsSp, tmp1, my_nu
  
  REAL (pr), ALLOCATABLE   :: Array_Temp(:)  
#endif   
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!






  CALL parallel_init



  
  ! READ ARGUMENTS from stdin
  !----------- inputs
  CALL read_command_line_input

  CALL start_reader(TRIM(file_name))      ! initialize general file reader

  CALL input_string ('file_name',file_gen,'stop','  base file name')

  CALL input_integer ('begin_station_num',begin_station_num,'stop','  begin_station_num')

  CALL input_integer ('end_station_num',end_station_num,'stop','  end_station_num')

  CALL input_integer ('step_station_num',step_station_num,'stop','  step_station_num')

  !
  ! For now, let's read  SINGLE_FILE  from .inp file. 
  ! But eventually, it should be analyzed based on the file_name 
  !                 similar to   SUBROUTINE read_file_type_res2vis  [/post_process/visualization/code_aux.f90 ] 
  !
  SINGLE_FILE = .FALSE.
  CALL input_logical ('SINGLE_FILE',SINGLE_FILE,'stop')

  CALL input_real ('eps_post',eps_post,'stop','  post-processing threshold value')

  CALL input_real ('eps_sim',eps_sim,'stop','  simulation threshold value')

  CALL input_real ('viscosity',viscosity,'stop','  viscosity')  

!3/4/2011     MIN_POINTS_PER_BIN = 100
!3/4/2011     CALL input_integer ('MIN_POINTS_PER_BIN',MIN_POINTS_PER_BIN,'default', &
!3/4/2011          '  minimum # of points in wavenumber space per shell')

!3/4/2011     MAX_POINTS_PER_BIN = 50000
!3/4/2011     CALL input_integer ('MAX_POINTS_PER_BIN',MAX_POINTS_PER_BIN,'default', &
!3/4/2011          '  maximum # of points in wavenumber space per shell')

!3/4/2011     PERCENT_OF_POINTS_PER_BIN_TO_USE = 1.0_pr
!3/4/2011     CALL input_real ('PERCENT_OF_POINTS_PER_BIN',PERCENT_OF_POINTS_PER_BIN_TO_USE,'default', &
!3/4/2011          '  percentage of points in a shell if between MIN and MAX_POINTS_PER_BIN')
     
!3/4/2011     NUMBER_OF_BINS_READ = 2**12
!3/4/2011     CALL input_integer ('NUMBER_OF_BINS',NUMBER_OF_BINS_READ,'default', &
!3/4/2011          '  # of shells in wavenumber space')
 
  CALL stop_reader           ! clean reader's memory
  ! IC_restart is assumed to be known so it is not passed as a parameter into new
  ! endien independent format read_solution_aux...
  ! It is a parameter for read_solution for backward compatibility only.
  IC_restart = .FALSE.
  



!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! CUSTOM  POST  PROCESS  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! SCALES__AdpEpsSpatialEvolution  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
#ifdef userpostprocess_AdpEpsSpatialEvolution
     IF (par_rank.EQ.0) THEN
        outputfile = 'results/'//TRIM(file_gen)//'.'
        OPEN(22,FILE=outputfile(1:INDEX(outputfile,' ')-1)//'post.custom', STATUS='UNKNOWN',POSITION='APPEND')
       !WRITE(22,'("% time       <DISS_res>        <DISS_SGS>        fracSGS_DISS         <DISS_SGS/(DISS_res+DISS_SGS)>")')
        WRITE(22,'("% time          dt             <DISS_res>     <DISS_SGS>     fracSGS_DISS   <DISS_SGS/(DISS_res+DISS_SGS)         (Min,Max,Mean)( SGSD/(SGSD+RD)               (Min,Max,Mean) Forcing_TimeScale             (Min,Max,Mean)Forcing_NormDenom_Min          Forcing_Norm_Denom__av     EpsSpatial_Forcing_TimeScale")')
        CLOSE(22)
     ENDIF

    my_nu = 0.09_pr
    
    Forcing_Factor__AdpEpsSp = 400.0_pr
    Psgs_diss_goal__AdpEpsSp = 0.3_pr
    Goal_Ratio = Psgs_diss_goal__AdpEpsSp/(1-Psgs_diss_goal__AdpEpsSp)


    !IF( do_Adp_Eps_Spatial ) THEN      
       Forcing_Norm_Denom__av  = 0.0_pr
       Number__av              = 0
    !ENDIF
    !IF( do_Adp_Eps_Spatial_Evol ) THEN      
       Forcing_Norm_Denom__av_Evol  = 0.0_pr
       Number__av_Evol              = 0
    !ENDIF
     
#endif
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!



!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! CUSTOM  POST  PROCESS  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!      MisBalance Calculations      !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
#ifdef userpostprocess_parallel_misbalance

     IF(       ALLOCATED(MyRank_nwlt) )        DEALLOCATE (MyRank_nwlt)
     IF( .not. ALLOCATED(MyRank_nwlt) )        ALLOCATE   (MyRank_nwlt(0:par_size-1), STAT=Allocate_Status)

     IF (par_rank.EQ.0) THEN
        outputfile = 'results/'//TRIM(file_gen)//'.'
        OPEN(22,FILE=outputfile(1:INDEX(outputfile,' ')-1)//'post.custom', STATUS='UNKNOWN',POSITION='APPEND')
        WRITE(22,'("% station_num     time       par_size      Total_Nwlt                     rank_nwlt")')
        CLOSE(22)
     ENDIF

#endif
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!





  
  !
  !-- Loop through each data file 
  !
  DO station_num = begin_station_num, end_station_num, step_station_num  
! DO station_num = begin_station_num, end_station_num
  
!     PRINT *, 'Process station_num ',station_num
!     !-----------------------------------------------------------
!     WRITE(file_num_str,FILE_NUM_FMT) station_num
!     IC_filename = TRIM( 'results/'//file_gen(1:INDEX(file_gen//' ' , ' ')-1)//''//file_num_str//'.res' )
!     CALL read_solution( scl, IC_filename, &
!          DO_READ_ALL_VARS_TRUE, EXACT=.TRUE., IGNOREINIT = .TRUE. )




!AR 3/4/2011!  The following 17 lines are added by AR     ( Copied from  PROGRAM res2vis  [/post_process/visualization/code1.f90]  )
     IF (par_rank.EQ.0.AND.begin_station_num.NE.end_station_num)   PRINT *, 'Process station_num ',station_num
     
     
     !IF ( SINGLE_FILE ) THEN
       !IC_filename = TRIM(input_file_name)
       !tmp = TRIM( TRIM(output_file_name)//TRIM(par_rank_str) )
     !ELSE
       !WRITE(file_num_str,FILE_NUM_FMT) station_num
       !IC_filename = TRIM( TRIM(input_file_name)//file_num_str//TRIM(par_name)//'.res' )
       !tmp = TRIM( TRIM(input_file_name)//file_num_str//TRIM(par_rank_str) )
     !END IF
     


     WRITE(file_num_str,FILE_NUM_FMT) station_num
     IC_filename = TRIM( 'results/'//file_gen(1:INDEX(file_gen//' ' , ' ')-1)//''//file_num_str//'.p0'//'.res' )

#ifdef userpostprocess_DEBUG
     WRITE (*,'(A, 2I6, A, A, A, A, A)')   ' station/rank/file_num_str/file_gen/IC_filename = ', station_num, par_rank, '     ', file_num_str, '     ', TRIM(file_gen), '     ', TRIM(IC_filename)
#endif     
     
     verb_read = 0
     DO_APPEND = .TRUE.
     CALL read_solution( scl, &
                         IC_filename, &
                         DO_READ_ALL_VARS_TRUE, &
                         VERBLEVEL=verb_read, &    ! verbouse reading (0, 1, 2)
                         EXACT=.TRUE., &           ! get data without any assumptions
                                                   !    read_solution from a stand-alone application to read exactly what is inside .res file,
                                                   !    without making assumptions if it is a restart or initial condition.
                                                   !    In a stand alone application we don't care, we need exact data.
                         IGNOREINIT = .TRUE., &    ! ingore .inp values
                         APPEND=DO_APPEND, &       ! append data from multiple .res files
                         CLEAN_IC_DD=.FALSE. )     ! CLEAN_IC_D (default T) deallocate IC_par_proc_tree(). Normally we deallocate
                                                   !    and set a new repartitioning, except for called with
                                                   !    domain_meth=-1 res2vis and n2m

!AR 3/4/2011!  end
     




     n_var = SIZE(u_variable_names(:))



!*********************** READ # of point per bin *****************************
!3/4/2011     NUMBER_OF_BINS = MIN(NUMBER_OF_BINS_READ,MINVAL(nxyz)/2)
 !****************************************************************************

     !----------------------------------------------------------------!
     ! |------------------------------------------------------------| !
     ! | NOTE: All this initialization is required for stand-alone  | !
     ! |       wavelet transform and is the same as in res2vis.     | !
     ! |       It could be nice to create a separate subroutine     | !
     ! |       for that initialization sometimes.                   | !
     ! |------------------------------------------------------------| !
     !----------------------------------------------------------------!

     !-----------------------------------------------------------
     ! CALL read_input(...)
!#ifdef userpostprocess_AdpEpsSpatialEvolution
    
    IC_adapt_grid = .FALSE. 

     maxval_n_updt = maxval(n_updt)
     maxval_n_prdct = maxval(n_prdct)
     n_diff = maxval(n_prdct)
     DO i=1,dim
        xyzlimits(1,i) = xx(0,i)
        xyzlimits(2,i) = xx(nxyz(i),i)
        xyzzone(1,i) =-1.0E+15_pr ! to ensure tha tall points are in the zone
        xyzzone(2,i) = 1.0E+15_pr ! to ensure tha tall points are in the zone
     END DO

     Scale_Meth = 3
     Weights_Meth = 1
     j_tree_root = j_mn                  ! db_tree related variable (normally set in read_input)
     j_mn_init = j_mn                    ! to make sure j_mn will not change in init_DB
     j_mn_evol = j_mn
     j_lev_init = j_lev
     j_IC = j_mx
     j_filt = j_mx + 20         

     BNDzone = .FALSE.
     !-----------------------------------------------------------
     !CALL read_input_finalize()
     i_h_inp = 123456
     i_l_inp = 111111

    ! set n_assym_prdct/prdct_bnd/updt/updt_bnd
    ! (keep values from .res file)
    CALL set_max_assymetry ( .TRUE. )

    nbnd = 0
    ibnd=imp6(i_h_inp)
    ibc=imp6(i_l_inp)
    IF(prd(1) == 1) THEN
       ibc(1)=0
       ibc(2)=0
       grid(1)=0
    END IF
    IF(prd(2) == 1) THEN
       ibc(3)=0
       ibc(4)=0
       grid(2)=0
    END IF
    IF(prd(3) == 1) THEN
       ibc(5)=0
       ibc(6)=0
       grid(3)=0
    END IF

     !-----------------------------------------------------------
     ! CALL check_inputs(...)
     jd = 1
     IF(dim==2) THEN
        mxyz(3) = 1
        prd(3) = 1
        jd = 0 
     END IF
     !-----------------------------------------------------------
     !-----------------------------------------------------------
     !  CALL alloc_variable_mappings
     !  CALL map_veloc_timesteps
     ALLOCATE (n_var_adapt(1:n_var,0:1))
     ALLOCATE (n_var_interpolate(1:n_var,0:1))
     ALLOCATE (n_var_index(1:n_var))
     n_var_adapt(:,0) = .TRUE.
     n_var_interpolate(:,0) = .TRUE.
     DO i=1,n_var        
        n_var_index(i) = i        
     END DO
     
     !-----------------------------------------------------------!
     ! |-------------------------------------------------------| !
     ! | END OF WAVELET TRANSFORM VARIABLES INITIALIZATION     | !
     ! |-------------------------------------------------------| !
     !-----------------------------------------------------------!

     
#ifdef userpostprocess_DEBUG
     PRINT *,' MAXVAL(ABS(u(1:nwlt,1))) = ', MAXVAL(ABS(u(1:nwlt,1)))
     PRINT *,' MAXVAL(ABS(u(1:nwlt,2))) = ', MAXVAL(ABS(u(1:nwlt,2)))
     PRINT *,' MAXVAL(ABS(u(1:nwlt,3))) = ', MAXVAL(ABS(u(1:nwlt,3)))
#endif     


     ! initialize fft's and internal i_p array
!!!!!     CALL init_fft

     !
     !-- Set output file name. If the results were wavelet filtered above (eps_post > eps_sim(ulation) )
     ! this filter eps_post is added to the file name.
     !
     IF( eps_post == 0.0 ) THEN
        outputfile = 'results/'//TRIM(file_gen)//'.'
     ELSE
        WRITE (epsname, '(f10.6)') eps_post
        ! WRITE leaves initial blanks if the non-decimal part is not big
        !enough. So we first use INDEX to find where the real number begins
        llc = INDEX(epsname,' ',BACK=.TRUE.)+1; 
        outputfile = 'results/'//TRIM(file_gen)//'filt.eps'//epsname(llc:LEN(epsname))//'_st.'
     END IF
     
     ! print spectra and dissipation into a file
!!!!!     CALL print_spectra_a( TRIM(outputfile)//file_num_str//'.spectra' , the_tke, the_tdiss, &
!!!!!          enrg_total, diss_total, e1d_total, e1d_num )
     
     !write log file
!!!!!     OPEN(1,FILE=outputfile(1:INDEX(outputfile,' ')-1)//'post.turbstats',&
!!!!!          STATUS='UNKNOWN',POSITION='APPEND')
!!!!!     WRITE(1,'("% time tke total_diss u_scaled_eps v_scaled_eps w_scaled_eps")')
!!!!!     WRITE(1,'( 6(E12.5,1X) )') t, the_tke, the_tdiss ,scl(1)*eps_post,scl(2)*eps_post,scl(3)*eps_post
!!!!!     CLOSE(1)
!!!!!     WRITE(*,'("% time tke total_diss u_scaled_eps v_scaled_eps w_scaled_eps")')
!!!!!     WRITE(*,'( 6(E12.5,1X) )') t, the_tke, the_tdiss ,scl(1)*eps_post,scl(2)*eps_post,scl(3)*eps_post

     !-------------------------------------------------------------------------------------------------
     !--  inverse the database
     !-------------------------------------------------------------------------------------------------     

     CALL pre_init_DB
     CALL init_DB( nwlt_old, nwlt, nwltj_old, nwltj, new_grid,&
                  j_lev_old, j_lev, j_mn, j_mx , MAXVAL(nxyz) , nxyz, .TRUE. )
     
     CALL set_weights()

     ! this is conditioned by non-updated init_DB subroutine for db_lines,
     ! as soon as it is updated, inverse transform to be removed for all the databases
     !IF (TYPE_DB.EQ.DB_TYPE_LINES) &
     !     CALL c_wlt_trns_mask (u, u, n_var, n_var_interpolate(:,0), n_var_adapt(:,0), .TRUE., .FALSE.,&
     !     nwlt, nwlt, j_lev, j_lev, HIGH_ORDER, WLT_TRNS_INV, DO_UPDATE_DB_FROM_U, DO_UPDATE_U_FROM_DB)
     !-------------------------------------------------------------------------------------------------
     !--  inverse wavelet transform things
     !-------------------------------------------------------------------------------------------------
     
!#endif









!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! CUSTOM  POST  PROCESS  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! SCALES__AdpEpsSpatialEvolution  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
#ifdef userpostprocess_AdpEpsSpatialEvolution


     IF(       ALLOCATED(Array_Temp) )         DEALLOCATE (Array_Temp)
     IF( .not. ALLOCATED(Array_Temp) )         ALLOCATE   (Array_Temp(1:nwlt), STAT=Allocate_Status)



     nvar__RD   = 8 !9
     nvar__SGSD = 7 !8



     total_resolved_diss  = SUM( dA* u(:,nvar__RD) )   / sumdA_global
     total_sgs_diss       = SUM( dA* u(:,nvar__SGSD) ) / sumdA_global 
     fracSGS_DISS         = total_sgs_diss/(total_resolved_diss+total_sgs_diss) 



     tmp1 = MAXVAL(abs( u(:,nvar__SGSD) - u(:,nvar__RD)*Goal_Ratio ))
     !CALL parallel_global_sum( REALMAXVAL=tmp1 )
     Forcing_Norm_Denom__av = ( Forcing_Norm_Denom__av * REAL(Number__av,pr) + tmp1 )  /  REAL(Number__av+1,pr)
     Number__av = Number__av + 1



     EpsSpatial_Forcing_TimeScale = SUM( dA * sqrt(u(:,nvar__RD) / my_nu)) 
    !CALL parallel_global_sum( REAL=EpsSpatial_Forcing_TimeScale)
     EpsSpatial_Forcing_TimeScale = EpsSpatial_Forcing_TimeScale / ( SQRT(2.0_pr) * sumdA_global )
     
     
     
     Array_Temp(:) = ( u(:,nvar__SGSD) - u(:,nvar__RD)*Goal_Ratio ) / EpsSpatial_Forcing_TimeScale
     Forcing_TimeScale_Min  = MINVAL( Array_Temp )
     Forcing_TimeScale_Max  = MAXVAL( Array_Temp )
     Forcing_TimeScale_Mean = SUM(    Array_Temp ) / nwlt


     
     Array_Temp(:) = ( u(:,nvar__SGSD) - u(:,nvar__RD)*Goal_Ratio ) * Forcing_Factor__AdpEpsSp /  Forcing_Norm_Denom__av
     Forcing_NormDenom_Min  = MINVAL( Array_Temp )
     Forcing_NormDenom_Max  = MAXVAL( Array_Temp )
     Forcing_NormDenom_Mean = SUM(    Array_Temp ) / nwlt



     Array_Temp(:) = u(:,nvar__SGSD) / ( u(:,nvar__SGSD) + u(:,nvar__RD) )

     F__SGSD_RD__VolumeAverage = SUM( dA(:)* Array_Temp(:) ) / sumdA_global
     F__SGSD_RD__Min   = MINVAL( Array_Temp(:) )
     F__SGSD_RD__Max   = MAXVAL( Array_Temp(:) )
     F__SGSD_RD__Mean  = SUM(    Array_Temp(:) ) / nwlt
    !CALL parallel_global_sum( REALMINVAL=F__SGSD_RD__Min )
    !CALL parallel_global_sum( REALMAXVAL=F__SGSD_RD__Max )
    !CALL parallel_global_sum( REAL=F__SGSD_RD__Mean )


     !write log file  for   CUSTOM  POST  PROCESS
     IF (par_rank.EQ.0) THEN
        OPEN(22,FILE=outputfile(1:INDEX(outputfile,' ')-1)//'post.custom', STATUS='UNKNOWN',POSITION='APPEND')
       !WRITE(22,'("% time          dt             <DISS_res>     <DISS_SGS>     fracSGS_DISS   <DISS_SGS/(DISS_res+DISS_SGS)         (Min,Max,Mean)( SGSD/(SGSD+RD)               (Min,Max,Mean) Forcing_TimeScale             (Min,Max,Mean)Forcing_NormDenom_Min          Forcing_Norm_Denom__av     EpsSpatial_Forcing_TimeScale")')
        WRITE(22,'( 6(E12.5,3X) 1(23X) 10(E12.5,3X) 1(12X) 1(E12.5,3X))') t, dt, total_resolved_diss, total_sgs_diss , fracSGS_DISS,  F__SGSD_RD__VolumeAverage,             F__SGSD_RD__Min, F__SGSD_RD__Max, F__SGSD_RD__Mean,      Forcing_TimeScale_Min,Forcing_TimeScale_Max,Forcing_TimeScale_Mean,      Forcing_NormDenom_Min,Forcing_NormDenom_Max,Forcing_NormDenom_Mean,     Forcing_Norm_Denom__av,   EpsSpatial_Forcing_TimeScale
        CLOSE(22)
       !WRITE(*, '("% time       <DISS_res>        <DISS_SGS>        fracSGS_DISS         <DISS_SGS/(DISS_res+DISS_SGS)>")')
       !WRITE(*, '( 5(E12.5,3X) )') t, total_resolved_diss, total_sgs_diss , fracSGS_DISS, F__SGSD_RD
     ENDIF

#ifdef userpostprocess_DEBUG
     WRITE (*,'(A, I6, A, I12)')          ' par_rank=', par_rank, ' MAXVAL(ABS(u_out(1:nwlt,1))) = ', MAXVAL(ABS(u(1:nwlt,1)))
     WRITE (*,'(A, I6, A, I12)')          ' par_rank=', par_rank, ' MAXVAL(ABS(u_out(1:nwlt,2))) = ', MAXVAL(ABS(u(1:nwlt,2)))
     WRITE (*,'(A, I6, A, I12)')          ' par_rank=', par_rank, ' MAXVAL(ABS(u_out(1:nwlt,3))) = ', MAXVAL(ABS(u(1:nwlt,3)))
#endif


#endif
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!












!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! CUSTOM  POST  PROCESS  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!      MisBalance Calculations      !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
#ifdef userpostprocess_parallel_misbalance

     !send_check(:) = 0
     MyRank_nwlt(par_rank) = nwlt

     !CALL MPI_ALLTOALL( nwlt, 1, MPI_INTEGER, MyRank_nwlt(par_rank), 1, MPI_INTEGER, MPI_COMM_WORLD, ierror )
     CALL parallel_gather_rank(MyRank_nwlt(:))
     
#ifdef userpostprocess_DEBUG
     WRITE (*,'(A, I6, A, 10I6)')  ' par_rank=', par_rank, ' MyRank_nwlt(:) = ', MyRank_nwlt(:)
#endif
     
     
     !DO i = 1, par_size-1  
     !   IF (par_rank.EQ.0.AND.begin_station_num.NE.end_station_num) 


     !write log file  for   CUSTOM  POST  PROCESS
     IF (par_rank.EQ.0) THEN
        OPEN(22,FILE=outputfile(1:INDEX(outputfile,' ')-1)//'post.custom', STATUS='UNKNOWN',POSITION='APPEND')
       !WRITE(22,'("% station_num     time       par_size      Total_Nwlt                     rank_nwlt")')
        WRITE(22,'( 1(I9,3X)  1(E12.5,3X) 1(I6,3X) 1(I12,3X) 1(23X) 10(I12,3X))') station_num, t, par_size, SUM(MyRank_nwlt(:)), MyRank_nwlt
        CLOSE(22)
     ENDIF

#endif
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!







     !calculate turbulent stats
!!!!!     CALL trb_stats( viscosity, scl, eps_post, eps_sim, &
!!!!!          outputfile(1:INDEX(outputfile,' ')-1)//'turbstats', .TRUE., &
!!!!!          enrg_total, diss_total, e1d_total, e1d_num )

     ! allocated in MAIN
     IF (ALLOCATED (n_var_adapt))        DEALLOCATE (n_var_adapt)
     IF (ALLOCATED (n_var_interpolate))  DEALLOCATE (n_var_interpolate)
     IF (ALLOCATED (n_var_index))        DEALLOCATE (n_var_index)
     ! allocated in READ_SOLUTION()
     IF (ALLOCATED (u_variable_names) )  DEALLOCATE (u_variable_names)
     IF (ALLOCATED (xx))                 DEALLOCATE (xx)
     IF (ALLOCATED (indx_DB))            DEALLOCATE (indx_DB)
     IF (ALLOCATED (u))                  DEALLOCATE (u)
     IF (ASSOCIATED(scl))                DEALLOCATE (scl)
     IF (ASSOCIATED(scl_global))         DEALLOCATE(scl_global)
     IF (ALLOCATED (n_var_wlt_fmly))     DEALLOCATE(n_var_wlt_fmly)

     CALL DB_finalize
     ! clean allocated in read_solution()
     CALL read_solution_free (scl)
     
  END DO !loop through stations
END PROGRAM wlt3d_user_post_process



