function [u,x,y,z,nx,ny,nz,dim,time,periodic] = inter3d(file,eps,bounds,plot_component,j_range,station_num,spm)
     global POSTPROCESS_DIR
     
     OUTPUT_MODE = 1;
     if ispc
        WLT_INTERPOLATION = ['..' slsh 'MVS_P2012' slsh 'Debug' slsh 'wlt_inter.exe'];
     else
        WLT_INTERPOLATION = 'c_wlt_inter.out';
     end
     if size(bounds,2) ~= 6
       bounds = [-10.^5 10.^5 -10.^5 10.^5  -10.^5 10.^5];
     end
     
     do_par_read = 0; % default - read single .res file only
     if nargin == 7  % spm present
       if strcmpi(spm,'p')
	 do_par_read = 1; %     - read parallel .res files
       end
     end
     
     u=0; x=0; y=0; z=0;
     ffl=  'inp.dat';
     fid = fopen(ffl,'w')
     fprintf(fid,'%s\n',file);
     
     fprintf(fid,'%12.5e %12.5e %12.5e %12.5e %12.5e %12.5e %12.5e %7i %7i %7i %7i %7i %7i\n',...
	     eps,bounds,j_range(1), j_range(2),station_num,station_num,OUTPUT_MODE,do_par_read);
     fprintf(fid,'%s\n',plot_component);
     status = fclose(fid);
     
     %eval(['!' POSTPROCESS_DIR slsh WLT_INTERPOLATION])
     inter3d_status_run=system([POSTPROCESS_DIR slsh WLT_INTERPOLATION]);
     if inter3d_status_run == 0 
        disp('inter3d: wavelet interpolation is completed');
     else
        disp('WARNING inter3d: wavelet interpolation is not completed');
     end
     
     if ispc
       delete('inp.dat')
     else
       %!\rm inp.dat
       inter3d_status_delinp=system('rm inp.dat');
       if inter3d_status_delinp == 0 
            disp('inter3d: file inp.dat is deleted');
       else
            disp('WARNING inter3d: file inp.dat is not deleted');
       end
     end
     %------------------- reading unformated file
     % 
     ffl=  'wrk.dat';
     fid = fopen(ffl,'rb','n'); % Original line
     dim = fread(fid,1,'int32');  %read dim (plus begin/end record)
     arr = fread(fid,3,'int32'); %read nx,ny,nz (plus begin/end record)
     nx=arr(1);
     ny=arr(2);
     if dim == 3
         nz=arr(3);
     else
         nz = 1;
     end
     disp(['nxyz=[' num2str(nx) ',' num2str(ny) ',' num2str(nz) ']']);
     %read x 
     x = fread(fid,nx,'double');
     
     %read y  
     y = fread(fid,ny,'double');
     
     if( dim == 3 )
       %read z 
       z = fread(fid,nz,'double');
     else
       nz = 1;    
     end
     
     %read u (plus begin/end record)
     arr = fread(fid,nx*ny*nz,'double');
     u=reshape(arr,nx,ny,nz);
     for i = 1:nz
       v(:,:,i)=transpose(u(:,:,i));
     end
     u=v;
     
     % read time  
     time = fread(fid,1,'double');
     
     % read periodicity  
     prd = fread(fid,3,'int32');
     periodic = any(prd(1:dim));
     
     status = fclose(fid);
     fid=fopen('vorticity3','w');
     fprintf(fid,'%f\n',u);
     status=fclose(fid);

     clear arr v

if ispc
    delete('wrk.dat');
else
    %!\rm wrk.dat
    inter3d_status_delwrk=system('rm wrk.dat');
    if inter3d_status_delwrk == 0 
        disp('inter3d: file wrk.dat is deleted');
    else
        disp('WARNING inter3d: file wrk.dat is not deleted');
    end
end
