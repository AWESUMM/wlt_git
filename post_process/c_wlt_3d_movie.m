function M = c_wlt_3d_movie(file,j_range,eps,bounds,fig_type,plot_type,obstacle,az,el,slev,plot_comp,station_num,fignum)

it = station_num

%Same aruments as c_wlt_3d except it becomes an array of 
% length two 
if( size(it,2) ~= 2 ) 
    error(' Error, it must be array of length 2, [min iteration, max iteration]');
end
 
% set frames per second for play back
fps = 4

mov = avifile([ file   'frames.' num2str(it(1),'%4.4i') '.'  ...
		num2str(it(1), ' %4.4i') '.avi'],'FPS',fps)
    
for i = it(1,1):it(1,2)
    c_wlt_3d(file,j_range,eps,bounds,fig_type,plot_type,obstacle, ...
	     az,el,slev,plot_comp,i,fignum)
     eval(['print -depsc2  '  file   'iso.' num2str(slev,'%8.2f')  '.frame.' num2str(i,'%4.4i')  '.eps']);
     F = getframe(gcf);
     mov = addframe(mov,F);
 
end
mov = close(mov);
