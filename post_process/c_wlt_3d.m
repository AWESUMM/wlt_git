% plot output from poisson_3d.out
%
%
% args:
%    file       output file namestm
%    j_range    = [j_min j_max] (range of levels to plot )
%    eps        plot only valuse > eps (if<0  percent of wlt coeff. range)
%    bounds     = [xmin xmax ymin ymax zmin zmax] plot within bounds
%    fig_type   'coeff' = plot wlt coefficents, 'grid' = plot grid, 'solution' = plot solution
%    plot_type  'surf' = surface plots, 'contour' = contour plots, 'isosurface' = plot isosurface
%               'slice' = sliceomatic
%    obstacle   'none' = no obstacle plot, 'sphere', 'cylinder' 
%    az, el     azimuth and elevation for the direction to look at the plot
%    slev       level of isosurface choosed (in % of maximum value)
%    plot_comp  ux, uy, uz, wx, wy, wz, magu, magw
%    station_num         station # (ie. the number of the output file
%    fignum     figure number to use for output
%   x0 - coordinate and the slise half thickness, e.g.  x0=[x y z del];
%   n0 - array of vectors normal to the slice planes, e.g. n0=[[0 1 1]' [0 1 -1]']; 
%   spm:        's' - read single file 'results/FILE.STATION_NUM.res'
%               'p' - read parallel files: 'results/FILE.STATION_NUM.p0.res', etc.
%   curv        0 or 1 - use curvilinear coordinates
%
function pl = c_wlt_3d(file,j_range,eps,bounds,fig_type,plot_type, ...
		       obstacle,az,el,slev,plot_comp,station_num,fignum,x0,n0, spmarker, curv)
global RYB
global Umn
global Zmn Zmx
global xh yh zh uh % for UQ use

% 'it looks like we do not need it to be global'
% global mrkcl mrksz                                % marker color, marker size

color_scheme = 0;                                  % 0 standard, 1 - for vorticity


% if mrkcl == []                                    % set marker color
%    mrkcl = 'r'
% end
%stb if mrksz == []                                    % set marker size
%   mrksz = 0.5
% end

mrkcl = 'k';
mrksz = 1.0;

%if nargin == 16      % s/p marker present
%  spmarker = spm;
%elseif nargin == 15  % default single processor file
%  spmarker = 's';
%end

curvilinear = 0      % default no curvilinear coordinates
if nargin == 17
  curvilinear = 1
end



if size(j_range,2) == 1                           % set j_min and j_max
  j_min=j_range;
                                    j_max=j_range;
elseif size(j_range,2) == 2  
  j_min=j_range(1);
  j_max=j_range(2);
else
  j_min=0;
  j_max=20;
end


if ( (~strcmp(fig_type, 'coeff')) & (~strcmp(fig_type, 'grid' )) & (~strcmp(fig_type, 'solution')) )
  fprintf(1,'Unknown fig_type: %s \n',fig_type);  
  return
end


if ( strcmp(plot_comp , 'ux') )                   % set plot_component,
  plot_component = 'Velocity_u_@t';               %  which is the name
elseif ( strcmp(plot_comp , 'uy' ))               %  of the actual variable
  plot_component = 'Velocity_v_@t';               %  from .res file
elseif ( strcmp(plot_comp , 'uz') )
  plot_component = 'Velocity_w_@t';
elseif ( strcmp(plot_comp , 'wx') )
  plot_component = 'Vorticity_u';
elseif ( strcmp(plot_comp , 'wy' ))
  plot_component = 'Vorticity_v';
elseif ( strcmp(plot_comp , 'wz') )
  plot_component = 'Vorticity_w';
elseif ( strcmp(plot_comp , 'magu' ))
  if( ~strcmp(fig_type , 'solution') )
    error('Error: ''magu'' only supported for fig_type == ''solution'' ');
  end
  plot_component = 'magu';
elseif ( strcmp(plot_comp , 'magw') )
  if( ~strcmp(fig_type , 'solution') )
    error('Error: ''magw'' only supported for fig_type == ''solution'' ');
  end
  plot_component = 'magw';
else                                              % default behavior
  plot_component =  plot_comp;
  fprintf(1,'Opening variable: %s \n',plot_comp);  
end


fprintf(1,'  range of levels to plot: %d to %d  \n', j_min , j_max );
fprintf(1,'  eps = %f          (plot only values > eps)(if<0  percent of wlt coeff. range)\n', eps );
fprintf(1,'  bounds = [ %f %f  %f %f %f %f ]\n', bounds );
fprintf(1,'  fig_type = %s          (''coeff'' or ''grid'' or ''solution'')\n',fig_type);
fprintf(1,'  plot_type = %s          (''surf'' or ''contour'' )\n',plot_type);
fprintf(1,'  component plot = %s\n',plot_component);
fprintf(1,'  station # = %d\n',station_num);
fprintf(1,'  figure : %d         (number to use for output)\n', fignum );





%
%------- Coefficients ---------------------
%
% required variables:
%  xstm, ystm, cstm, stm_indx, dim, nz
%  xmin, xmax, ymin, ymax, zmin, zmax, time
if( strcmp(fig_type , 'coeff') )
  
  [xstm,ystm,cstm,stm_indx,dim,nz,xmin, xmax, ymin, ymax, zmin, zmax, time, np] ...
    = res2matlab(file,eps,bounds,plot_component,j_min,j_max,station_num,x0,n0,spmarker,curvilinear);
  
  
  fprintf('time = %12.8f\n', time); % 6/7/07 addition
  
  %figure(fignum)
  if(dim == 3 )
    plotdim = 3;
    for zslice = 1:nz
      [aaa] = find(stm_indx(:,plotdim) == zslice )
      if( length(aaa) > 0 )
	
	stem3(xstm(aaa),ystm(aaa),abs(cstm(aaa))); 
	set(gca,'xlim',[xmin,xmax],'ylim',[ymin,ymax]);
	title('Wavelet Coefficients');
	%xlabel('x');
	%ylabel('y');
	fprintf(' zSlice = %d\n', zslice );
	inp = input(' Show next active Zslice? y/n [y]: ','s');
	if strcmpi(inp,'n')
	  break
	end
      end
    end
  else %dim == 2
    clf
    stem3(xstm,ystm,abs(cstm),'fill'); % Original line
    set(gca,'xlim',[xmin,xmax],'ylim',[ymin,ymax]);
    title('Wavelet Coefficients')
    %xlabel('x');
    %ylabel('y');
  end
end %if( strcmp(fig_type , 'coeff') )




%
%------------ Grid ---------------------------------------------------------
%
% required variables:
%  xstm, ystm, zstm, stm_indx, dim, nz
%  xmin, xmax, ymin, ymax, zmin, zmax
if( strcmp(fig_type , 'grid') )
  
  [xstm,ystm,zstm,cstm,stm_indx,dim,nz,xmin,xmax,ymin,ymax,zmin,zmax,time,np] ...
    = res2matlab(file,eps,bounds,plot_component,j_min,j_max,station_num,x0,n0,spmarker,curvilinear);

  fprintf('time = %12.8f\n', time);     % 6/7/07 addition
  
  %clf  
  if(strcmp(plot_type , 'isosurface'))
    if( dim == 3 )
      plot3(xstm,ystm,zstm,'o','MarkerEdgeColor',mrkcl,'MarkerFaceColor',mrkcl,'Markersize',mrksz)
      set(gca,'xlim',[xmin,xmax],'ylim',[ymin,ymax],'zlim',[zmin,zmax]); 
      set(gca,'PlotBoxAspectRatio',[(xmax-xmin),(ymax-ymin),(zmax-zmin)])
      grid on
      view(az,el);
      %xlabel('x');
      %ylabel('y');
      %zlabel('z');
    else
      plot(xstm,ystm,'o','MarkerEdgeColor',mrkcl,'MarkerFaceColor',mrkcl,'Markersize',mrksz);
      set(gca,'xlim',[xmin,xmax],'ylim',[ymin,ymax]);
      set(gca,'PlotBoxAspectRatio',[(xmax-xmin),(ymax-ymin),1])
      %xlabel('x');
      %ylabel('y');
    end
    
  elseif(strcmp(plot_type , 'surf'))
    plotdim = 3;
    if( dim == 3 )
      [aaa] = find(stm_indx(:,plotdim) >= nz/2 )
      pause
      plot3(xstm(aaa),ystm(aaa),zstm(aaa),'o','Markersize',mrksz,'MarkerEdgeColor',mrkcl,'MarkerEdgeColor',mrkcl)
      set(gca,'xlim',[xmin,xmax],'ylim',[ymin,ymax],'zlim',[0,zmax]);
      set(gca,'PlotBoxAspectRatio',[(xmax-xmin),(ymax-ymin),zmax])
      %xlabel('x');
      %ylabel('y');
      %zlabel('z');
      grid on
    else
      plot(xstm,ystm,'o','MarkerEdgeColor',mrkcl,'MarkerFaceColor',mrkcl,'Markersize',mrksz);
      xmin,xmax,ymin,ymax
      set(gca,'xlim',[xmin,xmax],'ylim',[ymin,ymax]);
      set(gca,'PlotBoxAspectRatio',[(xmax-xmin),(ymax-ymin),1])
      %xlabel('x');
      %ylabel('y');
    end
    view(az,el);
    
    
  elseif( strcmp(plot_type , 'contour'))
    if dim == 3
      plotdim = 3;
      for zslice = 1:nz
	[aaa] = find(stm_indx(:,plotdim) == zslice );
  if (isOctave)
    cla;
  else
    set(cla,'nextplot','replace')
  end
	clf;
	plot(xstm(aaa),ystm(aaa),'o','Markersize',mrksz,'MarkerEdgeColor',mrkcl,'MarkerFaceColor',mrkcl);
	set(gca,'xlim',[xmin,xmax],'ylim',[ymin,ymax]);
    set(gca,'PlotBoxAspectRatio',[(xmax-xmin),(ymax-ymin),1])

	axis('equal');
	axis([xmin xmax ymin ymax]);
	%              view(0,90);
	%              shading interp;
	view(az,el);
  if (isOctave)
    set(gcf,'renderer','opengl');
   else
    set(gcf,'renderer','zbuffer');
  end
	if( nz ~= 1 )
	  title(['Grid, zSlice =' , num2str(zslice) ])
	  fprintf(' Grid = %d\n', zslice );            
	else
	  %title(['Solution' ])
	end
	%xlabel('x')
	%ylabel('y')
	set(gca,'clim',[zmin,zmax])
	if( nz ~= 1 ) 
	  inp = input(' Show next active Zslice? y/n [y]: ','s');
	  if strcmpi(inp,'n')
	    break
	  end
	end
      end
    else
      plot(xstm,ystm,'o','MarkerEdgeColor',mrkcl,'MarkerFaceColor',mrkcl,'Markersize',mrksz);
      set(gca,'xlim',[xmin,xmax],'ylim',[ymin,ymax]);
      set(gca,'PlotBoxAspectRatio',[(xmax-xmin),(ymax-ymin),1])
      %xlabel('x');
      %ylabel('y');
    end
  end  
  title('Grid')
  
end %if( strcmp(fig_type , 'grid') )





%
%--------------- Solution---------------------------------------------------
%
% required variables:
%  u,x,y,z,nx,ny,nz,dim
if( strcmp(fig_type , 'solution') )

  [u,x,y,z,nx,ny,nz,dim,time] ...
      =  inter3d(file,eps,bounds,plot_component,[j_min,j_max],station_num,spmarker);
 
  xh=x;
  yh=y;
  zh=z;
  uh=u;
 
  if plot_component == 'U'
      if ~isempty(Umn)
         u = u + Umn;
      end
  end
  
  fprintf('time = %12.8f\n', time);     % 6/7/07 addition
  
  if ( dim == 2 )&((strcmp(plot_type,'isosurface'))|(strcmp(plot_type,'slice')))
    plot_type = 'surf';
  end
  
  fprintf(1,'size(u) =');
  disp(size(u));
  fprintf(1,'nz = %d\n',nz)
  
  
  
  if size(bounds,2) == 6
    xmin=bounds(1);
    xmax=bounds(2);
    ymin=bounds(3);
    ymax=bounds(4);
    zmin=bounds(5);
    zmax=bounds(6);
  else
    xmin=min(x);
    xmax=max(x);
    ymin=min(y);
    ymax=max(y);
    if(dim == 3 )
      zmin=min(z);
      zmax=max(z);
    end
  end 
  
  
  if( strcmp(plot_type  , 'surf'))
    if( nz ~= 1 ) % if nz ~=1 we assume dim == 3
      zmin=min(min(min(u)));
      zmax=max(max(max(u)));
    else
      zmin=min(min(u));
      zmax=max(max(u));
    end
    if( zmin == zmax ) 
      zmin = zmin -1;
      zmax= zmax +1;
    end
    zmin=min(min(min(u)));
    zmax=max(max(max(u)));
 
    
    if ~isempty(Zmn)
         zmin=Zmn;
    end
    if ~isempty(Zmx)
         zmax=Zmx;
    end

    for zslice = 1:nz
      if (isOctave)
        cla;
      else
        set(cla,'nextplot','replace');
      end
      clf;
      l1=0.1;b1=0.1;w1=0.75;h1=0.75;
      ax1 = axes('position',[l1,b1,w1,h1]);
      surface(x,y,u(:,:,zslice));
      if exist('RYB') & RYB
          zmin=-max([abs(zmin),abs(zmax)]);
          zmax=-zmin;
          ryb_colormap
      end
      shading interp;
      view(az,el);
      % for vorticity to look nice use :    vor_pal;
      %  wmax = max(max(abs(u(:,:,zslice))))
      zmin
      zmax
      % caxis will not accept equal arguments, therefore:
      if zmin == zmax
	zmax = zmax + zmax*1e-5;
	if zmax == 0
	  zmax = 1;
	end
      end
      caxis([zmin zmax]);
      %ax2 = axes('position',[l1+w1+0.02,b1+0.02,0.03,h1*0.92]);
      %set(ax2,'fontsize',16);
      if (isOctave)
        set(gcf,'renderer','opengl')
      else
        set(gcf,'renderer','zbuffer');
      end
      set(gca,'xlim',[xmin,xmax],'ylim',[ymin,ymax],'zlim',[zmin,zmax]);
      set(gca,'PlotBoxAspectRatio',[(xmax-xmin),(ymax-ymin),(zmax-zmin)])
      %set(gca,'PlotBoxAspectRatio',[1 1 1])
      %set(gca,'View',[40 30])
      %colorbar(ax2,'peer',ax1);
      mycolorbar('vert');
      zlabel('solution')
      if( nz ~= 1 )
	title(['Solution, zSlice =' , num2str(zslice) ])
	fprintf(' zSlice = %d\n', zslice );            
      else
	title(['Solution' ])
      end
      %xlabel('x')
      %ylabel('y')
      set(gca,'clim',[zmin,zmax])
      if( nz ~= 1 ) 
	inp = input(' Show next active Zslice? y/n [y]: ','s');
	if strcmpi(inp,'n')
	  break
	end
      end
    end
    
  elseif( strcmp(plot_type , 'contour'))
    for zslice = 1:nz
      if (isOctave)
        cla;
      else
        set(cla,'nextplot','replace');
      end
      clf;
      l1=0.1;b1=0.1;w1=0.75;h1=0.75;
      ax1 = axes('position',[l1,b1,w1,h1]);
      set(ax1,'fontsize',16); 
      contourf(x,y,u(:,:,zslice),100,'Linecolor','none')
      if color_scheme == 1 
	vor_pal;
	wmax = max(max(abs(u(:,:,zslice))))
	wmin = -wmax;
      else
	wmin = min(min(u(:,:,zslice)))
	wmax = max(max(u(:,:,zslice)))
      end
      axis([xmin xmax ymin ymax]);
      caxis([wmin wmax]);
      view(0,90);
      ax2 = axes('position',[l1+w1+0.02,b1+0.02,0.03,h1*0.92]);
      set(ax2,'fontsize',16);
      colorbar(ax2,'peer',ax1);
      if (isOctave)
        set(gcf,'renderer','opengl');
      else
        set(gcf,'renderer','zbuffer');
      end
      if( nz ~= 1 )
	title(['Solution, zSlice =' , num2str(zslice) ])
	fprintf(' zSlice = %d\n', zslice );            
      else
	title(['Solution' ])
      end
      %xlabel('x')
      %ylabel('y')
      set(gca,'clim',[wmin,wmax])
      if( nz ~= 1 ) 
	inp = input(' Show next active Zslice? y/n [y]: ','s');
	if strcmpi(inp,'n')
	  break
	end
      end
    end
    
  elseif(  strcmp(plot_type , 'isosurface'))
    clf;
    % Smooth data
    ws(:,:,:) = smooth3(u(:,:,:));
    clear w;
    
    % Set up coordinate grid
    wmin = min(min(min(ws(:,:,:))));
    wmax = max(max(max(ws(:,:,:))));
    wmin=0;
    wmax=1;
    s = [0 0 0];
    s(1) = size(ws,1);
    s(2) = size(ws,2);
    s(3) = size(ws,3);
    x=linspace(xmin,xmax,s(1));
    y=linspace(ymin,ymax,s(2));
    z=linspace(zmin,zmax,s(3));
    [x,y,z]=meshgrid(x,y,z);
    slev = wmin+slev./100.*(wmax-wmin)
    %slev = [0.15 0.75]
    
    
    % Plot the obstacle
    % -----------------
    
    if( strcmp(obstacle  , 'sphere'))
      [x1,y1,z1] = sphere(20);
      x1=0.5.*x1; y1=0.5.*y1; z1=0.5.*z1;
      surf(x1,y1,z1,'FaceColor',[0.8 0.8 0.8], 'EdgeColor', 'none')
    elseif( strcmp(obstacle  , 'cylinder'))
      [x1,y1,z1] = cylinder;
      x1=0.5.*x1; y1=0.5.*y1; z1=(xmax-xmin)*(z1-0.5);
      surf(x1,y1,z1,'FaceColor',[0.8 0.8 0.8], 'EdgeColor', 'none')
    elseif( strcmp(obstacle  , 'none'))
      fprintf(1,'None obstacle plot \n');
    else
      fprintf(1,'Unknown obstacle: %s \n',plot_type);  
      return
    end
    
    
    % Plot isosurfaces
    opengl autoselect
    %hold on
    for i=1:length(slev)
      slev
      hold on
      hiso1 = patch(isosurface(x,y,z,ws(:,:,:), slev(i),ws(:,:,:)),'FaceColor', 'interp','FaceAlpha', 0.5, 'EdgeColor', 'none');
      %hiso2 = patch(isosurface(x,y,z,ws(:,:,:),-slev(i),ws(:,:,:)),'FaceColor', 'interp','FaceAlpha', 0.5, 'EdgeColor', 'none');
      set(hiso1,'SpecularExponent',15);
      %set(hiso2,'SpecularExponent',15);
      isonormals(x,y,z,ws(:,:,:),hiso1);
      %isonormals(x,y,z,ws(:,:,:),hiso2);
    end
    hold off
    view(az,el);
    
    if( dim == 2 )
      vor_pal;
      caxis([-wmax,wmax]);
    else
      colormap jet
      caxis([0 , wmax]);
    end
    colormap jet
    caxis([wmin , wmax]);
    
    colorbar('vert');
    daspect([1,1,1]);
    lighting phong;
    [ylight,xlight,zlight] = sph2cart(az,el,1);
    light('Position',[xlight+1  -ylight+1  zlight+1],'Style','infinite');
    light('Position',[xlight-1  -ylight-1  zlight+1],'Style','infinite');
    alpha(0.5) 
    alphamap('increase',.3) 
    axis([xmin xmax ymin ymax zmin zmax]);
    %xlabel('X');ylabel('Y');zlabel('Z');
    set(gcf,'Renderer','OpenGL');
    %title('Solution')   
    %xlabel('x')
    %ylabel('y')
    grid on;
    set(gca,'clim',[wmin,wmax])
  elseif(  strcmp(plot_type , 'slice'))
    figure(fignum)
    clf;
    sliceomatic(u)
                                        
  else
    fprintf(1,'Unknown plot_type: %s \n',plot_type);  
    return
  end
  
  hold off
  
end %if( strcmp(fig_type , 'solution') )
