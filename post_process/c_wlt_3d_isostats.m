% plot output from poisson_3d.out
%
%
% args
%    file       output file name
%    form       (0 == read unformated results file, 1 == read formated file )
%    j_range    = [j_min j_max] (range of levels to plot )
%    eps        plot only valuse > eps (if<0  percent of wlt coeff. range)
%    bounds     = [xmin xmax ymin ymax zmin zmax] plot within bounds
%    fig_type   'coeff' = plot wlt coefficents, 'grid' = plot grid, 'solution' = plot solution
%    plot_type  'surf' = surface plots, 'contour' = contour plots, 'isosurface' = plot isosurface
%    mag_type   'magnitude' = plot magnitude, 'direction' = plot the value expected in the direction expected
%    obstacle   'none' = no obstacle plot, 'sphere', 'cylinder' 
%    az, el     azimuth and elevation for the direction to look at the plot
%    slev       level of isosurface choosed (in % of maximum value)
%    ie         # of variable to plot (ie 1 for u, 2 for v ,... )  
%    it         iteration #
%    fignum     figure number to use for output
%
%
function pl = c_wlt_3d_isostats(file,form,j_range,eps,bounds,fig_type,plot_type,mag_type,obstacle,az,el,slev,ie,it,fignum)
global POSTPROCESS_DIR
if size(j_range,2) == 1  
    j_min=j_range;
    j_max=j_range;
elseif size(j_range,2) == 2  
    j_min=j_range(1);
    j_max=j_range(2);
else
    j_min=0;
    j_max=20;
end

    if ( strcmp(fig_type , 'coeff') ) % strcmp : compare strings
    ifig = 1;
elseif ( strcmp(fig_type , 'grid' ))
    ifig = 2;
elseif ( strcmp(fig_type , 'solution') )
    ifig =  3;
elseif ( strcmp(fig_type , 'turb_stats') )
    ifig =  4;
else
    fprintf(1,'Unknown fig_type: %s \n',fig_type);  
    return
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Echo input
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
fprintf(1,'Results file : %s\n', file ); 
if ( form == 0 )
    fprintf(1,'Reading binary file\n');
else
    fprintf(1,'Reading ascii file\n');
end
fprintf(1,'range of levels to plot: %d to %d  \n', j_min , j_max );
fprintf(1,' eps = %f          (plot only values > eps)(if<0  percent of wlt coeff. range)\n', eps );
fprintf(1,'bounds = [ %f %f  %f %f %f %f ]\n', bounds );
fprintf(1,'fig_type = %s          (''coeff'' or ''grid'' or ''solution'')\n',fig_type);
fprintf(1,'ifig = %d',ifig);
fprintf(1,'plot_type = %s          (''surf'' or ''contour'' )\n',plot_type);
fprintf(1,'#equations = %d   (# of variable to plot )\n',ie);
fprintf(1,'iteration # = %d\n',it);
fprintf(1,'figure : %d         (number to use for output)\n', fignum );

%
% Read in the results file if we are going to plot either the wlt coefficients or just the grid
%
if ( ifig ~= 3  &  ifig ~= 4 )  % ~= not equal to
    fmin=10^16;
    fmax=-10^16;
    ! read top of file
    if form == 0  %------------------- reading unformated file
        ffl= [ file sprintf('%04.0f',it) '.res']
        fid = fopen(ffl,'rb','n'); %original line
%        fid = fopen(ffl,'r','l');
        fid
        dummyInteger = fread(fid,1,'int32');
        arr = fread(fid,23,'int32');
        dim = arr(1);
        n_prdct = arr(2);
        n_updt  = arr(3);
        n_assym_prdct = arr(4);
        n_assym_updt  = arr(5);
        n_assym_prdct_bnd = arr(6);
        n_assym_updt_bnd  = arr(7);
        n_var_saved = arr(8);
        saved_vort = arr(9);
        nx      = arr(10);
        ny      = arr(11);
        nz      = arr(12);
        mx      = arr(13);
        my      = arr(14);
        mz      = arr(15);
        ix_prd  = arr(16);
        iy_prd  = arr(17);
        iz_prd  = arr(18);
        ix_grid  = arr(19);
        iy_grid  = arr(20);
        iz_grid  = arr(21);
        j_lev   = arr(22);
        nwlt    = arr(23);
        
    else  %-------------------- reading formated file
        ffl= [file sprintf('%04.0f',it) '.res']
        fid = fopen(ffl);
        arr = fscanf(fid,'%f %f',[23,1]);
        dim = arr(1);
        n_prdct = arr(2);
        n_updt  = arr(3);
        n_assym_prdct = arr(4);
        n_assym_updt  = arr(5);
        n_assym_prdct_bnd = arr(6);
        n_assym_updt_bnd  = arr(7);
        n_var_saved        = arr(8);
        saved_vort = arr(9);
        nx      = arr(10);
        ny      = arr(11);
        nz      = arr(12);
        mx      = arr(13);
        my      = arr(14);
        mz      = arr(15);
        ix_prd  = arr(16);
        iy_prd  = arr(17);
        iz_prd  = arr(18);
        ix_grid  = arr(19);
        iy_grid  = arr(20);
        iz_grid  = arr(21);
        j_lev   = arr(22);
        nwlt    = arr(23);
    end
    
    %
    %check some things
    %
    
    %saved vort should be 1 if we saved vorticity 
    % set the number of componenents in the vorticity. (1 for 2D or 3 for 3D)
    if(saved_vort == 1 )
        n_saved_vort= (3-mod(dim,3) )
    else
        n_saved_vort = 0
    end
    fprintf(1,'Number of vars saved: %d\n',n_var_saved );  
    fprintf(1,'Number of components of vorticity saved: %d\n',n_saved_vort);  
    
    %make sure ie is less then the number of vars saved plus 1 (2D) or 3(3D) vars for vorticity. 
    if(ie > (n_var_saved + n_saved_vort ) )  
        error(sprintf('error, equation number exceeds number of variables in data file : %i > %i ',ie, (n_var_saved + n_saved_vort)  ))
    end  
    
    
    %
    % Read in the rest of the file
    %
    
    if form == 0  %------------------- reading unformated file
        dummyInteger = fread(fid,2,'int32'); % read fortran record end/beginning 
        lv_intrnl = fread(fid,j_lev+1,'int32');
%        iarr = fread(fid,4*dim*(j_lev+1),'int32');% Original line
        iarr = fread(fid,4*dim*(j_lev),'int32');
        for j = 1:j_lev
            lv_bnd(1:2,1:2*dim,j)=reshape(iarr(4*dim*(j-1)+1:4*dim*(j)),2,2*dim);
        end
        dummyInteger = fread(fid,2,'int32');% read fortran record end/beginning 
        xx=fread(fid,nx+1,'double');
        dummyInteger = fread(fid,2,'int32');% read fortran record end/beginning 
        yy=fread(fid,ny+1,'double');
        if(dim == 3 )
            dummyInteger = fread(fid,2,'int32');% read fortran record end/beginning 
            zz=fread(fid,nz+1,'double');
        end
        
        dummyInteger = fread(fid,2,'int32');% read fortran record end/beginning 
        indx_x = fread(fid,nwlt,'int32');
        dummyInteger = fread(fid,2,'int32');% read fortran record end/beginning 
        indx_y = fread(fid,nwlt,'int32');
        if(dim == 3 )
            dummyInteger = fread(fid,2,'int32');% read fortran record end/beginning 
            indx_z = fread(fid,nwlt,'int32');
        end
        dummyReal=fread(fid,1,'double');
        
        % read in the ie''th saved variable (skiping past anything before it)
        for i=1:ie
            c    = fread(fid,nwlt,'double');
         end
       
        status = fclose(fid);
        
        
    else  %-------------------- reading formated file
        
        lv_intrnl = fscanf(fid,'%i %i %i %i',[j_lev+1,1]);
        iarr = fscanf(fid,'%i %i %i %i',[4*dim*(j_lev),1]);
        for j = 1:j_lev
            lv_bnd(1:2,1:2*dim,j)=reshape(iarr(4*dim*(j-1)+1:4*dim*(j)),2,2*dim);
        end
        xx   = fscanf(fid,'%f %f %f %f %f %f %f %f %f %f',[nx+1,1]);
        yy   = fscanf(fid,'%f %f %f %f %f %f %f %f %f %f',[ny+1,1]);
        if(dim == 3 )
            zz   = fscanf(fid,'%f %f %f %f %f %f %f %f %f %f',[nz+1,1]);
        end
        indx_x = fscanf(fid,'%i %i %i %i %i %i %i %i %i %i',[nwlt,1]);
        indx_y = fscanf(fid,'%i %i %i %i %i %i %i %i %i %i',[nwlt,1]);
        if(dim == 3 )
            indx_z = fscanf(fid,'%i %i %i %i %i %i %i %i %i %i',[nwlt,1]);
        end
        for i=1:ie
            c    = fscanf(fid,'%f %f %f %f %f %f %f %f %f %f',[nwlt,1]);
        end
        status = fclose(fid);
    end 
    
    if eps < 0 
        minc = min(c)
        maxc = max(c)
        eps = minc + abs(eps)*(maxc-minc);
        fprintf(1,'Using eps = %d \d',eps);
    end
    
    
    j_min = max([1,j_min]);
    j_max = min([j_max,j_lev]);
    
    j = min([j,j_lev]);
    if(ix_prd==1) 
        disp('periodic in x-direction') 
    end
    if(iy_prd==1) 
        disp('periodic in y-direction') 
    end
    if(dim == 3 & iz_prd==1) 
        disp('periodic in z-direction') 
    end
    
    
    % make arrays x,y and z that are the real space location of the active wavelet collocation points
    fprintf(1,'nwlt = %d',nwlt);
    
   x(1:nwlt)=xx(indx_x(1:nwlt)+1);  %Initial lines
   y(1:nwlt)=yy(indx_y(1:nwlt)+1);
   if(dim == 3 )
       z(1:nwlt)=zz(indx_z(1:nwlt)+1);
   end

    % Set the bounds if we only want to plot a subregion
    if size(bounds,2) == 6
        xmin=bounds(1);
        xmax=bounds(2);
        ymin=bounds(3);
        ymax=bounds(4);
        zmin=bounds(5);
        zmax=bounds(6);
    else
        xmin=min(xx);
        xmax=max(xx);
        ymin=min(yy);
        ymax=max(yy);
        if(dim == 3 )
            zmin=min(zz);
            zmax=max(zz);
        end
    end 
    
    % Make arrays of the points that are >= eps 
    istm=0;
    for j=j_min:j_max
        [ii] = find( abs(c( lv_intrnl(j)+1:lv_intrnl(j+1) )) >= eps ); %find internal points >= eps
        if ~isempty( ii )
            len = length(ii);
            xstm(istm+1:istm+len)=xx(indx_x(ii+lv_intrnl(j))+1); %save the physical x location of the point
            ystm(istm+1:istm+len)=yy(indx_y(ii+lv_intrnl(j))+1); %save the physical y location of the point
            if( dim == 3) 
                stm_indx(istm+1:istm+len,1:3) = [indx_x(ii+lv_intrnl(j))+1, indx_y(ii+lv_intrnl(j))+1,indx_z(ii+lv_intrnl(j))+1 ];
                zstm(istm+1:istm+len)=zz(indx_z(ii+lv_intrnl(j))+1); %save the physical z location of the point
            end
            cstm(istm+1:istm+len)=c(ii+lv_intrnl(j)); %save the wlt coiffient at the point
            istm=istm+len;
        end
        
        for ibnd =1:2*dim
            [ii] = find( abs(c(  lv_bnd(1,ibnd,j):lv_bnd(2,ibnd,j))) >= eps ); %Now find the boundary points >= eps
            len = length(ii);
            if( ~isempty(ii) ) 
                xstm(istm+1:istm+len)=xx(indx_x(ii+lv_bnd(1,ibnd,j)-1)+1);%save the physical x location of the point
                ystm(istm+1:istm+len)=yy(indx_y(ii+lv_bnd(1,ibnd,j)-1)+1);%save the physical y location of the point
                if( dim == 3) 
                    stm_indx(istm+1:istm+len,1:3) = [indx_x(ii+lv_intrnl(j))+1, indx_y(ii+lv_intrnl(j))+1,indx_z(ii+lv_intrnl(j))+1 ];
                    zstm(istm+1:istm+len)=zz(indx_z(ii+lv_bnd(1,ibnd,j)-1)+1);%save the physical z location of the point
                end
                cstm(istm+1:istm+len)=c(ii+lv_bnd(1,ibnd,j)-1);%save the wlt coiffient at the point
                istm=istm+len;
            end
        end
    end
    
    
    if  istm  == 0 
        fprintf(1,'Zero wlt Coefficients > eps, Raise the value of eps. exiting...\n')
        return
    end
    fprintf(1,'# coeff''s above eps = %d \n', length(xstm))
end % if ifig ~= 3


%
%------- Plotting grid and solution for points with coefficients > eps  --
%
if( ifig == 1 )
    figure(fignum)
    
    if(dim == 3 ) 
        plotdim = 3;
        for zslice = 1:nz
            [aaa] = find(stm_indx(:,plotdim) == zslice );
            if( length(aaa) > 0 )

%                stem3(xstm(aaa),ystm(aaa),abs(cstm(aaa)),'fill'); % Original line
                stem3(xstm(aaa),ystm(aaa),abs(cstm(aaa))); 
                set(gca,'xlim',[xmin,xmax],'ylim',[ymin,ymax]);
                title('Wavelet Coefficients');
                xlabel('x');
                ylabel('y');
                fprintf('zSlice = %d (hit <CR> for next active Zslice )\n', zslice );
                pause;
            end
        end
    else %dim == 2
        clf
       stem3(xstm,ystm,abs(cstm),'fill'); % Original line
        set(gca,'xlim',[xmin,xmax],'ylim',[ymin,ymax]);
        title('Wavelet Coefficients')
        xlabel('x');
        ylabel('y');
    end
end 

%
%------------ Grid -------------------  
%
if( ifig == 2 )
    clf   

    if(strcmp(plot_type , 'isosurface'))
      if( dim == 3 )
        plot3(xstm,ystm,zstm,'.','Markersize',1)
        set(gca,'xlim',[xmin,xmax],'ylim',[ymin,ymax],'zlim',[zmin,zmax]);
        xlabel('x');
        ylabel('y');
        zlabel('z');
      else
        plot(xstm,ystm,'.');
        set(gca,'xlim',[xmin,xmax],'ylim',[ymin,ymax]);
        xlabel('x');
        ylabel('y');
      end

    elseif(strcmp(plot_type , 'surf'))
      plotdim = 3;
      [aaa] = find(stm_indx(:,plotdim) >= nz/2 );
      if( dim == 3 )
        plot3(xstm(aaa),ystm(aaa),zstm(aaa),'.','Markersize',1)
        set(gca,'xlim',[xmin,xmax],'ylim',[ymin,ymax],'zlim',[0,zmax]);
        xlabel('x');
        ylabel('y');
        zlabel('z');
        grid on
      else
        plot(xstm,ystm,'.');
        set(gca,'xlim',[xmin,xmax],'ylim',[ymin,ymax]);
        xlabel('x');
        ylabel('y');
      end
      view(az,el);


    elseif( strcmp(plot_type , 'contour'))
        plotdim = 3;
           for zslice = 1:nz
            [aaa] = find(stm_indx(:,plotdim) == zslice );
              set(cla,'nextplot','replace')
              clf;
              plot(xstm(aaa),ystm(aaa),'.','Markersize',1);
              set(gca,'xlim',[xmin,xmax],'ylim',[ymin,ymax]);

              axis('equal');
              axis([xmin xmax ymin ymax]);
%              view(0,90);
%              shading interp;
              view(az,el);
              set(gcf,'renderer','zbuffer');
              if( nz ~= 1 )
                title(['Grid, zSlice =' , num2str(zslice) ])
                fprintf('Grid = %d (hit <CR> for next active Zslice )\n', zslice );            
              else
                title(['Solution' ])
              end
              xlabel('x')
              ylabel('y')
              set(gca,'clim',[zmin,zmax])
              if( nz ~= 1 ) 
                pause;
              end
           end

     end


   title('Grid')

end


%
%--------------- Solution -----------------
%
if( ifig == 3 )
    %--------------------------
    %Need to read the dimension
    %--------------------------
    dim = 3

    if( strcmp(mag_type  , 'magnitude'))
        i_plot = dim;
	ie = dim*floor((ie-1)/dim)+1;
    else
       i_plot = 1;
    end
    for i_p = 1:i_plot
       figure(fignum)
       [u,x,y,z,nx,ny,nz]=inter3d(file,form,eps,bounds,ie,j_max,it);
          u_work (:,:,:,i_p) = u(:,:,:);
       if( strcmp(mag_type  , 'magnitude'))
          ie=ie+1;
       end
    end
    if( strcmp(mag_type  , 'magnitude'))
     u = zeros(size(u_work(:,:,:,1)));
       for i_p = 1:dim
         u(:,:,:) = u(:,:,:) + u_work(:,:,:,i_p).^2;
       end
       u = u.^0.5;
    else
       u = u_work;
    end

    if size(bounds,2) == 6
        xmin=bounds(1);
        xmax=bounds(2);
        ymin=bounds(3);
        ymax=bounds(4);
        zmin=bounds(5);
        zmax=bounds(6);
    else
        xmin=min(xx);
        xmax=max(xx);
        ymin=min(yy);
        ymax=max(yy);
        if(dim == 3 )
            zmin=min(zz);
            zmax=max(zz);
        end
    end 
    
    nu = 0.01
    x = (1/nu)*(1+x);
    
        if( strcmp(plot_type  , 'surf'))
         if( nz ~= 1 ) % if nz ~=1 we assume dim == 3
           zmin=min(min(min(u)));
           zmax=max(max(max(u)));
         else
           zmin=min(min(u));
           zmax=max(max(u));
         end
         if( zmin == zmax ) 
           zmin = zmin -1;
           zmax= zmax +1;
         end
          for zslice = 1:nz
             set(cla,'nextplot','replace')
             clf;
            l1=0.1;b1=0.1;w1=0.75;h1=0.75;
              ax1 = axes('position',[l1,b1,w1,h1]);
            surface(x,y,u(:,:,zslice));
            view(az,el);
            vor_pal;
              wmax = max(max(abs(u(:,:,zslice))))
              caxis([-wmax wmax]);
              ax2 = axes('position',[l1+w1+0.02,b1+0.02,0.03,h1*0.92]);
              set(ax2,'fontsize',16);
              colorbar(ax2,'peer',ax1);
              set(gcf,'renderer','zbuffer');
            set(gca,'xlim',[xmin,xmax],'ylim',[ymin,ymax],'zlim',[zmin,zmax]);
            shading interp
            set(gca,'PlotBoxAspectRatio',[1 1 1])
            %set(gca,'View',[40 30])
            zlabel('solution')
            if( nz ~= 1 )
              title(['Solution, zSlice =' , num2str(zslice) ])
              fprintf('zSlice = %d (hit <CR> for next active Zslice )\n', zslice );            
            else
              title(['Solution' ])
            end
            xlabel('x')
            ylabel('y')
            set(gca,'clim',[zmin,zmax])
            if( nz ~= 1 ) 
               pause;
            end
          end

        elseif( strcmp(plot_type , 'contour'))
           for zslice = 1:nz
              set(cla,'nextplot','replace')
              clf;
              l1=0.1;b1=0.1;w1=0.75;h1=0.75;
              ax1 = axes('position',[l1,b1,w1,h1]);
              set(ax1,'fontsize',16); 
              mycontourf(x,y,u(:,:,zslice),100); %was 32 contour levels
              vor_pal;
              axis([xmin xmax ymin ymax]);
              view(0,90);
              wmax = max(max(abs(u(:,:,zslice))))
              caxis([-wmax wmax]);
              ax2 = axes('position',[l1+w1+0.02,b1+0.02,0.03,h1*0.92]);
              set(ax2,'fontsize',16);
              colorbar(ax2,'peer',ax1);
              set(gcf,'renderer','zbuffer');
           if( nz ~= 1 )
              title(['Solution, zSlice =' , num2str(zslice) ])
              fprintf('zSlice = %d (hit <CR> for next active Zslice )\n', zslice );            
           else
              title(['Solution' ])
           end
           xlabel('x')
           ylabel('y')
           set(gca,'clim',[zmin,zmax])
           if( nz ~= 1 ) 
              pause;
            end
           end

        elseif(  strcmp(plot_type , 'isosurface'))
          clf;
          % Smooth data
             ws(:,:,:) = smooth3(u(:,:,:));
          clear w;

          % Set up coordinate grid
          wmax = max(max(max(abs(ws(:,:,:)))));
          s = [0 0 0];
          s(1) = size(ws,1);
          s(2) = size(ws,2);
          s(3) = size(ws,3);
          x=linspace(xmin,xmax,s(1));
          y=linspace(ymin,ymax,s(2));
          z=linspace(zmin,zmax,s(3));
          [x,y,z]=meshgrid(x,y,z);
          slev = slev/100*wmax


	  % Plot the obstacle
          % -----------------

        if( strcmp(obstacle  , 'sphere'))
	    [x1,y1,z1] = sphere(20);
            x1=0.5.*x1; y1=0.5.*y1; z1=0.5.*z1;
            surf(x1,y1,z1,'FaceColor',[0.8 0.8 0.8], 'EdgeColor', 'none')
        elseif( strcmp(obstacle  , 'cylinder'))
	    [x1,y1,z1] = cylinder;
            x1=0.5.*x1; y1=0.5.*y1; z1=(xmax-xmin)*(z1-0.5);
            surf(x1,y1,z1,'FaceColor',[0.8 0.8 0.8], 'EdgeColor', 'none')
        elseif( strcmp(obstacle  , 'none'))
            fprintf(1,'None obstacle plot \n');
        else
            fprintf(1,'Unknown obstacle: %s \n',plot_type);  
            return
        end


          % Plot isosurfaces
             opengl autoselect
             hiso1 = patch(isosurface(x,y,z,ws(:,:,:), slev,ws(:,:,:)),'FaceColor', 'interp','FaceAlpha', 0.5, 'EdgeColor', 'none');
             hiso2 = patch(isosurface(x,y,z,ws(:,:,:),-slev,ws(:,:,:)),'FaceColor', 'interp','FaceAlpha', 0.5, 'EdgeColor', 'none');
             set(hiso1,'SpecularExponent',15);
             set(hiso2,'SpecularExponent',15);
             isonormals(x,y,z,ws(:,:,:),hiso1);
             isonormals(x,y,z,ws(:,:,:),hiso2);
             view(az,el);
             vor_pal;
             caxis([-wmax,wmax]);
             colorbar('vert');
             daspect([1,1,1]);
             lighting phong;
             [ylight,xlight,zlight] = sph2cart(az,el,1);
             light('Position',[xlight+1  -ylight+1  zlight+1],'Style','infinite');
             light('Position',[xlight-1  -ylight-1  zlight+1],'Style','infinite');
             alpha(0.5) 
             alphamap('increase',.3) 
             axis([xmin xmax ymin ymax zmin zmax]);
             xlabel('X');ylabel('Y');zlabel('Z');
             set(gcf,'Renderer','OpenGL');
             title('Solution')   
             xlabel('x')
             ylabel('y')
             grid on;
             set(gca,'clim',[zmin,zmax])
             if( nz ~= 1 ) 
               pause;
             end

       else 
            fprintf(1,'Unknown plot_type: %s \n',plot_type);  
            return
       end

    hold off


end


%
%---------------  plot energy spectra -----------------
%
if( ifig == 4 )
    %--------------------------
    %Need to read the dimension
    %--------------------------
    dim = 3
    figure(fignum)
    if size(bounds,2) ~= 6
        bounds = [-10.^5 10.^5 -10.^5 10.^5  -10.^5 10.^5];
     end
     ffl=  'inp.dat';
     fid = fopen(ffl,'w');
     fprintf(fid,'%s\n',file);
     fprintf(fid,'%12.5e \n %12.5e %12.5e %12.5e %12.5e %12.5e %12.5e \n %7i %7i %7i %7i',...
	     eps,bounds,form,ie,j_max,it);
     status = fclose(fid);
     
     eval(['!' POSTPROCESS_DIR slsh 'view_spectra' slsh 'c_wlt_turbstats.out'])
     % !\rm inp.dat
     ffl= [ file sprintf('%04.0f',it) '.spectra']
     s = load(ffl);
     
     loglog(s(:,1),s(:,2))
     title([ 'Spectra,  iteration: ' file sprintf('%04.0f',it) ])
     ylabel('energy spectra')
     xlabel('Wave #')
     
     % make the x^5/3 line
    x = 0:.1:100 ;
    %y = (x).^(-5/3) ;
    % make shifted  x^5/3 line
    y = (x).^(-5/3) * exp(6) ;
    hold on;
    loglog(x,y,'k--');
    %hold off;

     figure(fignum+1)
     loglog(s(:,1),s(:,3))
     title([ ' Dissipation, iteration: ' file sprintf('%04.0f',it) ])
     ylabel('Dissipation')
     xlabel('Wave #')
     hold on
 end
 