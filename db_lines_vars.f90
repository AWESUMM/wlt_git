!
!  This module contains all variable that are shared between db_lines_frontend, db_lines_backend
!  and wavelet_3d_lines
MODULE db_lines_vars
  USE precision
  USE user_case_db
! Lines starting with !$ are only compiled if for openMP
!$  USE OMP_LIB

  INTEGER , PARAMETER :: KIND_INT_J           = SELECTED_INT_KIND(2) ! integers needed to represent 1:j_lev
  INTEGER , PARAMETER :: KIND_INT_FLAGS       = SELECTED_INT_KIND(2) ! integers needed to represent 1:MAX_FLAGS bit flags
  INTEGER , PARAMETER :: KIND_INT_J_LEV_FLAGS = SELECTED_INT_KIND(4) ! integers needed to represent 1:J_LEV bit flags
  INTEGER , PARAMETER :: KIND_INT_BND         = SELECTED_INT_KIND(1) ! integers needed to represent # of boundaries


  TYPE grid_pt_ptr
     TYPE (grid_pt) , POINTER  :: ptr  => NULL()  ! initalize as null
  END TYPE grid_pt_ptr

 
  TYPE grid_pt
!     INTEGER (KIND_INT_J) :: level                           ! level
     INTEGER                :: reg_grid_index(max_dim)        ! index in the regular grid of size Nx*Ny*Nz , x,y,z
     REAL(pr)               :: u(n_var_db)                    ! real values of function at pt
!COMMENTiu     INTEGER , POINTER      :: iu(:)    => NULL()   ! integer values of function at pt
     INTEGER (KIND_INT_FLAGS) :: flags                        ! Flags of grid point
     TYPE  (grid_pt_ptr)    :: next(max_dim)                  ! dim links to the next nodes in the line
     INTEGER (KIND_INT_J)   :: deriv_lev                      ! derivative level for this collocation point
!    INTEGER (KIND_INT_J)   :: truncated_deriv_lev            ! truncated derivative level for this collocation point
     INTEGER                :: indx                           ! linear index from the flat array u (or du d2u)
	 INTEGER (KIND_INT_BND) :: ibndr                          ! # of the bounary 
  END TYPE grid_pt



  TYPE element_of_face_ptr
     TYPE (element_of_face), POINTER :: ptr   => NULL()  ! initalize as null
  END TYPE element_of_face_ptr

  ! curret cost 2*jmx*8 + 2 bytes per face element
  !
! Note, This needs to be converted so that there is a single ptr to the head of a list 
! of points ordered from j=1 to j=j_mx. Then all sweeps are done from the
! head of this list list until either we hit a null next or j>j_in. D.G.
  TYPE element_of_face
!UNUSED DG. may be used later     INTEGER, POINTER :: number_of_nodes_of_each_level(:) => NULL()      ! 1, 2, ..., J_MAX (UNNEEDED)
     TYPE(grid_pt_ptr), POINTER :: first_node_of_each_level(:)  => NULL()! 1 -> 2 -> .... -> J_MAX
!UNUSED DG.     TYPE(grid_pt_ptr) :: leftmost_node, rightmost_node                  ! boundary nodes
!UNUSED DG.          INTEGER :: leftmost_level, rightmost_level                          ! levels of boundary nodes
     INTEGER (KIND_INT_J) :: line_flags    ! bit 0 - if internal arrays allocated
	                                       ! bit j=1:j_mx true is atleast 1 pt with derive lev == j (bit position)
!UNUSED DG.	 INTEGER , POINTER    :: face_coord(:)    => NULL()  !(UNNEEDED, find from first point!!!DG ) initalize as null  ! coordinate of this face. 
  END TYPE element_of_face

  ! PARAMETERS for accessing elements_of_face%line_flags
  INTEGER, PARAMETER   :: LINE_IS_INITIALIZED = 0

  !
  ! This array is pointers from u indexing (1,nwlt) directly into the
  ! database. This array is created/updated in update_u_from_db
  ! and then used in update_db_from_u to return the pts to the db
  TYPE (grid_pt_ptr)   , DIMENSION(:) , ALLOCATABLE :: u_ptrs_to_db
  INTEGER              ::  len_u_ptrs_to_db !length of current u_ptrs_to_db
  INTEGER  , PARAMETER :: u_ptrs_to_db_buf = 500 ! length to grow u_ptrs_to_db over db_nwlt when necessary

!  !
!  ! This array is pointers from u indexing (1,nwlt) directly into the
!  ! database. This array is created/updated in update_u_from_db
!  ! to keep track of bnd points int he first db sweep.
!  TYPE (grid_pt_ptr)   , DIMENSION(:) , ALLOCATABLE :: u_ptrs_to_db_bnd
!  INTEGER  :: len_u_ptrs_to_db_bnd !length of current u_ptrs_to_db
!  INTEGER  ,PARAMETER :: u_ptrs_to_db_buf_bnd = 50 ! length to grow u_ptrs_to_db over db_nwlt when necessary


  ! flag bit position in grid_pt%flags
  INTEGER ,PARAMETER :: ACTIVE = 0    
  INTEGER ,PARAMETER :: ADJACENT_X = 1  
  INTEGER ,PARAMETER :: ADJACENT_Y = 2  
  INTEGER ,PARAMETER :: ADJACENT_Z = 3  
  INTEGER ,PARAMETER :: SIG_FLAG = 4
  INTEGER ,PARAMETER :: GHOST = 5     
  INTEGER ,PARAMETER :: BND = 6       ! boundary pt (DO WE NEED THIS?..)
    ! INSTEAD just enforce rule that in any line of active points the first two
    ! or less points are BDN points. This way the maximum search for BND points
    ! is checking two points. ANd interior starts after 2nd point
    ! or last point that is a BND point.
  INTEGER ,PARAMETER :: DELETE = 7       ! marked to be deleted 
  INTEGER ,PARAMETER :: MAX_FLAG =  8    ! maximum number fo flag bits
  

  

  ! acceleration indices for use with line by line transform
  TYPE TRNS_ACCEL
     INTEGER :: ix_l
     INTEGER :: ix_h
     INTEGER :: ix_j !ix_j is consecutive from 1 to max pts on this level j
  END TYPE TRNS_ACCEL
  TYPE (TRNS_ACCEL) , DIMENSION(:,:,:,:) , ALLOCATABLE, SAVE :: ix_lh_db! (ix_even,idim,j,type) 

  ! 
  ! Acceleration indeces for finding derivative
  !  ix_lh_diff_db( indx, first/second deriv, deriv_lev, dim, meth) 
  !  indx - index into line in idim dir
  !  first/second deriv - 1:first, 2: second
  !  deriv_lev - 1:j_lev
  !  dim - direction of transform
  !  method 
  ! nlvD_in( 1:2, :, : )  - first derivative in x-dir
  ! nlvD_in( 3:4, :, : )  - first derivative in y-dir
  ! nlvD_in( 5:6, :, : )   - second derivative in x-dir
  ! nlvD_in( 7:8, :, : )  - second derivative in y-dir
  ! nlvD_in( 9:10, :, : )  - first derivative in z-dir
  ! nlvD_in( 11:12, :, : ) - second derivative in z-dir
TYPE DERIV_ACCEL
     INTEGER :: ix_l
     INTEGER :: ix_h
END TYPE DERIV_ACCEL
TYPE (DERIV_ACCEL) , DIMENSION(:,:,:,:,:)   , ALLOCATABLE, SAVE :: ix_lh_diff_db ! 
   
  REAL (pr), DIMENSION ( :,:,:,:,: ) , ALLOCATABLE, SAVE :: wgh_updt_db
  REAL (pr), DIMENSION ( :,:,:,:,: ) , ALLOCATABLE, SAVE  :: wgh_prdct_db


!  INTEGER :: N_bnd_db(1:3)  ! number of current boundary pts, This is updated
!                         ! when a bnd pt is added. 


!  TYPE (element_of_face_ptr) , DIMENSION(:), POINTER :: lines_indx_backend !  list of active line faces
!  INTEGER ::  numlines_indx_backend ! number of active lines indexed in lines_indx_backend
!  INTEGER ::  lines_indx_current    ! current active line to be returned 

  !LOGICAL ::  start_dim_sweep       !Reset start_dim_sweep so the next call to get_active_lines will get the first active line

  INTEGER , DIMENSION(:) , ALLOCATABLE, SAVE :: lvl_db !lvl(1:dim,1:MAXVAL(nxyz))
                                                   !map between location and level
  INTEGER :: db_nwlt ! number of active(+adjacent) points in data base
                     ! This is internal to db_mod and wavelet_3d_lines.
                     ! The external count nwlt
                     ! is updated in the call to update_u_from_db()
  INTEGER :: db_nwlt_new ! number of active(+adjacent) points in data base
                     ! that is updated while points are added and deleted in
                     ! db_adjacent_wlt and db_reconstr_check. It is
                     ! set to db_nwlt at the beginning of adapt_grid_lines.
                     ! This is internal to db_mod. The external count nwlt
                     ! is updated in the call to update_u_from_db()
  INTEGER :: db_ghost !number of ghost points
  INTEGER :: db_n_bnd_total ! number of active bnd points
  INTEGER ::  dbmax(1:3) ! maximum dimensions of database
 

  ! Flag defined here are used in MODULES db_mod and  db_backend_mod
  ! They need to be defined exactly like this for any version of
  ! db_backend_mod.
  !
  ! Flags to show if we want first intitial point or the next
  ! point when  calling a link list traverse function
  ! 
  INTEGER ,PARAMETER ::  INIT =1, NEXT=2 
  ! Flags to define what boundary we are working on
  INTEGER ,PARAMETER ::  XMINBD=1,XMAXBD=2,YMINBD=3,YMAXBD=4,ZMINBD=5,ZMAXBD=6

  ! Flags to define whether to get DELETE points in get_active_line()
  INTEGER ,PARAMETER ::  NO_DELETE=0, GET_DEL_PTS=1, GET_DEL_INLST=2 
  ! Flags to define whether to get points on level above in get_active_line()
  INTEGER ,PARAMETER ::  NO_GET_J_ABOVE=0, GET_ABOVE_PTS=1
  ! Flags to define whether to get points and return them in even/odd lists in get_active_line()
  INTEGER ,PARAMETER ::  LINE_ONE=0, LINE_EVEN_ODD=1
  ! Flags to define whether to get ghost points  get_active_line()
  INTEGER ,PARAMETER ::    GET_GHOST_PTS = 0, NO_GHOST_PTS=1
  ! Flags for put_active_line
  INTEGER ,PARAMETER ::  IGNORE_U=0, UPDATE_U=1
  INTEGER ,PARAMETER ::  DELETE_FROM_DB = 0, DELETE_ZERO_ONLY=1, DELETE_UPDATE=2 
  INTEGER ,PARAMETER ::  GET_INT_PTS    = 0, GET_ALL_PTS=1
  INTEGER ,PARAMETER ::  GET_ALL_DERIV_LEV = -1 ! tell get_active_lines_indx to disregard deriv_lev
  LOGICAL ,PARAMETER ::  SORT_PTS_TRUE = .TRUE.
  LOGICAL ,PARAMETER ::  SORT_PTS_FALSE = .FALSE.
  INTEGER , DIMENSION(6) ::   db_nbnd, db_ibc

  ! Flag for first argument of, init_lines_db_backend()
  ! to determine if and actual node points are added to DB
  !
  INTEGER, PARAMETER :: INIT_DONT_ADD_PTS = 0 
  INTEGER, PARAMETER :: INIT_ADD_PTS = 1

END MODULE db_lines_vars

