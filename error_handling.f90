! This module contains error handling subroutines for easy and general errors, warnings, and diagnostic messages

MODULE error_handling
  IMPLICIT NONE

  PUBLIC :: error, print_extrema

CONTAINS  
  !--------------------------------------------------------------------------------------------
  ! ERROR:
  !    error (msg,...)
  !                                   it will print optional message(s)
  !                                   and terminate the program
  !
  ! WARNING:
  !    error (msg,...,ignore_warnings)
  !                                   it will print optional message(s)
  !                                   and terminate if .NOT.ignore_warnings
  !
  SUBROUTINE error ( msg0, msg1, msg2, msg3, msg4, msg5, msg6, msg7, msg8, msg9, msg10, &
       msg11, msg12, msg13, msg14, msg15, IW, F )
    USE parallel
    CHARACTER (LEN=*), INTENT(IN), OPTIONAL :: msg0, &  ! messages to display
         msg1, msg2, msg3, msg4, &
         msg5, msg6, msg7, msg8, &
         msg9, msg10, msg11, msg12, &
         msg13, msg14, msg15
    LOGICAL, INTENT(IN), OPTIONAL :: IW                 ! present in warnings calls
    LOGICAL, INTENT(IN), OPTIONAL :: F                  ! forced ignoring of warnings (if .TRUE.)
    LOGICAL :: terminate, forced_ignore
    
    terminate = .TRUE.                                       ! default: terminate
    forced_ignore = .FALSE.                                  ! and do not ignore
    IF (PRESENT(IW)) terminate = .NOT.IW                     !
    IF (PRESENT(F)) forced_ignore = F

    IF (forced_ignore) terminate = .FALSE.                   ! forced ignoring of warnings
    
    
    IF (par_rank.EQ.0) THEN
       WRITE (*,'(" ")')
       IF (PRESENT(IW)) THEN
          WRITE (*,'(" WARNING:")')
       ELSE
          WRITE (*,'(" ERROR:")')
       END IF
       IF (PRESENT(msg0)) WRITE (*,'("  ",A)') TRIM(msg0)
       IF (PRESENT(msg1)) WRITE (*,'("  ",A)') TRIM(msg1)
       IF (PRESENT(msg2)) WRITE (*,'("  ",A)') TRIM(msg2)
       IF (PRESENT(msg3)) WRITE (*,'("  ",A)') TRIM(msg3)
       IF (PRESENT(msg4)) WRITE (*,'("  ",A)') TRIM(msg4)
       IF (PRESENT(msg5)) WRITE (*,'("  ",A)') TRIM(msg5)
       IF (PRESENT(msg6)) WRITE (*,'("  ",A)') TRIM(msg6)
       IF (PRESENT(msg7)) WRITE (*,'("  ",A)') TRIM(msg7)
       IF (PRESENT(msg8)) WRITE (*,'("  ",A)') TRIM(msg8)
       IF (PRESENT(msg9)) WRITE (*,'("  ",A)') TRIM(msg9)
       IF (PRESENT(msg10)) WRITE (*,'("  ",A)') TRIM(msg10)
       IF (PRESENT(msg10)) WRITE (*,'("  ",A)') TRIM(msg11)
       IF (PRESENT(msg10)) WRITE (*,'("  ",A)') TRIM(msg12)
       IF (PRESENT(msg10)) WRITE (*,'("  ",A)') TRIM(msg13)
       IF (PRESENT(msg10)) WRITE (*,'("  ",A)') TRIM(msg14)
       IF (PRESENT(msg10)) WRITE (*,'("  ",A)') TRIM(msg15)
       IF (terminate) WRITE (*,'(" terminating ...")')
       IF (forced_ignore) THEN
          WRITE (*,'("  ignored (hardwired into the code)")')
       ELSE
          IF (PRESENT(IW)) THEN
             IF (IW) THEN
                WRITE (*,'("  ignored (by input parameter ignore_warnings = T)")')
             ELSE
                WRITE (*,'("  (set input parameter ignore_warnings = T")')
                WRITE (*,'("   if you are certain to ignore that warning)")')
             END IF
          END IF
       END IF
       WRITE (*,'(" ")')
    END IF
    IF (terminate) THEN
       CALL parallel_finalize
       STOP
    END IF
    
  END SUBROUTINE error


  ! Diagnostic tool for checking the extrema of an array
  SUBROUTINE print_extrema ( data_array, nlocal, msg )
    USE parallel
    USE precision 
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: nlocal
    REAL (pr), DIMENSION(1:nlocal), INTENT(IN) :: data_array
    CHARACTER (LEN=*), INTENT(IN)  :: msg     ! messages to display
    REAL (pr) :: tmp_min, tmp_max

    tmp_min = MINVAL( data_array )
    tmp_max = MAXVAL( data_array )
    CALL parallel_global_sum( REALMINVAL=tmp_min )
    CALL parallel_global_sum( REALMAXVAL=tmp_max )
    IF( par_rank .EQ. 0 ) THEN
       WRITE (*,'("  ",A,2X)', ADVANCE='NO') TRIM(msg)
       WRITE (*,'(2X,A,5X)', ADVANCE='NO') "(MIN/MAX)"
       WRITE (*,'(2E12.5)', ADVANCE='YES') tmp_min, tmp_max
    END IF

  END SUBROUTINE print_extrema

END MODULE error_handling
