MODULE wlt_trns_vars
  USE wlt_vars

  !=========================== TYPE structure for indx_DB ===============================================================
  TYPE, PUBLIC :: indx_list_DB
     INTEGER :: ixyz ! points to global contracted coordinate on wrk array
     INTEGER :: i    ! location in 1...nwlt index 
                     ! think how to add pointer to DB here and make it PRIVITE
  END TYPE indx_list_DB
  ! one cell of j_df x FACE x LEVEL x j matrix
  TYPE, PUBLIC :: indx_type_DB 
     TYPE ( indx_list_DB ), POINTER :: p(:)
     INTEGER :: length                           ! used as a counter
     INTEGER :: real_length                      ! real size of the allocated array p(:)
     INTEGER :: shift                            ! used for finding coordinates of bounary points in V cycle, when j < j_lev
  END TYPE indx_type_DB
  TYPE (indx_type_DB), ALLOCATABLE, DIMENSION(:,:,:,:), TARGET, PUBLIC :: indx_DB
                 
     !indx_DB(j_df,wlt_type,face_type,j) j_df level of derivative everywhere except %shift
     !Special interpretation of %shift
     !indx_DB(j,wlt_type,face_type,jj)%shift - first index has meaning of level of resolution j < j_lev used for multigrid calculations
  
  INTEGER, PARAMETER, PUBLIC :: TYPE_DB = DB_TYPE_WRK                ! so main knows which db module is being used.
  

END MODULE wlt_trns_vars
