MODULE util_mod
  USE share_consts
  USE wlt_vars
  USE wavelet_filters_vars
  USE wlt_trns_mod
  IMPLICIT NONE

CONTAINS




  !OLD! SUBROUTINE echo_inputs(file_input)
  !OLD!  IMPLICIT NONE
  !OLD!  CHARACTER (LEN=256) :: file_input
  !OLD! CHARACTER (LEN=256) :: buf
  !OLD! INTEGER :: io_stat

  !OLD!  OPEN (1, FILE=file_input, STATUS='OLD')

  !OLD!  PRINT *, 'Input file used for this run '
  !OLD!  PRINT *, '######################################################################'
  !OLD!  DO 
  !OLD!     READ (UNIT=1,FMT='(A256)',IOSTAT=io_stat) buf
  !OLD!    IF( io_stat < 0 ) THEN !eof has been reached
  !OLD!       WRITE(*,*) TRIM(buf)
  !OLD!        exit !exit loop
  !OLD!     ELSE IF( io_stat > 0 ) THEN ! error condition other then eof
  !OLD!        PRINT *, 'Error in echo_inputs, io_stat = ', io_stat
  !OLD!        stop
  !OLD!    END IF
  !OLD!     WRITE(*,*) TRIM(buf)
  !OLD!  END DO
  !OLD!
  !OLD!  PRINT *, 'End of Input File'
  !OLD!  PRINT *, '######################################################################'
  !OLD!  PRINT *, ''
  !OLD!END SUBROUTINE

  !
  ! Calculate the current wlt compression
  !
  FUNCTION wlt_comp(nlocal)
    USE sizes
    IMPLICIT NONE
    INTEGER , INTENT(IN) :: nlocal
    REAL (pr) :: wlt_comp
    REAL (pr) :: den
    INTEGER :: idim

    den = 1.0_pr
    DO idim = 1,dim
       den = den * (nxyz(idim)+(1-prd(idim)))
    ENDDO

    wlt_comp  = 100.0_pr * (REAL(nlocal)/den)

  END FUNCTION wlt_comp

  !
  ! same thing but for integers 
  ! Need sepearate routine to define the INTERFACE for integer argument
  !
  !in progress! SUBROUTINE reverse_endian_real( buff, n )
  !in progress!   USE precision
  !in progress!   IMPLICIT NONE
  !in progress!   INTEGER   ,INTENT(IN)     :: n
  !in progress!   REAL(pr)   ,INTENT(INOUT) :: buff(1:n)
  !in progress!   INTEGER                   :: the_real, size, i
  !in progress! 
  !in progress!   COMMON /rev_ind/ the_real
  !in progress! 
  !in progress!   size = pr ! get size of REAL in bytes
  !in progress!      
  !in progress!   DO i = 1, n
  !in progress!       the_real = buff(i)
  !in progress!       CALL reverse_endian(size)  !swaps bytes in COMMON BLOCK
  !in progress! 	  buff(i) = the_real
  !in progress!   END DO
  !in progress! END SUBROUTINE reverse_endian_real

  !
  ! same thing but for integers 
  ! Need sepearate routine to define the INTERFACE for integer argument
  !
  !in progress! SUBROUTINE reverse_endian_int( buff, n )
  !in progress!   USE precision
  !in progress!   IMPLICIT NONE
  !in progress!   INTEGER   ,INTENT(IN)    :: n
  !in progress!   INTEGER   ,INTENT(INOUT) :: buff(1:n)
  !in progress!   INTEGER                  :: the_int, size, i
  !in progress! 
  !in progress!   COMMON /rev_ind/ the_int
  !in progress! 
  !in progress!   size = BIT_SIZE(the_int)/8 ! get size of int in bytes
  !in progress! 
  !in progress!   DO i = 1, n
  !in progress!       the_int = buff(i)
  !in progress!       CALL reverse_endian(size)  !swaps bytes in COMMON BLOCK
  !in progress! 	  buff(i) = the_int
  !in progress!   END DO
  !in progress! END SUBROUTINE reverse_endian_int




  SUBROUTINE set_weights()
    USE precision
    USE pde
    USE sizes
    USE util_vars
    USE wlt_trns_mod
    USE debug_vars
    USE parallel
    IMPLICIT NONE
    INTEGER :: j
    REAL(pr) :: sum_dA_tmp, sum_dA_level_tmp
    


    !--Set weights for area/volume associated with each wlt collocation point
    IF(ALLOCATED(dA)) DEALLOCATE (dA)
    ALLOCATE (dA(1:nwlt))
    IF(ALLOCATED(dA_level)) DEALLOCATE (dA_level)
    ALLOCATE (dA_level(1:nwlt,1:j_lev))
    
    CALL weights
    
    
    
    ! debugging weight information
    IF (verb_level.GT.1) THEN
       
       sum_dA_tmp = SUM(dA)
       CALL parallel_global_sum (REAL=sum_dA_tmp)
       IF (par_rank.EQ.0) PRINT *,'TEST sum dA = ', sum_dA_tmp
       
       DO j =1, j_lev
          sum_dA_level_tmp = SUM(dA_level(1:Nwlt_lev(j,1),j))
          CALL parallel_global_sum (REAL=sum_dA_level_tmp)
          IF (par_rank.EQ.0) WRITE(*,'(" TEST sum dA_(",I2,")=",E12.5)') j, sum_dA_level_tmp
       END DO
       IF (par_rank.EQ.0) THEN
          PRINT *,'TEST area = ', PRODUCT(xyzlimits(2,1:dim)-xyzlimits(1,1:dim))
          PRINT *,'TEST sum dA/area = ', sum_dA_tmp/PRODUCT(xyzlimits(2,1:dim)-xyzlimits(1,1:dim))
       END IF

    END IF


    !TEST ONLY
    !write(it_string,'(I4.4)') it
    !file_out=TRIM(file_wlt)//it_string//'.dA'
    !OPEN (111, FILE=file_out, STATUS='UNKNOWN',FORM='FORMATTED')
    !do j=1,nwlt
    !   WRITE(111,'(I5,1X,F30.20)') j,dA(j)
    !end do
    !CLOSE(111)
    !TEST ONLY
    !write(it_string,'(I4.4)') it
    !file_out=TRIM(file_wlt)//it_string//'.dA_low'
    !OPEN (111, FILE=file_out, STATUS='UNKNOWN',FORM='FORMATTED')
    !do j=1,nwlt
    !   WRITE(111,'(I5,1X,F20.8)') j,dA(j)
    !end do
    !CLOSE(111)
  END SUBROUTINE set_weights

!!$  SUBROUTINE resolved_stats ( u ,j_mn, startup_flag)
!!$    IMPLICIT NONE
!!$    REAL (pr), DIMENSION (nwlt,1:n_var), INTENT (IN) :: u
!!$    INTEGER , INTENT (IN) :: j_mn 
!!$    INTEGER , INTENT (IN) :: startup_flag
!!$    CHARACTER (LEN=256)  :: filename
!!$    INTEGER outputfileUNIT
!!$    REAL (pr) :: area !area of domain to normalize dA
!!$    REAL (pr) :: ttke
!!$	REAL (pr), DIMENSION (1:nlocal,6) :: S_ij 
!!$    REAL (pr), DIMENSION (nlocal)     :: twoSijSij !turbulent eddy viscosity 
!!$    REAL (pr) :: total_resolved_diss_loc
!!$	LOGICAL , SAVE :: start = .TRUE.
!!$
!!$    filename = TRIM(res_path)//TRIM(file_gen)//'resolved.stats'
!!$    PRINT *,' resolved_stats, logging to :', filename
!!$
!!$    IF( ALLOCATED(dA) ) THEN
!!$       area =  (xyzlimits(2,1)- xyzlimits(1,1)) * &
!!$            (xyzlimits(2,2)- xyzlimits(1,2)) * &
!!$            (xyzlimits(2,3)- xyzlimits(1,3)) 
!!$
!!$       ttke = 0.5_pr * SUM( (dA(:)/area)*(u(:,1)**2 + u(:,2)**2 + u(:,3)**2 ))
!!$       
!!$ 
!!$    CALL Sij( u(1:nlocal,1:dim) ,nlocal, S_ij(:,1:6), twoSijSij, .TRUE. )
!!$ 
!!$
!!$    total_resolved_diss_loc = SUM( dA*nu* twoSijSij**2 )
!!$
!!$       ! Find the TKE if the dA weights have been allocated (and it is
!!$       ! assumed that if they are allocated they have been calculated)
!!$       PRINT *,' Resolved Field Stats'
!!$       PRINT *,'***********************************************'
!!$      WRITE(*,'(" (Approximate) TKE =",E12.5)')  ttke
!!$       PRINT *,' total_resolved_diss = ',total_resolved_diss
!!$       PRINT *,'***********************************************'
!!$       PRINT *,''
  
!!$       outputfileUNIT = 157
!!$       OPEN (UNIT = outputfileUNIT, FILE =filename, STATUS='unknown',&
!!$            POSITION='APPEND')
!!$ 
!!$	   IF( start ) THEN
!!$       write(UNIT=outputfileUNIT,ADVANCE='YES', FMT='( a )' ) &
!!$            '% Time tke Cs total_resolved_diss_loc '
!!$            
!!$	   END IF
!!$	   start = .FALSE.
!!$	   write(UNIT=outputfileUNIT,ADVANCE='YES' , &
!!$            FMT='(  3(e15.7 , '' '') )' ) &
!!$            t, ttke , total_resolved_diss 
!!$       CLOSE(UNIT = outputfileUNIT)
!!$    END IF
  
!!$ END SUBROUTINE resolved_stats

SUBROUTINE diffsmooth (smoothu,nlocal,ne_local,methdiff,IDdiff,diffdir,Dfac_loc,delta_loc,deltadel_loc,j_sm_in,j_st_in,BCtype,boundit)
    !diffsmooth uses explicit solver of the diffusion eqn to smooth fields
    !written by Scott Reckinger
    !
    !PARAMETERS:
    !   smoothu      - Both input and output of field that will be smoothed
    !   nlocal       - field size (number of points in smoothu)
    !   ne_local     - number of variables in smoothu
    !   methdiff     - meth for c_diff_fast (0 or 1 for central, 2 or 3 for backward, 4 or 5 for forward; even for low order, odd for high order)
    !   IDdiff       - ID for c_diff_fast (+/- [10, 01, or 11])
    !   diffdir      - direction of diffusion (0 = all directions, [1, 2, or 3] = diffuse only in that direction)
    !   Dfac_loc     - factor applied to stability requirement for dt (1 = at stability requirement, 0.1 = one tenth of stability requirement)
    !   delta_loc    - approximate post-smoothing diffusion thickness for and input step function, used to set total diffusion time; set <0 to use deltadel_loc
    !   deltadel_loc - sets final diffusion thickness using spatial resolution (dx, dy, or dz), number of points across final thickness, used to set total diffusion time; only if delta_loc<0
    !   j_sm_in      - level to set dx, dy, and dz for smoothing; only used for deltadel_loc; set =-1 to use j_lev, set =-2 to use j_mx, set =-3 to use j_mx-1
    !   j_st_in      - level to set dx, dy, and dz for stability
    !   BCtype       - type of boundary treatment; 1=Dirichlet (use input boundary values), 2=Neumann (not implemented yet), 3=Diffuse parallel to boundary only
    !   boundit      - set .TRUE. to bound the fields by the input max and min values during diffusion smoothing
    !
    USE parallel
    USE share_consts
    USE wlt_trns_util_mod
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: nlocal, ne_local, j_sm_in, j_st_in
    REAL (pr), DIMENSION (nlocal,ne_local), INTENT(INOUT) :: smoothu
    INTEGER, INTENT(IN) :: methdiff, IDdiff, BCtype !1=Dirichlet, 2=Neumann, 3=Diffuse in parallel direction to boundary
    INTEGER, INTENT(IN) :: diffdir !0=diff all direction, 1,2,3=diff direction
    LOGICAL, INTENT(IN) :: boundit !TRUE to bound solution by initial max and min
    REAL(pr), INTENT(IN) :: Dfac_loc, delta_loc, deltadel_loc
    REAL (pr) :: diffdt, difft, curt, mydelta
    REAL (pr), DIMENSION (ne_local) :: umax,umin
    REAL (pr), DIMENSION (dim) :: delxyz_sm, delxyz_st
    REAL (pr), DIMENSION (ne_local,nlocal,dim) :: du
    REAL (pr), DIMENSION (ne_local,nlocal,dim) :: d2u
    INTEGER :: Nt, tstp, i, ii, j_sm, j_st
    INTEGER, PARAMETER :: meth=1
    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nlocal) :: iloc
    
!        PRINT *, 'diffsmooth: start'
        IF (boundit) THEN
          DO i=1,ne_local
             umax(i)=MAXVAL(smoothu(:,i))
             umin(i)=MINVAL(smoothu(:,i))
             CALL parallel_global_sum (REALMAXVAL=umax(i))
             CALL parallel_global_sum (REALMINVAL=umin(i))
          END DO
       END IF
       j_sm=j_sm_in
       j_st=j_st_in
       IF (j_sm==-1) j_sm=j_lev
       IF (j_sm==-2) j_sm=j_mx
       IF (j_sm==-3) j_sm=j_mx-1
       delxyz_st(1:dim)=(xyzlimits(2,1:dim)-xyzlimits(1,1:dim))/REAL(mxyz(1:dim)*2**(j_st-1),pr)  !delxyz_st = L/N in every direction on j_st (for stability)
       delxyz_sm(1:dim)=(xyzlimits(2,1:dim)-xyzlimits(1,1:dim))/REAL(mxyz(1:dim)*2**(j_sm-1),pr)  !delxyz_sm = L/N in every direction on j_sm (for smoothing)
       IF (diffdir .EQ. 0) THEN
          diffdt=Dfac_loc/2.0_pr/(SUM(1.0_pr/delxyz_st(:)**2))
       ELSE
          diffdt=Dfac_loc/2.0_pr*delxyz_st(diffdir)**2
       END IF
       mydelta=delta_loc
       IF (mydelta .LT. 0.0_pr) mydelta=deltadel_loc*delxyz_sm(1)
       difft=mydelta**2/4.0_pr/pi
       Nt=CEILING(difft/diffdt)
       tstp=0
       curt=0.0_pr
       DO WHILE(curt.lt.difft)
          curt=curt+diffdt
          tstp=tstp+1
          CALL c_diff_fast(smoothu(:,1:ne_local), du(1:ne_local,:,:), d2u(1:ne_local,:,:), j_lev, nlocal, methdiff, IDdiff, ne_local, 1, ne_local)

          DO i=1,ne_local
             i_p_face(0) = 1
             DO ii=1,dim
                i_p_face(ii) = i_p_face(ii-1)*3
             END DO
             DO face_type = 0, 3**dim - 1
                face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
                CALL get_all_indices_by_face (face_type, j_lev, nloc, iloc)
                IF(nloc > 0 ) THEN
                   IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
                      IF (BCtype .EQ. 3) THEN !diffuse parallel to boundaries
                         IF (diffdir .EQ. 0) THEN
                            smoothu(iloc(1:nloc),i)=smoothu(iloc(1:nloc),i)+diffdt*SUM(d2u(i,iloc(1:nloc),:),DIM=2)
                         ELSE
                            smoothu(iloc(1:nloc),i)=smoothu(iloc(1:nloc),i)+diffdt*d2u(i,iloc(1:nloc),diffdir)
                         END IF
                         DO ii=1,dim
                            IF ( (ABS(face(ii))==1) .AND. (diffdir==0 .OR. diffdir==ii) ) smoothu(iloc(1:nloc),i)=smoothu(iloc(1:nloc),i)-diffdt*d2u(i,iloc(1:nloc),ii)
                         END DO
                      ELSE IF (BCtype .EQ. 2) THEN !Nuemann BCs

                      END IF !IF BCtype==1, Dirichlet conditions require nothing to be done on boundaries
                   ELSE
                      IF (diffdir .EQ. 0) THEN
                         smoothu(iloc(1:nloc),i)=smoothu(iloc(1:nloc),i)+diffdt*SUM(d2u(i,iloc(1:nloc),:),DIM=2)
                      ELSE
                         smoothu(iloc(1:nloc),i)=smoothu(iloc(1:nloc),i)+diffdt*d2u(i,iloc(1:nloc),diffdir)
                      END IF
                   END IF
                END IF
             END DO
          END DO
          IF (boundit) THEN
             DO i=1,ne_local
                DO ii=1,nlocal
                   smoothu(ii,i) = MIN(umax(i),MAX(umin(i),smoothu(ii,i)))
                END DO
             END DO
          END IF
       END DO
!       PRINT *,'diffsmooth: finish',tstp,diffdt,curt,difft
  END SUBROUTINE diffsmooth

END MODULE util_mod
