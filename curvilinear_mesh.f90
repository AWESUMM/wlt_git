MODULE curvilinear_mesh 
  USE precision
  USE curvilinear
  USE wlt_vars!, ONLY: dim 
  USE sizes!, ONLY: nwlt 
  IMPLICIT NONE

  ! Protected variables
  INTEGER, ALLOCATABLE, PROTECTED                   :: n_map(:), Jij_map(:,:) 

  ! Public routines
  PUBLIC :: &
       curv_mesh_setup, &
       curv_mesh_finalize_setup, &
       curv_mesh_read_input, &
       curv_mesh_scales, &
       transform_vector_to_comp, &
       calc_mesh_stretching, &
       form_curvilinear_jacobian,  &
       form_linear_coordgrad,  &
       computational_dA,  &
       record_curvilinear_mapping, &
       record_Jij_mapping, &
       periodized_mapping, &
       unperiodize
!!$       setup_linear_mapping

  ! Public coordinate mapping helper functions 
  PUBLIC :: &
        channel_tanh, &
        channel_sinh, &
        BL_tanh, &
        cylindrical, &
        const_aspect_cylindrical, &
        spherical    

  REAL (pr), PRIVATE :: coordinate_adapt_scale
  REAL (pr), DIMENSION ( max_dim ), PRIVATE :: coordinate_adapt_scalevec
  LOGICAL, PRIVATE :: vec_coord_adapt
  LOGICAL, PRIVATE :: curv_low_asymmetry  


CONTAINS
 
  ! Read parameters from input files 
  SUBROUTINE  curv_mesh_read_input() 
    USE input_file_reader
    USE pde
    IMPLICIT NONE

    ! Note: transform_dir(k) = 0 if and only if no mapped coordinate exept  x_phys_k do not depend on x_comp_k and x_phys_k = x_comp_k
    transform_dir(:) = 0
    call input_integer_vector ('transform_dir',transform_dir,dim,'default', &
         '  transform_dir(:) (0/1) 0: uniform; 1: case-specified curvilinear mapping')

    coordinate_adapt_scale = 1.0_pr
    call input_real ('coordinate_adapt_scale',coordinate_adapt_scale,'default', &
         '  coordinate_adapt_scale: Scale for adapting on the coordinate map')

    coordinate_adapt_scalevec = 1.0_pr
    call input_real_vector ('coordinate_adapt_scalevec',coordinate_adapt_scalevec(1:dim),dim,'default', &
         '  coordinate_adapt_scalevec: Scale vector for adapting on the coordinate map')

    vec_coord_adapt = .FALSE. 
    call input_logical ('vec_coord_adapt ',vec_coord_adapt ,'default', &
         '  vec_coord_adapt: use vector scales for curvilinear coordinate adaptation')

    curv_low_asymmetry = .FALSE. 
    call input_logical ('curv_low_asymmetry ',curv_low_asymmetry ,'default', &
         '  curv_low_asymmetry : Use low boundary asymmetry for stencils in conjunction with curvilinear transforms')

  END SUBROUTINE curv_mesh_read_input 


  SUBROUTINE  curv_mesh_setup () 
    USE parallel
    USE variable_mapping 
    IMPLICIT NONE
    LOGICAL :: adapt_loc
    INTEGER :: i, j, k
    CHARACTER (LEN=3), DIMENSION(dim, dim) :: Jij_str

    
    IF (par_rank.EQ.0) THEN
       PRINT *, '************** Setting up Curvilinear Mesh function ****************'
    END IF

    ! If mapping in any direction are used, all are tracked for consistency
    ! Note: transform_dir(k) = 0 if and only if no mapped coordinate exept  x_phys_k do not depend on x_comp_k and x_phys_k = x_comp_k
    IF ( ANY( transform_dir(1:dim) .EQ. 1 ) ) THEN
      adapt_loc = transform_dir(1) .EQ. 1
      CALL register_var( 'XCoord  ', integrated=.FALSE., adapt=(/adapt_loc,adapt_loc/), interpolate=(/.TRUE.,.TRUE./), &
        exact=(/.FALSE.,.FALSE./), saved=.TRUE., req_restart=.TRUE. )

      adapt_loc = transform_dir(2) .EQ. 1
      IF( dim .GE. 2 ) CALL register_var( 'YCoord  ', integrated=.FALSE., adapt=(/adapt_loc,adapt_loc/), interpolate=(/.TRUE.,.TRUE./), &
        exact=(/.FALSE.,.FALSE./), saved=.TRUE., req_restart=.TRUE. )

      adapt_loc = transform_dir(3) .EQ. 1
      IF( dim .GE. 3 ) CALL register_var( 'ZCoord  ', integrated=.FALSE., adapt=(/adapt_loc,adapt_loc/), interpolate=(/.TRUE.,.TRUE./), &
        exact=(/.FALSE.,.FALSE./), saved=.TRUE., req_restart=.TRUE. )
     
    END IF

    IF (  ANY( transform_dir(1:dim) .EQ. 1 ) .AND. ANY( prd(1:dim) .EQ. 1 ) ) THEN
       DO j = 1, dim
          DO i = 1, dim
             WRITE(Jij_str(i,j),'("J",I1,I1)') i,j
          END DO
       END DO
       DO j = 1, dim
          DO i = 1, dim
             CALL register_var( Jij_str(j,i), integrated=.FALSE., adapt=(/.FALSE.,.FALSE./), interpolate=(/.TRUE.,.TRUE./), &
                  exact=(/.FALSE.,.FALSE./), saved=.TRUE., req_restart=.TRUE. )
          END DO
       END DO
    END IF

  END SUBROUTINE curv_mesh_setup 

  ! Called after all variables and mappings and counts are established
  SUBROUTINE curv_mesh_finalize_setup 
    USE variable_mapping 
    USE wlt_trns_vars
    IMPLICIT NONE
    INTEGER :: nd_assym_high, nd_assym_bnd_high, nd2_assym_high, nd2_assym_bnd_high, nd_assym_low, nd_assym_bnd_low, nd2_assym_low, nd2_assym_bnd_low
    INTEGER :: i, j
    CHARACTER (LEN=3), DIMENSION(dim, dim) :: Jij_str
    
    IF (ALLOCATED(n_map)) DEALLOCATE(n_map); ALLOCATE(n_map(dim))
    IF ( ANY( transform_dir(1:dim) .EQ. 1 ) ) THEN
      n_map(1) = get_index('XCoord')
      IF( dim .GE. 2 ) n_map(2) = get_index('YCoord')
      IF( dim .GE. 3 ) n_map(3) = get_index('ZCoord')

      IF (vec_coord_adapt) THEN
        scaleCoeff(n_map(1:dim)) = coordinate_adapt_scalevec(1:dim)
      ELSE 
        scaleCoeff(n_map(1:dim)) = coordinate_adapt_scale
      END IF

   END IF

    IF (ALLOCATED(Jij_map)) DEALLOCATE(Jij_map); ALLOCATE(Jij_map(dim,dim))
    IF ( ANY( transform_dir(1:dim) .EQ. 1 ) .AND. ANY( prd(1:dim) .EQ. 1 ) ) THEN
       DO j = 1, dim
          DO i = 1, dim
             WRITE(Jij_str(i,j),'("J",I1,I1)') i,j
          END DO
       END DO
       DO j = 1, dim
          DO i = 1, dim
             Jij_map(i,j) = get_index(Jij_str(i,j))
          END DO
       END DO
    END IF

   ! Set stencils asymmetry for stability on curvilinear meshes
   ! ERIC: this functionality should really be made into a central function of the trunk
   IF ( ANY( transform_dir(:) .EQ. 1 ) .AND. curv_low_asymmetry ) THEN
     !**************** Predefine maximum assymetry allowed
     !***********************************
     !CALL set_max_assymetry   ! from wlt_vars module

     nd_assym_high        = 0 !n_diff+1 !MAX(0,n_diff)!0 !1 !  Nd_assym_high
     nd_assym_bnd_high    = 1 !n_diff+1 !MAX(1,n_diff)!1 !1 ! Nd_assym_bnd_high                                 %
     nd2_assym_high       = 0 !n_diff+2 !MAX(0,n_diff)   !1 !  Nd2_assym_high
     nd2_assym_bnd_high   = 2 !n_diff+2 !MAX(2,n_diff)   !1 ! Nd2_assym_bnd_high                                %
     nd_assym_low         = 0 !2*n_diff +3 !0 !  Nd_assym_low
     nd_assym_bnd_low     = 1 !2*n_diff +3 !1 !  Nd_assym_bnd_low
     nd2_assym_low        = 0 !2*n_diff +3 !0 !  Nd2_assym_low
     nd2_assym_bnd_low    = 2 !2*n_diff +3 !2 !  Nd2_assym_bnd_low
     !**********************************************************************************

     nd_assym = (/nd_assym_low,nd_assym_high,nd_assym_low,nd_assym_high,nd_assym_low,nd_assym_high/)
     nd_assym_bnd = (/nd_assym_bnd_low,nd_assym_bnd_high,nd_assym_bnd_low,nd_assym_bnd_high,nd_assym_bnd_low,nd_assym_bnd_high/)
     nd2_assym = (/nd_assym_low,nd2_assym_high,nd_assym_low,nd2_assym_high,nd_assym_low,nd2_assym_high/)
     nd2_assym_bnd = (/nd2_assym_bnd_low,nd2_assym_bnd_high,nd2_assym_bnd_low,nd2_assym_bnd_high,nd2_assym_bnd_low,nd2_assym_bnd_high/)

     DO i = 0, n_trnsf_type !0 - regular, 1 - inernal
       n_assym_prdct(:,i)     = 0 !  N_assym_prdct for regualr wavelet transform       %
       n_assym_prdct_bnd(:,i) = 1 !  N_assym_prdct_bnd for regualr wavelet transform   %
       n_assym_updt(:,i)      = 0 !  N_assym_updt for regualr wavelet transform        %
       n_assym_updt_bnd(:,i)  = 1 !  N_assym_updt_bnd for regualr wavelet transform    %
     END DO
   END IF

  END SUBROUTINE curv_mesh_finalize_setup 

  !
  !************ Calculating Scales ***************************
  !
  ! Note that all of the variables should be passed in 
  ! 
  SUBROUTINE curv_mesh_scales(flag,use_default, u_loc, nlocal, ne_local, l_n_var_adapt , l_n_var_adapt_index, &
       scl, scl_fltwt) !add
    USE variable_mapping
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: flag ! 0 during initial adaptation, then 1 during time advancement
    LOGICAL , INTENT(INOUT) :: use_default
    INTEGER, INTENT (IN) :: nlocal, ne_local
    REAL (pr), DIMENSION (1:nlocal,1:ne_local), INTENT (IN) :: u_loc
    LOGICAL , INTENT (IN) :: l_n_var_adapt(ne_local)
    INTEGER , INTENT (IN) :: l_n_var_adapt_index(1:ne_local)
    REAL (pr), DIMENSION (1:ne_local), INTENT (INOUT) :: scl
    REAL (pr) ,  INTENT(IN) ::scl_fltwt !weight for temporal filter on scl
    INTEGER :: i, ie

    !
    ! Ignore the output of this routine and use default scales routine
    !
    use_default = .FALSE. 

    IF( .NOT. use_default .AND.ANY( transform_dir(1:dim) .EQ. 1 ) ) THEN
       scl(n_map(1:dim)) =  scaleCoeff(n_map(1:dim))

       IF (par_rank.EQ.0) THEN
          WRITE(*,'("*********** CURVILINEAR MAPPING SCALES *******************")')
          DO ie=1,dim
             WRITE(*,'("***", A,E12.5,"        ***")') u_variable_names(n_map(ie)), scl(n_map(ie))
          END DO
          WRITE(*,'("**********************************************************")')
       END IF
    END IF

  END SUBROUTINE curv_mesh_scales

  ! Transform a vector from physical space to computational space
  FUNCTION transform_vector_to_comp( vector )
    IMPLICIT NONE
    REAL (pr), DIMENSION(1:nwlt,1:dim), INTENT (IN) :: vector
    REAL (pr), DIMENSION(1:nwlt,1:dim)              :: transform_vector_to_comp
    INTEGER :: i, imap

    IF ( ANY( transform_dir(1:dim) .EQ. 1 ) ) THEN
       DO imap = 1, dim
          DO i =1, nwlt
             transform_vector_to_comp(i,imap) = SUM(curvilinear_jacobian(imap,1:dim,i) * vector(i,1:dim) )
          END DO
       END DO
    ELSE
       transform_vector_to_comp(1:nwlt,1:dim) = vector(1:nwlt,1:dim)
    END IF

  END FUNCTION transform_vector_to_comp

  ! Calculate velocity in curvilinear space and mesh sthretching
  ! estimate_type : 0 - in physical space, 1 - in computational space
  SUBROUTINE calc_mesh_stretching( vector_comp, stretch, vector, estimate_type )
    USE precision
    USE wlt_vars!, ONLY: dim 
    USE wlt_trns_mod 
    USE parallel 
    USE curvilinear
    USE error_handling 
    IMPLICIT NONE
    REAL (pr), DIMENSION(1:nwlt,1:dim), INTENT (IN)    :: vector
    REAL (pr), DIMENSION(1:nwlt,1:dim), INTENT (INOUT) :: vector_comp, stretch
    INTEGER, INTENT(IN) :: estimate_type
    INTEGER :: i, imap

    IF ( ANY( transform_dir(1:dim) .EQ. 1 ) ) THEN
       IF(estimate_type .EQ. 1) THEN
          DO imap = 1, dim
             DO i =1, nwlt
                vector_comp(i,imap) = SUM(curvilinear_jacobian(imap,1:dim,i) * vector(i,1:dim) )
             END DO
             stretch(:,imap) = SQRT( SUM(curvilinear_jacobian(imap,1:dim,:)**2, DIM=1 ) )
          END DO
       ELSE
          DO imap = 1, dim
             DO i =1, nwlt
                vector_comp(i,imap) = SUM(curvilinear_coordgrad(1:dim,imap,i) * vector(i,1:dim) )
             END DO
             stretch(:,imap) = 1.0_pr / SQRT( SUM(curvilinear_coordgrad(1:dim,imap,:)**2, DIM=1 ) )
          END DO
          vector_comp(:,:) = vector_comp(:,:) * stretch(:,:)**2
       END IF
    ELSE
       vector_comp(1:nwlt,1:dim) = vector(1:nwlt,1:dim)
       stretch(1:nwlt,1:dim) = 1.0_pr
    END IF

  END SUBROUTINE calc_mesh_stretching

  ! Calculates the inverse transform jacobian tensor [ d xi_i/d x_j ] for the user_case specified coordinate mapping 
  ! OUTPUT: curvilinear_jacobian(i,j,1:nwlt) for d xi_i/d x_j
  SUBROUTINE form_curvilinear_jacobian( x_comp, nlocal, t_local, periodized, mapping_function)
    USE precision
    USE wlt_vars!, ONLY: dim 
    USE wlt_trns_mod 
    USE parallel 
    USE curvilinear
    USE error_handling 
    IMPLICIT NONE
    LOGICAL :: periodized
    INTEGER, INTENT(IN) :: nlocal
    REAL (pr), INTENT(IN) :: t_local
    REAL (pr), DIMENSION(1:nlocal,1:dim), INTENT(IN) :: x_comp
    REAL (pr), DIMENSION(1:nlocal,1:dim)             :: x_phys, x_aux
    INTEGER :: idim, i, j, k
    REAL (pr), DIMENSION(1:dim,1:nlocal,1:dim) :: du, du_aux, du_dummy
    REAL (pr), DIMENSION(1:nlocal)             :: jacobian_det 
    INTEGER :: flag
    INTERFACE
       FUNCTION mapping_function ( xlocal, nlocal, t_local )
         USE curvilinear
         USE wlt_vars!, ONLY: dim 
         IMPLICIT NONE
         INTEGER, INTENT (IN) :: nlocal
         REAL (pr), INTENT (IN) :: t_local
         REAL (pr), DIMENSION (nlocal,dim), INTENT (IN) :: xlocal
         REAL (pr), DIMENSION (nlocal,dim) :: mapping_function
       END FUNCTION mapping_function
    END INTERFACE
   
    x_phys = mapping_function( x_comp, nlocal, t_local )

    IF(periodized) THEN
       curvilinear_coordgrad = form_linear_coordgrad( x_comp, nlocal, t_local, mapping_function ) ! cordinate gradient for periodic correction
       DO j = 1, dim
          IF(prd(j) .EQ. 1) THEN
             DO i = 1, dim
                x_phys(:,i)   = x_phys(:,i) - curvilinear_coordgrad(i,j,:) * ( x_comp(:,j) - xyzlimits(1,j) ) 
             END DO
          END IF
       END DO
    END IF

    CALL c_diff_fast (x_phys(1:nlocal, 1:dim), du(1:dim,1:nlocal,1:dim), du_dummy, j_lev, nlocal, HIGH_ORDER, 10, dim, 1, dim, FORCE_RECTILINEAR = .TRUE.)

    DO j = 1, dim
       IF( ( prd(j) .EQ. 1 ) .AND. periodized ) THEN! account for non-transformed periodic corrections
          !coorection for nonlinear mapping in non-periodic direction
          DO i = 1, dim
             x_aux(:,i)   = curvilinear_coordgrad(i,j,:)
          END DO
          
          CALL c_diff_fast (x_aux(1:nlocal, 1:dim), du_aux(1:dim,1:nlocal,1:dim), du_dummy, j_lev, nlocal, HIGH_ORDER, 10, dim, 1, dim, FORCE_RECTILINEAR = .TRUE.)

          DO k = 1, dim
             IF(k .NE. j) THEN
                DO i = 1, dim
                   du(i,:,k) = du(i,:,k) + du_aux(i,:,k) * ( x_comp(:,j) - xyzlimits(1,j) )
                END DO
             END IF
          END DO
       END IF
    END DO
    DO j = 1, dim
       DO i = 1, dim
          curvilinear_coordgrad(i,j,:) = curvilinear_coordgrad(i,j,:) + du(i,:,j)
       END DO
    END DO
    
    ! overriding non-transfromed directions
    DO j = 1, dim
       IF( transform_dir(j) .EQ. 0 ) THEN  ! account for non-transformed directions, regardless of periodicity
          curvilinear_coordgrad(j,1:dim,:) = 0.0_pr
          curvilinear_coordgrad(j,j,:)  = 1.0_pr
       END IF
    END DO

    IF( dim .EQ. 3) THEN
      jacobian_det(:) = curvilinear_coordgrad(1,1,:) * ( curvilinear_coordgrad(2,2,:) * curvilinear_coordgrad(3,3,:) - curvilinear_coordgrad(2,3,:) * curvilinear_coordgrad(3,2,:) ) &
                      - curvilinear_coordgrad(1,2,:) * ( curvilinear_coordgrad(2,1,:) * curvilinear_coordgrad(3,3,:) - curvilinear_coordgrad(2,3,:) * curvilinear_coordgrad(3,1,:) ) &
                      + curvilinear_coordgrad(1,3,:) * ( curvilinear_coordgrad(2,1,:) * curvilinear_coordgrad(3,2,:) - curvilinear_coordgrad(2,2,:) * curvilinear_coordgrad(3,1,:) ) 
      flag = 0
      IF ( MINVAL( ABS( jacobian_det ) ) .LT. 1.0e-10_pr ) flag = 1
      CALL parallel_global_sum( INTEGERMAXVAL= flag )
      IF ( flag .EQ. 1 ) THEN
        CALL error('Transform Jacobian Determinant is zero.')
      ELSE
        jacobian_det(:) = 1.0_pr/jacobian_det(:) 
      END IF

      curvilinear_jacobian(1,1,:) =   jacobian_det(:) * (curvilinear_coordgrad(2,2,:)*curvilinear_coordgrad(3,3,:) - curvilinear_coordgrad(2,3,:)*curvilinear_coordgrad(3,2,:)) ! d xi / dx
      curvilinear_jacobian(2,1,:) = - jacobian_det(:) * (curvilinear_coordgrad(2,1,:)*curvilinear_coordgrad(3,3,:) - curvilinear_coordgrad(2,3,:)*curvilinear_coordgrad(3,1,:)) ! d eta / dx
      curvilinear_jacobian(3,1,:) =   jacobian_det(:) * (curvilinear_coordgrad(2,1,:)*curvilinear_coordgrad(3,2,:) - curvilinear_coordgrad(2,2,:)*curvilinear_coordgrad(3,1,:)) ! d zeta / dx

      curvilinear_jacobian(1,2,:) = - jacobian_det(:) * (curvilinear_coordgrad(1,2,:)*curvilinear_coordgrad(3,3,:) - curvilinear_coordgrad(1,3,:)*curvilinear_coordgrad(3,2,:)) ! d xi / dy
      curvilinear_jacobian(2,2,:) =   jacobian_det(:) * (curvilinear_coordgrad(1,1,:)*curvilinear_coordgrad(3,3,:) - curvilinear_coordgrad(1,3,:)*curvilinear_coordgrad(3,1,:)) ! d eta / dy
      curvilinear_jacobian(3,2,:) = - jacobian_det(:) * (curvilinear_coordgrad(1,1,:)*curvilinear_coordgrad(3,2,:) - curvilinear_coordgrad(1,2,:)*curvilinear_coordgrad(3,1,:)) ! d zeta / dy

      curvilinear_jacobian(1,3,:) =   jacobian_det(:) * (curvilinear_coordgrad(1,2,:)*curvilinear_coordgrad(2,3,:) - curvilinear_coordgrad(1,3,:)*curvilinear_coordgrad(2,2,:)) ! d xi / dz
      curvilinear_jacobian(2,3,:) = - jacobian_det(:) * (curvilinear_coordgrad(1,1,:)*curvilinear_coordgrad(2,3,:) - curvilinear_coordgrad(1,3,:)*curvilinear_coordgrad(2,1,:)) ! d eta / dz
      curvilinear_jacobian(3,3,:) =   jacobian_det(:) * (curvilinear_coordgrad(1,1,:)*curvilinear_coordgrad(2,2,:) - curvilinear_coordgrad(1,2,:)*curvilinear_coordgrad(2,1,:)) ! d zeta / dz
           
    ELSE IF( dim .EQ. 2) THEN

      jacobian_det(:) =   curvilinear_coordgrad(1,1,:) * curvilinear_coordgrad(2,2,:) &
                        - curvilinear_coordgrad(1,2,:) * curvilinear_coordgrad(2,1,:) 
      flag = 0
      IF ( MINVAL( ABS( jacobian_det ) ) .LT. 1.0e-10_pr ) flag = 1
      CALL parallel_global_sum( INTEGERMAXVAL= flag )
      IF ( flag .EQ. 1 ) THEN
        CALL error('Transform Jacobian Determinant is zero.')
      ELSE
        jacobian_det(:) = 1.0_pr/jacobian_det(:) 
      END IF

      curvilinear_jacobian(1,1,:) =   jacobian_det(:) * curvilinear_coordgrad(2,2,:) ! d xi / dx 
      curvilinear_jacobian(2,1,:) = - jacobian_det(:) * curvilinear_coordgrad(2,1,:) ! d eta / dx

      curvilinear_jacobian(1,2,:) = - jacobian_det(:) * curvilinear_coordgrad(1,2,:) ! d xi / dy
      curvilinear_jacobian(2,2,:) =   jacobian_det(:) * curvilinear_coordgrad(1,1,:) ! d eta / dy

    ELSE IF( dim .EQ. 1) THEN
      flag = 0
      IF ( MINVAL( ABS( curvilinear_coordgrad(1,1,:) ) ) .LT. 1.0e-10_pr ) flag = 1
      CALL parallel_global_sum( INTEGERMAXVAL= flag )
      IF ( flag .EQ. 1 ) THEN
        CALL error('Transform Jacobian Determinant is zero.')
      ELSE
        curvilinear_jacobian(1,1,:) =    1.0_pr/curvilinear_coordgrad(1,1,:)  
      END IF
    ELSE
      CALL error( 'UNKNOWN DIMENSIONALITY (CURVILINEAR JACOBIAN).' )
    END IF
   
  END SUBROUTINE form_curvilinear_jacobian

  FUNCTION form_linear_coordgrad( x_comp, nlocal, t_local, mapping_function)
    USE precision
    USE wlt_trns_mod 
    USE parallel 
    USE curvilinear
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: nlocal
    INTEGER :: idim, i, j
    REAL (pr), INTENT(IN) :: t_local
    REAL (pr), DIMENSION(1:nlocal,1:dim), INTENT(IN)  :: x_comp
    REAL (pr), DIMENSION(1:nlocal,1:dim)              :: x_low, x_high
    REAL (pr), DIMENSION(1:dim,1:dim,1:nlocal)        :: form_linear_coordgrad
    INTERFACE
       FUNCTION mapping_function ( xlocal, nlocal, t_local )
         USE curvilinear
         USE wlt_vars!, ONLY: dim 
         IMPLICIT NONE
         INTEGER, INTENT (IN) :: nlocal
         REAL (pr), INTENT (IN) :: t_local
         REAL (pr), DIMENSION (nlocal,dim), INTENT (IN) :: xlocal
         REAL (pr), DIMENSION (nlocal,dim) :: mapping_function
       END FUNCTION mapping_function
    END INTERFACE

    form_linear_coordgrad = 0.0_pr    
    IF ( ANY( transform_dir(1:dim) .EQ. 1 ) .AND. ANY( prd(1:dim) .EQ. 1 ) ) THEN
       DO j = 1, dim
          IF(prd(j) .EQ. 1) THEN
             x_low       = x_comp
             x_high      = x_comp
             x_low(:,j)  = xyzlimits(1,j)
             x_high(:,j) = xyzlimits(2,j)
             x_low  = mapping_function ( x_low, nlocal, t_local )
             x_high = mapping_function ( x_high, nlocal, t_local )
             DO i = 1, dim
                form_linear_coordgrad(i,j,:) = ( x_high(:,i) - x_low(:,i) ) / ( xyzlimits(2,j)-xyzlimits(1,j) )
             END DO
          END IF
       END DO
    END IF

  END FUNCTION form_linear_coordgrad

  FUNCTION  periodized_mapping( x_comp, nlocal, t_local, mapping_function )
    USE precision
    USE wlt_trns_mod 
    USE parallel 
    USE curvilinear
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: nlocal
    INTEGER :: idim, i, j
    REAL (pr) :: t_local
    REAL (pr), DIMENSION(1:nlocal,1:dim), INTENT(IN)  :: x_comp
    REAL (pr), DIMENSION(1:nlocal,1:dim)              :: periodized_mapping
    REAL (pr), DIMENSION(1:dim,1:dim,1:nlocal)        :: Jij_linear
    INTERFACE
       FUNCTION mapping_function ( xlocal, nlocal, t_local )
         USE curvilinear
         USE wlt_vars!, ONLY: dim 
         IMPLICIT NONE
         INTEGER, INTENT (IN) :: nlocal
         REAL (pr), INTENT (IN) :: t_local
         REAL (pr), DIMENSION (nlocal,dim), INTENT (IN) :: xlocal
         REAL (pr), DIMENSION (nlocal,dim) :: mapping_function
       END FUNCTION mapping_function
    END INTERFACE

    periodized_mapping = mapping_function( x_comp, nlocal, t_local )

    IF ( ANY( transform_dir(1:dim) .EQ. 1 ) .AND. ANY( prd(1:dim) .EQ. 1 ) ) THEN

       Jij_linear = form_linear_coordgrad( x_comp, nlocal, t_local, mapping_function ) ! temporary jacobian for periodic correction
    
       DO j = 1, dim
          IF(prd(j) .EQ. 1) THEN
             DO i = 1, dim
                periodized_mapping(:,i)   = periodized_mapping(:,i) -  Jij_linear(i,j,:) * ( x_comp(:,j) - xyzlimits(1,j) ) 
             END DO
          END IF
       END DO
    END IF

  END FUNCTION periodized_mapping


FUNCTION  unperiodize( x_phys_periodized, x_comp, nlocal )
    USE precision
    USE wlt_trns_mod 
    USE parallel 
    USE curvilinear
    USE field
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: nlocal
    INTEGER :: i, j
    REAL (pr), DIMENSION(1:nlocal,1:dim), INTENT(IN)  :: x_phys_periodized, x_comp
    REAL (pr), DIMENSION(1:nlocal,1:dim)              :: unperiodize
    INTERFACE
       FUNCTION mapping_function ( xlocal, nlocal, t_local )
         USE curvilinear
         USE wlt_vars!, ONLY: dim 
         IMPLICIT NONE
         INTEGER, INTENT (IN) :: nlocal
         REAL (pr), INTENT (IN) :: t_local
         REAL (pr), DIMENSION (nlocal,dim), INTENT (IN) :: xlocal
         REAL (pr), DIMENSION (nlocal,dim) :: mapping_function
       END FUNCTION mapping_function
    END INTERFACE

    unperiodize = x_phys_periodized

    IF ( ANY( transform_dir(1:dim) .EQ. 1 ) .AND. ANY( prd(1:dim) .EQ. 1 ) ) THEN
       DO j = 1, dim
          IF(prd(j) .EQ. 1) THEN
             DO i = 1, dim
                unperiodize(:,i)   = unperiodize(:,i) +  u(:,Jij_map(i,j)) * ( x_comp(:,j) - xyzlimits(1,j) ) 
             END DO
          END IF
       END DO
    END IF

  END FUNCTION unperiodize


  ! Helper function to compute the computational space weights, dA, with checks
  FUNCTION computational_dA( nlocal )
    USE precision
    USE wlt_trns_mod 
    USE parallel 
    USE curvilinear
    USE util_vars
    USE error_handling 
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: nlocal
    REAL (pr), DIMENSION(1:nlocal) :: computational_dA
   
    IF ( ANY( transform_dir(1:dim) .EQ. 1 ) ) THEN
      computational_dA(:) = dA(:)*calc_jacobian_determinant( curvilinear_jacobian(1:dim,1:dim,1:nlocal), dim, nlocal)
    ELSE
      computational_dA(:) = dA(:)
    END IF
   
  END FUNCTION computational_dA


  ! Routine to record mapping to the u_vector
  SUBROUTINE record_curvilinear_mapping( x_phys )
    USE precision
    USE curvilinear
    USE field
    IMPLICIT NONE
    REAL (pr), DIMENSION(1:nwlt,1:dim), INTENT(IN)       :: x_phys 

    IF ( ANY( transform_dir(1:dim) .EQ. 1 ) ) u(:,n_map(1:dim)) = x_phys

  END SUBROUTINE record_curvilinear_mapping

  ! Routine to record mapping to the u_vector
  SUBROUTINE record_Jij_mapping( Jij )
    USE precision
    USE curvilinear
    USE field
    IMPLICIT NONE
    REAL (pr), DIMENSION(1:dim,1:dim,1:nwlt), INTENT(IN)       :: Jij 
    INTEGER :: i, j

    IF ( ANY( transform_dir(1:dim) .EQ. 1 ) .AND. ANY( prd(1:dim) .EQ. 1 ) ) THEN
       DO j = 1, dim
          DO i = 1, dim
             u(:,Jij_map(i,j)) = Jij(i,j,:)
          END DO
       END DO
    END IF

  END SUBROUTINE record_Jij_mapping

  ! Mapping library
  ! These are prepackaged mappings for easy implementations in user case.
  ! Since they are helper functions that do not access global variables, they can be called successively to compound algebraic transforms
  ! Warning: there are no checks for proper use of transform_dir.  This must be performed by the user

  ! A Tanh function for channel flow
  FUNCTION channel_tanh( x_comp, nlocal, mesh_cluster_param, stretch_dir )
    USE parallel
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: nlocal, stretch_dir
    REAL (pr), DIMENSION(1:nlocal,1:dim), INTENT(IN) :: x_comp   ! Current coordinates in
    REAL (pr), INTENT(IN) :: mesh_cluster_param
    REAL (pr), DIMENSION(1:nlocal,1:dim) :: channel_tanh         ! Mapped coordinates output
    REAL (pr) :: R1, R2, length
    INTEGER :: i
    
    ! Set up to normalize against 
    R1 = MINVAL( x_comp(:,stretch_dir) )
    R2 = MAXVAL( x_comp(:,stretch_dir) )
    CALL parallel_global_sum(REALMINVAL=R1)
    CALL parallel_global_sum(REALMAXVAL=R2)
    length = R2 - R1

    channel_tanh = x_comp
    IF ( dim .GE. stretch_dir ) THEN 
       channel_tanh(:,stretch_dir) = 0.5_pr + 0.5_pr * TANH( mesh_cluster_param * ( ( x_comp(:,stretch_dir) - R1 ) / length - 0.5_pr  ) ) / ( TANH( 0.5_pr * mesh_cluster_param  ) + 1.0e-10_pr ) 
       channel_tanh(:,stretch_dir) = channel_tanh(:,stretch_dir) * length + R1 
    END IF

  END FUNCTION channel_tanh

  ! A Sinh function for channel flow
  FUNCTION channel_sinh( x_comp, nlocal, mesh_cluster_param, stretch_dir )
    USE parallel
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: nlocal, stretch_dir
    REAL (pr), DIMENSION(1:nlocal,1:dim), INTENT(IN) :: x_comp   ! Current coordinates in
    REAL (pr), INTENT(IN) :: mesh_cluster_param
    REAL (pr), DIMENSION(1:nlocal,1:dim) :: channel_sinh         ! Mapped coordinates output
    REAL (pr) :: R1, R2, length

    ! Set up to normalize against 
    R1 = MINVAL( x_comp(:,stretch_dir) )
    R2 = MAXVAL( x_comp(:,stretch_dir) )
    CALL parallel_global_sum(REALMINVAL=R1)
    CALL parallel_global_sum(REALMAXVAL=R2)
    length = R2 - R1

    channel_sinh = x_comp
    IF ( dim .GE. stretch_dir ) THEN 
       channel_sinh(:,stretch_dir) = 0.5_pr + 0.5_pr * SINH( mesh_cluster_param * ( ( x_comp(:,stretch_dir) - R1 ) / length - 0.5_pr  ) ) / ( SINH( 0.5_pr * mesh_cluster_param  ) + 1.0e-10_pr ) 
       channel_sinh(:,stretch_dir) = channel_sinh(:,stretch_dir) * length + R1 
    END IF

  END FUNCTION channel_sinh


  ! A Single-sided Tanh function for boundary layers 
  FUNCTION BL_tanh( x_comp, nlocal, mesh_cluster_param, stretch_dir, bnd )
    USE parallel
    IMPLICIT NONE
    INTEGER, INTENT(IN) ::  nlocal, &         ! Number of points
                            stretch_dir, &    ! Direction to stretch in
                            bnd               ! Boundary: 1 = min, 2 = max
    REAL (pr), DIMENSION(1:nlocal,1:dim), INTENT(IN) :: x_comp   ! Current coordinates in
    REAL (pr), INTENT(IN) :: mesh_cluster_param
    REAL (pr), DIMENSION(1:nlocal,1:dim) :: BL_tanh         ! Mapped coordinates output

    REAL (pr) :: R1, R2, length

    ! Set up to normalize against 
    R1 = MINVAL( x_comp(:,stretch_dir) )
    R2 = MAXVAL( x_comp(:,stretch_dir) )
    CALL parallel_global_sum(REALMINVAL=R1)
    CALL parallel_global_sum(REALMAXVAL=R2)
    length = R2 - R1

    BL_tanh = x_comp
    IF ( dim .GE. stretch_dir ) THEN 

       IF ( bnd .EQ. 1 ) THEN
          BL_tanh(:,stretch_dir) = 1.0_pr * ( 1.0_pr + TANH( mesh_cluster_param * ( ( BL_tanh(:,stretch_dir) - R1 )/length - 1.0_pr ) ) / ( TANH( mesh_cluster_param )+ 1.0e-10_pr ) )
          BL_tanh(:,stretch_dir) = BL_tanh(:,stretch_dir) * length + R1 
       ELSE IF ( bnd .EQ. 2 ) THEN
          BL_tanh(:,stretch_dir) = 1.0_pr * ( 1.0_pr + TANH( mesh_cluster_param * ( ( R2 - BL_tanh(:,stretch_dir) )/length - 1.0_pr ) ) / ( TANH( mesh_cluster_param )+ 1.0e-10_pr ) )
          BL_tanh(:,stretch_dir) = R2 - BL_tanh(:,stretch_dir) * length
       END IF

    END IF

  END FUNCTION BL_tanh

  ! x_computational -> radius
  ! y_computational -> theta 
  FUNCTION cylindrical( x_comp, nlocal )
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: nlocal
    REAL (pr), DIMENSION(1:nlocal,1:dim), INTENT(IN) :: x_comp   ! Current coordinates in
    REAL (pr), DIMENSION(1:nlocal,1:dim) :: cylindrical ! Mapped coordinates output

    cylindrical = x_comp
    IF ( dim .GE. 2 ) THEN 
       cylindrical(:,1) = x_comp(:,1) * COS(x_comp(:,2)) 
       cylindrical(:,2) = x_comp(:,1) * SIN(x_comp(:,2)) 
    END IF

  END FUNCTION cylindrical 

  ! x_computational -> radius
  ! y_computational -> theta 
  FUNCTION const_aspect_cylindrical( x_comp, nlocal )
    USE parallel
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: nlocal
    REAL (pr), DIMENSION(1:nlocal,1:dim), INTENT(IN) :: x_comp   ! Current coordinates in
    REAL (pr), DIMENSION(1:nlocal,1:dim) :: const_aspect_cylindrical ! Mapped coordinates output

    REAL (pr) :: R1, R2

    IF ( dim .GE. 2 ) THEN 
      R1 = MINVAL( x_comp(:,1) )
      R2 = MAXVAL( x_comp(:,1) )
      CALL parallel_global_sum(REALMINVAL=R1)
      CALL parallel_global_sum(REALMAXVAL=R2)

      ! Stretching to optimize aspect ratio towards 1 everywhere 
      const_aspect_cylindrical = x_comp
      const_aspect_cylindrical(:,1) = R1 * exp( ( x_comp(:,1)-R1 )/( R2-R1) * log(R2/R1) ) 
      const_aspect_cylindrical(:,:) = cylindrical( const_aspect_cylindrical(:,1:dim), nlocal ) 
    END IF

  END FUNCTION const_aspect_cylindrical 

  ! x_computational -> radius
  ! y_computational -> phi 
  ! y_computational -> theta 
  FUNCTION spherical( x_comp, nlocal )
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: nlocal
    REAL (pr), DIMENSION(1:nlocal,1:dim), INTENT(IN) :: x_comp   ! Current coordinates in
    REAL (pr), DIMENSION(1:nlocal,1:dim) :: spherical ! Mapped coordinates output

    spherical = x_comp
    IF ( dim .GE. 3 ) THEN 
       spherical(:,1) = x_comp(:,1) * COS(x_comp(:,2)) * SIN(x_comp(:,3)) 
       spherical(:,2) = x_comp(:,1) * SIN(x_comp(:,2)) * SIN(x_comp(:,3))
       spherical(:,3) = x_comp(:,1) * COS(x_comp(:,3)) 
    END IF

  END FUNCTION spherical

END MODULE curvilinear_mesh 
