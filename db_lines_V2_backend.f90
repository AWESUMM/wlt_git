!  Lines database (fortran version 3) 
!  by Alexei Vezolainen
!  April 2005
!  modified by Daniel Goldstein 

!
! if this cpp variable is set then the face%number_of_nodes_of_each_level
! array is used
!X! #define USE_number_of_nodes_of_each_level

MODULE lines_database
  USE precision
  USE debug_vars
  USE db_lines_vars
  USE user_case_db
  IMPLICIT NONE

  !-----------------------------------------------------------------
  ! PUBLIC STRUCTURE OF THE DATABASE
  !-----------------------------------------------------------------
  PUBLIC :: init_lines_db_backend, &       ! initialize the database
       finalize_lines_db_backend, &        ! destroy the database
       add_node_lines_db_backend, &        ! add the node to the database
       get_active_lines_indx_backend , &   ! return array of active elements_of_face
       get_1_backend, &                    ! returns one pointer to a node
       delete_some_backend, &              ! deletes nodes with a specific flag
       info_backend                        ! template of a subroutine for verification purposes

!unused for now       getp_active_lines_indx_backend , &  ! ... and perform shared memory partitioning



  !-----------------------------------------------------------------
  ! PRIVATE STRUCTURE OF THE DATABASE
  !-----------------------------------------------------------------
  TYPE, PRIVATE :: face
     INTEGER :: pdim                                ! dimension, orthogonal to the face
     INTEGER :: odim(max_dim-1)                     ! other dimensions but pdim
     TYPE (element_of_face), POINTER :: line(:) => NULL() ! array of lines (the face itself)
     INTEGER :: line_size                           !  ... size of it
  END TYPE face

  INTEGER, PRIVATE :: DIM_NUM                       ! dimension of the domain
  INTEGER, PRIVATE :: VAL_NUM                       ! number of real function values
  INTEGER, PRIVATE :: VAL_NUM_I                     ! number of integer function values
  INTEGER, PRIVATE :: J_MAX                         ! maximum level
  INTEGER, PRIVATE :: J_MIN                         ! minumum level of initialization
  INTEGER, PRIVATE :: BND_FLAG                      ! location of boundary flag in flags bit mask
  INTEGER, PRIVATE, ALLOCATABLE :: FINEST_MESH (:)  ! m_x * 2^{J_max-1}
  INTEGER, PRIVATE, ALLOCATABLE :: PERIODIC (:)     ! periodic boundary conditions marker

  TYPE (face), PRIVATE, ALLOCATABLE :: faces(:)     ! array of faces of size dim, the database itself

  INTEGER, PRIVATE, ALLOCATABLE :: products(:)      ! 1, Nx, Ny*Nz, etc
!  INTEGER, PRIVATE              :: face_type_internal_db !face that is internal pts
  LOGICAL, PRIVATE, PARAMETER :: verb = .FALSE.     ! verbouse initializations flag
  CHARACTER*1, PRIVATE :: cdim                      ! string representation of DIM_NUM
  PRIVATE :: initialize_the_face_for_dim, & !
       calc_the_real_level_of_node, &        !
       add_node_level_finest_coord, &            !
       array_index, &                                 !
       add_node, &                                    !
       delete_node, &                               ! memory management
       allocate_node                                ! ...

CONTAINS
  !-------------------------------------------------------------------------------------------------------------
  ! PUBLIC STRUCTURE OF THE DATABASE
  !-------------------------------------------------------------------------------------------------------------
  FUNCTION init_lines_db_backend (add_pts, m, p, jmin, jmax, fnum, ifnum, dim, init_flag, init_BND_flag, &
      j_deriv_lev_init) !,face_type_internal)
    INTEGER, INTENT(IN) :: add_pts            ! flag to control if we add points
    INTEGER, INTENT(IN) :: dim                ! dimension
    INTEGER, INTENT(IN) :: m(dim)             ! grid at the level 1
    INTEGER, INTENT(IN) :: p(dim)             ! periodic (1), non-periodic (0)
    INTEGER, INTENT(IN) :: jmin               ! minimum level of initialization
    INTEGER, INTENT(IN) :: jmax               ! maximum level
    INTEGER, INTENT(IN) :: fnum, ifnum        ! number of double and integer values inside a node
    INTEGER(KIND_INT_FLAGS), INTENT(IN) :: init_flag          ! initial flag value for initial points !ADDED DG
    INTEGER, INTENT(IN) :: init_BND_flag      ! initial location of boundary flag in flags bit mask !ADDED DG
!	INTEGER, INTENT(IN) :: face_type_internal !face that is internal pts 
    INTEGER :: factor, i, ierr, j, i_tmp
    INTEGER :: coord(dim), level
	INTEGER :: j_deriv_lev_init               ! in initial j_deriv_lev until we actually calculate it
    INTEGER :: init_lines_db_backend          ! returns # of points added
    !INTEGER :: use_init_flag                  ! internal or bnd init flags

    IF (ALLOCATED(FINEST_MESH)) DEALLOCATE (FINEST_MESH)
    IF (ALLOCATED(PERIODIC)) DEALLOCATE (PERIODIC)
    ALLOCATE (FINEST_MESH(dim), PERIODIC(dim), STAT=ierr)          ! initialize globals
    IF (ierr.NE.0) THEN
       WRITE (*,'(A)') 'ERROR: allocating FINEST_MESH(:) and PERIODIC(:)'
       STOP
    END IF
    DIM_NUM = dim
    VAL_NUM = fnum
    VAL_NUM_I = ifnum
    J_MIN = jmin
    J_MAX = jmax
    BND_FLAG = init_BND_flag
    PERIODIC (:) = p(:)
    FINEST_MESH (:) =  m(:)*2**(jmax-1)
!    face_type_internal_db = face_type_internal

    IF (verb) THEN
       WRITE( *, '("init_lines_db, FINEST_MESH= ", 3(i6,1x) )' ) FINEST_MESH
       WRITE( *, '("               PERIODIC= ", 3(i6,1x) )' ) PERIODIC
    END IF

    IF (ALLOCATED(faces)) DEALLOCATE (faces)
    ALLOCATE (faces(dim))                                          ! allocate the database itself
    DO i=1,dim                                                     !  and initialize each face
       CALL initialize_the_face_for_dim (i)              !
    END DO                                                         !

    !
	! Only continue if arguments add_pts called for adding points
	!
    IF( add_pts == INIT_DONT_ADD_PTS) THEN
	   init_lines_db_backend =  0
	   return
    END IF

    IF (ALLOCATED (products)) DEALLOCATE (products)
    ALLOCATE (products(0:dim))                                      ! single dimension conversion
    factor = 2**(jmin-1)                                            !  for the coordinates on grid J_MIN
    products(0) = 1                                                 ! array: (1, Nx, Nx*Ny, Nx*Ny*Nz)
    DO i=1,dim                                                      !  where Nx is the number of nodes in x-dir
       products(i) = products(i-1) * (m(i)*factor + 1 - p(i))       !  on the grid of the level J_MIN
    END DO
    
    init_lines_db_backend = products(dim)                           ! # of points added

    DO i=1,products(dim)                                         ! add all the nodes .LE. level J_MIN to the database
       i_tmp = i
       DO j=dim,1,-1                                                ! find N-dim coordinates from 1-dim index          
          coord(j) = (i_tmp-1) / products(j-1)                          !
          i_tmp = MOD(i_tmp-1,products(j-1)) + 1
       END DO                                                       !

       level = J_MIN
       CALL calc_the_real_level_of_node (level, coord)
       CALL add_node_level_finest_coord (level, coord, init_flag, j_deriv_lev_init)

       IF (verb) THEN                                            ! and print the result of each addition
          WRITE (*,'(A,I6,A,I6, A,'//CHAR(ICHAR('0')+DIM_NUM)//'I3,A)') &
               'added: #',i, ' level= ', level, ' (',coord,') '
          WRITE (*,*) ' '
       END IF
    END DO
    
  END FUNCTION init_lines_db_backend
  !-----------------------------------------------------------------
  SUBROUTINE finalize_lines_db_backend                          ! cleans the database
    INTEGER :: i,j,lev,dim !  ,nodes                            ! from the memory completely
    TYPE (grid_pt), POINTER :: current_first => NULL()
    TYPE (grid_pt), POINTER :: to_be_deleted => NULL()

    DO j=1,faces(1)%line_size                                          ! deallocate nodes from Face 1
       IF (BTEST(faces(1)%line(j)%line_flags, LINE_IS_INITIALIZED)) THEN                  !
          DO lev=1,J_MAX                                                  ! for each level
!NONUMNODES             nodes = faces(1)%line(j)%number_of_nodes_of_each_level(lev)
!NONUMNODES             IF (nodes.EQ.0) CYCLE                                        ! if there are some nodes
             IF( .NOT. ASSOCIATED(faces(1)%line(j)%first_node_of_each_level(lev)%ptr) ) CYCLE !NEW FOR NONUMNODES
             current_first => &                                           !
                  faces(1)%line(j)%first_node_of_each_level(lev)%ptr      !
!NONUMNODES             DO k=1,nodes                                                    ! and for each node
             DO WHILE (ASSOCIATED(current_first) )
                to_be_deleted => current_first
                !IF (ASSOCIATED(current_first%next(1)%ptr)) THEN
                current_first => current_first%next(1)%ptr
                !END IF
                IF (verb) WRITE( *, '( "deleting ", i6 )' ) to_be_deleted%reg_grid_index
                DO dim=1,DIM_NUM
                   NULLIFY (to_be_deleted% next(dim)%ptr)
                END DO
!nolonger allocated                DEALLOCATE (to_be_deleted% reg_grid_index)
!nolonger allocated                DEALLOCATE (to_be_deleted% u)
!                DEALLOCATE (to_be_deleted% iu)
!nolonger allocated                DEALLOCATE (to_be_deleted% next)
                DEALLOCATE (to_be_deleted)
             END DO                                                          !
          END DO                                                          !
       END IF                                                          !
    END DO                                                             !
    
    DO i=1,DIM_NUM                                              ! deallocate each face
       DO j=1,faces(i)%line_size                                           ! and each line
          IF (BTEST(faces(i)%line(j)%line_flags, LINE_IS_INITIALIZED)) THEN                   !
             IF (verb) &
                  WRITE (*,'(A,I3,A,I3)') 'deallocating face',i,'line',j
#ifdef USE_number_of_nodes_of_each_level
             DEALLOCATE (faces(i)%line(j)% number_of_nodes_of_each_level)  !
#endif
             DEALLOCATE (faces(i)%line(j)% first_node_of_each_level)       !
!UNUSED             DEALLOCATE (faces(i)%line(j)% face_coord)
          END IF                                                           !
       END DO                                                              !
!no longer allocated       DEALLOCATE (faces(i)%odim)                               !
       DEALLOCATE (faces(i)%line)                               !
    END DO                                                      !

    DEALLOCATE (faces)                                 ! deallocate globals
    DEALLOCATE (FINEST_MESH, PERIODIC, products)       !

  END SUBROUTINE finalize_lines_db_backend
  !-----------------------------------------------------------------
  SUBROUTINE info_backend (tnp, tnp_f_set, f)
    ! general template for accessing all the nodes of the database
    INTEGER, INTENT(OUT) :: tnp        ! Total number of points in the database,
    INTEGER, INTENT(OUT) :: tnp_f_set  ! total number of points which flag is
    INTEGER, INTENT(IN)  :: f          !  set to that value
    INTEGER :: i, j !, nodes
    TYPE(grid_pt), POINTER :: ptr => NULL()
    
    tnp = 0
    tnp_f_set = 0
    
    DO i=1,faces(1)%line_size
       IF (BTEST(faces(1)%line(i)%line_flags, LINE_IS_INITIALIZED)) THEN
          DO j=1,J_MAX
!NONUMNODES             nodes = faces(1)%line(i)%number_of_nodes_of_each_level(j)
!NONUMNODES             IF (nodes.EQ.0) CYCLE
             IF( .NOT. ASSOCIATED(faces(1)%line(i)%first_node_of_each_level(j)%ptr) ) CYCLE !NEW FOR NONUMNODES
!NONUMNODES             tnp = tnp + nodes                    ! update total number of points
             ptr => faces(1)%line(i)%first_node_of_each_level(j)%ptr

             DO WHILE (ASSOCIATED(ptr))           ! here we have all the access to the point
                tnp = tnp + 1 ! count points
                IF (ptr%flags.EQ.f) &
                     tnp_f_set = tnp_f_set + 1
                
                ptr => ptr%next(1)%ptr            !
             END DO                               !

          END DO
       END IF
    END DO
    
  END SUBROUTINE info_backend
    !-----------------------------------------------------------------
  SUBROUTINE info_check_backend (tnp, tnp_f_set, flag, j_in)
    ! general template for accessing all the nodes of the database
    INTEGER, INTENT(OUT) :: tnp        ! Total number of points in the database,
    INTEGER, INTENT(OUT) :: tnp_f_set  ! total number of points which flag is
    INTEGER(KIND_INT_FLAGS), INTENT(IN)  :: flag          !  set to that value
	INTEGER, INTENT(IN)  :: j_in
    INTEGER :: i, j !, nodes
    TYPE(grid_pt), POINTER :: ptr => NULL()
	INTEGER :: nline
    
    tnp = 0
    tnp_f_set = 0
    
    DO i=1,faces(1)%line_size
       IF (BTEST(faces(1)%line(i)%line_flags, LINE_IS_INITIALIZED)) THEN
          DO j=1,j_in
!NONUMNODES             nodes = faces(1)%line(i)%number_of_nodes_of_each_level(j)
!NONUMNODES             IF (nodes.EQ.0) CYCLE
             IF( .NOT. ASSOCIATED(faces(1)%line(i)%first_node_of_each_level(j)%ptr) ) CYCLE !NEW FOR NONUMNODES
!NONUMNODES             tnp = tnp + nodes                    ! update total number of points
             ptr => faces(1)%line(i)%first_node_of_each_level(j)%ptr

!nline=0
!IF( i == 1029 .AND. j == 4 ) THEN
!   print*,''
!END IF
             DO WHILE (ASSOCIATED(ptr))           ! here we have all the access to the point
                tnp = tnp + 1 !count all nodes
                nline= nline+1
                IF (IAND(ptr%flags,flag) /= 0 ) &
                     tnp_f_set = tnp_f_set + 1
                
                ptr => ptr%next(1)%ptr            !
             END DO                               !
!IF( nline /= nodes ) THEN
! print *,'ERROR nline /= nodes',nline , nodes
! STOP
!ENDIF
          END DO
       END IF
    END DO
    
  END SUBROUTINE info_check_backend

!broken not used
#ifdef FALSE
  !-----------------------------------------------------------------
  SUBROUTINE delete_some_backend_OLD (del_flag, chla)       ! delete from the database the nodes
    INTEGER(KIND_INT_FLAGS), INTENT(IN) :: del_flag                     !  with the given flag
    INTEGER, INTENT(OUT) :: chla                        ! return the current highest level available
    INTEGER :: pdim, i, j, n, nodes
    TYPE (grid_pt), POINTER :: ptr1 => NULL(), ptr=> NULL()

    chla = 1

    DO pdim=1,DIM_NUM                                           ! for all dimensions
       DO i=1,faces(pdim)%line_size                               ! for all lines at the faces
          IF (BTEST(faces(pdim)%line(i)%line_flags, LINE_IS_INITIALIZED)) THEN         ! for all initialized lines
             DO j=1,J_MAX                                             ! for all the levels ================

!NONUMNODES                nodes = faces(pdim)%line(i)% &
!NONUMNODES                     number_of_nodes_of_each_level(j)                
!NONUMNODES                IF (nodes.EQ.0) CYCLE                                 ! which have some nodes
             ptr1 => faces(pdim)%line(i)% &                        ! starting from that one
                  first_node_of_each_level(j)%ptr
             IF( .NOT. ASSOCIATED(ptr1) ) CYCLE !NEW FOR NONUMNODES

                DO n=1,nodes                                            ! ----- find new first in the list -----
                   IF (ptr1%flags.EQ.del_flag) THEN
                   !IF (IAND(ptr1%flags,del_flag)) THEN

                      ptr => ptr1                                         ! that node to be deleted
                      IF (ASSOCIATED(ptr%next(pdim)%ptr)) THEN
                         faces(pdim)%line(i)% &                              !
                              first_node_of_each_level(j)%ptr => &           ! set new first node in the list
                              ptr%next(pdim)%ptr                             !
                         ptr1 => ptr%next(pdim)%ptr                          !
                         NULLIFY (ptr%next(pdim)%ptr)                        !
                      ELSE
                         NULLIFY (faces(pdim)%line(i)% &
                              first_node_of_each_level(j)%ptr)
                      END IF
!TEST

                      IF (pdim.EQ.DIM_NUM) THEN
					      IF( ASSOCIATED(ptr) ) THEN
                           CALL delete_node (ptr)                         ! delete that node
						  ELSE
						     PRINT *,'delete_some_backend, attemping to delete un allocated (pdim,i,j,n)',pdim,i,j,n
						  END IF
					  END IF
#ifdef USE_number_of_nodes_of_each_level
                      faces(pdim)%line(i)% &                              !
                           number_of_nodes_of_each_level(j) = &           ! update counter
                           faces(pdim)%line(i)% &                         !
                           number_of_nodes_of_each_level(j) - 1           !
#endif
                   ELSE
                      EXIT                                                ! we found the first undeletable
                   END IF

                END DO                                                  ! -----

!NONUMNODES                nodes = faces(pdim)%line(i)% &                          ! number of nodes left in the line
!NONUMNODES                     number_of_nodes_of_each_level(j)
                
!NONUMNODES                IF (nodes.LE.1) CYCLE                                  ! thus, we don't need to check others
                ptr1 => faces(pdim)%line(i)%&                          ! otherwise, we have to
                     first_node_of_each_level(j)%ptr                     ! set pointer to previous
                ptr => ptr1%next(pdim)%ptr                               ! set pointer to current
                DO n=1,nodes-1                                             ! cycle through the nodes left
                   IF (ptr%flags.EQ.del_flag) THEN                           ! delete the node
                   !IF (IAND(ptr%flags,del_flag)) THEN                           ! delete the node


                      IF (ASSOCIATED(ptr%next(pdim)%ptr)) THEN                 ! cut links
                         ptr1%next(pdim)%ptr => ptr%next(pdim)%ptr             !
                         NULLIFY (ptr%next(pdim)%ptr)                          !
                      ELSE                                                     !
                         NULLIFY (ptr1%next(pdim)%ptr)                         !
                      END IF                                                   !
                      IF (pdim.EQ.DIM_NUM) &
                           CALL delete_node (ptr)                              ! delete that node
#ifdef USE_number_of_nodes_of_each_level
                      faces(pdim)%line(i)% &                                   !
                           number_of_nodes_of_each_level(j) = &                ! update counter
                           faces(pdim)%line(i)% &                              !
                           number_of_nodes_of_each_level(j) - 1                !
#endif
                      IF (ASSOCIATED(ptr1%next(pdim)%ptr)) THEN
                         ptr => ptr1%next(pdim)%ptr
                      END IF
                   ELSE                                                      ! do not delete the node
                      ptr1 => ptr1%next(pdim)%ptr
                      IF (ASSOCIATED(ptr1%next(pdim)%ptr)) THEN
                         ptr => ptr1%next(pdim)%ptr
                      END IF
                   END IF                                                    !
                END DO                                                     !

                IF ((pdim.EQ.1).AND.(j.GT.chla).AND. &                                   ! set chla
				    ASSOCIATED(faces(pdim)%line(i)%first_node_of_each_level(j)%ptr) ) &
					chla = j 
!NONUMNODES                     (faces(pdim)%line(i)%number_of_nodes_of_each_level(j).NE.0)) &      !
                                                                                !
                
             END DO                                                   ! =====================================
          END IF                                                    !
       END DO                                                     !
    END DO                                                      !

  END SUBROUTINE delete_some_backend_OLD
#endif
  !-----------------------------------------------------------------

  SUBROUTINE delete_some_backend (del_flag, chla)       ! delete from the database the nodes
    INTEGER(KIND_INT_FLAGS), INTENT(IN) :: del_flag                     !  with the given flag
    INTEGER, INTENT(OUT) :: chla                        ! return the current highest level available
    INTEGER :: pdim, i, j !, n !, nodes
    TYPE (grid_pt_ptr), DIMENSION(:), POINTER :: ptr_tmp => NULL() !, debugptr=> NULL()
    TYPE (grid_pt)    , POINTER :: ptr_deleted => NULL()
    LOGICAL :: first
    INTEGER :: pai !pointer array index
    !INTEGER ::TMP
    chla = 1
    !print *,'delete_some_backend'

    DO pdim=1,DIM_NUM                                           ! for all dimensions
	   IF (verb) WRITE (*,'(A,I8,I8)') 'delete_some_backend, pdim=', pdim, faces(pdim)%line_size
       DO i=1,faces(pdim)%line_size                               ! for all lines at the faces
          IF (verb) WRITE (*,'(A,I8,I8,I8)') 'delete_some_backend, line i=', i, faces(pdim)%line(i)%line_flags
		  IF (BTEST(faces(pdim)%line(i)%line_flags, LINE_IS_INITIALIZED)) THEN         ! for all initialized lines
             IF (verb) WRITE (*,'(A)') 'delete_some_backend Line is initialized'
             ! zero all flags leaving LINE_IS_INITIALIZED set on
             !TMP=faces(pdim)%line(i)%line_flags
             faces(pdim)%line(i)%line_flags = IBSET(0 , LINE_IS_INITIALIZED)
             !TMP=faces(pdim)%line(i)%line_flags
             DO j=1,J_MAX                                             ! for all the levels ================
!NONUMNODES                nodes = faces(pdim)%line(i)% &
!NONUMNODES                     number_of_nodes_of_each_level(j)                
!NONUMNODES                IF (nodes.EQ.0) CYCLE                                 ! which have some nodes

                ptr_tmp => faces(pdim)%line(i)% &                     ! Save pointer to array of pointers to node
                     first_node_of_each_level                         ! Head this is first_node_of_each_level(:)
                !first = .TRUE. ! reset first flag                     ! Below for rest of list it is next(:)
                ! debugptr => faces(pdim)%line(i)%first_node_of_each_level !TEST 
                first = .TRUE.            
                pai = j ! we have ptr_tmp to first_node_of_each_level(1:J_MAX) array of pointer to nodes
                IF (verb) WRITE (*,'(A,L1,L1)') 'ptrs', ASSOCIATED(ptr_tmp) , ASSOCIATED(ptr_tmp(pai)%ptr)
				DO WHILE   (ASSOCIATED(ptr_tmp) .AND. ASSOCIATED(ptr_tmp(pai)%ptr) )   ! ----- loop through nodes -----

                   ptr_deleted => ptr_tmp(pai)%ptr                          ! Save pointer to node that may be deleted
!PRINT * ,'ptr_deleted%flags',ptr_deleted%flags, del_flag, IAND(ptr_deleted%flags,del_flag)
                   IF (IAND(ptr_deleted%flags,del_flag)/=0) THEN
!PRINT *,'delete it'
                      ptr_tmp(pai)%ptr => ptr_deleted%next(pdim)%ptr      ! update item in array of pointers to new nextpoint
                      IF (pdim.EQ.DIM_NUM) CALL delete_node (ptr_deleted) ! delete that node
                      ! COunt the node we just deleted
#ifdef USE_number_of_nodes_of_each_level
                      faces(pdim)%line(i)% &                              !
                           number_of_nodes_of_each_level(j) = &           ! update counter
                           faces(pdim)%line(i)% &                         !
                           number_of_nodes_of_each_level(j) - 1           !
#endif
                   ELSE 
                      chla = MAX(j,chla) !track highest level retained

                      ! update the line_flags bits that mark what deriv level points are on this line
                      ! ptr_deleted% is just ptr to this node
					  ! we do not do this for ghost points because they do nto have a deriv_lev
                      !IF( ptr_deleted%deriv_lev<= j ) THEN
					  IF( .NOT.BTEST(ptr_deleted%flags,GHOST) ) THEN

					     IF (verb) WRITE (*,'(A,I8)') 'delete_some_backend1, line_flags',&
		                    faces(pdim)%line(i)%line_flags 

                         faces(pdim)%line(i)%line_flags = &
                           IBSET(faces(pdim)%line(i)%line_flags, ptr_deleted%deriv_lev)

					     IF (verb) WRITE (*,'(A,I8)') 'delete_some_backend2, line_flags',&
		                    faces(pdim)%line(i)%line_flags 
					  END IF
                      !TMP = faces(pdim)%line(i)%line_flags
                      !END IF
!error here, when pai = 4, with IBM compiler and array bounds
! checking on, I beleive it is ok, just a problem with the
! declaration of the ptr_tmp. The compiler thinks it is an array
! of length one and thus gets unhappy when it is longer, whihc is ok because it points
! to an array that is longer.. D.G. 03/20/06
!PRINT *,'pai = ', pai
!PRINT *,'pdim = ', pdim

                      ptr_tmp => ptr_tmp(pai)%ptr%next  ! point to array of pointer next(:)
                      first = .FALSE.
                      pai = pdim ! we now have ptr_tmp to next(1:dim) array of pointer to nodes

                   END IF
!NONUMNODES                   nodes = nodes -1
				   IF (verb) WRITE (*,'(A,L1,L1)') 'ptrs endloop', ASSOCIATED(ptr_tmp) , ASSOCIATED(ptr_tmp(pai)%ptr)

                END DO
!NONUMNODES                IF( nodes /= 0 ) THEN
!NONUMNODES                   print*,'delete_some_backend() Error nodes /= 0 ', nodes
!NONUMNODES                   STOP
!NONUMNODES                END IF                                                  ! -----

             END DO                      ! =====================================
          	 IF (verb) WRITE (*,'(A,I8)') 'delete_some_backend, line_flags',&
		        faces(pdim)%line(i)%line_flags 
		  END IF
                                                  !
       END DO                                                     !
    END DO                                                      !

  END SUBROUTINE delete_some_backend
  !-----------------------------------------------------------------
  SUBROUTINE add_node_lines_db_backend (level, coord, &                         ! add to the database the node, with
       flags, deriv_lev, truncated_deriv_lev, indx)                     !
    INTEGER(KIND_INT_FLAGS), INTENT (IN) :: flags
	INTEGER, INTENT (IN) :: deriv_lev, truncated_deriv_lev, indx !
    INTEGER, INTENT (IN) :: level(1:DIM_NUM)                                       !  the given real level and
    INTEGER, INTENT (IN) :: coord(DIM_NUM)                              !  the coordinates on J_MAX grid
    INTEGER :: ierr, i, ierr_tmp
    TYPE(grid_pt_ptr)    :: node_itself

    ierr = 0
    DO i=1,DIM_NUM
       IF (verb) WRITE (*,'(A,I2)') 'add_node_lines_db, the face',faces(i)%pdim
       CALL add_node (node_itself,ierr_tmp,i,coord,level(i), &      ! add the node to each Face from faces(i)
            flags,               &
            deriv_lev,           &
            truncated_deriv_lev, &
            indx)                                    ! set: flags, deriv_lev, truncated_deriv_lev, indx
       ierr = ierr + ierr_tmp
    END DO
    
    IF (ierr.NE.1) THEN
       PRINT *, 'ERROR: in add_node_level_finest_coord (',level,',',coord,')'
       STOP
    END IF
    
  END SUBROUTINE add_node_lines_db_backend
  !-----------------------------------------------------------------
  FUNCTION get_1_backend (fep, pdim, max_level, node_ptr, start,current_level)     ! , min_level returns one node for the given line
    TYPE (element_of_face_ptr), INTENT (IN) :: fep             !  according to that face_element pointer
    INTEGER, INTENT(IN) :: pdim                                !  in that direction
    INTEGER, INTENT(IN) :: max_level                           !  for all the levels min_level:max_level
    TYPE (grid_pt), POINTER :: node_ptr              ! the pointer to be returned, Also used after first call to find next pt
    LOGICAL, INTENT(INOUT) :: start                            ! start from beginning, if true
    LOGICAL :: get_1_backend                                   ! Returns .TRUE., if another valid pt exists
    INTEGER, INTENT(INOUT) :: current_level                             !  which is of that level
    INTEGER :: j
    

    IF (start) THEN                                            ! start from the beginning -------------
       start = .FALSE.
       DO j=1,max_level                                          ! find start pointer
	      IF(ASSOCIATED(fep%ptr%first_node_of_each_level(j)%ptr ) ) THEN
             node_ptr => fep%ptr% &                       !
                  first_node_of_each_level(j)%ptr                ! set current_ptr
             current_level = j                                   ! set current_level
             get_1_backend = .TRUE. ; RETURN
          END IF
       END DO

    ELSE                                                       ! continue ------------------------------
       j=current_level
       DO WHILE (j.LE.max_level)  ! --- cycle over the levels left

          IF( ASSOCIATED(node_ptr%next(pdim)%ptr) ) THEN ! there are some nodes left at that level
             node_ptr => node_ptr%next(pdim)%ptr
             current_level = j
             get_1_backend = .TRUE. ; RETURN

          ! Go to next level up
          ELSE                                                      ! we have to go to another level
             DO                                                     ! let's find one that has some nodes on it
                j = j + 1
                IF (j.GT.max_level) THEN
				   get_1_backend = .FALSE. ; RETURN
				END IF
                IF (ASSOCIATED(fep%ptr%first_node_of_each_level(j)%ptr))   EXIT
             END DO
             IF (j.LE.max_level) THEN                               ! we have found such a level
                node_ptr => fep%ptr% &                       !
                     first_node_of_each_level(j)%ptr
                current_level = j
                ! Here we know node_ptr is associated because we checked fep%ptr%first_node_of_each_level(j)%ptr
				! in above loop.
                get_1_backend = .TRUE. ; RETURN
             END IF
          END IF

       END DO                     ! --- cycle over the levels left
    END IF
    get_1_backend = .FALSE. ; RETURN


  END FUNCTION get_1_backend
  !-----------------------------------------------------------------
  ! this routine relies on number_of_nodes_of_each_level as written
#ifdef USE_number_of_nodes_of_each_level
  SUBROUTINE getp_active_lines_indx_backend (pdim, lev, lines_ind, number, nproc)
    INTEGER, INTENT(IN) :: nproc                                         ! number of processors
    TYPE (element_of_face_ptr), INTENT (OUT) :: lines_ind(nproc)         !  first active elements_of_face
    INTEGER, INTENT(OUT) :: number(nproc)                                !  total number of such elements
    INTEGER, INTENT(IN) :: pdim                                          !  for that direction
    INTEGER, INTENT(IN) :: lev                                           !  and all the levels 1:lev
    LOGICAL :: first_set
    INTEGER :: i,j,index,nodes,nodes_total,p, &
         fc(DIM_NUM-1), products(0:DIM_NUM-1), i_tmp, i_real, prod
    
    number(:) = 0                                                        ! initalize output aray

    products(0) = 1                                                      ! initialize auxiliary array
    DO i=1,DIM_NUM-1
       j = faces(pdim)%odim(i)
       products(i) = products(i-1) * (FINEST_MESH(j)/2**(J_MAX-lev) + 1 - PERIODIC(j))
    END DO
    
    index = 0
    nodes_total = 0                                                      ! count all the nodes
    DO i=1,products(DIM_NUM-1)                                             ! (1)D index, at lev
       i_tmp = i
       DO j=DIM_NUM-1,1,-1
          fc(j) = (i_tmp-1) / products(j-1)                                ! (d-1)D coordinates on the face, at lev
          i_tmp = MOD(i_tmp-1,products(j-1)) + 1
       END DO
       fc(:) = fc(:)*2**(J_MAX-lev)
       prod = 1
       i_real = 0                                                          ! (1)D index, at J_MAX
       DO j=1,DIM_NUM-1
          i_real = i_real + fc(j) * prod
          prod = prod * (FINEST_MESH(faces(pdim)%odim(j)) + 1 - PERIODIC(faces(pdim)%odim(j)))
       END DO
       i_real = i_real + 1

       IF (BTEST(faces(pdim)%line(i_real)%line_flags, LINE_IS_INITIALIZED)) THEN
          nodes = SUM(faces(pdim)%line(i_real)%number_of_nodes_of_each_level(1:lev))
          nodes_total = nodes_total + nodes
       END IF
    END DO
    nodes_total = nodes_total / nproc                                    ! average number per processor
    p = 1                                                                ! current processor
    nodes = 0                                                            ! nodes at currend processor
    first_set = .FALSE.
!!$    WRITE (*,'(A,I6,A,I6,A)') 'nodes per processor =',nodes_total,' from max possible ',products(DIM_NUM-1),' lines'

    DO i=1,products(DIM_NUM-1)                                             ! (1)D index, at lev
       i_tmp = i
       DO j=DIM_NUM-1,1,-1
          fc(j) = (i_tmp-1) / products(j-1)                                ! (d-1)D coordinates on the face, at lev
          i_tmp = MOD(i_tmp-1,products(j-1)) + 1
       END DO
       fc(:) = fc(:)*2**(J_MAX-lev)
       prod = 1
       i_real = 0                                                          ! (1)D index, at J_MAX
       DO j=1,DIM_NUM-1
          i_real = i_real + fc(j) * prod
          prod = prod * (FINEST_MESH(faces(pdim)%odim(j)) + 1 - PERIODIC(faces(pdim)%odim(j)))
       END DO
       i_real = i_real + 1

       IF (BTEST(faces(pdim)%line(i_real)%line_flags, LINE_IS_INITIALIZED)) THEN
          IF (SUM(faces(pdim)%line(i_real)%number_of_nodes_of_each_level(1:lev)).NE.0) THEN ! for all the lines with nodes
             nodes = nodes + SUM(faces(pdim)%line(i_real)%number_of_nodes_of_each_level(1:lev))
!!$             WRITE(*,'(A,I1,A,I3)') '#',p,', non-zero line, nodes=',nodes
             IF (.NOT.first_set) THEN                                                  ! set the first line
                lines_ind(p)%ptr => faces(pdim)%line(i_real)
                first_set = .TRUE.
             END IF
             number(p) = nodes
             IF (nodes.GE.nodes_total) THEN                                            ! set number of nodes per processor
                p = p + 1
                nodes = 0
                first_set = .FALSE.
             END IF
          END IF                                                                       !
       END IF
    END DO

!!$    WRITE (*,'(A,I1,A,10I3)') '#proc=',nproc,', nodes:',number

  END SUBROUTINE getp_active_lines_indx_backend
#endif
  !-----------------------------------------------------------------
  !
  ! Get active line pointers
  ! for level <= j
  ! with atleast 1 pt having derive level <= j_df_max and >= j_df_min
  ! if j_df_max = -1 then deriv level is ignored
  !
  SUBROUTINE get_active_lines_indx_backend (pdim, lev, j_df_min, j_df_max, lines_ind, number)  ! fills the given array with pointers
    TYPE (element_of_face_ptr), INTENT (OUT) :: lines_ind(:)               !  to active elements_of_face
    INTEGER, INTENT(OUT) :: number                                         !  in that quantity
    INTEGER, INTENT(IN) :: pdim                                            !  for that direction
    INTEGER, INTENT(IN) :: lev                                             !  and all the levels 1:lev
	INTEGER, INTENT(IN) :: j_df_min, j_df_max                              !  pts with j_df_min<deriv_lev<j_df_max exist on line 
    INTEGER :: i,j,index, &
         fc(DIM_NUM-1), products(0:DIM_NUM-1), i_tmp, i_real, prod

! INTEGER :: TMP, TMP2 !TEST
    products(0) = 1
    DO i=1,DIM_NUM-1
       j = faces(pdim)%odim(i)
       products(i) = products(i-1) * (FINEST_MESH(j)/2**(J_MAX-lev) + 1 - PERIODIC(j))
    END DO    
    
    index = 0
    DO i=1,products(DIM_NUM-1)                                           ! (1)D index, at lev
       i_tmp = i
       DO j=DIM_NUM-1,1,-1
          fc(j) = (i_tmp-1) / products(j-1)                              ! (d-1)D coordinates on the face, at lev
          i_tmp = MOD(i_tmp-1,products(j-1)) + 1
       END DO
       fc(:) = fc(:)*2**(J_MAX-lev)

       prod = 1
       i_real = 0                                                        ! (1)D index, at J_MAX
       DO j=1,DIM_NUM-1
          i_real = i_real + fc(j) * prod
          prod = prod * (FINEST_MESH(faces(pdim)%odim(j)) + 1 - PERIODIC(faces(pdim)%odim(j)))
       END DO
       i_real = i_real + 1


       IF (BTEST(faces(pdim)%line(i_real)%line_flags, LINE_IS_INITIALIZED) .AND. &
	           (j_df_max==-1 .OR. IBITS(	faces(pdim)%line(i_real)%line_flags, j_df_min, j_df_max-j_df_min+1) /= 0 ) &	!pts of desired deriv lev exist on line
			   ) THEN

          DO j=1,lev
		     !TMP = faces(pdim)%line(i_real)%line_flags
			 !TMP2 = IBITS(	faces(pdim)%line(i_real)%line_flags, j_df_min, j_df_max-j_df_min+1)
!
             IF( ASSOCIATED( faces(pdim)%line(i_real)%first_node_of_each_level(j)%ptr) ) THEN
!NONUMNODES             IF (faces(pdim)%line(i_real)%number_of_nodes_of_each_level(j).NE.0 ) THEN
                index = index + 1
                lines_ind (index)%ptr => faces(pdim)%line(i_real)

                EXIT
             END IF
          END DO
       END IF

    END DO
    number = index



  END SUBROUTINE get_active_lines_indx_backend
  !-------------------------------------------------------------------------------------------------------------
  ! PRIVATE STRUCTURE OF THE DATABASE
  !-------------------------------------------------------------------------------------------------------------
  SUBROUTINE delete_node (node)
    TYPE (grid_pt), POINTER :: node 
    
    IF (verb) WRITE (*,'(A,'//CHAR(ICHAR('0')+DIM_NUM)//'I2,A)') 'deleting (',node%reg_grid_index,')'
!nolonger allocated    DEALLOCATE (node% reg_grid_index)
!nolonger allocated    DEALLOCATE (node% u)
!COMMENTiu    DEALLOCATE (node% iu)
!nolonger allocated    DEALLOCATE (node% next)
    DEALLOCATE (node)
 
  END SUBROUTINE delete_node
  !-----------------------------------------------------------------
  SUBROUTINE allocate_node (n)
    TYPE(grid_pt_ptr), INTENT(INOUT) :: n
    TYPE(grid_pt), POINTER :: node_itself => NULL()
    INTEGER :: i

    ALLOCATE (node_itself)
!nolonger allocated    ALLOCATE (node_itself% reg_grid_index(DIM_NUM))
!nolonger allocated    ALLOCATE (node_itself% next(DIM_NUM))
    DO i=1,DIM_NUM
       NULLIFY (node_itself% next(i)%ptr)
    END DO
!nolonger allocated    ALLOCATE (node_itself% u(VAL_NUM))
!COMMENTiu    ALLOCATE (node_itself% iu(VAL_NUM_I))
    
    n%ptr => node_itself

  END SUBROUTINE allocate_node
  !-----------------------------------------------------------------
  ! NOTE truncated_deriv_lev is unused and can be removed from args...D.G.
  SUBROUTINE add_node (node_itself, ierr, thisfn, coord, level, &            ! add a node and initialize everything
       flags, deriv_lev, truncated_deriv_lev, indx)                        ! inside of that node
    INTEGER, INTENT(OUT) :: ierr
    INTEGER(KIND_INT_FLAGS), INTENT(IN) :: flags
	INTEGER, INTENT(IN) :: deriv_lev, truncated_deriv_lev, indx
    INTEGER, INTENT(IN) :: level, coord(DIM_NUM)
    INTEGER, INTENT(IN) :: thisfn                     ! face number the function was called for
    INTEGER :: one_dim_index                          ! 1D index of the cell at the face
	TYPE(grid_pt_ptr), INTENT(INOUT) :: node_itself   ! ptr to node added
                
    !TYPE(grid_pt_ptr) , SAVE :: node_itself
    INTEGER :: i
	INTEGER(KIND_INT_FLAGS) :: flags_loc !, tmpmesh(DIM_NUM),tmpper(DIM_NUM),tmpbnd
    
    one_dim_index = array_index (faces(thisfn), coord)                     ! add the node to that cell of the face
    IF (verb) &
         WRITE( *,'( "add_node: (",3(i6,1x) ,"), 1D index = ", i6 )' ) coord, one_dim_index
    ierr = 0
    IF (.NOT. BTEST(faces(thisfn)%line(one_dim_index)% line_flags, LINE_IS_INITIALIZED)) THEN  ! initialize the line, if necessary
       IF (verb) WRITE( *, '( "initialize the line (", i6 ,")" )' ) one_dim_index
#ifdef USE_number_of_nodes_of_each_level
       ALLOCATE (faces(thisfn)%line(one_dim_index)% number_of_nodes_of_each_level(J_MAX))
#endif
       ALLOCATE (faces(thisfn)%line(one_dim_index)% first_node_of_each_level(J_MAX))
!UNUSED	   ALLOCATE (faces(thisfn)%line(one_dim_index)% face_coord(DIM_NUM))
!UNUSED DG.              faces(thisfn)%line(one_dim_index)% leftmost_level = 0
!UNUSED DG.              faces(thisfn)%line(one_dim_index)% rightmost_level = 0
       faces(thisfn)%line(one_dim_index)% line_flags = &
	      IBSET( faces(thisfn)%line(one_dim_index)% line_flags, LINE_IS_INITIALIZED)
#ifdef USE_number_of_nodes_of_each_level
       faces(thisfn)%line(one_dim_index)% number_of_nodes_of_each_level (:) = 0
#endif
!UNUSED	   faces(thisfn)%line(one_dim_index)% face_coord = coord
!UNUSED	   faces(thisfn)%line(one_dim_index)% face_coord(thisfn) = 0 ! coord perpendicular is always 0
!UNUSED DG.       NULLIFY (faces(thisfn)%line(one_dim_index)% leftmost_node%ptr)
!UNUSED DG.       NULLIFY (faces(thisfn)%line(one_dim_index)% rightmost_node%ptr)
       DO i=1,J_MAX
          NULLIFY (faces(thisfn)%line(one_dim_index)% first_node_of_each_level(i)%ptr)
       END DO
    END IF
    IF (thisfn.EQ.1) THEN                            ! physically add the node to the beginning of the list
       IF (verb) WRITE( *, '("physically create the node")' )
       CALL allocate_node (node_itself)
       flags_loc = flags
!tmpmesh = FINEST_MESH
!tmpper  = PERIODIC
!tmpbnd = BND_FLAG 
	   DO i=1,DIM_NUM
	      IF( ( coord(i) == 0 .OR. coord(i) == FINEST_MESH(i) ) .AND. PERIODIC(i) == 0 ) THEN
			 flags_loc = IBSET( flags_loc , BND_FLAG )
			 exit
		  END IF
	   END DO
!unused       node_itself%ptr% level = level                                    ! set level
       node_itself%ptr% reg_grid_index(:) = coord(:)                     ! set coordinates
       node_itself%ptr% flags = flags_loc                                ! ...
       node_itself%ptr% deriv_lev = deriv_lev                            ! ...
!unused       node_itself%ptr% truncated_deriv_lev = truncated_deriv_lev        ! ...
       node_itself%ptr% indx = indx                                      ! flat indx set when poitns are pulled into u
       node_itself%ptr% u(:) = 0
!COMMENTiu       node_itself%ptr% iu(:) = 0
       node_itself%ptr%ibndr  = 0                                        ! set to interior, (ibndr==0) , then it will be reset
	                                                                     ! in update_u_from_ptrs for a bnd pt
       ierr = 1                                       ! set number of nodes created

	   IF (verb) WRITE( *, '("new node", 6(i5,1x) )' ) node_itself%ptr% reg_grid_index(:), &
	       node_itself%ptr% flags, node_itself%ptr% deriv_lev, node_itself%ptr% indx
    END IF
    
    IF( ASSOCIATED( faces(thisfn)%line(one_dim_index)%first_node_of_each_level(level)%ptr) ) THEN
!NONUMNODES    IF (faces(thisfn)%line(one_dim_index)% number_of_nodes_of_each_level(level).NE.0) THEN
       node_itself%ptr% next(faces(thisfn)%pdim)%ptr => &
            faces(thisfn)%line(one_dim_index)% first_node_of_each_level(level)%ptr
    END IF
#ifdef USE_number_of_nodes_of_each_level
    faces(thisfn)%line(one_dim_index)% number_of_nodes_of_each_level (level) = &
         faces(thisfn)%line(one_dim_index)% number_of_nodes_of_each_level (level) + 1           ! set counter
#endif

    IF (verb) THEN
	     WRITE(*,'(A,I6,L1)') 'add_node, thisfn node alocated', thisfn, ASSOCIATED(node_itself%ptr)
	END IF

    faces(thisfn)%line(one_dim_index)% first_node_of_each_level(level)%ptr => node_itself%ptr   ! set first node of the list

#ifdef USE_number_of_nodes_of_each_level
    IF (verb) &
         WRITE (*,'(A,I3,A,I3)') 'after add_node line has ', &
         faces(thisfn)%line(one_dim_index)% number_of_nodes_of_each_level (level), &
         ' nodes at the level ',level
#endif    
  END SUBROUTINE add_node
  !-----------------------------------------------------------------
  FUNCTION array_index (this, coord)                   ! returns index in the face array
    INTEGER, INTENT(IN) :: coord(DIM_NUM)              ! for the given coordinates
    INTEGER :: array_index                             ! at the level J_MAX
    TYPE(face), INTENT(IN) :: this                     ! for the given face
    INTEGER :: prod, one_dim_coord, i

    prod = 1
    one_dim_coord = 0
    DO i=1,DIM_NUM-1
       one_dim_coord = one_dim_coord + coord (this%odim(i)) * prod
       prod = prod * (FINEST_MESH(this%odim(i)) + 1 - PERIODIC(this%odim(i)))
    END DO
    array_index = one_dim_coord + 1

  END FUNCTION array_index
  !-----------------------------------------------------------------
  SUBROUTINE add_node_level_finest_coord (level, coord, init_flag, j_deriv_lev)  ! add to the database the node, with
    INTEGER, INTENT (IN) :: level                                   !  the given real 1D level and
    INTEGER, INTENT (IN) :: coord(DIM_NUM)                          !  the coordinates on J_MAX grid
    INTEGER(KIND_INT_FLAGS), INTENT(IN)  :: init_flag       ! initial flag value for initial points !ADDED DG
	INTEGER, INTENT(IN)  :: j_deriv_lev     ! initial level for  deriv_lev, truncated_deriv_lev passed to add_node()
    INTEGER :: ierr, i, ierr_tmp 
	TYPE(grid_pt_ptr)    :: node_itself
	                                   
!    INTEGER :: use_init_flag                ! internal or bnd init flags
    
!    use_init_flag = init_flag ! majority of points will be interior
	!if point is a boundary in any direction then set the BND flag
!    DO i=1,DIM_NUM
!       IF( PERIODIC(i) == 0 ) THEN ! check if periodic in this direction
!	      IF( coord(i) == 0 .OR. coord(i) == FINEST_MESH(i) ) THEN ! If so then check if boundary
!			use_init_flag = init_flag_BND
!			exit !loop
!	      END IF
!       END IF
!    END DO

    ierr = 0
    DO i=1,DIM_NUM
       IF (verb) WRITE (*,'(A,I2)') 'add_node_level_finest_coord, the face',faces(i)%pdim


       CALL  add_node (node_itself,ierr_tmp,i,coord,level, &      ! add the node to each Face from faces(i)
            init_flag,j_deriv_lev,j_deriv_lev,0) ! set: flags, deriv_lev, truncated_deriv_lev, indx
       ierr = ierr + ierr_tmp
    END DO
    
    IF (ierr.NE.1) THEN
       PRINT *, 'ERROR: in add_node_level_finest_coord (',level,',',coord,')'
       STOP
    END IF

  END SUBROUTINE add_node_level_finest_coord
  !-----------------------------------------------------------------
  SUBROUTINE calc_the_real_level_of_node (xlevel, coord)        ! for the nodes of the 1:J_MIN levels
    INTEGER, INTENT (INOUT) :: xlevel                                    !  calculate their real levels and
    INTEGER, INTENT (INOUT) :: coord(DIM_NUM)                            !  their coordinates on J_MAX grid
    INTEGER :: rlevel, lv, node_belongs, dim, space
    
    rlevel = xlevel
    DO lv = xlevel-1,1, -1
       space = 2**(J_MIN - lv)               ! space between the nodes of that level
       node_belongs = 0                      ! 0, if belongs to that level
       DO dim=1,DIM_NUM
          node_belongs = node_belongs + &
               MOD(coord(dim),space)         ! all 0, if belongs
       END DO
       IF (node_belongs .NE. 0) THEN      ! xlevel is the real node's level
          EXIT                            ! no need to check hoarser levels
       ELSE
          rlevel = rlevel - 1             ! real is hoarser than xlevel
       END IF
    END DO
    xlevel = rlevel                           ! output real node's level
    coord(:) = coord(:) *  2**(J_MAX-J_MIN)   ! output coordinates at the level J_MAX

  END SUBROUTINE calc_the_real_level_of_node
  !-----------------------------------------------------------------
  SUBROUTINE initialize_the_face_for_dim (perpendicular_dim)   ! set all the initials inside
    INTEGER, INTENT(IN) :: perpendicular_dim                             !  the Face
    INTEGER :: ierr, index, product, i
    
    faces(perpendicular_dim)%pdim = perpendicular_dim               ! initialize pdim
!no longer allocated    ALLOCATE (faces(perpendicular_dim)%odim(DIM_NUM-1), STAT=ierr)
!no longer allocated    IF (ierr.NE.0) THEN
!no longer allocated       WRITE (*,'(A,X,I1)') 'ERROR: allocating odim for the face ', perpendicular_dim
!no longer allocated       STOP
!no longer allocated    END IF
    index = 1
    product = 1
    DO i=1,DIM_NUM                                                  ! initialize odim(:)
       IF (perpendicular_dim .EQ. i) CYCLE
       faces(perpendicular_dim)%odim(index) = i
       index = index + 1
       product = product * (FINEST_MESH(i) + 1 - PERIODIC(i))
    END DO

    ALLOCATE (faces(perpendicular_dim)%line(product), STAT=ierr)    ! initialize line(:)
    IF (ierr.NE.0) THEN
       WRITE (*,'(A,X,I1)') 'ERROR: allocating the face ', perpendicular_dim
       STOP
    END IF
    faces(perpendicular_dim)%line_size = product                    ! initialize line_size
    DO i=1,product                                                  ! set initials for each line
       faces(perpendicular_dim)%line(i)%line_flags = &
	      IBCLR(faces(perpendicular_dim)%line(i)%line_flags, LINE_IS_INITIALIZED)
    END DO

    IF (verb) THEN
       WRITE (*,'(A,I1)') 'Initializing Face for perpendicular dimension ', perpendicular_dim
       WRITE (*,'(" total number of face elements is ", i6 ,", odim=", i3 )' ) &
            product, faces(perpendicular_dim)%odim
       WRITE (*,*) ''
    END IF

  END SUBROUTINE initialize_the_face_for_dim


    !-----------------------------------------------------------------
	! Zero ghost points on levels 1:j_in
	! NOTE: this is called from c_diff_fast, It would be faster to do this from
	! the sweep in diff_aux..DG
	!
  SUBROUTINE zero_ghost_lines_db_backend( j_in )     ! cleans the database up to j_in
    INTEGER , INTENT(IN) :: j_in
    INTEGER :: j,lev !,nodes !,dim                           ! from the memory completely
    TYPE (grid_pt), POINTER :: current_first => NULL()
    !TYPE (grid_pt), POINTER :: to_be_deleted => NULL()

    DO j=1,faces(1)%line_size                                          ! deallocate nodes from Face 1
       IF (BTEST(faces(1)%line(j)%line_flags, LINE_IS_INITIALIZED)) THEN                  !
          DO lev=1,j_in                       ! for each level
             IF( .NOT. ASSOCIATED(faces(1)%line(j)%first_node_of_each_level(lev)%ptr) ) CYCLE !NEW FOR NONUMNODES
!NONUMNODES             nodes = faces(1)%line(j)%number_of_nodes_of_each_level(lev)
!NONUMNODES             IF (nodes.EQ.0) CYCLE                                        ! if there are some nodes
             current_first => &                                           !
                  faces(1)%line(j)%first_node_of_each_level(lev)%ptr      !
             DO ! k=1,nodes                                                    ! and for each node
                !zero out if this is a ghost point
				IF( BTEST( current_first%flags, GHOST ) ) THEN
				   current_first%u(:) = 0.0_pr
				ENDIF
                IF (ASSOCIATED(current_first%next(1)%ptr)) THEN
                   current_first => current_first%next(1)%ptr
				ELSE
				   EXIT ! loop
                END IF

             END DO                                                          !
          END DO                                                          !
       END IF                                                          !
    END DO                                                             !
    

  END SUBROUTINE zero_ghost_lines_db_backend

    !-----------------------------------------------------------------
	! Zero all db storage, SHould only be used for debugging
	! 
	! 
	!
  SUBROUTINE zero_all_db_backend( )     ! cleans the database up to j_in
    INTEGER :: j,lev !,nodes !,dim                           ! from the memory completely
    TYPE (grid_pt), POINTER :: current_first => NULL()
    !TYPE (grid_pt), POINTER :: to_be_deleted => NULL()

    DO j=1,faces(1)%line_size                                          ! deallocate nodes from Face 1
       IF (BTEST(faces(1)%line(j)%line_flags, LINE_IS_INITIALIZED)) THEN                  !
          DO lev=1,j_max                      ! for each level
             IF( .NOT. ASSOCIATED(faces(1)%line(j)%first_node_of_each_level(lev)%ptr) ) CYCLE !NEW FOR NONUMNODES
             current_first => &                                           !
                  faces(1)%line(j)%first_node_of_each_level(lev)%ptr      !
             DO ! k=1,nodes                                                    ! and for each node
                !zero out if this storage
				
				current_first%u(:) = 0.0_pr
				
                IF (ASSOCIATED(current_first%next(1)%ptr)) THEN
                   current_first => current_first%next(1)%ptr
				ELSE
				   EXIT ! loop
                END IF

             END DO                                                          !
          END DO                                                          !
       END IF                                                          !
    END DO                                                             !
    

  END SUBROUTINE zero_all_db_backend
END MODULE lines_database


