MODULE distance 
  USE precision
  IMPLICIT NONE

  PUBLIC
  REAL (pr), DIMENSION (:, :), ALLOCATABLE :: norm
  REAL (pr), DIMENSION (:), ALLOCATABLE :: dist  !distance function used with penalization 
  
  INTEGER, PROTECTED :: ndist=0
  LOGICAL, PROTECTED :: interpolate_dist, save_dist, use_dist, adapt_dist

  PUBLIC :: distance_setup, distance_finalize_setup, distance_input

CONTAINS
  
  !
  ! In hyper_setup() we setup all the varaibles necessary for SGS 
  !
  ! The following variables must be setup in this routine:
  !
  !
  !
  SUBROUTINE  distance_setup () 
    USE parallel
    USE variable_mapping 
    IMPLICIT NONE
    
    IF (par_rank.EQ.0) THEN
       PRINT *, '************** Setting up distance function ****************'
    END IF

    CALL register_var( 'Dist  ', .FALSE., adapt=(/adapt_dist,adapt_dist/), interpolate=(/interpolate_dist,interpolate_dist/), exact=(/.FALSE.,.FALSE./), saved=save_dist, req_restart=.FALSE. )

  END SUBROUTINE distance_setup

  ! Called after all variables and mappings and counts are established
  SUBROUTINE  distance_finalize_setup () 
    USE variable_mapping 
    IMPLICIT NONE
    
     ndist = get_index('Dist')

  END SUBROUTINE distance_finalize_setup

  SUBROUTINE  distance_input() 
    USE input_file_reader
    USE penalization 
    USE pde
    IMPLICIT NONE

    interpolate_dist = .FALSE. ! default value
    IF(imask_obstacle) call input_logical ('interpolate_dist',interpolate_dist,'default', &
         'if T calcualtes the distance function, otherwise interpolates it without calculating during time integration')

    use_dist = .FALSE. ! default value
    IF(imask_obstacle) call input_logical ('use_dist',use_dist,'default', &
         'if T allocstes array and calcualtes, otherwise interpolates it without calculating during time integration')
    IF(interpolate_dist) use_dist = .TRUE.

    save_dist = .FALSE.
    IF(use_dist) call input_logical ('save_dist',save_dist,'default', &
         'if T save distance function to res files')

    adapt_dist = .FALSE.
    IF(use_dist) call input_logical ('adapt_dist',adapt_dist,'default', &
         'if T adapt on the distance function')

  END SUBROUTINE distance_input

END MODULE distance 
