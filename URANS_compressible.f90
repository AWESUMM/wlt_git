MODULE URANS_compressible
  !
  ! URANS models for compressible turbulence
  ! Moscow, August 24, 2017
  ! 
  USE precision
  USE input_file_reader
  USE io_3D_vars
  USE pde
  USE variable_mapping 
  USE share_consts
  USE share_kry
  USE util_mod
  USE vector_util_mod
  USE spectra_module
  USE SGS_util 
  USE equations_compressible 
  USE compressible_mean_vars
  USE curvilinear
  USE curvilinear_mesh !physical coordinates
  USE field !u
  USE URANS_compressible_KOME
  USE URANS_compressible_SA

  REAL (pr), PRIVATE :: total_Ksgs, total_Kdiss

CONTAINS

  SUBROUTINE URANS_compressible_additional_vars( t_local, flag )
    !USE field
    IMPLICIT NONE

    REAL (pr), INTENT (IN) ::  t_local
    INTEGER , INTENT(IN) :: flag ! 0- called during adaption to IC, 1 called during main integration loop
    LOGICAL, SAVE :: first_call = .TRUE.

    INTEGER :: idim, nS
    REAL (pr) :: dt_avg, relax_avg

    REAL (pr), DIMENSION(nwlt,dim) :: v
    REAL (pr), DIMENSION(dim, nwlt, dim) :: du, d2u
    REAL (pr), DIMENSION (nwlt,dim*(dim+1)/2) :: S0_ij
    REAL (pr), DIMENSION(nwlt) :: temp, props, chi, fv1, Smod
    INTEGER :: i
    INTEGER, PARAMETER :: meth=1 ! used meth=1 to match calc_vort()

    !CALL nearest_wall_dist( nwlt, u(:,n_var_distw) ) 
    ! move to case file

    IF( sgsmodel == compressible_KOME .OR. sgsmodel == compressible_KOMET ) THEN

       CALL KOME_compressible_additional_vars( t_local, flag )

    ELSE IF( sgsmodel == compressible_SA ) THEN

       CALL SA_compressible_additional_vars( t_local, flag )
       
    END IF

  END SUBROUTINE URANS_compressible_additional_vars

  
  SUBROUTINE  URANS_compressible_setup ()
  ! In SGS_setup() we setup all the varaibles necessary for SGS 
  ! The following variables must be setup in this routine: n_var_SGS
    USE parallel
    IMPLICIT NONE

    REAL (pr) :: floor = 1.e-12_pr

    IF( sgsmodel == compressible_KEPS ) THEN
       !To be added later
       !CALL KEPS_compressible_setup ( floor )
    ELSE IF( sgsmodel == compressible_KOME .OR. sgsmodel == compressible_KOMET ) THEN
       CALL KOME_compressible_setup ( floor )
    ELSE IF( sgsmodel == compressible_SA ) THEN
       CALL SA_compressible_setup ( floor )
    ELSE
      n_var_SGS = 0
    END IF    

  END SUBROUTINE URANS_compressible_setup

  ! get the indices alloted to sgs variables
  SUBROUTINE  URANS_compressible_finalize_setup ()
    USE parallel
    IMPLICIT NONE
    INTEGER :: i

    IF( sgsmodel == compressible_KEPS) THEN
       !To be added later
       !CALL KEPS_compressible_finalize_setup ()
    ELSE IF( sgsmodel == compressible_KOME .OR. sgsmodel == compressible_KOMET ) THEN
       CALL KOME_compressible_finalize_setup ()
    ELSE IF( sgsmodel == compressible_SA ) THEN
       CALL SA_compressible_finalize_setup ()
    END IF

    IF (par_rank.EQ.0) THEN
       PRINT * ,''; PRINT * ,''
       PRINT *, 'PRINT   from   SUBROUTINE URANS_compressible_setup'
       PRINT *, '************** Setting up SGS (RANS) module ****************'
       PRINT * ,' SGS (RANS) module'
       PRINT *, '*****************************************************'
       PRINT *, 'n_var_SGS (RANS)    = ',  n_var_SGS
       PRINT *, '- - - - - - - - - - - - - - - - - -'
       PRINT *, 'n_integrated      = ',n_integrated 
       PRINT *, 'n_var_additional  = ',n_var_additional   
       PRINT *, 'n_var             = ',n_var 
       PRINT *, 'n_var_exact       = ',n_var_exact 
       PRINT *, '*******************Variable Names*******************'
       DO i = 1,n_var
          WRITE (*, u_variable_names_fmt) u_variable_names(i)
       END DO
       PRINT *, '****************************************************'
    END IF
    
  END SUBROUTINE URANS_compressible_finalize_setup

  SUBROUTINE  URANS_compressible_read_input()
  !
  ! read variables from input file for this user case 
  !
     USE parallel
     IMPLICIT NONE

     IF( sgsmodel == compressible_KEPS) THEN
       !To be added later
       !CALL KEPS_compressible_read_input ()
    ELSE IF( sgsmodel == compressible_KOME .OR. sgsmodel == compressible_KOMET ) THEN
       CALL KOME_compressible_read_input ()
    ELSE IF( sgsmodel == compressible_SA ) THEN
       CALL SA_compressible_read_input ()
    END IF

  END SUBROUTINE  URANS_compressible_read_input


  SUBROUTINE URANS_compressible_initial_conditions (u_in, nlocal, ne_local, t_local, scl, scl_fltwt)
    !--Defines initial conditions for the URANS eqns
    IMPLICIT NONE
    INTEGER,                                INTENT (IN)    :: nlocal, ne_local
    REAL (pr), DIMENSION (nlocal,ne_local), INTENT (INOUT) :: u_in
    REAL (pr)                                              :: scl(1:n_var), scl_fltwt
    REAL (pr),                              INTENT (IN)    :: t_local
  END SUBROUTINE URANS_compressible_initial_conditions

  SUBROUTINE URANS_algebraic_BC (Lu, u_in, nlocal, ne_local, jlev, meth)
    !--Defines default SGS boundary conditions for the URANS eqns
    IMPLICIT NONE
    INTEGER,                                INTENT (IN)    :: jlev, meth, ne_local, nlocal
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (INOUT) :: Lu
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (IN)    :: u_in
  END SUBROUTINE URANS_algebraic_BC

  SUBROUTINE URANS_algebraic_BC_diag (Lu_diag, nlocal, ne_local, jlev, meth)
    !--Defines default SGS boundary conditions for the URANS eqns
    IMPLICIT NONE
    INTEGER,                                INTENT (IN)    :: jlev, meth, ne_local, nlocal
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (INOUT) :: Lu_diag
  END SUBROUTINE URANS_algebraic_BC_diag

  SUBROUTINE URANS_algebraic_BC_rhs (rhs, ne_local, nlocal, jlev)
    !--Defines default SGS boundary conditions for the URANS eqns
    IMPLICIT NONE
    INTEGER,                                INTENT (IN)    :: ne_local, nlocal, jlev
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (INOUT) :: rhs
  END SUBROUTINE URANS_algebraic_BC_rhs

  SUBROUTINE URANS_compressible_rhs ( urans_rhs, u_integrated, meth )
    !
    !-- Form RHS of URANS equations
    !
    IMPLICIT NONE
    REAL (pr), DIMENSION (ng*ne), INTENT(INOUT) :: urans_rhs
    REAL (pr), DIMENSION (ng,ne), INTENT(IN)    :: u_integrated ! 1D flat version of integrated variables without BC 
    REAL (pr), DIMENSION (ng,ne)                :: u_integrated_aux
    INTEGER, INTENT(IN) :: meth 

    IF( sgsmodel == compressible_KOME ) THEN
       ! clipping integrated turbulence energy
       u_integrated_aux = u_integrated
       CALL KOME_compressible_rhs ( urans_rhs, u_integrated_aux, meth )
    ELSE IF( sgsmodel == compressible_SA ) THEN
!       CALL print_extrema(u_integrated(:,n_var_MT), ng, 'mu_tilde')
       u_integrated_aux = u_integrated
       CALL SA_compressible_rhs ( urans_rhs, u_integrated_aux, meth )
    END IF

  END SUBROUTINE URANS_compressible_rhs

  SUBROUTINE URANS_compressible_Drhs (urans_Drhs, u_p, u_prev, meth )
    !
    !-- Form DRHS of URANS equations
    !
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: meth
    REAL (pr), DIMENSION (ng*ne), INTENT(INOUT) :: urans_Drhs
    REAL (pr), DIMENSION (ng,ne), INTENT(IN) :: u_p, u_prev         ! u_prev           - previous, u_p                         - perturbation                            
    REAL (pr), DIMENSION (ng,ne)             :: u_prev_aux

    IF( sgsmodel == compressible_KOME ) THEN
       ! clipping integrated turbulence energy
       u_prev_aux = u_prev
       CALL KOME_compressible_Drhs (urans_Drhs, u_p, u_prev_aux, meth )
    ELSE IF( sgsmodel == compressible_SA ) THEN
       CALL SA_compressible_Drhs (urans_Drhs, u_p, u_prev, meth )
    END IF

  END SUBROUTINE URANS_compressible_Drhs

  SUBROUTINE URANS_compressible_Drhs_diag (urans_Drhs_diag,u_prev,meth)
    !
    !-- Form DRHS_diag of URANS model
    !
    USE variable_mapping
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: meth
    REAL (pr), DIMENSION (ng*ne), INTENT(INOUT) :: urans_Drhs_diag
    REAL (pr), DIMENSION (ng,ne), INTENT(IN) :: u_prev
    REAL (pr), DIMENSION (ng,ne)             :: u_prev_aux


    IF( sgsmodel == compressible_KOME ) THEN
       ! clipping integrated turbulence energy
       u_prev_aux = u_prev
       CALL KOME_compressible_Drhs_diag (urans_Drhs_diag,u_prev_aux,meth )
    ELSE IF( sgsmodel == compressible_SA ) THEN
       CALL SA_compressible_Drhs_diag (urans_Drhs_diag,u_prev,meth)
    END IF

  END SUBROUTINE URANS_compressible_Drhs_diag

  SUBROUTINE URANS_compressible_stats ( u_in ,j_mn, startup_flag)
    USE util_vars        ! dA
    USE parallel
    USE io_3D_vars
   !USE fft_module
   !USE spectra_module
    IMPLICIT NONE
    REAL (pr), DIMENSION (nwlt,1:n_var), INTENT (IN) :: u_in
    INTEGER ,                            INTENT (IN) :: j_mn, startup_flag 
    
    CHARACTER (LEN=256)  :: filename
   !CHARACTER (LEN=256)  :: file_out
    INTEGER              :: outputfileUNIT
    INTEGER              :: count_u_g_mdl_gridfilt_mask, count_u_g_mdl_testfilt_mask
    REAL (pr)            :: ttke, tmp1
    LOGICAL , SAVE       :: start = .TRUE.

    
    ! only proc 0 writes the file
    IF (par_rank.EQ.0) THEN
       filename = TRIM(res_path)//TRIM(file_gen)//'case_turb.log'
      !PRINT *,' SGS_stats, logging at proc #0 to : ', filename
    END IF

    ! Find the TKE and ... if the dA weights have been allocated (and it is
    ! assumed that if they are allocated they have been calculated)

    IF( ALLOCATED(dA) ) THEN

       !
       ! volume-averaged resolved kinetic energy
       !
       ttke = 0.5_pr * SUM( dA(:)*SUM(u_in(:,1:dim)**2,DIM=2) ) /sumdA_global
       CALL parallel_global_sum( REAL=ttke )
 
       !
       ! volume-averaged Ksgs and Kdiss (fluid domain)
       !
       IF( sgsmodel == compressible_KOME ) THEN
          CALL KOME_compressible_stats ( u_in, total_Ksgs, total_Kdiss )
       ELSE
          total_Ksgs          = 0.0_pr
          total_Kdiss         = 0.0_pr
       END IF
       
       IF (verb_level.GT.0 .AND. par_rank.EQ.0) THEN
          PRINT *,' SGS_STATS'
          PRINT *,'***********************************************'
          WRITE(*,'(" (Approximate) TKE =",E12.5)') ttke
          IF(sgsmodel >= compressible_KEPS .AND. sgsmodel <= compressible_KOMET) &
               WRITE(*,'(" (Approximate) TKsgs =",E12.5," TKsgs/TKE=",E12.5)')  total_Ksgs, total_Ksgs/ttke
       END IF
       
       !
       ! active points on masks
       !
       IF( ALLOCATED(u_g_mdl_gridfilt_mask ) ) THEN
          tmp1 = SUM(u_g_mdl_gridfilt_mask)
          CALL parallel_global_sum( REAL=tmp1 )
          count_u_g_mdl_gridfilt_mask = INT( tmp1 )
       ELSE
          count_u_g_mdl_gridfilt_mask = -1
       ENDIF

       IF( ALLOCATED(u_g_mdl_testfilt_mask ) ) THEN
          tmp1 = SUM(u_g_mdl_testfilt_mask)
          CALL parallel_global_sum( REAL=tmp1 )
          count_u_g_mdl_testfilt_mask = INT( tmp1 )
       ELSE
          count_u_g_mdl_testfilt_mask = -1
       ENDIF

       IF (verb_level.GT.0 .AND. par_rank.EQ.0) THEN
          PRINT *,' # points on grid filt mask = ', count_u_g_mdl_gridfilt_mask
          PRINT *,' # points on test filt mask = ', count_u_g_mdl_testfilt_mask
          PRINT *,'***********************************************'
          PRINT *,''
       END IF

       !
       ! output on file (only proc 0 writes the file)
       !
       IF (par_rank.EQ.0) THEN
          outputfileUNIT = 157
          OPEN (UNIT = outputfileUNIT, FILE =filename, STATUS='unknown',&
               POSITION='APPEND')
         !PRINT *,'SGS_stats(), start = ', start 
          IF( start ) THEN
             write(UNIT=outputfileUNIT,ADVANCE='YES', FMT='( a )' ) &
             '%Time          avg_tke        gridfilt_mask  testfilt_mask  avg_Ksgs       eps            avg_Kdiss'
          END IF
          start = .FALSE.
          write(UNIT=outputfileUNIT,ADVANCE='YES' , &
               FMT='(  2(e14.7 , '' '') , 2(i14 , '' ''), 3(e14.7 , '' ''))' ) &
               t, ttke, count_u_g_mdl_gridfilt_mask, count_u_g_mdl_testfilt_mask, total_Ksgs, eps, total_Kdiss
          CLOSE(UNIT = outputfileUNIT)
       END IF

    END IF

  END SUBROUTINE URANS_compressible_stats

  SUBROUTINE URANS_compressible_post_process
    USE variable_mapping
    IMPLICIT NONE

    IF( sgsmodel == compressible_KOME ) THEN
       CALL KOME_compressible_post_process
    END IF
  END SUBROUTINE URANS_compressible_post_process
 
  !
  ! Intialize sgs model
  ! This routine is called once in the first
  ! iteration of the main time integration loop.
  ! weights and model filters have been setup for first loop when this routine is called.
  !
  SUBROUTINE URANS_compressible_model_setup( nlocal )
    USE wlt_trns_vars      ! indx_DB 
    USE wlt_trns_mod       ! delta_from_mask
    USE curvilinear
    USE parallel
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: nlocal
    
    INTEGER, SAVE        :: nlocal_last = 0
    INTEGER              :: i, k, ii, j, j_df, wlt_type, face_type, idim
    REAL (pr)            :: tmp1, tmp2

    IF( nlocal /= nlocal_last) THEN

       IF( ALLOCATED(delta) )       DEALLOCATE( delta )
                                    ALLOCATE(   delta(1:nlocal) )

    END IF

    !
    ! this part to find grid-filter local width delta(:)
    !
    ! this is for uniform mesh, for streched meshes use DB_get_all_local_h
    
    IF( COUNT(u_g_mdl_gridfilt_mask==1.0_pr) == nwlt ) THEN
       DO j = 1, j_lev
          DO wlt_type = MIN(j-1,1),2**dim-1
             DO face_type = 0, 3**dim - 1
                DO j_df = j, j_lev
                   DO k = 1, indx_DB(j_df,wlt_type,face_type,j)%length  
                      i = indx_DB(j_df,wlt_type,face_type,j)%p(k)%i
                      delta(i) = PRODUCT(h(j_df,1:dim))**(1.0_pr/REAL(dim,pr))
                   END DO
                END DO
             END DO
          END DO
       END DO
    ELSE
       CALL delta_from_mask(delta,u_g_mdl_gridfilt_mask,nlocal)
    END IF

    IF( ANY(transform_dir(1:dim)>0) ) THEN
       delta = calc_jacobian_determinant( curvilinear_jacobian, dim, nwlt)**(-1.0_pr/REAL(dim,pr))*delta
    END IF
    
    IF (verb_level.GT.0) THEN
       tmp1 = MINVAL( delta(:) )
       tmp2 = MAXVAL( delta(:) )
       CALL parallel_global_sum( REALMINVAL=tmp1 )
       CALL parallel_global_sum( REALMAXVAL=tmp2 )
       IF (par_rank.EQ.0) PRINT *,'MINMAX delta  = ', tmp1, tmp2
    END IF
    
    nlocal_last = nlocal

  END SUBROUTINE URANS_compressible_model_setup


  !
  !************ Calculating Scales ***************************
  !
  ! Note the order of the components in the scl array
  ! correspond to u_tn, v_tn, w_tn, u_tn-1, v_tn-1, w_tn-1, u_tn-2, v_tn-2, w_tn-2
  ! 
  SUBROUTINE URANS_compressible_scales(flag,use_default, u_loc, nlocal, ne_local, l_n_var_adapt , l_n_var_adapt_index, &
       scl, scl_fltwt) !add
    USE variable_mapping
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: flag ! 0 during initial adaptation, then 1 during time advancement
    LOGICAL , INTENT(INOUT) :: use_default
    INTEGER, INTENT (IN) :: nlocal, ne_local
    REAL (pr), DIMENSION (1:nlocal,1:ne_local), INTENT (IN) :: u_loc
    LOGICAL , INTENT (IN) :: l_n_var_adapt(ne_local)
    INTEGER , INTENT (IN) :: l_n_var_adapt_index(1:ne_local)
    REAL (pr), DIMENSION (1:ne_local), INTENT (INOUT) :: scl
    REAL (pr), DIMENSION (1:ne_local) :: mean
    REAL (pr) ,  INTENT(IN) ::scl_fltwt !weight for temporal filter on scl
    REAL (pr) :: tmp,tmpmax,tmpmin,tmpsum
    INTEGER :: i, ie, ie_index, tmpint
    REAL (pr), SAVE, ALLOCATABLE, DIMENSION (:) :: scl_old
    LOGICAL, SAVE :: startup_init = .TRUE.

  END SUBROUTINE URANS_compressible_scales

END MODULE URANS_compressible
