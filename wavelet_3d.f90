!
! This module contains utility routines that are used by all versions
! of the wavelet transform.
!

MODULE wlt_trns_util_mod
!!$  USE debug_vars
!!$  USE sizes
!!$  USE util_vars
  USE share_consts
  IMPLICIT NONE

  !TEST
  !   TYPE DERIV_ACCEL
  !     INTEGER :: ix_l
  !     INTEGER :: ix_h
  !  END TYPE DERIV_ACCEL
  !  TYPE (DERIV_ACCEL) , DIMENSION(:,:,:,:,:)   , ALLOCATABLE, SAVE :: ix_lh_diff_db ! 

  PUBLIC ::  get_all_indices_by_face,         & !
       get_all_local_h,                       & !
       get_indices_and_coordinates_by_types,  & !
       set_xx,                                & !
       test_indx_DB,                          & !
       get_xx_index,                          & ! find index in xx(1:nxyz()) array for a given real x value
       interpolate_to_box_1,                  & ! first order interpolation into a box
       interpolate_to_box_3                     ! third order interpolation into a box

CONTAINS
  
  !************ GRID ********************************************


  SUBROUTINE set_xx (xx_local, h_local, nxyz_local, max_nxyz_local, j_mx_local)
    USE precision
    USE wlt_vars !xyzlimts, dim
    INTEGER, INTENT(IN) :: max_nxyz_local, nxyz_local(dim), j_mx_local
    REAL (pr), DIMENSION(0:max_nxyz_local,dim), INTENT(INOUT) :: xx_local
    REAL (pr), DIMENSION(j_mx_local,dim), INTENT(INOUT) ::       h_local    !grid spacing, only for uniform grid
    REAL (pr) :: dxyz(dim)
    INTEGER :: i, j, idim
    LOGICAL :: use_default


    ! CALL user_set_xx(use_default,xx_local,h_local,nxyz_local,max_nxyz_local,j_mx_local)  ! add this funciotnality in the future
    use_default = .TRUE.

    IF(use_default) THEN
       !sanity check
       IF( MAXVAL(nxyz_local) /= max_nxyz_local) THEN
          WRITE(*,'("SET_XX: wrong leading dimension")')
          STOP 1
       END IF

       ! calculate dx and xx for each dim
       ! dx is delta x and xx(i,1), xx(i,2), xx(i,3) is the real space coordinates of point i
       DO idim = 1,dim
          IF(grid(idim) == 0) THEN !regular grid
             dxyz(idim)=(xyzlimits(2,idim)-xyzlimits(1,idim))/REAL(nxyz_local(idim),pr)
             DO i=0,nxyz_local(idim)
                xx_local(i,idim)=xyzlimits(1,idim)+dxyz(idim)*REAL(i,pr)
             END DO
          ELSE !Chebyshev grid
             dxyz(idim)=0.5_pr*(xyzlimits(2,idim)-xyzlimits(1,idim))
             DO i=0,nxyz_local(idim)
                xx_local(i,idim)=xyzlimits(1,idim)+dxyz(idim)*(1.0_pr-COS(pi*REAL(i,pr)&
                     /REAL(nxyz_local(idim),pr)) )
             END DO
          END IF
       END DO
       ! real space distance to the next point on the next coarser level
       DO idim=1,dim
          DO j=1,j_mx_local
             h_local(j,idim)=(xyzlimits(2,idim)-xyzlimits(1,idim))/REAL(mxyz(idim)*2**(j-1),pr)
          END DO
       END DO

    END IF
  END SUBROUTINE set_xx
  
  !-------------------------------------------------------------------------------------------
  SUBROUTINE interpolate_to_box_1( ubox, res, var_size, var, rel_x )
    ! linear interpolation into the box of iwlt indices
    USE wlt_vars  ! xyzlimts, dim
    INTEGER, INTENT(IN) :: var_size
    REAL(pr), INTENT(INOUT) :: ubox(var_size,2**dim) ! box of u values
    REAL(pr), INTENT(OUT) :: res(var_size)    ! interpolation result
    INTEGER, INTENT (IN) :: var(1:var_size)   ! variables to interpolate for
    REAL(pr), INTENT(IN) :: rel_x(dim)        ! relative to box corner coordinate \in \[0,1\]
    INTEGER :: i, idim, box_marker          ! counters


    ! shrink each dimension of ubox
    box_marker = 2**(dim-1)
    DO idim=dim,1,-1
       DO i=1,box_marker
          ubox(:,i) = ubox(:,i)*(1-rel_x(idim)) + ubox(:,box_marker+i)*rel_x(idim)
       END DO
       box_marker = box_marker / 2
    END DO

    ! write interpolation result
    res(:) = ubox(:,1)

  END SUBROUTINE interpolate_to_box_1
  !-------------------------------------------------------------------------------------------
  SUBROUTINE interpolate_to_box_3( ubox, dubox, res, var_size, var, rel_x )
    ! 1 order interpolation into the box of iwlt indices
    USE wlt_vars  ! dim
    INTEGER, INTENT(IN) :: var_size
    REAL(pr), INTENT(INOUT) :: ubox(var_size,2**dim), & ! box of u values
         dubox(var_size,2**dim,dim),                  & ! box of du values
         res(var_size)                                  ! interpolation result
    INTEGER, INTENT (IN) :: var(1:var_size)    ! variables to interpolate for
    REAL(pr), INTENT(IN) :: rel_x(dim)         ! relative to box corner coordinate \in \[0,1\]
    INTEGER :: i, idim, jdim, box_marker      ! counters
    REAL(pr) :: x_2(dim), x_3(dim)
    

    x_2(1:dim) = rel_x(1:dim)*rel_x(1:dim)    ! x^2
    x_3(1:dim) = x_2(1:dim)*rel_x(1:dim)      ! x^3

    ! shrink each dimension of ubox
    box_marker = 2**(dim-1)
    DO idim=dim,1,-1
       DO i=1,box_marker

          ! f_1  -- ubox(:,i)
          ! f_2  -- ubox(:,box_marker+i)
          ! df_1 -- dubox(:,i,idim)
          ! df_2 -- dubox(:,box_marker+i,idim)
          ! x    -- rel_x(idim)
          ! interpolate function: (3rd order, F(1), F(2) and F'(1), F'(2) based)
          ubox(:,i) = ubox(:,i) + dubox(:,i,idim)*rel_x(idim) &
               - x_2(idim)*( 3*(ubox(:,i)-ubox(:,box_marker+i)) + dubox(:,box_marker+i,idim)+2*dubox(:,i,idim) ) &
               + x_3(idim)*( 2*(ubox(:,i)-ubox(:,box_marker+i)) + dubox(:,i,idim)+dubox(:,box_marker+i,idim) )
          ! interpolate derivatives: (1st order, F'(1) and F'(2) based)
          DO jdim=idim,1,-1
             dubox(:,i,jdim) = dubox(:,i,jdim)*(1-rel_x(idim)) + dubox(:,box_marker+i,idim)*rel_x(idim)
          END DO

       END DO
       box_marker = box_marker / 2
    END DO

    ! write interpolation result
    res(:) = ubox(:,1)

  END SUBROUTINE interpolate_to_box_3
  
  

  !-------------------------------------------------------------------------------------------
  SUBROUTINE get_xx_index( x, output_index )
    ! for a given points set correspondent ixyz index of the lower box corner,
    ! i.e. find index in xx(0:nxyz()) array for a given real x value
    USE wlt_vars  ! xyzlimts, dim
    REAL(pr), INTENT (IN) :: x(1:dim)
    INTEGER, INTENT (OUT) :: output_index(1:dim)
    INTEGER :: idim, min, max, mid
    REAL(pr) :: dxyz(1:dim)
    
    ! case of uniform grid
    dxyz(1:dim) = (xyzlimits(2,1:dim)-xyzlimits(1,1:dim))/ REAL(nxyz(1:dim),pr)
    output_index(1:dim) = INT( (x(1:dim)-xyzlimits(1,1:dim)) / dxyz(1:dim) )

    ! case of nonuniform grid
    DO idim=1,dim
       IF (grid(idim).NE.0) THEN
          min = 0;
          max = nxyz(idim)+1;
          DO WHILE (max-min.GT.1)
             mid = (max + min)/2;
             IF (xx(mid,idim).GT.x(idim)) THEN
                max = mid
             ELSE
                min = mid
             END IF
          END DO
          output_index(idim) = min
       END IF
    END DO

  END SUBROUTINE get_xx_index
  
  !
  !************ AUXILIARY INDEXING FUNCTIONS *********************
  !
  PURE FUNCTION ij2j (ij, j)
    USE precision
    IMPLICIT NONE 
    INTEGER, INTENT (IN) :: ij, j
    INTEGER :: ij2j
    INTEGER :: k
    k = 0

    DO WHILE (MOD(ij+2**k, 2**(k+1)) /= 0 .AND. k < j-1)
       k = k + 1
    END DO
    ij2j = j-k
  END FUNCTION ij2j

  PURE FUNCTION ixyz2j (ixyz, Jmx) RESULT(j)
    USE precision
    USE wlt_vars
    IMPLICIT NONE 
    INTEGER, INTENT (IN):: Jmx
    INTEGER, DIMENSION(dim), INTENT (IN):: ixyz
    INTEGER :: i, j, k
    j = 1
    DO i = 1,dim
       k = 0
       DO WHILE (MOD(ixyz(i)+2**k, 2**(k+1)) /= 0 .AND. k < Jmx-1)
          k = k + 1
       END DO
       j = MAX(j,Jmx-k)
    END DO

  END FUNCTION ixyz2j


  !
  !*************************************************************
  !
  FUNCTION wgh (k, i, i_l, i_h, istep, i_prd, nx, x)
    USE precision
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: k, i, i_l, i_h, istep, i_prd, nx
    REAL (pr), DIMENSION(0:nx), INTENT (IN) :: x
    REAL (pr) :: wgh
    INTEGER :: iprd1, iprd2, l, ip, ipm, kp
    REAL (pr) :: prd1, prd2

    IF(i_prd == 1) THEN
       iprd1 = 1
       iprd2 = 1
       DO l = i_l, k-1
          iprd1 = iprd1*(2*l+1)
          iprd2 = iprd2*2*(l-k)
       END DO
       DO l = k+1, i_h
          iprd1 = iprd1*(2*l+1)
          iprd2 = iprd2*2*(l-k)
       END DO
       wgh = REAL (iprd1, pr) / REAL (iprd2, pr)
       !PRINT *, '-f1-',wgh
    ELSE
       prd1 = 1.0_pr
       prd2 = 1.0_pr
       kp = i+(2*k+1)*istep
       DO ipm = i_l, k-1
          ip = i+(2*ipm+1)*istep
          prd1 = prd1*(x(i)-x(ip))
          prd2 = prd2*(x(kp)-x(ip))
       END DO
       DO ipm = k+1,i_h
          ip = i+(2*ipm+1)*istep
          prd1 = prd1*(x(i)-x(ip))
          prd2 = prd2*(x(kp)-x(ip))
       END DO
       wgh = prd1/prd2
       !PRINT *, '-f2-',wgh, i, ip, kp, ip
    END IF


  END FUNCTION wgh

  ! ------------------------------------------------------------------------
  ! this function has problems if compiled with ifort9.1.036 -O3 on 64 bit
  ! (ipm, i, i_l(idim), i_h(idim), step, prd_DB(idim), nxyz_DB(idim), xx_DB(0:nxyz_DB(idim),idim), h_DB(j,idim), order)
  FUNCTION diff_wgh (k, i, i_l, i_h, istep, i_prd, nx, x, h, order)
    USE precision
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: k, i, i_l, i_h, istep, i_prd, nx, order
    REAL (pr), DIMENSION(0:nx), INTENT (IN) :: x
    REAL (pr), INTENT (IN) :: h
    REAL (pr) :: diff_wgh
    INTEGER :: iprd1, iprd2, iprd3, l, ll, lr
    REAL (pr) :: prd1, prd2, prd3

    IF(order == 1) THEN
       IF(i_prd == 1) THEN
          IF (k == 0) THEN
             diff_wgh = 0.0_pr
             DO l = i_l, -1
                diff_wgh = diff_wgh - 1.0_pr / REAL (l, pr)
             END DO
             DO l = 1, i_h
                diff_wgh = diff_wgh - 1.0_pr / REAL (l, pr)
             END DO
          ELSE
             iprd1 = 1
             iprd2 = 1
             !********* Numerator **************
             DO l =  i_l, k-1
                iprd2 = iprd2*(l-k)
             END DO
             DO l = k+1, i_h
                iprd2 = iprd2*(l-k)
             END DO
             !********* Denominator **************
             DO l = i_l, MIN (0, k) - 1
                iprd1 = iprd1*l
             END DO
             DO l = MIN (0, k) + 1, MAX (0, k) - 1
                iprd1 = iprd1*l
             END DO
             DO l = MAX (0, k) + 1, i_h
                iprd1 = iprd1*l
             END DO
             diff_wgh = - REAL (iprd1, pr) / REAL (iprd2, pr)
          END IF
          diff_wgh=diff_wgh/h
       ELSE
          IF (k == 0) THEN
             diff_wgh = 0.0_pr
             DO l = i_l, -1
                diff_wgh = diff_wgh + 1.0_pr / ( x(i)-x(i+istep*l) )
             END DO
             DO l = 1, i_h
                diff_wgh = diff_wgh + 1.0_pr / ( x(i)-x(i+istep*l) )
             END DO
          ELSE
             prd1 = 1.0_pr
             prd2 = 1.0_pr
             !********* Numerator **************
             DO l =  i_l, k-1
                prd2 = prd2*( x(i+istep*k)-x(i+istep*l) )
             END DO
             DO l = k+1, i_h
                prd2 = prd2*( x(i+istep*k)-x(i+istep*l) )
             END DO
             !********* Denominator **************
             DO l = i_l, MIN (0, k) - 1
                prd1 = prd1*( x(i)-x(i+istep*l) )
             END DO
             DO l = MIN (0, k) + 1, MAX (0, k) - 1
                prd1 = prd1*( x(i)-x(i+istep*l) )
             END DO
             DO l = MAX (0, k) + 1, i_h
                prd1 = prd1*( x(i)-x(i+istep*l) )
             END DO
             diff_wgh = prd1/prd2
          END IF
       END IF
    ELSE IF(order == 2) THEN
       IF(i_prd == 1) THEN
          iprd1 = 0
          iprd2 = 1
          !*********Numerator **************
          DO l = i_l, k-1
             iprd2 = iprd2*(l-k)
          END DO
          DO l = k+1, i_h
             iprd2 = iprd2*(l-k)
          END DO
          !*********Denominator **************
          DO l = i_l, i_h
             IF (l /= k) THEN
                DO lr = i_l, i_h
                   IF (lr /= k .AND. lr /= l) THEN
                      iprd3 = 1
                      DO ll = i_l, i_h
                         IF (ll /= k .AND. ll /= lr .AND. ll /= l) THEN
                            iprd3 = iprd3*ll
                         END IF
                      END DO
                      iprd1 = iprd1 + iprd3
                   END IF
                END DO
             END IF
          END DO
          diff_wgh = REAL (iprd1, pr) / REAL (iprd2, pr)/ h**2
       ELSE
          prd1 = 0.0_pr
          prd2 = 1.0_pr
          !********* Numerator **************
          DO l =  i_l, k-1
             prd2 = prd2*( x(i+istep*k)-x(i+istep*l) )
          END DO
          DO l = k+1, i_h
             prd2 = prd2*( x(i+istep*k)-x(i+istep*l) )
          END DO
          !*********Denominator **************
          DO l = i_l, i_h
             IF (l /= k) THEN
                DO lr = i_l, i_h
                   IF (lr /= k .AND. lr /= l) THEN
                      prd3 = 1.0_pr
                      DO ll = i_l, i_h
                         IF (ll /= k .AND. ll /= lr .AND. ll /= l) THEN
                            prd3 = prd3*( x(i)-x(i+istep*ll) )
                         END IF
                      END DO
                      prd1 = prd1 + prd3
                   END IF
                END DO
             END IF
          END DO
          diff_wgh = prd1/prd2
       END IF
    END IF
  END FUNCTION diff_wgh


  PURE FUNCTION imp6(i) RESULT(iarray)
    IMPLICIT NONE 
    INTEGER, INTENT (IN) :: i
    INTEGER, DIMENSION(6):: iarray

    iarray(1)=INT(i/100000)                         
    iarray(2)=INT((i-100000*iarray(1))/10000)            
    iarray(3)=INT((i-100000*iarray(1)-10000*iarray(2))/1000)  
    iarray(4)=INT((i-100000*iarray(1)-10000*iarray(2)-1000*iarray(3))/100)  
    iarray(5)=INT((i-100000*iarray(1)-10000*iarray(2)-1000*iarray(3)-100*iarray(4))/10)  
    iarray(6)=INT((i-100000*iarray(1)-10000*iarray(2)-1000*iarray(3)-100*iarray(4)-10*iarray(5))) 

  END FUNCTION imp6

  !------------- auxiliary routines for exctracting arrays or data from indx_DB

  SUBROUTINE get_all_local_h (h_arr)
    USE precision
    USE sizes
    USE wlt_vars
    USE wlt_trns_vars

    IMPLICIT NONE
    REAL (pr), INTENT (INOUT)    :: h_arr(dim,nwlt)

    INTEGER, DIMENSION (dim)   :: ixyz
    INTEGER   :: i, k,     j, j_df, wlt_type, face_type, idim              
    INTEGER*8, DIMENSION(0:dim) :: i_p                                     

    i_p(0) = 1
    DO i=1,dim
       i_p(i) = i_p(i-1)*(1+nxyz(i))
    END DO
    DO j = 1, j_lev
       DO wlt_type = MIN(j-1,1),2**dim-1
          DO face_type = 0, 3**dim - 1
             DO j_df = j, j_lev
                DO k = 1, indx_DB(j_df,wlt_type,face_type,j)%length  
                   ixyz(1:dim) = INT(MOD(indx_DB(j_df,wlt_type,face_type,j)%p(k)%ixyz-1,i_p(1:dim))/i_p(0:dim-1))
                   i = indx_DB(j_df,wlt_type,face_type,j)%p(k)%i
                   DO idim = 1, dim
                   
!AR 2/22/2011!  The following 19 lines are added by AR                   
!                      IF ( MIN(ixyz(idim)+2**(j_lev-j_df),nxyz(idim)) .LT. 0 ) &                                               !AR 2/21/2011! added by AR
!                                                       PRINT *, 'get_all_local_h ', ixyz(idim), j_lev ,j_df, nxyz(idim)        !AR 2/21/2011! added by AR
!                      IF ( ixyz(idim) .LT. 0 ) THEN                                                                            !AR 2/21/2011! added by AR
!                                                       WRITE (*,'(A, 4I12)') 'get_all_local_h   idim   ixyz(1:dim) :: ', idim, ixyz(:) !AR 2/21/2011! added by AR
!                                                       WRITE (*,'(A, 7I12)') 'get_all_local_h   idim   i   i_INTEGER8   i_p(:) :: ', idim, i, i_INTEGER8, i_p(:) !AR 2/21/2011! added by AR
!                                                       !PRINT *, 'get_all_local_h ', ixyz(idim)                                        !AR 2/21/2011! added by AR
!                      END IF                                                                                                           !AR 2/21/2011! added by AR
!   N O T E :::
!                 The above WRITE statements revealed that  i_INTEGER8  becomes negative  
!                 Because i_INTEGER8 is   ixyz   in   indx_DB(...)%p(..)%ixyz       
!                 And   indx_DB(...)%p(..)%ixyz    is the "Global 1D non-adaptive index", similar to all those i_p(0:dim)
!
!                 SOLUTION:   ixyz  must be INTEGER*8  in  indx_list_DB  which is defined in  [ wavelet_3d_tree_vars.f90 ]
!
!                 QUESTIONs:  If we have   indx_DB(...)%p(..)%ixyz ,  why each time we are calculating the i_p(:) again ?
!                            In many places   indx_DB(...)%p(..)%ixyz   is used and many other places  i_p(:)
!
!                            And  i_p(:)  itself is both Global in   "MODULE db_tree"  ( in SUBROUTINE zone_wlt_DB & SUBROUTINE reconstr_check_DB & SUBROUTINE indices_DB )
!                                                   and at the same time defined  locally in all the other SUBROUTINES ?

                      h_arr(idim,i) = MAX ( xx(MIN(ixyz(idim)+2**(j_lev-j_df),nxyz(idim)),idim) - xx(ixyz(idim),idim), &
                           xx(ixyz(idim),idim) - xx(MAX(ixyz(idim)-2**(j_lev-j_df),0),idim) )
                   END DO
                END DO
             END DO
          END DO
       END DO
    END DO

  END SUBROUTINE get_all_local_h
  
  SUBROUTINE test_indx_DB (ulocal, nlocal, ne)
    USE precision
    USE sizes
    USE wlt_vars
    USE wlt_trns_vars
    
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: nlocal, ne
    REAL(pr), DIMENSION(nlocal, ne) :: ulocal
    INTEGER, DIMENSION (dim)   :: ixyz
    INTEGER :: i, k,     j, j_df, wlt_type, face_type
    INTEGER*8 :: i_INTEGER8
    INTEGER*8, DIMENSION(0:dim) :: i_p                                                           
    
    i_p(0) = 1
    DO i=1,dim
       i_p(i) = i_p(i-1)*(1+nxyz(i))
    END DO
    DO j = 1, j_lev
       DO wlt_type = MIN(j-1,1),2**dim-1
          DO face_type = 0, 3**dim - 1
             DO j_df = j, j_lev
                DO k = 1, indx_DB(j_df,wlt_type,face_type,j)%length
                   i_INTEGER8 = indx_DB(j_df,wlt_type,face_type,j)%p(k)%ixyz
                   ixyz(1:dim) = INT(MOD(i_INTEGER8-1,i_p(1:dim))/i_p(0:dim-1))
                   i = indx_DB(j_df,wlt_type,face_type,j)%p(k)%i
                   WRITE(*,'(4(I4,1x),3(I6,1x),E12.5)') j, wlt_type, face_type, j_df, k, i, i_INTEGER8, ulocal(i,1)
                END DO
             END DO
          END DO
       END DO
    END DO
    
  END SUBROUTINE test_indx_DB

  !  returns iloc with shifted indices of all points of given face_type and j <= jlev
  SUBROUTINE get_all_indices_by_face (face_type, jlev, nloc, iloc)
    USE precision
    USE sizes
    USE wlt_vars
    USE wlt_trns_vars

    IMPLICIT NONE
    INTEGER, INTENT (IN) :: face_type, jlev
    INTEGER, INTENT (OUT) :: nloc
    INTEGER, DIMENSION(nwlt), INTENT (OUT) :: iloc

    INTEGER :: dnloc
    INTEGER :: j, j_df, wlt_type

    nloc  = 0
    dnloc = 0
    DO j = 1, jlev
       DO wlt_type = MIN(j-1,1),2**dim-1
          DO j_df = j, j_lev
             dnloc = indx_DB(j_df,wlt_type,face_type,j)%length
             IF(dnloc > 0) THEN
                iloc(nloc+1:nloc+dnloc) = indx_DB(j_df,wlt_type,face_type,j)%p(1:dnloc)%i + indx_DB(jlev,wlt_type,face_type,j)%shift

!!$                ! error check REMOVE
!!$                IF( MINVAL(iloc(nloc+1:nloc+dnloc)) <= 0 ) THEN
!!$                   PRINT *,'ERROR in get_all_indices_by_face '
!!$                   STOP
!!$                END IF
                nloc = nloc + dnloc
             END IF
          END DO
       END DO
    END DO

  END SUBROUTINE get_all_indices_by_face

  !  returns iloc with shifted indices of all points of given types
  !  asssumes that data are provided on jlev <= j_lev
  SUBROUTINE get_indices_and_coordinates_by_types (wlt_types, face_types,&
       js, jlev, nloc, iloc, indx, deriv_lev_loc)
    USE precision
    USE sizes
    USE wlt_vars
    USE wlt_trns_vars
    IMPLICIT NONE
    INTEGER, DIMENSION(0:1), INTENT (IN) :: wlt_types, face_types, js
    INTEGER, INTENT(IN) :: jlev
    INTEGER, INTENT (OUT) :: nloc
    INTEGER, DIMENSION(nwlt), INTENT (OUT) :: iloc
    INTEGER, DIMENSION(nwlt,1:dim), INTENT (OUT) :: indx
    INTEGER, DIMENSION(nwlt), INTENT (OUT) :: deriv_lev_loc
    INTEGER :: wlt_type, face_type, k, j, j_df
    INTEGER*8, DIMENSION(0:dim) :: i_p                                                      
    INTEGER   :: dnloc                                                                           

    nloc  = 0
    dnloc = 0
    i_p(0) = 1
    DO k=1,dim
       i_p(k) = i_p(k-1)*(1+nxyz(k))
    END DO
    DO j = MAX(1,js(0)), MIN(jlev,js(1))
       DO face_type = MAX(0,face_types(0)),MIN(3**dim-1,face_types(1))
          DO wlt_type = MAX(0,wlt_types(0)),MIN(2**dim-1,wlt_types(1))
             DO j_df = j, j_lev
                dnloc = indx_DB(j_df,wlt_type,face_type,j)%length
                IF(dnloc > 0) THEN
                   DO k = 1,dnloc
                      iloc(nloc+k) = indx_DB(j_df,wlt_type,face_type,j)%p(k)%i + indx_DB(jlev,wlt_type,face_type,j)%shift
                      indx(nloc+k,1:dim) = INT(MOD(indx_DB(j_df,wlt_type,face_type,j)%p(k)%ixyz-1,i_p(1:dim))/i_p(0:dim-1))
                      deriv_lev_loc(nloc+k) = j_df ! derivatve level
                   END DO
                   nloc = nloc + dnloc
                END IF
             END DO
          END DO
       END DO
    END DO

  END SUBROUTINE get_indices_and_coordinates_by_types


END MODULE wlt_trns_util_mod
