MODULE io_3D_vars
  ! 
  ! Define some handy parameters to make calls more readable
  !
  LOGICAL, PARAMETER :: DO_RESTART_FALSE       = .FALSE.
  LOGICAL, PARAMETER :: DO_RESTART_TRUE        = .TRUE.
  LOGICAL, PARAMETER :: DO_READ_ALL_VARS_FALSE = .FALSE.
  LOGICAL, PARAMETER :: DO_READ_ALL_VARS_TRUE  = .TRUE.
  LOGICAL, PARAMETER :: DO_READ_COMMONFILE_FALSE = .FALSE.
  LOGICAL, PARAMETER :: DO_READ_COMMONFILE_TRUE  = .TRUE.
  

  INTEGER :: IC_restart_mode ! 0-new run; 1-hard restart; 2-soft restart; 3-restart from IC --------
  !                                                                                                 |
  ! Hard restart - restart a previous run (without changing any parameters)                         |
  ! Soft restart - ... (epsilons, names, j_mx, j_tree_root, etc, could be changed)                  |
  ! Restart from IC - some parameters can be changed and user initial conditions can be imposed     |
  ! ------------------------------------------------------------------------------------------------

  ! WARNING --------- temporal for compatibility -----------
  LOGICAL :: IC_restart, IC_from_file
  ! WARNING --------- temporal for compatibility -----------


  
  INTEGER   :: IC_RES_VERSION         ! VERSION of the IC_filename

  LOGICAL   :: IC_adapt_grid          ! If .FALSE., no grid addaptation is perfromed
                                      ! actiivated only in restart from IC and IC_file_fmt = 0
  LOGICAL   :: IC_SINGLE_LOOP         ! If .TRUE., adapts grid on initial conditions once, 
                                      ! the parameter is used if native file is used for initial conditions, but 
                                      ! but either epsilon is differnet or J_IC .NE. j_lev_init 
  LOGICAL   :: do_Sequential_run      ! If .TRUE., performs sequential run with  IC_restart_station  from  sequentialrun.log 
  
  INTEGER, PARAMETER, PRIVATE:: FLEN = 256    ! local length of file names
  
  CHARACTER (LEN=FLEN) :: IC_filename ! Name of the file to restart from
  INTEGER   :: IC_file_fmt            ! Format of restart file
  INTEGER   :: IC_restart_station     ! Station # to use when restarting from a restart file
  INTEGER   :: j_IC                   ! Limits resolution of IC

  LOGICAL :: res_save_coeff, &        ! Function or wavelet coefficient (TRUE - default) are stored in .res file
       res_save_coeff_IC              ! (res_save_coeff - read from .inp file, res_save_coeff_IC - read from .res file)
  
  INTEGER, POINTER :: IC_mxyz_global(:), IC_prd_global(:)

  CHARACTER(LEN=6) ,PARAMETER :: FILE_NUM_FMT = '(i4.4)'       ! format for making file number (some_case.XXXX.res)
  INTEGER   ,PARAMETER :: FILE_NUM_LEN = 4                     ! number of digits in saves file #, (MATCH FMT above!)
  CHARACTER(LEN=4) ,PARAMETER :: IT_STRING_PLACEHOLDER = '####'
  CHARACTER*1, PARAMETER :: slash = '/'


  !                                                        ! file units and names:
  INTEGER, PARAMETER :: UNIT_USER_STATS             = 11   ! ..._user_stats
  INTEGER, PARAMETER :: UNIT_LOG                    = 12   ! ..._log
  INTEGER, PARAMETER :: UNIT_USER_STATZ             = 13   ! ..._user_statz     !by Giuliano
  INTEGER, PARAMETER :: UNIT_SEQUENTIAL_RUN         = 17   ! sequentialrun.log
  INTEGER, PARAMETER :: UNIT_const_SGS_dissipation  = 18   ! const_SGS_dissipation.log
  CHARACTER (LEN=FLEN) :: file_name_user_stats
  CHARACTER (LEN=FLEN) :: file_name_log
  CHARACTER (LEN=FLEN) :: file_name_user_statz            !by Giuliano


  ! miscelaneous shared variables, like filenames
  PUBLIC
  CHARACTER (LEN=FLEN)  ::  file_gen, &  ! base of most of the output files, .inp parameter
       file_wlt, &                       ! base of .res filename as defined in io_3d.f90
       res_path, &                       ! results path (default './results')
       res_path_timestep, &              ! path to store timestep file ... 0000.res (default './results')
       file_name, &
       file_geom

  INTEGER, PARAMETER :: IT_STRING_LEN = 4
  CHARACTER (LEN=IT_STRING_LEN)   ::  it_string !string version of current iteration
  LOGICAL :: datafile_formatted ! set formated vs unformattred writes.
  INTEGER ,PARAMETER :: hdr_buf_len = 1024 !length of ascii solution file header

  ! variables for the lagrangian module
  LOGICAL :: use_lagrangian 			!flag to determine if the lagrangian module should be used
  CHARACTER (LEN=FLEN) :: lagrangian_file 	!name of input file that the lagrangian module uses

END MODULE io_3D_vars 
