!**************************************************************
! Begin shared module
!**************************************************************
MODULE precision
  IMPLICIT NONE
  INTEGER, PARAMETER, PUBLIC :: pr = KIND (1.0d0)
END MODULE precision


MODULE sizes
  USE precision
  PUBLIC
  INTEGER ::  nwlt, nwltj, nwlt_global       ! nwlt - # of active grid points
  INTEGER ::  nwlt_intrnl, nwlt_bnd
  INTEGER ::  nwlt_p_ghost                   ! # avtive wavelets plus all the ghost points
  INTEGER :: nwlt_old, nwltj_old
  REAL (pr) :: told                          ! used by Krylov time integration routine when RHS is explicitely time dependend
END MODULE sizes


MODULE util_vars
  USE precision
  PUBLIC
  REAL (pr), DIMENSION (:), ALLOCATABLE :: dA
  REAL (pr), DIMENSION (:,:), ALLOCATABLE :: dA_level
  REAL (pr) :: sumdA, sumdA_global ! sumdA = sum(dA) calculated in weigths routine (current processor/ global)
  REAL (pr) :: sumdA_computational, sumdA_computational_global ! the area integral of the computational space 
  REAL (pr), DIMENSION (:,:), ALLOCATABLE :: x ! NOTE: this should be replaced with a routine that returns x(1:nwlt,1:dim) if needed locally to a routine
END MODULE util_vars


MODULE field
  USE precision
  PUBLIC
  REAL (pr), DIMENSION (:,:), ALLOCATABLE, TARGET ::  u, u_ex, f
END MODULE field

!Oleg: this moduleneeds to be broken and uncommon parts ned to be moved to casefile
!no reason to declare nu, nu1 here
MODULE pde
  USE precision
  PUBLIC
  !--Time integration variables
  
  INTEGER   :: iwrite ! file number for saved soln file
  INTEGER   :: time_integration_method ! time integgration method to use (defined below)
  ! Supported time integration methods
  INTEGER  , PARAMETER ::  TIME_INT_Krylov          = 1, & !
                           TIME_INT_Crank_Nicholson = 2, & !
                           TIME_INT_RK              = 3, & !
                           TIME_INT_IMEX            = 4    !
  REAL (pr) :: dt, t, dt_original
  REAL(pr) :: timescl  ! global time scaling, set to 1.0 by default
  REAL (pr) :: t_begin ! intital time of simulation
  REAL (pr) :: t_end ! intital time of simulation
  REAL (pr) :: twrite
  REAL (pr) :: xmax, xmin, ymax, ymin, xlen, ylen
  LOGICAL   :: wlog
  LOGICAL   :: imask_obstacle ! TRUE - there is an obstacle defined
  LOGICAL   :: stationary_obstacle ! TRUE - if obstacle does not move and does not deform in time
  REAL (pr) :: Dpenal_factor
  LOGICAL   :: read_geometry  ! TRUE - the geometry is defined by geometry file defined in 
  INTEGER   :: it
  REAL (pr) :: tol1, tol2
  REAL (pr) :: tol3 ! used to set tolerance for time step
  REAL (pr) :: tol_gmres ! tol used in gmres iterations
  LOGICAL   :: tol_gmres_stop_if_larger ! If true stop iterating in gmres solver if error of last iteration was smalle
  LOGICAL :: first_loop_after_restart   ! true for first main time integration loop after restart
  REAL (pr) :: tolp
  REAL (pr) :: dtmax !maximum time step
  REAL (pr) :: dtmin !minimum time step
  REAL (pr), DIMENSION (:), POINTER :: scl_global  !vector of scales 
  REAL (pr), DIMENSION (:), ALLOCATABLE :: Umn !mean quantities, used in scales or equation

  REAL (pr), DIMENSION (:,:), POINTER :: u_old 

  ! These are indices that map the locations of variables at each time step 
  INTEGER, DIMENSION(:), ALLOCATABLE :: n_var_index

  INTEGER, DIMENSION(:), ALLOCATABLE :: n_var_wlt_fmly ! wavelet transform family for each variable

  INTEGER   ::  Scale_Meth !1- Linf, 2-L2 vector, 3 - L2 in physical space 
  INTEGER   ::  Weights_Meth ! # Weights_Meth, 0: dA=area/nwlt, 1: normal

  INTEGER :: sgsmodel ! 0=no model, 1=fixed parameter smagorinski
  INTEGER :: hypermodel ! 0=no hyperbolic module, 1=user hyperbolic module
  INTEGER :: J_hyper ! level above which the numerical viscosity for hyperbolic module is interduced., default value J_hyper=J_mx
  LOGICAL :: savevisc   ! T numerical viscosity is saved
  REAL (pr), DIMENSION (:,:) , ALLOCATABLE :: sgs_mdl_force
  REAL (pr) :: total_KE ! calculated in resolved_stats()

  ! Spatial Adaptive Epsilon
  LOGICAL                  :: do_Adp_Eps_Spatial         ! Perform  Spatial Adaptation  for  Epsilon
  INTEGER                  :: n_var_Epsilon              ! Start Index of Epsilon (defined as an Additional Variable)  in u array 

  REAL (pr), ALLOCATABLE   :: eps_in_SpatialSpace(:)     ! A Global Array for  eps_in_SpatialSpace(:)  [ NOTE :: This is in addition to  u(:,n_var_Epsilon) ]

  LOGICAL, DIMENSION(:,:), ALLOCATABLE :: n_var_adapt_eps   ! Logical map into Variables in u(,) used in 
                                                            ! wavelet adaptation
                                                            ! using  spatial-variable-thresholding  to determine new adaptive grid

  ! Spatial Adaptive Epsilon   Evolution
  LOGICAL                  :: do_Adp_Eps_Spatial_Evol    ! Perform  Spatial Adaptation  for  Epsilon   Usinf An Evolution Equation
  INTEGER                  :: n_var_EpsilonEvol          ! Start Index of Epsilon (defined as an Integrated Variable)  in u array 

END MODULE pde


MODULE penalization
  USE precision
  PUBLIC
  REAL (pr) :: eta_chi                           !penalization parameter
  REAL (pr), DIMENSION (:), ALLOCATABLE :: penal !penalization function
!**************** already set up in module distance *****************************************
!  INTEGER :: ndist=0
!  LOGICAL :: interpolate_dist, use_dist
!  REAL (pr), DIMENSION (:), ALLOCATABLE :: dist  !distance function used with penalization 
!  REAL (pr), DIMENSION (:, :), ALLOCATABLE :: norm
!********************************************************************************************  
  REAL (pr), DIMENSION (:, :), ALLOCATABLE :: u_target
  INTEGER, DIMENSION(:), ALLOCATABLE :: penal_color
END MODULE penalization


MODULE share_consts
  USE precision
  PUBLIC
  REAL (pr), PARAMETER :: pi = 4.0_pr * ATAN (1.0_pr) !3.1415926535897932_pr
END MODULE share_consts

! kry_l   is the linear Krylov dimension kry_l is normally taken quite large (e.g. 15 or 20).  
! kry_m   is the Krylov dimension used to solve the nonlinear part of the propagator.  
!         Edwards et al. found empirically that small dimensions (e.g. kry_m=2) are optimal, 
!         but that could depend on the type on nonlinear operator.
! j_it    is the number of functional iterations used to solve (3.30) in Edwards et al.  
!         Again, they find j_it = 2 is optimal.  One could also test for convergence 
!         (i.e. iterating until the error is less than tolerance).
! n_grid  is number of points used to evaluate time integral of nonlinear part.
MODULE share_kry
  !--Needs DGEEV, ZGETRF, ZGETRI from LAPACK
  USE precision
  IMPLICIT NONE
  PUBLIC
  INTEGER, PARAMETER :: kry_l = 8, kry_m = 2, n_grid = 2, j_it = 2
  INTEGER :: n, ng, ne
  REAL (pr) :: tol
  REAL (pr), DIMENSION (:), ALLOCATABLE :: u_prev_timestep ! velocity at previous time step
  REAL (pr), DIMENSION (:), ALLOCATABLE :: u_for_BCs 
  REAL (pr) :: t_adapt  ! up to this time the delta t is non-adaptive
  INTEGER :: IMEXswitch = 0 ! during IMEX, this switch will represent IM (=1) or EX (=-1)
  INTEGER :: RKtype, IMEXtype, imsteps, exsteps
  REAL (pr), DIMENSION (4,4) :: aim, aex
  REAL (pr), DIMENSION (4) :: bim, bex
END MODULE share_kry

!*********************************************************************************
!                       unused variables (eventually need to be deleted)
!*********************************************************************************
MODULE pde_unused
  USE precision
  PUBLIC
  !--Time integration variables
  REAL (pr) :: theta, u0, nu1, theta1, nub
  REAL (pr), PARAMETER :: r0 = 10.0_pr
  REAL (pr) :: x0, y0,z0, x1, y1, z1
  REAL (pr) ::  omega,phi,A,S,t00

  INTEGER, PARAMETER :: inl = 0, ivis = 1 !--Nonlinear and viscous term forms
  INTEGER   :: p0 = 1, p1 = 2             !--Time level counters for obstacle integration 
  REAL (pr) :: rho, Uinf
  REAL (pr), DIMENSION (1:3) :: Uo, Xo
  REAL (pr), DIMENSION (:), ALLOCATABLE :: beta
  REAL (pr), DIMENSION (:,:), ALLOCATABLE :: sol_rhs, u_star
  LOGICAL   :: wlog
  LOGICAL   :: ICrestart
  LOGICAL   :: imask_obstacle ! TRUE - there is an obstacle defined
  INTEGER   :: itime ! Order of time integration used in meth2 routines
  INTEGER   :: lch, nb
  REAL (pr) :: b, b_star,  k0, k_star, mass, m_star
  REAL (pr) :: dtcfl, dtforce, energy
  INTEGER, DIMENSION (1:3) :: imv ! flag to determine if an object is movable in a given dimension,
  INTEGER,  PARAMETER :: Base_case_meth2  = 10
  REAL (pr), DIMENSION (1:3,2) :: Uo_rhs, Xo_rhs
  REAL (pr) :: Pforce(1:3)  ! pressure forcing term
  REAL (pr) :: dtmax !maximum time step
  REAL (pr) :: dtmin !minimum time step
  LOGICAL, DIMENSION(:,:), ALLOCATABLE :: n_var_adapt       ! Logical map into Variables in u(,) used in 
                                                            ! wavelet adaptation
                                                            ! thresholding to determine new adaptive grid

  LOGICAL, DIMENSION(:,:), ALLOCATABLE :: n_var_interpolate ! Logical map into Variables in u(,)
                                                            ! that must be interpolated to
                                                            ! the new grid each time step.

  LOGICAL, DIMENSION(:,:), ALLOCATABLE :: n_var_exact_soln  ! Logical map into Variables in u(,)
                                                            ! that should be checked against an exact solution

  LOGICAL, DIMENSION(:), ALLOCATABLE ::  n_var_save         ! Logical map into Variables in u(,)
                                                            ! that are saved to restart/result file

  LOGICAL, DIMENSION(:), ALLOCATABLE ::  n_var_req_restart  ! Logical map into Variables in u(,)
                                                            ! that are required for a restart from a result file
														  
  ! These are indices that map the locations of variables at each time step 
  INTEGER, DIMENSION(:), ALLOCATABLE :: n_var_index

  INTEGER   ::  Scale_Meth !1- Linf, 2-L2

!  INTEGER ,PARAMETER :: u_variable_name_len = 32
!  CHARACTER (LEN=u_variable_name_len) , DIMENSION(:), ALLOCATABLE :: u_variable_names
!  CHARACTER (LEN=u_variable_name_len) ::  tmp_name_str
!  CHARACTER (LEN=10)  u_variable_names_fmt 

END MODULE pde_unused

MODULE variable_name_vars
  PUBLIC
  INTEGER, PARAMETER :: u_variable_name_len = 32
END MODULE variable_name_vars


MODULE unused ! module of variables NO LONGER IN USE, but left for backward comaptibility
  IMPLICIT NONE
  INTEGER   :: n0 = 1, n1=0 , n2=0               !NO LONGER USED, left as dummy argument for backward case compatibility
  INTEGER :: n_time_levels=1  !--number of time levels in time integration (NO LONGER USED, left as dummy argument for backward case compatibility)
END MODULE unused

MODULE shared_functions
  IMPLICIT NONE

  PUBLIC :: logical2int

CONTAINS

  FUNCTION logical2int(a)

    LOGICAL :: a
    INTEGER :: logical2int
    
    logical2int = a
    logical2int = -1*abs(logical2int)
    
  END FUNCTION logical2int

END MODULE shared_functions


!**************************************************************
! End shared modules
!**************************************************************
