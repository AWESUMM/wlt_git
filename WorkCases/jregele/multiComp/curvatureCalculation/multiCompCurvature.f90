MODULE user_case
  !!$ --- 1-2-3D Case --- !!$
  USE precision
  USE wlt_vars
  USE wavelet_filters_mod
  USE elliptic_mod
  USE elliptic_vars
  USE wlt_trns_mod
  USE wlt_trns_vars
  USE wlt_trns_util_mod
  USE io_3d_vars
  USE util_mod
  USE util_vars
  USE share_consts
  USE pde
  USE sizes
  USE share_kry
  USE vector_util_mod
  USE field
  USE input_file_reader
  USE debug_vars
  USE parallel
  USE hyperbolic_solver

  IMPLICIT NONE

  !
  ! case specific variables if there is any
  !
  REAL (pr),PRIVATE :: Re  ! Reynolds number
  REAL (pr),PRIVATE :: Pra  ! Prandtle number
  REAL (pr),PRIVATE :: gamma ! Gamma number C_p / C_v
  REAl (pr),PRIVATE :: m_visc !! power law for viscosity
  REAL (pr),PRIVATE :: Sc !! Schmidt Number
  REAL (pr) :: delta, xL, xR, radius
  REAL (pr) :: pi_inf

  INTEGER :: n_var_eng,n_var_den,n_var_pressure,n_var_alpha1D1,n_var_temp,n_var_alpha1,n_var_kappa
  INTEGER, DIMENSION(:), ALLOCATABLE :: n_var_vel 

CONTAINS

  !
  ! Set up the variables
  !

SUBROUTINE user_setup_pde (VERB)
  IMPLICIT NONE
  LOGICAL, OPTIONAL :: VERB         ! print debug info
  INTEGER :: i,j

    IF (verb_level.GT.0) THEN
       PRINT * ,''
       PRINT *, '**********************Setting up PDE*****************'
       PRINT * ,	'CASE:1-2-3D Euler Case'
       PRINT *, '*****************************************************'
    END IF

    n_integrated = 2 + dim + 1    ! 2 for density&Energy + dim ( velocity) + 1 for one component alpha1
    
    n_var_additional = 1 + 1 + 1 + 1  ! pressure + temperture + alpha1 + curvature kappa

    n_var = n_integrated + n_var_additional !--Total number of variables

    n_var_exact = 0
    
    n_var_den = 1  ! density
    
    IF(.NOT.ALLOCATED(n_var_vel)) THEN
       ALLOCATE (n_var_vel(dim))
       DO i=1,dim
          n_var_vel(i) = i+1
       END DO
    END IF

    IF(hypermodel /= 0) THEN
       n_var_mom = n_var_vel;
    END IF

    !!$ Integrated variables
    n_var_eng      = 1 + dim    + 1     ! Energy term
    n_var_alpha1D1 = n_var_eng + 1      ! alpha1*Density

    !!$ Additional variables
    n_var_pressure  = n_integrated + 1   ! Pressure
    n_var_temp      = n_integrated + 2   ! Temperature
    n_var_alpha1    = n_integrated + 3   ! alpha1
    n_var_kappa     = n_integrated + 4   ! curvature kappa

    !
    !--Allocate logical arrays
    !
    CALL alloc_variable_mappings 

    IF( dim == 3 ) THEN
       WRITE (u_variable_names(n_var_den     ), u_variable_names_fmt) 'Density  '
       WRITE (u_variable_names(n_var_vel(1)  ), u_variable_names_fmt) 'X-Momentum  '
       WRITE (u_variable_names(n_var_vel(2)  ), u_variable_names_fmt) 'Y-Momentum  '
       WRITE (u_variable_names(n_var_vel(3)  ), u_variable_names_fmt) 'Z-Momentum  '
       WRITE (u_variable_names(n_var_eng     ), u_variable_names_fmt) 'Energy  '
       WRITE (u_variable_names(n_var_alpha1D1), u_variable_names_fmt) 'Alpha1D1'

    ELSE IF(dim == 2) THEN
       WRITE (u_variable_names(n_var_den     ), u_variable_names_fmt) 'Density  '
       WRITE (u_variable_names(n_var_vel(1)  ), u_variable_names_fmt) 'X-Momentum  '
       WRITE (u_variable_names(n_var_vel(2)  ), u_variable_names_fmt) 'Y-Momentum  '
       WRITE (u_variable_names(n_var_eng     ), u_variable_names_fmt) 'Energy  '
       WRITE (u_variable_names(n_var_alpha1D1), u_variable_names_fmt) 'Alpha1D1'

    ELSE IF(dim == 1) THEN
       WRITE (u_variable_names(n_var_den     ), u_variable_names_fmt) 'Density  '
       WRITE (u_variable_names(n_var_vel(1)  ), u_variable_names_fmt) 'X-Momentum  '
       WRITE (u_variable_names(n_var_eng     ), u_variable_names_fmt) 'Energy  '
       WRITE (u_variable_names(n_var_alpha1D1), u_variable_names_fmt) 'Alpha1D1  '

    END IF
    WRITE (u_variable_names(n_var_pressure),u_variable_names_fmt) 'Pressure'
    WRITE (u_variable_names(n_var_temp    ),u_variable_names_fmt) 'Temperature'
    WRITE (u_variable_names(n_var_alpha1    ),u_variable_names_fmt) 'Alpha1'
    WRITE (u_variable_names(n_var_kappa    ),u_variable_names_fmt) 'Curvature'

    !
    !--Setup which components we will base grid adaptation on.
    !

    n_var_adapt = .FALSE. !intialize
    n_var_adapt(1:n_integrated,0)     = .TRUE. !--Initially adapted variables at first time level
    n_var_adapt(1:n_integrated,1)     = .TRUE. !--After first time step adapt on 

    !--integrated variables at first time level
    !Variables that need to be interpoleted to new adapted grid in initial grid adaptation

    n_var_interpolate        = .FALSE. !intialize
    n_var_interpolate(1:n_integrated+n_var_additional,0) = .TRUE.  

    !Variables that need to be interpoleted to new adapted grid at each time step
    n_var_interpolate(1:n_integrated+n_var_additional,1) = .TRUE. 

    !
    ! setup which components we have an exact solution for
    !

    n_var_exact_soln = .FALSE. !intialize
    n_var_exact_soln(1,0:1) = .FALSE. !.TRUE.

    !
    ! setup which variables we will save the solution
    !

    n_var_save = .FALSE. !intialize 
    n_var_save(1:n_var) = .TRUE. ! save all for restarting code

    !
    ! Set the maximum number of components for which we have an exact solution
    !

    n_var_exact = MAX( COUNT(n_var_exact_soln(:,0)),COUNT(n_var_exact_soln(:,1)) )

    !
    ! Setup a scaleCoeff array of we want to tweak the scaling of a variable
    ! ( if scaleCoeff < 1.0 then more points will be added to grid 
    !

    ALLOCATE ( scaleCoeff(1:n_var), Umn(1:n_var) )
    scaleCoeff = 1.0_pr

    IF (VERB) THEN
       PRINT *, 'n_integrated = ',n_integrated 
       PRINT *, 'n_var = ',n_var 
       PRINT *, '*******************Variable Names*******************'
       DO i = 1,n_var
          WRITE (*, u_variable_names_fmt) u_variable_names(i)
       END DO
       PRINT *, '****************************************************'
    END IF

  END SUBROUTINE  user_setup_pde

  !
  ! Set the exact solution for comparison to the simulated solution
  !
  ! u          - array to fill in the exact solution
  ! nlocal       - number of active wavelets  
  ! ne_local        - total number of equations
  ! t          - time of current time step
  ! l_n_var_exact_soln_index - index into the elements of u for which we need to
  !                            find the exact solution

  SUBROUTINE  user_exact_soln (u, nlocal,  t_local, l_n_var_exact_soln)  
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: nlocal
    REAL (pr), INTENT (IN) ::  t_local
    REAL (pr), DIMENSION (nlocal,n_var_exact), INTENT (INOUT) :: u
    LOGICAL , INTENT (IN) :: l_n_var_exact_soln(n_var)

  END SUBROUTINE  user_exact_soln

  !
  ! Setting up the intial condition for the problem
  !
  
  SUBROUTINE user_initial_conditions (u, nlocal, ne_local, t_local, scl, scl_fltwt,iter)
    IMPLICIT NONE

    INTEGER, INTENT (IN) :: nlocal, ne_local
    INTEGER  , INTENT (INOUT) :: iter ! iteration of call while adapting initial grid
    REAL (pr), DIMENSION (nlocal,ne_local), INTENT (INOUT) :: u
    REAL (pr), DIMENSION(nwlt) :: r2 , theta
    REAL (pr)  :: scl(1:n_var),scl_fltwt
    REAL (pr), INTENT (IN) :: t_local
    INTEGER :: i,j,k
    REAL (pr), DIMENSION(dim) :: lMAX !! size of the domain
    REAL (pr), DIMENSION (nlocal) :: p,rho, Uy,Ux,Ya, fun

    IF (.NOT. IC_restart) THEN
       
!!$ ----------- TEST 1D and 2D shock problem ------------- !!$
      IF (dim ==2) THEN
         ! Weak Shock tube problem
         fun = 0.0_pr
         DO i=1,nlocal
            IF ( SUM(x(i,1:dim)**2) .LT. xR**2 ) THEN
               fun(i)=1.0_pr
            END IF
         END DO

         rho = fun*(1.0_pr-0.125_pr)+0.125_pr
         p = fun*0.9_pr+0.1_pr

         u(:,n_var_den) = rho
         u(:,n_var_alpha1D1) = rho * fun
         u(:,n_var_vel(1:dim)) = 0.0_pr
         u(:,n_var_eng) = (p(:) + gamma*pi_inf)/(gamma-1.0_pr) + 0.5_pr*(SUM(u(:,n_var_vel(1:dim))**2,DIM=2))/rho
         
      ELSE IF (dim ==1) THEN
          fun = 0.5_pr*(1.0_pr-tanh((x(:,1)-xR)/delta)) - 0.5_pr*(1.0_pr-tanh((x(:,1)+xR)/delta))

!         fun = 0.5_pr*(1.0_pr-tanh((x(:,1))/delta))
         rho = fun*(1.0_pr-0.125_pr)+0.125_pr
         p = fun*0.9_pr+0.1_pr

         u(:,n_var_den) = rho
         u(:,n_var_alpha1D1) = rho * fun
         u(:,n_var_vel(1:dim)) = 0.0_pr
         u(:,n_var_eng) = (p(:)+gamma*pi_inf)/(gamma-1.0_pr) + 0.5_pr*u(:,n_var_vel(1))**2/rho         
       
!!$         IF (verb_level.GT.0) THEN
!!$            print*,'xmax ', maxval(x(:,1)), 'ymax ', maxval(x(:,2))
!!$
!!$            WRITE( *,'( "in user_IC t_local, nwlt " , f30.20, 1x , i5.5 )' ) t_local , nlocal
!!$         END IF
      END IF
   END IF
                      
!!$     print* ,'****---INITIAL CONDITION VARIABLES TEST ---****' 
!!$     print* ,'density  min: ' , minval(u(:,1))
!!$     print* ,'energy   min: ' , minval(u(:,4))
!!$     print* ,'velocity min: ' , minval(u(:,2))
!!$     print* ,'velocity min: ' , minval(u(:,3))
 
  END SUBROUTINE user_initial_conditions

  !--********************************  
  ! Arguments
  ! u         - field on adaptive grid
  ! nlocal      - number of active points
  ! ne_local       - number of equations
  ! t         - current time
  !
  ! Global variables used
  ! ifn       - number of equations
  ! x()
  ! xO
  ! y0
  !--******************************** 

  SUBROUTINE user_algebraic_BC (Lu, u, nlocal, ne_local, jlev, meth)
    !--Defines boundary condition type
    IMPLICIT NONE

    INTEGER , INTENT (IN) :: jlev, meth, ne_local, nlocal
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (INOUT) :: Lu
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (IN)    :: u

    INTEGER :: ie, i, ii, shift, denshift, eshift
    INTEGER, DIMENSION (dim) :: velshift
    REAL (pr), DIMENSION (ne_local,nlocal,dim+2) :: du, d2u !uncomment if Neuman BC are used
    REAL (pr), DIMENSION (1,nlocal,dim) :: du_prev
    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc
    
    !if Neuman BC are used, need to call 
    CALL c_diff_fast(u, du, d2u, jlev, nlocal, meth, 10, ne_local, 1, ne_local)
    
    DO ie = 1, ne_local
       shift = nlocal*(ie-1)
       !--Go through all Boundary points that are specified
       i_p_face(0) = 1
       DO i=1,dim
          i_p_face(i) = i_p_face(i-1)*3
       END DO
       DO face_type = 0, 3**dim - 1
          face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
          IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
             CALL get_all_indices_by_face (face_type, jlev, nloc, iloc)
             IF(nloc > 0 ) THEN
		! ************* Xmin Face ****************!
                IF(face(1) == -1) THEN   ! Entire Xmin face
                   IF(ie == 1) THEN        				
                      Lu(shift+iloc(1:nloc)) =du(ie,iloc(1:nloc),1)!! D BC: u(shift+iloc(1:nloc)) // N BC: du(ie,iloc(1:nloc),1)
                   ELSE IF (ie == 2) THEN                                
                      Lu(shift+iloc(1:nloc)) =du(ie,iloc(1:nloc),1)!! D BC: u(shift+iloc(1:nloc)) // N BC: du(ie,iloc(1:nloc),1)
                   ELSE IF (ie == 3) THEN                                
                      Lu(shift+iloc(1:nloc)) =du(ie,iloc(1:nloc),1)!! D BC: u(shift+iloc(1:nloc)) // N BC: du(ie,iloc(1:nloc),1) 
		   ELSE IF (ie == 4) THEN  			        
                      Lu(shift+iloc(1:nloc)) =du(ie,iloc(1:nloc),1)!! D BC: u(shift+iloc(1:nloc)) // N BC: du(ie,iloc(1:nloc),1)
                   ELSE IF (ie == 5) THEN
                      Lu(shift+iloc(1:nloc)) =du(ie,iloc(1:nloc),1)!! D BC: u(shift+iloc(1:nloc)) // N BC: du(ie,iloc(1:nloc),1)
                   ELSE IF (ie == 6) THEN
                      Lu(shift+iloc(1:nloc)) =du(ie,iloc(1:nloc),1)!! D BC: u(shift+iloc(1:nloc)) // N BC: du(ie,iloc(1:nloc),1)
                   END IF
		! ************* Xmax Face ****************!
                ELSE IF(face(1) == 1) THEN   ! Entire Xmax face
                   IF(ie == 1) THEN        				
                      Lu(shift+iloc(1:nloc)) =du(ie,iloc(1:nloc),1)!! D BC: u(shift+iloc(1:nloc)) // N BC: du(ie,iloc(1:nloc),1)
                   ELSE IF (ie == 2) THEN                               
                      Lu(shift+iloc(1:nloc)) =du(ie,iloc(1:nloc),1)!! D BC: u(shift+iloc(1:nloc)) // N BC: du(ie,iloc(1:nloc),1)
                   ELSE IF (ie == 3) THEN                                
                      Lu(shift+iloc(1:nloc)) =du(ie,iloc(1:nloc),1)!! D BC: u(shift+iloc(1:nloc)) // N BC: du(ie,iloc(1:nloc),1) 
		   ELSE IF (ie == 4) THEN  			        
                      Lu(shift+iloc(1:nloc)) =du(ie,iloc(1:nloc),1)!! D BC: u(shift+iloc(1:nloc)) // N BC: du(ie,iloc(1:nloc),1)
                   ELSE IF (ie == 5) THEN
                      Lu(shift+iloc(1:nloc)) =du(ie,iloc(1:nloc),1)!! D BC: u(shift+iloc(1:nloc)) // N BC: du(ie,iloc(1:nloc),1)
                   ELSE IF (ie == 6) THEN
                      Lu(shift+iloc(1:nloc)) =du(ie,iloc(1:nloc),1)!! D BC: u(shift+iloc(1:nloc)) // N BC: du(ie,iloc(1:nloc),1)
                   END IF
                END IF
                IF(dim >=2) THEN
                   ! ************* Ymin Face ****************!
                   IF(face(2) == -1) THEN   ! Entire Ymin face
                      IF(ie == 1) THEN        				
                         Lu(shift+iloc(1:nloc)) =du(ie,iloc(1:nloc),2) !! D BC: u(shift+iloc(1:nloc)) // N BC: du(ie,iloc(1:nloc),2)
                      ELSE IF (ie == 2) THEN                               
                         Lu(shift+iloc(1:nloc)) =du(ie,iloc(1:nloc),2) !! D BC: u(shift+iloc(1:nloc)) // N BC: du(ie,iloc(1:nloc),2)
                      ELSE IF (ie == 3) THEN                                
                         Lu(shift+iloc(1:nloc)) =du(ie,iloc(1:nloc),2) !! D BC: u(shift+iloc(1:nloc)) // N BC: du(ie,iloc(1:nloc),2) 
                      ELSE IF (ie == 4) THEN  			        
                         Lu(shift+iloc(1:nloc)) =du(ie,iloc(1:nloc),2) !! D BC: u(shift+iloc(1:nloc)) // N BC: du(ie,iloc(1:nloc),2)
                      ELSE IF (ie == 5) THEN
                         Lu(shift+iloc(1:nloc)) =du(ie,iloc(1:nloc),2) !! D BC: u(shift+iloc(1:nloc)) // N BC: du(ie,iloc(1:nloc),2)
                      ELSE IF (ie == 6) THEN
                         Lu(shift+iloc(1:nloc)) =du(ie,iloc(1:nloc),2) !! D BC: u(shift+iloc(1:nloc)) // N BC: du(ie,iloc(1:nloc),2)
                      END IF
                   ! ************* Ymax Face ****************!
                   ELSE IF(face(2) == 1) THEN   ! Entire Ymax face
                      IF(ie == 1) THEN        				
                         Lu(shift+iloc(1:nloc)) =du(ie,iloc(1:nloc),2)!! D BC: u(shift+iloc(1:nloc)) // N BC: du(ie,iloc(1:nloc),2)
                      ELSE IF (ie == 2) THEN                               
                         Lu(shift+iloc(1:nloc)) =du(ie,iloc(1:nloc),2)!! D BC: u(shift+iloc(1:nloc)) // N BC: du(ie,iloc(1:nloc),2)
                      ELSE IF (ie == 3) THEN                                
                         Lu(shift+iloc(1:nloc)) =du(ie,iloc(1:nloc),2)!! D BC: u(shift+iloc(1:nloc)) // N BC: du(ie,iloc(1:nloc),2) 
                      ELSE IF (ie == 4) THEN  			        
                         Lu(shift+iloc(1:nloc)) =du(ie,iloc(1:nloc),2)!! D BC: u(shift+iloc(1:nloc)) // N BC: du(ie,iloc(1:nloc),2)
                      ELSE IF (ie == 5) THEN
                         Lu(shift+iloc(1:nloc)) =du(ie,iloc(1:nloc),2)!! D BC: u(shift+iloc(1:nloc)) // N BC: du(ie,iloc(1:nloc),2)
                      ELSE IF (ie == 6) THEN
                         Lu(shift+iloc(1:nloc)) =du(ie,iloc(1:nloc),2)!! D BC: u(shift+iloc(1:nloc)) // N BC: du(ie,iloc(1:nloc),2)
                      END IF
                   END IF
                END IF
                IF(dim ==3) THEN
                   ! ************* Zmin Face ****************!
                   IF(face(3) == -1) THEN   ! Entire Zmin face
                      IF(ie == 1) THEN        				
                         Lu(shift+iloc(1:nloc)) = u(shift+iloc(1:nloc))!! D BC: u(shift+iloc(1:nloc)) // N BC: du(ie,iloc(1:nloc),3)
                      ELSE IF (ie == 2) THEN                               
                         Lu(shift+iloc(1:nloc)) = u(shift+iloc(1:nloc))!! D BC: u(shift+iloc(1:nloc)) // N BC: du(ie,iloc(1:nloc),3)
                      ELSE IF (ie == 3) THEN                                
                         Lu(shift+iloc(1:nloc)) = u(shift+iloc(1:nloc))!! D BC: u(shift+iloc(1:nloc)) // N BC: du(ie,iloc(1:nloc),3) 
                      ELSE IF (ie == 4) THEN  			        
                         Lu(shift+iloc(1:nloc)) = u(shift+iloc(1:nloc))!! D BC: u(shift+iloc(1:nloc)) // N BC: du(ie,iloc(1:nloc),3)
                      ELSE IF (ie == 5) THEN
                         Lu(shift+iloc(1:nloc)) = u(shift+iloc(1:nloc))!! D BC: u(shift+iloc(1:nloc)) // N BC: du(ie,iloc(1:nloc),3)
                      ELSE IF (ie == 6) THEN
                         Lu(shift+iloc(1:nloc)) = u(shift+iloc(1:nloc))!! D BC: u(shift+iloc(1:nloc)) // N BC: du(ie,iloc(1:nloc),3)
                      END IF
                   ! ************* Zmax Face ****************!
                   ELSE IF(face(3) == 1) THEN   ! Entire Zmax face
                      IF(ie == 1) THEN        				
                         Lu(shift+iloc(1:nloc)) = u(shift+iloc(1:nloc))!! D BC: u(shift+iloc(1:nloc)) // N BC: du(ie,iloc(1:nloc),3)
                      ELSE IF (ie == 2) THEN                               
                         Lu(shift+iloc(1:nloc)) = u(shift+iloc(1:nloc))!! D BC: u(shift+iloc(1:nloc)) // N BC: du(ie,iloc(1:nloc),3)
                      ELSE IF (ie == 3) THEN                                
                         Lu(shift+iloc(1:nloc)) = u(shift+iloc(1:nloc))!! D BC: u(shift+iloc(1:nloc)) // N BC: du(ie,iloc(1:nloc),3) 
                      ELSE IF (ie == 4) THEN  			        
                         Lu(shift+iloc(1:nloc)) = u(shift+iloc(1:nloc))!! D BC: u(shift+iloc(1:nloc)) // N BC: du(ie,iloc(1:nloc),3)
                      ELSE IF (ie == 5) THEN
                         Lu(shift+iloc(1:nloc)) = u(shift+iloc(1:nloc))!! D BC: u(shift+iloc(1:nloc)) // N BC: du(ie,iloc(1:nloc),3)
                      ELSE IF (ie == 6) THEN
                         Lu(shift+iloc(1:nloc)) = u(shift+iloc(1:nloc))!! D BC: u(shift+iloc(1:nloc)) // N BC: du(ie,iloc(1:nloc),3)
                      END IF
                   END IF
                END IF
             END IF
          END IF
       END DO
    END DO

  END SUBROUTINE user_algebraic_BC

  SUBROUTINE user_algebraic_BC_diag (Lu_diag, nlocal, ne_local, jlev, meth)
    !--Defines boundary condition type
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: jlev, meth, ne_local, nlocal
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (INOUT) :: Lu_diag

    INTEGER :: ie, i, ii, shift, denshift, eshift
    INTEGER, DIMENSION (dim) :: velshift

    REAL (pr), DIMENSION (nlocal,dim+2) :: du, d2u  !uncomment if Neuman BC are used
    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc
        
    !if Neuman BC are used, need to call 
    CALL c_diff_diag ( du, d2u, jlev, nlocal, meth, meth, 10)
    
    DO ie = 1, ne_local
       shift = nlocal*(ie-1)
       !--Go through all Boundary points that are specified
       i_p_face(0) = 1
       DO i=1,dim
          i_p_face(i) = i_p_face(i-1)*3
       END DO
       DO face_type = 0, 3**dim - 1
          face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
          IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
             CALL get_all_indices_by_face (face_type, jlev, nloc, iloc)
             IF(nloc > 0 ) THEN
		
		! ************* Xmin *********************** !
                IF( face(1) == -1 ) THEN   ! Entire Xmin face
                   IF(ie == 1) THEN                                                     
                     Lu_diag(shift+iloc(1:nloc))  = du(iloc(1:nloc),1)  !!D BC: 1.0_pr // N BC: du(iloc(1:nloc),1)
                   ELSE IF (ie == 2) THEN 
                      Lu_diag(shift+iloc(1:nloc)) = du(iloc(1:nloc),1) !!D BC: 1.0_pr // N BC: du(iloc(1:nloc),1)
                   ELSE IF (ie == 3) THEN  
                      Lu_diag(shift+iloc(1:nloc)) = du(iloc(1:nloc),1)  !!D BC: 1.0_pr // N BC: du(iloc(1:nloc),1)
                   ELSE IF (ie == 4) THEN                           
                      Lu_diag(shift+iloc(1:nloc)) = du(iloc(1:nloc),1)  !!D BC: 1.0_pr // N BC: du(iloc(1:nloc),1)
                   ELSE IF (ie == 5) THEN
                      Lu_diag(shift+iloc(1:nloc)) = du(iloc(1:nloc),1)  !!D BC: 1.0_pr // N BC: du(iloc(1:nloc),1)
                   ELSE IF (ie == 6) THEN
                      Lu_diag(shift+iloc(1:nloc)) = du(iloc(1:nloc),1)  !!D BC: 1.0_pr // N BC: du(iloc(1:nloc),1)
                   END IF
                   ! ************* Xmax *********************** !
                ELSE IF( face(1) == 1 ) THEN ! ENTIRE Xmax face
                   IF(ie == 1) THEN                                                     
                      Lu_diag(shift+iloc(1:nloc)) =  du(iloc(1:nloc),1) !!D BC: 1.0_pr // N BC: du(iloc(1:nloc),1)
                   ELSE IF (ie == 2) THEN 
                      Lu_diag(shift+iloc(1:nloc)) = du(iloc(1:nloc),1)  !!D BC: 1.0_pr // N BC: du(iloc(1:nloc),1)
                   ELSE IF (ie == 3) THEN  
                      Lu_diag(shift+iloc(1:nloc)) = du(iloc(1:nloc),1)  !!D BC: 1.0_pr // N BC: du(iloc(1:nloc),1)
                   ELSE IF (ie == 4) THEN                           
                      Lu_diag(shift+iloc(1:nloc)) =  du(iloc(1:nloc),1) !!D BC: 1.0_pr // N BC: du(iloc(1:nloc),1)
                   ELSE IF (ie == 5) THEN
                      Lu_diag(shift+iloc(1:nloc)) = du(iloc(1:nloc),1)  !!D BC: 1.0_pr // N BC: du(iloc(1:nloc),1)
                   ELSE IF (ie == 6) THEN
                      Lu_diag(shift+iloc(1:nloc)) =  du(iloc(1:nloc),1) !!D BC: 1.0_pr // N BC: du(iloc(1:nloc),1)
                   END IF
                END IF
                IF (dim >=2) THEN
                   ! ************* Ymin *********************** !
                   IF( face(2) == -1 ) THEN   ! Entire Ymin face
                      IF(ie == 1) THEN                                                     
                         Lu_diag(shift+iloc(1:nloc)) =du(iloc(1:nloc),2) !!D BC: 1.0_pr // N BC: du(iloc(1:nloc),2)
                      ELSE IF (ie == 2) THEN 
                         Lu_diag(shift+iloc(1:nloc)) =du(iloc(1:nloc),2) !!D BC: 1.0_pr // N BC: du(iloc(1:nloc),2)
                      ELSE IF (ie == 3) THEN  
                         Lu_diag(shift+iloc(1:nloc)) =du(iloc(1:nloc),2) !!D BC: 1.0_pr // N BC: du(iloc(1:nloc),2)
                      ELSE IF (ie == 4) THEN                           
                         Lu_diag(shift+iloc(1:nloc)) =du(iloc(1:nloc),2) !!D BC: 1.0_pr // N BC: du(iloc(1:nloc),2)
                      ELSE IF (ie == 5) THEN
                         Lu_diag(shift+iloc(1:nloc)) =du(iloc(1:nloc),2) !!D BC: 1.0_pr // N BC: du(iloc(1:nloc),2)
                      ELSE IF (ie == 6) THEN
                         Lu_diag(shift+iloc(1:nloc)) =du(iloc(1:nloc),2) !!D BC: 1.0_pr // N BC: du(iloc(1:nloc),2)
                      END IF
                      ! ************* Ymax *********************** !
                   ELSE IF( face(2) == 1  ) THEN ! ENTIRE Ymax face
                      IF(ie == 1) THEN                                                     
                         Lu_diag(shift+iloc(1:nloc)) =du(iloc(1:nloc),2) !!D BC: 1.0_pr // N BC: du(iloc(1:nloc),2)
                      ELSE IF (ie == 2) THEN 
                         Lu_diag(shift+iloc(1:nloc)) =du(iloc(1:nloc),2) !!D BC: 1.0_pr // N BC: du(iloc(1:nloc),2)
                      ELSE IF (ie == 3) THEN  
                         Lu_diag(shift+iloc(1:nloc)) =du(iloc(1:nloc),2) !!D BC: 1.0_pr // N BC: du(iloc(1:nloc),2)
                      ELSE IF (ie == 4) THEN                           
                         Lu_diag(shift+iloc(1:nloc)) =du(iloc(1:nloc),2) !!D BC: 1.0_pr // N BC: du(iloc(1:nloc),2)
                      ELSE IF (ie == 5) THEN
                         Lu_diag(shift+iloc(1:nloc)) =du(iloc(1:nloc),2) !!D BC: 1.0_pr // N BC: du(iloc(1:nloc),2)
                      ELSE IF (ie == 6) THEN
                         Lu_diag(shift+iloc(1:nloc)) =du(iloc(1:nloc),2) !!D BC: 1.0_pr // N BC: du(iloc(1:nloc),2)
                      END IF
                   END IF
                END IF
                IF (dim ==3) THEN
                   ! ************* Zmin *********************** !
                   IF( face(3) == -1 ) THEN   ! Entire Zmin face
                      IF(ie == 1) THEN                                                     
                         Lu_diag(shift+iloc(1:nloc)) = 1.0_pr !!D BC: 1.0_pr // N BC: du(iloc(1:nloc),3)
                      ELSE IF (ie == 2) THEN 
                         Lu_diag(shift+iloc(1:nloc)) = 1.0_pr !!D BC: 1.0_pr // N BC: du(iloc(1:nloc),3)
                      ELSE IF (ie == 3) THEN  
                         Lu_diag(shift+iloc(1:nloc)) = 1.0_pr !!D BC: 1.0_pr // N BC: du(iloc(1:nloc),3)
                      ELSE IF (ie == 4) THEN                           
                         Lu_diag(shift+iloc(1:nloc)) = 1.0_pr !!D BC: 1.0_pr // N BC: du(iloc(1:nloc),3)
                      ELSE IF (ie == 5) THEN
                         Lu_diag(shift+iloc(1:nloc)) = 1.0_pr !!D BC: 1.0_pr // N BC: du(iloc(1:nloc),3)
                      ELSE IF (ie == 6) THEN
                         Lu_diag(shift+iloc(1:nloc)) = 1.0_pr !!D BC: 1.0_pr // N BC: du(iloc(1:nloc),3)
                      END IF
                      ! ************* Zmax *********************** !
                   ELSE IF( face(3) == 1  ) THEN ! ENTIRE Zmax face
                      IF(ie == 1) THEN                                                     
                         Lu_diag(shift+iloc(1:nloc)) = 1.0_pr !!D BC: 1.0_pr // N BC: du(iloc(1:nloc),3)
                      ELSE IF (ie == 2) THEN 
                         Lu_diag(shift+iloc(1:nloc)) = 1.0_pr !!D BC: 1.0_pr // N BC: du(iloc(1:nloc),3)
                      ELSE IF (ie == 3) THEN  
                         Lu_diag(shift+iloc(1:nloc)) = 1.0_pr !!D BC: 1.0_pr // N BC: du(iloc(1:nloc),3)
                      ELSE IF (ie == 4) THEN                           
                         Lu_diag(shift+iloc(1:nloc)) = 1.0_pr !!D BC: 1.0_pr // N BC: du(iloc(1:nloc),3)
                      ELSE IF (ie == 5) THEN
                         Lu_diag(shift+iloc(1:nloc)) = 1.0_pr !!D BC: 1.0_pr // N BC: du(iloc(1:nloc),3)
                      ELSE IF (ie == 6) THEN
                         Lu_diag(shift+iloc(1:nloc)) = 1.0_pr !!D BC: 1.0_pr // N BC: du(iloc(1:nloc),3)
                      END IF
                   END IF
                END IF

             END IF
          END IF
       END DO
    END DO

  END SUBROUTINE user_algebraic_BC_diag

  SUBROUTINE user_algebraic_BC_rhs (rhs, ne_local, nlocal, jlev)
    !--Sets rhs for boundary conditions
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: ne_local, nlocal, jlev
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (INOUT) :: rhs

    INTEGER :: ie, i, ii, shift, denshift, eshift
    INTEGER, DIMENSION (dim) :: velshift

    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc

    DO ie = 1, ne_local
       shift = nlocal*(ie-1)
       !--Go through all Boundary points that are specified
       i_p_face(0) = 1
       DO i=1,dim
          i_p_face(i) = i_p_face(i-1)*3
       END DO
       DO face_type = 0, 3**dim - 1
          face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
          IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
             CALL get_all_indices_by_face (face_type, jlev, nloc, iloc)
             IF(nloc > 0 ) THEN

		! ************* Xmin *********************** !
                IF( face(1) == -1 ) THEN   ! Entire Xmin face
                   IF(ie == 1) THEN           
                      rhs(shift+iloc(1:nloc)) = 0.0_pr
                   ELSE IF (ie == 2) THEN 
                      rhs(shift+iloc(1:nloc)) = 0.0_pr
                   ELSE IF (ie == 3) THEN 
                      rhs(shift+iloc(1:nloc)) = 0.0_pr 
                   ELSE IF (ie == 4) THEN              
                      rhs(shift+iloc(1:nloc)) = 0.0_pr  
                   ELSE IF (ie == 5) THEN              
                      rhs(shift+iloc(1:nloc)) = 0.0_pr
                   ELSE IF (ie == 6) THEN              
                      rhs(shift+iloc(1:nloc)) = 0.0_pr
                   END IF
		! ************* Xmax *********************** !
                ELSE IF( face(1) == 1  ) THEN ! ENTIRE Xmax face
                   IF(ie == 1) THEN           
                      rhs(shift+iloc(1:nloc)) = 0.0_pr
                   ELSE IF (ie == 2) THEN 
                      rhs(shift+iloc(1:nloc)) = 0.0_pr
                   ELSE IF (ie == 3) THEN 
                      rhs(shift+iloc(1:nloc)) = 0.0_pr 
                   ELSE IF (ie == 4) THEN              
                      rhs(shift+iloc(1:nloc)) = 0.0_pr  
                   ELSE IF (ie == 5) THEN              
                      rhs(shift+iloc(1:nloc)) = 0.0_pr
                   ELSE IF (ie == 6) THEN              
                      rhs(shift+iloc(1:nloc)) = 0.0_pr
                   END IF
                END IF
                IF (dim >=2) THEN
                   ! ************* Ymin *********************** !
                   IF( face(2) == -1 ) THEN   ! Entire Ymin face
                      IF(ie == 1) THEN           
                         rhs(shift+iloc(1:nloc)) = 0.0_pr
                      ELSE IF (ie == 2) THEN 
                         rhs(shift+iloc(1:nloc)) = 0.0_pr
                      ELSE IF (ie == 3) THEN 
                         rhs(shift+iloc(1:nloc)) = 0.0_pr 
                      ELSE IF (ie == 4) THEN              
                         rhs(shift+iloc(1:nloc)) = 0.0_pr  
                      ELSE IF (ie == 5) THEN              
                         rhs(shift+iloc(1:nloc)) = 0.0_pr
                      ELSE IF (ie == 6) THEN              
                         rhs(shift+iloc(1:nloc)) = 0.0_pr
                      END IF
                      ! ************* Ymax *********************** !
                   ELSE IF( face(2) == 1  ) THEN ! ENTIRE Ymax face
                      IF(ie == 1) THEN           
                         rhs(shift+iloc(1:nloc)) = 0.0_pr
                      ELSE IF (ie == 2) THEN 
                         rhs(shift+iloc(1:nloc)) = 0.0_pr
                      ELSE IF (ie == 3) THEN 
                         rhs(shift+iloc(1:nloc)) = 0.0_pr 
                      ELSE IF (ie == 4) THEN              
                         rhs(shift+iloc(1:nloc)) = 0.0_pr  
                      ELSE IF (ie == 5) THEN              
                         rhs(shift+iloc(1:nloc)) = 0.0_pr
                      ELSE IF (ie == 6) THEN              
                         rhs(shift+iloc(1:nloc)) = 0.0_pr
                      END IF
                   END IF
                END IF
                IF (dim ==3) THEN
                   ! ************* Zmin *********************** !
                   IF( face(3) == -1 ) THEN   ! Entire Zmin face
                      IF(ie == 1) THEN           
                         rhs(shift+iloc(1:nloc)) = 0.0_pr
                      ELSE IF (ie == 2) THEN 
                         rhs(shift+iloc(1:nloc)) = 0.0_pr
                      ELSE IF (ie == 3) THEN 
                         rhs(shift+iloc(1:nloc)) = 0.0_pr 
                      ELSE IF (ie == 4) THEN              
                         rhs(shift+iloc(1:nloc)) = 0.0_pr  
                      ELSE IF (ie == 5) THEN              
                         rhs(shift+iloc(1:nloc)) = 0.0_pr
                      ELSE IF (ie == 6) THEN              
                         rhs(shift+iloc(1:nloc)) = 0.0_pr
                      END IF
                      ! ************* Zmax *********************** !
                   ELSE IF( face(3) == 1  ) THEN ! ENTIRE Zmax face
                      IF(ie == 1) THEN           
                         rhs(shift+iloc(1:nloc)) = 0.0_pr
                      ELSE IF (ie == 2) THEN 
                         rhs(shift+iloc(1:nloc)) = 0.0_pr
                      ELSE IF (ie == 3) THEN 
                         rhs(shift+iloc(1:nloc)) = 0.0_pr 
                      ELSE IF (ie == 4) THEN              
                         rhs(shift+iloc(1:nloc)) = 1.0_pr  
                      ELSE IF (ie == 5) THEN              
                         rhs(shift+iloc(1:nloc)) = 0.0_pr
                      ELSE IF (ie == 6) THEN              
                         rhs(shift+iloc(1:nloc)) = 0.0_pr
                      END IF
                   END IF
                END IF

             END IF
          END IF
       END DO
    END DO

  END SUBROUTINE user_algebraic_BC_rhs

  SUBROUTINE user_project (u, p, nlocal, meth)
    !--Makes u divergence free
    IMPLICIT NONE

    INTEGER, INTENT (IN) :: meth, nlocal
    REAL (pr), DIMENSION (nlocal),     INTENT(INOUT) :: p
    REAL (pr), DIMENSION (nlocal,n_integrated), INTENT(INOUT) :: u

    REAL (pr), DIMENSION (nlocal) :: dp
    REAL (pr), DIMENSION (nlocal) :: f

    INTEGER :: i
    INTEGER, PARAMETER :: ne = 1
    INTEGER, DIMENSION(ne) :: clip 

  END SUBROUTINE user_project

  !
  ! Setting up the RHS of the main equations
  !

  FUNCTION user_rhs (u_integrated,scalar)
    IMPLICIT NONE
    REAL (pr), DIMENSION (ng,ne), INTENT(IN) :: u_integrated !1D flat version of integrated variables without BC 
    REAL (pr), DIMENSION (ng), INTENT(IN) :: scalar
    REAL (pr), DIMENSION (ng*ne) :: user_rhs
    INTEGER :: i, j, ie, shift
    INTEGER, PARAMETER :: meth=0
    REAL (pr), DIMENSION (dim+3,ng,dim) :: du, d2u

    !
    ! User defined variables
    !
    REAL (pr), DIMENSION (ng,n_integrated,dim) :: F  ! Flux terms
    REAL (pr), DIMENSION (ng,dim+2) :: v             ! velocity components + temp + alpha
    REAL (pr), DIMENSION (ng) :: p,pDum                   ! pressures

    REAL (pr), DIMENSION (ng,n_integrated) :: S      ! Source terms
    REAL (pr), DIMENSION (ng) :: c                   ! User speed of sound
    
    ! Curvature calculation variables
    REAL (pr) :: alpha_w
    REAL (pr), DIMENSION (nwlt) :: psi, alpha, myNorm, kappa
    REAL (pr), DIMENSION (ng,dim) :: n
    INTEGER :: meth0, meth1

    ! Setting up pressure from the general EOS function
    CALL user_eos (u_integrated, u_integrated, ne, ng, p, pDum, c)

    ! Finding the velocities and Temperature
    DO i=1,dim 
       v(:,i) = u_integrated(:,n_var_vel(i))/u_integrated(:,n_var_den) ! u_i
    END DO

    v(:,dim+1) = p(:)/u_integrated(:,n_var_den)  ! Temperature

    alpha = u_integrated(:,n_var_alpha1D1)/u_integrated(:,n_var_den)  ! alpha1

    ! Calculate psi
!!$    alpha_w = 0.1_pr
!!$    alpha = max(alpha,1.0E-6_pr)-1.0E-6
!!$    psi = alpha**alpha_w/(alpha**alpha_w + (1-alpha)**alpha_w)

!!$    psi = alpha ! psi function isn't working properly, use alpha for now 
!!$
!!$    v(:,dim+2) = psi
!!$
!!$    !! find the derivatives of velocities and temperature and alpha
!!$    meth0=0
!!$    meth1=1
!!$    ! Use higher order derivative
!!$     CALL c_diff_fast(psi, du(1,:,:), d2u(1,:,:), j_lev, ng, meth1, 10, 1, 1, 1) ! du_i/dx_j
!!$
!!$    ! Calculate normal vector 
!!$    myNorm(1:ng) = SUM( du(1,1:ng,1:dim)**2,DIM=2)
!!$    DO j=1,dim
!!$       n(1:ng,j) = du(1,1:ng,j)/myNorm
!!$    END DO
!!$
!!$    ! Calculate curvature
!!$    ! Use 2nd order central difference derivative
!!$     CALL c_diff_fast(n(1:ng,1:dim), du(1:dim,1:ng,1:dim), d2u(1:dim,1:ng,1:dim), j_lev, ng, meth0, 10, dim, 1, 1) ! du_i/dx_j
!!$     kappa = 0.0_pr
!!$     DO j=1,dim
!!$        kappa = kappa + du(j,1:ng,j)
!!$     END DO

     kappa = test(u_integrated(1:ng,1:n_integrated))
!     kappa = curvature(u_integrated)!, scalar)
     kappa = curvature()!, scalar)
     u(:,n_var_kappa) = kappa*alpha*(1.0_pr-alpha)
    !
    !--Form right hand side of Euler Equations(Eqs without trasnport terms)
    !
    
    DO j = 1,dim
       ! MASS CONSERVATION EQUATION:
       F(:,n_var_den  ,j) = u_integrated(:,j+1) ! Mass Cons. Eq. X,Y,Z
       ! MOM. CONSERVATION EQUATION:
       DO i = 1,dim
          F(:,i+1,j) = u_integrated(:,i+1)*u_integrated(:,j+1)/u_integrated(:,n_var_den)
       END DO
       F(:,j+1,j) = F(:,j+1,j) + p(:) !! add pressure term
       ! ENG. CONSERVATION EQUATION:
       F(:,n_var_eng,j) = ( u_integrated(:,n_var_eng) + p(:) )*u_integrated(:,j+1)/u_integrated(:,n_var_den)
       ! Conservative Level set advection equation
       F(:,n_var_alpha1D1,j) =  u_integrated(:,n_var_alpha1D1)*u_integrated(:,j+1)/u_integrated(:,1) ! alpha component convective term X,Y,Z u_integrated(:,j+1)! 0.0_pr 
    END DO

    ! Set the source terms equal to zero
    S(:,:) = 0.0_pr


    ! Create right hand side
    user_rhs = 0.0_pr
    DO j = 1, dim
       CALL c_diff_fast(F(:,1:dim+3,j), du(1:dim+3,:,:), d2u(1:dim+3,:,:), j_lev, ng, meth, 10, dim+3, 1, dim+3) ! du(i,:,k)=dF_ij/dx_k
       DO i = 1, dim+3
          shift=(i-1)*ng
          user_rhs(shift+1:shift+ng) = user_rhs(shift+1:shift+ng) - du(i,:,j) + S(:,i)  ! - dF_ij/dx_j
       END DO
    END DO

    ! Create flux limiter/diffusion for current solution iteration
    IF (hypermodel /= 0) THEN
       c = user_sound_speed(u_integrated,n_integrated,ng)
       CALL hyper_pre_process (u_integrated,ng,n_integrated,c)
       CALL hyperbolic(u_integrated, ng, user_rhs)
    END IF

  END FUNCTION user_rhs

 !
 ! find Jacobian of Right Hand Side of the problem
 !
 
  FUNCTION user_Drhs (u, u_prev, meth)
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: meth
    REAL (pr), DIMENSION (ng,ne) :: u, u_prev
    REAL (pr), DIMENSION (n) :: user_Drhs

    INTEGER :: i, j, ie, shift
    REAL (pr), DIMENSION (n_integrated,ng,dim+2) :: du, d2u, dup, d2up
    !
    ! User defined variables
    !
    REAL (pr), DIMENSION (ng,n_integrated,dim) :: F
    REAL (pr), DIMENSION (ng,dim+2) :: v, v_prev      ! velocity components + temperature + alpha component
    REAL (pr), DIMENSION (ng) :: p,  p_prev, muT, muT_prev 

    REAL (pr), DIMENSION (ng,n_integrated) :: S      ! Source terms



    user_Drhs = 0.0_pr    

  END FUNCTION user_Drhs

  !
  ! Uses u_prev_timestep, which is global flat (1D ) array set in time_step_cn()
  !
  FUNCTION user_Drhs_diag (meth)
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: meth
    REAL (pr), DIMENSION (n) :: user_Drhs_diag

    INTEGER :: i,j, ie, shift
    REAL (pr), DIMENSION (ng,dim) :: du_diag, d2u_diag, du_diag_tmp
    REAL (pr), DIMENSION (n_integrated,ng,dim) :: du, d2u
    REAL (pr), DIMENSION (ng,n_integrated) :: S      ! Source terms

    !
    ! User defined variables
    !
    REAL (pr), DIMENSION (ng,n_integrated,dim) :: F
    REAL (pr), DIMENSION (ng,ne) :: u_prev
    REAL (pr), DIMENSION (ng,dim+2) :: v_prev ! velocity components
    REAL (pr), DIMENSION (ng) :: p,muT, muT_prev 
    
    ! Create u_prev from u_prev_timestep
    DO ie = 1, ne
       shift=(ie-1)*ng
       u_prev(1:ng,ie) = u_prev_timestep(shift+1:shift+ng)
    END DO

    !
    ! Calculate Diagonal derivative term
    !
    CALL c_diff_diag(du_diag, d2u_diag, j_lev, ng, meth, meth, 11)

    user_Drhs_diag = 0.0_pr
    
  END FUNCTION user_Drhs_diag

  FUNCTION user_chi (nlocal, t_local )
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: nlocal
    REAL (pr), INTENT (IN) :: t_local
    REAL (pr), DIMENSION (nlocal) :: user_chi
    !
    !  Create field object here, 1 inside and 0 outside
    !
    user_chi = 0.0_pr
  END FUNCTION user_chi


  !
  ! Calculate any statitics
  !
  ! startup_flag - 0 when adapting to IC,then 1 inmain integration loop
  !

  SUBROUTINE user_stats ( u ,j_mn, startup_flag)
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: startup_flag
    INTEGER , INTENT (IN) :: j_mn 
    REAL (pr), DIMENSION (nwlt,1:n_var), INTENT (IN) :: u
  END SUBROUTINE user_stats


  SUBROUTINE user_cal_force (u, n, t_local, force, drag, lift)
    !--Calculates drag and lift on obstacle using penalization formula
    IMPLICIT NONE

    INTEGER, INTENT (IN) :: n
    REAL (pr), INTENT (IN) :: t_local
    REAL (pr), DIMENSION (dim), INTENT (INOUT) :: force
    REAL (pr), INTENT (OUT) :: drag, lift
    REAL (pr), DIMENSION (n,dim) :: u
    drag = 0.0_PR
    lift = 0.0_PR

    !
    ! There is no obstacle in flow
    !

  END SUBROUTINE user_cal_force

  !
  ! Read input from "case_name"_pde.inp file
  ! case_name is in string file_name read from command line
  ! in read_command_line_input()
  ! 
  SUBROUTINE user_read_input()
    IMPLICIT NONE

    call input_real ('gamma',gamma,'stop',' gamma: Gamma = C_p / C_v')
    call input_real ('Re',Re,'stop',' Re: Reynolds number')
    call input_real ('Pra',Pra,'stop', ' Pra: Prandtle number')
    call input_real ('Sc',Sc,'stop',' Sc: Schmidt number')
    call input_real ('m_visc',m_visc,'stop','power law for visc')
    call input_real ('xL',xL,'stop',' xL: left boundary for heat addition')
    call input_real ('xR',xR,'stop',' xR: right boundary for heat addition')
    call input_real ('delta',delta,'stop',' delta: width of initial shock')
    call input_real ('radius',radius,'stop',' radius: Where initial discontinuity lies')
    call input_real ('pi_inf',pi_inf,'stop',' pi_inf: for stiffend-gas eos')
  END SUBROUTINE user_read_input

  SUBROUTINE user_additional_vars( t_local, flag )
    IMPLICIT NONE
    REAL (pr), INTENT (IN) :: t_local
    INTEGER, INTENT (IN) :: flag ! 0 during initial adaptation, then 1 during time advancement
    REAL (pr), DIMENSION (nwlt) :: p,pDum,c

!!$    ! Curvature calculation variables
!!$    REAL (pr) :: alpha_w
!!$    REAL (pr), DIMENSION (nwlt) :: psi, alpha
!!$    INTEGER, PARAMETER :: meth=0
!!$    REAL (pr), DIMENSION (n_integrated,ng,dim) :: du, d2u
!!$    REAL (pr), DIMENSION (ng,n_integrated) :: F  ! Flux terms


    CALL user_eos(u,u,n_integrated,nwlt,p,pDum,c)

    u(:,n_var_pressure) = p(:)
    u(:,n_var_temp)     = u(:,n_var_pressure)/u(:,n_var_den)
    u(:,n_var_alpha1)   = u(:,n_var_alpha1D1)/u(:,n_var_den)

    ! Calculate psi
!!$    alpha_w = 0.2_pr
!!$    alpha = max(u(:,n_var_alpha1),1.0E-6_pr)-1.0E-6
!!$    psi = alpha**alpha_w/(alpha**alpha_w + (1-alpha)**alpha_w)
!    psi = u(:,n_var_alpha1)

!    u(:,n_var_kappa)    = test(u(:,1:n_integrated)) !kappa !psi

    u(:,n_var_kappa)    = curvature() 
   
  END SUBROUTINE user_additional_vars

  SUBROUTINE user_scalar_vars( flag )
    IMPLICIT NONE
    INTEGER , INTENT(IN) :: flag ! 0- called during adaption to IC, 1 called during main integration loop

  END SUBROUTINE user_scalar_vars

  !
  !************ Calculating Scales ***************************
  !
  ! Note the order of the components in the scl array
  ! correspond to u_tn, v_tn, w_tn, u_tn-1, v_tn-1, w_tn-1, u_tn-2, v_tn-2, w_tn-2
  ! 
  SUBROUTINE user_scales(flag, use_default, u, nlocal, ne_local, l_n_var_adapt , l_n_var_adapt_index, &
       scl, scl_fltwt) !add
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: flag ! 0 during initial adaptation, then 1 during time advancement
    LOGICAL , INTENT(INOUT) :: use_default
    INTEGER, INTENT (IN) :: nlocal, ne_local
    REAL (pr), DIMENSION (1:nlocal,1:ne_local), INTENT (IN) :: u
    LOGICAL , INTENT (IN) :: l_n_var_adapt(ne_local)
    INTEGER , INTENT (IN) :: l_n_var_adapt_index(1:ne_local)
    REAL (pr), DIMENSION (1:ne_local), INTENT (INOUT) :: scl
    REAL (pr) ,  INTENT(IN) ::scl_fltwt !weight for temporal filter on scl

    !
    ! Ignore the output of this routine and use default scales routine
    !
    use_default = .TRUE. 

    !
    ! NOTE: For a parallel run, synchronize scl(:) across the processors in user_scales.
    !       Use the subroutine scales of default_util.f90 as an example,
    !       subroutine parallel_global_sum of parallel.f90 may be helpful.
    !       Already synchronized: sumdA_global
  END SUBROUTINE user_scales


  SUBROUTINE user_cal_cfl (use_default, u, cfl_out)
    USE precision
    USE sizes
    USE pde
    IMPLICIT NONE
    LOGICAL , INTENT(INOUT) :: use_default
    REAL (pr),                                INTENT (INOUT) :: cfl_out
    REAL (pr), DIMENSION (nwlt,n_integrated), INTENT (IN)    :: u

    INTEGER                    :: i
    REAL (pr)                  :: floor
    REAL (pr)                  :: cflx,cfly,cSum
    REAL (pr), DIMENSION(nwlt) :: c
    REAL (pr), DIMENSION(dim,nwlt) :: h_arr
    REAL (pr), DIMENSION (nwlt) :: p,pDum

    use_default = .FALSE.

    floor = 1e-12_pr
    cfl_out = floor

    CALL user_eos (u, u, n_integrated, nwlt, p, pDum, c)
    
    CALL get_all_local_h (h_arr)

    cSum = 0.0_pr
    DO i=1,dim
       cSum = cSum + MAXVAL( (ABS(u(:,n_var_vel(i))/u(:,n_var_den)) + c) *dt/h_arr(i,:) )
    END DO
    cfl_out = MAX (cfl_out, cSum)
    CALL parallel_global_sum( REALMAXVAL=cfl_out )

  END SUBROUTINE user_cal_cfl


  !******************************************************************************************
  !************************************* SGS MODEL ROUTINES *********************************
  !******************************************************************************************
  
  !
  ! Intialize sgs model
  ! This routine is called once in the first
  ! iteration of the main time integration loop.
  ! weights and model filters have been setup for first loop when this routine is called.
  !
  SUBROUTINE user_init_sgs_model( )
    IMPLICIT NONE


    ! LDM: Giuliano

    ! THE sgs forcign is stored for the time step in sgs_mdl_force(1:nlocal,1:n_integrated)
    ! where nlocal should be nwlt.


    !          print *,'initializing LDM ...'       
    !          CALL sgs_mdl_force( u(:,1:n_integrated), nwlt, j_lev, .TRUE.)


  END SUBROUTINE user_init_sgs_model

  !
  ! calculate sgs model forcing term
  ! user_sgs_force is called int he beginning of each times step in time_adv_cn().
  ! THE sgs forcign is stored for the time step in sgs_mdl_force(1:nlocal,1:n_integrated)
  ! where nlocal should be nwlt.
  ! 
  ! Accesses u from field module, 
  !          j_lev from wlt_vars module,
  !
  SUBROUTINE  user_sgs_force (u_loc, nlocal)
    IMPLICIT NONE

    INTEGER,                         INTENT (IN) :: nlocal
    REAL (pr), DIMENSION (nlocal,n_integrated), INTENT (INOUT) :: u_loc

  END SUBROUTINE  user_sgs_force

  FUNCTION user_sound_speed (u, neq, nwlt)
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: nwlt, neq
    REAL (pr), DIMENSION (nwlt,neq), INTENT(IN) :: u
    REAL (pr), DIMENSION (nwlt) :: user_sound_speed
    REAL (pr), DIMENSION (nwlt) :: p, pDum

    CALL user_eos (u, u, neq, nwlt, p, pDum, user_sound_speed)

  END FUNCTION user_sound_speed

SUBROUTINE user_eos (u_prev, u, neq, nwlt, p_prev, p, c_sound)
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: nwlt, neq
    REAL (pr), DIMENSION (nwlt,neq), INTENT(IN) :: u_prev, u
    REAL (pr), DIMENSION (nwlt), INTENT(OUT) :: p_prev, p, c_sound

    ! Pressure for Stiffened gas
    p_prev(:) = (gamma-1.0_pr)*( u_prev(:,n_var_eng)-0.5_pr*SUM(u_prev(:,n_var_vel(1:dim))**2,DIM=2)/u_prev(:,1) ) - gamma*pi_inf 

    ! Pressure prime term for stiffened gas should be same because gamma and pi_inf are constants for single phase
    p(:) = (gamma-1.0_pr)*( u(:,n_var_eng) - SUM(u_prev(:,n_var_vel(1:dim))*u(:,n_var_vel(1:dim)),DIM=2)/u_prev(:,1) &
                                + 0.5_pr*SUM(u_prev(:,n_var_vel(1:dim))**2,DIM=2)/u_prev(:,1)**2*u(:,1) ) ! pressure prime

    ! Calculate sound speed for ideal gas
    c_sound = SQRT( gamma * (p_prev+pi_inf)/u_prev(:,1) )
    
  END SUBROUTINE user_eos

  FUNCTION test(u)
    REAL (pr), DIMENSION (nwlt,1:n_var), INTENT(IN) :: u 
    REAL (pr), DIMENSION (nwlt) :: test
    INTEGER :: i, j, ie, shift
    INTEGER, PARAMETER :: meth=0
    REAL (pr), DIMENSION (nwlt,dim) :: du, d2u
!    REAL (pr), DIMENSION (nwlt,1) :: duT, d2uT
    REAL (pr), DIMENSION (nwlt) :: phi

    phi = u(:,n_var_alpha1D1)/u(:,n_var_den)
!    CALL c_diff_fast(phi, duT(:,1), d2uT(:,1), j_lev, nwlt, meth, 10, 1, 1, 1) ! du_i/dx_j
    CALL c_diff_fast(phi, du(:,1:dim), d2u(:,1:dim), j_lev, nwlt, 1, 10, 1, 1, 1) ! du_i/dx_j
    
    test = du(:,1)

  END FUNCTION test

  FUNCTION curvature ()
    IMPLICIT NONE
!    REAL (pr), DIMENSION (nwlt,ne), INTENT(IN) :: u_integrated !1D flat version of integrated variables without BC 
    REAL (pr), DIMENSION (nwlt) :: curvature
    INTEGER :: i, j, ie, shift
    INTEGER, PARAMETER :: meth=0
    REAL (pr), DIMENSION (dim,nwlt,dim) :: du, d2u

    ! Curvature calculation variables
    REAL (pr) :: alpha_w
    REAL (pr), DIMENSION (nwlt) :: psi, alpha, myNorm, kappa
    REAL (pr), DIMENSION (nwlt,dim) :: n
    INTEGER :: meth0, meth1

!    alpha = u_integrated(:,n_var_alpha1D1)/u_integrated(:,n_var_den)  ! alpha1
    alpha = u(:,n_var_alpha1D1)/u(:,n_var_den)  ! alpha1

    ! Calculate psi
!!$    alpha_w = 0.1_pr
!!$    alpha = max(alpha,1.0E-6_pr)-1.0E-6
!!$    psi = alpha**alpha_w/(alpha**alpha_w + (1-alpha)**alpha_w)
    psi = alpha ! psi function isn't working properly, use alpha for now 

    !! find the derivatives of velocities and temperature and alpha
    meth0=0
    meth1=1
    ! Use higher order derivative
     CALL c_diff_fast(psi, du(1,:,:), d2u(1,:,:), j_lev, nwlt, meth0, 10, 1, 1, 1) ! du_i/dx_j

    ! Calculate normal vector 
    myNorm(1:nwlt) = sqrt( SUM( du(1,1:nwlt,1:dim)**2,DIM=2) )
!    myNorm(1:nwlt) = abs( du(1,1:nwlt,1))
!    myNorm(1:nwlt) = sqrt( du(1,1:nwlt,1)**2)
    
    DO j=1,dim
       n(1:nwlt,j) = du(1,1:nwlt,j)/(myNorm+1E-10_pr)
    END DO

    ! Calculate curvature
    ! Use 2nd order central difference derivative
     CALL c_diff_fast(n(1:nwlt,1:dim), du(1:dim,1:nwlt,1:dim), d2u(1:dim,1:nwlt,1:dim), j_lev, nwlt, meth0, 10, dim, 1, 1) ! du_i/dx_j
     kappa = 0.0_pr
     DO j=1,dim
        kappa = kappa + du(j,1:nwlt,j)
     END DO

     curvature = kappa

  END FUNCTION curvature

  SUBROUTINE  user_pre_process
    IMPLICIT NONE

  END SUBROUTINE user_pre_process

  SUBROUTINE  user_post_process
    IMPLICIT NONE

  END SUBROUTINE user_post_process

 END MODULE user_case
