MODULE user_case
  ! 1-D Flow + Reaction Eq.
  USE precision
  USE wlt_vars
  USE wavelet_filters_mod
  USE elliptic_mod
  USE elliptic_vars
  USE wlt_trns_mod
  USE wlt_trns_vars
  USE wlt_trns_util_mod
  USE io_3d_vars
  USE util_mod
  USE util_vars
  USE share_consts
  USE pde
  USE sizes
  USE share_kry
  USE vector_util_mod
  USE field
  USE input_file_reader
  USE additional_nodes
  USE debug_vars
  USE parallel
  USE hyperbolic_solver

  !
  ! case specific variables if there is any
  !

  REAL (pr) :: rhoB ! burned density of the fuel based on T_ad
  REAL (pr) :: SL   ! Flame speed
  REAL (pr) :: Re  ! Reynolds number
  REAL (pr) :: Pra ! Prandtle number
  REAL (pr) :: Sc  ! Schmidt number
  REAL (pr) :: gamma ! Gamma number C_p / C_v
  REAL (pr) :: Prexp ! Prexponential term for the Fuel equation
  REAL (pr) :: Tact ! Activation temperature
  REAL (pr) :: p0 ! constant pressure
  REAL (pr) :: m_visc ! power law for viscosity
  REAL (pr) :: qHeat ! reaction heat
  REAL (pr) :: Lambda ! Lambda
  REAL (pr) :: p2p1 ! shock strength
  REAL (pr) :: tp , xp, FL_R, rho_ps,u_ps,e_ps, U_rel,u_bc
  REAL (pr) :: rho_exp,u_exp,p_exp,e_exp !!$ Expansion conditions
  REAL (pr) :: delta ! Initial shock thickness
  CHARACTER (LEN=256)  :: flame_file
  LOGICAL :: expan_flag !!$ flag for expansion/no expansion

  INTEGER :: n_var_den, n_var_eng, n_var_fuel, n_var_fuelD !!, n_var_mom 
  INTEGER :: n_var_pressure, n_var_temp, n_var_vel, n_var_reaction
  
  INTEGER :: startup_flag
  INTEGER :: NS, startup_file
  !REAL (pr) :: delta, xL, xR, radius, t_init, t_final

!  REAL (pr) ::
!  INTEGER ::
!  LOGICAL, PARAMETERS ::

CONTAINS

SUBROUTINE user_setup_pde (VERB)
  IMPLICIT NONE
  LOGICAL, OPTIONAL :: VERB         ! print debug info
  INTEGER :: i

    IF (verb_level.GT.0) THEN
       PRINT * ,''
       PRINT *, '**********************Setting up PDE*****************'
       PRINT * ,	'CASE: 1-D Flow + Reaction Eq.'
       PRINT *, '*****************************************************'
    END IF

    n_integrated = 1 + 1 + 1 + 1    ! 1-Density, 2-Momentum(dim=1), 3-Energy, 4-Reaction

    n_var_additional = 5             ! pressure + temperture + velocity + fuel + Reaction rate

    n_var = n_integrated + n_var_additional !--Total number of variables

    n_var_exact = 0
    
    n_var_den = 1   ! density

  IF( hypermodel /= 0 ) THEN
    DO i=1,dim
       n_var_mom(i) = i+1
    END DO
 END IF

    n_var_eng = 3   ! energy   rho*et
    n_var_fuelD= 4  ! Fuel     rho*Yf

    n_var_reaction  = n_integrated + 1 !Reaction rate(5)
    n_var_pressure  = n_integrated + 2 !pressure p(6)
    n_var_vel       = n_integrated + 3 !Velocity u(7)
    n_var_fuel      = n_integrated + 4 !Fuel Fraction Yf(8)
    n_var_temp      = n_integrated + 5 !Temp(9)
 
    !
    !--Allocate logical arrays
    !
    CALL alloc_variable_mappings 

    WRITE (u_variable_names(1),u_variable_names_fmt) 'Density'
    WRITE (u_variable_names(2),u_variable_names_fmt) 'X-Momentum'
    WRITE (u_variable_names(3),u_variable_names_fmt) 'Energy'
    WRITE (u_variable_names(4),u_variable_names_fmt) 'FuelDensity'
    WRITE (u_variable_names(5),u_variable_names_fmt) 'Reaction Rate'
    WRITE (u_variable_names(6),u_variable_names_fmt) 'Pressure'
    WRITE (u_variable_names(7),u_variable_names_fmt) 'Velocity'
    WRITE (u_variable_names(8),u_variable_names_fmt) 'Fuel Fraction'
    WRITE (u_variable_names(9),u_variable_names_fmt) 'Temperature'


   !WRITE (u_variable_names(9),u_variable_names_fmt) 'Energy'


   !WRITE (u_variable_names(--),u_variable_names_fmt) '----'

    !
    !--Setup which components we will base grid adaptation on.
    !
    
    !n_adapt = n_var_reaction

    n_var_adapt = .FALSE. 		       !intialize
    n_var_adapt(1:n_integrated,0) = .TRUE. !--Initially adapted variables at first time level
    n_var_adapt(1:n_integrated,1) = .TRUE. !--After first time step adapt on 

    n_var_adapt(3,0) = .FALSE. !-- Not adapt to Energy
    n_var_adapt(3,1) = .FALSE. !-- Not adapt to Energy

    !--integrated variables at first time level
    !Variables that need to be interpoleted to new adapted grid in initial grid adaptation

    n_var_interpolate = .FALSE. !intialize
    n_var_interpolate(1:n_integrated+n_var_additional,0) = .TRUE.  

    !Variables that need to be interpoleted to new adapted grid at each time step
    n_var_interpolate(1:n_integrated+n_var_additional,1) = .TRUE. 

    !
    ! setup which components we have an exact solution for
    !

    n_var_exact_soln = .FALSE. !intialize
    n_var_exact_soln(1,0:1) = .FALSE. !.TRUE.

    !
    ! setup which variables we will save the solution
    !

    n_var_save = .FALSE. !intialize 
    n_var_save(1:n_var) = .TRUE. ! save all for restarting code

    !
    ! Set the maximum number of components for which we have an exact solution
    !

    n_var_exact = MAX( COUNT(n_var_exact_soln(:,0)),COUNT(n_var_exact_soln(:,1)) )

    !
    ! Setup a scaleCoeff array of we want to tweak the scaling of a variable
    ! ( if scaleCoeff < 1.0 then more points will be added to grid 
    !

    ALLOCATE ( scaleCoeff(1:n_var), Umn(1:n_var) )
    scaleCoeff = 1.0_pr

    IF (VERB) THEN
       PRINT *, 'n_integrated = ',n_integrated 
        PRINT *, 'n_var = ',n_var 
       PRINT *, '*******************Variable Names*******************'
       DO i = 1,n_var
          WRITE (*, u_variable_names_fmt) u_variable_names(i)
       END DO
       PRINT *, '****************************************************'
    END IF

  END SUBROUTINE  user_setup_pde

  !
  ! Set the exact solution for comparison to the simulated solution
  !
  ! u          - array to fill in the exact solution
  ! nlocal       - number of active wavelets  
  ! ne_local        - total number of equations
  ! t          - time of current time step
  ! l_n_var_exact_soln_index - index into the elements of u for which we need to
  !                            find the exact solution

  SUBROUTINE  user_exact_soln (u, nlocal,  t_local, l_n_var_exact_soln)  
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: nlocal
    REAL (pr), INTENT (IN) ::  t_local
    REAL (pr), DIMENSION (nlocal,n_var_exact), INTENT (INOUT) :: u
    LOGICAL , INTENT (IN) :: l_n_var_exact_soln(n_var)
    INTEGER :: i

  END SUBROUTINE  user_exact_soln

  !
  ! Setting up the intial condition for the problem
  !
  
  SUBROUTINE user_initial_conditions (u, nlocal, ne_local, t_local, scl, scl_fltwt,iter)
    IMPLICIT NONE

    INTEGER, INTENT (IN) :: nlocal, ne_local
    INTEGER, INTENT (INOUT) :: iter ! iteration of call while adapting initial grid
    REAL (pr), DIMENSION (nlocal,ne_local), INTENT (INOUT) :: u
    REAL (pr)  :: scl(1:n_var),scl_fltwt
    REAL (pr), INTENT (IN) :: t_local
    INTEGER :: i,j, k, dir
    REAL (pr), DIMENSION (nlocal) :: p,rho, Uy,Ux,Ya

    IF (.NOT. IC_restart) THEN

!!$    rho = 1.0_pr
!!$    p=0.7142857E+00

    ! Weak Shock tube problem
!    rho = 0.5_pr*(1.0_pr-tanh(x(:,1)/delta))*(1.0_pr-0.125_pr)+0.125_pr
    rho = 0.5_pr*(1.0_pr-tanh((x(:,1))/delta))*(1.0_pr-0.125_pr)+0.125_pr
    p = .5_pr*(1.0_pr-tanh((x(:,1))/delta))*0.9_pr+0.1_pr

    u(:,n_var_den) = rho
    u(:,n_var_fuel) = rho
    u(:,n_var_mom(1:dim)) = 0.0_pr
    u(:,n_var_eng) = p(:)/(gamma-1.0_pr) + 0.5_pr*(SUM(u(:,n_var_mom(1:dim))**2,DIM=2))/rho

       IF (verb_level.GT.0) THEN
          print*,'xmax ', maxval(x(:,1))!, 'ymax ', maxval(x(:,2))
          
          WRITE( *,'( "in user_IC t_local, nwlt " , f30.20, 1x , i5.5 )' ) t_local , nlocal
       END IF



    
 END IF


!!$ %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% !!$
    
!!$     print* ,'****---INITIAL CONDITION VARIABLES---****' 
!!$     print* ,'density  min: ' , minval(u(:,1))
!!$     print* ,'energy   min: ' , minval(u(:,4))
!!$     print* ,'velocity min: ' , minval(u(:,2))
!!$     print* ,'velocity min: ' , minval(u(:,3))

  END SUBROUTINE user_initial_conditions

  !--********************************  
  ! Arguments
  ! u         - field on adaptive grid
  ! nlocal      - number of active points
  ! ne_local       - number of equations
  ! t         - current time
  !
  ! Global variables used
  ! ifn       - number of equations
  ! x()
  ! xO
  ! y0
  !--******************************** 

  SUBROUTINE user_algebraic_BC (Lu, u, nlocal, ne_local, jlev, meth)
    !--Defines boundary condition type
    IMPLICIT NONE

    INTEGER , INTENT (IN) :: jlev, meth, ne_local, nlocal
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (INOUT) :: Lu
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (IN)    :: u

    INTEGER :: ie, i, ii, shift, denshift, velshift, eshift
   !INTEGER, DIMENSION (dim) :: velshift
    REAL (pr), DIMENSION (ne_local,nlocal,dim) :: du , d2u !uncomment if Neuman BC are used
    REAL (pr), DIMENSION (1,nlocal,dim) :: du_prev
    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc

    ! Shift markers for BC implementation:
    denshift = nlocal*(n_var_den - 1)
    eshift   = nlocal*(n_var_eng - 1) 
    
    ! Boundary conditions for 1D Couette flow in a channel
    
    !if Neuman BC are used, need to call 
    CALL c_diff_fast (u, du, d2u, jlev, nlocal, meth, 11, ne_local, 1, ne_local)
    
    DO ie = 1, ne_local
       shift = nlocal*(ie-1)
       !--Go through all Boundary points that are specified
       i_p_face(0) = 1
       DO i=1,dim
          i_p_face(i) = i_p_face(i-1)*3
       END DO
       DO face_type = 0, 3**dim - 1
          face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
          IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
             CALL get_all_indices_by_face (face_type, jlev, nloc, iloc)
             IF(nloc > 0 ) THEN
                IF( face(1) == -1 ) THEN   ! Entire Xmin face
                   IF(ie == 1) THEN                               ! Density
                     Lu(shift+iloc(1:nloc)) = du(ie,iloc(1:nloc),1)      ! Nueman
                     !!Lu(shift+iloc(1:nloc)) = u(shift+iloc(1:nloc))      ! Dirichlet
                   ELSE IF (ie ==  2) THEN  ! Momentum
                        !!Lu(shift+iloc(1:nloc)) = u(shift+iloc(1:nloc))! - 0.1*u(denshift+iloc(1:nloc))      ! Dirichlet
                        Lu(shift+iloc(1:nloc)) = du(ie,iloc(1:nloc),1)     ! Nueman
                   ELSE IF (ie ==  3) THEN                                ! Energy
                       Lu(shift+iloc(1:nloc)) = du(ie,iloc(1:nloc),1)     ! Nueman
                       !!Lu(shift+iloc(1:nloc)) = u(shift+iloc(1:nloc)) 
                       !$!                        - u(denshift+iloc(1:nloc))*(R*T_lw/(gamma - 1.0_pr))  ! Dirichlet
                   ELSE IF (ie == 4) THEN                                ! Fuel
                      Lu(shift+iloc(1:nloc)) = du(ie,iloc(1:nloc),1)      ! Nueman
                      !!Lu(shift+iloc(1:nloc)) = u(shift+iloc(1:nloc)) ! Dirichlet
                   END IF

                ELSE IF( face(1) == 1  ) THEN ! ENTIRE Xmax face
                   IF(ie == 1) THEN                               ! Density
                      Lu(shift+iloc(1:nloc)) = du(ie,iloc(1:nloc),1)       ! Nueman
                      !!Lu(shift+iloc(1:nloc)) = u(shift+iloc(1:nloc))       ! Dirichlet
                   ELSE IF (ie == 2) THEN  ! Momentum
                      Lu(shift+iloc(1:nloc)) = u(shift   +iloc(1:nloc))  ! Dirichlet
                      !!Lu(shift+iloc(1:nloc)) = du(ie,iloc(1:nloc),1)     ! Nueman
                   ELSE IF (ie == 3) THEN                                 ! Energy
                      Lu(shift+iloc(1:nloc)) = du(ie,iloc(1:nloc),1)      ! Nueman
                      !Lu(shift+iloc(1:nloc)) = u(shift   +iloc(1:nloc))  & 
                      ! - u(denshift+iloc(1:nloc))*T_ad/(gamma - 1.0_pr)    ! Dirichlet
                   ELSE IF (ie == 4) THEN                                ! Fuel
                      Lu(shift+iloc(1:nloc)) = du(ie,iloc(1:nloc),1)      ! Nueman
                   END IF
                END IF
             END IF
          END IF
       END DO
    END DO

  END SUBROUTINE user_algebraic_BC

  SUBROUTINE user_algebraic_BC_diag (Lu_diag, nlocal, ne_local, jlev, meth)
    !--Defines boundary diagonal terms
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: jlev, meth, ne_local, nlocal
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (INOUT) :: Lu_diag

    INTEGER :: ie, i, ii, shift

    REAL (pr), DIMENSION (nlocal,dim) :: du, d2u  !uncomment if Neuman BC are used
    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc

   ! Boundary conditions for 1D-2D Couette flow in a channel
    
    !if Neuman BC are used, need to call 
    CALL c_diff_diag (du, d2u, jlev, nlocal, meth, meth, 11)
    
    DO ie = 1, ne_local
       shift = nlocal*(ie-1)
       !--Go through all Boundary points that are specified
       i_p_face(0) = 1
       DO i=1,dim
          i_p_face(i) = i_p_face(i-1)*3
       END DO
       DO face_type = 0, 3**dim - 1
          face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
          IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
             CALL get_all_indices_by_face (face_type, jlev, nloc, iloc)
             IF(nloc > 0 ) THEN
                IF( face(1) == -1 ) THEN   ! Entire Xmin face
                   IF(ie == 1) THEN                               ! Density                                     
                         Lu_diag(shift+iloc(1:nloc)) = du(iloc(1:nloc),1)     ! Nueman
                         !!Lu_diag(shift+iloc(1:nloc)) = 1.0_pr                 ! Dirichlet

                   ELSE IF (ie == 2) THEN   ! Momentum
                           !!Lu_diag(shift+iloc(1:nloc)) = 1.0_pr                ! Dirichlet
                           Lu_diag(shift+iloc(1:nloc)) = du(iloc(1:nloc),1)   ! Nueman

                   ELSE IF (ie == 3) THEN                                 ! Energy
                           Lu_diag(shift+iloc(1:nloc)) = du(iloc(1:nloc),1)   ! Nueman
                           !!Lu_diag(shift+iloc(1:nloc)) = 1.0_pr               ! Drichelet

                   ELSE IF (ie ==  4) THEN                                ! Fuel
                        !!Lu_diag(shift+iloc(1:nloc)) = 1.0_pr               ! Drichelet
                        Lu_diag(shift+iloc(1:nloc)) = du(iloc(1:nloc),1)    ! Nueman
                   END IF

                ELSE IF( face(1) == 1  ) THEN ! ENTIRE Xmax face
                   IF(ie == 1) THEN                               ! Density
                      Lu_diag(shift+iloc(1:nloc)) = du(iloc(1:nloc),1)    ! Nueman
                     !Lu_diag(shift+iloc(1:nloc)) = 1.0_pr                ! Dirichlet

                   ELSE IF (ie ==  2) THEN  ! Momentum
                       Lu_diag(shift+iloc(1:nloc)) = 1.0_pr              ! Dirichlet
                       !!Lu_diag(shift+iloc(1:nloc)) = du(iloc(1:nloc),1)  ! Nueman

                   ELSE IF (ie == 3) THEN                                 ! Energy
                      Lu_diag(shift+iloc(1:nloc)) = du(iloc(1:nloc),1)   ! Nueman
                      !Lu_diag(shift+iloc(1:nloc)) = 1.0_pr               ! Drichelet

                   ELSE IF (ie ==  4) THEN                                ! Fuel
                      Lu_diag(shift+iloc(1:nloc)) = du(iloc(1:nloc),1)    ! Nueman
                    END IF
                 END IF
              END IF
           END IF
        END DO
     END DO

  END SUBROUTINE user_algebraic_BC_diag

  SUBROUTINE user_algebraic_BC_rhs (rhs, ne_local, nlocal, jlev)
    !--Sets rhs for boundary conditions
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: ne_local, nlocal, jlev
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (INOUT) :: rhs

    INTEGER :: i, ie, ii, shift
    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc
    DO ie = 1, ne_local
       shift = nlocal*(ie-1)
       !--Go through all Boundary points that are specified
       i_p_face(0) = 1
       DO i=1,dim
          i_p_face(i) = i_p_face(i-1)*3
       END DO
       DO face_type = 0, 3**dim - 1
          face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
          IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
             CALL get_all_indices_by_face (face_type, jlev, nloc, iloc)
             IF(nloc > 0 ) THEN

                IF( face(1) == -1 ) THEN   ! Entire Xmin face
                   IF(ie == 1) THEN                               ! Density
                      !!rhs(shift+iloc(1:nloc)) = rho_exp!!$ 0.0_pr !! 2.111111_pr  ! Nueman:     f = 0
                      rhs(shift+iloc(1:nloc)) = 0.0_pr !! 2.111111_pr  ! Nueman:     f = 0
                   ELSE IF (ie ==  2) THEN  ! Momentum
                      !!rhs(shift+iloc(1:nloc)) = rho_exp*(u_bc - U_rel)  !!1.8305655_pr !!$0.0_pr  ! Dirichlet:  f = 0
                      rhs(shift+iloc(1:nloc)) =  0.0_pr !! 2.111111_pr  ! Nueman:     f = 0
                   ELSE IF (ie == 3) THEN                                 ! Energy
                      !!rhs(shift+iloc(1:nloc)) = e_exp!!e_exp !!6.150793650793652_pr !!$+ qHeat! Nueman:     f = 0
                      rhs(shift+iloc(1:nloc)) = 0.0_pr !! 2.111111_pr  ! Nueman:     f = 0
                   ELSE IF (ie == 4) THEN                                 ! fuel
                      !!rhs(shift+iloc(1:nloc)) = rho_exp!!rho2  ! Nueman:     f = 0
                      rhs(shift+iloc(1:nloc)) = 0.0_pr !! 2.111111_pr  ! Nueman:     f = 0
                   END IF

                ELSE IF( face(1) == 1) THEN ! ENTIRE Xmax face
                   IF(ie == 1) THEN                             ! Density
                      rhs(shift+iloc(1:nloc)) = 0.0_pr   ! Nueman:  f = 0 
 
                   ELSE IF (ie == 2) THEN ! Momentum
                      rhs(shift+iloc(1:nloc)) = 0.0_pr   ! Nueman:  f = 0 

                   ELSE IF (ie == 3) THEN                               ! Energy
                      rhs(shift+iloc(1:nloc)) = 0.0_pr   ! Nueman:  f = 0

                   ELSE IF (ie == 4) THEN                               !Fuel  
                      rhs(shift+iloc(1:nloc)) = 0.0_pr  ! Nueman:   f = 0
                   END IF
                 END IF
              END IF
           END IF
        END DO
     END DO

  END SUBROUTINE user_algebraic_BC_rhs

  SUBROUTINE user_project (u, p, nlocal, meth)
    !--Makes u divergence free
    IMPLICIT NONE

    INTEGER, INTENT (IN) :: meth, nlocal
    REAL (pr), DIMENSION (nlocal),     INTENT(INOUT) :: p
    REAL (pr), DIMENSION (nlocal,n_integrated), INTENT(INOUT) :: u

    REAL (pr), DIMENSION (nlocal) :: dp
    REAL (pr), DIMENSION (nlocal) :: f

    INTEGER :: i
    INTEGER, PARAMETER :: ne = 1
    INTEGER, DIMENSION(ne) :: clip 

  END SUBROUTINE user_project

  !
  ! Setting up the RHS of the main equations
  !

  FUNCTION user_rhs (u_integrated,scalar)
    IMPLICIT NONE
    REAL (pr), DIMENSION (ng,ne), INTENT(IN) :: u_integrated !1D flat version of integrated variables without BC 
    REAL (pr), DIMENSION (ng), INTENT(IN) :: scalar
    REAL (pr), DIMENSION (ng*ne) :: user_rhs
    INTEGER :: i, j, ie, shift
    INTEGER, PARAMETER :: meth=0
    REAL (pr), DIMENSION (n_integrated,ng,dim) :: du, d2u
    ! Testing
    REAL (pr), DIMENSION (ng) :: c

    !
    ! User defined variables
    !
    REAL (pr), DIMENSION (ng,n_integrated,dim) :: F  ! Flux terms
    REAL (pr), DIMENSION (ng,3) :: v                 ! velocity component x + temp + fuel fraction
    REAL (pr), DIMENSION (ng) :: p,muT               ! pressures, temperature dependepnt viscosity
    REAL (pr), DIMENSION (ng,n_integrated) :: S      ! Source terms
    REAL (pr), DIMENSION (ng) :: wf                                 ! reaction rate

 ! Setting up pressure from ideal gas assumption
    p(:) = (gamma - 1.0_pr)*(u_integrated(:,n_var_eng) - &
                     0.5_pr*(u_integrated(:,2)**2)/u_integrated(:,1)) ! pressure

    ! Finding the velocities

    v(:,1) = u_integrated(:,2)/u_integrated(:,1)        ! velocity u

    v(:,2) = p(:)/u_integrated(:,n_var_den)  ! Temperature

    v(:,3) = u_integrated(:,4)/u_integrated(:,1)         ! Fuel mass fractions

    muT = (v(:,2)*gamma)**m_visc !! mu = mu_0 (T/T0)^m
!!$     print* ,'Fuel     min: ' , minval(u_integrated(:,4))
!!$     print* ,'energy   min: ' , minval(u_integrated(:,3))
!!$     print* ,'velocity min: ' , minval(u_integrated(:,2))
!!$     print* ,'density  min: ' , minval(u_integrated(:,1))

    !
    !--Form right hand side of Euler Equations
    !

    F(:,1,1) = u_integrated(:,2) ! rho*u_j : Mass Cons. Eq.
    
    F(:,2,1) = u_integrated(:,2)**2/u_integrated(:,1) ! rho*u_i*u_j : Momentum

    F(:,2,1) = F(:,2,1) + p(:) ! rho*u_i^2+p : Momentum: pressure terms

    F(:,3,1) = ( u_integrated(:,3) + p(:) )*u_integrated(:,2)/u_integrated(:,1) ! (e+p)*u_j : Energy

    F(:,4,1) = u_integrated(:,4)*u_integrated(:,2)/u_integrated(:,1) ! fuel equation

    IF (NS) THEN

       !
       !-- Form right hand side of Navier-Stokes Equations: " (1/Re)* d(tau_ij) / dx_i
       !
       CALL c_diff_fast(v, du(1:3,:,:), d2u(1:3,:,:), j_lev, ng, meth, 10, 3, 1,3) ! du_i/dx_j

       F(:,2,1) = F(:,2,1) - 4.0_pr/3.0_pr/Re*muT(:)*du(1,:,1) ! rho*u_i*u_j - 4/3Re*du_i/dx_j

       F(:,3,1) = F(:,3,1) - 1.0_pr/Re/Pra*gamma/(gamma - 1.0_pr)*muT(:)*du(2,:,1) & 
                           - 4.0_pr/3.0_pr/Re*muT(:)*du(1,:,1)*v(:,1) ! (e+p)*u_j - 4/3Re*du_i/dx_j*u_j : Energy terms

       F(:,4,1) = F(:,4,1) - 1.0_pr/Re/Sc*muT(:)*u_integrated(:,1)*du(3,:,1) ! Fuel Equation

    END IF

    wf = 0.0_pr !1.0_pr*Prexp*u_integrated(:,4)*EXP(-Tact/v(:,2)); ! Reaction rate

    ! Add source term if there is any
    S(:,1) = 0.0_pr;
    S(:,2) = 0.0_pr;
    S(:,3) = 0.0_pr; !wf*qHeat/gamma;
    S(:,4) = 0.0_pr; !-1.0_pr*wf   ! Fuel Equation terms
	
    !
    ! -- Find derivatives wrt X or Y
    !
    user_rhs = 0.0_pr
    CALL c_diff_fast(F(:,1:4,1), du(1:4,:,:), d2u(1:4,:,:), j_lev, ng, meth, 10, 4, 1, 4) ! du(i,:,k)=dF_ij/dx_k
    DO i = 1, 4
       shift=(i-1)*ng
       user_rhs(shift+1:shift+ng) = user_rhs(shift+1:shift+ng) - du(i,:,1) + S(:,i) ! -dF_ij/dx_j
    END DO

    ! Create flux limiter/diffusion for current solution iteration
    IF (hypermodel /= 0) THEN

       c = user_sound_speed(u_integrated,n_integrated,ng)
       CALL hyper_pre_process (u_integrated,ng,n_integrated,c)
       CALL hyperbolic(u_integrated, ng, user_rhs)

    END IF
!!$    CALL hyperbolic(u_integrated, ng, user_rhs)


  END FUNCTION user_rhs

 !
 ! find DRHS
 !
 
 FUNCTION user_Drhs (u, u_prev, meth)
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: meth
    !u_prev_timestep passed local to cast it into 2dim array to work with vector derivatives
    REAL (pr), DIMENSION (ng,ne) :: u, u_prev
    REAL (pr), DIMENSION (n) :: user_Drhs

    INTEGER :: i, j, ie, shift
    REAL (pr), DIMENSION (MAX(ne,dim+2),ng,dim) :: du, d2u
    REAL (pr), DIMENSION (MAX(ne,dim+2),ng,dim) :: dup, d2up
    !
    ! User defined variables
    !
    REAL (pr), DIMENSION (ng,n_integrated,dim) :: F
    REAL (pr), DIMENSION (ng,3) :: v, v_prev      ! velocity components + temperature
    REAL (pr), DIMENSION (ng) :: p,  p_prev 
    REAL (pr), DIMENSION (ng) :: muT,  muT_prev 
    REAL (pr), DIMENSION (ng,n_integrated) :: S      ! Source terms

    REAL (pr), DIMENSION (ng) :: dwf

    ! Finding the velocities " u_i  and  u'_i "
    v_prev(:,1) = u_prev(:,2)/u_prev(:,1)  ! Velocity u 

    v_prev(:,2) = (gamma - 1.0_pr)*(u_prev(:,3)/u_prev(:,1) &
                                    - 0.5_pr*u_prev(:,2)**2/u_prev(:,1)**2)
                                    !!$- u_prev(:,4)*qHeat/u_prev(:,1)) ! Temp  

    v_prev(:,3) = u_prev(:,4)/u_prev(:,1); ! Fuel Fraction
    
    v(:,1) = -u_prev(:,2)/u_prev(:,1)**2*u(:,1) + 1.0_pr/u_prev(:,1)*u(:,2);! v' = (U2/U1)'

    v(:,2) =(gamma - 1.0_pr)*((- u_prev(:,3)/u_prev(:,1)**2 + u_prev(:,2)**2/u_prev(:,1)**3)*u(:,1) & 
                                 +(-u_prev(:,2)/u_prev(:,1)**2)*u(:,2) &  
                                 +  u(:,3)/u_prev(:,1) )
                                 !!$-  qHeat/u_prev(:,1)*u(:,4))                      ! v' = T'

    v(:,3) = (-u_prev(:,4)/u_prev(:,1)**2)*u(:,1) + u(:,4)/u_prev(:,1);     ! v' = Yf'

    muT_prev = (v_prev(:,2)*gamma)**m_visc 

    muT = m_visc*gamma**m_visc*v_prev(:,2)**(m_visc - 1.0_pr)*v(:,2)
    ! Setting up the pressure terms " p_i + p'_i "
!!$    p_prev(:) = (gamma - 1.0_pr)*(u_prev(:,3) &
!!$                         - 0.5_pr*u_prev(:,2)**2/u_prev(:,1) ;  ! Prev. pressure
!!$
!!$    p(:) =  (gamma - 1.0_pr)*(0.5_pr*u_prev(:,2)**2/u_prev(:,1)**2*u(:,1) &
!!$                            - u_prev(:,2)/u_prev(:,1)*u(:,2) &
!!$                            + u(:,3)                            ! pressure derivative

    p_prev(:) = u_prev(:,1)*v_prev(:,2); 

    p(:) = v_prev(:,2)*u(:,1) + u_prev(:,1)*v(:,2);  

!!$    print* ,'energy   min: ' , minval(u_prev(:,3))
!!$    print* ,'velocity min: ' , minval(u_prev(:,2))
!!$    print* ,'density  min: ' , minval(u_prev(:,1))

!!$    print* ,'energy   min: ' , minval(u(:,3))
!!$    print* ,'velocity min: ' , minval(u(:,2))
!!$    print* ,'density  min: ' , minval(u(:,1))

    !
    !--Form right hand side of Euler Equations
    !

    !-- Mass
    F(:,1,1) = u(:,2) ! rho*u_j
	
    ! -- Momentum
    F(:,2,1) = -u_prev(:,2)**2/u_prev(:,1)**2*u(:,1);                 ! First term for U1 without pressure terms
    F(:,2,1) = F(:,2,1) + 2.0_pr*u_prev(:,2)/u_prev(:,1)*u(:,2);      ! Second term for U2 without pressure terms
    F(:,2,1) = F(:,2,1) + p(:);  	  		              ! All + pressure terms
 
    ! -- Energy
    F(:,3,1) = ( u(:,3) + p(:) )*u_prev(:,2)/u_prev(:,1) &                     ! First pressur and energy terms 
             + ( u_prev(:,3) + p_prev(:) )*u(:,2)/u_prev(:,1) & 	       ! Now U2 terms
             - ( u_prev(:,3) + p_prev(:) )*u_prev(:,2)/u_prev(:,1)**2*u(:,1);  ! Finally U1 temrs

    F(:,4,1) = (-u_prev(:,2)*u_prev(:,4)/u_prev(:,1)**2)*u(:,1) &   ! U1 terms
                + u_prev(:,4)/u_prev(:,1)*u(:,2) &                  ! U2 terms
                + u_prev(:,2)/u_prev(:,1)*u(:,4) ;                  ! U4 terms

    IF (NS == 1) THEN
       !
       !-- Form right hand side of Navier Stokes Equations
       !
       CALL c_diff_fast(v,      du(1:3,:,:) , d2u(1:3,:,:) , j_lev, ng , meth , 10 , 3 , 1 , 3) ! du'_i / dx_j
       CALL c_diff_fast(v_prev, dup(1:3,:,:) ,d2up(1:3,:,:), j_lev, ng , meth , 10 , 3 , 1 , 3) ! du'_i / dx_j

       F(:,2,1) = F(:,2,1) - 4.0_pr/3.0_pr/Re*(muT_prev(:)*du(1,:,1) + muT(:)*dup(1,:,1))  !  Trans Term in NS Eq. for U2

       F(:,3,1) = F(:,3,1) - 4.0_pr/3.0_pr/Re*(muT_prev(:)*dup(1,:,1)*v(:,1) + muT_prev(:)*v_prev(:,1)*du(1,:,1) +  & 
                                               muT(:)*dup(1,:,1)*v_prev(:,1)) ! Trans Term in energy Eq. for U2 & U1

       F(:,3,1) = F(:,3,1) - 1.0_pr/Re/Pra*gamma/(gamma - 1.0_pr)*(du(2,:,1)*muT_prev(:) + dup(2,:,1)*muT(:))  ! Temperature term in Energy Eq.

       F(:,4,1) = F(:,4,1) - 1.0_pr/Re/Sc*(muT_prev(:)*dup(3,:,1)*u(:,1) + muT_prev(:)*u_prev(:,1)*du(3,:,1) + &
                                           muT(:)*u_prev(:,1)*dup(3,:,1));   ! Fuel terms 
    
    END IF
    !
    ! Add source terms DRHS
    !
    
    dwf = 1.0*Prexp*EXP(-Tact/v_prev(:,2))*(u(:,4) + u_prev(:,4)*(Tact/v_prev(:,2)**2)*v(:,2));

    S(:,1) = 0.0_pr;
    S(:,2) = 0.0_pr;
    S(:,3) = 0.0_pr; !dwf*qHeat/gamma;
    S(:,4) = 0.0_pr; !-1.0_pr*dwf;

    ! -- Find derivatives wrt X or Y
    user_Drhs = 0.0_pr
    CALL c_diff_fast(F(:,1:4,1), du(1:4,:,:), d2u(1:4,:,:), j_lev, ng, meth, 10, 4, 1, 4) ! du(i,:,k)=dF_ij/dx_k
    DO i = 1,dim+3
       shift=(i-1)*ng
       user_Drhs(shift+1:shift+ng) = user_Drhs(shift+1:shift+ng) - du(i,:,1) + S(:,i)  ! -dF_ij/dx_j
    END DO

    IF (hypermodel /= 0) THEN
       CALL hyperbolic(u, ng, user_Drhs)  
    END IF
 ! print* , 'USER_DRHS:' , minval(user_Drhs)  

  END FUNCTION user_Drhs

  !
  ! Uses u_prev_timestep, which is global flat (1D ) array set in time_step_cn()
  !
  FUNCTION user_Drhs_diag (meth)
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: meth
    REAL (pr), DIMENSION (n) :: user_Drhs_diag

    INTEGER :: i,j, ie, shift
    REAL (pr), DIMENSION (ng,dim) :: du_diag, d2u_diag, du_diag_tmp
    REAL (pr), DIMENSION (n_integrated,ng,dim) :: du, d2u
    REAL (pr), DIMENSION (ng,n_integrated) :: S      ! Source terms
    REAL (pr), DIMENSION (ng) :: dwf
    !
    ! User defined variables
    !
    REAL (pr), DIMENSION (ng,n_integrated,dim) :: F
    REAL (pr), DIMENSION (ng,ne) :: u_prev
    REAL (pr), DIMENSION (ng,dim+2) :: v_prev
    REAL (pr), DIMENSION (ng) :: p,muT,muT_prev

    ! Create u_prev from u_prev_timestep
    DO ie = 1, ne
       shift=(ie-1)*ng
       u_prev(1:ng,ie) = u_prev_timestep(shift+1:shift+ng)
    END DO

    !
    ! Calculate Diagonal derivative term
    !
    CALL c_diff_diag(du_diag, d2u_diag, j_lev, ng, meth, meth, -11)

    !!$ F(:,"No. of eq.",Dimension("F or G in my notes"))

    !
    !--Form right hand side of Euler Equations
    !
    
    !! Diag terms due to "U1 = rho"
    F(:,1,1) = 0.0_pr; !!F(1,1)
    !!F(:,1,2) = 0.0_pr; !!G(1,1)

    !! Diag terms due to "U2 = rho*u_1"
    F(:,2,1) = 2.0_pr*u_prev(:,2)/u_prev(:,1);!!F(2,1)
    F(:,2,1) = F(:,2,1) - (gamma - 1.0_pr)*u_prev(:,2)/u_prev(:,1); !!F(2,1)
    !!F(:,2,2) = u_prev(:,3)/u_prev(:,1);!!G(2,1)

    !! Diag terms due to "U3 = rho*u_2"
    !!F(:,3,1) = u_prev(:,2)/u_prev(:,1); !!F(3,1)
    !!F(:,3,2) = 2.0_pr*u_prev(:,3)/u_prev(:,1);!!G(2,1)
    !!F(:,3,2) = F(:,3,2) - (gamma - 1.0_pr)*u_prev(:,3)/u_prev(:,1); !!G(3,1)

    !! Diag terms due to "U4 = rho*eT"
    F(:,3,1) = gamma*u_prev(:,2)/u_prev(:,1);!!F(4,1)
    !!F(:,4,2) = gamma*u_prev(:,3)/u_prev(:,1);!!G(4,1)

    !! Diag terms due to "U5 = rho*Yf"
    F(:,4,1) = u_prev(:,2)/u_prev(:,1);
    !!F(:,5,2) = u_prev(:,3)/u_prev(:,1);

    !
    !--Form right hand side of NS Equations
    !
       
    DO i = 1, dim
       v_prev(:,i) = u_prev(:,1+i)/u_prev(:,1) ! u_i for previus time step
    END DO

    v_prev(:,dim+1) = (gamma - 1.0_pr)*(u_prev(:,3)/u_prev(:,1) &
                           - 0.5_pr*u_prev(:,2)**2/u_prev(:,1)**2)  ! Temp

    CALL c_diff_fast(v_prev(:,1:dim+1), du(1:dim+1,:,:), d2u(1:dim+1,:,:), j_lev, ng, meth, 10, dim+1, 1, dim+1) 

    muT(:) = m_visc*gamma**m_visc*v_prev(:,dim+1)**(m_visc - 1.0_pr)*(gamma - 1.0_pr)

    muT_prev(:) = (v_prev(:,2)*gamma)**m_visc 


    F(:,2,1) = F(:,2,1) + 4.0_pr/3.0_pr/Re*muT(:)*u_prev(:,2)/u_prev(:,1)**2*du(1,:,1)

    F(:,3,1) = F(:,3,1) - 1.0_pr/Re/Pra*gamma/(gamma - 1.0_pr)*muT(:)*du(dim+1,:,1)/u_prev(:,1)

    F(:,3,1) = F(:,3,1) - 4.0_pr/3.0_pr/Re*muT(:)*du(1,:,1)/u_prev(:,1)*v_prev(:,1)

    user_Drhs_diag = 0.0_pr

    dwf = 1.0_pr*Prexp*EXP(-Tact/v_prev(:,dim+1));

    S(:,1) = 0.0_pr;
    S(:,2) = 0.0_pr;
    S(:,3) = 0.0_pr; !dwf*(gamma - 1.0_pr)*Tact/v_prev(:,dim+1)**2/u_prev(:,1)*qHeat/gamma;
    S(:,4) = 0.0_pr; ! -dwf;

    DO j = 1, dim
       DO i = 1, dim+3
          shift = (i-1)*ng;
          user_Drhs_diag(shift+1:shift+ng) = user_Drhs_diag(shift+1:shift+ng) - F(:,i,j)*du_diag(:,j) + S(:,i);
          !!$ ADD TERMS DUE TO NS:
          !! Terms for Mom Eq. in X,Y Directions "Diagonal terms"
          IF(i>=2 .AND. i <= dim+1) THEN
              user_Drhs_diag(shift+1:shift+ng) = user_Drhs_diag(shift+1:shift+ng) &
                                               + 1.0_pr*muT_prev(:)/Re/u_prev(:,1)*d2u_diag(:,j);
          END IF
          !! Terms for Mom Eq. in X,Y Directions "off-Diagonal terms"
          IF (i == j + 1) THEN
              user_Drhs_diag(shift+1:shift+ng) = user_Drhs_diag(shift+1:shift+ng) &
                                               + 1.0_pr/3.0_pr/Re/u_prev(:,1)*muT_prev(:)*d2u_diag(:,j);
          END IF
          !! Terms for Energy Eq.
          IF(i == dim+2 ) THEN
             user_Drhs_diag(shift+1:shift+ng) = user_Drhs_diag(shift+1:shift+ng) &
                                              + gamma/Re/Pra/u_prev(:,1)*muT_prev(:)*d2u_diag(:,j);
          END IF
          !! Terms due to Fuel EQ.
          IF(i == dim+3 ) THEN
             user_Drhs_diag(shift+1:shift+ng) = user_Drhs_diag(shift+1:shift+ng) &
                                              + 1.0_pr/Re/Sc*muT_prev(:)*d2u_diag(:,j);
          END IF
          
       END DO
    END DO

    IF (hypermodel /= 0) THEN
       CALL hyperbolic_diag(user_Drhs_diag, ng)
    END IF

  END FUNCTION user_Drhs_diag

  FUNCTION user_chi (nlocal, t_local )
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: nlocal
    REAL (pr), INTENT (IN) :: t_local
    REAL (pr), DIMENSION (nlocal) :: user_chi
    !
    !  Create field object here, 1 inside and 0 outside
    !
    user_chi = 0.0_pr
  END FUNCTION user_chi

  !
  ! Calculate any statitics
  !
  ! startup_flag - 0 when adapting to IC,then 1 inmain integration loop
  !

  SUBROUTINE user_stats ( u ,j_mn, flag)

    IMPLICIT NONE
    INTEGER , INTENT (IN) :: flag
    INTEGER , INTENT (IN) :: j_mn
    INTEGER, PARAMETER :: meth=0
    REAL (pr), DIMENSION (ng,1:n_var), INTENT (IN) :: u
    REAL (pr), DIMENSION (ng,1) :: du , d2u , d3u
	
    CHARACTER (LEN=256)  :: flameFileName
    REAL (pr) :: floc, fvel, fth
    INTEGER :: i,i_min,i_max, status
    
    REAL (pr) :: x1, x2, x3, f1, f2 ,f3
    REAL (pr) :: A1, A2, A3, floc1, floc2, Tm, Tn

    flameFileName = flame_file
   
   IF(t .EQ. 0.0_pr .AND. startup_file .EQ. 1.0_pr ) THEN
      
      !!$ Find the location of the flame at the fist time step
      
       xp = FL_R*xyzlimits(2,1);               !!$ Initial Flame location
      !CALL c_diff_fast(u(:,n_var_temp), du(:,1), d2u(:,1), j_lev, n, meth, 11, 1, 1 ,1) ! dT_i/dx
      
      !i = MAXLOC(du(:,1),DIM=1); !! INDEX of where the MAX du is.
      tp = 0.0_pr;
      !xp = x(i,1); !! flame location @ t = 0.0
         OPEN(UNIT=20, FILE=flameFileName, ACTION='WRITE',FORM='FORMATTED',STATUS='UNKNOWN',IOSTAT=status )
         WRITE(UNIT = 20,FMT = '(8(A14,2x))') "t","flame_loc","flame_vel","flame_th"
         startup_file = 0;

   ELSEIF (t - tp >= 25.0_pr ) THEN
      
      CALL c_diff_fast(u(:,n_var_temp), du(:,1), d2u(:,1), j_lev, n, meth, 11, 1, 1 ,1) ! dT_i/dx
      CALL c_diff_fast(du(:,1), d2u(:,1), d3u(:,1), j_lev, n, meth, 01, 1, 1 ,1) ! dT^2,3_i/dx
      
      i = MAXLOC(du(:,1),DIM=1); !! INDEX of where the MAX du is.

      Tm= MAXVAL(u(:,n_var_temp));
      Tn= MINVAL(u(:,n_var_temp));

      !!if (xp .EQ. x(i,1)) THEN      
         floc = x(i,1) - d2u(i,1)/d3u(i,1);
      !!ELSE
      !!floc = x(i,1);
      !!END IF
      
      fth  = (Tm - Tn)/du(i,1); !!$ Flame thickness
      fvel = (floc - xp)/(t - tp); !!$ Flame Velocity

      xp = floc; !!$ previus time step flame location
      tp = t;    !!$ previus time step

      print* ,'Writing info into the flame file:'

      OPEN(UNIT = 20, FILE=flameFileName, ACTION = 'WRITE', FORM = 'FORMATTED', STATUS = 'UNKNOWN', &
           POSITION='APPEND',IOSTAT=status);

      WRITE(UNIT=20,FMT='(8(es14.6,2x))') t , floc , fvel , fth

      CLOSE (20)

   END IF
    
  END SUBROUTINE user_stats


  SUBROUTINE user_cal_force (u, n, t_local, force, drag, lift)
    !--Calculates drag and lift on obstacle using penalization formula
    IMPLICIT NONE

    INTEGER, INTENT (IN) :: n
    REAL (pr), INTENT (IN) :: t_local
    REAL (pr), DIMENSION (dim), INTENT (INOUT) :: force
    REAL (pr), INTENT (OUT) :: drag, lift
    REAL (pr), DIMENSION (n,dim) :: u
    drag = 0.0_PR
    lift = 0.0_PR

    !
    ! There is no obstacle in flow
    !

  END SUBROUTINE user_cal_force

  !
  ! Read input from "case_name"_pde.inp file
  ! case_name is in string file_name read from command line
  ! in read_command_line_input()
  ! 

  SUBROUTINE user_read_input()
    IMPLICIT NONE

  call input_real ('Tact',Tact,'stop', &
       ' Tact: activation Temp')
 
  call input_real ('gamma',gamma,'stop', &
       ' gamma: Gamma = C_p / C_v')

  call input_real ('Re',Re,'stop', &
       ' Re: Reynolds number')

  call input_real ('Pra',Pra,'stop', &
       ' Pra: Prandtle number')

  call input_real ('Sc',Sc,'stop', &
       ' Sc: Schmidt number')

!!$ call input_real ('p0',p0,'stop', &
!!$       'p0: Non dim pressure')

 call input_real ('qHeat',qHeat,'stop', &
       'qHeat: Heat of Reaction')

 call input_real ('Prexp',Prexp,'stop', &
       'Prexponential terms')

 call input_real ('Lambda',Lambda,'stop', &
       'Expansion wavelength')

 call input_logical ('expan_flag',expan_flag,'stop', &
       'expansion/ no expansion flag')

 call input_real ('p2p1',p2p1,'stop', &
       'Shock strength')

 call input_integer ('startup_flag',startup_flag,'stop', &
       'flag for stats file')

 call input_integer ('NS',NS,'stop', &
       'Do transport terms')

 call input_integer ('startup_file',startup_file,'stop', &
       'flag whether to write the flame file or not')

 call input_string ('flame_file',flame_file,'stop', &
       'flame file name:')

 call input_real ('m_visc',m_visc,'stop', &
       'power-law for viscoty')

 call input_real ('delta',delta,'stop', &
       'initial shock thickness')



  END SUBROUTINE user_read_input

  SUBROUTINE user_additional_vars( t_local, flag )
    IMPLICIT NONE
    REAL (pr), INTENT (IN) :: t_local
    INTEGER, INTENT (IN) :: flag ! 0 during initial adaptation, then 1 during time advancement

    u(:,n_var_temp) = (gamma - 1.0_pr)*(u(:,3)/u(:,1) - 0.5_pr*u(:,2)**2/u(:,1)**2)!!$ - u(:,4)/u(:,1)*qHeat) ! Temp
    u(:,n_var_pressure) = u(:,1)*u(:,n_var_temp) ! Pressure
    u(:,n_var_vel)  = u(:,2) / u(:,1)
    u(:,n_var_fuel) = u(:,4) / u(:,1) ! Fuel Fraction
    u(:,n_var_reaction) = Prexp*u(:,4)*EXP(-Tact/u(:,n_var_temp))  ! Reaction Rate

  END SUBROUTINE user_additional_vars


  SUBROUTINE user_scalar_vars( flag )
    IMPLICIT NONE
    INTEGER , INTENT(IN) :: flag ! 0- called during adaption to IC, 1 called during main integration loop

  END SUBROUTINE user_scalar_vars

  !
  !************ Calculating Scales ***************************
  !
  ! Note the order of the components in the scl array
  ! correspond to u_tn, v_tn, w_tn, u_tn-1, v_tn-1, w_tn-1, u_tn-2, v_tn-2, w_tn-2
  ! 
 SUBROUTINE user_scales(flag,use_default, u_loc, nlocal, ne_local, l_n_var_adapt , l_n_var_adapt_index, &
       scl, scl_fltwt) !add
   IMPLICIT NONE
   INTEGER, INTENT (IN) :: flag ! 0 during initial adaptation, then 1 during time advancement
   LOGICAL , INTENT(INOUT) :: use_default
   INTEGER, INTENT (IN) :: nlocal, ne_local
   REAL (pr), DIMENSION (1:nlocal,1:ne_local), INTENT (IN) :: u_loc
   LOGICAL , INTENT (IN) :: l_n_var_adapt(ne_local)
   INTEGER , INTENT (IN) :: l_n_var_adapt_index(1:ne_local)
   REAL (pr), DIMENSION (1:ne_local), INTENT (INOUT) :: scl
   REAL (pr), DIMENSION (1:ne_local) :: mean
   REAL (pr) ,  INTENT(IN) ::scl_fltwt !weight for temporal filter on scl
   REAL (pr) :: floor, tmp,tmpmax,tmpmin,tmpsum
   INTEGER :: i, ie, ie_index, tmpint
   REAL (pr) ,save, ALLOCATABLE, DIMENSION (:) :: scl_old
   LOGICAL  ,save :: startup_init = .TRUE.

   !
   ! Ignore the output of this routine and use default scales routine
   !
   use_default = .TRUE. 

  END SUBROUTINE user_scales

  SUBROUTINE user_cal_cfl (use_default, u, cfl_out)
    USE precision
    USE sizes
    USE pde
    IMPLICIT NONE
    LOGICAL , INTENT(INOUT) :: use_default
    REAL (pr),                                INTENT (INOUT) :: cfl_out
    REAL (pr), DIMENSION (nwlt,n_integrated), INTENT (IN)    :: u
    INTEGER                    :: i
    REAL (pr)                  :: floor
    REAL (pr) :: cfl
    REAL (pr), DIMENSION(dim,nwlt) :: h_arr
    REAL (pr), DIMENSION(nwlt) :: c
    REAL (pr) :: cMax, cSum

    use_default = .FALSE.

    floor = 1e-12_pr
    cfl_out = floor

    c = SQRT( gamma/u(:,n_var_den)*(gamma-1.0_pr)* &
         ( u(:,n_var_eng)-0.5_pr*SUM(u(:,n_var_mom(1:dim))**2,DIM=2)/u(:,1)) )
!    c = SQRT( gamma*(gamma-1.0_pr)*( u(:,n_var_eng)/u(:,1) -0.5_pr*u(:,2)**2/u(:,1)**2))!!$ &
                                   !!$- u(:,4)/u(:,1)*qHeat))

    CALL get_all_local_h (h_arr)

    cSum = 0.0_pr
    DO i=1,dim
       cSum = cSum + MAXVAL( (ABS(u(:,n_var_mom(i))/u(:,n_var_den)) + c) *dt/h_arr(i,:) )
    END DO
    cfl_out = MAX (cfl_out, cSum)
    CALL parallel_global_sum( REALMAXVAL=cfl_out )
    
!!$    cfl = MAXVAL( (ABS(u(:,2)/u(:,1))+c(:))* dt/h_arr(1,:) ) 
!!$    
!!$    cfl_out = MAX(cfl_out, cfl)
!!$    CALL parallel_global_sum( REALMAXVAL=cfl_out )

  END SUBROUTINE user_cal_cfl


  !******************************************************************************************
  !************************************* SGS MODEL ROUTINES *********************************
  !******************************************************************************************
  
  !
  ! Intialize sgs model
  ! This routine is called once in the first
  ! iteration of the main time integration loop.
  ! weights and model filters have been setup for first loop when this routine is called.
  !
  SUBROUTINE user_init_sgs_model( )
    IMPLICIT NONE


    ! LDM: Giuliano

    ! THE sgs forcign is stored for the time step in sgs_mdl_force(1:nlocal,1:n_integrated)
    ! where nlocal should be nwlt.


    !          print *,'initializing LDM ...'       
    !          CALL sgs_mdl_force( u(:,1:n_integrated), nwlt, j_lev, .TRUE.)


  END SUBROUTINE user_init_sgs_model

  !
  ! calculate sgs model forcing term
  ! user_sgs_force is called int he beginning of each times step in time_adv_cn().
  ! THE sgs forcign is stored for the time step in sgs_mdl_force(1:nlocal,1:n_integrated)
  ! where nlocal should be nwlt.
  ! 
  ! Accesses u from field module, 
  !          j_lev from wlt_vars module,
  !
  SUBROUTINE  user_sgs_force (u_loc, nlocal)
    IMPLICIT NONE

    INTEGER,                         INTENT (IN) :: nlocal
    REAL (pr), DIMENSION (nlocal,n_integrated), INTENT (INOUT) :: u_loc

  END SUBROUTINE  user_sgs_force

  FUNCTION user_sound_speed (u, neq, nwlt)
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: nwlt, neq
    REAL (pr), DIMENSION (nwlt,neq), INTENT(IN) :: u
    REAL (pr), DIMENSION (nwlt) :: user_sound_speed

    user_sound_speed(:) = SQRT( gamma*(gamma-1.0_pr)*( u(:,n_var_eng)/u(:,1) -0.5_pr*u(:,2)**2/u(:,1)**2))!!$ &

!!$ SQRT( gamma*u(:,n_var_temp))

  END FUNCTION user_sound_speed

  SUBROUTINE  user_pre_process
    IMPLICIT NONE

  END SUBROUTINE user_pre_process

  SUBROUTINE  user_post_process
    IMPLICIT NONE

  END SUBROUTINE user_post_process

 END MODULE user_case

