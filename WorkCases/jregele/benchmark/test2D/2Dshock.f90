MODULE user_case
  ! 2-D Couette Channel flow
  USE precision
  USE wlt_vars
  USE wavelet_filters_mod
  USE elliptic_mod
  USE elliptic_vars
  USE wlt_trns_mod
  USE wlt_trns_vars
  USE wlt_trns_util_mod
  USE io_3d_vars
  USE util_mod
  USE util_vars
  USE share_consts
  USE pde
  USE sizes
  USE share_kry
  USE vector_util_mod
  USE field
  USE input_file_reader
  USE debug_vars
  USE parallel
  USE hyperbolic_solver

  !
  ! case specific variables if there is any
  !

  REAL (pr) :: FL_R ! flame location ratio
  REAL (pr) :: R   ! Vortex Max Vel ratio to SOS
  REAL (pr) :: Re  ! Reynolds number
  REAL (pr) :: Pra  ! Prandtle number
  REAL (pr) :: gamma ! Gamma number C_p / C_v
  REAL (pr) :: GAM ! Vorticity Magnitude
  REAL (pr) :: p2p1  ! Shock strength
  REAL (pr) :: qHeat !! Heat of reaction
  REAL (pr) :: Prexp !! Prexponential factor
  REAL (pr) :: Tact !! Activation temperature
  REAL (pr) :: x_shock ! shock location
  REAL (pr) :: x_expan ! expansion location
  REAL (pr) :: Sc !! Schmidt Number
  REAL (pr) :: u_p,rho2,e2,Damp
  REAL (pr) :: rho_exp,u_exp,p_exp,e_exp !!$ Expansion conditions
  REAL (pr) :: delta, xL, xR, radius


  !!REAL (pr) :: qHeat !! Heat of reaction

  INTEGER :: n_var_eng, n_var_den , n_var_pressure , n_var_fuel, n_var_fuelD, n_var_reaction, n_var_temp
  
  INTEGER, DIMENSION(2) :: n_var_vel 
  !!INTEGER, DIMENSION(dim), ALLOCATABLE, PUBLIC :: n_var_mom

!  REAL (pr) ::
!  INTEGER ::
!  LOGICAL, PARAMETERS ::

CONTAINS

SUBROUTINE user_setup_pde (VERB)
  IMPLICIT NONE
  LOGICAL, OPTIONAL :: VERB         ! print debug info
  INTEGER :: i,j

    IF (verb_level.GT.0) THEN
       PRINT * ,''
       PRINT *, '**********************Setting up PDE*****************'
       PRINT * ,	'CASE: channel 2-D compressible Couette flow'
       PRINT *, '*****************************************************'
    END IF

    n_integrated = 2 + dim + 1    ! 1 for density, dim = 2 for momentum and 1 for energy(if we have...)
    
    n_var_additional = 1 + 1 + 1 + 1   ! pressure + temperture + Fuel + Reaction

    n_var = n_integrated + n_var_additional !--Total number of variables

    n_var_exact = 0
    
    n_var_den = 1  ! density

    DO i = 1, dim
       n_var_vel(i) = 1 + i   ! Velocities u,v,w
    END DO
    
    n_var_mom = n_var_vel;

    n_var_eng       = 1 + dim      + 1;   ! Energy term
    n_var_fuelD     = n_var_eng    + 1;   ! Fuel*Density
    n_var_reaction  = n_integrated + 1;   ! Reaction
    n_var_pressure  = n_integrated + 2;   ! Pressure
    n_var_temp      = n_integrated + 3;   ! Temp
    n_var_fuel      = n_integrated + 4;   ! Fuel

    !
    !--Allocate logical arrays
    !
    CALL alloc_variable_mappings 

    WRITE (u_variable_names(1),u_variable_names_fmt) 'Density'
    WRITE (u_variable_names(2),u_variable_names_fmt) 'X-Momentum'
    WRITE (u_variable_names(3),u_variable_names_fmt) 'Y-Momentum'
    WRITE (u_variable_names(4),u_variable_names_fmt) 'Energy'
    WRITE (u_variable_names(5),u_variable_names_fmt) 'Dens*Fuel'
    WRITE (u_variable_names(6),u_variable_names_fmt) 'Reaction'
    WRITE (u_variable_names(7),u_variable_names_fmt) 'Pressure'
    WRITE (u_variable_names(8),u_variable_names_fmt) 'Temperature'
    WRITE (u_variable_names(9),u_variable_names_fmt) 'Fuel'

   !WRITE (u_variable_names(-),u_variable_names_fmt) 'Energy'
   !WRITE (u_variable_names(-),u_variable_names_fmt) 'Temperature'

    !
    !--Setup which components we will base grid adaptation on.
    !

    n_var_adapt = .FALSE. !intialize
    n_var_adapt(1:n_integrated+1,0)     = .TRUE. !--Initially adapted variables at first time level
    n_var_adapt(1:n_integrated+1,1)     = .TRUE. !--After first time step adapt on 

    !--integrated variables at first time level
    !Variables that need to be interpoleted to new adapted grid in initial grid adaptation

    n_var_interpolate        = .FALSE. !intialize
    n_var_interpolate(1:n_integrated+n_var_additional,0) = .TRUE.  

    !Variables that need to be interpoleted to new adapted grid at each time step
    n_var_interpolate(1:n_integrated+n_var_additional,1) = .TRUE. 

    !
    ! setup which components we have an exact solution for
    !

    n_var_exact_soln = .FALSE. !intialize
    n_var_exact_soln(1,0:1) = .FALSE. !.TRUE.

    !
    ! setup which variables we will save the solution
    !

    n_var_save = .FALSE. !intialize 
    n_var_save(1:n_var) = .TRUE. ! save all for restarting code

    !
    ! Set the maximum number of components for which we have an exact solution
    !

    n_var_exact = MAX( COUNT(n_var_exact_soln(:,0)),COUNT(n_var_exact_soln(:,1)) )

    !
    ! Setup a scaleCoeff array of we want to tweak the scaling of a variable
    ! ( if scaleCoeff < 1.0 then more points will be added to grid 
    !

    ALLOCATE ( scaleCoeff(1:n_var), Umn(1:n_var) )
    scaleCoeff = 1.0_pr

    IF (VERB) THEN
       PRINT *, 'n_integrated = ',n_integrated 
       PRINT *, 'n_var = ',n_var 
       PRINT *, '*******************Variable Names*******************'
       DO i = 1,n_var
          WRITE (*, u_variable_names_fmt) u_variable_names(i)
       END DO
       PRINT *, '****************************************************'
    END IF

  END SUBROUTINE  user_setup_pde

  !
  ! Set the exact solution for comparison to the simulated solution
  !
  ! u          - array to fill in the exact solution
  ! nlocal       - number of active wavelets  
  ! ne_local        - total number of equations
  ! t          - time of current time step
  ! l_n_var_exact_soln_index - index into the elements of u for which we need to
  !                            find the exact solution

  SUBROUTINE  user_exact_soln (u, nlocal,  t_local, l_n_var_exact_soln)  
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: nlocal
    REAL (pr), INTENT (IN) ::  t_local
    REAL (pr), DIMENSION (nlocal,n_var_exact), INTENT (INOUT) :: u
    LOGICAL , INTENT (IN) :: l_n_var_exact_soln(n_var)
    INTEGER :: i

  END SUBROUTINE  user_exact_soln

  !
  ! Setting up the intial condition for the problem
  !
  
  SUBROUTINE user_initial_conditions (u, nlocal, ne_local, t_local, scl, scl_fltwt,iter)
    IMPLICIT NONE

    INTEGER, INTENT (IN) :: nlocal, ne_local
    INTEGER  , INTENT (INOUT) :: iter ! iteration of call while adapting initial grid
    REAL (pr), DIMENSION (nlocal,ne_local), INTENT (INOUT) :: u
    REAL (pr), DIMENSION(nwlt) :: r2 , theta
    REAL (pr)  :: scl(1:n_var),scl_fltwt
    REAL (pr), INTENT (IN) :: t_local
    INTEGER :: i,j, k, dir
    REAL (pr), DIMENSION (nlocal) :: p,rho, Uy,Ux,Ya, fun

    IF (.NOT. IC_restart) THEN
       
       ! Weak Shock tube problem
       fun = 0.0_pr
       DO i=1,nlocal
          IF ( SUM(x(i,1:dim)**2) .LT. xR**2 ) THEN
             fun(i)=1.0_pr
          END IF
       END DO
       
       rho = fun*(1.0_pr-0.125_pr)+0.125_pr
       p = fun*0.9_pr+0.1_pr
       
       u(:,n_var_den) = rho
       u(:,n_var_fuel) = rho
       u(:,n_var_mom(1:dim)) = 0.0_pr
       u(:,n_var_eng) = p(:)/(gamma-1.0_pr) + 0.5_pr*(SUM(u(:,n_var_mom(1:dim))**2,DIM=2))/rho
       
       IF (verb_level.GT.0) THEN
          print*,'xmax ', maxval(x(:,1)), 'ymax ', maxval(x(:,2))
          
          WRITE( *,'( "in user_IC t_local, nwlt " , f30.20, 1x , i5.5 )' ) t_local , nlocal
       END IF
       
    END IF

  END SUBROUTINE user_initial_conditions

  !--********************************  
  ! Arguments
  ! u         - field on adaptive grid
  ! nlocal      - number of active points
  ! ne_local       - number of equations
  ! t         - current time
  !
  ! Global variables used
  ! ifn       - number of equations
  ! x()
  ! xO
  ! y0
  !--******************************** 

  SUBROUTINE user_algebraic_BC (Lu, u, nlocal, ne_local, jlev, meth)
    !--Defines boundary condition type
    IMPLICIT NONE

    INTEGER , INTENT (IN) :: jlev, meth, ne_local, nlocal
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (INOUT) :: Lu
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (IN)    :: u

    INTEGER :: ie, i, ii, shift, denshift, eshift
    INTEGER, DIMENSION (dim) :: velshift
    REAL (pr), DIMENSION (ne_local,nlocal,dim+2) :: du, d2u !uncomment if Neuman BC are used
    REAL (pr), DIMENSION (1,nlocal,dim) :: du_prev
    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc

    ! Shift markers for BC implementation:
!!$    denshift    = nlocal*(n_var_den    - 1)
!!$    velshift(1) = nlocal*(n_var_mom(1) - 1)
!!$    velshift(2) = nlocal*(n_var_mom(2) - 1)
!!$    eshift      = nlocal*(n_var_mom(2) + 1 - 1) 

    ! Boundary conditions for 2D Couette flow in a channel
    
    !if Neuman BC are used, need to call 
    CALL c_diff_fast(u, du, d2u, jlev, nlocal, meth, 10, ne_local, 1, ne_local)
    
    DO ie = 1, ne_local
       shift = nlocal*(ie-1)
       !--Go through all Boundary points that are specified
       i_p_face(0) = 1
       DO i=1,dim
          i_p_face(i) = i_p_face(i-1)*3
       END DO
       DO face_type = 0, 3**dim - 1
          face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
          IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
             CALL get_all_indices_by_face (face_type, jlev, nloc, iloc)
             IF(nloc > 0 ) THEN

		! ************* Xmin Face ****************!
                IF(face(1) == -1) THEN   ! Entire Xmin face
                   IF(ie == 1) THEN        				
                      Lu(shift+iloc(1:nloc)) = du(ie,iloc(1:nloc),1)  !Neuman conditions
                   ELSE IF (ie == 2) THEN                               
                      Lu(shift+iloc(1:nloc)) = du(ie,iloc(1:nloc),1)  !Neuman conditions
                   ELSE IF (ie == 3) THEN                                
                      Lu(shift+iloc(1:nloc)) = du(ie,iloc(1:nloc),1)  !Neuman conditions
		   ELSE IF (ie == 4) THEN  			        
                      Lu(shift+iloc(1:nloc)) = du(ie,iloc(1:nloc),1)  !Neuman conditions
                   ELSE IF (ie == 5) THEN
                      Lu(shift+iloc(1:nloc)) = du(ie,iloc(1:nloc),1)  !Neuman conditions
                   END IF
		! ************* Xmax Face ****************!
                ELSE IF(face(1) == 1) THEN   ! Entire Xmin face
                   IF(ie == 1) THEN        				  ! Density
                      Lu(shift+iloc(1:nloc)) = du(ie,iloc(1:nloc),1)      ! Nueman
                   ELSE IF (ie == 2) THEN                                 ! Momentum
                      Lu(shift+iloc(1:nloc)) = du(ie,iloc(1:nloc),1)      ! Nueman
                   ELSE IF (ie == 3) THEN                                 ! Momentum     
                      Lu(shift+iloc(1:nloc)) = du(ie,iloc(1:nloc),1)      ! Nueman
                   ELSE IF (ie == 4) THEN  				  ! Energy
                      Lu(shift+iloc(1:nloc)) = du(ie,iloc(1:nloc),1)      ! Nueman
                   ELSE IF (ie == 5) THEN
                      Lu(shift+iloc(1:nloc)) = du(ie,iloc(1:nloc),1)
                   END IF
                END IF
		! ************* Ymin Face ****************!
               IF(face(2) == -1) THEN   ! Entire Ymax face
                   IF(ie == 1) THEN        			                ! Density
                      Lu(shift+iloc(1:nloc)) = du(ie,iloc(1:nloc),2)            ! Nueman
                   ELSE IF (ie == 2) THEN                                       ! Momentum
                      Lu(shift+iloc(1:nloc)) = du(ie,iloc(1:nloc),2)            ! Nueman
                   ELSE IF ( ie == 3) THEN                                      ! Momentum
                      Lu(shift+iloc(1:nloc)) = du(ie,iloc(1:nloc),2)            ! Nueman
                   ELSE IF (ie == 4) THEN  					! Energy
                      Lu(shift+iloc(1:nloc)) = du(ie,iloc(1:nloc),2)            ! Nueman
                   ELSE IF (ie == 5) THEN
                      Lu(shift+iloc(1:nloc)) = du(ie,iloc(1:nloc),2)
                   END IF
		! ************* Ymax Face ****************!
                ELSE IF( face(2) == 1  ) THEN ! ENTIRE Ymin face
                   IF(ie == 1) THEN        			               ! Density
                      Lu(shift+iloc(1:nloc)) = du(ie,iloc(1:nloc),2)           ! Nueman
                   ELSE IF (ie == 2) THEN                                      ! Momentum
                      Lu(shift+iloc(1:nloc)) = du(ie,iloc(1:nloc),2)           ! Nueman
                   ELSE IF (ie == 3) THEN                                      ! Momentum
                      Lu(shift+iloc(1:nloc)) = du(ie,iloc(1:nloc),2)           ! Nueman
                   ELSE IF (ie == 4) THEN  				       ! Energy
                      Lu(shift+iloc(1:nloc)) = du(ie,iloc(1:nloc),2)           ! Nueman
                   ELSE IF (ie == 5) THEN
                      Lu(shift+iloc(1:nloc)) = du(ie,iloc(1:nloc),2)
                   END IF
                END IF
             END IF
          END IF
       END DO
    END DO

!!$     print* ,'****---USER ALGEBRAIC BC VARIABLES---****' 
!!$     print* ,'Den     min: ' , minval(u(denshift    + iloc(1:nloc)))
!!$     print* ,'U Vel   min: ' , minval(u(velshift(1) + iloc(1:nloc)))
!!$     print* ,'V Vel   min: ' , minval(u(velshift(2) + iloc(1:nloc)))
!!$     print* ,'Eng     min: ' , minval(u(eshift      + iloc(1:nloc)))

  END SUBROUTINE user_algebraic_BC

  SUBROUTINE user_algebraic_BC_diag (Lu_diag, nlocal, ne_local, jlev, meth)
    !--Defines boundary condition type
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: jlev, meth, ne_local, nlocal
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (INOUT) :: Lu_diag

    INTEGER :: ie, i, ii, shift, denshift, eshift
    INTEGER, DIMENSION (dim) :: velshift

    REAL (pr), DIMENSION (nlocal,dim+2) :: du, d2u  !uncomment if Neuman BC are used
    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc
    
    ! Shift markers for BC implementation:
!!$    denshift    = nlocal*(n_var_den    - 1)
!!$    velshift(1) = nlocal*(n_var_mom(1) - 1)
!!$    velshift(2) = nlocal*(n_var_mom(2) - 1)
!!$    eshift      = nlocal*(n_var_mom(2) + 1 - 1) 
    
    !if Neuman BC are used, need to call 
    CALL c_diff_diag ( du, d2u, jlev, nlocal, meth, meth, 10)
    
    DO ie = 1, ne_local
       shift = nlocal*(ie-1)
       !--Go through all Boundary points that are specified
       i_p_face(0) = 1
       DO i=1,dim
          i_p_face(i) = i_p_face(i-1)*3
       END DO
       DO face_type = 0, 3**dim - 1
          face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
          IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
             CALL get_all_indices_by_face (face_type, jlev, nloc, iloc)
             IF(nloc > 0 ) THEN
		
		! ************* Xmin *********************** !
                IF( face(1) == -1 ) THEN   ! Entire Xmin face
                   IF(ie == 1) THEN                               ! Density                              
                     Lu_diag(shift+iloc(1:nloc)) = du(iloc(1:nloc),1)    ! Nueman
                   ELSE IF (ie == 2) THEN  ! Momentum
                      Lu_diag(shift+iloc(1:nloc)) = du(iloc(1:nloc),1)    ! Nueman
                   ELSE IF (ie == 3) THEN  ! Momentum
                      Lu_diag(shift+iloc(1:nloc)) = du(iloc(1:nloc),1)    ! Nueman
                   ELSE IF (ie == 4) THEN                                 ! Energy
                      Lu_diag(shift+iloc(1:nloc)) = du(iloc(1:nloc),1)    ! Nueman
                   ELSE IF (ie == 5) THEN
                      Lu_diag(shift+iloc(1:nloc)) = du(iloc(1:nloc),1)    ! Nueman
                   END IF
		! ************* Xmax *********************** !
                ELSE IF( face(1) == 1  ) THEN ! ENTIRE Xmax face
                   IF(ie == 1) THEN                                       ! Density                              
                      Lu_diag(shift+iloc(1:nloc)) = du(iloc(1:nloc),1)    ! Nueman
                   ELSE IF (ie == 2) THEN                                 ! Momentum
                      Lu_diag(shift+iloc(1:nloc)) = du(iloc(1:nloc),1)   
                   ELSE IF (ie == 3) THEN                                 ! Momentum
                      Lu_diag(shift+iloc(1:nloc)) = du(iloc(1:nloc),1)    ! Nueman
                   ELSE IF (ie == 4) THEN                                 ! Energy
                      Lu_diag(shift+iloc(1:nloc)) = du(iloc(1:nloc),1)    ! Nueman
                   ELSE IF (ie == 5) THEN
                      Lu_diag(shift+iloc(1:nloc)) = du(iloc(1:nloc),1)    ! Nueman
                   END IF
                 END IF
		! ************* Ymin *********************** !
                IF( face(2) == -1 ) THEN   ! Entire Ymin face
                   IF(ie == 1) THEN                               ! Density                              
                      Lu_diag(shift+iloc(1:nloc)) = du(iloc(1:nloc),2)     ! Nueman
                   ELSE IF (ie == 2) THEN  ! Momentum
                      Lu_diag(shift+iloc(1:nloc)) = du(iloc(1:nloc),2)    ! Neuman
                   ELSE IF (ie == 3) THEN  ! Momentum
                      Lu_diag(shift+iloc(1:nloc)) = du(iloc(1:nloc),2)    ! Neuman
                   ELSE IF (ie == 4) THEN                                 ! Energy
                      Lu_diag(shift+iloc(1:nloc)) = du(iloc(1:nloc),2)   ! Nueman
                   ELSE IF (ie == 5) THEN
                      Lu_diag(shift+iloc(1:nloc)) = du(iloc(1:nloc),2)   ! Nueman
                   END IF
		! ************* Ymax *********************** !
                ELSE IF( face(2) == 1  ) THEN ! ENTIRE Ymax face
                   IF(ie == 1) THEN                               ! Density                              
                      Lu_diag(shift+iloc(1:nloc)) = du(iloc(1:nloc),2)     ! Nueman
                   ELSE IF (ie == 2) THEN  ! Momentum
                      Lu_diag(shift+iloc(1:nloc)) = du(iloc(1:nloc),2)     ! Nueman
                   ELSE IF (ie == 3) THEN  ! Momentum
                      Lu_diag(shift+iloc(1:nloc)) = du(iloc(1:nloc),2)     ! Nueman
                   ELSE IF (ie == 4) THEN                                ! Energy
                      Lu_diag(shift+iloc(1:nloc)) = du(iloc(1:nloc),2)   ! Nueman
                   ELSE IF (ie == 5) THEN
                      Lu_diag(shift+iloc(1:nloc)) = du(iloc(1:nloc),2)   ! Nueman
                   END IF
                END IF
             END IF
          END IF
       END DO
    END DO

!!$    print* ,'****---BC DIAGONAL ENTRY ---****' 
!!$    print* ,'Den     min: ' , minval(Lu_diag(denshift    + iloc(1:nloc)))
!!$    print* ,'U Vel   min: ' , minval(Lu_diag(velshift(1) + iloc(1:nloc)))
!!$    print* ,'V Vel   min: ' , minval(Lu_diag(velshift(2) + iloc(1:nloc)))
!!$    print* ,'Eng     min: ' , minval(Lu_diag(eshift      + iloc(1:nloc)))

  END SUBROUTINE user_algebraic_BC_diag

  SUBROUTINE user_algebraic_BC_rhs (rhs, ne_local, nlocal, jlev)
    !--Sets rhs for boundary conditions
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: ne_local, nlocal, jlev
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (INOUT) :: rhs

    INTEGER :: ie, i, ii, shift, denshift, eshift
    INTEGER, DIMENSION (dim) :: velshift

    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc

    ! Shift markers for BC implementation:
!!$    denshift    = nlocal*(n_var_den    - 1)
!!$    velshift(1) = nlocal*(n_var_mom(1) - 1)
!!$    velshift(2) = nlocal*(n_var_mom(2) - 1)
!!$    eshift      = nlocal*(n_var_mom(2) + 1 - 1) 

    DO ie = 1, ne_local
       shift = nlocal*(ie-1)
       !--Go through all Boundary points that are specified
       i_p_face(0) = 1
       DO i=1,dim
          i_p_face(i) = i_p_face(i-1)*3
       END DO
       DO face_type = 0, 3**dim - 1
          face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
          IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
             CALL get_all_indices_by_face (face_type, jlev, nloc, iloc)
             IF(nloc > 0 ) THEN

		! ************* Xmin *********************** !
                IF( face(1) == -1 ) THEN   ! Entire Xmin face
                   IF(ie == 1) THEN             ! Density
                      rhs(shift+iloc(1:nloc)) = 0.0_pr 
                   ELSE IF (ie == 2) THEN   ! Momentum
                      rhs(shift+iloc(1:nloc)) = 0.0_pr !
                   ELSE IF (ie == 3) THEN  ! Momentum
                      rhs(shift+iloc(1:nloc)) = 0.0_pr 
                   ELSE IF (ie == 4) THEN               ! Energy
                      rhs(shift+iloc(1:nloc)) = 0.0_pr
                   ELSE IF (ie == 5) THEN               ! fuel
                      rhs(shift+iloc(1:nloc)) = 0.0_pr
                   END IF
		! ************* Xmax *********************** !
                ELSE IF( face(1) == 1  ) THEN ! ENTIRE Xmax face
                   IF(ie == 1) THEN             ! Density
                      rhs(shift+iloc(1:nloc)) = 0.0_pr  ! Nueman:     
                   ELSE IF (ie == 2) THEN   ! Momentum
                      rhs(shift+iloc(1:nloc)) = 0.0_pr 
                   ELSE IF (ie == 3) THEN  ! Momentum
                      rhs(shift+iloc(1:nloc)) = 0.0_pr  
                   ELSE IF (ie == 4) THEN               ! Energy
                      rhs(shift+iloc(1:nloc)) = 0.0_pr
                   ELSE IF (ie == 5) THEN               ! fuel
                      rhs(shift+iloc(1:nloc)) = 0.0_pr  
                   END IF
                END IF
		! ************* Ymin *********************** !
                IF( face(2) == -1 ) THEN   ! Entire Ymin face
                   IF(ie == 1) THEN             ! Density
                     rhs(shift+iloc(1:nloc)) = 0.0_pr  ! Nueman:    
                   ELSE IF (ie == 2) THEN   ! Momentum
                      rhs(shift+iloc(1:nloc)) = 0.0_pr 
                   ELSE IF (ie == 3) THEN  ! Momentum
                      rhs(shift+iloc(1:nloc)) = 0.0_pr 
                   ELSE IF (ie == 4) THEN               ! Energy
                      rhs(shift+iloc(1:nloc)) = 0.0_pr 
                   ELSE IF (ie == 5) THEN               ! fuel
                      rhs(shift+iloc(1:nloc)) = 0.0_pr  
                   END IF
		! ************* Ymax *********************** !
                ELSE IF( face(2) == 1  ) THEN ! ENTIRE Ymax face
                   IF(ie == 1) THEN            ! Density
                      rhs(shift+iloc(1:nloc)) = 0.0_pr  ! Nueman:    
                   ELSE IF (ie == 2) THEN  ! Momentum
                      rhs(shift+iloc(1:nloc)) = 0.0_pr  
                   ELSE IF (ie == 3) THEN ! Momentum
                      rhs(shift+iloc(1:nloc)) = 0.0_pr  
                   ELSE IF (ie == 4) THEN              ! Energy
                      rhs(shift+iloc(1:nloc)) = 0.0_pr
                   ELSE IF (ie == 5) THEN               ! fuel
                      rhs(shift+iloc(1:nloc)) = 0.0_pr  
                   END IF
                END IF
              END IF
           END IF
        END DO
     END DO

!!$     print* ,'****---RHS BC ENTRY---****' 
!!$     print* ,'Den     min: ' , minval(rhs(denshift    + iloc(1:nloc)))
!!$     print* ,'U Vel   min: ' , minval(rhs(velshift(1) + iloc(1:nloc)))
!!$     print* ,'V Vel   min: ' , minval(rhs(velshift(2) + iloc(1:nloc)))
!!$     print* ,'Eng     min: ' , minval(rhs(eshift      + iloc(1:nloc)))

  END SUBROUTINE user_algebraic_BC_rhs

  SUBROUTINE user_project (u, p, nlocal, meth)
    !--Makes u divergence free
    IMPLICIT NONE

    INTEGER, INTENT (IN) :: meth, nlocal
    REAL (pr), DIMENSION (nlocal),     INTENT(INOUT) :: p
    REAL (pr), DIMENSION (nlocal,n_integrated), INTENT(INOUT) :: u

    REAL (pr), DIMENSION (nlocal) :: dp
    REAL (pr), DIMENSION (nlocal) :: f

    INTEGER :: i
    INTEGER, PARAMETER :: ne = 1
    INTEGER, DIMENSION(ne) :: clip 

  END SUBROUTINE user_project

  !
  ! Setting up the RHS of the main equations
  !

  FUNCTION user_rhs (u_integrated,scalar)
    IMPLICIT NONE
    REAL (pr), DIMENSION (ng,ne), INTENT(IN) :: u_integrated !1D flat version of integrated variables without BC 
    REAL (pr), DIMENSION (ng), INTENT(IN) :: scalar
    REAL (pr), DIMENSION (ng*ne) :: user_rhs
    INTEGER :: i, j, ie, shift
    INTEGER, PARAMETER :: meth=0
    REAL (pr), DIMENSION (n_integrated,ng,dim+2) :: du, d2u

    !
    ! User defined variables
    !
    REAL (pr), DIMENSION (ng,n_integrated,dim) :: F  ! Flux terms
    REAL (pr), DIMENSION (ng,dim+2) :: v             ! velocity components + temp + fuel
    REAL (pr), DIMENSION (ng) :: p                   ! pressures

    REAL (pr), DIMENSION (ng,n_integrated) :: S      ! Source terms
    REAL (pr), DIMENSION (ng) :: wf                  ! reaction rate
    REAL (pr), DIMENSION (ng) :: c                   ! sound speed
    
    ! Setting up pressure from ideal gas assumption
    p(:) = (gamma - 1.0_pr)*(u_integrated(:,n_var_eng) - &
                     0.5_pr*(u_integrated(:,2)**2 + u_integrated(:,3)**2)/u_integrated(:,1)) ! pressure
     
!!$     print* ,'****---USER_RHS VARIABLES---****' 
!!$     print* ,'density  min: ' , minval(u_integrated(:,1))
!!$     print* ,'energy   min: ' , minval(u_integrated(:,4))
!!$     print* ,'velocity min: ' , minval(u_integrated(:,2))
!!$     print* ,'velocity min: ' , minval(u_integrated(:,3))
!!$     print* ,'pressure min: ' , minval(p)
    
    ! Finding the velocities and Temperature
    v(:,1) = u_integrated(:,n_var_vel(1))/u_integrated(:,n_var_den) ! u_i
    v(:,2) = u_integrated(:,n_var_vel(2))/u_integrated(:,n_var_den) ! u_j

    v(:,3) = p(:)/u_integrated(:,n_var_den)  ! Temperature

    v(:,4) = u_integrated(:,n_var_fuelD)/u_integrated(:,1)         ! Fuel mass fractions

    !
    !--Form right hand side of Euler Equations(Terms without trasnport terms)
    !
    
    !!$ MASS CONSERVATION EQUATION:
    F(:,1,1) = u_integrated(:,2) ! rho*u_j : Mass Cons. Eq. X
    F(:,1,2) = u_integrated(:,3) ! rho*u_j : Mass Cons. Eq. Y
    
    !!$ MOMENTUM CONSERVATION EQUATION:
    F(:,2,1) = u_integrated(:,2)*u_integrated(:,2)/u_integrated(:,1) ! rho*u_i*u_i : X-Momentum
    F(:,2,2) = u_integrated(:,2)*u_integrated(:,3)/u_integrated(:,1) ! rho*u_i*u_j : X-Momentum
    F(:,2,1) = F(:,2,1) + p(:)                                       ! ... + p     : X-Momentum pressure term

    F(:,3,1) = u_integrated(:,2)*u_integrated(:,3)/u_integrated(:,1) ! rho*u_i*u_j : Y-Momentum
    F(:,3,2) = u_integrated(:,3)*u_integrated(:,3)/u_integrated(:,1) ! rho*u_j*u_j : Y-Momentum
    F(:,3,2) = F(:,3,2) + p(:)                                       ! ... + p     : Y-Momentum pressure term

    !!$ ENERGY CONSERVATION TERMS 
    F(:,4,1) = ( u_integrated(:,4) + p(:) )*u_integrated(:,2)/u_integrated(:,1) ! Energy Eq. X
    F(:,4,2) = ( u_integrated(:,4) + p(:) )*u_integrated(:,3)/u_integrated(:,1) ! Energy Eq. Y

    !!$ FUEL CONSERVATION EQUATION
    F(:,5,1) = u_integrated(:,5)*u_integrated(:,2)/u_integrated(:,1) ! Fuel convective term X
    F(:,5,2) = u_integrated(:,5)*u_integrated(:,3)/u_integrated(:,1) ! Fuel convective term Y

    !
    !-- Form right hand side of Navier-Stokes Equations' transport and Conduction terms
    !
    
    !! find the derivatives of velocities and temperature
    CALL c_diff_fast(v, du(1:dim+2,:,:), d2u(1:dim+2,:,:), j_lev, ng, meth, 10, dim+2, 1,dim+2) ! du_i/dx_j
    
    !!$ Diagonal transport terms for X & Y Momentum
    F(:,2,1) = F(:,2,1) - 1.0_pr/3.0_pr/Re*(4.0_pr*du(1,:,1) - 2.0_pr*du(2,:,2)) ! ... - 1/Re*du_k/dx_k
    F(:,3,2) = F(:,3,2) - 1.0_pr/3.0_pr/Re*(4.0_pr*du(2,:,2) - 2.0_pr*du(1,:,1)) ! ... - 1/Re*du_k/dx_k

    !!$ Off-diagonal transport terms for X & Y momentum 
    F(:,2,2) = F(:,2,2) - 1.0_pr/Re*(du(1,:,2) + du(2,:,1)) ! ... - 1/Re*du_i/dx_j
    F(:,3,1) = F(:,3,1) - 1.0_pr/Re*(du(2,:,1) + du(1,:,2)) ! ... - 1/Re*du_i/dx_j
    
    !!$ Energy Transport terms in X-direction
    F(:,4,1) = F(:,4,1) - 1.0_pr/3.0_pr/Re*(4.0_pr*du(1,:,1) - 2.0_pr*du(2,:,2))*v(:,1) !... - 1/Re*du_k/dx_k*u_i
    F(:,4,2) = F(:,4,2) - 1.0_pr/Re*(du(1,:,2) + du(2,:,1))*v(:,1)                      !... - 1/Re*du_i/dx_j*u_i

    !!$ Energy Transport terms in Y-direction
    F(:,4,2) = F(:,4,2) - 1.0_pr/3.0_pr/Re*(4.0_pr*du(2,:,2) - 2.0_pr*du(1,:,1))*v(:,2) !... - 1/Re*du_k/dx_k*u_i
    F(:,4,1) = F(:,4,1) - 1.0_pr/Re*(du(1,:,2) + du(2,:,1))*v(:,2)                      !... - 1/Re*du_i/dx_j*u_i

    F(:,4,1) = F(:,4,1) - 1.0_pr/Re/Pra*du(3,:,1) ! Conduction term in Energy EQ. X
    F(:,4,2) = F(:,4,2) - 1.0_pr/Re/Pra*du(3,:,2) ! Conduction term in Energy EQ. Y

    !!$ FUEL CONSERVATION EQUATION
    F(:,5,1) = F(:,5,1) - 1.0_pr/Re/Sc*u_integrated(:,1)*du(4,:,1) ! Fuel Equation EQ. X
    F(:,5,2) = F(:,5,2) - 1.0_pr/Re/Sc*u_integrated(:,1)*du(4,:,2) ! Fuel Equation EQ. Y

    wf = 1.0_pr*Prexp*u_integrated(:,5)*EXP(-Tact/v(:,3)); ! Reaction rate

    ! Add source term if there is any
    S(:,1) = 0.0_pr; !! Mass
    S(:,2) = 0.0_pr; !! Momentum X
    S(:,3) = 0.0_pr; !! Momentum Y
    S(:,4) = 0.0_pr; !wf*qHeat; !! Energy
    S(:,5) = 0.0_pr; !-1.0_pr*wf;   ! Fuel Equation terms

    !
    ! -- Find derivatives wrt X or Y
    !
    user_rhs = 0.0_pr
    DO j = 1, dim
       CALL c_diff_fast(F(:,1:dim+3,j), du(1:dim+3,:,:), d2u(1:dim+3,:,:), j_lev, ng, meth, 10, dim+3, 1, dim+3) ! du(i,:,k)=dF_ij/dx_k
       DO i = 1, dim+3
          shift=(i-1)*ng
          user_rhs(shift+1:shift+ng) = user_rhs(shift+1:shift+ng) - du(i,:,j)  ! - dF_ij/dx_j
       END DO
    END DO

    ! Create flux limiter/diffusion for current solution iteration
!!$    c = user_sound_speed(u_integrated,n_integrated,ng)
!!$    CALL hyper_pre_process (u_integrated,ng,n_integrated,c)
    CALL hyperbolic(u_integrated, ng, user_rhs)

  END FUNCTION user_rhs

 !
 ! find Jacobian of Right Hand Side of the problem
 !
 
 FUNCTION user_Drhs (u, u_prev, meth)
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: meth
    REAL (pr), DIMENSION (ng,ne) :: u, u_prev
    REAL (pr), DIMENSION (n) :: user_Drhs

    INTEGER :: i, j, ie, shift
    REAL (pr), DIMENSION (n_integrated,ng,dim+2) :: du, d2u, dup, d2up
    !
    ! User defined variables
    !
    REAL (pr), DIMENSION (ng,n_integrated,dim) :: F
    REAL (pr), DIMENSION (ng,dim+2) :: v, v_prev      ! velocity components + temperature
    REAL (pr), DIMENSION (ng) :: p,  p_prev 
    REAL (pr), DIMENSION (ng) :: c                   ! sound speed

    ! Setting up the pressure terms " p_i + p'_i "
    p_prev(:) = (gamma-1.0_pr)*(u_prev(:,dim+2) &
                      -  0.5_pr*(u_prev(:,2)**2 + u_prev(:,3)**2)/u_prev(:,1)) ! previous pressure

    p(:) = (gamma - 1.0_pr)*( u(:,n_var_eng) &
                          - u_prev(:,2)*u(:,2)/u_prev(:,1) &
                          - u_prev(:,3)*u(:,3)/u_prev(:,1) &
                          + 0.5_pr*(u_prev(:,2)**2 + u_prev(:,3)**2)/u_prev(:,1)**2*u(:,1))   ! pressure

!!$     print* ,'****---USER_DRHS VARIABLES---****' 
!!$     print* ,'energy   min_2: ' , minval(u_prev(:,4))
!!$     print* ,'velocity min_2: ' , minval(u_prev(:,2))
!!$     print* ,'density  min_2: ' , minval(u_prev(:,1))
!!$     print* ,'pressure min_2: ' , minval(p_prev)
!!$     print* ,'pressure min_2: ' , minval(p)

   ! Finding the velocities and temperature
   v_prev(:,1) = u_prev(:,2)/u_prev(:,1)     ! u_1 = U2/U1
   v_prev(:,2) = u_prev(:,3)/u_prev(:,1)     ! u_2 = U3/U1 

   v_prev(:,3) = p_prev(:)/u_prev(:,1)       ! Temperature 

   v_prev(:,4) = u_prev(:,n_var_fuelD)/u_prev(:,1)         ! Fuel mass fractions

   ! derivative terms u',v',T'
   v(:,1) = -u_prev(:,2)/u_prev(:,1)**2*u(:,1) + 1.0_pr/u_prev(:,1)*u(:,2)      ! U2/U1
   v(:,2) = -u_prev(:,3)/u_prev(:,1)**2*u(:,1) + 1.0_pr/u_prev(:,1)*u(:,3)      ! U3/U1

   v(:,3) = p(:)/u_prev(:,1) - p_prev(:)/u_prev(:,1)**2*u(:,1)           ! Temperature

   v(:,4) = (-u_prev(:,n_var_fuelD)/u_prev(:,1)**2)*u(:,1) + u(:,n_var_fuelD)/u_prev(:,1);     ! Fuel v' = Yf'
    
   !
   !--Form right hand side of Euler Equations
   !

   !$$ MASS CONSERVATION 
   F(:,1,1) = u(:,2) ! rho*u_i 
   F(:,1,2) = u(:,3) ! rho*u_j

   !$$ MOMENTUM CONSERVATION
   F(:,2,1) = -u_prev(:,2)**2/u_prev(:,1)**2*u(:,1)             ! X-MOMENTUM Eq. WRT U1              
   F(:,2,1) = F(:,2,1) + 2.0_pr*u_prev(:,2)/u_prev(:,1)*u(:,2)  ! X-MOMENTUM Eq. WRT U2
   F(:,2,1) = F(:,2,1) + p(:)   	                        ! ... + pressure terms

   F(:,2,2) = -u_prev(:,2)*u_prev(:,3)/u_prev(:,1)**2*u(:,1)    ! X-MOMENTUM Eq. WRT U1
   F(:,2,2) = F(:,2,2) + u_prev(:,3)/u_prev(:,1)*u(:,2)         ! X-MOMENTUM Eq. WRT U2
   F(:,2,2) = F(:,2,2) + u_prev(:,2)/u_prev(:,1)*u(:,3)         ! X-MOMENTUM Eq. WRT U3
  
   F(:,3,2) = -u_prev(:,3)**2/u_prev(:,1)**2*u(:,1)             ! Y-MOMENTUM Eq. WRT U1              
   F(:,3,2) = F(:,3,2) + 2.0_pr*u_prev(:,3)/u_prev(:,1)*u(:,3)  ! Y-MOMENTUM Eq. WRT U3
   F(:,3,2) = F(:,3,2) + p(:)   	                        ! ... + pressure terms

   F(:,3,1) = -u_prev(:,2)*u_prev(:,3)/u_prev(:,1)**2*u(:,1)    ! Y-MOMENTUM Eq. WRT U1
   F(:,3,1) = F(:,3,1) + u_prev(:,3)/u_prev(:,1)*u(:,2)         ! Y-MOMENTUM Eq. WRT U2
   F(:,3,1) = F(:,3,1) + u_prev(:,2)/u_prev(:,1)*u(:,3)         ! Y-MOMENTUM Eq. WRT U3
  
   !$$ ENERGY CONSERVATION 
   F(:,4,1) = (u(:,4) + p(:))*u_prev(:,2)/u_prev(:,1) &                   ! Energy EQ. X WRT U4 and pressure
            + (u_prev(:,4)/u_prev(:,1) + p_prev(:)/u_prev(:,1))*u(:,2)&   ! Energy Eq. X WRT U2
            - (u_prev(:,4) + p_prev(:))*u_prev(:,2)/u_prev(:,1)**2*u(:,1) ! Energy Eq. X WRT U1 
   
   F(:,4,2) = (u(:,4) + p(:))*u_prev(:,3)/u_prev(:,1) &                   ! Energy EQ. Y WRT U4 and pressure
            + (u_prev(:,4)/u_prev(:,1) + p_prev(:)/u_prev(:,1))*u(:,3)&   ! Energy Eq. Y WRT U3
            - (u_prev(:,4) + p_prev(:))*u_prev(:,3)/u_prev(:,1)**2*u(:,1) ! Energy Eq. Y WRT U1 

    !!$ FUEL CONSERVATION 
    F(:,5,1) = (-u_prev(:,2)*u_prev(:,5)/u_prev(:,1)**2)*u(:,1) &   ! U1 terms
                + u_prev(:,5)/u_prev(:,1)*u(:,2) &                  ! U2 terms
                + u_prev(:,2)/u_prev(:,1)*u(:,5) ;                  ! U4 terms

    F(:,5,2) = (-u_prev(:,3)*u_prev(:,5)/u_prev(:,1)**2)*u(:,1) &   ! U1 terms
                + u_prev(:,5)/u_prev(:,1)*u(:,3) &                  ! U2 terms
                + u_prev(:,3)/u_prev(:,1)*u(:,5) ;                  ! U4 terms
 
    !
    !-- Form right hand side of Navier Stokes Equations for Transport terms
    !
    !!$ find the derivatives of velocities and temperature
    CALL c_diff_fast(v,      du (1:2+dim,:,:) ,d2u (1:2+dim,:,:), j_lev, ng , meth , 10 , 2+dim , 1 , 2+dim)
    CALL c_diff_fast(v_prev, dup(1:2+dim,:,:) ,d2up(1:2+dim,:,:), j_lev, ng , meth , 10 , 2+dim , 1 , 2+dim)
    
    ! Diagonal Terms for MOMENTUM
    F(:,2,1) = F(:,2,1) - 1.0_pr/3.0_pr/Re*(4.0_pr*du(1,:,1) - 2.0_pr*du(2,:,2))
    F(:,3,2) = F(:,3,2) - 1.0_pr/3.0_pr/Re*(4.0_pr*du(2,:,2) - 2.0_pr*du(1,:,1))
    
    ! Off-diagonal Terms for MOMENTUM
    F(:,3,1) = F(:,3,1) - 1.0_pr/Re*(du(1,:,2) + du(2,:,1)) 
    F(:,2,2) = F(:,2,2) - 1.0_pr/Re*(du(1,:,2) + du(2,:,1)) 
    
    ! Transport terms in ENERGY
    F(:,4,1) = F(:,4,1) - 1.0_pr/3.0_pr/Re*(v(:,1)*(4.0_pr*dup(1,:,1) - 2.0_pr*dup(2,:,2)) &
                                          + v_prev(:,1)*(4.0_pr*du(1,:,1)  - 2.0_pr*du(2,:,2))) &
                        - 1.0_pr/Re*(v(:,2)*(dup(1,:,2) + dup(2,:,1)) & 
                                   + v_prev(:,2)*(du(1,:,2) + du(2,:,1)))            ! Energy Eq. X

    F(:,4,2) = F(:,4,2) - 1.0_pr/3.0_pr/Re*(v(:,2)*(4.0_pr*dup(2,:,2) - 2.0_pr*dup(1,:,1)) &
                                          + v_prev(:,2)*(4.0_pr*du(2,:,2)  - 2.0_pr*du(1,:,1))) &
                        - 1.0_pr/Re*(v(:,1)*(dup(1,:,2) + dup(2,:,1)) & 
                                  + v_prev(:,1)*(du(1,:,2) + du(2,:,1)))            ! Energy Eq. Y
    
    ! Conduction term in ENERGY
    F(:,4,1) = F(:,4,1) - 1.0_pr/Re/Pra*du(3,:,1) 
    F(:,4,2) = F(:,4,2) - 1.0_pr/Re/Pra*du(3,:,2)                       
    
    !!$ FUEL TERMS
    F(:,5,1) = F(:,5,1) - 1.0_pr/Re/Sc*dup(4,:,1)*u(:,1);
    F(:,5,2) = F(:,5,2) - 1.0_pr/Re/Sc*dup(4,:,2)*u(:,1);

    F(:,5,1) = F(:,5,1) - 1.0_pr/Re/Sc*u_prev(:,1)*du(4,:,1);   ! Fuel terms 
    F(:,5,2) = F(:,5,2) - 1.0_pr/Re/Sc*u_prev(:,1)*du(4,:,2);   ! Fuel terms 

    ! -- Find derivatives wrt X or Y
    user_Drhs = 0.0_pr
    DO j = 1, dim
       CALL c_diff_fast(F(:,1:3+dim,j), du(1:3+dim,:,:), d2u(1:3+dim,:,:), j_lev, ng, meth, 10, 3+dim, 1, 3+dim)
       DO i = 1, 3+dim
          shift=(i-1)*ng
          user_Drhs(shift+1:shift+ng) = user_Drhs(shift+1:shift+ng) - du(i,:,j)  ! -dF_ij/dx_j
       END DO
    END DO

    ! Create flux limiter/diffusion for current solution iteration
!!$    c = user_sound_speed(u_integrated,n_integrated,ng)
!!$    CALL hyper_pre_process (u_integrated,ng,n_integrated,c)
    CALL hyperbolic(u, ng, user_Drhs)

  END FUNCTION user_Drhs

  !
  ! Uses u_prev_timestep, which is global flat (1D ) array set in time_step_cn()
  !
  FUNCTION user_Drhs_diag (meth)
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: meth
    REAL (pr), DIMENSION (n) :: user_Drhs_diag

    INTEGER :: i,j, ie, shift
    REAL (pr), DIMENSION (ng,dim) :: du_diag, d2u_diag, du_diag_tmp
    REAL (pr), DIMENSION (n_integrated,ng,dim) :: du, d2u
    REAL (pr), DIMENSION (ng,n_integrated) :: S      ! Source terms
    REAL (pr), DIMENSION (ng) :: dwf

    !
    ! User defined variables
    !
    REAL (pr), DIMENSION (ng,n_integrated,dim) :: F
    REAL (pr), DIMENSION (ng,ne) :: u_prev
    REAL (pr), DIMENSION (ng,dim+2) :: v_prev ! velocity components
    REAL (pr), DIMENSION (ng) :: p 
    REAL (pr), DIMENSION (ng) :: c                   ! sound speed

    ! Create u_prev from u_prev_timestep
    DO ie = 1, ne
       shift=(ie-1)*ng
       u_prev(1:ng,ie) = u_prev_timestep(shift+1:shift+ng)
    END DO

    !
    ! Calculate Diagonal derivative term
    !
    CALL c_diff_diag(du_diag, d2u_diag, j_lev, ng, meth, meth, -11)

    !!$ F(:,"No. of eq.",Dimension("F or G in my notes"))

    !
    !--Form right hand side of Euler Equations
    !
    
    !! Diag terms due to "U1 = rho"
    F(:,1,1) = 0.0_pr; !!F(1,1)
    F(:,1,2) = 0.0_pr; !!G(1,1)

    !! Diag terms due to "U2 = rho*u_1"
    F(:,2,1) = 2.0_pr*u_prev(:,2)/u_prev(:,1);!!F(2,1)
    F(:,2,1) = F(:,2,1) - (gamma - 1.0_pr)*u_prev(:,2)/u_prev(:,1); !!F(2,1)
    F(:,2,2) = u_prev(:,3)/u_prev(:,1);!!G(2,1)

    !! Diag terms due to "U3 = rho*u_2"
    F(:,3,1) = u_prev(:,2)/u_prev(:,1); !!F(3,1)
    F(:,3,2) = 2.0_pr*u_prev(:,3)/u_prev(:,1);!!G(2,1)
    F(:,3,2) = F(:,3,2) - (gamma - 1.0_pr)*u_prev(:,3)/u_prev(:,1); !!G(3,1)

    !! Diag terms due to "U4 = rho*eT"
    F(:,4,1) = gamma*u_prev(:,2)/u_prev(:,1);!!F(4,1)
    F(:,4,2) = gamma*u_prev(:,3)/u_prev(:,1);!!G(4,1)

    !! Diag terms due to "U5 = rho*Yf"
    F(:,5,1) = u_prev(:,2)/u_prev(:,1);!!F(5,1)
    F(:,5,2) = u_prev(:,3)/u_prev(:,1);!!G(5,1)

    !
    !--Form right hand side of NS Equations
    !
       
    DO i = 1, dim
       v_prev(:,i) = u_prev(:,1+i)/u_prev(:,1) ! u_i for previus time step
    END DO

    v_prev(:,dim+1) = (gamma - 1.0_pr)*(u_prev(:,4)/u_prev(:,1) &
                           - 0.5_pr*(u_prev(:,2)**2+u_prev(:,3)**2)/u_prev(:,1)**2)  ! Temp

    user_Drhs_diag = 0.0_pr

    dwf = 1.0_pr*Prexp*EXP(-Tact/v_prev(:,dim+1));

    S(:,1) = 0.0_pr;
    S(:,2) = 0.0_pr;
    S(:,3) = 0.0_pr;
    S(:,4) = dwf*(gamma - 1.0_pr)*Tact/v_prev(:,dim+1)**2/u_prev(:,1)*qHeat;
    S(:,5) = -dwf;

    DO j = 1, dim
       DO i = 1, dim+3
          shift = (i-1)*ng;
          user_Drhs_diag(shift+1:shift+ng) = user_Drhs_diag(shift+1:shift+ng) - F(:,i,j)*du_diag(:,j) + S(:,i);
          !!$ ADD TERMS DUE TO NS:
          !! Terms for Mom Eq. in X,Y Directions "Diagonal terms"
          IF(i>=2 .AND. i <= dim+1) THEN
              user_Drhs_diag(shift+1:shift+ng) = user_Drhs_diag(shift+1:shift+ng) &
                                               + 1.0_pr/Re/u_prev(:,1)*d2u_diag(:,j);
          END IF
          !! Terms for Mom Eq. in X,Y Directions "off-Diagonal terms"
          IF (i == j + 1) THEN
              user_Drhs_diag(shift+1:shift+ng) = user_Drhs_diag(shift+1:shift+ng) &
                                               + 1.0_pr/3.0_pr/Re/u_prev(:,1)*d2u_diag(:,j);
          END IF
          !! Terms for Energy Eq.
          IF(i == dim+2 ) THEN
             user_Drhs_diag(shift+1:shift+ng) = user_Drhs_diag(shift+1:shift+ng) &
                                              + (gamma - 1.0_pr)/Re/Pr/u_prev(:,1)*d2u_diag(:,j);
          END IF
          !! Terms due to Fuel EQ.
          IF(i == dim+3 ) THEN
             user_Drhs_diag(shift+1:shift+ng) = user_Drhs_diag(shift+1:shift+ng) &
                                              + 1.0_pr/Re/Sc*d2u_diag(:,j);
          END IF
          
       END DO
    END DO

    ! Create flux limiter/diffusion for current solution iteration
!!$    c = user_sound_speed(u_integrated,n_integrated,ng)
!!$    CALL hyper_pre_process (u_integrated,ng,n_integrated,c)
    CALL hyperbolic_diag(user_Drhs_diag, ng)

  END FUNCTION user_Drhs_diag

  FUNCTION user_chi (nlocal, t_local )
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: nlocal
    REAL (pr), INTENT (IN) :: t_local
    REAL (pr), DIMENSION (nlocal) :: user_chi
    !
    !  Create field object here, 1 inside and 0 outside
    !
    user_chi = 0.0_pr
  END FUNCTION user_chi


  !
  ! Calculate any statitics
  !
  ! startup_flag - 0 when adapting to IC,then 1 inmain integration loop
  !

  SUBROUTINE user_stats ( u ,j_mn, startup_flag)
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: startup_flag
    INTEGER , INTENT (IN) :: j_mn 
    REAL (pr), DIMENSION (nwlt,1:n_var), INTENT (IN) :: u
  END SUBROUTINE user_stats


  SUBROUTINE user_cal_force (u, n, t_local, force, drag, lift)
    !--Calculates drag and lift on obstacle using penalization formula
    IMPLICIT NONE

    INTEGER, INTENT (IN) :: n
    REAL (pr), INTENT (IN) :: t_local
    REAL (pr), DIMENSION (dim), INTENT (INOUT) :: force
    REAL (pr), INTENT (OUT) :: drag, lift
    REAL (pr), DIMENSION (n,dim) :: u
    drag = 0.0_PR
    lift = 0.0_PR

    !
    ! There is no obstacle in flow
    !

  END SUBROUTINE user_cal_force

  !
  ! Read input from "case_name"_pde.inp file
  ! case_name is in string file_name read from command line
  ! in read_command_line_input()
  ! 


  SUBROUTINE user_read_input()
    IMPLICIT NONE

  call input_real ('FL_R',FL_R,'stop', &
       'Flame location ratio')

  call input_real ('gamma',gamma,'stop', &
       ' gamma: Gamma = C_p / C_v')

  call input_real ('Re',Re,'stop', &
       ' Re: Reynolds number')

  call input_real ('Pra',Pra,'stop', &
       ' Pra: Prandtle number')

 call input_real ('p2p1',p2p1,'stop', &
       'Shock Strength')

 call input_real ('Sc',Sc,'stop', &
       ' Sc: Schmidt number')

 call input_real ('qHeat',qHeat,'stop', &
       'qHeat: Heat of Reaction')
 call input_real ('x_expan',x_expan,'stop', &
       'expansion wave')

 call input_real ('x_shock',x_shock,'stop', &
       'Shock location')

 call input_real ('Prexp',Prexp,'stop', &
       'Prexponential terms')

 call input_real ('Tact',Tact,'stop', &
       'Activation Temp')

 call input_real ('Damp',Damp,'stop', &
       'Disturbance amplitude')

  call input_real ('xL',xL,'stop',' xL: left boundary for heat addition')
  call input_real ('xR',xR,'stop',' xR: right boundary for heat addition')
  call input_real ('delta',delta,'stop',' delta: width of initial shock')
  call input_real ('radius',radius,'stop',' radius: Where initial discontinuity lies')

  END SUBROUTINE user_read_input

  SUBROUTINE user_additional_vars( t_local, flag )
    IMPLICIT NONE
    REAL (pr), INTENT (IN) :: t_local
    INTEGER, INTENT (IN) :: flag ! 0 during initial adaptation, then 1 during time advancement

    u(:,n_var_temp) = (gamma - 1.0_pr)*(u(:,4)/u(:,1) - 0.5_pr*(u(:,3)**2 + u(:,2)**2)/u(:,1)**2) ! Temp

    u(:,n_var_pressure) = u(:,1)*u(:,n_var_temp) ! Pressure

    u(:,n_var_fuel) = u(:,n_var_fuelD) / u(:,1) ! Fuel Fraction

    u(:,n_var_reaction) = Prexp*u(:,n_var_fuelD)*EXP(-Tact/u(:,n_var_temp))  ! Reaction Rate

  END SUBROUTINE user_additional_vars

  SUBROUTINE user_scalar_vars( flag )
    IMPLICIT NONE
    INTEGER , INTENT(IN) :: flag ! 0- called during adaption to IC, 1 called during main integration loop

  END SUBROUTINE user_scalar_vars

  !
  !************ Calculating Scales ***************************
  !
  ! Note the order of the components in the scl array
  ! correspond to u_tn, v_tn, w_tn, u_tn-1, v_tn-1, w_tn-1, u_tn-2, v_tn-2, w_tn-2
  ! 
  SUBROUTINE user_scales(flag, use_default, u, nlocal, ne_local, l_n_var_adapt , l_n_var_adapt_index, &
       scl, scl_fltwt) !add
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: flag ! 0 during initial adaptation, then 1 during time advancement
    LOGICAL , INTENT(INOUT) :: use_default
    INTEGER, INTENT (IN) :: nlocal, ne_local
    REAL (pr), DIMENSION (1:nlocal,1:ne_local), INTENT (IN) :: u
    LOGICAL , INTENT (IN) :: l_n_var_adapt(ne_local)
    INTEGER , INTENT (IN) :: l_n_var_adapt_index(1:ne_local)
    REAL (pr), DIMENSION (1:ne_local), INTENT (INOUT) :: scl
    REAL (pr) ,  INTENT(IN) ::scl_fltwt !weight for temporal filter on scl

    !
    ! Ignore the output of this routine and use default scales routine
    !
    use_default = .TRUE. 

    !
    ! NOTE: For a parallel run, synchronize scl(:) across the processors in user_scales.
    !       Use the subroutine scales of default_util.f90 as an example,
    !       subroutine parallel_global_sum of parallel.f90 may be helpful.
    !       Already synchronized: sumdA_global
  END SUBROUTINE user_scales


  SUBROUTINE user_cal_cfl (use_default, u, cfl_out)
    USE precision
    USE sizes
    USE pde
    IMPLICIT NONE
    LOGICAL , INTENT(INOUT) :: use_default
    REAL (pr),                                INTENT (INOUT) :: cfl_out
    REAL (pr), DIMENSION (nwlt,n_integrated), INTENT (IN)    :: u

    INTEGER                    :: i
    REAL (pr)                  :: floor
    REAL (pr)                  :: cfl
    REAL (pr), DIMENSION(nwlt) :: c
    REAL (pr), DIMENSION(dim,nwlt) :: h_arr
    REAL (pr) :: cSum

    use_default = .FALSE.

    floor = 1e-12_pr
    cfl_out = floor

!!$    c = SQRT(gamma*(gamma-1.0_pr)*( u(:,n_var_eng) - & 
!!$           0.5_pr*(u(:,n_var_vel(1))**2 + u(:,n_var_vel(2))**2))/u(:,1))
!!$    c = SQRT( gamma/u(:,n_var_den)*(gamma-1.0_pr)* &
!!$         ( u(:,n_var_eng)-0.5_pr*SUM(u(:,n_var_mom(1:dim))**2,DIM=2)/u(:,1)) ) ! pressure
!!$
!!$    p = (gamma-1.0_pr)*(u(:,n_var_eng)-0.5_pr*SUM(u(:,n_var_mom(1:dim))**2,DIM=2)/u(:,1))
    c = SQRT( gamma/u(:,n_var_den)*(gamma-1.0_pr)*(u(:,n_var_eng)-0.5_pr*SUM(u(:,n_var_mom(1:dim))**2,DIM=2)/u(:,1)) )

    CALL get_all_local_h (h_arr)

    ! Positivity preserving cfl calculation
    cSum = 0.0_pr
    DO i=1,dim
       cSum = cSum + MAXVAL( (ABS(u(:,n_var_mom(i))/u(:,n_var_den)) + c) *dt/h_arr(i,:) )
    END DO
    cfl_out = MAX (cfl_out, cSum)

!!$    ! Regular cfl calculation
!!$    cfl = MAXVAL(ABS(u(:,n_var_vel(1))/u(:,1) + c(:))/h_arr(1,:) +  &
!!$                 ABS(u(:,n_var_vel(2))/u(:,1) + c(:))/h_arr(2,:))*dt 
!!$    cfl_out = MAX(cfl_out, cfl)

    CALL parallel_global_sum( REALMAXVAL=cfl_out )

  END SUBROUTINE user_cal_cfl


  !******************************************************************************************
  !************************************* SGS MODEL ROUTINES *********************************
  !******************************************************************************************
  
  !
  ! Intialize sgs model
  ! This routine is called once in the first
  ! iteration of the main time integration loop.
  ! weights and model filters have been setup for first loop when this routine is called.
  !
  SUBROUTINE user_init_sgs_model( )
    IMPLICIT NONE


    ! LDM: Giuliano

    ! THE sgs forcign is stored for the time step in sgs_mdl_force(1:nlocal,1:n_integrated)
    ! where nlocal should be nwlt.


    !          print *,'initializing LDM ...'       
    !          CALL sgs_mdl_force( u(:,1:n_integrated), nwlt, j_lev, .TRUE.)


  END SUBROUTINE user_init_sgs_model

  !
  ! calculate sgs model forcing term
  ! user_sgs_force is called int he beginning of each times step in time_adv_cn().
  ! THE sgs forcign is stored for the time step in sgs_mdl_force(1:nlocal,1:n_integrated)
  ! where nlocal should be nwlt.
  ! 
  ! Accesses u from field module, 
  !          j_lev from wlt_vars module,
  !
  SUBROUTINE  user_sgs_force (u_loc, nlocal)
    IMPLICIT NONE

    INTEGER,                         INTENT (IN) :: nlocal
    REAL (pr), DIMENSION (nlocal,n_integrated), INTENT (INOUT) :: u_loc

  END SUBROUTINE  user_sgs_force

  FUNCTION user_sound_speed (u, neq, nwlt)
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: nwlt, neq
    REAL (pr), DIMENSION (nwlt,neq), INTENT(IN) :: u
    REAL (pr), DIMENSION (nwlt) :: user_sound_speed

    user_sound_speed(:) =  SQRT(gamma*(gamma-1.0_pr)*( u(:,n_var_eng) - & 
           0.5_pr*(u(:,n_var_vel(1))**2 + u(:,n_var_vel(2))**2))/u(:,1));

  END FUNCTION user_sound_speed

  SUBROUTINE  user_pre_process
    IMPLICIT NONE

  END SUBROUTINE user_pre_process

  SUBROUTINE  user_post_process
    IMPLICIT NONE

  END SUBROUTINE user_post_process

 END MODULE user_case
