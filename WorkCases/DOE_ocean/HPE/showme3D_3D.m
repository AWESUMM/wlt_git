function showme3D_3D(file,iter, procnum, slice, var)
%close all
%close all hidden

%Info on types of plots, 
  cntnum=100;   %sets number of contours
  vectoryn=1;   %1-yes, otherwise no, used to plot vectors over first velocity plot
  space=1;      %used to space out vectors
  fileformat = '.fig';
  setsurfscale = 1;   %0-want to set scale for surface/line plots, 1-default
  surfscale = 10e-4;   %set scale for surface/line plots
  savefile = 0;  %1-if want figure saved to file, 0-don't save to file
  dt_write = 0.01;   %used for plotting analytic solution
  analyticsoln=0;      %1-want to plot analytic solution of eta
  colorabaropt = 0;     %if 1, colorbar is on
  j_max_vector = 3;      %j_max for velocity vector plot

%To get path

local_dir = pwd
if isunix
    pl = '/'
else
    pl = '\';
end
global POSTPROCESS_DIR
POSTPROCESS_DIR = [pwd pl '..' pl '..' pl '..' pl 'post_process']

path(path,POSTPROCESS_DIR)
path(path,[POSTPROCESS_DIR pl 'sliceomatic'])
clear pl
cd(local_dir)

%Parameters for inter3d
global mrkcl mrksz
mrkcl = [0,0,0];
mrksz = 1;
eps=0.0;
bounds=[0];
j_range= [0 5];
station_num= iter;
x0=[0 0 0 2];
n0=[[0 1 1]' [0 1 -1]'];

  [u_var,x,y,z,nx,ny,nz,dim,time] ...
      =  inter3d(file,eps,bounds,var,j_range,station_num, procnum);
  

  contourf(x, y, u_var(:, :, slice), 5)
  axis equal
  axis tight
  set(gca,'XTick',0) 
  set(gca,'XTickLabel',{})
  set(gca,'YTick',0)
  set(gca,'YTickLabel',{})
  colormap cool



end
