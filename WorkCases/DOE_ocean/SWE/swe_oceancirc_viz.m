function swe_oceancirc_viz(file, iter, procnum)
close all
%To get path

local_dir = pwd
if isunix
    pl = '/'
else
    pl = '\';
end
global POSTPROCESS_DIR
POSTPROCESS_DIR = [pwd pl '..' pl '..' pl '..' pl 'post_process']

path(path,POSTPROCESS_DIR)
path(path,[POSTPROCESS_DIR pl 'sliceomatic'])
clear pl
cd(local_dir)

%Parameters for inter3d
global mrkcl mrksz
mrkcl = [0,0,0];
mrksz = 1;
eps=0.0;
bounds=[0];
j_range= [0 12];
j_max_vector = 8;
station_num= iter;
plot_type = 'surf';
x0=[0 0 0 2];
n0=[[0 1 1]' [0 1 -1]'];
procnum=procnum;
j_min_forgrid = 0;
j_max_forgrid = 10;
colorforgrid = [0, 0, 0];
mrkszforgrid = 2;

%close all
figure

  [u,x_v,y_v,z_v,nx,ny,nz,dim,time] ...
      =  inter3d(file,eps,bounds,'Ux',[0, j_max_vector],station_num, procnum);

  [v,x_v,y_v,z_v,nx,ny,nz,dim,time] ...
      =  inter3d(file,eps,bounds,'Uy',[0, j_max_vector],station_num, procnum);
  
  [eta,x_v,y_v,z_v,nx,ny,nz,dim,time] ...
       =  inter3d(file,eps,bounds,'eta',[0, j_max_vector],station_num, procnum);
   
  [X,Y]=meshgrid(x_v, y_v);
  
subplot(1, 2, 1)
contourf(x_v, y_v, eta, 5)
colormap cool
axis equal
axis tight
axis off

subplot(1, 2, 2)


[xstm,ystm,zstm,stm_indx,dim,nz,xmin, xmax, ymin, ymax, zmin, zmax, time] ...
    = res2matlab(file,eps,bounds,'Ux',j_min_forgrid,j_max_forgrid,station_num,1,x0,n0, procnum);
 
plot(xstm,ystm,'o','MarkerEdgeColor',colorforgrid,'MarkerFaceColor',colorforgrid,'Markersize',mrkszforgrid);
axis equal
set(gca,'xlim',[xmin,xmax],'ylim',[ymin,ymax]);
 
title('Adaptive Grid', 'fontsize', 14)
xlabel('x', 'fontsize', 14)
ylabel('y', 'fontsize', 14)

savefile=0;
if savefile==1
  filename=['test_images/sw_sq_sg.', num2str(iter, '%04.0f'), '.fig']
  saveas(gcf, filename)
end

  
 
