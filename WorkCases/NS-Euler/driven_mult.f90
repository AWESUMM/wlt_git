!$
!!$!****************************************************************************
!!$!* Module for adding Brinkman Penalization terms onto the full NS equations *
!!$!*  -Only valid for single species 10/7/2011                                *
!!$!*  -R is scaled to 1.0
!!$!****************************************************************************
!!$
!!$

MODULE user_case_vars
  USE precision
  ! REAL (pr) :: Re, Rebnd, Pra, Sc, Fr, Ma, At
  INTEGER :: Nspec, Nspecm
  INTEGER :: nden, neng, nprs,ndrho!, ndpx, nboy, nbRe, ndom, nton, BCver,ICver,nmsk,ncvtU
  INTEGER, ALLOCATABLE :: nvel(:)!, nspc(:), nvon(:), nson(:) 
  LOGICAL :: no_slip ! if NS=.TRUE. - Navier-Stoke, else Euler
  LOGICAL :: conv_drho ! input file parameter, extrapolate drho
  REAL (pr), ALLOCATABLE, DIMENSION (:) ::  mu_in, kk_in, cp_in, MW_in, gamm  

  REAL (pr) :: Ma
  ! INTEGER :: nden, neng, nprs, ndpx, nboy, nbRe, ndom, nton, BCver,ICver,nmsk,ncvtU
  ! INTEGER, ALLOCATABLE :: nvel(:), nspc(:), nvon(:), nson(:), nUn(:)
  ! REAL (pr) :: rhoL, pL, EL, rhoR, pRR, ER
  ! REAL (pr), DIMENSION(:), ALLOCATABLE :: uL, uR
  ! Spheres


END MODULE user_case_vars


MODULE Brinkman

  USE precision
  USE wlt_vars
  USE wavelet_filters_mod
  USE elliptic_mod
  USE elliptic_vars
  USE wlt_trns_mod
  USE wlt_trns_vars
  USE wlt_trns_util_mod
  USE io_3d_vars
  USE util_mod
  USE util_vars
  USE share_consts
  USE pde
  USE sizes
  USE share_kry
  USE vector_util_mod
  USE field
  USE input_file_reader
  USE additional_nodes
  USE penalization
  USE user_case_vars
  USE hyperbolic_solver

  !LOGICAL :: brinkman_penal, vp  !penalization type flags
  LOGICAL :: vp  !penalization type flags
  REAL (pr) :: cbvp_A, cbvp_B

  ! Obstacle
  LOGICAL :: doInvariant
  LOGICAL :: saveDIST
  INTEGER :: obs_type
  REAL (pr) :: xw, wa ! Wedge
  REAL (pr), DIMENSION(:), ALLOCATABLE :: xsp ! Sphere cener
  REAL (pr) :: rsp ! Sphere
  REAL (pr) :: rhosp ! obstacle density
  REAL (pr) :: msp ! Obstacle mass, calculated
  ! Spheres
  REAL (pr), DIMENSION(:), ALLOCATABLE :: xsps, rsps, vsps, rhosps, msps, inertias !Ryan - added inertias for moment of inertia of the spheres
  REAL (pr), DIMENSION(:), ALLOCATABLE :: wsps, psps   ! Ryan: angular velocity and position of particles
  INTEGER :: Nsp

  REAL (pr) :: visc_factor_d,visc_factor_n  !parameters for viscous zone around penalized obstacle
  INTEGER :: delta_pts, IB_pts

  LOGICAL :: meth_vect_diff

  REAL (pr) :: xwall
  REAL (pr) :: deltadel_loc
  LOGICAL :: smooth_dist
  LOGICAL :: doCURV
  REAL (pr) :: doNORM

  REAL (pr) :: eta_b, eta_c
  REAL (pr) :: porosity

  REAL (pr), DIMENSION(:), ALLOCATABLE  :: accel, vel, pos ! Obstacle dynamic params and position
  REAL (pr), DIMENSION(:,:), ALLOCATABLE  :: accels, vels, poss ! Many spheres
  REAL (pr), DIMENSION(:,:), ALLOCATABLE :: ang_vels, ang_poss

  ! Ryan
  ! Variables added for Lagrangian Module
  LOGICAL :: lagrangian_initialized = .FALSE.
  REAL (pr) :: particle_epsN, particle_epsT


CONTAINS

  SUBROUTINE user_dist (nlocal, t_local, DISTANCE, NORMAL, U_TARGET, COLOR)
    USE parallel
    USE lagrangian ! for omega x r function
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: nlocal
    REAL (pr), INTENT (IN) :: t_local
    REAL (pr), DIMENSION(nlocal), OPTIONAL, INTENT(INOUT) :: DISTANCE
    REAL (pr), DIMENSION(nlocal) :: TMPDIST
    REAL (pr), DIMENSION(nlocal, dim), OPTIONAL, INTENT(INOUT) :: NORMAL,U_TARGET
    REAL (pr), DIMENSION(nlocal, dim) :: d2u
    INTEGER, DIMENSION(nlocal), OPTIONAL, INTENT(INOUT) :: COLOR
    INTEGER :: idim, isp, jsp,i

    INTEGER :: i_prd(0:dim), n_prd, iprd, ixyz_prd(1:dim)
    !PRINT *, "Distance:", t_local

    REAL(pr), DIMENSION(dim) :: r_vec  ! Ryan - used for calculating u_target when angular velocity is present

    i_prd(0) = 1
    DO i=1,dim
       i_prd(i) = i_prd(i-1)*(1+2*prd(i))
    END DO
    n_prd = i_prd(dim)

    IF(PRESENT(DISTANCE) ) THEN
       IF (obs_type == 1) THEN
          IF (.NOT. stationary_obstacle .AND. t > 0.0_pr) THEN
             xsp = pos
          END IF
          DISTANCE(:) = 0.0_pr
          DO idim = 1, dim
             DISTANCE(:) = DISTANCE(:) + (x(:, idim) - xsp(idim))**2
          END DO
          DISTANCE(:) = SQRT(DISTANCE(:)) ! Not a real distance function yet.

          !Definition of NORMAL in the obstacle

          IF(PRESENT(NORMAL)) THEN
             DO idim = 1, dim
                NORMAL(:,idim) = (x(:,idim) - xsp(idim))/(- DISTANCE(:) + 1.0E-10_pr) !inward pointing normal
             END DO
          END IF

          DISTANCE(:) = rsp - DISTANCE(:) ! Now it is real signed distance function

          IF(smooth_dist) THEN
             CALL diffsmooth (DISTANCE,nlocal,1,HIGH_ORDER,11,0,0.5_pr,-MINVAL(h(1,1:dim))/2.0_pr**(REAL(j_mx,pr)-1.0_pr),deltadel_loc,-2,j_lev,3,.FALSE.)
          END IF
       ELSE IF (obs_type == 2) THEN
          IF (.NOT. stationary_obstacle .AND. t > 0.0_pr) THEN
             DO isp = 1, Nsp
                xsps(3*(isp - 1) + 1:3*(isp - 1) + dim) = poss(:, isp)
             END DO
!!$             PRINT *, SHAPE(xsps(1:dim))
!!$             PRINT *, SHAPE(poss)
          END IF
          DISTANCE(:) = -1.0E+15_pr
          IF (PRESENT(COLOR)) color(1:nlocal) = 0
          DO iprd = 0,n_prd-1
             ixyz_prd =  (INT(MOD(iprd,i_prd(1:dim))/i_prd(0:dim-1))-1)*prd(1:dim)
             DO isp = 1, Nsp
                TMPDIST(:) = 0.0_pr
                DO idim = 1, dim
                   jsp = idim + (isp - 1)*3
                   TMPDIST(:) = TMPDIST(:) + (x(:, idim) - xsps(jsp) - (xyzlimits(2,idim) - xyzlimits(1,idim))*REAL(ixyz_prd(idim), pr))**2
                END DO
                TMPDIST(:) = rsps(isp) - SQRT(TMPDIST(:))
                DISTANCE(:) = MAX(DISTANCE(:),TMPDIST(:))
                IF (PRESENT(COLOR)) THEN
                   WHERE (TMPDIST >= 0.0_pr) COLOR = isp
                END IF
             END DO
          END DO
          IF(PRESENT(NORMAL)) THEN
             CALL c_diff_fast(DISTANCE, NORMAL, d2u, j_lev, nlocal, HIGH_ORDER, 10, 1, 1, 1)  !first derivative unneeded in this implementation
             TMPDIST(:) = SQRT(SUM(NORMAL(:,1:dim)**2,DIM=2))
             DO idim = 1, dim
                NORMAL(:,idim) = NORMAL(:,idim)/(TMPDIST(:) + 1.0E-10_pr)
             END DO
          END IF
       END IF
    END IF

    IF(PRESENT(U_TARGET)) THEN
       U_TARGET=0.0_pr
       IF (.NOT. stationary_obstacle) THEN
!!$          DO idim = 1, dim
!!$             U_TARGET(:, idim) = vel(idim)
!!$          END DO
          IF (PRESENT(COLOR)) THEN
             DO i = 1, nlocal
                IF (color(i) > 0) THEN
                   U_TARGET(i, :) = vels(:, color(i))
                   r_vec(:) = x(i,:)-poss(:,color(i))
                   ! The next line corrects r for periodic boundary conditions
                   r_vec(:) = r_vec(:) - prd(:)*nint(r_vec(:)/(xyzlimits(2,:)-xyzlimits(1,:)))*(xyzlimits(2,:)-xyzlimits(1,:))
                   IF (dim == 2) THEN
                      U_TARGET(i,:) = U_TARGET(i,:) + CROSSPRODUCTW( ang_vels(1,color(i)) , r_vec(:))
                   ELSE IF (dim == 3) THEN
                      U_TARGET(i,:) = U_TARGET(i,:) + CROSSPRODUCTW( ang_vels(:,color(i)) , r_vec(:))
                   END IF
!!$                   IF (dim == 2) THEN
!!$                      U_TARGET(i,:) = U_TARGET(i,:) + CROSSPRODUCTW( ang_vels(1,color(i)) , (x(i,:)-poss(:,color(i))) )
!!$                   ELSE IF (dim == 3) THEN
!!$                      U_TARGET(i,:) = U_TARGET(i,:) + CROSSPRODUCTW( ang_vels(:,color(i)) , (x(i,:)-poss(:,color(i))) )
!!$                   END IF
                END IF
             END DO
          END IF
       END IF
    END IF


  END SUBROUTINE user_dist


  SUBROUTINE Brinkman_rhs (user_rhs, u_integrated, homog, meth)
    USE user_case_vars
    IMPLICIT NONE
    REAL (pr), DIMENSION (ng*ne), INTENT(INOUT) :: user_rhs
    REAL (pr), DIMENSION (ng,ne), INTENT(IN) :: u_integrated !1D flat version of integrated variables without BC 
    INTEGER, INTENT(IN) :: meth
    LOGICAL, INTENT(IN) :: homog

    INTEGER :: ie, shift,i,j, idim
    REAL (pr), DIMENSION (ng,dim)     :: dp
    REAL (pr), DIMENSION (ng,dim)     :: Unorm, Utan
    REAL (pr), DIMENSION (ng,dim)     :: u_0     !for non-stationary obstacle
    REAL (pr), DIMENSION (ng,dim)     :: brink_var     !for non-stationary obstacle
    REAL (pr), DIMENSION (dim,ng,dim) :: d2u_brink
    REAL (pr) :: cbvp_t


    REAL (pr), DIMENSION (ng,ne+dim) :: u_diff
    REAL (pr) :: nu_n    !artificial/physical viscosity with continuous fluxes
    REAL (pr), DIMENSION (ne+dim,ng,dim) :: du_diff, du_diffFD, du_diffBD, d2u_diff
    REAL (pr), DIMENSION (ne+dim,ng) :: dudn
    INTEGER :: n_deriv
    REAL (pr), DIMENSION (ng) :: Spenal,Spenal2,Spenal3
    REAL (pr) :: slope, offset
    REAL (pr) :: delta_Jmx
    INTEGER :: meth_central, meth_backward, meth_forward  
    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc

    REAL (pr), DIMENSION(ng) :: curv                 ! curvature
    REAL (pr), DIMENSION (ng) :: p                   ! pressure, for curvature adjustment if applicable
    REAL (pr), DIMENSION (dim, ng, dim) :: dn, d2n   ! derivative of normal

    REAL (pr), DIMENSION (ne+dim,ng) :: dudx

    REAL (pr), DIMENSION(ng, dim) :: vel_tan, vel_norm, vel_void
    REAL (pr), DIMENSION (ng)     :: A, C, F
    REAL (pr), DIMENSION(ng, dim) :: B

    REAL (pr), DIMENSION(ng, dim) :: v_back, v_for, vTemp


    REAL (pr), DIMENSION(2*ng,ne+2*dim) ::uh_vector
    REAL (pr), DIMENSION(ne+2*dim,2*ng,dim) ::du_vector, d2u_vector
    INTEGER, DIMENSION(2) :: meth_vector

    INTEGER, DIMENSION(ng) :: icolor

    !PRINT *, "rhs, t:", t
    IF(t.LE.1.0_pr) THEN
       cbvp_t = cbvp_B*t/1.0_pr
    ELSE
       cbvp_t = cbvp_B
    END IF

    !order is set in calling function
    meth_central  = meth + BIASING_NONE     !to allow for constant flux testing
    meth_backward = meth + BIASING_BACKWARD
    meth_forward  = meth + BIASING_FORWARD

!!$    IF (use_dist) THEN   !ERIC: what is this flag for
!!$       CALL user_dist (ng, t+dt, NORMAL=norm, U_TARGET=u_0, COLOR=icolor)
       Spenal = dist
       u_0 = u_target
       
!!$    ELSE
!!$    CALL user_dist (ng, t+dt, DISTANCE=Spenal, NORMAL=norm, U_TARGET=u_0, COLOR=icolor)
!!$    PRINT *, "U_TARGET", MINVAL(u_0), MAXVAL(u_0)
!!$    PRINT *, "color", MINVAL(icolor), MAXVAL(icolor)
!!$    END IF
    !PRINT *, MINVAL(u_0), MAXVAL(u_0)
    Spenal2= Spenal
    Spenal3= Spenal
    !MASK within penal to add diffusion and sink
    delta_Jmx = MINVAL(h(1,1:dim)/2.0_pr**(REAL(j_mx,pr) - 1.0_pr))

    slope = (ATANH( (0.5_pr-10.0_pr**-3.0_pr)/0.5_pr)- ATANH( (-0.5_pr+10.0_pr**-3.0_pr)/0.5_pr))/(REAL(delta_pts,pr)*delta_Jmx)
    offset = REAL(IB_pts,pr)*delta_Jmx - 1.0_pr/slope*ATANH( (-0.5_pr+10.0_pr**-3.0_pr)/0.5_pr )
    Spenal = MAX(0.5_pr - 10.0_pr**-3.0_pr - 0.5_pr*TANH(slope*(Spenal - offset- 0.0_pr*delta_Jmx)) , 0.0_pr )*penal
    !Spenal2 = MAX(0.5_pr - 10.0_pr**-3.0_pr + 0.5_pr*TANH(slope*(Spenal2 - offset- 1.0_pr*delta_Jmx)) , 0.0_pr )*penal  !core
    Spenal2 = MAX(0.5_pr - 10.0_pr**-3.0_pr + 0.5_pr*TANH(slope*(Spenal2 - offset- 0.0_pr*delta_Jmx)) , 0.0_pr )*penal
    !Spenal2 = penal
    Spenal3 = 0.0_pr
    !   Spenal3 = (MAX(0.5_pr - 10.0_pr**-3.0_pr - 0.5_pr*TANH(slope*(Spenal3 - offset- 3.0_pr*delta_Jmx)) , 0.0_pr )  - Spenal)  *penal  !low order convection before transition


    !Set slip condition for Euler and no-slip for NS

    IF (no_slip) THEN  !only normal is needed for euler, and all velocity components are penalized as "normal" for NS
       DO idim = 1,dim
          Utan(:, idim) = 0.0_pr
          Unorm (:,idim) = u_integrated(:, nvel(idim))
       END DO
       n_deriv = ne  !only take derivatives of the integrated variables
    ELSE     !ERIC: double check this section for Euler
       Utan(:, 1) = 0.0_pr
       DO idim = 1, dim
          Utan (:, 1) = Utan(:,1)+ u_integrated(:, nvel(idim))*norm(:,idim)
       END DO
       DO idim = 1, dim
          Unorm (:, idim) = Utan(:,1)*norm(:,idim)
       END DO
       DO idim = 1,dim
          Utan(:,idim)=u_integrated(:, nvel(idim))-Unorm(:,idim)
       END DO
       n_deriv = ne+dim  !take derivatives of integrated variables, normal momentum, and tangential momentum
    END IF

    !Set up for vector derivatives
    u_diff(:,1:ne) = u_integrated
    u_diff(:,nvel(1):nvel(dim)) = Unorm(:, 1:dim)   !Only "normal" derivatives needed
    u_diff(:,ne+1:ne+dim) = Utan(:, 1:dim)

    IF(conv_drho) THEN
       CALL c_diff_fast(u_integrated(:,nden), du_diff(1, :, :), d2u_diff(1, :, :), j_lev, ng, LOW_ORDER, 10, 1, 1, 1)  !density derivative for convecting
       u_diff(:,ndrho) = SUM(norm(:,:)*du_diff(1,:,:),DIM=2)*(1.0_pr-penal)+u_integrated(:,ndrho)*penal  !normal density derivative
    END IF

    DO i =1,dim
       brink_var(:,i) = Unorm(:,i)/u_integrated(:,nden)   !Dirichlet penalized variables for Laplacian region
    END DO

    CALL c_diff_fast(brink_var(:,1:dim), du_diff(1:dim, :, :), d2u_brink(1:dim, :, :), j_lev, ng, meth_central, 01, dim, 1, dim)  !first derivative unneeded in this implementation
    IF(.NOT. meth_vect_diff ) THEN
       CALL c_diff_fast(u_diff(:,1:n_deriv), du_diffBD(1:n_deriv, :, :), d2u_diff(1:n_deriv, :, :), j_lev, ng, meth_backward, 10, n_deriv, 1, n_deriv)
       CALL c_diff_fast(u_diff(:,1:n_deriv), du_diffFD(1:n_deriv, :, :), d2u_diff(1:n_deriv, :, :), j_lev, ng, meth_forward, 10, n_deriv, 1, n_deriv)
    ELSE
       meth_vector(1) = meth_backward
       meth_vector(2) = meth_forward
       uh_vector(1:ng,1:n_deriv) = u_diff(1:ng,1:n_deriv)
       uh_vector(ng+1:2*ng,1:n_deriv) = u_diff(1:ng,1:n_deriv)
       CALL c_diff_fast_vector(uh_vector(:,1:n_deriv), du_vector(1:n_deriv, :, :), d2u_vector(1:n_deriv, :, :), j_lev, 2*ng, 2 ,meth_vector, 10, n_deriv, 1, n_deriv)
       du_diffBD(1:n_deriv,:,:) = du_vector(1:n_deriv,1:ng,:)
       du_diffFD(1:n_deriv,:,:) = du_vector(1:n_deriv,1+ng:2*ng,:)
    END IF
    !For Navier-Stokes, zero out tangential velocity derivatives
    IF(no_slip) THEN
       du_diffBD((ne+1):(ne+dim),:,:) = 0.0_pr
       du_diffFD((ne+1):(ne+dim),:,:) = 0.0_pr
    END IF

    !Set normal upwind+derivatives
    DO j = 1,ne+dim
       dudn(j,:) = 0.5_pr*SUM( du_diffBD(j, :, :)*(norm(:,:) + ABS(norm(:,:))) + du_diffFD(j, :, :)*(norm(:,:) - ABS(norm(:,:))) ,DIM=2)
    END DO

    DO j = 1,ne+dim
       dudx(j,:) = 0.5_pr*SUM(du_diffBD(j,:,:)*(u_0(:,:) + ABS(u_0(:,:))) + du_diffFD(j,:,:)*(u_0(:,:) - ABS(u_0(:,:))), DIM=2) ! u d/dx
    END DO

    !set nu_n
    nu_n = visc_factor_d**2.0_pr *delta_Jmx**2.0_pr/eta_b
    !nu_n = visc_factor_d *delta_Jmx


    ! RHS to zero inside of Brinkman zone
    DO ie = 1,ne
       user_rhs((ie-1)*ng+1:ie*ng) = (1.0_pr-penal)*user_rhs((ie-1)*ng+1:ie*ng) 
    END DO

    IF (doInvariant) THEN
       ! ------------Add on invariant terms--------------
       ! Density
       user_rhs((nden-1)*ng+1:nden*ng) = user_rhs((nden-1)*ng+1:nden*ng) &   ! cont eq penalization
            - penal * dudx(nden,:)
       ! Momentum
       DO ie = 1,dim 
          user_rhs((nvel(ie)-1)*ng+1:nvel(ie)*ng) = user_rhs((nvel(ie)-1)*ng+1:nvel(ie)*ng) &
               - penal *  (dudx(nvel(ie),:) + dudx(ne+ie, :))
       END DO
       ! Energy
       user_rhs((neng-1)*ng+1:neng*ng) = user_rhs((neng-1)*ng+1:neng*ng) &
            - penal * dudx(neng,:)  !convect energy (for Neumann/Robin temperature condition)
       ! -----------------------------------------------
    END IF
    
    IF(conv_drho) THEN 
       !Convect normal derivative
       user_rhs((ndrho-1)*ng+1:ndrho*ng) = user_rhs((ndrho-1)*ng+1:ndrho*ng) &   ! cont eq penalization
            - (Spenal)* dudn(ndrho,:)/eta_c &!use low order
            - (Spenal3)*dudn(ndrho,:)/eta_c !use low order
       user_rhs((nden-1)*ng+1:nden*ng) = user_rhs((nden-1)*ng+1:nden*ng) &   ! cont eq penalization
            - Spenal*(-u_integrated(:,ndrho))/eta_c -Spenal3*(-u_integrated(:,ndrho))/eta_c   ! cont eq penalization for density matching, across entire convective zone (high and low order)
       DO ie = 1,dim 
          user_rhs((nvel(ie)-1)*ng+1:nvel(ie)*ng) = user_rhs((nvel(ie)-1)*ng+1:nvel(ie)*ng) &  !density evolution condition
                                !- (Spenal+Spenal3)*(-Unorm(:,ie)/ u_integrated(:,nden)*u_integrated(:,ndrho))/eta_c     !density target   !old
               - (Spenal+Spenal3)*(-u_integrated(:,nvel(ie))/ u_integrated(:,nden)*u_integrated(:,ndrho))/eta_c     !density target      !ERIC: to make it general for Euler
       END DO
       user_rhs((neng-1)*ng+1:neng*ng) = user_rhs((neng-1)*ng+1:neng*ng) &
            + Spenal *u_integrated(:,ndrho) * u_integrated(:,neng)/u_integrated(:,nden)/eta_c &  !remove evolutionary drho effects from Temperature slope
            + Spenal3*u_integrated(:,ndrho) * u_integrated(:,neng)/u_integrated(:,nden)/eta_c   !remove evolutionary drho effects from Temperature slope
    END IF

    !Continuity Equation
    user_rhs((nden-1)*ng+1:nden*ng) = user_rhs((nden-1)*ng+1:nden*ng) &   ! cont eq penalization
         - Spenal *(dudn(nden,:) )/eta_c &
         - Spenal3*(dudn(nden,:) )/eta_c 

    !Momentum Equation
    DO ie = 1,dim 
       user_rhs((nvel(ie)-1)*ng+1:nvel(ie)*ng) = user_rhs((nvel(ie)-1)*ng+1:nvel(ie)*ng) &
            - Spenal * dudn(ne+ie,:)/eta_c  &       !tangential: Euler, ERIC:need density term
            - Spenal3* dudn(ne+ie,:)/eta_c  &       !tangential: Euler
            + penal*nu_n*u_integrated(:,nden)*SUM(d2u_brink(ie, :, :),DIM=2)  !artificial viscosity in non-conservative form
       user_rhs((nvel(ie)-1)*ng+1:nvel(ie)*ng) = user_rhs((nvel(ie)-1)*ng+1:nvel(ie)*ng) &
            - penal*(Unorm(:,ie) - u_integrated(:,nden)*u_0(:,ie))/eta_b !&   !Normal & Navier-Stokes: Dirichlet
       user_rhs((nvel(ie)-1)*ng+1:nvel(ie)*ng) = user_rhs((nvel(ie)-1)*ng+1:nvel(ie)*ng) &  !density evolution condition
            - Spenal *(Unorm(:,ie)/ u_integrated(:,nden) * dudn(nden,:) )/eta_c &
            - Spenal3*(Unorm(:,ie)/ u_integrated(:,nden) * dudn(nden,:) )/eta_c 
    END DO


    !Energy Equation
    DO ie = 1,dim
       user_rhs((neng-1)*ng+1:neng*ng) = user_rhs((neng-1)*ng+1:neng*ng) &
            + Spenal *(   brink_var(:, ie)*( dudn(nvel(ie),:) - dudn(nden,:)*brink_var(:, ie) )         )/eta_c   &            !do not convect no-slip
            + Spenal3*( brink_var(:, ie)*(dudn(nvel(ie),:) - dudn(nden,:)*brink_var(:, ie) ))/eta_c   &            !do not convect no-slip
            + penal*nu_n*Unorm(:, ie)*SUM(d2u_brink(ie, :, :),DIM=2)  &                               !artificial viscosity in non-conservative form
            - penal*(Unorm(:, ie)*(Unorm(:, ie)/u_integrated(:,nden) - u_0(:,ie)))/eta_b ! &                   ! Dirichlet velocity BC term
       !+ Spenal * 0.5_pr*   Unorm(:,ie)**2.0_pr/u_integrated(:,nden)**2.0_pr *u_integrated(:, ndrho)  /eta_c &   !These terms part of specific total energy added later
       !+ Spenal3* 0.5_pr*   Unorm(:,ie)**2.0_pr/u_integrated(:,nden)**2.0_pr *u_integrated(:, ndrho)  /eta_c
    END DO
    !user_rhs((neng-1)*ng+1:neng*ng) = (1.0_pr-penal)*user_rhs((neng-1)*ng+1:neng*ng) + doNORM * penal*user_rhs((neng-1)*ng+1:neng*ng) !ERIC: is this used?

    ! ------- NEED TO CONSISTENTLY UPDATE du to duFD and duBD--------------------
    user_rhs((neng-1)*ng+1:neng*ng) = user_rhs((neng-1)*ng+1:neng*ng) &
         - Spenal * dudn(neng,:)/eta_c &  !convect energy (for Neumann/Robin temperature condition)
         - Spenal3* dudn(neng,:)/eta_c   !convect energy (for Neumann/Robin temperature condition)

    !add on Neumann/Robin condition targets
    user_rhs((neng-1)*ng+1:neng*ng) = user_rhs((neng-1)*ng+1:neng*ng) &   ! cont eq penalization
         + Spenal  * u_integrated(:,nden)*(cp_in(Nspec)-1.0_pr/MW_in(Nspec)/Ma**2.0_pr)*cbvp_B/eta_c &
         + Spenal3 * u_integrated(:,nden)*(cp_in(Nspec)-1.0_pr/MW_in(Nspec)/Ma**2.0_pr)*cbvp_B/eta_c 

    user_rhs((neng-1)*ng+1:neng*ng) = user_rhs((neng-1)*ng+1:neng*ng) &   ! cont eq penalization
         + Spenal  * (u_integrated(:,neng) - 0.5_pr*SUM(Unorm(:,1:dim)**2.0_pr,DIM=2)/u_integrated(:,nden) )*cbvp_A/eta_c &
         + Spenal3 * (u_integrated(:,neng) - 0.5_pr*SUM(Unorm(:,1:dim)**2.0_pr,DIM=2)/u_integrated(:,nden) )*cbvp_A/eta_c

    !Use Conservative form

    DO i=1,dim
       vTemp(1:nwlt,i) = Spenal2
    END DO

    CALL shift_by(vTemp, v_for, nwlt, 1)
    v_for(1:nwlt,1:dim) = MAX(vTemp, v_for)
    CALL shift_by(vTemp, v_back, nwlt, -1)
    v_back(1:nwlt,1:dim) = MAX(vTemp, v_back)


    !Diffusion

    DO ie = 1,ne
       IF( NOT(no_slip .AND. (ANY(nvel==ie)) .OR. ie==ndrho) ) THEN  !skip velocity components for NS
          !       IF( NOT(NS .AND. (ANY(nvel==ie))) ) THEN  !skip velocity components for NS
          shift = ng*(ie-1)
          !--Go through all Boundary points that are specified
          i_p_face(0) = 1
          DO i=1,dim
             i_p_face(i) = i_p_face(i-1)*3
          END DO
          DO face_type = 0, 3**dim - 1
             face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
             CALL get_all_indices_by_face (face_type, j_lev, nloc, iloc)
             IF(nloc > 0 ) THEN 
                DO i = 1, dim
                   !user_rhs(shift+iloc(1:nloc)) = user_rhs(shift+iloc(1:nloc)) + (1.0_pr-ABS(face(i)))*delta_Jmx* visc_factor_n *&
                   !                               Spenal2(iloc(1:nloc))*d2u(ie,iloc(1:nloc),i) ! No diffusion normal to the boundary

                   user_rhs(shift+iloc(1:nloc)) = user_rhs(shift+iloc(1:nloc)) + (1.0_pr-ABS( REAL(face(i),pr) ))*visc_factor_n&
                        *MAX(delta_Jmx/eta_c,1.0_pr)*(v_for(iloc(1:nloc),i)*du_diffFD(ie,iloc(1:nloc),i) - v_back(iloc(1:nloc),i)*du_diffBD(ie,iloc(1:nloc),i) )  
                END DO
             END IF
          END DO
       END IF
    END DO

    !----------------Add curvature related adjustment for penalization terms
    IF (doCURV) THEN
       ! Calculate additional parameters we'll need to simplify equations-----------------------------------
       DO i = 1, dim
          vel_tan(:, i) = Utan(:, i)/u_integrated(:, nden)                           ! Actual tangential velocity components
          vel_norm(:, i) = Unorm(:, i)/u_integrated(:, nden)                         ! Actual normal velocity components
          vel_void(:, i) = u_integrated(:, nvel(i))/u_integrated(:, nden)            ! Actual velocity components
       END DO

       p = (u_integrated(:,neng)-0.5_pr*SUM(u_integrated(:,nvel(1):nvel(dim))**2.0_pr,DIM=2)/u_integrated(:,nden))*(MW_in(Nspec)-1.0_pr) !pressure

       A = SUM(Utan**2.0_pr, DIM=2)/p                                                        ! A parameter, see pdf file for details
       DO i = 1, dim
          B(:, i) = A*vel_tan(:, i)                                                     ! B parameter, see pdf file for details
       END DO
       C = SUM(Utan*vel_tan, DIM=2)/(MW_in(Nspec) - 1.0_pr)                                    ! C parameter, see pdf file for details
       F = A*(SUM(vel_tan**2.0_pr + ( 1.0_pr - 2.0_pr*doNORM)*vel_norm**2.0_pr, DIM=2))/2.0_pr    ! F parameter, see pdf file for details
       !-----------------------------------------------------------------------------------------------------
       curv(:) = 0.0_pr
       CALL c_diff_fast(norm, dn, d2n, j_lev, ng, HIGH_ORDER, 10, dim, 1, dim)
       ! Calculating the curvature. PROBABLY NEED TO CREATE SEPARATE SUBROUTINE FOR SIMPLICITY!!!!
       DO i = 1, dim
          curv(:) = curv(:) + dn(i, :, i) ! Calculate the curvature
       END DO
       curv = Spenal*curv/eta_c ! Common term to simplify the equations, S*k/eta
       ! Add curvature to continuity
       user_rhs((nden-1)*ng+1:nden*ng) = user_rhs((nden-1)*ng+1:nden*ng) + curv*A
       ! Add curvature to momentum equation
       DO ie = 1,dim 
          user_rhs((nvel(ie)-1)*ng+1:nvel(ie)*ng) = user_rhs((nvel(ie)-1)*ng+1:nvel(ie)*ng) + curv*B(:, ie)
       END DO
       ! Add curvature to energy equation
       user_rhs((neng-1)*ng+1:neng*ng) = user_rhs((neng-1)*ng+1:neng*ng) + curv*(C + F)
    END IF
    !-----------------------------------------------------------------------

    ! END IF



  END SUBROUTINE Brinkman_rhs


  ! find Jacobian of Right Hand Side of the problem
  SUBROUTINE  Brinkman_Drhs (user_Drhs, ulc, u_prev, meth)
    USE penalization
    USE user_case_vars
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: meth
    REAL (pr), DIMENSION (ng,ne), INTENT(IN) :: ulc, u_prev
    REAL (pr), DIMENSION (ng*ne), INTENT(INOUT) :: user_Drhs
    REAL (pr), DIMENSION (ng,dim)     :: Unorm, Unorm_prev, Utan, Utan_prev
    REAL (pr), DIMENSION (ng,2*dim)     :: brink_var     !for non-stationary obstacle
    REAL (pr), DIMENSION (dim*2,ng,dim) :: d2u_brink

    REAL (pr), DIMENSION (ne*2,ng,dim) :: du_pass
    REAL (pr), DIMENSION (2*(ne+dim),ng) :: dudn
    REAL (pr), DIMENSION (ng,dim)     :: u_0     !for non-stationary obstacle

    REAL (pr), DIMENSION (ng) :: Spenal,Spenal2,Spenal3
    REAL (pr) :: slope, offset

    REAL (pr), DIMENSION (ne+dim,ng) :: dudx

    REAL (pr) :: delta_Jmx, nu_n
    REAL (pr) :: cbvp_t

    INTEGER :: ie, shift,i,j, idim
    INTEGER :: meth_central, meth_backward, meth_forward  

    REAL (pr), DIMENSION (ng,2*(ne+dim)) :: u_diff
    REAL (pr), DIMENSION (2*(ne+dim),ng,dim) :: du_diff, du_diffFD, du_diffBD, d2u_diff
    INTEGER :: n_deriv

    REAL (pr), DIMENSION(ng) :: curv                 ! curvature
    REAL (pr), DIMENSION (dim, ng, dim) :: dn, d2n   ! derivative of normal

    REAL (pr), DIMENSION (ng) :: p, p_prev                   ! pressure, for curvature adjustment if applicable
    REAL (pr), DIMENSION(ng, dim) :: vel_tan, vel_norm, vel_void
    REAL (pr), DIMENSION(ng, dim) :: vel_tan_prev, vel_norm_prev, vel_void_prev
    REAL (pr), DIMENSION (ng)     :: A, C, F
    REAL (pr), DIMENSION (ng)     :: A_prev, C_prev, F_prev
    REAL (pr), DIMENSION(ng, dim) :: B, B_prev

    INTEGER :: prev_shift

    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc

    REAL (pr), DIMENSION(ng, dim) :: v_back, v_for, vTemp

    REAL (pr), DIMENSION(2*ng,2*(ne+2*dim)) ::uh_vector
    REAL (pr), DIMENSION(2*(ne+2*dim),2*ng,dim) ::du_vector, d2u_vector
    INTEGER, DIMENSION(2) :: meth_vector

    INTEGER, DIMENSION(ng) :: icolor

    IF(t.LE.1.0_pr) THEN
       cbvp_t = cbvp_B*t/1.0_pr
    ELSE
       cbvp_t = cbvp_B
    END IF

    meth_central  = meth + BIASING_NONE
    meth_backward = meth + BIASING_BACKWARD
    meth_forward  = meth + BIASING_FORWARD

    !PRINT *, "Drhs, t:", t

!!$    IF (use_dist) THEN
!!$       CALL user_dist (ng, t+dt, NORMAL=norm, U_TARGET=u_0, COLOR=icolor)
       Spenal = dist
       u_0 = u_target
!!$    ELSE
!!$    CALL user_dist (ng, t+dt, DISTANCE=Spenal, NORMAL=norm, U_TARGET=u_0, COLOR=icolor)
!!$    PRINT *, "U_TARGET, drhs", MINVAL(u_0), MAXVAL(u_0)
!!$    PRINT *, "color, drhs", MINVAL(icolor), MAXVAL(icolor)
!!$    END IF
    Spenal2= Spenal
    Spenal3= Spenal
    !MASK within penal to add diffusion and sink
    delta_Jmx = MINVAL(h(1,1:dim)/2.0_pr**(REAL(j_mx,pr) - 1.0_pr))

    slope = (ATANH( (0.5_pr-10.0_pr**-3.0_pr)/0.5_pr)- ATANH( (-0.5_pr+10.0_pr**-3.0_pr)/0.5_pr))/(REAL(delta_pts,pr)*delta_Jmx)
    offset = REAL(IB_pts,pr)*delta_Jmx - 1.0_pr/slope*ATANH( (-0.5_pr+10.0_pr**-3.0_pr)/0.5_pr )
    Spenal = MAX(0.5_pr - 10.0_pr**-3.0_pr - 0.5_pr*TANH(slope*(Spenal - offset- 0.0_pr*delta_Jmx)) , 0.0_pr )*penal
    !Spenal2 = MAX(0.5_pr - 10.0_pr**-3.0_pr + 0.5_pr*TANH(slope*(Spenal2 - offset- 1.0_pr*delta_Jmx)) , 0.0_pr )*penal  !core
    Spenal2 = MAX(0.5_pr - 10.0_pr**-3.0_pr + 0.5_pr*TANH(slope*(Spenal2 - offset- 0.0_pr*delta_Jmx)) , 0.0_pr )*penal
    !Spenal2 = penal
    Spenal3 = 0.0_pr
    !   Spenal3 = (MAX(0.5_pr - 10.0_pr**-3.0_pr - 0.5_pr*TANH(slope*(Spenal3 - offset- 3.0_pr*delta_Jmx)) , 0.0_pr )  - Spenal)  *penal  !low order convection before transition


    IF (no_slip) THEN
       DO idim =1,dim
          Utan(:, idim) = 0.0_pr
          Utan_prev(:, idim) = 0.0_pr
          Unorm (:,idim) = ulc(:, nvel(idim))  
          Unorm_prev(:, idim) = u_prev(:, nvel(idim))
       END DO
    ELSE  !ERIC: double check this for Euler Eqs
       Utan(:, 1) = 0.0_pr
       Utan_prev(:, 1) = 0.0_pr
       DO idim = 1, dim
          Utan(:, 1) = Utan(:,1)+ ulc(:, nvel(idim))*norm(:,idim)
          Utan_prev(:, 1) = Utan_prev(:,1)+ u_prev(:, nvel(idim))*norm(:,idim)
       END DO
       DO idim = 1, dim
          Unorm(:, idim) = Utan(:,1)*norm(:,idim)
          Unorm_prev(:, idim) = Utan_prev(:,1)*norm(:,idim)
       END DO
       DO idim = 1,dim
          Utan(:,idim) = ulc(:, nvel(idim))-Unorm(:,idim)
          Utan_prev(:,idim) = u_prev(:, nvel(idim)) - Unorm_prev(:,idim)
       END DO
    END IF
    n_deriv = (ne+dim)*2 ! taking all of the derivatives because all perterbations are listed before previous  (maintain shift)


    !Take all high order derivatives at the same time -------
    !get normal derivatives
    prev_shift = ne+dim
    !perturbations
    u_diff(:,1:ne) = ulc
    u_diff(:,nvel(1):nvel(dim)) = Unorm(:, 1:dim)
    u_diff(:,(ne+1):(ne+dim)) = Utan(:, 1:dim)
    !pervious
    u_diff(:,1+prev_shift:ne+prev_shift) = u_prev
    u_diff(:,nvel(1)+prev_shift:nvel(dim)+prev_shift) = Unorm_prev(:, 1:dim)
    u_diff(:,(ne+1)+prev_shift:(ne+dim)+prev_shift) = Utan_prev(:, 1:dim)

    brink_var(:,1) = ulc(:,nden)  !reuse variable
    brink_var(:,2) = u_prev(:,nden)  !reuse variable
    CALL c_diff_fast(brink_var(:,1:2), du_diff(1:2, :, :), d2u_diff(1:2, :, :), j_lev, ng, LOW_ORDER, 10, 2, 1, 2)  !density derivative for convecting
    !PRINT *, 'u_ndrho1:', MINVAL(u_diff(:,1)*penal), MAXVAL(u_diff(:,1)*penal)

    IF (conv_drho) THEN 
       u_diff(:,ndrho)            = SUM(norm(:,:)*du_diff(1,:,:),DIM=2)*(1.0_pr-penal)+   ulc(:,ndrho)*penal  !normal density derivative
       u_diff(:,ndrho+prev_shift) = SUM(norm(:,:)*du_diff(2,:,:),DIM=2)*(1.0_pr-penal)+u_prev(:,ndrho)*penal  !normal density derivative
    END IF

    DO i=1,dim
       brink_var(:,i)       = (Unorm(:,i)*u_prev(:,nden)-Unorm_prev(:,i)*ulc(:,nden))/u_prev(:,nden)**2.0_pr
       brink_var(:,i+dim)   = Unorm_prev(:,i)/u_prev(:,nden)
    END DO

    CALL    c_diff_fast(brink_var(:,1:2*dim), du_diff(1:2*dim, :, :), d2u_brink(1:2*dim, :, :), j_lev, ng, meth_central, 01, 2*dim, 1, 2*dim)
    IF(.NOT. meth_vect_diff ) THEN
       CALL c_diff_fast(u_diff(:,1:n_deriv), du_diffBD(1:n_deriv, :, :), d2u_diff(1:n_deriv, :, :), j_lev, ng, meth_backward, 10, n_deriv, 1, n_deriv)
       CALL c_diff_fast(u_diff(:,1:n_deriv), du_diffFD(1:n_deriv, :, :), d2u_diff(1:n_deriv, :, :), j_lev, ng, meth_forward,  10, n_deriv, 1, n_deriv)
    ELSE
       meth_vector(1) = meth_backward
       meth_vector(2) = meth_forward
       uh_vector(1:ng,1:n_deriv) = u_diff(1:ng,1:n_deriv)
       uh_vector(ng+1:2*ng,1:n_deriv) = u_diff(1:ng,1:n_deriv)
       CALL c_diff_fast_vector(uh_vector(:,1:n_deriv), du_vector(1:n_deriv, :, :), d2u_vector(1:n_deriv, :, :), j_lev, 2*ng, 2 ,meth_vector, 10, n_deriv, 1, n_deriv)
       du_diffBD(1:n_deriv,:,:) = du_vector(1:n_deriv,1:ng,:)
       du_diffFD(1:n_deriv,:,:) = du_vector(1:n_deriv,1+ng:2*ng,:)
    END IF
    IF(no_slip) THEN  !verify that tangential derivatives are zero
       du_diffBD((ne+1):(ne+dim),:,:) = 0.0_pr
       du_diffFD((ne+1):(ne+dim),:,:) = 0.0_pr
       du_diffBD((ne+1)+prev_shift:(ne+dim)+prev_shift,:,:) = 0.0_pr
       du_diffFD((ne+1)+prev_shift:(ne+dim)+prev_shift,:,:) = 0.0_pr
    END IF

    !Set normal derivatives upwind
    DO j = 1,n_deriv
       dudn(j,:) = 0.5_pr*SUM( du_diffBD(j, :, :)*(norm(:,:) + ABS(norm(:,:))) + du_diffFD(j, :, :)*(norm(:,:) - ABS(norm(:,:))) ,DIM=2)
    END DO

    DO j = 1,ne+dim
       dudx(j,:) = 0.5_pr*SUM( (u_0(:,:) + ABS(u_0(:,:)))*du_diffBD(j,:,:) + (u_0(:,:) - ABS(u_0(:,:)))*du_diffFD(j,:,:), DIM=2)
    END DO

    !PRINT *, 'dudn_ndrho:', MINVAL(dudn(ndrho,:)*penal), MAXVAL(dudn(ndrho,:)*penal)


    !set nu_n
    nu_n = visc_factor_d**2.0_pr *delta_Jmx**2.0_pr/eta_b
    !nu_n = visc_factor_d *delta_Jmx

    ! sets NN RHS to zero inside of Brinkman zone
    DO ie = 1,ne
       user_Drhs((ie-1)*ng+1:ie*ng) = (1.0_pr-penal)*user_Drhs((ie-1)*ng+1:ie*ng) 
    END DO

    IF (doInvariant) THEN
       ! ------------Add on Lagrange terms--------------
       ! Density
       user_Drhs((nden-1)*ng+1:nden*ng) = user_Drhs((nden-1)*ng+1:nden*ng) &   ! cont eq penalization
            - penal  * dudx(nden,:)
       ! Momentum
       DO ie = 1,dim 
          user_Drhs((nvel(ie)-1)*ng+1:nvel(ie)*ng) = user_Drhs((nvel(ie)-1)*ng+1:nvel(ie)*ng) &
               - penal *  (dudx(nvel(ie),:) + dudx(ne+ie,:))
       END DO
       ! Energy
       user_Drhs((neng-1)*ng+1:neng*ng) = user_Drhs((neng-1)*ng+1:neng*ng) &
            - penal * dudx(neng,:)
       ! -------------------------------------------------
    END IF


    IF(conv_drho) THEN 
       !Convect normal derivative
       user_Drhs((ndrho-1)*ng+1:ndrho*ng) = user_Drhs((ndrho-1)*ng+1:ndrho*ng) &   ! cont eq penalization
            - (Spenal)* dudn(ndrho,:)/eta_c &!use low order
            - (Spenal3)*dudn(ndrho,:)/eta_c !use low order
       user_Drhs((nden-1)*ng+1:nden*ng) = user_Drhs((nden-1)*ng+1:nden*ng)  &  ! cont eq penalization
            - Spenal *(-ulc(:,ndrho))/eta_c - Spenal3 *(-ulc(:,ndrho))/eta_c   ! cont eq penalization for density matching, across entire convective zone (high and low order)
       DO ie = 1,dim 
          user_Drhs((nvel(ie)-1)*ng+1:nvel(ie)*ng) = user_Drhs((nvel(ie)-1)*ng+1:nvel(ie)*ng) &  !density evolution condition
!!$                  - (Spenal+Spenal3)*(-brink_var(:,ie+dim) *ulc(:,ndrho)     )/eta_c   &   !density target  !old
!!$                  - (Spenal+Spenal3)*(-brink_var(:,ie)     *u_prev(:,ndrho))/eta_c       !density target
               - (Spenal+Spenal3)*(-u_prev(:,nvel(ie))/u_prev(:,nden) *ulc(:,ndrho)     )/eta_c   &   !density target    !ERIC: to make general for euler  Oct 2013
               - (Spenal+Spenal3)*(-( ulc(:,nvel(ie))/u_prev(:,nden) - ulc(:,nden)* u_prev(:,nvel(ie))/u_prev(:,nden)**2.0_pr      )     *u_prev(:,ndrho))/eta_c       !density target
       END DO
       user_Drhs((neng-1)*ng+1:neng*ng) = user_Drhs((neng-1)*ng+1:neng*ng) &
            + Spenal *ulc(:,ndrho)* u_prev(:,neng)/u_prev(:,nden)/eta_c &  !remove evolutionary drho effects from Temperature slope
            + Spenal *u_prev(:,ndrho) * (ulc(:,neng)*u_prev(:,nden)-u_prev(:,neng)*ulc(:,nden) )/u_prev(:,nden)**2.0_pr/eta_c & !remove evolutionary drho effects from Temperature slope
            + Spenal3*ulc(:,ndrho)* u_prev(:,neng)/u_prev(:,nden)/eta_c &  !remove evolutionary drho effects from Temperature slope
            + Spenal3*u_prev(:,ndrho) * (ulc(:,neng)*u_prev(:,nden)-u_prev(:,neng)*ulc(:,nden) )/u_prev(:,nden)**2.0_pr/eta_c  !remove evolutionary drho effects from Temperature slope
    END IF

    !Continuity Equation
    user_Drhs((nden-1)*ng+1:nden*ng) = user_Drhs((nden-1)*ng+1:nden*ng)  &  ! cont eq penalization
         - Spenal *  (dudn(nden,:))/eta_c &
         - Spenal3*  (dudn(nden,:))/eta_c 

    !Momentum Equation
    DO ie = 1,dim 
       user_Drhs((nvel(ie)-1)*ng+1:nvel(ie)*ng) = user_Drhs((nvel(ie)-1)*ng+1:nvel(ie)*ng) &
            - Spenal * dudn(ne+ie,:)/eta_c  & !tangential :Euler
            - Spenal3* dudn(ne+ie,:)/eta_c  & !tangential: Euler
            + penal*nu_n *ulc(:,nden)     *SUM(d2u_brink(dim+ie, :, :),DIM=2) & !artificial viscosity in non-conservative form
            + penal*nu_n *u_prev(:,nden)*SUM(d2u_brink(ie, :, :)    ,DIM=2)   !artificial viscosity in non-conservative form
       user_Drhs((nvel(ie)-1)*ng+1:nvel(ie)*ng) = user_Drhs((nvel(ie)-1)*ng+1:nvel(ie)*ng) &
            - penal*(Unorm(:,ie) - ulc(:,nden)*u_0(:,ie))/eta_b !&   !Normal velocity & Navier-Stokes
       user_Drhs((nvel(ie)-1)*ng+1:nvel(ie)*ng) = user_Drhs((nvel(ie)-1)*ng+1:nvel(ie)*ng) &  !density evolution condition
            - Spenal *( brink_var(:,ie+dim) * dudn(nden,:)            )/eta_c &
            - Spenal *( brink_var(:,ie)     * dudn(nden+prev_shift,:) )/eta_c &
            - Spenal3*( brink_var(:,ie+dim) * dudn(nden,:)            )/eta_c &
            - Spenal3*( brink_var(:,ie)     * dudn(nden+prev_shift,:) )/eta_c 
    END DO
    !PRINT *, 'ENG1:', MINVAL(user_Drhs((neng-1)*ng+1:neng*ng)*penal), MAXVAL(user_Drhs((neng-1)*ng+1:neng*ng)*penal)

    !Energy Equation
    DO ie = 1,dim
       user_Drhs((neng-1)*ng+1:neng*ng) = user_Drhs((neng-1)*ng+1:neng*ng)  &
            + Spenal *( brink_var(:, ie+dim) *(dudn(nvel(ie),:)            - dudn(nden+prev_shift,:)*brink_var(:, ie) - dudn(nden,:)*brink_var(:, ie+dim)) )/eta_c   &    !do not convect no-slip
            + Spenal *( brink_var(:, ie)     *(dudn(nvel(ie)+prev_shift,:) - dudn(nden+prev_shift,:)*brink_var(:, ie+dim)                                ) )/eta_c   &   !do not convect no-slip
            + Spenal3*( brink_var(:, ie+dim) *(dudn(nvel(ie),:)            - dudn(nden+prev_shift,:)*brink_var(:, ie) - dudn(nden,:)*brink_var(:, ie+dim)) )/eta_c   &     
            + Spenal3*( brink_var(:, ie)     *(dudn(nvel(ie)+prev_shift,:) - dudn(nden+prev_shift,:)*brink_var(:, ie+dim)                                ) )/eta_c   &            
            + penal*nu_n*( Unorm(:, ie)*SUM(d2u_brink(ie+dim, :, :),DIM=2) +  Unorm_prev(:, ie)*SUM(d2u_brink(ie, :, :),DIM=2) )   &          !artificial viscosity in non-conservative form
            - penal*(     Unorm_prev(:, ie)*brink_var(:, ie) + Unorm(:, ie)*brink_var(:, ie+dim) - Unorm(:, ie)*u_0(:,ie)  )/eta_b ! &                   ! velocity BC term
    END DO
    ! user_Drhs((neng-1)*ng+1:neng*ng) = (1.0_pr-penal)*user_Drhs((neng-1)*ng+1:neng*ng) + doNORM * penal*user_Drhs((neng-1)*ng+1:neng*ng) !ERIC: is this used?

    user_Drhs((neng-1)*ng+1:neng*ng) = user_Drhs((neng-1)*ng+1:neng*ng) &
         - Spenal * dudn(neng,:)/eta_c   !convect energy (for Neumann/Robin temperature condition)
    user_Drhs((neng-1)*ng+1:neng*ng) = user_Drhs((neng-1)*ng+1:neng*ng) &
         - Spenal3* dudn(neng,:)/eta_c 

    !add on Neumann/Robin condition targets
    user_Drhs((neng-1)*ng+1:neng*ng) = user_Drhs((neng-1)*ng+1:neng*ng) &   ! cont eq penalization
         + Spenal  * ulc(:,nden)*(cp_in(Nspec)-1.0_pr/MW_in(Nspec)/Ma**2.0_pr)*cbvp_B/eta_c &
         + Spenal3 * ulc(:,nden)*(cp_in(Nspec)-1.0_pr/MW_in(Nspec)/Ma**2.0_pr)*cbvp_B/eta_c 
    user_Drhs((neng-1)*ng+1:neng*ng) = user_Drhs((neng-1)*ng+1:neng*ng) &   ! cont eq penalization
         + Spenal  * (ulc(:,neng) - 0.5_pr*SUM(Unorm_prev(:,1:dim)*brink_var(:,1:dim),DIM=2) - 0.5_pr*SUM(Unorm(:,1:dim)*brink_var(:,1+dim:2*dim),DIM=2 ))*cbvp_A/eta_c &
         + Spenal3 * (ulc(:,neng) - 0.5_pr*SUM(Unorm_prev(:,1:dim)*brink_var(:,1:dim),DIM=2) - 0.5_pr*SUM(Unorm(:,1:dim)*brink_var(:,1+dim:2*dim),DIM=2 ))*cbvp_A/eta_c


    !Use conservative form

    DO i=1,dim
       vTemp(1:nwlt,i) = Spenal2   
    END DO

    CALL shift_by(vTemp, v_for, nwlt, 1)
    v_for(1:nwlt,1:dim) = MAX(vTemp, v_for)
    CALL shift_by(vTemp, v_back, nwlt, -1)
    v_back(1:nwlt,1:dim) = MAX(vTemp, v_back)   !

    !Diffusion
    DO ie = 1,ne 
       IF( NOT(no_slip .AND. (ANY(nvel==ie)) .OR. ie==ndrho) ) THEN  !skip velocity components for NS
          !       IF( NOT(NS .AND. (ANY(nvel==ie)) ) ) THEN  !skip velocity components for NS
          shift = ng*(ie-1)
          !--Go through all Boundary points that are specified
          i_p_face(0) = 1
          DO i=1,dim
             i_p_face(i) = i_p_face(i-1)*3
          END DO
          DO face_type = 0, 3**dim - 1
             face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
             CALL get_all_indices_by_face (face_type, j_lev, nloc, iloc)
             IF(nloc > 0 ) THEN 
                DO i = 1, dim
                   !user_Drhs(shift+iloc(1:nloc)) = user_Drhs(shift+iloc(1:nloc)) + (1.0_pr-ABS(face(i)))*delta_Jmx* visc_factor_n*&
                   !                               Spenal2(iloc(1:nloc))*d2u(ie,iloc(1:nloc),i) ! No diffusion normal to the boundary

                   user_Drhs(shift+iloc(1:nloc)) = user_Drhs(shift+iloc(1:nloc)) + (1.0_pr-ABS( REAL(face(i),pr) )) *visc_factor_n&
                                !*MAX(9.0_pr*delta_Jmx/eta_b,0.1_pr/eta_c,1.0_pr)*(v_for(iloc(1:nloc),i)*duFD(ie,iloc(1:nloc),i) - v_back(iloc(1:nloc),i)*duBD(ie,iloc(1:nloc),i) )
                        *MAX(delta_Jmx/eta_c,1.0_pr)*(v_for(iloc(1:nloc),i)*du_diffFD(ie,iloc(1:nloc),i) - v_back(iloc(1:nloc),i)*du_diffBD(ie,iloc(1:nloc),i) )
                END DO
             END IF
          END DO
       END IF
    END DO

    !----------------Add curvature related adjustment for penalization terms
    IF (doCURV) THEN
       ! Calculate additional parameters we'll need to simplify equations-----------------------------------
       ! prev values
       DO i = 1, dim
          vel_tan_prev(:, i) = Utan_prev(:, i)/u_prev(:, nden)     ! Actual tangential velocity components
          vel_norm_prev(:, i) = Unorm_prev(:, i)/u_prev(:, nden)   ! Actual normal velocity components
          vel_void_prev(:, i) = u_prev(:, nvel(i))/u_prev(:, nden) ! Actual velocity components
       END DO

       p_prev = (u_prev(:,neng)-0.5_pr*SUM(u_prev(:,nvel(1):nvel(dim))**2.0_pr,DIM=2)/u_prev(:,nden))*(MW_in(Nspec)-1.0_pr) !pressure

       A_prev = SUM(Utan_prev**2.0_pr, DIM=2)/p_prev                                       ! A parameter, see pdf file for details
       DO i = 1, dim
          B_prev(:, i) = A_prev*vel_tan_prev(:, i)                                    ! B parameter, see pdf file for details
       END DO
       C_prev = SUM(Utan_prev*vel_tan, DIM=2)/(MW_in(Nspec) - 1.0_pr)                        ! C parameter, see pdf file for details
       F_prev = A_prev*(SUM(vel_tan_prev**2.0_pr - doNORM*vel_norm_prev**2.0_pr, DIM=2))/2.0_pr ! F parameter, see pdf file for details
       ! perturbations
       DO i = 1, dim
          vel_tan(:, i) = (Utan(:, i)*u_prev(:, nden) - Utan_prev(:, i)*ulc(:, nden))/u_prev(:, nden)**2.0_pr
          vel_norm(:, i) = (Unorm(:, i)*u_prev(:, nden) - Unorm_prev(:, i)*ulc(:, nden))/u_prev(:, nden)**2.0_pr
          vel_void(:, i) = (ulc(:, nvel(i))*u_prev(:, nden) - u_prev(:, nvel(i))*ulc(:, nden))/u_prev(:, nden)**2.0_pr
       END DO
       p = (ulc(:, neng) - 0.5_pr*(SUM(ulc(:, nvel(1):nvel(dim))*vel_void_prev + u_prev(:, nvel(1):nvel(dim))*vel_void, DIM=2)))*(MW_in(Nspec) - 1.0_pr)
       A = (2.0_pr*SUM(u_prev(:, nvel(1):nvel(dim))*ulc(:, nvel(1):nvel(dim)), DIM=2)*p_prev - SUM(u_prev(:, nvel(1):nvel(dim))*u_prev(:, nvel(1):nvel(dim)), DIM=2)*p)/p_prev**2.0_pr
       DO i = 1, dim
          B(:, i) = A*vel_tan_prev(:, i) + A_prev*vel_tan(:, i)
       END DO
       C = SUM(ulc(:, nvel(1):nvel(dim))*vel_tan_prev + u_prev(:, nvel(1):nvel(dim))*vel_tan, DIM=2)/(MW_in(Nspec) - 1.0_pr)
       F = A*SUM(vel_tan_prev**2.0_pr +(1.0_pr -2.0_pr*doNORM)*vel_norm_prev**2.0_pr,DIM=2)/2.0_pr + A_prev*SUM(vel_tan_prev*vel_tan + (1.0_pr -2.0_pr*doNORM)*vel_norm_prev*vel_norm,DIM=2)
       !-----------------------------------------------------------------------------------------------------
       curv(:) = 0.0_pr
       CALL c_diff_fast(norm, dn, d2n, j_lev, ng, meth_central, 10, dim, 1, dim)
       ! Calculating the curvature. PROBABLY NEED TO CREATE SEPARATE SUBROUTINE FOR SIMPLICITY!!!!
       DO i = 1, dim
          curv(:) = curv(:) + dn(i, :, i) ! Calculate the curvature
       END DO
       curv = Spenal*curv/eta_c ! Common term to simplify the equations, S*k/eta
       ! Add curvature to continuity
       user_Drhs((nden-1)*ng+1:nden*ng) = user_Drhs((nden-1)*ng+1:nden*ng) + curv*A
       ! Add curvature to momentum equation
       DO ie = 1,dim 
          user_Drhs((nvel(ie)-1)*ng+1:nvel(ie)*ng) = user_Drhs((nvel(ie)-1)*ng+1:nvel(ie)*ng) + curv*B(:, ie)
       END DO
       ! Add curvature to energy equation
       user_Drhs((neng-1)*ng+1:neng*ng) = user_Drhs((neng-1)*ng+1:neng*ng) + curv*(C + F)
    END IF
    !-----------------------------------------------------------------------
    !END IF


  END SUBROUTINE Brinkman_Drhs

  !
  ! Uses u_prev_timestep, which is global flat (1D ) array set in time_step_cn()
  !
  SUBROUTINE  Brinkman_Drhs_diag (user_Drhs_diag, u_prev, meth)
    USE user_case_vars
    USE parallel
    IMPLICIT NONE
    REAL (pr), DIMENSION (ng,ne), INTENT(IN) :: u_prev
    INTEGER, INTENT (IN) :: meth
    INTEGER :: meth_central, meth_backward, meth_forward
    REAL (pr), DIMENSION (n), INTENT(INOUT) :: user_Drhs_diag
    INTEGER :: ie, shift,i,j,idim
    REAL (pr), DIMENSION (ng,dim) :: du_diagF, d2u_diagF
    REAL (pr), DIMENSION (ng,dim) :: du_diagB, d2u_diagB
    REAL (pr), DIMENSION (ng,dim) :: du_diagC, d2u_diagC
    !REAL (pr), DIMENSION (ne,ng,dim) :: du, d2u
    REAL (pr), DIMENSION (ne,ng,dim) :: duF, d2uF,duB, d2uB
    REAL (pr), DIMENSION (ne,ng) :: dudn, d2udn_new
    !
    ! User defined variables
    !
    REAL (pr), DIMENSION (ng,dim)     ::  Unorm_prev, Utan_prev
    REAL (pr), DIMENSION (ng) :: dudn_diag

    REAL (pr), DIMENSION (ng,dim)     :: u_0     !for non-stationary obstacle

    REAL (pr), DIMENSION (ng) :: Spenal,Spenal2,Spenal3
    REAL (pr) :: slope, offset

    REAL (pr) :: delta_Jmx, nu_n, cbvp_t
    REAL (pr), DIMENSION(ng, dim) :: v_back, v_for, vTemp

    REAL (pr), DIMENSION (ne+dim,ng) :: dudx

    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc

    INTEGER, DIMENSION(ng) :: icolor
    !ERIC: not set up for Euler

    IF(t.LE.1.0_pr) THEN
       cbvp_t = cbvp_B*t/1.0_pr
    ELSE
       cbvp_t = cbvp_B
    END IF
    meth_central  = meth + BIASING_NONE
    meth_backward = meth + BIASING_BACKWARD
    meth_forward  = meth + BIASING_FORWARD

    CALL c_diff_diag(du_diagF, d2u_diagF, j_lev, ng, meth_forward,  meth_forward, 10)  !ERIC: what are the two different 'meth'?
    CALL c_diff_diag(du_diagB, d2u_diagB, j_lev, ng, meth_backward, meth_backward, 10)
    CALL c_diff_diag(du_diagC, d2u_diagC, j_lev, ng, meth_central,  meth_central, 01)

    !   CALL    c_diff_fast(u_prev, du, d2u, j_lev, ng, meth_central, 11, ne, 1, ne)
    CALL    c_diff_fast(u_prev, duF, d2uF, j_lev, ng, meth_forward, 10, ne, 1, ne)
    CALL    c_diff_fast(u_prev, duB, d2uB, j_lev, ng, meth_backward, 10, ne, 1, ne)

!!$    IF (use_dist) THEN
!!$       CALL user_dist (ng, t+dt, NORMAL=norm, U_TARGET=u_0, COLOR=icolor)
       Spenal = dist
       u_0 = u_target
!!$    ELSE
!!$    CALL user_dist (ng, t+dt, DISTANCE=Spenal, NORMAL=norm, U_TARGET=u_0, COLOR=icolor)
!!$    PRINT *, "U_TARGET, diag", MINVAL(u_0), MAXVAL(u_0)
!!$    PRINT *, "color, diag", MINVAL(icolor), MAXVAL(icolor)
!!$    END IF
    Spenal2= Spenal
    Spenal3= Spenal
    !MASK within penal to add diffusion and sink
    delta_Jmx = MINVAL(h(1,1:dim)/2.0_pr**(REAL(j_mx,pr) - 1.0_pr))

    slope = (ATANH( (0.5_pr-10.0_pr**-3.0_pr)/0.5_pr)- ATANH( (-0.5_pr+10.0_pr**-3.0_pr)/0.5_pr))/(REAL(delta_pts,pr)*delta_Jmx)
    offset = REAL(IB_pts,pr)*delta_Jmx - 1.0_pr/slope*ATANH( (-0.5_pr+10.0_pr**-3.0_pr)/0.5_pr )
    Spenal = MAX(0.5_pr - 10.0_pr**-3.0_pr - 0.5_pr*TANH(slope*(Spenal - offset- 0.0_pr*delta_Jmx)) , 0.0_pr )*penal
    !Spenal2 = MAX(0.5_pr - 10.0_pr**-3.0_pr + 0.5_pr*TANH(slope*(Spenal2 - offset- 1.0_pr*delta_Jmx)) , 0.0_pr )*penal  !core
    Spenal2 = MAX(0.5_pr - 10.0_pr**-3.0_pr + 0.5_pr*TANH(slope*(Spenal2 - offset- 0.0_pr*delta_Jmx)) , 0.0_pr )*penal
    !Spenal2 = penal
    Spenal3 = 0.0_pr
    !   Spenal3 = (MAX(0.5_pr - 10.0_pr**-3.0_pr - 0.5_pr*TANH(slope*(Spenal3 - offset- 3.0_pr*delta_Jmx)) , 0.0_pr )  - Spenal)  *penal  !low order convection before transition


    IF (no_slip) THEN
       DO idim =1,dim
          Utan_prev(:, idim) = 0.0_pr
          Unorm_prev (:,idim) = u_prev(:, nvel(idim))  
       END DO
    ELSE  !ERIC: double check this for Euler Eqsi
       PRINT *, 'DRHS_diag not set up for Euler eqns'
       Utan_prev(:, 1) = 0.0_pr
       DO idim = 1, dim
          Utan_prev(:, 1) = Utan_prev(:,1)+ u_prev(:, nvel(idim))*norm(:,idim)
       END DO
       DO idim = 1, dim
          Unorm_prev(:, idim) = Utan_prev(:,1)*norm(:,idim)
       END DO
       DO idim = 1,dim
          Utan_prev(:,idim) = u_prev(:, nvel(idim))-Unorm_prev(:,idim)
       END DO
    END IF




    !Set normal derivatives upwind
    DO j = 1,ne
       dudn(j,:) = 0.5_pr*SUM( duB(j,:, :)*(norm(:,:) + ABS(norm(:,:))) + duF(j,:, :)*(norm(:,:) - ABS(norm(:,:))) ,DIM=2)
    END DO
    dudn_diag(:) = 0.5_pr*SUM( du_diagB(:, :)*(norm(:,:) + ABS(norm(:,:))) + du_diagF(:, :)*(norm(:,:) - ABS(norm(:,:))) ,DIM=2)

    !PRINT *, 'dudn_ndrho:', MINVAL(dudn(ndrho,:)*penal), MAXVAL(dudn(ndrho,:)*penal)

    DO j = 1,ne
       dudx(j,:) = 0.5_pr*SUM((u_0 + ABS(u_0))*du_diagB(:,:) + (u_0 - ABS(u_0))*du_diagF(:,:), DIM=2)
    END DO


    !set nu_n
    nu_n = visc_factor_d**2.0_pr *delta_Jmx**2.0_pr/eta_b
    !nu_n = visc_factor_d *delta_Jmx

    ! sets NN RHS to zero inside of Brinkman zone
    DO ie = 1,ne
       user_Drhs_diag((ie-1)*ng+1:ie*ng) = (1.0_pr-penal)*user_Drhs_diag((ie-1)*ng+1:ie*ng) 
    END DO

    IF (doInvariant) THEN
       ! ------------Add on Lagrange terms--------------
       ! Density
       user_Drhs_diag((nden-1)*ng+1:nden*ng) = user_Drhs_diag((nden-1)*ng+1:nden*ng) &   ! cont eq penalization
            - penal  * dudx(nden,:)
       ! Momentum
       DO ie = 1,dim 
          user_Drhs_diag((nvel(ie)-1)*ng+1:nvel(ie)*ng) = user_Drhs_diag((nvel(ie)-1)*ng+1:nvel(ie)*ng) &
               - penal *  dudx(nvel(ie),:)
       END DO
       ! Energy
       user_Drhs_diag((neng-1)*ng+1:neng*ng) = user_Drhs_diag((neng-1)*ng+1:neng*ng) &
            - penal * dudx(neng,:)
       ! --------------------------------------------------
    END IF

    IF(conv_drho) THEN 
       !Convect normal derivative
       user_Drhs_diag((ndrho-1)*ng+1:ndrho*ng) = user_Drhs_diag((ndrho-1)*ng+1:ndrho*ng) &   ! cont eq penalization
            - (Spenal)* dudn_diag(:)/eta_c &!use low order
            - (Spenal3)*dudn_diag(:)/eta_c !use low order
       !PRINT *, 'DRHO2:', MINVAL(user_Drhs((ndrho-1)*ng+1:ndrho*ng)*penal), MAXVAL(user_Drhs((ndrho-1)*ng+1:ndrho*ng)*penal)
       DO ie = 1,dim 
          user_Drhs_diag((nvel(ie)-1)*ng+1:nvel(ie)*ng) = user_Drhs_diag((nvel(ie)-1)*ng+1:nvel(ie)*ng) &  !density evolution condition
               - (Spenal+Spenal3)*(-u_prev(:,ndrho)/u_prev(:,nden)  )/eta_c       !density target
       END DO
       user_Drhs_diag((neng-1)*ng+1:neng*ng) = user_Drhs_diag((neng-1)*ng+1:neng*ng) &
            + Spenal *u_prev(:,ndrho) * u_prev(:,nden)/u_prev(:,nden)**2.0_pr/eta_c & !remove evolutionary drho effects from Temperature slope
            + Spenal3*u_prev(:,ndrho) * u_prev(:,nden)/u_prev(:,nden)**2.0_pr/eta_c  !remove evolutionary drho effects from Temperature slope
    END IF

    !Continuity Equation
    user_Drhs_diag((nden-1)*ng+1:nden*ng) = user_Drhs_diag((nden-1)*ng+1:nden*ng)  &  ! cont eq penalization
         - Spenal *  (dudn_diag(:))/eta_c &
         - Spenal3*  (dudn_diag(:))/eta_c 


    !Momentum Equation
    DO ie = 1,dim  !ERIC: note, not valid for Euler 
       user_Drhs_diag((nvel(ie)-1)*ng+1:nvel(ie)*ng) = user_Drhs_diag((nvel(ie)-1)*ng+1:nvel(ie)*ng) &
                                !- Spenal * (dudn_diag(:)-0.0_pr)/eta_c  & !tangential :Euler  !ERIC:does d(1-n_x)/dn = 1 or 0?
                                !- Spenal3* (dudn_diag(:)-0.0_pr)/eta_c  & !tangential: Euler
                                !+ penal*nu_n *(2.0_pr/u_prev(:,nden)**3.0_pr*SUM(du(nden, :, :)**2.0_pr,DIM=2)-u_prev(:,nden)**-2.0_pr*SUM(d2u(nden, :, :),DIM=2)) !ERIC: how to do second derivative?  !artificial viscosity in non-conservative form
            + penal*nu_n * SUM(d2u_diagC(:,:),DIM=2) !ERIC: how to do second derivative?  !artificial viscosity in non-conservative form
       user_Drhs_diag((nvel(ie)-1)*ng+1:nvel(ie)*ng) = user_Drhs_diag((nvel(ie)-1)*ng+1:nvel(ie)*ng) &
            - penal/eta_b !&   !Normal velocity & Navier-Stokes
       user_Drhs_diag((nvel(ie)-1)*ng+1:nvel(ie)*ng) = user_Drhs_diag((nvel(ie)-1)*ng+1:nvel(ie)*ng) &  !density evolution condition
            - Spenal *( dudn(nden,:)/u_prev(:,nden))/eta_c &
            - Spenal3*( dudn(nden,:)/u_prev(:,nden))/eta_c 
    END DO

    !Energy Equation
    user_Drhs_diag((neng-1)*ng+1:neng*ng) = user_Drhs_diag((neng-1)*ng+1:neng*ng) &
         - Spenal * dudn_diag(:)/eta_c   !convect energy (for Neumann/Robin temperature condition)

    user_Drhs_diag((neng-1)*ng+1:neng*ng) = user_Drhs_diag((neng-1)*ng+1:neng*ng) &
         - Spenal3* dudn_diag(:)/eta_c 


    !add on Neumann/Robin condition targets
    user_Drhs_diag((neng-1)*ng+1:neng*ng) = user_Drhs_diag((neng-1)*ng+1:neng*ng) &   ! cont eq penalization
         + Spenal  *cbvp_A/eta_c &
         + Spenal3 *cbvp_A/eta_c


    !Use conservative form

    DO i=1,dim
       vTemp(1:nwlt,i) = Spenal2   
    END DO

    CALL shift_by(vTemp, v_for, nwlt, 1)
    v_for(1:nwlt,1:dim) = MAX(vTemp, v_for)
    CALL shift_by(vTemp, v_back, nwlt, -1)
    v_back(1:nwlt,1:dim) = MAX(vTemp, v_back)   !

    !Diffusion
    DO ie = 1,ne 
       IF( NOT(no_slip .AND. (ANY(nvel==ie)) .OR. ie==ndrho) ) THEN  !skip velocity components for NS
          !       IF( NOT(NS .AND. (ANY(nvel==ie)) ) ) THEN  !skip velocity components for NS
          shift = ng*(ie-1)
          !--Go through all Boundary points that are specified
          i_p_face(0) = 1
          DO i=1,dim
             i_p_face(i) = i_p_face(i-1)*3
          END DO
          DO face_type = 0, 3**dim - 1
             face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
             CALL get_all_indices_by_face (face_type, j_lev, nloc, iloc)
             IF(nloc > 0 ) THEN 
                DO i = 1, dim
                   !user_Drhs(shift+iloc(1:nloc)) = user_Drhs(shift+iloc(1:nloc)) + (1.0_pr-ABS(face(i)))*delta_Jmx* visc_factor_n*&
                   !                               Spenal2(iloc(1:nloc))*d2u(ie,iloc(1:nloc),i) ! No diffusion normal to the boundary

                   user_Drhs_diag(shift+iloc(1:nloc)) = user_Drhs_diag(shift+iloc(1:nloc)) + (1.0_pr-ABS( REAL(face(i),pr) )) *visc_factor_n&
                                !*MAX(9.0_pr*delta_Jmx/eta_b,0.1_pr/eta_c,1.0_pr)*(v_for(iloc(1:nloc),i)*duFD(ie,iloc(1:nloc),i) - v_back(iloc(1:nloc),i)*duBD(ie,iloc(1:nloc),i) )
                        *MAX(delta_Jmx/eta_c,1.0_pr)*(v_for(iloc(1:nloc),i)*du_diagF(iloc(1:nloc),i) - v_back(iloc(1:nloc),i)*du_diagB(iloc(1:nloc),i) )
                END DO
             END IF
          END DO
       END IF
    END DO

  END SUBROUTINE Brinkman_Drhs_diag

  SUBROUTINE Brinkman_input
   
    INTEGER :: isp

    !use a vector method for speed (at expense of memory) 
    call input_logical ('meth_vect_diff',meth_vect_diff,'stop','use c_diff_fast_vector for forward/backward derivatives')

    !Type of obstacle
    !call input_logical ('brinkman_penal',brinkman_penal,'stop','Brinkman penalization to be used')
    !IF(.NOT.(brinkman_penal))   call input_logical ('vp',vp,'stop','volume penalization to be used')
    call input_logical ('vp',vp,'stop','volume penalization to be used')

    call input_logical ('no_slip',no_slip,'stop','no_slip: use the no slip condition on velocity')
    conv_drho = .FALSE.
    IF(vp) call input_logical ('conv_drho',conv_drho,'stop','conv_drho: extrapolate drho as integrated variable')


    smooth_dist = .FALSE. ! default value
    call input_logical ('smooth_dist',smooth_dist,'default','T smoothes distance function prior finding the normal')

    deltadel_loc = 1.0_pr
    call input_real ('deltadel_loc',deltadel_loc,'default', 'sets final diffusion thickness using spatial resolution (dx, dy, or dz), number of points across final thickness')


    !brinkman parameters
    eta_b = 1.0_pr
    eta_c = 1.0_pr
    call input_real ('eta_b',eta_b,'stop',' penalization parameter') !also for volume penalization
    call input_real ('eta_c',eta_c,'stop',' penalization parameter for adiabatic conditions') !also for volume penalization

    call input_real ('porosity',porosity,'stop',' porosity for brinkman')

    !penalization control
    call input_logical ('doCURV',doCURV,'stop', 'If true use curvature adjustment')
    call input_real ('doNORM',doNORM,'stop', 'If true add terms with normal velocity')

    ! ---------------- Nurlybek ---------------------------
    saveDIST = .FALSE.
    call input_logical ('doInvariant', doInvariant, 'default', 'whether add lagrangian-like terms')
    call input_logical ('saveDIST',saveDIST,'default', 'saveDIST: save the distance function')    
    call input_integer ('obs_type',obs_type,'stop', 'Type of the obstacle')
    ! Single cylinder
    ALLOCATE(xsp(dim))
    ALLOCATE(accel(dim))
    ALLOCATE(vel(dim))
    ALLOCATE(pos(dim))
    xsp(:) = 0.0_pr
    rhosp = 1.0_pr
    rsp = 1.0_pr
    call input_real_vector ('xsp',xsp(1:dim),dim,'default','sphere center coordinates')
    call input_real ('rsp',rsp,'deault', 'spherical obstacle radius r')
    call input_real ('rhosp',rhosp,'deault', 'spherical obstacle density')
    ! Mass calculation
    msp = 8.0_pr*ASIN(1.0_pr)*rsp**3*rhosp/4.0_pr
    ! Multiple cylinders
    Nsp = 1
    call input_integer ('Nsp',Nsp,'default', 'Number of spheres')
    ALLOCATE(xsps(3*Nsp))
    ALLOCATE(rsps(Nsp))
    ALLOCATE(rhosps(Nsp))
    ALLOCATE(msps(Nsp))
    ALLOCATE(accels(dim, Nsp))
    ALLOCATE(vels(dim, Nsp))
    ALLOCATE(ang_vels(dim, Nsp))
    ALLOCATE(ang_poss(dim, Nsp))
    ALLOCATE(vsps(3*Nsp))
    ALLOCATE(wsps(3*Nsp))
    ALLOCATE(psps(3*Nsp))
    ALLOCATE(poss(dim, Nsp))
    ALLOCATE(inertias(Nsp))
    xsps(:)= 0.0_pr
    rsps(:) = 1.0_pr
    rhosps(:) = 1.0_pr
    vels(:,:) = 0.0_pr
    ang_vels(:,:) = 0.0_pr
    ang_poss(:,:) = 0.0_pr
    vsps(:) = 0.0_pr
    wsps(:) = 0.0_pr
    psps(:) = 0.0_pr
    call input_real_vector ('vsps',vsps(1:3*Nsp),3*Nsp,'default','sphere initial velocities')
    call input_real_vector ('xsps',xsps(1:3*Nsp),3*Nsp,'default','sphere center coordinates')
    call input_real_vector ('wsps',wsps(1:3*Nsp),3*Nsp,'default','sphere initial angular velocity')
    call input_real_vector ('psps',psps(1:3*Nsp),3*Nsp,'default','sphere initial angular position')
    call input_real_vector ('rsps',rsps(1:Nsp),Nsp,'default','sphere radii')
    call input_real_vector ('rhosps',rhosps(1:Nsp),Nsp,'default','sphere densities')
    msps(:) = 8.0_pr*ASIN(1.0_pr)*rsps(:)**3*rhosps(:)/4.0_pr
    inertias(:) = 0.4_pr*msps(:)*rsps(:)*rsps(:)  ! Ryan
    accels(:,:) = 0.0_pr
    DO isp = 1, Nsp
       poss(:, isp) = xsps(3*(isp - 1)+1:3*(isp-1)+dim)
       vels(:, isp) = vsps(3*(isp - 1)+1:3*(isp-1)+dim)
       ang_vels(:, isp) = wsps(3*(isp - 1)+1:3*(isp-1)+dim)
       IF (dim == 2) ang_vels(2:3,isp) = 0.0_pr    !set other components to zero for 2d
       ang_poss(:, isp) = psps(3*(isp - 1)+1:3*(isp-1)+dim)
    END DO
    PRINT *, "poss input", poss
    ! ---------------- End Nurlybek -----------------------


    !eric's support parameters
    call input_integer ('delta_pts',delta_pts,'stop', 'Number of points to transition from convection to diffusion')
    call input_integer ('IB_pts',IB_pts,'stop', 'Number of points (distance) for convection inside of the immersed boundary')

    cbvp_A = 0.0_pr 
    cbvp_B = 0.0_pr 
    IF (vp)                call input_real ('cbvp_A',cbvp_A,'stop','cbvp_A:  For robin condition of the form dT/dn = AT+B')
    IF (vp)                call input_real ('cbvp_B',cbvp_B,'stop','cbvp_B:  For robin condition of the form dT/dn = AT+B')
    !Only adiabatic condition for Euler
    IF(.NOT. no_slip) cbvp_A=0.0_pr
    IF(.NOT. no_slip) cbvp_B=0.0_pr
    !non-adiabatic obstacle requires conv_drho = T
    IF( (cbvp_A .NE. 0.0_pr) .OR. (cbvp_B .NE. 0.0_pr) ) conv_drho = .TRUE. 

    call input_real ('visc_factor_n',visc_factor_n,'stop','visc_factor_n: multiplier on numerical viscosity for penalization stability on neumann terms')
    call input_real ('visc_factor_d',visc_factor_d,'stop','visc_factor_d: multiplier on numerical viscosity for penalization stability on dirichlet terms')

    ! Ryan - added these variables to input file for lagrangian partilce collisions
    particle_epsN = 1.0_pr
    particle_epsT = 0.0_pr
    call input_real('particle_epsN',particle_epsN,'default','coefficient of restitution for the particles')
    call input_real('particle_epsT',particle_epsT,'default','coefficient of friction for the particles')


  END SUBROUTINE Brinkman_input

END MODULE Brinkman




MODULE user_case

  USE precision
  USE wlt_vars
  USE wavelet_filters_mod
  USE elliptic_mod
  USE elliptic_vars
  USE wlt_trns_mod
  USE wlt_trns_vars
  USE wlt_trns_util_mod
  USE io_3d_vars
  USE util_mod
  USE util_vars
  USE share_consts
  USE pde
  USE sizes
  USE share_kry
  USE vector_util_mod
  USE field
  USE input_file_reader
  USE parallel
  USE wlt_FWH
  USE hyperbolic_solver
  USE Brinkman
  USE user_case_vars
! Ryan
  USE lagrangian

  !
  ! case specific variables
  !

  REAL (pr) :: MAreport, CFLreport, CFLconv, CFLacou, maxsos, mydt_orig
  REAL (pr) :: peakchange, maxMa
  REAL (pr) :: Tconst, Pint, gammR, Y_Xhalf
  REAL (pr), ALLOCATABLE, DIMENSION (:) :: pureX, pureY, pureR, pureg, puregamma, YR
  REAL (pr) :: e0top, e0bot, e0right,e0left, rho0top, drho0top, rho0bot, drho0bot, rhoetopBC, rhoebotBC, rhoetop, rhoebot ,velleft  !used anymore?
  REAL (pr) ::  e0,rho0
  REAL (pr), ALLOCATABLE, DIMENSION (:) :: Y0top, Y0bot, rhoYtopBC, rhoYbotBC, rhoYtop, rhoYbot, Lxyz
  REAL (pr), DIMENSION (2) :: rhoYg, drho0, dP0, c0, P0, gammBC
  !REAL (pr), ALLOCATABLE, DIMENSION (:) ::  mu_in, kk_in, cp_in, MW_in, gamm  
  REAL (pr), ALLOCATABLE, DIMENSION (:) :: bD_in, gr_in
  REAL (pr), ALLOCATABLE, DIMENSION (:,:) :: bndvals
  REAL (pr), DIMENSION (5) :: buffcvd, buffcvf, buffbfd, buffbff
  INTEGER :: n_var_pressure  ! start of pressure in u array
  REAL (pr) ::  Re, S1, Pra, Sc, Fr, At!, Ma
  REAL (pr) :: FWHdt
  INTEGER :: ndpx, nboy, nbRe, ndom, nton, BCver,ICver,nmsk,ncvtU, nQcrit, ncol
  INTEGER :: methpd
  INTEGER, ALLOCATABLE :: nspc(:), nvon(:), nson(:) 
  LOGICAL :: NS ! if NS=.TRUE. - Navier-Stoke, else Euler
  LOGICAL, PARAMETER :: NSdiag=.TRUE. ! if NSdiag=.TRUE. - use viscous terms in user_rhs_diag
  INTEGER :: nspc_lil, nspc_big
  LOGICAL :: GRAV, adaptAt
  LOGICAL ::  buffBC, cnvtBC, upwind_cnvtBC, diffBC
  LOGICAL ::  polybuff,  polyconv,  pBrink
  REAL (pr) ::  buffU0, buffSig, buffDomFrac
  REAL (pr) :: buff_visc
  LOGICAL :: adaptMagVort, saveMagVort, adaptComVort, saveComVort, adaptMagVel, saveMagVel, adaptNormS, saveNormS, adaptGradY, saveGradY, saveUcvt, saveQcrit  !4extra - this whole line
  REAL (pr) :: MagVortScale
  LOGICAL :: FWHstats,object_forces_volume, object_forces_surface, object_forces_divstress
  INTEGER :: surf_pts,surf_pts_z
  INTEGER :: nmvt, ncvt(3), nmvl, nnms !4extra - this whole line
  INTEGER, ALLOCATABLE :: ngdy(:) !4extra - this whole line
  REAL (pr) :: Lczn, tfacczn, itfacczn, itczn, tczn, cczn, czn_min, czn_max
  LOGICAL :: boundY, modczn, splitFBpress, stepsplit, savepardom
  REAL (pr) ::  tempscl, specscl
  INTEGER ::  locnvar
  LOGICAL ::  adaptden, adaptvel, adapteng, adaptspc, adaptprs 
  LOGICAL :: adaptvelonly, adaptspconly, adapttmp, convcfl

  !REAL (pr) :: porosity
  REAL (pr) :: grav_coeff
  REAL (pr), DIMENSION(1000) :: Re_tau_hist, Re_bulk_hist, grav_coeff_hist    !vectors with spatially averaged values for the previous 1000 steps
  REAL (pr) :: Re_tau, Re_bulk                                                !spatio-temporal averaged values

  REAL (pr), ALLOCATABLE, DIMENSION(:) :: buff_thick  !thickness of buffer region

  INTEGER :: iter_stat    !used for plane-averaged statistics

CONTAINS

  SUBROUTINE  user_setup_pde ( VERB ) 
    USE hyperbolic_solver
    IMPLICIT NONE
    LOGICAL, OPTIONAL :: VERB         ! print debug info
    INTEGER :: i, l, n_next   !4extra
    CHARACTER(LEN=8) :: specnum  !4extra
    REAL(pr) :: nd_assym_high, nd_assym_bnd_high, nd2_assym_high, nd2_assym_bnd_high, nd_assym_low, nd_assym_bnd_low, nd2_assym_low, nd2_assym_bnd_low


    locnvar = dim+Nspec+1

    IF (par_rank.EQ.0) THEN
       PRINT * ,''
       PRINT *, '**********************Setting up PDE*****************'
       PRINT * ,'CASE: External Flow DNS '
       PRINT *, '*****************************************************'
    END IF

    n_integrated = dim + 1 + Nspec     
    IF(conv_drho) n_integrated = n_integrated +1 !extra one to convect drho/dn     
    IF(do_Adp_Eps_Spatial_Evol) n_integrated = n_integrated +1 ! variable thresholding



    n_var_additional = 1

    !    IF(hypermodel .NE. 0) THEN
    !       n_var_additional = n_var_additional + 1
    !    END IF

    IF (adapttmp) THEN
       n_var_additional = n_var_additional + 1
       nton = n_integrated + n_var_additional
    END IF

    IF (adaptvelonly) THEN
       IF (ALLOCATED(nvon)) DEALLOCATE(nvon)
       ALLOCATE (nvon(dim))
       DO i=1,dim
          n_var_additional = n_var_additional + 1
          nvon(i) = n_integrated + n_var_additional
       END DO
    END IF

    IF (adaptspconly .AND. Nspec > 1) THEN
       IF (ALLOCATED(nson)) DEALLOCATE(nson)
       ALLOCATE (nson(Nspecm))
       DO l=1,Nspecm
          n_var_additional = n_var_additional + 1
          nson(l) = n_integrated + n_var_additional
       END DO
    END IF

    IF (savepardom) THEN
       n_var_additional = n_var_additional + 1
       ndom = n_integrated + n_var_additional
    END IF

    !4extra - need all these definitions
    IF( adaptMagVort .or. saveMagVort ) THEN
       n_var_additional = n_var_additional + 1
       nmvt = n_integrated + n_var_additional
    END IF
    IF( (adaptComVort .or. saveComVort) .AND. dim==2 ) THEN
       n_var_additional = n_var_additional + 1
       ncvt(:) = n_integrated + n_var_additional
    END IF
    IF( (adaptComVort .or. saveComVort) .AND. dim==3 ) THEN
       DO i=1,3
          n_var_additional = n_var_additional + 1
          ncvt(i) = n_integrated + n_var_additional
       END DO
    END IF
    IF( adaptMagVel  .OR. saveMagVel  ) THEN
       n_var_additional = n_var_additional + 1
       nmvl = n_integrated + n_var_additional
    END IF
    IF( adaptNormS   .OR. saveNormS   ) THEN
       n_var_additional = n_var_additional + 1
       nnms = n_integrated + n_var_additional
    END IF
    IF (ALLOCATED(ngdy)) DEALLOCATE(ngdy)
    IF (Nspec>1) THEN
       ALLOCATE(ngdy(Nspecm))
       IF( adaptGradY   .OR. saveGradY   ) THEN
          DO l=1,Nspecm
             n_var_additional = n_var_additional + 1
             ngdy(l) = n_integrated + n_var_additional
          END DO
       END IF
    END IF
    IF (imask_obstacle) THEN
       IF (saveDIST) THEN
          n_var_additional = n_var_additional + 1 ! Distance function
          ndist = n_integrated + n_var_additional
       END IF
       n_var_additional = n_var_additional + 1
       nmsk = n_integrated + n_var_additional
       n_var_additional = n_var_additional + 1
       ncol = n_integrated + n_var_additional
    END IF
    IF ( saveUcvt) THEN 
       n_var_additional = n_var_additional + 1
       ncvtU = n_integrated + n_var_additional
    END IF
    IF ( saveQcrit) THEN 
       n_var_additional = n_var_additional + 1
       nQcrit = n_integrated + n_var_additional
    END IF


    n_var = n_integrated + n_var_additional !--Total number of variables

    n_var_exact = 0


    !***********Variable orders, ensure 1:dim+1+Nspec are density, velocity(dim), energy, species(Nspecm)**************
    IF (ALLOCATED(nvel)) DEALLOCATE(nvel)
    ALLOCATE(nvel(dim))
    IF (ALLOCATED(nspc)) DEALLOCATE(nspc)
    IF (Nspec>1) ALLOCATE(nspc(Nspecm))

    !this order is required: nden, nmom, neng
    nden = 1
    DO i=1,dim 
       nvel(i) = i+1
    END DO
    neng = dim+2
    IF (Nspec>1) THEN
       DO i=1,Nspecm
          nspc(i) = dim+2+i
       END DO
    END IF

    n_next = neng + Nspecm + 1  !ERIC: how does this mesh with sgs module
    IF( do_Adp_Eps_Spatial_Evol ) THEN
       n_var_EpsilonEvol = n_next
       n_next = n_next+1
    END IF
    IF( conv_drho ) THEN 
       ndrho = n_next
       n_next = n_next+1
    END IF
    nprs = n_integrated +1 


    n_var_pressure  = nprs !pressure
    !n_var_mom = nvel


    nspc_lil = 1
    nspc_big = 0
    IF (Nspec>1) nspc_lil=nspc(1)
    IF (Nspec>1) nspc_big=nspc(Nspecm)

    !******************************************************************************************************************
    !
    !--Allocate logical arrays 
    !  Must be done after setting number of different types of variables
    !  and before setting logical variable mapping arrays
    !
    CALL alloc_variable_mappings
    !
    ! Fill in variable names for default variables for integration meth 2
    ! These may be changed in the call to the user specified routine
    !  user_setup_pde()
    !
    ! In integrated variables (3D)
    WRITE (u_variable_names(nden), u_variable_names_fmt) 'Den_rho  '
    WRITE (u_variable_names(nvel(1)), u_variable_names_fmt) 'XMom  '
    IF (dim.GE.2)WRITE (u_variable_names(nvel(2)), u_variable_names_fmt) 'YMom  '
    IF (dim.EQ.3) WRITE (u_variable_names(nvel(3)), u_variable_names_fmt) 'ZMom  '
    WRITE (u_variable_names(neng), u_variable_names_fmt) 'E_Total  '
    IF (Nspec>1) THEN
       DO l=1,Nspecm      
          WRITE (specnum,'(I8)') l          
          WRITE (u_variable_names(nspc(l)), u_variable_names_fmt) TRIM('Spec_Scalar_'//ADJUSTL(TRIM(specnum)))//'  '
       END DO
    END IF
    WRITE (u_variable_names(nprs), u_variable_names_fmt) 'Pressure  '
    IF(conv_drho)WRITE (u_variable_names(ndrho), u_variable_names_fmt) 'Drho_dn  '
    !    IF(hypermodel .NE. 0) WRITE (u_variable_names(nprs+1), u_variable_names_fmt) 'visc  '


    IF (adapttmp) WRITE (u_variable_names(nton), u_variable_names_fmt) 'Temperature  '
    IF (adaptvelonly) THEN
       WRITE (u_variable_names(nvon(1)), u_variable_names_fmt) 'XVelocity  '
       WRITE (u_variable_names(nvon(2)), u_variable_names_fmt) 'YVelocity  '
       IF (dim.eq.3) WRITE (u_variable_names(nvon(3)), u_variable_names_fmt) 'ZVelocity  '
    END IF
    IF (adaptspconly .AND. Nspec > 1) THEN
       DO l=1,Nspecm      
          WRITE (specnum,'(I8)') l          
          WRITE (u_variable_names(nson(l)), u_variable_names_fmt) TRIM('Mass_Frac_'//ADJUSTL(TRIM(specnum)))//'  '
       END DO
    END IF

    IF (savepardom)  WRITE (u_variable_names(ndom), u_variable_names_fmt) 'MyDomain  '

    !4extra - need all these definitions
    IF( adaptMagVort .or. saveMagVort ) WRITE (u_variable_names(nmvt), u_variable_names_fmt) 'MagVort  '
    IF( (adaptComVort .or. saveComVort) .AND. dim==2 )  WRITE (u_variable_names(ncvt(1)), u_variable_names_fmt) 'ComVort  '
    IF( (adaptComVort .or. saveComVort) .AND. dim==3 ) THEN
       DO l=1,3
          WRITE (specnum,'(I8)') l
          WRITE (u_variable_names(ncvt(l)), u_variable_names_fmt) TRIM('ComVort_'//ADJUSTL(TRIM(specnum)))//'  '
       END DO
    END IF
    IF( adaptMagVel  .OR. saveMagVel  )  WRITE (u_variable_names(nmvl), u_variable_names_fmt) 'MagVel  '
    IF( adaptNormS   .OR. saveNormS   )  WRITE (u_variable_names(nnms), u_variable_names_fmt) 'NormS  '
    IF( (adaptGradY   .OR. saveGradY) .AND. Nspec > 1 ) THEN
       DO l=1,Nspecm
          WRITE (specnum,'(I8)') l
          WRITE (u_variable_names(ngdy(l)), u_variable_names_fmt) TRIM('GradY_'//ADJUSTL(TRIM(specnum)))//'  '
       END DO
    END IF

    IF (imask_obstacle) THEN
       WRITE (u_variable_names(nmsk), u_variable_names_fmt) 'Mask  '
       IF (ndist > 0) WRITE (u_variable_names(ndist), u_variable_names_fmt) 'Dist  '
       IF (ncol > 0) WRITE (u_variable_names(ncol), u_variable_names_fmt) 'Color  '
    END IF
    IF ( saveUcvt) WRITE (u_variable_names(ncvtU), u_variable_names_fmt) 'ConvectZone  '
    IF ( saveQcrit) WRITE (u_variable_names(nQcrit), u_variable_names_fmt) 'Q_criterion  '



    !
    ! setup logical variable mapping arrays
    ! This defined which variables are used for adaptation, Which need to be interpololated to the new grid at 
    ! each time step, which are saved, and for which we have an exect solutiuon we want to check at each time step
    !
    !
    ! setup which components we will base grid adaptation on.
    !
    n_var_adapt = .FALSE. !intialize
    IF (adaptden) n_var_adapt(nden,:)                  = .TRUE.
    IF (adaptvel) n_var_adapt(nvel(1):nvel(dim),:)     = .TRUE.
    IF (adapteng) n_var_adapt(neng,:)                  = .TRUE.
    IF (adaptspc) n_var_adapt(nspc(1):nspc(Nspecm),:) = .TRUE.
    IF (adaptprs) n_var_adapt(nprs,:)                  = .TRUE.

    IF (adapttmp) n_var_adapt(nton,:)                  = .TRUE.
    IF (adaptvelonly) n_var_adapt(nvel(1):nvel(dim),:)     = .FALSE.
    IF (adaptvelonly) n_var_adapt(nvon(1):nvon(dim),:)     = .TRUE.
    IF (adaptspconly .AND. Nspec > 1) n_var_adapt(nspc(1):nspc(Nspecm),:) = .FALSE.
    IF (adaptspconly .AND. Nspec > 1) n_var_adapt(nson(1):nson(Nspecm),:) = .TRUE.


    !4extra - adapt
    IF( adaptMagVort ) n_var_adapt(nmvt,:)                  = .TRUE.
    IF( adaptComVort ) n_var_adapt(ncvt(1):ncvt(3),:)       = .TRUE.
    IF( adaptMagVel  ) n_var_adapt(nmvl,:)                  = .TRUE.
    IF( adaptNormS   ) n_var_adapt(nnms,:)                  = .TRUE.
    IF( adaptGradY .AND. Nspec > 1 ) n_var_adapt(ngdy(1):ngdy(Nspecm),:) = .TRUE.

    IF( imask_obstacle ) THEN
       n_var_adapt(nmsk,:)                  = .TRUE.
!!$       n_var_adapt(ncol, :) = .FALSE.
    END IF


    !--integrated variables at first time level
    !Variables that need to be interpoleted to new adapted grid in initial grid adaptation
    !Variables that need to be interpoleted to new adapted grid at each time step
    n_var_interpolate        = .FALSE. !intialize
    n_var_interpolate(1:nprs,:) = .TRUE.  
    IF(conv_drho)   n_var_interpolate(ndrho,:) = .TRUE.  
    IF(imask_obstacle) n_var_interpolate(nmsk,:) = .TRUE.

    IF (adapttmp) n_var_interpolate(nton,:)                  = .TRUE.
    IF (adaptvelonly) n_var_interpolate(nvon(1):nvon(dim),:)     = .TRUE.
    IF (adaptspconly .AND. Nspec > 1) n_var_interpolate(nson(1):nson(Nspecm),:) = .TRUE.

    !4extra - need these
    IF( saveMagVort ) n_var_interpolate(nmvt,:)                   = .TRUE.
    IF( saveComVort ) n_var_interpolate(ncvt(1):ncvt(3),:)        = .TRUE.
    IF( saveMagVel  ) n_var_interpolate(nmvl,:)                   = .TRUE.
    IF( saveNormS   ) n_var_interpolate(nnms,:)                   = .TRUE.
    IF( saveGradY .AND. Nspec > 1 ) n_var_interpolate(ngdy(1):ngdy(Nspecm),:)  = .TRUE.
    IF( imask_obstacle    ) THEN
       n_var_interpolate(nmsk,:)    = .TRUE.
!!$       n_var_interpolate(ncol, :) = .TRUE.
    END IF
    IF( saveUcvt    ) n_var_interpolate(ncvtU,:)    = .TRUE.
    IF( saveQcrit    ) n_var_interpolate(nQcrit,:)    = .TRUE.

    !
    ! setup which components we have an exact solution for
    !
    n_var_exact_soln = .FALSE. !intialize

    !
    ! setup which variables we will save the solution
    !
    n_var_save = .FALSE. !intialize 
    n_var_save(1:nprs) = .TRUE. ! save all for restarting code
    !    IF(hypermodel .NE. 0) n_var_save(nprs+1) = .TRUE.

    IF (adapttmp) n_var_save(nton) = .TRUE.
    IF (savepardom)  n_var_save(ndom)  = .TRUE.

    !4extra - need these
    IF( saveMagVort ) n_var_save(nmvt)     = .TRUE.
    IF( saveComVort ) n_var_save(ncvt(1):ncvt(3))  = .TRUE.
    IF( saveMagVel  ) n_var_save(nmvl)     = .TRUE.
    IF( saveNormS   ) n_var_save(nnms)     = .TRUE.
    IF( saveGradY .AND. Nspec > 1) n_var_save(ngdy(1):ngdy(Nspecm)) = .TRUE.
    IF( imask_obstacle    ) THEN
       n_var_save(nmsk)    = .TRUE.
       IF(saveDIST )  n_var_save(ndist)    = .TRUE.
       n_var_save(ncol) = .TRUE.
    END IF
    IF( saveUcvt    ) n_var_save(ncvtU)    = .TRUE.
    IF( saveQcrit    ) n_var_save(nQcrit)    = .TRUE.

    n_var_req_restart = n_var_save !does not require hyperbolic module viscosity for restart



    !
    ! Set the maximum number of components for which we have an exact solution
    !
    n_var_exact = MAX( COUNT(n_var_exact_soln(:,0)),COUNT(n_var_exact_soln(:,1)) )

    !
    ! Setup a scaleCoeff array if we want to tweak the scaling of a variable
    ! ( if scaleCoeff < 1.0 then more points will be added to grid 
    !
    ALLOCATE ( scaleCoeff(1:n_var) )
    scaleCoeff = 1.0_pr
!!$    scaleCoeff(nmsk) = 2.0_pr
    IF(adaptMagVort)  scaleCoeff(nmvt) = MagVortScale


    IF(hypermodel .NE. 0) THEN
       IF(ALLOCATED(n_var_hyper)) DEALLOCATE(n_var_hyper)
       ALLOCATE(n_var_hyper(1:n_var))
       n_var_hyper = .FALSE.
       n_var_hyper(nden) = .TRUE.
       n_var_hyper(neng) = .TRUE.
       !n_var_hyper = .TRUE.


       IF(ALLOCATED(n_var_hyper_active)) DEALLOCATE(n_var_hyper_active)
       ALLOCATE(n_var_hyper_active(1:n_integrated))
       n_var_hyper_active = .FALSE.
       ! n_var_hyper_active(nden) = .TRUE.
       !       n_var_hyper_active(neng) = .TRUE.
       n_var_hyper_active = .TRUE.

    END IF

    IF (par_rank.EQ.0) THEN
       PRINT *, 'n_integrated = ',n_integrated 

       PRINT *, 'n_var = ',n_var 
       PRINT *, 'n_var_exact = ',n_var_exact 
       PRINT *, '*******************Variable Names*******************'
       DO i = 1,n_var
          WRITE (*, u_variable_names_fmt) u_variable_names(i)
       END DO
       PRINT *, '****************************************************'
    END IF


  END SUBROUTINE  user_setup_pde


  !
  ! Set the exact solution for comparison to the simulated solution
  !
  ! u          - array to fill in the exact solution
  ! nlocal       - number of active wavelets   
  ! ne_local        - total number of equations
  ! t          - time of current time step 
  ! l_n_var_exact_soln_index - index into the elements of u for which we need to 
  !                            find the exact solution
  SUBROUTINE  user_exact_soln (u, nlocal,  t_local, l_n_var_exact_soln)  
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: nlocal
    REAL (pr), INTENT (IN) ::  t_local
    REAL (pr), DIMENSION (nlocal,n_var_exact), INTENT (INOUT) :: u
    LOGICAL , INTENT (IN) :: l_n_var_exact_soln(n_var)
    INTEGER :: i

  END SUBROUTINE  user_exact_soln

  SUBROUTINE user_initial_conditions (u, nlocal, ne_local, t_local, scl, scl_fltwt,iter)
    USE penalization
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: nlocal, ne_local
    REAL (pr), DIMENSION (nlocal,ne_local), INTENT (INOUT) :: u
    REAL (pr)  :: scl(1:n_var),scl_fltwt
    REAL (pr), INTENT (IN) :: t_local
    INTEGER, INTENT (INOUT) :: iter
    INTEGER :: i, l, ie, ii, k
    INTEGER :: iseedsize, jx, jy, jz, sprt
    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc

    REAL (pr) :: u_max, u_min, v_max, v_min, w_max, w_min
    REAL (pr), DIMENSION(nlocal,1) :: forcing
    !REAL (pr), DIMENSION(nlocal) :: p
    REAL (pr), DIMENSION(nlocal) :: x_rndm
    REAL (pr), DIMENSION(nlocal,2) :: loc_support, Dloc_support

    REAL (pr) :: Re_tau, r_max, nwlt_total,v_mean
    REAL (pr) :: fourier_coeff1, fourier_coeff2,fourier_coeff3,L_x, L_y, L_z, x_mid, y_mid, z_mid
    REAL (pr) :: a_rndm, b_rndm, c_rndm
    REAL (pr) :: y_pos, width
    REAL (pr), DIMENSION(nlocal) :: u_r
    INTEGER, ALLOCATABLE, DIMENSION(:) :: seed
    REAL (pr), DIMENSION(nlocal) :: rnd_prtrb

    INTEGER :: mynwlt_global, avgnwlt, maxnwlt, minnwlt, fullnwlt

    REAL (pr) :: tmp1, tmp2
    LOGICAL, SAVE :: convert_parameters = .TRUE.

    !******************** This section for changing the Mach number ******************
    !tmp1 = MINVAL( (u(:,neng)  - 0.5_pr*SUM(u(:,nvel(1:dim))**2.0_pr,DIM=2)/u(:,nden) )/( u(:,nden)*(cp_in(Nspec)*Ma**2/0.5_pr**2-1.0_pr/MW_in(Nspec)/0.5_pr**2.0_pr)) )
    !tmp2 = MAXVAL( (u(:,neng)  - 0.5_pr*SUM(u(:,nvel(1:dim))**2.0_pr,DIM=2)/u(:,nden) )/( u(:,nden)*(cp_in(Nspec)*Ma**2/0.5_pr**2-1.0_pr/MW_in(Nspec)/0.5_pr**2.0_pr)) )
    !CALL parallel_global_sum( REALMINVAL=tmp1)
    !CALL parallel_global_sum( REALMAXVAL=tmp2)
    !IF(par_rank .EQ. 0 ) PRINT *,  'IC temperature 1:', tmp1, tmp2

    !IF(convert_parameters) THEN
    !   u(:,neng)              = 0.5_pr*SUM(u(:,nvel(1:dim))**2.0_pr,DIM=2)/u(:,nden) + u(:,nden)*(cp_in(Nspec)-1.0_pr/MW_in(Nspec)/Ma**2.0_pr) &
    !               * (u(:,neng)  - 0.5_pr*SUM(u(:,nvel(1:dim))**2.0_pr,DIM=2)/u(:,nden) )/( u(:,nden)*(cp_in(Nspec)*Ma**2/0.5_pr**2-1.0_pr/MW_in(Nspec)/0.5_pr**2.0_pr)) !Tempterature at prev Mach 
    !   PRINT *, 'Mach number updated', par_rank
    !   convert_parameters = .FALSE.
    !END IF

    !tmp1 = MINVAL( (u(:,neng)  - 0.5_pr*SUM(u(:,nvel(1:dim))**2.0_pr,DIM=2)/u(:,nden) )/( u(:,nden)*(cp_in(Nspec)-1.0_pr/MW_in(Nspec)/Ma**2.0_pr)) )
    !tmp2 = MAXVAL( (u(:,neng)  - 0.5_pr*SUM(u(:,nvel(1:dim))**2.0_pr,DIM=2)/u(:,nden) )/( u(:,nden)*(cp_in(Nspec)-1.0_pr/MW_in(Nspec)/Ma**2.0_pr)) )
    !CALL parallel_global_sum( REALMINVAL=tmp1)
    !CALL parallel_global_sum( REALMAXVAL=tmp2)
    !IF(par_rank .EQ. 0 ) PRINT *,  'IC temperature 2:', tmp1, tmp2
    !*********************************************************************************

    IF (IC_restart_mode > 0 ) THEN !in the case of restart
       !do nothing
    ELSE IF (IC_restart_mode .EQ. 0) THEN
       !Set Density
       u(:,nden)              = 1.0_pr

       !Set Background Momentum
       DO i=1,dim
          u(:,nvel(i)) = 0.0_pr
       END DO
       u(:,nvel(1))    = 1.0_pr*(1.0_pr-penal)    !constant flow in x-direction
       WHERE(penal_color > 0) u(:,nvel(1)) = penal*vels(1,penal_color(:))
!!$       call RANDOM_NUMBER(rnd_prtrb)
!!$       IF( dim .GE. 2 ) u(:,nvel(2))    = penal*(rnd_prtrb-0.5_pr)*0.04_pr
!!$       call RANDOM_NUMBER(rnd_prtrb)
!!$       IF( dim .EQ. 3 ) u(:,nvel(3))    = penal*(rnd_prtrb-0.5_pr)*0.04_pr

       IF(j_lev==j_mx)CALL diffsmooth (u(:,nvel(1)),nlocal,1,1,01,0,0.75_pr,0.08_pr,3.0_pr,-2,j_lev,1,.FALSE.)
       IF(j_lev==j_mx .AND. dim .GE. 2 )CALL diffsmooth (u(:,nvel(2)),nlocal,1,1,01,0,0.75_pr,0.08_pr,3.0_pr,-2,j_lev,1,.FALSE.)
       IF(j_lev==j_mx .AND. dim .EQ. 3 )CALL diffsmooth (u(:,nvel(3)),nlocal,1,1,01,0,0.75_pr,0.08_pr,3.0_pr,-2,j_lev,1,.FALSE.)

       !Set Energy for constant temperature
       u(:,neng)              = 0.5_pr*SUM(u(:,nvel(1:dim))**2.0_pr,DIM=2)/u(:,nden) + u(:,nden)*(cp_in(Nspec)-1.0_pr/MW_in(Nspec)/Ma**2.0_pr)*Tconst
       !u(:,neng)              =
       !u(:,nden)*(cp_in(Nspec)-1.0_pr/MW_in(Nspec)/Ma**2.0_pr)*((1.5_pr-1.0_pr)/(xyzlimits(1,2)-xyzlimits(2,2))*(x(:,2)-xyzlimits(1,2))+1.5_pr    )  ! temperature differential
       !u(:,neng)              = 0.5_pr*u(:,nvel(1))**2.0_pr/u(:,nden) + u(:,nden)*(cp_in(Nspec)-1.0_pr/MW_in(Nspec)/Ma**2.0_pr)*Tconst  !couette flow
       !add acoustic perturbation
       ! u(:,nden) = 1.0_pr +1.0e-3_pr*EXP( -LOG(2.0_pr) * ((x(:,1))**2.0_pr + SUM(x(:,2:dim)**2.0_pr,DIM=2)) / 0.004_pr )! gaussian centered on x,y = 0
       ! u(:,neng)              = u(:,neng) + 1.0e-3_pr*EXP( -LOG(2.0_pr) * ((x(:,1))**2.0_pr + SUM(x(:,2:dim)**2.0_pr,DIM=2)) / 0.004_pr )/(MW_in(Nspec)-1.0_pr)
       !Set species
       IF (Nspec>1) u(:,nspc(1)) = 0.0_pr

    END IF


    IF(ALLOCATED(bndvals)) DEALLOCATE(bndvals)
    ALLOCATE(bndvals(ne_local,dim*2))  !integrated variables,number of boundaries

    bndvals(:,:)                  =  0.0_pr 
    bndvals(nden,:)               =  1.0_pr
    bndvals(nvel(1):nvel(dim),:)  =  0.0_pr
    bndvals(nvel(1),:)            =  1.0_pr
    bndvals(neng,:)               =  0.5_pr*SUM(bndvals(nvel(1):nvel(dim),:)**2.0_pr,DIM=1)+bndvals(nden,:)*(cp_in(Nspec)-1.0_pr/MW_in(Nspec)/Ma**2.0_pr)*Tconst






    mynwlt_global = nwlt
    fullnwlt = PRODUCT(mxyz(1:dim)*2**(j_lev-1) + 1 - prd(:))
    CALL parallel_global_sum (INTEGER=mynwlt_global)
    avgnwlt = mynwlt_global/par_size 
    maxnwlt = nwlt
    minnwlt = -nwlt
    CALL parallel_global_sum(INTEGERMAXVAL=maxnwlt)
    CALL parallel_global_sum(INTEGERMAXVAL=minnwlt)
    minnwlt=-minnwlt
    IF (par_rank.EQ.0) THEN
       PRINT *, '**********************************************************'
       PRINT *, 'j_mx   = ', j_mx,           '  twrite  = ', twrite
       PRINT *, 'j_lev  = ', j_lev,          '  n%      = ', REAL(mynwlt_global,pr)/REAL(fullnwlt,pr)
       PRINT *, 'nwlt_g = ', mynwlt_global,  '  n_all   = ', fullnwlt
       PRINT *, 'nwlt_mx= ', maxnwlt,        '  nwlt_av = ', avgnwlt
       PRINT *, 'nwlt_mn= ', minnwlt,        '  p_size  = ', par_size
       PRINT *, '**********************************************************'

    END IF
  END SUBROUTINE user_initial_conditions


  !--********************************  
  ! Arguments
  ! u         - field on adaptive grid
  ! nlocal      - number of active points
  ! ne_local       - number of equations
  ! t         - current time
  !
  ! Global variables used
  ! ifn       - number of equations
  ! x()
  ! xO
  ! y0
  !--******************************** 
  SUBROUTINE user_algebraic_BC (Lu, u, nlocal, ne_local, jlev, meth)
    !--Defines boundary condition type
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: jlev, meth, ne_local, nlocal
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (INOUT) :: Lu
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (IN)    :: u

    INTEGER :: ie, i, ii, shift, denshift,eshift
    INTEGER, DIMENSION (dim) :: velshift 
    REAL (pr), DIMENSION (ne_local,nlocal,dim) :: du, d2u !uncomment if Neuman BC are used, need to call 
    REAL (pr), DIMENSION (1,nlocal,dim) :: du_prev
    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc
    REAL (pr), DIMENSION(dim, 2) :: u_wall

    !shift markers for BC implementation
    denshift  = nlocal*(nden-1)
    DO i = 1,dim
       velshift(i) = nlocal*(nvel(i)-1)
    END DO
    eshift    = nlocal*(neng-1)
    
    u_wall = 0.0
    u_wall(1, 2) = 1.25_pr
    u_wall(1, 1) = 0.75_pr
    !BC for both 2D and 3D cases

    !if Neuman BC are used, need to call 
    CALL c_diff_fast (u, du, d2u, jlev, nlocal, meth, 10, ne_local, 1, ne_local)
    DO ie = 1, ne_local
       shift = nlocal*(ie-1)
       !--Go through all Boundary points that are specified
       i_p_face(0) = 1
       DO i=1,dim
          i_p_face(i) = i_p_face(i-1)*3
       END DO
       DO face_type = 0, 3**dim - 1
          face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
          IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
             CALL get_all_indices_by_face (face_type, jlev, nloc, iloc)
             IF(nloc > 0 ) THEN 
                IF( ABS(face(2)) == 1  ) THEN  !Left  - Full
                   IF(ANY(nvel(1:dim) == ie)) THEN
                      Lu(shift+iloc(1:nloc)) = u(shift+iloc(1:nloc)) - & 
                           u_wall(MINLOC(ABS(nvel(1:dim) - ie), 1), (face(2)+1)/2+1)*u(iloc(1:nloc))
                   ELSE IF (ie == neng) THEN
                      Lu(shift+iloc(1:nloc)) = u(shift+iloc(1:nloc)) - (0.5_pr*SUM(u_wall(1:dim,(face(2)+1)/2+1)**2.0_pr,DIM=1) + &
                           (cp_in(Nspec)-1.0_pr/MW_in(Nspec)/Ma**2.0_pr)*Tconst)*u(iloc(1:nloc))
                   END IF
                END IF
             END IF
          END IF
       END DO
    END DO

  END SUBROUTINE user_algebraic_BC

  SUBROUTINE user_algebraic_BC_diag (Lu_diag, nlocal, ne_local, jlev, meth)
    !--Defines boundary condition type
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: jlev, meth, ne_local, nlocal
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (INOUT) :: Lu_diag

    INTEGER :: ie, i, ii, shift, denshift

    REAL (pr), DIMENSION (nlocal,dim) :: du_diag, d2u_diag  !uncomment if Neuman BC are used
    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc
    REAL (pr), DIMENSION (1,nlocal,dim) :: du_prev, d2u
    INTEGER, PARAMETER :: methprev=1

    !BC for both 2D and 3D cases

    denshift = nlocal*(nden-1)



    !if Neuman BC are used, need to call 
    CALL c_diff_diag ( du_diag, d2u_diag, jlev, nlocal, meth, meth, 10)

    DO ie = 1, ne_local
       shift = nlocal*(ie-1)
       !--Go through all Boundary points that are specified
       i_p_face(0) = 1
       DO i=1,dim
          i_p_face(i) = i_p_face(i-1)*3
       END DO
       DO face_type = 0, 3**dim - 1
          face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
          IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
             CALL get_all_indices_by_face (face_type, jlev, nloc, iloc)
             IF(nloc > 0 ) THEN 
                IF( ABS(face(2)) == 1  ) THEN 
                   IF(ANY(nvel(1:dim) == ie)) THEN
                      Lu_diag(shift+iloc(1:nloc)) = 1.0_pr
                   ELSE IF (ie == neng)  THEN
                      Lu_diag(shift+iloc(1:nloc)) = 1.0_pr
                   END IF
                END IF
             END IF
          END IF
       END DO
    END DO

  END SUBROUTINE user_algebraic_BC_diag

  SUBROUTINE user_algebraic_BC_rhs (rhs, ne_local, nlocal, jlev)
    !--Sets rhs for boundary conditions
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: ne_local, nlocal, jlev
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (INOUT) :: rhs

    INTEGER :: ie, i, ii, shift, denshift
    INTEGER, PARAMETER ::  meth=1
    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc
    REAL (pr), DIMENSION (ne_local,nlocal,dim) :: duall, d2uall
    REAL (pr), DIMENSION (1,nlocal,dim) :: du_prev, du, d2u  
    REAL (pr) :: p_bc

    p_bc = 1.0_pr  !pressure defined for outflow boundary conditions

    denshift=nlocal*(nden-1)



    DO ie = 1, ne_local
       shift = nlocal*(ie-1)
       !--Go through all Boundary points that are specified
       i_p_face(0) = 1
       DO i=1,dim
          i_p_face(i) = i_p_face(i-1)*3
       END DO
       DO face_type = 0, 3**dim - 1
          face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
          IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
             CALL get_all_indices_by_face (face_type, jlev, nloc, iloc)
             IF(nloc > 0 ) THEN 
                IF( ABS(face(2)) == 1  ) THEN              
                   IF(ANY(nvel(1:dim) == ie)) THEN
                      rhs(shift+iloc(1:nloc)) = 0.0_pr
                   ELSE IF (ie == neng)  THEN
                      rhs(shift+iloc(1:nloc)) = 0.0_pr
                   END IF
                END IF
             END IF
          END IF
       END DO
    END DO


  END SUBROUTINE user_algebraic_BC_rhs



  SUBROUTINE user_convectzone_BC_rhs (myrhs_loc, ulc, myne_loc, mynloc, myjl, mymeth)
    IMPLICIT NONE

    !SUBROUTINE only for single species flow
    INTEGER , INTENT (IN) :: myne_loc, mynloc, myjl, mymeth
    REAL (pr), DIMENSION (mynloc*myne_loc), INTENT (INOUT) :: myrhs_loc
    REAL (pr), DIMENSION (mynloc,myne_loc), INTENT (IN) :: ulc

    INTEGER :: ie, i, j, shift
    INTEGER :: mymeth_central, mymeth_backward, mymeth_forward
    REAL (pr), DIMENSION (myne_loc,mynloc,dim) :: du, duF, duB, d2u  !derivatives of native variables
    REAL (pr), DIMENSION (mynloc,2*dim) :: bigU
    !REAL (pr), DIMENSION (mynloc,myne_loc) :: myu_zer
    REAL (pr), DIMENSION(mynloc, dim) :: v_back, v_for, vTemp
    REAL (pr), DIMENSION(mynloc) :: buffzone
    REAL (pr) :: delta_Jmx

    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc

    mymeth_central  = mymeth + BIASING_NONE     !to allow for constant flux testing
    mymeth_backward = mymeth + BIASING_BACKWARD
    mymeth_forward  = mymeth + BIASING_FORWARD

    IF(cnvtBC .AND. .NOT.(upwind_cnvtBC))CALL c_diff_fast(ulc, du, d2u, myjl, mynloc, mymeth_central, 10, myne_loc, 1, myne_loc) 
    IF(diffBC .OR. upwind_cnvtBC) THEN
       CALL c_diff_fast(ulc, duF, d2u, myjl, mynloc, mymeth_forward, 10, myne_loc, 1, myne_loc) 
       CALL c_diff_fast(ulc, duB, d2u, myjl, mynloc, mymeth_backward, 10, myne_loc, 1, myne_loc) 
    END IF
!!$       CALL user_bufferfunc(bigU, mynloc, buffcvd, polyconv)
    CALL user_bufferfunc_eric(bigU, mynloc, buffcvd, polyconv)


    IF(diffBC) THEN  
       buffzone = SUM(bigU(:,:),DIM=2)   !ERIC: increased in the corners
       delta_Jmx = MINVAL(h(1,1:dim)/2.0_pr**(REAL(j_mx,pr) - 1.0_pr))
       DO i=1,dim
          vTemp(1:mynloc,i) = buffzone(1:mynloc) 
       END DO

       CALL shift_by(vTemp, v_for, mynloc, 1)
       v_for(1:mynloc,1:dim) = MAX(vTemp, v_for)
       CALL shift_by(vTemp, v_back, mynloc, -1)
       v_back(1:mynloc,1:dim) = MAX(vTemp, v_back)
    END IF

    DO ie=1,myne_loc
       shift = (ie-1)*mynloc   

       !convective zone terms
       IF(cnvtBC) THEN  !ERIC: adjust sign for upwinding inflow/outflow
          IF(upwind_cnvtBC) THEN  !use upwinding in Freund Zone
             DO j = 1,dim
!!$       myrhs_loc(shift+1:shift+mynloc) = myrhs_loc(shift+1:shift+mynloc) - (bigU(:,2)*c0(1)-bigU(:,1)*c0(2))*buffU0*du(ie,:,1)
                myrhs_loc(shift+1:shift+mynloc) = myrhs_loc(shift+1:shift+mynloc) &
                     - ( - bigU(:,2*j-1)* 0.5_pr*(duF(ie,:,j)*(SIGN(1.0_pr,buff_thick(2*j-1))+1.0_pr )+duB(ie,:,j)*(SIGN(1.0_pr,buff_thick(2*j-1)) -1.0_pr )  ) &
                     + bigU(:,2*j)      * 0.5_pr*(duF(ie,:,j)*(SIGN(1.0_pr,buff_thick(2*j))  -1.0_pr )+duB(ie,:,j)*(SIGN(1.0_pr,buff_thick(2*j))   +1.0_pr )  )     )*buffU0
             END DO
          ELSE   !use central differencing
             DO j = 1,dim
!!$       myrhs_loc(shift+1:shift+mynloc) = myrhs_loc(shift+1:shift+mynloc) - (bigU(:,2)*c0(1)-bigU(:,1)*c0(2))*buffU0*du(ie,:,1)
                myrhs_loc(shift+1:shift+mynloc) = myrhs_loc(shift+1:shift+mynloc) &
                     - ( - SIGN(1.0_pr,buff_thick(2*j-1))*bigU(:,2*j-1) + SIGN(1.0_pr,buff_thick(2*j))*bigU(:,2*j) )*buffU0*du(ie,:,j) 
             END DO
          END IF
       END IF
       IF(diffBC) THEN !include diffusion in Freund Zone 
          !--Go through all Boundary points that are specified
          i_p_face(0) = 1
          DO i=1,dim
             i_p_face(i) = i_p_face(i-1)*3
          END DO
          DO face_type = 0, 3**dim - 1
             face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
             CALL get_all_indices_by_face (face_type, myjl, nloc, iloc)
             IF(nloc > 0 ) THEN 
                DO i = 1, dim
                   myrhs_loc(shift+iloc(1:nloc)) = myrhs_loc(shift+iloc(1:nloc)) + (1.0_pr-ABS( REAL(face(i),pr) ))*buff_visc&
                        *MAX(delta_Jmx/Ma,1.0_pr)*(v_for(iloc(1:nloc),i)*duF(ie,iloc(1:nloc),i) - v_back(iloc(1:nloc),i)*duB(ie,iloc(1:nloc),i) )  
                END DO
             END IF
          END DO
       END IF
    END DO

  END SUBROUTINE user_convectzone_BC_rhs

  SUBROUTINE user_convectzone_BC_Drhs (Dmyrhs_loc, ulc, ulc_prev, myne_loc, mynloc, myjl, mymeth)
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: myne_loc, mynloc, myjl, mymeth
    REAL (pr), DIMENSION (mynloc*myne_loc), INTENT (INOUT) :: Dmyrhs_loc
    REAL (pr), DIMENSION (mynloc,myne_loc), INTENT (IN) :: ulc, ulc_prev

    INTEGER :: ie, i, j, shift
    INTEGER :: mymeth_central, mymeth_backward, mymeth_forward
    REAL (pr), DIMENSION (myne_loc,mynloc,dim) :: du, duF, duB, d2u
    REAL (pr), DIMENSION (mynloc,2*dim) :: bigU
    REAL (pr), DIMENSION(mynloc) :: buffzone
    REAL (pr) :: delta_Jmx
    REAL (pr), DIMENSION(mynloc, dim) :: v_back, v_for, vTemp

    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc


    mymeth_central  = mymeth + BIASING_NONE     !to allow for constant flux testing
    mymeth_backward = mymeth + BIASING_BACKWARD
    mymeth_forward  = mymeth + BIASING_FORWARD

    IF(cnvtBC .AND. .NOT.(upwind_cnvtBC))CALL c_diff_fast(ulc, du, d2u, myjl, mynloc, mymeth_central, 10, myne_loc, 1, myne_loc) 
    IF(diffBC .OR. upwind_cnvtBC) THEN
       CALL c_diff_fast(ulc, duF, d2u, myjl, mynloc, mymeth_forward, 10, myne_loc, 1, myne_loc) 
       CALL c_diff_fast(ulc, duB, d2u, myjl, mynloc, mymeth_backward, 10, myne_loc, 1, myne_loc) 
    END IF
!!$       CALL user_bufferfunc(bigU, mynloc, buffcvd, polyconv)
    CALL user_bufferfunc_eric(bigU, mynloc, buffcvd, polyconv)


    IF(diffBC) THEN  
       buffzone = SUM(bigU(:,:),DIM=2)   !ERIC: increased in the corners
       delta_Jmx = MINVAL(h(1,1:dim)/2.0_pr**(REAL(j_mx,pr) - 1.0_pr))

       DO i=1,dim
          vTemp(1:mynloc,i) = buffzone(1:mynloc) 
       END DO

       CALL shift_by(vTemp, v_for, mynloc, 1)
       v_for(1:mynloc,1:dim) = MAX(vTemp, v_for)
       CALL shift_by(vTemp, v_back, mynloc, -1)
       v_back(1:mynloc,1:dim) = MAX(vTemp, v_back)
    END IF


    DO ie=1,myne_loc
       shift = (ie-1)*mynloc   

       !convective zone terms
       IF(cnvtBC) THEN
          IF(upwind_cnvtBC) THEN  !use upwinding in Freund Zone
             DO j = 1,dim
                Dmyrhs_loc(shift+1:shift+mynloc) = Dmyrhs_loc(shift+1:shift+mynloc)&
                     - ( - bigU(:,2*j-1)* 0.5_pr*(duF(ie,:,j)*(SIGN(1.0_pr,buff_thick(2*j-1))+1.0_pr  )+duB(ie,:,j)*(SIGN(1.0_pr,buff_thick(2*j-1)) -1.0_pr )   )    &
                     + bigU(:,2*j)*  0.5_pr*(duF(ie,:,j)*(SIGN(1.0_pr,buff_thick(2*j))-1.0_pr  )+duB(ie,:,j)*(SIGN(1.0_pr,buff_thick(2*j)) +1.0_pr )   )  )*buffU0 
             END DO
          ELSE
             DO j = 1,dim
!!$          Dmyrhs_loc(shift+1:shift+mynloc) = Dmyrhs_loc(shift+1:shift+mynloc) - (bigU(:,2)*c0(1)-bigU(:,1)*c0(2))*buffU0*du(ie,:,1)
                Dmyrhs_loc(shift+1:shift+mynloc) = Dmyrhs_loc(shift+1:shift+mynloc)&
                     - ( - SIGN(1.0_pr,buff_thick(2*j-1))*bigU(:,2*j-1) + SIGN(1.0_pr,buff_thick(2*j))*bigU(:,2*j) )*buffU0*du(ie,:,j)   
             END DO
          END IF
       END IF

       IF(diffBC) THEN !include diffusion in Freund Zone 
          !--Go through all Boundary points that are specified
          i_p_face(0) = 1
          DO i=1,dim
             i_p_face(i) = i_p_face(i-1)*3
          END DO
          DO face_type = 0, 3**dim - 1
             face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
             CALL get_all_indices_by_face (face_type, myjl, nloc, iloc)
             IF(nloc > 0 ) THEN 
                DO i = 1, dim
                   Dmyrhs_loc(shift+iloc(1:nloc)) = Dmyrhs_loc(shift+iloc(1:nloc)) + (1.0_pr-ABS( REAL(face(i),pr) )) *buff_visc&
                        *MAX(delta_Jmx/Ma,1.0_pr)*(v_for(iloc(1:nloc),i)*duF(ie,iloc(1:nloc),i) - v_back(iloc(1:nloc),i)*duB(ie,iloc(1:nloc),i) )
                END DO
             END IF
          END DO
       END IF
    END DO


  END SUBROUTINE user_convectzone_BC_Drhs



  SUBROUTINE user_convectzone_BC_Drhs_diag (Drhs_diag_local, ulc_prev, myne_loc, mynloc, myjl, mymeth)
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: myne_loc, mynloc, myjl, mymeth
    REAL (pr), DIMENSION (mynloc*myne_loc), INTENT (INOUT) :: Drhs_diag_local
    REAL (pr), DIMENSION (mynloc,myne_loc), INTENT (IN) :: ulc_prev

    REAL (pr), DIMENSION (mynloc,dim) :: du_diag_local, du_diag_localF, du_diag_localB, d2u_diag_local
    INTEGER :: ie, i,j, shift
    INTEGER :: mymeth_central, mymeth_backward, mymeth_forward
    REAL (pr), DIMENSION (mynloc,2*dim) :: bigU
    REAL (pr), DIMENSION(mynloc) :: buffzone
    REAL (pr) :: delta_Jmx
    REAL (pr), DIMENSION(mynloc, dim) :: v_back, v_for, vTemp

    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc

    mymeth_central  = mymeth + BIASING_NONE     !to allow for constant flux testing
    mymeth_backward = mymeth + BIASING_BACKWARD
    mymeth_forward  = mymeth + BIASING_FORWARD

    IF(cnvtBC .AND. .NOT.(upwind_cnvtBC)) CALL c_diff_diag(du_diag_local, d2u_diag_local, myjl, mynloc, mymeth_central, mymeth, 10)
    IF(diffBC .OR. upwind_cnvtBC) THEN
       CALL c_diff_diag(du_diag_localF, d2u_diag_local, myjl, mynloc, mymeth_forward, mymeth_forward, 10)
       CALL c_diff_diag(du_diag_localB, d2u_diag_local, myjl, mynloc, mymeth_backward, mymeth_backward, 10)
    END IF
!!$       CALL user_bufferfunc(bigU, mynloc, buffcvd, polyconv)
    CALL user_bufferfunc_eric(bigU, mynloc, buffcvd, polyconv)


    IF(diffBC) THEN  
       buffzone = SUM(bigU(:,:),DIM=2)   !ERIC: increased in the corners
       delta_Jmx = MINVAL(h(1,1:dim)/2.0_pr**(REAL(j_mx,pr) - 1.0_pr))
       DO i=1,dim
          vTemp(1:mynloc,i) = buffzone(1:mynloc)
       END DO

       CALL shift_by(vTemp, v_for, mynloc, 1)
       v_for(1:mynloc,1:dim) = MAX(vTemp, v_for)
       CALL shift_by(vTemp, v_back, mynloc, -1)
       v_back(1:mynloc,1:dim) = MAX(vTemp, v_back)
    END IF




    DO ie=1,myne_loc
       shift = (ie-1)*mynloc   
       !convective zone terms
       IF(cnvtBC) THEN
          IF(upwind_cnvtBC) THEN  !use upwinding in Freund Zone
             DO j = 1,dim
                Drhs_diag_local(shift+1:shift+mynloc) = Drhs_diag_local(shift+1:shift+mynloc) &
                     - ( - bigU(:,2*j-1)* 0.5_pr*(du_diag_localF(:,j)*(SIGN(1.0_pr,buff_thick(2*j-1))+1.0_pr )+du_diag_localB(:,j)*(SIGN(1.0_pr,buff_thick(2*j-1)) -1.0_pr )   ) &
                     + bigU(:,2*j)      * 0.5_pr*(du_diag_localF(:,j)*(SIGN(1.0_pr,buff_thick(2*j))  -1.0_pr )+du_diag_localB(:,j)*(SIGN(1.0_pr,buff_thick(2*j))   +1.0_pr )   )   )*buffU0
             END DO
          ELSE
             DO j = 1,dim
!!$                Drhs_diag_local(shift+1:shift+mynloc) = Drhs_diag_local(shift+1:shift+mynloc) - (bigU(:,2)*c0(1)-bigU(:,1)*c0(2))*buffU0*du_diag_local(:,1)
                Drhs_diag_local(shift+1:shift+mynloc) = Drhs_diag_local(shift+1:shift+mynloc) &
                     - ( - SIGN(1.0_pr,buff_thick(2*j-1))*bigU(:,2*j-1) + SIGN(1.0_pr,buff_thick(2*j))*bigU(:,2*j) )*buffU0*du_diag_local(:,j) 
             END DO
          END IF
       END IF
       IF(diffBC) THEN
          !--Go through all Boundary points that are specified
          i_p_face(0) = 1
          DO i=1,dim
             i_p_face(i) = i_p_face(i-1)*3
          END DO
          DO face_type = 0, 3**dim - 1
             face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
             CALL get_all_indices_by_face (face_type, myjl, nloc, iloc)
             IF(nloc > 0 ) THEN 
                DO i = 1, dim
                   Drhs_diag_local(shift+iloc(1:nloc)) = Drhs_diag_local(shift+iloc(1:nloc)) + (1.0_pr-ABS( REAL(face(i),pr) )) *buff_visc &
                        *MAX(delta_Jmx/Ma,1.0_pr)*(v_for(iloc(1:nloc),i)*du_diag_localF(iloc(1:nloc),i) - v_back(iloc(1:nloc),i)*du_diag_localB(iloc(1:nloc),i) )
                END DO
             END IF
          END DO
       END IF
    END DO
  END SUBROUTINE user_convectzone_BC_Drhs_diag

  SUBROUTINE user_bufferfunc_eric (bigU, mynloc, buffd,locpoly)
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: mynloc
    REAL (pr), DIMENSION (mynloc,2*dim), INTENT(INOUT) :: bigU
    REAL (pr), DIMENSION (5), INTENT(IN) :: buffd
    LOGICAL, INTENT(IN) :: locpoly

    REAL (pr), DIMENSION (mynloc,2*dim) :: bigU2
    INTEGER :: ie, i, j, shift, stri, stpi
    REAL (pr) :: slope, offset
    REAL (pr) :: delta_Jmx 
    REAL (pr) :: penal_obs 

    !    penal_obs = 0.0_pr 
    !    IF(imask_obstacle) penal_obs = 1.0_pr    !ERIC: ? 
    bigU(:,:)=0.0_pr
    bigU2(:,:)=0.0_pr
    delta_Jmx = MINVAL(h(1,1:dim)/2.0_pr**(REAL(j_mx,pr) - 1.0_pr))
    !Establish buffer functions for each direction - boundary order for bigU: xmin, xmax, ymin, ymax, zmin, zmax

    !slope = (ATANH( (0.5_pr-10.0_pr**-3.0_pr)/0.5_pr)- ATANH( (-0.5_pr+10.0_pr**-3.0_pr)/0.5_pr))/(buff_thick) !same algorithm as for volume penalization
    DO j = 1,dim
       IF(ABS(buff_thick(2*j-1)) .LE. delta_Jmx/2.0_pr ) THEN 
          bigU(:,2*j-1) = 0.0_pr
       ELSE
          slope  =  ( ATANH(  (10.0_pr**-1.85_pr - 0.5_pr  )/0.5_pr)- ATANH(0.49_pr/0.5_pr)    )/-ABS(buff_thick(2*j-1)) 
          offset =  ATANH(0.49_pr/0.5_pr)/slope !offste distance
          bigU(:,2*j-1) = MAX(0.5_pr - 10.0_pr**-1.85_pr - 0.5_pr*TANH(slope*( x(:,j) - (xyzlimits(1,j) + offset) )) , 0.0_pr )
       END IF

       IF(ABS(buff_thick(2*j)) .LE. delta_Jmx/2.0_pr )  THEN 
          bigU(:,2*j) = 0.0_pr
       ELSE
          slope  =  ( ATANH(  (10.0_pr**-1.85_pr - 0.5_pr  )/0.5_pr)- ATANH(0.49_pr/0.5_pr)    )/-ABS(buff_thick(2*j))  
          offset =  ATANH(0.49_pr/0.5_pr)/slope !offste distance
          bigU(:,2*j) = MAX(0.5_pr - 10.0_pr**-1.85_pr + 0.5_pr*TANH(slope*( x(:,j) - (xyzlimits(2,j) - offset) )) , 0.0_pr )
       END IF
    END DO


  END SUBROUTINE user_bufferfunc_eric


  SUBROUTINE user_bndfunc (bndonly, mynloc, myjl)
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: mynloc, myjl
    REAL (pr), DIMENSION (mynloc), INTENT(INOUT) :: bndonly
    INTEGER :: i
    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc
    bndonly(:) = 0.0_pr
    i_p_face(0) = 1
    DO i=1,dim
       i_p_face(i) = i_p_face(i-1)*3
    END DO
    DO face_type = 0, 3**dim - 1
       face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
       IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
          CALL get_all_indices_by_face (face_type, myjl, nloc, iloc)
          IF(nloc > 0 ) THEN
             IF (ABS(face(1)) == 1) THEN
                bndonly(iloc(1:nloc)) = 1.0_pr
             END IF
          END IF
       END IF
    END DO
  END SUBROUTINE user_bndfunc

  SUBROUTINE user_flxfunc (flxfunc, mynloc, myjl)
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: mynloc, myjl
    REAL (pr), DIMENSION (dim,mynloc,dim), INTENT(INOUT) :: flxfunc
    REAL (pr), DIMENSION (mynloc) :: myflxfunc
    INTEGER :: i,j
    CALL user_bndfunc(myflxfunc, mynloc, myjl)  !puts 1.0 on boundaries, 0.0 elsewhere 
    DO j=1,dim
       DO i=1,dim
          !IF ((i.ne.j) .AND. (MIN(i,j).eq.1)) THEN
          IF ((i.ne.j) .AND. (j.eq.1)) THEN

             flxfunc(i,:,j) = 1.0_pr - myflxfunc(:)
          ELSE
             flxfunc(i,:,j) = 1.0_pr
          END IF
       END DO
    END DO
  END SUBROUTINE user_flxfunc



  FUNCTION user_chi (nlocal, t_local)
    USE parallel
    USE threedobject
    IMPLICIT NONE
    INTEGER,          INTENT (IN) :: nlocal
    REAL (pr),        INTENT (IN) :: t_local
    REAL (pr), DIMENSION (nlocal) :: user_chi
    REAL (pr), DIMENSION (nlocal) :: dist_loc
    REAL(pr), DIMENSION(nlocal,1) :: forcing
    REAL(pr) :: smooth_coeff

    user_chi = 0.0_pr
    IF(read_geometry) THEN
       CALL CADtest(nlocal,.FALSE.,user_chi)
    ELSE

       CALL user_dist (nlocal, t, DISTANCE=dist_loc)   !distance function from main code
       !PRINT *, MAXVAL(dist_loc),MINVAL(dist_loc)
       WHERE (   dist_loc .GE. 0.0_pr) 
          user_chi = 1.0_pr
       END WHERE

    END IF

!!$square obstacle
!!$   user_chi = 0.5_pr*(tanh((x(:,1)-0.4_pr))-tanh((x(:,1)-0.6_pr))) &
!!$              * 0.5_pr*(tanh((x(:,2)-0.4_pr))-tanh((x(:,2)-0.6_pr))) 


  END FUNCTION user_chi

  SUBROUTINE user_bufferzone_BC_rhs (myrhs_loc, ulc, myne_loc, mynloc, myjl, mymeth)
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: myne_loc, mynloc, myjl, mymeth
    REAL (pr), DIMENSION (mynloc*myne_loc), INTENT (INOUT) :: myrhs_loc
    REAL (pr), DIMENSION (mynloc,myne_loc), INTENT (IN) :: ulc

    INTEGER :: ie, i, j, shift, stri, stpi
    REAL (pr), DIMENSION (mynloc,2*dim) :: bigU

    CALL user_bufferfunc_eric(bigU, mynloc, buffbfd, polybuff)

    DO j = 1,dim
       DO ie=1,myne_loc
          shift = (ie-1)*mynloc   
          myrhs_loc(shift+1:shift+mynloc) = myrhs_loc(shift+1:shift+mynloc) - buffSig*((bigU(:,2*j)+bigU(:,2*j-1))*ulc(:,ie)-bigU(:,2*j-1)*bndvals(ie,2*j-1)-bigU(:,2*j)*bndvals(ie,2*j)) 
       END DO
    END DO



  END SUBROUTINE user_bufferzone_BC_rhs

  SUBROUTINE user_bufferzone_BC_Drhs (Dmyrhs_loc, ulc, ulc_prev, myne_loc, mynloc, myjl, mymeth)
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: myne_loc, mynloc, myjl, mymeth
    REAL (pr), DIMENSION (mynloc*myne_loc), INTENT (INOUT) :: Dmyrhs_loc
    REAL (pr), DIMENSION (mynloc,myne_loc), INTENT (IN) :: ulc, ulc_prev

    INTEGER :: ie, i, j, shift, stri, stpi
    REAL (pr), DIMENSION (mynloc,2*dim) :: bigU

    CALL user_bufferfunc_eric(bigU, mynloc, buffbfd, polybuff)

    DO j = 1,dim
       DO ie = 1,myne_loc
          shift = (ie-1)*mynloc   
          Dmyrhs_loc(shift+1:shift+mynloc) = Dmyrhs_loc(shift+1:shift+mynloc) - buffSig*(bigU(:,2*j)+bigU(:,2*j-1))*ulc(:,ie)
       END DO
    END DO

  END SUBROUTINE user_bufferzone_BC_Drhs

  SUBROUTINE user_bufferzone_BC_Drhs_diag (Drhs_diag_local, ulc_prev, myne_loc, mynloc, myjl, mymeth)
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: myne_loc, mynloc, myjl, mymeth
    REAL (pr), DIMENSION (mynloc*myne_loc), INTENT (INOUT) :: Drhs_diag_local
    REAL (pr), DIMENSION (mynloc,myne_loc), INTENT (IN) :: ulc_prev

    INTEGER :: ie, i, j, shift, stri, stpi
    REAL (pr), DIMENSION (mynloc,2*dim) :: bigU

    CALL user_bufferfunc_eric(bigU, mynloc, buffbfd, polybuff)

    DO j = 1,dim
       DO ie=1,myne_loc
          shift = (ie-1)*mynloc   
          Drhs_diag_local(shift+1:shift+mynloc) = Drhs_diag_local(shift+1:shift+mynloc) - buffSig*(bigU(:,2*j)+bigU(:,2*j-1))
       END DO
    END DO
  END SUBROUTINE user_bufferzone_BC_Drhs_diag

  SUBROUTINE internal_rhs (int_rhs,u_integrated,doBC,addingon)
    USE penalization
    IMPLICIT NONE
    REAL (pr), DIMENSION (ng,ne), INTENT(IN) :: u_integrated !1D flat version of integrated variables without BC 
    REAL (pr), DIMENSION (ng*ne), INTENT(INOUT) :: int_rhs
    INTEGER, INTENT(IN) :: doBC !if zero do full rhs, if one no x-derivatives 
    INTEGER, INTENT(IN) :: addingon 

    INTEGER :: i, j, l, idim, ie, shift,  tempintd, jshift
    INTEGER, PARAMETER :: meth=1
    REAL (pr), DIMENSION (ne*dim,ng,dim) :: du, d2u
    REAL (pr), DIMENSION (ne*dim,ng,dim) :: dudum, d2udum
    REAL (pr), DIMENSION (ne*dim,ng,dim) :: duC, d2uC,duF, d2uF,duB, d2uB,du_upwind, d2u_upwind    !forward and backwards differencing ERIC: apply to derivatives in interior domain
    REAL (pr), DIMENSION (ng,ne*dim) :: F
    REAL (pr), DIMENSION (ng,dim+Nspec) :: v ! velocity components + temperature + Y
    REAL (pr) :: usej
    REAL (pr), DIMENSION (ng) :: p
    REAL (pr), DIMENSION (ng,3) :: props

    REAL (pr) , DIMENSION(ng) :: dist_loc
    REAL (pr) , DIMENSION(ng,dim) :: obs_norm
    REAL (pr) , DIMENSION(ng) :: visc_loc
    REAL (pr) :: min_h
    REAL (pr) , DIMENSION(2,ng,dim) :: bias_mask
    INTEGER :: meth_central,meth_backward,meth_forward

    REAL (pr) :: diff_max,diff_min


    meth_central  = meth + BIASING_NONE
    meth_backward = meth + BIASING_BACKWARD
    meth_forward  = meth + BIASING_FORWARD

    min_h = MINVAL(h(1,1:dim)/2**(j_mx - 1))

    int_rhs = 0.0_pr
    IF (Nspec>1) THEN
       DO l = 1,Nspecm
          v(:,dim+1+l) = u_integrated(:,nspc(l))/u_integrated(:,nden)
       END DO
    END IF
    p(:) = 1.0_pr
    IF (Nspec>1)  p(:) = (1.0_pr-SUM(v(:,dim+2:dim+Nspec),DIM=2))  !Y_Nspec
    props(:,1) = p(:)*cp_in(Nspec) !cp_Nspec
    props(:,2) = p(:)/MW_in(Nspec)/Ma**2.0_pr !R_Nspec
    DO l=1,Nspecm
       props(:,1) = props(:,1) + v(:,dim+1+l)*cp_in(l) !cp
       props(:,2) = props(:,2) + v(:,dim+1+l)/MW_in(l)/Ma**2.0_pr !R
    END DO
    p(:) = (u_integrated(:,neng)-0.5_pr*SUM(u_integrated(:,nvel(1):nvel(dim))**2,DIM=2)/u_integrated(:,nden))/(props(:,1)/props(:,2)-1.0_pr)  !pressure
    v(:,dim+1) = p(:)/props(:,2)/u_integrated(:,nden)  !Temperature

    DO i = 1, dim
       v(:,i) = u_integrated(:,nvel(i)) / u_integrated(:,nden) ! u_i
    END DO

    !
    !--Form right hand side of Euler Equations
    !
    F(:,:) = 0.0_pr
    DO j = 1+doBC, dim  !iterate across dimensions
       jshift=ne*(j-1)
       F(:,nden+jshift) = F(:,nden+jshift) + u_integrated(:,nvel(j)) ! rho*u_j
       DO i = 1, dim
          F(:,nvel(i)+jshift) = F(:,nvel(i)+jshift) + u_integrated(:,nvel(i))*v(:,j) ! rho*u_i*u_j
       END DO
       F(:,nvel(j)+jshift) = F(:,nvel(j)+jshift) + p(:) ! rho*u_i^2+p
       F(:,neng+jshift) = F(:,neng+jshift) + ( u_integrated(:,neng) + p(:) )*v(:,j) ! (rho*e+p)*u_j
       DO l=1,Nspecm
          F(:,nspc(l)+jshift) = F(:,nspc(l)+jshift) + v(:,dim+1+l)*u_integrated(:,nvel(j)) ! rho*Y*u_j
       END DO
    END DO
    IF ((doBC .eq. 0) .AND. ((methpd .ne. 0) .OR. splitFBpress )) THEN  !IF doBC>0, these terms are done inside LODI subroutines,  ******problem location?
       F(:,nvel(1)) = F(:,nvel(1)) - p(:)
       F(:,neng)    = F(:,neng)    - p(:)*v(:,1)  
    END IF



    !
    !-- Form right hand side of Navier-Stokes Equations
    !
    IF(NS) THEN 

       CALL c_diff_fast(v, du(1:dim+Nspec,:,:), d2u(1:dim+Nspec,:,:), j_lev, ng, meth_central, 10, dim+Nspec, 1,dim+Nspec) ! du_i/dx_j & dT/dx_j & dY_l/dx_j

       p(:)=1.0_pr
       IF (Nspec>1) p(:)=p(:)-SUM(v(:,dim+2:dim+Nspec),DIM=2)
       !props(:,1)=p(:)*mu_in(Nspec)  !mu_Nspec
       props(:,1)=p(:)*mu_in(Nspec) * (1.0_pr+S1)/(v(:,dim+1) +S1)*(v(:,dim+1))**(1.5_pr)  !mu_Nspec - variable viscosity
       !props(:,2)=p(:)*kk_in(Nspec)  !kk_Nspec
       props(:,2)=p(:)*kk_in(Nspec) * (1.0_pr+S1)/(v(:,dim+1) +S1)*(v(:,dim+1))**(1.5_pr)   !kk_Nspec
       props(:,3)=p(:)*bD_in(Nspec)  !bD_Nspec
       IF (Nspec>1) THEN
          DO l=1,Nspecm
             props(:,1)=props(:,1)+v(:,dim+1+l)*mu_in(l) * (1.0_pr+S1)/(v(:,dim+1) +S1)*(v(:,dim+1))**(1.5_pr)  !mu
             props(:,2)=props(:,2)+v(:,dim+1+l)*kk_in(l) * (1.0_pr+S1)/(v(:,dim+1) +S1)*(v(:,dim+1))**(1.5_pr)  !kk
             props(:,3)=props(:,3)+v(:,dim+1+l)*bD_in(l)  !bD
          END DO
       END IF
       p(:) = du(1,:,1) !reuse of a variable
       DO i = 2, dim
          p(:) = p(:) + du(i,:,i) !du_i/dx_i
       END DO

       d2u(:,:,:) = 1.0_pr
       DO j = 1, dim
          jshift=ne*(j-1)
          DO i = 1, dim
             F(:,nvel(i)+jshift) = F(:,nvel(i)+jshift) - props(:,1)*( du(i,:,j)+du(j,:,i) ) * d2u(i,:,j)  !rho*u_i*u_j - 2*mu*S_ij/Re  
          END DO
          F(:,nvel(j)+jshift) = F(:,nvel(j)+jshift) + 2.0_pr/3.0_pr*props(:,1)*p(:) * d2u(j,:,j)  !rho*u_i*u_j - tau_ij/Re  [ tau_ij = 2*mu*( S_ij - 2/3*S_kk*delta_ij ) ] 
          DO i = 1, dim
             F(:,neng+jshift) =  F(:,neng+jshift) - props(:,1)*( du(i,:,j)+du(j,:,i) )*v(:,i) * d2u(i,:,j)  !(e+p)*u_j - u_i*2*mu*S_ij/Re 
          END DO
          F(:,neng+jshift) =  F(:,neng+jshift) + 2.0_pr/3.0_pr*props(:,1)*p(:)*v(:,j) * d2u(j,:,j) - props(:,2)*du(dim+1,:,j) * d2u(2,:,j)  
          !(e+p)*u_j - u_i*tau_ij/Re - q_j/Re/Pra [ q_j = mu dT/dx_j ]
          IF (Nspec>1) THEN
             DO l=1,Nspecm
                F(:,neng+jshift) =  F(:,neng+jshift) - u_integrated(:,nden)*cp_in(l)*v(:,dim+1)*props(:,3)*du(dim+1+l,:,j) * d2u(2,:,j)
             END DO
             F(:,neng+jshift) =  F(:,neng+jshift) +  u_integrated(:,nden)*cp_in(Nspec)*v(:,dim+1)*props(:,3)*SUM(du(dim+2:dim+Nspec,:,j),DIM=1) * d2u(2,:,j)
             DO l=1,Nspecm
                F(:,nspc(l)+jshift) =  F(:,nspc(l)+jshift) - u_integrated(:,nden)*props(:,3)*du(dim+1+l,:,j) * d2u(2,:,j)
             END DO
          END IF
       END DO
    END IF
    IF (GRAV) THEN    
       shift = (neng-1)*ng   
       DO j=1,dim
          int_rhs(shift+1:shift+ng) = int_rhs(shift+1:shift+ng) - u_integrated(:,nvel(j))*gr_in(Nspec*j)
       END DO
    END IF
    tempintd=ne*dim

    CALL c_diff_fast(F(:,1:tempintd), du(1:tempintd,:,:), d2u(1:tempintd,:,:), j_lev, ng, meth_central, 10, tempintd, 1, tempintd)  !du(i,:,k)=dF_ij/dx_k

    DO j = 1, dim
       jshift=ne*(j-1)
       DO i = 1, dim+1+Nspec
          shift=(i-1)*ng


          !IF (obstacle == .TRUE. .AND. (i == nden .OR. i == neng)) THEN  !add brinkman term to the continuity equation
          int_rhs(shift+1:shift+ng) = int_rhs(shift+1:shift+ng) - du(i+jshift,:,j)  ! -dF_ij/dx_j 
       END DO
       IF (GRAV) THEN      
          shift = (nvel(j)-1)*ng
          int_rhs(shift+1:shift+ng) = int_rhs(shift+1:shift+ng) - gr_in(Nspec*j)*u_integrated(:,nden)
       END IF
    END DO

  END SUBROUTINE internal_rhs

  SUBROUTINE internal_Drhs (int_Drhs, u_p, u_prev, meth,doBC,addingon)
    USE penalization
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: meth
    REAL (pr), DIMENSION (ng,ne), INTENT(IN) :: u_p, u_prev
    REAL (pr), DIMENSION (ng*ne), INTENT(INOUT) :: int_Drhs
    INTEGER, INTENT(IN) :: doBC !if zero do full rhs, if one no x-derivatives 
    INTEGER, INTENT(IN) :: addingon 

    INTEGER :: i, j, l, idim, ie, shift,  vshift, jshift, tempintd
    REAL (pr), DIMENSION (ne*(dim+Nspec),ng,dim) :: du, d2u
    REAL (pr), DIMENSION (ne*(dim+Nspec),ng,dim) :: dudum, d2udum
    REAL (pr), DIMENSION (ne*(dim+Nspec),ng,dim) :: duC, d2uC,duF, d2uF,duB, d2uB,du_upwind, d2u_upwind    !forward and backwards differencing
    REAL (pr), DIMENSION (ng,ne*dim) :: F
    REAL (pr), DIMENSION (ng,(dim+Nspec)*2) :: v_prev ! velocity components + temperature
    REAL (pr), DIMENSION (ng) :: p, p_prev
    REAL (pr), DIMENSION (ng,3) :: props, props_prev

    REAL (pr) , DIMENSION(ng) :: dist_loc
    REAL (pr) , DIMENSION(ng,dim) :: obs_norm
    REAL (pr) :: min_h
    REAL (pr) , DIMENSION(ng) :: visc_loc
    INTEGER :: meth_central,meth_backward,meth_forward
    REAL (pr) :: diff_max,diff_min
    REAL (pr) , DIMENSION(2,ng,dim) :: bias_mask


    meth_central  = meth + BIASING_NONE
    meth_backward = meth + BIASING_BACKWARD
    meth_forward  = meth + BIASING_FORWARD

    min_h = MINVAL(h(1,1:dim)/2**(j_mx - 1))

    int_Drhs = 0.0_pr
    vshift = dim+Nspec
    DO l=1,Nspecm
       v_prev(:,dim+1+l) = u_prev(:,nspc(l))/u_prev(:,nden)
       v_prev(:,vshift+dim+1+l) = u_p(:,nspc(l))/u_prev(:,nden) - u_prev(:,nspc(l))*u_p(:,nden)/u_prev(:,nden)**2
    END DO
    p_prev(:) = 1.0_pr
    p(:) = 0.0_pr
    IF (Nspec>1) p_prev(:) = (p_prev(:)-SUM(v_prev(:,dim+2:dim+Nspec),DIM=2))  !Y0_Nspec
    IF (Nspec>1) p(:)      =           -SUM(v_prev(:,vshift+dim+2:vshift+dim+Nspec),DIM=2)  !Y'_Nspec
    props_prev(:,1) = p_prev(:)*cp_in(Nspec) !cp0_Nspec
    props_prev(:,2) = p_prev(:)/MW_in(Nspec)/Ma**2.0_pr !R0_Nspec
    props(:,1)      = p(:)*cp_in(Nspec) !cp'_Nspec
    props(:,2)      = p(:)/MW_in(Nspec)/Ma**2.0_pr !R'_Nspec
    DO l=1,Nspecm
       props_prev(:,1) = props_prev(:,1) + v_prev(:,dim+1+l)*cp_in(l) !cp0
       props_prev(:,2) = props_prev(:,2) + v_prev(:,dim+1+l)/MW_in(l)/Ma**2.0_pr !R0
       props(:,1)      = props(:,1)      + v_prev(:,vshift+dim+1+l)*cp_in(l) !cp'
       props(:,2)      = props(:,2)      + v_prev(:,vshift+dim+1+l)/MW_in(l)/2.0_pr !R'
    END DO
    p_prev(:) = (u_prev(:,neng)-0.5_pr*SUM(u_prev(:,nvel(1):nvel(dim))**2,DIM=2)/u_prev(:,nden))/(props_prev(:,1)/props_prev(:,2)-1.0_pr)  !pressure
    p(:) = (u_prev(:,neng)-0.5_pr*SUM(u_prev(:,nvel(1):nvel(dim))**2,DIM=2)/u_prev(:,nden)) * (props_prev(:,1)*props(:,2)-props(:,1)*props_prev(:,2)) / (props_prev(:,1)-props_prev(:,2))**2 + &
         (u_p(:,neng)+0.5_pr*SUM(u_prev(:,nvel(1):nvel(dim))**2,DIM=2)/u_prev(:,nden)**2*u_p(:,nden)-SUM(u_prev(:,nvel(1):nvel(dim))*u_p(:,nvel(1):nvel(dim)),DIM=2)/u_prev(:,nden)) /  &
         (props_prev(:,1)/props_prev(:,2)-1.0_pr) 
    v_prev(:,dim+1) = p_prev(:)/props_prev(:,2)/u_prev(:,nden)  !Temperature
    v_prev(:,vshift+dim+1) = (p(:)-p_prev(:)*(props(:,2)/props_prev(:,2)+u_p(:,nden)/u_prev(:,nden)))/props_prev(:,2)/u_prev(:,nden)  !Temperature 

    DO i = 1, dim
       v_prev(:,i) = u_prev(:,nvel(i)) / u_prev(:,nden) ! u_i
       v_prev(:,vshift+i) = u_p(:,nvel(i))/u_prev(:,nden) - u_prev(:,nvel(i))*u_p(:,nden)/u_prev(:,nden)**2  !u_i
    END DO

    !
    !--Form right hand side of Euler Equations
    !
    F(:,:) = 0.0_pr
    DO j = 1+doBC, dim
       jshift=ne*(j-1)
       F(:,nden+jshift) = F(:,nden+jshift) + u_p(:,nvel(j)) ! rho*u_j
       DO i = 1, dim ! (rho*u_i*u_j)'
          F(:,nvel(i)+jshift) = F(:,nvel(i)+jshift) + u_prev(:,nvel(i))*v_prev(:,vshift+j) + u_p(:,nvel(i))*v_prev(:,j)
       END DO
       F(:,nvel(j)+jshift) = F(:,nvel(j)+jshift) + p(:) ! (rho*u_i^2)'+p'
       F(:,neng+jshift) = F(:,neng+jshift) + (u_p(:,neng) + p(:))*v_prev(:,j) + (u_prev(:,neng) + p_prev(:))*v_prev(:,vshift+j)  ! ((e+p)*u_j)'
       DO l=1,Nspecm
          F(:,nspc(l)+jshift) = F(:,nspc(l)+jshift) + v_prev(:,vshift+dim+1+l)*u_prev(:,nvel(j))+v_prev(:,dim+1+l)*u_p(:,nvel(j))
       END DO
    END DO

    IF (doBC .ne. 0) THEN  !Missing term from LODI assumps... gamma=constant
       shift = (neng-1)*ng
       props_prev(:,3)=props_prev(:,1)/props_prev(:,2)  !gamma/(gamma-1)0
       CALL c_diff_fast(props_prev(:,3), du(1,:,:), d2u(1,:,:), j_lev, ng, meth, 10, 1, 1, 1)  !d(gamma/(gamma-1))/dx
       int_Drhs(shift+1:shift+ng) = int_Drhs(shift+1:shift+ng)-du(1,:,1)*(v_prev(:,1)*p(:)+v_prev(:,vshift+1)*p_prev(:)) !pu*d(gamma/(gamma-1))/dx
       props(:,3)=((props(:,1)*props_prev(:,2)-props_prev(:,1)*props(:,2))/props_prev(:,2)**2) !gamma/(gamma-1)'
       CALL c_diff_fast(props(:,3), du(1,:,:), d2u(1,:,:), j_lev, ng, meth, 10, 1, 1, 1)  !d(gamma/(gamma-1))/dx
       int_Drhs(shift+1:shift+ng) = int_Drhs(shift+1:shift+ng)-du(1,:,1)*v_prev(:,1)*p_prev(:) !pu*d(gamma/(gamma-1))/dx
    END IF

    !
    !-- Form right hand side of Navier-Stokes Equations
    !
    IF(NS) THEN 
       tempintd = 2*vshift

       CALL c_diff_fast(v_prev(:,:), du(1:tempintd,:,:), d2u(1:tempintd,:,:), j_lev, ng, meth_central, 10, tempintd, 1, tempintd) ! du'_i/dx_j & dT'/dx_j

       p_prev(:) = 1.0_pr
       p(:) = 0.0_pr
       IF (Nspec>1) p_prev(:) = (p_prev(:)-SUM(v_prev(:,dim+2:dim+Nspec),DIM=2))  !Y0_Nspec
       IF (Nspec>1) p(:)      =           -SUM(v_prev(:,vshift+dim+2:vshift+dim+Nspec),DIM=2)  !Y'_Nspec
       !props_prev(:,1) = p_prev(:)*mu_in(Nspec)  !mu0_Nspec
       props_prev(:,1) = p_prev(:)*mu_in(Nspec) * (1.0_pr+S1)/(v_prev(:,dim+1) +S1)*(v_prev(:,dim+1))**(1.5_pr)  !mu_Nspec - variable viscosity
       !props_prev(:,2) = p_prev(:)*kk_in(Nspec)  !kk0_Nspec
       props_prev(:,2) = p_prev(:)*kk_in(Nspec) * (1.0_pr+S1)/(v_prev(:,dim+1) +S1)*(v_prev(:,dim+1))**(1.5_pr)  !kk0_Nspec
       props_prev(:,3) = p_prev(:)*bD_in(Nspec)  !bD0_Nspec
       !props(:,1)      = p(:)*mu_in(Nspec)  !mu'_Nspec
       props(:,1)      = p_prev(:)*mu_in(Nspec) * ((1.5_pr*(1.0_pr+S1)*v_prev(:,dim+1)**0.5_pr*v_prev(:,vshift+dim+1))/(v_prev(:,dim+1)+S1) &
            + v_prev(:,dim+1)**1.5_pr*( -(1.0_pr+S1)*v_prev(:,vshift+dim+1) )/(v_prev(:,dim+1)+S1)**2.0_pr) !mu'_Nspec !ERIC: double check for accuracy
       !props(:,2)      = p(:)*kk_in(Nspec)  !kk'_Nspec
       props(:,2)      = p_prev(:)*kk_in(Nspec)   * ((1.5_pr*(1.0_pr+S1)*v_prev(:,dim+1)**0.5_pr*v_prev(:,vshift+dim+1))/(v_prev(:,dim+1)+S1) &
            + v_prev(:,dim+1)**1.5_pr*( -(1.0_pr+S1)*v_prev(:,vshift+dim+1) )/(v_prev(:,dim+1)+S1)**2.0_pr) !kk'_Nspec
       props(:,3)      = p(:)*bD_in(Nspec)  !bD'_Nspec
       DO l=1,Nspecm
          props_prev(:,1) = props_prev(:,1) + v_prev(:,dim+1+l)*mu_in(l) * (1.0_pr+S1)/(v_prev(:,dim+1) +S1)*(v_prev(:,dim+1))**(1.5_pr)  !mu0
          props_prev(:,2) = props_prev(:,2) + v_prev(:,dim+1+l)*kk_in(l) * (1.0_pr+S1)/(v_prev(:,dim+1) +S1)*(v_prev(:,dim+1))**(1.5_pr)  !kk0
          props_prev(:,3) = props_prev(:,3) + v_prev(:,dim+1+l)*bD_in(l)  !bD0
          props(:,1)      = props(:,1)      + v_prev(:,vshift+dim+1+l)*mu_in(l) * (1.5_pr*(1.0_pr+S1)*v_prev(:,dim+1)**0.5_pr*v_prev(:,vshift+dim+1))/(v_prev(:,dim+1)+S1) &
               + v_prev(:,dim+1)**1.5_pr*( -(1.0_pr+S1)*v_prev(:,vshift+dim+1) )/(v_prev(:,dim+1)+S1)**2.0_pr  !mu'
          props(:,2)      = props(:,2)      + v_prev(:,vshift+dim+1+l)*kk_in(l) * (1.5_pr*(1.0_pr+S1)*v_prev(:,dim+1)**0.5_pr*v_prev(:,vshift+dim+1))/(v_prev(:,dim+1)+S1) &
               + v_prev(:,dim+1)**1.5_pr*( -(1.0_pr+S1)*v_prev(:,vshift+dim+1) )/(v_prev(:,dim+1)+S1)**2.0_pr  !kk'
          props(:,3)      = props(:,3)      + v_prev(:,vshift+dim+1+l)*bD_in(l)  !bD'
       END DO
       p(:) = du(vshift+1,:,1) !reuse of a variable
       DO i = 2, dim
          p(:) = p(:) + du(vshift+i,:,i) !du'_i/dx_i
       END DO

       d2u(:,:,:) = 1.0_pr
       DO j = 1, dim
          jshift=ne*(j-1)
          DO i = 1, dim
             F(:,nvel(i)+jshift) = F(:,nvel(i)+jshift) - props_prev(:,1)*( du(vshift+i,:,j)+du(vshift+j,:,i) ) * d2u(i,:,j)  !rho*u_i*u_j - 2*mu*S_ij/Re
          END DO
          F(:,nvel(j)+jshift) = F(:,nvel(j)+jshift) + 2.0_pr/3.0_pr*props_prev(:,1)*p(:) * d2u(j,:,j) !rho*u_i*u_j - tau_ij/Re  [ tau_ij = 2*mu*( S_ij - 2/3*S_kk*delta_ij ) ]
          DO i = 1, dim
             F(:,neng+jshift) =  F(:,neng+jshift) - props_prev(:,1)*( du(vshift+i,:,j)+du(vshift+j,:,i) )*v_prev(:,i) * d2u(i,:,j)  !(e+p)*u_j - u_i*2*mu*S_ij/Re
          END DO
          F(:,neng+jshift) =  F(:,neng+jshift) + 2.0_pr/3.0_pr*props_prev(:,1)*p(:)*v_prev(:,j) * d2u(j,:,j) - props_prev(:,2)*du(vshift+dim+1,:,j) * d2u(2,:,j)  
          !(e+p)*u_j - u_i*tau_ij/Re - q_j/Re/Pra [ q_j = mu dT/dx_j ]
          DO l=1,Nspecm
             F(:,neng+jshift) =  F(:,neng+jshift) - u_prev(:,nden)*cp_in(l)*v_prev(:,dim+1)*props_prev(:,3)*du(vshift+dim+1+l,:,j) * d2u(2,:,j)
          END DO
          IF (Nspec>1) F(:,neng+jshift) =  F(:,neng+jshift) +  u_prev(:,nden)*cp_in(Nspec)*v_prev(:,dim+1)*props_prev(:,3)*SUM(du(vshift+dim+2:vshift+dim+Nspec,:,j),DIM=1) * d2u(2,:,j)
          DO l=1,Nspecm
             F(:,nspc(l)+jshift) =  F(:,nspc(l)+jshift) - u_prev(:,nden)*props_prev(:,3)*du(vshift+dim+1+l,:,j) * d2u(2,:,j)
          END DO
       END DO

       p_prev(:) = du(1,:,1) !reuse of a variable
       DO i = 2, dim
          p_prev(:) = p_prev(:) + du(i,:,i) !du0_i/dx_i
       END DO
       DO j = 1, dim
          jshift=ne*(j-1)
          DO i = 1, dim
             F(:,nvel(i)+jshift) = F(:,nvel(i)+jshift) - props(:,1)*( du(i,:,j)+du(j,:,i) ) * d2u(i,:,j)  !rho*u_i*u_j - 2*mu*S_ij/Re
          END DO
          F(:,nvel(j)+jshift) = F(:,nvel(j)+jshift) + 2.0_pr/3.0_pr*props(:,1)*p_prev(:) * d2u(j,:,j) !rho*u_i*u_j - tau_ij/Re  [ tau_ij = 2*mu*( S_ij - 2/3*S_kk*delta_ij ) ]
          DO i = 1, dim
             F(:,neng+jshift) =  F(:,neng+jshift) - ( du(i,:,j)+du(j,:,i) )*(v_prev(:,i)*props(:,1)+v_prev(:,vshift+i)*props_prev(:,1)) * d2u(i,:,j)  !(e+p)*u_j - u_i*2*mu*S_ij/Re
          END DO
          F(:,neng+jshift) =  F(:,neng+jshift) + 2.0_pr/3.0_pr*p_prev(:)*(v_prev(:,j)*props(:,1)+v_prev(:,vshift+j)*props_prev(:,1)) * d2u(j,:,j) - props(:,2)*du(dim+1,:,j) * d2u(2,:,j)  
          !(e+p)*u_j - u_i*tau_ij/Re - q_j/Re/Pra [ q_j = mu dT/dx_j ]
          DO l=1,Nspecm
             F(:,neng+jshift) =  F(:,neng+jshift) - cp_in(l)*du(dim+1+l,:,j) * &
                  ( u_p(:,nden)*v_prev(:,dim+1)*props_prev(:,3) + u_prev(:,nden)*(v_prev(:,vshift+dim+1)*props_prev(:,3) + v_prev(:,dim+1)*props(:,3)) ) * d2u(2,:,j)
          END DO
          IF (Nspec>1) F(:,neng+jshift) =  F(:,neng+jshift) +  cp_in(Nspec)*SUM(du(dim+2:dim+Nspec,:,j),DIM=1) * &
               ( u_p(:,nden)*v_prev(:,dim+1)*props_prev(:,3) + u_prev(:,nden)*(v_prev(:,vshift+dim+1)*props_prev(:,3) + v_prev(:,dim+1)*props(:,3)) ) * d2u(2,:,j)
          DO l=1,Nspecm
             F(:,nspc(l)+jshift) =  F(:,nspc(l)+jshift) - (u_p(:,nden)*props_prev(:,3)+u_prev(:,nden)*props(:,3))*du(dim+1+l,:,j) * d2u(2,:,j)
          END DO
       END DO
    END IF
    IF (GRAV) THEN
       shift = (neng-1)*ng
       DO j=1,dim
          DO l=1,Nspecm
             int_Drhs(shift+1:shift+ng) = int_Drhs(shift+1:shift+ng)-(v_prev(:,dim+1+l)*u_p(:,nvel(j))+v_prev(:,vshift+dim+1+l)*u_prev(:,nvel(j)))*gr_in(l+Nspec*(j-1))
          END DO
          IF (Nspec>1) THEN
             int_Drhs(shift+1:shift+ng) = int_Drhs(shift+1:shift+ng) - ((1.0_pr-SUM(v_prev(:,dim+2:dim+Nspec),DIM=2))*u_p(:,nvel(j))-SUM(v_prev(:,vshift+dim+2:vshift+dim+Nspec),DIM=2)*u_prev(:,nvel(j)) )*gr_in(Nspec*j)
          ELSE
             int_Drhs(shift+1:shift+ng) = int_Drhs(shift+1:shift+ng) - u_p(:,nvel(j))*gr_in(Nspec*j)
          END IF
       END DO
    END IF
    tempintd=ne*dim

    CALL c_diff_fast(F(:,1:tempintd), du(1:tempintd,:,:), d2u(1:tempintd,:,:), j_lev, ng, meth_central, 10, tempintd, 1, tempintd)  !du(i,:,k)=dF_ij/dx_k

    DO j = 1, dim
       jshift=ne*(j-1)
       DO i = 1, dim+1+Nspec
          shift=(i-1)*ng
          int_Drhs(shift+1:shift+ng) = int_Drhs(shift+1:shift+ng) - du(i+jshift,:,j)  ! -dF_ij/dx_j  !continutiy set up
       END DO
       IF (GRAV) THEN      
          shift = (nvel(j)-1)*ng
          DO l=1,Nspecm        
             int_Drhs(shift+1:shift+ng) = int_Drhs(shift+1:shift+ng) - gr_in(l+Nspec*(j-1))*u_p(:,nspc(l))
          END DO
          IF (Nspec>1) THEN
             int_Drhs(shift+1:shift+ng) = int_Drhs(shift+1:shift+ng) - gr_in(Nspec*j)*(u_p(:,nden)-SUM(u_p(:,nspc(1):nspc(Nspecm)),DIM=2))
          ELSE
             int_Drhs(shift+1:shift+ng) = int_Drhs(shift+1:shift+ng) - gr_in(Nspec*j)*u_p(:,nden)
          END IF
       END IF
    END DO


  END SUBROUTINE internal_Drhs

  SUBROUTINE internal_Drhs_diag (int_diag,u_prev,meth,doBC,addingon)
    USE penalization
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: meth
    REAL (pr), DIMENSION (ng*ne), INTENT(INOUT) :: int_diag
    REAL (pr), DIMENSION (ng,ne), INTENT(IN) :: u_prev
    INTEGER, INTENT(IN) :: doBC !if zero do full rhs, if one no x-derivatives 
    INTEGER, INTENT(IN) :: addingon 

    INTEGER :: i, j, l, ll, ie, shift
    REAL (pr), DIMENSION (ng,dim) :: du_diag, d2u_diag, du_diag_F, d2u_diag_F, du_diag_B, d2u_diag_B
    REAL (pr), DIMENSION (MAX(Nspec,dim),ng,dim) :: du, d2u, duB, d2uB, duF, d2uF, du_upwind, d2u_upwind
    REAL (pr), DIMENSION (ng,n_integrated,dim) :: F
    REAL (pr), DIMENSION (ng, Nspec) :: v_prev
    REAL (pr), DIMENSION (ng, dim+1) :: T_prev  !temperature and velocity
    REAL (pr), DIMENSION (ng, dim+1) :: T_p_diag    !momentum and energy
    REAL (pr), DIMENSION (ng, dim+2) :: props_diag  !momentum and energy(x2)
    REAL (pr), DIMENSION (ng,6) :: props_prev
    REAL (pr), DIMENSION (ng) :: p_prev

    REAL (pr) , DIMENSION(ng) :: dist_loc
    REAL (pr) , DIMENSION(ng,dim) :: obs_norm
    REAL (pr) :: min_h
    INTEGER :: meth_central,meth_backward,meth_forward

    meth_central  = meth + BIASING_NONE
    meth_backward = meth + BIASING_BACKWARD
    meth_forward  = meth + BIASING_FORWARD

    min_h = MINVAL(h(1,1:dim)/2**(j_mx - 1))


    int_diag = 0.0_pr
    DO l=1,Nspecm
       v_prev(:,l) = u_prev(:,nspc(l))/u_prev(:,nden)  ! Y0
    END DO
    v_prev(:,Nspec)=1.0_pr
    IF (Nspec>1) v_prev(:,Nspec) = v_prev(:,Nspec)-SUM(v_prev(:,1:Nspecm),DIM=2)  !Y0_Nspec
    props_prev(:,:)=0.0_pr
    DO l=1,Nspec
       props_prev(:,1) = props_prev(:,1) + v_prev(:,l)*cp_in(l)  !cp0
       props_prev(:,2) = props_prev(:,2) + v_prev(:,l)/MW_in(l)/Ma**2.0_pr  !R0
       props_prev(:,4) = props_prev(:,1) + v_prev(:,l)*mu_in(l)  !mu0  ERIC: not set up for dynamic viscosity, not modularized
       props_prev(:,5) = props_prev(:,2) + v_prev(:,l)*kk_in(l)  !kk0
       props_prev(:,6) = props_prev(:,3) + v_prev(:,l)*bD_in(l)  !bD0
    END DO
    props_prev(:,3) = props_prev(:,1)/(props_prev(:,1)-props_prev(:,2)) !gamma0 = cp0/(cp0-R0)

    IF(.TRUE.) THEN !overwrite props !ERIC: correct this because energy and momentum perturbation used to calculate temperature
       p_prev(:) = (u_prev(:,neng)-0.5_pr*SUM(u_prev(:,nvel(1):nvel(dim))**2.0_pr,DIM=2)/u_prev(:,nden))/(props_prev(:,1)/props_prev(:,2)-1.0_pr)  !pressure    
       T_prev(:,1) = p_prev(:)/props_prev(:,2)/u_prev(:,nden)  !Temperature
       DO i=1,dim !momentum diag terms
          T_p_diag(:,i) =    ( u_prev(:,nvel(i))/u_prev(:,nden)) /  &
               (props_prev(:,1)/props_prev(:,2)-1.0_pr)/props_prev(:,2)/u_prev(:,nden)  
       END DO
       T_p_diag(:,dim+1) =    1.0_pr / (props_prev(:,1)/props_prev(:,2)-1.0_pr)/props_prev(:,2)/u_prev(:,nden)  !Energy diag term
       !ERIC: develop props_p coefficient for energy and momentum
       props_prev(:,4:5) = 0.0_pr
       props_diag(:,:)   = 0.0_pr
       DO l=1,Nspec
          props_prev(:,4) = props_prev(:,4) + v_prev(:,l)*mu_in(l)*(1.0_pr+S1)/(T_prev(:,1) +S1)*(T_prev(:,1))**(1.5_pr)   !mu0
          props_prev(:,5) = props_prev(:,5) + v_prev(:,l)*kk_in(l)*(1.0_pr+S1)/(T_prev(:,1) +S1)*(T_prev(:,1))**(1.5_pr)   !kk0
          !diag terms from Temperature-varying props
          DO i = 1,dim
             props_diag(:,i) = props_diag(:,i) + v_prev(:,l)*mu_in(l)* ((1.5_pr*(1.0_pr+S1)*T_prev(:,1)**0.5_pr*T_p_diag(:,i))/(T_prev(:,1)+S1) &
                  + T_prev(:,1)**1.5_pr*( -(1.0_pr+S1)*T_p_diag(:,i) )/(T_prev(:,1)+S1)**2.0_pr)  !mu', velocity
          END DO
          props_diag(:,dim+1) = props_diag(:,dim+1) + v_prev(:,l)*mu_in(l)* ((1.5_pr*(1.0_pr+S1)*T_prev(:,1)**0.5_pr*T_p_diag(:,dim+1))/(T_prev(:,1)+S1) &
               + T_prev(:,1)**1.5_pr*( -(1.0_pr+S1)*T_p_diag(:,dim+1) )/(T_prev(:,1)+S1)**2.0_pr)  !mu', energy
          props_diag(:,dim+2) = props_diag(:,dim+2) + v_prev(:,l)*kk_in(l)* ((1.5_pr*(1.0_pr+S1)*T_prev(:,1)**0.5_pr*T_p_diag(:,dim+1))/(T_prev(:,1)+S1) &
               + T_prev(:,1)**1.5_pr*( -(1.0_pr+S1)*T_p_diag(:,dim+1) )/(T_prev(:,1)+S1)**2.0_pr)  !kk', energy
       END DO
    END IF
    CALL c_diff_diag(du_diag, d2u_diag, j_lev, ng, meth, meth, -11)
    !
    !--Form right hand side of Euler Equations
    !
    F(:,:,:) = 0.0_pr
    DO j = 1+doBC, dim
       DO i = 1, dim ! (rho*u_i*u_j)'
          F(:,nvel(i),j) = F(:,nvel(i),j) + u_prev(:,nvel(j))/u_prev(:,nden)
       END DO
       F(:,nvel(j),j) = F(:,nvel(j),j) + u_prev(:,nvel(j))/u_prev(:,nden)*(2.0_pr-props_prev(:,3))
       F(:,neng,j) = F(:,neng,j) + props_prev(:,3)*u_prev(:,nvel(j))/u_prev(:,nden)
       DO l=1,Nspecm
          F(:,nspc(l),j) = F(:,nspc(l),j) + u_prev(:,nvel(j))/u_prev(:,nden)
       END DO
    END DO

    IF (doBC .ne. 0) THEN  !Missing term from LODI assumps... gamma=constant
       shift = (neng-1)*ng
       CALL c_diff_fast(props_prev(:,1)/props_prev(:,2), du(1,:,:), d2u(1,:,:), j_lev, ng, meth, 10, 1, 1, 1)  !d(gamma/(gamma-1))/dx
       int_diag(shift+1:shift+ng) = int_diag(shift+1:shift+ng)-du(1,:,1)*u_prev(:,nvel(1))/u_prev(:,nden)*(props_prev(:,3)-1.0_pr) !pu*d(gamma/(gamma-1))/dx
    END IF
    !
    !-- Form right hand side of Navier-Stokes Equations
    !
    IF(NS .AND. NSdiag) THEN
       CALL c_diff_fast(v_prev, du(1:Nspec,:,:), d2u(1:Nspec,:,:), j_lev, ng, meth, 10, Nspec, 1, Nspec) ! dY_l/dx_j

       d2u(:,:,:) = 1.0_pr
       DO j = 1, dim
          DO l=1,Nspec
             IF (Nspec>1) F(:,neng,j) =  F(:,neng,j) - props_prev(:,6)*du(l,:,j)*cp_in(l)/(props_prev(:,1)-props_prev(:,2))*d2u(2,:,j)
          END DO
       END DO
       DO j = 1, dim
          DO i = 1, dim
             shift=(nvel(i)-1)*ng
             int_diag(shift+1:shift+ng) = int_diag(shift+1:shift+ng) + props_prev(:,4)/u_prev(:,nden)*d2u_diag(:,j) * d2u(i,:,j)
          END DO
          shift=(nvel(j)-1)*ng
          int_diag(shift+1:shift+ng) = int_diag(shift+1:shift+ng) + props_prev(:,4)/u_prev(:,nden)*d2u_diag(:,j)/3.0_pr * d2u(j,:,j)
          shift=(neng-1)*ng
          int_diag(shift+1:shift+ng) = int_diag(shift+1:shift+ng) + props_prev(:,5)/u_prev(:,nden)/(props_prev(:,1)-props_prev(:,2))*d2u_diag(:,j) * d2u(2,:,j)
          DO l = 1, Nspecm
             shift=(nspc(l)-1)*ng
             int_diag(shift+1:shift+ng) = int_diag(shift+1:shift+ng) +(props_prev(:,6)*d2u_diag(:,j)+bD_in(l)*du(l,:,j)) * d2u(2,:,j)
          END DO
       END DO
    END IF
    DO j = 1, dim
       DO i = 1, dim+1+Nspec
          shift=(i-1)*ng
          int_diag(shift+1:shift+ng) = int_diag(shift+1:shift+ng) - F(:,i,j)*du_diag(:,j)
       END DO
    END DO



  END SUBROUTINE internal_Drhs_diag

  FUNCTION user_rhs (u_integrated,scalar)
    IMPLICIT NONE
    REAL (pr), DIMENSION (ng,ne), INTENT(IN) :: u_integrated !1D flat version of integrated variables without BC 
    REAL (pr), DIMENSION (ng), INTENT(IN) :: scalar
    REAL (pr), DIMENSION (ng*ne) :: user_rhs

    REAL (pr), DIMENSION (ne,ng,dim) :: du, d2u
    INTEGER, PARAMETER :: meth=1


    CALL internal_rhs(user_rhs,u_integrated,0,0)

    IF (imask_obstacle ) CALL Brinkman_rhs (user_rhs, u_integrated, .FALSE., LOW_ORDER)

    IF(hypermodel .NE. 0) CALL hyperbolic(u_integrated,ng,user_rhs)
    IF (cnvtBC .OR. diffBC) CALL user_convectzone_BC_rhs (user_rhs, u_integrated, ne, ng, j_lev, LOW_ORDER) !add artificial convection to bufferzone/obstacle
    IF (buffBC) CALL user_bufferzone_BC_rhs (user_rhs, u_integrated, ne, ng, j_lev, meth)

  END FUNCTION user_rhs

  FUNCTION user_Drhs (u_p, u_prev, meth)
!!$    USE Brinkman
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: meth
    REAL (pr), DIMENSION (ng,ne) :: u_p, u_prev
    REAL (pr), DIMENSION (ng*ne) :: user_Drhs
    REAL (pr), DIMENSION (2*ne,ng,dim) :: du
    REAL (pr), DIMENSION (ne,ng,dim)   :: d2u

    CALL internal_Drhs(user_Drhs,u_p,u_prev,meth,0,0)

    IF (imask_obstacle) CALL Brinkman_Drhs (user_Drhs, u_p, u_prev, LOW_ORDER)
    IF (hypermodel .NE. 0 ) CALL hyperbolic(u_p,ng,user_Drhs)
    IF (cnvtBC .OR. diffBC) CALL user_convectzone_BC_Drhs (user_Drhs, u_p, u_prev, ne, ng, j_lev, LOW_ORDER) !add artificial convection to bufferzone/obstacle
    IF (buffBC) CALL user_bufferzone_BC_Drhs (user_Drhs, u_p, u_prev, ne, ng, j_lev, meth)

  END FUNCTION user_Drhs

  FUNCTION user_Drhs_diag (meth)
!!$    USE Brinkman
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: meth
    REAL (pr), DIMENSION (ng*ne) :: user_Drhs_diag
    REAL (pr), DIMENSION (ng,ne) :: u_prev
    REAL (pr), DIMENSION (ne,ng,dim) :: du_dummy
    REAL (pr), DIMENSION (ng,dim)    :: du_diag, d2u_diag 
    INTEGER :: ie, shift

    DO ie = 1, ne
       shift=(ie-1)*ng
       u_prev(1:ng,ie) = u_prev_timestep(shift+1:shift+ng)
    END DO


    CALL internal_Drhs_diag(user_Drhs_diag,u_prev,meth,0,0)

    IF (imask_obstacle) CALL Brinkman_Drhs_diag (user_Drhs_diag, u_prev, LOW_ORDER)
    IF(hypermodel .NE. 0 ) CALL hyperbolic_diag(user_Drhs_diag, ng)
    IF (cnvtBC .OR. diffBC) CALL user_convectzone_BC_Drhs_diag (user_Drhs_diag, u_prev, ne, ng, j_lev, LOW_ORDER)
    IF (buffBC) CALL user_bufferzone_BC_Drhs_diag (user_Drhs_diag, u_prev, ne, ng, j_lev, meth)


  END FUNCTION user_Drhs_diag



  SUBROUTINE user_project (u, p, nlocal, meth)
    IMPLICIT NONE

    INTEGER, INTENT (IN) :: meth, nlocal
    REAL (pr), DIMENSION (nlocal),     INTENT(INOUT) :: p
    REAL (pr), DIMENSION (nlocal,n_integrated), INTENT(INOUT) :: u

    INTEGER :: i
    INTEGER, PARAMETER :: ne = 1
    INTEGER, DIMENSION(ne) :: clip 
    !no projection for compressible flow
  END SUBROUTINE user_project

  FUNCTION Laplace (jlev, u, nlocal, ne_local, meth)
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: jlev, nlocal, ne_local, meth
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (INOUT) :: u
    REAL (pr), DIMENSION (nlocal*ne_local) :: Laplace

    INTEGER :: i, ii, ie, shift, meth1, meth2, idim
    REAL (pr), DIMENSION (ne_local ,nlocal,dim) :: du
    REAL (pr), DIMENSION (ne_local*dim ,nlocal,dim) :: d2u
    REAL (pr), DIMENSION (ne_local*dim ,nlocal,dim) :: du_dummy ! passed when only calculating 1st derivative.
    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc

    Laplace=0.0_pr
  END FUNCTION Laplace

  FUNCTION Laplace_diag (jlev,  nlocal, ne_local, meth)
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: jlev, nlocal, ne_local, meth
    REAL (pr), DIMENSION (nlocal*ne_local) :: Laplace_diag

    INTEGER :: i, ii, ie, shift, meth1, meth2
    REAL (pr), DIMENSION (nlocal,dim) :: du, d2u
    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc

    Laplace_diag=0.0_pr
  END FUNCTION Laplace_diag

  FUNCTION Laplace_rhs(u, nlocal, ne_local, meth)
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: nlocal, ne_local, meth
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (INOUT) :: u
    REAL (pr), DIMENSION (nlocal) :: Laplace_rhs

    INTEGER :: i, ii, ie, meth1, meth2, idim
    REAL (pr), DIMENSION (ne_local ,nlocal,dim) :: du
    REAL (pr), DIMENSION (ne_local*dim ,nlocal,dim) :: d2u
    REAL (pr), DIMENSION (ne_local*dim ,nlocal,dim) :: du_dummy ! passed when only calculating 1st derivative.
    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc

    Laplace_rhs = 0.0_pr 
  END FUNCTION Laplace_rhs

  !
  ! Calculate any statitics
  !
  ! startup_flag - 0 when adapting to IC,then 1 inmain integration loop
  !
  SUBROUTINE user_stats ( u, j_mn_local, startup_flag)
    USE parallel
    USE penalization
    IMPLICIT NONE
    REAL (pr), DIMENSION (nwlt,1:n_var), INTENT (IN) :: u
    INTEGER , INTENT (IN) :: j_mn_local 
    INTEGER , INTENT (IN) :: startup_flag

    CHARACTER (LEN=4) :: string2
    CHARACTER (LEN=60) :: sformat

    REAL (pr), DIMENSION(dim) ::F_i, Fp_i ! global forces, Fp_i - pressure gradient force
    REAL (pr), DIMENSION (dim, 0:Nsp) :: F_n, Fp_n, Tor_n
    REAL (pr), DIMENSION(dim) :: Tor ! global Torque
    ! Nurlybek
    LOGICAL :: exists
    CHARACTER (LEN=128) :: filepath
    CHARACTER (LEN=256) :: string
    CHARACTER (LEN=6) :: numfmt ! number format
    CHARACTER (LEN=4) :: tfmt   ! time format
    CHARACTER (LEN=2) :: sep    ! separator in format
    CHARACTER (LEN=3) :: comma

    IF (object_forces_divstress .AND. startup_flag .EQ. 1  ) THEN
!!$       CALL user_cal_force(u, F_i, Fp_i, Tor)
!!$       IF(par_rank ==0) THEN
!!$          PRINT *,  'DIVSTRESS'
!!$          PRINT *,  'F_d:', F_i(1)
!!$          PRINT *,  'F_l:', F_i(2)
!!$          PRINT *,  'T_x:', Tor(1)
!!$       END IF
       IF(par_rank .EQ. 0) THEN
          IF(dim ==2) WRITE(string2,'(I4)') 3
          IF(dim ==3) WRITE(string2,'(I4)') 4
          sformat = '('//TRIM(ADJUSTL(string2))//'(E20.12))'
          OPEN(UNIT=331, FILE = TRIM(res_path)//TRIM(file_gen)//'divstress'//'.stat', STATUS = 'UNKNOWN', form="FORMATTED", POSITION="APPEND")
!!$          IF(dim == 2) WRITE(331,sformat) t, F_i(1), F_i(2)
!!$          IF(dim == 3)  WRITE(331,sformat) t, F_i(1), F_i(2), F_i(3)
!!$          PRINT *, 'Aero stats (Brinkman)  written'

          ! new file written with pressure gradient included.
          tfmt = "F0.6"
          numfmt = "E22.16"
          sep = ", "
          comma = "','"
          filepath = TRIM(res_path)//TRIM(file_gen)//'divstress_pgrad'//'.stat'
          IF (dim >= 2) THEN 
             string = "(" ! format opener
             string = TRIM(string)//tfmt
             string = TRIM(string)//sep
             string = TRIM(string)//comma
             string = TRIM(string)//sep
             ! time
             string = TRIM(string)//numfmt
             string = TRIM(string)//sep
             string = TRIM(string)//comma
             string = TRIM(string)//sep
             ! 1st number, x component of the total stress assoc force
             string = TRIM(string)//numfmt
             string = TRIM(string)//sep
             string = TRIM(string)//comma
             string = TRIM(string)//sep
             ! 2nd number, y component
             string = TRIM(string)//numfmt
             string = TRIM(string)//sep
             string = TRIM(string)//comma
             string = TRIM(string)//sep
             ! 3rd number, either z component (dim == 3) or x component of pressure force (dimm == 2)
             string = TRIM(string)//numfmt
             string = TRIM(string)//sep
             string = TRIM(string)//comma
             string = TRIM(string)//sep
             ! 4th number, either x component of pressure  force (dim == 3) or y component of pressure force (dim == 2)
             string = TRIM(string)//numfmt
             string = TRIM(string)//sep
             string = TRIM(string)//comma
             string = TRIM(string)//sep
             ! 5th number, either y componen of pressure force (dim == 3) or x component of Torque (dim == 2)
             string = TRIM(string)//numfmt
             ! 6th number, either z componen of pressure force (dim == 3) or y component of Torque (dim == 2)
             IF (dim==3) THEN
                string = TRIM(string)//sep
                string = TRIM(string)//comma
                string = TRIM(string)//sep
                ! added separator
                string = TRIM(string)//numfmt
                string = TRIM(string)//sep
                string = TRIM(string)//comma
                string = TRIM(string)//sep
                ! 7th number x component of Torque
                string = TRIM(string)//numfmt
                string = TRIM(string)//sep
                string = TRIM(string)//comma
                string = TRIM(string)//sep
                ! 8th number y component of Torque
                string = TRIM(string)//numfmt
                ! 9th number z component of Torque
             END IF
             string = TRIM(string)//")" ! format close
!!$             OPEN(UNIT=332, FILE = filepath, STATUS = 'UNKNOWN', form="FORMATTED", POSITION="APPEND")
!!$             IF(dim == 2) WRITE(332,string) t, F_i(1), F_i(2), Fp_i(1), Fp_i(2), Tor(1), Tor(2)
!!$             IF(dim == 3) WRITE(332,string) t, F_i(1), F_i(2), F_i(3), Fp_i(1), Fp_i(2), Fp_i(3), Tor(1), Tor(2), Tor(3)
          END IF
       END IF

    END IF

    IF(additional_planes_active) CALL plane_stats (u, j_mn_local, startup_flag)
    IF(additional_points_active) CALL point_stats (u, j_mn_local, startup_flag)

  END SUBROUTINE user_stats

  SUBROUTINE user_cal_force (u, F_n, Fp_n, Tor_n)
    USE parallel
    USE penalization
    !--Calculates drag and lift on obstacle using penalization formula
    IMPLICIT NONE
    ! Input and output params
    REAL (pr), DIMENSION (nwlt, n_var), INTENT(IN) :: u
!!$    REAL (pr), DIMENSION (dim), INTENT(INOUT) :: F_i, Fp_i, Tor
    REAL (pr), DIMENSION (dim, 0:Nsp), INTENT(INOUT) :: F_n, Fp_n, Tor_n

    ! Internal params
    INTEGER :: idim, j, i
    REAL(pr), DIMENSION(nwlt,dim) :: v
    REAL(pr), DIMENSION(dim*2 + 1,nwlt,dim) :: du ! +1 for pressure
    REAL(pr), DIMENSION(nwlt) :: Temperature,visc
    REAL(pr), DIMENSION (dim*dim,nwlt,dim) :: temp  !solutions to interpolate from
    REAL (pr), DIMENSION(nwlt, dim) :: Tor_loc, delta_r ! Local torque and radius vector of the point w.r.t. mass center
    REAL (pr), DIMENSION(nwlt ,dim+dim) :: F_loc ! +dim for pressure gradient components
    REAL (pr), DIMENSION(1:nwlt,dim*(dim+1)/2 + 1) :: S_ij, sigma_ij ! +1 for the pressure
    REAL (pr), DIMENSION(nwlt)                 :: Smod
    ! tmp variable
    REAL (pr), DIMENSION(nwlt) :: utmp

    INTEGER, DIMENSION(nwlt) :: icolor

    REAL(pr), DIMENSION(dim) :: r_vec ! Ryan - postion vector between center of particle and cell position for calculating torques

    !Velocity Derivatives  !ERIC: for robustness, can replace this with
    !Sij subroutine
    DO idim = 1,dim
       v(:,idim) = u(:,nvel(idim))/u(:,nden)
    END DO
    CALL Sij( v, nwlt, S_ij, Smod, .TRUE.)
    
    !VISCOSITY
    Temperature = MW_in(Nspec)*Ma**2.0_pr*u(:,nprs)/u(:,nden)
    
    !IF(dyn_visc) THEN
    IF(.TRUE.) THEN
       visc=mu_in(Nspec) * (1.0_pr+S1)/(Temperature +S1)*(Temperature)**(1.5_pr)  !mu_Nspec - variable viscosity
    ELSE
       visc=mu_in(Nspec)
    END IF
    
    !FULL Stress
    DO j = 1,dim*(dim+1)/2
       sigma_ij(:,j) = 2.0_pr*visc*S_ij(:,j)
    END DO

    DO j = 1,dim
       sigma_ij(:,j) = 2.0_pr*visc*S_ij(:,j) - u(:,nprs)
    END DO

    sigma_ij(:, dim*(dim+1)/2 + 1) = u(:, nprs)

    CALL c_diff_fast(sigma_ij(:,1:dim*(dim+1)/2+1),du(1:dim*(dim+1)/2+1,:,:),temp(1:dim,:,:),j_lev,nwlt,HIGH_ORDER,10,dim*(dim+1)/2+1,1,dim*(dim+1)/2+1)

    !Calculate forces from div
    DO i = 1,dim
       F_loc(:, i) = du(i,:,i)     !d/dx_i (sigma_ii)
       F_loc(:, dim+i) = -du(dim*(dim+1)/2+1, :, i) ! -dp/dx
!!$       delta_r(:, i) = x(:, i)
    END DO
    IF(dim == 2) THEN
       F_loc(:, 1) = F_loc(:, 1) + du(3,:,2)
       F_loc(:, 2) = F_loc(:, 2) + du(3,:,1)
    ELSE IF(dim ==3) THEN
       F_loc(:, 1) = F_loc(:, 1) + du(4,:,2) + du(5,:,3)
       F_loc(:, 2) = F_loc(:, 2) + du(4,:,1) + du(6,:,3)
       F_loc(:, 3) = F_loc(:, 3) + du(5,:,1) + du(6,:,2)
    END IF

    Tor_loc(:, :) = cross_prod(x, F_loc, nwlt)

!!$    CALL user_dist (nwlt, t, DISTANCE=utmp, COLOR = icolor)
    
    F_n(:,:) = 0.0_pr
    Fp_n(:,:) = 0.0_pr
    Tor_n(:,:) = 0.0_pr

    icolor = penal_color

    DO i = 1,nwlt
       F_n(:, icolor(i)) = F_n(:, icolor(i)) + F_loc(i, 1:dim)*dA(i)*penal(i)
       Fp_n(:, icolor(i)) = Fp_n(:, icolor(i)) + F_loc(i, dim+1:2*dim)*dA(i)*penal(i)
!!$       Tor_n(:, icolor(i)) = Tor_n(:, icolor(i)) + Tor_loc(i, 1:dim)*dA(i)*penal(i)
       IF (icolor(i) > 0 ) THEN
          r_vec(:) = x(i,:) - poss(:,icolor(i))
          r_vec(:) = r_vec(:) - prd(:)*nint(r_vec(:)/(xyzlimits(2,:)-xyzlimits(1,:)))*(xyzlimits(2,:)-xyzlimits(1,:))
          Tor_n(:, icolor(i)) = Tor_n(:, icolor(i)) + cross_prod_single(r_vec,F_loc(i,:))*dA(i)*penal(i)
       END IF
    END DO

!!$    ! Torque Correction
!!$    DO i = 1, Nsp
!!$       Tor_n(:, i) =  Tor_n(:, i) - cross_prod_single(poss(:, i), F_n(:, i))
!!$    END DO

    DO i = 1,Nsp
       CALL parallel_vector_sum(REAL=F_n(:, i), LENGTH=dim)
       CALL parallel_vector_sum(REAL=Fp_n(:, i), LENGTH=dim)
       CALL parallel_vector_sum(REAL=Tor_n(:, i), LENGTH=dim)
    END DO
  END SUBROUTINE user_cal_force

  SUBROUTINE point_stats (u , j_mn_local, startup_flag)
    USE precision
    USE db_tree
    USE wlt_vars
    USE vector_util_mod
    USE parallel            ! par_size
    IMPLICIT NONE
    REAL (pr), DIMENSION (nwlt,1:n_var), INTENT (IN) :: u
    INTEGER , INTENT (IN) :: j_mn_local
    INTEGER , INTENT (IN) :: startup_flag
    INTEGER :: UNIT_USER_POINTS

    !================= POINT STATS ====================
    REAL (pr), DIMENSION(:,:)    , ALLOCATABLE :: sampleU, sampleX 
    REAL (pr), DIMENSION(:,:)    , ALLOCATABLE :: sampleSij
    INTEGER :: Sij_size 
    REAL (pr)                                  :: TMPij(nwlt,dim*(dim+1)/2), tmp(nwlt)  !tmp for Sij
    REAL (pr), DIMENSION(nwlt,dim)             :: v      ! velocity for Sij 
    INTEGER :: inwlt(1)
    INTEGER :: i_point, j_point_loc, proc
    INTEGER :: i, idim, step, ixyz(1:dim), ixyz_point(1:dim)
    LOGICAL , SAVE :: writeheader = .TRUE.
    !==============================================

    LOGICAL :: err

    ALLOCATE(sampleU(n_additional_points,dim+2))
    ALLOCATE(sampleX(n_additional_points,dim))

    Sij_size = dim*(dim+1)/2
    ALLOCATE(sampleSij(n_additional_points,Sij_size))

    sampleU   = 0.0_pr
    sampleSij = 0.0_pr
    sampleX   = 0.0_pr

    !--------Calculate stresses-------
    DO i = 1,dim
       v(:,i) = u(:,nvel(i))/u(:,nden)
    END DO
    CALL Sij( v(:,1:dim) ,nwlt, TMPij(:,1:Sij_size), tmp(:), .TRUE. )
    !---------------------------------

    ! Locate ixyz coordinate
    j_point_loc = MIN(j_additional_points,j_lev)
    step = MAX(1,2**(j_mx-j_point_loc))
    DO i_point = 1, n_additional_points
       DO idim = 1, dim
          ixyz_point(idim) = (MINLOC( ABS(xx(0:nxyz(idim):step,idim) - xyz_points(idim,i_point)), DIM = 1  ) - 1 )*step
       END DO

       CALL DB_get_proc_by_coordinates (ixyz_point, j_mx, proc)

       ! Sample location
       IF (proc.EQ.par_rank) THEN
          CALL get_indices_by_coordinate(1,ixyz_point(1:dim),j_lev,inwlt(1), 0)
          PRINT *, inwlt
          sampleU(i_point,1:dim+2) = u(inwlt(1),nden:neng)
          sampleSij(i_point,1:Sij_size) = TMPij(inwlt(1),1:Sij_size)
          sampleX(i_point,1:dim)   = x(inwlt(1),1:dim)
       END IF
    END DO

    CALL parallel_vector_sum(REAL=sampleU, LENGTH=n_additional_points*(dim+1))
    CALL parallel_vector_sum(REAL=sampleSij, LENGTH=n_additional_points*Sij_size)
    CALL parallel_vector_sum(REAL=sampleX, LENGTH=n_additional_points*(dim))


    IF (par_rank.EQ.0)  THEN
       !---------- write output file ------------------
       UNIT_USER_POINTS = 345
       INQUIRE(UNIT=UNIT_USER_POINTS, OPENED=err)
       IF(err) THEN
          PRINT *, 'ERROR OPENING ADDITIONAL POINTS FILE: UNIT IN USE'
          STOP
       END IF
       IF(writeheader) THEN
          OPEN (UNIT_USER_POINTS, FILE = TRIM(res_path)//TRIM(file_gen)//'points'//'.stat', FORM='formatted', STATUS='unknown', POSITION='append')
          IF(dim ==3)WRITE(UNIT_USER_POINTS, ADVANCE='YES', FMT='( a )' )  '%Time     Den       XMom       YMom       ZMom      Eng     S11      S22     S33     S12     S13     S23' 
          IF(dim==2)WRITE(UNIT_USER_POINTS, ADVANCE='YES', FMT='( a )' )  '%Time     Den       XMom       YMom       ZMom      Eng     S11      S22     S12' 
          WRITE(UNIT_USER_POINTS, ADVANCE='NO', FMT= '(E12.5)') t
          DO i_point = 1, n_additional_points
             IF (i_point<n_additional_points) THEN
                WRITE(UNIT_USER_POINTS, ADVANCE='NO', FMT= '(4(E12.5))') REAL(i_point,pr),sampleX(i_point,1:dim) 
             ELSE
                WRITE(UNIT_USER_POINTS, ADVANCE='YES', FMT= '(4(E12.5))') REAL(i_point,pr),sampleX(i_point,1:dim) 
             END IF
          END DO
          writeheader = .FALSE.
       END IF

       OPEN (UNIT_USER_POINTS, FILE = TRIM(res_path)//TRIM(file_gen)//'points'//'.stat', FORM='formatted', STATUS='old', POSITION='append')
       WRITE(UNIT_USER_POINTS, ADVANCE='NO', FMT= '(E12.5)') t
       DO i_point = 1, n_additional_points
          IF (i_point<n_additional_points) THEN
             WRITE(UNIT_USER_POINTS, ADVANCE='NO', FMT= '(5(E12.5))') sampleU(i_point,1:dim+1) 
          ELSE
             WRITE(UNIT_USER_POINTS, ADVANCE='YES', FMT= '(5(E12.5))') sampleU(i_point,1:dim+1) 
          END IF
       END DO
       DO i_point = 1, n_additional_points
          IF (i_point<n_additional_points) THEN
             WRITE(UNIT_USER_POINTS, ADVANCE='NO', FMT= '(6(E12.5))') sampleSij(i_point,1:Sij_size) 
          ELSE
             WRITE(UNIT_USER_POINTS, ADVANCE='YES', FMT= '(6(E12.5))') sampleSij(i_point,1:Sij_size) 
          END IF
       END DO

       CLOSE(UNIT_USER_POINTS)
    END IF

  END SUBROUTINE point_stats


  SUBROUTINE plane_stats (u , j_mn_local, startup_flag)
    USE precision
    !    USE fft_module
    !    USE spectra_module
    USE db_tree
    USE wlt_vars
    USE vector_util_mod
    USE parallel            ! par_size
    IMPLICIT NONE
    REAL (pr), DIMENSION (nwlt,1:n_var), INTENT (IN) :: u
    INTEGER , INTENT (IN) :: j_mn_local
    INTEGER , INTENT (IN) :: startup_flag
    CHARACTER (LEN=256)  :: filename


    !================= PLANE STATS ====================
    REAL (pr), DIMENSION(:,:,:,:), ALLOCATABLE :: momU
    REAL (pr), DIMENSION(:,:,:),   ALLOCATABLE :: momMom, momHeat
    REAL (pr), DIMENSION(:,:)    , ALLOCATABLE :: momX, momS12, momKsgs, momUV, momT, momDen
    REAL (pr)                                  :: TMPij(nwlt,2*dim), tmp(nwlt)  !tmp for Sij

    REAL (pr), DIMENSION(nwlt,dim) :: v
    REAL (pr), DIMENSION(nwlt)     :: Temperature

    INTEGER, DIMENSION(:), ALLOCATABLE :: momN, momSHIFT, dir_line
    INTEGER :: dir_homog, mom, inwlt(1)
    INTEGER :: i_plane, j_plane, i_line, proc
    INTEGER :: i, idim, step, ixyz(1:dim), ixyz_plane(1:dim)
    LOGICAL , SAVE :: start = .TRUE.
    !==============================================

    !USER may define additional statistics output here.

    !--Calculate statistics on the additional planes
    IF (par_rank.EQ.0) WRITE(*,'("ITER_STAT = ",I4)')  iter_stat
    IF (additional_planes_active .AND. SUM(prd(1:dim)) == 1 .AND. iter_stat > 0) THEN    !conditional for calculating stats, change for channel flow, 
       !note that periodic directions need to be accounted for
       IF (par_rank.EQ.0) WRITE(*,'("STATISTICS ON THE ADDITIONAL PLANES")')

       j_plane = MIN(j_additional_planes,j_lev)
       step = MAX(1,2**(j_lev-j_plane))

       !Variables to calculate on extra planes
       ALLOCATE(  momU(MAXVAL(nxyz(:)/step+1-prd(:)),1:2,1:dim,n_additional_planes))
       ALLOCATE(  momMom(MAXVAL(nxyz(:)/step+1-prd(:)),1:dim,n_additional_planes))
       ALLOCATE(  momHeat(MAXVAL(nxyz(:)/step+1-prd(:)),1:dim,n_additional_planes))
       ALLOCATE(  momX(MAXVAL(nxyz(:)/step+1-prd(:)),n_additional_planes))
       ALLOCATE(  momT(MAXVAL(nxyz(:)/step+1-prd(:)),n_additional_planes))
       ALLOCATE(  momDen(MAXVAL(nxyz(:)/step+1-prd(:)),n_additional_planes))
       ALLOCATE(  momS12(MAXVAL(nxyz(:)/step+1-prd(:)),n_additional_planes))
       ALLOCATE(  momUV(MAXVAL(nxyz(:)/step+1-prd(:)),n_additional_planes))
       ALLOCATE(  momKsgs(MAXVAL(nxyz(:)/step+1-prd(:)),n_additional_planes))
       ALLOCATE(  momN(n_additional_planes),momSHIFT(n_additional_planes),dir_line(n_additional_planes))

       momU    = 0.0_pr
       momMom  = 0.0_pr
       momHeat = 0.0_pr
       momT    = 0.0_pr
       momDen  = 0.0_pr
       momS12  = 0.0_pr
       momUV   = 0.0_pr
       momKsgs = 0.0_pr

       ! Calculate s_12=s(:,4)
       DO i = 1,3
          v(:,i) = u(:,nvel(i))/u(:,nden)
       END DO
       Temperature = (u(:,neng) - 0.5_pr*SUM(u(:,nvel(1:dim))**2.0_pr,DIM=2)/u(:,nden) )/(cp_in(Nspec)-1.0_pr/MW_in(Nspec)/Ma**2.0_pr)/u(:,nden)
       CALL Sij( v(:,1:dim) ,nwlt, TMPij(:,1:2*dim), tmp(:), .TRUE. )

       ! find periodic direction
       IF(SUM(prd) .NE. 1) THEN
          IF(par_rank .EQ. 0 ) THEN
             PRINT *, 'Wrong periodicity for additional planes'
          END IF
       END IF

       HOMOG_LOOP: DO idim=1,dim
          IF(prd(idim) == 1) THEN
             dir_homog = idim
             EXIT HOMOG_LOOP
          END IF
       END DO HOMOG_LOOP

       DO i_plane = 1,n_additional_planes
          IF(prd(dir_planes(i_plane)) /= 1) THEN  !plane periodic in one direction
             DO idim=1,dim
                IF(idim /= dir_homog .AND. idim /= dir_planes(i_plane)) dir_line(i_plane)=idim  !non-periodic direction of a line that lies in the plane (line of desired profile)
             END DO
             i=0
             DO WHILE (xx(i*STEP,dir_line(i_plane)) <= xyzzone(1,dir_line(i_plane)) .AND. xx(i*STEP,dir_line(i_plane)) < xyzlimits(2,dir_line(i_plane)) )
                i=i+1
             END DO
             momSHIFT(i_plane)=i-1
             i=nxyz(dir_line(i_plane))/STEP
             DO WHILE (xx(i*STEP,dir_line(i_plane)) >= xyzzone(2,dir_line(i_plane)) .AND. xx(i*STEP,dir_line(i_plane)) > xyzlimits(1,dir_line(i_plane)) )
                i=i-1
             END DO
             momN(i_plane)=i-momSHIFT(i_plane)
             DO i=1,momN(i_plane)
                momX(i,i_plane) = xx((momSHIFT(i_plane)+i)*STEP,dir_line(i_plane))
             END DO

             DO idim = 1, dim
                ixyz_plane(idim) = (MINLOC( ABS(xx(0:nxyz(idim)-prd(idim):step,idim) - xyz_planes(idim,i_plane)), DIM = 1  ) - 1 )*step
             END DO
             !---------- go through all points on the plane for  j <= j_plane ------------------
             !-------------------- calculate and write statistics ------------------------------
             ixyz(dir_planes(i_plane)) = ixyz_plane(dir_planes(i_plane))
             DO i_line = 1 ,momN(i_plane)
                ixyz(dir_line(i_plane)) = (momSHIFT(i_plane)+i_line)*STEP
                DO i=0,nxyz(dir_homog)-prd(dir_homog),STEP
                   ixyz(dir_homog) = i
                   CALL DB_get_proc_by_coordinates (ixyz, j_lev, proc)
                   IF (proc.EQ.par_rank) THEN
                      CALL get_indices_by_coordinate(1,ixyz(1:dim),j_lev,inwlt(1), 0)
                      DO idim=1,dim
                         DO mom = 1,2
                            momU(i_line,mom,idim,i_plane) = momU(i_line,mom,idim,i_plane) + ( u(inwlt(1),nvel(idim))/u(inwlt(1),nden)  )**mom
                         END DO
                         momMom(i_line,idim,i_plane) = momMom(i_line,idim,i_plane) +  u(inwlt(1),nvel(idim))  
                         momHeat(i_line,idim,i_plane) = momHeat(i_line,idim,i_plane) +  u(inwlt(1),nvel(idim))*Temperature(inwlt(1))
                      END DO
                      momS12(i_line,i_plane) = momS12(i_line,i_plane) + TMPij(inwlt(1),4)
                      !this can only be used in a compressible SCALES context, commented out until module developed
                      !momKsgs(i_line,i_plane) = momKsgs(i_line,i_plane) + u(inwlt(1),n_var_compressible_K)/u(inwlt(1),nden)
                      momUV(i_line,i_plane) = momUV(i_line,i_plane) + u(inwlt(1),nvel(1))*u(inwlt(1),nvel(2))/u(inwlt(1),nden)**2.0_pr
                      momT(i_line,i_plane) = momT(i_line,i_plane) + Temperature(inwlt(1))
                      momDen(i_line,i_plane) = momDen(i_line,i_plane) + u(inwlt(1),nden)
                   END IF
                END DO
             END DO
             DO  i = 1,momN(i_plane)
                DO idim=1,dim
                   DO mom = 1,2
                      CALL parallel_global_sum( REAL = momU(i,mom,idim,i_plane)  )
                   END DO
                END DO
                CALL parallel_global_sum( REAL = momS12(i,i_plane)  )
                !CALL parallel_global_sum( REAL = momKsgs(i,i_plane)  )
                CALL parallel_global_sum( REAL = momUV(i,i_plane)  )
             END DO
             momU    = momU/(nxyz(dir_homog+1-prd(dir_homog)))*STEP
             momMom  = momMom/(nxyz(dir_homog+1-prd(dir_homog)))*STEP
             momHeat = momHeat/(nxyz(dir_homog+1-prd(dir_homog)))*STEP
             momT    = momT/(nxyz(dir_homog+1-prd(dir_homog)))*STEP
             momDen  = momDen/(nxyz(dir_homog+1-prd(dir_homog)))*STEP
             momUV   = momUV/(nxyz(dir_homog+1-prd(dir_homog)))*STEP
             momS12  = momS12/(nxyz(dir_homog+1-prd(dir_homog)))*STEP
             !momKsgs = momKsgs/(nxyz(dir_homog+1-prd(dir_homog)))*STEP
             IF (par_rank.EQ.0)  THEN
                WRITE(*,'("i_plane=",I4," dir_line=",I2)') i_plane, dir_line(i_plane)
                WRITE(*,'("x",13X,"u",13x,"u2",12x,"v",13x,"v2",12x,"w",13x,"w2",12x,"S12",12x,"Ksgs",13x,"uv")')
                DO  i = 1,momN(i_plane)
                   WRITE(*,'(10(E12.5,2X))') momX(i,i_plane),momU(i,1:2,1:dim,i_plane),momS12(i,i_plane),momKsgs(i,i_plane),momUV(i,i_plane)
                END DO
                !---------- write output file for time-average ------------------
                OPEN (UNIT_USER_STATZ, FILE = file_name_user_statz, FORM='formatted', STATUS='old', POSITION='append')
                WRITE(UNIT_USER_STATZ,ADVANCE='YES', FMT='( a )' ) '%Time         Number      Plane'
                WRITE(UNIT_USER_STATZ,'(E12.5,2X,I6,2X,I6)') t, momN(i_plane), i_plane
                WRITE(UNIT_USER_STATZ,ADVANCE='YES', FMT='( a )' ) &
                     !                   '%Abscissa      u             u^2           v            v^2            w            w^2            S12          Ksgs        uv'
                     '%Abscissa      u             u^2           v            v^2            w            w^2            S12          uv          Den          T          rho*u*T          rho*v*T          rho*w*T          rho*u          rho*v          rho*w'
                DO  i = 1,momN(i_plane)
                   !                  WRITE(UNIT_USER_STATZ,'(10(E12.5,2X))') momX(i,i_plane),momU(i,1:2,1:dim,i_plane),momS12(i,i_plane),momKsgs(i,i_plane),momUV(i,i_plane)
                   WRITE(UNIT_USER_STATZ,'(17(E12.5,2X))') momX(i,i_plane),momU(i,1:2,1:dim,i_plane),momS12(i,i_plane),momUV(i,i_plane),momDen(i,i_plane),momT(i,i_plane),momHeat(i,1:dim,i_plane),momMom(i,1:dim,i_plane)
                END DO
                CLOSE (UNIT_USER_STATZ)
             END IF

          END IF

       END DO

    END IF

    IF (additional_planes_active .AND. SUM(prd(1:dim)) == 1 ) iter_stat = iter_stat + 1
    IF (par_rank.EQ.0) WRITE(*,'("ITER_STAT after = ",I4)')  iter_stat


  END SUBROUTINE plane_stats



  !
  ! Read input from "case_name"_pde.inp file
  ! case_name is in string file_name read from command line
  ! in read_command_line_input()
  ! 
  SUBROUTINE user_read_input()
    IMPLICIT NONE
    INTEGER :: i, j, ind
    CHARACTER(LEN=8):: numstrng, procnum
    INTEGER :: myiter
    LOGICAL :: checkit 
    REAL (pr) :: halfdomsize

    call input_real ('Re',Re,'stop',' Re: Reynolds Number')
    call input_real ('Pra',Pra,'stop',' Pra: Prandtl Number')
    call input_real ('Sc',Sc,'stop',' Sc: Schmidt Number')
    call input_real ('Fr',Fr,'stop',' Fr: Froude Number')
    call input_real ('Ma',Ma,'stop',' Ma: Mach Number')
    call input_real ('S1',S1,'stop',' S1: Sutherland Constant S_1/T_0 (variable viscosity)')
    call input_integer ('Nspec',Nspec,'stop','  Nspec: Number of species')
    call input_integer ('methpd',methpd,'stop','  methpd: meth for taking pressure derivative in time')
    call input_integer ('BCver',BCver,'stop','  BCver: version for BC')
    call input_integer ('ICver',ICver,'stop','  ICver: version for IC')



    Nspec=MAX(Nspec,1)

    ALLOCATE(gr_in(1:Nspec*dim))  
    ALLOCATE(bD_in(1:Nspec))
    ALLOCATE(mu_in(1:Nspec))
    ALLOCATE(kk_in(1:Nspec))
    ALLOCATE(cp_in(1:Nspec))
    ALLOCATE(MW_in(1:Nspec))
    ALLOCATE(gamm(1:Nspec))
    ALLOCATE(pureX(1:Nspec), pureY(1:Nspec), pureR(1:Nspec), pureg(1:Nspec), puregamma(1:Nspec), YR(1:Nspec))

    call input_real ('At',At,'stop',' At: Atwood Number')
    call input_real_vector ('gamm',gamm(1:Nspec),Nspec,'stop',' gamm: cp/(cp-R)')
    call input_real ('peakchange',peakchange,'stop',' peakchange: how far offset to put peaks')
    call input_real ('buffU0',buffU0,'stop', 'buffU0: scale for convect BC')
    buffU0 = buffU0/Ma
    call input_real ('buffSig',buffSig,'stop', 'buffSig: scale for buff BC')
    call input_real ('Lczn',Lczn,'stop', 'Lcoordzn: length of coord_zone on one side')
    call input_real ('tfacczn',tfacczn,'stop', 'tfacczn: factor multiply by time for coord_zone time=L/min(c)*tfac, set <0 for no czn modification')
    call input_real ('itfacczn',itfacczn,'stop', 'itfacczn: factor multiply by initial time for coord_zone itime=Lczn/min(c)*tfac, set <0 to set itime=0')
    call input_real_vector ('buffcvd',buffcvd(1:5),5,'stop', 'buffcvd: convect zone distances')
    call input_real_vector ('buffcvf',buffcvf(1:5),5,'stop', 'buffcvf: convect zone distance fractions')
    call input_real_vector ('buffbfd',buffbfd(1:5),5,'stop', 'buffbfd: buffer zone distances')
    call input_real_vector ('buffbff',buffbff(1:5),5,'stop', 'buffbff: buffer zone distance fractions')

    IF(ALLOCATED(buff_thick)) DEALLOCATE(buff_thick)
    ALLOCATE(buff_thick(2*dim))
    call input_real_vector ('buff_thick',buff_thick(1:2*dim),2*dim,'stop',' buff_thick: thickness of freund buffer region')


    call input_real ('tempscl',tempscl,'stop', 'tempscl: scale for temperature, set < 0 for default')
    call input_real ('specscl',specscl,'stop', 'specscl: scale for species, set < 0 for default')
    call input_real ('mydt_orig',mydt_orig,'stop', 'mydt_orig: max dt for time integration... dt is only initial dt, set <0 to use dt')

    call input_logical ('GRAV',GRAV,'stop', 'GRAV: T to include body force terms, F to ingore them')
    call input_logical ('NS',NS,'stop', 'NS: T to include viscous terms (full Navier-Stokes), F for Euler Eqs')

    call input_real ('grav_coeff',grav_coeff,'stop', 'grav_coeff: coefficient for gravity forcing')

    call input_logical ('adaptAt',adaptAt,'stop', 'adaptAt: T to modify At to get effectivie At = input At due to peakchange')
    call input_logical ('buffBC',buffBC,'stop', 'buffBC: T to use buff zone damping BCs (Brinkman), F to use algebraic BCs')
    call input_logical ('polybuff',polybuff,'stop', 'polybuff: T to use polynomials for buff zone BCs,  F to use tanh')
    call input_logical ('cnvtBC',cnvtBC,'stop', 'cnvtBC: T to use convection zone BCs (Freund), F to use algebraic BCs')
    upwind_cnvtBC = .FALSE.
    IF(cnvtBC) call input_logical ('upwind_cnvtBC',upwind_cnvtBC,'stop', 'upwind_cnvtBC: T to use upwind differencing for convection zone BCs (Freund), F to use central')
    diffBC = .FALSE.
    IF(buffBC .OR. cnvtBC) call input_logical ('diffBC',diffBC,'stop', 'diffBC: T to use diffusive zone damping BCs, F to use algebraic BCs')
    call input_real ('buff_visc',buff_visc,'stop', 'buff_visc: coefficient for diffusion in buffer zone')
    call input_logical ('polyconv',polyconv,'stop', 'polyconv: T to use polynomials for conv zone BCs,  F to use tanh')
    call input_logical ('pBrink',pBrink,'stop', 'pBrink: T to do Brinkman buffer only on pressure')
    call input_logical ('splitFBpress',splitFBpress,'stop', 'splitFBpress: to split derivs for pressure dp/dx FB/BB light/heavy')
    call input_logical ('stepsplit',stepsplit,'stop', 'stepsplit: T to do Heaviside split, F to use X')
    call input_logical ('adapttmp',adapttmp,'stop', 'adapttmp: T to adapt on temperature instead of density and pressure')
    call input_logical ('adaptvelonly',adaptvelonly,'stop', 'adaptvelonly: T to adapt on velocity instead of momentum')
    call input_logical ('adaptspconly',adaptspconly,'stop', 'adaptspconly: T to adapt on mass fraction (Y) instead of volume fraction (rhoY)')
    call input_logical ('adaptden',adaptden,'stop', 'adapt on density')
    call input_logical ('adaptvel',adaptvel,'stop', 'adapt on momentum')
    call input_logical ('adapteng',adapteng,'stop', 'adapt on energy')
    call input_logical ('adaptspc',adaptspc,'stop', 'adapt on species')
    call input_logical ('adaptprs',adaptprs,'stop', 'adapt on pressure')

    call input_logical ('savepardom',savepardom,'stop', 'savepardom: T to save parallel domain as additional var')
    call input_logical ('convcfl',convcfl,'stop', 'convcfl: T to use convective cfl, F to use acoustic cfl')
    call input_logical ('boundY',boundY,'stop', 'boundY: T to bound Y to [0,1] in user_pre/post_process')

    !4extra - these 8 input definitions - MUST ALSO ADD TO .inp 
    call input_logical ('adaptMagVort',adaptMagVort,'stop','adaptMagVort: Adapt on the magnitude of vorticity')
    call input_logical ('adaptComVort',adaptComVort,'stop','adaptComVort: Adapt on the components of vorticity')
    call input_logical ('adaptMagVel',adaptMagVel,'stop','adaptMagVel: Adapt on the magnitude of velocity')
    call input_logical ('adaptNormS',adaptNormS,'stop', 'adaptNormS: Adapt on the L2 of Sij')
    call input_logical ('adaptGradY',adaptGradY,'stop', 'adaptGradY: Adapt on |dY/dx_i*dY/dx_i|')
    call input_logical ('saveMagVort',saveMagVort,'stop', 'saveMagVort: if T adds and saves magnitude of vorticity')
    call input_logical ('saveComVort',saveComVort,'stop', 'saveComVort: if T adds and saves components of vorticity')
    call input_logical ('saveMagVel',saveMagVel,'stop','saveMagVel: if T adds and saves magnitude of velocity')
    call input_logical ('saveNormS',saveNormS,'stop', 'saveNormS: if T adds and saves magnitude of strain rate')
    call input_logical ('saveGradY',saveGradY,'stop', 'saveGradY: if T adds and saves |dY/dx_i*dY/dx_i|')
    call input_logical ('saveUcvt',saveUcvt,'stop', 'saveUcvt: save U and damping support for Freund NRBCs to debug')
    call input_logical ('saveQcrit',saveQcrit,'stop', 'saveQcrit: save Q criterion for visualization')

    call input_real ('MagVortScale',MagVortScale,'stop', 'MagVortScale: scale weight for adaptation on modulus of vorticity')

    FWHstats = .FALSE.
    object_forces_surface = .FALSE.
    object_forces_divstress = .FALSE.
    object_forces_volume = .FALSE.
    call input_logical ('FWHstats',FWHstats,'stop', 'FWHstats: save FWH stats for use in aeroacoustic analogy')
    call input_real ('FWHdt',FWHdt,'stop',' FWHdt: timestep for FWH variables')


    call input_logical ('object_forces_volume',object_forces_volume,'stop', 'calculate and save aerodynamic forces using brinkman volume integration')
    call input_logical ('object_forces_divstress',object_forces_divstress,'stop', 'calculate and save aerodynamic forces using the divergence theorum to integrate the stress over volume') 
    call input_logical ('object_forces_surface',object_forces_surface,'stop', 'calculate and save aerodynamic forces from interpolated surface points')
    surf_pts = 20
    call input_integer ('surf_pts',surf_pts,'stop', 'Number of surface points to use for calculating aerodynamic forces')
    surf_pts_z = 1
    IF(dim ==3) surf_pts_z =  mxyz(3)*2**(j_mx-1)





    IF (imask_obstacle) THEN

       call Brinkman_input

    END IF

    IF (mydt_orig < 0.0_pr) mydt_orig = dt
    IF (IC_restart_mode==1) dt_original = mydt_orig

!!$    IF (buffNe) imask_obstacle=.TRUE.


    IF (Nspec<2) adaptspc=.FALSE. 
    IF (Nspec<2) adaptspconly=.FALSE. 
    IF (Nspec<2) adaptGradY=.FALSE. 
    IF (Nspec<2) saveGradY=.FALSE. 
    IF (Nspec<2) splitFBpress=.FALSE. 
    Nspecm = Nspec - 1


    IF (adaptAt) At=At*(1.0_pr/(1.0_pr-2.0_pr*peakchange))


    !ALIREZA: the body force is defined here
    gr_in(:) = 0.0_pr  !g's by species, then dimension
    !IF (GRAV) gr_in(1:Nspec) = -grav_coeff  !sign change for forcing in the positive x-direction
    grav_coeff_hist = grav_coeff
    Re_bulk_hist = Re
    Re_tau_hist = 200.0_pr


    !    MW_in(:) = 1.0_pr
    MW_in(:) = gamm(:)
    !    MW_in(1) = 1.0_pr+At
    MW_in(1) = MW_in(1)+At
    !    IF (Nspec>1) MW_in(2) = 1.0_pr-At
    IF (Nspec>1) MW_in(2) = MW_in(2)-At
    YR(:) = MW_in(:) / SUM(MW_in(:))
    cp_in(:) = 1.0_pr/MW_in(:)*gamm(:)/(gamm(:)-1.0_pr)/Ma**2.0_pr
    gammR = SUM(cp_in(:)*YR(:))/(SUM(cp_in(:)*YR(:))-SUM(YR(:)/MW_in(:)/Ma**2.0_pr) )
    mu_in(:) = 1.0_pr/Re
    !    kk_in(:) = 1.0_pr/Re/Pra*gammR/(gammR-1.0_pr)
    kk_in(:) = 1.0_pr/Re/Pra/(gammR-1.0_pr)/Ma**2.0_pr

    bD_in(:) = 1.0_pr/Re/Sc

    gammBC(:)=gamm(1)
    IF (Nspec>1) gammBC(2)=gamm(2)

!!$    Tconst = 1.0_pr/Ma**2
!!$    Pint = 1.0_pr/Ma**2

    Tconst = 1.0_pr
    Pint = 1.0_pr

    IF (NOT(ALLOCATED(Lxyz))) ALLOCATE(Lxyz(1:dim))

    IF (NOT(ALLOCATED(Y0top))) ALLOCATE(Y0top(1:Nspec))
    IF (NOT(ALLOCATED(Y0bot))) ALLOCATE(Y0bot(1:Nspec))
    IF (NOT(ALLOCATED(rhoYtopBC))) ALLOCATE(rhoYtopBC(1:Nspecm))
    IF (NOT(ALLOCATED(rhoYbotBC))) ALLOCATE(rhoYbotBC(1:Nspecm))
    IF (NOT(ALLOCATED(rhoYtop))) ALLOCATE(rhoYtop(1:Nspecm))
    IF (NOT(ALLOCATED(rhoYbot))) ALLOCATE(rhoYbot(1:Nspecm))

    Lxyz(1:dim)=xyzlimits(2,1:dim)-xyzlimits(1,1:dim)

    halfdomsize = (xyzlimits(2,1)-xyzlimits(1,1))/2.0_pr



    IF (buffcvd(1)<0.0_pr) THEN
       buffcvd(1)=halfdomsize*buffcvf(1)
    ELSE
       buffcvf(1)=buffcvd(1)/halfdomsize
    END IF
    IF (ANY(buffcvd(2:4)<0.0_pr)) THEN
       buffcvf(2)=MAX(0.0_pr,MIN(1.0_pr,buffcvf(2)))
       buffcvf(3)=MAX(0.0_pr,MIN(1.0_pr-buffcvf(2),buffcvf(3)))
       buffcvf(4)=MAX(0.0_pr,MIN(1.0_pr-SUM(buffcvf(2:3)),buffcvf(4)))
       buffcvf(5)=MAX(0.0_pr,    1.0_pr-SUM(buffcvf(2:4)))
       buffcvd(2:5)=buffcvf(2:5)*buffcvd(1)
    ELSE
       buffcvd(2)=MIN(buffcvd(2),buffcvd(1))
       buffcvd(3)=MIN(buffcvd(3),buffcvd(1)-buffcvd(2))
       buffcvd(4)=MIN(buffcvd(4),buffcvd(1)-SUM(buffcvd(2:3)))
       buffcvd(5)=MAX(0.0_pr  ,buffcvd(1)-SUM(buffcvd(2:4)))
       buffcvf(2:5)=buffcvd(2:5)/buffcvd(1)
    END IF
    IF (verb_level>0 .AND. par_rank==0) PRINT *,'FOR bufferfunc, buffcvd=', buffcvd
    IF (verb_level>0 .AND. par_rank==0) PRINT *,'FOR bufferfunc, buffcvf=', buffcvf

    IF (buffbfd(1)<0.0_pr) THEN
       buffbfd(1)=halfdomsize*buffbff(1)
    ELSE
       buffbff(1)=buffbfd(1)/halfdomsize
    END IF
    IF (ANY(buffbfd(2:4)<0.0_pr)) THEN
       buffbff(2)=MAX(0.0_pr,MIN(1.0_pr,buffbff(2)))
       buffbff(3)=MAX(0.0_pr,MIN(1.0_pr-buffbff(2),buffbff(3)))
       buffbff(4)=MAX(0.0_pr,MIN(1.0_pr-SUM(buffbff(2:3)),buffbff(4)))
       buffbff(5)=MAX(0.0_pr,    1.0_pr-SUM(buffbff(2:4)))
       buffbfd(2:5)=buffbff(2:5)*buffbfd(1)
    ELSE
       buffbfd(2)=MIN(buffbfd(2),buffbfd(1))
       buffbfd(3)=MIN(buffbfd(3),buffbfd(1)-buffbfd(2))
       buffbfd(4)=MIN(buffbfd(4),buffbfd(1)-SUM(buffbfd(2:3)))
       buffbfd(5)=MAX(0.0_pr  ,buffbfd(1)-SUM(buffbfd(2:4)))
       buffbff(2:5)=buffbfd(2:5)/buffbfd(1)
    END IF
    IF (verb_level>0 .AND. par_rank==0) PRINT *,'FOR bufferfunc, buffbfd=', buffbfd
    IF (verb_level>0 .AND. par_rank==0) PRINT *,'FOR bufferfunc, buffbff=', buffbff

    buffDomFrac = MIN(buffcvf(1),buffbff(1))

    IF (ALLOCATED(bndvals)) DEALLOCATE(bndvals)
    ALLOCATE(bndvals(dim+Nspec+3,2*dim))  ! 1-flow variable, 2-boundary


  END SUBROUTINE user_read_input

  !
  ! calculate any additional variables
  ! 
  ! arg 
  ! flag  - 0 calledwhile adapting to IC, 1 - called in main time integration loop
  !
  ! These additiona variables are calculated and left in real space.
  !
  SUBROUTINE user_additional_vars( t_local, flag )
    USE penalization
    IMPLICIT NONE
    REAL (pr), INTENT (IN) ::  t_local
    INTEGER , INTENT(IN) :: flag ! 0- called during adaption to IC, 1 called during main integration loop
    REAL (pr) :: cp, R, mygr, cmax, uval
    INTEGER :: l, i, j, myiter
    REAL (pr), DIMENSION(1,nwlt,MAX(dim,Nspecm,2)) :: du, d2u    
    REAL (pr) :: iterdiff, initdiff, curtau, fofn, deln, delnold, delfofn
    INTEGER, PARAMETER :: meth=1
    REAL (pr), DIMENSION(nwlt,dim*2) :: bigU


    REAL (pr), DIMENSION(nwlt) :: a_wall
    INTEGER :: face_type, nloc, wlt_type, k, j_df
    INTEGER :: ie, i_bnd,pts_total
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc
    REAL (pr) :: a_total, rho_w, rho_m, volume_total, vel_bulk
    REAL (pr), DIMENSION(nwlt,dim) :: v
    REAL (pr), DIMENSION(dim,nwlt,dim) :: dv, d2v

    INTEGER, DIMENSION(nwlt) :: icolor

    CALL user_bufferfunc_eric(bigU, nwlt, buffcvd, polyconv)
    IF(saveUcvt) u(:,ncvtU) = SUM(bigU(:,:),DIM=2)


    d2u(1,:,2) = cp_in(Nspec) 
    d2u(1,:,1) = 1.0_pr/MW_in(Nspec)/Ma**2.0_pr 
    IF (Nspec>1) d2u(1,:,2) = (1.0_pr - SUM(u(:,nspc(1):nspc(Nspecm)),DIM=2)/u(:,nden))*cp_in(Nspec)
    IF (Nspec>1) d2u(1,:,1) = (1.0_pr - SUM(u(:,nspc(1):nspc(Nspecm)),DIM=2)/u(:,nden))/MW_in(Nspec)/Ma**2.0_pr
    DO l=1,Nspecm
       d2u(1,:,2) = d2u(1,:,2) + u(:,nspc(l))/u(:,nden)*cp_in(l) !cp
       d2u(1,:,1) = d2u(1,:,1) + u(:,nspc(l))/u(:,nden)/MW_in(l)/Ma**2.0_pr !R
    END DO

    u(:,nprs) = ( u(:,neng)-0.5_pr*SUM(u(:,nvel(1):nvel(dim))**2,DIM=2)/u(:,nden))/(d2u(1,:,2)/d2u(1,:,1)-1.0_pr)  ! p
    IF (adapttmp) u(:,nton) = u(:,nprs)/u(:,nden)/d2u(1,:,1)
    IF (adaptvelonly) THEN
       DO i=1,dim
          u(:,nvon(i)) = u(:,nvel(i))/u(:,nden)
       END DO
    END IF
    IF (adaptspconly) THEN
       DO i=1,Nspecm
          u(:,nson(i)) = u(:,nspc(i))/u(:,nden)
       END DO
    END IF
    IF( adaptMagVort .OR. saveMagVort .OR. adaptComVort .OR. saveComVort .OR. adaptMagVel .OR. saveMagVel .OR. adaptNormS .OR. saveNormS .OR. saveQcrit) THEN
       DO i=1,dim
          du(1,:,i) = u(:,nvel(i))/u(:,nden)
       END DO
       CALL bonusvars_vel(du(1,:,1:dim))  !4extra - call it
    END IF
    IF( adaptGradY .OR. saveGradY) THEN
       DO i=1,Nspecm
          du(1,:,i) = u(:,nspc(i))/u(:,nden)
       END DO
       CALL bonusvars_spc(du(1,:,1:Nspecm))  !4extra - call it
    END IF

    IF (modczn) THEN
       IF (t>tczn) THEN
          modczn=.FALSE.
          xyzzone(1,1) = czn_min
          xyzzone(2,1) = czn_max            
          IF (par_rank.EQ.0 .AND. verb_level>0) THEN
             PRINT *, '*****************************************************'
             PRINT *, 'FINALIZING TEMPORARY COORD_ZONE'
             PRINT *, 'COORD_ZONE OUTSIDE OF x=[',xyzzone(1,1),',',xyzzone(2,1),']'
             PRINT *, 'TIME: ', t
             PRINT *, 'STOPPING TIME FOR COORD_ZONE: ', tczn
             PRINT *, '*****************************************************'
          END IF
       END IF
    ELSE  
       IF (tfacczn>0.0_pr .AND. itfacczn>0.0_pr .AND. t>itczn) THEN
          itczn=t_end*10.0_pr
          czn_min = xyzzone(1,1)
          czn_max = xyzzone(2,1)
          xyzzone(1,1) = -Lczn
          xyzzone(2,1) =  Lczn
          cczn = MINVAL(SQRT(gamm/MW_in*Tconst))
          tczn = MAXVAL(ABS(xyzlimits(:,1)))/cczn*tfacczn
          IF (par_rank.EQ.0 .AND. verb_level>0) THEN
             PRINT *, '*****************************************************'
             PRINT *, 'APPLYING TEMPORARY COORD_ZONE'
             PRINT *, 'COORD_ZONE OUTSIDE OF x=[',-Lczn,',',Lczn,']'
             PRINT *, 'MINIMUM WAVE SPEED: ', cczn
             PRINT *, 'MAXIMUM DISTANCE: ', MAXVAL(ABS(xyzlimits(:,1)))
             PRINT *, 'TIME: ', t
             PRINT *, 'STARTING TIME FOR COORD_ZONE: ', itczn
             PRINT *, 'TIME SAFETY FACTOR: ', tfacczn
             PRINT *, 'STOPPING TIME FOR COORD_ZONE: ', tczn
             PRINT *, '*****************************************************'
          END IF
          IF (t<tczn) THEN
             modczn = .TRUE.
          ELSE
             IF (par_rank.EQ.0) PRINT *, '!!!TOO LATE TO START COORD_ZONE: t>tczn!!!'
          END IF
       END IF
    END IF



    IF (savepardom) u(:,ndom) = REAL(par_rank,pr)

    IF (imask_obstacle) THEN
       CALL user_dist (nwlt, t_local, DISTANCE=u(:,ndist), COLOR = icolor )
       u(:, ncol) = REAL(icolor, pr)
       u(:,nmsk) = penal
    END IF

    IF (saveUcvt) THEN  !12/2 - modified to carry u = x^2 function to validate surface integrals
!!$          u(:,ncvtU) = 0.0_pr
!!$          DO j = 1,dim
!!$             IF (j == ICver ) THEN
!!$                u(:,ncvtU) = u(:,ncvtU) + (bigU(:,2*j-1) + bigU(:,2*j))*buffU0
!!$             ELSE
!!$                u(:,ncvtU) = u(:,ncvtU) + (-bigU(:,2*j-1) + bigU(:,2*j))*buffU0
!!$             END IF
!!$          END DO

       !u(:,ncvtU) = x(:,1)**2
    END IF


  END SUBROUTINE user_additional_vars

  SUBROUTINE bonusvars_vel(velvec)   !4extra - this whole subroutine
    IMPLICIT NONE
    REAL (pr), INTENT(IN) :: velvec(nwlt,dim) !velocity
    REAL (pr) :: tmp(nwlt,2*dim) !tmp for vorticity
    INTEGER :: i
    REAL (pr), DIMENSION(nwlt,2*dim) :: tmp_array
    REAL (pr), DIMENSION(nwlt,1:2) :: tmp_array2

    IF( adaptMagVort .OR. saveMagVort .OR. adaptComVort .OR. saveComVort .OR. adaptMagVel .OR. saveMagVel .OR. adaptNormS .OR. saveNormS .OR. saveQcrit) THEN
       tmp = 0.0_pr
       ! Calculate the magnitude vorticity
       IF( adaptMagVort .OR. saveMagVort .OR. adaptComVort .OR.  saveComVort ) THEN
          CALL cal_vort (velvec, tmp(:,1:3-MOD(dim,3)), nwlt)
          IF ( adaptMagVort .OR. saveMagVort )  u(:,nmvt) = SQRT(tmp(:,1)**2.0_pr + tmp(:,2)**2.0_pr + tmp(:,3)**2.0_pr )
          IF ( adaptComVort .OR. saveComVort )  THEN
             DO i=1,3
                u(:,ncvt(4-i)) = tmp(:,4-i)
             END DO
          END IF
       END IF
       ! Calculate the magnitude of Sij (sqrt(SijSij) )
       ! s(:,1) = s_11
       ! s(:,2) = s_22
       ! s(:,3) = s_33
       ! s(:,4) = s_12
       ! s(:,5) = s_13
       ! s(:,6) = s_23
       tmp = 0.0_pr
       IF( adaptNormS .OR. saveNormS) THEN
          CALL Sij( velvec ,nwlt, tmp(:,1:2*dim), u(:,nnms), .TRUE. )
       END IF

       IF (saveQcrit .AND. dim .EQ. 3 ) THEN !save Q criterion (Hunt, Wray, Moin 1988)
          CALL Sij( velvec ,nwlt, tmp_array(:,1:2*dim), tmp_array2(:,1), .TRUE. ) !Traceless
          CALL Wij( velvec, nwlt, tmp_array(:,1:dim), tmp_array2(:,2)) 
          u(:,nQcrit) = 0.5_pr*(tmp_array2(:,2)**2.0_pr - tmp_array2(:,1)**2.0_pr) !Q = 1/2(|Omga|^2-|S|^2)
       END IF

       ! Calculate the magnitude of velocity
       IF( adaptMagVel .OR. saveMagVel) THEN
          u(:,nmvl) = 0.5_pr*(SUM(velvec(:,:)**2.0_pr,DIM=2))
       END IF
    END IF
  END SUBROUTINE bonusvars_vel

  SUBROUTINE bonusvars_spc(spcvec)   !4extra - this whole subroutine
    IMPLICIT NONE
    REAL (pr), INTENT(IN) :: spcvec(nwlt,Nspecm) !species
    REAL (pr), DIMENSION(Nspecm,nwlt,dim) :: du, d2u    
    INTEGER :: i
    INTEGER, PARAMETER :: meth=1

    IF( adaptGradY .OR. saveGradY) THEN
       CALL c_diff_fast(spcvec(:,1:Nspecm), du(1:Nspecm,:,:), d2u(1:Nspecm,:,:), j_lev, nwlt,meth, 10, Nspecm, 1, Nspecm)  !dY/dx
       DO i=1,Nspecm
          u(:,ngdy(i)) = SUM(du(i,:,:)**2,DIM=2) !|dY/dx_i*dY/dx_i|
       END DO
    END IF
  END SUBROUTINE bonusvars_spc

  !
  ! calculate any additional scalar variables
  ! 
  ! arg 
  ! flag  - 0 calledwhile adapting to IC, 1 - called in main time integration loop
  !
  SUBROUTINE user_scalar_vars( flag )
    IMPLICIT NONE
    INTEGER , INTENT(IN) :: flag ! 0- called during adaption to IC, 1 called during main integration loop

  END SUBROUTINE user_scalar_vars

  !
  !************ Calculating Scales ***************************
  !
  ! Note the order of the components in the scl array
  ! correspond to u_tn, v_tn, w_tn, u_tn-1, v_tn-1, w_tn-1, u_tn-2, v_tn-2, w_tn-2
  ! 
  SUBROUTINE user_scales(flag,use_default, u_loc, nlocal, ne_local, l_n_var_adapt , l_n_var_adapt_index, &
       scl, scl_fltwt) !add
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: flag ! 0 during initial adaptation, then 1 during time advancement
    LOGICAL , INTENT(INOUT) :: use_default
    INTEGER, INTENT (IN) :: nlocal, ne_local
    REAL (pr), DIMENSION (1:nlocal,1:ne_local), INTENT (IN) :: u_loc
    LOGICAL , INTENT (IN) :: l_n_var_adapt(ne_local)
    INTEGER , INTENT (IN) :: l_n_var_adapt_index(1:ne_local)
    REAL (pr), DIMENSION (1:ne_local), INTENT (INOUT) :: scl
    REAL (pr), DIMENSION (1:ne_local) :: mean
    REAL (pr) ,  INTENT(IN) ::scl_fltwt !weight for temporal filter on scl
    REAL (pr) :: floor, tmp,tmpmax,tmpmin,tmpsum
    INTEGER :: i, ie, ie_index, tmpint
    REAL (pr) ,save, ALLOCATABLE, DIMENSION (:) :: scl_old
    LOGICAL  ,save :: startup_init = .TRUE.

    !
    ! Ignore the output of this routine and use default scales routine
    !
    use_default = .FALSE. 

    IF( .NOT. use_default ) THEN
       !IF (par_rank.EQ.0) PRINT *,'Use user_scales() <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<'
       !
       ! ALLOCATE scl_old if it was not done previously in user_scales
       !
       IF( startup_init .AND. .NOT. ALLOCATED(scl_old) ) THEN
          ALLOCATE( scl_old(1:ne_local) )
          scl_old = 0.0_pr
       END IF

       floor = 1.0e-12_pr
       scl   =1.0_pr
       !
       ! Calculate scl per component
       !
       mean = 0.0_pr
       IF(.TRUE.) THEN
          DO ie=1,ne_local
             IF(l_n_var_adapt(ie)) THEN
                ie_index = l_n_var_adapt_index(ie)
                IF( Scale_Meth == 1 ) THEN ! Use Linf scale
                   tmpmax=MAXVAL ( u_loc(1:nlocal,ie_index) )
                   tmpmin=MINVAL ( u_loc(1:nlocal,ie_index) )
                   CALL parallel_global_sum(REALMAXVAL=tmpmax)
                   CALL parallel_global_sum(REALMINVAL=tmpmin)
                   mean(ie)= 0.5_pr*(tmpmax-tmpmin)
                ELSE IF ( Scale_Meth == 2 ) THEN ! Use L2 scale
                   tmpsum=SUM( u_loc(1:nlocal,ie_index) )
                   tmpint=nlocal
                   CALL parallel_global_sum(REAL=tmpsum)
                   CALL parallel_global_sum(INTEGER=tmpint)
                   mean(ie)= tmpsum/REAL(tmpint,pr)
                ELSE IF ( Scale_Meth == 3 ) THEN ! Use L2 scale using dA
                   tmpsum=SUM( u_loc(1:nlocal,ie_index)*dA )
                   CALL parallel_global_sum(REAL=tmpsum)
                   mean(ie)= tmpsum/ sumdA_global 
                END IF
             END IF
          END DO
       END IF
       DO ie=1,ne_local
          IF(l_n_var_adapt(ie)) THEN
             ie_index = l_n_var_adapt_index(ie)
             IF( Scale_Meth == 1 ) THEN ! Use Linf scale
                scl(ie)= MAXVAL ( ABS( u_loc(1:nlocal,ie_index) - mean(ie) ) )
                CALL parallel_global_sum( REALMAXVAL=scl(ie) )
             ELSE IF ( Scale_Meth == 2 ) THEN ! Use L2 scale
                scl(ie)= SUM( (u_loc(1:nlocal,ie_index)-mean(ie))**2 )
                tmpint=nlocal
                CALL parallel_global_sum( REAL=scl(ie) )
                CALL parallel_global_sum(INTEGER=tmpint)
                scl(ie)=SQRT(scl(ie)/REAL(tmpint,pr))
             ELSE IF ( Scale_Meth == 3 ) THEN ! Use L2 scale using dA
                scl(ie)= SUM( (u_loc(1:nlocal,ie_index)-mean(ie))**2*dA )
                CALL parallel_global_sum(REAL=scl(ie))
                scl(ie)=SQRT(scl(ie)/sumdA_global)
             ELSE
                PRINT *, 'Error in Scales(), Unknown Scale_Meth type: ', Scale_Meth
                PRINT *, 'Known Scale_Meth types: 1 = Linf scale, 2 = L2 scale '
                PRINT *, 'Exiting ...'
                stop
             END IF

             !     IF (par_rank.EQ.0) THEN
             !        WRITE (6,'("Scaling before taking vector(1:dim) magnitude")')
             !        WRITE (6,'("Scale factor eqn", I4 ," = ", es11.4 ," scalecoeff = ",es11.4 )') &
             !             ie, scl(ie), scaleCoeff(ie)
             !     END IF
          END IF
       END DO
       !
       ! take appropriate vector norm over scl(1:dim)
       IF (adaptvelonly) THEN
          IF( Scale_Meth == 1 ) THEN ! Use Linf scale
             scl(nvon(1):nvon(dim)) = MAXVAL(scl(nvon(1):nvon(dim)))
          ELSE IF ( Scale_Meth == 2 ) THEN ! Use L2 scale
             scl(nvon(1):nvon(dim)) = SQRT(SUM(scl(nvon(1):nvon(dim))**2))
          ELSE IF ( Scale_Meth == 3 ) THEN ! Use L2 scale using dA
             scl(nvon(1):nvon(dim)) = SQRT(SUM(scl(nvon(1):nvon(dim))**2))
          END IF
       END IF
       IF( Scale_Meth == 1 ) THEN ! Use Linf scale
          scl(nvel(1):nvel(dim)) = MAXVAL(scl(nvel(1):nvel(dim)))
       ELSE IF ( Scale_Meth == 2 ) THEN ! Use L2 scale
          scl(nvel(1):nvel(dim)) = SQRT(SUM(scl(nvel(1):nvel(dim))**2)) !now velocity vector length, rather than individual component
       ELSE IF ( Scale_Meth == 3 ) THEN ! Use L2 scale using dA
          scl(nvel(1):nvel(dim)) = SQRT(SUM(scl(nvel(1):nvel(dim))**2)) !now velocity vector length, rather than individual component
       END IF
       !
       ! Print out new scl
       !
       IF (adapttmp .AND. tempscl>floor) scl(nton)=tempscl
       IF (adapttmp .AND. adaptvelonly .AND. ABS(tempscl)<floor) scl(nton)=scl(nvon(1))
       IF (adaptspconly .AND. specscl>0.0_pr) scl(nson(1):nson(Nspecm))=specscl
       DO ie=1,ne_local
          IF(l_n_var_adapt(ie)) THEN
             !this is done if one of the variable is exactly zero inorder not to adapt to the noise
             IF(scl(ie) .le. floor) scl(ie)=1.0_pr 
             tmp = scl(ie)
             ! temporally filter scl
             IF(.not. startup_init ) scl(ie) = scl_fltwt * scl_old(ie) + (1.0_pr - scl_fltwt) * scl(ie)
             scl = scaleCoeff * scl
             IF (par_rank.EQ.0 .AND. verb_level .GT. 0) THEN
                WRITE (6,'("Scale factor eqn", I4 ," = ", es11.4 ," scalecoeff = ",es11.4 )') &
                     ie, scl(ie), scaleCoeff(ie)
                WRITE (6,'("(before filtering = ",es11.4,")" )') tmp
             END IF
          END IF
       END DO

       scl_old = scl !save scl for this time step
       startup_init = .FALSE.
    END IF

  END SUBROUTINE user_scales

  SUBROUTINE user_cal_cfl (use_default, ulocal, cfl_out)
    USE precision
    USE sizes
    USE pde
    IMPLICIT NONE
    LOGICAL , INTENT(INOUT) :: use_default
    REAL (pr),                                INTENT (INOUT) :: cfl_out
    REAL (pr), DIMENSION (nwlt,n_integrated), INTENT (IN)    :: ulocal
    INTEGER                    :: i
    REAL (pr)                  :: flor
    REAL (pr), DIMENSION(nwlt,dim) :: cfl, cfl_conv, cfl_acou
    REAL (pr), DIMENSION(dim,nwlt) :: h_arr
    REAL (pr)                  :: min_h
    REAL (pr), DIMENSION(nwlt) :: gam, sos
    INTEGER :: l, mynwlt_global, fullnwlt, maxnwlt, minnwlt, avgnwlt

    IF (it>0) dt_original = mydt_orig

    use_default = .FALSE.

    flor = 1e-12_pr
    cfl_out = flor

    CALL get_all_local_h (h_arr)

    gam(:) = cp_in(Nspec)
    sos(:) = 1.0_pr/MW_in(Nspec)/Ma**2.0_pr 
    IF (Nspec>1) gam(:) = (1.0_pr-SUM(ulocal(:,nspc(1):nspc(Nspecm)),DIM=2)/ulocal(:,nden))*cp_in(Nspec) !cp_Nspec
    IF (Nspec>1) sos(:) = (1.0_pr-SUM(ulocal(:,nspc(1):nspc(Nspecm)),DIM=2)/ulocal(:,nden))/MW_in(Nspec)/Ma**2.0_pr !R_Nspec
    DO l=1,Nspecm
       gam(:) = gam(:) + ulocal(:,nspc(l))/ulocal(:,nden)*cp_in(l) !cp
       sos(:) = sos(:) + ulocal(:,nspc(l))/ulocal(:,nden)/MW_in(l)/Ma**2.0_pr !R
    END DO
    gam(:) = gam(:)/(gam(:)-sos(:))
    sos(:) = SQRT(gam(:)*(gam(:)-1.0_pr)*(ulocal(:,neng)-0.5_pr*SUM(ulocal(:,nvel(1):nvel(dim))**2,DIM=2)/ulocal(:,nden))/ulocal(:,nden))! spd of snd = sqrt(gamma p/rho)
    maxsos = MAXVAL(sos(:))
    CALL parallel_global_sum(REALMAXVAL=maxsos)


    !    maxMa = MAXVAL(    SQRT(SUM(ulocal(:,nvel(1):nvel(dim))**2,DIM=2)/ulocal(:,nden)**2)/sos(:)* SQRT(gam(:))     )
    maxMa = MAXVAL(    SQRT(SUM(ulocal(:,nvel(1):nvel(dim))**2,DIM=2)/ulocal(:,nden)**2)/sos(:))

    CALL parallel_global_sum(REALMAXVAL=maxMa)

    DO i=1,dim
       cfl(:,i)      = ( ABS(ulocal(:,nvel(i))/ulocal(:,nden))+sos(:) ) * dt/h_arr(i,:)
       cfl_conv(:,i) = ( ABS(ulocal(:,nvel(i))/ulocal(:,nden))        ) * dt/h_arr(i,:)
       cfl_acou(:,i) = (                                       sos(:) ) * dt/h_arr(i,:)
    END DO
    CFLreport = MAXVAL(cfl(:,:))
    CFLconv   = MAXVAL(cfl_conv(:,:))
    CFLacou   = MAXVAL(cfl_acou(:,:))
    CALL parallel_global_sum(REALMAXVAL=CFLreport)
    CALL parallel_global_sum(REALMAXVAL=CFLconv)
    CALL parallel_global_sum(REALMAXVAL=CFLacou)
    min_h = MINVAL(h(1,1:dim)/2**(j_mx - 1))
    CALL parallel_global_sum(REALMINVAL=min_h)
    mynwlt_global = nwlt
    fullnwlt = PRODUCT(mxyz(1:dim)*2**(j_lev-1) + 1 - prd(:))
    CALL parallel_global_sum (INTEGER=mynwlt_global)
    avgnwlt = mynwlt_global/par_size 
    maxnwlt = nwlt
    minnwlt = -nwlt
    CALL parallel_global_sum(INTEGERMAXVAL=maxnwlt)
    CALL parallel_global_sum(INTEGERMAXVAL=minnwlt)
    minnwlt=-minnwlt
    MAreport = maxMa
    IF (convcfl) THEN  !use acoustic cfl for initial states when acoustics dominate
       cfl_out = MAX(cfl_out, CFLconv)
    ELSE
       cfl_out = MAX(cfl_out, CFLreport)
    END IF
    IF( (time_integration_method .NE. 1) .AND. (time_integration_method .NE. 2) ) cfl_out = MAX(cfl_out,dt/Re/(min_h**2.0_pr)) !viscous length/time scale
    !calculate current Re_tau

    IF (par_rank.EQ.0) THEN
       PRINT *, '**********************************************************'
       PRINT *, 'case:    ', file_gen(1:LEN_TRIM(file_gen)-1)
       PRINT *, 'max Ma = ', maxMa
       PRINT *, 'CFL    = ', cfl_out
       PRINT *, 'CFLconv= ', CFLconv, 'CFLacou= ', CFLacou
       !PRINT *, 'BL pts = ', 1.0_pr/((Re)**0.5_pr*min_h)
       !PRINT *, 'Re_tau = ', Re_tau
       !PRINT *, 'it     = ', it,             '  t       = ', t
       PRINT *, 'iwrite = ', iwrite-1,       '  dt      = ', dt 
       !PRINT *, 'j_mx   = ', j_mx,           '  dt_orig = ', dt_original
       PRINT *, 'j_mx   = ', j_mx,           '  twrite  = ', twrite
       !PRINT *, 'j_lev  = ', j_lev,          '  twrite  = ', twrite
       PRINT *, 'j_lev  = ', j_lev,          '  n%      = ', REAL(mynwlt_global,pr)/REAL(fullnwlt,pr)
       !PRINT *, 'nwlt_g = ', mynwlt_global,  '  cpu     = ', timer_val(2)
       PRINT *, 'nwlt_g = ', mynwlt_global,  'n_all     = ', fullnwlt
       !PRINT *, 'n_all  = ', fullnwlt,       '  n%      = ', REAL(mynwlt_global,pr)/REAL(fullnwlt,pr)
       !PRINT *, 'nwlt   = ', nwlt,           '  eps     = ', eps
       PRINT *, 'nwlt_mx= ', maxnwlt,        '  nwlt_av = ', avgnwlt
       PRINT *, 'nwlt_mn= ', minnwlt,        '  p_size  = ', par_size
       PRINT *, '**********************************************************'

    END IF
    maxMa = MIN(maxMa,1.0_pr)
  END SUBROUTINE user_cal_cfl

  SUBROUTINE user_init_sgs_model( )
    IMPLICIT NONE
  END SUBROUTINE user_init_sgs_model

  SUBROUTINE  user_sgs_force (u_loc, nlocal)
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: nlocal
    REAL (pr), DIMENSION (nlocal,n_integrated), INTENT (INOUT) :: u_loc
  END SUBROUTINE  user_sgs_force

  FUNCTION user_sound_speed (u, neq, nwlt)
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: nwlt, neq
    REAL (pr), DIMENSION (nwlt,neq), INTENT(IN) :: u
    REAL (pr), DIMENSION (nwlt) :: user_sound_speed,gam

    !user_sound_speed(:) = SQRT( gamm(1)*(gamm(1)-1.0_pr)*(u(:,neng)-0.5_pr*SUM(u(:,nvel(1:dim))**2,DIM=2)/u(:,nden))) ! pressure
    !PRINT *, "c = ", MINVAL(user_sound_speed), MAXVAL(user_sound_speed), "nden = ", nden, MINVAL(u(:, nden)), MAXVAL(u(:, nden))

    gam(:) = cp_in(Nspec)
    user_sound_speed(:) = SQRT(gam(:)*(gam(:)-1.0_pr)*(u(:,neng)-0.5_pr*SUM(u(:,nvel(1):nvel(dim))**2,DIM=2)/u(:,nden))/u(:,nden))! spd of snd = sqrt(gamma p/rho)

  END FUNCTION user_sound_speed

  SUBROUTINE  user_pre_process
    IMPLICIT NONE
    INTEGER :: idim, isp, iii
    REAL (pr), DIMENSION(dim) :: F_i, Fp_i, Tor
    REAL (pr), DIMENSION(dim, 0:Nsp) :: F_n, Fp_n, Tor_n

    REAL(pr), DIMENSION(Nsp,dim) :: F_par,  r_par, v_par
    REAL(pr), DIMENSION(Nsp,2*dim-3) :: T_par, w_par, phi_par ! Ryan - temporary arrays for the lagrangian module
    REAL(pr), DIMENSION(dim,Nsp) :: r_diff, v_diff

    ! Ryan 
    ! Initialize the lagrangian module, if not done already
    IF (.NOT. lagrangian_initialized) THEN
       CALL init(Nsp,rsps,msps,inertias,particle_epsN,particle_epsT)  ! Need to change to multiple obstical variables and add moment of inertia
       lagrangian_initialized = .TRUE.
    END IF

    IF (imask_obstacle) THEN
       IF(ALLOCATED(penal_color) ) THEN
          IF(size(penal_color) /= nwlt) THEN 
             DEALLOCATE(penal_color, norm, u_target)
             ALLOCATE ( penal_color(1:nwlt))
             ALLOCATE ( norm(1:nwlt, 1:dim))
             ALLOCATE ( u_target(1:nwlt, 1:dim))
          END IF
       ELSE
          ALLOCATE ( penal_color(1:nwlt) )
          ALLOCATE ( norm(1:nwlt, 1:dim))
          ALLOCATE ( u_target(1:nwlt, 1:dim) )
       END IF

       CALL user_dist (nwlt, t, DISTANCE=dist, NORMAL=norm, U_TARGET=u_target, COLOR=penal_color)

       IF (t > t_begin) THEN
          CALL user_cal_force(u, F_n, Fp_n, Tor_n)
          
          ! Ryan - Call lagrangian module to get the particle positions and velocities
          F_par = TRANSPOSE(F_n(:,1:Nsp)) ! Need to transpose to get in the shape needed for lagrangian module
          T_par = TRANSPOSE(Tor_n(1:2*dim-3,1:Nsp))  ! 1-component in 2d, 3 in 3d
          r_par = TRANSPOSE(poss(:,1:Nsp))
          v_par = TRANSPOSE(vels(:,1:Nsp))
          phi_par = TRANSPOSE(ang_poss(1:2*dim-3,1:Nsp))
          w_par = TRANSPOSE(ang_vels(1:2*dim-3,1:Nsp))   ! This needs to be included when we add angular velocity
          CALL timeDrivenMDstep(dt,F_par,T_par,t,r_par,v_par,phi_par,w_par)  ! This advances the particles by dt (accounting for collisions) and returns the new values for r_par, v_par, w_par
          poss(:,1:Nsp) = TRANSPOSE(r_par)
          vels(:,1:Nsp) = TRANSPOSE(v_par)
          ang_poss(1:2*dim-3,1:Nsp) = TRANSPOSE(phi_par)
          ang_vels(1:2*dim-3,1:Nsp) = TRANSPOSE(w_par)
          
!!$          DO idim = 1, dim
!!$             accels(idim,1:Nsp) = F_n(idim,  1:Nsp) / msps(1:Nsp)
!!$             poss(idim,  1:Nsp) = poss(idim, 1:Nsp) + vels(idim, 1:Nsp)*dt + 0.5_pr*accels(idim, 1:Nsp)*dt**2
!!$             vels(idim,  1:Nsp) = vels(idim, 1:Nsp) + accels(idim, 1:Nsp)*dt
!!$          END DO
!!$          r_diff(:,:) = poss(:,1:Nsp) - TRANSPOSE(r_par(:,:))
!!$          v_diff(:,:) = vels(:,1:Nsp) - TRANSPOSE(v_par(:,:))
     

       END IF
       IF (par_rank == 0) THEN
          PRINT *, "msps", msps(1:Nsp)
          PRINT *, "F_n", F_n(1,1:Nsp)!, F_n(2,1:Nsp)
          PRINT *, "T_n", Tor_n(1,1:Nsp)
!!$          PRINT *, 'accel: ', accels(1,1:Nsp), accels(2,1:Nsp)
          PRINT *, 'velocity: ', vels(1,1:Nsp)
          PRINT *, 'position: ', poss(1,1:Nsp)
          PRINT *, 'angular velocity: ', ang_vels(1,1:Nsp)

!!$          DO iii=1,Nsp
!!$             PRINT *, 'position diff, particle ', iii,', : ', r_diff(:,iii)
!!$             PRINT *, 'velocity diff, particle ', iii,', : ', v_diff(:,iii)
!!$          END DO
                          
!!$       OPEN(UNIT=431, FILE = TRIM(res_path)//TRIM(file_gen)//'pos_vel_accel'//'.stat', STATUS = 'UNKNOWN', form="FORMATTED", POSITION="APPEND")
!!$       WRITE(431,*) t, accel(1), vel(1), pos(1)
       END IF
       CALL user_dist (nwlt, t, DISTANCE=dist, NORMAL=norm, U_TARGET=u_target, COLOR=penal_color)
       penal = user_chi(nwlt,t)
    END IF
    
  END SUBROUTINE user_pre_process

  SUBROUTINE  user_post_process
    IMPLICIT NONE
!!$    CALL user_pre_process

    IF (par_rank == 0) THEN
       CALL vtk_print(iwrite)
    END IF

  END SUBROUTINE user_post_process





END MODULE user_case
