MODULE user_case
  ! CASE 3D Boussinesq convection
  USE wlt_trns_mod
  USE wlt_trns_util_mod
  ! case specific variables
  REAL (pr) :: Re, Pra, Gr, teps
  INTEGER n_var_pressure  ! start of pressure in u array
CONTAINS
  ! In user_setup_pde() we setup how many variables are integrated, which are interpolated
  ! to the next times step and if any exact solution exists to compare to during the run.
  ! We also set the variable names that are used when the result files are written.
  !
  ! The following variables must be setup in this routine:
  !
  ! n_integrated     ! first n_integrated eqns will be acted on for time integration
  ! n_var_additional ! 
  ! n_var
  SUBROUTINE  user_setup_pde ( VERB ) 
    USE precision
    USE share_vars
    USE share_consts
    USE pde
    USE util_mod
    IMPLICIT NONE
    LOGICAL, OPTIONAL :: VERB         ! print debug info
    INTEGER :: i
    PRINT * ,''

    PRINT *, '**********************Setting up PDE*****************'

    n_integrated = dim+1
    n_time_levels = 3  !--3 time levels
    n_var_time_levels = n_time_levels * n_integrated
    n_var_additional = 1  !--1 pressure at one time level 
    n_var = n_var_time_levels + n_var_additional !--Total number of variables
    n_var_exact = dim+1 !--One exact solution for u,v,w,T
    n_var_pressure  = n_var_time_levels + 1 !pressure

    !--Allocate logical arrays 
    !  Must be done after setting number of different types of variables
    !  and before setting logical variable mapping arrays
    !---------------------------
    CALL alloc_variable_mappings
    ! Fill in variable names for default variables for integration meth 2
    ! These may be changed in the call to the user specified routine
    ! user_setup_pde()
    !
    ! In integrated variables (3/2D)
    IF( dim == 3 ) THEN
       WRITE (u_variable_names(1), u_variable_names_fmt) 'Velocity_u_@t  '
       WRITE (u_variable_names(2), u_variable_names_fmt) 'Velocity_v_@t  '
       WRITE (u_variable_names(3), u_variable_names_fmt) 'Velocity_w_@t  '
       WRITE (u_variable_names(4), u_variable_names_fmt) 'Temperature@t  '
       WRITE (u_variable_names(5), u_variable_names_fmt) 'Velocity_u_@tm1  '
       WRITE (u_variable_names(6), u_variable_names_fmt) 'Velocity_v_@tm1  '
       WRITE (u_variable_names(7), u_variable_names_fmt) 'Velocity_w_@tm1  '
       WRITE (u_variable_names(8), u_variable_names_fmt) 'Temperature@tm1  '
       WRITE (u_variable_names(9), u_variable_names_fmt) 'Velocity_u_@tm2  '
       WRITE (u_variable_names(10), u_variable_names_fmt) 'Velocity_v_@tm2  '
       WRITE (u_variable_names(11), u_variable_names_fmt) 'Velocity_w_@tm2  '
       WRITE (u_variable_names(12), u_variable_names_fmt) 'Temperature@tm2  '
       WRITE (u_variable_names(13), u_variable_names_fmt) 'Pressure  '
    ELSE IF(dim == 2) THEN
       WRITE (u_variable_names(1), u_variable_names_fmt) 'Velocity_u_@t  '
       WRITE (u_variable_names(2), u_variable_names_fmt) 'Velocity_v_@t  '
       WRITE (u_variable_names(3), u_variable_names_fmt) 'Temperature@t  '
       WRITE (u_variable_names(4), u_variable_names_fmt) 'Velocity_u_@tm1  '
       WRITE (u_variable_names(5), u_variable_names_fmt) 'Velocity_v_@tm1  '
       WRITE (u_variable_names(6), u_variable_names_fmt) 'Temperature@tm1  '
       WRITE (u_variable_names(7), u_variable_names_fmt) 'Velocity_u_@tm2  '
       WRITE (u_variable_names(8), u_variable_names_fmt) 'Velocity_v_@tm2  '
       WRITE (u_variable_names(9), u_variable_names_fmt) 'Temperature@tm2  '
       WRITE (u_variable_names(10), u_variable_names_fmt) 'Pressure  '
    END IF
    ! Setup logical variable mapping arrays
    ! This defined which variables are used for adaptation, Which need to be interpololated 
    ! to the new grid at each time step, which are saved, and for which we have an exect
    ! solutiuon we want to check at each time step
    !
    ! Setup which components we will base grid adaptation on.
    ! Adapt on the 3 velocity components
    n_var_adapt(1:dim+1,0) = .TRUE. !--Initially adapt on integrated variables at first time level
    n_var_adapt(1:dim+1,1) = .TRUE. !--After first time step adapt on 

    !Variables that need to be interpoleted to new adapted grid at each time step:
    n_var_interpolate(1:n_var_time_levels+1,0) = .TRUE. !pressure
    !Variables that need to be interpoleted to new adapted grid at each time step
    n_var_interpolate(1:n_var_time_levels+1,1) = .TRUE. 

    ! setup which components we have an exact solution for:
    n_var_exact_soln(1:dim+1,0:1) = .TRUE.

    ! setup which variables we will save in the solution:
    n_var_save(1:n_var) = .TRUE. ! save all for restarting the code

    !--Time level counters for NS integration
    !   Used for integration meth2
    ! n0 is current time step
    ! n1 is t_n-1 time step
    ! n2 is t_n-2 time step
    n0 = 1; n1 = n0+dim+1; n2 = n1+dim+1
    ! Set the maximum number of components for which we have an exact solution:
    n_var_exact = MAX( COUNT(n_var_exact_soln(:,0)),COUNT(n_var_exact_soln(:,1)) )
    PRINT *, 'n_integrated = ',n_integrated 
    PRINT *, 'n_time_levels = ',n_time_levels
    PRINT *, 'n_var_time_levels = ',n_var_time_levels 
    PRINT *, 'n_var = ',n_var 
    PRINT *, 'n_var_exact = ',n_var_exact 
    PRINT *, '*******************Variable Names*******************'
    DO i = 1,n_var
       WRITE (*, u_variable_names_fmt) u_variable_names(i)
    END DO
    PRINT *, '****************************************************'
  END SUBROUTINE  user_setup_pde

  !---------------------------------------------------------------------
  ! Set the exact solution for comparison to the simulated solution:
  ! u          - array to fill in the exact solution
  ! nwlt       - number of active wavelets   
  ! ieq        - total number of equations
  ! t          - time of current time step 
  ! l_n_var_exact_soln_index - index into the elements of u for which we 
  !              need to find the exact solution
  SUBROUTINE  user_exact_soln (u, nwlt,  t_local, l_n_var_exact_soln)  
    USE precision
    USE share_vars
    USE share_consts
    USE pde
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: nwlt
    REAL (pr), INTENT (IN) ::  t_local
    REAL (pr), DIMENSION (nwlt,n_var_exact), INTENT (INOUT) :: u
    LOGICAL , INTENT (IN) :: l_n_var_exact_soln(n_var)
    u(:,1) = 0.0_pr
    u(:,2) =  6.0_pr*Re*x(:,1)*(1.0_pr - x(:,1))
    IF(dim == 2) THEN
       u(:,3) = (1._pr+teps)-2.0_pr*teps*(x(:,1)-xylimits(1,1))&
            /(xylimits(2,1)-xylimits(1,1))
    ELSE
       u(:,3) = 0.0_p
       u(:,4) = (1._pr+teps)-2.0_pr*teps*(x(:,1)-xylimits(1,1))&
            /(xylimits(2,1)-xylimits(1,1))
    END IF
  END SUBROUTINE  user_exact_soln
  
  SUBROUTINE user_initial_conditions (u, nwlt, ieq, t_local, iter)
    USE precision
    USE pde
    USE share_consts
    USE share_vars
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: nwlt, ieq
    INTEGER  , INTENT (INOUT) :: iter ! iteration of call while adapting initial grid
    REAL (pr), DIMENSION (nwlt,ieq), INTENT (INOUT) :: u
    REAL (pr), INTENT (IN) :: t_local
    u(:,1) = 0.e-12_pr
    u(:,2) =  Re !*6.0_pr/*x(:,1)*(1.0_pr - x(:,1) ) 
    IF(dim == 2) THEN
       u(:,3) = (1._pr+teps)-2.0_pr*teps*(x(:,1)-xylimits(1,1))&
            /(xylimits(2,1)-xylimits(1,1))
    ELSE
       u(:,3) = 0.0_p
       u(:,4) = (1._pr+teps)-2.0_pr*teps*(x(:,1)-xylimits(1,1))&
            /(xylimits(2,1)-xylimits(1,1))
    END IF
    WRITE( *,'( "In user_IC t_local, nwlt " , f30.20, 1x , i5.5 )' )&
         t_local , nwlt
  END SUBROUTINE user_initial_conditions

  !--********************************  
  ! Arguments
  ! u         - field on adaptive grid
  ! nwlt      - number of active points
  ! ieq       - number of equations
  ! t         - current time
  !
  ! Global variables used
  ! ifn       - number of equations
  ! x()
  ! x0
  ! y0
  !--******************************** 
  SUBROUTINE user_BC (Lu, u, nlocal, ne, jlev, meth)
    !--Defines boundary condition type
    USE precision
    USE share_vars
    USE share_consts
    USE pde
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: jlev, meth, ne, nlocal
    REAL (pr), DIMENSION (nlocal*ne), INTENT (INOUT) :: Lu
    REAL (pr), DIMENSION (nlocal*ne), INTENT (IN)    :: u
    INTEGER :: ie, ii, shift
    REAL (pr), DIMENSION (nlocal,ne) :: du, d2u
    IF (dim == 3) THEN
       shift = (ne-1) * nlocal  ! temperature gradient
       CALL c_diff_fast(u(shift+1:shift+nlocal),du,d2u,jlev,nlocal,meth1,10)
    END IF
    DO ie = 1, ne
       shift = (ie-1) * nlocal
       DO ii = 1, 2*dim
          IF(ibnd(ii) == 1 .AND. ibc(1)==1) THEN      !--Left boundary  (x==xmin)
             Lu(shift+lv_bnd(2,ii,jlev):shift+lv_bnd(3,ii,jlev)) = &
                  u(shift+lv_bnd(2,ii,jlev):shift+lv_bnd(3,ii,jlev)) !--Dirichlet
          ELSE IF(ibnd(ii) == 2 .AND. ibc(2)==1) THEN !--Right boundary  (x==xmax)
             Lu(shift+lv_bnd(2,ii,jlev):shift+lv_bnd(3,ii,jlev)) =  &
                  u(shift+lv_bnd(2,ii,jlev):shift+lv_bnd(3,ii,jlev)) !--Dirichlet
          ELSE IF(ibnd(ii) == 3 .AND. ibc(3)==1) THEN !--Bottom boundary (y==ymin)
             Lu(shift+lv_bnd(2,ii,jlev):shift+lv_bnd(3,ii,jlev)) =  &
                  u(shift+lv_bnd(2,ii,jlev):shift+lv_bnd(3,ii,jlev)) !--Dirichlet
          ELSE IF(ibnd(ii) == 4 .AND. ibc(4)==1) THEN !--Top boundary  (y==ymax)
             !--Convective (evolution): nothing to specify
          ELSE IF(ibnd(ii) == 5 .AND. ibc(5)==1 .AND. dim==3) THEN !--Front boundary (z==zmin)
             IF(ie == ne) THEN
                Lu(shift+lv_bnd(2,ii,jlev):shift+lv_bnd(3,ii,jlev)) =  &
                     du(lv_bnd(2,ii,jlev):lv_bnd(3,ii,jlev),3) !Temperature
             ELSE
                Lu(shift+lv_bnd(2,ii,jlev):shift+lv_bnd(3,ii,jlev)) =  &
                     u(shift+lv_bnd(2,ii,jlev):shift+lv_bnd(3,ii,jlev))!Velocity
             END IF
          ELSE IF(ibnd(ii) == 6 .AND. ibc(6)==1 .AND. dim==3) THEN !--Behind boundary (z==zmax)
             IF(ie == ne) THEN
                Lu(shift+lv_bnd(2,ii,jlev):shift+lv_bnd(3,ii,jlev)) =  &
                     du(lv_bnd(2,ii,jlev):lv_bnd(3,ii,jlev),3) !Temperature
             ELSE
                Lu(shift+lv_bnd(2,ii,jlev):shift+lv_bnd(3,ii,jlev)) =  &
                     u(shift+lv_bnd(2,ii,jlev):shift+lv_bnd(3,ii,jlev))!Velocity
             END IF
          END IF
       END DO
    END DO
  END SUBROUTINE user_BC

  SUBROUTINE user_BC_diag (Lu, u, nlocal, ne, jlev, meth)
    !--Defines boundary condition type
    USE precision
    USE share_vars
    USE share_consts
    USE pde
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: jlev, meth, ne, nlocal
    REAL (pr), DIMENSION (nlocal*ne), INTENT (INOUT) :: Lu
    REAL (pr), DIMENSION (nlocal*ne), INTENT (IN)    :: u
    INTEGER :: ie, ii, shift
    REAL (pr), DIMENSION (nlocal,ne) :: du, d2u
    IF(dim == 3) THEN
       shift = (ne-1) * nlocal     !temperature
       CALL c_diff_diag (u(shift+1:shift+nlocal), du, d2u, jlev, nlocal, meth, meth, 10)
    END IF
    DO ie = 1, ne
       shift = (1e-1) * nlocal
       DO ii = 1, 2*dim
          IF(ibnd(ii) == 1 .AND. ibc(1)==1) THEN      !--Left boundary  (x==xmin)
             Lu(shift+lv_bnd(2,ii,jlev):shift+lv_bnd(3,ii,jlev)) = 1.0_pr !--Dirichlet
          ELSE IF(ibnd(ii) == 2 .AND. ibc(2)==1) THEN !--Right boundary  (x==xmax)
             Lu(shift+lv_bnd(2,ii,jlev):shift+lv_bnd(3,ii,jlev)) = 1.0_pr !--Dirichlet
          ELSE IF(ibnd(ii) == 3 .AND. ibc(3)==1) THEN !--Bottom boundary (y==ymin)
             Lu(shift+lv_bnd(2,ii,jlev):shift+lv_bnd(3,ii,jlev)) = 1.0_pr !--Dirichlet
          ELSE IF(ibnd(ii) == 4 .AND. ibc(4)==1) THEN !--Top boundary  (y==ymax) 
             !Evolution: nothing to specify
          ELSE IF(ibnd(ii) == 5 .AND. ibc(5)==1 .AND. dim==3) THEN !--Front boundary (z==zmin)
             IF(ie == ne) THEN
                Lu(shift+lv_bnd(2,ii,jlev):shift+lv_bnd(3,ii,jlev)) =  &
                     du(lv_bnd(2,ii,jlev):lv_bnd(3,ii,jlev),3) !Temperature
             ELSE
                Lu(shift+lv_bnd(2,ii,jlev):shift+lv_bnd(3,ii,jlev)) = 1.0_pr !Velocity
             END IF
          ELSE IF(ibnd(ii) == 6 .AND. ibc(6)==1 .AND. dim==3) THEN !--Behind boundary (z==zmax)
             IF(ie == ne) THEN
                Lu(shift+lv_bnd(2,ii,jlev):shift+lv_bnd(3,ii,jlev)) =  &
                     du(lv_bnd(2,ii,jlev):lv_bnd(3,ii,jlev),3) !Temperature
             ELSE
                Lu(shift+lv_bnd(2,ii,jlev):shift+lv_bnd(3,ii,jlev)) = 1.0_pr !Velocity
             END IF
          END IF
       END DO
    END DO
  END SUBROUTINE user_BC_diag

  SUBROUTINE user_rhs_BC (rhs, ne, nlocal)
    !--Sets rhs for boundary conditions
    USE precision
    USE share_vars
    USE share_consts
    USE pde
    USE solution_aux
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: ne, nlocal
    REAL (pr), DIMENSION (nlocal*ne), INTENT (INOUT) :: rhs
    INTEGER :: ie, ii, shift
    DO ie = 1, ne
       shift = (ie-1) * nlocal
       DO ii = 1, 2*dim
          IF(ibnd(ii)==1 .AND. ibc(1)==1) THEN      !--Left boundary
             rhs(shift+lv_bnd(2,ii,j_lev):shift+lv_bnd(3,ii,j_lev)) =   0.0_pr !&
             !-u_b(shift+lv_bnd(2,ii,j_lev):shift+lv_bnd(3,ii,j_lev))
          ELSE IF(ibnd(ii)==2 .AND. ibc(2)==1) THEN !--Right boundary
             rhs(shift+lv_bnd(2,ii,j_lev):shift+lv_bnd(3,ii,j_lev)) =  0.0_pr !&
             !-u_b(shift+lv_bnd(2,ii,j_lev):shift+lv_bnd(3,ii,j_lev))
          ELSE IF(ibnd(ii)==3 .AND. ibc(3)==1) THEN !--Bottom boundary
             rhs(shift+lv_bnd(2,ii,j_lev):shift+lv_bnd(3,ii,j_lev)) =  0.0_pr!&
             !-u_b(shift+lv_bnd(2,ii,j_lev):shift+lv_bnd(3,ii,j_lev)) !--Dirichlet
             IF(ie==2) rhs(shift+lv_bnd(2,ii,j_lev):shift+lv_bnd(3,ii,j_lev)) = 0._pr!&
             !6.0_pr*Re*x(lv_bnd(2,ii,j_lev):lv_bnd(3,ii,j_lev),1)*&
             ! (1.0_pr - x(lv_bnd(2,ii,j_lev):lv_bnd(3,ii,j_lev),1) ) &
             !-u_b(shift+lv_bnd(2,ii,j_lev):shift+lv_bnd(3,ii,j_lev))
             IF((ie==3 .AND. dim==2).OR.(ie==4.AND.dim==3))&!Temperature
                  rhs(shift+lv_bnd(2,ii,j_lev):shift+lv_bnd(3,ii,j_lev)) = 0.0_pr
          ELSE IF(ibnd(ii) == 4 .AND. ibc(4)==1) THEN !--Top boundary 
             ! Evolution
          ELSE IF(ibnd(ii) == 5 .AND. ibc(5)==1 .AND. dim==3) THEN !--Front boundary
             rhs(shift+lv_bnd(2,ii,j_lev):shift+lv_bnd(3,ii,j_lev)) =  0.0_pr!&
             !-u_b(shift+lv_bnd(2,ii,j_lev):shift+lv_bnd(3,ii,j_lev))*0.0_pr !--Dirichlet
          ELSE IF(ibnd(ii) == 6 .AND. ibc(6)==1 .AND. dim==3) THEN !--Behind boundary
             rhs(shift+lv_bnd(2,ii,j_lev):shift+lv_bnd(3,ii,j_lev)) =  0.0_pr!&
             !-u_b(shift+lv_bnd(2,ii,j_lev):shift+lv_bnd(3,ii,j_lev))*0.0_pr !--Dirichlet
          END IF
       END DO
    END DO
  END SUBROUTINE user_rhs_BC

  FUNCTION Lcn (jlev, u, nlocal, ne, meth, clip)
    USE precision
    USE share_vars
    USE sizes
    USE pde
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: jlev, nlocal, ne, meth, clip
    INTEGER :: i, ii, ie, shift
    REAL (pr), DIMENSION (nlocal*ne) :: u
    REAL (pr), DIMENSION (nlocal*ne) :: Lcn
    REAL (pr), DIMENSION (nlocal*ne) :: Jac
    Jac = user_Drhs(u, meth)
    DO ie = 1, ne
       shift = (ie-1) * nlocal
       Lcn(shift+1:shift+nlocal) = u(shift+1:shift+nlocal)/dt&
            - Jac(shift+1:shift+nlocal)/2.0_pr
    END DO
    !--Set operator on boundaries:
    IF(SUM(nbnd*ibc) /= 0) CALL user_BC(Lcn,u,nlocal,ne,jlev,meth)
  END FUNCTION Lcn

  FUNCTION Lcn_diag (jlev, u, nlocal, ne, meth, clip)
    USE precision
    USE share_vars
    USE sizes
    USE pde
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: jlev, nlocal, ne, meth, clip
    INTEGER :: ie, shift
    REAL (pr), DIMENSION (nlocal*ne) :: u
    REAL (pr), DIMENSION (nlocal*ne) :: Lcn_diag
    REAL (pr), DIMENSION (nlocal*ne) :: Jac_diag
    Jac_diag = user_Drhs_diag (u, meth)
    DO ie = 1, ne
       shift = nlocal * (ie-1)
       Lcn_diag(shift+1:shift+nlocal) = 1.0_pr/dt &
            - Jac_diag(shift+1:shift+nlocal)/2.0_pr
    END DO
    !--Set operator on boundaries
    IF(SUM(nbnd*ibc) /= 0) CALL user_BC_diag(Lcn_diag,u,nlocal,ne,jlev,meth)
  END FUNCTION Lcn_diag

  FUNCTION Laplace (jlev, u, nlocal, ne, meth, clip)
    USE precision
    USE share_vars
    USE sizes
    USE pde
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: jlev, nlocal, ne, meth, clip
    REAL (pr), DIMENSION (nlocal*ne), INTENT (INOUT) :: u
    REAL (pr), DIMENSION (nlocal*ne) :: Laplace
    INTEGER :: i, iclip, ii, ie, shift, meth1, meth2, idim
    REAL (pr), DIMENSION (nlocal,dim) :: du, d2u, dum
    meth1 = meth + 2
    meth2 = meth + 4
    
    ! clipping is not used here anyway
!!$    IF (SUM(nbnd) == 0 ) THEN
!!$       iclip = 1
!!$    ELSE 
!!$       iclip = (mxyz(1)-1+prd(1))*(mxyz(2)/2-1+prd(2)) + mxyz(1)/2+prd(1)
!!$    END IF
    
    DO ie = 1, ne
       shift=(ie-1)*nlocal
       !--- Internal points       !--- div(grad)
       CALL c_diff_fast (u(shift+1:shift+nlocal), du, d2u, jlev, nlocal, meth1, 10)
       Laplace(shift+1:shift+lv_intrnl(jlev)) = 0.0_pr
       DO idim = 1,dim
          CALL c_diff_fast (du(1:nlocal,idim), d2u, dum, jlev, nlocal, meth2, 10)        
          Laplace(shift+1:shift+lv_intrnl(jlev))=&
               Laplace(shift+1:shift+lv_intrnl(jlev)) + d2u(1:lv_intrnl(jlev),idim)
       END DO
       !--Boundary points (incorporate compatible boundary conditions as part of operator)
       IF(SUM(nbnd) /= 0) THEN
          DO ii = 1, 2*dim
             IF(ibnd(ii) == 1 ) THEN !--Left boundary
                Laplace(shift+lv_bnd(2,ii,jlev):shift+lv_bnd(3,ii,jlev))=&
                     du(lv_bnd(2,ii,jlev):lv_bnd(3,ii,jlev),1)
             ELSE IF(ibnd(ii) == 2 ) THEN !--Right boundary
                Laplace(shift+lv_bnd(2,ii,jlev):shift+lv_bnd(3,ii,jlev))=&
                     du(lv_bnd(2,ii,jlev):lv_bnd(3,ii,jlev),1)
             ELSE IF(ibnd(ii) == 3 ) THEN !--Bottom boundary (inflow)
                Laplace(shift+lv_bnd(2,ii,jlev):shift+lv_bnd(3,ii,jlev))=&
                     du(lv_bnd(2,ii,jlev):lv_bnd(3,ii,jlev),2)
             ELSE IF(ibnd(ii) == 4 ) THEN !--Top boundary
                Laplace(shift+lv_bnd(2,ii,jlev):shift+lv_bnd(3,ii,jlev))=&
                     du(lv_bnd(2,ii,jlev):lv_bnd(3,ii,jlev),2)
             ELSE IF(ibnd(ii) == 5 .AND. dim==3) THEN !--Front boundary
                Laplace(shift+lv_bnd(2,ii,jlev):shift+lv_bnd(3,ii,jlev))=&
                     du(lv_bnd(2,ii,jlev):lv_bnd(3,ii,jlev),3)
             ELSE IF(ibnd(ii) == 6 .AND. dim==3) THEN !--Behind boundary
                Laplace(shift+lv_bnd(2,ii,jlev):shift+lv_bnd(3,ii,jlev))=&
                     du(lv_bnd(2,ii,jlev):lv_bnd(3,ii,jlev),3)
             END IF
          END DO
       END IF
    END DO
  END FUNCTION Laplace

  FUNCTION Laplace_diag (jlev, u, nlocal, ne, meth, clip)
    USE precision
    USE share_vars
    USE sizes
    USE pde
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: jlev, nlocal, ne, meth, clip
    REAL (pr), DIMENSION (nlocal*ne), INTENT (INOUT) :: u
    REAL (pr), DIMENSION (nlocal*ne) :: Laplace_diag
    INTEGER :: i, iclip, ii, ie, shift, meth1, meth2, idim
    REAL (pr), DIMENSION (nlocal,dim) :: du, d2u
    meth1 = meth + 2
    meth2 = meth + 4

    ! clipping is not used here anyway
!!$    IF (SUM(nbnd) == 0) THEN
!!$       iclip = 1
!!$    ELSE 
!!$       iclip = (mxyz(1)-1+prd(1))*(mxyz(2)/2-1+prd(2)) + mxyz(1)/2+prd(1)
!!$    END IF
    
    DO ie = 1, ne
       shift=(ie-1)*nlocal
       !--- div(grad):
       CALL c_diff_diag(u(shift+1:shift+nlocal),du,d2u,jlev,nlocal,meth1,meth2,-11)
       Laplace_diag(shift+1:shift+lv_intrnl(jlev)) = SUM(d2u(1:lv_intrnl(jlev),1:dim),2)
       !--Boundary points:
       IF(SUM(nbnd) /= 0) THEN
          DO ii = 1, 2*dim
             IF (ibnd(ii) == 1 ) THEN !--Left boundary
                Laplace_diag(shift+lv_bnd(2,ii,jlev):shift+lv_bnd(3,ii,jlev))=&
                     du(lv_bnd(2,ii,jlev):lv_bnd(3,ii,jlev),1)
             ELSE IF(ibnd(ii) == 2 ) THEN !--Right boundary
                Laplace_diag(shift+lv_bnd(2,ii,jlev):shift+lv_bnd(3,ii,jlev))=&
                     du(lv_bnd(2,ii,jlev):lv_bnd(3,ii,jlev),1)
             ELSE IF(ibnd(ii) == 3 ) THEN !--Bottom boundary inflow
                Laplace_diag(shift+lv_bnd(2,ii,jlev):shift+lv_bnd(3,ii,jlev))=&
                     du(lv_bnd(2,ii,jlev):lv_bnd(3,ii,jlev),2)
             ELSE IF(ibnd(ii) == 4 ) THEN !--Top boundary
                Laplace_diag(shift+lv_bnd(2,ii,jlev):shift+lv_bnd(3,ii,jlev))=&
                     du(lv_bnd(2,ii,jlev):lv_bnd(3,ii,jlev),2)
             ELSE IF(ibnd(ii) == 5 .AND. dim==3) THEN !--Front boundary
                Laplace_diag(shift+lv_bnd(2,ii,jlev):shift+lv_bnd(3,ii,jlev))=&
                     du(lv_bnd(2,ii,jlev):lv_bnd(3,ii,jlev),3)
             ELSE IF(ibnd(ii) == 6  .AND. dim==3) THEN !--Behind boundary
                Laplace_diag(shift+lv_bnd(2,ii,jlev):shift+lv_bnd(3,ii,jlev))=&
                     du(lv_bnd(2,ii,jlev):lv_bnd(3,ii,jlev),3)
             END IF
          END DO
       END IF
    END DO
  END FUNCTION Laplace_diag

  FUNCTION Laplace_rhs (u, nlocal, ne, meth)
    USE precision
    USE share_vars
    USE sizes
    USE pde
    USE vector_util_mod
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: nlocal, ne, meth
    REAL (pr), DIMENSION (nlocal,dim), INTENT (IN) :: u
    REAL (pr), DIMENSION (nlocal*ne) :: Laplace_rhs
    INTEGER :: ii, ie, shift, meth1, meth2
    meth1 = meth + 2
    meth2 = meth + 4
    !--find divergence of u, i.e. f = div(u):
    Laplace_rhs = div(u,nlocal,j_lev,meth2)
    !--Boundary points (incorporate compatible boundary conditions as part of operator)
    DO ie = 1, ne
       shift=(ie-1)*nlocal
       IF(SUM(nbnd) /= 0) THEN
          DO ii = 1, 2*dim
             IF(ibnd(ii) == 1 ) THEN !--Left boundary
                Laplace_rhs(shift+lv_bnd(2,ii,j_lev):shift+lv_bnd(3,ii,j_lev))=0.0_pr
             ELSE IF(ibnd(ii) == 2 ) THEN !--Right boundary
                Laplace_rhs(shift+lv_bnd(2,ii,j_lev):shift+lv_bnd(3,ii,j_lev))=0.0_pr
             ELSE IF(ibnd(ii) == 3 ) THEN !--Bottom boundary (inflow)
                Laplace_rhs(shift+lv_bnd(2,ii,j_lev):shift+lv_bnd(3,ii,j_lev))=0.0_pr
             ELSE IF(ibnd(ii) == 4 ) THEN !--Top boundary
                Laplace_rhs(shift+lv_bnd(2,ii,j_lev):shift+lv_bnd(3,ii,j_lev))=0.0_pr
             ELSE IF(ibnd(ii) == 5 .AND. dim==3) THEN !--Front boundary
                Laplace_rhs(shift+lv_bnd(2,ii,j_lev):shift+lv_bnd(3,ii,j_lev))=0.0_pr
             ELSE IF(ibnd(ii) == 6 .AND. dim==3) THEN !--Behind boundary
                Laplace_rhs(shift+lv_bnd(2,ii,j_lev):shift+lv_bnd(3,ii,j_lev))=0.0_pr
             END IF
          END DO
       END IF
    END DO
  END FUNCTION Laplace_rhs

  FUNCTION user_rhs (u_integrated,p)
    USE precision
    USE share_vars
    USE sizes
    USE pde
    USE share_kry
    USE solution_aux
	USE vector_util_mod
	USE field
    IMPLICIT NONE
???????????????!1D flat version of integrated variables without BC:
    REAL (pr), DIMENSION (n), INTENT(IN) :: u_integrated 
    REAL (pr), DIMENSION (ng), INTENT(IN) :: p
    REAL (pr), DIMENSION (n) :: user_rhs
    INTEGER :: ie, shift, idim, ii, shiftt
    INTEGER, PARAMETER :: meth=1
    REAL (pr), DIMENSION (ng,ne) :: dp, du, d2u
    dp = grad (p, ng, j_lev, meth+2)
    !--Form right hand side of Navier-Stokes equations:
    shiftt=(ne-1)*ng
    DO ie = 1,dim 
       shift=(ie-1)*ng
       CALL c_diff_fast(u_integrated(shift+1:shift+ng),du,d2u,j_lev,ng,meth,11)
       user_rhs(shift+1:shift+ng) = SUM(d2u,2) - dp(:,ie)&
            +(12.0_pr*Re+0.5_pr*Gr/teps*(u_integrated(shiftt+1:shiftt+ng)))&
            *REAL(MAX(0,1-ABS(ie-2)),pr)
       DO idim =1, dim !convective term
          user_rhs(shift+1:shift+ng) = user_rhs(shift+1:shift+ng)&
               -u_integrated(ng*(idim-1)+1:idim*ng)*du(:,idim)
       END DO
       !evolution BC
       IF(SUM(nbnd*(ibc-1)) /= 0) THEN
          DO ii = 1, 2*dim
             IF (ibnd(ii) == 1 .AND. ibc(1)==0) THEN !--Left boundary
             ELSE IF(ibnd(ii) == 2 .AND. ibc(2)==0) THEN !--Right boundary
             ELSE IF(ibnd(ii) == 3 .AND. ibc(3)==0) THEN !--Bottom boundary inflow
             ELSE IF(ibnd(ii) == 4 .AND. ibc(4)==0) THEN !--Top boundary
                user_rhs(shift+lv_bnd(2,ii,j_lev):shift+lv_bnd(3,ii,j_lev)) =  &
                     - Re*du(lv_bnd(2,ii,j_lev):lv_bnd(3,ii,j_lev),2) 
!!$                - u_integrated(ng+lv_bnd(2,ii,j_lev):ng+lv_bnd(3,ii,j_lev))*&
!!$                     du(lv_bnd(2,ii,j_lev):lv_bnd(3,ii,j_lev),2) 
             ELSE IF(ibnd(ii) == 5 .AND. ibc(5)==0 .AND. dim==3) THEN !--Front boundary
             ELSE IF(ibnd(ii) == 6 .AND. ibc(6)==0 .AND. dim==3) THEN !--Behind boundary
             END IF
          END DO
       END IF
    END DO
    !Temperature
    shift=shiftt
    CALL c_diff_fast(u_integrated(shift+1:shift+ng), du, d2u, j_lev, ng, meth, 11)
    user_rhs(shift+1:shift+ng) = SUM(d2u,2)/Pra
    DO idim =1, dim !convective term
       user_rhs(shift+1:shift+ng) = user_rhs(shift+1:shift+ng)&
            -u_integrated(ng*(idim-1)+1:idim*ng)*du(:,idim)
    END DO
??????????????????????
    IF(SUM(nbnd*(ibc-1)) /= 0) THEN     !evolution BC
       DO ii = 1, 2*dim
          IF (ibnd(ii) == 1 .AND. ibc(1)==0) THEN !--Left boundary
          ELSE IF(ibnd(ii) == 2 .AND. ibc(2)==0) THEN !--Right boundary
          ELSE IF(ibnd(ii) == 3 .AND. ibc(3)==0) THEN !--Bottom boundary inflow
          ELSE IF(ibnd(ii) == 4 .AND. ibc(4)==0) THEN !--Top boundary
             user_rhs(shift+lv_bnd(2,ii,j_lev):shift+lv_bnd(3,ii,j_lev)) =  &
                  - Re*du(lv_bnd(2,ii,j_lev):lv_bnd(3,ii,j_lev),2) 
          ELSE IF(ibnd(ii) == 5 .AND. ibc(5)==0 .AND. dim==3) THEN !--Front boundary
          ELSE IF(ibnd(ii) == 6 .AND. ibc(6)==0 .AND. dim==3) THEN !--Behind boundary
          END IF
       END DO
    END IF
    !--Set operator on algebraic boundaries:
??????????????
    IF(SUM(nbnd*ibc) /= 0) CALL user_rhs_BC (user_rhs, ne, ng)
  END FUNCTION user_rhs

  !-----------------------------------------------------------
  ! find Jacobian of Right Hand Side of the problem
  FUNCTION user_Drhs (u, meth)
    USE share_kry
    USE solution_aux
    USE precision
    USE pde
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: meth
    REAL (pr), DIMENSION (n) :: u
    REAL (pr), DIMENSION (n) :: user_Drhs
    INTEGER :: ie, shift, idim, ii, shiftt
    REAL (pr), DIMENSION (ng,ne) :: du, d2u
    shiftt=(ne-1)*ng
    DO ie = 1,dim 
       shift=(ie-1)*ng
       user_Drhs(shift+1:shift+ng) = 0.0_pr
       CALL c_diff_fast(u_b(shift+1:shift+ng), du, d2u, j_lev, ng, meth, 10)
       DO idim =1, dim !convective term
          user_Drhs(shift+1:shift+ng) = user_Drhs(shift+1:shift+ng)&
               -u(ng*(idim-1)+1:idim*ng)*du(:,idim)
       END DO
       CALL c_diff_fast(u(shift+1:shift+ng), du, d2u, j_lev, ng, meth, 11)
       DO idim =1, dim !convective term
          user_Drhs(shift+1:shift+ng) = user_Drhs(shift+1:shift+ng)&
               -u_b(ng*(idim-1)+1:idim*ng)*du(:,idim)
       END DO
       user_Drhs(shift+1:shift+ng) = user_Drhs(shift+1:shift+ng) + SUM(d2u,2)&
            +0.5_pr*Gr/teps*(u(shiftt+1:shiftt+ng)))*REAL(MAX(0,1-ABS(ie-2)),pr)
???????????????
       IF(SUM(nbnd*(ibc-1)) /= 0) THEN   !evolution BC
          DO ii = 1, 2*dim
             IF (ibnd(ii) == 1 .AND. ibc(1)==0) THEN !--Left boundary
             ELSE IF(ibnd(ii) == 2 .AND. ibc(2)==0) THEN !--Right boundary
             ELSE IF(ibnd(ii) == 3 .AND. ibc(3)==0) THEN !--Bottom boundary inflow
             ELSE IF(ibnd(ii) == 4 .AND. ibc(4)==0) THEN !--Top boundary
                user_Drhs(shift+lv_bnd(2,ii,j_lev):shift+lv_bnd(3,ii,j_lev)) =  &
                     - Re*du(lv_bnd(2,ii,j_lev):lv_bnd(3,ii,j_lev),2)
             ELSE IF(ibnd(ii) == 5 .AND. ibc(5)==0 .AND. dim==3) THEN !--Front boundary
             ELSE IF(ibnd(ii) == 6 .AND. ibc(6)==0 .AND. dim==3) THEN !--Behind boundary
             END IF
          END DO
       END IF
    END DO
    !Temperature
    shift=shiftt
    user_Drhs(shift+1:shift+ng) = 0.0_pr
    CALL c_diff_fast(u_b(shift+1:shift+ng), du, d2u, j_lev, ng, meth, 10)
    DO idim =1, dim !convective term
       user_Drhs(shift+1:shift+ng) = user_Drhs(shift+1:shift+ng)&
            -u(ng*(idim-1)+1:idim*ng)*du(:,idim)
    END DO
    CALL c_diff_fast(u(shift+1:shift+ng), du, d2u, j_lev, ng, meth, 11)
    DO idim =1, dim !convective term
       user_Drhs(shift+1:shift+ng) = user_Drhs(shift+1:shift+ng)&
            -u_b(ng*(idim-1)+1:idim*ng)*du(:,idim)
    END DO
    user_Drhs(shift+1:shift+ng) = user_Drhs(shift+1:shift+ng) + SUM(d2u,2)/Pra
????????????????????????
!    IF(SUM(nbnd*ibnd) /= 0) THEN    !evolution BC
    IF(SUM(nbnd*(ibc-1)) /= 0) THEN    !evolution BC
       DO ii = 1, 2*dim
          IF (ibnd(ii) == 1 .AND. ibc(1)==0) THEN !--Left boundary
          ELSE IF(ibnd(ii) == 2 .AND. ibc(2)==0) THEN !--Right boundary
          ELSE IF(ibnd(ii) == 3 .AND. ibc(3)==0) THEN !--Bottom boundary inflow
          ELSE IF(ibnd(ii) == 4 .AND. ibc(4)==0) THEN !--Top boundary
             user_Drhs(shift+lv_bnd(2,ii,j_lev):shift+lv_bnd(3,ii,j_lev)) =  &
                  - Re*du(lv_bnd(2,ii,j_lev):lv_bnd(3,ii,j_lev),2)
          ELSE IF(ibnd(ii) == 5 .AND. ibc(5)==0 .AND. dim==3) THEN !--Front boundary
          ELSE IF(ibnd(ii) == 6 .AND. ibc(6)==0 .AND. dim==3) THEN !--Behind boundary
          END IF
       END DO
    END IF
?????????????????????????
  END FUNCTION user_Drhs

  FUNCTION user_Drhs_diag (u, meth)
    USE share_kry
    USE solution_aux
    USE precision
    USE pde
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: meth
    REAL (pr), DIMENSION (n) :: u
    REAL (pr), DIMENSION (n) :: user_Drhs_diag
    INTEGER :: ie, shift, idim, ii, shiftt
    REAL (pr), DIMENSION (ng,dim) :: du, d2u
    shiftt=(ne-1)*ng
    DO ie = 1, dim
       shift=(ie-1)*ng
       CALL c_diff_fast(u_b(shift+1:shift+ng), du, d2u, j_lev, ng, meth, 10)
       user_Drhs_diag(shift+1:shift+ng) = - du(:,ie)
       CALL c_diff_diag(u(shift+1:shift+ng), du, d2u, j_lev, ng, meth, meth, 11)
       DO idim =1, dim !convective term
          user_Drhs_diag(shift+1:shift+ng) = user_Drhs_diag(shift+1:shift+ng)&
               - u_b(ng*(idim-1)+1:idim*ng)*du(:,idim)
       END DO
       user_Drhs_diag(shift+1:shift+ng) = user_Drhs_diag(shift+1:shift+ng) + SUM(d2u,2)
?????????????????????
       IF(SUM(nbnd*(1-ibc) /= 0) THEN        !evolution BC
          DO ii = 1, 2*dim
             IF (ibnd(ii) == 1 .AND. ibc(1)==0) THEN !--Left boundary
             ELSE IF(ibnd(ii) == 2 .AND. ibc(2)==0) THEN !--Right boundary
             ELSE IF(ibnd(ii) == 3 .AND. ibc(3)==0) THEN !--Bottom boundary inflow
             ELSE IF(ibnd(ii) == 4 .AND. ibc(4)==0) THEN !--Top boundary
                user_Drhs_diag(shift+lv_bnd(2,ii,j_lev):shift+lv_bnd(3,ii,j_lev)) =  &
                     -Re*du(lv_bnd(2,ii,j_lev):lv_bnd(3,ii,j_lev),2)
             ELSE IF(ibnd(ii) == 5 .AND. ibc(5)==0 .AND. dim==3) THEN !--Front boundary
             ELSE IF(ibnd(ii) == 6 .AND. ibc(6)==0 .AND. dim==3) THEN !--Behind boundary
             END IF
          END DO
       END IF
    END DO
    !Temperature
    shift=shiftt
    user_Drhs(shift+1:shift+ng) = 0.0_pr
    CALL c_diff_fast(u_b(shift+1:shift+ng), du, d2u, j_lev, ng, meth, 10)
    DO idim =1, dim !convective term
       user_Drhs(shift+1:shift+ng) = user_Drhs(shift+1:shift+ng)&
            -u(ng*(idim-1)+1:idim*ng)*du(:,idim)
    END DO
    CALL c_diff_fast(u(shift+1:shift+ng), du, d2u, j_lev, ng, meth, 11)
    DO idim =1, dim !convective term
       user_Drhs(shift+1:shift+ng) = user_Drhs(shift+1:shift+ng)&
            -u_b(ng*(idim-1)+1:idim*ng)*du(:,idim)
    END DO
    user_Drhs(shift+1:shift+ng) = user_Drhs(shift+1:shift+ng) + SUM(d2u,2)/Pra
??????????????
    IF(SUM(nbnd*(ibc-1) /= 0) THEN     !evolution BC
       DO ii = 1, 2*dim
          IF (ibnd(ii) == 1 .AND. ibc(1)==0) THEN !--Left boundary
          ELSE IF(ibnd(ii) == 2 .AND. ibc(2)==0) THEN !--Right boundary
          ELSE IF(ibnd(ii) == 3 .AND. ibc(3)==0) THEN !--Bottom boundary inflow
          ELSE IF(ibnd(ii) == 4 .AND. ibc(4)==0) THEN !--Top boundary
             user_Drhs(shift+lv_bnd(2,ii,j_lev):shift+lv_bnd(3,ii,j_lev)) =  &
                  - Re*du(lv_bnd(2,ii,j_lev):lv_bnd(3,ii,j_lev),2)
          ELSE IF(ibnd(ii) == 5 .AND. ibc(5)==0 .AND. dim==3) THEN !--Front boundary
          ELSE IF(ibnd(ii) == 6 .AND. ibc(6)==0 .AND. dim==3) THEN !--Behind boundary
          END IF
       END DO
    END IF
????????????????????
  END FUNCTION user_Drhs_diag

  SUBROUTINE user_project (u, p, nlocal, meth)
    !--Makes u divergence free
    USE precision
    USE pde
    USE share_vars
	USE vector_util_mod
    USE elliptic_mod
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: meth, nlocal
    REAL (pr), DIMENSION (nlocal),     INTENT(INOUT) :: p
    REAL (pr), DIMENSION (nlocal,dim), INTENT(INOUT) :: u
    REAL (pr), DIMENSION (nlocal) :: dp
    REAL (pr), DIMENSION (nlocal) :: f
    INTEGER :: ne 
    ne = 1
?????????????????????
    !--Make u divergence free
    dp = 0.0_pr
    f = Laplace_rhs (u, nlocal, ne, meth)
    PRINT *,'Pressure projection'
    CALL Linsolve (dp, f , tol2, nlocal, ne, Laplace, Laplace_diag) 
    u = u - grad(dp, nlocal, j_lev, meth+2)
    p = p + dp/dt
  END SUBROUTINE user_project

  FUNCTION user_chi (nlocal, t_local )
    USE precision
    USE pde
    USE share_vars
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: nlocal
    REAL (pr), INTENT (IN) :: t_local
    REAL (pr), DIMENSION (nlocal) :: user_chi
    user_chi = 0.0_pr
  END FUNCTION user_chi
  !-------------------------------------------------------------------------
  ! Calculate any statitics
  ! startup_flag - 0 when adapting to IC,then 1 inmain integration loop
  SUBROUTINE user_stats ( u ,j_mn, startup_flag)
    USE precision
    USE share_vars
    USE pde
    USE sizes
    USE fft_module
    USE spectra_module
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: startup_flag
    INTEGER , INTENT (IN) :: j_mn 
    REAL (pr), DIMENSION (nwlt,1:dim), INTENT (IN) :: u
  END SUBROUTINE user_stats

  SUBROUTINE user_cal_force (u, n, t_local, force, drag, lift)
    !--Calculates drag and lift on obstacle using penalization formula
    USE precision
    USE pde
    USE share_vars
    USE share_consts
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: n
    REAL (pr), INTENT (IN) :: t_local
    REAL (pr), DIMENSION (dim), INTENT (INOUT) :: force
    REAL (pr), INTENT (OUT) :: drag, lift
    REAL (pr), DIMENSION (n,dim) :: u
    drag = 0.0_PR
    lift = 0.0_PR
    ! There is no obstacle in flow
  END SUBROUTINE user_cal_force

  !-----------------------------------------------------------------
  ! Read input from "case_name"_pde.inp file
  ! case_name is in string file_name read from command line
  ! in read_command_line_input()
  SUBROUTINE user_read_input()
    USE PRECISION
    USE share_vars
    USE share_consts
    USE pde
    USE sizes
    USE share_kry
    USE solution_aux
    USE input_file_reader
    IMPLICIT NONE

!!$  PRINT*,'Reading input file:',TRIM(file_name)//'_pde.inp'
!!$  PRINT*,'********************************************************************'
!!$  call start_reader(TRIM(file_name)//'_pde.inp')      ! initialize general file reader
  
!!$  call input_real ('Re',Re,'stop', &
!!$       ' Re ')
!!$  
!!$  call input_real ('Pr',Pra,'stop', &
!!$       ' Pr ')
!!$  
!!$  call input_real ('Gr',Gr,'stop', &
!!$       ' Gr ')
    Re = 1.0_pr
    Gr = 1.0_pr
    Pra = 0.71_pr
    teps=1.0e-5_pr
  END SUBROUTINE user_read_input

  !------------------------------------------------------------
  ! Read input from "case_name"_geometry.inp file
  ! case_name is in string file_name read from command line
  ! in read_command_line_input()
  SUBROUTINE user_read_input_geometry()
    USE PRECISION
    USE share_vars
    USE share_consts
    USE pde
    USE sizes
    USE share_kry
    USE solution_aux
    IMPLICIT NONE
  END SUBROUTINE user_read_input_geometry

  !-------------------------------------------------------------------------------
  ! calculate any additional variables
  ! arg 
  ! flag  - 0 calledwhile adapting to IC, 1 - called in main time integration loop
  SUBROUTINE user_additional_vars(flag)
    USE PRECISION
    USE share_vars
    USE share_consts
    USE pde
    USE sizes
    USE share_kry
    USE solution_aux
    USE field
    USE wlt_trns_mod
    USE vector_util_mod
    IMPLICIT NONE
    INTEGER,INTENT(IN)::flag!0-called during adaption to IC, 1 called during main integration loop
    ! Calculate the vorticity if we are inthe main integration loop and we
    ! are saving the solution
       IF(flag == 0)  u(1:nwlt,n_var_pressure) = 0.0_pr !initializing pressure for 
!!$    IF (flag == 1 .AND. t  > twrite ) THEN 
!!$       CALL cal_vort(u(:,n0:n0+dim-1), u(:,n_var_vorticity:n_var_vorticity+3-MOD(dim,3)-1), nwlt)
!!$       CALL c_wlt_trns ( &
!!$            u(:,n_var_vorticity:n_var_vorticity+3-MOD(dim,3)-1), &
!!$            u(:,n_var_vorticity:n_var_vorticity+3-MOD(dim,3)-1), n_var, &
!!$            n_var_vorticity,n_var_vorticity+3-MOD(dim,3)-1 , indx, indx,&
!!$            & nwlt, nwlt, j_lev, j_lev, j_lev, j_lev, 1)
!!$    END IF
  END SUBROUTINE user_additional_vars

  !-----------------------------------------------------------------
  ! calculate any additional scalar variables
  ! arg 
  ! flag  - 0 calledwhile adapting to IC, 1 - called in main time integration loop
  SUBROUTINE user_scalar_vars( flag )
    USE PRECISION
    USE share_vars
    USE share_consts
    USE pde
    USE sizes
    USE share_kry
    USE solution_aux
    IMPLICIT NONE
    INTEGER,INTENT(IN)::flag!0-called during adaption to IC, 1 called during main integration loop
  END SUBROUTINE user_scalar_vars

  !************ Calculating Scales ***************************
  ! Note the order of the components in the scl array
  ! correspond to u_tn, v_tn, w_tn, u_tn-1, v_tn-1, w_tn-1, u_tn-2, v_tn-2, w_tn-2
  SUBROUTINE user_scales(use_default, u, nwlt, ieq, l_n_var_adapt , l_n_var_adapt_index, &
       scl, scl_fltwt) !add
    USE precision
    USE pde
    IMPLICIT NONE
    LOGICAL , INTENT(INOUT) :: use_default
    INTEGER, INTENT (IN) :: nwlt, ieq
    REAL (pr), DIMENSION (1:nwlt,1:ieq), INTENT (IN) :: u
    LOGICAL , INTENT (IN) :: l_n_var_adapt(ieq)
    INTEGER , INTENT (IN) :: l_n_var_adapt_index(1:ieq)
    REAL (pr), DIMENSION (1:ieq), INTENT (INOUT) :: scl
    REAL (pr) ,  INTENT(IN) ::scl_fltwt !weight for temporal filter on scl
    ! Ignore the output of this routine and use default scales routine
    use_default = .TRUE. 
    !
    ! NOTE: For a parallel run, synchronize scl(:) across the processors in user_scales.
    !       Use the subroutine scales of default_util.f90 as an example,
    !       subroutine parallel_global_sum of parallel.f90 may be helpful.
    !       Already synchronized: sumdA_global
    !
  END SUBROUTINE user_scales

  SUBROUTINE user_set_dt (use_default, u, n, cfl)
    USE precision
    USE share_vars
    USE pde
    IMPLICIT NONE
    LOGICAL , INTENT(INOUT) :: use_default
    INTEGER,                    INTENT (IN) :: n
    REAL (pr),                  INTENT (IN) :: cfl
    REAL (pr), DIMENSION (n,n_integrated), INTENT (IN) :: u
    INTEGER   :: i, j, k
    INTEGER   :: ix, iy, iz
    REAL (pr) :: dt_new, dtx, dty,dtz, floor, hx, hy, hz
    use_default = .FALSE. ! Use the results of this user version of set_dt
    dt_new = 1.0e12_pr
    floor = 1.0e-12_pr
    DO j = 1, j_lev-1
       DO k = lv(j)+1, lv(j+1)
          i  = nlv(1,k)
          ix = nlv(2,k)
          iy = nlv(3,k)
          iz = nlv(4,k)
          hx = MAX(xx(MIN(ix+2**(j_lev-j-1),nxyz(1)),1)&
               -xx(ix,1),xx(ix,1)-xx(MAX(ix-2**(j_lev-j-1),0),1))
          hy = MAX(xx(MIN(iy+2**(j_lev-j-1),nxyz(2)),2)&
               -xx(iy,2),xx(iy,2)-xx(MAX(iy-2**(j_lev-j-1),0),2))
          IF(dim==3) THEN
             hz = MAX(xx(MIN(iz+2**(j_lev-j-1),nxyz(3)),3)&
                  -xx(iz,3),xx(iz,3)-xx(MAX(iz-2**(j_lev-j-1),0),3))
          END IF
          IF (ABS (u(i,1)) > floor) THEN
             dtx = cfl * hx / (ABS (u(i,1)) + 1.e-12_pr)
          ELSE 
             dtx = 1.0_pr/floor
          END IF
          IF (ABS (u(i,2)) > floor) THEN
             dty = cfl * hy / (ABS (u(i,2)) + 1.e-12_pr)
          ELSE 
             dty = 1.0_pr/floor
          END IF
          IF(dim==3) THEN
             IF (ABS (u(i,3)) > floor) THEN
                dtz = cfl * hz / (ABS (u(i,3) ) + 1.e-12_pr)
             ELSE 
                dtz = 1.0_pr/floor
             END IF
          END IF
          IF (dim==3) THEN
             dt_new = MIN (dt_new, dtx, dty, dtz)
          ELSE
             dt_new = MIN (dt_new, dtx, dty)
          END IF
       END DO
    END DO
    dt = MIN (dt_new, 1.1_pr * dt,dtmax)
  END SUBROUTINE user_set_dt
END MODULE user_case
