MODULE user_case_db

  !
  ! n_var_db must be set to
  !   Count( Union (n_var_adapt, n_var_interpolate))
  ! This is the total number of variable that either are used for adaptation
  ! criteria or need to be interpolated to new grid at each time step
  ! or that are needed for vector derivatives.
  !
  INTEGER , PARAMETER :: n_var_db =  10  !3d
  !INTEGER , PARAMETER :: n_var_db = 5   !2d



END MODULE user_case_db
MODULE user_case
  !
  ! Case isotropic decaying turbulence
  ! Dan Goldstein 11/30/2004
  ! Updated for use with Crank Nicholson time integration format 3/10/2005, Dan Goldstein
  USE precision
  USE wlt_vars
  USE wavelet_filters_mod
  USE elliptic_vars
  USE wlt_trns_mod
  USE wlt_trns_vars
  USE wlt_trns_util_mod
  USE io_3d_vars
  !
  ! case specific variables
  !
  INTEGER n_var_vorticity ! start of vorticity in u array (Must exist)
  INTEGER n_var_pressure  ! start of pressure in u array  (Must exist)

  INTEGER n_var_Ilm       ! location of Ilm  in u array
  INTEGER n_var_Imm       ! location of Imm  in u array
  INTEGER n_var_modvort   ! location of modulus of vorticity
  INTEGER n_var_modSij    ! location of modulus of Sij

  !
  ! Model formulation
  ! 0 - pure convection of Ilm & Imm
  ! 1 - explicit integration of Ilm & Imm
  ! 2 - implicit (except for 1/T, LijMij and MijMij which are calculated once explicitly)
  ! 3 - implicit (except for LijMij and MijMij which are calculated once explicitly)
  !

  REAL (pr) :: nu ! kinematic viscosity (1/Re)
  REAL (pr) :: Cs ! smagorinski model coefficient
  INTEGER , PARAMETER :: mdl_form = 2
  REAL (PR) :: sgsmodel_coef1 ! first model coefficient for model set by sgsmodel (see above)
  REAL (pr) :: LDMtheta !time constant for Langrangian model
  REAL (pr), DIMENSION (:,:) , ALLOCATABLE :: sgs_mdl_force
  REAL (pr), DIMENSION (:) , ALLOCATABLE :: delta, nuI
  REAL (pr) :: CI !CI, langrangian model diffusion scale coefficient
  LOGICAL   :: adaptMagVort, adaptNormS, adaptIlm, adaptImm, SpaceTimeAvg, deltaMij

!------------------ from share_vars module --------------------------------------------------------
  REAL (pr) :: total_resolved_diss, total_sgs_diss
  REAL (pr) :: Mcoeff ! Mij = Mcoeff * |S>2eps| Sij>2eps - (|S|Sij )>2eps
  LOGICAL :: clip_nu_nut ! if true clip nu + nu_t  in the sgs modeling
  LOGICAL :: clip_Cs ! if true clip  in the sgs modeling
  LOGICAL :: Lijtraceless,Mijtraceless ! make Lij and Mij traceless in Dyn model
  LOGICAL :: DynSmodGridFilt !if true |S| is based on grid filt in nu_t=Cdyn|S|
  LOGICAL :: DynSmodGridFiltS !if true S is based on grid filt in tauij = -2 nu_t Sij
  LOGICAL :: DynMdlFiltLijLast ! if true Lij=(ugrid ugrid - u_test u_test )_test
                             ! else  Lij=(ugrid ugrid)_test - u_test u_test 
!--------------------------- adaptive LES for k-based models ------------------------------------
  REAL (pr) :: alpha_k ! portion of the SGS kinetic energy
  REAL (pr) :: eps_min ! minimum allowed epsilon
  REAL (pr) :: eps_max ! maximum allowed epsilon
  REAL (pr) :: q_eps   ! q_eps used for time relazation (user specified parameter)
!------------------------------------------------------------------------------------------------

CONTAINS

  !
  ! In user_setup_pde() we setup how many variables are integrated, which are interpolated
  ! to the next times step and if any exeact solution exists to compare to during the run.
  ! We also set the variable names that are used when the result files are written.
  !
  ! The following variables must be setup in this routine:
  !
  !
  ! n_integrated     ! first n_integrated eqns will be acted on for time integration
  ! n_var_additional ! 
  ! n_var
  !
  !
  !
  !
  SUBROUTINE  user_setup_pde ( VERB ) 
    USE precision
    USE share_consts
    USE pde
    USE util_mod
    IMPLICIT NONE
    LOGICAL, OPTIONAL :: VERB         ! print debug info
    INTEGER :: i

    PRINT * ,''
    PRINT *, '**********************Setting up PDE*****************'
    PRINT * ,'CASE ISOTURB '
    PRINT *, '*****************************************************'

    n_integrated = dim + 2 ! uvw , two scalars Ilm, Imm
    n_time_levels = 1  !--# time levels

    n_var_time_levels = n_time_levels * n_integrated !--Number of equations (2D velocity at two time levels)

    n_var_additional = 1 !--1 pressure at one time level 

    n_var_exact = 0 !--No exact solution 



    

    n_var_Ilm       = dim +1
    n_var_Imm       = dim +2
    n_var_pressure  = dim + 3 !pressure

    IF( adaptMagVort ) THEN
	   n_var_additional = n_var_additional +1
	   n_var_modvort   = n_integrated + n_var_additional ! (wi.wi)^0.5vorticity magnitude
	END IF

	IF( adaptNormS ) THEN
	   n_var_additional = n_var_additional +1
       n_var_modSij    = n_integrated + n_var_additional ! (SijSij)^0.5
    END IF

	n_var = n_var_time_levels + n_var_additional !--Total number of variables


    !
    !--Allocate logical arrays 
    !  Must be done after setting number of different types of variables
    !  and before setting logical variable mapping arrays
    !
    CALL alloc_variable_mappings


    !
    ! Fill in variable names for default variables for integration meth 2
    ! These may be changed in the call to the user specified routine
    !  user_setup_pde()
    !
    ! In integrated variables (3D)
    IF( dim == 3 ) THEN
       WRITE (u_variable_names(1), u_variable_names_fmt) 'Velocity_u_@t  '
       WRITE (u_variable_names(2), u_variable_names_fmt) 'Velocity_v_@t  '
       WRITE (u_variable_names(3), u_variable_names_fmt) 'Velocity_w_@t  '
       WRITE (u_variable_names(4), u_variable_names_fmt) 'Ilm            '
       WRITE (u_variable_names(5), u_variable_names_fmt) 'Imm            '
       WRITE (u_variable_names(6), u_variable_names_fmt) 'Pressure  '
       !WRITE (u_variable_names(5), u_variable_names_fmt) 'Vorticity_u  '
       !WRITE (u_variable_names(6), u_variable_names_fmt) 'Vorticity_v  '
       !WRITE (u_variable_names(7), u_variable_names_fmt) 'Vorticity_w  '
    ELSE IF(dim == 2) THEN
       WRITE (u_variable_names(1), u_variable_names_fmt) 'Velocity_u_@t  '
       WRITE (u_variable_names(2), u_variable_names_fmt) 'Velocity_v_@t  '
       WRITE (u_variable_names(3), u_variable_names_fmt) 'Pressure  '
       !WRITE (u_variable_names(4), u_variable_names_fmt) 'Vorticity_u  '
       !WRITE (u_variable_names(5), u_variable_names_fmt) 'Vorticity_v  '
    END IF


    !
    ! setup logical variable mapping arrays
    ! This defined which variables are used for adaptation, Which need to be interpololated to the new grid at 
    ! each time step, which are saved, and for which we have an exect solutiuon we want to check at each time step
    !


    !
    ! setup which components we will base grid adaptation on.
    ! Adapt on the 3 velocity components
    !
    n_var_adapt(1:dim,0) = .TRUE. !--Initially adapt on integrated variables at first time level
    !eventually adapt to mdl variables    n_var_adapt(1:dim+2,1) = .TRUE. !--After first time step adapt on  velocity and Ilm and Imm

    n_var_adapt(1:dim,1) = .TRUE. !--After first time step adapt on  velocity
	IF( adaptIlm  ) n_var_adapt(n_var_Ilm ,1)  = .TRUE. !adapt in Ilm
    IF( adaptImm  ) n_var_adapt(n_var_Imm ,1)  = .TRUE. !adapt in Imm
	 
    IF( adaptMagVort ) n_var_adapt(n_var_modvort,1)  = .TRUE.
    IF( adaptNormS )   n_var_adapt(n_var_modSij,1 )  = .TRUE.
    

    !Variables that need to be interpoleted to new adapted grid in initial grid adaptation
    n_var_interpolate(1:n_integrated,0) = .TRUE. 

    !Variables that need to be interpoleted to new adapted grid at each time step
    n_var_interpolate(1:n_var_pressure,1) = .TRUE. 

    !
    ! setup which components we have an exact solution for
    !
    n_var_exact_soln(:,0:1) = .FALSE.

    ! variables required for restart
    !
    n_var_req_restart = .FALSE.
    n_var_req_restart(1:n_var_pressure)	= .TRUE.
    !n_var_req_restart(n_var_pressure ) = .TRUE.

    !
    ! setup which variables we will save the solution
    !
    n_var_save(1:n_var_pressure) = .TRUE. ! save all for restarting code

    !
    !--Time level counters for NS integration
    !   Used for integration meth2
    ! n0 is current time step
    ! n1 is t_n-1 time step
    ! n2 is t_n-2 time step
    !
    n0 = 1; n1 = n0+dim; n2 = n1+dim


    !
    ! Set the maximum number of components for which we have an exact solution
    !
    n_var_exact = MAX( COUNT(n_var_exact_soln(:,0)),COUNT(n_var_exact_soln(:,1)) )


    !
	! Setup a scaleCoeff array of we want to tweak the scaling of a variable
	! ( if scaleCoeff < 1.0 then more points will be added to grid 
	!
    ALLOCATE ( scaleCoeff(1:n_var), Umn(1:n_var) )
    scaleCoeff = 1.0_pr
    scaleCoeff(n_var_Ilm)  = 0.1_pr
    scaleCoeff(n_var_Imm)  = 0.1_pr


    Umn = 0.0_pr !set up here if mean quantities anre not zero and used in scales or equation

    PRINT *, 'n_integrated = ',n_integrated 
    PRINT *, 'n_time_levels = ',n_time_levels
    PRINT *, 'n_var_time_levels = ',n_var_time_levels 
    PRINT *, 'n_var = ',n_var 
    PRINT *, 'n_var_exact = ',n_var_exact 
    PRINT *, '*******************Variable Names*******************'
    DO i = 1,n_var
       WRITE (*, u_variable_names_fmt) u_variable_names(i)
    END DO
    PRINT *, '****************************************************'

  END SUBROUTINE  user_setup_pde




  !
  ! read variables from input file for this user case 
  !
  SUBROUTINE  user_read_input()
    USE precision

    USE share_consts
    USE pde
    USE util_mod
	USE input_file_reader
    IMPLICIT NONE

  call input_real ('nu',nu,'stop', &
       ' nu: viscosity')

   call input_integer ('SGS_model',sgsmodel,'stop', &
       ' sgsmodel  !0=no model, >0 chooses a sgs model')

   call input_real ('SGS_model_coef1',sgsmodel_coef1,'stop', &
       ' sgsmodel_coef1 ! first model coefficient for model set by sgsmodel (see above)')

   call input_integer ('mdl_filt_type_grid', mdl_filt_type_grid, 'stop', &
       ' mdl_filt_type_grid ! dyn mdl grid filter (defined in make_mdl_filts() )')

   call input_integer ('mdl_filt_type_test', mdl_filt_type_test, 'stop', &
       ' mdl_filt_type_test ! dyn mdl test filter(defined in make_mdl_filts() )')

   call input_real ('Mcoeff', Mcoeff, 'stop', &
       ' Mcoeff ! Mij = Mcoeff * |S>2eps| Sij>2eps - (|S|Sij )>2eps')

   call input_logical ('clip_nu_nut', clip_nu_nut, 'stop', &
       ' clip_nu_nut ! clip nu+ nu_t if true')

   call input_logical ('clip_Cs', clip_Cs, 'stop', &
       ' clip_Cs ! clip Cs if true')

   call input_logical ('Lijtraceless', Lijtraceless, 'stop', &
       ' Lijtraceless ! make Lij traceless in dyn sgs model')

   call input_logical ('Mijtraceless', Mijtraceless, 'stop', &
       ' Mijtraceless ! make Mij traceless in dyn sgs model')

   call input_logical ('ExplicitFilter', ExplicitFilter, 'stop', &
       ' ExplicitFilter !apply grid filter as an explicit filter each time step')

   call input_logical ('ExplicitFilterNL', ExplicitFilterNL, 'stop', &
       ' ExplicitFilterNL !apply grid filter as an explicit filter to the non-linear term')

   call input_logical ('DynSmodGridFilt', DynSmodGridFilt, 'stop', &
       ' DynSmodGridFilt !if true |S| is based on grid filt in nu_t=Cdyn|S|')

   call input_logical ('DynSmodGridFiltS', DynSmodGridFiltS, 'stop', &
       ' DynSmodGridFiltS !if true S is based on grid filt in tauij = -2 nu_t Sij')

   call input_logical ('DynMdlFiltLijLast', DynMdlFiltLijLast, 'stop', &
       ' ! if true Lij=(ugrid ugrid - u_test u_test )_test ! else  Lij=(ugrid ugrid)_test - u_test u_test')

   call input_real ('LDMtheta', LDMtheta, 'stop', &
       ' LDMtheta, langrangian model time scale coefficient')

   call input_real ('CI', CI, 'stop', &
       ' CI, langrangian model diffusion scale coefficient')

   call input_logical ('adaptIlm',adaptIlm,'stop', &
       '  adaptIlm, Adapt on the Ilm')

   call input_logical ('adaptImm',adaptImm,'stop', &
       '  adaptImm, Adapt on the Imm')

   call input_logical ('adaptMagVort',adaptMagVort,'stop', &
       '  adaptMagVort, Adapt on the magnitude of vorticity')

   call input_logical ('adaptNormS',adaptNormS,'stop', &
       '  adaptNormS, Adapt on the L2 of Sij')

   call input_logical ('SpaceTimeAvg',SpaceTimeAvg,'stop', &
       '  SpaceTimeAvg, Lagrangian spatial filter')

   call input_logical ('deltaMij',deltaMij,'stop', &
       '  deltaMij, Mij definition including delta^2')

!--------------------------- adaptive LES for k-based models ------------------------------------

   call input_real ('alpha_k', alpha_k, 'stop', &
       ' alpha_k, portion of the SGS kinetic energy')

   call input_real ('q_eps', q_eps, 'stop', &
       ' q_eps, q_eps used for time relazation (user specified parameter)')

   call input_real ('eps_min', eps_min, 'stop', &
       ' eps_min, minimum allowed epsilon')

   call input_real ('eps_max', eps_max, 'stop', &
       ' eps_max, maximum allowed epsilon')

!-------------------------------------------------------------------------------------------------

  END SUBROUTINE  user_read_input


  !
  ! Set the exact solution for comparison to the simulated solution
  !
  ! u          - array to fill in the exact solution
  ! nlocal       - number of active wavelets   
  ! ne_local        - total number of equations
  ! t          - time of current time step 
  ! l_n_var_exact_soln_index - index into the elements of u for which we need to 
  !                            find the exact solution
  SUBROUTINE  user_exact_soln (u, nlocal,  t_local, l_n_var_exact_soln)  
    USE precision
    USE share_consts
    USE pde
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: nlocal
    REAL (pr), INTENT (IN) ::  t_local
    REAL (pr), DIMENSION (nlocal,n_var_exact), INTENT (INOUT) :: u
    LOGICAL , INTENT (IN) :: l_n_var_exact_soln(n_var)
    INTEGER :: i


    ! There is no exact solution



  END SUBROUTINE  user_exact_soln

  SUBROUTINE user_initial_conditions (u, nlocal, ne_local, t_local, scl, scl_fltwt, iter)
    USE precision
    USE pde
    USE share_consts
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: nlocal, ne_local
    INTEGER  , INTENT (INOUT) :: iter ! iteration of call while adapting initial grid
    REAL (pr), DIMENSION (nlocal,ne_local), INTENT (INOUT) :: u
    REAL (pr)  :: scl(1:n_var),scl_fltwt
    REAL (pr), INTENT (IN) :: t_local

    INTEGER :: i !TEST

    
    !
    ! Initial conditions read from DNS data file whos name is supplied in the input file
    ! 
    !u = 0.0_pr !TESTING
    ! u(1,:) = 1

    ! 
    ! Initialize scalars
    !
	IF( .NOT. IC_restart ) THEN
       u(:,n_var_Ilm) = 1.0_pr
       u(:,n_var_Imm) = 2.0_pr
    END IF

  END SUBROUTINE user_initial_conditions


!  SUBROUTINE user_initial_conditions (u, nlocal, ne_local, t_local, scl, iter)
!    USE precision
!    USE pde
!    USE share_consts
!    IMPLICIT NONE
!    INTEGER, INTENT (IN) :: nlocal, ne_local
!    INTEGER  , INTENT (INOUT) :: iter ! iteration of call while adapting initial grid
!    REAL (pr), DIMENSION (nlocal,ne_local), INTENT (INOUT) :: u
!   REAL (pr)  :: scl(1:n_var)
!    REAL (pr), INTENT (IN) :: t_local


! END SUBROUTINE user_initial_conditions


  !--********************************  
  ! Arguments
  ! u         - field on adaptive grid
  ! nlocal      - number of active points
  ! ne_local       - number of equations
  ! t         - current time
  !
  ! Global variables used
  ! ifn       - number of equations
  ! x()
  ! xO
  ! y0
  !--******************************** 
  SUBROUTINE user_algebraic_BC (Lu, u, nlocal, ne_local, jlev, meth)
    !--Defines boundary condition type
    USE precision
    USE share_consts
    USE pde
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: jlev, meth, ne_local, nlocal
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (INOUT) :: Lu
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (IN)    :: u

    INTEGER :: ie, ii, shift
    REAL (pr), DIMENSION (nlocal,ne_local) :: du, d2u


    !
    ! There are periodic BC conditions
    !


  END SUBROUTINE user_algebraic_BC

  SUBROUTINE user_algebraic_BC_diag (Lu, nlocal, ne_local, jlev, meth)
    !--Defines boundary condition type
    USE precision
    USE share_consts
    USE pde
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: jlev, meth, ne_local, nlocal
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (INOUT) :: Lu

    INTEGER :: ie, ii, shift
    REAL (pr), DIMENSION (nlocal,ne_local) :: du, d2u

    !
    ! There are periodic BC conditions
    !

  END SUBROUTINE user_algebraic_BC_diag



  SUBROUTINE user_algebraic_BC_rhs (rhs, ne_local, nlocal, jlev)
    !--Sets rhs for boundary conditions
    USE precision
    USE share_consts
    USE pde
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: ne_local, nlocal , jlev
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (INOUT) :: rhs

    INTEGER :: ie, ii, shift
    !
    ! There are periodic BC conditions
    !

  END SUBROUTINE user_algebraic_BC_rhs


  FUNCTION Laplace (jlev, u, nlocal, ne_local, meth, clip)
    USE precision
    USE sizes
    USE pde
    USe parallel
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: jlev, nlocal, ne_local, meth, clip
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (INOUT) :: u
    REAL (pr), DIMENSION (nlocal*ne_local) :: Laplace

    INTEGER :: i, iclip, ii, ie, shift, meth1, meth2, idim
    REAL (pr), DIMENSION (ne_local ,nlocal,dim) :: du
    REAL (pr), DIMENSION (ne_local*dim ,nlocal,dim) :: d2u
    REAL (pr), DIMENSION (ne_local*dim ,nlocal,dim) :: du_dummy ! passed when only calculating 1st derivative.
    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc
    REAL(pr) :: u_clip

    meth1 = meth + 2
    meth2 = meth + 4
    IF (SUM(nbnd) == 0) THEN
       iclip = 1
    ELSE 
       iclip = Nwlt_lev(jlev,0) + 1 !+ mxyz(2)/2
       !                            ! commented out to avoid problems in parallel
    END IF


    !
    ! Find 1st deriviative of u. 
    !
    CALL c_diff_fast (u, du, d2u, jlev, nlocal, meth1, 10, ne_local, 1, ne_local)


    !
    ! Find 2nd deriviative of u.  d( du ) 
    !
    IF ( TYPE_DB == DB_TYPE_WRK ) THEN 
       CALL c_diff_fast(du, d2u, du_dummy, jlev, nlocal, meth2, 10, ne_local*dim, 1, ne_local*dim )
    ELSE !db

       ! Load du  into db
       DO ie=1,ne_local		
          CALL update_db_from_u(  du(ie,1:nlocal,1:dim)  , nlocal ,dim  , 1, dim, 1+(ne_local-1)*dim  ) !Load du	    
       END DO

       ! find  2nd deriviative of u (so we first derivative of du/dx)
       CALL c_diff_fast_db(d2u, du_dummy, jlev, nlocal, meth2, 10, ne_local*dim,&
            1,         &  ! MIN(mn_varD,mn_varD2)
            ne_local*dim,    &  ! MAX(mx_varD,mx_varD2)
            1,         &  ! mn_varD  :   min variable in db that we are doing the 1st derivatives for.
            ne_local*dim,    &  ! mn_varD  :   max variable in db that we are doing the 1st derivatives for.
            0 ,        &  ! mn_varD2 :   min variable in db that we are doing the 2nd derivatives for.
            0     )  ! mn_varD2 :   max variable in db that we are doing the 2nd derivatives for.

    END IF
    ! Now:
    ! d2u(1,:,1) = d^2 U/ d^2 x
    ! d2u(1,:,2) = d^2 U/ d^2 y
    ! d2u(1,:,3) = d^2 U/ d^2 z
    ! d2u(2,:,1) = d^2 V/ d^2 x
    ! d2u(2,:,2) = d^2 V/ d^2 y
    ! d2u(2,:,3) = d^2 V/ d^2 z
    ! d2u(3,:,1) = d^2 W/ d^2 x
    ! d2u(3,:,2) = d^2 W/ d^2 y
    ! d2u(3,:,3) = d^2 W/ d^2 z

    DO ie = 1, ne_local
       shift=(ie-1)*nlocal
       !--- Internal points
       !--- div(grad)
       idim = 1
       Laplace(shift+1:shift+Nwlt_lev(jlev,0)) = d2u( (ie-1)*dim+idim ,1:Nwlt_lev(jlev,0),1)
       DO idim = 2,dim
          Laplace(shift+1:shift+Nwlt_lev(jlev,0)) = Laplace(shift+1:shift+Nwlt_lev(jlev,0)) + &
               d2u((ie-1)*dim+idim ,1:Nwlt_lev(jlev,0),idim)
       END DO
       !--Boundary points
       i_p_face(0) = 1
       DO i=1,dim
          i_p_face(i) = i_p_face(i-1)*3
       END DO
       DO face_type = 0, 3**dim - 1
          face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
          IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
             CALL get_all_indices_by_face (face_type, jlev, nloc, iloc)
             IF(nloc > 0 .AND. dim == 3) THEN ! 3-D case only
                IF( ALL( face == (/-1,0,0/) ) ) THEN  ! Xmin face, no edges or corners
                   Laplace(shift+iloc(1:nloc)) = u(shift+iloc(1:nloc))  !Dirichlet conditions
                ELSE IF( ALL( face == (/1,0,0/) ) ) THEN  ! Xmax face, no edges or corners
                   Laplace(shift+iloc(1:nloc)) = u(shift+iloc(1:nloc))  !Dirichlet conditions
!!$                Laplace(shift+iloc(1:nloc)) = du(ie,iloc(1:nloc),1)  !Neuman conditions
                ELSE IF( ALL( face(2:dim) == (/-1,0/) ) ) THEN  ! Ymin face, edges || to Z, no corners
                   Laplace(shift+iloc(1:nloc)) = u(shift+iloc(1:nloc))  !Dirichlet conditions
!!$                Laplace(shift+iloc(1:nloc)) = du(ie,iloc(1:nloc),2)  !Neuman conditions
                ELSE IF( ALL( face(2:dim) == (/1,0/) ) ) THEN  ! Ymax face, edges || to Z, no corners
                   Laplace(shift+iloc(1:nloc)) = u(shift+iloc(1:nloc))  !Dirichlet conditions
!!$                Laplace(shift+iloc(1:nloc)) = du(ie,iloc(1:nloc),2)  !Neuman conditions
                ELSE IF( face(3) == -1 ) THEN  ! entire Zmin face
                   Laplace(shift+iloc(1:nloc)) = u(shift+iloc(1:nloc))  !Dirichlet conditions
!!$                Laplace(shift+iloc(1:nloc)) = du(ie,iloc(1:nloc),3)  !Neuman conditions
                ELSE IF( face(3) == 1 ) THEN  ! entire Zmax face
                   Laplace(shift+iloc(1:nloc)) = u(shift+iloc(1:nloc))  !Dirichlet conditions
!!$                Laplace(shift+iloc(1:nloc)) = du(ie,iloc(1:nloc),3)  !Neuman conditions
                END IF
             END IF
          END IF
       END DO
       IF (clip == 1) THEN
          u_clip = u(shift+iclip)
          CALL parallel_broadcast( REAL=u_clip )
          Laplace(shift+iclip)= u_clip
       END IF
    END DO
  END FUNCTION Laplace

  FUNCTION Laplace_diag (jlev,  nlocal, ne_local, meth, clip)
    USE precision
    USE sizes
    USE pde
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: jlev, nlocal, ne_local, meth, clip
    REAL (pr), DIMENSION (nlocal*ne_local) :: Laplace_diag

    INTEGER :: i, iclip, ii, ie, shift, meth1, meth2
    REAL (pr), DIMENSION (nlocal,dim) :: du, d2u
    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc

    meth1 = meth + 2
    meth2 = meth + 4

    IF (SUM(nbnd) == 0) THEN
       iclip = 1
    ELSE 
       iclip = Nwlt_lev(jlev,0) + 1 !+ mxyz(2)/2
       !                            ! commented out to avoid problems in parallel
    END IF

    !PRINT *,'IN Laplace_diag, ne_local = ', ne_local

    DO ie = 1, ne_local
       shift=(ie-1)*nlocal
       !--- div(grad)
       !PRINT *,'CAlling c_diff_diag from Laplace_diag() '
       !PRINT *,'--- jlev, nlocal, meth1, meth2', jlev, nlocal, meth1, meth2

       CALL c_diff_diag ( du, d2u, jlev, nlocal, meth1, meth2, -11)

       Laplace_diag(shift+1:shift+Nwlt_lev(jlev,0)) = d2u(1:Nwlt_lev(jlev,0),1) + d2u(1:Nwlt_lev(jlev,0),2)
       IF (dim==3) Laplace_diag(shift+1:shift+Nwlt_lev(jlev,0)) = &
            Laplace_diag(shift+1:shift+Nwlt_lev(jlev,0))+d2u(1:Nwlt_lev(jlev,0),3)
       !--Boundary points
       i_p_face(0) = 1
       DO i=1,dim
          i_p_face(i) = i_p_face(i-1)*3
       END DO
       DO face_type = 0, 3**dim - 1
          face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
          IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
             CALL get_all_indices_by_face (face_type, jlev, nloc, iloc)
             IF(nloc > 0 .AND. dim == 3) THEN ! 3-D case only
                IF( ALL( face == (/-1,0,0/) ) ) THEN                         ! Xmin face, no edges or corners
                   Laplace_diag(shift+iloc(1:nloc)) = 1.0_pr                 !Dirichlet conditions
                ELSE IF( ALL( face == (/1,0,0/) ) ) THEN                     ! Xmax face, no edges or corners
                   Laplace_diag(shift+iloc(1:nloc)) = 1.0_pr                 !Dirichlet conditions
!!$                Laplace_diag(shift+iloc(1:nloc)) = du(iloc(1:nloc),1)     !Neuman conditions
                ELSE IF( ALL( face(2:dim) == (/-1,0/) ) ) THEN               ! Ymin face, edges || to Z, no corners
                   Laplace_diag(shift+iloc(1:nloc)) = 1.0_pr                 !Dirichlet conditions
!!$                Laplace_diag(shift+iloc(1:nloc)) = du(iloc(1:nloc),2)     !Neuman conditions
                ELSE IF( ALL( face(2:dim) == (/1,0/) ) ) THEN                ! Ymax face, edges || to Z, no corners
                   Laplace_diag(shift+iloc(1:nloc)) = 1.0_pr                 !Dirichlet conditions
!!$                Laplace_diag(shift+iloc(1:nloc)) = du(iloc(1:nloc),2)     !Neuman conditions
                ELSE IF( face(3) == -1 ) THEN                                ! entire Zmin face
                   Laplace_diag(shift+iloc(1:nloc)) = 1.0_pr                 !Dirichlet conditions
!!$                Laplace_diag(shift+iloc(1:nloc)) = du(iloc(1:nloc),3)     !Neuman conditions
                ELSE IF( face(3) == 1 ) THEN                                 ! entire Zmax face
                   Laplace_diag(shift+iloc(1:nloc)) = 1.0_pr                 !Dirichlet conditions
!!$                Laplace_diag(shift+iloc(1:nloc)) = du(iloc(1:nloc),3)     !Neuman conditions
                END IF
             END IF
          END IF
       END DO
       !-- Clipping one point
       IF (clip == 1) Laplace_diag(shift+iclip) = 1.0_pr
    END DO
  END FUNCTION Laplace_diag



  FUNCTION user_rhs (u_integrated,p)
    USE precision
    USE sizes
    USE pde
    USE share_kry
    USE vector_util_mod
    USE field
    IMPLICIT NONE
    REAL (pr), DIMENSION (ng,ne), INTENT(IN) :: u_integrated !1D flat version of integrated variables without BC 
    REAL (pr), DIMENSION (ng), INTENT(IN) :: p
    REAL (pr), DIMENSION (n) :: user_rhs

    INTEGER :: ie, shift, i
    INTEGER, PARAMETER :: meth=1
    REAL (pr), DIMENSION (ne,ng,dim) :: du, d2u
    REAL (pr), DIMENSION (ng,dim)     :: dp




    !PRINT *,'user_rhs MINMAX(p) ', MINVAL( p), MAXVAL(dp)
    dp(1:ng,1:dim) = grad (p, ng, j_lev, meth+2)

    !PRINT *,'user_rhs MINMAX(dp) ', MINVAL( dp), MAXVAL(dp)
    !PRINT *,'user_rhs MINMAX(u_integrated) ', MINVAL( u_integrated), MAXVAL(u_integrated)

    CALL c_diff_fast(u_integrated, du, d2u, j_lev, ng, 1, 11, ne, 1, ne)

    !PRINT *,'user_rhs MINMAX(du) ', MINVAL( du), MAXVAL(du)


    !--Form right hand side of Navier-Stokes equations
    IF (dim==2) THEN
       PRINT *,'Dim=2 not yet supported in this case...Exiting...'
       STOP
       DO ie = 1, 2
          shift=(ie-1)*ng
          !CALL c_diff_fast(u_integrated(shift+1:shift+ng), du, d2u, j_lev, ng, 1, 11, 1, 1, 1)
          user_rhs(shift+1:shift+ng) = - (u_integrated(:,2)+Umn(1))*du(ie,:,1) - &
               (u_integrated(:,2)+Umn(2))*du(ie,:,2) + nu*SUM(d2u(ie,:,:),2) + sgs_mdl_force(:,ie) - dp(:,ie)
       END DO
    ELSE IF (dim==3) THEN
       DO ie = 1, 3
          shift=(ie-1)*ng
          !CALL c_diff_fast(u_integrated(shift+1:shift+ng), du, d2u, j_lev, nwlt, 1, 11, 1, 1, 1)
          user_rhs(shift+1:shift+ng) = - (u_integrated(:,1)+Umn(1))*du(ie,:,1) - (u_integrated(:,2)+Umn(2))*du(ie,:,2) - &
               (u_integrated(:,3)+Umn(3))*du(ie,:,3) &
               + nu*SUM(d2u(ie,:,:),2) + sgs_mdl_force(:,ie) - dp(:,ie)

          !PRINT *,'user_rhs ie MINMAX(user_rhs(shift+1:shift+ng))', ie, &
          !  MINVAL( user_rhs(shift+1:shift+ng)), MAXVAL(user_rhs(shift+1:shift+ng))

       END DO

       IF( sgsmodel == 4 ) THEN
          IF( mdl_form == 0 ) THEN        !pure convection of Ilm and Imm

             !
             ! Ilm
             !

             shift=(n_var_Ilm-1)*ng
             ie = n_var_Ilm
             user_rhs(shift+1:shift+ng) = - u_integrated(:,1)*du(ie,:,1) - u_integrated(:,2)*du(ie,:,2) - &
                  u_integrated(:,3)*du(ie,:,3)  + nuI(:)*SUM(d2u(n_var_Ilm,:,:),2) 
             !
             ! Imm
             !
             shift=(n_var_Imm-1)*ng
             ie = n_var_Imm
             user_rhs(shift+1:shift+ng) = - u_integrated(:,1)*du(ie,:,1) - u_integrated(:,2)*du(ie,:,2) - &
                  u_integrated(:,3)*du(ie,:,3)  + nuI(:)*SUM(d2u(n_var_Imm,:,:),2) 

          ELSE IF( mdl_form == 1 ) THEN  !EXPLICIT

             !
             ! Ilm
             !

             shift=(n_var_Ilm-1)*ng
             ie = n_var_Ilm
             user_rhs(shift+1:shift+ng) = - u_integrated(:,1)*du(ie,:,1) - u_integrated(:,2)*du(ie,:,2) - &
                  u_integrated(:,3)*du(ie,:,3) + sgs_mdl_force(:,ie)  + nuI(:)*SUM(d2u(ie,:,:),2) 

             !PRINT *, 'shift ',shift
             !PRINT *,'user_rhs ie MINMAX(user_rhs(shift+1:shift+ng))', ie, &
             !  MINVAL( user_rhs(shift+1:shift+ng)), MAXVAL(user_rhs(shift+1:shift+ng))
             !PRINT *,'user_rhs ie MINMAX(sgs_mdl_force(:,ie))', ie, &
             !  MINVAL( sgs_mdl_force(:,ie)), MAXVAL(sgs_mdl_force(:,ie))
             !DO i=1,10
             ! PRINT *,'>',sgs_mdl_force(i,ie)
             ! END DO
             !
             ! Imm
             !
             shift=(n_var_Imm-1)*ng
             ie = n_var_Imm
             user_rhs(shift+1:shift+ng) = - u_integrated(:,1)*du(ie,:,1) - u_integrated(:,2)*du(ie,:,2) - &
                  u_integrated(:,3)*du(ie,:,3) + sgs_mdl_force(:,ie) + nuI(:)*SUM(d2u(ie,:,:),2) 

             !PRINT *, 'shift ',shift
             !PRINT *,'user_rhs ie MINMAX(user_rhs(shift+1:shift+ng))', ie, &
             !  MINVAL( user_rhs(shift+1:shift+ng)), MAXVAL(user_rhs(shift+1:shift+ng))
             !PRINT *,'user_rhs ie MINMAX(sgs_mdl_force(:,ie))', ie, &
             !  MINVAL( sgs_mdl_force(:,ie)), MAXVAL(sgs_mdl_force(:,ie))

          ELSE IF ( mdl_form == 2 ) THEN  !implicit with 1/T=const 

             ! these are set from smag_dyn_Meneveau
             ! sgs_mdl_force(:,n_var_Ilm) == LijMij
             ! sgs_mdl_force(:,n_var_Imm) == MijMi
             ! delta(:) = 1/(delta^(1/4) *theta) * (Ilm Imm )^1/8 ( = 1/T ) or
             ! delta(:) = 1/(delta*theta) * (Ilm Imm )^1/8 ( = 1/T )

             !
             ! Ilm
             !

             shift=(n_var_Ilm-1)*ng
             ie = n_var_Ilm
             user_rhs(shift+1:shift+ng) = - u_integrated(:,1)*du(ie,:,1) - u_integrated(:,2)*du(ie,:,2) - &
                  u_integrated(:,3)*du(ie,:,3) +  &
                  delta(:) * (sgs_mdl_force(:,ie) - u_integrated(:,ie) ) + nuI(:)*SUM(d2u(ie,:,:),2) 

             !
             ! Imm
             !

             shift=(n_var_Imm-1)*ng
             ie = n_var_Imm
             user_rhs(shift+1:shift+ng) = - u_integrated(:,1)*du(ie,:,1) - u_integrated(:,2)*du(ie,:,2) - &
                  u_integrated(:,3)*du(ie,:,3) +  &
                  delta(:) * (sgs_mdl_force(:,ie) - u_integrated(:,ie) ) + nuI(:)*SUM(d2u(ie,:,:),2) 

          ELSE IF ( mdl_form == 3 ) THEN  !implicit with 1/T variable

             ! these are set from smag_dyn_Meneveau
             ! sgs_mdl_force(:,n_var_Ilm) == LijMij
             ! sgs_mdl_force(:,n_var_Imm) == MijMij
             ! delta(:) = 1/(delta^(1/4) *theta)  or  = 1/(delta*theta)

             !
             ! Ilm
             !

             shift=(n_var_Ilm-1)*ng
             user_rhs(shift+1:shift+ng) = - u_integrated(:,1)*du(n_var_Ilm,:,1) - u_integrated(:,2)*du(n_var_Ilm,:,2) - &
                  u_integrated(:,3)*du(n_var_Ilm,:,3) +  &
                  delta(:) * MAX(0.0_pr, u_integrated(:,n_var_Ilm)*u_integrated(:,n_var_Imm) ) ** (0.125_pr) * &
                  (sgs_mdl_force(:,n_var_Ilm) - u_integrated(:,n_var_Ilm) ) + nuI(:)*SUM(d2u(n_var_Ilm,:,:),2) 

             !
             ! Imm
             !

             shift=(n_var_Imm-1)*ng
             user_rhs(shift+1:shift+ng) = - u_integrated(:,1)*du(n_var_Imm,:,1) - u_integrated(:,2)*du(n_var_Imm,:,2) - &
                  u_integrated(:,3)*du(n_var_Imm,:,3) +  &
                  delta(:) * MAX(0.0_pr,u_integrated(:,n_var_Ilm)*u_integrated(:,n_var_Imm) ) ** (0.125_pr) * &
                  (sgs_mdl_force(:,n_var_Imm) - u_integrated(:,n_var_Imm) ) + nuI(:)*SUM(d2u(n_var_Imm,:,:),2)

          END IF
       END IF
    END IF

    !--Set operator on boundaries
    IF(SUM(nbnd) /= 0) CALL user_algebraic_BC_rhs (user_rhs, ne, ng, j_lev)
  END FUNCTION user_rhs


  ! find Jacobian of Right Hand Side of the problem
  ! u_prev == u_prev_timestep_loc

  FUNCTION user_Drhs (u, u_prev, meth)
    USE share_kry
    USE precision
    USE pde
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: meth
    !u_prev_timestep passed local to cast it into 2dim array to work with vector derivatives
    REAL (pr), DIMENSION (ng,ne) :: u, u_prev
    REAL (pr), DIMENSION (n) :: user_Drhs

    INTEGER :: ie, shift
    REAL (pr), DIMENSION (2*ne,ng,dim) :: du  ! 1st derivatives for u (in du(1:ne,:,:)) and u_prev_timestep (in du(ne+1:2*ne,:,:))
    REAL (pr), DIMENSION (ne  ,ng,dim) :: d2u ! 2nd derivatives for u 
    REAL (pr), DIMENSION (ne  ,ng,dim) :: du_dummy ! passed when only calculating 1st derivative.
    !Find batter way to do this!! du_dummy with no storage..

    IF ( TYPE_DB == DB_TYPE_WRK )THEN 
       ! find 1st and 2nd deriviative of u and
       CALL c_diff_fast(u, du(1:ne,:,:), d2u(1:ne,:,:), j_lev, ng, meth, 11, ne , 1, ne )

       ! find only first deriviativ e  u_prev_timestep
       CALL c_diff_fast(u_prev, du(ne+1:2*ne,:,:), du_dummy(1:ne,:,:), j_lev, ng, meth, 10, ne , 1, ne )

    ELSE !db

       ! Load u and u_prev_timestep in to db
       ! update the db from u
       ! u(:, mn_var:mx_var) -> db%u(db_offset:mx_var-mn_var+1)
       ! db_offset = 1 
       CALL update_db_from_u(  u       , ng ,ne  , 1, ne, 1  ) !Load u
       CALL update_db_from_u(  u_prev , ng ,ne  , 1, ne, ne+1  ) !Load u_prev offset in db by ne+1

       ! find 1st and 2nd deriviative of u and
       ! find only first deriviativ e  u_prev_timestep
       CALL c_diff_fast_db(du, d2u, j_lev, ng, meth, 11, 2*ne,&
            1,   &  ! MIN(mn_varD,mn_varD2)
            2*ne,&  ! MAX(mx_varD,mx_varD2)
            1,   &  ! mn_varD  :   min variable in db that we are doing the 1st derivatives for.
            2*ne,&  ! mn_varD  :   max variable in db that we are doing the 1st derivatives for.
            1 ,  &  ! mn_varD2 :   min variable in db that we are doing the 2nd derivatives for.
            dim   )  ! mn_varD2 :   max variable in db that we are doing the 2nd derivatives for.


    END IF
    !CALL c_diff_fast_db(du_b, du_b, j_lev, ng, meth, 10, ne, 1, ne) !find 1st derivative u_b

    !--Form right hand side of Navier--Stokes equations
    IF (dim==2) THEN
       DO ie = 1, 2
          shift=(ie-1)*ng

          !CALL c_diff_fast(u(shift+1:shift+ng), du, d2u, j_lev, ng, meth, 11, 1, 1, 1)
          user_Drhs(shift+1:shift+ng) = - (u_prev(:,1)+Umn(1))*du(ie,:,1) - &
               (u_prev(:,2)+Umn(2))*du(ie,:,2) + &
               nu*SUM(d2u(ie,:,:),2)

          !CALL c_diff_fast(u_prev_timestep(shift+1:shift+ng), du, d2u, j_lev, ng, meth, 10, 1, 1, 1)
          user_Drhs(shift+1:shift+ng) = user_Drhs(shift+1:shift+ng) - u(:,1)*du(ne+ie,:,1) - u(:,2)*du(ne+ie,:,2) 
       END DO
    ELSE IF (dim==3) THEN
       DO ie = 1, 3
          shift=(ie-1)*ng

          !CALL c_diff_fast(u(shift+1:shift+ng), du, d2u, j_lev, ng, meth, 11, 1, 1, 1)
          user_Drhs(shift+1:shift+ng) = - (u_prev(:,1)+Umn(1))*du(ie,:,1) - &
               (u_prev(:,2)+Umn(2))*du(ie,:,2) &
               - (u_prev(:,3)+Umn(3))*du(ie,:,3) + nu*SUM(d2u(ie,:,:),2)

          !CALL c_diff_fast(u_prev_timestep(shift+1:shift+ng), du, d2u, j_lev, ng, meth, 10, 1, 1, 1)
          user_Drhs(shift+1:shift+ng) = user_Drhs(shift+1:shift+ng) - u(:,1)*du(ne+ie,:,1) - u(:,2)*du(ne+ie,:,2) &
               - u(:,3)*du(ne+ie,:,3)
       END DO
       
       IF( sgsmodel == 4 ) THEN
          IF( mdl_form == 0 .OR. mdl_form == 1  ) THEN

             !
             ! Ilm
             !
             shift=(n_var_Ilm-1)*ng
             ie = n_var_Ilm
             ! Drhs = 
             user_Drhs(shift+1:shift+ng) = - u_prev(:,1)*du(ie,:,1) - u_prev(:,2)*du(ie,:,2) &
                  - u_prev(:,3)*du(ie,:,3) - u(:,1)*du(ne+ie,:,1) - u(:,2)*du(ne+ie,:,2) - u(:,3)*du(ne+ie,:,3) &
                  + nuI(:)*SUM(d2u(n_var_Ilm,:,:),2)

             !
             ! Imm
             !
             shift=(n_var_Imm-1)*ng
             ie = n_var_Imm
             ! Drhs = 
             user_Drhs(shift+1:shift+ng) = - u_prev(:,1)*du(ie,:,1) - u_prev(:,2)*du(ie,:,2) &
                  - u_prev(:,3)*du(ie,:,3) - u(:,1)*du(ne+ie,:,1) - u(:,2)*du(ne+ie,:,2) - u(:,3)*du(ne+ie,:,3)&
                  + nuI(:)*SUM(d2u(n_var_Imm,:,:),2)

          ELSE IF ( mdl_form == 2 ) THEN  !implicit
             ! these are set from smag_dyn_Meneveau
             ! sgs_mdl_force(:,n_var_Ilm) == LijMij
             ! sgs_mdl_force(:,n_var_Imm) == MijMij
             ! delta(:) = 1/(delta^(1/4) *theta) * (Ilm Imm )^1/8 or
             ! delta(:) = 1/(delta*theta) * (Ilm Imm )^1/8

             !
             ! Drhs(Ilm) = - u_prev d Ilm/dx_i - u dIlm_prev/ Dx_i 
             !   + ( IlmImm)^(1/8)  /( 8*theta*delta^(1/4) )  &
             !   * ( LijMij*( Ilm/Ilm_prev + Imm/Imm_prev) - 9*Ilm - Ilm_prev*Imm/Imm_prev )         
             !
             ! where, _prev is value from prev time step and all non_prev are perterbations
             ! du(ne+ie,:,1) is du_prev(ie)/dx_1

             shift=(n_var_Ilm-1)*ng

             ! Drhs = 
             user_Drhs(shift+1:shift+ng) = - u_prev(:,1)*du(n_var_Ilm,:,1) - u_prev(:,2)*du(n_var_Ilm,:,2) &
                  - u_prev(:,3)*du(n_var_Ilm,:,3) &
                  - u(:,1)*du(ne+n_var_Ilm,:,1) - u(:,2)*du(ne+n_var_Ilm,:,2) &
                  - u(:,3)*du(ne+n_var_Ilm,:,3) - delta(:) * u(:,n_var_Ilm) &
                  + nuI(:)*SUM(d2u(n_var_Ilm,:,:),2)

             !
             ! Drhs(Imm) = - u_prev d Imm/dx_i - u dImm_prev/ Dx_i 
             !   + Imm*( Imm_prev Imm_prev)^(1/8)  /( 8*theta*delta^(1/4) )  &
             !   * ( 2.0*MijMij/Imm_prev - 10 )         
             !
             ! where, _prev is value from prev time step and all non_prev are perterbations
             ! du(ne+ie,:,1) is du_prev(ie)/dx_1

             shift=(n_var_Imm-1)*ng

             ! Drhs = 
             user_Drhs(shift+1:shift+ng) = - u_prev(:,1)*du(n_var_Imm,:,1) - u_prev(:,2)*du(n_var_Imm,:,2) &
                  - u_prev(:,3)*du(n_var_Imm,:,3) &
                  - u(:,1)*du(ne+n_var_Imm,:,1) - u(:,2)*du(ne+n_var_Imm,:,2) &
                  - u(:,3)*du(ne+n_var_Imm,:,3) - delta(:) * u(:,n_var_Imm) &
                  + nuI(:)*SUM(d2u(n_var_Imm,:,:),2)

          ELSE IF ( mdl_form == 3 ) THEN  !implicit

             ! these are set from smag_dyn_Meneveau
             ! sgs_mdl_force(:,n_var_Ilm) == LijMij
             ! sgs_mdl_force(:,n_var_Imm) == MijMij
             ! delta(:) = 1/(delta^(1/4) *theta) or 1/(delta*theta)  

             !
             ! Drhs(Ilm) = - u_prev d Ilm/dx_i - u dIlm_prev/ Dx_i 
             !   + ( IlmImm)^(1/8)  /( 8*theta*delta^(1/4) )  &
             !   * ( LijMij*( Ilm/Ilm_prev + Imm/Imm_prev) - 9*Ilm - Ilm_prev*Imm/Imm_prev )         
             !
             ! where, _prev is value from prev time step and all non_prev are perterbations
             ! du(ne+ie,:,1) is du_prev(ie)/dx_1

             shift=(n_var_Ilm-1)*ng

             ! Drhs = 
             user_Drhs(shift+1:shift+ng) = - u_prev(:,1)*du(n_var_Ilm,:,1) - u_prev(:,2)*du(n_var_Ilm,:,2) &
                  - u_prev(:,3)*du(n_var_Ilm,:,3) &
                  - u(:,1)*du(ne+n_var_Ilm,:,1) - u(:,2)*du(ne+n_var_Ilm,:,2) &
                  - u(:,3)*du(ne+n_var_Ilm,:,3) &
                  + delta(:)/8.0_pr*MAX(0.0_pr,u_prev(:,n_var_Ilm)*u_prev(:,n_var_Imm))**(0.125_pr) &
                  * (  sgs_mdl_force(:,n_var_Ilm)*( u(:,n_var_Ilm)/u_prev(:,n_var_Ilm) + u(:,n_var_Imm)/u_prev(:,n_var_Imm) ) &
                  - 9.0_pr * u(:,n_var_Ilm) - u_prev(:,n_var_Ilm) * u(:,n_var_Imm)/u_prev(:,n_var_Imm) ) &
                  + nuI(:)*SUM(d2u(n_var_Ilm,:,:),2)

             !
             ! Drhs(Imm) = - u_prev d Imm/dx_i - u dImm_prev/ Dx_i 
             !   + Imm*( Imm_prev Imm_prev)^(1/8)  /( 8*theta*delta^(1/4) )  &
             !   * ( 2.0*MijMij/Imm_prev - 10 )         
             !
             ! where, _prev is value from prev time step and all non_prev are perterbations
             ! du(ne+ie,:,1) is du_prev(ie)/dx_1

             shift=(n_var_Imm-1)*ng

             ! Drhs = 
             user_Drhs(shift+1:shift+ng) = - u_prev(:,1)*du(n_var_Imm,:,1) - u_prev(:,2)*du(n_var_Imm,:,2) &
                  - u_prev(:,3)*du(n_var_Imm,:,3) &
                  - u(:,1)*du(ne+n_var_Imm,:,1) - u(:,2)*du(ne+n_var_Imm,:,2) &
                  - u(:,3)*du(ne+n_var_Imm,:,3) &
                  + delta(:)/8.0_pr*MAX(0.0_pr, u_prev(:,n_var_Ilm)*u_prev(:,n_var_Imm))**(0.125_pr)  &
                  * (  sgs_mdl_force(:,n_var_Imm)*( u(:,n_var_Ilm)/u_prev(:,n_var_Ilm) + u(:,n_var_Imm)/u_prev(:,n_var_Imm) ) &
                  - 9.0_pr * u(:,n_var_Imm) - u_prev(:,n_var_Imm) * u(:,n_var_Ilm)/u_prev(:,n_var_Ilm) ) &
                  + nuI(:)*SUM(d2u(n_var_Imm,:,:),2)

          END IF
       END IF
    END IF
  END FUNCTION user_Drhs


  !
  ! Uses u_prev_timestep, which is global flat (1D ) array set in time_step_cn()
  !
  FUNCTION user_Drhs_diag (meth)
    USE share_kry
    USE precision
    USE pde
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: meth
    REAL (pr), DIMENSION (n) :: user_Drhs_diag

    INTEGER :: ie, shift,shiftIlm,shiftImm
    REAL (pr), DIMENSION (ng,dim) :: du, d2u
    REAL (pr), DIMENSION (ne,ng,dim) :: du_prev_timestep
    REAL (pr), DIMENSION (ne  ,ng,dim) :: du_dummy ! passed when only calculating 1st derivative.

    CALL c_diff_fast(u_prev_timestep, du_prev_timestep, du_dummy, j_lev, ng, meth, 10, ne, 1, ne)

    !
    ! does not rely on u so we can call it once here
    !
    shift = 0 !tmp
    CALL c_diff_diag(du, d2u, j_lev, ng, meth, meth, 11)

    !--Form right hand side of Navier--Stokes equations
    IF (dim==2) THEN
       DO ie = 1, 2
          shift=(ie-1)*ng

          !CALL c_diff_diag(u(shift+1:shift+ng), du, d2u, j_lev, ng, meth, meth, 11)
          user_Drhs_diag(shift+1:shift+ng) = - (u_prev_timestep(1:ng)+Umn(1))*du(:,1) -&
               (u_prev_timestep(ng+1:2*ng)+Umn(2))*du(:,2) + nu*SUM(d2u,2)

          !CALL c_diff_fast(u_prev_timestep(shift+1:shift+ng), du, d2u, j_lev, ng, meth, 10, 1, 1, 1)
          user_Drhs_diag(shift+1:shift+ng) = user_Drhs_diag(shift+1:shift+ng) - du_prev_timestep(ie,:,ie)
       END DO
    ELSE IF (dim==3) THEN
       DO ie = 1, 3
          shift=(ie-1)*ng
          !PRINT *,'CAlling c_diff_diag from user_Drhs_diag()'
          !CALL c_diff_diag(u(shift+1:shift+ng), du, d2u, j_lev, ng, meth, meth, 11)
          user_Drhs_diag(shift+1:shift+ng) = - (u_prev_timestep(1:ng)+Umn(1))*du(:,1) -&
               (u_prev_timestep(ng+1:2*ng)+Umn(2))*du(:,2) - &
               (u_prev_timestep(2*ng+1:3*ng)+Umn(3))*du(:,3) + nu*SUM(d2u,2)

          !CALL c_diff_fast(u_prev_timestep(shift+1:shift+ng), du, d2u, j_lev, ng, meth, 10, 1, 1, 1)
          user_Drhs_diag(shift+1:shift+ng) = user_Drhs_diag(shift+1:shift+ng) - du_prev_timestep(ie,:,ie)

       END DO
       IF( sgsmodel == 4 ) THEN
          IF( mdl_form == 0 .OR. mdl_form == 1 ) THEN

             !
             ! Ilm
             !
             shift=(n_var_Ilm-1)*ng
             ie = n_var_Ilm
             ! Drhs = 
             user_Drhs_diag(shift+1:shift+ng) = - u_prev_timestep(1:ng)*du(:,1) - u_prev_timestep(ng+1:2*ng)*du(:,2) &
		  - u_prev_timestep(2*ng+1:3*ng)*du(:,3) + nuI(:)*SUM(d2u,2)


             !
             ! Imm
             !
             shift=(n_var_Imm-1)*ng
             ie = n_var_Imm
             ! Drhs = 
             user_Drhs_diag(shift+1:shift+ng) = - u_prev_timestep(1:ng)*du(:,1) - u_prev_timestep(ng+1:2*ng)*du(:,2) &
		  - u_prev_timestep(2*ng+1:3*ng)*du(:,3) + nuI(:)*SUM(d2u,2)


          ELSE IF ( mdl_form == 2 ) THEN  !explicit
             ! these are set from smag_dyn_Meneveau
             ! sgs_mdl_force(:,n_var_Ilm) == LijMij
             ! sgs_mdl_force(:,n_var_Imm) == MijMij
             ! delta(:) = 1/(delta^(1/4) *theta) * (Ilm Imm )^1/8

             shiftIlm=(n_var_Ilm-1)*ng
             shiftImm=(n_var_Imm-1)*ng

             !
             ! Ilm
             !

             user_Drhs_diag(shiftIlm+1:shiftIlm+ng) = - u_prev_timestep(1:ng)*du(:,1) - u_prev_timestep(ng+1:2*ng)*du(:,2) &
		  - u_prev_timestep(2*ng+1:3*ng)*du(:,3) - delta(:)	+ nuI(:)*SUM(d2u,2)

             !
             ! Ilm
             !

             user_Drhs_diag(shiftImm+1:shiftImm+ng) = - u_prev_timestep(1:ng)*du(:,1) - u_prev_timestep(ng+1:2*ng)*du(:,2) &
		  - u_prev_timestep(2*ng+1:3*ng)*du(:,3) - delta(:) + nuI(:)*SUM(d2u,2)


          ELSE IF ( mdl_form == 3 ) THEN  !explicit
             ! these are set from smag_dyn_Meneveau
             ! sgs_mdl_force(:,n_var_Ilm) == LijMij
             ! sgs_mdl_force(:,n_var_Imm) == MijMij
             ! delta(:) = 1/(delta^(1/4) *theta) 

             shiftIlm=(n_var_Ilm-1)*ng
             shiftImm=(n_var_Imm-1)*ng

             !
             ! Ilm
             !
             ! -u_i*d_diag + 1/(8*theta*delta^1/4) (IlmImm)^1/8(LijMij/Ilm - 9 )
             !

             user_Drhs_diag(shiftIlm+1:shiftIlm+ng) = - u_prev_timestep(1:ng)*du(:,1) - u_prev_timestep(ng+1:2*ng)*du(:,2) &
                  - u_prev_timestep(2*ng+1:3*ng)*du(:,3) &
                  + delta(:)/8.0_pr &
                  *MAX(0.0_pr, u_prev_timestep(shiftIlm+1:shiftIlm+ng)*u_prev_timestep(shiftImm+1:shiftImm+ng))**(0.125_pr)  &
                  * (  sgs_mdl_force(:,n_var_Ilm)/u_prev_timestep(shiftIlm+1:shiftIlm+ng)  - 9.0_pr ) &
                  + nuI(:)*SUM(d2u,2)


             !
             ! Ilm
             !
             ! -u_i*d_diag + 1/(8*theta*delta^1/4) (IlmImm)^1/8( 2 MijMij/Imm - 10 )
             !

             user_Drhs_diag(shiftImm+1:shiftImm+ng) = - u_prev_timestep(1:ng)*du(:,1) - u_prev_timestep(ng+1:2*ng)*du(:,2) &
		  - u_prev_timestep(2*ng+1:3*ng)*du(:,3) &
		  + delta(:)/8.0_pr &
		  *MAX(0.0_pr, u_prev_timestep(shiftIlm+1:shiftIlm+ng)*u_prev_timestep(shiftImm+1:shiftImm+ng))**(0.125_pr)  &
		  * (  sgs_mdl_force(:,n_var_Imm)/u_prev_timestep(shiftImm+1:shiftImm+ng)  - 9.0_pr ) &
   		  + nuI(:)*SUM(d2u,2)

          END IF
       END IF
    END IF
  END FUNCTION user_Drhs_diag

  SUBROUTINE user_project (u, p, nlocal, meth)
    !--Makes u divergence free
    USE precision
    USE pde
    USE vector_util_mod
    USE elliptic_mod
    IMPLICIT NONE

    INTEGER, INTENT (IN) :: meth, nlocal
    REAL (pr), DIMENSION (nlocal),     INTENT(INOUT) :: p
    REAL (pr), DIMENSION (nlocal,n_integrated), INTENT(INOUT) :: u

    REAL (pr), DIMENSION (nlocal) :: dp
    REAL (pr), DIMENSION (nlocal) :: f

    !--Make u  divergence free
    dp = 0.0_pr
    f = div(u(:,1:dim)  ,nlocal,j_lev,meth+4)
    CALL Linsolve (dp, f , tol2, nlocal, 1, Laplace, Laplace_diag)  !
    u(:,1:dim) = u(:,1:dim) - grad(dp, nlocal, j_lev, meth+2)
    p = p + dp/dt
  END SUBROUTINE user_project

  FUNCTION user_chi (nlocal, t_local )
    USE precision
    USE pde
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: nlocal
    REAL (pr), INTENT (IN) :: t_local
    REAL (pr), DIMENSION (nlocal) :: user_chi

    user_chi = 0.0_pr
  END FUNCTION user_chi



  !
  ! Calculate any statitics
  !
  ! startup_flag - 0 when adapting to IC,then 1 inmain integration loop
  !
  !
  SUBROUTINE user_stats ( u ,j_mn, startup_flag)
    USE precision
    USE io_3d_vars
    USE pde
    USE sizes
    USE util_vars
    USE fft_module
    USE spectra_module
    USE parallel
    IMPLICIT NONE
    REAL (pr), DIMENSION (nwlt,1:n_var), INTENT (IN) :: u
    INTEGER , INTENT (IN) :: j_mn 
    INTEGER , INTENT (IN) :: startup_flag
    CHARACTER (LEN=256)  :: filename
    ! CHARACTER (LEN=256)             :: file_out
    INTEGER outputfileUNIT
    REAL (pr) :: area !area of domain to normalize dA
    REAL (pr) :: ttke
    INTEGER count_u_g_mdl_gridfilt_mask, count_u_g_mdl_testfilt_mask

    filename = 'results/'//TRIM(file_gen)//'case_isoturb.log'
    PRINT *,' user_stats, logging to :', filename

    IF( ALLOCATED(dA) ) THEN
       area =  (xyzlimits(2,1)- xyzlimits(1,1)) * &
            (xyzlimits(2,2)- xyzlimits(1,2)) * &
            (xyzlimits(2,3)- xyzlimits(1,3)) 

       ttke = 0.5_pr * SUM( (dA(:)/area)*(u(:,1)**2 + u(:,2)**2 + u(:,3)**2 ))
       CALL parallel_global_sum( REAL=ttke )

       IF( ALLOCATED(u_g_mdl_gridfilt_mask ) ) THEN
          count_u_g_mdl_gridfilt_mask = INT( SUM(u_g_mdl_gridfilt_mask) )
       ELSE
          count_u_g_mdl_gridfilt_mask = -1
       ENDIF

       IF( ALLOCATED(u_g_mdl_testfilt_mask ) ) THEN
          count_u_g_mdl_testfilt_mask = INT(  SUM(u_g_mdl_testfilt_mask) )
       ELSE
          count_u_g_mdl_testfilt_mask = -1
       ENDIF



       ! Find the TKE if the dA weights have been allocated (and it is
       ! assumed that if they are allocated they have been calculated)
       PRINT *,' USER_STATS'
       PRINT *,'***********************************************'
       PRINT *,' (Approximate) TKE =', ttke
       PRINT *,' Smagorinski Mdl. Coeff. Cs = ', Cs
       PRINT *,' total_resolved_diss = ',total_resolved_diss
       PRINT *,' total_sgs_diss = ',total_sgs_diss

       PRINT *,' # points on grid filt mask = ', count_u_g_mdl_gridfilt_mask
       PRINT *,' # points on test filt mask = ', count_u_g_mdl_testfilt_mask
       PRINT *,'***********************************************'
       PRINT *,''

       outputfileUNIT = 157
       OPEN (UNIT = outputfileUNIT, FILE =filename, STATUS='unknown',&
            POSITION='APPEND')

       IF( startup_flag == 0 ) THEN
          write(UNIT=outputfileUNIT,ADVANCE='NO', &
            FMT='( "% Time tke Cs total_resolved_diss total_sgs_diss ")')
          write(UNIT=outputfileUNIT,ADVANCE='YES', &
            FMT='( "count_u_g_mdl_gridfilt_mask count_u_g_mdl_testfilt_mask ")')
       END IF

       write(UNIT=outputfileUNIT,ADVANCE='YES' , &
            FMT='(  5(e15.7 , '' '') , 2(i15 , '' ''))' ) &
            t, ttke, Cs  , total_resolved_diss , total_sgs_diss ,&
            count_u_g_mdl_gridfilt_mask, count_u_g_mdl_testfilt_mask
       CLOSE(UNIT = outputfileUNIT)
    END IF
!!$ ttke = 1.0_pr
!!$  Cs  = 1.0_pr
!!$     write(*, &
!!$          FMT='( ''% Time tke Cs '')' )
!!$     write(*, &
!!$          FMT='(  3(e15.7 , '' '') )' ) &
!!$          t, ttke, Cs  
!!$stop
  END SUBROUTINE user_stats

!!$!
!!$! Calculate any statitics
!!$!
!!$SUBROUTINE user_stats_XXX ( u , filename ,j_mn)
!!$  USE precision
!!$  USE pde
!!$  USE sizes
!!$  USE fft_module
!!$  USE spectra_module
!!$  IMPLICIT NONE
!!$  REAL (pr), DIMENSION (nwlt,1:dim), INTENT (IN) :: u
!!$  CHARACTER (LEN=*) , INTENT (IN) :: filename
!!$!  CHARACTER (LEN=256)             :: file_out
!!$  INTEGER :: j_mn 
!!$!  INTEGER :: it_local !iteration
!!$
!!$  LOGICAL , SAVE :: start = .TRUE.
!!$  REAL (pr) ::  pi2
!!$  REAL (pr) :: field(nxyz(1)+2,nxyz(2)+1,nxyz(3)+1,3)
!!$  REAL (pr) :: the_tke, the_int_length,total_diss
!!$  INTEGER   :: i, io_status
!!$  !CHARACTER (LEN=LEN(filename))  :: filename2
!!$
!!$  !* Initialize fft's on the first call
!!$  pi2 = 8.0D0 *atan(1.0D0 )
!!$
!!$  IF( start ) THEN
!!$     print *,' In user_stats  initializing fft''s '
!!$     CALL init_fft(nxyz(1),nxyz(2),nxyz(3),pi2,pi2,pi2, .true. )
!!$  END IF
!!$
!!$  !CALL test_fft()
!!$  !stop
!!$
!!$  ! Save the  flat u array  into the field 
!!$  field = 0.0_pr
!!$  DO i=1,nwlt
!!$     field(indx(i,1)+1,indx(i,2)+1,indx(i,3)+1,1) = u(i,1)
!!$  END DO
!!$  DO i=1,nwlt
!!$     field(indx(i,1)+1,indx(i,2)+1,indx(i,3)+1,2) = u(i,2)
!!$  END DO
!!$  DO i=1,nwlt
!!$     field(indx(i,1)+1,indx(i,2)+1,indx(i,3)+1,3) = u(i,3)
!!$  END DO
!!$
!!$  CALL rtoft(field(:,:,:,1))
!!$  CALL rtoft(field(:,:,:,2))
!!$  CALL rtoft(field(:,:,:,3))
!!$
!!$  CALL print_spectra2(field(:,:,:,1),field(:,:,:,2),field(:,:,:,3),&
!!$       TRIM(filename)//"it"//it_string//'.spectra',nxyz(1),nxyz(2),nxyz(3),the_tke,total_diss )
!!$
!!$!  CALL tke( field(:,:,:,1),field(:,:,:,2),field(:,:,:,3), the_tke, the_int_length )
!!$  PRINT *,' '
!!$  PRINT *,'******************* Turbulent statistics ******************* '
!!$  PRINT *,'* TKE                = ', the_tke
!!$  PRINT *,'* TKE real space     = ', SUM( Da*(u(:,1)**2 +u(:,2)**2 + u(:,3)**2) )
!!$
!!$  PRINT *,'* Total Dissipation  = ', total_diss
!!$  PRINT *,'******************* End Turbulent statistics *************** '
!!$  PRINT *,' '
!!$
!!$  ! make sure that any log files exist, and if not create them.
!!$  !filename2 = TRIM(filename)//'.turb.stats'
!!$  CALL open_file(TRIM(filename)//'.turb.stats', it, 13, .TRUE., .TRUE.)
!!$
!!$  IF( start ) WRITE(13,'("% time iteration tke total_dissipation ")')
!!$
!!$  WRITE(13,'(E12.5,1x,I10,1x,3(E12.5,1x) )') t,it,the_tke,total_diss
!!$  CLOSE (13)
!!$     
!!$  start = .FALSE.
!!$
!!$END SUBROUTINE user_stats_XXX


  SUBROUTINE user_cal_force (u, n, t_local, force, drag, lift)
    !--Calculates drag and lift on obstacle using penalization formula
    USE precision
    USE pde
    USE share_consts
    IMPLICIT NONE

    INTEGER, INTENT (IN) :: n
    REAL (pr), INTENT (IN) :: t_local
    REAL (pr), DIMENSION (dim), INTENT (INOUT) :: force
    REAL (pr), INTENT (OUT) :: drag, lift
    REAL (pr), DIMENSION (n,dim) :: u
    drag = 0.0_PR
    lift = 0.0_PR

    !
    ! There is no obstacle in flow
    !

  END SUBROUTINE user_cal_force




  !
  ! calculate any additional variables
  ! 
  ! arg 
  ! flag  - 0 calledwhile adapting to IC, 1 - called in main time integration loop
  !
  ! These additiona variables are calculated and left in real space.
  !
  SUBROUTINE user_additional_vars( t_local, flag )
    USE PRECISION
    USE share_consts
    USE pde
    USE sizes
    USE share_kry
    USE field
    USE wlt_trns_mod
    USE vector_util_mod
    IMPLICIT NONE
    REAL (pr), INTENT (IN) ::  t_local
    INTEGER , INTENT(IN) :: flag ! 0- called during adaption to IC, 1 called during main integration loop
    REAL(pr) tmp(nwlt,6) !tmp for vorticity

    
    IF( flag == 1 ) THEN ! only in main time int loop, not initial adaptation
       ! Calculate the magnitude vorticity
	   IF( adaptMagVort ) THEN 
      
          CALL cal_vort (u(:,n0:n0+dim-1), tmp(:,1:3), nwlt)
          u(:,n_var_modvort) = SQRT(tmp(:,1)**2.0_pr + tmp(:,2)**2.0_pr + tmp(:,3)**2.0_pr )
       END IF

       ! Calculate the magnitude of Sij (sqrt(SijSij) )
       ! s(:,1) = s_11
       ! s(:,2) = s_22
       ! s(:,3) = s_33
       ! s(:,4) = s_12
       ! s(:,5) = s_13
       ! s(:,6) = s_23
	   IF( adaptNormS ) THEN
          CALL Sij( u(:,1:dim) ,nwlt, tmp(:,1:6), .TRUE. )
       
          u(:,n_var_modSij) = SQRT( ( tmp(:,1)**2.0_pr + tmp(:,2)**2.0_pr + tmp(:,3)**2.0_pr)  + &
              2.0_pr*(tmp(:,6)**2.0_pr + tmp(:,5)**2.0_pr + tmp(:,6)**2.0_pr) )
       END IF
    END IF

  END SUBROUTINE user_additional_vars


  !
  ! calculate any additional scalar variables
  ! 
  ! arg 
  ! flag  - 0 calledwhile adapting to IC, 1 - called in main time integration loop
  !
  SUBROUTINE user_scalar_vars( flag )
    USE PRECISION
    USE share_consts
    USE pde
    USE sizes
    USE share_kry
    IMPLICIT NONE
    INTEGER , INTENT(IN) :: flag ! 0- called during adaption to IC, 1 called during main integration loop



  END SUBROUTINE user_scalar_vars

  !
  !************ Calculating Scales ***************************
  !
  ! Note the order of the components in the scl array
  ! correspond to u_tn, v_tn, w_tn, u_tn-1, v_tn-1, w_tn-1, u_tn-2, v_tn-2, w_tn-2
  ! 
  SUBROUTINE user_scales(use_default, u_loc, nlocal, ne_local, l_n_var_adapt , l_n_var_adapt_index, &
       scl, scl_fltwt) !add
    USE precision
    USE pde
    USE util_vars
    USE parallel
    IMPLICIT NONE
    LOGICAL , INTENT(INOUT) :: use_default
    INTEGER, INTENT (IN) :: nlocal, ne_local
    REAL (pr), DIMENSION (1:nlocal,1:ne_local), INTENT (IN) :: u_loc
    LOGICAL , INTENT (IN) :: l_n_var_adapt(ne_local)
    INTEGER , INTENT (IN) :: l_n_var_adapt_index(1:ne_local)
    REAL (pr), DIMENSION (1:ne_local), INTENT (INOUT) :: scl
    REAL (pr) ,  INTENT(IN) ::scl_fltwt !weight for temporal filter on scl
    REAL (pr) :: floor, tmp, K_sgs, K_resolved
    INTEGER :: ie, ie_index, itmp
    REAL (pr) ,save, ALLOCATABLE, DIMENSION (:) :: scl_old
    REAL (pr), save :: eps_old
    LOGICAL  ,save :: startup_init = .TRUE.

    !
    ! Ignore the output of this routine and use default scales routine
    !
    use_default = .FALSE.

    !
    ! NOTE: For a parallel run, synchronize scl(:) across the processors in user_scales.
    !       Use the subroutine scales of default_util.f90 as an example,
    !       subroutine parallel_global_sum of parallel.f90 may be helpful.
    !       Already synchronized: sumdA_global
    
    !
    ! ALLOCATE scl_old if it was not done previously in user_scales
    !
    IF( startup_init .AND. .NOT. ALLOCATED(scl_old) ) THEN
        ALLOCATE( scl_old(1:ne_local) )
        scl_old = 0.0_pr
        eps_old = eps
    END IF

    floor = 1.e-12_pr
    scl   =1.0_pr
    !
    ! Calculate scl per component
    !
    DO ie=1,ne_local
       IF(l_n_var_adapt(ie)) THEN
          ie_index = l_n_var_adapt_index(ie)
          IF( Scale_Meth == 1 ) THEN ! Use Linf scale
             scl(ie) = MAXVAL ( ABS( u_loc(1:nlocal,ie_index) ) )
             CALL parallel_global_sum( REALMAXVAL=scl(ie) )
             
          ELSE IF ( Scale_Meth == 2 ) THEN ! Use L2 scale
             scl(ie) = SUM( (u_loc(1:nlocal,ie_index)**2) )
             itmp = nlocal
             CALL parallel_global_sum( REAL=scl(ie) )
             CALL parallel_global_sum( INTEGER=itmp )
             scl(ie) = SQRT ( scl(ie)/itmp )
             
          ELSE IF ( Scale_Meth == 3) THEN ! Use L2 scale using dA
             scl(ie) = SUM( (u_loc(1:nlocal,ie_index)**2)*dA )
             CALL parallel_global_sum( REAL=scl(ie) )
             scl(ie) = SQRT ( scl(ie)/sumdA_global )
             
          ELSE IF ( Scale_Meth == 4) THEN ! Use L2 scale using dA
             scl(ie) = SUM( (u_loc(1:nlocal,ie_index)**2)*dA )
             CALL parallel_global_sum( REAL=scl(ie) )
             scl(ie) = SQRT ( scl(ie)/sumdA_global )
             
             IF(ie == 4) THEN ! kinetic energy
                scl(ie) = SUM( u_loc(1:nlocal,ie_index)*dA )
                CALL parallel_global_sum( REAL=scl(ie) )
                scl(ie) = scl(ie)/sumdA_global
                
             END IF
          ELSE
             IF (par_rank.EQ.0) THEN
                PRINT *, 'Error in Scales(), Unknown Scale_Meth type: ', Scale_Meth
                PRINT *, 'Known Scale_Meth types: 1 = Linf scale, 2 = L2 scale '
                PRINT *, 'Exiting ...'
             END IF
             CALL parallel_finalize; STOP
          END IF
       END IF
    END DO

    !
    ! take appropriate vector norm over scl(1:dim)
    !
    IF( Scale_Meth == 1 ) THEN ! Use Linf scale
     tmp = MAXVAL(scl(1:dim))
     scl(1:dim) = tmp
    ELSE IF ( Scale_Meth == 2 ) THEN ! Use L2 scale
     tmp = SQRT(SUM( scl(1:dim)**2 )/REAL(dim,pr) )
     scl(1:dim) = tmp
    ELSE IF ( Scale_Meth == 3 ) THEN ! Use L2 scale using dA
     tmp = SQRT(SUM( scl(1:dim)**2 )/REAL(dim,pr) )
     scl(1:dim) = tmp
    ELSE IF ( Scale_Meth == 4 ) THEN ! Use L2 scale using dA
     K_sgs = scl(4)
     K_resolved = 0.5_pr * SUM( scl(1:dim)**2 ) 
     scl(1:dim) = SQRT(2.0_pr*(K_resolved+K_sgs)/REAL(dim,pr) )
    END IF

    !
    ! Print out new scl
    !
    DO ie=1,ne_local
       IF(l_n_var_adapt(ie)) THEN
          IF(scl(ie) .le. floor) scl(ie)=1.0_pr !Shouldn't this just set it to floor????
          tmp = scl(ie)
          ! temporally filter scl
          IF(.not. startup_init ) scl(ie) = scl_fltwt * scl_old(ie) + (1.0_pr - scl_fltwt) * scl(ie)
          scl = scaleCoeff * scl
          IF (par_rank.EQ.0) THEN
             WRITE (6,'("Scaling on vector(1:dim) magnitude")')
             WRITE (6,'("Scale factor eqn", I4 ," = ", es11.4 ," scalecoeff = ",es11.4 )') &
                  ie, scl(ie), scaleCoeff(ie)
             WRITE (6,'("(before filtering = ",es11.4,")" )') tmp
          END IF
       END IF
    END DO

    scl_old = scl !save scl for this time step

    ! adjsuting epsilon 
    IF( .not. startup_init .and.  Scale_Meth == 4) THEN
       IF(K_sgs > floor ) THEN
          ! Adjust eps,
          ! if Ksgs/(K_resolved + K_sgs) > alpha_k then
          !    The field is under-resolved because there is a higher % of 
          !    SGS KE then we want. So we decrease eps to get more points
          !     in the next time step.
          !  else if Ksgs/(K_resolved + K_sgs) < alpha_k then
          !    The field is over-resolved because there is a lower % of 
          !    SGS KE then we want. So we increase eps to get less points
          !     in the next time step. D.G.

          eps = eps * SQRT( alpha_k * (K_resolved + K_sgs) / K_sgs )
          
       ELSE
          ! So if Ksgs ==0 then the logic says the field is over resolved,
          ! we are doing DNS but we want to be doing SCALES. So we want to 
          ! increase eps to resolve less points and thus increase the Ksgs,!
          ! but since from our formulas Ksgs==0 => eps == infty we will limit
          ! eps to eps_max. D.G.
          eps = eps_max
       END IF

       !
       ! Apply temporal filter to limit the rate of change of eps over time
       !
       eps = (1.0_pr - q_eps) * eps_old + q_eps * eps
       eps_old = eps
    END IF

    !
    !  When we are all done we want to make sure that eps falls within the
    !  absolute limits set by the user: [eps_min, eps_max]
    !
    eps =  MAX( MIN(eps,eps_max), eps_min )
    startup_init = .FALSE.
    !PRINT *,'TEST scl_old ', scl_old

  END SUBROUTINE user_scales 



SUBROUTINE user_cal_cfl (use_default, u, cfl_out)
  USE precision
  USE sizes
  USE pde
  USE parallel
  IMPLICIT NONE
  LOGICAL , INTENT(INOUT) :: use_default
  REAL (pr),                                INTENT (INOUT) :: cfl_out
  REAL (pr), DIMENSION (nwlt,n_integrated), INTENT (IN)    :: u

  INTEGER                    :: i
  REAL (pr)                  :: floor
  REAL (pr), DIMENSION (dim) :: cfl
  REAL (pr), DIMENSION(dim,nwlt) :: h_arr

  use_default = .FALSE.

  floor = 1e-12_pr
  cfl_out = floor
  
  CALL get_all_local_h (h_arr)
  
  DO i = 1, nwlt
     cfl(1:dim) = ABS (u(i,1:dim)+Umn(1:dim)) * dt/h_arr(1:dim,i)
     cfl_out = MAX (cfl_out, MAXVAL(cfl))
  END DO
  CALL parallel_global_sum( REALMAXVAL=cfl_out )

END SUBROUTINE user_cal_cfl

  !******************************************************************************************
  !************************************* SGS MODEL ROUTINES *********************************
  !******************************************************************************************


  !
  ! Intialize sgs model
  ! This routine is called once in the first
  ! iteration of the main time integration loop.
  ! weights and model filters have been setup for first loop when this routine is called.
  !
  SUBROUTINE user_init_sgs_model( )
    USE precision
    USE pde
    USE vector_util_mod
    USE util_mod
    IMPLICIT NONE


    ! LDM: Giuliano

    ! THE sgs forcign is stored for the time step in sgs_mdl_force(1:nlocal,1:n_integrated)
    ! where nlocal should be nwlt.


    !          print *,'initializing LDM ...'       
    !          CALL sgs_mdl_force( u(:,1:n_integrated), nwlt, j_lev, .TRUE.)


  END SUBROUTINE user_init_sgs_model

  !
  ! calculate sgs model forcing term
  ! user_sgs_force is called int he beginning of each times step in time_adv_cn().
  ! THE sgs forcign is stored for the time step in sgs_mdl_force(1:nlocal,1:n_integrated)
  ! where nlocal should be nwlt.
  ! 
  ! Accesses u from field module, 
  !          j_lev from wlt_vars module,
  !
  SUBROUTINE  user_sgs_force ( u, nlocal)
    USE precision
	USE util_vars
    USE pde
    USE vector_util_mod
    USE util_mod
    !USE field
    USE parallel
    IMPLICIT NONE

    INTEGER,                         INTENT (IN) :: nlocal
    REAL (pr), DIMENSION (nlocal,n_integrated), INTENT (INOUT) :: u


    INTEGER                                      :: idim,i, deriv_meth_sgs
    INTEGER, SAVE :: nlocal_last = 0

    !  REAL (pr), DIMENSION (nlocal,3-MOD(dim,3))   :: w ! vorticity, if dim=2 then w(1), dim==3 then w(1:3)
    REAL (pr), DIMENSION (1,nlocal,dim)            :: du, d2u

    ! variables used only for conservative form
    REAL (pr), DIMENSION (:,:) ,ALLOCATABLE :: s_ij 
    REAL (pr), DIMENSION (:)   ,ALLOCATABLE :: nu_t !turbulent eddy viscosity 
    REAL (pr), DIMENSION (:)   ,ALLOCATABLE :: twoSijSij

    LOGICAL,   SAVE :: init_flag = .TRUE. !true if we are initializing the model

    IF(  IC_restart ) init_flag = .FALSE.
    PRINT * ,'entering, user_sgs_force u(1:5,4:5) ', u(1:5,4:5)		        

    deriv_meth_sgs = 1  ! define deriviative method to use (see c_diff_fast )


    !This is commented out to restart from non LDM case...  IF( IC_restart )  init_flag = .FALSE. ! use save Ilm Imm if we restarted..

    IF( nlocal /= nlocal_last) THEN
       IF( ALLOCATED(sgs_mdl_force) ) DEALLOCATE(sgs_mdl_force)
       ALLOCATE(sgs_mdl_force(1:nlocal, n_integrated))
       PRINT *,' Reallocating sgs_mdl_force(1:nlocal, n_integrated) ', nlocal,n_integrated
    END IF

    !
    ! 
    ! 
    IF( sgsmodel > 0  ) THEN 
       PRINT *,'SGS model = ', sgsmodel



       !--Conservative form 
       ALLOCATE(s_ij(1:nlocal,6))
       ALLOCATE(nu_t(1:nlocal))
       ALLOCATE(twoSijSij(1:nlocal))



       CALL Sij( u(1:nlocal,1:dim) ,nlocal, s_ij(:,1:6) )

       DO i=1,nlocal
          twoSijSij(i) =  2.0_pr *(  s_ij(i,1)*s_ij(i,1) + s_ij(i,2)*s_ij(i,2) + s_ij(i,3)*s_ij(i,3) ) + &
               4.0_pr *( s_ij(i,4)*s_ij(i,4) + s_ij(i,5)*s_ij(i,5) + s_ij(i,6)*s_ij(i,6) )
       END DO

       !find nu_t turbulent eddy viscosity
       SELECT CASE (sgsmodel)
       CASE(1) !constant coefficient smagorinski
          IF( sgsmodel_coef1 == 0.0_pr ) THEN
             PRINT *,' sgsmodel_coef1 == 0.0, setting nu_t = 0.0 '
             nu_t = 0.0_pr
          ELSE
             Cs = sgsmodel_coef1
             PRINT *, 'Setting Cs = ', sgsmodel_coef1
             ! 
             ! calculate nu_t
             !
             CALL calc_nu_t( nu_t, s_ij, nlocal, Cs )

             !
             ! Zero RHS forcing for evolution of model scalars Ilm Imm (they are not used for mdl 2:3)
             !
             sgs_mdl_force(:,n_var_Ilm) = 0.0_pr
             sgs_mdl_force(:,n_var_Imm) = 0.0_pr
          END IF
       CASE(2:3) !dynamic coefficient smagorinski
          CALL smag_dyn ( nu_t,  nlocal, u(1:nlocal,1:dim) ) 

          !
          ! Zero RHS forcing for evolution of model scalars Ilm Imm (they are not used for mdl 2:3)
          !
          sgs_mdl_force(:,n_var_Ilm) = 0.0_pr
          sgs_mdl_force(:,n_var_Imm) = 0.0_pr
       CASE (4) !Lagrangian dynamic coefficient Smagorinski (LDM)
          CALL smag_dyn_Meneveau ( u, nu_t,  nlocal, init_flag ) 

       CASE DEFAULT
          PRINT *, 'Error, Unknown model type in sgs_mdl_force(), sgsmodel = ', sgsmodel
          PRINT *, 'Exiting...'
          STOP
       END SELECT

       PRINT *, 'sgs_mdl_force() MINMAXVAL (nu_t(:) ) ',  &
            MINVAL (nu_t(:) ), MAXVAL (nu_t(:) )

       ! this needs to be changed to use dA/volume because sum(dA) is the
       ! volume of the domain I am not changign it now to stay consistent
       ! with phd runs DG 
       total_resolved_diss = SUM( dA*nu * twoSijSij)
       total_sgs_diss = SUM(dA* nu_t * twoSijSij)
       CALL parallel_global_sum( REAL=total_resolved_diss )
       CALL parallel_global_sum( REAL=total_sgs_diss )

       !
       ! Here nu_t(:) = Cs eps^2 * |S| 
       !

       ! this factor 2.0_pr *nu_t is because below we want  nu_t*2*sij so I just
       ! multiply the factor once here
       nu_t(:) =  2.0_pr *  nu_t(:)

       PRINT *,'test1'
       !
       ! DynSmodGridFiltS then we use the grid filter for Sij
       ! in finding tau = 2 nut Sij
       ! if NOT DynSmodGridFiltS we base the Sij filter on epsilon alone
       ! (use du to hold the grid filtered version of u )
       IF(  DynSmodGridFiltS ) THEN
          du(1,1:nlocal,1:dim) = u(1:nlocal,1:dim)
          CALL wlt_filt_wmask( du(1,:,1:dim),dim,u_g_mdl_gridfilt_mask)
          CALL Sij( du(1,:,:),nlocal, s_ij(:,1:6) )
       END IF
       PRINT *,'test2'
       !find d ( nu*Sij)/dx_j
       CALL c_diff_fast(nu_t(:)*s_ij(:,1), du, d2u, j_lev, nlocal, deriv_meth_sgs, 10, 1, 1, 1) !d s_11/dx_i
       sgs_mdl_force(:,1) = du(1,:,1)!d s_11/dx_1
       CALL c_diff_fast(nu_t(:)*s_ij(:,2), du, d2u, j_lev, nlocal, deriv_meth_sgs, 10, 1, 1, 1) !d s_22/dx_i
       sgs_mdl_force(:,2) = du(1,:,2)!d s_22/dx_2
       CALL c_diff_fast(nu_t(:)*s_ij(:,3), du, d2u, j_lev, nlocal, deriv_meth_sgs, 10, 1, 1, 1) !d s_33/dx_i
       sgs_mdl_force(:,3) = du(1,:,3)!d s_33/dx_3
       CALL c_diff_fast(nu_t(:)*s_ij(:,4), du, d2u, j_lev, nlocal, deriv_meth_sgs, 10, 1, 1, 1) !d s_12/dx_i
       sgs_mdl_force(:,1) =  sgs_mdl_force(:,1) + du(1,:,2) ! d s_11/dx_1 + d s_12/dx_2
       sgs_mdl_force(:,2) =  sgs_mdl_force(:,2) + du(1,:,1) ! d s_22/dx_2 + d s_21/dx_1
       CALL c_diff_fast(nu_t(:)*s_ij(:,5), du, d2u, j_lev, nlocal, deriv_meth_sgs, 10, 1, 1, 1) !d s_13/dx_i
       sgs_mdl_force(:,1) =  sgs_mdl_force(:,1) + du(1,:,3) ! d s_11/dx_1 + d s_12/dx_2 + d s_13/dx_3
       sgs_mdl_force(:,3) =  sgs_mdl_force(:,3) + du(1,:,1) ! d s_33/dx_3 + d s_31/dx_1
       PRINT *,'test3'
       CALL c_diff_fast(nu_t(:)*s_ij(:,6), du, d2u, j_lev, nlocal, deriv_meth_sgs, 10, 1, 1, 1) !d s_23/dx_i
       sgs_mdl_force(:,2) =  sgs_mdl_force(:,2) + du(1,:,3) ! d s_22/dx_2 + d s_21/dx_1 + d s_23/dx_3
       sgs_mdl_force(:,3) =  sgs_mdl_force(:,3) + du(1,:,2) ! d s_33/dx_3 + d s_31/dx_1 + d s_32/dx_2
       PRINT *,'test4'

       DEALLOCATE(s_ij)
       DEALLOCATE(nu_t)
       DEALLOCATE(twoSijSij)





       print *,'MinMaxVal sgs_mdl_force(:,1) ',  MINVAL(sgs_mdl_force(:,1)),&
            MAXVAL(sgs_mdl_force(:,1))
       print *,'MinMaxVal sgs_mdl_force(:,2) ',  MINVAL(sgs_mdl_force(:,2)), &
            MAXVAL(sgs_mdl_force(:,2))
       print *,'MinMaxVal sgs_mdl_force(:,3) ',  MINVAL(sgs_mdl_force(:,3)), &
            MAXVAL(sgs_mdl_force(:,3))
       print *,'MinMaxVal sgs_mdl_force(:,4) ',  MINVAL(sgs_mdl_force(:,4)), &
            MAXVAL(sgs_mdl_force(:,4))
       print *,'MinMaxVal sgs_mdl_force(:,5) ',  MINVAL(sgs_mdl_force(:,5)), &
            MAXVAL(sgs_mdl_force(:,5))

    ELSE
       sgs_mdl_force = 0.0_pr
    END IF



    nlocal_last = nlocal
    init_flag = .FALSE.
    PRINT * ,'leaving, user_sgs_force u(1:5,4:5) ', u(1:5,4:5)		        

  END  SUBROUTINE user_sgs_force

  !
  ! this is a routine that was just used for finding 2 nu sum(sijsij)
  ! in the case of a non-adaptive grid. it was used only for testing so far.
  ! It needs to do a weighted sum to work with the adapted grid
  !
  ! vis - viscosity 
  SUBROUTINE find_2nu_sijsij( u, nwlt_local, viscosity) 
    USE vector_util_mod
    IMPLICIT NONE
    INTEGER,    INTENT (IN) :: nwlt_local
    REAL (pr), DIMENSION (1:nwlt_local,1:dim) , INTENT (INOUT) :: u
    REAL (pr), DIMENSION (1:nwlt_local,6) :: s 
    REAL (pr)   viscosity,  smagcoef
    REAL (pr), DIMENSION (1:nwlt_local) :: SijSij
    INTEGER :: i

    smagcoef = 1.0

    CALL Sij( u,nwlt_local, s )

    SijSij = 0.0_pr
    DO i=1,nwlt_local
       SijSij(i) = SijSij(i) + s(i,1)*s(i,1) + s(i,2)*s(i,2) + s(i,3)*s(i,3) + &
            2.0_pr *( s(i,4)*s(i,4) + s(i,5)*s(i,5) + s(i,6)*s(i,6) )
    END DO


    PRINT *, 'sum 2 nu SijSij = ', 2.0_pr * viscosity * sum(SijSij)

!!$  DO i=1,6
!!$     s(
!!$  ENDDO
  END SUBROUTINE find_2nu_sijsij

  !
  ! find turbulent eddy viscosity for constant smagorinski coeffient model
  !
  ! for now uses the model coeffient read from the input file: sgsmodel_coef1
  !
  ! NOTE this should be done with Sij grid filt if DynSmodGridFilt==TRUE
  ! to make it consistent with the Dyn Mdl
  !
  SUBROUTINE calc_nu_t( nu_t, s_ij, nwlt_local, smagcoef ) 
    USE pde
    IMPLICIT NONE
    INTEGER,    INTENT (IN) :: nwlt_local
    REAL (pr), DIMENSION (1:nwlt_local) , INTENT (OUT) :: nu_t
    REAL (pr), DIMENSION (1:nwlt_local,6), INTENT (IN) :: s_ij 
    REAL (pr)  smagcoef
    REAL (pr), DIMENSION (1:nwlt_local) :: Smod
    INTEGER :: i

    PRINT *, 'Calculating nu_t,  smagcoef = ', smagcoef
    DO i=1,nwlt_local
       nu_t(i) =  smagcoef * SQRT( &
            2.0_pr *(  s_ij(i,1)*s_ij(i,1) + s_ij(i,2)*s_ij(i,2) + s_ij(i,3)*s_ij(i,3) ) + &
            4.0_pr *( s_ij(i,4)*s_ij(i,4) + s_ij(i,5)*s_ij(i,5) + s_ij(i,6)*s_ij(i,6) ) )
    END DO

    PRINT *, 'calc_nu_t() MIN/MAXVAL( nu_t ) = ', MINVAL(nu_t), MAXVAL(nu_t)

  END SUBROUTINE calc_nu_t

  !!
  ! find turbulent eddy viscosity for dynamic smagorinski  model
  !
  ! dynCs -result 
  ! s_ij  - sij based on u>eps+adj
  ! nwlt
  ! u_in - u on current adaptive grid (u>eps+adj)
  !
  ! Globals accessed
  !  sgsmodel ! 2 averaged Dynamic coeff, 3 non-averaged,else error
  !  Cs - Cs = the Averaged Dynamic Coeff (For either sgsmodel case)
  !  sgsmodel_coef1 - Cs is multiplied by sgsmodel_coef1 (if sgsmodel_coef1 /= 0.0)
  !
  SUBROUTINE smag_dyn( nu_t,  nwlt_local, u_in ) 
    USE pde
	USE util_vars
    USE vector_util_mod
    USE util_mod
    USe parallel
    IMPLICIT NONE
    INTEGER,    INTENT (IN) :: nwlt_local
    REAL (pr), DIMENSION (1:nwlt_local), INTENT (INOUT) :: nu_t ! dynamic smagorinski coeffient
    REAL (pr), DIMENSION (1:nwlt_local,6) :: s_ij 
    REAL (pr), DIMENSION (1:nwlt_local,dim), INTENT (IN) :: u_in
    !REAL (pr)  smagcoef
    REAL (pr), DIMENSION (1:nwlt_local) :: Smod, Smod2, trace
    REAL (pr), DIMENSION (1:nwlt_local,6) :: SSijgeps,SM, L 
    INTEGER :: i
    REAL(pr) :: tmp1

    IF( dim /= 3 ) THEN
       PRINT *, 'Error, Dynamic smagorinski model is only defined for 3D simulations'
       PRINT *, 'Exiting...'
       stop
    END IF

    !TEST
    ! make test filter u_g_mdl_testfilt_mask == u_g_mdl_gridfilt_mask 
    ! make grid filter u_g_mdl_gridfilt_mask = 1.0 (none or eps +adj+recontruction chck)
    ! 
    !u_g_mdl_testfilt_mask = u_g_mdl_gridfilt_mask
    !u_g_mdl_gridfilt_mask = 1.0_pr
    !
    !

    PRINT *,'In smag_dyn() '
    PRINT *,'SUM(u_g_mdl_gridfilt_mask) ', SUM(u_g_mdl_gridfilt_mask)
    PRINT *,'SUM(u_g_mdl_testfilt_mask) ', SUM(u_g_mdl_testfilt_mask)
    PRINT *,'SUM(u_g_mdl_gridfilt_mask-u_g_mdl_testfilt_mask)',SUM(u_g_mdl_gridfilt_mask-u_g_mdl_testfilt_mask)
    !
    ! grid filter u_in, Save it in L(:,4:6)
    !
    L(:,4:6) = u_in(:,1:3)  
    CALL wlt_filt_wmask( L(:,4:6),3,u_g_mdl_gridfilt_mask)

    ! find Sij (based on grid filtered u)
    CALL Sij( L(:,4:6),nwlt_local, s_ij )

    ! find  ||S_gridfilt||
    DO i=1,nwlt_local
       Smod(i) =  SQRT( &
            2.0_pr *(  s_ij(i,1)*s_ij(i,1) + s_ij(i,2)*s_ij(i,2) + s_ij(i,3)*s_ij(i,3) ) + &
            4.0_pr *( s_ij(i,4)*s_ij(i,4) + s_ij(i,5)*s_ij(i,5) + s_ij(i,6)*s_ij(i,6) ) )
    END DO

    ! find ||S_gridfilt|| Sij_gridfilt (6 components)
    DO i=1,6
       SSijgeps(:,i) =  Smod(:) * s_ij(:,i)
    END DO

    !
    ! test filter ||S_gridfilt|| Sij_gridfilt 
    !
    CALL wlt_filt_wmask(SSijgeps,6,u_g_mdl_testfilt_mask)

    ! now SSijgeps == ( ||S_gridfilt|| Sij_gridfilt )_testfilt

    ! 
    !  test filter u and save in L(:,1:3)
    ! 
    L(:,1:3) = u_in(:,1:3)
    CALL wlt_filt_wmask(L(:,1:3) ,3,u_g_mdl_testfilt_mask) 

    ! find Sij based on the test filtered  u
    CALL Sij( L(:,1:3) ,nwlt_local, s_ij )

    ! find  ||S>2eps|| (store it in Smod2 )
    DO i=1,nwlt_local
       Smod2(i) =  SQRT( &
            2.0_pr *(  s_ij(i,1)*s_ij(i,1) + s_ij(i,2)*s_ij(i,2) + s_ij(i,3)*s_ij(i,3) ) + &
            4.0_pr *( s_ij(i,4)*s_ij(i,4) + s_ij(i,5)*s_ij(i,5) + s_ij(i,6)*s_ij(i,6) ) )
    END DO



    ! find ||S>2eps|| Sij>2eps (6 components)
    DO i=1,6
       SM(:,i) = Smod2(:) * s_ij(:,i)
    END DO

    !now SM == ||S>2eps|| Sij>2eps

    !
    ! find Mij
    DO i=1,6
!!$     ! special case of either >eps or none grid filter with
!!$     ! >2eps test filter.  
!!$     IF( (mdl_filt_type_grid == 0 .OR. mdl_filt_type_grid == 1 ) &
!!$          .AND. mdl_filt_type_test == 2 ) THEN
       PRINT *,' Mij =',Mcoeff ,'* |S>2eps| Sij>2eps - (|S|Sij )>2eps '
       SM(:,i) = Mcoeff* SM(:,i) - SSijgeps(:,i)
!!$     ELSE
!!$        ! for other cases there may 
!!$        PRINT *,' Mij =  |S>test| Sij>test - (|S|Sij )>test '
!!$        SM(:,i) = SM(:,i) - SSijgeps(:,i)
!!$     END IF
    END DO

    !
    ! Subtract trace for Mij (in SM)
    ! only makes sense in dim=3 case
    !
    trace(:) = (SM(:,1) + SM(:,2) + SM(:,3)) 
    !  PRINT *,'Mij(), MINMAXVAL(trace(Mij)) =', MINVAL(trace),MAXVAL(trace)
    IF( Mijtraceless ) THEN
       DO i=1,nwlt_local
          trace(i) =  trace(i)/3.0_pr
          SM(i,1:3) = SM(i,1:3) - trace(i)
       END DO
       trace(:) = (SM(:,1) + SM(:,2) + SM(:,3)) 
       !     PRINT *,'Mij()_traceless, MINMAXVAL(trace(Mij)) =', MINVAL(trace),MAXVAL(trace)
    END IF


    !
    ! Now find L
    !

    !CALL wlt_filt_wmask(SSijgeps(:,1:3) ,3,u_g_mdl_gridfilt_mask) !filt u >eps

    !
    ! find u_gridfilt u_gridfilt
    !note   L(:,4:6) = u_in(:,1:3)_gridfilt
    SSijgeps(:,1) = L(:,4) * L(:,4) !u_in(:,1) * u_in(:,1)
    SSijgeps(:,2) = L(:,5) * L(:,5) !u_in(:,2) * u_in(:,2)
    SSijgeps(:,3) = L(:,6) * L(:,6) !u_in(:,3) * u_in(:,3)
    SSijgeps(:,4) = L(:,4) * L(:,5) !u_in(:,1) * u_in(:,2)
    SSijgeps(:,5) = L(:,4) * L(:,6) !u_in(:,1) * u_in(:,3)
    SSijgeps(:,6) = L(:,5) * L(:,6) !u_in(:,2) * u_in(:,3)

    !
    ! The first version is Lij = (ugrid ugrid)_test - u_test u_test
    !
    IF( .NOT. DynMdlFiltLijLast ) THEN
       CALL wlt_filt_wmask(SSijgeps(:,1:6) ,6,u_g_mdl_testfilt_mask) !testfilt Lij 
    END IF

    !
    ! find Lij, Note that u_test is in L(:,1:3)
    ! so the order is important here.
    !
    L(:,4) = SSijgeps(:,4) - L(:,1) * L(:,2)
    L(:,5) = SSijgeps(:,5) - L(:,1) * L(:,3)
    L(:,6) = SSijgeps(:,6) - L(:,2) * L(:,3)
    L(:,1) = SSijgeps(:,1) - L(:,1) * L(:,1)
    L(:,2) = SSijgeps(:,2) - L(:,2) * L(:,2)
    L(:,3) = SSijgeps(:,3) - L(:,3) * L(:,3)

    !
    ! The second  version is Lij = (ugrid ugrid - u_test u_test )_test
    !
    IF(  DynMdlFiltLijLast ) THEN
       CALL wlt_filt_wmask(L(:,1:6) ,6,u_g_mdl_testfilt_mask) !testfilt Lij 
    END IF

    !
    ! Subtract trace for Lij (in SM)
    ! only makes sense in dim=3 case
    !
    trace(:) = (L(:,1) + L(:,2) + L(:,3)) 
    !PRINT *,'Lij(), MINMAXVAL(trace(Lij)) =', MINVAL(trace),MAXVAL(trace)
    IF( Lijtraceless ) THEN
       DO i=1,nwlt_local
          trace(i) =  trace(i)/3.0_pr
          L(i,1:3) = L(i,1:3) - trace(i)
       END DO
       trace(:) = (L(:,1) + L(:,2) + L(:,3)) 
       !PRINT *,'Lij()_traceless, MINMAXVAL(trace(Lij)) =', MINVAL(trace),MAXVAL(trace)
    END IF


    !
    ! find find LijMij and MijMij
    ! (store LijMij in SSijgeps(:,1) and
    ! store MijMij in SSijgeps(:,2)
    !
    SSijgeps(:,1) = L(:,1)*SM(:,1) + L(:,2)*SM(:,2) + L(:,3)*SM(:,3) + &
         2.0_pr *( L(:,4)*SM(:,4) + L(:,5)*SM(:,5) + L(:,6)*SM(:,6))

    SSijgeps(:,2) = SM(:,1)*SM(:,1) + SM(:,2)*SM(:,2) + SM(:,3)*SM(:,3) + &
         2.0_pr *( SM(:,4)*SM(:,4) + SM(:,5)*SM(:,5) + SM(:,6)*SM(:,6))



    !
    ! find dynCs = <LijMij>/( MijMij) 
    ! 
    ! in either case calculate the averaged value of Cs
    ! to be logged in __.case_isoturb.log
    Cs = - 0.5* SUM(dA*SSijgeps(:,1))
    tmp1 = SUM(dA*SSijgeps(:,2))
    CALL parallel_global_sum( REAL=Cs )
    CALL parallel_global_sum( REAL=tmp1 )
    Cs = Cs / tmp1

    IF( sgsmodel_coef1 /= 0.0_pr ) THEN
       Cs =  Cs * sgsmodel_coef1
       PRINT *, 'smag_dyn() Setting  Cs =  Cs * sgsmodel_coef1 '
    END IF

    IF( clip_Cs ) THEN
       Cs =  MAX(Cs , 0.0_pr )
       PRINT *, 'Cs =  MAX(Cs , 0.0_pr ) '
    END IF

    IF( sgsmodel == 3 ) THEN !find local Cs
       nu_t(:) = - 0.5 *SSijgeps(:,1)/ SSijgeps(:,2)
       IF( sgsmodel_coef1 /= 0.0_pr ) THEN
          nu_t(:) =   nu_t(:) * sgsmodel_coef1
          PRINT *, 'smag_dyn() Setting  Clocal(:) =  Clocal(:) * sgsmodel_coef1 '
       END IF
       IF( clip_Cs ) THEN
          PRINT *, ' Number of points in Clocal to be clipped:',COUNT( nu_t(:) < 0.0_pr )
          nu_t(:) =  MAX( nu_t(:) , 0.0_pr )
          PRINT *, 'Cs_local =  MAX(Cs_local , 0.0_pr ) '
       END IF

    ENDIF

    PRINT *,'Dynamic Smagorinski Coeff = ', Cs

    !
    ! here we need |S_(on adaptive grid)|
    ! in other word we need |S| with Sij based on u_in
    ! IF( mdl_filt_type_grid == 0 ) then Smod has what we need from
    ! above. If not we need to recalculate it.
    IF( mdl_filt_type_grid /= 0 .AND. .NOT. DynSmodGridFilt ) THEN
       PRINT *,'smag_dyn() Recalculate Smod for eps+adj grid'
       ! find Sij based on u_in
       CALL Sij( u_in(:,1:3),nwlt_local, s_ij )

       ! find  ||S_gridfilt||
       DO i=1,nwlt_local
          Smod(i) =  SQRT( &
               2.0_pr *(  s_ij(i,1)*s_ij(i,1) + s_ij(i,2)*s_ij(i,2) + s_ij(i,3)*s_ij(i,3) ) + &
               4.0_pr *( s_ij(i,4)*s_ij(i,4) + s_ij(i,5)*s_ij(i,5) + s_ij(i,6)*s_ij(i,6) ) )
       END DO

    END IF

    ! Now calculate nu_t
    !  Cs * ||S||
    IF( sgsmodel == 2 ) nu_t(:) =  Cs * Smod(:)
    ! non-averaged Cs case
    IF( sgsmodel == 3 ) nu_t(:) =  nu_t(:) * Smod(:)




    !
    ! clip nu + nu_t(:) if called for
    !
    IF( clip_nu_nut ) THEN
       PRINT *, 'Dyn_Smag number of points of (nu +  nu_t(:)) < 0.0 =',&
            COUNT( nu + nu_t(:) < 0.0_pr )
       nu_t(:) =  MAX( nu_t(:), -nu )
       !PRINT *, 'vis() After clipping MINMAXVAL ((nu + nu_t(:)) ) ',&
       !MINVAL (nu + nu_t(:) ), MAXVAL (nu + nu_t(:) )
    END IF




    !PRINT *,'smag_dyn(), MINMAXVAL(nu_t(:)) =',  MINVAL(nu_t(:)), MAXVAL(nu_t(:))


  END SUBROUTINE smag_dyn

  !!
  ! find turbulent eddy viscosity for dynamic smagorinski  model using 
  ! Meneveau's langrangian model avereraging
  !
  ! In this routine we calculate nu_t based on the current value of Ilm,Imm which are convected scalars
  ! and calculate the RHS forcing for the convection of Ilm,Imm for the next time step.
  !
  ! dynCs -result 
  ! s_ij  - sij based on u>eps+adj
  ! nwlt
  ! init_flag - true if we are initializing the model
  !
  ! Globals accessed
  !  sgsmodel ! 2 averaged Dynamic coeff, 3 non-averaged,else error
  !  Cs - Cs = the Averaged Dynamic Coeff (For either sgsmodel case)
  !  sgsmodel_coef1 - Cs is multiplied by sgsmodel_coef1 (if sgsmodel_coef1 /= 0.0)
  !  u(:,n_var) field variables, uvw, Ilm, Imm, pressure
  !
  SUBROUTINE smag_dyn_Meneveau(u,  nu_t,  nwlt_local, init_flag ) 
    USE pde
	USE wlt_trns_vars
	USE util_vars
    USE vector_util_mod
    USE util_mod
    !USE field
    USE parallel
    IMPLICIT NONE
    INTEGER,    INTENT (IN) :: nwlt_local
    REAL (pr), DIMENSION (nwlt_local,n_integrated), INTENT (INOUT) :: u
    REAL (pr), DIMENSION (2,nwlt_local) :: fu

    LOGICAL,                         INTENT (IN) :: init_flag !true if we are initializing the model

    REAL (pr), DIMENSION (1:nwlt_local), INTENT (INOUT) :: nu_t ! dynamic smagorinski coeffient
    REAL (pr), DIMENSION (1:nwlt_local,6) :: s_ij 
    !  REAL (pr), DIMENSION (1:nwlt_local,dim), INTENT (IN) :: u_in
    !REAL (pr)  smagcoef
    REAL (pr), DIMENSION (1:nwlt_local) :: Smod, Smod2, trace
    REAL (pr), DIMENSION (1:nwlt_local,6) :: SSijgeps,SM, L 
!    REAL (pr)  :: mindelta  !, Cs_tmp
    INTEGER :: i, k, ii, j, j_df, wlt_type, face_type, idim
    REAL(pr) :: tmp1

    IF( dim /= 3 ) THEN
       PRINT *, 'Error, Dynamic smagorinski model is only defined for 3D simulations'
       PRINT *, 'Exiting...'
       stop
    END IF

    IF(sgsmodel /= 4 ) THEN !problem
       PRINT *,'sgsmodel should be 4 to call smag_dyn_Meneveau()'
       STOP
    END IF

    PRINT *,'smag_dyn_Meneveau(), LDMtheta = ', LDMtheta
    ! constant for LDM
    !  LDMtheta = 100.0_pr  !1.5_pr

    !TEST
    ! make test filter u_g_mdl_testfilt_mask == u_g_mdl_gridfilt_mask 
    ! make grid filter u_g_mdl_gridfilt_mask = 1.0 (none or eps +adj+recontruction chck)
    ! 
    !u_g_mdl_testfilt_mask = u_g_mdl_gridfilt_mask
    !u_g_mdl_gridfilt_mask = 1.0_pr
    !
    !

    PRINT *,'In smag_dyn() '
    PRINT *,'SUM(u_g_mdl_gridfilt_mask) ', SUM(u_g_mdl_gridfilt_mask)
    PRINT *,'SUM(u_g_mdl_testfilt_mask) ', SUM(u_g_mdl_testfilt_mask)
    PRINT *,'SUM(u_g_mdl_gridfilt_mask-u_g_mdl_testfilt_mask)',SUM(u_g_mdl_gridfilt_mask-u_g_mdl_testfilt_mask)
    !
    ! grid filter u_in, Save it in L(:,4:6)
    !
    L(:,4:6) = u(:,1:3)  
    CALL wlt_filt_wmask( L(:,4:6),3,u_g_mdl_gridfilt_mask)

    ! find Sij (based on grid filtered u)
    CALL Sij( L(:,4:6),nwlt_local, s_ij )

    ! find  ||S_gridfilt||
    DO i=1,nwlt_local
       Smod(i) =  SQRT( &
            2.0_pr *(  s_ij(i,1)*s_ij(i,1) + s_ij(i,2)*s_ij(i,2) + s_ij(i,3)*s_ij(i,3) ) + &
            4.0_pr *( s_ij(i,4)*s_ij(i,4) + s_ij(i,5)*s_ij(i,5) + s_ij(i,6)*s_ij(i,6) ) )
    END DO

    ! find ||S_gridfilt|| Sij_gridfilt (6 components)
    DO i=1,6
       SSijgeps(:,i) =  Smod(:) * s_ij(:,i)
    END DO

    !
    ! test filter ||S_gridfilt|| Sij_gridfilt 
    !
    CALL wlt_filt_wmask(SSijgeps,6,u_g_mdl_testfilt_mask)

    ! now SSijgeps == ( ||S_gridfilt|| Sij_gridfilt )_testfilt

    ! 
    !  test filter u and save in L(:,1:3)
    ! 
    L(:,1:3) = u(:,1:3)
    CALL wlt_filt_wmask(L(:,1:3) ,3,u_g_mdl_testfilt_mask) 

    ! find Sij based on the test filtered  u
    CALL Sij( L(:,1:3) ,nwlt_local, s_ij )

    ! find  ||S_testfilt|| (store it in Smod2 )
    DO i=1,nwlt_local
       Smod2(i) =  SQRT( &
            2.0_pr *(  s_ij(i,1)*s_ij(i,1) + s_ij(i,2)*s_ij(i,2) + s_ij(i,3)*s_ij(i,3) ) + &
            4.0_pr *( s_ij(i,4)*s_ij(i,4) + s_ij(i,5)*s_ij(i,5) + s_ij(i,6)*s_ij(i,6) ) )
    END DO



    ! find ||S>2eps|| Sij>2eps (6 components)
    DO i=1,6
       SM(:,i) = Smod2(:) * s_ij(:,i)
    END DO

    !now SM == ||S>2eps|| Sij>2eps

    !
    ! find Mij
    DO i=1,6
       PRINT *,' Mij =',Mcoeff ,'*  (|S|Sij )>2eps - |S>2eps| Sij>2eps '
       SM(:,i) =  SSijgeps(:,i) - Mcoeff* SM(:,i) 
    END DO

    !
    ! Subtract trace for Mij (in SM)
    ! only makes sense in dim=3 case
    !
    trace(:) = (SM(:,1) + SM(:,2) + SM(:,3)) 
    !  PRINT *,'Mij(), MINMAXVAL(trace(Mij)) =', MINVAL(trace),MAXVAL(trace)
    IF( Mijtraceless ) THEN
       DO i=1,nwlt_local
          trace(i) =  trace(i)/3.0_pr
          SM(i,1:3) = SM(i,1:3) - trace(i)
       END DO
       trace(:) = (SM(:,1) + SM(:,2) + SM(:,3)) 
       !     PRINT *,'Mij()_traceless, MINMAXVAL(trace(Mij)) =', MINVAL(trace),MAXVAL(trace)
    END IF


    !
    ! Now find L
    !

    !CALL wlt_filt_wmask(SSijgeps(:,1:3) ,3,u_g_mdl_gridfilt_mask) !filt u >eps

    !
    ! find u_gridfilt u_gridfilt
    !note   L(:,4:6) = u_in(:,1:3)_gridfilt
    SSijgeps(:,1) = L(:,4) * L(:,4) !u_in(:,1) * u_in(:,1)
    SSijgeps(:,2) = L(:,5) * L(:,5) !u_in(:,2) * u_in(:,2)
    SSijgeps(:,3) = L(:,6) * L(:,6) !u_in(:,3) * u_in(:,3)
    SSijgeps(:,4) = L(:,4) * L(:,5) !u_in(:,1) * u_in(:,2)
    SSijgeps(:,5) = L(:,4) * L(:,6) !u_in(:,1) * u_in(:,3)
    SSijgeps(:,6) = L(:,5) * L(:,6) !u_in(:,2) * u_in(:,3)

    !
    ! The first version is Lij = (ugrid ugrid)_test - u_test u_test
    !
    IF( .NOT. DynMdlFiltLijLast ) THEN
       CALL wlt_filt_wmask(SSijgeps(:,1:6) ,6,u_g_mdl_testfilt_mask) !testfilt Lij 
    END IF

    !
    ! find Lij, Note that u_test is in L(:,1:3)
    ! so the order is important here.
    !
    L(:,4) = SSijgeps(:,4) - L(:,1) * L(:,2)
    L(:,5) = SSijgeps(:,5) - L(:,1) * L(:,3)
    L(:,6) = SSijgeps(:,6) - L(:,2) * L(:,3)
    L(:,1) = SSijgeps(:,1) - L(:,1) * L(:,1)
    L(:,2) = SSijgeps(:,2) - L(:,2) * L(:,2)
    L(:,3) = SSijgeps(:,3) - L(:,3) * L(:,3)

    !
    ! The second  version is Lij = (ugrid ugrid - u_test u_test )_test
    !
    IF(  DynMdlFiltLijLast ) THEN
       CALL wlt_filt_wmask(L(:,1:6) ,6,u_g_mdl_testfilt_mask) !testfilt Lij 
    END IF

    !
    ! Subtract trace for Lij (in SM)
    ! only makes sense in dim=3 case
    !
    trace(:) = (L(:,1) + L(:,2) + L(:,3)) 
    !PRINT *,'Lij(), MINMAXVAL(trace(Lij)) =', MINVAL(trace),MAXVAL(trace)
    IF( Lijtraceless ) THEN
       DO i=1,nwlt_local
          trace(i) =  trace(i)/3.0_pr
          L(i,1:3) = L(i,1:3) - trace(i)
       END DO
       trace(:) = (L(:,1) + L(:,2) + L(:,3)) 
       !PRINT *,'Lij()_traceless, MINMAXVAL(trace(Lij)) =', MINVAL(trace),MAXVAL(trace)
    END IF


    !
    ! find find LijMij and MijMij
    ! (store LijMij in SSijgeps(:,1) and
    ! store MijMij in SSijgeps(:,2)
    !


    SSijgeps(:,1) =  L(:,1)*SM(:,1) + L(:,2)*SM(:,2) + L(:,3)*SM(:,3) + &
         2.0_pr *( L(:,4)*SM(:,4) + L(:,5)*SM(:,5) + L(:,6)*SM(:,6))

    SSijgeps(:,2) = SM(:,1)*SM(:,1) + SM(:,2)*SM(:,2) + SM(:,3)*SM(:,3) + &
         2.0_pr *( SM(:,4)*SM(:,4) + SM(:,5)*SM(:,5) + SM(:,6)*SM(:,6))


    !  print *,'no.times LijMij<0', count( SSijgeps(:,1) < 0.0_pr)
    !  PRINT *,'MINMAX LijMij', MINVAL( SSijgeps(:,1) ), MAXVAL( SSijgeps(:,1) )
    !  PRINT *,'clipping LijMij < 0.0 to 0.0 '
    !  SSijgeps(:,1) = MAX( SSijgeps(:,1) , 0.0_pr )


    !
    ! this part to find grid-filter local width delta(i) for LDM model
    !
    IF( ALLOCATED(delta) ) DEALLOCATE(delta)
    ALLOCATE( delta(1:nwlt_local) )
    ! this is for uniform mesh, for streched meshes use DB_get_all_local_h
    DO j = 1, j_lev
       DO wlt_type = MIN(j-1,1),2**dim-1
          DO face_type = 0, 3**dim - 1
             DO j_df = j, j_lev
                DO k = 1, indx_DB(j_df,wlt_type,face_type,j)%length  
                   i = indx_DB(j_df,wlt_type,face_type,j)%p(k)%i
                   delta(i) = PRODUCT(h(j_df,1:dim))**(1.0_pr/REAL(dim,pr))
                END DO
             END DO
          END DO
       END DO
    END DO
    PRINT *,'MINMAX delta', MINVAL( delta(:) ), MAXVAL( delta(:) )


    !
    ! Mij definition including delta^2
    !
    IF ( deltaMij ) THEN
       SSijgeps(:,1) = delta(:)**2 * SSijgeps(:,1)
       SSijgeps(:,2) = delta(:)**4 * SSijgeps(:,2)
    END IF

    !
    ! initialize Ilm = LijMij and Imm = MijMij
    !
    IF( init_flag ) Then
       u(:,n_var_Ilm) = SSijgeps(:,1) !initialize Ilm = LijMij only to calculate Cs average for first time step
       ! below Ilm is set to Ilm = Cs * Imm 
       PRINT *, '....... initialized Imm to a its average'
!       u(:,n_var_Imm) = SSijgeps(:,2) ! initialize Imm = MijMij
       tmp1 = SUM(dA*SSijgeps(:,2))/sumdA_global
       CALL parallel_global_sum( REAL=tmp1 )
       u(:,n_var_Imm) = tmp1 ! initialize Imm to a its average
    END IF

    !
    ! find dynCs = Ilm/ Imm 
    ! 
    ! in either case calculate the averaged value of Cs
    ! to be logged in __.case_isoturb.log

    ! In first iteration this is really LijMij/MijMij
    ! but in subsequent iteration it will be Ilm/Imm
    Cs =  0.5* SUM(dA*u(:,n_var_Ilm) )
    tmp1 = SUM(dA*u(:,n_var_Imm))
    CALL parallel_global_sum( REAL=Cs )
    CALL parallel_global_sum( REAL=tmp1 )
    Cs = Cs / tmp1

    IF( sgsmodel_coef1 /= 0.0_pr ) THEN
       Cs =  Cs * sgsmodel_coef1
       PRINT *, 'smag_dyn() Setting  Cs =  Cs * sgsmodel_coef1 '
    END IF

    IF( clip_Cs ) THEN
       Cs =  MAX(Cs , 0.0_pr )
       PRINT *, 'Cs =  MAX(Cs , 0.0_pr ) '
    END IF

    IF( init_flag ) Then
       ! initialize Ilm = Cseps^2 * Imm 
       u(:,n_var_Ilm) = 2.0_pr* Cs * u(:,n_var_Imm)
       !TEST
       !Cs_tmp =  0.5* SUM(dA*u(:,n_var_Ilm) ) / SUM(dA*u(:,n_var_Imm))
       !PRINT *,'Dynamic Smagorinski Coeff, based on  u(:,n_var_Ilm) = Cs * u(:,n_var_Imm) = ', Cs_tmp
    END IF

    !
    ! clipping Ilm < 0.0 to 0.0
    !
    PRINT *,'MINMAX Ilm', MINVAL( u(:,n_var_Ilm) ), MAXVAL( u(:,n_var_Ilm) )
    PRINT *,'MINMAX Imm', MINVAL( u(:,n_var_Imm) ), MAXVAL( u(:,n_var_Imm) )
    print *, 'no.times Ilm<0', count(u(:,n_var_Ilm) < 0.0_pr)
    print *, 'no.times Imm<0', count(u(:,n_var_Imm) < 0.0_pr)

    !  PRINT *,'clipping Ilm < 0.0 to 0.0 '
    !  u(:,n_var_Ilm) = MAX( u(:,n_var_Ilm) , 0.0_pr ) ! clip all LijMij < 0.0 to 0.0 

    IF( sgsmodel == 4 ) THEN !find local Cs
       nu_t(:) =  0.5 * u(:,n_var_Ilm)/ u(:,n_var_Imm)
!       IF( sgsmodel_coef1 /= 0.0_pr ) THEN
!          nu_t(:) =   nu_t(:) * sgsmodel_coef1
!          PRINT *, 'smag_dyn() Setting  Clocal(:) =  Clocal(:) * sgsmodel_coef1 '
!       END IF
       IF( clip_Cs ) THEN
          PRINT *, ' Number of points in Clocal to be clipped:',COUNT( nu_t(:) < 0.0_pr )
          nu_t(:) =  MAX( nu_t(:) , 0.0_pr )
          PRINT *, 'Cs_local =  MAX(Cs_local , 0.0_pr ) '
       END IF
    ENDIF

    PRINT *,'Dynamic Smagorinski Coeff = ', Cs

    !
    ! here we need |S_(on adaptive grid)|
    ! in other word we need |S| with Sij based on u_in
    ! IF( mdl_filt_type_grid == 0 ) then Smod has what we need from
    ! above. If not we need to recalculate it.
    !
    IF( mdl_filt_type_grid /= 0 .AND. .NOT. DynSmodGridFilt ) THEN
       PRINT *,'smag_dyn() Recalculate Smod for eps+adj grid'
       ! find Sij based on u_in
       CALL Sij( u(:,1:3),nwlt_local, s_ij )
       ! find  ||S_gridfilt||
       DO i=1,nwlt_local
          Smod(i) =  SQRT( &
               2.0_pr *( s_ij(i,1)*s_ij(i,1) + s_ij(i,2)*s_ij(i,2) + s_ij(i,3)*s_ij(i,3) ) + &
               4.0_pr *( s_ij(i,4)*s_ij(i,4) + s_ij(i,5)*s_ij(i,5) + s_ij(i,6)*s_ij(i,6) ) )
       END DO
    END IF

    ! Now calculate nu_t
    !  Cs * ||S||
    IF( init_flag ) Then
       PRINT * ,'>>>Averaged Cs used<<<'
       nu_t(:) =  Cs * Smod(:) !averaged nu_t
       IF( deltaMij ) nu_t(:) =  delta(:)**2 * nu_t(:)
	ELSE

           ! only for output
           Cs = SUM(dA*nu_t(:)) / sumdA_global; 
           CALL parallel_global_sum( REAL=Cs )
           print *, 'Averaged Cs=', Cs

       ! non-averaged Cs case
       PRINT * ,'>>>Local Cs used<<<'
       nu_t(:) =  nu_t(:) * Smod(:)
       IF( deltaMij ) nu_t(:) =  delta(:)**2 * nu_t(:)

    END IF

    !
    ! clip nu + nu_t(:) if called for
    !
    PRINT *, 'Dyn_Smag no. points of (nu +  nu_t(:)) < 0.0 =', COUNT( nu + nu_t(:) < 0.0_pr )
    IF( clip_nu_nut ) THEN
       nu_t(:) =  MAX( nu_t(:), -nu )
       !PRINT *, 'vis() After clipping MINMAXVAL ((nu + nu_t(:)) ) ',&
       !MINVAL (nu + nu_t(:) ), MAXVAL (nu + nu_t(:) )
    END IF

    !
    ! artificial viscosity for Ilm&Imm evolution
    !
    IF( ALLOCATED(nuI) ) DEALLOCATE( nuI )
    ALLOCATE( nuI(1:nwlt_local) )
    nuI(:) = CI*delta(:)**2*Smod(:)
    PRINT *,'MINMAX nuI', MINVAL( nuI(:) ), MAXVAL( nuI(:) )

    !
    ! tuning delta
    !
    !  fix delta(:) to its minimum
    !!PRINT *,'delta fixed to its minimum'
    !!mindelta = MINVAL(delta(:))
    !!delta(:) = mindelta
!    PRINT *,'MINMAX delta', MINVAL( delta(:) ), MAXVAL( delta(:) )

    ! find 1/T = 1/(delta^(1/4)*theta) or ...  (store in delta)
    IF ( deltaMij ) THEN
       delta(:) =  1.0_pr/(LDMtheta*delta(:))
    ELSE
   	   delta(:) =  1.0_pr/(LDMtheta*delta(:)**0.25_pr)
    END IF
    PRINT *,'MINMAX 1/T', MINVAL( delta(:) ), MAXVAL( delta(:) )

    !************************
    !     
    ! Set RHS forcing for evolution of model scalars Ilm Imm based on current LijMij and MijMij
    !
    IF( sgsmodel == 4 ) THEN  
       IF( mdl_form == 1 ) THEN

          ! RHS( Ilm ) = 1/(delta^(1/4) *theta) *(Ilm Imm)^(1/8) (LijMij - Ilm)
          !
          sgs_mdl_force(:,n_var_Ilm) =  delta(:) * &
               (MAX(u(:,n_var_Ilm)*u(:,n_var_Imm),0.0_pr))**(0.125_pr) *( SSijgeps(:,1)-u(:,n_var_Ilm))

          ! RHS( Imm ) = 1/(delta^(1/4)*theta) *(Imm Imm)^(1/8)  (MijMij - Imm)
          !
          sgs_mdl_force(:,n_var_Imm) = delta(:) * &
               (MAX(u(:,n_var_Ilm)*u(:,n_var_Imm),0.0_pr))**(0.125_pr) *( SSijgeps(:,2)-u(:,n_var_Imm))

       ELSE IF ( mdl_form == 2 ) THEN

          ! 1/(delta^(1/4) *theta) * (Ilm Imm )^1/8
          delta(:) = delta(:) * MAX(0.0_pr,u(:,n_var_Ilm)*u(:,n_var_Imm))**(0.125_pr)
          sgs_mdl_force(:,n_var_Ilm) = SSijgeps(:,1) ! LijMij
          sgs_mdl_force(:,n_var_Imm) = SSijgeps(:,2) ! MijMij
          WHERE( u(:,n_var_Ilm) <= 0.0_pr ) sgs_mdl_force(:,n_var_Ilm) = MAX(0.0_pr,sgs_mdl_force(:,n_var_Ilm))

          IF(SpaceTimeAvg) THEN
             PRINT *,'------------------------- Before Filtering -----------------------------------'
             PRINT *,'MINMAX sgs_mdl_force(:,n_var_Ilm): ', &
			 MINVAL( sgs_mdl_force(:,n_var_Ilm) ), MAXVAL( sgs_mdl_force(:,n_var_Ilm) )
             PRINT *,'MINMAX sgs_mdl_force(:,n_var_Imm): ', &
			 MINVAL( sgs_mdl_force(:,n_var_Imm) ), MAXVAL( sgs_mdl_force(:,n_var_Imm) )
             PRINT *,'------------------------------------------------------------------------------'; PRINT *,''
             CALL local_lowpass_filt(sgs_mdl_force(:,n_var_Ilm:n_var_Imm), j_lev, nwlt_local, 2, 1, 2 )
             PRINT *,'------------------------- After  Filtering -----------------------------------'
             PRINT *,'MINMAX sgs_mdl_force(:,n_var_Ilm): ', &
			 MINVAL( sgs_mdl_force(:,n_var_Ilm) ), MAXVAL( sgs_mdl_force(:,n_var_Ilm) )
             PRINT *,'MINMAX sgs_mdl_force(:,n_var_Imm): ', &
			 MINVAL( sgs_mdl_force(:,n_var_Imm) ), MAXVAL( sgs_mdl_force(:,n_var_Imm) )
             PRINT *,'------------------------------------------------------------------------------'; PRINT *,''
          END IF


       ELSE IF ( mdl_form == 3 ) THEN

          sgs_mdl_force(:,n_var_Ilm) = SSijgeps(:,1) ! LijMij
          sgs_mdl_force(:,n_var_Imm) = SSijgeps(:,2) ! MijMij
          WHERE( u(:,n_var_Ilm) <= 0.0_pr ) sgs_mdl_force(:,n_var_Ilm) = MAX(0.0_pr,sgs_mdl_force(:,n_var_Ilm))

          IF(SpaceTimeAvg) THEN
             PRINT *,'------------------------- Before Filtering -----------------------------------'
             PRINT *,'MINMAX sgs_mdl_force(:,n_var_Ilm): ', &
			 MINVAL( sgs_mdl_force(:,n_var_Ilm) ), MAXVAL( sgs_mdl_force(:,n_var_Ilm) )
             PRINT *,'MINMAX sgs_mdl_force(:,n_var_Imm): ', &
			 MINVAL( sgs_mdl_force(:,n_var_Imm) ), MAXVAL( sgs_mdl_force(:,n_var_Imm) )
             PRINT *,'------------------------------------------------------------------------------'; PRINT *,''
             CALL local_lowpass_filt(sgs_mdl_force(:,n_var_Ilm:n_var_Imm), j_lev, nwlt_local, 2, 1, 2 )
             PRINT *,'------------------------- After  Filtering -----------------------------------'
             PRINT *,'MINMAX sgs_mdl_force(:,n_var_Ilm): ', &
			 MINVAL( sgs_mdl_force(:,n_var_Ilm) ), MAXVAL( sgs_mdl_force(:,n_var_Ilm) )
             PRINT *,'MINMAX sgs_mdl_force(:,n_var_Imm): ', &
			 MINVAL( sgs_mdl_force(:,n_var_Imm) ), MAXVAL( sgs_mdl_force(:,n_var_Imm) )
             PRINT *,'------------------------------------------------------------------------------'; PRINT *,''
          END IF


       END IF
    END IF

    PRINT *,'------------------------------------------------------------------------------'
    PRINT *,'-----------------------------LDM model variables------------------------------'

    PRINT *,'------------------------------------------------------------------------------'
    PRINT *,'MINMAX u(:,n_var_Ilm) ', MINVAL( u(:,n_var_Ilm) ), MAXVAL( u(:,n_var_Ilm) )
    PRINT *,'u(:,n_var_Ilm) ',u(1:10,n_var_Ilm)
    PRINT *,'------------------------------------------------------------------------------'; PRINT *,''


    PRINT *,'------------------------------------------------------------------------------'
    PRINT *,'MINMAX u(:,n_var_Imm) ', MINVAL( u(:,n_var_Imm) ), MAXVAL( u(:,n_var_Imm) )
    PRINT *,'u(:,n_var_Imm) ',u(1:10,n_var_Imm)
    PRINT *,'------------------------------------------------------------------------------'; PRINT *,''


    PRINT *,'(1.0_pr/(LDMtheta*delta(:)**(1.0_pr/4.0_pr))) ', MINVAL((1.0_pr/(LDMtheta*delta(:)**(1.0_pr/4.0_pr)))),&
         MAXVAL((1.0_pr/(LDMtheta*delta(:)**(1.0_pr/4.0_pr))))
    PRINT *,'-------------------------------------------------------------------------------'; PRINT *,''

    PRINT *,'MINMAX (MAX(u(:,n_var_Ilm)*u(:,n_var_Imm),0.0_pr))**(0.125_pr) ', &
         MINVAL((MAX(u(:,n_var_Ilm)*u(:,n_var_Imm),0.0_pr))**(0.125_pr) ), &
         MAXVAL((MAX(u(:,n_var_Ilm)*u(:,n_var_Imm),0.0_pr))**(0.125_pr) )
    PRINT *,'(MAX(u(1:10,n_var_Ilm)*u(1:10,n_var_Imm),0.0_pr))**(0.125_pr) ', &
         (MAX(u(1:10,n_var_Ilm)*u(1:10,n_var_Imm),0.0_pr))**(0.125_pr)
    PRINT *,'-------------------------------------------------------------------------------'; PRINT *,''


    PRINT *,'MINMAX (SSijgeps(:,1) -u(:,n_var_Ilm)) ', &
         MINVAL( SSijgeps(:,1) -u(:,n_var_Ilm) ), &
         MAXVAL( SSijgeps(:,1) -u(:,n_var_Ilm) )
    PRINT *,'(LijMij(1:10,1)-u(1:10,n_var_Ilm)) ', (SSijgeps(1:10,1)-u(1:10,n_var_Ilm))
    PRINT *,'-------------------------------------------------------------------------------'; PRINT *,''

    PRINT *,'MINMAX (u(:,n_var_Imm)*u(:,n_var_Imm))**(0.125_pr) ', &
         MINVAL((u(:,n_var_Imm)*u(:,n_var_Imm))**(0.125_pr) ), &
         MAXVAL((u(:,n_var_Imm)*u(:,n_var_Imm))**(0.125_pr) )
    PRINT *,'-------------------------------------------------------------------------------'; PRINT *,''

    PRINT *,'MINMAX (SSijgeps(:,2)-u(:,n_var_Imm)) ', &
         MINVAL((SSijgeps(:,2)-u(:,n_var_Imm))), &
         MAXVAL((SSijgeps(:,2)-u(:,n_var_Imm)))
    PRINT *,'-------------------------------------------------------------------------------'; PRINT *,''

    !TEST Use old dynamic model for evolution
    ! Cs =  0.5* SUM(dA*SSijgeps(:,1) ) / SUM(dA*SSijgeps(:,2))
    !  PRINT *,'TEST override:Dynamic Smagorinski Coeff = ', Cs
    !  nu_t(:) =  Cs * Smod(:)

  END SUBROUTINE smag_dyn_Meneveau
END MODULE user_case





