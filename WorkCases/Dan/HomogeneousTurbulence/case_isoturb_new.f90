MODULE user_case
  !
  ! Case isotropic decaying turbulence
  ! Dan Goldstein 11/30/2004
  ! Updated for use with Crank Nicholson time integration format 3/10/2005, Dan Goldstein

  USE precision
  USE elliptic_vars
  USE elliptic_mod
  USE field
  USE input_file_reader
  USE io_3D_vars
  USE pde
  USE share_consts
  USE share_kry
  USE sizes
  USE util_mod
  USE util_vars
  USE vector_util_mod
  USE wavelet_filters_mod
  USE wlt_vars
  USE wlt_trns_vars
  USE wlt_trns_mod
  USE wlt_trns_util_mod
  USE fft_module
  USE spectra_module
  USE SGS_incompressible

  !
  ! case specific variables
  !
  INTEGER n_var_vorticity ! start of vorticity in u array (Must exist)
  INTEGER n_var_pressure  ! start of pressure in u array  (Must exist)

  INTEGER n_var_modvort   ! location of modulus of vorticity
  INTEGER n_var_modSij    ! location of modulus of Sij
  INTEGER n_var_modVel     ! location of velocity magnitude

  LOGICAL   :: adaptMagVort, adaptNormS, adaptMagVel

CONTAINS

  !
  ! In user_setup_pde() we setup how many variables are integrated, which are interpolated
  ! to the next times step and if any exeact solution exists to compare to during the run.
  ! We also set the variable names that are used when the result files are written.
  !
  ! The following variables must be setup in this routine:
  !
  !
  ! n_integrated     ! first n_integrated eqns will be acted on for time integration
  ! n_var_additional ! 
  ! n_var
  !
  !
  !
  !
  SUBROUTINE  user_setup_pde ( VERB ) 
    IMPLICIT NONE
    LOGICAL, OPTIONAL :: VERB         ! print debug info
    INTEGER :: i

    PRINT * ,''
    PRINT *, '**********************Setting up PDE*****************'
    PRINT * ,'CASE ISOTURB '
    PRINT *, '*****************************************************'


    !------------ setting up default values 

    n_integrated = dim ! uvw - # of equations to solve without SGS model
 
    n_integrated = n_integrated + n_var_SGS ! adds additional equations for SGS model
    
    n_time_levels = 1  !--# time levels

    n_var_time_levels = n_time_levels * n_integrated !--Number of equations (2D velocity at two time levels)

    n_var_additional = 1 !--1 pressure at one time level 

    n_var_exact = 0 !--No exact solution 

    n_var_pressure  = n_integrated + 1 !pressure

    IF( adaptMagVort ) THEN
       n_var_additional = n_var_additional +1
       n_var_modvort    = n_integrated + n_var_additional ! (wi.wi)^0.5vorticity magnitude
    END IF
    IF( adaptNormS ) THEN
       n_var_additional = n_var_additional +1
       n_var_modSij     = n_integrated + n_var_additional ! (SijSij)^0.5
    END IF
    IF( adaptMagVel ) THEN
       n_var_additional = n_var_additional +1
       n_var_modVel     = n_integrated + n_var_additional ! (UiUi)^0.5
    END IF

    n_var = n_var_time_levels + n_var_additional !--Total number of variables

    !
    !--Allocate logical arrays 
    !  Must be done after setting number of different types of variables
    !  and before setting logical variable mapping arrays
    !
    CALL alloc_variable_mappings
    !
    ! Fill in variable names for default variables for integration meth 2
    ! These may be changed in the call to the user specified routine
    !  user_setup_pde()
    !
    ! In integrated variables (3D)

                   WRITE (u_variable_names(1), u_variable_names_fmt) 'Velocity_u_@t  '
    IF( dim >= 2 ) WRITE (u_variable_names(2), u_variable_names_fmt) 'Velocity_v_@t  '
    IF( dim >= 3 ) WRITE (u_variable_names(3), u_variable_names_fmt) 'Velocity_w_@t  '
    WRITE (u_variable_names(n_var_pressure), u_variable_names_fmt) 'Pressure  '
    IF( adaptMagVort ) WRITE (u_variable_names(n_var_modvort), u_variable_names_fmt) '|Wij|  '
    IF( adaptNormS )   WRITE (u_variable_names(n_var_modSij), u_variable_names_fmt)  '|Sij|  '
    !
    ! setup logical variable mapping arrays
    ! This defined which variables are used for adaptation, Which need to be interpololated to the new grid at 
    ! each time step, which are saved, and for which we have an exect solutiuon we want to check at each time step
    !


    !
    ! setup which components we will base grid adaptation on.
    ! Adapt on the 3 velocity components
    !
    n_var_adapt(1:dim,0) = .TRUE. !--Initially adapt on integrated variables at first time level
    !eventually adapt to mdl variables    n_var_adapt(1:dim+2,1) = .TRUE. !--After first time step adapt on  velocity and Ilm and Imm

    n_var_adapt(1:dim,1) = .TRUE. !--After first time step adapt on  velocity
	 
    IF( adaptMagVort ) n_var_adapt(n_var_modvort,1)  = .TRUE.
    IF( adaptNormS )   n_var_adapt(n_var_modSij,1 )  = .TRUE.
    

    !Variables that need to be interpoleted to new adapted grid in initial grid adaptation
    n_var_interpolate(1:n_integrated,0) = .TRUE. 

    !Variables that need to be interpoleted to new adapted grid at each time step
    n_var_interpolate(1:n_var_pressure,1) = .TRUE. 

    !
    ! setup which components we have an exact solution for

    n_var_exact_soln(:,0:1) = .FALSE.

    !
    ! variables required for restart
    !
    n_var_req_restart = .FALSE.
    n_var_req_restart(1:dim)	        = .TRUE. !restart with velocities and pressure to begin with!
    n_var_req_restart(n_var_pressure)	= .TRUE. !

    ! no pressure for restart from initial file
    !n_var_req_restart(n_var_pressure ) = .TRUE.

    !
    ! setup which variables we will save the solution
    !
    n_var_save(1:n_var_pressure) = .TRUE. ! save all for restarting code

    !
    !--Time level counters for NS integration
    !   Used for integration meth2
    ! n0 is current time step
    ! n1 is t_n-1 time step
    ! n2 is t_n-2 time step
    !
    n0 = 1; n1 = n0+dim; n2 = n1+dim


    !
    ! Set the maximum number of components for which we have an exact solution
    !
    n_var_exact = MAX( COUNT(n_var_exact_soln(:,0)),COUNT(n_var_exact_soln(:,1)) )


    !
	! Setup a scaleCoeff array of we want to tweak the scaling of a variable
	! ( if scaleCoeff < 1.0 then more points will be added to grid 
	!
    ALLOCATE ( scaleCoeff(1:n_var), Umn(1:n_var) )
    scaleCoeff = 1.0_pr

    Umn = 0.0_pr !set up here if mean quantities anre not zero and used in scales or equation

    PRINT *, 'n_integrated = ',n_integrated 
    PRINT *, 'n_time_levels = ',n_time_levels
    PRINT *, 'n_var_time_levels = ',n_var_time_levels 
    PRINT *, 'n_var = ',n_var 
    PRINT *, 'n_var_exact = ',n_var_exact 
    PRINT *, '*******************Variable Names*******************'
    DO i = 1,n_var
       WRITE (*, u_variable_names_fmt) u_variable_names(i)
    END DO
    PRINT *, '****************************************************'



  END SUBROUTINE  user_setup_pde

  !
  ! read variables from input file for this user case 
  !
  SUBROUTINE  user_read_input()
    IMPLICIT NONE

  call input_real ('nu',nu,'stop', &
       ' nu: viscosity')

   call input_logical ('ExplicitFilter', ExplicitFilter, 'stop', &
       ' ExplicitFilter !apply grid filter as an explicit filter each time step')

   call input_logical ('ExplicitFilterNL', ExplicitFilterNL, 'stop', &
       ' ExplicitFilterNL !apply grid filter as an explicit filter to the non-linear term')

   call input_logical ('adaptMagVort',adaptMagVort,'stop', &
       '  adaptMagVort, Adapt on the magnitude of vorticity')

   call input_logical ('adaptNormS',adaptNormS,'stop', &
       '  adaptNormS, Adapt on the L2 of Sij')

   call input_logical ('adaptMagVel',adaptMagVel,'stop', &
       '  adaptMagVel, Adapt on the magnitude of velocity')

  END SUBROUTINE  user_read_input

  !
  ! Set the exact solution for comparison to the simulated solution
  !
  ! u          - array to fill in the exact solution
  ! nlocal       - number of active wavelets   
  ! ne_local        - total number of equations
  ! t          - time of current time step 
  ! l_n_var_exact_soln_index - index into the elements of u for which we need to 
  !                            find the exact solution
  SUBROUTINE  user_exact_soln (u, nlocal,  t_local, l_n_var_exact_soln)  
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: nlocal
    REAL (pr), INTENT (IN) ::  t_local
    REAL (pr), DIMENSION (nlocal,n_var_exact), INTENT (INOUT) :: u
    LOGICAL , INTENT (IN) :: l_n_var_exact_soln(n_var)
    INTEGER :: i


    ! There is no exact solution



  END SUBROUTINE  user_exact_soln

  SUBROUTINE user_initial_conditions (u, nlocal, ne_local, t_local, scl, scl_fltwt, iter)
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: nlocal, ne_local
    INTEGER  , INTENT (INOUT) :: iter ! iteration of call while adapting initial grid
    REAL (pr), DIMENSION (nlocal,ne_local), INTENT (INOUT) :: u
    REAL (pr)  :: scl(1:n_var),scl_fltwt
    REAL (pr), INTENT (IN) :: t_local

    INTEGER :: i !TEST


    !
    ! Initial conditions read from DNS data file whos name is supplied in the input file
    ! 
    !u = 0.0_pr !TESTING
    ! u(1,:) = 1

  END SUBROUTINE user_initial_conditions

  ! Arguments
  ! u         - field on adaptive grid
  ! nlocal      - number of active points
  ! ne_local       - number of equations
  ! t         - current time
  !
  ! Global variables used
  ! ifn       - number of equations
  ! x()
  ! xO
  ! y0
  !--******************************** 
  SUBROUTINE user_algebraic_BC (Lu, u, nlocal, ne_local, jlev, meth)
    !--Defines boundary condition type
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: jlev, meth, ne_local, nlocal
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (INOUT) :: Lu
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (IN)    :: u

    INTEGER :: ie, ii, shift

    !
    ! There are periodic BC conditions
    !

    !default SGS BC, can be explicitely deifined below instead of calling default BCs
    CALL SGS_algebraic_BC (Lu, u, nlocal, ne_local, jlev, meth) 

  END SUBROUTINE user_algebraic_BC

  SUBROUTINE user_algebraic_BC_diag (Lu_diag, nlocal, ne_local, jlev, meth)
    !--Defines boundary condition type
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: jlev, meth, ne_local, nlocal
    REAL (pr), DIMENSION (nlocal,ne_local), INTENT (INOUT) :: Lu_diag

    INTEGER :: ie, ii, shift

    !
    ! There are periodic BC conditions
    !
    
    !default diagonal terms of SGS BC, can be explicitely deifined below instead of calling default BCs
    IF(sgsmodel /=0 ) CALL SGS_algebraic_BC_diag (Lu_diag, nlocal, ne_local, jlev, meth)

  END SUBROUTINE user_algebraic_BC_diag

  SUBROUTINE user_algebraic_BC_rhs (rhs, ne_local, nlocal, jlev)
    !--Sets rhs for boundary conditions
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: ne_local, nlocal, jlev
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (INOUT) :: rhs

    INTEGER :: ie, ii, shift

    !
    ! There are periodic BC conditions
    !

    !default  SGS BC, can be explicitely deifined below instead of calling default BCs
    IF(sgsmodel /= 0) CALL SGS_algebraic_BC_rhs (rhs, ne_local, nlocal, jlev)

  END SUBROUTINE user_algebraic_BC_rhs

  SUBROUTINE user_project (u, p, nlocal, meth)
    !--Makes u divergence free
    IMPLICIT NONE
    INTEGER, PARAMETER :: ne_local = 1
    INTEGER, INTENT (IN) :: meth, nlocal
    REAL (pr), DIMENSION (nlocal),     INTENT(INOUT) :: p
    REAL (pr), DIMENSION (nlocal,n_integrated), INTENT(INOUT) :: u
    REAL (pr), DIMENSION (nlocal) :: dp
    REAL (pr), DIMENSION (nlocal) :: f
    REAL (pr), DIMENSION(ne_local) :: scl_p
    INTEGER, DIMENSION(ne_local) :: clip 

    !--Make u  divergence free
    dp = 0.0_pr
    f = div(u(:,1:dim),nlocal,j_lev,meth+4)
    clip = 1

    scl_p = MAXVAL(scl_global(1:dim)) !scale of pressure increment based on dynamic pressure
    PRINT *, 'scl_p=',scl_p
    CALL Linsolve (dp, f , tol2, nlocal, ne_local, clip, Laplace, Laplace_diag, SCL=scl_p) !new with scaling 
!    CALL Linsolve (dp, f , tol2, nlocal, ne_local, clip, Laplace, Laplace_diag)  !old without scaling
    u(:,1:dim) = u(:,1:dim) - grad(dp, nlocal, j_lev, meth+2)
    p = p + dp/dt

  END SUBROUTINE user_project

  FUNCTION Laplace (jlev, u, nlocal, ne_local, meth_in)
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: jlev, nlocal, ne_local, meth_in
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (INOUT) :: u
    REAL (pr), DIMENSION (nlocal*ne_local) :: Laplace

    INTEGER :: i, ii, ie, meth, meth1, meth2, idim, shift
    REAL (pr), DIMENSION (ne_local ,nlocal,dim) :: du
    REAL (pr), DIMENSION (ne_local*dim ,nlocal,dim) :: d2u
    REAL (pr), DIMENSION (ne_local*dim ,nlocal,dim) :: du_dummy ! passed when only calculating 1st derivative.
    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc
    LOGICAL :: hold_debug_zero_db ! debug only

    meth1 = meth_in + 2
    meth2 = meth_in + 4

    !
    ! Find 1st deriviative of u. 
    !
	IF( debug_c_diff_fast>=1) WRITE(*,'(A)') "Call 1 c_diff_fast() from Laplace()"
!	IF( nlocal==184850 .AND. MAXVAL(ABS(u(1:nlocal*ne_local))) > 0.0 ) debug_c_diff_fast=4
    CALL c_diff_fast (u, du, d2u(1:ne_local, 1:nlocal, 1:dim), jlev, nlocal, meth1, 10, ne_local, 1, ne_local)
!    IF( nlocal==184850 .AND. MAXVAL(ABS(u(1:nlocal*ne_local))) > 0.0) stop

    !
    ! Find 2nd deriviative of u.  d( du ) 
    !
   !
    ! Find 2nd deriviative of u.  d( du ) 
    !
	IF( debug_c_diff_fast>=1) WRITE(*,'(A)') "Call 2 c_diff_fast() from Laplace()"
    IF ( TYPE_DB == DB_TYPE_WRK ) THEN
       DO ie = 1, ne_local
          CALL c_diff_fast(du(ie,1:nlocal,1:dim), &
               d2u((ie-1)*dim+1:ie*dim,1:nlocal,1:dim), du_dummy((ie-1)*dim+1:ie*dim,1:nlocal,1:dim), &
               jlev, nlocal, meth2, 10, dim, 1, dim )
       END DO
    ELSE !db
  
	   hold_debug_zero_db=debug_zero_db  !debug

       ! Load du  into db
       DO ie=1,ne_local		
          CALL update_db_from_u(  du(ie,1:nlocal,1:dim)  , nlocal ,dim  , 1, dim, 1+(ne_local-1)*dim  ) !Load du	    
	      debug_zero_db = .FALSE. ! db was already zeroed out above.. Cant do it again or we loose values	  
       END DO
       debug_zero_db=hold_debug_zero_db !debug

       ! find  2nd deriviative of u (so we first derivative of du/dx)
       CALL c_diff_fast_db(d2u, du_dummy, jlev, nlocal, meth2, 10, ne_local*dim,&
            1,         &  ! MIN(mn_varD,mn_varD2)
            ne_local*dim,    &  ! MAX(mx_varD,mx_varD2)
            1,         &  ! mn_varD  :   min variable in db that we are doing the 1st derivatives for.
            ne_local*dim,    &  ! mn_varD  :   max variable in db that we are doing the 1st derivatives for.
            0 ,        &  ! mn_varD2 :   min variable in db that we are doing the 2nd derivatives for.
            0     )  ! mn_varD2 :   max variable in db that we are doing the 2nd derivatives for.

!-----------------
		IF( debug_c_diff_fast>=1) THEN
		   PRINT *,'c_diff_fastu() enter' !debug
		   DO i = 1,ne_local
		      DO idim = 1,dim
			    WRITE(*,'( "minmax u(:,",i2.2,") ", 2(G20.12,1X) )') &
				    idim*i, MINVAL(du(i,1:nlocal,idim)),MAXVAL(du(i,1:nlocal,idim)) !debug 
		      END DO 
		   END DO
		   PRINT *,'c_diff_fastu()' !debug
		   DO i = 1,ne_local*dim
			 DO idim = 1,dim
			   WRITE(*,'( "minmax du(",i2.2,":,",i2.2,") ", 2(G20.12,1X) )') &
  				  i,idim,MINVAL(d2u(i,:,idim)),MAXVAL(d2u(i,:,idim)) !debug 
  			 ENDDO
		   END DO

		END IF

!----------------
    END IF
    ! Now:
    ! d2u(1,:,1) = d^2 U/ dx dx
    ! d2u(1,:,2) = d^2 U/ dy dx
    ! d2u(1,:,3) = d^2 U/ dz dx
    ! d2u(2,:,1) = d^2 V/ dx dy
    ! d2u(2,:,2) = d^2 V/ dy dy
    ! d2u(2,:,3) = d^2 V/ dz dy
    ! d2u(3,:,1) = d^2 W/ dx dz
    ! d2u(3,:,2) = d^2 W/ dy dz
    ! d2u(3,:,3) = d^2 W/ dz dz

    DO ie = 1, ne_local
       shift=(ie-1)*nlocal
       !--- Internal points
       !--- div(grad)
       idim = 1
       Laplace(shift+1:shift+Nwlt_lev(jlev,0)) = d2u( (ie-1)*dim+idim ,1:Nwlt_lev(jlev,0),1)
       DO idim = 2,dim
          Laplace(shift+1:shift+Nwlt_lev(jlev,0)) = Laplace(shift+1:shift+Nwlt_lev(jlev,0)) + &
               d2u((ie-1)*dim+idim ,1:Nwlt_lev(jlev,0),idim)
       END DO
       !--Boundary points
       i_p_face(0) = 1
       DO i=1,dim
          i_p_face(i) = i_p_face(i-1)*3
       END DO
       DO face_type = 0, 3**dim - 1
          face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
          IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
             CALL get_all_indices_by_face (face_type, jlev, nloc, iloc)
             IF(nloc > 0 .AND. dim == 3) THEN ! 3-D case only
                IF( ALL( face == (/-1,0,0/) ) ) THEN  ! Xmin face, no edges or corners
                   Laplace(shift+iloc(1:nloc)) = u(shift+iloc(1:nloc))  !Dirichlet conditions
                ELSE IF( ALL( face == (/1,0,0/) ) ) THEN  ! Xmax face, no edges or corners
                   Laplace(shift+iloc(1:nloc)) = u(shift+iloc(1:nloc))  !Dirichlet conditions
!!$                Laplace(shift+iloc(1:nloc)) = du(ie,iloc(1:nloc),1)  !Neuman conditions
                ELSE IF( ALL( face(2:dim) == (/-1,0/) ) ) THEN  ! Ymin face, edges || to Z, no corners
                   Laplace(shift+iloc(1:nloc)) = u(shift+iloc(1:nloc))  !Dirichlet conditions
!!$                Laplace(shift+iloc(1:nloc)) = du(ie,iloc(1:nloc),2)  !Neuman conditions
                ELSE IF( ALL( face(2:dim) == (/1,0/) ) ) THEN  ! Ymax face, edges || to Z, no corners
                   Laplace(shift+iloc(1:nloc)) = u(shift+iloc(1:nloc))  !Dirichlet conditions
!!$                Laplace(shift+iloc(1:nloc)) = du(ie,iloc(1:nloc),2)  !Neuman conditions
                ELSE IF( face(3) == -1 ) THEN  ! entire Zmin face
                   Laplace(shift+iloc(1:nloc)) = u(shift+iloc(1:nloc))  !Dirichlet conditions
!!$                Laplace(shift+iloc(1:nloc)) = du(ie,iloc(1:nloc),3)  !Neuman conditions
                ELSE IF( face(3) == 1 ) THEN  ! entire Zmax face
                   Laplace(shift+iloc(1:nloc)) = u(shift+iloc(1:nloc))  !Dirichlet conditions
!!$                Laplace(shift+iloc(1:nloc)) = du(ie,iloc(1:nloc),3)  !Neuman conditions
                END IF
             END IF
          END IF
       END DO
    END DO
  END FUNCTION Laplace

  FUNCTION Laplace_diag (jlev,  nlocal, ne_local, meth_in)
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: jlev, nlocal, ne_local, meth_in
    REAL (pr), DIMENSION (nlocal*ne_local) :: Laplace_diag

    INTEGER :: i, ii, ie, shift, meth1, meth2
    REAL (pr), DIMENSION (nlocal,dim) :: du, d2u
    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc

    meth1 = meth_in + 2
    meth2 = meth_in + 4


    !PRINT *,'IN Laplace_diag, ne_local = ', ne_local

    DO ie = 1, ne_local
       shift=(ie-1)*nlocal
       !--- div(grad)
       !PRINT *,'CAlling c_diff_diag from Laplace_diag() '
       !PRINT *,'--- jlev, nlocal, meth1, meth2', jlev, nlocal, meth1, meth2

       CALL c_diff_diag ( du, d2u, jlev, nlocal, meth1, meth2, -11)

       Laplace_diag(shift+1:shift+Nwlt_lev(jlev,0)) = d2u(1:Nwlt_lev(jlev,0),1) + d2u(1:Nwlt_lev(jlev,0),2)
       IF (dim==3) Laplace_diag(shift+1:shift+Nwlt_lev(jlev,0)) = &
            Laplace_diag(shift+1:shift+Nwlt_lev(jlev,0))+d2u(1:Nwlt_lev(jlev,0),3)
       !--Boundary points
       i_p_face(0) = 1
       DO i=1,dim
          i_p_face(i) = i_p_face(i-1)*3
       END DO
       DO face_type = 0, 3**dim - 1
          face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
          IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
             CALL get_all_indices_by_face (face_type, jlev, nloc, iloc)
             IF(nloc > 0 .AND. dim == 3) THEN ! 3-D case only
                IF( ALL( face == (/-1,0,0/) ) ) THEN                         ! Xmin face, no edges or corners
                   Laplace_diag(shift+iloc(1:nloc)) = 1.0_pr                 !Dirichlet conditions
                ELSE IF( ALL( face == (/1,0,0/) ) ) THEN                     ! Xmax face, no edges or corners
                   Laplace_diag(shift+iloc(1:nloc)) = 1.0_pr                 !Dirichlet conditions
!!$                Laplace_diag(shift+iloc(1:nloc)) = du(iloc(1:nloc),1)     !Neuman conditions
                ELSE IF( ALL( face(2:dim) == (/-1,0/) ) ) THEN               ! Ymin face, edges || to Z, no corners
                   Laplace_diag(shift+iloc(1:nloc)) = 1.0_pr                 !Dirichlet conditions
!!$                Laplace_diag(shift+iloc(1:nloc)) = du(iloc(1:nloc),2)     !Neuman conditions
                ELSE IF( ALL( face(2:dim) == (/1,0/) ) ) THEN                ! Ymax face, edges || to Z, no corners
                   Laplace_diag(shift+iloc(1:nloc)) = 1.0_pr                 !Dirichlet conditions
!!$                Laplace_diag(shift+iloc(1:nloc)) = du(iloc(1:nloc),2)     !Neuman conditions
                ELSE IF( face(3) == -1 ) THEN                                ! entire Zmin face
                   Laplace_diag(shift+iloc(1:nloc)) = 1.0_pr                 !Dirichlet conditions
!!$                Laplace_diag(shift+iloc(1:nloc)) = du(iloc(1:nloc),3)     !Neuman conditions
                ELSE IF( face(3) == 1 ) THEN                                 ! entire Zmax face
                   Laplace_diag(shift+iloc(1:nloc)) = 1.0_pr                 !Dirichlet conditions
!!$                Laplace_diag(shift+iloc(1:nloc)) = du(iloc(1:nloc),3)     !Neuman conditions
                END IF
             END IF
          END IF
       END DO
    END DO
  END FUNCTION Laplace_diag



  FUNCTION user_rhs (u_integrated,p)
    IMPLICIT NONE
    REAL (pr), DIMENSION (ng,ne), INTENT(IN) :: u_integrated !1D flat version of integrated variables without BC 
    REAL (pr), DIMENSION (ng), INTENT(IN) :: p
    REAL (pr), DIMENSION (n) :: user_rhs

    INTEGER :: ie, shift, i
    INTEGER, PARAMETER :: meth=1
    REAL (pr), DIMENSION (ne,ng,dim) :: du, d2u
    REAL (pr), DIMENSION (ng,dim)     :: dp

    !PRINT *,'user_rhs MINMAX(p) ', MINVAL( p), MAXVAL(dp)
     dp(1:ng,1:dim) = grad (p, ng, j_lev, meth+2)

    !PRINT *,'user_rhs MINMAX(dp) ', MINVAL( dp), MAXVAL(dp)
    !PRINT *,'user_rhs MINMAX(u_integrated) ', MINVAL( u_integrated), MAXVAL(u_integrated)

	IF( debug_c_diff_fast>=1) WRITE(*,'(A)') "Call 1 c_diff_fast() from user_rhs()"
    CALL c_diff_fast(u_integrated, du, d2u, j_lev, ng, 1, 11, ne, 1, ne) !derivatives are calculated even for SGS terms

    !PRINT *,'user_rhs MINMAX(du) ', MINVAL( du), MAXVAL(du)

    !--Form right hand side of Navier-Stokes equations
    IF (dim==2) THEN
       PRINT *,'Dim=2 not yet supported in this case...Exiting...'
       STOP
       DO ie = 1, 2
          shift=(ie-1)*ng
          !CALL c_diff_fast(u_integrated(shift+1:shift+ng), du, d2u, j_lev, ng, 1, 11, 1, 1, 1)
          user_rhs(shift+1:shift+ng) = - (u_integrated(:,1)+Umn(1))*du(ie,:,1) - &
               (u_integrated(:,2)+Umn(2))*du(ie,:,2) + nu*SUM(d2u(ie,:,:),2) - dp(:,ie)
       END DO
    ELSE IF (dim==3) THEN
       DO ie = 1, 3
          shift=(ie-1)*ng
          !CALL c_diff_fast(u_integrated(shift+1:shift+ng), du, d2u, j_lev, nwlt, 1, 11, 1, 1, 1)
          user_rhs(shift+1:shift+ng) = - (u_integrated(:,1)+Umn(1))*du(ie,:,1) - (u_integrated(:,2)+Umn(2))*du(ie,:,2) - &
               (u_integrated(:,3)+Umn(3))*du(ie,:,3) &
               + nu*SUM(d2u(ie,:,:),2) - dp(:,ie)

          !PRINT *,'user_rhs ie MINMAX(user_rhs(shift+1:shift+ng))', ie, &
          !  MINVAL( user_rhs(shift+1:shift+ng)), MAXVAL(user_rhs(shift+1:shift+ng))

       END DO

       IF(sgsmodel /= 0) CALL SGS_rhs (user_rhs, u_integrated, du, d2u)

    END IF


    !--Set operator on boundaries
    IF(SUM(nbnd) /= 0) CALL user_algebraic_BC_rhs (user_rhs, ne, ng, j_lev)
  
END FUNCTION user_rhs


  ! find Jacobian of Right Hand Side of the problem
  ! u_prev == u_prev_timestep_loc

  FUNCTION user_Drhs (u, u_prev, meth)
    IMPLICIT NONE
    INTEGER, INTENT(IN) ::  meth
    !u_prev_timestep passed local to cast it into 2dim array to work with vector derivatives
    REAL (pr), DIMENSION (ng,ne) :: u, u_prev
    REAL (pr), DIMENSION (n) :: user_Drhs

    INTEGER :: ie, shift, i, idim
    REAL (pr), DIMENSION (2*ne,ng,dim) :: du  ! 1st derivatives for u (in du(1:ne,:,:)) and u_prev_timestep (in du(ne+1:2*ne,:,:))
    REAL (pr), DIMENSION (ne  ,ng,dim) :: d2u ! 2nd derivatives for u 
    REAL (pr), DIMENSION (ne  ,ng,dim) :: du_dummy ! passed when only calculating 1st derivative.
    !Find better way to do this!! du_dummy with no storage..

    LOGICAL :: hold_debug_zero_db ! debug only

    IF ( TYPE_DB == DB_TYPE_WRK )THEN 
       ! find 1st and 2nd deriviative of u and
	   IF( debug_c_diff_fast>=1) WRITE(*,'(A)') "Call 1 c_diff_fast() from user_Drhs()"
       CALL c_diff_fast(u, du(1:ne,:,:), d2u(1:ne,:,:), j_lev, ng, meth, 11, ne , 1, ne )

       ! find only first deriviativ e  u_prev_timestep
	   IF( debug_c_diff_fast>=1) WRITE(*,'(A)') "Call 2 c_diff_fast() from user_Drhs()"
       CALL c_diff_fast(u_prev, du(ne+1:2*ne,:,:), du_dummy(1:ne,:,:), j_lev, ng, meth, 10, ne , 1, ne )

    ELSE !db

       ! Load u and u_prev_timestep in to db
       ! update the db from u
       ! u(:, mn_var:mx_var) -> db%u(db_offset:mx_var-mn_var+1)
       ! db_offset = 1 
       CALL update_db_from_u(  u       , ng ,ne  , 1, ne, 1  ) !Load u
	   hold_debug_zero_db=debug_zero_db  !debug
	   debug_zero_db = .FALSE. ! db was already zeroed out above.. Cant do it again or we loose values	  
  
       CALL update_db_from_u(  u_prev , ng ,ne  , 1, ne, ne+1  ) !Load u_prev offset in db by ne+1
       debug_zero_db=hold_debug_zero_db !debug

       ! find 1st and 2nd deriviative of u and
       ! find only first deriviativ e  u_prev_timestep
	   IF( debug_c_diff_fast>=1) WRITE(*,'(A)') "Call 1&2 c_diff_fast() from user_Drhs()"
       CALL c_diff_fast_db(du, d2u, j_lev, ng, meth, 11, 2*ne,&
            1,   &  ! MIN(mn_varD,mn_varD2)
            2*ne,&  ! MAX(mx_varD,mx_varD2)
            1,   &  ! mn_varD  :   min variable in db that we are doing the 1st derivatives for.
            2*ne,&  ! mn_varD  :   max variable in db that we are doing the 1st derivatives for.
            1 ,  &  ! mn_varD2 :   min variable in db that we are doing the 2nd derivatives for.
            dim   )  ! mn_varD2 :   max variable in db that we are doing the 2nd derivatives for.

!-----------------
		IF( debug_c_diff_fast>=1) THEN
		   PRINT *,'c_diff_fastu() enter' !debug
		   DO i = 1,ne
			 WRITE(*,'( "minmax u(:,",i2.2,") ", 2(G20.12,1X) )') &
				 i, MINVAL(u(:,i)),MAXVAL(u(:,i)) !debug 
		   END DO 
		   PRINT *,'c_diff_fastu()' !debug
		   DO i = 1,ne
			 DO idim = 1,dim
			   WRITE(*,'( "minmax du(",i2.2,":,",i2.2,") ", 2(G20.12,1X) )') &
  				  i,idim,MINVAL(du(i,:,idim)),MAXVAL(du(i,:,idim)) !debug 
  			 ENDDO
		   END DO

	   		 DO i = 1,ne
			   DO idim = 1,dim
				 WRITE(*,'( "minmax d2u(",i2.2,":,",i2.2,") ", 2(G20.12,1X) )') &
  					  i,idim,MINVAL(d2u(i,:,idim)),MAXVAL(d2u(i,:,idim)) !debug 
  			   ENDDO
			 END DO


		   PRINT *,'c_diff_fastu() enter' !debug
		   DO i = 1,ne
			 WRITE(*,'( "minmax u(:,",i2.2,") ", 2(G20.12,1X) )') &
				 i, MINVAL(u_prev(:,i)),MAXVAL(u_prev(:,i)) !debug 
		   END DO 
		   PRINT *,'c_diff_fastu()' !debug
		   DO i = ne+1,2*ne
			 DO idim = 1,dim
			   WRITE(*,'( "minmax du(",i2.2,":,",i2.2,") ", 2(G20.12,1X) )') &
  				  i-ne,idim,MINVAL(du(i,:,idim)),MAXVAL(du(i,:,idim)) !debug 
  			 ENDDO
		   END DO
		END IF

!----------------
    END IF
    !CALL c_diff_fast_db(du_b, du_b, j_lev, ng, meth, 10, ne, 1, ne) !find 1st derivative u_b


    !--Form right hand side of Navier--Stokes equations
    IF (dim==2) THEN
       DO ie = 1, 2
          shift=(ie-1)*ng

          !CALL c_diff_fast(u(shift+1:shift+ng), du, d2u, j_lev, ng, meth, 11, 1, 1, 1)
          user_Drhs(shift+1:shift+ng) = - (u_prev(:,1)+Umn(1))*du(ie,:,1) - &
               (u_prev(:,2)+Umn(2))*du(ie,:,2) + &
               nu*SUM(d2u(ie,:,:),2)

          !CALL c_diff_fast(u_prev_timestep(shift+1:shift+ng), du, d2u, j_lev, ng, meth, 10, 1, 1, 1)
          user_Drhs(shift+1:shift+ng) = user_Drhs(shift+1:shift+ng) - u(:,1)*du(ne+ie,:,1) - u(:,2)*du(ne+ie,:,2) 
       END DO
    ELSE IF (dim==3) THEN
       DO ie = 1, 3
          shift=(ie-1)*ng

          !CALL c_diff_fast(u(shift+1:shift+ng), du, d2u, j_lev, ng, meth, 11, 1, 1, 1)
          user_Drhs(shift+1:shift+ng) = - (u_prev(:,1)+Umn(1))*du(ie,:,1) - &
               (u_prev(:,2)+Umn(2))*du(ie,:,2) &
               - (u_prev(:,3)+Umn(3))*du(ie,:,3) + nu*SUM(d2u(ie,:,:),2)

          !CALL c_diff_fast(u_prev_timestep(shift+1:shift+ng), du, d2u, j_lev, ng, meth, 10, 1, 1, 1)
          user_Drhs(shift+1:shift+ng) = user_Drhs(shift+1:shift+ng) - u(:,1)*du(ne+ie,:,1) - u(:,2)*du(ne+ie,:,2) &
               - u(:,3)*du(ne+ie,:,3)
       END DO
       
       IF(sgsmodel /= 0) CALL SGS_Drhs (user_Drhs, u, u_prev, du, d2u, meth)

    END IF
  END FUNCTION user_Drhs


  !
  ! Uses u_prev_timestep, which is global flat (1D ) array set in time_step_cn()
  !
  FUNCTION user_Drhs_diag (meth)
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: meth
    REAL (pr), DIMENSION (n) :: user_Drhs_diag

    INTEGER :: ie, shift,shiftIlm,shiftImm
    REAL (pr), DIMENSION (ng,dim) :: du, d2u
    REAL (pr), DIMENSION (ne,ng,dim) :: du_prev_timestep
    REAL (pr), DIMENSION (ne  ,ng,dim) :: du_dummy ! passed when only calculating 1st derivative.

	IF( debug_c_diff_fast>=1) WRITE(*,'(A)') "Call 1 c_diff_fast() from user_Drhs_diag()"
    CALL c_diff_fast(u_prev_timestep, du_prev_timestep, du_dummy, j_lev, ng, meth, 10, ne, 1, ne)

    !
    ! does not rely on u so we can call it once here
    !
    shift = 0 !tmp
    CALL c_diff_diag(du, d2u, j_lev, ng, meth, meth, 11)

    !--Form right hand side of Navier--Stokes equations
    IF (dim==2) THEN
       DO ie = 1, 2
          shift=(ie-1)*ng

          !CALL c_diff_diag(u(shift+1:shift+ng), du, d2u, j_lev, ng, meth, meth, 11)
          user_Drhs_diag(shift+1:shift+ng) = - (u_prev_timestep(1:ng)+Umn(1))*du(:,1) -&
               (u_prev_timestep(ng+1:2*ng)+Umn(2))*du(:,2) + nu*SUM(d2u,2)

          !CALL c_diff_fast(u_prev_timestep(shift+1:shift+ng), du, d2u, j_lev, ng, meth, 10, 1, 1, 1)
          user_Drhs_diag(shift+1:shift+ng) = user_Drhs_diag(shift+1:shift+ng) - du_prev_timestep(ie,:,ie)
       END DO
    ELSE IF (dim==3) THEN
       DO ie = 1, 3
          shift=(ie-1)*ng
          !PRINT *,'CAlling c_diff_diag from user_Drhs_diag()'
          !CALL c_diff_diag(u(shift+1:shift+ng), du, d2u, j_lev, ng, meth, meth, 11)
          user_Drhs_diag(shift+1:shift+ng) = - (u_prev_timestep(1:ng)+Umn(1))*du(:,1) -&
               (u_prev_timestep(ng+1:2*ng)+Umn(2))*du(:,2) - &
               (u_prev_timestep(2*ng+1:3*ng)+Umn(3))*du(:,3) + nu*SUM(d2u,2)

          !CALL c_diff_fast(u_prev_timestep(shift+1:shift+ng), du, d2u, j_lev, ng, meth, 10, 1, 1, 1)
          user_Drhs_diag(shift+1:shift+ng) = user_Drhs_diag(shift+1:shift+ng) - du_prev_timestep(ie,:,ie)

       END DO
       IF(sgsmodel /= 0) CALL SGS_Drhs_diag (user_Drhs_diag, du_prev_timestep, du, d2u, meth)
    END IF
  END FUNCTION user_Drhs_diag

  FUNCTION user_chi (nlocal, t_local )
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: nlocal
    REAL (pr), INTENT (IN) :: t_local
    REAL (pr), DIMENSION (nlocal) :: user_chi

    user_chi = 0.0_pr
  END FUNCTION user_chi



  !
  ! Calculate any statitics
  !
  ! startup_flag - 0 when adapting to IC,then 1 inmain integration loop
  !
  !
  SUBROUTINE user_stats ( u ,j_mn, startup_flag)
!    USE fft_module
!    USE spectra_module
    USE parallel
    IMPLICIT NONE
    REAL (pr), DIMENSION (nwlt,1:n_var), INTENT (IN) :: u
    INTEGER , INTENT (IN) :: j_mn 
    INTEGER , INTENT (IN) :: startup_flag
    CHARACTER (LEN=256)  :: filename
    ! CHARACTER (LEN=256)             :: file_out
    INTEGER outputfileUNIT
    REAL (pr) :: area !area of domain to normalize dA
    REAL (pr) :: ttke
    INTEGER count_u_g_mdl_gridfilt_mask, count_u_g_mdl_testfilt_mask
    LOGICAL , SAVE :: start = .TRUE.
   

    !USER may define additional statistics ouutput here.

        

    IF( ALLOCATED(dA) .AND. sgsmodel == 0) THEN
       area =  (xyzlimits(2,1)- xyzlimits(1,1)) * &
            (xyzlimits(2,2)- xyzlimits(1,2)) * &
            (xyzlimits(2,3)- xyzlimits(1,3)) 

       ttke = 0.5_pr * SUM( (dA(:)/area)*(u(:,1)**2 + u(:,2)**2 + u(:,3)**2 ))
       CALL parallel_global_sum( REAL=ttke )
       
       IF (par_rank.EQ.0) THEN
          PRINT *,' Resolved Field Stats'
          PRINT *,'***********************************************'
          WRITE(*,'(" (Approximate) TKE =",E12.5)')  ttke
       END IF
    END IF
  END SUBROUTINE user_stats

  SUBROUTINE user_cal_force (u, n, t_local, force, drag, lift)
    !--Calculates drag and lift on obstacle using penalization formula
    IMPLICIT NONE

    INTEGER, INTENT (IN) :: n
    REAL (pr), INTENT (IN) :: t_local
    REAL (pr), DIMENSION (dim), INTENT (INOUT) :: force
    REAL (pr), INTENT (OUT) :: drag, lift
    REAL (pr), DIMENSION (n,dim) :: u
    drag = 0.0_PR
    lift = 0.0_PR

    !
    ! There is no obstacle in flow
    !

  END SUBROUTINE user_cal_force

  !
  ! calculate any additional variables
  ! 
  ! arg 
  ! flag  - 0 calledwhile adapting to IC, 1 - called in main time integration loop
  !
  ! These additiona variables are calculated and left in real space.
  !
  SUBROUTINE user_additional_vars( t_local, flag )
    IMPLICIT NONE
    REAL (pr), INTENT (IN) ::  t_local
    INTEGER , INTENT(IN) :: flag ! 0- called during adaption to IC, 1 called during main integration loop
    REAL(pr) tmp(nwlt,6) !tmp for vorticity

    
    IF( flag == 1 ) THEN ! only in main time int loop, not initial adaptation
       ! Calculate the magnitude vorticity
       IF( adaptMagVort ) THEN 
          CALL cal_vort (u(:,n0:n0+dim-1), tmp(:,1:3), nwlt)
          u(:,n_var_modvort) = SQRT(tmp(:,1)**2.0_pr + tmp(:,2)**2.0_pr + tmp(:,3)**2.0_pr )
       END IF

       ! Calculate the magnitude of Sij (sqrt(SijSij) )
       ! s(:,1) = s_11
       ! s(:,2) = s_22
       ! s(:,3) = s_33
       ! s(:,4) = s_12
       ! s(:,5) = s_13
       ! s(:,6) = s_23
       IF( adaptNormS ) THEN
          CALL Sij( u(:,1:dim) ,nwlt, tmp(:,1:6), u(:,n_var_modSij), .TRUE. )
       END IF

       ! Calculate the magnitude of velocity
       IF( adaptMagVel ) THEN 
          u(:,n_var_modvel) = 0.5_pr*(u(:,1)**2.0_pr + u(:,2)**2.0_pr + u(:,3)**2.0_pr )
       END IF

    END IF

  END SUBROUTINE user_additional_vars


  !
  ! calculate any additional scalar variables
  ! 
  ! arg 
  ! flag  - 0 calledwhile adapting to IC, 1 - called in main time integration loop
  !
  SUBROUTINE user_scalar_vars( flag )
    IMPLICIT NONE
    INTEGER , INTENT(IN) :: flag ! 0- called during adaption to IC, 1 called during main integration loop



  END SUBROUTINE user_scalar_vars

  !
  !************ Calculating Scales ***************************
  !
  ! Note the order of the components in the scl array
  ! correspond to u_tn, v_tn, w_tn, u_tn-1, v_tn-1, w_tn-1, u_tn-2, v_tn-2, w_tn-2
  ! 
  SUBROUTINE user_scales(flag,use_default, u_loc, nlocal, ne_local, l_n_var_adapt , l_n_var_adapt_index, &
       scl, scl_fltwt) !add
    USE parallel
    IMPLICIT NONE
	INTEGER, INTENT (IN) :: flag ! 0 during initial adaptation, then 1 during time advancement
    LOGICAL , INTENT(INOUT) :: use_default
    INTEGER, INTENT (IN) :: nlocal, ne_local
    REAL (pr), DIMENSION (1:nlocal,1:ne_local), INTENT (IN) :: u_loc
    LOGICAL , INTENT (IN) :: l_n_var_adapt(ne_local)
    INTEGER , INTENT (IN) :: l_n_var_adapt_index(1:ne_local)
    REAL (pr), DIMENSION (1:ne_local), INTENT (INOUT) :: scl
    REAL (pr) ,  INTENT(IN) ::scl_fltwt !weight for temporal filter on scl
    REAL (pr) :: floor, tmp
    INTEGER :: ie, ie_index, itmp
    REAL (pr) ,save, ALLOCATABLE, DIMENSION (:) :: scl_old
    LOGICAL  ,save :: startup_init = .TRUE.

    !
    ! Ignore the output of this routine and use default scales routine
    !
    use_default = .FALSE. 

    !
    ! NOTE: For a parallel run, synchronize scl(:) across the processors in user_scales.
    !       Use the subroutine scales of default_util.f90 as an example,
    !       subroutine parallel_global_sum of parallel.f90 may be helpful.
    !       Already synchronized: sumdA_global
    IF( .NOT. use_default ) THEN

       IF (par_rank.EQ.0) PRINT *,'Use user_scales() <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<'
       !
       ! ALLOCATE scl_old if it was not done previously in user_scales
       !
       IF( startup_init .AND. .NOT. ALLOCATED(scl_old) ) THEN
          ALLOCATE( scl_old(1:ne_local) )
          scl_old = 0.0_pr
       END IF
       
       floor = 1.e-12_pr
       scl   =1.0_pr
       !
       ! Calculate scl per component
       !
       DO ie=1,ne_local
          IF(l_n_var_adapt(ie)) THEN
             ie_index = l_n_var_adapt_index(ie)
             IF( Scale_Meth == 1 ) THEN ! Use Linf scale
                scl(ie)= MAXVAL ( ABS( u_loc(1:nlocal,ie_index) ) )
                CALL parallel_global_sum( REALMAXVAL=scl(ie) )
                
             ELSE IF ( Scale_Meth == 2 ) THEN ! Use L2 scale
                scl(ie) = SUM( (u_loc(1:nlocal,ie_index)**2) )
                itmp = nlocal
                CALL parallel_global_sum( REAL=scl(ie) )
                CALL parallel_global_sum( INTEGER=itmp )
                scl(ie) = SQRT ( scl(ie)/itmp )
                
             ELSE IF ( Scale_Meth == 3 ) THEN ! Use L2 scale using dA
                scl(ie) = SUM( (u_loc(1:nlocal,ie_index)**2)*dA )
                CALL parallel_global_sum( REAL=scl(ie) )
                scl(ie) = SQRT ( scl(ie)/sumdA_global )

             ELSE
                IF (par_rank.EQ.0) THEN
                   PRINT *, 'Error in Scales(), Unknown Scale_Meth type: ', Scale_Meth
                   PRINT *, 'Known Scale_Meth types: 1 = Linf scale, 2 = L2 scale '
                   PRINT *, 'Exiting ...'
                END IF
                CALL parallel_finalize; STOP
             END IF
             
             IF (par_rank.EQ.0) THEN
                WRITE (6,'("Scaling before taking vector(1:dim) magnitude")')
                WRITE (6,'("Scale factor eqn", I4 ," = ", es11.4 ," scalecoeff = ",es11.4 )') &
                     ie, scl(ie), scaleCoeff(ie)
             END IF
             
          END IF
       END DO
       !
       ! take appropriate vector norm over scl(1:dim)
       IF( Scale_Meth == 1 ) THEN ! Use Linf scale
          tmp = MAXVAL(scl(1:dim)) !this is statistically equivalent to velocity vector length
          scl(1:dim) = tmp
       ELSE IF ( Scale_Meth == 2 ) THEN ! Use L2 scale
          tmp = SQRT(SUM( scl(1:dim)**2 ) ) !now velocity vector length, rather than individual component
          scl(1:dim) = tmp
       ELSE IF ( Scale_Meth == 3 ) THEN ! Use L2 scale using dA
          tmp = SQRT(SUM( scl(1:dim)**2 ) ) !now velocity vector length, rather than individual component
          scl(1:dim) = tmp
       END IF
       !
       ! Print out new scl
       !
       DO ie=1,ne_local
          IF(l_n_var_adapt(ie)) THEN
             !this is done if one of the variable is exactly zero inorder not to adapt to the nois
             IF(scl(ie) .le. floor) scl(ie)=1.0_pr 
             tmp = scl(ie)
             ! temporally filter scl
             IF(.not. startup_init ) scl(ie) = scl_fltwt * scl_old(ie) + (1.0_pr - scl_fltwt) * scl(ie)
             scl = scaleCoeff * scl
             IF (par_rank.EQ.0) THEN
                WRITE (6,'("Scaling on vector(1:dim) magnitude")')
                WRITE (6,'("Scale factor eqn", I4 ," = ", es11.4 ," scalecoeff = ",es11.4 )') &
                     ie, scl(ie), scaleCoeff(ie)
                WRITE (6,'("(before filtering = ",es11.4,")" )') tmp
             END IF
          END IF
       END DO
       
       scl_old = scl !save scl for this time step
       startup_init = .FALSE.
       !PRINT *,'TEST scl_old ', scl_old
    END IF ! use default
    
  END SUBROUTINE user_scales

  SUBROUTINE user_cal_cfl (use_default, u, cfl_out)
    USE parallel
    IMPLICIT NONE
    LOGICAL , INTENT(INOUT) :: use_default
    REAL (pr),                                INTENT (INOUT) :: cfl_out
    REAL (pr), DIMENSION (nwlt,n_integrated), INTENT (IN)    :: u
    
    INTEGER                    :: i
    REAL (pr)                  :: floor
    REAL (pr), DIMENSION (dim) :: cfl
    REAL (pr), DIMENSION(dim,nwlt) :: h_arr
    
    use_default = .FALSE.
    
    floor = 1e-12_pr
    cfl_out = floor
    
    CALL get_all_local_h (h_arr)
    
    DO i = 1, nwlt
       cfl(1:dim) = ABS (u(i,1:dim)+Umn(1:dim)) * dt/h_arr(1:dim,i)
       cfl_out = MAX (cfl_out, MAXVAL(cfl))
    END DO
    CALL parallel_global_sum( REALMAXVAL=cfl_out )
    
  END SUBROUTINE user_cal_cfl
  
  SUBROUTINE  user_sgs_force ( u, nlocal)
    IMPLICIT NONE
    
    INTEGER,                         INTENT (IN) :: nlocal
    REAL (pr), DIMENSION (nlocal,n_integrated), INTENT (INOUT) :: u
    
    !default SGS_force, can be explicitely deifined below instead of calling default SGS_force
    IF(sgsmodel /= 0) CALL sgs_force ( u, nlocal)
    
  END SUBROUTINE user_sgs_force

  SUBROUTINE  user_pre_process
    IMPLICIT NONE
     
  END SUBROUTINE user_pre_process

  SUBROUTINE  user_post_process
    IMPLICIT NONE
    
  END SUBROUTINE user_post_process

END MODULE user_case





