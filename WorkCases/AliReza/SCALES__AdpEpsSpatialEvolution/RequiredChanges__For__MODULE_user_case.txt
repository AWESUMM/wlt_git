1)  Add        ::      USE variable_thresholding                        



2)  Add        ::      CALL user_read_input__variable_thersholding
    @ the end of       SUBROUTINE  user_read_input



3)  Add        ::      CALL user_setup_pde__variable_thersholding            
    @ the end of       SUBROUTINE  user_setup_pde



4)  Add        ::      CALL user_initial_conditions__variable_thersholding(u, nlocal, ne_local, t_local, scl, scl_fltwt, iter)
    @ the end of       SUBROUTINE  user_initial_conditions



5)  Add        ::      CALL user_post_process__variable_thersholding(n_var_ExtraAdpt_Der)
    @ the end of       SUBROUTINE  user_post_process

6)
The logical Flags are ::
do_Adp_Eps_Spatial = T           
do_Adp_Eps_Spatial_Evol = T

The first one activate  Performing Spatial Adaptation for Epsilon Using
"Lagrangian Path-Line Diffusive Averaging Evolution Equation"  
This is using you interpolation module.
 
The second one activate   Performing  Spatial Adaptation  for  Epsilon  Using
Evolution Equation for "Lagrangian Variable Thresholding"   
 
variable_thresholding.f90   is a new file
Where  user can adjust the  forcing term  in   "SUBROUTINE
user_post_process__variable_thersholding"   for  case 1.
or  in  "SUBROUTINE Calculate__Forcing_AdpEpsSpEvol"   for  case 2

