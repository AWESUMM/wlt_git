function AdpEpsSpatial
%% Compare  SCALES__AdpEpsSpatial  __  init Gcf6b-ldkm-BB.db_wrk.0000.res  __  Nu0.09_Cf6.m
%
%% Comparing Different Serial Runs   
%                                     on   Different Machines   
%                                  using   Different Number of Processors   
%                                    and   Different Queues
%                                    FOR   Different Epsilon
% 
%%  

clear all;
% close all;

LineColorMarker      =  {'sk--'; '^r--'; 'xb-.';  '>g- '; '<m: '; '^c--'; 'ob-.'; 'sg-.'; 'or'; '^k'; 'c'; 'm'; 'k'; 'r'; 'xg'; 'xm'; 'ob'; '^c'; '<m'; '>y'; 'g'; 'b-.'  };
LineColorMarker      =  {'r-.'; 'b--';  'g: '; 'm-. '; 'k-'; 'r-'; 'y-'; 'c'; 'g'; 'c'; 'm--'; 'k'; 'r'; 'm'; 'c'; 'b'; 'g'; 'ok'; '>y'; 'g'; 'b-.'  };
LineColorMarkerMean  =  {'r:'; 'b:';  'g:';  'm:'; 'k:'; 'r:'; 'y:'; 'c'; 'g'; 'c'; 'm--'; 'k'; 'r'; 'm'; 'c'; 'b'; 'g'; 'ok'; '>y'; 'g'; 'b-.'  };

LineColorMarker      =  {'r-.'; 'b--';  'r: '; 'm-. '; 'k-'; 'r-'; 'y-'; 'c'; 'g'; 'c'; 'm--'; 'k'; 'r'; 'm'; 'c'; 'b'; 'g'; 'ok'; '>y'; 'g'; 'b-.'  };
LineColorMarkerMean  =  {'b:'; 'b:';  'r:';  'm:'; 'k:'; 'r:'; 'y:'; 'c'; 'g'; 'c'; 'm--'; 'k'; 'r'; 'm'; 'c'; 'b'; 'g'; 'ok'; '>y'; 'g'; 'b-.'  };

LineColorMarker      =  {'r-.'; 'r--';  'b: '; 'g-. '; 'k-'; 'y-'; 'c-'; 'c'; 'g'; 'k'; 'm'; 'b'; 'g:'; 'y:'; 'c'; 'b'; 'g'; 'ok'; '>y'; 'g'; 'b-.'  };
LineColorMarker      =  {       'r--';  'b--'; 'g-- '; 'k-'; 'y-'; 'c-'; 'b'; 'g'; 'k'; 'm'; 'b'; 'g:'; 'y:'; 'r'; 'b'; 'g'; 'ok'; '>y'; 'g'; 'b-.'  };
LineColorMarker      =  {       'r-';   'b--'; 'g-. '; 'k-'; 'y>-'; 'c.-'; 'b.-'; 'g'; 'k'; 'm'; 'b'; 'g:'; 'y:'; 'r'; 'b'; 'g'; 'ok'; '>y'; 'g'; 'b-.'  };
LineColorMarkerMean  =  {       'r:';   'b:';  'g:';   'k:'; 'y:'; 'y:'; 'c'; 'g'; 'c'; 'm--'; 'k'; 'r'; 'm'; 'c'; 'b'; 'g'; 'ok'; '>y'; 'g'; 'b-.'  };

LineWidth        =  {0.5;    0.5;    0.5;     0.5;    0.5;    0.5;    0.5;    0.5;    0.5;  0.5; 0.5; 0.5; 0.5; 0.5; 0.5;  0.5;  0.5;  0.5;  0.5;  0.5;  2.0; 0.5    };
%                    1        2       3        4       5       6       7       8       9     10   11   12   13   14   15    16    17    18   19     20   21   22  

case_Dim = 3;
plot_step=4;
Font_Size = 16;

%%%%%  .log   &   _user_stats        F i l e     N a m e
File_Type_Number = 2; %2;                                      %  1:: '.p0_log'         2 ::  '.p0_user_stats'       3 :: '.case_isoturb.log'

Log_Info_Variable = 3; %10;  %(for File=5) 10, 15, 22, 29, 30

% '._log'                F i l e     V a r i a b l e s  ::: 
% t,  dt,  j_lev,  nwlt,  nxyz(1),  nxyz(2),  nxyz(3),  eps,  nwlt(j=1),  nwlt(j=2),  nwlt(j=3),  nwlt(j=4),  nwlt(j=5),  nwlt(j=6)

% '.p0_user_stats'       F i l e     V a r i a b l e s  ::: 
%Time          t_eddy           Nwlt    Nwlt/Nmax     Re_taylor     eta/h         <KE>          <DISS_res>    <DISS_SGS>    fracSGS_DISS  epsilon

% '.case_isoturb.log'    F i l e     V a r i a b l e s  ::: 
%Time           tke            Cs             ttl_res_diss   ttl_sgs_diss   gridfilt_mask  testfilt_mask  Psgs_diss      total_Ksgs     eps   total_neg_sgs_diss  backscatter_frac

%'..turbstats'           F i l e     V a r i a b l e s  ::: 
% Time  eps_sim, eps_post, scl.u, scl.v, scl.w, nwlt, percent.compression  uprime  tke  tke.from.Fourier.Components  uprime  skew.u  kurt.u  duprime  skew.du  kurt.du  dduprime  skew.dd  kurt.ddu    vis  ediss  eta  lambda  ldiss  int.length  duprime/dduprime  kts  lett  ret  rei  
% 1     2        3         4      5      6      7     8                    9       10   11                           12      13      14      15       16       17       18        19       20          21   22     23    24     25     26          27                28    29   30   31
 








switch (File_Type_Number)
    case (1)
        File_Type = '.p0_log'; 
    case (2)
        File_Type = '.p0_user_stats';
    case (3)
        File_Type = '.case_isoturb.log';               
    case (4)
        File_Type = '..avg.spectra';               
    case (5)
        File_Type = '..turbstats';
end



switch (case_Dim)
    case (2)
        no_columns = 7;    % t,  dt,  j_lev,  nwlt,  nxyz(1),  nxyz(2),  eps
    case (3)
        no_columns = 8;    % t,  dt,  j_lev,  nwlt,  nxyz(1),  nxyz(2),  nxyz(3),  eps
end



Plot_LDKMG = 'F';
Plot_PostProcessed_Forcing = false;
Plot_PostProcessed_Diss    = false;





%%

Test_Number = 2;
i=1;

switch Test_Number
    case {1}
        


%% requin.sharcnet.ca  HP            tg-steele.purdue.teragrid.org  Dell

%% QueenBee   LONI   Dell
DIR       = 'C:\Documents and Settings\AliReza\My Documents\T e r a__G r i d\QueenBee   LONI   Dell\';

%% Steele   Purdue   Dell
DIR      = 'C:\Documents and Settings\AliReza\My Documents\T e r a__G r i d\Steele   Purdue   Dell\SpeedUpTest_01__Eps2_Nu9_Cf5.2\DLB\';

%% Prof. Giuliano De Stefano
DIR       = 'D:\wavelet_Simulations\ForcedHomogeneousTurbulence\SCALES__AdpEpsSpatialEvolution\';

% CaseDIR                 = 'ldkmGB\';
% CaseName                = 'FHT-00900_ldkmGB043-wrk';
% Case_Path_Name{i}       = [DIR CaseDIR  CaseName File_Type];  
% CaseLegend{i}           = ['LDKM  eps=0.43'];
% CaseLegend{i}           = ['eps = 0.43'];
% Number_Processors{i}    = 1;
% Case_CPUTime__Path_Name{i} = [DIR CaseDIR CaseName '.cputime'];
% i=i+1;

%% blast   MSMSL   HP (Intel)
% DIR       = 'D:\wavelet_Simulations\ForcedHomogeneousTurbulence\SCALES__AdpEpsSpatial\';
% CaseDIR                 = 'GDM\eps=0.43\';
% CaseName                = 'SCALES_GDM_Nu0.09_Cf6_Eps0.43.db_tree';
% Case_Path_Name{i}       = [DIR CaseDIR  CaseName File_Type];  
% CaseLegend{i}           = ['GDM  eps=0.43'];
% CaseLegend{i}           = ['eps = 0.43'];
% Number_Processors{i}    = 1;
% Case_CPUTime__Path_Name{i} = [DIR CaseDIR CaseName '.cputime'];
% i=i+1;

DIR       = 'D:\wavelet_Simulations\ForcedHomogeneousTurbulence\SCALES\';

CaseDIR                 = 'LKM__eps=043__dbwrk\';
CaseName                = 'SCALES_LKM_Nu009_Cf6_Eps043_dbwrk';
Case_Path_Name{i}       = [DIR CaseDIR  CaseName File_Type];  
CaseLegend{i}           = ['eps=0.43'];  %['LKM  eps=0.43'];
Number_Processors{i}    = 1;
Case_CPUTime__Path_Name{i} = [DIR CaseDIR CaseName '.cputime'];
i=i+1;


% DIR       = 'D:\wavelet_Simulations\ForcedHomogeneousTurbulence\SCALES__AdpEpsSpatialEvolution\';
% 
% CaseDIR                 = 'LKM\eps=0.43\';
% CaseName                = 'SCALES_LKM_Nu0.09_Cf6_Eps0.43.db_tree';
% Case_Path_Name{i}       = [DIR CaseDIR  CaseName File_Type];  
% CaseLegend{i}           = ['LKM  eps=0.43'];
% Number_Processors{i}    = 1;
% Case_CPUTime__Path_Name{i} = [DIR CaseDIR CaseName '.cputime'];
% i=i+1;
% 
% CaseDIR                 = 'LKM\eps=0.3\';
% CaseName                = 'SCALES_LKM_Nu0.09_Cf6_Eps0.3.db_tree';
% Case_Path_Name{i}       = [DIR CaseDIR  CaseName File_Type];  
% CaseLegend{i}           = ['LKM  eps=0.3'];
% Number_Processors{i}    = 1;
% Case_CPUTime__Path_Name{i} = [DIR CaseDIR CaseName '.cputime'];
% i=i+1;










% DIR       = 'D:\wavelet_Simulations\ForcedHomogeneousTurbulence\SCALES__AdpEpsSpatial\';
% 
% %D:\wavelet_Simulations\ForcedHomogeneousTurbulence\SCALES__AdpEpsSpatial\GDM\eps=0.2_0.6\
% % CaseDIR                 = 'GDM\eps=0.2_0.6\';
% % CaseName                = 'SCALES_GDM_Nu0.09_Cf6_AdpEpsSp.db_tree';
% % Case_Path_Name{i}       = [DIR CaseDIR  CaseName File_Type];   
% % CaseLegend{i}           = [10 'blast Serial' 10 'GDM  eps_{min}=0.2 eps_{max}=0.6'];     %'eps =[0.2 , 0.6]'      'eps_{min}=0.2 eps_{max}=0.6'
% % Number_Processors{i}    = 1;
% % Case_CPUTime__Path_Name{i} = [DIR CaseDIR CaseName '.cputime'];
% % i=i+1;
% 
% CaseDIR                 = 'GDM\eps=0.2_0.43\';
% CaseName                = 'SCALES_GDM_Nu0.09_Cf6_AdpEpsSp0.2&0.43.db_tree';
% Case_Path_Name{i}       = [DIR CaseDIR  CaseName File_Type];   
% CaseLegend{i}           = ['GDM  eps_{min}=0.2 eps_{max}=0.43  Forcing : |w|'];                         %'eps =[0.2 , 0.43]'     'eps_{min}=0.2 eps_{max}=0.43'
% Number_Processors{i}    = 1;
% Case_CPUTime__Path_Name{i} = [DIR CaseDIR CaseName '.cputime'];
% i=i+1;
% 
% CaseDIR                 = 'GDM\SGSD\eps=0.2_0.43\';
% CaseName                = 'SCALES_GDM_Nu0.09_Cf6_AdpEpsSp0.2&0.43.db_tree';
% Case_Path_Name{i}       = [DIR CaseDIR  CaseName File_Type];  
% CaseLegend{i}           = ['GDM  eps_{min}=0.2 eps_{max}=0.43  Forcing : SGSD,RD'];
% Number_Processors{i}    = 1;
% Case_CPUTime__Path_Name{i} = [DIR CaseDIR CaseName '.cputime'];
% i=i+1;
% 
% CaseDIR                 = 'LKM\SGSD\eps=0.2_0.43\Forcing_Factor__AdpEpsSp=400\';
% CaseName                = 'SCALES_LKM_Nu0.09_Cf6_AdpEpsSp0.2&0.43.db_tree';
% Case_Path_Name{i}       = [DIR CaseDIR  CaseName File_Type];  
% CaseLegend{i}           = ['LKM  eps_{min}=0.2 eps_{max}=0.43  Forcing : SGSD,RD'];
% Number_Processors{i}    = 1;
% Case_CPUTime__Path_Name{i} = [DIR CaseDIR CaseName '.cputime'];
% i=i+1;
% 
% CaseDIR                 = 'LKM\SGSD\eps=0.2_0.43\Forcing_Factor__AdpEpsSp=600\';
% CaseName                = 'SCALES_LKM_Nu0.09_Cf6_AdpEpsSp0.2&0.43_600.db_tree';
% Case_Path_Name{i}       = [DIR CaseDIR  CaseName File_Type];  
% CaseLegend{i}           = ['LKM  eps_{min}=0.2 eps_{max}=0.43  Forcing : SGSD,RD 600'];
% Number_Processors{i}    = 1;
% Case_CPUTime__Path_Name{i} = [DIR CaseDIR CaseName '.cputime'];
% i=i+1;








DIR       = 'D:\wavelet_Simulations\ForcedHomogeneousTurbulence\SCALES__AdpEpsSpatialEvolution\';

% CaseDIR                 = 'LKM\SGSD\eps=0.2_0.43\Forcing_Factor__AdpEpsSp=400\test_12__with_interpolation\';
% CaseName                = 'SCALES_LKM_Nu0.09_Cf6_AdpEpsSp0.2&0.43.db_tree';
% Case_Path_Name{i}       = [DIR CaseDIR  CaseName File_Type];  
% CaseLegend{i}           = ['LKM  eps_{min}=0.2 eps_{max}=0.43  Forcing : SGSD,RD 400  Interpolation'];    %Evolution-Eps
% Number_Processors{i}    = 1;
% Case_CPUTime__Path_Name{i} = [DIR CaseDIR CaseName '.cputime'];
% i=i+1;

CaseDIR                 = 'LKM\SGSD\eps=0.2_0.43\Forcing_Factor__AdpEpsSp=400\Optimized_1\';
CaseName                = 'SCALES_LKM_Nu0.09_Cf6_AdpEpsSp0.2&0.43.db_tree';
Case_Path_Name{i}       = [DIR CaseDIR  CaseName File_Type];  
%CaseLegend{i}          = ['LKM  eps_{min}=0.2 eps_{max}=0.43  Forcing : SGSD,RD 400  Interpolation   Optimized-1'];    %Evolution-Eps
CaseLegend{i}           = ['LKM  Goal=0.4'];
CaseLegend{i}           = ['Goal = 0.4   FT1'];
Number_Processors{i}    = 1;
Case_CPUTime__Path_Name{i} = [DIR CaseDIR CaseName '.cputime'];
i=i+1;

% CaseDIR                 = 'LKM\SGSD\eps=0.2_0.43\Forcing_Factor__AdpEpsSp=400\test_12__with_interpolation\Psgs_diss_goal=0.2\';
% CaseName                = 'SCALES_LKM_Nu0.09_Cf6_AdpEpsSp0.2&0.43_Goal0.2.db_tree';
% Case_Path_Name{i}       = [DIR CaseDIR  CaseName File_Type];  
% CaseLegend{i}           = ['LKM  eps_{min}=0.2 eps_{max}=0.43  Forcing : SGSD,RD,400  Interpolation  Goal=0.2'];
% Number_Processors{i}    = 1;
% Case_CPUTime__Path_Name{i} = [DIR CaseDIR CaseName '.cputime'];
% i=i+1;

CaseDIR                 = 'LKM\SGSD\eps=0.2_0.43\Forcing_Factor__AdpEpsSp=400\Psgs_diss_goal=0.3\';
CaseName                = 'SCALES_LKM_Nu0.09_Cf6_AdpEpsSp0.2&0.43_Goal0.3.db_tree';
Case_Path_Name{i}       = [DIR CaseDIR  CaseName File_Type];  
%CaseLegend{i}          = ['LKM  eps_{min}=0.2 eps_{max}=0.43  Forcing : SGSD,RD,400  Interpolation  Goal=0.3'];
CaseLegend{i}           = ['LKM  Goal=0.3'];
CaseLegend{i}           = ['Goal = 0.3   FT1'];
Number_Processors{i}    = 1;
Case_CPUTime__Path_Name{i} = [DIR CaseDIR CaseName '.cputime'];
i=i+1;

CaseDIR                 = 'LKM\SGSD\eps=0.2_0.43\Forcing_Factor__AdpEpsSp=400\Psgs_diss_goal=0.25\';
CaseName                = 'SCALES_LKM_Nu0.09_Cf6_AdpEpsSp0.2&0.43_Goal0.25.db_tree';
Case_Path_Name{i}       = [DIR CaseDIR  CaseName File_Type];  
%CaseLegend{i}          = ['LKM  eps_{min}=0.2 eps_{max}=0.43  Forcing : SGSD,RD,400  Interpolation  Goal=0.25'];
CaseLegend{i}           = ['LKM  Goal=0.25'];
CaseLegend{i}           = ['Goal = 0.25   FT1'];
Number_Processors{i}    = 1;
Case_CPUTime__Path_Name{i} = [DIR CaseDIR CaseName '.cputime'];
i=i+1;






% DIR       = 'D:\wavelet_Simulations\ForcedHomogeneousTurbulence\SCALES\';
% 
% CaseDIR                 = 'LKM__eps=043__dbwrk\';
% CaseName                = 'SCALES_LKM_Nu009_Cf6_AdpEpsSpatial02043_G03__3';
% Case_Path_Name{i}       = [DIR CaseDIR  CaseName File_Type];  
% CaseLegend{i}           = ['Goal = 0.3  *EpsSpatial_Forcing_TimeScale'];
% Number_Processors{i}    = 1;
% Case_CPUTime__Path_Name{i} = [DIR CaseDIR CaseName '.cputime'];
% i=i+1;
% 
% CaseDIR                 = 'LKM__eps=043__dbwrk\';
% CaseName                = 'SCALES_LKM_Nu009_Cf6_AdpEpsSpatial02043_G03__4';
% Case_Path_Name{i}       = [DIR CaseDIR  CaseName File_Type];  
% CaseLegend{i}           = ['Goal = 0.3  /EpsSpatial_Forcing_TimeScale'];
% Number_Processors{i}    = 1;
% Case_CPUTime__Path_Name{i} = [DIR CaseDIR CaseName '.cputime'];
% i=i+1;



DIR       = 'D:\wavelet_Simulations\ForcedHomogeneousTurbulence\SCALES__AdpEpsSpatialEvolution\';

CaseDIR                 = 'LKM\SGSD\eps=0.2_0.43\Forcing_Factor__AdpEpsSp=400\Psgs_diss_goal=03__ICrestart__Forcing11\';
CaseName                = 'cSCALES_LKM_Nu0.09_Cf6_AdpEpsSp0.2&0.43_Goal0.3.db_tree';
Case_Path_Name{i}       = [DIR CaseDIR  CaseName File_Type];  
CaseLegend{i}           = ['Goal = 0.3   FT2'];
Number_Processors{i}    = 1;
Case_CPUTime__Path_Name{i} = [DIR CaseDIR CaseName '.cputime'];
i=i+1;






% DIR       = 'D:\wavelet_Simulations\ForcedHomogeneousTurbulence\SCALES__AdpEpsSpatialEvolution\';
% 
% CaseDIR                 = 'whale.sharcnet.ca\LKM\eps=02_043\Goal02__ICfromGoal025__Forcing11\';
% CaseName                = 'SCALES_LKM_Nu009_Cf6_AdpEpsSp02043_Goal02.db_tree';
% Case_Path_Name{i}       = [DIR CaseDIR  CaseName File_Type];  
% CaseLegend{i}           = ['Goal = 0.2   FT2'];
% Number_Processors{i}    = 1;
% Case_CPUTime__Path_Name{i} = [DIR CaseDIR CaseName '.cputime'];
% i=i+1;








% DIR       = 'D:\wavelet_Simulations\ForcedHomogeneousTurbulence\SCALES__AdpEpsSpatialEvolution\';
% 
% CaseDIR                 = 'requin.sharcnet.ca\LKM\eps_02_043\Forcing11\Goal03\dt_2e4\';
% CaseName                = 'cSCALES_LKM_Nu0.09_Cf6_AdpEpsSp0.2&0.43_Goal0.3.db_tree';
% Case_Path_Name{i}       = [DIR CaseDIR  CaseName File_Type];  
% CaseLegend{i}           = ['Goal = 0.3   FT2   \Deltat=2e-4'];
% Number_Processors{i}    = 1;
% Case_CPUTime__Path_Name{i} = [DIR CaseDIR CaseName '.cputime'];
% i=i+1;
% 
% CaseDIR                 = 'requin.sharcnet.ca\LKM\eps_02_043\Forcing11\Goal03\dt_2e4__smallTol12\';
% CaseName                = 'cSCALES_LKM_Nu0.09_Cf6_AdpEpsSp0.2&0.43_Goal0.3.db_tree';
% Case_Path_Name{i}       = [DIR CaseDIR  CaseName File_Type];  
% CaseLegend{i}           = ['Goal = 0.3   FT2   \Deltat=2e-4   Small tol1 & tol2'];
% Number_Processors{i}    = 1;
% Case_CPUTime__Path_Name{i} = [DIR CaseDIR CaseName '.cputime'];
% i=i+1;






% DIR       = 'D:\wavelet_Simulations\ForcedHomogeneousTurbulence\SCALES__AdpEpsSpatialEvolution\';
% 
% CaseDIR                 = 'LKM\SGSD\eps=02_06\Forcing11\Goal03\';
% CaseName                = 'cSCALES_LKM_Nu0.09_Cf6_AdpEpsSp0.2&0.43_Goal0.3.db_tree';
% Case_Path_Name{i}       = [DIR CaseDIR  CaseName File_Type];  
% CaseLegend{i}           = ['Goal = 0.3   FT2   eps-max=0.6   \Deltat=5e-4'];
% Number_Processors{i}    = 1;
% Case_CPUTime__Path_Name{i} = [DIR CaseDIR CaseName '.cputime'];
% i=i+1;




% DIR       = 'D:\wavelet_Simulations\ForcedHomogeneousTurbulence\SCALES__AdpEpsSpatialEvolution\whale.sharcnet.ca\';
% 
% CaseDIR                 = 'LKM\eps_02_06\Forcing11\Goal03\dt_4e4\';
% CaseName                = 'cSCALES_LKM_Nu0.09_Cf6_AdpEpsSp0.2&0.43_Goal0.3.db_tree';
% Case_Path_Name{i}       = [DIR CaseDIR  CaseName File_Type];  
% CaseLegend{i}           = ['Goal = 0.3   FT2   eps-max=0.6   \Deltat=4e-4'];
% Number_Processors{i}    = 1;
% Case_CPUTime__Path_Name{i} = [DIR CaseDIR CaseName '.cputime'];
% i=i+1;
% 
% CaseDIR                 = 'LKM\eps_02_06\Forcing11\Goal03\dt_5e4\';
% CaseName                = 'cSCALES_LKM_Nu0.09_Cf6_AdpEpsSp0.2&0.43_Goal0.3.db_tree';
% Case_Path_Name{i}       = [DIR CaseDIR  CaseName File_Type];  
% CaseLegend{i}           = ['Goal = 0.3   FT2   eps-max=0.6   \Deltat=5e-4'];
% Number_Processors{i}    = 1;
% Case_CPUTime__Path_Name{i} = [DIR CaseDIR CaseName '.cputime'];
% i=i+1;






% DIR       = 'D:\wavelet_Simulations\ForcedHomogeneousTurbulence\SCALES__AdpEpsSpatialEvolution\whale.sharcnet.ca\';
% 
% CaseDIR                 = 'LKM\eps_02_noMax\Forcing11\Goal03\dt_4e4\';
% CaseName                = 'cSCALES_LKM_Nu0.09_Cf6_AdpEpsSp0.2&0.43_Goal0.3.db_tree';
% Case_Path_Name{i}       = [DIR CaseDIR  CaseName File_Type];  
% CaseLegend{i}           = ['Goal = 0.3   FT2   eps-02-noMax   \Deltat=4e-4'];
% Number_Processors{i}    = 1;
% Case_CPUTime__Path_Name{i} = [DIR CaseDIR CaseName '.cputime'];
% i=i+1;
% 
% CaseDIR                 = 'LKM\eps_02_noMax\Forcing11\Goal03\dt_5e4\';
% CaseName                = 'cSCALES_LKM_Nu0.09_Cf6_AdpEpsSp0.2&0.43_Goal0.3.db_tree';
% Case_Path_Name{i}       = [DIR CaseDIR  CaseName File_Type];  
% CaseLegend{i}           = ['Goal = 0.3   FT2   eps-02-noMax   \Deltat=5e-4'];
% Number_Processors{i}    = 1;
% Case_CPUTime__Path_Name{i} = [DIR CaseDIR CaseName '.cputime'];
% i=i+1;








DIR       = 'D:\wavelet_Simulations\ForcedHomogeneousTurbulence\SCALES__AdpEpsSpatialEvolution\requin.sharcnet.ca\';

CaseDIR                 = 'LKM\eps=02_05\Forcing11\Goal03\dt_5e4\';
CaseName                = 'cSCALES_LKM_Nu0.09_Cf6_AdpEpsSp0.2&0.43_Goal0.3.db_tree';
Case_Path_Name{i}       = [DIR CaseDIR  CaseName File_Type];  
%CaseLegend{i}           = ['Goal = 0.3   FT2   eps-max=0.5   \Deltat=5e-4'];
CaseLegend{i}           = ['Goal = 0.3   FT2   eps-max=0.5'];
Number_Processors{i}    = 1;
Case_CPUTime__Path_Name{i} = [DIR CaseDIR CaseName '.cputime'];
i=i+1;


% DIR       = 'D:\wavelet_Simulations\ForcedHomogeneousTurbulence\SCALES__AdpEpsSpatialEvolution\requin.sharcnet.ca\';
% 
% CaseDIR                 = 'LKM\eps_02_043\Forcing11\Goal025\dt_4e4\';
% CaseName                = 'SCALES_LKM_Nu009_Cf6_AdpEpsSp02043_Goal025.db_tree';
% Case_Path_Name{i}       = [DIR CaseDIR  CaseName File_Type];  
% %CaseLegend{i}           = ['Goal = 0.3   FT2   eps-max=0.5   \Deltat=5e-4'];
% CaseLegend{i}           = ['Goal = 0.25   FT2'];
% Number_Processors{i}    = 1;
% Case_CPUTime__Path_Name{i} = [DIR CaseDIR CaseName '.cputime'];
% i=i+1;








% DIR       = 'D:\wavelet_Simulations\ForcedHomogeneousTurbulence\SCALES\requin.sharcnet.ca\';
% 
% CaseDIR                 = 'LKM__eps=03__dbwrk\';
% CaseName                = 'SCALES_LKM_Nu009_Cf6_Eps03_dbwrk';
% Case_Path_Name{i}       = [DIR CaseDIR  CaseName File_Type];  
% CaseLegend{i}           = ['eps=0.3'];  %['LKM  eps=0.43'];
% Number_Processors{i}    = 1;
% Case_CPUTime__Path_Name{i} = [DIR CaseDIR CaseName '.cputime'];
% i=i+1;
% 
% CaseDIR                 = 'LKM__eps=02__dbwrk\';
% CaseName                = 'SCALES_LKM_Nu009_Cf6_Eps02_dbwrk';
% Case_Path_Name{i}       = [DIR CaseDIR  CaseName File_Type];  
% CaseLegend{i}           = ['eps=0.3'];  %['LKM  eps=0.43'];
% Number_Processors{i}    = 1;
% Case_CPUTime__Path_Name{i} = [DIR CaseDIR CaseName '.cputime'];
% i=i+1;







if (Plot_LDKMG=='T')

DIR       = 'D:\wavelet_Simulations\ForcedHomogeneousTurbulence\SCALES__AdpEpsSpatialEvolution\ldkmGB\SGSD\eps=0.2_0.43\Forcing_Factor__AdpEpsSp=400\';

CaseDIR                 = 'Psgs_diss_goal=0.25\';
CaseName                = 'FHT-00900_ldkmGB043_Goal025_tree';
Case_Path_Name{i}       = [DIR CaseDIR  CaseName File_Type];  
CaseLegend{i}           = ['LDKM  Goal=0.25'];
CaseLegend{i}           = ['Goal=0.25'];
Number_Processors{i}    = 1;
Case_CPUTime__Path_Name{i} = [DIR CaseDIR CaseName '.cputime'];
i=i+1;

CaseDIR                 = 'Psgs_diss_goal=0.27\';
CaseName                = 'FHT-00900_ldkmGB043_Goal027_tree';
Case_Path_Name{i}       = [DIR CaseDIR  CaseName File_Type];  
CaseLegend{i}           = ['LDKM  Goal=0.27'];
CaseLegend{i}           = ['Goal=0.27'];
Number_Processors{i}    = 1;
Case_CPUTime__Path_Name{i} = [DIR CaseDIR CaseName '.cputime'];
i=i+1;

CaseDIR                 = 'Psgs_diss_goal=0.3\';
CaseName                = 'FHT-00900_ldkmGB043_Goal03_tree';
Case_Path_Name{i}       = [DIR CaseDIR  CaseName File_Type];  
CaseLegend{i}           = ['LDKM  Goal=0.3'];
CaseLegend{i}           = ['Goal=0.3'];
Number_Processors{i}    = 1;
Case_CPUTime__Path_Name{i} = [DIR CaseDIR CaseName '.cputime'];
i=i+1;

CaseDIR                 = 'Psgs_diss_goal=0.32\';
CaseName                = 'FHT-00900_ldkmGB043_Goal032_tree';
Case_Path_Name{i}       = [DIR CaseDIR  CaseName File_Type];  
CaseLegend{i}           = ['LDKM  Goal=0.32'];
CaseLegend{i}           = ['Goal=0.32'];
Number_Processors{i}    = 1;
Case_CPUTime__Path_Name{i} = [DIR CaseDIR CaseName '.cputime'];
i=i+1;

CaseDIR                 = 'Psgs_diss_goal=0.35\';
CaseName                = 'FHT-00900_ldkmGB043_Goal035_tree';
Case_Path_Name{i}       = [DIR CaseDIR  CaseName File_Type];  
CaseLegend{i}           = ['LDKM  Goal=0.35'];
CaseLegend{i}           = ['Goal=0.35'];
Number_Processors{i}    = 1;
Case_CPUTime__Path_Name{i} = [DIR CaseDIR CaseName '.cputime'];
i=i+1;

CaseDIR                 = 'Psgs_diss_goal=0.37\';
CaseName                = 'FHT-00900_ldkmGB043_Goal037_tree';
Case_Path_Name{i}       = [DIR CaseDIR  CaseName File_Type];  
CaseLegend{i}           = ['LDKM  Goal=0.37'];
CaseLegend{i}           = ['Goal=0.37'];
Number_Processors{i}    = 1;
Case_CPUTime__Path_Name{i} = [DIR CaseDIR CaseName '.cputime'];
i=i+1;

CaseDIR                 = 'Psgs_diss_goal=0.4\';
CaseName                = 'FHT-00900_ldkmGB043_Goal04_tree';
Case_Path_Name{i}       = [DIR CaseDIR  CaseName File_Type];  
CaseLegend{i}           = ['LDKM  Goal=0.4'];
CaseLegend{i}           = ['Goal=0.4'];
Number_Processors{i}    = 1;
Case_CPUTime__Path_Name{i} = [DIR CaseDIR CaseName '.cputime'];
i=i+1;

end












    case {2}

x_max = 0.8;

eps = 0.2;
nu  = 0.09; %0.015;
C_f = 6;

Title_Part_1 = 'SCALES'; %'CVS  (( Initialized by CVS cinit-00375-eps005.0000.res (serial) ))'
J_max_Title  = 6;
Resolution_Title = 256;
        
DIR      = 'D:\wavelet_Simulations\ForcedHomogeneousTurbulence\SCALES\';



CaseDIR  = 'LKM__eps02\Steele\d1_4cpu_dt2e4\';
CaseName = 'SCALES_LKM_Nu009_Cf6_Eps02';
Case_Path_Name{i} = [DIR CaseDIR CaseName File_Type];   %.p0_user_stats
CaseLegend{i}      = ['Re=70  \epsilon=0.2  d1  4cpu'];
Number_Processors{i} = 4;
J_max(i) = 6;
i=i+1;

CaseDIR  = 'LKM__eps02\Steele\d1_32cpu_dt2e4\';
CaseName = 'SCALES_LKM_Nu009_Cf6_Eps02';
Case_Path_Name{i} = [DIR CaseDIR CaseName File_Type];   %.p0_user_stats
CaseLegend{i}      = ['Re=70  \epsilon=0.2  d1  32cpu'];
Number_Processors{i} = 32;
J_max(i) = 6;
i=i+1;



CaseDIR  = 'LKM__eps03\Steele\d1_4cpu_dt2e4\';
CaseName = 'SCALES_LKM_Nu009_Cf6_Eps03';
Case_Path_Name{i} = [DIR CaseDIR CaseName File_Type];   %.p0_user_stats
CaseLegend{i}      = ['Re=70  \epsilon=0.3  d1  4cpu'];
Number_Processors{i} = 4;
J_max(i) = 6;
i=i+1;

CaseDIR  = 'LKM__eps03\Steele\d1_32cpu_dt2e4\';
CaseName = 'SCALES_LKM_Nu009_Cf6_Eps03';
Case_Path_Name{i} = [DIR CaseDIR CaseName File_Type];   %.p0_user_stats
CaseLegend{i}      = ['Re=70  \epsilon=0.3  d1  32cpu'];
Number_Processors{i} = 32;
J_max(i) = 6;
i=i+1;



CaseDIR  = 'LKM__eps03\blast\d1_2cpu_dt2e4\';
CaseName = 'SCALES_LKM_Nu009_Cf6_Eps03';
Case_Path_Name{i} = [DIR CaseDIR CaseName File_Type];   %.p0_user_stats
CaseLegend{i}      = ['Re=70  \epsilon=0.3  d1  2cpu'];
Number_Processors{i} = 2;
J_max(i) = 6;
i=i+1;



CaseDIR  = 'LKM__eps02\Steele\d3_32cpu_dt2e4\';
CaseName = 'SCALES_LKM_Nu009_Cf6_Eps02_d3';
Case_Path_Name{i} = [DIR CaseDIR CaseName File_Type];   %.p0_user_stats
CaseLegend{i}      = ['Re=70  \epsilon=0.2  d3  32cpu'];
Number_Processors{i} = 32;
J_max(i) = 6;
i=i+1;

CaseDIR  = 'LKM__eps03\Steele\d3_32cpu_dt2e4\';
CaseName = 'SCALES_LKM_Nu009_Cf6_Eps03_d3';
Case_Path_Name{i} = [DIR CaseDIR CaseName File_Type];   %.p0_user_stats
CaseLegend{i}      = ['Re=70  \epsilon=0.3  d3  32cpu'];
Number_Processors{i} = 32;
J_max(i) = 6;
i=i+1;



end






global CaseLegend
global Font_Size
Number_Case = i-1;


if   (File_Type_Number >= 4) i_start = 1;  i_end=4;
else                         i_start = 1;  i_end=Number_Case;
end


















%%

if ( strcmp(File_Type, '..avg.spectra')==0 )



%% COMPARE  all  Different  Runs

figure
hold on;



for i = i_start:i_end
    Log_Info = load( Case_Path_Name{i} );
    aaaa=Log_Info(:,1);
    bbbb=Log_Info(:,Log_Info_Variable);% + Log_Info(:,Log_Info_Variable+1);
    plot( aaaa(1:plot_step:end), bbbb(1:plot_step:end), LineColorMarker{i}, 'LineWidth', LineWidth{i} );    
    fprintf('%d  Case File Name (Full Path)  : %s \n', i, Case_Path_Name{i} );
end



xlim([0.0  x_max]);
%ylim([0.2 0.45]);


set(gca,'XTick',[0.2 0.4 0.6 0.8 1 1.2 1.4 1.6 1.8 2 2.2 2.4 2.6 2.8 3. 3.2 3.4 3.6 3.8 4])
% set(gca,'FontSize', Font_Size-2);


xlabel('Time', 'FontSize', Font_Size-2);
box(gca,'on');


AddLegend(Number_Case);


title([Title_Part_1 10 num2str(Resolution_Title),'^3','   \epsilon=',num2str(eps),'   J_{max}=',num2str(J_max_Title),'   \nu=',num2str(nu),'   C_f=',num2str(C_f),'']);
title([Title_Part_1 10 num2str(Resolution_Title),'^3','   J_{max}=',num2str(J_max_Title),'   \nu=',num2str(nu),'   C_f=',num2str(C_f),'']);





 


% '.p0_log'             F i l e     V a r i a b l e s  ::: 
% t,  dt,  j_lev,  nwlt,  nxyz(1),  nxyz(2),  nxyz(3),  eps,  nwlt(j=1),  nwlt(j=2),  nwlt(j=3),  nwlt(j=4),  nwlt(j=5),  nwlt(j=6)

% '.p0_user_stats'      F i l e     V a r i a b l e s  ::: 
%Time          t_eddy           Nwlt    Nwlt/Nmax     Re_taylor     eta/h         <KE>          <DISS_res>    <DISS_SGS>    fracSGS_DISS  epsilon

% '.case_isoturb.log'    F i l e     V a r i a b l e s  ::: 
%Time           tke            Cs             ttl_res_diss   ttl_sgs_diss   gridfilt_mask  testfilt_mask  Psgs_diss      total_Ksgs     eps   total_neg_sgs_diss  backscatter_frac

switch lower(File_Type)
   case {'.p0_user_stats'}
      switch (Log_Info_Variable)
          case (2)
              ylabel('{\tau}_{eddy}');   %'t _e_d_d_y');
          case (3)
              ylabel('N_w_l_t   ( {\Sigma} nwlt    over all  processors )');
          case (4)
              ylabel('N _w_l_t / N _m_a_x');
          case (5)
              ylabel('Re _T_a_y_l_o_r');
          case (6)
              ylabel('eta/h');
          case (7)
              ylabel('<Energy _K_i_n_e_t_i_c>');
          case (8)
              ylabel('<Dissipation _r_e_s>');
          case (9)
              ylabel('<Dissipation _S_G_S>');
          case (10)
              ylabel('Total  FSGSD'); %'fraction Dissipation _S_G_S');
              ylim([0.1 0.5]);
          case (11)
              ylabel('{\epsilon}');
      end
   case {'.case_isoturb.log'}
      switch (Log_Info_Variable)
      %Time           tke            Cs             ttl_res_diss   ttl_sgs_diss   gridfilt_mask  testfilt_mask  Psgs_diss      total_Ksgs     eps   total_neg_sgs_diss  backscatter_frac          
          case (2)
              ylabel('tke');
          case (3)
              ylabel('Cs');
          case (4)
              ylabel('Total  Resolved  Dissipation');
          case (5)
              ylabel('Total  SGS  Dissipation');
          case (6)
              ylabel('gridfilt_mask');
          case (7)
              ylabel('testfilt_mask');
          case (8)
              ylabel('% Dissipation _S_G_S');
          case (9)
              ylabel('total_Ksgs');
          case (10)
              ylabel('{\epsilon}');
          case (11)
              ylabel('Total  Negative  SGS  Dissipation');
          case (12)
              ylabel('Backscatter  Fraction');
      end
   case '.p0_log'
      switch (Log_Info_Variable)
      % t,  dt,  j_lev,  nwlt,  nxyz(1),  nxyz(2),  nxyz(3),  eps,  nwlt(j=1),  nwlt(j=2),  nwlt(j=3),  nwlt(j=4),  nwlt(j=5),  nwlt(j=6),  [pr=0]  NWLT,  GHO,  SEND,  RECV,  TIMER(1),  TIMER(2),  [pr=1]  NWLT,  GHO,  SEND,  RECV,  TIMER(1),  TIMER(2),  [pr=2]  NWLT,  GHO,  SEND,  RECV,  TIMER(1),  TIMER(2),  [pr=3]  NWLT,  GHO,  SEND,  RECV,  TIMER(1),  TIMER(2),  [pr=4]  NWLT,  GHO,  SEND,  RECV,  TIMER(1),  TIMER(2),  [pr=5]  NWLT,  GHO,  SEND,  RECV,  TIMER(1),  TIMER(2),  [pr=6]  NWLT,  GHO,  SEND,  RECV,  TIMER(1),  TIMER(2),  [pr=7]  NWLT,  GHO,  SEND,  RECV,  TIMER(1),  TIMER(2),  [pr=8]  NWLT,  GHO,  SEND,  RECV,  TIMER(1),  TIMER(2),  [pr=9]  NWLT,  GHO,  SEND,  RECV,  TIMER(1),  TIMER(2),  [pr=10]  NWLT,  GHO,  SEND,  RECV,  TIMER(1),  TIMER(2),  [pr=11]  NWLT,  GHO,  SEND,  RECV,  TIMER(1),  TIMER(2),  [pr=12]  NWLT,  GHO,  SEND,  RECV,  TIMER(1),  TIMER(2),  [pr=13]  NWLT,  GHO,  SEND,  RECV,  TIMER(1),  TIMER(2),  [pr=14]  NWLT,  GHO,  SEND,  RECV,  TIMER(1),  TIMER(2),  [pr=15]  NWLT,  GHO,  SEND,  RECV,  TIMER(1),  TIMER(2) 
          case (2)
              ylabel('{\Delta} _t');
          %case (3)
          %    ylabel('Loop  CPU _T_i_m_e');
          case (3)
              ylabel('J _l_e_v_e_l');
          case (4)
              ylabel('nwlt');
          case (5)
              ylabel('nxyz(1)');
          case (6)
              ylabel('nxyz(2)');
          case (7)
              ylabel('nxyz(3)');
          case (8)
              ylabel('{\epsilon}');
          case (9)
              ylabel('nwlt(j=1)');
          case (10)
              ylabel('nwlt(j=2)');
          case (11)
              ylabel('nwlt(j=3)');
          case (12)
              ylabel('nwlt(j=4)');
          case (13)
              ylabel('nwlt(j=5)');
          case (14)
              ylabel('nwlt(j=6)');
          case (20)
              ylabel('main time integration loop   TIMER(2)   [pr=0] ');
      end
   case '..turbstats'
      switch (Log_Info_Variable)
          case (10);      ylabel('Resolved Kinetic Energy');
          case (15);      ylabel('duprime');              
          case (16);      ylabel('skewness du_i/dx_j');
          case (22);      ylabel('viscous dissipation');
          case (29);      ylabel('Large Eddy Turnover Time');
          case (30);      ylabel('Re_\lambda  (Taylor Microscale Reynolds number)');    % title('Taylor Microscale Reynolds number');
      end
   otherwise
      disp('Unknown File_Type.')
end




%h = gcf
%ylabel(gcf,'FontSize', 12);
ylabel_hand = get(gca,'ylabel');
set(ylabel_hand,'fontsize',Font_Size-2);  %'string','X'


if ( File_Type_Number == 2 &&  Log_Info_Variable == 10 )
    plot([0 4],[0.4  0.4],  LineColorMarkerMean{2}, 'LineWidth', 2)
    plot([0 4],[0.3  0.3],  LineColorMarkerMean{3}, 'LineWidth', 2)
    plot([0 4],[0.25 0.25], LineColorMarkerMean{4}, 'LineWidth', 2)
end
if ( File_Type_Number == 2 &&  Log_Info_Variable == 5 )
    %plot([0 3],[0.4  0.4],  ':k', 'LineWidth', 1)
    %plot([0 3],[0.3  0.3],  ':k', 'LineWidth', 1)
    %plot([0 3],[0.25 0.25], ':k', 'LineWidth', 1)
    for i = 2:Number_Case    
        Log_Info = load( Case_Path_Name{i} );
        ReTaylorMean = mean(Log_Info(:,Log_Info_Variable));
        plot( [0 3], [ReTaylorMean, ReTaylorMean], LineColorMarkerMean{i}, 'LineWidth', 1); %LineWidth{i} );    
        fprintf('%d  Case File Name (Full Path)  : %s \n', i, Case_Path_Name{i} );
    end
end
if ( File_Type_Number == 5 &&  Log_Info_Variable == 30 )
    for i = i_start:i_end
        Log_Info = load( Case_Path_Name{i} );
        ReTaylorMean = mean(Log_Info(:,Log_Info_Variable))
        plot( [0 6], [ReTaylorMean, ReTaylorMean], LineColorMarkerMean{i}, 'LineWidth', 1); %LineWidth{i} );    
        fprintf('%d  Case File Name (Full Path)  : %s \n', i, Case_Path_Name{i} );
    end
end









% Plot (Min,Max,Mean)( SGSD/(SGSD+RD) ) 

Plot_MeanFSGSD_FromOutput = 'F';
if (Plot_MeanFSGSD_FromOutput=='T')    
if ( File_Type_Number == 2 &&  Log_Info_Variable == 10 )
for i = 9:9    
   %[Line_Number, message, CPUTime__For_Integration]  = textread( Case_CPUTime__Path_Name{i}, '%u %33c %f' );
    MinMaxMean_FileName{i} = 'D:\wavelet_Simulations\ForcedHomogeneousTurbulence\SCALES__AdpEpsSpatialEvolution\LKM\SGSD\eps=02_06\Forcing11\Goal03\cSCALES_LKM_Nu0.09_Cf6_AdpEpsSp0.2&0.43_Goal0.3.db_tree.mmm';
    [Line_Number, message, Min_FSGSD, Max_FSGSD, Mean_FSGSD]  = textread( MinMaxMean_FileName{i}, '%u %43c %f %f %f' );
    %bbbb=cell2mat( Mean_FSGSD{i} );
    bbbb=Mean_FSGSD;

    Log_Info = load( Case_Path_Name{i} );
    aaaa=Log_Info(:,1);

    plot( aaaa(1:3:960), bbbb(1:3:960), LineColorMarker{i}, 'LineWidth', LineWidth{i} );    
end
end
end



Plot_CorrectFSGSD_FromOutput = 'F';
if (Plot_CorrectFSGSD_FromOutput=='T')    
if ( File_Type_Number == 2 &&  Log_Info_Variable == 10 )
MinMaxMean_FileName{9} = 'D:\wavelet_Simulations\ForcedHomogeneousTurbulence\SCALES__AdpEpsSpatialEvolution\LKM\SGSD\eps=02_06\Forcing11\Goal03\cSCALES_LKM_Nu0.09_Cf6_AdpEpsSp0.2&0.43_Goal0.3.db_tree..post.custom';
%MinMaxMean_FileName{6} = 'D:\wavelet_Simulations\ForcedHomogeneousTurbulence\SCALES__AdpEpsSpatialEvolution\LKM\SGSD\eps=0.2_0.43\Forcing_Factor__AdpEpsSp=400\Psgs_diss_goal=0.25\cSCALES_LKM_Nu0.09_Cf6_AdpEpsSp0.2&0.43_Goal0.25.db_tree..post.custom';
for i = 9:9 %6:3:9     
   %[Line_Number, message, CPUTime__For_Integration]  = textread( Case_CPUTime__Path_Name{i}, '%u %33c %f' );
    Log_Info = load( MinMaxMean_FileName{i} );
    
    aaaa=Log_Info(:,1);
    bbbb=Log_Info(:,6);

    plot( aaaa(1:3:end), bbbb(1:3:end), LineColorMarker{i}, 'LineWidth', LineWidth{i} );    
end
end
end



% figure
% hold on;    
% MinMaxMean_FileName{9} = 'D:\wavelet_Simulations\ForcedHomogeneousTurbulence\SCALES__AdpEpsSpatialEvolution\LKM\SGSD\eps=02_06\Forcing11\Goal03\cSCALES_LKM_Nu0.09_Cf6_AdpEpsSp0.2&0.43_Goal0.3.db_tree..post.custom';
% MinMaxMean_FileName{6} = 'D:\wavelet_Simulations\ForcedHomogeneousTurbulence\SCALES__AdpEpsSpatialEvolution\LKM\SGSD\eps=0.2_0.43\Forcing_Factor__AdpEpsSp=400\Psgs_diss_goal=0.25\cSCALES_LKM_Nu0.09_Cf6_AdpEpsSp0.2&0.43_Goal0.25.db_tree..post.custom';
% MinMaxMean_FileName{3} = 'D:\wavelet_Simulations\ForcedHomogeneousTurbulence\SCALES__AdpEpsSpatialEvolution\LKM\SGSD\eps=0.2_0.43\Forcing_Factor__AdpEpsSp=400\Psgs_diss_goal=0.3\cSCALES_LKM_Nu0.09_Cf6_AdpEpsSp0.2&0.43_Goal0.3.db_tree..post.custom';
% MinMaxMean_FileName{6} = 'D:\wavelet_Simulations\ForcedHomogeneousTurbulence\SCALES__AdpEpsSpatialEvolution\requin.sharcnet.ca\LKM\eps=02_05\Forcing11\Goal03\dt_5e4\cSCALES_LKM_Nu0.09_Cf6_AdpEpsSp0.2&0.43_Goal0.3.db_tree..post.custom';
% for i = 6:6 %3:3:9     
%    %[Line_Number, message, CPUTime__For_Integration]  = textread( Case_CPUTime__Path_Name{i}, '%u %33c %f' );
%     Log_Info = load( MinMaxMean_FileName{i} );
%     
%     aaaa=Log_Info(:,1);
%     bbbb=Log_Info(:,6);
% 
%     plot( aaaa(1:end), bbbb(1:end), LineColorMarker{i}, 'LineWidth', LineWidth{i} );    
% end
% 
% i=7;
% MinMaxMean_FileName{7} = 'D:\wavelet_Simulations\ForcedHomogeneousTurbulence\SCALES__AdpEpsSpatialEvolution\requin.sharcnet.ca\LKM\eps_02_043\Forcing11\Goal025\dt_4e4\volumeAverage.log';
%     Log_Info = load( MinMaxMean_FileName{i} );
%     aaaa=Log_Info(:,1);
%     bbbb=Log_Info(:,2);
%     plot( aaaa(1:end), bbbb(1:end), LineColorMarker{i}, 'LineWidth', LineWidth{i} );    
%     



















else   % if ( strcmp(File_Type, '..avg.spectra') )

    

figure
hold on;    

for i = 1:i_end
    Log_Info = load( Case_Path_Name{i} );
   %plot(   Log_Info(:,1), Log_Info(:,2),                     LineColorMarker{i}, 'LineWidth', LineWidth{i} );
   %loglog( Log_Info(:,1), Log_Info(:,2)./max(Log_Info(:,2)), LineColorMarker{i}, 'LineWidth', LineWidth{i} );
    loglog( Log_Info(:,1), Log_Info(:,2)                    , LineColorMarker{i}, 'LineWidth', LineWidth{i} );
    fprintf('%d  Case File Name (Full Path)  : %s \n', i, Case_Path_Name{i} );
end

pwerlaw_x=[1.5 128];
pwerlaw_y=pwerlaw_x.^(-5/3);
%loglog(pwerlaw_x, pwerlaw_y, ':k')

xlabel('Wavenumber',     'FontSize', Font_Size);
ylabel('Time Averaged   Energy Spectra', 'FontSize', Font_Size);
% ylabel('Time Averaged   Resolved Dissipation  Spectra', 'FontSize', Font_Size);

xlim([0.0 128]);


AddLegend(i_end);


 
 
set(gca,'XScale','log','YScale','log')
box(gca,'on'); 
 
 
end   % if ( strcmp(File_Type, '..avg.spectra') )


































%================================================================================================================================================
%%

if (Plot_PostProcessed_Diss)
    
i = Number_Case+1;

DIR = 'D:\wavelet_Simulations\ForcedHomogeneousTurbulence\SCALES__AdpEpsSpatialEvolution\LKM\SGSD\eps=0.2_0.43\Forcing_Factor__AdpEpsSp=400\';

CaseDIR                 = 'Optimized_1\';
CaseName                = 'cSCALES_LKM_Nu0.09_Cf6_AdpEpsSp0.2&0.43.db_tree..post.custom';
Case_Path_Name{i}       = [DIR CaseDIR  CaseName];  
CaseLegend{i}           = ['Goal=0.4'];
Number_Processors{i}    = 1;
i=i+1;

CaseDIR                 = 'Psgs_diss_goal=0.3\';
CaseName                = 'cSCALES_LKM_Nu0.09_Cf6_AdpEpsSp0.2&0.43_Goal0.3.db_tree..post.custom';
Case_Path_Name{i}       = [DIR CaseDIR  CaseName];  
CaseLegend{i}           = ['Goal=0.3'];
Number_Processors{i}    = 1;
i=i+1;

CaseDIR                 = 'Psgs_diss_goal=0.25\';
CaseName                = 'cSCALES_LKM_Nu0.09_Cf6_AdpEpsSp0.2&0.43_Goal0.25.db_tree..post.custom';
Case_Path_Name{i}       = [DIR CaseDIR  CaseName];  
CaseLegend{i}           = ['Goal=0.25'];
Number_Processors{i}    = 1;
i=i+1;

i_start = Number_Case+1;
i_end   = i-1;

figure;
hold on;
for i = i_start:i_end
    Log_Info = load( Case_Path_Name{i} );
    plot( Log_Info(:,1), Log_Info(:,2), LineColorMarker{i}, 'LineWidth', LineWidth{i} );    
    plot( Log_Info(:,1), Log_Info(:,3), LineColorMarker{i}, 'LineWidth', LineWidth{i} );    
    fprintf('%d  Case File Name (Full Path)  : %s \n', i, Case_Path_Name{i} );
end
title('<DISS-res>   &   <DISS-SGS>');
xlim([0.0 4]);
% ylim([0.2 0.5]);
legend( CaseLegend{Number_Case+1}, CaseLegend{Number_Case+2}, CaseLegend{Number_Case+3});
% legend( CaseLegend{Number_Case+1}, CaseLegend{Number_Case+2});



figure;
hold on;
for i = i_start:i_end
    Log_Info = load( Case_Path_Name{i} );
    %plot( Log_Info(:,1), Log_Info(:,4), LineColorMarker{i}, 'LineWidth', LineWidth{i} );    
    plot( Log_Info(:,1), Log_Info(:,5), LineColorMarker{i}, 'LineWidth', LineWidth{i} );    
    fprintf('%d  Case File Name (Full Path)  : %s \n', i, Case_Path_Name{i} );
end
title('<DISS-SGS/(DISS-res+DISS-SGS)>');
xlim([0.0 4]);
% ylim([0.2 0.5]);
legend( CaseLegend{Number_Case+1}, CaseLegend{Number_Case+2}, CaseLegend{Number_Case+3});
% legend( CaseLegend{Number_Case+1}, CaseLegend{Number_Case+2});



figure;
hold on;
for i = i_start:i_end
    Log_Info = load( Case_Path_Name{i} );
    plot( Log_Info(:,1), Log_Info(:,4), LineColorMarker{i}, 'LineWidth', LineWidth{i} );    
    fprintf('%d  Case File Name (Full Path)  : %s \n', i, Case_Path_Name{i} );
end
title('fracSGS-DISS');
xlim([0.0 4]);
ylim([0.2 0.5]);
plot([0 3],[0.4  0.4],  LineColorMarkerMean{3}, 'LineWidth', 2)
plot([0 3],[0.3  0.3],  LineColorMarkerMean{4}, 'LineWidth', 2)
plot([0 3],[0.25 0.25], LineColorMarkerMean{5}, 'LineWidth', 2)
legend( CaseLegend{Number_Case+1}, CaseLegend{Number_Case+2}, CaseLegend{Number_Case+3});
% legend( CaseLegend{Number_Case+1}, CaseLegend{Number_Case+2});

end    
    

    








%================================================================================================================================================
%%

if (Plot_PostProcessed_Forcing)
    
i = Number_Case+1;

DIR = 'D:\wavelet_Simulations\ForcedHomogeneousTurbulence\SCALES__AdpEpsSpatialEvolution\LKM\SGSD\eps=0.2_0.43\Forcing_Factor__AdpEpsSp=400\';

% CaseDIR                 = 'Optimized_1\';
% CaseName                = 'cSCALES_LKM_Nu0.09_Cf6_AdpEpsSp0.2&0.43.db_tree..post.custom';
% Case_Path_Name{i}       = [DIR CaseDIR  CaseName];  
% CaseLegend{i}           = ['Goal=0.4'];
% Number_Processors{i}    = 1;
% i=i+1;

CaseDIR                 = 'Psgs_diss_goal=0.3\';
CaseName                = 'cSCALES_LKM_Nu0.09_Cf6_AdpEpsSp0.2&0.43_Goal0.3.db_tree..post.custom';
Case_Path_Name{i}       = [DIR CaseDIR  CaseName];  
CaseLegend{i}           = ['Goal=0.3'];
Number_Processors{i}    = 1;
i=i+1;

% CaseDIR                 = 'Psgs_diss_goal=0.25\';
% CaseName                = 'cSCALES_LKM_Nu0.09_Cf6_AdpEpsSp0.2&0.43_Goal0.25.db_tree..post.custom';
% Case_Path_Name{i}       = [DIR CaseDIR  CaseName];  
% CaseLegend{i}           = ['Goal=0.25'];
% Number_Processors{i}    = 1;
% i=i+1;

i_start = Number_Case+1;
i_end   = i-1;

% time          dt             <DISS_res>     <DISS_SGS>     fracSGS_DISS   <DISS_SGS/(DISS_res+DISS_SGS)         (Min,Max,Mean)( SGSD/(SGSD+RD)               (Min,Max,Mean) Forcing_TimeScale             (Min,Max,Mean)Forcing_NormDenom_Min          Forcing_Norm_Denom__av     EpsSpatial_Forcing_TimeScale
figure;
hold on;
for i = i_start:i_end
    Log_Info = load( Case_Path_Name{i} );
    plot( Log_Info(:,1), Log_Info(:,3), LineColorMarker{i}, 'LineWidth', LineWidth{i} );    
    plot( Log_Info(:,1), Log_Info(:,4), LineColorMarker{i}, 'LineWidth', LineWidth{i} );    
    fprintf('%d  Case File Name (Full Path)  : %s \n', i, Case_Path_Name{i} );
end
title('<DISS-res>   &   <DISS-SGS>');
xlim([0.0 4]);
% ylim([0.2 0.5]);
legend('<DISS-res>','<DISS-SGS>');



figure;
hold on;
for i = i_start:i_end
    Log_Info = load( Case_Path_Name{i} );
    %plot( Log_Info(:,1), Log_Info(:,4), LineColorMarker{i}, 'LineWidth', LineWidth{i} );    
    plot( Log_Info(:,1), Log_Info(:,6), LineColorMarker{i}, 'LineWidth', LineWidth{i} );    
    fprintf('%d  Case File Name (Full Path)  : %s \n', i, Case_Path_Name{i} );
end
title('<DISS-SGS/(DISS-res+DISS-SGS)>');
xlim([0.0 4]);
ylim([0.2 0.5]);
%legend( CaseLegend{Number_Case+1}, CaseLegend{Number_Case+2}, CaseLegend{Number_Case+3});
% legend( CaseLegend{Number_Case+1}, CaseLegend{Number_Case+2});



figure;
hold on;
for i = i_start:i_end
    Log_Info = load( Case_Path_Name{i} );
    plot( Log_Info(:,1), Log_Info(:,5), LineColorMarker{i}, 'LineWidth', LineWidth{i} );    
    fprintf('%d  Case File Name (Full Path)  : %s \n', i, Case_Path_Name{i} );
end
title('fracSGS-DISS');
xlim([0.0 4]);
ylim([0.2 0.5]);
plot([0 3],[0.4  0.4],  LineColorMarkerMean{3}, 'LineWidth', 2)
plot([0 3],[0.3  0.3],  LineColorMarkerMean{4}, 'LineWidth', 2)
plot([0 3],[0.25 0.25], LineColorMarkerMean{5}, 'LineWidth', 2)
%legend( CaseLegend{Number_Case+1}, CaseLegend{Number_Case+2}, CaseLegend{Number_Case+3});
% legend( CaseLegend{Number_Case+1}, CaseLegend{Number_Case+2});






% time          dt             <DISS_res>     <DISS_SGS>     fracSGS_DISS   <DISS_SGS/(DISS_res+DISS_SGS)         (Min,Max,Mean)( SGSD/(SGSD+RD)               (Min,Max,Mean) Forcing_TimeScale             (Min,Max,Mean)Forcing_NormDenom_Min          Forcing_Norm_Denom__av     EpsSpatial_Forcing_TimeScale
figure;
hold on;
for i = i_start:i_end
    Log_Info = load( Case_Path_Name{i} );
    plot( Log_Info(:,1), Log_Info(:,7),  'b');%LineColorMarker{i}, 'LineWidth', LineWidth{i} );    
    plot( Log_Info(:,1), Log_Info(:,10), 'g');%LineColorMarker{i}, 'LineWidth', LineWidth{i} );    
    plot( Log_Info(:,1), Log_Info(:,13), 'm');%LineColorMarker{i}, 'LineWidth', LineWidth{i} );    
    fprintf('%d  Case File Name (Full Path)  : %s \n', i, Case_Path_Name{i} );
end
title('min Forcing');
xlim([0.0 4]);
legend('SGSD/(SGSD+RD)','Forcing_TimeScale','Forcing_NormDenom');

figure;
hold on;
for i = i_start:i_end
    Log_Info = load( Case_Path_Name{i} );
    plot( Log_Info(:,1), Log_Info(:,8),  'b');%LineColorMarker{i}, 'LineWidth', LineWidth{i} );    
    plot( Log_Info(:,1), Log_Info(:,11), 'g');%LineColorMarker{i}, 'LineWidth', LineWidth{i} );    
    plot( Log_Info(:,1), Log_Info(:,14), 'm');%LineColorMarker{i}, 'LineWidth', LineWidth{i} );    
    fprintf('%d  Case File Name (Full Path)  : %s \n', i, Case_Path_Name{i} );
end
title('max Forcing');
xlim([0.0 4]);
legend('SGSD/(SGSD+RD)','Forcing_TimeScale','Forcing_NormDenom');

figure;
hold on;
for i = i_start:i_end
    Log_Info = load( Case_Path_Name{i} );
    plot( Log_Info(:,1), Log_Info(:,9),  'b');%LineColorMarker{i}, 'LineWidth', LineWidth{i} );    
    %plot( Log_Info(:,1), Log_Info(:,12), 'g');%LineColorMarker{i}, 'LineWidth', LineWidth{i} );    
    plot( Log_Info(:,1), Log_Info(:,15)/75, 'm');%LineColorMarker{i}, 'LineWidth', LineWidth{i} );    
    fprintf('%d  Case File Name (Full Path)  : %s \n', i, Case_Path_Name{i} );
end
title('mean Forcing');
xlim([0.0 4]);
%legend('SGSD/(SGSD+RD)','Forcing_TimeScale','Forcing_NormDenom');
legend('SGSD/(SGSD+RD)','Forcing_NormDenom');

figure;
hold on;
for i = i_start:i_end
    Log_Info = load( Case_Path_Name{i} );
    plot( Log_Info(:,1), Log_Info(:,7), 'b');%LineColorMarker{i}, 'LineWidth', LineWidth{i} );    
    plot( Log_Info(:,1), Log_Info(:,8), 'g');%LineColorMarker{i}, 'LineWidth', LineWidth{i} );    
    plot( Log_Info(:,1), Log_Info(:,9), 'm');%LineColorMarker{i}, 'LineWidth', LineWidth{i} );    
    fprintf('%d  Case File Name (Full Path)  : %s \n', i, Case_Path_Name{i} );
end
title('max min mean   SGSD/(SGSD+RD) Forcing');
xlim([0.0 4]);
legend('min','max','mean');










figure;
hold on;
for i = i_start:i_end
    Log_Info = load( Case_Path_Name{i} );
    plot( Log_Info(:,1), Log_Info(:,16), LineColorMarker{i}, 'LineWidth', LineWidth{i} );    
    fprintf('%d  Case File Name (Full Path)  : %s \n', i, Case_Path_Name{i} );
end
title('Forcing_Norm_Denom__av');
xlim([0.0 4]);


figure;
hold on;
for i = i_start:i_end
    Log_Info = load( Case_Path_Name{i} );
    plot( Log_Info(:,1), Log_Info(:,17), LineColorMarker{i}, 'LineWidth', LineWidth{i} );    
    fprintf('%d  Case File Name (Full Path)  : %s \n', i, Case_Path_Name{i} );
    Time_Goal3 = Log_Info(:,1);
    TimeScale_Goal3  = Log_Info(:,17);
    dt_Goal3   = Log_Info(:,2);
end
title('EpsSpatial_Forcing__TimeScale');
xlim([0.0 4]);





figure;
hold on;
for i = i_start:i_end
    Log_Info = load( Case_Path_Name{i} );
    %plot( Log_Info(:,1), Log_Info(:,17), LineColorMarker{i}, 'LineWidth', LineWidth{i} );    
    %plot( Log_Info(:,1), Log_Info(:,16), LineColorMarker{i}, 'LineWidth', LineWidth{i} );    
    fprintf('%d  Case File Name (Full Path)  : %s \n', i, Case_Path_Name{i} );
    xxx = Log_Info(:,1);
    yy1 = Log_Info(:,17);
    yy2 = Log_Info(:,16);
    [AX,H1,H2] = plotyy(xxx,yy1,xxx,yy2,'plot');
end
title('');
%xlim([0.0 4]);

%set(get(AX(1),'Ylabel'),'String','{\taw}_{\epsilon}') 
%set(get(AX(1),'Ylabel'),'{\taw}') 
ylabel('{\tau}_{\epsilon}^{-1}');

set(get(AX(2),'Ylabel'),'String','TAF') 

% set(get(AX(2),'YColor',[0 0.5 0.1]); %,'XColor','r')
% set(get(H1,'Color'),'String','r'); 
% set(get(AX(2),'Color'),'String','m'); 

xlabel('Time')  %('Time (\musec)') 

% set(H1,'LineStyle','--');%,'Color','r')
% set(H2,'LineStyle',':')







Figure_TAF_tau = figure;
hold on;
for i = i_start:i_end
    Log_Info = load( Case_Path_Name{i} );
    %plot( Log_Info(:,1), Log_Info(:,17), LineColorMarker{i}, 'LineWidth', LineWidth{i} );    
    %plot( Log_Info(:,1), Log_Info(:,16), LineColorMarker{i}, 'LineWidth', LineWidth{i} );    
    fprintf('%d  Case File Name (Full Path)  : %s \n', i, Case_Path_Name{i} );
    xxx = Log_Info(:,1);
    yy1 = Log_Info(:,17);
    yy2 = Log_Info(:,16);
end

axes1 = axes('Parent',Figure_TAF_tau,'YColor',[0 0 1]);
% xlim(axes1,[0 3.5]);
% ylim(axes1,[100 180]);
box(axes1,'on');
hold(axes1,'all');
plot(xxx,yy1,'Parent',axes1);
ylabel('{\tau}_{\epsilon}','Color',[0 0 1]);


axes2 = axes('Parent',Figure_TAF_tau,'YAxisLocation','right','YColor','red','Color','none');
% xlim(axes2,[0 3.5]);
% ylim(axes2,[25000 65000]);
hold(axes2,'all');
plot(xxx,yy2,'Parent',axes2,'Color','red');
ylabel('TAF','VerticalAlignment','cap','Color','red');%[0 0.5 0]);












figure;
hold on;
DIR         = 'D:\wavelet_Simulations\ForcedHomogeneousTurbulence\SCALES__AdpEpsSpatialEvolution\';
CaseDIR     = 'LKM\SGSD\eps=0.2_0.43\Forcing_Factor__AdpEpsSp=400\Psgs_diss_goal=0.3\';
CaseName    = 'SCALES_LKM_Nu0.09_Cf6_AdpEpsSp0.2&0.43_Goal0.3.db_tree';
CaseName    = [DIR CaseDIR  CaseName '._user_stats'];  
  
Log_Info      = load( CaseName );
time_goal3_2  = Log_Info(:,1);
teddy_goal3   = Log_Info(:,2);
plot( time_goal3_2(1:end),     teddy_goal3(1:end),      'b' );    
plot( time_goal3_2(1:end),   3*teddy_goal3(1:end),      'b' );    
plot( Time_Goal3(1:end),   400*dt_Goal3(1:end),         'r' );    
plot( Time_Goal3(1:end),       dt_Goal3(1:end).*TimeScale_Goal3(1:end),  'g' );    
plot( Time_Goal3(1:end),   130*dt_Goal3(1:end),         'm' );    
legend('t_{eddy}','3 * t_{eddy}','400 * {\Delta} _t','{\Delta} _t * T_{epsilon}','100 * {\Delta} _t');







figure;
hold on;
DIR         = 'D:\wavelet_Simulations\ForcedHomogeneousTurbulence\SCALES__AdpEpsSpatialEvolution\';
CaseDIR     = 'LKM\SGSD\eps=0.2_0.43\Forcing_Factor__AdpEpsSp=400\Psgs_diss_goal=0.3\';
CaseName    = 'SCALES_LKM_Nu0.09_Cf6_AdpEpsSp0.2&0.43_Goal0.3.db_tree';
CaseName    = [DIR CaseDIR  CaseName '._user_stats'];  
  
Log_Info      = load( CaseName );
time_goal3_2  = Log_Info(:,1);
teddy_goal3   = Log_Info(:,2);
plot( Time_Goal3(1:end),  dt_Goal3(1:end),         'r' );    
legend('{\Delta} _t');


end    
    

    







end






    
    

%%
function AddLegend(Number_Legend)
global CaseLegend
global Font_Size


switch Number_Legend
    case (3)
            LL=legend(CaseLegend{1}, CaseLegend{2}, CaseLegend{3},'Location','NorthWest'); 
    case (4)
            LL=legend(CaseLegend{1}, CaseLegend{2}, CaseLegend{3}, CaseLegend{4},'Location','SouthWest'); 
    case (5)
            LL=legend(CaseLegend{1}, CaseLegend{2}, CaseLegend{3}, CaseLegend{4}, CaseLegend{5},'Location','NorthEast'); 
    case (6)
            LL=legend(CaseLegend{1}, CaseLegend{2}, CaseLegend{3}, CaseLegend{4}, CaseLegend{5}, CaseLegend{6},'Location','NorthEast'); 
    case (7)
            LL=legend(CaseLegend{1}, CaseLegend{2}, CaseLegend{3}, CaseLegend{4}, CaseLegend{5}, CaseLegend{6}, CaseLegend{7}); 
    case (8)
            LL=legend(CaseLegend{1}, CaseLegend{2}, CaseLegend{3}, CaseLegend{4}, CaseLegend{5}, CaseLegend{6}, CaseLegend{7}, CaseLegend{8}); 
    case (9)
            LL=legend(CaseLegend{1}, CaseLegend{2}, CaseLegend{3}, CaseLegend{4}, CaseLegend{5}, CaseLegend{6}, CaseLegend{7}, CaseLegend{8}, CaseLegend{9}); 
    case (10)
            LL=legend(CaseLegend{1}, CaseLegend{2}, CaseLegend{3}, CaseLegend{4}, CaseLegend{5}, CaseLegend{6}, CaseLegend{7}, CaseLegend{8}, CaseLegend{9}, CaseLegend{10} ); 
    case (11)
            LL=legend(CaseLegend{1}, CaseLegend{2}, CaseLegend{3}, CaseLegend{4}, CaseLegend{5}, CaseLegend{6}, CaseLegend{7}, CaseLegend{8}, CaseLegend{9}, CaseLegend{10}, CaseLegend{11} ); 
    case (12)
            LL=legend(CaseLegend{1}, CaseLegend{2}, CaseLegend{3}, CaseLegend{4}, CaseLegend{5}, CaseLegend{6}, CaseLegend{7}, CaseLegend{8}, CaseLegend{9}, CaseLegend{10}, CaseLegend{11}, CaseLegend{12} ); 
    case (13)
            LL=legend(CaseLegend{1}, CaseLegend{2}, CaseLegend{3}, CaseLegend{4}, CaseLegend{5}, CaseLegend{6}, CaseLegend{7}, CaseLegend{8}, CaseLegend{9}, CaseLegend{10}, CaseLegend{11}, CaseLegend{12}, CaseLegend{13} ); 
    case (14)
            LL=legend(CaseLegend{1}, CaseLegend{2}, CaseLegend{3}, CaseLegend{4}, CaseLegend{5}, CaseLegend{6}, CaseLegend{7}, CaseLegend{8}, CaseLegend{9}, CaseLegend{10}, CaseLegend{11}, CaseLegend{12}, CaseLegend{13}, CaseLegend{14} ); 
    case (15)
            LL=legend(CaseLegend{1}, CaseLegend{2}, CaseLegend{3}, CaseLegend{4}, CaseLegend{5}, CaseLegend{6}, CaseLegend{7}, CaseLegend{8}, CaseLegend{9}, CaseLegend{10}, CaseLegend{11}, CaseLegend{12}, CaseLegend{13}, CaseLegend{14}, CaseLegend{15} ); 
    case (16)
            LL=legend(CaseLegend{1}, CaseLegend{2}, CaseLegend{3}, CaseLegend{4}, CaseLegend{5}, CaseLegend{6}, CaseLegend{7}, CaseLegend{8}, CaseLegend{9}, CaseLegend{10}, CaseLegend{11}, CaseLegend{12}, CaseLegend{13}, CaseLegend{14}, CaseLegend{15}, CaseLegend{16} ); 
    case (22)
            LL=legend(CaseLegend{1}, CaseLegend{2}, CaseLegend{3}, CaseLegend{4}, CaseLegend{5}, CaseLegend{6}, CaseLegend{7}, CaseLegend{8}, CaseLegend{9}, CaseLegend{10}, CaseLegend{11}, CaseLegend{12}, CaseLegend{13}, CaseLegend{14}, CaseLegend{15}, CaseLegend{16}, CaseLegend{17}, CaseLegend{18}, CaseLegend{19}, CaseLegend{20}, CaseLegend{21}, CaseLegend{22} ); 
    otherwise
            disp('Unknown Legend Number')
end

set(LL, 'FontSize',Font_Size,'Box','off'); %'FontName','Times New-Roman'

%legend('Variable \epsilon','Constant \epsilon');
%legend('\fontsize{16} {\epsilon}       \fontsize{11} Np=Nu=2','\fontsize{16} {|| f ||}_{\infty}       \fontsize{11} Np=Nu=2', '\fontsize{16} {\epsilon}       \fontsize{11} Np=3 Nu=2','\fontsize{16} {|| f ||}_{\infty}       \fontsize{11} Np=3 Nu=2',  '\fontsize{16} {\epsilon}       \fontsize{11} Np=4 Nu=4','\fontsize{16} {|| f ||}_{\infty}       \fontsize{11} Np=4 Nu=4')

end
