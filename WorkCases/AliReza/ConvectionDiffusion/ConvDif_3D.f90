MODULE user_case
  ! CASE   2/3D   2-variables (independent)   Convection+Diffusion   
  ! Periodic in all derections

  
  USE precision
  USE wlt_vars
  USE wavelet_filters_mod
  USE elliptic_mod
  USE elliptic_vars
  USE wlt_trns_mod
  USE wlt_trns_vars
  USE wlt_trns_util_mod
  USE io_3d_vars
  USE util_mod
  USE util_vars
  USE share_consts
  USE pde
  USE sizes
  USE share_kry
  USE vector_util_mod
  USE field
  USE input_file_reader



  !
  ! case specific variables
  !
  INTEGER n_var_pressure  ! start of pressure in u array

  REAL (pr) :: nu     ! kinematic viscosity (1/Re)heat conduction
  REAL (pr) :: T_0    ! Temperature of the top wall + initial temperature
  REAL (pr) :: T_1    ! Temperature of the Left   wall  (Xmin)                        
  REAL (pr) :: T_2    ! Temperature of the Right  wall  (Xmax)                        
  REAL (pr) :: T_3    ! Temperature of the Bottom wall  (Ymin)                        
  REAL (pr) :: T_4    ! Temperature of the Top    wall  (Ymax)                        
  REAL (pr) :: T_5    ! Temperature of the Front  wall  (Zmin)                        
  REAL (pr) :: T_6    ! Temperature of the Back   wall  (Zmax)                        
  INTEGER, PARAMETER :: Tdim = 1



  INTEGER                   :: RigidBody_Type         ! Rigid Body Type  
                                                      ! (1: Sphere     2:  Array of Quadrilaterals) 

  REAL (pr)                 :: omega(3)


  REAL (pr)                 :: X_Center__RigidBody
  REAL (pr)                 :: Y_Center__RigidBody
  REAL (pr)                 :: Z_Center__RigidBody
  REAL (pr)                 :: Radius__RigidBody


  INTEGER                   :: RigidBody_Number
  REAL (pr)                 :: Radius__RigidBody_Array, Length__RigidBody, Width__RigidBody, axis_a, axis_b, axis_c
  REAL (pr), ALLOCATABLE    :: X_min__RigidBody(:), Y_min__RigidBody(:), X_max__RigidBody(:), Y_max__RigidBody(:)
 !REAL (pr)                 :: pi = 3.1415926535897932384626433832795_pr


CONTAINS


 




  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  !
  ! In user_setup_pde() we setup how many variables are integrated, which are interpolated
  ! to the next times step and if any exeact solution exists to compare to during the run.
  ! We also set the variable names that are used when the result files are written.
  !
  ! The following variables must be setup in this routine:
  !
  !
  ! n_integrated     ! first n_integrated eqns will be acted on for time integration
  ! n_var_additional ! 
  ! n_var
  !
  !
  !
  !
  SUBROUTINE  user_setup_pde ( VERB ) 
    USE parallel
    IMPLICIT NONE
  
    LOGICAL,   OPTIONAL  ::  VERB         ! print debug info
    INTEGER              ::  i


    IF (par_rank.EQ.0) THEN
       PRINT * ,''
       PRINT *, '****************************Setting up PDE***********************'
       PRINT * ,'CASE:   2/3D   2-variables (independent)   Convection+Diffusion  '
       PRINT * ,'        Periodic in all directions  '
       PRINT *, '*****************************************************************'
    END IF

    n_integrated       = 2                                       ! T1,T2         :: # of equations to solve without SGS model
    
   !n_time_levels      = 1                                       ! # time levels

   !n_var_time_levels  = n_time_levels * n_integrated            ! Number of equations (2D velocity at two time levels)

    n_var_additional   = 1                                       ! heat flux pressure
    
    n_var              = n_integrated + n_var_additional         ! Total number of variables

    n_var_exact        = 0                                       ! No exact solution 

    n_var_pressure     = n_integrated + 1                        ! pressure

    !--Allocate logical arrays 
    !  Must be done after setting number of different types of variables
    !  and before setting logical variable mapping arrays
    !
    CALL alloc_variable_mappings


    !
    ! Fill in variable names for default variables for integration meth 2
    ! These may be changed in the call to the user specified routine
    !  user_setup_pde()
    !
    ! In integrated variables (3D)
    IF( dim <= 3 ) THEN
       WRITE (u_variable_names(1), u_variable_names_fmt) 'T1  '
       WRITE (u_variable_names(2), u_variable_names_fmt) 'T2  '
       WRITE (u_variable_names(3), u_variable_names_fmt) 'pressure '
    END IF

    !
    ! setup logical variable mapping arrays
    ! This defined which variables are used for adaptation, Which need to be interpololated to the new grid at 
    ! each time step, which are saved, and for which we have an exect solutiuon we want to check at each time step
    !


    ! setup which components we will base grid adaptation on.
    ! Adapt on the 3 velocity components
    !
    n_var_adapt = .FALSE. !intialize
    n_var_adapt(1:n_integrated,0)     = .TRUE. !--Initially adapt on integrated variables at first time level

    n_var_adapt(1:n_integrated,1)     = .TRUE. !--After first time step adapt on 

    !--integrated variables at first time level

    !Variables that need to be interpoleted to new adapted grid in initial grid adaptation
    n_var_interpolate                   = .FALSE. !intialize
    n_var_interpolate(1:n_integrated,0) = .TRUE.  
    
    !Variables that need to be interpoleted to new adapted grid at each time step
    n_var_interpolate(1:n_integrated,1) = .TRUE. 

    !
    ! setup which components we have an exact solution for
    !
    n_var_exact_soln = .FALSE. !intialize
    n_var_exact_soln(1:n_integrated,0:1) = .TRUE.                                    ! ########
    n_var_exact_soln(1:n_integrated,0:1) = .FALSE.

    !
    ! setup which variables we will save the solution
    !
    n_var_save = .FALSE. !intialize 
    n_var_save(1:n_var) = .TRUE. ! save all for restarting code

    !
    !--Time level counters for NS integration
    !   Used for integration meth2
    ! n0 is current time step
    ! n1 is t_n-1 time step
    ! n2 is t_n-2 time step
    !
    ! n0 = 1; n1 = n0+dim; n2 = n1+dim

    !
    ! Set the maximum number of components for which we have an exact solution
    !
    ! n_var_exact = MAX( COUNT(n_var_exact_soln(:,0)),COUNT(n_var_exact_soln(:,1)) )     ! #######
      n_var_exact = 0

    !
    ! Setup a scaleCoeff array of we want to tweak the scaling of a variable
    ! ( if scaleCoeff < 1.0 then more points will be added to grid 
    !
    ALLOCATE ( scaleCoeff(1:n_var), Umn(1:n_var) )
    scaleCoeff = 1.0_pr


    Umn = 0.0_pr !set up here if mean quantities are not zero and used in scales or equation


    IF (par_rank.EQ.0) THEN
       PRINT *, 'n_integrated       = ',  n_integrated 
      !PRINT *, 'n_time_levels      = ',  n_time_levels
      !PRINT *, 'n_var_time_levels  = ',  n_var_time_levels 
       PRINT *, 'n_var              = ',  n_var 
       PRINT *, 'n_var_exact        = ',  n_var_exact 
       PRINT *, '*************************Variable Names**************************'
       DO i = 1,n_var
          WRITE (*, u_variable_names_fmt) u_variable_names(i)
       END DO
       PRINT *, '*****************************************************************'
    END IF

  END SUBROUTINE  user_setup_pde

  
  
  
  
  
  
  
  
  
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  !
  ! Set the exact solution for comparison to the simulated solution
  !
  ! u                          -  array to fill in the exact solution
  ! nlocal                     -  number of active wavelets   
  ! ne_local                   -  total number of equations
  ! t                          -  time of current time step 
  ! l_n_var_exact_soln_index   -  index into the elements of u for which we need to find the exact solution
  !                            
  SUBROUTINE  user_exact_soln (u, nlocal,  t_local, l_n_var_exact_soln)  
    USE parallel

    IMPLICIT NONE
    
    INTEGER ,                                  INTENT (IN)     ::  nlocal
    REAL (pr),                                 INTENT (IN)     ::  t_local
    REAL (pr), DIMENSION (nlocal,n_var_exact), INTENT (INOUT)  ::  u
    LOGICAL ,                                  INTENT (IN)     ::  l_n_var_exact_soln(n_var)
    REAL (pr)                                                  ::  t_zero
    INTEGER                                                    ::  i
    DOUBLE PRECISION                                           ::  DERF
    EXTERNAL                                                   ::  DERF


    IF (par_rank.EQ.0) PRINT *, 'Error IN [user_exact_soln]::    No Exact Solution Available at this time'
    IF (par_rank.EQ.0) PRINT *, 'Exiting...'
    CALL parallel_finalize; STOP
   

  END SUBROUTINE  user_exact_soln










  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  SUBROUTINE user_initial_conditions (u_in, nlocal, ne_local, t_local, scl, scl_fltwt, iter)
    IMPLICIT NONE
    
    INTEGER,                                INTENT (IN)     ::  nlocal, ne_local
    INTEGER  ,                              INTENT (INOUT)  ::  iter                   ! iteration of call while adapting initial grid
    REAL (pr), DIMENSION (nlocal,ne_local), INTENT (INOUT)  ::  u_in
    REAL (pr)                                               ::  scl(1:n_var),scl_fltwt
    REAL (pr),                              INTENT (IN)     ::  t_local
    
    REAL (pr)                                               ::  t_zero, teta, x_rotated, y_rotated, z_rotated, X_center, Y_center, Z_center
    INTEGER                                                 ::  i, k
    DOUBLE PRECISION                                        ::  DERF
    EXTERNAL                                                ::  DERF


    
    IF ( RigidBody_Type == 1 ) THEN  
    
       IF       ( dim == 2 )  THEN
          DO i =1, nlocal       
             IF ( ((x(i,1)-X_Center__RigidBody)**2 + (x(i,2)-Y_Center__RigidBody)**2) <= Radius__RigidBody**2 ) THEN
                    u_in(i,1) = 1
             ELSE
                    u_in(i,1) = 1.1
             END IF
          END DO
       ELSE IF  ( dim == 3 )  THEN
          DO i =1, nlocal       
             IF ( ((x(i,1)-X_Center__RigidBody)**2 + (x(i,2)-Y_Center__RigidBody)**2 + (x(i,3)-Z_Center__RigidBody)**2) <= Radius__RigidBody**2 ) THEN
                   u_in(i,1) = 1
             ELSE
                    u_in(i,1) = 1.1
             END IF
          END DO
       END IF
       
    END IF
    





    IF ( RigidBody_Type == 2 ) THEN  

       IF( .not. ALLOCATED(X_min__RigidBody) )    ALLOCATE (X_min__RigidBody(1:RigidBody_Number))
       IF( .not. ALLOCATED(Y_min__RigidBody) )    ALLOCATE (Y_min__RigidBody(1:RigidBody_Number))
       IF( .not. ALLOCATED(X_max__RigidBody) )    ALLOCATE (X_max__RigidBody(1:RigidBody_Number))
       IF( .not. ALLOCATED(Y_max__RigidBody) )    ALLOCATE (Y_max__RigidBody(1:RigidBody_Number))
    
    
       Do k=1, RigidBody_Number
          teta = (k-1) * 2*pi / RigidBody_Number
          X_min__RigidBody(k) = Radius__RigidBody_Array * cos(teta)
          Y_min__RigidBody(k) = Radius__RigidBody_Array * sin(teta)
          X_max__RigidBody(k) = X_min__RigidBody(k) + Length__RigidBody
          Y_max__RigidBody(k) = Y_min__RigidBody(k) + Width__RigidBody
          WRITE(*,*) k, X_min__RigidBody(k), Y_min__RigidBody(k), X_max__RigidBody(k), Y_max__RigidBody(k)
       END DO  
       
       PRINT *, X_min__RigidBody, Y_min__RigidBody, X_max__RigidBody, Y_max__RigidBody 
    
    
       u_in(:,1) = 1.1   
       IF       ( dim == 2 )  THEN
          Do k=1, RigidBody_Number
             DO i =1, nlocal       
                IF ( (x(i,1)>=X_min__RigidBody(k)) .AND. (x(i,2)>=Y_min__RigidBody(k)) .AND. (x(i,1)<=X_max__RigidBody(k)) .AND. (x(i,2)<=Y_max__RigidBody(k)) .AND. (u_in(i,1)==1.1) ) THEN
                    u_in(i,1) = 1
               !ELSE
               !    u_in(i,1) = 1.1
                END IF      
             END DO
          END DO
       ELSE IF  ( dim == 3 )  THEN
       END IF      
       
    END IF
    





    IF ( RigidBody_Type == 3 ) THEN  

       u_in(:,1) = 1.1   
       
       IF       ( dim == 2 )  THEN
          X_center = Radius__RigidBody_Array
          Y_center = 0.0_pr
          Do k=1, RigidBody_Number
             teta = (k-1) * 2*pi / RigidBody_Number
            !X_center = Radius__RigidBody_Array * cos(teta)
            !Y_center = Radius__RigidBody_Array * sin(teta)
            !X_center = X_center*cos(teta) - Y_center*sin(teta)
            !Y_center = X_center*sin(teta) + Y_center*cos(teta)
             DO i =1, nlocal  
                x_rotated = x(i,1)*cos(teta) - x(i,2)*sin(teta)
                y_rotated = x(i,1)*sin(teta) + x(i,2)*cos(teta)
                IF ( (( ((x_rotated - X_center)/axis_a)**2 + ((y_rotated - Y_center)/axis_b)**2 )<=1)  .AND. (u_in(i,1)==1.1) ) THEN
                    u_in(i,1) = 1
               !ELSE
               !    u_in(i,1) = 1.1
                END IF      
             END DO
          END DO
       ELSE IF  ( dim == 3 )  THEN
          X_center = Radius__RigidBody_Array
          Y_center = 0.0_pr
          Z_center = 0.0_pr
          Do k=1, RigidBody_Number
             teta = (k-1) * 2*pi / RigidBody_Number
             DO i =1, nlocal  
                x_rotated = x(i,1)*cos(teta) - x(i,2)*sin(teta)
                y_rotated = x(i,1)*sin(teta) + x(i,2)*cos(teta)
                z_rotated = x(i,3)
                IF ( (( ((x_rotated - X_center)/axis_a)**2 + ((y_rotated - Y_center)/axis_b)**2 + ((z_rotated - Z_center)/axis_c)**2 )<=1)  .AND. (u_in(i,1)==1.1) ) THEN
                    u_in(i,1) = 1
               !ELSE
               !    u_in(i,1) = 1.1
                END IF      
             END DO
          END DO
       END IF      
       
    END IF




    
    u_in(:,2) = u_in(:,1)

    !t_zero = 0.0_pr
    !IF(t_local == 0) t_zero = 1.0e-12_pr
    !DO i =1, nlocal
    !   u(i,1) = T_1 - (T_1 - T_0)*DERF( (x(i,Tdim) - xyzlimits(1,Tdim))/SQRT(4.0_pr*nu*(t_local+t_zero)) ) &
    !           /DERF( (xyzlimits(2,Tdim) - xyzlimits(1,Tdim))/SQRT(4.0_pr*nu*t_local) )
    !   u(i,2) = u(i,1)
    !END DO

  END SUBROUTINE user_initial_conditions










  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  !--********************************  
  ! Arguments
  ! u         - field on adaptive grid
  ! nlocal      - number of active points
  ! ne_local       - number of equations
  ! t         - current time
  !
  ! Global variables used
  ! ifn       - number of equations
  ! x()
  ! xO
  ! y0
  !--******************************** 
  SUBROUTINE user_algebraic_BC (Lu, u, nlocal, ne_local, jlev, meth)
    !--Defines boundary condition type
    IMPLICIT NONE

    INTEGER , INTENT (IN)                                   ::  jlev, meth, ne_local, nlocal
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (INOUT)  ::  Lu
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (IN)     ::  u

    INTEGER                                     ::  i, ie, ii, shift
    REAL (pr), DIMENSION (ne_local,nlocal,dim)  ::  du                         ! Uncomment if Neuman BC are used, need to call         ! #### Neuman BC
    REAL (pr), DIMENSION (ne_local,nlocal,dim)  ::  d2u                        ! Uncomment if Neuman BC are used, need to call         ! #### Neuman BC
    INTEGER                                     ::  face_type, nloc
    INTEGER, DIMENSION(0:dim)                   ::  i_p_face
    INTEGER, DIMENSION(dim)                     ::  face
    INTEGER, DIMENSION(nwlt)                    ::  iloc



    !BC for both 2D and 3D cases

    IF ( prd(1) == 0 ) THEN     
    !PRINT *, 'Periodicity   ::  ',    prd     
    PRINT *, 'Non Periodic BC ..... algebraic_BC'


    !if Neuman BC are used, need to call                                                                                              ! #### Neuman BC
    CALL c_diff_fast (u, du, d2u, jlev, nlocal, meth, 10, ne_local, 1, ne_local)

    DO ie = 1, ne_local
       shift = nlocal*(ie-1)
       !--Go through all Boundary points that are specified
       i_p_face(0) = 1
       DO i=1,dim
          i_p_face(i) = i_p_face(i-1)*3
       END DO
       DO face_type = 0, 3**dim - 1
          face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
          IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
             CALL get_all_indices_by_face (face_type, jlev, nloc, iloc)
             IF(nloc > 0 ) THEN 

                IF     ( face(1) == -1 ) THEN                          ! Left   faces (Xmin)
                   Lu(shift+iloc(1:nloc)) = u(shift+iloc(1:nloc))                          !Dirichlet conditions
!                  Lu(shift+iloc(1:nloc)) = du(ie,iloc(1:nloc),2)                          !Neuman conditions
                ELSE IF( face(1) == 1 )  THEN                          ! Right  faces (Xmax)
!                  Lu(shift+iloc(1:nloc)) = u(shift+iloc(1:nloc))                          !Dirichlet conditions
                   Lu(shift+iloc(1:nloc)) = du(ie,iloc(1:nloc),1)                          !Neuman conditions
                ELSE IF( face(2) == -1 ) THEN                          ! Bottom faces (Ymin) 
                   Lu(shift+iloc(1:nloc)) = u(shift+iloc(1:nloc))                          !Dirichlet conditions      
!                  Lu(shift+iloc(1:nloc)) = du(ie,iloc(1:nloc),2)                          !Neuman conditions
                ELSE IF( face(2) == 1 )  THEN                          ! Top    faces (Ymax) 
!                  Lu(shift+iloc(1:nloc)) = u(shift+iloc(1:nloc))                          !Dirichlet conditions      
                   Lu(shift+iloc(1:nloc)) = du(ie,iloc(1:nloc),2)                          !Neuman conditions
!                ELSE IF( ALL( face == (/-1,-1/) ) ) THEN              ! Lower-Left  Corner (Xmin & Ymin) 
!                   Lu(shift+iloc(1:nloc)) = u(shift+iloc(1:nloc))                         !Dirichlet conditions      
!                   Lu(shift+iloc(1:nloc)) = du(ie,iloc(1:nloc),2)                         !Neuman conditions
!                ELSE IF( ALL( face == (/1,-1/) ) ) THEN               ! Lower-Right Corner (Xmax & Ymin) 
!                   Lu(shift+iloc(1:nloc)) = u(shift+iloc(1:nloc))                         !Dirichlet conditions      
!                   Lu(shift+iloc(1:nloc)) = du(ie,iloc(1:nloc),2)                         !Neuman conditions
!                ELSE                                                   ! Xmax & Ymax & Zmin & Zmax 
!                    Lu(shift+iloc(1:nloc)) = du(ie,iloc(1:nloc),2)                         !Neuman conditions
                END IF                                                    


!                IF( face(Tdim) == -1  ) THEN                          ! Xmin face (entire face) 
!                   Lu(shift+iloc(1:nloc)) = u(shift+iloc(1:nloc))         !Dirichlet conditions
!!$                Lu(shift+iloc(1:nloc)) = du(ie,iloc(1:nloc),2)          !Neuman conditions
!                ELSE IF( face(Tdim) == 1  ) THEN                      ! Xmax face (entire face) 
!                   Lu(shift+iloc(1:nloc)) = u(shift+iloc(1:nloc))         !Dirichlet conditions
!!$                Lu(iloc(1:nloc),ie) = du(ie,iloc(1:nloc),2)             !Neuman conditions
!                END IF
!                IF( face(2) == -1  ) THEN                             ! Ymin face (entire face)        
!                   Lu(shift+iloc(1:nloc)) = u(shift+iloc(1:nloc))         !Dirichlet conditions        
!!$                Lu(shift+iloc(1:nloc)) = du(ie,iloc(1:nloc),2)          !Neuman conditions           
!                END IF                                                                                 

             END IF
          END IF
       END DO
    END DO

    END IF   ! IF ( prd(1) == 0 )


  END SUBROUTINE user_algebraic_BC










  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  SUBROUTINE user_algebraic_BC_diag (Lu_diag, nlocal, ne_local, jlev, meth)
    !--Defines boundary condition type
    IMPLICIT NONE

    INTEGER , INTENT (IN)                                   ::  jlev, meth, ne_local, nlocal
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (INOUT)  ::  Lu_diag

    INTEGER                            ::  i, ie, ii, shift
    REAL (pr), DIMENSION (nlocal,dim)  ::  du, d2u                          ! Uncomment if Neuman BC are used, need to call         ! #### Neuman BC
    INTEGER                            ::  face_type, nloc
    INTEGER, DIMENSION(0:dim)          ::  i_p_face
    INTEGER, DIMENSION(dim)            ::  face
    INTEGER, DIMENSION(nwlt)           ::  iloc
    


    !BC for both 2D and 3D cases

    IF ( prd(1) == 0 ) THEN     
    !PRINT *, 'Periodicity   ::  ',    prd     
    PRINT *, 'Non Periodic BC ..... algebraic_BC'


    !if Neuman BC are used, need to call 
    CALL c_diff_diag ( du, d2u, jlev, nlocal, meth, meth, 10)

    DO ie = 1, ne_local
       shift = nlocal*(ie-1)
       !--Go through all Boundary points that are specified
       i_p_face(0) = 1
       DO i=1,dim
          i_p_face(i) = i_p_face(i-1)*3
       END DO
       DO face_type = 0, 3**dim - 1
          face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
          IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
             CALL get_all_indices_by_face (face_type, jlev, nloc, iloc)
             IF(nloc > 0 ) THEN 

                IF     ( face(1) == -1 ) THEN                          ! Left   faces (Xmin)
                   Lu_diag(shift+iloc(1:nloc)) = 1.0_pr                                    !Dirichlet conditions
!                  Lu_diag(shift+iloc(1:nloc)) = du(iloc(1:nloc),2)                        !Neuman conditions
                ELSE IF( face(1) == 1 )  THEN                          ! Right  faces (Xmax)
!                  Lu_diag(shift+iloc(1:nloc)) = 1.0_pr                                    !Dirichlet conditions
                   Lu_diag(shift+iloc(1:nloc)) = du(iloc(1:nloc),1)                        !Neuman conditions
                ELSE IF( face(2) == -1 ) THEN                          ! Bottom faces (Ymin) 
                   Lu_diag(shift+iloc(1:nloc)) = 1.0_pr                                    !Dirichlet conditions      
!                  Lu_diag(shift+iloc(1:nloc)) = du(iloc(1:nloc),2)                        !Neuman conditions
                ELSE IF( face(2) == 1 )  THEN                          ! Top    faces (Ymax) 
!                  Lu_diag(shift+iloc(1:nloc)) = 1.0_pr                                    !Dirichlet conditions      
                   Lu_diag(shift+iloc(1:nloc)) = du(iloc(1:nloc),2)                        !Neuman conditions
!               ELSE IF( ALL( face == (/-1,-1/) ) ) THEN               ! Lower-Left  Corner (Xmin & Ymin) 
!                  Lu_diag(shift+iloc(1:nloc)) = 1.0_pr                                    !Dirichlet conditions      
!                  Lu_diag(shift+iloc(1:nloc)) = du(iloc(1:nloc),2)                        !Neuman conditions
!               ELSE IF( ALL( face == (/1,-1/) ) ) THEN                ! Lower-Right Corner (Xmax & Ymin) 
!                  Lu_diag(shift+iloc(1:nloc)) = 1.0_pr                                    !Dirichlet conditions      
!                  Lu_diag(shift+iloc(1:nloc)) = du(iloc(1:nloc),2)                        !Neuman conditions
!               ELSE                                                   ! Xmax & Ymax & Zmin & Zmax 
!                  Lu_diag(shift+iloc(1:nloc)) = du(iloc(1:nloc),2)                        !Neuman conditions
                END IF                                                    

!                IF( face(Tdim) == -1 ) THEN                           ! Xmin face (entire face) 
!                   Lu_diag(shift+iloc(1:nloc)) = 1.0_pr                   !Dirichlet conditions
!!$                Lu_diag(shift+iloc(1:nloc)) = du(iloc(1:nloc),2)        !Neuman conditions
!                ELSE IF( face(Tdim) == 1  ) THEN                      ! Xmax face (entire face) 
!                   Lu_diag(shift+iloc(1:nloc)) = 1.0_pr                   !Dirichlet conditions
!!$                Lu_diag(shift+iloc(1:nloc)) = du(iloc(1:nloc),2)        !Neuman conditions
!                END IF
!                IF( face(2) == -1 ) THEN                              ! Ymin face (entire face)      
!                   Lu_diag(shift+iloc(1:nloc)) = 1.0_pr                   !Dirichlet conditions      
!!$                Lu_diag(shift+iloc(1:nloc)) = du(iloc(1:nloc),2)        !Neuman conditions         
!                END IF                                                                               

             END IF
          END IF
       END DO
    END DO
    
    END IF   ! IF ( prd(1) == 0 )

 
  END SUBROUTINE user_algebraic_BC_diag










  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  SUBROUTINE user_algebraic_BC_rhs (rhs, ne_local, nlocal, jlev)
    !--Sets rhs for boundary conditions
    IMPLICIT NONE

    INTEGER ,                               INTENT (IN)     ::  ne_local, nlocal, jlev
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (INOUT ) ::  rhs

    INTEGER                    :: i, ie, ii, shift
    INTEGER                    :: face_type, nloc
    INTEGER, DIMENSION(0:dim)  :: i_p_face
    INTEGER, DIMENSION(dim)    :: face
    INTEGER, DIMENSION(nwlt)   :: iloc



    !BC for both 2D and 3D cases

    IF ( prd(1) == 0 ) THEN     
    !PRINT *, 'Periodicity   ::  ',    prd     
    PRINT *, 'Non Periodic BC ..... algebraic_BC'


    DO ie = 1, ne_local
       shift = nlocal*(ie-1)
       !--Go through all Boundary points that are specified
       i_p_face(0) = 1
       DO i=1,dim
          i_p_face(i) = i_p_face(i-1)*3
       END DO
       DO face_type = 0, 3**dim - 1
          face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
          IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
             CALL get_all_indices_by_face (face_type, jlev, nloc, iloc)
             IF(nloc > 0 ) THEN 

                IF     ( face(1) == -1 ) THEN                          ! Left   faces (Xmin)
                   rhs(shift+iloc(1:nloc)) = T_1                                           !Dirichlet conditions
!                  rhs(shift+iloc(1:nloc)) = 0.0_pr                                        !Neuman conditions
                ELSE IF( face(1) == 1 )  THEN                          ! Right  faces (Xmax)
!                  rhs(shift+iloc(1:nloc)) = T_2                                           !Dirichlet conditions
                   rhs(shift+iloc(1:nloc)) = 0.0_pr                                        !Neuman conditions
                ELSE IF( face(2) == -1 ) THEN                          ! Bottom faces (Ymin) 
                   rhs(shift+iloc(1:nloc)) = T_3                                           !Dirichlet conditions      
!                  rhs(shift+iloc(1:nloc)) = 0.0_pr                                        !Neuman conditions
                ELSE IF( face(2) == 1 )  THEN                          ! Top    faces (Ymax) 
!                  rhs(shift+iloc(1:nloc)) = T_4                                           !Dirichlet conditions      
                   rhs(shift+iloc(1:nloc)) = 0.0_pr                                        !Neuman conditions
!               ELSE IF( ALL( face == (/-1,-1/) ) ) THEN               ! Lower-Left  Corner (Xmin & Ymin) 
!                  rhs(shift+iloc(1:nloc)) = (T_1+T_3)/2.0_pr                              !Dirichlet conditions      
!                  rhs(shift+iloc(1:nloc)) = 0.0_pr                                        !Neuman conditions
!               ELSE IF( ALL( face == (/1,-1/) ) ) THEN                ! Lower-Right Corner (Xmax & Ymin) 
!                  rhs(shift+iloc(1:nloc)) = (T_1+T_2)/2.0_pr                              !Dirichlet conditions      
!                  rhs(shift+iloc(1:nloc)) = 0.0_pr                                        !Neuman conditions
!               ELSE                                                   ! Xmax & Ymax & Zmin & Zmax 
!                  rhs(shift+iloc(1:nloc)) = 0.0_pr                                        !Neuman conditions
                END IF                                                    

!                IF( face(Tdim) == -1  ) THEN                          ! Xmin face (entire face) 
!                   rhs(shift+iloc(1:nloc)) = T_1                 
!                ELSE IF( face(Tdim) == 1 ) THEN                       ! Xmax face (entire face) 
!                   rhs(shift+iloc(1:nloc)) = T_0
!                END IF
!                IF( face(2) == -1  ) THEN                             ! Ymin face (entire face)   
!                   rhs(shift+iloc(1:nloc)) = T_3                                                  
!                END IF                                                                            

             END IF
          END IF
       END DO
    END DO
    
    END IF   ! IF ( prd(1) == 0 )


  END SUBROUTINE user_algebraic_BC_rhs










  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  SUBROUTINE user_project (u, p, nlocal, meth)
    !--Makes u divergence free
    IMPLICIT NONE

    INTEGER,                                    INTENT (IN)    ::  meth, nlocal
    REAL (pr), DIMENSION (nlocal),              INTENT(INOUT)  ::  p
    REAL (pr), DIMENSION (nlocal,n_integrated), INTENT(INOUT)  ::  u

    REAL (pr), DIMENSION (nlocal)                              ::  dp
    REAL (pr), DIMENSION (nlocal)                              ::  f

    INTEGER                                                    ::  i
	INTEGER, PARAMETER                                         ::  ne = 1
    INTEGER, DIMENSION(ne)                                     ::  clip 


  END SUBROUTINE user_project










  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  FUNCTION user_rhs (u_integrated,p)	 
    IMPLICIT NONE

    REAL (pr), DIMENSION (ng,ne),   INTENT(IN)  ::  u_integrated !1D flat version of integrated variables without BC 
    REAL (pr), DIMENSION (ng),      INTENT(IN)  ::  p
    REAL (pr), DIMENSION (n)                    ::  user_rhs

    INTEGER                                     ::  ie, shift
    INTEGER, PARAMETER                          ::  meth=1
    REAL (pr), DIMENSION (ne,ng,dim)            ::  du, d2u
    REAL (pr), DIMENSION (ng,dim)               ::  dp


    CALL c_diff_fast(u_integrated, du, d2u, j_lev, ng, 1, 11, ne, 1, ne)

    !--Form right hand side of heat equations
    DO ie = 1, 2
       shift = ng*(ie-1)
       !CALL c_diff_fast(u_integrated(1:ng,ie), du, d2u, j_lev, nwlt, 1, 11, 1, 1, 1)
      !user_rhs(shift+1:shift+ng) = omega*x(:,2)*du(ie,:,1) - omega*x(:,1)*du(ie,:,2) + nu*SUM(d2u(ie,:,:),2)  ! Rotation in  XY  plane
      !user_rhs(shift+1:shift+ng) = omega*x(:,3)*du(ie,:,1) - omega*x(:,1)*du(ie,:,3) + nu*SUM(d2u(ie,:,:),2)  ! Rotation in  XZ  plane
      !user_rhs(shift+1:shift+ng) = omega*x(:,2)*du(ie,:,1) - omega*x(:,1)*du(ie,:,2) + & 
      !                             omega*x(:,3)*du(ie,:,1) - omega*x(:,1)*du(ie,:,3) + nu*SUM(d2u(ie,:,:),2)  ! Rotation in  X=Y  plane
       IF       ( dim == 2 )  THEN
          user_rhs(shift+1:shift+ng) = omega(3) * (x(:,2)*du(ie,:,1) - x(:,1)*du(ie,:,2)) + nu*SUM(d2u(ie,:,:),2)  ! Rotation in  XY  plane
       ELSE IF  ( dim == 3 )  THEN
          user_rhs(shift+1:shift+ng) = omega(3) * (x(:,2)*du(ie,:,1) - x(:,1)*du(ie,:,2)) + &  ! in XY plane (around Z axis)
                                       omega(2) * (x(:,3)*du(ie,:,1) - x(:,1)*du(ie,:,3)) + &  ! in XZ plane (around Y axis)
                                       omega(1) * (x(:,3)*du(ie,:,2) - x(:,2)*du(ie,:,3)) + &  ! in YZ plane (around X axis)
                                       nu*SUM(d2u(ie,:,:),2)  
       END IF
    END DO

  !---------------- Algebraic BC are set up in user_algebraic_BC and they autmatically overwrite evolution BC

  END FUNCTION user_rhs











  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  ! find Jacobian of Right Hand Side of the problem
  FUNCTION user_Drhs (u, u_prev_timestep_loc, meth)
    IMPLICIT NONE
    
    INTEGER,                        INTENT(IN)  ::  meth
	!u_prev_timestep passed local to cast it into 2dim array to work with vector derivatives
    REAL (pr), DIMENSION (ng,ne)                ::  u, u_prev_timestep_loc
    REAL (pr), DIMENSION (n)                    ::  user_Drhs
 
    INTEGER                                     ::  ie, shift
    REAL (pr), DIMENSION (2*ne,ng,dim)          ::  du         ! 1st derivatives for u (in du(1:ne,:,:)) and u_prev_timestep (in du(ne+1:2*ne,:,:))
    REAL (pr), DIMENSION (ne  ,ng,dim)          ::  d2u        ! 2nd derivatives for u 
    REAL (pr), DIMENSION (ne  ,ng,dim)          ::  du_dummy   ! passed when only calculating 1st derivative.
    !Find better way to do this!! du_dummy with no storage..


    CALL c_diff_fast(u, du(1:ne,:,:), d2u(1:ne,:,:), j_lev, ng, meth, 11, ne , 1, ne )

    DO ie = 1, 2
       shift = ng*(ie-1)
       !CALL c_diff_fast(u_integrated(1:ng,ie), du, d2u, j_lev, nwlt, 1, 11, 1, 1, 1)
      !user_Drhs(shift+1:shift+ng) = omega*x(:,2)*du(ie,:,1) - omega*x(:,1)*du(ie,:,2) + nu*SUM(d2u(ie,:,:),2)  ! Rotation in  XY  plane
      !user_Drhs(shift+1:shift+ng) = omega*x(:,3)*du(ie,:,1) - omega*x(:,1)*du(ie,:,3) + nu*SUM(d2u(ie,:,:),2)  ! Rotation in  XZ  plane
      !user_Drhs(shift+1:shift+ng) = omega*x(:,2)*du(ie,:,1) - omega*x(:,1)*du(ie,:,2) + &
      !                              omega*x(:,3)*du(ie,:,1) - omega*x(:,1)*du(ie,:,3) + nu*SUM(d2u(ie,:,:),2)  ! Rotation in  X=Y  plane
       IF       ( dim == 2 )  THEN
          user_Drhs(shift+1:shift+ng) = omega(3) * (x(:,2)*du(ie,:,1) - x(:,1)*du(ie,:,2)) + nu*SUM(d2u(ie,:,:),2)  ! Rotation in  XY  plane
       ELSE IF  ( dim == 3 )  THEN
          user_Drhs(shift+1:shift+ng) = omega(3) * (x(:,2)*du(ie,:,1) - x(:,1)*du(ie,:,2)) + &  ! in XY plane (around Z axis)
                                        omega(2) * (x(:,3)*du(ie,:,1) - x(:,1)*du(ie,:,3)) + &  ! in XZ plane (around Y axis)
                                        omega(1) * (x(:,3)*du(ie,:,2) - x(:,2)*du(ie,:,3)) + &  ! in YZ plane (around X axis)
                                        nu*SUM(d2u(ie,:,:),2)  
       END IF
    END DO

  END FUNCTION user_Drhs










  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  !
  ! Uses u_prev_timestep, which is global flat (1D ) array set in time_step_cn()
  !
  FUNCTION user_Drhs_diag (meth)
    IMPLICIT NONE
    
    INTEGER,                          INTENT (IN)  ::  meth
    REAL (pr), DIMENSION (n)                       ::  user_Drhs_diag

    INTEGER                                        ::  ie, shift
    REAL (pr), DIMENSION (ng,dim)                  ::  du, d2u
    REAL (pr), DIMENSION (ne,ng,dim)               ::  du_prev_timestep
    REAL (pr), DIMENSION (ne  ,ng,dim)             ::  du_dummy ! passed when only calculating 1st derivative.


    !
    ! does not rely on u so we can call it once here
    !
    CALL c_diff_diag(du, d2u, j_lev, ng, meth, meth, 11)

    DO ie = 1, 2
       shift = ng*(ie-1)
       !CALL c_diff_fast(u_integrated(1:ng,ie), du, d2u, j_lev, nwlt, 1, 11, 1, 1, 1)
      !user_Drhs_diag(shift+1:shift+ng) =   omega * x(:,2)* du(:,1)  &     ! Rotation in  XY  plane
      !                                   - omega * x(:,1)* du(:,2)  &
      !                                   + nu * SUM(d2u,2)                       
      !user_Drhs_diag(shift+1:shift+ng) =   omega * x(:,3)* du(:,1)  &     ! Rotation in  XZ  plane 
      !                                   - omega * x(:,1)* du(:,3)  &
      !                                   + nu * SUM(d2u,2)                       
      !user_Drhs_diag(shift+1:shift+ng) =   omega * x(:,2)* du(:,1) + omega * x(:,3)* du(:,1)  &  ! Rotation in  X=Y  plane
      !                                   - omega * x(:,1)* du(:,2) - omega * x(:,1)* du(:,3)  &
      !                                   + nu * SUM(d2u,2)                
       IF       ( dim == 2 )  THEN
          user_Drhs_diag(shift+1:shift+ng) =   omega(3) * (x(:,2)*du(:,1) - x(:,1)*du(:,2)) + nu * SUM(d2u,2)       ! Rotation in  XY  plane
       ELSE IF  ( dim == 3 )  THEN
          user_Drhs_diag(shift+1:shift+ng) =   omega(3) * (x(:,2)*du(:,1) - x(:,1)*du(:,2)) + &  ! in XY plane (around Z axis)
                                               omega(2) * (x(:,3)*du(:,1) - x(:,1)*du(:,3)) + &  ! in XZ plane (around Y axis)
                                               omega(1) * (x(:,3)*du(:,2) - x(:,2)*du(:,3)) + &  ! in YZ plane (around X axis)
                                               nu * SUM(d2u,2)                
       !omega*x(i,2)*du(ie,:,1) - omega*x(i,1)*du(ie,:,2) + nu*SUM(d2u,2)  !nu*SUM(d2u,2)
       END IF
    END DO

  END FUNCTION user_Drhs_diag
          !user_Drhs(shift+1:shift+ng) = - (u_prev(:,1)+Umn(1))*du(ie,:,1) - &
          !     (u_prev(:,2)+Umn(2))*du(ie,:,2) &
          !     - (u_prev(:,3)+Umn(3))*du(ie,:,3) + nu*SUM(d2u(ie,:,:),2)

          !user_Drhs_diag(shift+1:shift+ng) = - (u_prev_timestep(1:ng)+Umn(1))*du(:,1) -&
          !     (u_prev_timestep(ng+1:2*ng)+Umn(2))*du(:,2) - &
          !     (u_prev_timestep(2*ng+1:3*ng)+Umn(3))*du(:,3) + nu*SUM(d2u,2)







  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  FUNCTION user_chi (nlocal, t_local )
    IMPLICIT NONE
    INTEGER,                      INTENT (IN)  ::  nlocal
    REAL (pr),                    INTENT (IN)  ::  t_local
    REAL (pr), DIMENSION (nlocal)              ::  user_chi


    user_chi = 0.0_pr
    
  END FUNCTION user_chi










  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  !
  ! Calculate any statitics
  !
  ! startup_flag - 0 when adapting to IC,then 1 inmain integration loop
  !
  SUBROUTINE user_stats ( u ,j_mn, startup_flag)
    IMPLICIT NONE
    INTEGER ,                            INTENT (IN)  ::  startup_flag
    INTEGER ,                            INTENT (IN)  ::  j_mn 
    REAL (pr), DIMENSION (nwlt,1:n_var), INTENT (IN)  ::  u
    
  END SUBROUTINE user_stats










  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  SUBROUTINE user_cal_force (u, n, t_local, force, drag, lift)
    !--Calculates drag and lift on obstacle using penalization formula
    IMPLICIT NONE

    INTEGER,                    INTENT (IN)     ::  n
    REAL (pr),                  INTENT (IN)     ::  t_local
    REAL (pr), DIMENSION (dim), INTENT (INOUT)  ::  force
    REAL (pr),                  INTENT (OUT)    ::  drag, lift
    REAL (pr), DIMENSION (n,dim)                ::  u


    drag = 0.0_PR
    lift = 0.0_PR

    !
    ! There is no obstacle in flow
    !

  END SUBROUTINE user_cal_force










  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  !
  ! Read input from "case_name"_pde.inp file
  ! case_name is in string file_name read from command line
  ! in read_command_line_input()
  ! 
  SUBROUTINE user_read_input()
    IMPLICIT NONE
    
    INTEGER    :: i


    call input_real ('nu',nu,'stop',    ' nu: heat conduction coefficient')


    call input_real ('T_0',T_0,'stop',  ' T_0: initial temperature')
    call input_real ('T_1',T_1,'stop',  ' T_1: temperature at Xmin')
    call input_real ('T_2',T_2,'stop',  ' T_2: temperature at Xmax')
    call input_real ('T_3',T_3,'stop',  ' T_3: temperature at Ymin')
    call input_real ('T_4',T_4,'stop',  ' T_4: temperature at Ymax')
    call input_real ('T_5',T_5,'stop',  ' T_5: temperature at Zmin')
    call input_real ('T_6',T_6,'stop',  ' T_6: temperature at Zmax')



   !call input_real ('omega',omega,'stop',  ' omega:')
    call input_real_vector ('omega',omega,3,'stop',  ' omega:')



    call input_integer ('RigidBody_Type',     RigidBody_Type,        'stop',  ' RigidBody_Type')


    IF ( (RigidBody_Type == 1) ) THEN  
       call input_real ('X_Center__RigidBody', X_Center__RigidBody, 'stop',  ' X_Center__RigidBody')
       call input_real ('Y_Center__RigidBody', Y_Center__RigidBody, 'stop',  ' Y_Center__RigidBody')
       call input_real ('Z_Center__RigidBody', Z_Center__RigidBody, 'stop',  ' Z_Center__RigidBody')
       call input_real ('Radius__RigidBody',   Radius__RigidBody,   'stop',  ' Radius__RigidBody')
    ENDIF


    
    IF ( (RigidBody_Type == 2) ) THEN  
       call input_integer ('RigidBody_Number',         RigidBody_Number,         'stop',  ' RigidBody_Number')
       call input_real    ('Radius__RigidBody_Array ', Radius__RigidBody_Array , 'stop',  ' Radius__RigidBody_Array ')
       call input_real    ('Length__RigidBody ',       Length__RigidBody ,       'stop',  ' Length__RigidBody ')
       call input_real    ('Width__RigidBody',         Width__RigidBody,         'stop',  ' Width__RigidBody')
    ENDIF


    
    IF ( (RigidBody_Type == 3) ) THEN  
       call input_integer ('RigidBody_Number',         RigidBody_Number,         'stop',  ' RigidBody_Number')
       call input_real    ('Radius__RigidBody_Array ', Radius__RigidBody_Array , 'stop',  ' Radius__RigidBody_Array ')
       call input_real    ('axis_a ',                  axis_a ,                  'stop',  ' axis_a')
       call input_real    ('axis_b',                   axis_b,                   'stop',  ' axis_b')
       IF ( dim == 3) THEN
       call input_real    ('axis_c',                   axis_c,                   'stop',  ' axis_c')
       ENDIF
    ENDIF 


  END SUBROUTINE user_read_input










  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  !
  ! calculate any additional variables
  ! 
  ! arg 
  ! flag  - 0 calledwhile adapting to IC, 1 - called in main time integration loop
  !
  ! These additiona variables are calculated and left in real space.
  !
  SUBROUTINE user_additional_vars( t_local, flag )
    IMPLICIT NONE
    
    REAL (pr), INTENT (IN) ::  t_local
    INTEGER ,  INTENT(IN)  ::  flag        ! 0- called during adaption to IC, 1 called during main integration loop
    REAL (pr)              :: t_zero
    INTEGER                :: i
    DOUBLE PRECISION       :: DERF
    EXTERNAL               :: DERF


    t_zero = 0.0_pr
    IF(t_local == 0) t_zero = 1.0e-12_pr
    DO i =1, nwlt
       u(i,n_var_pressure) = T_1 - (T_1 - T_0)*DERF( (x(i,Tdim) - xyzlimits(1,Tdim))/SQRT(4.0_pr*nu*(t_local+t_zero)) ) &
                            /DERF( (xyzlimits(2,Tdim) - xyzlimits(1,Tdim))/SQRT(4.0_pr*nu*t_local) )
    END DO

  END SUBROUTINE user_additional_vars










  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  !
  ! calculate any additional scalar variables
  ! 
  ! arg 
  ! flag  - 0 calledwhile adapting to IC, 1 - called in main time integration loop
  !
  SUBROUTINE user_scalar_vars( flag )
    IMPLICIT NONE
    
    INTEGER , INTENT(IN) :: flag ! 0- called during adaption to IC, 1 called during main integration loop

  END SUBROUTINE user_scalar_vars










  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  !
  !************ Calculating Scales ***************************
  !
  ! Note the order of the components in the scl array
  ! correspond to u_tn, v_tn, w_tn, u_tn-1, v_tn-1, w_tn-1, u_tn-2, v_tn-2, w_tn-2
  ! 
  SUBROUTINE user_scales(flag, use_default, u, nlocal, ne_local, l_n_var_adapt , l_n_var_adapt_index, scl, scl_fltwt) !add
    IMPLICIT NONE
    
    INTEGER,                                    INTENT (IN)    ::  flag                             ! 0 during initial adaptation, then 1 during time advancement
    LOGICAL ,                                   INTENT(INOUT)  ::  use_default
    INTEGER,                                    INTENT (IN)    ::  nlocal, ne_local
    REAL (pr), DIMENSION (1:nlocal,1:ne_local), INTENT (IN)    ::  u
    LOGICAL ,                                   INTENT (IN)    ::  l_n_var_adapt(ne_local)
    INTEGER ,                                   INTENT (IN)    ::  l_n_var_adapt_index(1:ne_local)
    REAL (pr), DIMENSION (1:ne_local),          INTENT (INOUT) ::  scl
    REAL (pr) ,                                 INTENT(IN)     ::  scl_fltwt                         !weight for temporal filter on scl

    !
    ! Ignore the output of this routine and use default scales routine
    !
    use_default = .TRUE. 

  END SUBROUTINE user_scales










  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  SUBROUTINE user_cal_cfl (use_default, u, cfl_out)
    USE precision
    USE sizes
    USE pde

    IMPLICIT NONE

    LOGICAL , INTENT(INOUT) :: use_default
    REAL (pr),                                INTENT (INOUT)  ::  cfl_out
    REAL (pr), DIMENSION (nwlt,n_integrated), INTENT (IN)     ::  u

    INTEGER                                                   ::  i
    REAL (pr)                                                 ::  floor
    REAL (pr), DIMENSION (dim)                                ::  cfl
    REAL (pr), DIMENSION(dim,nwlt)                            ::  h_arr


    use_default = .FALSE.

    floor = 1e-12_pr
    cfl_out = floor
  
    CALL get_all_local_h (h_arr)
  
    !!$  DO i = 1, nwlt
    !!$     cfl(1:dim) = ABS (u(i,1:dim)+Umn(1:dim)) * dt/h_arr(1:dim,i)
    !!$     cfl_out = MAX (cfl_out, MAXVAL(cfl))
    !!$  END DO

    cfl_out = 1.0_pr ! no CFL condition

  END SUBROUTINE user_cal_cfl










  
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  !
  ! calculate sgs model forcing term
  ! user_sgs_force is called int he beginning of each times step in time_adv_cn().
  ! THE sgs forcign is stored for the time step in sgs_mdl_force(1:nlocal,1:n_integrated)
  ! where nlocal should be nwlt.
  ! 
  ! Accesses u from field module, 
  !          j_lev from wlt_vars module,
  !
  SUBROUTINE  user_sgs_force (u_loc, nlocal)
    IMPLICIT NONE

    INTEGER,                                    INTENT (IN)      ::  nlocal
    REAL (pr), DIMENSION (nlocal,n_integrated), INTENT (INOUT)   ::  u_loc

  END SUBROUTINE  user_sgs_force









  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  FUNCTION user_sound_speed (u, neq, nwlt)
    IMPLICIT NONE
    
    INTEGER,                         INTENT(IN)  ::  nwlt, neq
    REAL (pr), DIMENSION (nwlt,neq), INTENT(IN)  ::  u
    REAL (pr), DIMENSION (nwlt)                  ::  user_sound_speed

    !user_sound_speed(:) = SQRT( gamma*(gamma-1.0_pr)* &
    !       ( u(:,4)-0.5_pr*SUM(u(:,n_var_mom(1:dim))**2,DIM=2)/u(:,1)) ) ! pressure

    user_sound_speed(:) = 0.0_pr

  END FUNCTION user_sound_speed
  
  
  
  






  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  SUBROUTINE  user_pre_process
    IMPLICIT NONE
     
  END SUBROUTINE user_pre_process










  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  SUBROUTINE  user_post_process
    USE parallel
    IMPLICIT NONE

       !IF ( (it > 0) .AND. (mod(it,5)==0) )      THEN
       !  IF (par_rank.EQ.0)  PRINT *, 'it= ', it, '  INSIDE user_post_process'
       !  nu = nu - 0.01_pr
       !  IF ( nu <= 0.0_pr) nu = 0.01
       !  IF (par_rank.EQ.0)  PRINT *, 'nu= ', nu, '  INSIDE user_post_process'
        
       !END IF

    
  END SUBROUTINE user_post_process








END MODULE user_case

