!***********************************************************************   
!*                                                                     *
!*              3-D PDE SOLVER BASED on                                *
!*              SECOND GENERATION WAVELET TRANSFORM                    *
!*                                                Ver. on 10/19/2001   *
!*                                                                     *
!*                                                                     *
!*  Authors:  OLEG V. VASILYEV ( Adaptive Wavelet Technologies, LLC )  *
!*            Oleg.V.Vasilyev@AdaptiveWaveletTechnologies.com          *
!*            NICHOLAS KEVLAHAN (McMaster University)                  *
!*            kevlahan@mcmaster.ca                                     *
!*            http://icarus.math.mcmaster.ca/kevla/                    *
!*            DAN GOLDSTEIN                                            *
!*            ALEXEI VEZOLAINEN                                        *
!*            a_vez@mail.ru                                            *
!*                                                                     *
!*  $Revision: 1.11 $  $Date: 2018/09/19 14:00:00                      *
!***********************************************************************

PROGRAM wavelet_transform
  USE precision
  USE main_vars
  USE debug_vars
  USE wlt_vars
  USE util_vars
  USE wavelet_filters_mod
  USE wavelet_filters_vars
  USE elliptic_vars
  USE wlt_trns_mod
  USE share_kry
  USE pde
  USE share_consts
  USE sizes
  USE user_case
  USE SGS_incompressible
  USE hyperbolic_solver
  USE penalization
  !  USE wlt_trns_mod

  !  USE fft_module !testing only DG

  USE io_3d_vars
  USE io_3D
  USE wlt_trns_mod
  USE wlt_trns_util_mod
  USE kry_mod
  USE rk_mod
  USE read_init_mod
  USE util_mod
  USE elliptic_mod
  USE time_int_cn_mod
  USE read_netcdf_mod
  USE field
  USE default_mod
  USE parallel              ! MPI related subroutines
  !USE db_tree !TEST ONLY
  !the following directive is necessary on unpatched absoft compiler so it will release memory.
  ! See also call release_cache() at end of main loop

  !DIR$ NAME (release_cache="_f90a_free_all")

  !  USE test_routines


  IMPLICIT NONE



  !--Wavelet transform variables

  REAL (pr), DIMENSION (:), POINTER :: scl
  REAL (pr) ::  scl_fltwt !weight for temporal filter on scl
  REAL (pr), ALLOCATABLE :: du(:,:,:), d2u(:,:,:), u_tmp(:,:)
  REAL (pr) ::  area, eps_init, eps_run
  INTEGER   :: eps_adapt_steps 
  INTEGER   :: j_lev_a,  j_lev_new, j_lev_old, j_filt

  INTEGER, DIMENSION(3) :: nxyz_a
  INTEGER   :: i, j, k,  ix, iy, idim ! dummy variables
  INTEGER   :: ie, ih, ixh, iyh, type
  INTEGER   :: iii,meth2,iter_lev,iter_grid
  INTEGER   ::  n_l, n_h, iter, iter_max, inum, idenom

  LOGICAL :: new_grid
  LOGICAL :: first_time_integration_iter ! flag to track when we are in first iteration of main time iteration loop
  LOGICAL :: test_forcewrite



  !--Time integration variables
  REAL (pr) :: dl,dl1, cfl, cflmax, cflmin,  ddt, dtwrite
  LOGICAL   :: dt_changed=.FALSE.
  REAL (pr) :: drag, lift
  REAL (pr), DIMENSION (:), ALLOCATABLE :: force
  REAL  (pr) :: tmp1, tmp2  ! parallel debug info

  INTEGER :: io_status, &             ! allocation status
       flag, &
       num1_tmp, &                    ! DEBUG number of adapt_grid calls
       nwlt_tmp                       ! counter
    
  INTEGER :: i1(1), ixyz(3) !test 

  CHARACTER (LEN = 12) DATE_TIME_ZONE(3)
  INTEGER :: DATE_TIME(8)             ! Date and Time   to be written in   file_gen.sequentialrun.log
  

#ifdef DATA_ASSIMILATION
  !OLEG: convergence test
  INTEGER :: iwrite_init
#endif
  
  CALL timer_start(1)
  
  ! initialize parallel module
  CALL parallel_init
  
  ! initialize variables
  j_lev_old = 0
  j_lev_a   = 0
  j_mx      = 0    ! must be zero so that in read solution if it is still zero it will be
  !                ! set to j_mx_read (from .res file) for post processing code.
  !                ! THE ACTUAL REASON IS:
  !                !  in read_input  j_mn_evol is set as j_mn_evol=j_mn
  !                !   DO WE NEED THIS ?
  j_mn      = 0    !   <--

  ! initialize debug_vars module timers and echo debug status
  CALL debug_initialize( VERB=(par_rank.EQ.0.AND.verb_level.GT.0) )


  ! Read in command line arguments
#ifdef USE_USER_CLI
  CALL user_read_command_line_input !OLEG: this is an option for CMA optimization
#else
  CALL read_command_line_input      !      this is normal usage
#endif
  
  
  ! Read .inp input parameters file
  !  use optional argument VERB for verbose reading:
  CALL read_input(eps_init,eps_run,eps_adapt_steps, &
       scl_fltwt, &
       cflmax,cflmin,dtwrite,  &
       j_mn, j_mx, &
       j_filt,ij_adj, adj_type, VERB=(par_rank.EQ.0) )

  
  ! Read in any input variables needed by the user case module
  IF (par_rank.EQ.0.AND.verb_level.GT.0) THEN
     PRINT *, ' '
     PRINT *, '********************************************************************'
     PRINT *, '*  Inputs read by user case                                        *'
     PRINT *, '********************************************************************'
  END IF
  CALL user_read_input		                    

  ! read inputs for SGS model
  IF(sgsmodel > 0) CALL SGS_read_input
  IF( (sgsmodel > 0) .AND. (do_const_diss .OR. do_const_k) ) THEN
     eps_run  = eps
     eps_expl = eps
     eps_init = eps   
   END IF
  ! read inputs for hyperbolic module
  IF(hypermodel > 0) CALL hyper_read_input


  ! Finalize reading of inputs
  ! (clean reader, set assymetry, check some inputs)
  CALL read_input_finalize(eps_init,eps_run,eps_adapt_steps, &
       scl_fltwt, &
       cflmax,cflmin,dtwrite,  &
       j_mn, j_mx, &
       j_filt,ij_adj, adj_type, VERB=(par_rank.EQ.0.AND.verb_level.GT.0) )


  !
  ! Setup any parameters specific to the pde being solved 
  !
  IF (sgsmodel == 0) THEN
     n_var_SGS = 0
  ELSE
     CALL SGS_setup(0)
  END IF
  IF(hypermodel /= 0) THEN
     CALL hyper_setup(0)
  END IF

  CALL user_setup_pde( VERB=(par_rank.EQ.0.AND.verb_level.GT.0) )

  IF(sgsmodel /=0) THEN
     CALL SGS_setup(1)
  END IF
  IF(interpolate_dist) THEN
     n_var_interpolate(ndist,:)    = .TRUE.
     IF (n_var_req_restart(ndist)) n_var_save(ndist) = .TRUE.
  END IF
  IF(hypermodel /= 0) THEN
     CALL hyper_setup(1)
  END IF

  !
  ! Do some sanity checks to see that the code can solve the pde defined by the user
  !
  CALL check_setup_pde

  DO i=1,n_var        
     n_var_index(i) = i        
  END DO

  !
  ! IF scaleCoeff was not allocated and set in user_setup_pde we alocate it and set to 1.0_pr here
  !
  IF( .NOT. ALLOCATED(scaleCoeff) ) THEN
     ALLOCATE ( scaleCoeff(1:n_var), STAT=io_status )
     CALL test_alloc( io_status, 'scaleCoeff in main', n_var )
     scaleCoeff = 1.0_pr
  END IF
  
  iter = 0
  
  ! for pure restart we don't want to change anything
  IF (IC_restart_mode.EQ.1 .OR. IC_restart_mode.EQ.2) &
       j_IC = j_mx

  j_mx_adj = MIN(j_IC,j_mx)
  
  
  ! Initialize tree and compute initial domain decomposition
  ! (we have already read all the input parameters)
  IF(IC_restart_mode > 0 ) THEN
     CALL read_solution_version( IC_filename )
     !!!!IF ( IC_COMMONFILE ) &
     IF ( IC_RES_VERSION .EQ. 5 ) &
        CALL read_solution( scl, IC_filename, DO_READ_ALL_VARS_FALSE, DO_READ_COMMONFILE_TRUE, VERBLEVEL=verb_level )
  END IF
  CALL pre_init_DB


  !********* Open _user_stats and _log files ************
  CALL open_single_file( 'user_stats', 'old' )
  CALL open_single_file( 'log',        'old' )
  !********* Write header for stats _log file*******************
  CALL write_log_header( j_mx, dim )
  

  !
  ! We either restart, take restart file as initial condition or do a clean fresh start.
  !
  IF ((IC_restart .OR. (IC_from_file .AND. IC_file_fmt == 0)) ) THEN !--Restart with
     IF( IC_restart .AND. par_rank.EQ.0 .AND. verb_level.GT.0 ) THEN 
        PRINT *, '********************** RESTART ************************'
        PRINT *, 'Restarting from file:', TRIM(IC_filename)
        PRINT *, 'Next station number after restart will be: ', IC_restart_station
     END IF
     
     flag = 0                     ! restart from IC
     IF(IC_restart) flag = 1      ! pure restart

     !
     ! Read in restart data
     ! data is read in field u(,)
     ! in wlt space.
     CALL read_solution( scl, IC_filename, DO_READ_ALL_VARS_FALSE, DO_READ_COMMONFILE_FALSE, VERBLEVEL=verb_level )
     

     !j_lev_old = j_lev ! added DG I think this is correct
     
     
     
     !********* Open _user_stats and _log files ************
     !CALL open_single_file( 'user_stats', 'old' )
     !CALL open_single_file( 'log', 'old' )
     
     
     !
     ! Initialize the field database
     !
     j_lev_a = j_lev ! j_lev taken from restart file

     CALL init_DB( nwlt_old, nwlt, nwltj_old, nwltj, new_grid,&
          j_lev_old, j_lev_a, j_mn, j_mx , MAXVAL(nxyz) , nxyz, &
          (IC_restart .OR. (IC_from_file .AND. IC_file_fmt == 0)) )


     !--Set weights for area/volume associated with each wlt collocation point
     CALL set_weights()


     IF (BTEST(verb_level,3)) THEN
        PRINT *, 'main: after init_DB and set_weights'
        DO i=1,n_var
           WRITE (*,'("MAXVAL(ABS(u(:),",I2,"))=",E22.15)') i, MAXVAL(ABS(u(:,i)))
        END DO
     END IF

     IF (imask_obstacle) THEN  
        !--Set up distance function
        IF(use_dist) THEN
           IF(ALLOCATED(dist) ) THEN
              IF(size(dist) /= nwlt) THEN 
                 DEALLOCATE(dist)
                 ALLOCATE ( dist(1:nwlt), STAT=io_status )
                 CALL test_alloc( io_status, 'dist in main', nwlt )
              END IF
           ELSE
              ALLOCATE ( dist(1:nwlt), STAT=io_status )
              CALL test_alloc( io_status, 'dist in main', nwlt )
           END IF
           IF (interpolate_dist .AND. n_var_req_restart(ndist)) THEN
              dist = u(:,ndist)
           ELSE
#ifdef DISTANCE_FUNCTION
              CALL user_dist (nwlt, t, DISTANCE=dist)
              IF (interpolate_dist .OR. ndist > 0) u(:,ndist) = dist
#else 
              IF (par_rank.EQ.0) THEN
                 WRITE(*,'("ERROR: to use this functionality the user need to define:")' ) 
                 WRITE(*,'("optional flag DISTANCE_FUNCTION and SUBROUTINE user_dist")' ) 
              END IF
              CALL parallel_finalize; STOP
#endif
           END IF
        END IF
        !--Set up mask
        IF(ALLOCATED(penal) ) THEN
           IF(size(penal) /= nwlt) THEN 
              DEALLOCATE(penal)
              ALLOCATE ( penal(1:nwlt), STAT=io_status )
              CALL test_alloc( io_status, 'penal in main', nwlt )
           END IF
        ELSE
           ALLOCATE ( penal(1:nwlt), STAT=io_status )
           CALL test_alloc( io_status, 'penal in main', nwlt )
        END IF
        penal = user_chi(nwlt,t)
     END IF
     
     IF (IC_restart_mode .eq. 3) it = 0
     
     CALL user_pre_process
     !!PAUSE 'WARNING: user_pre_process/user_post_process have been ignored in main:281,293'

     !
     ! WE call user_initial_conditions here, In user_initial_conditions we can see if we are doing a restart
     ! by looking at the logical flag IC_restart
     ! We use u_old (set to u in init_DB on restart) because below adapt_grid starts with u_old and puts new adapted data in u.
     CALL user_initial_conditions( u, nwlt, n_var, t, scl, scl_fltwt, iter)
     IF(sgsmodel /= 0) CALL SGS_initial_conditions (u, nwlt, n_var, t, scl, scl_fltwt)

     CALL user_post_process

     IF(flag == 1 .AND. sgsmodel /= 0) THEN
        IF (BTEST(verb_level,3)) PRINT *,'Finding mdl filter masks '
        CALL make_mdl_filts( u(1:nwlt,1:n_integrated), n_integrated, scl, &
             mdl_filt_type_grid, mdl_filt_type_test ) 
        IF (BTEST(verb_level,3)) THEN
           PRINT *,'In main() '
           PRINT *,'SUM(u_g_mdl_gridfilt_mask) ', SUM(u_g_mdl_gridfilt_mask)
           PRINT *,'SUM(u_g_mdl_testfilt_mask) ', SUM(u_g_mdl_testfilt_mask)
           PRINT *,'SUM(u_g_mdl_gridfilt_mask-u_g_mdl_testfilt_mask)',SUM(u_g_mdl_gridfilt_mask-u_g_mdl_testfilt_mask)
        END IF
        CALL sgs_model_setup( nwlt )
        CALL user_sgs_force ( u, nwlt) !calculating SGS values for SGS_stats
     END IF
     IF(print_SGS_stats .OR. sgsmodel /= 0) CALL SGS_stats ( u ,j_mn, flag)
     CALL user_stats(u, j_mn, flag)

     !debug
     IF (BTEST(verb_level,2)) THEN
        IF (par_rank.EQ.0) PRINT *,'Field var stats, After user_initial_conditions'
        DO i=1,n_var
           tmp1 = MINVAL(u(:,i)); tmp2 = MAXVAL(u(:,i))
           CALL parallel_global_sum( REALMINVAL=tmp1 )
           CALL parallel_global_sum( REALMAXVAL=tmp2 )
           IF (par_rank.EQ.0) WRITE(*,'(" MINMAX(u_i) ", A, 2(es22.15,1X) )' ) u_variable_names(i), tmp1, tmp2
           IF (par_rank.EQ.0) WRITE(*,'(" ")' )
        END DO
     END IF
     !end debug 

     IF(n_var_exact > 0) THEN
        IF (BTEST(verb_level,2)) THEN
           PRINT *,''
           PRINT *,'===================================================='
           PRINT *,'== Before adapting grid                           =='
        END IF
        CALL Check_Exact_Soln(u,  u_ex,  t, scl,(n_var_exact_soln(:,flag) .AND. n_var_interpolate(:,flag)), &
            VERB=BTEST(verb_level,2) )
     END IF
     
     !
     !============ Additional variables to base grid adaptation
     ! 
     !
     
     IF( n_var_additional > 0 ) CALL user_additional_vars(t, flag ) 
     IF( hypermodel /= 0 ) THEN
        !NOTE: arrays allocation could be modified, once the stuctrue of the code reorganized such that
        !      there is no mutual use of hyperbolic and user_case modules
        IF(ALLOCATED(c_sound))DEALLOCATE(c_sound) 
        ALLOCATE ( c_sound(1:nwlt), STAT=io_status )
        CALL test_alloc( io_status, 'c_sound in main', nwlt )
        c_sound = user_sound_speed(u(1:nwlt,1:n_var),n_var,nwlt)
        CALL hyper_additional_vars(u,nwlt,c_sound,0)
     END IF
     
     CALL user_scalar_vars( flag ) !update any user defined scalar variables
     
     CALL scales(flag,u, nwlt, n_var, n_var_adapt(:,flag),&
          n_var_index(:), scl, scl_fltwt)


     IF (BTEST(verb_level,3)) THEN
        PRINT *, 'main: before the transform'
        DO i=1,n_var
           WRITE (*,'("MAXVAL(ABS(u_in(:),",I2,"))=",E22.15)') i, MAXVAL(ABS(u(:,i)))
        END DO
     END IF

     CALL c_wlt_trns_mask (u, u, n_var, n_var_interpolate(:,flag), n_var_adapt(:,flag), .TRUE., .FALSE.,&
          nwlt, nwlt, j_lev, j_lev, HIGH_ORDER, WLT_TRNS_FWD, DO_UPDATE_DB_FROM_U, DO_UPDATE_U_FROM_DB)

     IF (BTEST(verb_level,3)) THEN
        PRINT *, 'main: after the transform'
        DO i=1,n_var
           WRITE (*,'("MAXVAL(ABS(u_out(:),",I2,"))=",E22.15)') i, MAXVAL(ABS(u(:,i)))
        END DO
     END IF
     
     !
     ! re-allocate u_old array  (for wrk version)
     !
     IF ( TYPE_DB == DB_TYPE_WRK ) THEN
        IF( nwlt /= nwlt_old ) THEN
           IF( ASSOCIATED(u_old) ) DEALLOCATE (u_old)
           ALLOCATE ( u_old(1:nwlt,1:n_var), STAT=io_status )
           CALL test_alloc( io_status, 'u_old in main', nwlt*n_var )
           u_old = 0.0_pr
        END IF
        u_old = u
     END IF

     !***********************************************************
     !* Start grid adaptation criteria
     !***********************************************************

     !
     ! Adapt to grid based on j_mn_init from the input file and j_lev from the
     ! restart file
     !
     j_lev_old = j_lev
     j_mn = j_mn_init
     CALL adapt_grid( 1  ,eps , j_lev_old, j_lev_a, j_mn_init, j_mx, nxyz_a, ij_adj,&
          adj_type, scl ,new_grid)


     IF (BTEST(verb_level,2)) THEN
        IF (par_rank.EQ.0) THEN
           PRINT *,'Field var stats, After grid adaptation. (Interpolation to new grid)'
           PRINT *,'nwlt=',nwlt
        END IF
        DO i=1,n_var
           tmp1 = MINVAL(u(:,i)); tmp2 = MAXVAL(u(:,i))
           CALL parallel_global_sum( REALMINVAL=tmp1 )
           CALL parallel_global_sum( REALMAXVAL=tmp2 )
           !WRITE(*,'(" ||u_i||L2   ", A, es15.8)' ) u_variable_names(i), SQRT ( SUM( (u(:,i)**2)*dA )/ sumdA  )
           IF (par_rank.EQ.0) THEN
              WRITE(*,'(" MINMAX(u_i) ", A, 2(es22.15,1X) )' ) u_variable_names(i), tmp1, tmp2
              WRITE(*,'(" ")' )
           END IF
        END DO
     END IF
     
     !--Set weights for area/volume associated with each wlt collocation point
     CALL set_weights()

     IF (imask_obstacle) THEN  
        !--Set up distance function
        IF(use_dist) THEN
           IF(ALLOCATED(dist) ) THEN
              IF(size(dist) /= nwlt) THEN 
                 DEALLOCATE(dist)
                 ALLOCATE ( dist(1:nwlt), STAT=io_status )
                 CALL test_alloc( io_status, 'dist in main', nwlt )
              END IF
           ELSE
              ALLOCATE ( dist(1:nwlt), STAT=io_status )
              CALL test_alloc( io_status, 'dist in main', nwlt )
           END IF
           IF (interpolate_dist) THEN
              dist = u(:,ndist)
           ELSE
#ifdef DISTANCE_FUNCTION
              CALL user_dist (nwlt, t, DISTANCE=dist)
              IF(ndist > 0) u(:,ndist) = dist
#else 
              IF (par_rank.EQ.0) THEN
                 WRITE(*,'("ERROR: to use this functionality the user need to define:")' ) 
                 WRITE(*,'("optional flag DISTANCE_FUNCTION and SUBROUTINE user_dist")' ) 
              END IF
              CALL parallel_finalize; STOP
#endif
           END IF
        END IF
        !--Set up mask
        IF(ALLOCATED(penal) ) THEN
           IF(size(penal) /= nwlt) THEN 
              DEALLOCATE(penal)
              ALLOCATE ( penal(1:nwlt), STAT=io_status )
              CALL test_alloc( io_status, 'penal in main', nwlt )
		   END IF
        ELSE
           ALLOCATE ( penal(1:nwlt), STAT=io_status )
           CALL test_alloc( io_status, 'penal in main', nwlt )
        END IF
        penal = user_chi(nwlt,t)
     END IF

     !
     !Check Exact Solution if it exists
     !
     ! We only compare the actual variable that are being adapted to (those being interpolated to new grid each time though 
     ! this loop)
     !
     IF(n_var_exact > 0) THEN
        IF( par_rank.EQ.0 .AND. BTEST(verb_level,2) ) THEN
           PRINT *,''
           PRINT *,'===================================================='
           PRINT *,'== After adapting grid                            =='
        END IF
        CALL Check_Exact_Soln(u,  u_ex,  t, scl,(n_var_exact_soln(:,flag) .AND. n_var_interpolate(:,flag)) )
        !PRINT *, MAXVAL(u-u_ex), MINVAL(u-u_ex)
     END IF

  ELSE
     ALLOCATE ( scl(1:n_var), scl_global(1:n_var), STAT=io_status )
     CALL test_alloc( io_status, 'scl, scl_global in main', n_var*2 )
     scl =1.0_pr; scl_global = scl
  END IF

  
  CALL timer_stop(1)
  CALL timer_start(2)

     
  IF( .NOT. IC_restart ) THEN !--Fresh start (possibly with a restart file as the IC )
  
     !CALL open_single_file( 'user_stats', 'replace' )
     !     OPEN  (13, FILE = TRIM(res_path)//TRIM(file_gen)//'_comp_rate', FORM='formatted', STATUS='replace') ; CLOSE (13)



     !
     ! Initialize the field database if we are not using a native restart file for the IC
     !
     IF( .NOT. (IC_from_file .AND. IC_file_fmt == 0)  ) THEN
        CALL init_DB( nwlt_old, nwlt, nwltj_old, nwltj, new_grid,&
             j_lev_old, j_lev_a, j_mn, j_mx , MAXVAL(nxyz) , nxyz_a,&
             (IC_restart .OR. (IC_from_file .AND. IC_file_fmt == 0)))
     ENDIF
     !*****************************************************************
     !*        Adapt wavelet grid to initial conditions               *
     !*****************************************************************

     iter = 0 ; iter_max = 100 ; new_grid = .TRUE. ; it = 0 ; t = t_begin 
     !--- Analytical Initial Conditions or IC from a data file

     !--Set weights for area/volume associated with each wlt collocation point
     CALL set_weights()

     num1_tmp = 0     ! DEBUG number of adapt_grid calls


     
#ifdef DATA_ASSIMILATION
           !OLEG: convergence test
           iwrite_init = iwrite
           IF (par_rank.EQ.0)PRINT *, 'DATA ASSIMILATION'
     
!!$           EPS_LOOP: DO WHILE(iwrite < 2) 
!!$              PRINT *, 'BEGINING OF EXTRA LOOP:', iwrite
!!$              !PAUSE
!!$        eta = eps**2
!!$        eta = eta_max 
!!$        ETA_LOOP: DO WHILE (eta >= eta_min) 
!!$        tol1 = MIN(1.0e-3_pr*eps, 1.0e-3_pr*SQRT(eta)) 
!!$        tol2 = MIN(1.0e-3_pr*eps, 1.0e-3_pr*SQRT(eta)) 
!!$        tol3 = MIN(1.0e-3_pr*eps, 1.0e-3_pr*SQRT(eta))
!!$        iter = 1 
#endif


     new_grid = .TRUE. !tmp
     j_mx_adj = j_mx !tmp
#ifdef DATA_ASSIMILATION
  j_mx_adj = MIN(j_IC,j_mx)
#endif

     INITIAL_CONDITIONS_LOOP: DO WHILE (new_grid .AND. iter <= j_lev+2 .AND. iter /= -1)

        iter = iter+1

        IF(iter == 1) THEN
           j_mn = j_mn_init ! use j_mn_init during the first iteration
        ELSE
           j_mn = j_mn_evol ! use j_mn_evol afterward
        END IF

        IF( IC_from_file .AND. IC_file_fmt /= 0 )THEN !IC from data file other then native restart format

           !
           ! read in initial data to a buffer on the first call (iter==1)
           ! then read_initial_data() is called with iter==-1 below to deallocate and
           ! clear buffer once the intitial adapted grid is formed.
           !
           CALL read_initial_data( u, nwlt , n_var, &
                IC_filename, IC_file_fmt, iter,j_IC)

        END IF
        
        IF (imask_obstacle) THEN  
           !--Set up distance function
           IF(use_dist) THEN
              IF(ALLOCATED(dist) ) THEN
                 IF(size(dist) /= nwlt) THEN 
                    DEALLOCATE(dist)
                    ALLOCATE ( dist(1:nwlt), STAT=io_status )
                    CALL test_alloc( io_status, 'dist in main', nwlt )
                 END IF
              ELSE
                 ALLOCATE ( dist(1:nwlt), STAT=io_status )
                 CALL test_alloc( io_status, 'dist in main', nwlt )
              END IF
#ifdef DISTANCE_FUNCTION
              CALL user_dist (nwlt, t, DISTANCE=dist)
              IF (interpolate_dist .OR. ndist>0) u(:,ndist) = dist
#else 
              IF (par_rank.EQ.0) THEN
                 WRITE(*,'("ERROR: to use this functionality the user need to define:")' ) 
                 WRITE(*,'("optional flag DISTANCE_FUNCTION and SUBROUTINE user_dist")' ) 
              END IF
              CALL parallel_finalize; STOP
#endif
           END IF
           !--Set up mask
           IF(ALLOCATED(penal) ) THEN
              IF(size(penal) /= nwlt) THEN 
                 DEALLOCATE(penal)
                 ALLOCATE ( penal(1:nwlt), STAT=io_status )
                 CALL test_alloc( io_status, 'penal in main', nwlt )
              END IF
           ELSE
              ALLOCATE ( penal(1:nwlt), STAT=io_status )
              CALL test_alloc( io_status, 'penal in main', nwlt )
           END IF
           penal = user_chi(nwlt,t)
        END IF

        CALL user_pre_process
        !
        ! we always need to call user_initial_conditions so even when adapting to  intital data the other variables
        !   get filled in DG
        !
        CALL user_initial_conditions( u, nwlt, n_var, t, scl, scl_fltwt, iter)

        IF(sgsmodel /= 0) CALL SGS_initial_conditions (u, nwlt, n_var, t, scl, scl_fltwt)

        CALL user_post_process
        !
        ! CAll case specific statistics routine
        ! Last arg 0 for adapting to IC then 1 for main integration loop
        !
        !CALL resolved_stats(u, j_mn, 0)

        ! what about the scales no model or cvs cases.. we still want the run time stats..
        IF(print_SGS_stats .OR. sgsmodel /= 0) CALL SGS_stats ( u ,j_mn, 0)
        CALL user_stats(u, j_mn, 0)


        !
        ! Check Exact Solution if it exists before adapting grid. This will always show zero in the case  
        ! where the IC is defined by the same analytical equations as the exact solution
        !
        ! We only compare the actual variable that are being adapted to (those being interpolated to new grid each time though 
        ! this loop)
        !
        IF(n_var_exact > 0) THEN
           IF ( par_rank.EQ.0 .AND. BTEST(verb_level,2) ) THEN
              PRINT *,''
              PRINT *,'===================================================='
              PRINT *,'== Before adapting grid                           =='
           END IF
           CALL Check_Exact_Soln(u,  u_ex,  t, scl,(n_var_exact_soln(:,0) .AND. n_var_interpolate(:,0)) )
        END IF

        !
        !============ Additional variables to base grid adaptation
        ! 
        !

        IF( n_var_additional > 0 ) CALL user_additional_vars(t, 0 ) 
        IF( hypermodel /= 0 ) THEN
  	       !NOTE: arrays allocation could be modified, once the stuctrue of the code reorganized such that
		   !      there is no mutual use of hyperbolic and user_case modules
           IF(ALLOCATED(c_sound))DEALLOCATE(c_sound)
           ALLOCATE ( c_sound(1:nwlt), STAT=io_status )
           CALL test_alloc( io_status, 'c_sound in main', nwlt )
           c_sound = user_sound_speed(u(1:nwlt,1:n_var),n_var,nwlt)
           CALL hyper_additional_vars(u,nwlt,c_sound,0)
        END IF

        CALL user_scalar_vars( 0 ) !update any user defined scalar variables
        
        CALL scales (0, u, nwlt, n_var, n_var_adapt(:,0), n_var_index(:), scl, scl_fltwt)


        !
        !************ Forward Wavelet Transform ****************
        ! we do fwd transform on interpolate and adapt variables
        !
        !debugging
!!$        u = 0.0_pr
!!$        DO i=1,dim
!!$           WRITE(*,'("ixyz(",I1,")=",$)') i
!!$           READ *, ixyz(i)
!!$        END DO
!!$        ixyz = 1
!!$        PRINT *, ixyz(1:dim)
!!$        CALL get_indices_by_coordinate(1,ixyz(1:dim),j_lev,i1(1), 0)
!!$
!!$        PRINT *, x(i1(1),:)
!!$        u(i1(1),1)=1.0_pr
!
!        !TEST
!        DO i = 1,n_var
!        tmp = SUM(u(:,i)*u(:,i))
!        IF( tmp > 0.0_pr) WRITE(*,'( "before trns u(:,",i1,") ",es30.20 )') i, SQRT(tmp)
!        END DO
!        !END TEST
!!$        CALL update_db_from_u(  u , nwlt ,n_var  , 1, n_var , 1 )
!!$        DO i=1,n_var
!!$           WRITE (*,'("MAXVAL(ABS(u_in(:),",I2,"))=",E12.5)') i, MAXVAL(ABS(u(:,i)))
!!$        END DO        
!!$        CALL count_DB(.TRUE.,2,6, VERB=.TRUE.)


        CALL c_wlt_trns_mask (u, u, n_var, n_var_interpolate(:,0), n_var_adapt(:,0), .TRUE., .FALSE.,&
             nwlt, nwlt, j_lev, j_lev, HIGH_ORDER, WLT_TRNS_FWD, DO_UPDATE_DB_FROM_U, DO_NOT_UPDATE_U_FROM_DB)

        
        IF ( BTEST(verb_level,3) ) THEN
           DO i=1,n_var
              WRITE (*,'("MAXVAL(ABS(u_out(:),",I2,"))=",E22.15)') i, MAXVAL(ABS(u(:,i)))
           END DO
        END IF

!!$        !        PRINT *, 'error=',MAXVAL(ABS(u-u_ex))
!!$        !TEST
!!$        CALL update_u_from_db(  u_ex , nwlt ,n_var  , 1, n_var , 1 )
!!$
!!$		PRINT *, MAXVAL(ABS(u-u_ex))
!!$        DO i = 1, n_var
!!$        tmp = SUM(u(:,i)*u(:,i))
!!$        IF( tmp > 0.0_pr) WRITE(*,'( "after trns u(:,",i1,") ",es30.20 )') i, SQRT(tmp)
!!$        END DO
!!$        STOP
!!$        !END TEST

        !
        ! DEBUGGING, Place a single point in wlt space
        ! 
        !        CALL place_pt_wlt_space(j_mx)

        !
        ! re-allocate u_old array  (for wrk version)
        !
        IF ( TYPE_DB == DB_TYPE_WRK ) THEN
           IF( nwlt /= nwlt_old ) THEN
              IF( ASSOCIATED(u_old) ) DEALLOCATE (u_old)
              ALLOCATE ( u_old(1:nwlt,1:n_var), STAT=io_status )
              CALL test_alloc( io_status, 'u_old in main', nwlt )
              u_old = 0.0_pr
           END IF
           u_old = u
        END IF

        !--Save the solution at the most recent time step
        CALL save_solution(nwlt, n_var, scl, iwrite, VERBLEVEL=verb_level)

        !************ Analyzing wavelet coefficients ***************
        !*        Start grid adaptation criteria                   *
        !*    Coefficients below EPS threshold are set to zero     *
        !*                                                         *
        !***********************************************************


        j_lev_old = j_lev ! Save j_lev to see if we need to update grid indices

        !
        ! adapt to new grid with values of u still in db from above
        ! inverse trans at end of adapt grid and get values back into u
        !

        CALL adapt_grid( 0  ,eps , j_lev_old, j_lev_a, j_mn, j_mx, nxyz_a, ij_adj,&
                adj_type, scl ,new_grid)
        
        IF( BTEST(debug_level,0) ) THEN        ! print statistics from MAIN
           num1_tmp = num1_tmp + 1             ! number of adapt_grid calls
           IF (par_rank.EQ.0) THEN
              WRITE (6, '(" ")')
              WRITE (6, '("********************************************************************************")')
              WRITE (6, &
                   '("* j_lev = ", i3, " nwlt  = ", i10,"(% = ", f10.4, ") nxyz = (",i6,",",i6,",",i6,")")') &
                   j_lev, nwlt_global, wlt_comp(nwlt_global), nxyz(1:3)
              WRITE (6, &
                   '("*    nwlt + nghost  = ", i10,"(% = ", f10.4, ")" )') &
                   nwlt_p_ghost, wlt_comp(nwlt_p_ghost)
              WRITE (6, '("********************************************************************************")')
              WRITE (6, '(" ")')
           END IF
        END IF


        !-- Update weights for area/volume associated with each wlt collocation point
        CALL set_weights()

        IF (imask_obstacle) THEN  
           !--Set up distance function
           IF(use_dist) THEN
              IF(ALLOCATED(dist) ) THEN
                 IF(size(dist) /= nwlt) THEN 
                    DEALLOCATE(dist)
                    ALLOCATE ( dist(1:nwlt), STAT=io_status )
                    CALL test_alloc( io_status, 'dist in main', nwlt )
                 END IF
              ELSE
                 ALLOCATE ( dist(1:nwlt), STAT=io_status )
                 CALL test_alloc( io_status, 'dist in main', nwlt )
              END IF
              !Always calculate distance funciton during initial condition, since mesh is changing
#ifdef DISTANCE_FUNCTION
              CALL user_dist (nwlt, t, DISTANCE=dist)
              IF (interpolate_dist .OR. ndist>0 ) u(:,ndist) = dist
#else 
              IF (par_rank.EQ.0) THEN
                 WRITE(*,'("ERROR: to use this functionality the user need to define:")' ) 
                 WRITE(*,'("optional flag DISTANCE_FUNCTION and SUBROUTINE user_dist")' ) 
              END IF
              CALL parallel_finalize; STOP
#endif
           END IF
           !--Set up mask
           IF(ALLOCATED(penal) ) THEN
              IF(size(penal) /= nwlt) THEN 
                 DEALLOCATE(penal)
                 ALLOCATE ( penal(1:nwlt), STAT=io_status )
                 CALL test_alloc( io_status, 'penal in main', nwlt )
              END IF
           ELSE
              ALLOCATE ( penal(1:nwlt), STAT=io_status )
              CALL test_alloc( io_status, 'penal in main', nwlt )
           END IF
           penal = user_chi(nwlt,t)
        END IF

        !DO i=1,n_var
        !   WRITE(*,'("before Check_Exact_Soln MAXVAL(ABS(u(1:",i6,",",i2,")) = ", f15.7 )' ) nwlt, i, MAXVAL(ABS(u(1:nwlt,i)))
        !END DO
        !PRINT *,'before Check_Exact_Soln MAXVAL(ABS(u)) ',MAXVAL(ABS(u)) 
        IF (verb_level.GT.0.AND.n_var_exact > 0.AND.par_rank.EQ.0) &
             PRINT *,'before Check_Exact_Soln MAXVAL(ABS(u_ex)) ',MAXVAL(ABS(u_ex)) 
        
        !
        !Check Exact Solution if it exists
        !
        ! We only compare the actual variable that are being adapted to (those being interpolated to new grid each time though 
        ! this loop)
        !

        IF(n_var_exact > 0) THEN
           IF ( par_rank.EQ.0 .AND. BTEST(verb_level,2) ) THEN
              PRINT *,''
              PRINT *,'===================================================='
              PRINT *,'== After adapting grid                            =='
           END IF
           CALL Check_Exact_Soln(u,  u_ex,  t, scl,(n_var_exact_soln(:,0) .AND. n_var_interpolate(:,0)), &
                VERB=BTEST(verb_level,2) )
           !PRINT *, MAXVAL(u-u_ex), MINVAL(u-u_ex)
        END IF
        
        !************ Write to _log file during initial adaptation *************
        ! time is negative for initial adaptation
        ! then for iter>1 it is the real loop time
        CALL write_log( j_mx, j_lev, dim, -iter*1.0_pr, dt*0.0_pr, nxyz, Nwlt_lev(:,1), nwlt_global, nwlt, eps, &
             n_transfer_send, n_transfer_recv, nwlt_p_ghost )
#ifdef DATA_ASSIMILATION
        IF(lg_adjoint .AND. .NOT.n_var_adapt(2,0)) new_grid = .TRUE. 
        IF(lg_adjoint .AND. .NOT.n_var_adapt(2,0) .AND.  iter <= j_lev+2 .AND. iter /= -1 .AND. j_mx_adj < MIN(j_IC,j_mx) .AND. IC_adapt_grid) THEN
           j_mx_adj = MIN(j_IC,j_mx)
#else
        IF(.NOT.(new_grid .AND. iter <= j_lev+2 .AND. iter /= -1) .AND. j_mx_adj < j_mx .AND. IC_adapt_grid) THEN
           j_mx_adj = j_mx
#endif
           new_grid = .TRUE.
           iter = 1
        END IF
        
        IF( BTEST(debug_level,0) ) PAUSE

        IF(IC_SINGLE_LOOP) EXIT INITIAL_CONDITIONS_LOOP

#ifdef DATA_ASSIMILATION
        !WRITE(*,FMT="(A40,X,I3,X,A,X,I3,X,A,X,I3,X,A,X,I3)") 'END INITIAL_CONDITIONS_LOOOP: iter =', iter, ' iwrite=', iwrite, ' j_lev=',j_lev, ' j_zn = ', j_zn
        iwrite = iwrite + 1
#endif

     END DO INITIAL_CONDITIONS_LOOP
#ifdef DATA_ASSIMILATION
     iwrite = iwrite - 1     

     !OLEG: convergence test  
!!$     CALL user_stats(u, j_mn, -1) ! final user defined staistics output
!!$     iwrite = iwrite + 1
!!$     eta = 0.01_pr*eta !tmp
!!$     END DO ETA_LOOP
!!$     eps = 0.1_pr*eps !tmp
!!$  END DO EPS_LOOP
!!$  eps = 10.0_pr * eps
!!$  !  eta = 100.0_pr * eta
!!$  eta = eps**2
#endif
  
  
  
     IF( BTEST(debug_level,0).AND.par_rank.EQ.0 ) &                    ! print statistics from MAIN
          PRINT *, 'DEBUG: number of adapt_grid call is ', num1_tmp    !
!!$     PAUSE
     
     !
     ! release memory of initial data buffer if it hasn't already been done
     ! If iter=-1 then it was set this way in read_initial_data() to tell us that the buffer is already deallocated.
     !
     IF( IC_from_file .AND. IC_file_fmt /= 0 .AND. iter /= -1) THEN
        iter = -1
        CALL read_initial_data( u, nwlt , dim, &
             IC_filename, IC_file_fmt, iter ,j_IC)
     END IF

     IF( iter /= -1) THEN
        iter = -1
        CALL user_initial_conditions( u, nwlt, n_var, t, scl, scl_fltwt, iter)
     END IF
  END IF ! from IC or restart
  j_mn = j_mn_evol ! resets j_mn to the right value of j_mn after IC\
  j_mx_adj = j_mx
#ifdef DATA_ASSIMILATION
  j_mx_adj = MIN(j_IC,j_mx)
#endif

  IF (par_rank.EQ.0) PRINT *,'End of Initial Conditions'


  CALL timer_stop(2)

  
#ifdef USE_DEBUG_TIMERS_S
#define USE_DEBUG_TIMERS
#endif
  
#ifdef USE_DEBUG_TIMERS
  ! output some timing
  tmp1 = timer_val(1)
  tmp2 = timer_val(2)
  CALL parallel_global_sum( REAL=tmp1 )
  CALL parallel_global_sum( REAL=tmp2 )
  IF (par_rank.EQ.0) THEN
     WRITE (6,'("CPU TIME (inputs and initializations) at #0 :", es12.5,", sum:",es12.5)') timer_val(1), tmp1
     WRITE (6,'("CPU TIME (adapt to initial conditions) at #0:", es12.5,", sum:",es12.5)') timer_val(2), tmp2
  END IF
#endif
  !*****************************************************************
  !*  End of Adapt wavelet grid to initial conditions section      *
  !*****************************************************************


  !
  ! Ensure that the data array is zero for variables that were not adapted to in initial conditions section
  ! or in the case of restart we will zero out vars that will not be adapted to in main time integration loop
  !
  DO ie = 1,n_var
     IF( IC_restart) THEN
        IF( .NOT. (n_var_interpolate(ie,1) .OR. n_var_adapt(ie,1)) ) u(:,ie) = 0.0_pr
     ELSE
        IF( .NOT. (n_var_interpolate(ie,0) .OR. n_var_adapt(ie,0)) ) u(:,ie) = 0.0_pr
     END IF
  END DO
  

  
  !********* Save stats to _log file after last adapt, before first time integration loop *******************
  CALL write_log( j_mx, j_lev, dim, t, dt, nxyz, Nwlt_lev(:,1), nwlt_global, nwlt, eps, &
       n_transfer_send, n_transfer_recv, nwlt_p_ghost )
    
  ! reuse all the timers for the main loop
  CALL timer_reset_all
  
  !*************************************************************
  !  BEGIN TIME INTEGRATION LOOP
  !*************************************************************
  IF (par_rank.EQ.0) WRITE (6,'("Start Time Integration"/)')
  new_grid = .TRUE.
  
  IF (IC_restart_mode .eq. 2) THEN 
     CALL save_solution(nwlt, n_var, scl, iwrite, VERBLEVEL=verb_level)
     iwrite = iwrite + 1
  END IF
  
  !*********** Initial Conditions ************
  IF(IC_restart) THEN
     twrite = t + dtwrite 
     first_loop_after_restart = .TRUE.
  ELSE
     it = 0 ; t = t_begin ; twrite = t_begin + dtwrite;
     first_loop_after_restart = .FALSE.
     iwrite = iwrite + 1
  END IF
  
  
  first_time_integration_iter = .TRUE.

  ALLOCATE ( force(1:dim), STAT=io_status )
  CALL test_alloc( io_status, 'force in main', dim )
  force = 0.0_pr
  
  
  ! total time till the end of time integration, CUMULATIVE
  CALL timer_start(1)


  !--Set weights for area/volume associated with each wlt collocation point
  CALL set_weights()
  

  DO WHILE (t < t_end)
     ! main loop timer (reseted to count each timestep separately)
     CALL timer_reset(2); CALL timer_start(2)
     
     it = it + 1
     WRITE(it_string,'(I4.4)') it
     IF (par_rank.EQ.0) THEN
        WRITE (*,'(" ")')
        WRITE (*,'(" ")')
        WRITE (*,'(A,I10)') 'Begin Iteration ',it
     END IF

     
     !debug
     IF ( BTEST(debug_level,0) ) THEN
        IF (par_rank.EQ.0) WRITE (*,'(A)') 'Field var stats, begining of time step'
        DO i=1,n_var
           tmp1 = MINVAL(u(:,i)); tmp2 = MAXVAL(u(:,i))
           CALL parallel_global_sum( REALMINVAL=tmp1 )
           CALL parallel_global_sum( REALMAXVAL=tmp2 )
           IF (par_rank.EQ.0) WRITE(*,'(" MINMAX(u_i) ", A, 2(es15.8,1X) )' ) u_variable_names(i), tmp1, tmp2
           IF (par_rank.EQ.0) WRITE(*,'(" ")' )
        END DO
     END IF
     !end debug   
     
     
     !
     ! set eps from eps_init and eps_run
     !
     IF( ABS(eps - eps_run) > 1.0D-10 ) THEN
        eps = eps + (eps_run - eps_init)/ eps_adapt_steps
     END IF
     IF (verb_level.GT.0.AND.par_rank.EQ.0) PRINT *,'eps = ', eps
     
     !
     ! Setup up any model filter masks needed 
     !
     IF(  sgsmodel >= 1 .OR. ExplicitFilter .OR. ExplicitFilterNL ) THEN
        IF (par_rank.EQ.0 .AND. verb_level.GT.0) PRINT *,'Finding mdl filter masks '
        
        CALL make_mdl_filts( u(1:nwlt,1:n_integrated), n_integrated, scl, &
             mdl_filt_type_grid, mdl_filt_type_test ) 
        
        IF (par_rank.EQ.0 .AND. verb_level.GT.0) THEN
           PRINT *,'In main() '
           PRINT *,'SUM(u_g_mdl_gridfilt_mask) ', SUM(u_g_mdl_gridfilt_mask)
           PRINT *,'SUM(u_g_mdl_testfilt_mask) ', SUM(u_g_mdl_testfilt_mask)
           PRINT *,'SUM(u_g_mdl_gridfilt_mask-u_g_mdl_testfilt_mask)',SUM(u_g_mdl_gridfilt_mask-u_g_mdl_testfilt_mask)
        END IF
     END IF


     !
     ! Apply grid filter if an explicit filter is called for 
     !
     IF ( ExplicitFilter ) THEN
        IF (par_rank.EQ.0 .AND. verb_level.GT.0) PRINT *,'Apply grid filter as an explicit filter'
        
        CALL mdl_filt_by_type(u(1:nwlt,1:n_integrated),n_integrated,GRIDfilter)
        
        IF(par_rank.EQ.0.AND.GRIDfilter == MDL_FILT_LPlocal) THEN
           WRITE(*,'("WARNING: low pass filter is not a projection filter and should not be used to explicitly filter u")')
        END IF
     END IF
     
     !
     ! Calculate any SGS model forcing
     ! 
     IF(sgsmodel /= 0) THEN
        !-- SGS model step
        IF (par_rank.EQ.0 .AND. verb_level.GT.0) PRINT *,'--------------- SGS model step ---------------------'
        CALL sgs_model_setup( nwlt )

        IF(SGS_TimeIntegrationType /= 1 ) THEN
           CALL user_sgs_force(u, nwlt) 
        ELSE IF(SGS_TimeIntegrationType == 1 .AND.  N_SGS_TimeIntegration < n_integrated ) THEN
           CALL user_sgs_force(u, nwlt) 
           sgs_mdl_force(:,1:N_SGS_TimeIntegration) = 0.0_pr
        END IF

     END IF
     
     !
     ! Calculate hyperbolic module variables
     ! 
     IF(hypermodel /= 0) THEN
        !NOTE: arrays allocation could be modified, once the stuctrue of the code reorganized such that
        !      there is no mutual use of hyperbolic and user_case modules
        IF(ALLOCATED(c_sound))DEALLOCATE(c_sound)
        ALLOCATE ( c_sound(1:nwlt), STAT=io_status )
        CALL test_alloc( io_status, 'c_sound in main', nwlt )
        c_sound = user_sound_speed(u(1:nwlt,1:n_var),n_var, nwlt)
        CALL hyper_pre_process(u,nwlt,n_var,c_sound)
     END IF

     IF (imask_obstacle) THEN  
        !--Set up distance function
        IF(use_dist) THEN
           IF(ALLOCATED(dist) ) THEN
              IF(size(dist) /= nwlt) THEN 
                 DEALLOCATE(dist)
                 ALLOCATE ( dist(1:nwlt), STAT=io_status )
                 CALL test_alloc( io_status, 'dist in main', nwlt )
              END IF
           ELSE
              ALLOCATE ( dist(1:nwlt), STAT=io_status )
              CALL test_alloc( io_status, 'dist in main', nwlt )
           END IF
           IF (interpolate_dist) THEN
              dist = u(:,ndist)
           ELSE
#ifdef DISTANCE_FUNCTION
              CALL user_dist (nwlt, t+dt, DISTANCE=dist)
              IF(ndist > 0) u(:,ndist) = dist
#else 
              IF (par_rank.EQ.0) THEN
                 WRITE(*,'("ERROR: to use this functionality the user need to define:")' ) 
                 WRITE(*,'("optional flag DISTANCE_FUNCTION and SUBROUTINE user_dist")' ) 
              END IF
              CALL parallel_finalize; STOP
#endif
           END IF
        END IF
        !--Set up mask
        IF(ALLOCATED(penal) ) THEN
           IF(size(penal) /= nwlt) THEN 
              DEALLOCATE(penal)
              ALLOCATE ( penal(1:nwlt), STAT=io_status )
              CALL test_alloc( io_status, 'penal in main', nwlt )
           END IF
        ELSE
           ALLOCATE ( penal(1:nwlt), STAT=io_status )
           CALL test_alloc( io_status, 'penal in main', nwlt )
        END IF
        penal = user_chi(nwlt,t+dt)
     END IF
     
     CALL user_pre_process
     
     !
     !  time advancement 
     !
     CALL timer_reset(4); CALL timer_start(4)
     IF( time_integration_method == TIME_INT_Krylov ) THEN

        CALL kry_time_step (u(1:nwlt,1:n_integrated), u(1:nwlt,n_var_pressure), nwlt, dt, t)

     ELSE IF( time_integration_method == TIME_INT_Crank_Nicholson ) THEN
        
        CALL time_adv_cn (u(1:nwlt,1:n_integrated), u(1:nwlt,n_var_pressure), nwlt, t)
        
     ELSE IF( time_integration_method == TIME_INT_RK2 ) THEN
        
        CALL time_adv_rk2 (u(1:nwlt,1:n_integrated), u(1:nwlt,n_var_pressure), nwlt, dt, t)
        
     ELSE IF( time_integration_method == TIME_INT_RK4 ) THEN
        
        CALL time_adv_rk4 (u(1:nwlt,1:n_integrated), u(1:nwlt,n_var_pressure), nwlt, dt, t)
        
     ELSE IF( time_integration_method == TIME_INT_RK2TVD ) THEN
        
        CALL time_adv_rk2tvd (u(1:nwlt,1:n_integrated), u(1:nwlt,n_var_pressure), nwlt, dt, t)
        
     ELSE IF( time_integration_method == TIME_INT_RK3TVD ) THEN
        
        CALL time_adv_rk3tvd (u(1:nwlt,1:n_integrated), u(1:nwlt,n_var_pressure), nwlt, dt, t)
        
     ELSE
        PRINT *,' Unsupported Time Integration Method: time_integration_method = ', time_integration_method
        PRINT *,' Exiting ...'
        CALL parallel_finalize; STOP
     END IF
     CALL timer_stop(4)
     
     CALL user_post_process
     
     !debug
     IF ( BTEST(debug_level,0) ) THEN
        IF (par_rank.EQ.0) PRINT *,'Field var stats, After time advancement'
        DO i=1,n_var
           tmp1 = MINVAL(u(:,i)); tmp2 = MAXVAL(u(:,i))
           CALL parallel_global_sum( REALMINVAL=tmp1 )
           CALL parallel_global_sum( REALMAXVAL=tmp2 )
           IF (par_rank.EQ.0) WRITE(*,'(" MINMAX(u_i) ", A, 2(es15.8,1X) )' ) u_variable_names(i), tmp1, tmp2
           IF (par_rank.EQ.0) WRITE(*,'(" ")' )
        END DO
     END IF
     !end debug  

     !--Calculate cfl
     CALL timer_reset(8); CALL timer_start(8)
     CALL cal_cfl (u(:,1:n_integrated), cfl)
     IF (verb_level.GT.0.AND.par_rank.EQ.0) PRINT *,'CFL = ', cfl
     IF (cfl>cflmax) THEN !--Limit time step
        IF (par_rank.EQ.0) PRINT *,'WARNING: dt being limited by cfl>cflmax: dt, dt_new',dt , MIN(dt * cflmax/cfl,dt_original)
        dt = MIN(dtmax,dt * cflmax/cfl,dt_original)
        !OLEG. I have commented this out because I want to dump every time step, without limiting dt by dt_write..
        ! we will come up with an input flag for this as soona s I can. D.G.
        !IF( dt >= dtwrite ) THEN
        !   dt = dtwrite !limit dt based on dtwrite
        !   PRINT *,'WARNING: dt being limited by dtwrite, setting dt = dtwrite '
        !END IF
        cfl = cflmax
        dt_changed = .TRUE.
     ELSE IF( dt_changed .AND. t-t_begin<=t_adapt) THEN
        dt = MIN(1.5_pr*dt, dt_original, dt * cflmax/cfl)
        IF (par_rank.EQ.0) PRINT *, 'dt is increased toward original: dt=',dt 
        dt_changed = .FALSE.
        IF( dt < dt_original) dt_changed = .TRUE.
     END IF
     CALL timer_stop(8)
     
     
     !
     !============ Additional variables to base grid adaptation put here, e.g. u(1:nwlt,ie+1)
     !

     IF( n_var_additional > 0 ) CALL user_additional_vars( t, 1 ) 
     IF ( hypermodel > 0 ) CALL hyper_additional_vars(u(1:nwlt,1:n_var),nwlt,u(1:nwlt,1)*0.0_pr,1)

     CALL user_scalar_vars( 1) !update any user defined scalar variables

     CALL scales(1,u(1:nwlt,1:n_var) , nwlt, n_var, n_var_adapt(:,1),  &
          n_var_index(:),scl, scl_fltwt)

     IF ( BTEST(debug_level,0) ) THEN
        DO i=1,n_var
           tmp1 = MINVAL(u(1:nwlt,i)); tmp2 = MAXVAL(u(1:nwlt,i))
           CALL parallel_global_sum( REALMINVAL=tmp1 )
           CALL parallel_global_sum( REALMAXVAL=tmp2 )
           IF (par_rank.EQ.0) WRITE (*,'(I4,A,F15.5,A,3(F15.5,1X) )') &
                i,' scl=',scl(i),' Uminmax=',tmp1,tmp2,MAX(ABS(tmp1),ABS(tmp2))
        END DO
     END IF
     
     !
     IF (verb_level.GT.0.AND.t >= twrite.AND.par_rank.EQ.0) &
          WRITE (6, '("iwrite=", i4, " twrite=", es9.2, " t=", es9.2, " eps=",es9.2)') iwrite,  &
          timescl*twrite,  timescl*t, eps  
        
     !
     ! Calculate any statitics
     !
     !CALL resolved_stats(u, j_mn, 0)
     IF(print_SGS_stats .OR. sgsmodel /= 0) CALL SGS_stats ( u ,j_mn, 0)
     CALL user_stats(u ,j_mn, 1)


     !************ Forward Wavelet Transform ****************
     ! fwd wlt transform is done on adapted, interpolated and saved variables (if we are saving this time step)
     ! The inverse wlt transform at the end of the loop is only done on the interpolated variables
     ! 
     CALL timer_reset(7); CALL timer_start(7)
     CALL c_wlt_trns_mask(u, u, n_var, n_var_interpolate(:,1), (n_var_adapt(:,1) .OR. (t > twrite .AND. n_var_save )), .TRUE. ,&
          .FALSE., nwlt, &
          nwlt, j_lev, j_lev, HIGH_ORDER, WLT_TRNS_FWD, DO_UPDATE_DB_FROM_U, DO_NOT_UPDATE_U_FROM_DB)
     CALL timer_stop(7)

     !
     ! This print out of wavelet coefficients is only for wrk version,
     ! because in db versions the wlt coeffients are not restored to u at this point.
     ! (See lart arg of c_wlt_trns_mask() in call above: DO_NOT_UPDATE_U_FROM_DB
     !
     IF ( TYPE_DB == DB_TYPE_WRK .AND. BTEST(debug_level,0) ) THEN
        DO i=1,n_var
           WRITE (*,'(I4,A,F15.5,A,3(F15.5,1X) )') &
                i,' scl=',scl(i),' COEFminmax=',MINVAL(u(1:nwlt,i)),MAXVAL(u(1:nwlt,i)),MAXVAL(ABS(u(1:nwlt,i)))
        END DO
     END IF
     !
     ! re-allocate u_old array  (for wrk version)
     !	
     IF ( TYPE_DB == DB_TYPE_WRK ) THEN
        IF( nwlt /= nwlt_old .OR. first_loop_after_restart ) THEN
           IF( ASSOCIATED(u_old) ) DEALLOCATE (u_old)
           ALLOCATE   (u_old(1:nwlt,1:n_var), STAT=io_status )
           CALL test_alloc( io_status, 'u-old in main', nwlt*n_var )
           u_old = 0.0_pr
        END IF
        u_old = u
     END IF

     !
     !check control file <run name>.forcewrite in working directory,
     !
     IF (par_rank.EQ.0 .AND. verb_level.GT.0) PRINT *,'Checking for forcewrite file:',TRIM(file_gen)//'forcewrite' 
     INQUIRE (FILE = TRIM(file_gen)//'forcewrite',  EXIST=test_forcewrite)
     IF( test_forcewrite.AND.par_rank.EQ.0 ) PRINT *,'Forcing write of data'

     IF (t  >= twrite .OR. test_forcewrite ) THEN  


        !--Save the solution
        CALL save_solution(nwlt, n_var, scl, iwrite, VERBLEVEL=verb_level)
        !--Save the solution in wavelet space
        !        IF (INT (t/dtwrite) > 0 .AND. ABS (t - INT (t/dtwrite)*dtwrite) < dt) THEN  
        !IF (wlog) WRITE (6,*) "Saving data ..."
        twrite = twrite + dtwrite
        iwrite = iwrite + 1

  
        !
        !do_Sequential_run
        !
        IF (do_Sequential_run .AND. par_rank.EQ.0) THEN
           CALL DATE_AND_TIME( DATE_TIME_ZONE(1), DATE_TIME_ZONE(2), DATE_TIME_ZONE(3), DATE_TIME )
           OPEN  (UNIT=UNIT_SEQUENTIAL_RUN, FILE = TRIM(file_gen)//'sequentialrun.log', FORM='formatted', STATUS='replace', IOSTAT=io_status)
           WRITE (UNIT_SEQUENTIAL_RUN, FMT='(I12)')  iwrite-1
           WRITE (UNIT_SEQUENTIAL_RUN, FMT='(3A, 2(I2.2,A), I4.4, A, I6, 2A, 3(I2.2,A), I3.3)') & 
                                                     'This is the last station number (iwrite) which was saved for file_gen=',TRIM(file_gen),&
                                                     ' on ',DATE_TIME(2),'/',DATE_TIME(3),'/',DATE_TIME(1),' (UTC:',DATE_TIME(4),')', & 
                                                     ' at ',DATE_TIME(5),':',DATE_TIME(6),':',DATE_TIME(7),'.',     DATE_TIME(8) 
           CLOSE (UNIT_SEQUENTIAL_RUN)
        END IF


        !
        !do_const_diss (Time-Varying Thresholding)
        !
        IF (do_const_diss .AND. par_rank.EQ.0) THEN
           OPEN  (UNIT=UNIT_const_SGS_dissipation, FILE = TRIM(file_gen)//'const_SGS_dissipation.log', FORM='formatted', STATUS='replace', IOSTAT=io_status)
           WRITE (UNIT_const_SGS_dissipation, FMT='(F12.6)')      eps
           WRITE (UNIT_const_SGS_dissipation, FMT='(A,I12,2A)')   'This is the last  threshold-level (eps) which was calculated at station number (iwrite)=', iwrite-1,'  for file_gen=',TRIM(file_gen)
           CLOSE (UNIT_const_SGS_dissipation)
        END IF


     END IF

     
     
     !********* Save stats to _log file*******************
     ! time is since top of main loop for first iteration,
     ! then for iter>1 it is the real loop time from this point.
     CALL write_log( j_mx, j_lev, dim, t, dt, nxyz, Nwlt_lev(:,1), nwlt_global, nwlt, eps, &
          n_transfer_send, n_transfer_recv, nwlt_p_ghost )
     
     
     
     
     !************ Analyzing wavelet coefficients ***************
     !*        Start grid adaptation criteria                   *
     !*    Coefficients below EPS threshold are set to zero     *
     !*                                                         *
     !***********************************************************

     !DG1206 adapt to new grid with values of u stil in db from above
     !DG1206 inverse trans at end of adapt grid and get values back into u


     CALL adapt_grid( 1  ,eps , j_lev_old, j_lev_a, j_mn, j_mx, nxyz_a, ij_adj,&
          adj_type, scl , new_grid)
     
     
     IF ( BTEST(verb_level,2) ) THEN
        IF (par_rank.EQ.0) PRINT *,'Field var stats, After grid adaptation. (Interpolation to new grid)'
        DO i=1,n_var
           tmp1 = MINVAL(u(:,i)); tmp2 = MAXVAL(u(:,i))
           CALL parallel_global_sum( REALMINVAL=tmp1 )
           CALL parallel_global_sum( REALMAXVAL=tmp2 )
           !WRITE(*,'(" ||u_i||L2   ", A, es15.8)' ) u_variable_names(i), SQRT ( SUM( (u(:,i)**2)*dA )/ sumdA  )
           IF (par_rank.EQ.0) WRITE(*,'(" MINMAX(u_i) ", A, 2(es15.8,1X) )' ) u_variable_names(i), tmp1, tmp2
           IF (par_rank.EQ.0) WRITE(*,'(" ")' )
        END DO
     END IF
     
     
     ! main loop timer, from the beginning of time integration loop, for each timestep
     CALL timer_stop(2)
     
     
     IF (wlog.AND.par_rank.EQ.0) WRITE (6, '("t= ", es13.6, " dt= ", es13.6, " cfl=", es13.6, " cpu= ", es13.6, " j_lev= ", i2, " nwlt= ", i12/)') &
          t, dt, cfl, timer_val(2), j_lev, nwlt_global
     
     
     IF( BTEST(debug_level,0) ) THEN        ! print statistics from MAIN
        IF (par_rank.EQ.0) THEN
           PRINT *,''
           WRITE (6, '("********************************************************************************")')
           WRITE (6, '("* It          = ",i6     )') it  
           WRITE (6, '("* t           = ", es9.2 )') timescl*t
           WRITE (6, '("* dt          = ", es9.2 )') timescl*dt
           WRITE (6, '("* j_lev       = ", i2    )') j_lev
           WRITE (6, '("* nwlt        = ", i10," (% = ",f10.4, ")" )')  nwlt_global, wlt_comp(nwlt_global)
           WRITE (6, '("* nwlt+nghost = ", i10," (% = ",f10.4, ")" )') &
                nwlt_p_ghost, wlt_comp(nwlt_p_ghost)
           WRITE (6, '("* nxyz        = (", i4,",",i4,",",i4,")")') nxyz(1:3)
#ifdef USE_DEBUG_TIMERS
           WRITE (6, '("* cpu         = ", es8.2 )') timer_val(2)
           WRITE (6, '("* cpu/nwlt    = ", es8.2 )') timer_val(2) / real(nwlt_global)
           WRITE (6, '("* cpu/nwlt+nghost = ", es8.2 )') timer_val(2) / real(nwlt_p_ghost)
           WRITE (6, '("* cpu - time integration timestep time at processor 0, check _log for details  *")')
#endif
           WRITE (6, '("********************************************************************************")')
           PRINT *,''
        END IF
     END IF


     !--Update weights for area/volume associated with each wlt collocation point
     CALL set_weights()

     IF (imask_obstacle) THEN  
        !--Set up distance function
        IF(use_dist) THEN
           IF(ALLOCATED(dist) ) THEN
              IF(size(dist) /= nwlt) THEN 
                 DEALLOCATE(dist)
                 ALLOCATE ( dist(1:nwlt), STAT=io_status )
                 CALL test_alloc( io_status, 'dist in main', nwlt )
              END IF
           ELSE
              ALLOCATE ( dist(1:nwlt), STAT=io_status )
              CALL test_alloc( io_status, 'dist in main', nwlt )
           END IF
           IF (interpolate_dist) THEN
              dist = u(:,ndist)
           ELSE
#ifdef DISTANCE_FUNCTION
              CALL user_dist (nwlt, t, DISTANCE=dist)
              IF(ndist > 0) u(:,ndist) = dist
#else 
              IF (par_rank.EQ.0) THEN
                 WRITE(*,'("ERROR: to use this functionality the user need to define:")' ) 
                 WRITE(*,'("optional flag DISTANCE_FUNCTION and SUBROUTINE user_dist")' ) 
              END IF
              CALL parallel_finalize; STOP
#endif
           END IF
        END IF
        !--Set up mask
        IF(ALLOCATED(penal) ) THEN
           IF(size(penal) /= nwlt) THEN 
              DEALLOCATE(penal)
              ALLOCATE ( penal(1:nwlt), STAT=io_status )
              CALL test_alloc( io_status, 'penal in main', nwlt )
           END IF
        ELSE
           ALLOCATE ( penal(1:nwlt), STAT=io_status )
           CALL test_alloc( io_status, 'penal in main', nwlt )
        END IF
        penal = user_chi(nwlt,t)
     END IF
     
     !
     !--Calculate error
     !
     ! Check Exact Solution if it exists
     IF (n_var_exact > 0) THEN
        IF (BTEST(verb_level,2).AND.par_rank.EQ.0) THEN
           PRINT *,''
           PRINT *,'===================================================='
        END IF
        CALL Check_Exact_Soln(u,  u_ex,  t, scl,n_var_exact_soln(:,1), &
             VERB=BTEST(verb_level,2) )
     END IF
     
     !
     ! Check is either dt < dtmin or
     ! cfl < cflmin
     !
     IF( dt < dtmin ) THEN
        IF (par_rank.EQ.0) PRINT *,'Error, dt < dtmin, Exiting main loop...'
        EXIT !exit main loop
     END IF

     IF (cfl < cflmin) THEN
        IF (par_rank.EQ.0) PRINT *,'Error, cfl < cflmin, Exiting main loop...'
        EXIT !exit main loop
     END IF

     first_loop_after_restart = .FALSE. ! reset flag
     first_time_integration_iter = .FALSE. ! reset flag
     
     IF( BTEST(debug_level,0) ) THEN        ! print statistics from MAIN
        PAUSE 'in main'
     END IF
     
     
     ! The following call is needed on unpatched absoft compiler to make it release memory
#ifdef COMPILER_UNPATCHED_ABSOFT
     call release_cache()
#endif 
     
     
  END DO
  !*************************************************************
  !  END TIME INTEGRATION LOOP
  !*************************************************************

  
  
  ! total time till the end of time integration, CUMULATIVE
  CALL timer_stop(1)
  
  
#ifdef USE_DEBUG_TIMERS
  ! output some timing
  tmp1 = timer_val(1)
  CALL parallel_global_sum( REAL=tmp1 )
  IF (par_rank.EQ.0) &
       WRITE (6,'("CPU TIME (whole time integration) at #0:", es12.5,", sum:",es12.5)') timer_val(1), tmp1
#endif
  
  
  CALL user_stats(u, j_mn, -1) ! final user defined staistics output
  
  
  
  ! clean alocated in main:
  IF (ALLOCATED(scaleCoeff)) DEALLOCATE(scaleCoeff)
  IF (ASSOCIATED(scl)) DEALLOCATE(scl)
  IF (ASSOCIATED(scl_global)) DEALLOCATE(scl_global)
  IF (ALLOCATED(force)) DEALLOCATE(force)
  
  IF (ALLOCATED(penal)) DEALLOCATE(penal)                      ! IF (imask_obstacle)
  IF (ALLOCATED(dist)) DEALLOCATE(dist)                      ! IF (imask_obstacle)
  IF (ALLOCATED(c_sound)) DEALLOCATE(c_sound)                  ! IF( hypermodel /= 0 )
  IF (ASSOCIATED(u_old)) DEALLOCATE(u_old)                     ! IF ( TYPE_DB == DB_TYPE_WRK )

  
  CALL release_memory_DB
  CALL parallel_finalize
  
END PROGRAM wavelet_transform

