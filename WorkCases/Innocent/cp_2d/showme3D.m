%function out = showme3D(fileName, figType, plotType, varName, time)

function showme3D(fileName,varName,st_num,fig_type)
global RYB
global Umn
Umn = 0.0;
varName
if ~isempty(strmatch(varName,'modWij','exact'))
    RYB=logical(1);
else
    RYB=logical(0);
end
local_dir = pwd
if isunix
    pl = '/'
else
    pl = '\';
end
global POSTPROCESS_DIR
%POSTPROCESS_DIR = '/home/vasilyev/wlt_3d_DBs_Jonathan/post_process'
%POSTPROCESS_DIR = 'D:\Oleg_Vasilyev\Research\FORTRAN\wlt_3d_DBs\post_process'
POSTPROCESS_DIR = [pwd pl '..' pl '..' pl '..' pl 'post_process']

path(path,POSTPROCESS_DIR)
path(path,[POSTPROCESS_DIR pl 'sliceomatic'])
clear pl
cd(local_dir)

% plot output from poisson_3d.out
%
%
% args 
%    file       output file name
%    j_range    = [j_min j_max] (range of levels to plot )
%    eps        plot only valuse > eps (if<0  percent of wlt coeff. range)
%    bounds     = [xmin xmax ymin ymax zmin zmax] plot within bounds
%    fig_type   'coeff' = plot wlt coefficents, 'grid' = plot grid, 'solution' = plot solution
%    plot_type  'surf' = surface plots, 'contour' = contour plots, 'isosurface' = plot isosurface
%               'slice' = sliceomatic
%    obstacle   'none' = no obstacle plot, 'sphere', 'cylinder' 
%    az, el     azimuth and elevation for the direction to look at the plot
%    slev       level of isosurface choosed (in % of maximum value)
%    plot_comp ux, uy, uz, wx, wy, wz, magu, magw
%    station_num         station # (ie. the number of the output file
%    fignum     figure number to use for output
%   x0 - coordinate and the slise half thickness, e.g.  x0=[x y z del];
%   n0 - array of vectors normal to the slice planes, e.g. n0=[[0 1 1]' [0 1 -1]']; 

%
%
% function pl = c_wlt_3d(file,j_range,eps,bounds,fig_type,plot_type,obstacle,az,el,slev,plot_comp,station_num,fignum,x0,n0)
 
c_wlt_3d( ...
 fileName ... %file,
, [ 0 20] ...% j_range,
, 0.0 ...% eps,
, [-2 5 -2 2] ...% bounds,
, fig_type ...% fig_type,
, 'slice' ...% plot_type,
, 'none' ...% obstacle,
, 0 ...% az,
, 0 ...% el,
, 0.9 ...% slev,
, varName ...% plot_comp,
, st_num ...% station_num,
, 0 ...% fignum,
, [0 0 0 1000]...% x0,
, [[0 1 0]' [1 0 0]'] ) % n0 
set(gca,'View',[0 90])

