!>
!!
!<
MODULE mytools
IMPLICIT NONE
  INTEGER, PARAMETER :: sp = KIND(1.0),&
                        dp = KIND(1D0),&
                        cp = dp
  INTEGER, PARAMETER :: PB_0D_NO_TIME   = 100,& !> zero spatial dimension, time independant problem
                        PB_1D_NO_TIME   = 110,& !> one spatial dimension, time independant problem
                        PB_2D_NO_TIME   = 120,& !> two spatial dimension, time independant problem
                        PB_3D_NO_TIME   = 130,& !> three spatial dimensions, time independant problem
                        PB_0D_PLUS_TIME = 101,& !> zero spatial dimension plus time
                        PB_1D_PLUS_TIME = 111,& !> one spatial dimension plus time
                        PB_2D_PLUS_TIME = 121,& !> two spatial dimensions plus time
                        PB_3D_PLUS_TIME = 131,& !> three spatial dimensions plus time
                        COORD_TIME = 1000,& !> time coordinate indicator
                        COORD_X1   = 1001,& !> first spatial dimension indicator
                        COORD_X2   = 1002,& !> second spatial dimension indicator
                        COORD_X3   = 1003   !> third spatial dimension indicator

  !> \brief Strings length, used for short file name or other short string variables
  !<
  INTEGER, PARAMETER :: ip_snl = 80
  !> \brief Strings length, used for full file name or other short string variables
  !<
  INTEGER, PARAMETER :: ip_fnl = 255
  CHARACTER (LEN=*), PARAMETER :: SCREEN = "SCREEN",&    !> Screen file name for output
                                  OBS_FILE = 'obs.dat',& !> Observation file name
                                  OBS_GAP_FILE = 'obsgap.dat' !> Obs gap file name

  !> \brief format to print integer
  !<
  CHARACTER(LEN=*), PARAMETER :: IFORMAT = "(I5)"
  CHARACTER(LEN=*), PARAMETER :: IFMT = IFORMAT
  !> \brief format to print real
  !<
  CHARACTER(LEN=*), PARAMETER :: RFORMAT = "(E10.2)"
  !> \brief format to print integer with comment, debug
  !<
  CHARACTER(LEN=*), PARAMETER :: DEBUG_IFORMAT = "(A,"//IFORMAT//")"
  !> \brief format to print logical value with comment, debug
  !<
  CHARACTER(LEN=*), PARAMETER :: DEBUG_LFORMAT = "(A,L)"
  !> \brief format to print character value with comment, debug
  !<
  CHARACTER(LEN=*), PARAMETER :: DEBUG_AFORMAT = "(A,A)"
  !> \brief format to print real with comment, debug
  !<
  CHARACTER(LEN=*), PARAMETER :: DEBUG_RFORMAT = "(A,"//RFORMAT//")"
  !> \brief status for file input/output
  !<
  CHARACTER(LEN=*), PARAMETER :: FILE_REPLACE = 'REPLACE'
  !> \brief format for file input/output
  !<
  CHARACTER(LEN=*), PARAMETER :: FILE_FORMATTED = 'FORMATTED'

  !> \brief action value for make obs simulation
  !<
  CHARACTER(LEN=*), PARAMETER :: MAKE_OBS= "OBS"
  !> \brief action value for data assimilation
  !<
  CHARACTER(LEN=*), PARAMETER :: RUN_ASSIM   = "ASSIM"
  !> \brief Action value for direct simulation
  !<
  CHARACTER(LEN=*), PARAMETER :: RUN_DIRECT  = "DIRECT"
  !> \brief Action value for adjoint simulation
  !! The meaning of this variable depends on the context:
  !!   - in the solver, it means solving only the adjoint model,
  !!     the obs gap is supposed to be saved from previous computation
  !!   - in the minimization driver, it means computing the cost function (direct model) followed by the adjoint
  !<
  CHARACTER(LEN=*), PARAMETER :: RUN_ADJOINT = "ADJOINT"
  !> \brief Action value for the computation of the cost function (this is direct followed by cost)
  !! used only by the solver
  !<
  CHARACTER(LEN=*), PARAMETER :: RUN_COST = "COST"
  !> \brief action value for the computation of the gradient of the cost function (this is adjoint followed by g)
  !! used only by the solver
  !<
  CHARACTER(LEN=*), PARAMETER :: RUN_GRADIENT = "GRAD"

  
  !> \brief Maximum size for the action variable
  !<
  INTEGER, PARAMETER :: IP_ACTION_LEN = 10

  !> \brief User defined type for exchanging parameters between programs
  !! Defines parameters to be exchanged between the solver and the minimization driver
  !<
  TYPE exchange_param
     CHARACTER(LEN=ip_snl), PRIVATE :: fileName = "exchange_namelist"
     CHARACTER(LEN=ip_snl) :: obs_fileName = "runtime_obs.dat"
     CHARACTER(LEN=ip_snl) :: obsgap_fileName = "runtime_obsgap.dat"
     CHARACTER(LEN=ip_snl) :: savedep_fileName = "runtime_saved_ep.dat"!unused
     CHARACTER(LEN=IP_ACTION_LEN)   :: aa_action
     REAL(KIND=cp) :: r_mu
     REAL(KIND=cp) :: r_sigma
     REAL(KIND=cp) :: r_v0
     REAL(KIND=cp) :: r_omega

     !informations on control parameters
     LOGICAl :: l_amplitude !>
     LOGICAl :: l_location  !>
     LOGICAl :: l_sigma     !>

     !observation parameters
     INTEGER :: i_nobs_x !> obs count in the space direction
     INTEGER :: i_nobs_t !> obs count in the time direction
     REAL(KIND=cp) :: r_sigmaR

     !control parameters
     INTEGER :: i_nctl !> size of the control vector
     REAL(KIND=cp) :: r_cost!> value of the cost function at ctl
     REAL(KIND=cp), DIMENSION(:), ALLOCATABLE :: ra_ctl      !> control vector
     REAL(KIND=cp), DIMENSION(:), ALLOCATABLE :: ra_grad     !> grad of the cost function at the control vector
     REAL(KIND=cp), DIMENSION(:), ALLOCATABLE :: ra_b_ctl    !> background control vector
     REAL(KIND=cp), DIMENSION(:), ALLOCATABLE :: ra_sigma_ctl!> standard deviation of errors in the b_ctl

  END TYPE exchange_param


  !> \brief User defined type for exchanging parameters between programs observations
  !<
  TYPE obs_structure
     CHARACTER(LEN=ip_snl) :: obs_fileName, obsgap_fileName
     INTEGER :: i_pbDims!> dimensions of the problem 
     INTEGER :: i_nobs  !> total number of observations
     REAL(KIND=cp), DIMENSION(:), ALLOCATABLE :: ra_data     !> observation data
     REAL(KIND=cp), DIMENSION(:), ALLOCATABLE :: ra_sigma    !> standard deviation of observation error
     REAL(KIND=cp), DIMENSION(:), ALLOCATABLE :: ra_Rm1      !> inverse covariance (diag matrix)
     REAL(KIND=cp), DIMENSION(:), ALLOCATABLE :: ra_times    !> observation time coordinates
     REAL(KIND=cp), DIMENSION(:), ALLOCATABLE :: ra_x1       !> x1 (first spatial) coordinates
     REAL(KIND=cp), DIMENSION(:), ALLOCATABLE :: ra_x2       !> x1 (second spatial) coordinates
     REAL(KIND=cp), DIMENSION(:), ALLOCATABLE :: ra_x3       !> x3 (third spatial) coordinates
     REAL(KIND=cp), DIMENSION(:), ALLOCATABLE :: ra_obsgap   !> obsgap, this is gap between obs and model output
  END TYPE obs_structure

  INTERFACE NUM2STR
     MODULE PROCEDURE itoa!> Integer to ASCII
     MODULE PROCEDURE rtoa!> Real single precision to ASCII
     MODULE PROCEDURE dtoa!> Real double precision to ASCII
  END INTERFACE

CONTAINS
  
  FUNCTION itoa(id_val) RESULT(ala_val)
    INTEGER, INTENT(IN) :: id_val
    CHARACTER(LEN=ip_snl)   :: ala_val
    
    WRITE(ala_val, *) id_val
    ala_val = ADJUSTL(ala_val)
  END FUNCTION itoa
  
  FUNCTION rtoa(rd_val) RESULT(ala_val)
    REAL(sp), INTENT(IN) :: rd_val
    CHARACTER(LEN=ip_snl)   :: ala_val
    
    WRITE(ala_val, *) rd_val
    ala_val = ADJUSTL(ala_val)
  END FUNCTION rtoa
  
  FUNCTION dtoa(rd_val) RESULT(ala_val)
    REAL(dp), INTENT(IN) :: rd_val
    CHARACTER(LEN=ip_snl)   :: ala_val
    
    WRITE(ala_val, *) rd_val
    ala_val = ADJUSTL(ala_val)
  END FUNCTION dtoa

  !> \brief Check if a given dimension (time, x1, x2, x3) is prensent in the description of a problem
  !! \param[in] id_pbDims descriptuion of the dimensions of the problem
  !! \param[in] id_dim dimension to check
  !! \detail 
  !<
  FUNCTION coord_present(id_pbDims, id_dim) RESULT(ll_present)
    INTEGER, INTENT(IN) :: id_pbDims, id_dim
    LOGICAL :: ll_present
    
    ll_present = .FALSE.
    SELECT CASE(id_dim)
      CASE (COORD_TIME)
        ll_present = (id_pbDims==PB_0D_PLUS_TIME).OR.(id_pbDims==PB_1D_PLUS_TIME).OR.&
                     (id_pbDims==PB_2D_PLUS_TIME).OR.(id_pbDims==PB_3D_PLUS_TIME)
      CASE (COORD_X1)
        ll_present = (id_pbDims==PB_1D_PLUS_TIME).OR.(id_pbDims==PB_1D_NO_TIME).OR.&
                     (id_pbDims==PB_2D_PLUS_TIME).OR.(id_pbDims==PB_2D_NO_TIME).OR.&
                     (id_pbDims==PB_3D_PLUS_TIME).OR.(id_pbDims==PB_3D_NO_TIME)
      CASE (COORD_X2)
        ll_present = (id_pbDims==PB_2D_PLUS_TIME).OR.(id_pbDims==PB_2D_NO_TIME).OR.&
                     (id_pbDims==PB_3D_PLUS_TIME).OR.(id_pbDims==PB_3D_NO_TIME)
      CASE (COORD_X3)
        ll_present = (id_pbDims==PB_3D_PLUS_TIME).OR.(id_pbDims==PB_3D_NO_TIME)
      CASE DEFAULT
        WRITE(*,*), 'In coord_present:  Unknown dimension description'
        STOP
    END SELECT
    !WRITE(*,*), 'In coord_present:  id_pbDims, id_dim, ll_present = ', id_pbDims, id_dim, ll_present
  END FUNCTION coord_present
  
  !> \brief read observations coordinates from file
  !! \param[in] td_os, observation structure
  !! \param[in] id_fileId file identifier, that has been obtaine by call to OPEN
  !!  td_os contains the name of the file and the dimension (type) of the problem as input
  !!  and the read params as output
  !!  this subroutine is used by read_obs and read_obsgap
  !<
  SUBROUTINE read_obs_coord(td_os, id_fileId)
    TYPE(obs_structure), INTENT(INOUT) :: td_os
    INTEGER, INTENT(IN) :: id_fileId
    INTEGER :: il_max_nobs, il_nobs, il_ncoord
    REAL(KIND=cp), DIMENSION(:), ALLOCATABLE :: rla_obs_t, rla_obs_x1, rla_obs_x2, rla_obs_x3

    NAMELIST/NAM_info/il_max_nobs,il_nobs
    NAMELIST/NAM_t_coord/rla_obs_t
    NAMELIST/NAM_x1_coord/rla_obs_x1
    NAMELIST/NAM_x2_coord/rla_obs_x2
    NAMELIST/NAM_x3_coord/rla_obs_x3
    
    !If one is going to read many blocs in the same file, it is recommended to rewind
    !WRITE(*,*) 'In read_obs_coord: REWIND, id_fileId = ', id_fileId
    REWIND(id_fileId)
    !WRITE(*,*) 'In read_obs_coord: reading  NAM_info'
    READ(id_fileId, NAM_info)!reading the info block
    td_os%i_nobs = il_nobs
    !time coordinates
    il_ncoord = 0
    IF( coord_present(td_os%i_pbDims, COORD_TIME) ) THEN
      il_ncoord = il_ncoord + 1
      ALLOCATE( rla_obs_t(il_nobs) )
      REWIND(id_fileId)
      !WRITE(*,*) 'In read_obs_coord: reading NAM_t_coord, il_nobs = ', il_nobs
      READ(id_fileId, NAM_t_coord)!reading the time coordinate block
      !WRITE(*,*) 'In read_obs_coord'
      IF( ALLOCATED(td_os%ra_times) )  DEALLOCATE(td_os%ra_times)
      ALLOCATE( td_os%ra_times(il_nobs) )
      td_os%ra_times = rla_obs_t
      !WRITE(*,*) 'In read_obs_coord, td_os%ra_times = ', td_os%ra_times
    END IF
    !x1 coordinates
    IF( coord_present(td_os%i_pbDims, COORD_X1) ) THEN
      il_ncoord = il_ncoord + 1
      ALLOCATE( rla_obs_x1(il_nobs) )
      REWIND(id_fileId)
      !WRITE(*,*) 'In read_obs_coord: reading NAM_x1_coord'
      READ(id_fileId, NAM_x1_coord)!reading the x1 coordinate block
      IF( ALLOCATED(td_os%ra_x1) )  DEALLOCATE(td_os%ra_x1)
      ALLOCATE( td_os%ra_x1(il_nobs) )
      td_os%ra_x1 = rla_obs_x1
    END IF
    !x2 coordinates
    IF( coord_present(td_os%i_pbDims, COORD_X2) ) THEN
      il_ncoord = il_ncoord + 1
      ALLOCATE( rla_obs_x2(il_nobs) )
      REWIND(id_fileId)
      !WRITE(*,*) 'In read_obs_coord: reading NAM_x2_coord'
      READ(id_fileId, NAM_x2_coord)!reading the x2 coordinate block
      IF( ALLOCATED(td_os%ra_x2) )  DEALLOCATE(td_os%ra_x2)
      ALLOCATE( td_os%ra_x2(il_nobs) )
      td_os%ra_x2 = rla_obs_x2
    END IF
    !x3 coordinates
    IF( coord_present(td_os%i_pbDims, COORD_X3) ) THEN
      il_ncoord = il_ncoord + 1
      ALLOCATE( rla_obs_x3(il_nobs) )
      REWIND(id_fileId)
      !WRITE(*,*) 'In read_obs_coord: reading NAM_x3_coord'
      READ(id_fileId, NAM_x3_coord)!reading the x3 coordinate block
      IF( ALLOCATED(td_os%ra_x3) )  DEALLOCATE(td_os%ra_x3)
      ALLOCATE( td_os%ra_x3(il_nobs) )
      td_os%ra_x3 = rla_obs_x3
    END IF
    IF(il_ncoord ==0 ) THEN
       WRITE(*,*) 'In read_obs_coord: no coordinate, check the initialization  of pbDims'
       WRITE(*,*) '  td_os%i_pbDims = ', td_os%i_pbDims
       WRITE(*,*) '  Stopping the program ...'
       STOP
    END IF
  END SUBROUTINE read_obs_coord
  
  !> \brief read observations from file
  !! \param[in, out] td_os, observation structure, 
  !! contains the name of the file and the dimension (type) of the problem as input and the read params as output
  !<
  SUBROUTINE read_obs(td_os)
    INTEGER, PARAMETER :: ip_numnam = 69
    TYPE(obs_structure), INTENT(INOUT) :: td_os
    INTEGER :: il_max_nobs, il_nobs
    REAL(KIND=cp), DIMENSION(:), ALLOCATABLE :: rla_obs_data, rla_obs_sigma

    NAMELIST/NAM_info/il_max_nobs,il_nobs
    NAMELIST/NAM_data/rla_obs_data, rla_obs_sigma
    
    
    OPEN(ip_numnam, FILE=td_os%obs_fileName, FORM='FORMATTED', STATUS='OLD')
    !If one is going to read many blocs in the same file, it is recommended to rewind
    !WRITE(*,*) 'In read_obs: reading NAM_info'
    READ(ip_numnam, NAM_info)!reading the info block
    td_os%i_nobs = il_nobs
    ALLOCATE( rla_obs_data(il_nobs), rla_obs_sigma(il_nobs) )
    REWIND(ip_numnam)
    !WRITE(*,*) 'In read_obs: reading NAM_data'
    READ(ip_numnam, NAM_data)!reading the data block
    IF( ALLOCATED(td_os%ra_data) )  DEALLOCATE(td_os%ra_data)
    IF( ALLOCATED(td_os%ra_sigma) ) DEALLOCATE(td_os%ra_sigma)
    IF( ALLOCATED(td_os%ra_Rm1) ) DEALLOCATE(td_os%ra_Rm1)
    IF( ALLOCATED(td_os%ra_obsgap) ) DEALLOCATE(td_os%ra_obsgap)!Initializes obsgap variable
    ALLOCATE( td_os%ra_data(il_nobs), td_os%ra_sigma(il_nobs),&
              td_os%ra_Rm1(il_nobs) , td_os%ra_obsgap(il_nobs)&
    )
    td_os%ra_data  = rla_obs_data
    td_os%ra_sigma = rla_obs_sigma
    td_os%ra_Rm1 = 1/rla_obs_sigma**2
    !reading coordinates
    CALL read_obs_coord(td_os, ip_numnam)
    !WRITE(*,*) 'In read_obs : td_os%ra_times = ', td_os%ra_times
    CLOSE(ip_numnam)
    !WRITE(*,*) 'In read_obs: end of reading'
  END SUBROUTINE read_obs

  
  !> \brief read obsgap from file
  !! \param[in, out] td_os, observation structure, 
  !! contains the name of the file and the dimension (type) of the problem as input
  !! and the read params as output
  !<
  SUBROUTINE read_obsgap(td_os)
    INTEGER, PARAMETER :: ip_numnam = 69
    TYPE(obs_structure), INTENT(INOUT) :: td_os
    INTEGER :: il_nobs
    REAL(KIND=cp), DIMENSION(:), ALLOCATABLE :: rla_obsgap, rla_Rm1
    NAMELIST/NAM_info/il_nobs
    NAMELIST/NAM_data/rla_obsgap, rla_Rm1   
    
    OPEN(ip_numnam, FILE=td_os%obsgap_fileName, FORM='FORMATTED', STATUS='OLD')
    !If one is going to read many blocs in the same file, it is recommended to rewind
    !WRITE(*,*) 'In read_obs: reading NAM_info'
    READ(ip_numnam, NAM_info)!reading the info block
    td_os%i_nobs = il_nobs
    !WRITE(*,*) 'In read_obsgap: td_os%i_nobs = ', td_os%i_nobs
    IF( ALLOCATED(td_os%ra_obsgap) )  DEALLOCATE(td_os%ra_obsgap)
    IF( ALLOCATED(td_os%ra_Rm1)    )  DEALLOCATE(td_os%ra_Rm1   )
    ALLOCATE( rla_obsgap(il_nobs), td_os%ra_obsgap(il_nobs),&
              rla_Rm1(il_nobs)   , td_os%ra_Rm1(il_nobs)    &
    )
    REWIND(ip_numnam)
    READ(ip_numnam, NAM_data)!reading the data block
    td_os%ra_obsgap = rla_obsgap
    td_os%ra_Rm1    = rla_Rm1
    !reading coordinates
    CALL read_obs_coord(td_os, ip_numnam)
    !closing the file
    CLOSE(ip_numnam)
    !WRITE(*,*) 'In read_obsgap: td_os%ra_obsgap = ', td_os%ra_obsgap
    !WRITE(*,*) 'In read_obsgap: td_os%ra_Rm1    = ', td_os%ra_Rm1
  END SUBROUTINE read_obsgap
  
  !> \brief write observations coordinates to file
  !! \param[in] td_os, observation structure
  !! \param[in] id_fileId file identifier, that has been obtaine by call to OPEN
  !! the file must already be opened and the identifier passed to this routine
  !!  this subroutine is used by write_obs and write_obsgap
  !<
  SUBROUTINE write_obs_coord(td_os, id_fileId)
    TYPE(obs_structure), INTENT(IN) :: td_os
    INTEGER, INTENT(IN) :: id_fileId
    INTEGER :: il_max_nobs, il_nobs
    REAL(KIND=cp), DIMENSION(:), ALLOCATABLE :: rla_obs_t, rla_obs_x1, rla_obs_x2, rla_obs_x3

    NAMELIST/NAM_info/il_max_nobs,il_nobs
    NAMELIST/NAM_t_coord/rla_obs_t
    NAMELIST/NAM_x1_coord/rla_obs_x1
    NAMELIST/NAM_x2_coord/rla_obs_x2
    NAMELIST/NAM_x3_coord/rla_obs_x3
    
    il_nobs     = td_os%i_nobs

    IF( coord_present(td_os%i_pbDims, COORD_TIME) ) THEN
      ALLOCATE( rla_obs_t(il_nobs) )
      rla_obs_t = td_os%ra_times
      WRITE(id_fileId, NAM_t_coord)!writing the time coordinate block
    END IF
    !x1 coordinates
    IF( coord_present(td_os%i_pbDims, COORD_X1) ) THEN
      ALLOCATE( rla_obs_x1(il_nobs) )
      rla_obs_x1 = td_os%ra_x1
      WRITE(id_fileId, NAM_x1_coord)!writing the x1 coordinate block
    END IF
    !x2 coordinates
    IF( coord_present(td_os%i_pbDims, COORD_X2) ) THEN
      ALLOCATE( rla_obs_x2(il_nobs) )
      rla_obs_x2 = td_os%ra_x2
      WRITE(id_fileId, NAM_x2_coord)!writing the x2 coordinate block
    END IF
    !x3 coordinates
    IF( coord_present(td_os%i_pbDims, COORD_X3) ) THEN
      ALLOCATE( rla_obs_x3(il_nobs) )
      rla_obs_x3 = td_os%ra_x3
      WRITE(id_fileId, NAM_x3_coord)!writing the x2 coordinate block
    END IF
  END SUBROUTINE write_obs_coord
  
  !> \brief write observations to file
  !! \param[in] td_os, observation structure
  !<
  SUBROUTINE write_obs(td_os)
    INTEGER, PARAMETER :: ip_numnam = 69
    TYPE(obs_structure), INTENT(IN) :: td_os
    INTEGER :: il_max_nobs, il_nobs
    REAL(KIND=cp), DIMENSION(:), ALLOCATABLE :: rla_obs_data, rla_obs_sigma

    NAMELIST/NAM_info/il_max_nobs,il_nobs
    NAMELIST/NAM_data/rla_obs_data,rla_obs_sigma
    
    OPEN(ip_numnam, FILE=td_os%obs_fileName, FORM='FORMATTED', STATUS='REPLACE')
    il_nobs     = td_os%i_nobs
    il_max_nobs = il_nobs
    WRITE(ip_numnam, NAM_info)!writing the info block
    ALLOCATE( rla_obs_data(td_os%i_nobs), rla_obs_sigma(td_os%i_nobs) )
    rla_obs_data  = td_os%ra_data
    rla_obs_sigma = td_os%ra_sigma
    WRITE(ip_numnam, NAM_data)!writing the data block
    !writing coordinates
    CALL write_obs_coord(td_os, ip_numnam)
    CLOSE(ip_numnam)

  END SUBROUTINE write_obs
  
  !> \brief write observations to file
  !! \param[in] td_os, observation structure
  !<
  SUBROUTINE write_obsgap(td_os)
    INTEGER, PARAMETER :: ip_numnam = 69
    TYPE(obs_structure), INTENT(IN) :: td_os
    INTEGER :: il_nobs
    REAL(KIND=cp), DIMENSION(:), ALLOCATABLE :: rla_obsgap, rla_Rm1

    NAMELIST/NAM_info/il_nobs
    NAMELIST/NAM_data/rla_obsgap,rla_Rm1
    
    OPEN(ip_numnam, FILE=td_os%obsgap_fileName, FORM='FORMATTED', STATUS='REPLACE')
    il_nobs     = td_os%i_nobs
    WRITE(ip_numnam, NAM_info)!writing the info block
    ALLOCATE( rla_obsgap(td_os%i_nobs), rla_Rm1(td_os%i_nobs) )
    rla_obsgap = td_os%ra_obsgap
    rla_Rm1    = td_os%ra_Rm1
    !WRITE(*,*) 'In write_obsgap, writing NAM_data'
    WRITE(ip_numnam, NAM_data)!writing the data block
    !writing coordinates
    !WRITE(*,*) 'In write_obsgap, calling write_obs_coord'
    CALL write_obs_coord(td_os, ip_numnam)
    CLOSE(ip_numnam)

  END SUBROUTINE write_obsgap
  
  SUBROUTINE free_ep(td_ep)
    TYPE(exchange_param), INTENT(INOUT) :: td_ep
    WRITE(*, *) 'In free_ep : deallocating exchange parameter structure'
    IF( ALLOCATED(td_ep%ra_ctl) )       DEALLOCATE(td_ep%ra_ctl)
    IF( ALLOCATED(td_ep%ra_grad) )      DEALLOCATE(td_ep%ra_grad)
    IF( ALLOCATED(td_ep%ra_b_ctl) )     DEALLOCATE(td_ep%ra_b_ctl)
    IF( ALLOCATED(td_ep%ra_sigma_ctl) ) DEALLOCATE(td_ep%ra_sigma_ctl)
  END SUBROUTINE free_ep

  !> \brief read exchange parameters between the solver and the minimization driver
  !! \param[in, out] td_ep, exchange parameter, contains the name of the file as input
  !!     and the read params as output
  !<
  SUBROUTINE read_ep(td_ep)
    INTEGER, PARAMETER:: ip_numnam = 68
    TYPE(exchange_param), INTENT(INOUT) :: td_ep
    REAL(KIND=cp):: rl_mu,rl_sigma, rl_v0, rl_omega, rl_sigmaR, rl_cost
    LOGICAL :: ll_amplitude, ll_location, ll_sigma
    INTEGER :: il_nobs_x, il_nobs_t, il_nctl
    CHARACTER(LEN=IP_ACTION_LEN)   :: ala_action
    REAL(KIND=cp), DIMENSION(:), ALLOCATABLE :: rla_ctl, rla_grad, rla_b_ctl, rla_sigma_ctl
    
    NAMELIST/NAM_size/il_nctl

    NAMELIST/NAM_ep/&
      ala_action  ,&
      rl_mu       ,&
      rl_sigma    ,&
      rl_v0       ,&
      rl_omega    ,&
      ll_amplitude,&
      ll_location ,&
      ll_sigma    ,&
      il_nobs_x   ,&
      il_nobs_t   ,& 
      rl_sigmaR   ,&
      rl_cost     ,&
      rla_ctl     ,&
      rla_grad    ,&
      rla_b_ctl   ,&
      rla_sigma_ctl
    
    OPEN(ip_numnam, FILE=td_ep%fileName, FORM='FORMATTED', STATUS='OLD')
    !If one is going to read many blocs in the same file, it is recommended to rewind
    READ(ip_numnam, NAM_size)!reading the block
    ALLOCATE( rla_ctl(il_nctl), rla_grad(il_nctl), rla_b_ctl(il_nctl), rla_sigma_ctl(il_nctl) )
    REWIND(ip_numnam)
    READ(ip_numnam, NAM_ep)!reading the block
    CLOSE(ip_numnam)

    IF( ALLOCATED(td_ep%ra_ctl) )       DEALLOCATE(td_ep%ra_ctl)
    IF( ALLOCATED(td_ep%ra_grad) )      DEALLOCATE(td_ep%ra_grad)
    IF( ALLOCATED(td_ep%ra_b_ctl) )     DEALLOCATE(td_ep%ra_b_ctl)
    IF( ALLOCATED(td_ep%ra_sigma_ctl) ) DEALLOCATE(td_ep%ra_sigma_ctl)
    ALLOCATE( td_ep%ra_ctl(il_nctl), td_ep%ra_grad(il_nctl),&
              td_ep%ra_b_ctl(il_nctl), td_ep%ra_sigma_ctl(il_nctl)&
    )
    
    td_ep%aa_action    = ala_action
    td_ep%r_mu         = rl_mu
    td_ep%r_sigma      = rl_sigma
    td_ep%r_v0         = rl_v0
    td_ep%r_omega      = rl_omega
    td_ep%l_amplitude  = ll_amplitude
    td_ep%l_location   = ll_location
    td_ep%l_sigma      = ll_sigma
    td_ep%i_nobs_x     = il_nobs_x
    td_ep%i_nobs_t     = il_nobs_t
    td_ep%r_sigmaR     = rl_sigmaR
    td_ep%i_nctl       = il_nctl
    td_ep%r_cost       = rl_cost
    td_ep%ra_ctl       = rla_ctl
    td_ep%ra_grad      = rla_grad
    td_ep%ra_b_ctl     = rla_b_ctl
    td_ep%ra_sigma_ctl = rla_sigma_ctl
  END SUBROUTINE read_ep

  
  !> \brief write exchange parameters between the solver and the minimization driver
  !! \param[in, out] td_ep, exchange parameter, contains the name of the file as input
  !!  and the read params as output
  !<
  SUBROUTINE write_ep(td_ep)
    INTEGER, PARAMETER:: ip_numnam = 68
    TYPE(exchange_param), INTENT(INOUT) :: td_ep
    REAL(KIND=cp):: rl_mu,rl_sigma, rl_v0, rl_omega, rl_sigmaR, rl_cost
    LOGICAL :: ll_amplitude, ll_location, ll_sigma
    INTEGER :: il_nobs_x, il_nobs_t, il_nctl, il_lb

    CHARACTER(LEN=IP_ACTION_LEN)   :: ala_action
    REAL(KIND=cp), DIMENSION(:), ALLOCATABLE :: rla_ctl, rla_grad, rla_b_ctl, rla_sigma_ctl
    
    NAMELIST/NAM_size/il_nctl
    
    NAMELIST/NAM_ep/&
      ala_action  ,&
      rl_mu       ,&
      rl_sigma    ,&
      rl_v0       ,&
      rl_omega    ,& 
      ll_amplitude,&
      ll_location ,&
      ll_sigma    ,&
      il_nobs_x   ,&
      il_nobs_t   ,&
      rl_sigmaR   ,&
      rl_cost     ,&
      rla_ctl     ,&
      rla_grad    ,&
      rla_b_ctl   ,&
      rla_sigma_ctl
    
    il_nctl = td_ep%i_nctl
    ALLOCATE( rla_ctl(il_nctl), rla_grad(il_nctl), rla_b_ctl(il_nctl), rla_sigma_ctl(il_nctl) )
    ala_action    = td_ep%aa_action
    rl_mu         = td_ep%r_mu
    rl_sigma      = td_ep%r_sigma
    rl_v0         = td_ep%r_v0
    rl_omega      = td_ep%r_omega
    ll_amplitude  = td_ep%l_amplitude
    ll_location   = td_ep%l_location
    ll_sigma      = td_ep%l_sigma
    il_nobs_x     = td_ep%i_nobs_x
    il_nobs_t     = td_ep%i_nobs_t
    rl_sigmaR     = td_ep%r_sigmaR
    rl_cost       = td_ep%r_cost
    rla_ctl       = td_ep%ra_ctl
    rla_grad      = td_ep%ra_grad
    rla_b_ctl     = td_ep%ra_b_ctl
    rla_sigma_ctl = td_ep%ra_sigma_ctl

    OPEN(ip_numnam, FILE=td_ep%fileName, FORM='FORMATTED', STATUS='REPLACE')
    !If one is going to read many blocs in the same file, it is recommended to rewind
    !REWIND(ip_numnam)
    il_nctl = td_ep%i_nctl
    WRITE(ip_numnam, NAM_size)!reading the block

    WRITE(ip_numnam, NAM_ep)!reading the block
    CLOSE(ip_numnam)

  END SUBROUTINE write_ep
  
  !>\brief print exchange parameters for diagnostics
  SUBROUTINE print_ep(td_ep)
    TYPE(exchange_param), INTENT(IN) :: td_ep
    CHARACTER(LEN=80)   :: array_rformat
    array_rformat = "(A,"//TRIM( NUM2STR(td_ep%i_nctl) )//RFORMAT//")"
    
    WRITE(*,                 *) 'Mytools::print_ep : exchange_param-----------------------'
    WRITE(*, FMT=DEBUG_AFORMAT) '  Action to be taken (action)    = ',  td_ep%aa_action
    WRITE(*, FMT=DEBUG_RFORMAT) '  Diffusion coefficient (mu)     = ',  td_ep%r_mu
    WRITE(*, FMT=DEBUG_RFORMAT) '  Bell shape sigma      (sigma)  = ',  td_ep%r_sigma
    WRITE(*, FMT=DEBUG_RFORMAT) '  Vvelocity parameter    (v0)    = ',  td_ep%r_v0
    WRITE(*, FMT=DEBUG_RFORMAT) '  Velocity oscillation  (omega)  = ',  td_ep%r_omega
    WRITE(*, FMT=DEBUG_LFORMAT) '  Bell shape amplitude (amplitu) = ',  td_ep%l_amplitude
    WRITE(*, FMT=DEBUG_LFORMAT) '  Bell shape location (location) = ',  td_ep%l_location
    WRITE(*, FMT=DEBUG_LFORMAT) '  B. shape sigma control?        = ',  td_ep%l_sigma
    WRITE(*, FMT=DEBUG_IFORMAT) '  Obs count along x direction    = ',  td_ep%i_nobs_x
    WRITE(*, FMT=DEBUG_IFORMAT) '  Obs count along time direction = ',  td_ep%i_nobs_t
    WRITE(*, FMT=DEBUG_RFORMAT) '  Obs standard deviation(sigmaR) = ',  td_ep%r_sigmaR
    WRITE(*, FMT=DEBUG_IFORMAT) '  Size of the control vector     = ',  td_ep%i_nctl
    WRITE(*, FMT=DEBUG_RFORMAT) '  Value of the cost function     = ',  td_ep%r_cost
    WRITE(*, FMT=array_rformat) '  Control vector           (ctl) = ',  td_ep%ra_ctl
    WRITE(*, FMT=array_rformat) '  Gradient of the cost fnc(grad) = ',  td_ep%ra_grad
    WRITE(*, FMT=array_rformat) '  Background of C.vector (b_ctl) = ',  td_ep%ra_b_ctl
    WRITE(*, FMT=array_rformat) '  ctl STD            (sigma_ctl) = ',  td_ep%ra_sigma_ctl
  END SUBROUTINE print_ep
  
  !>\brief print observation for diagnostics
  SUBROUTINE print_os(td_os)
    TYPE(obs_structure), INTENT(IN) :: td_os
    CHARACTER(LEN=80)   :: array_rformat
    array_rformat = "(A,"//TRIM( NUM2STR(td_os%i_nobs) )//RFORMAT//")"
    
    WRITE(*,                 *) 'Mytools::print_obs : obs_structure-----------------------'
    WRITE(*, FMT=DEBUG_AFORMAT) '  Observation file name           = ',  td_os%obs_fileName
    WRITE(*, FMT=DEBUG_IFORMAT) '  total number of observations    = ',  td_os%i_nobs
    WRITE(*, FMT=array_rformat) '  observation data                = ',  td_os%ra_data
    WRITE(*, FMT=array_rformat) '  standard deviation in obs error = ',  td_os%ra_sigma
    IF( coord_present(td_os%i_pbDims, COORD_TIME) )&
         WRITE(*, FMT=array_rformat) '  observation times               = ',  td_os%ra_times
    IF( coord_present(td_os%i_pbDims, COORD_X1) )&
         WRITE(*, FMT=array_rformat) '  observation x1 coordinates      = ',  td_os%ra_x1
    IF( coord_present(td_os%i_pbDims, COORD_X2) )&
         WRITE(*, FMT=array_rformat) '  observation x2 coordinates      = ',  td_os%ra_x2
    IF( coord_present(td_os%i_pbDims, COORD_X3) )&
         WRITE(*, FMT=array_rformat) '  observation x3 coordinates      = ',  td_os%ra_x3
  END SUBROUTINE print_os
  
  
  !>\brief Write 2D array to file
  !<
  SUBROUTINE myio_write_matrix(fileName, rda_A)
    REAL(KIND=dp), DIMENSION(:, :), INTENT(IN) :: rda_A
    CHARACTER(LEN=*)         , INTENT(IN) :: fileName
    INTEGER ib_i, ib_j, il_ios
    INTEGER :: ip_fid = 41

    IF(fileName==SCREEN) THEN
      ip_fid = 6!standard output
      PRINT*, 'rows count = ', size(rda_A, 1)
      PRINT*, 'columns count = ', size(rda_A, 2)
    ELSE
      ip_fid = 41
      OPEN( UNIT = ip_fid, FILE = fileName, STATUS = 'REPLACE', FORM = 'FORMATTED', IOSTAT = il_ios)
      IF (il_ios /= 0) then
        WRITE(* , *) 'Error creating file', fileName
        STOP;
      END IF
    END IF
    PRINT*, "In writeMatrix", LBOUND(rda_A,1), UBOUND(rda_A,1), LBOUND(rda_A,2), UBOUND(rda_A,2)
    !stop
    WRITE(unit=ip_fid, fmt=IFMT) size(rda_A, 1)
    WRITE(unit=ip_fid, fmt=IFMT) size(rda_A, 2)
    DO ib_j = LBOUND(rda_A,2), UBOUND(rda_A,2)
    	DO ib_i = LBOUND(rda_A,1), UBOUND(rda_A,1)
        WRITE(unit=ip_fid, fmt='(E18.8)', advance="no") rda_A(ib_i,ib_j)!"no" to avoid end of line
      END DO
      !WRITE(unit=ip_fid, fmt=*)''! just for adding end of line
    END DO
    IF(fileName/=SCREEN) THEN
      CLOSE(ip_fid )
    END IF
  END SUBROUTINE myio_write_matrix

  SUBROUTINE myio_read_matrix(fileName, rda_A)
    REAL(KIND=dp), DIMENSION(:, :), INTENT(OUT) :: rda_A
    CHARACTER(LEN=*)         , INTENT(IN)  :: fileName
    INTEGER ib_i, ib_j, il_nbRow, il_nbCol, il_ios
    INTEGER , PARAMETER :: ip_fid =615
      
    OPEN( UNIT = ip_fid, FILE = fileName, IOSTAT = il_ios)
    IF (il_ios /= 0) then
      WRITE(* , *) 'Error opening file', fileName
      STOP;
    END IF
    !stop
    READ(UNIT=ip_fid, FMT=IFMT) il_nbRow
    READ(UNIT=ip_fid, FMT=IFMT) il_nbCol
    PRINT*, "In readMatrix, il_nbRow = ", il_nbRow, "; il_nbCol = ", il_nbCol
    DO ib_j = 1, il_nbCol
       DO ib_i = 1, il_nbRow 
          READ(UNIT=ip_fid, FMT='(E18.8)', advance="no") rda_A(ib_i,ib_j)!"no" to avoid going to the next line
          PRINT*, 'row, col', ib_i, ib_j, ' : ', rda_A(ib_i,ib_j)
       END DO
       !READ(unit=ip_fid, fmt=*) !just to skip the end of line
    END DO
    CLOSE(ip_fid )
  END SUBROUTINE myio_read_matrix

  !!read the head of the file: nbRow and nbCol
  SUBROUTINE  myio_readInfo(fileName, id_nbRow, id_nbCol)
    INTEGER , PARAMETER :: ip_fid =616
    CHARACTER(LEN=*)         , INTENT(IN)  :: fileName
    INTEGER, INTENT(OUT) :: id_nbRow, id_nbCol
    INTEGER :: il_ios
    
    OPEN( UNIT = ip_fid, FILE = fileName, IOSTAT = il_ios)
    IF (il_ios /= 0) then
      WRITE(* , *) 'In readInfo : Error opening file', fileName
      STOP;
    END IF
    READ(UNIT=ip_fid, FMT=IFMT) id_nbRow
    READ(UNIT=ip_fid, FMT=IFMT) id_nbCol
    PRINT*, "In readInfo, id_nbRow = ", id_nbRow, "; id_nbCol = ", id_nbCol
    CLOSE(ip_fid )
  END SUBROUTINE myio_readInfo

END MODULE mytools
