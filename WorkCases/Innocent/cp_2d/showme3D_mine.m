%function out = showme3D_mine(fileName, st_num, nloop, loop_delay)
%plot direct model solution, adjoint model solution and the grid
%fileName name of the file containing data
%st_num ordinal number of the record to be plotted, first record is 0
%if nloop is present, data are plotted nloop times with a delay of
%loop_delay seconds (default is 15) between successive plots. This is usefull to visualize the
%evolution of the solution while the solver is still running
%if loop_delay is present, it is used as delay in seconds between
%consecutive plots, default value is 15
function showme3D_mine(fileName, st_num, nloop, loop_delay)
    if(nargin<2)
        display('bad number of arguments, run <help showme3D_mine> for help');
        return
    elseif(nargin < 3)% nloop not present
        nloop = 1;
        loop_delay = 0;
    elseif(nargin < 4)% loop_delay not present
        if (nloop > 1)
            loop_delay = 15;
        else
            loop_delay = 0;
        end
    end
    for i = 1:nloop
        figure(1)
        showme3D(fileName,'u', st_num, 'solution')
        title 'direct model'
        figure(2)
        showme3D(fileName,'p', st_num, 'solution')
        title 'adjoint model'
        figure(3)
        showme3D(fileName,'p', st_num,'grid')
        if(i<nloop)
            pause(loop_delay)
        end
        display(['plot ' num2str(i) ' / ' num2str(nloop)]);
    end
end