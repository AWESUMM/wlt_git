MODULE conv_obs
  !USE balaise_constant
  USE debug_tools
  USE ncTools
IMPLICIT NONE
  !> \brief User defined type for observations at runtime
  !! PRIVATE (removed for compatibility with old compilers): i_max_nobs, i_ndim, i_nobs, l_icoord_allocated, l_rcoord_allocated
  !<
  TYPE obs_structure
		CHARACTER(LEN=ip_snl) :: obs_fName, ogap_fName
		!> maximum number of observations, usefull when the totalnumber of observations
		!! to be used is less than the total number of observation in the file
		!<
		INTEGER :: i_max_nobs = 0
		INTEGER :: i_ndim = 0 !> number of dimensions for the problem
		INTEGER :: i_nobs   = 0 !> \brief total number of observations
		INTEGER  :: i_ts
		REAL(dp) :: r_date
		!> \brief real coordinates
		!! this logical field says if real-type coordinates are presents.  Real coordinates are the coordinates in the physical domain
		!<
		LOGICAL :: l_rcoord = .FALSE.
		!> \brief integer coordinates
		!! this logical field says if integer-type coordinates are presents. Integer coordinates are the indices in a vector. They are usefull when observations correspond to a subsampling of the state variable
		!<
		LOGICAL :: l_icoord = .FALSE.
		!> \brief Allocation status of the integer coordinates array.
		!! this logical field says if there is a proper array allocated for the given obs_structure variable. For a time dependent problem, the location of observation can be the same for every observation time. In such case, only one array of coordinates can be allocated for all observation time in order to save memory space. The variable l_icoord_allocated is use to manage memory deallocation in this case.
		!<
		LOGICAL :: l_icoord_allocated = .FALSE.
		!> \brief Allocation status of the real coordinates array. See l_icoord_allocated for details
		LOGICAL :: l_rcoord_allocated = .FALSE.
		REAL(dp), DIMENSION(:), POINTER :: ra_obs    => NULL()!> observation data
		REAL(dp), DIMENSION(:), POINTER :: ra_sigma  => NULL()!> standard deviation of observation error
		REAL(dp), DIMENSION(:), POINTER :: ra_Rm1    => NULL()!> inverse covariance (diag matrix)
		REAL(dp), DIMENSION(:), POINTER :: ra_obsgap => NULL()!> obsgap, this is gap between obs and model output

		!wavelet related variables
		INTEGER :: i_obs_level = 0!> observation level, this variable changes the interpretation of integer coordinates
		INTEGER, DIMENSION(3) :: ia_M_vector = (/0, 0, 0/)!>wavelet grid at the coarsest scale (level =1)
		!> \brief Observation coordinates, indices in the discretization grid
		!! This array gives the indices of the observations in the full grid at observation level.
		!! It is to be differentiated from the indices in the adapted grid that must be computed at each time
		!<
		INTEGER, DIMENSION(:,:), POINTER :: ia_icoord => NULL()
		!> \brief Observation coordinates, real coordinates in the computation domain
		!<
		REAL(dp), DIMENSION(:,:), POINTER :: ra_rcoord => NULL()
  END TYPE obs_structure
	
  !> \brief Debug routines, used to print values of variables with a predefined comment
  !! The goal is to make it possible to activate or deactivate the printing with a global flag
  !<
  INTERFACE set_obs
     MODULE PROCEDURE set_obs_with_icoord
     MODULE PROCEDURE set_obs_data
  END INTERFACE
CONTAINS

  !> \brief get the size of the control vector
  !! \param[in] td_obs, obstructure structure
  !<
  FUNCTION get_obsSize(td_obs) RESULT(il_nobs)
    TYPE(obs_structure), INTENT(IN) :: td_obs
    INTEGER :: il_nobs

    il_nobs = td_obs%i_nobs
  END FUNCTION get_obsSize

  !> \brief set observation data with integer coordinates
  !! \param[in,out] td_obs, obstructure structure
  !! \param[in] ada_fName observation file name
  !<
  SUBROUTINE set_obs_fName(td_obs, ada_fName)
    TYPE(obs_structure), INTENT(INOUT) :: td_obs
    CHARACTER(len=*), INTENT(IN) :: ada_fName
    !local variables

    td_obs%obs_fName = ada_fName
  END SUBROUTINE set_obs_fName

  !> \brief set observation data assuming that the coordinates are already set
  !! \param[in,out] td_obs, obstructure structure
  !! \param[in] rda_data observation data
  !<
  SUBROUTINE set_obs_data(td_obs, rda_data)
    TYPE(obs_structure), INTENT(INOUT) :: td_obs
    REAL(dp), DIMENSION(:), INTENT(IN) :: rda_data

    td_obs%ra_obs = rda_data
  END SUBROUTINE set_obs_data

  !> \brief set observation data with integer coordinates
  !! \param[in,out] td_obs, obstructure structure
  !! \param[in] rda_data observation data
  !! \param[in] ida_coord coordinates
  !<
  SUBROUTINE set_obs_with_icoord(td_obs, rda_data, ida_coord)
    TYPE(obs_structure), INTENT(INOUT) :: td_obs
    REAL(dp), DIMENSION(:), INTENT(IN) :: rda_data
    INTEGER , DIMENSION(:, :), INTENT(IN) :: ida_coord
    !local variables
    td_obs%ra_obs = rda_data
    td_obs%ia_icoord = ida_coord
  END SUBROUTINE set_obs_with_icoord

  SUBROUTINE set_default_obs(td_os)
    TYPE(obs_structure), INTENT(INOUT) :: td_os

		td_os%i_ts   = -1
		td_os%r_date = -1.0_dp
    IF( (td_os%i_nobs>0).AND.(td_os%i_ndim>0) )THEN
      td_os%ra_obs    = 0.0_dp
      td_os%ra_sigma  = 1.0_dp
      td_os%ra_Rm1    = 1.0_dp
      td_os%ra_obsgap = 0.0_dp

      IF(td_os%l_icoord) td_os%ia_icoord = -1
      IF(td_os%l_rcoord ) td_os%ra_rcoord = -1.0_dp
    END IF
  END SUBROUTINE set_default_obs

  !> \brief Allocates space for observation
  !! \param[in] td_os, observation structure
  !! \param[in] id_nobs total number of observations
  !! \param[in] id_coord_ndim number of dimension (for coordinates)
  !! \param[in] ld_icoord says if integer coordinates are presents
  !! \param[in] ld_rcoord says if real coordinates are presents
  !<
  SUBROUTINE set_obsSize(td_os, id_nobs, id_coord_ndim, ld_icoord, ld_rcoord)
    TYPE(obs_structure), INTENT(INOUT) :: td_os
    INTEGER, INTENT(IN) :: id_nobs, id_coord_ndim
    LOGICAL, INTENT(IN) :: ld_icoord, ld_rcoord

    !if the current size or dimensionality are different from the required ones, reallocate
    IF( (td_os%i_nobs /= id_nobs).OR.(td_os%i_ndim /= id_coord_ndim).OR.&
        (td_os%l_icoord .NEQV. ld_icoord).OR.(td_os%l_rcoord .NEQV. ld_rcoord) )THEN
      !if the current size and dimensionality are nonzero, then deallocate
      IF( (td_os%i_nobs>0).AND.(td_os%i_ndim>0) )THEN
        DEALLOCATE(td_os%ra_obs, td_os%ra_sigma, td_os%ra_Rm1, td_os%ra_obsgap)

        IF(td_os%l_rcoord) DEALLOCATE(td_os%ra_rcoord)
        IF(td_os%l_icoord ) DEALLOCATE(td_os%ia_icoord)
      END IF
      !if the required size and dimensionality are nonzero, then allocate
      IF( (id_nobs>0).AND.(id_coord_ndim>0) )THEN
        ALLOCATE( td_os%ra_obs(id_nobs)  , td_os%ra_obsgap(id_nobs),&
                  td_os%ra_sigma(id_nobs), td_os%ra_Rm1(id_nobs)    &
        )

        IF(ld_icoord) ALLOCATE( td_os%ia_icoord (id_coord_ndim, id_nobs) )
        IF(ld_rcoord) ALLOCATE( td_os%ra_rcoord(id_coord_ndim, id_nobs) )
        td_os%i_nobs   = id_nobs
        td_os%i_ndim   = id_coord_ndim
        td_os%l_rcoord = ld_rcoord
        td_os%l_icoord = ld_icoord
        CALL set_default_obs(td_os)
      END IF
    END IF
  END SUBROUTINE set_obsSize

  SUBROUTINE print_os_info(td_os)
    TYPE(obs_structure), INTENT(IN) :: td_os

    CALL print_var(td_os%i_nobs      , '  total number of observations    = ')
    CALL print_var(td_os%i_obs_level , '  Observation level for wavelet   = ')
    IF( ASSOCIATED(td_os%ra_rcoord) )&
         CALL print_var(td_os%ra_rcoord, '  real coordinates               = ')
    IF( ASSOCIATED(td_os%ia_icoord) )&
         CALL print_var(td_os%ia_icoord, '  int coordinates            = ')
  END SUBROUTINE print_os_info

  !>\brief print observation for diagnostics
  SUBROUTINE print_os(td_os)
    TYPE(obs_structure), INTENT(IN) :: td_os

    WRITE(*,                 *) 'com_tools::print_obs : obs_structure-----------------------'
    CALL print_var(td_os%obs_fName, '  Observation file name.......... = ')
    CALL print_os_info(td_os)
    CALL print_var(td_os%ra_obs      , '  observation data............... = ')
    CALL print_var(td_os%ra_sigma    , '  standard deviation in obs error = ')
  END SUBROUTINE print_os

  !>\brief print obsgap for diagnostics
  SUBROUTINE print_os_gap(td_os)
    TYPE(obs_structure), INTENT(IN) :: td_os

    WRITE(*,                 *) 'com_tools::print_obs : obs_structure-----------------------'
    CALL print_var(td_os%ogap_fName, '  Obs gap file name...... = ')
    CALL print_os_info(td_os)
    CALL print_var(td_os%ra_obsgap   , '  observation data.......... = ')
    CALL print_var(td_os%ra_Rm1      , '  diagonal of the cov matrix = ')
  END SUBROUTINE print_os_gap
END MODULE conv_obs