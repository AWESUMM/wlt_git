# GNU Makefile for wavelet collocation solver
# (Must use gnu make)
#
# Calling arguments:
# DB       - required - which database routines to use
# CASE     - required - which case to link to
# MP       - optional - define to use multiple processors
# CASEMAKE - optional - define to include makefile.case_specific from CASE directiry
#
# Example call:
#   gmake DB=db_wrk CASE=TestCases/EllipticTest1/case_elliptic_poisson wlt_3d
#   gmake DB=db_lines CASE=WorkCases/NASA/dns_module wlt_3d
#   gmake DB=db_tree CASE=TestCases/SmallVortexArrayTest1/case_small_vortex_array MP=yes wlt_3d
# With Debuging turned on:
#   gmake DB=db_lines CASE=case_isoturb_Lagrangian_Mdl.DeltaMij DEBUG=YES wlt_3d
#
# Running Test Cases:
# gmake tests
#

TOP = .  

# defaults
FIX_LFCR = NO
COMPLETED = NO

no_target:
	@echo -e '\tPlease examine the makefile first'

#
# include machine specific settings based on WLT_COMPILE_HOSTNAME environment variable
ifndef WLT_COMPILE_HOSTNAME
$(error Please define WLT_COMPILE_HOSTNAME)
endif
include makefiles.machine_specific/makefile.$(WLT_COMPILE_HOSTNAME)

#
# define case specific makefile parameters, define CASEMAKE flag to use them
ifdef CASEMAKE
include $(dir ${CASE})makefile.case_specific
endif


# Define rule to make .f90 and .f files
ifeq ($(FIX_LFCR),YES)
%.o : %.f90
	dos2unix $<
	$(FC) -c $(FFLAGS) $(FPPFLAGS) $< -o $@
.f.o  : 
	dos2unix $<
	$(FC77) $(F77FLAGS) -c $< -o $@
else
%.o : %.f90 # theParameters
	$(FC) -c $(FFLAGS) $(FPPFLAGS) $< -o $@
.f.o : # theParameters
	$(FC77) $(F77FLAGS) -c $< -o $@
endif

# Define rule to make C objects
%.cc.o : C++/%.cxx
	$(CC) $(CCFLAG) -c $< -o $@

# Database related flags to be passed to the makefiles in subdirectories
#  db_tree is the default one for visualization and other postprocessing
#  which will initialize db_tree regardless of the main database used.
# Defining MP, e.g. as MP=1, will set multiple processor mode.
DBEXTRAFLAG =
CC_OBJ = tree.cc.o interface.cc.o amr.cc.o
NMP_OBJ =
NMP_OUT =
MULTIPROC = 0

#
# Get data structure type compiling from command line arg
#

# Mixed DB (Fortran, fixed sizes, line extractions)
ifeq ($(DB),db_mix)
CC_OBJ =tree.cc.o interface.cc.o amr.cc.o
DATA_STRUCT_SPECIFIC_WLT = db_mix.o wavelet_3d_mix.o $(CC_OBJ)
DATA_STRUCT_SPECIFIC_WLT_VARS = db_mix_vars.o wavelet_3d_mix_vars.o
DBEXTRAFLAG = -DTREE_VERSION=1
DATA_STRUCT_SPECIFIC_VT = 
#$(error db_mix is not ready yet)
theDB:

else
# Tree DB (Fortran, variable size nodes)
# TREE_VERSION=2 - allocatable arrays inside tree node
# TREE_VERSION=3 - sizes are fixed in user_case_db module
ifeq ($(DB),db_treef)
CC_OBJ =
DATA_STRUCT_SPECIFIC_WLT = db_tree.o wavelet_3d_tree.o
DATA_STRUCT_SPECIFIC_WLT_VARS = db_tree_vars.o db_tree_f.o wavelet_3d_tree_vars.o
DBEXTRAFLAG = -DTREE_VERSION=3
DATA_STRUCT_SPECIFIC_VT = 
theDB:

else
# Tree DB (C++ quad tree with fixed size nodes)
ifeq ($(DB),db_tree1)
CC_OBJ = tree.cc.o interface.cc.o amr.cc.o
DATA_STRUCT_SPECIFIC_WLT = db_tree.o wavelet_3d_tree.o $(CC_OBJ)
DATA_STRUCT_SPECIFIC_WLT_VARS = db_tree_vars.o wavelet_3d_tree_vars.o
DBEXTRAFLAG = -DTREE_VERSION=1
DATA_STRUCT_SPECIFIC_VT = 
theDB:

else
# Tree DB (C++ variable size nodes)
ifeq ($(DB),db_tree)
CC_OBJ = tree.cc.o interface.cc.o amr.cc.o
DATA_STRUCT_SPECIFIC_WLT = db_tree.o wavelet_3d_tree.o $(CC_OBJ)
DATA_STRUCT_SPECIFIC_WLT_VARS = db_tree_vars.o wavelet_3d_tree_vars.o
DATA_STRUCT_SPECIFIC_VT = variable_thresholding.o
ifdef MP
MULTIPROC=1
endif
theDB:

else
# Lines DB
ifeq ($(DB),db_lines)
DATA_STRUCT_SPECIFIC_WLT =  db_lines_V2_frontend.o   wavelet_3d_lines.o
DATA_STRUCT_SPECIFIC_WLT_VARS = db_lines_vars.o db_lines_V2_backend.o wavelet_3d_lines_vars.o
DATA_STRUCT_SPECIFIC_VT = 
theDB:
	@echo 'DB is defined: ' $(DB)
else
# Pseudo Lines DB
ifeq ($(DB),pseudo_db_lines)
DATA_STRUCT_SPECIFIC_WLT = db_lines.o wavelet_3d_lines.o
DATA_STRUCT_SPECIFIC_WLT_VARS = wavelet_3d_lines_vars.o
DATA_STRUCT_SPECIFIC_VT = 
theDB:
	@echo 'DB is defined: ' $(DB)
else
# working array version
ifeq ($(DB),db_wrk)
DATA_STRUCT_SPECIFIC_WLT = wavelet_3d_wrk.o
DATA_STRUCT_SPECIFIC_WLT_VARS = wavelet_3d_wrk_vars.o
DATA_STRUCT_SPECIFIC_VT = 
theDB:
	@echo 'DB is defined: ' $(DB)
	@echo 'OBJ =  ' $(OBJ)
	@echo 'TEST_CASE_NAM =  ' $(TEST_CASE_NAME)
	@echo 'TEST_INP  =  ' $(TEST_INP)
	@echo 'TEST_OUT  =  ' $(TEST_OUT)
	@echo 'EXEC  =  ' $(EXEC)
	@echo 'CASE  =  ' $(CASE)
else
theDB:
	$(error 'DB is not correctly defined. DB types: db_mix, db_tree, db_lines, db_wrk, etc ...')
endif
endif
endif
endif
endif
endif
endif


# set DB related flag DBEXTRAFLAG, MPI library link path
# set flags to compile n2m tool together with parallel wlt_3d
ifeq ($(MULTIPROC),1)
FFLAGS += $(MPIFLAGS) $(MPIINC)
LINKLIB += $(MPILIB)
NMP_OBJ = nmp_aux.o nmp_main.o
NMP_OUT = n2m
EXECTYPE = parallel
MULTIPROC_FLAG = -DMULTIPROC
CCFLAG += $(MULTIPROC_FLAG)
else
EXECTYPE = $(DB)
endif
CCFLAG += $(DBEXTRAFLAG)
FFLAGS += $(DBEXTRAFLAG)

#
# identify machine/compiler parameters set in makefiles.machine_specific/makefile...
theParameters:
	@echo '##########################################'
	@echo ' Prameters set in makefile.$(WLT_COMPILE_HOSTNAME) '
	@echo 'FTAR            = ' $(FTAR)
	@echo 'FC              = ' $(FC)
	@echo 'FC77            = ' $(FC77)
	@echo 'LIB_LAPACK      = ' $(LIB_LAPACK)
	@echo '##########################################'
	@echo ' '

#
# set executable name based on DB and CASE
#
ifeq ($(DEBUG),YES)
  DEBUG_STR = _debug
else
  DEBUG_STR = 
endif
EXEC = $(dir ${CASE})/wlt_3d_${EXECTYPE}_$(basename $(notdir ${CASE}))$(DEBUG_STR).out
EXEC2 = $(dir ${CASE})/n2m_${EXECTYPE}_$(basename $(notdir ${CASE}))$(DEBUG_STR).out


# shared global variables
SHARED_OBJ =    shared_modules.o wlt_3d_main_vars.o debug_vars.o


# Zoltan library (domain decomposition)
ifeq ($(SUPPORT_ZOLTAN),YES)
ifeq ($(MULTIPROC),1)
  DOMAIN_DECOMPOSITION += domain_geometric.o domain_zoltan.o
  CCFLAG += $(ZOLTANINC) $(MPIINC)
  CC_ZOLTAN = zoltan_partitioning.cc.o
endif
else
  DOMAIN_DECOMPOSITION += domain_geometric.o domain_zoltan_empty.o
  ZOLTANLIB=
  ZOLTANINC=
  CC_ZOLTAN=
endif


# Define list of all object files to be made
FFT     =	fft/fft.interface.temperton.o fft/fft.o fft/spectra.o \
		fft/ch_resolution_fs.o fft/fftpacktvms.o

VARS    =	wavelet_3d_vars.o io_3d_vars.o $(DATA_STRUCT_SPECIFIC_WLT_VARS) \
		wavelet_filters_vars.o elliptic_solve_3d_vars.o 

OBJ     =       ${SHARED_OBJ} \
		$(CASE).PARAMS.o \
		$(VARS) $(FFT) \
		fft/fft_util.o $(LOCALMATH) autoCAD_geometry.o additional_nodes.o \
		wavelet_3d.o coord_mapping.o \
		$(DOMAIN_DECOMPOSITION) parallel.o \
		$(DATA_STRUCT_SPECIFIC_WLT) $(CC_ZOLTAN) \
		wavelet_filters.o vector_util.o  util_3d.o input_files_reader.o \
		sgs_incompressible.o hyperbolic.o elliptic_solve_3d.o wlt_FWH.o \
		$(DATA_STRUCT_SPECIFIC_VT) \
		$(CASE).o \
		io.cc.o io_3d.o \
		default_util.o time_int_cn.o read_data_wray.o read_init.o \
		time_int_krylov_3d.o
WLT3D_OBJ = wlt_3d_main.o

ifndef LIB_LAPACK
LAPACK_OBJ = zgetri.o zgetrf.o  r1mach.o needblas.o gaussq.o dtrsm.o \
		dqage.o dqag.o dgels.o  \
		 dgeev.o dgamma.o \
		derfc.o derf.o  d1mach.o \
		m1qn3.o
endif


ifneq (,$(findstring YES,$(SUPPORT_HDF))) 
READNETCDF	=	read_netcdf.o 
NETCDFLINK	=	read_netcdf.o -I$(HDFINC) -L$(HDFLIBDIR) -lnetcdf -L$(LIBJPEG) -ljpeg -lz $(LIBS)
else
READNETCDF	=	read_netcdf_empty.o
NETCDFLINK	=	read_netcdf_empty.o
endif

ifeq ($(LINK),$(FC))
   LINKFLAGS:=${FFLAGS}
else
   LINKFLAGS:=${CCFLAG}
endif

all: wlt_3d vis $(NMP_OUT) #inter convert spectrum spectrum_adaptive int
wlt_3d: wlt_3d_  #$(NMP_OUT)

wlt_3d_ : theDB theCase   ${SHARED_OBJ} $(READNETCDF) $(OBJ) $(WLT3D_OBJ) $(LAPACK_OBJ)
	@echo '!!!                                        !!!'
	@echo '!!! check stack size (ulimit -s unlimited) !!!'
	@echo '!!!                                        !!!'
	$(LINK) $(LINKFLAGS)  $(OBJ) $(WLT3D_OBJ) $(OBJ_ARC_SPECIFIC) $(NETCDFLINK) $(LIB_LAPACK) $(LAPACK_OBJ) -o $(EXEC) $(ZOLTANLIB) $(LINKLIB)

n2m: theDB theCase ${SHARED_OBJ}  $(READNETCDF) $(OBJ) $(NMP_OBJ) $(LAPACK_OBJ)
	$(LINK) $(LINKFLAGS)  $(OBJ) $(NMP_OBJ) $(OBJ_ARC_SPECIFIC) $(NETCDFLINK) $(LIB_LAPACK) $(LAPACK_OBJ) -o $(EXEC2) $(ZOLTANLIB) $(LINKLIB)

nmp: n2m


#
# Get case we are compiling for from command line arg
theCase:
ifndef CASE
	$(error 'ERROR: CASE is not defined. The correct syntax is: gmake CASE=casename')
endif



read_netcdf.o: read_netcdf.f90
	$(FC) -c $(FFLAGS)  -I$(HDFINC) $< -o $@

#$(OBJ): makefile

fft/fftpacktvms.o: fft/fftpacktvms.f
	$(FC77) $(F77FLAGS)  $(RSIZE8) fft/fftpacktvms.f -c -o fft/fftpacktvms.o


# nonadaptive matlab postprocessing for showme3D.m script (will use db_wrk)
inter: theDB
	(cd post_process;  gmake)

# postprocessing to convert output data to different graphics file formats
convert: theDB
	(cd post_process/convert_output_format; gmake \
	"DATA_STRUCT_SPECIFIC_WLT_VARS=$(DATA_STRUCT_SPECIFIC_WLT_VARS)" \
	"DATA_STRUCT_SPECIFIC_WLT=$(DATA_STRUCT_SPECIFIC_WLT)" \
	"DBEXTRAFLAG=$(DBEXTRAFLAG)")

# spectrum computing
spectrum: theDB
	(cd  post_process/view_spectra; gmake \
	"DATA_STRUCT_SPECIFIC_WLT=$(DATA_STRUCT_SPECIFIC_WLT)" \
	"DATA_STRUCT_SPECIFIC_WLT_VARS=$(DATA_STRUCT_SPECIFIC_WLT_VARS)" \
	"CASE=$(CASE)" \
	"DBEXTRAFLAG=$(DBEXTRAFLAG)" )
spectrum_adaptive: theDB
	(cd  post_process/view_spectra/adaptive; gmake \
	"DATA_STRUCT_SPECIFIC_WLT=$(DATA_STRUCT_SPECIFIC_WLT)" \
	"DATA_STRUCT_SPECIFIC_WLT_VARS=$(DATA_STRUCT_SPECIFIC_WLT_VARS)" \
	"DBEXTRAFLAG=$(DBEXTRAFLAG)" "MULTIPROC=$(MULTIPROC)")

# volume rendering, interpolation, other visualization (to be combined into single directory)
vis: theDB
	(cd  post_process/visualization; gmake \
	"DATA_STRUCT_SPECIFIC_WLT=$(DATA_STRUCT_SPECIFIC_WLT)" \
        "DATA_STRUCT_SPECIFIC_WLT_VARS=$(DATA_STRUCT_SPECIFIC_WLT_VARS)" \
	"CASE=$(CASE)" \
	"DBEXTRAFLAG=$(DBEXTRAFLAG)" "CC_OBJ=$(CC_OBJ)" "MULTIPROC=$(MULTIPROC)")
int: theDB
	(cd  post_process/interpolation; gmake \
        "DATA_STRUCT_SPECIFIC_WLT=$(DATA_STRUCT_SPECIFIC_WLT)" \
        "DATA_STRUCT_SPECIFIC_WLT_VARS=$(DATA_STRUCT_SPECIFIC_WLT_VARS)" \
        "CASE=$(CASE)" \
	"DBEXTRAFLAG=$(DBEXTRAFLAG)" "CC_OBJ=$(CC_OBJ)")

# user_post_process computing
user_post_process: theDB
	(cd  post_process/user_post_process; gmake \
	"DATA_STRUCT_SPECIFIC_WLT=$(DATA_STRUCT_SPECIFIC_WLT)" \
	"DATA_STRUCT_SPECIFIC_WLT_VARS=$(DATA_STRUCT_SPECIFIC_WLT_VARS)" \
	"DBEXTRAFLAG=$(DBEXTRAFLAG)" "MULTIPROC=$(MULTIPROC)")

# run verification test cases
tests:
	@(cd TestCases && gmake tests); \
	(cd post_process && gmake tests)


#########################################################################################################
# END BEGIN TEST CASES
#########################################################################################################

clean:
	@\rm -rf $(CLEANFLS); \
	(cd  fft/adaptive/ && gmake clean); \
	(cd post_process && gmake clean); \
	(cd TestCases && gmake clean); \
	(cd Documentation/ && make clean); \
	(find .   \(  -name '*.o' \)  -exec rm -f  {} \; ) ;

distclean:
	@\rm -rf $(CLEANFLS) *~; \
	(cd  fft/adaptive/ && gmake distclean); \
	(cd post_process/ && gmake distclean); \
	(cd TestCases && gmake distclean); \
	(cd Documentation/ && make distclean); \
	(find .   \(  -name '*.o' \)  -exec rm -f  {} \; ) ;

doc: man
man:
	@(cd Documentation/ && make)

tartest:
	echo $(FTAR)

tar:
	tar $(FTAR) ./wlt_3d.tar.gz \
		$(TAR_MACHINE_SPECIFIC)  \
		./wavelet_3d.f90 input_files_reader.f90 \
		./db_lines.f90 ./user_case_db_defs.f90 \
		./db_lines_V2_frontend.f90 ./db_lines_V2_backend.f90 \
		./wavelet_3d_lines.f90 ./default_util.f90 \
		./wlt_3d_main.f90  ./wlt_3d_main_vars.f90 ./elliptic_solve_3d.f90  ./io_3d.f90  \
		./util_3d.f90 ./shared_modules.f90 ./vector_util.f90 \
		./time_int_krylov_3d.f90 ./wavelet_3d_wrk.f90 \
		./user_elliptic_IC.f90 ./sgs_incompressible.f90 ./hyperbolic.f90 \
		./time_integration_meth2_aux.f90 ./time_integration_meth2.f90\
		./user_case.f90 \
		./case_isoturb.f90 ./case_vortex_array.f90 ./case_channel.f90 \
		./case_sphere.f90 ./case_vortex_timeint_cn.f90 ./case_isoturb_timeint_cn.f90 \
		./case_isoturb_Lagrangian_Mdl.V2.f90 ./case_isoturb_Lagrangian_Mdl.f90  \
		./case_isoturb_Lagrangian_Mdl.f90 \
		./read_netcdf.f90 ./read_netcdf_empty.f90 ./time_int_cn.f90 \
		./read_init.f90    \
		./user_case_stubs.f90 \
		./dgels.f ./zgetri.f ./zgetrf.f ./dgeev.f \
		./gaussq.f ./dqag.f ./d1mach.f \
		./derfc.f ./derf.f ./dgamma.f ./dqage.f \
		./needblas.f ./r1mach.f ./dtrsm.f \
		./makefile ./post_process/makefile \
		./poisson_3d_template.inp \
		./post_process/c_wlt_3d.m  \
		./post_process/c_wlt_3d_movie.m  \
		./post_process/c_wlt_inter.f90    \
		./post_process/inter3d.m \
		./post_process/mycolorbar.m \
		./post_process/mycontourf.m  \
		./post_process/c_wlt_3d_isostats.m \
		./post_process/vor_pal.m \
		./post_process/view_spectra/c_wlt_turbstats.f90\
		./post_process/view_spectra/makefile \
		./fft/spectra.f90 \
		./fft/ch_resolution_fs.f90 \
		./fft/fftpacktvms.f \
		./fft/fft.interface.temperton.f90   \
		./fft/fft.f90  \
		./fft/fft_util.f90 \
		./read_data_wray.f90 

#
# fix dos linefeed/return -> linefeed to compile correctly on unix
dos2unix:
	find .   \( -name '*.f90' -o -name '*.f'  \)  -exec  echo {} \; -exec dos2unix --dos2unix {} \;

