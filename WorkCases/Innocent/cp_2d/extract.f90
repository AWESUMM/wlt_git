!Extract all point on the face(dim)=-1 up to level id_jlev
    i_p_face(0) = 1
    DO ibi=1,dim
       i_p_face(ibi) = i_p_face(ibi-1)*3
    END DO
    i_p_xyz(0) = 1
    DO i=1,dim
       i_p_xyz(i) = i_p_xyz(i-1)*(1+nxyz(i))
    END DO
    j_face = min(j_zn,j_mx)
    i_p_face_xyz(0) = 1
    DO i=1,dim
       i_p_face_xyz(i) = i_p_xyz(i-1)*(mxyz(i)*2**(j_face-1)+1-prd(i)) )
    END DO

    ! note that id_jlev <= j_lev
    DO face_type = 0, 3**dim - 1
       face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
       IF(face(dim) == -1) THEN
          DO j = 1, MIN(id_jlev,j_face)
             DO wlt_type = MIN(j-1,1),2**dim-1
                DO j_df = j, j_lev
                   DO k = 1, indx_DB(j_df,wlt_type,face_type,j)%length
                      i = indx_DB(j_df,wlt_type,face_type,j)%p(k)%i  ! i is 1...nwlt index on the adaptive mesh
                      ii = indx_DB(j_df,wlt_type,face_type,j)%p(k)%ixyz ! 1-D index on non-adaptive mesh
                      ixyz(1:dim) = INT(MOD(ii-1,i_p_xyz(1:dim))/i_p_xyz(0:dim-1)) ! dim-dimensional index on the non-adaptive mesh at j_lev level of resolution
                      ixyz_face(1:dim) = ixyz(1:dim)/2**(j_lev-j_face) ! dim-dimensional index on the non-adaptive mesh at j_face level of resolution, note j_lev >= j_face
                      iface = 1+SUM(ixyz_face(1:dim-1)*i_p_face_xyz(0:dim-2))
                      nface2nwlt(iface) = i
                   END DO
                END DO
             END DO
          END DO
       END IF
    END DO
       
