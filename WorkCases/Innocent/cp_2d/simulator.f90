!>
!<
PROGRAM simul
  USE com_tools
  USE solver_tools
IMPLICIT NONE


  INTEGER, PARAMETER ::  &
       M1QN3_DIRECT   = 0,& !> direct communication in M1QN3, used for parameter reverse
       M1QN3_REVERSE  = 1,& !> reverse communication in M1QN3, used for parameter reverse
       SIMUL_NOTHING  = 1,& !> nothing required by M1QN3, used for parameter indic
       SIMUL_COSTGRAD = 4,& !> M1QN3 requires cost function and its gradient, used for parameter indic
       SIMUL_ASK_STOP = 0,& !> something that M1QN3 can't not understand happened
       SIMUL_IMPOSSIBLE=-1  !> impossible to compute the cost function and/or its gradient

!variables for the minimization algorithm
!!$  !> brief Name of the subroutine that provide the cost function and its gradient in direct communication
!!$  !! \detail In direct communication, simul is the name of the simulator inside m1qn3.
!!$  !! The simulator is the subroutine that computes the value of the (cost) function f and
!!$  !! the value of its gradient g at the current iterate. When m1qn3 needs these values, it executes the instruction
!!$  !! call simulator (...)
!!$  !! In reverse communication, the empty subroutine simul_rc, provided in the standard distribution can be used
!!$  !! EXTERNAL simul_rc
!!$  !<
!!$  EXTERNAL simulator
!!$  !> \brief Calling name of the subroutine that computes the inner product <u, v> of two vectors u and v of Rn.
!!$  !! \detail This subroutine is supposed to have the following declaration statement:
!!$  !! subroutine prosca (n, u, v, ps, izs, rzs, dzs).
!!$  !<
!!$  EXTERNAL prosca
!!$  !> \brief Calling name of the subroutine that makes the change of variable
!!$  !! \detail This subroutine is used only in DIS mode. It is supposed to have the following declaration statement:
!!$  !!   subroutine ctonb (n, x, y, izs, rzs, dzs).
!!$  !<
!!$  EXTERNAL ctonb
!!$  !> \brief Calling name of the subroutine that does the operation reverse to the one done by ctonb
!!$  !! \detail This subroutine is used only in DIS mode. It is supposed to have the following declaration statement:
!!$  !!  subroutine ctcab (n, y, x, izs, rzs, dzs).
!!$  !<
!!$  EXTERNAL ctcab
  !> \brief Positive integer variable that gives the numbers of vector pairs used to approximate the Hessian.
  !! \detail
  !<
  INTEGER :: ig_npairs = 2
  !> \brief Positive integer variable that gives the size of the control vector or the dimension of the problem.
  !! \detail
  !<
  INTEGER :: ig_nctl
  !> \brief Control vector
  !! \detail It must be an array of dimension ig_nctl. On entry, it is supposed to be the initial value.
  !! On return, it is the value of the final point calculated by m1qn3.
  !<
  REAL(KIND=cp), DIMENSION(:), ALLOCATABLE  :: rga_ctl
  
  !> \brief
  !! \detail On entry, it is supposed to be the value of the cost function evaluated at the initial value of the control variable
  !! On return, it is the value of f at the final point.
  !<
  REAL(KIND=cp) :: rg_cost
  
  !> \brief Array variable for the gradient of the cost function
  !! \detail On entry, it is supposed to be the value of the gradient of the cost function at the initial control variable.
  !! On return with omode = 1 (see below), it is the value of the gradient of the cost function at the final point.
  !! For other output modes, its value is undetermined.
  !<
  REAL(KIND=cp), DIMENSION(:), ALLOCATABLE  :: rga_grad
  
  !> \brief Resolution in the control vector for the l$B!g(B norm (positive)
  !! \detail two points whose distance in the sup-norm is less than dxmin will be considered as indistinguishable by the optimizer.
  !<
  REAL(KIND=cp) :: rg_xmin
  
  !> \brief Estimation of the expected decrease in the cost function during the first iteration (positive)
  !! \detail
  !<
  REAL(KIND=cp) :: rg_df1
  
  !> \brief Stopping criterion that is based on the norm of the gradient of the cost function
  !! \detail value range is ]0, 1[
  !!
  !<
  REAL(KIND=cp) :: rg_epsg
  
  !> \brief norm that is used to test optimality (see the argument rg_epsg).
  !! \detail It can be one of the following strings:
  !! - N!twoN" denotes the Euclidean or $(C'$(B2 norm
  !! - N!supN" denotes the sup or $(C'$$B!g(B norm
  !! - N!dfnN" denotes the norm k associated with the scalar product defined in the user-supplied subroutine prosca
  !<
  CHARACTER(LEN=3) :: aga_norm
  
  !> \brief variable that controls the outputs on channel io
  !! \detail
  !! 0 : No print.
  !! >= 1 Initial and final printouts, error messages.
  !! >= 3 One line of printout per iteration that gives: the index k of the iteration going from the point xk to the point xk+1
  !!      the number of time the simulator has been called, the value f(xk) of the objective function and the directional derivative
  !! >= 4 Print outs from mlis3 during the line-search: see the write-up on mlis3 in modulopt library.
  !! >= 5 Some more printouts at the end of iteration k (see m1qn3 doc for details)
  !<
  INTEGER :: ig_impres
  
  !> \brief variable that will be taken as the channel number for the outputs
  !! \detail
  !<
  INTEGER :: ig_m1qn3io
  
  !> \brief  Input mode of m1qn3 that tunes its behavior.
  !! \detail Integer array of dimension 3 with the following values are meaningful.
  !! imode(1) determines the scaling mode of m1qn3.
  !!   - M1qn3 will run in DIS (recommended) mode if imode(1) = 0
  !!   - M1qn3 will run in SIS mode if imode(1) = 1
  !! imode(2) specifies the starting mode of m1qn3.
  !!   - A cold start is performed if imode(2) = 0: the first descent direction is then $B!](Bg1.
  !!   - A warm start is performed if imode(2) = 1: the first descent direction is $B!](BW1g1 (W1 Hessian approx)
  !! imode(3) specifies in direct communication when the simulator has to be called with indic = 1 or
  !!   similarly in reverse communication, when m1qn3 returns to the calling subroutine with indic = 1.
  !!   When imode(3) = 0, the simulator is never called with indic = 1
  !!   When imode(3) > 0, the simulator is called with indic = 1 every imode(3) iteration(s), starting at iteration 1.
  !<
  INTEGER, DIMENSION(3) :: iga_imode
  
  !> \brief output mode of m1qn3
  !! \detail Meaningful values
  !! = 0: The simulator asks to stop by returning the value indic = 0.
    !! = 1: This is the normal way of stopping for m1qn3: the test on the gradient is satisfied (see the meaning of rg_epsg).
  !! = 2: One of the input arguments is not well initialized. This can be:
  !!  - n <= 0, niter <= 0, nsim <= 0, dxmin <= 0.0 or epsg mot in ]0, 1[,
  !!  - ndz < 5n + 1 (in SIS mode) or ndz < 6n + 1 (in DIS mode): not enough storage in memory
  !!  - the contents of iz is not correct for a warm restart,
  !!  - the starting point is almost optimal (the norm of the initial gradient is less than 10$B!](B20).
  !! = 3: The line-search is blocked on tmax = 1020 (see section 4.4 and then documentation on mlis3 in modulopt library).
  !! = 4: The maximal number of iterations is reached.
  !! = 5: The maximal number of simulations is reached.
  !! = 6: Stop on dxmin during the line-search (see section 4.4 of m1qn3 doc).
  !! = 7: Either hg, di is nonnegative or hy, si is nonpositive (see section 4.4 of m1qn3 doc).
  !! For additional information and comments, see section 4  of m1qn3 doc.
  !<
  INTEGER :: ig_omode
  
  !> \brief Maximal number of iterations accepted from m1qn3.
  !! \detail m1qn3 uses this variable to return the number of iterations really done
  !<
  INTEGER :: ig_niter
  
  !> \brief Maximal number of simulations accepted from m1qn3.
  !! \detail m1qn3 uses this variable to return the number of simulations really done
  !<
  INTEGER :: ig_nsimul
  
  !> \brief working array for m1qn3
  !! \detail
  !<
  INTEGER, DIMENSION(5) :: iga_iz
  
  !> \brief working array for m1qn3
  !! \detail this array is of size ndz
  !<
  REAL(KIND = dp), DIMENSION(:), ALLOCATABLE :: rga_dz
  
  !> \brief Size of the working array rga_dz for m1qn3
  !! \detail In SIS mode, m1qn3 needs a working area formed of at least 3 vectors of
  !! dimension n (dk, gk and an auxiliary vector) and it needs for each update (Hessian approx)
  !! one scalar and two vectors. Therefore, if m is the desired number of updates for forming the matrix Wk(Hessian approx),
  !! it is necessary to have: ig_ndz >= 3n +m(2n + 1). m1qn3 based its calculation on this value to determine m.
  !!  if ndz is less than 5n + 1, m1qn3 stops with omode = 2.
  !! In DIS mode, m1qn3 needs an additional vector of dimension n for storing Dk. So, take ig_ndz >= 4n + m(2n + 1) and m >= 1.
  !<
  INTEGER :: ig_ndz
  
  !> \brief Specify whether direct or reverse communication is desired for m1qn3
  !! \detail In reverse communication, it is used to communicate with the call loop
  !! values :
  !! < 0: implies that m1qn3 will stop immediately using the instruction stop, to prevent entering an infinite call loop in reverse communication,
  !!      due to the fact that the calling program has not left the call loop when m1qn3 returns a negative value;
  !! = 0: indicates that m1qn3 has to work in direct communication;
  !! = 1: indicates that m1qn3 has to work in reverse communication.
  !! it is used by m1qn3 on return to send informations to the calling loop, values
  !! < 0: when m1qn3 has terminated, in which case the call loop must be interrupted;
  !! = 1: the call loop must be pursued.
  !<
  INTEGER :: ig_reverse
  
  !> \brief Indicates the state of the computation required by m1qn3
  !! \detail values
  !! < 0: the computation of f and g required by m1qn3 on its last return was not possible at the given control variable
    !!      this indicates to m1qn3 to adjust the step-size;
  !! = 0: m1qn3 has to stop, for example because some events that m1qn3 cannot understand (not in the field of optimization) has occurred;
  !! > 0: the required computation has been done.
  !! m1qn3 also uses this variable to send information to the calling loop (reverse mode) or simulator (direct mode)
  !! values :
  !! = 1: means that the calling program can do anything except changing the values of indic, n, x, f, and g;
  !!      this value of indic is used by m1qn3 every imode(3) iteration(s), when imode(3) > 0, and never, when imode(3) = 0;
  !! = 4: means that the calling program has to compute f and g, to put them in f and g, and to call back m1qn3.
  !<
  INTEGER :: ig_indic
  
  !> \brief Integer working array for simulator, prosca, ctonb, and ctcab
  !! \detail not used in practice
    !<
  INTEGER, DIMENSION(2)  :: iga_wa !(izs in m1qn3
  
  !> \brief Real working array for simulator, prosca, ctonb, and ctcab
  !! \detail not used in practice
  !<
  REAL, DIMENSION(sp)  :: rga_wa !(rzs in m1qn3)
  
  !> \brief Real working array for simulator, prosca, ctonb, and ctcab
  !! \detail not used in practice
  !<
  REAL(KIND=dp), DIMENSION(2)  :: dga_wa !(dzs in m1qn3)

  !> \brief Shall direct and adjoint problems be solved simultaneously?
  !! this is used for solvers that can solve direct and adjoint problems at the same time, especially space time solvers
  !<
  LOGICAL  :: lg_solve_all_together
  
  !REAL(cp) :: rg_mu, rg_sigma, rg_v0, rg_omega, rg_alpha
  TYPE(exchange_param) :: tg_ep

  CHARACTER(LEN=*), PARAMETER :: ala_namelist = "simulator_namelist"
  CHARACTER(LEN=ip_snl) :: ala_inputfName

  CALL debug( "In simulator; loading "//TRIM(ala_namelist) )
  CALL load_namelist(ala_namelist)
  CALL debug( "In simulator; after loading "//TRIM(ala_namelist) )
  !!!!!
  CALL init_solver(tg_ep, ig_nctl) !Initialize the solver
  CALL debug( "In simulator; after  init_solver" )
!
  SELECT CASE(tg_ep%aa_simul_action)
    CASE (RUN_ASSIM) !data assimilation experiment
      CALL assim_run()
    CASE (RUN_COST) !cost function recquired
      !CALL read_ep_data(tg_ep, BCTL_DATA, INPUT_FILE )
      WRITE(*, *) 'nothing to do for <'//TRIM(tg_ep%aa_solver_action)//'>'
    CASE (RUN_GRADIENT) !gradient of the cost function recquired
      !CALL read_ep_data(tg_ep, BCTL_DATA, INPUT_FILE )
      !WRITE(*, *) 'nothing to do for <'//TRIM(tg_ep%aa_solver_action)//'>'
    CASE (MAKE_OBS) !make observations for twin experiments
      tg_ep%aa_solver_action=MAKE_OBS
			CALL debug( "In simulator; Calling run_solver -> MAKE_OBS" )
      CALL run_solver(tg_ep)
			CALL debug( "In simulator; after run_solver -> MAKE_OBS" )
    CASE (RUN_DIRECT) ! direct model run recquired
      tg_ep%aa_solver_action=RUN_DIRECT
      CALL run_solver(tg_ep)
    CASE (RUN_ADJOINT)! adjoint model run recquired
      tg_ep%aa_solver_action=RUN_ADJOINT
      CALL run_solver(tg_ep)
    CASE (MAKE_CTL)! making a control vector
      tg_ep%aa_solver_action=MAKE_CTL
      !CALL print_ep(tg_ep)
      CALL run_solver(tg_ep)
      CALL debug('back to the main program..........')
    CASE (MAKE_BG)! make default background
      tg_ep%aa_solver_action=MAKE_BG
      CALL run_solver(tg_ep)
    CASE DEFAULT
      WRITE(*, *) 'Unknow action <'//TRIM(tg_ep%aa_solver_action)//'>'
      STOP
  END SELECT
  CALL set_ctlsize(tg_ep, 0)
  WRITE(*, *) 'End of the main program @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@'
  !STOP
  !PAUSE
CONTAINS
  
  SUBROUTINE load_namelist(fName)
    INTEGER, PARAMETER           :: ip_numnam = 68
    CHARACTER(LEN=*), INTENT(IN) :: fName
    INTEGER                      :: il_obs_level, il_nobs_x, il_nobs_t

    REAL(cp) :: rl_sigmaR
    REAL(cp) :: rl_max_coef, rl_nz_ratio, rl_mes_fact, rl_cs_reg!CS parameters
    REAL(cp) :: rl_wGD, rl_wb, rl_wGrad !regularization weighting parameters
    LOGICAL  :: ll_amplitude, ll_location, ll_sigma, ll_restart_from_previous, ll_run_from_ctl, ll_useGD
    CHARACTER(LEN=ip_snl) :: dmt_fName, obs_fName, ogap_fName, ctl_fName, bctl_fName, input_dir, output_dir, ala_reverse
    CHARACTER(LEN=ip_snl) :: ala_action, ala_solver_path

    NAMELIST/NAM_general/&
      ala_action,&
      ig_nctl   ,&
      input_dir ,&
      output_dir,&
      ctl_fName ,&
      obs_fName ,&
      dmt_fName ,&
      bctl_fName,&
      ogap_fName,&
      ala_solver_path,&
      ll_run_from_ctl,&
      lg_solve_all_together,&
      ll_restart_from_previous

    NAMELIST/NAM_m1qn3/& 
      rg_xmin   ,&
      rg_epsg   ,&
      aga_norm  ,&
      ig_impres ,&
      ig_m1qn3io,&
      iga_imode ,&
      ig_niter  ,&
      ig_nsimul ,&
      ig_npairs ,&
      ala_reverse

    NAMELIST/NAM_obs/&
      il_obs_level,&
      il_nobs_x   ,&
      il_nobs_t   ,&
      rl_sigmaR

    NAMELIST/NAM_control/&
      ll_amplitude ,&
      ll_location  ,&
      ll_sigma

    NAMELIST/NAM_regul/&
      ll_useGD,&
      rl_wGD  ,&
      rl_wb   ,&
      rl_wGrad

    NAMELIST/NAM_CS/&
      rl_max_coef,&
      rl_nz_ratio,&
      rl_mes_fact,&
      rl_cs_reg
    
    OPEN(ip_numnam, FILE=fName, FORM='FORMATTED', STATUS='OLD')
    !If one is going to read many blocs in the same file, it is recommended to rewind
    REWIND(ip_numnam)
    READ(ip_numnam, NAM_general)!reading the block NAM_general
    REWIND(ip_numnam)
    READ(ip_numnam, NAM_m1qn3  )!reading the block NAM_m1qn3
    REWIND(ip_numnam)
    READ(ip_numnam, NAM_obs    )!reading the block NAM_obs
    REWIND(ip_numnam)
    READ(ip_numnam, NAM_control)!reading the block NAM_control
        
    REWIND(ip_numnam)
    READ(ip_numnam, NAM_regul  )!reading the block NAM_regul
    
    REWIND(ip_numnam)
    READ(ip_numnam, NAM_CS  )!reading the block NAM_CS

    CLOSE(ip_numnam)
    SELECT CASE (ala_reverse)
      CASE ('REVERSE', 'reverse')
        ig_reverse = M1QN3_REVERSE
      CASE ('DIRECT', 'direct')
        ig_reverse = M1QN3_DIRECT
      CASE DEFAULT
        CALL print_var(ala_reverse, 'In load_namelist; bad value of m1qn3 communication: ')
        STOP
    END SELECT

    tg_ep%aa_solver_path  = TRIM(ala_solver_path)
    tg_ep%aa_simul_action = TRIM(ala_action)
    tg_ep%l_amplitude = ll_amplitude
    tg_ep%l_location  = ll_location
    tg_ep%l_sigma     = ll_sigma
    tg_ep%i_obs_level = il_obs_level
    tg_ep%i_nobs_x    = il_nobs_x
    tg_ep%i_nobs_t    = il_nobs_t
    tg_ep%r_sigmaR    = rl_sigmaR
    tg_ep%l_run_from_ctl          = ll_run_from_ctl
    tg_ep%l_first_simul           = .TRUE.
    tg_ep%l_restart_from_previous = ll_restart_from_previous
    
    tg_ep%r_cost      = 0.0_cp
    !setting CS parameters
    tg_ep%r_max_coef  = rl_max_coef
    tg_ep%r_nz_ratio  = rl_nz_ratio
    tg_ep%r_mes_fact  = rl_mes_fact
    tg_ep%r_cs_reg    = rl_cs_reg
    !regul parameters
    tg_ep%l_useGD = ll_useGD
    tg_ep%r_wGD   = rl_wGD
    tg_ep%r_wb    = rl_wb
    tg_ep%r_wGrad = rl_wGrad
    
    CALL set_data_fileNames(tg_ep, ctl_fName, bctl_fName, obs_fName, dmt_fName, ogap_fName)
    CALL set_data_dir(tg_ep, input_dir, output_dir)
  END SUBROUTINE load_namelist

  SUBROUTINE print_namelist_variables()
    CHARACTER(LEN=ip_snl)   :: array_rformat, array_iformat
    array_rformat = "(A,"//TRIM( NUM2STR(ig_nctl) )//RFORMAT//")"
    array_iformat = "(A,"//TRIM( NUM2STR(ig_nctl) )//IFORMAT//")"

    CALL debug( ''             , 'Print_namelist_variables------------------' )
    CALL debug( ''             , ' NAM_general *****************************' )
    CALL debug( ''             , ' NAM_m1qn3 *******************************' )
    CALL debug( rg_xmin        , '   rg_xmin   = ' )
    CALL debug( rg_epsg        , '   rg_epsg   = ' )
    CALL debug( aga_norm       , '   aga_norm  = ' )
    CALL debug( ig_impres      , '   ig_impres = ' ) 
    CALL debug( ig_m1qn3io     , '   ig_m1qn3io= ' ) 
    CALL debug( iga_imode      , '   iga_imode = ' )
    CALL debug( ig_niter       , '   ig_niter  = ' )
    CALL debug( ig_nsimul      , '   ig_nsimul = ' )
    CALL debug( ig_npairs      , '   ig_npairs = ' )
    CALL debug( ''             , ' NAM_obs *********************************' )
    !CALL debug( ig_nobs_x      , '   ig_nobs_x = ' )
    !CALL debug( ig_nobs_t      , '   ig_nobs_t = ' )
    !CALL debug( rg_sigmaR      , '   rg_sigmaR = ' )
    CALL debug( ''             , ' NAM_control *****************************' )
    CALL debug( ig_nctl        , '   ig_nctl       = ' )
    !CALL debug( rga_ctlt       , '   rga_ctlt      = ' )
    !CALL debug( rga_b_ctl      , '   rga_b_ctl     = ' )
    !CALL debug( rga_sigma_ctl  , '   rga_sigma_ctl = ' )
    !CALL debug( lg_amplitude   , '   lg_amplitude  = ' )
    !CALL debug( lg_location    , '   lg_location   = ' )
    !CALL debug( lg_sigma       , '   lg_sigma      = ' )
    CALL debug( ''             ,' NAM_model *******************************' )
    !CALL debug( rg_mu          , '   rg_mu    = ' )
    !CALL debug( rg_sigma       , '   rg_sigma = ' )
    !CALL debug( rg_v0          , '   rg_v0    = ' )
    !CALL debug( rg_omega       , '   rg_omega = ' )
    CALL debug( ''             ,' Other *******************************' )
    CALL debug( lg_solve_all_together       , '   lg_solve_all_together = ' )
    CALL debug( ''             , 'End of print_namelist_variables ##########' )
  END SUBROUTINE print_namelist_variables
  
  !> run data assimilation experiment
  !!
  !<
  SUBROUTINE assim_run()
    
    CALL init_assim()
    OPEN(UNIT=ip_ctl_out, FILE='rt_ctl_evolution.dat', STATUS='REPLACE',FORM='FORMATTED')
    OPEN(UNIT=ip_grad_out,FILE='rt_grad_evolution.dat',STATUS='REPLACE',FORM='FORMATTED')

    !initialization of the minimizer
    ig_indic = SIMUL_COSTGRAD
    CALL simulator(ig_indic, ig_nctl, rga_ctl, rg_cost, rga_grad, iga_wa, rga_wa, dga_wa )
    CALL debug('', 'After initialization of the minimizer')
    !CALL dpause()
    SELECT CASE (ig_indic)
    CASE (SIMUL_IMPOSSIBLE)!impossible to compute the cost function and/or its gradient at the given ctl
      CALL print_var('', ' Impossible to compute the cost function and/or its gradient at the initial point')
      !CALL print_ctl_bound(tg_ep)
      !STOP
    CASE (SIMUL_ASK_STOP)!something that can not be handle by the minimizer happened
      CALL print_var('', ' Something that can not be handled by the minimizer happened')
      STOP
    CASE DEFAULT
    !
    END SELECT

    CALL debug(ig_niter, 'Before call to m1qn3,ig_niter = ')
!!$    CALL dpause()
    rg_df1 = rg_cost*0.75_cp
    SELECT CASE(ig_reverse)
      CASE(M1QN3_REVERSE)
        CALL m1qn3_reverse_driver()
      CASE(M1QN3_DIRECT)
        CALL m1qn3_direct_driver()
    END SELECT
    CALL debug(ig_niter, 'After call to m1qn3,ig_niter = ')
!!$    CALL dpause()
    CLOSE(ip_ctl_out)
    CLOSE(ip_grad_out)
    CALL clean_assim()
    CALL debug('', 'After clean_assim ')
    CALL write_ep_data(tg_ep, ACTL_DATA, OUTPUT_FILE, 'Simulator for ANALYSIS') !saving the analysed ctl
    CALL write_ep_data(tg_ep, BCTL_DATA, OUTPUT_FILE, 'Simulator for BACKGROUND')! saving the background
    !Saving the analysed trajectory
    tg_ep%l_restart_from_previous = .FALSE.
    tg_ep%aa_solver_action = MAKE_ADMT
    CALL run_solver(tg_ep)
  END SUBROUTINE assim_run

  !> Computes initial condition in direct mode, this is the system state at initial time
  !!
  !! \detail here this routine is used as the driver for the minimizer algorithm
  !! It runs m1qn3 in direct  mode
  !<
  SUBROUTINE m1qn3_direct_driver()
   
    ig_reverse = M1QN3_DIRECT
    CALL m1qn3(simulator, prosca, ctonb, ctcab, ig_nctl, rga_ctl, rg_cost, rga_grad,&
            rg_xmin, rg_df1, rg_epsg, aga_norm, ig_impres, ig_m1qn3io,&
            iga_imode, ig_omode, ig_niter, ig_nsimul, iga_iz, rga_dz, ig_ndz,&
            ig_reverse, ig_indic, iga_wa, rga_wa, dga_wa &
     )

    CALL m1qn3_print_stat(ig_omode, ig_niter, ig_nsimul)
  END SUBROUTINE m1qn3_direct_driver


  !> Computes initial condition in reverse mode, this is the system state at initial time
  !!
  !! \detail here this routine is used as the driver for the minimizer algorithm
  !! It runs m1qn3 in reverse mode
  !<
  SUBROUTINE m1qn3_reverse_driver ()
    IMPLICIT NONE
    !local variable
    
    !initialization
    !CALL make_twin_obs(u, nlocal, ne_local, t_local, scl, scl_fltwt, iter)

    !1 - computing or loading the initial control variable
    
    !2 - computing the cost function and its gradient at the initial control variable
    !CALL init_assim

    CALL debug('', 'In M1QN3_driver')

    !Setting the communication mode
    ig_reverse = M1QN3_REVERSE !set reverse communication on
    DO WHILE (ig_reverse == M1QN3_REVERSE)
       CALL m1qn3(&
            simulator, prosca, ctonb, ctcab, ig_nctl, rga_ctl, rg_cost, rga_grad,&
            rg_xmin, rg_df1, rg_epsg, aga_norm, ig_impres, ig_m1qn3io,&
            iga_imode, ig_omode, ig_niter, ig_nsimul, iga_iz, rga_dz, ig_ndz,&
            ig_reverse, ig_indic, iga_wa, rga_wa, dga_wa &
       )
       IF (ig_reverse == M1QN3_REVERSE) THEN
             CALL simulator(ig_indic, ig_nctl, rga_ctl, rg_cost, rga_grad, iga_wa, rga_wa, dga_wa )
       END IF
    END DO
    CALL m1qn3_print_stat(ig_omode, ig_niter, ig_nsimul)
    !post processing
  END SUBROUTINE m1qn3_reverse_driver

  !> \brief Computes the cost function and its gradient
  !! \param [in,out] id_indic Indicates what is required from the simulator
  !! \param [in] id_ctl size of the control vector
  !! \param [in,out] rda_ctl control vector
  !! \param [in,out] rda_grad Gradient of the cost function
  !! \param [in,out] rd_cost value of the cost function,
  !! \param [in] ida_wa Integer working array for simulator
  !! \param [in] rda_wa Simple precision working array for simulator
  !! \param [in] dda_wa Double precision working array for simulator
  !! \detail parameter id_indic has the following meaningful values
  !! - 4 : compute the cost function and its gradient
  !! - 1 : informes the simulator that the minimizer is moving to the next iteration
  !<
  SUBROUTINE simulator(id_indic, id_ctl, rda_ctl, rd_cost, rda_grad, ida_wa, rda_wa, dda_wa)
    IMPLICIT NONE
    !parameters for m1qn3 simulator
    INTEGER, INTENT(INOUT) :: id_indic
    INTEGER, INTENT(IN)    :: id_ctl
    REAL(KIND=cp),DIMENSION(id_ctl),INTENT(INOUT) :: rda_ctl
    REAL(KIND=cp),DIMENSION(id_ctl),INTENT(INOUT) :: rda_grad
    REAL(KIND=cp),INTENT(INOUT) :: rd_cost
    !*V [idr]da_wa : working array inustite (contrainte modulopt)
    INTEGER,DIMENSION(2),INTENT(IN) :: ida_wa
    REAL(KIND=sp), DIMENSION(2),INTENT(IN) :: rda_wa
    REAL(KIND=dp), DIMENSION(2),INTENT(IN) :: dda_wa
    !local variables
    !REAL(KIND=cp) :: rl_costb, rl_tmp
    CHARACTER(LEN=80)   :: array_format
    
    array_format = "("//TRIM( NUM2STR(id_ctl+1) )//RFORMAT//")"
    !
    CALL nothing(ida_wa, rda_wa, dda_wa)!to avoid warning of unused variables
    !
    !PRINT*, 'Entering Simulator ---------------------------------'
    SELECT CASE (id_indic)
    CASE(SIMUL_COSTGRAD) !required : cost function and its gradient
       !** initializing exchange parameters
       !tg_ep%i_nctl = id_ctl
       tg_ep%ra_ctl = rda_ctl

       IF(get_nb_outOfBound(tg_ep)/=0)THEN
          id_indic = SIMUL_IMPOSSIBLE
          CALL print_ctl_bound(tg_ep)
       ELSE
          !/!\ lg_solve_all_together should be set to .TRUE. only if the solver can solve the direct and the adjoint problem simultaneously
          IF(lg_solve_all_together)THEN
             tg_ep%aa_solver_action=RUN_COSTGRAD
             CALL run_solver(tg_ep)
          ELSE
            !**   1. Computing the cost function
            !PRINT*, 'integrating direct '
            
            tg_ep%aa_solver_action=RUN_COST
            CALL run_solver(tg_ep)
            IF( .NOT. tg_ep%l_simul_diverged ) THEN
              !**   2. computing the gradient
              tg_ep%aa_solver_action=RUN_GRADIENT
              CALL run_solver(tg_ep)
            END IF
          END IF!lg_solve_all_together
          IF( .NOT. tg_ep%l_simul_diverged ) THEN
            rd_cost  = tg_ep%r_cost
            rda_grad = tg_ep%ra_grad
          ELSE
            id_indic = SIMUL_IMPOSSIBLE
          END IF
       END IF!(il_nb_outOfBound/=0)
       tg_ep%l_first_simul = .FALSE.
    CASE(SIMUL_NOTHING)! m1qn3 is moving to the next iteration
      WRITE(ip_ctl_out, FMT=array_format)rda_ctl+tg_ep%ra_b_ctl, tg_ep%r_cost
      WRITE(ip_grad_out, FMT=array_format)rda_grad
      CALL debug('', ' In simulator::simulator; m1qn3 moving to the next iteration ')
      tg_ep%aa_solver_action = RUN_NOTHING
      CALL run_solver(tg_ep)
    CASE DEFAULT !This value of indic is not supported
      CALL debug(id_indic, ' In simulator::simulator; indic value not supported: ')
      STOP
    END SELECT!( id_indic == M1QN3_COSTGRAD)

    !PRINT*, 'Exiting Simulator -----------------------------'
  END SUBROUTINE simulator
  
  !> \brief Initializes variables for data assimilation
  !<
  SUBROUTINE init_assim()
    IMPLICIT NONE
    !INTEGER, INTENT(IN) :: id_nctl
    !LOGICAL, DIMENSION(3) :: lla_param
    !REAL(cp), DIMENSION(:, :), ALLOCATABLE :: rla_obs
    !INTEGER :: il_nbCol
    
    CALL debug('', 'Entering init_assim')
    IF(ig_nctl<=0)THEN
       WRITE(*,*) ' In init_assim : zero or negative size control vector'
       STOP
    END IF
    ig_ndz = 4*ig_nctl + ig_npairs*( 2*ig_nctl + 1 )
    CALL debug('', ' In init_assim : allocating space for optimization process')
    ALLOCATE(&
         rga_ctl (ig_nctl),&
         rga_grad(ig_nctl),&
         rga_dz  (ig_ndz )&
    )
    
    rga_ctl = tg_ep%ra_ctl
    
    CALL debug('', 'Exiting init_assim ')
  END SUBROUTINE init_assim
  
  SUBROUTINE clean_assim()
    CALL debug('', ' In clean_assim : deallocating assim variables')
    DEALLOCATE(rga_ctl, rga_grad, rga_dz)
  END SUBROUTINE clean_assim

  SUBROUTINE m1qn3_print_stat(id_omode, id_niter, id_nsim)
    INTEGER, INTENT(IN) :: id_omode, id_niter, id_nsim
    CALL debug('', 'm1qn3_print_stat:: output from m1qn3 $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$')
    CALL debug(id_niter, '  **Number of iterations = ')
    CALL debug(id_nsim , '  **Number of simulations = ')
    CALL debug(id_omode, '  **M1QN3 ends with output mode = ')
    SELECT CASE(id_omode)
    CASE (0)
       CALL debug('', '  The simulator ask to stop by returning the value indic = 0')
    CASE (1)
       CALL debug('', '  Normal way of stopping for m1qn3: the test on the gradient is satisfied')
    CASE (2)
       CALL debug('', '  One of the input arguments is not well initialized')
       !CALL debug('', '    - n <= 0, niter <= 0, nsim <= 0, dxmin <= 0.0 or epsg not in ]0, 1[;')
       IF(ig_nctl<=0)&
            CALL debug(ig_nctl,   '     * n <= 0, current value = ')
       IF(ig_niter<=0)&
            CALL debug(ig_niter,  '     * niter <= 0, current value = ')
       IF(ig_nsimul<=0)&
            CALL debug(ig_nsimul, '     * nsim <= 0, current value = ')
       IF(rg_xmin<=0)&
            CALL debug(rg_xmin,   '     * dxmin <= 0, current value = ')
       IF(rg_epsg<=0)&
            CALL debug(rg_epsg,   '     * epsg not in ]0, 1[, current value = ')
       IF( (iga_imode(1)==0).AND.(ig_ndz< 6*ig_nctl+1) )THEN!DIS mode
          CALL debug('', '    - Running DIS mode: this mode assumes ndz < 6n + 1')
          CALL debug((/ig_ndz, 6*ig_nctl+1/), '     * actual values, ndz, 6n+1 = ')
       ELSE IF( (iga_imode(1)==1).AND.(ig_ndz< 6*ig_nctl+1) )THEN!SIS mode
          CALL debug('', '    - Running SIS mode: this mode assumes ndz < 5n + 1')
          CALL debug((/ig_ndz, 5*ig_nctl+1/), '     * actual values, ndz, 5n+1 = ')
       END IF
       CALL debug('', '    - the contents of iz is not good for a warm restart,')
       CALL debug('', '    - the starting point is almost optimal')
    CASE (3)
       CALL debug('', '  The line search is blocked on tmax')
    CASE (4)
       CALL debug('', '  The maximal number of iterations is reached')
    CASE (5)
       CALL debug('', '  The maximal number of simulation is reached')
    CASE (6)
       CALL debug('', '  Stop on dxmin during the line-search')
    CASE (7)
       CALL debug('', '  Non negative dot product <g,d> or <y,s>')
    CASE DEFAULT
       CALL debug('', '  Unknown output mode')
    END SELECT
    CALL debug('', 'm1qn3_print_stat:: $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$')
  END SUBROUTINE m1qn3_print_stat
END PROGRAM simul
