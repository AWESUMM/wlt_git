!!Special routines for BALAISE assimilation
!!@author Innocent Souopgui
MODULE adjoint_tools
	USE general_constant
  IMPLICIT NONE

  !> \brief interface for MAVAL ADJOINT
  INTERFACE MAXVALADJ
    MODULE PROCEDURE MAXVALADJ_1d!1d
    MODULE PROCEDURE MAXVALADJ_2d !2d
!     MODULE PROCEDURE MAXVALADJ_3d !3d
  END INTERFACE MAXVALADJ
CONTAINS

  !> \brief (pseudo) Adjoint of MAXVAL function
  !! \param[in] rda_A original vector
  !! \param[in,out] rda_Aad adjoint vector
  !! \param[in, out] rd_maxad
  !<
  SUBROUTINE MAXVALADJ_1d(rda_A, rda_Aad, rd_maxad)
    !     Purpose: adjoint de MAXVAL, avec une matrice en parametre
    !     --------
    !     Interface : rda_A : matrice originale
    !                 rda_A : matrice adjointe
    !     ----------
    !     History:
    !     --------
    !      Version    Programmer      Date         Description
    !      -------    ----------      ----         -----------
    !       1.0        Innocent       04/09         Creation
    !*-----------------------------------------------------------------

    REAL(KIND=dp), DIMENSION(:), INTENT(IN)   :: rda_A
    REAL(KIND=dp), DIMENSION(:), INTENT(OUT)  :: rda_Aad
    REAL(KIND=dp)              , INTENT(OUT)  :: rd_maxad
    INTEGER :: il_imax, ib_i

    il_imax = LBOUND(rda_A, 1)

    DO ib_i = LBOUND(rda_A, 1), UBOUND(rda_A, 1)
      IF( rda_A(il_imax) < rda_A(ib_i) ) THEN
        il_imax = ib_i
      END IF
    END DO
    rda_Aad(il_imax) = rda_Aad(il_imax) + rd_maxad
    rd_maxad = 0.0_dp
  END SUBROUTINE MAXVALADJ_1d
  
  SUBROUTINE MAXVALADJ_2d(rda_A, rda_Aad, rd_maxad)
    !     Purpose: adjoint de MAXVAL, avec une matrice en param�tre
    !     --------
    !     Interface : rda_A : matrice originale
    !                 rda_A : matrice adjointe
    !     ----------
    !     History:
    !     --------
    !      Version    Programmer      Date         Description
    !      -------    ----------      ----         -----------
    !       1.0        Innocent       04/09         Creation
    !*-----------------------------------------------------------------
    
    REAL(KIND=dp), DIMENSION(:, :), INTENT(IN)   :: rda_A
    REAL(KIND=dp), DIMENSION(:, :), INTENT(OUT)  :: rda_Aad
    REAL(KIND=dp)                 , INTENT(OUT)  :: rd_maxad
    INTEGER                               :: il_imax, il_jmax, ib_i, ib_j
    
    il_imax = LBOUND(rda_A, 1)
    il_jmax = LBOUND(rda_A, 2)
    
    DO ib_j = LBOUND(rda_A, 2), UBOUND(rda_A, 2)
      DO ib_i = LBOUND(rda_A, 1), UBOUND(rda_A, 1)
        IF( rda_A(il_imax, il_jmax) < rda_A(ib_i, ib_j) ) THEN
          il_imax = ib_i
          il_jmax = ib_j
        END IF
      END DO
    END DO
    rda_Aad(il_imax, il_jmax) = rda_Aad(il_imax, il_jmax) + rd_maxad
    rd_maxad = 0.0_dp
  END SUBROUTINE MAXVALADJ_2d
  
END MODULE adjoint_tools
