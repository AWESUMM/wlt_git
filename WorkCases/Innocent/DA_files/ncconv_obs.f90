MODULE ncconv_obs
	USE conv_obs
	USE nctools
	USE general_tools
	USE debug_tools
IMPLICIT NONE


  !> \brief User defined type for attributes of observations
  !<
  TYPE obs_structure_components
    CHARACTER(LEN=ip_cnl) ::&
      obs_fName  = "obs_fName",&
      ogap_fName = "ogap_fName",&
      aa_title   = "title",&
      aa_history = "history",&
      i_max_nobs = "i_max_nobs",&
      i_ndim = "i_ndim",&
      i_nobs   = "i_nobs",&
      r_dt     = "delta_t" ,&
      l_icoord_allocated = "l_icoord_allocated",&
      l_rcoord_allocated = "l_rcoord_allocated"

    CHARACTER(LEN=ip_cnl) :: ra_data = "data"
    CHARACTER(LEN=ip_cnl) ::&
      ra_obs    = "ra_obs",&
      ra_obsu   = "N/A",&
      ra_obsln  = "observation data",&
      ra_sigma   = "ra_sigma",&
      ra_sigmau  = "N/A",&
      ra_sigmaln = "standard deviation of observation error"

    CHARACTER(LEN=ip_cnl) ::&
      ra_Rm1    = "ra_Rm1",&
      ra_Rm1u   = "N/A",&
      ra_Rm1ln  = "inverse covariance (diag matrix)",&
      ra_obsgap   = "ra_obsgap",&
      ra_obsgapu  = "N/A",&
      ra_obsgapln = "Diference between obs and model output"

    !wavelet related variables
    CHARACTER(LEN=ip_cnl) :: i_obs_level = "Obs_level"
    CHARACTER(LEN=ip_cnl) ::&
      ia_M_vector = "M_vector",&
      ia_M_vectoru = "N/A",&
      ia_M_vectorln = "Size of the wavelet grid at the coarsest scale"

    CHARACTER(LEN=ip_cnl) ::&
      ia_icoord = "icoord",&
      ia_icoordu = "N/A",&
      ia_icoordln = "Integer coordinates, indices in the discretization grid",&
      ra_rcoord   = "rcoord",&
      ra_rcoordu  = "N/A",&
      ra_rcoordln = "real coordinates in the computation domain"

    CHARACTER(LEN=ip_cnl) ::&
      l_rcoord   = "l_rcoord",&
      l_rcoordu  = "N/A",&
      l_rcoordln = "Status of the real coordinates",&
      l_icoord   = "l_icoord",&
      l_icoordu  = "l_icoord",&
      l_icoordln = "Status of the integer coordinates"
      
		CHARACTER(LEN=ip_cnl) ::&
			r_date   = "t",&
			r_dateu  = "s",&
			r_dateln = "time"
		CHARACTER(LEN=ip_cnl) ::&
			i_ts     = "timeStep",&
			i_tsu    = "N/A",&
			i_tsln   = "time step ordinal number"
  END TYPE obs_structure_components

  TYPE obsOutput
		CHARACTER(LEN=ip_snl) :: fileName
		CHARACTER(LEN=ip_snl) :: aa_title
		CHARACTER(LEN=ip_fnl) :: aa_history
    !wavelet related variables
    INTEGER :: i_obs_level = 0!> observation level, this variable changes the interpretation of integer coordinates
    INTEGER, DIMENSION(3) :: ia_M_vector = (/0, 0, 0/)!>wavelet grid at the coarsest scale (level =1)
    
		INTEGER :: ncid      = -1
		INTEGER :: i_nobs    = -1
		INTEGER :: i_ndim    = -1
		REAL(dp):: r_dt      = -1.0d0
    INTEGER :: nb_record = -1 !number of record in the file

		INTEGER :: ra_dataid   = -1
		INTEGER :: ra_sigmaid  = -1
		INTEGER :: ia_icoordid = -1
		INTEGER :: ra_rcoordid = -1
		INTEGER :: l_rcoordid  = -1
		INTEGER :: l_icoordid  = -1
    INTEGER :: i_tsid      = -1
    INTEGER :: r_dateid    = -1
    
    LOGICAL :: isOpened    = .FALSE.
    INTEGER :: i_nextRec   = -1
  END TYPE obsOutput
			
  TYPE(obs_structure_components), PRIVATE, SAVE :: tm_oAtt
CONTAINS

	SUBROUTINE initObsOutput(td_ncfile, ada_fileName, id_nobs, id_ndim, dt)
    type(obsOutput), INTENT(INOUT) :: td_ncfile
    CHARACTER(LEN=ip_snl), INTENT(IN) :: ada_fileName
    INTEGER, INTENT(IN), OPTIONAL :: id_nobs, id_ndim
    REAL(dp), INTENT(IN), OPTIONAL:: dt

    td_ncfile%fileName = ada_fileName
    IF( PRESENT(id_nobs) )THEN
			td_ncfile%i_nobs	=	id_nobs
		ELSE
			td_ncfile%i_nobs	=	-1
		END IF
    IF( PRESENT(id_ndim) )THEN
			td_ncfile%i_ndim	=	id_ndim
		ELSE
			td_ncfile%i_ndim	=	-1
		END IF
    IF( PRESENT(dt) )THEN
			td_ncfile%r_dt	=	dt
		ELSE
			td_ncfile%r_dt	=	-1.0d0
		END IF
	END SUBROUTINE initObsOutput

	SUBROUTINE printObsOutput(td_ncfile)
    type(obsOutput), INTENT(IN) :: td_ncfile
    PRINT*, 'fileName  = ', TRIM(td_ncfile%fileName)
    PRINT*, 'i_nobs    = ', td_ncfile%i_nobs
    PRINT*, 'nb_record = ', td_ncfile%nb_record
    PRINT*, 'r_dt      = ', td_ncfile%r_dt
	END SUBROUTINE printObsOutput

	SUBROUTINE ncObsClose(td_ncfile)
    type(obsOutput), INTENT(INOUT) :: td_ncfile

    CALL chkerr( nf90_close( td_ncfile%ncid) )
    td_ncfile%ncid = -1
    td_ncfile%i_nextRec = -1
    td_ncfile%isOpened = .FALSE.
	END SUBROUTINE ncObsClose
	
	SUBROUTINE ncObsOpen(td_ncfile)
		TYPE(obsOutput), INTENT(INOUT) :: td_ncfile
		INTEGER :: il_nDimensions, il_nVariables, il_nAttributes, il_unlimitedDimId, il_formatNum,&
			il_nobsid, il_ndimid
		character(len = nf90_max_name) :: lc_name
		
		CALL debug( TRIM(td_ncfile%fileName), 'In ncObsOpen, opening -- ' )

		CALL chkerr(nf90_open(TRIM(td_ncfile%filename),NF90_NOCLOBBER,td_ncfile%ncid))

		CALL chkerr( nf90_inq_varid( td_ncfile%ncid, tm_oAtt%ra_data  , td_ncfile%ra_dataid   ) )
		CALL chkerr( nf90_inq_varid( td_ncfile%ncid, tm_oAtt%ra_sigma , td_ncfile%ra_sigmaid  ) )
		CALL chkerr( nf90_inq_varid( td_ncfile%ncid, tm_oAtt%ia_icoord, td_ncfile%ia_icoordid ) )
		CALL chkerr( nf90_inq_varid( td_ncfile%ncid, tm_oAtt%ra_rcoord, td_ncfile%ra_rcoordid ) )
		CALL chkerr( nf90_inq_varid( td_ncfile%ncid, tm_oAtt%l_icoord , td_ncfile%l_icoordid  ) )
		CALL chkerr( nf90_inq_varid( td_ncfile%ncid, tm_oAtt%l_rcoord , td_ncfile%l_rcoordid  ) )
		CALL chkerr( nf90_inq_varid( td_ncfile%ncid, tm_oAtt%r_date   , td_ncfile%r_dateid    ) )
		CALL chkerr( nf90_inq_varid( td_ncfile%ncid, tm_oAtt%i_ts     , td_ncfile%i_tsid      ) )

		CALL chkerr(nf90_inquire(td_ncfile%ncid, il_nDimensions, il_nVariables, il_nAttributes, &
				il_unlimitedDimId, il_formatNum))

		CALL chkerr(nf90_inquire_dimension(td_ncfile%ncid, il_unlimitedDimId, name = lc_name, len = td_ncfile%nb_record))
		CALL debug( td_ncfile%nb_record, 'In ncObsOpen, td_ncfile%nb_record = ' )
		CALL chkerr( nf90_inq_dimid( td_ncfile%ncid, tm_oAtt%i_nobs, il_nobsid ) )
		CALL chkerr( nf90_inq_dimid( td_ncfile%ncid, tm_oAtt%i_ndim, il_ndimid ) )
		CALL chkerr( nf90_inquire_dimension( td_ncfile%ncid, il_nobsid, lc_name, td_ncfile%i_nobs ) )
		CALL chkerr( nf90_inquire_dimension( td_ncfile%ncid, il_ndimid, lc_name, td_ncfile%i_ndim ) )
		!PRINT*, "getting attributres"
		CALL readatt(td_ncfile)
		td_ncfile%isOpened = .TRUE.
    td_ncfile%i_nextRec = 1
  CALL debug('... done')
	END SUBROUTINE ncObsOpen

	SUBROUTINE readatt( td_ncfile )
		!read des attributs (param�tres) de la trajectoire
		type(ObsOutput), INTENT(INOUT) :: td_ncfile
		PRINT*, "In readatt, reading attributes"
		CALL chkerr( nf90_get_att( td_ncfile%ncid, NF90_GLOBAL, tm_oAtt%r_dt       , td_ncfile%r_dt       ) )
		CALL chkerr( nf90_get_att( td_ncfile%ncid, NF90_GLOBAL, tm_oAtt%aa_title   , td_ncfile%aa_title   ) )
		CALL chkerr( nf90_get_att( td_ncfile%ncid, NF90_GLOBAL, tm_oAtt%aa_history , td_ncfile%aa_history ) )
		CALL chkerr( nf90_get_att( td_ncfile%ncid, NF90_GLOBAL, tm_oAtt%i_obs_level, td_ncfile%i_obs_level) )
		CALL chkerr( nf90_get_att( td_ncfile%ncid, NF90_GLOBAL, tm_oAtt%ia_M_vector, td_ncfile%ia_M_vector) )
		PRINT*, "End of attributes reading"
		!End of saving
	END SUBROUTINE readatt

	SUBROUTINE ncObsCreate(td_ncfile)
		type(ObsOutput), INTENT(INOUT) :: td_ncfile
		INTEGER, DIMENSION(3) :: ila_dims3D
		INTEGER, DIMENSION(2) :: ila_dims2D
		INTEGER :: il_nobs, il_ndim, il_date
		
		CALL debug( TRIM(td_ncfile%fileName), 'In ncObsCreate, creating -- ' )
		CALL chkerr(nf90_create(TRIM(td_ncfile%fileName),NF90_CLOBBER	,	td_ncfile%ncid))
		CALL chkerr(nf90_def_dim(td_ncfile%ncid, tm_oAtt%i_nobs, td_ncfile%i_nobs, il_nobs))
		CALL chkerr(nf90_def_dim(td_ncfile%ncid, tm_oAtt%i_ndim, td_ncfile%i_ndim, il_ndim))
		CALL chkerr(nf90_def_dim(td_ncfile%ncid, tm_oAtt%r_date, NF90_UNLIMITED  , il_date))

    CALL chkerr(nf90_def_var(td_ncfile%ncid, tm_oAtt%r_date  , NF90_DOUBLE, il_date, td_ncfile%r_dateid))
    CALL chkerr(nf90_def_var(td_ncfile%ncid, tm_oAtt%i_ts    , NF90_INT   , il_date, td_ncfile%i_tsid  ))
		CALL chkerr(nf90_def_var(td_ncfile%ncid, tm_oAtt%l_icoord, NF90_INT   , il_date, td_ncfile%l_icoordid))
		CALL chkerr(nf90_def_var(td_ncfile%ncid, tm_oAtt%l_rcoord, NF90_INT   , il_date, td_ncfile%l_rcoordid))
		ila_dims2D(1)=il_nobs
		ila_dims2D(2)=il_date
		CALL chkerr(nf90_def_var(td_ncfile%ncid, tm_oAtt%ra_data , NF90_DOUBLE, ila_dims2D, td_ncfile%ra_dataid ))
		CALL chkerr(nf90_def_var(td_ncfile%ncid, tm_oAtt%ra_sigma, NF90_DOUBLE, ila_dims2D, td_ncfile%ra_sigmaid))
		ila_dims3D(1)=il_ndim
		ila_dims3D(2)=il_nobs
		ila_dims3D(3)=il_date
		CALL chkerr(nf90_def_var(td_ncfile%ncid, tm_oAtt%ia_icoord, NF90_DOUBLE, ila_dims3D, td_ncfile%ia_icoordid))
		CALL chkerr(nf90_def_var(td_ncfile%ncid, tm_oAtt%ra_rcoord, NF90_DOUBLE, ila_dims3D, td_ncfile%ra_rcoordid))
		CALL saveatt(td_ncfile)
		CALL chkerr(nf90_enddef(td_ncfile%ncid))
		td_ncfile%isOpened = .TRUE.
    td_ncfile%i_nextRec = 1
		PRINT*,'... ncObsCreate -> done'
	END SUBROUTINE ncObsCreate

	SUBROUTINE saveatt(td_ncfile)
		!sauvegarde des attributs (param�tres) de la trajectoire
		type(ObsOutput), INTENT(INOUT) :: td_ncfile
		CALL chkerr( nf90_put_att( td_ncfile%ncid, NF90_GLOBAL, tm_oAtt%r_dt       , td_ncfile%r_dt       ) )
		CALL chkerr( nf90_put_att( td_ncfile%ncid, NF90_GLOBAL, tm_oAtt%aa_title   , td_ncfile%aa_title   ) )
		CALL chkerr( nf90_put_att( td_ncfile%ncid, NF90_GLOBAL, tm_oAtt%aa_history , td_ncfile%aa_history ) )
		CALL chkerr( nf90_put_att( td_ncfile%ncid, NF90_GLOBAL, tm_oAtt%i_obs_level, td_ncfile%i_obs_level) )
		CALL chkerr( nf90_put_att( td_ncfile%ncid, NF90_GLOBAL, tm_oAtt%ia_M_vector, td_ncfile%ia_M_vector) )
		PRINT*, "End of attributes recording"
		!End of saving
	END SUBROUTINE saveatt

  !> \brief write observation data to netcdf file
  !! \param[in,out] td_ncfile file to be written
  !! \param[in,out] td_obs obs structure that contains the data to write
  !! \param[in] rec (optional) gives the ordinal number of the record to write
  !! \param[in] ogap (optional) this argument changes completely the behaviour of the subroutine. It value is not important. When it is absent, the file is supposed to contain observation data.
  !! When it is present, the file is suppose to contain the obsgap data, difference between observation and the state. When reading obsgap, only the obsgap field and the RM1 field are read
  !! \note the fields obs abd obsgap share the same place and name in the netcdf file. Similarly, the field Rm1 and sigma share the same place and name in the netcdf file.
  !<
	SUBROUTINE ncObsWrite(td_ncfile, td_obs, rec, ogap)
		type(ObsOutput), INTENT(INOUT) :: td_ncfile
		type(obs_structure), INTENT(IN) :: td_obs
		INTEGER, INTENT(IN), OPTIONAL  :: rec
    INTEGER, INTENT(IN), OPTIONAL  :: ogap

		INTEGER, DIMENSION(3) :: ila_count3D, ila_start3D
		INTEGER, DIMENSION(2) :: ila_count2D, ila_start2D
    INTEGER, DIMENSION(1) :: ila_start1D
    INTEGER :: il_rec, il_icoord, il_rcoord

		IF( PRESENT(rec) )THEN
			il_rec = rec
		ELSE
			il_rec = td_ncfile%i_nextRec
		END IF

		CALL debug(il_rec, 'In ncObsWrite, writing '//TRIM(td_ncfile%filename)//' - record' )
		ila_start1D = (/il_rec/)
		ila_count2D=(/td_ncfile%i_nobs, 1 /)
		ila_start2D=(/ 1, il_rec /)
		ila_count3D= (/td_ncfile%i_ndim, td_ncfile%i_nobs, 1/)
		ila_start3D=(/ 1, 1, il_rec /)
		
		il_icoord = INT(td_obs%l_icoord)
		il_rcoord = INT(td_obs%l_rcoord)
		CALL chkerr(nf90_put_var(td_ncfile%ncid, td_ncfile%l_icoordid, il_icoord    , start = ila_start1D))
		CALL chkerr(nf90_put_var(td_ncfile%ncid, td_ncfile%l_rcoordid, il_rcoord    , start = ila_start1D))
		CALL chkerr(nf90_put_var(td_ncfile%ncid, td_ncfile%r_dateid  , td_obs%r_date, start = ila_start1D))
		CALL chkerr(nf90_put_var(td_ncfile%ncid, td_ncfile%i_tsid    , td_obs%i_ts  , start = ila_start1D))
		IF(td_obs%l_icoord)&
			CALL chkerr(nf90_put_var(td_ncfile%ncid, td_ncfile%ia_icoordid, td_obs%ia_icoord, start = ila_start3D, COUNT = ila_count3D))
		IF(td_obs%l_rcoord)&
			CALL chkerr(nf90_put_var(td_ncfile%ncid, td_ncfile%ra_rcoordid, td_obs%ra_rcoord, start = ila_start3D, COUNT = ila_count3D))
      
		IF( PRESENT(ogap) ) THEN
      CALL chkerr(nf90_put_var(td_ncfile%ncid, td_ncfile%ra_dataid , td_obs%ra_obsgap  , start = ila_start2D, COUNT = ila_count2D))
      CALL chkerr(nf90_put_var(td_ncfile%ncid, td_ncfile%ra_sigmaid, td_obs%ra_Rm1, start = ila_start2D, COUNT = ila_count2D))
		ELSE
      CALL chkerr(nf90_put_var(td_ncfile%ncid, td_ncfile%ra_dataid , td_obs%ra_obs  , start = ila_start2D, COUNT = ila_count2D))
      CALL chkerr(nf90_put_var(td_ncfile%ncid, td_ncfile%ra_sigmaid, td_obs%ra_sigma, start = ila_start2D, COUNT = ila_count2D))
    END IF
    td_ncfile%i_nextRec = il_rec + 1
    CALL debug('... done')
	END SUBROUTINE ncObsWrite

  !> \brief read obs from netcdf file
  !! \param[in,out] td_ncfile file to be read from
  !! \param[in,out] td_obs obs structure to contain the read data
  !! \param[in] rec (optional) gives the ordinal number of the record to read
  !! \param[in] ogap (optional) this argument changes completely the behaviour of the subroutine. It value is not important. When it is absent, the file is supposed to contain observation data and the subroutine allocates space if needed.
  !! When it is present, the file is suppose to contain the obsgap data, difference between observation and the state. When reading obsgap, only the obsgap field and the RM1 field are read. The space is suppose to be allocated.
  !! \note the fields obs abd obsgap share the same place and name in the netcdf file. Similarly, the field Rm1 and sigma share the same place and name in the netcdf file.
  !<
	SUBROUTINE ncObsRead(td_ncfile, td_obs, rec, ogap)
		type(ObsOutput), INTENT(INOUT) :: td_ncfile
		type(obs_structure), INTENT(INOUT) :: td_obs
		INTEGER, INTENT(IN), OPTIONAL  :: rec
    INTEGER, INTENT(IN), OPTIONAL  :: ogap

		INTEGER, DIMENSION(3) :: ila_count3D, ila_start3D
		INTEGER, DIMENSION(2) :: ila_count2D, ila_start2D
    INTEGER, DIMENSION(1) :: ila_start1D, ila_count1D
    REAL(KIND=sp), DIMENSION(1)	:: rl_date
    INTEGER :: il_rec, il_icoord, il_rcoord
    LOGICAL :: ll_icoord, ll_rcoord

		IF( PRESENT(rec) )THEN
			il_rec = rec
		ELSE
			il_rec = td_ncfile%i_nextRec
		END IF
		CALL debug(il_rec, 'In ncObsRead, Reading '//TRIM(td_ncfile%filename)//' - record' )
		ila_start1D = (/il_rec/)
		ila_count1D = (/1/)
		ila_count2D = (/td_ncfile%i_nobs, 1/)
		ila_start2D = (/ 1, il_rec /)
		ila_count3D = (/td_ncfile%i_ndim, td_ncfile%i_nobs, 1/)
		ila_start3D = (/ 1, 1, il_rec /)

		CALL chkerr( nf90_get_var( td_ncfile%ncid, td_ncfile%l_icoordid, il_icoord, start = ila_start1D ) )
		CALL chkerr( nf90_get_var( td_ncfile%ncid, td_ncfile%l_rcoordid, il_rcoord, start = ila_start1D ) )
		ll_icoord = int2l(il_icoord)
		ll_rcoord = int2l(il_rcoord)
		CALL set_obsSize( td_obs, td_ncfile%i_nobs, id_coord_ndim=td_ncfile%i_ndim, ld_icoord=ll_icoord, ld_rcoord=ll_rcoord )
		IF(td_obs%l_icoord)&
			CALL chkerr(nf90_get_var(td_ncfile%ncid, td_ncfile%ia_icoordid, td_obs%ia_icoord, start = ila_start3D, COUNT = ila_count3D))
		IF(td_obs%l_rcoord)&
			CALL chkerr(nf90_get_var(td_ncfile%ncid, td_ncfile%ra_rcoordid, td_obs%ra_rcoord, start = ila_start3D, COUNT = ila_count3D))
		CALL chkerr( nf90_get_var( td_ncfile%ncid, td_ncfile%r_dateid  , td_obs%r_date, start = ila_start1D ) )
		CALL chkerr (nf90_get_var( td_ncfile%ncid, td_ncfile%i_tsid    , td_obs%i_ts  , start = ila_start1D ) )

    IF( PRESENT(ogap) ) THEN
      CALL chkerr(nf90_get_var(td_ncfile%ncid, td_ncfile%ra_dataid , td_obs%ra_obsgap  , start = ila_start2D, COUNT = ila_count2D))
      CALL chkerr(nf90_get_var(td_ncfile%ncid, td_ncfile%ra_sigmaid, td_obs%ra_Rm1, start = ila_start2D, COUNT = ila_count2D))
    ELSE
      CALL chkerr(nf90_get_var(td_ncfile%ncid, td_ncfile%ra_dataid , td_obs%ra_obs  , start = ila_start2D, COUNT = ila_count2D))
      CALL chkerr(nf90_get_var(td_ncfile%ncid, td_ncfile%ra_sigmaid, td_obs%ra_sigma, start = ila_start2D, COUNT = ila_count2D))
		END IF
    td_ncfile%i_nextRec = il_rec + 1
    CALL debug('... done')
	END SUBROUTINE ncObsRead

  SUBROUTINE readObs_date(td_ncfile, rda_date)
    type(ObsOutput),INTENT(IN)			:: td_ncfile
    REAL(KIND=dp), DIMENSION(:), INTENT(OUT)		:: rda_date

    CALL debug(TRIM(td_ncfile%filename), 'In readObs_date : reading date from ')
    CALL chkerr(nf90_get_var(td_ncfile%ncid, td_ncfile%r_dateid, rda_date))
    CALL debug('... done')
  END SUBROUTINE readObs_date

  SUBROUTINE readObs_timeStep(td_ncfile, id_recNum, id_ts)
    type(ObsOutput),INTENT(IN):: td_ncfile
    INTEGER, INTENT(IN)					:: id_recNum
    INTEGER, INTENT(OUT)				:: id_ts

    CALL debug(TRIM(td_ncfile%filename), 'In readObs_timeStep : reading timeStep from ')
    CALL chkerr(nf90_get_var(td_ncfile%ncid, td_ncfile%i_tsid, id_ts, start=(/id_recNum/) ))
    CALL debug('... done')
  END SUBROUTINE readObs_timeStep

  SUBROUTINE readObs_AllTimeStep(td_ncfile, ida_ts)
    type(ObsOutput),INTENT(IN)		:: td_ncfile
    INTEGER, DIMENSION(:), INTENT(OUT):: ida_ts

    CALL debug(TRIM(td_ncfile%filename), 'In readObs_AllTimeStep : reading timeStep from ')
    CALL chkerr(nf90_get_var(td_ncfile%ncid, td_ncfile%i_tsid, ida_ts))
    CALL debug('... done')
  END SUBROUTINE readObs_AllTimeStep
	
END MODULE ncconv_obs