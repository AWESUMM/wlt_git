!>file com_tools.f90
!!\brief global communication tools
!<
MODULE com_tools
  USE general_constant
  USE general_tools
  USE debug_tools
  USE conv_obs
  USE ncconv_obs
IMPLICIT NONE

  INTEGER, PARAMETER :: INT_COORD  = 301,& !> integer coordinates
                        REAL_COORD = 310,& !> real coordinates
                        BOTH_COORD = 311   !> both integer and real coordinates
                        
  !enumeration for the type of files
  !todo this should be turn into enumeration type
  INTEGER, PARAMETER :: &
      CTL_DATA  = 711,&
      BCTL_DATA = 712,&!>background ctl
      ACTL_DATA = 713,&!>analysed ctl
      DCTL_DATA = 714,&!>Delta ctl (increment to add to the background to get the value of the ctl)
      TCTL_DATA = 715,&!>true ctl (increment to add to the background to get the value of the ctl)
      OBS_DATA  = 731,&
      AOBS_DATA = 732,&
      TOBS_DATA = 733,&!>true observation
      DMT_DATA  = 741,&
      ADMT_DATA = 742,&!>analysed direct model trajectory
      BDMT_DATA = 742,&!>background direct model trajectory
      TDMT_DATA = 743,&!>true direct model trajectory
      OGAP_DATA = 750,&   
      EP_DATA   = 760,&
      COST_DATA = 770,&
      GRAD_DATA = 780,&
      CTL_BOUND_DATA = 718,&
      CTL_ALL_DATA   = 719 !>All data about the control vector including ctl, background and bounds
  !enumeration for the status or usage of files
  !todo this should be turn into enumeration type
  INTEGER, PARAMETER :: &
      INPUT_FILE  = 381,& !> input file
      OUTPUT_FILE = 382,& !> output file
      RTIME_FILE  = 383,& !> run time file
      BASE_FILE   = 384   !> base file name (no path)

  !enumeration for the identification of the program or routine that saves exchange parameters
  !todo this should be turn into enumeration type
  INTEGER, PARAMETER ::&
       SIMUL_PROGRAM  = 0,&!>value corresponding to the simulator
       SOLVER_PROGRAM = 1111  !>value corresponding to the solver

  INTEGER, PARAMETER ::&
       ip_ctl_out  = 43,&!>id of the output file for the gradientcontrol vector
       ip_grad_out = 47  !>id of the output file for the gradient

  !> \brief action value for make obs simulation
  !<
  CHARACTER(LEN=*), PARAMETER :: MAKE_OBS= "OBS"
  CHARACTER(LEN=*), PARAMETER :: MAKE_AOBS= "AOBS"!analysed observation
  CHARACTER(LEN=*), PARAMETER :: MAKE_TOBS= "TOBS"!true observation
  !> \brief action value for make obs simulation, given coordinates
  !<
  CHARACTER(LEN=*), PARAMETER :: MAKE_OBS_FROM_COORD= "OBS_FROM_COORD"
  !> \brief action value for data assimilation
  !<
  CHARACTER(LEN=*), PARAMETER :: RUN_ASSIM   = "ASSIM"
  !> \brief Action value for direct simulation
  !<
  CHARACTER(LEN=*), PARAMETER :: RUN_DIRECT  = "DIRECT"
  !> \brief Action value for adjoint simulation
  !! The meaning of this variable depends on the context:
  !!   - in the solver, it means solving only the adjoint model,
  !!     the obs gap is supposed to be saved from previous computation
  !!   - in the minimization driver, it means computing the cost function (direct model) followed by the adjoint
  !<
  CHARACTER(LEN=*), PARAMETER :: RUN_ADJOINT = "ADJOINT"
  !> \brief Action value for the computation of the cost function (this is direct followed by cost)
  !! used only by the solver
  !<
  CHARACTER(LEN=*), PARAMETER :: RUN_COST = "COST"
  !> \brief action value for the computation of the gradient of the cost function (this is adjoint followed by g)
  !! used only by the solver
  !<
  CHARACTER(LEN=*), PARAMETER :: RUN_GRADIENT = "GRAD"
  !> \brief Action value for the computation of the cost function and the gradient simultaneously
  !!  used only by the solver that support this feature
  !<
  CHARACTER(LEN=*), PARAMETER :: RUN_COSTGRAD = "COST_GRAD"
  !> \brief Action value to tell the solver that m1qn3 is moving to the next iteratio
  !! used by the solver to save some data (restart files ...)
  !<
  CHARACTER(LEN=*), PARAMETER :: RUN_NOTHING = "NOTHING"
  !> \brief action value for the generation of a control vector
  !! used only by the solver
  !<
  CHARACTER(LEN=*), PARAMETER :: MAKE_CTL = "CTL"
  !> \brief action value for the generation of a background control vector
  !! used only by the solver
  !<
  CHARACTER(LEN=*), PARAMETER :: MAKE_BG = "BG"
  !> \brief action value for the generation of a background control vector from pseu inversion of observation
  !!
  !<
  CHARACTER(LEN=*), PARAMETER :: OBS_TO_BG = "OBS2BG"
  !> \brief action value for the generation of analysed model trajectory
  !! used only by the solver
  !<
  CHARACTER(LEN=*), PARAMETER :: MAKE_DMT = "DMT"!>direct model trajectory
  CHARACTER(LEN=*), PARAMETER :: MAKE_ADMT = "ADMT"!>analysed model trajectory
  CHARACTER(LEN=*), PARAMETER :: MAKE_TDMT = "TDMT"!>true model trajectory

  
  !> \brief Maximum size for the action variable
  !<
  INTEGER, PARAMETER :: IP_ACTION_LEN = 15

  !> \brief User defined type for exchanging parameters between programs
  !! Defines parameters to be exchanged between the solver and the minimization driver
  !<
  TYPE exchange_param
     CHARACTER(LEN=ip_snl) ::& !, PRIVATE (removed for compatibility with old compilers)
        ep_fName   = "ep.nc"   ,&
        cost_fName = "cost.nc" ,&
        grad_fName = "grad.nc" ,&
        ctl_fName  = "ctl.nc"  ,&
        bctl_fName = "b_ctl.nc",&
        obs_fName  = "obs.nc"  ,&
        dmt_fName  = "dmt.nc"  ,&
        ogap_fName = "obsgap.nc",&
        ctl_all_fName="ctl_all.nc",&
        ctl_bound_fName="ctl_bound.nc",&
        rt_dir     = "rt",&!>runtime directory, temporary runtime data are saved in this directory
				input_dir  = "." ,& !>input directory, input data are read from this directory
				output_dir = "."   !>output directory, output data are saved in this directory
				
     CHARACTER(LEN=IP_ACTION_LEN) :: &
        aa_solver_action,&! solver action
        aa_simul_action! simul action
     CHARACTER(LEN=ip_snl) :: aa_solver_path   = "nothing" !>solver path for extenal solver
     CHARACTER(LEN=ip_snl) :: restart_fileName = "rt_restart" !> restart file name for the solver
     CHARACTER(LEN=ip_snl) :: restart_candidate= "rt_restart_cand" !> candidate file for the restart
     CHARACTER(LEN=ip_snl) :: direct_fileName  = "rt_direct" !> direct solution file, used for the adjoint
     !solver control variables
     LOGICAL :: l_simul_diverged !Says if the simulation has diverged or not, to be used with solvers that support this feature
     LOGICAL :: l_restart_from_previous!restart th solver from the result of the previous iteration
     LOGICAL :: l_first_simul ! says if this is the first call to simulator
     LOGICAL :: l_run_from_ctl! says ifthe solver should run from ctl
     !some model parameters
     REAL(cp) :: r_mu
     REAL(cp) :: r_sigma
     REAL(cp) :: r_v0
     REAL(cp) :: r_omega
     
     !regularization weighting parameters
     LOGICAl  :: l_useGD !use Generalized diffusion projection?
     REAL(cp) :: r_wGD   !weighting parameter for the GD term
     REAL(cp) :: r_wb    !background weighting parameter
     REAL(cp) :: r_wGrad !weighting parameter for gradient regularization

     !informations on control parameters
     LOGICAl :: l_amplitude !>
     LOGICAl :: l_location  !>
     LOGICAl :: l_sigma     !>

     !observation parameters
     INTEGER  :: i_obs_level !> observation level
     INTEGER  :: i_nobs_x !> obs count in the space direction
     INTEGER  :: i_nobs_t !> obs count in the time direction
     REAL(cp) :: r_sigmaR
     
     !CS parameters
     REAL(cp) :: r_max_coef !> max value for randomly generated coefficients
     REAL(cp) :: r_nz_ratio !> ratio of nonzero coefficients for randomly andomly generated control vector
     REAL(cp) :: r_mes_fact !> multiplicative factor between the number of measurements and the nomber of nonzero (nb_mes = rl_mes_fact*nb_nz)
     REAL(cp) :: r_cs_reg   !> regularization parameter

     !control parameters
     INTEGER  :: i_nctl=0 !> size of the control vector!, PRIVATE
     REAL(cp) :: r_cost    !> value of the cost function at ctl
     INTEGER  :: i_ctl_lev !>level of ctl when using wavelet model
     REAL(cp), DIMENSION(:), POINTER :: ra_ctl       => NULL()!> control vector
     REAL(cp), DIMENSION(:), POINTER :: ra_grad      => NULL()!> grad of the cost function at the control vector
     REAL(cp), DIMENSION(:), POINTER :: ra_b_ctl     => NULL()!> background control vector
     REAL(cp), DIMENSION(:), POINTER :: ra_sigma_ctl => NULL()!> standard deviation of errors in the b_ctl
     REAL(cp), DIMENSION(:), POINTER :: ra_Bm1       => NULL()!> covariance of errors in the b_ctl

     !checking parameters
     REAL(cp) :: r_nothing! use to avoid non used warning in some generic subroutines
     REAL(cp), DIMENSION(:), POINTER :: &
          ra_ctl_lb => NULL(),& !>lower bound of acceptable values for the control variable
          ra_ctl_ub => NULL()   !>upper bound of acceptable values for the control variable
     LOGICAl , DIMENSION(:), POINTER :: &
          la_check_ctl_lb => NULL(),& !>Check lower bound of acceptable values for the control variable
          la_check_ctl_ub => NULL()   !>Check upper bound of acceptable values for the control variable
  END TYPE exchange_param


  TYPE exchange_param_components
     CHARACTER(LEN=ip_cnl) ::& !, PRIVATE (removed for compatibility with old compilers)
				aa_title   = "aa_title"   ,&
				aa_history = "aa_history" ,&
        ep_fName   = "ep_fName"   ,&
        cost_fName = "cost_fName" ,&
        grad_fName = "grad_fName" ,&
        ctl_fName  = "ctl_fName"  ,&
        bctl_fName = "bctl_fName" ,&
        obs_fName  = "obs_fName"  ,&
        dmt_fName  = "dmt_fName"  ,&
        ogap_fName = "ogap_fName" ,&
        ctl_all_fName="ctl_all_fName",&
        ctl_bound_fName="ctl_bound_fName",&
        rt_dir     = "runtime_dir",&!>runtime directory, temporary runtime data are saved in this directory
				input_dir  = "input_dir" ,& !>input directory, input data are read from this directory
				output_dir = "output_dir" !>output directory, output data are saved in this directory

     CHARACTER(LEN=ip_cnl) :: &
        aa_solver_action = "aa_solver_action",&! solver action
        aa_simul_action  = "aa_simul_action",&! simul action
        aa_solver_path   = "aa_solver_path",& !>solver path for extenal solver
        restart_fileName = "restart_fileName",& !> restart file name for the solver
        restart_candidate= "restart_candidate",& !> candidate file for the restart
        direct_fileName  = "direct_fileName" !> direct solution file, used for the adjoint
     !solver control variables
     CHARACTER(LEN=ip_cnl) ::&
				l_simul_diverged = "l_simul_diverged",&
				l_restart_from_previous = "l_restart_from_previous",&
				l_first_simul = "l_first_simul",&
				l_run_from_ctl = "l_run_from_ctl"
     !some model parameters
     CHARACTER(LEN=ip_cnl) ::&
				r_mu    = "r_mu",&
				r_sigma = "r_sigma",&
				r_v0    = "r_v0",&
				r_omega = "r_omega"

     !regularization weighting parameters
     CHARACTER(LEN=ip_cnl)  ::&
				l_useGD = "l_useGD",&
				r_wGD   = "r_wGD",&
				r_wb    = "r_wb",&
				r_wGrad = "r_wGrad"

     !informations on control parameters
     CHARACTER(LEN=ip_cnl) ::&
				l_amplitude = "l_amplitude",&
				l_location = "l_location",&
				l_sigma = "l_sigma"

     !observation parameters
     CHARACTER(LEN=ip_cnl) ::&
				i_obs_level = "i_obs_level",&
				i_nobs_x = "i_nobs_x",&
				i_nobs_t = "i_nobs_t",&
				r_sigmaR = "r_sigmaR"

     !CS parameters
     CHARACTER(LEN=ip_cnl) ::&
				r_max_coef = "r_max_coef",&
				r_nz_ratio = "r_nz_ratio",&
				r_mes_fact = "r_mes_fact",&
				r_cs_reg = "r_cs_reg"

     !control parameters
     CHARACTER(LEN=ip_cnl) ::&
				i_nctl = "i_nctl",&
				r_cost = "r_cost",&
				i_ctl_lev = "i_ctl_lev"
     CHARACTER(LEN=ip_cnl) ::&
				ra_ctl     = "ra_ctl",&
				ra_ctlu    = "",&
				ra_ctlln   = "Control vector",&
				ra_dctl     = "ra_dctl",&
				ra_dctlu    = "",&
				ra_dctlln   = "Increment of the control vector",&
				ra_grad    = "ra_grad",&
				ra_gradu   = "N/A",&
				ra_gradln  = "Gradient of the cost function at the control vector",&
				ra_b_ctl   = "ra_b_ctl",&
				ra_b_ctlu  = "",&
				ra_b_ctlln = "Backgroung estimation of the control vector",&
				ra_sigma_ctl   = "ra_sigma_ctl",&
				ra_sigma_ctlu  = "N/A",&
				ra_sigma_ctlln = "Standard deviation of the error in the control vector",&
				ra_Bm1   = "ra_Bm1",&
				ra_Bm1u  = "N/A",&
				ra_Bm1ln = "Inverse standard Deviation of the error in the control vector"

     !checking parameters
     CHARACTER(LEN=ip_cnl) ::r_nothing = "r_nothing"
     CHARACTER(LEN=ip_cnl) ::&
				ra_ctl_lb   = "ra_ctl_lb",&
				ra_ctl_lbu  = "N/A",&
				ra_ctl_lbln = "Lower bound of the control vector",&
				ra_ctl_ub   = "ra_ctl_ub",&
				ra_ctl_ubu  = "N/A",&
				ra_ctl_ubln = "Upper bound of the control vector",&
				la_check_ctl_lb   = "la_check_ctl_lb",&
				la_check_ctl_lbu  = "N/A",&
				la_check_ctl_lbln = "Checking status of the lower bound of the control vector",&
				la_check_ctl_ub   = "la_check_ctl_ub",&
				la_check_ctl_ubu  = "N/A",&
				la_check_ctl_ubln = "Checking status of the upper bound of the control vector"
  END TYPE exchange_param_components

  TYPE exchange_param_output
		CHARACTER(LEN=ip_snl) :: fileName, aa_title
		CHARACTER(LEN=ip_fnl) :: aa_history
     INTEGER :: ncid = -1

     !control parameters
     INTEGER :: i_nctl=0 !> size of the control vector
     INTEGER :: r_costid = -1    !> value of the cost function at ctl
     INTEGER :: l_simul_divergedid = -1
     !INTEGER :: i_ctl_lev !>level of ctl when using wavelet model
     INTEGER ::&
				ra_ctlid = -1,&
				ra_dctlid = -1,&
				ra_gradid = -1,&
				ra_b_ctlid = -1,&
				ra_sigma_ctlid = -1,&
				ra_Bm1id = -1

     !checking parameters
     !REAL(cp) :: r_nothing! use to avoid non used warning in some generic subroutines
     INTEGER :: &
				ra_ctl_lbid = -1,&
				ra_ctl_ubid = -1,&
				la_check_ctl_lbid = -1,&
				la_check_ctl_ubid = -1

    LOGICAL :: isOpened    = .FALSE.
  END TYPE exchange_param_output

	TYPE(exchange_param_components), PRIVATE :: tm_epAtt
	PRIVATE save_ep_att, read_ep_att
CONTAINS

  !
  !********************** Exchange parameter routines ************
  !

	SUBROUTINE initEPOutput(td_ncfile, ada_fileName, id_nctl)
    type(exchange_param_output), INTENT(INOUT)    :: td_ncfile
    CHARACTER(LEN=ip_snl), INTENT(IN) :: ada_fileName
    INTEGER, INTENT(IN), OPTIONAL     :: id_nctl

    td_ncfile%fileName = ada_fileName
    IF( PRESENT(id_nctl) )THEN
			td_ncfile%i_nctl	=	id_nctl
		ELSE
			td_ncfile%i_nctl	=	-1
		END IF
	END SUBROUTINE initEPOutput

	SUBROUTINE ncEPclose(td_ncfile)
    type(exchange_param_output), INTENT(INOUT) :: td_ncfile

    CALL chkerr( nf90_close( td_ncfile%ncid) )
    td_ncfile%ncid = -1
    td_ncfile%isOpened = .FALSE.
	END SUBROUTINE ncEPclose

	SUBROUTINE ncEPOpen(td_ncfile, td_ep)
		TYPE(exchange_param_output), INTENT(INOUT) :: td_ncfile
    TYPE(exchange_param), INTENT(INOUT), OPTIONAL :: td_ep
		INTEGER :: il_nDimensions, il_nVariables, il_nAttributes, il_unlimitedDimId, il_formatNum,&
			il_nctlid, il_nothing
		character(len = nf90_max_name) :: lc_name
		CALL debug(TRIM(td_ncfile%filename), 'In ncEPOpen, opening ')

		CALL chkerr(nf90_open(TRIM(td_ncfile%filename),NF90_NOCLOBBER,td_ncfile%ncid))
		CALL chkerr( nf90_inq_varid( td_ncfile%ncid, tm_epAtt%r_cost          , td_ncfile%r_costid          ) )
		CALL chkerr( nf90_inq_varid( td_ncfile%ncid, tm_epAtt%l_simul_diverged, td_ncfile%l_simul_divergedid) )
		CALL chkerr( nf90_inq_varid( td_ncfile%ncid, tm_epAtt%ra_ctl          , td_ncfile%ra_ctlid          ) )
		CALL chkerr( nf90_inq_varid( td_ncfile%ncid, tm_epAtt%ra_dctl         , td_ncfile%ra_dctlid         ) )
		CALL chkerr( nf90_inq_varid( td_ncfile%ncid, tm_epAtt%ra_grad         , td_ncfile%ra_gradid         ) )
		CALL chkerr( nf90_inq_varid( td_ncfile%ncid, tm_epAtt%ra_b_ctl        , td_ncfile%ra_b_ctlid        ) )
		CALL chkerr( nf90_inq_varid( td_ncfile%ncid, tm_epAtt%ra_sigma_ctl    , td_ncfile%ra_sigma_ctlid    ) )
		CALL chkerr( nf90_inq_varid( td_ncfile%ncid, tm_epAtt%ra_Bm1          , td_ncfile%ra_Bm1id          ) )
		CALL chkerr( nf90_inq_varid( td_ncfile%ncid, tm_epAtt%ra_ctl_lb       , td_ncfile%ra_ctl_lbid       ) )
		CALL chkerr( nf90_inq_varid( td_ncfile%ncid, tm_epAtt%ra_ctl_ub       , td_ncfile%ra_ctl_ubid       ) )
		CALL chkerr( nf90_inq_varid( td_ncfile%ncid, tm_epAtt%la_check_ctl_lb , td_ncfile%la_check_ctl_lbid ) )
		CALL chkerr( nf90_inq_varid( td_ncfile%ncid, tm_epAtt%la_check_ctl_ub , td_ncfile%la_check_ctl_ubid ) )

		CALL chkerr(nf90_inquire(td_ncfile%ncid, il_nDimensions, il_nVariables, il_nAttributes, &
				il_unlimitedDimId, il_formatNum))

		CALL chkerr( nf90_inq_dimid( td_ncfile%ncid, tm_epAtt%i_nctl, il_nctlid ) )
		CALL chkerr( nf90_inquire_dimension( td_ncfile%ncid, il_nctlid, lc_name, td_ncfile%i_nctl ) )
		!PRINT*, "getting attributres"
		IF( PRESENT(td_ep) ) CALL read_ep_att(td_ncfile, td_ep)
		td_ncfile%isOpened = .TRUE.
		CALL debug('... done')
	END SUBROUTINE ncEPOpen

	SUBROUTINE read_ep_att( td_ncfile, td_ep )
		!read des attributs (param�tres) de la trajectoire
		type(exchange_param_output), INTENT(INOUT) :: td_ncfile
    TYPE(exchange_param), INTENT(INOUT) :: td_ep
    INTEGER :: il_simul_diverged, il_from_previous, il_first_simul, il_run_from_ctl,&
				il_useGD, il_amplitude, il_location, il_sigma
    
		CALL debug("  In read_ep_att, reading attributes")
		CALL set_ctlsize(td_ep, td_ncfile%i_nctl)
		CALL chkerr( nf90_get_att( td_ncfile%ncid, NF90_GLOBAL, tm_epAtt%aa_title   , td_ncfile%aa_title   ) )
		CALL chkerr( nf90_get_att( td_ncfile%ncid, NF90_GLOBAL, tm_epAtt%aa_history , td_ncfile%aa_history ) )
		CALL chkerr( nf90_get_att( td_ncfile%ncid, NF90_GLOBAL, tm_epAtt%ep_fName   , td_ep%ep_fName ) )
		CALL chkerr( nf90_get_att( td_ncfile%ncid, NF90_GLOBAL, tm_epAtt%cost_fName , td_ep%cost_fName ) )
		CALL chkerr( nf90_get_att( td_ncfile%ncid, NF90_GLOBAL, tm_epAtt%grad_fName , td_ep%grad_fName ) )
		CALL chkerr( nf90_get_att( td_ncfile%ncid, NF90_GLOBAL, tm_epAtt%ctl_fName  , td_ep%ctl_fName ) )
		CALL chkerr( nf90_get_att( td_ncfile%ncid, NF90_GLOBAL, tm_epAtt%bctl_fName , td_ep%bctl_fName ) )
		CALL chkerr( nf90_get_att( td_ncfile%ncid, NF90_GLOBAL, tm_epAtt%obs_fName  , td_ep%obs_fName ) )
		CALL chkerr( nf90_get_att( td_ncfile%ncid, NF90_GLOBAL, tm_epAtt%dmt_fName  , td_ep%dmt_fName ) )
		CALL chkerr( nf90_get_att( td_ncfile%ncid, NF90_GLOBAL, tm_epAtt%ogap_fName , td_ep%ogap_fName ) )
		CALL chkerr( nf90_get_att( td_ncfile%ncid, NF90_GLOBAL, tm_epAtt%ctl_all_fName  , td_ep%ctl_all_fName ) )
		CALL chkerr( nf90_get_att( td_ncfile%ncid, NF90_GLOBAL, tm_epAtt%ctl_bound_fName, td_ep%ctl_bound_fName ) )
		CALL chkerr( nf90_get_att( td_ncfile%ncid, NF90_GLOBAL, tm_epAtt%rt_dir     , td_ep%rt_dir ) )
		CALL chkerr( nf90_get_att( td_ncfile%ncid, NF90_GLOBAL, tm_epAtt%input_dir  , td_ep%input_dir ) )
		CALL chkerr( nf90_get_att( td_ncfile%ncid, NF90_GLOBAL, tm_epAtt%output_dir , td_ep%output_dir ) )
		CALL chkerr( nf90_get_att( td_ncfile%ncid, NF90_GLOBAL, tm_epAtt%aa_solver_action , td_ep%aa_solver_action ) )
		CALL chkerr( nf90_get_att( td_ncfile%ncid, NF90_GLOBAL, tm_epAtt%aa_simul_action  , td_ep%aa_simul_action ) )
		CALL chkerr( nf90_get_att( td_ncfile%ncid, NF90_GLOBAL, tm_epAtt%aa_solver_path   , td_ep%aa_solver_path ) )
		CALL chkerr( nf90_get_att( td_ncfile%ncid, NF90_GLOBAL, tm_epAtt%restart_fileName , td_ep%restart_fileName ) )
		CALL chkerr( nf90_get_att( td_ncfile%ncid, NF90_GLOBAL, tm_epAtt%restart_candidate, td_ep%restart_candidate ) )
		CALL chkerr( nf90_get_att( td_ncfile%ncid, NF90_GLOBAL, tm_epAtt%direct_fileName  , td_ep%direct_fileName ) )
		
		CALL chkerr( nf90_get_att( td_ncfile%ncid, NF90_GLOBAL, tm_epAtt%l_simul_diverged        , il_simul_diverged ) )
		CALL chkerr( nf90_get_att( td_ncfile%ncid, NF90_GLOBAL, tm_epAtt%l_restart_from_previous , il_from_previous ) )
		CALL chkerr( nf90_get_att( td_ncfile%ncid, NF90_GLOBAL, tm_epAtt%l_first_simul           , il_first_simul ) )
		CALL chkerr( nf90_get_att( td_ncfile%ncid, NF90_GLOBAL, tm_epAtt%l_run_from_ctl          , il_run_from_ctl ) )
		td_ep%l_simul_diverged        = int2l(il_simul_diverged)
		td_ep%l_restart_from_previous = int2l(il_from_previous)
		td_ep%l_first_simul  = int2l(il_first_simul)
		td_ep%l_run_from_ctl = int2l(il_run_from_ctl)
		
		CALL chkerr( nf90_get_att( td_ncfile%ncid, NF90_GLOBAL, tm_epAtt%r_mu , td_ep%r_mu ) )
		CALL chkerr( nf90_get_att( td_ncfile%ncid, NF90_GLOBAL, tm_epAtt%r_sigma , td_ep%r_sigma ) )
		CALL chkerr( nf90_get_att( td_ncfile%ncid, NF90_GLOBAL, tm_epAtt%r_v0 , td_ep%r_v0 ) )
		CALL chkerr( nf90_get_att( td_ncfile%ncid, NF90_GLOBAL, tm_epAtt%r_omega , td_ep%r_omega ) )
		CALL chkerr( nf90_get_att( td_ncfile%ncid, NF90_GLOBAL, tm_epAtt%l_useGD , il_useGD ) )
		td_ep%l_useGD = int2l(il_useGD)
		CALL chkerr( nf90_get_att( td_ncfile%ncid, NF90_GLOBAL, tm_epAtt%r_wGD , td_ep%r_wGD ) )
		CALL chkerr( nf90_get_att( td_ncfile%ncid, NF90_GLOBAL, tm_epAtt%r_wb , td_ep%r_wb ) )
		CALL chkerr( nf90_get_att( td_ncfile%ncid, NF90_GLOBAL, tm_epAtt%r_wGrad , td_ep%r_wGrad ) )
		CALL chkerr( nf90_get_att( td_ncfile%ncid, NF90_GLOBAL, tm_epAtt%l_amplitude , il_amplitude ) )
		CALL chkerr( nf90_get_att( td_ncfile%ncid, NF90_GLOBAL, tm_epAtt%l_location  , il_location ) )
		CALL chkerr( nf90_get_att( td_ncfile%ncid, NF90_GLOBAL, tm_epAtt%l_sigma     , il_sigma ) )
		td_ep%l_amplitude    = int2l(il_amplitude)
		td_ep%l_location     = int2l(il_location)
		td_ep%l_sigma        = int2l(il_sigma)
		CALL chkerr( nf90_get_att( td_ncfile%ncid, NF90_GLOBAL, tm_epAtt%i_obs_level, td_ep%i_obs_level) )
		CALL chkerr( nf90_get_att( td_ncfile%ncid, NF90_GLOBAL, tm_epAtt%i_nobs_x   , td_ep%i_nobs_x   ) )
		CALL chkerr( nf90_get_att( td_ncfile%ncid, NF90_GLOBAL, tm_epAtt%i_nobs_t   , td_ep%i_nobs_t   ) )
		CALL chkerr( nf90_get_att( td_ncfile%ncid, NF90_GLOBAL, tm_epAtt%r_sigmaR   , td_ep%r_sigmaR   ) )
		CALL chkerr( nf90_get_att( td_ncfile%ncid, NF90_GLOBAL, tm_epAtt%r_max_coef , td_ep%r_max_coef ) )
		CALL chkerr( nf90_get_att( td_ncfile%ncid, NF90_GLOBAL, tm_epAtt%r_nz_ratio , td_ep%r_nz_ratio ) )
		CALL chkerr( nf90_get_att( td_ncfile%ncid, NF90_GLOBAL, tm_epAtt%r_mes_fact , td_ep%r_mes_fact ) )
		CALL chkerr( nf90_get_att( td_ncfile%ncid, NF90_GLOBAL, tm_epAtt%r_cs_reg   , td_ep%r_cs_reg   ) )
		CALL chkerr( nf90_get_att( td_ncfile%ncid, NF90_GLOBAL, tm_epAtt%i_ctl_lev  , td_ep%i_ctl_lev  ) )
		CALL debug("  ... Done")
		!End of saving
	END SUBROUTINE read_ep_att

	SUBROUTINE ncEPCreate( td_ncfile, td_ep )
		type(exchange_param_output), INTENT(INOUT)      :: td_ncfile
    TYPE(exchange_param), INTENT(IN) :: td_ep
		!local variables
		INTEGER, DIMENSION(0) :: ila_empty
		INTEGER :: il_nctl

		CALL debug( TRIM(td_ncfile%filename), 'In ncEPCreate: creating ' )
		CALL chkerr( nf90_create( TRIM(td_ncfile%fileName), NF90_CLOBBER, td_ncfile%ncid ) )
		CALL chkerr( nf90_def_dim( td_ncfile%ncid, tm_epAtt%i_nctl, td_ncfile%i_nctl, il_nctl ) )
    CALL chkerr(nf90_def_var(td_ncfile%ncid, tm_epAtt%ra_ctl       , NF90_DOUBLE, il_nctl, td_ncfile%ra_ctlid  ) )
    CALL chkerr(nf90_def_var(td_ncfile%ncid, tm_epAtt%ra_dctl      , NF90_DOUBLE, il_nctl, td_ncfile%ra_dctlid ) )
    CALL chkerr(nf90_def_var(td_ncfile%ncid, tm_epAtt%ra_grad      , NF90_DOUBLE, il_nctl, td_ncfile%ra_gradid ) )
    CALL chkerr(nf90_def_var(td_ncfile%ncid, tm_epAtt%ra_b_ctl     , NF90_DOUBLE, il_nctl, td_ncfile%ra_b_ctlid) )
    CALL chkerr(nf90_def_var(td_ncfile%ncid, tm_epAtt%ra_sigma_ctl , NF90_DOUBLE, il_nctl, td_ncfile%ra_sigma_ctlid))
    CALL chkerr(nf90_def_var(td_ncfile%ncid, tm_epAtt%ra_Bm1       , NF90_DOUBLE, il_nctl, td_ncfile%ra_Bm1id))
    CALL chkerr(nf90_def_var(td_ncfile%ncid, tm_epAtt%ra_ctl_lb    , NF90_DOUBLE, il_nctl, td_ncfile%ra_ctl_lbid))
    CALL chkerr(nf90_def_var(td_ncfile%ncid, tm_epAtt%ra_ctl_ub    , NF90_DOUBLE, il_nctl, td_ncfile%ra_ctl_ubid))
    CALL chkerr(nf90_def_var(td_ncfile%ncid, tm_epAtt%la_check_ctl_lb , NF90_INT, il_nctl, td_ncfile%la_check_ctl_lbid))
    CALL chkerr(nf90_def_var(td_ncfile%ncid, tm_epAtt%la_check_ctl_ub , NF90_INT, il_nctl, td_ncfile%la_check_ctl_ubid))
    
    CALL chkerr(nf90_def_var(td_ncfile%ncid, tm_epAtt%r_cost    , NF90_DOUBLE   , ila_empty, td_ncfile%r_costid  ))
    CALL chkerr(nf90_def_var(td_ncfile%ncid, tm_epAtt%l_simul_diverged, NF90_INT, ila_empty, td_ncfile%l_simul_divergedid  ))
    
		CALL save_ep_att(td_ncfile, td_ep)
		CALL chkerr(nf90_enddef(td_ncfile%ncid))
		td_ncfile%isOpened = .TRUE.
		CALL debug('... ncEPCreate -> done')
	END SUBROUTINE ncEPCreate

	SUBROUTINE save_ep_att( td_ncfile, td_ep )
		!sauvegarde des attributs (param�tres) de la trajectoire
		type(exchange_param_output), INTENT(IN) :: td_ncfile
    TYPE(exchange_param), INTENT(IN) :: td_ep
    INTEGER :: il_simul_diverged, il_from_previous, il_first_simul, il_run_from_ctl,&
				il_useGD, il_amplitude, il_location, il_sigma

		CALL debug("  Saving attributes")
		CALL chkerr( nf90_put_att( td_ncfile%ncid, NF90_GLOBAL, tm_epAtt%aa_title   , td_ncfile%aa_title   ) )
		CALL chkerr( nf90_put_att( td_ncfile%ncid, NF90_GLOBAL, tm_epAtt%aa_history , td_ncfile%aa_history ) )
		CALL chkerr( nf90_put_att( td_ncfile%ncid, NF90_GLOBAL, tm_epAtt%ep_fName   , td_ep%ep_fName ) )
		CALL chkerr( nf90_put_att( td_ncfile%ncid, NF90_GLOBAL, tm_epAtt%cost_fName , td_ep%cost_fName ) )
		CALL chkerr( nf90_put_att( td_ncfile%ncid, NF90_GLOBAL, tm_epAtt%grad_fName , td_ep%grad_fName ) )
		CALL chkerr( nf90_put_att( td_ncfile%ncid, NF90_GLOBAL, tm_epAtt%ctl_fName  , td_ep%ctl_fName ) )
		CALL chkerr( nf90_put_att( td_ncfile%ncid, NF90_GLOBAL, tm_epAtt%bctl_fName , td_ep%bctl_fName ) )
		CALL chkerr( nf90_put_att( td_ncfile%ncid, NF90_GLOBAL, tm_epAtt%obs_fName  , td_ep%obs_fName ) )
		CALL chkerr( nf90_put_att( td_ncfile%ncid, NF90_GLOBAL, tm_epAtt%dmt_fName  , td_ep%dmt_fName ) )
		CALL chkerr( nf90_put_att( td_ncfile%ncid, NF90_GLOBAL, tm_epAtt%ogap_fName , td_ep%ogap_fName ) )
		CALL chkerr( nf90_put_att( td_ncfile%ncid, NF90_GLOBAL, tm_epAtt%ctl_all_fName  , td_ep%ctl_all_fName ) )
		CALL chkerr( nf90_put_att( td_ncfile%ncid, NF90_GLOBAL, tm_epAtt%ctl_bound_fName, td_ep%ctl_bound_fName ) )
		CALL chkerr( nf90_put_att( td_ncfile%ncid, NF90_GLOBAL, tm_epAtt%rt_dir     , td_ep%rt_dir ) )
		CALL chkerr( nf90_put_att( td_ncfile%ncid, NF90_GLOBAL, tm_epAtt%input_dir  , td_ep%input_dir ) )
		CALL chkerr( nf90_put_att( td_ncfile%ncid, NF90_GLOBAL, tm_epAtt%output_dir , td_ep%output_dir ) )
		CALL chkerr( nf90_put_att( td_ncfile%ncid, NF90_GLOBAL, tm_epAtt%aa_solver_action , td_ep%aa_solver_action ) )
		CALL chkerr( nf90_put_att( td_ncfile%ncid, NF90_GLOBAL, tm_epAtt%aa_simul_action  , td_ep%aa_simul_action ) )
		CALL chkerr( nf90_put_att( td_ncfile%ncid, NF90_GLOBAL, tm_epAtt%aa_solver_path   , td_ep%aa_solver_path ) )
		CALL chkerr( nf90_put_att( td_ncfile%ncid, NF90_GLOBAL, tm_epAtt%restart_fileName , td_ep%restart_fileName ) )
		CALL chkerr( nf90_put_att( td_ncfile%ncid, NF90_GLOBAL, tm_epAtt%restart_candidate, td_ep%restart_candidate ) )
		CALL chkerr( nf90_put_att( td_ncfile%ncid, NF90_GLOBAL, tm_epAtt%direct_fileName  , td_ep%direct_fileName ) )

		il_simul_diverged = INT(td_ep%l_simul_diverged)
		il_from_previous  = INT(td_ep%l_restart_from_previous)
		il_first_simul    = INT(td_ep%l_first_simul)
		il_run_from_ctl   = INT(td_ep%l_run_from_ctl)
		CALL chkerr( nf90_put_att( td_ncfile%ncid, NF90_GLOBAL, tm_epAtt%l_simul_diverged        , il_simul_diverged ) )
		CALL chkerr( nf90_put_att( td_ncfile%ncid, NF90_GLOBAL, tm_epAtt%l_restart_from_previous , il_from_previous ) )
		CALL chkerr( nf90_put_att( td_ncfile%ncid, NF90_GLOBAL, tm_epAtt%l_first_simul           , il_first_simul ) )
		CALL chkerr( nf90_put_att( td_ncfile%ncid, NF90_GLOBAL, tm_epAtt%l_run_from_ctl          , il_run_from_ctl ) )

		CALL chkerr( nf90_put_att( td_ncfile%ncid, NF90_GLOBAL, tm_epAtt%r_mu , td_ep%r_mu ) )
		CALL chkerr( nf90_put_att( td_ncfile%ncid, NF90_GLOBAL, tm_epAtt%r_sigma , td_ep%r_sigma ) )
		CALL chkerr( nf90_put_att( td_ncfile%ncid, NF90_GLOBAL, tm_epAtt%r_v0 , td_ep%r_v0 ) )
		CALL chkerr( nf90_put_att( td_ncfile%ncid, NF90_GLOBAL, tm_epAtt%r_omega , td_ep%r_omega ) )
		il_useGD = INT(td_ep%l_useGD)
		CALL chkerr( nf90_put_att( td_ncfile%ncid, NF90_GLOBAL, tm_epAtt%l_useGD , il_useGD ) )
		CALL chkerr( nf90_put_att( td_ncfile%ncid, NF90_GLOBAL, tm_epAtt%r_wGD , td_ep%r_wGD ) )
		CALL chkerr( nf90_put_att( td_ncfile%ncid, NF90_GLOBAL, tm_epAtt%r_wb , td_ep%r_wb ) )
		CALL chkerr( nf90_put_att( td_ncfile%ncid, NF90_GLOBAL, tm_epAtt%r_wGrad , td_ep%r_wGrad ) )
		il_amplitude = INT(td_ep%l_amplitude)
		il_location  = INT(td_ep%l_location)
		il_sigma     = INT(td_ep%l_sigma)
		CALL chkerr( nf90_put_att( td_ncfile%ncid, NF90_GLOBAL, tm_epAtt%l_amplitude , il_amplitude ) )
		CALL chkerr( nf90_put_att( td_ncfile%ncid, NF90_GLOBAL, tm_epAtt%l_location  , il_location ) )
		CALL chkerr( nf90_put_att( td_ncfile%ncid, NF90_GLOBAL, tm_epAtt%l_sigma     , il_sigma ) )
		CALL chkerr( nf90_put_att( td_ncfile%ncid, NF90_GLOBAL, tm_epAtt%i_obs_level, td_ep%i_obs_level) )
		CALL chkerr( nf90_put_att( td_ncfile%ncid, NF90_GLOBAL, tm_epAtt%i_nobs_x   , td_ep%i_nobs_x   ) )
		CALL chkerr( nf90_put_att( td_ncfile%ncid, NF90_GLOBAL, tm_epAtt%i_nobs_t   , td_ep%i_nobs_t   ) )
		CALL chkerr( nf90_put_att( td_ncfile%ncid, NF90_GLOBAL, tm_epAtt%r_sigmaR   , td_ep%r_sigmaR   ) )
		CALL chkerr( nf90_put_att( td_ncfile%ncid, NF90_GLOBAL, tm_epAtt%r_max_coef , td_ep%r_max_coef ) )
		CALL chkerr( nf90_put_att( td_ncfile%ncid, NF90_GLOBAL, tm_epAtt%r_nz_ratio , td_ep%r_nz_ratio ) )
		CALL chkerr( nf90_put_att( td_ncfile%ncid, NF90_GLOBAL, tm_epAtt%r_mes_fact , td_ep%r_mes_fact ) )
		CALL chkerr( nf90_put_att( td_ncfile%ncid, NF90_GLOBAL, tm_epAtt%r_cs_reg   , td_ep%r_cs_reg   ) )
		CALL chkerr( nf90_put_att( td_ncfile%ncid, NF90_GLOBAL, tm_epAtt%i_ctl_lev  , td_ep%i_ctl_lev  ) )
		CALL debug("  ...Done")
		!End of saving
	END SUBROUTINE save_ep_att
  
  !> \brief make file name according to the usage
  !! \param[in] td_ep, exchange parameter structure
  !! \param[in] id_dType data type (CTL_DATA, )
  !! \param[in] id_status file status (input, output, or runtime)
  !! \detail this function builds the appropriate file name(relative path and base name) giving the data type to be store in the file and its usage
  !<
  FUNCTION make_fileName(td_ep, id_dType, id_status)RESULT(ala_fName)
    TYPE(exchange_param), INTENT(IN) :: td_ep
    INTEGER, INTENT(IN) :: id_dType, id_status
    !local variables
    CHARACTER(len=ip_snl) :: ala_fName, ala_dir, ala_prefix, ala_baseName
    
    SELECT CASE(id_dType)
      CASE (CTL_DATA)
        ala_baseName = td_ep%ctl_fName
      CASE (BCTL_DATA)
        ala_baseName = td_ep%bctl_fName
      CASE (ACTL_DATA)
        ala_baseName = 'a_'//TRIM(td_ep%ctl_fName)
      CASE (DCTL_DATA)
        ala_baseName = 'd_'//TRIM(td_ep%ctl_fName)
      CASE (TCTL_DATA)
        ala_baseName = 't_'//TRIM(td_ep%ctl_fName)
      CASE (CTL_BOUND_DATA)
        ala_baseName = td_ep%ctl_bound_fName
      CASE (CTL_ALL_DATA)
        ala_baseName = td_ep%ctl_all_fName
      CASE (OBS_DATA)
        ala_baseName = td_ep%obs_fName
      CASE (AOBS_DATA)
        ala_baseName = 'a_'//TRIM(td_ep%obs_fName)
      CASE (TOBS_DATA)
        ala_baseName = 't_'//TRIM(td_ep%obs_fName)
      CASE (DMT_DATA)
        ala_baseName = td_ep%dmt_fName
      CASE (ADMT_DATA)
        ala_baseName = 'a_'//TRIM(td_ep%dmt_fName)
      CASE (TDMT_DATA)
        ala_baseName = 't_'//TRIM(td_ep%dmt_fName)
      CASE (OGAP_DATA)
        ala_baseName = td_ep%ogap_fName
      CASE (EP_DATA)
        ala_baseName = td_ep%ep_fName
      CASE (COST_DATA)
        ala_baseName = td_ep%cost_fName
      CASE (GRAD_DATA)
        ala_baseName = td_ep%grad_fName
      CASE DEFAULT
        WRITE(*,*) 'In make_fileName; bad data type :', id_dType
        WRITE(*,*) '  Stopping the program ...'
        STOP
    END SELECT
    
    SELECT CASE(id_status)
      CASE (INPUT_FILE)
        ala_dir = td_ep%input_dir
        ala_prefix = '/'
      CASE (OUTPUT_FILE)
        ala_dir = td_ep%output_dir
        ala_prefix = '/'
      CASE (RTIME_FILE)
        ala_dir = './'
        ala_prefix = 'rt_'
      CASE (BASE_FILE)
        ala_dir = ''
        ala_prefix = ''
      CASE DEFAULT
        WRITE(*,*) 'In make_fileName; bad file status :', id_status
        WRITE(*,*) '  Stopping the program ...'
        STOP
    END SELECT
    ala_fName = TRIM(ala_dir)//TRIM(ala_prefix)//TRIM(ala_baseName)
  END FUNCTION make_fileName

	!> \brief delete a runtime file
  !! \param[in] td_ep, exchange parameter structure
  !! \param[in] id_dType data type (CTL_DATA, )
	!<
  SUBROUTINE delete_rtFile(td_ep, id_dType)
    TYPE(exchange_param), INTENT(IN) :: td_ep
    INTEGER, INTENT(IN) :: id_dType
    !local variables
    CHARACTER(len=ip_snl) :: ala_fName
    
		ala_fName = make_fileName(td_ep, id_dType, RTIME_FILE)
		CALL system( 'rm -f '//ala_fName )
  END SUBROUTINE delete_rtFile
  
  !> \brief get the size of the control vector
  !! \param[in] td_ep, exchange parameter structure
  !<
  FUNCTION get_ctlSize(td_ep) RESULT(il_nctl)
    TYPE(exchange_param), INTENT(IN) :: td_ep
    INTEGER :: il_nctl
    
    il_nctl = td_ep%i_nctl
  END FUNCTION get_ctlSize
  
  !> \brief set single file name in exchange parameter
  !! \param[in] td_ep, exchange parameter
  !! \param[in] ada_fName file name
  !! \param[in] id_dType data type (CTL_DATA, )
  !<
  SUBROUTINE set_fileName(td_ep, id_dType, ada_fName)
    TYPE(exchange_param), INTENT(INOUT) :: td_ep
    INTEGER, INTENT(IN) :: id_dType
    CHARACTER(LEN=*), INTENT(IN) :: ada_fName
    
    SELECT CASE(id_dType)
      CASE (CTL_DATA)
        td_ep%ctl_fName  = ADJUSTL(ada_fName)
      CASE (BCTL_DATA)
        td_ep%bctl_fName = ADJUSTL(ada_fName)
      CASE (CTL_BOUND_DATA)
        td_ep%ctl_bound_fName = ADJUSTL(ada_fName)
      CASE (CTL_ALL_DATA)
        td_ep%ctl_all_fName   = ADJUSTL(ada_fName)
      CASE (OBS_DATA)
        td_ep%obs_fName  = ADJUSTL(ada_fName)
      CASE (DMT_DATA)
        td_ep%dmt_fName  = ADJUSTL(ada_fName)
      CASE (OGAP_DATA)
        td_ep%ogap_fName = ADJUSTL(ada_fName)
      CASE (EP_DATA)
        td_ep%ep_fName   = ADJUSTL(ada_fName)
      CASE (COST_DATA)
        td_ep%cost_fName = ADJUSTL(ada_fName)
      CASE (GRAD_DATA)
        td_ep%grad_fName = ADJUSTL(ada_fName)
      CASE DEFAULT
        WRITE(*,*) 'In set_fileName; bad data type or no field defined :', id_dType
        WRITE(*,*) '  Stopping the program ...'
        STOP
    END SELECT
  END SUBROUTINE set_fileName
  
  !> \brief set the name of the data files
  !! \param[in] td_ep, exchange parameter
  !<
  SUBROUTINE set_data_fileNames(td_ep, ada_ctl_fName, ada_bctl_fName,&
                                ada_obs_fName, ada_dmt_fName, ada_ogap_fName&
  )
    TYPE(exchange_param), INTENT(INOUT) :: td_ep
    CHARACTER(LEN=*), INTENT(IN) :: ada_ctl_fName, ada_bctl_fName, ada_obs_fName,&
                                    ada_dmt_fName, ada_ogap_fName
    
    CALL set_fileName(td_ep, CTL_DATA , ada_ctl_fName)
    CALL set_fileName(td_ep, BCTL_DATA, ada_bctl_fName)
    CALL set_fileName(td_ep, OBS_DATA , ada_obs_fName)
    CALL set_fileName(td_ep, DMT_DATA , ada_dmt_fName)
    CALL set_fileName(td_ep, OGAP_DATA, ada_ogap_fName)
  END SUBROUTINE set_data_fileNames

  !> \brief set the data directories
  !! \param[in, out] td_ep, exchange parameter
  !! \param[in] ada_input_dir input directory
  !! \param[in] ada_output_dir output directory
  !! \detail empty path is replaced by local directory
  !<
  SUBROUTINE set_data_dir(td_ep, ada_input_dir, ada_output_dir)
    TYPE(exchange_param), INTENT(INOUT) :: td_ep
    CHARACTER(LEN=*), INTENT(IN) :: ada_input_dir, ada_output_dir
    !local variables
    INTEGER :: il_sepPos, il_trimSize
    
    CALL debug('', 'Entering set_data_dir ***********************')
    !input dir
    il_trimSize = LEN_TRIM(ada_input_dir)
    il_sepPos   = INDEX(ada_input_dir, '/', BACK = .TRUE.)! Last occurence
    IF ( (il_sepPos > 0).AND.(il_sepPos == il_trimSize) )THEN !Rrailing separator
       td_ep%input_dir = ADJUSTL( ada_input_dir(:il_sepPos-1) )
    ELSE
       td_ep%input_dir = ADJUSTL( ada_input_dir )
    END IF
    IF (LEN_TRIM(td_ep%input_dir)==0) td_ep%input_dir = '.' !Replace empty string by working dir

    !output dir
    il_trimSize = LEN_TRIM(ada_output_dir)
    il_sepPos   = INDEX(ada_output_dir, '/', BACK = .TRUE.) !last occurence
    IF ( (il_sepPos > 0).AND.(il_sepPos == il_trimSize) )THEN !trailing separator
       td_ep%output_dir = ADJUSTL( ada_output_dir(:il_sepPos-1) )
    ELSE
       td_ep%output_dir = ADJUSTL( ada_output_dir )
    END IF
    IF (LEN_TRIM(td_ep%output_dir)==0)THEN !empty directory path
       td_ep%output_dir = '.'! replace empty string by working dir
    ELSE
       CALL SYSTEM('mkdir -p '//td_ep%output_dir) ! create the directory if it does not exist
    END IF
    
!!$    td_ep%input_dir  = 'input/'//ADJUSTL(ada_data_dir)
!!$    td_ep%output_dir = 'results/'//ADJUSTL(ada_data_dir)
    CALL debug(td_ep%input_dir, 'td_ep%input_dir = ')
    CALL debug(td_ep%output_dir, 'td_ep%output_dir = ')
    CALL debug('', 'Exiting set_data_dir  ***********************')
  END SUBROUTINE set_data_dir

  !> \brief get the input directory
  !! \param[in] td_ep, exchange parameter structure
  !<
  FUNCTION get_input_dir(td_ep) RESULT(ala_dir)
    TYPE(exchange_param), INTENT(IN) :: td_ep
    CHARACTER(LEN=ip_snl) :: ala_dir
    
    ala_dir = td_ep%input_dir
  END FUNCTION get_input_dir

  !> \brief Returns the number of elements of the control vector that are out of bounds
  !! \param[in] td_ep, exchange parameter
  !<
  FUNCTION get_nb_outOfBound(td_ep) RESULT(il_nb_outOfBound)
    TYPE(exchange_param), INTENT(IN) :: td_ep
    INTEGER :: il_nb_outOfBound
    
    il_nb_outOfBound = COUNT(&
         ( td_ep%la_check_ctl_lb.AND.(td_ep%ra_b_ctl+td_ep%ra_ctl <= td_ep%ra_ctl_lb) ).OR.&
         ( td_ep%la_check_ctl_ub.AND.(td_ep%ra_b_ctl+td_ep%ra_ctl >= td_ep%ra_ctl_ub) )&
    )
  END FUNCTION get_nb_outOfBound
  
  !>resize the array variables, control parameters
  !! \param[in, out] td_ep exchange parameter
  !! \param[in] id_nctl size of the control vector
  !! \detail this procedure set the boolean parameters to .FALSE.
  !<
  SUBROUTINE set_ctlsize(td_ep, id_nctl)
    TYPE(exchange_param), INTENT(INOUT) :: td_ep
    INTEGER, INTENT(IN) :: id_nctl
    !if the current size is different from the required size, reallocate
    IF(td_ep%i_nctl/=id_nctl)THEN
      !if currently allocated, deallocate
      IF(td_ep%i_nctl>0)THEN
        DEALLOCATE(td_ep%ra_ctl         , td_ep%ra_grad        ,&
                   td_ep%ra_b_ctl       , td_ep%ra_sigma_ctl   ,&
                   td_ep%ra_ctl_lb      , td_ep%ra_ctl_ub      ,&
                   td_ep%la_check_ctl_lb, td_ep%la_check_ctl_ub,&
                   td_ep%ra_Bm1&
        )
      END IF
      !if the required size is greater than zero, allocate
      IF(id_nctl > 0) THEN
        ALLOCATE(td_ep%ra_ctl(id_nctl)   , td_ep%ra_grad(id_nctl)        ,&
                 td_ep%ra_b_ctl(id_nctl) , td_ep%ra_sigma_ctl(id_nctl)   ,&
                 td_ep%ra_ctl_lb(id_nctl), td_ep%la_check_ctl_lb(id_nctl),&
                 td_ep%ra_ctl_ub(id_nctl), td_ep%la_check_ctl_ub(id_nctl),&
                 td_ep%ra_Bm1(id_nctl)&
        )
      END IF
    END IF
    !set the new size and set the fields to the default values
    td_ep%i_nctl = id_nctl
    CALL set_default_ctl_param(td_ep)
  END SUBROUTINE set_ctlsize
  
  SUBROUTINE set_default_ctl_param(td_ep)
    TYPE(exchange_param), INTENT(INOUT) :: td_ep
    
    IF(td_ep%i_nctl>0)THEN
      td_ep%ra_ctl          = 0.0_cp
      td_ep%ra_grad         = -999.0_cp
      td_ep%ra_b_ctl        = 0.0_cp
      td_ep%la_check_ctl_lb = .FALSE.
      td_ep%la_check_ctl_ub = .FALSE.
      td_ep%ra_ctl_lb       = 0.0_cp
      td_ep%ra_ctl_ub       = 0.0_cp
      td_ep%ra_sigma_ctl    = 1.0_cp
    END IF
  END SUBROUTINE set_default_ctl_param
  
  !> \brief write ctl in the friendly-plot format
  !! \param[in] rda_data vector to be saved
  !<
  SUBROUTINE write_vector_for_plot(rda_data, ada_fName)
    REAL(cp), DIMENSION(:), INTENT(IN) :: rda_data
    CHARACTER(LEN=*), INTENT(IN) :: ada_fName
    !local variables
    INTEGER :: ibi, il_nb
    TYPE(dyn_rVector), DIMENSION(2) :: tla_save
    il_nb = SIZE(rda_data)
    tla_save(1) = REAL( (/(ibi, ibi=1,il_nb)/), cp )
    tla_save(2) = rda_data
    
    CALL save_trj( tla_save, TRIM( make_pFileName(ada_fName) ), ld_column = .TRUE. )
    CALL reset_drv_array( tla_save )
  END SUBROUTINE write_vector_for_plot
  
  !> \brief read exchange parameters between the solver and the minimization driver
  !! \param[in, out] td_ep, exchange parameter, contains the name of the file as input
  !!     and the read params as output
  !! \param[in] id_fileId file identifier of the data file
  !! \detail the file is supposed to be opened and the associated identifier store in the variable id_fileId;
  !<
  SUBROUTINE read_ep_internal(td_ep, td_ncfile)
    TYPE(exchange_param), INTENT(INOUT) :: td_ep
		type(exchange_param_output), INTENT(INOUT)      :: td_ncfile

    CALL read_ctl_size_internal(td_ep, td_ncfile)
    CALL read_ep_att( td_ncfile, td_ep )
    
    !If one is going to read many blocs in the same file, it is recommended to rewind
    !reading other blocs
    CALL read_ctl_all_internal(td_ep, td_ncfile)
    CALL read_cost_internal(td_ep, td_ncfile)
    CALL read_grad_internal(td_ep, td_ncfile)
    !assigning file name informations
  END SUBROUTINE read_ep_internal

  !> \brief write exchange parameters between the solver and the minimization driver
  !! \param[in, out] td_ep, exchange parameter, contains the name of the file as input
  !!  and the read params as output
  !! \param[in] id_fileId file identifier of the data file
  !! \detail the file is supposed to be opened and the associated identifier store in the variable id_fileId;
  !<
  SUBROUTINE write_ep_internal(td_ep, td_ncfile)
    TYPE(exchange_param), INTENT(IN)        :: td_ep
		type(exchange_param_output), INTENT(IN) :: td_ncfile

    CALL write_ctl_all_internal(td_ep, td_ncfile)
    CALL write_cost_internal(td_ep, td_ncfile)
    CALL write_grad_internal(td_ep, td_ncfile)
  END SUBROUTINE write_ep_internal

  !> \brief internal subroutine to read the simulation state
  !! \param[in, out] td_ep, exchange parameter
  !! \param[in] id_fileId file identifier of the data file
  !<
  SUBROUTINE read_simulStatus_internal(td_ep, td_ncfile)
    TYPE(exchange_param), INTENT(INOUT)     :: td_ep
		type(exchange_param_output), INTENT(IN) :: td_ncfile
		!local variable
		INTEGER :: il_simul_diverged

		CALL chkerr( nf90_get_var( td_ncfile%ncid, td_ncfile%l_simul_divergedid, il_simul_diverged ) )
		td_ep%l_simul_diverged = int2l(il_simul_diverged)
  END SUBROUTINE read_simulStatus_internal
  
  !> \brief internal subroutine to read the ctl size, this subroutine allocates space for dynamic fields
  !! \param[in, out] td_ep, exchange parameter
  !! \param[in] id_fileId file identifier of the data file
  !<
  SUBROUTINE read_ctl_size_internal(td_ep, td_ncfile)
    TYPE(exchange_param), INTENT(INOUT)     :: td_ep
		type(exchange_param_output), INTENT(IN) :: td_ncfile
		
    CALL set_ctlsize(td_ep, td_ncfile%i_nctl)
  END SUBROUTINE read_ctl_size_internal

  !>\brief internal subroutine to read the cost function between the solver and the minimization driver
  !! \param[in, out] td_ep, exchange parameter
  !! \param[in] id_fileId file identifier of the data file
  !!\detail the file is supposed to be opened and the associated identifier store in the variable id_fileId; The pointer fields in td_ep are supposed to be associated and the allocatable fields allocated
  !<
  SUBROUTINE read_cost_internal(td_ep, td_ncfile)
    TYPE(exchange_param), INTENT(INOUT)     :: td_ep
		type(exchange_param_output), INTENT(IN) :: td_ncfile

		CALL chkerr( nf90_get_var( td_ncfile%ncid, td_ncfile%r_costid, td_ep%r_cost ) )
  END SUBROUTINE read_cost_internal

  !>\brief internal subroutine to read the gradient of the cost function between the solver and the minimization driver
  !! \param[in, out] td_ep, exchange parameter
  !! \param[in] id_fileId file identifier of the data file
  !!\detail the file is supposed to be opened and the associated identifier store in the variable id_fileId; The pointer fields in td_ep are supposed to be associated and the allocatable fields allocated
  !<
  SUBROUTINE read_grad_internal(td_ep, td_ncfile)
    TYPE(exchange_param), INTENT(INOUT)     :: td_ep
		type(exchange_param_output), INTENT(IN) :: td_ncfile

		CALL chkerr( nf90_get_var( td_ncfile%ncid, td_ncfile%ra_gradid, td_ep%ra_grad ) )
  END SUBROUTINE read_grad_internal
  
  !>\brief internal subroutine to read ctl data
  !! \param[in, out] td_ep, exchange parameter
  !! \param[in] id_fileId file identifier of the data file
  !!\detail the file is supposed to be opened and the associated identifier store in the variable id_fileId; The pointer fields in td_ep are supposed to be associated and the allocatable fields allocated
  !! this routine set the background and zeroe the increment
  !<
  SUBROUTINE read_ctl_internal(td_ep, td_ncfile)
    TYPE(exchange_param), INTENT(INOUT)     :: td_ep
		type(exchange_param_output), INTENT(IN) :: td_ncfile

		CALL chkerr( nf90_get_var( td_ncfile%ncid, td_ncfile%ra_ctlid, td_ep%ra_b_ctl ) )
		td_ep%ra_ctl = 0.0_cp
!     !local variables  
!     REAL(KIND=cp), DIMENSION(:), POINTER :: rla_ctl
!     
!     NAMELIST/NAM_ctl/rla_ctl
!     
!     rla_ctl => td_ep%ra_b_ctl
!     td_ep%ra_ctl = 0.0_cp
!     
!     REWIND(id_fileId)
!     READ(id_fileId, NAM_ctl)!reading the ctl block
  END SUBROUTINE read_ctl_internal
  
  !>\brief internal subroutine to read ctl data (ctl increment or delta ctl)
  !! \param[in, out] td_ep, exchange parameter
  !! \param[in] id_fileId file identifier of the data file
  !!\detail the file is supposed to be opened and the associated identifier store in the variable id_fileId; The pointer fields in td_ep are supposed to be associated and the allocatable fields allocated
  !! the true CTL is obtained by addind the background and the increment
  !<
  SUBROUTINE read_dctl_internal(td_ep, td_ncfile)
    TYPE(exchange_param), INTENT(INOUT)     :: td_ep
		type(exchange_param_output), INTENT(IN) :: td_ncfile

		CALL chkerr( nf90_get_var( td_ncfile%ncid, td_ncfile%ra_dctlid, td_ep%ra_ctl ) )
  END SUBROUTINE read_dctl_internal
  
  !>\brief internal subroutine to read backgroung ctl data and associated informations
  !! \param[in, out] td_ep, exchange parameter
  !! \param[in] id_fileId file identifier of the data file
  !!\detail the file is supposed to be opened and the associated identifier store in the variable id_fileId; the pointer fields in td_ep are supposed to be associated and the allocatable fields allocated
  !<
  SUBROUTINE read_bctl_internal(td_ep, td_ncfile)
    TYPE(exchange_param), INTENT(INOUT)     :: td_ep
		type(exchange_param_output), INTENT(IN) :: td_ncfile

		CALL chkerr( nf90_get_var( td_ncfile%ncid, td_ncfile%ra_b_ctlid    , td_ep%ra_b_ctl     ) )
		CALL chkerr( nf90_get_var( td_ncfile%ncid, td_ncfile%ra_sigma_ctlid, td_ep%ra_sigma_ctl ) )
		
    td_ep%ra_Bm1         = 0.0_cp
    WHERE( td_ep%ra_sigma_ctl > 0.0_cp ) td_ep%ra_Bm1 = 1.0_cp/td_ep%ra_sigma_ctl**2
  END SUBROUTINE read_bctl_internal
  
  !>\brief internal subroutine to read ctl bounds and associated informations
  !! \param[in, out] td_ep, exchange parameter
  !! \param[in] id_fileId file identifier of the data file
  !!\detail the file is supposed to be opened and the associated identifier store in the variable id_fileId; The pointer fields in td_ep are supposed to be associated and the allocatable fields allocated
  !<
  SUBROUTINE read_ctl_bound_internal(td_ep, td_ncfile)
    TYPE(exchange_param), INTENT(INOUT)     :: td_ep
		type(exchange_param_output), INTENT(IN) :: td_ncfile
		INTEGER, DIMENSION( td_ncfile%i_nctl ) :: ila_check
		INTEGER :: ibi

		CALL chkerr( nf90_get_var( td_ncfile%ncid, td_ncfile%ra_ctl_lbid, td_ep%ra_ctl_lb ) )
		CALL chkerr( nf90_get_var( td_ncfile%ncid, td_ncfile%ra_ctl_ubid, td_ep%ra_ctl_ub ) )
		CALL chkerr( nf90_get_var( td_ncfile%ncid, td_ncfile%la_check_ctl_lbid, ila_check ) )
		DO ibi = 1, td_ncfile%i_nctl
			td_ep%la_check_ctl_lb(ibi) = int2l( ila_check(ibi) )
		END DO
		CALL chkerr( nf90_get_var( td_ncfile%ncid, td_ncfile%la_check_ctl_ubid, ila_check ) )
		DO ibi = 1, td_ncfile%i_nctl
			td_ep%la_check_ctl_ub(ibi) = int2l( ila_check(ibi) )
		END DO
  END SUBROUTINE read_ctl_bound_internal
  
  !>\brief internal subroutine to read all the ctl informations
  !! \param[in, out] td_ep, exchange parameter
  !! \param[in] id_fileId file identifier of the data file
  !!\detail the file is supposed to be opened and the associated identifier store in the variable id_fileId; The pointer fields in td_ep are supposed to be associated and the allocatable fields allocated
  !<
  SUBROUTINE read_ctl_all_internal(td_ep, td_ncfile)
    TYPE(exchange_param), INTENT(INOUT)     :: td_ep
		type(exchange_param_output), INTENT(IN) :: td_ncfile
    
    CALL read_dctl_internal(td_ep, td_ncfile)
    CALL read_bctl_internal(td_ep, td_ncfile)
    CALL read_ctl_bound_internal(td_ep, td_ncfile)
  END SUBROUTINE read_ctl_all_internal

  SUBROUTINE write_simulStatus_internal(td_ep, td_ncfile)
    TYPE(exchange_param), INTENT(IN)        :: td_ep
		type(exchange_param_output), INTENT(IN) :: td_ncfile
		INTEGER :: il_simul_diverged

		il_simul_diverged = INT(td_ep%l_simul_diverged)
		CALL chkerr( nf90_put_var( td_ncfile%ncid, td_ncfile%l_simul_divergedid, il_simul_diverged ) )
  END SUBROUTINE write_simulStatus_internal

  !> \brief internal routine to write the cost function between the solver and the minimization driver
  !! \param[in, out] td_ep, exchange parameter
  !! \param[in] id_fileId file identifier of the data file
  !!\detail the file is supposed to be opened and the associated identifier store in the variable id_fileId; The pointer fields in td_ep are supposed to be associated and the allocatable fields allocated
  !<
  SUBROUTINE write_cost_internal(td_ep, td_ncfile)
    TYPE(exchange_param), INTENT(IN)        :: td_ep
		type(exchange_param_output), INTENT(IN) :: td_ncfile

		CALL chkerr( nf90_put_var( td_ncfile%ncid, td_ncfile%r_costid, td_ep%r_cost ) )
  END SUBROUTINE write_cost_internal

  !> \brief write the gradient of the cost function between the solver and the minimization driver
  !! \param[in, out] td_ep, exchange parameter
  !! \param[in] id_status file status (input, output, or runtime)
  !<
  SUBROUTINE write_grad_internal(td_ep, td_ncfile)
    TYPE(exchange_param), INTENT(IN)        :: td_ep
		type(exchange_param_output), INTENT(IN) :: td_ncfile

		CALL chkerr( nf90_put_var( td_ncfile%ncid, td_ncfile%ra_gradid, td_ep%ra_grad ) )
  END SUBROUTINE write_grad_internal
  
  !>\brief internal subroutine to write ctl data
  !! \param[in, out] td_ep, exchange parameter
  !! \param[in] id_fileId file identifier of the data file
  !!\detail the file is supposed to be opened and the associated identifier store in the variable id_fileId; The pointer fields in td_ep are supposed to be associated and the allocatable fields allocated
  !<
  SUBROUTINE write_ctl_internal(td_ep, td_ncfile)
    TYPE(exchange_param), INTENT(IN)        :: td_ep
		type(exchange_param_output), INTENT(IN) :: td_ncfile
    !local variables  
    REAL(KIND=cp), DIMENSION(:), ALLOCATABLE :: rla_ctl

    ALLOCATE( rla_ctl(td_ep%i_nctl) )
    rla_ctl = td_ep%ra_ctl + td_ep%ra_b_ctl
    CALL chkerr( nf90_put_var( td_ncfile%ncid, td_ncfile%ra_ctlid, rla_ctl ) )
    DEALLOCATE(rla_ctl)
  END SUBROUTINE write_ctl_internal
  
  !>\brief internal subroutine to write backgroung ctl data and associated informations
  !! \param[in, out] td_ep, exchange parameter
  !! \param[in] id_fileId file identifier of the data file
  !!\detail the file is supposed to be opened and the associated identifier store in the variable id_fileId; The pointer fields in td_ep are supposed to be associated and the allocatable fields allocated
  !<
  SUBROUTINE write_bctl_internal(td_ep, td_ncfile)
    TYPE(exchange_param), INTENT(IN)        :: td_ep
		type(exchange_param_output), INTENT(IN) :: td_ncfile

		CALL chkerr( nf90_put_var( td_ncfile%ncid, td_ncfile%ra_b_ctlid    , td_ep%ra_b_ctl     ) )
		CALL chkerr( nf90_put_var( td_ncfile%ncid, td_ncfile%ra_sigma_ctlid, td_ep%ra_sigma_ctl ) )
  END SUBROUTINE write_bctl_internal
  
  !>\brief internal subroutine to write the increment of ctl data
  !! \param[in, out] td_ep, exchange parameter
  !! \param[in] id_fileId file identifier of the data file
  !!\detail the file is supposed to be opened and the associated identifier store in the variable id_fileId; The pointer fields in td_ep are supposed to be associated and the allocatable fields allocated
  !<
  SUBROUTINE write_dctl_internal(td_ep, td_ncfile)
    TYPE(exchange_param), INTENT(IN)        :: td_ep
		type(exchange_param_output), INTENT(IN) :: td_ncfile

		CALL chkerr( nf90_put_var( td_ncfile%ncid, td_ncfile%ra_dctlid, td_ep%ra_ctl ) )
  END SUBROUTINE write_dctl_internal
  
  !>\brief internal subroutine to write ctl bounds and associated informations
  !! \param[in, out] td_ep, exchange parameter
  !! \param[in] id_fileId file identifier of the data file
  !!\detail the file is supposed to be opened and the associated identifier store in the variable id_fileId; The pointer fields in td_ep are supposed to be associated and the allocatable fields allocated
  !<
  SUBROUTINE write_ctl_bound_internal(td_ep, td_ncfile)
    TYPE(exchange_param), INTENT(IN) :: td_ep
		type(exchange_param_output), INTENT(IN)         :: td_ncfile
		INTEGER, DIMENSION( td_ncfile%i_nctl ) :: ila_check
		INTEGER :: ibi

		CALL chkerr( nf90_put_var( td_ncfile%ncid, td_ncfile%ra_ctl_lbid, td_ep%ra_ctl_lb ) )
		CALL chkerr( nf90_put_var( td_ncfile%ncid, td_ncfile%ra_ctl_ubid, td_ep%ra_ctl_ub ) )
		DO ibi = 1, td_ncfile%i_nctl
			ila_check(ibi) = INT(td_ep%la_check_ctl_lb(ibi) )
		END DO
		CALL chkerr( nf90_put_var( td_ncfile%ncid, td_ncfile%la_check_ctl_lbid, ila_check ) )
		DO ibi = 1, td_ncfile%i_nctl
			ila_check(ibi) = INT(td_ep%la_check_ctl_ub(ibi) )
		END DO
		CALL chkerr( nf90_put_var( td_ncfile%ncid, td_ncfile%la_check_ctl_ubid, ila_check ) )
  END SUBROUTINE write_ctl_bound_internal
  
  !>\brief internal subroutine to write all the ctl informations
  !! \param[in, out] td_ep, exchange parameter
  !! \param[in] id_fileId file identifier of the data file
  !!\detail the file is supposed to be opened and the associated identifier store in the variable id_fileId; The pointer fields in td_ep are supposed to be associated and the allocatable fields allocated
  !<
  SUBROUTINE write_ctl_all_internal(td_ep, td_ncfile)
    TYPE(exchange_param), INTENT(IN) :: td_ep
		type(exchange_param_output), INTENT(IN)         :: td_ncfile
    
    CALL write_dctl_internal(td_ep, td_ncfile)
    CALL write_bctl_internal(td_ep, td_ncfile)
    CALL write_ctl_bound_internal(td_ep, td_ncfile)
  END SUBROUTINE write_ctl_all_internal
  
  !> \brief read the ctl size from the given file and allocates space for dynamic fields
  !! \param[in] td_ep, exchange parameter structure
  !! \param[in] ada_fName file name
  !<
  SUBROUTINE read_ctl_size(td_ep, ada_fName)
    INTEGER, PARAMETER:: ip_numnam = 67
    TYPE(exchange_param), INTENT(INOUT) :: td_ep
    type(exchange_param_output) :: tl_ncfile
    CHARACTER(LEN=ip_snl), INTENT(IN)   :: ada_fName

    CALL initEPOutput(tl_ncfile, ada_fName)
    CALL ncEPOpen( tl_ncfile )
    CALL read_ctl_size_internal(td_ep, tl_ncfile)
    CALL ncEPclose(tl_ncfile)
  END SUBROUTINE read_ctl_size
  
  !> \brief read exchange parameter data to file
  !! \param[in] td_ep, exchange parameter structure
  !! \param[in] id_dType data type (CTL_DATA, )
  !! \param[in] id_status file status (input, or runtime)
  !! \detail the status information is used to choose the input directory
  !<
  SUBROUTINE read_ep_data(td_ep, id_dType, id_status)
    INTEGER, PARAMETER:: ip_numnam = 67
    TYPE(exchange_param), INTENT(INOUT) :: td_ep
    INTEGER, INTENT(IN) :: id_dType, id_status
    !local variables
    type(exchange_param_output) :: tl_ncfile
    CHARACTER(LEN=ip_snl) :: ala_fName
    
    ala_fName = make_fileName(td_ep, id_dType, id_status)
    CALL debug(ala_fName, 'In read_ep_data, reading the file ** ')
    CALL initEPOutput(tl_ncfile, ala_fName)
    CALL ncEPOpen( tl_ncfile )
    !processing simulation state when needed
    SELECT CASE(id_dType)
      CASE(EP_DATA, COST_DATA, GRAD_DATA)
        CALL read_simulStatus_internal(td_ep, tl_ncfile) !always read the status
      CASE DEFAULT
        !nothing
    END SELECT
    SELECT CASE(id_dType)
      CASE (CTL_DATA, ACTL_DATA, TCTL_DATA)
        CALL read_ctl_internal(td_ep, tl_ncfile)
      CASE (BCTL_DATA)
        CALL read_bctl_internal(td_ep, tl_ncfile)
      CASE (DCTL_DATA)
        CALL read_dctl_internal(td_ep, tl_ncfile)
      CASE (CTL_BOUND_DATA)
        CALL read_ctl_bound_internal(td_ep, tl_ncfile)
      CASE (CTL_ALL_DATA)
        CALL read_ctl_internal(td_ep, tl_ncfile)
      CASE (EP_DATA)
        CALL read_ep_internal(td_ep, tl_ncfile)
      CASE (COST_DATA)
        CALL read_cost_internal(td_ep, tl_ncfile)
      CASE (GRAD_DATA)
        CALL read_grad_internal(td_ep, tl_ncfile)
      CASE DEFAULT
        WRITE(*,*) 'In read_ep_data; bad data type :', id_dType
        WRITE(*,*) '  Stopping the program ...'
        STOP
    END SELECT
    CALL ncEPclose(tl_ncfile)
  END SUBROUTINE read_ep_data
  
  !> \brief write exchange parameter data to file
  !! \param[in] td_ep, exchange parameter structure
  !! \param[in] id_dType data type (CTL_DATA, )
  !! \param[in] id_status file status (output, or runtime)
  !! \param[in] ada_text text description of the action that created the information to be saved
  !! \detail the status information is used to choose the output directory
  !! the size of the control vector is automatically saved
  !<
  SUBROUTINE write_ep_data(td_ep, id_dType, id_status, ada_text)
    INTEGER, PARAMETER:: ip_numnam = 68
    TYPE(exchange_param), INTENT(IN) :: td_ep
    INTEGER, INTENT(IN) :: id_dType, id_status
    CHARACTER(LEN=*), INTENT(IN) :: ada_text
    !local variables
    type(exchange_param_output) :: tl_ncfile
    CHARACTER(LEN=ip_fnl) :: ala_history, ala_fName
    REAL(cp), DIMENSION(:), POINTER:: rla_plot_data
    
    !initializing dynamic space
    rla_plot_data => NULL()
    
    ala_fName = make_fileName(td_ep, id_dType, id_status)
    CALL initEPOutput(tl_ncfile, ala_fName, td_ep%i_nctl)
    tl_ncfile%aa_title = ""
    tl_ncfile%aa_history = make_ncHistory(ada_text)
    CALL ncEPCreate( tl_ncfile, td_ep )
        
    !processing simulation state when needed
    !SELECT CASE(id_dType)
    !  CASE(EP_DATA, COST_DATA, GRAD_DATA)
				CALL write_simulStatus_internal(td_ep, tl_ncfile) !always write the status
    !  CASE DEFAULT
        !nothing
    !END SELECT
    SELECT CASE(id_dType)
      CASE (CTL_DATA, ACTL_DATA, TCTL_DATA)
        CALL write_ctl_internal(td_ep, tl_ncfile)
        ALLOCATE( rla_plot_data(td_ep%i_nctl) )
        rla_plot_data = td_ep%ra_ctl + td_ep%ra_b_ctl
      CASE (BCTL_DATA)
        CALL write_bctl_internal(td_ep, tl_ncfile)
        rla_plot_data => td_ep%ra_b_ctl
      CASE (DCTL_DATA)
        CALL write_dctl_internal(td_ep, tl_ncfile)
        rla_plot_data => td_ep%ra_ctl
      CASE (CTL_BOUND_DATA)
        CALL write_ctl_bound_internal(td_ep, tl_ncfile)
      CASE (CTL_ALL_DATA)
        CALL write_ctl_all_internal(td_ep, tl_ncfile)
      CASE (EP_DATA)
        CALL write_ep_internal(td_ep, tl_ncfile)
      CASE (COST_DATA)
        CALL write_cost_internal(td_ep, tl_ncfile)
      CASE (GRAD_DATA)
        CALL write_grad_internal(td_ep, tl_ncfile)
        rla_plot_data => td_ep%ra_grad
      CASE DEFAULT
        WRITE(*,*) 'In write_ep_data; bad data type or no adequate routine :', id_dType
        WRITE(*,*) '  Stopping the program ...'
        STOP
    END SELECT
    CALL ncEPclose(tl_ncfile)
    
    IF( ASSOCIATED(rla_plot_data) )THEN
      CALL write_vector_for_plot(rla_plot_data, ala_fName)
      SELECT CASE(id_dType)
        CASE (CTL_DATA, ACTL_DATA)
          DEALLOCATE( rla_plot_data )
        CASE DEFAULT
          !nothing
      END SELECT
    END IF
    
  END SUBROUTINE write_ep_data

  !> \brief Print the control vector and its bounds
  !! \param[in] td_ep, exchange parameter
  !<
  SUBROUTINE print_ctl_bound(td_ep)
    TYPE(exchange_param), INTENT(IN) :: td_ep
    INTEGER :: ibi
    
    WRITE(*,*)"print_ctl_bound: control vector and its bounds"
    WRITE(*,*)"  idx left_bound control_val right_bound"
    WRITE(*,*)"--------------------------------------------"
    DO ibi = 1, SIZE(td_ep%ra_ctl)
       WRITE(*, FMT=IFORMAT, ADVANCE='NO') ibi
       IF( td_ep%la_check_ctl_lb(ibi) )THEN
          WRITE(*, FMT=RFORMAT, ADVANCE='NO') td_ep%ra_ctl_lb(ibi)
       ELSE
          WRITE(*, FMT=AFORMAT, ADVANCE='NO') '     -      '
       END IF
       WRITE(*, FMT=RFORMAT, ADVANCE='NO') td_ep%ra_b_ctl(ibi) + td_ep%ra_ctl(ibi)
       IF( td_ep%la_check_ctl_ub(ibi) )THEN
          WRITE(*, FMT=RFORMAT) td_ep%ra_ctl_ub(ibi)
       ELSE
          WRITE(*, FMT=AFORMAT) '     -      '
       END IF
    END DO
    WRITE(*,*)"--------------------------------------------"
  END SUBROUTINE print_ctl_bound
  
  !>\brief print exchange parameters for diagnostics
  SUBROUTINE print_ep(td_ep)
    TYPE(exchange_param), INTENT(IN) :: td_ep
    CHARACTER(LEN=80)   :: array_rformat
    array_rformat = "(A,"//TRIM( NUM2STR(td_ep%i_nctl) )//RFORMAT//")"
    
    WRITE(*,                 *) 'com_tools::print_ep : exchange_param-----------------------'
    CALL print_var(td_ep%aa_solver_action     , '  Action to be taken (action)    = ')
    CALL print_var(td_ep%aa_solver_path, '  Path to the external solver    = ')
    CALL print_var(td_ep%input_dir     , '  Input directory .............. = ')
    CALL print_var(td_ep%output_dir    , '  Output directory ............  = ')
    CALL print_var(td_ep%dmt_fName     , '  Direct model output file name. = ')
    CALL print_var(td_ep%obs_fName     , '  Observation file name......... = ')
    CALL print_var(td_ep%ogap_fName    , '  Obsgap file Name ............. = ')
    CALL print_var(td_ep%ctl_fName     , '  CTL file Name ................ = ')
    CALL print_var(td_ep%bctl_fName    , '  background ctl file name...... = ')
    CALL print_var(td_ep%r_mu          , '  Diffusion coefficient (mu)     = ')
    CALL print_var(td_ep%r_sigma       , '  Bell shape sigma      (sigma)  = ')
    CALL print_var(td_ep%r_v0          , '  Vvelocity parameter    (v0)    = ')
    CALL print_var(td_ep%r_omega       , '  Velocity oscillation  (omega)  = ')
    
    CALL print_var(td_ep%l_useGD       , '  Use GD projection?             = ')
    CALL print_var(td_ep%r_wGD         , '  GD weight                      = ')
    CALL print_var(td_ep%r_wb          , '  Weighting param for the bg     = ')
    CALL print_var(td_ep%r_wGrad       , '  Weighting param for grad regul = ')
    
    CALL print_var(td_ep%l_amplitude   , '  Bell shape amplitude (amplitu) = ')
    CALL print_var(td_ep%l_location    , '  Bell shape location (location) = ')
    CALL print_var(td_ep%l_sigma       , '  B. shape sigma control?        = ')
    CALL print_var(td_ep%i_obs_level   , '  Observation level              = ')
    CALL print_var(td_ep%i_nobs_x      , '  Obs count along x direction    = ')
    CALL print_var(td_ep%i_nobs_t      , '  Obs count along time direction = ')
    CALL print_var(td_ep%r_sigmaR      , '  Obs standard deviation(sigmaR) = ')
    CALL print_var(td_ep%i_nctl        , '  Size of the control vector     = ')
    CALL print_var(td_ep%r_cost        , '  Value of the cost function     = ')
    CALL print_var(td_ep%r_max_coef    , '  max value for rand coef        = ')
    CALL print_var(td_ep%r_nz_ratio    , '  ratio of nonzero coef          = ')
    CALL print_var(td_ep%r_mes_fact    , '  mult fact for measurements     = ')
    CALL print_var(td_ep%r_cs_reg      , '  regularization parameter       = ')    
    CALL print_var(td_ep%ra_ctl        , '  Control vector           (ctl) = ')
    CALL print_var(td_ep%ra_grad       , '  Gradient of the cost fnc(grad) = ')
    CALL print_var(td_ep%ra_b_ctl      , '  Background of C.vector (b_ctl) = ')
    CALL print_var(td_ep%ra_sigma_ctl  , '  ctl STD            (sigma_ctl) = ')
    
    
    CALL print_var(td_ep%l_first_simul , ' First simulation?              = ')
    CALL print_var(td_ep%l_run_from_ctl ,' Run from CTL?                  = ')
    CALL print_var(td_ep%l_restart_from_previous , ' Restart from previous simulation?              = ')
    CALL print_var(td_ep%l_simul_diverged , ' Did the simulation diverged?              = ')
  END SUBROUTINE print_ep
  
  !
  !********************************** observation !!!!!!!!!!!!!!!!!!!!!!!!!!
  !

	!> \brief read observations from file
  !! \param[in, out] td_os, observation structure, 
  !! contains the name of the file and the dimension (type) of the problem as input and the read params as output
  !<
  SUBROUTINE read_obs(td_os)
    TYPE(obs_structure), INTENT(INOUT) :: td_os
    type(ObsOutput) :: tl_ncfile
    
    CALL initObsOutput( tl_ncfile, td_os%obs_fName )
    CALL ncObsOpen( tl_ncfile )
    CALL ncObsRead( tl_ncfile, td_os, rec=1 )

    td_os%i_obs_level  = tl_ncfile%i_obs_level
    td_os%ia_M_vector  = tl_ncfile%ia_M_vector
    td_os%i_nobs = tl_ncfile%i_nobs
    td_os%i_ndim = tl_ncfile%i_ndim
    
    td_os%ra_Rm1 = 0.0_cp
    WHERE( td_os%ra_sigma > 0.0_cp ) td_os%ra_Rm1 = 1/td_os%ra_sigma**2
    CALL ncObsClose( tl_ncfile )
    !WRITE(*,*) 'In read_obs: end of reading'
  END SUBROUTINE read_obs
  
  !> \brief read obsgap from file
  !! \param[in, out] td_os, observation structure, 
  !! contains the name of the file and the dimension (type) of the problem as input
  !! and the read params as output
  !<
  SUBROUTINE read_obsgap(td_os)
    TYPE(obs_structure), INTENT(INOUT) :: td_os
    type(ObsOutput) :: tl_ncfile
    INTEGER :: il_ogap

    CALL initObsOutput( tl_ncfile, td_os%ogap_fName )
    CALL ncObsOpen( tl_ncfile )
    CALL ncObsRead( tl_ncfile, td_os, rec=1, ogap=il_ogap )
		td_os%i_obs_level = tl_ncfile%i_obs_level
		td_os%ia_M_vector = tl_ncfile%ia_M_vector
    td_os%i_nobs = tl_ncfile%i_nobs
    td_os%i_ndim = tl_ncfile%i_ndim
    
    CALL ncObsClose( tl_ncfile )
  END SUBROUTINE read_obsgap
  
  !> \brief write observations in the friendly-plot format
  !! \param[in] td_os, observation structure
  !<
  SUBROUTINE write_obs_for_plot(td_os)
    INTEGER, PARAMETER :: ip_numnam = 79
    TYPE(obs_structure), INTENT(INOUT) :: td_os
    TYPE(dyn_rVector), DIMENSION(2) :: tla_save
    
    IF(td_os%l_icoord)THEN
      tla_save(1) = REAL(td_os%ia_icoord(1, :), cp)
    ELSE
      tla_save(1) = td_os%ra_rcoord(1, :)
    END IF
    tla_save(2) = td_os%ra_obs
    
    CALL save_trj( tla_save, TRIM( make_pFileName(td_os%obs_fName) ), ld_column = .TRUE. )
    CALL reset_drv_array( tla_save )
  END SUBROUTINE write_obs_for_plot
  
  !> \brief write observations to file
  !! \param[in] td_os, observation structure
  !<
  SUBROUTINE write_obs(td_os, ada_title)
    TYPE(obs_structure), INTENT(INOUT) :: td_os
    type(ObsOutput) :: tl_ncfile
    CHARACTER(LEN=*), OPTIONAL :: ada_title
    CHARACTER(LEN=ip_snl)      :: ala_title

		IF ( PRESENT(ada_title) )THEN
			ala_title = TRIM(ada_title)
		ELSE
			ala_title = 'Observation (use the second param in call to write_obs for precise title)'
		END IF

    td_os%i_nobs  = SIZE(td_os%ra_obs)!important
    CALL initObsOutput(&
        tl_ncfile, td_os%obs_fName, id_nobs=td_os%i_nobs, &
        id_ndim=td_os%i_ndim &
    )
    tl_ncfile%aa_title   = ala_title
    tl_ncfile%aa_history = make_ncHistory("COM_TOOLS::write_obs")
		tl_ncfile%i_obs_level = td_os%i_obs_level
		tl_ncfile%ia_M_vector = td_os%ia_M_vector
    CALL ncObsCreate(tl_ncfile)
    CALL ncObsWrite(tl_ncfile, td_os)
    CALL ncObsClose(tl_ncfile)
    !writing obs to the friendly plotting format
    CALL write_obs_for_plot(td_os)
  END SUBROUTINE write_obs
  
  !> \brief write observations to file
  !! \param[in] td_os, observation structure
  !! \param[in] ada_title, title of the file, to be saved in the netcdf
  !<
  SUBROUTINE write_obsgap(td_os, ada_title)
    TYPE(obs_structure), INTENT(INOUT) :: td_os
    type(ObsOutput) :: tl_ncfile
    CHARACTER(LEN=*), OPTIONAL :: ada_title
    CHARACTER(LEN=ip_snl)      :: ala_title
    INTEGER :: il_ogap

		IF ( PRESENT(ada_title) )THEN
			ala_title = TRIM(ada_title)
		ELSE
			ala_title = "Obsgap"
		END IF
    td_os%i_nobs  = SIZE(td_os%ra_obs)!important
    CALL initObsOutput(&
        tl_ncfile, td_os%ogap_fName, id_nobs=td_os%i_nobs, &
        id_ndim=td_os%i_ndim &
    )
    tl_ncfile%aa_title   = ala_title
    tl_ncfile%aa_history = make_ncHistory("COM_TOOLS::write_obsgap")
		tl_ncfile%i_obs_level = td_os%i_obs_level
		tl_ncfile%ia_M_vector = td_os%ia_M_vector
    CALL ncObsCreate(tl_ncfile)
    CALL ncObsWrite(tl_ncfile, td_os, ogap=il_ogap)
    CALL ncObsClose(tl_ncfile)

  END SUBROUTINE write_obsgap

END MODULE com_tools
