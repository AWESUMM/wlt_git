!>
!!
!<
MODULE debug_tools
USE general_constant
IMPLICIT NONE

  LOGICAL, PARAMETER :: LP_DEBUG = .TRUE.
  CHARACTER (LEN=*), PARAMETER :: SCREEN = "SCREEN"!> Screen file name for output

  !> \brief format to print integer
  !<
  CHARACTER(LEN=*), PARAMETER :: IFORMAT = "(I5)"
  CHARACTER(LEN=*), PARAMETER :: IFMT = IFORMAT
  !> \brief format to print real
  !<
  CHARACTER(LEN=*), PARAMETER :: RFORMAT = "(ES13.5E2)"
  !> \brief format to print ascii (characters string)
  !<
  CHARACTER(LEN=*), PARAMETER :: AFORMAT = "(A)"
  !> \brief format to print integer with comment, debug
  !<
  CHARACTER(LEN=*), PARAMETER :: DEBUG_IFORMAT = "(A,"//IFORMAT//")"
  !> \brief format to print logical value with comment, debug
  !<
  CHARACTER(LEN=*), PARAMETER :: DEBUG_LFORMAT = "(A,L)"
  !> \brief format to print character value with comment, debug
  !<
  CHARACTER(LEN=*), PARAMETER :: DEBUG_AFORMAT = "(A,A)"
  !> \brief format to print real with comment, debug
  !<
  CHARACTER(LEN=*), PARAMETER :: DEBUG_RFORMAT = "(A,"//RFORMAT//")"
  
  INTERFACE NUM2STR
     MODULE PROCEDURE itoa!> Integer to ASCII
     MODULE PROCEDURE rtoa!> Real single precision to ASCII
     MODULE PROCEDURE dtoa!> Real double precision to ASCII
  END INTERFACE
  
  !> \brief Debug routines, used to print values of variables with a predefined comment
  !! The goal is to make it possible to activate or deactivate the printing with a global flag
  !<
  INTERFACE DEBUG
     MODULE PROCEDURE debug_string
     MODULE PROCEDURE debug_string_val
     MODULE PROCEDURE debug_integer
     MODULE PROCEDURE debug_sp_real
     MODULE PROCEDURE debug_dp_real
     MODULE PROCEDURE debug_logical
     MODULE PROCEDURE debug_integer_array1D
     MODULE PROCEDURE debug_integer_array2D
     MODULE PROCEDURE debug_sp_real_array1D
     MODULE PROCEDURE debug_sp_real_array2D
     MODULE PROCEDURE debug_dp_real_array1D
     MODULE PROCEDURE debug_dp_real_array2D
  END INTERFACE
  
  !> \brief Print value of variables with a predefined description
  !<
  INTERFACE PRINT_VAR
     MODULE PROCEDURE print_string
     MODULE PROCEDURE print_integer
     MODULE PROCEDURE print_sp_real
     MODULE PROCEDURE print_dp_real
     MODULE PROCEDURE print_logical
     MODULE PROCEDURE print_integer_array1D
     MODULE PROCEDURE print_integer_array2D
     MODULE PROCEDURE print_sp_real_array1D
     MODULE PROCEDURE print_sp_real_array2D
     MODULE PROCEDURE print_dp_real_array1D
     MODULE PROCEDURE print_dp_real_array2D
  END INTERFACE

  INTERFACE NOTHING
    !MODULE PROCEDURE nothing_string
    !MODULE PROCEDURE nothing_integer
    MODULE PROCEDURE nothing_sp_real
    MODULE PROCEDURE nothing_dp_real
    !MODULE PROCEDURE nothing_logical
    !MODULE PROCEDURE nothing_integer_array1D
    !MODULE PROCEDURE nothing_integer_array2D
    MODULE PROCEDURE nothing_sp_real_array1D
    !MODULE PROCEDURE nothing_sp_real_array2D
    MODULE PROCEDURE nothing_dp_real_array1D
    !MODULE PROCEDURE nothing_dp_real_array2D
    MODULE PROCEDURE nothing_isd_array1D!for the simulator
  END INTERFACE

 !> \brief Check for Nan values
 !<
 INTERFACE isNaN
    MODULE PROCEDURE isnan_sp_real
    MODULE PROCEDURE isnan_dp_real
    MODULE PROCEDURE isnan_sp_real_array1D
    MODULE PROCEDURE isnan_dp_real_array1D
 END INTERFACE

 !> \brief Count Nan values in an array
 !<
 INTERFACE NaNCount
    MODULE PROCEDURE nanCount_sp_real_array1D
    MODULE PROCEDURE nanCount_dp_real_array1D
 END INTERFACE

 !> \brief Check for Infinity values
 !<
 INTERFACE isInf
    MODULE PROCEDURE isinf_sp_real
    MODULE PROCEDURE isinf_dp_real
    MODULE PROCEDURE isinf_sp_real_array1D
    MODULE PROCEDURE isinf_dp_real_array1D
 END INTERFACE

 !> \brief Count Inf values in an array
 !<
 INTERFACE InfCount
    MODULE PROCEDURE infCount_sp_real_array1D
    MODULE PROCEDURE infCount_dp_real_array1D
 END INTERFACE

CONTAINS

  !>pause function
  SUBROUTINE dpause(ada_val)
    CHARACTER(LEN=*), INTENT(IN), OPTIONAL :: ada_val
    IF( PRESENT(ada_val) )THEN
       CALL print_var(' - press <Enter> key to continue', ada_val)
    ELSE
       CALL print_var('', 'Pause: press <Enter> key to continue')
    END IF
    READ(*,*)
  END SUBROUTINE dpause

 !> \brief Check if a simple precision value is NaN
 FUNCTION isnan_sp_real(rd_val)RESULT(ll_isnan)
   REAL(sp), INTENT(IN) :: rd_val
   LOGICAL :: ll_isnan

   ll_isnan = ( (rd_val>0.0_sp).EQV.(rd_val<=0.0_sp) )
 END FUNCTION isnan_sp_real

 !> \brief Check if a double precision value is NaN
 FUNCTION isnan_dp_real(rd_val)RESULT(ll_isnan)
   REAL(dp), INTENT(IN) :: rd_val
   LOGICAL :: ll_isnan

   ll_isnan = ( (rd_val>0.0_dp).EQV.(rd_val<=0.0_dp) )
 END FUNCTION isnan_dp_real

 !> \brief Check if a simple precision array is NaN
 FUNCTION isnan_sp_real_array1D(rda_val)RESULT(lla_isnan)
   REAL(sp), DIMENSION(:), INTENT(IN) :: rda_val
   LOGICAL,  DIMENSION(SIZE(rda_val)) :: lla_isnan
   INTEGER :: ibi

   DO ibi=1,SIZE(rda_val)
      lla_isnan(ibi) = ( (rda_val(ibi)>0.0_sp).EQV.(rda_val(ibi)<=0.0_sp) )
   END DO
 END FUNCTION isnan_sp_real_array1D

 !> \brief Check if a double precision array is NaN
 FUNCTION isnan_dp_real_array1D(rda_val)RESULT(lla_isnan)
   REAL(dp), DIMENSION(:), INTENT(IN) :: rda_val
   LOGICAL,  DIMENSION(SIZE(rda_val)) :: lla_isnan
   INTEGER :: ibi

   DO ibi=1,SIZE(rda_val)
      lla_isnan(ibi) = ( (rda_val(ibi)>0.0_dp).EQV.(rda_val(ibi)<=0.0_dp) )
   END DO
 END FUNCTION isnan_dp_real_array1D

 !> \brief Count NaN values in a simple precision array
 FUNCTION nanCount_sp_real_array1D(rda_val)RESULT(il_count)
   REAL(sp), DIMENSION(:), INTENT(IN) :: rda_val
   INTEGER :: ibi, il_count

   il_count = 0
   DO ibi=1,SIZE(rda_val)
      IF ( (rda_val(ibi)>0.0_sp).EQV.(rda_val(ibi)<=0.0_sp) )THEN
         il_count = il_count + 1
         !PRINT*, 'In InfCount: Value at ', ibi, ' is ', rda_val(ibi)
      END IF
   END DO
 END FUNCTION nanCount_sp_real_array1D

 !> \brief Count NaN values in a simple precision array
 FUNCTION nanCount_dp_real_array1D(rda_val)RESULT(il_count)
   REAL(dp), DIMENSION(:), INTENT(IN) :: rda_val
   INTEGER :: ibi, il_count

   il_count = 0
   DO ibi=1,SIZE(rda_val)
      IF ( (rda_val(ibi)>0.0_dp).EQV.(rda_val(ibi)<=0.0_dp) )THEN
         il_count = il_count + 1
         !PRINT*, 'In InfCount: Value at ', ibi, ' is ', rda_val(ibi)
      END IF
   END DO
 END FUNCTION nanCount_dp_real_array1D

 !> \brief Check if a simple precision value is Inf
 FUNCTION isinf_sp_real(rd_val)RESULT(ll_isinf)
   REAL(sp), INTENT(IN) :: rd_val
   LOGICAL :: ll_isinf

   ll_isinf = ( (rd_val+1.0)==(2.0*rd_val) )
 END FUNCTION isinf_sp_real

 !> \brief Check if a double precision value is Inf
 FUNCTION isinf_dp_real(rd_val)RESULT(ll_isinf)
   REAL(dp), INTENT(IN) :: rd_val
   LOGICAL :: ll_isinf

   ll_isinf = ( (rd_val+1.0)==(2.0*rd_val) )
 END FUNCTION isinf_dp_real

 !> \brief Check if a simple precision array is Inf
 FUNCTION isinf_sp_real_array1D(rda_val)RESULT(lla_isinf)
   REAL(sp), DIMENSION(:), INTENT(IN) :: rda_val
   LOGICAL,  DIMENSION(SIZE(rda_val)) :: lla_isinf
   INTEGER :: ibi

   DO ibi=1,SIZE(rda_val)
      lla_isinf(ibi) = ( ( rda_val(ibi)+1.0_sp==2.0_sp*rda_val(ibi)).AND.( rda_val(ibi)+2.0_sp==2.0_sp*rda_val(ibi) ) )
   END DO
 END FUNCTION isinf_sp_real_array1D

 !> \brief Check if a double precision array is Inf
 FUNCTION isinf_dp_real_array1D(rda_val)RESULT(lla_isinf)
   REAL(dp), DIMENSION(:), INTENT(IN) :: rda_val
   LOGICAL,  DIMENSION(SIZE(rda_val)) :: lla_isinf
   INTEGER :: ibi

   DO ibi=1,SIZE(rda_val)
      lla_isinf(ibi) = ( ( rda_val(ibi)+1.0_dp==2.0_dp*rda_val(ibi) ).AND.( rda_val(ibi)+2.0_dp==2.0_dp*rda_val(ibi) ) )
   END DO
 END FUNCTION isinf_dp_real_array1D

 !> \brief Count Inf values in a simple precision array
 FUNCTION infCount_sp_real_array1D(rda_val)RESULT(il_count)
   REAL(sp), DIMENSION(:), INTENT(IN) :: rda_val
   INTEGER :: ibi, il_count

   il_count = 0
   DO ibi=1,SIZE(rda_val)
      IF ( ( rda_val(ibi)+1.0_sp==2.0_sp*rda_val(ibi)).AND.( rda_val(ibi)+2.0_sp==2.0_sp*rda_val(ibi) ) ) THEN
         il_count = il_count + 1
         !PRINT*, 'In InfCount: Value at ', ibi, ' is ', rda_val(ibi)
      END IF
   END DO
 END FUNCTION infCount_sp_real_array1D

 !> \brief Count Inf values in a simple precision array
 FUNCTION infCount_dp_real_array1D(rda_val)RESULT(il_count)
   REAL(dp), DIMENSION(:), INTENT(IN) :: rda_val
   INTEGER :: ibi, il_count

   il_count = 0
   DO ibi=1,SIZE(rda_val)
      IF ( ( rda_val(ibi)+1.0_dp==2.0_dp*rda_val(ibi) ).AND.( rda_val(ibi)+2.0_dp==2.0_dp*rda_val(ibi) ) ) THEN
         il_count = il_count + 1
         !PRINT*, 'In InfCount: Value at ', ibi, ' is ', rda_val(ibi)
      END IF
   END DO
 END FUNCTION infCount_dp_real_array1D
 
  
  FUNCTION itoa(id_val) RESULT(ala_val)
    INTEGER, INTENT(IN) :: id_val
    CHARACTER(LEN=ip_snl)   :: ala_val
    
    WRITE(ala_val, *) id_val
    ala_val = ADJUSTL(ala_val)
  END FUNCTION itoa
  
  FUNCTION rtoa(rd_val) RESULT(ala_val)
    REAL(sp), INTENT(IN) :: rd_val
    CHARACTER(LEN=ip_snl)   :: ala_val
    
    WRITE(ala_val, *) rd_val
    ala_val = ADJUSTL(ala_val)
  END FUNCTION rtoa
  
  !>
  FUNCTION dtoa(rd_val) RESULT(ala_val)
    REAL(dp), INTENT(IN) :: rd_val
    CHARACTER(LEN=ip_snl)   :: ala_val
    
    WRITE(ala_val, *) rd_val
    ala_val = ADJUSTL(ala_val)
  END FUNCTION dtoa  
  
  !>\brief Write 2D array to file
  !<
  SUBROUTINE myio_write_matrix(fileName, rda_A)
    REAL(KIND=dp), DIMENSION(:, :), INTENT(IN) :: rda_A
    CHARACTER(LEN=*)         , INTENT(IN) :: fileName
    INTEGER ib_i, ib_j, il_ios
    INTEGER :: ip_fid = 41

    IF(fileName==SCREEN) THEN
      ip_fid = 6!standard output
      PRINT*, 'rows count = ', size(rda_A, 1)
      PRINT*, 'columns count = ', size(rda_A, 2)
    ELSE
      ip_fid = 41
      OPEN( UNIT = ip_fid, FILE = fileName, STATUS = 'REPLACE', FORM = 'FORMATTED', IOSTAT = il_ios)
      IF (il_ios /= 0) then
        WRITE(* , *) 'Error creating file', fileName
        STOP;
      END IF
    END IF
    PRINT*, "In writeMatrix", LBOUND(rda_A,1), UBOUND(rda_A,1), LBOUND(rda_A,2), UBOUND(rda_A,2)
    !stop
    WRITE(unit=ip_fid, fmt=IFMT) size(rda_A, 1)
    WRITE(unit=ip_fid, fmt=IFMT) size(rda_A, 2)
    DO ib_j = LBOUND(rda_A,2), UBOUND(rda_A,2)
      DO ib_i = LBOUND(rda_A,1), UBOUND(rda_A,1)
        WRITE(unit=ip_fid, fmt='(E18.8)', advance="no") rda_A(ib_i,ib_j)!"no" to avoid end of line
      END DO
      !WRITE(unit=ip_fid, fmt=*)''! just for adding end of line
    END DO
    IF(fileName/=SCREEN) THEN
      CLOSE(ip_fid )
    END IF
  END SUBROUTINE myio_write_matrix

  SUBROUTINE myio_read_matrix(fileName, rda_A)
    REAL(KIND=dp), DIMENSION(:, :), INTENT(OUT) :: rda_A
    CHARACTER(LEN=*)         , INTENT(IN)  :: fileName
    INTEGER ib_i, ib_j, il_nbRow, il_nbCol, il_ios
    INTEGER , PARAMETER :: ip_fid =615
      
    OPEN( UNIT = ip_fid, FILE = fileName, IOSTAT = il_ios)
    IF (il_ios /= 0) then
      WRITE(* , *) 'Error opening file', fileName
      STOP;
    END IF
    !stop
    READ(UNIT=ip_fid, FMT=IFMT) il_nbRow
    READ(UNIT=ip_fid, FMT=IFMT) il_nbCol
    PRINT*, "In readMatrix, il_nbRow = ", il_nbRow, "; il_nbCol = ", il_nbCol
    DO ib_j = 1, il_nbCol
       DO ib_i = 1, il_nbRow 
          READ(UNIT=ip_fid, FMT='(E18.8)', advance="no") rda_A(ib_i,ib_j)!"no" to avoid going to the next line
          PRINT*, 'row, col', ib_i, ib_j, ' : ', rda_A(ib_i,ib_j)
       END DO
       !READ(unit=ip_fid, fmt=*) !just to skip the end of line
    END DO
    CLOSE(ip_fid )
  END SUBROUTINE myio_read_matrix

  !!read the head of the file: nbRow and nbCol
  SUBROUTINE  myio_readInfo(fileName, id_nbRow, id_nbCol)
    INTEGER , PARAMETER :: ip_fid =616
    CHARACTER(LEN=*)         , INTENT(IN)  :: fileName
    INTEGER, INTENT(OUT) :: id_nbRow, id_nbCol
    INTEGER :: il_ios
    
    OPEN( UNIT = ip_fid, FILE = fileName, IOSTAT = il_ios)
    IF (il_ios /= 0) then
      WRITE(* , *) 'In readInfo : Error opening file', fileName
      STOP;
    END IF
    READ(UNIT=ip_fid, FMT=IFMT) id_nbRow
    READ(UNIT=ip_fid, FMT=IFMT) id_nbCol
    PRINT*, "In readInfo, id_nbRow = ", id_nbRow, "; id_nbCol = ", id_nbCol
    CLOSE(ip_fid )
  END SUBROUTINE myio_readInfo
  
!print_var routines
  SUBROUTINE print_string(ada_val, ada_description)
    CHARACTER(LEN=*), INTENT(IN) :: ada_val
    CHARACTER(LEN=*), INTENT(IN) :: ada_description
    
    WRITE(*, FMT=DEBUG_AFORMAT) ada_description,ada_val
  END SUBROUTINE print_string
  
  SUBROUTINE print_integer(id_val, ada_description)
    INTEGER, INTENT(IN) :: id_val
    CHARACTER(LEN=*), INTENT(IN) :: ada_description
    
    WRITE(*, FMT=DEBUG_IFORMAT) ada_description, id_val
  END SUBROUTINE print_integer
  
  SUBROUTINE print_sp_real(rd_val, ada_description)
    REAL(sp), INTENT(IN) :: rd_val
    CHARACTER(LEN=*), INTENT(IN) :: ada_description
    
    WRITE(*, FMT=DEBUG_RFORMAT) ada_description, rd_val
  END SUBROUTINE print_sp_real
  
  SUBROUTINE print_dp_real(rd_val, ada_description)
    REAL(dp), INTENT(IN) :: rd_val
    CHARACTER(LEN=*), INTENT(IN) :: ada_description
    
    WRITE(*, FMT=DEBUG_RFORMAT) ada_description, rd_val
  END SUBROUTINE print_dp_real
  
  SUBROUTINE print_logical(ld_val, ada_description)
    LOGICAL, INTENT(IN) :: ld_val
    CHARACTER(LEN=*), INTENT(IN) :: ada_description
    
    WRITE(*, FMT=DEBUG_LFORMAT) ada_description, ld_val
  END SUBROUTINE print_logical
  
  SUBROUTINE print_integer_array1D(ida_val, ada_description)
    INTEGER, DIMENSION(:), INTENT(IN) :: ida_val
    CHARACTER(LEN=*), INTENT(IN) :: ada_description
    CHARACTER(LEN=80)   :: array_format

    array_format = "(A,"//TRIM( NUM2STR(SIZE(ida_val)) )//IFORMAT//")"
    
    WRITE(*, FMT=array_format) ada_description, ida_val
  END SUBROUTINE print_integer_array1D
  
  SUBROUTINE print_sp_real_array1D(rda_val, ada_description)
    REAL(sp), DIMENSION(:), INTENT(IN) :: rda_val
    CHARACTER(LEN=*), INTENT(IN) :: ada_description
    CHARACTER(LEN=80)   :: array_format

    array_format = "(A,"//TRIM( NUM2STR(SIZE(rda_val)) )//RFORMAT//")"
    
    WRITE(*, FMT=array_format) ada_description, rda_val
  END SUBROUTINE print_sp_real_array1D
  
  SUBROUTINE print_dp_real_array1D(rda_val, ada_description)
    REAL(dp), DIMENSION(:), INTENT(IN) :: rda_val
    CHARACTER(LEN=*), INTENT(IN) :: ada_description
    CHARACTER(LEN=80)   :: array_format

    array_format = "(A,"//TRIM( NUM2STR(SIZE(rda_val)) )//RFORMAT//")"
    
    WRITE(*, FMT=array_format) ada_description, rda_val
  END SUBROUTINE print_dp_real_array1D
  
  SUBROUTINE print_integer_array2D(ida_val, ada_description)
    INTEGER, DIMENSION(:,:), INTENT(IN) :: ida_val
    CHARACTER(LEN=*), INTENT(IN) :: ada_description
    CHARACTER(LEN=80)   :: array_format
    INTEGER :: ibi

    array_format = "(A,"//TRIM( NUM2STR(SIZE(ida_val,2)) )//IFORMAT//")"
    WRITE(*,*)ada_description
    DO ibi = 1, SIZE(ida_val,1)
       WRITE(*, FMT=array_format)'', ida_val(ibi, :)
    END DO
  END SUBROUTINE print_integer_array2D
  
  SUBROUTINE print_sp_real_array2D(rda_val, ada_description)
    REAL(sp), DIMENSION(:,:), INTENT(IN) :: rda_val
    CHARACTER(LEN=*), INTENT(IN) :: ada_description
    CHARACTER(LEN=80)   :: array_format
    INTEGER :: ibi

    array_format = "(A,"//TRIM( NUM2STR(SIZE(rda_val,2)) )//RFORMAT//")"
    WRITE(*,*)ada_description
    DO ibi = 1, SIZE(rda_val,1)
       WRITE(*, FMT=array_format)'', rda_val(ibi, :)
    END DO
  END SUBROUTINE print_sp_real_array2D
  
  SUBROUTINE print_dp_real_array2D(rda_val, ada_description)
    REAL(dp), DIMENSION(:,:), INTENT(IN) :: rda_val
    CHARACTER(LEN=*), INTENT(IN) :: ada_description
    CHARACTER(LEN=80)   :: array_format
    INTEGER :: ibi

    array_format = "(A,"//TRIM( NUM2STR(SIZE(rda_val,2)) )//RFORMAT//")"
    WRITE(*,*)ada_description
    DO ibi = 1, SIZE(rda_val,1)
       WRITE(*, FMT=array_format)'', rda_val(ibi, :)
    END DO
  END SUBROUTINE print_dp_real_array2D
  
!debug routines
  SUBROUTINE debug_string_val(ada_val)
    CHARACTER(LEN=*), INTENT(IN) :: ada_val
    
    IF(LP_DEBUG) CALL print_string('', ada_val)
  END SUBROUTINE debug_string_val
  
  SUBROUTINE debug_string(ada_val, ada_description)
    CHARACTER(LEN=*), INTENT(IN) :: ada_val
    CHARACTER(LEN=*), INTENT(IN) :: ada_description
    
    IF(LP_DEBUG) CALL print_string(ada_val, ada_description)
  END SUBROUTINE debug_string
  
  SUBROUTINE debug_integer(id_val, ada_description)
    INTEGER, INTENT(IN) :: id_val
    CHARACTER(LEN=*), INTENT(IN) :: ada_description
    
    IF(LP_DEBUG) CALL print_integer(id_val, ada_description)
  END SUBROUTINE debug_integer
  
  SUBROUTINE debug_sp_real(rd_val, ada_description)
    REAL(sp), INTENT(IN) :: rd_val
    CHARACTER(LEN=*), INTENT(IN) :: ada_description
    
    IF(LP_DEBUG) CALL print_sp_real(rd_val, ada_description)
  END SUBROUTINE debug_sp_real
  
  SUBROUTINE debug_dp_real(rd_val, ada_description)
    REAL(dp), INTENT(IN) :: rd_val
    CHARACTER(LEN=*), INTENT(IN) :: ada_description
    
    IF(LP_DEBUG) CALL print_dp_real(rd_val, ada_description)
  END SUBROUTINE debug_dp_real
  
  SUBROUTINE debug_logical(ld_val, ada_description)
    LOGICAL, INTENT(IN) :: ld_val
    CHARACTER(LEN=*), INTENT(IN) :: ada_description
    
    IF(LP_DEBUG) CALL print_logical(ld_val, ada_description)
  END SUBROUTINE debug_logical
  
  SUBROUTINE debug_integer_array1D(ida_val, ada_description)
    INTEGER, DIMENSION(:), INTENT(IN) :: ida_val
    CHARACTER(LEN=*), INTENT(IN) :: ada_description

    IF(LP_DEBUG) CALL print_integer_array1D(ida_val, ada_description)
  END SUBROUTINE debug_integer_array1D
  
  SUBROUTINE debug_sp_real_array1D(rda_val, ada_description)
    REAL(sp), DIMENSION(:), INTENT(IN) :: rda_val
    CHARACTER(LEN=*), INTENT(IN) :: ada_description
    
    IF(LP_DEBUG) CALL print_sp_real_array1D(rda_val, ada_description)
  END SUBROUTINE debug_sp_real_array1D
  
  SUBROUTINE debug_dp_real_array1D(rda_val, ada_description)
    REAL(dp), DIMENSION(:), INTENT(IN) :: rda_val
    CHARACTER(LEN=*), INTENT(IN) :: ada_description
    
    IF(LP_DEBUG) CALL print_dp_real_array1D(rda_val, ada_description)
  END SUBROUTINE debug_dp_real_array1D
  
  SUBROUTINE debug_integer_array2D(ida_val, ada_description)
    INTEGER, DIMENSION(:,:), INTENT(IN) :: ida_val
    CHARACTER(LEN=*), INTENT(IN) :: ada_description

    IF(LP_DEBUG) CALL print_integer_array2D(ida_val, ada_description)
  END SUBROUTINE debug_integer_array2D
  
  SUBROUTINE debug_sp_real_array2D(rda_val, ada_description)
    REAL(sp), DIMENSION(:,:), INTENT(IN) :: rda_val
    CHARACTER(LEN=*), INTENT(IN) :: ada_description

    IF(LP_DEBUG) CALL print_sp_real_array2D(rda_val, ada_description)
  END SUBROUTINE debug_sp_real_array2D
  
  SUBROUTINE debug_dp_real_array2D(rda_val, ada_description)
    REAL(dp), DIMENSION(:,:), INTENT(IN) :: rda_val
    CHARACTER(LEN=*), INTENT(IN) :: ada_description
    
    IF(LP_DEBUG) CALL print_dp_real_array2D(rda_val, ada_description)
  END SUBROUTINE debug_dp_real_array2D

  !NOTHINg interface
  SUBROUTINE nothing_sp_real(rd_val)
    REAL(sp), INTENT(IN) :: rd_val
    REAL(dp) :: rl_tmp
    rl_tmp = REAL(rd_val, dp)
  END SUBROUTINE nothing_sp_real
  
  SUBROUTINE nothing_dp_real(rd_val)
    REAL(dp), INTENT(IN) :: rd_val
    REAL(dp) :: rl_tmp
    rl_tmp = REAL(rd_val, dp)
  END SUBROUTINE nothing_dp_real

  SUBROUTINE nothing_sp_real_array1D(rda_val)
    REAL(sp), DIMENSION(:), INTENT(IN) :: rda_val
    REAL(dp) :: rl_tmp
    rl_tmp = REAL(rda_val(1), dp)
  END SUBROUTINE nothing_sp_real_array1D

  SUBROUTINE nothing_dp_real_array1D(rda_val)
    REAL(dp), DIMENSION(:), INTENT(IN) :: rda_val
    REAL(dp) :: rl_tmp
    rl_tmp = REAL(rda_val(1), dp)
  END SUBROUTINE nothing_dp_real_array1D

  SUBROUTINE nothing_isd_array1D(ida_int, rda_sp, rda_dp)
    INTEGER, DIMENSION(:) :: ida_int
    REAL(dp), DIMENSION(:), INTENT(IN) :: rda_dp
    REAL(sp), DIMENSION(:), INTENT(IN) :: rda_sp
    REAL(dp) :: rl_tmp
    rl_tmp = REAL(rda_dp(1), dp)
    rl_tmp = REAL(rda_sp(1), dp)
    rl_tmp = REAL(ida_int(1), dp)
  END SUBROUTINE nothing_isd_array1D
END MODULE debug_tools
