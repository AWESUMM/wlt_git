MODULE user_case
  !
  ! Case sphere
  USE precision
  USE elliptic_vars
  USE elliptic_mod
  USE field
  USE input_file_reader
  USE io_3D_vars
  USE pde
  USE share_consts
  USE share_kry
  USE sizes
  USE util_mod
  USE util_vars
  USE vector_util_mod
  USE wavelet_filters_mod
  USE wlt_vars
  USE wlt_trns_vars
  USE wlt_trns_mod
  USE wlt_trns_util_mod
  USE fft_module
  USE SGS_incompressible

  !
  ! case specific variables
  !
  INTEGER n_var_vorticity ! start of vorticity in u array (Must exist)
  INTEGER n_var_pressure  ! start of pressure in u array  (Must exist)

  INTEGER n_var_modvort   ! location of modulus of vorticity
  INTEGER n_var_modSij    ! location of modulus of Sij
  INTEGER n_var_modVel    ! location of velocity magnitude
  INTEGER :: n_var_mask   ! variable for penalization mask 


  LOGICAL :: adaptMagVort, adaptNormS, adaptMagVel
  LOGICAL :: saveMagVort, saveNormS

  REAL(pr) :: R_obst
CONTAINS

  !
  ! In user_setup_pde() we setup how many variables are integrated, which are interpolated
  ! to the next times step and if any exeact solution exists to compare to during the run.
  ! We also set the variable names that are used when the result files are written.
  !
  ! The following variables must be setup in this routine:
  !
  !
  ! n_integrated     ! first n_integrated eqns will be acted on for time integration
  ! n_var_additional ! 
  ! n_var
  !
  !
  !
  !
  SUBROUTINE  user_setup_pde ( VERB ) 
    IMPLICIT NONE
    LOGICAL, OPTIONAL :: VERB         ! print debug info
    INTEGER :: i

    PRINT * ,''
    PRINT *, '**********************Setting up PDE*****************'
    PRINT * ,'CASE ISOTURB '
    PRINT *, '*****************************************************'


    !------------ setting up default values 

    n_integrated = dim ! uvw - # of equations to solve without SGS model
 
    n_integrated = n_integrated + n_var_SGS ! adds additional equations for SGS model
    
    n_time_levels = 1  !--# time levels

    n_var_time_levels = n_time_levels * n_integrated !--Number of equations (2D velocity at two time levels)

    n_var_additional = 2 !--1 pressure at one time level, 1 mask function

    n_var_exact = 0 !--No exact solution 

    n_var_pressure  = n_integrated + 1 !pressure
    n_var_mask      = n_integrated + 2 !mask

    IF( adaptMagVort .or. saveMagVort ) THEN
       n_var_additional = n_var_additional +1
       n_var_modvort    = n_integrated + n_var_additional ! (wi.wi)^0.5vorticity magnitude
    END IF
    IF( adaptNormS .OR. saveNormS) THEN
       n_var_additional = n_var_additional +1
       n_var_modSij     = n_integrated + n_var_additional ! (SijSij)^0.5
    END IF
    IF( adaptMagVel ) THEN
       n_var_additional = n_var_additional +1
       n_var_modVel     = n_integrated + n_var_additional ! (UiUi)^0.5
    END IF

    n_var = n_var_time_levels + n_var_additional !--Total number of variables

    !
    !--Allocate logical arrays 
    !  Must be done after setting number of different types of variables
    !  and before setting logical variable mapping arrays
    !
    CALL alloc_variable_mappings
    !
    ! Fill in variable names for default variables for integration meth 2
    ! These may be changed in the call to the user specified routine
    !  user_setup_pde()
    !
    ! In integrated variables (3D)

                   WRITE (u_variable_names(1), u_variable_names_fmt) 'Velocity_u_@t  '
    IF( dim >= 2 ) WRITE (u_variable_names(2), u_variable_names_fmt) 'Velocity_v_@t  '
    IF( dim >= 3 ) WRITE (u_variable_names(3), u_variable_names_fmt) 'Velocity_w_@t  '
    WRITE (u_variable_names(n_var_pressure), u_variable_names_fmt) 'Pressure  '
    IF( n_var_mask /= 0 ) WRITE (u_variable_names(n_var_mask), u_variable_names_fmt) 'obstacle_@t  '
    IF( adaptMagVort .OR. saveMagVort) WRITE (u_variable_names(n_var_modvort), u_variable_names_fmt) '|Wij|  '
    IF( adaptNormS .OR. saveNormS)   WRITE (u_variable_names(n_var_modSij), u_variable_names_fmt)  '|Sij|  '

    !
    ! setup logical variable mapping arrays
    ! This defined which variables are used for adaptation, Which need to be interpololated to the new grid at 
    ! each time step, which are saved, and for which we have an exect solutiuon we want to check at each time step
    !

    !
    ! setup which components we will base grid adaptation on.
    ! Adapt on the 3 velocity components
    !
    n_var_adapt = .FALSE. !--Initially adapt on integrated variables at first time level
    n_var_adapt(n_var_mask,0) = .TRUE.

    !eventually adapt to mdl variables    n_var_adapt(1:dim+2,1) = .TRUE. !--After first time step adapt on  velocity and Ilm and Imm

    n_var_adapt(1:dim,1) = .TRUE. !--After first time step adapt on  velocity
    n_var_adapt(n_var_mask,1) = .TRUE. !--Adapt on mask

    IF( adaptMagVort ) n_var_adapt(n_var_modvort,1)  = .TRUE.
    IF( adaptNormS )   n_var_adapt(n_var_modSij,1 )  = .TRUE.
    

    !Variables that need to be interpoleted to new adapted grid in initial grid adaptation
    n_var_interpolate(1:n_integrated,0) = .TRUE. 

    !Variables that need to be interpoleted to new adapted grid at each time step
    n_var_interpolate(1:n_var_pressure,0) = .TRUE. 
    n_var_interpolate(1:n_var_pressure,1) = .TRUE. 

    IF( saveMagVort ) n_var_interpolate(n_var_modvort,:) = .TRUE. 
    IF( saveNormS )   n_var_interpolate(n_var_modSij,:) = .TRUE.
    !
    ! setup which components we have an exact solution for

    n_var_exact_soln(:,0:1) = .FALSE.

    !
    ! variables required for restart
    !
    n_var_req_restart = .FALSE.
    n_var_req_restart(1:dim)	        = .TRUE. !restart with velocities and pressure to begin with!
    n_var_req_restart(n_var_pressure)	= .TRUE. !

    ! no pressure for restart from initial file
    !n_var_req_restart(n_var_pressure ) = .TRUE.

    !
    ! setup which variables we will save the solution
    !
    n_var_save(1:n_var_mask) = .TRUE. ! save all for restarting code

    IF( saveMagVort ) n_var_save(n_var_modvort)  = .TRUE.
    IF( saveNormS )   n_var_save(n_var_modSij)  = .TRUE.
    
    !
    !--Time level counters for NS integration
    !   Used for integration meth2
    ! n0 is current time step
    ! n1 is t_n-1 time step
    ! n2 is t_n-2 time step
    !
    n0 = 1; n1 = n0+dim; n2 = n1+dim


    !
    ! Set the maximum number of components for which we have an exact solution
    !
    n_var_exact = MAX( COUNT(n_var_exact_soln(:,0)),COUNT(n_var_exact_soln(:,1)) )


    !
	! Setup a scaleCoeff array of we want to tweak the scaling of a variable
	! ( if scaleCoeff < 1.0 then more points will be added to grid 
	!
    ALLOCATE ( scaleCoeff(1:n_var), Umn(1:n_var) )
    scaleCoeff = 1.0_pr

    Umn = 0.0_pr !set up here if mean quantities anre not zero and used in scales or equation
    Umn(1) = 1.0_pr !--Uniform mean velocity in x-direction

    PRINT *, 'n_integrated = ',n_integrated 
    PRINT *, 'n_time_levels = ',n_time_levels
    PRINT *, 'n_var_time_levels = ',n_var_time_levels 
    PRINT *, 'n_var = ',n_var 
    PRINT *, 'n_var_exact = ',n_var_exact 
    PRINT *, '*******************Variable Names*******************'
    DO i = 1,n_var
       WRITE (*, u_variable_names_fmt) u_variable_names(i)
    END DO
    PRINT *, '****************************************************'



  END SUBROUTINE  user_setup_pde

  !
  ! read variables from input file for this user case 
  !
  SUBROUTINE  user_read_input()
    IMPLICIT NONE

  call input_real ('nu',nu,'stop', &
       ' nu: viscosity')

  call input_real ('R_obst',R_obst,'stop', &
       ' R_obst: radius of sphere')

   call input_logical ('adaptMagVort',adaptMagVort,'stop', &
       '  adaptMagVort, Adapt on the magnitude of vorticity')

   call input_logical ('adaptNormS',adaptNormS,'stop', &
       '  adaptNormS, Adapt on the L2 of Sij')

   call input_logical ('adaptMagVel',adaptMagVel,'stop', &
       '  adaptMagVel, Adapt on the magnitude of velocity')

   saveMagVort = .FALSE.
   call input_logical ('saveMagVort',saveMagVort,'default', &
       '  if .TRUE. adds and saves magnitude of vorticity')

   saveNormS = .FALSE.
   call input_logical ('saveNormS',saveNormS,'default', &
       '  if .TRUE. adds and saves magnitude of strain rate')
  END SUBROUTINE  user_read_input

  !
  ! Set the exact solution for comparison to the simulated solution
  !
  ! u          - array to fill in the exact solution
  ! nlocal       - number of active wavelets   
  ! ne_local        - total number of equations
  ! t          - time of current time step 
  ! l_n_var_exact_soln_index - index into the elements of u for which we need to 
  !                            find the exact solution
  SUBROUTINE  user_exact_soln (u, nlocal,  t_local, l_n_var_exact_soln)  
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: nlocal
    REAL (pr), INTENT (IN) ::  t_local
    REAL (pr), DIMENSION (nlocal,n_var_exact), INTENT (INOUT) :: u
    LOGICAL , INTENT (IN) :: l_n_var_exact_soln(n_var)
    INTEGER :: i


    ! There is no exact solution

  END SUBROUTINE  user_exact_soln

  SUBROUTINE user_initial_conditions (u, nlocal, ne_local, t_local, scl, scl_fltwt, iter)
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: nlocal, ne_local
    INTEGER  , INTENT (INOUT) :: iter ! iteration of call while adapting initial grid
    REAL (pr), DIMENSION (nlocal,ne_local), INTENT (INOUT) :: u
    REAL (pr)  :: scl(1:n_var),scl_fltwt
    REAL (pr), INTENT (IN) :: t_local

    INTEGER :: i,ie !TEST

    INTEGER, DIMENSION(1) :: SEED
    REAL (pr) ::   Uprime=1.0e-3_pr
    REAL (pr) ::   Umean,U2mean

	!add or overtise teh values in the fileds
    IF (IC_restart ) THEN !in the case of restart
	   !if restarts - DO NOTHING
    ELSE IF (IC_from_file) THEN  ! Initial conditions read from data file 
       !
	   ! Modify appropriate values
       ! 
       !u(:,n_var_pressure) = 1.0_pr
    ELSE !initialization from random field
!!$       CALL randspecU(iter)
       SEED(1) = 54321
       CALL RANDOM_SEED(PUT=SEED(1:1))
       U2mean = 0.0_pr
       DO ie = 1,dim
          DO i=1,nwlt
             CALL RANDOM_NUMBER(u(i,ie))
			 !u(i,ie) = 0.1_pr*(-1.0_pr)**(i) ! to test if wrk and tree are identical, deterministic IC
          END DO
          Umean = SUM(u(:,ie)*dA)/sumdA 
          u(:,ie) = u(:,ie) - Umean
          U2mean = U2mean+SUM(u(:,ie)**2*dA)/sumdA 
       END DO
       u(:,1:dim) = Uprime/SQRT(U2mean/3.0_pr)*u(:,1:dim)
    END IF

  END SUBROUTINE user_initial_conditions

  ! Arguments
  ! u         - field on adaptive grid
  ! nlocal      - number of active points
  ! ne_local       - number of equations
  ! t         - current time
  !
  ! Global variables used
  ! ifn       - number of equations
  ! x()
  ! xO
  ! y0
  !--******************************** 
  SUBROUTINE user_algebraic_BC (Lu, u, nlocal, ne_local, jlev, meth)
    !--Defines boundary condition type
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: jlev, meth, ne_local, nlocal
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (INOUT) :: Lu
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (IN)    :: u

    INTEGER :: ie, ii, shift

    !
    ! There are periodic BC conditions
    !

    !default SGS BC, can be explicitely deifined below instead of calling default BCs
    CALL SGS_algebraic_BC (Lu, u, nlocal, ne_local, jlev, meth) 

  END SUBROUTINE user_algebraic_BC

  SUBROUTINE user_algebraic_BC_diag (Lu_diag, nlocal, ne_local, jlev, meth)
    !--Defines boundary condition type
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: jlev, meth, ne_local, nlocal
    REAL (pr), DIMENSION (nlocal,ne_local), INTENT (INOUT) :: Lu_diag

    INTEGER :: ie, ii, shift

    !
    ! There are periodic BC conditions
    !
    
    !default diagonal terms of SGS BC, can be explicitely deifined below instead of calling default BCs
    IF(sgsmodel /=0 ) CALL SGS_algebraic_BC_diag (Lu_diag, nlocal, ne_local, jlev, meth)

  END SUBROUTINE user_algebraic_BC_diag

  SUBROUTINE user_algebraic_BC_rhs (rhs, ne_local, nlocal, jlev)
    !--Sets rhs for boundary conditions
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: ne_local, nlocal, jlev
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (INOUT) :: rhs

    INTEGER :: ie, ii, shift

    !
    ! There are periodic BC conditions
    !

    !default  SGS BC, can be explicitely deifined below instead of calling default BCs
    IF(sgsmodel /= 0) CALL SGS_algebraic_BC_rhs (rhs, ne_local, nlocal, jlev)

  END SUBROUTINE user_algebraic_BC_rhs

  SUBROUTINE user_project (u, p, nlocal, meth)
    !--Makes u divergence free
    IMPLICIT NONE
    INTEGER, PARAMETER :: ne_local = 1
    INTEGER, INTENT (IN) :: meth, nlocal
    REAL (pr), DIMENSION (nlocal),     INTENT(INOUT) :: p
    REAL (pr), DIMENSION (nlocal,n_integrated), INTENT(INOUT) :: u
    REAL (pr), DIMENSION (nlocal) :: dp
    REAL (pr), DIMENSION (nlocal) :: f
    REAL (pr), DIMENSION(ne_local) :: scl_p
    INTEGER, DIMENSION(ne_local) :: clip 

    !--Make u  divergence free
    dp = 0.0_pr
    f = div(u(:,1:dim),nlocal,j_lev,meth+4)
    clip = 1

    scl_p = MAXVAL(scl_global(1:dim)) !scale of pressure increment based on dynamic pressure
    PRINT *, 'scl_p=',scl_p
    CALL Linsolve (dp, f , tol2, nlocal, ne_local, clip, Laplace, Laplace_diag, SCL=scl_p) !new with scaling 
!    CALL Linsolve (dp, f , tol2, nlocal, ne_local, clip, Laplace, Laplace_diag)  !old without scaling
    u(:,1:dim) = u(:,1:dim) - grad(dp, nlocal, j_lev, meth+2)
    p = p + dp/dt

  END SUBROUTINE user_project

  FUNCTION Laplace (jlev, u, nlocal, ne_local, meth_in)
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: jlev, nlocal, ne_local, meth_in
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (INOUT) :: u
    REAL (pr), DIMENSION (nlocal*ne_local) :: Laplace

    INTEGER :: i, ii, ie, meth, meth1, meth2, idim, shift
    REAL (pr), DIMENSION (ne_local ,nlocal,dim) :: du
    REAL (pr), DIMENSION (ne_local*dim ,nlocal,dim) :: d2u
    REAL (pr), DIMENSION (ne_local*dim ,nlocal,dim) :: du_dummy ! passed when only calculating 1st derivative.
    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc

    meth1 = meth_in + 2
    meth2 = meth_in + 4

    !
    ! Find 1st deriviative of u. 
    !
    CALL c_diff_fast (u, du, d2u(1:ne_local, 1:nlocal, 1:dim), jlev, nlocal, meth1, 10, ne_local, 1, ne_local)


    !
    ! Find 2nd deriviative of u.  d( du ) 
    !
   !
    ! Find 2nd deriviative of u.  d( du ) 
    !
    IF ( TYPE_DB .NE. DB_TYPE_LINES ) THEN
       DO ie = 1, ne_local
          CALL c_diff_fast(du(ie,1:nlocal,1:dim), &
               d2u((ie-1)*dim+1:ie*dim,1:nlocal,1:dim), du_dummy((ie-1)*dim+1:ie*dim,1:nlocal,1:dim), &
               jlev, nlocal, meth2, 10, dim, 1, dim )
       END DO
    ELSE !db_lines

       ! Load du  into db
       DO ie=1,ne_local		
          CALL update_db_from_u(  du(ie,1:nlocal,1:dim)  , nlocal ,dim  , 1, dim, 1+(ne_local-1)*dim  ) !Load du	    
       END DO

       ! find  2nd deriviative of u (so we first derivative of du/dx)
       CALL c_diff_fast_db(d2u, du_dummy, jlev, nlocal, meth2, 10, ne_local*dim,&
            1,         &  ! MIN(mn_varD,mn_varD2)
            ne_local*dim,    &  ! MAX(mx_varD,mx_varD2)
            1,         &  ! mn_varD  :   min variable in db that we are doing the 1st derivatives for.
            ne_local*dim,    &  ! mn_varD  :   max variable in db that we are doing the 1st derivatives for.
            0 ,        &  ! mn_varD2 :   min variable in db that we are doing the 2nd derivatives for.
            0     )  ! mn_varD2 :   max variable in db that we are doing the 2nd derivatives for.

    END IF
    ! Now:
    ! d2u(1,:,1) = d^2 U/ dx dx
    ! d2u(1,:,2) = d^2 U/ dy dx
    ! d2u(1,:,3) = d^2 U/ dz dx
    ! d2u(2,:,1) = d^2 V/ dx dy
    ! d2u(2,:,2) = d^2 V/ dy dy
    ! d2u(2,:,3) = d^2 V/ dz dy
    ! d2u(3,:,1) = d^2 W/ dx dz
    ! d2u(3,:,2) = d^2 W/ dy dz
    ! d2u(3,:,3) = d^2 W/ dz dz

    DO ie = 1, ne_local
       shift=(ie-1)*nlocal
       !--- Internal points
       !--- div(grad)
       idim = 1
       Laplace(shift+1:shift+Nwlt_lev(jlev,0)) = d2u( (ie-1)*dim+idim ,1:Nwlt_lev(jlev,0),1)
       DO idim = 2,dim
          Laplace(shift+1:shift+Nwlt_lev(jlev,0)) = Laplace(shift+1:shift+Nwlt_lev(jlev,0)) + &
               d2u((ie-1)*dim+idim ,1:Nwlt_lev(jlev,0),idim)
       END DO
       !--Boundary points
       i_p_face(0) = 1
       DO i=1,dim
          i_p_face(i) = i_p_face(i-1)*3
       END DO
       DO face_type = 0, 3**dim - 1
          face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
          IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
             CALL get_all_indices_by_face (face_type, jlev, nloc, iloc)
             IF(nloc > 0 .AND. dim == 3) THEN ! 3-D case only
                IF( ALL( face == (/-1,0,0/) ) ) THEN  ! Xmin face, no edges or corners
                   Laplace(shift+iloc(1:nloc)) = u(shift+iloc(1:nloc))  !Dirichlet conditions
                ELSE IF( ALL( face == (/1,0,0/) ) ) THEN  ! Xmax face, no edges or corners
                   Laplace(shift+iloc(1:nloc)) = u(shift+iloc(1:nloc))  !Dirichlet conditions
!!$                Laplace(shift+iloc(1:nloc)) = du(ie,iloc(1:nloc),1)  !Neuman conditions
                ELSE IF( ALL( face(2:dim) == (/-1,0/) ) ) THEN  ! Ymin face, edges || to Z, no corners
                   Laplace(shift+iloc(1:nloc)) = u(shift+iloc(1:nloc))  !Dirichlet conditions
!!$                Laplace(shift+iloc(1:nloc)) = du(ie,iloc(1:nloc),2)  !Neuman conditions
                ELSE IF( ALL( face(2:dim) == (/1,0/) ) ) THEN  ! Ymax face, edges || to Z, no corners
                   Laplace(shift+iloc(1:nloc)) = u(shift+iloc(1:nloc))  !Dirichlet conditions
!!$                Laplace(shift+iloc(1:nloc)) = du(ie,iloc(1:nloc),2)  !Neuman conditions
                ELSE IF( face(3) == -1 ) THEN  ! entire Zmin face
                   Laplace(shift+iloc(1:nloc)) = u(shift+iloc(1:nloc))  !Dirichlet conditions
!!$                Laplace(shift+iloc(1:nloc)) = du(ie,iloc(1:nloc),3)  !Neuman conditions
                ELSE IF( face(3) == 1 ) THEN  ! entire Zmax face
                   Laplace(shift+iloc(1:nloc)) = u(shift+iloc(1:nloc))  !Dirichlet conditions
!!$                Laplace(shift+iloc(1:nloc)) = du(ie,iloc(1:nloc),3)  !Neuman conditions
                END IF
             END IF
          END IF
       END DO
    END DO
  END FUNCTION Laplace

  FUNCTION Laplace_diag (jlev,  nlocal, ne_local, meth_in)
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: jlev, nlocal, ne_local, meth_in
    REAL (pr), DIMENSION (nlocal*ne_local) :: Laplace_diag

    INTEGER :: i, ii, ie, shift, meth1, meth2
    REAL (pr), DIMENSION (nlocal,dim) :: du, d2u
    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc

    meth1 = meth_in + 2
    meth2 = meth_in + 4


    !PRINT *,'IN Laplace_diag, ne_local = ', ne_local

    DO ie = 1, ne_local
       shift=(ie-1)*nlocal
       !--- div(grad)
       !PRINT *,'CAlling c_diff_diag from Laplace_diag() '
       !PRINT *,'--- jlev, nlocal, meth1, meth2', jlev, nlocal, meth1, meth2

       CALL c_diff_diag ( du, d2u, jlev, nlocal, meth1, meth2, -11)

       Laplace_diag(shift+1:shift+Nwlt_lev(jlev,0)) = d2u(1:Nwlt_lev(jlev,0),1) + d2u(1:Nwlt_lev(jlev,0),2)
       IF (dim==3) Laplace_diag(shift+1:shift+Nwlt_lev(jlev,0)) = &
            Laplace_diag(shift+1:shift+Nwlt_lev(jlev,0))+d2u(1:Nwlt_lev(jlev,0),3)
       !--Boundary points
       i_p_face(0) = 1
       DO i=1,dim
          i_p_face(i) = i_p_face(i-1)*3
       END DO
       DO face_type = 0, 3**dim - 1
          face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
          IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
             CALL get_all_indices_by_face (face_type, jlev, nloc, iloc)
             IF(nloc > 0 .AND. dim == 3) THEN ! 3-D case only
                IF( ALL( face == (/-1,0,0/) ) ) THEN                         ! Xmin face, no edges or corners
                   Laplace_diag(shift+iloc(1:nloc)) = 1.0_pr                 !Dirichlet conditions
                ELSE IF( ALL( face == (/1,0,0/) ) ) THEN                     ! Xmax face, no edges or corners
                   Laplace_diag(shift+iloc(1:nloc)) = 1.0_pr                 !Dirichlet conditions
!!$                Laplace_diag(shift+iloc(1:nloc)) = du(iloc(1:nloc),1)     !Neuman conditions
                ELSE IF( ALL( face(2:dim) == (/-1,0/) ) ) THEN               ! Ymin face, edges || to Z, no corners
                   Laplace_diag(shift+iloc(1:nloc)) = 1.0_pr                 !Dirichlet conditions
!!$                Laplace_diag(shift+iloc(1:nloc)) = du(iloc(1:nloc),2)     !Neuman conditions
                ELSE IF( ALL( face(2:dim) == (/1,0/) ) ) THEN                ! Ymax face, edges || to Z, no corners
                   Laplace_diag(shift+iloc(1:nloc)) = 1.0_pr                 !Dirichlet conditions
!!$                Laplace_diag(shift+iloc(1:nloc)) = du(iloc(1:nloc),2)     !Neuman conditions
                ELSE IF( face(3) == -1 ) THEN                                ! entire Zmin face
                   Laplace_diag(shift+iloc(1:nloc)) = 1.0_pr                 !Dirichlet conditions
!!$                Laplace_diag(shift+iloc(1:nloc)) = du(iloc(1:nloc),3)     !Neuman conditions
                ELSE IF( face(3) == 1 ) THEN                                 ! entire Zmax face
                   Laplace_diag(shift+iloc(1:nloc)) = 1.0_pr                 !Dirichlet conditions
!!$                Laplace_diag(shift+iloc(1:nloc)) = du(iloc(1:nloc),3)     !Neuman conditions
                END IF
             END IF
          END IF
       END DO
    END DO
  END FUNCTION Laplace_diag



  FUNCTION user_rhs (u_integrated,p)
    USE penalization
    IMPLICIT NONE
    REAL (pr), DIMENSION (ng,ne), INTENT(IN) :: u_integrated !1D flat version of integrated variables without BC 
    REAL (pr), DIMENSION (ng), INTENT(IN) :: p
    REAL (pr), DIMENSION (n) :: user_rhs

    INTEGER :: ie, ie1, shift, i
    INTEGER, PARAMETER :: meth=1
    REAL (pr), DIMENSION (ne,ng,dim) :: du, d2u
    REAL (pr), DIMENSION (ng,dim)     :: dp
    

   !PRINT *,'user_rhs MINMAX(p) ', MINVAL( p), MAXVAL(dp)
     dp(1:ng,1:dim) = grad (p, ng, j_lev, meth+2)

    !PRINT *,'user_rhs MINMAX(dp) ', MINVAL( dp), MAXVAL(dp)
    !PRINT *,'user_rhs MINMAX(u_integrated) ', MINVAL( u_integrated), MAXVAL(u_integrated)

    CALL c_diff_fast(u_integrated, du, d2u, j_lev, ng, 1, 11, ne, 1, ne) !derivatives are calculated even for SGS terms

    !PRINT *,'user_rhs MINMAX(du) ', MINVAL( du), MAXVAL(du)

    !--Form right hand side of Navier-Stokes equations
    DO ie = 1, dim
       shift=(ie-1)*ng
       user_rhs(shift+1:shift+ng) = nu*SUM(d2u(ie,:,:),2) - dp(:,ie) 
       DO ie1 = 1,dim
          user_rhs(shift+1:shift+ng) = user_rhs(shift+1:shift+ng) - (u_integrated(:,ie1)+Umn(ie1))*du(ie,:,ie1) 
       END DO
    END DO

    IF(sgsmodel /= 0) CALL SGS_rhs (user_rhs, u_integrated, du, d2u)

    IF(imask_obstacle) THEN
       DO ie = 1, dim
          shift=(ie-1)*ng
          user_rhs(shift+1:shift+ng) = user_rhs(shift+1:shift+ng) - penal/eta_chi*(u_integrated(:,ie)+Umn(ie))
       END DO
    END IF

    !--Set operator on boundaries
    IF(SUM(nbnd) /= 0) CALL user_algebraic_BC_rhs (user_rhs, ne, ng, j_lev)
  END FUNCTION user_rhs


  ! find Jacobian of Right Hand Side of the problem
  ! u_prev == u_prev_timestep_loc

  FUNCTION user_Drhs (u, u_prev, meth)
    USE penalization
    IMPLICIT NONE
    INTEGER, INTENT(IN) ::  meth
    !u_prev_timestep passed local to cast it into 2dim array to work with vector derivatives
    REAL (pr), DIMENSION (ng,ne) :: u, u_prev
    REAL (pr), DIMENSION (n) :: user_Drhs

    INTEGER :: ie, ie1, shift
    REAL (pr), DIMENSION (2*ne,ng,dim) :: du  ! 1st derivatives for u (in du(1:ne,:,:)) and u_prev_timestep (in du(ne+1:2*ne,:,:))
    REAL (pr), DIMENSION (ne  ,ng,dim) :: d2u ! 2nd derivatives for u 
    REAL (pr), DIMENSION (ne  ,ng,dim) :: du_dummy ! passed when only calculating 1st derivative.
    !Find batter way to do this!! du_dummy with no storage..

    IF ( TYPE_DB .NE. DB_TYPE_LINES )THEN 
       ! find 1st and 2nd deriviative of u and
       CALL c_diff_fast(u, du(1:ne,:,:), d2u(1:ne,:,:), j_lev, ng, meth, 11, ne , 1, ne )

       ! find only first deriviativ e  u_prev_timestep
       CALL c_diff_fast(u_prev, du(ne+1:2*ne,:,:), du_dummy(1:ne,:,:), j_lev, ng, meth, 10, ne , 1, ne )

    ELSE !db_lines

       ! Load u and u_prev_timestep in to db
       ! update the db from u
       ! u(:, mn_var:mx_var) -> db%u(db_offset:mx_var-mn_var+1)
       ! db_offset = 1 
       CALL update_db_from_u(  u       , ng ,ne  , 1, ne, 1  ) !Load u
       CALL update_db_from_u(  u_prev , ng ,ne  , 1, ne, ne+1  ) !Load u_prev offset in db by ne+1

       ! find 1st and 2nd deriviative of u and
       ! find only first deriviativ e  u_prev_timestep
       CALL c_diff_fast_db(du, d2u, j_lev, ng, meth, 11, 2*ne,&
            1,   &  ! MIN(mn_varD,mn_varD2)
            2*ne,&  ! MAX(mx_varD,mx_varD2)
            1,   &  ! mn_varD  :   min variable in db that we are doing the 1st derivatives for.
            2*ne,&  ! mn_varD  :   max variable in db that we are doing the 1st derivatives for.
            1 ,  &  ! mn_varD2 :   min variable in db that we are doing the 2nd derivatives for.
            dim   )  ! mn_varD2 :   max variable in db that we are doing the 2nd derivatives for.


    END IF
    !CALL c_diff_fast_db(du_b, du_b, j_lev, ng, meth, 10, ne, 1, ne) !find 1st derivative u_b

    !--Form right hand side of Navier--Stokes equations
    DO ie = 1, dim
       shift=(ie-1)*ng
       
       user_Drhs(shift+1:shift+ng) =  nu*SUM(d2u(ie,:,:),2)
       DO ie1 = 1, dim
          user_Drhs(shift+1:shift+ng) = user_Drhs(shift+1:shift+ng) - (u_prev(:,ie1)+Umn(ie1))*du(ie,:,ie1) & 
                                                                    - u(:,ie1)*du(ne+ie,:,ie1) 
       END DO
    END DO
       

    IF(sgsmodel /= 0) CALL SGS_Drhs (user_Drhs, u, u_prev, du, d2u, meth)


    IF(imask_obstacle) THEN
       DO ie = 1, dim
          shift=(ie-1)*ng
          user_Drhs(shift+1:shift+ng) = user_Drhs(shift+1:shift+ng) - 2.0_pr * penal/eta_chi*u(:,ie)
       END DO
    END IF


  END FUNCTION user_Drhs


  !
  ! Uses u_prev_timestep, which is global flat (1D ) array set in time_step_cn()
  !
  FUNCTION user_Drhs_diag (meth)
    USE penalization
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: meth
    REAL (pr), DIMENSION (n) :: user_Drhs_diag

    INTEGER :: ie, ie1, shift,shiftIlm,shiftImm
    REAL (pr), DIMENSION (ng,dim) :: du, d2u
    REAL (pr), DIMENSION (ne,ng,dim) :: du_prev_timestep
    REAL (pr), DIMENSION (ne  ,ng,dim) :: du_dummy ! passed when only calculating 1st derivative.

    CALL c_diff_fast(u_prev_timestep, du_prev_timestep, du_dummy, j_lev, ng, meth, 10, ne, 1, ne)

    !
    ! does not rely on u so we can call it once here
    !
    shift = 0 !tmp
    CALL c_diff_diag(du, d2u, j_lev, ng, meth, meth, 11)

    !--Form right hand side of Navier--Stokes equations
    DO ie = 1, dim
       shift=(ie-1)*ng
       user_Drhs_diag(shift+1:shift+ng) = nu*SUM(d2u,2) - du_prev_timestep(ie,:,ie)
       DO ie1 = 1, dim
          user_Drhs_diag(shift+1:shift+ng) = user_Drhs_diag(shift+1:shift+ng) - (u_prev_timestep((ie1-1)*ng+1:ie1*ng)+Umn(ie1))*du(:,ie1)
       END DO
    END DO
    
    IF(sgsmodel /= 0) CALL SGS_Drhs_diag (user_Drhs_diag, du_prev_timestep, du, d2u, meth)

    IF(imask_obstacle) THEN
       DO ie = 1, dim
          shift=(ie-1)*ng
          user_Drhs_diag(shift+1:shift+ng) = user_Drhs_diag(shift+1:shift+ng) - 2.0_pr * penal/eta_chi
       END DO
    END IF


  END FUNCTION user_Drhs_diag

  FUNCTION user_chi (nlocal, t_local)
    IMPLICIT NONE
    INTEGER,          INTENT (IN) :: nlocal
    REAL (pr),        INTENT (IN) :: t_local
    REAL (pr), DIMENSION (nlocal) :: user_chi
    !--Defines mask for sphere (3d) or cylinder (2d)

    user_chi = 0.5_pr*(1.0_pr+SIGN(1.0_pr, R_obst**2-SUM(x**2,DIM=2)))

  END FUNCTION user_chi

  !
  ! Calculate any statitics
  !
  ! startup_flag - 0 when adapting to IC,then 1 inmain integration loop
  !
  !
  SUBROUTINE user_stats (u , j_mn, startup_flag)
!    USE fft_module
!    USE spectra_module
    USE wlt_vars
    USE vector_util_mod
    IMPLICIT NONE
    REAL (pr), DIMENSION (nwlt,1:n_var), INTENT (IN) :: u
    INTEGER , INTENT (IN) :: j_mn 
    INTEGER , INTENT (IN) :: startup_flag
    CHARACTER (LEN=256)  :: filename

    !================= OBSTACLE ====================
    REAL (pr)                       :: drag, lift
    REAL (pr), DIMENSION (dim)      :: force
    !==============================================
    
    !USER may define additional statistics ouutput here.

    WRITE(*,'(" ")')
    WRITE(*,'("****************** Turbulence Statistics on the Fly *******************")')

    IF (imask_obstacle) THEN !--Calculate force
       CALL user_cal_force (u(:,1:dim), nwlt, t, force, drag, lift)
       WRITE (6,'("Drag = ", es9.2, 1x, "Lift = ", es9.2)') drag, lift
       OPEN  (11, FILE = 'results/'//TRIM(file_gen)//'_user_stats', FORM='formatted', STATUS='old', POSITION='append')
       WRITE (11,'(3(es15.8,1x))') t, drag, lift
       CLOSE (11)
    END IF

    WRITE(*,'("***********************************************************************")')
    WRITE(*,'(" ")')

  END SUBROUTINE user_stats

  SUBROUTINE user_cal_force (u, n, t_local, force, drag, lift)
    !--Calculates drag and lift on obstacle using penalization formula
    USE penalization
    IMPLICIT NONE

    INTEGER, INTENT (IN) :: n
    INTEGER :: ie
    REAL (pr), INTENT (IN) :: t_local
    REAL (pr), DIMENSION (dim), INTENT (INOUT) :: force
    REAL (pr), INTENT (OUT) :: drag, lift
    REAL (pr), DIMENSION (n,dim) :: u
    REAL (pr) :: A

    IF(dim == 3) THEN
       A = pi * R_obst**2
    ELSE IF(dim == 2) THEN
       A = 2.0_pr* R_obst
    ELSE
       A = 1.0_pr
    END IF
   
    DO ie = 1, dim
       force(ie) = SUM(penal/eta_chi*(u(:,ie)+Umn(ie))*dA)
       CALL parallel_global_sum( REAL=force(ie) )
       force(ie) = force(ie) / (0.5_pr * A * SUM(Umn(1:dim)**2))
    END DO

    drag = force(1) ; lift = force(2)
    
  END SUBROUTINE user_cal_force

  !
  ! calculate any additional variables
  ! 
  ! arg 
  ! flag  - 0 calledwhile adapting to IC, 1 - called in main time integration loop
  !
  ! These additiona variables are calculated and left in real space.
  !
  SUBROUTINE user_additional_vars( t_local, flag )
    IMPLICIT NONE
    REAL (pr), INTENT (IN) ::  t_local
    INTEGER , INTENT(IN) :: flag ! 0- called during adaption to IC, 1 called during main integration loop
    REAL(pr) tmp(nwlt,2*dim) !tmp for vorticity


    u(:,n_var_mask) =  user_chi (nwlt, t_local) !--Defines mask for sphere (3d) or cylinder (2d)
    
    IF( flag == 1 .OR. adaptMagVort .OR. saveMagVort .OR. adaptNormS .OR. saveNormS) THEN ! only in main time int loop, not initial adaptation
       ! Calculate the magnitude vorticity
       IF( adaptMagVort .OR. saveMagVort ) THEN 
          CALL cal_vort (u(:,n0:n0+dim-1), tmp(:,1:3-MOD(dim,3)), nwlt)
          IF (dim==2) THEN
             u(:,n_var_modvort) = tmp(:,1)
          ELSE
             u(:,n_var_modvort) = SQRT(SUM(tmp(:,1:3-MOD(dim,3))**2, 2))
          END IF
       END IF

       ! Calculate the magnitude of Sij (sqrt(SijSij) )
       ! s(:,1) = s_11
       ! s(:,2) = s_22
       ! s(:,3) = s_33
       ! s(:,4) = s_12
       ! s(:,5) = s_13
       ! s(:,6) = s_23

       IF( adaptNormS .OR. saveNormS) THEN
          CALL Sij( u(:,1:dim) ,nwlt, tmp(:,1:2*dim), u(:,n_var_modSij), .TRUE. )
       END IF

       ! Calculate the magnitude of velocity
       IF( adaptMagVel ) THEN 
          u(:,n_var_modvel) = 0.5_pr*SUM(u(:,1:dim)**2.0_pr,DIM=2)
       END IF

    END IF

  END SUBROUTINE user_additional_vars


  !
  ! calculate any additional scalar variables
  ! 
  ! arg 
  ! flag  - 0 calledwhile adapting to IC, 1 - called in main time integration loop
  !
  SUBROUTINE user_scalar_vars( flag )
    IMPLICIT NONE
    INTEGER , INTENT(IN) :: flag ! 0- called during adaption to IC, 1 called during main integration loop



  END SUBROUTINE user_scalar_vars

  !
  !************ Calculating Scales ***************************
  !
  ! Note the order of the components in the scl array
  ! correspond to u_tn, v_tn, w_tn, u_tn-1, v_tn-1, w_tn-1, u_tn-2, v_tn-2, w_tn-2
  ! 
  SUBROUTINE user_scales(flag, use_default, u_loc, nlocal, ne_local, l_n_var_adapt , l_n_var_adapt_index, &
       scl, scl_fltwt) !add
    USE parallel
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: flag ! 0 during initial adaptation, then 1 during time advancement
    LOGICAL , INTENT(INOUT) :: use_default
    INTEGER, INTENT (IN) :: nlocal, ne_local
    REAL (pr), DIMENSION (1:nlocal,1:ne_local), INTENT (IN) :: u_loc
    LOGICAL , INTENT (IN) :: l_n_var_adapt(ne_local)
    INTEGER , INTENT (IN) :: l_n_var_adapt_index(1:ne_local)
    REAL (pr), DIMENSION (1:ne_local), INTENT (INOUT) :: scl
    REAL (pr) ,  INTENT(IN) ::scl_fltwt !weight for temporal filter on scl
    REAL (pr) :: floor, tmp
    INTEGER :: ie, ie_index, itmp
    REAL (pr) ,save, ALLOCATABLE, DIMENSION (:) :: scl_old
    LOGICAL  ,save :: startup_init = .TRUE.

    !
    ! Ignore the output of this routine and use default scales routine
    !
    use_default = .FALSE. 

    !
    ! NOTE: For a parallel run, synchronize scl(:) across the processors in user_scales.
    !       Use the subroutine scales of default_util.f90 as an example,
    !       subroutine parallel_global_sum of parallel.f90 may be helpful.
    !       Already synchronized: sumdA_global
    IF( .NOT. use_default ) THEN

       IF (par_rank.EQ.0) PRINT *,'Use user_scales() <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<'
       !
       ! ALLOCATE scl_old if it was not done previously in user_scales
       !
       IF( startup_init .AND. .NOT. ALLOCATED(scl_old) ) THEN
          ALLOCATE( scl_old(1:ne_local) )
          scl_old = 0.0_pr
       END IF
       
       floor = 1.e-12_pr
       scl   =1.0_pr
       !
       ! Calculate scl per component
       !
       DO ie=1,ne_local
          IF(l_n_var_adapt(ie)) THEN
             ie_index = l_n_var_adapt_index(ie)
             IF( Scale_Meth == 1 ) THEN ! Use Linf scale
                scl(ie)= MAXVAL ( ABS( u_loc(1:nlocal,ie_index) ) )
                CALL parallel_global_sum( REALMAXVAL=scl(ie) )
                
             ELSE IF ( Scale_Meth == 2 ) THEN ! Use L2 scale
                scl(ie) = SUM( (u_loc(1:nlocal,ie_index)**2) )
                itmp = nlocal
                CALL parallel_global_sum( REAL=scl(ie) )
                CALL parallel_global_sum( INTEGER=itmp )
                scl(ie)= SQRT ( scl(ie)/itmp )
                
             ELSE IF ( Scale_Meth == 3 ) THEN ! Use L2 scale using dA
                scl(ie) = SUM( (u_loc(1:nlocal,ie_index)**2)*dA )
                CALL parallel_global_sum( REAL=scl(ie) )
                scl(ie) = SQRT ( scl(ie)/sumdA_global )
                
             ELSE
                IF (par_rank.EQ.0) THEN
                   PRINT *, 'Error in Scales(), Unknown Scale_Meth type: ', Scale_Meth
                   PRINT *, 'Known Scale_Meth types: 1 = Linf scale, 2 = L2 scale '
                   PRINT *, 'Exiting ...'
                END IF
                CALL parallel_finalize; STOP
             END IF
             
             IF (par_rank.EQ.0) THEN
                WRITE (6,'("Scaling before taking vector(1:dim) magnitude")')
                WRITE (6,'("Scale factor eqn", I4 ," = ", es11.4 ," scalecoeff = ",es11.4 )') &
                     ie, scl(ie), scaleCoeff(ie)
             END IF
             
          END IF
       END DO
       !
       ! take appropriate vector norm over scl(1:dim)
       IF( Scale_Meth == 1 ) THEN ! Use Linf scale
          tmp = MAXVAL(scl(1:dim)) !this is statistically equivalent to velocity vector length
          scl(1:dim) = tmp
       ELSE IF ( Scale_Meth == 2 ) THEN ! Use L2 scale
          tmp = SQRT(SUM( scl(1:dim)**2 ) ) !now velocity vector length, rather than individual component
          scl(1:dim) = tmp
       ELSE IF ( Scale_Meth == 3 ) THEN ! Use L2 scale using dA
          tmp = SQRT(SUM( scl(1:dim)**2 ) ) !now velocity vector length, rather than individual component
          scl(1:dim) = tmp
       END IF
       !
       ! Print out new scl
       !
       DO ie=1,ne_local
          IF(l_n_var_adapt(ie)) THEN
             !this is done if one of the variable is exactly zero inorder not to adapt to the nois
             IF(scl(ie) .le. floor) scl(ie)=1.0_pr 
             tmp = scl(ie)
             ! temporally filter scl
             IF(.not. startup_init ) scl(ie) = scl_fltwt * scl_old(ie) + (1.0_pr - scl_fltwt) * scl(ie)
             scl = scaleCoeff * scl
             IF (par_rank.EQ.0) THEN
                WRITE (6,'("Scaling on vector(1:dim) magnitude")')
                WRITE (6,'("Scale factor eqn", I4 ," = ", es11.4 ," scalecoeff = ",es11.4 )') &
                     ie, scl(ie), scaleCoeff(ie)
                WRITE (6,'("(before filtering = ",es11.4,")" )') tmp
             END IF
          END IF
       END DO
       
       scl_old = scl !save scl for this time step
       startup_init = .FALSE.
       !PRINT *,'TEST scl_old ', scl_old
    END IF ! use default
    
  END SUBROUTINE user_scales

  SUBROUTINE user_cal_cfl (use_default, u, cfl_out)
    USE parallel
    IMPLICIT NONE
    LOGICAL , INTENT(INOUT) :: use_default
    REAL (pr),                                INTENT (INOUT) :: cfl_out
    REAL (pr), DIMENSION (nwlt,n_integrated), INTENT (IN)    :: u
    
    INTEGER                    :: i
    REAL (pr)                  :: floor
    REAL (pr), DIMENSION (dim) :: cfl
    REAL (pr), DIMENSION(dim,nwlt) :: h_arr
    
    use_default = .FALSE.
    
    floor = 1e-12_pr
    cfl_out = floor
    
    CALL get_all_local_h (h_arr)
    
    DO i = 1, nwlt
       cfl(1:dim) = ABS (u(i,1:dim)+Umn(1:dim)) * dt/h_arr(1:dim,i)
       cfl_out = MAX (cfl_out, MAXVAL(cfl))
    END DO
    CALL parallel_global_sum( REALMAXVAL=cfl_out )
    
  END SUBROUTINE user_cal_cfl
  
  SUBROUTINE  user_sgs_force ( u, nlocal)
    IMPLICIT NONE
    
    INTEGER,                         INTENT (IN) :: nlocal
    REAL (pr), DIMENSION (nlocal,n_integrated), INTENT (INOUT) :: u
    
    !default SGS_force, can be explicitely deifined below instead of calling default SGS_force
    IF(sgsmodel /= 0) CALL sgs_force ( u, nlocal)
    
  END SUBROUTINE user_sgs_force

  FUNCTION user_sound_speed (u, neq, nwlt)
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: nwlt, neq
    REAL (pr), DIMENSION (nwlt,neq), INTENT(IN) :: u
    REAL (pr), DIMENSION (nwlt) :: user_sound_speed

    !user_sound_speed(:) = SQRT( gamma*(gamma-1.0_pr)* &
    !       ( u(:,4)-0.5_pr*SUM(u(:,n_var_mom(1:dim))**2,DIM=2)/u(:,1)) ) ! pressure

    user_sound_speed(:) = 0.0_pr

  END FUNCTION user_sound_speed

  SUBROUTINE  user_pre_process
    IMPLICIT NONE
     
  END SUBROUTINE user_pre_process

  SUBROUTINE  user_post_process
    IMPLICIT NONE
    
  END SUBROUTINE user_post_process

END MODULE user_case





