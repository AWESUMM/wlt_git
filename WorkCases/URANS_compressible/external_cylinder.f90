!$
!!$!****************************************************************************
!!$!* Module for adding Brinkman Penalization terms onto the full NS equations *
!!$!*  -Only valid for single species 10/7/2011                                *
!!$!*  -R is scaled to 1.0
!!$!****************************************************************************
!!$
!!$

MODULE user_case

  USE precision
  USE wlt_vars
  USE wavelet_filters_mod
  USE elliptic_mod
  USE elliptic_vars
  USE wlt_trns_mod
  USE wlt_trns_vars
  USE wlt_trns_util_mod
  USE io_3d_vars
  USE util_mod
  USE util_vars
  USE share_consts
  USE pde
  USE variable_mapping
  USE sizes
  USE share_kry
  USE vector_util_mod
  USE field
  USE input_file_reader
  USE parallel
  USE hyperbolic_solver
  USE SGS_util 
  USE equations_compressible
  USE freundBC 
  USE URANS_compressible      ! to use for rhs, Drhs, and Drhs_diag
  USE URANS_compressible_KOME ! to use Ksgs_init & delta_n 

  !
  ! case specific variables
  !
  REAL (pr), PROTECTED :: Re, Pra, Sc, Fr, Ma

  REAL (pr), ALLOCATABLE, DIMENSION (:) :: Lxyz
  INTEGER :: n_var_pressure  ! start of pressure in u array
  INTEGER :: nmsk,n_util
  LOGICAL ::  saveUtil  !4extra - this whole line
  REAL (pr) :: Lczn, tfacczn, itfacczn, itczn, tczn, cczn, czn_min, czn_max
  LOGICAL :: modczn

  REAL (pr) :: grav_coeff
  LOGICAL :: dynamic_gravity

  LOGICAL :: mesh_clustering
  REAL (pr) :: BL_cluster_param, azimuth_cluster_param 

  INTEGER :: hyper_it = 0  ! for temporary hyperbolic solver application - often used to smooth local instabilities
  INTEGER :: hyper_it_limit = 0  ! for temporary hyperbolic solver application - often used to smooth local instabilities

CONTAINS

   SUBROUTINE  user_setup_pde ( VERB ) 
    USE variable_mapping
    IMPLICIT NONE
    LOGICAL, OPTIONAL :: VERB         ! print debug info
    INTEGER :: i, idim, l
    INTEGER :: n_int_comp, n_add_comp

    IF (par_rank.EQ.0) THEN
       PRINT * ,''
       PRINT *, '**********************Setting up PDE*****************'
       PRINT * ,'CASE: External flow around a 2D cylinder' 
       PRINT *, '*****************************************************'
    END IF

    ! Set up compressible equation variables 
    CALL compressible_setup_pde

    IF (imask_obstacle) THEN
        CALL register_var( 'Mask',       integrated=.FALSE.,  adapt=(/.TRUE.,.TRUE./),   interpolate=(/.TRUE.,.TRUE./),   exact=(/.FALSE.,.FALSE./),   saved=.TRUE., req_restart=.TRUE. )
    END IF

    IF ( saveUtil) THEN 
        CALL register_var( 'Utility',       integrated=.FALSE.,  adapt=(/.FALSE.,.FALSE./),   interpolate=(/.TRUE.,.TRUE./),   exact=(/.FALSE.,.FALSE./),   saved=.TRUE., req_restart=.TRUE. )
    END IF


    CALL setup_mapping() 
    scaleCoeff = 1.0_pr

    ! Finalize setup of compressible equations 
    CALL compressible_finalize_setup_pde
    !CALL set_exact( get_index('Pressure'), (/.TRUE.,.TRUE./) )  ! Set pressure as an exact solution

    n_var_pressure  = n_prs  ! redundant, but required by code.  Unused in this module
    IF(hypermodel .NE. 0 ) THEN
      n_var_mom_hyper(1:dim) = n_mom(1:dim)
      n_var_den_hyper = n_den
    END IF

    nmsk = get_index('Mask') 
    n_util = get_index('Utility') 
    
    IF(hypermodel .NE. 0) THEN
       IF(ALLOCATED(n_var_hyper)) DEALLOCATE(n_var_hyper)
       ALLOCATE(n_var_hyper(1:n_var))
       n_var_hyper = .FALSE.
       !n_var_hyper(n_den) = .TRUE.
       n_var_hyper(n_mom(1:dim)) = .TRUE.
       n_var_hyper(n_eng) = .TRUE.
       IF ( n_mvort .GT. 0) n_var_hyper(n_mvort) = .TRUE.  ! Then the magnitude of vorticity is being tracked and saved
       !n_var_hyper = .TRUE.


       IF(ALLOCATED(n_var_hyper_active)) DEALLOCATE(n_var_hyper_active)
       ALLOCATE(n_var_hyper_active(1:n_integrated))
       n_var_hyper_active = .FALSE.
      ! n_var_hyper_active(n_den) = .TRUE.
!       n_var_hyper_active(n_eng) = .TRUE.
       n_var_hyper_active = .TRUE.
       
       IF(sgsmodel > sgsmodel_max) THEN
          n_var_hyper = .FALSE.
!!$          IF( n_mvort > 0 ) THEN
!!$             n_var_hyper(n_mvort) = .TRUE.
!!$          ELSE
!!$             n_var_hyper(n_var_K) = .TRUE.
!!$             n_var_hyper(n_var_W) = .TRUE.
!!$          END IF
          n_var_hyper(n_var_K) = .TRUE.
          n_var_hyper(n_var_W) = .TRUE.
          n_var_hyper_active = .FALSE.
          n_var_hyper_active(n_var_K) = .TRUE.
          n_var_hyper_active(n_var_W) = .TRUE.
       END IF

    END IF


    IF (par_rank.EQ.0) THEN
       PRINT *, 'n_integrated = ',n_integrated 

       PRINT *, 'n_var = ',n_var 
       PRINT *, 'n_var_exact = ',n_var_exact 
       PRINT *, '*******************Variable Names*******************'
       DO i = 1,n_var
          WRITE (*, u_variable_names_fmt) u_variable_names(i)
       END DO
       PRINT *, '****************************************************'
    END IF

  END SUBROUTINE  user_setup_pde


  !
  ! Set the exact solution for comparison to the simulated solution
  !
  ! u          - array to fill in the exact solution
  ! nlocal       - number of active wavelets   
  ! ne_local        - total number of equations
  ! t          - time of current time step 
  ! l_n_var_exact_soln_index - index into the elements of u for which we need to 
  !                            find the exact solution
  SUBROUTINE  user_exact_soln (u, nlocal,  t_local, l_n_var_exact_soln)  
    USE variable_mapping
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: nlocal
    REAL (pr), INTENT (IN) ::  t_local
    REAL (pr), DIMENSION (nlocal,n_var_exact), INTENT (INOUT) :: u
    LOGICAL , INTENT (IN) :: l_n_var_exact_soln(n_var)
    INTEGER :: i

    REAL (pr), DIMENSION(nlocal,1:dim) :: x_phys 
    REAL (pr), DIMENSION(nlocal) :: radius ! radial coordinate
   
  END SUBROUTINE  user_exact_soln

  SUBROUTINE user_initial_conditions (u, nlocal, ne_local, t_local, scl, scl_fltwt,iter)
    USE variable_mapping
    USE penalization
    USE parallel 
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: nlocal, ne_local
    REAL (pr), DIMENSION (nlocal,ne_local), INTENT (INOUT) :: u
    REAL (pr)  :: scl(1:n_var),scl_fltwt
    REAL (pr), INTENT (IN) :: t_local
    INTEGER, INTENT (INOUT) :: iter
    INTEGER :: i, j, idim, l, ie, ii, k
    INTEGER :: iseedsize, jx, jy, jz, sprt
    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc

    REAL (pr), DIMENSION(nlocal,1:dim) :: x_phys 
    REAL (pr), DIMENSION(2,1:dim) :: x_phys_limits

    REAL (pr), DIMENSION(nlocal) :: distance 

    REAL (pr) :: tmp1, tmp2 
    REAL (pr) :: d_thick, delta_trans 
    INTEGER :: nwlt_total, nwlt_max

    x_phys(:,:) = user_mapping(x, nlocal, t )
    DO idim = 1,dim
       x_phys_limits(1,idim) = MINVAL( x_phys(:,idim) )
       x_phys_limits(2,idim) = MAXVAL( x_phys(:,idim) )
       CALL parallel_global_sum( REALMINVAL=x_phys_limits(1,idim) )
       CALL parallel_global_sum( REALMAXVAL=x_phys_limits(2,idim) )
       IF ( par_rank .EQ. 0 )  PRINT *, idim,'X_phys min = ', x_phys_limits(1,idim), 'max = ',x_phys_limits(2,idim)      
    END DO

  IF (IC_restart_mode > 0 ) THEN !in the case of restart
     !do nothing
!     IF ( sgsmodel > sgsmodel_max .AND. IC_restart_mode .EQ. 3) THEN
!        distance(:) = SQRT( SUM( x_phys(:,1:2)**2, DIM=2 ) ) - xyzlimits(1,1) !Assumes cylinder, not sphere 
!        u(:,n_var_K) = Ksgs_init*(1.0_pr-EXP(-1.0_pr*distance(:)/delta_n))
!        u(:,n_var_W) = Kdiss_init+(80.0_pr*mu_in(Nspec)/(delta_n**2)-Kdiss_init)*EXP(-1.0_pr*distance(:)/delta_n)
!        IF(par_rank.EQ.0) PRINT *, IC_restart_mode,'Ksgs_init and Kdiss_init:', Ksgs_init, Kdiss_init
!     END IF
  ELSE IF (IC_restart_mode .EQ. 0) THEN

     ! Quiescent field

     !Set Density
     u(:,n_den)              = 1.0_pr

     !Set Background Momentum
     DO i=1,dim
        u(:,n_mom(i)) = 0.0_pr
     END DO

     ! Add mean flow damped to the boundary conditions
     ! For cylindrical coordinates
     distance(:) = SQRT( SUM( x_phys(:,1:2)**2, DIM=2 ) ) - xyzlimits(1,1) !Assumes cylinder, not sphere 
     IF( dim .EQ. 2 )u(:,n_mom(1)) = TANH( ( 1.0_pr + 0.10_pr * SIN( x(:,2) )  ) * distance(:) )  ! Asymmetric distribution to encourace initiation of shedding for 2D laminar case 
     IF( dim .EQ. 3 )u(:,n_mom(1)) = TANH( ( 1.0_pr &
                  + 0.07_pr * SIN( x(:,2) + 6.0_pr * PI * x(:,3)/(xyzlimits(2,3)-xyzlimits(1,3)) ) &
                  + 0.05_pr * COS( x(:,2) + 2.0_pr * PI * x(:,3)/(xyzlimits(2,3)-xyzlimits(1,3)) ) &
                  ) * distance(:) )  ! Asymmetric distribution to encourace initiation of shedding for 2D laminar case 
    
     u(:,n_eng)              = 0.5_pr*SUM(u(:,n_mom(1:dim))**2.0_pr, DIM=2)/u(:,n_den) + u(:,n_den)*cv_in(Nspec) * F_compress * 1.0_pr 
     !Set species
     IF (Nspec>1) u(:,n_spc(:)) = 0.0_pr



     DO i = 1,dim + 2
        tmp1 = MINVAL(u(:,i))
        tmp2 = MAXVAL(u(:,i))
        CALL parallel_global_sum( REALMINVAL=tmp1 )
        CALL parallel_global_sum( REALMAXVAL=tmp2 )
        IF(par_rank .EQ. 0) PRINT *, 'Initialize var', i, tmp1,tmp2
     END DO
             
     nwlt_max = nwlt
     CALL parallel_global_sum(INTEGERMAXVAL=nwlt_max)
     IF(par_rank .EQ. 0 ) PRINT *, 'nwlt_max:', nwlt_max

     ! Initialize URANS variables ( Ksgs_wall = 0, Kdiss_wall = 80*mu/delta_n**2 ) 
     IF ( sgsmodel > sgsmodel_max ) THEN
        u(:,n_var_K) = Ksgs_init*(1.0_pr-EXP(-1.0_pr*distance(:)/delta_n))
        u(:,n_var_W) = Kdiss_init + (80.0_pr*mu_in(Nspec)/(delta_n**2)-Kdiss_init)*EXP(-1.0_pr*distance(:)/delta_n)
        IF(par_rank.EQ.0) PRINT *,'Ksgs_init and Kdiss_init:', Ksgs_init, Kdiss_init
     END IF

  END IF

  ! Initialize Freund zone
  IF ( use_freund_damping .OR. use_freund_convect ) CALL setup_freund()

  IF ( use_freund_damping ) THEN
    freund_damping_target(1:2,1:dim,n_den)    = 1.0_pr 
    freund_damping_target(1:2,1:dim,n_mom(:)) = 0.0_pr 
    freund_damping_target(1:2,1:dim,n_mom(1)) = 1.0_pr 
    freund_damping_target(1:2,1:dim,n_eng)    = 0.5_pr*SUM(freund_damping_target(1:2,1:dim,n_mom(1:dim))**2.0_pr, DIM=3)/freund_damping_target(1:2,1:dim,n_den) &
                                                + freund_damping_target(1:2,1:dim,n_den)*cv_in(Nspec) * F_compress * 1.0_pr 
    ! URANS variables
    IF ( sgsmodel > sgsmodel_max ) THEN
      freund_damping_target(1:2,1:dim,n_var_K) = Ksgs_init
      freund_damping_target(1:2,1:dim,n_var_W) = Kdiss_init
    END IF

    freund_damping_active = .TRUE.  
  END IF
  IF ( use_freund_convect ) THEN 
    freund_convect_active = .TRUE.  
  END IF

  nwlt_total = nwlt
  CALL parallel_global_sum(INTEGER=nwlt_total) 
  IF (par_rank .EQ. 0) THEN
        PRINT *, '********************************************'
        PRINT *, '* j_lev = ',j_lev,        '  *' 
        PRINT *, '* nwlt  = ', nwlt_total,  '  *' 
        PRINT *, '********************************************'
  END IF

END SUBROUTINE user_initial_conditions

  !--********************************  
  ! Arguments
  ! u         - field on adaptive grid
  ! nlocal      - number of active points
  ! ne_local       - number of equations
  ! t         - current time
  !
  ! Global variables used
  ! ifn       - number of equations
  ! x()
  ! xO
  ! y0
  !--******************************** 
  SUBROUTINE user_algebraic_BC (Lu, u, nlocal, ne_local, jlev, meth)
    !--Defines boundary condition type
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: jlev, meth, ne_local, nlocal
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (INOUT) :: Lu
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (IN)    :: u

    INTEGER :: ie, i, ii, shift, denshift,eshift
    INTEGER, DIMENSION (dim) :: velshift 
    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc

    !shift markers for BC implementation
    denshift  = nlocal*(n_den-1)

    IF(Nspec > 1) THEN
       IF (par_rank.EQ.0) PRINT*,'ERROR: the URANS module is not set up for multiple species, requires modification'
       STOP
    END IF 

    DO ie = 1, ne_local
       shift = nlocal*(ie-1)
       !--Go through all Boundary points that are specified
       i_p_face(0) = 1
       DO i=1,dim
          i_p_face(i) = i_p_face(i-1)*3
       END DO
       DO face_type = 0, 3**dim - 1
          face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
          IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
             CALL get_all_indices_by_face (face_type, jlev, nloc, iloc)
             IF(nloc > 0 ) THEN 
                IF( face(1) .EQ. -1  ) THEN  ! y_min (radius_min) face
                   IF(ie >= n_mom(1) .AND. ie <= n_mom(dim)) THEN  !Momentums
                      Lu(shift+iloc(1:nloc)) = u(shift+iloc(1:nloc))       !Dirichlet conditions
                   ELSE IF(ie == n_eng) THEN
                      ! For multi species implementation, will need to average for cv
                      Lu(shift+iloc(1:nloc)) = u(shift+iloc(1:nloc)) - u(denshift+iloc(1:nloc))*cv_in(Nspec) * F_compress * 1.0_pr     !Dirichlet conditions - Isothermal, no slip
                   ! URANS equations
                   ELSE IF(sgsmodel > sgsmodel_max .AND. ie == n_var_K) THEN
                      Lu(shift+iloc(1:nloc)) = u(shift+iloc(1:nloc))    ! Dirichlet condition
                   ELSE IF(sgsmodel > sgsmodel_max .AND. ie == n_var_W) THEN
                      Lu(shift+iloc(1:nloc)) = u(shift+iloc(1:nloc))    ! Dirichlet condition
                   END IF
                END IF
             END IF
          END IF
       END DO
    END DO

  END SUBROUTINE user_algebraic_BC



  SUBROUTINE user_algebraic_BC_diag (Lu_diag, nlocal, ne_local, jlev, meth)
    !--Defines boundary condition type
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: jlev, meth, ne_local, nlocal
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (INOUT) :: Lu_diag

    INTEGER :: ie, i, ii, shift, denshift

    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc
    INTEGER, PARAMETER :: methprev=1

    !BC for both 2D and 3D cases
    INTEGER :: lowvar, highvar

    denshift = nlocal*(n_den-1)

    DO ie = 1, ne_local
       shift = nlocal*(ie-1)
       !--Go through all Boundary points that are specified
       i_p_face(0) = 1
       DO i=1,dim
          i_p_face(i) = i_p_face(i-1)*3
       END DO
       DO face_type = 0, 3**dim - 1
          face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
          IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
             CALL get_all_indices_by_face (face_type, jlev, nloc, iloc)
             IF(nloc > 0 ) THEN 
                IF( face(1) .EQ. -1  ) THEN 
                   IF(ie >= n_mom(1) .AND. ie <= n_mom(dim)) THEN
                      Lu_diag(shift+iloc(1:nloc)) = 1.0_pr  !Dirichlet conditions
                   ELSE IF(ie == n_eng) THEN
                      Lu_diag(shift+iloc(1:nloc)) = 1.0_pr  !Dirichlet conditions
                   ! URANS equations
                   ELSE IF(sgsmodel > sgsmodel_max .AND. ie == n_var_K) THEN
                      Lu_diag(shift+iloc(1:nloc)) = 1.0_pr  !Dirichlet conditions
                   ELSE IF(sgsmodel > sgsmodel_max .AND. ie == n_var_W) THEN
                      Lu_diag(shift+iloc(1:nloc)) = 1.0_pr  !Dirichlet conditions
                   END IF
                END IF
             END IF
          END IF
       END DO
    END DO

  END SUBROUTINE user_algebraic_BC_diag



  SUBROUTINE user_algebraic_BC_rhs (rhs, ne_local, nlocal, jlev)
    !--Sets rhs for boundary conditions
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: ne_local, nlocal, jlev
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (INOUT) :: rhs

    INTEGER :: ie, i, ii, shift, denshift
    INTEGER, PARAMETER ::  meth=1
    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc
    REAL (pr) :: p_bc

    REAL (pr), DIMENSION(nwlt) :: temp, local_sutherland
    REAL (pr) :: tmp1, tmp2

    denshift=nlocal*(n_den-1)

    DO ie = 1, ne_local
       shift = nlocal*(ie-1)
       !--Go through all Boundary points that are specified
       i_p_face(0) = 1
       DO i=1,dim
          i_p_face(i) = i_p_face(i-1)*3
       END DO
       DO face_type = 0, 3**dim - 1
          face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
          IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
             CALL get_all_indices_by_face (face_type, jlev, nloc, iloc)
             IF(nloc > 0 ) THEN 
                IF( face(1) .EQ. -1  ) THEN              
                   IF(ie == n_mom(1)) THEN
                      rhs(shift+iloc(1:nloc)) = 0.0_pr 
                   ELSE IF(ie >= n_mom(2) .AND. ie <= n_mom(dim)) THEN
                      rhs(shift+iloc(1:nloc)) = 0.0_pr 
                   ELSE IF(ie == n_eng) THEN
                      rhs(shift+iloc(1:nloc)) = 0.0_pr !define temp in algebraic BC
                   ! URANS equations
                   ELSE IF(sgsmodel > sgsmodel_max .AND. ie == n_var_K) THEN
                      rhs(shift+iloc(1:nloc)) = 0.0_pr                   ! wall level
                   ELSE IF(sgsmodel > sgsmodel_max .AND. ie == n_var_W) THEN
                      rhs(shift+iloc(1:nloc)) = 80.0_pr*mu_in(Nspec)/(delta_n**2)      ! wall level (delta_n by input)
                      ! variable viscosity
                      IF ( viscmodel .EQ. viscmodel_sutherland ) THEN
                         temp(:) = calc_pressure(u(:,1:n_integrated), nwlt, n_integrated ) / u(:,n_den) / R_in(Nspec) 
                         ! mu_in*(1.0_pr+sutherland_const)/(temp(:)+sutherland_const)*(temp(:))**(1.5_pr)
                         local_sutherland(:) = calc_nondim_sutherland_visc( temp(:), nwlt, sutherland_const )
                         rhs(shift+iloc(1:nloc)) = rhs(shift+iloc(1:nloc)) * local_sutherland( iloc(1:nloc) )
                      END IF
                   END IF
                END IF
             END IF
          END IF
       END DO
    END DO

  END SUBROUTINE user_algebraic_BC_rhs



  FUNCTION user_chi (nlocal, t_local)
    USE parallel
    USE threedobject
    IMPLICIT NONE
    INTEGER,          INTENT (IN) :: nlocal
    REAL (pr),        INTENT (IN) :: t_local
    REAL (pr), DIMENSION (nlocal) :: user_chi
    REAL (pr), DIMENSION (nlocal) :: dist_loc
    REAL(pr), DIMENSION(nlocal,1) :: forcing
    REAL(pr) :: smooth_coeff

    user_chi = 0.0_pr

  END FUNCTION user_chi

  FUNCTION user_mapping ( xlocal, nlocal, t_local )
    USE curvilinear
    USE curvilinear_mesh
    USE wlt_vars!, ONLY: dim 
    USE error_handling 
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: nlocal
    REAL (pr), INTENT (IN) :: t_local
    REAL (pr), DIMENSION (nlocal,dim), INTENT(IN) :: xlocal
    REAL (pr), DIMENSION (nlocal,dim) :: user_mapping, xh

    REAL (pr) :: R1, R2
    REAL (pr), DIMENSION (nlocal) :: theta_0_field,theta_1_field 
    REAL (pr), DIMENSION (nlocal) :: support 

    ! Theta indicates locations of intersaction on cylinder and far-field boundary
    !REAL (pr), PARAMETER :: theta_1=0.3490658503988659_pr, theta_0=1.4835298641951802_pr
    !REAL (pr), PARAMETER :: U_0=-2.0_pr, exp_param=10.0_pr
    REAL (pr), PARAMETER :: theta_1=0.3490658503988659_pr, theta_0=1.3089969389957472_pr
    REAL (pr), PARAMETER :: U_0=-2.0_pr, exp_param=10.0_pr
    
    REAL (pr), PARAMETER :: A_param = PI*(-theta_0 + theta_1) /( 2.0_pr*(PI-theta_0)**3*theta_0)
    REAL (pr), PARAMETER :: B_param = 3.0_pr*PI**2*(theta_0 - theta_1) /( 2.0_pr*(PI-theta_0)**3*theta_0)
    REAL (pr), PARAMETER :: C_param = ( 6.0_pr*PI**2*theta_0**2 - 2.0_pr*PI**3*theta_1 + 2.0_pr*theta_0**3*theta_1 - 3.0_pr*PI* theta_0**2*(theta_0+theta_1) ) /( 2.0_pr*(-PI+theta_0)**3*theta_0)
    REAL (pr), PARAMETER :: D_param = PI*(3.0_pr*PI - 2.*theta_0)*theta_0*(theta_0-theta_1)/( 2.0_pr*(PI-theta_0)**3)

    ! Radial stretching params
    REAL (pr), PARAMETER :: s_wall = 1.2_pr, &                ! Stretching factor for the wall region
                            s_wake = 1.01_pr, &                ! Stretching factor for the wake region
                            psi_wall = 0.1_pr, &                ! fraction of the  computational domain in the 'wall' region for radial stretching
                            radial_trans_param = 50.0_pr      ! Parameter controlling transition from wall to wake stretching 
    REAL(pr) :: stretch_const

    user_mapping(:,1:dim) = xlocal(:,1:dim)


    IF ( dim .GE. 2 .AND. transform_dir(2) .EQ. 1 ) THEN 
      R1 = MINVAL( user_mapping(:,1) )
      R2 = MAXVAL( user_mapping(:,1) )
      CALL parallel_global_sum(REALMINVAL=R1)
      CALL parallel_global_sum(REALMAXVAL=R2)

      ! ******************** Moin and Beaudan 1994 *****************
      ! Radial distribution 
      stretch_const = 1.0_pr + (R2-1.0_pr)/(1.0_pr - s_wake)
      support = 0.5_pr - 0.5_pr * TANH( radial_trans_param*(user_mapping(:,1) - (psi_wall*(R2-R1)+R1)  ) )
      !user_mapping(:,1) = 1.0_pr + (R2 - 1.0_pr)*( 1.0_pr - stretch_factor )/( 1.0_pr - s_wake )
      !user_mapping(:,1) = 1.0_pr + (R2 - 1.0_pr)*( 1.0_pr - stretch_factor )/( 1.0_pr - s_wake )
      user_mapping(:,1) = ( stretch_const - (stretch_const-R1)*EXP( (user_mapping(:,1)-R1 )/( R2-R1) * log(s_wall) ) ) * support &
          + ( ( - (stretch_const-R1)*EXP( psi_wall * log(s_wall))*log(s_wall)  )*( (user_mapping(:,1)-R1 )/( R2-R1) - psi_wall ) + stretch_const - (stretch_const-R1)*EXP( psi_wall * log(s_wall) ) )*(1.0_pr - support)
      ! normalize into a parameter
      !user_mapping(:,1) = R1 * exp( ( user_mapping(:,1)-R1 )/( R2-R1) * log(R2/R1) ) 
      R1 = MINVAL( user_mapping(:,1) )
      R2 = MAXVAL( user_mapping(:,1) )
      CALL parallel_global_sum(REALMINVAL=R1)
      CALL parallel_global_sum(REALMAXVAL=R2)
      ! Normalize into a parameter [0 1]
      user_mapping(:,1) = ( user_mapping(:,1) - R1 ) / (R2-R1) 

      ! Azimuthal stretching params
      theta_0_field = user_mapping(:,2) ! ERIC: can replace with a direct reference to x (computational coordinates) 

      WHERE( theta_0_field .LE. theta_0)
        theta_1_field = ( theta_1/theta_0 * theta_0_field )
      ELSE WHERE( theta_0_field .GE. 2.0_pr * PI - theta_0)
        theta_1_field = ( theta_1/theta_0 * theta_0_field + 2.0_pr * PI * ( 1.0_pr - theta_1 / theta_0 ) )
      ELSEWHERE
        theta_1_field = ( A_param*theta_0_field*theta_0_field*theta_0_field + B_param*theta_0_field*theta_0_field + C_param*theta_0_field + D_param ) 
                    !+ ( (PI-theta_1)/(PI-theta_0) * user_mapping(:,2) + theta_1 - (PI-theta_1)/(PI-theta_0) * theta_0 )        * (1.0_pr - support1 - support2 )&
      END WHERE

      user_mapping(:,2) = (R2 * SIN(theta_1_field) - R1*SIN(theta_0_field(:)) + U_0*SIN(theta_0_field(:))) & 
          *user_mapping(:,1)*user_mapping(:,1) &
          - U_0*SIN(theta_0_field(:))*user_mapping(:,1) + R1*SIN(theta_0_field(:))
      user_mapping(:,1) = (R2 * COS(theta_1_field) - R1*COS(theta_0_field(:)) + U_0*COS(theta_0_field(:)))*user_mapping(:,1)*user_mapping(:,1) - U_0 *COS(theta_0_field(:))* user_mapping(:,1) + R1*COS(theta_0_field(:)) 
      ! ******************** Moin and Beaudan 1994 *****************

      ! ******************** Eric: tanh and aspect ratio stretching *****************
      ! Stretching to optimize aspect ratio towards 1 everywhere 
      !user_mapping(:,1) = R1 * exp( ( user_mapping(:,1)-R1 )/( R2-R1) * log(R2/R1) ) 

      ! Boundary layer stretching: boundary layer on the x_min face
      !IF ( mesh_clustering ) THEN
      !  user_mapping(:,:) = BL_tanh( user_mapping(:,:), nlocal, BL_cluster_param, 1, 1 )
      !  user_mapping(:,:) = channel_tanh( user_mapping(:,:), nlocal, azimuth_cluster_param, 2 )
      !END IF

      ! Cylindrical 
      ! user_mapping(:,:) = cylindrical( user_mapping(:,1:dim), nlocal ) 
      ! ******************** Eric: tanh and aspect ratio stretching *****************
    END IF

    IF( transform_dir(1) .NE. 1 &
        .OR. ( transform_dir(3) .NE. 0 .AND. dim .GE. 3  )  ) THEN
       CALL error( 'Incorrect transform directions for acoustic scattering case.  Check transform_dir()' )
    END IF

  END FUNCTION user_mapping



  FUNCTION user_rhs (u_integrated,scalar)
    IMPLICIT NONE
    REAL (pr), DIMENSION (ng,ne), INTENT(IN) :: u_integrated !1D flat version of integrated variables without BC 
    REAL (pr), DIMENSION (ng), INTENT(IN) :: scalar
    REAL (pr), DIMENSION (ng*ne) :: user_rhs

    INTEGER, PARAMETER :: meth = HIGH_ORDER

    CALL compressible_rhs ( user_rhs, u_integrated, meth) 

    !
    ! URANS models ( sgsmodel = 21 (KEPS), 22 (KOME), 23 (KOMET) ) 
    !

    IF ( sgsmodel > sgsmodel_max ) CALL URANS_compressible_rhs( user_rhs, u_integrated, meth )

    IF ( use_freund_damping ) CALL freund_damping_rhs( user_rhs, u_integrated ) 
    IF ( use_freund_convect ) CALL freund_convect_rhs( user_rhs, u_integrated, j_lev ) 

    IF(hypermodel .NE. 0 .AND. ( hyper_it .LT. hyper_it_limit .OR. hyper_it_limit < 0) ) THEN
      !hyper_mask = set_hyper_mask(ng) 
       CALL hyperbolic(u_integrated,ng,user_rhs)
       IF( par_rank .EQ. 0 ) PRINT *, 'Using Hyperbolic Module'
    END IF

  END FUNCTION user_rhs



  FUNCTION user_Drhs (u_p, u_prev, meth)
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: meth
    REAL (pr), DIMENSION (ng,ne) :: u_p, u_prev
    REAL (pr), DIMENSION (ng*ne) :: user_Drhs

    CALL compressible_Drhs ( user_Drhs, u_p, u_prev, meth ) 

    !
    ! URANS models ( sgsmodel = 21 (KEPS), 22 (KOME), 23 (KOMET) ) 
    !
    IF ( sgsmodel > sgsmodel_max ) CALL URANS_compressible_Drhs( user_Drhs, u_p, u_prev, meth )

    IF ( use_freund_damping ) CALL freund_damping_drhs( user_Drhs, u_p, u_prev )  
    IF ( use_freund_convect ) CALL freund_convect_drhs( user_Drhs, u_p, u_prev, j_lev ) 

    IF (hypermodel .NE. 0 .AND. ( hyper_it .LT. hyper_it_limit .OR. hyper_it_limit < 0)) THEN
      !hyper_mask = set_hyper_mask(ng) 
      CALL hyperbolic(u_p,ng,user_Drhs)
    END IF

  END FUNCTION user_Drhs



  FUNCTION user_Drhs_diag (meth)
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: meth
    REAL (pr), DIMENSION (ng*ne) :: user_Drhs_diag
    REAL (pr), DIMENSION (ng,ne) :: u_prev
    INTEGER :: ie, shift

    DO ie = 1, ne
       shift=(ie-1)*ng
       u_prev(1:ng,ie) = u_prev_timestep(shift+1:shift+ng)
    END DO

    CALL compressible_Drhs_diag(user_Drhs_diag,u_prev,meth )

    !
    ! URANS models ( sgsmodel = 21 (KEPS), 22 (KOME), 23 (KOMET) ) 
    !
    IF ( sgsmodel > sgsmodel_max ) CALL URANS_compressible_Drhs_diag( user_Drhs_diag, u_prev, meth )

    IF ( use_freund_damping ) CALL freund_damping_drhs_diag( user_Drhs_diag, u_prev )  
    IF ( use_freund_convect ) CALL freund_convect_drhs_diag( user_Drhs_diag, u_prev, j_lev ) 

    IF(hypermodel .NE. 0 .AND. ( hyper_it .LT. hyper_it_limit .OR. hyper_it_limit < 0)) THEN
      !hyper_mask = set_hyper_mask(ng) 
      CALL hyperbolic_diag(user_Drhs_diag, ng)
    END IF

  END FUNCTION user_Drhs_diag



  SUBROUTINE user_project (u, p, nlocal, meth)
    USE variable_mapping
    IMPLICIT NONE

    INTEGER, INTENT (IN) :: meth, nlocal
    REAL (pr), DIMENSION (nlocal),     INTENT(INOUT) :: p
    REAL (pr), DIMENSION (nlocal,n_integrated), INTENT(INOUT) :: u

    !no projection for compressible flow
  END SUBROUTINE user_project

  !
  ! Calculate any statitics
  !
  ! startup_flag - 0 when adapting to IC,then 1 inmain integration loop
  !
  SUBROUTINE user_stats ( u, j_mn_local, startup_flag)
    USE parallel
    USE penalization
    USE variable_mapping
    USE error_handling 
    USE vector_util_mod
    IMPLICIT NONE
    REAL (pr), DIMENSION (nwlt,1:n_var), INTENT (IN) :: u
    INTEGER , INTENT (IN) :: j_mn_local 
    INTEGER , INTENT (IN) :: startup_flag

    REAL (pr), DIMENSION (1:nwlt, dim)           :: v
    REAL (pr), DIMENSION (1:nwlt,dim*(dim+1)/2)  :: s_ij
    REAL (pr), DIMENSION (1:nwlt)                :: mu_loc, p, rad
    REAL (pr), DIMENSION(1:nwlt,1:dim) :: stress
    REAL (pr), DIMENSION(1:dim) :: force 
    
    INTEGER :: i,idim

    REAL (pr), DIMENSION(1:nwlt,1:dim) :: x_phys, cyl_norm 

    ! Calculate the surface normal    
    x_phys(:,:) = user_mapping(x, nwlt, t )

    ! Outward facing normal
    rad = SQRT( SUM( x_phys(:,1:2)*x_phys(:,1:2), DIM = 2 ) ) + 1.0e-10_pr
    cyl_norm(:,1) = x_phys(:,1) / rad
    cyl_norm(:,2) = x_phys(:,2) / rad
    IF( dim .GE. 3 ) cyl_norm(:,3:dim) = 0.0_pr

    ! Calculate the normal pressure at each point
    p(:) = calc_pressure(u(:,1:n_integrated), nwlt, n_integrated ) 

    ! Calculate the shear stress
    DO idim = 1,dim
      v(:,idim) = u(:,n_mom(idim))/u(:,n_den)
    END DO
    CALL Sij( v, nwlt, s_ij, mu_loc, .TRUE.)

    
    ! Assume isothermal single species
    DO i = 1,dim*(dim+1)/2
      s_ij(:,i) = 2.0_pr * mu_in(Nspec) * s_ij(:,i)
    END DO
    DO i = 1,dim 
      s_ij(:,i) = s_ij(:,i) - p(:) 
    END DO


    IF (dim .EQ. 2) THEN
      stress(:,1) = cyl_norm(:,1)*s_ij(:,1) + cyl_norm(:,2)*s_ij(:,3)
      stress(:,2) = cyl_norm(:,1)*s_ij(:,3) + cyl_norm(:,2)*s_ij(:,2) 
    ELSE IF (dim .EQ. 3) THEN
      stress(:,1) = cyl_norm(:,1)*s_ij(:,1) + cyl_norm(:,2)*s_ij(:,4) + cyl_norm(:,3)*s_ij(:,5) 
      stress(:,2) = cyl_norm(:,1)*s_ij(:,4) + cyl_norm(:,2)*s_ij(:,2) + cyl_norm(:,3)*s_ij(:,6) 
      stress(:,3) = cyl_norm(:,1)*s_ij(:,5) + cyl_norm(:,2)*s_ij(:,6) + cyl_norm(:,3)*s_ij(:,3) 
    ELSE
      CALL error('Lift/Drag calc only valid for 2D/3D')
    END IF 

    IF( dim .EQ. 2 ) CALL integrate_additional_line( force, stress, 1,  nwlt, dim ) 
    IF( dim .EQ. 3 ) CALL integrate_additional_plane( force, stress, 1,  nwlt, dim ) 


    IF (par_rank .EQ. 0) THEN
      OPEN(UNIT=431, FILE = TRIM(res_path)//TRIM(file_gen)//'_force.stat', STATUS = 'UNKNOWN', form="FORMATTED", POSITION="APPEND")
      WRITE( UNIT=431, ADVANCE='NO',FMT='((E20.12),A)') t, ','
      DO idim = 1,dim-1
         WRITE( UNIT=431, ADVANCE='NO', FMT='((E20.12),A)') force(idim), ','
      END DO
      WRITE( UNIT=431, ADVANCE='YES', FMT='(E20.12)') force(dim)

    END IF

  END SUBROUTINE user_stats

  SUBROUTINE user_cal_force (u, n, t_local, force, drag, lift)
    !--Calculates drag and lift on obstacle using penalization formula
    IMPLICIT NONE

    INTEGER, INTENT (IN) :: n
    REAL (pr), INTENT (IN) :: t_local
    REAL (pr), DIMENSION (dim), INTENT (INOUT) :: force
    REAL (pr), INTENT (OUT) :: drag, lift
    REAL (pr), DIMENSION (n,dim) :: u
    drag = 0.0_PR
    lift = 0.0_PR
    !
    ! There is no obstacle in flow
    !

  END SUBROUTINE user_cal_force

  !
  ! Read input from "case_name"_pde.inp file
  ! case_name is in string file_name read from command line
  ! in read_command_line_input()
  ! 
  SUBROUTINE user_read_input()
    IMPLICIT NONE
    INTEGER :: i, j, ind
    CHARACTER(LEN=8):: numstrng, procnum
    REAL (pr), ALLOCATABLE, DIMENSION(:) :: YR, gammR  ! temporary calculation quantities 

    CALL compressible_read_input()
    CALL freund_read_input()  

    call input_real ('Re',Re,'stop',' Re: Reynolds Number')
    call input_real ('Pra',Pra,'stop',' Pra: Prandtl Number')
    call input_real ('Sc',Sc,'stop',' Sc: Schmidt Number')
    call input_real ('Fr',Fr,'stop',' Fr: Froude Number')
    call input_real ('Ma',Ma,'stop',' Ma: Mach Number')

    call input_integer ('hyper_it_limit',hyper_it_limit,'stop','  hyper_it_limit: number of iterations to apply the hyperbolic viscosity to smooth local instabilities.  hyper_it_limit < 0 for all iterations')
 
    call input_real ('Lczn',Lczn,'stop', 'Lcoordzn: length of coord_zone on one side')
    call input_real ('tfacczn',tfacczn,'stop', 'tfacczn: factor multiply by time for coord_zone time=L/min(c)*tfac, set <0 for no czn modification')
    call input_real ('itfacczn',itfacczn,'stop', 'itfacczn: factor multiply by initial time for coord_zone itime=Lczn/min(c)*tfac, set <0 to set itime=0')



    call input_real ('grav_coeff',grav_coeff,'stop', 'grav_coeff: coefficient for gravity forcing')
    call input_logical ('dynamic_gravity',dynamic_gravity,'stop', 'dynamic_gravity: use dynamic volume forcing to maintain some bulk flow quantity' )

    !4extra - these 8 input definitions - MUST ALSO ADD TO .inp 
    call input_logical ('saveUtil',saveUtil,'stop', 'saveUtil: saveUtil: save utility variable') 
   
     
    ! mesh stretching
    call input_logical ('mesh_clustering',mesh_clustering,'stop', 'mesh_clustering: Turn clustering optimizations on or off (F - use const aspect ration cylindrical formulation)') 
    call input_real ('BL_cluster_param', BL_cluster_param, 'stop',' BL_cluster_param: Mesh clustering parameter for curvilinear stretching in boundary layers: TANH')
    call input_real ('azimuth_cluster_param', azimuth_cluster_param, 'stop',' azimuth_cluster_param: Mesh clustering parameter for curvilinear stretching in azimuthal direction: SINH')
 
    IF (par_rank.EQ.0) THEN
       PRINT *, 'Using Re=', Re     
    END IF

    CALL set_F_compress( 1.0_pr/Ma**2 / gamm(1) )
    ! Set properties
    DO i = 1, Nspec
       IF (GRAV) CALL set_body_force(-grav_coeff, i, 1 )  !sign change for forcing in the positive x-direction
       ! All molecular weights set equal
       !CALL set_thermo_props( 1.0_pr, 1.0_pr/MW_in(i) * gamm_loc(i)/(gamm_loc(i)-1.0_pr), gamm_loc(i), i )  ! To consistantly initialize derived thermo properties 
       CALL set_thermo_props( 1.0_pr/MW_in(i) * gamm(i)/(gamm(i)-1.0_pr), 1.0_pr/MW_in(i), i )  ! To consistantly initialize derived thermo properties 
    END DO

    ! Intermediate quantities for calculating diffusivity
    IF( ALLOCATED( YR ) ) DEALLOCATE( YR )
    IF( ALLOCATED( gammR ) ) DEALLOCATE( gammR )
    ALLOCATE( YR(1:Nspec), gammR(1:Nspec) )
    YR(:) = MW_in(:) / SUM(MW_in(1:Nspec))                                   
    IF( Nspec .GT. 1 ) YR(Nspec) = 1.0_pr - SUM(YR(1:Nspecm)) ! higher accuracy
    gammR = SUM(cp_in(:)*YR(:))/( SUM(cp_in(:)*YR(:))-SUM(YR(:)/MW_in(:)) )   
    DO i = 1, Nspec
       CALL set_dynamic_viscosity( 1.0_pr/Re, i )
       CALL set_conductivity( 1.0_pr/Re/Pra*gammR(i)/(gammR(i)-1.0_pr), i )   
       CALL set_diffusivity( 1.0_pr/Re/Sc, i )
    END DO
    print *, 'gammR',gammR
    print *, 'YR',YR
    DEALLOCATE( YR )
    DEALLOCATE( gammR )


    print *, 'f_comp',F_compress
    print *, 'cp',cp_in
    print *, 'cv',cv_in
    print *, 'R',R_in
    print *, 'mu_in',mu_in
    print *, 'kk_in',kk_in
    print *, 'bd_in',bd_in
    print *, 'gamma',gamm
    print *, 'MW',MW_in
    
     IF (NOT(ALLOCATED(Lxyz))) ALLOCATE(Lxyz(1:dim))

     !pi=4.0_pr*ATAN(1.0_pr)
     Lxyz(1:dim)=xyzlimits(2,1:dim)-xyzlimits(1,1:dim)

  END SUBROUTINE user_read_input

  !
  ! calculate any additional variables
  ! 
  ! arg 
  ! flag  - 0 calledwhile adapting to IC, 1 - called in main time integration loop
  !
  ! These additiona variables are calculated and left in real space.
  !
  SUBROUTINE user_additional_vars( t_local, flag )
    USE penalization
    USE variable_mapping 
    IMPLICIT NONE
    REAL (pr), INTENT (IN) ::  t_local
    INTEGER , INTENT(IN) :: flag ! 0- called during adaption to IC, 1 called during main integration loop
    REAL (pr) :: cp, R, mygr, cmax, uval
    INTEGER :: l, i, j
    REAL (pr), DIMENSION(1,nwlt,MAX(dim,Nspecm,2)) :: du, d2u    
    REAL (pr) :: iterdiff, initdiff, curtau, fofn, deln, delnold, delfofn
    INTEGER, PARAMETER :: meth=1
    REAL (pr), DIMENSION(nwlt,dim*2) :: bigU


    REAL (pr), DIMENSION(nwlt) :: a_wall
    INTEGER :: face_type, nloc, wlt_type, k, j_df
    INTEGER :: ie, i_bnd
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim)   :: face
    INTEGER, DIMENSION(nwlt)  :: iloc
    REAL (pr) :: a_total, rho_wav, rho_m, volume_total, vel_bulk
    REAL (pr), DIMENSION(2) :: tau_wall
    INTEGER,   DIMENSION(2) :: pts_total
    REAL (pr), DIMENSION(nwlt,dim) :: v
    REAL (pr), DIMENSION(dim,nwlt,dim) :: dv, d2v
    INTEGER :: j_wall    !minimum j_level at the wall

    REAL (pr), DIMENSION(nwlt,dim*(dim+1)/2) :: s_ij
    REAL (pr), DIMENSION(nwlt)               :: smod

    LOGICAL, SAVE :: header_flag = .TRUE.

    LOGICAL :: dummy(1:n_var)
 
    IF (saveUtil) THEN
      CALL user_exact_soln ( s_ij(:,1), nwlt,  t_local, dummy) 
      u(:,n_util) = s_ij(:,1) 
    END IF
    IF (imask_obstacle) u(:,nmsk) = penal

    CALL compressible_additional_vars( t_local, flag )


       IF (modczn) THEN
          IF (t>tczn) THEN
             modczn=.FALSE.
             xyzzone(1,1) = czn_min
             xyzzone(2,1) = czn_max            
            IF (par_rank.EQ.0 .AND. verb_level>0) THEN
             PRINT *, '*****************************************************'
             PRINT *, 'FINALIZING TEMPORARY COORD_ZONE'
             PRINT *, 'COORD_ZONE OUTSIDE OF x=[',xyzzone(1,1),',',xyzzone(2,1),']'
             PRINT *, 'TIME: ', t
             PRINT *, 'STOPPING TIME FOR COORD_ZONE: ', tczn
             PRINT *, '*****************************************************'
            END IF
          END IF
       ELSE  
          IF (tfacczn>0.0_pr .AND. itfacczn>0.0_pr .AND. t>itczn) THEN
             itczn=t_end*10.0_pr
             czn_min = xyzzone(1,1)
             czn_max = xyzzone(2,1)
             xyzzone(1,1) = -Lczn
             xyzzone(2,1) =  Lczn
             cczn = MINVAL(SQRT(gamm/MW_in/Ma**2))
             tczn = MAXVAL(ABS(xyzlimits(:,1)))/cczn*tfacczn
            IF (par_rank.EQ.0 .AND. verb_level>0) THEN
             PRINT *, '*****************************************************'
             PRINT *, 'APPLYING TEMPORARY COORD_ZONE'
             PRINT *, 'COORD_ZONE OUTSIDE OF x=[',-Lczn,',',Lczn,']'
             PRINT *, 'MINIMUM WAVE SPEED: ', cczn
             PRINT *, 'MAXIMUM DISTANCE: ', MAXVAL(ABS(xyzlimits(:,1)))
             PRINT *, 'TIME: ', t
             PRINT *, 'STARTING TIME FOR COORD_ZONE: ', itczn
             PRINT *, 'TIME SAFETY FACTOR: ', tfacczn
             PRINT *, 'STOPPING TIME FOR COORD_ZONE: ', tczn
             PRINT *, '*****************************************************'
            END IF
             IF (t<tczn) THEN
                modczn = .TRUE.
             ELSE
                IF (par_rank.EQ.0) PRINT *, '!!!TOO LATE TO START COORD_ZONE: t>tczn!!!'
             END IF
          END IF  
       END IF


  END SUBROUTINE user_additional_vars

  !
  ! calculate any additional scalar variables
  ! 
  ! arg 
  ! flag  - 0 calledwhile adapting to IC, 1 - called in main time integration loop
  !
  SUBROUTINE user_scalar_vars( flag )
    IMPLICIT NONE
    INTEGER , INTENT(IN) :: flag ! 0- called during adaption to IC, 1 called during main integration loop

  END SUBROUTINE user_scalar_vars

  !
  !************ Calculating Scales ***************************
  !
  ! Note the order of the components in the scl array
  ! correspond to u_tn, v_tn, w_tn, u_tn-1, v_tn-1, w_tn-1, u_tn-2, v_tn-2, w_tn-2
  ! 
  SUBROUTINE user_scales(flag,use_default, u_loc, nlocal, ne_local, l_n_var_adapt , l_n_var_adapt_index, &
       scl, scl_fltwt) !add
    USE curvilinear_mesh
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: flag ! 0 during initial adaptation, then 1 during time advancement
    LOGICAL , INTENT(INOUT) :: use_default
    INTEGER, INTENT (IN) :: nlocal, ne_local
    REAL (pr), DIMENSION (1:nlocal,1:ne_local), INTENT (IN) :: u_loc
    LOGICAL , INTENT (IN) :: l_n_var_adapt(ne_local)
    INTEGER , INTENT (IN) :: l_n_var_adapt_index(1:ne_local)
    REAL (pr), DIMENSION (1:ne_local), INTENT (INOUT) :: scl
    REAL (pr) ,  INTENT(IN) ::scl_fltwt !weight for temporal filter on scl
    LOGICAL  , SAVE :: startup_init = .TRUE.

    !
    ! Ignore the output of this routine and use default scales routine
    !
    use_default = .FALSE. 

    IF( .NOT. use_default ) THEN
       CALL compressible_scales(flag,use_default, u_loc, nlocal, ne_local, l_n_var_adapt , l_n_var_adapt_index, scl, scl_fltwt) 
       CALL curv_mesh_scales(flag,use_default, u_loc, nlocal, ne_local, l_n_var_adapt , l_n_var_adapt_index, scl, scl_fltwt) 
       startup_init = .FALSE.
    END IF

  END SUBROUTINE user_scales

  SUBROUTINE user_cal_cfl (use_default, ulocal, cfl_out)
    USE precision
    USE sizes
    USE pde
    USE variable_mapping
    IMPLICIT NONE
    LOGICAL , INTENT(INOUT) :: use_default
    REAL (pr),                                INTENT (INOUT) :: cfl_out
    REAL (pr), DIMENSION (nwlt,n_integrated), INTENT (IN)    :: ulocal

    INTEGER                    :: i, idim
    REAL (pr), DIMENSION(nwlt,dim) :: cfl
    REAL (pr), DIMENSION(nwlt,dim) :: v 
    REAL (pr)                  :: min_h
    REAL (pr) :: min_w, max_w
    REAL (pr) :: inv_vortex_timescale, cfl_vortex

    use_default = .FALSE.
    CALL compressible_cal_cfl (use_default, ulocal, cfl_out)

    IF ( ( use_freund_damping .OR. use_freund_convect) .AND. time_integration_method == TIME_INT_RK ) THEN
      cfl_out = MAX( cfl_out, freund_cal_cfl() )
    END IF 


    IF(dim .EQ. 3) THEN
       min_w = MINVAL(ulocal(:,n_mom(3))/ulocal(:,n_den))
       max_w = MAXVAL(ulocal(:,n_mom(3))/ulocal(:,n_den))
       CALL parallel_global_sum(REALMINVAL=min_w)
       CALL parallel_global_sum(REALMAXVAL=max_w)
    END IF

    IF (par_rank.EQ.0) THEN
       PRINT *, '**********************************************************'
       !PRINT *, 'BL pts = ', 1.0_pr/((Re)**0.5_pr*min_h)
       IF(dim .EQ.3)PRINT *, 'min/max w = ',min_w, max_w
       PRINT *, '**********************************************************'
       
    END IF
  END SUBROUTINE user_cal_cfl

  SUBROUTINE user_init_sgs_model( )
    IMPLICIT NONE
  END SUBROUTINE user_init_sgs_model

  SUBROUTINE  user_sgs_force (u_loc, nlocal)
    USE variable_mapping
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: nlocal
    REAL (pr), DIMENSION (nlocal,n_integrated), INTENT (INOUT) :: u_loc

  END SUBROUTINE  user_sgs_force

  FUNCTION user_sound_speed (u, neq, nwlt)
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: nwlt, neq
    REAL (pr), DIMENSION (nwlt,neq), INTENT(IN) :: u
    REAL (pr), DIMENSION (nwlt) :: user_sound_speed,gam

    user_sound_speed(:) = calc_speed_of_sound( u, nwlt, neq )

  END FUNCTION user_sound_speed

  SUBROUTINE  user_pre_process
    USE variable_mapping
    IMPLICIT NONE

    CALL compressible_pre_process() 

    !IF(sgsmodel > sgsmodel_max) u(:,n_var_K) = MAX(0.0_pr,u(:,n_var_K))


  END SUBROUTINE user_pre_process

  SUBROUTINE  user_post_process
    USE variable_mapping
    IMPLICIT NONE

    CALL compressible_post_process() 

    IF(hypermodel .NE. 0 .AND. ( hyper_it .LT. hyper_it_limit ) ) hyper_it = hyper_it + 1 


  END SUBROUTINE user_post_process

  ! Area integration (physical space) across an additional_plane
  SUBROUTINE integrate_additional_plane( result_out, integrand, i_plane,  nlocal, ne_local ) 
    USE parallel
    USE curvilinear
    IMPLICIT NONE
    INTEGER,                               INTENT(IN)  :: nlocal, ne_local  ! Number of points and number of fields to integrate
    INTEGER,                               INTENT(IN)  :: i_plane 
    REAL (pr), DIMENSION(nlocal,ne_local), INTENT(IN)  :: integrand         ! Field to be integrated
    REAL (pr), DIMENSION(ne_local),        INTENT(OUT) :: result_out        ! Vector of results, reduced across all ranks

    REAL (pr), DIMENSION(1:dim,1:nlocal,1:dim) :: du, du_dummy 
    REAL (pr), DIMENSION(1:nlocal,1:dim)       :: x_phys 

    INTEGER :: ie, i, j, idim, step, ixyz(1:dim), ixyz_plane(1:dim)
    INTEGER :: j_plane_loc, proc
    INTEGER :: dir_1, dir_2 
    INTEGER :: inwlt(1)

    REAL (pr) :: da_plane, tmp1, tmp2, tmp3 

    result_out = 0.0_pr

    ! Only valid in 3 dimensions
    IF( dim .GE. 3 .AND. additional_planes_active .AND. i_plane .LE. n_additional_planes ) THEN

      x_phys(:,:) = user_mapping( x, nlocal, t )
      CALL c_diff_fast (x_phys(1:nlocal, 1:dim), du(1:dim,1:nlocal,1:dim), du_dummy, j_lev, nlocal, HIGH_ORDER, 10, dim, 1, dim, FORCE_RECTILINEAR = .TRUE.)
      DO idim = 1, dim
        IF( transform_dir(idim) .NE. 1 ) THEN  ! account for non-transformed directions
          du(1:dim,:,idim) = 0.0_pr
          du(idim,:,1:dim) = 0.0_pr
          du(idim,:,idim)  = 1.0_pr
        END IF
      END DO

      j_plane_loc = MIN(j_additional_planes,j_lev)
      step = MAX(1,2**(j_lev-j_plane_loc))

      !---------- go through all points on the plane for  j <= j_plane ------------------
      DO idim = 1, dim
        ixyz_plane(idim) = (MINLOC( ABS(xx(0:nxyz(idim)-prd(idim):step,idim) - xyz_planes(idim,i_plane)), DIM = 1  ) - 1 )*step
      END DO

      ! Determine in-plane directions
      idim = 1
      dir_1 = 0
      dir_2 = 0
      DO WHILE ( idim .LE. dim .AND. dir_1 .EQ. 0 ) 
        IF( idim .NE. dir_planes(i_plane) ) dir_1 = idim
        idim = idim + 1
      END DO
      DO WHILE ( idim .LE. dim .AND. dir_2 .EQ. 0 )
        IF( idim .NE. dir_planes(i_plane) ) dir_2 = idim
      END DO

      PRINT *, 'PLANES integration test, directions to integrate:', dir_1, dir_2

      ixyz(dir_planes(i_plane)) = ixyz_plane(dir_planes(i_plane))
      DO i=0,nxyz(dir_1)-prd(dir_1),STEP
        DO j=0,nxyz(dir_2)-prd(dir_2),STEP
          ixyz(dir_1) = i
          ixyz(dir_2) = j

          ! ERIC: j_lev????
          CALL DB_get_proc_by_coordinates (ixyz, j_lev, proc)
          IF (proc.EQ.par_rank) THEN
            CALL get_indices_by_coordinate(1,ixyz(1:dim),j_lev,inwlt(1), 0)

            ! Calculate discrete area
            da_plane = h(j_plane_loc,dir_1)*h(j_plane_loc,dir_2)
            IF ( ANY( transform_dir(1:dim) .EQ. 1 ) ) THEN
              tmp1 = 0.0_pr
              tmp2 = 0.0_pr
              tmp3 = 0.0_pr
              DO idim = 1, dim
                !tmp1 = tmp1 + 1.0_pr*(curvilinear_jacobian(dir_1,idim,inwlt(1))*curvilinear_jacobian(dir_1,idim,inwlt(1)) )
                !tmp2 = tmp2 + 1.0_pr*(curvilinear_jacobian(dir_2,idim,inwlt(1))*curvilinear_jacobian(dir_2,idim,inwlt(1)) )
                !tmp3 = tmp3 + 1.0_pr*(curvilinear_jacobian(dir_1,idim,inwlt(1))*curvilinear_jacobian(dir_2,idim,inwlt(1)) )
                tmp1 = tmp1 + du(idim,inwlt(1),dir_1)*du(idim,inwlt(1),dir_1) 
                tmp2 = tmp2 + du(idim,inwlt(1),dir_2)*du(idim,inwlt(1),dir_2) 
                tmp3 = tmp3 + du(idim,inwlt(1),dir_1)*du(idim,inwlt(1),dir_2) 
              END DO
!print *, 'ERIC: tmp check', tmp1, tmp2, tmp3
              da_plane = SQRT( tmp1 * tmp2 - tmp3*tmp3 )*h(j_plane_loc,dir_1)*h(j_plane_loc,dir_2)
            END IF

            DO ie = 1, ne_local
              result_out(ie) = result_out(ie) + integrand(inwlt(1),ie)* da_plane
            END DO

          END IF
        END DO
      END DO
      CALL parallel_vector_sum( REAL=result_out, LENGTH=ne_local)
    END IF 

  END SUBROUTINE integrate_additional_plane


  ! Length integration (physical space) across an additional_line
  SUBROUTINE integrate_additional_line( result_out, integrand, i_line,  nlocal, ne_local ) 
    USE parallel
    USE curvilinear
    IMPLICIT NONE
    INTEGER,                               INTENT(IN)  :: nlocal, ne_local  ! Number of points and number of fields to integrate
    INTEGER,                               INTENT(IN)  :: i_line 
    REAL (pr), DIMENSION(nlocal,ne_local), INTENT(IN)  :: integrand         ! Field to be integrated
    REAL (pr), DIMENSION(ne_local),        INTENT(OUT) :: result_out        ! Vector of results, reduced across all ranks

    REAL (pr), DIMENSION(1:dim,1:nlocal,1:dim) :: du, du_dummy 

    INTEGER :: ie, i, j, idim, step, ixyz(1:dim), ixyz_line(1:dim)
    INTEGER :: j_line_loc, proc
    INTEGER :: inwlt(1)
    REAL (pr), DIMENSION(1:nlocal,1:dim)       :: x_phys 

    REAL (pr) :: da_line, h_phys

    result_out = 0.0_pr

    ! Only valid in 3 dimensions
    IF( dim .GE. 2 .AND. additional_lines_active .AND. i_line .LE. n_additional_lines ) THEN

      x_phys(:,:) = user_mapping(x, nlocal, t )
      CALL c_diff_fast (x_phys(1:nlocal, 1:dim), du(1:dim,1:nlocal,1:dim), du_dummy, j_lev, nlocal, HIGH_ORDER, 10, dim, 1, dim, FORCE_RECTILINEAR = .TRUE.)
      DO idim = 1, dim
        IF( transform_dir(idim) .NE. 1 ) THEN  ! account for non-transformed directions
          du(1:dim,:,idim) = 0.0_pr
          du(idim,:,1:dim) = 0.0_pr
          du(idim,:,idim)  = 1.0_pr
        END IF
      END DO


      j_line_loc = MIN(j_additional_lines,j_lev)
      step = MAX(1,2**(j_lev-j_line_loc))

      !---------- go through all points on the line for  j <= j_line ------------------
      DO idim = 1, dim
        ixyz_line(idim) = (MINLOC( ABS(xx(0:nxyz(idim)-prd(idim):step,idim) - xyz_lines(idim,i_line)), DIM = 1  ) - 1 )*step
      END DO

      ! Determine in-line direction

      ixyz(1:dim) = ixyz_line(1:dim)
      DO i=0,nxyz(dir_lines(i_line))-prd(dir_lines(i_line)),STEP
        ixyz(dir_lines(i_line)) = i

        CALL DB_get_proc_by_coordinates (ixyz, j_lev, proc)
        IF (proc.EQ.par_rank) THEN
          CALL get_indices_by_coordinate(1,ixyz(1:dim),j_lev,inwlt(1), 0)

          ! Calculate discrete area
          da_line = h(j_line_loc,dir_lines(i_line))
          IF ( ANY( transform_dir(1:dim) .EQ. 1 ) ) THEN
            h_phys = 0.0_pr
            DO idim = 1,dim 
              !h_phys = h_phys + curvilinear_jacobian(dir_lines(i_line),idim,inwlt(1))* curvilinear_jacobian(dir_lines(i_line),idim,inwlt(1))
              h_phys = h_phys + du(idim,inwlt(1),dir_lines(i_line)) * du(idim,inwlt(1),dir_lines(i_line)) 
            END DO
            da_line = SQRT(1.0_pr/h_phys) * h(j_line_loc,dir_lines(i_line)) 
!print *, 'ERIC: area',h(j_line_loc,dir_lines(i_line)), da_line 
          END IF

          DO ie = 1, ne_local
            result_out(ie) = result_out(ie) + integrand(inwlt(1),ie)* da_line
          END DO

        END IF
      END DO

      CALL parallel_vector_sum( REAL=result_out, LENGTH=ne_local)

    END IF 

  END SUBROUTINE integrate_additional_line

END MODULE user_case

