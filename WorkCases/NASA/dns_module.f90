
!-------------------------- Brinkman module --------------------------

MODULE Brinkman_penalization

  USE precision
  USE wlt_vars
  USE wavelet_filters_mod
  USE elliptic_mod
  USE elliptic_vars
  USE wlt_trns_mod
  USE wlt_trns_vars
  USE wlt_trns_util_mod
  USE io_3d_vars
  USE util_mod
  USE util_vars
  USE share_consts
  USE pde
  USE sizes
  USE share_kry
  USE vector_util_mod
  USE field
  USE input_file_reader
  USE additional_nodes
  USE wlt_FWH

  REAL (pr) :: nita

CONTAINS


  SUBROUTINE Brinkman_rhs (user_rhs, u_integrated)
    IMPLICIT NONE
    REAL (pr), DIMENSION (n), INTENT(INOUT) :: user_rhs
    REAL (pr), DIMENSION (ng,ne), INTENT(IN) :: u_integrated !1D flat version of integrated variables without BC 

    INTEGER :: ie, shift
    INTEGER, PARAMETER :: meth=1
!    REAL (pr), DIMENSION (ne,ng,dim) :: dux,duy,duz, d2u
    REAL (pr), DIMENSION (ng,dim)     :: dp
!
! User defined variables
!
!    REAL (pr), DIMENSION (ng,n_integrated,dim) :: F

  REAL (pr), DIMENSION (ng,ne) :: uh
  REAL (pr), DIMENSION (ng) ::  rho, Ux, Uy, e, press, TT
  REAL (pr), DIMENSION (ng) ::  Fx, obstacle
  REAL (pr), DIMENSION (ng,2) :: dFx, dDum 


   obstacle = Brinkman_obstacle (nwlt)

   uh(1:ng,1:ne)=u_integrated(1:ng,1:ne)
     rho(1:ng) = uh(1:ng,1)
     Ux(1:ng)  = uh(1:ng,2)/rho(1:ng)
     Uy(1:ng)  = uh(1:ng,3)/rho(1:ng)
     e(1:ng)   = uh(1:ng,4)
     press(1:ng)   = (gamma-1.0_pr)*(e-0.5_pr*rho*(Ux**2+Uy**2))
     TT(1:ng)  = press*gamma/rho

     Fx = uh(1:nwlt,2)
     !CALL c_diff_fast (Fx, dFx, dDum, j_lev, nwlt, 1, 11,1,1,1)
     user_rhs(ng+1:2*ng) = user_rhs(ng+1:2*ng) -obstacle(1:ng)*Ux(1:ng)/nita


     Fx = uh(1:nwlt,3)
     !CALL c_diff_fast (Fx, dFx, dDum, j_lev, nwlt, 1, 11,1,1,1)
     user_rhs(2*ng+1:3*ng) = user_rhs(2*ng+1:3*ng) -obstacle(1:ng)*Uy(1:ng)/nita

     Fx = uh(1:nwlt,4)
     !CALL c_diff_fast (Fx, dFx, dDum, j_lev, nwlt, 1, 11,1,1,1)
     user_rhs(3*ng+1:4*ng) = user_rhs(3*ng+1:4*ng)- obstacle(1:ng)*(TT(1:ng)-1.0_pr)/nita 
   END SUBROUTINE Brinkman_rhs



! find Jacobian of Right Hand Side of the problem
  SUBROUTINE  Brinkman_Drhs (user_Drhs, u, u_prev, meth)
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: meth
	!u_prev_timestep passed local to cast it into 2dim array to work with vector derivatives
    REAL (pr), DIMENSION (ng,ne) :: u, u_prev
    REAL (pr), DIMENSION (n), INTENT(INOUT) :: user_Drhs

    INTEGER :: ie, shift
    REAL (pr), DIMENSION (2*ne,ng,dim) :: du  ! 1st derivatives for u (in du(1:ne,:,:)) and u_prev_timestep (in du(ne+1:2*ne,:,:))
    REAL (pr), DIMENSION (ne  ,ng,dim) :: d2u ! 2nd derivatives for u 
    REAL (pr), DIMENSION (ne  ,ng,dim) :: du_dummy ! passed when only calculating 1st derivative.
    !Find batter way to do this!! du_dummy with no storage..
!
! User defined variables
!
!    REAL (pr), DIMENSION (ng,n_integrated,dim) :: F

  REAL (pr), DIMENSION (ng,ne) :: uh, uh_b
  REAL (pr), DIMENSION (ng) ::  rho_b, Ux_b, Uy_b, e_b, press_b, TT_b
  REAL (pr), DIMENSION (ng) ::  rho, Ux, Uy, e, press, TT
  REAL (pr), DIMENSION (ng) ::  Fx,obstacle
  REAL (pr), DIMENSION (ng,2) :: dFx, dDum
  REAL (pr), DIMENSION (ng,2) :: dUx_b, dUy_b  
  
  obstacle = Brinkman_obstacle (nwlt)
  
  IF ( TYPE_DB .NE. DB_TYPE_LINES ) THEN
     ! find 1st and 2nd deriviative of u and
     CALL c_diff_fast(u, du(1:ne,:,:), d2u(1:ne,:,:), j_lev, ng, meth, 11, ne , 1, ne )
     !du(1:ne,:,:) = dub(1:ne,:,:) 
     ! find only first deriviative  u_prev_timestep
     CALL c_diff_fast(u_prev, du(ne+1:2*ne,:,:), du_dummy(1:ne,:,:), j_lev, ng, meth, 10, ne , 1, ne )
     
  ELSE !db_lines
     ! Load u and u_prev_timestep in to db
     ! update the db from u
     ! u(:, mn_var:mx_var) -> db%u(db_offset:mx_var-mn_var+1)
     ! db_offset = 1 
     CALL update_db_from_u(  u       , ng ,ne  , 1, ne, 1  ) !Load u
     CALL update_db_from_u(  u_prev , ng ,ne  , 1, ne, ne+1  ) !Load u_prev offset in db by ne+1
     
     ! find 1st and 2nd deriviative of u and
     ! find only first deriviativ e  u_prev_timestep
     CALL c_diff_fast_db(du, d2u, j_lev, ng, meth, 11, 2*ne,&
          1,   &  ! MIN(mn_varD,mn_varD2)
          2*ne,&  ! MAX(mx_varD,mx_varD2)
          1,   &  ! mn_varD  :   min variable in db that we are doing the 1st derivatives for.
          2*ne,&  ! mn_varD  :   max variable in db that we are doing the 1st derivatives for.
          1 ,  &  ! mn_varD2 :   min variable in db that we are doing the 2nd derivatives for.
          dim   )  ! mn_varD2 :   max variable in db that we are doing the 2nd derivatives for.
     
     
  END IF
  !CALL c_diff_fast_db(du_b, du_b, j_lev, ng, meth, 10, ne, 1, ne) !find 1st derivative u_b
  
  
  
  uh(1:ng,1:ne)=u(1:ng,1:ne)
  uh_b(1:ng,1:ne)=u_prev(1:ng,1:ne)


     rho_b(1:ng) = uh_b(1:ng,1)
     Ux_b(1:ng)  = uh_b(1:ng,2)/rho_b(1:ng)
     Uy_b(1:ng)  = uh_b(1:ng,3)/rho_b(1:ng)
     e_b(1:ng)   = uh_b(1:ng,4)
     press_b(1:ng)   = (gamma-1.0_pr)*(e_b-0.5_pr*rho_b*(Ux_b**2+Uy_b**2))
     TT_b(1:ng)  = press_b*gamma/rho_b
 
!-----------
     rho(1:ng) = uh(1:ng,1)
     Ux(1:ng)  = (uh(1:ng,2)-rho*Ux_b)/rho_b(1:ng)
     Uy(1:ng)  = (uh(1:ng,3)-rho*Uy_b)/rho_b(1:ng)
     e(1:ng)   = uh(1:ng,4)
     press(1:ng)   = (gamma-1.0_pr)*(e-0.5_pr*rho*(Ux_b**2+Uy_b**2)-rho_b*(Ux_b*Ux+Uy_b*Uy))
     TT(1:ng)  = gamma*(press/rho_b-press_b*rho/rho_b**2)


     Fx = uh(1:nwlt,2)
     CALL c_diff_fast (Fx, dFx, dDum, j_lev, nwlt, 1, 11,1,1,1)
     user_Drhs(ng+1:2*ng) = user_Drhs(ng+1:2*ng)- obstacle(1:ng)*Ux(1:ng)/nita 

     Fx = uh(1:nwlt,3)
     CALL c_diff_fast (Fx, dFx, dDum, j_lev, nwlt, 1, 11,1,1,1)
     user_Drhs(2*ng+1:3*ng) = user_Drhs(2*ng+1:3*ng)-obstacle(1:ng)*Uy(1:ng)/nita 

     Fx = uh(1:nwlt,4)
     CALL c_diff_fast (Fx, dFx, dDum, j_lev, nwlt, 1, 11,1,1,1)
     user_Drhs(3*ng+1:4*ng) = user_Drhs(3*ng+1:4*ng) - obstacle(1:ng)*TT(1:ng)/nita 


   END SUBROUTINE Brinkman_Drhs




  !
  ! Uses u_prev_timestep, which is global flat (1D ) array set in time_step_cn()
  !
  SUBROUTINE  Brinkman_Drhs_diag (user_Drhs_diag, meth)
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: meth
    REAL (pr), DIMENSION (n), INTENT(INOUT) :: user_Drhs_diag

    INTEGER :: ie, shift
     REAL (pr), DIMENSION (ng,dim) :: du, du2
     REAL (pr), DIMENSION (ne,ng,dim) :: dux,duy,duz, d2u
    REAL (pr), DIMENSION (ne,ng,dim) :: du_dummy ! passed when only calculating 1st derivative.
!
! User defined variables
!
!    REAL (pr), DIMENSION (ng,n_integrated,dim) :: F
    REAL (pr), DIMENSION (ng,ne) :: u_prev
  REAL (pr), DIMENSION (ng) :: obstacle


   obstacle = Brinkman_obstacle (nwlt)


    CALL c_diff_diag(du, du2,j_lev,ng,meth,meth,11)

! Create u_prev from u_prev_timestep
    DO ie = 1, ne
       shift=(ie-1)*ng
       u_prev(1:ng,ie) = u_prev_timestep(shift+1:shift+ng)
    END DO


    !--Form right hand side of Navier--Stokes equations
    IF (dim==1) THEN
    ELSE IF (dim==2) THEN

       user_Drhs_diag(ng+1:ng+ng)=user_Drhs_diag(ng+1:ng+ng)-obstacle(1:ng)/nita/u_prev(1:ng,1) 
       user_Drhs_diag(2*ng+1:2*ng+ng)=user_Drhs_diag(2*ng+1:2*ng+ng)-obstacle(1:ng)/nita/u_prev(1:ng,1) 
       user_Drhs_diag(3*ng+1:3*ng+ng)=user_Drhs_diag(3*ng+1:3*ng+ng)-gamma*(gamma-1.0_pr)*obstacle(1:ng)/nita/u_prev(1:ng,1)


    ELSE IF (dim==3) THEN
    END IF

  END SUBROUTINE Brinkman_Drhs_diag


!!$  FUNCTION user_obstacle (nlocal, t_local ) ! in future release
  FUNCTION Brinkman_obstacle (nlocal)
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: nlocal
!!$    REAL (pr), INTENT (IN) :: t_local ! in future release
    REAL (pr), DIMENSION (nlocal) :: Brinkman_obstacle
!
!  Create field object here, 1 inside and 0 outside
!

!    user_obstacle = 0.0_pr

   Brinkman_obstacle = 0.5_pr*(1.0_pr+TANH(-( SQRT(x(:,1)**2  &
        + x(:,2)**2) - 0.5_pr)/1.0e-2_pr )) 

!   Brinkman_obstacle = 0.5_pr*(1.0_pr+TANH(-( SQRT((x(:,1)-cyl_center(1))**2  &
!        + (x(:,2)-cyl_center(2))**2) - cyl_radius(1))/delta_obstacle )) 

!        +0.5_pr*(1.0_pr+TANH(-( SQRT((x(:,1)-cyl_center(1)-cyl_shift(1))**2  &
!        + (x(:,2)-cyl_center(2)-cyl_shift(2))**2) - cyl_radius(2))/delta_obstacle ))

 END FUNCTION Brinkman_obstacle


END MODULE Brinkman_penalization




!-------------------------- Freund module --------------------------


MODULE Freund_boundary


  USE precision
  USE wlt_vars
  USE wavelet_filters_mod
  USE elliptic_mod
  USE elliptic_vars
  USE wlt_trns_mod
  USE wlt_trns_vars
  USE wlt_trns_util_mod
  USE io_3d_vars
  USE util_mod
  USE util_vars
  USE share_consts
  USE pde
  USE sizes
  USE share_kry
  USE vector_util_mod
  USE field
  USE input_file_reader
  USE additional_nodes
  USE Brinkman_penalization
  USE wlt_FWH

!---- freund nonreflecting bc
  REAL (pr) :: sigma_ol = 1.125_pr, sigma_or = 1.125_pr
  REAL (pr) :: width_l = 2.0_pr, width_r = 2.0_pr
  REAL (pr) :: U_ol = 1.15_pr, U_or = 1.15_pr

  REAL (pr) :: Mach

CONTAINS


  SUBROUTINE Freund_rhs (user_rhs, u_integrated)
    IMPLICIT NONE
    REAL (pr), DIMENSION (n), INTENT(INOUT) :: user_rhs
    REAL (pr), DIMENSION (ng,ne), INTENT(IN) :: u_integrated !1D flat version of integrated variables without BC 

    INTEGER :: ie, shift
    INTEGER, PARAMETER :: meth=1
!    REAL (pr), DIMENSION (ne,ng,dim) :: dux,duy,duz, d2u
    REAL (pr), DIMENSION (ng,dim)     :: dp
!
! User defined variables
!
!    REAL (pr), DIMENSION (ng,n_integrated,dim) :: F

  REAL (pr), DIMENSION (ng,ne) :: uh
  REAL (pr), DIMENSION (ng) ::  Fx
  REAL (pr), DIMENSION (ng,2) :: dFx, dDum 

!--- freund nonreflecting bc variables
  REAL (pr), DIMENSION (nwlt,2) :: sigma_f, U_f


  U_f = convection_f(ng)
  sigma_f = damping_f(ng)

   uh(1:ng,1:ne)=u_integrated(1:ng,1:ne)

     Fx = uh(1:nwlt,1)
     CALL c_diff_fast (Fx, dFx, dDum, j_lev, nwlt, 1, 11,1,1,1)
     user_rhs(1:ng) = user_rhs(1:ng) - U_f(:,1) * dFx(1:ng,1) - sigma_f(:,1)*(Fx-1.0_pr) &
          - U_f(:,2) * dFx(1:ng,2) - sigma_f(:,2)*(Fx-1.0_pr)


     Fx = uh(1:nwlt,2)
     CALL c_diff_fast (Fx, dFx, dDum, j_lev, nwlt, 1, 11,1,1,1)
     user_rhs(ng+1:2*ng) = user_rhs(ng+1:2*ng) &
          - U_f(:,1) * dFx(1:ng,1) - sigma_f(:,1)*(Fx-Mach) &
          - U_f(:,2) * dFx(1:ng,2) - sigma_f(:,2)*(Fx-Mach)


     Fx = uh(1:nwlt,3)
     CALL c_diff_fast (Fx, dFx, dDum, j_lev, nwlt, 1, 11,1,1,1)
     user_rhs(2*ng+1:3*ng) = user_rhs(2*ng+1:3*ng) &
          - U_f(:,1) * dFx(1:ng,1) - sigma_f(:,1)*Fx &
          - U_f(:,2) * dFx(1:ng,2) - sigma_f(:,2)*Fx

     Fx = uh(1:nwlt,4)
     CALL c_diff_fast (Fx, dFx, dDum, j_lev, nwlt, 1, 11,1,1,1)
     user_rhs(3*ng+1:4*ng) = user_rhs(3*ng+1:4*ng)&
          - U_f(:,1) * dFx(1:ng,1) &
          - sigma_f(:,1)*(Fx-Mach**2/2.0_pr - 1.0_pr/gamma/(gamma-1.0_pr)) &
          - U_f(:,2) * dFx(1:ng,2) &
          - sigma_f(:,2)*(Fx-Mach**2/2.0_pr - 1.0_pr/gamma/(gamma-1.0_pr))


   END SUBROUTINE Freund_rhs



! find Jacobian of Right Hand Side of the problem
  SUBROUTINE  Freund_Drhs (user_Drhs, u, u_prev, meth)
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: meth
	!u_prev_timestep passed local to cast it into 2dim array to work with vector derivatives
    REAL (pr), DIMENSION (ng,ne) :: u, u_prev
    REAL (pr), DIMENSION (n), INTENT(INOUT) :: user_Drhs

    INTEGER :: ie, shift
    REAL (pr), DIMENSION (2*ne,ng,dim) :: du  ! 1st derivatives for u (in du(1:ne,:,:)) and u_prev_timestep (in du(ne+1:2*ne,:,:))
    REAL (pr), DIMENSION (ne  ,ng,dim) :: d2u ! 2nd derivatives for u 
    REAL (pr), DIMENSION (ne  ,ng,dim) :: du_dummy ! passed when only calculating 1st derivative.
    !Find batter way to do this!! du_dummy with no storage..
!
! User defined variables
!
!    REAL (pr), DIMENSION (ng,n_integrated,dim) :: F

  REAL (pr), DIMENSION (ng,ne) :: uh, uh_b
  REAL (pr), DIMENSION (ng) ::  Fx
  REAL (pr), DIMENSION (ng,2) :: dFx, dDum
  REAL (pr), DIMENSION (ng,2) :: dUx_b, dUy_b  



!--- freund nonreflecting bc variables
  REAL (pr), DIMENSION (nwlt,2) :: sigma_f, U_f


  U_f = convection_f(ng)
  sigma_f = damping_f(ng)


    IF ( TYPE_DB .NE. DB_TYPE_LINES ) THEN
       ! find 1st and 2nd deriviative of u and
       CALL c_diff_fast(u, du(1:ne,:,:), d2u(1:ne,:,:), j_lev, ng, meth, 11, ne , 1, ne )
!du(1:ne,:,:) = dub(1:ne,:,:) 
       ! find only first deriviative  u_prev_timestep
       CALL c_diff_fast(u_prev, du(ne+1:2*ne,:,:), du_dummy(1:ne,:,:), j_lev, ng, meth, 10, ne , 1, ne )

    ELSE !db_lines
       ! Load u and u_prev_timestep in to db
       ! update the db from u
       ! u(:, mn_var:mx_var) -> db%u(db_offset:mx_var-mn_var+1)
       ! db_offset = 1 
       CALL update_db_from_u(  u       , ng ,ne  , 1, ne, 1  ) !Load u
       CALL update_db_from_u(  u_prev , ng ,ne  , 1, ne, ne+1  ) !Load u_prev offset in db by ne+1

       ! find 1st and 2nd deriviative of u and
       ! find only first deriviativ e  u_prev_timestep
       CALL c_diff_fast_db(du, d2u, j_lev, ng, meth, 11, 2*ne,&
            1,   &  ! MIN(mn_varD,mn_varD2)
            2*ne,&  ! MAX(mx_varD,mx_varD2)
            1,   &  ! mn_varD  :   min variable in db that we are doing the 1st derivatives for.
            2*ne,&  ! mn_varD  :   max variable in db that we are doing the 1st derivatives for.
            1 ,  &  ! mn_varD2 :   min variable in db that we are doing the 2nd derivatives for.
            dim   )  ! mn_varD2 :   max variable in db that we are doing the 2nd derivatives for.


    END IF
	!CALL c_diff_fast_db(du_b, du_b, j_lev, ng, meth, 10, ne, 1, ne) !find 1st derivative u_b



  uh(1:ng,1:ne)=u(1:ng,1:ne)
  uh_b(1:ng,1:ne)=u_prev(1:ng,1:ne)

     Fx = uh(1:nwlt,1)
     CALL c_diff_fast (Fx, dFx, dDum, j_lev, nwlt, 1, 11,1,1,1)
     user_Drhs(1:ng) = user_Drhs(1:ng) - U_f(:,1) * dFx(1:ng,1) - sigma_f(:,1)*Fx &
          - U_f(:,2) * dFx(1:ng,2) - sigma_f(:,2)*Fx

     Fx = uh(1:nwlt,2)
     CALL c_diff_fast (Fx, dFx, dDum, j_lev, nwlt, 1, 11,1,1,1)
     user_Drhs(ng+1:2*ng) = user_Drhs(ng+1:2*ng) &
          - U_f(:,1) * dFx(1:ng,1) - sigma_f(:,1)*Fx &
          - U_f(:,2) * dFx(1:ng,2) - sigma_f(:,2)*Fx

     Fx = uh(1:nwlt,3)
     CALL c_diff_fast (Fx, dFx, dDum, j_lev, nwlt, 1, 11,1,1,1)
     user_Drhs(2*ng+1:3*ng) = user_Drhs(2*ng+1:3*ng) &
          - U_f(:,1) * dFx(1:ng,1) - sigma_f(:,1)*Fx &
          - U_f(:,2) * dFx(1:ng,2) - sigma_f(:,2)*Fx

     Fx = uh(1:nwlt,4)
     CALL c_diff_fast (Fx, dFx, dDum, j_lev, nwlt, 1, 11,1,1,1)
     user_Drhs(3*ng+1:4*ng) = user_Drhs(3*ng+1:4*ng) &
          - U_f(:,1) * dFx(1:ng,1) - sigma_f(:,1)*Fx &
          - U_f(:,2) * dFx(1:ng,2) - sigma_f(:,2)*Fx


   END SUBROUTINE Freund_Drhs




  !
  ! Uses u_prev_timestep, which is global flat (1D ) array set in time_step_cn()
  !
  SUBROUTINE  Freund_Drhs_diag (user_Drhs_diag, meth)
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: meth
    REAL (pr), DIMENSION (n), INTENT(INOUT) :: user_Drhs_diag

    INTEGER :: ie, shift
     REAL (pr), DIMENSION (ng,dim) :: du, du2
     REAL (pr), DIMENSION (ne,ng,dim) :: dux,duy,duz, d2u
    REAL (pr), DIMENSION (ne,ng,dim) :: du_dummy ! passed when only calculating 1st derivative.
!
! User defined variables
!
!    REAL (pr), DIMENSION (ng,n_integrated,dim) :: F
    REAL (pr), DIMENSION (ng,ne) :: u_prev

!--- freund nonreflecting bc variables
  REAL (pr), DIMENSION (nwlt,2) :: sigma_f, U_f


  U_f = convection_f(ng)
  sigma_f = damping_f(ng)

    CALL c_diff_diag(du, du2,j_lev,ng,meth,meth,11)

! Create u_prev from u_prev_timestep
    DO ie = 1, ne
       shift=(ie-1)*ng
       u_prev(1:ng,ie) = u_prev_timestep(shift+1:shift+ng)
    END DO


    !--Form right hand side of Navier--Stokes equations
    IF (dim==1) THEN
    ELSE IF (dim==2) THEN

       user_Drhs_diag(ng+1:ng+ng)=user_Drhs_diag(ng+1:ng+ng)&
            - U_f(:,1) * du(:,1) - sigma_f(:,1) &
            - U_f(:,2) * du(:,2) - sigma_f(:,2)
       user_Drhs_diag(ng+1:ng+ng)=user_Drhs_diag(ng+1:ng+ng) &
            - U_f(:,1) * du(:,1) - sigma_f(:,1) &
            - U_f(:,2) * du(:,2) - sigma_f(:,2)
       user_Drhs_diag(2*ng+1:2*ng+ng)=user_Drhs_diag(2*ng+1:2*ng+ng) &
            - U_f(:,1) * du(:,1) - sigma_f(:,1) &
            - U_f(:,2) * du(:,2) - sigma_f(:,2)       
       user_Drhs_diag(3*ng+1:3*ng+ng)=user_Drhs_diag(3*ng+1:3*ng+ng) &
            - U_f(:,1) * du(:,1) - sigma_f(:,1) &
            - U_f(:,2) * du(:,2) - sigma_f(:,2)


    ELSE IF (dim==3) THEN
    END IF

  END SUBROUTINE Freund_Drhs_diag



SUBROUTINE Freund_cal_cfl (use_default, u, cfl_out)
  USE precision
  USE sizes
  USE pde
  USE parallel
  IMPLICIT NONE
  LOGICAL , INTENT(INOUT) :: use_default
  REAL (pr),                                INTENT (INOUT) :: cfl_out
  REAL (pr), DIMENSION (nwlt,n_integrated), INTENT (IN)    :: u

  INTEGER                    :: i, j
  REAL (pr)                  :: floor
  REAL (pr), DIMENSION (dim) :: cfl
  REAL (pr), DIMENSION(dim,nwlt) :: h_arr


!--- freund nonreflecting bc variables
  REAL (pr), DIMENSION (nwlt,2) ::  U_f


  U_f = convection_f(ng)

  use_default = .FALSE.

  floor = 1e-12_pr
  cfl_out = floor
  
  CALL get_all_local_h (h_arr)
  
  DO i = 1, nwlt
     cfl(1) = (ABS(u(i,2)/u(i,1))+ABS(U_f(i,1))+1.0_pr) * dt/h_arr(1,i)
     cfl(2) = (ABS(u(i,3)/u(i,1))+ABS(U_f(i,2))+1.0_pr) * dt/h_arr(2,i)
     cfl_out = MAX (cfl_out, MAXVAL(cfl))
  END DO
  CALL parallel_global_sum( REALMAXVAL=cfl_out )

END SUBROUTINE Freund_cal_cfl


FUNCTION  convection_f(nlocal)
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: nlocal
    REAL (pr), DIMENSION (nlocal,2):: convection_f
    INTEGER :: i


!artificial convection
  DO i=1,nwlt 
     IF (x(i,1) < xyzlimits(1,1) + width_l) THEN
        convection_f(i,1)=U_ol *((xyzlimits(1,1) + width_l - x(i,1))/width_l)**3
     ELSE IF (x(i,1) > xyzlimits(2,1) - width_r) THEN
        convection_f(i,1)=U_or *((x(i,1) - xyzlimits(2,1) + width_r)/width_r)**3
     ELSE
        convection_f(i,1)=0.0_pr
     END IF

     IF (x(i,2) < xyzlimits(1,2) + width_l) THEN
        convection_f(i,2)=-U_ol *((xyzlimits(1,2) + width_l - x(i,2))/width_l)**3
     ELSE IF (x(i,2) > xyzlimits(2,2) - width_r) THEN
        convection_f(i,2)=U_or *((x(i,2) - xyzlimits(2,2) + width_r)/width_r)**3
     ELSE
        convection_f(i,2)=0.0_pr
     END IF

  END DO

END FUNCTION convection_f


FUNCTION  damping_f(nlocal)
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: nlocal
    REAL (pr), DIMENSION (nlocal,2):: damping_f
    INTEGER :: i


!artificial convection and damping terms
  DO i=1,nwlt 
     IF (x(i,1) < xyzlimits(1,1) + width_l) THEN
        damping_f(i,1)=sigma_ol *((xyzlimits(1,1) + width_l - x(i,1))/width_l)**3
     ELSE IF (x(i,1) > xyzlimits(2,1) - width_r) THEN
        damping_f(i,1)=sigma_or *((x(i,1) - xyzlimits(2,1) + width_r)/width_r)**3
     ELSE
        damping_f(i,1)=0.0_pr
     END IF

     IF (x(i,2) < xyzlimits(1,2) + width_l) THEN
        damping_f(i,2)=sigma_ol *((xyzlimits(1,2) + width_l - x(i,2))/width_l)**3
     ELSE IF (x(i,2) > xyzlimits(2,2) - width_r) THEN
        damping_f(i,2)=sigma_or *((x(i,2) - xyzlimits(2,2) + width_r)/width_r)**3
     ELSE
        damping_f(i,2)=0.0_pr
     END IF

  END DO

END FUNCTION damping_f



END MODULE Freund_boundary



!-------------------------- user module --------------------------


MODULE user_case
  ! CASE DNS - periodic (non-periodic BC are almost done)


  USE precision
  USE wlt_vars
  USE wavelet_filters_mod
  USE elliptic_mod
  USE elliptic_vars
  USE wlt_trns_mod
  USE wlt_trns_vars
  USE wlt_trns_util_mod
  USE io_3d_vars
  USE util_mod
  USE util_vars
  USE share_consts
  USE pde
  USE sizes
  USE share_kry
  USE vector_util_mod
  USE field
  USE input_file_reader
  USE additional_nodes
  USE Brinkman_penalization
  USE Freund_boundary
  USE wlt_FWH
  !
  ! case specific variables
  !
  INTEGER n_var_pressure  ! start of pressure in u array
  INTEGER n_var_obstacle  ! start of obstacle function in u array
  REAL (pr) :: Re
  REAL (pr) :: delta, radius
  REAL (pr):: Pra
  REAL (pr), DIMENSION (2) :: cyl_shift, cyl_radius,cyl_center
  REAL (pr):: delta_obstacle

  LOGICAL :: Brinkman_active, Freund_active



CONTAINS

  !
  ! In user_setup_pde() we setup how many variables are integrated, which are interpolated
  ! to the next times step and if any exeact solution exists to compare to during the run.
  ! We also set the variable names that are used when the result files are written.
  !
  ! The following variables must be setup in this routine:
  !
  !
  ! n_integrated     ! first n_integrated eqns will be acted on for time integration
  ! n_var_additional ! 
  ! n_var
  !
  !
  !
  !
  SUBROUTINE  user_setup_pde ( VERB ) 
    IMPLICIT NONE
    LOGICAL, OPTIONAL :: VERB         ! print debug info
    INTEGER :: i

    PRINT * ,''
    PRINT *, '**********************Setting up PDE*****************'
    PRINT * ,'CASE VORTEX ARRAY '
    PRINT *, '*****************************************************'

    n_integrated = dim + 2 ! rho, rhou, rhov, (rhow), rhoE
    n_time_levels = 1  !--3 time levels Obsolete, don't change

    n_var_time_levels = n_time_levels * n_integrated !--Number of equations (2D velocity at two time levels) Obsolete, don't change

    n_var_additional = 2 ! Additional vars to carry, i.e. mask for obstacle, p
    n_var = n_var_time_levels + n_var_additional !--Total number of variables

    n_var_exact = 0

    n_var_obstacle = n_integrated + 1 !obstacle
    n_var_pressure = n_var !pressure

    !
    !--Allocate logical arrays 
    !  Must be done after setting number of different types of variables
    !  and before setting logical variable mapping arrays
    !
    CALL alloc_variable_mappings


    !
    ! Fill in variable names for default variables for integration meth 2
    ! These may be changed in the call to the user specified routine
    !  user_setup_pde()
    !
    ! In integrated variables (3D)

    IF( dim == 3 ) THEN
       WRITE (u_variable_names(1), u_variable_names_fmt) 'Density_rho  '
       WRITE (u_variable_names(2), u_variable_names_fmt) 'X-Momentum  '
       WRITE (u_variable_names(3), u_variable_names_fmt) 'Y-Momentum  '
       WRITE (u_variable_names(4), u_variable_names_fmt) 'Z-Momentum  '
       WRITE (u_variable_names(5), u_variable_names_fmt) 'E_Total  '
       WRITE (u_variable_names(6), u_variable_names_fmt) 'Obstacle  '
       WRITE (u_variable_names(6), u_variable_names_fmt) 'Pressure  '
    ELSE IF(dim == 2) THEN
       WRITE (u_variable_names(1), u_variable_names_fmt) 'Rho  '
       WRITE (u_variable_names(2), u_variable_names_fmt) 'X_M  '
       WRITE (u_variable_names(3), u_variable_names_fmt) 'Y_M  '
       WRITE (u_variable_names(4), u_variable_names_fmt) 'E_total  '
       WRITE (u_variable_names(5), u_variable_names_fmt) 'Obstacle  '
       WRITE (u_variable_names(6), u_variable_names_fmt) 'Pressure  '
    ELSE IF(dim == 1) THEN
       WRITE (u_variable_names(1), u_variable_names_fmt) 'Density_rho  '
       WRITE (u_variable_names(2), u_variable_names_fmt) 'X-Momentum  '
       WRITE (u_variable_names(3), u_variable_names_fmt) 'E_Total  '
       WRITE (u_variable_names(4), u_variable_names_fmt) 'Obstacle  '
       WRITE (u_variable_names(4), u_variable_names_fmt) 'Pressure  '
    END IF
    !
    ! setup logical variable mapping arrays
    ! This defined which variables are used for adaptation, Which need to be interpololated to the new grid at 
    ! each time step, which are saved, and for which we have an exect solutiuon we want to check at each time step
    !

    !
    ! setup which components we will base grid adaptation on.
    !
    n_var_adapt = .FALSE. !intialize
    n_var_adapt(1:n_integrated,0)     = .TRUE. !--Initially adapted variables at first time level
    n_var_adapt(n_var_obstacle,0)     = .TRUE. !--Initially adapted variables at first time level

    n_var_adapt(1:n_integrated,1)     = .TRUE. !--After first time step adapt on 
    n_var_adapt(n_var_obstacle,1)     = .TRUE. !--After first time step adapt on 

    ! Adapt on everything except the pressure


    
    !--integrated variables at first time level

    !Variables that need to be interpoleted to new adapted grid in initial grid adaptation
    n_var_interpolate        = .FALSE. !intialize
    n_var_interpolate(1:n_integrated,0) = .TRUE.  

    !Variables that need to be interpoleted to new adapted grid at each time step
    n_var_interpolate(1:n_integrated,1) = .TRUE. 

    !
    ! setup which components we have an exact solution for
    !
	n_var_exact_soln = .FALSE. !intialize
!    n_var_exact_soln(1:2+dim,0:1) = .FALSE.
 


    !
    ! setup which variables we will save the solution
    !
	n_var_save = .FALSE. !intialize 
    n_var_save(1:n_var) = .TRUE. ! save all for restarting code

    !
    !--Time level counters for NS integration
    !   Used for integration meth2
    ! n0 is current time step
    ! n1 is t_n-1 time step
    ! n2 is t_n-2 time step
    !
    n0 = 1; n1 = n0+dim; n2 = n1+dim ! obsolete


    !
    ! Set the maximum number of components for which we have an exact solution
    !
!!$    n_var_exact = MAX( COUNT(n_var_exact_soln(:,0)),COUNT(n_var_exact_soln(:,1)) )
    n_var_exact = 0


    !
    ! Setup a scaleCoeff array if we want to tweak the scaling of a variable
    ! ( if scaleCoeff < 1.0 then more points will be added to grid 
    ! Allows one to increase or decrease the sensitivity to a variable.
    ! A small value increases the sensitivity.
    ALLOCATE ( scaleCoeff(1:n_var) )
    scaleCoeff = 1.0_pr


    PRINT *, 'n_integrated = ',n_integrated 
    PRINT *, 'n_time_levels = ',n_time_levels
    PRINT *, 'n_var_time_levels = ',n_var_time_levels 
    PRINT *, 'n_var = ',n_var 
    PRINT *, 'n_var_exact = ',n_var_exact 
    PRINT *, '*******************Variable Names*******************'
    DO i = 1,n_var
       WRITE (*, u_variable_names_fmt) u_variable_names(i)
    END DO
    PRINT *, '****************************************************'

  END SUBROUTINE  user_setup_pde


  !
  ! Set the exact solution for comparison to the simulated solution
  !
  ! u          - array to fill in the exact solution
  ! nlocal       - number of active wavelets   
  ! ne_local        - total number of equations
  ! t          - time of current time step 
  ! l_n_var_exact_soln_index - index into the elements of u for which we need to 
  !                            find the exact solution
  SUBROUTINE  user_exact_soln (u, nlocal,  t_local, l_n_var_exact_soln)  
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: nlocal
    REAL (pr), INTENT (IN) ::  t_local
    REAL (pr), DIMENSION (nlocal,n_var_exact), INTENT (INOUT) :: u
    LOGICAL , INTENT (IN) :: l_n_var_exact_soln(n_var)
    INTEGER :: i

!!$
!!$    !u component of velocity
!!$    u(:,1) = -COS(x(:,1))*SIN(x(:,2))*EXP(-2.0_pr*t_local)
!!$    !v component of velocity
!!$    u(:,2) =  SIN(x(:,1))*COS(x(:,2))*EXP(-2.0_pr*t_local)
!!$    !w component of velocity
!!$    IF(dim == 3) u(:,3) = 0.0_pr


    !WRITE( *,'( "in user_exact_soln t_local, nwlt " , f30.20, 1x , i8.8 )' ) t_local , nlocal
   

  END SUBROUTINE  user_exact_soln
  
  SUBROUTINE user_initial_conditions (u, nlocal, ne_local, t_local, scl, scl_fltwt, iter)
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: nlocal, ne_local
    INTEGER  , INTENT (INOUT) :: iter ! iteration of call while adapting initial grid
    REAL (pr), DIMENSION (nlocal,ne_local), INTENT (INOUT) :: u
    REAL (pr)  :: scl(1:n_var),scl_fltwt
    REAL (pr), INTENT (IN) :: t_local
    INTEGER :: i 
    
    ! 
    ! User defined variables
    !
    REAL (pr), DIMENSION (nlocal) :: p
    
    
    IF( .NOT. (IC_restart .OR. IC_from_file )) THEN
       
       ! 2D, rho, rho u, rho v, rho E initialized
       
        u(:,1) = 1.0_pr +1.e-1_pr*EXP( -LOG(2.0_pr) * ((x(:,1)-0.0_pr)**2 + x(:,2)**2) / 0.04_pr )
        u(:,2) = Mach
        u(:,3) = 0.0_pr
        u(:,4) =(1.0_pr/gamma+ 1.e-1_pr*EXP( -LOG(2.0_pr) * ((x(:,1)-0.0_pr)**2 + x(:,2)**2) / 0.04_pr )) / (gamma-1.0_pr) + Mach**2/2.0_pr

!        u(:,1) = 1.0_pr 
!        u(:,2) = Mach 
!        u(:,3) = 0.0_pr 
!        u(:,4) = (u(:,2)**2+u(:,3)**2)/2.0_pr+1.0_pr/gamma/(gamma-1.0_pr) 
!   u(:,n_var_obstacle) = initial_obstacle (nwlt)
!   u(:,n_var_pressure) = (gamma-1.0_pr)* (u(:,4) - 0.5_pr*(u(:,2)**2+u(:,3)**2)/u(:,1))


   u(:,n_var_obstacle) = 1.0_pr

   u(:,n_var_pressure) = u(:,1)


		WRITE( *,'( "in user_IC t_local, nwlt " , f30.20, 1x , i5.5 )' ) t_local , nlocal
    END IF
	!
	! Set Mean flow
	!
	Umn = 0.0_pr




  END SUBROUTINE user_initial_conditions

  ! Basic format for BC's
  ! Lu = f
  !  Specify
  ! Lu in user_algebraic_BC
  ! Diagonal of d(Lu)/du  in user_algebraic_BC_diag
  ! f in user_algebraic_BC_rh

  SUBROUTINE user_algebraic_BC (Lu, u, nlocal, ne_local, jlev, meth)
    !--Defines boundary condition type
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: jlev, meth, ne_local, nlocal
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (INOUT) :: Lu
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (IN)    :: u

    INTEGER :: ie, i, ii, shift
    !REAL (pr), DIMENSION (ne_nlocal,nlocal,dim) :: du, d2u !uncomment if Neuman BC are used, need to call 
    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc


    !BC for both 2D and 3D cases

    ! Neumann BC not fully functional for Krylov time integration
    !if Neuman BC are used, need to call 
    !CALL c_diff_fast (u, du, d2u, jlev, nlocal, meth, 10, ne_local, 1, ne_local)

    ! Inflow: Freund BC - supersonic INFLOW?????

    !--Go through all Boundary points that are specified
    i_p_face(0) = 1
    DO i=1,dim
       i_p_face(i) = i_p_face(i-1)*3
    END DO
    DO face_type = 0, 3**dim - 1
       face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
       IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
          CALL get_all_indices_by_face (face_type, jlev, nloc, iloc)
          IF(nloc > 0 ) THEN 
             IF( face(1) == -1  ) THEN                          ! Xmin face (entire face) 
                DO ie = 1, ne_local
                   shift = nlocal*(ie-1)
                   Lu(shift+iloc(1:nloc)) = u(shift+iloc(1:nloc))  !Dirichlet conditions
!!$                      Lu(shift+iloc(1:nloc)) = du(ie,iloc(1:nloc),2)  !Neuman conditions
                END DO
             END IF
          END IF
       END IF
    END DO
    
  END SUBROUTINE user_algebraic_BC

  SUBROUTINE user_algebraic_BC_diag (Lu_diag, nlocal, ne_local, jlev, meth)
    !--Defines boundary condition type
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: jlev, meth, ne_local, nlocal
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (INOUT) :: Lu_diag

    INTEGER :: ie, i, ii, shift
    REAL (pr), DIMENSION (nlocal,ne_local) :: du, d2u


    ! REAL (pr), DIMENSION (nlocal,dim) :: du, d2u  !uncomment if Neuman BC are used
    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc
    
    ! Provide Jacobian of user_algebraic_BC in Lu_diag

    !BC for both 2D and 3D cases

    !if Neuman BC are used, need to call  
    ! du and d2u are the diagonals of the matrix operator to perform the desired
    !  derivative operations
    !CALL c_diff_diag ( du, d2u, jlev, nlocal, meth, meth, 10) ! returns du
    !CALL c_diff_diag ( du, d2u, jlev, nlocal, meth, meth, 01) ! returns d2u
    !CALL c_diff_diag ( du, d2u, jlev, nlocal, meth, meth, 11) ! returns du & d2u

    i_p_face(0) = 1
    DO i=1,dim
       i_p_face(i) = i_p_face(i-1)*3
    END DO
    DO face_type = 0, 3**dim - 1
       face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
       IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
          CALL get_all_indices_by_face (face_type, jlev, nloc, iloc)
          IF(nloc > 0 ) THEN 
             IF( face(1) == -1  ) THEN                          ! Xmin face (entire face) 
                DO ie = 1, ne_local
                   shift = nlocal*(ie-1)
                   Lu_diag(shift+iloc(1:nloc)) = 1.0_pr  !Dirichlet conditions
!!$                      Lu(shift+iloc(1:nloc)) = du(ie,iloc(1:nloc),2)  !Neuman conditions
                END DO
             END IF
          END IF
       END IF
    END DO

  END SUBROUTINE user_algebraic_BC_diag



  SUBROUTINE user_algebraic_BC_rhs (rhs, ne_local, nlocal, jlev)
    !--Sets rhs for boundary conditions
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: ne_local, nlocal, jlev
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (INOUT) :: rhs

    INTEGER :: ie, i, ii, shift
     INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc

    !BC for both 2D and 3D cases

    i_p_face(0) = 1
    DO i=1,dim
       i_p_face(i) = i_p_face(i-1)*3
    END DO
    DO face_type = 0, 3**dim - 1
       face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
       IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
          CALL get_all_indices_by_face (face_type, jlev, nloc, iloc)
          IF(nloc > 0 ) THEN 
             IF( face(1) == -1  ) THEN                          ! Xmin face (entire face) 
                   rhs(         iloc(1:nloc)) =  1.0_pr ! rho_l
                   rhs(  nlocal+iloc(1:nloc)) =  Mach ! (rho Ux)_l
                   rhs(2*nlocal+iloc(1:nloc)) =  0.0_pr ! (rho Uy)_l
                   rhs(3*nlocal+iloc(1:nloc)) =  Mach**2/2.0_pr+1.0_pr/gamma/(gamma-1.0_pr) ! (rho e)_l
             END IF
          END IF
       END IF
    END DO

  END SUBROUTINE user_algebraic_BC_rhs


  SUBROUTINE user_evolutionary_BC_rhs (rhs, ne_local, nlocal, jlev)
    !--Sets rhs for boundary conditions
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: ne_local, nlocal, jlev
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (INOUT) :: rhs

    INTEGER :: ie, i, ii, shift
     INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc

    !BC for both 2D and 3D cases

    i_p_face(0) = 1
    DO i=1,dim
       i_p_face(i) = i_p_face(i-1)*3
    END DO
    DO face_type = 0, 3**dim - 1
       face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
       IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
          CALL get_all_indices_by_face (face_type, jlev, nloc, iloc)
          IF(nloc > 0 ) THEN 
             IF( face(1) == -1  ) THEN                          ! Xmin face (entire face) 
                DO ie = 1, ne_local
                   shift = nlocal*(ie-1)
                   rhs(shift+iloc(1:nloc)) = 0.0_pr  
                END DO
             END IF
          END IF
       END IF
    END DO


  END SUBROUTINE user_evolutionary_BC_rhs

  SUBROUTINE user_evolutionary_BC_Drhs (Drhs, ne_local, nlocal, jlev)
    !--Sets rhs for boundary conditions
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: ne_local, nlocal, jlev
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (INOUT) :: Drhs

    INTEGER :: ie, i, ii, shift
     INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc

    !BC for both 2D and 3D cases


    i_p_face(0) = 1
    DO i=1,dim
       i_p_face(i) = i_p_face(i-1)*3
    END DO
    DO face_type = 0, 3**dim - 1
       face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
       IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
          CALL get_all_indices_by_face (face_type, jlev, nloc, iloc)
          IF(nloc > 0 ) THEN 
             IF( face(1) == -1  ) THEN                          ! Xmin face (entire face) 
                DO ie = 1, ne_local
                   shift = nlocal*(ie-1)
                   Drhs(shift+iloc(1:nloc)) = 0.0_pr  
                END DO
             END IF
          END IF
       END IF
    END DO


  END SUBROUTINE user_evolutionary_BC_Drhs



  !This functioality is not used in this particular case
  FUNCTION Laplace (jlev, u, nlocal, ne_local, meth)
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: jlev, nlocal, ne_local, meth
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (INOUT) :: u
    REAL (pr), DIMENSION (nlocal*ne_local) :: Laplace

    INTEGER :: i, ii, ie, shift, meth1, meth2, idim
    REAL (pr), DIMENSION (ne_local ,nlocal,dim) :: du
    REAL (pr), DIMENSION (ne_local*dim ,nlocal,dim) :: d2u
    REAL (pr), DIMENSION (ne_local*dim ,nlocal,dim) :: du_dummy ! passed when only calculating 1st derivative.
    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc

    meth1 = meth + 2
    meth2 = meth + 4
    !
	! Find 1st deriviative of u. 
    !
	CALL c_diff_fast (u, du, d2u, jlev, nlocal, meth1, 10, ne_local, 1, ne_local)
!PRINT *,'Laplace du', MINVAL(du), MAXVAL(du)

    !
	! Find 2nd deriviative of u.  d( du ) 
    !
	IF ( TYPE_DB .NE. DB_TYPE_LINES ) THEN 
		CALL c_diff_fast(du, d2u, du_dummy, jlev, nlocal, meth2, 10, ne_local*dim, 1, ne_local*dim )
	ELSE !db_lines

		! Load du  into db
		DO ie=1,ne_local		
			CALL update_db_from_u(  du(ie,1:nlocal,1:dim)  , nlocal ,dim  , 1, dim, 1+(ne_local-1)*dim  ) !Load du	    
		END DO

		! find  2nd deriviative of u (so we first derivative of du/dx)
		CALL c_diff_fast_db(d2u, du_dummy, jlev, nlocal, meth2, 10, ne_local*dim,&
			 1,         &  ! MIN(mn_varD,mn_varD2)
			 ne_local*dim,    &  ! MAX(mx_varD,mx_varD2)
			 1,         &  ! mn_varD  :   min variable in db that we are doing the 1st derivatives for.
			 ne_local*dim,    &  ! mn_varD  :   max variable in db that we are doing the 1st derivatives for.
			 0 ,        &  ! mn_varD2 :   min variable in db that we are doing the 2nd derivatives for.
			 0     )  ! mn_varD2 :   max variable in db that we are doing the 2nd derivatives for.
!PRINT *,'Laplace d2u', MINVAL(d2u), MAXVAL(d2u)

	END IF
    ! Now:
	! d2u(1,:,1) = d^2 U/ d^2 x
	! d2u(1,:,2) = d^2 U/ d^2 y
	! d2u(1,:,3) = d^2 U/ d^2 z
	! d2u(2,:,1) = d^2 V/ d^2 x
	! d2u(2,:,2) = d^2 V/ d^2 y
	! d2u(2,:,3) = d^2 V/ d^2 z
	! d2u(3,:,1) = d^2 W/ d^2 x
	! d2u(3,:,2) = d^2 W/ d^2 y
	! d2u(3,:,3) = d^2 W/ d^2 z

    DO ie = 1, ne_local
       shift=(ie-1)*nlocal
       !--- Internal points
       !--- div(grad)
	   idim = 1
       Laplace(shift+1:shift+Nwlt_lev(jlev,0)) = d2u( (ie-1)*dim+idim ,1:Nwlt_lev(jlev,0),1)
       DO idim = 2,dim
          Laplace(shift+1:shift+Nwlt_lev(jlev,0)) = Laplace(shift+1:shift+Nwlt_lev(jlev,0)) + &
				d2u((ie-1)*dim+idim ,1:Nwlt_lev(jlev,0),idim)
       END DO
       !--Boundary points
       i_p_face(0) = 1
       DO i=1,dim
          i_p_face(i) = i_p_face(i-1)*3
       END DO
       DO face_type = 0, 3**dim - 1
          face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
          IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
             CALL get_all_indices_by_face (face_type, jlev, nloc, iloc)
             IF(nloc > 0 .AND. dim == 3) THEN ! 3-D case only
                IF( ALL( face == (/-1,0,0/) ) ) THEN  ! Xmin face, no edges or corners
                   Laplace(shift+iloc(1:nloc)) = u(shift+iloc(1:nloc))  !Dirichlet conditions
                ELSE IF( ALL( face == (/1,0,0/) ) ) THEN  ! Xmax face, no edges or corners
                   Laplace(shift+iloc(1:nloc)) = u(shift+iloc(1:nloc))  !Dirichlet conditions
!!$                Laplace(shift+iloc(1:nloc)) = du(ie,iloc(1:nloc),1)  !Neuman conditions
                ELSE IF( ALL( face(2:dim) == (/-1,0/) ) ) THEN  ! Ymin face, edges || to Z, no corners
                   Laplace(shift+iloc(1:nloc)) = u(shift+iloc(1:nloc))  !Dirichlet conditions
!!$                Laplace(shift+iloc(1:nloc)) = du(ie,iloc(1:nloc),2)  !Neuman conditions
                ELSE IF( ALL( face(2:dim) == (/1,0/) ) ) THEN  ! Ymax face, edges || to Z, no corners
                   Laplace(shift+iloc(1:nloc)) = u(shift+iloc(1:nloc))  !Dirichlet conditions
!!$                Laplace(shift+iloc(1:nloc)) = du(ie,iloc(1:nloc),2)  !Neuman conditions
                ELSE IF( face(3) == -1 ) THEN  ! entire Zmin face
                   Laplace(shift+iloc(1:nloc)) = u(shift+iloc(1:nloc))  !Dirichlet conditions
!!$                Laplace(shift+iloc(1:nloc)) = du(ie,iloc(1:nloc),3)  !Neuman conditions
                ELSE IF( face(3) == 1 ) THEN  ! entire Zmax face
                   Laplace(shift+iloc(1:nloc)) = u(shift+iloc(1:nloc))  !Dirichlet conditions
!!$                Laplace(shift+iloc(1:nloc)) = du(ie,iloc(1:nloc),3)  !Neuman conditions
                END IF
             END IF
          END IF
       END DO
    END DO
  END FUNCTION Laplace

  !This functioality is not used in this particular case
  FUNCTION Laplace_diag (jlev,  nlocal, ne_local, meth)
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: jlev, nlocal, ne_local, meth
    REAL (pr), DIMENSION (nlocal*ne_local) :: Laplace_diag

    INTEGER :: i, ii, ie, shift, meth1, meth2
    REAL (pr), DIMENSION (nlocal,dim) :: du, d2u
    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc

    meth1 = meth + 2
    meth2 = meth + 4

!PRINT *,'IN Laplace_diag, ne_local = ', ne_local

    DO ie = 1, ne_local
       shift=(ie-1)*nlocal
       !--- div(grad)
!PRINT *,'CAlling c_diff_diag from Laplace_diag() '
!PRINT *,'--- jlev, nlocal, meth1, meth2', jlev, nlocal, meth1, meth2

       CALL c_diff_diag ( du, d2u, jlev, nlocal, meth1, meth2, -11)

       Laplace_diag(shift+1:shift+Nwlt_lev(jlev,0)) = d2u(1:Nwlt_lev(jlev,0),1) + d2u(1:Nwlt_lev(jlev,0),2)
       IF (dim==3) Laplace_diag(shift+1:shift+Nwlt_lev(jlev,0)) = &
            Laplace_diag(shift+1:shift+Nwlt_lev(jlev,0))+d2u(1:Nwlt_lev(jlev,0),3)

       !--Boundary points
       i_p_face(0) = 1
       DO i=1,dim
          i_p_face(i) = i_p_face(i-1)*3
       END DO
       DO face_type = 0, 3**dim - 1
          face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
          IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
             CALL get_all_indices_by_face (face_type, jlev, nloc, iloc)
             IF(nloc > 0 .AND. dim == 3) THEN ! 3-D case only
                IF( ALL( face == (/-1,0,0/) ) ) THEN                         ! Xmin face, no edges or corners
                   Laplace_diag(shift+iloc(1:nloc)) = 1.0_pr                 !Dirichlet conditions
                ELSE IF( ALL( face == (/1,0,0/) ) ) THEN                     ! Xmax face, no edges or corners
                   Laplace_diag(shift+iloc(1:nloc)) = 1.0_pr                 !Dirichlet conditions
!!$                Laplace_diag(shift+iloc(1:nloc)) = du(iloc(1:nloc),1)     !Neuman conditions
                ELSE IF( ALL( face(2:dim) == (/-1,0/) ) ) THEN               ! Ymin face, edges || to Z, no corners
                   Laplace_diag(shift+iloc(1:nloc)) = 1.0_pr                 !Dirichlet conditions
!!$                Laplace_diag(shift+iloc(1:nloc)) = du(iloc(1:nloc),2)     !Neuman conditions
                ELSE IF( ALL( face(2:dim) == (/1,0/) ) ) THEN                ! Ymax face, edges || to Z, no corners
                   Laplace_diag(shift+iloc(1:nloc)) = 1.0_pr                 !Dirichlet conditions
!!$                Laplace_diag(shift+iloc(1:nloc)) = du(iloc(1:nloc),2)     !Neuman conditions
                ELSE IF( face(3) == -1 ) THEN                                ! entire Zmin face
                   Laplace_diag(shift+iloc(1:nloc)) = 1.0_pr                 !Dirichlet conditions
!!$                Laplace_diag(shift+iloc(1:nloc)) = du(iloc(1:nloc),3)     !Neuman conditions
                ELSE IF( face(3) == 1 ) THEN                                 ! entire Zmax face
                   Laplace_diag(shift+iloc(1:nloc)) = 1.0_pr                 !Dirichlet conditions
!!$                Laplace_diag(shift+iloc(1:nloc)) = du(iloc(1:nloc),3)     !Neuman conditions
                END IF
             END IF
          END IF
       END DO
    END DO
  END FUNCTION Laplace_diag

  ! Define N-S for interior points

  FUNCTION user_rhs (u_integrated,p)
    IMPLICIT NONE
    REAL (pr), DIMENSION (ng,ne), INTENT(IN) :: u_integrated !1D flat version of integrated variables without BC 
    REAL (pr), DIMENSION (ng), INTENT(IN) :: p
    REAL (pr), DIMENSION (n) :: user_rhs

    INTEGER :: ie, shift
    INTEGER, PARAMETER :: meth=1
!    REAL (pr), DIMENSION (ne,ng,dim) :: dux,duy,duz, d2u
    REAL (pr), DIMENSION (ng,dim)     :: dp
!
! User defined variables
!
!    REAL (pr), DIMENSION (ng,n_integrated,dim) :: F

  REAL (pr), DIMENSION (ng,ne) :: uh
  REAL (pr), DIMENSION (ng,2) :: du, duxy, d2u
  REAL (pr), DIMENSION (ng) ::  rho, Ux, Uy, e, press, TT, mu
  REAL (pr), DIMENSION (ng) ::  tau11, tau22, tau12, Fx, Fy
  REAL (pr), DIMENSION (ng,2) :: dUx, dUy,dTT,dFx, dFy, dDum


   uh(1:ng,1:ne)=u_integrated(1:ng,1:ne)
     rho(1:ng) = uh(1:ng,1)
     Ux(1:ng)  = uh(1:ng,2)/rho(1:ng)
     Uy(1:ng)  = uh(1:ng,3)/rho(1:ng)
     e(1:ng)   = uh(1:ng,4)
     press(1:ng)   = (gamma-1.0_pr)*(e-0.5_pr*rho*(Ux**2+Uy**2))
     TT(1:ng)  = press*gamma/rho
     mu(1:ng)  = TT**0.76_pr
     CALL c_diff_fast (Ux, dUx, dDum, j_lev, ng, 1, 10,1,1,1) ! Full derivative, dUx = d/dx(Ux)
     CALL c_diff_fast (Uy, dUy, dDum, j_lev, ng, 1, 10,1,1,1)
     CALL c_diff_fast (TT, dTT, dDum, j_lev, ng, 1, 10,1,1,1)

     tau11 = mu*( 4.0_pr/3.0_pr*dUx(:,1)-2.0_pr/3.0_pr*dUy(:,2) )
     tau22 = mu*(-2.0_pr/3.0_pr*dUx(:,1)+4.0_pr/3.0_pr*dUy(:,2) )
     tau12 = mu*( dUx(:,2)+dUy(:,1) )
     
!     CALL c_diff_fast (tau11, dtau11, dDum, j_lev, ng, 1, 10,1,1,1)
!     CALL c_diff_fast (tau12, dtau12, dDum, j_lev, ng, 1, 10,1,1,1)
!     CALL c_diff_fast (tau22, dtau22, dDum, j_lev, ng, 1, 10,1,1,1)

     Fx = -rho*Ux ! Fluxes
     Fy = -rho*Uy
     CALL c_diff_fast (Fx, dFx, dDum, j_lev, ng, 1, 10,1,1,1)
     CALL c_diff_fast (Fy, dFy, dDum, j_lev, ng, 1, 10,1,1,1)
     user_rhs(1:ng) = dFx(1:ng,1) + dFy(1:ng,2)


     Fx = -rho*Ux**2-press+tau11/Re ! Fluxes
     Fy = -rho*Ux*Uy+tau12/Re
        
     CALL c_diff_fast (Fx, dFx, dDum, j_lev, ng, 1, 10,1,1,1)
     CALL c_diff_fast (Fy, dFy, dDum, j_lev, ng, 1, 10,1,1,1)
     
     user_rhs(ng+1:2*ng) = dFx(1:ng,1) + dFy(1:ng,2)


     Fx = -rho*Ux*Uy+tau12/Re
     Fy = -rho*Uy**2-press+tau22/Re
  
     CALL c_diff_fast (Fx, dFx, dDum, j_lev, ng, 1, 10,1,1,1)
     CALL c_diff_fast (Fy, dFy, dDum, j_lev, ng, 1, 10,1,1,1)

     user_rhs(2*ng+1:3*ng) = dFx(1:ng,1) + dFy(1:ng,2)


     Fx = -(e+press)*Ux+(Ux*tau11+Uy*tau12)/Re &
          +mu/Pra*dTT(:,1)/Re/(gamma-1.0_pr)
     Fy = -(e+press)*Uy+(Ux*tau12+Uy*tau22)/Re &
          +mu/Pra*dTT(:,2)/Re/(gamma-1.0_pr) 
        
     CALL c_diff_fast (Fx, dFx, dDum, j_lev, ng, 1, 10,1,1,1)
     CALL c_diff_fast (Fy, dFy, dDum, j_lev, ng, 1, 10,1,1,1)
     user_rhs(3*ng+1:4*ng) = dFx(1:ng,1) + dFy(1:ng,2)

     IF (Brinkman_active) CALL Brinkman_rhs(user_rhs, u_integrated)
     IF (Freund_active)   CALL Freund_rhs(user_rhs, u_integrated)
     
     !--Set operator on boundaries
     IF( Nwlt_lev(j_lev,1) >  Nwlt_lev(j_lev,0) ) CALL user_evolutionary_BC_rhs (user_rhs, ne, ng,j_lev)
     
  END FUNCTION user_rhs


! find Jacobian of Right Hand Side of the problem
  FUNCTION user_Drhs (u, u_prev, meth)
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: meth
	!u_prev_timestep passed local to cast it into 2dim array to work with vector derivatives
    REAL (pr), DIMENSION (ng,ne) :: u, u_prev
    REAL (pr), DIMENSION (n) :: user_Drhs

    INTEGER :: ie, shift
    REAL (pr), DIMENSION (2*ne,ng,dim) :: du  ! 1st derivatives for u (in du(1:ne,:,:)) and u_prev_timestep (in du(ne+1:2*ne,:,:))
    REAL (pr), DIMENSION (ne  ,ng,dim) :: d2u ! 2nd derivatives for u 
    REAL (pr), DIMENSION (ne  ,ng,dim) :: du_dummy ! passed when only calculating 1st derivative.
    !Find batter way to do this!! du_dummy with no storage..

!
! User defined variables
!
    REAL (pr), DIMENSION (ng,n_integrated,dim) :: F

  REAL (pr), DIMENSION (ng,ne) :: uh, uh_b
  REAL (pr), DIMENSION (ng) ::  rho_b, Ux_b, Uy_b, e_b, press_b, TT_b, mu_b
  REAL (pr), DIMENSION (ng) ::  tau11_b, tau22_b, tau12_b
  REAL (pr), DIMENSION (ng) ::  rho, Ux, Uy, e, press, TT, mu
  REAL (pr), DIMENSION (ng) ::  tau11, tau22, tau12, Fx, Fy
  REAL (pr), DIMENSION (ng,2) :: dUx, dUy,dTT, dFx, dFy, dDum
  REAL (pr), DIMENSION (ng,2) :: dUx_b, dUy_b  



    IF ( TYPE_DB .NE. DB_TYPE_LINES ) THEN
       ! find 1st and 2nd deriviative of u and
       CALL c_diff_fast(u, du(1:ne,:,:), d2u(1:ne,:,:), j_lev, ng, meth, 11, ne , 1, ne )
!du(1:ne,:,:) = dub(1:ne,:,:) 
       ! find only first deriviative  u_prev_timestep
       CALL c_diff_fast(u_prev, du(ne+1:2*ne,:,:), du_dummy(1:ne,:,:), j_lev, ng, meth, 10, ne , 1, ne )

    ELSE !db
       ! Load u and u_prev_timestep in to db
       ! update the db from u
       ! u(:, mn_var:mx_var) -> db%u(db_offset:mx_var-mn_var+1)
       ! db_offset = 1 
       CALL update_db_from_u(  u       , ng ,ne  , 1, ne, 1  ) !Load u
       CALL update_db_from_u(  u_prev , ng ,ne  , 1, ne, ne+1  ) !Load u_prev offset in db by ne+1

       ! find 1st and 2nd deriviative of u and
       ! find only first deriviativ e  u_prev_timestep
       CALL c_diff_fast_db(du, d2u, j_lev, ng, meth, 11, 2*ne,&
            1,   &  ! MIN(mn_varD,mn_varD2)
            2*ne,&  ! MAX(mx_varD,mx_varD2)
            1,   &  ! mn_varD  :   min variable in db that we are doing the 1st derivatives for.
            2*ne,&  ! mn_varD  :   max variable in db that we are doing the 1st derivatives for.
            1 ,  &  ! mn_varD2 :   min variable in db that we are doing the 2nd derivatives for.
            dim   )  ! mn_varD2 :   max variable in db that we are doing the 2nd derivatives for.


    END IF
	!CALL c_diff_fast_db(du_b, du_b, j_lev, ng, meth, 10, ne, 1, ne) !find 1st derivative u_b



  uh(1:ng,1:ne)=u(1:ng,1:ne)
  uh_b(1:ng,1:ne)=u_prev(1:ng,1:ne)


     rho_b(1:ng) = uh_b(1:ng,1)
     Ux_b(1:ng)  = uh_b(1:ng,2)/rho_b(1:ng)
     Uy_b(1:ng)  = uh_b(1:ng,3)/rho_b(1:ng)
     e_b(1:ng)   = uh_b(1:ng,4)
     press_b(1:ng)   = (gamma-1.0_pr)*(e_b-0.5_pr*rho_b*(Ux_b**2+Uy_b**2))
     TT_b(1:ng)  = press_b*gamma/rho_b
     mu_b(1:ng)  = TT_b**0.76_pr
 
!-----------
     rho(1:ng) = uh(1:ng,1)
     Ux(1:ng)  = (uh(1:ng,2)-rho*Ux_b)/rho_b(1:ng)
     Uy(1:ng)  = (uh(1:ng,3)-rho*Uy_b)/rho_b(1:ng)
     e(1:ng)   = uh(1:ng,4)
     press(1:ng)   = (gamma-1.0_pr)*(e-0.5_pr*rho*(Ux_b**2+Uy_b**2)-rho_b*(Ux_b*Ux+Uy_b*Uy))
     TT(1:ng)  = gamma*(press/rho_b-press_b*rho/rho_b**2)
     mu(1:ng)  = 0.76_pr*mu_b/TT_b*TT

     CALL c_diff_fast (Ux_b, dUx_b, dDum, j_lev, ng, 1, 10,1,1,1)
     CALL c_diff_fast (Uy_b, dUy_b, dDum, j_lev, ng, 1, 10,1,1,1)
     CALL c_diff_fast (Ux, dUx, dDum, j_lev, ng, 1, 10,1,1,1)
     CALL c_diff_fast (Uy, dUy, dDum, j_lev, ng, 1, 10,1,1,1)
     CALL c_diff_fast (TT, dTT, dDum, j_lev, ng, 1, 10,1,1,1)

     tau11_b = mu_b*( 4.0_pr/3.0_pr*dUx_b(:,1)-2.0_pr/3.0_pr*dUy_b(:,2) )
     tau22_b = mu_b*(-2.0_pr/3.0_pr*dUx_b(:,1)+4.0_pr/3.0_pr*dUy_b(:,2) )
     tau12_b = mu_b*( dUx_b(:,2)+dUy_b(:,1) )

     tau11 = mu_b*( 4.0_pr/3.0_pr*dUx(:,1)-2.0_pr/3.0_pr*dUy(:,2) ) &
           + mu*( 4.0_pr/3.0_pr*dUx_b(:,1)-2.0_pr/3.0_pr*dUy_b(:,2) )
     tau22 = mu_b*(-2.0_pr/3.0_pr*dUx(:,1)+4.0_pr/3.0_pr*dUy(:,2) ) &
           + mu*(-2.0_pr/3.0_pr*dUx_b(:,1)+4.0_pr/3.0_pr*dUy_b(:,2) )
     tau12 = mu_b*( dUx(:,2)+dUy(:,1) ) + mu*( dUx_b(:,2)+dUy_b(:,1) )


     Fx = -rho_b*Ux-rho*Ux_b
     Fy = -rho_b*Uy-rho*Uy_b

     CALL c_diff_fast (Fx, dFx, dDum, j_lev, ng, 1, 10,1,1,1)
     CALL c_diff_fast (Fy, dFy, dDum, j_lev, ng, 1, 10,1,1,1)
     user_Drhs(1:ng) = dFx(1:ng,1) + dFy(1:ng,2)


     Fx = -rho_b*Ux*2.0_pr*Ux_b-rho*Ux_b**2-press+tau11/Re
     Fy = -rho_b*(Ux_b*Uy+Ux*Uy_b)-rho*Ux_b*Uy_b+tau12/Re
        
     CALL c_diff_fast (Fx, dFx, dDum, j_lev, ng, 1, 10,1,1,1)
     CALL c_diff_fast (Fy, dFy, dDum, j_lev, ng, 1, 10,1,1,1)
     
     user_Drhs(ng+1:2*ng) = dFx(1:ng,1) + dFy(1:ng,2)


     Fx = -rho_b*(Ux*Uy_b+Ux_b*Uy)-rho*Ux_b*Uy_b+tau12/Re
     Fy = -rho_b*Uy*2.0_pr*Uy_b-rho*Uy_b**2-press+tau22/Re

  
     CALL c_diff_fast (Fx, dFx, dDum, j_lev, ng, 1, 10,1,1,1)
     CALL c_diff_fast (Fy, dFy, dDum, j_lev, ng, 1, 10,1,1,1)

     user_Drhs(2*ng+1:3*ng) = dFx(1:ng,1) + dFy(1:ng,2)


     Fx = -(e_b+press_b)*Ux -(e+press)*Ux_b &
          +(Ux*tau11_b+Uy*tau12_b+Ux_b*tau11+Uy_b*tau12)/Re &
          +mu/Pra*dTT(:,1)/Re/(gamma-1.0_pr)
     Fy = -(e_b+press_b)*Uy -(e+press)*Uy_b &
          +(Ux*tau12_b+Uy*tau22_b+Ux_b*tau12+Uy_b*tau22)/Re &
          +mu/Pra*dTT(:,2)/Re/(gamma-1.0_pr)
         
     CALL c_diff_fast (Fx, dFx, dDum, j_lev, ng, 1, 10,1,1,1)
     CALL c_diff_fast (Fy, dFy, dDum, j_lev, ng, 1, 10,1,1,1)
     user_Drhs(3*ng+1:4*ng) = dFx(1:ng,1) + dFy(1:ng,2)

     IF (Brinkman_active) CALL Brinkman_Drhs(user_Drhs, u, u_prev, meth)
     IF (Freund_active)   CALL Freund_Drhs(user_Drhs, u, u_prev, meth)
     
     IF( Nwlt_lev(j_lev,1) >  Nwlt_lev(j_lev,0) ) CALL user_evolutionary_BC_Drhs (user_Drhs, ne, ng,j_lev)
     
   END FUNCTION user_Drhs
   
  !
  ! Uses u_prev_timestep, which is global flat (1D ) array set in time_step_cn()
  !
  FUNCTION user_Drhs_diag (meth)
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: meth
    REAL (pr), DIMENSION (n) :: user_Drhs_diag

    INTEGER :: ie, shift
     REAL (pr), DIMENSION (ng,dim) :: du, du2
     REAL (pr), DIMENSION (ne,ng,dim) :: dux,duy,duz, d2u
    REAL (pr), DIMENSION (ne,ng,dim) :: du_dummy ! passed when only calculating 1st derivative.
!
! User defined variables
!
    REAL (pr), DIMENSION (ng,n_integrated,dim) :: F
    REAL (pr), DIMENSION (ng,ne) :: u_prev




    CALL c_diff_diag(du, du2,j_lev,ng,meth,meth,11)

! Create u_prev from u_prev_timestep
    DO ie = 1, ne
       shift=(ie-1)*ng
       u_prev(1:ng,ie) = u_prev_timestep(shift+1:shift+ng)
    END DO


    !--Form right hand side of Navier--Stokes equations
    IF (dim==1) THEN
    ELSE IF (dim==2) THEN
       F(:,1,1) = 0.0_pr
       F(:,2,1) = -(gamma-3.0_pr)*u_prev(:,2)/u_prev(:,1)
       F(:,3,1) = u_prev(:,2)/u_prev(:,1)
       F(:,4,1) = u_prev(:,2)/u_prev(:,1)*gamma

       F(:,1,2) = 0.0_pr
       F(:,2,2) = u_prev(:,3)/u_prev(:,1)
       F(:,3,2) = -(gamma-3.0_pr)*u_prev(:,3)/u_prev(:,1)
       F(:,4,2) = u_prev(:,3)/u_prev(:,1)*gamma
       CALL c_diff_fast(F(:,1:ne,1), dux, d2u, j_lev, ng, 1, 10, ne, 1, ne)
       CALL c_diff_fast(F(:,1:ne,2), duy, d2u, j_lev, ng, 1, 10, ne, 1, ne)

       DO ie = 1, dim+2
          shift=(ie-1)*ng
          user_Drhs_diag(shift+1:shift+ng) = - dux(ie,:,1) - duy(ie,:,2) &
               -F(:,ie,1)*du(:,1)-F(:,ie,2)*du(:,2)
       END DO

       user_Drhs_diag(ng+1:ng+ng)=user_Drhs_diag(ng+1:ng+ng)
       user_Drhs_diag(ng+1:ng+ng)=user_Drhs_diag(ng+1:ng+ng)
       user_Drhs_diag(2*ng+1:2*ng+ng)=user_Drhs_diag(2*ng+1:2*ng+ng)
       user_Drhs_diag(3*ng+1:3*ng+ng)=user_Drhs_diag(3*ng+1:3*ng+ng)


    ELSE IF (dim==3) THEN
    END IF

     IF (Brinkman_active) CALL Brinkman_Drhs_diag(user_Drhs_diag, meth)
     IF (Freund_active)   CALL Freund_Drhs_diag(user_Drhs_diag, meth)


  END FUNCTION user_Drhs_diag

  SUBROUTINE user_project (u, p, nlocal, meth)
    !--Makes u divergence free
    IMPLICIT NONE

    INTEGER, INTENT (IN) :: meth, nlocal
    REAL (pr), DIMENSION (nlocal),     INTENT(INOUT) :: p
    REAL (pr), DIMENSION (nlocal,n_integrated), INTENT(INOUT) :: u

    REAL (pr), DIMENSION (nlocal) :: dp
    REAL (pr), DIMENSION (nlocal) :: f


  END SUBROUTINE user_project

  FUNCTION user_chi (nlocal, t_local ) !in future release will use auto CAD output for geometry
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: nlocal
    REAL (pr), INTENT (IN) :: t_local
    REAL (pr), DIMENSION (nlocal) :: user_chi
!
!  Create field object here, 1 inside and 0 outside
!
    user_chi = 0.0_pr
  END FUNCTION user_chi


!!$  FUNCTION initial_obstacle (nlocal, t_local ) ! in future release
  FUNCTION initial_obstacle (nlocal)
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: nlocal
!!$    REAL (pr), INTENT (IN) :: t_local ! in future release
    REAL (pr), DIMENSION (nlocal) :: initial_obstacle
!
!  Create field object here, 1 inside and 0 outside
!

!    initial_obstacle = 0.0_pr

   initial_obstacle = 0.5_pr*(1.0_pr+TANH(-( SQRT((x(:,1)-cyl_center(1))**2  &
        + (x(:,2)-cyl_center(2))**2) - cyl_radius(1))/delta_obstacle )) 

!        +0.5_pr*(1.0_pr+TANH(-( SQRT((x(:,1)-cyl_center(1)-cyl_shift(1))**2  &
!        + (x(:,2)-cyl_center(2)-cyl_shift(2))**2) - cyl_radius(2))/delta_obstacle ))

  END FUNCTION initial_obstacle



  !
  ! Calculate any statitics
  !
  ! startup_flag - 0 when adapting to IC,then 1 inmain integration loop
  !
  SUBROUTINE user_stats ( u, j_mn_local, startup_flag)
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: startup_flag
    INTEGER , INTENT (IN) :: j_mn_local 
    REAL (pr), DIMENSION (nwlt,1:n_var), INTENT (IN) :: u
    INTEGER, DIMENSION(dim,1) :: ZERO
    INTEGER :: i1(1), ipatch
  CHARACTER (LEN=4) :: string
  !
  ! Generate statisitics with variables previously defined in this module
  !

  CALL FWH_stats ( u , nwlt, n_var, startup_flag)

  END SUBROUTINE user_stats

  SUBROUTINE user_cal_force (u, n, t_local, force, drag, lift, itype)
    !--Calculates drag and lift on obstacle using penalization formula
    IMPLICIT NONE

    INTEGER, INTENT (IN) :: itype, n
    REAL (pr), INTENT (IN) :: t_local
    REAL (pr), DIMENSION (dim), INTENT (INOUT) :: force
    REAL (pr), INTENT (OUT) :: drag, lift
    REAL (pr), DIMENSION (n,dim) :: u
    drag = 0.0_PR
    lift = 0.0_PR

    !
    ! There is no obstacle in flow
    !

  END SUBROUTINE user_cal_force

  !
  ! Read input from "case_name"_pde.inp file
  ! case_name is in string file_name read from command line
  ! in read_command_line_input()
  ! 
  SUBROUTINE user_read_input()
    IMPLICIT NONE


  call input_real ('Re',Re,'stop',' Re: Reynolds Number')
  call input_real ('gamma',gamma,'stop',' gamma: Specific Heat Ratio')
  call input_real ('delta',delta,'stop',' delta: width of initial condition')
  call input_real ('radius',radius,'stop',' radius: radius of initial condition')
  call input_real ('Mach',Mach,'stop',' Mach Number')
  call input_real ('Pra',Pra,'stop',' Prantl Number')
  call input_real ('nita',nita,'stop',' penalization parameter')
  call input_real_vector ('cyl_radius',cyl_radius(:),dim,'stop','  ')
  call input_real_vector ('cyl_center',cyl_center(:),dim,'stop','  ')
  call input_real ('delta_obstacle',delta_obstacle,'stop',' penalization parameter')

  Brinkman_active = .FALSE. ! default value
  call input_logical ('Brinkman_active',Brinkman_active,'default',' for brinlman penalization method')

  Freund_active = .FALSE. ! default value
  call input_logical ('Freund_active',Freund_active,'default',' for brinlman penalization method')

  END SUBROUTINE user_read_input


  !
  ! calculate any additional variables
  ! 
  ! arg 
  ! flag  - 0 calledwhile adapting to IC, 1 - called in main time integration loop
  !
  ! These additiona variables are calculated and left in real space.
  !
  SUBROUTINE user_additional_vars( t_local, flag )
    IMPLICIT NONE
    REAL (pr), INTENT (IN) ::  t_local
    INTEGER , INTENT(IN) :: flag ! 0- called during adaption to IC, 1 called during main integration loop
!
! Caluculate additional field variables
!

! Calculate pressure


   u(:,n_var_obstacle) = initial_obstacle (nwlt)

  u(:,n_var_pressure) = (gamma-1.0_pr)* (u(:,4) - 0.5_pr* (u(:,2)**2+u(:,3)**2)/u(:,1))

 
  END SUBROUTINE user_additional_vars


  !
  ! calculate any additional scalar variables
  ! 
  ! arg 
  ! flag  - 0 calledwhile adapting to IC, 1 - called in main time integration loop
  !
  SUBROUTINE user_scalar_vars( flag )
    IMPLICIT NONE
    INTEGER , INTENT(IN) :: flag ! 0- called during adaption to IC, 1 called during main integration loop


  END SUBROUTINE user_scalar_vars


  !
  !************ Calculating Scales ***************************
  !
  ! Note the order of the components in the scl array
  ! correspond to u_tn, v_tn, w_tn, u_tn-1, v_tn-1, w_tn-1, u_tn-2, v_tn-2, w_tn-2
  ! 
 SUBROUTINE user_scales(flag, use_default, u_loc, nlocal, ne_local, l_n_var_adapt , l_n_var_adapt_index, &
       scl, scl_fltwt) !add
    USE precision
    USE pde
    USE util_vars
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: flag ! 0 during initial adaptation, then 1 during time advancement
    LOGICAL , INTENT(INOUT) :: use_default
    INTEGER, INTENT (IN) :: nlocal, ne_local
    REAL (pr), DIMENSION (1:nlocal,1:ne_local), INTENT (IN) :: u_loc
    LOGICAL , INTENT (IN) :: l_n_var_adapt(ne_local)
    INTEGER , INTENT (IN) :: l_n_var_adapt_index(1:ne_local)
    REAL (pr), DIMENSION (1:ne_local), INTENT (INOUT) :: scl
    REAL (pr) ,  INTENT(IN) ::scl_fltwt !weight for temporal filter on scl
    REAL (pr) :: floor, tmp
    INTEGER :: ie, ie_index
    REAL (pr) ,save, ALLOCATABLE, DIMENSION (:) :: scl_old
    LOGICAL  ,save :: startup_init = .TRUE.

    !
    ! Ignore the output of this routine and use default scales routine
    !
    use_default = .TRUE. 
    !
    ! NOTE: For a parallel run, synchronize scl(:) across the processors in user_scales.
    !       Use the subroutine scales of default_util.f90 as an example,
    !       subroutine parallel_global_sum of parallel.f90 may be helpful.
    !       Already synchronized: sumdA_global

  END SUBROUTINE user_scales







  !
  !************ Calculating Scales ***************************
  !
  ! Note the order of the components in the scl array
  ! correspond to u_tn, v_tn, w_tn, u_tn-1, v_tn-1, w_tn-1, u_tn-2, v_tn-2, w_tn-2
  ! 
!  SUBROUTINE user_scales(use_default, u, nlocal, ne_local, l_n_var_adapt , l_n_var_adapt_index, &
!       scl, scl_fltwt) !add
!    IMPLICIT NONE
!    LOGICAL , INTENT(INOUT) :: use_default
!    INTEGER, INTENT (IN) :: nlocal, ne_local
!    REAL (pr), DIMENSION (1:nlocal,1:ne_local), INTENT (IN) :: u
!    LOGICAL , INTENT (IN) :: l_n_var_adapt(ne_local)
!    INTEGER , INTENT (IN) :: l_n_var_adapt_index(1:ne_local)
!    REAL (pr), DIMENSION (1:ne_local), INTENT (INOUT) :: scl
!    REAL (pr) ,  INTENT(IN) ::scl_fltwt !weight for temporal filter on scl
!    INTEGER :: ie, ie_index
!  REAL (pr) :: floor
    !
    ! Ignore the output of this routine and use default scales routine
    !
!    use_default = .FALSE. 

!	  floor = 1.e-6_pr
!	  scl   = 1.0_pr
!	  DO ie=1,ne_local
!		 IF(l_n_var_adapt(ie)) THEN
!			ie_index = l_n_var_adapt_index(ie)
!			IF( Scale_Meth == 1 ) THEN ! Use Linf scale
!			   scl(ie)= MAXVAL ( ABS( u(1:nlocal,ie_index) ) )
!			ELSE IF ( Scale_Meth == 2 ) THEN ! Use L2 scale
!			   scl(ie)= SQRT ( SUM( (u(1:nlocal,ie_index)**2) )/nlocal  )
!			ELSE IF ( Scale_Meth == 3 ) THEN ! Use L2 scale using dA
!			   scl(ie)= SQRT ( SUM( (u(1:nlocal,ie_index)**2)*dA )/ sumdA  )
!			ELSE
!			   PRINT *, 'Error in Scales(), Unknown Scale_Meth type: ', Scale_Meth
!			   PRINT *, 'Known Scale_Meth types: 1 = Linf scale, 2 = L2 scale '
!			   PRINT *, 'Exiting ...'
!			   stop
!			END IF
!			IF(scl(ie) .le. floor) scl(ie)=1.0_pr !Shouldn't this just set it to floor????
!		 END IF
!	  END DO
!          scl(3)=scl(2)

!  END SUBROUTINE user_scales

SUBROUTINE user_cal_cfl (use_default, u, cfl_out)
  USE precision
  USE sizes
  USE pde
  USE parallel
  IMPLICIT NONE
  LOGICAL , INTENT(INOUT) :: use_default
  REAL (pr),                                INTENT (INOUT) :: cfl_out
  REAL (pr), DIMENSION (nwlt,n_integrated), INTENT (IN)    :: u

  INTEGER                    :: i, j
  REAL (pr)                  :: floor
  REAL (pr), DIMENSION (dim) :: cfl
  REAL (pr), DIMENSION(dim,nwlt) :: h_arr



  use_default = .FALSE.



  floor = 1e-12_pr
  cfl_out = floor
  
  CALL get_all_local_h (h_arr)
  
  DO i = 1, nwlt
     cfl(1) = (ABS(u(i,2)/u(i,1))+1.0_pr) * dt/h_arr(1,i)
     cfl(2) = (ABS(u(i,3)/u(i,1))+1.0_pr) * dt/h_arr(2,i)
     cfl_out = MAX (cfl_out, MAXVAL(cfl))
  END DO
  CALL parallel_global_sum( REALMAXVAL=cfl_out )


  IF (Freund_active) Call Freund_cal_cfl (use_default, u, cfl_out)

END SUBROUTINE user_cal_cfl

  !******************************************************************************************
  !************************************* SGS MODEL ROUTINES *********************************
  !******************************************************************************************



!******************************************************************************************
!************************************* SGS MODEL ROUTINES *********************************
!******************************************************************************************


!
! Intailize sgs model
! This routine is called once in the first
! iteration of the main time integration loop.
! weights and model filters have been setup for first loop when this routine is called.
!
SUBROUTINE user_init_sgs_model( )
  IMPLICIT NONE


! LDM: Giuliano

! THE sgs forcign is stored for the time step in sgs_mdl_force(1:nlocal,1:n_integrated)
! where nlocal should be nwlt.


!          print *,'initializing LDM ...'       
!          CALL sgs_mdl_force( u(:,1:n_integrated), nwlt, j_lev, .TRUE.)


END SUBROUTINE user_init_sgs_model

!
! calculate sgs model forcing term
! user_sgs_force is called int he beginning of each times step in time_adv_cn().
! THE sgs forcign is stored for the time step in sgs_mdl_force(1:nlocal,1:n_integrated)
! where nlocal should be nwlt.
! 
! Accesses u from field module, 
!          j_lev from wlt_vars module,
!
SUBROUTINE  user_sgs_force (u_loc, nlocal)
  IMPLICIT NONE

  INTEGER,                         INTENT (IN) :: nlocal
  REAL (pr), DIMENSION (nlocal,n_integrated), INTENT (INOUT) :: u_loc

END SUBROUTINE  user_sgs_force

  FUNCTION user_sound_speed (u, neq, nwlt)
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: nwlt, neq
    REAL (pr), DIMENSION (nwlt,neq), INTENT(IN) :: u
    REAL (pr), DIMENSION (nwlt) :: user_sound_speed

    !user_sound_speed(:) = SQRT( gamma*(gamma-1.0_pr)* &
    !       ( u(:,4)-0.5_pr*SUM(u(:,n_var_mom(1:dim))**2,DIM=2)/u(:,1)) ) ! pressure

  END FUNCTION user_sound_speed

  SUBROUTINE  user_pre_process
    IMPLICIT NONE
     
  END SUBROUTINE user_pre_process

  SUBROUTINE  user_post_process
    IMPLICIT NONE
    
  END SUBROUTINE user_post_process

END MODULE user_case





