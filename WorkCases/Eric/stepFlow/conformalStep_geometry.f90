MODULE user_case
  ! CASE 2/3 D channel periodic in x-direction and if dim=3 in z-derection
  USE precision
  USE wlt_vars
  USE wavelet_filters_mod
  USE elliptic_mod
  USE elliptic_vars
  USE wlt_trns_mod
  USE wlt_trns_vars
  USE wlt_trns_util_mod
  USE io_3d_vars
  USE util_mod
  USE util_vars
  USE share_consts
  USE pde
  USE sizes
  USE share_kry
  USE vector_util_mod
  USE field
  USE input_file_reader
  USE debug_vars
  !
  ! case specific variables
  !
  INTEGER n_var_pressure  ! start of pressure in u array

  REAL (pr) :: nu     ! kinematic viscosity (1/Re)heat conduction
    
  INTEGER, ALLOCATABLE, DIMENSION(:)       :: nmap   ! indices for curvilinear mapping

CONTAINS

  !
  ! In user_setup_pde() we setup how many variables are integrated, which are interpolated
  ! to the next times step and if any exeact solution exists to compare to during the run.
  ! We also set the variable names that are used when the result files are written.
  !
  ! The following variables must be setup in this routine:
  !
  !
  ! n_integrated     ! first n_integrated eqns will be acted on for time integration
  ! n_var_additional ! 
  ! n_var
  !
  !
  !
  !
  SUBROUTINE  user_setup_pde ( VERB ) 
    USE curvilinear
    IMPLICIT NONE
    LOGICAL, OPTIONAL :: VERB         ! print debug info
    INTEGER :: i

    IF (verb_level.GT.0) THEN
       PRINT * ,''
       PRINT *, '**********************Setting up PDE*****************'
       PRINT * ,'CASE: Backwards Facing Step - compressible           ' 
       PRINT *, '*****************************************************'
    END IF

    n_integrated = 1

    n_var_additional = 0 
    IF( ANY(transform_dir(1:dim)>0) ) n_var_additional = n_var_additional + dim


    n_var = n_integrated + n_var_additional !--Total number of variables

    n_var_exact = 0 !--One exact solution for u,v,w

    n_var_pressure  = n_integrated + 1 !pressure


    IF( ANY(transform_dir(1:dim)>0) ) THEN
      IF ( ALLOCATED( nmap ) ) DEALLOCATE(nmap); ALLOCATE(nmap(dim))
      DO i = 1,dim
        nmap(i) = n_var - dim + i
      END DO
    END IF  

    !--Allocate logical arrays 
    !  Must be done after setting number of different types of variables
    !  and before setting logical variable mapping arrays
    !
    CALL alloc_variable_mappings


    !
    ! Fill in variable names for default variables for integration meth 2
    ! These may be changed in the call to the user specified routine
    !  user_setup_pde()
    !
    ! In integrated variables (3D)
    IF( dim <= 3 ) THEN
       WRITE (u_variable_names(1), u_variable_names_fmt) 'T  '

       IF( ANY(transform_dir(1:dim)>0) ) THEN
          IF(dim >= 1) WRITE (u_variable_names(nmap(1)), u_variable_names_fmt) 'X '
          IF(dim >= 2) WRITE (u_variable_names(nmap(2)), u_variable_names_fmt) 'Y '
          IF(dim >= 3) WRITE (u_variable_names(nmap(3)), u_variable_names_fmt) 'Z '
       END IF
    END IF

    !
    ! setup logical variable mapping arrays
    ! This defined which variables are used for adaptation, Which need to be interpololated to the new grid at 
    ! each time step, which are saved, and for which we have an exect solutiuon we want to check at each time step
    !


    ! setup which components we will base grid adaptation on.
    ! Adapt on the 3 velocity components
    !
    n_var_adapt = .FALSE. !intialize
    n_var_adapt(1:n_integrated,0)     = .TRUE. !--Initially adapt on integrated variables at first time level

    n_var_adapt(1:n_integrated,1)     = .TRUE. !--After first time step adapt on 

    IF( ANY(transform_dir(1:dim)>0) ) THEN
       DO i = 1,dim
          IF( prd(i) .EQ. 0 ) n_var_adapt(nmap(1:dim),0:1)     = .TRUE. !--Only adapt on non-periodic coordinates
       END DO
    END IF

    !--integrated variables at first time level

    !Variables that need to be interpoleted to new adapted grid in initial grid adaptation
    n_var_interpolate        = .FALSE. !intialize
    n_var_interpolate(1:n_integrated,0) = .TRUE.  
    IF( ANY(transform_dir(1:dim)>0) ) THEN
       n_var_interpolate(nmap(1:dim),0:1) = .TRUE.  
    END IF
    
    !Variables that need to be interpoleted to new adapted grid at each time step
    n_var_interpolate(1:n_integrated,1) = .TRUE. 

    !
    ! setup which components we have an exact solution for
    !
    n_var_exact_soln = .FALSE. !intialize
    !n_var_exact_soln(1:n_integrated,0:1) = .TRUE.

    !
    ! setup which variables we will save the solution
    !
    n_var_save = .FALSE. !intialize 
    n_var_save(1:n_var) = .TRUE. ! save all for restarting code

    !
    ! Set the maximum number of components for which we have an exact solution
    !
    n_var_exact = 0 

    !
    ! Setup a scaleCoeff array of we want to tweak the scaling of a variable
    ! ( if scaleCoeff < 1.0 then more points will be added to grid 
    !
    ALLOCATE ( scaleCoeff(1:n_var), Umn(1:n_var) )
    scaleCoeff = 1.0_pr


    Umn = 0.0_pr !set up here if mean quantities are not zero and used in scales or equation

    IF (verb_level.GT.0) THEN
       PRINT *, 'n_integrated = ',n_integrated 
       PRINT *, 'n_var = ',n_var 
       PRINT *, 'n_var_exact = ',n_var_exact 
       PRINT *, '*******************Variable Names*******************'
       DO i = 1,n_var
          WRITE (*, u_variable_names_fmt) u_variable_names(i)
       END DO
       PRINT *, '****************************************************'
    END IF

  END SUBROUTINE  user_setup_pde
  !
  ! Set the exact solution for comparison to the simulated solution
  !
  ! u          - array to fill in the exact solution
  ! nlocal       - number of active wavelets   
  ! ne_local        - total number of equations
  ! t          - time of current time step 
  ! l_n_var_exact_soln_index - index into the elements of u for which we need to 
  !                            find the exact solution
  SUBROUTINE  user_exact_soln (u, nlocal,  t_local, l_n_var_exact_soln)  
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: nlocal
    REAL (pr), INTENT (IN) ::  t_local
    REAL (pr), DIMENSION (nlocal,n_var_exact), INTENT (INOUT) :: u
    LOGICAL , INTENT (IN) :: l_n_var_exact_soln(n_var)
    INTEGER :: i
    DOUBLE PRECISION :: DERF
    EXTERNAL :: DERF

   

  END SUBROUTINE  user_exact_soln

  SUBROUTINE user_initial_conditions (u, nlocal, ne_local, t_local, scl, scl_fltwt, iter)
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: nlocal, ne_local
    INTEGER  , INTENT (INOUT) :: iter ! iteration of call while adapting initial grid
    REAL (pr), DIMENSION (nlocal,ne_local), INTENT (INOUT) :: u
    REAL (pr)  :: scl(1:n_var),scl_fltwt
    REAL (pr), INTENT (IN) :: t_local
    INTEGER :: i
    DOUBLE PRECISION :: DERF
    EXTERNAL :: DERF

    REAL (pr), DIMENSION(nlocal, dim) :: x_phys

    u(:,1) = 0.0_pr 

  END SUBROUTINE user_initial_conditions

!--********************************  
  ! Arguments
  ! u         - field on adaptive grid
  ! nlocal      - number of active points
  ! ne_local       - number of equations
  ! t         - current time
  !
  ! Global variables used
  ! ifn       - number of equations
  ! x()
  ! xO
  ! y0
  !--******************************** 
  SUBROUTINE user_algebraic_BC (Lu, u, nlocal, ne_local, jlev, meth)
    !--Defines boundary condition type
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: jlev, meth, ne_local, nlocal
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (INOUT) :: Lu
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (IN)    :: u

    INTEGER :: i, ie, ii, shift
    REAL (pr), DIMENSION (ne_local,nlocal,dim) :: du, d2u !uncomment if Neuman BC are used, need to call 
    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc

    !BC for both 2D and 3D cases

    !if Neuman BC are used, need to call 
    CALL c_diff_fast (u, du, d2u, jlev, nlocal, meth, 10, ne_local, 1, ne_local)

    DO ie = 1, ne_local
       shift = nlocal*(ie-1)
       !--Go through all Boundary points that are specified
       i_p_face(0) = 1
       DO i=1,dim
          i_p_face(i) = i_p_face(i-1)*3
       END DO
       DO face_type = 0, 3**dim - 1
          face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
          IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
             CALL get_all_indices_by_face (face_type, jlev, nloc, iloc)
             IF(nloc > 0 ) THEN 
                IF( ANY(ABS(face(:)) .EQ. 1)  ) THEN               !All faces 
                   Lu(shift+iloc(1:nloc)) = u(shift+iloc(1:nloc))  !Dirichlet conditions
                END IF
             END IF
          END IF
       END DO
    END DO

  END SUBROUTINE user_algebraic_BC

  SUBROUTINE user_algebraic_BC_diag (Lu_diag, nlocal, ne_local, jlev, meth)
    !--Defines boundary condition type
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: jlev, meth, ne_local, nlocal
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (INOUT) :: Lu_diag

    INTEGER :: i, ie, ii, shift
    REAL (pr), DIMENSION (nlocal,dim) :: du, d2u  !uncomment if Neuman BC are used
    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc
    
    !BC for both 2D and 3D cases

    !if Neuman BC are used, need to call 
    CALL c_diff_diag ( du, d2u, jlev, nlocal, meth, meth, 10)

    DO ie = 1, ne_local
       shift = nlocal*(ie-1)
       !--Go through all Boundary points that are specified
       i_p_face(0) = 1
       DO i=1,dim
          i_p_face(i) = i_p_face(i-1)*3
       END DO
       DO face_type = 0, 3**dim - 1
          face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
          IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
             CALL get_all_indices_by_face (face_type, jlev, nloc, iloc)
             IF(nloc > 0 ) THEN 
                IF( ANY(ABS(face(:)) .EQ. 1 )  ) THEN                   ! All faces 
                   Lu_diag(shift+iloc(1:nloc)) = 1.0_pr                ! Dirichlet conditions
                END IF
             END IF
          END IF
       END DO
    END DO
 
  END SUBROUTINE user_algebraic_BC_diag

  SUBROUTINE user_algebraic_BC_rhs (rhs, ne_local, nlocal, jlev)
    !--Sets rhs for boundary conditions
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: ne_local, nlocal, jlev
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (INOUT) :: rhs

    INTEGER :: i, ie, ii, shift
    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc

    !BC for both 2D and 3D cases

    DO ie = 1, ne_local
       shift = nlocal*(ie-1)
       !--Go through all Boundary points that are specified
       i_p_face(0) = 1
       DO i=1,dim
          i_p_face(i) = i_p_face(i-1)*3
       END DO
       DO face_type = 0, 3**dim - 1
          face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
          IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
             CALL get_all_indices_by_face (face_type, jlev, nloc, iloc)
             IF(nloc > 0 ) THEN 
                IF( face(2) .EQ. 1 )  THEN                   ! YMin face 
                   rhs(shift+iloc(1:nloc)) = 1.0_pr!T_1                 
                ELSE IF( ANY(ABS(face(:)) .EQ. 1 ) ) THEN                   ! All faces 
                   rhs(shift+iloc(1:nloc)) = 0.0_pr!T_1                 
                END IF
             END IF
          END IF
       END DO
    END DO

  END SUBROUTINE user_algebraic_BC_rhs

  SUBROUTINE user_project (u, p, nlocal, meth)
    !--Makes u divergence free
    IMPLICIT NONE

    INTEGER, INTENT (IN) :: meth, nlocal
    REAL (pr), DIMENSION (nlocal),     INTENT(INOUT) :: p
    REAL (pr), DIMENSION (nlocal,n_integrated), INTENT(INOUT) :: u

    REAL (pr), DIMENSION (nlocal) :: dp
    REAL (pr), DIMENSION (nlocal) :: f

    INTEGER :: i
	INTEGER, PARAMETER :: ne = 1
    INTEGER, DIMENSION(ne) :: clip 


  END SUBROUTINE user_project

  FUNCTION user_rhs (u_integrated,p)	 
    USE curvilinear
    IMPLICIT NONE
    REAL (pr), DIMENSION (ng,ne), INTENT(IN) :: u_integrated !1D flat version of integrated variables without BC 
    REAL (pr), DIMENSION (ng), INTENT(IN) :: p
    REAL (pr), DIMENSION (n) :: user_rhs


    INTEGER :: idim, ie, shift
    INTEGER, PARAMETER :: meth=1
    REAL (pr), DIMENSION (dim,ng,dim) :: du, d2u
    REAL (pr), DIMENSION (dim,ng,dim) :: du_comp, d2u_comp
    REAL (pr), DIMENSION (ng,dim)     :: temp


    !--Form right hand side of heat equations
    CALL c_diff_fast(u_integrated(:,1), du(1,:,:), d2u(1,:,:), j_lev, ng, 1, 01, 1, 1, 1)
    ie = 1
    shift = ng*(ie-1)
    user_rhs(shift+1:shift+ng) = SUM(d2u(1,:,:),DIM=2)

  !---------------- Algebraic BC are set up in user_algebraic_BC and they autmatically overwrite evolution BC

  END FUNCTION user_rhs

  ! find Jacobian of Right Hand Side of the problem
  FUNCTION user_Drhs (u, u_prev_timestep_loc, meth)
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: meth
	!u_prev_timestep passed local to cast it into 2dim array to work with vector derivatives
    REAL (pr), DIMENSION (ng,ne) :: u, u_prev_timestep_loc
    REAL (pr), DIMENSION (n) :: user_Drhs

 
    INTEGER :: idim, ie, shift
    REAL (pr), DIMENSION (dim,ng,dim) :: du  ! 1st derivatives for u (in du(1:ne,:,:)) and u_prev_timestep (in du(ne+1:2*ne,:,:))
    REAL (pr), DIMENSION (dim  ,ng,dim) :: d2u ! 2nd derivatives for u 
    REAL (pr), DIMENSION (ng,dim)     :: temp
    !Find batter way to do this!! du_dummy with no storage..

    CALL c_diff_fast(u(:,1), du(1,:,:), d2u(1,:,:), j_lev, ng, 1, 10, 1, 1, 1 )
    !CALL c_diff_fast(u(:,1), du(1,:,:), d2u(1,:,:), j_lev, ng, 1, 10, 1, 1, 1 )
    DO idim = 1,dim
       temp(:,idim) = du(1,:,idim)
    END DO
    CALL c_diff_fast(temp(:,1:dim), du(1:dim,:,:), d2u(1:dim,:,:), j_lev, ng, 1, 10, dim, 1, dim )
    !CALL c_diff_fast(temp(:,1:dim), du(1:dim,:,:), d2u(1:dim,:,:), j_lev, ng, 1, 10, dim, 1, dim )


    !--Form right hand side of heat equations
    ie = 1
    shift = ng*(ie-1)
    user_Drhs(shift+1:shift+ng) = du(1,:,1)
    DO idim = 2,dim
       user_Drhs(shift+1:shift+ng) = user_Drhs(shift+1:shift+ng) + du(idim,:,idim)
    END DO

  END FUNCTION user_Drhs


  !
  ! Uses u_prev_timestep, which is global flat (1D ) array set in time_step_cn()
  !
  FUNCTION user_Drhs_diag (meth)
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: meth
    REAL (pr), DIMENSION (n) :: user_Drhs_diag

    INTEGER :: ie, shift
    REAL (pr), DIMENSION (ng,dim) :: du, d2u
    REAL (pr), DIMENSION (ne,ng,dim) :: du_prev_timestep
    REAL (pr), DIMENSION (ne  ,ng,dim) :: du_dummy ! passed when only calculating 1st derivative.

    PRINT *, 'NOT SET UP FOR DIAG TERMS.  EXITING...'; STOP

    !
    ! does not rely on u so we can call it once here
    !
    CALL c_diff_diag(du, d2u, j_lev, ng, meth, meth, 01)

    DO ie = 1, 1
       shift = ng*(ie-1)
       !CALL c_diff_fast(u_integrated(1:ng,ie), du, d2u, j_lev, nwlt, 1, 11, 1, 1, 1)
       user_Drhs_diag(shift+1:shift+ng) = SUM(d2u,2)
    END DO

  END FUNCTION user_Drhs_diag

  FUNCTION user_chi (nlocal, t_local )
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: nlocal
    REAL (pr), INTENT (IN) :: t_local
    REAL (pr), DIMENSION (nlocal) :: user_chi

    user_chi = 0.0_pr
  END FUNCTION user_chi

  FUNCTION user_mapping (nlocal, t_local )
    USE curvilinear
    USE parallel
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: nlocal
    REAL (pr), INTENT (IN) :: t_local
    REAL (pr), DIMENSION (nlocal,dim) :: user_mapping
    INTEGER :: i


    ! forward step params
    COMPLEX (pr), DIMENSION(nlocal) :: z, z1, t, w
    COMPLEX (pr) :: q

    REAL (pr), PARAMETER :: PI = 3.14159265358979323846_pr

    user_mapping(:,1:dim) = x(:,1:dim)

    q = (0.8_pr, 0.0_pr)

    IF( dim < 2 ) THEN
       IF(par_rank .EQ. 0) PRINT *, 'User coordinate system requires at least 2D'
       STOP
    END IF

    DO i = 1, nlocal
       ! forward step
       z(i) = CMPLX(x(i, 1), x(i, 2))
       z1(i) = EXP(z(i))
       t(i) = SQRT((z1(i) - 1.0_pr)/(z1(i) - q**2))
       !w(i) = ATANH(t(i)) - ATANH(q*t(i))/q
       w(i) = 0.5_pr * ( LOG(1.0_pr + t(i)) - LOG(1.0_pr - t(i)) ) - 0.5_pr * ( LOG(1.0_pr+q*t(i) ) - LOG( 1.0_pr-q*t(i) ))/q
       user_mapping(i, 1) = REAL(w(i))
       user_mapping(i, 2) = AIMAG(w(i))
    END DO


  END FUNCTION user_mapping

  !
  ! Calculate any statitics
  !
  ! startup_flag - 0 when adapting to IC,then 1 inmain integration loop
  !
  SUBROUTINE user_stats ( u ,j_mn, startup_flag)
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: startup_flag
    INTEGER , INTENT (IN) :: j_mn 
    REAL (pr), DIMENSION (nwlt,1:n_var), INTENT (IN) :: u
  END SUBROUTINE user_stats


  SUBROUTINE user_cal_force (u, n, t_local, force, drag, lift)
    !--Calculates drag and lift on obstacle using penalization formula
    IMPLICIT NONE

    INTEGER, INTENT (IN) :: n
    REAL (pr), INTENT (IN) :: t_local
    REAL (pr), DIMENSION (dim), INTENT (INOUT) :: force
    REAL (pr), INTENT (OUT) :: drag, lift
    REAL (pr), DIMENSION (n,dim) :: u
    drag = 0.0_PR
    lift = 0.0_PR

    !
    ! There is no obstacle in flow
    !

  END SUBROUTINE user_cal_force

  !
  ! Read input from "case_name"_pde.inp file
  ! case_name is in string file_name read from command line
  ! in read_command_line_input()
  ! 
  SUBROUTINE user_read_input()
    IMPLICIT NONE



  END SUBROUTINE user_read_input



  !
  ! calculate any additional variables
  ! 
  ! arg 
  ! flag  - 0 calledwhile adapting to IC, 1 - called in main time integration loop
  !
  ! These additiona variables are calculated and left in real space.
  !
  SUBROUTINE user_additional_vars( t_local, flag )
    USE curvilinear
    IMPLICIT NONE
    REAL (pr), INTENT (IN) ::  t_local
    INTEGER , INTENT(IN) :: flag ! 0- called during adaption to IC, 1 called during main integration loop
    INTEGER :: i
    DOUBLE PRECISION :: DERF
    EXTERNAL :: DERF

    IF( ANY(transform_dir(1:dim)>0) ) THEN
       u(:,nmap(1:dim)) = user_mapping(nwlt, t )
    END IF

  END SUBROUTINE user_additional_vars


  !
  ! calculate any additional scalar variables
  ! 
  ! arg 
  ! flag  - 0 calledwhile adapting to IC, 1 - called in main time integration loop
  !
  SUBROUTINE user_scalar_vars( flag )
    IMPLICIT NONE
    INTEGER , INTENT(IN) :: flag ! 0- called during adaption to IC, 1 called during main integration loop



  END SUBROUTINE user_scalar_vars

  !
  !************ Calculating Scales ***************************
  !
  ! Note the order of the components in the scl array
  ! correspond to u_tn, v_tn, w_tn, u_tn-1, v_tn-1, w_tn-1, u_tn-2, v_tn-2, w_tn-2
  ! 
  SUBROUTINE user_scales(flag, use_default, u, nlocal, ne_local, l_n_var_adapt , l_n_var_adapt_index, &
       scl, scl_fltwt) !add
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: flag ! 0 during initial adaptation, then 1 during time advancement
    LOGICAL , INTENT(INOUT) :: use_default
    INTEGER, INTENT (IN) :: nlocal, ne_local
    REAL (pr), DIMENSION (1:nlocal,1:ne_local), INTENT (IN) :: u
    LOGICAL , INTENT (IN) :: l_n_var_adapt(ne_local)
    INTEGER , INTENT (IN) :: l_n_var_adapt_index(1:ne_local)
    REAL (pr), DIMENSION (1:ne_local), INTENT (INOUT) :: scl
    REAL (pr) ,  INTENT(IN) ::scl_fltwt !weight for temporal filter on scl

    !
    ! Ignore the output of this routine and use default scales routine
    !
    use_default = .TRUE. 

    !
    ! NOTE: For a parallel run, synchronize scl(:) across the processors in user_scales.
    !       Use the subroutine scales of default_util.f90 as an example,
    !       subroutine parallel_global_sum of parallel.f90 may be helpful.
    !       Already synchronized: sumdA_global
  END SUBROUTINE user_scales

SUBROUTINE user_cal_cfl (use_default, u, cfl_out)
  USE precision
  USE sizes
  USE pde
  IMPLICIT NONE
  LOGICAL , INTENT(INOUT) :: use_default
  REAL (pr),                                INTENT (INOUT) :: cfl_out
  REAL (pr), DIMENSION (nwlt,n_integrated), INTENT (IN)    :: u

  INTEGER                    :: i
  REAL (pr)                  :: floor
  REAL (pr), DIMENSION (dim) :: cfl
  REAL (pr), DIMENSION(dim,nwlt) :: h_arr

  use_default = .FALSE.

  floor = 1e-12_pr
  cfl_out = floor
  
  CALL get_all_local_h (h_arr)
  
!!$  DO i = 1, nwlt
!!$     cfl(1:dim) = ABS (u(i,1:dim)+Umn(1:dim)) * dt/h_arr(1:dim,i)
!!$     cfl_out = MAX (cfl_out, MAXVAL(cfl))
!!$  END DO

  cfl_out = 1.0_pr ! no CFL condition

END SUBROUTINE user_cal_cfl

!******************************************************************************************
!************************************* SGS MODEL ROUTINES *********************************
!******************************************************************************************

!
! Intialize sgs model
! This routine is called once in the first
! iteration of the main time integration loop.
! weights and model filters have been setup for first loop when this routine is called.
!
SUBROUTINE user_init_sgs_model( )
  IMPLICIT NONE


! LDM: Giuliano

! THE sgs forcign is stored for the time step in sgs_mdl_force(1:nlocal,1:n_integrated)
! where nlocal should be nwlt.


!          print *,'initializing LDM ...'       
!          CALL sgs_mdl_force( u(:,1:n_integrated), nwlt, j_lev, .TRUE.)


END SUBROUTINE user_init_sgs_model

!
! calculate sgs model forcing term
! user_sgs_force is called int he beginning of each times step in time_adv_cn().
! THE sgs forcign is stored for the time step in sgs_mdl_force(1:nlocal,1:n_integrated)
! where nlocal should be nwlt.
! 
! Accesses u from field module, 
!          j_lev from wlt_vars module,
!
SUBROUTINE  user_sgs_force (u_loc, nlocal)
  IMPLICIT NONE

  INTEGER,                         INTENT (IN) :: nlocal
  REAL (pr), DIMENSION (nlocal,n_integrated), INTENT (INOUT) :: u_loc

END SUBROUTINE  user_sgs_force

  FUNCTION user_sound_speed (u, neq, nwlt)
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: nwlt, neq
    REAL (pr), DIMENSION (nwlt,neq), INTENT(IN) :: u
    REAL (pr), DIMENSION (nwlt) :: user_sound_speed

    user_sound_speed(:) = 0.0_pr 
    !user_sound_speed(:) = SQRT( gamma*(gamma-1.0_pr)* &
    !       ( u(:,4)-0.5_pr*SUM(u(:,n_var_mom(1:dim))**2,DIM=2)/u(:,1)) ) ! pressure

  END FUNCTION user_sound_speed

  SUBROUTINE  user_pre_process
    IMPLICIT NONE

  END SUBROUTINE user_pre_process

  SUBROUTINE  user_post_process
    IMPLICIT NONE
  END SUBROUTINE user_post_process

END MODULE user_case
