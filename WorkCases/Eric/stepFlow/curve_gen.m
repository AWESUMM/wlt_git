function curve_gen
clc
close all
% vertical lines
global Nx NNx Ny NNy omega control_cls type
Nx = 21; NNx = 1001;
Ny = 21; NNy = 1001;
control_cls = ['r', 'g', 'b', 'magenta'];
type = 1;
loop = 'type';
T = linspace(0, 1, 1);
omega = 100*pi;

% modes = 1:7;
% modes = [1,3,4,5,7];
modes = 9;

if strcmp(loop, 'time')
    figure;
    s1 = subplot(1,2,1);
    s2 = subplot(1,2,2);
    for t = T
        rotate(s1, s2, t);
        hold off
        pause(0.05)
    end
elseif strcmp(loop, 'type')
    for i = modes
        figure;
        s1 = subplot(1,2,1);
        s2 = subplot(1,2,2);
        get_domain(i);
        type = i;
        rotate(s1, s2, 0);
    end
end

function title = get_title(type)
switch type
    case 1
        title = 'Simple wedge';
    case 2
        title = 'Forward step';
    case 3
        title = 'Forward step with horizontal upper wall';
    case 4
        title = 'Bullet';
    case 5
        title = 'Diverging nozzle';
    case 6
        title = 'Slope';
    case 7
        title = 'Slope with horizontal upper wall';
    case 8
        title = 'Horizontal plate';
    case 9
        title = 'Bump';
end

function get_domain(type)
global xmin xmax ymin ymax control_pts
switch type
    case 1
        xmin = -1; xmax = 1; 
        ymin = 0; ymax = 1;
        control_pts = [xmin, ymin; 0, 0; xmax, ymax];
    case 2
        xmin = -2; xmax = 3;
        ymin = eps; ymax = 5;
        control_pts = [xmin, ymin; 1, 0; xmax, ymax];
    case 3
        xmin = -pi; xmax = pi;
        ymin = 0; ymax = pi;
        control_pts = [xmin, ymin; -1.02164, 0; xmax, ymax];
    case 4
        xmin = -2; xmax = 2;
        ymin = eps; ymax = 1;
        control_pts = [xmin, ymin; 1, 0; xmax, ymax];
    case 5
        xmin = -pi; xmax = pi;
        ymin = -pi; ymax = pi;
        control_pts = [xmin, ymin; 1, 0; xmax, ymax];
    case 6
        xmin = -2; xmax = 2;
        ymin = eps; ymax = 2;
        control_pts = [xmin, ymin; 1, eps; xmax, ymax];
    case 7
        xmin = -pi; xmax = pi;
        ymin = eps; ymax = pi;
        control_pts = [xmin, ymin; -1.07, eps; xmax, ymax];
    case 8
        tmp = 1.4;
        xmin = -tmp; xmax = tmp;
        ymin = 0; ymax = tmp;
%         control_pts = [xmin, ymin; xmin, ymax; xmax, ymax; xmax, 0];
        control_pts = [xmax, ymin; xmax, ymax];
    case 9
        xmin = -2; xmax = 2;
        ymin = eps; ymax = 2;
%         control_pts = [xmin, ymin; xmin, ymax; xmax, ymax; xmax, 0];
        control_pts = [xmax, ymin; xmax, ymax];
end

function rotate(s1, s2, t)
global xmin xmax ymin ymax Nx NNx Ny NNy alpha omega control_pts control_cls type
alpha = 10 + 45*sin(omega*t);
ttl = get_title(type);
for x = linspace(xmin, xmax, Nx)
    if x == xmin
        th = 2;
        col = hex2rgb('#e41a1c');
    else
        th = 1;
        if x == xmax
            col = hex2rgb('#377eb8');
            th = 2;
        else
            col = hex2rgb('#4daf4a');
        end
    end
    % plot vertical line
    Y = linspace(ymin, ymax, NNy);
    X = ones(size(Y))*x;
    subplot(s1);
    plot(X,Y, 'Color', col, 'LineWidth', th)
    hold on
    set(gca, 'DataAspectRatio', [1,1,1])
    title('Rectangular mesh');
%         xlim([-1, 1])
%         ylim([0, 1.6])
    % plot curve <-> vertical line
    [U,V] = transformation(X,Y);
%         [U,V] = transformation2(X,Y);
    subplot(s2);
    plot(U,V, 'Color', col, 'LineWidth', th)
    hold on
    set(gca, 'DataAspectRatio', [1,1,1])
    title(ttl);
%         xlim([-1.5, 1])
%         ylim([0, 1.6])
end

% horizontal lines
for y = linspace(ymin, ymax, Ny)
    if y == ymin
        th = 2;
        col = hex2rgb('#984ea3');
    else
        th = 1;
        if y == ymax
            col = hex2rgb('#ff7f00');
            th = 2;
        else
            col = hex2rgb('#fb9a99');
        end
    end
    X = linspace(xmin, xmax, NNx);
    Y = ones(size(X))*y;
    subplot(s1);
    plot(X, Y, 'Color', col, 'LineWidth', th)
    hold on
    set(gca, 'DataAspectRatio', [1,1,1])
%         xlim([-1, 1])
%         ylim([0, 1.6])
    % plot curve <-> vertical line
    [U,V] = transformation(X,Y);
%         [U,V] = transformation2(X,Y);
    subplot(s2);
    plot(U,V, 'Color', col, 'LineWidth', th)
    hold on
    set(gca, 'DataAspectRatio', [1,1,1])
%         xlim([-1.5, 1])
%         ylim([0, 1.6])
end

[m, ~] = size(control_pts);
% 
for i = 1:m
    pts = control_pts(i, :);
%     z = pts(1) + pts(2)*1i;
    [u, v] = transformation(pts(1), pts(2));
%     u = real(w);
%     v = imag(w);
    subplot(s1);
    plot(pts(1), pts(2), 'Marker' ,'o', 'MarkerFaceColor', control_cls(i));
    subplot(s2);
    plot(u, v, 'Marker' ,'o', 'MarkerFaceColor', control_cls(i));
%     hold off
end

function [u, v] = transformation(x, y)
global alpha type xmin xmax ymin ymax
z = x + y*1i;
switch type
    case 1
        f = @(z) exp(alpha*pi*1i/180)*z.^(1-alpha/180);
        w = f(z);
    case 2
        f = @(z) 1i*(asin(z) + (1-z.^2).^0.5 - pi/2);
        w = f(z);
    case 3
        q = 0.6;
        z1 = @(z) exp(z);
        t = @(z1) ((z1-1)./(z1-q^2)).^0.5;
        f = @(t) atanh(t) - atanh(q*t)/q;
        w = f(t(z1(z)));
    case 4
        t = @(z) (1-1./z.^2).^0.25;
        f = @(t) 4*t./(1-t.^4) - log((t+1)./(t-1)) - 2*atan(t);
        w = f(t(z));
    case 5
        t = @(z) (exp(z) + 1).^0.25;
        f = @(t) 4*t + log((t-1)./(t+1)) - 2*atan(t);
        w = f(t(z));
    case 6
        t = @(z) (1+1./z).^0.25;
        f = @(t, z) 2*t.*z + atanh(t) - atan(1./t);
        w = f(t(z), z);
    case 7
        q = 0.7;
        z1 = @(z) exp(z);
        t = @(z1) ((z1-1)./(z1-q^3)).^(1/3);
        f = @(t) -sqrt(3)*pi+6*sqrt(3)*atan((1+2*t)/sqrt(3))-6*log(1-t)+3*log(1+t+t.^2) - (-sqrt(3)*pi+6*sqrt(3)*atan((1+2*q*t)/sqrt(3))-6*log(1-q*t)+3*log(1+q*t+q^2*t.^2))/q;
        w = f(t(z1(z)));
        w = reflect('x', 0, w);
        z0 = xmax;
        w0 = f(t(z1(z0)));
        w = translate(w0, w);
        z0 = xmin;
        w0 = f(t(z1(z0)));
        w = translate(imag(w0)*1i, w);
        kx = 1/2; ky = 1/3;
        w = scale(kx, ky, w);
    case 8
        m = 0.5;
        [Fi,Ei,Zi] = elliptic12i(asin(z),m);
%         f = @(z) elliptic12i(z);
        w = Fi;
%         w = reflect('x', 0, w);
    case 9
        aa = 1;
        bb = 1;
        f = @(z) aa*z + 1i*bb*sqrt(1 - z.^2);
        w = f(z);
end
u = real(w);
v = imag(w);

function w = reflect(dir, ln, z)
x = real(z);
y = imag(z);
switch dir
    case 'x'
        x = x + 2*(ln - x);
    case 'y'
        y = y + 2*(ln - y);
end
w = x + y*1i;

function w = translate(z0, z)
w = z + z0;

function w = scale(kx, ky, z)
w = real(z)*kx + imag(z)*ky*1i;

function rgbc = hex2rgb(hexc)
rgbc = [hex2dec(hexc(2:3)), hex2dec(hexc(4:5)), hex2dec(hexc(6:7))]/255;