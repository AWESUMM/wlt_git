!$
!!$!****************************************************************************
!!$!* Module for adding Brinkman Penalization terms onto the full NS equations *
!!$!*  -Only valid for single species 10/7/2011                                *
!!$!*  -R is scaled to 1.0
!!$!****************************************************************************
!!$
!!$

MODULE user_case_vars
  USE precision
 ! REAL (pr) :: Re, Pra, Sc, Fr, Ma, At
  INTEGER :: Nspec, Nspecm, methpd
  INTEGER :: nden, neng, nprs!, ndpx, nboy, nbRe, ndom, nton, BCver,ICver,nmsk,ncvtU
  INTEGER, ALLOCATABLE :: nvel(:)!, nspc(:), nvon(:), nson(:) 
  LOGICAL :: NS ! if NS=.TRUE. - Navier-Stoke, else Euler
  REAL (pr), ALLOCATABLE, DIMENSION (:) ::  mu_in, kk_in, cp_in, MW_in, gamm  

 ! INTEGER :: nden, neng, nprs, ndpx, nboy, nbRe, ndom, nton, BCver,ICver,nmsk,ncvtU
 ! INTEGER, ALLOCATABLE :: nvel(:), nspc(:), nvon(:), nson(:), nUn(:)
 ! REAL (pr) :: rhoL, pL, EL, rhoR, pRR, ER
 ! REAL (pr), DIMENSION(:), ALLOCATABLE :: uL, uR
  REAL (pr) :: xw, wa ! Wedge
  REAL (pr) :: xsp, ysp, rsp ! Sphere
  ! Spheres
  REAL (pr), DIMENSION(:), ALLOCATABLE :: xsps, ysps, rsps
  INTEGER :: Nsp
  REAL (pr) :: Re,S1!, Pra, Sc, Fr, Ma, At
  ! Spheres
  REAL (pr) :: delta_conv, delta_diff, shift_conv, shift_diff
  REAL (pr) :: xwall
  REAL (pr) :: deltadel_loc
  LOGICAL :: smooth_dist
  LOGICAL :: doCURV
  REAL (pr) :: doNORM
  
  REAL (pr) :: eta_b, eta_c
  REAL (pr) :: visc_factor_d,visc_factor_n  !parameters for viscous zone around penalized obstacle
  INTEGER :: delta_pts, IB_pts


END MODULE user_case_vars


MODULE user_case

  USE precision
  USE wlt_vars
  USE wavelet_filters_mod
  USE elliptic_mod
  USE elliptic_vars
  USE wlt_trns_mod
  USE wlt_trns_vars
  USE wlt_trns_util_mod
  USE io_3d_vars
  USE util_mod
  USE util_vars
  USE share_consts
  USE pde
  USE sizes
  USE share_kry
  USE vector_util_mod
  USE field
  USE input_file_reader
  USE parallel
  USE hyperbolic_solver
  USE user_case_vars

  !
  ! case specific variables
  !

  REAL (pr) :: MAreport, CFLreport, CFLconv, CFLacou, maxsos, mydt_orig
  REAL (pr) :: peakchange, maxMa
  REAL (pr) :: Tconst, Pint, gammR, Y_Xhalf
  REAL (pr), ALLOCATABLE, DIMENSION (:) :: pureX, pureY, pureR, pureg, puregamma, YR
  REAL (pr) :: e0top, e0bot, e0right,e0left, rho0top, drho0top, rho0bot, drho0bot, rhoetopBC, rhoebotBC, rhoetop, rhoebot ,velleft  !used anymore?
  REAL (pr) ::  e0,rho0
  REAL (pr), ALLOCATABLE, DIMENSION (:) :: Y0top, Y0bot, rhoYtopBC, rhoYbotBC, rhoYtop, rhoYbot, Lxyz
  REAL (pr), DIMENSION (2) :: rhoYg, drho0, dP0, c0, P0, gammBC
  !REAL (pr), ALLOCATABLE, DIMENSION (:) ::  mu_in, kk_in, cp_in, MW_in, gamm  
  REAL (pr), ALLOCATABLE, DIMENSION (:) :: bD_in, gr_in
  INTEGER :: n_var_pressure  ! start of pressure in u array
  REAL (pr) ::  Pra, Sc, Fr, Ma, At
  INTEGER :: ndpx, nboy, nbRe, ndom, nton, BCver,ICver,nmsk,nutil
  LOGICAL :: BC_perturb, IC_blasius, perturb_forcing 
  INTEGER, ALLOCATABLE :: nspc(:), nvon(:), nson(:) 
  !LOGICAL :: NS ! if NS=.TRUE. - Navier-Stoke, else Euler
  LOGICAL :: brinkman_penal, vp  !penalization type flags
  LOGICAL, PARAMETER :: NSdiag=.TRUE. ! if NSdiag=.TRUE. - use viscous terms in user_rhs_diag
  LOGICAL :: GRAV, adaptAt
  LOGICAL ::  saveMagVort, adaptComVort, saveComVort, adaptMagVel, saveMagVel, saveNormS, adaptGradY, saveGradY, saveUtil  !4extra - this whole line
  INTEGER :: adaptvel 
  INTEGER :: adaptMagVort, adaptNormS
  REAL (pr) :: MagVortScale, NormSScale
  REAL (pr) :: VelScale 
  INTEGER :: nmvt, ncvt(3), nmvl, nnms !4extra - this whole line
  INTEGER, ALLOCATABLE :: ngdy(:) !4extra - this whole line
  REAL (pr) :: Lczn, tfacczn, itfacczn, itczn, tczn, cczn, czn_min, czn_max
  LOGICAL :: boundY, modczn, splitFBpress, stepsplit
  REAL (pr) ::  tempscl, specscl
  INTEGER ::  locnvar
  LOGICAL ::  adaptden, adapteng, adaptspc, adaptprs 
  LOGICAL :: adaptvelonly, adaptspconly, adapttmp
  LOGICAL :: convcfl, strainratecfl, vorticityCFL

  REAL (pr) :: porosity
  REAL (pr) :: grav_coeff
  LOGICAL :: dynamic_gravity
  REAL (pr), DIMENSION(20) :: Re_tau_hist, Re_bulk_hist, grav_coeff_hist    !vectors with spatially averaged values for the previous 1000 steps
  REAL (pr) :: Re_tau, Re_bulk, Re_tau_target                                                !spatio-temporal averaged values

  REAL (pr) :: mesh_cluster_param 
  LOGICAL :: use_OS_IC 

  INTEGER :: hyper_it = 0  ! for temporary hyperbolic solver application - often used to smooth local instabilities
  INTEGER :: hyper_it_limit = 0  ! for temporary hyperbolic solver application - often used to smooth local instabilities
  LOGICAL :: filter_field

  ! Smagorinsky model
  LOGICAL :: user_smagorinsky = .FALSE.                  ! Flag for activating a user_case defined smagorinsky model
  REAL (pr), DIMENSION(:), ALLOCATABLE :: user_mu_turb   ! Eddy viscosity
  REAL (pr) :: user_pr_turb, user_sc_turb, user_van_driest, user_cs

CONTAINS

   SUBROUTINE  user_setup_pde ( VERB ) 
    USE curvilinear
    USE variable_mapping
    IMPLICIT NONE
    LOGICAL, OPTIONAL :: VERB         ! print debug info
    INTEGER :: i, l
    CHARACTER(LEN=8) :: specnum  !4extra
    CHARACTER (LEN=u_variable_name_len) :: specname 
    LOGICAL :: tmp_flag

    !IF(compressible_SCALES) CALL SGS_compressible_setup (0)
 
    IF (par_rank.EQ.0) THEN
       PRINT * ,''
       PRINT *, '**********************Setting up PDE*****************'
       PRINT * ,'CASE: Compressible Channel Flow '
       PRINT *, '*****************************************************'
    END IF

    ! Compressible flow variables
    CALL register_var( 'Den_rho  ',  integrated=.TRUE.,  adapt=(/adaptden,adaptden/),   interpolate=(/.TRUE.,.TRUE./),   exact=(/.FALSE.,.FALSE./),   saved=.TRUE., req_restart=.TRUE., FRONT_LOAD=.TRUE. )
    tmp_flag = (adaptvel .NE. 0) .AND. .NOT. adaptvelonly
    CALL register_var( 'XMom  ',  integrated=.TRUE.,  adapt=(/tmp_flag, tmp_flag/),   interpolate=(/.TRUE.,.TRUE./),   exact=(/.FALSE.,.FALSE./),   saved=.TRUE., req_restart=.TRUE., FRONT_LOAD=.TRUE. )
    CALL register_var( 'YMom  ',  integrated=.TRUE.,  adapt=(/tmp_flag, tmp_flag/),   interpolate=(/.TRUE.,.TRUE./),   exact=(/.FALSE.,.FALSE./),   saved=.TRUE., req_restart=.TRUE., FRONT_LOAD=.TRUE. )
    CALL register_var( 'ZMom  ',  integrated=.TRUE.,  adapt=(/tmp_flag, tmp_flag/),   interpolate=(/.TRUE.,.TRUE./),   exact=(/.FALSE.,.FALSE./),   saved=.TRUE., req_restart=.TRUE., FRONT_LOAD=.TRUE. )
    CALL register_var( 'E_Total  ',  integrated=.TRUE.,  adapt=(/adapteng,adapteng/),    interpolate=(/.TRUE.,.TRUE./),   exact=(/.FALSE.,.FALSE./),   saved=.TRUE., req_restart=.TRUE., FRONT_LOAD=.TRUE. )
    IF (Nspec>1) THEN
       tmp_flag = (adaptspc) .AND. .NOT. adaptspconly
       DO l=1,Nspecm      
          WRITE (specnum,'(I8)') l          
          WRITE (specname, u_variable_names_fmt) TRIM('Spec_Scalar_'//ADJUSTL(TRIM(specnum)))//'  '
          CALL register_var( specname,  integrated=.TRUE.,  adapt=(/tmp_flag,tmp_flag/),   interpolate=(/.TRUE.,.TRUE./),   exact=(/.FALSE.,.FALSE./),   saved=.TRUE., req_restart=.TRUE., FRONT_LOAD=.TRUE. )
       END DO
    END IF
    CALL register_var( 'Pressure  ', integrated=.FALSE.,  adapt=(/adaptprs,adaptprs/),   interpolate=(/.TRUE.,.TRUE./),   exact=(/.FALSE.,.FALSE./),   saved=.TRUE., req_restart=.TRUE., FRONT_LOAD=.TRUE. )

    ! Additional Variables
    IF ( adapttmp ) THEN
       CALL register_var( 'Temperature ',       integrated=.FALSE.,  adapt=(/.TRUE.,.TRUE./),   interpolate=(/.TRUE.,.TRUE./),   exact=(/.FALSE.,.FALSE./),   saved=.TRUE., req_restart=.FALSE. )
    END IF

    IF (adaptvelonly) THEN
       ! ERIC: req restart
       CALL register_var( 'XVelocity  ', integrated=.FALSE.,  adapt=(/.TRUE.,.TRUE./),   interpolate=(/.TRUE.,.TRUE./),   exact=(/.FALSE.,.FALSE./),   saved=.TRUE., req_restart=.FALSE. )
       IF( dim .GE. 2) CALL register_var( 'YVelocity  ', integrated=.FALSE.,  adapt=(/.TRUE.,.TRUE./),   interpolate=(/.TRUE.,.TRUE./),   exact=(/.FALSE.,.FALSE./),   saved=.TRUE., req_restart=.FALSE. )
       IF( dim .GE. 3) CALL register_var( 'ZVelocity  ', integrated=.FALSE.,  adapt=(/.TRUE.,.TRUE./),   interpolate=(/.TRUE.,.TRUE./),   exact=(/.FALSE.,.FALSE./),   saved=.TRUE., req_restart=.FALSE. )
    END IF

    IF (adaptspconly .AND. Nspec > 1) THEN
       DO l=1,Nspecm
          WRITE (specnum,'(I8)') l          
          WRITE (specname, u_variable_names_fmt) TRIM('Mass_Frac_'//ADJUSTL(TRIM(specnum)))//'  '
          CALL register_var( specname,       integrated=.FALSE.,  adapt=(/.TRUE.,.TRUE./),   interpolate=(/.TRUE.,.TRUE./),   exact=(/.FALSE.,.FALSE./),   saved=.TRUE., req_restart=.FALSE. )
       END DO
    END IF

    !4extra - need all these definitions
    IF( adaptMagVort .NE. 0 .or. saveMagVort ) THEN
       CALL register_var( 'MagVort  ',  integrated=.FALSE.,  adapt=(/adaptMagVort .NE. 0,adaptMagVort .NE. 0/),   interpolate=(/.TRUE.,.TRUE./),   exact=(/.FALSE.,.FALSE./),   saved=saveMagVort, req_restart=.FALSE. )
    END IF
    IF( (adaptComVort .or. saveComVort) .AND. dim==2 ) THEN
       CALL register_var( 'ComVort  ',  integrated=.FALSE.,  adapt=(/adaptComVort,adaptComVort/),   interpolate=(/.TRUE.,.TRUE./),   exact=(/.FALSE.,.FALSE./),   saved=saveComVort, req_restart=.FALSE. )
    END IF
    IF( (adaptComVort .or. saveComVort) .AND. dim==3 ) THEN
       CALL register_var( 'ComVort_1  ',  integrated=.FALSE.,  adapt=(/adaptComVort,adaptComVort/),   interpolate=(/.TRUE.,.TRUE./),   exact=(/.FALSE.,.FALSE./),   saved=saveComVort, req_restart=.FALSE. )
       CALL register_var( 'ComVort_2  ',  integrated=.FALSE.,  adapt=(/adaptComVort,adaptComVort/),   interpolate=(/.TRUE.,.TRUE./),   exact=(/.FALSE.,.FALSE./),   saved=saveComVort, req_restart=.FALSE. )
       CALL register_var( 'ComVort_3  ',  integrated=.FALSE.,  adapt=(/adaptComVort,adaptComVort/),   interpolate=(/.TRUE.,.TRUE./),   exact=(/.FALSE.,.FALSE./),   saved=saveComVort, req_restart=.FALSE. )
    END IF
    IF( adaptMagVel  .OR. saveMagVel  ) THEN
       CALL register_var( 'MagVel  ',       integrated=.FALSE.,  adapt=(/adaptMagVel,adaptMagVel/),   interpolate=(/.TRUE.,.TRUE./),   exact=(/.FALSE.,.FALSE./),   saved=saveMagVel, req_restart=.FALSE. )
    END IF
    IF( adaptNormS .NE. 0 .OR. saveNormS   ) THEN
       CALL register_var( 'NormS  ',       integrated=.FALSE.,  adapt=(/adaptNormS .NE. 0,adaptNormS .NE. 0/),   interpolate=(/.TRUE.,.TRUE./),   exact=(/.FALSE.,.FALSE./),   saved=saveNormS, req_restart=.FALSE. )
    END IF

    IF( adaptGradY   .OR. saveGradY   ) THEN
       IF (Nspec>1) THEN
          DO l=1,Nspecm
             WRITE (specnum,'(I8)') l          
             WRITE (specname, u_variable_names_fmt) TRIM('GradY'//ADJUSTL(TRIM(specnum)))//'  '
             CALL register_var( specname,       integrated=.FALSE.,  adapt=(/adaptGradY,adaptGradY/),   interpolate=(/.TRUE.,.TRUE./),   exact=(/.FALSE.,.FALSE./),   saved=saveGradY, req_restart=.FALSE. )
          END DO
       END IF
    END IF

    IF (imask_obstacle) THEN
       CALL register_var( 'Mask  ',       integrated=.FALSE.,  adapt=(/.TRUE.,.TRUE./),   interpolate=(/.TRUE.,.TRUE./),   exact=(/.FALSE.,.FALSE./),   saved=.TRUE., req_restart=.FALSE. )
    END IF

    IF ( saveUtil) THEN 
       CALL register_var( 'Utility  ',       integrated=.FALSE.,  adapt=(/.FALSE.,.FALSE./),   interpolate=(/.TRUE.,.TRUE./),   exact=(/.FALSE.,.FALSE./),   saved=.TRUE., req_restart=.FALSE. )
    END IF

    CALL setup_mapping()

    IF (ALLOCATED(nvel)) DEALLOCATE(nvel); ALLOCATE(nvel(dim))
    IF (ALLOCATED(nspc)) DEALLOCATE(nspc)
    IF (Nspec>1) ALLOCATE(nspc(Nspecm))


    nden = get_index('Den_rho') 
    nvel(1) = get_index('XMom') 
    IF(dim .GE. 2) nvel(2) = get_index('YMom') 
    IF(dim .GE. 3) nvel(3) = get_index('ZMom') 
    neng = get_index('E_Total') 
    IF (Nspec>1) THEN
       DO i=1,Nspecm
          WRITE (specnum,'(I8)') l          
          WRITE (specname, u_variable_names_fmt) TRIM('Spec_Scalar_'//ADJUSTL(TRIM(specnum)))//'  '
          nspc(i) = get_index(specname) 
       END DO
    END IF
 
    nprs = get_index('Pressure') 
    n_var_pressure  = nprs !pressure

    ! Additional Variables
    nton = get_index('Temperature')
    IF (ALLOCATED(nvon)) DEALLOCATE(nvon)
    ALLOCATE (nvon(dim))
    nvon(1) = get_index('XVelocity') 
    IF(dim .GE. 2) nvon(2) = get_index('YVelocity') 
    IF(dim .GE. 3) nvon(3) = get_index('ZVelocity') 
    IF (ALLOCATED(nson)) DEALLOCATE(nson)
    ALLOCATE (nson(Nspecm))
    DO l=1,Nspecm
          WRITE (specnum,'(I8)') l          
          WRITE (specname, u_variable_names_fmt) TRIM('Mass_Frac_'//ADJUSTL(TRIM(specnum)))//'  '
       nson(l) = get_index(specname) 
    END DO
    nmvt = get_index('MagVort')
    IF( dim .EQ. 2 ) THEN
       ncvt(:)= get_index('ComVort')  !ERIC 2 dim
    ELSE IF ( dim .EQ. 3 ) THEN
       ncvt(1)= get_index('ComVort_1') 
       ncvt(2)= get_index('ComVort_2') 
       ncvt(3)= get_index('ComVort_3') 
    END IF
    nmvl = get_index('MagVel')
    nnms = get_index('NormS')
    IF (ALLOCATED(ngdy)) DEALLOCATE(ngdy)
    ALLOCATE(ngdy(Nspecm))
    DO l=1,Nspecm
       WRITE (specnum,'(I8)') l          
       WRITE (specname, u_variable_names_fmt) TRIM('GradY'//ADJUSTL(TRIM(specnum)))//'  '
       ngdy(l) = get_index( specname )
    END DO
    nmsk = get_index('Mask')
    nutil = get_index('Utility')

    IF(hypermodel .NE. 0 ) THEN
      n_var_mom_hyper(1:dim) = nvel(1:dim)
      n_var_den_hyper = nden
    END IF

    !
    ! Setup a scaleCoeff array if we want to tweak the scaling of a variable
    ! ( if scaleCoeff < 1.0 then more points will be added to grid 
    !
    !ALLOCATE ( scaleCoeff(1:n_var) )
    scaleCoeff = 1.0_pr
    IF(adaptvel .NE. 0 )  scaleCoeff(nvel(1:dim)) = VelScale
    IF(adaptMagVort .NE. 0 )  scaleCoeff(nmvt) = MagVortScale
    IF(adaptNormS .NE. 0 )  scaleCoeff(nnms) = NormSScale

    PRINT *, 'here -7'    

    
    IF(hypermodel .NE. 0) THEN
       IF(ALLOCATED(n_var_hyper)) DEALLOCATE(n_var_hyper)
       ALLOCATE(n_var_hyper(1:n_var))
       n_var_hyper = .FALSE.
       !n_var_hyper(nden) = .TRUE.
       n_var_hyper(nvel(1:dim)) = .TRUE.
       n_var_hyper(neng) = .TRUE.
       IF ( adaptMagVort .NE. 0) n_var_hyper(nmvt) = .TRUE.
       !n_var_hyper = .TRUE.


       IF(ALLOCATED(n_var_hyper_active)) DEALLOCATE(n_var_hyper_active)
       ALLOCATE(n_var_hyper_active(1:n_integrated))
       n_var_hyper_active = .FALSE.
      ! n_var_hyper_active(nden) = .TRUE.
!       n_var_hyper_active(neng) = .TRUE.
       n_var_hyper_active = .TRUE.

    END IF

    IF (par_rank.EQ.0) THEN
       PRINT *, 'n_integrated = ',n_integrated 

       PRINT *, 'n_var = ',n_var 
       PRINT *, 'n_var_exact = ',n_var_exact 
       PRINT *, '*******************Variable Names*******************'
       DO i = 1,n_var
          WRITE (*, u_variable_names_fmt) u_variable_names(i)
       END DO
       PRINT *, '****************************************************'
    END IF

    !Use variable thresholding
    !IF ( do_Adp_Eps_Spatial .OR. do_Adp_Eps_Spatial_Evol ) &
    !CALL user_setup_pde__VT

   !IF(compressible_SCALES)  CALL SGS_compressible_setup (1)
!***********Variable orders, ensure 1:dim+1+Nspec are density, velocity(dim), energy, species(Nspecm)**************

  END SUBROUTINE  user_setup_pde


  !
  ! Set the exact solution for comparison to the simulated solution
  !
  ! u          - array to fill in the exact solution
  ! nlocal       - number of active wavelets   
  ! ne_local        - total number of equations
  ! t          - time of current time step 
  ! l_n_var_exact_soln_index - index into the elements of u for which we need to 
  !                            find the exact solution
  SUBROUTINE  user_exact_soln (u, nlocal,  t_local, l_n_var_exact_soln)  
    USE variable_mapping
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: nlocal
    REAL (pr), INTENT (IN) ::  t_local
    REAL (pr), DIMENSION (nlocal,n_var_exact), INTENT (INOUT) :: u
    LOGICAL , INTENT (IN) :: l_n_var_exact_soln(n_var)
    INTEGER :: i

  END SUBROUTINE  user_exact_soln

  SUBROUTINE user_initial_conditions (u, nlocal, ne_local, t_local, scl, scl_fltwt,iter)
    USE penalization
    USE parallel 
    USE variable_mapping
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: nlocal, ne_local
    REAL (pr), DIMENSION (nlocal,ne_local), INTENT (INOUT) :: u
    REAL (pr)  :: scl(1:n_var),scl_fltwt
    REAL (pr), INTENT (IN) :: t_local
    INTEGER, INTENT (INOUT) :: iter
    INTEGER :: i, j, idim, l, ie, ii, k
    INTEGER :: iseedsize, jx, jy, jz, sprt
    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc

    REAL (pr), DIMENSION(nlocal,1:dim) :: x_phys 
    REAL (pr), DIMENSION(2,1:dim) :: x_phys_limits

    REAL (pr) :: u_max, u_min, v_max, v_min, w_max, w_min
    REAL (pr), DIMENSION(nlocal,1) :: forcing
    !REAL (pr), DIMENSION(nlocal) :: p
    REAL (pr), DIMENSION(nlocal) :: x_rndm
    REAL (pr), DIMENSION(nlocal) :: rand_array
    REAL (pr), DIMENSION(nlocal,2) :: loc_support, Dloc_support

    REAL (pr) :: r_max, PI,v_mean
    REAL (pr) :: fourier_coeff1, fourier_coeff2,fourier_coeff3,L_x, L_y, L_z, x_mid, y_mid, z_mid
    REAL (pr) :: a_rndm, b_rndm, c_rndm
    REAL (pr) :: y_pos, width
    REAL (pr), DIMENSION(nlocal) :: u_r
    INTEGER, ALLOCATABLE, DIMENSION(:) :: seed

    REAL (pr) :: tmp1, tmp2 
    REAL (pr) :: d_thick, delta_trans 
    INTEGER :: nwlt_total, nwlt_max

    INTEGER :: n_cycles
    REAL (pr) :: phase_loc
    REAL (pr), DIMENSION(18) :: fourier_coeffs 
    REAL (pr), DIMENSION(nlocal) :: signal, d_signal
    !REAL (pr), DIMENSION(2049) :: eigenfcn_real, eigenfcn_imag 
    !REAL (pr), DIMENSION(nlocal) :: u_pert
    !INTEGER, DIMENSION(nlocal) :: iy_loc 
    !REAL (pr), DIMENSION(1,nlocal,dim) :: du, d2u 

    REAL (pr) :: volume_total 
       volume_total = SUM(dA)
       CALL parallel_global_sum(REAL    = volume_total               )   
       IF(par_rank .EQ. 0) PRINT *, 'INITIAL CONDITIONS VOLUME: ', volume_total


    x_phys(:,:) = user_mapping(nlocal, t )
    DO idim = 1,dim
       x_phys_limits(1,idim) = MINVAL( x_phys(:,idim) )
       x_phys_limits(2,idim) = MAXVAL( x_phys(:,idim) )
       CALL parallel_global_sum( REALMINVAL=x_phys_limits(1,idim) )
       CALL parallel_global_sum( REALMAXVAL=x_phys_limits(2,idim) )
       IF ( par_rank .EQ. 0 )  PRINT *, idim,'X_phys min = ', x_phys_limits(1,idim), 'max = ',x_phys_limits(2,idim)      
    END DO

  IF (IC_restart_mode > 0 ) THEN !in the case of restart
     !do nothing
  ELSE IF (IC_restart_mode .EQ. 0) THEN
     !Set Density
     u(:,nden)              = 1.0_pr

     !Set Background Momentum
     DO i=1,dim
        u(:,nvel(i)) = 0.0_pr
     END DO


          !parameters
        u(:,nvel(1)) = 1.5_pr*(1.0_pr-x_phys(:,2)**2) !background flow

     IF ( use_os_ic ) THEN
        !******** Orr - Sommerfeld Solution from fourier modes *************

        !DO k = 1,2049
        !   READ(20,'(E12.5)') eigenfcn_real(k) 
        !END DO
        !DO k = 1,2049
        !   READ(20,'(E12.5)') eigenfcn_imag(k) 
        !END DO


        ! eigenfunction indice, valid on all j_level <= j_mx (where ny = 2049 at j_mx), ERIC: needs to be reconciled as a more general expression, rather than point-by-point lookup
        !iy_loc = INT((x(:,2)-xyzlimits(1,2))/(xyzlimits(2,2)-xyzlimits(1,2))*2048.0_pr) + 1  


        !u_pert(:) = eigenfcn_real(iy_loc)*COS(-x(:,1)) - eigenfcn_imag(iy_loc)*SIN(-x(:,1))
        !CALL c_diff_fast(u_pert(:), du(:,:,:), d2u(:,:,:), j_lev, nlocal, 1, 10, 1, 1, 1)
        !u(:,nvel(1)) = u(:,nvel(1)) + 0.085_pr*du(1,:,2)    ! %8.5 turbulence level
        !u(:,nvel(2)) = 0.085_pr*eigenfcn_real(iy_loc)*SIN(-x(:,1)) + 0.085_pr*eigenfcn_imag(iy_loc)*COS(-x(:,1))
        n_cycles = 0
        IF ( par_rank .EQ. 0 ) THEN 
           OPEN (20, FILE='SPECTRA_OS_eigenfunction_R3000.dat', STATUS='OLD', FORM='FORMATTED', READONLY, POSITION='REWIND')
           READ(20,'(I20)') n_cycles
        END IF
        CALL parallel_global_sum( INTEGER = n_cycles )
        !PRINT *, 'cycles', n_cycles
        d_signal(:) = 0.0_pr
        ! Real Part
        DO i = 0,n_cycles - 1
                fourier_coeffs(:) = 0.0_pr
                IF ( par_rank .EQ. 0 ) READ(20,'(18(E20.12))') fourier_coeffs(:)
                CALL parallel_vector_sum( REAL = fourier_coeffs(1:18), LENGTH = 18 )
                phase_loc = fourier_coeffs(18);
                signal(:) = 0.0_pr
                signal = signal + fourier_coeffs(1);
                !PRINT *, 'a_0', fourier_coeffs(1);
                DO j = 1,8
                        signal   = signal   + fourier_coeffs( 2 * j     )                 * COS( j * phase_loc * x_phys(:,2));
                        d_signal = d_signal - fourier_coeffs( 2 * j     ) * j * phase_loc * SIN( j * phase_loc * x_phys(:,2)) * COS(- x_phys(:,1));
                        signal   = signal   + fourier_coeffs( 2 * j + 1 )                 * SIN( j * phase_loc * x_phys(:,2));
                        d_signal = d_signal + fourier_coeffs( 2 * j + 1 ) * j * phase_loc * COS( j * phase_loc * x_phys(:,2)) * COS(- x_phys(:,1));
                END DO
                u(:,nvel(2)) = u(:,nvel(2)) + 0.02_pr*signal(:)*SIN(- x_phys(:,1)) 
        END DO
        ! Imaginary Part
        DO i = 0,n_cycles - 1
                fourier_coeffs(:) = 0.0_pr
                IF ( par_rank .EQ. 0 ) READ(20,'(18(E20.12))') fourier_coeffs(:)
                CALL parallel_vector_sum( REAL = fourier_coeffs(1:18), LENGTH = 18 )
                phase_loc = fourier_coeffs(18);
                signal(:) = 0.0_pr
                signal = signal + fourier_coeffs(1);
                !PRINT *, 'a_0', fourier_coeffs(1);
                DO j = 1,8
                        signal   = signal   + fourier_coeffs( 2 * j     )                 * COS( j * phase_loc * x_phys(:,2));
                        d_signal = d_signal - fourier_coeffs( 2 * j     ) * j * phase_loc * SIN( j * phase_loc * x_phys(:,2)) * ( -1.0_pr )*SIN(- x_phys(:,1));
                        signal   = signal   + fourier_coeffs( 2 * j + 1 )                 * SIN( j * phase_loc * x_phys(:,2));
                        d_signal = d_signal + fourier_coeffs( 2 * j + 1 ) * j * phase_loc * COS( j * phase_loc * x_phys(:,2)) * ( -1.0_pr )*SIN(- x_phys(:,1));
                END DO
                u(:,nvel(2)) = u(:,nvel(2)) + 0.02_pr*signal(:)*COS(- x_phys(:,1))
        END DO
        u(:,nvel(1)) = u(:,nvel(1)) + 0.02_pr * d_signal(:)    ! %8.5 turbulence level
        IF( par_rank .EQ. 0 )CLOSE(20)
        !******** Orr - Sommerfeld Solution *************

     END IF

     !CALL add_smooth_noise( u(:,nvel(1)), nlocal, 100, 0.02_pr, 23423, DEFINE_Z = 1.0_pr )
     !CALL add_smooth_noise( u(:,nvel(1)), nlocal, 100, 0.02_pr, 234463, DEFINE_Z = -1.0_pr )
     !CALL add_smooth_noise( u(:,nvel(2)), nlocal, 100, 0.02_pr, 233, DEFINE_Z = 1.0_pr )
     !CALL add_smooth_noise( u(:,nvel(2)), nlocal, 100, 0.02_pr, 235173, DEFINE_Z = -1.0_pr )
     CALL add_smooth_noise( u(:,nvel(2)), nlocal, 50, 0.010_pr, 233, DEFINE_Y = 0.20_pr )
     CALL add_smooth_noise( u(:,nvel(2)), nlocal, 50, 0.010_pr, 3389, DEFINE_Y = -0.20_pr )
     IF( dim .EQ. 3 ) THEN
        CALL add_smooth_noise( u(:,nvel(3)), nlocal, 50, 0.010_pr, 3987983, DEFINE_Y = 0.20_pr )
        CALL add_smooth_noise( u(:,nvel(3)), nlocal, 50, 0.010_pr, 9234, DEFINE_Y = -0.20_pr )
     !  CALL add_smooth_noise( u(:,nvel(3)), nlocal, 100, 0.02_pr, 423, DEFINE_Z = 1.0_pr )
     !  CALL add_smooth_noise( u(:,nvel(3)), nlocal, 100, 0.02_pr, 42673, DEFINE_Z = -1.0_pr )
     END IF

     !Set Energy for constant temperature
     u(:,neng)              = 0.5_pr*SUM(u(:,nvel(1:dim))**2.0_pr, DIM=2)/u(:,nden) + u(:,nden)*(cp_in(Nspec)-1.0_pr/MW_in(Nspec)/Ma**2.0_pr)*Tconst
     !u(:,neng)              =
     !u(:,nden)*(cp_in(Nspec)-1.0_pr/MW_in(Nspec)/Ma**2.0_pr)*((1.5_pr-1.0_pr)/(xyzlimits(1,2)-xyzlimits(2,2))*(x(:,2)-xyzlimits(1,2))+1.5_pr    )  ! temperature differential
     !u(:,neng)              = 0.5_pr*u(:,nvel(1))**2.0_pr/u(:,nden) + u(:,nden)*(cp_in(Nspec)-1.0_pr/MW_in(Nspec)/Ma**2.0_pr)*Tconst  !couette flow
      !add acoustic perturbation
    ! u(:,nden) = 1.0_pr +1.0e-3_pr*EXP( -LOG(2.0_pr) * ((x(:,1))**2.0_pr + SUM(x(:,2:dim)**2.0_pr,DIM=2)) / 0.004_pr )! gaussian centered on x,y = 0
    ! u(:,neng)              = u(:,neng) + 1.0e-3_pr*EXP( -LOG(2.0_pr) * ((x(:,1))**2.0_pr + SUM(x(:,2:dim)**2.0_pr,DIM=2)) / 0.004_pr )/(MW_in(Nspec)-1.0_pr)
     !Set species
     IF (Nspec>1) u(:,nspc(1)) = 0.0_pr

     DO i = 1,dim + 2
        tmp1 = MINVAL(u(:,i))
        tmp2 = MAXVAL(u(:,i))
        CALL parallel_global_sum( REALMINVAL=tmp1 )
        CALL parallel_global_sum( REALMAXVAL=tmp2 )
        IF(par_rank .EQ. 0) PRINT *, 'Initialize var', i, tmp1,tmp2
     END DO
             
     !Modify ICs for steady mean flow
     !u(:,nvel(1))       = 1.0_pr
     !u(:,neng)              = u(:,nvel(1))**2.0_pr/(2.0_pr*u(:,nden)) + u(:,nden)*(cp_in(Nspec)-1.0_pr/MW_in(Nspec)/Ma**2.0_pr)*Tconst

     !introduce boundary conditions so that the code will adapt on IC's if
     !necessary
     DO ie = 1, ne_local
        !--Go through all Boundary points that are specified
        i_p_face(0) = 1
        DO i=1,dim
           i_p_face(i) = i_p_face(i-1)*3
        END DO
        DO face_type = 0, 3**dim - 1
           face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
           IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
              CALL get_all_indices_by_face (face_type, j_lev, nloc, iloc)
              IF(nloc > 0 ) THEN 
                 IF( ABS(face(2)) == 1  ) THEN  !Left and Right - Full
                   IF(ie >= nvel(1) .AND. ie <= nvel(dim)) THEN  !Momentums
                      u(iloc(1:nloc),ie)              =  0.0_pr  !no-slip
                   ELSE IF(ie == neng) THEN
                      u(iloc(1:nloc),ie)              =  u(iloc(1:nloc),nden)*(cp_in(Nspec)-1.0_pr/MW_in(Nspec)/Ma**2.0_pr)*Tconst !temperatures
                   END IF

                 END IF
              END IF
           END IF
        END DO
     END DO
     


     nwlt_max = nwlt
     CALL parallel_global_sum(INTEGERMAXVAL=nwlt_max)
     IF(par_rank .EQ. 0 ) PRINT *, 'nwlt_max:', nwlt_max


  END IF

  !variable thresholding
  !IF ( do_Adp_Eps_Spatial .OR. do_Adp_Eps_Spatial_Evol ) &
  !     CALL user_initial_conditions__VT(u, nlocal, ne_local)!, t_local, scl, scl_fltwt, iter)
  

  nwlt_total = nwlt
  CALL parallel_global_sum(INTEGER=nwlt_total) 
  IF (par_rank .EQ. 0) THEN
        PRINT *, '********************************************'
        PRINT *, '* j_lev = ',j_lev,        '  *' 
        PRINT *, '* nwlt  = ', nwlt_total,  '  *' 
        PRINT *, '********************************************'
  END IF

 
END SUBROUTINE user_initial_conditions

SUBROUTINE dpdx (pderiv,nlocal,Yloc,meth,myjl,split)
  IMPLICIT NONE
  INTEGER, INTENT (IN) :: nlocal
    REAL (pr), DIMENSION (nlocal), INTENT(INOUT) :: pderiv
    REAL (pr), DIMENSION (nlocal,MAX(Nspecm,1)), INTENT(IN) :: Yloc
    INTEGER, INTENT(IN) :: meth, myjl
    LOGICAL, INTENT(IN) :: split
    INTEGER :: i
    REAL (pr), DIMENSION (nlocal,dim) :: du
    REAL (pr), DIMENSION (nlocal,dim) :: d2u



    IF (split) THEN 
       CALL c_diff_fast(pderiv(:), du(:,:), d2u(:,:), myjl, nlocal, MOD(meth,2)+BIASING_BACKWARD, 10, 1, 1, 1)
       d2u(:,1) = du(:,1)
       CALL c_diff_fast(pderiv(:), du(:,:), d2u(:,:), myjl, nlocal, MOD(meth,2)+BIASING_FORWARD , 10, 1, 1, 1)
       IF (stepsplit) THEN
          d2u(:,2) = 1.0_pr
          d2u(:,2) = (SIGN(d2u(:,2), Yloc(:,1) - Y_Xhalf) + 1.0_pr)/2.0_pr
       ELSE
          d2u(:,2) = (1.0_pr - SUM(Yloc(:,1:Nspecm),DIM=2))/MW_in(Nspec)  !Y_Nspec/W_Nspec
          DO i=1,Nspecm
             d2u(:,2) = d2u(:,2) + Yloc(:,i)/MW_in(i)  !Y_l/W_l
          END DO
          d2u(:,2) = Yloc(:,1)/MW_in(1)/d2u(:,2)  !X_1
          d2u(:,2) = (d2u(:,2)-peakchange)/(1.0_pr-2.0_pr*peakchange)  !X undoing peak change
       END IF
       d2u(:,2) = MIN(1.0_pr,MAX(0.0_pr,d2u(:,2)))
       pderiv(:) = d2u(:,2)*d2u(:,1) + (1.0_pr - d2u(:,2))*du(:,1)
    ELSE
       CALL c_diff_fast(pderiv(:), du(:,:), d2u(:,:), myjl, nlocal, meth, 10, 1, 1, 1)
       pderiv(:) = du(:,1)
    END IF
  END SUBROUTINE dpdx

  SUBROUTINE dpdx_diag (pd_diag,nlocal,Yloc,meth,myjl,split)
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: nlocal
    REAL (pr), DIMENSION (nlocal), INTENT(INOUT) :: pd_diag
    REAL (pr), DIMENSION (nlocal,MAX(Nspecm,1)), INTENT(IN) :: Yloc
    INTEGER, INTENT(IN) :: meth, myjl
    LOGICAL, INTENT(IN) :: split
    INTEGER :: i
    REAL (pr), DIMENSION (nlocal,dim) :: du_diag, d2u_diag

    IF (split) THEN 
       CALL c_diff_diag(du_diag, d2u_diag, myjl, nlocal, MOD(meth,2)+BIASING_BACKWARD, MOD(meth,2)+BIASING_FORWARD, 10) 
       pd_diag(:) = du_diag(:,1)
       CALL c_diff_diag(du_diag, d2u_diag, myjl, nlocal, MOD(meth,2)+BIASING_FORWARD, MOD(meth,2)+BIASING_BACKWARD, 10)   
       IF (stepsplit) THEN
          d2u_diag(:,2) = 1.0_pr
          d2u_diag(:,2) = (SIGN(d2u_diag(:,2), Yloc(:,1) - Y_Xhalf) + 1.0_pr)/2.0_pr
       ELSE
          d2u_diag(:,2) = (1.0_pr - SUM(Yloc(:,1:Nspecm),DIM=2))/MW_in(Nspec)
          DO i=1,Nspecm
             d2u_diag(:,2) = d2u_diag(:,2) + Yloc(:,i)/MW_in(i)
          END DO
          d2u_diag(:,2)=Yloc(:,1)/MW_in(1)/d2u_diag(:,2)  !X
          d2u_diag(:,2)=(d2u_diag(:,2)-peakchange)/(1.0_pr-2.0_pr*peakchange)  !X undoing peak change
       END IF
       d2u_diag(:,2) = MIN(1.0_pr,MAX(0.0_pr,d2u_diag(:,2)))
       pd_diag(:) = d2u_diag(:,2)*pd_diag(:) + (1.0_pr - d2u_diag(:,2))*du_diag(:,1)
    ELSE
       CALL c_diff_diag(du_diag, d2u_diag, j_lev, ng, 2*methpd+meth, 2*methpd+meth, 10)   !dp/dx done using methpd 
       pd_diag(:) = du_diag(:,1)
    END IF
  END SUBROUTINE dpdx_diag


  !--********************************  
  ! Arguments
  ! u         - field on adaptive grid
  ! nlocal      - number of active points
  ! ne_local       - number of equations
  ! t         - current time
  !
  ! Global variables used
  ! ifn       - number of equations
  ! x()
  ! xO
  ! y0
  !--******************************** 
  SUBROUTINE user_algebraic_BC (Lu, u, nlocal, ne_local, jlev, meth)
    !--Defines boundary condition type
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: jlev, meth, ne_local, nlocal
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (INOUT) :: Lu
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (IN)    :: u

    INTEGER :: ie, i, ii, shift, denshift,eshift
    INTEGER, DIMENSION (dim) :: velshift 
    REAL (pr), DIMENSION (ne_local,nlocal,dim) :: du, d2u !uncomment if Neuman BC are used, need to call 
    REAL (pr), DIMENSION (1,nlocal,dim) :: du_prev
    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc
   
    INTEGER :: lowvar, highvar
 
    !shift markers for BC implementation
    denshift  = nlocal*(nden-1)
    DO i = 1,dim
       velshift(i) = nlocal*(nvel(i)-1)
    END DO
    eshift    = nlocal*(neng-1)


    !IF(sgsmodel_compressible == sgsmodel_compressible_LDKM) THEN 
    !   lowvar = n_var_k
    !   highvar = n_var_k
    !   IF (lagrangian_avg .EQ. 1) highvar = n_var_compressible_Inn
    !END IF


    !BC for both 2D and 3D cases

    !if Neuman BC are used, need to call 
!!$    CALL c_diff_fast (u, du, d2u, jlev, nlocal, meth, 10, ne_local, 1, ne_local)
    DO ie = 1, ne_local
       shift = nlocal*(ie-1)
       !--Go through all Boundary points that are specified
       i_p_face(0) = 1
       DO i=1,dim
          i_p_face(i) = i_p_face(i-1)*3
       END DO
       DO face_type = 0, 3**dim - 1
          face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
          IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
             CALL get_all_indices_by_face (face_type, jlev, nloc, iloc)
             IF(nloc > 0 ) THEN 
                IF( face(2) == 1  ) THEN  !Left and Right - Full
                   IF(ie >= nvel(1) .AND. ie <= nvel(dim)) THEN  !Momentums
                      Lu(shift+iloc(1:nloc)) = u(shift+iloc(1:nloc))       !Dirichlet conditions
                   ELSE IF(ie == neng) THEN
                      Lu(shift+iloc(1:nloc)) = u(shift+iloc(1:nloc))   - u(denshift+iloc(1:nloc))*(cp_in(Nspec)-1.0_pr/MW_in(Nspec)/Ma**2.0_pr)*1.0_pr     !Dirichlet conditions - Isothermal, no slip
                   END IF
                ELSE IF( face(2) == -1  ) THEN  !Left and Right - Full
                   IF(ie >= nvel(2) .AND. ie <= nvel(dim)) THEN  !Momentums
                      Lu(shift+iloc(1:nloc)) = u(shift+iloc(1:nloc))   !Dirichlet conditions
                   ELSE IF(ie == nvel(1) ) THEN  !Momentums
                      Lu(shift+iloc(1:nloc)) = u(shift+iloc(1:nloc))  !-     u(denshift+iloc(1:nloc))   !Dirichlet conditions  - additional for couette flow
                   ELSE IF(ie == neng) THEN
                      Lu(shift+iloc(1:nloc)) = u(shift+iloc(1:nloc))   - u(denshift+iloc(1:nloc))*(cp_in(Nspec)-1.0_pr/MW_in(Nspec)/Ma**2.0_pr)*1.0_pr     !Dirichlet conditions - Isothermal, no slip
                      !Lu(shift+iloc(1:nloc)) = u(shift+iloc(1:nloc)) -0.5_pr*u(denshift+iloc(1:nloc))  - u(denshift+iloc(1:nloc))*(cp_in(Nspec)-1.0_pr/MW_in(Nspec)/Ma**2.0_pr)*1.0_pr     !Dirichlet conditions - Isothermal, no slip
                   END IF
                END IF
             END IF
          END IF
       END DO
    END DO

    !variable thresholding if Evolution eps is used, penalization generally uses interpolation
    !IF ( do_Adp_Eps_Spatial_Evol ) &
    !CALL VT_algebraic_BC (Lu, u, du, nlocal, ne_local, jlev, meth)

  END SUBROUTINE user_algebraic_BC

  SUBROUTINE user_algebraic_BC_diag (Lu_diag, nlocal, ne_local, jlev, meth)
    !--Defines boundary condition type
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: jlev, meth, ne_local, nlocal
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (INOUT) :: Lu_diag

    INTEGER :: ie, i, ii, shift, denshift

    REAL (pr), DIMENSION (nlocal,dim) :: du_diag, d2u_diag  !uncomment if Neuman BC are used
    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc
    REAL (pr), DIMENSION (1,nlocal,dim) :: du_prev, d2u
    INTEGER, PARAMETER :: methprev=1

    !BC for both 2D and 3D cases
    INTEGER :: lowvar, highvar

    denshift = nlocal*(nden-1)


    !IF(sgsmodel_compressible == sgsmodel_compressible_LDKM) THEN 
    !   lowvar = n_var_k
    !   highvar = n_var_k
    !   IF (lagrangian_avg .EQ. 1) highvar = n_var_compressible_Inn
    !END IF

    !if Neuman BC are used, need to call 
!!$    CALL c_diff_diag ( du_diag, d2u_diag, jlev, nlocal, meth, meth, 10)

    DO ie = 1, ne_local
       shift = nlocal*(ie-1)
       !--Go through all Boundary points that are specified
       i_p_face(0) = 1
       DO i=1,dim
          i_p_face(i) = i_p_face(i-1)*3
       END DO
       DO face_type = 0, 3**dim - 1
          face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
          IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
             CALL get_all_indices_by_face (face_type, jlev, nloc, iloc)
             IF(nloc > 0 ) THEN 
                IF( ABS(face(2)) == 1  ) THEN 
                 
                   IF(ie >= nvel(1) .AND. ie <= nvel(dim)) THEN
                      Lu_diag(shift+iloc(1:nloc)) = 1.0_pr  !Dirichlet conditions
                   ELSE IF(ie == neng) THEN
                      Lu_diag(shift+iloc(1:nloc)) = 1.0_pr  !Dirichlet conditions
                   END IF
                END IF
             END IF
          END IF
       END DO
    END DO

    !varibale thresholding for evolution eps
    !IF ( do_Adp_Eps_Spatial_Evol ) &
    !CALL VT_algebraic_BC_diag (Lu_diag, du_diag, nlocal, ne_local, jlev, meth) 
  END SUBROUTINE user_algebraic_BC_diag

  SUBROUTINE user_algebraic_BC_rhs (rhs, ne_local, nlocal, jlev)
    !--Sets rhs for boundary conditions
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: ne_local, nlocal, jlev
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (INOUT) :: rhs

    INTEGER :: ie, i, ii, shift, denshift
    INTEGER, PARAMETER ::  meth=1
    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc
    REAL (pr), DIMENSION (ne_local,nlocal,dim) :: duall, d2uall
    REAL (pr), DIMENSION (1,nlocal,dim) :: du_prev, du, d2u  
    REAL (pr) :: p_bc
    INTEGER :: lowvar, highvar

    p_bc = 1.0_pr  !pressure defined for outflow boundary conditions

    denshift=nlocal*(nden-1)

    

    DO ie = 1, ne_local
       shift = nlocal*(ie-1)
       !--Go through all Boundary points that are specified
       i_p_face(0) = 1
       DO i=1,dim
          i_p_face(i) = i_p_face(i-1)*3
       END DO
       DO face_type = 0, 3**dim - 1
          face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
          IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
             CALL get_all_indices_by_face (face_type, jlev, nloc, iloc)
             IF(nloc > 0 ) THEN 
                IF( ABS(face(2)) == 1  ) THEN              
                   IF(ie == nvel(1)) THEN
                      !IF(BC_perturb) THEN
                      !   rhs(shift+iloc(1:nloc)) =  (0.25_pr+ 0.1_pr*SIN(t))*EXP( -x(iloc(1:nloc),1)**2.0_pr/(4.0_pr/36.0_pr) - x(iloc(1:nloc),3)**2.0_pr/(4.0_pr/36.0_pr) ) 
                      !ELSE
                         rhs(shift+iloc(1:nloc)) = 0.0_pr 
                      !END IF  
                   ELSE IF(ie >= nvel(2) .AND. ie <= nvel(dim)) THEN
                      rhs(shift+iloc(1:nloc)) = 0.0_pr 
                   ELSE IF(ie == neng) THEN
                      rhs(shift+iloc(1:nloc)) = 0.0_pr !define temp in algebraic BC
                   END IF
                END IF
             END IF
          END IF
       END DO
    END DO

    !variable thresholding for evolution eps
    !IF ( do_Adp_Eps_Spatial_Evol ) &
    !     CALL VT_algebraic_BC_rhs(rhs, ne_local, nlocal, jlev) 

  END SUBROUTINE user_algebraic_BC_rhs

  FUNCTION user_chi (nlocal, t_local)
    USE parallel
    USE threedobject
    IMPLICIT NONE
    INTEGER,          INTENT (IN) :: nlocal
    REAL (pr),        INTENT (IN) :: t_local
    REAL (pr), DIMENSION (nlocal) :: user_chi
    REAL (pr), DIMENSION (nlocal) :: dist_loc
    REAL(pr), DIMENSION(nlocal,1) :: forcing
    REAL(pr) :: smooth_coeff

    user_chi = 0.0_pr
     !WHERE ( SQRT( (x(:,1)- SIGN(2.5_pr/3.0_pr,x(:,2)))**2.0_pr + (ABS(x(:,2)) - 0.7_pr )**2.0_pr) <= 0.2_pr )
     !   user_chi = 1.0_pr
     !ELSEWHERE
     !   user_chi = 0.0_pr
     !END WHERE

  END FUNCTION user_chi

  FUNCTION user_mapping (nlocal, t_local )
    USE curvilinear
    USE curvilinear_mesh
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: nlocal
    REAL (pr), INTENT (IN) :: t_local
    REAL (pr), DIMENSION (nlocal,dim) :: user_mapping

    user_mapping(:,1:dim) = x(:,1:dim)

    ! channel boundary layer stretching 
    IF ( dim .GE. 2 .AND. transform_dir(2) .EQ. 1 ) THEN 
       user_mapping(:,:) = channel_tanh( x, nlocal, mesh_cluster_param, 2 )
    END IF

   

    IF( transform_dir(1) .NE. 0 &
        .OR. ( transform_dir(3) .NE. 0 .AND. dim .GE. 3  )  ) THEN
       PRINT *, 'Incorrect transform directions for channel flow.  Check transform_dir()'
       PRINT *, 'EXITING...'
       STOP
    END IF

  END FUNCTION user_mapping

  SUBROUTINE internal_rhs (int_rhs,u_integrated,doBC,addingon)
    USE penalization
    IMPLICIT NONE
    REAL (pr), DIMENSION (ng,ne), INTENT(IN) :: u_integrated !1D flat version of integrated variables without BC 
    REAL (pr), DIMENSION (ng*ne), INTENT(INOUT) :: int_rhs
    INTEGER, INTENT(IN) :: doBC !if zero do full rhs, if one no x-derivatives 
    INTEGER, INTENT(IN) :: addingon 

    INTEGER :: i, j, l, idim, ie, shift,  tempintd, jshift
    INTEGER, PARAMETER :: meth=1
    REAL (pr), DIMENSION (ne*dim,ng,dim) :: du, d2u
    REAL (pr), DIMENSION (ne*dim,ng,dim) :: dudum, d2udum
    REAL (pr), DIMENSION (ne*dim,ng,dim) :: duC, d2uC,duF, d2uF,duB, d2uB,du_upwind, d2u_upwind    !forward and backwards differencing ERIC: apply to derivatives in interior domain
    REAL (pr), DIMENSION (ng,ne*dim) :: F
    REAL (pr), DIMENSION (ng,dim+Nspec) :: v ! velocity components + temperature + Y
    REAL (pr) :: usej
    REAL (pr), DIMENSION (ng) :: p
    REAL (pr), DIMENSION (ng,3) :: props

    REAL (pr) , DIMENSION(ng) :: dist_loc
    REAL (pr) , DIMENSION(ng,dim) :: obs_norm
    REAL (pr) , DIMENSION(ng) :: visc_loc
    REAL (pr) , DIMENSION(2,ng,dim) :: bias_mask
    INTEGER :: meth_central,meth_backward,meth_forward

    REAL (pr) :: diff_max,diff_min


    meth_central  = meth + BIASING_NONE
    meth_backward = meth + BIASING_BACKWARD
    meth_forward  = meth + BIASING_FORWARD

    int_rhs = 0.0_pr
    IF (Nspec>1) THEN
       DO l = 1,Nspecm
          v(:,dim+1+l) = u_integrated(:,nspc(l))/u_integrated(:,nden)
       END DO
    END IF
    p(:) = 1.0_pr
    IF (Nspec>1)  p(:) = (1.0_pr-SUM(v(:,dim+2:dim+Nspec),DIM=2))  !Y_Nspec
    props(:,1) = p(:)*cp_in(Nspec) !cp_Nspec
    props(:,2) = p(:)/MW_in(Nspec)/Ma**2.0_pr !R_Nspec
    DO l=1,Nspecm
       props(:,1) = props(:,1) + v(:,dim+1+l)*cp_in(l) !cp
       props(:,2) = props(:,2) + v(:,dim+1+l)/MW_in(l)/Ma**2.0_pr !R
    END DO
    p(:) = (u_integrated(:,neng)-0.5_pr*SUM(u_integrated(:,nvel(1):nvel(dim))**2,DIM=2)/u_integrated(:,nden))/(props(:,1)/props(:,2)-1.0_pr)  !pressure
    v(:,dim+1) = p(:)/props(:,2)/u_integrated(:,nden)  !Temperature

    DO i = 1, dim
       v(:,i) = u_integrated(:,nvel(i)) / u_integrated(:,nden) ! u_i
    END DO

    !
    !--Form right hand side of Euler Equations
    !
    F(:,:) = 0.0_pr
    DO j = 1+doBC, dim  !iterate across dimensions
       jshift=ne*(j-1)
       F(:,nden+jshift) = F(:,nden+jshift) + u_integrated(:,nvel(j)) ! rho*u_j
       DO i = 1, dim
          F(:,nvel(i)+jshift) = F(:,nvel(i)+jshift) + u_integrated(:,nvel(i))*v(:,j) ! rho*u_i*u_j
       END DO
       F(:,nvel(j)+jshift) = F(:,nvel(j)+jshift) + p(:) ! rho*u_i^2+p
       F(:,neng+jshift) = F(:,neng+jshift) + ( u_integrated(:,neng) + p(:) )*v(:,j) ! (rho*e+p)*u_j
       DO l=1,Nspecm
          F(:,nspc(l)+jshift) = F(:,nspc(l)+jshift) + v(:,dim+1+l)*u_integrated(:,nvel(j)) ! rho*Y*u_j
       END DO  
    END DO
    IF ((doBC .eq. 0) .AND. ((methpd .ne. 0) .OR. splitFBpress )) THEN  !IF doBC>0, these terms are done inside LODI subroutines,  ******problem location?
          F(:,nvel(1)) = F(:,nvel(1)) - p(:)
          F(:,neng)    = F(:,neng)    - p(:)*v(:,1)  
    END IF



    !
    !-- Form right hand side of Navier-Stokes Equations
    !
    IF(NS) THEN 

       CALL c_diff_fast(v, du(1:dim+Nspec,:,:), d2u(1:dim+Nspec,:,:), j_lev, ng, meth_central, 10, dim+Nspec, 1,dim+Nspec) ! du_i/dx_j & dT/dx_j & dY_l/dx_j

       p(:)=1.0_pr
       IF (Nspec>1) p(:)=p(:)-SUM(v(:,dim+2:dim+Nspec),DIM=2)
       !props(:,1)=p(:)*mu_in(Nspec)  !mu_Nspec
       props(:,1)=p(:)*mu_in(Nspec) * (1.0_pr+S1)/(v(:,dim+1) +S1)*(v(:,dim+1))**(1.5_pr)  !mu_Nspec - variable viscosity
       !props(:,2)=p(:)*kk_in(Nspec)  !kk_Nspec
       props(:,2)=p(:)*kk_in(Nspec) * (1.0_pr+S1)/(v(:,dim+1) +S1)*(v(:,dim+1))**(1.5_pr)   !kk_Nspec
       props(:,3)=p(:)*bD_in(Nspec)  !bD_Nspec
       IF (Nspec>1) THEN
          DO l=1,Nspecm
             props(:,1)=props(:,1)+v(:,dim+1+l)*mu_in(l) * (1.0_pr+S1)/(v(:,dim+1) +S1)*(v(:,dim+1))**(1.5_pr)  !mu
             props(:,2)=props(:,2)+v(:,dim+1+l)*kk_in(l) * (1.0_pr+S1)/(v(:,dim+1) +S1)*(v(:,dim+1))**(1.5_pr)  !kk
             props(:,3)=props(:,3)+v(:,dim+1+l)*bD_in(l)  !bD
          END DO
       END IF
       IF ( user_smagorinsky ) THEN 
          props(:,1) = props(:,1) + user_mu_turb(:)
          props(:,2) = props(:,2) + cp_in(Nspec) * user_mu_turb(:) / user_pr_turb
          props(:,3) = props(:,3) + user_mu_turb(:) / user_sc_turb
       END IF

       p(:) = du(1,:,1) !reuse of a variable
       DO i = 2, dim
          p(:) = p(:) + du(i,:,i) !du_i/dx_i
       END DO
       
          d2u(:,:,:) = 1.0_pr
       DO j = 1, dim
          jshift=ne*(j-1)
          DO i = 1, dim
             F(:,nvel(i)+jshift) = F(:,nvel(i)+jshift) - props(:,1)*( du(i,:,j)+du(j,:,i) ) * d2u(i,:,j)  !rho*u_i*u_j - 2*mu*S_ij/Re  
          END DO
          F(:,nvel(j)+jshift) = F(:,nvel(j)+jshift) + 2.0_pr/3.0_pr*props(:,1)*p(:) * d2u(j,:,j)  !rho*u_i*u_j - tau_ij/Re  [ tau_ij = 2*mu*( S_ij - 2/3*S_kk*delta_ij ) ] 
          DO i = 1, dim
             F(:,neng+jshift) =  F(:,neng+jshift) - props(:,1)*( du(i,:,j)+du(j,:,i) )*v(:,i) * d2u(i,:,j)  !(e+p)*u_j - u_i*2*mu*S_ij/Re 
          END DO
          F(:,neng+jshift) =  F(:,neng+jshift) + 2.0_pr/3.0_pr*props(:,1)*p(:)*v(:,j) * d2u(j,:,j) - props(:,2)*du(dim+1,:,j) * d2u(2,:,j)  
                                                                                  !(e+p)*u_j - u_i*tau_ij/Re - q_j/Re/Pra [ q_j = mu dT/dx_j ]
          IF (Nspec>1) THEN
             DO l=1,Nspecm
                F(:,neng+jshift) =  F(:,neng+jshift) - u_integrated(:,nden)*cp_in(l)*v(:,dim+1)*props(:,3)*du(dim+1+l,:,j) * d2u(2,:,j)
             END DO
             F(:,neng+jshift) =  F(:,neng+jshift) +  u_integrated(:,nden)*cp_in(Nspec)*v(:,dim+1)*props(:,3)*SUM(du(dim+2:dim+Nspec,:,j),DIM=1) * d2u(2,:,j)
             DO l=1,Nspecm
                F(:,nspc(l)+jshift) =  F(:,nspc(l)+jshift) - u_integrated(:,nden)*props(:,3)*du(dim+1+l,:,j) * d2u(2,:,j)
             END DO
          END IF
       END DO
    END IF
    IF (GRAV) THEN    
      shift = (neng-1)*ng   
      DO j=1,dim
         int_rhs(shift+1:shift+ng) = int_rhs(shift+1:shift+ng) - u_integrated(:,nvel(j))*gr_in(Nspec*j)
      END DO
    END IF
    tempintd=ne*dim

    CALL c_diff_fast(F(:,1:tempintd), du(1:tempintd,:,:), d2u(1:tempintd,:,:), j_lev, ng, meth_central, 10, tempintd, 1, tempintd)  !du(i,:,k)=dF_ij/dx_k
    
    
    DO j = 1, dim
       jshift=ne*(j-1)
       DO i = 1, dim+1+Nspec
          shift=(i-1)*ng


          !IF (obstacle == .TRUE. .AND. (i == nden .OR. i == neng)) THEN  !add brinkman term to the continuity equation
             int_rhs(shift+1:shift+ng) = int_rhs(shift+1:shift+ng) - du(i+jshift,:,j)  ! -dF_ij/dx_j 
       END DO
       IF (GRAV) THEN      
          shift = (nvel(j)-1)*ng
          int_rhs(shift+1:shift+ng) = int_rhs(shift+1:shift+ng) - gr_in(Nspec*j)*u_integrated(:,nden)
       END IF
    END DO
 
    
    IF (imask_obstacle) THEN
       IF(perturb_forcing) THEN
             int_rhs((nvel(2)-1)*ng+1:nvel(2)*ng) = int_rhs((nvel(2)-1)*ng+1:nvel(2)*ng) + penal*0.25_pr*SIN(2.0_pr/3.0_pr*t)  !ERIC: assuming alpha = 5/3 and c = 0.4 for blasius
       END IF

       !penalize energy for brinkman
       IF(brinkman_penal) THEN 
          int_rhs((nden-1)*ng+1:nden*ng) = int_rhs((nden-1)*ng+1:nden*ng)*(1.0_pr+ (1.0_pr/porosity-1.0_pr)*penal) !brinkman on energy
          DO i = 1,dim  !penalize momentum for both brinkman/vp
             int_rhs((nvel(i)-1)*ng+1:nvel(i)*ng) = int_rhs((nvel(i)-1)*ng+1:nvel(i)*ng) -penal*v(1:ng,i)/eta_b  !we need velocity
          END DO
          int_rhs((neng-1)*ng+1:neng*ng) = int_rhs((neng-1)*ng+1:neng*ng)*(1.0_pr+ (1.0_pr/porosity-1.0_pr)*penal) !brinkman on energy
       END IF
       !volume penalizations are done in separate subroutine
    END IF


    !cv_loc = 
    !IF(compressible_SCALES)CALL SGS_compressible_rhs (int_rhs, u_integrated, props(:,1), 1.0_pr/MW_in(Nspec)/Ma**2.0_pr  , (cp_in(Nspec)-1.0_pr/MW_in(Nspec)/Ma**2.0_pr), 1.0_pr/Ma, MW_in(Nspec)  , meth)

  END SUBROUTINE internal_rhs

  SUBROUTINE internal_Drhs (int_Drhs, u_p, u_prev, meth,doBC,addingon)
    USE penalization
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: meth
    REAL (pr), DIMENSION (ng,ne), INTENT(IN) :: u_p, u_prev
    REAL (pr), DIMENSION (ng*ne), INTENT(INOUT) :: int_Drhs
    INTEGER, INTENT(IN) :: doBC !if zero do full rhs, if one no x-derivatives 
    INTEGER, INTENT(IN) :: addingon 

    INTEGER :: i, j, l, idim, ie, shift,  vshift, jshift, tempintd
    REAL (pr), DIMENSION (ne*(dim+Nspec),ng,dim) :: du, d2u
    REAL (pr), DIMENSION (ne*(dim+Nspec),ng,dim) :: dudum, d2udum
    REAL (pr), DIMENSION (ne*(dim+Nspec),ng,dim) :: duC, d2uC,duF, d2uF,duB, d2uB,du_upwind, d2u_upwind    !forward and backwards differencing
    REAL (pr), DIMENSION (ng,ne*dim) :: F
    REAL (pr), DIMENSION (ng,(dim+Nspec)*2) :: v_prev ! velocity components + temperature
    REAL (pr), DIMENSION (ng) :: p, p_prev
    REAL (pr), DIMENSION (ng,3) :: props, props_prev

    REAL (pr) , DIMENSION(ng) :: dist_loc
    REAL (pr) , DIMENSION(ng,dim) :: obs_norm
    REAL (pr) , DIMENSION(ng) :: visc_loc
    INTEGER :: meth_central,meth_backward,meth_forward
    REAL (pr) :: diff_max,diff_min
    REAL (pr) , DIMENSION(2,ng,dim) :: bias_mask


    meth_central  = meth + BIASING_NONE
    meth_backward = meth + BIASING_BACKWARD
    meth_forward  = meth + BIASING_FORWARD


    int_Drhs = 0.0_pr
    vshift = dim+Nspec
    DO l=1,Nspecm
       v_prev(:,dim+1+l) = u_prev(:,nspc(l))/u_prev(:,nden)
       v_prev(:,vshift+dim+1+l) = u_p(:,nspc(l))/u_prev(:,nden) - u_prev(:,nspc(l))*u_p(:,nden)/u_prev(:,nden)**2
    END DO
    p_prev(:) = 1.0_pr
    p(:) = 0.0_pr
    IF (Nspec>1) p_prev(:) = (p_prev(:)-SUM(v_prev(:,dim+2:dim+Nspec),DIM=2))  !Y0_Nspec
    IF (Nspec>1) p(:)      =           -SUM(v_prev(:,vshift+dim+2:vshift+dim+Nspec),DIM=2)  !Y'_Nspec
    props_prev(:,1) = p_prev(:)*cp_in(Nspec) !cp0_Nspec
    props_prev(:,2) = p_prev(:)/MW_in(Nspec)/Ma**2.0_pr !R0_Nspec
    props(:,1)      = p(:)*cp_in(Nspec) !cp'_Nspec
    props(:,2)      = p(:)/MW_in(Nspec)/Ma**2.0_pr !R'_Nspec
    DO l=1,Nspecm
       props_prev(:,1) = props_prev(:,1) + v_prev(:,dim+1+l)*cp_in(l) !cp0
       props_prev(:,2) = props_prev(:,2) + v_prev(:,dim+1+l)/MW_in(l)/Ma**2.0_pr !R0
       props(:,1)      = props(:,1)      + v_prev(:,vshift+dim+1+l)*cp_in(l) !cp'
       props(:,2)      = props(:,2)      + v_prev(:,vshift+dim+1+l)/MW_in(l)/2.0_pr !R'
    END DO
    p_prev(:) = (u_prev(:,neng)-0.5_pr*SUM(u_prev(:,nvel(1):nvel(dim))**2,DIM=2)/u_prev(:,nden))/(props_prev(:,1)/props_prev(:,2)-1.0_pr)  !pressure
    p(:) = (u_prev(:,neng)-0.5_pr*SUM(u_prev(:,nvel(1):nvel(dim))**2,DIM=2)/u_prev(:,nden)) * (props_prev(:,1)*props(:,2)-props(:,1)*props_prev(:,2)) / (props_prev(:,1)-props_prev(:,2))**2 + &
           (u_p(:,neng)+0.5_pr*SUM(u_prev(:,nvel(1):nvel(dim))**2,DIM=2)/u_prev(:,nden)**2*u_p(:,nden)-SUM(u_prev(:,nvel(1):nvel(dim))*u_p(:,nvel(1):nvel(dim)),DIM=2)/u_prev(:,nden)) /  &
             (props_prev(:,1)/props_prev(:,2)-1.0_pr) 
    v_prev(:,dim+1) = p_prev(:)/props_prev(:,2)/u_prev(:,nden)  !Temperature
    v_prev(:,vshift+dim+1) = (p(:)-p_prev(:)*(props(:,2)/props_prev(:,2)+u_p(:,nden)/u_prev(:,nden)))/props_prev(:,2)/u_prev(:,nden)  !Temperature 
    
    DO i = 1, dim
       v_prev(:,i) = u_prev(:,nvel(i)) / u_prev(:,nden) ! u_i
       v_prev(:,vshift+i) = u_p(:,nvel(i))/u_prev(:,nden) - u_prev(:,nvel(i))*u_p(:,nden)/u_prev(:,nden)**2  !u_i
    END DO
 
    !
    !--Form right hand side of Euler Equations
    !
    F(:,:) = 0.0_pr
    DO j = 1+doBC, dim
       jshift=ne*(j-1)
       F(:,nden+jshift) = F(:,nden+jshift) + u_p(:,nvel(j)) ! rho*u_j
       DO i = 1, dim ! (rho*u_i*u_j)'
          F(:,nvel(i)+jshift) = F(:,nvel(i)+jshift) + u_prev(:,nvel(i))*v_prev(:,vshift+j) + u_p(:,nvel(i))*v_prev(:,j)
       END DO
       F(:,nvel(j)+jshift) = F(:,nvel(j)+jshift) + p(:) ! (rho*u_i^2)'+p'
       F(:,neng+jshift) = F(:,neng+jshift) + (u_p(:,neng) + p(:))*v_prev(:,j) + (u_prev(:,neng) + p_prev(:))*v_prev(:,vshift+j)  ! ((e+p)*u_j)'
       DO l=1,Nspecm
          F(:,nspc(l)+jshift) = F(:,nspc(l)+jshift) + v_prev(:,vshift+dim+1+l)*u_prev(:,nvel(j))+v_prev(:,dim+1+l)*u_p(:,nvel(j))
       END DO
    END DO

    IF (doBC .ne. 0) THEN  !Missing term from LODI assumps... gamma=constant
       shift = (neng-1)*ng
       props_prev(:,3)=props_prev(:,1)/props_prev(:,2)  !gamma/(gamma-1)0
       CALL c_diff_fast(props_prev(:,3), du(1,:,:), d2u(1,:,:), j_lev, ng, meth, 10, 1, 1, 1)  !d(gamma/(gamma-1))/dx
       int_Drhs(shift+1:shift+ng) = int_Drhs(shift+1:shift+ng)-du(1,:,1)*(v_prev(:,1)*p(:)+v_prev(:,vshift+1)*p_prev(:)) !pu*d(gamma/(gamma-1))/dx
       props(:,3)=((props(:,1)*props_prev(:,2)-props_prev(:,1)*props(:,2))/props_prev(:,2)**2) !gamma/(gamma-1)'
       CALL c_diff_fast(props(:,3), du(1,:,:), d2u(1,:,:), j_lev, ng, meth, 10, 1, 1, 1)  !d(gamma/(gamma-1))/dx
       int_Drhs(shift+1:shift+ng) = int_Drhs(shift+1:shift+ng)-du(1,:,1)*v_prev(:,1)*p_prev(:) !pu*d(gamma/(gamma-1))/dx
    END IF

    !
    !-- Form right hand side of Navier-Stokes Equations
    !
    IF(NS) THEN 
       tempintd = 2*vshift

       CALL c_diff_fast(v_prev(:,:), du(1:tempintd,:,:), d2u(1:tempintd,:,:), j_lev, ng, meth_central, 10, tempintd, 1, tempintd) ! du'_i/dx_j & dT'/dx_j

       p_prev(:) = 1.0_pr
       p(:) = 0.0_pr
       IF (Nspec>1) p_prev(:) = (p_prev(:)-SUM(v_prev(:,dim+2:dim+Nspec),DIM=2))  !Y0_Nspec
       IF (Nspec>1) p(:)      =           -SUM(v_prev(:,vshift+dim+2:vshift+dim+Nspec),DIM=2)  !Y'_Nspec
       !props_prev(:,1) = p_prev(:)*mu_in(Nspec)  !mu0_Nspec
       props_prev(:,1) = p_prev(:)*mu_in(Nspec) * (1.0_pr+S1)/(v_prev(:,dim+1) +S1)*(v_prev(:,dim+1))**(1.5_pr)  !mu_Nspec - variable viscosity
       !props_prev(:,2) = p_prev(:)*kk_in(Nspec)  !kk0_Nspec
       props_prev(:,2) = p_prev(:)*kk_in(Nspec) * (1.0_pr+S1)/(v_prev(:,dim+1) +S1)*(v_prev(:,dim+1))**(1.5_pr)  !kk0_Nspec
       props_prev(:,3) = p_prev(:)*bD_in(Nspec)  !bD0_Nspec
       !props(:,1)      = p(:)*mu_in(Nspec)  !mu'_Nspec
       props(:,1)      = p_prev(:)*mu_in(Nspec) * ((1.5_pr*(1.0_pr+S1)*v_prev(:,dim+1)**0.5_pr*v_prev(:,vshift+dim+1))/(v_prev(:,dim+1)+S1) &
        + v_prev(:,dim+1)**1.5_pr*( -(1.0_pr+S1)*v_prev(:,vshift+dim+1) )/(v_prev(:,dim+1)+S1)**2.0_pr) !mu'_Nspec !ERIC: double check for accuracy
       !props(:,2)      = p(:)*kk_in(Nspec)  !kk'_Nspec
       props(:,2)      = p_prev(:)*kk_in(Nspec)   * ((1.5_pr*(1.0_pr+S1)*v_prev(:,dim+1)**0.5_pr*v_prev(:,vshift+dim+1))/(v_prev(:,dim+1)+S1) &
        + v_prev(:,dim+1)**1.5_pr*( -(1.0_pr+S1)*v_prev(:,vshift+dim+1) )/(v_prev(:,dim+1)+S1)**2.0_pr) !kk'_Nspec
       props(:,3)      = p(:)*bD_in(Nspec)  !bD'_Nspec
       DO l=1,Nspecm
          props_prev(:,1) = props_prev(:,1) + v_prev(:,dim+1+l)*mu_in(l) * (1.0_pr+S1)/(v_prev(:,dim+1) +S1)*(v_prev(:,dim+1))**(1.5_pr)  !mu0
          props_prev(:,2) = props_prev(:,2) + v_prev(:,dim+1+l)*kk_in(l) * (1.0_pr+S1)/(v_prev(:,dim+1) +S1)*(v_prev(:,dim+1))**(1.5_pr)  !kk0
          props_prev(:,3) = props_prev(:,3) + v_prev(:,dim+1+l)*bD_in(l)  !bD0
          props(:,1)      = props(:,1)      + v_prev(:,vshift+dim+1+l)*mu_in(l) * (1.5_pr*(1.0_pr+S1)*v_prev(:,dim+1)**0.5_pr*v_prev(:,vshift+dim+1))/(v_prev(:,dim+1)+S1) &
        + v_prev(:,dim+1)**1.5_pr*( -(1.0_pr+S1)*v_prev(:,vshift+dim+1) )/(v_prev(:,dim+1)+S1)**2.0_pr  !mu'
          props(:,2)      = props(:,2)      + v_prev(:,vshift+dim+1+l)*kk_in(l) * (1.5_pr*(1.0_pr+S1)*v_prev(:,dim+1)**0.5_pr*v_prev(:,vshift+dim+1))/(v_prev(:,dim+1)+S1) &
        + v_prev(:,dim+1)**1.5_pr*( -(1.0_pr+S1)*v_prev(:,vshift+dim+1) )/(v_prev(:,dim+1)+S1)**2.0_pr  !kk'
          props(:,3)      = props(:,3)      + v_prev(:,vshift+dim+1+l)*bD_in(l)  !bD'
       END DO
       IF ( user_smagorinsky ) THEN 
          props_prev(:,1) = props_prev(:,1) + user_mu_turb(:)
          props_prev(:,2) = props_prev(:,2) + cp_in(Nspec) * user_mu_turb(:) / user_pr_turb
          props_prev(:,3) = props_prev(:,3) + user_mu_turb(:) / user_sc_turb
          props(:,1) = props(:,1) + user_mu_turb(:)
          props(:,2) = props(:,2) + cp_in(Nspec) * user_mu_turb(:) / user_pr_turb
          props(:,3) = props(:,3) + user_mu_turb(:) / user_sc_turb
       END IF

       p(:) = du(vshift+1,:,1) !reuse of a variable
       DO i = 2, dim
          p(:) = p(:) + du(vshift+i,:,i) !du'_i/dx_i
       END DO

          d2u(:,:,:) = 1.0_pr
       DO j = 1, dim
          jshift=ne*(j-1)
          DO i = 1, dim
             F(:,nvel(i)+jshift) = F(:,nvel(i)+jshift) - props_prev(:,1)*( du(vshift+i,:,j)+du(vshift+j,:,i) ) * d2u(i,:,j)  !rho*u_i*u_j - 2*mu*S_ij/Re
          END DO
          F(:,nvel(j)+jshift) = F(:,nvel(j)+jshift) + 2.0_pr/3.0_pr*props_prev(:,1)*p(:) * d2u(j,:,j) !rho*u_i*u_j - tau_ij/Re  [ tau_ij = 2*mu*( S_ij - 2/3*S_kk*delta_ij ) ]
          DO i = 1, dim
             F(:,neng+jshift) =  F(:,neng+jshift) - props_prev(:,1)*( du(vshift+i,:,j)+du(vshift+j,:,i) )*v_prev(:,i) * d2u(i,:,j)  !(e+p)*u_j - u_i*2*mu*S_ij/Re
          END DO
          F(:,neng+jshift) =  F(:,neng+jshift) + 2.0_pr/3.0_pr*props_prev(:,1)*p(:)*v_prev(:,j) * d2u(j,:,j) - props_prev(:,2)*du(vshift+dim+1,:,j) * d2u(2,:,j)  
                                                                                             !(e+p)*u_j - u_i*tau_ij/Re - q_j/Re/Pra [ q_j = mu dT/dx_j ]
          DO l=1,Nspecm
             F(:,neng+jshift) =  F(:,neng+jshift) - u_prev(:,nden)*cp_in(l)*v_prev(:,dim+1)*props_prev(:,3)*du(vshift+dim+1+l,:,j) * d2u(2,:,j)
          END DO
          IF (Nspec>1) F(:,neng+jshift) =  F(:,neng+jshift) +  u_prev(:,nden)*cp_in(Nspec)*v_prev(:,dim+1)*props_prev(:,3)*SUM(du(vshift+dim+2:vshift+dim+Nspec,:,j),DIM=1) * d2u(2,:,j)
          DO l=1,Nspecm
             F(:,nspc(l)+jshift) =  F(:,nspc(l)+jshift) - u_prev(:,nden)*props_prev(:,3)*du(vshift+dim+1+l,:,j) * d2u(2,:,j)
          END DO
       END DO

       p_prev(:) = du(1,:,1) !reuse of a variable
       DO i = 2, dim
          p_prev(:) = p_prev(:) + du(i,:,i) !du0_i/dx_i
       END DO
       DO j = 1, dim
          jshift=ne*(j-1)
          DO i = 1, dim
             F(:,nvel(i)+jshift) = F(:,nvel(i)+jshift) - props(:,1)*( du(i,:,j)+du(j,:,i) ) * d2u(i,:,j)  !rho*u_i*u_j - 2*mu*S_ij/Re
          END DO
          F(:,nvel(j)+jshift) = F(:,nvel(j)+jshift) + 2.0_pr/3.0_pr*props(:,1)*p_prev(:) * d2u(j,:,j) !rho*u_i*u_j - tau_ij/Re  [ tau_ij = 2*mu*( S_ij - 2/3*S_kk*delta_ij ) ]
          DO i = 1, dim
             F(:,neng+jshift) =  F(:,neng+jshift) - ( du(i,:,j)+du(j,:,i) )*(v_prev(:,i)*props(:,1)+v_prev(:,vshift+i)*props_prev(:,1)) * d2u(i,:,j)  !(e+p)*u_j - u_i*2*mu*S_ij/Re
          END DO
          F(:,neng+jshift) =  F(:,neng+jshift) + 2.0_pr/3.0_pr*p_prev(:)*(v_prev(:,j)*props(:,1)+v_prev(:,vshift+j)*props_prev(:,1)) * d2u(j,:,j) - props(:,2)*du(dim+1,:,j) * d2u(2,:,j)  
                                                                                             !(e+p)*u_j - u_i*tau_ij/Re - q_j/Re/Pra [ q_j = mu dT/dx_j ]
          DO l=1,Nspecm
             F(:,neng+jshift) =  F(:,neng+jshift) - cp_in(l)*du(dim+1+l,:,j) * &
                             ( u_p(:,nden)*v_prev(:,dim+1)*props_prev(:,3) + u_prev(:,nden)*(v_prev(:,vshift+dim+1)*props_prev(:,3) + v_prev(:,dim+1)*props(:,3)) ) * d2u(2,:,j)
          END DO
          IF (Nspec>1) F(:,neng+jshift) =  F(:,neng+jshift) +  cp_in(Nspec)*SUM(du(dim+2:dim+Nspec,:,j),DIM=1) * &
                             ( u_p(:,nden)*v_prev(:,dim+1)*props_prev(:,3) + u_prev(:,nden)*(v_prev(:,vshift+dim+1)*props_prev(:,3) + v_prev(:,dim+1)*props(:,3)) ) * d2u(2,:,j)
          DO l=1,Nspecm
             F(:,nspc(l)+jshift) =  F(:,nspc(l)+jshift) - (u_p(:,nden)*props_prev(:,3)+u_prev(:,nden)*props(:,3))*du(dim+1+l,:,j) * d2u(2,:,j)
          END DO
       END DO
    END IF
    IF (GRAV) THEN
      shift = (neng-1)*ng
      DO j=1,dim
         DO l=1,Nspecm
            int_Drhs(shift+1:shift+ng) = int_Drhs(shift+1:shift+ng)-(v_prev(:,dim+1+l)*u_p(:,nvel(j))+v_prev(:,vshift+dim+1+l)*u_prev(:,nvel(j)))*gr_in(l+Nspec*(j-1))
         END DO
         IF (Nspec>1) THEN
            int_Drhs(shift+1:shift+ng) = int_Drhs(shift+1:shift+ng) - ((1.0_pr-SUM(v_prev(:,dim+2:dim+Nspec),DIM=2))*u_p(:,nvel(j))-SUM(v_prev(:,vshift+dim+2:vshift+dim+Nspec),DIM=2)*u_prev(:,nvel(j)) )*gr_in(Nspec*j)
         ELSE
            int_Drhs(shift+1:shift+ng) = int_Drhs(shift+1:shift+ng) - u_p(:,nvel(j))*gr_in(Nspec*j)
         END IF
      END DO
   END IF
    tempintd=ne*dim

    CALL c_diff_fast(F(:,1:tempintd), du(1:tempintd,:,:), d2u(1:tempintd,:,:), j_lev, ng, meth_central, 10, tempintd, 1, tempintd)  !du(i,:,k)=dF_ij/dx_k

    DO j = 1, dim
       jshift=ne*(j-1)
       DO i = 1, dim+1+Nspec
          shift=(i-1)*ng
             int_Drhs(shift+1:shift+ng) = int_Drhs(shift+1:shift+ng) - du(i+jshift,:,j)  ! -dF_ij/dx_j  !continutiy set up
       END DO
       IF (GRAV) THEN      
          shift = (nvel(j)-1)*ng
          DO l=1,Nspecm        
             int_Drhs(shift+1:shift+ng) = int_Drhs(shift+1:shift+ng) - gr_in(l+Nspec*(j-1))*u_p(:,nspc(l))
          END DO
          IF (Nspec>1) THEN
             int_Drhs(shift+1:shift+ng) = int_Drhs(shift+1:shift+ng) - gr_in(Nspec*j)*(u_p(:,nden)-SUM(u_p(:,nspc(1):nspc(Nspecm)),DIM=2))
          ELSE
             int_Drhs(shift+1:shift+ng) = int_Drhs(shift+1:shift+ng) - gr_in(Nspec*j)*u_p(:,nden)
          END IF
       END IF
    END DO
    
    !IF(compressible_SCALES)    CALL SGS_compressible_Drhs (int_Drhs, u_p, u_prev, props(:,1), props_prev(:,1), &
    !                                 1.0_pr/MW_in(Nspec)/Ma**2.0_pr ,(cp_in(Nspec)-1.0_pr/MW_in(Nspec)/Ma**2.0_pr)  ,1.0_pr/Ma , MW_in(Nspec) , meth)!, du, d2u, meth)

    !Add Brinkman Penalization Terms

    IF (imask_obstacle == .TRUE.) THEN
       IF(brinkman_penal) THEN
          int_Drhs((nden-1)*ng+1:nden*ng) = int_Drhs((nden-1)*ng+1:nden*ng) *(1.0_pr+ (1.0_pr/porosity-1.0_pr)*penal) 
          DO i = 1,dim
             !internal rhs, not user_rhs
             int_Drhs((nvel(i)-1)*ng+1:nvel(i)*ng) = int_Drhs((nvel(i)-1)*ng+1:nvel(i)*ng) -penal/eta_b*v_prev(:,vshift+i)
          END DO
          int_Drhs((neng-1)*ng+1:neng*ng) = int_Drhs((neng-1)*ng+1:neng*ng) *(1.0_pr+ (1.0_pr/porosity-1.0_pr)*penal) !/porosity*(TANH(20.0_pr*((x(:,1)**2.0_pr+x(:,2)**2.0_pr)**0.5_pr - 0.25_pr)) + 1.0_pr)*SUM(du(neng,:,1:2)*obs_norm(:,1:2),DIM=2)!check if temperature in subroutine is formed same as TT in brinkman
       END IF
    END IF
  END SUBROUTINE internal_Drhs

  SUBROUTINE internal_Drhs_diag (int_diag,u_prev,meth,doBC,addingon)
    USE penalization
    USE variable_mapping
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: meth
    REAL (pr), DIMENSION (ng*ne), INTENT(INOUT) :: int_diag
    REAL (pr), DIMENSION (ng,ne), INTENT(IN) :: u_prev
    INTEGER, INTENT(IN) :: doBC !if zero do full rhs, if one no x-derivatives 
    INTEGER, INTENT(IN) :: addingon 

    INTEGER :: i, j, l, ll, ie, shift
    REAL (pr), DIMENSION (ng,dim) :: du_diag, d2u_diag, du_diag_F, d2u_diag_F, du_diag_B, d2u_diag_B
    REAL (pr), DIMENSION (MAX(Nspec,dim),ng,dim) :: du, d2u, duB, d2uB, duF, d2uF, du_upwind, d2u_upwind
    REAL (pr), DIMENSION (ng,n_integrated,dim) :: F
    REAL (pr), DIMENSION (ng, Nspec) :: v_prev
    REAL (pr), DIMENSION (ng, dim+1) :: T_prev  !temperature and velocity
    REAL (pr), DIMENSION (ng, dim+1) :: T_p_diag    !momentum and energy
    REAL (pr), DIMENSION (ng, dim+2) :: props_diag  !momentum and energy(x2)
    REAL (pr), DIMENSION (ng,6) :: props_prev
    REAL (pr), DIMENSION (ng) :: p_prev

    REAL (pr) , DIMENSION(ng) :: dist_loc
    REAL (pr) , DIMENSION(ng,dim) :: obs_norm
    INTEGER :: meth_central,meth_backward,meth_forward

    meth_central  = meth + BIASING_NONE
    meth_backward = meth + BIASING_BACKWARD
    meth_forward  = meth + BIASING_FORWARD

    int_diag = 0.0_pr
    DO l=1,Nspecm
       v_prev(:,l) = u_prev(:,nspc(l))/u_prev(:,nden)  ! Y0
    END DO
    v_prev(:,Nspec)=1.0_pr
    IF (Nspec>1) v_prev(:,Nspec) = v_prev(:,Nspec)-SUM(v_prev(:,1:Nspecm),DIM=2)  !Y0_Nspec
    props_prev(:,:)=0.0_pr
    DO l=1,Nspec
       props_prev(:,1) = props_prev(:,1) + v_prev(:,l)*cp_in(l)  !cp0
       props_prev(:,2) = props_prev(:,2) + v_prev(:,l)/MW_in(l)/Ma**2.0_pr  !R0
       props_prev(:,4) = props_prev(:,1) + v_prev(:,l)*mu_in(l)  !mu0  ERIC: not set up for dynamic viscosity, not modularized
       props_prev(:,5) = props_prev(:,2) + v_prev(:,l)*kk_in(l)  !kk0
       props_prev(:,6) = props_prev(:,3) + v_prev(:,l)*bD_in(l)  !bD0
    END DO
    props_prev(:,3) = props_prev(:,1)/(props_prev(:,1)-props_prev(:,2)) !gamma0 = cp0/(cp0-R0)
   
    IF(.TRUE.) THEN !overwrite props !ERIC: correct this because energy and momentum perturbation used to calculate temperature
       p_prev(:) = (u_prev(:,neng)-0.5_pr*SUM(u_prev(:,nvel(1):nvel(dim))**2.0_pr,DIM=2)/u_prev(:,nden))/(props_prev(:,1)/props_prev(:,2)-1.0_pr)  !pressure    
       T_prev(:,1) = p_prev(:)/props_prev(:,2)/u_prev(:,nden)  !Temperature
       DO i=1,dim !momentum diag terms
          T_p_diag(:,i) =    ( u_prev(:,nvel(i))/u_prev(:,nden)) /  &
               (props_prev(:,1)/props_prev(:,2)-1.0_pr)/props_prev(:,2)/u_prev(:,nden)  
       END DO
       T_p_diag(:,dim+1) =    1.0_pr / (props_prev(:,1)/props_prev(:,2)-1.0_pr)/props_prev(:,2)/u_prev(:,nden)  !Energy diag term
       !ERIC: develop props_p coefficient for energy and momentum
       props_prev(:,4:5) = 0.0_pr
       props_diag(:,:)   = 0.0_pr
       DO l=1,Nspec
          props_prev(:,4) = props_prev(:,4) + v_prev(:,l)*mu_in(l)*(1.0_pr+S1)/(T_prev(:,1) +S1)*(T_prev(:,1))**(1.5_pr)   !mu0
          props_prev(:,5) = props_prev(:,5) + v_prev(:,l)*kk_in(l)*(1.0_pr+S1)/(T_prev(:,1) +S1)*(T_prev(:,1))**(1.5_pr)   !kk0
          !diag terms from Temperature-varying props
          DO i = 1,dim
             props_diag(:,i) = props_diag(:,i) + v_prev(:,l)*mu_in(l)* ((1.5_pr*(1.0_pr+S1)*T_prev(:,1)**0.5_pr*T_p_diag(:,i))/(T_prev(:,1)+S1) &
                  + T_prev(:,1)**1.5_pr*( -(1.0_pr+S1)*T_p_diag(:,i) )/(T_prev(:,1)+S1)**2.0_pr)  !mu', velocity
          END DO
          props_diag(:,dim+1) = props_diag(:,dim+1) + v_prev(:,l)*mu_in(l)* ((1.5_pr*(1.0_pr+S1)*T_prev(:,1)**0.5_pr*T_p_diag(:,dim+1))/(T_prev(:,1)+S1) &
               + T_prev(:,1)**1.5_pr*( -(1.0_pr+S1)*T_p_diag(:,dim+1) )/(T_prev(:,1)+S1)**2.0_pr)  !mu', energy
          props_diag(:,dim+2) = props_diag(:,dim+2) + v_prev(:,l)*kk_in(l)* ((1.5_pr*(1.0_pr+S1)*T_prev(:,1)**0.5_pr*T_p_diag(:,dim+1))/(T_prev(:,1)+S1) &
               + T_prev(:,1)**1.5_pr*( -(1.0_pr+S1)*T_p_diag(:,dim+1) )/(T_prev(:,1)+S1)**2.0_pr)  !kk', energy
       END DO
    END IF
    IF ( user_smagorinsky ) THEN 
       props_prev(:,1) = props_prev(:,1) + user_mu_turb(:)
       props_prev(:,2) = props_prev(:,2) + cp_in(Nspec) * user_mu_turb(:) / user_pr_turb
       props_prev(:,3) = props_prev(:,3) + user_mu_turb(:) / user_sc_turb
    END IF
    CALL c_diff_diag(du_diag, d2u_diag, j_lev, ng, meth, meth, -11)
    !
    !--Form right hand side of Euler Equations
    !
    F(:,:,:) = 0.0_pr
    DO j = 1+doBC, dim
       DO i = 1, dim ! (rho*u_i*u_j)'
          F(:,nvel(i),j) = F(:,nvel(i),j) + u_prev(:,nvel(j))/u_prev(:,nden)
       END DO
       F(:,nvel(j),j) = F(:,nvel(j),j) + u_prev(:,nvel(j))/u_prev(:,nden)*(2.0_pr-props_prev(:,3))
       F(:,neng,j) = F(:,neng,j) + props_prev(:,3)*u_prev(:,nvel(j))/u_prev(:,nden)
       DO l=1,Nspecm
          F(:,nspc(l),j) = F(:,nspc(l),j) + u_prev(:,nvel(j))/u_prev(:,nden)
       END DO
    END DO

    IF (doBC .ne. 0) THEN  !Missing term from LODI assumps... gamma=constant
       shift = (neng-1)*ng
       CALL c_diff_fast(props_prev(:,1)/props_prev(:,2), du(1,:,:), d2u(1,:,:), j_lev, ng, meth, 10, 1, 1, 1)  !d(gamma/(gamma-1))/dx
       int_diag(shift+1:shift+ng) = int_diag(shift+1:shift+ng)-du(1,:,1)*u_prev(:,nvel(1))/u_prev(:,nden)*(props_prev(:,3)-1.0_pr) !pu*d(gamma/(gamma-1))/dx
    END IF
    !
    !-- Form right hand side of Navier-Stokes Equations
    !
    IF(NS .AND. NSdiag) THEN
       CALL c_diff_fast(v_prev, du(1:Nspec,:,:), d2u(1:Nspec,:,:), j_lev, ng, meth, 10, Nspec, 1, Nspec) ! dY_l/dx_j

          d2u(:,:,:) = 1.0_pr
       DO j = 1, dim
          DO l=1,Nspec
             IF (Nspec>1) F(:,neng,j) =  F(:,neng,j) - props_prev(:,6)*du(l,:,j)*cp_in(l)/(props_prev(:,1)-props_prev(:,2))*d2u(2,:,j)
          END DO 
       END DO
       DO j = 1, dim
          DO i = 1, dim
             shift=(nvel(i)-1)*ng
             int_diag(shift+1:shift+ng) = int_diag(shift+1:shift+ng) + props_prev(:,4)/u_prev(:,nden)*d2u_diag(:,j) * d2u(i,:,j)
          END DO
          shift=(nvel(j)-1)*ng
          int_diag(shift+1:shift+ng) = int_diag(shift+1:shift+ng) + props_prev(:,4)/u_prev(:,nden)*d2u_diag(:,j)/3.0_pr * d2u(j,:,j)
          shift=(neng-1)*ng
          int_diag(shift+1:shift+ng) = int_diag(shift+1:shift+ng) + props_prev(:,5)/u_prev(:,nden)/(props_prev(:,1)-props_prev(:,2))*d2u_diag(:,j) * d2u(2,:,j)
          DO l = 1, Nspecm
             shift=(nspc(l)-1)*ng
             int_diag(shift+1:shift+ng) = int_diag(shift+1:shift+ng) +(props_prev(:,6)*d2u_diag(:,j)+bD_in(l)*du(l,:,j)) * d2u(2,:,j)
          END DO
       END DO
    END IF
    DO j = 1, dim
       DO i = 1, dim+1+Nspec
          shift=(i-1)*ng
             int_diag(shift+1:shift+ng) = int_diag(shift+1:shift+ng) - F(:,i,j)*du_diag(:,j)
       END DO
    END DO

    
    IF (imask_obstacle == .TRUE. .AND. brinkman_penal) THEN  !not sure if these are correct, use krylov
       int_diag((nden-1)*ng+1:nden*ng) = int_diag((nden-1)*ng+1:nden*ng)*(1.0_pr+ (1.0_pr/porosity-1.0_pr)*penal)
       DO i = 1,dim
          int_diag((nvel(i)-1)*ng+1:nvel(i)*ng) = int_diag((nvel(i)-1)*ng+1:nvel(i)*ng) -penal/eta_b/u_prev(1:ng,nden)
       END DO
       int_diag((neng-1)*ng+1:neng*ng) = int_diag((neng-1)*ng+1:neng*ng)*(1.0_pr+ (1.0_pr/porosity-1.0_pr)*penal)
    END IF
        
    !IF(compressible_SCALES)    CALL SGS_compressible_Drhs_diag (int_diag, u_prev, mu_in(Nspec),1.0_pr/MW_in(Nspec)/Ma**2.0_pr ,(cp_in(Nspec)-1.0_pr/MW_in(Nspec)/Ma**2.0_pr)  ,1.0_pr/Ma , MW_in(Nspec) , meth )

  END SUBROUTINE internal_Drhs_diag

  FUNCTION user_rhs (u_integrated,scalar)
    IMPLICIT NONE
    REAL (pr), DIMENSION (ng,ne), INTENT(IN) :: u_integrated !1D flat version of integrated variables without BC 
    REAL (pr), DIMENSION (ng), INTENT(IN) :: scalar
    REAL (pr), DIMENSION (ng*ne) :: user_rhs

    REAL (pr), DIMENSION (ne,ng,dim) :: du, d2u
    INTEGER, PARAMETER :: meth=1

    !REAL (pr), DIMENSION(ng) :: hyper_mask
    CALL internal_rhs(user_rhs,u_integrated,0,0)

    !IF( do_Adp_Eps_Spatial_Evol )  THEN 
    !   CALL c_diff_fast(u_integrated, du, d2u, j_lev, ng, meth, 11, ne, 1, ne)
    !   CALL user_rhs__VT (user_rhs, u_integrated, du, d2u) ! Cf: OPTIONAL
    !END IF

    IF(hypermodel .NE. 0 .AND. ( hyper_it .LT. hyper_it_limit .OR. hyper_it_limit < 0) ) THEN
      !hyper_mask = set_hyper_mask(ng) 
       CALL hyperbolic(u_integrated,ng,user_rhs)
       IF( par_rank .EQ. 0 ) PRINT *, 'Using Hyperbolic Module'
    END IF

  END FUNCTION user_rhs

  FUNCTION user_Drhs (u_p, u_prev, meth)
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: meth
    REAL (pr), DIMENSION (ng,ne) :: u_p, u_prev
    REAL (pr), DIMENSION (ng*ne) :: user_Drhs
    REAL (pr), DIMENSION (2*ne,ng,dim) :: du
    REAL (pr), DIMENSION (ne,ng,dim)   :: d2u
    !REAL (pr), DIMENSION(ng) :: hyper_mask

    CALL internal_Drhs(user_Drhs,u_p,u_prev,meth,0,0)

    !IF( do_Adp_Eps_Spatial_Evol ) THEN
    !   CALL c_diff_fast(u_prev, du(ne+1:2*ne,:,:), d2u, j_lev, ng, meth, 10, ne, 1, ne)
    !   CALL c_diff_fast(u_p,    du(1:ne,:,:)     , d2u, j_lev, ng, meth, 11, ne, 1, ne)
    !   CALL user_Drhs__VT (user_Drhs, u_p, u_prev, du(1:2*ne,:,:), d2u, meth)
    !END IF

    IF (hypermodel .NE. 0 .AND. ( hyper_it .LT. hyper_it_limit .OR. hyper_it_limit < 0)) THEN
      !hyper_mask = set_hyper_mask(ng) 
      CALL hyperbolic(u_p,ng,user_Drhs)
    END IF

  END FUNCTION user_Drhs

  FUNCTION user_Drhs_diag (meth)
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: meth
    REAL (pr), DIMENSION (ng*ne) :: user_Drhs_diag
    REAL (pr), DIMENSION (ng,ne) :: u_prev
    REAL (pr), DIMENSION (ne,ng,dim) :: du_dummy
    REAL (pr), DIMENSION (ng,dim)    :: du_diag, d2u_diag 
    INTEGER :: ie, shift
    !REAL (pr), DIMENSION(ng) :: hyper_mask

    DO ie = 1, ne
       shift=(ie-1)*ng
       u_prev(1:ng,ie) = u_prev_timestep(shift+1:shift+ng)
    END DO

 
    CALL internal_Drhs_diag(user_Drhs_diag,u_prev,meth,0,0)

    !IF( do_Adp_Eps_Spatial_Evol ) THEN
    !   CALL c_diff_diag(du_diag, d2u_diag, j_lev, ng, meth,  meth, 11) 
    !   CALL user_Drhs_diag__VT (user_Drhs_diag, du_dummy, du_diag, d2u_diag, meth)
    !END IF
    IF(hypermodel .NE. 0 .AND. ( hyper_it .LT. hyper_it_limit .OR. hyper_it_limit < 0)) THEN
      !hyper_mask = set_hyper_mask(ng) 
      CALL hyperbolic_diag(user_Drhs_diag, ng)
    END IF


  END FUNCTION user_Drhs_diag

  ! Local function to define the mask for the hyperbolic viscosity
  !FUNCTION set_hyper_mask( nlocal )
  !  IMPLICIT NONE
  !  INTEGER, INTENT(IN) :: nlocal
  !  REAL (pr), DIMENSION(nlocal) :: set_hyper_mask
  !
  ! WHERE( ABS( x(:,2) ) .GT. 0.95_pr )  ! The boundary regions
  !    set_hyper_mask = 0.0_pr 
  ! ELSEWHERE
  !    set_hyper_mask = 1.0_pr 
  ! END WHERE
  !
  !END FUNCTION set_hyper_mask



  SUBROUTINE user_project (u, p, nlocal, meth)
    USE variable_mapping
    IMPLICIT NONE

    INTEGER, INTENT (IN) :: meth, nlocal
    REAL (pr), DIMENSION (nlocal),     INTENT(INOUT) :: p
    REAL (pr), DIMENSION (nlocal,n_integrated), INTENT(INOUT) :: u

    INTEGER :: i
    INTEGER, PARAMETER :: ne = 1
    INTEGER, DIMENSION(ne) :: clip 
    !no projection for compressible flow
  END SUBROUTINE user_project

  FUNCTION Laplace (jlev, u, nlocal, ne_local, meth)
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: jlev, nlocal, ne_local, meth
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (INOUT) :: u
    REAL (pr), DIMENSION (nlocal*ne_local) :: Laplace

    INTEGER :: i, ii, ie, shift, meth1, meth2, idim
    REAL (pr), DIMENSION (ne_local ,nlocal,dim) :: du
    REAL (pr), DIMENSION (ne_local*dim ,nlocal,dim) :: d2u
    REAL (pr), DIMENSION (ne_local*dim ,nlocal,dim) :: du_dummy ! passed when only calculating 1st derivative.
    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc
    
    Laplace=0.0_pr
  END FUNCTION Laplace

  FUNCTION Laplace_diag (jlev,  nlocal, ne_local, meth)
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: jlev, nlocal, ne_local, meth
    REAL (pr), DIMENSION (nlocal*ne_local) :: Laplace_diag

    INTEGER :: i, ii, ie, shift, meth1, meth2
    REAL (pr), DIMENSION (nlocal,dim) :: du, d2u
    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc

    Laplace_diag=0.0_pr
  END FUNCTION Laplace_diag

  FUNCTION Laplace_rhs(u, nlocal, ne_local, meth)
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: nlocal, ne_local, meth
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (INOUT) :: u
    REAL (pr), DIMENSION (nlocal) :: Laplace_rhs

    INTEGER :: i, ii, ie, meth1, meth2, idim
    REAL (pr), DIMENSION (ne_local ,nlocal,dim) :: du
    REAL (pr), DIMENSION (ne_local*dim ,nlocal,dim) :: d2u
    REAL (pr), DIMENSION (ne_local*dim ,nlocal,dim) :: du_dummy ! passed when only calculating 1st derivative.
    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc

    Laplace_rhs = 0.0_pr 
  END FUNCTION Laplace_rhs

  !
  ! Calculate any statitics
  !
  ! startup_flag - 0 when adapting to IC,then 1 inmain integration loop
  !
  SUBROUTINE user_stats ( u ,j_mn, startup_flag)
    USE parallel
    USE penalization
    USE variable_mapping
    IMPLICIT NONE
    REAL (pr), DIMENSION (nwlt,1:n_var), INTENT (IN) :: u
    INTEGER , INTENT (IN) :: j_mn 
    INTEGER , INTENT (IN) :: startup_flag

    INTEGER :: i,n

    REAL(pr) :: num_nodes_added

    REAL(pr),DIMENSION(nwlt,2+dim) :: u_FWH

    !surface integration variables
    INTEGER, DIMENSION(1) :: int_var

    REAL(pr) :: integral
    REAL(pr), DIMENSION(n_var,nwlt,dim) :: du,temp
    REAL(pr), DIMENSION(dim,nwlt) :: h_arr
    REAL(pr) :: hhx, hhy, area_reconstruction,pi,circ_recon
    REAL(pr), DIMENSION(2) :: F_surf, F_chi

    CHARACTER (LEN=4) :: string,string2
    CHARACTER (LEN=60) :: sformat




    
  END SUBROUTINE user_stats

  SUBROUTINE user_cal_force (u, n, t_local, force, drag, lift)
    !--Calculates drag and lift on obstacle using penalization formula
    IMPLICIT NONE

    INTEGER, INTENT (IN) :: n
    REAL (pr), INTENT (IN) :: t_local
    REAL (pr), DIMENSION (dim), INTENT (INOUT) :: force
    REAL (pr), INTENT (OUT) :: drag, lift
    REAL (pr), DIMENSION (n,dim) :: u
    drag = 0.0_PR
    lift = 0.0_PR
    !
    ! There is no obstacle in flow
    !

  END SUBROUTINE user_cal_force

  !
  ! Read input from "case_name"_pde.inp file
  ! case_name is in string file_name read from command line
  ! in read_command_line_input()
  ! 
  SUBROUTINE user_read_input()
    IMPLICIT NONE
    INTEGER :: i, j, ind
    CHARACTER(LEN=8):: numstrng, procnum
    INTEGER :: myiter
    LOGICAL :: checkit 

    call input_logical ('user_smagorinsky',user_smagorinsky,'stop',' user_smagorinsky: Add a user-defined smagorinsky LES model')
    call input_real ('user_pr_turb',user_pr_turb,'stop',' user_pr_turb: Users turbulent Prandtl number' ) 
    call input_real ('user_sc_turb',user_sc_turb,'stop',' user_sc_turb: Users turbulent Schmidt number' ) 
    call input_real ('user_cs',user_cs,'stop',' user_cs: Users smagorinsky coefficient' ) 
    call input_real ('user_van_driest',user_van_driest,'stop',' user_van_driest: Van Driest number for scaling the eddy viscosity damping function' ) 

    call input_real ('Re',Re,'stop',' Re: Reynolds Number')
    call input_real ('Pra',Pra,'stop',' Pra: Prandtl Number')
    call input_real ('Sc',Sc,'stop',' Sc: Schmidt Number')
    call input_real ('Fr',Fr,'stop',' Fr: Froude Number')
    call input_real ('Ma',Ma,'stop',' Ma: Mach Number')
    call input_real ('S1',S1,'stop',' S1: Sutherland Constant S_1/T_0 (variable viscosity)')
    call input_real ('Re_tau_target',Re_tau_target,'stop',' Re_tau_target: Target Wall Reynolds Number')
    call input_integer ('Nspec',Nspec,'stop','  Nspec: Number of species')
    call input_integer ('methpd',methpd,'stop','  methpd: meth for taking pressure derivative in time')
    call input_integer ('BCver',BCver,'stop','  BCver: version for BC')
    call input_integer ('ICver',ICver,'stop','  ICver: version for IC')

    call input_logical ('use_OS_IC',use_OS_IC,'stop', 'use_OS_IC: Use the Orr-Sommerfield solution from file for the IC')

    call input_real ('mesh_cluster_param', mesh_cluster_param, 'stop',' mesh_cluster_param: Mesh clustering parameter for curvilinear stretching in boundary layers')

    call input_logical ('BC_perturb',BC_perturb,'stop', 'Boundary perturbation to aid transition')
    call input_logical ('IC_blasius',IC_blasius,'stop', 'pseudo-blasius BL IC')
    call input_logical ('perturb_forcing',perturb_forcing,'stop', 'volume forcing at unstable frequency')

    call input_integer ('hyper_it_limit',hyper_it_limit,'stop','  hyper_it_limit: number of iterations to apply the hyperbolic viscosity to smooth local instabilities.  hyper_it_limit < 0 for all iterations')
 
    filter_field = .FALSE.
    call input_logical ('filter_field',filter_field,'default', 'filter_field: pre_process stabilization of field by applying lowpass filter once')

    Nspec=MAX(Nspec,1)
  
    ALLOCATE(gr_in(1:Nspec*dim))  
    ALLOCATE(bD_in(1:Nspec))
    ALLOCATE(mu_in(1:Nspec))
    ALLOCATE(kk_in(1:Nspec))
    ALLOCATE(cp_in(1:Nspec))
    ALLOCATE(MW_in(1:Nspec))
    ALLOCATE(gamm(1:Nspec))
    ALLOCATE(pureX(1:Nspec), pureY(1:Nspec), pureR(1:Nspec), pureg(1:Nspec), puregamma(1:Nspec), YR(1:Nspec))

    call input_real ('At',At,'stop',' At: Atwood Number')
    call input_real_vector ('gamm',gamm(1:Nspec),Nspec,'stop',' gamm: cp/(cp-R)')
    call input_real ('peakchange',peakchange,'stop',' peakchange: how far offset to put peaks')
    call input_real ('Lczn',Lczn,'stop', 'Lcoordzn: length of coord_zone on one side')
    call input_real ('tfacczn',tfacczn,'stop', 'tfacczn: factor multiply by time for coord_zone time=L/min(c)*tfac, set <0 for no czn modification')
    call input_real ('itfacczn',itfacczn,'stop', 'itfacczn: factor multiply by initial time for coord_zone itime=Lczn/min(c)*tfac, set <0 to set itime=0')

    call input_real ('tempscl',tempscl,'stop', 'tempscl: scale for temperature, set < 0 for default')
    call input_real ('specscl',specscl,'stop', 'specscl: scale for species, set < 0 for default')
    call input_real ('mydt_orig',mydt_orig,'stop', 'mydt_orig: max dt for time integration... dt is only initial dt, set <0 to use dt')

    call input_logical ('GRAV',GRAV,'stop', 'GRAV: T to include body force terms, F to ingore them')
    call input_logical ('NS',NS,'stop', 'NS: T to include viscous terms (full Navier-Stokes), F for Euler Eqs')

    call input_real ('grav_coeff',grav_coeff,'stop', 'grav_coeff: coefficient for gravity forcing')
    call input_logical ('dynamic_gravity',dynamic_gravity,'stop', 'dynamic_gravity: use dynamic volume forcing to maintain some bulk flow quantity' )

    call input_logical ('adaptAt',adaptAt,'stop', 'adaptAt: T to modify At to get effectivie At = input At due to peakchange')
    call input_logical ('splitFBpress',splitFBpress,'stop', 'splitFBpress: to split derivs for pressure dp/dx FB/BB light/heavy')
    call input_logical ('stepsplit',stepsplit,'stop', 'stepsplit: T to do Heaviside split, F to use X')
    call input_logical ('adapttmp',adapttmp,'stop', 'adapttmp: T to adapt on temperature instead of density and pressure')
    call input_logical ('adaptvelonly',adaptvelonly,'stop', 'adaptvelonly: T to adapt on velocity instead of momentum')
    call input_logical ('adaptspconly',adaptspconly,'stop', 'adaptspconly: T to adapt on mass fraction (Y) instead of volume fraction (rhoY)')
    call input_logical ('adaptden',adaptden,'stop', 'adapt on density')
    call input_integer ('adaptvel',adaptvel,'stop', 'adapt on momentum: 0 - none, 1 - const scale, 2 - dynamic scale')
    call input_logical ('adapteng',adapteng,'stop', 'adapt on energy')
    call input_logical ('adaptspc',adaptspc,'stop', 'adapt on species')
    call input_logical ('adaptprs',adaptprs,'stop', 'adapt on pressure')

    call input_logical ('convcfl',convcfl,'stop', 'convcfl: T to use convective cfl, F to use acoustic cfl')
    call input_logical ('strainratecfl',strainratecfl,'stop', 'strainratecfl: T to use mod of strain rate |S| as timescale in cfl condition')
    call input_logical ('vorticitycfl', vorticitycfl,'stop', 'vorticitycfl: T to use mod of vorticity |omega| as timescale in cfl condition')
    call input_logical ('boundY',boundY,'stop', 'boundY: T to bound Y to [0,1] in user_pre/post_process')

    !4extra - these 8 input definitions - MUST ALSO ADD TO .inp 
    call input_integer ('adaptMagVort',adaptMagVort,'stop','adaptMagVort: Adapt on the magnitude of vorticity: 0 - none, 1 - const scale, 2 - dynamic scale')
    call input_logical ('adaptComVort',adaptComVort,'stop','adaptComVort: Adapt on the components of vorticity')
    call input_logical ('adaptMagVel',adaptMagVel,'stop','adaptMagVel: Adapt on the magnitude of velocity')
    call input_integer ('adaptNormS',adaptNormS,'stop', 'adaptNormS: Adapt on the L2 of Sij: 0 - none, 1 - const scale, 2 - dynamic scale')
    call input_logical ('adaptGradY',adaptGradY,'stop', 'adaptGradY: Adapt on |dY/dx_i*dY/dx_i|')
    call input_logical ('saveMagVort',saveMagVort,'stop', 'saveMagVort: if T adds and saves magnitude of vorticity')
    call input_logical ('saveComVort',saveComVort,'stop', 'saveComVort: if T adds and saves components of vorticity')
    call input_logical ('saveMagVel',saveMagVel,'stop','saveMagVel: if T adds and saves magnitude of velocity')
    call input_logical ('saveNormS',saveNormS,'stop', 'saveNormS: if T adds and saves magnitude of strain rate')
    call input_logical ('saveGradY',saveGradY,'stop', 'saveGradY: if T adds and saves |dY/dx_i*dY/dx_i|')
    call input_logical ('saveUtil',saveUtil,'stop', 'saveUtil: saveUtil: save utility variable') 
   
    IF( adaptvel .NE. 0 ) call input_real ('VelScale',VelScale,'stop', 'VelScale: scale weight for adaptation on velocity (momentum)')
    IF( adaptMagVort .NE. 0 ) call input_real ('MagVortScale',MagVortScale,'stop', 'MagVortScale: scale weight for adaptation on modulus of vorticity')
    IF( adaptNormS .NE. 0 ) call input_real ('NormSScale',NormSScale,'stop', 'NormSScale: scale weight for adaptation on modulus of strain rate')


    IF (imask_obstacle) THEN
       !Type of obstacle
       call input_logical ('brinkman_penal',brinkman_penal,'stop','Brinkman penalization to be used')
       call input_logical ('vp',vp,'stop','volume penalization to be used')
       
       !obstacle inputs for distance function
       call input_real ('xw',xw,'stop', 'wedge apex')
       call input_real ('wa',wa,'stop', 'wedge angle (degree)')
       call input_real ('xsp',xsp,'stop', 'sphere center x')
       call input_real ('ysp',ysp,'stop', 'sphere center y')
       call input_real ('rsp',rsp,'stop', 'sphere radius')
       
       call input_real ('xwall',xwall,'stop', 'wall position')
       
       ! multiple spheres parameters
       call input_integer ('Nsp',Nsp,'stop', 'Number of spheres')
       IF (.NOT. ALLOCATED(xsps)) ALLOCATE(xsps(Nsp))
       IF (.NOT. ALLOCATED(ysps)) ALLOCATE(ysps(Nsp))
       IF (.NOT. ALLOCATED(rsps)) ALLOCATE(rsps(Nsp))
       call input_real_vector ('xsps',xsps(1:Nsp),Nsp,'stop', 'sphere centers x')
       call input_real_vector ('ysps',ysps(1:Nsp),Nsp,'stop', 'sphere centers y')
       call input_real_vector ('rsps',rsps(1:Nsp),Nsp,'stop', 'spheres radii')

       smooth_dist = .FALSE. ! default value
       call input_logical ('smooth_dist',smooth_dist,'default','T smoothes distance function prior finding the normal')
    
       deltadel_loc = 1.0_pr
       call input_real ('deltadel_loc',deltadel_loc,'default', 'sets final diffusion thickness using spatial resolution (dx, dy, or dz), number of points across final thickness')


       !brinkman parameters
       call input_real ('eta_b',eta_b,'stop',' penalization parameter') !also for volume penalization
       call input_real ('eta_c',eta_c,'stop',' penalization parameter for adiabatic conditions') !also for volume penalization
       
       call input_real ('porosity',porosity,'stop',' porosity for brinkman')

       !penalization control
        call input_logical ('doCURV',doCURV,'stop', 'If true use curvature adjustment')
        call input_real ('doNORM',doNORM,'stop', 'If true add terms with normal velocity')


       !eric's support parameters
       call input_integer ('delta_pts',delta_pts,'stop', 'Number of points to transition from convection to diffusion')
       call input_integer ('IB_pts',IB_pts,'stop', 'Number of points (distance) for convection inside of the immersed boundary')
       !Nurlybek's support parameters
       call input_real ('delta_conv',delta_conv,'stop', 'transition thickness of the convective penalization zone')
       call input_real ('delta_diff',delta_diff,'stop', 'transition thickness of the diffusion penalization zone')
       call input_real ('shift_conv',shift_conv,'stop', 'thickness when convections is off')
       call input_real ('shift_diff',shift_diff,'stop', 'thickness of the diffusion is on')

       call input_real ('visc_factor_n',visc_factor_n,'stop','visc_factor_n: multiplier on numerical viscosity for penalization stability on neumann terms')
       call input_real ('visc_factor_d',visc_factor_d,'stop','visc_factor_d: multiplier on numerical viscosity for penalization stability on dirichlet terms')
    END IF
 
    IF (mydt_orig < 0.0_pr) mydt_orig = dt
    IF (IC_restart_mode==1) dt_original = mydt_orig

!!$    IF (buffNe) imask_obstacle=.TRUE.


    IF (Nspec<2) adaptspc=.FALSE. 
    IF (Nspec<2) adaptspconly=.FALSE. 
    IF (Nspec<2) adaptGradY=.FALSE. 
    IF (Nspec<2) saveGradY=.FALSE. 
    IF (Nspec<2) splitFBpress=.FALSE. 
    Nspecm = Nspec - 1


    IF (adaptAt) At=At*(1.0_pr/(1.0_pr-2.0_pr*peakchange))


    !ALIREZA: the body force is defined here
    gr_in(:) = 0.0_pr  !g's by species, then dimension
    !IF (GRAV) gr_in(1:Nspec) = -grav_coeff  !sign change for forcing in the positive x-direction
    grav_coeff_hist = grav_coeff
    Re_bulk_hist = Re
    Re_tau_hist = Re_tau_target 
    

!    MW_in(:) = 1.0_pr
    MW_in(:) = gamm(:)
!    MW_in(1) = 1.0_pr+At
    MW_in(1) = MW_in(1)+At
!    IF (Nspec>1) MW_in(2) = 1.0_pr-At
    IF (Nspec>1) MW_in(2) = MW_in(2)-At
    YR(:) = MW_in(:) / SUM(MW_in(:))
    cp_in(:) = 1.0_pr/MW_in(:)*gamm(:)/(gamm(:)-1.0_pr)/Ma**2.0_pr
    gammR = SUM(cp_in(:)*YR(:))/(SUM(cp_in(:)*YR(:))-SUM(YR(:)/MW_in(:)/Ma**2.0_pr) )
    mu_in(:) = 1.0_pr/Re
!    kk_in(:) = 1.0_pr/Re/Pra*gammR/(gammR-1.0_pr)
    kk_in(:) = 1.0_pr/Re/Pra/(gammR-1.0_pr)/Ma**2.0_pr

    bD_in(:) = 1.0_pr/Re/Sc

    gammBC(:)=gamm(1)
    IF (Nspec>1) gammBC(2)=gamm(2)

!!$    Tconst = 1.0_pr/Ma**2
!!$    Pint = 1.0_pr/Ma**2

    Tconst = 1.0_pr
    Pint = 1.0_pr

     IF (NOT(ALLOCATED(Lxyz))) ALLOCATE(Lxyz(1:dim))

     IF (NOT(ALLOCATED(Y0top))) ALLOCATE(Y0top(1:Nspec))
     IF (NOT(ALLOCATED(Y0bot))) ALLOCATE(Y0bot(1:Nspec))
     IF (NOT(ALLOCATED(rhoYtopBC))) ALLOCATE(rhoYtopBC(1:Nspecm))
     IF (NOT(ALLOCATED(rhoYbotBC))) ALLOCATE(rhoYbotBC(1:Nspecm))
     IF (NOT(ALLOCATED(rhoYtop))) ALLOCATE(rhoYtop(1:Nspecm))
     IF (NOT(ALLOCATED(rhoYbot))) ALLOCATE(rhoYbot(1:Nspecm))

     !pi=4.0_pr*ATAN(1.0_pr)
     Lxyz(1:dim)=xyzlimits(2,1:dim)-xyzlimits(1,1:dim)

    !variable thresholding
    !CALL user_read_input__VT
    !IF ( do_Adp_Eps_Spatial .OR. do_Adp_Eps_Spatial_Evol ) nu = 1.0_pr/Re   ! to be used as timescale for eps_forcing

    !CALL SGS_compressible_read_input

  END SUBROUTINE user_read_input

  !
  ! calculate any additional variables
  ! 
  ! arg 
  ! flag  - 0 calledwhile adapting to IC, 1 - called in main time integration loop
  !
  ! These additiona variables are calculated and left in real space.
  !
  SUBROUTINE user_additional_vars( t_local, flag )
    USE penalization
    USE curvilinear 
    IMPLICIT NONE
    REAL (pr), INTENT (IN) ::  t_local
    INTEGER , INTENT(IN) :: flag ! 0- called during adaption to IC, 1 called during main integration loop
    REAL (pr) :: cp, R, mygr, cmax, uval
    INTEGER :: l, i, j, myiter
    REAL (pr), DIMENSION(1,nwlt,MAX(dim,Nspecm,2)) :: du, d2u    
    REAL (pr) :: iterdiff, initdiff, curtau, fofn, deln, delnold, delfofn
    INTEGER, PARAMETER :: meth=1
    REAL (pr), DIMENSION(nwlt,dim*2) :: bigU


    REAL (pr), DIMENSION(nwlt) :: a_wall
    INTEGER :: face_type, nloc, wlt_type, k, j_df
    INTEGER :: ie, i_bnd
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim)   :: face
    INTEGER, DIMENSION(nwlt)  :: iloc
    REAL (pr) :: a_total, rho_wav, rho_m, volume_total, vel_bulk
    REAL (pr), DIMENSION(2) :: tau_wall
    INTEGER,   DIMENSION(2) :: pts_total
    REAL (pr), DIMENSION(nwlt,dim) :: v
    REAL (pr), DIMENSION(dim,nwlt,dim) :: dv, d2v
    INTEGER :: j_wall    !minimum j_level at the wall

    REAL (pr), DIMENSION(nwlt,dim*(dim+1)/2) :: s_ij
    REAL (pr), DIMENSION(nwlt)               :: smod

    LOGICAL, SAVE :: header_flag = .TRUE.

    IF(flag .EQ. 1) THEN
    !Wall averaging:

    j_wall = j_mn
    IF(additional_planes_active) j_wall = j_additional_planes

    DO i = 1,dim
       v(:,i) = u(:,nvel(i))/u(:,nden)
    END DO
    CALL Sij( v, nwlt, s_ij, smod, .TRUE.) 
    s_ij = s_ij*2.0_pr  !S_ij -> tau_ij

    !IF(MOD(it,21)==0   ) THEN
    IF(.TRUE.   ) THEN
       tau_wall(:)     = 0.0_pr
       a_total         = 0.0_pr
       pts_total(:)    = 0
       rho_wav         = 0.0_pr
       IF ( dim .EQ. 3 ) THEN
          DO j = 1, j_wall
             i_p_face(0) = 1
             DO i=1,dim
                i_p_face(i) = i_p_face(i-1)*3
             END DO
             DO face_type = 0, 3**dim - 1
                face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
                IF( face(2) == -1  ) THEN                                    !bottom wall
                   DO wlt_type = MIN(j-1,1),2**dim-1
                      DO j_df = j, j_lev
                         DO k = 1, indx_DB(j_df,wlt_type,face_type,j)%length
                            i_bnd = indx_DB(j_df,wlt_type,face_type,j)%p(k)%i

                            tau_wall(1)  = tau_wall(1) + S_ij(i_bnd,4)
                            rho_wav      = rho_wav + u(i_bnd,nden) 
                            pts_total(1) = pts_total(1) + 1
                            a_total      = a_total + h(j_wall,1)*h(j_wall,3)   !y-norm area based on j_mn    
                         END DO
                      END DO
                   END DO
                ELSE IF( face(2) == 1  ) THEN                                 !top wall
                   DO wlt_type = MIN(j-1,1),2**dim-1
                      DO j_df = j, j_lev
                         DO k = 1, indx_DB(j_df,wlt_type,face_type,j)%length
                            i_bnd = indx_DB(j_df,wlt_type,face_type,j)%p(k)%i

                            tau_wall(2)  = tau_wall(2) + S_ij(i_bnd,4)
                            rho_wav      = rho_wav + u(i_bnd,nden) 
                            pts_total(2) = pts_total(2) + 1
                            a_total      = a_total + h(j_wall,1)*h(j_wall,3)   !y-norm area based on j_mn    
                         END DO
                      END DO
                   END DO
                END IF
             END DO
          END DO
       END IF
       !Bulk averaged values
       rho_m        = SUM(u(:,nden)*dA)
       volume_total = SUM(dA)
       !vel_bulk     = SUM(SQRT(SUM( u(:,nvel(1:dim))**2.0_pr, DIM=2)/u(:,nden)**2.0_pr)*dA)
       vel_bulk     =  SUM(u(:,nvel(1))/u(:,nden)*dA) 
   
       !PRINT *, 'summing on proc', par_rank
       !sum across processors
       CALL parallel_vector_sum(REAL    = tau_wall(1:2), LENGTH = 2  )
       CALL parallel_global_sum(REAL    = rho_wav                    )   
       CALL parallel_global_sum(REAL    = rho_m                      )   
       CALL parallel_global_sum(REAL    = volume_total               )   
       CALL parallel_global_sum(REAL    = vel_bulk                   )   
       CALL parallel_vector_sum(INTEGER = pts_total(1:2), LENGTH = 2 )  !equal sized area samples
       CALL parallel_global_sum(REAL    = a_total                    )    !area_check

       !SANITY CHECK
       IF(pts_total(1) .NE. pts_total(2) .AND. par_rank ==0) THEN
          PRINT *, 'WARNING: Total points on top and bottom walls do not agree', pts_total(1), pts_total(2)
       END IF

       !Wall averaging
       tau_wall(1:2) = tau_wall(1:2)/REAL(pts_total(1:2),pr)
       rho_wav       = rho_wav/REAL( SUM(pts_total)  ,pr) 
       Re_tau        = SQRT(rho_wav * (tau_wall(1)-tau_wall(2))/2.0_pr * Re)

       !Bulk Averaging
       rho_m    = rho_m/volume_total
       vel_bulk = vel_bulk/volume_total

 
        !Temporal averaging
       !wall
       Re_tau_hist(MOD(it,20)+1)     = Re_tau
       !bulk
       Re_bulk_hist(MOD(it,20)+1)    = vel_bulk*Re 
       Re_bulk                        = vel_bulk*Re 
       !grav_coeff_hist(MOD(it,20)+1) = !Re_tau_target**2.0_pr/Re**2.0_pr/rho_m/rho_wav !with target of Re_tau = 200
       !grav_coeff_hist(MOD(it,20)+1) = !grav_coeff_hist(MOD(it,20)+1)*(5.0_pr*(Re_tau_target-Re_tau)/Re_tau_target + 1.2_pr) !adaptive forcing to hasten convergence
       !IF(Re_bulk .GT. 2.0_pr*Re) grav_coeff_hist(MOD(it,20)+1) = grav_coeff/2.0_pr  !upper limiter to avoid high Re during transition
       !grav_coeff = SUM(grav_coeff_hist)/20.0_pr
       
       !grav_coeff = grav_coeff + 0.01_pr*grav_coeff*(Re_tau_target-Re_tau)/Re_tau_target

       IF ( dynamic_gravity ) THEN
          grav_coeff = grav_coeff*(1.0_pr + (1.0 - vel_bulk)*0.03_pr)  !0.01 means it will take 100 steps to adjust F by % delta in target velocity
          grav_coeff = MIN(0.02_pr,MAX(grav_coeff,0.0_pr)) !limiting in case timescale of grav_coeff is too much faster than vel_bulk

          grav_coeff_hist(MOD(it,20)+1) = grav_coeff 
       END IF
  
         IF (GRAV) gr_in(1:Nspec) = -grav_coeff  !sign change for forcing in the positive x-direction
         !IF (GRAV) gr_in(1+Nspec:Nspec*2) = 0.05_pr  !laterla forcing to re-energize the boundary layer

         IF(par_rank == 0) THEN 
             PRINT *, '*********************************************************'
             PRINT *, 'WALL PTS (TOP)', pts_total(2),               'WALL AREA       ', a_total
            ! PRINT *, 'DEN WAV       ', rho_wav,                    'TAU WAV         ', (tau_wall(2)-tau_wall(1))/2.0_pr 
            ! PRINT *, 'TAU WAV(1)    ', tau_wall(1),                'TAU WAV (2)     ', tau_wall(2) 
             PRINT *, 'VOLUME        ', volume_total,               'RHO_M           ', rho_m
             PRINT *, 'Re_m (tmp avg)', SUM(Re_bulk_hist)/20.0_pr, 'Re_tau (tmp avg)', SUM(Re_tau_hist)/20.0_pr
             PRINT *, 'Re_m          ', Re_bulk,                    'Re_tau          ', Re_tau
             PRINT *, 'Grav_coeff    ', grav_coeff,                 'Bulk Vel        ', vel_bulk 
             PRINT *,'**********************************************************'
            
             IF(header_flag) THEN
                OPEN(UNIT=431, FILE = TRIM(res_path)//TRIM(file_gen)//'_wall.stat', STATUS = 'UNKNOWN', form="FORMATTED", POSITION="APPEND")
                WRITE(431,  ADVANCE='YES', FMT='( a )' ) &
                  '%t      Re_bulk             Re_tau           vel_bulk'
                header_flag = .FALSE.
             END IF 

             OPEN(UNIT=431, FILE = TRIM(res_path)//TRIM(file_gen)//'_wall.stat', STATUS = 'OLD', form="FORMATTED", POSITION="APPEND")
             WRITE(431,'(4(E20.12))') t, Re_bulk, Re_tau, vel_bulk 
          END IF
       END IF
       Re_bulk    = SUM(Re_bulk_hist   )/20.0_pr
       Re_tau     = SUM(Re_tau_hist    )/20.0_pr
    END IF


    d2u(1,:,2) = cp_in(Nspec) 
    d2u(1,:,1) = 1.0_pr/MW_in(Nspec)/Ma**2.0_pr 
    IF (Nspec>1) d2u(1,:,2) = (1.0_pr - SUM(u(:,nspc(1):nspc(Nspecm)),DIM=2)/u(:,nden))*cp_in(Nspec)
    IF (Nspec>1) d2u(1,:,1) = (1.0_pr - SUM(u(:,nspc(1):nspc(Nspecm)),DIM=2)/u(:,nden))/MW_in(Nspec)/Ma**2.0_pr
    DO l=1,Nspecm
        d2u(1,:,2) = d2u(1,:,2) + u(:,nspc(l))/u(:,nden)*cp_in(l) !cp
        d2u(1,:,1) = d2u(1,:,1) + u(:,nspc(l))/u(:,nden)/MW_in(l)/Ma**2.0_pr !R
    END DO

    u(:,nprs) = ( u(:,neng)-0.5_pr*SUM(u(:,nvel(1):nvel(dim))**2,DIM=2)/u(:,nden))/(d2u(1,:,2)/d2u(1,:,1)-1.0_pr)  ! p
    IF (adapttmp) u(:,nton) = u(:,nprs)/u(:,nden)/d2u(1,:,1)
    IF (adaptvelonly) THEN
       DO i=1,dim
          u(:,nvon(i)) = u(:,nvel(i))/u(:,nden)
       END DO
    END IF
    IF (adaptspconly) THEN
       DO i=1,Nspecm
          u(:,nson(i)) = u(:,nspc(i))/u(:,nden)
       END DO
    END IF
    IF( adaptMagVort .NE. 0 .OR. saveMagVort    &
            .OR. adaptComVort .OR. saveComVort  &
            .OR. adaptMagVel .OR. saveMagVel    &
            .OR. adaptNormS .NE. 0 .OR. saveNormS ) THEN
       DO i=1,dim
          du(1,:,i) = u(:,nvel(i))/u(:,nden)
       END DO
       CALL bonusvars_vel(du(1,:,1:dim))  !4extra - call it
    END IF
    IF( adaptGradY .OR. saveGradY) THEN
       DO i=1,Nspecm
          du(1,:,i) = u(:,nspc(i))/u(:,nden)
       END DO
       CALL bonusvars_spc(du(1,:,1:Nspecm))  !4extra - call it
    END IF

       IF (modczn) THEN
          IF (t>tczn) THEN
             modczn=.FALSE.
             xyzzone(1,1) = czn_min
             xyzzone(2,1) = czn_max            
            IF (par_rank.EQ.0 .AND. verb_level>0) THEN
             PRINT *, '*****************************************************'
             PRINT *, 'FINALIZING TEMPORARY COORD_ZONE'
             PRINT *, 'COORD_ZONE OUTSIDE OF x=[',xyzzone(1,1),',',xyzzone(2,1),']'
             PRINT *, 'TIME: ', t
             PRINT *, 'STOPPING TIME FOR COORD_ZONE: ', tczn
             PRINT *, '*****************************************************'
            END IF
          END IF
       ELSE  
          IF (tfacczn>0.0_pr .AND. itfacczn>0.0_pr .AND. t>itczn) THEN
             itczn=t_end*10.0_pr
             czn_min = xyzzone(1,1)
             czn_max = xyzzone(2,1)
             xyzzone(1,1) = -Lczn
             xyzzone(2,1) =  Lczn
             cczn = MINVAL(SQRT(gamm/MW_in*Tconst))
             tczn = MAXVAL(ABS(xyzlimits(:,1)))/cczn*tfacczn
            IF (par_rank.EQ.0 .AND. verb_level>0) THEN
             PRINT *, '*****************************************************'
             PRINT *, 'APPLYING TEMPORARY COORD_ZONE'
             PRINT *, 'COORD_ZONE OUTSIDE OF x=[',-Lczn,',',Lczn,']'
             PRINT *, 'MINIMUM WAVE SPEED: ', cczn
             PRINT *, 'MAXIMUM DISTANCE: ', MAXVAL(ABS(xyzlimits(:,1)))
             PRINT *, 'TIME: ', t
             PRINT *, 'STARTING TIME FOR COORD_ZONE: ', itczn
             PRINT *, 'TIME SAFETY FACTOR: ', tfacczn
             PRINT *, 'STOPPING TIME FOR COORD_ZONE: ', tczn
             PRINT *, '*****************************************************'
            END IF
             IF (t<tczn) THEN
                modczn = .TRUE.
             ELSE
                IF (par_rank.EQ.0) PRINT *, '!!!TOO LATE TO START COORD_ZONE: t>tczn!!!'
             END IF
          END IF  
       END IF




       IF (imask_obstacle) u(:,nmsk) = penal
  END SUBROUTINE user_additional_vars

  SUBROUTINE bonusvars_vel(velvec)   !4extra - this whole subroutine
    IMPLICIT NONE
    REAL (pr), INTENT(IN) :: velvec(nwlt,dim) !velocity
    REAL (pr) :: tmp(nwlt,2*dim) !tmp for vorticity
    INTEGER :: i

    IF( adaptMagVort .NE. 0 .OR. saveMagVort    &
        .OR. adaptComVort .OR. saveComVort      &
        .OR. adaptMagVel .OR. saveMagVel        &
        .OR. adaptNormS .NE. 0 .OR. saveNormS) THEN
       tmp = 0.0_pr
       ! Calculate the magnitude vorticity
       IF( adaptMagVort .NE. 0 .OR. saveMagVort &
                .OR. adaptComVort .OR.  saveComVort ) THEN
          CALL cal_vort (velvec, tmp(:,1:3-MOD(dim,3)), nwlt)
          IF ( adaptMagVort .NE. 0 .OR. saveMagVort )  u(:,nmvt) = SQRT(tmp(:,1)**2.0_pr + tmp(:,2)**2.0_pr + tmp(:,3)**2.0_pr )
          IF ( adaptComVort .OR. saveComVort )  THEN
             DO i=1,3
                u(:,ncvt(4-i)) = tmp(:,4-i)
             END DO
          END IF
       END IF
       ! Calculate the magnitude of Sij (sqrt(SijSij) )
       ! s(:,1) = s_11
       ! s(:,2) = s_22
       ! s(:,3) = s_33
       ! s(:,4) = s_12
       ! s(:,5) = s_13
       ! s(:,6) = s_23
       tmp = 0.0_pr
       IF( adaptNormS .NE. 0 .OR. saveNormS) THEN
          CALL Sij( velvec ,nwlt, tmp(:,1:2*dim), u(:,nnms), .TRUE. )
       END IF
       ! Calculate the magnitude of velocity
       IF( adaptMagVel .OR. saveMagVel) THEN
          u(:,nmvl) = 0.5_pr*(SUM(velvec(:,:)**2.0_pr,DIM=2))
       END IF
    END IF
  END SUBROUTINE bonusvars_vel

  SUBROUTINE bonusvars_spc(spcvec)   !4extra - this whole subroutine
    IMPLICIT NONE
    REAL (pr), INTENT(IN) :: spcvec(nwlt,Nspecm) !species
    REAL (pr), DIMENSION(Nspecm,nwlt,dim) :: du, d2u    
    INTEGER :: i
    INTEGER, PARAMETER :: meth=1

    IF( adaptGradY .OR. saveGradY) THEN
       CALL c_diff_fast(spcvec(:,1:Nspecm), du(1:Nspecm,:,:), d2u(1:Nspecm,:,:), j_lev, nwlt,meth, 10, Nspecm, 1, Nspecm)  !dY/dx
       DO i=1,Nspecm
          u(:,ngdy(i)) = SUM(du(i,:,:)**2,DIM=2) !|dY/dx_i*dY/dx_i|
       END DO
    END IF
  END SUBROUTINE bonusvars_spc

  !
  ! calculate any additional scalar variables
  ! 
  ! arg 
  ! flag  - 0 calledwhile adapting to IC, 1 - called in main time integration loop
  !
  SUBROUTINE user_scalar_vars( flag )
    IMPLICIT NONE
    INTEGER , INTENT(IN) :: flag ! 0- called during adaption to IC, 1 called during main integration loop

  END SUBROUTINE user_scalar_vars

  !
  !************ Calculating Scales ***************************
  !
  ! Note the order of the components in the scl array
  ! correspond to u_tn, v_tn, w_tn, u_tn-1, v_tn-1, w_tn-1, u_tn-2, v_tn-2, w_tn-2
  ! 
  SUBROUTINE user_scales(flag,use_default, u_loc, nlocal, ne_local, l_n_var_adapt , l_n_var_adapt_index, &
       scl, scl_fltwt) !add
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: flag ! 0 during initial adaptation, then 1 during time advancement
    LOGICAL , INTENT(INOUT) :: use_default
    INTEGER, INTENT (IN) :: nlocal, ne_local
    REAL (pr), DIMENSION (1:nlocal,1:ne_local), INTENT (IN) :: u_loc
    LOGICAL , INTENT (IN) :: l_n_var_adapt(ne_local)
    INTEGER , INTENT (IN) :: l_n_var_adapt_index(1:ne_local)
    REAL (pr), DIMENSION (1:ne_local), INTENT (INOUT) :: scl
    REAL (pr), DIMENSION (1:ne_local) :: mean
    REAL (pr) ,  INTENT(IN) ::scl_fltwt !weight for temporal filter on scl
    REAL (pr) :: floor, tmp,tmpmax,tmpmin,tmpsum
    INTEGER :: i, ie, ie_index, tmpint
    REAL (pr) ,save, ALLOCATABLE, DIMENSION (:) :: scl_old
    LOGICAL  ,save :: startup_init = .TRUE.

    !
    ! Ignore the output of this routine and use default scales routine
    !
    use_default = .FALSE. 

    IF( .NOT. use_default ) THEN
       !IF (par_rank.EQ.0) PRINT *,'Use user_scales() <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<'
       !
       ! ALLOCATE scl_old if it was not done previously in user_scales
       !
       IF( startup_init .AND. .NOT. ALLOCATED(scl_old) ) THEN
          ALLOCATE( scl_old(1:ne_local) )
          scl_old = 0.0_pr
       END IF

       floor = 1.0e-12_pr
       scl   =1.0_pr
       !
       ! Calculate scl per component
       !
       mean = 0.0_pr
       IF(.TRUE.) THEN
          DO ie=1,ne_local
             IF(l_n_var_adapt(ie)) THEN
                ie_index = l_n_var_adapt_index(ie)
                IF( Scale_Meth == 1 ) THEN ! Use Linf scale
                   tmpmax=MAXVAL ( u_loc(1:nlocal,ie_index) )
                   tmpmin=MINVAL ( u_loc(1:nlocal,ie_index) )
                   CALL parallel_global_sum(REALMAXVAL=tmpmax)
                   CALL parallel_global_sum(REALMINVAL=tmpmin)
                   mean(ie)= 0.5_pr*(tmpmax+tmpmin)
                ELSE IF ( Scale_Meth == 2 ) THEN ! Use L2 scale
                   tmpsum=SUM( u_loc(1:nlocal,ie_index) )
                   tmpint=nlocal
                   CALL parallel_global_sum(REAL=tmpsum)
                   CALL parallel_global_sum(INTEGER=tmpint)
                   mean(ie)= tmpsum/REAL(tmpint,pr)
                ELSE IF ( Scale_Meth == 3 ) THEN ! Use L2 scale using dA
                   tmpsum=SUM( u_loc(1:nlocal,ie_index)*dA )
                   CALL parallel_global_sum(REAL=tmpsum)
                   mean(ie)= tmpsum/ sumdA_global 
                END IF
             END IF
          END DO
       END IF
       DO ie=1,ne_local
          IF(l_n_var_adapt(ie)) THEN
             ie_index = l_n_var_adapt_index(ie)
             IF( Scale_Meth == 1 ) THEN ! Use Linf scale
                scl(ie)= MAXVAL ( ABS( u_loc(1:nlocal,ie_index) - mean(ie) ) )
                CALL parallel_global_sum( REALMAXVAL=scl(ie) )
             ELSE IF ( Scale_Meth == 2 ) THEN ! Use L2 scale
                scl(ie)= SUM( (u_loc(1:nlocal,ie_index)-mean(ie))**2 )
                tmpint=nlocal
                CALL parallel_global_sum( REAL=scl(ie) )
                CALL parallel_global_sum(INTEGER=tmpint)
                scl(ie)=SQRT(scl(ie)/REAL(tmpint,pr))
             ELSE IF ( Scale_Meth == 3 ) THEN ! Use L2 scale using dA
                scl(ie)= SUM( (u_loc(1:nlocal,ie_index)-mean(ie))**2*dA )
                CALL parallel_global_sum(REAL=scl(ie))
                scl(ie)=SQRT(scl(ie)/sumdA_global)
             ELSE
                PRINT *, 'Error in Scales(), Unknown Scale_Meth type: ', Scale_Meth
                PRINT *, 'Known Scale_Meth types: 1 = Linf scale, 2 = L2 scale '
                PRINT *, 'Exiting ...'
                stop
             END IF
             
        !     IF (par_rank.EQ.0) THEN
        !        WRITE (6,'("Scaling before taking vector(1:dim) magnitude")')
        !        WRITE (6,'("Scale factor eqn", I4 ," = ", es11.4 ," scalecoeff = ",es11.4 )') &
        !             ie, scl(ie), scaleCoeff(ie)
        !     END IF
          END IF
       END DO
       !
       ! take appropriate vector norm over scl(1:dim)
       IF (adaptvelonly) THEN
          IF( Scale_Meth == 1 ) THEN ! Use Linf scale
             scl(nvon(1):nvon(dim)) = MAXVAL(scl(nvon(1):nvon(dim)))
          ELSE IF ( Scale_Meth == 2 ) THEN ! Use L2 scale
             scl(nvon(1):nvon(dim)) = SQRT(SUM(scl(nvon(1):nvon(dim))**2))
          ELSE IF ( Scale_Meth == 3 ) THEN ! Use L2 scale using dA
             scl(nvon(1):nvon(dim)) = SQRT(SUM(scl(nvon(1):nvon(dim))**2))
          END IF
       END IF
       IF( Scale_Meth == 1 ) THEN ! Use Linf scale
          scl(nvel(1):nvel(dim)) = MAXVAL(scl(nvel(1):nvel(dim)))
       ELSE IF ( Scale_Meth == 2 ) THEN ! Use L2 scale
          scl(nvel(1):nvel(dim)) = SQRT(SUM(scl(nvel(1):nvel(dim))**2)) !now velocity vector length, rather than individual component
       ELSE IF ( Scale_Meth == 3 ) THEN ! Use L2 scale using dA
          scl(nvel(1):nvel(dim)) = SQRT(SUM(scl(nvel(1):nvel(dim))**2)) !now velocity vector length, rather than individual component
       END IF
       !
       ! Print out new scl
       !
       ! Constant scale override ERIC: make standard for all variables with 2
       ! controls
       IF (adapttmp .AND. tempscl>floor) scl(nton)=tempscl
       IF (adapttmp .AND. adaptvelonly .AND. ABS(tempscl)<floor) scl(nton)=scl(nvon(1))
       IF (adaptspconly .AND. specscl>0.0_pr) scl(nson(1):nson(Nspecm))=specscl

       IF ( adaptvel     .EQ. 1 ) scl( nvel(1:dim) ) = 1.0_pr 
       IF ( adaptMagVort .EQ. 1 ) scl( nmvt ) = 1.0_pr
       IF ( adaptNormS   .EQ. 1 ) scl( nnms ) = 1.0_pr

       DO ie=1,ne_local
          IF(l_n_var_adapt(ie)) THEN
             !this is done if one of the variable is exactly zero inorder not to adapt to the noise
             IF(scl(ie) .le. floor) scl(ie)=1.0_pr 
             tmp = scl(ie)
             ! temporally filter scl
             IF(.not. startup_init ) scl(ie) = scl_fltwt * scl_old(ie) + (1.0_pr - scl_fltwt) * scl(ie)
             scl_old(ie) = scl(ie) !save scl for this time step
             scl(ie) = scaleCoeff(ie) * scl(ie)
             IF (par_rank.EQ.0 .AND. verb_level .GT. 0) THEN
                WRITE (6,'("Scale factor eqn", I4 ," = ", es11.4 ," scalecoeff = ",es11.4 )') &
                     ie, scl(ie), scaleCoeff(ie)
                WRITE (6,'("(before filtering = ",es11.4,")" )') tmp
             END IF
          END IF
       END DO
       
       startup_init = .FALSE.
    END IF

  END SUBROUTINE user_scales

  SUBROUTINE user_cal_cfl (use_default, ulocal, cfl_out)
    USE precision
    USE sizes
    USE pde
    USE curvilinear
    USE curvilinear_mesh
    USE variable_mapping 
    IMPLICIT NONE
    LOGICAL , INTENT(INOUT) :: use_default
    REAL (pr),                                INTENT (INOUT) :: cfl_out
    REAL (pr), DIMENSION (nwlt,n_integrated), INTENT (IN)    :: ulocal
    INTEGER                    :: i, imap, idim
    REAL (pr)                  :: flor
    REAL (pr), DIMENSION(nwlt,dim) :: cfl, cfl_conv, cfl_acou, visc_timescale
    REAL (pr), DIMENSION(nwlt,dim) :: v
    REAL (pr), DIMENSION(nwlt) :: visc_local
    REAL (pr), DIMENSION(nwlt,dim*(dim+1)/2) :: S_ij 
    REAL (pr), DIMENSION(nwlt)     :: Smod 
    REAL (pr), DIMENSION(dim,nwlt) :: h_arr
    REAL (pr)                  :: min_h
    REAL (pr), DIMENSION(nwlt) :: gam, sos
    INTEGER :: l, mynwlt_global, fullnwlt, maxnwlt, minnwlt, avgnwlt
    REAL (pr) :: min_w, max_w
    REAL (pr) :: viscStability, inv_vortex_timescale, cfl_vortex

    IF (it>0) dt_original = mydt_orig

    use_default = .FALSE.

    flor = 1e-12_pr
    cfl_out = flor

    CALL get_all_local_h (h_arr)
    ! Convert to computational coordinate vectors
    v(:,1:dim) = transform_vector_to_comp( ulocal(:,nvel(1:dim)) )


    gam(:) = cp_in(Nspec)
    sos(:) = 1.0_pr/MW_in(Nspec)/Ma**2.0_pr 
    IF (Nspec>1) gam(:) = (1.0_pr-SUM(ulocal(:,nspc(1):nspc(Nspecm)),DIM=2)/ulocal(:,nden))*cp_in(Nspec) !cp_Nspec
    IF (Nspec>1) sos(:) = (1.0_pr-SUM(ulocal(:,nspc(1):nspc(Nspecm)),DIM=2)/ulocal(:,nden))/MW_in(Nspec)/Ma**2.0_pr !R_Nspec
    DO l=1,Nspecm
       gam(:) = gam(:) + ulocal(:,nspc(l))/ulocal(:,nden)*cp_in(l) !cp
       sos(:) = sos(:) + ulocal(:,nspc(l))/ulocal(:,nden)/MW_in(l)/Ma**2.0_pr !R
    END DO
    gam(:) = gam(:)/(gam(:)-sos(:))
    sos(:) = SQRT(gam(:)*(gam(:)-1.0_pr)*(ulocal(:,neng)-0.5_pr*SUM(ulocal(:,nvel(1):nvel(dim))**2,DIM=2)/ulocal(:,nden))/ulocal(:,nden))! spd of snd = sqrt(gamma p/rho)
    maxsos = MAXVAL(sos(:))
    CALL parallel_global_sum(REALMAXVAL=maxsos)


!    maxMa = MAXVAL(    SQRT(SUM(ulocal(:,nvel(1):nvel(dim))**2,DIM=2)/ulocal(:,nden)**2)/sos(:)* SQRT(gam(:))     )
    maxMa = MAXVAL(    SQRT(SUM(ulocal(:,nvel(1):nvel(dim))**2,DIM=2)/ulocal(:,nden)**2)/sos(:))  ! Rectilinear

    CALL parallel_global_sum(REALMAXVAL=maxMa)
    IF ( ANY( transform_dir(1:dim) .EQ. 1 ) ) THEN
       DO i=1,dim
          cfl_acou(:,i) = ( sos(:) * SQRT( SUM(curvilinear_jacobian(i,1:dim,:)**2, DIM=1 ) ) ) * dt/h_arr(i,:)
       END DO
    ELSE
       DO i=1,dim
          cfl_acou(:,i) = sos(:) * dt/h_arr(i,:)
       END DO
    END IF


    DO i=1,dim
       cfl_conv(:,i) = ( ABS(v(:,i)/ulocal(:,nden)) ) * dt/h_arr(i,:)
       !cfl_acou(:,i) = ( sos(:) * SQRT( SUM( curvilinear_jacobian(i,1:dim,:)**2, DIM=1 ) ) ) * dt/h_arr(i,:)
       cfl(:,i)      = cfl_conv(:,i) + cfl_acou(:,i) 
       !cfl(:,i)      = ( ABS(v(:,i)/ulocal(:,nden))+sos(:) ) * dt/h_arr(i,:)
    END DO
    CFLreport = MAXVAL(cfl(:,:))
    CFLconv   = MAXVAL(cfl_conv(:,:))
    CFLacou   = MAXVAL(cfl_acou(:,:))
    CALL parallel_global_sum(REALMAXVAL=CFLreport)
    CALL parallel_global_sum(REALMAXVAL=CFLconv)
    CALL parallel_global_sum(REALMAXVAL=CFLacou)
    min_h = MINVAL(h(1,1:dim)/2**(j_mx - 1))
    CALL parallel_global_sum(REALMINVAL=min_h)
    mynwlt_global = nwlt
    fullnwlt = PRODUCT(mxyz(1:dim)*2**(j_lev-1) + 1 - prd(:))
    CALL parallel_global_sum (INTEGER=mynwlt_global)
    avgnwlt = mynwlt_global/par_size 
    maxnwlt = nwlt
    minnwlt = -nwlt
    CALL parallel_global_sum(INTEGERMAXVAL=maxnwlt)
    CALL parallel_global_sum(INTEGERMAXVAL=minnwlt)
    minnwlt=-minnwlt
    MAreport = maxMa
    IF (convcfl) THEN  !use acoustic cfl for initial states when acoustics dominate
       cfl_out = MAX(cfl_out, CFLconv)
    ELSE
       cfl_out = MAX(cfl_out, CFLreport)
    END IF

    ! Von Neumann stability number
    viscStability = 0.0_pr
    !IF(time_integration_method .NE. 1 .AND. time_integration_method .NE. 2) cfl_out = MAX(cfl_out,dt/Re/(min_h**2.0_pr)) !viscous length/time scale
    IF ( time_integration_method .EQ. TIME_INT_RK ) THEN
       visc_local = 1.0_pr / Re
       IF ( user_smagorinsky ) visc_local(:) = visc_local(:) + user_mu_turb(:)

       IF ( ANY( transform_dir(1:dim) .EQ. 1 ) ) THEN
          DO i=1,dim
             visc_timescale(:,i) = ( SUM(curvilinear_jacobian(i,1:dim,:)**2, DIM=1 ) ) * dt * visc_local(:) / ( h_arr(i,:) * h_arr(i,:) ) !viscous length/time scale
             !visc_timescale(:,i) = SQRT( SUM(curvilinear_jacobian(i,1:dim,:)**2, DIM=1 ) ) * dt / Re / ( h_arr(i,:) * h_arr(i,:) ) !viscous length/time scale
          END DO
       ELSE
          DO i=1,dim
             visc_timescale(:,i) = dt * visc_local(:) / ( h_arr(i,:) * h_arr(i,:) ) !viscous length/time scale
             !visc_timescale(:,i) = SQRT( SUM(curvilinear_jacobian(i,1:dim,:)**2, DIM=1 ) ) * dt / Re / ( h_arr(i,:) * h_arr(i,:) ) !viscous length/time scale
          END DO
       END IF
       viscStability = MAXVAL( visc_timescale(1:nwlt,1:dim) )
       CALL parallel_global_sum( REALMAXVAL=viscStability )
       cfl_out = MAX( viscStability, cfl_out )
    END IF

    ! Strain rate timescale
    cfl_vortex = 1.0e-12_pr 
    IF( strainrateCFL .OR. vorticityCFL ) THEN
       DO idim = 1,dim
          v(:,idim) = ulocal(:,nvel(idim)) / ulocal(:,nden)
       END DO
       CALL calc_inv_vortex_timescales( v, nwlt, inv_vortex_timescale ) 
       cfl_vortex = inv_vortex_timescale * dt  

       CALL parallel_global_sum( REALMAXVAL=cfl_vortex )
       cfl_out = MAX( cfl_vortex, cfl_out )
    END IF



    IF(dim .EQ. 3) THEN
       min_w = MINVAL(ulocal(:,nvel(3))/ulocal(:,nden))
       max_w = MAXVAL(ulocal(:,nvel(3))/ulocal(:,nden))
       CALL parallel_global_sum(REALMINVAL=min_w)
       CALL parallel_global_sum(REALMAXVAL=max_w)
    END IF

    IF (par_rank.EQ.0) THEN
       PRINT *, '**********************************************************'
       PRINT *, ' t = ', t 
       PRINT *, '**********************************************************'
       PRINT *, 'case:    ', file_gen(1:LEN_TRIM(file_gen)-1)
       PRINT *, 'max Ma = ', maxMa
       PRINT *, 'CFL    = ', cfl_out
       PRINT *, 'CFLconv= ', CFLconv,       'CFLacou   = ', CFLacou
       PRINT *, 'VonNeum= ', viscStability, 'CFLvortex = ', cfl_vortex
       !PRINT *, 'BL pts = ', 1.0_pr/((Re)**0.5_pr*min_h)
       !PRINT *, 'Re_tau = ', Re_tau
       !PRINT *, 'it     = ', it,             '  t       = ', t
       PRINT *, 'iwrite = ', iwrite-1,       '  dt      = ', dt 
       !PRINT *, 'j_mx   = ', j_mx,           '  dt_orig = ', dt_original
       PRINT *, 'j_mx   = ', j_mx,           '  twrite  = ', twrite
       !PRINT *, 'j_lev  = ', j_lev,          '  twrite  = ', twrite
       PRINT *, 'j_lev  = ', j_lev,          '  n%      = ', REAL(mynwlt_global,pr)/REAL(fullnwlt,pr)
       !PRINT *, 'nwlt_g = ', mynwlt_global,  '  cpu     = ', timer_val(2)
       !PRINT *, 'n_all  = ', fullnwlt,       '  n%      = ', REAL(mynwlt_global,pr)/REAL(fullnwlt,pr)
       !PRINT *, 'nwlt   = ', nwlt,           '  eps     = ', eps
       PRINT *, 'nwlt_mx= ', maxnwlt,        '  nwlt_av = ', avgnwlt
       PRINT *, 'nwlt_mn= ', minnwlt,        '  p_size  = ', par_size
       IF(dim .EQ.3)PRINT *, 'min/max w = ',min_w, max_w
       PRINT *, '**********************************************************'
       
    END IF
    maxMa = MIN(maxMa,1.0_pr)
  END SUBROUTINE user_cal_cfl

  SUBROUTINE user_init_sgs_model( )
    IMPLICIT NONE
  END SUBROUTINE user_init_sgs_model

  SUBROUTINE  user_sgs_force (u_loc, nlocal)
    USE variable_mapping
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: nlocal
    REAL (pr), DIMENSION (nlocal,n_integrated), INTENT (INOUT) :: u_loc
  END SUBROUTINE  user_sgs_force

  FUNCTION user_sound_speed (u, neq, nwlt)
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: nwlt, neq
    REAL (pr), DIMENSION (nwlt,neq), INTENT(IN) :: u
    REAL (pr), DIMENSION (nwlt) :: user_sound_speed,gam

    !user_sound_speed(:) = SQRT( gamm(1)*(gamm(1)-1.0_pr)*(u(:,neng)-0.5_pr*SUM(u(:,nvel(1:dim))**2,DIM=2)/u(:,nden))) ! pressure
    !PRINT *, "c = ", MINVAL(user_sound_speed), MAXVAL(user_sound_speed), "nden = ", nden, MINVAL(u(:, nden)), MAXVAL(u(:, nden))

    gam(:) = cp_in(Nspec)
    user_sound_speed(:) = SQRT(gam(:)*(gam(:)-1.0_pr)*(u(:,neng)-0.5_pr*SUM(u(:,nvel(1):nvel(dim))**2,DIM=2)/u(:,nden))/u(:,nden))! spd of snd = sqrt(gamma p/rho)

  END FUNCTION user_sound_speed

  SUBROUTINE  user_pre_process
    USE variable_mapping
    IMPLICIT NONE

    ! Random noise
    INTEGER :: iseedsize
    REAL (pr), DIMENSION(nwlt) :: rand_array
    INTEGER, ALLOCATABLE, DIMENSION(:) :: seed
    INTEGER, SAVE :: add_noise=.TRUE.

    REAL (pr), DIMENSION(1:nwlt,1:n_integrated) :: u_tmp 
    INTEGER :: i, l
    REAL (pr), DIMENSION(nwlt) :: mu_loc, kappa_loc, Temperature
!    REAL (pr), DIMENSION(nwlt) :: Pressure 
    REAL (pr)                  :: a0_in, cv_loc
    REAL (pr)                  :: tmp1, tmp2, tmp3
    REAL (pr)                  :: rho_m_before, rho_m_after 

    LOGICAL, SAVE :: first_skip = .TRUE.   !  Skip the first iteration - for sgs_calc_forcing control to allow restart.  Forcing needs to be called in wlt_3d_main once compressible variables and properties are set up
    LOGICAL, SAVE :: filter_field_flag = .TRUE.


    ! Set eddy viscosity
    IF ( user_smagorinsky ) CALL set_eddy_viscosity



!    IF(add_noise .AND. t<dt) THEN  ! only call during initial run to avoid wiping out solution
!    !IF(add_noise) THEN  ! only call during initial run to avoid wiping out solution
!        call RANDOM_SEED(SIZE=iseedsize)  
!        ALLOCATE(seed(iseedsize))
!        seed(:)=1234
!        call RANDOM_SEED(PUT=seed)
!        DEALLOCATE(seed)
!        !generate random coefficient
!        call RANDOM_NUMBER(rand_array)


!        IF(par_rank ==0) PRINT *, 'Random noise added to z-momentum'
!        u(:,nvel(3)) = 0.01_pr*(rand_array - 0.5_pr)
    
!        add_noise = .FALSE.
!     END IF


    !IF(par_rank ==0) PRINT *, 'user_pre_process'

    IF ( filter_field .AND. filter_field_flag ) THEN
       IF( par_rank .EQ. 0 ) PRINT *, 'FILTERING FIELD FOR STABILIZATION'
       rho_m_before        = SUM(u(:,nden)*dA)
       u_tmp(1:nwlt,1:n_integrated) = u(1:nwlt,1:n_integrated)
       CALL local_lowpass_filt( u_tmp(:,1:n_integrated), j_lev, nwlt, n_integrated, 1, n_integrated,&
             lowpassfilt_support, lowpass_filt_type, tensorial_filt)
       u(1:nwlt,1:n_integrated) = 0.75_pr*u(1:nwlt,1:n_integrated) + 0.25_pr*u_tmp(1:nwlt,1:n_integrated) ! To avoid zero coefficients
       ! maintain total mass
       rho_m_after        = SUM(u(:,nden)*dA)
       CALL parallel_global_sum(REAL    = rho_m_before   )   
       CALL parallel_global_sum(REAL    = rho_m_after    )   
       u(:,nden) = u(:,nden) * rho_m_before / rho_m_after

       filter_field_flag = .FALSE.
    END IF  
    !IF(compressible_SCALES ) CALL SGS_compressible_pre_process(u(:,1:n_integrated),n_integrated,nwlt)
    !IF(compressible_SCALES .AND. .NOT.( first_skip ) .AND. it > 0) THEN 
    !!!!!IF(compressible_SCALES ) THEN 
    !   cv_loc = (cp_in(Nspec)-1.0_pr/MW_in(Nspec)/Ma**2.0_pr)
    !   !calculate mu (dynamic) 
    !   !Pressure    = (u(:,neng)-0.5_pr*SUM(u(:,nvel(1):nvel(dim))**2,DIM=2)/u(:,nden))/( cp_in(Nspec) /(1.0_pr/MW_in(Nspec)/Ma**2.0_pr)-1.0_pr) 
    !   !Temperature = u(:,nprs)/(1.0_pr/MW_in(Nspec)/Ma**2.0_pr)/u(:,nden)  !Temperature
    !   Temperature = (u(:,neng) - 0.5_pr*SUM(u(:,nvel(1):nvel(dim))**2.0_pr,DIM=2)/u(:,nden)) /cv_loc /u(:,nden) !Temperature
    !   mu_loc      = mu_in(Nspec) * (1.0_pr+S1)/(Temperature +S1)*(Temperature)**(1.5_pr)  !mu_loc - dynamic viscosity
    !   kappa_loc   = kk_in(Nspec) * (1.0_pr+S1)/(Temperature +S1)*(Temperature)**(1.5_pr)  !kappa_loc - dynamic conductivity 
    !   !Sanity check
    !   IF (verb_level.GT.0 ) THEN
    !      tmp1 = SUM(Temperature*dA)
    !      tmp2 = SUM(mu_loc*dA)
    !      tmp3 = SUM(dA)
    !      CALL parallel_global_sum(REAL = tmp1)
    !      CALL parallel_global_sum(REAL = tmp2)
    !      CALL parallel_global_sum(REAL = tmp3)
    !      IF(par_rank .EQ. 0) PRINT *, 'usr_pre_process: preparing to call sgs_compressible_model_setup and _model_force'
    !      IF(par_rank .EQ. 0) PRINT *, 'SANITY CHECK: nondimensional, volume-averaged TEMPERATURE/MU:', tmp1/tmp3, tmp2/tmp3
    !   END IF
    !
    !   a0_in = 1.0_pr/Ma          !nondimensional based on freestream velocity      
    !   ! if it < = 0, this performs initialization
    !   CALL sgs_compressible_force ( u, nwlt, mu_loc, kappa_loc, cv_loc, a0_in, MW_in(Nspec), 1.0_pr/MW_in(Nspec)/Ma**2.0_pr )  !ERIC: double check c_v
    !END IF 

    !IF ( do_Adp_Eps_Spatial .OR. do_Adp_Eps_Spatial_Evol ) &
    !CALL user_pre_process__VT
  
    first_skip = .FALSE.
 

  END SUBROUTINE user_pre_process

  SUBROUTINE  user_post_process
    IMPLICIT NONE

    !IF(compressible_SCALES .AND. it > 0) CALL SGS_compressible_post_process(u(:,1:n_integrated),n_integrated,nwlt)

    !variable thresholding
    !IF ( do_Adp_Eps_Spatial .OR. do_Adp_Eps_Spatial_Evol ) &
    !CALL user_post_process__VT(n_var_SGSD)    ! n_var_ExtraAdpt_Der    ! n_var_Epsilon   ! n_var_RSGSD         ! Cf: OPTIONAL
    IF(hypermodel .NE. 0 .AND. ( hyper_it .LT. hyper_it_limit ) ) hyper_it = hyper_it + 1 


  END SUBROUTINE user_post_process

  FUNCTION initial_velocity_spectra( nloc, k0, direction, RNDM_OFFSET_SEED ) 
    IMPLICIT NONE
    INTEGER,                      INTENT(IN) :: k0, direction, nloc
    INTEGER, OPTIONAL,            INTENT(IN) :: RNDM_OFFSET_SEED 
    REAL (pr), DIMENSION(nloc)               :: initial_velocity_spectra

    INTEGER :: k      ! Iterators
    INTEGER :: nyquist      ! nyquist wavenumber 

    REAL (pr)                           :: offset, spectra
    INTEGER                             :: iseedsize
    INTEGER, ALLOCATABLE, DIMENSION(:)  :: seed
    
    REAL (pr), PARAMETER :: PI = 3.14159265358979323846    

    IF( direction < 1 .OR. direction > dim ) THEN
      PRINT *, 'Cannot compute spectra for invalid direction', direction, 'of', dim
      STOP
    END IF
    
    IF( PRESENT(RNDM_OFFSET_SEED)) THEN 
      CALL RANDOM_SEED(SIZE=iseedsize)  
      ALLOCATE(seed(iseedsize))
      seed(:) = RNDM_OFFSET_SEED
      CALL RANDOM_SEED(PUT=seed)
      DEALLOCATE(seed)
    END IF

    initial_velocity_spectra = 0.0_pr

    offset = 0.0_pr
    nyquist = (mxyz(direction)*2**(j_mx - 1) + 1 - prd(direction))/2
    DO k = 0, nyquist/4
      ! Energy spectra
      spectra = ( REAL(k,pr)/REAL(k0,pr) )**4 * EXP( -2.0_pr *  ( REAL(k,pr)/REAL(k0,pr) )**2 )
      IF( PRESENT(RNDM_OFFSET_SEED)) call RANDOM_NUMBER(offset)
      !IF(par_rank .EQ. 0)PRINT *, 'spectra with offset', spectra, offset
      initial_velocity_spectra = initial_velocity_spectra + &
        spectra * SIN( 2.0_pr * PI * REAL(k,pr) * ( x(:,direction) / (xyzlimits(2,direction) - xyzlimits(1,direction) )+ offset + ( 0.25_pr - 0.25_pr*REAL(MOD(k,2),pr) )   ) )
    END DO



  END FUNCTION initial_velocity_spectra


  !! ADD_SMOOTH_NOISE:  Adds gaussian perturbations at random locations to the
  !given field
  ! field       - field to add noise to
  ! nloc        - number of points in the field
  ! number_points - number of noise perturbations to add
  ! scale       - height of the perturbations
  ! seed_input  - random seed for reproducability across initial adaptation cycles
  ! OPTIONAL ARGUMENTS:
  ! DEFINE_X    - specify perturbation locations on the x plane
  ! DEFINE_Y    - specify perturbation locations on the y plane
  ! DEFINE_Z    - specify perturbation locations on the z plane
  SUBROUTINE add_smooth_noise( field, nloc, number_points, scale, seed_input, DEFINE_X, DEFINE_Y, DEFINE_Z )
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: nloc, number_points, seed_input
    REAL (pr), DIMENSION(nloc), INTENT(INOUT) :: field
    REAL (pr), INTENT (IN) :: scale
    REAL (pr), OPTIONAL, INTENT (IN) :: DEFINE_X, DEFINE_Y, DEFINE_Z

    INTEGER :: iseedsize
    REAL (pr), DIMENSION(nwlt) :: rand_array
    INTEGER, ALLOCATABLE, DIMENSION(:) :: seed
    REAL (pr), DIMENSION(3) :: x_loc

    INTEGER :: ipoint, idim, iloc



    CALL RANDOM_SEED(SIZE=iseedsize)  
    
    ALLOCATE(seed(iseedsize))
    seed(:) = seed_input 
    CALL RANDOM_SEED(PUT=seed)
    DEALLOCATE(seed)

       
    DO ipoint = 1,number_points
      ! generate random point
      call RANDOM_NUMBER(x_loc)
      IF( par_rank .EQ. 0 ) THEN 
        x_loc(1:dim) = x_loc(1:dim) * (xyzlimits(2,1:dim) - xyzlimits(1,1:dim)) + xyzlimits(1,1:dim)
        !PRINT *, 'NOISE:', par_rank, INT( 1.0_pr - 2.0_pr * MODULO(ipoint, 2 )) * ipoint, x_loc(1), x_loc(2), x_loc(3)    
      ELSE
        x_loc = 0.0_pr
      END IF  
      CALL parallel_vector_sum( REAL=x_loc, LENGTH=3)
  
      IF( PRESENT(DEFINE_X) ) x_loc(1) = DEFINE_X
      IF( PRESENT(DEFINE_Y) ) x_loc(2) = DEFINE_Y
      IF( PRESENT(DEFINE_Z) ) x_loc(3) = DEFINE_Z

      ! Scale and generate point
      DO iloc = 1,nloc
        field(iloc) = field(iloc) + ( 1.0_pr - 2.0_pr * MODULO(ipoint, 2 )) * scale * EXP( - (SUM( ( x(iloc,1:dim) - x_loc(1:dim))**2 ))/ ( 2.0_pr*MINVAL(h(1,1:dim)/2.0_pr**(j_mx - 1)))   )
      END DO
    END DO

  END SUBROUTINE add_smooth_noise

  SUBROUTINE calc_total_KE( momentum, density, total_KE, nloc )
    IMPLICIT NONE
    INTEGER,                        INTENT(IN)  :: nloc
    REAL (pr), DIMENSION(nloc,dim), INTENT(IN)  :: momentum
    REAL (pr), DIMENSION(nloc),     INTENT(IN)  :: density 
    REAL (pr),                      INTENT(OUT) :: total_KE 

    INTEGER   :: idim
    total_KE = SUM(  0.5_pr * momentum(:,1) * momentum(:,1) * dA(:) / density(:) )
    DO idim = 2,dim
      total_KE = total_KE + SUM(  0.5_pr * momentum(:,idim) * momentum(:,idim) * dA(:) / density(:) )
    END DO

    CALL parallel_global_sum( REAL=total_KE )

  END SUBROUTINE calc_total_KE

  SUBROUTINE calc_inv_vortex_timescales( velocity, nlocal, inv_vortex_timescale )
    USE vector_util_mod
    IMPLICIT NONE
    INTEGER,                        INTENT(IN)  :: nlocal
    REAL (pr), DIMENSION(nlocal,dim), INTENT(IN)  :: velocity
    REAL (pr),                      INTENT(OUT) :: inv_vortex_timescale

    REAL (pr), DIMENSION ( dim, 1:nlocal, dim ) ::du, d2u
    REAL (pr), DIMENSION ( 1:nlocal )           :: smod, wmod
    INTEGER, PARAMETER                          :: meth=1 ! used meth=1 to match calc_vort()

    ! Default to very low value, indicates very slow/long timescale
    inv_vortex_timescale = 1.0e-12_pr

    IF ( strainrateCFL .OR. vorticityCFL ) THEN
       ! dui/dxj
       CALL c_diff_fast ( velocity, du, d2u, j_lev, nlocal ,meth, 10, dim, 1, dim)  

       IF ( strainrateCFL ) THEN
          CALL modSij_velgrad( du, nlocal, smod, .TRUE. )
          inv_vortex_timescale = MAX( inv_vortex_timescale, MAXVAL(smod) )
       END IF
       IF ( vorticityCFL ) THEN
          CALL cal_magvort_velgrad ( du, wmod, nlocal)
          inv_vortex_timescale = MAX( inv_vortex_timescale, MAXVAL(wmod) )
       END IF
    END IF

  END SUBROUTINE calc_inv_vortex_timescales

  ! Local subroutine for implementing a constant coefficient smagorinsky model
  SUBROUTINE set_eddy_viscosity() 
    USE curvilinear
    IMPLICIT NONE
    INTEGER, SAVE        :: nlocal_last = 0
    REAL (pr), DIMENSION(1:nwlt) :: delta_local
    REAL(pr), DIMENSION(dim,nwlt) :: h_arr

    REAL(pr), DIMENSION(nwlt,dim) :: v 
    REAL (pr), DIMENSION(nwlt,dim*(dim+1)/2) :: s_ij
    REAL (pr), DIMENSION(nwlt)               :: smod
    REAL (pr) :: tmp_min, tmp_max
    INTEGER :: i

    IF( nwlt /= nlocal_last) THEN
      IF( ALLOCATED(user_mu_turb) ) DEALLOCATE( user_mu_turb )
      ALLOCATE( user_mu_turb(1:nwlt) )
    END IF

    CALL get_all_local_h (h_arr)

    ! Calculate delta  ERIC: check
    delta_local(:) = PRODUCT( h_arr(1:dim,1:nwlt), DIM=1 )
    IF( ANY(transform_dir(1:dim)>0) ) THEN
       delta_local = calc_jacobian_determinant( curvilinear_jacobian, dim, nwlt) * delta_local 
       !cfl_acou(:,i) = ( sos(:) * SQRT( SUM(curvilinear_jacobian(i,1:dim,:)**2, DIM=1 ) ) ) * dt/h_arr(i,:)
       !delta_local(:) = PRODUCT( h_arr(1:dim,1:nwlt), DIM=1 )**(1.0_pr/REAL(dim,pr))
    END IF
    delta_local(:) = delta_local**(1.0_pr/REAL(dim,pr))

    tmp_min = MINVAL( delta_local )
    tmp_max = MAXVAL( delta_local )
    CALL parallel_global_sum( REALMINVAL=tmp_min )
    CALL parallel_global_sum( REALMAXVAL=tmp_max )
    IF( par_rank .EQ. 0 ) PRINT *, 'delta_loc: ', tmp_min, tmp_max

    ! Calculate modulus of strain rate
    DO i = 1,dim
       v(1:nwlt,i) = u(1:nwlt,nvel(i)) / u(1:nwlt,nden)
    END DO
    CALL Sij( v, nwlt, s_ij, smod, .TRUE.) 
    tmp_min = MINVAL( smod )
    tmp_max = MAXVAL( smod )
    CALL parallel_global_sum( REALMINVAL=tmp_min )
    CALL parallel_global_sum( REALMAXVAL=tmp_max )
    IF( par_rank .EQ. 0 ) PRINT *, 'smod: ', tmp_min, tmp_max

    ! Calculate a damping function for a channel flow (reusing the local variable Sij)
    s_ij(:,1) = 1.0_pr - EXP( -(1.0_pr - ABS(x(:,2)) )*Re_tau/user_van_driest ) 
    tmp_min = MINVAL( s_ij(:,1) )
    tmp_max = MAXVAL( s_ij(:,1) )
    CALL parallel_global_sum( REALMINVAL=tmp_min )
    CALL parallel_global_sum( REALMAXVAL=tmp_max )
    IF( par_rank .EQ. 0 ) PRINT *, 'damping: ', tmp_min, tmp_max

    ! Calculate the eddy viscosity
    user_mu_turb(:) = u(:,nden) * ( delta_local(:) * user_cs * s_ij(:,1) )**2 * smod(:)

    tmp_min = MINVAL( user_mu_turb )
    tmp_max = MAXVAL( user_mu_turb )
    CALL parallel_global_sum( REALMINVAL=tmp_min )
    CALL parallel_global_sum( REALMAXVAL=tmp_max )
    IF( par_rank .EQ. 0 ) PRINT *, 'Calc eddy viscosity: ', tmp_min, tmp_max, tmp_min*cp_in(Nspec)/user_pr_turb, tmp_max*cp_in(Nspec)/user_pr_turb



    nlocal_last = nwlt

  END SUBROUTINE set_eddy_viscosity

END MODULE user_case
