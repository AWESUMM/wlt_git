! Compressible, gravity-driven flow demonstrative case


MODULE user_case

  USE precision
  USE wlt_vars
  USE wavelet_filters_mod
  USE wlt_trns_mod
  USE wlt_trns_vars
  USE wlt_trns_util_mod
  USE io_3d_vars
  USE util_mod
  USE util_vars
  USE share_consts
  USE pde
  USE sizes
  USE share_kry
  USE vector_util_mod
  USE field
  USE input_file_reader
  USE parallel
  USE hyperbolic_solver
  USE equations_compressible
  USE equations_compressible_vars

  INTEGER n_var_pressure  ! start of pressure in u array

  INTEGER, ALLOCATABLE, DIMENSION(:) :: ngdy
  LOGICAL :: adaptgrady = .FALSE., savegrady = .FALSE.

CONTAINS

  SUBROUTINE  user_setup_pde ( VERB ) 
    IMPLICIT NONE
    LOGICAL, OPTIONAL :: VERB         ! print debug info
    INTEGER :: i, l, n_next   
    CHARACTER(LEN=8) :: specnum  


    IF (par_rank.EQ.0) THEN
       PRINT * ,''
       PRINT *, '**********************Setting up PDE*****************'
       PRINT * ,'CASE: Gravity-driven flow '
       PRINT *, '*****************************************************'
    END IF

    ! Set up compressible variable counts and numbers
    CALL compressible_setup_pde(0)

    n_integrated = n_var_integrated_compressible  

    n_var_additional = n_var_additional_compressible

    IF (ALLOCATED(ngdy)) DEALLOCATE(ngdy)
    IF (Nspec>1) THEN
       ALLOCATE(ngdy(Nspec-1))
       IF( adaptGradY   .OR. saveGradY   ) THEN
          DO l=1,Nspec-1
             n_var_additional = n_var_additional + 1
             ngdy(l) = n_integrated + n_var_additional
          END DO
       END IF
    END IF

    n_var = n_integrated + n_var_additional !--Total number of variables

    n_var_exact = 0

    !******************************************************************************************************************
    !
    !--Allocate logical arrays 
    !  Must be done after setting number of different types of variables
    !  and before setting logical variable mapping arrays
    !
    CALL alloc_variable_mappings

    !
    ! Setup a scaleCoeff array if we want to tweak the scaling of a variable
    ! ( if scaleCoeff < 1.0 then more points will be added to grid 
    !
    ALLOCATE ( scaleCoeff(1:n_var) )
    scaleCoeff = 1.0_pr

    ! Fill in variable names for default variables for integration meth 2
    ! These may be changed in the call to the user specified routine
    !  user_setup_pde()
    !

    IF( (adaptGradY   .OR. saveGradY) .AND. Nspec > 1 ) THEN
       DO l=1,Nspec-1
          WRITE (specnum,'(I8)') l
          WRITE (u_variable_names(ngdy(l)), u_variable_names_fmt) TRIM('GradY_'//ADJUSTL(TRIM(specnum)))//'  '
       END DO
    END IF

    !
    ! setup logical variable mapping arrays
    ! This defined which variables are used for adaptation, Which need to be interpololated to the new grid at 
    ! each time step, which are saved, and for which we have an exect solutiuon we want to check at each time step
    !
    !
    ! setup which components we will base grid adaptation on.
    !
    n_var_adapt = .FALSE. !intialize

    IF( adaptGradY .AND. Nspec > 1 ) n_var_adapt(ngdy(1):ngdy(Nspec-1),:) = .TRUE.

    !--integrated variables at first time level
    !Variables that need to be interpoleted to new adapted grid in initial grid adaptation
    !Variables that need to be interpoleted to new adapted grid at each time step
    n_var_interpolate        = .FALSE. !intialize

    IF( saveGradY .AND. Nspec > 1 ) n_var_interpolate(ngdy(1):ngdy(Nspec-1),:)  = .TRUE.

    !
    ! setup which components we have an exact solution for
    !
    n_var_exact_soln = .FALSE. !intialize

    !
    ! setup which variables we will save the solution
    !
    n_var_save = .FALSE. !intialize 


    IF( saveGradY .AND. Nspec > 1) n_var_save(ngdy(1):ngdy(Nspec-1)) = .TRUE.

    n_var_req_restart = .FALSE.
    n_var_req_restart(1:n_integrated) = .FALSE.


    ! Specify variable mappings, names, adaptation, save, etc.
    CALL compressible_setup_pde(1)


    !
    ! Set the maximum number of components for which we have an exact solution
    !
    n_var_exact = MAX( COUNT(n_var_exact_soln(:,0)),COUNT(n_var_exact_soln(:,1)) )

    IF(hypermodel .NE. 0) THEN
       IF(ALLOCATED(n_var_hyper)) DEALLOCATE(n_var_hyper)
       ALLOCATE(n_var_hyper(1:n_var))
       n_var_hyper = .FALSE.
       n_var_hyper(n_var_den) = .TRUE.
       n_var_hyper(n_var_eng) = .TRUE.
       !n_var_hyper = .TRUE.


       IF(ALLOCATED(n_var_hyper_active)) DEALLOCATE(n_var_hyper_active)
       ALLOCATE(n_var_hyper_active(1:n_integrated))
       n_var_hyper_active = .FALSE.
       ! n_var_hyper_active(nden) = .TRUE.
       !       n_var_hyper_active(neng) = .TRUE.
       n_var_hyper_active = .TRUE.

    END IF

    IF (par_rank.EQ.0) THEN
       PRINT *, 'n_integrated = ',n_integrated 

       PRINT *, 'n_var = ',n_var 
       PRINT *, 'n_var_exact = ',n_var_exact 
       PRINT *, '*******************Variable Names*******************'
       DO i = 1,n_var
          WRITE (*, u_variable_names_fmt) u_variable_names(i)
       END DO
       PRINT *, '****************************************************'
    END IF

    n_var_pressure = n_var_prs

  END SUBROUTINE  user_setup_pde


  !
  ! Set the exact solution for comparison to the simulated solution
  !
  ! u          - array to fill in the exact solution
  ! nlocal       - number of active wavelets   
  ! ne_local        - total number of equations
  ! t          - time of current time step 
  ! l_n_var_exact_soln_index - index into the elements of u for which we need to 
  !                            find the exact solution
  SUBROUTINE  user_exact_soln (u, nlocal,  t_local, l_n_var_exact_soln)  
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: nlocal
    REAL (pr), INTENT (IN) ::  t_local
    REAL (pr), DIMENSION (nlocal,n_var_exact), INTENT (INOUT) :: u
    LOGICAL , INTENT (IN) :: l_n_var_exact_soln(n_var)
    INTEGER :: i

  END SUBROUTINE  user_exact_soln

  SUBROUTINE user_initial_conditions (u, nlocal, ne_local, t_local, scl, scl_fltwt,iter)
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: nlocal, ne_local
    REAL (pr), DIMENSION (nlocal,ne_local), INTENT (INOUT) :: u
    REAL (pr), INTENT (IN) :: t_local
    REAL (pr) :: scl(1:n_var),scl_fltwt

    INTEGER, INTENT (INOUT) :: iter
    INTEGER :: i
    INTEGER :: mynwlt_global, avgnwlt, maxnwlt, minnwlt, fullnwlt


    IF (IC_restart_mode > 0 ) THEN !in the case of restart
       !do nothing
    ELSE IF (IC_restart_mode .EQ. 0) THEN
       !Set Density
       u(:,n_var_den)              = 1.0_pr

       !Set Background Momentum
       DO i=1,dim
          u(:,n_var_mom(i)) = 0.0_pr
       END DO

       !Set Energy for constant temperature
       u(:,n_var_eng)              = 10.0_pr

       IF (Nspec>1) u(:,n_var_spc(1:Nspec-1)) = 0.0_pr

    END IF

    ! Initial condition logger
    mynwlt_global = nwlt
    fullnwlt = PRODUCT(mxyz(1:dim)*2**(j_lev-1) + 1 - prd(:))
    CALL parallel_global_sum (INTEGER=mynwlt_global)
    avgnwlt = mynwlt_global/par_size 
    maxnwlt = nwlt
    minnwlt = -nwlt
    CALL parallel_global_sum(INTEGERMAXVAL=maxnwlt)
    CALL parallel_global_sum(INTEGERMAXVAL=minnwlt)
    minnwlt=-minnwlt
    IF (par_rank.EQ.0) THEN
       PRINT *, '**********************************************************'
       PRINT *, 'j_mx   = ', j_mx,           '  twrite  = ', twrite
       PRINT *, 'j_lev  = ', j_lev,          '  n%      = ', REAL(mynwlt_global,pr)/REAL(fullnwlt,pr)
       PRINT *, 'nwlt_g = ', mynwlt_global,  '  n_all   = ', fullnwlt
       PRINT *, 'nwlt_mx= ', maxnwlt,        '  nwlt_av = ', avgnwlt
       PRINT *, 'nwlt_mn= ', minnwlt,        '  p_size  = ', par_size
       PRINT *, '**********************************************************'
    END IF

  END SUBROUTINE user_initial_conditions


  !--********************************  
  ! Arguments
  ! u         - field on adaptive grid
  ! nlocal      - number of active points
  ! ne_local       - number of equations
  ! t         - current time
  !
  ! Global variables used
  ! ifn       - number of equations
  ! x()
  ! xO
  ! y0
  !--******************************** 
  SUBROUTINE user_algebraic_BC (Lu, u, nlocal, ne_local, jlev, meth)
    !--Defines boundary condition type
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: jlev, meth, ne_local, nlocal
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (INOUT) :: Lu
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (IN)    :: u

    INTEGER :: ie, i, ii, shift, denshift,eshift
    INTEGER, DIMENSION (dim) :: velshift 
    REAL (pr), DIMENSION (ne_local,nlocal,dim) :: du, d2u !uncomment if Neuman BC are used, need to call 
    REAL (pr), DIMENSION (1,nlocal,dim) :: du_prev
    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc
    REAL (pr), DIMENSION(dim, 2) :: u_wall


    !if Neuman BC are used, need to call 
    !CALL c_diff_fast (u, du, d2u, jlev, nlocal, meth, 10, ne_local, 1, ne_local)
    DO ie = 1, ne_local
       shift = nlocal*(ie-1)
       !--Go through all Boundary points that are specified
       i_p_face(0) = 1
       DO i=1,dim
          i_p_face(i) = i_p_face(i-1)*3
       END DO
       DO face_type = 0, 3**dim - 1
          face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
          IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
             CALL get_all_indices_by_face (face_type, jlev, nloc, iloc)
             IF(nloc > 0 ) THEN 
                ! Nothing: periodic BCs
                ! Lu(shift+iloc(1:nloc)) = u(shift+iloc(1:nloc)) 
             END IF
          END IF
       END DO
    END DO

  END SUBROUTINE user_algebraic_BC

  SUBROUTINE user_algebraic_BC_diag (Lu_diag, nlocal, ne_local, jlev, meth)
    !--Defines boundary condition type
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: jlev, meth, ne_local, nlocal
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (INOUT) :: Lu_diag

    INTEGER :: ie, i, ii, shift, denshift

    REAL (pr), DIMENSION (nlocal,dim) :: du_diag, d2u_diag  !uncomment if Neuman BC are used
    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc
    REAL (pr), DIMENSION (1,nlocal,dim) :: du_prev, d2u
    INTEGER, PARAMETER :: methprev=1

    !if Neuman BC are used, need to call 
    CALL c_diff_diag ( du_diag, d2u_diag, jlev, nlocal, meth, meth, 10)

    DO ie = 1, ne_local
       shift = nlocal*(ie-1)
       !--Go through all Boundary points that are specified
       i_p_face(0) = 1
       DO i=1,dim
          i_p_face(i) = i_p_face(i-1)*3
       END DO
       DO face_type = 0, 3**dim - 1
          face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
          IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
             CALL get_all_indices_by_face (face_type, jlev, nloc, iloc)
             IF(nloc > 0 ) THEN 
                ! Lu_diag(shift+iloc(1:nloc)) = 1.0_pr
             END IF
          END IF
       END DO
    END DO

  END SUBROUTINE user_algebraic_BC_diag

  SUBROUTINE user_algebraic_BC_rhs (rhs, ne_local, nlocal, jlev)
    !--Sets rhs for boundary conditions
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: ne_local, nlocal, jlev
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (INOUT) :: rhs

    INTEGER :: ie, i, ii, shift, denshift
    INTEGER, PARAMETER ::  meth=1
    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc
    REAL (pr), DIMENSION (ne_local,nlocal,dim) :: duall, d2uall
    REAL (pr), DIMENSION (1,nlocal,dim) :: du_prev, du, d2u  
    REAL (pr) :: p_bc


    DO ie = 1, ne_local
       shift = nlocal*(ie-1)
       !--Go through all Boundary points that are specified
       i_p_face(0) = 1
       DO i=1,dim
          i_p_face(i) = i_p_face(i-1)*3
       END DO
       DO face_type = 0, 3**dim - 1
          face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
          IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
             CALL get_all_indices_by_face (face_type, jlev, nloc, iloc)
             IF(nloc > 0 ) THEN 
                ! rhs(shift+iloc(1:nloc)) = 0.0_pr
             END IF
          END IF
       END DO
    END DO


  END SUBROUTINE user_algebraic_BC_rhs

  FUNCTION user_chi (nlocal, t_local)
    USE parallel
    USE threedobject
    IMPLICIT NONE
    INTEGER,          INTENT (IN) :: nlocal
    REAL (pr),        INTENT (IN) :: t_local
    REAL (pr), DIMENSION (nlocal) :: user_chi

    user_chi = 0.0_pr
    !IF(read_geometry) THEN
    !   CALL CADtest(nlocal,.FALSE.,user_chi)
    !END IF

!!$square obstacle
!!$   user_chi = 0.5_pr*(tanh((x(:,1)-0.4_pr))-tanh((x(:,1)-0.6_pr))) &
!!$              * 0.5_pr*(tanh((x(:,2)-0.4_pr))-tanh((x(:,2)-0.6_pr))) 


  END FUNCTION user_chi

  FUNCTION user_rhs (u_integrated,scalar)
    IMPLICIT NONE
    REAL (pr), DIMENSION (ng,ne), INTENT(IN) :: u_integrated !1D flat version of integrated variables without BC 
    REAL (pr), DIMENSION (ng), INTENT(IN) :: scalar
    REAL (pr), DIMENSION (ng*ne) :: user_rhs

    REAL (pr), DIMENSION (ne,ng,dim) :: du, d2u
    INTEGER, PARAMETER :: meth=1

    user_rhs(:) = 0.0_pr

    CALL compressible_rhs()
    !IF ( hypermodel .NE. 0 ) CALL hyperbolic(u_integrated,ng,user_rhs)
    !IF ( cnvtBC .OR. diffBC ) CALL user_convectzone_BC_rhs (user_rhs, u_integrated, ne, ng, j_lev, LOW_ORDER) !add artificial convection to bufferzone/obstacle
    !IF ( buffBC ) CALL user_bufferzone_BC_rhs (user_rhs, u_integrated, ne, ng, j_lev, meth)

  END FUNCTION user_rhs

  FUNCTION user_Drhs (u_p, u_prev, meth)
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: meth
    REAL (pr), DIMENSION (ng,ne) :: u_p, u_prev
    REAL (pr), DIMENSION (ng*ne) :: user_Drhs
    REAL (pr), DIMENSION (2*ne,ng,dim) :: du
    REAL (pr), DIMENSION (ne,ng,dim)   :: d2u

    user_drhs(:) = 0.0_pr

    CALL compressible_Drhs()

    !IF (hypermodel .NE. 0 ) CALL hyperbolic(u_p,ng,user_Drhs)
    !IF (cnvtBC .OR. diffBC) CALL user_convectzone_BC_Drhs (user_Drhs, u_p, u_prev, ne, ng, j_lev, LOW_ORDER) !add artificial convection to bufferzone/obstacle
    !IF (buffBC) CALL user_bufferzone_BC_Drhs (user_Drhs, u_p, u_prev, ne, ng, j_lev, meth)

  END FUNCTION user_Drhs

  FUNCTION user_Drhs_diag (meth)
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: meth
    REAL (pr), DIMENSION (ng*ne) :: user_Drhs_diag
    REAL (pr), DIMENSION (ng,ne) :: u_prev
    REAL (pr), DIMENSION (ne,ng,dim) :: du_dummy
    REAL (pr), DIMENSION (ng,dim)    :: du_diag, d2u_diag 
    INTEGER :: ie, shift

    user_drhs_diag(:) = 0.0_pr

    CALL compressible_Drhs_diag()

    !IF(hypermodel .NE. 0 ) CALL hyperbolic_diag(user_Drhs_diag, ng)
    !IF (cnvtBC .OR. diffBC) CALL user_convectzone_BC_Drhs_diag (user_Drhs_diag, u_prev, ne, ng, j_lev, LOW_ORDER)
    !IF (buffBC) CALL user_bufferzone_BC_Drhs_diag (user_Drhs_diag, u_prev, ne, ng, j_lev, meth)


  END FUNCTION user_Drhs_diag



  SUBROUTINE user_project (u, p, nlocal, meth)
    IMPLICIT NONE

    INTEGER, INTENT (IN) :: meth, nlocal
    REAL (pr), DIMENSION (nlocal),     INTENT(INOUT) :: p
    REAL (pr), DIMENSION (nlocal,n_integrated), INTENT(INOUT) :: u

    !no projection for compressible flow
  END SUBROUTINE user_project

  !
  ! Calculate any statitics
  !
  ! startup_flag - 0 when adapting to IC,then 1 inmain integration loop
  !
  SUBROUTINE user_stats ( u ,j_mn, startup_flag)
    USE parallel
    USE penalization
    IMPLICIT NONE
    REAL (pr), DIMENSION (nwlt,1:n_var), INTENT (IN) :: u
    INTEGER , INTENT (IN) :: j_mn 
    INTEGER , INTENT (IN) :: startup_flag


  END SUBROUTINE user_stats

!!$  SUBROUTINE user_cal_force (u, F_n, Fp_n, Tor_n)
!!$    IMPLICIT NONE
!!$    ! Input and output params
!!$    REAL (pr), DIMENSION (nwlt, n_var), INTENT(IN) :: u
!!$    REAL (pr), DIMENSION (dim, 0:Nsp), INTENT(INOUT) :: F_n, Fp_n, Tor_n
!!$    
!!$   
!!$  END SUBROUTINE user_cal_force



  !
  ! Read input from "case_name"_pde.inp file
  ! case_name is in string file_name read from command line
  ! in read_command_line_input()
  ! 
  SUBROUTINE user_read_input()
    IMPLICIT NONE
    INTEGER :: i, j, ind
    CHARACTER(LEN=8):: numstrng, procnum
    INTEGER :: myiter
    LOGICAL :: checkit 
    REAL (pr) :: halfdomsize

    CALL compressible_read_input_and_initialize()

    !call input_real ('Re',Re,'stop',' Re: Reynolds Number')
    !call input_real ('Pra',Pra,'stop',' Pra: Prandtl Number')
    !call input_real ('Sc',Sc,'stop',' Sc: Schmidt Number')
    !call input_real ('Fr',Fr,'stop',' Fr: Froude Number')
    !call input_real ('Ma',Ma,'stop',' Ma: Mach Number')
    !call input_real ('S1',S1,'stop',' S1: Sutherland Constant S_1/T_0 (variable viscosity)')

    !call input_real_vector ('gamm',gamm, Nspec,'stop',' gamm: heat capacity ratio for each pure fluid')

    !call input_logical ('convcfl',convcfl,'stop', 'convcfl: T to use convective cfl, F to use acoustic cfl')
    !call input_logical ('boundY',boundY,'stop', 'boundY: T to bound Y to [0,1] in user_pre/post_process')



    gr_ref(:) = 0.0_pr  !g's by species, then dimension
    !IF (GRAV) gr_in(1:Nspec) = -grav_coeff  !sign change for forcing in the positive x-direction

    !    MW_in(:) = 1.0_pr
    !MW_ref(:) = gamm(:)
    !    MW_in(1) = 1.0_pr+At
    !MW_in(1) = MW_in(1)+At
    !    IF (Nspec>1) MW_in(2) = 1.0_pr-At
    !IF (Nspec>1) MW_in(2) = MW_in(2)-At
    !YR(:) = MW_in(:) / SUM(MW_in(:))
    !cp_in(:) = 1.0_pr/MW_in(:)*gamm(:)/(gamm(:)-1.0_pr)/Ma**2.0_pr
    !gammR = SUM(cp_in(:)*YR(:))/(SUM(cp_in(:)*YR(:))-SUM(YR(:)/MW_in(:)/Ma**2.0_pr) )
    !mu_in(:) = 1.0_pr/Re
    !    kk_in(:) = 1.0_pr/Re/Pra*gammR/(gammR-1.0_pr)
    !kk_in(:) = 1.0_pr/Re/Pra/(gammR-1.0_pr)/Ma**2.0_pr

    !bD_in(:) = 1.0_pr/Re/Sc

  END SUBROUTINE user_read_input

  !
  ! calculate any additional variables
  ! 
  ! arg 
  ! flag  - 0 calledwhile adapting to IC, 1 - called in main time integration loop
  !
  ! These additiona variables are calculated and left in real space.
  !
  SUBROUTINE user_additional_vars( t_local, flag )
    IMPLICIT NONE
    REAL (pr), INTENT (IN) ::  t_local
    INTEGER , INTENT(IN) :: flag ! 0- called during adaption to IC, 1 called during main integration loop
!!$    REAL (pr) :: cp, R, mygr, cmax, uval
!!$    INTEGER :: l, i, j, myiter
!!$    REAL (pr), DIMENSION(1,nwlt,MAX(dim,Nspec-1,2)) :: du, d2u    
!!$    REAL (pr) :: iterdiff, initdiff, curtau, fofn, deln, delnold, delfofn
!!$    INTEGER, PARAMETER :: meth=1
!!$    REAL (pr), DIMENSION(nwlt,dim*2) :: bigU
!!$
!!$
!!$    REAL (pr), DIMENSION(nwlt) :: a_wall
!!$    INTEGER :: face_type, nloc, wlt_type, k, j_df
!!$    INTEGER :: ie, i_bnd,pts_total
!!$    INTEGER, DIMENSION(0:dim) :: i_p_face
!!$    INTEGER, DIMENSION(dim) :: face
!!$    INTEGER, DIMENSION(nwlt) :: iloc
!!$    REAL (pr) :: a_total, rho_w, rho_m, volume_total, vel_bulk
!!$    REAL (pr), DIMENSION(nwlt,dim) :: v
!!$    REAL (pr), DIMENSION(dim,nwlt,dim) :: dv, d2v
!!$
!!$    INTEGER, DIMENSION(nwlt) :: icolor


  END SUBROUTINE user_additional_vars


  !
  ! calculate any additional scalar variables
  ! 
  ! arg 
  ! flag  - 0 calledwhile adapting to IC, 1 - called in main time integration loop
  !
  SUBROUTINE user_scalar_vars( flag )
    IMPLICIT NONE
    INTEGER , INTENT(IN) :: flag ! 0- called during adaption to IC, 1 called during main integration loop

  END SUBROUTINE user_scalar_vars

  !
  !************ Calculating Scales ***************************
  !
  ! Note the order of the components in the scl array
  ! correspond to u_tn, v_tn, w_tn, u_tn-1, v_tn-1, w_tn-1, u_tn-2, v_tn-2, w_tn-2
  ! 
  SUBROUTINE user_scales(flag,use_default, u_loc, nlocal, ne_local, l_n_var_adapt , l_n_var_adapt_index, &
       scl, scl_fltwt) !add
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: flag ! 0 during initial adaptation, then 1 during time advancement
    LOGICAL , INTENT(INOUT) :: use_default
    INTEGER, INTENT (IN) :: nlocal, ne_local
    REAL (pr), DIMENSION (1:nlocal,1:ne_local), INTENT (IN) :: u_loc
    LOGICAL , INTENT (IN) :: l_n_var_adapt(ne_local)
    INTEGER , INTENT (IN) :: l_n_var_adapt_index(1:ne_local)
    REAL (pr), DIMENSION (1:ne_local), INTENT (INOUT) :: scl
    REAL (pr), DIMENSION (1:ne_local) :: mean
    REAL (pr) ,  INTENT(IN) ::scl_fltwt !weight for temporal filter on scl
    REAL (pr) :: floor, tmp,tmpmax,tmpmin,tmpsum
    INTEGER :: i, ie, ie_index, tmpint

    !
    ! Ignore the output of this routine and use default scales routine
    !
    use_default = .FALSE. 

    IF( .NOT. use_default ) THEN
       CALL compressible_scales( flag, use_default, u_loc, nlocal, ne_local, l_n_var_adapt , l_n_var_adapt_index, &
       scl, scl_fltwt)
    END IF

  END SUBROUTINE user_scales

  SUBROUTINE user_cal_cfl (use_default, ulocal, cfl_out)
    USE precision
    USE sizes
    USE pde
    IMPLICIT NONE
    LOGICAL , INTENT(INOUT) :: use_default
    REAL (pr),                                INTENT (INOUT) :: cfl_out
    REAL (pr), DIMENSION (nwlt,n_integrated), INTENT (IN)    :: ulocal
    REAL (pr)                  :: flor


    use_default = .FALSE.

    flor = 1e-12_pr
    cfl_out = 0.01_pr

  END SUBROUTINE user_cal_cfl

  SUBROUTINE user_init_sgs_model( )
    IMPLICIT NONE
  END SUBROUTINE user_init_sgs_model

  SUBROUTINE  user_sgs_force (u_loc, nlocal)
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: nlocal
    REAL (pr), DIMENSION (nlocal,n_integrated), INTENT (INOUT) :: u_loc
  END SUBROUTINE  user_sgs_force

  FUNCTION user_sound_speed (u, neq, nwlt)
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: nwlt, neq
    REAL (pr), DIMENSION (nwlt,neq), INTENT(IN) :: u
    REAL (pr), DIMENSION (nwlt) :: user_sound_speed,gam

    user_sound_speed(:) = 0.0_pr
  END FUNCTION user_sound_speed

  SUBROUTINE  user_pre_process
    IMPLICIT NONE
 
  END SUBROUTINE user_pre_process

  SUBROUTINE  user_post_process
    IMPLICIT NONE

  END SUBROUTINE user_post_process





END MODULE user_case
