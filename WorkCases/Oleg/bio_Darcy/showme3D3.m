function showme3D3(fileName,varName,st_num,fig_type)
titName=[];
prefix=[];
if strcmp(fig_type,'solution')
  titName=varName;
  printName=titName;
  if strcmp(varName,'rho') prefix='\'; end
  if strcmp(varName,'visc') titName='\nu_t'; end
elseif strcmp(fig_type,'grid')
  titName='grid'
  printName=titName;
end
figure(1)
  showme3D([fileName '_alt_8.'],varName,st_num,fig_type)
  set(gca,'PlotBoxAspectRatio',[1 1 1])
  res='641x641';
  ylabel('y'); xlabel('x'); title([prefix titName ', ' res ',  t=' num2str(0.1*st_num,'%2.1f')]);
  print('-dpdf',[printName '_t'  num2str(0.1*st_num,'%2.1f') '_' res '.pdf']);
figure(2)
showme3D([fileName '_alt_9.'],varName,st_num,fig_type)
  set(gca,'PlotBoxAspectRatio',[1 1 1])
  res='1281x1281';
  ylabel('y'); xlabel('x'); title([prefix titName ', ' res ',  t=' num2str(0.1*st_num,'%2.1f')]);
  print('-dpdf',[printName '_t'  num2str(0.1*st_num,'%2.1f') '_' res '.pdf']);
figure(3)
showme3D([fileName '_alt_10.'],varName,st_num,fig_type)
  set(gca,'PlotBoxAspectRatio',[1 1 1])
  res='2561x2561';
  ylabel('y'); xlabel('x'); title([prefix titName ', ' res ',  t=' num2str(0.1*st_num,'%2.1f')]);
  print('-dpdf',[printName '_t'  num2str(0.1*st_num,'%2.1f') '_' res '.pdf']);
end
