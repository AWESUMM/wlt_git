MODULE user_case
  !
  ! Hydrostatic Primitive Equations
  USE precision
  USE elliptic_vars
  USE elliptic_mod
  USE field
  USE input_file_reader
  USE io_3D_vars
  USE pde
  USE share_consts
  USE share_kry
  USE sizes
  USE util_mod
  USE util_vars
  USE vector_util_mod
  USE wavelet_filters_mod
  USE wlt_vars
  USE wlt_trns_vars
  USE wlt_trns_mod
  USE wlt_trns_util_mod
!  USE misc_vars
  USE fft_module
  USE SGS_incompressible

  !
  ! case specific variables
  !
  INTEGER :: n_var_eta ! start of level set function in u array (Must exist)
  INTEGER, ALLOCATABLE, DIMENSION (:) :: n_var_U ! start of velecities in the array u (Must exist)
  INTEGER :: n_var_pressure  ! start of pressure in u array  (Must exist)
  INTEGER :: n_var_mask    !Brinkman penalization mask

  REAL(pr) :: delta_wind, delta_i, delta_m, delta_s, Pl, Fr, A_LH, nu_ratio, visconoff, delta_pen, eta_pen, kappa1, kappa2, kappa3, kappa4, x01_pen, x02_pen, c_s, c_z, tol_ex
  INTEGER, PARAMETER :: ie_pressure = 1, ie_W = 2, ne_vertical = 2
  REAL(pr), DIMENSION(:), ALLOCATABLE :: w_correction
  LOGICAL :: continent_active
  REAL(pr), DIMENSION(:), ALLOCATABLE :: chi_continent
CONTAINS

  !
  ! In user_setup_pde() we setup how many variables are integrated, which are interpolated
  ! to the next times step and if any exeact solution exists to compare to during the run.
  ! We also set the variable names that are used when the result files are written.
  !
  ! The following variables must be setup in this routine:
  !
  !
  ! n_integrated     ! first n_integrated eqns will be acted on for time integration
  ! n_var_additional ! 
  ! n_var
  !
  !
  !
  !
  SUBROUTINE  user_setup_pde ( VERB ) 
    IMPLICIT NONE
    LOGICAL, OPTIONAL :: VERB         ! print debug info
    INTEGER :: i

    PRINT * ,''
    PRINT *, '**********************Setting up PDE*****************'
    PRINT * ,'CASE HPE '
    PRINT *, '*****************************************************'


    !------------ setting up default values 

    n_integrated = dim ! uvw - # of equations to solve without SGS model
 
    n_integrated = n_integrated + n_var_SGS ! adds additional equations for SGS model
    
    n_time_levels = 1  !--# time levels

    n_var_time_levels = n_time_levels * n_integrated !--Number of equations (2D velocity at two time levels)

    n_var_additional = 3 !--1 pressure & vertical velocity & mask at one time level & mask

    n_var_exact = 0 !--No exact solution 


    ALLOCATE(n_var_U(dim))
    n_var_eta = 1
    DO i = 1, dim-1 
       n_var_U(i)   = 1 + i !u(i)
    END DO

    n_var_U(dim) = n_integrated + ie_W
    n_var_pressure  = n_integrated + ie_pressure !pressure
 
    n_var_mask = n_var_U(dim) + 1  !BP mask
    
    n_var = n_var_time_levels + n_var_additional !--Total number of variables

    !
    !--Allocate logical arrays 
    !  Must be done after setting number of different types of variables
    !  and before setting logical variable mapping arrays
    !

    CALL alloc_variable_mappings

    !
    ! Fill in variable names for default variables for integration meth 2
    ! These may be changed in the call to the user specified routine
    !  user_setup_pde()
    !
    ! In integrated variables (3D)

    WRITE (u_variable_names(1), u_variable_names_fmt)                              'eta  '
    IF( dim >= 2 ) WRITE (u_variable_names(2), u_variable_names_fmt)               'U    '
    IF( dim >= 3 ) WRITE (u_variable_names(3), u_variable_names_fmt)               'V    '
    WRITE (u_variable_names(n_var_pressure), u_variable_names_fmt)                 'P    '
    IF( size(n_var_U) == dim) WRITE (u_variable_names(n_var_U(dim)), u_variable_names_fmt)'W    '
    WRITE (u_variable_names(n_var_mask), u_variable_names_fmt) 'Mask'

    !
    ! setup logical variable mapping arrays
    ! This defined which variables are used for adaptation, Which need to be interpololated to the new grid at 
    ! each time step, which are saved, and for which we have an exect solutiuon we want to check at each time step
    !

    !
    ! setup which components we will base grid adaptation on.
    ! Adapt on the 3 velocity components
    !
    n_var_adapt = .FALSE. !--Initially adapt on integrated variables at first time level

    !eventually adapt to mdl variables    n_var_adapt(1:dim+2,1) = .TRUE. !--After first time step adapt on  velocity and Ilm and Imm

    n_var_adapt(1:n_var,0) = .TRUE. !--After first time step adapt on  velocity
    n_var_adapt(1:n_var,1) = .TRUE. !--After first time step adapt on  velocity

!    n_var_adapt(2,0) = .TRUE. !--After first time step adapt on  velocity
!    n_var_adapt(2,1) = .TRUE. !--After first time step adapt on  velocity

    n_var_adapt(n_var_mask, 0) = .FALSE.
    n_var_adapt(n_var_mask, 1) = .FALSE.
    IF (imask_obstacle) THEN    
       n_var_adapt(n_var_mask, 0) = .FALSE.
       n_var_adapt(n_var_mask, 1) = .FALSE.
    ENDIF

!    n_var_adapt(n_var_pressure,0) = .TRUE. !--After first time step adapt on  velocity
!    n_var_adapt(n_var_U(dim),0) = .TRUE. !--After first time step adapt on  velocity
!    n_var_adapt(n_var_pressure,1) = .TRUE. !--After first time step adapt on  velocity
!    n_var_adapt(n_var_U(dim),1) = .TRUE. !--After first time step adapt on  velocity

!    n_var_adapt(n_var_pressure,1) = .TRUE. !--After first time step adapt on  velocity

    !Variables that need to be interpoleted to new adapted grid in initial grid adaptation
    n_var_interpolate(1:n_var, 0) = .TRUE.
    n_var_interpolate(1:n_var, 1) = .TRUE.
    n_var_interpolate(1:n_integrated,0) = .TRUE. 
    n_var_interpolate(1:n_var_pressure,0) = .TRUE. 
    n_var_interpolate(1:n_var_U(dim),0) = .TRUE. 
    n_var_interpolate(1:n_integrated,1) = .TRUE. 
    n_var_interpolate(1:n_var_pressure,1) = .TRUE. 
    n_var_interpolate(1:n_var_U(dim),1) = .TRUE. 
    
    !Variables that need to be interpoleted to new adapted grid at each time step

    ! setup which components we have an exact solution for

    n_var_exact_soln(:,0:1) = .FALSE.

    !
    ! variables required for restart
    !
    n_var_req_restart = .FALSE.
    n_var_req_restart(1:dim)	        = .TRUE. !restart with velocities and pressure to begin with!

    ! no pressure for restart from initial file
    !n_var_req_restart(n_var_pressure ) = .TRUE.

    !
    ! setup which variables we will save the solution
    !
    n_var_save(1:n_var) = .TRUE. ! save all for restarting code

    !
    !--Time level counters for NS integration
    !   Used for integration meth_forward
    ! n0 is current time step
    ! n1 is t_n-1 time step
    ! n2 is t_n-2 time step
    !
    n0 = 1; n1 = n0+dim; n2 = n1+dim


    !
    ! Set the maximum number of components for which we have an exact solution
    !
    n_var_exact = MAX( COUNT(n_var_exact_soln(:,0)),COUNT(n_var_exact_soln(:,1)) )


    !
	! Setup a scaleCoeff array of we want to tweak the scaling of a variable
	! ( if scaleCoeff < 1.0 then more points will be added to grid 
	!

    ALLOCATE ( scaleCoeff(1:n_var), Umn(1:n_var) )
    scaleCoeff = 1.0_pr


    Umn = 0.0_pr !set up here if mean quantities anre not zero and used in scales or equation

    PRINT *, 'n_integrated = ',n_integrated 
    PRINT *, 'n_time_levels = ',n_time_levels
    PRINT *, 'n_var_time_levels = ',n_var_time_levels 
    PRINT *, 'n_var = ',n_var 
    PRINT *, 'n_var_exact = ',n_var_exact 
    PRINT *, '*******************Variable Names*******************'
    DO i = 1,n_var
       WRITE (*, u_variable_names_fmt) u_variable_names(i)
    END DO
    PRINT *, '****************************************************'



  END SUBROUTINE  user_setup_pde


  !
  ! read variables from input file for this user case 
  !
  SUBROUTINE  user_read_input()
    IMPLICIT NONE


  call input_real ('Fr',Fr,'stop', ' Fr: Froude number')

  call input_real ('delta_i',delta_i,'stop', ' delta_i: Charney boundary layer scale')

  call input_real ('delta_m',delta_m,'stop', ' delta_m: Munk boundary layer scale')

  call input_real ('delta_s',delta_s,'stop', ' delta_s: Bottom drag coefficient')

  call input_real ('delta_wind',delta_wind,'stop', ' delta_wind: vertical stretching of wind')

  call input_real ('Pl',Pl,'stop', ' Pl: Planetary number')

  call input_real ('A_LH',A_LH,'stop', ' A_LH: Ratio of horizontal to depth length scales (and velocity scales)')

  call input_real ('nu_ratio',nu_ratio,'stop', ' nu_ratio: Ratio of vertical to horizontal viscosities')

  call input_real ('visconoff',visconoff,'stop', ' visconoff: Turns viscosity on/off')

  call input_real ('delta_pen',delta_pen,'stop', ' delta_pen: delta parameter for penalization')

  call input_real ('eta_pen',eta_pen,'stop', ' eta_pen: eta parameter for penalization')

  call input_real ('kappa1',kappa1,'stop', ' kappa1: used in boundary conditions at bottom')

  call input_real ('kappa2',kappa2,'stop', ' kappa2: used in boundary conditions at bottom')

  call input_real ('kappa3',kappa3,'stop', ' kappa3: used in boundary conditions at bottom')

  call input_real ('kappa4',kappa4,'stop', ' kappa4: used in boundary conditions at bottom, horizontal viscosity')

  call input_logical ('continent_active',continent_active,'stop', 'continent_active - turns on continental topology ')

  call input_real ('x01_pen',x01_pen,'stop', ' x01_pen: location of left continental boundary')

  call input_real ('x02_pen',x02_pen,'stop', ' x02_pen: location of right continental boundary')

  call input_real ('c_s',c_s,'stop', ' c_s:  time to stop integrating')
    
  call input_real ('c_z',c_z,'stop', ' c_z:  to set time step delta tao = c_z delta z')

  call input_real ('tol_ex',tol_ex,'stop', ' tol_ex:  to set stop integration if error is less than tol_ex')

  END SUBROUTINE  user_read_input

  !
  ! Set the exact solution for comparison to the simulated solution
  !
  ! u          - array to fill in the exact solution
  ! nlocal       - number of active wavelets   
  ! ne_local        - total number of equations
  ! t          - time of current time step 
  ! l_n_var_exact_soln_index - index into the elements of u for which we need to 
  !                            find the exact solution
  SUBROUTINE  user_exact_soln (u, nlocal,  t_local, l_n_var_exact_soln)  
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: nlocal
    REAL (pr), INTENT (IN) ::  t_local
    REAL (pr), DIMENSION (nlocal,n_var_exact), INTENT (INOUT) :: u
    LOGICAL , INTENT (IN) :: l_n_var_exact_soln(n_var)
    INTEGER :: i


    ! There is no exact solution

  END SUBROUTINE  user_exact_soln

  SUBROUTINE user_initial_conditions (u, nlocal, ne_local, t_local, scl, scl_fltwt, iter)
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: nlocal, ne_local
    INTEGER  , INTENT (INOUT) :: iter ! iteration of call while adapting initial grid
    REAL (pr), DIMENSION (nlocal,ne_local), INTENT (INOUT) :: u
    REAL (pr)  :: scl(1:n_var),scl_fltwt
    REAL (pr), INTENT (IN) :: t_local

    INTEGER :: i,ie !TEST

    INTEGER, DIMENSION(1) :: SEED
    REAL (pr) ::   Uprime=1.0e-3_pr
    REAL (pr) ::   Umean,U2mean

	!add or overtise teh values in the fileds
    IF (IC_restart_mode==1 ) THEN !in the case of restart
	   !if restarts - DO NOTHING
    ELSE IF (IC_restart_mode==2) THEN  ! 
  
    ELSE IF (IC_restart_mode==3) THEN  ! 
    
    ELSE IF (IC_restart_mode==0) THEN  ! 

!u(:, 1) = 0.001_pr*COS(-wavenumber*x(:, 1))
!u(:, 2) = (0.001_pr*wavenumber/Re)*(-0.5_pr*SIN(-wavenumber*x(:, 1))+ SQRT((Re/wavenumber/Fr)**2.0_pr - 0.25_pr)*COS(-wavenumber*x(:, 1)))

! Gaussian hump
!       u(:,1) = 0.001_pr*exp(-70.0_pr*(x(:,1)-0.5_pr)**2) ! eta
!       u(:, 2:dim) = 0.0_pr

! Zero initial conditions
!       DO ie = 2, dim
!          u(:,ie) = 0.0E-10_pr !velocity V(:.1:dim-1)
!       END DO
       u(:, 1) = sin(PI*x(:, 2))*(cos(PI*(2.0_pr*x(:, 1)+1.0_pr))+1.0_pr)
       u(:, 2) = -cos(PI*x(:, 2))*(cos(PI*(2.0_pr*x(:, 1)+1.0_pr))+1.0_pr)
       u(:, 3) = -2.0_pr*PI*sin(PI*x(:, 2))*sin(PI*(2.0_pr*x(:, 1)+1.0_pr))

       !u(:, 2) = (x(:, 2)+1.0_pr)/2.0_pr
       !PRINT *, 'here'
       !PAUSE

    END IF



  END SUBROUTINE user_initial_conditions

  ! Arguments
  ! u         - field on adaptive grid
  ! nlocal      - number of active points
  ! ne_local       - number of equations
  ! t         - current time
  !
  ! Global variables used
  ! ifn       - number of equations
  ! x()
  ! xO
  ! y0
  !--******************************** 
  SUBROUTINE user_algebraic_BC (Lu, u, nlocal, ne_local, jlev, meth)
    !--Defines boundary condition type
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: jlev, meth, ne_local, nlocal
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (INOUT) :: Lu
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (IN)    :: u
    REAL (pr), DIMENSION (ne_local, nlocal, dim) :: du
    REAL (pr), DIMENSION (ne_local, nlocal, dim) :: d2u

    INTEGER :: i, ie, ii, shift
    !REAL (pr), DIMENSION (ne_nlocal,nlocal,dim) :: du, d2u !uncomment if Neuman BC are used, need to call 
    INTEGER :: face_type, nloc, meth_local
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc

    !BC for both 2D and 3D cases
    !if Neuman BC are used, need to call 
    CALL c_diff_fast (u, du, d2u, jlev, nlocal, meth, 10, ne_local, 1, ne_local)

    DO ie = 2, dim
       shift = nlocal*(ie-1)
       !--Go through all Boundary points that are specified
       i_p_face(0) = 1
       DO i=1,dim
          i_p_face(i) = i_p_face(i-1)*3
       END DO
       DO face_type = 0, 3**dim - 1
          face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
          IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
             CALL get_all_indices_by_face (face_type, jlev, nloc, iloc)
             IF(nloc > 0 ) THEN 
                IF( face(dim) == 1  ) THEN                      ! top face
!                   Lu(shift+iloc(1:nloc)) = u(shift+iloc(1:nloc))  !Dirichlet conditions  !no slip
                   Lu(shift+iloc(1:nloc)) = du(ie,iloc(1:nloc),dim)    !Newman conditions  !shear-free
                ELSE IF( face(dim) == -1  ) THEN                      ! bottom face
!                   Lu(shift+iloc(1:nloc)) = u(shift+iloc(1:nloc))  !Dirichlet conditions  !no slip
                   Lu(shift+iloc(1:nloc)) = du(ie,iloc(1:nloc),dim)    !Newman conditions  !shear-free
!                   Lu(shift+iloc(1:nloc)) = du(ie,iloc(1:nloc),dim) - kappa2*u(shift+iloc(1:nloc))    !Newman conditions  !slip
                ELSE IF( ABS(face(1)) == 1  ) THEN                          ! E, W faces
                   IF(ie == n_var_U(1)) THEN !U
                      Lu(shift+iloc(1:nloc)) = u(shift+iloc(1:nloc))  !Dirichlet conditions (no slip)
                   ELSE IF(ie == n_var_U(2)) THEN !V
                      Lu(shift+iloc(1:nloc)) = u(shift+iloc(1:nloc))  !Dirichlet conditions (no slip)
                   END IF
                ELSE IF( ABS(face(dim-1)) == 1  ) THEN                          ! N, S faces
                   IF(ie == n_var_U(1)) THEN !U
                      Lu(shift+iloc(1:nloc)) = du(ie,iloc(1:nloc),2)  !Neuman conditions
                      !Lu(shift+iloc(1:nloc)) = u(shift+iloc(1:nloc))  !Dirichlet conditions
                   ELSE IF(ie == n_var_U(2)) THEN !V
                      Lu(shift+iloc(1:nloc)) = u(shift+iloc(1:nloc))  !Dirichlet conditions
                   END IF
                END IF
             END IF
          END IF
       END DO
    END DO

    !default SGS BC, can be explicitely deifined below instead of calling default BCs
    CALL SGS_algebraic_BC (Lu, u, nlocal, ne_local, jlev, meth) 
  END SUBROUTINE user_algebraic_BC

  SUBROUTINE user_algebraic_BC_diag (Lu_diag, nlocal, ne_local, jlev, meth)
    !--Defines boundary condition type
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: jlev, meth, ne_local, nlocal
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (INOUT) :: Lu_diag
    REAL (pr), DIMENSION (nlocal,dim) :: du, d2u

    INTEGER :: i, ie, ii, shift
    ! REAL (pr), DIMENSION (nlocal,dim) :: du, d2u  !uncomment if Neuman BC are used
    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc

    !BC for both 2D and 3D cases

    !if Neuman BC are used, need to call 
    CALL c_diff_diag ( du, d2u, jlev, nlocal, meth, meth, 10)

    DO ie = 2, dim
       shift = nlocal*(ie-1)
       !--Go through all Boundary points that are specified
       i_p_face(0) = 1
       DO i=1,dim
          i_p_face(i) = i_p_face(i-1)*3
       END DO
       DO face_type = 0, 3**dim - 1
          face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
          IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
             CALL get_all_indices_by_face (face_type, jlev, nloc, iloc)
             IF(nloc > 0 ) THEN
!                IF (ALL(ABS(face) == (/1, 0/) ) ) THEN !left and right faces
!                   Lu_diag(shift+iloc(1:nloc)) = 1.0_pr  !Dirichlet conditions 
!                ELSE IF (ALL(ABS(face) == (/0, 1/) ) ) THEN !top and bottom faces
!                   Lu_diag(shift+iloc(1:nloc)) = du(iloc(1:nloc),dim)    !Newman conditions 
!                END IF

!!!!Current BCs 
                IF( face(dim) == 1  ) THEN                        ! top face
!                  Lu_diag(shift+iloc(1:nloc)) = 1.0_pr                       !Dirichlet conditions  !no slip
                   Lu_diag(shift+iloc(1:nloc)) = du(iloc(1:nloc),dim)    !Newman conditions  !shear-free
                ELSE IF( face(dim) == -1  ) THEN                        ! bottom  face
!                  Lu_diag(shift+iloc(1:nloc)) = 1.0_pr                       !Dirichlet conditions  !no slip
                   Lu_diag(shift+iloc(1:nloc)) = du(iloc(1:nloc),dim)    !Newman conditions  !shear-free
!                   Lu_diag(shift+iloc(1:nloc)) = du(iloc(1:nloc),dim) - kappa2*1.0_pr !Newman conditions  !slip
                ELSE IF( ABS(face(1)) == 1  ) THEN                            ! E, W faces
                  IF(ie == n_var_U(1)) THEN !U
                     Lu_diag(shift+iloc(1:nloc)) = 1.0_pr                       !Dirichlet conditions !no slip
                      !Lu_diag(shift+iloc(1:nloc)) = du(iloc(1:nloc),dim) !Neuman conditions !shear-free
                   ELSE IF(ie == n_var_U(2)) THEN !V
                      Lu_diag(shift+iloc(1:nloc)) = 1.0_pr                       !Dirichlet conditions !no slip
                      !Lu_diag(shift+iloc(1:nloc)) = du(iloc(1:nloc),dim) !Neuman conditions !shear-free
                   END IF                
                ELSE IF( ABS(face(dim-1)) == 1  ) THEN                            ! N, S faces
                   IF(ie == n_var_U(1)) THEN !U
                     !Lu_diag(shift+iloc(1:nloc)) = 1.0_pr                       !Dirichlet conditions !no slip
                     Lu_diag(shift+iloc(1:nloc)) = du(iloc(1:nloc),2) !Neuman conditions !shear-free
                   ELSE IF(ie == n_var_U(2)) THEN !V
                      Lu_diag(shift+iloc(1:nloc)) = 1.0_pr                       !Dirichlet conditions !no slip
                      !Lu_diag(shift+iloc(1:nloc)) = du(iloc(1:nloc),dim) !Neuman conditions !shear-free
                   END IF
                END IF
             END IF
          END IF
       END DO
    END DO
    
    !default diagonal terms of SGS BC, can be explicitely deifined below instead of calling default BCs
    IF(sgsmodel /=0 ) CALL SGS_algebraic_BC_diag (Lu_diag, nlocal, ne_local, jlev, meth)
  END SUBROUTINE user_algebraic_BC_diag

  SUBROUTINE user_algebraic_BC_rhs (rhs, ne_local, nlocal, jlev)
    !--Sets rhs for boundary conditions
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: ne_local, nlocal, jlev
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (INOUT) :: rhs


    INTEGER :: i, ie, ii, shift
    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc

    !BC for both 2D and 3D cases
    DO ie = 2, dim
       shift = nlocal*(ie-1)
       !--Go through all Boundary points that are specified
       i_p_face(0) = 1
       DO i=1,dim
          i_p_face(i) = i_p_face(i-1)*3
       END DO
       DO face_type = 0, 3**dim - 1
          face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
          IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
             CALL get_all_indices_by_face (face_type, jlev, nloc, iloc)
             IF(nloc > 0 ) THEN 
!                IF (ALL(ABS(face) == (/1, 0/) ) ) THEN !left and right faces
!                   rhs(shift+iloc(1:nloc)) = 0.0_pr  !Dirichlet conditions 
!                ELSE IF (ALL(ABS(face) == (/0, 1/) ) ) THEN !top and bottom faces
!                   rhs(shift+iloc(1:nloc)) = 0.0_pr    !Newman conditions 
!                END IF

!!!!Current B
                IF( face(dim) == 1  ) THEN                        ! top faces 
!                ELSE IF( face(dim) == 1  ) THEN                        ! top and bottom face
                   rhs(shift+iloc(1:nloc)) = 0.0_pr                 !Dirichlet conditions  !no slip
!                   rhs(shift+iloc(1:nloc)) = 0.0_pr                    !Newman conditions   !slip
                ELSE IF( face(dim) == -1  ) THEN                        ! bottom faces 
!                ELSE IF( face(dim) == 1  ) THEN                        ! top and bottom face
                   rhs(shift+iloc(1:nloc)) = 0.0_pr                 !Dirichlet conditions  !no slip
!                   rhs(shift+iloc(1:nloc)) = 0.0_pr                    !Newman conditions   !slip
                ELSE IF( ABS(face(1)) == 1  ) THEN                            ! E, W faces 
                   rhs(shift+iloc(1:nloc)) = 0.0_pr                 !Dirichlet conditions  !no slip
!                   rhs(shift+iloc(1:nloc)) = 0.0_pr                    !Neuman conditions  !shear-free
                ELSE IF( ABS(face(dim-1)) == 1  ) THEN                            ! N, S faces 
                   rhs(shift+iloc(1:nloc)) = 0.0_pr                 !Dirichlet conditions  !no slip

                END IF
             END IF
          END IF
       END DO
    END DO

    !default  SGS BC, can be explicitely deifined below instead of calling default BCs
    IF(sgsmodel /= 0) CALL SGS_algebraic_BC_rhs (rhs, ne_local, nlocal, jlev)
  END SUBROUTINE user_algebraic_BC_rhs

  SUBROUTINE user_project (u, p, nlocal, meth)
    !--Makes u divergence free
    USE penalization
    IMPLICIT NONE
    INTEGER, PARAMETER :: ne_local = 1
    INTEGER, INTENT (IN) :: meth, nlocal
    REAL (pr), DIMENSION (nlocal),     INTENT(INOUT) :: p
    REAL (pr), DIMENSION (nlocal,n_var), INTENT(INOUT) :: u
    REAL (pr), DIMENSION (nlocal,ne_local) :: dp
    REAL (pr), DIMENSION (nlocal,ne_local) :: f
    REAL (pr), DIMENSION(ne_local) :: scl_p
    INTEGER, DIMENSION(ne_local) :: clip 
    INTEGER :: face_type, nloc, meth_local
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc
    REAL (pr), DIMENSION (ng,dim+1) :: deltastar
    REAL (pr), DIMENSION (ng,1) :: etasave
    REAL (pr), DIMENSION (dim,ng,dim) :: du, d2u
    REAL (pr), DIMENSION (ng,dim+1) :: u_prev
    INTEGER :: ie, shift, i
   
    DO ie = 1, dim
       shift=(ie-1)*ng
       u_prev(1:nwlt,ie) = u_prev_timestep(shift+1:shift+ng)   !eta, u and v prev
    END DO

    u_prev(:, dim+1) = w_correction(1:ng)    !w_prev

    CALL vert_integ_w (u(:, 2:dim), u(:, n_var_U(dim)))

    etasave(:, 1) = u(:, n_var_eta)  !eta old , eta*
    u(:, n_var_eta) = u(:, n_var_eta) + (dt*(delta_i**2)/2.0_pr)*(u(:, n_var_U(dim)) - w_correction(1:nwlt))   ! eta = eta* + (dt/2)*(w* - w_n)

    CALL vert_integ_eta (u(:, n_var_eta))

    u_prev(:, n_var_eta) = u(:, n_var_eta) - etasave(:, 1)

    CALL c_diff_fast(u_prev(:, 1:dim), du(1:dim, :, :), d2u, j_lev, ng, HIGH_ORDER, 10, dim, 1, dim) !need dun/dz

    DO i=2, (dim-1)
       u(:, i) = u(:, i) + (dt/2.0_pr)*(-(delta_i**2/Fr**2)*du(n_var_eta, :, i-1) - (delta_i**2)*(u(:, n_var_U(dim)) - w_correction(1:ng) )*du(i, :, dim))    !u_new = u* - (dt/2Fr^2)*(eta_new_cor - eta*) + (1/2)*(deltaw*)(du*/dz)
    END DO

    CALL vert_integ_w (u(:, 2:dim), u(:, n_var_U(dim)))    


  END SUBROUTINE user_project

!!$  FUNCTION Laplace (jlev, u, nlocal, ne_local, meth_order)
!!$    IMPLICIT NONE
!!$    INTEGER, INTENT (IN) :: jlev, nlocal, ne_local, meth_order
!!$    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (INOUT) :: u
!!$    REAL (pr), DIMENSION (nlocal*ne_local) :: Laplace
!!$
!!$    INTEGER :: i, ii, ie, idim, shift
!!$    INTEGER :: meth_central, meth_backward, meth_forward 
!!$    REAL (pr), DIMENSION (ne_local, nlocal, dim) :: du
!!$    REAL (pr), DIMENSION (dim, nlocal, dim)      :: d2u
!!$    REAL (pr), DIMENSION (dim, nlocal, dim)      :: du_dummy ! passed when only calculating 1st derivative.
!!$    INTEGER :: face_type, nloc
!!$    INTEGER, DIMENSION(0:dim) :: i_p_face
!!$    INTEGER, DIMENSION(dim) :: face
!!$    INTEGER, DIMENSION(nwlt) :: iloc
!!$
!!$    meth_central  = meth_order + BIASING_NONE
!!$    meth_backward = meth_order + BIASING_BACKWARD
!!$    meth_forward  = meth_order + BIASING_FORWARD
!!$
!!$    !
!!$    ! Find 1st deriviative of u. 
!!$    !
!!$    CALL c_diff_fast (u, du, d2u(1:ne_local, 1:nlocal, 1:dim), jlev, nlocal, meth_backward, 10, ne_local, 1, ne_local)
!!$
!!$    DO ie = 1, ne_local
!!$       shift=(ie-1)*nlocal
!!$       !--- Internal points
!!$       CALL c_diff_fast(du(ie,1:nlocal,1:dim), d2u(1:dim, 1:nlocal, 1:dim), du_dummy(1:dim, 1:nlocal, 1:dim), &
!!$            jlev, nlocal, meth_forward, 10, dim, 1, dim )
!!$       !--- div(grad)
!!$       idim = 1
!!$       Laplace(shift+1:shift+Nwlt_lev(jlev,0)) = d2u( idim ,1:Nwlt_lev(jlev,0),idim)
!!$       DO idim = 2,dim
!!$          Laplace(shift+1:shift+Nwlt_lev(jlev,0)) = Laplace(shift+1:shift+Nwlt_lev(jlev,0)) + &
!!$               d2u(idim ,1:Nwlt_lev(jlev,0),idim)
!!$       END DO
!!$       !--Boundary points
!!$       i_p_face(0) = 1
!!$       DO i=1,dim
!!$          i_p_face(i) = i_p_face(i-1)*3
!!$       END DO
!!$       DO face_type = 0, 3**dim - 1
!!$          face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
!!$          IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
!!$             CALL get_all_indices_by_face (face_type, jlev, nloc, iloc)
!!$             IF(nloc > 0 ) THEN ! ANY dimension
!!$                IF (ALL(ABS(face) == (/1, 0/) ) ) THEN !left and right faces
!!$                   Laplace(shift+iloc(1:nloc)) = u(shift+iloc(1:nloc))  !Dirichlet conditions 
!!$                ELSE IF (ALL(ABS(face) == (/0, 1/) ) ) THEN !top and bottom faces
!!$                   Laplace(shift+iloc(1:nloc)) = du(ie,iloc(1:nloc),dim)    !Newman conditions 
!!$                END IF
!!$
!!$
!!$!                IF( face(1) == -1  ) THEN                          ! Xmin face (entire face) 
!!$!                   Laplace(shift+iloc(1:nloc)) = u(shift+iloc(1:nloc))  !Dirichlet conditions
!!$!                ELSE IF( face(1) == 1  ) THEN                      ! Xmax face (entire face) 
!                   Laplace(shift+iloc(1:nloc)) = u(shift+iloc(1:nloc))  !Dirichlet conditions
!!$!                   Laplace(shift+iloc(1:nloc)) = du(ie,iloc(1:nloc),1)  !Neuman conditions
!!$!                END IF
!!$             END IF
!!$          END IF
!!$       END DO
!!$    END DO
!!$  END FUNCTION Laplace
!!$
!!$  FUNCTION Laplace_diag (jlev,  nlocal, ne_local, meth_order)
!!$    IMPLICIT NONE
!!$    INTEGER, INTENT (IN) :: jlev, nlocal, ne_local, meth_order
!!$    REAL (pr), DIMENSION (nlocal*ne_local) :: Laplace_diag
!!$
!!$    INTEGER :: i, ii, ie, idim, shift
!!$    INTEGER :: meth_central, meth_backward, meth_forward 
!!$    REAL (pr), DIMENSION (nlocal,dim) :: du, d2u
!!$    INTEGER :: face_type, nloc
!!$    INTEGER, DIMENSION(0:dim) :: i_p_face
!!$    INTEGER, DIMENSION(dim) :: face
!!$    INTEGER, DIMENSION(nwlt) :: iloc
!!$
!!$    meth_central  = meth_order + BIASING_NONE
!!$    meth_backward = meth_order + BIASING_BACKWARD
!!$    meth_forward  = meth_order + BIASING_FORWARD
!!$
!!$    CALL c_diff_diag ( du, d2u, jlev, nlocal, meth_backward, meth_forward, -11)
!!$
!!$    !PRINT *,'IN Laplace_diag, ne_local = ', ne_local
!!$
!!$    DO ie = 1, ne_local
!!$       shift=(ie-1)*nlocal
!!$       !--- div(grad)
!!$       !PRINT *,'CAlling c_diff_diag from Laplace_diag() '
!!$       !PRINT *,'--- jlev, nlocal, meth_backward, meth_forward', jlev, nlocal, meth_backward, meth_forward
!!$
!!$       idim = 1
!!$       Laplace_diag(shift+1:shift+Nwlt_lev(jlev,0)) = d2u(1:Nwlt_lev(jlev,0),idim)
!!$       DO idim = 2,dim
!!$          Laplace_diag(shift+1:shift+Nwlt_lev(jlev,0)) = Laplace_diag(shift+1:shift+Nwlt_lev(jlev,0)) + &
!!$               d2u(1:Nwlt_lev(jlev,0),idim)
!!$       END DO
!!$       !--Boundary points
!!$       i_p_face(0) = 1
!!$       DO i=1,dim
!!$          i_p_face(i) = i_p_face(i-1)*3
!!$       END DO
!!$       DO face_type = 0, 3**dim - 1
!!$          face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
!!$          IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
!!$             CALL get_all_indices_by_face (face_type, jlev, nloc, iloc)
!!$             IF(nloc > 0 ) THEN ! ANY dimension
!!$                IF (ALL(ABS(face) == (/1, 0/) ) ) THEN !left and right faces
!!$                   Laplace_diag(shift+iloc(1:nloc)) = 1.0_pr  !Dirichlet conditions 
!!$                ELSE IF (ALL(ABS(face) == (/0, 1/) ) ) THEN !top and bottom faces
!!$                   Laplace_diag(shift+iloc(1:nloc)) = du(iloc(1:nloc),dim)    !Newman conditions !!$                END IF
!!$
!!$!                IF( face(1) == -1  ) THEN                          ! Xmin face (entire face) 
!!$!                   Laplace_diag(shift+iloc(1:nloc)) = 1.0_pr                 !Dirichlet conditions
!!$!                ELSE IF( face(1) == 1  ) THEN                      ! Xmax face (entire face) 
 !                  Laplace_diag(shift+iloc(1:nloc)) = 1.0_pr                 !Dirichlet conditions
!!$!                   Laplace_diag(shift+iloc(1:nloc)) = du(iloc(1:nloc),1)     !Neuman conditions
!!$!                END IF
!!$             END IF
!!$          END IF
!!$       END DO
!!$    END DO
!!$
!!$   END FUNCTION Laplace_diag
!!$
!!$  FUNCTION Laplace_rhs(u, nlocal, ne_local, meth_order)
!!$    IMPLICIT NONE
!!$    INTEGER, INTENT (IN) :: nlocal, ne_local, meth_order
!!$    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (INOUT) :: u
!!$    REAL (pr), DIMENSION (nlocal,ne_local) :: Laplace_rhs
!!$
!!$    INTEGER :: i, ii, ie, meth, idim
!!$    INTEGER :: meth_central, meth_backward, meth_forward 
!!$    REAL (pr), DIMENSION (ne_local ,nlocal,dim) :: du
!!$    REAL (pr), DIMENSION (ne_local*dim ,nlocal,dim) :: d2u
!!$    REAL (pr), DIMENSION (ne_local*dim ,nlocal,dim) :: du_dummy ! passed when only calculating 1st derivative.
!!$    INTEGER :: face_type, nloc
!!$    INTEGER, DIMENSION(0:dim) :: i_p_face
!!$    INTEGER, DIMENSION(dim) :: face
!!$    INTEGER, DIMENSION(nwlt) :: iloc
!!$
!!$    meth_central  = meth_order + BIASING_NONE
!!$    meth_backward = meth_order + BIASING_BACKWARD
!!$    meth_forward  = meth_order + BIASING_FORWARD
!!$
!!$    Laplace_rhs(:,1) = div(u,nlocal,j_lev,meth_forward)
!!$
!!$    !--Boundary points
!!$    i_p_face(0) = 1
!!$    DO i=1,dim
!!$       i_p_face(i) = i_p_face(i-1)*3
!!$    END DO
!!$    DO face_type = 0, 3**dim - 1
!!$       face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
!!$       IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
!!$          CALL get_all_indices_by_face (face_type, j_lev, nloc, iloc)
!!$          IF(nloc > 0 ) THEN 
!!$
!!$                IF (ALL(ABS(face) == (/1, 0/) ) ) THEN !left and right faces
!!$                   Laplace_rhs(iloc(1:nloc), 1) = 0.0_pr  !Dirichlet conditions 
!!$                ELSE IF (ALL(ABS(face) == (/0, 1/) ) ) THEN !top and bottom faces
!!$                   Laplace_rhs(iloc(1:nloc), 1) = 0.0_pr    !Newman conditions 
!!$                END IF
!!$
!!$!             IF( face(1) == -1  ) THEN                          ! Xmin face (entire face) 
!!$!                Laplace_rhs(iloc(1:nloc),1) = 0.0_pr  !Dirichlet conditions
!!$!             ELSE IF( face(1) == 1  ) THEN                      ! Xmax face (entire face) 
 !               Laplace_rhs(iloc(1:nloc),1) = 0.0_pr !Dirichlet conditions
!!$!                Laplace_rhs(iloc(1:nloc),1) = 0.0_pr !Neuman conditions
!!$!             END IF
!!$          END IF
!!$       END IF
!!$    END DO
!!$    
!!$  END FUNCTION Laplace_rhs


  FUNCTION user_rhs (u_integrated,p)
    USE penalization
    IMPLICIT NONE
    REAL (pr), DIMENSION (ng,ne), INTENT(IN) :: u_integrated !1D version of integrated variables without BC 
    REAL (pr), DIMENSION (ng), INTENT(IN) :: p
    REAL (pr), DIMENSION (n) :: user_rhs

    INTEGER :: ie, ie1, shift, i
    INTEGER, PARAMETER :: meth=1
    REAL (pr), DIMENSION (dim+1,ng,dim) :: du, d2u
    REAL (pr), DIMENSION (ng,dim+2)     :: V   
    
    V(:,dim+1) = (u_integrated(:, n_var_eta) - x(:, dim))!/Fr**2    ! p
    V(:,dim+2) =  w_correction(1:ng)   ! w_c
    V(:,1:dim-1) = u_integrated(:,2:dim) ! u and v
    V(:,dim) = u_integrated(:, 1)   ! eta

    CALL c_diff_fast(V(:,1:dim+1), du(1:(dim+1), :, :), d2u(1:(dim+1), :, :), j_lev, ng, HIGH_ORDER, 11, dim+1, 1, dim+1) !derivatives are calculated for u, v, eta, and p

    V(:,dim) = V(:,dim+2)     ! w
 
    !--Form right hand side of equations

    user_rhs = 0.0_pr

    !eta evolution equation 

    DO ie = 1,(dim-1)
       user_rhs(1:ng) = user_rhs(1:ng) - (delta_i**2)*V(:, ie)*du(dim, :, ie)    !  - ui . d(eta)/dxi 
    END DO
    
    user_rhs(1:ng) = user_rhs(1:ng) + (delta_i**2)*V(:, dim) ! + w      
  
   !xi-momentum equations

    DO ie = 1, (dim-1)
       shift=(ie)*ng
       user_rhs(shift+1:shift+ng) = user_rhs(shift+1:shift+ng) - (delta_i**2/Fr**2)*du(dim+1, :, ie)       !  - dp/dxj  
       DO ie1 = 1,dim  
          user_rhs(shift+1:shift+ng) = user_rhs(shift+1:shift+ng) - (delta_i**2)*V(:,ie1)*du(ie,:,ie1)     !  - ui*duj/dxi  

          IF (ie1 .lt. dim) THEN
             user_rhs(shift+1:shift+ng) = user_rhs(shift+1:shift+ng) + (delta_m**3)*d2u(ie, :,ie1)   ! + (1/Re)*d2uj/dxi2 (only in x and y)
          ELSE
             user_rhs(shift+1:shift+ng) = user_rhs(shift+1:shift+ng) + (delta_m**3)*(A_LH**2)*(nu_ratio)*d2u(ie, :, ie1)    ! + (1/Re)*A^2*(nuv/nuh)*d2uj/dz2
          ENDIF
         
       END DO
       !Brinkman-Bottom Bathymetry
       IF (imask_obstacle) THEN
          user_rhs(shift+1:shift+ng) = user_rhs(shift+1:shift+ng)*(1.0_pr - penal) - (kappa1*V(:,ie))*penal/eta_pen  &
                                                                                + (- kappa2*du(ie, :, dim) + kappa3*d2u(ie, :, dim))*penal/eta_pen     !  - chi*u/eta_pen
!                                                                                 + (- (kappa2**2)*V(:,ie) + kappa3*d2u(ie, :, dim))*penal/eta_pen     !  - chi*u/eta_pen
          DO ie1 = 1,(dim-1)
             user_rhs(shift+1:shift+ng) = user_rhs(shift+1:shift+ng) + kappa4*d2u(ie, :, ie1)*penal/eta_pen     !  - chi*u/eta_pen
          END DO
       ENDIF
       !Brinkman-Continental boundaries
       IF(continent_active) THEN
          user_rhs(shift+1:shift+ng) = user_rhs(shift+1:shift+ng) - V(:,ie)*chi_continent/eta_pen     !  - chi*u/eta_pen
       END IF
       !Coriolis
       IF (dim==3) THEN
          IF (ie==1) THEN
             user_rhs(shift+1:shift+ng) = user_rhs(shift+1:shift+ng) + ((1.0_pr/Pl)+x(:, 2))*V(:, 2)     !  +fv
          ELSEIF (ie==2) THEN
             user_rhs(shift+1:shift+ng) = user_rhs(shift+1:shift+ng) - ((1.0_pr/Pl)+x(:, 2))*V(:, 1)     !  -fu
          ENDIF
       ENDIF

    END DO

    !Wind   
   ! IF (dim==2) THEN
    !   user_rhs(ng+1:ng+ng) = user_rhs(ng+1:ng+ng) !+ amp_wind*exp(-((x(1:ng,dim)-xyzlimits(2,dim))/delta_wind)**2)   ! 
    IF (dim==3) THEN
       !DO ie=1,(1)
          shift=(1)*ng
          user_rhs(shift+1:shift+ng) = user_rhs(shift+1:shift+ng) - (1.0_pr/PI)*COS(PI*x(:, 2))*exp(-((x(1:ng,dim)-xyzlimits(2,dim))/delta_wind)**2)   ! + amp_wind*cos(2pix)*exp(-a*z^2), Wind forcing term, added to x-momentum
       !ENDDO
    ENDIF

   IF(sgsmodel /= 0) CALL SGS_rhs (user_rhs, u_integrated, du, d2u)

  END FUNCTION user_rhs



  FUNCTION user_Drhs (u, u_prev, meth)
    USE penalization
    IMPLICIT NONE
    INTEGER, INTENT(IN) ::  meth
    !u_prev_timestep passed local to cast it into 2dim array to work with vector derivatives
    REAL (pr), DIMENSION (ng,ne) :: u, u_prev
    REAL (pr), DIMENSION (n) :: user_Drhs

    INTEGER :: ie, ie1, shift
    REAL (pr), DIMENSION (dim+1,ng,dim) :: du, d2u
    REAL (pr), DIMENSION (dim+1,ng,dim) :: du_prev
    REAL (pr), DIMENSION (ng,dim+1)     :: V_prev
    REAL (pr), DIMENSION (ng,dim+2)     :: V


    V_prev(:,1:dim-1) = u_prev(:,2:dim)            ! u and v 
    V_prev(:,dim) = w_correction(1:ng)     ! w
    V_prev(:, dim+1) = u_prev(:,1)                ! eta
    
    CALL c_diff_fast(V_prev(:,1:dim+1), du_prev, d2u, j_lev, ng, meth, 10, dim+1, 1, dim+1)  !du_prev/dx1 and dv_prev/dxi calculated
    
    V(:,dim+1) = u(:, n_var_eta) !*delta_i**2/Fr**2       ! p'
    V(:,dim+2) = 0.0_pr        ! w' = 0 for corrected w
    V(:,1:dim-1) = u(:,2:dim) ! u' and v'
    V(:,dim) = u(:, 1)     ! eta'
    

    CALL c_diff_fast(V(:,1:dim+1), du, d2u, j_lev, ng, meth, 11, dim+1, 1, dim+1) !derivatives are calculated for perturbation terms
    V(:,dim) = V(:,dim+2)     ! move w

    !--Form D-right hand side of HPE
    
    user_Drhs = 0.0_pr
    
    ! eta evolution equation

    DO ie = 1,(dim-1)
       user_Drhs(1:ng) = user_Drhs(1:ng) - (delta_i**2)*(V_prev(:, ie)*du(dim, :, ie) - V(:, ie)*du_prev(dim+1, :, ie))      ! - ubi deta'/dxi + u'i detab/dxi
    END DO
    user_Drhs(1:ng) = user_Drhs(1:ng) + (delta_i**2)*V(:,dim)  ! + w'

    ! U and V momentum equations

    DO ie = 1, (dim-1)
       shift=(ie)*ng
       user_Drhs(shift+1:shift+ng) = user_Drhs(shift+1:shift+ng) - ((delta_i**2)/(Fr**2))*du(dim+1,:,ie)    ! - dp/dxi  
       DO ie1 = 1,dim  
          user_Drhs(shift+1:shift+ng) = user_Drhs(shift+1:shift+ng) - (delta_i**2)*V(:,ie1)*du_prev(ie,:,ie1) & ! - uj' dui_prev/dxj
                                                                    - (delta_i**2)*V_prev(:,ie1)*du(ie,:,ie1)   ! - uj_prev dui'/dxj
          IF (ie1 .lt. dim) THEN
            user_Drhs(shift+1:shift+ng) = user_Drhs(shift+1:shift+ng) + (delta_m**3)*d2u(ie, :,ie1)   ! + (1/Re)*d2uj'/dxi2 (only in x and y)
          ELSE
            user_Drhs(shift+1:shift+ng) = user_Drhs(shift+1:shift+ng) + (delta_m**3)*(A_LH**2)*(nu_ratio)*d2u(ie, :, ie1)    ! + (1/Re)*A^2*(nuv/nuh)*d2uj'/dz2
          END IF
      END DO
      !Brinkman-Bottom Bathymetry
      IF (imask_obstacle) THEN
         user_Drhs(shift+1:shift+ng) = user_Drhs(shift+1:shift+ng)*(1.0_pr - penal) - (kappa1*V(:,ie) )*penal/eta_pen &
                                                                                  + (- kappa2*du(ie, :, dim) + kappa3*d2u(ie, :, dim))*penal/eta_pen     !  - chi*u/eta_pen
  !                                                                                  + (- (kappa2**2)*V(:, ie) + kappa3*d2u(ie, :, dim))*penal/eta_pen     !  - chi*u/eta_pen
         DO ie1 = 1,(dim-1)
            user_Drhs(shift+1:shift+ng) = user_Drhs(shift+1:shift+ng) + kappa4*d2u(ie, :, ie1)*penal/eta_pen     !  - chi*u/eta_pen
         END DO
      ENDIF
      !Brinkman-Continental Boundaries
      IF(continent_active) THEN
            user_Drhs(shift+1:shift+ng) = user_Drhs(shift+1:shift+ng) - V(:,ie)*chi_continent/eta_pen     !  - chi*u/eta_pen
      END IF
      !Coriolis
      IF (dim==3) THEN
         IF (ie==1) THEN
            user_Drhs(shift+1:shift+ng) = user_Drhs(shift+1:shift+ng) + ((1.0_pr/Pl)+x(:, 2))*V(:, 2)     !  +fv
         ELSEIF (ie==2) THEN
            user_Drhs(shift+1:shift+ng) = user_Drhs(shift+1:shift+ng) - ((1.0_pr/Pl)+x(:, 2))*V(:, 1)     !  -fu
         ENDIF
      ENDIF

   END DO

   IF(sgsmodel /= 0) CALL SGS_Drhs (user_Drhs, u, u_prev, du, d2u, meth)


 END FUNCTION user_Drhs


  !
  ! Uses u_prev_timestep, which is global flat (1D ) array set in time_step_cn()
  !
  FUNCTION user_Drhs_diag (meth)
    USE penalization
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: meth
    REAL (pr), DIMENSION (n) :: user_Drhs_diag

    INTEGER :: ie, ie1, shift,shiftIlm,shiftImm
    REAL (pr), DIMENSION (ng,dim) :: du, d2u
    REAL (pr), DIMENSION (dim-1,ng,dim) :: du_prev_timestep
    REAL (pr), DIMENSION (dim-1,ng,dim) :: du_dummy ! passed when only calculating 1st derivative.
    REAL (pr), DIMENSION (ng,dim)     :: V_prev

    V_prev(:,dim) = w_correction(1:ng)    ! w

    DO ie = 1, (dim-1)
       shift=(ie)*ng
       V_prev(:,ie) = u_prev_timestep(shift+1:shift+ng)  ! u, v 
    END DO

    CALL c_diff_fast(V_prev(:,1:dim-1), du_prev_timestep, du_dummy, j_lev, ng, meth, 10, dim-1, 1, dim-1)


    CALL c_diff_diag(du, d2u, j_lev, ng, meth, meth, 11)


    !--Form D-right hand side diag of HPE
    user_Drhs_diag = 0.0_pr

    ! eta evolution equation    

    DO ie = 1,(dim-1)
       user_Drhs_diag(1:ng) = - (delta_i**2)*du(:,ie)*V_prev(:,ie)  ! - ui_prev*w_xi
    END DO

    
    ! xi momentum equation 

    DO ie = 1,(dim-1)
       shift=(ie)*ng
       DO ie1 = 1, dim
          user_Drhs_diag(shift+1:shift+ng) = user_Drhs_diag(shift+1:shift+ng) - (delta_i**2)*V_prev(:,ie1)*du(:,ie1)         ! - ui_prev*w_xi
          IF (ie1 .lt. dim) THEN
             user_Drhs_diag(shift+1:shift+ng) = user_Drhs_diag(shift+1:shift+ng) + (delta_m**3)*d2u(:,ie1)   ! + (1/Re)*dw_xi^2 (only in x and y)
          ELSE
             user_Drhs_diag(shift+1:shift+ng) = user_Drhs_diag(shift+1:shift+ng) + (delta_m**3)*(A_LH**2)*(nu_ratio)*d2u(:, ie1)    ! + (1/Re)*A^2*(nuv/nuh)*w_z^2
          END IF
       END DO
       user_Drhs_diag(shift+1:shift+ng) = user_Drhs_diag(shift+1:shift+ng) - (delta_i**2)*du_prev_timestep(ie,:,ie)         ! - dui_prev/dxi 
       !Brinkman-Bottom Bathymetry
       IF (imask_obstacle) THEN
          user_Drhs_diag(shift+1:shift+ng) = user_Drhs_diag(shift+1:shift+ng)*(1.0-pr - penal) - (kappa1*1.0_pr)*penal/eta_pen &
                                                                                           +  (- kappa2*du(:, dim) + kappa3*d2u(:, dim))*penal/eta_pen     !  - chi*u/eta_pen
!                                                                                             + (- (kappa2**2)*1.0_pr + kappa3*d2u(:, dim))*penal/eta_pen     !  - chi*u/eta_pen
          DO ie1 = 1,(dim-1)
             user_Drhs_diag(shift+1:shift+ng) = user_Drhs_diag(shift+1:shift+ng)+ kappa4*d2u(:, ie1)*penal/eta_pen     !  - chi*u/eta_pen
          END DO
       ENDIF
       !Brinkman-Continental Boundaries
       IF(continent_active) THEN
          user_Drhs_diag(shift+1:shift+ng) = user_Drhs_diag(shift+1:shift+ng) - chi_continent/eta_pen     !  - chi*u/eta_pen
       END IF
    END DO

    IF(sgsmodel /= 0) CALL SGS_Drhs_diag (user_Drhs_diag, du_prev_timestep, du, d2u, meth)

  END FUNCTION user_Drhs_diag

  FUNCTION user_chi (nlocal, t_local)
    IMPLICIT NONE
    INTEGER,          INTENT (IN) :: nlocal
    REAL (pr),        INTENT (IN) :: t_local
    REAL (pr), DIMENSION (nlocal) :: user_chi
    !--Defines mask for sphere (3d) or cylinder (2d)

!    user_chi = (1.0_pr - tanh((x(:, dim) - (0.1_pr*exp(-100_pr*(x(:, 1)-0.5_pr)**2) - 1.0_pr))/delta_pen))/2.0_pr   !gaussian bottom
!    user_chi = (1.0_pr - tanh((x(:, dim) - (0.05_pr*cos(2.0_pr*pi*x(:, 1)) - 1.0_pr))/delta_pen))/2.0_pr   !sinusoidal bottom
    user_chi = (1.0_pr - tanh((x(:, dim) - 0.0_pr)/delta_pen))/2.0_pr   !flat bottom
!    user_chi = (1.0_pr + tanh((x(:, 1)**2 + x(:, dim)**2 - 0.5_pr**2)/delta_pen))/2.0_pr   !spherical bottom

!     user_chi = 0.0_pr !0.5_pr*(1.0_pr+SIGN(1.0_pr, R_obst**2-SUM(x**2,DIM=2)))

  END FUNCTION user_chi

  !
  ! Calculate any statitics
  !
  ! startup_flag - 0 when adapting to IC,then 1 inmain integration loop
  !
  !
  SUBROUTINE user_stats (u , j_mn, startup_flag)
!    USE misc_vars
!    USE fft_module
!    USE spectra_module
    USE wlt_vars
    USE vector_util_mod
    IMPLICIT NONE
    REAL (pr), DIMENSION (nwlt,1:n_var), INTENT (IN) :: u
    INTEGER , INTENT (IN) :: j_mn 
    INTEGER , INTENT (IN) :: startup_flag
    CHARACTER (LEN=256)  :: filename

    !================= OBSTACLE ====================
    REAL (pr)                       :: drag, lift
    REAL (pr), DIMENSION (dim)      :: force
    !==============================================
    
    !USER may define additional statistics ouutput here.

!    WRITE(*,'(" ")')
!    WRITE(*,'("****************** Turbulence Statistics on the Fly *******************")')

!    IF (imask_obstacle) THEN !--Calculate force
!       CALL user_cal_force (u(:,1:dim), nwlt, t, force, drag, lift)
!       WRITE (6,'("Drag = ", es9.2, 1x, "Lift = ", es9.2)') drag, lift
!       OPEN  (11, FILE = 'results/'//TRIM(file_gen)//'_drag', FORM='formatted', STATUS='old', POSITION='append')
!       WRITE (11,'(3(es15.8,1x))') t, drag, lift
!       CLOSE (11)
!    END IF

!    WRITE(*,'("***********************************************************************")')
!    WRITE(*,'(" ")')

  END SUBROUTINE user_stats

  SUBROUTINE user_cal_force (u, n, t_local, force, drag, lift)
    !--Calculates drag and lift on obstacle using penalization formula
    USE penalization
    IMPLICIT NONE

    INTEGER, INTENT (IN) :: n
    INTEGER :: ie
    REAL (pr), INTENT (IN) :: t_local
    REAL (pr), DIMENSION (dim), INTENT (INOUT) :: force
    REAL (pr), INTENT (OUT) :: drag, lift
    REAL (pr), DIMENSION (n,dim) :: u
    REAL (pr) :: A

!    IF(dim == 3) THEN
!       A = pi * R_obst**2
!    ELSE IF(dim == 2) THEN
!       A = 2.0_pr* R_obst
!    ELSE
!       A = 1.0_pr
!    END IF
!   
!    DO ie = 1, dim
!       force(ie) = SUM(penal/eta_chi*(u(:,ie)+Umn(ie))*dA) / (0.5_pr * A * SUM(Umn(1:dim)**2))
!    END DO

!    drag = force(1) ; lift = force(2)
drag = 0.0_pr
lift = 0.0_pr
    
  END SUBROUTINE user_cal_force

  !
  ! calculate any additional variables
  ! 
  ! arg 
  ! flag  - 0 calledwhile adapting to IC, 1 - called in main time integration loop
  !
  ! These additiona variables are calculated and left in real space.
  !
  SUBROUTINE user_additional_vars( t_local, flag )
    IMPLICIT NONE
    REAL (pr), INTENT (IN) ::  t_local
    INTEGER , INTENT(IN) :: flag ! 0- called during adaption to IC, 1 called during main integration loop
    INTEGER :: i
    LOGICAL, SAVE :: init_status = .FALSE.
    REAL (pr), DIMENSION (1, ng,dim) :: du, d2u

     
   
    IF(NOT(init_status)) THEN
       init_status=.TRUE.
!  Initialize variables with analytic solution
!       u(:, n_var_pressure) = (u(:, n_var_eta) - x(:, dim))/Fr**2  !0.001_pr*COS(2*PI*x(:, 1))
!       u(:, n_var_U(dim)) =  (0.001_pr*wavenumber**2/Re)*(-0.5_pr*COS(-wavenumber*x(:, 1)) - SQRT((Re/wavenumber/Fr)**2 - 0.25_pr)*SIN(-wavenumber*x(:, 1)))*(x(:, dim)+1.0_pr)

!  Initialize variables with zero solutions
       u(:,n_var_pressure) = 0.0e-10_pr !u(:, n_var_eta)/Fr**2
       u(:,n_var_U(dim)) =  0.0_pr
    END IF

! Allocate additional variables
    u(:, n_var_pressure) = (u(:, n_var_eta) - x(:, dim))/Fr**2  !0.001_pr*COS(2*PI*x(:, 1))
    u(:, n_var_U(dim)) = u(:, n_var_U(dim))
    !u(:, n_var_U(dim)) = 0.0_pr
    u(:, n_var_mask) = user_chi(nwlt, t_local) 
    !u(:, n_var_eta) = 0.0_pr


  END SUBROUTINE user_additional_vars


  !
  ! calculate any additional scalar variables
  ! 
  ! arg 
  ! flag  - 0 calledwhile adapting to IC, 1 - called in main time integration loop
  !
  SUBROUTINE user_scalar_vars( flag )
    IMPLICIT NONE
    INTEGER , INTENT(IN) :: flag ! 0- called during adaption to IC, 1 called during main integration loop



  END SUBROUTINE user_scalar_vars

  !
  !************ Calculating Scales ***************************
  !
  ! Note the order of the components in the scl array
  ! correspond to u_tn, v_tn, w_tn, u_tn-1, v_tn-1, w_tn-1, u_tn-2, v_tn-2, w_tn-2
  ! 
  SUBROUTINE user_scales(flag, use_default, u_loc, nlocal, ne_local, l_n_var_adapt , l_n_var_adapt_index, &
       scl, scl_fltwt) !add
    USE parallel
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: flag ! 0 during initial adaptation, then 1 during time advancement
    LOGICAL , INTENT(INOUT) :: use_default
    INTEGER, INTENT (IN) :: nlocal, ne_local
    REAL (pr), DIMENSION (1:nlocal,1:ne_local), INTENT (IN) :: u_loc
    LOGICAL , INTENT (IN) :: l_n_var_adapt(ne_local)
    INTEGER , INTENT (IN) :: l_n_var_adapt_index(1:ne_local)
    REAL (pr), DIMENSION (1:ne_local), INTENT (INOUT) :: scl
    REAL (pr), DIMENSION (1:ne_local):: mean
    REAL (pr) ,  INTENT(IN) ::scl_fltwt !weight for temporal filter on scl
    REAL (pr) :: floor, tmp
    INTEGER :: ie, ie_index, itmp
    REAL (pr) ,save, ALLOCATABLE, DIMENSION (:) :: scl_old
    LOGICAL  ,save :: startup_init = .TRUE.

    !
    ! Ignore the output of this routine and use default scales routine
    !
    use_default = .FALSE. 

    !
    ! NOTE: For a parallel run, synchronize scl(:) across the processors in user_scales.
    !       Use the subroutine scales of default_util.f90 as an example,
    !       subroutine parallel_global_sum of parallel.f90 may be helpful6
    !       Already synchronized: sumdA_global
    !
    IF( .NOT. use_default ) THEN

       !IF (par_rank.EQ.0) PRINT *,'Use user_scales() <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<'
       !
       ! ALLOCATE scl_old if it was not done previously in user_scales
       !
       IF( startup_init .AND. .NOT. ALLOCATED(scl_old) ) THEN
          ALLOCATE( scl_old(1:ne_local) )
          scl_old = 0.0_pr
       END IF
       
       floor = 1.0e-12_pr
       scl   =1.0_pr
       !scl(n_var_U(dim)) = 0.0_pr
       !
       ! Calculate scl per component
       !
      !Scale_Meth=2
      mean = 0.0_pr
      IF (.TRUE.) THEN
       DO ie=1,ne_local
          IF(l_n_var_adapt(ie)) THEN
             ie_index = l_n_var_adapt_index(ie)
             IF( Scale_Meth == 1 ) THEN ! Use Linf scale
                mean(ie)= 0.5_pr* ( MAXVAL ( u_loc(1:nlocal,ie_index)) - MINVAL ( u_loc(1:nlocal, ie_index)) )
                CALL parallel_global_sum( REALMAXVAL=mean(ie) )
                
             ELSE IF ( Scale_Meth == 2 ) THEN ! Use L2 scale
                mean(ie)= SUM( (u_loc(1:nlocal,ie_index)**2) )
                itmp = nlocal
                CALL parallel_global_sum( REAL=mean(ie) )
                CALL parallel_global_sum( INTEGER=itmp )
                mean(ie) = SQRT ( mean(ie)/itmp  )
                
             ELSE IF ( Scale_Meth == 3 ) THEN ! Use L2 scale using dA
                mean(ie)= SUM( (u_loc(1:nlocal,ie_index)**2)*dA )
                CALL parallel_global_sum( REAL=mean(ie) )
                mean(ie)= mean(ie) / sumdA_global
             ELSE
                IF (par_rank.EQ.0) THEN
                   !PRINT *, 'Error in Scales(), Unknown Scale_Meth type: ', Scale_Meth
                   !PRINT *, 'Known Scale_Meth types: 1 = Linf scale, 2 = L2 scale '
                   !PRINT *, 'Exiting ...'
                END IF
                CALL parallel_finalize; STOP
             END IF
             
             IF (par_rank.EQ.0) THEN
                !WRITE (6,'("Scaling before taking vector(1:dim) magnitude")')
                !WRITE (6,'("Scale factor eqn", I4 ," = ", es11.4 ," scalecoeff = ",es11.4 )') &
                !     ie, scl(ie), scaleCoeff(ie)             
             END IF
          END IF
       END DO
      END IF
       DO ie=1,ne_local
          IF(l_n_var_adapt(ie)) THEN
             ie_index = l_n_var_adapt_index(ie)
             IF( Scale_Meth == 1 ) THEN ! Use Linf scale
                scl(ie)= MAXVAL ( ABS( u_loc(1:nlocal,ie_index) - mean(ie) ) )
                CALL parallel_global_sum( REALMAXVAL=scl(ie) )
                
             ELSE IF ( Scale_Meth == 2 ) THEN ! Use L2 scale
                scl(ie)= SUM( ((u_loc(1:nlocal,ie_index)-mean(ie))**2) )
                itmp = nlocal
                CALL parallel_global_sum( REAL=scl(ie) )
                CALL parallel_global_sum( INTEGER=itmp )
                scl(ie) = SQRT ( scl(ie)/itmp  )
                
             ELSE IF ( Scale_Meth == 3 ) THEN ! Use L2 scale using dA
                scl(ie)= SUM( ((u_loc(1:nlocal,ie_index)-mean(ie))**2)*dA )
                CALL parallel_global_sum( REAL=scl(ie) )
                scl(ie)= SQRT ( scl(ie) / sumdA_global  )
                
             ELSE
                IF (par_rank.EQ.0) THEN
                   !PRINT *, 'Error in Scales(), Unknown Scale_Meth type: ', Scale_Meth
                   !PRINT *, 'Known Scale_Meth types: 1 = Linf scale, 2 = L2 scale '
                   !PRINT *, 'Exiting ...'
                END IF
                CALL parallel_finalize; STOP
             END IF
             
             IF (par_rank.EQ.0) THEN
                !WRITE (6,'("Scaling before taking vector(1:dim) magnitude")')
                !WRITE (6,'("Scale factor eqn", I4 ," = ", es11.4 ," scalecoeff = ",es11.4 )') &
                !     ie, scl(ie), scaleCoeff(ie)             
             END IF
          END IF
       END DO
!PRINT *, 'check scl here', scl, 'something', l_n_var_adapt
!PAUSE
       !
      IF (.TRUE.) THEN
       ! take appropriate vector norm over scl(1:dim)
       IF( Scale_Meth == 1 ) THEN ! Use Linf scale
          tmp = MAXVAL(scl(n_var_U(1:dim))) !this is statistically equivalent to velocity vector length
          scl(n_var_U(1:dim)) = tmp
       ELSE IF ( Scale_Meth == 2 ) THEN ! Use L2 scale
          tmp = SQRT(SUM( scl(n_var_U(1:dim))**2 ) ) !now velocity vector length, rather than individual component
          scl(n_var_U(1:dim)) = tmp
       ELSE IF ( Scale_Meth == 3 ) THEN ! Use L2 scale using dA
          tmp = SQRT(SUM( scl(n_var_U(1:dim))**2 )) !now velocity vector length, rather than individual component
          scl(n_var_U(1:dim)) = tmp
       END IF
      END IF
!PRINT *,'check scl indices', scl(n_var_U(1:dim)), n_var_U(1:dim)
!STOP
!       scl(n_var_U(dim)) = tmp
!       scl(2:dim) = 1.0_pr
!PRINT *, 'SCL = ', scl, 'Scale_meth', Scale_Meth
!PAUSE
       !
       ! Print out new scl
       !
       DO ie=1,ne_local
          IF(l_n_var_adapt(ie)) THEN
             !this is done if one of the variable is exactly zero inorder not to adapt to the nois
             IF(scl(ie) .le. floor) scl(ie)=1.0_pr 
             tmp = scl(ie)
             ! temporally filter scl
             IF(.not. startup_init ) scl(ie) = scl_fltwt * scl_old(ie) + (1.0_pr - scl_fltwt) * scl(ie)
             scl = scaleCoeff * scl
             !WRITE (6,'("Scaling on vector(1:dim) magnitude")')
             !WRITE (6,'("Scale factor eqn", I4 ," = ", es11.4 ," scalecoeff = ",es11.4 )') &
             !     ie, scl(ie), scaleCoeff(ie)
             !WRITE (6,'("(before filtering = ",es11.4,")" )') tmp
          END IF
       END DO
       
       scl_old = scl !save scl for this time step
       startup_init = .FALSE.
!       PRINT *,'TEST scl_old ', scl_old
!       PAUSE
    END IF ! use default
    
  END SUBROUTINE user_scales

  SUBROUTINE user_cal_cfl (use_default, u, cfl_out)
    USE parallel
    IMPLICIT NONE
    LOGICAL , INTENT(INOUT) :: use_default
    REAL (pr),                                INTENT (INOUT) :: cfl_out
    REAL (pr), DIMENSION (nwlt,n_integrated), INTENT (IN)    :: u
    
    INTEGER                    :: i
    REAL (pr)                  :: floor
    REAL (pr), DIMENSION (dim) :: cfl, cfl_grav
    REAL (pr), DIMENSION(dim,nwlt) :: h_arr
    INTEGER :: mynwlt_global

    use_default = .FALSE.
    
    floor = 1e-12_pr
    cfl_out = floor
    
    CALL get_all_local_h (h_arr)
    
    DO i = 1, nwlt
       !cfl(1:dim) = ABS ( (delta_i**2)*u(i,1:dim)) * dt/h_arr(1:dim,i)  !convective CFL
       cfl_grav(1:dim) = ABS ((delta_i**2)*u(i,1:dim)+(delta_i**2)/Fr) * dt/h_arr(1:dim,i)  !gravity CFL  c=sqrt(gH)/U 
       cfl_out = MAX (cfl_out, MAXVAL(cfl_grav))
    END DO
    
    CALL parallel_global_sum( REALMAXVAL=cfl_out )

    mynwlt_global = nwlt
    CALL parallel_global_sum (INTEGER=mynwlt_global)
    IF (par_rank.EQ.0) PRINT *, 'CFL    = ', cfl_out
    IF (par_rank.EQ.0) PRINT *, 'it     = ', it,             '  t       = ', t
    IF (par_rank.EQ.0) PRINT *, 'iwrite = ', iwrite,         '  dt      = ', dt
    IF (par_rank.EQ.0) PRINT *, 'j_mx   = ', j_mx,           '  dt_orig = ', dt_original
    IF (par_rank.EQ.0) PRINT *, 'j_lev  = ', j_lev,          '  twrite  = ', twrite
    IF (par_rank.EQ.0) PRINT *, 'nwlt   = ', mynwlt_global,  '  cpu     = ', timer_val(2)
    
    
  END SUBROUTINE user_cal_cfl
  
  SUBROUTINE  user_sgs_force ( u, nlocal)
    IMPLICIT NONE
    
    INTEGER,                         INTENT (IN) :: nlocal
    REAL (pr), DIMENSION (nlocal,n_integrated), INTENT (INOUT) :: u
    
    !default SGS_force, can be explicitely deifined below instead of calling default SGS_force
    IF(sgsmodel /= 0) CALL sgs_force ( u, nlocal)
    
  END SUBROUTINE user_sgs_force

  FUNCTION user_sound_speed (u, neq, nwlt)
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: nwlt, neq
    REAL (pr), DIMENSION (nwlt,neq), INTENT(IN) :: u
    REAL (pr), DIMENSION (nwlt) :: user_sound_speed

    !user_sound_speed(:) = SQRT( gamma*(gamma-1.0_pr)* &
    !       ( u(:,4)-0.5_pr*SUM(u(:,n_var_mom(1:dim))**2,DIM=2)/u(:,1)) ) ! pressure

    user_sound_speed(:) = 0.0_pr

  END FUNCTION user_sound_speed


!!$
!!!!!!!!!!!!!!!!!!!
!!$  SUBROUTINE vert_integ (u_local, u_vert, nlocal, ne_local, ne_vert, meth_order)
!!$    !--Makes u divergence free
!!$    IMPLICIT NONE
!!$    INTEGER, INTENT (IN) :: meth_order, nlocal, ne_local, ne_vert
!!$    REAL (pr), DIMENSION (nlocal,ne_local), INTENT(IN) :: u_local
!!$    REAL (pr), DIMENSION (nlocal,ne_vert), INTENT(INOUT) :: u_vert
!!$    REAL (pr), DIMENSION (nlocal,ne_vert) :: f_local
!!$    REAL (pr), DIMENSION(ne_vert) :: scl_u_local
!!$    INTEGER, DIMENSION(ne_vert) :: clip
!!$
!!$    f_local = rhs_vert_integ (u_local, nlocal, dim, ne_vert, meth_order)
!!$
!!$    clip = 0
!!$    scl_u_local = MAXVAL(scl_global(n_integrated+1:n_integrated+ne_vert)) !scale of pressure increment based on dynamic pressure
!!$
!!$    CALL Linsolve (u_vert, f_local, tol2, nlocal, ne_vert, clip, L_vert_integ, L_vert_integ_diag, SCL=scl_u_local(1)) !new with scaling 
!!$
!!$  END SUBROUTINE vert_integ
!!$
!!$  FUNCTION L_vert_integ (jlev, u_local, nlocal, ne_vert, meth_order)
!!$    IMPLICIT NONE
!!$    INTEGER, INTENT (IN) :: jlev, nlocal, ne_vert, meth_order
!!$    REAL (pr), DIMENSION (nlocal*ne_vert), INTENT (INOUT) :: u_local
!!$    REAL (pr), DIMENSION (nlocal*ne_vert) :: L_vert_integ
!!$
!!$    INTEGER :: i, ii, ie, idim, shift
!!$    INTEGER :: meth_central, meth_backward, meth_forward 
!!$    REAL (pr), DIMENSION (nlocal, dim) :: du, d2u
!!$    INTEGER :: face_type, nloc, ne_one
!!$    INTEGER, DIMENSION(0:dim) :: i_p_face
!!$    INTEGER, DIMENSION(dim) :: face
!!$    INTEGER, DIMENSION(nwlt) :: iloc
!!$    INTEGER, DIMENSION(ne_vert) :: meth_u
!!$
!!$    meth_central  = meth_order + BIASING_NONE
!!$    meth_backward = meth_order + BIASING_BACKWARD
!!$    meth_forward  = meth_order + BIASING_FORWARD
!!$    DO ie =1, ne_vert
!!$       IF(ie == 1)        meth_u(ie) = meth_backward
!!$    END DO
!!$    ne_one = 1
!!$
!!$
!!$    DO ie = 1, ne_vert
!!$       shift=(ie-1)*nlocal
!!$       !--Internal points
!!$       CALL c_diff_fast (u_local(shift+1:shift+nlocal), du, d2u, jlev, nlocal, meth_u(ie), 10, ne_one, 1, ne_one)
!!$       L_vert_integ(shift+1:shift+nlocal) = du( :, dim)
!!$       !--Boundary points
!!$       i_p_face(0) = 1
!!$       DO i=1,dim
!!$          i_p_face(i) = i_p_face(i-1)*3
!!$       END DO
!!$       DO face_type = 0, 3**dim - 1
!!$          face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
!!$          IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
!!$             CALL get_all_indices_by_face (face_type, jlev, nloc, iloc)
!!$             IF(nloc > 0 ) THEN ! ANY dimension
!!$                IF( face(dim) == -1 .AND. ie == 1 ) THEN                ! bottom boundary (entire face) 
!!$                   L_vert_integ(shift+iloc(1:nloc)) = u_local(shift+iloc(1:nloc))  ! Dirichlet conditions
!!$                END IF
!!$             END IF
!!$          END IF
!!$       END DO
!!$    END DO
!!$  END FUNCTION L_vert_integ
!!$
!!$  FUNCTION L_vert_integ_diag (jlev, nlocal, ne_vert, meth_order)
!!$    IMPLICIT NONE
!!$    INTEGER, INTENT (IN) :: jlev, nlocal, ne_vert, meth_order
!!$    REAL (pr), DIMENSION (nlocal*ne_vert) :: L_vert_integ_diag
!!$
!!$    INTEGER :: i, ii, ie, idim, shift
!!$    INTEGER :: meth_central, meth_backward, meth_forward 
!!$    REAL (pr), DIMENSION (nlocal, dim) :: du, d2u
!!$    INTEGER :: face_type, nloc
!!$    INTEGER, DIMENSION(0:dim) :: i_p_face
!!$    INTEGER, DIMENSION(dim) :: face
!!$    INTEGER, DIMENSION(nwlt) :: iloc
!!$    INTEGER, DIMENSION(ne_vert) :: meth_u
!!$
!!$    meth_central  = meth_order + BIASING_NONE
!!$    meth_backward = meth_order + BIASING_BACKWARD
!!$    meth_forward  = meth_order + BIASING_FORWARD
!!$    DO ie =1, ne_vert
!!$       IF(ie == 1) meth_u(ie) = meth_backward
!!$    END DO
!!$
!!$    DO ie = 1, ne_vert
!!$       shift=(ie-1)*nlocal
!!$       !--Internal points
!!$       CALL c_diff_diag ( du, d2u, jlev, nlocal, meth_u(ie), meth_u(ie), 10)
!!$       L_vert_integ_diag(shift+1:shift+nlocal) = du(:, dim)
!!$       !--Boundary points
!!$       i_p_face(0) = 1
!!$       DO i=1,dim
!!$          i_p_face(i) = i_p_face(i-1)*3
!!$       END DO
!!$       DO face_type = 0, 3**dim - 1
!!$          face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
!!$          IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
!!$             CALL get_all_indices_by_face (face_type, jlev, nloc, iloc)
!!$             IF(nloc > 0 ) THEN ! ANY dimension
!!$                IF( face(dim) == -1 .AND. ie == 1 ) THEN                ! bottom boundary (entire face) 
!!$                   L_vert_integ_diag(shift+iloc(1:nloc)) = 1.0_pr  ! Dirichlet conditions
!!$                END IF
!!$             END IF
!!$          END IF
!!$       END DO
!!$    END DO
!!$    
!!$    IF(MINVAL(ABS(L_vert_integ_diag)) < 1.e-12_pr) THEN
!!$	   PRINT *, 'RANGE of L_vert_integ_daig=',  MINVAL(ABS(L_vert_integ_diag)),  MAXVAL(ABS(L_vert_integ_diag))
!!$       PAUSE
!!$    END IF
!!$
!!$  END FUNCTION L_vert_integ_diag
!!$
!!$
!!$  FUNCTION rhs_vert_integ (u_local, nlocal, ne_local, ne_vert, meth_order)
!!$    USE penalization
!!$    IMPLICIT NONE
!!$    INTEGER, INTENT (IN) :: meth_order, nlocal, ne_local, ne_vert
!!$    REAL (pr), DIMENSION (nlocal,ne_local), INTENT(IN) :: u_local
!!$    REAL (pr), DIMENSION (nlocal,ne_vert) :: rhs_vert_integ
!!$    REAL (pr), DIMENSION (dim-1, nlocal, dim) :: du, d2u
!!$
!!$    INTEGER :: i, ii, ie, idim, shift
!!$    INTEGER :: meth_central, meth_backward, meth_forward 
!!$    INTEGER :: face_type, nloc, ne_h
!!$    INTEGER, DIMENSION(0:dim) :: i_p_face
!!$    INTEGER, DIMENSION(dim) :: face
!!$    INTEGER, DIMENSION(nwlt) :: iloc
!!$    INTEGER, DIMENSION(ne_local) :: meth_u
!!$
!!$
!!$    meth_central  = meth_order + BIASING_NONE
!!$    meth_backward = meth_order + BIASING_BACKWARD
!!$    meth_forward  = meth_order + BIASING_FORWARD
!!$    DO ie =1, ne_vert
!!$       IF(ie == 1) meth_u(ie) = meth_backward
!!$    END DO
!!$
!!$    CALL c_diff_fast (u_local(:,1:dim-1), du, d2u, j_lev, nlocal, meth_central, 10, dim-1, 1, dim-1)
!!$
!!$    DO ie = 1, ne_vert
!!$       shift=(ie-1)*nlocal
!!$	   !--Internal points
!!$       IF(ie == 1) THEN
!!$          rhs_vert_integ(:,ie) = 0.0_pr
!!$          DO i = 1, (dim-1)
!!$             IF (imask_obstacle) THEN
!!$                rhs_vert_integ(:,ie) = rhs_vert_integ(:,ie) - du(i,:,i)*(1.0_pr - penal)
!!$             ELSE
!!$                rhs_vert_integ(:,ie) = rhs_vert_integ(:,ie) - du(i,:,i)
!!$             ENDIF
!!$          END DO
!!$          !rhs_vert_integ(:, ie) = 1.0_pr
!!$       END IF
!!$       !--Boundary points
!!$       i_p_face(0) = 1
!!$       DO i=1,dim
!!$          i_p_face(i) = i_p_face(i-1)*3
!!$       END DO
!!$       DO face_type = 0, 3**dim - 1
!!$          face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
!!$          IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
!!$             CALL get_all_indices_by_face (face_type, j_lev, nloc, iloc)
!!$             IF(nloc > 0 ) THEN ! ANY dimension
!!$                IF( face(dim) == -1 .AND. ie == 1 ) THEN                ! bottom boundary (entire face)
!!$                   rhs_vert_integ(iloc(1:nloc),ie) = 0.0_pr  ! Dirichlet conditions
!!$                END IF
!!$             END IF
!!$          END IF
!!$       END DO
!!$    END DO
!!$
!!$  END FUNCTION rhs_vert_integ
!!$
!!$!************** Added to solve deta/dz=0 ***********************
!!$
!!$  SUBROUTINE vert_integ_eta (u_vert, nlocal, ne_local, ne_vert, meth_order)
!!$    !--Makes u divergence free
!!$    IMPLICIT NONE
!!$    INTEGER, INTENT (IN) :: meth_order, nlocal, ne_local, ne_vert
!!$    REAL (pr), DIMENSION (nlocal,ne_vert), INTENT(INOUT) :: u_vert
!!$    REAL (pr), DIMENSION (nlocal,ne_vert) :: f_local
!!$    REAL (pr), DIMENSION(ne_vert) :: scl_u_local
!!$    INTEGER, DIMENSION(ne_vert) :: clip
!!$    
!!$    f_local = rhs_vert_integ_eta (u_vert, nlocal, dim, ne_vert, meth_order)
!!$    clip = 0
!!$    scl_u_local = MAXVAL(scl_global(n_integrated+1:n_integrated+ne_vert)) !scale of pressure increment based on dynamic pressure
!!$
!!$    CALL Linsolve (u_vert, f_local, tol2, nlocal, ne_vert, clip, L_vert_integ_eta, L_vert_integ_diag_eta, SCL=scl_u_local(1)) !new with scaling 
!!$!   CALL gmres (j_lev, 100, nlocal, ne_local, HIGH_ORDER, clip, u_local(:,1), f_local(:,1), L_vert_integ, L_vert_integ_diag)
!!$!	CALL BiCGSTAB (j_lev, nlocal, ne_local, 100, HIGH_ORDER, clip, tol_gmres, u_local(:,1), f_local(:,1), L_vert_integ, L_vert_integ_diag, SCL=scl_u_local(1))
!!$
!!$ 
!!$  END SUBROUTINE vert_integ_eta
!!$
!!$  FUNCTION L_vert_integ_eta (jlev, u_local, nlocal, ne_vert, meth_order)
!!$    IMPLICIT NONE
!!$    INTEGER, INTENT (IN) :: jlev, nlocal, ne_vert, meth_order
!!$    REAL (pr), DIMENSION (nlocal*ne_vert), INTENT (INOUT) :: u_local
!!$    REAL (pr), DIMENSION (nlocal*ne_vert) :: L_vert_integ_eta
!!$
!!$    INTEGER :: i, ii, ie, idim, shift
!!$    INTEGER :: meth_central, meth_backward, meth_forward 
!!$    REAL (pr), DIMENSION (nlocal, dim) :: du, d2u
!!$    INTEGER :: face_type, nloc, ne_one
!!$    INTEGER, DIMENSION(0:dim) :: i_p_face
!!$    INTEGER, DIMENSION(dim) :: face
!!$    INTEGER, DIMENSION(nwlt) :: iloc
!!$    INTEGER, DIMENSION(ne_vert) :: meth_u
!!$
!!$    meth_central  = meth_order + BIASING_NONE
!!$    meth_backward = meth_order + BIASING_BACKWARD
!!$    meth_forward  = meth_order + BIASING_FORWARD
!!$    DO ie =1, ne_vert
!!$       IF(ie == n_var_eta) meth_u(ie) = meth_forward
!!$!       IF(ie == ie_W)        meth_u(ie) = meth_backward
!!$    END DO
!!$    ne_one = 1
!!$
!!$
!!$    DO ie = 1, ne_vert
!!$       shift=(ie-1)*nlocal
!!$       !--Internal points
!!$       CALL c_diff_fast (u_local(shift+1:shift+nlocal), du, d2u, jlev, nlocal, meth_u(ie), 10, ne_one, 1, ne_one)
!!$       L_vert_integ_eta(shift+1:shift+nlocal) = du( :, dim)
!!$       !--Boundary points
!!$       i_p_face(0) = 1
!!$       DO i=1,dim
!!$          i_p_face(i) = i_p_face(i-1)*3
!!$       END DO
!!$       DO face_type = 0, 3**dim - 1
!!$          face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
!!$          IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
!!$             CALL get_all_indices_by_face (face_type, jlev, nloc, iloc)
!!$             IF(nloc > 0 ) THEN ! ANY dimension
!!$                IF( face(dim) == 1 .AND. ie == n_var_eta ) THEN                      ! top boundary (entire face) 
!!$                   L_vert_integ_eta(shift+iloc(1:nloc)) = u_local(shift+iloc(1:nloc))  ! Dirichlet conditions
!!$                END IF
!!$             END IF
!!$          END IF
!!$       END DO
!!$    END DO
!!$  END FUNCTION L_vert_integ_eta
!!$
!!$  FUNCTION L_vert_integ_diag_eta (jlev, nlocal, ne_vert, meth_order)
!!$    IMPLICIT NONE
!!$    INTEGER, INTENT (IN) :: jlev, nlocal, ne_vert, meth_order
!!$    REAL (pr), DIMENSION (nlocal*ne_vert) :: L_vert_integ_diag_eta
!!$
!!$    INTEGER :: i, ii, ie, idim, shift
!!$    INTEGER :: meth_central, meth_backward, meth_forward 
!!$    REAL (pr), DIMENSION (nlocal, dim) :: du, d2u
!!$    INTEGER :: face_type, nloc
!!$    INTEGER, DIMENSION(0:dim) :: i_p_face
!!$    INTEGER, DIMENSION(dim) :: face
!!$    INTEGER, DIMENSION(nwlt) :: iloc
!!$    INTEGER, DIMENSION(ne_vert) :: meth_u
!!$
!!$    meth_central  = meth_order + BIASING_NONE
!!$    meth_backward = meth_order + BIASING_BACKWARD
!!$    meth_forward  = meth_order + BIASING_FORWARD
!!$    DO ie =1, ne_vert
!!$       IF(ie == n_var_eta) meth_u(ie) = meth_forward
!!$!       IF(ie == ie_W) meth_u(ie) = meth_backward
!!$    END DO
!!$
!!$    DO ie = 1, ne_vert
!!$       shift=(ie-1)*nlocal
!!$       !--Internal points
!!$       CALL c_diff_diag ( du, d2u, jlev, nlocal, meth_u(ie), meth_u(ie), 10)
!!$       L_vert_integ_diag_eta(shift+1:shift+nlocal) = du(:, dim)
!!$       !--Boundary points
!!$       i_p_face(0) = 1
!!$       DO i=1,dim
!!$          i_p_face(i) = i_p_face(i-1)*3
!!$       END DO
!!$       DO face_type = 0, 3**dim - 1
!!$          face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
!!$          IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
!!$             CALL get_all_indices_by_face (face_type, jlev, nloc, iloc)
!!$             IF(nloc > 0 ) THEN ! ANY dimension
!!$                IF( face(dim) == 1 .AND. ie ==n_var_eta ) THEN                      ! top boundary (entire face) 
!!$                   L_vert_integ_diag_eta(shift+iloc(1:nloc)) = 1.0_pr           ! Dirichlet conditions
!!$                END IF
!!$             END IF
!!$          END IF
!!$       END DO
!!$    END DO
!!$    
!!$    IF(MINVAL(ABS(L_vert_integ_diag_eta)) < 1.e-12_pr) THEN
!!$	   PRINT *, 'RANGE of L_vert_integ_daig_eta=',  MINVAL(ABS(L_vert_integ_diag_eta)),  MAXVAL(ABS(L_vert_integ_diag_eta))
!!$       PAUSE
!!$    END IF
!!$
!!$  END FUNCTION L_vert_integ_diag_eta
!!$
!!$
!!$  FUNCTION rhs_vert_integ_eta (u_local, nlocal, ne_local, ne_vert, meth_order)
!!$    IMPLICIT NONE
!!$    INTEGER, INTENT (IN) :: meth_order, nlocal, ne_local, ne_vert
!!$    REAL (pr), DIMENSION (nlocal,n_var_eta), INTENT(IN) :: u_local
!!$    REAL (pr), DIMENSION (nlocal,ne_vert) :: rhs_vert_integ_eta
!!$    REAL (pr), DIMENSION (dim-1, nlocal, dim) :: du, d2u
!!$
!!$    INTEGER :: i, ii, ie, idim, shift
!!$    INTEGER :: meth_central, meth_backward, meth_forward 
!!$    INTEGER :: face_type, nloc, ne_h
!!$    INTEGER, DIMENSION(0:dim) :: i_p_face
!!$    INTEGER, DIMENSION(dim) :: face
!!$    INTEGER, DIMENSION(nwlt) :: iloc
!!$    INTEGER, DIMENSION(ne_local) :: meth_u
!!$
!!$
!!$    meth_central  = meth_order + BIASING_NONE
!!$    meth_backward = meth_order + BIASING_BACKWARD
!!$    meth_forward  = meth_order + BIASING_FORWARD
!!$    DO ie =1, ne_vert
!!$       IF(ie == n_var_eta) meth_u(ie) = meth_forward
!!$    END DO
!!$
!!$
!!$       
!!$    DO ie = 1, ne_vert
!!$       shift=(ie-1)*nlocal
!!$	   !--Internal points
!!$       IF(ie == n_var_eta) rhs_vert_integ_eta(:,ie) = 0.0_pr
!!$       i_p_face(0) = 1
!!$       DO i=1,dim
!!$          i_p_face(i) = i_p_face(i-1)*3
!!$       END DO
!!$       DO face_type = 0, 3**dim - 1
!!$          face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
!!$          IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
!!$             CALL get_all_indices_by_face (face_type, j_lev, nloc, iloc)
!!$             IF(nloc > 0 ) THEN ! ANY dimension
!!$                IF( face(dim) == 1 .AND. ie == n_var_eta ) THEN                      ! top boundary (entire face) 
!!$                   rhs_vert_integ_eta(iloc(1:nloc),ie) = u_local(iloc(1:nloc),ie) ! Dirichlet conditions
!!$                END IF
!!$             END IF
!!$          END IF
!!$       END DO
!!$    END DO
!!$
!!$  END FUNCTION rhs_vert_integ_eta

  SUBROUTINE vert_integ_eta (u_eta)
    USE penalization
    USE parallel
    IMPLICIT NONE
    REAL (pr), DIMENSION (nwlt), INTENT(INOUT) :: u_eta
    INTEGER :: i
    REAL (pr) :: t_ex, t_stop, delta_t_ex, delta_z, LHSerr
    REAL (pr), DIMENSION (1, nwlt, dim) :: du, d2u
    INTEGER :: face_type, nloc, meth_local
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc
    INTEGER :: meth_central, meth_backward, meth_forward 
    meth_local=1
    meth_central  = meth_local + BIASING_NONE
    meth_backward = meth_local + BIASING_BACKWARD
    meth_forward  = meth_local + BIASING_FORWARD

    t_ex = 0.0_pr
    t_stop = c_s*(xyzlimits(2, dim) - xyzlimits(1, dim))
    delta_z = (xyzlimits(2, dim) - xyzlimits(1, dim))/REAL(mxyz(dim)*2**(j_lev-1), pr)
    delta_t_ex = c_z*delta_z
    LHSerr = 1.0_pr

    !Integrate deta/dz=0
    DO WHILE (LHSerr.GT.tol_ex)    
!    DO WHILE ((t_ex < t_stop) .AND. (LHSerr.GT.tol_ex))    
       CALL c_diff_fast (u_eta, du, d2u, j_lev, nwlt, meth_forward, 10, 1, 1, 1)
           
       !--Go through all Boundary points that are specified
       i_p_face(0) = 1
       DO i=1,dim
          i_p_face(i) = i_p_face(i-1)*3
       END DO
       DO face_type = 0, 3**dim - 1
          face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
          IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
             CALL get_all_indices_by_face (face_type, j_lev, nloc, iloc)
             IF(nloc > 0 ) THEN 
                IF( face(dim) == 1 ) THEN  ! Zmax face (entire face) 
                   du(1,iloc(1:nloc), dim) = 0.0_pr
                END IF
             END IF
          END IF
       END DO

       u_eta=(delta_t_ex)*du(1, :, dim) + u_eta
       t_ex = t_ex + delta_t_ex
       LHSerr = MAXVAL(ABS(du(1, :, dim)))
       CALL parallel_global_sum( REALMAXVAL=LHSerr )
       !PRINT *, 'max of LHS_eta', LHSerr
    END DO


  END SUBROUTINE vert_integ_eta

 SUBROUTINE vert_integ_w (u_local, u_w)
    USE penalization
    USE parallel
    IMPLICIT NONE
    REAL (pr), DIMENSION (nwlt, (dim-1)), INTENT(IN) :: u_local
    REAL (pr), DIMENSION (nwlt), INTENT(INOUT) :: u_w
    INTEGER :: i
    REAL (pr) :: t_ex, t_stop, delta_t_ex, delta_z, LHSerr
    REAL (pr), DIMENSION (dim, nwlt, dim) :: du, d2u
    INTEGER :: face_type, nloc, meth_local
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc
    INTEGER :: meth_central, meth_backward, meth_forward 
    meth_local=1
    meth_central  = meth_local + BIASING_NONE
    meth_backward = meth_local + BIASING_BACKWARD
    meth_forward  = meth_local + BIASING_FORWARD

    t_ex = 0.0_pr
    t_stop = c_s*(xyzlimits(2, dim) - xyzlimits(1, dim))
    delta_z = (xyzlimits(2, dim) - xyzlimits(1, dim))/REAL(mxyz(dim)*2**(j_lev-1), pr)
    delta_t_ex = c_z*delta_z
    LHSerr = 1.0_pr

    !--Set w=0 on the boundary
    i_p_face(0) = 1
    DO i=1,dim
       i_p_face(i) = i_p_face(i-1)*3
    END DO
    DO face_type = 0, 3**dim - 1
       face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
       IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
          CALL get_all_indices_by_face (face_type, j_lev, nloc, iloc)
          IF(nloc > 0 ) THEN 
             IF( face(dim) == -1 ) THEN  ! Zmin face (entire face) 
                u_w(iloc(1:nloc)) = 0.0_pr
             END IF
          END IF
       END IF
    END DO

    
    !Calculate du/dx and dv/dy outside of loop because they don't change
    CALL c_diff_fast (u_local, du(1:(dim-1),:,:), d2u(1:(dim-1),:,:), j_lev, nwlt, meth_central, 10, (dim-1), 1, (dim-1))

    !Start integration
 !   DO WHILE ((t_ex < t_stop) .AND. (LHSerr.GT.tol_ex))       
    DO WHILE (LHSerr.GT.tol_ex)       
       CALL c_diff_fast (u_w, du(dim, :, :), d2u(1,:,:), j_lev, nwlt, meth_backward, 10, 1, 1, 1)
       
       DO i=1, (dim-1)
          IF (imask_obstacle) THEN
             du(dim,:, dim) = (du(dim, :, dim) + du(i, :, i))*(1.0_pr - penal)
          ELSE
             du(dim,:, dim) = du(dim, :, dim) + du(i, :, i)
          ENDIF
      END DO
    !--Go through all Boundary points that are specified
    i_p_face(0) = 1
    DO i=1,dim
       i_p_face(i) = i_p_face(i-1)*3
    END DO
    DO face_type = 0, 3**dim - 1
       face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
       IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
          CALL get_all_indices_by_face (face_type, j_lev, nloc, iloc)
          IF(nloc > 0 ) THEN 
             IF( face(dim) == -1 ) THEN  ! Zmin face (entire face) 
                du(dim,iloc(1:nloc), dim) = 0.0_pr
             END IF
          END IF
       END IF
    END DO
    
 
    u_w(:)=-(delta_t_ex)*du(dim, :, dim) + u_w(:)   !choose negative since doing backward biasing
    t_ex = t_ex + delta_t_ex
    LHSerr = MAXVAL(ABS(du(dim, :, dim)))
    CALL parallel_global_sum( REALMAXVAL=LHSerr )
    !PRINT *, 'max of LHS_w', LHSerr
 END DO

  END SUBROUTINE vert_integ_w
!!$!***************************************************************




 SUBROUTINE  user_pre_process
  USE penalization
  IMPLICIT NONE


  CALL vert_integ_w (u(:, 2:dim), u(:, n_var_U(dim)))
  CALL vert_integ_eta (u(:, n_var_eta))


  IF(ALLOCATED(w_correction)) DEALLOCATE(w_correction)
  ALLOCATE(w_correction(nwlt))
     w_correction(1:nwlt) = u(:, n_var_U(dim))

  IF(continent_active) THEN
     IF(ALLOCATED(chi_continent)) DEALLOCATE(chi_continent)
     ALLOCATE(chi_continent(nwlt))
        chi_continent =  0.25_pr*(tanh((x(:,1)-x01_pen)/delta_pen)+tanh((x(:,1)-x02_pen)/delta_pen))**2.0_pr
  END IF
 
     
 END SUBROUTINE user_pre_process

 SUBROUTINE  user_post_process
  IMPLICIT NONE

    
  END SUBROUTINE user_post_process


END MODULE user_case





