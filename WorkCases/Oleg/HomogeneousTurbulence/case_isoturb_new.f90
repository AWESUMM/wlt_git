MODULE random_spectra_init
  USE precision
  IMPLICIT NONE
  PRIVATE
  REAL (pr), PUBLIC:: k0, KMAX, KE
    
  PUBLIC :: randspecU

  CONTAINS

  SUBROUTINE randspecU(iter)

  USE precision
  USE pde
  USE share_consts
  USE fft_module
  USE spectra_module
  USE wlt_trns_mod
  USE wlt_trns_util_mod
  USE util_mod
  USE sizes
  USE io_3D_vars
  USE share_consts
  USE wlt_vars
  USE field


  IMPLICIT NONE
  INTEGER, INTENT (INOUT) :: iter ! iteration of call while adapting initial grid
  REAL (pr), ALLOCATABLE,DIMENSION (:,:) :: kxyz
  REAL (pr), DIMENSION (7) :: sumn
  REAL (pr) :: espec 
  REAL (pr), DIMENSION (6) :: Cvals
  REAL (pr), SAVE, ALLOCATABLE, DIMENSION (:,:,:,:) :: uu
  REAL (pr) ::   pi2, kmagE2, const, r, kmag, volconst, const2
  INTEGER, DIMENSION(3) :: Nxyz_init
  INTEGER :: i, j, k, knum, num, m
  INTEGER, DIMENSION (3)   :: offset
  INTEGER :: nloc,idim
  INTEGER, DIMENSION(nwlt) ::  i_arr, deriv_lev_loc
  INTEGER, DIMENSION(nwlt,1:dim) ::  indx_loc
  INTEGER, DIMENSION(1) :: SEED
  INTEGER, DIMENSION(3) :: ONE

  IF(dim /= 3) THEN
     PRINT *, 'ERROR: this initialization assumes 3-D field and uses 3-D FFT'
     STOP
  END IF
  IF(MAXVAL(grid(1:dim)) /= 0) THEN
     PRINT *, 'ERROR: this initialization assumes uniform grid in all 3 directions'
     STOP
  END IF

  ONE = 1
  pi2=pi*2.0_pr
  volconst=4.0_pr/3.0_pr*pi

  Nxyz_init = 1
  Nxyz_init(1:dim)=mxyz(1:dim)*2**(j_IC-1)

  IF( iter == 1 ) THEN

     ALLOCATE(uu(Nxyz_init(1)+2,Nxyz_init(2)+1,Nxyz_init(3)+1,3))
     ALLOCATE(kxyz(MAXVAL(Nxyz_init)+2,3))
     IF( ANY(MOD(MOD(Nxyz_init(1:dim),nxyz(1:dim)),2*ONE(1:dim))/= 0 ) ) THEN 
        PRINT *,'Error in randspec(), mod(mod(Nxyz_init,nxyz)),2) /= 0 '
        PRINT *,' mod(mod(Nxyz_init,nxyz)) =', MOD(MOD(Nxyz_init(1:dim),nxyz(1:dim)),2*ONE(1:dim))
        PRINT *,' Exiting ... '
        STOP
     END IF

     kxyz=0.0_pr
     idim = 1  ! x-direction - real transfrom 
     DO i=1,Nxyz_init(idim)/2+1
        kxyz(2*i-1,idim)=REAL(i-1)!*pi2/Lxyz_init(idim)
        kxyz(2*i,idim)=kxyz(2*i-1,idim)
     END DO
     DO idim = 2, 3
        DO j=1,Nxyz_init(idim)/2
           kxyz(j,idim)=REAL(j-1)!*pi2/Lxyz_init(idim)
           kxyz(j+Nxyz_init(idim)/2+1,idim)=(REAL(j)-REAL(Nxyz_init(idim))/2.0_pr)!*pi2/Lxyz_init(idim)
        END DO
        kxyz(Nxyz_init(idim)/2+1,idim)=REAL(Nxyz_init(idim))/2.0_pr!*pi2/Lxyz_init(idim)
     END DO
     
     SEED(1) = 54321
     CALL RANDOM_SEED(PUT=SEED(1:1))

     DO k=1,Nxyz_init(3)+1
        DO j=1,Nxyz_init(2)+1
           DO i=1,Nxyz_init(1)+2
              DO m=1,3
                 CALL RANDOM_NUMBER(const)
                 CALL RANDOM_NUMBER(const2)
                 uu(i,j,k,m)=SQRT(-2.0_pr*LOG(const))*SIN(2.0_pr*pi*const2)
              END DO
           END DO
        END DO
     END DO
      
     CALL init_fft(Nxyz_init(1),Nxyz_init(2),Nxyz_init(3),pi2,pi2,pi2,.FALSE.)
!     CALL init_fft(Nxyz_init(1),Nxyz_init(2),Nxyz_init(3),&
!                   xyzlimits(2,1)-xyzlimits(1,1),xyzlimits(2,2)-xyzlimits(1,2),&
!                   xyzlimits(2,3)-xyzlimits(1,3),.FALSE.)
     
     DO k=1,Nxyz_init(3)
        DO j=1,Nxyz_init(2)
           DO i=1,Nxyz_init(1)/2+1
              kmagE2=kxyz(2*i-1,1)**2+kxyz(j,2)**2+kxyz(k,3)**2
              IF (kmagE2<0.00000001_pr) THEN
                 kmagE2=9999999999.0_pr
              END IF
              const=(kxyz(2*i,1)*uu(2*i,j,k,1)+kxyz(j,2)*uu(2*i,j,k,2)+kxyz(k,3)*uu(2*i,j,k,3))/kmagE2
              uu(2*i,j,k,1)=uu(2*i,j,k,1)-const*kxyz(2*i,1)
              uu(2*i,j,k,2)=uu(2*i,j,k,2)-const*kxyz(j,2)
              uu(2*i,j,k,3)=uu(2*i,j,k,3)-const*kxyz(k,3)
              const=(kxyz(2*i-1,1)*uu(2*i-1,j,k,1)+kxyz(j,2)*uu(2*i-1,j,k,2)+kxyz(k,3)*uu(2*i-1,j,k,3))/kmagE2
              uu(2*i-1,j,k,1)=uu(2*i-1,j,k,1)-const*kxyz(2*i-1,1)
              uu(2*i-1,j,k,2)=uu(2*i-1,j,k,2)-const*kxyz(j,2)
              uu(2*i-1,j,k,3)=uu(2*i-1,j,k,3)-const*kxyz(k,3)

           END DO
        END DO
     END DO
     CALL ftort(uu(:,:,:,1))
     CALL ftort(uu(:,:,:,2))
     CALL ftort(uu(:,:,:,3))
     sumn=0.0_pr
     DO k=1,Nxyz_init(3)
        DO j=1,Nxyz_init(2)
           DO i=1,Nxyz_init(1)
              sumn(1)=sumn(1)+uu(i,j,k,1)**2+uu(i,j,k,2)**2+uu(i,j,k,3)**2
           END DO
        END DO
     END DO
     sumn(1)=sumn(1)/REAL(PRODUCT(Nxyz_init(1:3)))
     DO k=1,Nxyz_init(3)
        DO j=1,Nxyz_init(2)
           DO i=1,Nxyz_init(1)
              uu(i,j,k,1:3)=SQRT(1.0_pr/sumn(1))*uu(i,j,k,1:3)
           END DO
        END DO
     END DO
     
     CALL rtoft(uu(:,:,:,1))
     CALL rtoft(uu(:,:,:,2))
     CALL rtoft(uu(:,:,:,3))

     DO knum = 1,2*MAX(CEILING((REAL(Nxyz_init(1))+1.0_pr)/2.0_pr),CEILING((REAL(Nxyz_init(2)))/2.0_pr),CEILING((REAL(Nxyz_init(3)))/2.0_pr))
        r=REAL(knum)
        num=0
        sumn=0.0_pr
        DO k=1,Nxyz_init(3)
           DO j=1,Nxyz_init(2)
              DO i=1,Nxyz_init(1)/2+1
                 kmag=SQRT(kxyz(2*i-1,1)**2+kxyz(j,2)**2+kxyz(k,3)**2)
                 IF ((knum==1 .AND. kmag<=0.5_pr) .OR. (kmag>r-0.5_pr .AND. kmag<=r+0.5_pr)) THEN
                    num=num+1
                    DO m=1,3
                       sumn(m)=sumn(m)+uu(2*i-1,j,k,m)**2+uu(2*i,j,k,m)**2
                    END DO
                 END IF
              END DO
           END DO
        END DO
        sumn(1:3)=sumn(1)+sumn(2)+sumn(3)
        sumn(7)=REAL(num)
        DO m=1,3
           IF (sumn(m)<0.0000001_pr) THEN
              sumn(m)=9999999999.0_pr
           END IF
        END DO
        !  Ssums(knum)=sumn(7)
        DO m=1,3
           Cvals(m)=2.0_pr*sumn(7)/(volconst*((r+0.5_pr)**3-(r-0.5_pr)**3)*sumn(m))
        END DO
        IF (knum==1) THEN
           DO m=1,3
              Cvals(m)=2.0_pr*sumn(7)/(volconst*((1.5_pr)**3)*sumn(m))
           END DO
        END IF
        IF (r>KMAX) THEN
           Cvals(:)=0.0_pr
        END IF
        !  espec(knum)=32.0_pr/3.0_pr*SQRT(2.0_pr/pi)*r**4/k0**5*EXP(-2*(r/k0)**2)
        espec=r**4/k0**5*EXP(-2*(r/k0)**2)
        !  END DO
        DO k=1,Nxyz_init(3)
           DO j=1,Nxyz_init(2)
              DO i=1,Nxyz_init(1)/2+1
                 kmag=SQRT(kxyz(2*i-1,1)**2+kxyz(j,2)**2+kxyz(k,3)**2)
                 IF ((knum==1 .AND. kmag<=0.5_pr) .OR. (kmag>r-0.5_pr .AND. kmag<=r+0.5_pr)) THEN
                    uu(2*i-1,j,k,1)=uu(2*i-1,j,k,1)*SQRT(espec*Cvals(1))
                    uu(2*i,j,k,1)=uu(2*i,j,k,1)*SQRT(espec*Cvals(1))
                    uu(2*i-1,j,k,2)=uu(2*i-1,j,k,2)*SQRT(espec*Cvals(2))
                    uu(2*i,j,k,2)=uu(2*i,j,k,2)*SQRT(espec*Cvals(2))
                    uu(2*i-1,j,k,3)=uu(2*i-1,j,k,3)*SQRT(espec*Cvals(3))
                    uu(2*i,j,k,3)=uu(2*i,j,k,3)*SQRT(espec*Cvals(3))
                 END IF
              END DO
           END DO
        END DO
     END DO
     
     CALL ftort(uu(:,:,:,1))
     CALL ftort(uu(:,:,:,2))
     CALL ftort(uu(:,:,:,3))
     
     sumn(1)=0.0_pr
     DO k=1,Nxyz_init(3)
        DO j=1,Nxyz_init(2)
           DO i=1,Nxyz_init(1)
              sumn(1)=sumn(1)+uu(i,j,k,1)**2+uu(i,j,k,2)**2+uu(i,j,k,3)**2
           END DO
        END DO
     END DO
     sumn(1)=sumn(1)/REAL(Nxyz_init(1)*Nxyz_init(2)*Nxyz_init(3))/2.0_pr/KE
     DO k=1,Nxyz_init(3)
        DO j=1,Nxyz_init(2)
           DO i=1,Nxyz_init(1)
              uu(i,j,k,1:3)=SQRT(1.0_pr/sumn(1))*uu(i,j,k,1:3)
           END DO
        END DO
     END DO
    
 
  END IF !done reading in file for iter == 1


  IF (  nxyz(1) > Nxyz_init(1)  .or. &
       nxyz(2) > Nxyz_init(2)  .or. &
       nxyz(3) > Nxyz_init(3) ) THEN

     !
     ! we have used all the input data we can so set iter to -1
     ! (which will cause the memory to be deallocated below)
     !
     iter = -1
  ELSE


     !
     ! Calculate the offsets to put the data into the u array
     !

     offset(1:3) = Nxyz_init(1:dim)/nxyz(1:dim)

     ! Save the field into the flat u array that will be used in the rest
     ! of the code

     !Assumes 3-D inital data field
     CALL get_indices_and_coordinates_by_types ( (/0,2**dim-1/), (/0,3**dim-1/), (/1,j_lev/), &
            j_lev, nloc, i_arr, indx_loc,deriv_lev_loc)
     DO k = 1, nloc
        u(i_arr(k),1) = uu(offset(1)*indx_loc(k,1)+1,offset(2)*indx_loc(k,2)+1,offset(3)*indx_loc(k,3)+1,1)
        u(i_arr(k),2) = uu(offset(1)*indx_loc(k,1)+1,offset(2)*indx_loc(k,2)+1,offset(3)*indx_loc(k,3)+1,2)
        u(i_arr(k),3) = uu(offset(1)*indx_loc(k,1)+1,offset(2)*indx_loc(k,2)+1,offset(3)*indx_loc(k,3)+1,3)
     END DO
  END IF
  PRINT *,' in read_init iter = ', iter
!  PRINT *,'at 1'
!  pause
  IF( iter == -1 ) THEN
 	    DEALLOCATE(uu)
        PRINT *,'DEALLOCATE(uu) '
  END IF

  END SUBROUTINE randspecU

END MODULE random_spectra_init

MODULE user_case
  !
  ! Case isotropic decaying turbulence
  ! Dan Goldstein 11/30/2004
  ! Updated for use with Crank Nicholson time integration format 3/10/2005, Dan Goldstein
  ! Linear forcing 06/26/2007

  USE precision
  USE elliptic_vars
  USE elliptic_mod
  USE field
  USE input_file_reader
  USE io_3D_vars
  USE pde
  USE share_consts
  USE share_kry
  USE sizes
  USE util_mod
  USE util_vars
  USE vector_util_mod
  USE wavelet_filters_mod
  USE wlt_vars
  USE wlt_trns_vars
  USE wlt_trns_mod
  USE wlt_trns_util_mod
  USE fft_module
  USE spectra_module
  USE SGS_incompressible

  USE VT                      ! Spatial Adaptive Epsilon

  !
  ! case specific variables
  !
  INTEGER n_var_vorticity ! start of vorticity in u array (Must exist)
  INTEGER n_var_pressure  ! start of pressure in u array  (Must exist)

  INTEGER n_var_modvort   ! location of modulus of vorticity
  INTEGER n_var_modSij    ! location of modulus of Sij
  INTEGER n_var_modVel     ! location of velocity magnitude

  LOGICAL :: adaptMagVort, adaptNormS, adaptMagVel, adaptVel
  LOGICAL ::  saveMagVort, saveNormS

  REAL (pr) :: Cf
  REAL (pr) :: scaleCoef_NormS
  REAL (pr) :: expl_filt_diss

CONTAINS

  !
  ! In user_setup_pde() we setup how many variables are integrated, which are interpolated
  ! to the next times step and if any exeact solution exists to compare to during the run.
  ! We also set the variable names that are used when the result files are written.
  !
  ! The following variables must be setup in this routine:
  !
  !
  ! n_integrated     ! first n_integrated eqns will be acted on for time integration
  ! n_var_additional ! 
  ! n_var
  !
  !
  !
  !
  SUBROUTINE  user_setup_pde ( VERB ) 
    USE parallel
    USE unused
    IMPLICIT NONE
    LOGICAL, OPTIONAL :: VERB         ! print debug info
    INTEGER :: i
    
    IF (par_rank.EQ.0) THEN
       PRINT * ,''
       PRINT *, '**********************Setting up PDE*****************'
       PRINT * ,'CASE ISOTURB '
       PRINT *, '*****************************************************'
    END IF


    !------------ setting up default values 

    n_integrated = dim ! uvw - # of equations to solve without SGS model
 
    ! Spatial Adaptive Epsilon   Evolution
    IF( do_Adp_Eps_Spatial_Evol==.TRUE. ) THEN
        n_var_EpsilonEvol  = n_integrated  +  1
        n_integrated       = n_integrated  +  1
    END IF
 
    n_integrated = n_integrated + n_var_SGS ! adds additional equations for SGS model
    
    n_time_levels = 1  !--# time levels

    n_var_time_levels = n_time_levels * n_integrated !--Number of equations (2D velocity at two time levels)

    n_var_additional = 1 !--1 pressure at one time level 

    n_var_exact = 0 !--No exact solution 

    n_var_pressure  = n_integrated + 1 !pressure

    IF( adaptMagVort .or. saveMagVort ) THEN
       n_var_additional = n_var_additional +1
       n_var_modvort    = n_integrated + n_var_additional ! (wi.wi)^0.5vorticity magnitude
    END IF
    IF( adaptNormS .OR. saveNormS) THEN
       n_var_additional = n_var_additional +1
       n_var_modSij     = n_integrated + n_var_additional ! (SijSij)^0.5
    END IF
    IF( adaptMagVel ) THEN
       n_var_additional = n_var_additional +1
       n_var_modVel     = n_integrated + n_var_additional ! (UiUi)^0.5
    END IF


    ! Spatial Adaptive Epsilon
    IF( do_Adp_Eps_Spatial==.TRUE. .OR. do_Adp_Eps_Spatial_Evol==.TRUE. ) THEN
        n_var_additional = n_var_additional + 2  ! n_var_SGSD  &  n_var_RD      But they will be set in    SUBROUTINE SGS_setup
    END IF


    n_var = n_var_time_levels + n_var_additional !--Total number of variables

    !
    !--Allocate logical arrays 
    !  Must be done after setting number of different types of variables
    !  and before setting logical variable mapping arrays
    !
    CALL alloc_variable_mappings
    !
    ! Fill in variable names for default variables for integration meth 2
    ! These may be changed in the call to the user specified routine
    !  user_setup_pde()
    !
    ! In integrated variables (3D)

                   WRITE (u_variable_names(1), u_variable_names_fmt) 'Velocity_u_@t  '
    IF( dim >= 2 ) WRITE (u_variable_names(2), u_variable_names_fmt) 'Velocity_v_@t  '
    IF( dim >= 3 ) WRITE (u_variable_names(3), u_variable_names_fmt) 'Velocity_w_@t  '
    WRITE (u_variable_names(n_var_pressure),   u_variable_names_fmt) 'Pressure  '
    IF( adaptMagVort .OR. saveMagVort) WRITE (u_variable_names(n_var_modvort), u_variable_names_fmt) 'modWij  ' !'|Wij|  '
    IF( adaptNormS .OR. saveNormS)     WRITE (u_variable_names(n_var_modSij),  u_variable_names_fmt) 'modSij  ' !'|Sij|  '
    
    
    
    !
    ! setup logical variable mapping arrays
    ! This defined which variables are used for adaptation, Which need to be interpololated to the new grid at 
    ! each time step, which are saved, and for which we have an exect solutiuon we want to check at each time step
    !

    !
    ! setup which components we will base grid adaptation on.
    ! Adapt on the 3 velocity components
    !
    n_var_adapt(1:dim,0) = .TRUE. !--Initially adapt on integrated variables at first time level
    !eventually adapt to mdl variables    n_var_adapt(1:dim+2,1) = .TRUE. !--After first time step adapt on  velocity and Ilm and Imm

    IF( adaptVel )     n_var_adapt(1:dim,1)          = .TRUE. !--After first time step adapt on  velocity
	 
    IF( adaptMagVort ) n_var_adapt(n_var_modvort,1)  = .TRUE.
    IF( adaptNormS )   n_var_adapt(n_var_modSij,1 )  = .TRUE.
    


    !Variables that need to be interpoleted to new adapted grid in initial grid adaptation
    n_var_interpolate(1:n_integrated,0) = .TRUE. 

    !Variables that need to be interpoleted to new adapted grid at each time step
    n_var_interpolate(1:n_var_pressure,0) = .TRUE. 
    n_var_interpolate(1:n_var_pressure,1) = .TRUE. 

    IF( saveMagVort ) n_var_interpolate(n_var_modvort,:) = .TRUE. 
    IF( saveNormS )   n_var_interpolate(n_var_modSij,:)  = .TRUE.
    
    !
    ! setup which components we have an exact solution for

    n_var_exact_soln(:,0:1) = .FALSE.

    !
    ! variables required for restart
    !
    n_var_req_restart = .FALSE.
    n_var_req_restart(1:dim)	        = .TRUE. !restart with velocities and pressure to begin with!
    n_var_req_restart(n_var_pressure)	= .TRUE. !

    ! no pressure for restart from initial file
    !n_var_req_restart(n_var_pressure ) = .TRUE.

    !
    ! setup which variables we will save the solution
    !
    n_var_save(1:n_var_pressure) = .TRUE. ! save all for restarting code

    IF( saveMagVort ) n_var_save(n_var_modvort) = .TRUE.
    IF( saveNormS )   n_var_save(n_var_modSij)  = .TRUE.

	!OPTIONAL: set up variables useed for calculation of numerical viscosity
    !IF( hypermodel /= 0 ) THEN
    !   n_var_hyper = .FALSE.
	!   n_var_hyper(1:n_integrated) = .TRUE
	!END IF

    
    !
    !--Time level counters for NS integration
    !   Used for integration meth2
    ! n0 is current time step
    ! n1 is t_n-1 time step
    ! n2 is t_n-2 time step
    !
    n0 = 1; n1 = n0+dim; n2 = n1+dim


    !
    ! Set the maximum number of components for which we have an exact solution
    !
    n_var_exact = MAX( COUNT(n_var_exact_soln(:,0)),COUNT(n_var_exact_soln(:,1)) )


    !
	! Setup a scaleCoeff array of we want to tweak the scaling of a variable
	! ( if scaleCoeff < 1.0 then more points will be added to grid 
	!
    ALLOCATE ( scaleCoeff(1:n_var), Umn(1:n_var) )
    scaleCoeff = 1.0_pr
    IF( adaptNormS )   scaleCoeff(n_var_modSij )  = scaleCoef_NormS

    Umn = 0.0_pr !set up here if mean quantities anre not zero and used in scales or equation
    
    IF (par_rank.EQ.0) THEN
       PRINT *, 'n_integrated       = ',n_integrated 
       PRINT *, 'n_time_levels      = ',n_time_levels
       PRINT *, 'n_var_time_levels  = ',n_var_time_levels 
       PRINT *, 'n_var_additional   = ',n_var_additional
       PRINT *, 'n_var              = ',n_var 
       PRINT *, 'n_var_exact        = ',n_var_exact 
       PRINT *, '*******************Variable Names*******************'
       DO i = 1,n_var
          WRITE (*, u_variable_names_fmt) u_variable_names(i)
       END DO
       PRINT *, '****************************************************'
    END IF

    IF ( do_Adp_Eps_Spatial .OR. do_Adp_Eps_Spatial_Evol ) &
    CALL user_setup_pde__VT


  END SUBROUTINE  user_setup_pde

  !
  ! read variables from input file for this user case 
  !
  SUBROUTINE  user_read_input()
    USE random_spectra_init
    IMPLICIT NONE

  call input_real ('nu',nu,'stop', &
       ' nu: viscosity')

   adaptVel = .TRUE.
   call input_logical ('adaptVel',adaptVel,'default', &
       '  adaptVel, Adapt on the velocity components.   DefaultValue=T')

   call input_logical ('adaptMagVort',adaptMagVort,'stop', &
       '  adaptMagVort, Adapt on the magnitude of vorticity')

   call input_logical ('adaptNormS',adaptNormS,'stop', &
       '  adaptNormS, Adapt on the L2 of Sij')

   call input_logical ('adaptMagVel',adaptMagVel,'stop', &
       '  adaptMagVel, Adapt on the magnitude of velocity')

   saveMagVort = .FALSE.
   call input_logical ('saveMagVort',saveMagVort,'default', &
       '  if .TRUE. adds and saves magnitude of vorticity')

   saveNormS = .FALSE.
   call input_logical ('saveNormS',saveNormS,'default', &
       '  if .TRUE. adds and saves magnitude of strain rate')
   scaleCoef_NormS = 1.0_pr
   IF( adaptNormS )      call input_real ('scaleCoef_NormS', scaleCoef_NormS, 'default', ' scaleCoef_NormS: scaleCoef for L2 of Sij')

   Cf = 0.0_pr
   call input_real ('Cf',Cf,'default', &
       ' Cf: linear forcing coefficient')

   k0 = 4.0_pr
   call input_real ('k0',k0,'default', &
       ' k0: wavenumber for the peak of the energy spectrum')
   
   Kmax=1.0_pr
   call input_real ('Kmax',Kmax,'default', &
       ' Kmax*K_Nyquist: wavenumber cut-off for spectrum initialization')
   Kmax = Kmax*REAL(MINVAL(mxyz)*2**(j_IC-1)/2,pr)

   KE = 5.E2_pr
   call input_real ('KE',KE,'default', &
       ' KE: average kinetic energy')

   CALL user_read_input__VT

  END SUBROUTINE  user_read_input

  !
  ! Set the exact solution for comparison to the simulated solution
  !
  ! u          - array to fill in the exact solution
  ! nlocal       - number of active wavelets   
  ! ne_local        - total number of equations
  ! t          - time of current time step 
  ! l_n_var_exact_soln_index - index into the elements of u for which we need to 
  !                            find the exact solution
  SUBROUTINE  user_exact_soln (u, nlocal,  t_local, l_n_var_exact_soln)  
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: nlocal
    REAL (pr), INTENT (IN) ::  t_local
    REAL (pr), DIMENSION (nlocal,n_var_exact), INTENT (INOUT) :: u
    LOGICAL , INTENT (IN) :: l_n_var_exact_soln(n_var)
    INTEGER :: i


    ! There is no exact solution

  END SUBROUTINE  user_exact_soln

  SUBROUTINE user_initial_conditions (u, nlocal, ne_local, t_local, scl, scl_fltwt, iter)
    USE random_spectra_init
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: nlocal, ne_local
    INTEGER  , INTENT (INOUT) :: iter ! iteration of call while adapting initial grid
    REAL (pr), DIMENSION (nlocal,ne_local), INTENT (INOUT) :: u
    REAL (pr)  :: scl(1:n_var),scl_fltwt
    REAL (pr), INTENT (IN) :: t_local

    INTEGER :: i !TEST

	!add or overtise teh values in the fileds
    IF (IC_restart ) THEN !in the case of restart
	   !if restarts - DO NOTHING
    ELSE IF (IC_from_file) THEN  ! Initial conditions read from data file 
       !
	   ! Modify appropriate values
       ! 
       !u(:,n_var_pressure) = 0.0_pr
    ELSE !initialization from random field
       CALL randspecU(iter)
       u(:,n_var_pressure) = 0.0_pr
    END IF

    IF ( do_Adp_Eps_Spatial .OR. do_Adp_Eps_Spatial_Evol ) &
    CALL user_initial_conditions__VT(u, nlocal, ne_local)!, t_local, scl, scl_fltwt, iter)

  END SUBROUTINE user_initial_conditions

  ! Arguments
  ! u         - field on adaptive grid
  ! nlocal      - number of active points
  ! ne_local       - number of equations
  ! t         - current time
  !
  ! Global variables used
  ! ifn       - number of equations
  ! x()
  ! xO
  ! y0
  !--******************************** 
  SUBROUTINE user_algebraic_BC (Lu, u, nlocal, ne_local, jlev, meth)
    !--Defines boundary condition type
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: jlev, meth, ne_local, nlocal
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (INOUT) :: Lu
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (IN)    :: u

    INTEGER :: ie, ii, shift

    !
    ! There are periodic BC conditions
    !

    !default SGS BC, can be explicitely deifined below instead of calling default BCs
    CALL SGS_algebraic_BC (Lu, u, nlocal, ne_local, jlev, meth) 

  END SUBROUTINE user_algebraic_BC

  SUBROUTINE user_algebraic_BC_diag (Lu_diag, nlocal, ne_local, jlev, meth)
    !--Defines boundary condition type
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: jlev, meth, ne_local, nlocal
    REAL (pr), DIMENSION (nlocal,ne_local), INTENT (INOUT) :: Lu_diag

    INTEGER :: ie, ii, shift

    !
    ! There are periodic BC conditions
    !
    
    !default diagonal terms of SGS BC, can be explicitely deifined below instead of calling default BCs
    IF(sgsmodel /=0 ) CALL SGS_algebraic_BC_diag (Lu_diag, nlocal, ne_local, jlev, meth)

  END SUBROUTINE user_algebraic_BC_diag

  SUBROUTINE user_algebraic_BC_rhs (rhs, ne_local, nlocal, jlev)
    !--Sets rhs for boundary conditions
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: ne_local, nlocal, jlev
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (INOUT) :: rhs

    INTEGER :: ie, ii, shift

    !
    ! There are periodic BC conditions
    !

    !default  SGS BC, can be explicitely deifined below instead of calling default BCs
    IF(sgsmodel /= 0) CALL SGS_algebraic_BC_rhs (rhs, ne_local, nlocal, jlev)

  END SUBROUTINE user_algebraic_BC_rhs

  SUBROUTINE user_project (u, p, nlocal, meth)
    !--Makes u divergence free
    IMPLICIT NONE
    INTEGER, PARAMETER :: ne_local = 1
    INTEGER, INTENT (IN) :: meth, nlocal
    REAL (pr), DIMENSION (nlocal),     INTENT(INOUT) :: p
    REAL (pr), DIMENSION (nlocal,n_integrated), INTENT(INOUT) :: u
    REAL (pr), DIMENSION (nlocal) :: dp
    REAL (pr), DIMENSION (nlocal) :: f
    REAL (pr), DIMENSION(ne_local) :: scl_p
    INTEGER, DIMENSION(ne_local) :: clip 

    !--Make u  divergence free
    dp = 0.0_pr
    f = div(u(:,1:dim),nlocal,j_lev,meth+4)
    clip = 1

    scl_p = MAXVAL(scl_global(1:dim)) !scale of pressure increment based on dynamic pressure
    !PRINT *, 'scl_p=',scl_p
    CALL Linsolve (dp, f , tol2, nlocal, ne_local, clip, Laplace, Laplace_diag, SCL=scl_p) !new with scaling 
!    CALL Linsolve (dp, f , tol2, nlocal, ne_local, clip, Laplace, Laplace_diag)  !old without scaling
    u(:,1:dim) = u(:,1:dim) - grad(dp, nlocal, j_lev, meth+2)
    p = p + dp/dt

  END SUBROUTINE user_project

  FUNCTION Laplace (jlev, u, nlocal, ne_local, meth_in)
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: jlev, nlocal, ne_local, meth_in
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (INOUT) :: u
    REAL (pr), DIMENSION (nlocal*ne_local) :: Laplace

    INTEGER :: i, ii, ie, meth, meth1, meth2, idim, shift
    REAL (pr), DIMENSION (ne_local ,nlocal,dim) :: du
    REAL (pr), DIMENSION (ne_local*dim ,nlocal,dim) :: d2u
    REAL (pr), DIMENSION (ne_local*dim ,nlocal,dim) :: du_dummy ! passed when only calculating 1st derivative.
    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc

    meth1 = meth_in + 2
    meth2 = meth_in + 4

    !
    ! Find 1st deriviative of u. 
    !
    CALL c_diff_fast (u, du, d2u(1:ne_local, 1:nlocal, 1:dim), jlev, nlocal, meth1, 10, ne_local, 1, ne_local)


    !
    ! Find 2nd deriviative of u.  d( du ) 
    !
    !
    ! Find 2nd deriviative of u.  d( du ) 
    !
    IF ( TYPE_DB .NE. DB_TYPE_LINES ) THEN
       DO ie = 1, ne_local
          CALL c_diff_fast(du(ie,1:nlocal,1:dim), &
               d2u((ie-1)*dim+1:ie*dim,1:nlocal,1:dim), du_dummy((ie-1)*dim+1:ie*dim,1:nlocal,1:dim), &
               jlev, nlocal, meth2, 10, dim, 1, dim )
       END DO
    ELSE !db_lines

       ! Load du  into db
       DO ie=1,ne_local		
          CALL update_db_from_u(  du(ie,1:nlocal,1:dim)  , nlocal ,dim  , 1, dim, 1+(ne_local-1)*dim  ) !Load du	    
       END DO

       ! find  2nd deriviative of u (so we first derivative of du/dx)
       CALL c_diff_fast_db(d2u, du_dummy, jlev, nlocal, meth2, 10, ne_local*dim,&
            1,         &  ! MIN(mn_varD,mn_varD2)
            ne_local*dim,    &  ! MAX(mx_varD,mx_varD2)
            1,         &  ! mn_varD  :   min variable in db that we are doing the 1st derivatives for.
            ne_local*dim,    &  ! mn_varD  :   max variable in db that we are doing the 1st derivatives for.
            0 ,        &  ! mn_varD2 :   min variable in db that we are doing the 2nd derivatives for.
            0     )  ! mn_varD2 :   max variable in db that we are doing the 2nd derivatives for.

    END IF
    ! Now:
    ! d2u(1,:,1) = d^2 U/ dx dx
    ! d2u(1,:,2) = d^2 U/ dy dx
    ! d2u(1,:,3) = d^2 U/ dz dx
    ! d2u(2,:,1) = d^2 V/ dx dy
    ! d2u(2,:,2) = d^2 V/ dy dy
    ! d2u(2,:,3) = d^2 V/ dz dy
    ! d2u(3,:,1) = d^2 W/ dx dz
    ! d2u(3,:,2) = d^2 W/ dy dz
    ! d2u(3,:,3) = d^2 W/ dz dz

    DO ie = 1, ne_local
       shift=(ie-1)*nlocal
       !--- Internal points
       !--- div(grad)
       idim = 1
       Laplace(shift+1:shift+Nwlt_lev(jlev,0)) = d2u( (ie-1)*dim+idim ,1:Nwlt_lev(jlev,0),1)
       DO idim = 2,dim
          Laplace(shift+1:shift+Nwlt_lev(jlev,0)) = Laplace(shift+1:shift+Nwlt_lev(jlev,0)) + &
               d2u((ie-1)*dim+idim ,1:Nwlt_lev(jlev,0),idim)
       END DO
       !--Boundary points
       i_p_face(0) = 1
       DO i=1,dim
          i_p_face(i) = i_p_face(i-1)*3
       END DO
       DO face_type = 0, 3**dim - 1
          face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
          IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
             CALL get_all_indices_by_face (face_type, jlev, nloc, iloc)
             IF(nloc > 0 .AND. dim == 3) THEN ! 3-D case only
                IF( ALL( face == (/-1,0,0/) ) ) THEN  ! Xmin face, no edges or corners
                   Laplace(shift+iloc(1:nloc)) = u(shift+iloc(1:nloc))  !Dirichlet conditions
                ELSE IF( ALL( face == (/1,0,0/) ) ) THEN  ! Xmax face, no edges or corners
                   Laplace(shift+iloc(1:nloc)) = u(shift+iloc(1:nloc))  !Dirichlet conditions
!!$                Laplace(shift+iloc(1:nloc)) = du(ie,iloc(1:nloc),1)  !Neuman conditions
                ELSE IF( ALL( face(2:dim) == (/-1,0/) ) ) THEN  ! Ymin face, edges || to Z, no corners
                   Laplace(shift+iloc(1:nloc)) = u(shift+iloc(1:nloc))  !Dirichlet conditions
!!$                Laplace(shift+iloc(1:nloc)) = du(ie,iloc(1:nloc),2)  !Neuman conditions
                ELSE IF( ALL( face(2:dim) == (/1,0/) ) ) THEN  ! Ymax face, edges || to Z, no corners
                   Laplace(shift+iloc(1:nloc)) = u(shift+iloc(1:nloc))  !Dirichlet conditions
!!$                Laplace(shift+iloc(1:nloc)) = du(ie,iloc(1:nloc),2)  !Neuman conditions
                ELSE IF( face(3) == -1 ) THEN  ! entire Zmin face
                   Laplace(shift+iloc(1:nloc)) = u(shift+iloc(1:nloc))  !Dirichlet conditions
!!$                Laplace(shift+iloc(1:nloc)) = du(ie,iloc(1:nloc),3)  !Neuman conditions
                ELSE IF( face(3) == 1 ) THEN  ! entire Zmax face
                   Laplace(shift+iloc(1:nloc)) = u(shift+iloc(1:nloc))  !Dirichlet conditions
!!$                Laplace(shift+iloc(1:nloc)) = du(ie,iloc(1:nloc),3)  !Neuman conditions
                END IF
             END IF
          END IF
       END DO
    END DO
  END FUNCTION Laplace

  FUNCTION Laplace_diag (jlev,  nlocal, ne_local, meth_in)
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: jlev, nlocal, ne_local, meth_in
    REAL (pr), DIMENSION (nlocal*ne_local) :: Laplace_diag

    INTEGER :: i, ii, ie, shift, meth1, meth2
    REAL (pr), DIMENSION (nlocal,dim) :: du, d2u
    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc

    meth1 = meth_in + 2
    meth2 = meth_in + 4


    !PRINT *,'IN Laplace_diag, ne_local = ', ne_local

    DO ie = 1, ne_local
       shift=(ie-1)*nlocal
       !--- div(grad)
       !PRINT *,'CAlling c_diff_diag from Laplace_diag() '
       !PRINT *,'--- jlev, nlocal, meth1, meth2', jlev, nlocal, meth1, meth2

       CALL c_diff_diag ( du, d2u, jlev, nlocal, meth1, meth2, -11)

       Laplace_diag(shift+1:shift+Nwlt_lev(jlev,0)) = d2u(1:Nwlt_lev(jlev,0),1) + d2u(1:Nwlt_lev(jlev,0),2)
       IF (dim==3) Laplace_diag(shift+1:shift+Nwlt_lev(jlev,0)) = &
            Laplace_diag(shift+1:shift+Nwlt_lev(jlev,0))+d2u(1:Nwlt_lev(jlev,0),3)
       !--Boundary points
       i_p_face(0) = 1
       DO i=1,dim
          i_p_face(i) = i_p_face(i-1)*3
       END DO
       DO face_type = 0, 3**dim - 1
          face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
          IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
             CALL get_all_indices_by_face (face_type, jlev, nloc, iloc)
             IF(nloc > 0 .AND. dim == 3) THEN ! 3-D case only
                IF( ALL( face == (/-1,0,0/) ) ) THEN                         ! Xmin face, no edges or corners
                   Laplace_diag(shift+iloc(1:nloc)) = 1.0_pr                 !Dirichlet conditions
                ELSE IF( ALL( face == (/1,0,0/) ) ) THEN                     ! Xmax face, no edges or corners
                   Laplace_diag(shift+iloc(1:nloc)) = 1.0_pr                 !Dirichlet conditions
!!$                Laplace_diag(shift+iloc(1:nloc)) = du(iloc(1:nloc),1)     !Neuman conditions
                ELSE IF( ALL( face(2:dim) == (/-1,0/) ) ) THEN               ! Ymin face, edges || to Z, no corners
                   Laplace_diag(shift+iloc(1:nloc)) = 1.0_pr                 !Dirichlet conditions
!!$                Laplace_diag(shift+iloc(1:nloc)) = du(iloc(1:nloc),2)     !Neuman conditions
                ELSE IF( ALL( face(2:dim) == (/1,0/) ) ) THEN                ! Ymax face, edges || to Z, no corners
                   Laplace_diag(shift+iloc(1:nloc)) = 1.0_pr                 !Dirichlet conditions
!!$                Laplace_diag(shift+iloc(1:nloc)) = du(iloc(1:nloc),2)     !Neuman conditions
                ELSE IF( face(3) == -1 ) THEN                                ! entire Zmin face
                   Laplace_diag(shift+iloc(1:nloc)) = 1.0_pr                 !Dirichlet conditions
!!$                Laplace_diag(shift+iloc(1:nloc)) = du(iloc(1:nloc),3)     !Neuman conditions
                ELSE IF( face(3) == 1 ) THEN                                 ! entire Zmax face
                   Laplace_diag(shift+iloc(1:nloc)) = 1.0_pr                 !Dirichlet conditions
!!$                Laplace_diag(shift+iloc(1:nloc)) = du(iloc(1:nloc),3)     !Neuman conditions
                END IF
             END IF
          END IF
       END DO
    END DO
  END FUNCTION Laplace_diag



  FUNCTION user_rhs (u_integrated,p)
    USE parallel
    IMPLICIT NONE
    REAL (pr), DIMENSION (ng,ne), INTENT(IN) :: u_integrated !1D flat version of integrated variables without BC 
    REAL (pr), DIMENSION (ng), INTENT(IN) :: p
    REAL (pr), DIMENSION (n) :: user_rhs

    INTEGER :: ie, ie1, shift, i
    INTEGER, PARAMETER :: meth=1
    REAL (pr), DIMENSION (ne,ng,dim) :: du, d2u
    REAL (pr), DIMENSION (ng,dim)     :: dp
    REAL (pr), DIMENSION(dim) :: Uav
    REAL (pr) :: tmp1

    IF( Zero_Mean ) THEN
       DO ie = 1, dim
          tmp1 = SUM(u_integrated(:,ie)*dA)
          CALL parallel_global_sum( REAL=tmp1 )
          Uav(ie) = tmp1/sumdA_global
       END DO
    ELSE
       Uav =0.0_pr
    END IF
 
    !PRINT *,'user_rhs MINMAX(p) ', MINVAL( p), MAXVAL(dp)
     dp(1:ng,1:dim) = grad (p, ng, j_lev, meth+2)

    !PRINT *,'user_rhs MINMAX(dp) ', MINVAL( dp), MAXVAL(dp)
    !PRINT *,'user_rhs MINMAX(u_integrated) ', MINVAL( u_integrated), MAXVAL(u_integrated)

    CALL c_diff_fast(u_integrated, du, d2u, j_lev, ng, 1, 11, ne, 1, ne) !derivatives are calculated even for SGS terms

    !PRINT *,'user_rhs MINMAX(du) ', MINVAL( du), MAXVAL(du)

    !--Form right hand side of Navier-Stokes equations (nonlinear terms only)
    DO ie = 1, dim
       shift=(ie-1)*ng
       user_rhs(shift+1:shift+ng) = 0.0_pr
       DO ie1 = 1,dim
          user_rhs(shift+1:shift+ng) = user_rhs(shift+1:shift+ng) - (u_integrated(:,ie1)-Uav(ie1)+Umn(ie1))*du(ie,:,ie1) 
       END DO
    END DO

     !
     ! Apply grid filter if an explicit filter to convective term
     ! Note: can also filter SGS term, but the order needs to be changed.
     !
!     IF ( (sgsmodel /= 0) .AND. ExplicitFilterNL ) THEN
     IF ( ExplicitFilterNL ) THEN
        IF (par_rank.EQ.0 .AND. verb_level.GT.0) PRINT *,'Apply grid filter as an explicit filter to the convective term'
        
        CALL mdl_filt_by_type(user_rhs(1:dim*ng),dim,GRIDfilter)
        PRINT *,'...................', dim, dim*ng  
        IF(par_rank.EQ.0.AND.GRIDfilter == MDL_FILT_LPlocal) THEN
           WRITE(*,'("WARNING: low pass filter is not a projection filter and should not be used to explicitly filter u")')
        END IF
     END IF

    !--Form right hand side of Navier-Stokes equations (linear terms only)
    DO ie = 1, dim
       shift=(ie-1)*ng
       user_rhs(shift+1:shift+ng) = user_rhs(shift+1:shift+ng) + nu*SUM(d2u(ie,:,:),2) - dp(:,ie) + Cf*u_integrated(:,ie)
    END DO


    IF(sgsmodel /= 0) CALL SGS_rhs (user_rhs, u_integrated, du, d2u)

    !--Set operator on boundaries
    IF(SUM(nbnd) /= 0) CALL user_algebraic_BC_rhs (user_rhs, ne, ng, j_lev)
    
    
    
    IF( do_Adp_Eps_Spatial_Evol ) & 
    CALL user_rhs__VT (user_rhs, u_integrated, du, d2u, Cf)
  
END FUNCTION user_rhs


  ! find Jacobian of Right Hand Side of the problem
  ! u_prev == u_prev_timestep_loc

  FUNCTION user_Drhs (u, u_prev, meth)
    USE parallel
    IMPLICIT NONE
    INTEGER, INTENT(IN) ::  meth
    !u_prev_timestep passed local to cast it into 2dim array to work with vector derivatives
    REAL (pr), DIMENSION (ng,ne) :: u, u_prev
    REAL (pr), DIMENSION (n) :: user_Drhs

    INTEGER :: ie, ie1, shift
    REAL (pr), DIMENSION (2*ne,ng,dim) :: du  ! 1st derivatives for u (in du(1:ne,:,:)) and u_prev_timestep (in du(ne+1:2*ne,:,:))
    REAL (pr), DIMENSION (ne  ,ng,dim) :: d2u ! 2nd derivatives for u 
    REAL (pr), DIMENSION (ne  ,ng,dim) :: du_dummy ! passed when only calculating 1st derivative.
    !Find batter way to do this!! du_dummy with no storage..
    REAL (pr), DIMENSION(dim) :: Uav_prev, Uav
    REAL (pr) :: tmp1, tmp2

    IF( Zero_Mean ) THEN
       DO ie = 1, dim
          tmp1 = SUM(u_prev(:,ie)*dA)
          CALL parallel_global_sum( REAL=tmp1 )
          Uav_prev(ie) = tmp1/sumdA_global
          tmp2 = SUM(u(:,ie)*dA)
          CALL parallel_global_sum( REAL=tmp2 )
          Uav(ie) = tmp2/sumdA_global
       END DO
    ELSE
       Uav_prev =0.0_pr
       Uav =0.0_pr
    END IF

    IF ( TYPE_DB .NE. DB_TYPE_LINES )THEN 
       ! find 1st and 2nd deriviative of u and
       CALL c_diff_fast(u, du(1:ne,:,:), d2u(1:ne,:,:), j_lev, ng, meth, 11, ne , 1, ne )

       ! find only first deriviativ e  u_prev_timestep
       CALL c_diff_fast(u_prev, du(ne+1:2*ne,:,:), du_dummy(1:ne,:,:), j_lev, ng, meth, 10, ne , 1, ne )

    ELSE !db_lines

       ! Load u and u_prev_timestep in to db
       ! update the db from u
       ! u(:, mn_var:mx_var) -> db%u(db_offset:mx_var-mn_var+1)
       ! db_offset = 1 
       CALL update_db_from_u(  u       , ng ,ne  , 1, ne, 1  ) !Load u
       CALL update_db_from_u(  u_prev , ng ,ne  , 1, ne, ne+1  ) !Load u_prev offset in db by ne+1

       ! find 1st and 2nd deriviative of u and
       ! find only first deriviativ e  u_prev_timestep
       CALL c_diff_fast_db(du, d2u, j_lev, ng, meth, 11, 2*ne,&
            1,   &  ! MIN(mn_varD,mn_varD2)
            2*ne,&  ! MAX(mx_varD,mx_varD2)
            1,   &  ! mn_varD  :   min variable in db that we are doing the 1st derivatives for.
            2*ne,&  ! mn_varD  :   max variable in db that we are doing the 1st derivatives for.
            1 ,  &  ! mn_varD2 :   min variable in db that we are doing the 2nd derivatives for.
            dim   )  ! mn_varD2 :   max variable in db that we are doing the 2nd derivatives for.


    END IF
    !CALL c_diff_fast_db(du_b, du_b, j_lev, ng, meth, 10, ne, 1, ne) !find 1st derivative u_b

    !--Form right hand side of Navier-Stokes equations (nonlinear terms only)
    DO ie = 1, dim
       shift=(ie-1)*ng
       
       user_Drhs(shift+1:shift+ng) =  0.0_pr
       DO ie1 = 1, dim
          user_Drhs(shift+1:shift+ng) = user_Drhs(shift+1:shift+ng) - (u_prev(:,ie1)-Uav_prev(ie1)+Umn(ie1))*du(ie,:,ie1) & 
                                                                    - (u(:,ie1)-Uav(ie1))*du(ne+ie,:,ie1) 
       END DO
    END DO

     !
     ! Apply grid filter if an explicit filter to convective term
     ! Note: can also filter SGS term, but the order needs to be changed.
     !
!     IF ( (sgsmodel /= 0) .AND. ExplicitFilterNL ) THEN
     IF ( ExplicitFilterNL ) THEN
        IF (par_rank.EQ.0 .AND. verb_level.GT.0) PRINT *,'Apply grid filter as an explicit filter to the convective term'
        
        CALL mdl_filt_by_type(user_Drhs(1:dim*ng),dim,GRIDfilter)
        
        IF(par_rank.EQ.0.AND.GRIDfilter == MDL_FILT_LPlocal) THEN
           WRITE(*,'("WARNING: low pass filter is not a projection filter and should not be used to explicitly filter u")')
        END IF
     END IF


    !--Form right hand side of Navier-Stokes equations (linear terms only)
    DO ie = 1, dim
       shift=(ie-1)*ng       
       user_Drhs(shift+1:shift+ng) =   user_Drhs(shift+1:shift+ng) + nu*SUM(d2u(ie,:,:),2) + Cf*u(:,ie)
    END DO


    IF(sgsmodel /= 0) CALL SGS_Drhs (user_Drhs, u, u_prev, du, d2u, meth)
    
    
    
    IF( do_Adp_Eps_Spatial_Evol ) & 
    CALL user_Drhs__VT (user_Drhs, u, u_prev, du, d2u, meth)

  END FUNCTION user_Drhs


  !
  ! Uses u_prev_timestep, which is global flat (1D ) array set in time_step_cn()
  !
  FUNCTION user_Drhs_diag (meth)
    USE parallel
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: meth
    REAL (pr), DIMENSION (n) :: user_Drhs_diag

    INTEGER :: ie, ie1, shift,shiftIlm,shiftImm
    REAL (pr), DIMENSION (ng,dim) :: du, d2u
    REAL (pr), DIMENSION (ne,ng,dim) :: du_prev_timestep
    REAL (pr), DIMENSION (ne  ,ng,dim) :: du_dummy ! passed when only calculating 1st derivative.
    REAL (pr), DIMENSION(dim) :: Uav_prev
    REAL (pr) :: tmp1

    IF( Zero_Mean ) THEN
       DO ie = 1, dim
          shift=(ie-1)*ng
          tmp1 = SUM(u_prev_timestep(shift+1:shift+ng)*dA)
          CALL parallel_global_sum( REAL=tmp1 )
          Uav_prev(ie) = tmp1/sumdA_global
       END DO
    ELSE
       Uav_prev =0.0_pr
    END IF

    CALL c_diff_fast(u_prev_timestep, du_prev_timestep, du_dummy, j_lev, ng, meth, 10, ne, 1, ne)

    !
    ! does not rely on u so we can call it once here
    !
    shift = 0 !tmp
    CALL c_diff_diag(du, d2u, j_lev, ng, meth, meth, 11)

    !--Form right hand side of Navier--Stokes equations
    DO ie = 1, dim
       shift=(ie-1)*ng
       user_Drhs_diag(shift+1:shift+ng) = nu*SUM(d2u,2)  + Cf - du_prev_timestep(ie,:,ie)
       DO ie1 = 1, dim
          user_Drhs_diag(shift+1:shift+ng) = user_Drhs_diag(shift+1:shift+ng) - (u_prev_timestep((ie1-1)*ng+1:ie1*ng)-Uav_prev(ie1)+Umn(ie1))*du(:,ie1)
       END DO
    END DO

    IF(sgsmodel /= 0) CALL SGS_Drhs_diag (user_Drhs_diag, du_prev_timestep, du, d2u, meth)
    

    
    IF( do_Adp_Eps_Spatial_Evol ) &
    CALL user_Drhs_diag__VT (user_Drhs_diag, du_prev_timestep, du, d2u, meth)

  END FUNCTION user_Drhs_diag

  FUNCTION user_chi (nlocal, t_local )
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: nlocal
    REAL (pr), INTENT (IN) :: t_local
    REAL (pr), DIMENSION (nlocal) :: user_chi

    user_chi = 0.0_pr
  END FUNCTION user_chi



  !
  ! Calculate any statitics
  !
  ! startup_flag - 0 when adapting to IC,then 1 inmain integration loop
  !
  !
  SUBROUTINE user_stats ( u ,j_mn, startup_flag)
!    USE fft_module
!    USE spectra_module
    USE util_vars
    USE wlt_vars
    USE vector_util_mod
    USE parallel
    IMPLICIT NONE
    REAL (pr), DIMENSION (nwlt,1:n_var), INTENT (IN) :: u
    INTEGER , INTENT (IN) :: j_mn 
    INTEGER , INTENT (IN) :: startup_flag
    CHARACTER (LEN=256)  :: filename
    ! CHARACTER (LEN=256)             :: file_out
    INTEGER outputfileUNIT, idim
    REAL (pr) :: area !area of domain to normalize dA
    REAL (pr) :: ttke
    INTEGER :: count_u_g_mdl_gridfilt_mask, count_u_g_mdl_testfilt_mask
   !INTEGER :: nwlt_global   !05.13.2011  no need for this, nwlt_global is a global variable
    REAL (pr), DIMENSION (1:nwlt,6) :: S_ij 
    REAL (pr), DIMENSION (1:nwlt) :: Smod 
    REAL (pr) :: resolved_diss_av, sgs_diss_av, KE_av, Cf0
    REAL (pr) :: lambda, uprime, duprime, Ret, t_eddy_turnover, eta
    REAL (pr), DIMENSION (nwlt,2) :: utst
    REAL (pr), DIMENSION (1:nwlt,dim) :: dv, d2v
    REAL(pr) :: tmp1, correction_factor, frac_SGS_DISS
    LOGICAL , SAVE :: start = .TRUE.
    !USER may define additional statistics ouutput here.


    Smod = 0.0_pr
    DO idim = 1,dim
       Smod = Smod+u(:,idim)**2
    END DO
    
    tmp1 = SUM( dA*Smod )
    CALL parallel_global_sum( REAL=tmp1 )
    
    uprime =SQRT( tmp1/sumdA_global/REAL(dim,pr))
    KE_av = 0.5_pr*tmp1/sumdA_global

    CALL Sij( u(1:nwlt,1:dim) ,nwlt, S_ij(:,1:6), Smod, .TRUE. )
    
    IF(sgsmodel == 0 .OR. total_resolved_diss == 0.0_pr) THEN ! calculates resolved dissipation for no SGS model case
       tmp1 = SUM( dA*nu* Smod**2 )
       CALL parallel_global_sum( REAL=tmp1 )
       resolved_diss_av = tmp1/sumdA_global
       sgs_diss_av      = 0.0_pr    
    ELSE !use dissipation calculated in SGS model case
       resolved_diss_av = total_resolved_diss/sumdA_global
       sgs_diss_av      = total_sgs_diss/sumdA_global
    END IF

!================ Definition of duprime ==========================

    Smod = 0.0_pr
    DO idim = 1,dim
       Smod = Smod+S_ij(:,idim)**2
    END DO
    
    tmp1 = SUM( dA*Smod )
    CALL parallel_global_sum( REAL=tmp1 )
    
    duprime =SQRT( tmp1/sumdA_global/REAL(dim,pr))
    
!====================================================================
    
    lambda = uprime/duprime
    IF(resolved_diss_av > 0.0_pr.AND.par_rank.EQ.0.AND.verb_level.GT.0) PRINT *, 'lambda_old=',lambda, ' lambda_new=',SQRT(10.0_pr*nu*KE_av/resolved_diss_av)
    Ret = uprime * lambda / nu
    t_eddy_turnover = uprime**2/resolved_diss_av
    eta = (nu**3/resolved_diss_av)**0.25_pr
    Cf0= 0.5*resolved_diss_av/KE_av
    frac_SGS_DISS=sgs_diss_av/(resolved_diss_av+sgs_diss_av)
    
    ! output at processor 0 only
    IF (par_rank.EQ.0) THEN
       IF (verb_level.GT.0) THEN
          WRITE(*,'(" ")')
          WRITE(*,'("****************** Turbulence Statistics on the Fly *******************")')
          WRITE(*,'("*  Re_taylor=",E12.5," eta/h=",E12.5,25x," *")') &
               Ret,eta/maxval((xyzlimits(2,:)-xyzlimits(1,:))/REAL(mxyz(:)*2**(j_lev-1),pr))
          WRITE(*,'("*  t_eddy_turnover=",E12.5," t=",E12.5,23x," *")') &
               t_eddy_turnover,t
          WRITE(*,'("*  <DISS>=",E12.5," <KE>=",E12.5," <Cf>=",E12.5,12x,"*")') resolved_diss_av, KE_av, Cf0
       END IF
       
       IF(sgsmodel /= 0) THEN
          IF(resolved_diss_av > 0.0_pr) THEN
             correction_factor = SQRT((resolved_diss_av+sgs_diss_av)/resolved_diss_av)
             Ret=Ret/correction_factor
             eta = eta/SQRT(correction_factor)
             t_eddy_turnover=t_eddy_turnover/correction_factor**2
             lambda = lambda/correction_factor
             duprime = duprime*correction_factor
             Cf0=Cf0*correction_factor**2
             IF (verb_level.GT.0) THEN
                WRITE(*,'("***** SGS Corrected Turbulence Statistics on the Fly ****************")')
                WRITE(*,'("*  Re_taylor=",E12.5," eta/h=",E12.5,25x," *")') &
                     Ret,eta/maxval((xyzlimits(2,:)-xyzlimits(1,:))/REAL(mxyz(:)*2**(j_lev-1),pr))
                WRITE(*,'("*  t_eddy_turnover=",E12.5," t=",E12.5,23x," *")') &
                     t_eddy_turnover,t
                WRITE(*,'("*  <DISS_SGS(%)>=",E12.5," <Cf>=",E12.5,12x,"*")') frac_SGS_DISS,Cf0
                WRITE(*,'("*********************************************************************")')
                WRITE(*,'(" ")')
             END IF
          END IF
       END IF
    END IF
    
   !nwlt_global = nwlt
   !CALL parallel_global_sum (INTEGER=nwlt_global)
    
    ! output at processor 0 only
    IF (par_rank.EQ.0) THEN
       OPEN  (UNIT=UNIT_USER_STATS, FILE = file_name_user_stats, FORM='formatted', STATUS='old', POSITION='append')
       IF( start ) THEN
          WRITE(UNIT=UNIT_USER_STATS,ADVANCE='YES', FMT='( a )' ) &
               '%Time          t_eddy           Nwlt    Nwlt/Nmax     Re_taylor     eta/h         <KE>          <DISS_res>    <DISS_SGS>    fracSGS_DISS  epsilon       expl_filt_diss'
       END IF
       start = .FALSE.
       IF(resolved_diss_av > 0.0_pr) &
            WRITE (UNIT_USER_STATS,'(2(es13.6,1x),I10,1x,9(es13.6,1x))') t, t_eddy_turnover, nwlt_global, &
            REAL(nwlt_global,pr)/PRODUCT(REAL(mxyz(1:dim)*2**(j_mx-1),pr)), Ret, &
            eta/maxval((xyzlimits(2,1:dim)-xyzlimits(1,1:dim))/REAL(mxyz(1:dim)*2**(j_mx-1),pr)), &
            KE_av, resolved_diss_av, sgs_diss_av, frac_SGS_DISS, eps, expl_filt_diss
       CLOSE (UNIT=UNIT_USER_STATS)
    END IF

  END SUBROUTINE user_stats

  SUBROUTINE user_cal_force (u, n, t_local, force, drag, lift)
    !--Calculates drag and lift on obstacle using penalization formula
    IMPLICIT NONE

    INTEGER, INTENT (IN) :: n
    REAL (pr), INTENT (IN) :: t_local
    REAL (pr), DIMENSION (dim), INTENT (INOUT) :: force
    REAL (pr), INTENT (OUT) :: drag, lift
    REAL (pr), DIMENSION (n,dim) :: u
    drag = 0.0_PR
    lift = 0.0_PR

    !
    ! There is no obstacle in flow
    !

  END SUBROUTINE user_cal_force

  !
  ! calculate any additional variables
  ! 
  ! arg 
  ! flag  - 0 calledwhile adapting to IC, 1 - called in main time integration loop
  !
  ! These additiona variables are calculated and left in real space.
  !
  SUBROUTINE user_additional_vars( t_local, flag )
    USE unused
    IMPLICIT NONE
    REAL (pr), INTENT (IN) ::  t_local
    INTEGER , INTENT(IN) :: flag ! 0- called during adaption to IC, 1 called during main integration loop
    REAL(pr) tmp(nwlt,2*dim) !tmp for vorticity

    
    IF( flag == 1 .OR. adaptMagVort .OR. saveMagVort .OR. adaptNormS .OR. saveNormS) THEN ! only in main time int loop, not initial adaptation
       ! Calculate the magnitude vorticity
       IF( adaptMagVort .OR. saveMagVort ) THEN 
          CALL cal_vort (u(:,n0:n0+dim-1), tmp(:,1:dim), nwlt)
          u(:,n_var_modvort) = SQRT(tmp(:,1)**2.0_pr + tmp(:,2)**2.0_pr + tmp(:,3)**2.0_pr )
       END IF

       ! Calculate the magnitude of Sij (sqrt(SijSij) )
       ! s(:,1) = s_11
       ! s(:,2) = s_22
       ! s(:,3) = s_33
       ! s(:,4) = s_12
       ! s(:,5) = s_13
       ! s(:,6) = s_23

       IF( adaptNormS .OR. saveNormS) THEN
          CALL Sij( u(:,1:dim) ,nwlt, tmp(:,1:2*dim), u(:,n_var_modSij), .TRUE. )
       END IF

       ! Calculate the magnitude of velocity
       IF( adaptMagVel ) THEN 
          u(:,n_var_modvel) = 0.5_pr*(u(:,1)**2.0_pr + u(:,2)**2.0_pr + u(:,3)**2.0_pr )
       END IF

    END IF

  END SUBROUTINE user_additional_vars


  !
  ! calculate any additional scalar variables
  ! 
  ! arg 
  ! flag  - 0 calledwhile adapting to IC, 1 - called in main time integration loop
  !
  SUBROUTINE user_scalar_vars( flag )
    IMPLICIT NONE
    INTEGER , INTENT(IN) :: flag ! 0- called during adaption to IC, 1 called during main integration loop



  END SUBROUTINE user_scalar_vars

  !
  !************ Calculating Scales ***************************
  !
  ! Note the order of the components in the scl array
  ! correspond to u_tn, v_tn, w_tn, u_tn-1, v_tn-1, w_tn-1, u_tn-2, v_tn-2, w_tn-2
  ! 
  SUBROUTINE user_scales(flag, use_default, u_loc, nlocal, ne_local, l_n_var_adapt , l_n_var_adapt_index, &
       scl, scl_fltwt) !add
    USE parallel
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: flag ! 0 during initial adaptation, then 1 during time advancement
    LOGICAL , INTENT(INOUT) :: use_default
    INTEGER, INTENT (IN) :: nlocal, ne_local
    REAL (pr), DIMENSION (1:nlocal,1:ne_local), INTENT (IN) :: u_loc
    LOGICAL , INTENT (IN) :: l_n_var_adapt(ne_local)
    INTEGER , INTENT (IN) :: l_n_var_adapt_index(1:ne_local)
    REAL (pr), DIMENSION (1:ne_local), INTENT (INOUT) :: scl
    REAL (pr) ,  INTENT(IN) ::scl_fltwt !weight for temporal filter on scl
    REAL (pr) :: floor, tmp, tmp1
    INTEGER :: ie, ie_index, itmp
    REAL (pr) ,save, ALLOCATABLE, DIMENSION (:) :: scl_old
    LOGICAL  ,save :: startup_init = .TRUE.

    !
    ! Ignore the output of this routine and use default scales routine
    !
    use_default = .FALSE. 

    IF( .NOT. use_default ) THEN
       
       IF (verb_level.GT.0 .AND. par_rank.EQ.0) PRINT *,'Use user_scales() <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<'
       !
       ! ALLOCATE scl_old if it was not done previously in user_scales
       !
       IF( startup_init .AND. .NOT. ALLOCATED(scl_old) ) THEN
          ALLOCATE( scl_old(1:ne_local) )
          scl_old = 0.0_pr
       END IF
       
       
       floor = 1.e-12_pr
       scl   =1.0_pr
       !
       ! Calculate scl per component
       !
       DO ie=1,ne_local
          IF(l_n_var_adapt(ie)) THEN
             ie_index = l_n_var_adapt_index(ie)
             IF( Scale_Meth == 1 ) THEN ! Use Linf scale
                scl(ie) = MAXVAL ( ABS( u_loc(1:nlocal,ie_index) ) )
                CALL parallel_global_sum( REALMAXVAL=scl(ie) )
                
             ELSE IF ( Scale_Meth == 2 ) THEN ! Use L2 scale
                tmp1 = SUM( (u_loc(1:nlocal,ie_index)**2) )
                itmp = nlocal
                CALL parallel_global_sum( REAL=tmp1 )
                CALL parallel_global_sum( INTEGER=itmp )
                scl(ie)= SQRT ( tmp1/itmp  )
                
             ELSE IF ( Scale_Meth == 3 ) THEN ! Use L2 scale using dA
                tmp1 = SUM( (u_loc(1:nlocal,ie_index)**2)*dA )
                CALL parallel_global_sum( REAL=tmp1 )
                scl(ie)= SQRT ( tmp1 / sumdA_global  )
                
             ELSE
                IF (par_rank.EQ.0) THEN
                   PRINT *, 'Error in user_scales(), Unknown Scale_Meth type: ', Scale_Meth
                   PRINT *, 'Known Scale_Meth types: 1 = Linf scale, 2 = L2 scale '
                   PRINT *, 'Exiting ...'
                END IF
                CALL parallel_finalize; STOP
             END IF
             
             IF (verb_level.GT.0 .AND. par_rank.EQ.0) THEN
                WRITE (6,'("Scaling before taking vector(1:dim) magnitude")')
                WRITE (6,'("Scale factor eqn", I4 ," = ", es11.4 ," scalecoeff = ",es11.4 )') &
                     ie, scl(ie), scaleCoeff(ie)
             END IF
             
          END IF
       END DO
       !
       ! take appropriate vector norm over scl(1:dim)
       IF( Scale_Meth == 1 ) THEN ! Use Linf scale
          tmp = MAXVAL(scl(1:dim)) !this is statistically equivalent to velocity vector length
          scl(1:dim) = tmp
       ELSE IF ( Scale_Meth == 2 ) THEN ! Use L2 scale
          tmp = SQRT(SUM( scl(1:dim)**2 ) ) !now velocity vector length, rather than individual component
          scl(1:dim) = tmp
       ELSE IF ( Scale_Meth == 3 ) THEN ! Use L2 scale using dA
          tmp = SQRT(SUM( scl(1:dim)**2 ) ) !now velocity vector length, rather than individual component
          scl(1:dim) = tmp
       END IF
       !
       ! Print out new scl
       !
       DO ie=1,ne_local
          IF(l_n_var_adapt(ie)) THEN
             !this is done if one of the variable is exactly zero inorder not to adapt to the nois
             IF(scl(ie) .le. floor) scl(ie)=1.0_pr 
             tmp = scl(ie)
             ! temporally filter scl
             IF(.not. startup_init ) scl(ie) = scl_fltwt * scl_old(ie) + (1.0_pr - scl_fltwt) * scl(ie)
             scl = scaleCoeff * scl

             IF (verb_level.GT.0 .AND. par_rank.EQ.0) THEN
                WRITE (6,'("Scaling on vector(1:dim) magnitude")')
                WRITE (6,'("Scale factor eqn", I4 ," = ", es11.4 ," scalecoeff = ",es11.4 )') &
                     ie, scl(ie), scaleCoeff(ie)
                WRITE (6,'("(before filtering = ",es11.4,")" )') tmp
             END IF
             
          END IF
       END DO
       
       scl_old = scl !save scl for this time step
       startup_init = .FALSE.
       !PRINT *,'TEST scl_old ', scl_old
    END IF ! use default
    
  END SUBROUTINE user_scales

  SUBROUTINE user_cal_cfl (use_default, u, cfl_out)
    USE parallel
    IMPLICIT NONE
    LOGICAL , INTENT(INOUT) :: use_default
    REAL (pr),                                INTENT (INOUT) :: cfl_out
    REAL (pr), DIMENSION (nwlt,n_integrated), INTENT (IN)    :: u
    
    INTEGER                    :: i
    REAL (pr)                  :: floor
    REAL (pr), DIMENSION (dim) :: cfl
    REAL (pr), DIMENSION(dim,nwlt) :: h_arr
    
    use_default = .FALSE.
    
    floor = 1e-12_pr
    cfl_out = floor
    
    CALL get_all_local_h (h_arr)
    
    DO i = 1, nwlt
       cfl(1:dim) = ABS (u(i,1:dim)+Umn(1:dim)) * dt/h_arr(1:dim,i)
       cfl_out = MAX (cfl_out, MAXVAL(cfl))
    END DO
    CALL parallel_global_sum( REALMAXVAL=cfl_out )
    
  END SUBROUTINE user_cal_cfl
  
  SUBROUTINE  user_sgs_force ( u, nlocal)
    IMPLICIT NONE
    
    INTEGER,                         INTENT (IN) :: nlocal
    REAL (pr), DIMENSION (nlocal,n_integrated), INTENT (INOUT) :: u
    
    !default SGS_force, can be explicitely deifined below instead of calling default SGS_force
    IF(sgsmodel /= 0) CALL sgs_force ( u, nlocal)
    
  END SUBROUTINE user_sgs_force

  FUNCTION user_sound_speed (u, neq, nwlt)
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: nwlt, neq
    REAL (pr), DIMENSION (nwlt,neq), INTENT(IN) :: u
    REAL (pr), DIMENSION (nwlt) :: user_sound_speed

    !user_sound_speed(:) = SQRT( gamma*(gamma-1.0_pr)* &
    !       ( u(:,4)-0.5_pr*SUM(u(:,n_var_mom(1:dim))**2,DIM=2)/u(:,1)) ) ! pressure

    user_sound_speed(:) = 0.0_pr

  END FUNCTION user_sound_speed

  SUBROUTINE  user_pre_process
    USE parallel
    USE elliptic_vars
    IMPLICIT NONE
    REAL (pr), DIMENSION(dim) :: Uav
    REAL (pr) :: tmp1
    INTEGER :: i, ie
    
    IF( Zero_Mean ) THEN
       DO ie = 1, dim
          tmp1 = SUM(u(:,ie)*dA)
          CALL parallel_global_sum( REAL=tmp1 )
          u(:,ie) = u(:,ie) - tmp1/sumdA_global
       END DO
    END IF

  END SUBROUTINE user_pre_process

  SUBROUTINE  user_post_process
    USE parallel
    USE elliptic_vars
    IMPLICIT NONE
    REAL (pr), DIMENSION(dim) :: Uav
    REAL (pr) :: tmp1
    INTEGER :: i, ie

    IF( Zero_Mean ) THEN
       DO ie = 1, dim
          tmp1 = SUM(u(:,ie)*dA)
          CALL parallel_global_sum( REAL=tmp1 )
          u(:,ie) = u(:,ie) - tmp1/sumdA_global
       END DO
    END IF

    
    IF ( do_Adp_Eps_Spatial .OR. do_Adp_Eps_Spatial_Evol ) &
    CALL user_post_process__VT(n_var_SGSD, Cf)    ! n_var_ExtraAdpt_Der    ! n_var_Epsilon   ! n_var_RSGSD
    
  END SUBROUTINE user_post_process

END MODULE user_case





