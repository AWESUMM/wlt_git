  %plotnum = 1; lclr = 'k-'; lg_input = '\epsilon=0.43'; status='new';timeshift=-1; close all; FileNameTarget='SCALES_GDM_eps_Nu0.09_Nupdt0._user_stats'
  %plotnum = 2; lclr = 'r--'; lg_input = '%DISS_{SGS}^{goal}=10% \tau_{\epsilon}=0.1'; status='old';timeshift=1.3088; FileNameTarget='SCALES_GDM_vareps_Nu0.09_Nupdt0_DIS0.10._user_stats'
  %plotnum = 3; lclr = 'b-.'; lg_input = '%DISS_{SGS}^{goal}=20% \tau_{\epsilon}=0.1'; status='old';timeshift=1.3088; FileNameTarget='SCALES_GDM_vareps_Nu0.09_Nupdt0_DIS0.20._user_stats'
  %plotnum = 4; lclr = 'g--'; lg_input = '%DISS_{SGS}^{goal}=30% \tau_{\epsilon}=0.1'; status='old';timeshift=1.3088; FileNameTarget='SCALES_GDM_vareps_Nu0.09_Nupdt0_DIS0.30._user_stats'
  %plotnum = 5; lclr = 'm:'; lg_input = '%DISS_{SGS}^{goal}=40% \tau_{\epsilon}=0.1'; status='old';timeshift=1.3088; FileNameTarget='SCALES_GDM_vareps_Nu0.09_Nupdt0_DIS0.40._user_stats'
  %plotnum = 6; lclr = 'r:'; lg_input = '%DISS_{SGS}^{goal}=50% \tau_{\epsilon}=0.1'; status='old';timeshift=1.3088; FileNameTarget='SCALES_GDM_vareps_Nu0.09_Nupdt0_DIS0.50._user_stats'
  %plotnum = 7; lclr = 'b:'; lg_input = '%DISS_{SGS}^{goal}=55% \tau_{\epsilon}=0.1'; status='old';timeshift=1.3088; FileNameTarget='SCALES_GDM_vareps_Nu0.09_Nupdt0_DIS0.55._user_stats'
  %plotnum = 8; lclr = 'k:'; lg_input = '\epsilon=0.43'; status='old'; timeshift=1.3088;  FileNameTarget='SCALES_GDM_eps0.43_Nu0.09_Nupdt0._user_stats'
  %plotnum = 2; lclr = 'r--'; lg_input = '%DISS_{SGS}^{goal}=10% \tau_{\epsilon}=1'; status='old';timeshift=1.3088; FileNameTarget='SCALES_GDM_vareps_Nu0.09_q1_Nupdt0_DIS0.10._user_stats'
  %plotnum = 3; lclr = 'b-.'; lg_input = '%DISS_{SGS}^{goal}=20% \tau_{\epsilon}=1'; status='old';timeshift=1.3088; FileNameTarget='SCALES_GDM_vareps_Nu0.09_q1_Nupdt0_DIS0.20._user_stats'
  %plotnum = 4; lclr = 'g--'; lg_input = '%DISS_{SGS}^{goal}=30% \tau_{\epsilon}=1'; status='old';timeshift=1.3088; FileNameTarget='SCALES_GDM_vareps_Nu0.09_q1_Nupdt0_DIS0.30._user_stats'
  %plotnum = 5; lclr = 'm:'; lg_input = '%DISS_{SGS}^{goal}=40% \tau_{\epsilon}=1'; status='old';timeshift=1.3088; FileNameTarget='SCALES_GDM_vareps_Nu0.09_q1_Nupdt0_DIS0.40._user_stats'
  %plotnum = 6; lclr = 'r:'; lg_input = '%DISS_{SGS}^{goal}=50% \tau_{\epsilon}=1'; status='old';timeshift=1.3088; FileNameTarget='SCALES_GDM_vareps_Nu0.09_q1_Nupdt0_DIS0.50._user_stats'
  %plotnum = 7; lclr = 'b:'; lg_input = '%DISS_{SGS}^{goal}=55% \tau_{\epsilon}=1'; status='old';timeshift=1.3088; FileNameTarget='SCALES_GDM_vareps_Nu0.09_q1_Nupdt0_DIS0.55._user_stats'
  %plotnum = 8; lclr = 'k:'; lg_input = '\epsilon=0.43'; status='old';timeshift=1.3088;  FileNameTarget='SCALES_GDM_eps0.43_Nu0.09_Nupdt0._user_stats'
  %plotnum = 2; lclr = 'r--'; lg_input = 'P_{goal}=0.1'; status='old';timeshift=-1; FileNameTarget='SCALES_GDM_eps_Nu0.09_Nupdt0._user_stats'
  %plotnum = 3; lclr = 'b-.'; lg_input = 'P_{goal}=0.2'; status='old';timeshift=-1; FileNameTarget='SCALES_GDM_eps_Nu0.09_Nupdt0._user_stats'
  %plotnum = 4; lclr = 'g--'; lg_input = 'P_{goal}=0.3'; status='old';timeshift=-1; FileNameTarget='SCALES_GDM_eps_Nu0.09_Nupdt0._user_stats'
  %plotnum = 5; lclr = 'm:'; lg_input = 'P_{goal}=0.4'; status='old';timeshift=-1; FileNameTarget='SCALES_GDM_eps_Nu0.09_Nupdt0._user_stats'
  %plotnum = 6; lclr = 'r:'; lg_input = 'P_{goal}=0.5'; status='old';timeshift=-1; FileNameTarget='SCALES_GDM_eps_Nu0.09_Nupdt0._user_stats'
  plotnum = 7; lclr = 'b:'; lg_input = 'P_{goal}=0.55'; status='old';timeshift=-1; FileNameTarget='SCALES_GDM_eps_Nu0.09_Nupdt0._user_stats'
  if exist('plotnum') & exist('lclr') & exist('lg_input') & exist('status') 
      user_input_all = logical(1);
  else
      user_input_all = logical(0);
  end
  if(~exist('timeshift'))
      timeshift = 0.0;
  end
  %status = 'new';
  if ~user_input_all
      if (exist('status') & status == 'new') | ~exist('status')
          close all;
          status = 'new';
          plotnum = 1;
      else
          plotnum = plotnum + 1;
      end
      if logical(1)
          if(plotnum==1) ;lclr = 'k--';end
          if(plotnum==2) ;lclr = 'r--';end
          if(plotnum==3) ;lclr = 'b-.';end
          if(plotnum==4) ;lclr = 'g- ';end
          if(plotnum==5) ;lclr = 'm: ';end
          if(plotnum==6) ;lclr = 'k--';end
          if(plotnum==7) ;lclr = 'b-.';end
          if(plotnum==8) ;lclr = 'g-.';end
          if(plotnum==9) ;lclr = 'k';end
          if(plotnum==10) ;lclr = 'r';end
          if(plotnum==11) ;lclr = 'b';end
          if(plotnum==12) ;lclr = 'g';end
      end
  end

  lineThickness = 2;

  % Load fitness history file and plot
  if exist('FileNameTarget') 
      FileNameTarget=[pwd '\results\' FileNameTarget];
  else
      [FileNameTarget PathNameTarget] = uigetfile('./results/*_user_stats','Insert user_stats file');
      FileNameTarget =  [PathNameTarget FileNameTarget];
  end
  fidTarget = fopen(FileNameTarget,'r');
  data = importdata(FileNameTarget);
  data(:,4) = data(:,3)/(256^3)*100;
  epsarr = [0.43 0.1 0.2 0.3 0.4 0.5 0.55];
  if plotnum > 1
      data(:,10) = data(:,11)/epsarr(1)*epsarr(plotnum);
  end
  dimension = size(data);
  if exist('lg_input')  
      lg(:,plotnum) = cell(1,dimension(2)-1);
      for i = 1:10
          lg{i,plotnum} = lg_input;
      end
  else
      lg(:,plotnum) = cell(1,dimension(2)-1);
      lg{1,plotnum} = [num2str(plotnum) ': \tau_{eddy}'];
      lg{2,plotnum} = [num2str(plotnum) ': N_{>}'];
      lg{3,plotnum} = [num2str(plotnum) ': N_{>}/N_{max}'];
      lg{4,plotnum} = [num2str(plotnum) ': Re_{\tau}'];
      lg{5,plotnum} = [num2str(plotnum) ': \eta/h'];
      lg{6,plotnum} = [num2str(plotnum) ': KE'];
      lg{7,plotnum} = [num2str(plotnum) ': DISS'];
      lg{8,plotnum} = [num2str(plotnum) ': DISS_{SGS}'];
      lg{9,plotnum} = [num2str(plotnum) ': %DISS_{SGS}'];
      lg{10,plotnum} = [num2str(plotnum) ': \epsilon'];
  end
  lname{1} ='eddy turnover time';
  lname{2} ='N_{wlt}';
  lname{3} ='compression (%)';
  lname{4} ='Re';
  lname{5} ='Kolmogorov length to grid ratio';
  lname{6} ='kinetic energy';
  lname{7} ='resolved dissipation';
  lname{8} ='SGS dissipation';
  lname{9} ='% of SGS dissipation';
  lname{10} ='\epsilon';
  timeshift
  t = data(:,1)+timeshift;
  y = data(:,2:dimension(2));
  for var_num = 1:dimension(2)-1
      figure(var_num);
      if(status ~= 'new'); hold on; end
      %figname=lname(var_num)
      set(gcf,'Name',lname{var_num}); %set the window title
      if status == 'new'
          set(gcf,'Position',[50 200 800 450]); %set position of the window
      end
      plot(t,y(:,var_num),[lclr ],'LineWidth',lineThickness);
      legend(lg(var_num,:),'Location','EastOutside')
      if status == 'new'
        xlabel('time');
        ylabel(lname{var_num});
      end
      set(gca,'Xlim',[0,2],'PlotBoxAspectRatio',[4 3 1],'Position',[0.05 0.1 0.7 0.8]); %set the window title
  end
  fclose( fidTarget );
  if status == 'new'
      status = 'old';
  end













