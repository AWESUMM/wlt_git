global spectra_type; spectra_type =3 % 2- Energey, 3 - Dissiption also enstyrophy
global presentation_type; presentation_type=0
global file_num
file_num = 1
for i=[1:10]
    figure(i);clf
end
filtered_DNS = logical(0)
testname1=''; testname2=''; testname3=''; testname4=''; testname5=''; testname6=''; testname7='';
t1_max = 1; t2_max = 1; t3_max = 1; t4_max = 1; t5_max = 1; t6_max = 1;t7_max = 1;
title1=''; title2=''; title3=''; title4=''; title5=''; title6=''; title7=''; 


group = 6
if group == 6  %group 5
    bw = logical(0)
    ncases = 5
    testname1='ldm_103'
    t1_max = 600
    %testname2='lkm_106'
    testname2='lkm_105'
    t2_max = 600
    testname3='cvs_102'
    t3_max = 600
    testname4='cvs_001'
    t4_max = 600
    testname5='les_001'
    t5_max = 600
    title1='Lagrangian Dynamic Model'
    title2='K_{sgs} model (LKM)'
    title3='SCALES no model'
    title4='CVS'
    title5='LES'
    preprocessing1 = 0
    preprocessing2 = 0
    preprocessing3 = 0
    preprocessing4 = 0
    preprocessing5 = 0
    preprocessing6 = 0
    preprocessing7 = 0
end
if group == 5  %group 5
    bw = logical(0)
    ncases = 7
    testname1='gdm_023.eps0.43'
    t1_max = 600
    testname2='ldm_103'
    t2_max = 600
    testname3='dsm_103'
    t3_max = 600
    testname4='lkm_106'
    t4_max = 600
    testname5='cvs_102'
    t5_max = 600
    testname6='cvs_001'
    t6_max = 600
    testname7='les_001'
    t7_max = 600
    title1='Global Dynamic Model'
    title2='Lagrangian Dynamic Model'
    title3='K_{sgs} model (DSM)'
    title4='K_{sgs} model (LKM)'
    title5='SCALES no model'
    title6='CVS'
    title7='LES'
    preprocessing1 = 0
    preprocessing2 = 0
    preprocessing3 = 0
    preprocessing4 = 0
    preprocessing5 = 0
    preprocessing6 = 0
    preprocessing7 = 0
end
if group == 4  %group 1
    bw = logical(0)
    ncases = 5
    %testname1='gdm_023.eps0.43'
    %testname1='SCALES_LDM_Nu0.14'
    testname1='CVS0.1_Nu0.14.updt0.jmx11'
    t1_max = 57
    %testname2='SCALES_LDM_Nu0.09'
    testname2='CVS0.1_Nu0.09.updt0.jmx11'
    t2_max = 39
    %testname3='SCALES_LDM_Nu0.06'
    testname3='CVS0.1_Nu0.06.updt0.jmx11'
    t3_max = 33
    %testname4='SCALES_LDM_Nu0.04'
    testname4='CVS0.1_Nu0.04.updt0.jmx11'
    t4_max = 30
    testname5='SCALES_LDM_Nu0.14'
    t5_max = 9
    testname6='SCALES_LDM_Nu0.14'
    t6_max = 9
    title1='CVS \nu=0.14, max 256^3'
    title2='CVS \nu=0.09, max 512^3'
    title3='CVS \nu=0.06, max 512^3'
    title4='CVS \nu=0.04, max 1024^3' 
    title5='SCALES LDM \nu=0.02'
    title6='SCALES LDM'
    preprocessing1 = 0
    preprocessing2 = 0
    preprocessing3 = 0
    preprocessing4 = 0
    preprocessing5 = 0
    preprocessing6 = 0
end
if group == 1  %group 1
    bw = logical(0)
    ncases = 5
    testname1='ldm_103'
    t1_max = 600
    testname2='ldm_104'
    t2_max = 600
    testname3='gdm_023.eps0.43'
    t3_max = 600
    testname4='cvs_004_eps0.43'
    t4_max = 600
    testname5='les_001'
    t5_max = 600
    title1='LDM pathlione diffusive'
    title2='LDM pathtube'
    title3='Global Dynamic Model'
    title4='SCALES no model' 
    title5='LES'
    preprocessing1 = 0
    preprocessing2 = 0
    preprocessing3 = 0
    preprocessing4 = 0
    preprocessing5 = 0
end
if group == 2  %group 2
    bw = logical(0)
    ncases = 1
    testname1='les_001'
    t1_max = 600
    testname2='lkm_106'
    t2_max = 600
    testname3='les_001'
    t3_max = 600
    title1='LES with Dynamic Smagorinsky Model'
    title2='K_{sgs} model (DSM)'
    title3='K_{sgs} model (LKM)'
    preprocessing1 = 0
    preprocessing2 = 0
    preprocessing3 = 0
    preprocessing4 = 0
end
if group == 3 %group 3
    bw = logical(0)
    ncases = 5
    testname1='cvs_001'
    t1_max = 600
    testname2='cvs_102'
    t2_max = 600
    testname3='gdm_004'
    t3_max = 600
    testname4='gdm_023.eps0.43'
    t4_max = 600
    testname5='les_001'
    t5_max = 600
    title1='CVS, \epsilon=0.23'
    title2='SCALES (no model) \epsilon=0.43'
    title3='SCALES (GDM) \epsilon=0.23'
    title4='SCALES (GDM) \epsilon=0.43'
    title5='LES'
    preprocessing1 = 0
    preprocessing2 = 0
    preprocessing3 = 0
    preprocessing4 = 0
    preprocessing5 = 0
end

%win
%cd 'C:\Oleg_Vasilyev\Research\FORTRAN\work_wlt_3d_DBs\trunk\WorkCases\Oleg\HomogeneousTurbulence\results'

DIR_DNS = 'C:\Oleg_Vasilyev\Research\FORTRAN\work_wlt_3d_DBs\trunk\WorkCases\Oleg\HomogeneousTurbulence\DNS_results\'

plotDir   = 'C:\Oleg_Vasilyev\Research\FORTRAN\work_wlt_3d_DBs\trunk\WorkCases\Oleg\HomogeneousTurbulence'

%REMOTE_RESULTS ='H:\research2\wavelet_code2\results.remote\from_oleg'

% use m files from post_process/view/spectra

addpath('C:\Oleg_Vasilyev\Research\FORTRAN\work_wlt_3d_DBs\trunk\post_process\view_spectra')


global lw lw_dns; lw =2 ; lw_dns = 2
if bw 
    global lspec;  lspec = [ 'k- ' ; 'k- ' ; 'ko ' ; 'k- ' ; 'k--' ; 'k-.' ; 'k: ' ; 'r- ' ; 'm--'; 'r- '; 'r- '  ]; %bw
else
    global lspec;  lspec = [ 'k- ' ; 'r: ' ; 'ko ' ; 'r- ' ; 'b-.' ; 'm--' ; 'r: ' ; 'g--' ; 'k-.'; 'r- '  ]; %color
 end


DIR_RUN = 'C:\Oleg_Vasilyev\Research\FORTRAN\work_wlt_3d_DBs\trunk\WorkCases\Oleg\HomogeneousTurbulence\results\';
DIR_RUN2 = ''
if 1==1
%compare spectra
%station 1 t = 0.08
db_used = '.db_wrk.';
testcase1=[testname1 db_used];
testcase2=[testname2 db_used];
testcase3=[testname3 db_used];
testcase4=[testname4 db_used];
testcase5=[testname5 db_used];
testcase6=[testname6 db_used];
testcase7=[testname7 db_used];

    %[DIR_DNS 'nc.1000.wlt.filt.t0.08.spectra'],... % DNSfilt_spectra_file 
    %[DIR_DNS 'nc.1000.nu0.09Filt.eps0.43.0000.spectra'],... % DNSfilt_spectra_file
    
   fl = [DIR_RUN testcase1 num2str(min(t1_max,20),'%4.4d') '.spectra']
plot_spectra_wDNS_7('Compare gdm at t=0.08',...
    11,...              %figure number
    [DIR_DNS 'nc.1000.init.t0.08.spectra'],...     %DNSfull_spectra_file
    [DIR_DNS 'nc.1000.nu0.09Filt.eps0.43.0000.spectra'],... % DNSfilt_spectra_file 
    [DIR_DNS 'nc.1000.nu0.09Filt.eps0.43.0000.spectra'],... % DNSfilt_spectra_file 
    'DNS, Re_{\lambda}=72',...
    [DIR_RUN testcase1 num2str(min(t1_max,80),'%4.4d') '.spectra'],...  %run 1 spectra
    title1,...        %run 1 legend_str
    [DIR_RUN testcase2 num2str(min(t2_max,80),'%4.4d') '.spectra'],...  %run 2 spectra
    title2,...        %run 2 legend_str
    [DIR_RUN testcase3 num2str(min(t3_max,80),'%4.4d') '.spectra'],...  %run 3 spectra
    title3,...        %run 3 legend_str
    [DIR_RUN testcase4 num2str(min(t4_max,80),'%4.4d') '.spectra'],...  %run 4 spectra
    title4,...        %run 4 legend_str
    [DIR_RUN testcase5 num2str(min(t5_max,80),'%4.4d') '.spectra'],...  %run 5 spectra
    title5...
    )       %run 7 legend_str
    title('')
%station 1 t = 0.16
    %[DIR_DNS 'nc.2000.wlt.filt.t0.16.spectra'],... % DNSfilt_spectra_file
    %[DIR_DNS 'nc.2000.nu0.09Filt.eps0.43.0000.spectra'],... % DNSfilt_spectra_file
plot_spectra_wDNS_7('Compare gdm at t=0.16',...
    12,...              %figure number
    [DIR_DNS 'nc.2000.init.t0.16.spectra'],...     %DNSfull_spectra_file
    [DIR_DNS 'nc.2000.nu0.09Filt.eps0.43.0000.spectra'],... % DNSfilt_spectra_file
    [DIR_DNS 'nc.2000.nu0.09Filt.eps0.43.0000.spectra'],... % DNSfilt_spectra_file
    'filtered DNS',...
    [DIR_RUN testcase1 num2str(min(t1_max,160),'%4.4d') '.spectra'],...  %run 1 spectra
    title1,...        %run 1 legend_str
    [DIR_RUN testcase2 num2str(min(t2_max,160),'%4.4d') '.spectra'],...  %run 2 spectra
    title2,...        %run 2 legend_str
    [DIR_RUN testcase3 num2str(min(t3_max,160),'%4.4d') '.spectra'],...  %run 2 spectra
    title3,...        %run 2 legend_str
    [DIR_RUN testcase4 num2str(min(t4_max,160),'%4.4d') '.spectra'],...  %run 2 spectra
    title4,...        %run 2 legend_str
    [DIR_RUN testcase5 num2str(min(t5_max,160),'%4.4d') '.spectra'],...  %run 2 spectra
    title5,...
    [DIR_RUN testcase6 num2str(min(t6_max,160),'%4.4d') '.spectra'],...  %run 2 spectra
    title6,...
    [DIR_RUN testcase7 num2str(min(t7_max,160),'%4.4d') '.spectra'],...  %run 2 spectra
    title7...
    )       %run 3 legend_str
    title('')
%new gdm 80 160
end

global lw; lw =2 ;

if filtered_DNS
    if bw
        global lspec;  lspec = [ 'b- ' ; 'ko ' ; 'k- ' ; 'k--' ; 'k-.' ; 'k: ' ; 'r- ' ; 'y- ' ; 'm--'; 'r- ' ]; %bw
    else
        global lspec;  lspec = [ 'k- ' ; 'ko ' ; 'r- ' ; 'b-.' ; 'm--' ; 'r: ' ; 'g- ' ; 'k-.'; 'r-.' ]; %color
    end
else
    if bw
        global lspec;  lspec = [ 'b- ' ; 'k- ' ; 'k--' ; 'k-.' ; 'k: ' ; 'r- ' ; 'y- ' ; 'm--'; 'r- ' ]; %bw
    else
        global lspec;  lspec = [ 'b- ' ; 'r- ' ; 'b-.' ; 'm--' ; 'r: ' ; 'g- ' ; 'k-.'; 'r-.' ]; %color
    end

end

%%%%DNS%%%%
%256^3 runs giuliano's DNS
plot_num = 1
%GiulianoDNS_phd(1,[DIR_DNS '\dns.data.longrun2'])

%
% 256 scales cDynamic from JOT
%
if 1==0
plot_num = plot_num + 1
plot_turb_stats(256^3, ...  % total number of non-adaptive points (for calculating compression
    'C:\Oleg_Vasilyev\Research\FORTRAN\work_wlt_3d_DBs\trunk\WorkCases\Oleg\HomogeneousTurbulence\old_SCALES_results\JOT.SCALES_final340.run\',...         % directory run is located in
    'decay.g128.dns.tst340dan.', ...         % base name of run
    [0 324], ...            % begin and end station numbers (file numbers) for run
    0.0,...                 % eps for wlt filter applied during post processing (NOT USED FOR NOW)
     0.5,...                % eps used in simulation (needed for post_processing code)
    0.09,...                % viscity used in simulation (needed for post_processing code)
    1, ...                  % base figure number
    plot_num,...                   % plot number for comparing multiple plots. if 1 then allplots are cleared before plotting
    'JOT tst340dan SCALES',... % dan Cdyn eps0.5L2 Mdl , consadj, gridfile=eps,testfilt=2eps,DynSmodGridFilt, SgridFilt,',... 
    'nospectra',...           % if 'spectra' spectras are plotted for all data files with a pause in between (keep hitting return)
    ...                     % else if 'movie' then make an avi movie of the changing spectra (just one run )
    ...                     % else if 'nospectra' no spectra is plotted
    plotDir,...             % directory to put movie (or plots eventually)
    0.6,...                    % maximum x for all stats plots.
    0 )                     % 1 plot some values from pre postprocessing files (else 0 do not)
end
% 
% 256 scales filtered DNS with eps= 0.43 
%
if filtered_DNS
testcase=['wlt_filt_dns' '.db_wrk.']
plot_num = plot_num + 1
plot_turb_stats(256^3, ...  % total number of non-adaptive points (for calculating compression
    'C:\Oleg_Vasilyev\Research\FORTRAN\work_wlt_3d_DBs\trunk\WorkCases\Oleg\HomogeneousTurbulence\results' ,... % directory run is located in  
     testcase, ...         % base name of run
    [1 50], ...            % begin and end station numbers (file numbers) for run
    0.0,...                 % eps for wlt filter applied during post processing (NOT USED FOR NOW)
    0.3,...                % eps used in simulation (needed for post_processing code)
    0.09,...                % viscoty used in simulation (needed for post_processing code)
    1, ...                  % base figure number 
    plot_num,...                   % plot number for comparing multiple plots. if 1 then allplots are cleared before plotting
    'Filtered DNS, eps=0.43',... % legend string
    'nospectra',...         % if 'spectra' spectras are plotted for all data files with a pause in between (keep hitting return)
    ...                     % else if 'movie' then make an avi movie of the changing spectra (just one run )
    ...                     % else if 'nospectra' no spectra is plotted
    plotDir,...             % directory to put movie (or plots eventually)
    0.5,...                 % maximum x for all stats plots.
    1 )                     % 1 plot some values from pre postprocessing files (else 0 do not)
end

% 
% 256 scales gdm 
%
if 1==1 & ncases > 0
file_num = 1
testcase=[testname1 db_used]
plot_num = plot_num + 1
plot_turb_stats(256^3, ...  % total number of non-adaptive points (for calculating compression
    'C:\Oleg_Vasilyev\Research\FORTRAN\work_wlt_3d_DBs\trunk\WorkCases\Oleg\HomogeneousTurbulence\results' ,... % directory run is located in  
     testcase, ...         % base name of run
    [1 t1_max], ...            % begin and end station numbers (file numbers) for run
    0.0,...                 % eps for wlt filter applied during post processing (NOT USED FOR NOW)
    0.3,...                % eps used in simulation (needed for post_processing code)
    0.09,...                % viscoty used in simulation (needed for post_processing code)
    1, ...                  % base figure number 
    plot_num,...                   % plot number for comparing multiple plots. if 1 then allplots are cleared before plotting
    title1,... % dan Cdyn eps0.5L2 Mdl , consadj, gridfile=eps,testfilt=2eps,DynSmodGridFilt, SgridFilt,',...              % legend string
    'nospectra',...           % if 'spectra' spectras are plotted for all data files with a pause in between (keep hitting return)
    ...                     % else if 'movie' then make an avi movie of the changing spectra (just one run )
    ...                     % else if 'nospectra' no spectra is plotted
    plotDir,...             % directory to put movie (or plots eventually)
    0.5,...                    % maximum x for all stats plots.
    preprocessing2 )                     % 1 plot some values from pre postprocessing files (else 0 do not)
end
% 
% 256 scales gdm 
%
if 1==1 & ncases > 1
file_num = 2
testcase=[testname2 db_used]
plot_num = plot_num + 1
plot_turb_stats(256^3, ...  % total number of non-adaptive points (for calculating compression
    'C:\Oleg_Vasilyev\Research\FORTRAN\work_wlt_3d_DBs\trunk\WorkCases\Oleg\HomogeneousTurbulence\results' ,... % directory run is located in  
     testcase, ...         % base name of run
    [1 t2_max], ...            % begin and end station numbers (file numbers) for run
    0.0,...                 % eps for wlt filter applied during post processing (NOT USED FOR NOW)
    0.3,...                % eps used in simulation (needed for post_processing code)
    0.09,...                % viscoty used in simulation (needed for post_processing code)
    1, ...                  % base figure number 
    plot_num,...                   % plot number for comparing multiple plots. if 1 then allplots are cleared before plotting
    title2,... % dan Cdyn eps0.5L2 Mdl , consadj, gridfile=eps,testfilt=2eps,DynSmodGridFilt, SgridFilt,',...              % legend string
    'nospectra',...           % if 'spectra' spectras are plotted for all data files with a pause in between (keep hitting return)
    ...                     % else if 'movie' then make an avi movie of the changing spectra (just one run )
    ...                     % else if 'nospectra' no spectra is plotted
    plotDir,...             % directory to put movie (or plots eventually)
    0.5,...                    % maximum x for all stats plots.
    preprocessing2 )                     % 1 plot some values from pre postprocessing files (else 0 do not)
end

% 
% 256 scales gdm 
%
if 1==1 & ncases > 2
file_num = 3
testcase=[testname3 db_used]
plot_num = plot_num + 1
plot_turb_stats(256^3, ...  % total number of non-adaptive points (for calculating compression
    'C:\Oleg_Vasilyev\Research\FORTRAN\work_wlt_3d_DBs\trunk\WorkCases\Oleg\HomogeneousTurbulence\results' ,... % directory run is located in  
     testcase, ...         % base name of run
    [1 t3_max], ...            % begin and end station numbers (file numbers) for run
    0.0,...                 % eps for wlt filter applied during post processing (NOT USED FOR NOW)
    0.3,...                % eps used in simulation (needed for post_processing code)
    0.09,...                % viscoty used in simulation (needed for post_processing code)
    1, ...                  % base figure number 
    plot_num,...                   % plot number for comparing multiple plots. if 1 then allplots are cleared before plotting
    title3,... % dan Cdyn eps0.5L2 Mdl , consadj, gridfile=eps,testfilt=2eps,DynSmodGridFilt, SgridFilt,',...              % legend string
    'nospectra',...           % if 'spectra' spectras are plotted for all data files with a pause in between (keep hitting return)
    ...                     % else if 'movie' then make an avi movie of the changing spectra (just one run )
    ...                     % else if 'nospectra' no spectra is plotted
    plotDir,...             % directory to put movie (or plots eventually)
    0.5,...                    % maximum x for all stats plots.
    preprocessing3 )                     % 1 plot some values from pre postprocessing files (else 0 do not)
end


% 
% 256 scales gdm 
%
if 1==1 & ncases > 3
%lw = 1
file_num = 4
testcase=[testname4 db_used]
plot_num = plot_num + 1
plot_turb_stats(256^3, ...  % total number of non-adaptive points (for calculating compression
    'C:\Oleg_Vasilyev\Research\FORTRAN\work_wlt_3d_DBs\trunk\WorkCases\Oleg\HomogeneousTurbulence\results' ,... % directory run is located in  
     testcase, ...         % base name of run
    [1 t4_max], ...            % begin and end station numbers (file numbers) for run
    0.0,...                 % eps for wlt filter applied during post processing (NOT USED FOR NOW)
    0.3,...                % eps used in simulation (needed for post_processing code)
    0.09,...                % viscoty used in simulation (needed for post_processing code)
    1, ...                  % base figure number 
    plot_num,...                   % plot number for comparing multiple plots. if 1 then allplots are cleared before plotting
    title4,... % dan Cdyn eps0.5L2 Mdl , consadj, gridfile=eps,testfilt=2eps,DynSmodGridFilt, SgridFilt,',...              % legend string
    'nospectra',...           % if 'spectra' spectras are plotted for all data files with a pause in between (keep hitting return)
    ...                     % else if 'movie' then make an avi movie of the changing spectra (just one run )
    ...                     % else if 'nospectra' no spectra is plotted
    plotDir,...             % directory to put movie (or plots eventually)
    0.5,...                    % maximum x for all stats plots.
    preprocessing4 )                     % 1 plot some values from pre postprocessing files (else 0 do not)
end
    
if 1==1 & ncases > 4
testcase=[testname5 db_used]
plot_num = plot_num + 1
plot_turb_stats(256^3, ...  % total number of non-adaptive points (for calculating compression
    'C:\Oleg_Vasilyev\Research\FORTRAN\work_wlt_3d_DBs\trunk\WorkCases\Oleg\HomogeneousTurbulence\results' ,... % directory run is located in  
     testcase, ...         % base name of run
    [1 t5_max], ...            % begin and end station numbers (file numbers) for run
    0.0,...                 % eps for wlt filter applied during post processing (NOT USED FOR NOW)
    0.3,...                % eps used in simulation (needed for post_processing code)
    0.09,...                % viscoty used in simulation (needed for post_processing code)
    1, ...                  % base figure number 
    plot_num,...                   % plot number for comparing multiple plots. if 1 then allplots are cleared before plotting
    title5,... % dan Cdyn eps0.5L2 Mdl , consadj, gridfile=eps,testfilt=2eps,DynSmodGridFilt, SgridFilt,',...              % legend string
    'nospectra',...           % if 'spectra' spectras are plotted for all data files with a pause in between (keep hitting return)
    ...                     % else if 'movie' then make an avi movie of the changing spectra (just one run )
    ...                     % else if 'nospectra' no spectra is plotted
    plotDir,...             % directory to put movie (or plots eventually)
    0.5,...                    % maximum x for all stats plots.
    preprocessing5 )                     % 1 plot some values from pre postprocessing files (else 0 do not)
end

if 1==1 & ncases > 5
testcase=[testname6 db_used]
plot_num = plot_num + 1
plot_turb_stats(256^3, ...  % total number of non-adaptive points (for calculating compression
    'C:\Oleg_Vasilyev\Research\FORTRAN\work_wlt_3d_DBs\trunk\WorkCases\Oleg\HomogeneousTurbulence\results' ,... % directory run is located in  
     testcase, ...         % base name of run
    [1 t6_max], ...            % begin and end station numbers (file numbers) for run
    0.0,...                 % eps for wlt filter applied during post processing (NOT USED FOR NOW)
    0.3,...                % eps used in simulation (needed for post_processing code)
    0.09,...                % viscoty used in simulation (needed for post_processing code)
    1, ...                  % base figure number 
    plot_num,...                   % plot number for comparing multiple plots. if 1 then allplots are cleared before plotting
    title6,... % dan Cdyn eps0.5L2 Mdl , consadj, gridfile=eps,testfilt=2eps,DynSmodGridFilt, SgridFilt,',...              % legend string
    'nospectra',...           % if 'spectra' spectras are plotted for all data files with a pause in between (keep hitting return)
    ...                     % else if 'movie' then make an avi movie of the changing spectra (just one run )
    ...                     % else if 'nospectra' no spectra is plotted
    plotDir,...             % directory to put movie (or plots eventually)
    0.5,...                    % maximum x for all stats plots.
    preprocessing6 )                     % 1 plot some values from pre postprocessing files (else 0 do not)
end

if 1==1 & ncases > 6
testcase=[testname7 db_used]
plot_num = plot_num + 1
plot_turb_stats(256^3, ...  % total number of non-adaptive points (for calculating compression
    'C:\Oleg_Vasilyev\Research\FORTRAN\work_wlt_3d_DBs\trunk\WorkCases\Oleg\HomogeneousTurbulence\results' ,... % directory run is located in  
     testcase, ...         % base name of run
    [1 t7_max], ...            % begin and end station numbers (file numbers) for run
    0.0,...                 % eps for wlt filter applied during post processing (NOT USED FOR NOW)
    0.3,...                % eps used in simulation (needed for post_processing code)
    0.09,...                % viscoty used in simulation (needed for post_processing code)
    1, ...                  % base figure number 
    plot_num,...                   % plot number for comparing multiple plots. if 1 then allplots are cleared before plotting
    title7,... % dan Cdyn eps0.5L2 Mdl , consadj, gridfile=eps,testfilt=2eps,DynSmodGridFilt, SgridFilt,',...              % legend string
    'nospectra',...           % if 'spectra' spectras are plotted for all data files with a pause in between (keep hitting return)
    ...                     % else if 'movie' then make an avi movie of the changing spectra (just one run )
    ...                     % else if 'nospectra' no spectra is plotted
    plotDir,...             % directory to put movie (or plots eventually)
    0.5,...                    % maximum x for all stats plots.
    preprocessing7 )                     % 1 plot some values from pre postprocessing files (else 0 do not)
end