
#------------------------------------------------------------------#
# General input file format                                        #
#                                                                  #
# comments start with # till the end of the line                   #
# string constant are quoted by "..." or '...'                     #
# boolean can be (T F 1 0 on off) in or without ' or " quotes      #
# integers are integers                                            #
# real numbers are whatever (e.g. 1   1.0   1e-23   1.123d-54 )    #
# vector elements are separated by commas                          #
# spaces between tokens are not important                          #
# empty or comment lines are not important                         #
# order of lines is not important                                  #
#------------------------------------------------------------------#

file_gen = 'CVS0.1_Nu0.09.db_tree.'
dimension = 3		#  dim (2,3), # of dimensions
IC_restart = F          	#  ICrestart
IC_restart_station = 0             #  it_start, restart file number to use (NOT iteration!)
IC_from_file = F    # Do a new run with IC from a restart file
IC_file_fmt = 0     # IC data file format (0 - native restart file, 1-netcdf, 2- A.Wray in fourier space, 3-simple binary) next line is IC filename

#IC_filename = 'C:\Oleg_Vasilyev\Research\TURBULENCE_DATA\nc.0_IC_eps043.0000.res'    # initial field on laptop
#IC_filename = 'C:\Oleg_Vasilyev\Research\TURBULENCE_DATA\cfDNS.db_wrk.0003.res'    # initial field on laptop
#IC_filename = 'C:\Oleg_Vasilyev\Research\TURBULENCE_DATA\nc.0.res'    # initial field on laptop
#IC_filename = 'nc.0.bin'    # initial field on laptop
#IC_filename = 'data/nc.0.res'
IC_filename = 'results/cfDNS2.db_wrk.0094.res'
Data_file_format = F    #  T = formatted, F = unformatted
IC_adapt_grid = F  # parameter defaulted to .TRUE. If is set to .FALSE. no grid adaptation is done after the data are read.

coord_min =  0.0, 0.0, 0.0 	                #  XMIN, YMIN, ZMIN, etc
coord_max =  6.283185,6.283185,6.283185  	#  XMAX, YMAX, ZMAX, etc

coord_zone_min = -10, -10, -10,  # XMINzone, etc
coord_zone_max = 10e+00 , 10e+00 , 10e+00 ,	# XMAXzone, etc

eps_init = 0.1e-0  	#  (1.5 they match first adapt) EPS used to adapt initial grid  
eps_run =  0.1e-0  	#  EPS used in run  
eps_adapt_steps =  2    # eps_adapt_steps ! how many time steps to adapt from eps_init to eps_run
eps_expl = 0.1e-0         # eps_expl - threshold sed for explicit filtering
Scale_Meth = 3             	# (3) Scale_Meth !1- Linf, 2-L2
scl_fltwt = 0.0           	#  scl temporal filter weight, scl_new = scl_fltwt*scl_old + ( 1-scl_fltwt)*scl_new
scaleCoeff = 1 1 1 1 1 1 1 1
j_mn_init = 2             	#  J_mn_init force J_mn == J_INIT while adapting to IC
j_lev_init = 4                  #  force j_lev == j_lev_init when initializing
j_IC = 4             	#  J_IC if the IC data does not have dimensions in it then mxyz(:)*2^(j_IC-1) is used
J_MN = 2             	#  J_MN
J_MX = 6             	#  J_MX
J_FILT = 20            	#  J_FILT

M_vector = 8,8,8	#  Mx, My, Mz 
periodic = 1,1,1	#  prd(:) (0/1) 0: non-periodic; 1: periodic
uniform = 0,0,0		#  grid(:) (0/1) 0: uniform; 1: non-uniform

i_h = 123456        	#  order of boundaries (1-xmin,2-xmax,3-ymin,4-ymax,5-zmin,6-zmax)
i_l = 111111        	#  algebraic/evolution (1/0) BC order: (lrbt)
N_predict = 3           #  N_predict
N_predict_low_order = 1 # 1 N_predict_low_order
N_update =3           	#  N_update
N_update_low_order = 1  # 0 N_update_low_order
N_diff = 3             		#  N_diff

IJ_ADJ = 0,1,1		# IJ_ADJ(-1:1) = (coarser level), (same level), (finer level)
ADJ_type = 0,0,0        #  ADJ_type(-1:1) = (coarser level), (same level), (finer level) # (0 - less conservative, 1 - more conservative)

BNDzone = F	  	#  BNDzone
BCtype = 0             	#  BCtype (0 - Dirichlet, 1 - Neuman, 2 - Mixed)

time_integration_method = 2 # 0- meth2, 1 -krylov, 2 - Crank Nicolson

t_begin = 0.0000    	#  tbeg  0.2500000e+00
t_end =   2.0        	#  tend  0.2600000e+00
dt = 5.0000000e-04 	    #  dt
dtmax = 1.0000000e-02 	#  dtmax
dtmin = 1.0000000e-08 	#  dtmin-if dt < dtmin then exection stops(likely blowing up)
dtwrite = 1.0000000e-03 #  dtwrite 1.0000000e-02 
t_adapt = 2.0e+4        # when t > t_adapt use an adaptive time step if possible

# cfl = 1.0000000e-00 	#  cfl
cflmax = 1.0000000e-00 	#  cflmax
cflmin = 0.0000000e-00 	#  cflmin
nu =  0.090     	#  nu 1.0000000e-03, viscosity
nu1 = 1.0000000e-02 	#  nu1 5.0000000e-02
Zero_Mean = T           #  T- enforce zero mean for 1:dim first variables (velocity usually), F- do nothing

u0 = 0.0000000e+00 	#  u0
eta = 1.0000000e-04 	#  eta, alpha
theta = 0.0000000e+01 		#  theta (in degrees) (angle of 2D Burger's equation, ifn=3=> theta is angular velocity)
theta1 = -4.5000000e+01 	#  theta1 (in degrees) (angle of 2D Burger's equation)

debug_force_wrk_wlt_order = F
debug_level = 0                #  debug
debug_c_diff_fast = 0           # 0- nothing,1- derivative MIN/MAXVALS,2- low level, are printed in c_diff_fast
diagnostics_elliptic =  F       #  diagnostics_elliptic: If T print full diagnostic for elliptic solver
GMRESflag = T              	#  GMRESflag
BiCGSTABflag = F              	#  BiCGSTABflag

Jacoby_correction = T           # Jacoby_correction
multigrid_correction = T        # multigrid_correction
GMRES_correction  = T           # GMRES_correction 
kry_p = 3			#  kry_p
kry_perturb = 3			#  kry_perturb
kry_p_coarse = 100           	#  kry_p_coarse
len_bicgstab = 6             	#  len_bicgstab
len_bicgstab_coarse = 100	#  len_bicgstab_coarse
len_iter = 5            	#  len_iter

W0 = 1.0000000e+00 	#  W0 underrelaxation factor for inner V-cycle
W1 = 1.0000000e+00 	#  W1 underrelaxation factor on the finest level
W2 = 0.6000000e+00 	#  W2 underrelaxation factor for weighted Jacoby (inner points)
W3 = 1.0000000e-00 	#  W3 underrelaxation factor for weighted Jacoby (boundary points)
W_min = 1.00000e-02	#  W_min correcton factor 
W_max = 1.00000e-00 	#  W_max correction factor 

obstacle = F              	# imask !TRUE  - there is an obstacle defined
itime = 3   	           	# itime ! 0- Euler first-order, 1- Rotational first-order, 2- Second-order, 3-Semi-implicit second-order
obstacle_X = 0,0,0 		#  Xo(:)! Location of obstacle
obstacle_U = 0,0,0		#  Uo(:)! Velocity of obstacle
obstacle_move = 0,0,0		#  1- Obstacle allowed to move in that direction, else == 0


diameter = 1.0		#  d diameter of cylinder as an obstacle
k_star = 8.7400000e+00 	#  k_star
m_star = 5.0000000e+00 	#  m_star
b_star = 0.0000000e-03 	#  b_star
tol1 = 1.0000000e-03 	#  tol1 used to set tolerence for non-solenoidal half-step
tol2 = 1.0000000e-02 	#  tol2 used to set tolerence for solenoidal half-step
tol3 = 1.0000000e-1     #  used to set tolerence for time step
tol_gmres =  1.0e-3
tol_gmres_stop_if_larger = F   # If true stop iterating in gmres solver if error of last iteration was smaller

wlog = T              	#  wlog

dtforce = 0.0           	#  dtforce ! time interval for calculating the lift force
Pressure_force = 0,0,0		#  Pressure forcing term in x-dr

Cf = 6.0                        #  Linear forcing coefficient
#***************************************************************************************
#  USER CASE INPUTS
#***************************************************************************************

SGS_model = 0             	#  SGS model: 0=none, 1- fixed parameter Smagorinski, 2- dynamic smagorinski model
                                #             3 - global dynamic Smagorinsky (GDM), 4 - Lagrangian dynamic model (LDM)
                                #             5 - dynamic structure model (DSM), 
                                #             6 - localized kinetic energy based model (LKM)
                                #             7 - localized dynamic kinetic energy based model (LKM)
SGS_mdl_form = 2                #  some SGS models have different forms (currently only sgsmodel=4-7)
SGS_TimeIntegrationType = 1     #  0 - explicit 1st order, 1 - semi-implicit 1st order, 2 - semi-implicit 2nd order

#***************************************************************************************
#  FILTER TYPES INPUTS
#***************************************************************************************
                                #  filter types: 0 - none, 1 - >eps, 2 - >2eps, 
				#                3 - level <= j_lev-1, 4 - level <= j_lev-2
				#                5 - 2eps + adjacent zone, 
                                #                6 - local low-pass filter (lowpass_filt_type, lowpassfilt_support, tensorial_filt)
				#                7 - u > eps_explicit filter  
				#                8 - u > eps_explicit filter + adjecent zone 
				#                9 - u > 2eps_explicit filter  
				#               10 - u > 2eps_explicit filter + adjecent zone 
mdl_filt_type_grid = 0          # dyn mdl grid filter, 0 - 6 
mdl_filt_type_test = 6        	# dyn mdl test filter, 0 - 6
lowpass_filt_type = 0        # 0 - volume averged, 1 - trapezoidal
lowpass_filt_type_GRID = 0   # 0 - volume averged, 1 - trapezoidal
lowpass_filt_type_TEST = 0   # 0 - volume averged, 1 - trapezoidal
lowpassfilt_support = 2 #  support for lowpass filter used for other prposes, such as in Lagrangian pathtube averging 
lowpassfilt_support_GRID = 0 #  support for lowpass filter if  mdl_filt_type_grid = 6 
lowpassfilt_support_TEST = 0 #  support for lowpass filter if  mdl_filt_type_test = 6 

tensorial_filt = F      # if .TRUE. uses tensorial filter for comparison with lines

Mcoeff =  2.0          	# Mcoeff in GDM:  Mij = Mcoeff * |S>2eps| Sij>2eps - (|S|Sij )>2eps 
	          	# Mcoeff in LDKM: \hat{\Delta} = Mcoeff * \Delta
deltaMij = T            # deltaMij, Mij definition including delta^2 (GSM, LDM) or delta (LKM, LDKM)
#***************************************************************************************

Lijtraceless = T	# Lijtraceless ! make Lij traceless in dyn sgs model
Mijtraceless = T	# Mijtraceless ! make Mij traceless in dyn sgs model
ExplicitFilter = F	# ExplicitFilter ,apply grid filter as an explicit filter each time step
ExplicitFilterNL = F	# ExplicitFilterNL !apply grid filter as an explicit filter to the non-linear term
DynSmodGridFilt = F	# DynSmodGridFilt !if true |S| is based on grid filt in nu_t=Cdyn|S|
DynSmodGridFiltS = F    # DynSmodGridFiltS !if true S is based on grid filt in tauij = -2 nu_t Sij
DynMdlFiltLijLast = F	#  DynMdlFiltLijLast, if true Lij=(ugrid ugrid - u_test u_test )_test,  else  Lij=(ugrid ugrid)_test - u_test u_test

lowpass_filt_type = 0   # 0 - volume averged, 1 - trapezoidal
tensorial_filt = F      # if .TRUE. uses tensorial filter for comparison with lines

adaptMagVort = F    # Adapt on the magnitude of vorticity
adaptNormS   = F    # Adapt on the L2 of Sij
adaptMagVel  = F    # Adapt on the magnitude of velocity

#-------------- Smagorinsky-based model ------

SGS_model_coef1 =  0.0  #   sgsmodel_coef1 ! first model coefficient for model set by sgsmodel (see above)

clip_nu_nut = T              	# clip_nu_nut ! clip nu+ nu_t if true
clip_Cs = F              	# clip_Cs ! clip Cs if true

#-------------- LDM --------------------------

LDMtheta = 1.5      # Langrangian model time coefficient
CI = 5.0            # Langrangian model diffusion scale coefficient
adaptIlm = F        # Adapt on the Ilm
adaptImm = F        # Adapt on the Imm

SpaceTimeAvg = F    # SpaceTimeAvg, Lagrangian spatial filter

#-------------- k-based SGS model ------------

adaptK   = F          # Adapt on the Ksgs
#LijLowPassFilter = T  # explicit test lowpass filter- no longer needed
C_eps_sgs = 1.0       # Coefficient for ksgs dissipation
C_nu_art_k = 0.1      # artificial diffusion coefficient for ksgs evolution
do_const_K = F        # Adjust eps for constant dissipation
alpha_k = 0.10        # portion of the SGS kinetic energy 
Ceps_model = 0	      # 0 - fixed coefficient, 1 - dynamic Bardina-like scaling, 2 - dynamic Germano like scaling

#-------------- DSM --------------------------


#-------------- Constant dissipation ---------

do_const_diss = F   # Adjust eps for constant dissipation
Psgs_diss_goal=0.35 # Percent SGS dissipation we want to model
q_eps=1.0           # Temporal filtering factor for eps change (1=no filtering)
eps_min=0.4         # Allowable lower range for eps to change
eps_max=0.8         # Allowable upper range for eps to change
sgs_gain = 0.0      # rescale used for recalculating tau_ij
Ilm_gain = 0.0      # rescale used for recalculating Ilm,Ilm forcing...

