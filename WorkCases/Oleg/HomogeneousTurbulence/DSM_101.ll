#@ shell = /usr/bin/csh
#@ job_type = parallel
#@ environment = COPY_ALL
# (notification options: always,start,error,complete,never)
#@ notification = always
#@ class = batch 
##@ account_no = abc
#@ resources = ConsumableCpus(1) ConsumableMemory(2000Mb)
# Specify the wall clock limit = hrs:min:sec
#@ wall_clock_limit = 96:00:00

# Specify the name of the job
#@ job_name = DSM_101

#@ output = $(job_name).$(jobid).output
#@ error = $(job_name).$(jobid).err

# This has to be the last loadleveler directive
#@ queue

##########################################################
set echo               # echo commands before execution; use for debugging

set job_name_csh = DSM_101

# change to the scratch directory
# (This directory is removed when the job finishes)

cd $SCR
mkdir results


set input_file = isoturb_$job_name_csh.inp 
echo $input_file
cp $HOME/wlt_3d_trunk/WorkCases/Oleg/HomogeneousTurbulence/wlt_3d_db_wrk_case_isoturb_new.out $SCR
cp $HOME/wlt_3d_trunk/post_process/view_spectra/c_wlt_turbstats.out $SCR

# msscmd "cd wlt_3d, get nc.0" 
msscmd "cd wlt_3d, get nc.0.bin" 
cp $HOME/wlt_3d_trunk/WorkCases/Oleg/HomogeneousTurbulence/isoturb_$job_name_csh.inp $SCR
cp $HOME/wlt_3d_trunk/WorkCases/Oleg/HomogeneousTurbulence/isoturb_$job_name_csh"_turbstats.inp" $SCR

# get executable and input file from unitree
# msscmd "cd test2,get a.out,mget *.input" 

# due to unitree "feature", must set executable bit
chmod u+x $SCR/wlt_3d_db_wrk_case_isoturb_new.out
chmod u+x $SCR/c_wlt_turbstats.out
 

# tar up output files and save them on unitree after the job is completed
saveafterjob "cd wlt_3d/HomogeneousTurbulence, tar cf $job_name_csh.tar results/*"

echo 'start the run'

# run serial executable 
./wlt_3d_db_wrk_case_isoturb_new.out isoturb_$job_name_csh.inp

# run postprocessing
./c_wlt_turbstats.out <  isoturb_$job_name_csh_turbstats.inp

echo 'job is done'

echo job done
