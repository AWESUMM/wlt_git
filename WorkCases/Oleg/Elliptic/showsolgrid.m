function showsolgrid(solgrid,el_spm,el_myplot_comp,el_filebase,el_station_num,curvilinear_flag)
%plot solution and/or grid
%written by Scot Reckinger
%___________________________________________________________________________________________
%INPUTS
% solgrid        - 1 for solution only, 2 for grid only, 3 for both side by side, 4 for both vertically stacked
% el_spm         - 's' for serial... 'p' for parallel
% el_myplot_comp - variable name of component to be plotted
% el_filebase    - file_gen from .inp file... without the '.' at the end... I add it automatically
% el_station_num - station number (# of the .res file)
% curvilinear_flag - imput is present curvilinear=1, else curvilinear = 0
%___________________________________________________________________________________________
% PARAMETERS FOR BOTH SOLUTION AND GRID
    el_jcs = 0:20;
    el_j_mx = 20;
    el_watchit = 0;
    el_saveit = 0;
    if nargin > 5 
        curvilinear = 1
    else
        curvilinear = 0
    end
%
%___________________________________________________________________________________________
% PARAMETERS FOR BOTH SOLUTION AND GRID
    casefoldersup = 0; %number of folders to climb up to get to the root of the case
    ppfoldersup = 2;   %number of folder to climb up to get to the trunk of the code
        
    slicenum = 2;  %for 3d cases, slice number to plot... set 0 to sweep through all planes 
                                               %with pauses in between, and plot3d for grid
    dimtoslice = 3; %dimension to be sliced in for 3d cases with slicenum not equal to 0        

    colorbarloc = 'EastOutside';  %location of colorbar, set to 0 to not display colorbar
    legendloc   = 'EastOutside';  %location of legend, set to 0 to not display legend
%___________________________________________________________________________________________
    %close all
    %figure()
    if solgrid==3
        subplot(1,2,2)
    elseif solgrid==4
        subplot(2,1,2)
    end
    if solgrid>1
       showgrid_gen(el_jcs,el_j_mx,el_watchit,el_saveit,el_spm,el_filebase,el_station_num)
    end
    if solgrid==3
        subplot(1,2,1)
    elseif solgrid==4
        subplot(2,1,1)
    end
    if solgrid~=2
        showsol_gen(el_saveit,el_spm,el_myplot_comp,el_filebase,el_station_num,curvilinear)
    end

    function showsol_gen(saveit,spm,myplot_comp,filebase,station_num,curvilinear)
        %plot solution in variable colors
        %written by Scot Reckinger
        %___________________________________________________________________________________________
        %INPUTS
        % saveit      - set 1 to save .fig... set 0 to not save figure
        % spm         - 's' for serial... 'p' for parallel
        % myplot_comp - variable name of component to be plotted
        % filebase    - file_gen from .inp file... without the '.' at the end... I add it automatically
        % station_num - station number (# of the .res file)
        % curvilinear - 0/1: if 1 curvilinear mesh is plotted
        %___________________________________________________________________________________________
        % PARAMETERS
        %casefoldersup = 1; %number of folders to climb up to get to the root of the case
        %ppfoldersup = 4;   %number of folder to climb up to get to the trunk of the code
        %
        %slicenum = 1;  %for 3d cases, slice number to plot... set 0 to sweep through all planes with pauses in between
        %dimtoslice = 1; %dimension to be sliced in for 3d cases with slicenum not equal to 0
        %
        docont=1; %set 1 to use contourf, set 0 to use surf
        %
        %colorbarloc = 'EastOutside';  %location of colorbar, set to 0 to not display colorbar
        %___________________________________________________________________________________________
        
        file=strcat(filebase,'.');
        
        local_dir = pwd;
        if isunix
            pl = '/';
        else
            pl = '\';
        end
        global POSTPROCESS_DIR
        POSTPROCESS_DIR = pwd;
        for i=1:ppfoldersup
            POSTPROCESS_DIR = [POSTPROCESS_DIR pl '..' ];
        end
        POSTPROCESS_DIR = [POSTPROCESS_DIR pl 'post_process'];
        path(path,POSTPROCESS_DIR)     
        
        eps=0.0;
        j_range=[0 20];
        bounds=[0];
        az=0;
        el=90;
        
        for i=1:casefoldersup
            cd('..')
        end
        [u_var,x,y,z,nx,ny,nz,dim,time] = inter3d(file,eps,bounds,myplot_comp,j_range,station_num,spm);
        if curvilinear == 1
            [Xp,x,y,z,nx,ny,nz,dim,time] = inter3d(file,eps,bounds,'XCoord',j_range,station_num,spm);
            [Yp,x,y,z,nx,ny,nz,dim,time] = inter3d(file,eps,bounds,'YCoord',j_range,station_num,spm);
            if dim == 3
                [Zp,x,y,z,nx,ny,nz,dim,time] = inter3d(file,eps,bounds,'ZCoord',j_range,station_num,spm);
            end
        else
            Xp = x; 
            Yp = y;
            Zp = z;
        end
        %[ux,x,y,z,nx,ny,nz,dim,time] = inter3d(file,eps,bounds,'U1',j_range,station_num,spm);
        %[uy,x,y,z,nx,ny,nz,dim,time] = inter3d(file,eps,bounds,'U2',j_range,station_num,spm);
        %[X,Y]=meshgrid(x,y);
         
        
        %if myplot_comp=='U'
        %    u_var = u_var + 1.0;
        %end
        if myplot_comp=='P'
            u_var = u_var - u_var(1);
        end

        
        cd(local_dir)
        
        fprintf('time = %12.8f\n', time);     % 6/7/07 addition
        
        if( dim==3 )
            cmin=min(min(min(u_var)));
            cmax=max(max(max(u_var)));
        else
            cmin=min(min(u_var));
            cmax=max(max(u_var));
        end
        if( cmin == cmax )
            cmin = cmin -1;
            cmax = cmax +1;
        end
        
        if dimtoslice==1
            nd=nx;
            dirstr='x';
        elseif dimtoslice==2
            nd=ny;
            dirstr='y';
        else
            nd=nz;
            dirstr='z';
        end
        
        sweeps=nd*2;
        if dim==2 || slicenum>0
            sweeps=1;
        end
        for myzslice = 1:sweeps
            zslice=mod(myzslice-1,nd-1)+1;
            if slicenum>0
                zslice=min(max(slicenum,1),nd);
            end
            set(cla,'nextplot','replace')
            if dim==2
                u_var_plot(:,:)=u_var(:,:);
                x_plot=Xp;
                y_plot=Yp;
            else
                if dimtoslice==1
                    u_var_plot(:,:)=u_var(zslice,:,:);
                    x_plot=Yp;
                    y_plot=Zp;
                elseif dimtoslice==2
                    u_var_plot(:,:)=u_var(:,zslice,:);
                    x_plot=Xp;
                    y_plot=Zp;
                else
                    u_var_plot(:,:)=u_var(:,:,zslice);
                    x_plot=Xp;
                    y_plot=Yp;
                end
            end
            if docont==1
                contourf(x_plot,y_plot,u_var_plot,100,'Linecolor','none')
            else
                surface(x_plot,y_plot,u_var_plot);
                %surface(X+ux,Y+uy,u_var_plot);
                shading interp;
            end
            view(az,el);
            if cmin==cmax
                cmin=cmax-1;
            end
            caxis([cmin cmax]);
            set(gcf,'renderer','zbuffer');
            xmin=min(x_plot(:));
            xmax=max(x_plot(:));
            ymin=min(y_plot(:));
            ymax=max(y_plot(:));
            set(gca,'xlim',[xmin,xmax],'ylim',[ymin,ymax]);
            minlen=min((xmax-xmin),(ymax-ymin));
            yasp=(ymax-ymin)/minlen;
            xasp=(xmax-xmin)/minlen;
            zasp=(cmax-cmin)/minlen;
            set(gca,'PlotBoxAspectRatio',[xasp,yasp,zasp]);
            mypos=get(gca,'Position');
            if colorbarloc~=0
                colorbar(colorbarloc);
            end
            set(gca,'clim',[cmin,cmax]);
            set(gca,'FontSize',16);
            set(gca,'Position',mypos);
            if( sweeps ~= 1 )
                display(['Sweeping through domain - on ',dirstr,'-plane ', num2str(myzslice), ' of ', num2str(nd) ' - Hit ENTER to continue'])
                pause;
            end
        end
        curstr=strcat('plots/',filebase,'_',myplot_comp,'_',num2str(station_num),'.fig');
        if saveit==1
            saveas(gcf,curstr)
        end
    end

    function showgrid_gen(jcs,j_mx_grid,watchit,saveit,spm,filebase,iter)
        %plot grid in variable colors
        %written by Scot Reckinger
        %___________________________________________________________________________________________
        %INPUTS
        % jcs       - list of minimum levels for plotting in ascending order... put zero in places where you want to skip a color
        % j_mx_grid - maximum level for plotting grid (basically a bound on the highest color level)
        % watchit   - set 1 to pause at all color levels so that the levels can be "watched"... set 0 to do all color levels (no pausing)
        % saveit    - set 1 to save .fig... set 0 to not save figure
        % spm       - 's' for serial... 'p' for parallel
        % filebase  - file_gen from .inp file... without the '.' at the end... I add it automatically
        % iter      - station_num (# of the .res file)
        % curvilinear - 0/1: if 1 curvilinear mesh is plotted
        %___________________________________________________________________________________________
        % PARAMETERS 
        maxsz=4;  %maximum marker size... I like 4
        minsz=1;  %minimum marker size... I like 1
        dotorcircle='o'; %  'o' or '.'... or whatever marker you want to plot
        maxcol=0.8;  %  maximum scale for colors... 1 is max... I like 0.8
        legmarksz = 8;  %marker size for legend... I like 8... 0 if you do not want a legend
        legdotorcircle='o'; %marker for legend... best to use 'o'
        %
        %casefoldersup = 1; %number of folders to climb up to get to the root of the case
        %ppfoldersup = 4;   %number of folder to climb up to get to the trunk of the code
        %
        %slicenum = 1;  %for 3d cases, slice number to plot... set 0 to use plot3d
        %dimtoslice = 3; %dimension to be sliced in for 3d cases with slicenum not equal to 0
        %
        %legendloc = 'EastOutside';  %location of legend, set to 0 to not display legend
        %___________________________________________________________________________________________
        
        file=strcat(filebase,'.');
        
        local_dir = pwd;
        if isunix
            pl = '/';
        else
            pl = '\';
        end
        global POSTPROCESS_DIR
        POSTPROCESS_DIR = pwd;
        for i=1:ppfoldersup
            POSTPROCESS_DIR = [POSTPROCESS_DIR pl '..' ];
        end
        POSTPROCESS_DIR = [POSTPROCESS_DIR pl 'post_process'];
        path(path,POSTPROCESS_DIR)
        
        eps=0.0;
        bounds=0;
        az=0;
        el=90;
        x0=[0 0 0 100];
        n0=[[0 1 1]' [0 1 -1]'];
        
        station_num=iter;
        
        if curvilinear == 1
            j_range = [0 20];
            [Xp,x,y,z,nx,ny,nz,dim,time] = inter3d(file,eps,bounds,'XCoord',j_range,station_num,spm);
            [Yp,x,y,z,nx,ny,nz,dim,time] = inter3d(file,eps,bounds,'YCoord',j_range,station_num,spm);
            Xpmin=min(Xp(:));
            Xpmax=max(Xp(:));
            Ypmin=min(Yp(:));
            Ypmax=max(Yp(:));
            if dim == 3
                [Zp,x,y,z,nx,ny,nz,dim,time] = inter3d(file,eps,bounds,'ZCoord',j_range,station_num,spm);
                Zpmin=min(Zp(:));
                Zpmax=max(Zp(:));
            end
        end   

        plot_component='x';
        foundjmx=0;
        jmxiter=1;
        
        delx=1.0e10;
        while foundjmx==0
            j_min=jmxiter;
            j_max=jmxiter;
            for i=1:casefoldersup
                cd('..')
            end
            [xstm,ystm,zstm,cstm,stm_indx,dim,nz,xmin,xmax,ymin,ymax,zmin,zmax,time,np] ...
                = res2matlab(file,eps,bounds,plot_component,j_min,j_max,station_num,x0,n0,spm,curvilinear);
            if slicenum>0 && dim==3
                if dimtoslice==1
                    stmsrt=sort(xstm);
                elseif dimtoslice==2
                    stmsrt=sort(ystm);
                else
                    stmsrt=sort(zstm);
                end
                xdiff=abs(stmsrt(2:end)-stmsrt(1:end-1));
                if sum(xdiff>0)>0
                    mydel=min(xdiff(xdiff>0));
                    delx=min(delx,mydel);
                end
            end
            cd(local_dir)
            if isempty(xstm)
                jmx=jmxiter-1;
                foundjmx=1;
            else
                jmxiter=jmxiter+1;
            end
        end
        
        if slicenum>0 && dim==3
            if dimtoslice==1
                xslice=max(min(xmin+(slicenum-1)*delx,xmax),xmin);
            elseif dimtoslice==2
                xslice=max(min(ymin+(slicenum-1)*delx,ymax),ymin);
            else
                xslice=max(min(zmin+(slicenum-1)*delx,zmax),zmin);
            end
        end
        
        jcs(jcs>j_mx_grid) = 0;
        jcs(jcs>jmx) = 0;
        jcs(jcs<0) = 0;
        
        numcolors = length(jcs);
        
        curlev=0;
        for i=1:numcolors
            if jcs(i)<=curlev
                jcs(i)=0;
            else
                curlev=jcs(i);
            end
        end
        
        myjcs=jcs(jcs>0);
        myjcs=[myjcs min(jmx,j_mx_grid)+1];
        clevs=sum(jcs>0);
        
        numcolorints=ceil((numcolors)/6);
        rgbarray=zeros(numcolorints*6+1,3);
        rgbarray(1,:)=[0 0 0];
        for i=1:numcolorints
            curval=(numcolorints-i+1)/numcolorints*maxcol;
            rgbarray((i-1)*6+2,:)=[curval 0 0];
            rgbarray((i-1)*6+3,:)=[0 curval 0];
            rgbarray((i-1)*6+4,:)=[0 0 curval];
            rgbarray((i-1)*6+5,:)=[0 curval curval];
            rgbarray((i-1)*6+6,:)=[curval 0 curval];
            rgbarray((i-1)*6+7,:)=[curval curval 0];
        end
        
        curc=1;
        for i=1:numcolors
            if jcs(i)>0
                rgbarray(curc,:)=rgbarray(i,:);
                curc=curc+1;
            end
        end
        
        if clevs==1
            szs(1)=maxsz;
        else
            if maxsz==minsz
                szs(1:clevs)=maxsz;
            else
                szs=maxsz:(minsz-maxsz)/(clevs-1):minsz;
            end
        end
        
        legiter=1;
        
        for curc=1:clevs
            j_min=myjcs(curc);
            j_max=myjcs(curc+1)-1;
            if j_max == 0
                j_max = -1;
            end
            if j_max>=j_min
                if zmax == zmin
                    zmin=zmax-1;
                end
                if dim == 3 && slicenum==0
                    plot3(xmax+1,ymax+1,zmax+1,legdotorcircle,'Markersize',legmarksz,'MarkerEdgeColor',rgbarray(curc,:),'MarkerFaceColor',rgbarray(curc,:))
                else
                    if dim==3 && dimtoslice==1
                        plot(ymax+1,zmax+1,legdotorcircle,'Markersize',legmarksz,'MarkerEdgeColor',rgbarray(curc,:),'MarkerFaceColor',rgbarray(curc,:))
                    elseif dim==3 && dimtoslice==2
                        plot(xmax+1,zmax+1,legdotorcircle,'Markersize',legmarksz,'MarkerEdgeColor',rgbarray(curc,:),'MarkerFaceColor',rgbarray(curc,:))
                    else
                        plot(xmax+1,ymax+1,legdotorcircle,'Markersize',legmarksz,'MarkerEdgeColor',rgbarray(curc,:),'MarkerFaceColor',rgbarray(curc,:))
                    end
                end
                if j_max==j_min
                    tempstr=['level ' num2str(j_max)];
                else
                    tempstr=['levels ' strcat(num2str(j_min),'-',num2str(j_max))];
                end
                forleg(1:length(tempstr),legiter)=tempstr;
                legiter=legiter+1;
                hold on
            end
        end
        
        for curc=1:clevs
            j_min=myjcs(curc);
            j_max=myjcs(curc+1)-1;
            if j_max == 0
                j_max = -1;
            end
            if j_max>=j_min
                for i=1:casefoldersup
                    cd('..')
                end
                [xstm,ystm,zstm,cstm,stm_indx,dim,nz,xmin, xmax, ymin, ymax, zmin, zmax, time, np] ...
                    = res2matlab(file,eps,bounds,plot_component,j_min,j_max,station_num,x0,n0,spm,curvilinear);
                           
                if zmax == zmin
                    zmin=zmax-1;
                end
                
                if curvilinear
                    if dim == 2
                        for i = 1:length(xstm)
                            xstm(i)=Xp(stm_indx(i,2)+1,stm_indx(i,1)+1);
                            ystm(i)=Yp(stm_indx(i,2)+1,stm_indx(i,1)+1);
                        end
                        xmin = Xpmin;
                        xmax = Xpmax;
                        ymin = Ypmin;
                        ymax = Ypmax;
                    end
                    if dim == 3
                        for i = 1:length(xstm)
                            xstm(i)=Xp(stm_indx(i,2)+1,stm_indx(i,1)+1,stm_indx(i,3)+1);
                            ystm(i)=Yp(stm_indx(i,2)+1,stm_indx(i,1)+1,stm_indx(i,3)+1);
                            zstm(i)=Zp(stm_indx(i,2)+1,stm_indx(i,1)+1,stm_indx(i,3)+1);
                        end
                        xmin = Xpmin;
                        xmax = Xpmax;
                        ymin = Ypmin;
                        ymax = Ypmax;
                        zmin = Zpmin;
                        zmax = Zpmax;
                    end
                end
                
                minlen=min((xmax-xmin),(ymax-ymin));
                yasp=(ymax-ymin)/minlen;
                xasp=(xmax-xmin)/minlen;
                zasp=(zmax-zmin)/minlen;
                
                if dim == 3 && slicenum==0
                    plot3(xstm,ystm,zstm,dotorcircle,'Markersize',szs(curc),'MarkerEdgeColor',rgbarray(curc,:),'MarkerFaceColor',rgbarray(curc,:))
                    set(gca,'xlim',[ymin,ymax],'ylim',[xmin,xmax],'zlim',[zmin,zmax]);
                    set(gca,'PlotBoxAspectRatio',[xasp,yasp,zasp])
                    set(gca,'FontSize',16)
                    grid on
                else
                    if dim==2
                        plot(xstm,ystm,dotorcircle,'Markersize',szs(curc),'MarkerEdgeColor',rgbarray(curc,:),'MarkerFaceColor',rgbarray(curc,:))
                        asp=[xasp,yasp,zasp];
                        xmx=[xmin,xmax];
                        ymx=[ymin,ymax];
                        set(gca,'PlotBoxAspectRatio',[xasp,yasp,zasp])
                        set(gca,'xlim',[xmin,xmax],'ylim',[ymin,ymax]);
                    else
                        if dimtoslice==1
                            mask=abs(xstm-xslice)<1.0e-10;
                            plot(ystm(mask),zstm(mask),dotorcircle,'Markersize',szs(curc),'MarkerEdgeColor',rgbarray(curc,:),'MarkerFaceColor',rgbarray(curc,:))
                            set(gca,'PlotBoxAspectRatio',[yasp,zasp,xasp]);
                            set(gca,'xlim',[ymin,ymax],'ylim',[zmin,zmax]);
                        elseif dimtoslice==2
                            mask=abs(ystm-xslice)<1.0e-10;
                            plot(xstm(mask),zstm(mask),dotorcircle,'Markersize',szs(curc),'MarkerEdgeColor',rgbarray(curc,:),'MarkerFaceColor',rgbarray(curc,:))
                            set(gca,'PlotBoxAspectRatio',[xasp,zasp,yasp]);
                            set(gca,'xlim',[xmin,xmax],'ylim',[zmin,zmax]);
                        else
                            mask=abs(zstm-xslice)<1.0e-10;
                            plot(xstm(mask),ystm(mask),dotorcircle,'Markersize',szs(curc),'MarkerEdgeColor',rgbarray(curc,:),'MarkerFaceColor',rgbarray(curc,:))
                            set(gca,'PlotBoxAspectRatio',[xasp,yasp,zasp]);
                            set(gca,'xlim',[xmin,xmax],'ylim',[ymin,ymax]);
                        end
                    end
                    set(gca,'FontSize',16)
                end
                hold on
                view(az,el);
                if slicenum==0 && dim==3
                    view(30,30);
                end
                set(gca,'FontSize',16)
                if watchit
                    pause
                end
            end
        end
        mypos=get(gca,'Position');
        if legendloc~=0
           legend(forleg','Location',legendloc)
        end
        set(gca,'FontSize',16)
        hold off
        set(gca,'Position',mypos);
        curstr=strcat('plots/',filebase,'_grid_',num2str(iter),'.fig');
        if saveit
            saveas(gcf,curstr)
        end        
    end
end 
