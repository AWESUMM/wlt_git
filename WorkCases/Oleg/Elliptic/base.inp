#------------------------------------------------------------------#
# General input file format                                        #
#                                                                  #
# comments start with # till the end of the line                   #
# string constant are quoted by "..." or '...'                     #
# boolean can be (T F 1 0 on off) in or without ' or " quotes      #
# integers are integers                                            #
# real numbers are whatever (e.g. 1   1.0   1e-23   1.123d-54 )    #
# vector elements are separated by commas                          #
# spaces between tokens are not important                          #
# empty or comment lines are not important                         #
# order of lines is not important                                  #
#------------------------------------------------------------------#

file_gen = 'test.'
#***************************************************************************************
IC_restart_mode 	= 0    #  0: normal, 1: hard restart, 2: soft restart, 3: from IC 
IC_filename 		= 'results/test.0000.res'
IC_restart_station 	= 81    #  it_start, restart file number to use (NOT iteration!)
IC_file_fmt 		= 0    # IC data file format (0 - native restart file, 1-netcdf, 2- A.Wray in fourier space, 3-simple binary)
Data_file_format 	= F    #  T = formatted, F = unformatted
IC_adapt_grid 		= T    # parameter defaulted to .TRUE. If is set to .FALSE. no grid adaptation is done after the data are read.
do_Sequential_run       = F
#***************************************************************************************

dimension = 2		#  dim (2,3), # of dimensions

coord_min =  -1.0, -1.0, -1.0 	                #  XMIN, YMIN, ZMIN, etc
coord_max =   1.0,  1.0,  1.0		 	#  XMAX, YMAX, ZMAX, etc

coord_zone_min = -0.5e+10, -0.5e+10, -1e+10,  # XMINzone, default to -1.0e12
coord_zone_max =  0.5e+10 , 0.5e+10 , 1e+10 ,	# XMAXzone, default to 1.0e12

eps_init = 1.e-2  	#  (1.5 they match first adapt) EPS used to adapt initial grid  
eps_run =  1.e-2  	#  EPS used in run  
eps_adapt_steps =  2            # eps_adapt_steps ! how many time steps to adapt from eps_init to eps_run

Scale_Meth = 1             	# Scale_Meth !1- Linf, 2-L2
scl_fltwt = 0.0           	#  scl temporal filter weight, scl_new = scl_fltwt*scl_old + ( 1-scl_fltwt)*scl_new
scaleCoeff = 1 1 1 1 1 1 1 1
j_mn_init = 2             	#  J_mn_init force J_mn == J_INIT while adapting to IC
j_lev_init = 3             	#  starts adapting IC by having all the points on this level of resolution
j_IC = 10             	#  J_IC if the IC data does not have dimensions in it then mxyz(:)*2^(j_IC-1) is used
J_MN = 2             	#  J_MN
J_MX = 9             	#  J_MX
J_FILT = 20            	#  J_FILT
J_TREE_ROOT = 2         #  j_tree_root - OPTIONAL for db_tree, DEAFALT: j_tree_root=j_mn

M_vector = 2,2,2	#  Mx, My, Mz 
periodic = 0,0,0	#  prd(:) (0/1) 0: non-periodic; 1: periodic
uniform = 0,0,0		#  grid(:) (0/1) 0: uniform; 1: non-uniform

i_h = 123456        	#  order of boundaries (1-xmin,2-xmax,3-ymin,4-ymax,5-zmin,6-zmax)
i_l = 111111        	#  algebraic/evolution (1/0) BC order: (lrbt)
N_predict = 3           #  N_predict
N_predict_low_order = 1 # N_predict_low_order
N_update = 3           	#  N_update
N_update_low_order = 0  # N_update_low_order
N_diff = 3             	#  N_diff

IJ_ADJ = 1,1,1		# IJ_ADJ(-1:1) = (coarser level), (same level), (finer level)
ADJ_type = 1,1,1        #  ADJ_type(-1:1) = (coarser level), (same level), (finer level) # (0 - less conservative, 1 - more conservative)

BNDzone = F	  	#  BNDzone
BCtype = 0             	#  BCtype (0 - Dirichlet, 1 - Neuman, 2 - Mixed)


time_integration_method = 2 # 0- meth2, 1 -krylov, 2 - Crank Nicolson

t_begin = 0.0000    	#  tbeg  0.2500000e+00
t_end =   0.0        	#  tend  0.2600000e+00
dt = 1.0000000e-04 	    #  dt
dtmax = 1.0000000e-02 	#  dtmax
dtmin = 1.0000000e-08 	#  dtmin-if dt < dtmin then exection stops(likely blowing up)
dtwrite = 1.0000000e-04 #  dtwrite 1.0000000e-02 
t_adapt = 2.0e-4        # when t > t_adapt use an adaptive time step if possible

# cfl = 1.0000000e-00 	#  cfl
cflmax = 1.0000000e-00 	#  cflmax
cflmin = 0.0000000e-00 	#  cflmin
nu =  0.135	    	#  nu 1.0000000e-03, viscosity
nu1 = 1.0000000e-03 	#  nu1 5.0000000e-02
Zero_Mean = T           #  T- enforce zero mean for 1:dim first variables (velocity usually), F- do nothing

u0 = 0.0000000e+00 	#  u0
eta = 1.0000000e-04 	#  eta, alpha
theta = 0.0000000e+01 		#  theta (in degrees) (angle of 2D Burger's equation, ifn=3=> theta is angular velocity)
theta1 = -7.5000000e+01 	#  theta1 (in degrees) (angle of 2D Burger's equation)

GMRESflag = T              	#  GMRESflag
BiCGSTABflag = F              	#  BiCGSTABflag

Jacoby_correction = T           # Jacoby_correction
multigrid_correction = T        # multigrid_correction
GMRES_correction  = F           # GMRES_correction 

kry_p = 3			#  kry_p
kry_perturb = 0			#  kry_perturb
kry_p_coarse = 100           	#  kry_p_coarse
len_bicgstab = 6             	#  len_bicgstab
len_bicgstab_coarse = 100	#  len_bicgstab_coarse
len_iter = 5             	#  len_iter

W0 = 1.0000000e+00 	#  W0 underrelaxation factor for inner V-cycle
W1 = 1.0000000e+00 	#  W1 underrelaxation factor on the finest level
W2 = 0.6666667e+00 	#  W2 underrelaxation factor for weighted Jacoby (inner points)
W3 = 1.0000000e-00 	#  W3 underrelaxation factor for weighted Jacoby (boundary points)
W_min = 1.00000e-02	#  W_min correcton factor 
W_max = 1.00000e-00 	#  W_max correction factor 

obstacle = F              	# imask !TRUE  - there is an obstacle defined
itime = 3   	           	# itime ! 0- Euler first-order, 1- Rotational first-order, 2- Second-order, 3-Semi-implicit second-order
obstacle_X = 0,0,0 		#  Xo(:)! Location of obstacle
obstacle_U = 0,0,0		#  Uo(:)! Velocity of obstacle
obstacle_move = 0,0,0		#  1- Obstacle allowed to move in that direction, else == 0


diameter = 1.0		#  d diameter of cylinder as an obstacle
k_star = 8.7400000e+00 	#  k_star
m_star = 5.0000000e+00 	#  m_star
b_star = 0.0000000e-03 	#  b_star
tol1 = 1.0000000e-07 	#  tol1 used to set tolerence for non-solenoidal half-step
tol2 = 1.0000000e-07 	#  tol2 used to set tolerence for solenoidal half-step
tol3 = 1.0000000e-07     #  used to set tolerence for time step
tol_gmres =  1.0e-07
tol_gmres_stop_if_larger = F   # If true stop iterating in gmres solver if error of last iteration was smaller


dtforce = 0.0           	#  dtforce ! time interval for calculating the lift force
Pressure_force = 0,0,0		#  Pressure forcing term in x-dr

additional_nodes_active = F
j_additional_nodes = 5
n_additional_patches = 8                        #  n_additional_patches
x_patches = -2.,  3.,  3.,  2., 2., 0., 0., -2. #  x-coordinates for additioanl patches
y_patches = -2., -2., -1., -1., 3., 3., 2.,  2. #  y-coordinates for additioanl patches

# bit flag to print debugging information:
# 1  - print statistics from MAIN
# 2  - print statistics from vector utilities and sgs_incompressible module
# 4  - print statistics after each internal subroutine call from ADAPT_GRID
# 8  - print info inside DB specific subroutines
	debug_level = 0
	diagnostics_elliptic =  F       # If T print full diagnostic for elliptic solver
	wlog = F             	 	#  wlog
	debug_c_diff_fast = 0
        debug_force_wrk_wlt_order = F   # sort db_tree wavelets in db_wrk order (slow, use for debugging only)
#	debug_test_interpolation = T	# test database independent interpolation inside usercase
