MODULE user_case
  !
  ! Case elliptic poisson 
  USE precision
  USE wlt_vars
  USE wavelet_filters_mod
  USE elliptic_mod
  USE elliptic_vars
  USE wlt_trns_mod									
  USE wlt_trns_vars
  USE wlt_trns_util_mod
  USE io_3d_vars
  USE util_mod
  USE util_vars
  USE share_consts
  USE pde
  USE sizes
  USE share_kry
  USE vector_util_mod
  USE field
  USE input_file_reader
  !
  ! case specific variables
  !
  INTEGER n_var_vorticity ! start of vorticity in u array
  INTEGER n_var_pressure  ! start of pressure in u array


  !
  ! local variables
  !
  REAL(pr)  :: nu
  REAL(pr)  :: theta_loc(1:3)   !oriantation of axis for ellipsoid 
  REAL(pr)  :: nu_loc(0:3)      ! scales for principal axis of ellipsiod
  REAL(pr)  :: x1_loc(1:3), x0_loc(1:3)
  !  REAL(pr)  :: xp

  LOGICAL :: divgrad

  ! debug flag to be read from .inp file to test database independent interpolation
  LOGICAL, PRIVATE :: test_interpolate

CONTAINS

  !
  ! In user_setup_pde() we setup how many variables are integrated, which are interpolated
  ! to the next times step and if any exeact solution exists to compare to during the run.
  ! We also set the variable names that are used when the result files are written.
  !
  ! The following variables must be setup in this routine:
  !
  !
  ! n_integrated     ! first n_integrated eqns will be acted on for time integration
  ! n_var_additional ! 
  ! n_var
  !
  !
  !
  !
  SUBROUTINE  user_setup_pde ( VERB ) 
    IMPLICIT NONE
    LOGICAL, OPTIONAL :: VERB         ! print debug info
    LOGICAL :: do_verb
    INTEGER :: i
    
    do_verb = .TRUE.
    IF (PRESENT(VERB)) do_verb = VERB
    
    IF (do_verb) THEN
       PRINT * ,''
       PRINT *, '**********************Setting up PDE*****************'
       PRINT * ,'CASE Elliptic '
       PRINT *, '*****************************************************'
    END IF

    n_integrated = 1 ! dim


    n_var_additional = 0 !only doing elliptic solver !
    n_var = n_integrated + n_var_additional !--Total number of variables

    n_var_exact = 1 ! 0 <--> No exact solution


    n_var_pressure  = n_integrated ! ! no pressure

    !
    !--Allocate logical arrays 
    !  Must be done after setting number of different types of variables
    !  and before setting logical variable mapping arrays
    !
    CALL alloc_variable_mappings


    !
    ! Fill in variable names for default variables for integration meth 2
    ! These may be changed in the call to the user specified routine
    !  user_setup_pde()
    !
    ! In integrated variables (3D)
    IF( dim == 3 ) THEN
       WRITE (u_variable_names(1), u_variable_names_fmt)  'Scalar_1       '
    ELSE IF(dim == 2) THEN
       WRITE (u_variable_names(1), u_variable_names_fmt)  'Scalar_1       '
    ELSE IF(dim == 1) THEN
       WRITE (u_variable_names(1), u_variable_names_fmt)  'Scalar_1       '
    END IF


    !
    ! setup logical variable mapping arrays
    ! This defined which variables are used for adaptation, Which need to be interpololated to the new grid at 
    ! each time step, which are saved, and for which we have an exect solutiuon we want to check at each time step
    !


    !
    ! setup which components we will base grid adaptation on.
    ! Adapt on the 3 velocity components
    !
    n_var_adapt(1:n_integrated,0) = .TRUE. !--Initially adapt on integrated variables at first time level
    n_var_adapt(1:n_integrated,1) = .TRUE. !--After first time step adapt on 

    !Variables that need to be interpoleted to new adapted grid in initial grid adaptation
    n_var_interpolate(1:n_integrated,0) = .TRUE. 

    !Variables that need to be interpoleted to new adapted grid at each time step
    n_var_interpolate(1:n_var_pressure,1) = .TRUE. 

    !
    ! setup which components we have an exact solution for
    !
    n_var_exact_soln(:,0:1) = .TRUE.



    !
    ! setup which variables we will save the solution
    !
    n_var_save(1) = .TRUE. ! save all for restarting code


    ! Setup which variables are required on restart
    !
    n_var_req_restart = .FALSE.
    n_var_req_restart(1:n_integrated) = .TRUE.
    !

    !
    ! Set the maximum number of components for which we have an exact solution
    !
    n_var_exact = 1 ! MAX( COUNT(n_var_exact_soln(:,0)),COUNT(n_var_exact_soln(:,1)) )

    !
    ! Setup a scaleCoeff array of we want to tweak the scaling of a variable
    ! ( if scaleCoeff < 1.0 then more points will be added to grid 
    !
    ALLOCATE ( scaleCoeff(1:n_var), Umn(1:n_var) )
    scaleCoeff = 1.0_pr

    IF (do_verb) THEN
       PRINT *, 'n_integrated = ',n_integrated 
       PRINT *, 'n_var = ',n_var 
       PRINT *, 'n_var_exact = ',n_var_exact 
       PRINT *, '*******************Variable Names*******************'
       DO i = 1,n_var
          WRITE (*, u_variable_names_fmt) u_variable_names(i)
       END DO
       PRINT *, '****************************************************'
    END IF

  END SUBROUTINE  user_setup_pde

  !
  ! read variables from input file for this user case 
  !
  SUBROUTINE  user_read_input()
    IMPLICIT NONE


!!$  ! examples of reading
!!$  !  call input_real ('Mcoeff', Mcoeff, 'stop', &
!!$  !     ' Mcoeff ! Mij = Mcoeff * |S>2eps| Sij>2eps - (|S|Sij )>2eps')
!!$
!!$
!!$  ! call input_integer ('mdl_filt_type_grid', mdl_filt_type_grid, 'stop', &
!!$       ' mdl_filt_type_grid ! dyn mdl grid filter (defined in make_mdl_filts() )')
!!$
!!$   call input_logical ('clip_Cs', clip_Cs, 'stop', &
!!$       ' clip_Cs ! clip Cs if true')
    
    test_interpolate = .FALSE.
    call input_logical ('debug_test_interpolation',test_interpolate,'default', &
         ' test database independent interpolation inside usercase')

  END SUBROUTINE  user_read_input


  !
  ! Set the exact solution for comparison to the simulated solution
  !
  ! u          - array to fill in the exact solution
  ! nlocal       - number of active wavelets   
  ! ne_local        - total number of equations
  ! t          - time of current time step 
  ! l_n_var_exact_soln_index - index into the elements of u for which we need to 
  !                            find the exact solution
  SUBROUTINE  user_exact_soln (u, nlocal,  t_local, l_n_var_exact_soln)
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: nlocal
    REAL (pr), INTENT (IN) ::  t_local
    REAL (pr), DIMENSION (nlocal,n_var_exact), INTENT (INOUT) :: u
    LOGICAL , INTENT (IN) :: l_n_var_exact_soln(n_var)
    INTEGER :: i



    REAL(pr) :: xp(nlocal,1:dim)

    !set variables by hand
    IF(dim == 1) THEN
       theta_loc(1:dim) = (/ -45.0_pr /)/1.8e2_pr*pi   !oriantation of axis for ellipsoid 
       nu_loc(0:dim)    = (/ 0.01_pr , 0.04_pr   /)  ! scales for principal axis of ellipsiod
       x0_loc(1:dim)    = (/-0.4_pr  /)
       x1_loc(1:dim)    = (/ 0.1_pr  /)
    ELSE IF(dim == 2) THEN
       theta_loc(1:dim) = (/ -45.0_pr , -30.0_pr /)/1.8e2_pr*pi   !oriantation of axis for ellipsoid 
       nu_loc(0:dim)    = (/ 0.01_pr , 0.04_pr  , 0.01_pr /)  ! scales for principal axis of ellipsiod
       x0_loc(1:dim)    = (/-0.4_pr , -0.6_pr /)
       x1_loc(1:dim)    = (/ 0.1_pr ,  0.15_pr /)
    ELSE
       theta_loc(1:dim) = (/ 45.0_pr , -45.0_pr , 30.0_pr /)/1.8e2_pr*pi   !oriantation of axis for ellipsoid 
       nu_loc(0:dim)    = (/ 0.05_pr , 0.05_pr  , 0.02_pr , 0.5_pr /)  ! scales for principal axis of ellipsiod
       x0_loc(1:dim)    = (/ -0.5_pr , -0.5_pr  ,-0.5_pr /)
       x1_loc(1:dim)    = (/ 0.2_pr  , 0.15_pr  , 0.1_pr /)
    END IF
    ! set Initial Conditions
    IF( dim == 1) THEN 
       xp(:,1) =  (x(:,1)-x1_loc(1)) 
    ELSE IF( dim == 2) THEN 
       xp(:,1) =  (x(:,1)-x1_loc(1)) * COS(theta_loc(1))+ (x(:,2)-x1_loc(2)) * SIN(theta_loc(1))
       xp(:,2) = -(x(:,1)-x1_loc(1)) * SIN(theta_loc(1))+ (x(:,2)-x1_loc(2)) * COS(theta_loc(1))
    ELSE
       xp(:,1) = ( COS(theta_loc(3))*COS(theta_loc(2))*COS(theta_loc(1))-SIN(theta_loc(3))*SIN(theta_loc(1)))*(x(:,1)-x1_loc(1)) &
            + ( COS(theta_loc(3))*COS(theta_loc(2))*SIN(theta_loc(1))+SIN(theta_loc(3))*SIN(theta_loc(1)))*(x(:,2)-x1_loc(2)) &
            -   COS(theta_loc(3))*SIN(theta_loc(2))*(x(:,3)-x1_loc(3)) 
       xp(:,2) = (-SIN(theta_loc(3))*COS(theta_loc(2))*COS(theta_loc(1))-COS(theta_loc(3))*SIN(theta_loc(1)))*(x(:,1)-x1_loc(1)) &
            + (-SIN(theta_loc(3))*COS(theta_loc(2))*SIN(theta_loc(1))+COS(theta_loc(3))*COS(theta_loc(1)))*(x(:,2)-x1_loc(2)) &
            +   SIN(theta_loc(3))*SIN(theta_loc(2))*(x(:,3)-x1_loc(3)) 
       xp(:,3) =   SIN(theta_loc(2))*COS(theta_loc(1))*(x(:,1)-x1_loc(1)) &
            +   SIN(theta_loc(2))*SIN(theta_loc(1))*(x(:,2)-x1_loc(2)) &
            +   COS(theta_loc(2))*(x(:,3)-x1_loc(3)) 
    END IF

    IF( dim == 1 ) THEN
       u(:,1) = 1.0_pr + exp(-0.5_pr*(x(:,1)-x0_loc(1))**2/nu_loc(0))&
            +exp(-0.5_pr*xp(:,1)**2/nu_loc(1))
    ELSE IF( dim == 2 ) THEN
       u(:,1) = 1.0_pr + exp(-0.5_pr*((x(:,1)-x0_loc(1))**2+(x(:,2)-x0_loc(2))**2)/nu_loc(0)) &
            +exp(-0.5_pr*(xp(:,1)**2/nu_loc(1)+xp(:,2)**2/nu_loc(2)))
    ELSE IF( dim == 3 ) THEN
       u(:,1) = 1.0_pr +exp(-0.5_pr*((x(:,1)-x0_loc(1))**2+(x(:,2)-x0_loc(2))**2+  (x(:,3)-x0_loc(3))**2)/nu_loc(0))&
            +exp(-0.5_pr*(xp(:,1)**2/nu_loc(1)+xp(:,2)**2/nu_loc(2)+xp(:,3)**2/nu_loc(3)))
    END IF

    !WRITE (*, '("in user_exact_soln min/max U:", E12.5,X,E12.5)')  MINVAL(u), MAXVAL(u)

  END SUBROUTINE  user_exact_soln

  SUBROUTINE user_initial_conditions (u, nlocal, ne_local, t_local, scl, scl_fltwt, iter)
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: nlocal, ne_local
    INTEGER  , INTENT (INOUT) :: iter ! iteration of call while adapting initial grid
    REAL (pr), DIMENSION (nlocal,ne_local), INTENT (INOUT) :: u
    REAL (pr), DIMENSION (nlocal,ne_local) :: f
    REAL (pr), INTENT (IN)   :: scl(1:n_var),scl_fltwt
    REAL (pr), INTENT (IN) :: t_local
    REAL(pr) :: xp(nlocal,1:dim)
    REAL (pr), DIMENSION(ne_local) :: scl_u

    INTEGER :: i, nb, idim !TEST
    INTEGER, DIMENSION(ne_local) :: clip
!!$    INTEGER :: seed(2)
!!$    seed(1)=123456789; seed(2)=987654321
!!$    RETURN

    clip = 0 !no clipping for Dirichlet BC
    scl_u = 1.0_pr
    !
    ! Set Mean flow
    !
    Umn = 0.0_pr

    !set variables by hand
    IF(dim == 1) THEN
       theta_loc(1:dim) = (/ -45.0_pr /)/1.8e2_pr*pi   !oriantation of axis for ellipsoid 
       nu_loc(0:dim)    = (/ 0.01_pr , 0.04_pr   /)  ! scales for principal axis of ellipsiod
       x0_loc(1:dim)    = (/-0.4_pr  /)
       x1_loc(1:dim)    = (/ 0.1_pr  /)
    ELSE IF(dim == 2) THEN
       theta_loc(1:dim) = (/ -45.0_pr , -30.0_pr /)/1.8e2_pr*pi   !oriantation of axis for ellipsoid 
       nu_loc(0:dim)    = (/ 0.01_pr , 0.08_pr  , 0.001_pr /)  ! scales for principal axis of ellipsiod
       x0_loc(1:dim)    = (/-0.5_pr , -0.5_pr /)
       x1_loc(1:dim)    = (/ 0.1_pr ,  0.1_pr /)
    ELSE
       theta_loc(1:dim) = (/ 45.0_pr , -45.0_pr , 30.0_pr /)/1.8e2_pr*pi   !oriantation of axis for ellipsoid 
       nu_loc(0:dim)    = (/ 0.05_pr , 0.05_pr  , 0.02_pr , 0.5_pr /)  ! scales for principal axis of ellipsiod
       x0_loc(1:dim)    = (/ -0.5_pr , -0.5_pr  ,-0.5_pr /)
       x1_loc(1:dim)    = (/ 0.2_pr  , 0.15_pr  , 0.1_pr /)
    END IF

    ! set Initial Conditions
    IF( dim == 1) THEN 
       xp(:,1) =  (x(:,1)-x1_loc(1))
    ELSE IF( dim == 2) THEN 
       xp(:,1) =  (x(:,1)-x1_loc(1)) * COS(theta_loc(1))+ (x(:,2)-x1_loc(2)) * SIN(theta_loc(1))
       xp(:,2) = -(x(:,1)-x1_loc(1)) * SIN(theta_loc(1))+ (x(:,2)-x1_loc(2)) * COS(theta_loc(1))
    ELSE IF( dim == 3) THEN
       xp(:,1) = ( COS(theta_loc(3))*COS(theta_loc(2))*COS(theta_loc(1))-SIN(theta_loc(3))*SIN(theta_loc(1)))*(x(:,1)-x1_loc(1)) &
            + ( COS(theta_loc(3))*COS(theta_loc(2))*SIN(theta_loc(1))+SIN(theta_loc(3))*SIN(theta_loc(1)))*(x(:,2)-x1_loc(2)) &
            -   COS(theta_loc(3))*SIN(theta_loc(2))*(x(:,3)-x1_loc(3)) 
       xp(:,2) = (-SIN(theta_loc(3))*COS(theta_loc(2))*COS(theta_loc(1))-COS(theta_loc(3))*SIN(theta_loc(1)))*(x(:,1)-x1_loc(1)) &
            + (-SIN(theta_loc(3))*COS(theta_loc(2))*SIN(theta_loc(1))+COS(theta_loc(3))*COS(theta_loc(1)))*(x(:,2)-x1_loc(2)) &
            +   SIN(theta_loc(3))*SIN(theta_loc(2))*(x(:,3)-x1_loc(3)) 
       xp(:,3) =   SIN(theta_loc(2))*COS(theta_loc(1))*(x(:,1)-x1_loc(1)) &
            +   SIN(theta_loc(2))*SIN(theta_loc(1))*(x(:,2)-x1_loc(2)) &
            +   COS(theta_loc(2))*(x(:,3)-x1_loc(3)) 
    END IF
    !---------- internal forcing -------------------
    IF( dim == 1 ) THEN
       f(:,1) = ((x(:,1)-x0_loc(1))**2/nu_loc(0)**2-1.0_pr/nu_loc(0) ) &
            * exp(-0.5_pr*(x(:,1)-x0_loc(1))**2/nu_loc(0) )&
            + (xp(:,1)**2/nu_loc(1)**2-1.0_pr/nu_loc(1)) &
            * exp(-0.5_pr*xp(:,1)**2/nu_loc(1) ) 
    ELSE IF (dim == 2 ) THEN
       f(:,1) = ((x(:,1)-x0_loc(1))**2/nu_loc(0)**2+(x(:,2)-x0_loc(2))**2/nu_loc(0)**2-2.0_pr/nu_loc(0) ) &
            * exp(-0.5_pr*((x(:,1)-x0_loc(1))**2+(x(:,2)-x0_loc(2))**2)/nu_loc(0) )&
            + (xp(:,1)**2/nu_loc(1)**2+xp(:,2)**2/nu_loc(2)**2-1.0_pr/nu_loc(1)-1.0_pr/nu_loc(2)) &
            * exp(-0.5_pr*( xp(:,1)**2/nu_loc(1) + xp(:,2)**2/nu_loc(2) ) ) 
    ELSE IF (dim == 3 ) THEN
       f(:,1) = ((x(:,1)-x0_loc(1))**2/nu_loc(0)**2+(x(:,2)-x0_loc(2))**2/nu_loc(0)**2+(x(:,3)-x0_loc(3))**2/nu_loc(0)**2-3.0_pr/nu_loc(0) ) &
            * exp(-0.5_pr*((x(:,1)-x0_loc(1))**2+(x(:,2)-x0_loc(2))**2+  (x(:,3)-x0_loc(3))**2)/nu_loc(0)) &
            + (xp(:,1)**2/nu_loc(1)**2+xp(:,2)**2/nu_loc(2)**2+xp(:,3)**2/nu_loc(3)**2-1.0_pr/nu_loc(1)-1.0_pr/nu_loc(2)-1.0_pr/nu_loc(3) ) &
            * exp(-0.5_pr*( xp(:,1)**2/nu_loc(1) + xp(:,2)**2/nu_loc(2)  + xp(:,3)**2/nu_loc(3)) ) 
    END IF

    !--------- Dirichlet boundary conditions -------------------- 
    IF(Nwlt_lev(j_lev,1) >  Nwlt_lev(j_lev,0)) THEN
       nb = Nwlt_lev(j_lev,0)
       IF( dim == 1 ) THEN
          f(nb+1:nlocal,1) =  1.0_pr + exp(-0.5_pr* (x(nb+1:nlocal,1)-x0_loc(1))**2/nu_loc(0) ) &
               + exp(-0.5_pr*xp(nb+1:nlocal,1)**2/nu_loc(1) )
       ELSE IF( dim == 2 ) THEN
          f(nb+1:nlocal,1) =  1.0_pr + exp(-0.5_pr*( (x(nb+1:nlocal,1)-x0_loc(1))**2+(x(nb+1:nlocal,2)-x0_loc(2))**2 )/nu_loc(0) ) &
               + exp(-0.5_pr*( xp(nb+1:nlocal,1)**2/nu_loc(1)+xp(nb+1:nlocal,2)**2/nu_loc(2) ) )
       ELSE IF( dim == 3 ) THEN
          f(nb+1:nlocal,1) =  1.0_pr + exp(-0.5_pr*( (x(nb+1:nlocal,1)-x0_loc(1))**2+(x(nb+1:nlocal,2)-x0_loc(2))**2+ (x(nb+1:nlocal,3)-x0_loc(3))**2 )/nu_loc(0) ) &
               + exp(-0.5_pr*( xp(nb+1:nlocal,1)**2/nu_loc(1)+xp(nb+1:nlocal,2)**2/nu_loc(2)+xp(nb+1:nlocal,3)**2/nu_loc(3) ) )
       END IF
    END IF
    
    CALL Laplace_rhs (j_lev, u(:,1:ne_local), f(:,1:ne_local), nlocal, ne_local)

    CALL Linsolve (u(:,1:ne_local), f(:,1:ne_local) , tol2, nlocal, ne_local, clip, Laplace, Laplace_diag, SCL=scl_u)  !

  END SUBROUTINE user_initial_conditions

  !--********************************  
  ! Arguments
  ! u         - field on adaptive grid
  ! nlocal      - number of active points
  ! ne_local       - number of equations
  ! t         - current time
  !
  ! Global variables used
  ! ifn       - number of equations
  ! x()
  ! xO
  ! y0
  !--******************************** 
  SUBROUTINE user_algebraic_BC (Lu, u, nlocal, ne_local, jlev, meth)
    !--Defines boundary condition type
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: jlev, meth, ne_local, nlocal
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (INOUT) :: Lu
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (IN)    :: u

    INTEGER :: ie, ii, shift
    REAL (pr), DIMENSION (nlocal,ne_local) :: du, d2u


    !
    ! There are periodic BC conditions
    !


  END SUBROUTINE user_algebraic_BC

  SUBROUTINE user_algebraic_BC_diag (Lu_diag, nlocal, ne_local, jlev, meth)
    !--Defines boundary condition type
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: jlev, meth, ne_local, nlocal
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (INOUT) :: Lu_diag
    REAL (pr), DIMENSION (nlocal,dim) :: du, d2u

    !
    ! There are periodic BC conditions
    !

  END SUBROUTINE user_algebraic_BC_diag



  SUBROUTINE user_algebraic_BC_rhs (rhs, ne_local, nlocal, jlev)
    !--Sets rhs for boundary conditions
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: ne_local, nlocal, jlev
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (INOUT) :: rhs

    INTEGER :: ie, ii, shift
    !
    ! There are periodic BC conditions
    !

  END SUBROUTINE user_algebraic_BC_rhs

  SUBROUTINE user_project (u, p, nlocal, meth)
    !--Makes u divergence free
    IMPLICIT NONE

    INTEGER, PARAMETER :: ne_local = 1
    INTEGER, INTENT (IN) :: meth, nlocal
    REAL (pr), DIMENSION (nlocal),     INTENT(INOUT) :: p
    REAL (pr), DIMENSION (nlocal,n_integrated), INTENT(INOUT) :: u

    !------------ not used in this case

  END SUBROUTINE user_project


  FUNCTION Laplace (jlev, u, nlocal, ne_local, meth)
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: jlev, nlocal, ne_local, meth
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (INOUT) :: u
    REAL (pr), DIMENSION (nlocal*ne_local) :: Laplace

    INTEGER :: i, ii, ie, shift, meth1, meth2, idim
    REAL (pr), DIMENSION (ne_local,nlocal,dim) :: du, d2u
    REAL (pr), DIMENSION (ne_local*dim ,nlocal,dim) :: d2u_divgrad
    REAL (pr), DIMENSION (ne_local*dim ,nlocal,dim) :: du_dummy ! passed when only calculating 1st derivative.
    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc

    divgrad = .FALSE.
    meth1 = meth + 2
    meth2 = meth + 4

!!$    du = 0.0_pr
!!$    d2u = 0.0_pr

    !
    ! Find 2nd deriviative of u. 
    !
!!$       CALL c_diff_fast (x(:,2)**2, du, d2u, jlev, nlocal, meth, 01, ne_local, 1, ne_local)
!!$       PRINT *,'Laplace ', MINVAL(ABS(d2u(1,:,2))), MAXVAL(ABS(d2u(1,:,2))), SQRT(SUM(ABS(d2u(1,:,2))*ABS(d2u(1,:,2))))!TESTDG
!!$       PAUSE
   
    CALL c_diff_fast (u, du, d2u, jlev, nlocal, meth, 11, ne_local, 1, ne_local)


    DO ie = 1, ne_local
       shift=(ie-1)*nlocal

       idim = 1

       Laplace(shift+1:shift+Nwlt_lev(jlev,0)) = SUM(d2u( ie ,1:Nwlt_lev(jlev,0),1:dim),DIM=2)

!!$       PRINT *,'Laplace: ', MINVAL(ABS(Laplace)), MAXVAL(ABS(Laplace))
       !--Boundary points
       i_p_face(0) = 1
       DO i=1,dim
          i_p_face(i) = i_p_face(i-1)*3
       END DO
       DO face_type = 0, 3**dim - 1
          face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
          IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
             CALL get_all_indices_by_face (face_type, jlev, nloc, iloc)
             IF(nloc > 0 ) THEN ! 2-D & 3-D cases
                IF( face(1) == -1  ) THEN                                 ! Xmin face, entire face
!!$                   Laplace(shift+iloc(1:nloc)) = u(shift+iloc(1:nloc))    !Dirichlet conditions
                   Laplace(shift+iloc(1:nloc)) = du(ie,iloc(1:nloc),1)    !Neuman conditions
                ELSE IF( face(1) == 1 ) THEN                              ! Xmax face, entire face
                   Laplace(shift+iloc(1:nloc)) = u(shift+iloc(1:nloc))    !Dirichlet conditions
!!$                   Laplace(shift+iloc(1:nloc)) = du(ie,iloc(1:nloc),1)    !Neuman conditions
                ELSE IF( ALL( face(1:2) == (/0,-1/) ) ) THEN              ! Ymin face, edges || to X only, no corners
                   Laplace(shift+iloc(1:nloc)) = u(shift+iloc(1:nloc))    !Dirichlet conditions
!!!$                Laplace(shift+iloc(1:nloc)) = du(ie,iloc(1:nloc),2)    !Neuman conditions
                ELSE IF( ALL( face(1:2) == (/0,1/) ) ) THEN               ! Ymax face, edges || to X only, no corners
!!$                   Laplace(shift+iloc(1:nloc)) = u(shift+iloc(1:nloc))    !Dirichlet conditions
                   Laplace(shift+iloc(1:nloc)) = du(ie,iloc(1:nloc),2)    !Neuman conditions
                ELSE IF( dim == 3 .AND. ALL( face == (/0,0,-1/) ) ) THEN  ! Zmin face, no edges no cornres
                   Laplace(shift+iloc(1:nloc)) = u(shift+iloc(1:nloc))    !Dirichlet conditions
!!!$                Laplace(shift+iloc(1:nloc)) = du(ie,iloc(1:nloc),3)    !Neuman conditions
                ELSE IF( dim == 3 .AND. ALL( face == (/0,0,1/) ) ) THEN   ! Zmax face, no edges no cornres
                   Laplace(shift+iloc(1:nloc)) = u(shift+iloc(1:nloc))    !Dirichlet conditions
!!!$                Laplace(shift+iloc(1:nloc)) = du(ie,iloc(1:nloc),3)    !Neuman conditions
                END IF
             END IF
          END IF
       END DO
    END DO
!!$    PRINT *,'Laplace_BC: ', MINVAL(ABS(Laplace)), MAXVAL(ABS(Laplace))

!!$    Laplace = 0.0_pr

  END FUNCTION Laplace

  FUNCTION Laplace_diag (jlev,  nlocal, ne_local, meth)
    USE precision
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: jlev, nlocal, ne_local, meth
    REAL (pr), DIMENSION (1:nlocal*ne_local) :: Laplace_diag

    INTEGER :: i, ii, ie, shift, meth1, meth2
    REAL (pr), DIMENSION (nlocal,dim) :: du, d2u

    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc

    meth1 = meth + 2
    meth2 = meth + 4

    DO ie = 1, ne_local
       shift=(ie-1)*nlocal

       CALL c_diff_diag ( du, d2u, jlev, nlocal, meth, meth, 11)
       
       Laplace_diag(shift+1:shift+Nwlt_lev(jlev,0)) = SUM(d2u(1:Nwlt_lev(jlev,0),1:2),DIM=2)

       !--Boundary points
       i_p_face(0) = 1
       DO i=1,dim
          i_p_face(i) = i_p_face(i-1)*3
       END DO

       DO face_type = 0, 3**dim - 1
          face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
          IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points

             CALL get_all_indices_by_face (face_type, jlev, nloc, iloc)

             IF(nloc > 0 ) THEN ! 2-D & 3-D cases
                IF( face(1) == -1  ) THEN                                    ! Xmin face, entire face
!!$                   Laplace_diag(shift+iloc(1:nloc)) = 1.0_pr                 !Dirichlet conditions
                   Laplace_diag(shift+iloc(1:nloc)) = du(iloc(1:nloc),1)  !Neuman conditions
                ELSE IF( face(1) == 1 ) THEN                                 ! Xmax face, entire face
                   Laplace_diag(shift+iloc(1:nloc)) = 1.0_pr                 !Dirichlet conditions
!!!$                Laplace_diag(shift+iloc(1:nloc)) = du(iloc(1:nloc),1)  !Neuman conditions
                ELSE IF( ALL( face(1:2) == (/0,-1/) ) ) THEN                 ! Ymin face, edges || to X only, no corners
                   Laplace_diag(shift+iloc(1:nloc)) = 1.0_pr                 !Dirichlet conditions
!!!$                Laplace_diag(shift+iloc(1:nloc)) = du(iloc(1:nloc),2)  !Neuman conditions
                ELSE IF( ALL( face(1:2) == (/0,1/) ) ) THEN                  ! Ymax face, edges || to X only, no corners
!!$                   Laplace_diag(shift+iloc(1:nloc)) = 1.0_pr                 !Dirichlet conditions
                   Laplace_diag(shift+iloc(1:nloc)) = du(iloc(1:nloc),2)  !Neuman conditions
                ELSE IF( dim == 3 .AND. ALL( face == (/0,0,-1/) ) ) THEN     ! Zmin face, no edges no cornres
                   Laplace_diag(shift+iloc(1:nloc)) = 1.0_pr                 !Dirichlet conditions
!!!$                Laplace_diag(shift+iloc(1:nloc)) = du(iloc(1:nloc),3)  !Neuman conditions
                ELSE IF( dim == 3 .AND. ALL( face == (/0,0,1/) ) ) THEN      ! Zmax face, no edges no cornres
                   Laplace_diag(shift+iloc(1:nloc)) = 1.0_pr                 !Dirichlet conditions
!!!$                Laplace_diag(shift+iloc(1:nloc)) = du(iloc(1:nloc),3)  !Neuman conditions
                END IF
             END IF
          END IF
       END DO

    END DO

  END FUNCTION Laplace_diag

  SUBROUTINE Laplace_rhs (jlev, u, rhs_local, nlocal, ne_local)
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: jlev, nlocal, ne_local
    REAL (pr), DIMENSION (nlocal,ne_local), INTENT (INOUT) :: u, rhs_local

    INTEGER :: i, ii, ie, shift, idim
    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc


    i_p_face(0) = 1
    DO i=1,dim
       i_p_face(i) = i_p_face(i-1)*3
    END DO
    
    DO ie = 1, ne_local
       shift=(ie-1)*nlocal

       DO face_type = 0, 3**dim - 1
          face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
          IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
             CALL get_all_indices_by_face (face_type, jlev, nloc, iloc)
             IF(nloc > 0 ) THEN ! 2-D & 3-D cases
                IF( face(1) == -1  ) THEN                                 ! Xmin face, entire face
                   rhs_local(iloc(1:nloc),ie) = 0.0_pr    !Dirichlet conditions
!!$                ELSE IF( face(1) == 1 ) THEN                              ! Xmax face, entire face
!!$                   rhs_local(iloc(1:nloc),ie) = u(iloc(1:nloc),ie)    !Dirichlet conditions
!!$                ELSE IF( ALL( face(1:2) == (/0,-1/) ) ) THEN              ! Ymin face, edges || to X only, no corners
!!$                   rhs_local(iloc(1:nloc),ie) = u(iloc(1:nloc),ie)    !Dirichlet conditions
                ELSE IF( ALL( face(1:2) == (/0,1/) ) ) THEN               ! Ymax face, edges || to X only, no corners
                   rhs_local(iloc(1:nloc),ie) = 0.0_pr    !Dirichlet conditions
!!$                ELSE IF( dim == 3 .AND. ALL( face == (/0,0,-1/) ) ) THEN  ! Zmin face, no edges no cornres
!!$                   rhs_local(iloc(1:nloc),ie) = u(iloc(1:nloc),ie)    !Dirichlet conditions
!!$                ELSE IF( dim == 3 .AND. ALL( face == (/0,0,1/) ) ) THEN   ! Zmax face, no edges no cornres
!!$                   rhs_local(iloc(1:nloc),ie) = u(iloc(1:nloc),ie)    !Dirichlet conditions
                END IF
             END IF
          END IF
       END DO
    END DO

  END SUBROUTINE Laplace_rhs

  FUNCTION user_rhs (u_integrated,p)
    IMPLICIT NONE
    REAL (pr), DIMENSION (ng,ne), INTENT(IN) :: u_integrated !1D flat version of integrated variables without BC 
    REAL (pr), DIMENSION (ng), INTENT(IN) :: p
    REAL (pr), DIMENSION (n) :: user_rhs

    INTEGER :: ie, shift
    INTEGER, PARAMETER :: meth=1
    REAL (pr), DIMENSION (ne,ng,dim) :: du, d2u
    REAL (pr), DIMENSION (ng,dim)    :: dp

  END FUNCTION user_rhs

  ! find Jacobian of Right Hand Side of the problem
  FUNCTION user_Drhs (u, u_prev_timestep_loc, meth)
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: meth
    !u_prev_timestep passed local to cast it into 2dim array to work with vector derivatives
    REAL (pr), DIMENSION (ng,ne) :: u, u_prev_timestep_loc
    REAL (pr), DIMENSION (n) :: user_Drhs

    INTEGER :: ie, shift
    REAL (pr), DIMENSION (2*ne,ng,dim) :: du  ! 1st derivatives for u (in du(1:ne,:,:)) and u_prev_timestep (in du(ne+1:2*ne,:,:))
    REAL (pr), DIMENSION (ne  ,ng,dim) :: d2u ! 2nd derivatives for u 
    REAL (pr), DIMENSION (ne  ,ng,dim) :: du_dummy ! passed when only calculating 1st derivative.
    !Find batter way to do this!! du_dummy with no storage..
    
  END FUNCTION user_Drhs


  !
  ! Uses u_prev_timestep, which is global flat (1D ) array set in time_step_cn()
  !
  FUNCTION user_Drhs_diag (meth)
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: meth
    REAL (pr), DIMENSION (n) :: user_Drhs_diag

    INTEGER :: ie, shift
    REAL (pr), DIMENSION (ng,dim) :: du, d2u
    REAL (pr), DIMENSION (ne,ng,dim) :: du_prev_timestep
    REAL (pr), DIMENSION (ne  ,ng,dim) :: du_dummy ! passed when only calculating 1st derivative.


  END FUNCTION user_Drhs_diag


  FUNCTION user_chi (nlocal, t_local )
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: nlocal
    REAL (pr), INTENT (IN) :: t_local
    REAL (pr), DIMENSION (nlocal) :: user_chi

    user_chi = 0.0_pr
  END FUNCTION user_chi



  !
  ! Calculate any statitics
  !
  ! startup_flag - 0 when adapting to IC,then 1 inmain integration loop
  !
  !
  SUBROUTINE user_stats ( u ,j_mn, startup_flag)
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: startup_flag
    INTEGER , INTENT (IN) :: j_mn 
    REAL (pr), DIMENSION (nwlt,1:n_var), INTENT (IN) :: u

  END SUBROUTINE user_stats

  !
  ! calculate any additional variables
  ! 
  ! arg 
  ! flag  - 0 calledwhile adapting to IC, 1 - called in main time integration loop
  !
  ! These additiona variables are calculated and left in real space.
  !
  SUBROUTINE user_cal_force (u, n, t_local, force, drag, lift)
    !--Calculates drag and lift on obstacle using penalization formula
    IMPLICIT NONE

    INTEGER, INTENT (IN) :: n
    REAL (pr), INTENT (IN) :: t_local
    REAL (pr), DIMENSION (dim), INTENT (INOUT) :: force
    REAL (pr), INTENT (OUT) :: drag, lift
    REAL (pr), DIMENSION (n,dim) :: u
    drag = 0.0_PR
    lift = 0.0_PR

    !
    ! There is no obstacle in flow
    !

  END SUBROUTINE user_cal_force

  !
  ! calculate any additional variables
  ! 
  ! arg 
  ! flag  - 0 calledwhile adapting to IC, 1 - called in main time integration loop
  !
  ! These additiona variables are calculated and left in real space.
  !
  SUBROUTINE user_additional_vars( t_local, flag )
    IMPLICIT NONE
    REAL (pr), INTENT (IN) ::  t_local
    INTEGER , INTENT(IN) :: flag ! 0- called during adaption to IC, 1 called during main integration loop


    ! Calculate the vorticity if we are in the main integration loop and we
    ! are saving the solution
    ! 
    ! NOTE we do not calculate vorticity in initial adaptation because
    ! derivatives are not setup yet (to save memmory)
    ! This shoild be changed eventually DG
    !
!!$    IF (flag == 1 .AND. t  > twrite ) THEN 
!!$       CALL cal_vort (u(:,1:dim), u(:,n_var_vorticity:n_var_vorticity+3-MOD(dim,3)-1), nwlt)
!!$    END IF
!!$    PRINT * ,'Calc Vort '

  END SUBROUTINE user_additional_vars

  !
  ! calculate any additional scalar variables
  ! 
  ! arg 
  ! flag  - 0 calledwhile adapting to IC, 1 - called in main time integration loop
  !
  SUBROUTINE user_scalar_vars( flag )
    IMPLICIT NONE
    INTEGER , INTENT(IN) :: flag ! 0- called during adaption to IC, 1 called during main integration loop



  END SUBROUTINE user_scalar_vars

  !
  !************ Calculating Scales ***************************
  !
  ! Note the order of the components in the scl array
  ! correspond to u_tn, v_tn, w_tn, u_tn-1, v_tn-1, w_tn-1, u_tn-2, v_tn-2, w_tn-2
  ! 
  SUBROUTINE user_scales(flag, use_default, u_loc, nlocal, ne_local, l_n_var_adapt , l_n_var_adapt_index, &
       scl, scl_fltwt) !add
    USE precision
    USE pde
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: flag ! 0 during initial adaptation, then 1 during time advancement
    LOGICAL , INTENT(INOUT) :: use_default
    INTEGER, INTENT (IN) :: nlocal, ne_local
    REAL (pr), DIMENSION (1:nlocal,1:ne_local), INTENT (IN) :: u_loc
    LOGICAL , INTENT (IN) :: l_n_var_adapt(ne_local)
    INTEGER , INTENT (IN) :: l_n_var_adapt_index(1:ne_local)
    REAL (pr), DIMENSION (1:ne_local), INTENT (INOUT) :: scl
    REAL (pr) ,  INTENT(IN) ::scl_fltwt !weight for temporal filter on scl

    !
    ! Ignore the output of this routine and use default scales routine
    !
    use_default = .TRUE. 

    !
    ! NOTE: For a parallel run, synchronize scl(:) across the processors in user_scales.
    !       Use the subroutine scales of default_util.f90 as an example,
    !       subroutine parallel_global_sum of parallel.f90 may be helpful.
    !       Already synchronized: sumdA_global
    !
  END SUBROUTINE user_scales

  SUBROUTINE user_cal_cfl (use_default, u, cfl_out)
    USE precision
    USE sizes
    USE pde
    USE parallel
    IMPLICIT NONE
    LOGICAL , INTENT(INOUT) :: use_default
    REAL (pr),                                INTENT (INOUT) :: cfl_out
    REAL (pr), DIMENSION (nwlt,n_integrated), INTENT (IN)    :: u

    INTEGER                    :: i
    REAL (pr)                  :: floor
    REAL (pr), DIMENSION (dim) :: cfl
    REAL (pr), DIMENSION(dim,nwlt) :: h_arr

    use_default = .FALSE.

    floor = 1e-12_pr
    cfl_out = floor

    CALL get_all_local_h (h_arr)

    DO i = 1, nwlt
       cfl(1:dim) = ABS (u(i,1:dim)+Umn(1:dim)) * dt/h_arr(1:dim,i)
       cfl_out = MAX (cfl_out, MAXVAL(cfl))
    END DO
    CALL parallel_global_sum( REALMAXVAL=cfl_out )

  END SUBROUTINE user_cal_cfl

  !******************************************************************************************
  !************************************* SGS MODEL ROUTINES *********************************
  !******************************************************************************************

  !
  ! Intialize sgs model
  ! This routine is called once in the first
  ! iteration of the main time integration loop.
  ! weights and model filters have been setup for first loop when this routine is called.
  !
  SUBROUTINE user_init_sgs_model( )
    IMPLICIT NONE


    ! LDM: Giuliano

    ! THE sgs forcign is stored for the time step in sgs_mdl_force(1:nlocal,1:n_integrated)
    ! where nlocal should be nwlt.


    !          print *,'initializing LDM ...'       
    !          CALL sgs_mdl_force( u(:,1:n_integrated), nwlt, j_lev, .TRUE.)


  END SUBROUTINE user_init_sgs_model

  !
  ! calculate sgs model forcing term
  ! user_sgs_force is called int he beginning of each times step in time_adv_cn().
  ! THE sgs forcign is stored for the time step in sgs_mdl_force(1:nlocal,1:n_integrated)
  ! where nlocal should be nwlt.
  ! 
  ! Accesses u from field module, 
  !          j_lev from wlt_vars module,
  !
  SUBROUTINE  user_sgs_force (u_loc, nlocal)
    IMPLICIT NONE

    INTEGER,                         INTENT (IN) :: nlocal
    REAL (pr), DIMENSION (nlocal,n_integrated), INTENT (INOUT) :: u_loc

  END SUBROUTINE  user_sgs_force

  FUNCTION user_sound_speed (u, neq, nwlt)
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: nwlt, neq
    REAL (pr), DIMENSION (nwlt,neq), INTENT(IN) :: u
    REAL (pr), DIMENSION (nwlt) :: user_sound_speed

    !user_sound_speed(:) = SQRT( gamma*(gamma-1.0_pr)* &
    !       ( u(:,4)-0.5_pr*SUM(u(:,n_var_mom(1:dim))**2,DIM=2)/u(:,1)) ) ! pressure
    user_sound_speed = 0.0_pr

  END FUNCTION user_sound_speed

  SUBROUTINE  user_pre_process
    IMPLICIT NONE
     
  END SUBROUTINE user_pre_process

  SUBROUTINE  user_post_process
    IMPLICIT NONE
    
  END SUBROUTINE user_post_process

  
END MODULE user_case
