
#------------------------------------------------------------------#
# General input file format                                        #
#                                                                  #
# comments start with # till the end of the line                   #
# string constant are quoted by "..." or '...'                     #
# boolean can be (T F 1 0 on off) in or without ' or " quotes      #
# integers are integers                                            #
# real numbers are whatever (e.g. 1   1.0   1e-23   1.123d-54 )    #
# vector elements are separated by commas                          #
# spaces between tokens are not important                          #
# empty or comment lines are not important                         #
# order of lines is not important                                  #
#------------------------------------------------------------------#

file_gen   = 'square_tst5.'
#***************************************************************************************
IC_restart_mode 	= 0    #  0: normal, 1: hard restart, 2: soft restart, 3: from IC 
IC_filename 		= 'results/square_tst5.0547.res'
IC_restart_station 	= 547    #  it_start, restart file number to use (NOT iteration!)
IC_file_fmt 		= 0    # IC data file format (0 - native restart file, 1-netcdf, 2- A.Wray in fourier space, 3-simple binary)
Data_file_format 	= F    #  T = formatted, F = unformatted
IC_adapt_grid 		= T    # parameter defaulted to .TRUE. If is set to .FALSE. no grid adaptation is done after the data are read.
do_Sequential_run       = F
#***************************************************************************************

dimension  	= 3		# dimensions

time_integration_method = 2 	# 0- meth2, 1 -krylov, 2 - Crank Nicolson
t_begin	   = 0.00e+00    	# tbeg
t_end      = 2.50e+03     	# tend
t_adapt    = 1.00e+04       	# when t > t_adapt use an adaptive time step if possible
dt         = 1.00e-02	        # dt
dtwrite    = 1.00e-01 		# dtwrite

dtmax = 1.0000000e-01   #  dtmax
dtmin = 1.0000000e-05 	#  dtmin-if dt < dtmin then exection stops(likely blowing up)

# cfl = 1.0000000e-00 	#  cfl
cflmax = 0.7500000e-00 	#  cflmax   0.75
cflmin = 0.0000000e-00 	#  cflmin

coord_min  = -6.0, -6.0, -4.0	#  XMIN, YMIN, ZMIN, etc
coord_max  = 18.0,  6.0,  4.0 	#  XMAX, YMAX, ZMAX, etc

# ZONE parameters: outside of the zone the mesh is uniform at level j_zn
coord_zone_min =  -5.75, -5.75, -1.0e+05  # XMINzone, etc
coord_zone_max =  17.75,  5.75,  1.0e+05  # XMAXzone, etc


# ADD_PLANES
additional_planes_active = F #  if T adds nodes for additional user specified planes and activates the module
j_additional_planes = 9      #  level at which points on the planes are addded
n_additional_planes = 3      #  number of additional planes to be added
x_planes =  1.0,  0.0,  3.0  #  x-coordinates for additional planes
y_planes = -2.0,  0.0,  0.0  #  y-coordinates for additional planes
z_planes =  0.0,  0.0,  0.0  #  z-coordinates for additional planes
dir_planes =  2,    1,    1  #   direction for additioanl planes




#-------------- Parameters related to sponge zone settings:
sponge_active = F           # if .TRUE. adds sponge layer to dump velocity fluctuations
sponge_zone_min =  -5.5, -35.5, -1.0e+05  # sponge zone, etc
sponge_zone_max =  37.0,  35.5,  1.0e+05  # sponge zone, etc
del_sponge_min =  0.2, 0.2, 0.1         # del_sponge: widths of sponge layer at sponge_zone_min(0,1;dim) locations
del_sponge_max =  0.5, 0.2, 0.1         # del_sponge: widths of sponge layer at sponge_zone_max(0,1;dim) locations
eta_sponge     =  0.1                   # eta_sponge: characteristic dumping time of sponge layer


M_vector 	 = 6, 3, 2	#  Mx, My, Mz
j_mn_init  = 3             	#  4 J_mn_init force J_mn == J_INIT while adapting to IC
j_lev_init = 3                  #  4 force j_lev == j_lev_init when initializing
J_MN 	   = 3         		#  4 J_MN
J_MX 	   = 5        		#  7 J_MX
j_IC       = 20                 #  J_IC if the IC data does not have dimensions in it then mxyz(:)*2^(j_IC-1) is used
J_FILT     = 20        	        #  J_FILT
j_zn       = 5                  #  5 
j_tree_root= 3 
periodic 	 = 0,0,1	#  prd(:) (0/1) 0: non-periodic; 1: periodic
uniform 	 = 0,0,0	#  grid(:) (0/1) 0: uniform; 1: non-uniform

tol1 			 = 1.00e-05 	#  tol1 used to set tolerence for non-solenoidal half-step
tol2 			 = 1.00e-04	#  tol2 used to set tolerence for solenoidal half-step
tol3 			 = 1.00e-03     #  used to set tolerence for time step

eps_init 	 = 3.00e-01	# 5.00e-03 (1.5 they match first adapt) EPS used to adapt initial grid  
eps_run 	 = 3.00e-01     # EPS used in run  
eps_adapt_steps =  4    	# eps_adapt_steps ! how many time steps to adapt from eps_init to eps_run
eps_expl = 0.00e-0         	# eps_expl - threshold sed for explicit filtering

Scale_Meth = 3             	# (3) Scale_Meth !1- Linf, 2-L2(vector), 3-L2(physical)
scl_fltwt = 0.0           	#  scl temporal filter weight, scl_new = scl_fltwt*scl_old + ( 1-scl_fltwt)*scl_new
scaleCoeff = 1 1 1 1 1 1 1 1 1 1

i_h = 123456        	#  order of boundaries (1-xmin,2-xmax,3-ymin,4-ymax,5-zmin,6-zmax)
i_l = 111111        	#  algebraic/evolution (1/0) BC order: (lrbt)
N_predict = 1           #  N_predict
N_predict_low_order = 1 # 1 N_predict_low_order
N_update =0           	#  N_update
N_update_low_order = 0  # 0 N_update_low_order
N_diff = 1          		#  N_diff

IJ_ADJ = 1,1,1		# IJ_ADJ(-1:1) = (coarser level), (same level), (finer level)
ADJ_type = 0,0,0        #  ADJ_type(-1:1) = (coarser level), (same level), (finer level) # (0 - less conservative, 1 - more conservative)

BNDzone = F	  	#  BNDzone
BCtype = 0             	#  BCtype (0 - Dirichlet, 1 - Neuman, 2 - Mixed)

nu1 = 1.0000000e-02 	#  nu1 5.0000000e-02
Zero_Mean = F          #  T- enforce zero mean for 1:dim first variables (velocity usually), F- do nothing

u0 = 0.0000000e+00 	#  u0
theta = 0.0000000e+01 		#  theta (in degrees) (angle of 2D Burger's equation, ifn=3=> theta is angular velocity)
theta1 = -4.5000000e+01 	#  theta1 (in degrees) (angle of 2D Burger's equation)
 

# elliptic_zone thickness: 
elliptic_zone_min =  1, 0, 0  # elliptic_zone thickness on MIN boundaries
elliptic_zone_max =  1, 0, 0  # elliptic_zone thickness on MAX boundaries

GS_ACTIVE = F           # if T, use Gauss-Siedel iterations based on level of resolution instead of Jacoby iterations.

debug_force_wrk_wlt_order = F
debug_level = 0                #  debug
debug_c_diff_fast = 0           # 0- nothing,1- derivative MIN/MAXVALS,2- low level, are printed in c_diff_fast
diagnostics_elliptic =  F       #  diagnostics_elliptic: If T print full diagnostic for elliptic solver
GMRESflag = T              	#  GMRESflag
BiCGSTABflag = F              	#  BiCGSTABflag

Jacoby_correction = T           # Jacoby_correction
multigrid_correction = T        # multigrid_correction
GMRES_correction  = F           # GMRES_correction 
kry_p = 3			#  kry_p
kry_perturb = 0			#  kry_perturb
kry_p_coarse = 100           	#  kry_p_coarse
len_bicgstab = 100             	#  len_bicgstab
len_bicgstab_coarse = 2	        #  len_bicgstab_coarse
len_iter = 5            	#  len_iter

W0 = 1.0000000e+00 	#  W0 underrelaxation factor for inner V-cycle
W1 = 1.0000000e+00 	#  W1 underrelaxation factor on the finest level
W2 = 0.6000000e+00 	#  W2 underrelaxation factor for weighted Jacoby (inner points)
W3 = 0.6000000e+00 	#  W3 underrelaxation factor for weighted Jacoby (boundary points)
W_GS = 0.6000000e+00 	#  W_GS overrelaxation factor for Gauss-Sidel (inner points)
W_min = 1.00000e-03	#  W_min correcton factor 
W_max = 1.00000e-00 	#  W_max correction factor 

tol_gmres =  1.0e-5
tol_gmres_stop_if_larger = F   # If true stop iterating in gmres solver if error of last iteration was smaller

wlog = T              	#  wlog

dtforce = 0.0           	#  dtforce ! time interval for calculating the lift force
Pressure_force = 0,0,0		#  Pressure forcing term in x-dr

Cf = 0.0                        #  Linear forcing coefficient
#***************************************************************************************
#  USER CASE INPUTS
#***************************************************************************************

SGS_model = 0             	#  SGS model: 0=none, 1- fixed parameter Smagorinski, 2- dynamic smagorinski model
                                #             3 - global dynamic Smagorinsky (GDM), 4 - Lagrangian dynamic model (LDM)
                                #             5 - dynamic structure model (DSM), 
                                #             6 - localized kinetic energy based model (LKM)
                                #             7 - localized dynamic kinetic energy based model (LKM)
SGS_mdl_form = 3                #  some SGS models have different forms (currently only sgsmodel=4-7)
SGS_TimeIntegrationType = 1     #  0 - explicit 1st order, 1 - semi-implicit 1st order, 2 - semi-implicit 2nd order

#***************************************************************************************
#  FILTER TYPES INPUTS
#***************************************************************************************
                                #  filter types: 0 - none, 1 - >eps, 2 - >2eps, 
				#                3 - level <= j_lev-1, 4 - level <= j_lev-2
				#                5 - 2eps + adjacent zone, 
                                #                6 - local low-pass filter (lowpass_filt_type, lowpassfilt_support, tensorial_filt)
				#                7 - u > eps_explicit filter  
				#                8 - u > eps_explicit filter + adjecent zone 
				#                9 - u > 2eps_explicit filter  
				#               10 - u > 2eps_explicit filter + adjecent zone 
mdl_filt_type_grid = 0          # dyn mdl grid filter, 0 - 6 
mdl_filt_type_test = 6        	# dyn mdl test filter, 0 - 6
lowpass_filt_type = 0        # 0 - volume averged, 1 - trapezoidal
lowpass_filt_type_GRID = 0   # 0 - volume averged, 1 - trapezoidal
lowpass_filt_type_TEST = 1   # 0 - volume averged, 1 - trapezoidal
lowpassfilt_support = 3 #  support for lowpass filter used for other prposes, such as in Lagrangian pathtube averging 
lowpassfilt_support_GRID = 0 #  support for lowpass filter if  mdl_filt_type_grid = 6 
lowpassfilt_support_TEST = 1 #  support for lowpass filter if  mdl_filt_type_test = 6 

tensorial_filt = F      # if .TRUE. uses tensorial filter for comparison with lines

Mcoeff =  2.0          	# Mcoeff=4 in GDM:  Mij = Mcoeff * |S>2eps| Sij>2eps - (|S|Sij )>2eps 
	          	# Mcoeff=2 in LDKM: \hat{\Delta} = Mcoeff * \Delta
deltaMij = F            # deltaMij, Mij definition including delta^2 (GSM, LDM) or delta (LKM, LDKM)
#***************************************************************************************

Lijtraceless = T	# Lijtraceless ! make Lij traceless in dyn sgs model
Mijtraceless = T	# Mijtraceless ! make Mij traceless in dyn sgs model
ExplicitFilter = F	# ExplicitFilter ,apply grid filter as an explicit filter each time step
ExplicitFilterNL = F	# ExplicitFilterNL !apply grid filter as an explicit filter to the non-linear term
DynSmodGridFilt = F	# DynSmodGridFilt !if true |S| is based on grid filt in nu_t=Cdyn|S|
DynSmodGridFiltS = F    # DynSmodGridFiltS !if true S is based on grid filt in tauij = -2 nu_t Sij
DynMdlFiltLijLast = F	#  DynMdlFiltLijLast, if true Lij=(ugrid ugrid - u_test u_test )_test,  else  Lij=(ugrid ugrid)_test - u_test u_test

lowpass_filt_type = 0   # 0 - volume averged, 1 - trapezoidal
tensorial_filt = F      # if .TRUE. uses tensorial filter for comparison with lines

adaptMagVort = F    # Adapt on the magnitude of vorticity
adaptNormS   = F    # Adapt on the L2 of Sij
adaptMagVel  = F    # Adapt on the magnitude of velocity

saveMagVort = T    # Save the magnitude of vorticity
saveNormS   = F    # Save the L2 of Sij
saveVort = T       # Save the vorticity components

#-------------- Smagorinsky-based model ------

SGS_model_coef1 =  0.0  #   sgsmodel_coef1 ! first model coefficient for model set by sgsmodel (see above)

clip_nu_nut = T              	# clip_nu_nut ! clip nu+ nu_t if true
clip_Cs = F              	# clip_Cs ! clip Cs if true

#-------------- LDM --------------------------

LDMtheta = 1.5      # Langrangian model time coefficient
CI = 5.0            # Langrangian model diffusion scale coefficient
adaptIlm = F        # Adapt on the Ilm
adaptImm = F        # Adapt on the Imm

SpaceTimeAvg = F    # SpaceTimeAvg, Lagrangian spatial filter

#-------------- k-based SGS model ------------

adaptK   = F          # Adapt on the Ksgs
#LijLowPassFilter = T  # explicit test lowpass filter- no longer needed
C_eps_sgs = 1.0       # Coefficient for ksgs dissipation (max for clipping)
C_nu_art_k = 0.0      # artificial diffusion coefficient for ksgs evolution
do_const_K = F        # Adjust eps for constant dissipation
alpha_k = 0.10        # portion of the SGS kinetic energy 
Ceps_model = 1	      # 0 - fixed coefficient, 1 - dynamic Bardina-like scaling, 2 - dynamic Germano like scaling
uniform_C_nu = F      # C_nu uniform given the total sgs dissipation

#-------------- DSM --------------------------


#-------------- Constant dissipation ---------

do_const_diss = F   # Adjust eps for constant dissipation
Psgs_diss_goal=0.35 # Percent SGS dissipation we want to model
q_eps=1.0           # Temporal filtering factor for eps change (1=no filtering)
eps_min=0.4         # Allowable lower range for eps to change
eps_max=0.8         # Allowable upper range for eps to change
sgs_gain = 0.0      # rescale used for recalculating tau_ij
Ilm_gain = 0.0      # rescale used for recalculating Ilm,Ilm forcing...

#----------------- Parameters related to Obstacle 
obstacle = T              	# imask !TRUE  - there is an obstacle defined
eta_chi 	 = 1.00e-03					#  1e-3  eta_chi

#-------------- Parameters related to joint-flat plate settings:
Re      = 2.0e+02    	#  Reynolds number (nu = 1/Re)
height  = 1.0		#  height of rectangular box
length  = 1.0		#  length of rectangular box
angle   = 0            #  angle of rectangular box in DEG         

#-------------- Parameters related to user_stats:
t_av       = 1.0E+01   # t_av: time after which the averaging starts
tau_av     = 1.0E+01   # tau_av: exponential time average constant
t_decision = 1.0E+06   # for t>t_decision and drag_av > drag_max, the code stops with dtag_av=drag_max
drag_max   = 1.5E+00   # if for t>t-decision drag>drag_max the codes stops with drag_av=drag_max 
 
#--- TO DO  div_grad_sym - to improve symmetry of the eoperator 
div_grad                = T # if F - div(grad) is approximated analitically as Laplacian
pressure_high_order     = T
divergence_correction   = T # if T - additional projection is perfromed to ensure that the velcity on interpolated mesh is div-free
set_to_zero_if_diverged = F # if T - set the solution of elliptic problem to zero if diverged, F - do not do anything
zero_flux_correction    = F # if T - corrects mean velocity based on net mass flux trhough boundary to ensure no mass accumulation inside of the domain
two_step_pressure       = F # if T - implements 2 step solution, with prediction for pressure using Derichlet BC
doassym 		= F # if T - change assymetry of wavelet transform and derivatives in user_setup_pde

#-------------- Parameters for parallel version
domain_split = 0,0,1
domain_meth  = 3          # 0 geometric  2 zoltan geometric  3 zoltan hypergraph

#-------------- Parameters for Ksgs-equation
initialize_SGS = F
Ksgs_init = 1.0E-2
Ksgs_inflow = 1.0E-2
