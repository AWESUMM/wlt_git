MODULE CMA_input

  USE precision
  IMPLICIT NONE
  PRIVATE

  INTEGER :: N_Q, N_input_names, N_output_names
  REAL(pr), DIMENSION(:), ALLOCATABLE :: Q 
  CHARACTER (LEN=256), DIMENSION(:), ALLOCATABLE ::  input_name_array, output_name_array
  LOGICAL :: CMA_active=.FALSE.
  

  PUBLIC :: lenght, height, angle
  PUBLIC :: read_CMA_input, CMA_active, CMA_test

!!$#define USE_USER_CLI

  CONTAINS
    
    SUBROUTINE read_CMA_input(iarg)
      USE precision
      USE input_file_reader
#ifdef _MSC_VER
      USE DFLIB
#endif
      IMPLICIT NONE

	  INTEGER :: iarg
#ifdef _MSC_VER
      INTEGER(2) :: i
#else
      INTEGER(4) :: i
#endif
      CHARACTER (LEN=256) ::  inputline
      INTEGER :: rec_len, Nrec, rec_beg, rec_end, rec_beg_n, rec_beg_p, next_rec, Nparams, k, itmp
      
    
    ! Use the first argument as the input file name if is exists

      !input line arguments tested
      !n=2p=2p=-.5n=2p=test.inpp=test1.inpn=0

      i=iarg
      CALL getarg( i, inputline )
      
      PRINT *,'inputline:', inputline
      
      rec_len = LEN(TRIM(inputline))
      rec_end = 0
      Nrec = 0
            
      DO WHILE(rec_end < rec_len) 
         rec_beg=INDEX(inputline(rec_end+1:rec_len),'n=')
         !PRINT *, 'test:',INDEX(inputline(rec_end+rec_beg:MIN(rec_end+rec_beg+1,rec_len)),'n=')
         IF(rec_beg>0) THEN
            Nrec = Nrec + 1  
            rec_end = rec_end + rec_beg + 1 
            rec_beg=INDEX(inputline(rec_end+1:rec_len),'p=')-1
            IF(rec_beg == -1) rec_beg = rec_len - rec_end
            !PRINT *,'Nparamsline:', inputline(rec_end+1:rec_end+rec_beg)
            READ(UNIT=inputline(rec_end+1:rec_end+rec_beg),FMT='(I4)') Nparams       ! transform to integer
            rec_end = rec_end + rec_beg 
            IF(Nrec == 1) THEN
               N_Q = Nparams
               IF(ALLOCATED(Q)) DEALLOCATE(Q); IF(Nparams>0) ALLOCATE(Q(1:Nparams))
            ELSE IF(Nrec == 2) THEN
               N_input_names = Nparams
               IF(ALLOCATED(input_name_array)) DEALLOCATE(input_name_array); IF(Nparams>0) ALLOCATE(input_name_array(1:Nparams))
            ELSE IF(Nrec == 3) THEN
               N_output_names = Nparams
               IF(ALLOCATED(output_name_array)) DEALLOCATE(output_name_array); IF(Nparams>0) ALLOCATE(output_name_array(1:Nparams))
            END IF
		    next_rec=rec_end+INDEX(inputline(rec_end+1:rec_len),'n=')-1
            IF(next_rec == rec_end-1) next_rec = rec_len+1
			IF(Nparams==0) rec_end=next_rec-1
            DO k=1,Nparams
               rec_beg=INDEX(inputline(rec_end+1:rec_len),'p=')
               IF(rec_beg==0) THEN
                  WRITE(*,'("INPUT ERROR: no record p= is found")') 
                  WRITE(*,'("PROGRAM is TERMINATED")')
                  STOP
               END IF
               rec_end = rec_end + rec_beg+1
               rec_beg_p=INDEX(inputline(rec_end+1:rec_len),'p=')
               rec_beg_n=INDEX(inputline(rec_end+1:rec_len),'n=')
               IF(MIN(rec_beg_p,rec_beg_n)/=0 ) THEN
                  rec_beg = MIN(rec_beg_p,rec_beg_n)-1
               ELSE IF(MIN(rec_beg_p,rec_beg_n)==0 .AND. MAX(rec_beg_p,rec_beg_n) /=0) THEN
                  rec_beg = MAX(rec_beg_p,rec_beg_n)-1
               ELSE
                  rec_beg = rec_len - rec_end
               END IF
               IF(rec_beg <= 0 .OR. rec_end >= next_rec) THEN
                  WRITE(*,'("INPUT ERROR: # of parematers in input line is ",I4," out of ",I4)') k-1, Nparams
                  WRITE(*,'("PROGRAM is TERMINATED")')
                  STOP
               END IF
               !PRINT *,'line:', inputline(rec_end+1:rec_end+rec_beg)
               IF(Nrec == 1) THEN
			      IF( INDEX(inputline(rec_end+1:rec_end+rec_beg),'.') > 0)  THEN
                    READ(UNIT=inputline(rec_end+1:rec_end+rec_beg),FMT='(E14.8)') Q(k) ! read floaign point format
                  ELSE
			        READ(UNIT=inputline(rec_end+1:rec_end+rec_beg),FMT='(I8)') itmp    ! read integer format
					Q(k) = REAL(itmp,pr)
                  END IF	
				  PRINT *, k,'Q=',Q(k)
               ELSE IF(Nrec == 2) THEN
                  READ(UNIT=inputline(rec_end+1:rec_end+rec_beg),FMT='(A256)') input_name_array(k) ! transform to CHAR*256
               ELSE IF(Nrec == 3) THEN
                  READ(UNIT=inputline(rec_end+1:rec_end+rec_beg),FMT='(A256)') output_name_array(k) ! transform to CHAR*256
               END IF
               rec_end = rec_end + rec_beg 
            END DO
            IF(Nrec == 1)  PRINT *,'Nparams=',Nparams, ' Q=',Q
            IF(Nrec == 2)  THEN 
               DO k = 1, Nparams
                  PRINT *, 'k=',k,' input_name_array=',TRIM(input_name_array(k))
               END DO
            END IF
            IF(Nrec == 3)  THEN 
               DO k = 1, Nparams
                  PRINT *, 'k=',k,' output_name_array=',TRIM(output_name_array(k))
               END DO
            END IF
         ELSE
            rec_end = rec_end + rec_beg  
         END IF
      END DO
      
    END SUBROUTINE read_CMA_input

END MODULE CMA_input



MODULE user_case
  !
  ! Case sphere
  USE precision
  USE elliptic_vars
  USE elliptic_mod
  USE field
  USE input_file_reader
  USE io_3D_vars
  USE pde
  USE share_consts
  USE share_kry
  USE sizes
  USE util_mod
  USE util_vars
  USE vector_util_mod
  USE wavelet_filters_mod
  USE wlt_vars
  USE wlt_trns_vars
  USE wlt_trns_mod
  USE wlt_trns_util_mod
  USE fft_module
  USE SGS_incompressible
  !
  ! case specific variables
  !
  INTEGER n_var_vorticity ! start of vorticity in u array (Must exist)
  INTEGER n_var_pressure  ! start of pressure in u array  (Must exist)

  INTEGER n_var_modvort   ! location of modulus of vorticity
  INTEGER n_var_modSij    ! location of modulus of Sij
  INTEGER n_var_modVel    ! location of velocity magnitude
  INTEGER :: n_var_mask   ! variable for penalization mask 


  LOGICAL :: adaptMagVort, adaptNormS, adaptMagVel
  LOGICAL :: saveMagVort, saveNormS

  REAL(pr) :: Re, length, height, angle
  REAL (pr) :: t_av, tau_av, t_decision, drag_max
  REAL (pr) :: x_sponge(0:1), del_sponge(0:1), eta_sponge
  LOGICAL :: sponge_active
  LOGICAL :: divergence_correction
  REAL(pr), DIMENSION(:), ALLOCATABLE :: chi_sponge 
CONTAINS

  !
  ! In user_setup_pde() we setup how many variables are integrated, which are interpolated
  ! to the next times step and if any exeact solution exists to compare to during the run.
  ! We also set the variable names that are used when the result files are written.
  !
  ! The following variables must be setup in this routine:
  !
  !
  ! n_integrated     ! first n_integrated eqns will be acted on for time integration
  ! n_var_additional ! 
  ! n_var
  !
  !
  !
  !
  SUBROUTINE  user_setup_pde ( VERB ) 
    IMPLICIT NONE
    LOGICAL, OPTIONAL :: VERB         ! print debug info
    INTEGER :: i

    PRINT * ,''
    PRINT *, '**********************Setting up PDE*****************'
    PRINT * ,'CASE ISOTURB '
    PRINT *, '*****************************************************'


    !------------ setting up default values 

    n_integrated = dim ! uvw - # of equations to solve without SGS model
 
    n_integrated = n_integrated + n_var_SGS ! adds additional equations for SGS model
    
    n_time_levels = 1  !--# time levels

    n_var_time_levels = n_time_levels * n_integrated !--Number of equations (2D velocity at two time levels)

    n_var_additional = 2 !--1 pressure at one time level, 1 mask function

    n_var_exact = 0 !--No exact solution 

    n_var_pressure  = n_integrated + 1 !pressure
    n_var_mask      = n_integrated + 2 !mask

    IF( adaptMagVort .or. saveMagVort ) THEN
       n_var_additional = n_var_additional +1
       n_var_modvort    = n_integrated + n_var_additional ! (wi.wi)^0.5vorticity magnitude
    END IF
    IF( adaptNormS .OR. saveNormS) THEN
       n_var_additional = n_var_additional +1
       n_var_modSij     = n_integrated + n_var_additional ! (SijSij)^0.5
    END IF
    IF( adaptMagVel ) THEN
       n_var_additional = n_var_additional +1
       n_var_modVel     = n_integrated + n_var_additional ! (UiUi)^0.5
    END IF

    n_var = n_var_time_levels + n_var_additional !--Total number of variables

    !
    !--Allocate logical arrays 
    !  Must be done after setting number of different types of variables
    !  and before setting logical variable mapping arrays
    !
    CALL alloc_variable_mappings
    !
    ! Fill in variable names for default variables for integration meth 2
    ! These may be changed in the call to the user specified routine
    !  user_setup_pde()
    !
    ! In integrated variables (3D)

                   WRITE (u_variable_names(1), u_variable_names_fmt) 'U    '
    IF( dim >= 2 ) WRITE (u_variable_names(2), u_variable_names_fmt) 'V    '
    IF( dim >= 3 ) WRITE (u_variable_names(3), u_variable_names_fmt) 'W    '
    WRITE (u_variable_names(n_var_pressure), u_variable_names_fmt)   'P    '
    IF( n_var_mask /= 0 ) WRITE (u_variable_names(n_var_mask), u_variable_names_fmt) 'Mask  '
    IF( adaptMagVort .OR. saveMagVort) WRITE (u_variable_names(n_var_modvort), u_variable_names_fmt) 'modWij'
    IF( adaptNormS .OR. saveNormS)   WRITE (u_variable_names(n_var_modSij), u_variable_names_fmt)  'modSij'

    !
    ! setup logical variable mapping arrays
    ! This defined which variables are used for adaptation, Which need to be interpololated to the new grid at 
    ! each time step, which are saved, and for which we have an exect solutiuon we want to check at each time step
    !

    !
    ! setup which components we will base grid adaptation on.
    ! Adapt on the 3 velocity components
    !
    n_var_adapt = .FALSE. !--Initially adapt on integrated variables at first time level
    n_var_adapt(n_var_mask,0) = .TRUE.

    !eventually adapt to mdl variables    n_var_adapt(1:dim+2,1) = .TRUE. !--After first time step adapt on  velocity and Ilm and Imm

    n_var_adapt(1:dim,1) = .TRUE. !--After first time step adapt on  velocity
    n_var_adapt(n_var_mask,1) = .TRUE. !--Adapt on mask

    IF( adaptMagVort ) n_var_adapt(n_var_modvort,1)  = .TRUE.
    IF( adaptNormS )   n_var_adapt(n_var_modSij,1 )  = .TRUE.
    

    !Variables that need to be interpoleted to new adapted grid in initial grid adaptation
    n_var_interpolate(1:n_integrated,0) = .TRUE. 

    !Variables that need to be interpoleted to new adapted grid at each time step
    n_var_interpolate(1:n_var_pressure,0) = .TRUE. 
    n_var_interpolate(1:n_var_pressure,1) = .TRUE. 

    IF( saveMagVort ) n_var_interpolate(n_var_modvort,:) = .TRUE. 
    IF( saveNormS )   n_var_interpolate(n_var_modSij,:) = .TRUE.
    !
    ! setup which components we have an exact solution for

    n_var_exact_soln(:,0:1) = .FALSE.

    !
    ! variables required for restart
    !
    n_var_req_restart = .FALSE.
    n_var_req_restart(1:dim)	        = .TRUE. !restart with velocities and pressure to begin with!
    n_var_req_restart(n_var_pressure)	= .TRUE. !

    ! no pressure for restart from initial file
    !n_var_req_restart(n_var_pressure ) = .TRUE.

    !
    ! setup which variables we will save the solution
    !
    n_var_save(1:n_var_mask) = .TRUE. ! save all for restarting code

    IF( saveMagVort ) n_var_save(n_var_modvort)  = .TRUE.
    IF( saveNormS )   n_var_save(n_var_modSij)  = .TRUE.
    
    !
    !--Time level counters for NS integration
    !   Used for integration meth2
    ! n0 is current time step
    ! n1 is t_n-1 time step
    ! n2 is t_n-2 time step
    !
    n0 = 1; n1 = n0+dim; n2 = n1+dim


    !
    ! Set the maximum number of components for which we have an exact solution
    !
    n_var_exact = MAX( COUNT(n_var_exact_soln(:,0)),COUNT(n_var_exact_soln(:,1)) )


    !
	! Setup a scaleCoeff array of we want to tweak the scaling of a variable
	! ( if scaleCoeff < 1.0 then more points will be added to grid 
	!
    ALLOCATE ( scaleCoeff(1:n_var), Umn(1:n_var) )
    scaleCoeff = 1.0_pr
    scaleCoeff(n_var_Mask) = 10.0_pr

    Umn = 0.0_pr !set up here if mean quantities anre not zero and used in scales or equation
    Umn(1) = 1.0_pr !--Uniform mean velocity in x-direction

    PRINT *, 'n_integrated = ',n_integrated 
    PRINT *, 'n_time_levels = ',n_time_levels
    PRINT *, 'n_var_time_levels = ',n_var_time_levels 
    PRINT *, 'n_var = ',n_var 
    PRINT *, 'n_var_exact = ',n_var_exact 
    PRINT *, '*******************Variable Names*******************'
    DO i = 1,n_var
       WRITE (*, u_variable_names_fmt) u_variable_names(i)
    END DO
    PRINT *, '****************************************************'



  END SUBROUTINE  user_setup_pde

  SUBROUTINE  user_read_command_line_input
 	USE share_consts
    USE CMA_input
	IMPLICIT NONE
    INTEGER:: i

    !***************************************************************
    CALL read_CMA_input(1) !input parameter is the # of the argument 
    !***************************************************************

    file_name = TRIM(input_name_array(1))
	CMA_active = .TRUE.


END SUBROUTINE user_read_command_line_input

  !
  ! read variables from input file for this user case 
  !
  SUBROUTINE  user_read_input()
 	USE share_consts
    USE CMA_input
	IMPLICIT NONE
    INTEGER:: i, k
    CHARACTER (LEN=4) :: id_string
   
  pi = 2.0_pr*ASIN(1.0_pr)

  call input_real ('Re',Re,'stop', &
       ' Re: Reynolds number')
  nu = 1.0_pr/Re

  omega = 0.0_pr
  call input_real ('omega',omega,'default', &
       ' omega: frequency of blade rotation')
  omega = 2.0_pr * pi * omega ! now omega is angular velocity

   call input_logical ('adaptMagVort',adaptMagVort,'stop', &
       '  adaptMagVort, Adapt on the magnitude of vorticity')

   call input_logical ('adaptNormS',adaptNormS,'stop', &
       '  adaptNormS, Adapt on the L2 of Sij')

   call input_logical ('adaptMagVel',adaptMagVel,'stop', &
       '  adaptMagVel, Adapt on the magnitude of velocity')

   saveMagVort = .FALSE.
   call input_logical ('saveMagVort',saveMagVort,'default', &
       '  if .TRUE. adds and saves magnitude of vorticity')

   saveNormS = .FALSE.
   call input_logical ('saveNormS',saveNormS,'default', &
       '  if .TRUE. adds and saves magnitude of strain rate')

!================= Joint-flat blade parameters =================

  call input_real ('length',length,'stop',' lenght of the rectangular box')

  call input_real ('height',height,'stop',' lenght of the rectangular box')

  call input_real ('angle',angle,'stop',' lenght of the rectangular box')

  t_av = 0.0_pr
  call input_real ('t_av',t_av,'default', ' t_av: time after which the averaging starts')

  t_decision = t_begin + 0.2_pr*(t_end-t_begin)
  call input_real ('t_decision',t_decision,'default', ' t_decision: time after which the code stops if drag is too large')

  drag_max = 1.0_pr
  call input_real ('drag_max',drag_max,'default', ' drag_max: sets up drag-av to drag_max is after t_decision drag_av is reached drag_max')

  tau_av = 0.1_pr*(t_end-t_av)

  tau_av = 0.1_pr*(t_end-t_av)
  call input_real ('tau_av',tau_av,'default', ' tau_av: exponential time average constant')

  angle = angle/180.0_pr*pi

  sponge_active = .FALSE.
  call input_logical ('sponge_active',sponge_active,'default', &
                  '  if .TRUE. adds sponge layer to dump velocity fluctuations')

  IF(sponge_active) THEN

     x_sponge = 1.0E+14_pr
     call input_real_vector('x_sponge', x_sponge, 2, 'default',&
                 ' x_sponge: (xmin,xmax) location of sponge layer to dump velocity fluctuations')
     
     del_sponge = 1.0_pr
     call input_real_vector ('del_sponge', del_sponge, 2, 'default', &
          ' del_sponge: width of sponge layers at xsponge(0:1) locations')

     eta_sponge = 1.0_pr
     call input_real ('eta_sponge',eta_sponge,'default', &
          ' eta_sponge: characteristic dumping time of sponge layer')

  END IF

  divergence_correction = .FALSE. 
  call input_logical ('divergence_correction',divergence_correction,'default', &
       '  if T, additional projection is perfromed to ensure that the velcity on interpolated mesh is div-free')


  END SUBROUTINE  user_read_input

  !
  ! Set the exact solution for comparison to the simulated solution
  !
  ! u          - array to fill in the exact solution
  ! nlocal       - number of active wavelets   
  ! ne_local        - total number of equations
  ! t          - time of current time step 
  ! l_n_var_exact_soln_index - index into the elements of u for which we need to 
  !                            find the exact solution
  SUBROUTINE  user_exact_soln (u, nlocal,  t_local, l_n_var_exact_soln)  
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: nlocal
    REAL (pr), INTENT (IN) ::  t_local
    REAL (pr), DIMENSION (nlocal,n_var_exact), INTENT (INOUT) :: u
    LOGICAL , INTENT (IN) :: l_n_var_exact_soln(n_var)
    INTEGER :: i


    ! There is no exact solution

  END SUBROUTINE  user_exact_soln

  SUBROUTINE user_initial_conditions (u, nlocal, ne_local, t_local, scl, scl_fltwt, iter)
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: nlocal, ne_local
    INTEGER  , INTENT (INOUT) :: iter ! iteration of call while adapting initial grid
    REAL (pr), DIMENSION (nlocal,ne_local), INTENT (INOUT) :: u
    REAL (pr)  :: scl(1:n_var),scl_fltwt
    REAL (pr), INTENT (IN) :: t_local

    INTEGER :: i,ie !TEST

    INTEGER, DIMENSION(2) :: SEED
    REAL (pr) ::   Uprime=0.0e-3_pr
    REAL (pr) ::   Umean,U2mean

	!add or overtise teh values in the fileds
    IF (IC_restart ) THEN !in the case of restart
	   !if restarts - DO NOTHING
    ELSE IF (IC_from_file) THEN  ! Initial conditions read from data file 
       !
	   ! Modify appropriate values
       ! 
       !u(:,n_var_pressure) = 1.0_pr
    ELSE !initialization from random field
!!$       CALL randspecU(iter)
       SEED = (/54321,12345/)
       CALL RANDOM_SEED(PUT=SEED(1:2))
       U2mean = 0.0_pr
       DO ie = 1,dim
          DO i=1,nwlt
             CALL RANDOM_NUMBER(u(i,ie))
			 !u(i,ie) = 0.01_pr*(-1.0_pr)**(i) ! to test if wrk and tree are identical, deterministic IC
          END DO
          Umean = SUM(u(:,ie)*dA)/sumdA 
          u(:,ie) = u(:,ie) - Umean
          U2mean = U2mean+SUM(u(:,ie)**2*dA)/sumdA 
       END DO
       u(:,1:dim) = Uprime/SQRT(U2mean/3.0_pr)*u(:,1:dim)
    END IF

  END SUBROUTINE user_initial_conditions

  ! Arguments
  ! u         - field on adaptive grid
  ! nlocal      - number of active points
  ! ne_local       - number of equations
  ! t         - current time
  !
  ! Global variables used
  ! ifn       - number of equations
  ! x()
  ! xO
  ! y0
  !--******************************** 
  SUBROUTINE user_algebraic_BC (Lu, u, nlocal, ne_local, jlev, meth)
    !--Defines boundary condition type
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: jlev, meth, ne_local, nlocal
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (INOUT) :: Lu
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (IN)    :: u
    REAL (pr), DIMENSION (ne_local, nlocal, dim) :: du
    REAL (pr), DIMENSION (ne_local, nlocal, dim) :: d2u

    INTEGER :: i, ie, ii, shift
    !REAL (pr), DIMENSION (ne_nlocal,nlocal,dim) :: du, d2u !uncomment if Neuman BC are used, need to call 
    INTEGER :: face_type, nloc, meth_local
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc

    !BC for both 2D and 3D cases

    !if Neuman BC are used, need to call 
    CALL c_diff_fast (u, du, d2u, jlev, nlocal, meth, 10, ne_local, 1, ne_local)

    DO ie = 1, ne_local
       shift = nlocal*(ie-1)
       !--Go through all Boundary points that are specified
       i_p_face(0) = 1
       DO i=1,dim
          i_p_face(i) = i_p_face(i-1)*3
       END DO
       DO face_type = 0, 3**dim - 1
          face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
          IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
             CALL get_all_indices_by_face (face_type, jlev, nloc, iloc)
             IF(nloc > 0 ) THEN 
                IF( face(1) == -1  ) THEN                          ! Xmin face (entire face) 
                   Lu(shift+iloc(1:nloc)) = u(shift+iloc(1:nloc))  !Dirichlet conditions
                ELSE IF( face(1) == 1  ) THEN                      ! Xmax face (entire face) 
!!$                   Lu(shift+iloc(1:nloc)) = u(shift+iloc(1:nloc))  !Dirichlet conditions
!!$                   Lu(shift+iloc(1:nloc)) = du(ie,iloc(1:nloc),1) + u(shift+iloc(1:nloc))  !Robin conditions
                ELSE IF(  ALL(ABS(face(1:dim)) == (/0,1/))  ) THEN                     ! Ymin & Ymax face (entire face) 
                   IF(ie == 2) THEN
                      Lu(shift+iloc(1:nloc)) = u(shift+iloc(1:nloc))  !Dirichlet conditions
                   ELSE
                      Lu(shift+iloc(1:nloc)) = du(ie,iloc(1:nloc),2)   !Neuman conditions
                   END IF
                END IF
             END IF
          END IF
       END DO
    END DO

    !default SGS BC, can be explicitely deifined below instead of calling default BCs
    CALL SGS_algebraic_BC (Lu, u, nlocal, ne_local, jlev, meth) 

  END SUBROUTINE user_algebraic_BC

  SUBROUTINE user_algebraic_BC_diag (Lu_diag, nlocal, ne_local, jlev, meth)
    !--Defines boundary condition type
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: jlev, meth, ne_local, nlocal
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (INOUT) :: Lu_diag
    REAL (pr), DIMENSION (nlocal,dim) :: du, d2u

    INTEGER :: i, ie, ii, shift
    ! REAL (pr), DIMENSION (nlocal,dim) :: du, d2u  !uncomment if Neuman BC are used
    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc

    !BC for both 2D and 3D cases

    !if Neuman BC are used, need to call 
    CALL c_diff_diag ( du, d2u, jlev, nlocal, meth, meth, 10)

    DO ie = 1, ne_local
       shift = nlocal*(ie-1)
       !--Go through all Boundary points that are specified
       i_p_face(0) = 1
       DO i=1,dim
          i_p_face(i) = i_p_face(i-1)*3
       END DO
       DO face_type = 0, 3**dim - 1
          face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
          IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
             CALL get_all_indices_by_face (face_type, jlev, nloc, iloc)
             IF(nloc > 0 ) THEN 
                IF( face(1) == -1  ) THEN                          ! Xmin face (entire face) 
                   Lu_diag(shift+iloc(1:nloc)) = 1.0_pr            ! Dirichlet conditions
                ELSE IF( face(1) == 1  ) THEN                      ! Xmax face (entire face) 
!!$                   Lu_diag(shift+iloc(1:nloc)) = 1.0_pr         ! Dirichlet condition
!!$                   Lu_diag(shift+iloc(1:nloc)) = du(iloc(1:nloc),1) + 1.0_pr  !Robin conditions
                ELSE IF(  ALL(ABS(face(1:dim)) == (/0,1/))  ) THEN                     ! Ymin & Ymax face (entire face) 
                   IF(ie == 2) THEN
                      Lu_diag(shift+iloc(1:nloc)) = 1.0_pr              !Dirichlet conditions
                   ELSE
                      Lu_diag(shift+iloc(1:nloc)) = du(iloc(1:nloc),2)   !Newman conditions
                   END IF
                END IF
             END IF
          END IF
       END DO
    END DO
    
    !default diagonal terms of SGS BC, can be explicitely deifined below instead of calling default BCs
    IF(sgsmodel /=0 ) CALL SGS_algebraic_BC_diag (Lu_diag, nlocal, ne_local, jlev, meth)

  END SUBROUTINE user_algebraic_BC_diag

  SUBROUTINE user_algebraic_BC_rhs (rhs, ne_local, nlocal, jlev)
    !--Sets rhs for boundary conditions
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: ne_local, nlocal, jlev
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (INOUT) :: rhs

    INTEGER :: i, ie, ii, shift
    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc

    !BC for both 2D and 3D cases

    DO ie = 1, ne_local
       shift = nlocal*(ie-1)
       !--Go through all Boundary points that are specified
       i_p_face(0) = 1
       DO i=1,dim
          i_p_face(i) = i_p_face(i-1)*3
       END DO
       DO face_type = 0, 3**dim - 1
          face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
          IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
             CALL get_all_indices_by_face (face_type, jlev, nloc, iloc)
             IF(nloc > 0 ) THEN 
                IF( face(1) == -1  ) THEN                          ! Xmin face (entire face) 
!!$                   rhs(shift+iloc(1:nloc)) = 0.0_pr                       !Dirichlet conditions
                   rhs(shift+iloc(1:nloc)) = 0.0_pr  !Robin conditions
                ELSE IF( face(1) == 1  ) THEN                      ! Xmax face (entire face) 
!!$                   rhs(shift+iloc(1:nloc)) = 0.0_pr                       !Dirichlet condition
!!$                   rhs(shift+iloc(1:nloc)) = 0.0_pr  !Robin conditions
                ELSE IF(  ALL(ABS(face(1:dim)) == (/0,1/))  ) THEN                     ! Ymin & Ymax face (entire face) 
!!$                   rhs(shift+iloc(1:nloc)) = 0.0_pr                       !Dirichlet conditions
                   rhs(shift+iloc(1:nloc)) = 0.0_pr  !all conditions
                END IF
             END IF
          END IF
       END DO
    END DO

    !default  SGS BC, can be explicitely deifined below instead of calling default BCs
    IF(sgsmodel /= 0) CALL SGS_algebraic_BC_rhs (rhs, ne_local, nlocal, jlev)

  END SUBROUTINE user_algebraic_BC_rhs

  SUBROUTINE user_project (u, p, nlocal, meth)
    !--Makes u divergence free
    IMPLICIT NONE
    INTEGER, PARAMETER :: ne_local = 1
    INTEGER, INTENT (IN) :: meth, nlocal
    INTEGER :: meth_central, meth_backward, meth_forward 
    REAL (pr), DIMENSION (nlocal),     INTENT(INOUT) :: p
    REAL (pr), DIMENSION (nlocal,n_integrated), INTENT(INOUT) :: u
    REAL (pr), DIMENSION (nlocal,ne_local) :: dp
    REAL (pr), DIMENSION (nlocal,ne_local) :: f
    REAL (pr), DIMENSION(ne_local) :: scl_p
    INTEGER, DIMENSION(ne_local) :: clip 

    meth_central  = meth + BIASING_NONE
    meth_backward = meth + BIASING_BACKWARD
    meth_forward  = meth + BIASING_FORWARD

    !--Make u  divergence free
    dp = 0.0_pr 
    f = Laplace_rhs (u(:,1:dim), nlocal, dim, meth)
    clip = 0

    scl_p = MAXVAL(scl_global(1:dim)) !scale of pressure increment based on dynamic pressure
    PRINT *, 'scl_p=',scl_p
    CALL Linsolve (dp, f , tol2, nlocal, ne_local, clip, Laplace, Laplace_diag, SCL=scl_p) !new with scaling 
!    CALL Linsolve (dp, f , tol2, nlocal, ne_local, clip, Laplace, Laplace_diag)  !old without scaling
    u(:,1:dim) = u(:,1:dim) - grad(dp, nlocal, j_lev, meth_backward)
    p = p + dp(:,1)/dt

  END SUBROUTINE user_project

  FUNCTION Laplace (jlev, u, nlocal, ne_local, meth_in)
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: jlev, nlocal, ne_local, meth_in
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (INOUT) :: u
    REAL (pr), DIMENSION (nlocal*ne_local) :: Laplace

    INTEGER :: i, ii, ie, ie1, meth, idim, shift
    INTEGER :: meth_central, meth_backward, meth_forward 
    REAL (pr), DIMENSION (ne_local, nlocal, dim) :: du
    REAL (pr), DIMENSION (dim, nlocal, dim)      :: d2u
    REAL (pr), DIMENSION (dim, nlocal, dim)      :: du_dummy ! passed when only calculating 1st derivative.
    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc

    meth_central  = meth_in + BIASING_NONE
    meth_backward = meth_in + BIASING_BACKWARD
    meth_forward  = meth_in + BIASING_FORWARD

    !
    ! Find 1st deriviative of u. 
    !
    CALL c_diff_fast (u, du, d2u(1:ne_local, 1:nlocal, 1:dim), jlev, nlocal, meth_backward, 10, ne_local, 1, ne_local)

    DO ie = 1, ne_local
       shift=(ie-1)*nlocal
       !--- Internal points
       CALL c_diff_fast(du(ie,1:nlocal,1:dim), d2u(1:dim, 1:nlocal, 1:dim), du_dummy(1:dim, 1:nlocal, 1:dim), &
            jlev, nlocal, meth_forward, 10, dim, 1, dim )
       !--- div(grad)
       idim = 1
       Laplace(shift+1:shift+Nwlt_lev(jlev,0)) = d2u( idim ,1:Nwlt_lev(jlev,0),idim)
       DO idim = 2,dim
          Laplace(shift+1:shift+Nwlt_lev(jlev,0)) = Laplace(shift+1:shift+Nwlt_lev(jlev,0)) + &
               d2u(idim ,1:Nwlt_lev(jlev,0),idim)
       END DO
       !--Boundary points
       i_p_face(0) = 1
       DO i=1,dim
          i_p_face(i) = i_p_face(i-1)*3
       END DO
       DO face_type = 0, 3**dim - 1
          face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
          IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
             CALL get_all_indices_by_face (face_type, jlev, nloc, iloc)
             IF(nloc > 0 ) THEN ! ANY dimension
                Laplace(shift+iloc(1:nloc)) = 0.0_pr
                DO ie1 = 1, dim !no stress: du/dn=0, n - normal
                   Laplace(shift+iloc(1:nloc)) =  Laplace(shift+iloc(1:nloc)) + face(ie1)*du(ie,iloc(1:nloc),ie1)  
                END DO
             END IF
          END IF
       END DO
    END DO
  END FUNCTION Laplace

  FUNCTION Laplace_diag (jlev,  nlocal, ne_local, meth_in)
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: jlev, nlocal, ne_local, meth_in
    REAL (pr), DIMENSION (nlocal*ne_local) :: Laplace_diag

    INTEGER :: i, ii, ie, ie1, idim, shift
    INTEGER :: meth_central, meth_backward, meth_forward 
    REAL (pr), DIMENSION (nlocal,dim) :: du, d2u
    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc

    meth_central  = meth_in + BIASING_NONE
    meth_backward = meth_in + BIASING_BACKWARD
    meth_forward  = meth_in + BIASING_FORWARD

    CALL c_diff_diag ( du, d2u, jlev, nlocal, meth_forward, meth_backward, -11)

    !PRINT *,'IN Laplace_diag, ne_local = ', ne_local

    DO ie = 1, ne_local
       shift=(ie-1)*nlocal
       !--- div(grad)
       !PRINT *,'CAlling c_diff_diag from Laplace_diag() '
       !PRINT *,'--- jlev, nlocal, meth1, meth2', jlev, nlocal, meth1, meth2

       idim = 1
       Laplace_diag(shift+1:shift+Nwlt_lev(jlev,0)) = d2u(1:Nwlt_lev(jlev,0),idim)
       DO idim = 2,dim
          Laplace_diag(shift+1:shift+Nwlt_lev(jlev,0)) = Laplace_diag(shift+1:shift+Nwlt_lev(jlev,0)) + &
               d2u(1:Nwlt_lev(jlev,0),idim)
       END DO
       !--Boundary points
       i_p_face(0) = 1
       DO i=1,dim
          i_p_face(i) = i_p_face(i-1)*3
       END DO
       DO face_type = 0, 3**dim - 1
          face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
          IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
             CALL get_all_indices_by_face (face_type, jlev, nloc, iloc)
             IF(nloc > 0 ) THEN ! ANY dimension
                Laplace_diag(shift+iloc(1:nloc)) = 0.0_pr
                DO ie1 = 1, dim !no stress: du/dn=0, n - normal
                   Laplace_diag(shift+iloc(1:nloc)) =  Laplace_diag(shift+iloc(1:nloc)) + face(ie1)*du(iloc(1:nloc),ie1)  
                END DO
             END IF
          END IF
       END DO
    END DO

   END FUNCTION Laplace_diag

  FUNCTION Laplace_rhs(u, nlocal, ne_local, meth_in)
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: nlocal, ne_local, meth_in
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (INOUT) :: u
    REAL (pr), DIMENSION (nlocal,ne_local) :: Laplace_rhs

    INTEGER :: i, ii, ie, meth, idim
    INTEGER :: meth_central, meth_backward, meth_forward 
    REAL (pr), DIMENSION (ne_local ,nlocal,dim) :: du
    REAL (pr), DIMENSION (ne_local*dim ,nlocal,dim) :: d2u
    REAL (pr), DIMENSION (ne_local*dim ,nlocal,dim) :: du_dummy ! passed when only calculating 1st derivative.
    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc

    meth_central  = meth_in + BIASING_NONE
    meth_backward = meth_in + BIASING_BACKWARD
    meth_forward  = meth_in + BIASING_FORWARD

    Laplace_rhs(:,1) = div(u,nlocal,j_lev,meth_forward)

    !--Boundary points
    i_p_face(0) = 1
    DO i=1,dim
       i_p_face(i) = i_p_face(i-1)*3
    END DO
    DO face_type = 0, 3**dim - 1
       face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
       IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
          CALL get_all_indices_by_face (face_type, j_lev, nloc, iloc)
          IF(nloc > 0 ) THEN 
             Laplace_rhs(iloc(1:nloc),1) = 0.0_pr !Neuman conditions
          END IF
       END IF
    END DO
    
  END FUNCTION Laplace_rhs


  FUNCTION user_rhs (u_integrated,p)
    USE penalization
    IMPLICIT NONE
    REAL (pr), DIMENSION (ng,ne), INTENT(IN) :: u_integrated !1D flat version of integrated variables without BC 
    REAL (pr), DIMENSION (ng), INTENT(IN) :: p
    REAL (pr), DIMENSION (n) :: user_rhs

    INTEGER :: ie, ie1, shift, i
    INTEGER, PARAMETER :: meth=1
    REAL (pr), DIMENSION (ne,ng,dim) :: du, d2u
    REAL (pr), DIMENSION (ng,dim)     :: dp, u_rot

    INTEGER :: face_type, nloc, meth_local
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc
    INTEGER :: meth_central, meth_backward, meth_forward 
    
    meth_central  = meth + BIASING_NONE
    meth_backward = meth + BIASING_BACKWARD
    meth_forward  = meth + BIASING_FORWARD

    u_rot = 0.0_pr
    IF(omega /= 0.0_pr) THEN !for moving obstacle recalculates poistion of the blades
       u_rot(:,1) = - omega * x(:,2) ! rotarion around center of the rotor 
       u_rot(:,2) =   omega * x(:,1) ! rotarion around center of the rotor
!!$       ! rotatrion of individual blade
!!$       DO i=1,Nblade
!!$          Qblade_arr(i)=Qblade+omega*t_local+2.0_pr*pi/REAL(Nblade,pr)*REAL(i-1,pr)
!!$       END DO
!!$       alpha_arr(:) = Ablade*SIN(Qblade_arr(:)-Qblade)
!!$       rot_alpha_arr(:) = omega*Ablade*COS(Qblade_arr(:)-Qblade)
!!$       Xblade_arr = 0.0_pr
!!$       Xblade_arr(1,:) = Rblade*COS(Qblade_arr(:))
!!$       Xblade_arr(2,:) = Rblade*SIN(Qblade_arr(:))
!!$    user_chi = 0.0_pr
!!$    DO i = 1, Nblade
!!$       user_chi  = user_chi + &
!!$    END DO
!!$    user_chi = MIN(1.0_pr, user_chi)

    END IF


   !PRINT *,'user_rhs MINMAX(p) ', MINVAL( p), MAXVAL(dp)
     dp(1:ng,1:dim) = grad (p, ng, j_lev,  meth_backward)

    !PRINT *,'user_rhs MINMAX(dp) ', MINVAL( dp), MAXVAL(dp)
    !PRINT *,'user_rhs MINMAX(u_integrated) ', MINVAL( u_integrated), MAXVAL(u_integrated)

    CALL c_diff_fast(u_integrated, du, d2u, j_lev, ng, 1, 11, ne, 1, ne) !derivatives are calculated even for SGS terms

    !PRINT *,'user_rhs MINMAX(du) ', MINVAL( du), MAXVAL(du)

    !--Form right hand side of Navier-Stokes equations
    DO ie = 1, dim
       shift=(ie-1)*ng
       user_rhs(shift+1:shift+ng) = nu*SUM(d2u(ie,:,:),2) - dp(:,ie) 
       DO ie1 = 1,dim
          user_rhs(shift+1:shift+ng) = user_rhs(shift+1:shift+ng) - (u_integrated(:,ie1)+Umn(ie1))*du(ie,:,ie1) 
       END DO
    END DO

    IF(sgsmodel /= 0) CALL SGS_rhs (user_rhs, u_integrated, du, d2u)

    IF(imask_obstacle) THEN
       DO ie = 1, dim
          shift=(ie-1)*ng
          user_rhs(shift+1:shift+ng) = user_rhs(shift+1:shift+ng) - penal/eta_chi*(u_integrated(:,ie)+Umn(ie))
       END DO
    END IF

    IF(sponge_active) THEN
       DO ie = 2, MIN(2,dim) !dump only vertical velocity
          shift=(ie-1)*ng
          user_rhs(shift+1:shift+ng) = user_rhs(shift+1:shift+ng) - chi_sponge/eta_sponge*u_integrated(:,ie)
       END DO
    END IF

    !--Go through all Boundary points that are specified
    i_p_face(0) = 1
    DO i=1,dim
       i_p_face(i) = i_p_face(i-1)*3
    END DO
    DO face_type = 0, 3**dim - 1
       face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
       IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
          IF( face(1) == 1 .AND. ALL(face(2:dim) == 0)  ) THEN  ! Xmax face (internal points) 
             CALL get_all_indices_by_face (face_type, j_lev, nloc, iloc)
             IF(nloc > 0 ) THEN 
                DO ie = 1, ne
                   shift = ng*(ie-1)
                   user_rhs(shift+iloc(1:nloc)) = 0.0_pr
                   DO ie1 = 1,dim !convection with the mean
!!$                      user_rhs(shift+iloc(1:nloc)) = user_rhs(shift+iloc(1:nloc)) - Umn(ie1)*du(ie,iloc(1:nloc),ie1) 
                      user_rhs(shift+iloc(1:nloc)) = user_rhs(shift+iloc(1:nloc)) - (u_integrated(+iloc(1:nloc),ie1)+Umn(ie1))*du(ie,iloc(1:nloc),ie1) 
                   END DO
                END DO
             END IF
          END IF
       END IF
    END DO


!!$    !--Set operator on boundaries
!!$    IF(SUM(nbnd) /= 0) CALL user_algebraic_BC_rhs (user_rhs, ne, ng, j_lev)

  END FUNCTION user_rhs


  ! find Jacobian of Right Hand Side of the problem
  ! u_prev == u_prev_timestep_loc

  FUNCTION user_Drhs (u, u_prev, meth)
    USE penalization
    IMPLICIT NONE
    INTEGER, INTENT(IN) ::  meth
    !u_prev_timestep passed local to cast it into 2dim array to work with vector derivatives
    REAL (pr), DIMENSION (ng,ne) :: u, u_prev
    REAL (pr), DIMENSION (n) :: user_Drhs

    INTEGER :: i, ie, ie1, shift
    REAL (pr), DIMENSION (2*ne,ng,dim) :: du  ! 1st derivatives for u (in du(1:ne,:,:)) and u_prev_timestep (in du(ne+1:2*ne,:,:))
    REAL (pr), DIMENSION (ne  ,ng,dim) :: d2u ! 2nd derivatives for u 
    REAL (pr), DIMENSION (ne  ,ng,dim) :: du_dummy ! passed when only calculating 1st derivative.
    !Find batter way to do this!! du_dummy with no storage..

    INTEGER :: face_type, nloc, meth_local
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc
    INTEGER :: meth_central, meth_backward, meth_forward 
    
    meth_central  = meth + BIASING_NONE
    meth_backward = meth + BIASING_BACKWARD
    meth_forward  = meth + BIASING_FORWARD

    ! find 1st and 2nd deriviative of u and
    CALL c_diff_fast(u, du(1:ne,:,:), d2u(1:ne,:,:), j_lev, ng, meth, 11, ne , 1, ne )
    
    ! find only first deriviativ e  u_prev_timestep
    CALL c_diff_fast(u_prev, du(ne+1:2*ne,:,:), du_dummy(1:ne,:,:), j_lev, ng, meth, 10, ne , 1, ne )
    
    !CALL c_diff_fast_db(du_b, du_b, j_lev, ng, meth, 10, ne, 1, ne) !find 1st derivative u_b
    
    !--Form right hand side of Navier--Stokes equations
    DO ie = 1, dim
       shift=(ie-1)*ng
       
       user_Drhs(shift+1:shift+ng) =  nu*SUM(d2u(ie,:,:),2)
       DO ie1 = 1, dim
          user_Drhs(shift+1:shift+ng) = user_Drhs(shift+1:shift+ng) - (u_prev(:,ie1)+Umn(ie1))*du(ie,:,ie1) & 
                                                                    - u(:,ie1)*du(ne+ie,:,ie1) 
       END DO
    END DO
       

    IF(sgsmodel /= 0) CALL SGS_Drhs (user_Drhs, u, u_prev, du, d2u, meth)


    IF(imask_obstacle) THEN
       DO ie = 1, dim
          shift=(ie-1)*ng
          user_Drhs(shift+1:shift+ng) = user_Drhs(shift+1:shift+ng) - Dpenal_factor * penal/eta_chi*u(:,ie)
       END DO
    END IF

    IF(sponge_active) THEN
       DO ie = 2, MIN(2,dim) !dump only vertical velocity
          shift=(ie-1)*ng
          user_Drhs(shift+1:shift+ng) = user_Drhs(shift+1:shift+ng) - chi_sponge/eta_sponge*u(:,ie)
       END DO
    END IF



    !--Go through all Boundary points that are specified
    i_p_face(0) = 1
    DO i=1,dim
       i_p_face(i) = i_p_face(i-1)*3
    END DO
    DO face_type = 0, 3**dim - 1
       face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
       IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
          IF( face(1) == 1 .AND. ALL(face(2:dim) == 0)  ) THEN  ! Xmax face (internal points) 
             CALL get_all_indices_by_face (face_type, j_lev, nloc, iloc)
             IF(nloc > 0 ) THEN 
                DO ie = 1, ne
                   shift = ng*(ie-1)
                   user_Drhs(shift+iloc(1:nloc)) = 0.0_pr
                   DO ie1 = 1,dim !convection with the mean
!!$                      user_Drhs(shift+iloc(1:nloc)) = user_Drhs(shift+iloc(1:nloc)) - Umn(ie1)*du(ie,iloc(1:nloc),ie1) 
                      user_Drhs(shift+iloc(1:nloc)) = user_Drhs(shift+iloc(1:nloc)) - (u_prev(iloc(1:nloc),ie1)+Umn(ie1))*du(ie,iloc(1:nloc),ie1) & 
                                                                    - u(iloc(1:nloc),ie1)*du(ne+ie,iloc(1:nloc),ie1) 
                   END DO
                END DO
             END IF
          END IF
       END IF
    END DO


  END FUNCTION user_Drhs


  !
  ! Uses u_prev_timestep, which is global flat (1D ) array set in time_step_cn()
  !
  FUNCTION user_Drhs_diag (meth)
    USE penalization
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: meth
    REAL (pr), DIMENSION (n) :: user_Drhs_diag

    INTEGER :: i, ie, ie1, shift,shiftIlm,shiftImm
    REAL (pr), DIMENSION (ng,dim) :: du, d2u
    REAL (pr), DIMENSION (ne,ng,dim) :: du_prev_timestep
    REAL (pr), DIMENSION (ne  ,ng,dim) :: du_dummy ! passed when only calculating 1st derivative.

    INTEGER :: face_type, nloc, meth_local
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc
    INTEGER :: meth_central, meth_backward, meth_forward 

    meth_central  = meth + BIASING_NONE
    meth_backward = meth + BIASING_BACKWARD
    meth_forward  = meth + BIASING_FORWARD

    CALL c_diff_fast(u_prev_timestep, du_prev_timestep, du_dummy, j_lev, ng, meth, 10, ne, 1, ne)

    !
    ! does not rely on u so we can call it once here
    !
    shift = 0 !tmp
    CALL c_diff_diag(du, d2u, j_lev, ng, meth, meth, 11)

    !--Form right hand side of Navier--Stokes equations
    DO ie = 1, dim
       shift=(ie-1)*ng
       user_Drhs_diag(shift+1:shift+ng) = nu*SUM(d2u,2) - du_prev_timestep(ie,:,ie)
       DO ie1 = 1, dim
          user_Drhs_diag(shift+1:shift+ng) = user_Drhs_diag(shift+1:shift+ng) - (u_prev_timestep((ie1-1)*ng+1:ie1*ng)+Umn(ie1))*du(:,ie1)
       END DO
    END DO
    
    IF(sgsmodel /= 0) CALL SGS_Drhs_diag (user_Drhs_diag, du_prev_timestep, du, d2u, meth)

    IF(imask_obstacle) THEN
       DO ie = 1, dim
          shift=(ie-1)*ng
          user_Drhs_diag(shift+1:shift+ng) = user_Drhs_diag(shift+1:shift+ng) - Dpenal_factor * penal/eta_chi
       END DO
    END IF

    IF(sponge_active) THEN
       DO ie = 2, MIN(2,dim) !dump only vertical velocity
          shift=(ie-1)*ng
          user_Drhs_diag(shift+1:shift+ng) = user_Drhs_diag(shift+1:shift+ng) - chi_sponge/eta_sponge
       END DO
    END IF


    !--Go through all Boundary points that are specified
    i_p_face(0) = 1
    DO i=1,dim
       i_p_face(i) = i_p_face(i-1)*3
    END DO
    DO face_type = 0, 3**dim - 1
       face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
       IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
          IF( face(1) == 1 .AND. ALL(face(2:dim) == 0)  ) THEN  ! Xmax face (internal points) 
             CALL get_all_indices_by_face (face_type, j_lev, nloc, iloc)
             IF(nloc > 0 ) THEN 
                DO ie = 1, ne
                   shift = ng*(ie-1)
                   user_Drhs_diag(shift+iloc(1:nloc)) = 0.0_pr
                   DO ie1 = 1,dim !convection with the mean
!!$                      user_Drhs_diag(shift+iloc(1:nloc)) = user_Drhs_diag(shift+iloc(1:nloc)) - Umn(ie1)*du(iloc(1:nloc),ie1) 
                      user_Drhs_diag(shift+iloc(1:nloc)) = user_Drhs_diag(shift+iloc(1:nloc)) - (u_prev_timestep(iloc(1:nloc))+Umn(ie1))*du(iloc(1:nloc),ie1)
                   END DO
                END DO
             END IF
          END IF
       END IF
    END DO

  END FUNCTION user_Drhs_diag

  FUNCTION user_chi (nlocal, t_local)
    IMPLICIT NONE
    INTEGER,          INTENT (IN) :: nlocal
    REAL (pr),        INTENT (IN) :: t_local
    REAL (pr), DIMENSION (nlocal) :: user_chi
    INTEGER :: i
    LOGICAL, SAVE :: user_chi_initialized = .FALSE.

    !--Defines mask for array of airfoils

    IF(omega /= 0.0_pr .OR. .NOT.user_chi_initialized) THEN !for moving obstacle recalculates poistion of the blades
       DO i=1,Nblade
          Qblade_arr(i)=Qblade+omega*t_local+2.0_pr*pi/REAL(Nblade,pr)*REAL(i-1,pr)
       END DO
       alpha_arr(:) = Ablade*COS(Qblade_arr(:)-Qblade)
       Xblade_arr = 0.0_pr
       Xblade_arr(1,:) = Rblade*COS(Qblade_arr(:))
       Xblade_arr(2,:) = Rblade*SIN(Qblade_arr(:))
       user_chi_initialized = .TRUE.
     END IF
 
    user_chi = 0.0_pr
    DO i = 1, Nblade
       user_chi  = user_chi + airfoil (nlocal, Lblade, Dblade, Cblade, Qblade_arr(i)-0.5_pr*pi-alpha_arr(i), Xblade_arr(1:2,i)) 
    END DO
    user_chi = MIN(1.0_pr, user_chi)

  END FUNCTION user_chi

  FUNCTION airfoil (nlocal, chord, thickness, center, alpha, Xc)
    IMPLICIT NONE
    INTEGER,          INTENT (IN) :: nlocal
    REAL (pr),        INTENT (IN) :: chord, thickness, center, alpha
    REAL (pr), DIMENSION(2), INTENT (IN) :: Xc
    REAL (pr), DIMENSION (nlocal) :: airfoil
    REAL (pr), DIMENSION (nlocal,2) :: Xloc
    INTEGER :: i

    !--Defines mask for NACA0015 airfoil


    airfoil = 0.0_pr

    Xloc(:,1) = COS(alpha)*(x(:,1)-Xc(1)) + SIN(alpha)*(x(:,2)-Xc(2)) !shift and rotate
    Xloc(:,2) =-SIN(alpha)*(x(:,1)-Xc(1)) + COS(alpha)*(x(:,2)-Xc(2)) !shift and rotate
    Xloc(:,1) = Xloc(:,1)/chord+center !scale and orient airfoil with head in opposite direction of x, and shift
    Xloc(:,2) = Xloc(:,2)/chord/thickness

    WHERE ( ABS(Xloc(:,1)-0.5_pr) <= 0.5_pr .AND. &
            ABS(Xloc(:,2)) <= (0.2969_pr*SQRT(ABS(Xloc(:,1)))-0.126_pr*Xloc(:,1)-0.3516_pr*Xloc(:,1)**2&
                              +0.2843_pr*Xloc(:,1)**3-0.1015_pr*Xloc(:,1)**4)/0.2_pr )
       airfoil = 1.0_pr
    ELSEWHERE
       airfoil = 0.0_pr
    END WHERE

  END FUNCTION airfoil

  

  !
  ! Calculate any statitics
  !
  ! startup_flag - 0 when adapting to IC,then 1 inmain integration loop
  !
  !
  SUBROUTINE user_stats (u , j_mn, startup_flag)
!    USE fft_module
!    USE spectra_module
    USE wlt_vars
    USE vector_util_mod
    USE CMA_input
    USE parallel
    IMPLICIT NONE
    REAL (pr), DIMENSION (nwlt,1:n_var), INTENT (IN) :: u
    INTEGER , INTENT (IN) :: j_mn 
    INTEGER , INTENT (IN) :: startup_flag
    CHARACTER (LEN=256)  :: filename

    !================= OBSTACLE ====================
    REAL (pr), SAVE             :: drag, drag_av, drag_exp_av
    REAL (pr), SAVE             :: lift, lift_av, lift_exp_av, q_av
    REAL (pr), DIMENSION (dim)  :: force
    INTEGER, SAVE               :: N_av
    !==============================================
    
    !USER may define additional statistics ouutput here.

    IF(startup_flag /= -1) THEN !statistics accumulation
       IF (par_rank.EQ.0) THEN
          WRITE(*,'(" ")')
          WRITE(*,'("****************** Statistics on the Fly *******************")')
       END IF
       
       IF (imask_obstacle) THEN !--Calculate force
          CALL user_cal_force (u(:,1:dim), nwlt, t, force, drag, lift)
          !--- calculating time averaged quantities ---------
          IF(t <= t_av) THEN
             N_av = 1
             lift_av = lift
             lift_exp_av = lift
             drag_av = drag
             drag_exp_av = drag
          ELSE
             lift_av = (lift_av*REAL(N_av,pr)+lift)/REAL(N_av+1,pr)
             drag_av = (drag_av*REAL(N_av,pr)+drag)/REAL(N_av+1,pr)
             N_av = N_av+1
             q_av = dt/tau_av
             lift_exp_av = (1.0_pr - q_av)*lift_exp_av + q_av*lift
             drag_exp_av = (1.0_pr - q_av)*drag_exp_av + q_av*drag
          END IF
          IF (par_rank.EQ.0) THEN
             WRITE (6,'(" Drag = ", es9.2, 1x, "Drag_av = ", es9.2, 1x, "Drag_exp_av = ", es9.2)') drag, drag_av, drag_exp_av
             WRITE (6,'(" Lift = ", es9.2, 1x, "Lift_av = ", es9.2, 1x, "Lift_exp_av = ", es9.2)') lift, lift_av, lift_exp_av
             
             OPEN  (UNIT=UNIT_USER_STATS, FILE = file_name_user_stats, FORM='formatted', STATUS='old', POSITION='append')
             IF( startup_flag == 0 ) THEN
                WRITE(UNIT=UNIT_USER_STATS,ADVANCE='YES', FMT='( a )' ) &
                     '%Time            Drag            Drag_av         Drag_exp_av     Lift            Lift_av         Lift_exp_av'
             END IF
             WRITE (UNIT_USER_STATS,'(7(es15.8,1x))') t, drag, drag_av, drag_exp_av, lift, lift_av, lift_exp_av
             CLOSE (UNIT=UNIT_USER_STATS)
          END IF
       END IF
       IF (par_rank.EQ.0) THEN
          WRITE(*,'("***********************************************************************")')
          WRITE(*,'(" ")')
       END IF
    ELSE !output for very last line in the code: used in CMA optimization procedure
       WRITE(*,'("AVERAGED DRAG")')
       IF(drag_av < 0.0_pr) THEN
          WRITE(*,'(E12.5)') drag_max
       ELSE
          WRITE(*,'(E12.5)') drag_av
       END IF
    END IF
    IF(t > t_decision .AND. ( drag_av > drag_max .OR. drag_av < 0.0_pr) ) THEN
       WRITE(*,'("MAX DRAG")')
       WRITE(*,'(E12.5)') MAX(drag_av,drag_max)
       STOP
    END IF

  END SUBROUTINE user_stats

  SUBROUTINE user_cal_force (u, n, t_local, force, drag, lift)
    !--Calculates drag and lift on obstacle using penalization formula
    USE penalization
    USE parallel
    IMPLICIT NONE

    INTEGER, INTENT (IN) :: n
    INTEGER :: ie
    REAL (pr), INTENT (IN) :: t_local
    REAL (pr), DIMENSION (dim), INTENT (INOUT) :: force
    REAL (pr), INTENT (OUT) :: drag, lift
    REAL (pr), DIMENSION (n,dim) :: u
    REAL (pr) :: A
    REAL (pr) :: tmp1, tmp2, tmp3

    IF(dim == 3) THEN
       A = Lblade * (xyzlimits(2,dim)-xyzlimits(1,dim))
    ELSE IF(dim == 2) THEN
       A = Lblade
    ELSE
       A = 1.0_pr
    END IF
   
    DO ie = 1, dim
       tmp1 = SUM(penal/eta_chi*(u(:,ie)+Umn(ie))*dA) / (0.5_pr * A * SUM(Umn(1:dim)**2))
       CALL parallel_global_sum( REAL=tmp1 )
       force(ie) = tmp1
    END DO

    drag = force(1) ; lift = force(2)
    
  END SUBROUTINE user_cal_force

  !
  ! calculate any additional variables
  ! 
  ! arg 
  ! flag  - 0 calledwhile adapting to IC, 1 - called in main time integration loop
  !
  ! These additiona variables are calculated and left in real space.
  !
  SUBROUTINE user_additional_vars( t_local, flag )
    IMPLICIT NONE
    REAL (pr), INTENT (IN) ::  t_local
    INTEGER , INTENT(IN) :: flag ! 0- called during adaption to IC, 1 called during main integration loop
    REAL(pr) tmp(nwlt,2*dim) !tmp for vorticity
    INTEGER :: i

    u(:,n_var_mask) =  user_chi (nwlt, t_local) !--Defines mask for sphere (3d) or cylinder (2d)
    
    IF( flag == 1 .OR. adaptMagVort .OR. saveMagVort .OR. adaptNormS .OR. saveNormS) THEN ! only in main time int loop, not initial adaptation
       ! Calculate the magnitude vorticity
       IF( adaptMagVort .OR. saveMagVort ) THEN 
          CALL cal_vort (u(:,n0:n0+dim-1), tmp(:,1:3-MOD(dim,3)), nwlt)
          IF (dim==2) THEN
             u(:,n_var_modvort) = tmp(:,1)
          ELSE
             u(:,n_var_modvort) = SQRT(SUM(tmp(:,1:3-MOD(dim,3))**2, 2))
          END IF
       END IF

       ! Calculate the magnitude of Sij (sqrt(SijSij) )
       ! s(:,1) = s_11
       ! s(:,2) = s_22
       ! s(:,3) = s_33
       ! s(:,4) = s_12
       ! s(:,5) = s_13
       ! s(:,6) = s_23

       IF( adaptNormS .OR. saveNormS) THEN
          CALL Sij( u(:,1:dim) ,nwlt, tmp(:,1:2*dim), u(:,n_var_modSij), .TRUE. )
       END IF

       ! Calculate the magnitude of velocity
       IF( adaptMagVel ) THEN 
          u(:,n_var_modvel) = 0.5_pr*SUM(u(:,1:dim)**2.0_pr,DIM=2)
       END IF

    END IF


  END SUBROUTINE user_additional_vars


  !
  ! calculate any additional scalar variables
  ! 
  ! arg 
  ! flag  - 0 calledwhile adapting to IC, 1 - called in main time integration loop
  !
  SUBROUTINE user_scalar_vars( flag )
    IMPLICIT NONE
    INTEGER , INTENT(IN) :: flag ! 0- called during adaption to IC, 1 called during main integration loop



  END SUBROUTINE user_scalar_vars

  !
  !************ Calculating Scales ***************************
  !
  ! Note the order of the components in the scl array
  ! correspond to u_tn, v_tn, w_tn, u_tn-1, v_tn-1, w_tn-1, u_tn-2, v_tn-2, w_tn-2
  ! 
  SUBROUTINE user_scales(flag, use_default, u_loc, nlocal, ne_local, l_n_var_adapt , l_n_var_adapt_index, &
       scl, scl_fltwt) !add
    USE parallel
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: flag ! 0 during initial adaptation, then 1 during time advancement
    LOGICAL , INTENT(INOUT) :: use_default
    INTEGER, INTENT (IN) :: nlocal, ne_local
    REAL (pr), DIMENSION (1:nlocal,1:ne_local), INTENT (IN) :: u_loc
    LOGICAL , INTENT (IN) :: l_n_var_adapt(ne_local)
    INTEGER , INTENT (IN) :: l_n_var_adapt_index(1:ne_local)
    REAL (pr), DIMENSION (1:ne_local), INTENT (INOUT) :: scl
    REAL (pr) ,  INTENT(IN) ::scl_fltwt !weight for temporal filter on scl
    REAL (pr) :: floor, tmp
    INTEGER :: ie, ie_index, itmp
    REAL (pr) ,save, ALLOCATABLE, DIMENSION (:) :: scl_old
    LOGICAL  ,save :: startup_init = .TRUE.

    !
    ! Ignore the output of this routine and use default scales routine
    !
    use_default = .FALSE. 

    !
    ! NOTE: For a parallel run, synchronize scl(:) across the processors in user_scales.
    !       Use the subroutine scales of default_util.f90 as an example,
    !       subroutine parallel_global_sum of parallel.f90 may be helpful.
    !       Already synchronized: sumdA_global
    !
    IF( .NOT. use_default ) THEN

       IF (par_rank.EQ.0) PRINT *,'Use user_scales() <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<'
       !
       ! ALLOCATE scl_old if it was not done previously in user_scales
       !
       IF( startup_init .AND. .NOT. ALLOCATED(scl_old) ) THEN
          ALLOCATE( scl_old(1:ne_local) )
          scl_old = 0.0_pr
       END IF
       
       floor = 1.e-12_pr
       scl   =1.0_pr
       !
       ! Calculate scl per component
       !
       DO ie=1,ne_local
          IF(l_n_var_adapt(ie)) THEN
             ie_index = l_n_var_adapt_index(ie)
             IF( Scale_Meth == 1 ) THEN ! Use Linf scale
                scl(ie)= MAXVAL ( ABS( u_loc(1:nlocal,ie_index) ) )
                CALL parallel_global_sum( REALMAXVAL=scl(ie) )
                
             ELSE IF ( Scale_Meth == 2 ) THEN ! Use L2 scale
                scl(ie) = SUM( (u_loc(1:nlocal,ie_index)**2) )
                itmp = nlocal
                CALL parallel_global_sum( REAL=scl(ie) )
                CALL parallel_global_sum( INTEGER=itmp )
                scl(ie) = SQRT ( scl(ie)/itmp  )
                
             ELSE IF ( Scale_Meth == 3 ) THEN ! Use L2 scale using dA
                scl(ie) = SUM( (u_loc(1:nlocal,ie_index)**2)*dA )
                CALL parallel_global_sum( REAL=scl(ie) )
                scl(ie)= SQRT ( scl(ie) / sumdA_global  )
                
             ELSE
                IF (par_rank.EQ.0) THEN
                   PRINT *, 'Error in Scales(), Unknown Scale_Meth type: ', Scale_Meth
                   PRINT *, 'Known Scale_Meth types: 1 = Linf scale, 2 = L2 scale '
                   PRINT *, 'Exiting ...'
                END IF
                CALL parallel_finalize; STOP
             END IF
             
             IF (par_rank.EQ.0) THEN
                WRITE (6,'("Scaling before taking vector(1:dim) magnitude")')
                WRITE (6,'("Scale factor eqn", I4 ," = ", es11.4 ," scalecoeff = ",es11.4 )') &
                     ie, scl(ie), scaleCoeff(ie)
             END IF
             
          END IF
       END DO
       !
       ! take appropriate vector norm over scl(1:dim)
       IF( Scale_Meth == 1 ) THEN ! Use Linf scale
          tmp = MAXVAL(scl(1:dim)) !this is statistically equivalent to velocity vector length
          scl(1:dim) = tmp
       ELSE IF ( Scale_Meth == 2 ) THEN ! Use L2 scale
          tmp = SQRT(SUM( scl(1:dim)**2 ) ) !now velocity vector length, rather than individual component
          scl(1:dim) = tmp
       ELSE IF ( Scale_Meth == 3 ) THEN ! Use L2 scale using dA
          tmp = SQRT(SUM( scl(1:dim)**2 ) ) !now velocity vector length, rather than individual component
          scl(1:dim) = tmp
       END IF
       !
       ! Print out new scl
       !
       DO ie=1,ne_local
          IF(l_n_var_adapt(ie)) THEN
             !this is done if one of the variable is exactly zero inorder not to adapt to the nois
             IF(scl(ie) .le. floor) scl(ie)=1.0_pr 
             tmp = scl(ie)
             ! temporally filter scl
             IF(.not. startup_init ) scl(ie) = scl_fltwt * scl_old(ie) + (1.0_pr - scl_fltwt) * scl(ie)
             scl = scaleCoeff * scl
             IF (par_rank.EQ.0) THEN
                WRITE (6,'("Scaling on vector(1:dim) magnitude")')
                WRITE (6,'("Scale factor eqn", I4 ," = ", es11.4 ," scalecoeff = ",es11.4 )') &
                     ie, scl(ie), scaleCoeff(ie)
                WRITE (6,'("(before filtering = ",es11.4,")" )') tmp
             END IF
          END IF
       END DO
       
       scl_old = scl !save scl for this time step
       startup_init = .FALSE.
       !PRINT *,'TEST scl_old ', scl_old
    END IF ! use default
    
  END SUBROUTINE user_scales

  SUBROUTINE user_cal_cfl (use_default, u, cfl_out)
    USE parallel
    IMPLICIT NONE
    LOGICAL , INTENT(INOUT) :: use_default
    REAL (pr),                                INTENT (INOUT) :: cfl_out
    REAL (pr), DIMENSION (nwlt,n_integrated), INTENT (IN)    :: u
    
    INTEGER                    :: i
    REAL (pr)                  :: floor
    REAL (pr), DIMENSION (dim) :: cfl
    REAL (pr), DIMENSION(dim,nwlt) :: h_arr
    
    use_default = .FALSE.
    
    floor = 1e-12_pr
    cfl_out = floor
    
    CALL get_all_local_h (h_arr)
    
    DO i = 1, nwlt
       cfl(1:dim) = ABS (u(i,1:dim)+Umn(1:dim)) * dt/h_arr(1:dim,i)
       cfl_out = MAX (cfl_out, MAXVAL(cfl))
    END DO
    CALL parallel_global_sum( REALMAXVAL=cfl_out )
    
  END SUBROUTINE user_cal_cfl
  
  SUBROUTINE  user_sgs_force ( u, nlocal)
    IMPLICIT NONE
    
    INTEGER,                         INTENT (IN) :: nlocal
    REAL (pr), DIMENSION (nlocal,n_integrated), INTENT (INOUT) :: u
    
    !default SGS_force, can be explicitely deifined below instead of calling default SGS_force
    IF(sgsmodel /= 0) CALL sgs_force ( u, nlocal)
    
  END SUBROUTINE user_sgs_force

  FUNCTION user_sound_speed (u, neq, nwlt)
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: nwlt, neq
    REAL (pr), DIMENSION (nwlt,neq), INTENT(IN) :: u
    REAL (pr), DIMENSION (nwlt) :: user_sound_speed

    !user_sound_speed(:) = SQRT( gamma*(gamma-1.0_pr)* &
    !       ( u(:,4)-0.5_pr*SUM(u(:,n_var_mom(1:dim))**2,DIM=2)/u(:,1)) ) ! pressure

    user_sound_speed(:) = 0.0_pr

  END FUNCTION user_sound_speed

  SUBROUTINE  user_pre_process
    IMPLICIT NONE
    INTEGER, PARAMETER :: ne_local = 1
    INTEGER :: i
    INTEGER :: meth_central, meth_backward, meth_forward 
    REAL (pr), DIMENSION (nwlt,ne_local) :: dp
    REAL (pr), DIMENSION (nwlt,ne_local) :: f
    REAL (pr), DIMENSION(ne_local) :: scl_p
    INTEGER, DIMENSION(ne_local) :: clip 
    
    IF(divergence_correction) THEN
       PRINT *, '**********************************************************************'
       PRINT *, '*                       divergence correction                        *'
       PRINT *, '**********************************************************************'
       meth_central  = HIGH_ORDER + BIASING_NONE
       meth_backward = HIGH_ORDER + BIASING_BACKWARD
       meth_forward  = HIGH_ORDER + BIASING_FORWARD

       !--Make u  divergence free
       dp = 0.0_pr 
       
       f = Laplace_rhs (u(:,1:dim), nwlt, dim, HIGH_ORDER)
       clip = MINVAL(prd(1:dim))
       
       scl_p = MAXVAL(scl_global(1:dim)) !scale of pressure increment based on dynamic pressure
       PRINT *, 'scl_p=',scl_p
       CALL Linsolve (dp, f , tol2, nwlt, ne_local, clip, Laplace, Laplace_diag, SCL=scl_p) !new with scaling 
       u(:,1:dim) = u(:,1:dim) - grad(dp, nwlt, j_lev, meth_backward)

    END IF

    IF(sponge_active) THEN
       IF(ALLOCATED(chi_sponge)) DEALLOCATE(chi_sponge)
       ALLOCATE(chi_sponge(nwlt))
       chi_sponge = 0.0_pr
       DO i = -prd(1), prd(1)
          chi_sponge = chi_sponge + &
               0.5_pr * ( tanh( ( x(:,1)-x_sponge(0)+REAL(i,pr)*(xyzlimits(2,1)-xyzlimits(1,1)) )/del_sponge(0) )    &
               -tanh( ( x(:,1)-x_sponge(1)+REAL(i,pr)*(xyzlimits(2,1)-xyzlimits(1,1)) )/del_sponge(1) ) ) 
       END DO
    END IF
    
  END SUBROUTINE user_pre_process

  SUBROUTINE  user_post_process
    IMPLICIT NONE

    
  END SUBROUTINE user_post_process


END MODULE user_case






