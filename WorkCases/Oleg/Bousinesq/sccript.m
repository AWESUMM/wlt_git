%========================
var_num   = 5; %U, V, T, P, Wij, Sij
case_num  = 3; %Pr=1, 10
saveVideo = false; % T/F 
%========================
case_file=['Boussinesq        ';'Boussinesq_Pr10   ';'test              '];
Pr = [1,10,1000];
dt = 0.5;
var_type=['U     ';'V     ';'T     ';'P     ';'modWij';'modSij'];
var_title=['x velocity ';'y velocity ';'Temperature';'Pressure   ';'Vorticity  ';'|S_{ij}|   '];
coord_range=[-4 4 -4 4];
aspect=[1 1 1];
irange=[0:1:400];
j=0;
h = figure(1);
clear frames;
for i=irange
    j=j+1;
    txt=join(["Re=" sprintf('%2.0f',Pr(case_num)) ",  t=" sprintf('%3.1f',dt*i)]);
    showsolgrid(3,'s',var_type(var_num,:),strtrim(case_file(case_num,:)),i); 
    s1=subplot(1,2,1);
    colormap(s1,jet);
    axis(coord_range);
    set(gca,'DataAspectRatio',aspect);
    text('Units', 'normalized', 'Position', [0.99 1.08], 'String', txt, 'FontSize',20, 'FontWeight','bold');
    title(var_title(var_num,:));
    s2=subplot(1,2,2);
    axis(coord_range);
    set(gca,'DataAspectRatio',aspect);
    title('Grid');
    pause(0.1);
    if saveVideo
        frames(j)=getframe(h);
    end
end
if saveVideo
  v = VideoWriter(['Kelvin-Helmholtz_Pr' sprintf('%2u',Pr(case_num)) '.avi']);
  v.FrameRate = 30;
  v.Quality = 75;
  open(v);
  writeVideo(v,frames);
  close(v);
end