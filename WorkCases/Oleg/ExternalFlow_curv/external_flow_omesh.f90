MODULE user_case
  !
  ! Case sphere
  !
  USE precision
  USE elliptic_vars
  USE elliptic_mod
  USE field
  USE input_file_reader
  USE io_3D_vars
  USE pde
  USE share_consts
  USE share_kry
  USE sizes
  USE util_mod
  USE util_vars
  USE vector_util_mod
  USE wavelet_filters_mod
  USE wlt_vars
  USE wlt_trns_vars
  USE wlt_trns_mod
  USE wlt_trns_util_mod
  USE fft_module
  USE SGS_incompressible
  USE parallel                         !added on 20 jul 2011
  USE VT                      ! Spatial Adaptive Epsilon

  !
  ! case specific variables
  !
  INTEGER, DIMENSION(:), ALLOCATABLE :: n_var_vel
  INTEGER n_var_vorticity ! start of vorticity in u array (Must exist)
  INTEGER n_var_pressure  ! start of pressure in u array  (Must exist)
  INTEGER :: BCtype, BCclip

  INTEGER n_var_modvort   ! location of modulus of vorticity
  INTEGER n_var_modSij    ! location of modulus of Sij
  INTEGER n_var_modVel    ! location of velocity magnitude
  INTEGER n_var_Delta     ! location of Delta (giuliano jan 2014)
  INTEGER n_var_NormGradVort   ! location of modulus of vorticity gradient
  INTEGER :: n_var_mask   ! variable for penalization mask 
  LOGICAL :: adaptMagVort, adaptNormS, adaptMagVel
  LOGICAL :: saveDelta, saveNormGradVort    ! added by Giuliano jan 2014
  LOGICAL :: saveVort     ! Giuliano oct 2010
  LOGICAL :: saveMagVort, saveNormS, initialize_SGS

  REAL(pr) :: Re, R_eta, eta_min, eta_max, Ksgs_inflow, Kdiss_inflow, d_dtau, d0_dtau
  REAL(pr) :: delta_n !   Komeg value inside the obstacle = 800*nu/delta_n**2
  REAL(pr) :: length, height, angle, stretch  !not here because already in VT ???
  REAL (pr) :: t_av, tau_av, t_decision, drag_max
  REAL (pr) :: MAX_COS_INFLOW
  REAL (pr), ALLOCATABLE :: sponge_zone(:,:), del_sponge(:,:), eta_sponge(:)
  LOGICAL :: sponge_active
  LOGICAL :: divergence_correction
  LOGICAL :: divergence_correction_h
  LOGICAL :: div_grad, pressure_high_order, zero_flux_correction, two_step_pressure
  INTEGER :: pressure_BC_type
  REAL(pr), DIMENSION(:), ALLOCATABLE :: chi_sponge 
  REAL(pr), DIMENSION(:), ALLOCATABLE :: grad_P                   ! by Giuliano 
  REAL(pr) :: expl_filt_diss                                      ! by Giuliano
  LOGICAL :: doassym
  INTEGER :: iter_stat = 0

CONTAINS

  !
  ! In user_setup_pde() we setup how many variables are integrated, which are interpolated
  ! to the next times step and if any exeact solution exists to compare to during the run.
  ! We also set the variable names that are used when the result files are written.
  !
  ! The following variables must be setup in this routine:
  !
  !
  ! n_integrated     ! first n_integrated eqns will be acted on for time integration
  ! n_var_additional ! 
  ! n_var
  !
  !
  !
  !
  SUBROUTINE  user_setup_pde ( VERB ) 
    USE curvilinear
    USE curvilinear_mesh 
    USE variable_mapping
    IMPLICIT NONE
    LOGICAL, OPTIONAL :: VERB         ! print debug info
    INTEGER :: i
    REAL(pr) :: nd_assym_high, nd_assym_bnd_high, nd2_assym_high, nd2_assym_bnd_high, nd_assym_low, nd_assym_bnd_low, nd2_assym_low, nd2_assym_bnd_low

    IF (par_rank.EQ.0) THEN
      PRINT * ,''
      PRINT *, '**********************Setting up PDE*****************'
      PRINT * ,'CASE EXTERNAL FLOW '
      PRINT *, '*****************************************************'
    END IF

    !------------ registering variables 

    !register_var( var_name, integrated, adapt, interpolate, exact, saved, req_restart, FRONT_LOAD )
    CALL register_var( ' U ',       integrated=.TRUE.,  adapt=(/.FALSE.,.TRUE./),   interpolate=(/.TRUE.,.TRUE./),   exact=(/.FALSE.,.FALSE./),   saved=.TRUE., req_restart=.TRUE. )
    IF( dim >= 2 ) CALL register_var( ' V ',       integrated=.TRUE.,  adapt=(/.FALSE.,.TRUE./),   interpolate=(/.TRUE.,.TRUE./),   exact=(/.FALSE.,.FALSE./),   saved=.TRUE., req_restart=.TRUE. )
    IF( dim >= 3 ) CALL register_var( ' W ',       integrated=.TRUE.,  adapt=(/.FALSE.,.TRUE./),   interpolate=(/.TRUE.,.TRUE./),   exact=(/.FALSE.,.FALSE./),   saved=.TRUE., req_restart=.TRUE. )
    CALL register_var( ' P ',       integrated=.FALSE.,  adapt=(/.FALSE.,.FALSE./),   interpolate=(/.TRUE.,.TRUE./),   exact=(/.FALSE.,.FALSE./),   saved=.TRUE., req_restart=.TRUE. )
    IF( imask_obstacle ) CALL register_var( ' Mask ',       integrated=.FALSE.,  adapt=(/.TRUE.,.TRUE./),   interpolate=(/.TRUE.,.TRUE./),   exact=(/.FALSE.,.FALSE./),   saved=.TRUE., req_restart=.FALSE. )

    IF( adaptMagVort .OR. saveMagVort) CALL register_var( ' modWij ',       integrated=.FALSE.,  adapt=(/.FALSE.,adaptMagVort/),   interpolate=(/saveMagVort,saveMagVort/),   exact=(/.FALSE.,.FALSE./),   saved= saveMagVort, req_restart=.FALSE. )
    IF( adaptNormS .OR. saveNormS) CALL register_var( ' modSij ',       integrated=.FALSE.,  adapt=(/.FALSE.,adaptNormS/),   interpolate=(/saveNormS,saveNormS/),   exact=(/.FALSE.,.FALSE./),   saved=saveNormS, req_restart=.FALSE. )
    IF( adaptMagVel ) CALL register_var( ' modVel ',       integrated=.FALSE.,  adapt=(/.TRUE.,.TRUE./),   interpolate=(/.FALSE.,.FALSE./),   exact=(/.FALSE.,.FALSE./),   saved=.TRUE., req_restart=.FALSE. )
    IF( saveVort) THEN     
       IF(dim == 2) THEN
          CALL register_var( ' Vort_z ',       integrated=.FALSE.,  adapt=(/.TRUE.,.TRUE./),   interpolate=(/.TRUE.,.TRUE./),   exact=(/.FALSE.,.FALSE./),   saved=.TRUE., req_restart=.FALSE. )
       ELSE IF(dim == 3) THEN
          CALL register_var( ' Vort_x ',       integrated=.FALSE.,  adapt=(/.TRUE.,.TRUE./),   interpolate=(/.TRUE.,.TRUE./),   exact=(/.FALSE.,.FALSE./),   saved=.TRUE., req_restart=.FALSE. )
          CALL register_var( ' Vort_y ',       integrated=.FALSE.,  adapt=(/.TRUE.,.TRUE./),   interpolate=(/.TRUE.,.TRUE./),   exact=(/.FALSE.,.FALSE./),   saved=.TRUE., req_restart=.FALSE. )
          CALL register_var( ' Vort_z ',       integrated=.FALSE.,  adapt=(/.TRUE.,.TRUE./),   interpolate=(/.TRUE.,.TRUE./),   exact=(/.FALSE.,.FALSE./),   saved=.TRUE., req_restart=.FALSE. )
       END IF
    END IF
    IF( saveDelta) CALL register_var( ' Delta ',       integrated=.FALSE.,  adapt=(/.TRUE.,.TRUE./),   interpolate=(/.TRUE.,.TRUE./),   exact=(/.FALSE.,.FALSE./),   saved=.TRUE., req_restart=.FALSE. )
    IF( saveNormGradVort) CALL register_var( ' NormGradVort ',       integrated=.FALSE.,  adapt=(/.TRUE.,.TRUE./),   interpolate=(/.TRUE.,.TRUE./),   exact=(/.FALSE.,.FALSE./),   saved=.TRUE., req_restart=.FALSE. )
    
    CALL setup_VT

    !------------ indexing variables 
    CALL setup_mapping()
    CALL finalize_setup_VT
    CALL print_variable_registery( FULL=.TRUE.)

    ALLOCATE(n_var_vel(1:dim))
    n_var_vel(1)  = get_index('U ')  
    if(dim >= 2) n_var_vel(2)  = get_index('V ')  
    if(dim >= 3) n_var_vel(3)  = get_index('W ')  
    n_var_pressure  = get_index('P ')  
    IF(imask_obstacle) n_var_mask      = get_index('Mask    ')  
    IF( adaptMagVort .or. saveMagVort ) n_var_modvort = get_index('modWij    ')  
    IF( adaptNormS .OR. saveNormS) n_var_modSij = get_index('modSij    ')  
    IF( adaptMagVel ) n_var_modVel = get_index('modVel    ') 
    IF( saveVort) THEN     
       IF(dim == 2) THEN
          n_var_vorticity = get_index('Vort_z    ') 
       ELSE IF(dim == 3) THEN
          n_var_vorticity = get_index('Vort_x    ') 
       END IF
    END IF 
    IF( saveDelta ) n_var_Delta = get_index('Delta    ')
    IF( saveNormGradVort ) n_var_NormGradVort = get_index('NormGradVort    ')

    ! change if modificatin of realtive importance of variables is important, see example below
    ! scaleCoeff(n_var_Mask) = 10.0_pr 

    ALLOCATE ( Umn(1:n_var) )
    Umn = 0.0_pr !set up here if mean quantities anre not zero and used in scales or equation
    Umn(n_var_vel(1)) = 0.0_pr !--Uniform mean velocity in x-direction

    IF (verb_level.GT.0 .AND. par_rank.EQ.0) THEN
       PRINT *, 'n_integrated = ',n_integrated 
       PRINT *, 'n_var = ',n_var 
       PRINT *, 'n_var_exact = ',n_var_exact 
       PRINT *, '*******************Variable Names*******************'
       DO i = 1,n_var
          WRITE (*, u_variable_names_fmt) u_variable_names(i)
       END DO
       PRINT *, '****************************************************'
    END IF

  END SUBROUTINE  user_setup_pde

  !
  ! read variables from input file for this user case 
  !
  SUBROUTINE  user_read_input()
    USE share_consts
    IMPLICIT NONE
    INTEGER:: i
    CHARACTER (LEN=4) :: id_string
   
  call input_real ('Re',Re,'stop', &
       ' Re: Reynolds number')
  nu = 1.0_pr/Re

   call input_logical ('adaptMagVort',adaptMagVort,'stop', &
       '  adaptMagVort, Adapt on the magnitude of vorticity')

   call input_logical ('adaptNormS',adaptNormS,'stop', &
       '  adaptNormS, Adapt on the L2 of Sij')

   call input_logical ('adaptMagVel',adaptMagVel,'stop', &
       '  adaptMagVel, Adapt on the magnitude of velocity')

   saveMagVort = .FALSE.
   call input_logical ('saveMagVort',saveMagVort,'default', &
       '  if .TRUE. adds and saves magnitude of vorticity')

   saveNormS = .FALSE.
   call input_logical ('saveNormS',saveNormS,'default', &
       '  if .TRUE. adds and saves magnitude of strain rate')

   saveVort = .FALSE.   !by Giuliano
   call input_logical ('saveVort',saveVort,'default', &
       '  if .TRUE. adds and saves three components of vorticity vector')

   saveDelta = .FALSE.  !by Giuliano jan 2014
   call input_logical ('saveDelta',saveDelta,'default', &
       '  if .TRUE. adds and saves WTF width')

   saveNormGradVort = .FALSE. !by Giuliano jan 2014
   call input_logical ('saveNormGradVort',saveNormGradVort,'default', &
       '  if .TRUE. adds and saves magnitude of vorticity gradient')

!================= Joint-flat plate parameters =================

! eventually move these lines to io_3d.f90 once the VT module is converged

  call input_real ('length',length,'stop',' lenght of the rectangular box')

  call input_real ('height',height,'stop',' height of the rectangular box')

  call input_real ('angle',angle,'stop',' angle of the rectangular box')

  stretch = 1.0_pr
  call input_real ('stretch',stretch,'default',' stretching factor outside the box')

  pi = 2.0_pr*ASIN(1.0_pr)
  angle = angle / 180_pr * pi ! angle in radians

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! added by Giuliano on 30 sept 2010 to be checked
  ALLOCATE(grad_P(dim))
  grad_P = 0.0_pr
!!!  grad_P(1) = -1.0_pr
  call input_real_vector ('grad_P',grad_P,dim,'default', &
         '  grad_P: mean pressure gradient')
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  t_av = 0.0_pr
  call input_real ('t_av',t_av,'default', ' t_av: time after which the averaging starts')

  t_decision = t_begin + 0.2_pr*(t_end-t_begin)
  call input_real ('t_decision',t_decision,'default', ' t_decision: time after which the code stops if drag is too large')

  drag_max = 1.0_pr
  call input_real ('drag_max',drag_max,'default', ' drag_max: sets up drag-av to drag_max is after t_decision drag_av is reached drag_max')

  tau_av = 0.1_pr*(t_end-t_av)

  tau_av = 0.1_pr*(t_end-t_av)
  call input_real ('tau_av',tau_av,'default', ' tau_av: exponential time average constant')


  sponge_active = .FALSE.
  call input_logical ('sponge_active',sponge_active,'default', &
                  '  if .TRUE. adds sponge layer to dump velocity fluctuations')

  IF(sponge_active) THEN

     ALLOCATE(sponge_zone(0:1,dim), del_sponge(0:1,dim), eta_sponge(dim))

     call input_real_vector('sponge_zone_min', sponge_zone(0,:), dim, 'stop',&
                 ' sponge_zone_min: min locations of sponge zone')

     call input_real_vector('sponge_zone_max', sponge_zone(1,:), dim, 'stop',&
                 ' sponge_zone_max: max locations of sponge zone')
     
     call input_real_vector('del_sponge_min', del_sponge(0,:), dim, 'stop',&
          ' del_sponge: width of sponge layers at sponge_zone_min(0:1,:) locations')

     call input_real_vector('del_sponge_max', del_sponge(1,:), dim, 'stop',&
          ' del_sponge: width of sponge layers at sponge_zone_max(0:1,:) locations')

     call input_real_vector('eta_sponge', eta_sponge(:), dim, 'stop',&
          ' eta_sponge: characteristic dumping times in sponge layers')

  END IF

  divergence_correction = .FALSE. 
  call input_logical ('divergence_correction',divergence_correction,'default', &
       '  if T, additional projection is perfromed to ensure that the velcity on interpolated mesh is div-free')

  div_grad = .TRUE.
  call input_logical ('div_grad',div_grad,'default', &
       '  if F, div(grad) is approximated analityically as Laplacian')

  pressure_high_order = .TRUE.
  call input_logical ('pressure_high_order',pressure_high_order,'default', &
       '  if F, div(grad) is approximated analityically as Laplacian')

  zero_flux_correction = .FALSE.
  call input_logical ('zero_flux_correction',zero_flux_correction,'default', &
       '  if T,  corrects mean velocity based on net mass flux trhough boundary to ensure no mass accumulation inside of the domain')

  two_step_pressure = .FALSE.
  call input_logical (' two_step_pressure',two_step_pressure,'default', &
       ' if T - implements 2 step solution, with prediction for pressure using Derichlet BC')

!!$  doassym = .FALSE.
!!$  call input_logical ('doassym',doassym,'default', 'doassym: T to modify assym parameters in user_code')

   call input_integer ('BCtype', BCtype, 'stop',' BCtype: 0 - Dirichlet, 1 - Neuman')

   call input_integer ('BCclip', BCclip, 'stop',' BCclip: 0 - no clipping, 1 - clipping is on')
   
   MAX_COS_INFLOW = 0.0_pr 
   call input_real ('MAX_COS_INFLOW',  MAX_COS_INFLOW, 'default', 'boundary is considered as inlet for COS(Pi*X2) <= MAX_COS_INFLOW')

  IF (sgsmodel /= 0) THEN

    initialize_SGS = .FALSE.
    call input_logical ('initialize_SGS',initialize_SGS,'default', &
       '  if .TRUE. SGS variables are initialized')

     IF( sgsmodel_DSM <= sgsmodel  .AND. sgsmodel <= sgsmodel_LDKMG ) THEN
        Ksgs_inflow = 0.0_pr
        call input_real ('Ksgs_inflow',Ksgs_inflow,'default', &
          ' Ksgs_inflow: inflow conditions for Ksgs')
     ELSEIF( sgsmodel_KEPS <= sgsmodel  .AND. sgsmodel <= sgsmodel_KOME ) THEN
        Ksgs_inflow = 0.0_pr
        call input_real ('Ksgs_inflow',Ksgs_inflow,'default', &
          ' Ksgs_inflow: inflow conditions for Ksgs')
        Kdiss_inflow = 0.0_pr
        call input_real ('Kdiss_inflow',Kdiss_inflow,'default', &
          ' Kdiss_inflow: inflow conditions for Kdiss')
     END IF

  END IF

   CALL user_read_input__VT

  END SUBROUTINE  user_read_input

  !
  ! Set the exact solution for comparison to the simulated solution
  !
  ! u          - array to fill in the exact solution
  ! nlocal       - number of active wavelets   
  ! ne_local        - total number of equations
  ! t          - time of current time step 
  ! l_n_var_exact_soln_index - index into the elements of u for which we need to 
  !                            find the exact solution
  SUBROUTINE  user_exact_soln (u, nlocal,  t_local, l_n_var_exact_soln)  
    USE variable_mapping
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: nlocal
    REAL (pr), INTENT (IN) ::  t_local
    REAL (pr), DIMENSION (nlocal,n_var_exact), INTENT (INOUT) :: u
    LOGICAL , INTENT (IN) :: l_n_var_exact_soln(n_var)
    INTEGER :: i


    ! There is no exact solution

  END SUBROUTINE  user_exact_soln

  SUBROUTINE user_initial_conditions (u, nlocal, ne_local, t_local, scl, scl_fltwt, iter)
    USE variable_mapping
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: nlocal, ne_local
    INTEGER  , INTENT (INOUT) :: iter ! iteration of call while adapting initial grid
    REAL (pr), DIMENSION (nlocal,ne_local), INTENT (INOUT) :: u
    REAL (pr)  :: scl(1:n_var),scl_fltwt
    REAL (pr), INTENT (IN) :: t_local

    INTEGER :: i,ie !TEST

    INTEGER, DIMENSION(2) :: SEED
    REAL (pr) ::   Uprime=0.0e-3_pr
    REAL (pr) ::   Umean,U2mean,tmp1

    !add or overtise teh values in the fileds
    IF (IC_restart ) THEN !in the case of restart
    !if restarts - DO NOTHING
    ELSE IF (IC_from_file) THEN  ! Initial conditions read from data file 
       !
       ! Modify appropriate values
       ! 
       !u(:,n_var_pressure) = 1.0_pr

       IF (sgsmodel /= 0 .AND. initialize_SGS) THEN
          IF( sgsmodel_DSM <= sgsmodel  .AND. sgsmodel <= sgsmodel_LDKMG ) THEN
               u(:,n_var_K) = Ksgs_init
               IF(par_rank.EQ.0) PRINT *,'Ksgs parameters:', Ksgs_init, Ksgs_inflow
          ELSEIF( sgsmodel_KEPS <= sgsmodel  .AND. sgsmodel <= sgsmodel_KOME ) THEN
               u(:,n_var_K) = Ksgs_init
               u(:,n_var_KD) = Kdiss_init
               IF(par_rank.EQ.0) PRINT *,'Ksgs parameters:', Ksgs_init, Ksgs_inflow
               IF(par_rank.EQ.0) PRINT *,'Kdiss parameters:', Kdiss_init, Kdiss_inflow
          END IF
       END IF

    ELSE !initialization from random field
!!$       CALL randspecU(iter)
       SEED = (/54321,12345/)
       CALL RANDOM_SEED(PUT=SEED(1:2))
       U2mean = 0.0_pr
       DO ie = 1,dim
          DO i=1,nwlt
             CALL RANDOM_NUMBER(u(i,ie))
             !u(i,ie) = 0.01_pr*(-1.0_pr)**(i) ! to test if wrk and tree are identical, deterministic IC
          END DO
!          Umean = SUM(u(:,ie)*dA)/sumdA 
          tmp1 = SUM(u(:,ie)*dA)
          CALL parallel_global_sum( REAL=tmp1 )
          Umean = tmp1/sumdA_global

          u(:,ie) = u(:,ie) - Umean

!          U2mean = U2mean+SUM(u(:,ie)**2*dA)/sumdA 
          tmp1 = SUM(u(:,ie)**2*dA)
          CALL parallel_global_sum( REAL=tmp1 )
          U2mean = U2mean+tmp1/sumdA_global

       END DO
       u(:,1:dim) = Uprime/SQRT(U2mean/3.0_pr)*u(:,1:dim)

       IF (sgsmodel /= 0) THEN
          IF( sgsmodel_DSM <= sgsmodel  .AND. sgsmodel <= sgsmodel_LDKMG ) THEN
               u(:,n_var_K) = Ksgs_init
               IF(par_rank.EQ.0) PRINT *,'Ksgs_init and Ksgs_inflow:', Ksgs_init, Ksgs_inflow
          ELSEIF( sgsmodel_KEPS <= sgsmodel  .AND. sgsmodel <= sgsmodel_KOME ) THEN
               u(:,n_var_K) = Ksgs_init
               u(:,n_var_KD) = Kdiss_init
               IF(par_rank.EQ.0) PRINT *,'Ksgs parameters:', Ksgs_init, Ksgs_inflow
               IF(par_rank.EQ.0) PRINT *,'Kdiss parameters:', Kdiss_init, Kdiss_inflow
          END IF
       END IF

    END IF

!    IF(par_rank.EQ.0) PRINT *,'before calling user_pre_process in user_initial_conditions'
!!    IF(project_IC) THEN
!       divergence_correction_h = divergence_correction
!       divergence_correction = .TRUE.
!       CALL user_pre_process
!       divergence_correction = divergence_correction_h
!!    END IF
!    IF(par_rank.EQ.0) PRINT *,'after calling user_pre_process in user_initial_conditions'

    IF ( do_Adp_Eps_Spatial .OR. do_Adp_Eps_Spatial_Evol ) &
    CALL user_initial_conditions__VT(u, nlocal, ne_local)!, t_local, scl, scl_fltwt, iter)

    ! CC Initial Conditions freestream (TEMPORARY)
    IF (.TRUE.) THEN 
      u(:,n_var_vel(1:dim)) = 0.0_pr
      u(:,n_var_vel(1)) = 1.0_pr - UMN(n_var_vel(1))
      u(:,n_var_pressure) = 0.0_pr
    END IF

  END SUBROUTINE user_initial_conditions

  ! Arguments
  ! u         - field on adaptive grid
  ! nlocal      - number of active points
  ! ne_local       - number of equations
  ! t         - current time
  !
  ! Global variables used
  ! ifn       - number of equations
  ! x()
  ! xO
  ! y0
  !--******************************** 
  SUBROUTINE user_algebraic_BC (Lu, u, nlocal, ne_local, jlev, meth)
    !--Defines boundary condition type
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: jlev, meth, ne_local, nlocal
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (INOUT) :: Lu
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (IN)    :: u
    REAL (pr), DIMENSION (ne_local, nlocal, dim) :: du, du_comp
    REAL (pr), DIMENSION (ne_local, nlocal, dim) :: d2u

    INTEGER :: i, ie, ii, shift
    !REAL (pr), DIMENSION (ne_nlocal,nlocal,dim) :: du, d2u !uncomment if Neuman BC are used, need to call 
    INTEGER :: face_type, nloc, meth_local
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc

    !BC for both 2D and 3D cases

    !if Neuman BC are used, need to call 
    CALL c_diff_fast (u, du_comp, d2u, jlev, nlocal, meth, 10, ne_local, 1, ne_local, FORCE_RECTILINEAR=.TRUE.)
    CALL c_diff_fast (u, du, d2u, jlev, nlocal, meth, 10, ne_local, 1, ne_local)

    DO ie = 1, ne_local
       shift = nlocal*(ie-1)
       !--Go through all Boundary points that are specified
       i_p_face(0) = 1
       DO i=1,dim
          i_p_face(i) = i_p_face(i-1)*3
       END DO
       DO face_type = 0, 3**dim - 1
          face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
          IF( ABS(face(1)) == 1 ) THEN ! goes only through boundary points
             CALL get_all_indices_by_face (face_type, jlev, nloc, iloc)
             IF(nloc > 0 ) THEN 
                IF( face(1) == -1  ) THEN                             ! Xmin face (entire face) 
                   Lu(shift+iloc(1:nloc)) = u(shift+iloc(1:nloc))     !Dirichlet conditions
                END IF
                IF( face(1) ==  1  ) THEN
                  WHERE ( COS(PI*x(iloc(1:nloc),2)) <= MAX_COS_INFLOW )
                   Lu(shift+iloc(1:nloc)) = u(shift+iloc(1:nloc))     !Dirichlet conditions
                  END WHERE
                END IF
             END IF
          END IF
       END DO
    END DO

    !default SGS BC, can be explicitely deifined below instead of calling default BCs
    CALL SGS_algebraic_BC (Lu, u, nlocal, ne_local, jlev, meth) 

    !Below BC for SGS module are defined
    IF(sgsmodel /= 0) THEN
       IF( sgsmodel_DSM <= sgsmodel  .AND. sgsmodel <= sgsmodel_LDKMG ) THEN
          ie = n_var_K
          shift=(n_var_K-1)*ng
          !--Go through all Boundary points that are specified
          i_p_face(0) = 1
          DO i=1,dim
             i_p_face(i) = i_p_face(i-1)*3
          END DO
          DO face_type = 0, 3**dim - 1
             face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
             IF( ABS(face(1)) == 1 ) THEN ! goes only through boundary points
                CALL get_all_indices_by_face (face_type, jlev, nloc, iloc)
                IF(nloc > 0) THEN 
                   IF( face(1) == -1  ) THEN                             ! Xmin face (entire face) 
                      Lu(shift+iloc(1:nloc)) = u(shift+iloc(1:nloc))     !Dirichlet conditions
                   END IF
                   IF( face(1) ==  1  ) THEN
                   WHERE ( COS(PI*x(iloc(1:nloc),2)) <= MAX_COS_INFLOW )
                      Lu(shift+iloc(1:nloc)) = u(shift+iloc(1:nloc))     !Dirichlet conditions
                   END WHERE
                   END IF
                END IF
             END IF
          END DO
       ELSE IF( sgsmodel == sgsmodel_KEPS .OR. sgsmodel == sgsmodel_KOME ) THEN

          ie = n_var_K
          shift=(n_var_K-1)*ng
          !--Go through all Boundary points that are specified
          i_p_face(0) = 1
          DO i=1,dim
             i_p_face(i) = i_p_face(i-1)*3
          END DO
          DO face_type = 0, 3**dim - 1
             face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
             IF( ABS(face(1)) == 1 ) THEN ! goes only through boundary points
                CALL get_all_indices_by_face (face_type, jlev, nloc, iloc)
                IF(nloc > 0) THEN 
                   IF( face(1) == -1  ) THEN                             ! Xmin face (entire face) 
                      Lu(shift+iloc(1:nloc)) = u(shift+iloc(1:nloc))     ! Dirichlet conditions
                   END IF
                   IF( face(1) ==  1  ) THEN
                   WHERE ( COS(PI*x(iloc(1:nloc),2)) <= MAX_COS_INFLOW )
                      Lu(shift+iloc(1:nloc)) = u(shift+iloc(1:nloc))     !Dirichlet conditions
!                      Lu(shift+iloc(1:nloc)) = du_comp(ie,iloc(1:nloc),1)     ! Neumann conditions
                   END WHERE
                   END IF
                END IF
             END IF
          END DO

          !  secondary variable: Kdiss/Komeg
          ie = n_var_KD
          shift=(n_var_KD-1)*ng
          !--Go through all Boundary points that are specified
          i_p_face(0) = 1
          DO i=1,dim
             i_p_face(i) = i_p_face(i-1)*3
          END DO
          DO face_type = 0, 3**dim - 1
             face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
             IF( ABS(face(1)) == 1 ) THEN ! goes only through boundary points
                CALL get_all_indices_by_face (face_type, jlev, nloc, iloc)
                IF(nloc > 0) THEN 
                   IF( face(1) == -1  ) THEN                             ! Xmin face (entire face) 
                      Lu(shift+iloc(1:nloc)) = u(shift+iloc(1:nloc))     ! Dirichlet conditions
                   END IF
                   IF( face(1) ==  1  ) THEN
                   WHERE ( COS(PI*x(iloc(1:nloc),2)) <= MAX_COS_INFLOW )
                      Lu(shift+iloc(1:nloc)) = u(shift+iloc(1:nloc))     !Dirichlet conditions
!                      Lu(shift+iloc(1:nloc)) = du_comp(ie,iloc(1:nloc),1)     ! Neumann conditions
                   END WHERE
                   END IF
                END IF
             END IF
          END DO

       END IF
    END IF

    IF ( do_Adp_Eps_Spatial_Evol ) &
    CALL VT_algebraic_BC (Lu, u, du, nlocal, ne_local, jlev, meth) 

  END SUBROUTINE user_algebraic_BC

  SUBROUTINE user_algebraic_BC_diag (Lu_diag, nlocal, ne_local, jlev, meth)
    !--Defines boundary condition type
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: jlev, meth, ne_local, nlocal
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (INOUT) :: Lu_diag
    REAL (pr), DIMENSION (nlocal,dim) :: du, d2u, du_comp

    INTEGER :: i, ie, ii, shift
    ! REAL (pr), DIMENSION (nlocal,dim) :: du, d2u  !uncomment if Neuman BC are used
    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc

    !BC for both 2D and 3D cases

    !if Neuman BC are used, need to call 
    CALL c_diff_diag ( du_comp, d2u, jlev, nlocal, meth, meth, 10, FORCE_RECTILINEAR=.TRUE.)
    CALL c_diff_diag ( du, d2u, jlev, nlocal, meth, meth, 10)

    DO ie = 1, ne_local
       shift = nlocal*(ie-1)
       !--Go through all Boundary points that are specified
       i_p_face(0) = 1
       DO i=1,dim
          i_p_face(i) = i_p_face(i-1)*3
       END DO
       DO face_type = 0, 3**dim - 1
          face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
          IF( ABS(face(1)) == 1 ) THEN ! goes only through boundary points
             CALL get_all_indices_by_face (face_type, jlev, nloc, iloc)
             IF(nloc > 0 ) THEN 
                IF( face(1) == -1  ) THEN                          ! Xmin face (entire face) 
                   Lu_diag(shift+iloc(1:nloc)) = 1.0_pr            ! Dirichlet conditions
                END IF
                IF( face(1) ==  1  ) THEN
                  WHERE ( COS(PI*x(iloc(1:nloc),2)) <= MAX_COS_INFLOW )
                   Lu_diag(shift+iloc(1:nloc)) = 1.0_pr            ! Dirichlet conditions
                  END WHERE
                END IF
             END IF
          END IF
       END DO
    END DO
    
    !default diagonal terms of SGS BC, can be explicitely deifined below instead of calling default BCs
    IF(sgsmodel /=0 ) CALL SGS_algebraic_BC_diag (Lu_diag, nlocal, ne_local, jlev, meth)

    !Below BC for SGS module are defined
    IF(sgsmodel /= 0) THEN
       IF( sgsmodel_DSM <= sgsmodel  .AND. sgsmodel <= sgsmodel_LDKMG ) THEN
          ie = n_var_K
          shift=(n_var_K-1)*ng
          !--Go through all Boundary points that are specified
          i_p_face(0) = 1
          DO i=1,dim
             i_p_face(i) = i_p_face(i-1)*3
          END DO
          DO face_type = 0, 3**dim - 1
             face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
             IF( ABS(face(1)) == 1 ) THEN ! goes only through boundary points
                CALL get_all_indices_by_face (face_type, jlev, nloc, iloc)
                IF(nloc > 0 ) THEN 
                   IF( face(1) == -1  ) THEN                          ! Xmin face (entire face) 
                      Lu_diag(shift+iloc(1:nloc)) = 1.0_pr            ! Dirichlet conditions
                   END IF
                   IF( face(1) ==  1  ) THEN
                   WHERE ( COS(PI*x(iloc(1:nloc),2)) <= MAX_COS_INFLOW )
                      Lu_diag(shift+iloc(1:nloc)) = 1.0_pr            ! Dirichlet conditions
                   END WHERE
                   END IF
                END IF
             END IF
          END DO
       ELSE IF( sgsmodel == sgsmodel_KEPS .OR. sgsmodel == sgsmodel_KOME ) THEN

          ie = n_var_K
          shift=(n_var_K-1)*ng
          !--Go through all Boundary points that are specified
          i_p_face(0) = 1
          DO i=1,dim
             i_p_face(i) = i_p_face(i-1)*3
          END DO
          DO face_type = 0, 3**dim - 1
             face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
             IF( ABS(face(1)) == 1 ) THEN ! goes only through boundary points
                CALL get_all_indices_by_face (face_type, jlev, nloc, iloc)
                IF(nloc > 0 ) THEN 
                   IF( face(1) == -1  ) THEN                          ! Xmin face (entire face) 
                      Lu_diag(shift+iloc(1:nloc)) = 1.0_pr            ! Dirichlet conditions
                   END IF
                   IF( face(1) ==  1  ) THEN
                   WHERE ( COS(PI*x(iloc(1:nloc),2)) <= MAX_COS_INFLOW )
                      Lu_diag(shift+iloc(1:nloc)) = 1.0_pr            ! Dirichlet conditions
!                      Lu_diag(shift+iloc(1:nloc)) = du_comp(iloc(1:nloc),1)! Neumann conditions
                   END WHERE
                   END IF
                END IF
             END IF
          END DO

          !  secondary variable: Kdiss/Komeg
          ie = n_var_KD
          shift=(n_var_KD-1)*ng
          !--Go through all Boundary points that are specified
          i_p_face(0) = 1
          DO i=1,dim
             i_p_face(i) = i_p_face(i-1)*3
          END DO
          DO face_type = 0, 3**dim - 1
             face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
             IF( ABS(face(1)) == 1 ) THEN ! goes only through boundary points
                CALL get_all_indices_by_face (face_type, jlev, nloc, iloc)
                IF(nloc > 0 ) THEN 
                   IF( face(1) == -1  ) THEN                          ! Xmin face (entire face) 
                      Lu_diag(shift+iloc(1:nloc)) = 1.0_pr            ! Dirichlet conditions
                   END IF
                   IF( face(1) ==  1  ) THEN
                   WHERE ( COS(PI*x(iloc(1:nloc),2)) <= MAX_COS_INFLOW )
                      Lu_diag(shift+iloc(1:nloc)) = 1.0_pr            ! Dirichlet conditions
!                      Lu_diag(shift+iloc(1:nloc)) = du_comp(iloc(1:nloc),1)! Neumann conditions
                   END WHERE
                   END IF
                END IF
             END IF
          END DO

       END IF
    END IF

    IF ( do_Adp_Eps_Spatial_Evol ) THEN
       CALL VT_algebraic_BC_diag (Lu_diag, du, nlocal, ne_local, jlev, meth) 
    END IF

  END SUBROUTINE user_algebraic_BC_diag

  SUBROUTINE user_algebraic_BC_rhs (rhs, ne_local, nlocal, jlev)
    !--Sets rhs for boundary conditions
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: ne_local, nlocal, jlev
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (INOUT) :: rhs
    INTEGER :: i, ie, ii, shift
    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc

    !BC for both 2D and 3D cases

    DO ie = 1, ne_local
       shift = nlocal*(ie-1)
       !--Go through all Boundary points that are specified
       i_p_face(0) = 1
       DO i=1,dim
          i_p_face(i) = i_p_face(i-1)*3
       END DO
       DO face_type = 0, 3**dim - 1
          face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
          IF( ABS(face(1)) == 1 ) THEN ! goes only through boundary points
             CALL get_all_indices_by_face (face_type, jlev, nloc, iloc)
             IF(nloc > 0 ) THEN 
                IF( face(1) == -1  ) THEN                          ! Xmin face (entire face) 
                      rhs(shift+iloc(1:nloc)) = 0.0_pr     !! only when Umn /= 0 
                END IF
                IF( face(1) ==  1  ) THEN                          ! Xmin face (entire face) 
                   IF ( ie == n_var_vel(1) ) THEN
                      WHERE ( COS(PI*x(iloc(1:nloc),2)) <= MAX_COS_INFLOW )
                         rhs(shift+iloc(1:nloc)) = 1.0_pr
                      END WHERE
                   ELSEIF ( ie == n_var_vel(2) ) THEN
                      WHERE ( COS(PI*x(iloc(1:nloc),2)) <= MAX_COS_INFLOW )
                         rhs(shift+iloc(1:nloc)) = 0.0_pr
                      END WHERE
                   END IF
                END IF
             END IF
          END IF
       END DO
    END DO

    !default  SGS BC, can be explicitely deifined below instead of calling default BCs
    IF(sgsmodel /= 0) CALL SGS_algebraic_BC_rhs (rhs, ne_local, nlocal, jlev)

    !Below BC for SGS module are defined
    IF(sgsmodel /= 0) THEN
       IF( sgsmodel_DSM <= sgsmodel  .AND. sgsmodel <= sgsmodel_LDKMG ) THEN
          ie = n_var_K
          shift=(n_var_K-1)*ng
          !--Go through all Boundary points that are specified
          i_p_face(0) = 1
          DO i=1,dim
             i_p_face(i) = i_p_face(i-1)*3
          END DO
          DO face_type = 0, 3**dim - 1
             face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
             IF( ABS(face(1)) == 1 ) THEN ! goes only through boundary points
                CALL get_all_indices_by_face (face_type, jlev, nloc, iloc)
                IF(nloc > 0 ) THEN 
                   IF( face(1) == -1  ) THEN                          ! Xmin face (entire face) 
                      rhs(shift+iloc(1:nloc)) = 0.0_pr                ! constant level imposed by input
                   END IF
                   IF( face(1) ==  1  ) THEN                          ! Xmin face (entire face) 
                   WHERE ( COS(PI*x(iloc(1:nloc),2)) <= MAX_COS_INFLOW )
                      rhs(shift+iloc(1:nloc)) = Ksgs_inflow           ! constant level imposed by input
                   END WHERE
                   END IF
                END IF
             END IF
          END DO
       ELSE IF( sgsmodel == sgsmodel_KEPS .OR. sgsmodel == sgsmodel_KOME ) THEN

          ie = n_var_K
          shift=(n_var_K-1)*ng
          !--Go through all Boundary points that are specified
          i_p_face(0) = 1
          DO i=1,dim
             i_p_face(i) = i_p_face(i-1)*3
          END DO
          DO face_type = 0, 3**dim - 1
             face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
             IF( ABS(face(1)) == 1 ) THEN ! goes only through boundary points
                CALL get_all_indices_by_face (face_type, jlev, nloc, iloc)
                IF(nloc > 0 ) THEN 
                   IF( face(1) == -1  ) THEN                          ! Xmin face (entire face) 
                      rhs(shift+iloc(1:nloc)) = 0.0_pr           ! constant level imposed by input
                   END IF
                   IF( face(1) ==  1  ) THEN                          ! Xmin face (entire face) 
                   WHERE ( COS(PI*x(iloc(1:nloc),2)) <= MAX_COS_INFLOW )
                      rhs(shift+iloc(1:nloc)) = Ksgs_inflow           ! constant level imposed by input
                   END WHERE
                   END IF
                END IF
             END IF
          END DO

          ie = n_var_KD
          shift=(n_var_KD-1)*ng
          !--Go through all Boundary points that are specified
          i_p_face(0) = 1
          DO i=1,dim
             i_p_face(i) = i_p_face(i-1)*3
          END DO
          DO face_type = 0, 3**dim - 1
             face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
             IF( ABS(face(1)) == 1 ) THEN ! goes only through boundary points
                CALL get_all_indices_by_face (face_type, jlev, nloc, iloc)
                IF(nloc > 0 ) THEN 
                   IF( face(1) == -1  ) THEN                          ! Xmin face (entire face) 
                      IF(sgsmodel == sgsmodel_KEPS) THEN
                         rhs(shift+iloc(1:nloc)) = 0.0_pr           ! constant level imposed by input
                      ELSE IF(sgsmodel == sgsmodel_KOME) THEN
                         rhs(shift+iloc(1:nloc)) = 800.0_pr*nu/delta_n**2           ! constant level imposed by input
                      END IF
                   END IF
                   IF( face(1) ==  1  ) THEN                          ! Xmin face (entire face) 
                   WHERE ( COS(PI*x(iloc(1:nloc),2)) <= MAX_COS_INFLOW )
                      rhs(shift+iloc(1:nloc)) = Kdiss_inflow           ! constant level imposed by input
                   END WHERE
                   END IF
                END IF
             END IF
          END DO
       END IF
    END IF

    IF ( do_Adp_Eps_Spatial_Evol ) THEN
       CALL VT_algebraic_BC_rhs(rhs, ne_local, nlocal, jlev) 
    END IF
  
  END SUBROUTINE user_algebraic_BC_rhs

  SUBROUTINE user_project (u, p, nlocal, meth)
    USE variable_mapping 
    !--Makes u divergence free
    IMPLICIT NONE
    INTEGER, PARAMETER :: ne_local = 1
    INTEGER, INTENT (IN) :: meth, nlocal
    INTEGER :: i
    INTEGER :: meth_p, meth_central, meth_backward, meth_forward 
    REAL (pr), DIMENSION (nlocal),     INTENT(INOUT) :: p
    REAL (pr), DIMENSION (nlocal,n_integrated), INTENT(INOUT) :: u
    REAL (pr), DIMENSION (nlocal,ne_local) :: dp, dp0
    REAL (pr), DIMENSION (nlocal,ne_local) :: f, f0
    REAL (pr), DIMENSION(ne_local) :: scl_p
    REAL (pr), DIMENSION(dim) :: du, dS
    REAL (pr) :: tmp1, tmp2           
    INTEGER, DIMENSION(ne_local) :: clip 
    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc

    meth_central  = meth + BIASING_NONE
    meth_backward = meth + BIASING_BACKWARD
    meth_forward  = meth + BIASING_FORWARD
    IF(pressure_high_order) THEN
       meth_p = meth
    ELSE
       meth_p = LOW_ORDER
    END IF

    !zero flux correction to ensure no mass accumulation inside of the domain
    IF(zero_flux_correction) THEN
       !--Boundary points
       i_p_face(0) = 1
       DO i=1,dim
          i_p_face(i) = i_p_face(i-1)*3
       END DO
       du=0.0_pr
       dS=0.0_pr
       DO face_type = 0, 3**dim - 1
          face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
          IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
             CALL get_all_indices_by_face (face_type, j_lev, nloc, iloc)
             IF(nloc > 0 ) THEN ! ANY dimension
                DO i = 1, dim !net flux du and net bundary volume (does not matter, since resolution is set to j_zn
                   du(i) =  du(i) + face(i)*SUM(u(iloc(1:nloc),i)*dA(iloc(1:nloc)))
                   dS(i) =  dS(i) + ABS(face(i))*SUM(dA(iloc(1:nloc)))
                END DO
             END IF
          END IF
       END DO
       DO i = 1, dim !substracting velocity correction
          IF(prd(i) == 0) THEN
             tmp1 = du(i)
             tmp2 = dS(i)
             CALL parallel_global_sum( REAL=tmp1 )
             CALL parallel_global_sum( REAL=tmp2 )
             u(:,i) = u(:,i)-tmp1/tmp2
          END IF
       END DO
   END IF

    !--Make u  divergence free
    dp = 0.0_pr 
    dp0 = 0.0_pr
    f = Laplace_rhs (u(:,1:dim), nlocal, dim, meth_p)
    clip = MIN(1,BCtype,BCclip) ! if no part of the boundary explicetely set up as Dirichlet, then clip = 0

    scl_p = MAXVAL(scl_global(1:dim)) !scale of pressure increment based on dynamic pressure
    IF(par_rank.EQ.0) PRINT *, 'scl_p=',scl_p

    IF(two_step_pressure) THEN
       IF (par_rank.EQ.0) PRINT *,'-------- user_project - TWO-STEP PRESSURE: pressure_BC_type = 0 --------'
       pressure_BC_type = 0
       BCtype = 2 
       clip = MIN(1,BCtype,BCclip) ! if no part of the boundary explicetely set up as Diricket, then clip = 0

       CALL Linsolve (dp0, f , tol2, nlocal, ne_local, clip, Laplace, Laplace_diag, SCL=scl_p) !new with scaling 
       IF (par_rank.EQ.0) PRINT *,'-------- user_project - TWO-STEP PRESSURE: pressure_BC_type = 1 --------'
       pressure_BC_type = 1
       BCtype = 1
       clip = MIN(1,BCtype,BCclip) ! if no part of the boundary explicetely set up as Diricket, then clip = 0
       f = f - RESHAPE(Laplace (j_lev, dp0, nlocal, ne_local, meth_p), (/nlocal, ne_local/) )

       CALL Linsolve (dp, f , tol2, nlocal, ne_local, clip, Laplace, Laplace_diag, SCL=scl_p) !new with scaling 
       dp = dp + dp0
    ELSE
       IF (par_rank.EQ.0) PRINT *,'-------- user_project - ONE-STEP PRESSURE: pressure_BC_type = 0 --------'
       pressure_BC_type = 1
       clip = MIN(1,BCtype,BCclip) ! if no part of the boundary explicetely set up as Diricket, then clip = 0
       IF(.TRUE.) THEN
          d_dtau = 0.0_pr
          dp = 0.0_pr
          CALL Linsolve (dp, f, tol2, nlocal, ne_local, clip, Laplace, Laplace_diag, SCL=scl_p) !new with scaling 
       ELSE
          d0_dtau = 1.0_pr/dt
          dp  = 0.0_pr
          dp0 = 0.0_pr
          i = 0
          DO WHILE( ALL(MAXVAL(ABS(f)) > scl_p*tol2) .AND. d0_dtau > MAXVAL(scl_p)*tol2 )
             i = i + 1
             dp0 = 0.0_pr
             !    CALL Linsolve (dp, f , tol2, nlocal, ne_local, clip, Laplace, Laplace_diag)  !old without scaling
             dp = dp + dp0
             d_dtau = 0.0_pr
             f = f - RESHAPE(Laplace (j_lev, dp0, nwlt, ne_local, meth_p), (/nwlt, ne_local/) )
             WRITE(*,'("External loop for LINSOLVE, i=",I4," d_dtau=",E12.5," |f|=",E15.8)'), i, d0_dtau,  MAXVAL(ABS(f))
             d0_dtau = 0.8_pr*d0_dtau
          END DO
          PRINT *, 'External loop for LINSOLVE is finished'
          PAUSE
       END IF
    END IF
    

    IF(div_grad) THEN
       p = p + dp(:,1)/dt - 0.5_pr*nu*div(u(:,1:dim),nlocal,j_lev, meth_p+BIASING_FORWARD) 
       u(:,1:dim) = u(:,1:dim) - grad(dp, nlocal, j_lev, meth_p+BIASING_BACKWARD)
    ELSE   !! second derivative also
       p = p + dp(:,1)/dt - 0.5_pr*nu*div(u(:,1:dim),nlocal,j_lev, meth_p+BIASING_NONE) 
       u(:,1:dim) = u(:,1:dim) - grad(dp, nlocal, j_lev, meth_p+BIASING_NONE)
    END IF

    IF (set_to_zero_if_diverged .AND. MAXVAL(ABS(dp(:,1))) == 0.0_pr ) p = 0.0_pr

  END SUBROUTINE user_project

  FUNCTION Laplace (jlev, u, nlocal, ne_local, meth_in)
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: jlev, nlocal, ne_local, meth_in
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (INOUT) :: u
    REAL (pr), DIMENSION (nlocal*ne_local) :: Laplace

    INTEGER :: i, ii, ie, ie1, idim, shift
    INTEGER :: meth, meth_central, meth_backward, meth_forward 
    REAL (pr), DIMENSION (ne_local, nlocal, dim) :: du, d2u, du_comp, d2u_comp
    REAL (pr), DIMENSION (ne_local*dim, nlocal, dim)      :: d2u_divgrad, du_dummy
    REAL (pr), DIMENSION (nlocal, dim) :: normal
    INTEGER :: face_type, nloc, j, wlt_type, j_df

    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc, iloc_lev

    IF(pressure_high_order) THEN
       meth = meth_in
    ELSE
       meth = LOW_ORDER
    END IF
    meth_central  = meth + BIASING_NONE
    meth_backward = meth + BIASING_BACKWARD
    meth_forward  = meth + BIASING_FORWARD

    i_p_face(0) = 1
    DO i=1,dim
       i_p_face(i) = i_p_face(i-1)*3
    END DO

    !
    ! Find 1st deriviative of u. 
    !

    IF(ne_local > dim) THEN
       WRITE(*,'("USER_CASE ERROR - Laplace: ne_local > dim")')
       STOP
    END IF

    IF(div_grad) THEN
       CALL c_diff_fast (u, du, d2u, jlev, nlocal, meth_backward, 10, ne_local, 1, ne_local)
!====================================================================================
       normal = 0.0_pr
       IF(BCtype == 1) THEN ! setting dp/dn = 0 on the boundary
          DO ie = 1, ne_local
             shift=(ie-1)*nlocal
             !--Boundary points
             DO face_type = 0, 3**dim - 1
                face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
                IF( ABS(face(1)) == 1 ) THEN ! goes only through boundary points
                   DO j = 1, jlev
                   DO wlt_type = MIN(j-1,1),2**dim-1
                   DO j_df = j, j_lev
                      nloc = indx_DB(j_df,wlt_type,face_type,j)%length
                      IF(nloc > 0 ) THEN ! ANY dimension
                         iloc_lev(1:nloc) = indx_DB(j_df,wlt_type,face_type,j)%p(1:nloc)%i !index on j_lev mesh
                         iloc(1:nloc) = iloc_lev(1:nloc) + indx_DB(jlev,wlt_type,face_type,j)%shift  !index on jlev mesh used by multilevel solver
                         normal(iloc(1:nloc),1) =  COS(PI*x(iloc_lev(1:nloc),2))
                         normal(iloc(1:nloc),2) =  SIN(PI*x(iloc_lev(1:nloc),2))
                         DO idim = 1, dim
                            du(ie,iloc(1:nloc),idim) = du(ie,iloc(1:nloc),idim) - SUM(du(ie,iloc(1:nloc),1:dim)*normal(iloc(1:nloc),1:dim),DIM=2)*normal(iloc(1:nloc),idim)
                         END DO
                      END IF
                   END DO
                   END DO
                   END DO
                END IF
             END DO
          END DO
       END IF
!====================================================================================
       CALL c_diff_fast(du, d2u_divgrad, du_dummy, jlev, nlocal, meth_forward, 10, ne_local*dim, 1, ne_local*dim )
    ELSE   !! second derivative also
       CALL c_diff_fast (u, du, d2u, jlev, nlocal, meth_central, 11, ne_local, 1, ne_local)
    END IF

    DO ie = 1, ne_local
       shift=(ie-1)*nlocal
       IF(div_grad) THEN
          idim = 1
          Laplace(shift+1:shift+Nwlt_lev(jlev,0)) = -d_dtau*u(shift+1:shift+Nwlt_lev(jlev,0)) + d2u_divgrad( (ie-1)*dim+idim ,1:Nwlt_lev(jlev,0),1)
          DO idim = 2,dim
             Laplace(shift+1:shift+Nwlt_lev(jlev,0)) = Laplace(shift+1:shift+Nwlt_lev(jlev,0)) + &
                  d2u_divgrad((ie-1)*dim+idim ,1:Nwlt_lev(jlev,0),idim)
          END DO
       ELSE
          !--- grad^2
          idim = 1
          Laplace(shift+1:shift+Nwlt_lev(jlev,0)) = -d_dtau*u(shift+1:shift+Nwlt_lev(jlev,0)) + d2u( ie ,1:Nwlt_lev(jlev,0),idim)
          DO idim = 2,dim
             Laplace(shift+1:shift+Nwlt_lev(jlev,0)) = Laplace(shift+1:shift+Nwlt_lev(jlev,0)) + &
                  d2u(ie ,1:Nwlt_lev(jlev,0),idim)
          END DO
       END IF
    END DO

    IF(BCtype > 0) CALL c_diff_fast (u, du_comp, d2u_comp, jlev, nlocal, meth_backward, 11, ne_local, 1, ne_local, FORCE_RECTILINEAR=.TRUE.)

    DO ie = 1, ne_local
       shift=(ie-1)*nlocal
       !--Boundary points
       DO face_type = 0, 3**dim - 1
          face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
          IF( ABS(face(1)) == 1 ) THEN ! goes only through boundary points
             DO j = 1, jlev
             DO wlt_type = MIN(j-1,1),2**dim-1
             DO j_df = j, j_lev
                nloc = indx_DB(j_df,wlt_type,face_type,j)%length
                IF(nloc > 0 ) THEN ! ANY dimension
                   iloc_lev(1:nloc) = indx_DB(j_df,wlt_type,face_type,j)%p(1:nloc)%i !index on j_lev mesh
                   iloc(1:nloc) = iloc_lev(1:nloc) + indx_DB(jlev,wlt_type,face_type,j)%shift  !index on jlev mesh used by multilevel solver
                   IF( ABS(face(1)) == 1 ) THEN
                      IF(BCtype == 0) THEN
                         Laplace(shift+iloc(1:nloc)) = u(shift+iloc(1:nloc))
                      ELSE IF(BCtype ==1) THEN
!!$                         Laplace(shift+iloc(1:nloc)) = -face(1)*du_comp(ie,iloc(1:nloc),1)
                         Laplace(shift+iloc(1:nloc)) = -face(1)*(COS(PI*x(iloc_lev(1:nloc),2))*du(ie,iloc(1:nloc),1)+SIN(PI*x(iloc_lev(1:nloc),2))*du(ie,iloc(1:nloc),2))
                      ELSE IF(BCtype ==2) THEN
                         Laplace(shift+iloc(1:nloc)) = SUM(d2u_comp(ie,iloc(1:nloc),2:dim),DIM=2)
                      END IF
                   END IF
!!$                   IF( face(1) == 1  ) THEN                                
!!$                      WHERE ( COS(PI*x(iloc_lev(1:nloc),2)) < -0.0_pr )
!!$                         Laplace(shift+iloc(1:nloc)) = u(shift+iloc(1:nloc))  !Dirichlet conditions
!!$                      END WHERE
!!$                   END IF
!!$                   IF(pressure_BC_type == 0 .AND. ABS(face(1)) == 1 ) THEN ! left and right face
!!$                      WHERE ( -COS(PI*x(iloc_lev(1:nloc),2)) > MAX_COS_INFLOW )
!!$                      Laplace(shift+iloc(1:nloc)) = u(shift+iloc(1:nloc))  !Dirichlet conditions
!!$                     END WHERE
!!$                   END IF
                END IF
             END DO
             END DO
             END DO
          END IF
       END DO
    END DO
  END FUNCTION Laplace

  FUNCTION Laplace_diag (jlev,  nlocal, ne_local, meth_in)
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: jlev, nlocal, ne_local, meth_in
    REAL (pr), DIMENSION (nlocal*ne_local) :: Laplace_diag

    INTEGER :: i, ii, ie, ie1, idim, shift
    INTEGER :: meth, meth_central, meth_backward, meth_forward 
    REAL (pr), DIMENSION (nlocal,dim) :: dub, duf, du, d2u, du_comp, d2u_comp
    INTEGER :: face_type, nloc, j, wlt_type, j_df
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc, iloc_lev

    IF(pressure_high_order) THEN
       meth = meth_in
    ELSE
       meth = LOW_ORDER
    END IF
    meth_central  = meth + BIASING_NONE
    meth_backward = meth + BIASING_BACKWARD
    meth_forward  = meth + BIASING_FORWARD
    i_p_face(0) = 1
    DO i=1,dim
       i_p_face(i) = i_p_face(i-1)*3
    END DO

    IF(div_grad) THEN
       CALL c_diff_diag ( duf, d2u, jlev, nlocal, meth_forward, meth_backward, 10)
       CALL c_diff_diag ( dub, d2u, jlev, nlocal, meth_backward, meth_forward, -11)
    ELSE
       CALL c_diff_diag ( du, d2u, jlev, nlocal, meth_central, meth_central,  11)
    END IF

    DO ie = 1, ne_local
       shift=(ie-1)*nlocal
       !--- div(grad)
       !PRINT *,'CAlling c_diff_diag from Laplace_diag() '
       !PRINT *,'--- jlev, nlocal, meth1, meth2', jlev, nlocal, meth1, meth2

       idim = 1
       Laplace_diag(shift+1:shift+Nwlt_lev(jlev,0)) = -d_dtau + d2u(1:Nwlt_lev(jlev,0),idim)
       DO idim = 2,dim
          Laplace_diag(shift+1:shift+Nwlt_lev(jlev,0)) = Laplace_diag(shift+1:shift+Nwlt_lev(jlev,0)) + &
               d2u(1:Nwlt_lev(jlev,0),idim)
       END DO
    END DO
    IF(BCtype > 0) CALL c_diff_diag ( du_comp, d2u_comp, jlev, nlocal, meth_backward, meth_forward, -11, FORCE_RECTILINEAR=.TRUE.)
    DO ie = 1, ne_local
       shift=(ie-1)*nlocal
       !--Boundary points
       DO face_type = 0, 3**dim - 1
          face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
          IF( ABS(face(1)) == 1 ) THEN ! goes only through boundary points
             DO j = 1, jlev
             DO wlt_type = MIN(j-1,1),2**dim-1
             DO j_df = j, j_lev
                nloc = indx_DB(j_df,wlt_type,face_type,j)%length
                IF(nloc > 0 ) THEN ! ANY dimension
                   iloc_lev(1:nloc) = indx_DB(j_df,wlt_type,face_type,j)%p(1:nloc)%i !index on j_lev mesh
                   iloc(1:nloc) = iloc_lev(1:nloc) + indx_DB(jlev,wlt_type,face_type,j)%shift  !index on jlev mesh used by multilevel solver
                   IF( ABS(face(1)) == 1 ) THEN
                      IF(BCtype == 0) THEN
                         Laplace_diag(shift+iloc(1:nloc)) = 1.0_pr
                      ELSE IF(BCtype ==1) THEN
!!$                        PRINT *, 'LAPLACE_DIAG X_BC:', face, x(iloc_lev(1:nloc),1)
!!$                         Laplace_diag(shift+iloc(1:nloc)) = -face(1)*du_comp(iloc(1:nloc),1)
                         Laplace_diag(shift+iloc(1:nloc)) = -face(1)*(COS(PI*x(iloc_lev(1:nloc),2))*dub(iloc(1:nloc),1)+SIN(PI*x(iloc_lev(1:nloc),2))*dub(iloc(1:nloc),2))
                      ELSE IF(BCtype ==2) THEN
                         Laplace_diag(shift+iloc(1:nloc)) = SUM(d2u_comp(iloc(1:nloc),2:dim),DIM=2)
                      END IF
                   END IF
!!$                   IF( face(1) == 1  ) THEN                                
!!$                      WHERE ( COS(PI*x(iloc_lev(1:nloc),2)) < -0.0_pr )
!!$                         Laplace_diag(shift+iloc(1:nloc)) = 1.0_pr  !Dirichlet conditions
!!$                      END WHERE
!!$                   END IF
!!$                   IF(pressure_BC_type == 0 .AND. ABS(face(1)) == 1 ) THEN ! left and right face
!!$                      WHERE ( -COS(PI*x(iloc_lev(1:nloc),2)) > MAX_COS_INFLOW )
!!$                      Laplace_diag(shift+iloc(1:nloc)) = 1.0_pr  !Dirichlet conditions
!!$                     END WHERE
!!$                   END IF
                END IF
             END DO
             END DO
             END DO
          END IF
       END DO
    END DO

!!$    PRINT *, 'Laplace_diag internal:', MINVAL(ABS(Laplace_diag(1:Nwlt_lev(jlev,0)))), MAXVAL(ABS(Laplace_diag(1:Nwlt_lev(jlev,0))))
!!$    PRINT *, 'Laplace_diag all:     ', MINVAL(ABS(Laplace_diag)), MAXVAL(ABS(Laplace_diag))

  END FUNCTION Laplace_diag

  FUNCTION Laplace_rhs(u, nlocal, ne_local, meth_in)
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: nlocal, ne_local, meth_in
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (INOUT) :: u
    REAL (pr), DIMENSION (nlocal,ne_local) :: Laplace_rhs

    INTEGER :: i, ii, ie, idim
    INTEGER :: meth, meth_central, meth_backward, meth_forward 
    REAL (pr), DIMENSION (ne_local ,nlocal,dim) :: du
    REAL (pr), DIMENSION (ne_local*dim ,nlocal,dim) :: d2u
    REAL (pr), DIMENSION (ne_local*dim ,nlocal,dim) :: du_dummy ! passed when only calculating 1st derivative.
    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc

    IF(pressure_high_order) THEN
       meth = meth_in
    ELSE
       meth = LOW_ORDER
    END IF
    meth_central  = meth + BIASING_NONE
    meth_backward = meth + BIASING_BACKWARD
    meth_forward  = meth + BIASING_FORWARD

    IF(div_grad) THEN
       Laplace_rhs(:,1) = div(u,nlocal,j_lev,meth_forward)
    ELSE
       Laplace_rhs(:,1) = div(u,nlocal,j_lev,meth_central)
    END IF

    !--Boundary points
    i_p_face(0) = 1
    DO i=1,dim
       i_p_face(i) = i_p_face(i-1)*3
    END DO
    DO face_type = 0, 3**dim - 1
       face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
       IF( ABS(face(1)) == 1) THEN ! goes only through boundary points
          CALL get_all_indices_by_face (face_type, j_lev, nloc, iloc)
          IF(nloc > 0 ) THEN 
                Laplace_rhs(iloc(1:nloc),1) = 0.0_pr !all conditions
          END IF
       END IF
    END DO
    
  END FUNCTION Laplace_rhs


  FUNCTION user_rhs (u_integrated,p)
    USE curvilinear
    USE penalization
!    USE parallel                                       !!! by Giuliano
    USE curvilinear
    USE elliptic_vars                                  !!! by Giuliano
    IMPLICIT NONE
    REAL (pr), DIMENSION (ng,ne), INTENT(IN) :: u_integrated !1D flat version of integrated variables without BC 
    REAL (pr), DIMENSION (ng), INTENT(IN) :: p
    REAL (pr), DIMENSION (n) :: user_rhs

    INTEGER :: ie, ie1, shift, i
    INTEGER, PARAMETER :: meth=1
    INTEGER :: meth_p
    REAL (pr), DIMENSION (ne,ng,dim) :: du, d2u
    REAL (pr), DIMENSION (ng,dim)     :: dp
    REAL (pr), DIMENSION(dim) :: Uav                    !!! by Giuliano
    REAL (pr) :: tmp1                                   !!! by Giuliano
    INTEGER :: face_type, nloc, meth_local
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc
    INTEGER :: meth_central, meth_backward, meth_forward 
  
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! added by Giuliano on 30 sept 2010 to be checked
    IF( Zero_Mean ) THEN
       DO ie = 1, dim
          tmp1 = SUM(u_integrated(:,ie)*dA)
          CALL parallel_global_sum( REAL=tmp1 )
          Uav(ie) = tmp1/sumdA_global
       END DO
    ELSE
       Uav =0.0_pr
    END IF
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    meth_central  = meth + BIASING_NONE
    meth_backward = meth + BIASING_BACKWARD
    meth_forward  = meth + BIASING_FORWARD
    IF(pressure_high_order) THEN
       meth_p = meth
    ELSE
       meth_p = LOW_ORDER
    END IF

   !PRINT *,'user_rhs MINMAX(p) ', MINVAL( p), MAXVAL(dp)
    IF(div_grad) THEN
       dp(1:ng,1:dim) = grad (p, ng, j_lev, meth_p+BIASING_BACKWARD)
    ELSE
       dp(1:ng,1:dim) = grad (p, ng, j_lev, meth_p+BIASING_NONE)
    END IF

    !PRINT *,'user_rhs MINMAX(dp) ', MINVAL( dp), MAXVAL(dp)
    !PRINT *,'user_rhs MINMAX(u_integrated) ', MINVAL( u_integrated), MAXVAL(u_integrated)

    CALL c_diff_fast(u_integrated, du, d2u, j_lev, ng, 1, 11, ne, 1, ne) !derivatives are calculated even for SGS terms

    !PRINT *,'user_rhs MINMAX(du) ', MINVAL( du), MAXVAL(du)

    !--Form right hand side of Navier-Stokes equations
    DO ie = 1, dim
       shift=(ie-1)*ng
!       user_rhs(shift+1:shift+ng) = nu*SUM(d2u(ie,:,:),2) - dp(:,ie) 
       user_rhs(shift+1:shift+ng) = nu*SUM(d2u(ie,:,:),2) - dp(:,ie) - grad_P(ie)      !!! by Giuliano
       DO ie1 = 1,dim
!          user_rhs(shift+1:shift+ng) = user_rhs(shift+1:shift+ng) - (u_integrated(:,ie1)+Umn(ie1))*du(ie,:,ie1) 
          user_rhs(shift+1:shift+ng) = user_rhs(shift+1:shift+ng) - (u_integrated(:,ie1)-Uav(ie1)+Umn(ie1))*du(ie,:,ie1)   !!!by Giuliano
       END DO
    END DO

    IF(sgsmodel /= 0) CALL SGS_rhs (user_rhs, u_integrated, du, d2u)

    !--Add convection by mean-velocity at right hand side of k-equation (Giuliano 2012)
    IF( sgsmodel_DSM <= sgsmodel  .AND. sgsmodel <= sgsmodel_LDKMG ) THEN
       ie = n_var_K
       shift=(n_var_K-1)*ng
       DO ie1 = 1,dim
          user_rhs(shift+1:shift+ng) = user_rhs(shift+1:shift+ng) - (-Uav(ie1)+Umn(ie1))*du(ie,:,ie1)
       END DO
    ELSE IF( sgsmodel == sgsmodel_KEPS .OR. sgsmodel == sgsmodel_KOME ) THEN
       ie = n_var_K
       shift=(n_var_K-1)*ng
       DO ie1 = 1,dim
          user_rhs(shift+1:shift+ng) = user_rhs(shift+1:shift+ng) - (-Uav(ie1)+Umn(ie1))*du(ie,:,ie1)
       END DO
       ie = n_var_KD
       shift=(n_var_KD-1)*ng
       DO ie1 = 1,dim
          user_rhs(shift+1:shift+ng) = user_rhs(shift+1:shift+ng) - (-Uav(ie1)+Umn(ie1))*du(ie,:,ie1)
       END DO
    END IF

    IF(imask_obstacle) THEN
       DO ie = 1, dim
          shift=(ie-1)*ng
!          user_rhs(shift+1:shift+ng) = user_rhs(shift+1:shift+ng) - penal/eta_chi*(u_integrated(:,ie)+Umn(ie))
          user_rhs(shift+1:shift+ng) = user_rhs(shift+1:shift+ng) - penal/eta_chi*(u_integrated(:,ie)-Uav(ie)+Umn(ie))     !!!by Giuliano
       END DO
       IF(sgsmodel /= 0) THEN
         IF( sgsmodel_DSM <= sgsmodel  .AND. sgsmodel <= sgsmodel_LDKMG ) THEN
            ie = n_var_K
            shift=(n_var_K-1)*ng
            user_rhs(shift+1:shift+ng) = user_rhs(shift+1:shift+ng) - 2.0_pr*penal/eta_chi*u_integrated(:,ie) !penalizing 2k_sgs = 0 (2012)
         ELSE IF( sgsmodel == sgsmodel_KEPS ) THEN
            ie = n_var_K
            shift=(n_var_K-1)*ng
            user_rhs(shift+1:shift+ng) = user_rhs(shift+1:shift+ng) - penal/eta_chi*u_integrated(:,ie)  !penalizing k_sgs = 0 (2015)
            ie = n_var_KD
            shift=(n_var_KD-1)*ng
            user_rhs(shift+1:shift+ng) = user_rhs(shift+1:shift+ng) - penal/eta_chi*u_integrated(:,ie)  !penalizing k_diss = 0 (2015)
         ELSE IF( sgsmodel == sgsmodel_KOME ) THEN
            ie = n_var_K
            shift=(n_var_K-1)*ng
            user_rhs(shift+1:shift+ng) = user_rhs(shift+1:shift+ng) - penal/eta_chi*u_integrated(:,ie)  !penalizing k_sgs = 0 (2015)
            ie = n_var_KD
            shift=(n_var_KD-1)*ng
            user_rhs(shift+1:shift+ng) = user_rhs(shift+1:shift+ng) - penal/eta_chi*(u_integrated(:,ie)-800.0_pr*nu/delta_n**2)  !penalizing Komeg = Komeg_wall (2015)
         END IF
       END IF
    END IF

    IF(sponge_active) THEN
       DO ie = 2, dim !dump non-streamwise velocity components
          shift=(ie-1)*ng
!          user_rhs(shift+1:shift+ng) = user_rhs(shift+1:shift+ng) - chi_sponge/eta_sponge(ie)*u_integrated(:,ie)
          user_rhs(shift+1:shift+ng) = user_rhs(shift+1:shift+ng) - chi_sponge/eta_sponge(ie)*(u_integrated(:,ie)-Uav(ie))     !!!by Giuliano
       END DO
    END IF

    IF( do_Adp_Eps_Spatial_Evol ) THEN
       CALL user_rhs__VT (user_rhs, u_integrated, du, d2u) ! Cf: OPTIONAL
    !--Add convection by mean-velocity at RHS of eps-equation (Giuliano 2013)
       ie    =  n_var_EpsilonEvol
       shift = (n_var_EpsilonEvol-1)*ng
       DO ie1 = 1,dim
          user_rhs(shift+1:shift+ng) = user_rhs(shift+1:shift+ng) - (-Uav(ie1)+Umn(ie1))*du(ie,:,ie1)
       END DO
    END IF

    !--Go through all Boundary points that are specified
    i_p_face(0) = 1
    DO i=1,dim
       i_p_face(i) = i_p_face(i-1)*3
    END DO
    DO face_type = 0, 3**dim - 1
       face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
          IF( ABS(face(1)) == 1 ) THEN ! goes only through boundary points
          IF( face(1) == 1 ) THEN  ! Xmax face (internal points) 
             CALL get_all_indices_by_face (face_type, j_lev, nloc, iloc)
             IF(nloc > 0 ) THEN 
                DO ie = 1, ne
                   shift = ng*(ie-1)
                   user_rhs(shift+iloc(1:nloc)) = 0.0_pr
                   DO ie1 = 1,dim !convection with the mean
                   WHERE ( COS(PI*x(iloc(1:nloc),2)) > MAX_COS_INFLOW )
!!$                      user_rhs(shift+iloc(1:nloc)) = user_rhs(shift+iloc(1:nloc)) - Umn(ie1)*du(ie,iloc(1:nloc),ie1) 
!                      user_rhs(shift+iloc(1:nloc)) = user_rhs(shift+iloc(1:nloc)) - (u_integrated(+iloc(1:nloc),ie1)+Umn(ie1))*du(ie,iloc(1:nloc),ie1) 
                      user_rhs(shift+iloc(1:nloc)) = user_rhs(shift+iloc(1:nloc)) - &
                                                    MAX(0.0_pr,(u_integrated(iloc(1:nloc),ie1)-Uav(ie1)+Umn(ie1)))*du(ie,iloc(1:nloc),ie1)   !!! by Giuliano
                   END WHERE
                   END DO
                END DO
                IF (sgsmodel /= 0) THEN
                  IF( sgsmodel_DSM <= sgsmodel  .AND. sgsmodel <= sgsmodel_LDKMG ) THEN
                      ie = n_var_K
                      shift=(n_var_K-1)*ng 
                      user_rhs(shift+iloc(1:nloc)) = 0.0_pr
                      DO ie1 = 1,dim !convection with the mean
                      WHERE ( COS(PI*x(iloc(1:nloc),2)) > MAX_COS_INFLOW )
                          user_rhs(shift+iloc(1:nloc)) = user_rhs(shift+iloc(1:nloc)) - &
                                                    face(ie1)*MAX(0.0_pr,face(ie1)*(u_integrated(iloc(1:nloc),ie1)-Uav(ie1)+Umn(ie1)))*du(ie,iloc(1:nloc),ie1)   !!!free convection of k_sgs 
                      END WHERE
                      END DO
                  ELSEIF( sgsmodel_KEPS <= sgsmodel  .AND. sgsmodel <= sgsmodel_KOME ) THEN
                      ie = n_var_K
                      shift=(n_var_K-1)*ng 
                      user_rhs(shift+iloc(1:nloc)) = 0.0_pr
                      DO ie1 = 1,dim !convection with the mean
                      WHERE ( COS(PI*x(iloc(1:nloc),2)) > MAX_COS_INFLOW )
                          user_rhs(shift+iloc(1:nloc)) = user_rhs(shift+iloc(1:nloc)) - &
                                                    face(ie1)*MAX(0.0_pr,face(ie1)*(u_integrated(iloc(1:nloc),ie1)-Uav(ie1)+Umn(ie1)))*du(ie,iloc(1:nloc),ie1)   !!!free convection of k_sgs 
                      END WHERE
                      END DO
                      ie = n_var_KD
                      shift=(n_var_KD-1)*ng 
                      user_rhs(shift+iloc(1:nloc)) = 0.0_pr
                      DO ie1 = 1,dim !convection with the mean
                      WHERE ( COS(PI*x(iloc(1:nloc),2)) > MAX_COS_INFLOW )
                          user_rhs(shift+iloc(1:nloc)) = user_rhs(shift+iloc(1:nloc)) - &
                                                    face(ie1)*MAX(0.0_pr,face(ie1)*(u_integrated(iloc(1:nloc),ie1)-Uav(ie1)+Umn(ie1)))*du(ie,iloc(1:nloc),ie1)   !!!free convection of k_sgs 
                      END WHERE
                      END DO
                  END IF
                END IF

    !--Convection by mean-velocity at RHS of eps-equation (Giuliano 2013)
                IF( do_Adp_Eps_Spatial_Evol ) THEN 
                  ie    =  n_var_EpsilonEvol
                  shift = (n_var_EpsilonEvol-1)*ng
                  user_rhs(shift+iloc(1:nloc)) = 0.0_pr
                  DO ie1 = 1,dim
                      WHERE ( COS(PI*x(iloc(1:nloc),2)) > MAX_COS_INFLOW )
                          user_rhs(shift+iloc(1:nloc)) = user_rhs(shift+iloc(1:nloc)) - &
                                                    face(ie1)*MAX(0.0_pr,face(ie1)*(u_integrated(iloc(1:nloc),ie1)-Uav(ie1)+Umn(ie1)))*du(ie,iloc(1:nloc),ie1)
                      END WHERE
                  END DO
                END IF

            END IF
          END IF
       END IF
    END DO

!!$    !--Set operator on boundaries
!!$    IF(SUM(nbnd) /= 0) CALL user_algebraic_BC_rhs (user_rhs, ne, ng, j_lev)
!    IF( do_Adp_Eps_Spatial_Evol ) & 
!    CALL user_rhs__VT (user_rhs, u_integrated, du, d2u) ! Cf: OPTIONAL
    
  END FUNCTION user_rhs


  ! find Jacobian of Right Hand Side of the problem
  ! u_prev == u_prev_timestep_loc

  FUNCTION user_Drhs (u, u_prev, meth)
    USE curvilinear
    USE penalization
    USE elliptic_vars                                  !!! by Giuliano
!    USE parallel                                       !!! by Giuliano
    IMPLICIT NONE
    INTEGER, INTENT(IN) ::  meth
    !u_prev_timestep passed local to cast it into 2dim array to work with vector derivatives
    REAL (pr), DIMENSION (ng,ne) :: u, u_prev
    REAL (pr), DIMENSION (n) :: user_Drhs

    INTEGER :: i, ie, ie1, shift
    REAL (pr), DIMENSION (2*ne,ng,dim) :: du  ! 1st derivatives for u (in du(1:ne,:,:)) and u_prev_timestep (in du(ne+1:2*ne,:,:))
    REAL (pr), DIMENSION (ne  ,ng,dim) :: d2u ! 2nd derivatives for u 
    REAL (pr), DIMENSION (ne  ,ng,dim) :: du_dummy ! passed when only calculating 1st derivative.
    REAL (pr), DIMENSION(dim) :: Uav_prev, Uav         !!! by Giuliano
    REAL (pr) :: tmp1, tmp2                            !!! by Giuliano 
    !Find batter way to do this!! du_dummy with no storage..

    INTEGER :: face_type, nloc, meth_local
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc
    INTEGER :: meth_central, meth_backward, meth_forward 
   
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! added by Giuliano on 30 sept 2010 to be checked
     IF( Zero_Mean ) THEN
          DO ie = 1, dim
             tmp1 = SUM(u_prev(:,ie)*dA)
             CALL parallel_global_sum( REAL=tmp1 )
             Uav_prev(ie) = tmp1/sumdA_global
             tmp2 = SUM(u(:,ie)*dA)
             CALL parallel_global_sum( REAL=tmp2 )
             Uav(ie) = tmp2/sumdA_global
          END DO
       ELSE
          Uav_prev =0.0_pr
          Uav =0.0_pr
       END IF
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 
    meth_central  = meth + BIASING_NONE
    meth_backward = meth + BIASING_BACKWARD
    meth_forward  = meth + BIASING_FORWARD
    
    ! find 1st and 2nd deriviative of u and
    CALL c_diff_fast(u, du(1:ne,:,:), d2u(1:ne,:,:), j_lev, ng, meth, 11, ne , 1, ne )
    
    ! find only first deriviativ e  u_prev_timestep
    CALL c_diff_fast(u_prev, du(ne+1:2*ne,:,:), du_dummy(1:ne,:,:), j_lev, ng, meth, 10, ne , 1, ne )

    !CALL c_diff_fast_db(du_b, du_b, j_lev, ng, meth, 10, ne, 1, ne) !find 1st derivative u_b

    !--Form right hand side of Navier--Stokes equations
    DO ie = 1, dim
       shift=(ie-1)*ng
       
       user_Drhs(shift+1:shift+ng) =  nu*SUM(d2u(ie,:,:),2)
       DO ie1 = 1, dim
!          user_Drhs(shift+1:shift+ng) = user_Drhs(shift+1:shift+ng) - (u_prev(:,ie1)+Umn(ie1))*du(ie,:,ie1) & 
!                                                                    - u(:,ie1)*du(ne+ie,:,ie1) 
         user_Drhs(shift+1:shift+ng) = user_Drhs(shift+1:shift+ng) - (u_prev(:,ie1)-Uav_prev(ie1)+Umn(ie1))*du(ie,:,ie1) &
                                                                   - (u(:,ie1)-Uav(ie1))*du(ne+ie,:,ie1)      !!! by Giuliano
       END DO
    END DO
       

    IF(sgsmodel /= 0) CALL SGS_Drhs (user_Drhs, u, u_prev, du, d2u, meth)

    !--Add convection by mean-velocity at right hand side of k-equation (Giuliano 2012)
    IF( sgsmodel_DSM <= sgsmodel  .AND. sgsmodel <= sgsmodel_LDKMG ) THEN
       ie = n_var_K
       shift=(n_var_K-1)*ng
       DO ie1 = 1,dim
         user_Drhs(shift+1:shift+ng) = user_Drhs(shift+1:shift+ng) - (-Uav_prev(ie1)+Umn(ie1))*du(ie,:,ie1) - (-Uav(ie1))*du(ne+ie,:,ie1)
       END DO
    ELSEIF( sgsmodel_KEPS <= sgsmodel  .AND. sgsmodel <= sgsmodel_KOME ) THEN
       ie = n_var_K
       shift=(n_var_K-1)*ng
       DO ie1 = 1,dim
         user_Drhs(shift+1:shift+ng) = user_Drhs(shift+1:shift+ng) - (-Uav_prev(ie1)+Umn(ie1))*du(ie,:,ie1) - (-Uav(ie1))*du(ne+ie,:,ie1)
       END DO
       ie = n_var_KD
       shift=(n_var_KD-1)*ng
       DO ie1 = 1,dim
         user_Drhs(shift+1:shift+ng) = user_Drhs(shift+1:shift+ng) - (-Uav_prev(ie1)+Umn(ie1))*du(ie,:,ie1) - (-Uav(ie1))*du(ne+ie,:,ie1)
       END DO
    END IF

    IF(imask_obstacle) THEN
       DO ie = 1, dim
          shift=(ie-1)*ng
!          user_Drhs(shift+1:shift+ng) = user_Drhs(shift+1:shift+ng) - Dpenal_factor * penal/eta_chi*u(:,ie)
          user_Drhs(shift+1:shift+ng) = user_Drhs(shift+1:shift+ng) - Dpenal_factor * penal/eta_chi*(u(:,ie)-Uav(ie))  !!! by Giuliano
       END DO
       IF(sgsmodel /= 0) THEN
         IF( sgsmodel_DSM <= sgsmodel  .AND. sgsmodel <= sgsmodel_LDKMG ) THEN
            ie = n_var_K
            shift=(n_var_K-1)*ng
            user_Drhs(shift+1:shift+ng) = user_Drhs(shift+1:shift+ng) - Dpenal_factor * 2.0_pr*penal/eta_chi*u(:,ie) !penalizing 2k_sgs = 0 (2012)
         ELSEIF( sgsmodel_KEPS <= sgsmodel  .AND. sgsmodel <= sgsmodel_KOME ) THEN
            ie = n_var_K
            shift=(n_var_K-1)*ng
            user_Drhs(shift+1:shift+ng) = user_Drhs(shift+1:shift+ng) - Dpenal_factor * penal/eta_chi*u(:,ie) !penalizing 2k_sgs = 0 (2012)
            ie = n_var_KD
            shift=(n_var_KD-1)*ng
            user_Drhs(shift+1:shift+ng) = user_Drhs(shift+1:shift+ng) - Dpenal_factor * penal/eta_chi*u(:,ie) !penalizing 2k_sgs = 0 (2012)
         END IF
      END IF
    END IF

    IF(sponge_active) THEN
       DO ie = 2, dim !dump non-streamwise velocity component
          shift=(ie-1)*ng
!          user_Drhs(shift+1:shift+ng) = user_Drhs(shift+1:shift+ng) - chi_sponge/eta_sponge(ie)*u(:,ie)
          user_Drhs(shift+1:shift+ng) = user_Drhs(shift+1:shift+ng) - chi_sponge/eta_sponge(ie)*(u(:,ie)-Uav(ie))          !!! by Giuliano
       END DO
    END IF

    IF( do_Adp_Eps_Spatial_Evol ) THEN
       CALL user_Drhs__VT (user_Drhs, u, u_prev, du, d2u, meth)
    !--Add convection by mean-velocity at RHS of eps-equation (Giuliano 2013)
       ie    =  n_var_EpsilonEvol
       shift = (n_var_EpsilonEvol-1)*ng
       DO ie1 = 1,dim
         user_Drhs(shift+1:shift+ng) = user_Drhs(shift+1:shift+ng) - (-Uav_prev(ie1)+Umn(ie1))*du(ie,:,ie1) - (-Uav(ie1))*du(ne+ie,:,ie1)
       END DO
    END IF

    !--Go through all Boundary points that are specified
    i_p_face(0) = 1
    DO i=1,dim
       i_p_face(i) = i_p_face(i-1)*3
    END DO
    DO face_type = 0, 3**dim - 1
       face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
       IF( ABS(face(1)) == 1 ) THEN ! goes only through boundary points
          IF( face(1) == 1 ) THEN  ! Xmax face (internal points) 
             CALL get_all_indices_by_face (face_type, j_lev, nloc, iloc)
             IF(nloc > 0 ) THEN 
                DO ie = 1, ne
                   shift = ng*(ie-1)
                   user_Drhs(shift+iloc(1:nloc)) = 0.0_pr
                   DO ie1 = 1,dim !convection with the mean
                   WHERE ( COS(PI*x(iloc(1:nloc),2)) > MAX_COS_INFLOW )
!!$                      user_Drhs(shift+iloc(1:nloc)) = user_Drhs(shift+iloc(1:nloc)) - Umn(ie1)*du(ie,iloc(1:nloc),ie1) 
!                      user_Drhs(shift+iloc(1:nloc)) = user_Drhs(shift+iloc(1:nloc)) - (u_prev(iloc(1:nloc),ie1)+Umn(ie1))*du(ie,iloc(1:nloc),ie1) & 
!                                                                    - u(iloc(1:nloc),ie1)*du(ne+ie,iloc(1:nloc),ie1) 
                      user_Drhs(shift+iloc(1:nloc)) = user_Drhs(shift+iloc(1:nloc)) &
                                                    - MAX(0.0_pr,(u_prev(iloc(1:nloc),ie1)-Uav_prev(ie1)+Umn(ie1)))*du(ie,iloc(1:nloc),ie1) &
                                                    - ( MAX(0.0_pr,(u_prev(iloc(1:nloc),ie1)-Uav_prev(ie1)+Umn(ie1)+u(iloc(1:nloc),ie1)-Uav(ie1))) &
                                                                 -MAX(0.0_pr,(u_prev(iloc(1:nloc),ie1)-Uav_prev(ie1)+Umn(ie1)                             ))) &
                                                               *du(ne+ie,iloc(1:nloc),ie1) 
                   END WHERE
                   END DO
                END DO
                IF (sgsmodel /= 0) THEN
                  IF( sgsmodel_DSM <= sgsmodel  .AND. sgsmodel <= sgsmodel_LDKMG ) THEN
                      ie = n_var_K
                      shift=(n_var_K-1)*ng 
                      user_Drhs(shift+iloc(1:nloc)) = 0.0_pr
                      DO ie1 = 1,dim !convection with the mean
                      WHERE ( COS(PI*x(iloc(1:nloc),2)) > MAX_COS_INFLOW )
                         user_Drhs(shift+iloc(1:nloc)) = user_Drhs(shift+iloc(1:nloc)) &
                                                       - face(ie1)*MAX(0.0_pr,face(ie1)*(u_prev(iloc(1:nloc),ie1)-Uav_prev(ie1)+Umn(ie1)))*du(ie,iloc(1:nloc),ie1) &
                                                       - face(ie1)*( MAX(0.0_pr,face(ie1)*(u_prev(iloc(1:nloc),ie1)-Uav_prev(ie1)+Umn(ie1)+u(iloc(1:nloc),ie1)-Uav(ie1))) &
                                                                    -MAX(0.0_pr,face(ie1)*(u_prev(iloc(1:nloc),ie1)-Uav_prev(ie1)+Umn(ie1)                             ))) &
                                                                  *du(ne+ie,iloc(1:nloc),ie1) 
                      END WHERE
                      END DO
                  ELSEIF( sgsmodel_KEPS <= sgsmodel  .AND. sgsmodel <= sgsmodel_KOME ) THEN
                      ie = n_var_K
                      shift=(n_var_K-1)*ng 
                      user_Drhs(shift+iloc(1:nloc)) = 0.0_pr
                      DO ie1 = 1,dim !convection with the mean
                      WHERE ( COS(PI*x(iloc(1:nloc),2)) > MAX_COS_INFLOW )
                         user_Drhs(shift+iloc(1:nloc)) = user_Drhs(shift+iloc(1:nloc)) &
                                                       - face(ie1)*MAX(0.0_pr,face(ie1)*(u_prev(iloc(1:nloc),ie1)-Uav_prev(ie1)+Umn(ie1)))*du(ie,iloc(1:nloc),ie1) &
                                                       - face(ie1)*( MAX(0.0_pr,face(ie1)*(u_prev(iloc(1:nloc),ie1)-Uav_prev(ie1)+Umn(ie1)+u(iloc(1:nloc),ie1)-Uav(ie1))) &
                                                                    -MAX(0.0_pr,face(ie1)*(u_prev(iloc(1:nloc),ie1)-Uav_prev(ie1)+Umn(ie1)                             ))) &
                                                                  *du(ne+ie,iloc(1:nloc),ie1) 
                      END WHERE
                      END DO
                      ie = n_var_KD
                      shift=(n_var_KD-1)*ng 
                      user_Drhs(shift+iloc(1:nloc)) = 0.0_pr
                      DO ie1 = 1,dim !convection with the mean
                      WHERE ( COS(PI*x(iloc(1:nloc),2)) > MAX_COS_INFLOW )
                         user_Drhs(shift+iloc(1:nloc)) = user_Drhs(shift+iloc(1:nloc)) &
                                                       - face(ie1)*MAX(0.0_pr,face(ie1)*(u_prev(iloc(1:nloc),ie1)-Uav_prev(ie1)+Umn(ie1)))*du(ie,iloc(1:nloc),ie1) &
                                                       - face(ie1)*( MAX(0.0_pr,face(ie1)*(u_prev(iloc(1:nloc),ie1)-Uav_prev(ie1)+Umn(ie1)+u(iloc(1:nloc),ie1)-Uav(ie1))) &
                                                                    -MAX(0.0_pr,face(ie1)*(u_prev(iloc(1:nloc),ie1)-Uav_prev(ie1)+Umn(ie1)                             ))) &
                                                                  *du(ne+ie,iloc(1:nloc),ie1) 
                      END WHERE
                      END DO
                  END IF
                END IF

    !--Convection by mean-velocity at RHS of eps-equation (Giuliano 2013)
                IF( do_Adp_Eps_Spatial_Evol ) THEN 
                  ie    =  n_var_EpsilonEvol
                  shift = (n_var_EpsilonEvol-1)*ng
                      user_Drhs(shift+iloc(1:nloc)) = 0.0_pr
                      DO ie1 = 1,dim
                      WHERE ( COS(PI*x(iloc(1:nloc),2)) > MAX_COS_INFLOW )
                         user_Drhs(shift+iloc(1:nloc)) = user_Drhs(shift+iloc(1:nloc)) &
                                                       - face(ie1)*MAX(0.0_pr,face(ie1)*(u_prev(iloc(1:nloc),ie1)-Uav_prev(ie1)+Umn(ie1)))*du(ie,iloc(1:nloc),ie1) &
                                                       - face(ie1)*( MAX(0.0_pr,face(ie1)*(u_prev(iloc(1:nloc),ie1)-Uav_prev(ie1)+Umn(ie1)+u(iloc(1:nloc),ie1)-Uav(ie1))) &
                                                                    -MAX(0.0_pr,face(ie1)*(u_prev(iloc(1:nloc),ie1)-Uav_prev(ie1)+Umn(ie1)                             ))) &
                                                                  *du(ne+ie,iloc(1:nloc),ie1) 
                      END WHERE
                      END DO
                END IF

             END IF
          END IF
       END IF
    END DO

!    IF( do_Adp_Eps_Spatial_Evol ) & 
!    CALL user_Drhs__VT (user_Drhs, u, u_prev, du, d2u, meth)
    
  END FUNCTION user_Drhs


  !
  ! Uses u_prev_timestep, which is global flat (1D ) array set in time_step_cn()
  !
  FUNCTION user_Drhs_diag (meth)
    USE curvilinear
    USE penalization
!    USE parallel                                       !!! by Giuliano
    USE elliptic_vars                                  !!! by Giuliano
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: meth
    REAL (pr), DIMENSION (n) :: user_Drhs_diag

    INTEGER :: i, ie, ie1, shift,shiftIlm,shiftImm
    INTEGER :: shift1                                  !!! by Giuliano
    REAL (pr), DIMENSION (ng,dim) :: du, d2u
    REAL (pr), DIMENSION (ne,ng,dim) :: du_prev_timestep
    REAL (pr), DIMENSION (ne  ,ng,dim) :: du_dummy ! passed when only calculating 1st derivative.
    REAL (pr), DIMENSION(dim) :: Uav_prev              !!! by Giuliano
    REAL (pr) :: tmp1                                  !!! by Giuliano
    INTEGER :: face_type, nloc, meth_local
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc
    INTEGER :: meth_central, meth_backward, meth_forward 

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! added by Giuliano on 30 sept 2010 to be checked
    IF( Zero_Mean ) THEN
       DO ie = 1, dim
          shift=(ie-1)*ng
          tmp1 = SUM(u_prev_timestep(shift+1:shift+ng)*dA)
          CALL parallel_global_sum( REAL=tmp1 )
          Uav_prev(ie) = tmp1/sumdA_global
       END DO
    ELSE
       Uav_prev =0.0_pr
    END IF
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    meth_central  = meth + BIASING_NONE
    meth_backward = meth + BIASING_BACKWARD
    meth_forward  = meth + BIASING_FORWARD

    CALL c_diff_fast(u_prev_timestep, du_prev_timestep, du_dummy, j_lev, ng, meth, 10, ne, 1, ne)

    !
    ! does not rely on u so we can call it once here
    !
    shift = 0 !tmp
    CALL c_diff_diag(du, d2u, j_lev, ng, meth, meth, 11)

    !--Form right hand side of Navier--Stokes equations
    DO ie = 1, dim
       shift=(ie-1)*ng
       user_Drhs_diag(shift+1:shift+ng) = nu*SUM(d2u,2) - du_prev_timestep(ie,:,ie)
       DO ie1 = 1, dim
!          user_Drhs_diag(shift+1:shift+ng) = user_Drhs_diag(shift+1:shift+ng) - (u_prev_timestep((ie1-1)*ng+1:ie1*ng)+Umn(ie1))*du(:,ie1)
          user_Drhs_diag(shift+1:shift+ng) = user_Drhs_diag(shift+1:shift+ng) - (u_prev_timestep((ie1-1)*ng+1:ie1*ng)-Uav_prev(ie1)+Umn(ie1))*du(:,ie1)  !!! by Giuliano
       END DO
    END DO
    
    IF(sgsmodel /= 0) CALL SGS_Drhs_diag (user_Drhs_diag, du_prev_timestep, du, d2u, meth)

    !--Add convection by mean-velocity at right hand side of k-equation (Giuliano 2012)
    IF( sgsmodel_DSM <= sgsmodel  .AND. sgsmodel <= sgsmodel_LDKMG ) THEN
       ie = n_var_K
       shift=(n_var_K-1)*ng
       DO ie1 = 1,dim
         user_Drhs_diag(shift+1:shift+ng) = user_Drhs_diag(shift+1:shift+ng) - (-Uav_prev(ie1)+Umn(ie1))*du(:,ie1)
       END DO
    ELSEIF( sgsmodel_KEPS <= sgsmodel  .AND. sgsmodel <= sgsmodel_KOME ) THEN
       ie = n_var_K
       shift=(n_var_K-1)*ng
       DO ie1 = 1,dim
         user_Drhs_diag(shift+1:shift+ng) = user_Drhs_diag(shift+1:shift+ng) - (-Uav_prev(ie1)+Umn(ie1))*du(:,ie1)
       END DO
       ie = n_var_KD
       shift=(n_var_KD-1)*ng
       DO ie1 = 1,dim
         user_Drhs_diag(shift+1:shift+ng) = user_Drhs_diag(shift+1:shift+ng) - (-Uav_prev(ie1)+Umn(ie1))*du(:,ie1)
       END DO
    END IF

    IF(imask_obstacle) THEN
       DO ie = 1, dim
          shift=(ie-1)*ng
          user_Drhs_diag(shift+1:shift+ng) = user_Drhs_diag(shift+1:shift+ng) - Dpenal_factor * penal/eta_chi
       END DO
       IF(sgsmodel /= 0) THEN
         IF( sgsmodel_DSM <= sgsmodel  .AND. sgsmodel <= sgsmodel_LDKMG ) THEN
            ie = n_var_K
            shift=(n_var_K-1)*ng
            user_Drhs_diag(shift+1:shift+ng) = user_Drhs_diag(shift+1:shift+ng) - Dpenal_factor * 2.0_pr*penal/eta_chi !penalizing 2k_sgs = 0 (2012)
         ELSEIF( sgsmodel_KEPS <= sgsmodel  .AND. sgsmodel <= sgsmodel_KOME ) THEN
            ie = n_var_K
            shift=(n_var_K-1)*ng
            user_Drhs_diag(shift+1:shift+ng) = user_Drhs_diag(shift+1:shift+ng) - Dpenal_factor * penal/eta_chi !penalizing 2k_sgs = 0 (2012)
            ie = n_var_KD
            shift=(n_var_KD-1)*ng
            user_Drhs_diag(shift+1:shift+ng) = user_Drhs_diag(shift+1:shift+ng) - Dpenal_factor * penal/eta_chi !penalizing 2k_sgs = 0 (2012)
         END IF
      END IF
    END IF

    IF(sponge_active) THEN
       DO ie = 2, dim !dump non-streamwise velocity components
          shift=(ie-1)*ng
          user_Drhs_diag(shift+1:shift+ng) = user_Drhs_diag(shift+1:shift+ng) - chi_sponge/eta_sponge(ie)
       END DO
    END IF

    IF( do_Adp_Eps_Spatial_Evol ) THEN
       CALL user_Drhs_diag__VT (user_Drhs_diag, du_prev_timestep, du, d2u, meth)
    !--Add convection by mean-velocity at RHS of eps-equation (Giuliano 2013)
       ie    =  n_var_EpsilonEvol
       shift = (n_var_EpsilonEvol-1)*ng
       DO ie1 = 1,dim
         user_Drhs_diag(shift+1:shift+ng) = user_Drhs_diag(shift+1:shift+ng) - (-Uav_prev(ie1)+Umn(ie1))*du(:,ie1)
       END DO
    END IF

    !--Go through all Boundary points that are specified
    i_p_face(0) = 1
    DO i=1,dim
       i_p_face(i) = i_p_face(i-1)*3
    END DO
    DO face_type = 0, 3**dim - 1
       face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
       IF( ABS(face(1)) == 1 ) THEN ! goes only through boundary points
          IF( face(1) == 1 ) THEN  ! Xmax face (internal points) 
             CALL get_all_indices_by_face (face_type, j_lev, nloc, iloc)
             IF(nloc > 0 ) THEN 
                DO ie = 1, dim
                   shift = ng*(ie-1)
                   WHERE ( COS(PI*x(iloc(1:nloc),2)) > MAX_COS_INFLOW )
                   user_Drhs_diag(shift+iloc(1:nloc)) = &
                      - MAX(0.0_pr,SIGN(1.0_pr,(u_prev_timestep(shift+iloc(1:nloc))-Uav_prev(ie)+Umn(ie))))*du_prev_timestep(ie,iloc(1:nloc),ie)
                   END WHERE
                END DO
                DO ie = dim+1, ne
                   shift = ng*(ie-1)
                   WHERE ( COS(PI*x(iloc(1:nloc),2)) > MAX_COS_INFLOW )
                   user_Drhs_diag(shift+iloc(1:nloc)) = 0.0_pr
                   END WHERE
                END DO
                DO ie = 1, ne
                   shift = ng*(ie-1)
                   DO ie1 = 1,dim !convection with the mean
                      shift1=(ie1-1)*ng
                      WHERE ( COS(PI*x(iloc(1:nloc),2)) > MAX_COS_INFLOW )
                      user_Drhs_diag(shift+iloc(1:nloc)) = user_Drhs_diag(shift+iloc(1:nloc)) &
                                                         - MAX(0.0_pr,(u_prev_timestep(shift1+iloc(1:nloc))-Uav_prev(ie1)+Umn(ie1)))*du(iloc(1:nloc),ie1)
                      END WHERE
                   END DO
                END DO
                IF (sgsmodel /= 0) THEN
                  IF( sgsmodel_DSM <= sgsmodel  .AND. sgsmodel <= sgsmodel_LDKMG ) THEN
                      ie = n_var_K
                      shift=(n_var_K-1)*ng 
                      user_Drhs_diag(shift+iloc(1:nloc)) = 0.0_pr
                      DO ie1 = 1,dim !convection with the mean
                         shift1=(ie1-1)*ng                                                                                                         !!! by Giuliano
                         WHERE ( COS(PI*x(iloc(1:nloc),2)) > MAX_COS_INFLOW )
                         user_Drhs_diag(shift+iloc(1:nloc)) = user_Drhs_diag(shift+iloc(1:nloc)) &
                                                            - MAX(0.0_pr,(u_prev_timestep(shift1+iloc(1:nloc))-Uav_prev(ie1)+Umn(ie1)))*du(iloc(1:nloc),ie1)
                         END WHERE
                      END DO
                  ELSEIF( sgsmodel_KEPS <= sgsmodel  .AND. sgsmodel <= sgsmodel_KOME ) THEN
                      ie = n_var_K
                      shift=(n_var_K-1)*ng 
                      user_Drhs_diag(shift+iloc(1:nloc)) = 0.0_pr
                      DO ie1 = 1,dim !convection with the mean
                         shift1=(ie1-1)*ng                                                                                                         !!! by Giuliano
                         WHERE ( COS(PI*x(iloc(1:nloc),2)) > MAX_COS_INFLOW )
                         user_Drhs_diag(shift+iloc(1:nloc)) = user_Drhs_diag(shift+iloc(1:nloc)) &
                                                            - MAX(0.0_pr,(u_prev_timestep(shift1+iloc(1:nloc))-Uav_prev(ie1)+Umn(ie1)))*du(iloc(1:nloc),ie1)
                         END WHERE
                      END DO
                      ie = n_var_KD
                      shift=(n_var_KD-1)*ng 
                      user_Drhs_diag(shift+iloc(1:nloc)) = 0.0_pr
                      DO ie1 = 1,dim !convection with the mean
                         shift1=(ie1-1)*ng                                                                                                         !!! by Giuliano
                         WHERE ( COS(PI*x(iloc(1:nloc),2)) > MAX_COS_INFLOW )
                         user_Drhs_diag(shift+iloc(1:nloc)) = user_Drhs_diag(shift+iloc(1:nloc)) &
                                                            - MAX(0.0_pr,(u_prev_timestep(shift1+iloc(1:nloc))-Uav_prev(ie1)+Umn(ie1)))*du(iloc(1:nloc),ie1)
                         END WHERE
                      END DO
                  END IF
                END IF

    !--Convection by mean-velocity at RHS of eps-equation (Giuliano 2013)
                IF( do_Adp_Eps_Spatial_Evol ) THEN
                  ie    =  n_var_EpsilonEvol
                  shift = (n_var_EpsilonEvol-1)*ng
                      user_Drhs_diag(shift+iloc(1:nloc)) = 0.0_pr
                      DO ie1 = 1,dim
                         shift1=(ie1-1)*ng
                         WHERE ( COS(PI*x(iloc(1:nloc),2)) > MAX_COS_INFLOW )
                         user_Drhs_diag(shift+iloc(1:nloc)) = user_Drhs_diag(shift+iloc(1:nloc)) &
                                                            - MAX(0.0_pr,(u_prev_timestep(shift1+iloc(1:nloc))-Uav_prev(ie1)+Umn(ie1)))*du(iloc(1:nloc),ie1)
                         END WHERE
                      END DO
                END IF

             END IF
          END IF
       END IF
    END DO

!    IF( do_Adp_Eps_Spatial_Evol ) &
!    CALL user_Drhs_diag__VT (user_Drhs_diag, du_prev_timestep, du, d2u, meth)

  END FUNCTION user_Drhs_diag

  FUNCTION user_chi (nlocal, t_local)
    USE curvilinear
    IMPLICIT NONE
    INTEGER,          INTENT (IN) :: nlocal
    REAL (pr),        INTENT (IN) :: t_local
    REAL (pr), DIMENSION (nlocal) :: user_chi
    REAL (pr), DIMENSION(nlocal,DIM) :: Xloc, x_phys
    INTEGER :: i
    LOGICAL, SAVE :: user_chi_initialized = .FALSE.

    !--Defines mask for array of plates

    IF( ANY(transform_dir(1:dim)>0) ) THEN
       x_phys = user_mapping (nlocal, t_local )
    ELSE
       x_phys = x
    END IF

    ! only aligned prism supported at this time
    !Xloc(:,1) = COS(angle)*x_phys(:,1) + SIN(angle)*x_phys(:,2) !shift and rotate
    !Xloc(:,2) =-SIN(angle)*x_phys(:,1) + COS(angle)*x_phys(:,2) !shift and rotate
    Xloc(:,1) = x_phys(:,1)
    Xloc(:,2) = x_phys(:,2)

    user_chi = 0.0_pr

    WHERE ( ABS(Xloc(:,1)) <= 0.5_pr*length .AND. ABS(Xloc(:,2)) <= 0.5_pr*height )
       user_chi = 1.0_pr
    ELSEWHERE
       user_chi = 0.0_pr
    END WHERE

    user_chi = MIN(1.0_pr, user_chi)

  END FUNCTION user_chi

  FUNCTION user_mapping (nlocal, t_local )
    USE curvilinear
    USE curvilinear_mesh
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: nlocal
    REAL (pr), INTENT (IN) :: t_local
    REAL (pr), DIMENSION (nlocal,dim) :: user_mapping

    ! Tensorial stretching variables
    REAL (pr), DIMENSION (dim)  :: limit, trans_width, tmp

    ! Stretching (0 < RR = (r-R1)/(R2-R1) < 1)
    REAL (pr), DIMENSION (nlocal) :: RR
    REAL (pr) :: R1, R2
    REAL (pr) :: a = -0.0_pr                ! MUST BE -1 < a < 3

    user_mapping(:,1:dim) = x(:,1:dim)

    IF( ANY(transform_dir(1:dim)>0) ) THEN

       PI = 2.0_pr*ASIN(1.0_pr)

!!$       user_mapping(:,1) =  x(:,1)*COS(PI*x(:,2))
!!$       user_mapping(:,2) =  x(:,1)*SIN(PI*x(:,2))

       R1 = MINVAL(x(:,1))
       R2 = MAXVAL(x(:,1))

       RR(:) = (x(:,1)-R1)/(R2-R1)          ! 0 <= RR <= 1

       user_mapping(:,1) =  COS(PI*x(:,2)) * R1 * exp( ( (1.0_pr+a)*RR(:) - 2.0_pr*a*RR(:)**2 + a*RR(:)**3) * log(R2/R1) )
       user_mapping(:,2) =  SIN(PI*x(:,2)) * R1 * exp( ( (1.0_pr+a)*RR(:) - 2.0_pr*a*RR(:)**2 + a*RR(:)**3) * log(R2/R1) )

    END IF ! transform_dir

  END FUNCTION user_mapping

   FUNCTION x_stretched (x_local, xmin_local, xmax_local, xc, stretching_factor, nlocal)
    USE curvilinear
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: nlocal
    REAL (pr), INTENT (IN) :: stretching_factor, xc, xmin_local, xmax_local
    REAL (pr), DIMENSION (nlocal), INTENT(IN) :: x_local
    REAL (pr), DIMENSION (nlocal) :: x_stretched 
    REAL (pr) :: yc, A
    
   yc=(xc-xmin_local)/(xmax_local-xmin_local)
   A=log((1.0_pr+(exp(stretching_factor)-1.0_pr)*yc)/(1+(exp(-stretching_factor)-1.0_pr)*yc))/(2.0_pr*stretching_factor)
   x_stretched=yc*(1.0_pr+sinh(stretching_factor*((x_local-xmin_local)/(xmax_local-xmin_local)-A))/sinh(stretching_factor*A))

 END FUNCTION x_stretched


  !
  ! Calculate any statitics
  !
  ! startup_flag - 0 when adapting to IC,then 1 inmain integration loop
  !
  !
  SUBROUTINE user_stats (u , j_mn, startup_flag)
    USE precision
    USE variable_mapping
!    USE fft_module
!    USE spectra_module
    USE db_tree
    USE wlt_vars
    USE vector_util_mod
    USE parallel            ! par_size
    IMPLICIT NONE
    REAL (pr), DIMENSION (nwlt,1:n_var), INTENT (IN) :: u
    INTEGER , INTENT (IN) :: j_mn
    INTEGER , INTENT (IN) :: startup_flag
    CHARACTER (LEN=256)  :: filename

    !================= OBSTACLE ====================
    REAL (pr), SAVE             :: drag, drag_av, drag_exp_av
    REAL (pr), SAVE             :: lift, lift_av, lift_exp_av, q_av
    REAL (pr), DIMENSION (dim)  :: force
    INTEGER, SAVE               :: N_av
    !==============================================

    !================= PLANE STATS ====================
    REAL (pr), DIMENSION(:,:,:,:), ALLOCATABLE :: momU
    REAL (pr), DIMENSION(:,:), ALLOCATABLE :: momS2, momSGSD, momX, momS12, momKsgs, momUV
    REAL (pr) TMPij(nwlt,2*dim), tmp(nwlt)  !tmp for Sij

    INTEGER, DIMENSION(:), ALLOCATABLE :: momN, momSHIFT, dir_line
    INTEGER :: dir_homog, mom, inwlt(1)
    INTEGER :: i_plane, j_plane, i_line, proc
    INTEGER :: i, idim, step, ixyz(1:dim), ixyz_plane(1:dim)
    LOGICAL , SAVE :: start = .TRUE.
    !==============================================


    !USER may define additional statistics output here.

    IF(startup_flag /= -1) THEN !statistics accumulation
       IF (par_rank.EQ.0) THEN
          WRITE(*,'(" ")')
          WRITE(*,'("****************** Statistics on the Fly *******************")')
          WRITE(*,'(" ")')
       END IF

       CALL user_cal_force (u(:,1:dim), nwlt, t, force, drag, lift, u(:,n_var_pressure))

       IF (imask_obstacle) THEN !--Calculate force
!          CALL user_cal_force (u(:,1:dim), nwlt, t, force, drag, lift)
          !--- calculating time averaged quantities ---------
          IF(t <= t_av) THEN
             N_av = 1
             lift_av = lift
             lift_exp_av = lift
             drag_av = drag
             drag_exp_av = drag
          ELSE
             lift_av = (lift_av*REAL(N_av,pr)+lift)/REAL(N_av+1,pr)
             drag_av = (drag_av*REAL(N_av,pr)+drag)/REAL(N_av+1,pr)
             N_av = N_av+1
             q_av = dt/tau_av
             lift_exp_av = (1.0_pr - q_av)*lift_exp_av + q_av*lift
             drag_exp_av = (1.0_pr - q_av)*drag_exp_av + q_av*drag
          END IF
          IF (par_rank.EQ.0) THEN
             WRITE (6,'(" Drag = ", es9.2, 1x, "Drag_av = ", es9.2, 1x, "Drag_exp_av = ", es9.2)') drag, drag_av, drag_exp_av
             WRITE (6,'(" Lift = ", es9.2, 1x, "Lift_av = ", es9.2, 1x, "Lift_exp_av = ", es9.2)') lift, lift_av, lift_exp_av

             OPEN  (UNIT=UNIT_USER_STATS, FILE = file_name_user_stats, FORM='formatted', STATUS='old', POSITION='append')
             IF( startup_flag == 0 ) THEN
                WRITE(UNIT=UNIT_USER_STATS,ADVANCE='YES', FMT='( a )' ) &
                     '%Time            Drag            Drag_av         Drag_exp_av     Lift            Lift_av         Lift_exp_av'
             END IF
             WRITE (UNIT_USER_STATS,'(7(es15.8,1x))') t, drag, drag_av, drag_exp_av, lift, lift_av, lift_exp_av
             CLOSE (UNIT=UNIT_USER_STATS)
          END IF
       END IF
       IF (par_rank.EQ.0) THEN
          WRITE(*,'("***********************************************************************")')
          WRITE(*,'(" ")')
       END IF
    END IF

    !--Calculate statistics on the additional planes
    IF (par_rank.EQ.0) WRITE(*,'("ITER_STAT = ",I4)')  iter_stat
    IF (additional_planes_active .AND. SUM(prd(1:dim)) == 1 .AND. iter_stat > 0) THEN
       IF (par_rank.EQ.0) WRITE(*,'("STATISTICS ON THE ADDITIONAL PLANES")')

       j_plane = MIN(j_additional_planes,j_lev)
       step = MAX(1,2**(j_lev-j_plane))
       ALLOCATE(  momU(MAXVAL(nxyz(:)/step+1-prd(:)),1:2,1:dim,n_additional_planes))
       ALLOCATE(  momX(MAXVAL(nxyz(:)/step+1-prd(:)),n_additional_planes))
       ALLOCATE(momS12(MAXVAL(nxyz(:)/step+1-prd(:)),n_additional_planes))
       ALLOCATE( momS2(MAXVAL(nxyz(:)/step+1-prd(:)),n_additional_planes))
       ALLOCATE(momSGSD(MAXVAL(nxyz(:)/step+1-prd(:)),n_additional_planes))
       ALLOCATE( momUV(MAXVAL(nxyz(:)/step+1-prd(:)),n_additional_planes))
       ALLOCATE(momKsgs(MAXVAL(nxyz(:)/step+1-prd(:)),n_additional_planes))
       ALLOCATE(momN(n_additional_planes),momSHIFT(n_additional_planes),dir_line(n_additional_planes))

       momU = 0.0_pr
       momS12 = 0.0_pr
       momS2 = 0.0_pr
       momSGSD = 0.0_pr
       momUV = 0.0_pr
       momKsgs = 0.0_pr

       ! Calculate s_12=s(:,4)
       CALL Sij( u(:,1:dim) ,nwlt, TMPij(:,1:2*dim), tmp(:), .TRUE. )

       HOMOG_LOOP: DO idim=1,dim
          IF(prd(idim) == 1) THEN
             dir_homog = idim
             EXIT HOMOG_LOOP
          END IF
       END DO HOMOG_LOOP

       DO i_plane = 1,n_additional_planes

          IF(prd(dir_planes(i_plane)) /= 1) THEN
             DO idim=1,dim
                IF(idim /= dir_homog .AND. idim /= dir_planes(i_plane)) dir_line(i_plane)=idim
             END DO
             i=0
             DO WHILE (xx(i*STEP,dir_line(i_plane)) <= xyzzone(1,dir_line(i_plane)) .AND. xx(i*STEP,dir_line(i_plane)) < xyzlimits(2,dir_line(i_plane)) )
                i=i+1
             END DO
             momSHIFT(i_plane)=i-1
             i=nxyz(dir_line(i_plane))/STEP
             DO WHILE (xx(i*STEP,dir_line(i_plane)) >= xyzzone(2,dir_line(i_plane)) .AND. xx(i*STEP,dir_line(i_plane)) > xyzlimits(1,dir_line(i_plane)) )
                i=i-1
             END DO
             momN(i_plane)=i-momSHIFT(i_plane)
             DO i=1,momN(i_plane)
                momX(i,i_plane) = xx((momSHIFT(i_plane)+i)*STEP,dir_line(i_plane))
             END DO

             DO idim = 1, dim
                ixyz_plane(idim) = (MINLOC( ABS(xx(0:nxyz(idim)-prd(idim):step,idim) - xyz_planes(idim,i_plane)), DIM = 1  ) - 1 )*step
             END DO
             !---------- go through all points on the plane for  j <= j_plane ------------------
             ixyz(dir_planes(i_plane)) = ixyz_plane(dir_planes(i_plane))
             DO i_line = 1 ,momN(i_plane)
                ixyz(dir_line(i_plane)) = (momSHIFT(i_plane)+i_line)*STEP
                DO i=0,nxyz(dir_homog)-prd(dir_homog),STEP
                   ixyz(dir_homog) = i
                   CALL DB_get_proc_by_coordinates (ixyz, j_lev, proc)
                   IF (proc.EQ.par_rank) THEN
                      CALL get_indices_by_coordinate(1,ixyz(1:dim),j_lev,inwlt(1), 0)
                      DO idim=1,dim
                         DO mom = 1,2
                            momU(i_line,mom,idim,i_plane) = momU(i_line,mom,idim,i_plane) + u(inwlt(1),idim)**mom
                         END DO
                      END DO
                      momS12(i_line,i_plane) = momS12(i_line,i_plane) + TMPij(inwlt(1),4)

                      momS2(i_line,i_plane) = momS2(i_line,i_plane) + 2.0_pr*(TMPij(inwlt(1),1)**2+TMPij(inwlt(1),2)**2+TMPij(inwlt(1),3)**2) + &
                                                                      4.0_pr*(TMPij(inwlt(1),4)**2+TMPij(inwlt(1),5)**2+TMPij(inwlt(1),6)**2)

                      IF ( do_Adp_Eps_Spatial .OR. do_Adp_Eps_Spatial_Evol ) &
                      momSGSD(i_line,i_plane) = momSGSD(i_line,i_plane) + u(inwlt(1),n_var_SGSD)
                      momKsgs(i_line,i_plane) = momKsgs(i_line,i_plane) + u(inwlt(1),n_var_K)
                      momUV(i_line,i_plane) = momUV(i_line,i_plane) + u(inwlt(1),1)*u(inwlt(1),2)
                   END IF
                END DO
             END DO
             DO  i = 1,momN(i_plane)
                DO idim=1,dim
                   DO mom = 1,2
                      CALL parallel_global_sum( REAL = momU(i,mom,idim,i_plane)  )
                   END DO
                END DO
                CALL parallel_global_sum( REAL = momS12(i,i_plane)  )
                CALL parallel_global_sum( REAL = momS2(i,i_plane)  )
                CALL parallel_global_sum( REAL = momSGSD(i,i_plane)  )
                CALL parallel_global_sum( REAL = momKsgs(i,i_plane)  )
                CALL parallel_global_sum( REAL = momUV(i,i_plane)  )
             END DO
             momU = momU/(nxyz(dir_homog+1-prd(dir_homog)))*STEP
             momUV = momUV/(nxyz(dir_homog+1-prd(dir_homog)))*STEP
             momS12 = momS12/(nxyz(dir_homog+1-prd(dir_homog)))*STEP
             momS2 = momS2/(nxyz(dir_homog+1-prd(dir_homog)))*STEP
             momSGSD = momSGSD/(nxyz(dir_homog+1-prd(dir_homog)))*STEP
             momKsgs = momKsgs/(nxyz(dir_homog+1-prd(dir_homog)))*STEP
             IF (par_rank.EQ.0)  THEN
                WRITE(*,'("i_plane=",I4," dir_line=",I2)') i_plane, dir_line(i_plane)
                WRITE(*,'("x",13X,"u",13x,"u2",12x,"v",13x,"v2",12x,"w",13x,"w2",12x,"S12",12x,"Ksgs",13x,"uv",13x,"S2",13x,"SGSD")')
                DO  i = 1,momN(i_plane)
                   WRITE(*,'(12(E12.5,2X))') momX(i,i_plane),momU(i,1:2,1:dim,i_plane),momS12(i,i_plane),momKsgs(i,i_plane),momUV(i,i_plane),momS2(i,i_plane),momSGSD(i,i_plane)
                END DO
                !---------- write output file for time-average ------------------
                OPEN (UNIT_USER_STATZ, FILE = file_name_user_statz, FORM='formatted', STATUS='old', POSITION='append')
                 WRITE(UNIT_USER_STATZ,ADVANCE='YES', FMT='( a )' ) '%Time         Number      Plane'
                 WRITE(UNIT_USER_STATZ,'(E12.5,2X,I6,2X,I6)') t, momN(i_plane), i_plane
                 WRITE(UNIT_USER_STATZ,ADVANCE='YES', FMT='( a )' ) &
                   '%Abscissa      u             u^2           v            v^2            w            w^2            S12          Ksgs        uv         S         SGSD'
                 DO  i = 1,momN(i_plane)
                  WRITE(UNIT_USER_STATZ,'(12(E12.5,2X))') momX(i,i_plane),momU(i,1:2,1:dim,i_plane),momS12(i,i_plane),momKsgs(i,i_plane),momUV(i,i_plane),momS2(i,i_plane),momSGSD(i,i_plane)
                 END DO
                CLOSE (UNIT_USER_STATZ)
             END IF

          END IF

       END DO

    END IF

    IF (additional_planes_active .AND. SUM(prd(1:dim)) == 1 ) iter_stat = iter_stat + 1
    IF (par_rank.EQ.0) WRITE(*,'("ITER_STAT after = ",I4)')  iter_stat

  END SUBROUTINE user_stats

  SUBROUTINE user_cal_force (u, n, t_local, force, drag, lift, pressure)
!  SUBROUTINE user_cal_force (u, n, t_local, force, drag, lift)
    !--Calculates drag and lift on obstacle using penalization formula
    USE penalization
!    USE parallel
    IMPLICIT NONE

    INTEGER, INTENT (IN) :: n
    INTEGER :: ie
    REAL (pr), INTENT (IN) :: t_local
    REAL (pr), DIMENSION (dim), INTENT (INOUT) :: force
    REAL (pr), INTENT (OUT) :: drag, lift
    REAL (pr), DIMENSION (n,dim) :: u
    REAL (pr), DIMENSION (n) :: pressure
    REAL (pr) :: A
    REAL (pr) :: tmp1, tmp2, tmp3
    REAL (pr), DIMENSION(dim) :: U_av      !!! by Giuliano

    INTEGER :: i, ii, shift
    INTEGER :: face_type, nloc, meth_local
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(n) :: iloc

    IF(dim == 3) THEN
       A = height * (xyzlimits(2,dim)-xyzlimits(1,dim))
    ELSE IF(dim == 2) THEN
       A = height
    ELSE
       A = 1.0_pr
    END IF
    IF(par_rank.EQ.0) WRITE(*,'(" A= ",E16.10)') A

!    DO ie = 1, dim
!       tmp1 = SUM(penal/eta_chi*(u(:,ie)+Umn(ie))*dA)       !by Giuliano Nov 2011
!       CALL parallel_global_sum( REAL=tmp1 )
!       force(ie) = tmp1/(0.5_pr * A * SUM(Umn(1:dim)**2))
!       IF(par_rank.EQ.0) print *, 'force(',ie,')=', tmp1
!    END DO
!    drag = force(1) ; lift = force(2)

     !--Go through all Boundary points that are specified
     i_p_face(0) = 1
     DO i=1,dim
        i_p_face(i) = i_p_face(i-1)*3
     END DO
     DO face_type = 0, 3**dim - 1
        face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
        IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
           CALL get_all_indices_by_face (face_type, j_lev, nloc, iloc)
           IF(nloc > 0 ) THEN 
              IF( face(1) == -1  ) THEN                             ! Xmin face (entire face) 
                 drag = SUM(   pressure(iloc(1:nloc)) * COS(PI*x(iloc(1:nloc),2)) * x(iloc(1:nloc),1) )
                 lift = SUM( - pressure(iloc(1:nloc)) * SIN(PI*x(iloc(1:nloc),2)) * x(iloc(1:nloc),1) )
              END IF
           END IF
        END IF
     END DO

!!! modified by Giuliano on 30 sept 2010   
!    DO ie = 1, dim
!       tmp1 = SUM(penal/eta_chi*(u(:,ie)+Umn(ie))*dA)
!       tmp2 = SUM(u(:,ie)*dA)
!       CALL parallel_global_sum( REAL=tmp1 )
!       CALL parallel_global_sum( REAL=tmp2 )
!       force(ie) = tmp1
!       U_av(ie) = tmp2/sumdA_global
!       IF(par_rank.EQ.0) WRITE(*,'(" ie= ",E16.10)') ie*1.0_pr
!       IF(par_rank.EQ.0) WRITE(*,'(" tmp1= ",E16.10)') tmp1
!       IF(par_rank.EQ.0) WRITE(*,'(" tmp2= ",E16.10)') tmp2
!    END DO
!    drag = force(1) / (0.5_pr * A * MAX(1.0e-12,(Umn(1)+U_av(1))**2))
!    lift = force(2) / (0.5_pr * A * MAX(1.0e-12,(Umn(1)+U_av(1))**2))
    IF(par_rank.EQ.0) WRITE(*,'(" force 1st component = ",E16.10)') drag
    IF(par_rank.EQ.0) WRITE(*,'(" force 2nd component = ",E16.10)') lift
!    IF(par_rank.EQ.0) WRITE(*,'(" force 3rd component = ",E16.10)') force(3) / (0.5_pr * A * MAX(1.0e-12,(Umn(1)+U_av(1))**2))
    
  END SUBROUTINE user_cal_force

  !
  ! calculate any additional variables
  ! 
  ! arg 
  ! flag  - 0 calledwhile adapting to IC, 1 - called in main time integration loop
  !
  ! These additiona variables are calculated and left in real space.
  !
  SUBROUTINE user_additional_vars( t_local, flag )
    USE curvilinear
    IMPLICIT NONE
    REAL (pr), INTENT (IN) ::  t_local
    INTEGER , INTENT(IN) :: flag ! 0- called during adaption to IC, 1 called during main integration loop
    REAL(pr) tmp(nwlt,2*dim) !tmp for vorticity
    INTEGER :: i


    ! For vorticity gradient norm evaluation (2014)
    REAL (pr), DIMENSION (1:nwlt,3)   :: tmp_w, tmp_AW 
    REAL (pr), DIMENSION (1:nwlt,6)   :: tmp_SW
    REAL (pr), DIMENSION (1:nwlt)     :: tmp_wmod, tmp_AWmod, tmp_SWmod


!    u(:,n_var_mask) =  user_chi (nwlt, t_local) !--Defines mask for sphere (3d) or cylinder (2d)
    
!    IF( flag == 1 .OR. adaptMagVort .OR. saveMagVort .OR. adaptNormS .OR. saveNormS) THEN ! only in main time int loop, not initial adaptationdd
    IF( flag == 1 .OR. adaptMagVort .OR. saveMagVort .OR. adaptNormS .OR. saveNormS .OR. saveVort .OR. saveNormGradVort ) THEN    !by Giuliano
       ! Calculate the magnitude vorticity
!       IF( adaptMagVort .OR. saveMagVort ) THEN 
       IF( adaptMagVort .OR. saveMagVort .OR. saveVort ) THEN        !by Giuliano
          CALL cal_vort (u(:,1:dim), tmp(:,1:3-MOD(dim,3)), nwlt)
          IF (saveMagVort) u(:,n_var_modvort) = SQRT(SUM(tmp(:,1:3-MOD(dim,3))**2, 2))
          IF (saveVort) THEN
             DO i =1,3-MOD(dim,3)
                u(:,n_var_vorticity + i-1) = tmp(:,i)     !by Giuliano 
             END DO
          END IF
       END IF

       ! Calculate the magnitude of Sij (sqrt(SijSij) )
       ! s(:,1) = s_11
       ! s(:,2) = s_22
       ! s(:,3) = s_33
       ! s(:,4) = s_12
       ! s(:,5) = s_13
       ! s(:,6) = s_23

       IF( adaptNormS .OR. saveNormS) THEN
          CALL Sij( u(:,1:dim) ,nwlt, tmp(:,1:2*dim), u(:,n_var_modSij), .TRUE. )
       END IF

       ! Calculate the magnitude of velocity
       IF( adaptMagVel ) THEN 
          u(:,n_var_modvel) = 0.5_pr*SUM(u(:,1:dim)**2.0_pr,DIM=2)
       END IF

       ! Calculate the magnitude of vorticity gradient (Giuliano jan 2014)
       IF( saveNormGradVort ) THEN
          CALL Wij( u(:,1:3), nwlt, tmp_w, tmp_wmod )
          tmp_w = -2.0_pr*tmp_w                                      ! tmp_w = vorticity
          CALL Sij( tmp_w(:,1:3), nwlt, tmp_SW, tmp_SWmod, .TRUE. )  ! tmp_SW = symmetric part
          CALL Wij( tmp_w(:,1:3), nwlt, tmp_AW, tmp_AWmod )          ! tmp_AW = emisymmetric
          u(:,n_var_NormGradVort) = (tmp_AWmod(:)**2+tmp_SWmod(:)**2)**0.5_pr
       END IF

    END IF


  END SUBROUTINE user_additional_vars


  !
  ! calculate any additional scalar variables
  ! 
  ! arg 
  ! flag  - 0 calledwhile adapting to IC, 1 - called in main time integration loop
  !
  SUBROUTINE user_scalar_vars( flag )
    IMPLICIT NONE
    INTEGER , INTENT(IN) :: flag ! 0- called during adaption to IC, 1 called during main integration loop



  END SUBROUTINE user_scalar_vars

  !
  !************ Calculating Scales ***************************
  !
  ! Note the order of the components in the scl array
  ! correspond to u_tn, v_tn, w_tn, u_tn-1, v_tn-1, w_tn-1, u_tn-2, v_tn-2, w_tn-2
  ! 
  SUBROUTINE user_scales(flag, use_default, u_loc, nlocal, ne_local, l_n_var_adapt , l_n_var_adapt_index, &
       scl, scl_fltwt) !add
!    USE parallel
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: flag ! 0 during initial adaptation, then 1 during time advancement
    LOGICAL , INTENT(INOUT) :: use_default
    INTEGER, INTENT (IN) :: nlocal, ne_local
    REAL (pr), DIMENSION (1:nlocal,1:ne_local), INTENT (IN) :: u_loc
    LOGICAL , INTENT (IN) :: l_n_var_adapt(ne_local)
    INTEGER , INTENT (IN) :: l_n_var_adapt_index(1:ne_local)
    REAL (pr), DIMENSION (1:ne_local), INTENT (INOUT) :: scl
    REAL (pr) ,  INTENT(IN) ::scl_fltwt !weight for temporal filter on scl
    REAL (pr) :: floor, tmp
    INTEGER :: ie, ie_index, itmp
    REAL (pr) ,save, ALLOCATABLE, DIMENSION (:) :: scl_old
    LOGICAL  ,save :: startup_init = .TRUE.

    !
    ! Ignore the output of this routine and use default scales routine
    !
    use_default = .FALSE. 

    !
    ! NOTE: For a parallel run, synchronize scl(:) across the processors in user_scales.
    !       Use the subroutine scales of default_util.f90 as an example,
    !       subroutine parallel_global_sum of parallel.f90 may be helpful.
    !       Already synchronized: sumdA_global
    !
    IF( .NOT. use_default ) THEN

       IF (par_rank.EQ.0) PRINT *,'Use user_scales() <<<<<<<<<<<<<<<<<<<<<<<<<<<'
       !
       ! ALLOCATE scl_old if it was not done previously in user_scales
       !
       IF( startup_init .AND. .NOT. ALLOCATED(scl_old) ) THEN
          ALLOCATE( scl_old(1:ne_local) )
          scl_old = 0.0_pr
       END IF
       
       floor = 1.e-12_pr
       scl   =1.0_pr
       !
       ! Calculate scl per component
       !
!       DO ie=1,ne_local
       DO ie=1,n_var
          IF(l_n_var_adapt(ie)) THEN
             ie_index = l_n_var_adapt_index(ie)
             IF( Scale_Meth == 1 ) THEN ! Use Linf scale
                scl(ie)= MAXVAL ( ABS( u_loc(1:nlocal,ie_index) ) )
                CALL parallel_global_sum( REALMAXVAL=scl(ie) )
                
             ELSE IF ( Scale_Meth == 2 ) THEN ! Use L2 scale
                scl(ie) = SUM( (u_loc(1:nlocal,ie_index)**2) )
                itmp = nlocal
                CALL parallel_global_sum( REAL=scl(ie) )
                CALL parallel_global_sum( INTEGER=itmp )
                scl(ie) = SQRT ( scl(ie)/itmp  )
                
             ELSE IF ( Scale_Meth == 3 ) THEN ! Use L2 scale using dA
                scl(ie) = SUM( (u_loc(1:nlocal,ie_index)**2)*dA )
                CALL parallel_global_sum( REAL=scl(ie) )
                scl(ie)= SQRT ( scl(ie) / sumdA_global  )
                
             ELSE
                IF (par_rank.EQ.0) THEN
                   PRINT *, 'Error in Scales(), Unknown Scale_Meth type: ', Scale_Meth
                   PRINT *, 'Known Scale_Meth types: 1 = Linf scale, 2 = L2 scale '
                   PRINT *, 'Exiting ...'
                END IF
                CALL parallel_finalize; STOP
             END IF
             
             IF (par_rank.EQ.0) THEN
                WRITE (6,'("Scaling before taking vector(1:dim) magnitude")')
                WRITE (6,'("Scale factor eqn", I4 ," = ", es11.4 ," scalecoeff = ",es11.4 )') &
                     ie, scl(ie), scaleCoeff(ie)
             END IF
             
          END IF
       END DO
       !
       ! take appropriate vector norm over scl(1:dim)
       IF( Scale_Meth == 1 ) THEN ! Use Linf scale
          tmp = MAXVAL(scl(1:dim)) !this is statistically equivalent to velocity vector length
          scl(1:dim) = tmp
       ELSE IF ( Scale_Meth == 2 ) THEN ! Use L2 scale
          tmp = SQRT(SUM( scl(1:dim)**2 ) ) !now velocity vector length, rather than individual component
          scl(1:dim) = tmp
       ELSE IF ( Scale_Meth == 3 ) THEN ! Use L2 scale using dA
          tmp = SQRT(SUM( scl(1:dim)**2 ) ) !now velocity vector length, rather than individual component
          scl(1:dim) = tmp
       END IF
       !
       ! Print out new scl
       !
!       DO ie=1,ne_local
       DO ie=1,n_var
          IF(l_n_var_adapt(ie)) THEN
             !this is done if one of the variable is exactly zero inorder not to adapt to the nois
             IF(scl(ie) .le. floor) scl(ie)=1.0_pr 
             tmp = scl(ie)
             ! temporally filter scl
             IF(.not. startup_init ) scl(ie) = scl_fltwt * scl_old(ie) + (1.0_pr - scl_fltwt) * scl(ie)
             scl_old(ie) = scl(ie)           !save scl for this time step
!             scl = scaleCoeff * scl
             scl(ie) = scaleCoeff(ie) * scl(ie)


             IF (par_rank.EQ.0) THEN
                WRITE (6,'("Scaling on vector(1:dim) magnitude")')
                WRITE (6,'("Scale factor eqn", I4 ," = ", es11.4 ," scalecoeff = ",es11.4 )') &
                     ie, scl(ie), scaleCoeff(ie)
                WRITE (6,'("(before filtering = ",es11.4,")" )') tmp
             END IF
          END IF
       END DO
 
       IF(startup_init) THEN
          scl(n_var_vel(1:dim)) = 1.0_pr
       END IF
       scl_old = scl !save scl for this time step
       startup_init = .FALSE.
       PRINT *,'TEST scl_old ', scl_old
    END IF ! use default
    
  END SUBROUTINE user_scales

  SUBROUTINE user_cal_cfl (use_default, u, cfl_out)
!    USE parallel
    USE variable_mapping 
    USE curvilinear
    USE curvilinear_mesh 
    IMPLICIT NONE
    LOGICAL , INTENT(INOUT) :: use_default
    REAL (pr),                                INTENT (INOUT) :: cfl_out
    REAL (pr), DIMENSION (nwlt,n_integrated), INTENT (IN)    :: u
    REAL (pr), DIMENSION (nwlt,dim)                          :: v
    
    INTEGER                    :: i, i_map
    REAL (pr)                  :: floor
    REAL (pr), DIMENSION (dim) :: cfl
    REAL (pr), DIMENSION(dim,nwlt) :: h_arr
    
    use_default = .FALSE.
    
    floor = 1e-12_pr
    cfl_out = floor
    
    CALL get_all_local_h (h_arr)

    !curvilinear velocity:

    !DO i =1, nwlt
    !   DO i_map = 1, dim
    !      v(i,i_map) =    SUM(curvilinear_jacobian(1:dim,i_map,i) * (u(i,1:dim)+Umn(1:dim))) 
    !   END DO
    !END DO
    DO i = 1, nwlt
       v(i,1:dim) = u(i,1:dim)+Umn(1:dim) 
    END DO
    v(:,1:dim) = transform_vector_to_comp( v(:,1:dim) )
    
    DO i = 1, nwlt
       cfl(1:dim) = ABS (v(i,1:dim)) * dt/h_arr(1:dim,i)
       cfl_out = MAX (cfl_out, MAXVAL(cfl))
    END DO

    CALL parallel_global_sum( REALMAXVAL=cfl_out )
    
  END SUBROUTINE user_cal_cfl
  
  SUBROUTINE  user_sgs_force ( u, nlocal)
    USE variable_mapping 
    IMPLICIT NONE
    INTEGER,                         INTENT (IN) :: nlocal
    REAL (pr), DIMENSION (nlocal,n_integrated), INTENT (INOUT) :: u

    !default SGS_force, can be explicitely deifined below instead of calling default SGS_force
    IF(sgsmodel /= 0) CALL sgs_force ( u, nlocal)

    !Saving the WTF width field (Giuliano jan 2014)
    IF( saveDelta )  u(:,n_var_Delta) = delta(:)
    
  END SUBROUTINE user_sgs_force

  FUNCTION user_sound_speed (u, neq, nwlt)
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: nwlt, neq
    REAL (pr), DIMENSION (nwlt,neq), INTENT(IN) :: u
    REAL (pr), DIMENSION (nwlt) :: user_sound_speed

    !user_sound_speed(:) = SQRT( gamma*(gamma-1.0_pr)* &
    !       ( u(:,4)-0.5_pr*SUM(u(:,n_var_mom(1:dim))**2,DIM=2)/u(:,1)) ) ! pressure

    user_sound_speed(:) = 0.0_pr

  END FUNCTION user_sound_speed

  SUBROUTINE  user_pre_process
!    USE parallel                                       !!! by Giuliano
    USE elliptic_vars                                  !!! by Giuliano
    IMPLICIT NONE
    INTEGER, PARAMETER :: ne_local = 1
    INTEGER :: i
    INTEGER :: meth_p, meth_central, meth_backward, meth_forward 
    REAL (pr), DIMENSION (nwlt,ne_local) :: dp, dp0
    REAL (pr), DIMENSION (nwlt,ne_local) :: f
    REAL (pr), DIMENSION(ne_local) :: scl_p
    INTEGER, DIMENSION(ne_local) :: clip 
    REAL (pr), DIMENSION(dim) :: Uav
    REAL (pr) :: tmp1, tmp2
    INTEGER :: ie, idim
    REAL (pr), DIMENSION(dim) :: du, dS
    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc
    REAL (pr), DIMENSION(dim,nwlt) :: normal
    REAL (pr), DIMENSION (dim,nwlt,dim) :: du_comp, d2u_comp

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! added by Giuliano on 30 sept 2010 to be checked
    IF( Zero_Mean ) THEN
       DO ie = 1, dim
          tmp1 = SUM(u(:,ie)*dA)
          CALL parallel_global_sum( REAL=tmp1 )
          u(:,ie) = u(:,ie) - tmp1/sumdA_global
       END DO
    END IF
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    !zero flux correction to ensure no mass accumulation inside of the domain
    !OLEG: 06.29.2012 - wrong implementation, mass is global. If mass is not conserved, the mass should be corrected at teh inflow.
    IF(zero_flux_correction) THEN 
       !--Boundary points
       i_p_face(0) = 1
       DO i=1,dim
          i_p_face(i) = i_p_face(i-1)*3
       END DO
       du=0.0_pr
       dS=0.0_pr
       DO face_type = 0, 3**dim - 1
          face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
          IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
             CALL get_all_indices_by_face (face_type, j_lev, nloc, iloc)
             IF(nloc > 0 ) THEN ! ANY dimension
                DO i = 1, dim !net flux du and net bundary volume (does not matter, since resolution is set to j_zn
                   du(i) =  du(i) + face(i)*SUM(u(iloc(1:nloc),i)*dA(iloc(1:nloc)))
                   dS(i) =  dS(i) + ABS(face(i))*SUM(dA(iloc(1:nloc)))
                END DO
             END IF
          END IF
       END DO
       DO i = 1, dim !substracting velocity correction
          IF(prd(i) == 0) THEN
             tmp1 = du(i)
             tmp2 = dS(i)
             CALL parallel_global_sum( REAL=tmp1 )
             CALL parallel_global_sum( REAL=tmp2 )
             u(:,i) = u(:,i)-tmp1/tmp2
          END IF
       END DO
    END IF

    IF(divergence_correction) THEN
       IF (par_rank.EQ.0) THEN 
        PRINT *, '**********************************************************************'
        PRINT *, '*                       divergence correction                        *'
        PRINT *, '**********************************************************************'
       END IF
       meth_central  = HIGH_ORDER + BIASING_NONE
       meth_backward = HIGH_ORDER + BIASING_BACKWARD
       meth_forward  = HIGH_ORDER + BIASING_FORWARD
       IF(pressure_high_order) THEN
          meth_p = HIGH_ORDER
       ELSE
          meth_p = LOW_ORDER
       END IF

       !--Make u  divergence free
       dp = 0.0_pr 
       dp0 = 0.0_pr
       
       f = Laplace_rhs (u(:,1:dim), nwlt, dim, meth_p)
       clip = MIN(1,BCtype,BCclip) ! if no part of the boundary explicetely set up as Diricket, then clip = 0
       
       scl_p = MAXVAL(scl_global(1:dim)) !scale of pressure increment based on dynamic pressure
       IF(two_step_pressure) THEN
          IF (par_rank.EQ.0) PRINT *,'-------- user_pre_process - TWO-STEP PRESSURE: pressure_BC_type = 0 --------'
          pressure_BC_type = 0
          BCtype = 2
          clip = MIN(1,BCtype,BCclip) ! if no part of the boundary explicetely set up as Diricket, then clip = 0

          CALL Linsolve (dp0, f , tol2, nwlt, ne_local, clip, Laplace, Laplace_diag, SCL=scl_p) !new with scaling  
          IF (par_rank.EQ.0) PRINT *,'-------- user_pre_process - TWO-STEP PRESSURE: pressure_BC_type = 1 --------'
          pressure_BC_type = 1
          BCtype = 1
          clip = MIN(1,BCtype,BCclip) ! if no part of the boundary explicetely set up as Diricket, then clip = 0
          f = f - RESHAPE(Laplace (j_lev, dp0, nwlt, ne_local, meth_p), (/nwlt, ne_local/) )

          CALL Linsolve (dp, f , tol2, nwlt, ne_local, clip, Laplace, Laplace_diag, SCL=scl_p) !new with scaling 
          dp = dp + dp0
       ELSE
          IF (par_rank.EQ.0) PRINT *,'-------- user_pre_process - ONE-STEP PRESSURE: pressure_BC_type = 1 --------'
          pressure_BC_type = 1
          clip = MIN(1,BCtype,BCclip) ! if no part of the boundary explicetely set up as Diricket, then clip = 0
          CALL Linsolve (dp, f , tol2, nwlt, ne_local, clip, Laplace, Laplace_diag, SCL=scl_p) !new with scaling 
          !    CALL Linsolve (dp, f , tol2, nlocal, ne_local, clip, Laplace, Laplace_diag)  !old without scaling
          
       END IF

       IF(div_grad) THEN
          u(:,1:dim) = u(:,1:dim) - grad(dp, nwlt, j_lev, meth_p+BIASING_BACKWARD)
       ELSE   !! second derivative also
          u(:,1:dim) = u(:,1:dim) - grad(dp, nwlt, j_lev, meth_p+BIASING_NONE)
       END IF

    END IF

    IF(sponge_active) THEN
       IF(ALLOCATED(chi_sponge)) DEALLOCATE(chi_sponge)
       ALLOCATE(chi_sponge(nwlt))
       chi_sponge = 1.0_pr
       DO idim = 1,dim
          chi_sponge = chi_sponge * &
               0.5_pr * ( tanh( ( x(:,idim)-sponge_zone(0,idim))/del_sponge(0,idim) )    &
                         -tanh( ( x(:,idim)-sponge_zone(1,idim))/del_sponge(1,idim) ) ) 
       END DO
       chi_sponge = 1.0_pr - chi_sponge    !!! to be commented for the strip version
    END IF

    !========================================================================
    IF(sgsmodel == sgsmodel_KOME) THEN
       i_p_face(0) = 1
       DO i=1,dim
          i_p_face(i) = i_p_face(i-1)*3
       END DO
       DO face_type = 0, 3**dim - 1
          face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
          IF( face(1) == -1 ) THEN
             CALL get_all_indices_by_face (face_type, j_lev, nloc, iloc)
             IF(nloc > 0 ) THEN 
                normal(1:dim,iloc(1:nloc)) = 0.0_pr
                normal(1,iloc(1:nloc)) =  COS(PI*x(iloc(1:nloc),2))
                normal(2,iloc(1:nloc)) =  SIN(PI*x(iloc(1:nloc),2))
                CALL c_diff_fast (user_mapping (nwlt, t ), du_comp, d2u_comp, j_lev, nwlt, HIGH_ORDER, 10, dim, 1, dim, FORCE_RECTILINEAR=.TRUE.)
                delta_n=h(1,1)/(2.0_pr**(J_mx-1))*MINVAL(ABS(SUM(du_comp(1:dim,iloc(1:nloc),1)*normal(1:dim,iloc(1:nloc)),DIM=1)))
                CALL parallel_global_sum( REALMINVAL=delta_n )
                IF(par_rank == 0) PRINT *, 'delta_n=', delta_n
             END IF
          END IF
       END DO
    END IF
    !========================================================================


  END SUBROUTINE user_pre_process

 SUBROUTINE  user_post_process

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! added by Giuliano on 30 sept 2010 to be checked
!    USE parallel
    USE elliptic_vars
    IMPLICIT NONE
    REAL (pr), DIMENSION(dim) :: Uav
    REAL (pr) :: tmp1
    INTEGER :: i, ie

    IF( Zero_Mean ) THEN
       DO ie = 1, dim
          tmp1 = SUM(u(:,ie)*dA)
          CALL parallel_global_sum( REAL=tmp1 )
          u(:,ie) = u(:,ie) - tmp1/sumdA_global
       END DO
    END IF
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    IF ( do_Adp_Eps_Spatial .OR. do_Adp_Eps_Spatial_Evol ) &
    CALL user_post_process__VT(n_var_SGSD)    ! n_var_ExtraAdpt_Der    ! n_var_Epsilon   ! n_var_RSGSD         ! Cf: OPTIONAL
    
  END SUBROUTINE user_post_process


END MODULE user_case





