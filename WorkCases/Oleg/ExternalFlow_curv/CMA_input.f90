MODULE CMA_input

  USE precision
  IMPLICIT NONE
  PRIVATE

  INTEGER :: N_Q, N_input_names, N_output_names
  REAL(pr), DIMENSION(:), ALLOCATABLE :: Q 
  CHARACTER (LEN=256), DIMENSION(:), ALLOCATABLE ::  input_name_array, output_name_array
  LOGICAL :: CMA_active=.FALSE., CMA_test=.FALSE.
  CHARACTER (LEN=1000):: fitnessFile
  REAL(pr) :: fitness
  INTEGER :: ID

 
  PUBLIC :: N_Q, N_input_names, N_output_names, Q, ID, input_name_array, output_name_array
  PUBLIC :: read_CMA_input, CMA_active, CMA_test
  PUBLIC :: fitness, fitnessFile

  CONTAINS
    

    SUBROUTINE read_CMA_input(iarg)
      USE precision
      USE input_file_reader
#ifdef _MSC_VER
      USE DFLIB
#endif
      IMPLICIT NONE

      INTEGER :: iarg
#ifdef _MSC_VER
      INTEGER(2) :: i
#else
      INTEGER(4) :: i
#endif
      INTEGER :: k
      CHARACTER (LEN=1000):: inputline, workDir, jobName, IDstring
      INTEGER :: legnth, numLines
          

      ! Get command line input
      CALL getarg(1,inputline)
      WRITE(*,*) TRIM(inputline)
      
      ! Open input file and parse it
      OPEN(2,file=TRIM(inputline))
        READ(2,'(A1000)') workDir
        READ(2,'(A1000)') jobName
        READ(2,*) numLines
        N_Q = numLines
        IF(ALLOCATED(Q)) DEALLOCATE(Q); IF(N_Q>0) ALLOCATE(Q(1:N_Q))
        DO k = 1, N_Q-1
           READ(2,*) Q(k)
        END DO
        READ(2,*) ID
      CLOSE(2)

      IF(ALLOCATED(input_name_array)) DEALLOCATE(input_name_array); ALLOCATE(input_name_array(1:1))
      IF(ALLOCATED(output_name_array)) DEALLOCATE(output_name_array); ALLOCATE(output_name_array(1:1))
      input_name_array(1:1) = "nonCMA.inp"


      WRITE(IDstring,'(I6.6)') ID
      fitnessFile = TRIM(workDir)//"fitness_"//trim(IDstring)
      WRITE(*,*) TRIM(fitnessFile)

   END SUBROUTINE read_CMA_input


    SUBROUTINE read_CMA_input_oldformat(iarg)
      USE precision
      USE input_file_reader
#ifdef _MSC_VER
      USE DFLIB
#endif
      IMPLICIT NONE

      INTEGER :: iarg
#ifdef _MSC_VER
      INTEGER(2) :: i
#else
      INTEGER(4) :: i
#endif
      CHARACTER (LEN=256) ::  inputline
      INTEGER :: rec_len, Nrec, rec_beg, rec_end, rec_beg_n, rec_beg_p, next_rec, Nparams, k, itmp

    
    ! Use the first argument as the input file name if is exists

      !input line arguments tested
      !n=2p=2p=-.5n=2p=test.inpp=test1.inpn=0

      i=iarg
      CALL getarg( i, inputline )
      
      PRINT *,'inputline:', inputline
      
      rec_len = LEN(TRIM(inputline))
      rec_end = 0
      Nrec = 0
            
      DO WHILE(rec_end < rec_len) 
         rec_beg=INDEX(inputline(rec_end+1:rec_len),'n=')
         !PRINT *, 'test:',INDEX(inputline(rec_end+rec_beg:MIN(rec_end+rec_beg+1,rec_len)),'n=')
         IF(rec_beg>0) THEN
            Nrec = Nrec + 1  
            rec_end = rec_end + rec_beg + 1 
            rec_beg=INDEX(inputline(rec_end+1:rec_len),'p=')-1
            IF(rec_beg == -1) rec_beg = rec_len - rec_end
            !PRINT *,'Nparamsline:', inputline(rec_end+1:rec_end+rec_beg)
            READ(UNIT=inputline(rec_end+1:rec_end+rec_beg),FMT='(I4)') Nparams       ! transform to integer
            rec_end = rec_end + rec_beg 
            IF(Nrec == 1) THEN
               N_Q = Nparams
               IF(ALLOCATED(Q)) DEALLOCATE(Q); IF(Nparams>0) ALLOCATE(Q(1:Nparams))
            ELSE IF(Nrec == 2) THEN
               N_input_names = Nparams
               IF(ALLOCATED(input_name_array)) DEALLOCATE(input_name_array); IF(Nparams>0) ALLOCATE(input_name_array(1:Nparams))
            ELSE IF(Nrec == 3) THEN
               N_output_names = Nparams
               IF(ALLOCATED(output_name_array)) DEALLOCATE(output_name_array); IF(Nparams>0) ALLOCATE(output_name_array(1:Nparams))
            END IF
            next_rec=rec_end+INDEX(inputline(rec_end+1:rec_len),'n=')-1
            IF(next_rec == rec_end-1) next_rec = rec_len+1
            IF(Nparams==0) rec_end=next_rec-1
            DO k=1,Nparams
               rec_beg=INDEX(inputline(rec_end+1:rec_len),'p=')
               IF(rec_beg==0) THEN
                  WRITE(*,'("INPUT ERROR: no record p= is found")') 
                  WRITE(*,'("PROGRAM is TERMINATED")')
                  STOP
               END IF
               rec_end = rec_end + rec_beg+1
               rec_beg_p=INDEX(inputline(rec_end+1:rec_len),'p=')
               rec_beg_n=INDEX(inputline(rec_end+1:rec_len),'n=')
               IF(MIN(rec_beg_p,rec_beg_n)/=0 ) THEN
                  rec_beg = MIN(rec_beg_p,rec_beg_n)-1
               ELSE IF(MIN(rec_beg_p,rec_beg_n)==0 .AND. MAX(rec_beg_p,rec_beg_n) /=0) THEN
                  rec_beg = MAX(rec_beg_p,rec_beg_n)-1
               ELSE
                  rec_beg = rec_len - rec_end
               END IF
               IF(rec_beg <= 0 .OR. rec_end >= next_rec) THEN
                  WRITE(*,'("INPUT ERROR: # of parematers in input line is ",I4," out of ",I4)') k-1, Nparams
                  WRITE(*,'("PROGRAM is TERMINATED")')
                  STOP
               END IF
               !PRINT *,'line:', inputline(rec_end+1:rec_end+rec_beg)
               IF(Nrec == 1) THEN
                 IF( INDEX(inputline(rec_end+1:rec_end+rec_beg),'.') > 0)  THEN
                   READ(UNIT=inputline(rec_end+1:rec_end+rec_beg),FMT='(E14.8)') Q(k) ! read floaign point format
                 ELSE
                   READ(UNIT=inputline(rec_end+1:rec_end+rec_beg),FMT='(I8)') itmp    ! read integer format
                   Q(k) = REAL(itmp,pr)
                 END IF
                 PRINT *, k,'Q=',Q(k)
               ELSE IF(Nrec == 2) THEN
                 READ(UNIT=inputline(rec_end+1:rec_end+rec_beg),FMT='(A256)') input_name_array(k) ! transform to CHAR*256
               ELSE IF(Nrec == 3) THEN
                 READ(UNIT=inputline(rec_end+1:rec_end+rec_beg),FMT='(A256)') output_name_array(k) ! transform to CHAR*256
               END IF
               rec_end = rec_end + rec_beg 
            END DO
            IF(Nrec == 1)  PRINT *,'Nparams=',Nparams, ' Q=',Q
            IF(Nrec == 2)  THEN 
               DO k = 1, Nparams
                  PRINT *, 'k=',k,' input_name_array=',TRIM(input_name_array(k))
               END DO
            END IF
            IF(Nrec == 3)  THEN 
               DO k = 1, Nparams
                  PRINT *, 'k=',k,' output_name_array=',TRIM(output_name_array(k))
               END DO
            END IF
         ELSE
            rec_end = rec_end + rec_beg  
         END IF
      END DO
      
    END SUBROUTINE read_CMA_input_oldformat

    SUBROUTINE  user_read_command_line_input
      USE share_consts

      IMPLICIT NONE
      INTEGER:: i
      CHARACTER (LEN = 256) :: file_name
      
      !***************************************************************
      CALL read_CMA_input(1) !input parameter is the # of the argument 
      !***************************************************************
      
      file_name = TRIM(input_name_array(1))
      CMA_active = .TRUE.

    END SUBROUTINE user_read_command_line_input

END MODULE CMA_input
