!$
!!$!****************************************************************************
!!$!* Module for adding Brinkman Penalization terms onto the full NS equations *
!!$!*  -Only valid for single species 10/7/2011                                *
!!$!*  -R is scaled to 1.0
!!$!****************************************************************************
!!$
!!$

MODULE user_case_vars
  USE precision
 ! REAL (pr) :: Re, Rebnd, Pra, Sc, Fr, Ma, At
  INTEGER :: Nspec, Nspecm
  INTEGER :: nden, neng, nprs,ndrho!, ndpx, nboy, nbRe, ndom, nton, BCver,ICver,nmsk,ncvtU
  INTEGER, ALLOCATABLE :: nvel(:)!, nspc(:), nvon(:), nson(:) 
  LOGICAL :: no_slip ! if NS=.TRUE. - Navier-Stoke, else Euler
  LOGICAL :: conv_drho ! input file parameter, extrapolate drho
  REAL (pr), ALLOCATABLE, DIMENSION (:) ::  mu_in, kk_in, cp_in, MW_in, gamm  

  REAL (pr) :: upwind_norm
  REAL (pr) :: Ma
 ! INTEGER :: nden, neng, nprs, ndpx, nboy, nbRe, ndom, nton, BCver,ICver,nmsk,ncvtU
 ! INTEGER, ALLOCATABLE :: nvel(:), nspc(:), nvon(:), nson(:), nUn(:)
 ! REAL (pr) :: rhoL, pL, EL, rhoR, pRR, ER
 ! REAL (pr), DIMENSION(:), ALLOCATABLE :: uL, uR
  ! Spheres
!!$  INTEGER :: ndist
  LOGICAL :: saveDIST
  LOGICAL :: saveUn
  INTEGER :: nUn, nUtan
!!$  INTEGER, ALLOCATABLE :: n_var_mom(:)!, nspc(:), nvon(:), nson(:)   
 

END MODULE user_case_vars


MODULE Brinkman

  USE precision
  USE wlt_vars
  USE wavelet_filters_mod
  USE elliptic_mod
  USE elliptic_vars
  USE wlt_trns_mod
  USE wlt_trns_vars
  USE wlt_trns_util_mod
  USE io_3d_vars
  USE util_mod
  USE util_vars
  USE share_consts
  USE pde
  USE sizes
  USE share_kry
  USE vector_util_mod
  USE field
  USE input_file_reader
  USE additional_nodes
  USE penalization
  USE user_case_vars
  USE hyperbolic_solver
  USE VT


  LOGICAL :: brinkman_penal, vp  !penalization type flags
  REAL (pr) :: cbvp_A, cbvp_B
  REAL (pr) :: brinkman_temp

  REAL (pr) :: xw, wa ! Wedge
  REAL (pr) :: xsp, ysp, rsp ! Sphere
  ! Spheres
  REAL (pr), DIMENSION(:), ALLOCATABLE :: xsps, ysps, rsps
  INTEGER :: Nsp

  REAL (pr) :: visc_factor_d,visc_factor_n  !parameters for viscous zone around penalized obstacle
  INTEGER :: delta_pts, IB_pts

  LOGICAL :: meth_vect_diff

  REAL (pr) :: delta_conv, delta_diff, shift_conv, shift_diff
  REAL (pr) :: xwall
  REAL (pr) :: deltadel_loc
  LOGICAL :: smooth_dist
  LOGICAL :: doCURV
  REAL (pr) :: doNORM

  REAL (pr) :: eta_b, eta_c
  REAL (pr) :: porosity

CONTAINS

  SUBROUTINE user_dist (nlocal, t_local, DISTANCE, NORMAL, U_TARGET)
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: nlocal
    REAL (pr), INTENT (IN) :: t_local
    REAL (pr), DIMENSION(nlocal), OPTIONAL, INTENT(INOUT) :: DISTANCE
    REAL (pr), DIMENSION(nlocal, dim), OPTIONAL, INTENT(INOUT) :: NORMAL,U_TARGET
    REAL (pr), DIMENSION(nlocal) :: DST
    REAL (pr), DIMENSION(nlocal,dim) :: xp
    INTEGER :: idim, isp
    REAL (pr), DIMENSION(dim) :: Xs
    REAL (pr), DIMENSION(nlocal) :: DSTS
    REAL (pr) :: pi

    !PRINT *, "Distance:", t_local

          pi = 2.0_pr*ASIN(1.0_pr)

    IF(PRESENT(DISTANCE) .OR. .NOT.use_dist ) THEN
       !PRINT *, 'Distance function is calculated'
       !Find distance function
       !1D obstacle 
       IF (dim ==1 .AND. .NOT.(stationary_obstacle)) THEN
          !DST = 0.075_pr-ABS(    x(:,1) - 0.075_pr)  !closed obstacle centered at x = 0.075
!          DST = x(:,1) - (- 0.001_pr*t_local)! - (-0.01_pr*SIN(5.0_pr*t_local+PI/2.0_pr)   )                           ! interface at origin
          
          DST = x(:,1) - (0.25_pr - 0.5_pr*t_local)
          !DST = x(:,1) - (-0.4_pr*COS(2.0_pr*pi*0.04255_pr*t_local)   )                           ! interface at origin
       ELSE IF (dim==1 .AND. stationary_obstacle) THEN
          DST = x(:,1)
       ELSE   !multidimensional obstacle
          ! WEDGE ONLY:
          !pi = 2.0_pr*ASIN(1.0_pr)
          !xp= 0.0_pr
          !Xs(1) = 1.5_pr
          !Xs(2) = (1.5-xw)*TAN(wa*pi/180.0_pr)
          !xp(:,1) = (x(:,1)-Xs(1))*COS(wa*pi/180.0_pr) + (x(:,2)-Xs(2))*SIN(wa*pi/180.0_pr) 
          !xp(:,2) = (x(:,1)-Xs(1))*SIN(wa*pi/180.0_pr) - (x(:,2)-Xs(2))*COS(wa*pi/180.0_pr)
          !DST(:) = xp(:,2)
          ! Cylinder only
          !DST(:) = rsp - SQRT((x(:, 1) - xsp)**2 + (x(:, 2) - ysp)**2)
          ! Wall only
          !DST(:) = x(:, 1) - xwall
          ! Many spheres
          DST(:) = -1.0D+10
          DO isp = 1, Nsp
             IF(.NOT.(stationary_obstacle)) THEN
                !DSTS(:) = rsps(isp) - SQRT((x(:, 1) - xsps(isp))**2.0_pr + (x(:, 2) - (ysps(isp) + 0.4_pr*COS(2.0_pr*pi*0.04255_pr*t_local)  ))**2.0_pr)
                DSTS(:) = rsps(isp) - SQRT((x(:, 1) - (-Ma*t_local))**2.0_pr + (x(:, 2) - (ysps(isp) + 0.00_pr*COS(2.0_pr*pi*0.04255_pr*t_local)  ))**2.0_pr)
                IF(t_local<0.5_pr/0.04255_pr)DSTS(:) = rsps(isp) - SQRT((x(:, 1) - (-Ma*t_local))**2.0_pr + (x(:, 2) - (ysps(isp) + 0.01_pr*COS(2.0_pr*pi*0.04255_pr*t_local)  ))**2.0_pr)
             ELSE
                DSTS(:) = rsps(isp) - SQRT((x(:, 1) - xsps(isp))**2.0_pr + (x(:, 2) - ysps(isp))**2.0_pr)
             END IF
             DST = MAX(DST, DSTS)
          END DO
       END IF


!!$       WHERE(xp(:,1) <= 0.0_pr) 
!!$          DST(:) = ABS(xp(:,2))
!!$       ELSEWHERE
!!$          DST(:) = SQRT(SUM(xp(:,1:dim)**2,DIM=2))
!!$       END WHERE
!!$       xp(:,1) = x(:,1)-Xs(1)
!!$       xp(:,2) = x(:,2)-Xs(2)
!!$       WHERE(xp(:,1) >= 0.0_pr) 
!!$          DST(:) = MIN(DST(:),ABS(xp(:,2)))
!!$       ELSEWHERE
!!$          DST(:) = MIN(DST(:),SQRT(SUM(xp(:,1:dim)**2,DIM=2)))
!!$       END WHERE
!!$
!!$       WHERE ( ((x(:, 2) - (x(:, 1) - xw)*TAN(wa*pi/180.0_pr) <= 0.0_pr) .AND. (x(:, 1) <= 1.5_pr)) .OR. &
!!$            ((x(:, 1) > 1.5_pr) .AND. (x(:, 2) <= (1.5_pr - xw)*TAN(wa*pi/180.0_pr))) ) 
!!$          DST(:) = DST(:)
!!$       ELSEWHERE
!!$          DST(:) = -DST(:)
!!$       END WHERE

       IF(smooth_dist) THEN
         CALL diffsmooth (DST,nlocal,1,HIGH_ORDER,11,0,0.5_pr,-MINVAL(h(1,1:dim))/2.0_pr**(REAL(j_mx,pr)-1.0_pr),deltadel_loc,-2,j_lev,3,.FALSE.)
       END IF

       IF (use_dist) dist = DST
    ELSE
       DST = dist
    END IF

    IF(PRESENT(DISTANCE)) DISTANCE = DST

    !Definition of NORMAL in the obstacle

    IF(PRESENT(NORMAL)) THEN
       CALL c_diff_fast(DST, NORMAL(:, :), xp(:, :), j_lev, nlocal, HIGH_ORDER, 10, 1, 1, 1)
    
       xp(:,1) = SQRT(SUM(NORMAL(:,1:dim)**2.0_pr,DIM=2))
       DO idim = 1, dim
          NORMAL(:,idim) = NORMAL(:,idim)/(xp(:,1) + 1.0D-10)
       END DO

       !1D obstacle at origin
       IF (dim ==1) THEN
          NORMAL(:,:) = 0.0_pr
          !WHERE (x(:,1) .LE. 0.075_pr)
          !   NORMAL(:,1) = 1.0_pr
          !ELSEWHERE
          !   NORMAL(:,1) = -1.0_pr
          !END WHERE
          NORMAL(:,1) = 1.0_pr
       END IF
          
    END IF

    IF(PRESENT(U_TARGET)) THEN
       U_TARGET=0.0_pr
       !U_TARGET=-0.001_pr
       !U_TARGET= -0.05_pr*COS(5.0_pr*t_local +PI/2.0_pr)
       IF(dim==1 .AND. .NOT.(stationary_obstacle)) THEN
          !U_TARGET= -0.05_pr*COS(5.0_pr*t_local +PI/2.0_pr)
          U_TARGET=-0.5_pr
           !  PRINT *, 'u_target moving', MINVAL(U_TARGET), MAXVAL(U_TARGET)

       END IF
       IF(dim >1 .AND. .NOT.(stationary_obstacle)) THEN
          !U_TARGET(:,2) = -0.4_pr*2.0_pr*PI*0.04255_pr*SIN(2.0_pr*PI*0.04255_pr*t_local) !oscillation in the y-direction 
          U_TARGET(:,2) = -0.0_pr 
          IF(t_local< 0.5_pr/0.04255_pr) U_TARGET(:,2) = -0.01_pr*2.0_pr*PI*0.04255_pr*SIN(2.0_pr*PI*0.04255_pr*t_local) !oscillation in the y-direction 
          U_TARGET(:,1) = -Ma 
       END IF
    END IF
            !  PRINT *, 'u_target moving', MINVAL(U_TARGET), MAXVAL(U_TARGET)


  END SUBROUTINE user_dist



  SUBROUTINE Brinkman_rhs (user_rhs, u_integrated, homog, meth)
    USE user_case_vars
    IMPLICIT NONE
    REAL (pr), DIMENSION (ng*ne), INTENT(INOUT) :: user_rhs
    REAL (pr), DIMENSION (ng,ne), INTENT(IN) :: u_integrated !1D flat version of integrated variables without BC 
    INTEGER, INTENT(IN) :: meth
    LOGICAL, INTENT(IN) :: homog

    INTEGER :: ie, shift,i,j, idim
    REAL (pr), DIMENSION (ng,dim)     :: dp
    REAL (pr), DIMENSION (ng,dim)     :: Unorm, Utan, norm
    REAL (pr), DIMENSION (ng,dim)     :: u_0     !for non-stationary obstacle
    REAL (pr), DIMENSION (ng,dim)     :: brink_var     !for non-stationary obstacle
    REAL (pr), DIMENSION (dim,ng,dim) :: d2u_brink
    REAL (pr) :: cbvp_t

   
    REAL (pr), DIMENSION (ng,ne+dim) :: u_diff
    REAL (pr) :: nu_n    !artificial/physical viscosity with continuous fluxes
    REAL (pr), DIMENSION (ne+dim,ng,dim) :: du_diff, du_diffFD, du_diffBD, d2u_diff
    REAL (pr), DIMENSION (ne+dim,ng) :: dudn
    INTEGER :: n_deriv
    REAL (pr), DIMENSION (ng) :: Spenal,Spenal2,Spenal3
    REAL (pr) :: slope, offset
    REAL (pr) :: delta_Jmx
    INTEGER :: meth_central, meth_backward, meth_forward  
    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc

    REAL (pr), DIMENSION(ng) :: curv                 ! curvature
    REAL (pr), DIMENSION (ng) :: p                   ! pressure, for curvature adjustment if applicable
    REAL (pr), DIMENSION (dim, ng, dim) :: dn, d2n   ! derivative of normal

    REAL (pr), DIMENSION(ng, dim) :: vel_tan, vel_norm, vel_void
    REAL (pr), DIMENSION (ng)     :: A, C, F
    REAL (pr), DIMENSION(ng, dim) :: B

    REAL (pr), DIMENSION(ng, dim) :: v_back, v_for, vTemp


    REAL (pr), DIMENSION(2*ng,ne+2*dim) ::uh_vector
    REAL (pr), DIMENSION(ne+2*dim,2*ng,dim) ::du_vector, d2u_vector
    INTEGER, DIMENSION(2) :: meth_vector

    !PRINT *, "rhs, t:", t
    IF(t.LE.1.0_pr) THEN
       cbvp_t = cbvp_B*t/1.0_pr
    ELSE
       cbvp_t = cbvp_B
    END IF
    IF(brinkman_penal) THEN   !ERIC: not set up for moving obstacle 
       user_rhs((nden-1)*ng+1:nden*ng) = user_rhs((nden-1)*ng+1:nden*ng) * ( 1.0_pr + (1.0_pr/porosity - 1.0_pr)*penal) !brinkman on density
       DO i = 1,dim  !penalize momentum for both brinkman/vp
          user_rhs((nvel(i)-1)*ng+1:nvel(i)*ng) = user_rhs((nvel(i)-1)*ng+1:nvel(i)*ng) -penal*u_integrated(:,nvel(i))/u_integrated(:,nden)/eta_b  !we need velocity
       END DO
       !user_rhs((neng-1)*ng+1:neng*ng) = user_rhs((neng-1)*ng+1:neng*ng) &
       !   - penal/eta_b*(   &             
       !   ((u_integrated(:,neng)-0.5_pr*SUM(u_integrated(:,nvel(1):nvel(dim))**2.0_pr,DIM=2) &
       !   /u_integrated(:,nden))/(cp_in(Nspec)*MW_in(Nspec)-1.0_pr)*MW_in(Nspec)/u_integrated(:,nden)) &
       !   - brinkman_temp) !brinkman on energy
       user_rhs((neng-1)*ng+1:neng*ng) = user_rhs((neng-1)*ng+1:neng*ng) * ( 1.0_pr + (1.0_pr/porosity - 1.0_pr)*penal) !brinkman on density
    ELSE

        !order is set in calling function
        meth_central  = meth + BIASING_NONE     !to allow for constant flux testing
        meth_backward = meth + BIASING_BACKWARD
        meth_forward  = meth + BIASING_FORWARD

        IF (use_dist) THEN   !ERIC: what is this flag for
                CALL user_dist (ng, t+dt, NORMAL=norm, U_TARGET=u_0)
                Spenal = dist
        ELSE
                CALL user_dist (ng, t+dt, DISTANCE=Spenal, NORMAL=norm, U_TARGET=u_0)
        END IF
        !PRINT *, MINVAL(u_0), MAXVAL(u_0)
        Spenal2= Spenal
        Spenal3= Spenal
        !MASK within penal to add diffusion and sink
        delta_Jmx = MINVAL(h(1,1:dim)/2.0_pr**(REAL(j_mx,pr) - 1.0_pr))

        slope = (ATANH( (0.5_pr-10.0_pr**-3.0_pr)/0.5_pr)- ATANH( (-0.5_pr+10.0_pr**-3.0_pr)/0.5_pr))/(REAL(delta_pts,pr)*delta_Jmx)
        offset = REAL(IB_pts,pr)*delta_Jmx - 1.0_pr/slope*ATANH( (-0.5_pr+10.0_pr**-3.0_pr)/0.5_pr )
        Spenal = MAX(0.5_pr - 10.0_pr**-3.0_pr - 0.5_pr*TANH(slope*(Spenal - offset- 0.0_pr*delta_Jmx)) , 0.0_pr )*penal
        !Spenal2 = MAX(0.5_pr - 10.0_pr**-3.0_pr + 0.5_pr*TANH(slope*(Spenal2 - offset- 1.0_pr*delta_Jmx)) , 0.0_pr )*penal  !core
        Spenal2 = MAX(0.5_pr - 10.0_pr**-3.0_pr + 0.5_pr*TANH(slope*(Spenal2 - offset- 0.0_pr*delta_Jmx)) , 0.0_pr )*penal
        !Spenal2 = penal
        Spenal3 = 0.0_pr
        !   Spenal3 = (MAX(0.5_pr - 10.0_pr**-3.0_pr - 0.5_pr*TANH(slope*(Spenal3 - offset- 3.0_pr*delta_Jmx)) , 0.0_pr )  - Spenal)  *penal  !low order convection before transition

    
        !Set slip condition for Euler and no-slip for NS

        IF (no_slip) THEN  !only normal is needed for euler, and all velocity components are penalized as "normal" for NS
           DO idim = 1,dim
              Utan(:, idim) = 0.0_pr
              Unorm (:,idim) = u_integrated(:, nvel(idim))
           END DO
           n_deriv = ne  !only take derivatives of the integrated variables
        ELSE     !ERIC: double check this section for Euler
           Utan(:, 1) = 0.0_pr
           DO idim = 1, dim
              Utan (:, 1) = Utan(:,1)+ u_integrated(:, nvel(idim))*norm(:,idim)
           END DO
           DO idim = 1, dim
              Unorm (:, idim) = Utan(:,1)*norm(:,idim)
           END DO
           DO idim = 1,dim
              Utan(:,idim)=u_integrated(:, nvel(idim))-Unorm(:,idim)
           END DO
           n_deriv = ne+dim  !take derivatives of integrated variables, normal momentum, and tangential momentum
        END IF

        !Set up for vector derivatives
        u_diff(:,1:ne) = u_integrated
        u_diff(:,nvel(1):nvel(dim)) = Unorm(:, 1:dim)   !Only "normal" derivatives needed
        u_diff(:,ne+1:ne+dim) = Utan(:, 1:dim)

        IF(conv_drho) THEN
                CALL c_diff_fast(u_integrated(:,nden), du_diff(1, :, :), d2u_diff(1, :, :), j_lev, ng, LOW_ORDER, 10, 1, 1, 1)  !density derivative for convecting
                u_diff(:,ndrho) = SUM(norm(:,:)*du_diff(1,:,:),DIM=2)*(1.0_pr-penal)+u_integrated(:,ndrho)*penal  !normal density derivative
        END IF

        DO i =1,dim
                brink_var(:,i) = Unorm(:,i)/u_integrated(:,nden)   !Dirichlet penalized variables for Laplacian region
        END DO

        CALL c_diff_fast(brink_var(:,1:dim), du_diff(1:dim, :, :), d2u_brink(1:dim, :, :), j_lev, ng, meth_central, 01, dim, 1, dim)  !first derivative unneeded in this implementation
        IF(.NOT. meth_vect_diff ) THEN
                CALL c_diff_fast(u_diff(:,1:n_deriv), du_diffBD(1:n_deriv, :, :), d2u_diff(1:n_deriv, :, :), j_lev, ng, meth_backward, 10, n_deriv, 1, n_deriv)
                CALL c_diff_fast(u_diff(:,1:n_deriv), du_diffFD(1:n_deriv, :, :), d2u_diff(1:n_deriv, :, :), j_lev, ng, meth_forward, 10, n_deriv, 1, n_deriv)
        ELSE
                meth_vector(1) = meth_backward
                meth_vector(2) = meth_forward
                uh_vector(1:ng,1:n_deriv) = u_diff(1:ng,1:n_deriv)
                uh_vector(ng+1:2*ng,1:n_deriv) = u_diff(1:ng,1:n_deriv)
                CALL c_diff_fast_vector(uh_vector(:,1:n_deriv), du_vector(1:n_deriv, :, :), d2u_vector(1:n_deriv, :, :), j_lev, 2*ng, 2 ,meth_vector, 10, n_deriv, 1, n_deriv)
                du_diffBD(1:n_deriv,:,:) = du_vector(1:n_deriv,1:ng,:)
                du_diffFD(1:n_deriv,:,:) = du_vector(1:n_deriv,1+ng:2*ng,:)
        END IF
        !For Navier-Stokes, zero out tangential velocity derivatives
        IF(no_slip) THEN
           du_diffBD((ne+1):(ne+dim),:,:) = 0.0_pr
           du_diffFD((ne+1):(ne+dim),:,:) = 0.0_pr
        END IF
        
        !Set normal upwind+derivatives
        DO j = 1,ne+dim
           dudn(j,:) = 0.5_pr*SUM( du_diffBD(j, :, :)*(norm(:,:) + ABS(norm(:,:))) + du_diffFD(j, :, :)*(norm(:,:) - ABS(norm(:,:))) ,DIM=2)
        END DO
        
        !set nu_n
        nu_n = visc_factor_d**2.0_pr *delta_Jmx**2.0_pr/eta_b
        !nu_n = visc_factor_d *delta_Jmx
        
        
        ! RHS to zero inside of Brinkman zone
        DO ie = 1,ne
           user_rhs((ie-1)*ng+1:ie*ng) = (1.0_pr-penal)*user_rhs((ie-1)*ng+1:ie*ng) 
        END DO
        
        IF(conv_drho) THEN 
           !Convect normal derivative
           user_rhs((ndrho-1)*ng+1:ndrho*ng) = user_rhs((ndrho-1)*ng+1:ndrho*ng) &   ! cont eq penalization
                - (Spenal)* dudn(ndrho,:)/eta_c &!use low order
                - (Spenal3)*dudn(ndrho,:)/eta_c !use low order
           user_rhs((nden-1)*ng+1:nden*ng) = user_rhs((nden-1)*ng+1:nden*ng) &   ! cont eq penalization
                - Spenal*(-u_integrated(:,ndrho))/eta_c -Spenal3*(-u_integrated(:,ndrho))/eta_c   ! cont eq penalization for density matching, across entire convective zone (high and low order)
           DO ie = 1,dim 
              user_rhs((nvel(ie)-1)*ng+1:nvel(ie)*ng) = user_rhs((nvel(ie)-1)*ng+1:nvel(ie)*ng) &  !density evolution condition
                   - (Spenal+Spenal3)*(-Unorm(:,ie)/ u_integrated(:,nden)*u_integrated(:,ndrho))/eta_c     !density target
           END DO
           user_rhs((neng-1)*ng+1:neng*ng) = user_rhs((neng-1)*ng+1:neng*ng) &
                + Spenal *u_integrated(:,ndrho) * u_integrated(:,neng)/u_integrated(:,nden)/eta_c &  !remove evolutionary drho effects from Temperature slope
                + Spenal3*u_integrated(:,ndrho) * u_integrated(:,neng)/u_integrated(:,nden)/eta_c   !remove evolutionary drho effects from Temperature slope
        END IF
        
        !Continuity Equation
        user_rhs((nden-1)*ng+1:nden*ng) = user_rhs((nden-1)*ng+1:nden*ng) &   ! cont eq penalization
             - Spenal *(dudn(nden,:) )/eta_c &
             - Spenal3*(dudn(nden,:) )/eta_c 
        
        !Momentum Equation
        DO ie = 1,dim 
           user_rhs((nvel(ie)-1)*ng+1:nvel(ie)*ng) = user_rhs((nvel(ie)-1)*ng+1:nvel(ie)*ng) &
                - Spenal * dudn(ne+ie,:)/eta_c  &       !tangential: Euler, ERIC:need density term
                - Spenal3* dudn(ne+ie,:)/eta_c  &       !tangential: Euler
                + penal*nu_n*u_integrated(:,nden)*SUM(d2u_brink(ie, :, :),DIM=2)  !artificial viscosity in non-conservative form
           user_rhs((nvel(ie)-1)*ng+1:nvel(ie)*ng) = user_rhs((nvel(ie)-1)*ng+1:nvel(ie)*ng) &
                - penal*(Unorm(:,ie) - u_integrated(:,nden)*u_0(:,ie))/eta_b !&   !Normal & Navier-Stokes: Dirichlet
           user_rhs((nvel(ie)-1)*ng+1:nvel(ie)*ng) = user_rhs((nvel(ie)-1)*ng+1:nvel(ie)*ng) &  !density evolution condition
                - Spenal *(Unorm(:,ie)/ u_integrated(:,nden) * dudn(nden,:) )/eta_c &
                - Spenal3*(Unorm(:,ie)/ u_integrated(:,nden) * dudn(nden,:) )/eta_c 
        END DO
        
        
        !Energy Equation
        DO ie = 1,dim
           user_rhs((neng-1)*ng+1:neng*ng) = user_rhs((neng-1)*ng+1:neng*ng) &
                + Spenal *(   brink_var(:, ie)*( dudn(nvel(ie),:) - dudn(nden,:)*brink_var(:, ie) )         )/eta_c   &            !do not convect no-slip
                + Spenal3*( brink_var(:, ie)*(dudn(nvel(ie),:) - dudn(nden,:)*brink_var(:, ie) ))/eta_c   &            !do not convect no-slip
                + penal*nu_n*Unorm(:, ie)*SUM(d2u_brink(ie, :, :),DIM=2)  &                               !artificial viscosity in non-conservative form
                - penal*(Unorm(:, ie)*(Unorm(:, ie)/u_integrated(:,nden) - u_0(:,ie)))/eta_b ! &                   ! Dirichlet velocity BC term
           !+ Spenal * 0.5_pr*   Unorm(:,ie)**2.0_pr/u_integrated(:,nden)**2.0_pr *u_integrated(:, ndrho)  /eta_c &   !These terms part of specific total energy added later
           !+ Spenal3* 0.5_pr*   Unorm(:,ie)**2.0_pr/u_integrated(:,nden)**2.0_pr *u_integrated(:, ndrho)  /eta_c
        END DO
        !user_rhs((neng-1)*ng+1:neng*ng) = (1.0_pr-penal)*user_rhs((neng-1)*ng+1:neng*ng) + doNORM * penal*user_rhs((neng-1)*ng+1:neng*ng) !ERIC: is this used?
        
        ! ------- NEED TO CONSISTENTLY UPDATE du to duFD and duBD--------------------
        user_rhs((neng-1)*ng+1:neng*ng) = user_rhs((neng-1)*ng+1:neng*ng) &
             - Spenal * dudn(neng,:)/eta_c &  !convect energy (for Neumann/Robin temperature condition)
             - Spenal3* dudn(neng,:)/eta_c   !convect energy (for Neumann/Robin temperature condition)
        
        !add on Neumann/Robin condition targets
        user_rhs((neng-1)*ng+1:neng*ng) = user_rhs((neng-1)*ng+1:neng*ng) &   ! cont eq penalization
             + Spenal  * u_integrated(:,nden)*(cp_in(Nspec)-1.0_pr/MW_in(Nspec))*cbvp_B/eta_c &
             + Spenal3 * u_integrated(:,nden)*(cp_in(Nspec)-1.0_pr/MW_in(Nspec))*cbvp_B/eta_c 
        
        user_rhs((neng-1)*ng+1:neng*ng) = user_rhs((neng-1)*ng+1:neng*ng) &   ! cont eq penalization
             + Spenal  * (u_integrated(:,neng) - 0.5_pr*SUM(Unorm(:,1:dim)**2.0_pr,DIM=2)/u_integrated(:,nden) )*cbvp_A/eta_c &
             + Spenal3 * (u_integrated(:,neng) - 0.5_pr*SUM(Unorm(:,1:dim)**2.0_pr,DIM=2)/u_integrated(:,nden) )*cbvp_A/eta_c
        
        !Use Conservative form
        
        DO i=1,dim
           vTemp(1:nwlt,i) = Spenal2
        END DO
        
        CALL shift_by(vTemp, v_for, nwlt, 1)
        v_for(1:nwlt,1:dim) = MAX(vTemp, v_for)
        CALL shift_by(vTemp, v_back, nwlt, -1)
        v_back(1:nwlt,1:dim) = MAX(vTemp, v_back)
        
        
        !Diffusion
        
        DO ie = 1,ne
           IF( NOT(no_slip .AND. (ANY(nvel==ie)) .OR. ie==ndrho) ) THEN  !skip velocity components for NS
              !       IF( NOT(NS .AND. (ANY(nvel==ie))) ) THEN  !skip velocity components for NS
              shift = ng*(ie-1)
              !--Go through all Boundary points that are specified
              i_p_face(0) = 1
              DO i=1,dim
                 i_p_face(i) = i_p_face(i-1)*3
              END DO
              DO face_type = 0, 3**dim - 1
                 face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
                 CALL get_all_indices_by_face (face_type, j_lev, nloc, iloc)
                 IF(nloc > 0 ) THEN 
                    DO i = 1, dim
                       !user_rhs(shift+iloc(1:nloc)) = user_rhs(shift+iloc(1:nloc)) + (1.0_pr-ABS(face(i)))*delta_Jmx* visc_factor_n *&
                       !                               Spenal2(iloc(1:nloc))*d2u(ie,iloc(1:nloc),i) ! No diffusion normal to the boundary
                       
                       user_rhs(shift+iloc(1:nloc)) = user_rhs(shift+iloc(1:nloc)) + (1.0_pr-ABS( REAL(face(i),pr) ))*visc_factor_n&
                            *MAX(delta_Jmx/eta_c,1.0_pr)*(v_for(iloc(1:nloc),i)*du_diffFD(ie,iloc(1:nloc),i) - v_back(iloc(1:nloc),i)*du_diffBD(ie,iloc(1:nloc),i) )  
                    END DO
                 END IF
              END DO
           END IF
        END DO
        
        !----------------Add curvature related adjustment for penalization terms
        IF (doCURV) THEN
           ! Calculate additional parameters we'll need to simplify equations-----------------------------------
           DO i = 1, dim
              vel_tan(:, i) = Utan(:, i)/u_integrated(:, nden)                           ! Actual tangential velocity components
              vel_norm(:, i) = Unorm(:, i)/u_integrated(:, nden)                         ! Actual normal velocity components
              vel_void(:, i) = u_integrated(:, nvel(i))/u_integrated(:, nden)            ! Actual velocity components
           END DO
           
           p = (u_integrated(:,neng)-0.5_pr*SUM(u_integrated(:,nvel(1):nvel(dim))**2.0_pr,DIM=2)/u_integrated(:,nden))*(MW_in(Nspec)-1.0_pr) !pressure
           
           A = SUM(Utan**2.0_pr, DIM=2)/p                                                        ! A parameter, see pdf file for details
           DO i = 1, dim
              B(:, i) = A*vel_tan(:, i)                                                     ! B parameter, see pdf file for details
           END DO
           C = SUM(Utan*vel_tan, DIM=2)/(MW_in(Nspec) - 1.0_pr)                                    ! C parameter, see pdf file for details
           F = A*(SUM(vel_tan**2.0_pr + ( 1.0_pr - 2.0_pr*doNORM)*vel_norm**2.0_pr, DIM=2))/2.0_pr    ! F parameter, see pdf file for details
           !-----------------------------------------------------------------------------------------------------
           curv(:) = 0.0_pr
           CALL c_diff_fast(norm, dn, d2n, j_lev, ng, HIGH_ORDER, 10, dim, 1, dim)
           ! Calculating the curvature. PROBABLY NEED TO CREATE SEPARATE SUBROUTINE FOR SIMPLICITY!!!!
           DO i = 1, dim
              curv(:) = curv(:) + dn(i, :, i) ! Calculate the curvature
           END DO
           curv = Spenal*curv/eta_c ! Common term to simplify the equations, S*k/eta
           ! Add curvature to continuity
           user_rhs((nden-1)*ng+1:nden*ng) = user_rhs((nden-1)*ng+1:nden*ng) + curv*A
           ! Add curvature to momentum equation
           DO ie = 1,dim 
              user_rhs((nvel(ie)-1)*ng+1:nvel(ie)*ng) = user_rhs((nvel(ie)-1)*ng+1:nvel(ie)*ng) + curv*B(:, ie)
           END DO
           ! Add curvature to energy equation
           user_rhs((neng-1)*ng+1:neng*ng) = user_rhs((neng-1)*ng+1:neng*ng) + curv*(C + F)
        END IF
        !-----------------------------------------------------------------------
        
     END IF
     
     IF( do_Adp_Eps_Spatial_Evol ) & 
          CALL user_rhs__VT (user_rhs, u_integrated, du_diff(1:ne,:,:), d2u_diff(1:ne,:,:)) ! Cf: OPTIONAL
     
     
   END SUBROUTINE Brinkman_rhs
   
   
   ! find Jacobian of Right Hand Side of the problem
   SUBROUTINE  Brinkman_Drhs (user_Drhs, ulc, u_prev, meth)
     USE penalization
     USE user_case_vars
     IMPLICIT NONE
     INTEGER, INTENT(IN) :: meth
     REAL (pr), DIMENSION (ng,ne), INTENT(IN) :: ulc, u_prev
     REAL (pr), DIMENSION (ng*ne), INTENT(INOUT) :: user_Drhs
     REAL (pr), DIMENSION (ng,dim)     :: Unorm, Unorm_prev, Utan, Utan_prev, norm
     REAL (pr), DIMENSION (ng,2*dim)     :: brink_var     !for non-stationary obstacle
     REAL (pr), DIMENSION (dim*2,ng,dim) :: d2u_brink
     
     REAL (pr), DIMENSION (ne*2,ng,dim) :: du_pass
     REAL (pr), DIMENSION (2*(ne+dim),ng) :: dudn
     REAL (pr), DIMENSION (ng,dim)     :: u_0     !for non-stationary obstacle
     
     REAL (pr), DIMENSION (ng) :: Spenal,Spenal2,Spenal3
     REAL (pr) :: slope, offset
     
     REAL (pr) :: delta_Jmx, nu_n
     REAL (pr) :: cbvp_t
     
     INTEGER :: ie, shift,i,j, idim
     INTEGER :: meth_central, meth_backward, meth_forward  
     
     REAL (pr), DIMENSION (ng,2*(ne+dim)) :: u_diff
     REAL (pr), DIMENSION (2*(ne+dim),ng,dim) :: du_diff, du_diffFD, du_diffBD, d2u_diff
     INTEGER :: n_deriv
     
     REAL (pr), DIMENSION(ng) :: curv                 ! curvature
     REAL (pr), DIMENSION (dim, ng, dim) :: dn, d2n   ! derivative of normal
     
     REAL (pr), DIMENSION (ng) :: p, p_prev                   ! pressure, for curvature adjustment if applicable
     REAL (pr), DIMENSION(ng, dim) :: vel_tan, vel_norm, vel_void
     REAL (pr), DIMENSION(ng, dim) :: vel_tan_prev, vel_norm_prev, vel_void_prev
     REAL (pr), DIMENSION (ng)     :: A, C, F
     REAL (pr), DIMENSION (ng)     :: A_prev, C_prev, F_prev
     REAL (pr), DIMENSION(ng, dim) :: B, B_prev
     
     INTEGER :: prev_shift
     
     INTEGER :: face_type, nloc
     INTEGER, DIMENSION(0:dim) :: i_p_face
     INTEGER, DIMENSION(dim) :: face
     INTEGER, DIMENSION(nwlt) :: iloc
     
     REAL (pr), DIMENSION(ng, dim) :: v_back, v_for, vTemp
     
     REAL (pr), DIMENSION(2*ng,2*(ne+2*dim)) ::uh_vector
     REAL (pr), DIMENSION(2*(ne+2*dim),2*ng,dim) ::du_vector, d2u_vector
     INTEGER, DIMENSION(2) :: meth_vector

    IF(t.LE.1.0_pr) THEN
       cbvp_t = cbvp_B*t/1.0_pr
    ELSE
       cbvp_t = cbvp_B
    END IF

    IF(brinkman_penal) THEN   !ERIC: not set up for moving obstacle 
       user_Drhs((nden-1)*ng+1:nden*ng) = user_Drhs((nden-1)*ng+1:nden*ng) * ( 1.0_pr + (1.0_pr/porosity - 1.0_pr)*penal) !brinkman on density
       DO i = 1,dim  !penalize momentum for both brinkman/vp
          user_Drhs((nvel(i)-1)*ng+1:nvel(i)*ng) = user_Drhs((nvel(i)-1)*ng+1:nvel(i)*ng) &
             -penal*ulc(:,nvel(i))/u_prev(:,nden)/eta_b & 
             +penal*u_prev(:,nvel(i))/u_prev(:,nden)**2.0_pr*ulc(:,nden)/eta_b 
       END DO
       !user_Drhs((neng-1)*ng+1:neng*ng) = user_Drhs((neng-1)*ng+1:neng*ng) &
       !   - penal/eta_b*(   &             
       !   (ulc(:,neng)-0.5_pr*SUM(2.0_pr*u_prev(:,nvel(1):nvel(dim))*ulc(:,nvel(1):nvel(dim)),DIM=2) &
       !   /u_prev(:,nden)+0.5_pr*SUM(u_prev(:,nvel(1):nvel(dim))**2.0_pr,DIM=2) &
       !   /u_prev(:,nden)**2.0_pr*ulc(:,nden))/(cp_in(Nspec)*MW_in(Nspec)-1.0_pr)*MW_in(Nspec)/u_prev(:,nden) &
       !   -(u_prev(:,neng)-0.5_pr*SUM(u_prev(:,nvel(1):nvel(dim))**2.0_pr,DIM=2) &
       !   /u_prev(:,nden))/(cp_in(Nspec)*MW_in(Nspec)-1.0_pr)*MW_in(Nspec)/u_prev(:,nden)**2.0_pr*ulc(:,nden) &
       !   ) !brinkman on energy

       user_Drhs((neng-1)*ng+1:neng*ng) = user_Drhs((neng-1)*ng+1:neng*ng) * ( 1.0_pr + (1.0_pr/porosity - 1.0_pr)*penal) !brinkman on density
    ELSE
       
       
       meth_central  = meth + BIASING_NONE
       meth_backward = meth + BIASING_BACKWARD
       meth_forward  = meth + BIASING_FORWARD
       
       !PRINT *, "Drhs, t:", t
       
       IF (use_dist) THEN
          CALL user_dist (ng, t+dt, NORMAL=norm, U_TARGET=u_0)
          Spenal = dist
       ELSE
          CALL user_dist (ng, t+dt, DISTANCE=Spenal, NORMAL=norm, U_TARGET=u_0)
       END IF
       Spenal2= Spenal
       Spenal3= Spenal
       !MASK within penal to add diffusion and sink
       delta_Jmx = MINVAL(h(1,1:dim)/2.0_pr**(REAL(j_mx,pr) - 1.0_pr))
       
       slope = (ATANH( (0.5_pr-10.0_pr**-3.0_pr)/0.5_pr)- ATANH( (-0.5_pr+10.0_pr**-3.0_pr)/0.5_pr))/(REAL(delta_pts,pr)*delta_Jmx)
       offset = REAL(IB_pts,pr)*delta_Jmx - 1.0_pr/slope*ATANH( (-0.5_pr+10.0_pr**-3.0_pr)/0.5_pr )
       Spenal = MAX(0.5_pr - 10.0_pr**-3.0_pr - 0.5_pr*TANH(slope*(Spenal - offset- 0.0_pr*delta_Jmx)) , 0.0_pr )*penal
       !Spenal2 = MAX(0.5_pr - 10.0_pr**-3.0_pr + 0.5_pr*TANH(slope*(Spenal2 - offset- 1.0_pr*delta_Jmx)) , 0.0_pr )*penal  !core
       Spenal2 = MAX(0.5_pr - 10.0_pr**-3.0_pr + 0.5_pr*TANH(slope*(Spenal2 - offset- 0.0_pr*delta_Jmx)) , 0.0_pr )*penal
       !Spenal2 = penal
       Spenal3 = 0.0_pr
       !   Spenal3 = (MAX(0.5_pr - 10.0_pr**-3.0_pr - 0.5_pr*TANH(slope*(Spenal3 - offset- 3.0_pr*delta_Jmx)) , 0.0_pr )  - Spenal)  *penal  !low order convection before transition
       
       
       IF (no_slip) THEN
          DO idim =1,dim
             Utan(:, idim) = 0.0_pr
             Utan_prev(:, idim) = 0.0_pr
             Unorm (:,idim) = ulc(:, nvel(idim))  
             Unorm_prev(:, idim) = u_prev(:, nvel(idim))
          END DO
       ELSE  !ERIC: double check this for Euler Eqs
          Utan(:, 1) = 0.0_pr
          Utan_prev(:, 1) = 0.0_pr
          DO idim = 1, dim
             Utan(:, 1) = Utan(:,1)+ ulc(:, nvel(idim))*norm(:,idim)
             Utan_prev(:, 1) = Utan_prev(:,1)+ u_prev(:, nvel(idim))*norm(:,idim)
          END DO
          DO idim = 1, dim
             Unorm(:, idim) = Utan(:,1)*norm(:,idim)
             Unorm_prev(:, idim) = Utan_prev(:,1)*norm(:,idim)
          END DO
          DO idim = 1,dim
             Utan(:,idim) = ulc(:, nvel(idim))-Unorm(:,idim)
             Utan_prev(:,idim) = u_prev(:, nvel(idim)) - Unorm_prev(:,idim)
          END DO
       END IF
       n_deriv = (ne+dim)*2 ! taking all of the derivatives because all perterbations are listed before previous  (maintain shift)
       
       
       !Take all high order derivatives at the same time -------
       !get normal derivatives
       prev_shift = ne+dim
       !perturbations
       u_diff(:,1:ne) = ulc
       u_diff(:,nvel(1):nvel(dim)) = Unorm(:, 1:dim)
       u_diff(:,(ne+1):(ne+dim)) = Utan(:, 1:dim)
       !pervious
       u_diff(:,1+prev_shift:ne+prev_shift) = u_prev
       u_diff(:,nvel(1)+prev_shift:nvel(dim)+prev_shift) = Unorm_prev(:, 1:dim)
       u_diff(:,(ne+1)+prev_shift:(ne+dim)+prev_shift) = Utan_prev(:, 1:dim)
       
       brink_var(:,1) = ulc(:,nden)  !reuse variable
       brink_var(:,2) = u_prev(:,nden)  !reuse variable
       CALL c_diff_fast(brink_var(:,1:2), du_diff(1:2, :, :), d2u_diff(1:2, :, :), j_lev, ng, LOW_ORDER, 10, 2, 1, 2)  !density derivative for convecting
       !PRINT *, 'u_ndrho1:', MINVAL(u_diff(:,1)*penal), MAXVAL(u_diff(:,1)*penal)
      
       IF (conv_drho) THEN 
          u_diff(:,ndrho)            = SUM(norm(:,:)*du_diff(1,:,:),DIM=2)*(1.0_pr-penal)+   ulc(:,ndrho)*penal  !normal density derivative
          u_diff(:,ndrho+prev_shift) = SUM(norm(:,:)*du_diff(2,:,:),DIM=2)*(1.0_pr-penal)+u_prev(:,ndrho)*penal  !normal density derivative
       END IF
       
       DO i=1,dim
          brink_var(:,i)       = (Unorm(:,i)*u_prev(:,nden)-Unorm_prev(:,i)*ulc(:,nden))/u_prev(:,nden)**2.0_pr
          brink_var(:,i+dim)   = Unorm_prev(:,i)/u_prev(:,nden)
       END DO
       
       !PRINT *, 'ne', ne, 'ndrho', neng, 'n_deriv', n_deriv, 'prev_shift', prev_shift
       !PRINT *, 'u_ndrho2:', MINVAL(u_diff(:,ndrho)*penal), MAXVAL(u_diff(:,ndrho)*penal)
       
       
       CALL    c_diff_fast(brink_var(:,1:2*dim), du_diff(1:2*dim, :, :), d2u_brink(1:2*dim, :, :), j_lev, ng, meth_central, 01, 2*dim, 1, 2*dim)
       IF(.NOT. meth_vect_diff ) THEN
          CALL c_diff_fast(u_diff(:,1:n_deriv), du_diffBD(1:n_deriv, :, :), d2u_diff(1:n_deriv, :, :), j_lev, ng, meth_backward, 10, n_deriv, 1, n_deriv)
          CALL c_diff_fast(u_diff(:,1:n_deriv), du_diffFD(1:n_deriv, :, :), d2u_diff(1:n_deriv, :, :), j_lev, ng, meth_forward,  10, n_deriv, 1, n_deriv)
       ELSE
          meth_vector(1) = meth_backward
          meth_vector(2) = meth_forward
          uh_vector(1:ng,1:n_deriv) = u_diff(1:ng,1:n_deriv)
          uh_vector(ng+1:2*ng,1:n_deriv) = u_diff(1:ng,1:n_deriv)
          CALL c_diff_fast_vector(uh_vector(:,1:n_deriv), du_vector(1:n_deriv, :, :), d2u_vector(1:n_deriv, :, :), j_lev, 2*ng, 2 ,meth_vector, 10, n_deriv, 1, n_deriv)
          du_diffBD(1:n_deriv,:,:) = du_vector(1:n_deriv,1:ng,:)
          du_diffFD(1:n_deriv,:,:) = du_vector(1:n_deriv,1+ng:2*ng,:)
       END IF
       IF(no_slip) THEN  !verify that tangential derivatives are zero
          du_diffBD((ne+1):(ne+dim),:,:) = 0.0_pr
          du_diffFD((ne+1):(ne+dim),:,:) = 0.0_pr
          du_diffBD((ne+1)+prev_shift:(ne+dim)+prev_shift,:,:) = 0.0_pr
          du_diffFD((ne+1)+prev_shift:(ne+dim)+prev_shift,:,:) = 0.0_pr
       END IF
       
       !PRINT *, 'duFD_drho:', MINVAL(du_diffFD(neng,:,1)*penal), MAXVAL(du_diffFD(neng,:,1)*penal)
       !PRINT *, 'duBD_drho:', MINVAL(du_diffBD(neng,:,1)*penal), MAXVAL(du_diffBD(neng,:,1)*penal)
       
       
       !Set normal derivatives upwind
       DO j = 1,n_deriv
          dudn(j,:) = 0.5_pr*SUM( du_diffBD(j, :, :)*(norm(:,:) + ABS(norm(:,:))) + du_diffFD(j, :, :)*(norm(:,:) - ABS(norm(:,:))) ,DIM=2)
       END DO
       
       !PRINT *, 'dudn_ndrho:', MINVAL(dudn(ndrho,:)*penal), MAXVAL(dudn(ndrho,:)*penal)
       
       
       !set nu_n
       nu_n = visc_factor_d**2.0_pr *delta_Jmx**2.0_pr/eta_b
       !nu_n = visc_factor_d *delta_Jmx
       
       ! sets NN RHS to zero inside of Brinkman zone
       DO ie = 1,ne
          user_Drhs((ie-1)*ng+1:ie*ng) = (1.0_pr-penal)*user_Drhs((ie-1)*ng+1:ie*ng) 
       END DO
       
       !PRINT *, 'DEN_1:', MINVAL(user_Drhs((nden-1)*ng+1:nden*ng)), MAXVAL(user_Drhs((nden-1)*ng+1:nden*ng))
       !PRINT *, 'DRHO_1:', MINVAL(user_Drhs((ndrho-1)*ng+1:ndrho*ng)), MAXVAL(user_Drhs((ndrho-1)*ng+1:ndrho*ng))
       
       !PRINT *, 'DRHO2:', MINVAL(user_Drhs((ndrho-1)*ng+1:ndrho*ng)*penal), MAXVAL(user_Drhs((ndrho-1)*ng+1:ndrho*ng)*penal)
       
       IF(conv_drho) THEN 
          !Convect normal derivative
          user_Drhs((ndrho-1)*ng+1:ndrho*ng) = user_Drhs((ndrho-1)*ng+1:ndrho*ng) &   ! cont eq penalization
               - (Spenal)* dudn(ndrho,:)/eta_c &!use low order
               - (Spenal3)*dudn(ndrho,:)/eta_c !use low order
          user_Drhs((nden-1)*ng+1:nden*ng) = user_Drhs((nden-1)*ng+1:nden*ng)  &  ! cont eq penalization
               - Spenal *(-ulc(:,ndrho))/eta_c - Spenal3 *(-ulc(:,ndrho))/eta_c   ! cont eq penalization for density matching, across entire convective zone (high and low order)
          DO ie = 1,dim 
             user_Drhs((nvel(ie)-1)*ng+1:nvel(ie)*ng) = user_Drhs((nvel(ie)-1)*ng+1:nvel(ie)*ng) &  !density evolution condition
                  - (Spenal+Spenal3)*(-brink_var(:,ie+dim) *ulc(:,ndrho)     )/eta_c   &   !density target
                  - (Spenal+Spenal3)*(-brink_var(:,ie)     *u_prev(:,ndrho))/eta_c       !density target
          END DO
          user_Drhs((neng-1)*ng+1:neng*ng) = user_Drhs((neng-1)*ng+1:neng*ng) &
               + Spenal *ulc(:,ndrho)* u_prev(:,neng)/u_prev(:,nden)/eta_c &  !remove evolutionary drho effects from Temperature slope
               + Spenal *u_prev(:,ndrho) * (ulc(:,neng)*u_prev(:,nden)-u_prev(:,neng)*ulc(:,nden) )/u_prev(:,nden)**2.0_pr/eta_c & !remove evolutionary drho effects from Temperature slope
               + Spenal3*ulc(:,ndrho)* u_prev(:,neng)/u_prev(:,nden)/eta_c &  !remove evolutionary drho effects from Temperature slope
               + Spenal3*u_prev(:,ndrho) * (ulc(:,neng)*u_prev(:,nden)-u_prev(:,neng)*ulc(:,nden) )/u_prev(:,nden)**2.0_pr/eta_c  !remove evolutionary drho effects from Temperature slope
       END IF
       
       !Continuity Equation
       user_Drhs((nden-1)*ng+1:nden*ng) = user_Drhs((nden-1)*ng+1:nden*ng)  &  ! cont eq penalization
            - Spenal *  (dudn(nden,:))/eta_c &
            - Spenal3*  (dudn(nden,:))/eta_c 
       
       !PRINT *, 'DEN_2:', MINVAL(user_Drhs((nden-1)*ng+1:nden*ng)), MAXVAL(user_Drhs((nden-1)*ng+1:nden*ng))
       !PRINT *, 'DRHO_2:', MINVAL(user_Drhs((ndrho-1)*ng+1:ndrho*ng)), MAXVAL(user_Drhs((ndrho-1)*ng+1:ndrho*ng))
       
       !Momentum Equation
       DO ie = 1,dim 
          user_Drhs((nvel(ie)-1)*ng+1:nvel(ie)*ng) = user_Drhs((nvel(ie)-1)*ng+1:nvel(ie)*ng) &
               - Spenal * dudn(ne+ie,:)/eta_c  & !tangential :Euler
               - Spenal3* dudn(ne+ie,:)/eta_c  & !tangential: Euler
               + penal*nu_n *ulc(:,nden)     *SUM(d2u_brink(dim+ie, :, :),DIM=2) & !artificial viscosity in non-conservative form
               + penal*nu_n *u_prev(:,nden)*SUM(d2u_brink(ie, :, :)    ,DIM=2)   !artificial viscosity in non-conservative form
          user_Drhs((nvel(ie)-1)*ng+1:nvel(ie)*ng) = user_Drhs((nvel(ie)-1)*ng+1:nvel(ie)*ng) &
               - penal*(Unorm(:,ie) - ulc(:,nden)*u_0(:,ie))/eta_b !&   !Normal velocity & Navier-Stokes
          user_Drhs((nvel(ie)-1)*ng+1:nvel(ie)*ng) = user_Drhs((nvel(ie)-1)*ng+1:nvel(ie)*ng) &  !density evolution condition
               - Spenal *( brink_var(:,ie+dim) * dudn(nden,:)            )/eta_c &
               - Spenal *( brink_var(:,ie)     * dudn(nden+prev_shift,:) )/eta_c &
               - Spenal3*( brink_var(:,ie+dim) * dudn(nden,:)            )/eta_c &
               - Spenal3*( brink_var(:,ie)     * dudn(nden+prev_shift,:) )/eta_c 
       END DO
       !PRINT *, 'ENG1:', MINVAL(user_Drhs((neng-1)*ng+1:neng*ng)*penal), MAXVAL(user_Drhs((neng-1)*ng+1:neng*ng)*penal)
       
       !Energy Equation
       DO ie = 1,dim
          user_Drhs((neng-1)*ng+1:neng*ng) = user_Drhs((neng-1)*ng+1:neng*ng)  &
               + Spenal *( brink_var(:, ie+dim) *(dudn(nvel(ie),:)            - dudn(nden+prev_shift,:)*brink_var(:, ie) - dudn(nden,:)*brink_var(:, ie+dim)) )/eta_c   &    !do not convect no-slip
               + Spenal *( brink_var(:, ie)     *(dudn(nvel(ie)+prev_shift,:) - dudn(nden+prev_shift,:)*brink_var(:, ie+dim)                                ) )/eta_c   &   !do not convect no-slip
               + Spenal3*( brink_var(:, ie+dim) *(dudn(nvel(ie),:)            - dudn(nden+prev_shift,:)*brink_var(:, ie) - dudn(nden,:)*brink_var(:, ie+dim)) )/eta_c   &     
               + Spenal3*( brink_var(:, ie)     *(dudn(nvel(ie)+prev_shift,:) - dudn(nden+prev_shift,:)*brink_var(:, ie+dim)                                ) )/eta_c   &            
               + penal*nu_n*( Unorm(:, ie)*SUM(d2u_brink(ie+dim, :, :),DIM=2) +  Unorm_prev(:, ie)*SUM(d2u_brink(ie, :, :),DIM=2) )   &          !artificial viscosity in non-conservative form
               - penal*(     Unorm_prev(:, ie)*brink_var(:, ie) + Unorm(:, ie)*brink_var(:, ie+dim) - Unorm(:, ie)*u_0(:,ie)  )/eta_b ! &                   ! velocity BC term
       END DO
       ! user_Drhs((neng-1)*ng+1:neng*ng) = (1.0_pr-penal)*user_Drhs((neng-1)*ng+1:neng*ng) + doNORM * penal*user_Drhs((neng-1)*ng+1:neng*ng) !ERIC: is this used?
       !PRINT *, 'ENG1:', MINVAL(user_Drhs((neng-1)*ng+1:neng*ng)*penal), MAXVAL(user_Drhs((neng-1)*ng+1:neng*ng)*penal)
       
       user_Drhs((neng-1)*ng+1:neng*ng) = user_Drhs((neng-1)*ng+1:neng*ng) &
            - Spenal * dudn(neng,:)/eta_c   !convect energy (for Neumann/Robin temperature condition)
       !PRINT *, 'ENG2:', MINVAL(user_Drhs((neng-1)*ng+1:neng*ng)*penal), MAXVAL(user_Drhs((neng-1)*ng+1:neng*ng)*penal)
       user_Drhs((neng-1)*ng+1:neng*ng) = user_Drhs((neng-1)*ng+1:neng*ng) &
            - Spenal3* dudn(neng,:)/eta_c 
       !PRINT *, 'ENG3:', MINVAL(user_Drhs((neng-1)*ng+1:neng*ng)*penal), MAXVAL(user_Drhs((neng-1)*ng+1:neng*ng)*penal)
       
       !PRINT *, 'ENG4:', MINVAL(user_Drhs((neng-1)*ng+1:neng*ng)*penal), MAXVAL(user_Drhs((neng-1)*ng+1:neng*ng)*penal)
       
       !add on Neumann/Robin condition targets
       user_Drhs((neng-1)*ng+1:neng*ng) = user_Drhs((neng-1)*ng+1:neng*ng) &   ! cont eq penalization
            + Spenal  * ulc(:,nden)*(cp_in(Nspec)-1.0_pr/MW_in(Nspec))*cbvp_B/eta_c &
            + Spenal3 * ulc(:,nden)*(cp_in(Nspec)-1.0_pr/MW_in(Nspec))*cbvp_B/eta_c 
       user_Drhs((neng-1)*ng+1:neng*ng) = user_Drhs((neng-1)*ng+1:neng*ng) &   ! cont eq penalization
            + Spenal  * (ulc(:,neng) - 0.5_pr*SUM(Unorm_prev(:,1:dim)*brink_var(:,1:dim),DIM=2) - 0.5_pr*SUM(Unorm(:,1:dim)*brink_var(:,1+dim:2*dim),DIM=2 ))*cbvp_A/eta_c &
            + Spenal3 * (ulc(:,neng) - 0.5_pr*SUM(Unorm_prev(:,1:dim)*brink_var(:,1:dim),DIM=2) - 0.5_pr*SUM(Unorm(:,1:dim)*brink_var(:,1+dim:2*dim),DIM=2 ))*cbvp_A/eta_c
       
       
       !Use conservative form
       
       DO i=1,dim
          vTemp(1:nwlt,i) = Spenal2   
       END DO
       
       CALL shift_by(vTemp, v_for, nwlt, 1)
       v_for(1:nwlt,1:dim) = MAX(vTemp, v_for)
       CALL shift_by(vTemp, v_back, nwlt, -1)
       v_back(1:nwlt,1:dim) = MAX(vTemp, v_back)   !
       
       !Diffusion
       DO ie = 1,ne 
          IF( NOT(no_slip .AND. (ANY(nvel==ie)) .OR. ie==ndrho) ) THEN  !skip velocity components for NS
             !       IF( NOT(NS .AND. (ANY(nvel==ie)) ) ) THEN  !skip velocity components for NS
             shift = ng*(ie-1)
             !--Go through all Boundary points that are specified
             i_p_face(0) = 1
             DO i=1,dim
                i_p_face(i) = i_p_face(i-1)*3
             END DO
             DO face_type = 0, 3**dim - 1
                face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
                CALL get_all_indices_by_face (face_type, j_lev, nloc, iloc)
                IF(nloc > 0 ) THEN 
                   DO i = 1, dim
                      !user_Drhs(shift+iloc(1:nloc)) = user_Drhs(shift+iloc(1:nloc)) + (1.0_pr-ABS(face(i)))*delta_Jmx* visc_factor_n*&
                      !                               Spenal2(iloc(1:nloc))*d2u(ie,iloc(1:nloc),i) ! No diffusion normal to the boundary
                      
                      user_Drhs(shift+iloc(1:nloc)) = user_Drhs(shift+iloc(1:nloc)) + (1.0_pr-ABS( REAL(face(i),pr) )) *visc_factor_n&
                           !*MAX(9.0_pr*delta_Jmx/eta_b,0.1_pr/eta_c,1.0_pr)*(v_for(iloc(1:nloc),i)*duFD(ie,iloc(1:nloc),i) - v_back(iloc(1:nloc),i)*duBD(ie,iloc(1:nloc),i) )
                           *MAX(delta_Jmx/eta_c,1.0_pr)*(v_for(iloc(1:nloc),i)*du_diffFD(ie,iloc(1:nloc),i) - v_back(iloc(1:nloc),i)*du_diffBD(ie,iloc(1:nloc),i) )
                   END DO
                END IF
             END DO
          END IF
       END DO
       
       !----------------Add curvature related adjustment for penalization terms
       IF (doCURV) THEN
          ! Calculate additional parameters we'll need to simplify equations-----------------------------------
          ! prev values
          DO i = 1, dim
             vel_tan_prev(:, i) = Utan_prev(:, i)/u_prev(:, nden)     ! Actual tangential velocity components
             vel_norm_prev(:, i) = Unorm_prev(:, i)/u_prev(:, nden)   ! Actual normal velocity components
             vel_void_prev(:, i) = u_prev(:, nvel(i))/u_prev(:, nden) ! Actual velocity components
          END DO
          
          p_prev = (u_prev(:,neng)-0.5_pr*SUM(u_prev(:,nvel(1):nvel(dim))**2.0_pr,DIM=2)/u_prev(:,nden))*(MW_in(Nspec)-1.0_pr) !pressure
          
          A_prev = SUM(Utan_prev**2.0_pr, DIM=2)/p_prev                                       ! A parameter, see pdf file for details
          DO i = 1, dim
             B_prev(:, i) = A_prev*vel_tan_prev(:, i)                                    ! B parameter, see pdf file for details
          END DO
          C_prev = SUM(Utan_prev*vel_tan, DIM=2)/(MW_in(Nspec) - 1.0_pr)                        ! C parameter, see pdf file for details
          F_prev = A_prev*(SUM(vel_tan_prev**2.0_pr - doNORM*vel_norm_prev**2.0_pr, DIM=2))/2.0_pr ! F parameter, see pdf file for details
          ! perturbations
          DO i = 1, dim
             vel_tan(:, i) = (Utan(:, i)*u_prev(:, nden) - Utan_prev(:, i)*ulc(:, nden))/u_prev(:, nden)**2.0_pr
             vel_norm(:, i) = (Unorm(:, i)*u_prev(:, nden) - Unorm_prev(:, i)*ulc(:, nden))/u_prev(:, nden)**2.0_pr
             vel_void(:, i) = (ulc(:, nvel(i))*u_prev(:, nden) - u_prev(:, nvel(i))*ulc(:, nden))/u_prev(:, nden)**2.0_pr
          END DO
          p = (ulc(:, neng) - 0.5_pr*(SUM(ulc(:, nvel(1):nvel(dim))*vel_void_prev + u_prev(:, nvel(1):nvel(dim))*vel_void, DIM=2)))*(MW_in(Nspec) - 1.0_pr)
          A = (2.0_pr*SUM(u_prev(:, nvel(1):nvel(dim))*ulc(:, nvel(1):nvel(dim)), DIM=2)*p_prev - SUM(u_prev(:, nvel(1):nvel(dim))*u_prev(:, nvel(1):nvel(dim)), DIM=2)*p)/p_prev**2.0_pr
          DO i = 1, dim
             B(:, i) = A*vel_tan_prev(:, i) + A_prev*vel_tan(:, i)
          END DO
          C = SUM(ulc(:, nvel(1):nvel(dim))*vel_tan_prev + u_prev(:, nvel(1):nvel(dim))*vel_tan, DIM=2)/(MW_in(Nspec) - 1.0_pr)
          F = A*SUM(vel_tan_prev**2.0_pr +(1.0_pr -2.0_pr*doNORM)*vel_norm_prev**2.0_pr,DIM=2)/2.0_pr + A_prev*SUM(vel_tan_prev*vel_tan + (1.0_pr -2.0_pr*doNORM)*vel_norm_prev*vel_norm,DIM=2)
          !-----------------------------------------------------------------------------------------------------
          curv(:) = 0.0_pr
          CALL c_diff_fast(norm, dn, d2n, j_lev, ng, meth_central, 10, dim, 1, dim)
          ! Calculating the curvature. PROBABLY NEED TO CREATE SEPARATE SUBROUTINE FOR SIMPLICITY!!!!
          DO i = 1, dim
             curv(:) = curv(:) + dn(i, :, i) ! Calculate the curvature
          END DO
          curv = Spenal*curv/eta_c ! Common term to simplify the equations, S*k/eta
          ! Add curvature to continuity
          user_Drhs((nden-1)*ng+1:nden*ng) = user_Drhs((nden-1)*ng+1:nden*ng) + curv*A
          ! Add curvature to momentum equation
          DO ie = 1,dim 
             user_Drhs((nvel(ie)-1)*ng+1:nvel(ie)*ng) = user_Drhs((nvel(ie)-1)*ng+1:nvel(ie)*ng) + curv*B(:, ie)
          END DO
          ! Add curvature to energy equation
          user_Drhs((neng-1)*ng+1:neng*ng) = user_Drhs((neng-1)*ng+1:neng*ng) + curv*(C + F)
       END IF
       !-----------------------------------------------------------------------
    END IF
    
    IF( do_Adp_Eps_Spatial_Evol ) THEN
       du_pass(1:ne,:,:) = du_diff(1:ne,:,:)
       du_pass(ne+1:2*ne,:,:) = du_diff(ne+2*dim+1:2*ne+2*dim,:,:)
       CALL user_Drhs__VT (user_Drhs, ulc, u_prev, du_pass(1:2*ne,:,:), d2u_diff(1:ne,:,:), meth)
    END IF
    
    !PRINT *, 'DEN:', MINVAL(user_Drhs((nden-1)*ng+1:nden*ng)*penal), MAXVAL(user_Drhs((nden-1)*ng+1:nden*ng)*penal)
    !PRINT *, 'VEL:', MINVAL(user_Drhs((nvel(1)-1)*ng+1:nvel(1)*ng)*penal), MAXVAL(user_Drhs((nvel(1)-1)*ng+1:nvel(1)*ng)*penal)
    !PRINT *, 'ENG:', MINVAL(user_Drhs((neng-1)*ng+1:neng*ng)*penal), MAXVAL(user_Drhs((neng-1)*ng+1:neng*ng)*penal)
    !PRINT *, 'DRHO:', MINVAL(user_Drhs((ndrho-1)*ng+1:ndrho*ng)*penal), MAXVAL(user_Drhs((ndrho-1)*ng+1:ndrho*ng)*penal)
    !PRINT *, ' '
END SUBROUTINE Brinkman_Drhs

  !
  ! Uses u_prev_timestep, which is global flat (1D ) array set in time_step_cn()
  !
  SUBROUTINE  Brinkman_Drhs_diag (user_Drhs_diag, u_prev, meth)
    USE user_case_vars
    USE parallel
    IMPLICIT NONE
    REAL (pr), DIMENSION (ng,ne), INTENT(IN) :: u_prev
    INTEGER, INTENT (IN) :: meth
    INTEGER :: meth_central, meth_backward, meth_forward
    REAL (pr), DIMENSION (n), INTENT(INOUT) :: user_Drhs_diag
    INTEGER :: ie, shift,i,j,idim
    REAL (pr), DIMENSION (ng,dim) :: du_diagF, d2u_diagF
    REAL (pr), DIMENSION (ng,dim) :: du_diagB, d2u_diagB
    REAL (pr), DIMENSION (ng,dim) :: du_diagC, d2u_diagC
    REAL (pr), DIMENSION (ne,ng,dim) :: du, d2u, duF, d2uF,duB, d2uB
    REAL (pr), DIMENSION (ne,ng) :: dudn, d2udn_new
!
! User defined variables
!
    REAL (pr), DIMENSION (ng,dim)     ::  Unorm_prev, Utan_prev, norm
    REAL (pr), DIMENSION (ng) :: dudn_diag

    REAL (pr), DIMENSION (ng,dim)     :: u_0     !for non-stationary obstacle

    REAL (pr), DIMENSION (ng) :: Spenal,Spenal2,Spenal3
    REAL (pr) :: slope, offset

    REAL (pr) :: delta_Jmx, nu_n, cbvp_t
    REAL (pr), DIMENSION(ng, dim) :: v_back, v_for, vTemp


!    REAL (pr), DIMENSION(ng, dim) :: vel_tan, vel_norm, vel_void
!    REAL (pr), DIMENSION(ng, dim) :: vel_tan_prev, vel_norm_prev, vel_void_prev
!    REAL (pr), DIMENSION (ng)     :: A, C, F
!    REAL (pr), DIMENSION (ng)     :: A_prev, C_prev, F_prev
!    REAL (pr), DIMENSION(ng, dim) :: B, B_prev


    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc
    !ERIC: not set up for Euler

    IF(t.LE.1.0_pr) THEN
       cbvp_t = cbvp_B*t/1.0_pr
    ELSE
       cbvp_t = cbvp_B
    END IF

    IF(brinkman_penal) THEN   !ERIC: not set up for moving obstacle 
       user_Drhs_diag((nden-1)*ng+1:nden*ng) = user_Drhs_diag((nden-1)*ng+1:nden*ng) * ( 1.0_pr + (1.0_pr/porosity - 1.0_pr)*penal) !brinkman on density
       DO i = 1,dim  !penalize momentum for both brinkman/vp
          user_Drhs_diag((nvel(i)-1)*ng+1:nvel(i)*ng) = user_Drhs_diag((nvel(i)-1)*ng+1:nvel(i)*ng) &
             -penal/u_prev(:,nden)/eta_b  
       END DO
       !user_Drhs((neng-1)*ng+1:neng*ng) = user_Drhs((neng-1)*ng+1:neng*ng) &
       !   - penal/eta_b*(   &             
       !   (ulc_prev(:,neng)-0.5_pr*SUM(2.0_pr*u_prev(:,nvel(1):nvel(dim))*ulc(:,nvel(1):nvel(dim)),DIM=2) &
       !   /u_prev(:,nden)+0.5_pr*SUM(u_prev(:,nvel(1):nvel(dim))**2.0_pr,DIM=2) &
       !   /u_prev(:,nden)**2.0_pr*ulc(:,nden))/(cp_in(Nspec)*MW_in(Nspec)-1.0_pr)*MW_in(Nspec)/u_prev(:,nden) &
       !   -(u_prev(:,neng)-0.5_pr*SUM(u_prev(:,nvel(1):nvel(dim))**2.0_pr,DIM=2) &
       !   /u_prev(:,nden))/(cp_in(Nspec)*MW_in(Nspec)-1.0_pr)*MW_in(Nspec)/u_prev(:,nden)**2.0_pr*ulc(:,nden) &
       !   ) !brinkman on energy

       user_Drhs_diag((neng-1)*ng+1:neng*ng) = user_Drhs_diag((neng-1)*ng+1:neng*ng) * ( 1.0_pr + (1.0_pr/porosity - 1.0_pr)*penal) !brinkman on density
    ELSE
       meth_central  = meth + BIASING_NONE
       meth_backward = meth + BIASING_BACKWARD
       meth_forward  = meth + BIASING_FORWARD
       
       CALL c_diff_diag(du_diagF, d2u_diagF, j_lev, ng, meth_forward,  meth_forward, 10)  !ERIC: what are the two different 'meth'?
       CALL c_diff_diag(du_diagB, d2u_diagB, j_lev, ng, meth_backward, meth_backward, 10)
       CALL c_diff_diag(du_diagC, d2u_diagC, j_lev, ng, meth_central,  meth_central, 01)
       
       CALL    c_diff_fast(u_prev, du, d2u, j_lev, ng, meth_central, 11, ne, 1, ne)
       CALL    c_diff_fast(u_prev, duF, d2uF, j_lev, ng, meth_forward, 10, ne, 1, ne)
       CALL    c_diff_fast(u_prev, duB, d2uB, j_lev, ng, meth_backward, 10, ne, 1, ne)
       
       IF (use_dist) THEN
          CALL user_dist (ng, t+dt, NORMAL=norm, U_TARGET=u_0)
          Spenal = dist
       ELSE
          CALL user_dist (ng, t+dt, DISTANCE=Spenal, NORMAL=norm, U_TARGET=u_0)
       END IF
       Spenal2= Spenal
       Spenal3= Spenal
       !MASK within penal to add diffusion and sink
       delta_Jmx = MINVAL(h(1,1:dim)/2.0_pr**(REAL(j_mx,pr) - 1.0_pr))
       
       slope = (ATANH( (0.5_pr-10.0_pr**-3.0_pr)/0.5_pr)- ATANH( (-0.5_pr+10.0_pr**-3.0_pr)/0.5_pr))/(REAL(delta_pts,pr)*delta_Jmx)
       offset = REAL(IB_pts,pr)*delta_Jmx - 1.0_pr/slope*ATANH( (-0.5_pr+10.0_pr**-3.0_pr)/0.5_pr )
       Spenal = MAX(0.5_pr - 10.0_pr**-3.0_pr - 0.5_pr*TANH(slope*(Spenal - offset- 0.0_pr*delta_Jmx)) , 0.0_pr )*penal
       !Spenal2 = MAX(0.5_pr - 10.0_pr**-3.0_pr + 0.5_pr*TANH(slope*(Spenal2 - offset- 1.0_pr*delta_Jmx)) , 0.0_pr )*penal  !core
       Spenal2 = MAX(0.5_pr - 10.0_pr**-3.0_pr + 0.5_pr*TANH(slope*(Spenal2 - offset- 0.0_pr*delta_Jmx)) , 0.0_pr )*penal
       !Spenal2 = penal
       Spenal3 = 0.0_pr
       !   Spenal3 = (MAX(0.5_pr - 10.0_pr**-3.0_pr - 0.5_pr*TANH(slope*(Spenal3 - offset- 3.0_pr*delta_Jmx)) , 0.0_pr )  - Spenal)  *penal  !low order convection before transition
       
       
       IF (no_slip) THEN
          DO idim =1,dim
             Utan_prev(:, idim) = 0.0_pr
             Unorm_prev (:,idim) = u_prev(:, nvel(idim))  
          END DO
       ELSE  !ERIC: double check this for Euler Eqsi
          PRINT *, 'DRHS_diag not set up for Euler eqns'
          Utan_prev(:, 1) = 0.0_pr
          DO idim = 1, dim
             Utan_prev(:, 1) = Utan_prev(:,1)+ u_prev(:, nvel(idim))*norm(:,idim)
          END DO
          DO idim = 1, dim
             Unorm_prev(:, idim) = Utan_prev(:,1)*norm(:,idim)
          END DO
          DO idim = 1,dim
             Utan_prev(:,idim) = u_prev(:, nvel(idim))-Unorm_prev(:,idim)
          END DO
       END IF
       
       
       
       
       !Set normal derivatives upwind
       DO j = 1,ne
          dudn(j,:) = 0.5_pr*SUM( duB(j,:, :)*(norm(:,:) + ABS(norm(:,:))) + duF(j,:, :)*(norm(:,:) - ABS(norm(:,:))) ,DIM=2)
       END DO
       dudn_diag(:) = 0.5_pr*SUM( du_diagB(:, :)*(norm(:,:) + ABS(norm(:,:))) + du_diagF(:, :)*(norm(:,:) - ABS(norm(:,:))) ,DIM=2)
       
       !PRINT *, 'dudn_ndrho:', MINVAL(dudn(ndrho,:)*penal), MAXVAL(dudn(ndrho,:)*penal)
       
       
       !set nu_n
       nu_n = visc_factor_d**2.0_pr *delta_Jmx**2.0_pr/eta_b
       !nu_n = visc_factor_d *delta_Jmx
       
       ! sets NN RHS to zero inside of Brinkman zone
       DO ie = 1,ne
          user_Drhs_diag((ie-1)*ng+1:ie*ng) = (1.0_pr-penal)*user_Drhs_diag((ie-1)*ng+1:ie*ng) 
       END DO
       
       !PRINT *, 'DEN_1:', MINVAL(user_Drhs((nden-1)*ng+1:nden*ng)), MAXVAL(user_Drhs((nden-1)*ng+1:nden*ng))
       !PRINT *, 'DRHO_1:', MINVAL(user_Drhs((ndrho-1)*ng+1:ndrho*ng)), MAXVAL(user_Drhs((ndrho-1)*ng+1:ndrho*ng
       
       IF(conv_drho) THEN 
          !Convect normal derivative
          user_Drhs_diag((ndrho-1)*ng+1:ndrho*ng) = user_Drhs_diag((ndrho-1)*ng+1:ndrho*ng) &   ! cont eq penalization
               - (Spenal)* dudn_diag(:)/eta_c &!use low order
               - (Spenal3)*dudn_diag(:)/eta_c !use low order
          !PRINT *, 'DRHO2:', MINVAL(user_Drhs((ndrho-1)*ng+1:ndrho*ng)*penal), MAXVAL(user_Drhs((ndrho-1)*ng+1:ndrho*ng)*penal)
          DO ie = 1,dim 
             user_Drhs_diag((nvel(ie)-1)*ng+1:nvel(ie)*ng) = user_Drhs_diag((nvel(ie)-1)*ng+1:nvel(ie)*ng) &  !density evolution condition
                  - (Spenal+Spenal3)*(-u_prev(:,ndrho)/u_prev(:,nden)  )/eta_c       !density target
          END DO
          user_Drhs_diag((neng-1)*ng+1:neng*ng) = user_Drhs_diag((neng-1)*ng+1:neng*ng) &
               + Spenal *u_prev(:,ndrho) * u_prev(:,nden)/u_prev(:,nden)**2.0_pr/eta_c & !remove evolutionary drho effects from Temperature slope
               + Spenal3*u_prev(:,ndrho) * u_prev(:,nden)/u_prev(:,nden)**2.0_pr/eta_c  !remove evolutionary drho effects from Temperature slope
       END IF
       
       !Continuity Equation
       user_Drhs_diag((nden-1)*ng+1:nden*ng) = user_Drhs_diag((nden-1)*ng+1:nden*ng)  &  ! cont eq penalization
            - Spenal *  (dudn_diag(:))/eta_c &
            - Spenal3*  (dudn_diag(:))/eta_c 
       
       
       !Momentum Equation
       DO ie = 1,dim  !ERIC: note, not valid for Euler 
          user_Drhs_diag((nvel(ie)-1)*ng+1:nvel(ie)*ng) = user_Drhs_diag((nvel(ie)-1)*ng+1:nvel(ie)*ng) &
               - Spenal * (dudn_diag(:)-0.0_pr)/eta_c  & !tangential :Euler  !ERIC:does d(1-n_x)/dn = 1 or 0?
               - Spenal3* (dudn_diag(:)-0.0_pr)/eta_c  & !tangential: Euler
               + penal*nu_n *(2.0_pr/u_prev(:,nden)**3.0_pr*SUM(du(nden, :, :)**2.0_pr,DIM=2)-u_prev(:,nden)**-2.0_pr*SUM(d2u(nden, :, :),DIM=2)) !ERIC: how to do second derivative?  !artificial viscosity in non-conservative form
          user_Drhs_diag((nvel(ie)-1)*ng+1:nvel(ie)*ng) = user_Drhs_diag((nvel(ie)-1)*ng+1:nvel(ie)*ng) &
               - penal*(Unorm_prev(:,ie))/eta_b !&   !Normal velocity & Navier-Stokes
          user_Drhs_diag((nvel(ie)-1)*ng+1:nvel(ie)*ng) = user_Drhs_diag((nvel(ie)-1)*ng+1:nvel(ie)*ng) &  !density evolution condition
               - Spenal *( dudn(nden,:)/u_prev(:,nden))/eta_c &
               - Spenal3*( dudn(nden,:)/u_prev(:,nden))/eta_c 
       END DO
       
       !Energy Equation
       user_Drhs_diag((neng-1)*ng+1:neng*ng) = user_Drhs_diag((neng-1)*ng+1:neng*ng) &
            - Spenal * dudn_diag(:)/eta_c   !convect energy (for Neumann/Robin temperature condition)
       
       user_Drhs_diag((neng-1)*ng+1:neng*ng) = user_Drhs_diag((neng-1)*ng+1:neng*ng) &
            - Spenal3* dudn_diag(:)/eta_c 
       
       
       !add on Neumann/Robin condition targets
       user_Drhs_diag((neng-1)*ng+1:neng*ng) = user_Drhs_diag((neng-1)*ng+1:neng*ng) &   ! cont eq penalization
            + Spenal  *cbvp_A/eta_c &
            + Spenal3 *cbvp_A/eta_c
       
       
       !Use conservative form
       
       DO i=1,dim
          vTemp(1:nwlt,i) = Spenal2   
       END DO
       
       CALL shift_by(vTemp, v_for, nwlt, 1)
       v_for(1:nwlt,1:dim) = MAX(vTemp, v_for)
       CALL shift_by(vTemp, v_back, nwlt, -1)
       v_back(1:nwlt,1:dim) = MAX(vTemp, v_back)   !
       
       !Diffusion
       DO ie = 1,ne 
          IF( NOT(no_slip .AND. (ANY(nvel==ie)) .OR. ie==ndrho) ) THEN  !skip velocity components for NS
             !       IF( NOT(NS .AND. (ANY(nvel==ie)) ) ) THEN  !skip velocity components for NS
             shift = ng*(ie-1)
             !--Go through all Boundary points that are specified
             i_p_face(0) = 1
             DO i=1,dim
                i_p_face(i) = i_p_face(i-1)*3
             END DO
             DO face_type = 0, 3**dim - 1
                face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
                CALL get_all_indices_by_face (face_type, j_lev, nloc, iloc)
                IF(nloc > 0 ) THEN 
                   DO i = 1, dim
                      !user_Drhs(shift+iloc(1:nloc)) = user_Drhs(shift+iloc(1:nloc)) + (1.0_pr-ABS(face(i)))*delta_Jmx* visc_factor_n*&
                      !                               Spenal2(iloc(1:nloc))*d2u(ie,iloc(1:nloc),i) ! No diffusion normal to the boundary
                      
                      user_Drhs_diag(shift+iloc(1:nloc)) = user_Drhs_diag(shift+iloc(1:nloc)) + (1.0_pr-ABS( REAL(face(i),pr) )) *visc_factor_n&
                           !*MAX(9.0_pr*delta_Jmx/eta_b,0.1_pr/eta_c,1.0_pr)*(v_for(iloc(1:nloc),i)*duFD(ie,iloc(1:nloc),i) - v_back(iloc(1:nloc),i)*duBD(ie,iloc(1:nloc),i) )
                           *MAX(delta_Jmx/eta_c,1.0_pr)*(v_for(iloc(1:nloc),i)*du_diagF(iloc(1:nloc),i) - v_back(iloc(1:nloc),i)*du_diagB(iloc(1:nloc),i) )
                   END DO
                END IF
             END DO
          END IF
       END DO
       
       !----------------Add curvature related adjustment for penalization terms
       !    IF ( .NOT.(NS) .AND. doCURV) THEN
       !       ! Calculate additional parameters we'll need to simplify equations-----------------------------------
       !       ! prev values
       !       DO i = 1, dim
       !          vel_tan_prev(:, i) = Utan_prev(:, i)/u_prev(:, nden)     ! Actual tangential velocity components
       !          vel_norm_prev(:, i) = Unorm_prev(:, i)/u_prev(:, nden)   ! Actual normal velocity components
       !          vel_void_prev(:, i) = u_prev(:, nvel(i))/u_prev(:, nden) ! Actual velocity components
       !       END DO
       !
       !       p_prev = (u_prev(:,neng)-0.5_pr*SUM(u_prev(:,nvel(1):nvel(dim))**2.0_pr,DIM=2)/u_prev(:,nden))*(MW_in(Nspec)-1.0_pr) !pressure
       !
       !       A_prev = SUM(Utan_prev**2.0_pr, DIM=2)/p_prev                                       ! A parameter, see pdf file for details
       !       DO i = 1, dim
       !          B_prev(:, i) = A_prev*vel_tan_prev(:, i)                                    ! B parameter, see pdf file for details
       !       END DO
       !       C_prev = SUM(Utan_prev*vel_tan, DIM=2)/(MW_in(Nspec) - 1.0_pr)                        ! C parameter, see pdf file for details
       !       F_prev = A_prev*(SUM(vel_tan_prev**2.0_pr - doNORM*vel_norm_prev**2.0_pr, DIM=2))/2.0_pr ! F parameter, see pdf file for details
       !       ! perturbations
       !       DO i = 1, dim
       !          vel_tan(:, i) = (Utan(:, i)*u_prev(:, nden) - Utan_prev(:, i)*ulc(:, nden))/u_prev(:, nden)**2.0_pr
       !          vel_norm(:, i) = (Unorm(:, i)*u_prev(:, nden) - Unorm_prev(:, i)*ulc(:, nden))/u_prev(:, nden)**2.0_pr
       !          vel_void(:, i) = (ulc(:, nvel(i))*u_prev(:, nden) - u_prev(:, nvel(i))*ulc(:, nden))/u_prev(:, nden)**2.0_pr
       !       END DO
       !       p = (ulc(:, neng) - 0.5_pr*(SUM(ulc(:, nvel(1):nvel(dim))*vel_void_prev + u_prev(:, nvel(1):nvel(dim))*vel_void, DIM=2)))*(MW_in(Nspec) - 1.0_pr)
       !       A = (2.0_pr*SUM(u_prev(:, nvel(1):nvel(dim))*ulc(:, nvel(1):nvel(dim)), DIM=2)*p_prev - SUM(u_prev(:, nvel(1):nvel(dim))*u_prev(:, nvel(1):nvel(dim)), DIM=2)*p)/p_prev**2.0_pr
       !       DO i = 1, dim
       !          B(:, i) = A*vel_tan_prev(:, i) + A_prev*vel_tan(:, i)
       !       END DO
       !       C = SUM(ulc(:, nvel(1):nvel(dim))*vel_tan_prev + u_prev(:, nvel(1):nvel(dim))*vel_tan, DIM=2)/(MW_in(Nspec) - 1.0_pr)
       !       F = A*SUM(vel_tan_prev**2.0_pr +(1.0_pr -2.0_pr*doNORM)*vel_norm_prev**2.0_pr,DIM=2)/2.0_pr + A_prev*SUM(vel_tan_prev*vel_tan + (1.0_pr -2.0_pr*doNORM)*vel_norm_prev*vel_norm,DIM=2)
       !       !-----------------------------------------------------------------------------------------------------
       !       curv(:) = 0.0_pr
       !       CALL c_diff_fast(norm, dn, d2n, j_lev, ng, meth_central, 10, dim, 1, dim)
       !       ! Calculating the curvature. PROBABLY NEED TO CREATE SEPARATE SUBROUTINE FOR SIMPLICITY!!!!
       !       DO i = 1, dim
       !          curv(:) = curv(:) + dn(i, :, i) ! Calculate the curvature
       !       END DO
       !       curv = Spenal*curv/eta_c ! Common term to simplify the equations, S*k/eta
       !       ! Add curvature to continuity
       !       user_Drhs((nden-1)*ng+1:nden*ng) = user_Drhs((nden-1)*ng+1:nden*ng) + curv*A
       !       ! Add curvature to momentum equation
       !       DO ie = 1,dim 
       !          user_Drhs((nvel(ie)-1)*ng+1:nvel(ie)*ng) = user_Drhs((nvel(ie)-1)*ng+1:nvel(ie)*ng) + curv*B(:, ie)
       !       END DO
       !       ! Add curvature to energy equation
       !       user_Drhs((neng-1)*ng+1:neng*ng) = user_Drhs((neng-1)*ng+1:neng*ng) + curv*(C + F)
       !    END IF
       !-----------------------------------------------------------------------

    END IF
    !IF( do_Adp_Eps_Spatial_Evol ) &
    !CALL user_Drhs_diag__VT (user_Drhs_diag, du_prev_timestep, du, d2u, meth)

  END SUBROUTINE Brinkman_Drhs_diag

  SUBROUTINE Brinkman_input
  
       !use a vector method for speed (at expense of memory) 
       call input_logical ('meth_vect_diff',meth_vect_diff,'stop','use c_diff_fast_vector for forward/backward derivatives')
       
       !Type of obstacle
       call input_logical ('brinkman_penal',brinkman_penal,'stop','Brinkman penalization to be used')
       IF(.NOT.(brinkman_penal))   call input_logical ('vp',vp,'stop','volume penalization to be used')

       call input_logical ('no_slip',no_slip,'stop','no_slip: use the no slip condition on velocity')
       conv_drho = .FALSE.
       IF(vp) call input_logical ('conv_drho',conv_drho,'stop','conv_drho: extrapolate drho as integrated variable')

       !obstacle inputs for distance function
       call input_real ('xw',xw,'stop', 'wedge apex')
       call input_real ('wa',wa,'stop', 'wedge angle (degree)')
       call input_real ('xsp',xsp,'stop', 'sphere center x')
       call input_real ('ysp',ysp,'stop', 'sphere center y')
       call input_real ('rsp',rsp,'stop', 'sphere radius')
       
       call input_real ('xwall',xwall,'stop', 'wall position')
       
       ! multiple spheres parameters
       call input_integer ('Nsp',Nsp,'stop', 'Number of spheres')
       IF (.NOT. ALLOCATED(xsps)) ALLOCATE(xsps(Nsp))
       IF (.NOT. ALLOCATED(ysps)) ALLOCATE(ysps(Nsp))
       IF (.NOT. ALLOCATED(rsps)) ALLOCATE(rsps(Nsp))
       call input_real_vector ('xsps',xsps(1:Nsp),Nsp,'stop', 'sphere centers x')
       call input_real_vector ('ysps',ysps(1:Nsp),Nsp,'stop', 'sphere centers y')
       call input_real_vector ('rsps',rsps(1:Nsp),Nsp,'stop', 'spheres radii')

       smooth_dist = .FALSE. ! default value
       call input_logical ('smooth_dist',smooth_dist,'default','T smoothes distance function prior finding the normal')
    
       deltadel_loc = 1.0_pr
       call input_real ('deltadel_loc',deltadel_loc,'default', 'sets final diffusion thickness using spatial resolution (dx, dy, or dz), number of points across final thickness')


       !brinkman parameters
       eta_b = 1.0_pr
       eta_c = 1.0_pr
       call input_real ('eta_b',eta_b,'stop',' penalization parameter') !also for volume penalization
       call input_real ('eta_c',eta_c,'stop',' penalization parameter for adiabatic conditions') !also for volume penalization
       
       call input_real ('porosity',porosity,'stop',' porosity for brinkman')

       !penalization control
        call input_logical ('doCURV',doCURV,'stop', 'If true use curvature adjustment')
        call input_real ('doNORM',doNORM,'stop', 'If true add terms with normal velocity')


       !eric's support parameters
       call input_integer ('delta_pts',delta_pts,'stop', 'Number of points to transition from convection to diffusion')
       call input_integer ('IB_pts',IB_pts,'stop', 'Number of points (distance) for convection inside of the immersed boundary')
       !Nurlybek's support parameters
       call input_real ('delta_conv',delta_conv,'stop', 'transition thickness of the convective penalization zone')
       call input_real ('delta_diff',delta_diff,'stop', 'transition thickness of the diffusion penalization zone')
       call input_real ('shift_conv',shift_conv,'stop', 'thickness when convections is off')
       call input_real ('shift_diff',shift_diff,'stop', 'thickness of the diffusion is on')

       call input_real ('upwind_norm',upwind_norm,'stop', 'Normal-averaged upwind differencing thickness outside of interface.  < 0.0 to turn off')

       cbvp_A = 0.0_pr 
       cbvp_B = 0.0_pr 
       brinkman_temp = 1.0_pr 
       IF (vp)                call input_real ('cbvp_A',cbvp_A,'stop','cbvp_A:  For robin condition of the form dT/dn = AT+B')
       IF (vp)                call input_real ('cbvp_B',cbvp_B,'stop','cbvp_B:  For robin condition of the form dT/dn = AT+B')
       IF (brinkman_penal)    call input_real ('brinkman_temp',brinkman_temp,'stop','brinkman_temp:  Nondimensional obstacle temperature')
       !Only adiabatic condition for Euler
       IF(.NOT. no_slip) cbvp_A=0.0_pr
       IF(.NOT. no_slip) cbvp_B=0.0_pr
       !non-adiabatic obstacle requires conv_drho = T
       IF( (cbvp_A .NE. 0.0_pr) .OR. (cbvp_B .NE. 0.0_pr) ) conv_drho = .TRUE. 
 
       call input_real ('visc_factor_n',visc_factor_n,'stop','visc_factor_n: multiplier on numerical viscosity for penalization stability on neumann terms')
       call input_real ('visc_factor_d',visc_factor_d,'stop','visc_factor_d: multiplier on numerical viscosity for penalization stability on dirichlet terms')
  END SUBROUTINE
  
END MODULE Brinkman




MODULE user_case

  USE precision
  USE wlt_vars
  USE wavelet_filters_mod
  USE elliptic_mod
  USE elliptic_vars
  USE wlt_trns_mod
  USE wlt_trns_vars
  USE wlt_trns_util_mod
  USE io_3d_vars
  USE util_mod
  USE util_vars
  USE share_consts
  USE pde
  USE sizes
  USE share_kry
  USE vector_util_mod
  USE field
  USE input_file_reader
  USE parallel
  USE wlt_FWH
  USE hyperbolic_solver
  USE Brinkman
  USE user_case_vars
  USE VT

  !
  ! case specific variables
  !

  REAL (pr) :: MAreport, CFLreport, CFLconv, CFLacou, maxsos, mydt_orig
  REAL (pr) :: peakchange, maxMa
  REAL (pr) :: Tconst, Pint, gammR, Y_Xhalf
  REAL (pr), ALLOCATABLE, DIMENSION (:) :: pureX, pureY, pureR, pureg, puregamma, YR
  REAL (pr) :: e0top, e0bot, e0right,e0left, rho0top, drho0top, rho0bot, drho0bot, rhoetopBC, rhoebotBC, rhoetop, rhoebot ,velleft  !used anymore?
  REAL (pr) ::  e0,rho0
  REAL (pr), ALLOCATABLE, DIMENSION (:) :: Y0top, Y0bot, rhoYtopBC, rhoYbotBC, rhoYtop, rhoYbot, Lxyz
  REAL (pr), DIMENSION (2) :: rhoYg, drho0, dP0, c0, P0, gammBC
  !REAL (pr), ALLOCATABLE, DIMENSION (:) ::  mu_in, kk_in, cp_in, MW_in, gamm  
  REAL (pr), ALLOCATABLE, DIMENSION (:) :: bD_in, gr_in
  REAL (pr), ALLOCATABLE, DIMENSION (:,:) :: bndvals
  REAL (pr), DIMENSION (5) :: buffRed, buffRef, buffcvd, buffcvf, buffbfd, buffbff
  INTEGER :: n_var_pressure  ! start of pressure in u array
  REAL (pr) :: Rebnd, Pra, Sc, Fr, At!, Ma
  REAL (pr) :: Re,S1!, Rebnd, Pra, Sc, Fr, Ma, At
  LOGICAL :: dyn_visc
  REAL (pr) :: FWHdt
  INTEGER :: ndpx, nboy, nbRe, ndom, nton, BCver,ICver,nmsk,ncvtU
  INTEGER, ALLOCATABLE :: nspc(:), nvon(:), nson(:) 
  LOGICAL :: NS ! if NS=.TRUE. - Navier-Stoke, else Euler
  LOGICAL :: IC_gauss
  LOGICAL, PARAMETER :: NSdiag=.TRUE. ! if NSdiag=.TRUE. - use viscous terms in user_rhs_diag
  INTEGER :: nspc_lil, nspc_big
  INTEGER :: meth_pd
  LOGICAL :: GRAV, adaptAt
  LOGICAL :: evolBC, buffBC, cnvtBC
  LOGICAL :: polybuffRe, polybuff, pertchar, usedP0, polyconv, dozeroFlux, pBrink
  INTEGER :: globFlux, dervFlux, PLpinf 
  REAL (pr) :: buffBeta, buffU0, buffSig, buffDomFrac, buffOff, buffFac, pBcoef, pBsig
  LOGICAL :: adaptbuoy, savebuoys
  LOGICAL :: adaptMagVort, saveMagVort, adaptComVort, saveComVort, adaptMagVel, saveMagVel, adaptNormS, saveNormS, adaptGradY, saveGradY, saveUcvt  !4extra - this whole line
  LOGICAL :: FWHstats,aero_stats
  INTEGER :: surf_pts
  INTEGER :: nmvt, ncvt(3), nmvl, nnms !4extra - this whole line
  INTEGER, ALLOCATABLE :: ngdy(:) !4extra - this whole line
  REAL (pr) :: Lczn, tfacczn, itfacczn, itczn, tczn, cczn, czn_min, czn_max
  LOGICAL :: boundY, modczn, splitFBpress, stepsplit, savepardom
  REAL (pr) :: waitrat, maskscl, tempscl, specscl
  INTEGER :: LODIit, locnvar
  INTEGER :: methpd
  LOGICAL :: dobuffRe, buffRe, adaptbuffRe, buffRewait, LODIset, LODIslipeasy, adaptden, adaptvel, adapteng, adaptspc, adaptprs 
  LOGICAL :: adaptvelonly, adaptspconly, adapttmp, doassym, convcfl

  REAL (pr) :: buff_thick  !thickness of buffer region

  LOGICAL :: init_perturb !initial perturbation for inflow conditions

  ! NYRLYBEK
  LOGICAL :: fSUPERS, doSHOCK
  
  ! Shock params
  REAL (pr) :: rhoL, pL, EL, rhoR, pRR, ER
  REAL (pr), DIMENSION(:), ALLOCATABLE :: uL, uR
  REAL (pr) :: shock, shock_delta
  
CONTAINS

   SUBROUTINE  user_setup_pde ( VERB ) 
     USE hyperbolic_solver
    IMPLICIT NONE
    LOGICAL, OPTIONAL :: VERB         ! print debug info
    INTEGER :: i, l   !4extra
    CHARACTER(LEN=8) :: specnum  !4extra
    REAL(pr) :: nd_assym_high, nd_assym_bnd_high, nd2_assym_high, nd2_assym_bnd_high, nd_assym_low, nd_assym_bnd_low, nd2_assym_low, nd2_assym_bnd_low

 
    locnvar = dim+Nspec+1
 
    IF (par_rank.EQ.0) THEN
       PRINT * ,''
       PRINT *, '**********************Setting up PDE*****************'
       PRINT * ,'CASE: External Flow '
       PRINT *, '*****************************************************'
    END IF

    n_integrated = dim + 1 + Nspec     
    IF(conv_drho) n_integrated = n_integrated +1 !extra one to convect drho/dn     



    
    n_var_additional = 1

!    IF(hypermodel .NE. 0) THEN
!       n_var_additional = n_var_additional + 1
!    END IF

    IF (adapttmp) THEN
       n_var_additional = n_var_additional + 1
       nton = n_integrated + n_var_additional
    END IF

    IF (adaptvelonly) THEN
       IF (ALLOCATED(nvon)) DEALLOCATE(nvon)
       ALLOCATE (nvon(dim))
       DO i=1,dim
          n_var_additional = n_var_additional + 1
          nvon(i) = n_integrated + n_var_additional
       END DO
    END IF

    IF (adaptspconly .AND. Nspec > 1) THEN
       IF (ALLOCATED(nson)) DEALLOCATE(nson)
       ALLOCATE (nson(Nspecm))
       DO l=1,Nspecm
          n_var_additional = n_var_additional + 1
          nson(l) = n_integrated + n_var_additional
       END DO
    END IF

    IF (adaptbuffRe) THEN
       n_var_additional = n_var_additional + 1
       nbRe = n_integrated + n_var_additional
    END IF

    IF (savebuoys) THEN
       n_var_additional = n_var_additional + 2
       ndpx = n_integrated + n_var_additional - 1
       nboy = n_integrated + n_var_additional
    END IF

    IF (savepardom) THEN
       n_var_additional = n_var_additional + 1
       ndom = n_integrated + n_var_additional
    END IF

    !4extra - need all these definitions
    IF( adaptMagVort .or. saveMagVort ) THEN
       n_var_additional = n_var_additional + 1
       nmvt = n_integrated + n_var_additional
    END IF
    IF( (adaptComVort .or. saveComVort) .AND. dim==2 ) THEN
       n_var_additional = n_var_additional + 1
       ncvt(:) = n_integrated + n_var_additional
    END IF
    IF( (adaptComVort .or. saveComVort) .AND. dim==3 ) THEN
       DO i=1,3
          n_var_additional = n_var_additional + 1
          ncvt(i) = n_integrated + n_var_additional
       END DO
    END IF
    IF( adaptMagVel  .OR. saveMagVel  ) THEN
       n_var_additional = n_var_additional + 1
       nmvl = n_integrated + n_var_additional
    END IF
    IF( adaptNormS   .OR. saveNormS   ) THEN
       n_var_additional = n_var_additional + 1
       nnms = n_integrated + n_var_additional
    END IF
    IF (ALLOCATED(ngdy)) DEALLOCATE(ngdy)
    IF (Nspec>1) THEN
       ALLOCATE(ngdy(Nspecm))
       IF( adaptGradY   .OR. saveGradY   ) THEN
          DO l=1,Nspecm
             n_var_additional = n_var_additional + 1
             ngdy(l) = n_integrated + n_var_additional
          END DO
       END IF
    END IF
    IF (imask_obstacle) THEN
!!$       IF ( saveDIST) THEN 
!!$          n_var_additional = n_var_additional + 1
!!$          ndist = n_integrated + n_var_additional
!!$       END IF
       IF (saveUn) THEN
          n_var_additional = n_var_additional + 1
          nUn = n_integrated + n_var_additional
          n_var_additional = n_var_additional + 1
          nUtan = n_integrated + n_var_additional
       END IF
       n_var_additional = n_var_additional + 1
       nmsk = n_integrated + n_var_additional

    END IF
    IF ( saveUcvt) THEN 
       n_var_additional = n_var_additional + 1
       ncvtU = n_integrated + n_var_additional
    END IF

    n_var = n_integrated + n_var_additional !--Total number of variables

    n_var_exact = 0

!***********Variable orders, ensure 1:dim+1+Nspec are density, velocity(dim), energy, species(Nspecm)**************
    IF (ALLOCATED(nvel)) DEALLOCATE(nvel)
    ALLOCATE(nvel(dim))
    IF (ALLOCATED(nspc)) DEALLOCATE(nspc)
    IF (Nspec>1) ALLOCATE(nspc(Nspecm))

    nden = 1
    DO i=1,dim 
       nvel(i) = i+1
    END DO
    neng = dim+2
    IF (Nspec>1) THEN
       DO i=1,Nspecm
          nspc(i) = dim+2+i
       END DO
    END IF
    nprs = dim+2+Nspec   !to move pressure after all integrated variables

    IF(conv_drho) THEN
       ndrho = nprs
       nprs = ndrho +1
    END IF
    
    n_var_pressure  = nprs !pressure
    n_var_mom = nvel



!    nden = dim+Nspec+1
!    DO i=1,dim 
!       nvel(i) = i
!    END DO
!    neng = dim+Nspec
!    DO i=1,Nspecm
!       nspc(i) = dim+i
!    END DO
!    nprs = dim+2+Nspec

    nspc_lil = 1
    nspc_big = 0
    IF (Nspec>1) nspc_lil=nspc(1)
    IF (Nspec>1) nspc_big=nspc(Nspecm)

!******************************************************************************************************************
    !
    !--Allocate logical arrays 
    !  Must be done after setting number of different types of variables
    !  and before setting logical variable mapping arrays
    !
    CALL alloc_variable_mappings
    !
    ! Fill in variable names for default variables for integration meth 2
    ! These may be changed in the call to the user specified routine
    !  user_setup_pde()
    !
    ! In integrated variables (3D)
    WRITE (u_variable_names(nden), u_variable_names_fmt) 'Den_rho  '
    WRITE (u_variable_names(nvel(1)), u_variable_names_fmt) 'XMom  '
    IF (dim.GE.2)WRITE (u_variable_names(nvel(2)), u_variable_names_fmt) 'YMom  '
    IF (dim.EQ.3) WRITE (u_variable_names(nvel(3)), u_variable_names_fmt) 'ZMom  '
    WRITE (u_variable_names(neng), u_variable_names_fmt) 'E_Total  '
    IF (Nspec>1) THEN
       DO l=1,Nspecm      
          WRITE (specnum,'(I8)') l          
          WRITE (u_variable_names(nspc(l)), u_variable_names_fmt) TRIM('Spec_Scalar_'//ADJUSTL(TRIM(specnum)))//'  '
       END DO
    END IF
    WRITE (u_variable_names(nprs), u_variable_names_fmt) 'Pressure  '
    IF(conv_drho)WRITE (u_variable_names(ndrho), u_variable_names_fmt) 'Drho/dn  '
!    IF(hypermodel .NE. 0) WRITE (u_variable_names(nprs+1), u_variable_names_fmt) 'visc  '


    IF (adapttmp) WRITE (u_variable_names(nton), u_variable_names_fmt) 'Temperature  '
    IF (adaptvelonly) THEN
       WRITE (u_variable_names(nvon(1)), u_variable_names_fmt) 'XVelocity  '
       WRITE (u_variable_names(nvon(2)), u_variable_names_fmt) 'YVelocity  '
       IF (dim.eq.3) WRITE (u_variable_names(nvon(3)), u_variable_names_fmt) 'ZVelocity  '
    END IF
    IF (adaptspconly .AND. Nspec > 1) THEN
       DO l=1,Nspecm      
          WRITE (specnum,'(I8)') l          
          WRITE (u_variable_names(nson(l)), u_variable_names_fmt) TRIM('Mass_Frac_'//ADJUSTL(TRIM(specnum)))//'  '
       END DO
    END IF

    IF(adaptbuffRe) WRITE (u_variable_names(nbRe), u_variable_names_fmt) 'ReMask  ' 
    IF (savebuoys) THEN
       WRITE (u_variable_names(ndpx), u_variable_names_fmt) 'dPdx  '
       WRITE (u_variable_names(nboy), u_variable_names_fmt) 'Buoy  '
    END IF
    IF (savepardom)  WRITE (u_variable_names(ndom), u_variable_names_fmt) 'MyDomain  '
    
    !4extra - need all these definitions
    IF( adaptMagVort .or. saveMagVort ) WRITE (u_variable_names(nmvt), u_variable_names_fmt) 'MagVort  '
    IF( (adaptComVort .or. saveComVort) .AND. dim==2 )  WRITE (u_variable_names(ncvt(1)), u_variable_names_fmt) 'ComVort  '
    IF( (adaptComVort .or. saveComVort) .AND. dim==3 ) THEN
       DO l=1,3
          WRITE (specnum,'(I8)') l
          WRITE (u_variable_names(ncvt(l)), u_variable_names_fmt) TRIM('ComVort_'//ADJUSTL(TRIM(specnum)))//'  '
       END DO
    END IF
    IF( adaptMagVel  .OR. saveMagVel  )  WRITE (u_variable_names(nmvl), u_variable_names_fmt) 'MagVel  '
    IF( adaptNormS   .OR. saveNormS   )  WRITE (u_variable_names(nnms), u_variable_names_fmt) 'NormS  '
    IF( (adaptGradY   .OR. saveGradY) .AND. Nspec > 1 ) THEN
       DO l=1,Nspecm
          WRITE (specnum,'(I8)') l
          WRITE (u_variable_names(ngdy(l)), u_variable_names_fmt) TRIM('GradY_'//ADJUSTL(TRIM(specnum)))//'  '
       END DO
    END IF

    IF (imask_obstacle) THEN
!!$       IF (saveDST) WRITE (u_variable_names(ndist), u_variable_names_fmt) 'Dist  '
       IF (saveUn) THEN
          WRITE (u_variable_names(nUn), u_variable_names_fmt) 'Unorm  '
          WRITE (u_variable_names(nUtan), u_variable_names_fmt) 'Utan  '
       END IF
       WRITE (u_variable_names(nmsk), u_variable_names_fmt) 'Mask  '
    End IF
    IF ( saveUcvt) WRITE (u_variable_names(ncvtU), u_variable_names_fmt) 'ConvectZone  '



    !
    ! setup logical variable mapping arrays
    ! This defined which variables are used for adaptation, Which need to be interpololated to the new grid at 
    ! each time step, which are saved, and for which we have an exect solutiuon we want to check at each time step
    !
    !
    ! setup which components we will base grid adaptation on.
    !
    n_var_adapt = .FALSE. !intialize
    IF (adaptden) n_var_adapt(nden,:)                  = .TRUE.
    IF (adaptvel) n_var_adapt(nvel(1):nvel(dim),:)     = .TRUE.
    IF (adapteng) n_var_adapt(neng,:)                  = .TRUE.
    IF (adaptspc) n_var_adapt(nspc(1):nspc(Nspecm),:) = .TRUE.
    IF (adaptprs) n_var_adapt(nprs,:)                  = .TRUE.

    IF (adapttmp) n_var_adapt(nton,:)                  = .TRUE.
    IF (adaptvelonly) THEN
       n_var_adapt(nvel(1):nvel(dim),:)     = .FALSE.
       n_var_adapt(nvon(1):nvon(dim),:)     = .TRUE.
    END IF
    IF (adaptspconly .AND. Nspec > 1) THEN
       n_var_adapt(nspc(1):nspc(Nspecm),:) = .FALSE.
       n_var_adapt(nson(1):nson(Nspecm),:) = .TRUE.
    END IF

    IF (adaptbuffRe) n_var_adapt(nbRe,:)  = .TRUE.
    !IF (savebuoys) n_var_adapt(ndpx,:)    = .TRUE.
    IF (adaptbuoy) n_var_adapt(nboy,:)    = .TRUE.

    !4extra - adapt
    IF( adaptMagVort ) n_var_adapt(nmvt,:)                  = .TRUE.
    IF( adaptComVort ) n_var_adapt(ncvt(1):ncvt(3),:)       = .TRUE.
    IF( adaptMagVel  ) n_var_adapt(nmvl,:)                  = .TRUE.
    IF( adaptNormS   ) n_var_adapt(nnms,:)                  = .TRUE.
    IF( adaptGradY .AND. Nspec > 1 ) n_var_adapt(ngdy(1):ngdy(Nspecm),:) = .TRUE.

    IF( imask_obstacle ) n_var_adapt(nmsk,:)                  = .TRUE.


    !--integrated variables at first time level
    !Variables that need to be interpoleted to new adapted grid in initial grid adaptation
    !Variables that need to be interpoleted to new adapted grid at each time step
    n_var_interpolate        = .FALSE. !intialize
    n_var_interpolate(1:nprs,:) = .TRUE.  
    IF(conv_drho)   n_var_interpolate(ndrho,:) = .TRUE.  
    IF(imask_obstacle) n_var_interpolate(nmsk,:) = .TRUE.

    IF (adapttmp) n_var_interpolate(nton,:)                  = .TRUE.
    IF (adaptvelonly) n_var_interpolate(nvon(1):nvon(dim),:)     = .TRUE.
    IF (adaptspconly .AND. Nspec > 1) n_var_interpolate(nson(1):nson(Nspecm),:) = .TRUE.

    IF (adaptbuffRe) n_var_interpolate(nbRe,:)  = .TRUE.
    IF (savebuoys) n_var_interpolate(ndpx,:)    = .TRUE.
    IF (savebuoys) n_var_interpolate(nboy,:)    = .TRUE.

    !4extra - need these
    IF( saveMagVort ) n_var_interpolate(nmvt,:)                   = .TRUE.
    IF( saveComVort ) n_var_interpolate(ncvt(1):ncvt(3),:)        = .TRUE.
    IF( saveMagVel  ) n_var_interpolate(nmvl,:)                   = .TRUE.
    IF( saveNormS   ) n_var_interpolate(nnms,:)                   = .TRUE.
    IF( saveGradY .AND. Nspec > 1 ) n_var_interpolate(ngdy(1):ngdy(Nspecm),:)  = .TRUE.
    IF( imask_obstacle    ) THEN
       n_var_interpolate(nUn, :) = .TRUE.
       n_var_interpolate(nUtan, :) = .TRUE.
       n_var_interpolate(nmsk,:)    = .TRUE.
    END IF
    IF( saveUcvt    ) n_var_interpolate(ncvtU,:)    = .TRUE.

    !
    ! setup which components we have an exact solution for
    !
    n_var_exact_soln = .FALSE. !intialize

    !
    ! setup which variables we will save the solution
    !
    n_var_save = .FALSE. !intialize 
    n_var_save(1:nprs) = .TRUE. ! save all for restarting code
    !n_var_save(ndrho) = .FALSE. ! ERIC:need to do this?
!    IF(hypermodel .NE. 0) n_var_save(nprs+1) = .TRUE.

    IF (adapttmp) n_var_save(nton) = .TRUE.
    IF (adaptbuffRe) n_var_save(nbRe)  = .TRUE.
    IF (savebuoys) n_var_save(ndpx)    = .TRUE.
    IF (savebuoys) n_var_save(nboy)    = .TRUE.
    IF (savepardom)  n_var_save(ndom)  = .TRUE.

    !4extra - need these
    IF( saveMagVort ) n_var_save(nmvt)     = .TRUE.
    IF( saveComVort ) n_var_save(ncvt(1):ncvt(3))  = .TRUE.
    IF( saveMagVel  ) n_var_save(nmvl)     = .TRUE.
    IF( saveNormS   ) n_var_save(nnms)     = .TRUE.
    IF( saveGradY .AND. Nspec > 1) n_var_save(ngdy(1):ngdy(Nspecm)) = .TRUE.
    IF( imask_obstacle    ) THEN
       IF(saveDIST) n_var_save(ndist) = .TRUE.
       IF (saveUn) THEN
          n_var_save(nUn) = .TRUE.
          n_var_save(nUtan) = .TRUE.      
       END IF
       n_var_save(nmsk)    = .TRUE.
    END IF
    IF( saveUcvt    ) n_var_save(ncvtU)    = .TRUE.

    n_var_req_restart = n_var_save !does not require hyperbolic module viscosity for restart
    !
    ! Set the maximum number of components for which we have an exact solution
    !
    n_var_exact = MAX( COUNT(n_var_exact_soln(:,0)),COUNT(n_var_exact_soln(:,1)) )

    !
    ! Setup a scaleCoeff array if we want to tweak the scaling of a variable
    ! ( if scaleCoeff < 1.0 then more points will be added to grid 
    !
    ALLOCATE ( scaleCoeff(1:n_var) )
    scaleCoeff = 1.0_pr
 
    IF (doassym) THEN
       !**************** Predefine maximum assymetry allowed ***********************************
       !CALL set_max_assymetry   ! from wlt_vars module
    
       nd_assym_high        = 0 !n_diff+1 !MAX(0,n_diff)!0 !1 !  Nd_assym_high                                     %
       nd_assym_bnd_high    = 1 !n_diff+1 !MAX(1,n_diff)!1 !1 !  Nd_assym_bnd_high                                 %
       nd2_assym_high       = 0 !n_diff+2 !MAX(0,n_diff)   !1 !  Nd2_assym_high                                    %
       nd2_assym_bnd_high   = 2 !n_diff+2 !MAX(2,n_diff)   !1 !  Nd2_assym_bnd_high                                %
       nd_assym_low         = 0 !2*n_diff +3 !0 !  Nd_assym_low                                      %
       nd_assym_bnd_low     = 1 !2*n_diff +3 !1 !  Nd_assym_bnd_low                                  %
       nd2_assym_low        = 0 !2*n_diff +3 !0 !  Nd2_assym_low                                     %
       nd2_assym_bnd_low    = 2 !2*n_diff +3 !2 !  Nd2_assym_bnd_low                                 %
       !**********************************************************************************
    
       nd_assym =     (/nd_assym_low,nd_assym_high,nd_assym_low,nd_assym_high,nd_assym_low,nd_assym_high/)
       nd_assym_bnd = (/nd_assym_bnd_low,nd_assym_bnd_high,nd_assym_bnd_low,nd_assym_bnd_high,nd_assym_bnd_low,nd_assym_bnd_high/) 
       nd2_assym =    (/nd_assym_low,nd2_assym_high,nd_assym_low,nd2_assym_high,nd_assym_low,nd2_assym_high/)
       nd2_assym_bnd =(/nd2_assym_bnd_low,nd2_assym_bnd_high,nd2_assym_bnd_low,nd2_assym_bnd_high,nd2_assym_bnd_low,nd2_assym_bnd_high/) 

       DO i = 0, n_trnsf_type !0 - regular, 1 - inernal
          n_assym_prdct(:,i)     = 0 !  N_assym_prdct for regualr wavelet transform       %
          n_assym_prdct_bnd(:,i) = 1 !  N_assym_prdct_bnd for regualr wavelet transform   %
          n_assym_updt(:,i)      = 0 !  N_assym_updt for regualr wavelet transform        %
          n_assym_updt_bnd(:,i)  = 1 !  N_assym_updt_bnd for regualr wavelet transform    %
       END DO
    END IF    
    
    IF( hypermodel /= 0 ) THEN
          IF(ALLOCATED(n_var_hyper)) DEALLOCATE(n_var_hyper)
          ALLOCATE(n_var_hyper(1:n_var))
          n_var_hyper = .FALSE.
          n_var_hyper(1:neng) = .TRUE.
!!$          n_var_hyper(nden) = .FALSE.
          IF(ALLOCATED(n_var_hyper_active)) DEALLOCATE(n_var_hyper_active)
          ALLOCATE(n_var_hyper_active(1:n_integrated))
          n_var_hyper_active = .FALSE.
          n_var_hyper_active(1:neng) = .TRUE.
    END IF


    IF (par_rank.EQ.0) THEN
       PRINT *, 'n_integrated = ',n_integrated 

       PRINT *, 'n_var = ',n_var 
       PRINT *, 'n_var_exact = ',n_var_exact 
       PRINT *, '*******************Variable Names*******************'
       DO i = 1,n_var
          WRITE (*, u_variable_names_fmt) u_variable_names(i)
       END DO
       PRINT *, '****************************************************'
    END IF

    !Use variable thresholding
    IF ( do_Adp_Eps_Spatial .OR. do_Adp_Eps_Spatial_Evol ) &
    CALL user_setup_pde__VT

  END SUBROUTINE  user_setup_pde


  !
  ! Set the exact solution for comparison to the simulated solution
  !
  ! u          - array to fill in the exact solution
  ! nlocal       - number of active wavelets   
  ! ne_local        - total number of equations
  ! t          - time of current time step 
  ! l_n_var_exact_soln_index - index into the elements of u for which we need to 
  !                            find the exact solution
  SUBROUTINE  user_exact_soln (u, nlocal,  t_local, l_n_var_exact_soln)  
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: nlocal
    REAL (pr), INTENT (IN) ::  t_local
    REAL (pr), DIMENSION (nlocal,n_var_exact), INTENT (INOUT) :: u
    LOGICAL , INTENT (IN) :: l_n_var_exact_soln(n_var)
    INTEGER :: i

  END SUBROUTINE  user_exact_soln

  SUBROUTINE user_initial_conditions (u, nlocal, ne_local, t_local, scl, scl_fltwt,iter)
    USE penalization
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: nlocal, ne_local
    REAL (pr), DIMENSION (nlocal,ne_local), INTENT (INOUT) :: u
    REAL (pr)  :: scl(1:n_var),scl_fltwt
    REAL (pr), INTENT (IN) :: t_local
    INTEGER, INTENT (INOUT) :: iter
    INTEGER :: i, l

    REAL (pr), DIMENSION(nlocal,1) :: forcing
    REAL (pr), DIMENSION(nlocal) :: p
    REAL (pr) :: pulse_center
    ! NURLYBEK: I started here
    REAL (pr), DIMENSION (nlocal) :: rho, uvel, XMom, Eng
    REAL (pr) :: alpha, beta, hypdelta, SSPEED
    INTEGER, SAVE :: icnt = 0

    EL = rhoL*uL(1)**2/2.0_pr + pL/(gamm(1) - 1.0_pr)
    hypdelta = shock_delta*h(1, 1)/2.0_pr**(j_mx - 1) ! sometimes hypdelta is too high. NURLYBEK

    alpha = SQRT(rhoL)*(uL(1) - uR(1)); beta = SQRT(1 + 16.0_pr*pL*gamm(1)/(alpha**2*(1.0_pr + gamm(1))**2))
    pRR = pL - 0.25_pr*alpha**2*(gamm(1) + 1.0_pr)*(beta - 1.0_pr)
    rhoR = rhoL*(4.0_pr*pL*gamm(1) - alpha**2*(1.0_pr + gamm(1))*(beta - 1.0_pr))/(2*(alpha**2*(gamm(1) - 1.0_pr) + 2*pL*gamm(1)))
    ER = rhoR*uR(1)**2/2.0_pr + pRR/(gamm(1) - 1.0_pr)
    SSPEED = (uL(1) - uR(1))*(3.0_pr - gamm(1) + (1.0_pr + gamm(1))*beta)/4.0_pr + uR(1)

    IF (IC_restart_mode .EQ. 0) THEN
       IF (fSUPERS) THEN
          uvel = uL(1) + 0.5_pr*(uR(1) - uL(1))*(1.0_pr + TANH((x(:, 1) - shock)/hypdelta))!*0.5_pr*(1.0_pr - SIGN(1.0_pr, x(:, 1) - shock))
          rho = rhoL + 0.5_pr*(rhoR - rhoL)*(1.0_pr + TANH((x(:, 1) - shock)/hypdelta))!*0.5_pr*(1.0_pr - SIGN(1.0_pr, x(:, 1) - shock))
          p = pL + 0.5_pr*(pRR - pL)*(1.0_pr + TANH((x(:, 1) - shock)/hypdelta))!*0.5_pr*(1.0_pr - SIGN(1.0_pr, x(:, 1) - shock))
          XMom = rho*uvel
          Eng = p/(gamm(1)-1.0_pr) + 0.5_pr*rho*uvel**2
          u(:,nden) = rho
          u(:,nvel(1)) = XMom
          IF (dim > 1) u(:,nvel(2:dim)) = 0.0_pr
          u(:, neng) = Eng
       ELSE
     !Set Density
          u(:,nden)              = 1.0_pr

          !Set Momentums
          DO i=1,dim
             !        u(:,nvel(i)) = -1.0_pr
             u(:,nvel(i)) = 0.0_pr !- 0.001_pr*penal
          END DO
          IF (.NOT.(stationary_obstacle)) u(:,nvel(1)) = -0.5_pr !- 0.001_pr*penal

          !Set Energy
          u(:,neng)              = 0.5_pr*SUM(u(:,nvel(1):nvel(dim))**2.0_pr,DIM=2)/u(:,nden)+u(:,nden)*(cp_in(Nspec)-1.0_pr/MW_in(Nspec))*Tconst

          !Set species
          IF (Nspec>1) u(:,nspc(1)) = 0.0_pr



          !Modify ICs for shock mean flow
          IF (ICver .NE. 0) THEN
             WHERE(x(:,1) .LE. -0.25_pr)
                u(:,nden) = 1.0_pr
                u(:,nvel(1)) = 1.2_pr
                u(:,neng) = u(:,nvel(1))**2.0_pr/(2.0_pr*u(:,nden)) + 1.0_pr/(MW_in(Nspec)-1.0_pr)
             ELSEWHERE
                u(:,nden) = 0.35659_pr
                u(:,nvel(1)) = 0.0_pr
                u(:,neng) =   0.201924_pr/(MW_in(Nspec)-1.0_pr)
             END WHERE
          END IF

          IF(IC_gauss) THEN
             IF (dim==1) THEN
                pulse_center = 0.0_pr
                IF (stationary_obstacle)           pulse_center = -0.25_pr
                !u(:,nden) = 1.0_pr +1.e-3_pr*EXP( -LOG(2.0_pr) * ((x(:,1)+ 0.25_pr)**2  / 0.004_pr ))! gaussian centered on x = -0.25
                !u(:,nvel(1)) = 1.e-3_pr*EXP( -LOG(2.0_pr) * ((x(:,1)+ 0.25_pr)**2  / 0.004_pr ))  
                !p = 1.0_pr/MW_in(Nspec) + 1.e-3_pr*EXP( -LOG(2.0_pr) * ((x(:,1)+ 0.25_pr)**2  / 0.004_pr ))
                !u(:,neng)              = u(:,nvel(1))**2.0_pr/2.0_pr/u(:,nden) + p/(MW_in(Nspec)-1.0_pr)
                p = 1.0_pr/MW_in(Nspec)
                WHERE ( ABS(x(:,1)-pulse_center)<0.2_pr )  !alternate pulse of cubic splines centered at x = -0.25  !fourth order ensures continuity of piecewise through the 2nd derivative
                   u(:,nden)    = 1.0_pr              +             1.0e-3_pr * ((x(:,1)-pulse_center)/0.2_pr-1.0_pr)**4.0_pr * ((x(:,1)-pulse_center)/0.2_pr+1.0_pr)**4.0_pr
                   u(:,nvel(1)) = u(:,nden)*u(:,nvel(1))     +      u(:,nden)*1.e-3_pr * ((x(:,1)-pulse_center)/0.2_pr-1.0_pr)**4.0_pr * ((x(:,1)-pulse_center)/0.2_pr+1.0_pr)**4.0_pr
                   p            = 1.0_pr/MW_in(Nspec) +             1.0e-3_pr * ((x(:,1)-pulse_center)/0.2_pr-1.0_pr)**4.0_pr * ((x(:,1)-pulse_center)/0.2_pr+1.0_pr)**4.0_pr
                   u(:,neng)              = u(:,nvel(1))**2.0_pr/2.0_pr/u(:,nden) + p/(MW_in(Nspec)-1.0_pr)
                END WHERE
             ELSE
                u(:,nden) = 1.0_pr +1.0e-3_pr*EXP( -LOG(2.0_pr) * ((x(:,1)-1.5_pr)**2.0_pr + SUM(x(:,2:dim)**2.0_pr,DIM=2)) / 0.002_pr )! gaussian centered on x = -2
                u(:,neng) = u(:,nvel(1))**2.0_pr/(2.0_pr*u(:,nden)) + 1.0_pr/(MW_in(Nspec)*(MW_in(Nspec)-1.0_pr)) + (1.0e-3_pr*EXP( -LOG(2.0_pr) * ((x(:,1)-1.50_pr)**2.0_pr + SUM(x(:,2:dim)**2.0_pr,DIM=2)) / 0.002_pr ))/(MW_in(Nspec)-1.0_pr)
                !IF(dim==3 .AND. IC_gauss) u(:,nden) = 1.0_pr +1.e-1_pr*EXP( -LOG(2.0_pr) * ((x(:,1)-2.0_pr)**2.0_pr + x(:,3)**2.0_pr + x(:,2)**2.0_pr) / 0.04_pr )! gaussian centered on x = -2
             END IF
          END IF
       END IF
    END IF
  !for BC's 
  !***** necessary? *******
  e0top = (cp_in(Nspec)-1.0_pr/MW_in(Nspec))*Tconst  !to hold temperature on boundaries, KE = 0
  e0bot  = e0top
  !******************

  rho0 = 1.0_pr   !ambient density
  e0 = rho0*(cp_in(Nspec)-1.0_pr/MW_in(Nspec))*Tconst  !ambient energy - for volume penalization
  
  IF(ALLOCATED(bndvals)) DEALLOCATE(bndvals)
  ALLOCATE(bndvals(ne_local,dim*2))  !integrated variables,number of boundaries
  
  !quiescent boundary for Freund
  bndvals(:,:)                  = 0.0_pr 
  bndvals(nden,:)               =  1.0_pr
  bndvals(nvel(1):nvel(dim),:)  =  0.0_pr
  IF(.NOT.(stationary_obstacle)) bndvals(nvel(1),:)   =  -0.5_pr
  bndvals(neng,:)               =  0.5_pr*SUM(bndvals(nvel(1):nvel(dim),:)**2.0_pr,DIM=1)+bndvals(nden,:)*(cp_in(Nspec)-1.0_pr/MW_in(Nspec))*Tconst
!  bndvals(neng,:)               =  0.5_pr+bndvals(nden,:)*(cp_in(Nspec)-1.0_pr/MW_in(Nspec))*Tconst
  
  !add inflow boundary
  IF (BCver .EQ. 1) THEN
     bndvals(nden,1)               =  1.0_pr
     bndvals(nden,2)               =  0.35659_pr
     bndvals(nvel(1),1)    = 1.2_pr
     bndvals(nvel(1),2)    = 0.0_pr
     bndvals(neng,1)              = bndvals(nden,1)*1.2_pr**2.0_pr/2.0_pr +1.0_pr/(MW_in(Nspec)-1.0_pr) 
     bndvals(neng,2)              = 0.201924_pr/(MW_in(Nspec)-1.0_pr)
  END IF
  
!!$  IF(par_rank .EQ. 0) THEN
!!$     PRINT *, 'bndvals - den',bndvals(nden,:)
!!$     PRINT *, 'bndvals - eng',bndvals(neng,:)
!!$     PRINT *, 'bndvals - mom',bndvals(nvel(:),:)
!!$  END IF
  
  !variable thresholding
  IF ( do_Adp_Eps_Spatial .OR. do_Adp_Eps_Spatial_Evol ) &
       CALL user_initial_conditions__VT(u, nlocal, ne_local)!, t_local, scl, scl_fltwt, iter)
   !PRINT *, j_lev 
END SUBROUTINE user_initial_conditions

SUBROUTINE dpdx (pderiv,nlocal,Yloc,meth,myjl,split)
  IMPLICIT NONE
  INTEGER, INTENT (IN) :: nlocal
    REAL (pr), DIMENSION (nlocal), INTENT(INOUT) :: pderiv
    REAL (pr), DIMENSION (nlocal,MAX(Nspecm,1)), INTENT(IN) :: Yloc
    INTEGER, INTENT(IN) :: meth, myjl
    LOGICAL, INTENT(IN) :: split
    INTEGER :: i
    REAL (pr), DIMENSION (nlocal,dim) :: du
    REAL (pr), DIMENSION (nlocal,dim) :: d2u



    IF (split) THEN 
       CALL c_diff_fast(pderiv(:), du(:,:), d2u(:,:), myjl, nlocal, MOD(meth,2)+BIASING_BACKWARD, 10, 1, 1, 1)
       d2u(:,1) = du(:,1)
       CALL c_diff_fast(pderiv(:), du(:,:), d2u(:,:), myjl, nlocal, MOD(meth,2)+BIASING_FORWARD , 10, 1, 1, 1)
       IF (stepsplit) THEN
          d2u(:,2) = 1.0_pr
          d2u(:,2) = (SIGN(d2u(:,2), Yloc(:,1) - Y_Xhalf) + 1.0_pr)/2.0_pr
       ELSE
          d2u(:,2) = (1.0_pr - SUM(Yloc(:,1:Nspecm),DIM=2))/MW_in(Nspec)  !Y_Nspec/W_Nspec
          DO i=1,Nspecm
             d2u(:,2) = d2u(:,2) + Yloc(:,i)/MW_in(i)  !Y_l/W_l
          END DO
          d2u(:,2) = Yloc(:,1)/MW_in(1)/d2u(:,2)  !X_1
          d2u(:,2) = (d2u(:,2)-peakchange)/(1.0_pr-2.0_pr*peakchange)  !X undoing peak change
       END IF
       d2u(:,2) = MIN(1.0_pr,MAX(0.0_pr,d2u(:,2)))
       pderiv(:) = d2u(:,2)*d2u(:,1) + (1.0_pr - d2u(:,2))*du(:,1)
    ELSE
       CALL c_diff_fast(pderiv(:), du(:,:), d2u(:,:), myjl, nlocal, meth, 10, 1, 1, 1)
       pderiv(:) = du(:,1)
    END IF
  END SUBROUTINE dpdx

  SUBROUTINE dpdx_diag (pd_diag,nlocal,Yloc,meth,myjl,split)
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: nlocal
    REAL (pr), DIMENSION (nlocal), INTENT(INOUT) :: pd_diag
    REAL (pr), DIMENSION (nlocal,MAX(Nspecm,1)), INTENT(IN) :: Yloc
    INTEGER, INTENT(IN) :: meth, myjl
    LOGICAL, INTENT(IN) :: split
    INTEGER :: i
    REAL (pr), DIMENSION (nlocal,dim) :: du_diag, d2u_diag

    IF (split) THEN 
       CALL c_diff_diag(du_diag, d2u_diag, myjl, nlocal, MOD(meth,2)+BIASING_BACKWARD, MOD(meth,2)+BIASING_FORWARD, 10) 
       pd_diag(:) = du_diag(:,1)
       CALL c_diff_diag(du_diag, d2u_diag, myjl, nlocal, MOD(meth,2)+BIASING_FORWARD, MOD(meth,2)+BIASING_BACKWARD, 10)   
       IF (stepsplit) THEN
          d2u_diag(:,2) = 1.0_pr
          d2u_diag(:,2) = (SIGN(d2u_diag(:,2), Yloc(:,1) - Y_Xhalf) + 1.0_pr)/2.0_pr
       ELSE
          d2u_diag(:,2) = (1.0_pr - SUM(Yloc(:,1:Nspecm),DIM=2))/MW_in(Nspec)
          DO i=1,Nspecm
             d2u_diag(:,2) = d2u_diag(:,2) + Yloc(:,i)/MW_in(i)
          END DO
          d2u_diag(:,2)=Yloc(:,1)/MW_in(1)/d2u_diag(:,2)  !X
          d2u_diag(:,2)=(d2u_diag(:,2)-peakchange)/(1.0_pr-2.0_pr*peakchange)  !X undoing peak change
       END IF
       d2u_diag(:,2) = MIN(1.0_pr,MAX(0.0_pr,d2u_diag(:,2)))
       pd_diag(:) = d2u_diag(:,2)*pd_diag(:) + (1.0_pr - d2u_diag(:,2))*du_diag(:,1)
    ELSE
       CALL c_diff_diag(du_diag, d2u_diag, j_lev, ng, 2*methpd+meth, 2*methpd+meth, 10)   !dp/dx done using methpd 
       pd_diag(:) = du_diag(:,1)
    END IF
  END SUBROUTINE dpdx_diag


  !--********************************  
  ! Arguments
  ! u         - field on adaptive grid
  ! nlocal      - number of active points
  ! ne_local       - number of equations
  ! t         - current time
  !
  ! Global variables used
  ! ifn       - number of equations
  ! x()
  ! xO
  ! y0
  !--******************************** 
  SUBROUTINE user_algebraic_BC (Lu, u, nlocal, ne_local, jlev, meth)
    !--Defines boundary condition type
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: jlev, meth, ne_local, nlocal
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (INOUT) :: Lu
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (IN)    :: u

    INTEGER :: ie, i, ii, shift, denshift,eshift
    INTEGER, DIMENSION (dim) :: velshift 
    REAL (pr), DIMENSION (ne_local,nlocal,dim) :: du, d2u !uncomment if Neuman BC are used, need to call 
    REAL (pr), DIMENSION (1,nlocal,dim) :: du_prev
    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc
    
    !shift markers for BC implementation
    denshift  = nlocal*(nden-1)
    DO i = 1,dim
       velshift(i) = nlocal*(nvel(i)-1)
    END DO
    eshift    = nlocal*(neng-1)




    !BC for both 2D and 3D cases

    !if Neuman BC are used, need to call 
    CALL c_diff_fast (u, du, d2u, jlev, nlocal, meth, 10, ne_local, 1, ne_local)
        !PRINT *, 'alg_BC du', MINVAL(du(1:ne_local,1:nlocal,1:dim)),  MAXVAL(du(1:ne_local,1:nlocal,1:dim))

    DO ie = 1, ne_local
       shift = nlocal*(ie-1)
       !--Go through all Boundary points that are specified
       i_p_face(0) = 1
       DO i=1,dim
          i_p_face(i) = i_p_face(i-1)*3
       END DO
       DO face_type = 0, 3**dim - 1
          face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
          IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
             CALL get_all_indices_by_face (face_type, jlev, nloc, iloc)
             IF(nloc > 0 ) THEN 
!!$                IF( face(2) == 1  ) THEN  !Top - Full
!!$
!!$                   IF(ie >= nvel(1) .AND. ie <= nvel(dim)) THEN  !Momentums
!!$                      Lu(shift+iloc(1:nloc)) = u(shift+iloc(1:nloc))       !Dirichlet conditions
!!$                   ELSE IF(ie == neng) THEN
!!$                      Lu(shift+iloc(1:nloc)) = u(shift+iloc(1:nloc))   - u(denshift+iloc(1:nloc))*e0top     !Dirichlet conditions
!!$                   END IF
!!$
!!$                ELSE IF( face(2) == -1  ) THEN  !Bottom - Full
!!$
!!$                   IF(ie >= nvel(1) .AND. ie <= nvel(dim)) THEN  !Momentums
!!$                      Lu(shift+iloc(1:nloc)) = u(shift+iloc(1:nloc))       !Dirichlet conditions
!!$                   ELSE IF(ie == neng) THEN
!!$                      Lu(shift+iloc(1:nloc)) = u(shift+iloc(1:nloc))  - u(denshift+iloc(1:nloc))*e0bot      !Dirichlet conditions
!!$                   END IF
!!$
!!$                ELSE IF( face(1) == -1 .AND. face(2) == 0 ) THEN  !Left - No corners
                IF (face(1) == -1) THEN  !X min and X max
                   Lu(shift+iloc(1:nloc)) = u(shift+iloc(1:nloc))       !Dirichlet conditions
                   IF (imask_obstacle)  THEN
                      WHERE (penal(iloc(1:nloc)) ==  1.0_pr)  Lu(shift+iloc(1:nloc)) = du(ie,iloc(1:nloc),1) !Neuman conditions
                   END IF
                   !------------------------! reflecting bc Nurlybek
!!$                   IF (face(1) == 1 .AND. nvel(1) == ie) THEN
!!$                      Lu(shift+iloc(1:nloc)) = u(shift+iloc(1:nloc))       !Dirichlet conditions
!!$                   END IF
                   !------------------------! reflecting bc Nurlybek
                END IF
                IF (dim >= 2) THEN
                   IF( ABS(face(2)) == 1 ) THEN  !Y min and Y max
                      IF (imask_obstacle) THEN
                         IF (nvel(2) == ie) THEN
                            WHERE (penal(iloc(1:nloc)) /=  1.0_pr) Lu(shift+iloc(1:nloc)) = u(shift+iloc(1:nloc))        ! Dirichlet
                         ELSE
                            WHERE (penal(iloc(1:nloc)) /=  1.0_pr) Lu(shift+iloc(1:nloc)) = du(ie,iloc(1:nloc),2)   !Neuman conditions
                         END IF
                      END IF
                   END IF
                END IF
!!$                   
                IF(BCver .EQ. 1 ) THEN!add inflow
                   IF( face(1) == -1 ) THEN  !Full x-max faces
                      Lu(shift+iloc(1:nloc)) = u(shift+iloc(1:nloc))  !Dirichlet conditions
                   END IF
                END IF
             END IF

          END IF
       END DO
    END DO

    !variable thresholding if Evolution eps is used, penalization generally uses interpolation
    IF ( do_Adp_Eps_Spatial_Evol ) &
    CALL VT_algebraic_BC (Lu, u, du, nlocal, ne_local, jlev, meth)

  END SUBROUTINE user_algebraic_BC

  SUBROUTINE user_algebraic_BC_diag (Lu_diag, nlocal, ne_local, jlev, meth)
    !--Defines boundary condition type
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: jlev, meth, ne_local, nlocal
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (INOUT) :: Lu_diag

    INTEGER :: ie, i, ii, shift, denshift

    REAL (pr), DIMENSION (nlocal,dim) :: du_diag, d2u_diag  !uncomment if Neuman BC are used
    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc
    REAL (pr), DIMENSION (1,nlocal,dim) :: du_prev, d2u
    INTEGER, PARAMETER :: methprev=1

    !BC for both 2D and 3D cases

    denshift = nlocal*(nden-1)



    !if Neuman BC are used, need to call 
    CALL c_diff_diag ( du_diag, d2u_diag, jlev, nlocal, meth, meth, 10)

    DO ie = 1, ne_local
       shift = nlocal*(ie-1)
       !--Go through all Boundary points that are specified
       i_p_face(0) = 1
       DO i=1,dim
          i_p_face(i) = i_p_face(i-1)*3
       END DO
       DO face_type = 0, 3**dim - 1
          face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
          IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
             CALL get_all_indices_by_face (face_type, jlev, nloc, iloc)
             IF(nloc > 0 ) THEN 
                IF (face(1) == -1) THEN  !X min and X max
                   Lu_diag(shift+iloc(1:nloc)) = 1.0_pr       !Dirichlet conditions
                   IF (imask_obstacle)  THEN
                      WHERE (penal(iloc(1:nloc)) ==  1.0_pr)  Lu_diag(shift+iloc(1:nloc)) = du_diag(iloc(1:nloc),1) !Neuman conditions
                   END IF
                   !------------------------! reflecting bc Nurlybek
!!$                   IF (face(1) == 1 .AND. nvel(1) == ie) THEN
!!$                      Lu_diag(shift+iloc(1:nloc)) = 1.0_pr       !Dirichlet conditions
!!$                   END IF
                   !------------------------! reflecting bc Nurlybek
                END IF
                IF (dim >= 2) THEN
                   IF( ABS(face(2)) == 1 ) THEN  !Y min and Y max
                      IF (imask_obstacle) THEN
                         IF (nvel(2) == ie) THEN
                            WHERE (penal(iloc(1:nloc)) /=  1.0_pr) Lu_diag(shift+iloc(1:nloc)) = 1.0_pr        ! Dirichlet
                         ELSE
                            WHERE (penal(iloc(1:nloc)) /=  1.0_pr) Lu_diag(shift+iloc(1:nloc)) = du_diag(iloc(1:nloc),2)   !Neuman conditions
                         END IF
                      END IF
                   END IF
                END IF
                IF(BCver .EQ. 1)THEN
                   IF( face(1) == -1 ) THEN  !Full left face - Inflow
                      Lu_diag(shift+iloc(1:nloc)) = 1.0_pr  !Dirichlet conditions
                   END IF
                END IF

             END IF
          END IF
       END DO
    END DO

    !varibale thresholding for evolution eps
    !IF ( do_Adp_Eps_Spatial_Evol ) &
    !CALL VT_algebraic_BC_diag (Lu_diag, du, nlocal, ne_local, jlev, meth) 
  END SUBROUTINE user_algebraic_BC_diag

  SUBROUTINE user_algebraic_BC_rhs (rhs, ne_local, nlocal, jlev)
    !--Sets rhs for boundary conditions
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: ne_local, nlocal, jlev
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (INOUT) :: rhs

    INTEGER :: ie, i, ii, shift, denshift
    INTEGER, PARAMETER ::  meth=1
    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc
    REAL (pr), DIMENSION (ne_local,nlocal,dim) :: duall, d2uall
    REAL (pr), DIMENSION (1,nlocal,dim) :: du_prev, du, d2u  
    REAL (pr) :: p_bc

    p_bc = 1.0_pr  !pressure defined for outflow boundary conditions

    denshift=nlocal*(nden-1)

    DO ie = 1, ne_local
       shift = nlocal*(ie-1)
       !--Go through all Boundary points that are specified
       i_p_face(0) = 1
       DO i=1,dim
          i_p_face(i) = i_p_face(i-1)*3
       END DO
       DO face_type = 0, 3**dim - 1
          face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
          IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
             CALL get_all_indices_by_face (face_type, jlev, nloc, iloc)
             IF(nloc > 0 ) THEN 
!!$                Lu(shift+iloc(1:nloc)) = u(shift+iloc(1:nloc))       !Dirichlet conditions
!!$                IF (imask_obstacle)  THEN
!!$                   WHERE (penal(iloc(1:nloc)) ==  1.0_pr)  Lu(shift+iloc(1:nloc)) = du(ie,iloc(1:nloc),1) !Neuman conditions
!!$                END IF
!!$                IF ( ABS(face(1)) == 1) THEN  !X min and X max
!!$!!!$                   IF (nvel(1) == ie) THEN
!!$!!!$                      IF (imask_obstacle) THEN
!!$!!!$                         WHERE (penal(iloc(1:nloc)) ==  0.0_pr)   Lu(shift+iloc(1:nloc)) = u(shift+iloc(1:nloc))       !Dirichlet conditions
!!$!!!$                      ELSE 
!!$                   IF (ie == nden) THEN
!!$                      rhs(shift+iloc(1:nloc)) = 0.5_pr*REAL(1 - face(1), pr)*rhoL + 0.5_pr*REAL(1 + face(1), pr)*rhoR       !Dirichlet conditions
!!$                   ELSE IF (ie >= nvel(1) .AND. ie <= nvel(dim)) THEN
!!$                      rhs(shift+iloc(1:nloc)) = 0.5_pr*REAL(1 - face(1), pr)*rhoL*uL(MINLOC(ABS(nvel(1:dim) - ie), 1)) + &
!!$                           0.5_pr*REAL(1 + face(1), pr)*rhoR*uR(MINLOC(ABS(nvel(1:dim) - ie), 1))       !Dirichlet conditions
!!$                   ELSE IF (ie == neng) THEN
!!$                      rhs(shift+iloc(1:nloc)) = 0.5_pr*REAL(1 - face(1), pr)*EL + 0.5_pr*REAL(1 + face(1), pr)*ER       !Dirichlet conditions
!!$                   END IF
!!$                   IF (imask_obstacle)  THEN
!!$                      WHERE (penal(iloc(1:nloc)) ==  1.0_pr)  rhs(shift+iloc(1:nloc)) = 0.0_pr !Neuman conditions
!!$                   END IF
!!$                   !------------------------! reflecting bc Nurlybek
                   IF (face(1) == 1 .AND. nvel(1) == ie) THEN
                      rhs(shift+iloc(1:nloc)) = 0.0_pr       !Dirichlet conditions
                   END IF
!!$                   !------------------------! reflecting bc Nurlybek
!!$                END IF
                IF (face(1) == -1) THEN  !X min and X max
                   IF (ie == nden) THEN
                      rhs(shift+iloc(1:nloc)) = rhoL       !Dirichlet conditions
                   ELSE IF (ie >= nvel(1) .AND. ie <= nvel(dim)) THEN
                      rhs(shift+iloc(1:nloc)) = rhoL*uL(MINLOC(ABS(nvel(1:dim) - ie), 1))  !Dirichlet conditions
                   ELSE IF (ie == neng) THEN
                      rhs(shift+iloc(1:nloc)) = EL       !Dirichlet conditions
                   END IF
                   IF (imask_obstacle)  THEN
                      WHERE (penal(iloc(1:nloc)) ==  1.0_pr)  rhs(shift+iloc(1:nloc)) = 0.0_pr !Neuman conditions
                   END IF
                   !------------------------! reflecting bc Nurlybek
!!$                   IF (face(1) == 1 .AND. nvel(1) == ie) THEN
!!$                      rhs(shift+iloc(1:nloc)) = 0.0_pr       !Dirichlet conditions
!!$                   END IF
                   !------------------------! reflecting bc Nurlybek
                END IF
                IF (dim >= 2) THEN
                   IF( ABS(face(2)) == 1 ) THEN  !Y min and Y max
                      IF (imask_obstacle) THEN
                         IF (nvel(2) == ie) THEN
                            WHERE (penal(iloc(1:nloc)) /=  1.0_pr) rhs(shift+iloc(1:nloc)) = 0.0_pr        ! Dirichlet
                         ELSE
                            WHERE (penal(iloc(1:nloc)) /=  1.0_pr) rhs(shift+iloc(1:nloc)) = 0.0_pr   !Neuman conditions
                         END IF
                      END IF
                   END IF
                END IF

                IF (BCver .EQ. 1) THEN
                   IF( face(1) == -1 ) THEN   !Full Left Face - Inflow  , overwrites bc's on inflow 
                      IF(ie == nden) THEN
                         rhs(shift+iloc(1:nloc)) = 1.0_pr
                      ELSE IF(ie == nvel(1)) THEN
                         IF(t<3.0_pr  .AND. init_perturb) THEN
                            rhs(shift+iloc(1:nloc)) =  Ma + 0.05_pr * Ma * SIN(x( iloc(1:nloc),2 ) ) * (1.0_pr-t/3.0_pr) 
                         ELSE
                            rhs(shift+iloc(1:nloc)) =  Ma 
                         END IF
                      ELSE IF(ie > nvel(1) .AND. ie <= nvel(dim)) THEN
                         rhs(shift+iloc(1:nloc)) = 0.0_pr 
                      ELSE IF(ie == neng) THEN
                         rhs(shift+iloc(1:nloc)) = Ma**2.0_pr/2.0_pr +  (cp_in(Nspec)-1.0_pr/MW_in(Nspec))*Tconst
                      END IF
                   END IF
                END IF
             END IF
          END IF
       END DO
    END DO

    !variable thresholding for evolution eps
    IF ( do_Adp_Eps_Spatial_Evol ) &
         CALL VT_algebraic_BC_rhs(rhs, ne_local, nlocal, jlev) 

  END SUBROUTINE user_algebraic_BC_rhs

  




  SUBROUTINE user_convectzone_BC_rhs (myrhs_loc, ulc, myne_loc, mynloc, myjl, mymeth)
    IMPLICIT NONE

    !SUBROUTINE only for single species flow
    INTEGER , INTENT (IN) :: myne_loc, mynloc, myjl, mymeth
    REAL (pr), DIMENSION (mynloc*myne_loc), INTENT (INOUT) :: myrhs_loc
    REAL (pr), DIMENSION (mynloc,myne_loc), INTENT (IN) :: ulc

    INTEGER :: ie, i, j, shift
    REAL (pr), DIMENSION (myne_loc,mynloc,dim) :: du, d2u  !derivatives of native variables
    REAL (pr), DIMENSION (mynloc,2*dim) :: bigU
    REAL (pr), DIMENSION (mynloc,myne_loc) :: myu_zer
  
    CALL c_diff_fast(ulc, du, d2u, myjl, mynloc, mymeth, 10, myne_loc, 1, myne_loc) 

    IF(cnvtBC) THEN !add convection to bufferzone
!!$       CALL user_bufferfunc(bigU, mynloc, buffcvd, polyconv)
       CALL user_bufferfunc_eric(bigU, mynloc, buffcvd, polyconv)

       DO j = 1,dim
          DO ie=1,myne_loc
             shift = (ie-1)*mynloc   
!!$       myrhs_loc(shift+1:shift+mynloc) = myrhs_loc(shift+1:shift+mynloc) - (bigU(:,2)*c0(1)-bigU(:,1)*c0(2))*buffU0*du(ie,:,1)
             IF (j ==1 .AND. BCver .NE. 0) THEN  !add inflow at xmin
                myrhs_loc(shift+1:shift+mynloc) = myrhs_loc(shift+1:shift+mynloc) - (0.0_pr* bigU(:,2*j-1)+bigU(:,2*j) )*buffU0*du(ie,:,j) 
             ELSE
                myrhs_loc(shift+1:shift+mynloc) = myrhs_loc(shift+1:shift+mynloc) - (-0.0_pr*bigU(:,2*j-1)+bigU(:,2*j) )*buffU0*du(ie,:,j) 
             END IF
          END DO
       END DO
    END IF


  END SUBROUTINE user_convectzone_BC_rhs 

  SUBROUTINE user_convectzone_BC_Drhs (Dmyrhs_loc, ulc, ulc_prev, myne_loc, mynloc, myjl, mymeth)
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: myne_loc, mynloc, myjl, mymeth
    REAL (pr), DIMENSION (mynloc*myne_loc), INTENT (INOUT) :: Dmyrhs_loc
    REAL (pr), DIMENSION (mynloc,myne_loc), INTENT (IN) :: ulc, ulc_prev

    INTEGER :: ie, i, j, shift
    REAL (pr), DIMENSION (myne_loc,mynloc,dim) :: du, d2u
    REAL (pr), DIMENSION (mynloc,2*dim) :: bigU


  

    CALL c_diff_fast(ulc, du, d2u, myjl, mynloc, mymeth, 10, myne_loc, 1, myne_loc) 


!!$       CALL user_bufferfunc(bigU, mynloc, buffcvd, polyconv)
       CALL user_bufferfunc_eric(bigU, mynloc, buffcvd, polyconv)
       DO j = 1,dim
          DO ie=1,myne_loc
             shift = (ie-1)*mynloc   
!!$       Dmyrhs_loc(shift+1:shift+mynloc) = Dmyrhs_loc(shift+1:shift+mynloc) - (bigU(:,2)*c0(1)-bigU(:,1)*c0(2))*buffU0*du(ie,:,1)
             IF (j == 1 .AND. BCver .NE. 0) THEN
                Dmyrhs_loc(shift+1:shift+mynloc) = Dmyrhs_loc(shift+1:shift+mynloc) - (0.0_pr* bigU(:,2*j-1)+bigU(:,2*j) )*buffU0*du(ie,:,j)   
             ELSE
                Dmyrhs_loc(shift+1:shift+mynloc) = Dmyrhs_loc(shift+1:shift+mynloc) - ( -0.0_pr* bigU(:,2*j-1)+bigU(:,2*j) )*buffU0*du(ie,:,j)   
             END IF
          END DO
       END DO
  END SUBROUTINE user_convectzone_BC_Drhs 



  SUBROUTINE user_convectzone_BC_Drhs_diag (Drhs_diag_local, ulc_prev, myne_loc, mynloc, myjl, mymeth)
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: myne_loc, mynloc, myjl, mymeth
    REAL (pr), DIMENSION (mynloc*myne_loc), INTENT (INOUT) :: Drhs_diag_local
    REAL (pr), DIMENSION (mynloc,myne_loc), INTENT (IN) :: ulc_prev

    REAL (pr), DIMENSION (mynloc,dim) :: du_diag_local, d2u_diag_local
    INTEGER :: ie, i,j, shift
    REAL (pr), DIMENSION (mynloc,2*dim) :: bigU

  
    CALL c_diff_diag(du_diag_local, d2u_diag_local, j_lev, mynloc, mymeth, mymeth, 10)
!!$    CALL user_bufferfunc(bigU, mynloc, buffcvd, polyconv)
    CALL user_bufferfunc_eric(bigU, mynloc, buffcvd, polyconv)


    DO j = 1,dim
       DO ie=1,myne_loc
          shift = (ie-1)*mynloc   
!!$          Drhs_diag_local(shift+1:shift+mynloc) = Drhs_diag_local(shift+1:shift+mynloc) - (bigU(:,2)*c0(1)-bigU(:,1)*c0(2))*buffU0*du_diag_local(:,1)
          IF (j==1 .AND. BCver .NE. 0) THEN
             Drhs_diag_local(shift+1:shift+mynloc) = Drhs_diag_local(shift+1:shift+mynloc) - ( bigU(:,2*j-1) + bigU(:,2*j) )*buffU0*du_diag_local(:,j) 
          ELSE
             Drhs_diag_local(shift+1:shift+mynloc) = Drhs_diag_local(shift+1:shift+mynloc) - ( - bigU(:,2*j-1) + bigU(:,2*j) )*buffU0*du_diag_local(:,j) 
          END IF
       END DO
    END DO
  END SUBROUTINE user_convectzone_BC_Drhs_diag 

  SUBROUTINE user_bufferfunc_eric (bigU, mynloc, buffd,locpoly)
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: mynloc
    REAL (pr), DIMENSION (mynloc,2*dim), INTENT(INOUT) :: bigU
    REAL (pr), DIMENSION (5), INTENT(IN) :: buffd
    LOGICAL, INTENT(IN) :: locpoly

    REAL (pr), DIMENSION (mynloc,2*dim) :: bigU2
    INTEGER :: ie, i, j, shift, stri, stpi
    REAL (pr) :: slope, offset
    REAL (pr) :: penal_obs 

    penal_obs = 0.0_pr 
    IF(imask_obstacle) penal_obs = 1.0_pr 
    bigU(:,:)=0.0_pr
    bigU2(:,:)=0.0_pr

!Establish buffer functions for each direction - boundary order for bigU: xmin, xmax, ymin, ymax, zmin, zmax

    !slope = (ATANH( (0.5_pr-10.0_pr**-3.0_pr)/0.5_pr)- ATANH( (-0.5_pr+10.0_pr**-3.0_pr)/0.5_pr))/(buff_thick) !same algorithm as for volume penalization
    slope  =  ( ATANH(  (10.0_pr**-1.85_pr - 0.5_pr  )/0.5_pr)- ATANH(0.49_pr/0.5_pr)    ) / -buff_thick
    offset =  ATANH(0.49_pr/0.5_pr)/slope !offste distance

    DO j = 1,dim
       bigU(:,2*j-1) = MAX(0.5_pr - 10.0_pr**-1.85_pr - 0.5_pr*TANH(slope*( x(:,j) - (xyzlimits(1,j) + offset) )) , 0.0_pr )
       bigU(:,2*j) = penal_obs*MAX(0.5_pr - 10.0_pr**-1.85_pr + 0.5_pr*TANH(slope*( x(:,j) - (xyzlimits(2,j) - offset) )) , 0.0_pr )
    END DO


  END SUBROUTINE user_bufferfunc_eric 

  SUBROUTINE user_bufferfunc (bigU, mynloc, buffd,locpoly)
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: mynloc
    REAL (pr), DIMENSION (mynloc,2*dim), INTENT(INOUT) :: bigU
    REAL (pr), DIMENSION (5), INTENT(IN) :: buffd
    LOGICAL, INTENT(IN) :: locpoly

    REAL (pr), DIMENSION (mynloc,2*dim) :: bigU2
    INTEGER :: ie, i, j, shift, stri, stpi
    REAL (pr) :: topbnd, botbnd, buffsize, maxu, minu

    bigU(:,:)=0.0_pr
    bigU2(:,:)=0.0_pr

!Establish buffer functions for each direction - boundary order for bigU: xmin, xmax, ymin, ymax, zmin, zmax
    DO j = 1,dim
       buffsize=buffd(2)
       botbnd=xyzlimits(1,j)+buffd(1)
       topbnd=xyzlimits(2,j)-buffd(1)
       IF (buffsize>0.0_pr) THEN
          IF (locpoly) THEN
             bigU(:,2*j-1)=((botbnd-MIN(botbnd,MAX(botbnd-buffd(2),x(:,j))))/buffsize)**buffBeta
             bigU(:,2*j  )=((MAX(topbnd,MIN(topbnd+buffd(2),x(:,j)))-topbnd)/buffsize)**buffBeta
          ELSE
             bigU(:,2*j-1)=(TANH(((botbnd - MIN(botbnd,MAX(botbnd-buffd(2),x(:,j))))/buffsize - buffOff)*buffFac) + 1.0_pr)/2.0_pr
             bigU(:,2*j  )=(TANH(((MAX(topbnd,MIN(topbnd+buffd(2),x(:,j)))-topbnd)/buffsize - buffOff)*buffFac) + 1.0_pr)/2.0_pr
          END IF
          maxu=MAXVAL(bigU(:,2*j-1))
          minu=MINVAL(bigU(:,2*j-1))
          CALL parallel_global_sum (REALMAXVAL=maxu)
          CALL parallel_global_sum (REALMINVAL=minu)
          bigU(:,2*j-1)=(bigU(:,2*j-1)-minu)/(maxu-minu)
          maxu=MAXVAL(bigU(:,2*j))
          minu=MINVAL(bigU(:,2*j))
          CALL parallel_global_sum (REALMAXVAL=maxu)
          CALL parallel_global_sum (REALMINVAL=minu)
          bigU(:,2*j)=(bigU(:,2*j)-minu)/(maxu-minu)
       ELSEIF (botbnd>xyzlimits(1,j)) THEN
          bigU(:,2*j-1:2*j)=1.0_pr
          bigU(:,2*j-1)=(SIGN(bigU(:,j  ),botbnd-x(:,j)) + 1.0_pr)/2.0_pr
          bigU(:,2*j  )=(SIGN(bigU(:,j+1),x(:,j)-topbnd) + 1.0_pr)/2.0_pr 
       END IF
       buffsize=buffd(4)
       botbnd=xyzlimits(1,j)+(buffd(1)-buffd(2)-buffd(3))
       topbnd=xyzlimits(2,j)-(buffd(1)-buffd(2)-buffd(3))
       IF (buffsize>0.0_pr) THEN
          IF (locpoly) THEN
             bigU2(:,2*j-1)=((buffd(4)-(botbnd-MIN(botbnd,MAX(botbnd-buffd(4),x(:,1*j)))))/buffsize)**buffBeta - 1.0_pr
             bigU2(:,2*j  )=((buffd(4)-(MAX(topbnd,MIN(topbnd+buffd(4),x(:,1*j)))-topbnd))/buffsize)**buffBeta - 1.0_pr
          ELSE
             bigU2(:,2*j-1)=(TANH(-((botbnd-MIN(botbnd,MAX(botbnd-buffd(4),x(:,1*j))))/buffsize - buffOff)*buffFac) - 1.0_pr)/2.0_pr
             bigU2(:,2*j  )=(TANH(-((MAX(topbnd,MIN(topbnd+buffd(4),x(:,1*j)))-topbnd)/buffsize - buffOff)*buffFac) - 1.0_pr)/2.0_pr
          END IF
          maxu=MAXVAL(bigU2(:,2*j-1))
          minu=MINVAL(bigU2(:,2*j-1))
          CALL parallel_global_sum (REALMAXVAL=maxu)
          CALL parallel_global_sum (REALMINVAL=minu)
          bigU2(:,2*j-1)=(bigU2(:,2*j-1)-minu)/(maxu-minu)-1.0_pr
          maxu=MAXVAL(bigU2(:,2*j))
          minu=MINVAL(bigU2(:,2*j))
           CALL parallel_global_sum (REALMAXVAL=maxu)
          CALL parallel_global_sum (REALMINVAL=minu)
          bigU2(:,2*j)=(bigU2(:,2*j)-minu)/(maxu-minu)-1.0_pr
       ELSEIF (botbnd>xyzlimits(1,j)) THEN
          bigU2(:,2*j-1:2*j)=1.0_pr
          bigU2(:,2*j-1)=-(SIGN(bigU2(:,j  ),botbnd-x(:,1*j)) + 1.0_pr)/2.0_pr
          bigU2(:,2*j  )=-(SIGN(bigU2(:,j+1),x(:,1*j)-topbnd) + 1.0_pr)/2.0_pr 
       END IF
       bigU(:,2*j-1:2*j) = bigU(:,2*j-1:2*j) + bigU2(:,2*j-1:2*j)


    END DO



  END SUBROUTINE user_bufferfunc 

  SUBROUTINE user_bndfunc (bndonly, mynloc, myjl)
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: mynloc, myjl
    REAL (pr), DIMENSION (mynloc), INTENT(INOUT) :: bndonly
    INTEGER :: i
    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc
    bndonly(:) = 0.0_pr
    i_p_face(0) = 1
    DO i=1,dim
       i_p_face(i) = i_p_face(i-1)*3
    END DO
    DO face_type = 0, 3**dim - 1
       face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
       IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
          CALL get_all_indices_by_face (face_type, myjl, nloc, iloc)
          IF(nloc > 0 ) THEN
             IF (ABS(face(1)) == 1) THEN
                bndonly(iloc(1:nloc)) = 1.0_pr
             END IF
          END IF
       END IF
    END DO
  END SUBROUTINE user_bndfunc 

  SUBROUTINE user_flxfunc (flxfunc, mynloc, myjl)
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: mynloc, myjl
    REAL (pr), DIMENSION (dim,mynloc,dim), INTENT(INOUT) :: flxfunc
    REAL (pr), DIMENSION (mynloc) :: myflxfunc
    INTEGER :: i,j
       CALL user_bndfunc(myflxfunc, mynloc, myjl)  !puts 1.0 on boundaries, 0.0 elsewhere 
       DO j=1,dim
          DO i=1,dim
             !IF ((i.ne.j) .AND. (MIN(i,j).eq.1)) THEN
             IF ((i.ne.j) .AND. (j.eq.1)) THEN
                IF (globFlux==1) THEN
                   flxfunc(i,:,j) = 0.0_pr
                ELSE
                   flxfunc(i,:,j) = 1.0_pr - myflxfunc(:)
                END IF
             ELSE
                flxfunc(i,:,j) = 1.0_pr
             END IF
          END DO
       END DO
  END SUBROUTINE user_flxfunc

  SUBROUTINE user_bufferRe (ReU, mynloc)
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: mynloc
    REAL (pr), DIMENSION (mynloc), INTENT(INOUT) :: ReU
    REAL (pr), DIMENSION (mynloc,2*dim) :: bigU
    REAL (pr), DIMENSION (mynloc) :: buffmask

    IF (dobuffRe) THEN

       CALL user_bufferfunc(bigU,mynloc,buffRed,polybuffRe)
       buffmask=SUM(bigU(:,:),2)
       ReU(:) = 1.0_pr + (Re/Rebnd-1.0_pr)*buffmask(:)
    ELSE
       ReU(:) = 1.0_pr
    END IF
  END SUBROUTINE user_bufferRe 

  FUNCTION user_chi (nlocal, t_local)
    USE parallel
    USE threedobject
    IMPLICIT NONE
    INTEGER,          INTENT (IN) :: nlocal
    REAL (pr),        INTENT (IN) :: t_local
    REAL (pr), DIMENSION (nlocal) :: user_chi
    REAL (pr), DIMENSION (nlocal) :: dist_loc
    REAL(pr), DIMENSION(nlocal,1) :: forcing
    REAL(pr) :: smooth_coeff

    !PRINT *, "PENAL:", t_local
    user_chi = 0.0_pr
    IF(read_geometry) THEN
       CALL CADtest(nlocal,.FALSE.,user_chi)
    ELSE
!!$       user_chi = 0.5_pr*(1.0_pr+TANH(-( SQRT(x(:,1)**2  &
!!$        + x(:,2)**2) - 0.5_pr)/1.0e-2_pr )) 

       CALL user_dist (nlocal, t_local, DISTANCE=dist_loc)   !distance function from main code
       !CALL user_dist (nlocal, t+dt, DISTANCE=dist_loc)   !distance function from main code
       !PRINT *, MAXVAL(dist_loc),MINVAL(dist_loc)
       WHERE (dist_loc .GE. 0 )
          user_chi = 1.0_pr
       END WHERE
       
    END IF
    
!!$square obstacle
!!$   user_chi = 0.5_pr*(tanh((x(:,1)-0.4_pr))-tanh((x(:,1)-0.6_pr))) &
!!$              * 0.5_pr*(tanh((x(:,2)-0.4_pr))-tanh((x(:,2)-0.6_pr))) 


  END FUNCTION user_chi

  SUBROUTINE user_bufferzone_BC_rhs (myrhs_loc, ulc, myne_loc, mynloc, myjl, mymeth)
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: myne_loc, mynloc, myjl, mymeth
    REAL (pr), DIMENSION (mynloc*myne_loc), INTENT (INOUT) :: myrhs_loc
    REAL (pr), DIMENSION (mynloc,myne_loc), INTENT (IN) :: ulc

    INTEGER :: ie, i, j, shift, stri, stpi
    REAL (pr), DIMENSION (mynloc,2*dim) :: bigU

  
!!$    CALL user_bufferfunc(bigU, mynloc, buffbfd, polybuff)
    CALL user_bufferfunc_eric(bigU, mynloc, buffbfd, polybuff)

!!$    stri=1
!!$    stpi=myne_loc
!!$    IF (pBrink) THEN
!!$       stri=neng
!!$       stpi=neng
!!$    END IF

    DO j = 1,dim
       DO ie=1,myne_loc
          shift = (ie-1)*mynloc   
          myrhs_loc(shift+1:shift+mynloc) = myrhs_loc(shift+1:shift+mynloc) - buffSig*((bigU(:,2*j)+bigU(:,2*j-1))*ulc(:,ie)-bigU(:,2*j-1)*bndvals(ie,2*j-1)-bigU(:,2*j)*bndvals(ie,2*j)) 
       END DO
    END DO



  END SUBROUTINE user_bufferzone_BC_rhs 

  SUBROUTINE user_bufferzone_BC_Drhs (Dmyrhs_loc, ulc, ulc_prev, myne_loc, mynloc, myjl, mymeth)
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: myne_loc, mynloc, myjl, mymeth
    REAL (pr), DIMENSION (mynloc*myne_loc), INTENT (INOUT) :: Dmyrhs_loc
    REAL (pr), DIMENSION (mynloc,myne_loc), INTENT (IN) :: ulc, ulc_prev

    INTEGER :: ie, i, j, shift, stri, stpi
    REAL (pr), DIMENSION (mynloc,2*dim) :: bigU

!!$    CALL user_bufferfunc(bigU, mynloc, buffbfd, polybuff)
    CALL user_bufferfunc_eric(bigU, mynloc, buffbfd, polybuff)

!!$    stri=1
!!$    stpi=myne_loc
!!$    IF (pBrink) THEN
!!$       stri=neng
!!$       stpi=neng
!!$    END IF
    DO j = 1,dim
       DO ie = 1,myne_loc
          shift = (ie-1)*mynloc   
          Dmyrhs_loc(shift+1:shift+mynloc) = Dmyrhs_loc(shift+1:shift+mynloc) - buffSig*(bigU(:,2*j)+bigU(:,2*j-1))*ulc(:,ie)
       END DO
    END DO

  END SUBROUTINE user_bufferzone_BC_Drhs 

  SUBROUTINE user_bufferzone_BC_Drhs_diag (Drhs_diag_local, ulc_prev, myne_loc, mynloc, myjl, mymeth)
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: myne_loc, mynloc, myjl, mymeth
    REAL (pr), DIMENSION (mynloc*myne_loc), INTENT (INOUT) :: Drhs_diag_local
    REAL (pr), DIMENSION (mynloc,myne_loc), INTENT (IN) :: ulc_prev

    INTEGER :: ie, i, j, shift, stri, stpi
    REAL (pr), DIMENSION (mynloc,2*dim) :: bigU

!!$    CALL user_bufferfunc(bigU, mynloc, buffbfd, polybuff)
    CALL user_bufferfunc_eric(bigU, mynloc, buffbfd, polybuff)

!!$    stri=1
!!$    stpi=myne_loc
!!$    IF (pBrink) THEN
!!$       stri=neng
!!$       stpi=neng
!!$    END IF

    DO j = 1,dim
       DO ie=1,myne_loc
          shift = (ie-1)*mynloc   
          Drhs_diag_local(shift+1:shift+mynloc) = Drhs_diag_local(shift+1:shift+mynloc) - buffSig*(bigU(:,2*j)+bigU(:,2*j-1))
       END DO
    END DO
  END SUBROUTINE user_bufferzone_BC_Drhs_diag 

  SUBROUTINE internal_rhs (int_rhs,u_integrated,doBC,addingon)
    USE penalization
    IMPLICIT NONE
    REAL (pr), DIMENSION (ng,ne), INTENT(IN) :: u_integrated !1D flat version of integrated variables without BC 
    REAL (pr), DIMENSION (ng*ne), INTENT(INOUT) :: int_rhs
    INTEGER, INTENT(IN) :: doBC !if zero do full rhs, if one no x-derivatives 
    INTEGER, INTENT(IN) :: addingon 

    INTEGER :: i, j, l, idim, ie, shift, zeroFlux, tempintd, jshift
    INTEGER, PARAMETER :: meth=HIGH_ORDER
    REAL (pr), DIMENSION (ne*dim,ng,dim) :: du, d2u
    REAL (pr), DIMENSION (ne*dim,ng,dim) :: dudum, d2udum
    REAL (pr), DIMENSION (ne*dim,ng,dim) :: duC, d2uC,duF, d2uF,duB, d2uB,du_upwind, d2u_upwind    !forward and backwards differencing ERIC: apply to derivatives in interior domain
    REAL (pr), DIMENSION (ng,ne*dim) :: F
    REAL (pr), DIMENSION (ng,dim+Nspec) :: v ! velocity components + temperature + Y
    REAL (pr) :: usej
    REAL (pr), DIMENSION (ng) :: p
    REAL (pr), DIMENSION (ng,3) :: props
    REAL (pr), DIMENSION (ng) :: ReU

    REAL (pr) , DIMENSION(ng) :: dist_loc
    REAL (pr) , DIMENSION(ng,dim) :: obs_norm
    REAL (pr) , DIMENSION(ng) :: visc_loc
    REAL (pr) :: min_h
    REAL (pr) , DIMENSION(2,ng,dim) :: bias_mask
    INTEGER :: meth_central,meth_backward,meth_forward

    REAL (pr) :: diff_max,diff_min


    meth_central  = meth + BIASING_NONE
    meth_backward = meth + BIASING_BACKWARD
    meth_forward  = meth + BIASING_FORWARD

    min_h = MINVAL(h(1,1:dim)/2.0_pr**(REAL(j_mx,pr) - 1.0_pr))


    zeroFlux = 0
    IF (dozeroFlux .AND. (doBC+addingon)>0) zeroFlux = dervFlux + 1

    CALL user_bufferRe (ReU, ng)

    int_rhs = 0.0_pr
    IF (Nspec>1) THEN
       DO l = 1,Nspecm
          v(:,dim+1+l) = u_integrated(:,nspc(l))/u_integrated(:,nden)
       END DO
    END IF
    p(:) = 1.0_pr
    IF (Nspec>1)  p(:) = (1.0_pr-SUM(v(:,dim+2:dim+Nspec),DIM=2))  !Y_Nspec
    props(:,1) = p(:)*cp_in(Nspec) !cp_Nspec
    props(:,2) = p(:)/MW_in(Nspec) !R_Nspec
    DO l=1,Nspecm
       props(:,1) = props(:,1) + v(:,dim+1+l)*cp_in(l) !cp
       props(:,2) = props(:,2) + v(:,dim+1+l)/MW_in(l) !R
    END DO
    p(:) = (u_integrated(:,neng)-0.5_pr*SUM(u_integrated(:,nvel(1):nvel(dim))**2.0_pr,DIM=2)/u_integrated(:,nden))/(props(:,1)/props(:,2)-1.0_pr)  !pressure
    v(:,dim+1) = p(:)/props(:,2)/u_integrated(:,nden)  !Temperature

    DO i = 1, dim
       v(:,i) = u_integrated(:,nvel(i)) / u_integrated(:,nden) ! u_i
    END DO
    IF ((doBC .eq. 0) .AND. ((methpd .ne. 0) .OR. splitFBpress)) THEN  !IF doBC>0, these terms are done inside LODI subroutines,
       du(1,:,1) = p(:)
       IF (Nspec>1) THEN
          CALL dpdx (du(1,:,1),ng,v(:,dim+2:dim+Nspec),2*methpd+meth,j_lev,splitFBpress)
       ELSE
          CALL dpdx (du(1,:,1),ng,v(:,1),2*methpd+meth,j_lev,.FALSE.)
       END IF
       shift = (nvel(1)-1)*ng   
       int_rhs(shift+1:shift+ng) = int_rhs(shift+1:shift+ng)-du(1,:,1)
       shift = (neng-1)*ng   
       int_rhs(shift+1:shift+ng) = int_rhs(shift+1:shift+ng)-du(1,:,1)*v(:,1) !u*dp/dx
       CALL c_diff_fast(v(:,1), du(1,:,:), d2u(1,:,:), j_lev, ng, meth, 10, 1, 1, 1) ! du/dx
       int_rhs(shift+1:shift+ng) = int_rhs(shift+1:shift+ng)-du(1,:,1)*p(:)   !p*du/dx
    END IF
    !
    !--Form right hand side of Euler Equations
    !
    F(:,:) = 0.0_pr
    DO j = 1+doBC, dim  !iterate across dimensions
       jshift=ne*(j-1)
       F(:,nden+jshift) = F(:,nden+jshift) + u_integrated(:,nvel(j)) ! rho*u_j
       DO i = 1, dim
          F(:,nvel(i)+jshift) = F(:,nvel(i)+jshift) + u_integrated(:,nvel(i))*v(:,j) ! rho*u_i*u_j
       END DO
       F(:,nvel(j)+jshift) = F(:,nvel(j)+jshift) + p(:) ! rho*u_i^2+p
       F(:,neng+jshift) = F(:,neng+jshift) + ( u_integrated(:,neng) + p(:) )*v(:,j) ! (rho*e+p)*u_j
       DO l=1,Nspecm
          F(:,nspc(l)+jshift) = F(:,nspc(l)+jshift) + v(:,dim+1+l)*u_integrated(:,nvel(j)) ! rho*Y*u_j
       END DO  
    END DO
    IF ((doBC .eq. 0) .AND. ((methpd .ne. 0) .OR. splitFBpress )) THEN  !IF doBC>0, these terms are done inside LODI subroutines,  ******problem location?
          F(:,nvel(1)) = F(:,nvel(1)) - p(:)
          F(:,neng)    = F(:,neng)    - p(:)*v(:,1)  
    END IF
    IF (doBC .ne. 0) THEN  !Missing term from LODI assumps... gamma=constant
       CALL c_diff_fast(props(:,1)/props(:,2), du(1,:,:), d2u(1,:,:), j_lev, ng, meth, 10, 1, 1, 1)  !d(gamma/(gamma-1))/dx
       shift = (neng-1)*ng   
       int_rhs(shift+1:shift+ng) = int_rhs(shift+1:shift+ng)-du(1,:,1)*v(:,1)*p(:) !pu*d(gamma/(gamma-1))/dx
    END IF


    !
    !-- Form right hand side of Navier-Stokes Equations
    !
    IF(NS) THEN 

       CALL c_diff_fast(v, du(1:dim+Nspec,:,:), d2u(1:dim+Nspec,:,:), j_lev, ng, meth_central, 10, dim+Nspec, 1,dim+Nspec) ! du_i/dx_j & dT/dx_j & dY_l/dx_j

       IF (upwind_norm > 0.0_pr) THEN  !do upwinding near the edges of the obstacle
          CALL c_diff_fast(v, duB(1:dim+Nspec,:,:), d2uB(1:dim+Nspec,:,:), j_lev, ng, meth_backward, 10, dim+Nspec, 1,dim+Nspec) ! du_i/dx_j & dT/dx_j & dY_l/dx_j
          CALL c_diff_fast(v, duF(1:dim+Nspec,:,:), d2uF(1:dim+Nspec,:,:), j_lev, ng, meth_forward, 10, dim+Nspec, 1,dim+Nspec) ! du_i/dx_j & dT/dx_j & dY_l/dx_j
          CALL user_dist (ng, t, DISTANCE=dist_loc, NORMAL=obs_norm)   !distance function from main code

          DO i = 1,dim
             DO j = 1,dim +Nspec
                du_upwind(j,:,i) = (1.0_pr+SIGN(1.0_pr,obs_norm(:,i)))/2.0_pr*duB(j,:,i) + (1.0_pr-SIGN(1.0_pr,obs_norm(:,i)))/2.0_pr*duF(j,:,i)
                d2u_upwind(j,:,i) = (1.0_pr+SIGN(1.0_pr,obs_norm(:,i)))/2.0_pr*d2uB(j,:,i) + (1.0_pr-SIGN(1.0_pr,obs_norm(:,i)))/2.0_pr*d2uF(j,:,i)
                du(j,:,i) = (du_upwind(j,:,i)  - du(j,:,i) )*obs_norm(:,i)*MAX(dist_loc/upwind_norm/min_h + 1.0_pr ,0.0_pr) + du(j,:,i)
             END DO
          END DO

          !Create biased region
!!$          DO i = 1,dim+Nspec
!!$             DO idim = 1,dim
!!$                du(i,:,idim) = (du_upwind(i,:,idim)  - du(i,:,idim) )*obs_norm(:,idim)*MAX(dist_loc/4.0_pr/min_h + 1.0_pr ,0.0_pr) + du(i,:,idim)
!!$             END DO
!!$          END DO
       END IF



       p(:)=1.0_pr
       IF (Nspec>1) p(:)=p(:)-SUM(v(:,dim+2:dim+Nspec),DIM=2)
       
       IF(dyn_visc) THEN
          props(:,1)=p(:)*mu_in(Nspec) * (1.0_pr+S1)/(v(:,dim+1) +S1)*(v(:,dim+1))**(1.5_pr)  !mu_Nspec - variable viscosity
          props(:,2)=p(:)*kk_in(Nspec) * (1.0_pr+S1)/(v(:,dim+1) +S1)*(v(:,dim+1))**(1.5_pr) !kk_Nspec
          !PRINT *, 'mu_in', MINVAL(props(:,1)), MAXVAL(props(:,1)) 
          !PRINT *, 'kk_in', MINVAL(props(:,2)), MAXVAL(props(:,2)) 
          !PRINT *, 'Suth', MINVAL((1.0_pr+S1)/(v(:,dim+1) +S1)*(v(:,dim+1))**(1.5_pr)), MAXVAL((1.0_pr+S1)/(v(:,dim+1) +S1)*(v(:,dim+1))**(1.5_pr)) 
       ELSE
          props(:,1)=p(:)*mu_in(Nspec)  !mu_Nspec
          props(:,2)=p(:)*kk_in(Nspec)  !kk_Nspec
       END IF
       props(:,3)=p(:)*bD_in(Nspec)  !bD_Nspec
       IF (Nspec>1) THEN
          DO l=1,Nspecm
             IF(dyn_visc) THEN
                props(:,1)=props(:,1)+v(:,dim+1+l)*mu_in(l) * (1.0_pr+S1)/(v(:,dim+1) +S1)*(v(:,dim+1))**(1.5_pr)  !mu
                props(:,2)=props(:,2)+v(:,dim+1+l)*kk_in(l) * (1.0_pr+S1)/(v(:,dim+1) +S1)*(v(:,dim+1))**(1.5_pr) !kk
             ELSE
                props(:,1)=props(:,1)+v(:,dim+1+l)*mu_in(l)  !mu
                props(:,2)=props(:,2)+v(:,dim+1+l)*kk_in(l)  !kk
             END IF
             props(:,3)=props(:,3)+v(:,dim+1+l)*bD_in(l)  !bD
          END DO
       END IF
       p(:) = du(1,:,1) !reuse of a variable
       DO i = 2, dim
          p(:) = p(:) + du(i,:,i) !du_i/dx_i
       END DO
       IF (zeroFlux==1) THEN
          CALL user_flxfunc (d2u(1:dim,:,1:dim), ng, j_lev)
       ELSE
          d2u(:,:,:) = 1.0_pr
       END IF
       DO j = 1, dim
          jshift=ne*(j-1)
          DO i = 1, dim
             F(:,nvel(i)+jshift) = F(:,nvel(i)+jshift) - ReU(:)*props(:,1)*( du(i,:,j)+du(j,:,i) ) * d2u(i,:,j)  !rho*u_i*u_j - 2*mu*S_ij/Re  
          END DO
          F(:,nvel(j)+jshift) = F(:,nvel(j)+jshift) + 2.0_pr/3.0_pr*ReU(:)*props(:,1)*p(:) * d2u(j,:,j)  !rho*u_i*u_j - tau_ij/Re  [ tau_ij = 2*mu*( S_ij - 2/3*S_kk*delta_ij ) ] 
          DO i = 1, dim
             F(:,neng+jshift) =  F(:,neng+jshift) - props(:,1)*ReU(:)*( du(i,:,j)+du(j,:,i) )*v(:,i) * d2u(i,:,j)  !(e+p)*u_j - u_i*2*mu*S_ij/Re 
          END DO
          F(:,neng+jshift) =  F(:,neng+jshift) + 2.0_pr/3.0_pr*props(:,1)*ReU(:)*p(:)*v(:,j) * d2u(j,:,j) - ReU(:)*props(:,2)*du(dim+1,:,j) * d2u(2,:,j)  
                                                                                  !(e+p)*u_j - u_i*tau_ij/Re - q_j/Re/Pra [ q_j = mu dT/dx_j ]
          IF (Nspec>1) THEN
             DO l=1,Nspecm
                F(:,neng+jshift) =  F(:,neng+jshift) - u_integrated(:,nden)*cp_in(l)*v(:,dim+1)*ReU(:)*props(:,3)*du(dim+1+l,:,j) * d2u(2,:,j)
             END DO
             F(:,neng+jshift) =  F(:,neng+jshift) +  u_integrated(:,nden)*cp_in(Nspec)*v(:,dim+1)*ReU(:)*props(:,3)*SUM(du(dim+2:dim+Nspec,:,j),DIM=1) * d2u(2,:,j)
             DO l=1,Nspecm
                F(:,nspc(l)+jshift) =  F(:,nspc(l)+jshift) - u_integrated(:,nden)*ReU(:)*props(:,3)*du(dim+1+l,:,j) * d2u(2,:,j)
             END DO
          END IF
       END DO
    END IF
    IF (GRAV) THEN    
      shift = (neng-1)*ng   
      DO j=1,dim
         DO l=1,Nspecm
            int_rhs(shift+1:shift+ng) = int_rhs(shift+1:shift+ng)-v(:,dim+1+l)*u_integrated(:,nvel(j))*gr_in(l+Nspec*(j-1))
         END DO
         IF (Nspec>1) THEN
            int_rhs(shift+1:shift+ng) = int_rhs(shift+1:shift+ng) - (1.0_pr-SUM(v(:,dim+2:dim+Nspec),DIM=2))*u_integrated(:,nvel(j))*gr_in(Nspec*j)
         ELSE
            int_rhs(shift+1:shift+ng) = int_rhs(shift+1:shift+ng) - u_integrated(:,nvel(j))*gr_in(Nspec*j)
         END IF
      END DO
    END IF
    tempintd=ne*dim

    CALL c_diff_fast(F(:,1:tempintd), du(1:tempintd,:,:), d2u(1:tempintd,:,:), j_lev, ng, meth_central, 10, tempintd, 1, tempintd)  !du(i,:,k)=dF_ij/dx_k
    
    IF (upwind_norm > 0.0_pr) THEN  !do upwinding near the edges of the obstacle
       CALL c_diff_fast(F(:,1:tempintd), duB(1:tempintd,:,:), d2uB(1:tempintd,:,:), j_lev, ng, meth_backward, 10, tempintd, 1, tempintd)  !du(i,:,k)=dF_ij/dx_k
       CALL c_diff_fast(F(:,1:tempintd), duF(1:tempintd,:,:), d2uF(1:tempintd,:,:), j_lev, ng, meth_forward, 10, tempintd, 1, tempintd)  !du(i,:,k)=dF_ij/dx_k
       CALL user_dist (ng, t, DISTANCE=dist_loc, NORMAL=obs_norm)   !distance function from main code
!
       DO i = 1,dim
          DO j = 1,tempintd
             du_upwind(j,:,i) = (1.0_pr+SIGN(1.0_pr,obs_norm(:,i)))/2.0_pr*duB(j,:,i) + (1.0_pr-SIGN(1.0_pr,obs_norm(:,i)))/2.0_pr*duF(j,:,i)
             d2u_upwind(j,:,i) = (1.0_pr+SIGN(1.0_pr,obs_norm(:,i)))/2.0_pr*d2uB(j,:,i) + (1.0_pr-SIGN(1.0_pr,obs_norm(:,i)))/2.0_pr*d2uF(j,:,i)
             du(j,:,i) = (du_upwind(j,:,i)  - du(j,:,i) ) * obs_norm(:,i)*MAX(dist_loc/upwind_norm/min_h + 1.0_pr ,0.0_pr) + du(j,:,i)
          END DO
       END DO
       
       !Create biased region
!!$       DO i = 1,tempintd
!!$          DO idim = 1,dim
!!$             du(i,:,idim) = (du_upwind(i,:,idim)  - du(i,:,idim) )*obs_norm(:,idim)*MAX(dist_loc/4.0_pr/min_h + 1.0_pr ,0.0_pr) + du(i,:,idim)
!!$          END DO
!!$       END DO
    END IF
    
    DO j = 1, dim
       jshift=ne*(j-1)
       DO i = 1, dim+1+Nspec
          shift=(i-1)*ng
          int_rhs(shift+1:shift+ng) = int_rhs(shift+1:shift+ng) - du(i+jshift,:,j)  ! -dF_ij/dx_j 
       END DO
       IF (GRAV) THEN      
          shift = (nvel(j)-1)*ng

          IF (Nspec>1) THEN
             DO l=1,Nspecm
                int_rhs(shift+1:shift+ng) = int_rhs(shift+1:shift+ng) - gr_in(l+Nspec*(j-1))*u_integrated(:,nspc(l))
             END DO
          END IF
          IF (Nspec>1) THEN
             int_rhs(shift+1:shift+ng) = int_rhs(shift+1:shift+ng) - gr_in(Nspec*j)*(u_integrated(:,nden)-SUM(u_integrated(:,nspc(1):nspc(Nspecm)),DIM=2))
          ELSE
             int_rhs(shift+1:shift+ng) = int_rhs(shift+1:shift+ng) - gr_in(Nspec*j)*u_integrated(:,nden)
          END IF
       END IF
    END DO
    IF (NS .AND. zeroFlux==2) THEN
       CALL c_diff_fast(v, du(1:dim+Nspec,:,:), d2u(1:dim+Nspec,:,:), j_lev, ng, meth, 10, dim+Nspec, 1,dim+Nspec) ! du_i/dx_j & dT/dx_j & dY_l/dx_j
       DO i = 2, dim
          F(:,i-1) = ReU(:)*props(:,1)*( du(i,:,1)+du(1,:,i) )  !2*mu*S_ij/Re  
       END DO
       F(:,dim) = ReU(:)*props(:,2)*du(dim+1,:,1)  !q_j/Re/Pra [ q_j = mu dT/dx_j ]
       DO l=1,Nspecm
          F(:,dim+l) =  u_integrated(:,nden)*ReU(:)*props(:,3)*du(dim+1+l,:,1)
       END DO
       IF (Nspec>1) F(:,dim+Nspec) = -u_integrated(:,nden)*ReU(:)*props(:,3)*SUM(du(dim+2:dim+Nspec,:,1),DIM=1)
       CALL c_diff_fast(F(:,1:dim+Nspec), du(1:dim+Nspec,:,:), d2u(1:dim+Nspec,:,:), j_lev, ng, meth, 10, dim+Nspec, 1, dim+Nspec)  !du(i,:,k)=dF_ij/dx_k
       CALL user_flxfunc (d2u(1:dim,:,1:dim), ng, j_lev)
       d2u(1:dim,:,1:dim) = 1.0_pr - d2u(1:dim,:,1:dim)
       DO i=2,dim
          shift=(nvel(i)-1)*ng
          int_rhs(shift+1:shift+ng) = int_rhs(shift+1:shift+ng) - du(i-1,:,1)*d2u(2,:,1)
       END DO
       shift=(neng-1)*ng
       DO i=2,dim
          int_rhs(shift+1:shift+ng) = int_rhs(shift+1:shift+ng) - du(i-1,:,1)*v(:,i)*d2u(2,:,1)
       END DO
       int_rhs(shift+1:shift+ng) = int_rhs(shift+1:shift+ng) - du(dim,:,1)*d2u(2,:,1) 
       DO l=1,Nspec
          IF (Nspec>1) int_rhs(shift+1:shift+ng) = int_rhs(shift+1:shift+ng) - v(:,dim+1)*cp_in(l)*du(dim+l,:,1)*d2u(2,:,1)
       END DO
       DO l=1,Nspecm
          shift=(nspc(l)-1)*ng
          int_rhs(shift+1:shift+ng) = int_rhs(shift+1:shift+ng) - du(dim+l,:,1)*d2u(2,:,1) 
       END DO
    END IF

    !Add Penalization
    !circle - inward pointing normal
!!$    DO i = 1,2
!!$       obs_norm(:,i) =  -x(:,i)/((x(:,1)**2.0_pr+x(:,2)**2.0_pr)**0.5_pr)
!!$    END DO
!!$    DO i = 1,ng
!!$       IF (x(i,1) .EQ. x(i,2) .AND. x(i,2) .EQ. 0 ) THEN
!!$          obs_norm(i,:) = 0.0_pr
!!$       END IF
!!$
!!$    END DO

    
    !convecting drho/dn inside obstacle does not need to be done here
    IF(conv_drho)  int_rhs((ndrho-1)*ng+1:ndrho*ng) = 0.0_pr

  END SUBROUTINE internal_rhs

  SUBROUTINE internal_Drhs (int_Drhs, u_p, u_prev, meth,doBC,addingon)
    USE penalization
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: meth
    REAL (pr), DIMENSION (ng,ne), INTENT(IN) :: u_p, u_prev
    REAL (pr), DIMENSION (ng*ne), INTENT(INOUT) :: int_Drhs
    INTEGER, INTENT(IN) :: doBC !if zero do full rhs, if one no x-derivatives 
    INTEGER, INTENT(IN) :: addingon 

    INTEGER :: i, j, l, idim, ie, shift, zeroFlux, vshift, jshift, tempintd
    REAL (pr), DIMENSION (ne*(dim+Nspec),ng,dim) :: du, d2u
    REAL (pr), DIMENSION (ne*(dim+Nspec),ng,dim) :: dudum, d2udum
    REAL (pr), DIMENSION (ne*(dim+Nspec),ng,dim) :: duC, d2uC,duF, d2uF,duB, d2uB,du_upwind, d2u_upwind    !forward and backwards differencing
    REAL (pr), DIMENSION (ng,ne*dim) :: F
    REAL (pr), DIMENSION (ng,(dim+Nspec)*2) :: v_prev ! velocity components + temperature
    REAL (pr), DIMENSION (ng) :: p, p_prev
    REAL (pr), DIMENSION (ng,3) :: props, props_prev
    REAL (pr), DIMENSION (ng) :: ReU

    REAL (pr) , DIMENSION(ng) :: dist_loc
    REAL (pr) , DIMENSION(ng,dim) :: obs_norm
    REAL (pr) :: min_h
    REAL (pr) , DIMENSION(ng) :: visc_loc
    INTEGER :: meth_central,meth_backward,meth_forward
    REAL (pr) :: diff_max,diff_min
    REAL (pr) , DIMENSION(2,ng,dim) :: bias_mask


    meth_central  = meth + BIASING_NONE
    meth_backward = meth + BIASING_BACKWARD
    meth_forward  = meth + BIASING_FORWARD

    min_h = MINVAL(h(1,1:dim)/2.0_pr**(REAL(j_mx,pr) - 1.0_pr))

    zeroFlux = 0
    IF (dozeroFlux .AND. (doBC+addingon)>0) zeroFlux = dervFlux + 1

    CALL user_bufferRe (ReU, ng)

    int_Drhs = 0.0_pr
    vshift = dim+Nspec
    DO l=1,Nspecm
       v_prev(:,dim+1+l) = u_prev(:,nspc(l))/u_prev(:,nden)
       v_prev(:,vshift+dim+1+l) = u_p(:,nspc(l))/u_prev(:,nden) - u_prev(:,nspc(l))*u_p(:,nden)/u_prev(:,nden)**2.0_pr
    END DO
    p_prev(:) = 1.0_pr
    p(:) = 0.0_pr
    IF (Nspec>1) p_prev(:) = (p_prev(:)-SUM(v_prev(:,dim+2:dim+Nspec),DIM=2))  !Y0_Nspec
    IF (Nspec>1) p(:)      =           -SUM(v_prev(:,vshift+dim+2:vshift+dim+Nspec),DIM=2)  !Y'_Nspec
    props_prev(:,1) = p_prev(:)*cp_in(Nspec) !cp0_Nspec
    props_prev(:,2) = p_prev(:)/MW_in(Nspec) !R0_Nspec
    props(:,1)      = p(:)*cp_in(Nspec) !cp'_Nspec
    props(:,2)      = p(:)/MW_in(Nspec) !R'_Nspec
    DO l=1,Nspecm
       props_prev(:,1) = props_prev(:,1) + v_prev(:,dim+1+l)*cp_in(l) !cp0
       props_prev(:,2) = props_prev(:,2) + v_prev(:,dim+1+l)/MW_in(l) !R0
       props(:,1)      = props(:,1)      + v_prev(:,vshift+dim+1+l)*cp_in(l) !cp'
       props(:,2)      = props(:,2)      + v_prev(:,vshift+dim+1+l)/MW_in(l) !R'
    END DO
    p_prev(:) = (u_prev(:,neng)-0.5_pr*SUM(u_prev(:,nvel(1):nvel(dim))**2.0_pr,DIM=2)/u_prev(:,nden))/(props_prev(:,1)/props_prev(:,2)-1.0_pr)  !pressure
    p(:) = (u_prev(:,neng)-0.5_pr*SUM(u_prev(:,nvel(1):nvel(dim))**2.0_pr,DIM=2)/u_prev(:,nden)) * (props_prev(:,1)*props(:,2)-props(:,1)*props_prev(:,2)) / (props_prev(:,1)-props_prev(:,2))**2.0_pr + &
           (u_p(:,neng)+0.5_pr*SUM(u_prev(:,nvel(1):nvel(dim))**2.0_pr,DIM=2)/u_prev(:,nden)**2.0_pr*u_p(:,nden)-SUM(u_prev(:,nvel(1):nvel(dim))*u_p(:,nvel(1):nvel(dim)),DIM=2)/u_prev(:,nden)) /  &
             (props_prev(:,1)/props_prev(:,2)-1.0_pr) 
    v_prev(:,dim+1) = p_prev(:)/props_prev(:,2)/u_prev(:,nden)  !Temperature
    v_prev(:,vshift+dim+1) = (p(:)-p_prev(:)*(props(:,2)/props_prev(:,2)+u_p(:,nden)/u_prev(:,nden)))/props_prev(:,2)/u_prev(:,nden)  !Temperature
    DO i = 1, dim
       v_prev(:,i) = u_prev(:,nvel(i)) / u_prev(:,nden) ! u_i
       v_prev(:,vshift+i) = u_p(:,nvel(i))/u_prev(:,nden) - u_prev(:,nvel(i))*u_p(:,nden)/u_prev(:,nden)**2.0_pr  !u_i
    END DO
    IF ((doBC .eq. 0) .AND. ((methpd .ne. 0) .OR. splitFBpress)) THEN  !IF doBC>0, these terms are done inside LODI subroutines
       du(1,:,1) = p(:)
       du(1,:,2) = p_prev(:)
       IF (Nspec>1) THEN
          CALL dpdx (du(1,:,1),ng,v_prev(:,dim+2:dim+Nspec),2*methpd+meth,j_lev,splitFBpress)
       ELSE
          CALL dpdx (du(1,:,1),ng,v_prev(:,1),2*methpd+meth,j_lev,.FALSE.)
       END IF
       shift = (nvel(1)-1)*ng
       int_Drhs(shift+1:shift+ng) = int_Drhs(shift+1:shift+ng)-du(1,:,1)
       IF (Nspec>1) THEN
          CALL dpdx (du(1,:,2),ng,v_prev(:,dim+2:dim+Nspec),2*methpd+meth,j_lev,splitFBpress)
       ELSE
          CALL dpdx (du(1,:,2),ng,v_prev(:,1),2*methpd+meth,j_lev,.FALSE.)
       END IF
       shift = (neng-1)*ng
       int_Drhs(shift+1:shift+ng) = int_Drhs(shift+1:shift+ng)-du(1,:,1)*v_prev(:,1)-du(1,:,2)*v_prev(:,vshift+1) !u0*dp'/dx+u'*dp0/dx
       CALL c_diff_fast(v_prev(:,vshift+1), du(1,:,:), d2u(1,:,:), j_lev, ng, meth, 10, 1, 1, 1) ! du'/dx
       int_Drhs(shift+1:shift+ng) = int_Drhs(shift+1:shift+ng)-du(1,:,1)*p_prev(:)   !p0*du'/dx
       CALL c_diff_fast(v_prev(:,1), du(1,:,:), d2u(1,:,:), j_lev, ng, meth, 10, 1, 1, 1) ! du0/dx
       int_Drhs(shift+1:shift+ng) = int_Drhs(shift+1:shift+ng)-du(1,:,1)*p(:)   !p'*du0/dx
    END IF
    !
    !--Form right hand side of Euler Equations
    !
    F(:,:) = 0.0_pr
    DO j = 1+doBC, dim
       jshift=ne*(j-1)
       F(:,nden+jshift) = F(:,nden+jshift) + u_p(:,nvel(j)) ! rho*u_j
       DO i = 1, dim ! (rho*u_i*u_j)'
          F(:,nvel(i)+jshift) = F(:,nvel(i)+jshift) + u_prev(:,nvel(i))*v_prev(:,vshift+j) + u_p(:,nvel(i))*v_prev(:,j)
       END DO
       F(:,nvel(j)+jshift) = F(:,nvel(j)+jshift) + p(:) ! (rho*u_i^2)'+p'
       F(:,neng+jshift) = F(:,neng+jshift) + (u_p(:,neng) + p(:))*v_prev(:,j) + (u_prev(:,neng) + p_prev(:))*v_prev(:,vshift+j)  ! ((e+p)*u_j)'
       DO l=1,Nspecm
          F(:,nspc(l)+jshift) = F(:,nspc(l)+jshift) + v_prev(:,vshift+dim+1+l)*u_prev(:,nvel(j))+v_prev(:,dim+1+l)*u_p(:,nvel(j))
       END DO
    END DO
    IF ((doBC .eq. 0) .AND. ((methpd .ne. 0) .OR. splitFBpress)) THEN  !IF doBC>0, these terms are done inside LODI subroutines
       F(:,nvel(1)) = F(:,nvel(1)) - p(:)
       F(:,neng)    = F(:,neng)    - p_prev(:)*v_prev(:,vshift+1) - p(:)*v_prev(:,1)
    END IF
    IF (doBC .ne. 0) THEN  !Missing term from LODI assumps... gamma=constant
       shift = (neng-1)*ng
       props_prev(:,3)=props_prev(:,1)/props_prev(:,2)  !gamma/(gamma-1)0
       CALL c_diff_fast(props_prev(:,3), du(1,:,:), d2u(1,:,:), j_lev, ng, meth, 10, 1, 1, 1)  !d(gamma/(gamma-1))/dx
       int_Drhs(shift+1:shift+ng) = int_Drhs(shift+1:shift+ng)-du(1,:,1)*(v_prev(:,1)*p(:)+v_prev(:,vshift+1)*p_prev(:)) !pu*d(gamma/(gamma-1))/dx
       props(:,3)=((props(:,1)*props_prev(:,2)-props_prev(:,1)*props(:,2))/props_prev(:,2)**2.0_pr) !gamma/(gamma-1)'
       CALL c_diff_fast(props(:,3), du(1,:,:), d2u(1,:,:), j_lev, ng, meth, 10, 1, 1, 1)  !d(gamma/(gamma-1))/dx
       int_Drhs(shift+1:shift+ng) = int_Drhs(shift+1:shift+ng)-du(1,:,1)*v_prev(:,1)*p_prev(:) !pu*d(gamma/(gamma-1))/dx
    END IF

    !
    !-- Form right hand side of Navier-Stokes Equations
    !
    IF(NS) THEN 
       tempintd = 2*vshift

       CALL c_diff_fast(v_prev(:,:), du(1:tempintd,:,:), d2u(1:tempintd,:,:), j_lev, ng, meth_central, 10, tempintd, 1, tempintd) ! du'_i/dx_j & dT'/dx_j

       IF (upwind_norm > 0.0_pr) THEN  !do upwinding near the edges of the obstacle
          CALL c_diff_fast(v_prev(:,:), duB(1:tempintd,:,:), d2uB(1:tempintd,:,:), j_lev, ng, meth_backward, 10, tempintd, 1, tempintd) ! du'_i/dx_j & dT'/dx_j
          CALL c_diff_fast(v_prev(:,:), duF(1:tempintd,:,:), d2uF(1:tempintd,:,:), j_lev, ng, meth_forward, 10, tempintd, 1, tempintd) ! du'_i/dx_j & dT'/dx_j
          
          CALL user_dist (ng, t, DISTANCE=dist_loc, NORMAL=obs_norm)   !distance function from main code
          
          DO i = 1,dim
             DO j = 1,tempintd
                du_upwind(j,:,i) = (1.0_pr+SIGN(1.0_pr,obs_norm(:,i)))/2.0_pr*duB(j,:,i) + (1.0_pr-SIGN(1.0_pr,obs_norm(:,i)))/2.0_pr*duF(j,:,i)
                d2u_upwind(j,:,i) = (1.0_pr+SIGN(1.0_pr,obs_norm(:,i)))/2.0_pr*d2uB(j,:,i) + (1.0_pr-SIGN(1.0_pr,obs_norm(:,i)))/2.0_pr*d2uF(j,:,i)
                du(j,:,i) = (du_upwind(j,:,i)  - du(j,:,i) )*obs_norm(:,i)*MAX(dist_loc/upwind_norm/min_h + 1.0_pr ,0.0_pr) + du(j,:,i)
             END DO
          END DO
          
          !Create biased region
!!$       DO i = 1,tempintd
!!$          DO idim = 1,dim
!!$             du(i,:,idim) = (du_upwind(i,:,idim)  - du(i,:,idim) )*obs_norm(:,idim)*MAX(dist_loc/4.0_pr/min_h + 1.0_pr ,0.0_pr) + du(i,:,idim)
!!$          END DO
!!$       END DO
       END IF
    
       p_prev(:) = 1.0_pr
       p(:) = 0.0_pr
       IF (Nspec>1) p_prev(:) = (p_prev(:)-SUM(v_prev(:,dim+2:dim+Nspec),DIM=2))  !Y0_Nspec
       IF (Nspec>1) p(:)      =           -SUM(v_prev(:,vshift+dim+2:vshift+dim+Nspec),DIM=2)  !Y'_Nspec
       IF(dyn_visc) THEN
          props_prev(:,1) = p_prev(:)*mu_in(Nspec) * (1.0_pr+S1)/(v_prev(:,dim+1) +S1)*(v_prev(:,dim+1))**(1.5_pr)  !mu_Nspec - variable viscosity
          props(:,1)      = p_prev(:)*mu_in(Nspec) * ((1.5_pr*(1.0_pr+S1)*v_prev(:,dim+1)**0.5_pr*v_prev(:,vshift+dim+1))/(v_prev(:,dim+1)+S1) &
            + v_prev(:,dim+1)**1.5_pr*( -(1.0_pr+S1)*v_prev(:,vshift+dim+1) )/(v_prev(:,dim+1)+S1)**2.0_pr) !mu'_Nspec !ERIC: double check for accuracy
          props_prev(:,2) = p_prev(:)*kk_in(Nspec) * (1.0_pr+S1)/(v_prev(:,dim+1) +S1)*(v_prev(:,dim+1))**(1.5_pr)  !kk0_Nspec
          props(:,2)      = p_prev(:)*kk_in(Nspec) * ((1.5_pr*(1.0_pr+S1)*v_prev(:,dim+1)**0.5_pr*v_prev(:,vshift+dim+1))/(v_prev(:,dim+1)+S1) &
            + v_prev(:,dim+1)**1.5_pr*( -(1.0_pr+S1)*v_prev(:,vshift+dim+1) )/(v_prev(:,dim+1)+S1)**2.0_pr)  !kk'_Nspec
          !PRINT *, 'mu_in', MINVAL(props_prev(:,1)), MAXVAL(props_prev(:,1)),MINVAL(props_prev(:,1)), MAXVAL(props_prev(:,1)) 
          !PRINT *, 'kk_in', MINVAL(props_prev(:,2)), MAXVAL(props_prev(:,2)) ,MINVAL(props(:,2)), MAXVAL(props(:,2))
          !PRINT *, 'Suth_p', MINVAL(), MAXVAL()
       ELSE
          props_prev(:,1) = p_prev(:)*mu_in(Nspec)  !mu0_Nspec
          props(:,1)      = p(:)*mu_in(Nspec)  !mu'_Nspec
          props_prev(:,2) = p_prev(:)*kk_in(Nspec)  !kk0_Nspec
          props(:,2)      = p(:)*kk_in(Nspec) 
       END IF
       props_prev(:,3) = p_prev(:)*bD_in(Nspec)  !bD0_Nspec
       props(:,3)      = p(:)*bD_in(Nspec)  !bD'_Nspec
       DO l=1,Nspecm
          IF(dyn_visc) THEN
             props_prev(:,1) = props_prev(:,1) + v_prev(:,dim+1+l)*mu_in(l) * (1.0_pr+S1)/(v_prev(:,dim+1) +S1)*(v_prev(:,dim+1))**(1.5_pr)  !mu0
             props_prev(:,2) = props_prev(:,2) + v_prev(:,dim+1+l)*kk_in(l) * (1.0_pr+S1)/(v_prev(:,dim+1) +S1)*(v_prev(:,dim+1))**(1.5_pr)  !kk0
             props(:,1)      = props(:,1)      + v_prev(:,vshift+dim+1+l)*mu_in(l) * ((1.5_pr*(1.0_pr+S1)*v_prev(:,dim+1)**0.5_pr*v_prev(:,vshift+dim+1))/(v_prev(:,dim+1)+S1) &
              + v_prev(:,dim+1)**1.5_pr*( -(1.0_pr+S1)*v_prev(:,vshift+dim+1) )/(v_prev(:,dim+1)+S1)**2.0_pr)  !mu'
             props(:,2)      = props(:,2)      + v_prev(:,vshift+dim+1+l)*kk_in(l) * ((1.5_pr*(1.0_pr+S1)*v_prev(:,dim+1)**0.5_pr*v_prev(:,vshift+dim+1))/(v_prev(:,dim+1)+S1) &
              + v_prev(:,dim+1)**1.5_pr*( -(1.0_pr+S1)*v_prev(:,vshift+dim+1) )/(v_prev(:,dim+1)+S1)**2.0_pr)  !kk'
          ELSE
             props_prev(:,1) = props_prev(:,1) + v_prev(:,dim+1+l)*mu_in(l)   !mu0
             props_prev(:,2) = props_prev(:,2) + v_prev(:,dim+1+l)*kk_in(l)   !kk0
             props(:,1)      = props(:,1)      + v_prev(:,vshift+dim+1+l)*mu_in(l)   !mu'
             props(:,2)      = props(:,2)      + v_prev(:,vshift+dim+1+l)*kk_in(l)   !kk'
          END IF
          props_prev(:,3) = props_prev(:,3) + v_prev(:,dim+1+l)*bD_in(l)  !bD0
          props(:,3)      = props(:,3)      + v_prev(:,vshift+dim+1+l)*bD_in(l)  !bD'
       END DO
       p(:) = du(vshift+1,:,1) !reuse of a variable
       DO i = 2, dim
          p(:) = p(:) + du(vshift+i,:,i) !du'_i/dx_i
       END DO
       IF (zeroFlux==1) THEN
          CALL user_flxfunc (d2u(1:dim,:,1:dim), ng, j_lev)
       ELSE
          d2u(:,:,:) = 1.0_pr
       END IF
       DO j = 1, dim
          jshift=ne*(j-1)
          DO i = 1, dim
             F(:,nvel(i)+jshift) = F(:,nvel(i)+jshift) - ReU(:)*props_prev(:,1)*( du(vshift+i,:,j)+du(vshift+j,:,i) ) * d2u(i,:,j)  !rho*u_i*u_j - 2*mu*S_ij/Re
          END DO
          F(:,nvel(j)+jshift) = F(:,nvel(j)+jshift) + 2.0_pr/3.0_pr*ReU(:)*props_prev(:,1)*p(:) * d2u(j,:,j) !rho*u_i*u_j - tau_ij/Re  [ tau_ij = 2*mu*( S_ij - 2/3*S_kk*delta_ij ) ]
          DO i = 1, dim
             F(:,neng+jshift) =  F(:,neng+jshift) - props_prev(:,1)*ReU(:)*( du(vshift+i,:,j)+du(vshift+j,:,i) )*v_prev(:,i) * d2u(i,:,j)  !(e+p)*u_j - u_i*2*mu*S_ij/Re
          END DO
          F(:,neng+jshift) =  F(:,neng+jshift) + 2.0_pr/3.0_pr*props_prev(:,1)*ReU(:)*p(:)*v_prev(:,j) * d2u(j,:,j) - ReU(:)*props_prev(:,2)*du(vshift+dim+1,:,j) * d2u(2,:,j)  
                                                                                             !(e+p)*u_j - u_i*tau_ij/Re - q_j/Re/Pra [ q_j = mu dT/dx_j ]
          DO l=1,Nspecm
             F(:,neng+jshift) =  F(:,neng+jshift) - u_prev(:,nden)*cp_in(l)*v_prev(:,dim+1)*ReU(:)*props_prev(:,3)*du(vshift+dim+1+l,:,j) * d2u(2,:,j)
          END DO
          IF (Nspec>1) F(:,neng+jshift) =  F(:,neng+jshift) +  u_prev(:,nden)*cp_in(Nspec)*v_prev(:,dim+1)*ReU(:)*props_prev(:,3)*SUM(du(vshift+dim+2:vshift+dim+Nspec,:,j),DIM=1) * d2u(2,:,j)
          DO l=1,Nspecm
             F(:,nspc(l)+jshift) =  F(:,nspc(l)+jshift) - u_prev(:,nden)*ReU(:)*props_prev(:,3)*du(vshift+dim+1+l,:,j) * d2u(2,:,j)
          END DO
       END DO

       p_prev(:) = du(1,:,1) !reuse of a variable
       DO i = 2, dim
          p_prev(:) = p_prev(:) + du(i,:,i) !du0_i/dx_i
       END DO
       DO j = 1, dim
          jshift=ne*(j-1)
          DO i = 1, dim
             F(:,nvel(i)+jshift) = F(:,nvel(i)+jshift) - ReU(:)*props(:,1)*( du(i,:,j)+du(j,:,i) ) * d2u(i,:,j)  !rho*u_i*u_j - 2*mu*S_ij/Re
          END DO
          F(:,nvel(j)+jshift) = F(:,nvel(j)+jshift) + 2.0_pr/3.0_pr*ReU(:)*props(:,1)*p_prev(:) * d2u(j,:,j) !rho*u_i*u_j - tau_ij/Re  [ tau_ij = 2*mu*( S_ij - 2/3*S_kk*delta_ij ) ]
          DO i = 1, dim
             F(:,neng+jshift) =  F(:,neng+jshift) - ReU(:)*( du(i,:,j)+du(j,:,i) )*(v_prev(:,i)*props(:,1)+v_prev(:,vshift+i)*props_prev(:,1)) * d2u(i,:,j)  !(e+p)*u_j - u_i*2*mu*S_ij/Re
          END DO
          F(:,neng+jshift) =  F(:,neng+jshift) + 2.0_pr/3.0_pr*ReU(:)*p_prev(:)*(v_prev(:,j)*props(:,1)+v_prev(:,vshift+j)*props_prev(:,1)) * d2u(j,:,j) - ReU(:)*props(:,2)*du(dim+1,:,j) * d2u(2,:,j)  
                                                                                             !(e+p)*u_j - u_i*tau_ij/Re - q_j/Re/Pra [ q_j = mu dT/dx_j ]
          DO l=1,Nspecm
             F(:,neng+jshift) =  F(:,neng+jshift) - cp_in(l)*ReU(:)*du(dim+1+l,:,j) * &
                             ( u_p(:,nden)*v_prev(:,dim+1)*props_prev(:,3) + u_prev(:,nden)*(v_prev(:,vshift+dim+1)*props_prev(:,3) + v_prev(:,dim+1)*props(:,3)) ) * d2u(2,:,j)
          END DO
          IF (Nspec>1) F(:,neng+jshift) =  F(:,neng+jshift) +  cp_in(Nspec)*ReU(:)*SUM(du(dim+2:dim+Nspec,:,j),DIM=1) * &
                             ( u_p(:,nden)*v_prev(:,dim+1)*props_prev(:,3) + u_prev(:,nden)*(v_prev(:,vshift+dim+1)*props_prev(:,3) + v_prev(:,dim+1)*props(:,3)) ) * d2u(2,:,j)
          DO l=1,Nspecm
             F(:,nspc(l)+jshift) =  F(:,nspc(l)+jshift) - ReU(:)*(u_p(:,nden)*props_prev(:,3)+u_prev(:,nden)*props(:,3))*du(dim+1+l,:,j) * d2u(2,:,j)
          END DO
       END DO
    END IF
    IF (GRAV) THEN
      shift = (neng-1)*ng
      DO j=1,dim
         DO l=1,Nspecm
            int_Drhs(shift+1:shift+ng) = int_Drhs(shift+1:shift+ng)-(v_prev(:,dim+1+l)*u_p(:,nvel(j))+v_prev(:,vshift+dim+1+l)*u_prev(:,nvel(j)))*gr_in(l+Nspec*(j-1))
         END DO
         IF (Nspec>1) THEN
            int_Drhs(shift+1:shift+ng) = int_Drhs(shift+1:shift+ng) - ((1.0_pr-SUM(v_prev(:,dim+2:dim+Nspec),DIM=2))*u_p(:,nvel(j))-SUM(v_prev(:,vshift+dim+2:vshift+dim+Nspec),DIM=2)*u_prev(:,nvel(j)) )*gr_in(Nspec*j)
         ELSE
            int_Drhs(shift+1:shift+ng) = int_Drhs(shift+1:shift+ng) - u_p(:,nvel(j))*gr_in(Nspec*j)
         END IF
      END DO
   END IF
    tempintd=ne*dim

    CALL c_diff_fast(F(:,1:tempintd), du(1:tempintd,:,:), d2u(1:tempintd,:,:), j_lev, ng, meth_central, 10, tempintd, 1, tempintd)  !du(i,:,k)=dF_ij/dx_k

    IF (upwind_norm > 0.0_pr) THEN  !do upwinding near the edges of the obstacle
       CALL c_diff_fast(F(:,1:tempintd), duB(1:tempintd,:,:), d2uB(1:tempintd,:,:), j_lev, ng, meth_backward, 10, tempintd, 1, tempintd)  !du(i,:,k)=dF_ij/dx_k
       CALL c_diff_fast(F(:,1:tempintd), duF(1:tempintd,:,:), d2uF(1:tempintd,:,:), j_lev, ng, meth_forward, 10, tempintd, 1, tempintd)  !du(i,:,k)=dF_ij/dx_k
       CALL user_dist (ng, t, DISTANCE=dist_loc, NORMAL=obs_norm)   !distance function from main code

       DO i = 1,dim
          DO j = 1,tempintd
             du_upwind(j,:,i) = (1.0_pr+SIGN(1.0_pr,obs_norm(:,i)))/2.0_pr*duB(j,:,i) + (1.0_pr-SIGN(1.0_pr,obs_norm(:,i)))/2.0_pr*duF(j,:,i)
             d2u_upwind(j,:,i) = (1.0_pr+SIGN(1.0_pr,obs_norm(:,i)))/2.0_pr*d2uB(j,:,i) + (1.0_pr-SIGN(1.0_pr,obs_norm(:,i)))/2.0_pr*d2uF(j,:,i)
             du(j,:,i) = (du_upwind(j,:,i)  - du(j,:,i) )*obs_norm(:,i)*MAX(dist_loc/upwind_norm/min_h + 1.0_pr ,0.0_pr) + du(j,:,i)
          END DO
       END DO
       
       !Create biased region
!!$       DO i = 1,tempintd
!!$          DO idim = 1,dim
!!$             du(i,:,idim) = (du_upwind(i,:,idim)  - du(i,:,idim) )*obs_norm(:,idim)*MAX(dist_loc/4.0_pr/min_h + 1.0_pr ,0.0_pr) + du(i,:,idim)
!!$          END DO
!!$       END DO
    END IF    

    DO j = 1, dim
       jshift=ne*(j-1)
       DO i = 1, dim+1+Nspec
          shift=(i-1)*ng
          int_Drhs(shift+1:shift+ng) = int_Drhs(shift+1:shift+ng) - du(i+jshift,:,j)  ! -dF_ij/dx_j  !continutiy set up
       END DO
       IF (GRAV) THEN      
          shift = (nvel(j)-1)*ng
          DO l=1,Nspecm        
             int_Drhs(shift+1:shift+ng) = int_Drhs(shift+1:shift+ng) - gr_in(l+Nspec*(j-1))*u_p(:,nspc(l))
          END DO
          IF (Nspec>1) THEN
             int_Drhs(shift+1:shift+ng) = int_Drhs(shift+1:shift+ng) - gr_in(Nspec*j)*(u_p(:,nden)-SUM(u_p(:,nspc(1):nspc(Nspecm)),DIM=2))
          ELSE
             int_Drhs(shift+1:shift+ng) = int_Drhs(shift+1:shift+ng) - gr_in(Nspec*j)*u_p(:,nden)
          END IF
       END IF
    END DO
    IF (NS .AND. zeroFlux==2) THEN
       tempintd = 2*vshift
       CALL c_diff_fast(v_prev(:,:), du(1:tempintd,:,:), d2u(1:tempintd,:,:), j_lev, ng, meth, 10, tempintd, 1, tempintd) ! du'_i/dx_j & dT'/dx_j
       DO i = 2, dim
          F(:,i-1) = ReU(:)*props_prev(:,1)*( du(vshift+i,:,1)+du(vshift+1,:,i) )  !2*mu*S_ij/Re
       END DO
       F(:,dim) = ReU(:)*props_prev(:,2)*du(vshift+dim+1,:,1)  !q_j/Re/Pra [ q_j = mu dT/dx_j ]
       DO l=1,Nspecm
          F(:,dim+l) =  u_prev(:,nden)*ReU(:)*props_prev(:,3)*du(vshift+dim+1+l,:,1)
       END DO
       IF (Nspec>1) F(:,dim+Nspec) =  -u_prev(:,nden)*ReU(:)*props_prev(:,3)*SUM(du(vshift+dim+2:vshift+dim+Nspec,:,1),DIM=1)

       DO i = 2, dim
          F(:,i-1) = F(:,i-1) + ReU(:)*props(:,1)*( du(i,:,1)+du(1,:,i) )  !2*mu*S_ij/Re
       END DO
       F(:,dim) = F(:,dim) + ReU(:)*props(:,2)*du(dim+1,:,1)  !q_j/Re/Pra [ q_j = mu dT/dx_j ]
       DO l=1,Nspecm
          F(:,dim+l) = F(:,dim+l) + ReU(:)*(u_prev(:,nden)*props(:,3)+u_p(:,nden)*props_prev(:,3))*du(dim+1+l,:,1)
       END DO
       IF (Nspec>1) F(:,dim+Nspec) =  F(:,dim+Nspec) - ReU(:)*(u_prev(:,nden)*props(:,3)+u_p(:,nden)*props_prev(:,3))*SUM(du(dim+2:dim+Nspec,:,1),DIM=1)
       DO i = 2, dim
          F(:,i-1+ne) = ReU(:)*props_prev(:,1)*( du(i,:,1)+du(1,:,i) )  !2*mu*S_ij/Re
       END DO
       F(:,dim+ne) = ReU(:)*props_prev(:,2)*du(dim+1,:,1)  !q_j/Re/Pra [ q_j = mu dT/dx_j ]
       DO l=1,Nspecm
          F(:,dim+l+ne) = ReU(:)*u_prev(:,nden)*props_prev(:,3)*du(dim+1+l,:,1)
       END DO
       IF (Nspec>1) F(:,dim+Nspec+ne) = -ReU(:)*u_prev(:,nden)*props_prev(:,3)*SUM(du(dim+2:dim+Nspec,:,1),DIM=1)

       tempintd = 2*vshift
       CALL c_diff_fast(F(:,1:tempintd), du(1:tempintd,:,:), d2u(1:tempintd,:,:), j_lev, ng, meth, 10, tempintd, 1, tempintd)
       CALL user_flxfunc (d2u(1:dim,:,1:dim), ng, j_lev)
       d2u(1:dim,:,1:dim) = 1.0_pr - d2u(1:dim,:,1:dim)
       DO i=2,dim
          shift=(nvel(i)-1)*ng
          int_Drhs(shift+1:shift+ng) = int_Drhs(shift+1:shift+ng) - du(i-1,:,1)*d2u(2,:,1)
       END DO
       shift=(neng-1)*ng
       DO i=2,dim
          int_Drhs(shift+1:shift+ng) = int_Drhs(shift+1:shift+ng) - du(i-1,:,1)*v_prev(:,i)*d2u(2,:,1)
       END DO
       int_Drhs(shift+1:shift+ng) = int_Drhs(shift+1:shift+ng) - du(dim,:,1)*d2u(2,:,1)
       DO l=1,Nspec
          IF (Nspec>1) int_Drhs(shift+1:shift+ng) = int_Drhs(shift+1:shift+ng) - v_prev(:,dim+1)*cp_in(l)*du(dim+l,:,1)*d2u(2,:,1)
       END DO
       DO l=1,Nspecm
          shift=(nspc(l)-1)*ng 
          int_Drhs(shift+1:shift+ng) = int_Drhs(shift+1:shift+ng) - du(dim+l,:,1)*d2u(2,:,1)
       END DO

       shift=(neng-1)*ng
       DO i=2,dim
          int_Drhs(shift+1:shift+ng) = int_Drhs(shift+1:shift+ng) - du(i-1+ne,:,1)*v_prev(:,vshift+i)*d2u(2,:,1)
       END DO
       DO l=1,Nspec
          IF (Nspec>1) int_Drhs(shift+1:shift+ng) = int_Drhs(shift+1:shift+ng) - v_prev(:,vshift+dim+1)*cp_in(l)*du(dim+l+ne,:,1)*d2u(2,:,1)
       END DO
    END IF

    !Add Brinkman Penalization Terms

!!$    IF (obstacle == .TRUE. .AND. t<4.0_pr .AND. IC_restart_mode .NE. 3) THEN  !add brinkman term to the continuity equation
!!$       DO i = 1,dim
!!$          int_Drhs((nvel(i)-1)*ng+1:nvel(i)*ng) = int_Drhs((nvel(i)-1)*ng+1:nvel(i)*ng) -penal*(0.25_pr*t)**2.0_pr/eta_b*v_prev(:,vshift+i)-penal*(1.34_pr*t)/eta_b*v_prev(:,i)
!!$       END DO
!!$       int_Drhs((neng-1)*ng+1:neng*ng) = int_Drhs((neng-1)*ng+1:neng*ng)- penal*(0.25_pr*t)**2.0_pr/eta_b*v_prev(:,vshift+dim+1)- penal*(1.34_pr*t)/eta_b*v_prev(:,dim+1)!check if temperature in subroutine is formed same as TT in brinkman
    IF (imask_obstacle == .TRUE.) THEN

       !du = 0.0_pr
       !d2u = 0.0_pr
       !CALL c_diff_fast(u_p, du(1:ne, :, :), d2u(1:ne, :, :), j_lev, ng, meth, 10, ne, 1, ne)

       !int_Drhs((nden-1)*ng+1:nden*ng) = int_Drhs((nden-1)*ng+1:nden*ng) - penal/porosity*0.5_pr*(TANH(20.0_pr*((x(:,1)**2.0_pr+x(:,2)**2.0_pr)**0.5_pr - 0.25_pr)) + 1.0_pr) * SUM(du(nden,:,1:2)*obs_norm(:,1:2),DIM=2)


       IF(brinkman_penal) THEN
          DO i = 1,dim
             !internal rhs, not user_rhs
             int_Drhs((nvel(i)-1)*ng+1:nvel(i)*ng) = int_Drhs((nvel(i)-1)*ng+1:nvel(i)*ng) -penal/eta_b*v_prev(:,vshift+i)
          END DO
          int_Drhs((neng-1)*ng+1:neng*ng) = int_Drhs((neng-1)*ng+1:neng*ng)- penal/eta_b*(v_prev(:,vshift+dim+1))!/porosity*(TANH(20.0_pr*((x(:,1)**2.0_pr+x(:,2)**2.0_pr)**0.5_pr - 0.25_pr)) + 1.0_pr)*SUM(du(neng,:,1:2)*obs_norm(:,1:2),DIM=2)!check if temperature in subroutine is formed same as TT in brinkman
       END IF
    END IF

    IF(conv_drho)      int_Drhs((ndrho-1)*ng+1:ndrho*ng) = 0.0_pr
  END SUBROUTINE internal_Drhs

  SUBROUTINE internal_Drhs_diag (int_diag,u_prev,meth,doBC,addingon)
    USE penalization
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: meth
    REAL (pr), DIMENSION (ng*ne), INTENT(INOUT) :: int_diag
    REAL (pr), DIMENSION (ng,ne), INTENT(IN) :: u_prev
    INTEGER, INTENT(IN) :: doBC !if zero do full rhs, if one no x-derivatives 
    INTEGER, INTENT(IN) :: addingon 

    INTEGER :: i, j, l, ll, ie, shift, zeroFlux, jshift
    REAL (pr), DIMENSION (ng,dim) :: du_diag, d2u_diag, du_diag_F, d2u_diag_F, du_diag_B, d2u_diag_B
    REAL (pr), DIMENSION (MAX(Nspec,dim)+1,ng,dim) :: du, d2u, duB, d2uB, duF, d2uF, du_upwind, d2u_upwind
    REAL (pr), DIMENSION (n_integrated*dim,ng,dim) :: dv, d2v
    REAL (pr), DIMENSION (ng,n_integrated,dim) :: F
    REAL (pr), DIMENSION (ng,(n_integrated)*dim) :: F2  !only used for viscous terms, does not require density
    REAL (pr), DIMENSION (ng, Nspec) :: v_prev
    REAL (pr), DIMENSION (ng, dim+1) :: T_prev  !temperature and velocity
    REAL (pr), DIMENSION (ng, dim+1) :: T_p_diag    !momentum and energy
    REAL (pr), DIMENSION (ng, dim+2) :: props_diag  !momentum and energy(x2)
    REAL (pr), DIMENSION (ng) :: p_prev
    REAL (pr), DIMENSION (ng) :: ReU
    REAL (pr), DIMENSION (ng,7) :: props_prev   !extra for viscosity on energy and momentum

    REAL (pr) , DIMENSION(ng) :: dist_loc
    REAL (pr) , DIMENSION(ng,dim) :: obs_norm
    REAL (pr) :: min_h
    INTEGER :: meth_central,meth_backward,meth_forward

    meth_central  = meth + BIASING_NONE
    meth_backward = meth + BIASING_BACKWARD
    meth_forward  = meth + BIASING_FORWARD

    min_h = MINVAL(h(1,1:dim)/2.0_pr**(REAL(j_mx,pr) - 1.0_pr))

    zeroFlux = 0
    IF (dozeroFlux .AND. (doBC+addingon)>0) zeroFlux = dervFlux + 1

    CALL user_bufferRe (ReU, ng)

    int_diag = 0.0_pr
    DO l=1,Nspecm
       v_prev(:,l) = u_prev(:,nspc(l))/u_prev(:,nden)  ! Y0
    END DO
    v_prev(:,Nspec)=1.0_pr
    IF (Nspec>1) v_prev(:,Nspec) = v_prev(:,Nspec)-SUM(v_prev(:,1:Nspecm),DIM=2)  !Y0_Nspec
    props_prev(:,:)=0.0_pr
    DO l=1,Nspec
       props_prev(:,1) = props_prev(:,1) + v_prev(:,l)*cp_in(l)  !cp0
       props_prev(:,2) = props_prev(:,2) + v_prev(:,l)/MW_in(l)  !R0
       props_prev(:,4) = props_prev(:,1) + v_prev(:,l)*mu_in(l)  !mu0
       props_prev(:,5) = props_prev(:,2) + v_prev(:,l)*kk_in(l)  !kk0
       props_prev(:,6) = props_prev(:,3) + v_prev(:,l)*bD_in(l)  !bD0
    END DO
    props_prev(:,3) = props_prev(:,1)/(props_prev(:,1)-props_prev(:,2)) !gamma0 = cp0/(cp0-R0)

    IF(dyn_visc) THEN !overwrite props !ERIC: correct this because energy and momentum perturbation used to calculate temperature
       p_prev(:) = (u_prev(:,neng)-0.5_pr*SUM(u_prev(:,nvel(1):nvel(dim))**2.0_pr,DIM=2)/u_prev(:,nden))/(props_prev(:,1)/props_prev(:,2)-1.0_pr)  !pressure    
       T_prev(:,1) = p_prev(:)/props_prev(:,2)/u_prev(:,nden)  !Temperature
       DO i=1,dim !momentum diag terms
          T_p_diag(:,i) =    ( u_prev(:,nvel(i))/u_prev(:,nden)) /  &
               (props_prev(:,1)/props_prev(:,2)-1.0_pr)/props_prev(:,2)/u_prev(:,nden)  
       END DO
       T_p_diag(:,dim+1) =    1.0_pr / (props_prev(:,1)/props_prev(:,2)-1.0_pr)/props_prev(:,2)/u_prev(:,nden)  !Energy diag term
       !ERIC: develop props_p coefficient for energy and momentum
       props_prev(:,4:5) = 0.0_pr
       props_diag(:,:)   = 0.0_pr
       DO l=1,Nspec
          props_prev(:,4) = props_prev(:,4) + v_prev(:,l)*mu_in(l)*(1.0_pr+S1)/(T_prev(:,1) +S1)*(T_prev(:,1))**(1.5_pr)   !mu0
          props_prev(:,5) = props_prev(:,5) + v_prev(:,l)*kk_in(l)*(1.0_pr+S1)/(T_prev(:,1) +S1)*(T_prev(:,1))**(1.5_pr)   !kk0
          !diag terms from Temperature-varying props
          DO i = 1,dim
             props_diag(:,i) = props_diag(:,i) + v_prev(:,l)*mu_in(l)* ((1.5_pr*(1.0_pr+S1)*T_prev(:,1)**0.5_pr*T_p_diag(:,i))/(T_prev(:,1)+S1) &
                  + T_prev(:,1)**1.5_pr*( -(1.0_pr+S1)*T_p_diag(:,i) )/(T_prev(:,1)+S1)**2.0_pr)  !mu', velocity
          END DO
          props_diag(:,dim+1) = props_diag(:,dim+1) + v_prev(:,l)*mu_in(l)* ((1.5_pr*(1.0_pr+S1)*T_prev(:,1)**0.5_pr*T_p_diag(:,dim+1))/(T_prev(:,1)+S1) &
               + T_prev(:,1)**1.5_pr*( -(1.0_pr+S1)*T_p_diag(:,dim+1) )/(T_prev(:,1)+S1)**2.0_pr)  !mu', energy
          props_diag(:,dim+2) = props_diag(:,dim+2) + v_prev(:,l)*kk_in(l)* ((1.5_pr*(1.0_pr+S1)*T_prev(:,1)**0.5_pr*T_p_diag(:,dim+1))/(T_prev(:,1)+S1) &
               + T_prev(:,1)**1.5_pr*( -(1.0_pr+S1)*T_p_diag(:,dim+1) )/(T_prev(:,1)+S1)**2.0_pr)  !kk', energy
       END DO
    END IF

    IF ((doBC .eq. 0) .AND. ((methpd .ne. 0) .OR. splitFBpress)) THEN  !IF doBC>0, these terms are done inside LODI subroutines
       CALL c_diff_fast(u_prev(:,nvel(1))/u_prev(:,nden), du(1,:,:), d2u(1,:,:), j_lev, ng, meth, 10, 1, 1, 1) ! du0/dx
       IF (Nspec>1) THEN
          CALL dpdx_diag (du_diag(:,1),ng,v_prev(:,1:Nspecm),2*methpd+meth,j_lev,splitFBpress)
       ELSE
          CALL dpdx_diag (du_diag(:,1),ng,v_prev(:,1),2*methpd+meth,j_lev,.FALSE.)
       END IF
       shift = (nvel(1)-1)*ng
       int_diag(shift+1:shift+ng) = int_diag(shift+1:shift+ng) + du_diag(:,1)*u_prev(:,nvel(1))/u_prev(:,nden)*(props_prev(:,3)-1.0_pr)  !pd_rhou*diagFB_x
       shift = (neng-1)*ng
       int_diag(shift+1:shift+ng) = int_diag(shift+1:shift+ng) - (du(1,:,1) + du_diag(:,1)*u_prev(:,nvel(1))/u_prev(:,nden))*(props_prev(:,3)-1.0_pr) !pd_rhoe*(du0/dx+u0*diagFB_x)
    END IF
    CALL c_diff_diag(du_diag, d2u_diag, j_lev, ng, meth, meth, -11)
    !
    !--Form right hand side of Euler Equations
    !
    F(:,:,:) = 0.0_pr
    DO j = 1+doBC, dim
       DO i = 1, dim ! (rho*u_i*u_j)'
          F(:,nvel(i),j) = F(:,nvel(i),j) + u_prev(:,nvel(j))/u_prev(:,nden)
       END DO
       F(:,nvel(j),j) = F(:,nvel(j),j) + u_prev(:,nvel(j))/u_prev(:,nden)*(2.0_pr-props_prev(:,3))
       F(:,neng,j) = F(:,neng,j) + props_prev(:,3)*u_prev(:,nvel(j))/u_prev(:,nden)
       DO l=1,Nspecm
          F(:,nspc(l),j) = F(:,nspc(l),j) + u_prev(:,nvel(j))/u_prev(:,nden)
       END DO
    END DO
    IF ((doBC .eq. 0) .AND. ((methpd .ne. 0) .OR. splitFBpress)) THEN  !IF doBC>0, these terms are done inside LODI subroutines
       F(:,nvel(1),1) = F(:,nvel(1),1) + u_prev(:,nvel(j))/u_prev(:,nden)*(props_prev(:,3)-1.0_pr)
       F(:,neng,1) = F(:,neng,1) - (props_prev(:,3)-1.0_pr)*u_prev(:,nvel(1))/u_prev(:,nden)
    END IF
    IF (doBC .ne. 0) THEN  !Missing term from LODI assumps... gamma=constant
       shift = (neng-1)*ng
       CALL c_diff_fast(props_prev(:,1)/props_prev(:,2), du(1,:,:), d2u(1,:,:), j_lev, ng, meth, 10, 1, 1, 1)  !d(gamma/(gamma-1))/dx
       int_diag(shift+1:shift+ng) = int_diag(shift+1:shift+ng)-du(1,:,1)*u_prev(:,nvel(1))/u_prev(:,nden)*(props_prev(:,3)-1.0_pr) !pu*d(gamma/(gamma-1))/dx
    END IF
    !
    !-- Form right hand side of Navier-Stokes Equations
    !
    IF(NS .AND. NSdiag) THEN
       IF(Nspec>1) THEN
          CALL c_diff_fast(v_prev, du(1:Nspec,:,:), d2u(1:Nspec,:,:), j_lev, ng, meth, 10, Nspec, 1, Nspec) ! dY_l/dx_j
       ELSE
          du(1:Nspec,:,:) = 0.0_pr
          d2u(1:Nspec,:,:) = 0.0_pr
       END IF
       IF (zeroFlux==1) THEN
          CALL user_flxfunc (d2u(1:dim,:,1:dim), ng, j_lev)
       ELSE
          d2u(:,:,:) = 1.0_pr
       END IF
       DO j = 1, dim
          DO l=1,Nspec
             IF (Nspec>1) F(:,neng,j) =  F(:,neng,j) - ReU(:)*props_prev(:,6)*du(l,:,j)*cp_in(l)/(props_prev(:,1)-props_prev(:,2))*d2u(2,:,j)
          END DO 
       END DO
       DO j = 1, dim    
          DO i = 1, dim
             shift=(nvel(i)-1)*ng
             int_diag(shift+1:shift+ng) = int_diag(shift+1:shift+ng) + props_prev(:,4)*ReU(:)/u_prev(:,nden)*d2u_diag(:,j) * d2u(i,:,j)
          END DO
          shift=(nvel(j)-1)*ng
          int_diag(shift+1:shift+ng) = int_diag(shift+1:shift+ng) + props_prev(:,4)*ReU(:)/u_prev(:,nden)*d2u_diag(:,j)/3.0_pr * d2u(j,:,j)
          shift=(neng-1)*ng
          int_diag(shift+1:shift+ng) = int_diag(shift+1:shift+ng) + props_prev(:,5)*ReU(:)/u_prev(:,nden)/(props_prev(:,1)-props_prev(:,2))*d2u_diag(:,j) * d2u(2,:,j)
          DO l = 1, Nspecm
             shift=(nspc(l)-1)*ng
             int_diag(shift+1:shift+ng) = int_diag(shift+1:shift+ng) + ReU(:)*(props_prev(:,6)*d2u_diag(:,j)+bD_in(l)*du(l,:,j)) * d2u(2,:,j)
          END DO
       END DO
    END IF
    DO j = 1, dim
       DO i = 1, dim+1+Nspec
          shift=(i-1)*ng
          int_diag(shift+1:shift+ng) = int_diag(shift+1:shift+ng) - F(:,i,j)*du_diag(:,j)
       END DO
    END DO
    
    IF(dyn_visc .AND. .FALSE.) THEN  !ERIC: add on extra terms to momentum and energy (will also need derivatives) for dynamic viscosity
       !double check no interference from successive loops
       DO j = 1,dim
          T_prev(:,1+j) = u_prev(:,nvel(j))/u_prev(:,nden)
       END DO
       CALL c_diff_fast(v_prev, dv(1:dim+1,:,:), d2v(1:dim+1,:,:), j_lev, ng, meth, 10, dim+1, 1, dim+1) ! dY_l/dx_j
       p_prev(:) = dv(1+1,:,1) !reuse of a variable
       DO i = 2, dim
          p_prev(:) = p_prev(:) + dv(i+1,:,i) !du0_i/dx_i
       END DO
       IF (zeroFlux==1) THEN
          CALL user_flxfunc (d2v(1:dim,:,1:dim), ng, j_lev)
       ELSE
          d2u(:,:,:) = 1.0_pr
       END IF
       F2 = 0.0_pr
       DO j = 1, dim
          jshift=(ne)*(j-1)
          DO i = 1, dim
             F2(:,nvel(i)+jshift) = F2(:,nvel(i)+jshift) - ReU(:)*props_diag(:,i)*( dv(i+1,:,j)+du(j+1,:,i) ) * d2u(i,:,j)  !rho*u_i*u_j - 2*mu*S_ij/Re
          END DO
          F2(:,nvel(j)+jshift) = F2(:,nvel(j)+jshift) + 2.0_pr/3.0_pr*ReU(:)*props_diag(:,j)*p_prev(:) * d2u(j,:,j) !rho*u_i*u_j - tau_ij/Re  [ tau_ij = 2*mu*( S_ij - 2/3*S_kk*delta_ij ) ]
          DO i = 1, dim
             F2(:,neng+jshift) =  F2(:,neng+jshift) - ReU(:)*( du(i+1,:,j)+du(j+1,:,i) )*(T_prev(:,i+1)*props_diag(:,1+dim)) * d2u(i,:,j)  !(e+p)*u_j - u_i*2*mu*S_ij/Re
          END DO
          F2(:,neng+jshift) =  F2(:,neng+jshift) + 2.0_pr/3.0_pr*ReU(:)*p_prev(:)*(T_prev(:,j+1)*props_diag(:,1+dim)) * d2u(j,:,j) - ReU(:)*props_diag(:,dim+2)*du(1,:,j) * d2u(2,:,j)  
                                                                                             !(e+p)*u_j - u_i*tau_ij/Re - q_j/Re/Pra [ q_j = mu dT/dx_j ] 
       END DO
       CALL c_diff_fast(F2(:,1:(ne)*dim), dv(1:(ne)*dim,:,:), d2v(1:(ne)*dim,:,:), j_lev, ng, meth_central, 10, (ne)*dim, 1, (ne)*dim)  !du(i,:,k)=dF_ij/dx_k
       DO j = 1, dim
          jshift=ne*(j-1)
          DO i = 1, ne
             shift=(i-1)*ng
             int_diag(shift+1:shift+ng) = int_diag(shift+1:shift+ng) - dv(i+jshift,:,j)  ! -dF_ij/dx_j  !continutiy set up
          END DO
       END DO
    END IF

    IF (NS .AND. NSdiag .AND. zeroFlux==2) THEN
       CALL user_flxfunc (d2u(1:dim,:,1:dim), ng, j_lev)
       d2u(1:dim,:,1:dim) = 1.0_pr - d2u(1:dim,:,1:dim)
       DO i = 2, dim
          shift=(nvel(i)-1)*ng
          int_diag(shift+1:shift+ng) = int_diag(shift+1:shift+ng) - props_prev(:,4)*ReU(:)/u_prev(:,nden)*d2u_diag(:,1) * d2u(2,:,1)
       END DO
       DO l = 1, Nspecm
          shift=(nspc(l)-1)*ng
          int_diag(shift+1:shift+ng) = int_diag(shift+1:shift+ng) - ReU(:)*(props_prev(:,6)*d2u_diag(:,1)+bD_in(l)*du(l,:,1)) * d2u(2,:,1)
       END DO
       shift=(neng-1)*ng
       int_diag(shift+1:shift+ng) = int_diag(shift+1:shift+ng) - props_prev(:,5)*ReU(:)/u_prev(:,nden)/(props_prev(:,1)-props_prev(:,2))*d2u_diag(:,1) * d2u(2,:,1)
       DO l = 1, Nspec
          IF (Nspec>1) v_prev(:,l) = ReU(:)*du(l,:,1)*props_prev(:,6)*u_prev(:,nden)
       END DO
       CALL c_diff_fast(v_prev, du(1:Nspec,:,:), d2u(1:Nspec,:,:), j_lev, ng, meth, 10, Nspec, 1, Nspec) ! d/dx_j(rho0*D0*dY_l/dx)
       DO l = 1, Nspec
          IF (Nspec>1) int_diag(shift+1:shift+ng) = int_diag(shift+1:shift+ng) - cp_in(l)/u_prev(:,nden)/(props_prev(:,1)-props_prev(:,2))*du(l,:,1) * d2u(2,:,1)
       END DO
    END IF
        
    IF(conv_drho)      int_diag((ndrho-1)*ng+1:ndrho*ng) = 0.0_pr
  END SUBROUTINE internal_Drhs_diag

  FUNCTION user_rhs (u_integrated,scalar)
    IMPLICIT NONE
    REAL (pr), DIMENSION (ng,ne), INTENT(IN) :: u_integrated !1D flat version of integrated variables without BC 
    REAL (pr), DIMENSION (ng), INTENT(IN) :: scalar
    REAL (pr), DIMENSION (ng*ne) :: user_rhs
    INTEGER, PARAMETER :: meth=1
    IF ((LODIit .eq. 4) .OR. ((LODIit .eq. 2) .AND. (globFlux .ne. 1))) THEN
       CALL internal_rhs(user_rhs,u_integrated,0,1)
    ELSE
       CALL internal_rhs(user_rhs,u_integrated,0,0)
    END IF

    !IF (imask_obstacle == .TRUE.) CALL Brinkman_rhs (user_rhs, u_integrated, .FALSE., HIGH_ORDER) !add penalization ter

    !IF (imask_obstacle .AND. vp) CALL volume_penalization_rhs (user_rhs, u_integrated, ne, ng, j_lev, LOW_ORDER)
    IF (imask_obstacle == .TRUE.) CALL Brinkman_rhs (user_rhs, u_integrated, .FALSE., LOW_ORDER) !add penalization ter
    IF (cnvtBC) CALL user_convectzone_BC_rhs (user_rhs, u_integrated, ne, ng, j_lev, meth) !add artificial convection to bufferzone/obstacle
    IF (buffBC) CALL user_bufferzone_BC_rhs (user_rhs, u_integrated, ne, ng, j_lev, meth)
    IF(hypermodel .NE. 0) THEN
       CALL hyperbolic(u_integrated,ng,user_rhs)
    END IF

  END FUNCTION user_rhs

  FUNCTION user_Drhs (u_p, u_prev, meth)
!!$    USE Brinkman
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: meth
    REAL (pr), DIMENSION (ng,ne) :: u_p, u_prev
    REAL (pr), DIMENSION (ng*ne) :: user_Drhs
    IF ((LODIit .eq. 4) .OR. ((LODIit .eq. 2) .AND. (globFlux .ne. 1))) THEN
       CALL internal_Drhs(user_Drhs,u_p,u_prev,meth,0,1)
    ELSE
       CALL internal_Drhs(user_Drhs,u_p,u_prev,meth,0,0)
    END IF

    !IF (imask_obstacle == .TRUE.) CALL Brinkman_Drhs (user_Drhs, u_p, u_prev, HIGH_ORDER)  !add penalization terms

    !IF (imask_obstacle .AND. vp) CALL volume_penalization_Drhs (user_Drhs, u_p, u_prev, ne, ng, j_lev, LOW_ORDER)
    IF (imask_obstacle == .TRUE.) CALL Brinkman_Drhs (user_Drhs, u_p, u_prev, LOW_ORDER)  !add penalization terms
    IF (cnvtBC) CALL user_convectzone_BC_Drhs (user_Drhs, u_p, u_prev, ne, ng, j_lev, meth) !add artificial convection to bufferzone/obstacle
    IF (buffBC) CALL user_bufferzone_BC_Drhs (user_Drhs, u_p, u_prev, ne, ng, j_lev, meth)


    IF (hypermodel .NE. 0 ) CALL hyperbolic(u_p,ng,user_Drhs)

  END FUNCTION user_Drhs

  FUNCTION user_Drhs_diag (meth)
!!$    USE Brinkman
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: meth
    REAL (pr), DIMENSION (ng*ne) :: user_Drhs_diag
    REAL (pr), DIMENSION (ng,ne) :: u_prev
    INTEGER :: ie, shift

    DO ie = 1, ne
       shift=(ie-1)*ng
       u_prev(1:ng,ie) = u_prev_timestep(shift+1:shift+ng)
    END DO

    IF ((LODIit .eq. 4) .OR. ((LODIit .eq. 2) .AND. (globFlux .ne. 1))) THEN
       CALL internal_Drhs_diag(user_Drhs_diag,u_prev,meth,0,1)
    ELSE
       CALL internal_Drhs_diag(user_Drhs_diag,u_prev,meth,0,0)
    END IF

    IF (imask_obstacle == .TRUE.) CALL Brinkman_Drhs_diag (user_Drhs_diag,u_prev, LOW_ORDER)  !add penalization terms
    IF (cnvtBC) CALL user_convectzone_BC_Drhs_diag (user_Drhs_diag, u_prev, ne, ng, j_lev, meth)
    IF (buffBC) CALL user_bufferzone_BC_Drhs_diag (user_Drhs_diag, u_prev, ne, ng, j_lev, meth)
    !IF (imask_obstacle .AND. vp) CALL volume_penalization_Drhs_diag (user_Drhs_diag, u_prev, ne, ng, j_lev, LOW_ORDER)
    !IF (imask_obstacle == .TRUE.) CALL Brinkman_Drhs_diag (user_Drhs_diag, meth)  !add penalization terms

    IF(hypermodel .NE. 0 ) CALL hyperbolic_diag(user_Drhs_diag, ng)


  END FUNCTION user_Drhs_diag



  SUBROUTINE user_project (u, p, nlocal, meth)
    IMPLICIT NONE

    INTEGER, INTENT (IN) :: meth, nlocal
    REAL (pr), DIMENSION (nlocal),     INTENT(INOUT) :: p
    REAL (pr), DIMENSION (nlocal,n_integrated), INTENT(INOUT) :: u

    INTEGER :: i
    INTEGER, PARAMETER :: ne = 1
    INTEGER, DIMENSION(ne) :: clip 
    !no projection for compressible flow
  END SUBROUTINE user_project

  FUNCTION Laplace (jlev, u, nlocal, ne_local, meth)
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: jlev, nlocal, ne_local, meth
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (INOUT) :: u
    REAL (pr), DIMENSION (nlocal*ne_local) :: Laplace

    INTEGER :: i, ii, ie, shift, meth1, meth2, idim
    REAL (pr), DIMENSION (ne_local ,nlocal,dim) :: du
    REAL (pr), DIMENSION (ne_local*dim ,nlocal,dim) :: d2u
    REAL (pr), DIMENSION (ne_local*dim ,nlocal,dim) :: du_dummy ! passed when only calculating 1st derivative.
    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc
    
    Laplace=0.0_pr
  END FUNCTION Laplace

  FUNCTION Laplace_diag (jlev,  nlocal, ne_local, meth)
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: jlev, nlocal, ne_local, meth
    REAL (pr), DIMENSION (nlocal*ne_local) :: Laplace_diag

    INTEGER :: i, ii, ie, shift, meth1, meth2
    REAL (pr), DIMENSION (nlocal,dim) :: du, d2u
    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc

    Laplace_diag=0.0_pr
  END FUNCTION Laplace_diag

  FUNCTION Laplace_rhs(u, nlocal, ne_local, meth)
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: nlocal, ne_local, meth
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (INOUT) :: u
    REAL (pr), DIMENSION (nlocal) :: Laplace_rhs

    INTEGER :: i, ii, ie, meth1, meth2, idim
    REAL (pr), DIMENSION (ne_local ,nlocal,dim) :: du
    REAL (pr), DIMENSION (ne_local*dim ,nlocal,dim) :: d2u
    REAL (pr), DIMENSION (ne_local*dim ,nlocal,dim) :: du_dummy ! passed when only calculating 1st derivative.
    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc

    Laplace_rhs = 0.0_pr 
  END FUNCTION Laplace_rhs

  !
  ! Calculate any statitics
  !
  ! startup_flag - 0 when adapting to IC,then 1 inmain integration loop
  !
  SUBROUTINE user_stats ( u ,j_mn, startup_flag)
    USE parallel
    USE penalization
    IMPLICIT NONE
    REAL (pr), DIMENSION (nwlt,1:n_var), INTENT (IN) :: u
    INTEGER , INTENT (IN) :: j_mn 
    INTEGER , INTENT (IN) :: startup_flag

    INTEGER :: i,j,k,idim,shift,n,n_check

    REAL(pr) :: num_nodes_added

    REAL(pr),DIMENSION(nwlt,2+dim) :: u_FWH

    !surface integration variables
    INTEGER, DIMENSION(dim*dim) :: int_var
    INTEGER, DIMENSION(dim,surf_pts) :: nearest_ixyz
    INTEGER, DIMENSION(surf_pts) :: cproc,i_x_interpolate
    

    REAL(pr), DIMENSION(dim,surf_pts) :: x_interpolate, x_interp_local
    REAL(pr), DIMENSION(dim*dim,surf_pts) :: u_interpolate,u_interp_local  !interpolated values
    REAL(pr), DIMENSION(dim*dim,1) :: u_temp     !temporary placeholder for all processors to enter interpolation subroutine    
    REAL(pr), DIMENSION (nwlt,dim*dim) :: u_intfield  !solutions to interpolate from
    REAL(pr), DIMENSION (dim*dim,nwlt,dim) :: du_intfield,temp  !solutions to interpolate from
    REAL(pr), DIMENSION(nwlt) :: Temperature,visc
    REAL(pr), DIMENSION(nwlt,dim) :: v
    REAL(pr), DIMENSION(dim,nwlt,dim) :: dv,dvF,dvB
    REAL(pr) :: integral
    REAL(pr), DIMENSION(dim,nwlt) :: h_arr
    REAL(pr) :: hhx, hhy, area_reconstruction,pi,circ_recon
    REAL(pr), DIMENSION(2) :: F_surf, F_chi
    REAL(pr), DIMENSION(nwlt,dim) :: norm

    REAL (pr), DIMENSION(dim,1) :: x_temp         
    CHARACTER (LEN=4) :: string,string2
    CHARACTER (LEN=60) :: sformat


    IF(FWHstats) THEN  
       u_FWH(:,1) = u(:,nden)!density
       u_FWH(:,2) = (u(:,neng)-0.5_pr*SUM(u(:,nvel(1):nvel(dim))**2.0_pr,DIM=2)/u(:,nden))/(cp_in(Nspec)/ (1/MW_in(Nspec))-1.0_pr)  !pressure, is this even real???
       DO i = 1,dim
          u_FWH(:,2+i) = u(:,nvel(i))/u(:,nden) !velocity
       END DO
       CALL FWH_stats ( u_FWH,nwlt,dim+2,FWHdt,startup_flag)
    END IF



    !-------interpolate for aerodynamics------------
    
     !only for 2D cylinder, testing phase.  Need to adjust right variables and processing for Cp, Cd, Cl, remove print statements
       !points to interpolate on
       IF (aero_stats .AND. startup_flag .EQ. 1 .AND. dim .EQ.2 .AND. t .GE. twrite  ) THEN
          
          !get normals
          CALL user_dist (ng, t, NORMAL=norm)
      
          !initialize
          u_interpolate(:,:) = 0.0_pr
          u_interp_local(:,:) = 0.0_pr
          pi = 2.0_pr*ASIN(1.0_pr)

          !number of points in integration curve defined as a parameter
          DO i = 1,surf_pts  !calculate interpolation points
             x_interpolate(1,i) = 0.5_pr*COS(pi*2.0_pr/REAL(surf_pts,pr)*i) -Ma*t
             x_interpolate(2,i) = 0.5_pr*SIN(pi*2.0_pr/REAL(surf_pts,pr)*i)
          END DO
          
          !filter out points not on local processor  ERIC:possibly use h vector
          CALL get_all_local_h (h_arr)
          hhx = MINVAL(h_arr(1,:))
          hhy = MINVAL(h_arr(2,:))
          CALL parallel_global_sum(REALMINVAL=hhx)
          CALL parallel_global_sum(REALMINVAL=hhy)
          
    
          nearest_ixyz(1,:) = CEILING((x_interpolate(1,:)-xyzlimits(1,1))/hhx)
          nearest_ixyz(2,:) = CEILING((x_interpolate(2,:)-xyzlimits(1,2))/hhy)
          
          !Determine locally held points
          n=0
          DO i = 1,surf_pts  !same process backwards to reconstruct globally sized vectors with local values
             CALL DB_get_proc_by_coordinates( nearest_ixyz(1:dim,i), j_lev, cproc(i) )  !check proper dimension order
             IF (cproc(i).EQ.par_rank) THEN !move it to local point array for interpolation
                n=n+1
                x_interp_local(1:dim,n)=x_interpolate(1:dim,i)
             END IF
          END DO
          !print *, par_rank,'loc interp pts:',n
          
          !Reconstruction check
!!$       CALL get_indices_by_coordinate(n,nearest_ixyz(1:dim,1:n),j_lev,i_x_interpolate,0)
!!$       print *, i_x_interpolate
!!$       DO i = 1,n
!!$          if(cproc(i).eq.par_rank)PRINT *, par_rank, x(i_x_interpolate(i),: )  !check proper dimension order
!!$       END DO
         
          !calculate variables to interpolate from
          DO idim = 1,dim
             v(:,idim) = u(:,nvel(idim))/u(:,nden)
          END DO

          !Biased differencing to minimize impact of nonphysicical interior
          !region
          CALL c_diff_fast(v,dv,temp(1:dim,:,:),j_lev,nwlt,HIGH_ORDER,10,dim,1,dim)
          !CALL c_diff_fast(v,dvF,temp(1:dim,:,:),j_lev,nwlt,HIGH_ORDER+BIASING_FORWARD,10,dim,1,dim)
          
          !DO i = 1,dim
          !   DO j = 1,dim
          !      dv(j,:,i) = dvB(j,:,i)*0.5_pr*(SIGN(1.0_pr,norm(:,i))+1.0_pr)+dvF(j,:,i)*0.5_pr*(SIGN(1.0_pr,norm(:,i))-1.0_pr) 
          !   END DO
          !END DO

          Temperature = MW_in(Nspec)*u(:,nprs)/u(:,nden) 
          IF(dyn_visc) THEN
             visc=mu_in(Nspec) * (1.0_pr+S1)/(Temperature +S1)*(Temperature)**(1.5_pr)  !mu_Nspec - variable viscosity
          ELSE
             visc=mu_in(Nspec) 
          END IF
          !FULL STRESS
          u_intfield = 0.0_pr
          DO i = 1,dim
             DO j = 1,dim
                shift = (j-1)*dim+i
                u_intfield(:,shift) =visc* (dv(i,:,j) +dv(j,:,i))
                IF (i==j) THEN   
                   u_intfield(:,shift) =u_intfield(:,shift) - u(:,nprs)
                   DO k = 1,dim
                      u_intfield(:,shift) =u_intfield(:,shift)- visc*(2.0_pr/3.0_pr* dv(k,:,k))
                   END DO
                END IF
             END DO
          END DO
           !interpolate function on points
          
          CALL c_diff_fast(u_intfield,du_intfield,temp,j_lev,nwlt,LOW_ORDER,10,dim*dim,1,dim*dim)
         !IF(par_rank == 0) PRINT *, 'check symmetry of sigma:',MAXVAL(ABS(u_intfield(:,1)-u_intfield(:,4))),MAXVAL(ABS(u_intfield(:,2)-u_intfield(:,3)))  
          !PRINT *,  par_rank, 'interpolateing'
         DO i=1,dim*dim
              int_var(i)=i
         END DO 
          IF (n .NE.0) THEN !interpolation points exist on local processor - adjust for proper variables
             CALL interpolate(u_intfield,du_intfield,nwlt,dim*dim,n,x_interp_local(1:dim,1:n),3,dim*dim,int_var,u_interp_local(1:dim*dim,1:n))!use better interpolating b/c of nonphysical spikes in obstacle
             ! PRINT *, par_rank,'PROC',u_interpolate  !DIAGNOSTIC
          ELSE IF (n .EQ. 0) THEN  !need to pass a local point to interpolate because of parallel operations
             x_temp(:,1)=x(1,:)             !Seed x_tmp with a point that's on local processor
             !CALL interpolate(int_fcn,d_int_fcn,nwlt,n_var_interpolate,1,x_temp(1:dim,1:n),3,1,interp_vars(1),v_surface_loc(1:n_var_interpolate,1:n))   
             !CALL interpolate(u(:,1:dim+2),du(1:dim+2,:,:),nwlt,dim+2,1,x_surface_loc(1:dim,1:1),3,dim+2,interp_vars(1:dim+2),v_surface_loc(1:dim+2,1:1))
             !CALL interpolate(v(:,1:dim),dv(1:dim,:,:),nwlt,dim,1,x_temp(1:dim,1:1),3,dim,interp_vars(1:dim),v_surface_loc(1:dim,1:1)) !ERIC: make interpolation order a compile-time parameter
             CALL interpolate(u_intfield,du_intfield,nwlt,dim*dim,1,x_temp(1:dim,1:1),3,dim*dim,int_var,u_temp(1:dim*dim,1))!use better interpolating b/c of nonphysical spikes in obstacle
          END IF
          PRINT *, par_rank, 'interp complete'


          !Reconstruct in u_interpolate for global vector sum to save using processor 0
          n=0
          DO i = 1,surf_pts  !same process backwards to reconstruct globally sized vectors with local values
             IF (cproc(i).EQ.par_rank) THEN !move it to local point array for interpolation
                n=n+1
                u_interpolate(:,i)=u_interp_local(:,n)
             ELSE
                u_interpolate(:,i)=0.0_pr
             END IF
          END DO
          !PRINT *, MINVAL(u_interpolate(3,:)),MAXVAL(u_interpolate(3,:))
          !PRINT *, par_rank, 'ready for global sum'
          DO i = 1,dim*dim
               CALL parallel_vector_sum(REAL=u_interpolate(i,:),LENGTH=surf_pts) 
          END DO
          IF(par_rank==0)PRINT *, 'after sum', MINVAL(u_interpolate(3,:)),MAXVAL(u_interpolate(3,:))
          ! PRINT *, par_rank,'PROC',u_interpolate  !DIAGNOSTIC
          
          !CALCULATE COMPONENT FORCES LOCALLY
          
          !F_surf(1) = 2.0_pr*pi/REAL(surf_pts,pr)*SUM(u_interpolate(1,:)*x_interpolate(1,:))
          !F_surf(2) = 2.0_pr*pi/REAL(surf_pts,pr)*SUM(u_interpolate(1,:)*x_interpolate(2,:))
          !circ_recon = 

          !F_chi(1)  = SUM(penal/eta_b*u(:,nvel(1))/u(:,nden)*da) !is da local or global?
          !F_chi(2)  = SUM(penal/eta_b*u(:,nvel(2))/u(:,nden)*da) !is da local or global?
          !area_reconstruction = SUM(penal*da)
          
          !CALL parallel_global_sum(REAL=area_reconstruction)
          !CALL parallel_vector_sum(REAL=F_chi, LENGTH=2)
          !CALL parallel_vector_sum(REAL=u_interpolate(1,:),LENGTH=surf_pts)  !will need to repeat over n_vars
          !    IF (par_rank .EQ. 0) PRINT *, 'global',u_interpolate  !DIAGNOSTIC
          !PRINT *, par_rank, 'sum complete'
          
             !save forces
             
!!$          IF (par_rank .EQ. 0) THEN  !analyitical integration - development
!!$             integral = 0.0_pr
!!$             DO i = 1,surf_pts  
!!$                integral = integral + 3.14159/REAL(surf_pts)*(u_interpolate(1,i)+u_interpolate(1,MOD(i,surf_pts)+1))
!!$             END DO
!!$             
!!$             PRINT *, 'surface integration: Pi = ',integral
!!$          END IF
          !integral = 0.0_pr
          
!write stat files
           
          IF(par_rank .EQ. 0) THEN
               WRITE(string2,'(I4)') surf_pts+1
               sformat = '('//TRIM(ADJUSTL(string2))//'(E20.12))'
               !PRINT *, sformat
               OPEN(UNIT=330, FILE = TRIM(res_path)//TRIM(file_gen)//'surface'//'.stat', STATUS = 'UNKNOWN', form="FORMATTED") 
               DO i = 1,dim*dim
                  WRITE(330,sformat) t, u_interpolate(i,:)
               END DO 
               PRINT *, 'Aero stats (FULL STRESS)  written'   
               !WRITE(331,'(5(E20.12))') t, F_surf, F_chi
               !WRITE(332,'(2(E20.12))') t, area_reconstruction
          END IF

       END IF
    
  END SUBROUTINE user_stats

  SUBROUTINE user_cal_force (u, n, t_local, force, drag, lift)
    !--Calculates drag and lift on obstacle using penalization formula
    IMPLICIT NONE

    INTEGER, INTENT (IN) :: n
    REAL (pr), INTENT (IN) :: t_local
    REAL (pr), DIMENSION (dim), INTENT (INOUT) :: force
    REAL (pr), INTENT (OUT) :: drag, lift
    REAL (pr), DIMENSION (n,dim) :: u
    drag = 0.0_PR
    lift = 0.0_PR
    !
    ! There is no obstacle in flow
    !

  END SUBROUTINE user_cal_force

  !
  ! Read input from "case_name"_pde.inp file
  ! case_name is in string file_name read from command line
  ! in read_command_line_input()
  ! 
  SUBROUTINE user_read_input()
    IMPLICIT NONE
    INTEGER :: i, j, ind
    CHARACTER(LEN=8):: numstrng, procnum
    INTEGER :: myiter
    LOGICAL :: checkit 
    REAL (pr) :: halfdomsize

    call input_real ('Re',Re,'stop',' Re: Reynolds Number')
    call input_real ('Rebnd',Rebnd,'stop',' Rebnd: Reynolds Number on boundaries')
    call input_real ('Pra',Pra,'stop',' Pra: Prandtl Number')
    call input_real ('Sc',Sc,'stop',' Sc: Schmidt Number')
    call input_real ('Fr',Fr,'stop',' Fr: Froude Number')
    call input_real ('Ma',Ma,'stop',' Ma: Mach Number')
    call input_real ('S1',S1,'stop',' S1: Sutherland Constant S_1/T_0 (variable viscosity)')
    call input_logical ('dyn_visc',dyn_visc,'stop',' dyn_visc: use dynamic viscosity model')
    call input_integer ('Nspec',Nspec,'stop','  Nspec: Number of species')
    call input_integer ('methpd',methpd,'stop','  methpd: meth for taking pressure derivative in time')
    call input_integer ('BCver',BCver,'stop','  BCver: version for BC')
    call input_integer ('ICver',ICver,'stop','  ICver: version for IC')
    call input_logical ('IC_gauss',IC_gauss,'stop', 'IC_gauss: T/F - Include gaussian in density')
    call input_logical ('init_perturb',init_perturb,'stop', 'init_perturb:  initial perturbation on the inflow')


    call input_integer ('LODIit',LODIit,'stop', 'LODIit: 1 for LODI, 2 for LODIslip, 3 for LODIavg, 4 for LODIsimp')
    call input_integer ('PLpinf',PLpinf,'stop', 'PLpinf: Poinsot Lele P_inf Brinkman term with LODI, 1:2 for P, 3:4 for T, 5:6 for u, (odd const coef, even var coef) 0 for nothing')
    call input_integer ('globFlux',globFlux,'stop', 'globFlux: 1 to do global fluxes, 0 to do local fluxes on boundaries only')
    call input_integer ('dervFlux',dervFlux,'stop', 'dervFlux: 1 to do derivative fluxes, 0 to set fluxes to 0 on boundary only')
 
    Nspec=MAX(Nspec,1)
  
    ALLOCATE(gr_in(1:Nspec*dim))  
    ALLOCATE(bD_in(1:Nspec))
    ALLOCATE(mu_in(1:Nspec))
    ALLOCATE(kk_in(1:Nspec))
    ALLOCATE(cp_in(1:Nspec))
    ALLOCATE(MW_in(1:Nspec))
    ALLOCATE(gamm(1:Nspec))
    ALLOCATE(pureX(1:Nspec), pureY(1:Nspec), pureR(1:Nspec), pureg(1:Nspec), puregamma(1:Nspec), YR(1:Nspec))

    call input_real ('At',At,'stop',' At: Atwood Number')
    call input_real_vector ('gamm',gamm(1:Nspec),Nspec,'stop',' gamm: cp/(cp-R)')
    call input_real ('peakchange',peakchange,'stop',' peakchange: how far offset to put peaks')
    call input_real ('buffBeta',buffBeta,'stop', 'bufBeta: polynomial exponent for buff/convect BC')
    call input_real ('buffU0',buffU0,'stop', 'buffU0: scale for convect BC')
    call input_real ('buffSig',buffSig,'stop', 'buffSig: scale for buff BC')
    call input_real ('buffOff',buffOff,'stop', 'buffOff: x-offset used in tanh for buff BC')
    call input_real ('buffFac',buffFac,'stop', 'buffFac: multiplication factor in tanh for buff BC')
    call input_real ('pBsig',pBsig,'stop', 'pBsig: sigma used in coefficient for pressure Brinkman term')
    call input_real ('Lczn',Lczn,'stop', 'Lcoordzn: length of coord_zone on one side')
    call input_real ('tfacczn',tfacczn,'stop', 'tfacczn: factor multiply by time for coord_zone time=L/min(c)*tfac, set <0 for no czn modification')
    call input_real ('itfacczn',itfacczn,'stop', 'itfacczn: factor multiply by initial time for coord_zone itime=Lczn/min(c)*tfac, set <0 to set itime=0')
    call input_real ('waitrat',waitrat,'stop', 'waitrat: fraction to wait beyond acoustic speed travel time')
    call input_real_vector ('buffRed',buffRed(1:5),5,'stop', 'buffRed: buffRe distances')
    call input_real_vector ('buffRef',buffRef(1:5),5,'stop', 'buffRef: buffRe distance fractions')
    call input_real_vector ('buffcvd',buffcvd(1:5),5,'stop', 'buffcvd: convect zone distances')
    call input_real_vector ('buffcvf',buffcvf(1:5),5,'stop', 'buffcvf: convect zone distance fractions')
    call input_real_vector ('buffbfd',buffbfd(1:5),5,'stop', 'buffbfd: buffer zone distances')
    call input_real_vector ('buffbff',buffbff(1:5),5,'stop', 'buffbff: buffer zone distance fractions')

    call input_real ('buff_thick',buff_thick,'stop',' buff_thick: thickness of freund buffer region')


    call input_real ('maskscl',maskscl,'stop', 'maskscl: scale for mask, set < 0 for default')
    call input_real ('tempscl',tempscl,'stop', 'tempscl: scale for temperature, set < 0 for default')
    call input_real ('specscl',specscl,'stop', 'specscl: scale for species, set < 0 for default')
    call input_real ('mydt_orig',mydt_orig,'stop', 'mydt_orig: max dt for time integration... dt is only initial dt, set <0 to use dt')

    call input_logical ('GRAV',GRAV,'stop', 'GRAV: T to include body force terms, F to ingore them')
    call input_logical ('NS',NS,'stop', 'NS: T to include viscous terms (full Navier-Stokes), F for Euler Eqs')

    call input_logical ('adaptAt',adaptAt,'stop', 'adaptAt: T to modify At to get effectivie At = input At due to peakchange')
    call input_logical ('evolBC',evolBC,'stop', 'evolBC: T to use evol BCs, F to use algebraic BCs')
    call input_logical ('buffBC',buffBC,'stop', 'buffBC: T to use buff zone damping BCs (Brinkman), F to use algebraic BCs')
    call input_logical ('polybuff',polybuff,'stop', 'polybuff: T to use polynomials for buff zone BCs,  F to use tanh')
    call input_logical ('polybuffRe',polybuffRe,'stop', 'polybuffRe: T to use polynomials for buffRe,  F to use tanh')
    call input_logical ('cnvtBC',cnvtBC,'stop', 'cnvtBC: T to use convection zone BCs (Freund), F to use algebraic BCs')
    call input_logical ('polyconv',polyconv,'stop', 'polyconv: T to use polynomials for conv zone BCs,  F to use tanh')
    call input_logical ('pertchar',pertchar,'stop', 'pertchar: T to do characteristics on perturbed p and rho')
    call input_logical ('usedP0',usedP0,'stop', 'usedP0: T to use dP0 for X2 perthcar, F to use -rhoYg')
    call input_logical ('dozeroFlux',dozeroFlux,'stop', 'dozeroFlux: T to set dtau_i1/dx_1 (i.ne.1)=dq_1/dx_1=dY/dx_1=0 for evolBC')
    call input_logical ('pBrink',pBrink,'stop', 'pBrink: T to do Brinkman buffer only on pressure')
    call input_logical ('savebuoys',savebuoys,'stop', 'savebuoys: if T adds and saves dPdx and -(dPdx + rho*g)')
    call input_logical ('adaptbuoy',adaptbuoy,'stop', 'adaptbuoy: T to adapt on buoy term')
    call input_logical ('splitFBpress',splitFBpress,'stop', 'splitFBpress: to split derivs for pressure dp/dx FB/BB light/heavy')
    call input_logical ('stepsplit',stepsplit,'stop', 'stepsplit: T to do Heaviside split, F to use X')
    call input_logical ('buffRe',buffRe,'stop', 'buffRe: T to change Reynolds number near boundaries')
    call input_logical ('adaptbuffRe',adaptbuffRe,'stop', 'adaptbuffRe: T to adapt on buffer layer')
    call input_logical ('adapttmp',adapttmp,'stop', 'adapttmp: T to adapt on temperature instead of density and pressure')
    call input_logical ('adaptvelonly',adaptvelonly,'stop', 'adaptvelonly: T to adapt on velocity instead of momentum')
    call input_logical ('adaptspconly',adaptspconly,'stop', 'adaptspconly: T to adapt on mass fraction (Y) instead of volume fraction (rhoY)')
    call input_logical ('adaptden',adaptden,'stop', 'adapt on density')
    call input_logical ('adaptvel',adaptvel,'stop', 'adapt on momentum')
    call input_logical ('adapteng',adapteng,'stop', 'adapt on energy')
    call input_logical ('adaptspc',adaptspc,'stop', 'adapt on species')
    call input_logical ('adaptprs',adaptprs,'stop', 'adapt on pressure')
    call input_logical ('buffRewait',buffRewait,'stop', 'buffRewait: T to wait until acoustic waves reach boundaries to do buffRe')
    call input_logical ('LODIset',LODIset,'stop', 'LODIset: T to set characteristics u+/-c only, F to check all characteristics')
    call input_logical ('LODIslipeasy',LODIslipeasy,'stop', 'LODIslipeasy: T to simple LODIslip, where dp/dx=0 set in eqns')
    call input_logical ('savepardom',savepardom,'stop', 'savepardom: T to save parallel domain as additional var')
    call input_logical ('doassym',doassym,'stop', 'doassym: T to modify assym parameters in user_code')
    call input_logical ('convcfl',convcfl,'stop', 'convcfl: T to use convective cfl, F to use acoustic cfl')
    call input_logical ('boundY',boundY,'stop', 'boundY: T to bound Y to [0,1] in user_pre/post_process')

    !4extra - these 8 input definitions - MUST ALSO ADD TO .inp 
    call input_logical ('adaptMagVort',adaptMagVort,'stop','adaptMagVort: Adapt on the magnitude of vorticity')
    call input_logical ('adaptComVort',adaptComVort,'stop','adaptComVort: Adapt on the components of vorticity')
    call input_logical ('adaptMagVel',adaptMagVel,'stop','adaptMagVel: Adapt on the magnitude of velocity')
    call input_logical ('adaptNormS',adaptNormS,'stop', 'adaptNormS: Adapt on the L2 of Sij')
    call input_logical ('adaptGradY',adaptGradY,'stop', 'adaptGradY: Adapt on |dY/dx_i*dY/dx_i|')
    call input_logical ('saveMagVort',saveMagVort,'stop', 'saveMagVort: if T adds and saves magnitude of vorticity')
    call input_logical ('saveComVort',saveComVort,'stop', 'saveComVort: if T adds and saves components of vorticity')
    call input_logical ('saveMagVel',saveMagVel,'stop','saveMagVel: if T adds and saves magnitude of velocity')
    call input_logical ('saveNormS',saveNormS,'stop', 'saveNormS: if T adds and saves magnitude of strain rate')
    call input_logical ('saveGradY',saveGradY,'stop', 'saveGradY: if T adds and saves |dY/dx_i*dY/dx_i|')
    call input_logical ('saveUcvt',saveUcvt,'stop', 'saveUcvt: save U and damping support for Freund NRBCs to debug')
   
    FWHstats = .FALSE.
    aero_stats = .FALSE.
    call input_logical ('FWHstats',FWHstats,'stop', 'FWHstats: save FWH stats for use in aeroacoustic analogy')
    call input_logical ('aero_stats',aero_stats,'stop', 'calculate and save aerodynamic stats for Cp,Cd,Cl')
    surf_pts = 20
    call input_integer ('surf_pts',surf_pts,'stop', 'Number of surface points to use for calculating aerodynamic forces')

    call input_real ('FWHdt',FWHdt,'stop',' FWHdt: timestep for FWH variables')

    ! Nurlybek: I'm from here
    fSUPERS = .FALSE.
    call input_logical ('fSUPERS',fSUPERS,'default', 'sueprsonic flow flag')       
    IF(.NOT.ALLOCATED(uL)) ALLOCATE(uL(dim)); uL = 0.0_pr
    IF(.NOT.ALLOCATED(uR)) ALLOCATE(uR(dim)); uR = 0.0_pr
    rhoL  = 1.0_pr
    uL(1) = 2.0_pr
    pL    = 1.0_pr
    ! left-right states
    call input_real ('rhoL',rhoL,'default', 'Inflow density')
    call input_real ('uL',uL(1),'default', 'Inflow x-component of velocity')
    call input_real ('pL',pL,'default', 'Inflow pressure')
    call input_real ('uR',uR(1),'default', 'Inflow x-component of velocity') 
    ! Shock generation
    doSHOCK = .FALSE.
    call input_logical ('doSHOCK',doSHOCK,'default', 'shock work out')
!!$    call input_logical ('saveUn',saveUn,'default', 'save normal and tangental')
    ! Shock params
    call input_real ('shock',shock,'default', 'shock position')
    call input_real ('shock_delta',shock_delta,'default', 'shock thickness')

    ! Up to here

    IF (imask_obstacle) THEN
       CALL Brinkman_input !input penalization parameters
       saveDIST = .FALSE.
       saveUn = .FALSE.
       call input_logical ('saveDIST',saveDIST,'default', 'save distance function')
       call input_logical ('saveUn',saveUn,'default', 'save normal and tangental comps')
    END IF
 
    IF (mydt_orig < 0.0_pr) mydt_orig = dt
    IF (IC_restart_mode==1) dt_original = mydt_orig

!!$    IF (buffNe) imask_obstacle=.TRUE.
    IF (NOT(buffRe)) adaptbuffRe=.FALSE.
    dobuffRe = buffRe
    IF (buffRewait) dobuffRe = .FALSE.

    IF (Nspec<2) adaptspc=.FALSE. 
    IF (Nspec<2) adaptspconly=.FALSE. 
    IF (Nspec<2) adaptGradY=.FALSE. 
    IF (Nspec<2) saveGradY=.FALSE. 
    IF (Nspec<2) splitFBpress=.FALSE. 
    Nspecm = Nspec - 1

    IF (NOT(evolBC)) LODIit=0 
!!$    IF (evolBC) BCver=-1    
!!$    IF (evolBC .AND. LODIit==2) BCver=100
!!$    IF (evolBC) cnvtBC=.FALSE.
!!$    IF (cnvtBC) BCver=-1
!!$    IF (buffBC) BCver=-1
!!$    IF (LODIit == 4) globFlux=0

    IF (adaptAt) At=At*(1.0_pr/(1.0_pr-2.0_pr*peakchange))

    gr_in(:) = 0.0_pr  !g's by species, then dimension
    IF (GRAV) gr_in(1:Nspec) = 1.0_pr
    gr_in(:) = gr_in(:)/Fr**2.0_pr

!    MW_in(:) = 1.0_pr
    MW_in(:) = gamm(:)
!    MW_in(1) = 1.0_pr+At
    MW_in(1) = MW_in(1)+At
!    IF (Nspec>1) MW_in(2) = 1.0_pr-At
    IF (Nspec>1) MW_in(2) = MW_in(2)-At
    YR(:) = MW_in(:) / SUM(MW_in(:))
    cp_in(:) = 1.0_pr/MW_in(:)*gamm(:)/(gamm(:)-1.0_pr)
    gammR = SUM(cp_in(:)*YR(:))/( SUM(cp_in(:)*YR(:))-SUM(YR(:)/MW_in(:)) )
    mu_in(:) = 1.0_pr/Re
!    kk_in(:) = 1.0_pr/Re/Pra*gammR/(gammR-1.0_pr)
    kk_in(:) = 1.0_pr/Re/Pra/(gammR-1.0_pr)

    bD_in(:) = 1.0_pr/Re/Sc

    gammBC(:)=gamm(1)
    IF (Nspec>1) gammBC(2)=gamm(2)

!!$    Tconst = 1.0_pr/Ma**2.0_pr
!!$    Pint = 1.0_pr/Ma**2.0_pr

    Tconst = 1.0_pr
    Pint = 1.0_pr
    pBcoef=pBsig

     IF (NOT(ALLOCATED(Lxyz))) ALLOCATE(Lxyz(1:dim))

     IF (NOT(ALLOCATED(Y0top))) ALLOCATE(Y0top(1:Nspec))
     IF (NOT(ALLOCATED(Y0bot))) ALLOCATE(Y0bot(1:Nspec))
     IF (NOT(ALLOCATED(rhoYtopBC))) ALLOCATE(rhoYtopBC(1:Nspecm))
     IF (NOT(ALLOCATED(rhoYbotBC))) ALLOCATE(rhoYbotBC(1:Nspecm))
     IF (NOT(ALLOCATED(rhoYtop))) ALLOCATE(rhoYtop(1:Nspecm))
     IF (NOT(ALLOCATED(rhoYbot))) ALLOCATE(rhoYbot(1:Nspecm))

     pi=4.0_pr*ATAN(1.0_pr)
     Lxyz(1:dim)=xyzlimits(2,1:dim)-xyzlimits(1,1:dim)

    halfdomsize = (xyzlimits(2,1)-xyzlimits(1,1))/2.0_pr
    IF (buffRed(1)<0.0_pr) THEN
       buffRed(1)=halfdomsize*buffRef(1)
    ELSE
       buffRef(1)=buffRed(1)/halfdomsize
    END IF
    IF (ANY(buffRed(2:4)<0.0_pr)) THEN
       buffRef(2)=MAX(0.0_pr,MIN(1.0_pr,buffRef(2)))
       buffRef(3)=MAX(0.0_pr,MIN(1.0_pr-buffRef(2),buffRef(3)))
       buffRef(4)=MAX(0.0_pr,MIN(1.0_pr-SUM(buffRef(2:3)),buffRef(4)))
       buffRef(5)=MAX(0.0_pr,    1.0_pr-SUM(buffRef(2:4)))
       buffRed(2:5)=buffRef(2:5)*buffRed(1)
    ELSE
       buffRed(2)=MIN(buffRed(2),buffRed(1))
       buffRed(3)=MIN(buffRed(3),buffRed(1)-buffRed(2))
       buffRed(4)=MIN(buffRed(4),buffRed(1)-SUM(buffRed(2:3)))
       buffRed(5)=MAX(0.0_pr  ,buffRed(1)-SUM(buffRed(2:4)))
       buffRef(2:5)=buffRed(2:5)/buffRed(1)
    END IF
    IF (verb_level>0 .AND. par_rank==0) PRINT *,'FOR bufferfunc, buffRed=', buffRed
    IF (verb_level>0 .AND. par_rank==0) PRINT *,'FOR bufferfunc, buffRef=', buffRef

    IF (buffcvd(1)<0.0_pr) THEN
       buffcvd(1)=halfdomsize*buffcvf(1)
    ELSE
       buffcvf(1)=buffcvd(1)/halfdomsize
    END IF
    IF (ANY(buffcvd(2:4)<0.0_pr)) THEN
       buffcvf(2)=MAX(0.0_pr,MIN(1.0_pr,buffcvf(2)))
       buffcvf(3)=MAX(0.0_pr,MIN(1.0_pr-buffcvf(2),buffcvf(3)))
       buffcvf(4)=MAX(0.0_pr,MIN(1.0_pr-SUM(buffcvf(2:3)),buffcvf(4)))
       buffcvf(5)=MAX(0.0_pr,    1.0_pr-SUM(buffcvf(2:4)))
       buffcvd(2:5)=buffcvf(2:5)*buffcvd(1)
    ELSE
       buffcvd(2)=MIN(buffcvd(2),buffcvd(1))
       buffcvd(3)=MIN(buffcvd(3),buffcvd(1)-buffcvd(2))
       buffcvd(4)=MIN(buffcvd(4),buffcvd(1)-SUM(buffcvd(2:3)))
       buffcvd(5)=MAX(0.0_pr  ,buffcvd(1)-SUM(buffcvd(2:4)))
       buffcvf(2:5)=buffcvd(2:5)/buffcvd(1)
    END IF
    IF (verb_level>0 .AND. par_rank==0) PRINT *,'FOR bufferfunc, buffcvd=', buffcvd
    IF (verb_level>0 .AND. par_rank==0) PRINT *,'FOR bufferfunc, buffcvf=', buffcvf

    IF (buffbfd(1)<0.0_pr) THEN
       buffbfd(1)=halfdomsize*buffbff(1)
    ELSE
       buffbff(1)=buffbfd(1)/halfdomsize
    END IF
    IF (ANY(buffbfd(2:4)<0.0_pr)) THEN
       buffbff(2)=MAX(0.0_pr,MIN(1.0_pr,buffbff(2)))
       buffbff(3)=MAX(0.0_pr,MIN(1.0_pr-buffbff(2),buffbff(3)))
       buffbff(4)=MAX(0.0_pr,MIN(1.0_pr-SUM(buffbff(2:3)),buffbff(4)))
       buffbff(5)=MAX(0.0_pr,    1.0_pr-SUM(buffbff(2:4)))
       buffbfd(2:5)=buffbff(2:5)*buffbfd(1)
    ELSE
       buffbfd(2)=MIN(buffbfd(2),buffbfd(1))
       buffbfd(3)=MIN(buffbfd(3),buffbfd(1)-buffbfd(2))
       buffbfd(4)=MIN(buffbfd(4),buffbfd(1)-SUM(buffbfd(2:3)))
       buffbfd(5)=MAX(0.0_pr  ,buffbfd(1)-SUM(buffbfd(2:4)))
       buffbff(2:5)=buffbfd(2:5)/buffbfd(1)
    END IF
    IF (verb_level>0 .AND. par_rank==0) PRINT *,'FOR bufferfunc, buffbfd=', buffbfd
    IF (verb_level>0 .AND. par_rank==0) PRINT *,'FOR bufferfunc, buffbff=', buffbff

    buffDomFrac = MIN(buffcvf(1),buffbff(1))

    !IF (ALLOCATED(bndvals)) DEALLOCATE(bndvals)
    !ALLOCATE(bndvals(dim+Nspec+1,2*dim))  ! 1-flow variable, 2-boundary

    !variable thresholding
    CALL user_read_input__VT

  END SUBROUTINE user_read_input

  !
  ! calculate any additional variables
  ! 
  ! arg 
  ! flag  - 0 calledwhile adapting to IC, 1 - called in main time integration loop
  !
  ! These additiona variables are calculated and left in real space.
  !
  SUBROUTINE user_additional_vars( t_local, flag )
    USE penalization
    IMPLICIT NONE
    REAL (pr), INTENT (IN) ::  t_local
    INTEGER , INTENT(IN) :: flag ! 0- called during adaption to IC, 1 called during main integration loop
    REAL (pr) :: cp, R, mygr, cmax, uval
    INTEGER :: l, i, j, myiter, idim
    REAL (pr), DIMENSION(1,nwlt,MAX(dim,Nspecm,2)) :: du, d2u    
    REAL (pr) :: iterdiff, initdiff, curtau, fofn, deln, delnold, delfofn
    INTEGER, PARAMETER :: meth=1
    REAL (pr), DIMENSION(nwlt,dim*2) :: bigU
    REAL (pr), DIMENSION (nwlt,dim)     :: norm, Utan, Unorm

!!$    IF (adaptbuffRe) THEN
!!$
!!$       IF (dobuffRe) THEN
!!$          u(:,nbRe) = user_chi (nwlt, t_local) 
!!$       ELSE
!!$          u(:,nbRe) = 1.0_pr
!!$       END IF 
!!$    END IF

    CALL user_bufferfunc_eric(bigU, nwlt, buffcvd, polyconv)
    IF(saveUcvt) u(:,ncvtU) = SUM(bigU(:,:),DIM=2)


    d2u(1,:,2) = cp_in(Nspec) 
    d2u(1,:,1) = 1.0_pr/MW_in(Nspec) 
    IF (Nspec>1) d2u(1,:,2) = (1.0_pr - SUM(u(:,nspc(1):nspc(Nspecm)),DIM=2)/u(:,nden))*cp_in(Nspec)
    IF (Nspec>1) d2u(1,:,1) = (1.0_pr - SUM(u(:,nspc(1):nspc(Nspecm)),DIM=2)/u(:,nden))/MW_in(Nspec)
    DO l=1,Nspecm
        d2u(1,:,2) = d2u(1,:,2) + u(:,nspc(l))/u(:,nden)*cp_in(l) !cp
        d2u(1,:,1) = d2u(1,:,1) + u(:,nspc(l))/u(:,nden)/MW_in(l) !R
    END DO

    u(:,nprs) = ( u(:,neng)-0.5_pr*SUM(u(:,nvel(1):nvel(dim))**2.0_pr,DIM=2)/u(:,nden))/(d2u(1,:,2)/d2u(1,:,1)-1.0_pr)  ! p
    IF (adapttmp) u(:,nton) = u(:,nprs)/u(:,nden)/d2u(1,:,1)
    IF (adaptvelonly) THEN
       DO i=1,dim
          u(:,nvon(i)) = u(:,nvel(i))/u(:,nden)
       END DO
    END IF
    IF (adaptspconly) THEN
       DO i=1,Nspecm
          u(:,nson(i)) = u(:,nspc(i))/u(:,nden)
       END DO
    END IF
    IF (savebuoys) THEN
       u(:,ndpx) = u(:,nprs)
       DO i=1,Nspecm
          u(:,nspc(i)) = u(:,nspc(i))/u(:,nden)
       END DO
       IF (Nspec>1) THEN
          CALL dpdx (u(:,ndpx),nwlt,u(:,nspc(1):nspc(Nspecm)),2*methpd+meth,j_lev,splitFBpress)
       ELSE
          CALL dpdx (u(:,ndpx),nwlt,u(:,1),2*methpd+meth,j_lev,.FALSE.)
       END IF
       DO i=1,Nspecm
          u(:,nspc(i)) = u(:,nspc(i))*u(:,nden)
       END DO
       DO i=1,nwlt
          mygr=gr_in(Nspec)
          IF (Nspec>1) mygr=mygr*(1.0_pr-SUM(u(i,nspc(1):nspc(Nspecm))) /u(i,nden)   ) 
          DO j=1,Nspecm
             mygr=mygr+gr_in(j)*u(i,nspc(j))   /u(i,nden)   
          END DO
          u(i,nboy) =  u(i,ndpx) + mygr*u(i,nden)
       END DO
       IF (par_rank.EQ.0 .AND. verb_level>0) PRINT *, 'MAXABS of dp/dx+rho*g:', MAXVAL(ABS(u(:,nboy)))
    END IF
    IF( adaptMagVort .OR. saveMagVort .OR. adaptComVort .OR. saveComVort .OR. adaptMagVel .OR. saveMagVel .OR. adaptNormS .OR. saveNormS ) THEN
       DO i=1,dim
          du(1,:,i) = u(:,nvel(i))/u(:,nden)
       END DO
       CALL bonusvars_vel(du(1,:,1:dim))  !4extra - call it
    END IF
    IF( adaptGradY .OR. saveGradY) THEN
       DO i=1,Nspecm
          du(1,:,i) = u(:,nspc(i))/u(:,nden)
       END DO
       CALL bonusvars_spc(du(1,:,1:Nspecm))  !4extra - call it
    END IF

       IF (modczn) THEN
          IF (t>tczn) THEN
             modczn=.FALSE.
             xyzzone(1,1) = czn_min
             xyzzone(2,1) = czn_max            
            IF (par_rank.EQ.0 .AND. verb_level>0) THEN
             PRINT *, '*****************************************************'
             PRINT *, 'FINALIZING TEMPORARY COORD_ZONE'
             PRINT *, 'COORD_ZONE OUTSIDE OF x=[',xyzzone(1,1),',',xyzzone(2,1),']'
             PRINT *, 'TIME: ', t
             PRINT *, 'STOPPING TIME FOR COORD_ZONE: ', tczn
             PRINT *, '*****************************************************'
            END IF
          END IF
       ELSE  
          IF (tfacczn>0.0_pr .AND. itfacczn>0.0_pr .AND. t>itczn) THEN
             itczn=t_end*10.0_pr
             czn_min = xyzzone(1,1)
             czn_max = xyzzone(2,1)
             xyzzone(1,1) = -Lczn
             xyzzone(2,1) =  Lczn
             cczn = MINVAL(SQRT(gamm/MW_in*Tconst))
             tczn = MAXVAL(ABS(xyzlimits(:,1)))/cczn*tfacczn
            IF (par_rank.EQ.0 .AND. verb_level>0) THEN
             PRINT *, '*****************************************************'
             PRINT *, 'APPLYING TEMPORARY COORD_ZONE'
             PRINT *, 'COORD_ZONE OUTSIDE OF x=[',-Lczn,',',Lczn,']'
             PRINT *, 'MINIMUM WAVE SPEED: ', cczn
             PRINT *, 'MAXIMUM DISTANCE: ', MAXVAL(ABS(xyzlimits(:,1)))
             PRINT *, 'TIME: ', t
             PRINT *, 'STARTING TIME FOR COORD_ZONE: ', itczn
             PRINT *, 'TIME SAFETY FACTOR: ', tfacczn
             PRINT *, 'STOPPING TIME FOR COORD_ZONE: ', tczn
             PRINT *, '*****************************************************'
            END IF
             IF (t<tczn) THEN
                modczn = .TRUE.
             ELSE
                IF (par_rank.EQ.0) PRINT *, '!!!TOO LATE TO START COORD_ZONE: t>tczn!!!'
             END IF
          END IF  
       END IF

       IF (buffRe .AND. buffRewait) THEN
          IF ( (t>xyzlimits(1,1)/c0(2)*(1.0_pr+waitrat)) .AND. (t>xyzlimits(2,1)/c0(1)*(1.0_pr+waitrat)) ) dobuffRe = .TRUE.
       END IF

       IF (savepardom) u(:,ndom) = REAL(par_rank,pr)

       IF (imask_obstacle) THEN
          IF (ndist > 0) CALL user_dist (nwlt, t, DISTANCE=u(:,ndist), NORMAL=norm ) 
          IF (saveUn) THEN
             Unorm = 0.0_pr
             Utan = 0.0_pr
             Utan(:, 1) = 0.0_pr
             DO idim = 1, dim
                Utan (:, 1) = Utan(:,1)+ u(:, nvel(idim))*norm(:,idim)
             END DO
             DO idim = 1, dim
                Unorm (:, idim) = Utan(:,1)*norm(:,idim)
             END DO
             DO idim = 1,dim
                Utan(:,idim)=u(:, nvel(idim))-Unorm(:,idim)
             END DO
             u(:,nUn) = SQRT(SUM(Unorm(:,1:dim)**2,DIM=2))
             u(:,nUtan) = SQRT(SUM(Utan(:,1:dim)**2,DIM=2))
          END IF
          u(:,nmsk) = penal
       END IF

       IF (saveUcvt) THEN  !12/2 - modified to carry u = x^2 function to validate surface integrals
!!$          CALL user_bufferfunc(bigU, nwlt, buffcvd, polyconv)
!!$          u(:,ncvtU) = 0.0_pr
!!$          DO j = 1,dim
!!$             IF (j == ICver ) THEN
!!$                u(:,ncvtU) = u(:,ncvtU) + (bigU(:,2*j-1) + bigU(:,2*j))*buffU0
!!$             ELSE
!!$                u(:,ncvtU) = u(:,ncvtU) + (-bigU(:,2*j-1) + bigU(:,2*j))*buffU0
!!$             END IF
!!$          END DO

          !u(:,ncvtU) = x(:,1)**2
       END IF


  END SUBROUTINE user_additional_vars

  SUBROUTINE bonusvars_vel(velvec)   !4extra - this whole subroutine
    IMPLICIT NONE
    REAL (pr), INTENT(IN) :: velvec(nwlt,dim) !velocity
    REAL (pr) :: tmp(nwlt,2*dim) !tmp for vorticity
    INTEGER :: i

    IF( adaptMagVort .OR. saveMagVort .OR. adaptComVort .OR. saveComVort .OR. adaptMagVel .OR. saveMagVel .OR. adaptNormS .OR. saveNormS) THEN
       tmp = 0.0_pr
       ! Calculate the magnitude vorticity
       IF( adaptMagVort .OR. saveMagVort .OR. adaptComVort .OR.  saveComVort ) THEN
          CALL cal_vort (velvec, tmp(:,1:3-MOD(dim,3)), nwlt)
          IF ( adaptMagVort .OR. saveMagVort )  u(:,nmvt) = SQRT(tmp(:,1)**2.0_pr + tmp(:,2)**2.0_pr + tmp(:,3)**2.0_pr )
          IF ( adaptComVort .OR. saveComVort )  THEN
             DO i=1,3
                u(:,ncvt(4-i)) = tmp(:,4-i)
             END DO
          END IF
       END IF
       ! Calculate the magnitude of Sij (sqrt(SijSij) )
       ! s(:,1) = s_11
       ! s(:,2) = s_22
       ! s(:,3) = s_33
       ! s(:,4) = s_12
       ! s(:,5) = s_13
       ! s(:,6) = s_23
       tmp = 0.0_pr
       IF( adaptNormS .OR. saveNormS) THEN
          CALL Sij( velvec ,nwlt, tmp(:,1:2*dim), u(:,nnms), .TRUE. )
       END IF
       ! Calculate the magnitude of velocity
       IF( adaptMagVel .OR. saveMagVel) THEN
          u(:,nmvl) = 0.5_pr*(SUM(velvec(:,:)**2.0_pr,DIM=2))
       END IF
    END IF
  END SUBROUTINE bonusvars_vel

  SUBROUTINE bonusvars_spc(spcvec)   !4extra - this whole subroutine
    IMPLICIT NONE
    REAL (pr), INTENT(IN) :: spcvec(nwlt,Nspecm) !species
    REAL (pr), DIMENSION(Nspecm,nwlt,dim) :: du, d2u    
    INTEGER :: i
    INTEGER, PARAMETER :: meth=1

    IF( adaptGradY .OR. saveGradY) THEN
       CALL c_diff_fast(spcvec(:,1:Nspecm), du(1:Nspecm,:,:), d2u(1:Nspecm,:,:), j_lev, nwlt,meth, 10, Nspecm, 1, Nspecm)  !dY/dx
       DO i=1,Nspecm
          u(:,ngdy(i)) = SUM(du(i,:,:)**2.0_pr,DIM=2) !|dY/dx_i*dY/dx_i|
       END DO
    END IF
  END SUBROUTINE bonusvars_spc

  !
  ! calculate any additional scalar variables
  ! 
  ! arg 
  ! flag  - 0 calledwhile adapting to IC, 1 - called in main time integration loop
  !
  SUBROUTINE user_scalar_vars( flag )
    IMPLICIT NONE
    INTEGER , INTENT(IN) :: flag ! 0- called during adaption to IC, 1 called during main integration loop

  END SUBROUTINE user_scalar_vars

  !
  !************ Calculating Scales ***************************
  !
  ! Note the order of the components in the scl array
  ! correspond to u_tn, v_tn, w_tn, u_tn-1, v_tn-1, w_tn-1, u_tn-2, v_tn-2, w_tn-2
  ! 
  SUBROUTINE user_scales(flag,use_default, u_loc, nlocal, ne_local, l_n_var_adapt , l_n_var_adapt_index, &
       scl, scl_fltwt) !add
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: flag ! 0 during initial adaptation, then 1 during time advancement
    LOGICAL , INTENT(INOUT) :: use_default
    INTEGER, INTENT (IN) :: nlocal, ne_local
    REAL (pr), DIMENSION (1:nlocal,1:ne_local), INTENT (IN) :: u_loc
    LOGICAL , INTENT (IN) :: l_n_var_adapt(ne_local)
    INTEGER , INTENT (IN) :: l_n_var_adapt_index(1:ne_local)
    REAL (pr), DIMENSION (1:ne_local), INTENT (INOUT) :: scl
    REAL (pr), DIMENSION (1:ne_local) :: mean
    REAL (pr) ,  INTENT(IN) ::scl_fltwt !weight for temporal filter on scl
    REAL (pr) :: floor, tmp,tmpmax,tmpmin,tmpsum
    INTEGER :: i, ie, ie_index, tmpint
    REAL (pr) ,save, ALLOCATABLE, DIMENSION (:) :: scl_old
    LOGICAL  ,save :: startup_init = .TRUE.

    !
    ! Ignore the output of this routine and use default scales routine
    !
    use_default = .FALSE. 

    IF( .NOT. use_default ) THEN
       !IF (par_rank.EQ.0) PRINT *,'Use user_scales() <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<'
       !
       ! ALLOCATE scl_old if it was not done previously in user_scales
       !
       IF( startup_init .AND. .NOT. ALLOCATED(scl_old) ) THEN
          ALLOCATE( scl_old(1:ne_local) )
          scl_old = 0.0_pr
       END IF

       floor = 1.0e-12_pr
       scl   =1.0_pr
       !
       ! Calculate scl per component
       !
       mean = 0.0_pr
       IF(.TRUE.) THEN
          DO ie=1,ne_local
             IF(l_n_var_adapt(ie)) THEN
                ie_index = l_n_var_adapt_index(ie)
                IF( Scale_Meth == 1 ) THEN ! Use Linf scale
                   tmpmax=MAXVAL ( u_loc(1:nlocal,ie_index) )
                   tmpmin=MINVAL ( u_loc(1:nlocal,ie_index) )
                   CALL parallel_global_sum(REALMAXVAL=tmpmax)
                   CALL parallel_global_sum(REALMINVAL=tmpmin)
                   mean(ie)= 0.5_pr*(tmpmax-tmpmin)
                ELSE IF ( Scale_Meth == 2 ) THEN ! Use L2 scale
                   tmpsum=SUM( u_loc(1:nlocal,ie_index) )
                   tmpint=nlocal
                   CALL parallel_global_sum(REAL=tmpsum)
                   CALL parallel_global_sum(INTEGER=tmpint)
                   mean(ie)= tmpsum/REAL(tmpint,pr)
                ELSE IF ( Scale_Meth == 3 ) THEN ! Use L2 scale using dA
                   tmpsum=SUM( u_loc(1:nlocal,ie_index)*dA )
                   CALL parallel_global_sum(REAL=tmpsum)
                   mean(ie)= tmpsum/ sumdA_global 
                END IF
             END IF
          END DO
       END IF
       DO ie=1,ne_local
          IF(l_n_var_adapt(ie)) THEN
             ie_index = l_n_var_adapt_index(ie)
             IF( Scale_Meth == 1 ) THEN ! Use Linf scale
                scl(ie)= MAXVAL ( ABS( u_loc(1:nlocal,ie_index) - mean(ie) ) )
                CALL parallel_global_sum( REALMAXVAL=scl(ie) )
             ELSE IF ( Scale_Meth == 2 ) THEN ! Use L2 scale
                scl(ie)= SUM( (u_loc(1:nlocal,ie_index)-mean(ie))**2.0_pr )
                tmpint=nlocal
                CALL parallel_global_sum( REAL=scl(ie) )
                CALL parallel_global_sum(INTEGER=tmpint)
                scl(ie)=SQRT(scl(ie)/REAL(tmpint,pr))
             ELSE IF ( Scale_Meth == 3 ) THEN ! Use L2 scale using dA
                scl(ie)= SUM( (u_loc(1:nlocal,ie_index)-mean(ie))**2.0_pr*dA )
                CALL parallel_global_sum(REAL=scl(ie))
                scl(ie)=SQRT(scl(ie)/sumdA_global)
             ELSE
                PRINT *, 'Error in Scales(), Unknown Scale_Meth type: ', Scale_Meth
                PRINT *, 'Known Scale_Meth types: 1 = Linf scale, 2 = L2 scale '
                PRINT *, 'Exiting ...'
                stop
             END IF
             
        !     IF (par_rank.EQ.0) THEN
        !        WRITE (6,'("Scaling before taking vector(1:dim) magnitude")')
        !        WRITE (6,'("Scale factor eqn", I4 ," = ", es11.4 ," scalecoeff = ",es11.4 )') &
        !             ie, scl(ie), scaleCoeff(ie)
        !     END IF
          END IF
       END DO
       !
       ! take appropriate vector norm over scl(1:dim)
       IF (adaptvelonly) THEN
          IF( Scale_Meth == 1 ) THEN ! Use Linf scale
             scl(nvon(1):nvon(dim)) = MAXVAL(scl(nvon(1):nvon(dim)))
          ELSE IF ( Scale_Meth == 2 ) THEN ! Use L2 scale
             scl(nvon(1):nvon(dim)) = SQRT(SUM(scl(nvon(1):nvon(dim))**2.0_pr))
          ELSE IF ( Scale_Meth == 3 ) THEN ! Use L2 scale using dA
             scl(nvon(1):nvon(dim)) = SQRT(SUM(scl(nvon(1):nvon(dim))**2.0_pr))
          END IF
       END IF
       IF( Scale_Meth == 1 ) THEN ! Use Linf scale
          scl(nvel(1):nvel(dim)) = MAXVAL(scl(nvel(1):nvel(dim)))
       ELSE IF ( Scale_Meth == 2 ) THEN ! Use L2 scale
          scl(nvel(1):nvel(dim)) = SQRT(SUM(scl(nvel(1):nvel(dim))**2.0_pr)) !now velocity vector length, rather than individual component
       ELSE IF ( Scale_Meth == 3 ) THEN ! Use L2 scale using dA
          scl(nvel(1):nvel(dim)) = SQRT(SUM(scl(nvel(1):nvel(dim))**2.0_pr)) !now velocity vector length, rather than individual component
       END IF
       !
       ! Print out new scl
       !
       IF (adaptbuffRe .AND. maskscl>0.0_pr) scl(nbRe)=maskscl
       IF (adapttmp .AND. tempscl>floor) scl(nton)=tempscl
       IF (adapttmp .AND. adaptvelonly .AND. ABS(tempscl)<floor) scl(nton)=scl(nvon(1))
       IF (adaptspconly .AND. specscl>0.0_pr) scl(nson(1):nson(Nspecm))=specscl
       DO ie=1,ne_local
          IF(l_n_var_adapt(ie)) THEN
             !this is done if one of the variable is exactly zero inorder not to adapt to the noise
             IF(scl(ie) .le. floor) scl(ie)=1.0_pr 
             tmp = scl(ie)
             ! temporally filter scl
             IF(.not. startup_init ) scl(ie) = scl_fltwt * scl_old(ie) + (1.0_pr - scl_fltwt) * scl(ie)
             scl = scaleCoeff * scl
             IF (par_rank.EQ.0 .AND. verb_level .GT. 0) THEN
                WRITE (6,'("Scale factor eqn", I4 ," = ", es11.4 ," scalecoeff = ",es11.4 )') &
                     ie, scl(ie), scaleCoeff(ie)
                WRITE (6,'("(before filtering = ",es11.4,")" )') tmp
             END IF
          END IF
       END DO
       
       scl_old = scl !save scl for this time step
       startup_init = .FALSE.
    END IF

  END SUBROUTINE user_scales

  SUBROUTINE user_cal_cfl (use_default, ulocal, cfl_out)
    USE precision
    USE sizes
    USE pde
    IMPLICIT NONE
    LOGICAL , INTENT(INOUT) :: use_default
    REAL (pr),                                INTENT (INOUT) :: cfl_out
    REAL (pr), DIMENSION (nwlt,n_integrated), INTENT (IN)    :: ulocal
    INTEGER                    :: i
    REAL (pr)                  :: flor
    REAL (pr), DIMENSION(nwlt,dim) :: cfl, cfl_conv, cfl_acou
    REAL (pr), DIMENSION(dim,nwlt) :: h_arr
    REAL (pr)                  :: min_h
    REAL (pr), DIMENSION(nwlt) :: gam, sos
    INTEGER :: l, mynwlt_global, fullnwlt, maxnwlt, minnwlt, avgnwlt

    IF (it>0) dt_original = mydt_orig

    use_default = .FALSE.

    flor = 1e-12_pr
    cfl_out = flor

    CALL get_all_local_h (h_arr)

    gam(:) = cp_in(Nspec)
    sos(:) = 1.0_pr/MW_in(Nspec) 
    IF (Nspec>1) gam(:) = (1.0_pr-SUM(ulocal(:,nspc(1):nspc(Nspecm)),DIM=2)/ulocal(:,nden))*cp_in(Nspec) !cp_Nspec
    IF (Nspec>1) sos(:) = (1.0_pr-SUM(ulocal(:,nspc(1):nspc(Nspecm)),DIM=2)/ulocal(:,nden))/MW_in(Nspec) !R_Nspec
    DO l=1,Nspecm
       gam(:) = gam(:) + ulocal(:,nspc(l))/ulocal(:,nden)*cp_in(l) !cp
       sos(:) = sos(:) + ulocal(:,nspc(l))/ulocal(:,nden)/MW_in(l) !R
    END DO
    gam(:) = gam(:)/(gam(:)-sos(:))
    sos(:) = SQRT(gam(:)*(gam(:)-1.0_pr)*(ulocal(:,neng)-0.5_pr*SUM(ulocal(:,nvel(1):nvel(dim))**2.0_pr,DIM=2)/ulocal(:,nden))/ulocal(:,nden))! spd of snd = sqrt(gamma p/rho)
    maxsos = MAXVAL(sos(:))
    CALL parallel_global_sum(REALMAXVAL=maxsos)


!    maxMa = MAXVAL(    SQRT(SUM(ulocal(:,nvel(1):nvel(dim))**2.0_pr,DIM=2)/ulocal(:,nden)**2.0_pr)/sos(:)* SQRT(gam(:))     )
    maxMa = MAXVAL(    SQRT(SUM(ulocal(:,nvel(1):nvel(dim))**2.0_pr,DIM=2)/ulocal(:,nden)**2.0_pr)/sos(:))

    CALL parallel_global_sum(REALMAXVAL=maxMa)

    DO i=1,dim
       cfl(:,i)      = ( ABS(ulocal(:,nvel(i))/ulocal(:,nden))+sos(:) ) * dt/h_arr(i,:)
       cfl_conv(:,i) = ( ABS(ulocal(:,nvel(i))/ulocal(:,nden))        ) * dt/h_arr(i,:)
       cfl_acou(:,i) = (                                       sos(:) ) * dt/h_arr(i,:)
    END DO
    CFLreport = MAXVAL(cfl(:,:))
    CFLconv   = MAXVAL(cfl_conv(:,:))
    CFLacou   = MAXVAL(cfl_acou(:,:))
    CALL parallel_global_sum(REALMAXVAL=CFLreport)
    CALL parallel_global_sum(REALMAXVAL=CFLconv)
    CALL parallel_global_sum(REALMAXVAL=CFLacou)
    min_h = MINVAL(h(1,1:dim)/2.0_pr**(REAL(j_mx,pr) - 1.0_pr))
    CALL parallel_global_sum(REALMINVAL=min_h)
    mynwlt_global = nwlt
    fullnwlt = PRODUCT(mxyz(1:dim)*2**(j_lev-1) + 1 - prd(:))
    CALL parallel_global_sum (INTEGER=mynwlt_global)
    avgnwlt = mynwlt_global/par_size 
    maxnwlt = nwlt
    minnwlt = -nwlt
    CALL parallel_global_sum(INTEGERMAXVAL=maxnwlt)
    CALL parallel_global_sum(INTEGERMAXVAL=minnwlt)
    minnwlt=-minnwlt
    MAreport = maxMa
    IF (convcfl ) THEN  !use acoustic cfl for initial states when acoustics dominate
       cfl_out = MAX(cfl_out, CFLconv)
    ELSE
       cfl_out = MAX(cfl_out, CFLreport)
    END IF

!!$    IF (t > 3.0_pr .AND. hypermodel .NE.0) THEN  !Reduce shock-capturing to density after impulsive transients dissipate
!!$       n_var_hyper = .FALSE.
!!$       !n_var_hyper(nden) = .TRUE.
!!$       n_var_hyper_active = .FALSE.
!!$       !n_var_hyper_active(nden) = .TRUE.
!!$    END IF

    IF (par_rank.EQ.0) THEN
       PRINT *, '**********************************************************'
       PRINT *, 'case:    ', file_gen(1:LEN_TRIM(file_gen)-1)
       PRINT *, 'max Ma = ', maxMa
       PRINT *, 'CFL    = ', cfl_out
       PRINT *, 'CFLconv= ', CFLconv
       PRINT *, 'CFLacou= ', CFLacou
       PRINT *, 'BL pts = ', 1.0_pr/((Re*Ma)**0.5_pr*min_h)
       !PRINT *, 'it     = ', it,             '  t       = ', t
       PRINT *, 'iwrite = ', iwrite-1,       '  dt      = ', dt 
       !PRINT *, 'j_mx   = ', j_mx,           '  dt_orig = ', dt_original
       PRINT *, 'j_mx   = ', j_mx,           '  twrite  = ', twrite
       !PRINT *, 'j_lev  = ', j_lev,          '  twrite  = ', twrite
       PRINT *, 'j_lev  = ', j_lev,          '  n%      = ', REAL(mynwlt_global,pr)/REAL(fullnwlt,pr)
       !PRINT *, 'nwlt_g = ', mynwlt_global,  '  cpu     = ', timer_val(2)
       !PRINT *, 'n_all  = ', fullnwlt,       '  n%      = ', REAL(mynwlt_global,pr)/REAL(fullnwlt,pr)
       !PRINT *, 'nwlt   = ', nwlt,           '  eps     = ', eps
       PRINT *, 'nwlt_mx= ', maxnwlt,        '  nwlt_av = ', avgnwlt
       PRINT *, 'nwlt_mn= ', minnwlt,        '  p_size  = ', par_size
       PRINT *, '**********************************************************'
       
    END IF
    maxMa = MIN(maxMa,1.0_pr)
  END SUBROUTINE user_cal_cfl

  SUBROUTINE user_init_sgs_model( )
    IMPLICIT NONE
  END SUBROUTINE user_init_sgs_model

  SUBROUTINE  user_sgs_force (u_loc, nlocal)
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: nlocal
    REAL (pr), DIMENSION (nlocal,n_integrated), INTENT (INOUT) :: u_loc
  END SUBROUTINE  user_sgs_force

  FUNCTION user_sound_speed (u, neq, nwlt)
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: nwlt, neq
    REAL (pr), DIMENSION (nwlt,neq), INTENT(IN) :: u
    REAL (pr), DIMENSION (nwlt) :: user_sound_speed,gam

    !user_sound_speed(:) = SQRT( gamm(1)*(gamm(1)-1.0_pr)*(u(:,neng)-0.5_pr*SUM(u(:,nvel(1:dim))**2.0_pr,DIM=2)/u(:,nden))) ! pressure
    !PRINT *, "c = ", MINVAL(user_sound_speed), MAXVAL(user_sound_speed), "nden = ", nden, MINVAL(u(:, nden)), MAXVAL(u(:, nden))

    gam(:) = cp_in(Nspec)
    user_sound_speed(:) = SQRT(gam(:)*(gam(:)-1.0_pr)*(u(:,neng)-0.5_pr*SUM(u(:,nvel(1):nvel(dim))**2.0_pr,DIM=2)/u(:,nden))/u(:,nden))! spd of snd = sqrt(gamma p/rho)

  END FUNCTION user_sound_speed

  SUBROUTINE  user_pre_process
    IMPLICIT NONE
    INTEGER :: i, l

    !PRINT *, "PRE PROCESS:", t

    IF (boundY) THEN
       DO i=1,nwlt
          IF (Nspec>1) u(i,nspc(1)) = MIN(MAX(u(i,nspc(1)),0.0_pr),u(i,nden))
       END DO
       DO l=2,Nspecm
          DO i=1,nwlt
             u(i,nspc(l)) = MIN(MAX(u(i,nspc(l)),0.0_pr),u(i,nden)-SUM(u(i,nspc(1):nspc(l-1))))
          END DO
       END DO
    END IF
  END SUBROUTINE user_pre_process

  SUBROUTINE  user_post_process
    IMPLICIT NONE
    !CALL user_pre_process  !for boundY

    !PRINT *, "POST PROCESS:", t


    !variable thresholding
    IF ( do_Adp_Eps_Spatial .OR. do_Adp_Eps_Spatial_Evol ) &
    CALL user_post_process__VT(n_var_SGSD)    ! n_var_ExtraAdpt_Der    ! n_var_Epsilon   ! n_var_RSGSD         ! Cf: OPTIONAL
  END SUBROUTINE user_post_process





END MODULE user_case
