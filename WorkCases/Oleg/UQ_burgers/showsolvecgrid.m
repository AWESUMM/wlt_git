function showsolvecgrid(solgrid,el_spm,el_myplot_comps,el_filebase,el_station_num)
%plot solution and/or grid
%written by Scot Reckinger
%___________________________________________________________________________________________
%INPUTS
% solgrid         - 1 for solution only, 2 for grid only, 3 for both side by side, 4 for both vertically stacked
% el_spm          - 's' for serial... 'p' for parallel
% el_myplot_comps - variable names of components to be plotted, list all in this form {'var1', 'var2', 'var3'}
% el_filebase     - file_gen from .inp file... without the '.' at the end... I add it automatically
% el_station_num  - station number (# of the .res file)
%___________________________________________________________________________________________
% PARAMETERS FOR BOTH SOLUTION AND GRID
    el_jcs = 0:20;   % jcs       - list of minimum levels for plotting in ascending order... put zero in places where you want to skip a color
    el_j_mx = 20;    % j_mx_grid - maximum level for plotting grid (basically a bound on the highest color level)
    el_watchit = 0; % watchit   - set 1 to pause at all color levels so that the levels can be "watched"... set 0 to do all color levels (no pausing)
    el_saveit = 0;  % saveit    - set 1 to save .fig... set 0 to not save figure   
    savestr = 'stuff';  %savestr: label to add to the filenames for saving

    casefoldersup = 0; %number of folders to climb up to get to the root of the case
    ppfoldersup = 3;   %number of folder to climb up to get to the trunk of the code
        
    slicenum = 1;   %for 3d cases, slice number to plot... set 0 to sweep through all planes 
                    %                            with pauses in between, and plot3d for grid
    dimtoslice = 1; %dimension to be sliced in for 3d cases with slicenum not equal to 0        

    colorbarloc = 'EastOutside';  %location of colorbar, set to 0 to not display colorbar
    legendloc   = 'EastOutside';  %location of legend, set to 0 to not display legend
    docont=1; %set 1 to use mycontourf, set 0 to use surf
    
    posfrac = [0.8 0.5];         %Percentage of screen size to set figure size... [horizontal vertical]... less than 1
    axesshift = [-0.05 0.0 0.02 0.0];  %relative shift applied to axes if centerting is needed... [left bottom width height]... normalized units, so width=1 uses the entire figure
    
    bounds=0; %[-1 1 0 1 -10 10];
    j_maxes=[0 0]; %j_mx for scalar and vector, e.g. [5 3], zeros are reset to 20 

    scalarnum = 1; %variable number in el_myplot_comps of scalar to be plotted, set to 0 to call user_scalarmod, set <0 to not plot a scalar
    vectornum = [2,3]; %variable numbers in el_myplot_comps of vectors to be plotted, set to [0, 0] to call user_vectormod, set either number <0 to not plot a vector
    
    xjmp = 8;  %step in x for plotting arrows in vector plot, set to 0 to use xpts
    yjmp = 8;  %step in y for plotting arrows in vector plot, set to 0 to use ypts    
    xpts = 0;  %number of arrows in x-direction for vector plot, set to 0 to plot every point
    ypts = 0;  %number of arrows in y-direction for vector plot, set to 0 to plot every point
    
    function [u_var_out] = user_scalarmod(u_vars_in)
        u_var_out = u_vars_in(:,:,2).^2 + u_vars_in(:,:,3).^2;
    end
    function [u_out,v_out] = user_vectormod(u_vars_in)
        u_out = u_vars_in(:,:,2)./u_vars_in(:,:,1);
        v_out = u_vars_in(:,:,3)./u_vars_in(:,:,1);
    end
%___________________________________________________________________________________________

    close all
    scrsz = get(0,'ScreenSize');
    if length(axesshift)~=4
        axesshift=[0 0 0 0];
    end
    if length(posfrac)==2
        for ii=1:2
            posfrac(ii)=max(min(posfrac(ii),1),0);
        end
    else
        posfrac=[0.5 0.5];
    end
    figure('Position',[scrsz(3)*(1.0-posfrac(1))/2 scrsz(4)*(1.0-posfrac(2))/2  scrsz(3)*posfrac(1) scrsz(4)*posfrac(2)])
    if solgrid==3
        subplot(1,2,2)
    elseif solgrid==4
        subplot(2,1,2)
    end
    if solgrid>1
        showgrid_gen(el_jcs,el_j_mx,el_watchit,el_spm,el_filebase,el_station_num)
        outerpos = get(gca,'Position');
        set(gca,'Position',outerpos+axesshift)
    end
    if solgrid==3
        subplot(1,2,1)
    elseif solgrid==4
        subplot(2,1,1)
    end
    if solgrid~=2
        showsol_gen(el_spm,el_myplot_comps,el_filebase,el_station_num)
        outerpos = get(gca,'Position');
        set(gca,'Position',outerpos+axesshift)
    end
    if el_saveit==1
        curstr=strcat('plots/',filebase,'_',savestr,'_',num2str(station_num),'.fig');
        saveas(gcf,curstr)
    end
  
    function showsol_gen(spm,myplot_comps,filebase,station_num)
        %plot scalar
        %written by Scot Reckinger
        %___________________________________________________________________________________________
        %INPUTS
        % spm          - 's' for serial... 'p' for parallel
        % myplot_comps - variable names of components to be plotted
        % filebase     - file_gen from .inp file... without the '.' at the end... I add it automatically
        % station_num  - station number (# of the .res file)
        %___________________________________________________________________________________________

        file=strcat(filebase,'.');
        
        local_dir = pwd;
        if isunix
            pl = '/';
        else
            pl = '\';
        end
        global POSTPROCESS_DIR
        POSTPROCESS_DIR = pwd;
        for i=1:ppfoldersup
            POSTPROCESS_DIR = [POSTPROCESS_DIR pl '..' ];
        end
        POSTPROCESS_DIR = [POSTPROCESS_DIR pl 'post_process'];
        path(path,POSTPROCESS_DIR)
        
        eps=0.0;
        az=0;
        el=90;
        j_maxes(j_maxes<=0)=20;
        
        for i=1:casefoldersup
            cd('..')
        end
        
        j_range = [0 j_maxes(1)];
        if scalarnum==0 
            for i=1:length(myplot_comps)
                [u_var,x,y,z,nx,ny,nz,dim,time] = inter3d(file,eps,bounds,char(myplot_comps(i)),j_range,station_num,spm);
                if i==1
                    u_var_all = zeros([size(u_var) length(myplot_comps)]);
                end
                if dim==2
                    u_var_all(:,:,i) = u_var;
                else
                    u_var_all(:,:,:,i) = u_var;
                end
            end
            u_var = user_scalarmod(u_var_all);
        elseif scalarnum>0
            [u_var,x,y,z,nx,ny,nz,dim,time] = inter3d(file,eps,bounds,char(myplot_comps(scalarnum)),j_range,station_num,spm);
        end   
        
        j_range = [0 j_maxes(2)];
        if min(vectornum)==0 && j_maxes(1)==j_maxes(2) && scalarnum==0
            u_var_vec=u_var_all;
            [u_vec,v_vec] = user_vectormod(u_var_vec);
            x_v=x; y_v=y; z_v=z; nx_v=nx; ny_v=ny; nz_v=nz;
        elseif min(vectornum)==0
            for i=1:length(myplot_comps)
                [u_vec,x_v,y_v,z_v,nx_v,ny_v,nz_v,dim,time] = inter3d(file,eps,bounds,char(myplot_comps(i)),j_range,station_num,spm);
                if i==1
                    u_var_vec = zeros([size(u_var_v) length(myplot_comps)]);
                end
                if dim==2
                    u_var_vec(:,:,i) = u_var_v;
                else
                    u_var_vec(:,:,:,i) = u_var_v;
                end
            end
            [u_vec,v_vec] = user_vectormod(u_var_vec);
        elseif min(vectornum)>0
            [u_vec,x_v,y_v,z_v,nx_v,ny_v,nz_v,dim,time] = inter3d(file,eps,bounds,char(myplot_comps(vectornum(1))),j_range,station_num,spm);
            [v_vec,x_v,y_v,z_v,nx_v,ny_v,nz_v,dim,time] = inter3d(file,eps,bounds,char(myplot_comps(vectornum(2))),j_range,station_num,spm);
        end
        if scalarnum<0
            x=x_v; y=y_v; z=z_v; nx=nx_v; ny=ny_v; nz=nz_v;
            u_var=u_vec*0.0;
        end
        cd(local_dir)
        
        if( dim==3 )
            cmin=min(min(min(u_var)));
            cmax=max(max(max(u_var)));
        else
            cmin=min(min(u_var));
            cmax=max(max(u_var));
        end
        if( cmin == cmax )
            cmin = cmin -1;
            cmax = cmax +1;
        end
        
        if dimtoslice==1
            nd=nx;
            dirstr='x';
        elseif dimtoslice==2
            nd=ny;
            dirstr='y';
        else
            nd=nz;
            dirstr='z';
        end
        
        sweeps=nd*2;
        if dim==2 || slicenum>0
            sweeps=1;
        end
        for myzslice = 1:sweeps
            zslice=mod(myzslice-1,nd-1)+1;
            if slicenum>0
                zslice=min(max(slicenum,1),nd);
            end
            set(cla,'nextplot','replace')
            if dim==2
                u_var_plot(:,:)=u_var(:,:);
                x_plot=x;
                y_plot=y;
            else
                if dimtoslice==1
                    u_var_plot(:,:)=u_var(zslice,:,:);
                    x_plot=y;
                    y_plot=z;
                elseif dimtoslice==2
                    u_var_plot(:,:)=u_var(:,zslice,:);
                    x_plot=x;
                    y_plot=z;
                else
                    u_var_plot(:,:)=u_var(:,:,zslice);
                    x_plot=x;
                    y_plot=y;
                end
            end
            if docont==1
                contourf(x_plot,y_plot,u_var_plot,256,'Linecolor','none');
            else
                surface(x_plot,y_plot,u_var_plot);
                shading interp;
            end
            if min(vectornum)>=0
                hold on
                
                if dim==2
                    u_vec_plot(:,:)=u_vec(:,:);
                    v_vec_plot(:,:)=v_vec(:,:);
                    x_v_plot=x_v;
                    y_v_plot=y_v;
                else
                    if dimtoslice==1
                        u_vec_plot(:,:)=u_vec(zslice,:,:);
                        v_vec_plot(:,:)=v_vec(zslice,:,:);                        
                        x_v_plot=y_v;
                        y_v_plot=z_v;
                    elseif dimtoslice==2
                        u_vec_plot(:,:)=u_vec(:,zslice,:);
                        v_vec_plot(:,:)=v_vec(:,zslice,:);
                        x_v_plot=x_v;
                        y_v_plot=z_v;
                    else
                        u_vec_plot(:,:)=u_vec(:,:,zslice);
                        v_vec_plot(:,:)=v_vec(:,:,zslice);
                        x_v_plot=x_v;
                        y_v_plot=y_v;
                    end
                end
                
                if xjmp==0
                    if xpts==0
                        xstp=1;
                    else
                        xstp=max(floor((length(x_v_plot)-1)/xpts),1);
                    end
                else
                    xstp=xjmp;
                end
                if yjmp==0
                    if ypts==0
                        ystp=1;
                    else
                        ystp=max(floor((length(y_v_plot)-1)/ypts),1);
                    end
                else
                    ystp=yjmp;
                end
                if docont==1
                    quiver(x_v_plot(1:xstp:end),y_v_plot(1:ystp:end),u_vec_plot(1:ystp:end,1:xstp:end,zslice),v_vec_plot(1:ystp:end,1:xstp:end,zslice),'k','LineWidth',2);
                else
                    [myXX, myYY] =meshgrid(x_v_plot(1:xstp:end),y_v_plot(1:ystp:end));
                    myZZ=ones(size(myXX))*cmax+1;
                    quiver3(myXX,myYY,myZZ,u_vec_plot(1:ystp:end,1:xstp:end,zslice),v_vec_plot(1:ystp:end,1:xstp:end,zslice), ... 
                                 zeros(size(u_vec_plot(1:ystp:end,1:xstp:end,zslice))),'k','LineWidth',2)
                end
                hold off
            end
            
            view(az,el);
            if cmin==cmax
                cmin=cmax-1;
            end
            caxis([cmin cmax]);
            set(gcf,'renderer','zbuffer');
            xmin=min(x_plot);
            xmax=max(x_plot);
            ymin=min(y_plot);
            ymax=max(y_plot);
            set(gca,'xlim',[xmin,xmax],'ylim',[ymin,ymax]);
            minlen=min((xmax-xmin),(ymax-ymin));
            yasp=(ymax-ymin)/minlen;
            xasp=(xmax-xmin)/minlen;
            zasp=(cmax-cmin)/minlen;
            set(gca,'PlotBoxAspectRatio',[xasp,yasp,zasp]);
            mypos=get(gca,'Position');
            if colorbarloc~=0
                colorbar(colorbarloc);
            end
            %set(gca,'clim',[cmin,cmax]);
            set(gca,'clim',[cmin,cmax]);
            set(gca,'FontSize',16)
            set(gca,'Position',mypos);
            if( sweeps ~= 1 )
                display(['Sweeping through domain - on ',dirstr,'-plane ', num2str(myzslice), ' of ', num2str(nd) ' - Hit ENTER to continue'])
                pause
            end           
        end
    end


    function showgrid_gen(jcs,j_mx_grid,watchit,spm,filebase,iter)
        %plot grid in variable colors
        %written by Scot Reckinger
        %___________________________________________________________________________________________
        %INPUTS
        % jcs       - list of minimum levels for plotting in ascending order... put zero in places where you want to skip a color
        % j_mx_grid - maximum level for plotting grid (basically a bound on the highest color level)
        % watchit   - set 1 to pause at all color levels so that the levels
        % can be "watched"... set 0 to do all color levels (no pausing)
        % spm       - 's' for serial... 'p' for parallel
        % filebase  - file_gen from .inp file... without the '.' at the end... I add it automatically
        % iter      - station_num (# of the .res file)
        %___________________________________________________________________________________________
        % PARAMETERS 
        maxsz=4;  %maximum marker size... I like 4
        minsz=1;  %minimum marker size... I like 1
        dotorcircle='o'; %  'o' or '.'... or whatever marker you want to plot
        maxcol=0.8;  %  maximum scale for colors... 1 is max... I like 0.8
        legmarksz = 8;  %marker size for legend... I like 8... 0 if you do not want a legend
        legdotorcircle='o'; %marker for legend... best to use 'o'
        %
        %casefoldersup = 1; %number of folders to climb up to get to the root of the case
        %ppfoldersup = 4;   %number of folder to climb up to get to the trunk of the code
        %
        %slicenum = 1;  %for 3d cases, slice number to plot... set 0 to use plot3d
        %dimtoslice = 3; %dimension to be sliced in for 3d cases with slicenum not equal to 0
        %
        %legendloc = 'EastOutside';  %location of legend, set to 0 to not display legend
        %___________________________________________________________________________________________
        
        file=strcat(filebase,'.');
        
        local_dir = pwd;
        if isunix
            pl = '/';
        else
            pl = '\';
        end
        global POSTPROCESS_DIR
        POSTPROCESS_DIR = pwd;
        for i=1:ppfoldersup
            POSTPROCESS_DIR = [POSTPROCESS_DIR pl '..' ];
        end
        POSTPROCESS_DIR = [POSTPROCESS_DIR pl 'post_process'];
        path(path,POSTPROCESS_DIR)
        
        eps=0.0;
        %bounds=0;
        az=0;
        el=90;
        x0=[0 0 0 2];
        n0=[[0 1 1]' [0 1 -1]'];
        
        station_num=iter;
        
        plot_component='Density_rho';
        foundjmx=0;
        jmxiter=1;
        
        delx=1.0e10;
        while foundjmx==0
            j_min=jmxiter;
            j_max=jmxiter;
            for i=1:casefoldersup
                cd('..')
            end
            [xstm,ystm,zstm,stm_indx,dim,nz,xmin, xmax, ymin, ymax, zmin, zmax, time] ...
                = res2matlab(file,eps,bounds,plot_component,j_min,j_max,station_num,1,x0,n0,spm);
            if slicenum>0 && dim==3
                if dimtoslice==1
                    stmsrt=sort(xstm);
                elseif dimtoslice==2
                    stmsrt=sort(ystm);
                else
                    stmsrt=sort(zstm);
                end
                xdiff=abs(stmsrt(2:end)-stmsrt(1:end-1));
                if sum(xdiff>0)>0
                    mydel=min(xdiff(xdiff>0));
                    delx=min(delx,mydel);
                end
            end
            cd(local_dir)
            if isempty(xstm)
                jmx=jmxiter-1;
                foundjmx=1;
            else
                jmxiter=jmxiter+1;
            end
        end
        
        if slicenum>0 && dim==3
            if dimtoslice==1
                xslice=max(min(xmin+(slicenum-1)*delx,xmax),xmin);
            elseif dimtoslice==2
                xslice=max(min(ymin+(slicenum-1)*delx,ymax),ymin);
            else
                xslice=max(min(zmin+(slicenum-1)*delx,zmax),zmin);
            end
        end
        
        jcs(jcs>j_mx_grid) = 0;
        jcs(jcs>jmx) = 0;
        jcs(jcs<0) = 0;
        
        numcolors = length(jcs);
        
        curlev=0;
        for i=1:numcolors
            if jcs(i)<=curlev
                jcs(i)=0;
            else
                curlev=jcs(i);
            end
        end
        
        myjcs=jcs(jcs>0);
        myjcs=[myjcs min(jmx,j_mx_grid)+1];
        clevs=sum(jcs>0);
        
        numcolorints=ceil((numcolors)/6);
        rgbarray=zeros(numcolorints*6+1,3);
        rgbarray(1,:)=[0 0 0];
        for i=1:numcolorints
            curval=(numcolorints-i+1)/numcolorints*maxcol;
            rgbarray((i-1)*6+2,:)=[curval 0 0];
            rgbarray((i-1)*6+3,:)=[0 curval 0];
            rgbarray((i-1)*6+4,:)=[0 0 curval];
            rgbarray((i-1)*6+5,:)=[0 curval curval];
            rgbarray((i-1)*6+6,:)=[curval 0 curval];
            rgbarray((i-1)*6+7,:)=[curval curval 0];
        end
        
        curc=1;
        for i=1:numcolors
            if jcs(i)>0
                rgbarray(curc,:)=rgbarray(i,:);
                curc=curc+1;
            end
        end
        
        if clevs==1
            szs(1)=maxsz;
        else
            if maxsz==minsz
                szs(1:clevs)=maxsz;
            else
                szs=maxsz:(minsz-maxsz)/(clevs-1):minsz;
            end
        end
        
        legiter=1;
        
        for curc=1:clevs
            j_min=myjcs(curc);
            j_max=myjcs(curc+1)-1;
            if j_max == 0
                j_max = -1;
            end
            if j_max>=j_min
                if zmax == zmin
                    zmin=zmax-1;
                end
                if dim == 3 && slicenum==0
                    plot3(xmax+1,ymax+1,zmax+1,legdotorcircle,'Markersize',legmarksz,'MarkerEdgeColor',rgbarray(curc,:),'MarkerFaceColor',rgbarray(curc,:))
                else
                    if dim==3 && dimtoslice==1
                        plot(ymax+1,zmax+1,legdotorcircle,'Markersize',legmarksz,'MarkerEdgeColor',rgbarray(curc,:),'MarkerFaceColor',rgbarray(curc,:))
                    elseif dim==3 && dimtoslice==2
                        plot(xmax+1,zmax+1,legdotorcircle,'Markersize',legmarksz,'MarkerEdgeColor',rgbarray(curc,:),'MarkerFaceColor',rgbarray(curc,:))
                    else
                        plot(xmax+1,ymax+1,legdotorcircle,'Markersize',legmarksz,'MarkerEdgeColor',rgbarray(curc,:),'MarkerFaceColor',rgbarray(curc,:))
                    end
                end
                if j_max==j_min
                    tempstr=['level ' num2str(j_max)];
                else
                    tempstr=['levels ' strcat(num2str(j_min),'-',num2str(j_max))];
                end
                forleg(1:length(tempstr),legiter)=tempstr;
                legiter=legiter+1;
                hold on
            end
        end
        
        for curc=1:clevs
            j_min=myjcs(curc);
            j_max=myjcs(curc+1)-1;
            if j_max == 0
                j_max = -1;
            end
            if j_max>=j_min
                for i=1:casefoldersup
                    cd('..')
                end
                [xstm,ystm,zstm,stm_indx,dim,nz,xmin, xmax, ymin, ymax, zmin, zmax, time] ...
                    = res2matlab(file,eps,bounds,plot_component,j_min,j_max,station_num,1,x0,n0,spm);
                cd(local_dir)
                
                if zmax == zmin
                    zmin=zmax-1;
                end
                
                minlen=min((xmax-xmin),(ymax-ymin));
                yasp=(ymax-ymin)/minlen;
                xasp=(xmax-xmin)/minlen;
                zasp=(zmax-zmin)/minlen;
                
                if dim == 3 && slicenum==0
                    plot3(xstm,ystm,zstm,dotorcircle,'Markersize',szs(curc),'MarkerEdgeColor',rgbarray(curc,:),'MarkerFaceColor',rgbarray(curc,:))
                    set(gca,'xlim',[ymin,ymax],'ylim',[xmin,xmax],'zlim',[zmin,zmax]);
                    set(gca,'PlotBoxAspectRatio',[xasp,yasp,zasp])
                    set(gca,'FontSize',16)
                    grid on
                else
                    if dim==2
                        plot(xstm,ystm,dotorcircle,'Markersize',szs(curc),'MarkerEdgeColor',rgbarray(curc,:),'MarkerFaceColor',rgbarray(curc,:))
                        set(gca,'PlotBoxAspectRatio',[xasp,yasp,zasp])
                        set(gca,'xlim',[xmin,xmax],'ylim',[ymin,ymax]);
                    else
                        if dimtoslice==1
                            mask=abs(xstm-xslice)<1.0e-10;
                            plot(ystm(mask),zstm(mask),dotorcircle,'Markersize',szs(curc),'MarkerEdgeColor',rgbarray(curc,:),'MarkerFaceColor',rgbarray(curc,:))
                            set(gca,'PlotBoxAspectRatio',[yasp,zasp,xasp])
                            set(gca,'xlim',[ymin,ymax],'ylim',[zmin,zmax]);
                        elseif dimtoslice==2
                            mask=abs(ystm-xslice)<1.0e-10;
                            plot(xstm(mask),zstm(mask),dotorcircle,'Markersize',szs(curc),'MarkerEdgeColor',rgbarray(curc,:),'MarkerFaceColor',rgbarray(curc,:))
                            set(gca,'PlotBoxAspectRatio',[xasp,zasp,yasp])
                            set(gca,'xlim',[xmin,xmax],'ylim',[zmin,zmax]);
                        else
                            mask=abs(zstm-xslice)<1.0e-10;
                            plot(xstm(mask),ystm(mask),dotorcircle,'Markersize',szs(curc),'MarkerEdgeColor',rgbarray(curc,:),'MarkerFaceColor',rgbarray(curc,:))
                            set(gca,'PlotBoxAspectRatio',[xasp,yasp,zasp])
                            set(gca,'xlim',[xmin,xmax],'ylim',[ymin,ymax]);
                        end
                    end
                    set(gca,'FontSize',16)
                end
                hold on
                view(az,el);
                if slicenum==0 && dim==3
                    view(30,30);
                end
                set(gca,'FontSize',16)
                if watchit
                    pause
                end
            end
        end
        mypos=get(gca,'Position');
        if legendloc~=0
           legend(forleg','Location',legendloc)
        end
        set(gca,'FontSize',16)
        hold off
        set(gca,'Position',mypos);     
    end
end 
