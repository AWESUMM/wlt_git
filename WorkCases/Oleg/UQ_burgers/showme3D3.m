function showme3D3(fileName,st_num)

  figure(1)
  showme3D(fileName,'U',st_num,'solution')
  set(gca,'PlotBoxAspectRatio',[1 1 1])
  ylabel('\beta'); xlabel('x'); title(['U,  t=' num2str(0.1*st_num,'%2.1f')]);
  print('-dpdf',['U_t'  num2str(0.1*st_num,'%2.1f') '_1024x1280.pdf']);
  figure(2)
  showme3D(fileName,'visc',st_num,'solution')
  set(gca,'PlotBoxAspectRatio',[1 1 1])
  ylabel('\beta'); xlabel('x'); title(['\nu_t,  t=' num2str(0.1*st_num,'%2.1f')]);
  print('-dpdf',['visc_t'  num2str(0.1*st_num,'%2.1f') '_1024x1280.pdf']);
  figure(3)
  showme3D(fileName,'U',st_num,'grid')
  set(gca,'PlotBoxAspectRatio',[1 1 1])
  ylabel('\beta'); xlabel('x'); title(['Grid,  t=' num2str(0.1*st_num,'%2.1f')]);
  print('-dpdf',['grid_t'  num2str(0.1*st_num,'%2.1f') '_1024x1280.pdf']);

end
