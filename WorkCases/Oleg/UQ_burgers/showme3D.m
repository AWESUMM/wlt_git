%function out = showme3D(fileName, figType, plotType, varName, time)

function showme3D(fileName,varName,st_num,fig_type,usestat)
global RYB
global Umn
global xh yh zh uh % for UQ use
global yf xf ye xe

Umn = 0.0;
varName
if ~isempty(strmatch(varName,'modWij','exact'))
    RYB=logical(1);
else
    RYB=logical(0);
end
local_dir = pwd
if isunix
    pl = '/'
else
    pl = '\';
end
global POSTPROCESS_DIR
%POSTPROCESS_DIR = '/home/vasilyev/wlt_3d_DBs_Jonathan/post_process'
%POSTPROCESS_DIR = 'D:\Oleg_Vasilyev\Research\FORTRAN\wlt_3d_DBs\post_process'
POSTPROCESS_DIR = [pwd pl '..' pl '..' pl '..' pl 'post_process']

path(path,POSTPROCESS_DIR)
path(path,[POSTPROCESS_DIR pl 'sliceomatic'])
clear pl
cd(local_dir)

% plot output from poisson_3d.out
%
%
% args 
%    file       output file name
%    j_range    = [j_min j_max] (range of levels to plot )
%    eps        plot only valuse > eps (if<0  percent of wlt coeff. range)
%    bounds     = [xmin xmax ymin ymax zmin zmax] plot within bounds
%    fig_type   'coeff' = plot wlt coefficents, 'grid' = plot grid, 'solution' = plot solution
%    plot_type  'surf' = surface plots, 'contour' = contour plots, 'isosurface' = plot isosurface
%               'slice' = sliceomatic
%    obstacle   'none' = no obstacle plot, 'sphere', 'cylinder' 
%    az, el     azimuth and elevation for the direction to look at the plot
%    slev       level of isosurface choosed (in % of maximum value)
%    plot_comp ux, uy, uz, wx, wy, wz, magu, magw
%    station_num         station # (ie. the number of the output file
%    fignum     figure number to use for output
%   x0 - coordinate and the slise half thickness, e.g.  x0=[x y z del];
%   n0 - array of vectors normal to the slice planes, e.g. n0=[[0 1 1]' [0 1 -1]']; 

%
%
% function pl = c_wlt_3d(file,j_range,eps,bounds,fig_type,plot_type,obstacle,az,el,slev,plot_comp,station_num,fignum,x0,n0)
figure(1);
c_wlt_3d( ...
 fileName ... %file,
, [ 0 20] ...% j_range,
, 0.0 ...% eps,
, [-2 5 -2 2] ...% bounds,
, fig_type ...% fig_type,
, 'surf' ...% plot_type,
, 'none' ...% obstacle,
, 0 ...% az,
, 0 ...% el,
, 0.9 ...% slev,
, varName ...% plot_comp,
, st_num ...% station_num,
, 0 ...% fignum,youthe
, [0 0 0 1000]...% x0,
, [[0 1 0]' [1 0 0]'] ) % n0 
set(gca,'View',[0 90])

if nargin> 4 & usestat==1
  Jrefine=2;
  il=find(max(0,yh+1+1e-12),1,'first');
  ir=find(max(0,yh-1+1e-12),1,'first');
  Xi=linspace(min(xh),max(xh),(length(xh)-1)*2^Jrefine+1);
  for iy=il:ir
      i=iy-il+1;
      Ui=interp1(xh,uh(iy,:,1),Xi,'spline');
      Ys(i)=yh(iy);
      %dUi=diff(Ui);
      %imx=find(abs(dUi)==max(abs(dUi)),1);
      %Xs(i)=0.5*(Xi(imx)+Xi(imx+1));
      %find(abs(Ui)>max(abs(Ui))-1.0e-7)
      Xs(i)=mean(Xi(find(abs(Ui)==max(abs(Ui)))));
      %Xs(i)=Xi(find(Ui-max(Ui)==0,1));
      Xse(i)=atan2(sqrt(1.0-yh(iy)^2),-yh(iy))/pi;
  end
  figure(2);
  plot(Ys,Xs,Ys,Xse);
  legend('numerical','theoretical',0);
  ylabel('\beta');
  xlabel('x_s');

  %PDF distribution
  Npdf=1.0e+06;
  alph =0.6*randn(Npdf,1);
  bet=0*alph;
  ind=find(alph);
  bet(ind) = (-1+sqrt(1+4*alph(ind).^2))/2./alph(ind);
  Xbet=interp1(Ys,Xs,bet,'spline');
  Xsbet=atan2(sqrt(1.0-bet.^2),bet)/pi;
  figure(3);
  [xf,yf]=ksdensity(Xbet);
  [xe,ye]=ksdensity(Xsbet);
  plot(yf,xf,ye,xe);
  legend('numerical','theoretical',0);
end
    