!#define DISTANCE_FUNCTION
!when compile put -DDISTANCE_FUNCTION


MODULE user_case_vars
  USE precision
  REAL (pr) :: eta !penalization parameter
  REAL (pr) :: Re, Rebnd, Pra, Sc, Fr, Ma, At
  INTEGER :: Nspec, Nspecm, methpd
  INTEGER :: nden, neng, nprs, ndpx, nboy, nbRe, ndom, nton, BCver,ICver,nmsk,ncvtU
  INTEGER, ALLOCATABLE :: nvel(:), nspc(:), nvon(:), nson(:), nUn(:)
  REAL (pr) :: rhoL, pL, EL, rhoR, pRR, ER
  REAL (pr), DIMENSION(:), ALLOCATABLE :: uL, uR
  REAL (pr) :: xw, wa
  REAL (pr) :: deltadel_loc
  REAL (pr) :: delta_conv, delta_diff, shift_conv, shift_diff
  LOGICAL :: smooth_dist
END MODULE user_case_vars

!$fs brinkman module
!****************************************************************************
!* Module for adding Brinkman Penalization terms onto the full NS equations *
!*  -Only valid for single species 10/7/2011                                *
!*  -R is scaled to 1.0
!****************************************************************************

MODULE Brinkman
  USE precision
  USE wlt_vars
  USE wavelet_filters_mod
  USE elliptic_mod
  USE elliptic_vars
  USE wlt_trns_mod
  USE wlt_trns_vars
  USE wlt_trns_util_mod
  USE io_3d_vars
  USE util_mod
  USE util_vars
  USE share_consts
  USE pde
  USE sizes
  USE share_kry
  USE vector_util_mod
  USE field
  USE input_file_reader
  USE additional_nodes
  USE penalization
  USE user_case_vars

CONTAINS

  SUBROUTINE user_dist (nlocal, t_local, DISTANCE, NORMAL)
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: nlocal
    REAL (pr), INTENT (IN) :: t_local
    REAL (pr), DIMENSION(nlocal), OPTIONAL, INTENT(INOUT) :: DISTANCE
    REAL (pr), DIMENSION(nlocal, dim), OPTIONAL, INTENT(INOUT) :: NORMAL
    REAL (pr), DIMENSION(nlocal) :: DST
    REAL (pr), DIMENSION(nlocal,dim) :: xp
    INTEGER :: idim
    REAL (pr), DIMENSION(dim) :: Xs

    IF(PRESENT(DISTANCE) .OR. .NOT.use_dist ) THEN
       !PRINT *, 'Distance function is calculated'
       !Find distance function
       pi = 2.0_pr*ASIN(1.0_pr)
       xp= 0.0_pr
       Xs(1) = 1.5_pr
       Xs(2) = (1.5-xw)*TAN(wa*pi/180.0_pr)
       xp(:,1) = (x(:,1)-Xs(1))*COS(wa*pi/180.0_pr) + (x(:,2)-Xs(2))*SIN(wa*pi/180.0_pr) 
       xp(:,2) = (x(:,1)-Xs(1))*SIN(wa*pi/180.0_pr) - (x(:,2)-Xs(2))*COS(wa*pi/180.0_pr)
       !WEDGE ONLY:
       DST(:) = xp(:,2)

!!$       WHERE(xp(:,1) <= 0.0_pr) 
!!$          DST(:) = ABS(xp(:,2))
!!$       ELSEWHERE
!!$          DST(:) = SQRT(SUM(xp(:,1:dim)**2,DIM=2))
!!$       END WHERE
!!$       xp(:,1) = x(:,1)-Xs(1)
!!$       xp(:,2) = x(:,2)-Xs(2)
!!$       WHERE(xp(:,1) >= 0.0_pr) 
!!$          DST(:) = MIN(DST(:),ABS(xp(:,2)))
!!$       ELSEWHERE
!!$          DST(:) = MIN(DST(:),SQRT(SUM(xp(:,1:dim)**2,DIM=2)))
!!$       END WHERE
!!$
!!$       WHERE ( ((x(:, 2) - (x(:, 1) - xw)*TAN(wa*pi/180.0_pr) <= 0.0_pr) .AND. (x(:, 1) <= 1.5_pr)) .OR. &
!!$            ((x(:, 1) > 1.5_pr) .AND. (x(:, 2) <= (1.5_pr - xw)*TAN(wa*pi/180.0_pr))) ) 
!!$          DST(:) = DST(:)
!!$       ELSEWHERE
!!$          DST(:) = -DST(:)
!!$       END WHERE

       IF(smooth_dist) THEN
          CALL diffsmooth (DST,nlocal,1,HIGH_ORDER,11,0,0.5_pr,-MINVAL(h(j_mx,1:dim)),deltadel_loc,-2,j_lev,3,.FALSE.)
       END IF

       IF (use_dist) dist = DST
    ELSE
       DST = dist
    END IF

    IF(PRESENT(DISTANCE)) DISTANCE = DST

    !Definition of NORMAL in the obstacle

    IF(PRESENT(NORMAL)) THEN
       CALL c_diff_fast(DST, NORMAL(:, :), xp(:, :), j_lev, nlocal, HIGH_ORDER, 10, 1, 1, 1)
    
       xp(:,1) = SQRT(SUM(NORMAL(:,1:dim)**2,DIM=2))
       DO idim = 1, dim
          NORMAL(:,idim) = NORMAL(:,idim)/xp(:,1)
       END DO
    END IF

  END SUBROUTINE user_dist

  SUBROUTINE Brinkman_rhs (user_rhs, u_integrated, homog, meth)
    USE user_case_vars
    IMPLICIT NONE
    REAL (pr), DIMENSION (n), INTENT(INOUT) :: user_rhs
    REAL (pr), DIMENSION (ng,ne), INTENT(IN) :: u_integrated !1D flat version of integrated variables without BC 
    INTEGER, INTENT(IN) :: meth
    LOGICAL, INTENT(IN) :: homog

    INTEGER :: ie, shift,i, idim
    REAL (pr), DIMENSION (ng,dim)     :: dp
    REAL (pr), DIMENSION (ng,dim)     :: Unorm, Utan, xp, norm
    
    REAL (pr), DIMENSION (ng,ne) :: uh
    REAL (pr), DIMENSION (ne,ng,dim) :: du, d2u
    REAL (pr), DIMENSION (ne,ng,dim) :: duBD, duFD
    REAL (pr), DIMENSION (ng) :: Spenal,Spenal2
    REAL (pr), DIMENSION (ne) :: Ugoal
    REAL (pr) :: delta_Jmx
    INTEGER :: meth_central, meth_backward, meth_forward  
    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc

    meth_central  = meth + BIASING_NONE
    meth_backward = meth + BIASING_BACKWARD
    meth_forward  = meth + BIASING_FORWARD

    IF (use_dist) THEN
       CALL user_dist (ng, t, NORMAL=norm)
       Spenal = dist
    ELSE
       CALL user_dist (ng, t, DISTANCE=Spenal, NORMAL=norm)
    END IF
    Spenal2= Spenal
    !MASK within penal to add diffusion and sink
    delta_Jmx = MINVAL(h(j_mx,1:dim))
    Spenal  = 0.5_pr*(1.0_pr - TANH( Spenal/(delta_conv*delta_Jmx)  - shift_conv))*penal
    Spenal2 = 0.5_pr*(1.0_pr + TANH( Spenal2/(delta_diff*delta_Jmx) - shift_diff))*penal

    Utan(:, 1) = 0.0_pr
    DO idim = 1, dim
       Utan (:, 1) = Utan(:,1)+ u_integrated(:, nvel(idim))*norm(:,idim)
    END DO
    DO idim = 1, dim
       Unorm (:, idim) = Utan(:,1)*norm(:,idim)
    END DO
    Utan(:,1:dim)=u_integrated(:, nvel(1:dim))-Unorm(:,1:dim)

    uh = u_integrated
    uh(:, nvel(1):nvel(dim)) = Utan(:,1:dim)
    CALL c_diff_fast(uh, duBD(1:ne, :, :), d2u(1:ne, :, :), j_lev, ng, meth_backward, 11, ne, 1, ne)    
    CALL c_diff_fast(uh, duFD(1:ne, :, :), d2u(1:ne, :, :), j_lev, ng, meth_forward, 11, ne, 1, ne)    
    CALL c_diff_fast(u_integrated, du(1:ne, :, :),   d2u(1:ne, :, :), j_lev, ng, meth_central, 11, ne, 1, ne)    

    !For testoing purposes only, sets NN RHS to zero inside of Brinkman zone
    DO ie = 1,ne
       user_rhs((ie-1)*ng+1:ie*ng) = (1.0_pr-penal)*user_rhs((ie-1)*ng+1:ie*ng) 
    END DO


    !Continuity Equation
    DO i = 1,dim
       user_rhs((nden-1)*ng+1:nden*ng) = user_rhs((nden-1)*ng+1:nden*ng) - Spenal* &  ! cont eq penalization
            0.5_pr*( duBD(nden, :, i)*(norm(:,i)+ABS(norm(:,i))) + duFD(nden, :, i)*(norm(:,i)-ABS(norm(:,i))) )/eta    
    END DO

    !Momentum Equation
    DO ie = 1,dim
       DO i = 1, dim
          user_rhs((nvel(ie)-1)*ng+1:nvel(ie)*ng) = user_rhs((nvel(ie)-1)*ng+1:nvel(ie)*ng) - Spenal* &
              0.5_pr*( duBD(nvel(ie), :, i)*(norm(:,i)+ABS(norm(:,i))) + duFD(nvel(ie), :, i)*(norm(:,i)-ABS(norm(:,i))) )/eta    
     END DO
       user_rhs((nvel(ie)-1)*ng+1:nvel(ie)*ng) = user_rhs((nvel(ie)-1)*ng+1:nvel(ie)*ng) -  Spenal*Unorm(:,ie)/eta
    END DO

    !Energy Equation
    DO i = 1,dim
       user_rhs((neng-1)*ng+1:neng*ng) = (1.0_pr-penal)*user_rhs((neng-1)*ng+1:neng*ng) -  Spenal* &    ! cont eq penalization
                           0.5_pr*( duBD(neng, :, i)*(norm(:,i)+ABS(norm(:,i))) + duFD(neng, :, i)*(norm(:,i)-ABS(norm(:,i))) )/eta    
    END DO

    IF(homog) THEN
       Ugoal=0.0_pr
    ELSE
       Ugoal(nden) = rhoR
       Ugoal(nvel(1:dim)) = rhoR*uR(1:dim)
       Ugoal(neng) = ER
    END IF


    !Diffusion
    DO ie = 1, ne
       shift = ng*(ie-1)
       !--Go through all Boundary points that are specified
       i_p_face(0) = 1
       DO i=1,dim
          i_p_face(i) = i_p_face(i-1)*3
       END DO
       DO face_type = 0, 3**dim - 1
          face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
          CALL get_all_indices_by_face (face_type, j_lev, nloc, iloc)
          IF(nloc > 0 ) THEN 
             DO i = 1, dim
                user_rhs(shift+iloc(1:nloc)) = user_rhs(shift+iloc(1:nloc)) + (1.0_pr-ABS(face(i)))*delta_Jmx* &
                                               Spenal2(iloc(1:nloc))*d2u(ie,iloc(1:nloc),i) ! No diffusion normal to the boundary
             END DO
          END IF
       END DO
    END DO

  END SUBROUTINE Brinkman_rhs


! find Jacobian of Right Hand Side of the problem
SUBROUTINE  Brinkman_Drhs (user_Drhs, u, u_prev, meth)
    USE penalization
    USE user_case_vars
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: meth
    REAL (pr), DIMENSION (ng,ne), INTENT(IN) :: u, u_prev
    REAL (pr), DIMENSION (n), INTENT(INOUT) :: user_Drhs

    INTEGER :: ie, shift,i
    REAL (pr), DIMENSION (2*ne,ng,dim) :: du  ! 1st derivatives for u (in du(1:ne,:,:)) and u_prev_timestep (in du(ne+1:2*ne,:,:))
    REAL (pr), DIMENSION (ne  ,ng,dim) :: d2u ! 2nd derivatives for u 
    REAL (pr), DIMENSION (ne  ,ng,dim) :: du_dummy ! passed when only calculating 1st derivative.
    !Find batter way to do this!! du_dummy with no storage..
!
! User defined variables
!
!    REAL (pr), DIMENSION (ng,n_integrated,dim) :: F

    REAL (pr), DIMENSION (ng,ne) :: uh, uh_b
    REAL (pr), DIMENSION (ng) ::  rho_b, e_b, press_b,TT_b
    REAL (pr), DIMENSION (ng) ::  rho, e, press, TT
    REAL (pr), DIMENSION (ng,dim) ::vel, vel_b
    REAL (pr), DIMENSION (ng) ::  Fx
    REAL (pr), DIMENSION (ng,2) :: dFx, dDum
    REAL (pr), DIMENSION (ng,2) :: dUx_b, dUy_b  
    
    
    ! because linear penalization we can simply do that
    CALL Brinkman_rhs (user_Drhs, u, .TRUE., meth) !add penalization term

END SUBROUTINE Brinkman_Drhs

  !
  ! Uses u_prev_timestep, which is global flat (1D ) array set in time_step_cn()
  !
  SUBROUTINE  Brinkman_Drhs_diag (user_Drhs_diag, meth)
    USE user_case_vars
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: meth
    REAL (pr), DIMENSION (n), INTENT(INOUT) :: user_Drhs_diag
    INTEGER :: ie, shift,i
    REAL (pr), DIMENSION (ng,dim) :: du_diag, d2u_diag
!
! User defined variables
!
    REAL (pr), DIMENSION (ng,ne) :: u_prev

    CALL c_diff_diag(du_diag, d2u_diag, j_lev, ng, meth, meth, 10)
    
    !For testoing purposes only, sets NN RHS to zero inside of Brinkman zone
    DO ie = 1,ne
       user_Drhs_diag((ie-1)*ng+1:ie*ng) = (1.0_pr-penal)*user_Drhs_diag((ie-1)*ng+1:ie*ng) 
    END DO

!!$    !Continuity Equation
!!$    DO i = 1,dim
!!$       user_Drhs_diag((nden-1)*ng+1:nden*ng) = user_Drhs_diag((nden-1)*ng+1:nden*ng) - penal*du_diag(:, i)*norm(i)/eta     ! cont eq penalization
!!$    END DO
!!$    !Momentum Equation
!!$    DO ie = 1,dim
!!$       DO i = 1,dim
!!$          user_Drhs_diag((nvel(ie)-1)*ng+1:nvel(ie)*ng) = user_Drhs_diag((nvel(ie)-1)*ng+1:nvel(ie)*ng) - penal*du_diag(:, i)*norm(i)*(1.0_pr-norm(ie)**2)/eta
!!$       END DO
!!$          user_Drhs_diag((nvel(ie)-1)*ng+1:nvel(ie)*ng) = user_Drhs_diag((nvel(ie)-1)*ng+1:nvel(ie)*ng) - penal*norm(ie)**2/eta
!!$    END DO
!!$
!!$    !Energy Equation
!!$    DO i = 1,dim
!!$       user_Drhs_diag((neng-1)*ng+1:neng*ng) = user_Drhs_diag((neng-1)*ng+1:neng*ng) - penal*du_diag(:, i)*norm(i)/eta     ! cont eq penalization
!!$    END DO

    DO ie = 1,ne
       user_Drhs_diag((ie-1)*ng+1:ie*ng) = user_Drhs_diag((ie-1)*ng+1:ie*ng) + penal/eta
    END DO

  END SUBROUTINE Brinkman_Drhs_diag

END MODULE Brinkman
!$fe end of brinkmad module

MODULE user_case
  USE precision
  USE wlt_vars
  USE wavelet_filters_mod
  USE elliptic_mod
  USE elliptic_vars
  USE wlt_trns_mod
  USE wlt_trns_vars
  USE wlt_trns_util_mod
  USE io_3d_vars
  USE util_mod
  USE util_vars
  USE share_consts
  USE pde
  USE sizes
  USE share_kry
  USE vector_util_mod
  USE field
  USE input_file_reader
  USE parallel
  USE user_case_vars
  USE Brinkman
  USE hyperbolic_solver
  !
  ! case specific variables
  !

  REAL (pr) :: MAreport, CFLreport, CFLconv, CFLacou, maxsos, mydt_orig
  REAL (pr) :: peakchange, maxMa
  REAL (pr) :: Tconst, Pint, gammR, Y_Xhalf
  REAL (pr), ALLOCATABLE, DIMENSION (:) :: pureX, pureY, pureR, pureg, puregamma, YR
  REAL (pr) :: e0top, e0bot, e0right,e0left, rho0top, drho0top, rho0bot, drho0bot, rhoetopBC, rhoebotBC, rhoetop, rhoebot ,velleft
  REAL (pr), ALLOCATABLE, DIMENSION (:) :: Y0top, Y0bot, rhoYtopBC, rhoYbotBC, rhoYtop, rhoYbot, Lxyz
  REAL (pr), DIMENSION (2) :: rhoYg, drho0, dP0, c0, P0, gammBC
  REAL (pr), ALLOCATABLE, DIMENSION (:) ::  mu_in, kk_in, cp_in, MW_in, gamm
  REAL (pr), ALLOCATABLE, DIMENSION (:) :: bD_in, gr_in
  REAL (pr), ALLOCATABLE, DIMENSION (:,:) :: bndvals
  REAL (pr), DIMENSION (5) :: buffRed, buffRef, buffcvd, buffcvf, buffbfd, buffbff
  LOGICAL :: NS ! if NS=.TRUE. - Navier-Stoke, else Euler
  LOGICAL, PARAMETER :: NSdiag=.TRUE. ! if NSdiag=.TRUE. - use viscous terms in user_rhs_diag
  INTEGER :: nspc_lil, nspc_big
  INTEGER :: n_var_pressure
  LOGICAL :: GRAV, adaptAt
  LOGICAL :: evolBC, buffBC, cnvtBC
  LOGICAL :: polybuffRe, polybuff, pertchar, usedP0, polyconv, dozeroFlux, pBrink
  INTEGER :: globFlux, dervFlux, PLpinf 
  REAL (pr) :: buffBeta, buffU0, buffSig, buffDomFrac, buffOff, buffFac, pBcoef, pBsig
  LOGICAL :: adaptbuoy, savebuoys, saveUn
  LOGICAL :: adaptMagVort, saveMagVort, adaptComVort, saveComVort, adaptMagVel, saveMagVel, adaptNormS, saveNormS, adaptGradY, saveGradY, saveUcvt  !4extra - this whole line
  INTEGER :: nmvt, ncvt(3), nmvl, nnms !4extra - this whole line
  INTEGER, ALLOCATABLE :: ngdy(:) !4extra - this whole line
  REAL (pr) :: Lczn, tfacczn, itfacczn, itczn, tczn, cczn, czn_min, czn_max
  LOGICAL :: boundY, modczn, splitFBpress, stepsplit, savepardom
  REAL (pr) :: waitrat, maskscl, tempscl, specscl
  INTEGER :: LODIit, locnvar
  LOGICAL :: dobuffRe, buffRe, adaptbuffRe, buffRewait, LODIset, LODIslipeasy, adaptden, adaptvel, adapteng, adaptspc, adaptprs 
  LOGICAL :: adaptvelonly, adaptspconly, adapttmp, doassym, convcfl
  REAL (pr) :: phi
  REAL (pr) :: shock

CONTAINS

SUBROUTINE  user_setup_pde ( VERB ) 
  USE hyperbolic_solver
  IMPLICIT NONE
    LOGICAL, OPTIONAL :: VERB         ! print debug info
    INTEGER :: i, l   !4extra
    CHARACTER(LEN=8) :: specnum  !4extra
    REAL(pr) :: nd_assym_high, nd_assym_bnd_high, nd2_assym_high, nd2_assym_bnd_high, nd_assym_low, nd_assym_bnd_low, nd2_assym_low, nd2_assym_bnd_low
 
    locnvar = dim+Nspec+1
 
    IF (par_rank.EQ.0) THEN
       PRINT * ,''
       PRINT *, '**********************Setting up PDE*****************'
       PRINT * ,'CASE: Rayleigh-Taylor Instability '
       PRINT *, '*****************************************************'
    END IF

    n_integrated = dim + 1 + Nspec      

!***********Variable orders, ensure 1:dim+1+Nspec are density, velocity(dim), energy, species(Nspecm)**************
    IF (ALLOCATED(nvel)) DEALLOCATE(nvel)
    ALLOCATE(nvel(dim))
    IF (ALLOCATED(nspc)) DEALLOCATE(nspc)
    IF (Nspec>1) ALLOCATE(nspc(Nspecm))


    nden = 1                ! density, 1
    DO i=1,dim              !
       nvel(i) = i+1        ! X, Y mom, 2, 3
    END DO
    n_var_mom = nvel
    neng = dim+2            ! En, 4
    IF (Nspec>1) THEN
       DO i=1,Nspecm
          nspc(i) = dim+2+i    ! species, 5
       END DO
    END IF

    nspc_lil = 1
    nspc_big = 0
    IF (Nspec>1) nspc_lil=nspc(1)
    IF (Nspec>1) nspc_big=nspc(Nspecm)

    n_var_additional = 1

    n_var_pressure  = n_integrated + 1 !pressure
    nprs = n_var_pressure

    IF (adapttmp) THEN
       n_var_additional = n_var_additional + 1
       nton = n_integrated + n_var_additional
    END IF

    IF (adaptvelonly) THEN
       IF (ALLOCATED(nvon)) DEALLOCATE(nvon)
       ALLOCATE (nvon(dim))
       DO i=1,dim
          n_var_additional = n_var_additional + 1
          nvon(i) = n_integrated + n_var_additional
       END DO
    END IF

    IF (adaptspconly .AND. Nspec > 1) THEN
       IF (ALLOCATED(nson)) DEALLOCATE(nson)
       ALLOCATE (nson(Nspecm))
       DO l=1,Nspecm
          n_var_additional = n_var_additional + 1
          nson(l) = n_integrated + n_var_additional
       END DO
    END IF

    IF (adaptbuffRe) THEN
       n_var_additional = n_var_additional + 1
       nbRe = n_integrated + n_var_additional
    END IF

    IF (imask_obstacle .AND. saveUn) THEN
       IF (ALLOCATED(nUn)) DEALLOCATE(nUn)
       ALLOCATE (nUn(dim))
       n_var_additional = n_var_additional + 2
       nUn(1) = n_integrated + n_var_additional - 1
       nUn(2) = n_integrated + n_var_additional
    END IF

    IF (savebuoys) THEN
       n_var_additional = n_var_additional + 2
       ndpx = n_integrated + n_var_additional - 1
       nboy = n_integrated + n_var_additional
    END IF

    IF (savepardom) THEN
       n_var_additional = n_var_additional + 1
       ndom = n_integrated + n_var_additional
    END IF

    !4extra - need all these definitions
    IF( adaptMagVort .or. saveMagVort ) THEN
       n_var_additional = n_var_additional + 1
       nmvt = n_integrated + n_var_additional
    END IF
    IF( (adaptComVort .or. saveComVort) .AND. dim==2 ) THEN
       n_var_additional = n_var_additional + 1
       ncvt(:) = n_integrated + n_var_additional
    END IF
    IF( (adaptComVort .or. saveComVort) .AND. dim==3 ) THEN
       DO i=1,3
          n_var_additional = n_var_additional + 1
          ncvt(i) = n_integrated + n_var_additional
       END DO
    END IF
    IF( adaptMagVel  .OR. saveMagVel  ) THEN
       n_var_additional = n_var_additional + 1
       nmvl = n_integrated + n_var_additional
    END IF
    IF( adaptNormS   .OR. saveNormS   ) THEN
       n_var_additional = n_var_additional + 1
       nnms = n_integrated + n_var_additional
    END IF
    IF (ALLOCATED(ngdy)) DEALLOCATE(ngdy)
    IF (Nspec>1) THEN 
       ALLOCATE(ngdy(Nspecm))
       IF( adaptGradY   .OR. saveGradY   ) THEN
          DO l=1,Nspecm
             n_var_additional = n_var_additional + 1
             ngdy(l) = n_integrated + n_var_additional
          END DO
       END IF
    END IF
    IF (imask_obstacle) THEN !save distance function
       n_var_additional = n_var_additional + 1
       ndist = n_integrated + n_var_additional
    END IF
    IF (imask_obstacle) THEN !save penalty function
       n_var_additional = n_var_additional + 1
       nmsk = n_integrated + n_var_additional
    END IF
    IF ( saveUcvt) THEN 
       n_var_additional = n_var_additional + 1
       ncvtU = n_integrated + n_var_additional
    END IF

    n_var = n_integrated + n_var_additional !--Total number of variables

    n_var_exact = 0

!******************************************************************************************************************
    !
    !--Allocate logical arrays 
    !  Must be done after setting number of different types of variables
    !  and before setting logical variable mapping arrays
    !
    CALL alloc_variable_mappings
    !
    ! Fill in variable names for default variables for integration meth 2
    ! These may be changed in the call to the user specified routine
    !  user_setup_pde()
    !
    ! In integrated variables (3D)
    WRITE (u_variable_names(nden), u_variable_names_fmt) 'Den_rho  '
    WRITE (u_variable_names(nvel(1)), u_variable_names_fmt) 'XMom  '
    WRITE (u_variable_names(nvel(2)), u_variable_names_fmt) 'YMom  '
    IF (dim.eq.3) WRITE (u_variable_names(nvel(3)), u_variable_names_fmt) 'ZMom  '
    WRITE (u_variable_names(neng), u_variable_names_fmt) 'E_Total  '
    IF (Nspec>1) THEN 
       DO l=1,Nspecm      
          WRITE (specnum,'(I8)') l          
          WRITE (u_variable_names(nspc(l)), u_variable_names_fmt) TRIM('Spec_Scalar_'//ADJUSTL(TRIM(specnum)))//'  '
       END DO
    END IF
    WRITE (u_variable_names(nprs), u_variable_names_fmt) 'Pressure  '

    IF (adapttmp) WRITE (u_variable_names(nton), u_variable_names_fmt) 'Temperature  '
    IF (adaptvelonly) THEN
       WRITE (u_variable_names(nvon(1)), u_variable_names_fmt) 'XVelocity  '
       WRITE (u_variable_names(nvon(2)), u_variable_names_fmt) 'YVelocity  '
       IF (dim.eq.3) WRITE (u_variable_names(nvon(3)), u_variable_names_fmt) 'ZVelocity  '
    END IF
    IF (adaptspconly .AND. Nspec > 1) THEN
       DO l=1,Nspecm      
          WRITE (specnum,'(I8)') l          
          WRITE (u_variable_names(nson(l)), u_variable_names_fmt) TRIM('Mass_Frac_'//ADJUSTL(TRIM(specnum)))//'  '
       END DO
    END IF

    IF(adaptbuffRe) WRITE (u_variable_names(nbRe), u_variable_names_fmt) 'ReMask  ' 


    IF (imask_obstacle .AND. saveUn) THEN
       WRITE (u_variable_names(nUn(1)), u_variable_names_fmt) 'Unorm '
       WRITE (u_variable_names(nUn(2)), u_variable_names_fmt) 'Utan  '
    END IF
    IF (savebuoys) THEN
       WRITE (u_variable_names(ndpx), u_variable_names_fmt) 'dPdx  '
       WRITE (u_variable_names(nboy), u_variable_names_fmt) 'Buoy  '
    END IF
    IF (savepardom)  WRITE (u_variable_names(ndom), u_variable_names_fmt) 'MyDomain  '
    
    !4extra - need all these definitions
    IF( adaptMagVort .or. saveMagVort ) WRITE (u_variable_names(nmvt), u_variable_names_fmt) 'MagVort  '
    IF( (adaptComVort .or. saveComVort) .AND. dim==2 )  WRITE (u_variable_names(ncvt(1)), u_variable_names_fmt) 'ComVort  '
    IF( (adaptComVort .or. saveComVort) .AND. dim==3 ) THEN
       DO l=1,3
          WRITE (specnum,'(I8)') l
          WRITE (u_variable_names(ncvt(l)), u_variable_names_fmt) TRIM('ComVort_'//ADJUSTL(TRIM(specnum)))//'  '
       END DO
    END IF
    IF( adaptMagVel  .OR. saveMagVel  )  WRITE (u_variable_names(nmvl), u_variable_names_fmt) 'MagVel  '
    IF( adaptNormS   .OR. saveNormS   )  WRITE (u_variable_names(nnms), u_variable_names_fmt) 'NormS  '
    IF( (adaptGradY   .OR. saveGradY) .AND. Nspec > 1 ) THEN
       DO l=1,Nspecm
          WRITE (specnum,'(I8)') l
          WRITE (u_variable_names(ngdy(l)), u_variable_names_fmt) TRIM('GradY_'//ADJUSTL(TRIM(specnum)))//'  '
       END DO
    END IF

    IF (ndist > 0) WRITE (u_variable_names(ndist), u_variable_names_fmt) 'Dist  '
    IF (imask_obstacle) WRITE (u_variable_names(nmsk), u_variable_names_fmt) 'Mask'
    IF ( saveUcvt) WRITE (u_variable_names(ncvtU), u_variable_names_fmt) 'ConvectZone'
    
    !
    ! setup logical variable mapping arrays
    ! This defined which variables are used for adaptation, Which need to be interpololated to the new grid at 
    ! each time step, which are saved, and for which we have an exect solutiuon we want to check at each time step
    !
    !
    ! setup which components we will base grid adaptation on.
    !
    n_var_adapt = .FALSE. !intialize
    IF (adaptden) n_var_adapt(nden,:)                  = .TRUE.
    IF (adaptvel) n_var_adapt(nvel(1):nvel(dim),:)     = .TRUE.
    IF (adapteng) n_var_adapt(neng,:)                  = .TRUE.
    IF (adaptspc) n_var_adapt(nspc(1):nspc(Nspecm),:) = .TRUE.
    IF (adaptprs) n_var_adapt(nprs,:)                  = .TRUE.

    IF (adapttmp) n_var_adapt(nton,:)                  = .TRUE.
    IF (adaptvelonly) n_var_adapt(nvel(1):nvel(dim),:)     = .FALSE.
    IF (adaptvelonly) n_var_adapt(nvon(1):nvon(dim),:)     = .TRUE.
    IF (adaptspconly .AND. Nspec > 1) n_var_adapt(nspc(1):nspc(Nspecm),:) = .FALSE.
    IF (adaptspconly .AND. Nspec > 1) n_var_adapt(nson(1):nson(Nspecm),:) = .TRUE.

    IF (adaptbuffRe) n_var_adapt(nbRe,:)  = .TRUE.

    !IF (savebuoys) n_var_adapt(ndpx,:)    = .TRUE.
    IF (adaptbuoy) n_var_adapt(nboy,:)    = .TRUE.

    !4extra - adapt
    IF( adaptMagVort ) n_var_adapt(nmvt,:)                  = .TRUE.
    IF( adaptComVort ) n_var_adapt(ncvt(1):ncvt(3),:)       = .TRUE.
    IF( adaptMagVel  ) n_var_adapt(nmvl,:)                  = .TRUE.
    IF( adaptNormS   ) n_var_adapt(nnms,:)                  = .TRUE.
    IF( adaptGradY .AND. Nspec > 1 ) n_var_adapt(ngdy(1):ngdy(Nspecm),:) = .TRUE.

    IF( imask_obstacle ) n_var_adapt(nmsk,0)                  = .TRUE.
    IF( imask_obstacle ) n_var_adapt(nmsk,1)                  = .TRUE.

    !--integrated variables at first time level
    !Variables that need to be interpoleted to new adapted grid in initial grid adaptation
    !Variables that need to be interpoleted to new adapted grid at each time step
    n_var_interpolate        = .FALSE. !intialize
    n_var_interpolate(1:nprs,:) = .TRUE.  

    IF (adapttmp) n_var_interpolate(nton,:)                  = .TRUE.
    IF (adaptvelonly) n_var_interpolate(nvon(1):nvon(dim),:)     = .TRUE.
    IF (adaptspconly .AND. Nspec > 1) n_var_interpolate(nson(1):nson(Nspecm),:) = .TRUE.

    IF (adaptbuffRe) n_var_interpolate(nbRe,:)  = .TRUE.
    IF (savebuoys) n_var_interpolate(ndpx,:)    = .TRUE.
    IF (savebuoys) n_var_interpolate(nboy,:)    = .TRUE.

    !4extra - need these
    IF( saveMagVort ) n_var_interpolate(nmvt,:)                   = .TRUE.
    IF( saveComVort ) n_var_interpolate(ncvt(1):ncvt(3),:)        = .TRUE.
    IF( saveMagVel  ) n_var_interpolate(nmvl,:)                   = .TRUE.
    IF( saveNormS   ) n_var_interpolate(nnms,:)                   = .TRUE.
    IF( saveGradY .AND. Nspec > 1 ) n_var_interpolate(ngdy(1):ngdy(Nspecm),:)  = .TRUE. 
    IF( imask_obstacle .AND. interpolate_dist ) n_var_interpolate(ndist,:)    = .TRUE.
    IF( imask_obstacle    ) n_var_interpolate(nmsk,:)    = .TRUE.

    PRINT *, 'n_var'


    n_var_save = .TRUE.
    n_var_interpolate = .TRUE.

    !
    ! setup which components we have an exact solution for
    !
    n_var_exact_soln = .FALSE. !intialize

    !
    ! setup which variables we will save the solution
    !
    n_var_save = .FALSE. !intialize 
    n_var_save(1:nprs) = .TRUE. ! save all for restarting code
    IF (adapttmp) n_var_save(nton) = .TRUE.
    IF (adaptbuffRe) n_var_save(nbRe)  = .TRUE.
    IF (savebuoys) n_var_save(ndpx)    = .TRUE.
    IF (savebuoys) n_var_save(nboy)    = .TRUE.
    IF (savepardom)  n_var_save(ndom)  = .TRUE.

    !4extra - need these
    IF( saveMagVort ) n_var_save(nmvt)     = .TRUE.
    IF( saveComVort ) n_var_save(ncvt(1):ncvt(3))  = .TRUE.
    IF( saveMagVel  ) n_var_save(nmvl)     = .TRUE.
    IF( saveNormS   ) n_var_save(nnms)     = .TRUE.
    IF( saveGradY .AND. Nspec > 1) n_var_save(ngdy(1):ngdy(Nspecm)) = .TRUE.
    IF( ndist > 0 ) n_var_save(ndist)    = .TRUE.
    IF( imask_obstacle    ) n_var_save(nmsk)    = .TRUE.
    IF (imask_obstacle .AND. saveUn) n_var_save(nUn(1:dim))    = .TRUE.
    IF( saveUcvt    ) n_var_save(ncvtU)    = .TRUE.

    n_var_req_restart = n_var_save
    
    !
    ! Set the maximum number of components for which we have an exact solution
    !
    n_var_exact = MAX( COUNT(n_var_exact_soln(:,0)),COUNT(n_var_exact_soln(:,1)) )

    !
    ! Setup a scaleCoeff array if we want to tweak the scaling of a variable
    ! ( if scaleCoeff < 1.0 then more points will be added to grid 
    !
    ALLOCATE ( scaleCoeff(1:n_var) )
    scaleCoeff = 1.0_pr
    scaleCoeff(nmsk) = 0.1_pr/eps

    IF (doassym) THEN
       !**************** Predefine maximum assymetry allowed ***********************************
       !CALL set_max_assymetry   ! from wlt_vars module
    
       nd_assym_high        = 1 !n_diff+1 !MAX(0,n_diff)!0 !1 !  Nd_assym_high                                     %
       nd_assym_bnd_high    = 2 !n_diff+1 !MAX(1,n_diff)!1 !1 !  Nd_assym_bnd_high                                 %
       nd2_assym_high       = 1 !n_diff+2 !MAX(0,n_diff)   !1 !  Nd2_assym_high                                    %
       nd2_assym_bnd_high   = 2 !n_diff+2 !MAX(2,n_diff)   !1 !  Nd2_assym_bnd_high                                %
       nd_assym_low         = 1 !2*n_diff +3 !0 !  Nd_assym_low                                      %
       nd_assym_bnd_low     = 2 !2*n_diff +3 !1 !  Nd_assym_bnd_low                                  %
       nd2_assym_low        = 1 !2*n_diff +3 !0 !  Nd2_assym_low                                     %
       nd2_assym_bnd_low    = 2 !2*n_diff +3 !2 !  Nd2_assym_bnd_low                                 %
       !**********************************************************************************
    
       nd_assym =     (/nd_assym_low,nd_assym_high,nd_assym_low,nd_assym_high,nd_assym_low,nd_assym_high/)
       nd_assym_bnd = (/nd_assym_bnd_low,nd_assym_bnd_high,nd_assym_bnd_low,nd_assym_bnd_high,nd_assym_bnd_low,nd_assym_bnd_high/) 
       nd2_assym =    (/nd_assym_low,nd2_assym_high,nd_assym_low,nd2_assym_high,nd_assym_low,nd2_assym_high/)
       nd2_assym_bnd =(/nd2_assym_bnd_low,nd2_assym_bnd_high,nd2_assym_bnd_low,nd2_assym_bnd_high,nd2_assym_bnd_low,nd2_assym_bnd_high/) 

       DO i = 0, n_trnsf_type !0 - regular, 1 - inernal
          n_assym_prdct(:,i)     = 0 !  N_assym_prdct for regualr wavelet transform       %
          n_assym_prdct_bnd(:,i) = 1 !  N_assym_prdct_bnd for regualr wavelet transform   %
          n_assym_updt(:,i)      = 0 !  N_assym_updt for regualr wavelet transform        %
          n_assym_updt_bnd(:,i)  = 1 !  N_assym_updt_bnd for regualr wavelet transform    %
       END DO
    END IF    
    
    IF( hypermodel /= 0 ) THEN
          IF(ALLOCATED(n_var_hyper)) DEALLOCATE(n_var_hyper)
          ALLOCATE(n_var_hyper(1:n_var))
          n_var_hyper = .FALSE.
          n_var_hyper(1:neng) = .TRUE.
          n_var_hyper(nden) = .FALSE.
          IF(ALLOCATED(n_var_hyper_active)) DEALLOCATE(n_var_hyper_active)
          ALLOCATE(n_var_hyper_active(1:n_integrated))
          n_var_hyper_active = .FALSE.
          n_var_hyper_active(1:neng) = .TRUE.
    END IF

    IF (par_rank.EQ.0) THEN
       PRINT *, 'n_integrated = ',n_integrated 
       PRINT *, 'n_var = ',n_var 
       PRINT *, 'n_var_exact = ',n_var_exact 
       PRINT *, '*******************Variable Names*******************'
       DO i = 1,n_var
          WRITE (*, u_variable_names_fmt) u_variable_names(i)
       END DO
       PRINT *, '****************************************************'
    END IF
END SUBROUTINE  user_setup_pde


  !
  ! Set the exact solution for comparison to the simulated solution
  !
  ! u          - array to fill in the exact solution
  ! nlocal       - number of active wavelets   
  ! ne_local        - total number of equations
  ! t          - time of current time step 
  ! l_n_var_exact_soln_index - index into the elements of u for which we need to 
  !                            find the exact solution
  SUBROUTINE  user_exact_soln (u, nlocal,  t_local, l_n_var_exact_soln)  
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: nlocal
    REAL (pr), INTENT (IN) ::  t_local
    REAL (pr), DIMENSION (nlocal,n_var_exact), INTENT (INOUT) :: u
    LOGICAL , INTENT (IN) :: l_n_var_exact_soln(n_var)
    INTEGER :: i

  END SUBROUTINE  user_exact_soln

  SUBROUTINE user_initial_conditions (u, nlocal, ne_local, t_local, scl, scl_fltwt,iter)
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: nlocal, ne_local
    REAL (pr), DIMENSION (nlocal,ne_local), INTENT (INOUT) :: u
    REAL (pr), DIMENSION (nlocal) :: rho, p, uvel, XMom, Eng
    REAL (pr)  :: scl(1:n_var),scl_fltwt
    REAL (pr), INTENT (IN) :: t_local
    INTEGER, INTENT (INOUT) :: iter
    INTEGER :: i, l
    REAL (pr) :: alpha, beta, hypdelta

!!$    !establish quiescent fluid
!!$    IF(.NOT.ALLOCATED(uL)) ALLOCATE(uL(dim)); uL = 0.0_pr
!!$    IF(.NOT.ALLOCATED(uR)) ALLOCATE(uR(dim)); uR = 0.0_pr
!!$    rhoL  = 1.0_pr
!!$    uL(1) = 2.0_pr
!!$    pL    = 1.0_pr

    hypdelta = 2*h(j_mx, 1)

    alpha = SQRT(rhoL)*(uL(1) - uR(1)); beta = SQRT(1 + 16.0_pr*pL*gamm(1)/(alpha**2*(1.0_pr + gamm(1))**2))
    pRR = pL - 0.25_pr*alpha**2*(gamm(1) + 1.0_pr)*(beta - 1.0_pr)
    rhoR = rhoL*(4.0_pr*pL*gamm(1) - alpha**2*(1.0_pr + gamm(1))*(beta - 1.0_pr))/(2*(alpha**2*(gamm(1) - 1.0_pr) + 2*pL*gamm(1)))

    EL = rhoL*uL(1)**2/2.0_pr + pL/(gamm(1) - 1.0_pr)

!!$    uR = 0.0_pr
    ER = rhoR*uR(1)**2/2.0_pr + pRR/(gamm(1) - 1.0_pr)

    uvel = uL(1) + 0.5_pr*(uR(1) - uL(1))*(1.0_pr + TANH((x(:, 1) - shock)/hypdelta)) 
    rho = rhoL + 0.5_pr*(rhoR - rhoL)*(1.0_pr + TANH((x(:, 1) - shock)/hypdelta))
    XMom = rhoL*uL(1) + 0.5_pr*(rhoR*uR(1) - rhoL*uL(1))*(1.0_pr + TANH((x(:, 1) - shock)/hypdelta))
    Eng = EL + 0.5_pr*(ER - EL)*(1.0_pr + TANH((x(:, 1) - shock)/hypdelta))
    p = pL + 0.5_pr*(pRR - pL)*(1.0_pr + TANH((x(:, 1) - shock)/hypdelta))

!!$    XMom = rho*uvel
    
    u(:,nden) = rho
    u(:,nvel(1)) = XMom
    u(:,nvel(2)) = 0.0_pr
    u(:, neng) = Eng
    IF (Nspec>1) u(:,nspc(1)) = 0.0_pr
!    PRINT *, u(:, nden)
!    PAUSE
!    IF (ICver == 1) THEN !add 2D gaussian in density
!       u(:,nden)              = 1.0_pr +1.e-1_pr*EXP( -LOG(2.0_pr) * ((x(:,1)-2.0_pr)**2 + x(:,2)**2) / 0.04_pr )
!       u(:,neng)              = u(:,nden)*(cp_in(Nspec)-1.0_pr/MW_in(Nspec))*Tconst
!
!    ELSE IF (ICver == 2) THEN  !Impart flow in x-direction
!       u(:,nvel(1))           = Ma
!       u(:,neng)              = u(:,nden)*Ma**2.0_pr/2.0_pr + u(:,nden)*(cp_in(Nspec)-1.0_pr/MW_in(Nspec))*Tconst
!    END IF

!!$    u(:,nden)              = 1.0_pr
!!$    u(:,nvel(1):nvel(dim)) = 0.0_pr
!!$    u(:,nvel(1))           = Ma
!!$    u(:,neng)              = Ma**2.0_pr/2.0_pr + u(:,nden)*(cp_in(Nspec)-1.0_pr/MW_in(Nspec))*Tconst
!!$    IF (Nspec>1) u(:,nspc(1)) = 0.0_pr

    !for BC's
    e0top = (cp_in(Nspec)-1.0_pr/MW_in(Nspec))*Tconst  !to hold temperature on boundaries, KE = 0
    e0bot  = e0top
  
    IF(ALLOCATED(bndvals)) DEALLOCATE(bndvals)
    ALLOCATE(bndvals(ne_local,dim*2))  !integrated variables,number of boundaries

    !quiescent boundary for Freund
    bndvals(nden,:)               =  1.0_pr
    bndvals(neng,:)               =  u(:,nden)*(cp_in(Nspec)-1.0_pr/MW_in(Nspec))*Tconst
    bndvals(nvel(1):nvel(dim),:)  = 0.0_pr
    
    !Steady flow for freund
    IF (ICver == 2) THEN
       bndvals(nvel(1),:)    = Ma
       bndvals(neng,:)              = u(:,nden)*Ma**2.0_pr/2.0_pr + u(:,nden)*(cp_in(Nspec)-1.0_pr/MW_in(Nspec))*Tconst

    END IF
    
    IF (imask_obstacle .AND. (interpolate_dist .OR. ndist > 0)) THEN
       CALL user_dist (nwlt, t_local, DISTANCE=u(:,ndist) )
    END IF

  END SUBROUTINE user_initial_conditions

  SUBROUTINE dpdx (pderiv,nlocal,Yloc,meth,myjl,split)
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: nlocal
    REAL (pr), DIMENSION (nlocal), INTENT(INOUT) :: pderiv
    REAL (pr), DIMENSION (nlocal,MAX(Nspecm,1)), INTENT(IN) :: Yloc
    INTEGER, INTENT(IN) :: meth, myjl
    LOGICAL, INTENT(IN) :: split
    INTEGER :: i
    REAL (pr), DIMENSION (nlocal,dim) :: du
    REAL (pr), DIMENSION (nlocal,dim) :: d2u



    IF (split) THEN 
       CALL c_diff_fast(pderiv(:), du(:,:), d2u(:,:), myjl, nlocal, MOD(meth,2)+BIASING_BACKWARD, 10, 1, 1, 1)
       d2u(:,1) = du(:,1)
       CALL c_diff_fast(pderiv(:), du(:,:), d2u(:,:), myjl, nlocal, MOD(meth,2)+BIASING_FORWARD , 10, 1, 1, 1)
       IF (stepsplit) THEN
          d2u(:,2) = 1.0_pr
          d2u(:,2) = (SIGN(d2u(:,2), Yloc(:,1) - Y_Xhalf) + 1.0_pr)/2.0_pr
       ELSE
          d2u(:,2) = (1.0_pr - SUM(Yloc(:,1:Nspecm),DIM=2))/MW_in(Nspec)  !Y_Nspec/W_Nspec
          DO i=1,Nspecm
             d2u(:,2) = d2u(:,2) + Yloc(:,i)/MW_in(i)  !Y_l/W_l
          END DO
          d2u(:,2) = Yloc(:,1)/MW_in(1)/d2u(:,2)  !X_1
          d2u(:,2) = (d2u(:,2)-peakchange)/(1.0_pr-2.0_pr*peakchange)  !X undoing peak change
       END IF
       d2u(:,2) = MIN(1.0_pr,MAX(0.0_pr,d2u(:,2)))
       pderiv(:) = d2u(:,2)*d2u(:,1) + (1.0_pr - d2u(:,2))*du(:,1)
    ELSE
       CALL c_diff_fast(pderiv(:), du(:,:), d2u(:,:), myjl, nlocal, meth, 10, 1, 1, 1)
       pderiv(:) = du(:,1)
    END IF
  END SUBROUTINE dpdx

  SUBROUTINE dpdx_diag (pd_diag,nlocal,Yloc,meth,myjl,split)
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: nlocal
    REAL (pr), DIMENSION (nlocal), INTENT(INOUT) :: pd_diag
    REAL (pr), DIMENSION (nlocal,MAX(Nspecm,1)), INTENT(IN) :: Yloc
    INTEGER, INTENT(IN) :: meth, myjl
    LOGICAL, INTENT(IN) :: split
    INTEGER :: i
    REAL (pr), DIMENSION (nlocal,dim) :: du_diag, d2u_diag

    IF (split) THEN 
       CALL c_diff_diag(du_diag, d2u_diag, myjl, nlocal, MOD(meth,2)+BIASING_BACKWARD, MOD(meth,2)+BIASING_FORWARD, 10) 
       pd_diag(:) = du_diag(:,1)
       CALL c_diff_diag(du_diag, d2u_diag, myjl, nlocal, MOD(meth,2)+BIASING_FORWARD, MOD(meth,2)+BIASING_BACKWARD, 10)   
       IF (stepsplit) THEN
          d2u_diag(:,2) = 1.0_pr
          d2u_diag(:,2) = (SIGN(d2u_diag(:,2), Yloc(:,1) - Y_Xhalf) + 1.0_pr)/2.0_pr
       ELSE
          d2u_diag(:,2) = (1.0_pr - SUM(Yloc(:,1:Nspecm),DIM=2))/MW_in(Nspec)
          DO i=1,Nspecm
             d2u_diag(:,2) = d2u_diag(:,2) + Yloc(:,i)/MW_in(i)
          END DO
          d2u_diag(:,2)=Yloc(:,1)/MW_in(1)/d2u_diag(:,2)  !X
          d2u_diag(:,2)=(d2u_diag(:,2)-peakchange)/(1.0_pr-2.0_pr*peakchange)  !X undoing peak change
       END IF
       d2u_diag(:,2) = MIN(1.0_pr,MAX(0.0_pr,d2u_diag(:,2)))
       pd_diag(:) = d2u_diag(:,2)*pd_diag(:) + (1.0_pr - d2u_diag(:,2))*du_diag(:,1)
    ELSE
       CALL c_diff_diag(du_diag, d2u_diag, j_lev, ng, 2*methpd+meth, 2*methpd+meth, 10)   !dp/dx done using methpd 
       pd_diag(:) = du_diag(:,1)
    END IF
  END SUBROUTINE dpdx_diag


  !--********************************  
  ! Arguments
  ! u         - field on adaptive grid
  ! nlocal      - number of active points
  ! ne_local       - number of equations
  ! t         - current time
  !
  ! Global variables used
  ! ifn       - number of equations
  ! x()
  ! xO
  ! y0
  !--******************************** 
  SUBROUTINE user_algebraic_BC (Lu, u, nlocal, ne_local, jlev, meth)
    USE penalization
    !--Defines boundary condition type
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: jlev, meth, ne_local, nlocal
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (INOUT) :: Lu
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (IN)    :: u

    INTEGER :: ie, i, ii, shift, denshift,eshift
    INTEGER, DIMENSION (dim) :: velshift 
    REAL (pr), DIMENSION (ne_local,nlocal,dim) :: du, d2u !uncomment if Neuman BC are used, need to call 
    REAL (pr), DIMENSION (1,nlocal,dim) :: du_prev
    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc
    
    !shift markers for BC implementation
!    denshift  = nlocal*(nden-1)
!    DO i = 1,dim
!       velshift(i) = nlocal*(nvel(i)-1)
!    END DO
!    eshift    = nlocal*(neng-1)




    !BC for both 2D and 3D cases

    !if Neuman BC are used, need to call 
    CALL c_diff_fast (u, du, d2u, jlev, nlocal, meth, 10, ne_local, 1, ne_local)
    DO ie = 1, ne_local
       shift = nlocal*(ie-1)
       !--Go through all Boundary points that are specified
       i_p_face(0) = 1
       DO i=1,dim
          i_p_face(i) = i_p_face(i-1)*3
       END DO
       DO face_type = 0, 3**dim - 1
          face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
!          IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
          IF(ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
             CALL get_all_indices_by_face (face_type, jlev, nloc, iloc)
             IF(nloc > 0 ) THEN 
                IF ( face(1) == -1 ) THEN  !X min and X max
!!$                   IF (nvel(1) == ie) THEN
!!$                      IF (imask_obstacle) THEN
!!$                         WHERE (penal(iloc(1:nloc)) ==  0.0_pr)   Lu(shift+iloc(1:nloc)) = u(shift+iloc(1:nloc))       !Dirichlet conditions
!!$                      ELSE 
!!$                   IF(ie == nvel(1)) THEN
                   Lu(shift+iloc(1:nloc)) = u(shift+iloc(1:nloc))       !Dirichlet conditions
!!$                   END IF
!!$                      END IF
!!$                   END IF
!!$                ELSE IF (face(1) == 1) THEN
!!$                   IF (nvel(1) == ie) THEN
!!$                      Lu(shift+iloc(1:nloc)) = u(shift+iloc(1:nloc))       !Dirichlet conditions
!!$                   END IF
                ELSE IF( ABS(face(2)) == 1 ) THEN  !Y min and Y max
                   IF (nvel(2) == ie) THEN
                      IF (imask_obstacle) THEN
                         Lu(shift+iloc(1:nloc)) = penal(iloc(1:nloc))*Lu(shift+iloc(1:nloc))+(1.0_pr-penal(iloc(1:nloc)))*u(shift+iloc(1:nloc))  ! Dirichlet
                      ELSE
                         Lu(shift+iloc(1:nloc)) = u(shift+iloc(1:nloc))        ! Dirichlet
                      END IF
                   END IF
                END IF
              END IF
          END IF
       END DO
    END DO
  END SUBROUTINE user_algebraic_BC

  SUBROUTINE user_algebraic_BC_diag (Lu_diag, nlocal, ne_local, jlev, meth)
    !--Defines boundary condition type
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: jlev, meth, ne_local, nlocal
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (INOUT) :: Lu_diag

    INTEGER :: ie, i, ii, shift, denshift

    REAL (pr), DIMENSION (nlocal,dim) :: du_diag, d2u_diag  !uncomment if Neuman BC are used
    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc
    REAL (pr), DIMENSION (1,nlocal,dim) :: du_prev, d2u
    INTEGER, PARAMETER :: methprev=1

    !BC for both 2D and 3D cases

    denshift = nlocal*(nden-1)



    !if Neuman BC are used, need to call 
!!$    CALL c_diff_diag ( du_diag, d2u_diag, jlev, nlocal, meth, meth, 10)

    DO ie = 1, ne_local
       shift = nlocal*(ie-1)
       !--Go through all Boundary points that are specified
       i_p_face(0) = 1
       DO i=1,dim
          i_p_face(i) = i_p_face(i-1)*3
       END DO
       DO face_type = 0, 3**dim - 1
          face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
          IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
             CALL get_all_indices_by_face (face_type, jlev, nloc, iloc)
             IF(nloc > 0 ) THEN 
                IF ( face(1) == -1 ) THEN  !X min and X max
!!$                   IF (nvel(1) == ie) THEN
!!$                      IF (imask_obstacle) THEN
!!$                         WHERE (penal(iloc(1:nloc)) ==  0.0_pr)   Lu(shift+iloc(1:nloc)) = u(shift+iloc(1:nloc))       !Dirichlet conditions
!!$                      ELSE 
                   Lu_diag(shift+iloc(1:nloc)) = 1.0_pr       !Dirichlet conditions
!!$                      END IF
!!$                      END IF
!!$                ELSE IF (face(1) == 1) THEN
!!$                   IF (nvel(1) == ie) THEN
!!$                      Lu_diag(shift+iloc(1:nloc)) = 1.0_pr       !Dirichlet conditions
!!$                   END IF
                ELSE IF( ABS(face(2)) == 1 ) THEN  !Y min and Y max
                   IF (nvel(2) == ie) THEN
                      IF (imask_obstacle) THEN
                         Lu_diag(shift+iloc(1:nloc)) = penal(iloc(1:nloc))*Lu_diag(shift+iloc(1:nloc))+(1.0_pr-penal(iloc(1:nloc)))  ! Dirichlet
                      ELSE
                         Lu_diag(shift+iloc(1:nloc)) = 1.0_pr       ! Dirichlet
                      END IF
                   END IF
                END IF
             END IF
          END IF
       END DO
    END DO
  END SUBROUTINE user_algebraic_BC_diag

  SUBROUTINE user_algebraic_BC_rhs (rhs, ne_local, nlocal, jlev)
    !--Sets rhs for boundary conditions
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: ne_local, nlocal, jlev
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (INOUT) :: rhs

    INTEGER :: ie, i, ii, shift, denshift
    INTEGER, PARAMETER ::  meth=1
    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc
    REAL (pr), DIMENSION (ne_local,nlocal,dim) :: duall, d2uall
    REAL (pr), DIMENSION (1,nlocal,dim) :: du_prev, du, d2u  
    REAL (pr) :: p_bc

    p_bc = 1.0_pr  !pressure defined for outflow boundary conditions

    denshift=nlocal*(nden-1)

    DO ie = 1, ne_local
       shift = nlocal*(ie-1)
       !--Go through all Boundary points that are specified
       i_p_face(0) = 1
       DO i=1,dim
          i_p_face(i) = i_p_face(i-1)*3
       END DO
       DO face_type = 0, 3**dim - 1
          face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
          IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
             CALL get_all_indices_by_face (face_type, jlev, nloc, iloc)
             IF(nloc > 0 ) THEN 
                IF ( face(1) == -1 ) THEN  !X min and X max
!!$                   IF (nvel(1) == ie) THEN
!!$                      IF (imask_obstacle) THEN
!!$                         WHERE (penal(iloc(1:nloc)) ==  0.0_pr)   Lu(shift+iloc(1:nloc)) = u(shift+iloc(1:nloc))       !Dirichlet conditions
!!$                      ELSE 
                   IF (ie == nden) THEN
                      rhs(shift+iloc(1:nloc)) = rhoL       !Dirichlet conditions
                   ELSE IF (ie == nvel(1)) THEN
                      rhs(shift+iloc(1:nloc)) = rhoL*uL(1)       !Dirichlet condition
!!$                      rhs(shift+iloc(1:nloc)) = 0.0_pr       !Dirichlet conditions
                   ELSE IF (ie == nvel(2)) THEN
                      rhs(shift+iloc(1:nloc)) = rhoL*uL(2)      !Dirichlet conditions
                   ELSE IF (ie == neng) THEN
                      rhs(shift+iloc(1:nloc)) = EL       !Dirichlet conditions
                   END IF
!!$                ELSE IF (face(1) == 1) THEN
!!$                   IF (nvel(1) == ie) THEN
!!$                      rhs(shift+iloc(1:nloc)) = 0.0_pr       !Dirichlet conditions
!!$                   END IF
                ELSE IF( ABS(face(2)) == 1 ) THEN  !Y min and Y max
                   IF (nvel(2) == ie) THEN
                      IF (imask_obstacle) THEN
                         rhs(shift+iloc(1:nloc)) = penal(iloc(1:nloc))*rhs(shift+iloc(1:nloc))
                      ELSE
                         rhs(shift+iloc(1:nloc)) = 0.0_pr       ! Dirichlet
                      END IF
                   END IF
                END IF
             END IF
          END IF
       END DO
    END DO
  END SUBROUTINE user_algebraic_BC_rhs

  SUBROUTINE user_evol_LODIslip_BC_rhs (myrhs_loc, ulc, myne_loc, mynloc, myjl, mymeth)
    !--Sets rhs for boundary conditions
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: myne_loc, mynloc, myjl, mymeth
    REAL (pr), DIMENSION (mynloc*myne_loc), INTENT (INOUT) :: myrhs_loc
    REAL (pr), DIMENSION (mynloc,myne_loc), INTENT (IN) :: ulc

    INTEGER :: ie, i, ii, shift, jj
    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc

    REAL (pr), DIMENSION (:),     ALLOCATABLE :: temprhs_loc
    REAL (pr), DIMENSION (:,:),   ALLOCATABLE :: pres
    REAL (pr), DIMENSION (:,:,:), ALLOCATABLE :: du, d2u
    REAL (pr), DIMENSION (:,:),   ALLOCATABLE :: charX
    REAL (pr) :: faceval
    INTEGER :: l, faceint

    IF (ALLOCATED(temprhs_loc)) DEALLOCATE(temprhs_loc)
    ALLOCATE(temprhs_loc(myne_loc*mynloc))
    CALL internal_rhs(temprhs_loc,ulc,1,0)
    i_p_face(0) = 1
    DO i=1,dim
       i_p_face(i) = i_p_face(i-1)*3
    END DO
    DO face_type = 0, 3**dim - 1
       face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
       IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
          CALL get_all_indices_by_face (face_type, myjl, nloc, iloc)
          IF(nloc > 0 ) THEN
             IF (ABS(face(1)) == 1) THEN
                DO jj=1,myne_loc
                   myrhs_loc((jj-1)*mynloc+iloc(1:nloc)) = temprhs_loc((jj-1)*mynloc+iloc(1:nloc))  !from internal_rhs
                END DO
             END IF
          END IF
       END IF
    END DO
    IF (ALLOCATED(temprhs_loc)) DEALLOCATE(temprhs_loc) 

    IF (ALLOCATED(pres)) DEALLOCATE(pres) 
    IF (ALLOCATED(du)) DEALLOCATE(du) 
    IF (ALLOCATED(d2u)) DEALLOCATE(d2u) 
    ALLOCATE(pres(mynloc,Nspec+1),du(2,mynloc,dim),d2u(2,mynloc,dim))

    d2u(1,:,1) = 1.0_pr
    IF (Nspec>1) d2u(1,:,1) = d2u(1,:,1)-SUM(ulc(:,nspc(1):nspc(Nspecm)),DIM=2)/ulc(:,nden) !Y_Nspec
    d2u(2,:,1) = d2u(1,:,1)*cp_in(Nspec)  !cp_Nspec
    d2u(2,:,2) = d2u(1,:,1)/MW_in(Nspec)  !R_Nspec
    DO l=1,Nspecm
       d2u(2,:,1)  = d2u(2,:,1)  + ulc(:,nspc(l))/ulc(:,nden)*cp_in(l)  !cp
       d2u(2,:,2)  = d2u(2,:,2)  + ulc(:,nspc(l))/ulc(:,nden)/MW_in(l)  !R
    END DO
    d2u(1,:,1) = d2u(2,:,1)/(d2u(2,:,1)-d2u(2,:,2))  !gamma=cp/(cp-R)
    pres(:,1) = (ulc(:,neng)-0.5_pr*SUM(ulc(:,nvel(1):nvel(dim))**2,DIM=2)/ulc(:,nden))*(d2u(1,:,1)-1.0_pr)  !1: pressure
    d2u(1,:,2) = SQRT(d2u(1,:,1)*pres(:,1)/ulc(:,nden))  !c=SQRT(gamma*P/rho)
    pres(:,2) = ulc(:,nvel(1))/ulc(:,nden) !2: u

    IF ((methpd .ne. 0) .OR. splitFBpress) THEN 
       DO l=1,Nspecm
          pres(:,2+l) = ulc(:,nspc(l))/ulc(:,nden)
       END DO
       du(1,:,1) = pres(:,1)
       IF (Nspec>1) THEN
          CALL dpdx (du(1,:,1),ng,pres(:,3:Nspec+1),2*methpd+mymeth,myjl,splitFBpress)
       ELSE
          CALL dpdx (du(1,:,1),ng,pres(:,1),2*methpd+mymeth,myjl,.FALSE.)
       END IF
       CALL c_diff_fast(pres(:,2), du(2,:,:), d2u(2,:,:), myjl, mynloc, mymeth, 10, 1, 1, 1)  
    ELSE
       CALL c_diff_fast(pres(:,1:2), du(1:2,:,:), d2u(1:2,:,:), myjl, mynloc, mymeth, 10, 2, 1, 2) 
    END IF

    i_p_face(0) = 1
    DO i=1,dim
       i_p_face(i) = i_p_face(i-1)*3
    END DO
    DO face_type = 0, 3**dim - 1
       face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
       IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
          CALL get_all_indices_by_face (face_type, myjl, nloc, iloc)
          IF(nloc > 0 ) THEN
             IF (ABS(face(1)) == 1) THEN
                IF (ALLOCATED(charX)) DEALLOCATE(charX)
                ALLOCATE(charX(nloc,2))

                faceval=REAL(face(1),pr)
                faceint=(3-face(1))/2

                IF (LODIslipeasy) faceval=0.0_pr

                charX(1:nloc,1) = ulc(iloc(1:nloc),nden)*du(2,iloc(1:nloc),1)+faceval/d2u(1,iloc(1:nloc),2)*(du(1,iloc(1:nloc),1)-dP0(faceint))  
                charX(1:nloc,2) = charX(1:nloc,1)*d2u(1,iloc(1:nloc),2)**2

                myrhs_loc((nden-1)*mynloc+iloc(1:nloc)) = myrhs_loc((nden-1)*mynloc+iloc(1:nloc)) - charX(1:nloc,1)
                DO ii=nvel(1),nvel(dim)
                   myrhs_loc((ii-1)*mynloc+iloc(1:nloc)) = myrhs_loc((ii-1)*mynloc+iloc(1:nloc)) - ulc(iloc(1:nloc),ii)/ulc(iloc(1:nloc),nden)*charX(1:nloc,1)
                END DO
                myrhs_loc((neng-1)*mynloc+iloc(1:nloc)) = myrhs_loc((neng-1)*mynloc+iloc(1:nloc)) - &
                                                          0.5_pr*SUM(ulc(iloc(1:nloc),nvel(1):nvel(dim))**2,DIM=2)/ulc(iloc(1:nloc),nden)**2*charX(1:nloc,1) - &
                                                          1.0_pr/(d2u(1,iloc(1:nloc),1)-1.0_pr)*charX(1:nloc,2) 
                DO ii=1,Nspecm
                   myrhs_loc((nspc(ii)-1)*mynloc+iloc(1:nloc)) = myrhs_loc((nspc(ii)-1)*mynloc+iloc(1:nloc)) - ulc(iloc(1:nloc),nspc(ii))/ulc(iloc(1:nloc),nden)*charX(1:nloc,1)
                END DO

                IF (ALLOCATED(charX)) DEALLOCATE(charX) 
             END IF
          END IF
       END IF
    END DO
    IF (ALLOCATED(pres)) DEALLOCATE(pres) 
    IF (ALLOCATED(du)) DEALLOCATE(du) 
    IF (ALLOCATED(d2u)) DEALLOCATE(d2u) 
  END SUBROUTINE user_evol_LODIslip_BC_rhs 

  SUBROUTINE user_evol_LODIslip_BC_Drhs (Dmyrhs_loc, ulc, ulc_prev, myne_loc, mynloc, myjl, mymeth)
    !--Sets rhs for boundary conditions
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: myne_loc, mynloc, myjl, mymeth
    REAL (pr), DIMENSION (mynloc*myne_loc), INTENT (INOUT) :: Dmyrhs_loc
    REAL (pr), DIMENSION (mynloc,myne_loc), INTENT (IN) :: ulc, ulc_prev

    INTEGER :: ie, i, ii, shift, jj
    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc

    REAL (pr), DIMENSION (:),     ALLOCATABLE :: tempDrhs_loc
    REAL (pr), DIMENSION (:,:),   ALLOCATABLE :: pres !p0, p',u0, u'
    REAL (pr), DIMENSION (:,:,:), ALLOCATABLE :: du, d2u
    REAL (pr), DIMENSION (:,:),   ALLOCATABLE :: charX, charX_prev
    REAL (pr) :: faceval
    INTEGER :: l, faceint

    IF (ALLOCATED(tempDrhs_loc)) DEALLOCATE(tempDrhs_loc)
    ALLOCATE(tempDrhs_loc(myne_loc*mynloc))
    CALL internal_Drhs(tempDrhs_loc,ulc,ulc_prev,mymeth,1,0)
    i_p_face(0) = 1
    DO i=1,dim
       i_p_face(i) = i_p_face(i-1)*3
    END DO
    DO face_type = 0, 3**dim - 1
       face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
       IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
          CALL get_all_indices_by_face (face_type, myjl, nloc, iloc)
          IF(nloc > 0 ) THEN
             IF (ABS(face(1)) == 1) THEN
                DO jj=1,myne_loc
                   Dmyrhs_loc((jj-1)*mynloc+iloc(1:nloc)) = tempDrhs_loc((jj-1)*mynloc+iloc(1:nloc))  !from internal_Drhs
                END DO
             END IF
          END IF
       END IF
    END DO
    IF (ALLOCATED(tempDrhs_loc)) DEALLOCATE(tempDrhs_loc) 

    IF (ALLOCATED(pres)) DEALLOCATE(pres) 
    IF (ALLOCATED(du)) DEALLOCATE(du) 
    IF (ALLOCATED(d2u)) DEALLOCATE(d2u) 
    ALLOCATE(pres(mynloc,Nspec+3), du(4,mynloc,dim),d2u(4,mynloc,dim))

    d2u(1,:,1) = 1.0_pr
    d2u(1,:,2) = 0.0_pr
    IF (Nspec>1) d2u(1,:,1) = d2u(1,:,1)-SUM(ulc_prev(:,nspc(1):nspc(Nspecm)),DIM=2)/ulc_prev(:,nden) !Y0_Nspec
    IF (Nspec>1) d2u(1,:,2) = (SUM(ulc_prev(:,nspc(1):nspc(Nspecm)),DIM=2)*ulc(:,nden)/ulc_prev(:,nden) - SUM(ulc(:,nspc(1):nspc(Nspecm)),DIM=2))/ulc_prev(:,nden) !Y'_Nspec 
    d2u(4,:,1) = d2u(1,:,2)*cp_in(Nspec)  !cp'_Nspec
    d2u(4,:,2) = d2u(1,:,2)/MW_in(Nspec)  !R'_Nspec
    d2u(2,:,1) = d2u(1,:,1)*cp_in(Nspec)  !cp0_Nspec
    d2u(2,:,2) = d2u(1,:,1)/MW_in(Nspec)  !R0_Nspec
    DO l=1,Nspecm
       d2u(2,:,1)  = d2u(2,:,1)  + ulc_prev(:,nspc(l))/ulc_prev(:,nden)*cp_in(l)  !cp0
       d2u(2,:,2)  = d2u(2,:,2)  + ulc_prev(:,nspc(l))/ulc_prev(:,nden)/MW_in(l)  !R0
       d2u(4,:,1)  = d2u(4,:,1)  + (ulc(:,nspc(l))-ulc_prev(:,nspc(l))/ulc_prev(:,nden)*ulc(:,nden))/ulc_prev(:,nden)*cp_in(l)  !cp'
       d2u(4,:,2)  = d2u(4,:,2)  + (ulc(:,nspc(l))-ulc_prev(:,nspc(l))/ulc_prev(:,nden)*ulc(:,nden))/ulc_prev(:,nden)/MW_in(l)  !R'
    END DO
    d2u(1,:,1) = d2u(2,:,1)/(d2u(2,:,1)-d2u(2,:,2))  !gamma0=cp0/(cp0-R0)
    pres(:,1) = (ulc_prev(:,neng)-0.5_pr*SUM(ulc_prev(:,nvel(1):nvel(dim))**2,DIM=2)/ulc_prev(:,nden))*(d2u(1,:,1)-1.0_pr)  !1: p0
    d2u(1,:,2) = SQRT(d2u(1,:,1)*pres(:,1)/ulc_prev(:,nden))  !c0=SQRT(gamma0*P0/rho0) 
    d2u(3,:,1) = (d2u(2,:,1)*d2u(4,:,2) - d2u(4,:,1)*d2u(2,:,2)) / (d2u(2,:,1)-d2u(2,:,2))**2  !gamma'=(cp0*R'-cp'*R0)/(cp0-R0)^2
    pres(:,2) =  d2u(3,:,1)*(ulc_prev(:,neng)-0.5_pr*SUM(ulc_prev(:,nvel(1):nvel(dim))**2,DIM=2))/ulc_prev(:,nden) +  &
                (d2u(1,:,1)-1.0_pr)*(ulc(:,neng)+0.5_pr*SUM(ulc_prev(:,nvel(1):nvel(dim))**2,DIM=2)/ulc_prev(:,nden)**2*ulc(:,nden) -  &
                 SUM(ulc_prev(:,nvel(1):nvel(dim))*ulc(:,nvel(1):nvel(dim)),DIM=2)/ulc_prev(:,nden) )  !2: p'
    d2u(3,:,2) = 0.5_pr/SQRT(d2u(1,:,1)*pres(:,1)/ulc_prev(:,nden)) * ( d2u(1,:,1)*pres(:,2)+d2u(3,:,1)*pres(:,1) - &
                        d2u(1,:,1)*pres(:,1)/ulc_prev(:,nden)*ulc(:,nden) )/ulc_prev(:,nden)   !c'=1/2/SQRT(gamma0*P0/rho0)*(gamma*P/rho)'
    pres(:,3) = ulc_prev(:,nvel(1))/ulc_prev(:,nden)  !3: u0
    pres(:,4) = (ulc(:,nvel(1)) - ulc_prev(:,nvel(1))*ulc(:,nden)/ulc_prev(:,nden) )/ulc_prev(:,nden) !4: u'
 
    IF ((methpd .ne. 0) .OR. splitFBpress) THEN 
       DO l=1,Nspecm
          pres(:,4+l) = ulc(:,nspc(l))/ulc(:,nden)
       END DO
       du(1,:,1) = pres(:,1)
       IF (Nspec>1) THEN
          CALL dpdx (du(1,:,1),ng,pres(:,5:Nspec+3),2*methpd+mymeth,myjl,splitFBpress)
       ELSE
          CALL dpdx (du(1,:,1),ng,pres(:,1),2*methpd+mymeth,myjl,.FALSE.)
       END IF
       du(2,:,1) = pres(:,2)
       IF (Nspec>1) THEN
          CALL dpdx (du(2,:,1),ng,pres(:,5:Nspec+3),2*methpd+mymeth,myjl,splitFBpress)
       ELSE
          CALL dpdx (du(2,:,1),ng,pres(:,1),2*methpd+mymeth,myjl,.FALSE.)
       END IF
       CALL c_diff_fast(pres(:,3:4), du(3:4,:,:), d2u(3:4,:,:), myjl, mynloc, mymeth, 10, 2, 1, 2)  
    ELSE
       CALL c_diff_fast(pres(:,1:4), du(1:4,:,:), d2u(1:4,:,:), myjl, mynloc, mymeth, 10, 4, 1, 4) 
    END IF

    i_p_face(0) = 1
    DO i=1,dim
       i_p_face(i) = i_p_face(i-1)*3
    END DO
    DO face_type = 0, 3**dim - 1
       face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
       IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
          CALL get_all_indices_by_face (face_type, myjl, nloc, iloc)
          IF(nloc > 0 ) THEN
             IF (ABS(face(1)) == 1) THEN
                IF (ALLOCATED(charX)) DEALLOCATE(charX)
                IF (ALLOCATED(charX_prev)) DEALLOCATE(charX_prev)
                ALLOCATE(charX(nloc,2),charX_prev(nloc,2))

                faceval=REAL(face(1),pr)
                faceint=(3-face(1))/2

                IF (LODIslipeasy) faceval=0.0_pr

                charX_prev(1:nloc,1) = ulc_prev(iloc(1:nloc),nden)*du(3,iloc(1:nloc),1)+faceval/d2u(1,iloc(1:nloc),2)*(du(1,iloc(1:nloc),1)-dP0(faceint))  
                charX_prev(1:nloc,2) = charX_prev(1:nloc,1)*d2u(1,iloc(1:nloc),2)**2

                charX(1:nloc,1) = ulc_prev(iloc(1:nloc),nden)*du(2,iloc(1:nloc),1) + ulc(iloc(1:nloc),nden)*du(1,iloc(1:nloc),1) + &
                                  faceval/d2u(1,iloc(1:nloc),2)*du(2,iloc(1:nloc),1) - faceval*d2u(3,iloc(1:nloc),2)*(du(1,iloc(1:nloc),1)-dP0(faceint))/d2u(1,iloc(1:nloc),2)**2
                charX(1:nloc,2) = charX(1:nloc,1)*d2u(1,iloc(1:nloc),2)**2 + charX_prev(1:nloc,1)*2.0_pr*d2u(1,iloc(1:nloc),2)*d2u(3,iloc(1:nloc),2)

                Dmyrhs_loc((nden-1)*mynloc+iloc(1:nloc)) = Dmyrhs_loc((nden-1)*mynloc+iloc(1:nloc)) - charX(1:nloc,1)
                DO ii=nvel(1),nvel(dim)
                   Dmyrhs_loc((ii-1)*mynloc+iloc(1:nloc)) = Dmyrhs_loc((ii-1)*mynloc+iloc(1:nloc)) - ulc_prev(iloc(1:nloc),ii)/ulc_prev(iloc(1:nloc),nden)*charX(1:nloc,1) &
                     - (ulc(iloc(1:nloc),ii)-ulc_prev(iloc(1:nloc),ii)*ulc(iloc(1:nloc),nden)/ulc_prev(iloc(1:nloc),nden) )/ulc_prev(iloc(1:nloc),nden)*charX_prev(1:nloc,1)
                   Dmyrhs_loc((neng-1)*mynloc+iloc(1:nloc)) = Dmyrhs_loc((neng-1)*mynloc+iloc(1:nloc)) &
                      - 0.5_pr*ulc_prev(iloc(1:nloc),ii)**2/ulc_prev(iloc(1:nloc),nden)**2*charX(1:nloc,1) - ulc_prev(iloc(1:nloc),ii)/ulc_prev(iloc(1:nloc),nden)**2 * &
                                (ulc(iloc(1:nloc),ii)-ulc_prev(iloc(1:nloc),ii)*ulc(iloc(1:nloc),nden)/ulc_prev(iloc(1:nloc),nden))*charX_prev(1:nloc,1) 
                END DO
                Dmyrhs_loc((neng-1)*mynloc+iloc(1:nloc)) = Dmyrhs_loc((neng-1)*mynloc+iloc(1:nloc)) - 1.0_pr/(d2u(1,iloc(1:nloc),1)-1.0_pr) * &
                              (charX(1:nloc,2) - d2u(3,iloc(1:nloc),1)/(d2u(1,iloc(1:nloc),1)-1.0_pr)*charX_prev(1:nloc,2)) 
                DO ii=1,Nspecm
                   Dmyrhs_loc((nspc(ii)-1)*mynloc+iloc(1:nloc)) = Dmyrhs_loc((nspc(ii)-1)*mynloc+iloc(1:nloc)) - ulc_prev(iloc(1:nloc),nspc(ii))/ulc_prev(iloc(1:nloc),nden)*charX(1:nloc,1) &
                     - (ulc(iloc(1:nloc),nspc(ii))-ulc_prev(iloc(1:nloc),nspc(ii))*ulc(iloc(1:nloc),nden)/ulc_prev(iloc(1:nloc),nden) )/ulc_prev(iloc(1:nloc),nden)*charX_prev(1:nloc,1)
                END DO

                IF (ALLOCATED(charX)) DEALLOCATE(charX)
                IF (ALLOCATED(charX_prev)) DEALLOCATE(charX_prev)
             END IF
          END IF
       END IF
    END DO

    IF (ALLOCATED(pres)) DEALLOCATE(pres) 
    IF (ALLOCATED(du)) DEALLOCATE(du) 
    IF (ALLOCATED(d2u)) DEALLOCATE(d2u) 
  END SUBROUTINE user_evol_LODIslip_BC_Drhs 

  SUBROUTINE user_evol_LODIslip_BC_Drhs_diag (Drhs_diag_local, ulc_prev, myne_loc, mynloc, myjl, mymeth)
    !--Sets rhs for boundary conditions
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: myne_loc, mynloc, myjl, mymeth
    REAL (pr), DIMENSION (mynloc*myne_loc), INTENT (INOUT) :: Drhs_diag_local
    REAL (pr), DIMENSION (mynloc,myne_loc), INTENT (IN) :: ulc_prev

    INTEGER :: ie, i, ii, shift, jj
    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc

    REAL (pr), DIMENSION (:),     ALLOCATABLE :: tempdiag_loc
    REAL (pr), DIMENSION (mynloc,dim) :: du_diag_local, d2u_diag_local
    REAL (pr), DIMENSION (mynloc):: pres_prev
    REAL (pr), DIMENSION (mynloc,dim) :: du, d2u
    INTEGER :: l

    IF (ALLOCATED(tempdiag_loc)) DEALLOCATE(tempdiag_loc)
    ALLOCATE(tempdiag_loc(myne_loc*mynloc))
    CALL internal_Drhs_diag(tempdiag_loc,ulc_prev,mymeth,1,0)
    i_p_face(0) = 1
    DO i=1,dim
       i_p_face(i) = i_p_face(i-1)*3
    END DO
    DO face_type = 0, 3**dim - 1
       face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
       IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
          CALL get_all_indices_by_face (face_type, myjl, nloc, iloc)
          IF(nloc > 0 ) THEN
             IF (ABS(face(1)) == 1) THEN
                DO jj=1,myne_loc
                   Drhs_diag_local((jj-1)*mynloc+iloc(1:nloc)) = tempdiag_loc((jj-1)*mynloc+iloc(1:nloc))  !from internal_Drhs_diag
                END DO
             END IF
          END IF
       END IF
    END DO
    IF (ALLOCATED(tempdiag_loc)) DEALLOCATE(tempdiag_loc) 

    CALL c_diff_diag(du_diag_local, d2u_diag_local, j_lev, mynloc, mymeth, mymeth, 10)

    pres_prev(:) = ulc_prev(:,nvel(1))/ulc_prev(:,nden)  !u0
 
    CALL c_diff_fast(pres_prev(:), du(:,:), d2u(:,:), myjl, mynloc, mymeth, 10, 1, 1, 1) 

    du(:,2) = 1.0_pr
    IF (Nspec>1) du(:,2) = du(:,2)-SUM(ulc_prev(:,nspc(1):nspc(Nspecm)),DIM=2)/ulc_prev(:,nden) !Y_Nspec 0
    d2u(:,1) = du(:,2)*cp_in(Nspec)  !cp_Nspec 0
    d2u(:,2) = du(:,2)/MW_in(Nspec)  !R_Nspec 0
    DO l=1,Nspecm
       d2u(:,1)  = d2u(:,1)  + ulc_prev(:,nspc(l))/ulc_prev(:,nden)*cp_in(l)  !cp 0
       d2u(:,2)  = d2u(:,2)  + ulc_prev(:,nspc(l))/ulc_prev(:,nden)/MW_in(l)  !R 0 
    END DO
    du(:,2) = d2u(:,1)/(d2u(:,1)-d2u(:,2))  !gamma0=cp0/(cp0-R0)

    i_p_face(0) = 1
    DO i=1,dim
       i_p_face(i) = i_p_face(i-1)*3
    END DO
    DO face_type = 0, 3**dim - 1
       face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
       IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
          CALL get_all_indices_by_face (face_type, myjl, nloc, iloc)
          IF(nloc > 0 ) THEN
             IF (ABS(face(1)) == 1) THEN
                DO jj=1,myne_loc
                   IF (jj .ne. neng) THEN
                      Drhs_diag_local((jj-1)*mynloc+iloc(1:nloc)) = Drhs_diag_local((jj-1)*mynloc+iloc(1:nloc)) - du(iloc(1:nloc),1)
                      IF (jj .eq. nden) THEN
                         Drhs_diag_local((jj-1)*mynloc+iloc(1:nloc)) = Drhs_diag_local((jj-1)*mynloc+iloc(1:nloc)) + &
                                 ulc_prev(iloc(1:nloc),nvel(1))/ulc_prev(iloc(1:nloc),nden)*du_diag_local(iloc(1:nloc),1)
                      ELSE IF(jj .eq. nvel(1)) THEN
                         Drhs_diag_local((jj-1)*mynloc+iloc(1:nloc)) = Drhs_diag_local((jj-1)*mynloc+iloc(1:nloc)) - &
                                 ulc_prev(iloc(1:nloc),nvel(1))/ulc_prev(iloc(1:nloc),nden)*du_diag_local(iloc(1:nloc),1)
                      END IF
                   ELSE 
                      Drhs_diag_local((jj-1)*mynloc+iloc(1:nloc)) = Drhs_diag_local((jj-1)*mynloc+iloc(1:nloc)) - du(iloc(1:nloc),1)*du(iloc(1:nloc),2)
                   END IF
                END DO
             END IF
          END IF
       END IF
    END DO
  END SUBROUTINE user_evol_LODIslip_BC_Drhs_diag 

  SUBROUTINE user_evol_LODIsimp_BC_rhs (myrhs_loc, ulc, myne_loc, mynloc, myjl, mymeth)
    !--Sets rhs for boundary conditions
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: myne_loc, mynloc, myjl, mymeth
    REAL (pr), DIMENSION (mynloc*myne_loc), INTENT (INOUT) :: myrhs_loc
    REAL (pr), DIMENSION (mynloc,myne_loc), INTENT (IN) :: ulc

    INTEGER :: ie, i, ii, shift, jj
    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc

    REAL (pr), DIMENSION (mynloc,Nspec+1) :: pres
    REAL (pr), DIMENSION (2,mynloc,dim) :: du, d2u
    REAL (pr), DIMENSION (:),   ALLOCATABLE :: charI, charV
    REAL (pr), DIMENSION (:,:),   ALLOCATABLE :: charX

    REAL (pr) :: faceval
    INTEGER :: l, faceint

    d2u(1,:,1) = 1.0_pr
    IF (Nspec>1) d2u(1,:,1) = d2u(1,:,1)-SUM(ulc(:,nspc(1):nspc(Nspecm)),DIM=2)/ulc(:,nden) !Y_Nspec
    d2u(2,:,1) = d2u(1,:,1)*cp_in(Nspec)  !cp_Nspec
    d2u(2,:,2) = d2u(1,:,1)/MW_in(Nspec)  !R_Nspec
    DO l=1,Nspecm
       d2u(2,:,1)  = d2u(2,:,1)  + ulc(:,nspc(l))/ulc(:,nden)*cp_in(l)  !cp
       d2u(2,:,2)  = d2u(2,:,2)  + ulc(:,nspc(l))/ulc(:,nden)/MW_in(l)  !R
    END DO
    d2u(1,:,1) = d2u(2,:,1)/(d2u(2,:,1)-d2u(2,:,2))  !gamma=cp/(cp-R)
    pres(:,1) = (ulc(:,neng)-0.5_pr*SUM(ulc(:,nvel(1):nvel(dim))**2,DIM=2)/ulc(:,nden))*(d2u(1,:,1)-1.0_pr)  !1: pressure
    d2u(1,:,2) = SQRT(d2u(1,:,1)*pres(:,1)/ulc(:,nden))  !c=SQRT(gamma*P/rho)
    pres(:,2) = ulc(:,nvel(1))/ulc(:,nden)  !2: u

    IF ((methpd .ne. 0) .OR. splitFBpress) THEN 
       DO l=1,Nspecm
          pres(:,2+l) = ulc(:,nspc(l))/ulc(:,nden)
       END DO
       du(1,:,1) = pres(:,1)
       IF (Nspec>1) THEN
          CALL dpdx (du(1,:,1),ng,pres(:,3:Nspec+1),2*methpd+mymeth,myjl,splitFBpress)
       ELSE
          CALL dpdx (du(1,:,1),ng,pres(:,1),2*methpd+mymeth,myjl,.FALSE.)
       END IF
       CALL c_diff_fast(pres(:,2), du(2,:,:), d2u(2,:,:), myjl, mynloc, mymeth, 10, 1, 1, 1)  
    ELSE
       CALL c_diff_fast(pres(:,1:2), du(1:2,:,:), d2u(1:2,:,:), myjl, mynloc, mymeth, 10, 2, 1, 2) 
    END IF

    i_p_face(0) = 1
    DO i=1,dim
       i_p_face(i) = i_p_face(i-1)*3
    END DO
    DO face_type = 0, 3**dim - 1
       face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
       IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
          CALL get_all_indices_by_face (face_type, myjl, nloc, iloc)
          IF(nloc > 0 ) THEN
             IF (ABS(face(1)) == 1) THEN
                IF (ALLOCATED(charV)) DEALLOCATE(charV)
                IF (ALLOCATED(charI)) DEALLOCATE(charI)
                IF (ALLOCATED(charX)) DEALLOCATE(charX)
                ALLOCATE(charV(nloc), charI(nloc), charX(nloc,3))

                faceval=REAL(face(1),pr)
                faceint=(3-face(1))/2

                charV(1:nloc) = pres(iloc(1:nloc),2) - faceval*d2u(1,iloc(1:nloc),2)

                charI(1:nloc) = charV(1:nloc)*(du(1,iloc(1:nloc),1) - faceval*ulc(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)*du(2,iloc(1:nloc),1))  !(u+/-c)*(dp/dx+/-rho*c*du/dx)
                IF (pertchar) charI(1:nloc) = charI(1:nloc) - charV(1:nloc)*dP0(faceint)
                IF     (PLpinf==1) THEN
                   charI(1:nloc) = charI(1:nloc) + pBcoef*(1.0_pr-maxMa**2)*c0(faceint)          *(pres(iloc(1:nloc),1) - P0(faceint))
                ELSEIF (PLpinf==2) THEN
                   charI(1:nloc) = charI(1:nloc) + pBcoef*(1.0_pr-maxMa**2)*d2u(1,iloc(1:nloc),2)*(pres(iloc(1:nloc),1) - P0(faceint))
                ELSEIF (PLpinf==3) THEN
                   charI(1:nloc) = charI(1:nloc) + pBcoef*(1.0_pr-maxMa**2)*c0(faceint)          *(pres(iloc(1:nloc),1)/ulc(iloc(1:nloc),nden)/d2u(2,iloc(1:nloc),2) - Tconst)
                ELSEIF (PLpinf==4) THEN
                   charI(1:nloc) = charI(1:nloc) + pBcoef*(1.0_pr-maxMa**2)*d2u(1,iloc(1:nloc),2)*(pres(iloc(1:nloc),1) - ulc(iloc(1:nloc),nden)*d2u(2,iloc(1:nloc),2)*Tconst)
                ELSEIF (PLpinf==5) THEN
                   charI(1:nloc) = charI(1:nloc) - faceval*pBcoef*(1.0_pr-maxMa**2)*gammBC(faceint)*Pint                             *pres(iloc(1:nloc),2)
                ELSEIF (PLpinf==6) THEN
                   charI(1:nloc) = charI(1:nloc) - faceval*pBcoef*(1.0_pr-maxMa**2)*ulc(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)**2*pres(iloc(1:nloc),2)
                END IF

                charX(1:nloc,1) = 0.5_pr/d2u(1,iloc(1:nloc),2)**2*charI(1:nloc)
                charX(1:nloc,2) = -faceval*0.5_pr/d2u(1,iloc(1:nloc),2)/ulc(iloc(1:nloc),nden)*charI(1:nloc)
                charX(1:nloc,3) = 0.5_pr*charI(1:nloc)

                myrhs_loc((nden-1)*mynloc+iloc(1:nloc)) = myrhs_loc((nden-1)*mynloc+iloc(1:nloc)) + charX(1:nloc,1)
                DO ii=nvel(1),nvel(dim)
                   myrhs_loc((ii-1)*mynloc+iloc(1:nloc)) = myrhs_loc((ii-1)*mynloc+iloc(1:nloc)) + ulc(iloc(1:nloc),ii)/ulc(iloc(1:nloc),nden)*charX(1:nloc,1)
                END DO
                myrhs_loc((nvel(1)-1)*mynloc+iloc(1:nloc)) = myrhs_loc((nvel(1)-1)*mynloc+iloc(1:nloc)) + ulc(iloc(1:nloc),nden)*charX(1:nloc,2)
                myrhs_loc((neng-1)*mynloc+iloc(1:nloc)) = myrhs_loc((neng-1)*mynloc+iloc(1:nloc)) + 1.0_pr/(d2u(1,iloc(1:nloc),1)-1.0_pr)*charX(1:nloc,3) + &
                                                          0.5_pr*SUM(ulc(iloc(1:nloc),nvel(1):nvel(dim))**2,DIM=2)/ulc(iloc(1:nloc),nden)**2*charX(1:nloc,1) + &
                                                          ulc(iloc(1:nloc),nvel(1))*charX(1:nloc,2)
                DO ii=1,Nspecm
                   myrhs_loc((nspc(ii)-1)*mynloc+iloc(1:nloc)) = myrhs_loc((nspc(ii)-1)*mynloc+iloc(1:nloc)) +  ulc(iloc(1:nloc),nspc(ii))/ulc(iloc(1:nloc),nden)*charX(1:nloc,1)
                END DO
                IF (ALLOCATED(charV)) DEALLOCATE(charV)
                IF (ALLOCATED(charI)) DEALLOCATE(charI)
                IF (ALLOCATED(charX)) DEALLOCATE(charX)
             END IF
          END IF
       END IF
    END DO
  END SUBROUTINE user_evol_LODIsimp_BC_rhs 

  SUBROUTINE user_evol_LODIsimp_BC_Drhs (Dmyrhs_loc, ulc, ulc_prev, myne_loc, mynloc, myjl, mymeth)
    !--Sets rhs for boundary conditions
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: myne_loc, mynloc, myjl, mymeth
    REAL (pr), DIMENSION (mynloc*myne_loc), INTENT (INOUT) :: Dmyrhs_loc
    REAL (pr), DIMENSION (mynloc,myne_loc), INTENT (IN) :: ulc, ulc_prev

    INTEGER :: ie, i, ii, shift, jj
    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc

    REAL (pr), DIMENSION (mynloc,Nspec+1) :: pres, pres_prev
    REAL (pr), DIMENSION (4,mynloc,dim) :: d2u
    REAL (pr), DIMENSION (2,mynloc,dim) :: du, du_prev
    REAL (pr), DIMENSION (:),   ALLOCATABLE :: charI, charV, charI_prev, charV_prev
    REAL (pr), DIMENSION (:,:),   ALLOCATABLE :: charX, charX_prev
    
    REAL (pr) :: faceval
    INTEGER :: l, faceint

    d2u(1,:,1) = 1.0_pr
    d2u(1,:,2) = 0.0_pr
    IF (Nspec>1) d2u(1,:,1) = d2u(1,:,1)-SUM(ulc_prev(:,nspc(1):nspc(Nspecm)),DIM=2)/ulc_prev(:,nden) !Y0_Nspec
    IF (Nspec>1) d2u(1,:,2) = (SUM(ulc_prev(:,nspc(1):nspc(Nspecm)),DIM=2)*ulc(:,nden)/ulc_prev(:,nden) - SUM(ulc(:,nspc(1):nspc(Nspecm)),DIM=2))/ulc_prev(:,nden) !Y'_Nspec 
    d2u(4,:,1) = d2u(1,:,2)*cp_in(Nspec)  !cp'_Nspec
    d2u(4,:,2) = d2u(1,:,2)/MW_in(Nspec)  !R'_Nspec
    d2u(2,:,1) = d2u(1,:,1)*cp_in(Nspec)  !cp0_Nspec
    d2u(2,:,2) = d2u(1,:,1)/MW_in(Nspec)  !R0_Nspec
    DO l=1,Nspecm
       d2u(2,:,1)  = d2u(2,:,1)  + ulc_prev(:,nspc(l))/ulc_prev(:,nden)*cp_in(l)  !cp0
       d2u(2,:,2)  = d2u(2,:,2)  + ulc_prev(:,nspc(l))/ulc_prev(:,nden)/MW_in(l)  !R0
       d2u(4,:,1)  = d2u(4,:,1)  + (ulc(:,nspc(l))-ulc_prev(:,nspc(l))/ulc_prev(:,nden)*ulc(:,nden))/ulc_prev(:,nden)*cp_in(l)  !cp'
       d2u(4,:,2)  = d2u(4,:,2)  + (ulc(:,nspc(l))-ulc_prev(:,nspc(l))/ulc_prev(:,nden)*ulc(:,nden))/ulc_prev(:,nden)/MW_in(l)  !R'
    END DO
    d2u(1,:,1) = d2u(2,:,1)/(d2u(2,:,1)-d2u(2,:,2))  !gamma0=cp0/(cp0-R0)
    pres_prev(:,1) = (ulc_prev(:,neng)-0.5_pr*SUM(ulc_prev(:,nvel(1):nvel(dim))**2,DIM=2)/ulc_prev(:,nden))*(d2u(1,:,1)-1.0_pr)  !1: p0
    d2u(1,:,2) = SQRT(d2u(1,:,1)*pres_prev(:,1)/ulc_prev(:,nden))  !c0=SQRT(gamma0*P0/rho0) 
    d2u(3,:,1) = (d2u(2,:,1)*d2u(4,:,2) - d2u(4,:,1)*d2u(2,:,2)) / (d2u(2,:,1)-d2u(2,:,2))**2  !gamma'=(cp0*R'-cp'*R0)/(cp0-R0)^2
    pres(:,1) =  d2u(3,:,1)*(ulc_prev(:,neng)-0.5_pr*SUM(ulc_prev(:,nvel(1):nvel(dim))**2,DIM=2))/ulc_prev(:,nden) +  &
                (d2u(1,:,1)-1.0_pr)*(ulc(:,neng)+0.5_pr*SUM(ulc_prev(:,nvel(1):nvel(dim))**2,DIM=2)/ulc_prev(:,nden)**2*ulc(:,nden) -  &
                 SUM(ulc_prev(:,nvel(1):nvel(dim))*ulc(:,nvel(1):nvel(dim)),DIM=2)/ulc_prev(:,nden) )  !1: p'
    d2u(3,:,2) = 0.5_pr/SQRT(d2u(1,:,1)*pres_prev(:,1)/ulc_prev(:,nden)) * ( d2u(1,:,1)*pres(:,1)+d2u(3,:,1)*pres_prev(:,1) - &
                        d2u(1,:,1)*pres_prev(:,1)/ulc_prev(:,nden)*ulc(:,nden) )/ulc_prev(:,nden)   !c'=1/2/SQRT(gamma0*P0/rho0)*(gamma*P/rho)'
    pres_prev(:,2) = ulc_prev(:,nvel(1))/ulc_prev(:,nden)  !2: u
    pres(:,2) = (ulc(:,nvel(1)) - ulc_prev(:,nvel(1))*ulc(:,nden)/ulc_prev(:,nden) )/ulc_prev(:,nden) !2: u'

    IF ((methpd .ne. 0) .OR. splitFBpress) THEN 
       DO l=1,Nspecm
          pres_prev(:,2+l) = ulc_prev(:,nspc(l))/ulc_prev(:,nden)
       END DO
       du(1,:,1) = pres(:,1)
       IF (Nspec>1) THEN
          CALL dpdx (du(1,:,1),ng,pres_prev(:,3:Nspec+1),2*methpd+mymeth,myjl,splitFBpress)
       ELSE
          CALL dpdx (du(1,:,1),ng,pres_prev(:,1),2*methpd+mymeth,myjl,.FALSE.)
       END IF
       CALL c_diff_fast(pres(:,2), du(2,:,:), d2u(2,:,:), myjl, mynloc, mymeth, 10, 1, 1, 1)  
    ELSE
       CALL c_diff_fast(pres(:,1:2), du(1:2,:,:), d2u(1:2,:,:), myjl, mynloc, mymeth, 10, 2, 1, 2) 
    END IF
    IF ((methpd .ne. 0) .OR. splitFBpress) THEN 
       DO l=1,Nspecm
          pres_prev(:,2+l) = ulc_prev(:,nspc(l))/ulc_prev(:,nden)
       END DO
       du_prev(1,:,1) = pres_prev(:,1)
       IF (Nspec>1) THEN
          CALL dpdx (du_prev(1,:,1),ng,pres_prev(:,3:Nspec+1),2*methpd+mymeth,myjl,splitFBpress)
       ELSE
          CALL dpdx (du_prev(1,:,1),ng,pres_prev(:,1),2*methpd+mymeth,myjl,.FALSE.)
       END IF
       CALL c_diff_fast(pres_prev(:,2), du_prev(2,:,:), d2u(2,:,:), myjl, mynloc, mymeth, 10, 1, 1, 1)  
    ELSE
       CALL c_diff_fast(pres_prev(:,1:2), du_prev(1:2,:,:), d2u(1:2,:,:), myjl, mynloc, mymeth, 10, 2, 1, 2) 
    END IF

    i_p_face(0) = 1
    DO i=1,dim
       i_p_face(i) = i_p_face(i-1)*3
    END DO
    DO face_type = 0, 3**dim - 1
       face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
       IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
          CALL get_all_indices_by_face (face_type, myjl, nloc, iloc)
          IF(nloc > 0 ) THEN
             IF (ABS(face(1)) == 1) THEN
                IF (ALLOCATED(charV)) DEALLOCATE(charV)
                IF (ALLOCATED(charI)) DEALLOCATE(charI)
                IF (ALLOCATED(charX)) DEALLOCATE(charX)
                IF (ALLOCATED(charV_prev)) DEALLOCATE(charV_prev)
                IF (ALLOCATED(charI_prev)) DEALLOCATE(charI_prev)
                IF (ALLOCATED(charX_prev)) DEALLOCATE(charX_prev)
                ALLOCATE(charV(nloc),charI(nloc),charX(nloc,3),charV_prev(nloc),charI_prev(nloc),charX_prev(nloc,3))

                faceval=REAL(face(1),pr)
                faceint=(3-face(1))/2

                charV_prev(1:nloc) = pres_prev(iloc(1:nloc),2) - faceval*d2u(1,iloc(1:nloc),2)
                charV(1:nloc) = pres(iloc(1:nloc),2) - faceval*d2u(3,iloc(1:nloc),2)

                charI_prev(1:nloc) = charV_prev(1:nloc)*(du_prev(1,iloc(1:nloc),1)-faceval*ulc_prev(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)*du_prev(2,iloc(1:nloc),1))  !(u-c)*(dp/dx-rho*c*du/dx)
                charI(1:nloc) = charV_prev(1:nloc)*(du(1,iloc(1:nloc),1) - faceval*ulc_prev(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)*du(2,iloc(1:nloc),1)) + &
                                charV(1:nloc)*(du_prev(1,iloc(1:nloc),1) - faceval*ulc_prev(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)*du_prev(2,iloc(1:nloc),1)) - &
                                faceval*charV_prev(1:nloc)*du_prev(2,iloc(1:nloc),1) * (ulc(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)+ulc_prev(iloc(1:nloc),nden)*d2u(3,iloc(1:nloc),2)) 
                IF (pertchar) THEN
                   charI_prev(1:nloc) = charI_prev(1:nloc) - charV_prev(1:nloc) * dP0(faceint)
                   charI(1:nloc)      = charI(1:nloc)      - charV(1:nloc)      * dP0(faceint)
                END IF
                IF     (PLpinf==1) THEN
                   charI_prev(1:nloc) = charI_prev(1:nloc) + pBcoef*(1.0_pr-maxMa**2)*c0(faceint)          *(pres_prev(iloc(1:nloc),1) - P0(faceint))
                   charI(1:nloc) = charI(1:nloc) + pBcoef*(1.0_pr-maxMa**2)*c0(faceint)*pres(iloc(1:nloc),1)
                ELSEIF (PLpinf==2) THEN
                   charI_prev(1:nloc) = charI_prev(1:nloc) + pBcoef*(1.0_pr-maxMa**2)*d2u(1,iloc(1:nloc),2)*(pres_prev(iloc(1:nloc),1) - P0(faceint))
                   charI(1:nloc) = charI(1:nloc) + pBcoef*(1.0_pr-maxMa**2) * (d2u(3,iloc(1:nloc),2)*(pres_prev(iloc(1:nloc),1) - P0(faceint)) + d2u(1,iloc(1:nloc),2)*pres(iloc(1:nloc),1) )
                ELSEIF (PLpinf==3) THEN
                   charI_prev(1:nloc) = charI_prev(1:nloc) + pBcoef*(1.0_pr-maxMa**2)*c0(faceint)          *(pres_prev(iloc(1:nloc),1)/ulc_prev(iloc(1:nloc),nden)/d2u(2,iloc(1:nloc),2) - Tconst)
                   charI(1:nloc) = charI(1:nloc) + pBcoef*(1.0_pr-maxMa**2)*c0(faceint)/ulc_prev(iloc(1:nloc),nden)/d2u(2,iloc(1:nloc),2) * &
                                        (pres(iloc(1:nloc),1)-pres_prev(iloc(1:nloc),1)*(ulc(iloc(1:nloc),nden)/ulc_prev(iloc(1:nloc),nden)+d2u(4,iloc(1:nloc),2)/d2u(2,iloc(1:nloc),2)) )
                ELSEIF (PLpinf==4) THEN
                   charI_prev(1:nloc) = charI_prev(1:nloc) + pBcoef*(1.0_pr-maxMa**2)*d2u(1,iloc(1:nloc),2)*(pres_prev(iloc(1:nloc),1) - ulc_prev(iloc(1:nloc),nden)*d2u(2,iloc(1:nloc),2)*Tconst)
                   charI(1:nloc) = charI(1:nloc) + pBcoef*(1.0_pr-maxMa**2) * (d2u(3,iloc(1:nloc),2)*(pres_prev(iloc(1:nloc),1) - ulc_prev(iloc(1:nloc),nden)*d2u(2,iloc(1:nloc),2)*Tconst) + &
                                     d2u(1,iloc(1:nloc),2)*(pres(iloc(1:nloc),1) - ulc(iloc(1:nloc),nden)*d2u(2,iloc(1:nloc),2)*Tconst - ulc_prev(iloc(1:nloc),nden)*d2u(4,iloc(1:nloc),2)*Tconst) )
                ELSEIF (PLpinf==5) THEN
                   charI_prev(1:nloc) = charI_prev(1:nloc) - faceval*pBcoef*(1.0_pr-maxMa**2)*gammBC(faceint)*Pint                                  *pres_prev(iloc(1:nloc),2)
                   charI(1:nloc) = charI(1:nloc) - faceval*pBcoef*(1.0_pr-maxMa**2)*gammBC(faceint)*Pint*pres(iloc(1:nloc),2)
                ELSEIF (PLpinf==6) THEN
                   charI_prev(1:nloc) = charI_prev(1:nloc) - faceval*pBcoef*(1.0_pr-maxMa**2)*ulc_prev(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)**2*pres_prev(iloc(1:nloc),2)
                   charI(1:nloc) = charI(1:nloc) - faceval*pBcoef*(1.0_pr-maxMa**2)*d2u(1,iloc(1:nloc),2) * &
                                                   (ulc(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)*pres_prev(iloc(1:nloc),2) + &
                                                    2.0_pr*ulc_prev(iloc(1:nloc),nden)*d2u(3,iloc(1:nloc),2)*pres_prev(iloc(1:nloc),2) + &
                                                    ulc_prev(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)*pres(iloc(1:nloc),2) )
                END IF

                charX_prev(1:nloc,1) = 0.5_pr/d2u(1,iloc(1:nloc),2)**2*charI_prev(1:nloc)
                charX_prev(1:nloc,2) = -faceval*0.5_pr/d2u(1,iloc(1:nloc),2)/ulc_prev(iloc(1:nloc),nden)*charI_prev(1:nloc)
                charX_prev(1:nloc,3) = 0.5_pr*charI_prev(1:nloc)
                charX(1:nloc,1) = 0.5_pr/d2u(1,iloc(1:nloc),2)**2*charI(1:nloc) - d2u(3,iloc(1:nloc),2)/d2u(1,iloc(1:nloc),2)**3*charI_prev(1:nloc)
                charX(1:nloc,2) =-faceval*0.5_pr/d2u(1,iloc(1:nloc),2)/ulc_prev(iloc(1:nloc),nden)*(charI(1:nloc)-charI_prev(1:nloc) * &
                                                 (ulc(iloc(1:nloc),nden)/ulc_prev(iloc(1:nloc),nden) + d2u(3,iloc(1:nloc),2)/d2u(1,iloc(1:nloc),2)) )
                charX(1:nloc,3) = 0.5_pr*charI(1:nloc)

                Dmyrhs_loc((nden-1)*mynloc+iloc(1:nloc)) = Dmyrhs_loc((nden-1)*mynloc+iloc(1:nloc)) + charX(1:nloc,1)
                DO ii=nvel(1),nvel(dim)
                   Dmyrhs_loc((ii-1)*mynloc+iloc(1:nloc)) = Dmyrhs_loc((ii-1)*mynloc+iloc(1:nloc)) + ulc_prev(iloc(1:nloc),ii)/ulc_prev(iloc(1:nloc),nden)*charX(1:nloc,1) + &
                                 (ulc(iloc(1:nloc),ii) - ulc_prev(iloc(1:nloc),ii)*ulc(iloc(1:nloc),nden)/ulc_prev(iloc(1:nloc),nden) )/ulc_prev(iloc(1:nloc),nden)*charX_prev(1:nloc,1)
                END DO
                Dmyrhs_loc((nvel(1)-1)*mynloc+iloc(1:nloc)) = Dmyrhs_loc((nvel(1)-1)*mynloc+iloc(1:nloc)) + ulc_prev(iloc(1:nloc),nden)*charX(1:nloc,2) + &
                                                                                                            ulc(iloc(1:nloc),nden)*charX_prev(1:nloc,2)
                Dmyrhs_loc((neng-1)*mynloc+iloc(1:nloc)) = Dmyrhs_loc((neng-1)*mynloc+iloc(1:nloc)) + &
                               1.0_pr/(d2u(1,iloc(1:nloc),1)-1.0_pr)*(charX(1:nloc,3) - d2u(3,iloc(1:nloc),1)/(d2u(1,iloc(1:nloc),1)-1.0_pr)*charX_prev(1:nloc,3)) + & 
                               0.5_pr*SUM(ulc_prev(iloc(1:nloc),nvel(1):nvel(dim))**2,DIM=2)/ulc_prev(iloc(1:nloc),nden)**2*charX(1:nloc,1) + &
                              (SUM(ulc_prev(iloc(1:nloc),nvel(1):nvel(dim))*ulc(iloc(1:nloc),nvel(1):nvel(dim)),DIM=2) - &
                               SUM(ulc_prev(iloc(1:nloc),nvel(1):nvel(dim))**2,DIM=2)/ulc_prev(iloc(1:nloc),nden)*ulc(iloc(1:nloc),nden) ) / ulc_prev(iloc(1:nloc),nden)**2*charX_prev(1:nloc,1) + &
                               ulc_prev(iloc(1:nloc),nvel(1))*charX(1:nloc,2) + ulc(iloc(1:nloc),nvel(1))*charX_prev(1:nloc,2)
                DO ii=1,Nspecm
                   Dmyrhs_loc((nspc(ii)-1)*mynloc+iloc(1:nloc)) = Dmyrhs_loc((nspc(ii)-1)*mynloc+iloc(1:nloc)) + ulc_prev(iloc(1:nloc),nspc(ii))/ulc_prev(iloc(1:nloc),nden)*charX(1:nloc,1) + &
                                 (ulc(iloc(1:nloc),nspc(ii)) - ulc_prev(iloc(1:nloc),nspc(ii))*ulc(iloc(1:nloc),nden)/ulc_prev(iloc(1:nloc),nden) )/ulc_prev(iloc(1:nloc),nden)*charX_prev(1:nloc,1)
                END DO

                IF (ALLOCATED(charV)) DEALLOCATE(charV)
                IF (ALLOCATED(charI)) DEALLOCATE(charI)
                IF (ALLOCATED(charX)) DEALLOCATE(charX)
                IF (ALLOCATED(charV_prev)) DEALLOCATE(charV_prev)
                IF (ALLOCATED(charI_prev)) DEALLOCATE(charI_prev)
                IF (ALLOCATED(charX_prev)) DEALLOCATE(charX_prev)
             END IF
          END IF
       END IF
    END DO
  END SUBROUTINE user_evol_LODIsimp_BC_Drhs 

  SUBROUTINE user_evol_LODIsimp_BC_Drhs_diag (Drhs_diag_local, ulc_prev, myne_loc, mynloc, myjl, mymeth)
    !--Sets rhs for boundary conditions
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: myne_loc, mynloc, myjl, mymeth
    REAL (pr), DIMENSION (mynloc*myne_loc), INTENT (INOUT) :: Drhs_diag_local
    REAL (pr), DIMENSION (mynloc,myne_loc), INTENT (IN) :: ulc_prev

    INTEGER :: ie, i, ii, shift, jj
    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc

    REAL (pr), DIMENSION (:),   ALLOCATABLE :: charI, charV_prev, charI_prev, charX_prev
    REAL (pr), DIMENSION (:,:),   ALLOCATABLE :: charX
    REAL (pr), DIMENSION (mynloc,dim) :: du_diag_local, d2u_diag_local
    REAL (pr), DIMENSION (mynloc) :: du_diag_PFB
    REAL (pr), DIMENSION (mynloc,Nspec+1):: pres_prev
    REAL (pr), DIMENSION (3,mynloc,dim) :: du, d2u
    REAL (pr) :: faceval
    INTEGER :: l, faceint

    CALL c_diff_diag(du_diag_local, d2u_diag_local, j_lev, mynloc, mymeth, mymeth, 10)

    d2u(1,:,1) = 1.0_pr
    IF (Nspec>1) d2u(1,:,1) = d2u(1,:,1)-SUM(ulc_prev(:,nspc(1):nspc(Nspecm)),DIM=2)/ulc_prev(:,nden) !Y0_Nspec
    d2u(2,:,1) = d2u(1,:,1)*cp_in(Nspec)  !cp0_Nspec
    d2u(2,:,2) = d2u(1,:,1)/MW_in(Nspec)  !R0_Nspec
    DO l=1,Nspecm
       d2u(2,:,1)  = d2u(2,:,1)  + ulc_prev(:,nspc(l))/ulc_prev(:,nden)*cp_in(l)  !cp0
       d2u(2,:,2)  = d2u(2,:,2)  + ulc_prev(:,nspc(l))/ulc_prev(:,nden)/MW_in(l)  !R0
    END DO
    d2u(1,:,1) = d2u(2,:,1)/(d2u(2,:,1)-d2u(2,:,2))  !gamma0=cp0/(cp0-R0)
    pres_prev(:,1) = (ulc_prev(:,neng)-0.5_pr*SUM(ulc_prev(:,nvel(1):nvel(dim))**2,DIM=2)/ulc_prev(:,nden))*(d2u(1,:,1)-1.0_pr)  !1: pressure
    d2u(1,:,2) = SQRT(d2u(1,:,1)*pres_prev(:,1)/ulc_prev(:,nden))  !c0=SQRT(gamma*P/rho)
    pres_prev(:,2) = ulc_prev(:,nvel(1))/ulc_prev(:,nden)  !u0

    IF ((methpd .ne. 0) .OR. splitFBpress) THEN 
       du(1,:,1) = pres_prev(:,1)
       DO i=1,Nspecm
          pres_prev(:,2+i) = ulc_prev(:,nspc(i))/ulc_prev(:,nden)
       END DO
       IF (Nspec>1) THEN
          CALL dpdx (du(1,:,1),ng,pres_prev(:,3:Nspec+1),2*methpd+mymeth,myjl,splitFBpress)
       ELSE
          CALL dpdx (du(1,:,1),ng,pres_prev(:,1),2*methpd+mymeth,myjl,.FALSE.)
       END IF
       CALL c_diff_fast(pres_prev(:,2), du(2,:,:), d2u(2,:,:), myjl, mynloc, mymeth, 10, 1, 1, 1)  
       IF (Nspec>1) THEN
          CALL dpdx_diag (du_diag_PFB(:),ng,pres_prev(:,3:Nspec+1),2*methpd+mymeth,myjl,splitFBpress)
       ELSE
          CALL dpdx_diag (du_diag_PFB(:),ng,pres_prev(:,1),2*methpd+mymeth,myjl,.FALSE.)
       END IF
    ELSE
       CALL c_diff_fast(pres_prev(:,1:2), du(1:2,:,:), d2u(1:2,:,:), myjl, mynloc, mymeth, 10, 2, 1, 2) 
       du_diag_PFB(:) = du_diag_local(:,1)
    END IF

    i_p_face(0) = 1
    DO i=1,dim
       i_p_face(i) = i_p_face(i-1)*3
    END DO
    DO face_type = 0, 3**dim - 1
       face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
       IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
          CALL get_all_indices_by_face (face_type, myjl, nloc, iloc)
          IF(nloc > 0 ) THEN
             IF (ABS(face(1)) == 1) THEN
                IF (ALLOCATED(charI)) DEALLOCATE(charI)
                IF (ALLOCATED(charX)) DEALLOCATE(charX)
                IF (ALLOCATED(charV_prev)) DEALLOCATE(charV_prev)
                IF (ALLOCATED(charI_prev)) DEALLOCATE(charI_prev)
                IF (ALLOCATED(charX_prev)) DEALLOCATE(charX_prev)
                ALLOCATE(charI(nloc),charX(nloc,3),charV_prev(nloc),charI_prev(nloc),charX_prev(nloc))

                faceval=REAL(face(1),pr)
                faceint=(3-face(1))/2
               
                charV_prev(1:nloc) = pres_prev(iloc(1:nloc),2) - faceval*d2u(1,iloc(1:nloc),2)

                charI_prev(1:nloc) = charV_prev(1:nloc)*(du(1,iloc(1:nloc),1)-faceval*ulc_prev(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)*du(2,iloc(1:nloc),1))  !(u-c)*(dp/dx-rho*c*du/dx)
                IF (pertchar) charI_prev(1:nloc) = charI_prev(1:nloc) - charV_prev(1:nloc) * dP0(faceint)
                IF     (PLpinf==1) THEN
                   charI_prev(1:nloc) = charI_prev(1:nloc) + pBcoef*(1.0_pr-maxMa**2)*c0(faceint)          *(pres_prev(iloc(1:nloc),1) - P0(faceint))
                ELSEIF (PLpinf==2) THEN
                   charI_prev(1:nloc) = charI_prev(1:nloc) + pBcoef*(1.0_pr-maxMa**2)*d2u(1,iloc(1:nloc),2)*(pres_prev(iloc(1:nloc),1) - P0(faceint))
                ELSEIF (PLpinf==3) THEN
                   charI_prev(1:nloc) = charI_prev(1:nloc) + pBcoef*(1.0_pr-maxMa**2)*c0(faceint)          *(pres_prev(iloc(1:nloc),1)/ulc_prev(iloc(1:nloc),nden)/d2u(2,iloc(1:nloc),2) - Tconst)
                ELSEIF (PLpinf==4) THEN
                   charI_prev(1:nloc) = charI_prev(1:nloc) + pBcoef*(1.0_pr-maxMa**2)*d2u(1,iloc(1:nloc),2)*(pres_prev(iloc(1:nloc),1) - ulc_prev(iloc(1:nloc),nden)*d2u(2,iloc(1:nloc),2)*Tconst)
                ELSEIF (PLpinf==5) THEN
                   charI_prev(1:nloc) = charI_prev(1:nloc) - faceval*pBcoef*(1.0_pr-maxMa**2)*gammBC(faceint)*Pint                                  *pres_prev(iloc(1:nloc),2)
                ELSEIF (PLpinf==6) THEN
                   charI_prev(1:nloc) = charI_prev(1:nloc) - faceval*pBcoef*(1.0_pr-maxMa**2)*ulc_prev(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)**2*pres_prev(iloc(1:nloc),2)
                END IF
 
                charX_prev(1:nloc) = 0.5_pr/d2u(1,iloc(1:nloc),2)**2*charI_prev(1:nloc)
                
                DO jj=1,myne_loc
                   IF (jj==nden) THEN
                      du (2,iloc(1:nloc),2) = -ulc_prev(iloc(1:nloc),nvel(1))/ulc_prev(iloc(1:nloc),nden)**2 !u_diag
                      d2u(3,iloc(1:nloc),1) = 0.0_pr  !gamma_diag
                      du (1,iloc(1:nloc),2) = 0.5_pr*SUM(ulc_prev(iloc(1:nloc),nvel(1):nvel(dim))**2,DIM=2)/ulc_prev(iloc(1:nloc),nden)**2*(d2u(1,iloc(1:nloc),1)-1.0_pr) !p_diag
                      d2u(3,iloc(1:nloc),2) = 0.5_pr/d2u(1,iloc(1:nloc),2)*d2u(1,iloc(1:nloc),1)/ulc_prev(iloc(1:nloc),nden) * &
                                                    (du(1,iloc(1:nloc),2)-pres_prev(iloc(1:nloc),1)/ulc_prev(iloc(1:nloc),nden) ) !c_diag
                      charI(1:nloc) = (du(2,iloc(1:nloc),2)-faceval*d2u(3,iloc(1:nloc),2))*(du(1,iloc(1:nloc),1)-faceval*ulc_prev(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)*du(2,iloc(1:nloc),1)) + &
                                      charV_prev(1:nloc)*(du_diag_PFB(iloc(1:nloc))*du(1,iloc(1:nloc),2) - &
                                                          faceval*du_diag_local(iloc(1:nloc),1)*ulc_prev(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)*du(2,iloc(1:nloc),2) - &
                                                          faceval*du(2,iloc(1:nloc),1)*(ulc_prev(iloc(1:nloc),nden)*d2u(3,iloc(1:nloc),2) + d2u(1,iloc(1:nloc),2)))
                      IF (pertchar) charI(1:nloc) = charI(1:nloc) - (du(2,iloc(1:nloc),2)-faceval*d2u(3,iloc(1:nloc),2)) * dP0(faceint)
                      IF     (PLpinf==1) THEN
                         charI(1:nloc) = charI(1:nloc) + pBcoef*(1.0_pr-maxMa**2)*c0(faceint)*du(1,iloc(1:nloc),2)
                      ELSEIF (PLpinf==2) THEN
                         charI(1:nloc) = charI(1:nloc) + pBcoef*(1.0_pr-maxMa**2)*(d2u(3,iloc(1:nloc),2)*(pres_prev(iloc(1:nloc),1) - P0(faceint)) + d2u(1,iloc(1:nloc),2)*du(1,iloc(1:nloc),2))
                      ELSEIF (PLpinf==3) THEN
                         charI(1:nloc) = charI(1:nloc) + pBcoef*(1.0_pr-maxMa**2)*c0(faceint)/ulc_prev(iloc(1:nloc),nden)/d2u(2,iloc(1:nloc),2) * du(1,iloc(1:nloc),2)
                      ELSEIF (PLpinf==4) THEN
                         charI(1:nloc) = charI(1:nloc) + pBcoef*(1.0_pr-maxMa**2) * (d2u(3,iloc(1:nloc),2)*(pres_prev(iloc(1:nloc),1) - ulc_prev(iloc(1:nloc),nden)*d2u(2,iloc(1:nloc),2)*Tconst) + &
                                                                                     d2u(1,iloc(1:nloc),2)* du(1,iloc(1:nloc),2) )
                      ELSEIF (PLpinf==5) THEN
                         charI(1:nloc) = charI(1:nloc) - faceval*pBcoef*(1.0_pr-maxMa**2)*gammBC(faceint)*Pint*du(2,iloc(1:nloc),2)
                      ELSEIF (PLpinf==6) THEN
                         charI(1:nloc) = charI(1:nloc) - faceval*pBcoef*(1.0_pr-maxMa**2)*d2u(1,iloc(1:nloc),2) * &
                                                         (d2u(1,iloc(1:nloc),2)*pres_prev(iloc(1:nloc),2) + &
                                                          2.0_pr*ulc_prev(iloc(1:nloc),nden)*d2u(3,iloc(1:nloc),2)*pres_prev(iloc(1:nloc),2) + &
                                                          ulc_prev(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)*du(2,iloc(1:nloc),2) )
                      END IF
                      charX(1:nloc,1) = 0.5_pr/d2u(1,iloc(1:nloc),2)**2*charI(1:nloc) - d2u(3,iloc(1:nloc),2)/d2u(1,iloc(1:nloc),2)**3*charI_prev(1:nloc)
                      Drhs_diag_local((jj-1)*mynloc+iloc(1:nloc)) = Drhs_diag_local((jj-1)*mynloc+iloc(1:nloc)) + charX(1:nloc,1)
                   ELSE IF (jj==nvel(1)) THEN
                      du (2,iloc(1:nloc),2) = 1.0_pr/ulc_prev(iloc(1:nloc),nden) !u_diag
                      d2u(3,iloc(1:nloc),1) = 0.0_pr  !gamma_diag
                      du (1,iloc(1:nloc),2) = -ulc_prev(iloc(1:nloc),nvel(1))/ulc_prev(iloc(1:nloc),nden)*(d2u(1,iloc(1:nloc),1)-1.0_pr) !p_diag
                      d2u(3,iloc(1:nloc),2) = 0.5_pr/d2u(1,iloc(1:nloc),2)*d2u(1,iloc(1:nloc),1)/ulc_prev(iloc(1:nloc),nden)*du(1,iloc(1:nloc),2) !c_diag
                      charI(1:nloc) = (du(2,iloc(1:nloc),2)-faceval*d2u(3,iloc(1:nloc),2))*(du(1,iloc(1:nloc),1)-faceval*ulc_prev(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)*du(2,iloc(1:nloc),1)) + &
                                      charV_prev(1:nloc)*(du_diag_PFB(iloc(1:nloc))*du(1,iloc(1:nloc),2) - &
                                                          faceval*du_diag_local(iloc(1:nloc),1)*ulc_prev(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)*du(2,iloc(1:nloc),2) - &
                                                          faceval*du(2,iloc(1:nloc),1)*ulc_prev(iloc(1:nloc),nden)*d2u(3,iloc(1:nloc),2)  )
                      IF (pertchar) charI(1:nloc) = charI(1:nloc) - (du(2,iloc(1:nloc),2)-faceval*d2u(3,iloc(1:nloc),2)) * dP0(faceint)
                      IF     (PLpinf==1) THEN
                         charI(1:nloc) = charI(1:nloc) + pBcoef*(1.0_pr-maxMa**2)*c0(faceint)*du(1,iloc(1:nloc),2)
                      ELSEIF (PLpinf==2) THEN
                         charI(1:nloc) = charI(1:nloc) + pBcoef*(1.0_pr-maxMa**2)*(d2u(3,iloc(1:nloc),2)*(pres_prev(iloc(1:nloc),1) - P0(faceint)) + d2u(1,iloc(1:nloc),2)*du(1,iloc(1:nloc),2))
                      ELSEIF (PLpinf==3) THEN
                         charI(1:nloc) = charI(1:nloc) + pBcoef*(1.0_pr-maxMa**2)*c0(faceint)/ulc_prev(iloc(1:nloc),nden)/d2u(2,iloc(1:nloc),2) * du(1,iloc(1:nloc),2)
                      ELSEIF (PLpinf==4) THEN
                         charI(1:nloc) = charI(1:nloc) + pBcoef*(1.0_pr-maxMa**2) * (d2u(3,iloc(1:nloc),2)*(pres_prev(iloc(1:nloc),1) - ulc_prev(iloc(1:nloc),nden)*d2u(2,iloc(1:nloc),2)*Tconst) + &
                                                                                     d2u(1,iloc(1:nloc),2)* du(1,iloc(1:nloc),2) )
                      ELSEIF (PLpinf==5) THEN
                         charI(1:nloc) = charI(1:nloc) - faceval*pBcoef*(1.0_pr-maxMa**2)*gammBC(faceint)*Pint*du(2,iloc(1:nloc),2)
                      ELSEIF (PLpinf==6) THEN
                         charI(1:nloc) = charI(1:nloc) - faceval*pBcoef*(1.0_pr-maxMa**2)*d2u(1,iloc(1:nloc),2) * &
                                                         (2.0_pr*ulc_prev(iloc(1:nloc),nden)*d2u(3,iloc(1:nloc),2)*pres_prev(iloc(1:nloc),2) + &
                                                          ulc_prev(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)*du(2,iloc(1:nloc),2) )
                      END IF
                      charX(1:nloc,1) = 0.5_pr/d2u(1,iloc(1:nloc),2)**2*charI(1:nloc) - d2u(3,iloc(1:nloc),2)/d2u(1,iloc(1:nloc),2)**3*charI_prev(1:nloc)
                      charX(1:nloc,2) = -faceval*0.5_pr/ulc_prev(iloc(1:nloc),nden)/d2u(1,iloc(1:nloc),2)*(charI(1:nloc) - charI_prev(1:nloc)*d2u(3,iloc(1:nloc),2)/d2u(1,iloc(1:nloc),2))
                      Drhs_diag_local((jj-1)*mynloc+iloc(1:nloc)) = Drhs_diag_local((jj-1)*mynloc+iloc(1:nloc)) + du(2,iloc(1:nloc),2)*charX_prev(1:nloc) + &
                                                     pres_prev(iloc(1:nloc),2)*charX(1:nloc,1) + ulc_prev(iloc(1:nloc),nden)*charX(1:nloc,2)
                   ELSE IF (jj>=nvel(2) .AND. jj<=nvel(dim)) THEN
                      du (2,iloc(1:nloc),2) = 0.0_pr !u_diag
                      d2u(3,iloc(1:nloc),1) = 0.0_pr  !gamma_diag
                      du (1,iloc(1:nloc),2) = -ulc_prev(iloc(1:nloc),jj)/ulc_prev(iloc(1:nloc),nden)*(d2u(1,iloc(1:nloc),1)-1.0_pr) !p_diag
                      d2u(3,iloc(1:nloc),2) = 0.5_pr/d2u(1,iloc(1:nloc),2)*d2u(1,iloc(1:nloc),1)/ulc_prev(iloc(1:nloc),nden)*du(1,iloc(1:nloc),2) !c_diag
                      charI(1:nloc) = -faceval*d2u(3,iloc(1:nloc),2)*(du(1,iloc(1:nloc),1)-faceval*ulc_prev(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)*du(2,iloc(1:nloc),1)) + &
                                      charV_prev(1:nloc)*(du_diag_PFB(iloc(1:nloc))*du(1,iloc(1:nloc),2) - faceval*du(2,iloc(1:nloc),1)*ulc_prev(iloc(1:nloc),nden)*d2u(3,iloc(1:nloc),2))
                      IF (pertchar) charI(1:nloc) = charI(1:nloc) + faceval*d2u(3,iloc(1:nloc),2) * dP0(faceint)
                      IF     (PLpinf==1) THEN
                         charI(1:nloc) = charI(1:nloc) + pBcoef*(1.0_pr-maxMa**2)*c0(faceint)*du(1,iloc(1:nloc),2)
                      ELSEIF (PLpinf==2) THEN
                         charI(1:nloc) = charI(1:nloc) + pBcoef*(1.0_pr-maxMa**2)*(d2u(3,iloc(1:nloc),2)*(pres_prev(iloc(1:nloc),1) - P0(faceint)) + d2u(1,iloc(1:nloc),2)*du(1,iloc(1:nloc),2))
                      ELSEIF (PLpinf==3) THEN
                         charI(1:nloc) = charI(1:nloc) + pBcoef*(1.0_pr-maxMa**2)*c0(faceint)/ulc_prev(iloc(1:nloc),nden)/d2u(2,iloc(1:nloc),2) * du(1,iloc(1:nloc),2)
                      ELSEIF (PLpinf==4) THEN
                         charI(1:nloc) = charI(1:nloc) + pBcoef*(1.0_pr-maxMa**2) * (d2u(3,iloc(1:nloc),2)*(pres_prev(iloc(1:nloc),1) - ulc_prev(iloc(1:nloc),nden)*d2u(2,iloc(1:nloc),2)*Tconst) + &
                                                                                     d2u(1,iloc(1:nloc),2)* du(1,iloc(1:nloc),2) )
                      ELSEIF (PLpinf==6) THEN
                         charI(1:nloc) = charI(1:nloc) - faceval*pBcoef*(1.0_pr-maxMa**2)*d2u(1,iloc(1:nloc),2) * &
                                                         (2.0_pr*ulc_prev(iloc(1:nloc),nden)*d2u(3,iloc(1:nloc),2)*pres_prev(iloc(1:nloc),2))
                      END IF
                      charX(1:nloc,1) = 0.5_pr/d2u(1,iloc(1:nloc),2)**2*charI(1:nloc) - d2u(3,iloc(1:nloc),2)/d2u(1,iloc(1:nloc),2)**3*charI_prev(1:nloc)
                      Drhs_diag_local((jj-1)*mynloc+iloc(1:nloc)) = Drhs_diag_local((jj-1)*mynloc+iloc(1:nloc)) + (charX_prev(1:nloc)+ulc_prev(iloc(1:nloc),jj)*charX(1:nloc,1)) / &
                                                     ulc_prev(iloc(1:nloc),nden)
                   ELSE IF (jj==neng) THEN
                      du (2,iloc(1:nloc),2) = 0.0_pr !u_diag
                      d2u(3,iloc(1:nloc),1) = 0.0_pr  !gamma_diag
                      du (1,iloc(1:nloc),2) = d2u(1,iloc(1:nloc),1)-1.0_pr !p_diag
                      d2u(3,iloc(1:nloc),2) = 0.5_pr/d2u(1,iloc(1:nloc),2)*d2u(1,iloc(1:nloc),1)/ulc_prev(iloc(1:nloc),nden)*du(1,iloc(1:nloc),2) !c_diag
                      charI(1:nloc) = -faceval*d2u(3,iloc(1:nloc),2)*(du(1,iloc(1:nloc),1)-faceval*ulc_prev(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)*du(2,iloc(1:nloc),1)) + &
                                      charV_prev(1:nloc)*(du_diag_PFB(iloc(1:nloc))*du(1,iloc(1:nloc),2) - faceval*du(2,iloc(1:nloc),1)*ulc_prev(iloc(1:nloc),nden)*d2u(3,iloc(1:nloc),2))
                      IF (pertchar) charI(1:nloc) = charI(1:nloc) + faceval*d2u(3,iloc(1:nloc),2) * dP0(faceint)
                      IF     (PLpinf==1) THEN
                         charI(1:nloc) = charI(1:nloc) + pBcoef*(1.0_pr-maxMa**2)*c0(faceint)*du(1,iloc(1:nloc),2)
                      ELSEIF (PLpinf==2) THEN
                         charI(1:nloc) = charI(1:nloc) + pBcoef*(1.0_pr-maxMa**2)*(d2u(3,iloc(1:nloc),2)*(pres_prev(iloc(1:nloc),1) - P0(faceint)) + d2u(1,iloc(1:nloc),2)*du(1,iloc(1:nloc),2))
                      ELSEIF (PLpinf==3) THEN
                         charI(1:nloc) = charI(1:nloc) + pBcoef*(1.0_pr-maxMa**2)*c0(faceint)/ulc_prev(iloc(1:nloc),nden)/d2u(2,iloc(1:nloc),2) * du(1,iloc(1:nloc),2)
                      ELSEIF (PLpinf==4) THEN
                         charI(1:nloc) = charI(1:nloc) + pBcoef*(1.0_pr-maxMa**2) * (d2u(3,iloc(1:nloc),2)*(pres_prev(iloc(1:nloc),1) - ulc_prev(iloc(1:nloc),nden)*d2u(2,iloc(1:nloc),2)*Tconst) + &
                                                                                     d2u(1,iloc(1:nloc),2)* du(1,iloc(1:nloc),2) )
                      ELSEIF (PLpinf==6) THEN
                         charI(1:nloc) = charI(1:nloc) - faceval*pBcoef*(1.0_pr-maxMa**2)*d2u(1,iloc(1:nloc),2) * &
                                                         (2.0_pr*ulc_prev(iloc(1:nloc),nden)*d2u(3,iloc(1:nloc),2)*pres_prev(iloc(1:nloc),2))
                      END IF
                      charX(1:nloc,1) = 0.5_pr/d2u(1,iloc(1:nloc),2)**2*charI(1:nloc) - d2u(3,iloc(1:nloc),2)/d2u(1,iloc(1:nloc),2)**3*charI_prev(1:nloc)
                      charX(1:nloc,2) = -faceval*0.5_pr/ulc_prev(iloc(1:nloc),nden)/d2u(1,iloc(1:nloc),2)*(charI(1:nloc) - charI_prev(1:nloc)*d2u(3,iloc(1:nloc),2)/d2u(1,iloc(1:nloc),2))
                      charX(1:nloc,3) = 0.5_pr*charI(1:nloc)
                      Drhs_diag_local((jj-1)*mynloc+iloc(1:nloc)) = Drhs_diag_local((jj-1)*mynloc+iloc(1:nloc)) + 0.5_pr*SUM(ulc_prev(iloc(1:nloc),nvel(1):nvel(dim))**2,DIM=2) / &
                                        ulc_prev(iloc(1:nloc),nden)**2*charX(1:nloc,1) + ulc_prev(iloc(1:nloc),nvel(1))*charX(1:nloc,2) + &
                                        1.0_pr/(d2u(1,iloc(1:nloc),1)-1.0_pr)*charX(1:nloc,3)
                   ELSE IF (jj>=nspc_lil .AND. jj<=nspc_big) THEN
                      du (2,iloc(1:nloc),2) = 0.0_pr !u_diag
                      d2u(3,iloc(1:nloc),1) = (d2u(2,iloc(1:nloc),1)/MW_in(jj-nspc(1)+1) - d2u(2,iloc(1:nloc),2)*cp_in(jj-nspc(1)+1)) / &
                                                    ulc_prev(iloc(1:nloc),nden)/(d2u(2,iloc(1:nloc),1)-d2u(2,iloc(1:nloc),2))**2  !gamma_diag
                      du (1,iloc(1:nloc),2) = (ulc_prev(iloc(1:nloc),neng) - 0.5_pr*SUM(ulc_prev(iloc(1:nloc),nvel(1):nvel(dim))**2,DIM=2) / &
                                                    ulc_prev(iloc(1:nloc),nden))*d2u(3,iloc(1:nloc),1) !p_diag
                      d2u(3,iloc(1:nloc),2) = 0.5_pr/d2u(1,iloc(1:nloc),2)/ulc_prev(iloc(1:nloc),nden) * &
                                                    (d2u(3,iloc(1:nloc),1)*pres_prev(iloc(1:nloc),1)+d2u(1,iloc(1:nloc),1)*du(1,iloc(1:nloc),2)) !c_diag
                      charI(1:nloc) = -faceval*d2u(3,iloc(1:nloc),2)*(du(1,iloc(1:nloc),1)-faceval*ulc_prev(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)*du(2,iloc(1:nloc),1)) + &
                                      charV_prev(1:nloc)*(du_diag_PFB(iloc(1:nloc))*du(1,iloc(1:nloc),2) - faceval*du(2,iloc(1:nloc),1)*ulc_prev(iloc(1:nloc),nden)*d2u(3,iloc(1:nloc),2))
                      IF (pertchar) charI(1:nloc) = charI(1:nloc) + faceval*d2u(3,iloc(1:nloc),2) * dP0(faceint)
                      IF     (PLpinf==1) THEN
                         charI(1:nloc) = charI(1:nloc) + pBcoef*(1.0_pr-maxMa**2)*c0(faceint)*du(1,iloc(1:nloc),2)
                      ELSEIF (PLpinf==2) THEN
                         charI(1:nloc) = charI(1:nloc) + pBcoef*(1.0_pr-maxMa**2)*(d2u(3,iloc(1:nloc),2)*(pres_prev(iloc(1:nloc),1) - P0(faceint)) + d2u(1,iloc(1:nloc),2)*du(1,iloc(1:nloc),2))
                      ELSEIF (PLpinf==3) THEN
                         charI(1:nloc) = charI(1:nloc) + pBcoef*(1.0_pr-maxMa**2)*c0(faceint)/ulc_prev(iloc(1:nloc),nden)/d2u(2,iloc(1:nloc),2) * &
                                                         (du(1,iloc(1:nloc),2) - pres_prev(iloc(1:nloc),1)/MW_in(faceint)/ulc_prev(iloc(1:nloc),nden)/d2u(2,iloc(1:nloc),2) ) 
                      ELSEIF (PLpinf==4) THEN
                         charI(1:nloc) = charI(1:nloc) + pBcoef*(1.0_pr-maxMa**2) * (d2u(3,iloc(1:nloc),2)*(pres_prev(iloc(1:nloc),1) - ulc_prev(iloc(1:nloc),nden)*d2u(2,iloc(1:nloc),2)*Tconst)+ &
                                                                                     d2u(1,iloc(1:nloc),2)*(du(1,iloc(1:nloc),2) - Tconst/MW_in(faceint)) )
                      ELSEIF (PLpinf==6) THEN
                         charI(1:nloc) = charI(1:nloc) - faceval*pBcoef*(1.0_pr-maxMa**2)*d2u(1,iloc(1:nloc),2) * &
                                                         (2.0_pr*ulc_prev(iloc(1:nloc),nden)*d2u(3,iloc(1:nloc),2)*pres_prev(iloc(1:nloc),2))
                      END IF
                      charX(1:nloc,1) = 0.5_pr/d2u(1,iloc(1:nloc),2)**2*charI(1:nloc) - d2u(3,iloc(1:nloc),2)/d2u(1,iloc(1:nloc),2)**3*charI_prev(1:nloc) 
                      Drhs_diag_local((jj-1)*mynloc+iloc(1:nloc)) = Drhs_diag_local((jj-1)*mynloc+iloc(1:nloc)) + (charX_prev(1:nloc)+ulc_prev(iloc(1:nloc),jj)*charX(1:nloc,1)) / &
                                                     ulc_prev(iloc(1:nloc),nden)
                   END IF 
                END DO
                IF (ALLOCATED(charI)) DEALLOCATE(charI)
                IF (ALLOCATED(charX)) DEALLOCATE(charX)
                IF (ALLOCATED(charV_prev)) DEALLOCATE(charV_prev)
                IF (ALLOCATED(charI_prev)) DEALLOCATE(charI_prev)
                IF (ALLOCATED(charX_prev)) DEALLOCATE(charX_prev)
             END IF
          END IF
       END IF
    END DO
  END SUBROUTINE user_evol_LODIsimp_BC_Drhs_diag 

  SUBROUTINE user_evol_LODIavg_BC_rhs (myrhs_loc, ulc, myne_loc, mynloc, myjl, mymeth)
    !--Sets rhs for boundary conditions
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: myne_loc, mynloc, myjl, mymeth
    REAL (pr), DIMENSION (mynloc*myne_loc), INTENT (INOUT) :: myrhs_loc
    REAL (pr), DIMENSION (mynloc,myne_loc), INTENT (IN) :: ulc

    INTEGER :: ie, i, ii, shift, jj
    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc

    REAL (pr), DIMENSION (:),     ALLOCATABLE :: temprhs_loc
    REAL (pr), DIMENSION (:,:),   ALLOCATABLE :: pres
    REAL (pr), DIMENSION (:,:,:), ALLOCATABLE :: du, d2u
    REAL (pr), DIMENSION (:,:),   ALLOCATABLE :: charI,charX

    REAL (pr), DIMENSION (6) :: charVavg
    REAL (pr) :: faceval
    INTEGER :: l, faceint, nloc_glob

    IF (ALLOCATED(temprhs_loc)) DEALLOCATE(temprhs_loc)
    ALLOCATE(temprhs_loc(myne_loc*mynloc))
    CALL internal_rhs(temprhs_loc,ulc,1,0)
    i_p_face(0) = 1
    DO i=1,dim
       i_p_face(i) = i_p_face(i-1)*3
    END DO
    DO face_type = 0, 3**dim - 1
       face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
       IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
          CALL get_all_indices_by_face (face_type, myjl, nloc, iloc)
          IF(nloc > 0 ) THEN
             IF (ABS(face(1)) == 1) THEN
                DO jj=1,myne_loc
                   myrhs_loc((jj-1)*mynloc+iloc(1:nloc)) = temprhs_loc((jj-1)*mynloc+iloc(1:nloc))  !from internal_rhs
                END DO
             END IF
          END IF
       END IF
    END DO
    IF (ALLOCATED(temprhs_loc)) DEALLOCATE(temprhs_loc) 

    IF (ALLOCATED(pres)) DEALLOCATE(pres) 
    IF (ALLOCATED(du)) DEALLOCATE(du) 
    IF (ALLOCATED(d2u)) DEALLOCATE(d2u) 
    ALLOCATE(pres(mynloc,locnvar),du(locnvar,mynloc,dim),d2u(locnvar,mynloc,dim))

    d2u(1,:,1) = 1.0_pr
    IF (Nspec>1) d2u(1,:,1) = d2u(1,:,1)-SUM(ulc(:,nspc(1):nspc(Nspecm)),DIM=2)/ulc(:,nden) !Y_Nspec
    d2u(2,:,1) = d2u(1,:,1)*cp_in(Nspec)  !cp_Nspec
    d2u(2,:,2) = d2u(1,:,1)/MW_in(Nspec)  !R_Nspec
    DO l=1,Nspecm
       d2u(2,:,1)  = d2u(2,:,1)  + ulc(:,nspc(l))/ulc(:,nden)*cp_in(l)  !cp
       d2u(2,:,2)  = d2u(2,:,2)  + ulc(:,nspc(l))/ulc(:,nden)/MW_in(l)  !R
    END DO
    d2u(1,:,1) = d2u(2,:,1)/(d2u(2,:,1)-d2u(2,:,2))  !gamma=cp/(cp-R)
    pres(:,1) = (ulc(:,neng)-0.5_pr*SUM(ulc(:,nvel(1):nvel(dim))**2,DIM=2)/ulc(:,nden))*(d2u(1,:,1)-1.0_pr)  !1: pressure
    d2u(1,:,2) = SQRT(d2u(1,:,1)*pres(:,1)/ulc(:,nden))  !c=SQRT(gamma*P/rho)
    DO i=1,dim
       pres(:,1+i) = ulc(:,nvel(i))/ulc(:,nden)  !2--dim+1: u,v(,w)
    END DO
    DO i=1,Nspecm
       pres(:,dim+1+i) = ulc(:,nspc(i))/ulc(:,nden)  !dim+2--dim+Nspec: Y_l
    END DO 
    pres(:,locnvar) = ulc(:,nden) !dim+Nspec+1: rho

    IF ((methpd .ne. 0) .OR. splitFBpress) THEN 
       du(1,:,1) = pres(:,1)
       IF (Nspec>1) THEN
          CALL dpdx (du(1,:,1),ng,pres(:,dim+2:dim+Nspec),2*methpd+mymeth,myjl,splitFBpress)
       ELSE
          CALL dpdx (du(1,:,1),ng,pres(:,1),2*methpd+mymeth,myjl,.FALSE.)
       END IF
       CALL c_diff_fast(pres(:,2:locnvar), du(2:locnvar,:,:), d2u(2:locnvar,:,:), myjl, mynloc, mymeth, 10, locnvar-1, 1, locnvar-1)  
    ELSE
       CALL c_diff_fast(pres(:,1:locnvar), du(1:locnvar,:,:), d2u(1:locnvar,:,:), myjl, mynloc, mymeth, 10, locnvar, 1, locnvar) 
    END IF

    i_p_face(0) = 1
    DO i=1,dim
       i_p_face(i) = i_p_face(i-1)*3
    END DO
    DO face_type = 0, 3**dim - 1
       face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
       IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
          CALL get_all_indices_by_face (face_type, myjl, nloc, iloc)
          IF (ABS(face(1)) == 1) THEN
             faceval=REAL(face(1),pr)
             faceint=(3-face(1))/2  
             charVavg(:) = 0.0_pr
             nloc_glob = nloc
             IF(nloc > 0 ) THEN
                charVavg(1) = SUM(pres(iloc(1:nloc),2) - d2u(1,iloc(1:nloc),2))  !(u-c)
                charVavg(2) = SUM(pres(iloc(1:nloc),2))                          !(u)
                charVavg(3) = SUM(pres(iloc(1:nloc),2) + d2u(1,iloc(1:nloc),2))  !(u+c)
             END IF
             CALL parallel_global_sum(INTEGER=nloc_glob)
             IF (nloc_glob > 0) THEN
                IF (LODIset) THEN
                   DO ii=1,3
                      CALL parallel_global_sum(REAL=charVavg(ii))
                      charVavg(ii) = charVavg(ii)/REAL(nloc_glob,pr)
                   END DO
                   charVavg(2-face(1)) = 0.0_pr
                   charVavg(5-face(1)) = 1.0_pr
                ELSE
                   DO ii=1,3
                      CALL parallel_global_sum(REAL=charVavg(ii))
                      charVavg(ii) = charVavg(ii)/REAL(nloc_glob,pr)
                      IF (charVavg(ii)*faceval .le. 0.0_pr) charVavg(ii+3) = 1.0_pr
                      charVavg(ii) = faceval*MAX(0.0_pr,faceval*charVavg(ii)) 
                   END DO
                END IF
             END IF
             IF(nloc > 0 ) THEN
                IF (ALLOCATED(charI)) DEALLOCATE(charI)
                IF (ALLOCATED(charX)) DEALLOCATE(charX)
                ALLOCATE(charI(nloc,locnvar),charX(nloc,locnvar))

                charI(1:nloc,2) = charVavg(2)*(d2u(1,iloc(1:nloc),2)**2*du(locnvar,iloc(1:nloc),1) - du(1,iloc(1:nloc),1))  !u*(c^2*drho/dx - dp/dx)
                DO ii=3,dim+Nspec
                   charI(1:nloc,ii) = charVavg(2)*du(ii,iloc(1:nloc),1)
                END DO
                charI(1:nloc,locnvar) = charVavg(3)*(du(1,iloc(1:nloc),1) + ulc(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)*du(2,iloc(1:nloc),1))  !(u+c)*(dp/dx+rho*c*du/dx)
                charI(1:nloc,1)       = charVavg(1)*(du(1,iloc(1:nloc),1) - ulc(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)*du(2,iloc(1:nloc),1))  !(u-c)*(dp/dx-rho*c*du/dx)
                IF (pertchar) THEN   
                   charI(1:nloc,locnvar) = charI(1:nloc,locnvar) - charVavg(3) * dP0(faceint)
                   charI(1:nloc,1)       = charI(1:nloc,1)       - charVavg(1) * dP0(faceint)
                   charI(1:nloc,2)       = charI(1:nloc,2)       - charVavg(2) * (d2u(1,iloc(1:nloc),2)**2*drho0(faceint) - dP0(faceint))
                END IF
                IF     (PLpinf==1) THEN
                   charI(1:nloc,locnvar) = charI(1:nloc,locnvar) + charVavg(6)*pBcoef*(1.0_pr-maxMa**2)*c0(faceint)          *(pres(iloc(1:nloc),1) - P0(faceint))
                   charI(1:nloc,1)       = charI(1:nloc,1)       + charVavg(4)*pBcoef*(1.0_pr-maxMa**2)*c0(faceint)          *(pres(iloc(1:nloc),1) - P0(faceint))
                ELSEIF (PLpinf==2) THEN
                   charI(1:nloc,locnvar) = charI(1:nloc,locnvar) + charVavg(6)*pBcoef*(1.0_pr-maxMa**2)*d2u(1,iloc(1:nloc),2)*(pres(iloc(1:nloc),1) - P0(faceint))
                   charI(1:nloc,1)       = charI(1:nloc,1)       + charVavg(4)*pBcoef*(1.0_pr-maxMa**2)*d2u(1,iloc(1:nloc),2)*(pres(iloc(1:nloc),1) - P0(faceint))
                ELSEIF (PLpinf==3) THEN
                   charI(1:nloc,locnvar) = charI(1:nloc,locnvar) + charVavg(6)*pBcoef*(1.0_pr-maxMa**2)*c0(faceint)*(pres(iloc(1:nloc),1)/ulc(iloc(1:nloc),nden)/d2u(2,iloc(1:nloc),2) - Tconst)
                   charI(1:nloc,1)       = charI(1:nloc,1)       + charVavg(4)*pBcoef*(1.0_pr-maxMa**2)*c0(faceint)*(pres(iloc(1:nloc),1)/ulc(iloc(1:nloc),nden)/d2u(2,iloc(1:nloc),2) - Tconst)
                ELSEIF (PLpinf==4) THEN
                   charI(1:nloc,locnvar) = charI(1:nloc,locnvar) + charVavg(6)*pBcoef*(1.0_pr-maxMa**2)*d2u(1,iloc(1:nloc),2) * &
                                                                         (pres(iloc(1:nloc),1) - ulc(iloc(1:nloc),nden)*d2u(2,iloc(1:nloc),2)*Tconst)
                   charI(1:nloc,1)       = charI(1:nloc,1)       + charVavg(4)*pBcoef*(1.0_pr-maxMa**2)*d2u(1,iloc(1:nloc),2) * &
                                                                         (pres(iloc(1:nloc),1) - ulc(iloc(1:nloc),nden)*d2u(2,iloc(1:nloc),2)*Tconst)
                ELSEIF (PLpinf==5) THEN
                   charI(1:nloc,locnvar) = charI(1:nloc,locnvar) - faceval*charVavg(6)*pBcoef*(1.0_pr-maxMa**2)*gammBC(faceint)*Pint*pres(iloc(1:nloc),2)
                   charI(1:nloc,1)       = charI(1:nloc,1)       - faceval*charVavg(4)*pBcoef*(1.0_pr-maxMa**2)*gammBC(faceint)*Pint*pres(iloc(1:nloc),2)
                ELSEIF (PLpinf==6) THEN
                   charI(1:nloc,locnvar) = charI(1:nloc,locnvar) - faceval*charVavg(6)*pBcoef*(1.0_pr-maxMa**2)*ulc(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)**2*pres(iloc(1:nloc),2)
                   charI(1:nloc,1)       = charI(1:nloc,1)       - faceval*charVavg(4)*pBcoef*(1.0_pr-maxMa**2)*ulc(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)**2*pres(iloc(1:nloc),2)
                END IF

                charX(1:nloc,1) = 0.5_pr/d2u(1,iloc(1:nloc),2)**2*(charI(1:nloc,1)+charI(1:nloc,locnvar)+2.0_pr*charI(1:nloc,2))
                charX(1:nloc,2) = 0.5_pr/d2u(1,iloc(1:nloc),2)/ulc(iloc(1:nloc),nden)*(charI(1:nloc,locnvar)-charI(1:nloc,1))
                DO ii=3,dim+Nspec
                   charX(1:nloc,ii) = charI(1:nloc,ii)
                END DO
                charX(1:nloc,locnvar) = 0.5_pr*(charI(1:nloc,locnvar)+charI(1:nloc,1))
                IF (pertchar) THEN   
                   charX(1:nloc,1)       = charX(1:nloc,1)       + pres(iloc(1:nloc),2)*drho0(faceint)
                   IF (usedP0) THEN 
                      charX(1:nloc,2)    = charX(1:nloc,2)       + dP0(faceint)/ulc(iloc(1:nloc),nden)
                   ELSE
                      charX(1:nloc,2)    = charX(1:nloc,2)       - rhoYg(faceint)/ulc(iloc(1:nloc),nden)
                   END IF
                   charX(1:nloc,locnvar) = charX(1:nloc,locnvar) + dP0(faceint)*pres(iloc(1:nloc),2)
                END IF

                myrhs_loc((nden-1)*mynloc+iloc(1:nloc)) = myrhs_loc((nden-1)*mynloc+iloc(1:nloc)) - charX(1:nloc,1)
                DO ii=nvel(1),nvel(dim)
                   myrhs_loc((ii-1)*mynloc+iloc(1:nloc)) = myrhs_loc((ii-1)*mynloc+iloc(1:nloc)) - pres(iloc(1:nloc),ii-nvel(1)+2)*charX(1:nloc,1) &
                                                                                                 - ulc(iloc(1:nloc),nden)*charX(1:nloc,ii-nvel(1)+2)
                END DO
                myrhs_loc((neng-1)*mynloc+iloc(1:nloc)) = myrhs_loc((neng-1)*mynloc+iloc(1:nloc)) - 0.5_pr*SUM(pres(iloc(1:nloc),2:dim+1)**2,DIM=2)*charX(1:nloc,1) &
                             - SUM(ulc(iloc(1:nloc),nvel(1):nvel(dim))*charX(1:nloc,2:dim+1),DIM=2) - 1.0_pr/(d2u(1,iloc(1:nloc),1)-1.0_pr)*charX(1:nloc,locnvar) 
                DO ii=1,Nspecm
                   myrhs_loc((nspc(ii)-1)*mynloc+iloc(1:nloc)) = myrhs_loc((nspc(ii)-1)*mynloc+iloc(1:nloc)) - pres(iloc(1:nloc),ii+dim+1)*charX(1:nloc,1) &
                                                                                                 - ulc(iloc(1:nloc),nden)*charX(1:nloc,ii+dim+1)
                END DO
                IF (ALLOCATED(charI)) DEALLOCATE(charI)
                IF (ALLOCATED(charX)) DEALLOCATE(charX) 
             END IF
          END IF
       END IF
    END DO
    IF (ALLOCATED(pres)) DEALLOCATE(pres) 
    IF (ALLOCATED(du)) DEALLOCATE(du) 
    IF (ALLOCATED(d2u)) DEALLOCATE(d2u) 
  END SUBROUTINE user_evol_LODIavg_BC_rhs 

  SUBROUTINE user_evol_LODIavg_BC_Drhs (Dmyrhs_loc, ulc, ulc_prev, myne_loc, mynloc, myjl, mymeth)
    !--Sets rhs for boundary conditions
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: myne_loc, mynloc, myjl, mymeth
    REAL (pr), DIMENSION (mynloc*myne_loc), INTENT (INOUT) :: Dmyrhs_loc
    REAL (pr), DIMENSION (mynloc,myne_loc), INTENT (IN) :: ulc, ulc_prev

    INTEGER :: ie, i, ii, shift, jj
    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc

    REAL (pr), DIMENSION (:),     ALLOCATABLE :: tempDrhs_loc
    REAL (pr), DIMENSION (:,:),   ALLOCATABLE :: pres, pres_prev
    REAL (pr), DIMENSION (:,:,:), ALLOCATABLE :: du_prev, du, d2u
    REAL (pr), DIMENSION (:,:),   ALLOCATABLE :: charI, charX, charI_prev, charX_prev
    
    REAL (pr), DIMENSION (6) :: charVavg_prev
    REAL (pr), DIMENSION (3) :: charVavg
    REAL (pr) :: faceval
    INTEGER :: l, faceint, nloc_glob

    IF (ALLOCATED(tempDrhs_loc)) DEALLOCATE(tempDrhs_loc)
    ALLOCATE(tempDrhs_loc(myne_loc*mynloc))
    CALL internal_Drhs(tempDrhs_loc,ulc,ulc_prev,mymeth,1,0)
    i_p_face(0) = 1
    DO i=1,dim
       i_p_face(i) = i_p_face(i-1)*3
    END DO
    DO face_type = 0, 3**dim - 1
       face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
       IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
          CALL get_all_indices_by_face (face_type, myjl, nloc, iloc)
          IF(nloc > 0 ) THEN
             IF (ABS(face(1)) == 1) THEN
                DO jj=1,myne_loc
                   Dmyrhs_loc((jj-1)*mynloc+iloc(1:nloc)) = tempDrhs_loc((jj-1)*mynloc+iloc(1:nloc))  !from internal_Drhs
                END DO
             END IF
          END IF
       END IF
    END DO
    IF (ALLOCATED(tempDrhs_loc)) DEALLOCATE(tempDrhs_loc) 

    IF (ALLOCATED(pres)) DEALLOCATE(pres) 
    IF (ALLOCATED(pres_prev)) DEALLOCATE(pres_prev) 
    IF (ALLOCATED(du)) DEALLOCATE(du) 
    IF (ALLOCATED(du_prev)) DEALLOCATE(du_prev) 
    IF (ALLOCATED(d2u)) DEALLOCATE(d2u) 
    ALLOCATE(pres(mynloc,locnvar),pres_prev(mynloc,locnvar),du(locnvar,mynloc,dim),du_prev(locnvar,mynloc,dim),d2u(locnvar,mynloc,dim))

    d2u(1,:,1) = 1.0_pr
    d2u(1,:,2) = 0.0_pr
    IF (Nspec>1) d2u(1,:,1) = d2u(1,:,1)-SUM(ulc_prev(:,nspc(1):nspc(Nspecm)),DIM=2)/ulc_prev(:,nden) !Y0_Nspec
    IF (Nspec>1) d2u(1,:,2) = (SUM(ulc_prev(:,nspc(1):nspc(Nspecm)),DIM=2)*ulc(:,nden)/ulc_prev(:,nden) - SUM(ulc(:,nspc(1):nspc(Nspecm)),DIM=2))/ulc_prev(:,nden) !Y'_Nspec 
    d2u(4,:,1) = d2u(1,:,2)*cp_in(Nspec)  !cp'_Nspec
    d2u(4,:,2) = d2u(1,:,2)/MW_in(Nspec)  !R'_Nspec
    d2u(2,:,1) = d2u(1,:,1)*cp_in(Nspec)  !cp0_Nspec
    d2u(2,:,2) = d2u(1,:,1)/MW_in(Nspec)  !R0_Nspec
    DO l=1,Nspecm
       d2u(2,:,1)  = d2u(2,:,1)  + ulc_prev(:,nspc(l))/ulc_prev(:,nden)*cp_in(l)  !cp0
       d2u(2,:,2)  = d2u(2,:,2)  + ulc_prev(:,nspc(l))/ulc_prev(:,nden)/MW_in(l)  !R0
       d2u(4,:,1)  = d2u(4,:,1)  + (ulc(:,nspc(l))-ulc_prev(:,nspc(l))/ulc_prev(:,nden)*ulc(:,nden))/ulc_prev(:,nden)*cp_in(l)  !cp'
       d2u(4,:,2)  = d2u(4,:,2)  + (ulc(:,nspc(l))-ulc_prev(:,nspc(l))/ulc_prev(:,nden)*ulc(:,nden))/ulc_prev(:,nden)/MW_in(l)  !R'
    END DO
    d2u(1,:,1) = d2u(2,:,1)/(d2u(2,:,1)-d2u(2,:,2))  !gamma0=cp0/(cp0-R0)
    pres_prev(:,1) = (ulc_prev(:,neng)-0.5_pr*SUM(ulc_prev(:,nvel(1):nvel(dim))**2,DIM=2)/ulc_prev(:,nden))*(d2u(1,:,1)-1.0_pr)  !1: p0
    d2u(1,:,2) = SQRT(d2u(1,:,1)*pres_prev(:,1)/ulc_prev(:,nden))  !c0=SQRT(gamma0*P0/rho0) 
    d2u(3,:,1) = (d2u(2,:,1)*d2u(4,:,2) - d2u(4,:,1)*d2u(2,:,2)) / (d2u(2,:,1)-d2u(2,:,2))**2  !gamma'=(cp0*R'-cp'*R0)/(cp0-R0)^2
    pres(:,1) =  d2u(3,:,1)*(ulc_prev(:,neng)-0.5_pr*SUM(ulc_prev(:,nvel(1):nvel(dim))**2,DIM=2))/ulc_prev(:,nden) +  &
                (d2u(1,:,1)-1.0_pr)*(ulc(:,neng)+0.5_pr*SUM(ulc_prev(:,nvel(1):nvel(dim))**2,DIM=2)/ulc_prev(:,nden)**2*ulc(:,nden) -  &
                 SUM(ulc_prev(:,nvel(1):nvel(dim))*ulc(:,nvel(1):nvel(dim)),DIM=2)/ulc_prev(:,nden) )  !1: p'
    d2u(3,:,2) = 0.5_pr/SQRT(d2u(1,:,1)*pres_prev(:,1)/ulc_prev(:,nden)) * ( d2u(1,:,1)*pres(:,1)+d2u(3,:,1)*pres_prev(:,1) - &
                        d2u(1,:,1)*pres_prev(:,1)/ulc_prev(:,nden)*ulc(:,nden) )/ulc_prev(:,nden)   !c'=1/2/SQRT(gamma0*P0/rho0)*(gamma*P/rho)'
    DO i=1,dim
       pres_prev(:,1+i) = ulc_prev(:,nvel(i))/ulc_prev(:,nden)  !2--dim+1: u0
       pres(:,1+i) = (ulc(:,nvel(i)) - ulc_prev(:,nvel(i))*ulc(:,nden)/ulc_prev(:,nden) )/ulc_prev(:,nden) !2--dim+1: u'
    END DO
    DO i=1,Nspecm
       pres_prev(:,dim+1+i) = ulc_prev(:,nspc(i))/ulc_prev(:,nden)  !dim+2--dim+Nspec: Y0
       pres(:,dim+1+i) = (ulc(:,nspc(i)) - ulc_prev(:,nspc(i))*ulc(:,nden)/ulc_prev(:,nden) )/ulc_prev(:,nden) !dim+2--dim+Nspec: Y'
    END DO 
    pres_prev(:,locnvar) = ulc_prev(:,nden) !dim+Nspec+1: rho0
    pres(:,locnvar) = ulc(:,nden) !dim+Nspec+1: rho'

    IF ((methpd .ne. 0) .OR. splitFBpress) THEN 
       du(1,:,1) = pres(:,1)
       IF (Nspec>1) THEN
          CALL dpdx (du(1,:,1),ng,pres_prev(:,dim+2:dim+Nspec),2*methpd+mymeth,myjl,splitFBpress)
       ELSE
          CALL dpdx (du(1,:,1),ng,pres_prev(:,1),2*methpd+mymeth,myjl,.FALSE.)
       END IF
       CALL c_diff_fast(pres(:,2:locnvar), du(2:locnvar,:,:), d2u(2:locnvar,:,:), myjl, mynloc, mymeth, 10, locnvar-1, 1, locnvar-1)  
    ELSE
       CALL c_diff_fast(pres(:,1:locnvar), du(1:locnvar,:,:), d2u(1:locnvar,:,:), myjl, mynloc, mymeth, 10, locnvar, 1, locnvar) 
    END IF
    IF ((methpd .ne. 0) .OR. splitFBpress) THEN 
       du_prev(1,:,1) = pres_prev(:,1)
       IF (Nspec>1) THEN
          CALL dpdx (du_prev(1,:,1),ng,pres_prev(:,dim+2:dim+Nspec),2*methpd+mymeth,myjl,splitFBpress)
       ELSE
          CALL dpdx (du_prev(1,:,1),ng,pres_prev(:,1),2*methpd+mymeth,myjl,.FALSE.)
       END IF
       CALL c_diff_fast(pres_prev(:,2:locnvar), du_prev(2:locnvar,:,:), d2u(2:locnvar,:,:), myjl, mynloc, mymeth, 10, locnvar-1, 1, locnvar-1)  
    ELSE
       CALL c_diff_fast(pres_prev(:,1:locnvar), du_prev(1:locnvar,:,:), d2u(1:locnvar,:,:), myjl, mynloc, mymeth, 10, locnvar  , 1, locnvar  ) 
    END IF

    i_p_face(0) = 1
    DO i=1,dim
       i_p_face(i) = i_p_face(i-1)*3
    END DO
    DO face_type = 0, 3**dim - 1
       face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
       IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
          CALL get_all_indices_by_face (face_type, myjl, nloc, iloc)
          IF (ABS(face(1)) == 1) THEN
             faceval=REAL(face(1),pr)
             faceint=(3-face(1))/2  
             charVavg_prev(:) = 0.0_pr
             charVavg(:) = 0.0_pr
             nloc_glob = nloc
             IF(nloc > 0 ) THEN
                charVavg_prev(1) = SUM(pres_prev(iloc(1:nloc),2) - d2u(1,iloc(1:nloc),2))  !(u-c)
                charVavg_prev(2) = SUM(pres_prev(iloc(1:nloc),2))                          !(u)
                charVavg_prev(3) = SUM(pres_prev(iloc(1:nloc),2) + d2u(1,iloc(1:nloc),2))  !(u+c)
                charVavg(1) = SUM(pres(iloc(1:nloc),2) - d2u(3,iloc(1:nloc),2))  !(u-c)
                charVavg(2) = SUM(pres(iloc(1:nloc),2))                          !(u)
                charVavg(3) = SUM(pres(iloc(1:nloc),2) + d2u(3,iloc(1:nloc),2))  !(u+c)
             END IF
             CALL parallel_global_sum(INTEGER=nloc_glob)
             IF (nloc_glob > 0) THEN
                IF (LODIset) THEN
                   DO ii=1,3
                      CALL parallel_global_sum(REAL=charVavg_prev(ii))
                      CALL parallel_global_sum(REAL=charVavg(ii))
                      charVavg_prev(ii) = charVavg_prev(ii)/REAL(nloc_glob,pr)
                      charVavg(ii) = charVavg(ii)/REAL(nloc_glob,pr)
                   END DO
                   charVavg_prev(2-face(1)) = 0.0_pr
                   charVavg(2-face(1)) = 0.0_pr
                   charVavg_prev(5-face(1)) = 1.0_pr
                ELSE
                   DO ii=1,3
                      CALL parallel_global_sum(REAL=charVavg_prev(ii))
                      CALL parallel_global_sum(REAL=charVavg(ii))
                      charVavg_prev(ii) = charVavg_prev(ii)/REAL(nloc_glob,pr)
                      charVavg(ii) = charVavg(ii)/REAL(nloc_glob,pr)
                      IF (charVavg_prev(ii)*faceval .le. 0.0_pr) charVavg_prev(ii+3) = 1.0_pr
                      IF (charVavg_prev(ii)*faceval .le. 0.0_pr) charVavg(ii) = 0.0_pr
                      charVavg_prev(ii) = faceval*MAX(0.0_pr,faceval*charVavg_prev(ii)) 
                   END DO
                END IF
             END IF
             IF(nloc > 0 ) THEN
                IF (ALLOCATED(charI)) DEALLOCATE(charI)
                IF (ALLOCATED(charX)) DEALLOCATE(charX)
                IF (ALLOCATED(charI_prev)) DEALLOCATE(charI_prev)
                IF (ALLOCATED(charX_prev)) DEALLOCATE(charX_prev)
                ALLOCATE(charI(nloc,locnvar),charX(nloc,locnvar),charI_prev(nloc,locnvar),charX_prev(nloc,locnvar))

                charI(1:nloc,2) = charVavg_prev(2)*(d2u(1,iloc(1:nloc),2)**2*du(locnvar,iloc(1:nloc),1) - du(1,iloc(1:nloc),1))
                DO ii=3,dim+Nspec
                   charI(1:nloc,ii) = charVavg_prev(2)*du(ii,iloc(1:nloc),1)
                END DO
                charI(1:nloc,locnvar) = charVavg_prev(3)*(du(1,iloc(1:nloc),1) + ulc_prev(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)*du(2,iloc(1:nloc),1)) 
                charI(1:nloc,1) =       charVavg_prev(1)*(du(1,iloc(1:nloc),1) - ulc_prev(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)*du(2,iloc(1:nloc),1)) 

                charI_prev(1:nloc,2) = charVavg_prev(2)*(d2u(1,iloc(1:nloc),2)**2*du_prev(locnvar,iloc(1:nloc),1) - du_prev(1,iloc(1:nloc),1)) !u*(c^2*drho/dx - dp/dx)
                DO ii=3,dim+Nspec
                   charI_prev(1:nloc,ii) = charVavg_prev(2)*du_prev(ii,iloc(1:nloc),1)
                END DO
                charI_prev(1:nloc,locnvar) = charVavg_prev(3)*(du_prev(1,iloc(1:nloc),1)+ulc_prev(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)*du_prev(2,iloc(1:nloc),1))  !(u+c)*(dp/dx+rho*c*du/dx)
                charI_prev(1:nloc,1)       = charVavg_prev(1)*(du_prev(1,iloc(1:nloc),1)-ulc_prev(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)*du_prev(2,iloc(1:nloc),1))  !(u-c)*(dp/dx-rho*c*du/dx)

                IF (pertchar) THEN   
                   charI_prev(1:nloc,locnvar) = charI_prev(1:nloc,locnvar) - charVavg_prev(3) * dP0(faceint)
                   charI_prev(1:nloc,1)       = charI_prev(1:nloc,1)       - charVavg_prev(1) * dP0(faceint)
                   charI_prev(1:nloc,2)       = charI_prev(1:nloc,2)       - charVavg_prev(2) * (d2u(1,iloc(1:nloc),2)**2*drho0(faceint) - dP0(faceint))
                END IF

                charI(1:nloc,2) = charI(1:nloc,2) + charVavg(2)*(d2u(1,iloc(1:nloc),2)**2*du_prev(locnvar,iloc(1:nloc),1) - du_prev(1,iloc(1:nloc),1)) !u*(c^2*drho/dx - dp/dx)
                DO ii=3,dim+Nspec
                   charI(1:nloc,ii) = charI(1:nloc,ii) + charVavg(2)*du_prev(ii,iloc(1:nloc),1)
                END DO
                charI(1:nloc,locnvar) = charI(1:nloc,locnvar) + charVavg(3)*(du_prev(1,iloc(1:nloc),1) + ulc_prev(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)*du_prev(2,iloc(1:nloc),1))  
                                                                                                                                                           !(u+c)*(dp/dx+rho*c*du/dx)
                charI(1:nloc,1)       = charI(1:nloc,1)       + charVavg(1)*(du_prev(1,iloc(1:nloc),1) - ulc_prev(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)*du_prev(2,iloc(1:nloc),1))  
                                                                                                                                                           !(u-c)*(dp/dx-rho*c*du/dx)
                charI(1:nloc,2) = charI(1:nloc,2) + charVavg_prev(2)*2.0_pr*d2u(1,iloc(1:nloc),2)*d2u(3,iloc(1:nloc),2)*du_prev(locnvar,iloc(1:nloc),1)

                charI(1:nloc,locnvar) = charI(1:nloc,locnvar) + charVavg_prev(3)*du_prev(2,iloc(1:nloc),1) * &
                                                                (ulc(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)+ulc_prev(iloc(1:nloc),nden)*d2u(3,iloc(1:nloc),2)) 
                charI(1:nloc,1)       = charI(1:nloc,1)       - charVavg_prev(1)*du_prev(2,iloc(1:nloc),1) * &
                                                                (ulc(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)+ulc_prev(iloc(1:nloc),nden)*d2u(3,iloc(1:nloc),2)) 

                IF (pertchar) THEN   
                   charI(1:nloc,locnvar) = charI(1:nloc,locnvar) - charVavg(3) * dP0(faceint)
                   charI(1:nloc,1)       = charI(1:nloc,1)       - charVavg(1) * dP0(faceint)
                   charI(1:nloc,2)       = charI(1:nloc,2)       - charVavg(2) * (d2u(1,iloc(1:nloc),2)**2*drho0(faceint) - dP0(faceint)) &
                                                                 - charVavg_prev(2) * 2.0_pr*d2u(1,iloc(1:nloc),2)*d2u(3,iloc(1:nloc),2)*drho0(faceint) 
                END IF

                IF     (PLpinf==1) THEN
                   charI_prev(1:nloc,locnvar) = charI_prev(1:nloc,locnvar) + charVavg_prev(6)*pBcoef*(1.0_pr-maxMa**2)*c0(faceint)*(pres_prev(iloc(1:nloc),1) - P0(faceint))
                   charI_prev(1:nloc,1)       = charI_prev(1:nloc,1)       + charVavg_prev(4)*pBcoef*(1.0_pr-maxMa**2)*c0(faceint)*(pres_prev(iloc(1:nloc),1) - P0(faceint))
                   charI(1:nloc,locnvar)      = charI(1:nloc,locnvar)      + charVavg_prev(6)*pBcoef*(1.0_pr-maxMa**2)*c0(faceint)*pres(iloc(1:nloc),1)
                   charI(1:nloc,1)            = charI(1:nloc,1)            + charVavg_prev(4)*pBcoef*(1.0_pr-maxMa**2)*c0(faceint)*pres(iloc(1:nloc),1)
                ELSEIF (PLpinf==2) THEN
                   charI_prev(1:nloc,locnvar) = charI_prev(1:nloc,locnvar) + charVavg_prev(6)*pBcoef*(1.0_pr-maxMa**2)*d2u(1,iloc(1:nloc),2)*(pres_prev(iloc(1:nloc),1) - P0(faceint))
                   charI_prev(1:nloc,1)       = charI_prev(1:nloc,1)       + charVavg_prev(4)*pBcoef*(1.0_pr-maxMa**2)*d2u(1,iloc(1:nloc),2)*(pres_prev(iloc(1:nloc),1) - P0(faceint))
                   charI(1:nloc,locnvar)      = charI(1:nloc,locnvar)      + charVavg_prev(6)*pBcoef*(1.0_pr-maxMa**2) * &
                                                                             (d2u(3,iloc(1:nloc),2)*(pres_prev(iloc(1:nloc),1) - P0(faceint)) + d2u(1,iloc(1:nloc),2)*pres(iloc(1:nloc),1))
                   charI(1:nloc,1)            = charI(1:nloc,1)            + charVavg_prev(4)*pBcoef*(1.0_pr-maxMa**2) * &
                                                                             (d2u(3,iloc(1:nloc),2)*(pres_prev(iloc(1:nloc),1) - P0(faceint)) + d2u(1,iloc(1:nloc),2)*pres(iloc(1:nloc),1))
                ELSEIF (PLpinf==3) THEN
                   charI_prev(1:nloc,locnvar) = charI_prev(1:nloc,locnvar) + charVavg_prev(6)*pBcoef*(1.0_pr-maxMa**2)*c0(faceint) * &
                                                                              (pres_prev(iloc(1:nloc),1)/ulc_prev(iloc(1:nloc),nden)/d2u(2,iloc(1:nloc),2) - Tconst)
                   charI_prev(1:nloc,1)       = charI_prev(1:nloc,1)       + charVavg_prev(4)*pBcoef*(1.0_pr-maxMa**2)*c0(faceint) * &
                                                                              (pres_prev(iloc(1:nloc),1)/ulc_prev(iloc(1:nloc),nden)/d2u(2,iloc(1:nloc),2) - Tconst)
                   charI(1:nloc,locnvar)      = charI(1:nloc,locnvar)      + charVavg_prev(6)*pBcoef*(1.0_pr-maxMa**2)*c0(faceint)/ulc_prev(iloc(1:nloc),nden)/d2u(2,iloc(1:nloc),2) * &
                                        (pres(iloc(1:nloc),1)-pres_prev(iloc(1:nloc),1)*(ulc(iloc(1:nloc),nden)/ulc_prev(iloc(1:nloc),nden)+d2u(4,iloc(1:nloc),2)/d2u(2,iloc(1:nloc),2)) )
                   charI(1:nloc,1)            = charI(1:nloc,1)            + charVavg_prev(4)*pBcoef*(1.0_pr-maxMa**2)*c0(faceint)/ulc_prev(iloc(1:nloc),nden)/d2u(2,iloc(1:nloc),2) * &
                                        (pres(iloc(1:nloc),1)-pres_prev(iloc(1:nloc),1)*(ulc(iloc(1:nloc),nden)/ulc_prev(iloc(1:nloc),nden)+d2u(4,iloc(1:nloc),2)/d2u(2,iloc(1:nloc),2)) )
                ELSEIF (PLpinf==4) THEN
                   charI_prev(1:nloc,locnvar) = charI_prev(1:nloc,locnvar) + charVavg_prev(6)*pBcoef*(1.0_pr-maxMa**2)*d2u(1,iloc(1:nloc),2) * &
                                                                         (pres_prev(iloc(1:nloc),1) - ulc_prev(iloc(1:nloc),nden)*d2u(2,iloc(1:nloc),2)*Tconst)
                   charI_prev(1:nloc,1)       = charI_prev(1:nloc,1)       + charVavg_prev(4)*pBcoef*(1.0_pr-maxMa**2)*d2u(1,iloc(1:nloc),2) * &
                                                                         (pres_prev(iloc(1:nloc),1) - ulc_prev(iloc(1:nloc),nden)*d2u(2,iloc(1:nloc),2)*Tconst)
                   charI(1:nloc,locnvar)      = charI(1:nloc,locnvar)      + charVavg_prev(6)*pBcoef*(1.0_pr-maxMa**2) * &
                                                              (d2u(3,iloc(1:nloc),2)*(pres_prev(iloc(1:nloc),1) - ulc_prev(iloc(1:nloc),nden)*d2u(2,iloc(1:nloc),2)*Tconst) + &
                                     d2u(1,iloc(1:nloc),2)*(pres(iloc(1:nloc),1) - ulc(iloc(1:nloc),nden)*d2u(2,iloc(1:nloc),2)*Tconst - ulc_prev(iloc(1:nloc),nden)*d2u(4,iloc(1:nloc),2)*Tconst) )
                   charI(1:nloc,1)            = charI(1:nloc,1)            + charVavg_prev(4)*pBcoef*(1.0_pr-maxMa**2) * &
                                                              (d2u(3,iloc(1:nloc),2)*(pres_prev(iloc(1:nloc),1) - ulc_prev(iloc(1:nloc),nden)*d2u(2,iloc(1:nloc),2)*Tconst) + &
                                     d2u(1,iloc(1:nloc),2)*(pres(iloc(1:nloc),1) - ulc(iloc(1:nloc),nden)*d2u(2,iloc(1:nloc),2)*Tconst - ulc_prev(iloc(1:nloc),nden)*d2u(4,iloc(1:nloc),2)*Tconst) )
                ELSEIF (PLpinf==5) THEN
                   charI_prev(1:nloc,locnvar) = charI_prev(1:nloc,locnvar) - faceval*charVavg_prev(6)*pBcoef*(1.0_pr-maxMa**2)*gammBC(faceint)*Pint*pres_prev(iloc(1:nloc),2)
                   charI_prev(1:nloc,1)       = charI_prev(1:nloc,1)       - faceval*charVavg_prev(4)*pBcoef*(1.0_pr-maxMa**2)*gammBC(faceint)*Pint*pres_prev(iloc(1:nloc),2)
                   charI(1:nloc,locnvar)      = charI(1:nloc,locnvar)      - faceval*charVavg_prev(6)*pBcoef*(1.0_pr-maxMa**2)*gammBC(faceint)*Pint*pres(iloc(1:nloc),2)
                   charI(1:nloc,1)            = charI(1:nloc,1)            - faceval*charVavg_prev(4)*pBcoef*(1.0_pr-maxMa**2)*gammBC(faceint)*Pint*pres(iloc(1:nloc),2)
                ELSEIF (PLpinf==6) THEN
                   charI_prev(1:nloc,locnvar) = charI_prev(1:nloc,locnvar) - faceval*charVavg_prev(6)*pBcoef*(1.0_pr-maxMa**2) * &
                                                                                                      ulc_prev(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)**2*pres_prev(iloc(1:nloc),2)
                   charI_prev(1:nloc,1)       = charI_prev(1:nloc,1)       - faceval*charVavg_prev(4)*pBcoef*(1.0_pr-maxMa**2) * &
                                                                                                      ulc_prev(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)**2*pres_prev(iloc(1:nloc),2)
                   charI(1:nloc,locnvar)      = charI(1:nloc,locnvar)      - faceval*charVavg_prev(6)*pBcoef*(1.0_pr-maxMa**2)*d2u(1,iloc(1:nloc),2) * &
                                                   (ulc(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)*pres_prev(iloc(1:nloc),2) + &
                                                    2.0_pr*ulc_prev(iloc(1:nloc),nden)*d2u(3,iloc(1:nloc),2)*pres_prev(iloc(1:nloc),2) + &
                                                    ulc_prev(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)*pres(iloc(1:nloc),2) )
                   charI(1:nloc,1)            = charI(1:nloc,1)            - faceval*charVavg_prev(4)*pBcoef*(1.0_pr-maxMa**2)*d2u(1,iloc(1:nloc),2) * &
                                                   (ulc(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)*pres_prev(iloc(1:nloc),2) + &
                                                    2.0_pr*ulc_prev(iloc(1:nloc),nden)*d2u(3,iloc(1:nloc),2)*pres_prev(iloc(1:nloc),2) + &
                                                    ulc_prev(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)*pres(iloc(1:nloc),2) )
                END IF

                charX_prev(1:nloc,1) = 0.5_pr/d2u(1,iloc(1:nloc),2)**2*(charI_prev(1:nloc,1)+charI_prev(1:nloc,locnvar)+2.0_pr*charI_prev(1:nloc,2))
                charX_prev(1:nloc,2) = 0.5_pr/d2u(1,iloc(1:nloc),2)/ulc_prev(iloc(1:nloc),nden)*(charI_prev(1:nloc,locnvar)-charI_prev(1:nloc,1))
                DO ii=3,dim+Nspec
                   charX_prev(1:nloc,ii) = charI_prev(1:nloc,ii)
                END DO
                charX_prev(1:nloc,locnvar) = 0.5_pr*(charI_prev(1:nloc,locnvar)+charI_prev(1:nloc,1))

                IF (pertchar) THEN   
                   charX_prev(1:nloc,1)       = charX_prev(1:nloc,1)       + pres_prev(iloc(1:nloc),2)*drho0(faceint)
                   IF (usedP0) THEN 
                      charX_prev(1:nloc,2)    = charX_prev(1:nloc,2)       + dP0(faceint)/ulc_prev(iloc(1:nloc),nden)
                   ELSE
                      charX_prev(1:nloc,2)    = charX_prev(1:nloc,2)       - rhoYg(faceint)/ulc_prev(iloc(1:nloc),nden)
                   END IF
                   charX_prev(1:nloc,locnvar) = charX_prev(1:nloc,locnvar) + dP0(faceint)*pres_prev(iloc(1:nloc),2)
                END IF

                charX(1:nloc,1) = 0.5_pr/d2u(1,iloc(1:nloc),2)**2*(charI(1:nloc,1)+charI(1:nloc,locnvar)+2.0_pr*charI(1:nloc,2)) &
                                  - d2u(3,iloc(1:nloc),2)/d2u(1,iloc(1:nloc),2)**3*(charI_prev(1:nloc,1)+charI_prev(1:nloc,locnvar)+2.0_pr*charI_prev(1:nloc,2))
                charX(1:nloc,2) = 0.5_pr/d2u(1,iloc(1:nloc),2)/ulc_prev(iloc(1:nloc),nden) * &
                       (charI(1:nloc,locnvar) - charI(1:nloc,1) - (ulc(iloc(1:nloc),nden)/ulc_prev(iloc(1:nloc),nden) + d2u(3,iloc(1:nloc),2)/d2u(1,iloc(1:nloc),2)) * &
                       (charI_prev(1:nloc,locnvar)-charI_prev(1:nloc,1)))
                DO ii=3,dim+Nspec
                   charX(1:nloc,ii) = charI(1:nloc,ii)
                END DO
                charX(1:nloc,locnvar) = 0.5_pr*(charI(1:nloc,locnvar)+charI(1:nloc,1))

                IF (pertchar) THEN   
                   charX(1:nloc,1)       = charX(1:nloc,1)       + pres(iloc(1:nloc),2)*drho0(faceint)
                   IF (usedP0) THEN 
                      charX(1:nloc,2)    = charX(1:nloc,2)       - dP0(faceint)/ulc_prev(iloc(1:nloc),nden)**2*ulc(iloc(1:nloc),nden)
                   ELSE
                      charX(1:nloc,2)    = charX(1:nloc,2)       + rhoYg(faceint)/ulc_prev(iloc(1:nloc),nden)**2*ulc(iloc(1:nloc),nden)
                   END IF
                   charX(1:nloc,locnvar) = charX(1:nloc,locnvar) + dP0(faceint)*pres(iloc(1:nloc),2)
                END IF

                Dmyrhs_loc((nden-1)*mynloc+iloc(1:nloc)) = Dmyrhs_loc((nden-1)*mynloc+iloc(1:nloc)) - charX(1:nloc,1)
                DO ii=nvel(1),nvel(dim)
                   Dmyrhs_loc((ii-1)*mynloc+iloc(1:nloc)) = Dmyrhs_loc((ii-1)*mynloc+iloc(1:nloc))  & 
                                                            - pres_prev(iloc(1:nloc),ii-nvel(1)+2)*charX(1:nloc,1) - pres(iloc(1:nloc),ii-nvel(1)+2)*charX_prev(1:nloc,1) &
                                                            - ulc_prev(iloc(1:nloc),nden)*charX(1:nloc,ii-nvel(1)+2) - ulc(iloc(1:nloc),nden)*charX_prev(1:nloc,ii-nvel(1)+2)
                END DO
                Dmyrhs_loc((neng-1)*mynloc+iloc(1:nloc)) = Dmyrhs_loc((neng-1)*mynloc+iloc(1:nloc)) &
                      - 0.5_pr*SUM(pres_prev(iloc(1:nloc),2:dim+1)**2,DIM=2)*charX(1:nloc,1) - SUM(pres(iloc(1:nloc),2:dim+1)*pres_prev(iloc(1:nloc),2:dim+1),DIM=2)*charX_prev(1:nloc,1) &
                      - SUM(ulc_prev(iloc(1:nloc),nvel(1):nvel(dim))*charX(1:nloc,2:dim+1),DIM=2) - SUM(ulc(iloc(1:nloc),nvel(1):nvel(dim))*charX_prev(1:nloc,2:dim+1),DIM=2) &
                      - 1.0_pr/(d2u(1,iloc(1:nloc),1)-1.0_pr)*(charX(1:nloc,locnvar) - d2u(3,iloc(1:nloc),1)/(d2u(1,iloc(1:nloc),1)-1.0_pr)*charX_prev(1:nloc,locnvar)) 
                DO ii=1,Nspecm
                   Dmyrhs_loc((nspc(ii)-1)*mynloc+iloc(1:nloc)) = Dmyrhs_loc((nspc(ii)-1)*mynloc+iloc(1:nloc))  & 
                                                            - pres_prev(iloc(1:nloc),ii+dim+1)*charX(1:nloc,1) - pres(iloc(1:nloc),ii+dim+1)*charX_prev(1:nloc,1) &
                                                            - ulc_prev(iloc(1:nloc),nden)*charX(1:nloc,ii+dim+1) - ulc(iloc(1:nloc),nden)*charX_prev(1:nloc,ii+dim+1)
                END DO

                IF (ALLOCATED(charI)) DEALLOCATE(charI)
                IF (ALLOCATED(charX)) DEALLOCATE(charX)
                IF (ALLOCATED(charI_prev)) DEALLOCATE(charI_prev)
                IF (ALLOCATED(charX_prev)) DEALLOCATE(charX_prev)
             END IF
          END IF
       END IF
    END DO
    IF (ALLOCATED(pres)) DEALLOCATE(pres) 
    IF (ALLOCATED(pres_prev)) DEALLOCATE(pres_prev) 
    IF (ALLOCATED(du)) DEALLOCATE(du) 
    IF (ALLOCATED(du_prev)) DEALLOCATE(du_prev) 
    IF (ALLOCATED(d2u)) DEALLOCATE(d2u) 
  END SUBROUTINE user_evol_LODIavg_BC_Drhs 

  SUBROUTINE user_evol_LODIavg_BC_Drhs_diag (Drhs_diag_local, ulc_prev, myne_loc, mynloc, myjl, mymeth)
    !--Sets rhs for boundary conditions
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: myne_loc, mynloc, myjl, mymeth
    REAL (pr), DIMENSION (mynloc*myne_loc), INTENT (INOUT) :: Drhs_diag_local
    REAL (pr), DIMENSION (mynloc,myne_loc), INTENT (IN) :: ulc_prev

    INTEGER :: ie, i, ii, shift, jj
    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc

    REAL (pr), DIMENSION (:),     ALLOCATABLE :: tempdiag_loc
    REAL (pr), DIMENSION (:,:),   ALLOCATABLE :: charI, charX, charI_prev, charX_prev
    REAL (pr), DIMENSION (mynloc,dim) :: du_diag_local, d2u_diag_local
    REAL (pr), DIMENSION (mynloc) :: du_diag_PFB
    REAL (pr), DIMENSION (mynloc,Nspec+2):: pres_prev
    REAL (pr), DIMENSION (3,mynloc,dim) :: du, d2u
    REAL (pr), DIMENSION (6) :: charVavg_prev
    REAL (pr) :: faceval
    INTEGER :: l, faceint, nloc_glob

    IF (ALLOCATED(tempdiag_loc)) DEALLOCATE(tempdiag_loc)
    ALLOCATE(tempdiag_loc(myne_loc*mynloc))
    CALL internal_Drhs_diag(tempdiag_loc,ulc_prev,mymeth,1,0)
    i_p_face(0) = 1
    DO i=1,dim
       i_p_face(i) = i_p_face(i-1)*3
    END DO
    DO face_type = 0, 3**dim - 1
       face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
       IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
          CALL get_all_indices_by_face (face_type, myjl, nloc, iloc)
          IF(nloc > 0 ) THEN
             IF (ABS(face(1)) == 1) THEN
                DO jj=1,myne_loc
                   Drhs_diag_local((jj-1)*mynloc+iloc(1:nloc)) = tempdiag_loc((jj-1)*mynloc+iloc(1:nloc))  !from internal_Drhs_diag
                END DO
             END IF
          END IF
       END IF
    END DO
    IF (ALLOCATED(tempdiag_loc)) DEALLOCATE(tempdiag_loc) 

    CALL c_diff_diag(du_diag_local, d2u_diag_local, j_lev, mynloc, mymeth, mymeth, 10)

    d2u(1,:,1) = 1.0_pr
    IF (Nspec>1) d2u(1,:,1) = d2u(1,:,1)-SUM(ulc_prev(:,nspc(1):nspc(Nspecm)),DIM=2)/ulc_prev(:,nden) !Y0_Nspec
    d2u(2,:,1) = d2u(1,:,1)*cp_in(Nspec)  !cp0_Nspec
    d2u(2,:,2) = d2u(1,:,1)/MW_in(Nspec)  !R0_Nspec
    DO l=1,Nspecm
       d2u(2,:,1)  = d2u(2,:,1)  + ulc_prev(:,nspc(l))/ulc_prev(:,nden)*cp_in(l)  !cp0
       d2u(2,:,2)  = d2u(2,:,2)  + ulc_prev(:,nspc(l))/ulc_prev(:,nden)/MW_in(l)  !R0
    END DO
    d2u(1,:,1) = d2u(2,:,1)/(d2u(2,:,1)-d2u(2,:,2))  !gamma0=cp0/(cp0-R0)
    pres_prev(:,1) = (ulc_prev(:,neng)-0.5_pr*SUM(ulc_prev(:,nvel(1):nvel(dim))**2,DIM=2)/ulc_prev(:,nden))*(d2u(1,:,1)-1.0_pr)  !1: pressure
    d2u(1,:,2) = SQRT(d2u(1,:,1)*pres_prev(:,1)/ulc_prev(:,nden))  !c0=SQRT(gamma*P/rho)
    pres_prev(:,2) = ulc_prev(:,nvel(1))/ulc_prev(:,nden)  !u0
    pres_prev(:,3) = ulc_prev(:,nden)  !rho0

    IF ((methpd .ne. 0) .OR. splitFBpress) THEN 
       du(1,:,1) = pres_prev(:,1)
       DO i=1,Nspecm
          pres_prev(:,3+i) = ulc_prev(:,nspc(i))/ulc_prev(:,nden)
       END DO
       IF (Nspec>1) THEN
          CALL dpdx (du(1,:,1),ng,pres_prev(:,4:Nspec+2),2*methpd+mymeth,myjl,splitFBpress)
       ELSE
          CALL dpdx (du(1,:,1),ng,pres_prev(:,1),2*methpd+mymeth,myjl,.FALSE.)
       END IF
       CALL c_diff_fast(pres_prev(:,2:3), du(2:3,:,:), d2u(2:3,:,:), myjl, mynloc, mymeth, 10, 2, 1, 2)  
       IF (Nspec>1) THEN
          CALL dpdx_diag (du_diag_PFB(:),ng,pres_prev(:,4:Nspec+2),2*methpd+mymeth,myjl,splitFBpress)
       ELSE
          CALL dpdx_diag (du_diag_PFB(:),ng,pres_prev(:,1),2*methpd+mymeth,myjl,.FALSE.)
       END IF
    ELSE
       CALL c_diff_fast(pres_prev(:,1:3), du(1:3,:,:), d2u(1:3,:,:), myjl, mynloc, mymeth, 10, 3, 1, 3) 
       du_diag_PFB(:) = du_diag_local(:,1)
    END IF

    i_p_face(0) = 1
    DO i=1,dim
       i_p_face(i) = i_p_face(i-1)*3
    END DO
    DO face_type = 0, 3**dim - 1
       face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
       IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
          CALL get_all_indices_by_face (face_type, myjl, nloc, iloc)
          IF (ABS(face(1)) == 1) THEN
             faceval=REAL(face(1),pr)
             faceint=(3-face(1))/2  
             charVavg_prev(:) = 0.0_pr
             nloc_glob = nloc
             IF(nloc > 0 ) THEN
                charVavg_prev(1) = SUM(pres_prev(iloc(1:nloc),2) - d2u(1,iloc(1:nloc),2))  !(u-c)
                charVavg_prev(2) = SUM(pres_prev(iloc(1:nloc),2))                          !(u)
                charVavg_prev(3) = SUM(pres_prev(iloc(1:nloc),2) + d2u(1,iloc(1:nloc),2))  !(u+c)
             END IF
             CALL parallel_global_sum(INTEGER=nloc_glob)
             IF (nloc_glob > 0) THEN
                IF (LODIset) THEN
                   DO ii=1,3
                      CALL parallel_global_sum(REAL=charVavg_prev(ii))
                      charVavg_prev(ii) = charVavg_prev(ii)/REAL(nloc_glob,pr)
                   END DO
                   charVavg_prev(2-face(1)) = 0.0_pr
                   charVavg_prev(5-face(1)) = 1.0_pr
                ELSE
                   DO ii=1,3
                      CALL parallel_global_sum(REAL=charVavg_prev(ii))
                      charVavg_prev(ii) = charVavg_prev(ii)/REAL(nloc_glob,pr)
                      IF (charVavg_prev(ii)*faceval .le. 0.0_pr) charVavg_prev(ii+3) = 1.0_pr
                      charVavg_prev(ii) = faceval*MAX(0.0_pr,faceval*charVavg_prev(ii)) 
                   END DO
                END IF
             END IF
             IF(nloc > 0 ) THEN
                IF (ALLOCATED(charI)) DEALLOCATE(charI)
                IF (ALLOCATED(charX)) DEALLOCATE(charX)
                IF (ALLOCATED(charI_prev)) DEALLOCATE(charI_prev)
                IF (ALLOCATED(charX_prev)) DEALLOCATE(charX_prev)
                ALLOCATE(charI(nloc,4),charX(nloc,3),charI_prev(nloc,3),charX_prev(nloc,1))

                charI_prev(1:nloc,2) = charVavg_prev(2)*(d2u(1,iloc(1:nloc),2)**2*du(3,iloc(1:nloc),1) - du(1,iloc(1:nloc),1)) !u*(c^2*drho/dx - dp/dx)
                charI_prev(1:nloc,3) = charVavg_prev(3)*(du(1,iloc(1:nloc),1)+ulc_prev(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)*du(2,iloc(1:nloc),1))  !(u+c)*(dp/dx+rho*c*du/dx)
                charI_prev(1:nloc,1) = charVavg_prev(1)*(du(1,iloc(1:nloc),1)-ulc_prev(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)*du(2,iloc(1:nloc),1))  !(u-c)*(dp/dx-rho*c*du/dx)

                IF (pertchar) THEN   
                   charI_prev(1:nloc,3) = charI_prev(1:nloc,3) - charVavg_prev(3) * dP0(faceint)
                   charI_prev(1:nloc,1) = charI_prev(1:nloc,1) - charVavg_prev(1) * dP0(faceint)
                   charI_prev(1:nloc,2) = charI_prev(1:nloc,2) - charVavg_prev(2) * (d2u(1,iloc(1:nloc),2)**2*drho0(faceint) - dP0(faceint))
                END IF

                IF     (PLpinf==1) THEN
                   charI_prev(1:nloc,3) = charI_prev(1:nloc,3) + charVavg_prev(6)*pBcoef*(1.0_pr-maxMa**2)*c0(faceint)          *(pres_prev(iloc(1:nloc),1) - P0(faceint))
                   charI_prev(1:nloc,1) = charI_prev(1:nloc,1) + charVavg_prev(4)*pBcoef*(1.0_pr-maxMa**2)*c0(faceint)          *(pres_prev(iloc(1:nloc),1) - P0(faceint))
                ELSEIF (PLpinf==2) THEN
                   charI_prev(1:nloc,3) = charI_prev(1:nloc,3) + charVavg_prev(6)*pBcoef*(1.0_pr-maxMa**2)*d2u(1,iloc(1:nloc),2)*(pres_prev(iloc(1:nloc),1) - P0(faceint))
                   charI_prev(1:nloc,1) = charI_prev(1:nloc,1) + charVavg_prev(4)*pBcoef*(1.0_pr-maxMa**2)*d2u(1,iloc(1:nloc),2)*(pres_prev(iloc(1:nloc),1) - P0(faceint))
                ELSEIF (PLpinf==3) THEN
                   charI_prev(1:nloc,3) = charI_prev(1:nloc,3) + charVavg_prev(6)*pBcoef*(1.0_pr-maxMa**2)*c0(faceint) * &
                                                                                  (pres_prev(iloc(1:nloc),1)/ulc_prev(iloc(1:nloc),nden)/d2u(2,iloc(1:nloc),2) - Tconst)
                   charI_prev(1:nloc,1) = charI_prev(1:nloc,1) + charVavg_prev(4)*pBcoef*(1.0_pr-maxMa**2)*c0(faceint) * &
                                                                                  (pres_prev(iloc(1:nloc),1)/ulc_prev(iloc(1:nloc),nden)/d2u(2,iloc(1:nloc),2) - Tconst)
                ELSEIF (PLpinf==4) THEN
                   charI_prev(1:nloc,3) = charI_prev(1:nloc,3) + charVavg_prev(6)*pBcoef*(1.0_pr-maxMa**2)*d2u(1,iloc(1:nloc),2) * &
                                                                                  (pres_prev(iloc(1:nloc),1) - ulc_prev(iloc(1:nloc),nden)*d2u(2,iloc(1:nloc),2)*Tconst)
                   charI_prev(1:nloc,1) = charI_prev(1:nloc,1) + charVavg_prev(4)*pBcoef*(1.0_pr-maxMa**2)*d2u(1,iloc(1:nloc),2) * &
                                                                                  (pres_prev(iloc(1:nloc),1) - ulc_prev(iloc(1:nloc),nden)*d2u(2,iloc(1:nloc),2)*Tconst)
                ELSEIF (PLpinf==5) THEN
                   charI_prev(1:nloc,3) = charI_prev(1:nloc,3) - faceval*charVavg_prev(6)*pBcoef*(1.0_pr-maxMa**2)*gammBC(faceint)*Pint*pres_prev(iloc(1:nloc),2)
                   charI_prev(1:nloc,1) = charI_prev(1:nloc,1) - faceval*charVavg_prev(4)*pBcoef*(1.0_pr-maxMa**2)*gammBC(faceint)*Pint*pres_prev(iloc(1:nloc),2)
                ELSEIF (PLpinf==6) THEN
                   charI_prev(1:nloc,3) = charI_prev(1:nloc,3) - faceval*charVavg_prev(6)*pBcoef*(1.0_pr-maxMa**2)*ulc_prev(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)**2*pres_prev(iloc(1:nloc),2)
                   charI_prev(1:nloc,1) = charI_prev(1:nloc,1) - faceval*charVavg_prev(4)*pBcoef*(1.0_pr-maxMa**2)*ulc_prev(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)**2*pres_prev(iloc(1:nloc),2)
                END IF 
                
                charX_prev(1:nloc,1) = 0.5_pr/d2u(1,iloc(1:nloc),2)**2*(charI_prev(1:nloc,1)+charI_prev(1:nloc,3)+2.0_pr*charI_prev(1:nloc,2))

                IF (pertchar) THEN   
                   charX_prev(1:nloc,1) = charX_prev(1:nloc,1) + pres_prev(iloc(1:nloc),2)*drho0(faceint)
                END IF
                
                DO jj=1,myne_loc
                   IF (jj==nden) THEN
                      du (2,iloc(1:nloc),2) = -ulc_prev(iloc(1:nloc),nvel(1))/ulc_prev(iloc(1:nloc),nden)**2 !u_diag
                      d2u(3,iloc(1:nloc),1) = 0.0_pr  !gamma_diag
                      du (1,iloc(1:nloc),2) = 0.5_pr*SUM(ulc_prev(iloc(1:nloc),nvel(1):nvel(dim))**2,DIM=2)/ulc_prev(iloc(1:nloc),nden)**2*(d2u(1,iloc(1:nloc),1)-1.0_pr) !p_diag
                      d2u(3,iloc(1:nloc),2) = 0.5_pr/d2u(1,iloc(1:nloc),2)*d2u(1,iloc(1:nloc),1)/ulc_prev(iloc(1:nloc),nden) * &
                                                    (du(1,iloc(1:nloc),2)-pres_prev(iloc(1:nloc),1)/ulc_prev(iloc(1:nloc),nden) ) !c_diag
                      charI(1:nloc,3) = (1.0_pr-charVavg_prev(6))*(du(2,iloc(1:nloc),2)+d2u(3,iloc(1:nloc),2)) * &
                                                                      (du(1,iloc(1:nloc),1)+ulc_prev(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)*du(2,iloc(1:nloc),1)) + &
                                        charVavg_prev(3)*(du_diag_PFB(iloc(1:nloc))*du(1,iloc(1:nloc),2) + &
                                                              du_diag_local(iloc(1:nloc),1)*ulc_prev(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)*du(2,iloc(1:nloc),2) + &
                                                              du(2,iloc(1:nloc),1)*(ulc_prev(iloc(1:nloc),nden)*d2u(3,iloc(1:nloc),2) + d2u(1,iloc(1:nloc),2)))
                      charI(1:nloc,1) = (1.0_pr-charVavg_prev(4))*(du(2,iloc(1:nloc),2)-d2u(3,iloc(1:nloc),2)) * &
                                                                      (du(1,iloc(1:nloc),1)-ulc_prev(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)*du(2,iloc(1:nloc),1)) + &
                                        charVavg_prev(1)*(du_diag_PFB(iloc(1:nloc))*du(1,iloc(1:nloc),2) - & 
                                                              du_diag_local(iloc(1:nloc),1)*ulc_prev(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)*du(2,iloc(1:nloc),2) - &
                                                              du(2,iloc(1:nloc),1)*(ulc_prev(iloc(1:nloc),nden)*d2u(3,iloc(1:nloc),2) + d2u(1,iloc(1:nloc),2)))
                      charI(1:nloc,2) = (1.0_pr-charVavg_prev(5))*du(2,iloc(1:nloc),2)*(d2u(1,iloc(1:nloc),2)**2*du(3,iloc(1:nloc),1)-du(1,iloc(1:nloc),1)) + &
                                        charVavg_prev(2)*(du_diag_local(iloc(1:nloc),1)*d2u(1,iloc(1:nloc),2)**2-du_diag_PFB(iloc(1:nloc))*du(1,iloc(1:nloc),2) + &
                                                              2.0_pr*d2u(1,iloc(1:nloc),2)*d2u(3,iloc(1:nloc),2)*du(3,iloc(1:nloc),1) )
                      IF (pertchar) THEN   
                         charI(1:nloc,3) = charI(1:nloc,3) - (1.0_pr-charVavg_prev(6))*(du(2,iloc(1:nloc),2)+d2u(3,iloc(1:nloc),2)) * dP0(faceint)
                         charI(1:nloc,1) = charI(1:nloc,1) - (1.0_pr-charVavg_prev(4))*(du(2,iloc(1:nloc),2)-d2u(3,iloc(1:nloc),2)) * dP0(faceint)
                         charI(1:nloc,2) = charI(1:nloc,2) - (1.0_pr-charVavg_prev(5))*(du(2,iloc(1:nloc),2)) * (d2u(1,iloc(1:nloc),2)**2*drho0(faceint)-dP0(faceint)) - &
                                                             charVavg_prev(2)*2.0_pr*d2u(1,iloc(1:nloc),2)*d2u(3,iloc(1:nloc),2)*drho0(faceint)
                      END IF
                      IF     (PLpinf==1) THEN
                         charI(1:nloc,3) = charI(1:nloc,3) + charVavg_prev(6)*pBcoef*(1.0_pr-maxMa**2)*c0(faceint)*du(1,iloc(1:nloc),2)
                         charI(1:nloc,1) = charI(1:nloc,1) + charVavg_prev(4)*pBcoef*(1.0_pr-maxMa**2)*c0(faceint)*du(1,iloc(1:nloc),2)
                      ELSEIF (PLpinf==2) THEN
                         charI(1:nloc,3) = charI(1:nloc,3) + charVavg_prev(6)*pBcoef*(1.0_pr-maxMa**2) * &
                                                             (d2u(3,iloc(1:nloc),2)*(pres_prev(iloc(1:nloc),1)-P0(faceint)) + d2u(1,iloc(1:nloc),2)*du(1,iloc(1:nloc),2))
                         charI(1:nloc,1) = charI(1:nloc,1) + charVavg_prev(4)*pBcoef*(1.0_pr-maxMa**2) * &
                                                             (d2u(3,iloc(1:nloc),2)*(pres_prev(iloc(1:nloc),1)-P0(faceint)) + d2u(1,iloc(1:nloc),2)*du(1,iloc(1:nloc),2))
                      ELSEIF (PLpinf==3) THEN
                         charI(1:nloc,3) = charI(1:nloc,3) + charVavg_prev(6)*pBcoef*(1.0_pr-maxMa**2)*c0(faceint)/ulc_prev(iloc(1:nloc),nden)/d2u(2,iloc(1:nloc),2) * du(1,iloc(1:nloc),2)
                         charI(1:nloc,1) = charI(1:nloc,1) + charVavg_prev(4)*pBcoef*(1.0_pr-maxMa**2)*c0(faceint)/ulc_prev(iloc(1:nloc),nden)/d2u(2,iloc(1:nloc),2) * du(1,iloc(1:nloc),2)
                      ELSEIF (PLpinf==4) THEN
                         charI(1:nloc,3) = charI(1:nloc,3) + charVavg_prev(6)*pBcoef*(1.0_pr-maxMa**2)*(d2u(3,iloc(1:nloc),2) * &
                                                              (pres_prev(iloc(1:nloc),1) - ulc_prev(iloc(1:nloc),nden)*d2u(2,iloc(1:nloc),2)*Tconst) + d2u(1,iloc(1:nloc),2)*du(1,iloc(1:nloc),2) )
                         charI(1:nloc,1) = charI(1:nloc,1) + charVavg_prev(4)*pBcoef*(1.0_pr-maxMa**2)*(d2u(3,iloc(1:nloc),2) * &
                                                              (pres_prev(iloc(1:nloc),1) - ulc_prev(iloc(1:nloc),nden)*d2u(2,iloc(1:nloc),2)*Tconst) + d2u(1,iloc(1:nloc),2)*du(1,iloc(1:nloc),2) )
                      ELSEIF (PLpinf==5) THEN
                         charI(1:nloc,3) = charI(1:nloc,3) - faceval*charVavg_prev(6)*pBcoef*(1.0_pr-maxMa**2)*gammBC(faceint)*Pint*du(2,iloc(1:nloc),2)
                         charI(1:nloc,1) = charI(1:nloc,1) - faceval*charVavg_prev(4)*pBcoef*(1.0_pr-maxMa**2)*gammBC(faceint)*Pint*du(2,iloc(1:nloc),2)
                      ELSEIF (PLpinf==6) THEN
                         charI(1:nloc,3) = charI(1:nloc,3) - faceval*charVavg_prev(6)*pBcoef*(1.0_pr-maxMa**2)*d2u(1,iloc(1:nloc),2) * &
                                                         (d2u(1,iloc(1:nloc),2)*pres_prev(iloc(1:nloc),2) + &
                                                          2.0_pr*ulc_prev(iloc(1:nloc),nden)*d2u(3,iloc(1:nloc),2)*pres_prev(iloc(1:nloc),2) + &
                                                          ulc_prev(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)*du(2,iloc(1:nloc),2) )
                         charI(1:nloc,1) = charI(1:nloc,1) - faceval*charVavg_prev(4)*pBcoef*(1.0_pr-maxMa**2)*d2u(1,iloc(1:nloc),2) * &
                                                         (d2u(1,iloc(1:nloc),2)*pres_prev(iloc(1:nloc),2) + &
                                                          2.0_pr*ulc_prev(iloc(1:nloc),nden)*d2u(3,iloc(1:nloc),2)*pres_prev(iloc(1:nloc),2) + &
                                                          ulc_prev(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)*du(2,iloc(1:nloc),2) )
                      END IF
                      charX(1:nloc,1) = 0.5_pr/d2u(1,iloc(1:nloc),2)**2 * (charI(1:nloc,1)+charI(1:nloc,3)+2.0_pr*charI(1:nloc,2)) - &
                                               d2u(3,iloc(1:nloc),2)/d2u(1,iloc(1:nloc),2)**3 * (charI_prev(1:nloc,1)+charI_prev(1:nloc,3)+2.0_pr*charI_prev(1:nloc,2))

                      IF (pertchar) charX(1:nloc,1) = charX(1:nloc,1) + du(2,iloc(1:nloc),2)*drho0(faceint)   

                      Drhs_diag_local((jj-1)*mynloc+iloc(1:nloc)) = Drhs_diag_local((jj-1)*mynloc+iloc(1:nloc)) - charX(1:nloc,1)
                   ELSE IF (jj==nvel(1)) THEN
                      du (2,iloc(1:nloc),2) = 1.0_pr/ulc_prev(iloc(1:nloc),nden) !u_diag
                      d2u(3,iloc(1:nloc),1) = 0.0_pr  !gamma_diag
                      du (1,iloc(1:nloc),2) = -ulc_prev(iloc(1:nloc),nvel(1))/ulc_prev(iloc(1:nloc),nden)*(d2u(1,iloc(1:nloc),1)-1.0_pr) !p_diag
                      d2u(3,iloc(1:nloc),2) = 0.5_pr/d2u(1,iloc(1:nloc),2)*d2u(1,iloc(1:nloc),1)/ulc_prev(iloc(1:nloc),nden)*du(1,iloc(1:nloc),2) !c_diag
                      charI(1:nloc,3) = (1.0_pr-charVavg_prev(6))*(du(2,iloc(1:nloc),2)+d2u(3,iloc(1:nloc),2)) * &
                                                                      (du(1,iloc(1:nloc),1)+ulc_prev(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)*du(2,iloc(1:nloc),1)) + &
                                        charVavg_prev(3)*(du_diag_PFB(iloc(1:nloc))*du(1,iloc(1:nloc),2) + & 
                                                              du_diag_local(iloc(1:nloc),1)*ulc_prev(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)*du(2,iloc(1:nloc),2) + &
                                                              du(2,iloc(1:nloc),1)*ulc_prev(iloc(1:nloc),nden)*d2u(3,iloc(1:nloc),2))
                      charI(1:nloc,1) = (1.0_pr-charVavg_prev(4))*(du(2,iloc(1:nloc),2)-d2u(3,iloc(1:nloc),2)) * &
                                                                      (du(1,iloc(1:nloc),1)-ulc_prev(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)*du(2,iloc(1:nloc),1)) + &
                                        charVavg_prev(1)*(du_diag_PFB(iloc(1:nloc))*du(1,iloc(1:nloc),2) - & 
                                                              du_diag_local(iloc(1:nloc),1)*ulc_prev(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)*du(2,iloc(1:nloc),2) - &
                                                              du(2,iloc(1:nloc),1)*ulc_prev(iloc(1:nloc),nden)*d2u(3,iloc(1:nloc),2))
                      charI(1:nloc,2) = (1.0_pr-charVavg_prev(5))*du(2,iloc(1:nloc),2)*(d2u(1,iloc(1:nloc),2)**2*du(3,iloc(1:nloc),1)-du(1,iloc(1:nloc),1)) + &
                                        charVavg_prev(2)*(-du_diag_PFB(iloc(1:nloc))*du(1,iloc(1:nloc),2) + 2.0_pr*d2u(1,iloc(1:nloc),2)*d2u(3,iloc(1:nloc),2)*du(3,iloc(1:nloc),1))
                      IF (pertchar) THEN   
                         charI(1:nloc,3) = charI(1:nloc,3) - (1.0_pr-charVavg_prev(6))*(du(2,iloc(1:nloc),2)+d2u(3,iloc(1:nloc),2)) * dP0(faceint)
                         charI(1:nloc,1) = charI(1:nloc,1) - (1.0_pr-charVavg_prev(4))*(du(2,iloc(1:nloc),2)-d2u(3,iloc(1:nloc),2)) * dP0(faceint)
                         charI(1:nloc,2) = charI(1:nloc,2) - (1.0_pr-charVavg_prev(5))*(du(2,iloc(1:nloc),2)) * (d2u(1,iloc(1:nloc),2)**2*drho0(faceint)-dP0(faceint)) - &
                                                             charVavg_prev(2)*2.0_pr*d2u(1,iloc(1:nloc),2)*d2u(3,iloc(1:nloc),2)*drho0(faceint)
                      END IF
                      IF     (PLpinf==1) THEN
                         charI(1:nloc,3) = charI(1:nloc,3) + charVavg_prev(6)*pBcoef*(1.0_pr-maxMa**2)*c0(faceint)*du(1,iloc(1:nloc),2)
                         charI(1:nloc,1) = charI(1:nloc,1) + charVavg_prev(4)*pBcoef*(1.0_pr-maxMa**2)*c0(faceint)*du(1,iloc(1:nloc),2)
                      ELSEIF (PLpinf==2) THEN
                         charI(1:nloc,3) = charI(1:nloc,3) + charVavg_prev(6)*pBcoef*(1.0_pr-maxMa**2) * &
                                                             (d2u(3,iloc(1:nloc),2)*(pres_prev(iloc(1:nloc),1)-P0(faceint)) + d2u(1,iloc(1:nloc),2)*du(1,iloc(1:nloc),2))
                         charI(1:nloc,1) = charI(1:nloc,1) + charVavg_prev(4)*pBcoef*(1.0_pr-maxMa**2) * &
                                                             (d2u(3,iloc(1:nloc),2)*(pres_prev(iloc(1:nloc),1)-P0(faceint)) + d2u(1,iloc(1:nloc),2)*du(1,iloc(1:nloc),2))
                      ELSEIF (PLpinf==3) THEN
                         charI(1:nloc,3) = charI(1:nloc,3) + charVavg_prev(6)*pBcoef*(1.0_pr-maxMa**2)*c0(faceint)/ulc_prev(iloc(1:nloc),nden)/d2u(2,iloc(1:nloc),2) * du(1,iloc(1:nloc),2)
                         charI(1:nloc,1) = charI(1:nloc,1) + charVavg_prev(4)*pBcoef*(1.0_pr-maxMa**2)*c0(faceint)/ulc_prev(iloc(1:nloc),nden)/d2u(2,iloc(1:nloc),2) * du(1,iloc(1:nloc),2)
                      ELSEIF (PLpinf==4) THEN
                         charI(1:nloc,3) = charI(1:nloc,3) + charVavg_prev(6)*pBcoef*(1.0_pr-maxMa**2)*(d2u(3,iloc(1:nloc),2) * &
                                                              (pres_prev(iloc(1:nloc),1) - ulc_prev(iloc(1:nloc),nden)*d2u(2,iloc(1:nloc),2)*Tconst) + d2u(1,iloc(1:nloc),2)*du(1,iloc(1:nloc),2) )
                         charI(1:nloc,1) = charI(1:nloc,1) + charVavg_prev(4)*pBcoef*(1.0_pr-maxMa**2)*(d2u(3,iloc(1:nloc),2) * &
                                                              (pres_prev(iloc(1:nloc),1) - ulc_prev(iloc(1:nloc),nden)*d2u(2,iloc(1:nloc),2)*Tconst) + d2u(1,iloc(1:nloc),2)*du(1,iloc(1:nloc),2) )
                      ELSEIF (PLpinf==5) THEN
                         charI(1:nloc,3) = charI(1:nloc,3) - faceval*charVavg_prev(6)*pBcoef*(1.0_pr-maxMa**2)*gammBC(faceint)*Pint*du(2,iloc(1:nloc),2)
                         charI(1:nloc,1) = charI(1:nloc,1) - faceval*charVavg_prev(4)*pBcoef*(1.0_pr-maxMa**2)*gammBC(faceint)*Pint*du(2,iloc(1:nloc),2)
                      ELSEIF (PLpinf==6) THEN
                         charI(1:nloc,3) = charI(1:nloc,3) - faceval*charVavg_prev(6)*pBcoef*(1.0_pr-maxMa**2)*d2u(1,iloc(1:nloc),2) * &
                                                         (2.0_pr*ulc_prev(iloc(1:nloc),nden)*d2u(3,iloc(1:nloc),2)*pres_prev(iloc(1:nloc),2) + &
                                                          ulc_prev(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)*du(2,iloc(1:nloc),2) )
                         charI(1:nloc,1) = charI(1:nloc,1) - faceval*charVavg_prev(4)*pBcoef*(1.0_pr-maxMa**2)*d2u(1,iloc(1:nloc),2) * &
                                                         (2.0_pr*ulc_prev(iloc(1:nloc),nden)*d2u(3,iloc(1:nloc),2)*pres_prev(iloc(1:nloc),2) + &
                                                          ulc_prev(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)*du(2,iloc(1:nloc),2) )
                      END IF

                      charX(1:nloc,1) = 0.5_pr/d2u(1,iloc(1:nloc),2)**2 * (charI(1:nloc,1)+charI(1:nloc,3)+2.0_pr*charI(1:nloc,2)) - &
                                               d2u(3,iloc(1:nloc),2)/d2u(1,iloc(1:nloc),2)**3 * (charI_prev(1:nloc,1)+charI_prev(1:nloc,3)+2.0_pr*charI_prev(1:nloc,2))
                      charX(1:nloc,2) = 0.5_pr/ulc_prev(iloc(1:nloc),nden)/d2u(1,iloc(1:nloc),2) * (charI(1:nloc,3)-charI(1:nloc,1) - &
                                                     d2u(3,iloc(1:nloc),2)/d2u(1,iloc(1:nloc),2)*(charI_prev(1:nloc,3)-charI_prev(1:nloc,1)))

                      IF (pertchar) charX(1:nloc,1) = charX(1:nloc,1) + du(2,iloc(1:nloc),2)*drho0(faceint)   
                      
                      Drhs_diag_local((jj-1)*mynloc+iloc(1:nloc)) = Drhs_diag_local((jj-1)*mynloc+iloc(1:nloc)) - du(2,iloc(1:nloc),2)*charX_prev(1:nloc,1) - &
                                                     pres_prev(iloc(1:nloc),2)*charX(1:nloc,1) - ulc_prev(iloc(1:nloc),nden)*charX(1:nloc,2)
                   ELSE IF (jj>=nvel(2) .AND. jj<=nvel(dim)) THEN
                      du (2,iloc(1:nloc),2) = 0.0_pr !u_diag
                      d2u(3,iloc(1:nloc),1) = 0.0_pr  !gamma_diag
                      du (1,iloc(1:nloc),2) = -ulc_prev(iloc(1:nloc),jj)/ulc_prev(iloc(1:nloc),nden)*(d2u(1,iloc(1:nloc),1)-1.0_pr) !p_diag
                      d2u(3,iloc(1:nloc),2) = 0.5_pr/d2u(1,iloc(1:nloc),2)*d2u(1,iloc(1:nloc),1)/ulc_prev(iloc(1:nloc),nden)*du(1,iloc(1:nloc),2) !c_diag
                      charI(1:nloc,3) = (1.0_pr-charVavg_prev(6))*d2u(3,iloc(1:nloc),2) * (du(1,iloc(1:nloc),1)+ulc_prev(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)*du(2,iloc(1:nloc),1)) + &
                                        charVavg_prev(3)*(du_diag_PFB(iloc(1:nloc))*du(1,iloc(1:nloc),2) + du(2,iloc(1:nloc),1)*ulc_prev(iloc(1:nloc),nden)*d2u(3,iloc(1:nloc),2))
                      charI(1:nloc,1) =-(1.0_pr-charVavg_prev(4))*d2u(3,iloc(1:nloc),2) * (du(1,iloc(1:nloc),1)-ulc_prev(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)*du(2,iloc(1:nloc),1)) + &
                                        charVavg_prev(1)*(du_diag_PFB(iloc(1:nloc))*du(1,iloc(1:nloc),2) - du(2,iloc(1:nloc),1)*ulc_prev(iloc(1:nloc),nden)*d2u(3,iloc(1:nloc),2))
                      charI(1:nloc,2) = charVavg_prev(2)*(-du_diag_PFB(iloc(1:nloc))*du(1,iloc(1:nloc),2) + 2.0_pr*d2u(1,iloc(1:nloc),2)*d2u(3,iloc(1:nloc),2)*du(3,iloc(1:nloc),1))
                      charI(1:nloc,4) = charVavg_prev(2)*du_diag_local(iloc(1:nloc),1)/ulc_prev(iloc(1:nloc),nden)
                      IF (pertchar) THEN   
                         charI(1:nloc,3) = charI(1:nloc,3) - (1.0_pr-charVavg_prev(6))*d2u(3,iloc(1:nloc),2) * dP0(faceint)
                         charI(1:nloc,1) = charI(1:nloc,1) + (1.0_pr-charVavg_prev(4))*d2u(3,iloc(1:nloc),2) * dP0(faceint)
                         charI(1:nloc,2) = charI(1:nloc,2) - charVavg_prev(2)*2.0_pr*d2u(1,iloc(1:nloc),2)*d2u(3,iloc(1:nloc),2)*drho0(faceint)
                      END IF
                      IF     (PLpinf==1) THEN
                         charI(1:nloc,3) = charI(1:nloc,3) + charVavg_prev(6)*pBcoef*(1.0_pr-maxMa**2)*c0(faceint)*du(1,iloc(1:nloc),2)
                         charI(1:nloc,1) = charI(1:nloc,1) + charVavg_prev(4)*pBcoef*(1.0_pr-maxMa**2)*c0(faceint)*du(1,iloc(1:nloc),2)
                      ELSEIF (PLpinf==2) THEN
                         charI(1:nloc,3) = charI(1:nloc,3) + charVavg_prev(6)*pBcoef*(1.0_pr-maxMa**2) * &
                                                             (d2u(3,iloc(1:nloc),2)*(pres_prev(iloc(1:nloc),1)-P0(faceint)) + d2u(1,iloc(1:nloc),2)*du(1,iloc(1:nloc),2))
                         charI(1:nloc,1) = charI(1:nloc,1) + charVavg_prev(4)*pBcoef*(1.0_pr-maxMa**2) * &
                                                             (d2u(3,iloc(1:nloc),2)*(pres_prev(iloc(1:nloc),1)-P0(faceint)) + d2u(1,iloc(1:nloc),2)*du(1,iloc(1:nloc),2))
                      ELSEIF (PLpinf==3) THEN
                         charI(1:nloc,3) = charI(1:nloc,3) + charVavg_prev(6)*pBcoef*(1.0_pr-maxMa**2)*c0(faceint)/ulc_prev(iloc(1:nloc),nden)/d2u(2,iloc(1:nloc),2) * du(1,iloc(1:nloc),2)
                         charI(1:nloc,1) = charI(1:nloc,1) + charVavg_prev(4)*pBcoef*(1.0_pr-maxMa**2)*c0(faceint)/ulc_prev(iloc(1:nloc),nden)/d2u(2,iloc(1:nloc),2) * du(1,iloc(1:nloc),2)
                      ELSEIF (PLpinf==4) THEN
                         charI(1:nloc,3) = charI(1:nloc,3) + charVavg_prev(6)*pBcoef*(1.0_pr-maxMa**2)*(d2u(3,iloc(1:nloc),2) * &
                                                              (pres_prev(iloc(1:nloc),1) - ulc_prev(iloc(1:nloc),nden)*d2u(2,iloc(1:nloc),2)*Tconst) + d2u(1,iloc(1:nloc),2)*du(1,iloc(1:nloc),2) )
                         charI(1:nloc,1) = charI(1:nloc,1) + charVavg_prev(4)*pBcoef*(1.0_pr-maxMa**2)*(d2u(3,iloc(1:nloc),2) * &
                                                              (pres_prev(iloc(1:nloc),1) - ulc_prev(iloc(1:nloc),nden)*d2u(2,iloc(1:nloc),2)*Tconst) + d2u(1,iloc(1:nloc),2)*du(1,iloc(1:nloc),2) )
                      ELSEIF (PLpinf==6) THEN
                         charI(1:nloc,3) = charI(1:nloc,3) - faceval*charVavg_prev(6)*pBcoef*(1.0_pr-maxMa**2)*d2u(1,iloc(1:nloc),2) * &
                                                         2.0_pr*ulc_prev(iloc(1:nloc),nden)*d2u(3,iloc(1:nloc),2)*pres_prev(iloc(1:nloc),2)
                         charI(1:nloc,1) = charI(1:nloc,1) - faceval*charVavg_prev(4)*pBcoef*(1.0_pr-maxMa**2)*d2u(1,iloc(1:nloc),2) * &
                                                         2.0_pr*ulc_prev(iloc(1:nloc),nden)*d2u(3,iloc(1:nloc),2)*pres_prev(iloc(1:nloc),2)
                      END IF

                      charX(1:nloc,1) = 0.5_pr/d2u(1,iloc(1:nloc),2)**2 * (charI(1:nloc,1)+charI(1:nloc,3)+2.0_pr*charI(1:nloc,2)) - &
                                               d2u(3,iloc(1:nloc),2)/d2u(1,iloc(1:nloc),2)**3 * (charI_prev(1:nloc,1)+charI_prev(1:nloc,3)+2.0_pr*charI_prev(1:nloc,2))
                      charX(1:nloc,3) = charI(1:nloc,4)
                      Drhs_diag_local((jj-1)*mynloc+iloc(1:nloc)) = Drhs_diag_local((jj-1)*mynloc+iloc(1:nloc)) - (charX_prev(1:nloc,1)+ulc_prev(iloc(1:nloc),jj)*charX(1:nloc,1)) / &
                                                     ulc_prev(iloc(1:nloc),nden) - ulc_prev(iloc(1:nloc),nden)*charX(1:nloc,3)
                   ELSE IF (jj==neng) THEN
                      du (2,iloc(1:nloc),2) = 0.0_pr !u_diag
                      d2u(3,iloc(1:nloc),1) = 0.0_pr  !gamma_diag
                      du (1,iloc(1:nloc),2) = d2u(1,iloc(1:nloc),1)-1.0_pr !p_diag
                      d2u(3,iloc(1:nloc),2) = 0.5_pr/d2u(1,iloc(1:nloc),2)*d2u(1,iloc(1:nloc),1)/ulc_prev(iloc(1:nloc),nden)*du(1,iloc(1:nloc),2) !c_diag
                      charI(1:nloc,3) = (1.0_pr-charVavg_prev(6))*d2u(3,iloc(1:nloc),2) * (du(1,iloc(1:nloc),1)+ulc_prev(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)*du(2,iloc(1:nloc),1)) + &
                                        charVavg_prev(3)*(du_diag_PFB(iloc(1:nloc))*du(1,iloc(1:nloc),2) + du(2,iloc(1:nloc),1)*ulc_prev(iloc(1:nloc),nden)*d2u(3,iloc(1:nloc),2))
                      charI(1:nloc,1) =-(1.0_pr-charVavg_prev(4))*d2u(3,iloc(1:nloc),2) * (du(1,iloc(1:nloc),1)-ulc_prev(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)*du(2,iloc(1:nloc),1)) + &
                                        charVavg_prev(1)*(du_diag_PFB(iloc(1:nloc))*du(1,iloc(1:nloc),2) - du(2,iloc(1:nloc),1)*ulc_prev(iloc(1:nloc),nden)*d2u(3,iloc(1:nloc),2))
                      charI(1:nloc,2) = charVavg_prev(2)*(-du_diag_PFB(iloc(1:nloc))*du(1,iloc(1:nloc),2) + 2.0_pr*d2u(1,iloc(1:nloc),2)*d2u(3,iloc(1:nloc),2)*du(3,iloc(1:nloc),1))
                      IF (pertchar) THEN   
                         charI(1:nloc,3) = charI(1:nloc,3) - (1.0_pr-charVavg_prev(6))*d2u(3,iloc(1:nloc),2) * dP0(faceint)
                         charI(1:nloc,1) = charI(1:nloc,1) + (1.0_pr-charVavg_prev(4))*d2u(3,iloc(1:nloc),2) * dP0(faceint)
                         charI(1:nloc,2) = charI(1:nloc,2) - charVavg_prev(2)*2.0_pr*d2u(1,iloc(1:nloc),2)*d2u(3,iloc(1:nloc),2)*drho0(faceint)
                      END IF
                      IF     (PLpinf==1) THEN
                         charI(1:nloc,3) = charI(1:nloc,3) + charVavg_prev(6)*pBcoef*(1.0_pr-maxMa**2)*c0(faceint)*du(1,iloc(1:nloc),2)
                         charI(1:nloc,1) = charI(1:nloc,1) + charVavg_prev(4)*pBcoef*(1.0_pr-maxMa**2)*c0(faceint)*du(1,iloc(1:nloc),2)
                      ELSEIF (PLpinf==2) THEN
                         charI(1:nloc,3) = charI(1:nloc,3) + charVavg_prev(6)*pBcoef*(1.0_pr-maxMa**2) * &
                                                             (d2u(3,iloc(1:nloc),2)*(pres_prev(iloc(1:nloc),1)-P0(faceint)) + d2u(1,iloc(1:nloc),2)*du(1,iloc(1:nloc),2))
                         charI(1:nloc,1) = charI(1:nloc,1) + charVavg_prev(4)*pBcoef*(1.0_pr-maxMa**2) * &
                                                             (d2u(3,iloc(1:nloc),2)*(pres_prev(iloc(1:nloc),1)-P0(faceint)) + d2u(1,iloc(1:nloc),2)*du(1,iloc(1:nloc),2))
                      ELSEIF (PLpinf==3) THEN
                         charI(1:nloc,3) = charI(1:nloc,3) + charVavg_prev(6)*pBcoef*(1.0_pr-maxMa**2)*c0(faceint)/ulc_prev(iloc(1:nloc),nden)/d2u(2,iloc(1:nloc),2) * du(1,iloc(1:nloc),2)
                         charI(1:nloc,1) = charI(1:nloc,1) + charVavg_prev(4)*pBcoef*(1.0_pr-maxMa**2)*c0(faceint)/ulc_prev(iloc(1:nloc),nden)/d2u(2,iloc(1:nloc),2) * du(1,iloc(1:nloc),2)
                      ELSEIF (PLpinf==4) THEN
                         charI(1:nloc,3) = charI(1:nloc,3) + charVavg_prev(6)*pBcoef*(1.0_pr-maxMa**2)*(d2u(3,iloc(1:nloc),2) * &
                                                              (pres_prev(iloc(1:nloc),1) - ulc_prev(iloc(1:nloc),nden)*d2u(2,iloc(1:nloc),2)*Tconst) + d2u(1,iloc(1:nloc),2)*du(1,iloc(1:nloc),2) )
                         charI(1:nloc,1) = charI(1:nloc,1) + charVavg_prev(4)*pBcoef*(1.0_pr-maxMa**2)*(d2u(3,iloc(1:nloc),2) * &
                                                              (pres_prev(iloc(1:nloc),1) - ulc_prev(iloc(1:nloc),nden)*d2u(2,iloc(1:nloc),2)*Tconst) + d2u(1,iloc(1:nloc),2)*du(1,iloc(1:nloc),2) )
                      ELSEIF (PLpinf==6) THEN
                         charI(1:nloc,3) = charI(1:nloc,3) - faceval*charVavg_prev(6)*pBcoef*(1.0_pr-maxMa**2)*d2u(1,iloc(1:nloc),2) * &
                                                         2.0_pr*ulc_prev(iloc(1:nloc),nden)*d2u(3,iloc(1:nloc),2)*pres_prev(iloc(1:nloc),2)
                         charI(1:nloc,1) = charI(1:nloc,1) - faceval*charVavg_prev(4)*pBcoef*(1.0_pr-maxMa**2)*d2u(1,iloc(1:nloc),2) * &
                                                         2.0_pr*ulc_prev(iloc(1:nloc),nden)*d2u(3,iloc(1:nloc),2)*pres_prev(iloc(1:nloc),2)
                      END IF
                      charX(1:nloc,1) = 0.5_pr/d2u(1,iloc(1:nloc),2)**2 * (charI(1:nloc,1)+charI(1:nloc,3)+2.0_pr*charI(1:nloc,2)) - &
                                               d2u(3,iloc(1:nloc),2)/d2u(1,iloc(1:nloc),2)**3 * (charI_prev(1:nloc,1)+charI_prev(1:nloc,3)+2.0_pr*charI_prev(1:nloc,2))
                      charX(1:nloc,2) = 0.5_pr/ulc_prev(iloc(1:nloc),nden)/d2u(1,iloc(1:nloc),2) * (charI(1:nloc,3)-charI(1:nloc,1) - &
                                                     d2u(3,iloc(1:nloc),2)/d2u(1,iloc(1:nloc),2)*(charI_prev(1:nloc,3)-charI_prev(1:nloc,1)))
                      charX(1:nloc,3) = 0.5_pr*(charI(1:nloc,1)+charI(1:nloc,3))
                      Drhs_diag_local((jj-1)*mynloc+iloc(1:nloc)) = Drhs_diag_local((jj-1)*mynloc+iloc(1:nloc)) - 0.5_pr*SUM(ulc_prev(iloc(1:nloc),nvel(1):nvel(dim))**2,DIM=2) / &
                                        ulc_prev(iloc(1:nloc),nden)**2*charX(1:nloc,1) - ulc_prev(iloc(1:nloc),nvel(1))*charX(1:nloc,2) - &
                                        1.0_pr/(d2u(1,iloc(1:nloc),1)-1.0_pr)*charX(1:nloc,3)
                   ELSE IF (jj>=nspc_lil .AND. jj<=nspc_big) THEN
                      du (2,iloc(1:nloc),2) = 0.0_pr !u_diag
                      d2u(3,iloc(1:nloc),1) = (d2u(2,iloc(1:nloc),1)/MW_in(jj-nspc(1)+1) - d2u(2,iloc(1:nloc),2)*cp_in(jj-nspc(1)+1)) / &
                                                    ulc_prev(iloc(1:nloc),nden)/(d2u(2,iloc(1:nloc),1)-d2u(2,iloc(1:nloc),2))**2  !gamma_diag
                      du (1,iloc(1:nloc),2) = (ulc_prev(iloc(1:nloc),neng) - 0.5_pr*SUM(ulc_prev(iloc(1:nloc),nvel(1):nvel(dim))**2,DIM=2) / &
                                                    ulc_prev(iloc(1:nloc),nden))*d2u(3,iloc(1:nloc),1) !p_diag
                      d2u(3,iloc(1:nloc),2) = 0.5_pr/d2u(1,iloc(1:nloc),2)/ulc_prev(iloc(1:nloc),nden) * &
                                                    (d2u(3,iloc(1:nloc),1)*pres_prev(iloc(1:nloc),1)+d2u(1,iloc(1:nloc),1)*du(1,iloc(1:nloc),2)) !c_diag
          
                      charI(1:nloc,3) = (1.0_pr-charVavg_prev(6))*d2u(3,iloc(1:nloc),2) * (du(1,iloc(1:nloc),1)+ulc_prev(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)*du(2,iloc(1:nloc),1)) + &
                                        charVavg_prev(3)*(du_diag_PFB(iloc(1:nloc))*du(1,iloc(1:nloc),2) + du(2,iloc(1:nloc),1)*ulc_prev(iloc(1:nloc),nden)*d2u(3,iloc(1:nloc),2))
                      charI(1:nloc,1) =-(1.0_pr-charVavg_prev(4))*d2u(3,iloc(1:nloc),2) * (du(1,iloc(1:nloc),1)-ulc_prev(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)*du(2,iloc(1:nloc),1)) + &
                                        charVavg_prev(1)*(du_diag_PFB(iloc(1:nloc))*du(1,iloc(1:nloc),2) - du(2,iloc(1:nloc),1)*ulc_prev(iloc(1:nloc),nden)*d2u(3,iloc(1:nloc),2))
                      charI(1:nloc,2) = charVavg_prev(2)*(-du_diag_PFB(iloc(1:nloc))*du(1,iloc(1:nloc),2) + 2.0_pr*d2u(1,iloc(1:nloc),2)*d2u(3,iloc(1:nloc),2)*du(3,iloc(1:nloc),1))
                      charI(1:nloc,4) = charVavg_prev(2)*du_diag_local(iloc(1:nloc),1)/ulc_prev(iloc(1:nloc),nden)

                      IF (pertchar) THEN   
                         charI(1:nloc,3) = charI(1:nloc,3) - (1.0_pr-charVavg_prev(6))*d2u(3,iloc(1:nloc),2) * dP0(faceint)
                         charI(1:nloc,1) = charI(1:nloc,1) + (1.0_pr-charVavg_prev(4))*d2u(3,iloc(1:nloc),2) * dP0(faceint)
                         charI(1:nloc,2) = charI(1:nloc,2) - charVavg_prev(2)*2.0_pr*d2u(1,iloc(1:nloc),2)*d2u(3,iloc(1:nloc),2)*drho0(faceint)
                      END IF
                      IF     (PLpinf==1) THEN
                         charI(1:nloc,3) = charI(1:nloc,3) + charVavg_prev(6)*pBcoef*(1.0_pr-maxMa**2)*c0(faceint)*du(1,iloc(1:nloc),2)
                         charI(1:nloc,1) = charI(1:nloc,1) + charVavg_prev(4)*pBcoef*(1.0_pr-maxMa**2)*c0(faceint)*du(1,iloc(1:nloc),2)
                      ELSEIF (PLpinf==2) THEN
                         charI(1:nloc,3) = charI(1:nloc,3) + charVavg_prev(6)*pBcoef*(1.0_pr-maxMa**2) * &
                                                             (d2u(3,iloc(1:nloc),2)*(pres_prev(iloc(1:nloc),1)-P0(faceint)) + d2u(1,iloc(1:nloc),2)*du(1,iloc(1:nloc),2))
                         charI(1:nloc,1) = charI(1:nloc,1) + charVavg_prev(4)*pBcoef*(1.0_pr-maxMa**2) * &
                                                             (d2u(3,iloc(1:nloc),2)*(pres_prev(iloc(1:nloc),1)-P0(faceint)) + d2u(1,iloc(1:nloc),2)*du(1,iloc(1:nloc),2))
                      ELSEIF (PLpinf==3) THEN
                         charI(1:nloc,3) = charI(1:nloc,3) + charVavg_prev(6)*pBcoef*(1.0_pr-maxMa**2)*c0(faceint)/ulc_prev(iloc(1:nloc),nden)/d2u(2,iloc(1:nloc),2) * &
                                                         (du(1,iloc(1:nloc),2) - pres_prev(iloc(1:nloc),1)/MW_in(faceint)/ulc_prev(iloc(1:nloc),nden)/d2u(2,iloc(1:nloc),2) ) 
                         charI(1:nloc,1) = charI(1:nloc,1) + charVavg_prev(4)*pBcoef*(1.0_pr-maxMa**2)*c0(faceint)/ulc_prev(iloc(1:nloc),nden)/d2u(2,iloc(1:nloc),2) * &
                                                         (du(1,iloc(1:nloc),2) - pres_prev(iloc(1:nloc),1)/MW_in(faceint)/ulc_prev(iloc(1:nloc),nden)/d2u(2,iloc(1:nloc),2) ) 
                      ELSEIF (PLpinf==4) THEN
                         charI(1:nloc,3) = charI(1:nloc,3) + charVavg_prev(6)*pBcoef*(1.0_pr-maxMa**2)*(d2u(3,iloc(1:nloc),2) * &
                                                              (pres_prev(iloc(1:nloc),1) - ulc_prev(iloc(1:nloc),nden)*d2u(2,iloc(1:nloc),2)*Tconst) + &
                                                                                     d2u(1,iloc(1:nloc),2)*(du(1,iloc(1:nloc),2) - Tconst/MW_in(faceint)) )
                         charI(1:nloc,1) = charI(1:nloc,1) + charVavg_prev(4)*pBcoef*(1.0_pr-maxMa**2)*(d2u(3,iloc(1:nloc),2) * &
                                                              (pres_prev(iloc(1:nloc),1) - ulc_prev(iloc(1:nloc),nden)*d2u(2,iloc(1:nloc),2)*Tconst) + &
                                                                                     d2u(1,iloc(1:nloc),2)*(du(1,iloc(1:nloc),2) - Tconst/MW_in(faceint)) )
                      ELSEIF (PLpinf==6) THEN
                         charI(1:nloc,3) = charI(1:nloc,3) - faceval*charVavg_prev(6)*pBcoef*(1.0_pr-maxMa**2)*d2u(1,iloc(1:nloc),2) * &
                                                         2.0_pr*ulc_prev(iloc(1:nloc),nden)*d2u(3,iloc(1:nloc),2)*pres_prev(iloc(1:nloc),2)
                         charI(1:nloc,1) = charI(1:nloc,1) - faceval*charVavg_prev(4)*pBcoef*(1.0_pr-maxMa**2)*d2u(1,iloc(1:nloc),2) * &
                                                         2.0_pr*ulc_prev(iloc(1:nloc),nden)*d2u(3,iloc(1:nloc),2)*pres_prev(iloc(1:nloc),2)
                      END IF
                      charX(1:nloc,1) = 0.5_pr/d2u(1,iloc(1:nloc),2)**2 * (charI(1:nloc,1)+charI(1:nloc,3)+2.0_pr*charI(1:nloc,2)) - &
                                               d2u(3,iloc(1:nloc),2)/d2u(1,iloc(1:nloc),2)**3 * (charI_prev(1:nloc,1)+charI_prev(1:nloc,3)+2.0_pr*charI_prev(1:nloc,2))
                      charX(1:nloc,3) = charI(1:nloc,4)
                      Drhs_diag_local((jj-1)*mynloc+iloc(1:nloc)) = Drhs_diag_local((jj-1)*mynloc+iloc(1:nloc)) - (charX_prev(1:nloc,1)+ulc_prev(iloc(1:nloc),jj)*charX(1:nloc,1)) / &
                                                     ulc_prev(iloc(1:nloc),nden) - ulc_prev(iloc(1:nloc),nden)*charX(1:nloc,3)
                   END IF 
                END DO
                IF (ALLOCATED(charI)) DEALLOCATE(charI)
                IF (ALLOCATED(charX)) DEALLOCATE(charX)
                IF (ALLOCATED(charI_prev)) DEALLOCATE(charI_prev)
                IF (ALLOCATED(charX_prev)) DEALLOCATE(charX_prev)
             END IF
          END IF
       END IF
    END DO
  END SUBROUTINE user_evol_LODIavg_BC_Drhs_diag 

  SUBROUTINE user_evol_LODI_BC_rhs (myrhs_loc, ulc, myne_loc, mynloc, myjl, mymeth)
    !--Sets rhs for boundary conditions
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: myne_loc, mynloc, myjl, mymeth
    REAL (pr), DIMENSION (mynloc*myne_loc), INTENT (INOUT) :: myrhs_loc
    REAL (pr), DIMENSION (mynloc,myne_loc), INTENT (IN) :: ulc

    INTEGER :: ie, i, ii, shift, jj
    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc

    REAL (pr), DIMENSION (:),     ALLOCATABLE :: temprhs_loc
    REAL (pr), DIMENSION (:,:),   ALLOCATABLE :: pres
    REAL (pr), DIMENSION (:,:,:), ALLOCATABLE :: du, d2u
    REAL (pr), DIMENSION (:,:),   ALLOCATABLE :: charI,charX,charV
    REAL (pr) :: faceval
    INTEGER :: l, faceint

    IF (ALLOCATED(temprhs_loc)) DEALLOCATE(temprhs_loc)
    ALLOCATE(temprhs_loc(myne_loc*mynloc))
    CALL internal_rhs(temprhs_loc,ulc,1,0)
    i_p_face(0) = 1
    DO i=1,dim
       i_p_face(i) = i_p_face(i-1)*3
    END DO
    DO face_type = 0, 3**dim - 1
       face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
       IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
          CALL get_all_indices_by_face (face_type, myjl, nloc, iloc)
          IF(nloc > 0 ) THEN
             IF (ABS(face(1)) == 1) THEN
                DO jj=1,myne_loc
                   myrhs_loc((jj-1)*mynloc+iloc(1:nloc)) = temprhs_loc((jj-1)*mynloc+iloc(1:nloc))  !from internal_rhs
                END DO
             END IF
          END IF
       END IF
    END DO
    IF (ALLOCATED(temprhs_loc)) DEALLOCATE(temprhs_loc) 

    IF (ALLOCATED(pres)) DEALLOCATE(pres) 
    IF (ALLOCATED(du)) DEALLOCATE(du) 
    IF (ALLOCATED(d2u)) DEALLOCATE(d2u) 
    ALLOCATE(pres(mynloc,locnvar),du(locnvar,mynloc,dim),d2u(locnvar,mynloc,dim))

    d2u(1,:,1) = 1.0_pr
    IF (Nspec>1) d2u(1,:,1) = d2u(1,:,1) - SUM(ulc(:,nspc(1):nspc(Nspecm)),DIM=2)/ulc(:,nden) !Y_Nspec
    d2u(2,:,1) = d2u(1,:,1)*cp_in(Nspec)  !cp_Nspec
    d2u(2,:,2) = d2u(1,:,1)/MW_in(Nspec)  !R_Nspec
    DO l=1,Nspecm
       d2u(2,:,1)  = d2u(2,:,1)  + ulc(:,nspc(l))/ulc(:,nden)*cp_in(l)  !cp
       d2u(2,:,2)  = d2u(2,:,2)  + ulc(:,nspc(l))/ulc(:,nden)/MW_in(l)  !R
    END DO
    d2u(1,:,1) = d2u(2,:,1)/(d2u(2,:,1)-d2u(2,:,2))  !gamma=cp/(cp-R)
    pres(:,1) = (ulc(:,neng)-0.5_pr*SUM(ulc(:,nvel(1):nvel(dim))**2,DIM=2)/ulc(:,nden))*(d2u(1,:,1)-1.0_pr)  !1: pressure
    d2u(1,:,2) = SQRT(d2u(1,:,1)*pres(:,1)/ulc(:,nden))  !c=SQRT(gamma*P/rho)
    DO i=1,dim
       pres(:,1+i) = ulc(:,nvel(i))/ulc(:,nden)  !2--dim+1: u,v(,w)
    END DO
    DO i=1,Nspecm
       pres(:,dim+1+i) = ulc(:,nspc(i))/ulc(:,nden)  !dim+2--dim+Nspec: Y_l
    END DO 
    pres(:,locnvar) = ulc(:,nden) !dim+Nspec+1: rho

    IF ((methpd .ne. 0) .OR. splitFBpress) THEN 
       du(1,:,1) = pres(:,1)
       IF (Nspec>1) THEN
          CALL dpdx (du(1,:,1),ng,pres(:,dim+2:dim+Nspec),2*methpd+mymeth,myjl,splitFBpress)
       ELSE
          CALL dpdx (du(1,:,1),ng,pres(:,1),2*methpd+mymeth,myjl,.FALSE.)
       END IF
       CALL c_diff_fast(pres(:,2:locnvar), du(2:locnvar,:,:), d2u(2:locnvar,:,:), myjl, mynloc, mymeth, 10, locnvar-1, 1, locnvar-1)  
    ELSE
       CALL c_diff_fast(pres(:,1:locnvar), du(1:locnvar,:,:), d2u(1:locnvar,:,:), myjl, mynloc, mymeth, 10, locnvar, 1, locnvar) 
    END IF

    i_p_face(0) = 1
    DO i=1,dim
       i_p_face(i) = i_p_face(i-1)*3
    END DO
    DO face_type = 0, 3**dim - 1
       face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
       IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
          CALL get_all_indices_by_face (face_type, myjl, nloc, iloc)
          IF(nloc > 0 ) THEN
             IF (ABS(face(1)) == 1) THEN
                IF (ALLOCATED(charV)) DEALLOCATE(charV)
                IF (ALLOCATED(charI)) DEALLOCATE(charI)
                IF (ALLOCATED(charX)) DEALLOCATE(charX)
                ALLOCATE(charV(nloc,6),charI(nloc,locnvar),charX(nloc,locnvar))

                faceval=REAL(face(1),pr)
                faceint=(3-face(1))/2
               
                IF (LODIset) THEN
                   charV(:,1) = pres(iloc(1:nloc),2) - d2u(1,iloc(1:nloc),2) !(u-c)
                   charV(:,2) = pres(iloc(1:nloc),2)                         ! u
                   charV(:,3) = pres(iloc(1:nloc),2) + d2u(1,iloc(1:nloc),2) !(u+c)
                   charV(:,2-face(1)) = 0.0_pr
                   charV(:,4:6) = 0.0_pr
                   charV(:,5-face(1)) = 1.0_pr
                ELSE
                   charV(:,1) = faceval*MAX(0.0_pr,faceval*(pres(iloc(1:nloc),2) - d2u(1,iloc(1:nloc),2))) !(u-c)
                   charV(:,2) = faceval*MAX(0.0_pr,faceval* pres(iloc(1:nloc),2)                         ) ! u
                   charV(:,3) = faceval*MAX(0.0_pr,faceval*(pres(iloc(1:nloc),2) + d2u(1,iloc(1:nloc),2))) !(u+c)
                   charV(:,4) = (1.0_pr-SIGN(1.0_pr,faceval*(pres(iloc(1:nloc),2) - d2u(1,iloc(1:nloc),2))))/2.0_pr
                   charV(:,5) = (1.0_pr-SIGN(1.0_pr,faceval* pres(iloc(1:nloc),2)                         ))/2.0_pr 
                   charV(:,6) = (1.0_pr-SIGN(1.0_pr,faceval*(pres(iloc(1:nloc),2) + d2u(1,iloc(1:nloc),2))))/2.0_pr 
                END IF

                charI(1:nloc,2) = charV(1:nloc,2)*(d2u(1,iloc(1:nloc),2)**2*du(locnvar,iloc(1:nloc),1) - du(1,iloc(1:nloc),1))  !u*(c^2*drho/dx - dp/dx)
                DO ii=3,dim+Nspec
                   charI(1:nloc,ii) = charV(1:nloc,2)*du(ii,iloc(1:nloc),1)
                END DO
                charI(1:nloc,locnvar) = charV(1:nloc,3)*(du(1,iloc(1:nloc),1) + ulc(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)*du(2,iloc(1:nloc),1))  !(u+c)*(dp/dx+rho*c*du/dx)
                charI(1:nloc,1)       = charV(1:nloc,1)*(du(1,iloc(1:nloc),1) - ulc(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)*du(2,iloc(1:nloc),1))  !(u-c)*(dp/dx-rho*c*du/dx)
                IF (pertchar) THEN   
                   charI(1:nloc,locnvar) = charI(1:nloc,locnvar) - charV(1:nloc,3) * dP0(faceint)
                   charI(1:nloc,1)       = charI(1:nloc,1)       - charV(1:nloc,1) * dP0(faceint)
                   charI(1:nloc,2)       = charI(1:nloc,2)       - charV(1:nloc,2) * (d2u(1,iloc(1:nloc),2)**2*drho0(faceint) - dP0(faceint))
                END IF
                IF     (PLpinf==1) THEN
                   charI(1:nloc,locnvar) = charI(1:nloc,locnvar) + charV(1:nloc,6)*pBcoef*(1.0_pr-maxMa**2)*c0(faceint)          *(pres(iloc(1:nloc),1) - P0(faceint))
                   charI(1:nloc,1)       = charI(1:nloc,1)       + charV(1:nloc,4)*pBcoef*(1.0_pr-maxMa**2)*c0(faceint)          *(pres(iloc(1:nloc),1) - P0(faceint))
                ELSEIF (PLpinf==2) THEN
                   charI(1:nloc,locnvar) = charI(1:nloc,locnvar) + charV(1:nloc,6)*pBcoef*(1.0_pr-maxMa**2)*d2u(1,iloc(1:nloc),2)*(pres(iloc(1:nloc),1) - P0(faceint))
                   charI(1:nloc,1)       = charI(1:nloc,1)       + charV(1:nloc,4)*pBcoef*(1.0_pr-maxMa**2)*d2u(1,iloc(1:nloc),2)*(pres(iloc(1:nloc),1) - P0(faceint))
                ELSEIF (PLpinf==3) THEN
                   charI(1:nloc,locnvar) = charI(1:nloc,locnvar) + charV(1:nloc,6)*pBcoef*(1.0_pr-maxMa**2)*c0(faceint)*(pres(iloc(1:nloc),1)/ulc(iloc(1:nloc),nden)/d2u(2,iloc(1:nloc),2) - Tconst)
                   charI(1:nloc,1)       = charI(1:nloc,1)       + charV(1:nloc,4)*pBcoef*(1.0_pr-maxMa**2)*c0(faceint)*(pres(iloc(1:nloc),1)/ulc(iloc(1:nloc),nden)/d2u(2,iloc(1:nloc),2) - Tconst)
                ELSEIF (PLpinf==4) THEN
                   charI(1:nloc,locnvar) = charI(1:nloc,locnvar) + charV(1:nloc,6)*pBcoef*(1.0_pr-maxMa**2)*d2u(1,iloc(1:nloc),2) * &
                                                                         (pres(iloc(1:nloc),1) - ulc(iloc(1:nloc),nden)*d2u(2,iloc(1:nloc),2)*Tconst)
                   charI(1:nloc,1)       = charI(1:nloc,1)       + charV(1:nloc,4)*pBcoef*(1.0_pr-maxMa**2)*d2u(1,iloc(1:nloc),2) * &
                                                                         (pres(iloc(1:nloc),1) - ulc(iloc(1:nloc),nden)*d2u(2,iloc(1:nloc),2)*Tconst)
                ELSEIF (PLpinf==5) THEN
                   charI(1:nloc,locnvar) = charI(1:nloc,locnvar) - faceval*charV(1:nloc,6)*pBcoef*(1.0_pr-maxMa**2)*gammBC(faceint)*Pint*pres(iloc(1:nloc),2)
                   charI(1:nloc,1)       = charI(1:nloc,1)       - faceval*charV(1:nloc,4)*pBcoef*(1.0_pr-maxMa**2)*gammBC(faceint)*Pint*pres(iloc(1:nloc),2)
                ELSEIF (PLpinf==6) THEN
                   charI(1:nloc,locnvar) = charI(1:nloc,locnvar) - faceval*charV(1:nloc,6)*pBcoef*(1.0_pr-maxMa**2)*ulc(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)**2*pres(iloc(1:nloc),2)
                   charI(1:nloc,1)       = charI(1:nloc,1)       - faceval*charV(1:nloc,4)*pBcoef*(1.0_pr-maxMa**2)*ulc(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)**2*pres(iloc(1:nloc),2)
                END IF

                charX(1:nloc,1) = 0.5_pr/d2u(1,iloc(1:nloc),2)**2*(charI(1:nloc,1)+charI(1:nloc,locnvar)+2.0_pr*charI(1:nloc,2))
                charX(1:nloc,2) = 0.5_pr/d2u(1,iloc(1:nloc),2)/ulc(iloc(1:nloc),nden)*(charI(1:nloc,locnvar)-charI(1:nloc,1))
                DO ii=3,dim+Nspec
                   charX(1:nloc,ii) = charI(1:nloc,ii)
                END DO
                charX(1:nloc,locnvar) = 0.5_pr*(charI(1:nloc,locnvar)+charI(1:nloc,1))
                IF (pertchar) THEN   
                   charX(1:nloc,1)       = charX(1:nloc,1)       + pres(iloc(1:nloc),2)*drho0(faceint)
                   IF (usedP0) THEN
                      charX(1:nloc,2)    = charX(1:nloc,2)       + dP0(faceint)/ulc(iloc(1:nloc),nden)
                   ELSE
                      charX(1:nloc,2)    = charX(1:nloc,2)       - rhoYg(faceint)/ulc(iloc(1:nloc),nden) 
                   END IF
                   charX(1:nloc,locnvar) = charX(1:nloc,locnvar) + dP0(faceint)*pres(iloc(1:nloc),2)
                END IF

                myrhs_loc((nden-1)*mynloc+iloc(1:nloc)) = myrhs_loc((nden-1)*mynloc+iloc(1:nloc)) - charX(1:nloc,1)
                DO ii=nvel(1),nvel(dim)
                   myrhs_loc((ii-1)*mynloc+iloc(1:nloc)) = myrhs_loc((ii-1)*mynloc+iloc(1:nloc)) - pres(iloc(1:nloc),ii-nvel(1)+2)*charX(1:nloc,1) &
                                                                                                 - ulc(iloc(1:nloc),nden)*charX(1:nloc,ii-nvel(1)+2)
                END DO
                myrhs_loc((neng-1)*mynloc+iloc(1:nloc)) = myrhs_loc((neng-1)*mynloc+iloc(1:nloc)) - 0.5_pr*SUM(pres(iloc(1:nloc),2:dim+1)**2,DIM=2)*charX(1:nloc,1) &
                             - SUM(ulc(iloc(1:nloc),nvel(1):nvel(dim))*charX(1:nloc,2:dim+1),DIM=2) - 1.0_pr/(d2u(1,iloc(1:nloc),1)-1.0_pr)*charX(1:nloc,locnvar) 
                DO ii=1,Nspecm
                   myrhs_loc((nspc(ii)-1)*mynloc+iloc(1:nloc)) = myrhs_loc((nspc(ii)-1)*mynloc+iloc(1:nloc)) - pres(iloc(1:nloc),ii-1+dim+2)*charX(1:nloc,1) &
                                                                                                 - ulc(iloc(1:nloc),nden)*charX(1:nloc,ii-1+dim+2)
                END DO
                IF (ALLOCATED(charV)) DEALLOCATE(charV)
                IF (ALLOCATED(charI)) DEALLOCATE(charI)
                IF (ALLOCATED(charX)) DEALLOCATE(charX) 
             END IF
          END IF
       END IF
    END DO
    IF (ALLOCATED(pres)) DEALLOCATE(pres) 
    IF (ALLOCATED(du)) DEALLOCATE(du) 
    IF (ALLOCATED(d2u)) DEALLOCATE(d2u) 
  END SUBROUTINE user_evol_LODI_BC_rhs 

  SUBROUTINE user_evol_LODI_BC_Drhs (Dmyrhs_loc, ulc, ulc_prev, myne_loc, mynloc, myjl, mymeth)
    !--Sets rhs for boundary conditions
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: myne_loc, mynloc, myjl, mymeth
    REAL (pr), DIMENSION (mynloc*myne_loc), INTENT (INOUT) :: Dmyrhs_loc
    REAL (pr), DIMENSION (mynloc,myne_loc), INTENT (IN) :: ulc, ulc_prev

    INTEGER :: ie, i, ii, shift, jj
    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc

    REAL (pr), DIMENSION (:),     ALLOCATABLE :: tempDrhs_loc
    REAL (pr), DIMENSION (:,:),   ALLOCATABLE :: pres, pres_prev
    REAL (pr), DIMENSION (:,:,:), ALLOCATABLE :: du_prev, du, d2u
    REAL (pr), DIMENSION (:,:),   ALLOCATABLE :: charV, charI, charX, charV_prev, charI_prev, charX_prev
    
    REAL (pr) :: faceval
    INTEGER :: l, faceint

    IF (ALLOCATED(tempDrhs_loc)) DEALLOCATE(tempDrhs_loc)
    ALLOCATE(tempDrhs_loc(myne_loc*mynloc))
    CALL internal_Drhs(tempDrhs_loc,ulc,ulc_prev,mymeth,1,0)
    i_p_face(0) = 1
    DO i=1,dim
       i_p_face(i) = i_p_face(i-1)*3
    END DO
    DO face_type = 0, 3**dim - 1
       face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
       IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
          CALL get_all_indices_by_face (face_type, myjl, nloc, iloc)
          IF(nloc > 0 ) THEN
             IF (ABS(face(1)) == 1) THEN
                DO jj=1,myne_loc
                   Dmyrhs_loc((jj-1)*mynloc+iloc(1:nloc)) = tempDrhs_loc((jj-1)*mynloc+iloc(1:nloc))  !from internal_Drhs
                END DO
             END IF
          END IF
       END IF
    END DO
    IF (ALLOCATED(tempDrhs_loc)) DEALLOCATE(tempDrhs_loc) 

    IF (ALLOCATED(pres)) DEALLOCATE(pres) 
    IF (ALLOCATED(pres_prev)) DEALLOCATE(pres_prev) 
    IF (ALLOCATED(du)) DEALLOCATE(du) 
    IF (ALLOCATED(du_prev)) DEALLOCATE(du_prev) 
    IF (ALLOCATED(d2u)) DEALLOCATE(d2u) 
    ALLOCATE(pres(mynloc,locnvar),pres_prev(mynloc,locnvar),du(locnvar,mynloc,dim),du_prev(locnvar,mynloc,dim),d2u(locnvar,mynloc,dim))

    d2u(1,:,1) = 1.0_pr
    d2u(1,:,2) = 0.0_pr 
    IF (Nspec>1) d2u(1,:,1) = du(1,:,1)-SUM(ulc_prev(:,nspc(1):nspc(Nspecm)),DIM=2)/ulc_prev(:,nden) !Y0_Nspec
    IF (Nspec>1) d2u(1,:,2) = (SUM(ulc_prev(:,nspc(1):nspc(Nspecm)),DIM=2)*ulc(:,nden)/ulc_prev(:,nden) - SUM(ulc(:,nspc(1):nspc(Nspecm)),DIM=2))/ulc_prev(:,nden) !Y'_Nspec 
    d2u(4,:,1) = d2u(1,:,2)*cp_in(Nspec)  !cp'_Nspec
    d2u(4,:,2) = d2u(1,:,2)/MW_in(Nspec)  !R'_Nspec
    d2u(2,:,1) = d2u(1,:,1)*cp_in(Nspec)  !cp0_Nspec
    d2u(2,:,2) = d2u(1,:,1)/MW_in(Nspec)  !R0_Nspec
    DO l=1,Nspecm
       d2u(2,:,1)  = d2u(2,:,1)  + ulc_prev(:,nspc(l))/ulc_prev(:,nden)*cp_in(l)  !cp0
       d2u(2,:,2)  = d2u(2,:,2)  + ulc_prev(:,nspc(l))/ulc_prev(:,nden)/MW_in(l)  !R0
       d2u(4,:,1)  = d2u(4,:,1)  + (ulc(:,nspc(l))-ulc_prev(:,nspc(l))/ulc_prev(:,nden)*ulc(:,nden))/ulc_prev(:,nden)*cp_in(l)  !cp'
       d2u(4,:,2)  = d2u(4,:,2)  + (ulc(:,nspc(l))-ulc_prev(:,nspc(l))/ulc_prev(:,nden)*ulc(:,nden))/ulc_prev(:,nden)/MW_in(l)  !R'
    END DO
    d2u(1,:,1) = d2u(2,:,1)/(d2u(2,:,1)-d2u(2,:,2))  !gamma0=cp0/(cp0-R0)
    pres_prev(:,1) = (ulc_prev(:,neng)-0.5_pr*SUM(ulc_prev(:,nvel(1):nvel(dim))**2,DIM=2)/ulc_prev(:,nden))*(d2u(1,:,1)-1.0_pr)  !1: p0
    d2u(1,:,2) = SQRT(d2u(1,:,1)*pres_prev(:,1)/ulc_prev(:,nden))  !c0=SQRT(gamma0*P0/rho0) 
    d2u(3,:,1) = (d2u(2,:,1)*d2u(4,:,2) - d2u(4,:,1)*d2u(2,:,2)) / (d2u(2,:,1)-d2u(2,:,2))**2  !gamma'=(cp0*R'-cp'*R0)/(cp0-R0)^2
    pres(:,1) =  d2u(3,:,1)*(ulc_prev(:,neng)-0.5_pr*SUM(ulc_prev(:,nvel(1):nvel(dim))**2,DIM=2))/ulc_prev(:,nden) +  &
                (d2u(1,:,1)-1.0_pr)*(ulc(:,neng)+0.5_pr*SUM(ulc_prev(:,nvel(1):nvel(dim))**2,DIM=2)/ulc_prev(:,nden)**2*ulc(:,nden) -  &
                 SUM(ulc_prev(:,nvel(1):nvel(dim))*ulc(:,nvel(1):nvel(dim)),DIM=2)/ulc_prev(:,nden) )  !1: p'
    d2u(3,:,2) = 0.5_pr/SQRT(d2u(1,:,1)*pres_prev(:,1)/ulc_prev(:,nden)) * ( d2u(1,:,1)*pres(:,1)+d2u(3,:,1)*pres_prev(:,1) - &
                        d2u(1,:,1)*pres_prev(:,1)/ulc_prev(:,nden)*ulc(:,nden) )/ulc_prev(:,nden)   !c'=1/2/SQRT(gamma0*P0/rho0)*(gamma*P/rho)'
    DO i=1,dim
       pres_prev(:,1+i) = ulc_prev(:,nvel(i))/ulc_prev(:,nden)  !2--dim+1: u0
       pres(:,1+i) = (ulc(:,nvel(i)) - ulc_prev(:,nvel(i))*ulc(:,nden)/ulc_prev(:,nden) )/ulc_prev(:,nden) !2--dim+1: u'
    END DO
    DO i=1,Nspecm
       pres_prev(:,dim+1+i) = ulc_prev(:,nspc(i))/ulc_prev(:,nden)  !dim+2--dim+Nspec: Y0
       pres(:,dim+1+i) = (ulc(:,nspc(i)) - ulc_prev(:,nspc(i))*ulc(:,nden)/ulc_prev(:,nden) )/ulc_prev(:,nden) !dim+2--dim+Nspec: Y'
    END DO 
    pres_prev(:,locnvar) = ulc_prev(:,nden) !dim+Nspec+1: rho0
    pres(:,locnvar) = ulc(:,nden) !dim+Nspec+1: rho'

    IF ((methpd .ne. 0) .OR. splitFBpress) THEN 
       du(1,:,1) = pres(:,1)
       IF (Nspec>1) THEN
          CALL dpdx (du(1,:,1),ng,pres_prev(:,dim+2:dim+Nspec),2*methpd+mymeth,myjl,splitFBpress)
       ELSE
          CALL dpdx (du(1,:,1),ng,pres_prev(:,1),2*methpd+mymeth,myjl,.FALSE.)
       END IF
       CALL c_diff_fast(pres(:,2:locnvar), du(2:locnvar,:,:), d2u(2:locnvar,:,:), myjl, mynloc, mymeth, 10, locnvar-1, 1, locnvar-1)  
    ELSE
       CALL c_diff_fast(pres(:,1:locnvar), du(1:locnvar,:,:), d2u(1:locnvar,:,:), myjl, mynloc, mymeth, 10, locnvar, 1, locnvar) 
    END IF
    IF ((methpd .ne. 0) .OR. splitFBpress) THEN 
       du_prev(1,:,1) = pres_prev(:,1)
       IF (Nspec>1) THEN
          CALL dpdx (du_prev(1,:,1),ng,pres_prev(:,dim+2:dim+Nspec),2*methpd+mymeth,myjl,splitFBpress)
       ELSE
          CALL dpdx (du_prev(1,:,1),ng,pres_prev(:,1),2*methpd+mymeth,myjl,.FALSE.)
       END IF
       CALL c_diff_fast(pres_prev(:,2:locnvar), du_prev(2:locnvar,:,:), d2u(2:locnvar,:,:), myjl, mynloc, mymeth, 10, locnvar-1, 1, locnvar-1)  
    ELSE
       CALL c_diff_fast(pres_prev(:,1:locnvar), du_prev(1:locnvar,:,:), d2u(1:locnvar,:,:), myjl, mynloc, mymeth, 10, locnvar  , 1, locnvar  ) 
    END IF

    i_p_face(0) = 1
    DO i=1,dim
       i_p_face(i) = i_p_face(i-1)*3
    END DO
    DO face_type = 0, 3**dim - 1
       face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
       IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
          CALL get_all_indices_by_face (face_type, myjl, nloc, iloc)
          IF(nloc > 0 ) THEN
             IF (ABS(face(1)) == 1) THEN
                IF (ALLOCATED(charV)) DEALLOCATE(charV)
                IF (ALLOCATED(charI)) DEALLOCATE(charI)
                IF (ALLOCATED(charX)) DEALLOCATE(charX)
                IF (ALLOCATED(charV_prev)) DEALLOCATE(charV_prev)
                IF (ALLOCATED(charI_prev)) DEALLOCATE(charI_prev)
                IF (ALLOCATED(charX_prev)) DEALLOCATE(charX_prev)
                ALLOCATE(charV(nloc,3),charI(nloc,locnvar),charX(nloc,locnvar),charV_prev(nloc,6),charI_prev(nloc,locnvar),charX_prev(nloc,locnvar))

                faceval=REAL(face(1),pr)
                faceint=(3-face(1))/2
               
                IF (LODIset) THEN
                   charV_prev(:,1) = pres_prev(iloc(1:nloc),2) - d2u(1,iloc(1:nloc),2) !(u-c)
                   charV_prev(:,2) = pres_prev(iloc(1:nloc),2)                         ! u
                   charV_prev(:,3) = pres_prev(iloc(1:nloc),2) + d2u(1,iloc(1:nloc),2) !(u+c)
                   charV_prev(:,2-face(1)) = 0.0_pr
                   charV_prev(:,4:6) = 0.0_pr
                   charV_prev(:,5-face(1)) = 1.0_pr
                   charV(:,1) = pres(iloc(1:nloc),2) - d2u(3,iloc(1:nloc),2) !(u-c)
                   charV(:,2) = pres(iloc(1:nloc),2)                         ! u
                   charV(:,3) = pres(iloc(1:nloc),2) + d2u(3,iloc(1:nloc),2) !(u+c)
                   charV(:,2-face(1)) = 0.0_pr
                ELSE
                   charV_prev(:,1) = faceval*MAX(0.0_pr,faceval*(pres_prev(iloc(1:nloc),2) - d2u(1,iloc(1:nloc),2))) !(u-c)
                   charV_prev(:,2) = faceval*MAX(0.0_pr,faceval* pres_prev(iloc(1:nloc),2)                         ) ! u
                   charV_prev(:,3) = faceval*MAX(0.0_pr,faceval*(pres_prev(iloc(1:nloc),2) + d2u(1,iloc(1:nloc),2))) !(u+c)
                   charV_prev(:,4) = (1.0_pr-SIGN(1.0_pr,faceval*(pres_prev(iloc(1:nloc),2) - d2u(1,iloc(1:nloc),2))))/2.0_pr
                   charV_prev(:,5) = (1.0_pr-SIGN(1.0_pr,faceval* pres_prev(iloc(1:nloc),2)                         ))/2.0_pr 
                   charV_prev(:,6) = (1.0_pr-SIGN(1.0_pr,faceval*(pres_prev(iloc(1:nloc),2) + d2u(1,iloc(1:nloc),2))))/2.0_pr 
                   charV(:,1) = (1.0_pr+SIGN(1.0_pr,faceval*(pres_prev(iloc(1:nloc),2) - d2u(1,iloc(1:nloc),2))))/2.0_pr * (pres(iloc(1:nloc),2) - d2u(3,iloc(1:nloc),2))
                   charV(:,2) = (1.0_pr+SIGN(1.0_pr,faceval* pres_prev(iloc(1:nloc),2)                         ))/2.0_pr * pres(iloc(1:nloc),2)
                   charV(:,3) = (1.0_pr+SIGN(1.0_pr,faceval*(pres_prev(iloc(1:nloc),2) + d2u(1,iloc(1:nloc),2))))/2.0_pr * (pres(iloc(1:nloc),2) + d2u(3,iloc(1:nloc),2))
                END IF

                charI(1:nloc,2) = charV_prev(1:nloc,2)*(d2u(1,iloc(1:nloc),2)**2*du(locnvar,iloc(1:nloc),1) - du(1,iloc(1:nloc),1))
                DO ii=3,dim+Nspec
                   charI(1:nloc,ii) = charV_prev(1:nloc,2)*du(ii,iloc(1:nloc),1)
                END DO
                charI(1:nloc,locnvar) = charV_prev(1:nloc,3)*(du(1,iloc(1:nloc),1) + ulc_prev(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)*du(2,iloc(1:nloc),1)) 
                charI(1:nloc,1) =       charV_prev(1:nloc,1)*(du(1,iloc(1:nloc),1) - ulc_prev(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)*du(2,iloc(1:nloc),1)) 

                charI_prev(1:nloc,2) = charV_prev(1:nloc,2)*(d2u(1,iloc(1:nloc),2)**2*du_prev(locnvar,iloc(1:nloc),1) - du_prev(1,iloc(1:nloc),1)) !u*(c^2*drho/dx - dp/dx)
                DO ii=3,dim+Nspec
                   charI_prev(1:nloc,ii) = charV_prev(1:nloc,2)*du_prev(ii,iloc(1:nloc),1)
                END DO
                charI_prev(1:nloc,locnvar) = charV_prev(1:nloc,3)*(du_prev(1,iloc(1:nloc),1)+ulc_prev(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)*du_prev(2,iloc(1:nloc),1))  !(u+c)*(dp/dx+rho*c*du/dx)
                charI_prev(1:nloc,1)       = charV_prev(1:nloc,1)*(du_prev(1,iloc(1:nloc),1)-ulc_prev(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)*du_prev(2,iloc(1:nloc),1))  !(u-c)*(dp/dx-rho*c*du/dx)

                IF (pertchar) THEN   
                   charI_prev(1:nloc,locnvar) = charI_prev(1:nloc,locnvar) - charV_prev(1:nloc,3) * dP0(faceint)
                   charI_prev(1:nloc,1)       = charI_prev(1:nloc,1)       - charV_prev(1:nloc,1) * dP0(faceint)
                   charI_prev(1:nloc,2)       = charI_prev(1:nloc,2)       - charV_prev(1:nloc,2) * (d2u(1,iloc(1:nloc),2)**2*drho0(faceint) - dP0(faceint))
                END IF

                charI(1:nloc,2) = charI(1:nloc,2) + charV(1:nloc,2)*(d2u(1,iloc(1:nloc),2)**2*du_prev(locnvar,iloc(1:nloc),1) - du_prev(1,iloc(1:nloc),1)) !u*(c^2*drho/dx - dp/dx)
                DO ii=3,dim+Nspec
                   charI(1:nloc,ii) = charI(1:nloc,ii) + charV(1:nloc,2)*du_prev(ii,iloc(1:nloc),1)
                END DO
                charI(1:nloc,locnvar) = charI(1:nloc,locnvar) + charV(1:nloc,3)*(du_prev(1,iloc(1:nloc),1) + ulc_prev(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)*du_prev(2,iloc(1:nloc),1))  
                                                                                                                                                           !(u+c)*(dp/dx+rho*c*du/dx)
                charI(1:nloc,1)       = charI(1:nloc,1)       + charV(1:nloc,1)*(du_prev(1,iloc(1:nloc),1) - ulc_prev(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)*du_prev(2,iloc(1:nloc),1))  
                                                                                                                                                           !(u-c)*(dp/dx-rho*c*du/dx)
                charI(1:nloc,2) = charI(1:nloc,2) + charV_prev(1:nloc,2)*2.0_pr*d2u(1,iloc(1:nloc),2)*d2u(3,iloc(1:nloc),2)*du_prev(locnvar,iloc(1:nloc),1)

                charI(1:nloc,locnvar) = charI(1:nloc,locnvar) + charV_prev(1:nloc,3)*du_prev(2,iloc(1:nloc),1) * &
                                                                (ulc(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)+ulc_prev(iloc(1:nloc),nden)*d2u(3,iloc(1:nloc),2)) 
                charI(1:nloc,1)       = charI(1:nloc,1)       - charV_prev(1:nloc,1)*du_prev(2,iloc(1:nloc),1) * &
                                                                (ulc(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)+ulc_prev(iloc(1:nloc),nden)*d2u(3,iloc(1:nloc),2)) 

                IF (pertchar) THEN   
                   charI(1:nloc,locnvar) = charI(1:nloc,locnvar) - charV(1:nloc,3) * dP0(faceint)
                   charI(1:nloc,1)       = charI(1:nloc,1)       - charV(1:nloc,1) * dP0(faceint)
                   charI(1:nloc,2)       = charI(1:nloc,2)       - charV(1:nloc,2) * (d2u(1,iloc(1:nloc),2)**2*drho0(faceint) - dP0(faceint)) &
                                                                 - charV_prev(1:nloc,2) * 2.0_pr*d2u(1,iloc(1:nloc),2)*d2u(3,iloc(1:nloc),2)*drho0(faceint) 
                END IF

                IF     (PLpinf==1) THEN
                   charI_prev(1:nloc,locnvar) = charI_prev(1:nloc,locnvar) + charV_prev(1:nloc,6)*pBcoef*(1.0_pr-maxMa**2)*c0(faceint)*(pres_prev(iloc(1:nloc),1) - P0(faceint))
                   charI_prev(1:nloc,1)       = charI_prev(1:nloc,1)       + charV_prev(1:nloc,4)*pBcoef*(1.0_pr-maxMa**2)*c0(faceint)*(pres_prev(iloc(1:nloc),1) - P0(faceint))
                   charI(1:nloc,locnvar)      = charI(1:nloc,locnvar)      + charV_prev(1:nloc,6)*pBcoef*(1.0_pr-maxMa**2)*c0(faceint)*pres(iloc(1:nloc),1)
                   charI(1:nloc,1)            = charI(1:nloc,1)            + charV_prev(1:nloc,4)*pBcoef*(1.0_pr-maxMa**2)*c0(faceint)*pres(iloc(1:nloc),1)
                ELSEIF (PLpinf==2) THEN
                   charI_prev(1:nloc,locnvar) = charI_prev(1:nloc,locnvar) + charV_prev(1:nloc,6)*pBcoef*(1.0_pr-maxMa**2)*d2u(1,iloc(1:nloc),2)*(pres_prev(iloc(1:nloc),1) - P0(faceint))
                   charI_prev(1:nloc,1)       = charI_prev(1:nloc,1)       + charV_prev(1:nloc,4)*pBcoef*(1.0_pr-maxMa**2)*d2u(1,iloc(1:nloc),2)*(pres_prev(iloc(1:nloc),1) - P0(faceint))
                   charI(1:nloc,locnvar)      = charI(1:nloc,locnvar)      + charV_prev(1:nloc,6)*pBcoef*(1.0_pr-maxMa**2) * &
                                                                             (d2u(3,iloc(1:nloc),2)*(pres_prev(iloc(1:nloc),1) - P0(faceint)) + d2u(1,iloc(1:nloc),2)*pres(iloc(1:nloc),1))
                   charI(1:nloc,1)            = charI(1:nloc,1)            + charV_prev(1:nloc,4)*pBcoef*(1.0_pr-maxMa**2) * &
                                                                             (d2u(3,iloc(1:nloc),2)*(pres_prev(iloc(1:nloc),1) - P0(faceint)) + d2u(1,iloc(1:nloc),2)*pres(iloc(1:nloc),1))
                ELSEIF (PLpinf==3) THEN
                   charI_prev(1:nloc,locnvar) = charI_prev(1:nloc,locnvar) + charV_prev(1:nloc,6)*pBcoef*(1.0_pr-maxMa**2)*c0(faceint) * &
                                                                              (pres_prev(iloc(1:nloc),1)/ulc_prev(iloc(1:nloc),nden)/d2u(2,iloc(1:nloc),2) - Tconst)
                   charI_prev(1:nloc,1)       = charI_prev(1:nloc,1)       + charV_prev(1:nloc,4)*pBcoef*(1.0_pr-maxMa**2)*c0(faceint) * &
                                                                              (pres_prev(iloc(1:nloc),1)/ulc_prev(iloc(1:nloc),nden)/d2u(2,iloc(1:nloc),2) - Tconst)
                   charI(1:nloc,locnvar)      = charI(1:nloc,locnvar)      + charV_prev(1:nloc,6)*pBcoef*(1.0_pr-maxMa**2)*c0(faceint)/ulc_prev(iloc(1:nloc),nden)/d2u(2,iloc(1:nloc),2) * &
                                        (pres(iloc(1:nloc),1)-pres_prev(iloc(1:nloc),1)*(ulc(iloc(1:nloc),nden)/ulc_prev(iloc(1:nloc),nden)+d2u(4,iloc(1:nloc),2)/d2u(2,iloc(1:nloc),2)) )
                   charI(1:nloc,1)            = charI(1:nloc,1)            + charV_prev(1:nloc,4)*pBcoef*(1.0_pr-maxMa**2)*c0(faceint)/ulc_prev(iloc(1:nloc),nden)/d2u(2,iloc(1:nloc),2) * &
                                        (pres(iloc(1:nloc),1)-pres_prev(iloc(1:nloc),1)*(ulc(iloc(1:nloc),nden)/ulc_prev(iloc(1:nloc),nden)+d2u(4,iloc(1:nloc),2)/d2u(2,iloc(1:nloc),2)) )
                ELSEIF (PLpinf==4) THEN
                   charI_prev(1:nloc,locnvar) = charI_prev(1:nloc,locnvar) + charV_prev(1:nloc,6)*pBcoef*(1.0_pr-maxMa**2)*d2u(1,iloc(1:nloc),2) * &
                                                                         (pres_prev(iloc(1:nloc),1) - ulc_prev(iloc(1:nloc),nden)*d2u(2,iloc(1:nloc),2)*Tconst)
                   charI_prev(1:nloc,1)       = charI_prev(1:nloc,1)       + charV_prev(1:nloc,4)*pBcoef*(1.0_pr-maxMa**2)*d2u(1,iloc(1:nloc),2) * &
                                                                         (pres_prev(iloc(1:nloc),1) - ulc_prev(iloc(1:nloc),nden)*d2u(2,iloc(1:nloc),2)*Tconst)
                   charI(1:nloc,locnvar)      = charI(1:nloc,locnvar)      + charV_prev(1:nloc,6)*pBcoef*(1.0_pr-maxMa**2) * &
                                                              (d2u(3,iloc(1:nloc),2)*(pres_prev(iloc(1:nloc),1) - ulc_prev(iloc(1:nloc),nden)*d2u(2,iloc(1:nloc),2)*Tconst) + &
                                     d2u(1,iloc(1:nloc),2)*(pres(iloc(1:nloc),1) - ulc(iloc(1:nloc),nden)*d2u(2,iloc(1:nloc),2)*Tconst - ulc_prev(iloc(1:nloc),nden)*d2u(4,iloc(1:nloc),2)*Tconst) )
                   charI(1:nloc,1)            = charI(1:nloc,1)            + charV_prev(1:nloc,4)*pBcoef*(1.0_pr-maxMa**2) * &
                                                              (d2u(3,iloc(1:nloc),2)*(pres_prev(iloc(1:nloc),1) - ulc_prev(iloc(1:nloc),nden)*d2u(2,iloc(1:nloc),2)*Tconst) + &
                                     d2u(1,iloc(1:nloc),2)*(pres(iloc(1:nloc),1) - ulc(iloc(1:nloc),nden)*d2u(2,iloc(1:nloc),2)*Tconst - ulc_prev(iloc(1:nloc),nden)*d2u(4,iloc(1:nloc),2)*Tconst) )
                ELSEIF (PLpinf==5) THEN
                   charI_prev(1:nloc,locnvar) = charI_prev(1:nloc,locnvar) - faceval*charV_prev(1:nloc,6)*pBcoef*(1.0_pr-maxMa**2)*gammBC(faceint)*Pint*pres_prev(iloc(1:nloc),2)
                   charI_prev(1:nloc,1)       = charI_prev(1:nloc,1)       - faceval*charV_prev(1:nloc,4)*pBcoef*(1.0_pr-maxMa**2)*gammBC(faceint)*Pint*pres_prev(iloc(1:nloc),2)
                   charI(1:nloc,locnvar)      = charI(1:nloc,locnvar)      - faceval*charV_prev(1:nloc,6)*pBcoef*(1.0_pr-maxMa**2)*gammBC(faceint)*Pint*pres(iloc(1:nloc),2)
                   charI(1:nloc,1)            = charI(1:nloc,1)            - faceval*charV_prev(1:nloc,4)*pBcoef*(1.0_pr-maxMa**2)*gammBC(faceint)*Pint*pres(iloc(1:nloc),2)
                ELSEIF (PLpinf==6) THEN
                   charI_prev(1:nloc,locnvar) = charI_prev(1:nloc,locnvar) - faceval*charV_prev(1:nloc,6)*pBcoef*(1.0_pr-maxMa**2) * &
                                                                                                      ulc_prev(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)**2*pres_prev(iloc(1:nloc),2)
                   charI_prev(1:nloc,1)       = charI_prev(1:nloc,1)       - faceval*charV_prev(1:nloc,4)*pBcoef*(1.0_pr-maxMa**2) * &
                                                                                                      ulc_prev(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)**2*pres_prev(iloc(1:nloc),2)
                   charI(1:nloc,locnvar)      = charI(1:nloc,locnvar)      - faceval*charV_prev(1:nloc,6)*pBcoef*(1.0_pr-maxMa**2)*d2u(1,iloc(1:nloc),2) * &
                                                   (ulc(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)*pres_prev(iloc(1:nloc),2) + &
                                                    2.0_pr*ulc_prev(iloc(1:nloc),nden)*d2u(3,iloc(1:nloc),2)*pres_prev(iloc(1:nloc),2) + &
                                                    ulc_prev(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)*pres(iloc(1:nloc),2) )
                   charI(1:nloc,1)            = charI(1:nloc,1)            - faceval*charV_prev(1:nloc,4)*pBcoef*(1.0_pr-maxMa**2)*d2u(1,iloc(1:nloc),2) * &
                                                   (ulc(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)*pres_prev(iloc(1:nloc),2) + &
                                                    2.0_pr*ulc_prev(iloc(1:nloc),nden)*d2u(3,iloc(1:nloc),2)*pres_prev(iloc(1:nloc),2) + &
                                                    ulc_prev(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)*pres(iloc(1:nloc),2) )
                END IF

                charX_prev(1:nloc,1) = 0.5_pr/d2u(1,iloc(1:nloc),2)**2*(charI_prev(1:nloc,1)+charI_prev(1:nloc,locnvar)+2.0_pr*charI_prev(1:nloc,2))
                charX_prev(1:nloc,2) = 0.5_pr/d2u(1,iloc(1:nloc),2)/ulc_prev(iloc(1:nloc),nden)*(charI_prev(1:nloc,locnvar)-charI_prev(1:nloc,1))
                DO ii=3,dim+Nspec
                   charX_prev(1:nloc,ii) = charI_prev(1:nloc,ii)
                END DO
                charX_prev(1:nloc,locnvar) = 0.5_pr*(charI_prev(1:nloc,locnvar)+charI_prev(1:nloc,1))

                IF (pertchar) THEN   
                   charX_prev(1:nloc,1)       = charX_prev(1:nloc,1)       + pres_prev(iloc(1:nloc),2)*drho0(faceint)
                   IF (usedP0) THEN
                      charX_prev(1:nloc,2)    = charX_prev(1:nloc,2)       + dP0(faceint)/ulc_prev(iloc(1:nloc),nden)
                   ELSE
                      charX_prev(1:nloc,2)    = charX_prev(1:nloc,2)       - rhoYg(faceint)/ulc_prev(iloc(1:nloc),nden)
                   END IF
                   charX_prev(1:nloc,locnvar) = charX_prev(1:nloc,locnvar) + dP0(faceint)*pres_prev(iloc(1:nloc),2)
                END IF

                charX(1:nloc,1) = 0.5_pr/d2u(1,iloc(1:nloc),2)**2*(charI(1:nloc,1)+charI(1:nloc,locnvar)+2.0_pr*charI(1:nloc,2)) &
                                  - d2u(3,iloc(1:nloc),2)/d2u(1,iloc(1:nloc),2)**3*(charI_prev(1:nloc,1)+charI_prev(1:nloc,locnvar)+2.0_pr*charI_prev(1:nloc,2))
                charX(1:nloc,2) = 0.5_pr/d2u(1,iloc(1:nloc),2)/ulc_prev(iloc(1:nloc),nden) * &
                       (charI(1:nloc,locnvar) - charI(1:nloc,1) - (ulc(iloc(1:nloc),nden)/ulc_prev(iloc(1:nloc),nden) + d2u(3,iloc(1:nloc),2)/d2u(1,iloc(1:nloc),2)) * &
                       (charI_prev(1:nloc,locnvar)-charI_prev(1:nloc,1)))
                DO ii=3,dim+Nspec
                   charX(1:nloc,ii) = charI(1:nloc,ii)
                END DO
                charX(1:nloc,locnvar) = 0.5_pr*(charI(1:nloc,locnvar)+charI(1:nloc,1))

                IF (pertchar) THEN   
                   charX(1:nloc,1)       = charX(1:nloc,1)       + pres(iloc(1:nloc),2)*drho0(faceint)
                   IF (usedP0) THEN
                      charX(1:nloc,2)    = charX(1:nloc,2)       - dP0(faceint)/ulc_prev(iloc(1:nloc),nden)**2*ulc(iloc(1:nloc),nden)
                   ELSE
                      charX(1:nloc,2)    = charX(1:nloc,2)       + rhoYg(faceint)/ulc_prev(iloc(1:nloc),nden)**2*ulc(iloc(1:nloc),nden)
                   END IF
                   charX(1:nloc,locnvar) = charX(1:nloc,locnvar) + dP0(faceint)*pres(iloc(1:nloc),2)
                END IF

                Dmyrhs_loc((nden-1)*mynloc+iloc(1:nloc)) = Dmyrhs_loc((nden-1)*mynloc+iloc(1:nloc)) - charX(1:nloc,1)
                DO ii=nvel(1),nvel(dim)
                   Dmyrhs_loc((ii-1)*mynloc+iloc(1:nloc)) = Dmyrhs_loc((ii-1)*mynloc+iloc(1:nloc))  & 
                                                            - pres_prev(iloc(1:nloc),ii-nvel(1)+2)*charX(1:nloc,1) - pres(iloc(1:nloc),ii-nvel(1)+2)*charX_prev(1:nloc,1) &
                                                            - ulc_prev(iloc(1:nloc),nden)*charX(1:nloc,ii-nvel(1)+2) - ulc(iloc(1:nloc),nden)*charX_prev(1:nloc,ii-nvel(1)+2)
                END DO
                Dmyrhs_loc((neng-1)*mynloc+iloc(1:nloc)) = Dmyrhs_loc((neng-1)*mynloc+iloc(1:nloc)) &
                      - 0.5_pr*SUM(pres_prev(iloc(1:nloc),2:dim+1)**2,DIM=2)*charX(1:nloc,1) - SUM(pres(iloc(1:nloc),2:dim+1)*pres_prev(iloc(1:nloc),2:dim+1),DIM=2)*charX_prev(1:nloc,1) &
                      - SUM(ulc_prev(iloc(1:nloc),nvel(1):nvel(dim))*charX(1:nloc,2:dim+1),DIM=2) - SUM(ulc(iloc(1:nloc),nvel(1):nvel(dim))*charX_prev(1:nloc,2:dim+1),DIM=2) &
                      - 1.0_pr/(d2u(1,iloc(1:nloc),1)-1.0_pr)*(charX(1:nloc,locnvar) - d2u(3,iloc(1:nloc),1)/(d2u(1,iloc(1:nloc),1)-1.0_pr)*charX_prev(1:nloc,locnvar)) 
                DO ii=1,Nspecm
                   Dmyrhs_loc((nspc(ii)-1)*mynloc+iloc(1:nloc)) = Dmyrhs_loc((nspc(ii)-1)*mynloc+iloc(1:nloc))  & 
                                                            - pres_prev(iloc(1:nloc),ii-1+dim+2)*charX(1:nloc,1) - pres(iloc(1:nloc),ii-1+dim+2)*charX_prev(1:nloc,1) &
                                                            - ulc_prev(iloc(1:nloc),nden)*charX(1:nloc,ii-1+dim+2) - ulc(iloc(1:nloc),nden)*charX_prev(1:nloc,ii-1+dim+2)
                END DO

                IF (ALLOCATED(charV)) DEALLOCATE(charV)
                IF (ALLOCATED(charI)) DEALLOCATE(charI)
                IF (ALLOCATED(charX)) DEALLOCATE(charX)
                IF (ALLOCATED(charV_prev)) DEALLOCATE(charV_prev)
                IF (ALLOCATED(charI_prev)) DEALLOCATE(charI_prev)
                IF (ALLOCATED(charX_prev)) DEALLOCATE(charX_prev)
             END IF
          END IF
       END IF
    END DO
    IF (ALLOCATED(pres)) DEALLOCATE(pres) 
    IF (ALLOCATED(pres_prev)) DEALLOCATE(pres_prev) 
    IF (ALLOCATED(du)) DEALLOCATE(du) 
    IF (ALLOCATED(du_prev)) DEALLOCATE(du_prev) 
    IF (ALLOCATED(d2u)) DEALLOCATE(d2u) 
  END SUBROUTINE user_evol_LODI_BC_Drhs 

  SUBROUTINE user_evol_LODI_BC_Drhs_diag (Drhs_diag_local, ulc_prev, myne_loc, mynloc, myjl, mymeth)
    !--Sets rhs for boundary conditions
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: myne_loc, mynloc, myjl, mymeth
    REAL (pr), DIMENSION (mynloc*myne_loc), INTENT (INOUT) :: Drhs_diag_local
    REAL (pr), DIMENSION (mynloc,myne_loc), INTENT (IN) :: ulc_prev

    INTEGER :: ie, i, ii, shift, jj
    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc

    REAL (pr), DIMENSION (:),     ALLOCATABLE :: tempdiag_loc
    REAL (pr), DIMENSION (:,:),   ALLOCATABLE :: charI, charX, charV_prev, charI_prev, charX_prev
    REAL (pr), DIMENSION (mynloc,dim) :: du_diag_local, d2u_diag_local
    REAL (pr), DIMENSION (mynloc) :: du_diag_PFB
    REAL (pr), DIMENSION (mynloc,Nspec+2):: pres_prev
    REAL (pr), DIMENSION (3,mynloc,dim) :: du, d2u
    REAL (pr) :: faceval
    INTEGER :: l, faceint

    IF (ALLOCATED(tempdiag_loc)) DEALLOCATE(tempdiag_loc)
    ALLOCATE(tempdiag_loc(myne_loc*mynloc))
    CALL internal_Drhs_diag(tempdiag_loc,ulc_prev,mymeth,1,0)
    i_p_face(0) = 1
    DO i=1,dim
       i_p_face(i) = i_p_face(i-1)*3
    END DO
    DO face_type = 0, 3**dim - 1
       face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
       IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
          CALL get_all_indices_by_face (face_type, myjl, nloc, iloc)
          IF(nloc > 0 ) THEN
             IF (ABS(face(1)) == 1) THEN
                DO jj=1,myne_loc
                   Drhs_diag_local((jj-1)*mynloc+iloc(1:nloc)) = tempdiag_loc((jj-1)*mynloc+iloc(1:nloc))  !from internal_Drhs_diag
                END DO
             END IF
          END IF
       END IF
    END DO
    IF (ALLOCATED(tempdiag_loc)) DEALLOCATE(tempdiag_loc) 

    CALL c_diff_diag(du_diag_local, d2u_diag_local, j_lev, mynloc, mymeth, mymeth, 10)

    d2u(1,:,1) = 1.0_pr
    IF (Nspec>1) d2u(1,:,1) = d2u(1,:,1)-SUM(ulc_prev(:,nspc(1):nspc(Nspecm)),DIM=2)/ulc_prev(:,nden) !Y0_Nspec
    d2u(2,:,1) = d2u(1,:,1)*cp_in(Nspec)  !cp0_Nspec
    d2u(2,:,2) = d2u(1,:,1)/MW_in(Nspec)  !R0_Nspec
    DO l=1,Nspecm
       d2u(2,:,1)  = d2u(2,:,1)  + ulc_prev(:,nspc(l))/ulc_prev(:,nden)*cp_in(l)  !cp0
       d2u(2,:,2)  = d2u(2,:,2)  + ulc_prev(:,nspc(l))/ulc_prev(:,nden)/MW_in(l)  !R0
    END DO
    d2u(1,:,1) = d2u(2,:,1)/(d2u(2,:,1)-d2u(2,:,2))  !gamma0=cp0/(cp0-R0)
    pres_prev(:,1) = (ulc_prev(:,neng)-0.5_pr*SUM(ulc_prev(:,nvel(1):nvel(dim))**2,DIM=2)/ulc_prev(:,nden))*(d2u(1,:,1)-1.0_pr)  !1: pressure
    d2u(1,:,2) = SQRT(d2u(1,:,1)*pres_prev(:,1)/ulc_prev(:,nden))  !c0=SQRT(gamma*P/rho)
    pres_prev(:,2) = ulc_prev(:,nvel(1))/ulc_prev(:,nden)  !u0
    pres_prev(:,3) = ulc_prev(:,nden)  !rho0

    IF ((methpd .ne. 0) .OR. splitFBpress) THEN 
       du(1,:,1) = pres_prev(:,1)
       DO i=1,Nspecm
          pres_prev(:,3+i) = ulc_prev(:,nspc(i))/ulc_prev(:,nden)
       END DO
       IF (Nspec>1) THEN
          CALL dpdx (du(1,:,1),ng,pres_prev(:,4:Nspec+2),2*methpd+mymeth,myjl,splitFBpress)
       ELSE
          CALL dpdx (du(1,:,1),ng,pres_prev(:,1),2*methpd+mymeth,myjl,.FALSE.)
       END IF
       CALL c_diff_fast(pres_prev(:,2:3), du(2:3,:,:), d2u(2:3,:,:), myjl, mynloc, mymeth, 10, 2, 1, 2)  
       IF (Nspec>1) THEN
          CALL dpdx_diag (du_diag_PFB(:),ng,pres_prev(:,4:Nspec+2),2*methpd+mymeth,myjl,splitFBpress)
       ELSE
          CALL dpdx_diag (du_diag_PFB(:),ng,pres_prev(:,1),2*methpd+mymeth,myjl,.FALSE.)
       END IF
    ELSE
       CALL c_diff_fast(pres_prev(:,1:3), du(1:3,:,:), d2u(1:3,:,:), myjl, mynloc, mymeth, 10, 3, 1, 3) 
       du_diag_PFB(:) = du_diag_local(:,1)
    END IF

    i_p_face(0) = 1
    DO i=1,dim
       i_p_face(i) = i_p_face(i-1)*3
    END DO
    DO face_type = 0, 3**dim - 1
       face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
       IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
          CALL get_all_indices_by_face (face_type, myjl, nloc, iloc)
          IF(nloc > 0 ) THEN
             IF (ABS(face(1)) == 1) THEN
                IF (ALLOCATED(charI)) DEALLOCATE(charI)
                IF (ALLOCATED(charX)) DEALLOCATE(charX)
                IF (ALLOCATED(charV_prev)) DEALLOCATE(charV_prev)
                IF (ALLOCATED(charI_prev)) DEALLOCATE(charI_prev)
                IF (ALLOCATED(charX_prev)) DEALLOCATE(charX_prev)
                ALLOCATE(charI(nloc,4),charX(nloc,3),charV_prev(nloc,6),charI_prev(nloc,3),charX_prev(nloc,1))

                faceval=REAL(face(1),pr)
                faceint=(3-face(1))/2
               
                IF (LODIset) THEN
                   charV_prev(:,1) = pres_prev(iloc(1:nloc),2) - d2u(1,iloc(1:nloc),2) !(u-c)
                   charV_prev(:,2) = pres_prev(iloc(1:nloc),2)                         ! u
                   charV_prev(:,3) = pres_prev(iloc(1:nloc),2) + d2u(1,iloc(1:nloc),2) !(u+c)
                   charV_prev(:,2-face(1)) = 0.0_pr
                   charV_prev(:,4:6) = 0.0_pr
                   charV_prev(:,5-face(1)) = 1.0_pr
                ELSE
                   charV_prev(:,1) = faceval*MAX(0.0_pr,faceval*(pres_prev(iloc(1:nloc),2) - d2u(1,iloc(1:nloc),2))) !(u-c)
                   charV_prev(:,2) = faceval*MAX(0.0_pr,faceval* pres_prev(iloc(1:nloc),2)                         ) ! u
                   charV_prev(:,3) = faceval*MAX(0.0_pr,faceval*(pres_prev(iloc(1:nloc),2) + d2u(1,iloc(1:nloc),2))) !(u+c)
                   charV_prev(:,4) = (1.0_pr-SIGN(1.0_pr,faceval*(pres_prev(iloc(1:nloc),2) - d2u(1,iloc(1:nloc),2))))/2.0_pr
                   charV_prev(:,5) = (1.0_pr-SIGN(1.0_pr,faceval* pres_prev(iloc(1:nloc),2)                         ))/2.0_pr 
                   charV_prev(:,6) = (1.0_pr-SIGN(1.0_pr,faceval*(pres_prev(iloc(1:nloc),2) + d2u(1,iloc(1:nloc),2))))/2.0_pr 
                END IF

                charI_prev(1:nloc,2) = charV_prev(1:nloc,2)*(d2u(1,iloc(1:nloc),2)**2*du(3,iloc(1:nloc),1) - du(1,iloc(1:nloc),1)) !u*(c^2*drho/dx - dp/dx)
                charI_prev(1:nloc,3) = charV_prev(1:nloc,3)*(du(1,iloc(1:nloc),1)+ulc_prev(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)*du(2,iloc(1:nloc),1))  !(u+c)*(dp/dx+rho*c*du/dx)
                charI_prev(1:nloc,1) = charV_prev(1:nloc,1)*(du(1,iloc(1:nloc),1)-ulc_prev(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)*du(2,iloc(1:nloc),1))  !(u-c)*(dp/dx-rho*c*du/dx)

                IF (pertchar) THEN   
                   charI_prev(1:nloc,3) = charI_prev(1:nloc,3) - charV_prev(1:nloc,3) * dP0(faceint)
                   charI_prev(1:nloc,1) = charI_prev(1:nloc,1) - charV_prev(1:nloc,1) * dP0(faceint)
                   charI_prev(1:nloc,2) = charI_prev(1:nloc,2) - charV_prev(1:nloc,2) * (d2u(1,iloc(1:nloc),2)**2*drho0(faceint) - dP0(faceint))
                END IF

                IF     (PLpinf==1) THEN
                   charI_prev(1:nloc,3) = charI_prev(1:nloc,3) + charV_prev(1:nloc,6)*pBcoef*(1.0_pr-maxMa**2)*c0(faceint)          *(pres_prev(iloc(1:nloc),1) - P0(faceint))
                   charI_prev(1:nloc,1) = charI_prev(1:nloc,1) + charV_prev(1:nloc,4)*pBcoef*(1.0_pr-maxMa**2)*c0(faceint)          *(pres_prev(iloc(1:nloc),1) - P0(faceint))
                ELSEIF (PLpinf==2) THEN
                   charI_prev(1:nloc,3) = charI_prev(1:nloc,3) + charV_prev(1:nloc,6)*pBcoef*(1.0_pr-maxMa**2)*d2u(1,iloc(1:nloc),2)*(pres_prev(iloc(1:nloc),1) - P0(faceint))
                   charI_prev(1:nloc,1) = charI_prev(1:nloc,1) + charV_prev(1:nloc,4)*pBcoef*(1.0_pr-maxMa**2)*d2u(1,iloc(1:nloc),2)*(pres_prev(iloc(1:nloc),1) - P0(faceint))
                ELSEIF (PLpinf==3) THEN
                   charI_prev(1:nloc,3) = charI_prev(1:nloc,3) + charV_prev(1:nloc,6)*pBcoef*(1.0_pr-maxMa**2)*c0(faceint) * &
                                                                                  (pres_prev(iloc(1:nloc),1)/ulc_prev(iloc(1:nloc),nden)/d2u(2,iloc(1:nloc),2) - Tconst)
                   charI_prev(1:nloc,1) = charI_prev(1:nloc,1) + charV_prev(1:nloc,4)*pBcoef*(1.0_pr-maxMa**2)*c0(faceint) * &
                                                                                  (pres_prev(iloc(1:nloc),1)/ulc_prev(iloc(1:nloc),nden)/d2u(2,iloc(1:nloc),2) - Tconst)
                ELSEIF (PLpinf==4) THEN
                   charI_prev(1:nloc,3) = charI_prev(1:nloc,3) + charV_prev(1:nloc,6)*pBcoef*(1.0_pr-maxMa**2)*d2u(1,iloc(1:nloc),2) * &
                                                                                  (pres_prev(iloc(1:nloc),1) - ulc_prev(iloc(1:nloc),nden)*d2u(2,iloc(1:nloc),2)*Tconst)
                   charI_prev(1:nloc,1) = charI_prev(1:nloc,1) + charV_prev(1:nloc,4)*pBcoef*(1.0_pr-maxMa**2)*d2u(1,iloc(1:nloc),2) * &
                                                                                  (pres_prev(iloc(1:nloc),1) - ulc_prev(iloc(1:nloc),nden)*d2u(2,iloc(1:nloc),2)*Tconst)
                ELSEIF (PLpinf==5) THEN
                   charI_prev(1:nloc,3) = charI_prev(1:nloc,3) - faceval*charV_prev(1:nloc,6)*pBcoef*(1.0_pr-maxMa**2)*gammBC(faceint)*Pint*pres_prev(iloc(1:nloc),2)
                   charI_prev(1:nloc,1) = charI_prev(1:nloc,1) - faceval*charV_prev(1:nloc,4)*pBcoef*(1.0_pr-maxMa**2)*gammBC(faceint)*Pint*pres_prev(iloc(1:nloc),2)
                ELSEIF (PLpinf==6) THEN
                   charI_prev(1:nloc,3) = charI_prev(1:nloc,3) - faceval*charV_prev(1:nloc,6)*pBcoef*(1.0_pr-maxMa**2)*ulc_prev(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)**2*pres_prev(iloc(1:nloc),2)
                   charI_prev(1:nloc,1) = charI_prev(1:nloc,1) - faceval*charV_prev(1:nloc,4)*pBcoef*(1.0_pr-maxMa**2)*ulc_prev(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)**2*pres_prev(iloc(1:nloc),2)
                END IF 
                
                charX_prev(1:nloc,1) = 0.5_pr/d2u(1,iloc(1:nloc),2)**2*(charI_prev(1:nloc,1)+charI_prev(1:nloc,3)+2.0_pr*charI_prev(1:nloc,2))

                IF (pertchar) THEN   
                   charX_prev(1:nloc,1) = charX_prev(1:nloc,1) + pres_prev(iloc(1:nloc),2)*drho0(faceint)
                END IF
                
                DO jj=1,myne_loc
                   IF (jj==nden) THEN
                      du (2,iloc(1:nloc),2) = -ulc_prev(iloc(1:nloc),nvel(1))/ulc_prev(iloc(1:nloc),nden)**2 !u_diag
                      d2u(3,iloc(1:nloc),1) = 0.0_pr  !gamma_diag
                      du (1,iloc(1:nloc),2) = 0.5_pr*SUM(ulc_prev(iloc(1:nloc),nvel(1):nvel(dim))**2,DIM=2)/ulc_prev(iloc(1:nloc),nden)**2*(d2u(1,iloc(1:nloc),1)-1.0_pr) !p_diag
                      d2u(3,iloc(1:nloc),2) = 0.5_pr/d2u(1,iloc(1:nloc),2)*d2u(1,iloc(1:nloc),1)/ulc_prev(iloc(1:nloc),nden) * &
                                                    (du(1,iloc(1:nloc),2)-pres_prev(iloc(1:nloc),1)/ulc_prev(iloc(1:nloc),nden) ) !c_diag
                      charI(1:nloc,3) = (1.0_pr-charV_prev(1:nloc,6))*(du(2,iloc(1:nloc),2)+d2u(3,iloc(1:nloc),2)) * &
                                                                      (du(1,iloc(1:nloc),1)+ulc_prev(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)*du(2,iloc(1:nloc),1)) + &
                                        charV_prev(1:nloc,3)*(du_diag_PFB(iloc(1:nloc))*du(1,iloc(1:nloc),2) + &
                                                              du_diag_local(iloc(1:nloc),1)*ulc_prev(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)*du(2,iloc(1:nloc),2) + &
                                                              du(2,iloc(1:nloc),1)*(ulc_prev(iloc(1:nloc),nden)*d2u(3,iloc(1:nloc),2) + d2u(1,iloc(1:nloc),2)))
                      charI(1:nloc,1) = (1.0_pr-charV_prev(1:nloc,4))*(du(2,iloc(1:nloc),2)-d2u(3,iloc(1:nloc),2)) * &
                                                                      (du(1,iloc(1:nloc),1)-ulc_prev(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)*du(2,iloc(1:nloc),1)) + &
                                        charV_prev(1:nloc,1)*(du_diag_PFB(iloc(1:nloc))*du(1,iloc(1:nloc),2) - & 
                                                              du_diag_local(iloc(1:nloc),1)*ulc_prev(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)*du(2,iloc(1:nloc),2) - &
                                                              du(2,iloc(1:nloc),1)*(ulc_prev(iloc(1:nloc),nden)*d2u(3,iloc(1:nloc),2) + d2u(1,iloc(1:nloc),2)))
                      charI(1:nloc,2) = (1.0_pr-charV_prev(1:nloc,5))*du(2,iloc(1:nloc),2)*(d2u(1,iloc(1:nloc),2)**2*du(3,iloc(1:nloc),1)-du(1,iloc(1:nloc),1)) + &
                                        charV_prev(1:nloc,2)*(du_diag_local(iloc(1:nloc),1)*d2u(1,iloc(1:nloc),2)**2-du_diag_PFB(iloc(1:nloc))*du(1,iloc(1:nloc),2) + &
                                                              2.0_pr*d2u(1,iloc(1:nloc),2)*d2u(3,iloc(1:nloc),2)*du(3,iloc(1:nloc),1) )
                      IF (pertchar) THEN   
                         charI(1:nloc,3) = charI(1:nloc,3) - (1.0_pr-charV_prev(1:nloc,6))*(du(2,iloc(1:nloc),2)+d2u(3,iloc(1:nloc),2)) * dP0(faceint)
                         charI(1:nloc,1) = charI(1:nloc,1) - (1.0_pr-charV_prev(1:nloc,4))*(du(2,iloc(1:nloc),2)-d2u(3,iloc(1:nloc),2)) * dP0(faceint)
                         charI(1:nloc,2) = charI(1:nloc,2) - (1.0_pr-charV_prev(1:nloc,5))*(du(2,iloc(1:nloc),2)) * (d2u(1,iloc(1:nloc),2)**2*drho0(faceint)-dP0(faceint)) - &
                                                             charV_prev(1:nloc,2)*2.0_pr*d2u(1,iloc(1:nloc),2)*d2u(3,iloc(1:nloc),2)*drho0(faceint)
                      END IF
                      IF     (PLpinf==1) THEN
                         charI(1:nloc,3) = charI(1:nloc,3) + charV_prev(1:nloc,6)*pBcoef*(1.0_pr-maxMa**2)*c0(faceint)*du(1,iloc(1:nloc),2)
                         charI(1:nloc,1) = charI(1:nloc,1) + charV_prev(1:nloc,4)*pBcoef*(1.0_pr-maxMa**2)*c0(faceint)*du(1,iloc(1:nloc),2)
                      ELSEIF (PLpinf==2) THEN
                         charI(1:nloc,3) = charI(1:nloc,3) + charV_prev(1:nloc,6)*pBcoef*(1.0_pr-maxMa**2) * &
                                                             (d2u(3,iloc(1:nloc),2)*(pres_prev(iloc(1:nloc),1)-P0(faceint)) + d2u(1,iloc(1:nloc),2)*du(1,iloc(1:nloc),2))
                         charI(1:nloc,1) = charI(1:nloc,1) + charV_prev(1:nloc,4)*pBcoef*(1.0_pr-maxMa**2) * &
                                                             (d2u(3,iloc(1:nloc),2)*(pres_prev(iloc(1:nloc),1)-P0(faceint)) + d2u(1,iloc(1:nloc),2)*du(1,iloc(1:nloc),2))
                      ELSEIF (PLpinf==3) THEN
                         charI(1:nloc,3) = charI(1:nloc,3) + charV_prev(1:nloc,6)*pBcoef*(1.0_pr-maxMa**2)*c0(faceint)/ulc_prev(iloc(1:nloc),nden)/d2u(2,iloc(1:nloc),2) * du(1,iloc(1:nloc),2)
                         charI(1:nloc,1) = charI(1:nloc,1) + charV_prev(1:nloc,4)*pBcoef*(1.0_pr-maxMa**2)*c0(faceint)/ulc_prev(iloc(1:nloc),nden)/d2u(2,iloc(1:nloc),2) * du(1,iloc(1:nloc),2)
                      ELSEIF (PLpinf==4) THEN
                         charI(1:nloc,3) = charI(1:nloc,3) + charV_prev(1:nloc,6)*pBcoef*(1.0_pr-maxMa**2)*(d2u(3,iloc(1:nloc),2) * &
                                                              (pres_prev(iloc(1:nloc),1) - ulc_prev(iloc(1:nloc),nden)*d2u(2,iloc(1:nloc),2)*Tconst) + d2u(1,iloc(1:nloc),2)*du(1,iloc(1:nloc),2) )
                         charI(1:nloc,1) = charI(1:nloc,1) + charV_prev(1:nloc,4)*pBcoef*(1.0_pr-maxMa**2)*(d2u(3,iloc(1:nloc),2) * &
                                                              (pres_prev(iloc(1:nloc),1) - ulc_prev(iloc(1:nloc),nden)*d2u(2,iloc(1:nloc),2)*Tconst) + d2u(1,iloc(1:nloc),2)*du(1,iloc(1:nloc),2) )
                      ELSEIF (PLpinf==5) THEN
                         charI(1:nloc,3) = charI(1:nloc,3) - faceval*charV_prev(1:nloc,6)*pBcoef*(1.0_pr-maxMa**2)*gammBC(faceint)*Pint*du(2,iloc(1:nloc),2)
                         charI(1:nloc,1) = charI(1:nloc,1) - faceval*charV_prev(1:nloc,4)*pBcoef*(1.0_pr-maxMa**2)*gammBC(faceint)*Pint*du(2,iloc(1:nloc),2)
                      ELSEIF (PLpinf==6) THEN
                         charI(1:nloc,3) = charI(1:nloc,3) - faceval*charV_prev(1:nloc,6)*pBcoef*(1.0_pr-maxMa**2)*d2u(1,iloc(1:nloc),2) * &
                                                         (d2u(1,iloc(1:nloc),2)*pres_prev(iloc(1:nloc),2) + &
                                                          2.0_pr*ulc_prev(iloc(1:nloc),nden)*d2u(3,iloc(1:nloc),2)*pres_prev(iloc(1:nloc),2) + &
                                                          ulc_prev(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)*du(2,iloc(1:nloc),2) )
                         charI(1:nloc,1) = charI(1:nloc,1) - faceval*charV_prev(1:nloc,4)*pBcoef*(1.0_pr-maxMa**2)*d2u(1,iloc(1:nloc),2) * &
                                                         (d2u(1,iloc(1:nloc),2)*pres_prev(iloc(1:nloc),2) + &
                                                          2.0_pr*ulc_prev(iloc(1:nloc),nden)*d2u(3,iloc(1:nloc),2)*pres_prev(iloc(1:nloc),2) + &
                                                          ulc_prev(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)*du(2,iloc(1:nloc),2) )
                      END IF
                      charX(1:nloc,1) = 0.5_pr/d2u(1,iloc(1:nloc),2)**2 * (charI(1:nloc,1)+charI(1:nloc,3)+2.0_pr*charI(1:nloc,2)) - &
                                               d2u(3,iloc(1:nloc),2)/d2u(1,iloc(1:nloc),2)**3 * (charI_prev(1:nloc,1)+charI_prev(1:nloc,3)+2.0_pr*charI_prev(1:nloc,2))

                      IF (pertchar) charX(1:nloc,1) = charX(1:nloc,1) + du(2,iloc(1:nloc),2)*drho0(faceint)   

                      Drhs_diag_local((jj-1)*mynloc+iloc(1:nloc)) = Drhs_diag_local((jj-1)*mynloc+iloc(1:nloc)) - charX(1:nloc,1)
                   ELSE IF (jj==nvel(1)) THEN
                      du (2,iloc(1:nloc),2) = 1.0_pr/ulc_prev(iloc(1:nloc),nden) !u_diag
                      d2u(3,iloc(1:nloc),1) = 0.0_pr  !gamma_diag
                      du (1,iloc(1:nloc),2) = -ulc_prev(iloc(1:nloc),nvel(1))/ulc_prev(iloc(1:nloc),nden)*(d2u(1,iloc(1:nloc),1)-1.0_pr) !p_diag
                      d2u(3,iloc(1:nloc),2) = 0.5_pr/d2u(1,iloc(1:nloc),2)*d2u(1,iloc(1:nloc),1)/ulc_prev(iloc(1:nloc),nden)*du(1,iloc(1:nloc),2) !c_diag
                      charI(1:nloc,3) = (1.0_pr-charV_prev(1:nloc,6))*(du(2,iloc(1:nloc),2)+d2u(3,iloc(1:nloc),2)) * &
                                                                      (du(1,iloc(1:nloc),1)+ulc_prev(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)*du(2,iloc(1:nloc),1)) + &
                                        charV_prev(1:nloc,3)*(du_diag_PFB(iloc(1:nloc))*du(1,iloc(1:nloc),2) + & 
                                                              du_diag_local(iloc(1:nloc),1)*ulc_prev(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)*du(2,iloc(1:nloc),2) + &
                                                              du(2,iloc(1:nloc),1)*ulc_prev(iloc(1:nloc),nden)*d2u(3,iloc(1:nloc),2))
                      charI(1:nloc,1) = (1.0_pr-charV_prev(1:nloc,4))*(du(2,iloc(1:nloc),2)-d2u(3,iloc(1:nloc),2)) * &
                                                                      (du(1,iloc(1:nloc),1)-ulc_prev(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)*du(2,iloc(1:nloc),1)) + &
                                        charV_prev(1:nloc,1)*(du_diag_PFB(iloc(1:nloc))*du(1,iloc(1:nloc),2) - & 
                                                              du_diag_local(iloc(1:nloc),1)*ulc_prev(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)*du(2,iloc(1:nloc),2) - &
                                                              du(2,iloc(1:nloc),1)*ulc_prev(iloc(1:nloc),nden)*d2u(3,iloc(1:nloc),2))
                      charI(1:nloc,2) = (1.0_pr-charV_prev(1:nloc,5))*du(2,iloc(1:nloc),2)*(d2u(1,iloc(1:nloc),2)**2*du(3,iloc(1:nloc),1)-du(1,iloc(1:nloc),1)) + &
                                        charV_prev(1:nloc,2)*(-du_diag_PFB(iloc(1:nloc))*du(1,iloc(1:nloc),2) + 2.0_pr*d2u(1,iloc(1:nloc),2)*d2u(3,iloc(1:nloc),2)*du(3,iloc(1:nloc),1))
                      IF (pertchar) THEN   
                         charI(1:nloc,3) = charI(1:nloc,3) - (1.0_pr-charV_prev(1:nloc,6))*(du(2,iloc(1:nloc),2)+d2u(3,iloc(1:nloc),2)) * dP0(faceint)
                         charI(1:nloc,1) = charI(1:nloc,1) - (1.0_pr-charV_prev(1:nloc,4))*(du(2,iloc(1:nloc),2)-d2u(3,iloc(1:nloc),2)) * dP0(faceint)
                         charI(1:nloc,2) = charI(1:nloc,2) - (1.0_pr-charV_prev(1:nloc,5))*(du(2,iloc(1:nloc),2)) * (d2u(1,iloc(1:nloc),2)**2*drho0(faceint)-dP0(faceint)) - &
                                                             charV_prev(1:nloc,2)*2.0_pr*d2u(1,iloc(1:nloc),2)*d2u(3,iloc(1:nloc),2)*drho0(faceint)
                      END IF
                      IF     (PLpinf==1) THEN
                         charI(1:nloc,3) = charI(1:nloc,3) + charV_prev(1:nloc,6)*pBcoef*(1.0_pr-maxMa**2)*c0(faceint)*du(1,iloc(1:nloc),2)
                         charI(1:nloc,1) = charI(1:nloc,1) + charV_prev(1:nloc,4)*pBcoef*(1.0_pr-maxMa**2)*c0(faceint)*du(1,iloc(1:nloc),2)
                      ELSEIF (PLpinf==2) THEN
                         charI(1:nloc,3) = charI(1:nloc,3) + charV_prev(1:nloc,6)*pBcoef*(1.0_pr-maxMa**2) * &
                                                             (d2u(3,iloc(1:nloc),2)*(pres_prev(iloc(1:nloc),1)-P0(faceint)) + d2u(1,iloc(1:nloc),2)*du(1,iloc(1:nloc),2))
                         charI(1:nloc,1) = charI(1:nloc,1) + charV_prev(1:nloc,4)*pBcoef*(1.0_pr-maxMa**2) * &
                                                             (d2u(3,iloc(1:nloc),2)*(pres_prev(iloc(1:nloc),1)-P0(faceint)) + d2u(1,iloc(1:nloc),2)*du(1,iloc(1:nloc),2))
                      ELSEIF (PLpinf==3) THEN
                         charI(1:nloc,3) = charI(1:nloc,3) + charV_prev(1:nloc,6)*pBcoef*(1.0_pr-maxMa**2)*c0(faceint)/ulc_prev(iloc(1:nloc),nden)/d2u(2,iloc(1:nloc),2) * du(1,iloc(1:nloc),2)
                         charI(1:nloc,1) = charI(1:nloc,1) + charV_prev(1:nloc,4)*pBcoef*(1.0_pr-maxMa**2)*c0(faceint)/ulc_prev(iloc(1:nloc),nden)/d2u(2,iloc(1:nloc),2) * du(1,iloc(1:nloc),2)
                      ELSEIF (PLpinf==4) THEN
                         charI(1:nloc,3) = charI(1:nloc,3) + charV_prev(1:nloc,6)*pBcoef*(1.0_pr-maxMa**2)*(d2u(3,iloc(1:nloc),2) * &
                                                              (pres_prev(iloc(1:nloc),1) - ulc_prev(iloc(1:nloc),nden)*d2u(2,iloc(1:nloc),2)*Tconst) + d2u(1,iloc(1:nloc),2)*du(1,iloc(1:nloc),2) )
                         charI(1:nloc,1) = charI(1:nloc,1) + charV_prev(1:nloc,4)*pBcoef*(1.0_pr-maxMa**2)*(d2u(3,iloc(1:nloc),2) * &
                                                              (pres_prev(iloc(1:nloc),1) - ulc_prev(iloc(1:nloc),nden)*d2u(2,iloc(1:nloc),2)*Tconst) + d2u(1,iloc(1:nloc),2)*du(1,iloc(1:nloc),2) )
                      ELSEIF (PLpinf==5) THEN
                         charI(1:nloc,3) = charI(1:nloc,3) - faceval*charV_prev(1:nloc,6)*pBcoef*(1.0_pr-maxMa**2)*gammBC(faceint)*Pint*du(2,iloc(1:nloc),2)
                         charI(1:nloc,1) = charI(1:nloc,1) - faceval*charV_prev(1:nloc,4)*pBcoef*(1.0_pr-maxMa**2)*gammBC(faceint)*Pint*du(2,iloc(1:nloc),2)
                      ELSEIF (PLpinf==6) THEN
                         charI(1:nloc,3) = charI(1:nloc,3) - faceval*charV_prev(1:nloc,6)*pBcoef*(1.0_pr-maxMa**2)*d2u(1,iloc(1:nloc),2) * &
                                                         (2.0_pr*ulc_prev(iloc(1:nloc),nden)*d2u(3,iloc(1:nloc),2)*pres_prev(iloc(1:nloc),2) + &
                                                          ulc_prev(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)*du(2,iloc(1:nloc),2) )
                         charI(1:nloc,1) = charI(1:nloc,1) - faceval*charV_prev(1:nloc,4)*pBcoef*(1.0_pr-maxMa**2)*d2u(1,iloc(1:nloc),2) * &
                                                         (2.0_pr*ulc_prev(iloc(1:nloc),nden)*d2u(3,iloc(1:nloc),2)*pres_prev(iloc(1:nloc),2) + &
                                                          ulc_prev(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)*du(2,iloc(1:nloc),2) )
                      END IF
                      charX(1:nloc,1) = 0.5_pr/d2u(1,iloc(1:nloc),2)**2 * (charI(1:nloc,1)+charI(1:nloc,3)+2.0_pr*charI(1:nloc,2)) - &
                                               d2u(3,iloc(1:nloc),2)/d2u(1,iloc(1:nloc),2)**3 * (charI_prev(1:nloc,1)+charI_prev(1:nloc,3)+2.0_pr*charI_prev(1:nloc,2))
                      charX(1:nloc,2) = 0.5_pr/ulc_prev(iloc(1:nloc),nden)/d2u(1,iloc(1:nloc),2) * (charI(1:nloc,3)-charI(1:nloc,1) - &
                                                     d2u(3,iloc(1:nloc),2)/d2u(1,iloc(1:nloc),2)*(charI_prev(1:nloc,3)-charI_prev(1:nloc,1)))

                      IF (pertchar) charX(1:nloc,1) = charX(1:nloc,1) + du(2,iloc(1:nloc),2)*drho0(faceint)   
                      
                      Drhs_diag_local((jj-1)*mynloc+iloc(1:nloc)) = Drhs_diag_local((jj-1)*mynloc+iloc(1:nloc)) - du(2,iloc(1:nloc),2)*charX_prev(1:nloc,1) - &
                                                     pres_prev(iloc(1:nloc),2)*charX(1:nloc,1) - ulc_prev(iloc(1:nloc),nden)*charX(1:nloc,2)
                   ELSE IF (jj>=nvel(2) .AND. jj<=nvel(dim)) THEN
                      du (2,iloc(1:nloc),2) = 0.0_pr !u_diag
                      d2u(3,iloc(1:nloc),1) = 0.0_pr  !gamma_diag
                      du (1,iloc(1:nloc),2) = -ulc_prev(iloc(1:nloc),jj)/ulc_prev(iloc(1:nloc),nden)*(d2u(1,iloc(1:nloc),1)-1.0_pr) !p_diag
                      d2u(3,iloc(1:nloc),2) = 0.5_pr/d2u(1,iloc(1:nloc),2)*d2u(1,iloc(1:nloc),1)/ulc_prev(iloc(1:nloc),nden)*du(1,iloc(1:nloc),2) !c_diag
                      charI(1:nloc,3) = (1.0_pr-charV_prev(1:nloc,6))*d2u(3,iloc(1:nloc),2) * (du(1,iloc(1:nloc),1)+ulc_prev(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)*du(2,iloc(1:nloc),1)) + &
                                        charV_prev(1:nloc,3)*(du_diag_PFB(iloc(1:nloc))*du(1,iloc(1:nloc),2) + du(2,iloc(1:nloc),1)*ulc_prev(iloc(1:nloc),nden)*d2u(3,iloc(1:nloc),2))
                      charI(1:nloc,1) =-(1.0_pr-charV_prev(1:nloc,4))*d2u(3,iloc(1:nloc),2) * (du(1,iloc(1:nloc),1)-ulc_prev(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)*du(2,iloc(1:nloc),1)) + &
                                        charV_prev(1:nloc,1)*(du_diag_PFB(iloc(1:nloc))*du(1,iloc(1:nloc),2) - du(2,iloc(1:nloc),1)*ulc_prev(iloc(1:nloc),nden)*d2u(3,iloc(1:nloc),2))
                      charI(1:nloc,2) = charV_prev(1:nloc,2)*(-du_diag_PFB(iloc(1:nloc))*du(1,iloc(1:nloc),2) + 2.0_pr*d2u(1,iloc(1:nloc),2)*d2u(3,iloc(1:nloc),2)*du(3,iloc(1:nloc),1))
                      charI(1:nloc,4) = charV_prev(1:nloc,2)*du_diag_local(iloc(1:nloc),1)/ulc_prev(iloc(1:nloc),nden)
                      IF (pertchar) THEN   
                         charI(1:nloc,3) = charI(1:nloc,3) - (1.0_pr-charV_prev(1:nloc,6))*d2u(3,iloc(1:nloc),2) * dP0(faceint)
                         charI(1:nloc,1) = charI(1:nloc,1) + (1.0_pr-charV_prev(1:nloc,4))*d2u(3,iloc(1:nloc),2) * dP0(faceint)
                         charI(1:nloc,2) = charI(1:nloc,2) - charV_prev(1:nloc,2)*2.0_pr*d2u(1,iloc(1:nloc),2)*d2u(3,iloc(1:nloc),2)*drho0(faceint)
                      END IF
                      IF     (PLpinf==1) THEN
                         charI(1:nloc,3) = charI(1:nloc,3) + charV_prev(1:nloc,6)*pBcoef*(1.0_pr-maxMa**2)*c0(faceint)*du(1,iloc(1:nloc),2)
                         charI(1:nloc,1) = charI(1:nloc,1) + charV_prev(1:nloc,4)*pBcoef*(1.0_pr-maxMa**2)*c0(faceint)*du(1,iloc(1:nloc),2)
                      ELSEIF (PLpinf==2) THEN
                         charI(1:nloc,3) = charI(1:nloc,3) + charV_prev(1:nloc,6)*pBcoef*(1.0_pr-maxMa**2) * &
                                                             (d2u(3,iloc(1:nloc),2)*(pres_prev(iloc(1:nloc),1)-P0(faceint)) + d2u(1,iloc(1:nloc),2)*du(1,iloc(1:nloc),2))
                         charI(1:nloc,1) = charI(1:nloc,1) + charV_prev(1:nloc,4)*pBcoef*(1.0_pr-maxMa**2) * &
                                                             (d2u(3,iloc(1:nloc),2)*(pres_prev(iloc(1:nloc),1)-P0(faceint)) + d2u(1,iloc(1:nloc),2)*du(1,iloc(1:nloc),2))
                      ELSEIF (PLpinf==3) THEN
                         charI(1:nloc,3) = charI(1:nloc,3) + charV_prev(1:nloc,6)*pBcoef*(1.0_pr-maxMa**2)*c0(faceint)/ulc_prev(iloc(1:nloc),nden)/d2u(2,iloc(1:nloc),2) * du(1,iloc(1:nloc),2)
                         charI(1:nloc,1) = charI(1:nloc,1) + charV_prev(1:nloc,4)*pBcoef*(1.0_pr-maxMa**2)*c0(faceint)/ulc_prev(iloc(1:nloc),nden)/d2u(2,iloc(1:nloc),2) * du(1,iloc(1:nloc),2)
                      ELSEIF (PLpinf==4) THEN
                         charI(1:nloc,3) = charI(1:nloc,3) + charV_prev(1:nloc,6)*pBcoef*(1.0_pr-maxMa**2)*(d2u(3,iloc(1:nloc),2) * &
                                                              (pres_prev(iloc(1:nloc),1) - ulc_prev(iloc(1:nloc),nden)*d2u(2,iloc(1:nloc),2)*Tconst) + d2u(1,iloc(1:nloc),2)*du(1,iloc(1:nloc),2) )
                         charI(1:nloc,1) = charI(1:nloc,1) + charV_prev(1:nloc,4)*pBcoef*(1.0_pr-maxMa**2)*(d2u(3,iloc(1:nloc),2) * &
                                                              (pres_prev(iloc(1:nloc),1) - ulc_prev(iloc(1:nloc),nden)*d2u(2,iloc(1:nloc),2)*Tconst) + d2u(1,iloc(1:nloc),2)*du(1,iloc(1:nloc),2) )
                      ELSEIF (PLpinf==6) THEN
                         charI(1:nloc,3) = charI(1:nloc,3) - faceval*charV_prev(1:nloc,6)*pBcoef*(1.0_pr-maxMa**2)*d2u(1,iloc(1:nloc),2) * &
                                                         2.0_pr*ulc_prev(iloc(1:nloc),nden)*d2u(3,iloc(1:nloc),2)*pres_prev(iloc(1:nloc),2)
                         charI(1:nloc,1) = charI(1:nloc,1) - faceval*charV_prev(1:nloc,4)*pBcoef*(1.0_pr-maxMa**2)*d2u(1,iloc(1:nloc),2) * &
                                                         2.0_pr*ulc_prev(iloc(1:nloc),nden)*d2u(3,iloc(1:nloc),2)*pres_prev(iloc(1:nloc),2)
                      END IF
                      charX(1:nloc,1) = 0.5_pr/d2u(1,iloc(1:nloc),2)**2 * (charI(1:nloc,1)+charI(1:nloc,3)+2.0_pr*charI(1:nloc,2)) - &
                                               d2u(3,iloc(1:nloc),2)/d2u(1,iloc(1:nloc),2)**3 * (charI_prev(1:nloc,1)+charI_prev(1:nloc,3)+2.0_pr*charI_prev(1:nloc,2))
                      charX(1:nloc,3) = charI(1:nloc,4)
                      Drhs_diag_local((jj-1)*mynloc+iloc(1:nloc)) = Drhs_diag_local((jj-1)*mynloc+iloc(1:nloc)) - (charX_prev(1:nloc,1)+ulc_prev(iloc(1:nloc),jj)*charX(1:nloc,1)) / &
                                                     ulc_prev(iloc(1:nloc),nden) - ulc_prev(iloc(1:nloc),nden)*charX(1:nloc,3)
                   ELSE IF (jj==neng) THEN
                      du (2,iloc(1:nloc),2) = 0.0_pr !u_diag
                      d2u(3,iloc(1:nloc),1) = 0.0_pr  !gamma_diag
                      du (1,iloc(1:nloc),2) = d2u(1,iloc(1:nloc),1)-1.0_pr !p_diag
                      d2u(3,iloc(1:nloc),2) = 0.5_pr/d2u(1,iloc(1:nloc),2)*d2u(1,iloc(1:nloc),1)/ulc_prev(iloc(1:nloc),nden)*du(1,iloc(1:nloc),2) !c_diag
                      charI(1:nloc,3) = (1.0_pr-charV_prev(1:nloc,6))*d2u(3,iloc(1:nloc),2) * (du(1,iloc(1:nloc),1)+ulc_prev(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)*du(2,iloc(1:nloc),1)) + &
                                        charV_prev(1:nloc,3)*(du_diag_PFB(iloc(1:nloc))*du(1,iloc(1:nloc),2) + du(2,iloc(1:nloc),1)*ulc_prev(iloc(1:nloc),nden)*d2u(3,iloc(1:nloc),2))
                      charI(1:nloc,1) =-(1.0_pr-charV_prev(1:nloc,4))*d2u(3,iloc(1:nloc),2) * (du(1,iloc(1:nloc),1)-ulc_prev(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)*du(2,iloc(1:nloc),1)) + &
                                        charV_prev(1:nloc,1)*(du_diag_PFB(iloc(1:nloc))*du(1,iloc(1:nloc),2) - du(2,iloc(1:nloc),1)*ulc_prev(iloc(1:nloc),nden)*d2u(3,iloc(1:nloc),2))
                      charI(1:nloc,2) = charV_prev(1:nloc,2)*(-du_diag_PFB(iloc(1:nloc))*du(1,iloc(1:nloc),2) + 2.0_pr*d2u(1,iloc(1:nloc),2)*d2u(3,iloc(1:nloc),2)*du(3,iloc(1:nloc),1))
                      IF (pertchar) THEN   
                         charI(1:nloc,3) = charI(1:nloc,3) - (1.0_pr-charV_prev(1:nloc,6))*d2u(3,iloc(1:nloc),2) * dP0(faceint)
                         charI(1:nloc,1) = charI(1:nloc,1) + (1.0_pr-charV_prev(1:nloc,4))*d2u(3,iloc(1:nloc),2) * dP0(faceint)
                         charI(1:nloc,2) = charI(1:nloc,2) - charV_prev(1:nloc,2)*2.0_pr*d2u(1,iloc(1:nloc),2)*d2u(3,iloc(1:nloc),2)*drho0(faceint)
                      END IF
                      IF     (PLpinf==1) THEN
                         charI(1:nloc,3) = charI(1:nloc,3) + charV_prev(1:nloc,6)*pBcoef*(1.0_pr-maxMa**2)*c0(faceint)*du(1,iloc(1:nloc),2)
                         charI(1:nloc,1) = charI(1:nloc,1) + charV_prev(1:nloc,4)*pBcoef*(1.0_pr-maxMa**2)*c0(faceint)*du(1,iloc(1:nloc),2)
                      ELSEIF (PLpinf==2) THEN
                         charI(1:nloc,3) = charI(1:nloc,3) + charV_prev(1:nloc,6)*pBcoef*(1.0_pr-maxMa**2) * &
                                                             (d2u(3,iloc(1:nloc),2)*(pres_prev(iloc(1:nloc),1)-P0(faceint)) + d2u(1,iloc(1:nloc),2)*du(1,iloc(1:nloc),2))
                         charI(1:nloc,1) = charI(1:nloc,1) + charV_prev(1:nloc,4)*pBcoef*(1.0_pr-maxMa**2) * &
                                                             (d2u(3,iloc(1:nloc),2)*(pres_prev(iloc(1:nloc),1)-P0(faceint)) + d2u(1,iloc(1:nloc),2)*du(1,iloc(1:nloc),2))
                      ELSEIF (PLpinf==3) THEN
                         charI(1:nloc,3) = charI(1:nloc,3) + charV_prev(1:nloc,6)*pBcoef*(1.0_pr-maxMa**2)*c0(faceint)/ulc_prev(iloc(1:nloc),nden)/d2u(2,iloc(1:nloc),2) * du(1,iloc(1:nloc),2)
                         charI(1:nloc,1) = charI(1:nloc,1) + charV_prev(1:nloc,4)*pBcoef*(1.0_pr-maxMa**2)*c0(faceint)/ulc_prev(iloc(1:nloc),nden)/d2u(2,iloc(1:nloc),2) * du(1,iloc(1:nloc),2)
                      ELSEIF (PLpinf==4) THEN
                         charI(1:nloc,3) = charI(1:nloc,3) + charV_prev(1:nloc,6)*pBcoef*(1.0_pr-maxMa**2)*(d2u(3,iloc(1:nloc),2) * &
                                                              (pres_prev(iloc(1:nloc),1) - ulc_prev(iloc(1:nloc),nden)*d2u(2,iloc(1:nloc),2)*Tconst) + d2u(1,iloc(1:nloc),2)*du(1,iloc(1:nloc),2) )
                         charI(1:nloc,1) = charI(1:nloc,1) + charV_prev(1:nloc,4)*pBcoef*(1.0_pr-maxMa**2)*(d2u(3,iloc(1:nloc),2) * &
                                                              (pres_prev(iloc(1:nloc),1) - ulc_prev(iloc(1:nloc),nden)*d2u(2,iloc(1:nloc),2)*Tconst) + d2u(1,iloc(1:nloc),2)*du(1,iloc(1:nloc),2) )
                      ELSEIF (PLpinf==6) THEN
                         charI(1:nloc,3) = charI(1:nloc,3) - faceval*charV_prev(1:nloc,6)*pBcoef*(1.0_pr-maxMa**2)*d2u(1,iloc(1:nloc),2) * &
                                                         2.0_pr*ulc_prev(iloc(1:nloc),nden)*d2u(3,iloc(1:nloc),2)*pres_prev(iloc(1:nloc),2)
                         charI(1:nloc,1) = charI(1:nloc,1) - faceval*charV_prev(1:nloc,4)*pBcoef*(1.0_pr-maxMa**2)*d2u(1,iloc(1:nloc),2) * &
                                                         2.0_pr*ulc_prev(iloc(1:nloc),nden)*d2u(3,iloc(1:nloc),2)*pres_prev(iloc(1:nloc),2)
                      END IF
                      charX(1:nloc,1) = 0.5_pr/d2u(1,iloc(1:nloc),2)**2 * (charI(1:nloc,1)+charI(1:nloc,3)+2.0_pr*charI(1:nloc,2)) - &
                                               d2u(3,iloc(1:nloc),2)/d2u(1,iloc(1:nloc),2)**3 * (charI_prev(1:nloc,1)+charI_prev(1:nloc,3)+2.0_pr*charI_prev(1:nloc,2))
                      charX(1:nloc,2) = 0.5_pr/ulc_prev(iloc(1:nloc),nden)/d2u(1,iloc(1:nloc),2) * (charI(1:nloc,3)-charI(1:nloc,1) - &
                                                     d2u(3,iloc(1:nloc),2)/d2u(1,iloc(1:nloc),2)*(charI_prev(1:nloc,3)-charI_prev(1:nloc,1)))
                      charX(1:nloc,3) = 0.5_pr*(charI(1:nloc,1)+charI(1:nloc,3))
                      Drhs_diag_local((jj-1)*mynloc+iloc(1:nloc)) = Drhs_diag_local((jj-1)*mynloc+iloc(1:nloc)) - 0.5_pr*SUM(ulc_prev(iloc(1:nloc),nvel(1):nvel(dim))**2,DIM=2) / &
                                        ulc_prev(iloc(1:nloc),nden)**2*charX(1:nloc,1) - ulc_prev(iloc(1:nloc),nvel(1))*charX(1:nloc,2) - &
                                        1.0_pr/(d2u(1,iloc(1:nloc),1)-1.0_pr)*charX(1:nloc,3)
                   ELSE IF (jj>=nspc_lil .AND. jj<=nspc_big) THEN
                      du (2,iloc(1:nloc),2) = 0.0_pr !u_diag
                      d2u(3,iloc(1:nloc),1) = (d2u(2,iloc(1:nloc),1)/MW_in(jj-nspc(1)+1) - d2u(2,iloc(1:nloc),2)*cp_in(jj-nspc(1)+1)) / &
                                                    ulc_prev(iloc(1:nloc),nden)/(d2u(2,iloc(1:nloc),1)-d2u(2,iloc(1:nloc),2))**2  !gamma_diag
                      du (1,iloc(1:nloc),2) = (ulc_prev(iloc(1:nloc),neng) - 0.5_pr*SUM(ulc_prev(iloc(1:nloc),nvel(1):nvel(dim))**2,DIM=2) / &
                                                    ulc_prev(iloc(1:nloc),nden))*d2u(3,iloc(1:nloc),1) !p_diag
                      d2u(3,iloc(1:nloc),2) = 0.5_pr/d2u(1,iloc(1:nloc),2)/ulc_prev(iloc(1:nloc),nden) * &
                                                    (d2u(3,iloc(1:nloc),1)*pres_prev(iloc(1:nloc),1)+d2u(1,iloc(1:nloc),1)*du(1,iloc(1:nloc),2)) !c_diag
          
                      charI(1:nloc,3) = (1.0_pr-charV_prev(1:nloc,6))*d2u(3,iloc(1:nloc),2) * (du(1,iloc(1:nloc),1)+ulc_prev(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)*du(2,iloc(1:nloc),1)) + &
                                        charV_prev(1:nloc,3)*(du_diag_PFB(iloc(1:nloc))*du(1,iloc(1:nloc),2) + du(2,iloc(1:nloc),1)*ulc_prev(iloc(1:nloc),nden)*d2u(3,iloc(1:nloc),2))
                      charI(1:nloc,1) =-(1.0_pr-charV_prev(1:nloc,4))*d2u(3,iloc(1:nloc),2) * (du(1,iloc(1:nloc),1)-ulc_prev(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)*du(2,iloc(1:nloc),1)) + &
                                        charV_prev(1:nloc,1)*(du_diag_PFB(iloc(1:nloc))*du(1,iloc(1:nloc),2) - du(2,iloc(1:nloc),1)*ulc_prev(iloc(1:nloc),nden)*d2u(3,iloc(1:nloc),2))
                      charI(1:nloc,2) = charV_prev(1:nloc,2)*(-du_diag_PFB(iloc(1:nloc))*du(1,iloc(1:nloc),2) + 2.0_pr*d2u(1,iloc(1:nloc),2)*d2u(3,iloc(1:nloc),2)*du(3,iloc(1:nloc),1))
                      charI(1:nloc,4) = charV_prev(1:nloc,2)*du_diag_local(iloc(1:nloc),1)/ulc_prev(iloc(1:nloc),nden)

                      IF (pertchar) THEN   
                         charI(1:nloc,3) = charI(1:nloc,3) - (1.0_pr-charV_prev(1:nloc,6))*d2u(3,iloc(1:nloc),2) * dP0(faceint)
                         charI(1:nloc,1) = charI(1:nloc,1) + (1.0_pr-charV_prev(1:nloc,4))*d2u(3,iloc(1:nloc),2) * dP0(faceint)
                         charI(1:nloc,2) = charI(1:nloc,2) - charV_prev(1:nloc,2)*2.0_pr*d2u(1,iloc(1:nloc),2)*d2u(3,iloc(1:nloc),2)*drho0(faceint)
                      END IF
                      IF     (PLpinf==1) THEN
                         charI(1:nloc,3) = charI(1:nloc,3) + charV_prev(1:nloc,6)*pBcoef*(1.0_pr-maxMa**2)*c0(faceint)*du(1,iloc(1:nloc),2)
                         charI(1:nloc,1) = charI(1:nloc,1) + charV_prev(1:nloc,4)*pBcoef*(1.0_pr-maxMa**2)*c0(faceint)*du(1,iloc(1:nloc),2)
                      ELSEIF (PLpinf==2) THEN
                         charI(1:nloc,3) = charI(1:nloc,3) + charV_prev(1:nloc,6)*pBcoef*(1.0_pr-maxMa**2) * &
                                                             (d2u(3,iloc(1:nloc),2)*(pres_prev(iloc(1:nloc),1)-P0(faceint)) + d2u(1,iloc(1:nloc),2)*du(1,iloc(1:nloc),2))
                         charI(1:nloc,1) = charI(1:nloc,1) + charV_prev(1:nloc,4)*pBcoef*(1.0_pr-maxMa**2) * &
                                                             (d2u(3,iloc(1:nloc),2)*(pres_prev(iloc(1:nloc),1)-P0(faceint)) + d2u(1,iloc(1:nloc),2)*du(1,iloc(1:nloc),2))
                      ELSEIF (PLpinf==3) THEN
                         charI(1:nloc,3) = charI(1:nloc,3) + charV_prev(1:nloc,6)*pBcoef*(1.0_pr-maxMa**2)*c0(faceint)/ulc_prev(iloc(1:nloc),nden)/d2u(2,iloc(1:nloc),2) * &
                                                         (du(1,iloc(1:nloc),2) - pres_prev(iloc(1:nloc),1)/MW_in(faceint)/ulc_prev(iloc(1:nloc),nden)/d2u(2,iloc(1:nloc),2) ) 
                         charI(1:nloc,1) = charI(1:nloc,1) + charV_prev(1:nloc,4)*pBcoef*(1.0_pr-maxMa**2)*c0(faceint)/ulc_prev(iloc(1:nloc),nden)/d2u(2,iloc(1:nloc),2) * &
                                                         (du(1,iloc(1:nloc),2) - pres_prev(iloc(1:nloc),1)/MW_in(faceint)/ulc_prev(iloc(1:nloc),nden)/d2u(2,iloc(1:nloc),2) ) 
                      ELSEIF (PLpinf==4) THEN
                         charI(1:nloc,3) = charI(1:nloc,3) + charV_prev(1:nloc,6)*pBcoef*(1.0_pr-maxMa**2)*(d2u(3,iloc(1:nloc),2) * &
                                                              (pres_prev(iloc(1:nloc),1) - ulc_prev(iloc(1:nloc),nden)*d2u(2,iloc(1:nloc),2)*Tconst) + &
                                                                                     d2u(1,iloc(1:nloc),2)*(du(1,iloc(1:nloc),2) - Tconst/MW_in(faceint)) )
                         charI(1:nloc,1) = charI(1:nloc,1) + charV_prev(1:nloc,4)*pBcoef*(1.0_pr-maxMa**2)*(d2u(3,iloc(1:nloc),2) * &
                                                              (pres_prev(iloc(1:nloc),1) - ulc_prev(iloc(1:nloc),nden)*d2u(2,iloc(1:nloc),2)*Tconst) + &
                                                                                     d2u(1,iloc(1:nloc),2)*(du(1,iloc(1:nloc),2) - Tconst/MW_in(faceint)) )
                      ELSEIF (PLpinf==6) THEN
                         charI(1:nloc,3) = charI(1:nloc,3) - faceval*charV_prev(1:nloc,6)*pBcoef*(1.0_pr-maxMa**2)*d2u(1,iloc(1:nloc),2) * &
                                                         2.0_pr*ulc_prev(iloc(1:nloc),nden)*d2u(3,iloc(1:nloc),2)*pres_prev(iloc(1:nloc),2)
                         charI(1:nloc,1) = charI(1:nloc,1) - faceval*charV_prev(1:nloc,4)*pBcoef*(1.0_pr-maxMa**2)*d2u(1,iloc(1:nloc),2) * &
                                                         2.0_pr*ulc_prev(iloc(1:nloc),nden)*d2u(3,iloc(1:nloc),2)*pres_prev(iloc(1:nloc),2)
                      END IF
                      charX(1:nloc,1) = 0.5_pr/d2u(1,iloc(1:nloc),2)**2 * (charI(1:nloc,1)+charI(1:nloc,3)+2.0_pr*charI(1:nloc,2)) - &
                                               d2u(3,iloc(1:nloc),2)/d2u(1,iloc(1:nloc),2)**3 * (charI_prev(1:nloc,1)+charI_prev(1:nloc,3)+2.0_pr*charI_prev(1:nloc,2))
                      charX(1:nloc,3) = charI(1:nloc,4)
                      Drhs_diag_local((jj-1)*mynloc+iloc(1:nloc)) = Drhs_diag_local((jj-1)*mynloc+iloc(1:nloc)) - (charX_prev(1:nloc,1)+ulc_prev(iloc(1:nloc),jj)*charX(1:nloc,1)) / &
                                                     ulc_prev(iloc(1:nloc),nden) - ulc_prev(iloc(1:nloc),nden)*charX(1:nloc,3)
                   END IF 
                END DO
                IF (ALLOCATED(charI)) DEALLOCATE(charI)
                IF (ALLOCATED(charX)) DEALLOCATE(charX)
                IF (ALLOCATED(charV_prev)) DEALLOCATE(charV_prev)
                IF (ALLOCATED(charI_prev)) DEALLOCATE(charI_prev)
                IF (ALLOCATED(charX_prev)) DEALLOCATE(charX_prev)
             END IF
          END IF
       END IF
    END DO
  END SUBROUTINE user_evol_LODI_BC_Drhs_diag 

  SUBROUTINE user_evol_LODIaddon_BC_rhs (myrhs_loc, ulc, myne_loc, mynloc, myjl, mymeth)
    !--Sets rhs for boundary conditions
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: myne_loc, mynloc, myjl, mymeth
    REAL (pr), DIMENSION (mynloc*myne_loc), INTENT (INOUT) :: myrhs_loc
    REAL (pr), DIMENSION (mynloc,myne_loc), INTENT (IN) :: ulc

    INTEGER :: ie, i, ii, shift, jj
    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc

    REAL (pr), DIMENSION (:),     ALLOCATABLE :: temprhs_loc
    REAL (pr), DIMENSION (:,:),   ALLOCATABLE :: pres
    REAL (pr), DIMENSION (:,:,:), ALLOCATABLE :: du, d2u
    REAL (pr), DIMENSION (:,:),   ALLOCATABLE :: charI,charX
    REAL (pr) :: cp, R, curY, faceval
    INTEGER :: l, nstp, faceint

    IF (ALLOCATED(temprhs_loc)) DEALLOCATE(temprhs_loc)
    ALLOCATE(temprhs_loc(myne_loc*mynloc))
    CALL internal_rhs(temprhs_loc,ulc,0,1)
    i_p_face(0) = 1
    DO i=1,dim
       i_p_face(i) = i_p_face(i-1)*3
    END DO
    DO face_type = 0, 3**dim - 1
       face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
       IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
          CALL get_all_indices_by_face (face_type, myjl, nloc, iloc)
          IF(nloc > 0 ) THEN
             IF (ABS(face(1)) == 1) THEN
                DO jj=1,myne_loc
                   myrhs_loc((jj-1)*mynloc+iloc(1:nloc)) = temprhs_loc((jj-1)*mynloc+iloc(1:nloc))  !from internal_rhs
                END DO
             END IF
          END IF
       END IF
    END DO
    IF (ALLOCATED(temprhs_loc)) DEALLOCATE(temprhs_loc) 

    IF (ALLOCATED(pres)) DEALLOCATE(pres) 
    IF (ALLOCATED(du)) DEALLOCATE(du) 
    IF (ALLOCATED(d2u)) DEALLOCATE(d2u) 
    ALLOCATE(pres(mynloc,locnvar),du(locnvar,mynloc,dim),d2u(locnvar,mynloc,dim))

    DO nstp=1,mynloc
       curY=1.0_pr
       IF (Nspec>1) curY=curY-SUM(ulc(nstp,nspc(1):nspc(Nspecm)))/ulc(nstp,nden) !Y_Nspec
       cp = curY*cp_in(Nspec)
       R = curY/MW_in(Nspec)
       DO l=1,Nspecm
          cp = cp + ulc(nstp,nspc(l))/ulc(nstp,nden)*cp_in(l)  !cp
          R  = R  + ulc(nstp,nspc(l))/ulc(nstp,nden)/MW_in(l)  !R
       END DO
       pres(nstp,1) = (ulc(nstp,neng)-0.5_pr*SUM(ulc(nstp,nvel(1):nvel(dim))**2)/ulc(nstp,nden))/(cp/R-1.0_pr)  !1: pressure
    END DO
    DO i=1,dim
       pres(:,1+i) = ulc(:,nvel(i))/ulc(:,nden)  !2--dim+1: u,v(,w)
    END DO
    DO i=1,Nspecm
       pres(:,dim+1+i) = ulc(:,nspc(i))/ulc(:,nden)  !dim+2--dim+Nspec: Y_l
    END DO 
    pres(:,locnvar) = ulc(:,nden) !dim+Nspec+1: rho

    IF ((methpd .ne. 0) .OR. splitFBpress) THEN 
       du(1,:,1) = pres(:,1)
       IF (Nspec>1) THEN
          CALL dpdx (du(1,:,1),ng,pres(:,dim+2:dim+Nspec),2*methpd+mymeth,myjl,splitFBpress)
       ELSE
          CALL dpdx (du(1,:,1),ng,pres(:,1),2*methpd+mymeth,myjl,.FALSE.)
       END IF
       CALL c_diff_fast(pres(:,2:locnvar), du(2:locnvar,:,:), d2u(2:locnvar,:,:), myjl, mynloc, mymeth, 10, locnvar-1, 1, locnvar-1)  
    ELSE
       CALL c_diff_fast(pres(:,1:locnvar), du(1:locnvar,:,:), d2u(1:locnvar,:,:), myjl, mynloc, mymeth, 10, locnvar, 1, locnvar) 
    END IF

    i_p_face(0) = 1
    DO i=1,dim
       i_p_face(i) = i_p_face(i-1)*3
    END DO
    DO face_type = 0, 3**dim - 1
       face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
       IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
          CALL get_all_indices_by_face (face_type, myjl, nloc, iloc)
          IF(nloc > 0 ) THEN
             IF (ABS(face(1)) == 1) THEN
                IF (ALLOCATED(charI)) DEALLOCATE(charI)
                IF (ALLOCATED(charX)) DEALLOCATE(charX)
                ALLOCATE(charI(nloc,locnvar),charX(nloc,locnvar))

                faceval=REAL(face(1),pr)
                faceint=(3-face(1))/2

                d2u(1,iloc(1:nloc),1) = 1.0_pr
                IF (Nspec>1) d2u(1,iloc(1:nloc),1) = 1.0_pr-SUM(ulc(iloc(1:nloc),nspc(1):nspc(Nspecm)),DIM=2)/ulc(iloc(1:nloc),nden) !Y_Nspec
                d2u(2,iloc(1:nloc),1) = d2u(1,iloc(1:nloc),1)*cp_in(Nspec)  !cp_Nspec
                d2u(2,iloc(1:nloc),2) = d2u(1,iloc(1:nloc),1)/MW_in(Nspec)  !R_Nspec
                DO l=1,Nspecm
                   d2u(2,iloc(1:nloc),1)  = d2u(2,iloc(1:nloc),1)  + ulc(iloc(1:nloc),nspc(l))/ulc(iloc(1:nloc),nden)*cp_in(l)  !cp
                   d2u(2,iloc(1:nloc),2)  = d2u(2,iloc(1:nloc),2)  + ulc(iloc(1:nloc),nspc(l))/ulc(iloc(1:nloc),nden)/MW_in(l)  !R
                END DO
                d2u(1,iloc(1:nloc),1) = d2u(2,iloc(1:nloc),1)/(d2u(2,iloc(1:nloc),1)-d2u(2,iloc(1:nloc),2))  !gamma=cp/(cp-R)
                d2u(1,iloc(1:nloc),2) = DSQRT(d2u(1,iloc(1:nloc),1)*pres(iloc(1:nloc),1)/ulc(iloc(1:nloc),nden))  !c=SQRT(gamma*P/rho)

                IF (pertchar) THEN
                   du(1,iloc(1:nloc),1) = du(1,iloc(1:nloc),1) - dP0(faceint)
                   du(locnvar,iloc(1:nloc),1) = du(locnvar,iloc(1:nloc),1) - drho0(faceint)
                END IF

                charI(1:nloc,2) = faceval*MAX(0.0_pr,faceval*pres(iloc(1:nloc),2))*(d2u(1,iloc(1:nloc),2)**2*du(locnvar,iloc(1:nloc),1) - du(1,iloc(1:nloc),1)) 
                       !u*(c^2*drho/dx - dp/dx)
                DO ii=3,dim+Nspec
                   charI(1:nloc,ii) = faceval*MAX(0.0_pr,faceval*pres(iloc(1:nloc),2))*du(ii,iloc(1:nloc),1)
                END DO
                charI(1:nloc,locnvar) = faceval*MAX(0.0_pr,faceval*(pres(iloc(1:nloc),2) + d2u(1,iloc(1:nloc),2))) * &
                              (du(1,iloc(1:nloc),1) + ulc(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)*du(2,iloc(1:nloc),1))  !(u+c)*(dp/dx+rho*c*du/dx)
                charI(1:nloc,1)       = faceval*MAX(0.0_pr,faceval*(pres(iloc(1:nloc),2) - d2u(1,iloc(1:nloc),2))) * &
                              (du(1,iloc(1:nloc),1) - ulc(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)*du(2,iloc(1:nloc),1))  !(u-c)*(dp/dx-rho*c*du/dx)

                IF (PLpinf) THEN
                   charI(1:nloc,locnvar) = charI(1:nloc,locnvar) + (1.0_pr-SIGN(1.0_pr,faceval* (pres(iloc(1:nloc),2) + d2u(1,iloc(1:nloc),2)) ))/2.0_pr * pBcoef * &
                                                                   (1.0_pr-maxMa**2) * &
                                                                   d2u(1,iloc(1:nloc),2) * (pres(iloc(1:nloc),1) - P0(faceint) )
                   charI(1:nloc,1)       = charI(1:nloc,1)       + (1.0_pr-SIGN(1.0_pr,faceval* (pres(iloc(1:nloc),2) - d2u(1,iloc(1:nloc),2)) ))/2.0_pr * pBcoef * &
                                                                   (1.0_pr-maxMa**2) * &
                                                                   d2u(1,iloc(1:nloc),2) * (pres(iloc(1:nloc),1) - P0(faceint) )
                END IF

                charX(1:nloc,1) = -0.5_pr/d2u(1,iloc(1:nloc),2)**2*(charI(1:nloc,1)+charI(1:nloc,locnvar)+2.0_pr*charI(1:nloc,2))
                charX(1:nloc,2) = -0.5_pr/d2u(1,iloc(1:nloc),2)/ulc(iloc(1:nloc),nden)*(charI(1:nloc,locnvar)-charI(1:nloc,1))
                DO ii=3,dim+Nspec
                   charX(1:nloc,ii) = -charI(1:nloc,ii)
                END DO
                charX(1:nloc,locnvar) = -0.5_pr*(charI(1:nloc,locnvar)+charI(1:nloc,1))

                myrhs_loc((nden-1)*mynloc+iloc(1:nloc)) = myrhs_loc((nden-1)*mynloc+iloc(1:nloc)) - charX(1:nloc,1)
                DO ii=nvel(1),nvel(dim)
                   myrhs_loc((ii-1)*mynloc+iloc(1:nloc)) = myrhs_loc((ii-1)*mynloc+iloc(1:nloc)) - pres(iloc(1:nloc),ii-nvel(1)+2)*charX(1:nloc,1) &
                                                                                                 - ulc(iloc(1:nloc),nden)*charX(1:nloc,ii-nvel(1)+2)
                END DO
                myrhs_loc((neng-1)*mynloc+iloc(1:nloc)) = myrhs_loc((neng-1)*mynloc+iloc(1:nloc)) - 0.5_pr*SUM(pres(iloc(1:nloc),2:dim+1)**2,DIM=2)*charX(1:nloc,1) &
                             - SUM(ulc(iloc(1:nloc),nvel(1):nvel(dim))*charX(1:nloc,2:dim+1),DIM=2) - 1.0_pr/(d2u(1,iloc(1:nloc),1)-1.0_pr)*charX(1:nloc,locnvar) 
                DO ii=1,Nspecm
                   myrhs_loc((nspc(ii)-1)*mynloc+iloc(1:nloc)) = myrhs_loc((nspc(ii)-1)*mynloc+iloc(1:nloc)) - pres(iloc(1:nloc),ii+dim+1)*charX(1:nloc,1) &
                                                                                                 - ulc(iloc(1:nloc),nden)*charX(1:nloc,ii+dim+1)
                END DO
                IF (ALLOCATED(charI)) DEALLOCATE(charI)
                IF (ALLOCATED(charX)) DEALLOCATE(charX) 
             END IF
          END IF
       END IF
    END DO
    IF (ALLOCATED(pres)) DEALLOCATE(pres) 
    IF (ALLOCATED(du)) DEALLOCATE(du) 
    IF (ALLOCATED(d2u)) DEALLOCATE(d2u) 
  END SUBROUTINE user_evol_LODIaddon_BC_rhs 

  SUBROUTINE user_evol_LODIaddon_BC_Drhs (Dmyrhs_loc, ulc, ulc_prev, myne_loc, mynloc, myjl, mymeth)
    !--Sets rhs for boundary conditions
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: myne_loc, mynloc, myjl, mymeth
    REAL (pr), DIMENSION (mynloc*myne_loc), INTENT (INOUT) :: Dmyrhs_loc
    REAL (pr), DIMENSION (mynloc,myne_loc), INTENT (IN) :: ulc, ulc_prev

    INTEGER :: ie, i, ii, shift, jj
    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc

    REAL (pr), DIMENSION (:),     ALLOCATABLE :: tempDrhs_loc
    REAL (pr), DIMENSION (:,:),   ALLOCATABLE :: pres, pres_prev
    REAL (pr), DIMENSION (:,:,:), ALLOCATABLE :: du, d2u
    REAL (pr), DIMENSION (:,:),   ALLOCATABLE :: charI, charX, charI_prev, charX_prev
    
    REAL (pr) :: cp, R, curY, cp_prev, R_prev, curY_prev, faceval
    INTEGER :: l, nstp, faceint

    IF (ALLOCATED(tempDrhs_loc)) DEALLOCATE(tempDrhs_loc)
    ALLOCATE(tempDrhs_loc(myne_loc*mynloc))
    CALL internal_Drhs(tempDrhs_loc,ulc,ulc_prev,mymeth,0,1)
    i_p_face(0) = 1
    DO i=1,dim
       i_p_face(i) = i_p_face(i-1)*3
    END DO
    DO face_type = 0, 3**dim - 1
       face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
       IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
          CALL get_all_indices_by_face (face_type, myjl, nloc, iloc)
          IF(nloc > 0 ) THEN
             IF (ABS(face(1)) == 1) THEN
                DO jj=1,myne_loc
                   Dmyrhs_loc((jj-1)*mynloc+iloc(1:nloc)) = tempDrhs_loc((jj-1)*mynloc+iloc(1:nloc))  !from internal_Drhs
                END DO
             END IF
          END IF
       END IF
    END DO
    IF (ALLOCATED(tempDrhs_loc)) DEALLOCATE(tempDrhs_loc) 

    IF (ALLOCATED(pres)) DEALLOCATE(pres) 
    IF (ALLOCATED(pres_prev)) DEALLOCATE(pres_prev) 
    IF (ALLOCATED(du)) DEALLOCATE(du) 
    IF (ALLOCATED(d2u)) DEALLOCATE(d2u) 
    ALLOCATE(pres(mynloc,locnvar),pres_prev(mynloc,locnvar),du(locnvar,mynloc,dim),d2u(locnvar,mynloc,dim))

    DO nstp=1,mynloc
       curY_prev=1.0_pr
       curY=0.0_pr
       IF (Nspec>1) curY_prev=1.0_pr-SUM(ulc_prev(nstp,nspc(1):nspc(Nspecm)))/ulc_prev(nstp,nden) !Y0_Nspec
       IF (Nspec>1) curY=(-SUM(ulc(nstp,nspc(1):nspc(Nspecm)))-curY_prev*ulc(nstp,nden))/ulc_prev(nstp,nden) !Y'_Nspec
       R  = curY/MW_in(Nspec)  !R'_Nspec
       cp = curY*cp_in(Nspec)  !cp'_Nspec
       cp_prev = curY_prev*cp_in(Nspec) 
       R_prev  = curY_prev/MW_in(Nspec)
       DO l=1,Nspecm
          cp = cp + (ulc(nstp,nspc(l))-ulc_prev(nstp,nspc(l))/ulc_prev(nstp,nden)*ulc(nstp,nden))/ulc_prev(nstp,nden)*cp_in(l)  !cp'
          R  = R  + (ulc(nstp,nspc(l))-ulc_prev(nstp,nspc(l))/ulc_prev(nstp,nden)*ulc(nstp,nden))/ulc_prev(nstp,nden)/MW_in(l)  !R'
          cp_prev = cp_prev + ulc_prev(nstp,nspc(l))/ulc_prev(nstp,nden)*cp_in(l)  !cp0
          R_prev  = R_prev  + ulc_prev(nstp,nspc(l))/ulc_prev(nstp,nden)/MW_in(l)  !R0
       END DO
       pres_prev(nstp,1) = (ulc_prev(nstp,neng)-0.5_pr*SUM(ulc_prev(nstp,nvel(1):nvel(dim))**2)/ulc_prev(nstp,nden))/(cp_prev/R_prev-1.0_pr)  !pressure
       pres(nstp,1) = ( (ulc_prev(nstp,neng)-0.5_pr*SUM(ulc_prev(nstp,nvel(1):nvel(dim))**2)/ulc_prev(nstp,nden))*(cp_prev*R-cp*R_prev)/(cp_prev-R_prev)**2 +  &
                (ulc(nstp,neng)+0.5_pr*SUM(ulc_prev(nstp,nvel(1):nvel(dim))**2)/ulc_prev(nstp,nden)**2*ulc(nstp,nden) -  &
                 SUM(ulc_prev(nstp,nvel(1):nvel(dim))*ulc(nstp,nvel(1):nvel(dim)))/ulc_prev(nstp,nden) ) /  &
                (cp_prev/R_prev-1.0_pr) )
    END DO
    DO i=1,dim
       pres_prev(:,1+i) = ulc_prev(:,nvel(i))/ulc_prev(:,nden)  !u0
       pres(:,1+i) = (ulc(:,nvel(i)) - ulc_prev(:,nvel(i))*ulc(:,nden)/ulc_prev(:,nden) )/ulc_prev(:,nden) !u'
    END DO
    DO i=1,Nspecm
       pres_prev(:,dim+1+i) = ulc_prev(:,nspc(i))/ulc_prev(:,nden)  !Y0
       pres(:,dim+1+i) = (ulc(:,nspc(i)) - ulc_prev(:,nspc(i))*ulc(:,nden)/ulc_prev(:,nden) )/ulc_prev(:,nden) !Y'
    END DO 
    pres_prev(:,locnvar) = ulc_prev(:,nden) !rho0
    pres(:,locnvar) = ulc(:,nden) !rho'
 
    i_p_face(0) = 1
    DO i=1,dim
       i_p_face(i) = i_p_face(i-1)*3
    END DO
    DO face_type = 0, 3**dim - 1
       face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
       IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
          CALL get_all_indices_by_face (face_type, myjl, nloc, iloc)
          IF(nloc > 0 ) THEN
             IF (ABS(face(1)) == 1) THEN
                IF (ALLOCATED(charI)) DEALLOCATE(charI)
                IF (ALLOCATED(charX)) DEALLOCATE(charX)
                IF (ALLOCATED(charI_prev)) DEALLOCATE(charI_prev)
                IF (ALLOCATED(charX_prev)) DEALLOCATE(charX_prev)
                ALLOCATE(charI(nloc,locnvar),charX(nloc,locnvar),charI_prev(nloc,locnvar),charX_prev(nloc,locnvar))

                faceval=REAL(face(1),pr)
                faceint=(3-face(1))/2

                IF ((methpd .ne. 0) .OR. splitFBpress) THEN 
                   du(1,:,1) = pres(:,1)
                   IF (Nspec>1) THEN
                      CALL dpdx (du(1,:,1),ng,pres_prev(:,dim+2:dim+Nspec),2*methpd+mymeth,myjl,splitFBpress)
                   ELSE
                      CALL dpdx (du(1,:,1),ng,pres_prev(:,1),2*methpd+mymeth,myjl,.FALSE.)
                   END IF
                   CALL c_diff_fast(pres(:,2:locnvar), du(2:locnvar,:,:), d2u(2:locnvar,:,:), myjl, mynloc, mymeth, 10, locnvar-1, 1, locnvar-1)  
                ELSE
                   CALL c_diff_fast(pres(:,1:locnvar), du(1:locnvar,:,:), d2u(1:locnvar,:,:), myjl, mynloc, mymeth, 10, locnvar, 1, locnvar) 
                END IF

                d2u(1,iloc(1:nloc),1) = 1.0_pr
                IF (Nspec>1) d2u(1,iloc(1:nloc),1) = 1.0_pr-SUM(ulc_prev(iloc(1:nloc),nspc(1):nspc(Nspecm)),DIM=2)/ulc_prev(iloc(1:nloc),nden) !Y_Nspec 0
                d2u(2,iloc(1:nloc),1) = d2u(1,iloc(1:nloc),1)*cp_in(Nspec)  !cp_Nspec 0
                d2u(2,iloc(1:nloc),2) = d2u(1,iloc(1:nloc),1)/MW_in(Nspec)  !R_Nspec 0
                DO l=1,Nspecm
                   d2u(2,iloc(1:nloc),1)  = d2u(2,iloc(1:nloc),1)  + ulc_prev(iloc(1:nloc),nspc(l))/ulc_prev(iloc(1:nloc),nden)*cp_in(l)  !cp 0
                   d2u(2,iloc(1:nloc),2)  = d2u(2,iloc(1:nloc),2)  + ulc_prev(iloc(1:nloc),nspc(l))/ulc_prev(iloc(1:nloc),nden)/MW_in(l)  !R 0 
                END DO
                d2u(1,iloc(1:nloc),1) = d2u(2,iloc(1:nloc),1)/(d2u(2,iloc(1:nloc),1)-d2u(2,iloc(1:nloc),2))  !gamma0=cp0/(cp0-R0)
                d2u(1,iloc(1:nloc),2) = DSQRT(d2u(1,iloc(1:nloc),1)*pres_prev(iloc(1:nloc),1)/ulc_prev(iloc(1:nloc),nden))  !c0=SQRT(gamma0*P0/rho0) 

                charI(1:nloc,2) = faceval*MAX(0.0_pr,faceval*pres_prev(iloc(1:nloc),2)) * &
                                       (d2u(1,iloc(1:nloc),2)**2*du(locnvar,iloc(1:nloc),1) - du(1,iloc(1:nloc),1))
                DO ii=3,dim+Nspec
                   charI(1:nloc,ii) = faceval*MAX(0.0_pr,faceval*pres_prev(iloc(1:nloc),2)) * du(ii,iloc(1:nloc),1)
                END DO
                charI(1:nloc,locnvar) = faceval*MAX(0.0_pr,faceval*(pres_prev(iloc(1:nloc),2) + d2u(1,iloc(1:nloc),2))) * &
                                              (du(1,iloc(1:nloc),1) + ulc_prev(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)*du(2,iloc(1:nloc),1)) 
                charI(1:nloc,1) =       faceval*MAX(0.0_pr,faceval*(pres_prev(iloc(1:nloc),2) - d2u(1,iloc(1:nloc),2))) * &
                                              (du(1,iloc(1:nloc),1) - ulc_prev(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)*du(2,iloc(1:nloc),1)) 

                IF ((methpd .ne. 0) .OR. splitFBpress) THEN 
                   du(1,:,1) = pres_prev(:,1)
                   IF (Nspec>1) THEN
                      CALL dpdx (du(1,:,1),ng,pres_prev(:,dim+2:dim+Nspec),2*methpd+mymeth,myjl,splitFBpress)
                   ELSE
                      CALL dpdx (du(1,:,1),ng,pres_prev(:,1),2*methpd+mymeth,myjl,.FALSE.)
                   END IF
                   CALL c_diff_fast(pres_prev(:,2:locnvar), du(2:locnvar,:,:), d2u(2:locnvar,:,:), myjl, mynloc, mymeth, 10, locnvar-1, 1, locnvar-1)  
                ELSE
                   CALL c_diff_fast(pres_prev(:,1:locnvar), du(1:locnvar,:,:), d2u(1:locnvar,:,:), myjl, mynloc, mymeth, 10, locnvar  , 1, locnvar  ) 
                END IF

                IF (pertchar) THEN
                   du(1,iloc(1:nloc),1) = du(1,iloc(1:nloc),1) - dP0(faceint)
                   du(locnvar,iloc(1:nloc),1) = du(locnvar,iloc(1:nloc),1) - drho0(faceint)
                END IF

                d2u(1,iloc(1:nloc),1) = 1.0_pr
                IF (Nspec>1) d2u(1,iloc(1:nloc),1) = 1.0_pr-SUM(ulc_prev(iloc(1:nloc),nspc(1):nspc(Nspecm)),DIM=2)/ulc_prev(iloc(1:nloc),nden) !Y_Nspec 0
                d2u(2,iloc(1:nloc),1) = d2u(1,iloc(1:nloc),1)*cp_in(Nspec)  !cp_Nspec 0
                d2u(2,iloc(1:nloc),2) = d2u(1,iloc(1:nloc),1)/MW_in(Nspec)  !R_Nspec 0
                DO l=1,Nspecm
                   d2u(2,iloc(1:nloc),1)  = d2u(2,iloc(1:nloc),1)  + ulc_prev(iloc(1:nloc),nspc(l))/ulc_prev(iloc(1:nloc),nden)*cp_in(l)  !cp 0
                   d2u(2,iloc(1:nloc),2)  = d2u(2,iloc(1:nloc),2)  + ulc_prev(iloc(1:nloc),nspc(l))/ulc_prev(iloc(1:nloc),nden)/MW_in(l)  !R 0 
                END DO
                d2u(1,iloc(1:nloc),1) = d2u(2,iloc(1:nloc),1)/(d2u(2,iloc(1:nloc),1)-d2u(2,iloc(1:nloc),2))  !gamma0=cp0/(cp0-R0)
                d2u(1,iloc(1:nloc),2) = DSQRT(d2u(1,iloc(1:nloc),1)*pres_prev(iloc(1:nloc),1)/ulc_prev(iloc(1:nloc),nden))  !c0=SQRT(gamma0*P0/rho0)
 
                du(2,iloc(1:nloc),2) = 0.0_pr
                IF (Nspec>1) du(2,iloc(1:nloc),2) = (-SUM(ulc(iloc(1:nloc),nspc(1):nspc(Nspecm)),DIM=2)-(ulc_prev(iloc(1:nloc),nden)-SUM(ulc_prev(iloc(1:nloc),nspc(1):nspc(Nspecm)),DIM=2)) & 
                                             /ulc_prev(iloc(1:nloc),nden)*ulc(iloc(1:nloc),nden) )/ulc_prev(iloc(1:nloc),nden)  !Y'_Nspec
                du(1,iloc(1:nloc),2) = du(2,iloc(1:nloc),2)*cp_in(Nspec) !cp'_Nspec
                du(2,iloc(1:nloc),2) = du(2,iloc(1:nloc),2)/MW_in(Nspec) !R'_Nspec
                DO l=1,Nspecm
                   du(1,iloc(1:nloc),2)  = du(1,iloc(1:nloc),2)  + (ulc(iloc(1:nloc),nspc(l)) - &
                                 ulc_prev(iloc(1:nloc),nspc(l))/ulc_prev(iloc(1:nloc),nden)*ulc(iloc(1:nloc),nden))/ulc_prev(iloc(1:nloc),nden)*cp_in(l)  !cp'
                   du(2,iloc(1:nloc),2)  = du(2,iloc(1:nloc),2)  + (ulc(iloc(1:nloc),nspc(l)) - &
                                 ulc_prev(iloc(1:nloc),nspc(l))/ulc_prev(iloc(1:nloc),nden)*ulc(iloc(1:nloc),nden))/ulc_prev(iloc(1:nloc),nden)/MW_in(l)  !R'
                END DO
                du(1,iloc(1:nloc),2) = (d2u(2,iloc(1:nloc),1)*du(2,iloc(1:nloc),2) - du(1,iloc(1:nloc),2)*d2u(2,iloc(1:nloc),2)) / &
                                             (d2u(2,iloc(1:nloc),1)-d2u(2,iloc(1:nloc),2))**2  !gamma'=(cp0*R'-cp'*R0)/(cp0-R0)^2
                du(2,iloc(1:nloc),2) = 0.5_pr/DSQRT(d2u(1,iloc(1:nloc),1)*pres_prev(iloc(1:nloc),1)/ulc_prev(iloc(1:nloc),nden)) * &
                        (d2u(1,iloc(1:nloc),1)*pres(iloc(1:nloc),1)+du(1,iloc(1:nloc),2)*pres_prev(iloc(1:nloc),1))/ulc_prev(iloc(1:nloc),nden) - &
                        d2u(1,iloc(1:nloc),1)*pres_prev(iloc(1:nloc),1)/ulc_prev(iloc(1:nloc),nden)**2*ulc(iloc(1:nloc),nden)  !c'=1/2/SQRT(gamma0*P0/rho0)*(gamma*P/rho)'

                charI_prev(1:nloc,2) = faceval*MAX(0.0_pr,faceval*pres_prev(iloc(1:nloc),2))*(d2u(1,iloc(1:nloc),2)**2*du(locnvar,iloc(1:nloc),1) - du(1,iloc(1:nloc),1)) 
                        !u*(c^2*drho/dx - dp/dx)
                DO ii=3,dim+Nspec
                   charI_prev(1:nloc,ii) = faceval*MAX(0.0_pr,faceval*pres_prev(iloc(1:nloc),2))*du(ii,iloc(1:nloc),1)
                END DO
                charI_prev(1:nloc,locnvar) = faceval*MAX(0.0_pr,faceval*(pres_prev(iloc(1:nloc),2) + d2u(1,iloc(1:nloc),2))) * &
                           (du(1,iloc(1:nloc),1) + ulc_prev(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)*du(2,iloc(1:nloc),1))  !(u+c)*(dp/dx+rho*c*du/dx)
                charI_prev(1:nloc,1) = faceval*MAX(0.0_pr,faceval*(pres_prev(iloc(1:nloc),2) - d2u(1,iloc(1:nloc),2))) * &
                           (du(1,iloc(1:nloc),1) - ulc_prev(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)*du(2,iloc(1:nloc),1))  !(u-c)*(dp/dx-rho*c*du/dx)

                charI(1:nloc,2) = charI(1:nloc,2) + (SIGN(1.0_pr,faceval*pres_prev(iloc(1:nloc),2))+1.0_pr)/2.0_pr * pres(iloc(1:nloc),2) * &
                                        (d2u(1,iloc(1:nloc),2)**2*du(locnvar,iloc(1:nloc),1) - du(1,iloc(1:nloc),1)) !u*(c^2*drho/dx - dp/dx)
                DO ii=3,dim+Nspec
                   charI(1:nloc,ii) = charI(1:nloc,ii) + (SIGN(1.0_pr,faceval*pres_prev(iloc(1:nloc),2))+1.0_pr)/2.0_pr * pres(iloc(1:nloc),2)*du(ii,iloc(1:nloc),1)
                END DO
                charI(1:nloc,locnvar) = charI(1:nloc,locnvar) + (SIGN(1.0_pr,faceval*(pres_prev(iloc(1:nloc),2) + d2u(1,iloc(1:nloc),2) ) )+1.0_pr)/2.0_pr * &
                                                                                           (pres(iloc(1:nloc),2) + du(2,iloc(1:nloc),2)) * &
                           (du(1,iloc(1:nloc),1) + ulc_prev(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)*du(2,iloc(1:nloc),1))  !(u+c)*(dp/dx+rho*c*du/dx)
                charI(1:nloc,1) = charI(1:nloc,1) + (SIGN(1.0_pr,faceval*(pres_prev(iloc(1:nloc),2) - d2u(1,iloc(1:nloc),2) ) )+1.0_pr)/2.0_pr * &
                                                                              (pres(iloc(1:nloc),2) -  du(2,iloc(1:nloc),2)) * &
                           (du(1,iloc(1:nloc),1) - ulc_prev(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)*du(2,iloc(1:nloc),1))  !(u-c)*(dp/dx-rho*c*du/dx)
                charI(1:nloc,2) = charI(1:nloc,2) + faceval*MAX(0.0_pr,faceval*pres_prev(iloc(1:nloc),2)) * &
                              2.0_pr*d2u(1,iloc(1:nloc),2)*du(2,iloc(1:nloc),2)*du(locnvar,iloc(1:nloc),1)

                charI(1:nloc,locnvar) = charI(1:nloc,locnvar) + faceval*MAX(0.0_pr,faceval*(pres_prev(iloc(1:nloc),2) + d2u(1,iloc(1:nloc),2))) * du(2,iloc(1:nloc),1) * &
                           (ulc(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)+ulc_prev(iloc(1:nloc),nden)*du(2,iloc(1:nloc),2)) 
                charI(1:nloc,1)       = charI(1:nloc,1)       - faceval*MAX(0.0_pr,faceval*(pres_prev(iloc(1:nloc),2) - d2u(1,iloc(1:nloc),2))) * du(2,iloc(1:nloc),1) * &
                           (ulc(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)+ulc_prev(iloc(1:nloc),nden)*du(2,iloc(1:nloc),2)) 

                IF (PLpinf) THEN
                   charI_prev(1:nloc,locnvar) = charI_prev(1:nloc,locnvar) + (1.0_pr-SIGN(1.0_pr,faceval* (pres_prev(iloc(1:nloc),2) + d2u(1,iloc(1:nloc),2)) ))/2.0_pr * pBcoef * &
                                                                   (1.0_pr-maxMa**2) * &
                                                                   d2u(1,iloc(1:nloc),2) * (pres_prev(iloc(1:nloc),1) - P0(faceint) )
                   charI_prev(1:nloc,1)       = charI_prev(1:nloc,1)       + (1.0_pr-SIGN(1.0_pr,faceval* (pres_prev(iloc(1:nloc),2) - d2u(1,iloc(1:nloc),2)) ))/2.0_pr * pBcoef * &
                                                                   (1.0_pr-maxMa**2) * &
                                                                   d2u(1,iloc(1:nloc),2) * (pres_prev(iloc(1:nloc),1) - P0(faceint) )

                   charI(1:nloc,locnvar) = charI(1:nloc,locnvar) + (1.0_pr-SIGN(1.0_pr,faceval* (pres_prev(iloc(1:nloc),2) + d2u(1,iloc(1:nloc),2)) ))/2.0_pr * pBcoef * &
                                                                   (1.0_pr-maxMa**2) * &
                                                                   (du(2,iloc(1:nloc),2) * (pres_prev(iloc(1:nloc),1) - P0(faceint) ) + d2u(1,iloc(1:nloc),2)*pres(iloc(1:nloc),1))
                   charI(1:nloc,1)       = charI(1:nloc,1)       + (1.0_pr-SIGN(1.0_pr,faceval* (pres_prev(iloc(1:nloc),2) - d2u(1,iloc(1:nloc),2)) ))/2.0_pr * pBcoef * &
                                                                   (1.0_pr-maxMa**2) * &
                                                                   (du(2,iloc(1:nloc),2) * (pres_prev(iloc(1:nloc),1) - P0(faceint) ) + d2u(1,iloc(1:nloc),2)*pres(iloc(1:nloc),1))
                END IF

                charX_prev(1:nloc,1) = -0.5_pr/d2u(1,iloc(1:nloc),2)**2*(charI_prev(1:nloc,1)+charI_prev(1:nloc,locnvar)+2.0_pr*charI_prev(1:nloc,2))
                charX_prev(1:nloc,2) = -0.5_pr/d2u(1,iloc(1:nloc),2)/ulc_prev(iloc(1:nloc),nden)*(charI_prev(1:nloc,locnvar)-charI_prev(1:nloc,1))
                DO ii=3,dim+Nspec
                   charX_prev(1:nloc,ii) = -charI_prev(1:nloc,ii)
                END DO
                charX_prev(1:nloc,locnvar) = -0.5_pr*(charI_prev(1:nloc,locnvar)+charI_prev(1:nloc,1))

                charX(1:nloc,1) = - ( 0.5_pr/d2u(1,iloc(1:nloc),2)**2*(charI(1:nloc,1)+charI(1:nloc,locnvar)+2.0_pr*charI(1:nloc,2)) &
                                  - charX_prev(1:nloc,1)*2.0_pr*du(2,iloc(1:nloc),2)/d2u(1,iloc(1:nloc),2) )
                charX(1:nloc,2) = - ( 0.5_pr/d2u(1,iloc(1:nloc),2)/ulc_prev(iloc(1:nloc),nden) * &
                       (charI(1:nloc,locnvar) - charI(1:nloc,1) - (ulc(iloc(1:nloc),nden)/ulc_prev(iloc(1:nloc),nden) + du(2,iloc(1:nloc),2)/d2u(1,iloc(1:nloc),2)) * &
                       (charI_prev(1:nloc,locnvar)-charI_prev(1:nloc,1))) )
                DO ii=3,dim+Nspec
                   charX(1:nloc,ii) = -charI(1:nloc,ii)
                END DO
                charX(1:nloc,locnvar) = -0.5_pr*(charI(1:nloc,locnvar)+charI(1:nloc,1))

                Dmyrhs_loc((nden-1)*mynloc+iloc(1:nloc)) = Dmyrhs_loc((nden-1)*mynloc+iloc(1:nloc)) - charX(1:nloc,1)
                DO ii=nvel(1),nvel(dim)
                   Dmyrhs_loc((ii-1)*mynloc+iloc(1:nloc)) = Dmyrhs_loc((ii-1)*mynloc+iloc(1:nloc))  & 
                                                            - pres_prev(iloc(1:nloc),ii-nvel(1)+2)*charX(1:nloc,1) - pres(iloc(1:nloc),ii-nvel(1)+2)*charX_prev(1:nloc,1) &
                                                            - ulc_prev(iloc(1:nloc),nden)*charX(1:nloc,ii-nvel(1)+2) - ulc(iloc(1:nloc),nden)*charX_prev(1:nloc,ii-nvel(1)+2)
                END DO
                Dmyrhs_loc((neng-1)*mynloc+iloc(1:nloc)) = Dmyrhs_loc((neng-1)*mynloc+iloc(1:nloc)) &
                      - 0.5_pr*SUM(pres_prev(iloc(1:nloc),2:dim+1)**2,DIM=2)*charX(1:nloc,1) - SUM(pres(iloc(1:nloc),2:dim+1)*pres_prev(iloc(1:nloc),2:dim+1),DIM=2)*charX_prev(1:nloc,1) &
                      - SUM(ulc_prev(iloc(1:nloc),nvel(1):nvel(dim))*charX(1:nloc,2:dim+1),DIM=2) - SUM(ulc(iloc(1:nloc),nvel(1):nvel(dim))*charX_prev(1:nloc,2:dim+1),DIM=2) &
                      - 1.0_pr/(d2u(1,iloc(1:nloc),1)-1.0_pr)*(charX(1:nloc,locnvar) - du(1,iloc(1:nloc),2)/(d2u(1,iloc(1:nloc),1)-1.0_pr)*charX_prev(1:nloc,locnvar)) 
                DO ii=1,Nspecm
                   Dmyrhs_loc((nspc(ii)-1)*mynloc+iloc(1:nloc)) = Dmyrhs_loc((nspc(ii)-1)*mynloc+iloc(1:nloc))  & 
                                                            - pres_prev(iloc(1:nloc),ii+dim+1)*charX(1:nloc,1) - pres(iloc(1:nloc),ii+dim+1)*charX_prev(1:nloc,1) &
                                                            - ulc_prev(iloc(1:nloc),nden)*charX(1:nloc,ii+dim+1) - ulc(iloc(1:nloc),nden)*charX_prev(1:nloc,ii+dim+1)
                END DO

                IF (ALLOCATED(charI)) DEALLOCATE(charI)
                IF (ALLOCATED(charX)) DEALLOCATE(charX)
                IF (ALLOCATED(charI_prev)) DEALLOCATE(charI_prev)
                IF (ALLOCATED(charX_prev)) DEALLOCATE(charX_prev)
             END IF
          END IF
       END IF
    END DO

    IF (ALLOCATED(pres)) DEALLOCATE(pres) 
    IF (ALLOCATED(pres_prev)) DEALLOCATE(pres_prev) 
    IF (ALLOCATED(du)) DEALLOCATE(du) 
    IF (ALLOCATED(d2u)) DEALLOCATE(d2u) 
  END SUBROUTINE user_evol_LODIaddon_BC_Drhs 

  SUBROUTINE user_evol_LODIaddon_BC_Drhs_diag (Drhs_diag_local, ulc_prev, myne_loc, mynloc, myjl, mymeth)
    !--Sets rhs for boundary conditions
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: myne_loc, mynloc, myjl, mymeth
    REAL (pr), DIMENSION (mynloc*myne_loc), INTENT (INOUT) :: Drhs_diag_local
    REAL (pr), DIMENSION (mynloc,myne_loc), INTENT (IN) :: ulc_prev

    INTEGER :: ie, i, ii, shift, jj
    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc

    REAL (pr), DIMENSION (:),     ALLOCATABLE :: tempdiag_loc
    REAL (pr), DIMENSION (:,:),   ALLOCATABLE :: charI, charX, charI_prev, charX_prev
    REAL (pr), DIMENSION (mynloc,dim) :: du_diag_local, d2u_diag_local
    REAL (pr), DIMENSION (mynloc,Nspec+2):: pres_prev
    REAL (pr), DIMENSION (3,mynloc,dim) :: du, d2u
    REAL (pr) :: cp_prev, R_prev, curY_prev, faceval
    INTEGER :: l, nstp, faceint

    IF (ALLOCATED(tempdiag_loc)) DEALLOCATE(tempdiag_loc)
    ALLOCATE(tempdiag_loc(myne_loc*mynloc))
    CALL internal_Drhs_diag(tempdiag_loc,ulc_prev,mymeth,0,1)
    i_p_face(0) = 1
    DO i=1,dim
       i_p_face(i) = i_p_face(i-1)*3
    END DO
    DO face_type = 0, 3**dim - 1
       face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
       IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
          CALL get_all_indices_by_face (face_type, myjl, nloc, iloc)
          IF(nloc > 0 ) THEN
             IF (ABS(face(1)) == 1) THEN
                DO jj=1,myne_loc
                   Drhs_diag_local((jj-1)*mynloc+iloc(1:nloc)) = tempdiag_loc((jj-1)*mynloc+iloc(1:nloc))  !from internal_Drhs_diag
                END DO
             END IF
          END IF
       END IF
    END DO
    IF (ALLOCATED(tempdiag_loc)) DEALLOCATE(tempdiag_loc) 

    CALL c_diff_diag(du_diag_local, d2u_diag_local, j_lev, mynloc, mymeth, mymeth, 10)

    DO nstp=1,mynloc
       curY_prev=1.0_pr
       IF (Nspec>1) curY_prev=1.0_pr-SUM(ulc_prev(nstp,nspc(1):nspc(Nspecm)))/ulc_prev(nstp,nden) !Y0_Nspec
       cp_prev = curY_prev*cp_in(Nspec)
       R_prev  = curY_prev/MW_in(Nspec)
       DO l=1,Nspecm
          cp_prev = cp_prev + ulc_prev(nstp,nspc(l))/ulc_prev(nstp,nden)*cp_in(l)  !cp0
          R_prev  = R_prev  + ulc_prev(nstp,nspc(l))/ulc_prev(nstp,nden)/MW_in(l)  !R0
       END DO
       pres_prev(nstp,1) = (ulc_prev(nstp,neng)-0.5_pr*SUM(ulc_prev(nstp,nvel(1):nvel(dim))**2)/ulc_prev(nstp,nden))/(cp_prev/R_prev-1.0_pr)  !pressure
    END DO
    pres_prev(:,2) = ulc_prev(:,nvel(1))/ulc_prev(:,nden)  !u0
    pres_prev(:,3) = ulc_prev(:,nden)  !rho0
 
    IF ((methpd .ne. 0) .OR. splitFBpress) THEN 
       du(1,:,1) = pres_prev(:,1)
       DO i=1,Nspecm
          pres_prev(:,3+i) = ulc_prev(:,nspc(i))/ulc_prev(:,nden)
       END DO
       IF (Nspec>1) THEN
          CALL dpdx (du(1,:,1),ng,pres_prev(:,4:Nspec+2),2*methpd+mymeth,myjl,splitFBpress)
       ELSE
          CALL dpdx (du(1,:,1),ng,pres_prev(:,1),2*methpd+mymeth,myjl,.FALSE.)
       END IF
       CALL c_diff_fast(pres_prev(:,2:3), du(2:3,:,:), d2u(2:3,:,:), myjl, mynloc, mymeth, 10, 2, 1, 2)  
    ELSE
       CALL c_diff_fast(pres_prev(:,1:3), du(1:3,:,:), d2u(1:3,:,:), myjl, mynloc, mymeth, 10, 3, 1, 3) 
    END IF

    i_p_face(0) = 1
    DO i=1,dim
       i_p_face(i) = i_p_face(i-1)*3
    END DO
    DO face_type = 0, 3**dim - 1
       face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
       IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
          CALL get_all_indices_by_face (face_type, myjl, nloc, iloc)
          IF(nloc > 0 ) THEN
             IF (ABS(face(1)) == 1) THEN
                IF (ALLOCATED(charI)) DEALLOCATE(charI)
                IF (ALLOCATED(charX)) DEALLOCATE(charX)
                IF (ALLOCATED(charI_prev)) DEALLOCATE(charI_prev)
                IF (ALLOCATED(charX_prev)) DEALLOCATE(charX_prev)
                ALLOCATE(charI(nloc,4),charX(nloc,3),charI_prev(nloc,3),charX_prev(nloc,1))

                faceval=REAL(face(1),pr)
                faceint=(3-face(1))/2

                d2u(1,iloc(1:nloc),1) = 1.0_pr
                IF (Nspec>1) d2u(1,iloc(1:nloc),1) = 1.0_pr-SUM(ulc_prev(iloc(1:nloc),nspc(1):nspc(Nspecm)),DIM=2)/ulc_prev(iloc(1:nloc),nden) !Y_Nspec 0
                d2u(2,iloc(1:nloc),1) = d2u(1,iloc(1:nloc),1)*cp_in(Nspec)  !cp_Nspec 0
                d2u(2,iloc(1:nloc),2) = d2u(1,iloc(1:nloc),1)/MW_in(Nspec)  !R_Nspec 0
                DO l=1,Nspecm
                   d2u(2,iloc(1:nloc),1)  = d2u(2,iloc(1:nloc),1)  + ulc_prev(iloc(1:nloc),nspc(l))/ulc_prev(iloc(1:nloc),nden)*cp_in(l)  !cp 0
                   d2u(2,iloc(1:nloc),2)  = d2u(2,iloc(1:nloc),2)  + ulc_prev(iloc(1:nloc),nspc(l))/ulc_prev(iloc(1:nloc),nden)/MW_in(l)  !R 0 
                END DO
                d2u_diag_local(iloc(1:nloc),1) = d2u(2,iloc(1:nloc),1) !cp0
                d2u_diag_local(iloc(1:nloc),2) = d2u(2,iloc(1:nloc),2) !R0
                du(2,iloc(1:nloc),2) = d2u(2,iloc(1:nloc),1)/(d2u(2,iloc(1:nloc),1)-d2u(2,iloc(1:nloc),2))  !gamma0=cp0/(cp0-R0)
                du(1,iloc(1:nloc),2) = DSQRT(du(2,iloc(1:nloc),2)*pres_prev(iloc(1:nloc),1)/ulc_prev(iloc(1:nloc),nden))  !c0=SQRT(gamma0*P0/rho0)

                IF (pertchar) THEN
                   du(1,iloc(1:nloc),1) = du(1,iloc(1:nloc),1) - dP0(faceint)
                   du(3,iloc(1:nloc),1) = du(3,iloc(1:nloc),1) - drho0(faceint)
                END IF

                charI_prev(1:nloc,2) = faceval*MAX(0.0_pr,faceval*pres_prev(iloc(1:nloc),2)) * &
                                          (du(1,iloc(1:nloc),2)**2*du(3,iloc(1:nloc),1) - du(1,iloc(1:nloc),1)) !u*(c^2*drho/dx - dp/dx)
                charI_prev(1:nloc,3) = faceval*MAX(0.0_pr,faceval*(pres_prev(iloc(1:nloc),2) + du(1,iloc(1:nloc),2))) * &
                           (du(1,iloc(1:nloc),1) + ulc_prev(iloc(1:nloc),nden)*du(1,iloc(1:nloc),2)*du(2,iloc(1:nloc),1))  !(u+c)*(dp/dx+rho*c*du/dx)
                charI_prev(1:nloc,1) = faceval*MAX(0.0_pr,faceval*(pres_prev(iloc(1:nloc),2) - du(1,iloc(1:nloc),2))) * &
                           (du(1,iloc(1:nloc),1) - ulc_prev(iloc(1:nloc),nden)*du(1,iloc(1:nloc),2)*du(2,iloc(1:nloc),1))  !(u+c)*(dp/dx+rho*c*du/dx)

                IF (PLpinf) THEN
                   charI_prev(1:nloc,3) = charI_prev(1:nloc,3) + (1.0_pr-SIGN(1.0_pr,faceval* (pres_prev(iloc(1:nloc),2) + du(1,iloc(1:nloc),2)) ))/2.0_pr * pBcoef * &
                                                                   (1.0_pr-maxMa**2) * &
                                                                   du(1,iloc(1:nloc),2) * (pres_prev(iloc(1:nloc),1) - P0(faceint) )
                   charI_prev(1:nloc,1) = charI_prev(1:nloc,1) + (1.0_pr-SIGN(1.0_pr,faceval* (pres_prev(iloc(1:nloc),2) - du(1,iloc(1:nloc),2)) ))/2.0_pr * pBcoef * &
                                                                   (1.0_pr-maxMa**2) * &
                                                                   du(1,iloc(1:nloc),2) * (pres_prev(iloc(1:nloc),1) - P0(faceint) )
                END IF

                charX_prev(1:nloc,1) = 0.5_pr/du(1,iloc(1:nloc),2)**2*(charI_prev(1:nloc,1)+charI_prev(1:nloc,3)+2.0_pr*charI_prev(1:nloc,2))

                DO jj=1,myne_loc
                   IF (jj==nden) THEN
                      d2u(2,iloc(1:nloc),1) = -ulc_prev(iloc(1:nloc),nvel(1))/ulc_prev(iloc(1:nloc),nden)**2 !u_diag
                      d2u(2,iloc(1:nloc),2) = 0.0_pr  !gamma_diag
                      d2u(1,iloc(1:nloc),1) = 0.5_pr*SUM(ulc_prev(iloc(1:nloc),nvel(1):nvel(dim))**2,DIM=2)/ulc_prev(iloc(1:nloc),nden)**2*(du(2,iloc(1:nloc),2)-1.0_pr) !p_diag
                      d2u(1,iloc(1:nloc),2) = 0.5_pr/du(1,iloc(1:nloc),2)*du(2,iloc(1:nloc),2)/ulc_prev(iloc(1:nloc),nden) * &
                                                    (d2u(1,iloc(1:nloc),1)-pres_prev(iloc(1:nloc),1)/ulc_prev(iloc(1:nloc),nden) ) !c_diag
                      charI(1:nloc,3) = (SIGN(1.0_pr,faceval*(pres_prev(iloc(1:nloc),2) + du(1,iloc(1:nloc),2) ) )+1.0_pr)/2.0_pr * &
                                      ( (pres_prev(iloc(1:nloc),2)+du(1,iloc(1:nloc),2))*(du_diag_local(iloc(1:nloc),1)* &
                                        (d2u(1,iloc(1:nloc),1)+ulc_prev(iloc(1:nloc),nden)*du(1,iloc(1:nloc),2)*d2u(2,iloc(1:nloc),1)) + &
                                        du(2,iloc(1:nloc),1)*(du(1,iloc(1:nloc),2)+ulc_prev(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2))) + &
                                        (d2u(2,iloc(1:nloc),1)+d2u(1,iloc(1:nloc),2))*(du(1,iloc(1:nloc),1)+ &
                                        ulc_prev(iloc(1:nloc),nden)*du(1,iloc(1:nloc),2)*du(2,iloc(1:nloc),1)) )
                      charI(1:nloc,1) = (SIGN(1.0_pr,faceval*(pres_prev(iloc(1:nloc),2) - du(1,iloc(1:nloc),2) ) )+1.0_pr)/2.0_pr * &
                                      ( (pres_prev(iloc(1:nloc),2)-du(1,iloc(1:nloc),2))*(du_diag_local(iloc(1:nloc),1)* &
                                        (d2u(1,iloc(1:nloc),1)-ulc_prev(iloc(1:nloc),nden)*du(1,iloc(1:nloc),2)*d2u(2,iloc(1:nloc),1)) - &
                                        du(2,iloc(1:nloc),1)*(du(1,iloc(1:nloc),2)+ulc_prev(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2))) + &
                                        (d2u(2,iloc(1:nloc),1)-d2u(1,iloc(1:nloc),2))*(du(1,iloc(1:nloc),1)- &
                                        ulc_prev(iloc(1:nloc),nden)*du(1,iloc(1:nloc),2)*du(2,iloc(1:nloc),1)) )
                      charI(1:nloc,2) = (SIGN(1.0_pr,faceval*pres_prev(iloc(1:nloc),2))+1.0_pr)/2.0_pr * &
                                      ( pres_prev(iloc(1:nloc),2)*(du_diag_local(iloc(1:nloc),1)*(du(1,iloc(1:nloc),2)**2-d2u(1,iloc(1:nloc),1)) + &
                                        2.0_pr*du(1,iloc(1:nloc),2)*d2u(1,iloc(1:nloc),2)*du(3,iloc(1:nloc),1)) + &
                                        d2u(2,iloc(1:nloc),1)*(du(1,iloc(1:nloc),2)**2*du(3,iloc(1:nloc),1)-du(1,iloc(1:nloc),1)) )
                      IF (PLpinf) THEN
                         charI(1:nloc,3) = charI(1:nloc,3) + (1.0_pr-SIGN(1.0_pr,faceval* (pres_prev(iloc(1:nloc),2) + du(1,iloc(1:nloc),2)) ))/2.0_pr * pBcoef * &
                                                                   (1.0_pr-maxMa**2) * &
                                                              (d2u(1,iloc(1:nloc),2)*(pres_prev(iloc(1:nloc),1) - P0(faceint) ) + du(1,iloc(1:nloc),2)*d2u(1,iloc(1:nloc),1))
                         charI(1:nloc,1) = charI(1:nloc,1) + (1.0_pr-SIGN(1.0_pr,faceval* (pres_prev(iloc(1:nloc),2) - du(1,iloc(1:nloc),2)) ))/2.0_pr * pBcoef * &
                                                                   (1.0_pr-maxMa**2) * &
                                                              (d2u(1,iloc(1:nloc),2)*(pres_prev(iloc(1:nloc),1) - P0(faceint) ) + du(1,iloc(1:nloc),2)*d2u(1,iloc(1:nloc),1))
                      END IF
                      charX(1:nloc,1) = - ( 0.5_pr/du(1,iloc(1:nloc),2)**2 * (charI(1:nloc,1)+charI(1:nloc,3)+2.0_pr*charI(1:nloc,2)) - &
                                                     2.0_pr*d2u(1,iloc(1:nloc),2)/du(1,iloc(1:nloc),2)*charX_prev(1:nloc,1) )

                      Drhs_diag_local((jj-1)*mynloc+iloc(1:nloc)) = Drhs_diag_local((jj-1)*mynloc+iloc(1:nloc)) - charX(1:nloc,1)
                   ELSE IF (jj==nvel(1)) THEN
                      d2u(2,iloc(1:nloc),1) = 1.0_pr/ulc_prev(iloc(1:nloc),nden) !u_diag
                      d2u(2,iloc(1:nloc),2) = 0.0_pr  !gamma_diag
                      d2u(1,iloc(1:nloc),1) = -ulc_prev(iloc(1:nloc),nvel(1))/ulc_prev(iloc(1:nloc),nden)*(du(2,iloc(1:nloc),2)-1.0_pr) !p_diag
                      d2u(1,iloc(1:nloc),2) = 0.5_pr/du(1,iloc(1:nloc),2)*du(2,iloc(1:nloc),2)/ulc_prev(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),1) !c_diag
                      charI(1:nloc,3) = (SIGN(1.0_pr,faceval*(pres_prev(iloc(1:nloc),2) + du(1,iloc(1:nloc),2) ) )+1.0_pr)/2.0_pr * &
                                      ( (pres_prev(iloc(1:nloc),2)+du(1,iloc(1:nloc),2))*(du_diag_local(iloc(1:nloc),1)* &
                                        (d2u(1,iloc(1:nloc),1)+ulc_prev(iloc(1:nloc),nden)*du(1,iloc(1:nloc),2)*d2u(2,iloc(1:nloc),1)) + &
                                        du(2,iloc(1:nloc),1)*ulc_prev(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)) + &
                                        (d2u(2,iloc(1:nloc),1)+d2u(1,iloc(1:nloc),2))*(du(1,iloc(1:nloc),1)+ &
                                        ulc_prev(iloc(1:nloc),nden)*du(1,iloc(1:nloc),2)*du(2,iloc(1:nloc),1)) )
                      charI(1:nloc,1) = (SIGN(1.0_pr,faceval*(pres_prev(iloc(1:nloc),2) - du(1,iloc(1:nloc),2) ) )+1.0_pr)/2.0_pr * &
                                      ( (pres_prev(iloc(1:nloc),2)-du(1,iloc(1:nloc),2))*(du_diag_local(iloc(1:nloc),1)* &
                                        (d2u(1,iloc(1:nloc),1)-ulc_prev(iloc(1:nloc),nden)*du(1,iloc(1:nloc),2)*d2u(2,iloc(1:nloc),1)) - &
                                        du(2,iloc(1:nloc),1)*ulc_prev(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)) + &
                                        (d2u(2,iloc(1:nloc),1)-d2u(1,iloc(1:nloc),2))*(du(1,iloc(1:nloc),1)- &
                                        ulc_prev(iloc(1:nloc),nden)*du(1,iloc(1:nloc),2)*du(2,iloc(1:nloc),1)) )
                      charI(1:nloc,2) = (SIGN(1.0_pr,faceval*pres_prev(iloc(1:nloc),2))+1.0_pr)/2.0_pr * &
                                      ( pres_prev(iloc(1:nloc),2)*(2.0_pr*du(1,iloc(1:nloc),2)*d2u(1,iloc(1:nloc),2)*du(3,iloc(1:nloc),1) - &
                                        du_diag_local(iloc(1:nloc),1)*d2u(1,iloc(1:nloc),1)) + &
                                        d2u(2,iloc(1:nloc),1)*(du(1,iloc(1:nloc),2)**2*du(3,iloc(1:nloc),1)-du(1,iloc(1:nloc),1)) )
                      IF (PLpinf) THEN
                         charI(1:nloc,3) = charI(1:nloc,3) + (1.0_pr-SIGN(1.0_pr,faceval* (pres_prev(iloc(1:nloc),2) + du(1,iloc(1:nloc),2)) ))/2.0_pr * pBcoef * &
                                                                   (1.0_pr-maxMa**2) * &
                                                              (d2u(1,iloc(1:nloc),2)*(pres_prev(iloc(1:nloc),1) - P0(faceint) ) + du(1,iloc(1:nloc),2)*d2u(1,iloc(1:nloc),1))
                         charI(1:nloc,1) = charI(1:nloc,1) + (1.0_pr-SIGN(1.0_pr,faceval* (pres_prev(iloc(1:nloc),2) - du(1,iloc(1:nloc),2)) ))/2.0_pr * pBcoef * &
                                                                   (1.0_pr-maxMa**2) * &
                                                              (d2u(1,iloc(1:nloc),2)*(pres_prev(iloc(1:nloc),1) - P0(faceint) ) + du(1,iloc(1:nloc),2)*d2u(1,iloc(1:nloc),1))
                      END IF
                      charX(1:nloc,1) = - ( 0.5_pr/du(1,iloc(1:nloc),2)**2 * (charI(1:nloc,1)+charI(1:nloc,3)+2.0_pr*charI(1:nloc,2)) - &
                                                     2.0_pr*d2u(1,iloc(1:nloc),2)/du(1,iloc(1:nloc),2)*charX_prev(1:nloc,1) )
                      charX(1:nloc,2) = - ( 0.5_pr/ulc_prev(iloc(1:nloc),nden)/du(1,iloc(1:nloc),2) * (charI(1:nloc,3)-charI(1:nloc,1) - &
                                                     d2u(1,iloc(1:nloc),2)/du(1,iloc(1:nloc),2)*(charI_prev(1:nloc,3)-charI_prev(1:nloc,1))) )
   
                      Drhs_diag_local((jj-1)*mynloc+iloc(1:nloc)) = Drhs_diag_local((jj-1)*mynloc+iloc(1:nloc)) - d2u(2,iloc(1:nloc),1)*charX_prev(1:nloc,1) - &
                                                     pres_prev(iloc(1:nloc),2)*charX(1:nloc,1) - ulc_prev(iloc(1:nloc),nden)*charX(1:nloc,2)
                   ELSE IF (jj>=nvel(2) .AND. jj<=nvel(dim)) THEN
                      d2u(2,iloc(1:nloc),1) = 0.0_pr !u_diag
                      d2u(2,iloc(1:nloc),2) = 0.0_pr  !gamma_diag
                      d2u(1,iloc(1:nloc),1) = -ulc_prev(iloc(1:nloc),jj)/ulc_prev(iloc(1:nloc),nden)*(du(2,iloc(1:nloc),2)-1.0_pr) !p_diag
                      d2u(1,iloc(1:nloc),2) = 0.5_pr/du(1,iloc(1:nloc),2)*du(2,iloc(1:nloc),2)/ulc_prev(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),1) !c_diag
                      charI(1:nloc,3) = (SIGN(1.0_pr,faceval*(pres_prev(iloc(1:nloc),2) + du(1,iloc(1:nloc),2) ) )+1.0_pr)/2.0_pr * &
                                      ( (pres_prev(iloc(1:nloc),2)+du(1,iloc(1:nloc),2))*(du_diag_local(iloc(1:nloc),1)*d2u(1,iloc(1:nloc),1) + &
                                        du(2,iloc(1:nloc),1)*ulc_prev(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)) + &
                                        d2u(1,iloc(1:nloc),2)*(du(1,iloc(1:nloc),1)+ulc_prev(iloc(1:nloc),nden)*du(1,iloc(1:nloc),2)*du(2,iloc(1:nloc),1)) )
                      charI(1:nloc,1) = (SIGN(1.0_pr,faceval*(pres_prev(iloc(1:nloc),2) - du(1,iloc(1:nloc),2) ) )+1.0_pr)/2.0_pr * &
                                      ( (pres_prev(iloc(1:nloc),2)-du(1,iloc(1:nloc),2))*(du_diag_local(iloc(1:nloc),1)*d2u(1,iloc(1:nloc),1) - &
                                        du(2,iloc(1:nloc),1)*ulc_prev(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)) - &
                                        d2u(1,iloc(1:nloc),2)*(du(1,iloc(1:nloc),1)-ulc_prev(iloc(1:nloc),nden)*du(1,iloc(1:nloc),2)*du(2,iloc(1:nloc),1)) )
                      charI(1:nloc,2) = faceval*MAX(0.0_pr,faceval*pres_prev(iloc(1:nloc),2))*(2.0_pr*du(1,iloc(1:nloc),2)*d2u(1,iloc(1:nloc),2)*du(3,iloc(1:nloc),1) - &
                                                                   du_diag_local(iloc(1:nloc),1)*d2u(1,iloc(1:nloc),1))
                      charI(1:nloc,4) = faceval*MAX(0.0_pr,faceval*pres_prev(iloc(1:nloc),2))*du_diag_local(iloc(1:nloc),1)/ulc_prev(iloc(1:nloc),nden)
                      IF (PLpinf) THEN
                         charI(1:nloc,3) = charI(1:nloc,3) + (1.0_pr-SIGN(1.0_pr,faceval* (pres_prev(iloc(1:nloc),2) + du(1,iloc(1:nloc),2)) ))/2.0_pr * pBcoef * &
                                                                   (1.0_pr-maxMa**2) * &
                                                              (d2u(1,iloc(1:nloc),2)*(pres_prev(iloc(1:nloc),1) - P0(faceint) ) + du(1,iloc(1:nloc),2)*d2u(1,iloc(1:nloc),1))
                         charI(1:nloc,1) = charI(1:nloc,1) + (1.0_pr-SIGN(1.0_pr,faceval* (pres_prev(iloc(1:nloc),2) - du(1,iloc(1:nloc),2)) ))/2.0_pr * pBcoef * &
                                                                   (1.0_pr-maxMa**2) * &
                                                              (d2u(1,iloc(1:nloc),2)*(pres_prev(iloc(1:nloc),1) - P0(faceint) ) + du(1,iloc(1:nloc),2)*d2u(1,iloc(1:nloc),1))
                      END IF
                      charX(1:nloc,1) = - ( 0.5_pr/du(1,iloc(1:nloc),2)**2 * (charI(1:nloc,1)+charI(1:nloc,3)+2.0_pr*charI(1:nloc,2)) - &
                                                     2.0_pr*d2u(1,iloc(1:nloc),2)/du(1,iloc(1:nloc),2)*charX_prev(1:nloc,1) )
                      charX(1:nloc,3) = -charI(1:nloc,4)
                      Drhs_diag_local((jj-1)*mynloc+iloc(1:nloc)) = Drhs_diag_local((jj-1)*mynloc+iloc(1:nloc)) - (charX_prev(1:nloc,1)+ulc_prev(iloc(1:nloc),jj)*charX(1:nloc,1)) / &
                                                     ulc_prev(iloc(1:nloc),nden) - ulc_prev(iloc(1:nloc),nden)*charX(1:nloc,3)
                   ELSE IF (jj==neng) THEN
                      d2u(2,iloc(1:nloc),1) = 0.0_pr !u_diag
                      d2u(2,iloc(1:nloc),2) = 0.0_pr  !gamma_diag
                      d2u(1,iloc(1:nloc),1) = du(2,iloc(1:nloc),2)-1.0_pr !p_diag
                      d2u(1,iloc(1:nloc),2) = 0.5_pr/du(1,iloc(1:nloc),2)*du(2,iloc(1:nloc),2)/ulc_prev(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),1) !c_diag
                      charI(1:nloc,3) = (SIGN(1.0_pr,faceval*(pres_prev(iloc(1:nloc),2) + du(1,iloc(1:nloc),2) ) )+1.0_pr)/2.0_pr * &
                                      ( (pres_prev(iloc(1:nloc),2)+du(1,iloc(1:nloc),2))*(du_diag_local(iloc(1:nloc),1)*d2u(1,iloc(1:nloc),1) + &
                                        du(2,iloc(1:nloc),1)*ulc_prev(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)) + &
                                        d2u(1,iloc(1:nloc),2)*(du(1,iloc(1:nloc),1)+ulc_prev(iloc(1:nloc),nden)*du(1,iloc(1:nloc),2)*du(2,iloc(1:nloc),1)) )
                      charI(1:nloc,1) = (SIGN(1.0_pr,faceval*(pres_prev(iloc(1:nloc),2) - du(1,iloc(1:nloc),2) ) )+1.0_pr)/2.0_pr * &
                                      ( (pres_prev(iloc(1:nloc),2)-du(1,iloc(1:nloc),2))*(du_diag_local(iloc(1:nloc),1)*d2u(1,iloc(1:nloc),1) - &
                                        du(2,iloc(1:nloc),1)*ulc_prev(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)) - &
                                        d2u(1,iloc(1:nloc),2)*(du(1,iloc(1:nloc),1)-ulc_prev(iloc(1:nloc),nden)*du(1,iloc(1:nloc),2)*du(2,iloc(1:nloc),1)) )
                      charI(1:nloc,2) = faceval*MAX(0.0_pr,faceval*pres_prev(iloc(1:nloc),2))*(2.0_pr*du(1,iloc(1:nloc),2)*d2u(1,iloc(1:nloc),2)*du(3,iloc(1:nloc),1) - &
                                                                   du_diag_local(iloc(1:nloc),1)*d2u(1,iloc(1:nloc),1))
                      IF (PLpinf) THEN
                         charI(1:nloc,3) = charI(1:nloc,3) + (1.0_pr-SIGN(1.0_pr,faceval* (pres_prev(iloc(1:nloc),2) + du(1,iloc(1:nloc),2)) ))/2.0_pr * pBcoef * &
                                                                   (1.0_pr-maxMa**2) * &
                                                              (d2u(1,iloc(1:nloc),2)*(pres_prev(iloc(1:nloc),1) - P0(faceint) ) + du(1,iloc(1:nloc),2)*d2u(1,iloc(1:nloc),1))
                         charI(1:nloc,1) = charI(1:nloc,1) + (1.0_pr-SIGN(1.0_pr,faceval* (pres_prev(iloc(1:nloc),2) - du(1,iloc(1:nloc),2)) ))/2.0_pr * pBcoef * &
                                                                   (1.0_pr-maxMa**2) * &
                                                              (d2u(1,iloc(1:nloc),2)*(pres_prev(iloc(1:nloc),1) - P0(faceint) ) + du(1,iloc(1:nloc),2)*d2u(1,iloc(1:nloc),1))
                      END IF
                      charX(1:nloc,1) = - ( 0.5_pr/du(1,iloc(1:nloc),2)**2 * (charI(1:nloc,1)+charI(1:nloc,3)+2.0_pr*charI(1:nloc,2)) - &
                                                     2.0_pr*d2u(1,iloc(1:nloc),2)/du(1,iloc(1:nloc),2)*charX_prev(1:nloc,1) )
                      charX(1:nloc,2) = - ( 0.5_pr/ulc_prev(iloc(1:nloc),nden)/du(1,iloc(1:nloc),2) * (charI(1:nloc,3)-charI(1:nloc,1) - &
                                                     d2u(1,iloc(1:nloc),2)/du(1,iloc(1:nloc),2)*(charI_prev(1:nloc,3)-charI_prev(1:nloc,1))) )
                      charX(1:nloc,3) = -0.5_pr*(charI(1:nloc,1)+charI(1:nloc,3))
                      Drhs_diag_local((jj-1)*mynloc+iloc(1:nloc)) = Drhs_diag_local((jj-1)*mynloc+iloc(1:nloc)) - 0.5_pr*SUM(ulc_prev(iloc(1:nloc),nvel(1):nvel(dim))**2,DIM=2) / &
                                        ulc_prev(iloc(1:nloc),nden)**2*charX(1:nloc,1) - ulc_prev(iloc(1:nloc),nvel(1))*charX(1:nloc,2) - &
                                        1.0_pr/(du(2,iloc(1:nloc),2)-1.0_pr)*charX(1:nloc,3)
                   ELSE IF (jj>=nspc_lil .AND. jj<=nspc_big) THEN
                      d2u(2,iloc(1:nloc),1) = 0.0_pr !u_diag
                      d2u(2,iloc(1:nloc),2) = (d2u_diag_local(iloc(1:nloc),1)/MW_in(jj-nspc(1)+1) - d2u_diag_local(iloc(1:nloc),2)*cp_in(jj-nspc(1)+1)) / &
                                                    ulc_prev(iloc(1:nloc),nden)/(d2u_diag_local(iloc(1:nloc),1)-d2u_diag_local(iloc(1:nloc),2))**2  !gamma_diag
                      d2u(1,iloc(1:nloc),1) = (ulc_prev(iloc(1:nloc),neng) - 0.5_pr*SUM(ulc_prev(iloc(1:nloc),nvel(1):nvel(dim))**2,DIM=2) / &
                                                    ulc_prev(iloc(1:nloc),nden))*d2u(2,iloc(1:nloc),2) !p_diag
                      d2u(1,iloc(1:nloc),2) = 0.5_pr/du(1,iloc(1:nloc),2)/ulc_prev(iloc(1:nloc),nden) * &
                                                    (d2u(2,iloc(1:nloc),2)*pres_prev(iloc(1:nloc),1)+du(2,iloc(1:nloc),2)*d2u(1,iloc(1:nloc),1)) !c_diag
                      charI(1:nloc,3) = (SIGN(1.0_pr,faceval*(pres_prev(iloc(1:nloc),2) + du(1,iloc(1:nloc),2) ) )+1.0_pr)/2.0_pr * &
                                      ( (pres_prev(iloc(1:nloc),2)+du(1,iloc(1:nloc),2))*(du_diag_local(iloc(1:nloc),1)*d2u(1,iloc(1:nloc),1) + &
                                        du(2,iloc(1:nloc),1)*ulc_prev(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)) + &
                                        d2u(1,iloc(1:nloc),2)*(du(1,iloc(1:nloc),1)+ulc_prev(iloc(1:nloc),nden)*du(1,iloc(1:nloc),2)*du(2,iloc(1:nloc),1)) )
                      charI(1:nloc,1) = (SIGN(1.0_pr,faceval*(pres_prev(iloc(1:nloc),2) - du(1,iloc(1:nloc),2) ) )+1.0_pr)/2.0_pr * &
                                      ( (pres_prev(iloc(1:nloc),2)-du(1,iloc(1:nloc),2))*(du_diag_local(iloc(1:nloc),1)*d2u(1,iloc(1:nloc),1) - &
                                        du(2,iloc(1:nloc),1)*ulc_prev(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)) - &
                                        d2u(1,iloc(1:nloc),2)*(du(1,iloc(1:nloc),1)-ulc_prev(iloc(1:nloc),nden)*du(1,iloc(1:nloc),2)*du(2,iloc(1:nloc),1)) )
                      charI(1:nloc,2) = faceval*MAX(0.0_pr,faceval*pres_prev(iloc(1:nloc),2))*(2.0_pr*du(1,iloc(1:nloc),2)*d2u(1,iloc(1:nloc),2)*du(3,iloc(1:nloc),1) - &
                                                                   du_diag_local(iloc(1:nloc),1)*d2u(1,iloc(1:nloc),1))
                      charI(1:nloc,4) = faceval*MAX(0.0_pr,faceval*pres_prev(iloc(1:nloc),2))*du_diag_local(iloc(1:nloc),1)/ulc_prev(iloc(1:nloc),nden)
                      IF (PLpinf) THEN
                         charI(1:nloc,3) = charI(1:nloc,3) + (1.0_pr-SIGN(1.0_pr,faceval* (pres_prev(iloc(1:nloc),2) + du(1,iloc(1:nloc),2)) ))/2.0_pr * pBcoef * &
                                                                   (1.0_pr-maxMa**2) * &
                                                              (d2u(1,iloc(1:nloc),2)*(pres_prev(iloc(1:nloc),1) - P0(faceint) ) + du(1,iloc(1:nloc),2)*d2u(1,iloc(1:nloc),1))
                         charI(1:nloc,1) = charI(1:nloc,1) + (1.0_pr-SIGN(1.0_pr,faceval* (pres_prev(iloc(1:nloc),2) - du(1,iloc(1:nloc),2)) ))/2.0_pr * pBcoef * &
                                                                   (1.0_pr-maxMa**2) * &
                                                              (d2u(1,iloc(1:nloc),2)*(pres_prev(iloc(1:nloc),1) - P0(faceint) ) + du(1,iloc(1:nloc),2)*d2u(1,iloc(1:nloc),1))
                      END IF
                      charX(1:nloc,1) = - ( 0.5_pr/du(1,iloc(1:nloc),2)**2 * (charI(1:nloc,1)+charI(1:nloc,3)+2.0_pr*charI(1:nloc,2)) - &
                                                     2.0_pr*d2u(1,iloc(1:nloc),2)/du(1,iloc(1:nloc),2)*charX_prev(1:nloc,1) )
                      charX(1:nloc,3) = -charI(1:nloc,4)
                      Drhs_diag_local((jj-1)*mynloc+iloc(1:nloc)) = Drhs_diag_local((jj-1)*mynloc+iloc(1:nloc)) - (charX_prev(1:nloc,1)+ulc_prev(iloc(1:nloc),jj)*charX(1:nloc,1)) / &
                                                     ulc_prev(iloc(1:nloc),nden) - ulc_prev(iloc(1:nloc),nden)*charX(1:nloc,3)
                   END IF 
                END DO
                IF (ALLOCATED(charI)) DEALLOCATE(charI)
                IF (ALLOCATED(charX)) DEALLOCATE(charX)
                IF (ALLOCATED(charI_prev)) DEALLOCATE(charI_prev)
                IF (ALLOCATED(charX_prev)) DEALLOCATE(charX_prev)
             END IF
          END IF
       END IF
    END DO
  END SUBROUTINE user_evol_LODIaddon_BC_Drhs_diag 

  SUBROUTINE user_evol_liu_BC_rhs (myrhs_loc, ulc, myne_loc, mynloc, myjl, mymeth)
    !--Sets rhs for boundary conditions
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: myne_loc, mynloc, myjl, mymeth
    REAL (pr), DIMENSION (mynloc*myne_loc), INTENT (INOUT) :: myrhs_loc
    REAL (pr), DIMENSION (mynloc,myne_loc), INTENT (IN) :: ulc

    INTEGER :: ie, i, ii, shift, jj
    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc

    REAL (pr), DIMENSION (:),     ALLOCATABLE :: temprhs_loc
    REAL (pr), DIMENSION (:,:),   ALLOCATABLE :: pres
    REAL (pr), DIMENSION (:,:,:), ALLOCATABLE :: du, d2u
    REAL (pr), DIMENSION (:,:),   ALLOCATABLE :: charI,charX
    REAL (pr) :: cp, R, curY, faceval
    INTEGER :: l, nstp, faceint

    IF (ALLOCATED(temprhs_loc)) DEALLOCATE(temprhs_loc)
    ALLOCATE(temprhs_loc(myne_loc*mynloc))
    CALL internal_rhs(temprhs_loc,ulc,dim,0)
    i_p_face(0) = 1
    DO i=1,dim
       i_p_face(i) = i_p_face(i-1)*3
    END DO
    DO face_type = 0, 3**dim - 1
       face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
       IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
          CALL get_all_indices_by_face (face_type, myjl, nloc, iloc)
          IF(nloc > 0 ) THEN
             IF (ABS(face(1)) == 1) THEN
                DO jj=1,myne_loc
                   myrhs_loc((jj-1)*mynloc+iloc(1:nloc)) = temprhs_loc((jj-1)*mynloc+iloc(1:nloc))  !from internal_rhs
                END DO
             END IF
          END IF
       END IF
    END DO
    IF (ALLOCATED(temprhs_loc)) DEALLOCATE(temprhs_loc) 

    IF (ALLOCATED(pres)) DEALLOCATE(pres) 
    IF (ALLOCATED(du)) DEALLOCATE(du) 
    IF (ALLOCATED(d2u)) DEALLOCATE(d2u) 
    ALLOCATE(pres(mynloc,locnvar),du(locnvar,mynloc,dim),d2u(locnvar,mynloc,dim))

    DO nstp=1,mynloc
       curY=1.0_pr
       IF (Nspec>1) curY=1.0_pr-SUM(ulc(nstp,nspc(1):nspc(Nspecm)))/ulc(nstp,nden) !Y_Nspec
       cp = curY*cp_in(Nspec)
       R = curY/MW_in(Nspec)
       DO l=1,Nspecm
          cp = cp + ulc(nstp,nspc(l))/ulc(nstp,nden)*cp_in(l)  !cp
          R  = R  + ulc(nstp,nspc(l))/ulc(nstp,nden)/MW_in(l)  !R
       END DO
       pres(nstp,1) = (ulc(nstp,neng)-0.5_pr*SUM(ulc(nstp,nvel(1):nvel(dim))**2)/ulc(nstp,nden))/(cp/R-1.0_pr)  !1: pressure
    END DO
    DO i=1,dim
       pres(:,1+i) = ulc(:,nvel(i))/ulc(:,nden)  !2--dim+1: u,v(,w)
    END DO
    DO i=1,Nspecm
       pres(:,dim+1+i) = ulc(:,nspc(i))/ulc(:,nden)  !dim+2--dim+Nspec: Y_l
    END DO 
    pres(:,locnvar) = ulc(:,nden) !dim+Nspec+1: rho

    IF ((methpd .ne. 0) .OR. splitFBpress) THEN 
       du(1,:,1) = pres(:,1)
       IF (Nspec>1) THEN
          CALL dpdx (du(1,:,1),ng,pres(:,dim+2:dim+Nspec),2*methpd+mymeth,myjl,splitFBpress)
       ELSE
          CALL dpdx (du(1,:,1),ng,pres(:,1),2*methpd+mymeth,myjl,.FALSE.)
       END IF
       CALL c_diff_fast(pres(:,2:locnvar), du(2:locnvar,:,:), d2u(2:locnvar,:,:), myjl, mynloc, mymeth, 10, locnvar-1, 1, locnvar-1)  
    ELSE
       CALL c_diff_fast(pres(:,1:locnvar), du(1:locnvar,:,:), d2u(1:locnvar,:,:), myjl, mynloc, mymeth, 10, locnvar, 1, locnvar) 
    END IF

    i_p_face(0) = 1
    DO i=1,dim
       i_p_face(i) = i_p_face(i-1)*3
    END DO
    DO face_type = 0, 3**dim - 1
       face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
       IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
          CALL get_all_indices_by_face (face_type, myjl, nloc, iloc)
          IF(nloc > 0 ) THEN
             IF (ABS(face(1)) == 1) THEN
                IF (ALLOCATED(charI)) DEALLOCATE(charI)
                IF (ALLOCATED(charX)) DEALLOCATE(charX)
                ALLOCATE(charI(nloc,locnvar),charX(nloc,locnvar))

                faceval=REAL(face(1),pr)
                faceint=(3-face(1))/2

                d2u(1,iloc(1:nloc),1) = 1.0_pr
                IF (Nspec>1) d2u(1,iloc(1:nloc),1) = 1.0_pr-SUM(ulc(iloc(1:nloc),nspc(1):nspc(Nspecm)),DIM=2)/ulc(iloc(1:nloc),nden) !Y_Nspec
                d2u(2,iloc(1:nloc),1) = d2u(1,iloc(1:nloc),1)*cp_in(Nspec)  !cp_Nspec
                d2u(2,iloc(1:nloc),2) = d2u(1,iloc(1:nloc),1)/MW_in(Nspec)  !R_Nspec
                DO l=1,Nspecm
                   d2u(2,iloc(1:nloc),1)  = d2u(2,iloc(1:nloc),1)  + ulc(iloc(1:nloc),nspc(l))/ulc(iloc(1:nloc),nden)*cp_in(l)  !cp
                   d2u(2,iloc(1:nloc),2)  = d2u(2,iloc(1:nloc),2)  + ulc(iloc(1:nloc),nspc(l))/ulc(iloc(1:nloc),nden)/MW_in(l)  !R
                END DO
                d2u(1,iloc(1:nloc),1) = d2u(2,iloc(1:nloc),1)/(d2u(2,iloc(1:nloc),1)-d2u(2,iloc(1:nloc),2))  !gamma=cp/(cp-R)
                d2u(1,iloc(1:nloc),2) = DSQRT(d2u(1,iloc(1:nloc),1)*pres(iloc(1:nloc),1)/ulc(iloc(1:nloc),nden))  !c=SQRT(gamma*P/rho)

                charI(1:nloc,2) = faceval*MAX(0.0_pr,faceval*pres(iloc(1:nloc),2)) * &
                                  (d2u(1,iloc(1:nloc),2)**2*du(locnvar,iloc(1:nloc),1) - du(1,iloc(1:nloc),1)) !u*(c^2*drho/dx - dp/dx)
                DO ii=3,dim+Nspec
                   charI(1:nloc,ii) = faceval*MAX(0.0_pr,faceval*pres(iloc(1:nloc),2)) * du(ii,iloc(1:nloc),1)
                END DO
                charI(1:nloc,locnvar) = faceval*MAX(0.0_pr,faceval*(pres(iloc(1:nloc),2) + d2u(1,iloc(1:nloc),2))) * &
                            (du(1,iloc(1:nloc),1) + ulc(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)*du(2,iloc(1:nloc),1))  !(u+c)*(dp/dx+rho*c*du/dx)
                charI(1:nloc,1)       = faceval*MAX(0.0_pr,faceval*(pres(iloc(1:nloc),2) - d2u(1,iloc(1:nloc),2))) * &
                            (du(1,iloc(1:nloc),1) - ulc(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)*du(2,iloc(1:nloc),1))  !(u-c)*(dp/dx-rho*c*du/dx)

                DO jj=2,dim
                   charI(1:nloc,2) = charI(1:nloc,2) + (SIGN(1.0_pr,faceval*pres(iloc(1:nloc),2))+1.0_pr)/2.0_pr * pres(iloc(1:nloc),jj+1) * &
                                     (d2u(1,iloc(1:nloc),2)**2*du(locnvar,iloc(1:nloc),jj) - du(1,iloc(1:nloc),jj)) !u_i*(c^2*drho/dx_i-dp/dx_i)
                   DO ii=3,dim+Nspec
                      charI(1:nloc,ii) = charI(1:nloc,ii) + (SIGN(1.0_pr,faceval*pres(iloc(1:nloc),2))+1.0_pr)/2.0_pr * pres(iloc(1:nloc),jj+1)*du(ii,iloc(1:nloc),jj)
                   END DO
                   charI(1:nloc,locnvar) = charI(1:nloc,locnvar) + (SIGN(1.0_pr,faceval*(pres(iloc(1:nloc),2) + d2u(1,iloc(1:nloc),2)))+1.0_pr)/2.0_pr * & 
                                   pres(iloc(1:nloc),jj+1) * (du(1,iloc(1:nloc),jj) + ulc(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)*du(2,iloc(1:nloc),jj))
                                   !u_i*(dp/dx_i+rho*c*du/dx_i) + c(dp/dx+rho*c*du/dx)
                   charI(1:nloc,1)       = charI(1:nloc,1)       + (SIGN(1.0_pr,faceval*(pres(iloc(1:nloc),2) - d2u(1,iloc(1:nloc),2)))+1.0_pr)/2.0_pr * & 
                                   pres(iloc(1:nloc),jj+1) * (du(1,iloc(1:nloc),jj) - ulc(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)*du(2,iloc(1:nloc),jj))
                END DO

                IF (pertchar) THEN   
                      charI(1:nloc,locnvar) = charI(1:nloc,locnvar) - faceval*MAX(0.0_pr,faceval*(pres(iloc(1:nloc),2) + d2u(1,iloc(1:nloc),2))) * dP0(faceint)
                      charI(1:nloc,1)       = charI(1:nloc,1)       - faceval*MAX(0.0_pr,faceval*(pres(iloc(1:nloc),2) - d2u(1,iloc(1:nloc),2))) * dP0(faceint)
                      charI(1:nloc,2)       = charI(1:nloc,2)       - faceval*MAX(0.0_pr,faceval*pres(iloc(1:nloc),2)) * (d2u(1,iloc(1:nloc),2)**2*drho0(faceint) - dP0(faceint))
                END IF

                IF (PLpinf) THEN
                   charI(1:nloc,locnvar) = charI(1:nloc,locnvar) + (1.0_pr-SIGN(1.0_pr,faceval* (pres(iloc(1:nloc),2) + d2u(1,iloc(1:nloc),2)) ))/2.0_pr * pBcoef * &
                                                                   (1.0_pr-maxMa**2) * &
                                                                   d2u(1,iloc(1:nloc),2) * (pres(iloc(1:nloc),1) - P0(faceint) )
                   charI(1:nloc,1)       = charI(1:nloc,1)       + (1.0_pr-SIGN(1.0_pr,faceval* (pres(iloc(1:nloc),2) - d2u(1,iloc(1:nloc),2)) ))/2.0_pr * pBcoef * &
                                                                   (1.0_pr-maxMa**2) * &
                                                                   d2u(1,iloc(1:nloc),2) * (pres(iloc(1:nloc),1) - P0(faceint) )
                END IF

                charX(1:nloc,1) = 0.5_pr/d2u(1,iloc(1:nloc),2)**2*(charI(1:nloc,1)+charI(1:nloc,locnvar)+2.0_pr*charI(1:nloc,2))
                charX(1:nloc,2) = 0.5_pr/d2u(1,iloc(1:nloc),2)/ulc(iloc(1:nloc),nden)*(charI(1:nloc,locnvar)-charI(1:nloc,1))
                DO ii=3,dim+Nspec
                   charX(1:nloc,ii) = charI(1:nloc,ii)
                END DO
                charX(1:nloc,locnvar) = 0.5_pr*(charI(1:nloc,locnvar)+charI(1:nloc,1))

                IF (pertchar) THEN   
                   IF( face(1) == 1  ) THEN                          ! Top
                      charX(1:nloc,1)       = charX(1:nloc,1)       + pres(iloc(1:nloc),2)*drho0(1)
                      charX(1:nloc,2)       = charX(1:nloc,2)       - rhoYg(1)/ulc(iloc(1:nloc),nden)
                      charX(1:nloc,locnvar) = charX(1:nloc,locnvar) - rhoYg(1)*pres(iloc(1:nloc),2)
                   ELSE IF( face(1) == -1  ) THEN                    ! Bottom
                      charX(1:nloc,1)       = charX(1:nloc,1)       + pres(iloc(1:nloc),2)*drho0(2)
                      charX(1:nloc,2)       = charX(1:nloc,2)       - rhoYg(2)/ulc(iloc(1:nloc),nden)
                      charX(1:nloc,locnvar) = charX(1:nloc,locnvar) - rhoYg(2)*pres(iloc(1:nloc),2)
                   END IF
                END IF

                charI(1:nloc,1)=du(3,iloc(1:nloc),2)
                IF (dim==3) charI(1:nloc,1) = charI(1:nloc,1) + du(4,iloc(1:nloc),3)  !dv/dy+dw/dz
                IF( face(1) == 1  ) THEN                          ! Top
                   charI(1:nloc,2) = pres(iloc(1:nloc),2)/d2u(1,iloc(1:nloc),2)  !beta-=u/c
                   charI(1:nloc,3) = 1.0_pr                                            !beta+=1
                ELSE IF( face(1) == -1  ) THEN                    ! Bottom
                   charI(1:nloc,3) = pres(iloc(1:nloc),2)/d2u(1,iloc(1:nloc),2)  !beta+=u/c
                   charI(1:nloc,2) = 1.0_pr                                            !beta-=1
                END IF

                myrhs_loc((nden-1)*mynloc+iloc(1:nloc)) = myrhs_loc((nden-1)*mynloc+iloc(1:nloc)) - charX(1:nloc,1) &
                                                          - (charI(1:nloc,2)+charI(1:nloc,3))/2.0_pr*ulc(iloc(1:nloc),nden)*charI(1:nloc,1)
                DO ii=nvel(1),nvel(dim)
                   myrhs_loc((ii-1)*mynloc+iloc(1:nloc)) = myrhs_loc((ii-1)*mynloc+iloc(1:nloc)) - pres(iloc(1:nloc),ii-nvel(1)+2)*charX(1:nloc,1) &
                                                                                                 - ulc(iloc(1:nloc),nden)*charX(1:nloc,ii-nvel(1)+2) &
                                                          - (charI(1:nloc,2)+charI(1:nloc,3))/2.0_pr*ulc(iloc(1:nloc),ii)*charI(1:nloc,1)
                END DO
                myrhs_loc((nvel(1)-1)*mynloc+iloc(1:nloc)) = myrhs_loc((nvel(1)-1)*mynloc+iloc(1:nloc)) - (charI(1:nloc,3)-charI(1:nloc,2))/2.0_pr * &
                                                                       ulc(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)*charI(1:nloc,1)
                DO ii=nvel(2),nvel(dim)
                   myrhs_loc((ii-1)*mynloc+iloc(1:nloc)) = myrhs_loc((ii-1)*mynloc+iloc(1:nloc)) - du(1,iloc(1:nloc),ii-nvel(2)+2)
                END DO
                myrhs_loc((neng-1)*mynloc+iloc(1:nloc)) = myrhs_loc((neng-1)*mynloc+iloc(1:nloc)) - 0.5_pr*SUM(pres(iloc(1:nloc),2:dim+1)**2,DIM=2)*charX(1:nloc,1) &
                             - SUM(ulc(iloc(1:nloc),nvel(1):nvel(dim))*charX(1:nloc,2:dim+1),DIM=2) - 1.0_pr/(d2u(1,iloc(1:nloc),1)-1.0_pr)*charX(1:nloc,locnvar) &
                             - SUM(pres(iloc(1:nloc),3:dim+1)*du(1,iloc(1:nloc),2:dim),DIM=2) &
                                                          - (charI(1:nloc,2)+charI(1:nloc,3))/2.0_pr*ulc(iloc(1:nloc),neng)*charI(1:nloc,1)
                DO ii=1,Nspecm
                   myrhs_loc((nspc(ii)-1)*mynloc+iloc(1:nloc)) = myrhs_loc((nspc(ii)-1)*mynloc+iloc(1:nloc)) - pres(iloc(1:nloc),ii+dim+1)*charX(1:nloc,1) &
                                                                                                 - ulc(iloc(1:nloc),nden)*charX(1:nloc,ii+dim+1) &
                                                          - ((charI(1:nloc,2)+charI(1:nloc,3))/2.0_pr*(ulc(iloc(1:nloc),nspc(ii))+pres(iloc(1:nloc),1)) + &
                                                             (charI(1:nloc,3)-charI(1:nloc,2))/2.0_pr*ulc(iloc(1:nloc),nvel(1))*d2u(1,iloc(1:nloc),2))*charI(1:nloc,1)
                END DO
                IF (ALLOCATED(charI)) DEALLOCATE(charI)
                IF (ALLOCATED(charX)) DEALLOCATE(charX) 
             END IF
          END IF
       END IF
    END DO
    IF (ALLOCATED(pres)) DEALLOCATE(pres) 
    IF (ALLOCATED(du)) DEALLOCATE(du) 
    IF (ALLOCATED(d2u)) DEALLOCATE(d2u) 
  END SUBROUTINE user_evol_liu_BC_rhs 

  SUBROUTINE user_evol_liu_BC_Drhs (Dmyrhs_loc, ulc, ulc_prev, myne_loc, mynloc, myjl, mymeth)
    !--Sets rhs for boundary conditions
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: myne_loc, mynloc, myjl, mymeth
    REAL (pr), DIMENSION (mynloc*myne_loc), INTENT (INOUT) :: Dmyrhs_loc
    REAL (pr), DIMENSION (mynloc,myne_loc), INTENT (IN) :: ulc, ulc_prev

    INTEGER :: ie, i, ii, shift, jj
    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc

    REAL (pr), DIMENSION (:),     ALLOCATABLE :: tempDrhs_loc
    REAL (pr), DIMENSION (:,:),   ALLOCATABLE :: pres, pres_prev
    REAL (pr), DIMENSION (:,:,:), ALLOCATABLE :: du, d2u
    REAL (pr), DIMENSION (:,:),   ALLOCATABLE :: charI, charX, charI_prev, charX_prev, hdiv
    
    REAL (pr) :: cp, R, curY, cp_prev, R_prev, curY_prev, faceval
    INTEGER :: l, nstp, faceint

    IF (ALLOCATED(tempDrhs_loc)) DEALLOCATE(tempDrhs_loc)
    ALLOCATE(tempDrhs_loc(myne_loc*mynloc))
    CALL internal_Drhs(tempDrhs_loc,ulc,ulc_prev,mymeth,dim,0)
    i_p_face(0) = 1
    DO i=1,dim
       i_p_face(i) = i_p_face(i-1)*3
    END DO
    DO face_type = 0, 3**dim - 1
       face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
       IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
          CALL get_all_indices_by_face (face_type, myjl, nloc, iloc)
          IF(nloc > 0 ) THEN
             IF (ABS(face(1)) == 1) THEN
                DO jj=1,myne_loc
                   Dmyrhs_loc((jj-1)*mynloc+iloc(1:nloc)) = tempDrhs_loc((jj-1)*mynloc+iloc(1:nloc))  !from internal_Drhs
                END DO
             END IF
          END IF
       END IF
    END DO
    IF (ALLOCATED(tempDrhs_loc)) DEALLOCATE(tempDrhs_loc) 

    IF (ALLOCATED(pres)) DEALLOCATE(pres) 
    IF (ALLOCATED(pres_prev)) DEALLOCATE(pres_prev) 
    IF (ALLOCATED(du)) DEALLOCATE(du) 
    IF (ALLOCATED(d2u)) DEALLOCATE(d2u) 
    ALLOCATE(pres(mynloc,locnvar),pres_prev(mynloc,locnvar),du(locnvar,mynloc,dim),d2u(locnvar,mynloc,dim))

    DO nstp=1,mynloc
       curY_prev=1.0_pr
       curY=0.0_pr
       IF (Nspec>1) curY_prev=1.0_pr-SUM(ulc_prev(nstp,nspc(1):nspc(Nspecm)))/ulc_prev(nstp,nden) !Y0_Nspec
       IF (Nspec>1) curY=(-SUM(ulc(nstp,nspc(1):nspc(Nspecm)))-curY_prev*ulc(nstp,nden))/ulc_prev(nstp,nden) !Y'_Nspec
       R  = curY/MW_in(Nspec)  !R'_Nspec
       cp = curY*cp_in(Nspec)  !cp'_Nspec
       cp_prev = curY_prev*cp_in(Nspec) 
       R_prev  = curY_prev/MW_in(Nspec)
       DO l=1,Nspecm
          cp = cp + (ulc(nstp,nspc(l))-ulc_prev(nstp,nspc(l))/ulc_prev(nstp,nden)*ulc(nstp,nden))/ulc_prev(nstp,nden)*cp_in(l)  !cp'
          R  = R  + (ulc(nstp,nspc(l))-ulc_prev(nstp,nspc(l))/ulc_prev(nstp,nden)*ulc(nstp,nden))/ulc_prev(nstp,nden)/MW_in(l)  !R'
          cp_prev = cp_prev + ulc_prev(nstp,nspc(l))/ulc_prev(nstp,nden)*cp_in(l)  !cp0
          R_prev  = R_prev  + ulc_prev(nstp,nspc(l))/ulc_prev(nstp,nden)/MW_in(l)  !R0
       END DO
       pres_prev(nstp,1) = (ulc_prev(nstp,neng)-0.5_pr*SUM(ulc_prev(nstp,nvel(1):nvel(dim))**2)/ulc_prev(nstp,nden))/(cp_prev/R_prev-1.0_pr)  !pressure
       pres(nstp,1) = (ulc_prev(nstp,neng)-0.5_pr*SUM(ulc_prev(nstp,nvel(1):nvel(dim))**2)/ulc_prev(nstp,nden))*(cp_prev*R-cp*R_prev)/(cp_prev-R_prev)**2 +  &
                (ulc(nstp,neng)+0.5_pr*SUM(ulc_prev(nstp,nvel(1):nvel(dim))**2)/ulc_prev(nstp,nden)**2*ulc(nstp,nden) -  &
                 SUM(ulc_prev(nstp,nvel(1):nvel(dim))*ulc(nstp,nvel(1):nvel(dim)))/ulc_prev(nstp,nden) ) /  &
                (cp_prev/R_prev-1.0_pr)
    END DO
    DO i=1,dim
       pres_prev(:,1+i) = ulc_prev(:,nvel(i))/ulc_prev(:,nden)  !u0
       pres(:,1+i) = (ulc(:,nvel(i)) - ulc_prev(:,nvel(i))*ulc(:,nden)/ulc_prev(:,nden) )/ulc_prev(:,nden) !u'
    END DO
    DO i=1,Nspecm
       pres_prev(:,dim+1+i) = ulc_prev(:,nspc(i))/ulc_prev(:,nden)  !Y0
       pres(:,dim+1+i) = (ulc(:,nspc(i)) - ulc_prev(:,nspc(i))*ulc(:,nden)/ulc_prev(:,nden) )/ulc_prev(:,nden) !Y'
    END DO 
    pres_prev(:,locnvar) = ulc_prev(:,nden) !rho0
    pres(:,locnvar) = ulc(:,nden) !rho'
 
    i_p_face(0) = 1
    DO i=1,dim
       i_p_face(i) = i_p_face(i-1)*3
    END DO
    DO face_type = 0, 3**dim - 1
       face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
       IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
          CALL get_all_indices_by_face (face_type, myjl, nloc, iloc)
          IF(nloc > 0 ) THEN
             IF (ABS(face(1)) == 1) THEN
                IF (ALLOCATED(charI)) DEALLOCATE(charI)
                IF (ALLOCATED(charX)) DEALLOCATE(charX)
                IF (ALLOCATED(charI_prev)) DEALLOCATE(charI_prev)
                IF (ALLOCATED(charX_prev)) DEALLOCATE(charX_prev)
                IF (ALLOCATED(hdiv)) DEALLOCATE(hdiv)
                ALLOCATE(charI(nloc,locnvar),charX(nloc,locnvar),charI_prev(nloc,locnvar),charX_prev(nloc,locnvar),hdiv(nloc,2))

                faceval=REAL(face(1),pr)
                faceint=(3-face(1))/2

                IF ((methpd .ne. 0) .OR. splitFBpress) THEN 
                   du(1,:,1) = pres(:,1)
                   IF (Nspec>1) THEN
                      CALL dpdx (du(1,:,1),ng,pres_prev(:,dim+2:dim+Nspec),2*methpd+mymeth,myjl,splitFBpress)
                   ELSE
                      CALL dpdx (du(1,:,1),ng,pres_prev(:,1),2*methpd+mymeth,myjl,.FALSE.)
                   END IF
                   CALL c_diff_fast(pres(:,2:locnvar), du(2:locnvar,:,:), d2u(2:locnvar,:,:), myjl, mynloc, mymeth, 10, locnvar-1, 1, locnvar-1)  
                ELSE
                   CALL c_diff_fast(pres(:,1:locnvar), du(1:locnvar,:,:), d2u(1:locnvar,:,:), myjl, mynloc, mymeth, 10, locnvar, 1, locnvar) 
                END IF

                DO ii=nvel(2),nvel(dim)
                   Dmyrhs_loc((ii-1)*mynloc+iloc(1:nloc)) = Dmyrhs_loc((ii-1)*mynloc+iloc(1:nloc)) - du(1,iloc(1:nloc),ii-nvel(2)+2)
                END DO
                Dmyrhs_loc((neng-1)*mynloc+iloc(1:nloc)) = Dmyrhs_loc((neng-1)*mynloc+iloc(1:nloc)) - SUM(ulc_prev(iloc(1:nloc),nvel(2):nvel(dim))*du(1,iloc(1:nloc),2:dim),DIM=2)
                
                hdiv(1:nloc,1) = du(3,iloc(1:nloc),2)
                IF (dim==3) hdiv(1:nloc,1) = hdiv(1:nloc,1) + du(4,iloc(1:nloc),3)

                d2u(1,iloc(1:nloc),1) = 1.0_pr
                IF (Nspec>1) d2u(1,iloc(1:nloc),1) = 1.0_pr-SUM(ulc_prev(iloc(1:nloc),nspc(1):nspc(Nspecm)),DIM=2)/ulc_prev(iloc(1:nloc),nden) !Y_Nspec 0
                d2u(2,iloc(1:nloc),1) = d2u(1,iloc(1:nloc),1)*cp_in(Nspec)  !cp_Nspec 0
                d2u(2,iloc(1:nloc),2) = d2u(1,iloc(1:nloc),1)/MW_in(Nspec)  !R_Nspec 0
                DO l=1,Nspecm
                   d2u(2,iloc(1:nloc),1)  = d2u(2,iloc(1:nloc),1)  + ulc_prev(iloc(1:nloc),nspc(l))/ulc_prev(iloc(1:nloc),nden)*cp_in(l)  !cp 0
                   d2u(2,iloc(1:nloc),2)  = d2u(2,iloc(1:nloc),2)  + ulc_prev(iloc(1:nloc),nspc(l))/ulc_prev(iloc(1:nloc),nden)/MW_in(l)  !R 0 
                END DO
                d2u(1,iloc(1:nloc),1) = d2u(2,iloc(1:nloc),1)/(d2u(2,iloc(1:nloc),1)-d2u(2,iloc(1:nloc),2))  !gamma0=cp0/(cp0-R0)
                d2u(1,iloc(1:nloc),2) = DSQRT(d2u(1,iloc(1:nloc),1)*pres_prev(iloc(1:nloc),1)/ulc_prev(iloc(1:nloc),nden))  !c0=SQRT(gamma0*P0/rho0) 

                charI(1:nloc,2) = faceval*MAX(0.0_pr,faceval*pres_prev(iloc(1:nloc),2)) * (d2u(1,iloc(1:nloc),2)**2*du(locnvar,iloc(1:nloc),1) - du(1,iloc(1:nloc),1))
                DO ii=3,dim+Nspec
                   charI(1:nloc,ii) = faceval*MAX(0.0_pr,faceval*pres_prev(iloc(1:nloc),2))*du(ii,iloc(1:nloc),1)
                END DO
                charI(1:nloc,locnvar) = faceval*MAX(0.0_pr,faceval*(pres_prev(iloc(1:nloc),2) + d2u(1,iloc(1:nloc),2))) * &
                                              (du(1,iloc(1:nloc),1) + ulc_prev(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)*du(2,iloc(1:nloc),1)) 
                charI(1:nloc,1)       = faceval*MAX(0.0_pr,faceval*(pres_prev(iloc(1:nloc),2) - d2u(1,iloc(1:nloc),2))) * &
                                              (du(1,iloc(1:nloc),1) - ulc_prev(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)*du(2,iloc(1:nloc),1)) 
                DO jj=2,dim
                   charI(1:nloc,2) = charI(1:nloc,2) + (SIGN(1.0_pr,faceval*pres_prev(iloc(1:nloc),2))+1.0_pr)/2.0_pr * &
                                     pres_prev(iloc(1:nloc),jj+1)*(d2u(1,iloc(1:nloc),2)**2*du(locnvar,iloc(1:nloc),jj) - du(1,iloc(1:nloc),jj)) !u_i*(c^2*drho/dx_i-dp/dx_i)
                   DO ii=3,dim+Nspec
                      charI(1:nloc,ii) = charI(1:nloc,ii) + (SIGN(1.0_pr,faceval*pres_prev(iloc(1:nloc),2))+1.0_pr)/2.0_pr * pres_prev(iloc(1:nloc),jj+1)*du(ii,iloc(1:nloc),jj)
                   END DO
                   charI(1:nloc,locnvar) = charI(1:nloc,locnvar) + (SIGN(1.0_pr,faceval*(pres_prev(iloc(1:nloc),2) + d2u(1,iloc(1:nloc),2)))+1.0_pr)/2.0_pr * & 
                                   pres_prev(iloc(1:nloc),jj+1) * (du(1,iloc(1:nloc),jj) + ulc(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)*du(2,iloc(1:nloc),jj))
                   charI(1:nloc,1)       = charI(1:nloc,1)       + (SIGN(1.0_pr,faceval*(pres_prev(iloc(1:nloc),2) - d2u(1,iloc(1:nloc),2)))+1.0_pr)/2.0_pr * &
                                   pres_prev(iloc(1:nloc),jj+1) * (du(1,iloc(1:nloc),jj) - ulc(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)*du(2,iloc(1:nloc),jj))
                END DO

                IF ((methpd .ne. 0) .OR. splitFBpress) THEN 
                   du(1,:,1) = pres_prev(:,1)
                   IF (Nspec>1) THEN
                      CALL dpdx (du(1,:,1),ng,pres_prev(:,dim+2:dim+Nspec),2*methpd+mymeth,myjl,splitFBpress)
                   ELSE
                      CALL dpdx (du(1,:,1),ng,pres_prev(:,1),2*methpd+mymeth,myjl,.FALSE.)
                   END IF
                   CALL c_diff_fast(pres_prev(:,2:locnvar), du(2:locnvar,:,:), d2u(2:locnvar,:,:), myjl, mynloc, mymeth, 10, locnvar-1, 1, locnvar-1)  
                ELSE
                   CALL c_diff_fast(pres_prev(:,1:locnvar), du(1:locnvar,:,:), d2u(1:locnvar,:,:), myjl, mynloc, mymeth, 10, locnvar  , 1, locnvar  ) 
                END IF

                Dmyrhs_loc((neng-1)*mynloc+iloc(1:nloc)) = Dmyrhs_loc((neng-1)*mynloc+iloc(1:nloc)) - SUM(ulc(iloc(1:nloc),nvel(2):nvel(dim))*du(1,iloc(1:nloc),2:dim),DIM=2)
                
                hdiv(1:nloc,2) = du(3,iloc(1:nloc),2)
                IF (dim==3) hdiv(1:nloc,2) = hdiv(1:nloc,2) + du(4,iloc(1:nloc),3)

                d2u(1,iloc(1:nloc),1) = 1.0_pr
                IF (Nspec>1) d2u(1,iloc(1:nloc),1) = 1.0_pr-SUM(ulc_prev(iloc(1:nloc),nspc(1):nspc(Nspecm)),DIM=2)/ulc_prev(iloc(1:nloc),nden) !Y_Nspec 0
                d2u(2,iloc(1:nloc),1) = d2u(1,iloc(1:nloc),1)*cp_in(Nspec)  !cp_Nspec 0
                d2u(2,iloc(1:nloc),2) = d2u(1,iloc(1:nloc),1)/MW_in(Nspec)  !R_Nspec 0
                DO l=1,Nspecm
                   d2u(2,iloc(1:nloc),1)  = d2u(2,iloc(1:nloc),1)  + ulc_prev(iloc(1:nloc),nspc(l))/ulc_prev(iloc(1:nloc),nden)*cp_in(l)  !cp 0
                   d2u(2,iloc(1:nloc),2)  = d2u(2,iloc(1:nloc),2)  + ulc_prev(iloc(1:nloc),nspc(l))/ulc_prev(iloc(1:nloc),nden)/MW_in(l)  !R 0 
                END DO
                d2u(1,iloc(1:nloc),1) = d2u(2,iloc(1:nloc),1)/(d2u(2,iloc(1:nloc),1)-d2u(2,iloc(1:nloc),2))  !gamma0=cp0/(cp0-R0)
                d2u(1,iloc(1:nloc),2) = DSQRT(d2u(1,iloc(1:nloc),1)*pres_prev(iloc(1:nloc),1)/ulc_prev(iloc(1:nloc),nden))  !c0=SQRT(gamma0*P0/rho0)
 
                d2u(4,iloc(1:nloc),2) = 0.0_pr
                IF (Nspec>1) d2u(4,iloc(1:nloc),2) = (-SUM(ulc(iloc(1:nloc),nspc(1):nspc(Nspecm)),DIM=2) - (ulc_prev(iloc(1:nloc),nden)-SUM(ulc_prev(iloc(1:nloc),nspc(1):nspc(Nspecm)),DIM=2)) & 
                                             /ulc_prev(iloc(1:nloc),nden)*ulc(iloc(1:nloc),nden) )/ulc_prev(iloc(1:nloc),nden)  !Y'_Nspec
                d2u(3,iloc(1:nloc),2) = d2u(4,iloc(1:nloc),2)*cp_in(Nspec) !cp'_Nspec
                d2u(4,iloc(1:nloc),2) = d2u(4,iloc(1:nloc),2)/MW_in(Nspec) !R'_Nspec
                DO l=1,Nspecm
                   d2u(3,iloc(1:nloc),2)  = d2u(3,iloc(1:nloc),2)  + (ulc(iloc(1:nloc),nspc(l)) - &
                                 ulc_prev(iloc(1:nloc),nspc(l))/ulc_prev(iloc(1:nloc),nden)*ulc(iloc(1:nloc),nden))/ulc_prev(iloc(1:nloc),nden)*cp_in(l)  !cp'
                   d2u(4,iloc(1:nloc),2)  = d2u(4,iloc(1:nloc),2)  + (ulc(iloc(1:nloc),nspc(l)) - &
                                 ulc_prev(iloc(1:nloc),nspc(l))/ulc_prev(iloc(1:nloc),nden)*ulc(iloc(1:nloc),nden))/ulc_prev(iloc(1:nloc),nden)/MW_in(l)  !R'
                END DO
                d2u(3,iloc(1:nloc),2) = (d2u(2,iloc(1:nloc),1)*d2u(4,iloc(1:nloc),2) - d2u(3,iloc(1:nloc),2)*d2u(2,iloc(1:nloc),2)) / &
                                             (d2u(2,iloc(1:nloc),1)-d2u(2,iloc(1:nloc),2))**2  !gamma'=(cp0*R'-cp'*R0)/(cp0-R0)^2
                d2u(4,iloc(1:nloc),2) = 0.5_pr/DSQRT(d2u(1,iloc(1:nloc),1)*pres_prev(iloc(1:nloc),1)/ulc_prev(iloc(1:nloc),nden)) * &
                        (d2u(1,iloc(1:nloc),1)*pres(iloc(1:nloc),1)+d2u(3,iloc(1:nloc),2)*pres_prev(iloc(1:nloc),1))/ulc_prev(iloc(1:nloc),nden) - &
                        d2u(1,iloc(1:nloc),1)*pres_prev(iloc(1:nloc),1)/ulc_prev(iloc(1:nloc),nden)**2*ulc(iloc(1:nloc),nden)  !c'=1/2/SQRT(gamma0*P0/rho0)*(gamma*P/rho)'

                charI_prev(1:nloc,2) = faceval*MAX(0.0_pr,faceval*pres_prev(iloc(1:nloc),2))*(d2u(1,iloc(1:nloc),2)**2*du(locnvar,iloc(1:nloc),1) - du(1,iloc(1:nloc),1)) 
                                              !u*(c^2*drho/dx - dp/dx)
                DO ii=3,dim+Nspec
                   charI_prev(1:nloc,ii) = faceval*MAX(0.0_pr,faceval*pres_prev(iloc(1:nloc),2))*du(ii,iloc(1:nloc),1)
                END DO
                charI_prev(1:nloc,locnvar) = faceval*MAX(0.0_pr,faceval*(pres_prev(iloc(1:nloc),2) + d2u(1,iloc(1:nloc),2))) * &
                           (du(1,iloc(1:nloc),1) + ulc_prev(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)*du(2,iloc(1:nloc),1))  !(u+c)*(dp/dx+rho*c*du/dx)
                charI_prev(1:nloc,1)       = faceval*MAX(0.0_pr,faceval*(pres_prev(iloc(1:nloc),2) - d2u(1,iloc(1:nloc),2))) * &
                            (du(1,iloc(1:nloc),1) - ulc_prev(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)*du(2,iloc(1:nloc),1))  !(u-c)*(dp/dx-rho*c*du/dx)

                DO jj=2,dim
                   charI_prev(1:nloc,2) = charI_prev(1:nloc,2) + (SIGN(1.0_pr,faceval*pres_prev(iloc(1:nloc),2))+1.0_pr)/2.0_pr * &
                                     pres_prev(iloc(1:nloc),jj+1)*(d2u(1,iloc(1:nloc),2)**2*du(locnvar,iloc(1:nloc),jj) - du(1,iloc(1:nloc),jj)) !u_i*(c^2*drho/dx_i-dp/dx_i)
                   DO ii=3,dim+Nspec
                      charI_prev(1:nloc,ii) = charI_prev(1:nloc,ii) + (SIGN(1.0_pr,faceval*pres_prev(iloc(1:nloc),2))+1.0_pr)/2.0_pr * pres_prev(iloc(1:nloc),jj+1)*du(ii,iloc(1:nloc),jj)
                   END DO
                   charI_prev(1:nloc,locnvar) = charI_prev(1:nloc,locnvar) + (SIGN(1.0_pr,faceval*(pres_prev(iloc(1:nloc),2) + d2u(1,iloc(1:nloc),2)))+1.0_pr)/2.0_pr * & 
                                   pres_prev(iloc(1:nloc),jj+1) * (du(1,iloc(1:nloc),jj) + ulc(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)*du(2,iloc(1:nloc),jj))
                   charI_prev(1:nloc,1)       = charI_prev(1:nloc,1)       + (SIGN(1.0_pr,faceval*(pres_prev(iloc(1:nloc),2) - d2u(1,iloc(1:nloc),2)))+1.0_pr)/2.0_pr * & 
                                   pres_prev(iloc(1:nloc),jj+1) * (du(1,iloc(1:nloc),jj) - ulc(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)*du(2,iloc(1:nloc),jj))
                END DO

                IF (pertchar) THEN   
                   charI_prev(1:nloc,locnvar) = charI_prev(1:nloc,locnvar) + faceval*MAX(0.0_pr,faceval*(pres_prev(iloc(1:nloc),2) + d2u(1,iloc(1:nloc),2))) * rhoYg(faceint)
                   charI_prev(1:nloc,1)       = charI_prev(1:nloc,1)       + faceval*MAX(0.0_pr,faceval*(pres_prev(iloc(1:nloc),2) - d2u(1,iloc(1:nloc),2))) * rhoYg(faceint)
                   charI_prev(1:nloc,2)       = charI_prev(1:nloc,2)       - faceval*MAX(0.0_pr,faceval*pres_prev(iloc(1:nloc),2))*(d2u(1,iloc(1:nloc),2)**2*drho0(faceint) + rhoYg(faceint))
                END IF

                charI(1:nloc,2) = charI(1:nloc,2) + (SIGN(1.0_pr,faceval*pres_prev(iloc(1:nloc),2))+1.0_pr)/2.0_pr * &
                                            pres(iloc(1:nloc),2)*(d2u(1,iloc(1:nloc),2)**2*du(locnvar,iloc(1:nloc),1) - du(1,iloc(1:nloc),1)) !u*(c^2*drho/dx - dp/dx)
                DO ii=3,dim+Nspec
                   charI(1:nloc,ii) = charI(1:nloc,ii) + (SIGN(1.0_pr,faceval*pres_prev(iloc(1:nloc),2))+1.0_pr)/2.0_pr * pres(iloc(1:nloc),2)*du(ii,iloc(1:nloc),1)
                END DO
                charI(1:nloc,locnvar) = charI(1:nloc,locnvar) + (SIGN(1.0_pr,faceval*(pres_prev(iloc(1:nloc),2) + d2u(1,iloc(1:nloc),2)))+1.0_pr)/2.0_pr * & 
                           (pres(iloc(1:nloc),2) + d2u(4,iloc(1:nloc),2)) * &
                           (du(1,iloc(1:nloc),1) + ulc_prev(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)*du(2,iloc(1:nloc),1))  !(u+c)*(dp/dx+rho*c*du/dx)
                charI(1:nloc,1) = charI(1:nloc,1) + (SIGN(1.0_pr,faceval*(pres_prev(iloc(1:nloc),2) - d2u(1,iloc(1:nloc),2)))+1.0_pr)/2.0_pr * & 
                           (pres(iloc(1:nloc),2) - d2u(4,iloc(1:nloc),2)) * &
                           (du(1,iloc(1:nloc),1) - ulc_prev(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)*du(2,iloc(1:nloc),1))  !(u-c)*(dp/dx-rho*c*du/dx)

                DO jj=2,dim
                   charI(1:nloc,2) = charI(1:nloc,2) + (SIGN(1.0_pr,faceval*pres_prev(iloc(1:nloc),2))+1.0_pr)/2.0_pr * &
                                     pres(iloc(1:nloc),jj+1)*(d2u(1,iloc(1:nloc),2)**2*du(locnvar,iloc(1:nloc),jj) - du(1,iloc(1:nloc),jj)) !u_i*(c^2*drho/dx_i-dp/dx_i)
                   DO ii=3,dim+Nspec
                      charI(1:nloc,ii) = charI(1:nloc,ii) + (SIGN(1.0_pr,faceval*pres_prev(iloc(1:nloc),2))+1.0_pr)/2.0_pr * pres(iloc(1:nloc),jj+1)*du(ii,iloc(1:nloc),jj)
                   END DO
                   charI(1:nloc,locnvar) = charI(1:nloc,locnvar) + (SIGN(1.0_pr,faceval*(pres_prev(iloc(1:nloc),2) + d2u(1,iloc(1:nloc),2)))+1.0_pr)/2.0_pr * & 
                                   pres(iloc(1:nloc),jj+1) * (du(1,iloc(1:nloc),jj) + ulc_prev(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)*du(2,iloc(1:nloc),jj))
                   charI(1:nloc,1)       = charI(1:nloc,1)       + (SIGN(1.0_pr,faceval*(pres_prev(iloc(1:nloc),2) - d2u(1,iloc(1:nloc),2)))+1.0_pr)/2.0_pr * & 
                                   pres(iloc(1:nloc),jj+1) * (du(1,iloc(1:nloc),jj) - ulc_prev(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)*du(2,iloc(1:nloc),jj))
                END DO

                charI(1:nloc,2) = charI(1:nloc,2) + faceval*MAX(0.0_pr,faceval*pres_prev(iloc(1:nloc),2)) * &
                                                    2.0_pr*d2u(1,iloc(1:nloc),2)*d2u(4,iloc(1:nloc),2)*du(locnvar,iloc(1:nloc),1)
                charI(1:nloc,locnvar) = charI(1:nloc,locnvar) + faceval*MAX(0.0_pr,faceval*(pres_prev(iloc(1:nloc),2) + d2u(1,iloc(1:nloc),2)))*du(2,iloc(1:nloc),1) * &
                           (ulc(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)+ulc_prev(iloc(1:nloc),nden)*d2u(4,iloc(1:nloc),2)) 
                charI(1:nloc,1) = charI(1:nloc,1) - faceval*MAX(0.0_pr,faceval*(pres_prev(iloc(1:nloc),2) - d2u(1,iloc(1:nloc),2)))*du(2,iloc(1:nloc),1) * &
                           (ulc(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)+ulc_prev(iloc(1:nloc),nden)*d2u(4,iloc(1:nloc),2)) 

                DO jj=2,dim
                   charI(1:nloc,2) = charI(1:nloc,2) + (SIGN(1.0_pr,faceval*pres_prev(iloc(1:nloc),2))+1.0_pr)/2.0_pr * &
                                     pres_prev(iloc(1:nloc),jj+1)*2.0_pr*d2u(1,iloc(1:nloc),2)*d2u(4,iloc(1:nloc),2)*du(locnvar,iloc(1:nloc),jj)  !u_i*(c^2*drho/dx_i-dp/dx_i)
                   charI(1:nloc,locnvar) = charI(1:nloc,locnvar) + (SIGN(1.0_pr,faceval*(pres_prev(iloc(1:nloc),2) + d2u(1,iloc(1:nloc),2)))+1.0_pr)/2.0_pr * & 
                           pres_prev(iloc(1:nloc),jj+1) * du(2,iloc(1:nloc),jj) * &
                           (ulc(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)+ulc_prev(iloc(1:nloc),nden)*d2u(4,iloc(1:nloc),2))
                   charI(1:nloc,1)       = charI(1:nloc,1)       - (SIGN(1.0_pr,faceval*(pres_prev(iloc(1:nloc),2) - d2u(1,iloc(1:nloc),2)))+1.0_pr)/2.0_pr * & 
                           pres_prev(iloc(1:nloc),jj+1) * du(2,iloc(1:nloc),jj) * &
                           (ulc(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)+ulc_prev(iloc(1:nloc),nden)*d2u(4,iloc(1:nloc),2))
                END DO

                IF (pertchar) THEN   
                   charI(1:nloc,locnvar) = charI(1:nloc,locnvar) + (SIGN(1.0_pr,faceval*(pres_prev(iloc(1:nloc),2) + d2u(1,iloc(1:nloc),2)))+1.0_pr)/2.0_pr * & 
                                                                   (pres(iloc(1:nloc),2) + du(2,iloc(1:nloc),2)) * rhoYg(faceint)
                   charI(1:nloc,1)       = charI(1:nloc,1)       + (SIGN(1.0_pr,faceval*(pres_prev(iloc(1:nloc),2) - d2u(1,iloc(1:nloc),2)))+1.0_pr)/2.0_pr * & 
                                                                   (pres(iloc(1:nloc),2) - du(2,iloc(1:nloc),2)) * rhoYg(faceint)
                   charI(1:nloc,2)       = charI(1:nloc,2) + (SIGN(1.0_pr,faceval*pres_prev(iloc(1:nloc),2))+1.0_pr)/2.0_pr * &
                                                          ( -pres(iloc(1:nloc),2)*(d2u(1,iloc(1:nloc),2)**2*drho0(faceint) + rhoYg(faceint)) &
                                                            -pres_prev(iloc(1:nloc),2)*2.0_pr*d2u(1,iloc(1:nloc),2)*d2u(4,iloc(1:nloc),2)*drho0(faceint) )
                END IF

                IF (PLpinf) THEN
                   charI_prev(1:nloc,locnvar) = charI_prev(1:nloc,locnvar) + (1.0_pr-SIGN(1.0_pr,faceval* (pres_prev(iloc(1:nloc),2) + d2u(1,iloc(1:nloc),2)) ))/2.0_pr * pBcoef * &
                                                                   (1.0_pr-maxMa**2) * &
                                                                   d2u(1,iloc(1:nloc),2) * (pres_prev(iloc(1:nloc),1) - P0(faceint) )
                   charI_prev(1:nloc,1)       = charI_prev(1:nloc,1)       + (1.0_pr-SIGN(1.0_pr,faceval* (pres_prev(iloc(1:nloc),2) - d2u(1,iloc(1:nloc),2)) ))/2.0_pr * pBcoef * &
                                                                   (1.0_pr-maxMa**2) * &
                                                                   d2u(1,iloc(1:nloc),2) * (pres_prev(iloc(1:nloc),1) - P0(faceint) )

                   charI(1:nloc,locnvar) = charI(1:nloc,locnvar) + (1.0_pr-SIGN(1.0_pr,faceval* (pres_prev(iloc(1:nloc),2) + d2u(1,iloc(1:nloc),2)) ))/2.0_pr * pBcoef * &
                                                                   (1.0_pr-maxMa**2) * &
                                                                   (du(2,iloc(1:nloc),2) * (pres_prev(iloc(1:nloc),1) - P0(faceint) ) + d2u(1,iloc(1:nloc),2)*pres(iloc(1:nloc),1))
                   charI(1:nloc,1)       = charI(1:nloc,1)       + (1.0_pr-SIGN(1.0_pr,faceval* (pres_prev(iloc(1:nloc),2) - d2u(1,iloc(1:nloc),2)) ))/2.0_pr * pBcoef * &
                                                                   (1.0_pr-maxMa**2) * &
                                                                   (du(2,iloc(1:nloc),2) * (pres_prev(iloc(1:nloc),1) - P0(faceint) ) + d2u(1,iloc(1:nloc),2)*pres(iloc(1:nloc),1))
                END IF

                charX_prev(1:nloc,1) = 0.5_pr/d2u(1,iloc(1:nloc),2)**2*(charI_prev(1:nloc,1)+charI_prev(1:nloc,locnvar)+2.0_pr*charI_prev(1:nloc,2))
                charX_prev(1:nloc,2) = 0.5_pr/d2u(1,iloc(1:nloc),2)/ulc_prev(iloc(1:nloc),nden)*(charI_prev(1:nloc,locnvar)-charI_prev(1:nloc,1))
                DO ii=3,dim+Nspec
                   charX_prev(1:nloc,ii) = charI_prev(1:nloc,ii)
                END DO
                charX_prev(1:nloc,locnvar) = 0.5_pr*(charI_prev(1:nloc,locnvar)+charI_prev(1:nloc,1))

                IF (pertchar) THEN   
                   charX_prev(1:nloc,1)       = charX_prev(1:nloc,1)       + pres_prev(iloc(1:nloc),2)*drho0(faceint)
                   charX_prev(1:nloc,2)       = charX_prev(1:nloc,2)       - rhoYg(faceint)/ulc_prev(iloc(1:nloc),nden)
                   charX_prev(1:nloc,locnvar) = charX_prev(1:nloc,locnvar) - rhoYg(faceint)*pres_prev(iloc(1:nloc),2)
                END IF

                charX(1:nloc,1) = 0.5_pr/d2u(1,iloc(1:nloc),2)**2*(charI(1:nloc,1)+charI(1:nloc,locnvar)+2.0_pr*charI(1:nloc,2)) &
                                  - charX_prev(1:nloc,1)*2.0_pr*d2u(4,iloc(1:nloc),2)/d2u(1,iloc(1:nloc),2)
                charX(1:nloc,2) = 0.5_pr/d2u(1,iloc(1:nloc),2)/ulc_prev(iloc(1:nloc),nden) * &
                       (charI(1:nloc,locnvar) - charI(1:nloc,1) - (ulc(iloc(1:nloc),nden)/ulc_prev(iloc(1:nloc),nden) + d2u(4,iloc(1:nloc),2)/d2u(1,iloc(1:nloc),2)) * &
                       (charI_prev(1:nloc,locnvar)-charI_prev(1:nloc,1)))
                DO ii=3,dim+Nspec
                   charX(1:nloc,ii) = charI(1:nloc,ii)
                END DO
                charX(1:nloc,locnvar) = 0.5_pr*(charI(1:nloc,locnvar)+charI(1:nloc,1))

                IF (pertchar) THEN   
                   charX(1:nloc,1)       = charX(1:nloc,1)       + pres(iloc(1:nloc),2)*drho0(faceint)
                   charX(1:nloc,2)       = charX(1:nloc,2)       - rhoYg(faceint)/ulc_prev(iloc(1:nloc),nden)**2*ulc(iloc(1:nloc),nden)
                   charX(1:nloc,locnvar) = charX(1:nloc,locnvar) - rhoYg(faceint)*pres(iloc(1:nloc),2)
                END IF

                IF( face(1) == 1  ) THEN                          ! Top
                   charI_prev(1:nloc,2) = pres_prev(iloc(1:nloc),2)/d2u(1,iloc(1:nloc),2)  !beta-=u/c
                   charI_prev(1:nloc,3) = 1.0_pr                                            !beta+=1
                   charI(1:nloc,2) = (pres(iloc(1:nloc),2) - pres_prev(iloc(1:nloc),2)/d2u(1,iloc(1:nloc),2)*d2u(4,iloc(1:nloc),2))/d2u(1,iloc(1:nloc),2)  !beta-=u/c
                   charI(1:nloc,3) = 0.0_pr                                            !beta+=1
                ELSE IF( face(1) == -1  ) THEN                    ! Bottom
                   charI_prev(1:nloc,3) = pres_prev(iloc(1:nloc),2)/d2u(1,iloc(1:nloc),2)  !beta+=u/c
                   charI_prev(1:nloc,2) = 1.0_pr                                            !beta-=1
                   charI(1:nloc,3) = (pres(iloc(1:nloc),2) - pres_prev(iloc(1:nloc),2)/d2u(1,iloc(1:nloc),2)*d2u(4,iloc(1:nloc),2))/d2u(1,iloc(1:nloc),2)  !beta+=u/c
                   charI(1:nloc,2) = 0.0_pr                                            !beta-=1
                END IF
                charI(1:nloc,1) = (charI(1:nloc,2)+charI(1:nloc,3))/2.0_pr*hdiv(1:nloc,2) + (charI_prev(1:nloc,2)+charI_prev(1:nloc,3))/2.0_pr*hdiv(1:nloc,1)
                charI_prev(1:nloc,1) = (charI_prev(1:nloc,2)+charI_prev(1:nloc,3))/2.0_pr*hdiv(1:nloc,2)
                charI(1:nloc,4) = (charI(1:nloc,3)-charI(1:nloc,2))/2.0_pr*hdiv(1:nloc,2) + (charI_prev(1:nloc,3)-charI_prev(1:nloc,2))/2.0_pr*hdiv(1:nloc,1)
                charI_prev(1:nloc,4) = (charI_prev(1:nloc,3)-charI_prev(1:nloc,2))/2.0_pr*hdiv(1:nloc,2)
 
                Dmyrhs_loc((nden-1)*mynloc+iloc(1:nloc)) = Dmyrhs_loc((nden-1)*mynloc+iloc(1:nloc)) - charX(1:nloc,1) &
                                - charI_prev(1:nloc,1)*ulc(iloc(1:nloc),nden) - charI(1:nloc,1)*ulc_prev(iloc(1:nloc),nden)
                DO ii=nvel(1),nvel(dim)
                   Dmyrhs_loc((ii-1)*mynloc+iloc(1:nloc)) = Dmyrhs_loc((ii-1)*mynloc+iloc(1:nloc))  & 
                                                            - pres_prev(iloc(1:nloc),ii-nvel(1)+2)*charX(1:nloc,1) - pres(iloc(1:nloc),ii-nvel(1)+2)*charX_prev(1:nloc,1) &
                                                            - ulc_prev(iloc(1:nloc),nden)*charX(1:nloc,ii-nvel(1)+2) - ulc(iloc(1:nloc),nden)*charX_prev(1:nloc,ii-nvel(1)+2) &
                                - charI_prev(1:nloc,1)*ulc(iloc(1:nloc),ii) - charI(1:nloc,1)*ulc_prev(iloc(1:nloc),ii)
                END DO
                Dmyrhs_loc((nvel(1)-1)*mynloc+iloc(1:nloc)) = Dmyrhs_loc((nvel(1)-1)*mynloc+iloc(1:nloc)) & 
                            - charI_prev(1:nloc,4)*(ulc(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)+ulc_prev(iloc(1:nloc),nden)*d2u(4,iloc(1:nloc),2)) &
                            - charI(1:nloc,4)*(ulc_prev(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2))
                Dmyrhs_loc((neng-1)*mynloc+iloc(1:nloc)) = Dmyrhs_loc((neng-1)*mynloc+iloc(1:nloc)) &
                      - 0.5_pr*SUM(pres_prev(iloc(1:nloc),2:dim+1)**2,DIM=2)*charX(1:nloc,1) - SUM(pres_prev(iloc(1:nloc),2:dim+1)*pres_prev(iloc(1:nloc),2:dim+1),DIM=2)*charX_prev(1:nloc,1) &
                      - SUM(ulc_prev(iloc(1:nloc),nvel(1):nvel(dim))*charX(1:nloc,2:dim+1),DIM=2) - SUM(ulc(iloc(1:nloc),nvel(1):nvel(dim))*charX_prev(1:nloc,2:dim+1),DIM=2) &
                      - 1.0_pr/(d2u(1,iloc(1:nloc),1)-1.0_pr)*(charX(1:nloc,locnvar) - d2u(3,iloc(1:nloc),2)/(d2u(1,iloc(1:nloc),1)-1.0_pr)*charX_prev(1:nloc,locnvar)) &
                                - charI_prev(1:nloc,1)*(ulc(iloc(1:nloc),neng)+pres(iloc(1:nloc),1)) &
                                - charI(1:nloc,1)*(ulc_prev(iloc(1:nloc),neng)+pres_prev(iloc(1:nloc),1)) & 
                            - charI_prev(1:nloc,4)*(ulc(iloc(1:nloc),nvel(1))*d2u(1,iloc(1:nloc),2)+ulc_prev(iloc(1:nloc),nvel(1))*d2u(4,iloc(1:nloc),2)) &
                            - charI(1:nloc,4)*(ulc_prev(iloc(1:nloc),nvel(1))*d2u(1,iloc(1:nloc),2))
                DO ii=1,Nspecm
                   Dmyrhs_loc((nspc(ii)-1)*mynloc+iloc(1:nloc)) = Dmyrhs_loc((nspc(ii)-1)*mynloc+iloc(1:nloc))  & 
                                                            - pres_prev(iloc(1:nloc),ii+dim+1)*charX(1:nloc,1) - pres(iloc(1:nloc),ii+dim+1)*charX_prev(1:nloc,1) &
                                                            - ulc_prev(iloc(1:nloc),nden)*charX(1:nloc,ii+dim+1) - ulc(iloc(1:nloc),nden)*charX_prev(1:nloc,ii+dim+1) &
                                - charI_prev(1:nloc,1)*ulc(iloc(1:nloc),nspc(ii)) - charI(1:nloc,1)*ulc_prev(iloc(1:nloc),nspc(ii))
                END DO

                IF (ALLOCATED(charI)) DEALLOCATE(charI)
                IF (ALLOCATED(charX)) DEALLOCATE(charX)
                IF (ALLOCATED(charI_prev)) DEALLOCATE(charI_prev)
                IF (ALLOCATED(charX_prev)) DEALLOCATE(charX_prev)
                IF (ALLOCATED(hdiv)) DEALLOCATE(hdiv)
             END IF
          END IF
       END IF
    END DO

    IF (ALLOCATED(pres)) DEALLOCATE(pres) 
    IF (ALLOCATED(pres_prev)) DEALLOCATE(pres_prev) 
    IF (ALLOCATED(du)) DEALLOCATE(du) 
    IF (ALLOCATED(d2u)) DEALLOCATE(d2u) 
  END SUBROUTINE user_evol_liu_BC_Drhs 

  SUBROUTINE user_evol_liu_BC_Drhs_diag (Drhs_diag_local, ulc_prev, myne_loc, mynloc, myjl, mymeth)
    !--Sets rhs for boundary conditions
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: myne_loc, mynloc, myjl, mymeth
    REAL (pr), DIMENSION (mynloc*myne_loc), INTENT (INOUT) :: Drhs_diag_local
    REAL (pr), DIMENSION (mynloc,myne_loc), INTENT (IN) :: ulc_prev

    INTEGER :: ie, i, ii, shift, jj
    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc

    REAL (pr), DIMENSION (:),     ALLOCATABLE :: tempdiag_loc
    REAL (pr), DIMENSION (:,:),   ALLOCATABLE :: charI, charX, charI_prev, charX_prev, hdiv
    REAL (pr), DIMENSION (mynloc,dim) :: du_diag_local, d2u_diag_local
    REAL (pr), DIMENSION (:,:),   ALLOCATABLE :: pres_prev
    REAL (pr), DIMENSION (:,:,:), ALLOCATABLE :: du, d2u
    REAL (pr) :: cp_prev, R_prev, curY_prev, faceval
    INTEGER :: l, nstp, faceint

    IF (ALLOCATED(tempdiag_loc)) DEALLOCATE(tempdiag_loc)
    ALLOCATE(tempdiag_loc(myne_loc*mynloc))
    CALL internal_Drhs_diag(tempdiag_loc,ulc_prev,mymeth,dim,0)
    i_p_face(0) = 1
    DO i=1,dim
       i_p_face(i) = i_p_face(i-1)*3
    END DO
    DO face_type = 0, 3**dim - 1
       face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
       IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
          CALL get_all_indices_by_face (face_type, myjl, nloc, iloc)
          IF(nloc > 0 ) THEN
             IF (ABS(face(1)) == 1) THEN
                DO jj=1,myne_loc
                   Drhs_diag_local((jj-1)*mynloc+iloc(1:nloc)) = tempdiag_loc((jj-1)*mynloc+iloc(1:nloc))  !from internal_Drhs_diag
                END DO
             END IF
          END IF
       END IF
    END DO
    IF (ALLOCATED(tempdiag_loc)) DEALLOCATE(tempdiag_loc) 

    IF (ALLOCATED(pres_prev)) DEALLOCATE(pres_prev) 
    IF (ALLOCATED(du)) DEALLOCATE(du) 
    IF (ALLOCATED(d2u)) DEALLOCATE(d2u) 
    ALLOCATE(pres_prev(mynloc,locnvar),du(locnvar,mynloc,dim),d2u(locnvar,mynloc,dim))
    CALL c_diff_diag(du_diag_local, d2u_diag_local, j_lev, mynloc, mymeth, mymeth, 10)

    DO nstp=1,mynloc
       curY_prev=1.0_pr
       IF (Nspec>1) curY_prev=1.0_pr-SUM(ulc_prev(nstp,nspc(1):nspc(Nspecm)))/ulc_prev(nstp,nden) !Y0_Nspec
       cp_prev = curY_prev*cp_in(Nspec) 
       R_prev  = curY_prev/MW_in(Nspec)
       DO l=1,Nspecm
          cp_prev = cp_prev + ulc_prev(nstp,nspc(l))/ulc_prev(nstp,nden)*cp_in(l)  !cp0
          R_prev  = R_prev  + ulc_prev(nstp,nspc(l))/ulc_prev(nstp,nden)/MW_in(l)  !R0
       END DO
       pres_prev(nstp,1) = (ulc_prev(nstp,neng)-0.5_pr*SUM(ulc_prev(nstp,nvel(1):nvel(dim))**2)/ulc_prev(nstp,nden))/(cp_prev/R_prev-1.0_pr)  !pressure
    END DO
    DO i=1,dim
       pres_prev(:,1+i) = ulc_prev(:,nvel(i))/ulc_prev(:,nden)  !u0
    END DO
    DO i=1,Nspecm
       pres_prev(:,dim+1+i) = ulc_prev(:,nspc(i))/ulc_prev(:,nden)  !Y0
    END DO 
    pres_prev(:,locnvar) = ulc_prev(:,nden) !rho0
 
    CALL c_diff_fast(pres_prev, du(:,:,:), d2u(:,:,:), myjl, mynloc, mymeth, 10, locnvar, 1, locnvar) 

    i_p_face(0) = 1
    DO i=1,dim
       i_p_face(i) = i_p_face(i-1)*3
    END DO
    DO face_type = 0, 3**dim - 1
       face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
       IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
          CALL get_all_indices_by_face (face_type, myjl, nloc, iloc)
          IF(nloc > 0 ) THEN
             IF (ABS(face(1)) == 1) THEN
                IF (ALLOCATED(charI)) DEALLOCATE(charI)
                IF (ALLOCATED(charX)) DEALLOCATE(charX)
                IF (ALLOCATED(charI_prev)) DEALLOCATE(charI_prev)
                IF (ALLOCATED(charX_prev)) DEALLOCATE(charX_prev)
                IF (ALLOCATED(hdiv)) DEALLOCATE(hdiv)
                ALLOCATE(charI(nloc,4),charX(nloc,3),charI_prev(nloc,3),charX_prev(nloc,1),hdiv(nloc,1))

                faceval=REAL(face(1),pr)
                faceint=(3-face(1))/2

                hdiv(1:nloc,1) = du(3,iloc(1:nloc),2)
                IF (dim==3) hdiv(1:nloc,1) = hdiv(1:nloc,1) + du(4,iloc(1:nloc),3)

                d2u(1,iloc(1:nloc),1) = 1.0_pr
                IF (Nspec>1) d2u(1,iloc(1:nloc),1) = 1.0_pr-SUM(ulc_prev(iloc(1:nloc),nspc(1):nspc(Nspecm)),DIM=2)/ulc_prev(iloc(1:nloc),nden) !Y_Nspec 0
                d2u(2,iloc(1:nloc),1) = d2u(1,iloc(1:nloc),1)*cp_in(Nspec)  !cp_Nspec 0
                d2u(2,iloc(1:nloc),2) = d2u(1,iloc(1:nloc),1)/MW_in(Nspec)  !R_Nspec 0
                DO l=1,Nspecm
                   d2u(2,iloc(1:nloc),1)  = d2u(2,iloc(1:nloc),1)  + ulc_prev(iloc(1:nloc),nspc(l))/ulc_prev(iloc(1:nloc),nden)*cp_in(l)  !cp 0
                   d2u(2,iloc(1:nloc),2)  = d2u(2,iloc(1:nloc),2)  + ulc_prev(iloc(1:nloc),nspc(l))/ulc_prev(iloc(1:nloc),nden)/MW_in(l)  !R 0 
                END DO
                d2u_diag_local(iloc(1:nloc),1) = d2u(2,iloc(1:nloc),1) !cp0
                d2u_diag_local(iloc(1:nloc),2) = d2u(2,iloc(1:nloc),2) !R0
                d2u(4,iloc(1:nloc),2) = d2u(2,iloc(1:nloc),1)/(d2u(2,iloc(1:nloc),1)-d2u(2,iloc(1:nloc),2))  !gamma0=cp0/(cp0-R0)
                d2u(3,iloc(1:nloc),2) = DSQRT(d2u(4,iloc(1:nloc),2)*pres_prev(iloc(1:nloc),1)/ulc_prev(iloc(1:nloc),nden))  !c0=SQRT(gamma0*P0/rho0)

                charI_prev(1:nloc,2) = faceval*MAX(0.0_pr,faceval*pres_prev(iloc(1:nloc),2)) * &
                                       (d2u(3,iloc(1:nloc),2)**2*du(locnvar,iloc(1:nloc),1) - du(1,iloc(1:nloc),1)) !u*(c^2*drho/dx - dp/dx)
                charI_prev(1:nloc,3) = faceval*MAX(0.0_pr,faceval*(pres_prev(iloc(1:nloc),2) + d2u(3,iloc(1:nloc),2))) * &
                           (du(1,iloc(1:nloc),1) + ulc_prev(iloc(1:nloc),nden)*d2u(3,iloc(1:nloc),2)*du(2,iloc(1:nloc),1))  !(u+c)*(dp/dx+rho*c*du/dx)
                charI_prev(1:nloc,1) = faceval*MAX(0.0_pr,faceval*(pres_prev(iloc(1:nloc),2) - d2u(3,iloc(1:nloc),2))) * &
                           (du(1,iloc(1:nloc),1) - ulc_prev(iloc(1:nloc),nden)*d2u(3,iloc(1:nloc),2)*du(2,iloc(1:nloc),1))  !(u-c)*(dp/dx-rho*c*du/dx)

                DO jj=2,dim
                   charI_prev(1:nloc,2) = charI_prev(1:nloc,2) + (SIGN(1.0_pr,faceval*pres_prev(iloc(1:nloc),2))+1.0_pr)/2.0_pr * & 
                                   pres_prev(iloc(1:nloc),jj+1)*(d2u(3,iloc(1:nloc),2)**2*du(locnvar,iloc(1:nloc),jj) - du(1,iloc(1:nloc),jj)) !u_i*(c^2*drho/dx_i-dp/dx_i)
                   charI_prev(1:nloc,3) = charI_prev(1:nloc,3) + (SIGN(1.0_pr,faceval*(pres_prev(iloc(1:nloc),2) + d2u(3,iloc(1:nloc),2)))+1.0_pr)/2.0_pr * & 
                                   pres_prev(iloc(1:nloc),jj+1) * (du(1,iloc(1:nloc),jj) + ulc_prev(iloc(1:nloc),nden)*d2u(3,iloc(1:nloc),2)*du(2,iloc(1:nloc),jj))
                   charI_prev(1:nloc,1) = charI_prev(1:nloc,1) + (SIGN(1.0_pr,faceval*(pres_prev(iloc(1:nloc),2) - d2u(3,iloc(1:nloc),2)))+1.0_pr)/2.0_pr * & 
                                   pres_prev(iloc(1:nloc),jj+1) * (du(1,iloc(1:nloc),jj) - ulc_prev(iloc(1:nloc),nden)*d2u(3,iloc(1:nloc),2)*du(2,iloc(1:nloc),jj))
                END DO

                IF (pertchar) THEN   
                   charI_prev(1:nloc,3) = charI_prev(1:nloc,3) + faceval*MAX(0.0_pr,faceval*(pres_prev(iloc(1:nloc),2) + d2u(3,iloc(1:nloc),2))) * rhoYg(faceint)
                   charI_prev(1:nloc,1) = charI_prev(1:nloc,1) + faceval*MAX(0.0_pr,faceval*(pres_prev(iloc(1:nloc),2) - d2u(3,iloc(1:nloc),2))) * rhoYg(faceint)
                   charI_prev(1:nloc,2) = charI_prev(1:nloc,2) - faceval*MAX(0.0_pr,faceval*pres_prev(iloc(1:nloc),2))*(d2u(3,iloc(1:nloc),2)**2*drho0(faceint) + rhoYg(faceint))
                END IF

                IF (PLpinf) THEN
                   charI_prev(1:nloc,3) = charI_prev(1:nloc,3) + (1.0_pr-SIGN(1.0_pr,faceval* (pres_prev(iloc(1:nloc),2) + d2u(3,iloc(1:nloc),2)) ))/2.0_pr * pBcoef * &
                                                                   (1.0_pr-maxMa**2) * &
                                                                   d2u(3,iloc(1:nloc),2) * (pres_prev(iloc(1:nloc),1) - P0(faceint) )
                   charI_prev(1:nloc,1) = charI_prev(1:nloc,1) + (1.0_pr-SIGN(1.0_pr,faceval* (pres_prev(iloc(1:nloc),2) - d2u(3,iloc(1:nloc),2)) ))/2.0_pr * pBcoef * &
                                                                   (1.0_pr-maxMa**2) * &
                                                                   d2u(3,iloc(1:nloc),2) * (pres_prev(iloc(1:nloc),1) - P0(faceint) )
                END IF

                charX_prev(1:nloc,1) = 0.5_pr/d2u(3,iloc(1:nloc),2)**2*(charI_prev(1:nloc,1)+charI_prev(1:nloc,3)+2.0_pr*charI_prev(1:nloc,2))
                IF (pertchar) charX_prev(1:nloc,1) = charX_prev(1:nloc,1) + pres_prev(iloc(1:nloc),2)*drho0(faceint)

                DO jj=1,myne_loc
                   IF (jj==nden) THEN
                      d2u(2,iloc(1:nloc),1) = -ulc_prev(iloc(1:nloc),nvel(1))/ulc_prev(iloc(1:nloc),nden)**2 !u_diag
                      d2u(2,iloc(1:nloc),2) = 0.0_pr  !gamma_diag
                      d2u(1,iloc(1:nloc),1) = 0.5_pr*SUM(ulc_prev(iloc(1:nloc),nvel(1):nvel(dim))**2,DIM=2)/ulc_prev(iloc(1:nloc),nden)**2*(d2u(4,iloc(1:nloc),2)-1.0_pr) !p_diag
                      d2u(1,iloc(1:nloc),2) = 0.5_pr/d2u(3,iloc(1:nloc),2)*d2u(4,iloc(1:nloc),2)/ulc_prev(iloc(1:nloc),nden) * &
                                                    (d2u(1,iloc(1:nloc),1)-pres_prev(iloc(1:nloc),1)/ulc_prev(iloc(1:nloc),nden) ) !c_diag
                      charI(1:nloc,3) = (SIGN(1.0_pr,faceval*(pres_prev(iloc(1:nloc),2) + d2u(3,iloc(1:nloc),2) ) )+1.0_pr)/2.0_pr * &
                                      ( (pres_prev(iloc(1:nloc),2)+d2u(3,iloc(1:nloc),2))*(du_diag_local(iloc(1:nloc),1)* &
                                        (d2u(1,iloc(1:nloc),1)+ulc_prev(iloc(1:nloc),nden)*d2u(3,iloc(1:nloc),2)*d2u(2,iloc(1:nloc),1)) + &
                                        du(2,iloc(1:nloc),1)*(d2u(3,iloc(1:nloc),2)+ulc_prev(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2))) + &
                                        (d2u(2,iloc(1:nloc),1)+d2u(1,iloc(1:nloc),2))*(du(1,iloc(1:nloc),1) + &
                                        ulc_prev(iloc(1:nloc),nden)*d2u(3,iloc(1:nloc),2)*du(2,iloc(1:nloc),1)) )
                      charI(1:nloc,1) = (SIGN(1.0_pr,faceval*(pres_prev(iloc(1:nloc),2) - d2u(3,iloc(1:nloc),2) ) )+1.0_pr)/2.0_pr * &
                                      ( (pres_prev(iloc(1:nloc),2)-d2u(3,iloc(1:nloc),2))*(du_diag_local(iloc(1:nloc),1)* &
                                        (d2u(1,iloc(1:nloc),1)-ulc_prev(iloc(1:nloc),nden)*d2u(3,iloc(1:nloc),2)*d2u(2,iloc(1:nloc),1)) - &
                                        du(2,iloc(1:nloc),1)*(d2u(3,iloc(1:nloc),2)+ulc_prev(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2))) + &
                                        (d2u(2,iloc(1:nloc),1)-d2u(1,iloc(1:nloc),2))*(du(1,iloc(1:nloc),1) - &
                                        ulc_prev(iloc(1:nloc),nden)*d2u(3,iloc(1:nloc),2)*du(2,iloc(1:nloc),1)) )
                      charI(1:nloc,2) = (SIGN(1.0_pr,faceval*pres_prev(iloc(1:nloc),2))+1.0_pr)/2.0_pr * &
                                      ( pres_prev(iloc(1:nloc),2)*(du_diag_local(iloc(1:nloc),1)*(d2u(3,iloc(1:nloc),2)**2-d2u(1,iloc(1:nloc),1)) + &
                                        2.0_pr*d2u(3,iloc(1:nloc),2)*d2u(1,iloc(1:nloc),2)*du(locnvar,iloc(1:nloc),1)) + &
                                        d2u(2,iloc(1:nloc),1)*(d2u(3,iloc(1:nloc),2)**2*du(locnvar,iloc(1:nloc),1)-du(1,iloc(1:nloc),1)) )
                      DO ii=2,dim
                         charI(1:nloc,3) = charI(1:nloc,3) + (SIGN(1.0_pr,faceval*(pres_prev(iloc(1:nloc),2) + d2u(3,iloc(1:nloc),2) ) )+1.0_pr)/2.0_pr * &
                                        (    -ulc_prev(iloc(1:nloc),nvel(ii))/ulc_prev(iloc(1:nloc),nden)**2*(du(1,iloc(1:nloc),1) + &
                                              ulc_prev(iloc(1:nloc),nden)*d2u(3,iloc(1:nloc),2)*du(2,iloc(1:nloc),1)) + &
                                              pres_prev(iloc(1:nloc),1+ii)*(du_diag_local(iloc(1:nloc),ii)* &
                                        (d2u(1,iloc(1:nloc),1)+ulc_prev(iloc(1:nloc),nden)*d2u(3,iloc(1:nloc),2)*d2u(2,iloc(1:nloc),1)) + &
                                        du(2,iloc(1:nloc),ii)*(d2u(3,iloc(1:nloc),2)+ulc_prev(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2))) )
                         charI(1:nloc,1) = charI(1:nloc,1) + (SIGN(1.0_pr,faceval*(pres_prev(iloc(1:nloc),2) - d2u(3,iloc(1:nloc),2) ) )+1.0_pr)/2.0_pr * &
                                        (    -ulc_prev(iloc(1:nloc),nvel(ii))/ulc_prev(iloc(1:nloc),nden)**2*(du(1,iloc(1:nloc),1) - &
                                              ulc_prev(iloc(1:nloc),nden)*d2u(3,iloc(1:nloc),2)*du(2,iloc(1:nloc),1)) + &
                                              pres_prev(iloc(1:nloc),1+ii)*(du_diag_local(iloc(1:nloc),ii)* &
                                        (d2u(1,iloc(1:nloc),1)-ulc_prev(iloc(1:nloc),nden)*d2u(3,iloc(1:nloc),2)*d2u(2,iloc(1:nloc),1)) - &
                                        du(2,iloc(1:nloc),ii)*(d2u(3,iloc(1:nloc),2)+ulc_prev(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2))) )
                         charI(1:nloc,2) = charI(1:nloc,2) + (SIGN(1.0_pr,faceval*pres_prev(iloc(1:nloc),2))+1.0_pr)/2.0_pr * &
                                      ( pres_prev(iloc(1:nloc),1+ii)*(du_diag_local(iloc(1:nloc),ii)*(d2u(3,iloc(1:nloc),2)**2-d2u(1,iloc(1:nloc),1)) + &
                                        2.0_pr*d2u(3,iloc(1:nloc),2)*d2u(1,iloc(1:nloc),2)*du(locnvar,iloc(1:nloc),ii)) - &
                                        ulc_prev(iloc(1:nloc),nvel(ii))/ulc_prev(iloc(1:nloc),nden)**2 * &
                                        (d2u(3,iloc(1:nloc),2)**2*du(locnvar,iloc(1:nloc),ii)-du(1,iloc(1:nloc),ii)) )
                      END DO

                      IF (pertchar) THEN   
                         charI(1:nloc,3) = charI(1:nloc,3) + (SIGN(1.0_pr,faceval*(pres_prev(iloc(1:nloc),2) + d2u(3,iloc(1:nloc),2) ) )+1.0_pr)/2.0_pr * &
                                                             (d2u(2,iloc(1:nloc),1) + d2u(1,iloc(1:nloc),2)) * rhoYg(faceint)
                         charI(1:nloc,1) = charI(1:nloc,1) + (SIGN(1.0_pr,faceval*(pres_prev(iloc(1:nloc),2) - d2u(3,iloc(1:nloc),2) ) )+1.0_pr)/2.0_pr * &
                                                             (d2u(2,iloc(1:nloc),1) - d2u(1,iloc(1:nloc),2)) * rhoYg(faceint)
                         charI(1:nloc,2) = charI(1:nloc,2) + (SIGN(1.0_pr,faceval*pres_prev(iloc(1:nloc),2))+1.0_pr)/2.0_pr * &
                                                          ( -d2u(2,iloc(1:nloc),1)*(d2u(3,iloc(1:nloc),2)**2*drho0(faceint)+rhoYg(faceint)) &
                                                            -pres_prev(iloc(1:nloc),2)*2.0_pr*d2u(3,iloc(1:nloc),2)*d2u(1,iloc(1:nloc),2)*drho0(faceint) )
                      END IF
                      IF (PLpinf) THEN
                         charI(1:nloc,3) = charI(1:nloc,3) + (1.0_pr-SIGN(1.0_pr,faceval* (pres_prev(iloc(1:nloc),2) + d2u(3,iloc(1:nloc),2)) ))/2.0_pr * pBcoef * &
                                                                   (1.0_pr-maxMa**2) * &
                                                              (d2u(1,iloc(1:nloc),2)*(pres_prev(iloc(1:nloc),1) - P0(faceint) ) + d2u(3,iloc(1:nloc),2)*d2u(1,iloc(1:nloc),1))
                         charI(1:nloc,1) = charI(1:nloc,1) + (1.0_pr-SIGN(1.0_pr,faceval* (pres_prev(iloc(1:nloc),2) - d2u(3,iloc(1:nloc),2)) ))/2.0_pr * pBcoef * &
                                                                   (1.0_pr-maxMa**2) * &
                                                              (d2u(1,iloc(1:nloc),2)*(pres_prev(iloc(1:nloc),1) - P0(faceint) ) + d2u(3,iloc(1:nloc),2)*d2u(1,iloc(1:nloc),1))
                      END IF

                      charX(1:nloc,1) = 0.5_pr/d2u(3,iloc(1:nloc),2)**2 * (charI(1:nloc,1)+charI(1:nloc,3)+2.0_pr*charI(1:nloc,2)) - &
                                                     2.0_pr*d2u(1,iloc(1:nloc),2)/d2u(3,iloc(1:nloc),2)*charX_prev(1:nloc,1)

                      IF (pertchar) charX(1:nloc,1) = charX(1:nloc,1) + d2u(2,iloc(1:nloc),1)*drho0(faceint)   

                      IF( face(1) == 1  ) THEN                          ! Top
                         charI(1:nloc,1) = pres_prev(iloc(1:nloc),2)/d2u(3,iloc(1:nloc),2)  !beta-=u/c
                         charI(1:nloc,2) = 1.0_pr                                            !beta+=1
                         charI(1:nloc,3) = (d2u(2,iloc(1:nloc),1) - pres_prev(iloc(1:nloc),2)/d2u(3,iloc(1:nloc),2)*d2u(1,iloc(1:nloc),2))/d2u(3,iloc(1:nloc),2)  !beta-=u/c
                         charI(1:nloc,4) = 0.0_pr                                            !beta+=1
                      ELSE IF( face(1) == -1  ) THEN                    ! Bottom
                         charI(1:nloc,2) = pres_prev(iloc(1:nloc),2)/d2u(3,iloc(1:nloc),2)  !beta+=u/c
                         charI(1:nloc,1) = 1.0_pr                                            !beta-=1
                         charI(1:nloc,4) = (d2u(2,iloc(1:nloc),1) - pres_prev(iloc(1:nloc),2)/d2u(3,iloc(1:nloc),2)*d2u(1,iloc(1:nloc),2))/d2u(3,iloc(1:nloc),2)  !beta+=u/c
                         charI(1:nloc,3) = 0.0_pr                                            !beta-=1
                      END IF

                      Drhs_diag_local((jj-1)*mynloc+iloc(1:nloc)) = Drhs_diag_local((jj-1)*mynloc+iloc(1:nloc)) - charX(1:nloc,1) - 0.5_pr * &
                                  ( (charI(1:nloc,1)+charI(1:nloc,2))*(hdiv(1:nloc,1) - SUM(du_diag_local(iloc(1:nloc),2:dim)*ulc_prev(iloc(1:nloc),nvel(2):nvel(dim)),DIM=2) / &
                                    ulc_prev(iloc(1:nloc),nden)) + ulc_prev(iloc(1:nloc),nden)*hdiv(1:nloc,1)*(charI(1:nloc,3)+charI(1:nloc,4)) )  
                   ELSE IF (jj==nvel(1)) THEN
                      d2u(2,iloc(1:nloc),1) = 1.0_pr/ulc_prev(iloc(1:nloc),nden) !u_diag
                      d2u(2,iloc(1:nloc),2) = 0.0_pr  !gamma_diag
                      d2u(1,iloc(1:nloc),1) = -ulc_prev(iloc(1:nloc),nvel(1))/ulc_prev(iloc(1:nloc),nden)*(d2u(4,iloc(1:nloc),2)-1.0_pr) !p_diag
                      d2u(1,iloc(1:nloc),2) = 0.5_pr/d2u(3,iloc(1:nloc),2)*d2u(4,iloc(1:nloc),2)/ulc_prev(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),1) !c_diag
                      charI(1:nloc,3) = (SIGN(1.0_pr,faceval*(pres_prev(iloc(1:nloc),2) + d2u(3,iloc(1:nloc),2) ) )+1.0_pr)/2.0_pr * &
                                      ( (pres_prev(iloc(1:nloc),2)+d2u(3,iloc(1:nloc),2))*(du_diag_local(iloc(1:nloc),1)* &
                                        (d2u(1,iloc(1:nloc),1)+ulc_prev(iloc(1:nloc),nden)*d2u(3,iloc(1:nloc),2)*d2u(2,iloc(1:nloc),1)) + &
                                        du(2,iloc(1:nloc),1)*ulc_prev(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)) + &
                                        (d2u(2,iloc(1:nloc),1)+d2u(1,iloc(1:nloc),2))*(du(1,iloc(1:nloc),1) + &
                                        ulc_prev(iloc(1:nloc),nden)*d2u(3,iloc(1:nloc),2)*du(2,iloc(1:nloc),1)) )
                      charI(1:nloc,1) = (SIGN(1.0_pr,faceval*(pres_prev(iloc(1:nloc),2) - d2u(3,iloc(1:nloc),2) ) )+1.0_pr)/2.0_pr * &
                                      ( (pres_prev(iloc(1:nloc),2)-d2u(3,iloc(1:nloc),2))*(du_diag_local(iloc(1:nloc),1)* &
                                        (d2u(1,iloc(1:nloc),1)-ulc_prev(iloc(1:nloc),nden)*d2u(3,iloc(1:nloc),2)*d2u(2,iloc(1:nloc),1)) - &
                                        du(2,iloc(1:nloc),1)*ulc_prev(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)) + &
                                        (d2u(2,iloc(1:nloc),1)-d2u(1,iloc(1:nloc),2))*(du(1,iloc(1:nloc),1)- &
                                        ulc_prev(iloc(1:nloc),nden)*d2u(3,iloc(1:nloc),2)*du(2,iloc(1:nloc),1)) )
                      charI(1:nloc,2) = (SIGN(1.0_pr,faceval*pres_prev(iloc(1:nloc),2))+1.0_pr)/2.0_pr * &
                                      ( pres_prev(iloc(1:nloc),2)*(2.0_pr*d2u(3,iloc(1:nloc),2)*d2u(1,iloc(1:nloc),2)*du(locnvar,iloc(1:nloc),1) - &
                                        du_diag_local(iloc(1:nloc),1)*d2u(1,iloc(1:nloc),1)) + &
                                        d2u(2,iloc(1:nloc),1)*(d2u(3,iloc(1:nloc),2)**2*du(locnvar,iloc(1:nloc),1)-du(1,iloc(1:nloc),1)) )
                      DO ii=2,dim
                         charI(1:nloc,3) = charI(1:nloc,3) + (SIGN(1.0_pr,faceval*(pres_prev(iloc(1:nloc),2) + d2u(3,iloc(1:nloc),2) ) )+1.0_pr)/2.0_pr * &
                                      ( pres_prev(iloc(1:nloc),1+ii)*(du_diag_local(iloc(1:nloc),ii)* &
                                        (d2u(1,iloc(1:nloc),1)+ulc_prev(iloc(1:nloc),nden)*d2u(3,iloc(1:nloc),2)*d2u(2,iloc(1:nloc),1)) + &
                                        du(2,iloc(1:nloc),ii)*ulc_prev(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)) )
                         charI(1:nloc,1) = (SIGN(1.0_pr,faceval*(pres_prev(iloc(1:nloc),2) - d2u(3,iloc(1:nloc),2) ) )+1.0_pr)/2.0_pr * &
                                      ( charI(1:nloc,1) + pres_prev(iloc(1:nloc),1+ii)*(du_diag_local(iloc(1:nloc),ii)* &
                                        (d2u(1,iloc(1:nloc),1)-ulc_prev(iloc(1:nloc),nden)*d2u(3,iloc(1:nloc),2)*d2u(2,iloc(1:nloc),1)) - &
                                        du(2,iloc(1:nloc),ii)*ulc_prev(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)) )
                         charI(1:nloc,2) = (SIGN(1.0_pr,faceval*pres_prev(iloc(1:nloc),2))+1.0_pr)/2.0_pr * &
                                      ( charI(1:nloc,2) + pres_prev(iloc(1:nloc),1+ii)*(2.0_pr*d2u(3,iloc(1:nloc),2)*d2u(1,iloc(1:nloc),2)*du(locnvar,iloc(1:nloc),ii) - &
                                        du_diag_local(iloc(1:nloc),ii)*d2u(1,iloc(1:nloc),1)) )
                      END DO
                      IF (pertchar) THEN   
                         charI(1:nloc,3) = charI(1:nloc,3) + (SIGN(1.0_pr,faceval*(pres_prev(iloc(1:nloc),2) + d2u(3,iloc(1:nloc),2) ) )+1.0_pr)/2.0_pr * &
                                                             (d2u(2,iloc(1:nloc),1) + d2u(1,iloc(1:nloc),2)) * rhoYg(faceint)
                         charI(1:nloc,1) = charI(1:nloc,1) + (SIGN(1.0_pr,faceval*(pres_prev(iloc(1:nloc),2) - d2u(3,iloc(1:nloc),2) ) )+1.0_pr)/2.0_pr * &
                                                             (d2u(2,iloc(1:nloc),1) - d2u(1,iloc(1:nloc),2)) * rhoYg(faceint)
                         charI(1:nloc,2) = charI(1:nloc,2) + (SIGN(1.0_pr,faceval*pres_prev(iloc(1:nloc),2))+1.0_pr)/2.0_pr * &
                                                           ( -d2u(2,iloc(1:nloc),1)*(d2u(3,iloc(1:nloc),2)**2*drho0(faceint)+rhoYg(faceint)) &
                                                             -pres_prev(iloc(1:nloc),2)*2.0_pr*d2u(3,iloc(1:nloc),2)*d2u(1,iloc(1:nloc),2)*drho0(faceint) )
                      END IF
                      IF (PLpinf) THEN
                         charI(1:nloc,3) = charI(1:nloc,3) + (1.0_pr-SIGN(1.0_pr,faceval* (pres_prev(iloc(1:nloc),2) + d2u(3,iloc(1:nloc),2)) ))/2.0_pr * pBcoef * &
                                                                   (1.0_pr-maxMa**2) * &
                                                              (d2u(1,iloc(1:nloc),2)*(pres_prev(iloc(1:nloc),1) - P0(faceint) ) + d2u(3,iloc(1:nloc),2)*d2u(1,iloc(1:nloc),1))
                         charI(1:nloc,1) = charI(1:nloc,1) + (1.0_pr-SIGN(1.0_pr,faceval* (pres_prev(iloc(1:nloc),2) - d2u(3,iloc(1:nloc),2)) ))/2.0_pr * pBcoef * &
                                                                   (1.0_pr-maxMa**2) * &
                                                              (d2u(1,iloc(1:nloc),2)*(pres_prev(iloc(1:nloc),1) - P0(faceint) ) + d2u(3,iloc(1:nloc),2)*d2u(1,iloc(1:nloc),1))
                      END IF

                      charX(1:nloc,1) = 0.5_pr/d2u(3,iloc(1:nloc),2)**2 * (charI(1:nloc,1)+charI(1:nloc,3)+2.0_pr*charI(1:nloc,2)) - &
                                                     2.0_pr*d2u(1,iloc(1:nloc),2)/d2u(3,iloc(1:nloc),2)*charX_prev(1:nloc,1)
                      charX(1:nloc,2) = 0.5_pr/ulc_prev(iloc(1:nloc),nden)/d2u(3,iloc(1:nloc),2) * (charI(1:nloc,3)-charI(1:nloc,1) - &
                                                     d2u(1,iloc(1:nloc),2)/d2u(3,iloc(1:nloc),2)*(charI_prev(1:nloc,3)-charI_prev(1:nloc,1)))
                      IF (pertchar) charX(1:nloc,1) = charX(1:nloc,1) + d2u(2,iloc(1:nloc),1)*drho0(faceint)
   
                      IF( face(1) == 1  ) THEN                          ! Top
                         charI(1:nloc,1) = pres_prev(iloc(1:nloc),2)/d2u(3,iloc(1:nloc),2)  !beta-=u/c
                         charI(1:nloc,2) = 1.0_pr                                            !beta+=1
                         charI(1:nloc,3) = (d2u(2,iloc(1:nloc),1) - pres_prev(iloc(1:nloc),2)/d2u(3,iloc(1:nloc),2)*d2u(1,iloc(1:nloc),2))/d2u(3,iloc(1:nloc),2)  !beta-=u/c
                         charI(1:nloc,4) = 0.0_pr                                            !beta+=1
                      ELSE IF( face(1) == -1  ) THEN                    ! Bottom
                         charI(1:nloc,2) = pres_prev(iloc(1:nloc),2)/d2u(3,iloc(1:nloc),2)  !beta+=u/c
                         charI(1:nloc,1) = 1.0_pr                                            !beta-=1
                         charI(1:nloc,4) = (d2u(2,iloc(1:nloc),1) - pres_prev(iloc(1:nloc),2)/d2u(3,iloc(1:nloc),2)*d2u(1,iloc(1:nloc),2))/d2u(3,iloc(1:nloc),2)  !beta+=u/c
                         charI(1:nloc,3) = 0.0_pr                                            !beta-=1
                      END IF

                      Drhs_diag_local((jj-1)*mynloc+iloc(1:nloc)) = Drhs_diag_local((jj-1)*mynloc+iloc(1:nloc)) - d2u(2,iloc(1:nloc),1)*charX_prev(1:nloc,1) - &
                                                     pres_prev(iloc(1:nloc),2)*charX(1:nloc,1) - ulc_prev(iloc(1:nloc),nden)*charX(1:nloc,2) - hdiv(1:nloc,1)/2.0_pr * &
                                  ( (charI(1:nloc,1)+charI(1:nloc,2)) + (charI(1:nloc,3)+charI(1:nloc,4))*ulc_prev(iloc(1:nloc),nvel(1)) + &
                                    (charI(1:nloc,2)-charI(1:nloc,1))*ulc_prev(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2) + &
                                                                        (charI(1:nloc,4)-charI(1:nloc,3))*ulc_prev(iloc(1:nloc),nden)*d2u(3,iloc(1:nloc),2) ) 
                   ELSE IF (jj>=nvel(2) .AND. jj<=nvel(dim)) THEN
                      d2u(2,iloc(1:nloc),1) = 0.0_pr !u_diag
                      d2u(2,iloc(1:nloc),2) = 0.0_pr  !gamma_diag
                      d2u(1,iloc(1:nloc),1) = -ulc_prev(iloc(1:nloc),jj)/ulc_prev(iloc(1:nloc),nden)*(d2u(4,iloc(1:nloc),2)-1.0_pr) !p_diag
                      d2u(1,iloc(1:nloc),2) = 0.5_pr/d2u(3,iloc(1:nloc),2)*d2u(4,iloc(1:nloc),2)/ulc_prev(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),1) !c_diag
                      charI(1:nloc,3) = (SIGN(1.0_pr,faceval*(pres_prev(iloc(1:nloc),2) + d2u(3,iloc(1:nloc),2) ) )+1.0_pr)/2.0_pr * &
                                      ( (pres_prev(iloc(1:nloc),2)+d2u(3,iloc(1:nloc),2))*(du_diag_local(iloc(1:nloc),1)*d2u(1,iloc(1:nloc),1) + &
                                        du(2,iloc(1:nloc),1)*ulc_prev(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)) + &
                                        d2u(1,iloc(1:nloc),2)*(du(1,iloc(1:nloc),1)+ulc_prev(iloc(1:nloc),nden)*d2u(3,iloc(1:nloc),2)*du(2,iloc(1:nloc),1)) + &
                                        (du(1,iloc(1:nloc),jj-nvel(1)+1)+ulc_prev(iloc(1:nloc),nden)*d2u(3,iloc(1:nloc),2)*du(2,iloc(1:nloc),jj-nvel(1)+1)) / &
                                        ulc_prev(iloc(1:nloc),nden) )
                      charI(1:nloc,1) = (SIGN(1.0_pr,faceval*(pres_prev(iloc(1:nloc),2) - d2u(3,iloc(1:nloc),2) ) )+1.0_pr)/2.0_pr * &
                                      ( (pres_prev(iloc(1:nloc),2)-d2u(3,iloc(1:nloc),2))*(du_diag_local(iloc(1:nloc),1)*d2u(1,iloc(1:nloc),1) - &
                                        du(2,iloc(1:nloc),1)*ulc_prev(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)) - &
                                        d2u(1,iloc(1:nloc),2)*(du(1,iloc(1:nloc),1)-ulc_prev(iloc(1:nloc),nden)*d2u(3,iloc(1:nloc),2)*du(2,iloc(1:nloc),1)) + &
                                        (du(1,iloc(1:nloc),jj-nvel(1)+1)-ulc_prev(iloc(1:nloc),nden)*d2u(3,iloc(1:nloc),2)*du(2,iloc(1:nloc),jj-nvel(1)+1)) / &
                                        ulc_prev(iloc(1:nloc),nden) )
                      charI(1:nloc,2) = (SIGN(1.0_pr,faceval*pres_prev(iloc(1:nloc),2))+1.0_pr)/2.0_pr * &
                                      ( pres_prev(iloc(1:nloc),2)*(2.0_pr*d2u(3,iloc(1:nloc),2)*d2u(1,iloc(1:nloc),2)*du(locnvar,iloc(1:nloc),1) - &
                                        du_diag_local(iloc(1:nloc),1)*d2u(1,iloc(1:nloc),1)) - &
                                        (d2u(3,iloc(1:nloc),2)**2*du(locnvar,iloc(1:nloc),jj-nvel(1)+1)-du(1,iloc(1:nloc),jj-nvel(1)+1))/ulc_prev(iloc(1:nloc),nden) )
                      DO ii=2,dim
                         charI(1:nloc,3) = (SIGN(1.0_pr,faceval*(pres_prev(iloc(1:nloc),2) + d2u(3,iloc(1:nloc),2) ) )+1.0_pr)/2.0_pr * &
                                      ( charI(1:nloc,3) + pres_prev(iloc(1:nloc),1+ii)*(du_diag_local(iloc(1:nloc),ii)*d2u(1,iloc(1:nloc),1) + &
                                        du(2,iloc(1:nloc),ii)*ulc_prev(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)) )
                         charI(1:nloc,1) = (SIGN(1.0_pr,faceval*(pres_prev(iloc(1:nloc),2) - d2u(3,iloc(1:nloc),2) ) )+1.0_pr)/2.0_pr * &
                                      ( charI(1:nloc,1) + pres_prev(iloc(1:nloc),1+ii)*(du_diag_local(iloc(1:nloc),ii)*d2u(1,iloc(1:nloc),1) - &
                                        du(2,iloc(1:nloc),ii)*ulc_prev(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)) )
                         charI(1:nloc,2) = (SIGN(1.0_pr,faceval*pres_prev(iloc(1:nloc),2))+1.0_pr)/2.0_pr * &
                                      ( charI(1:nloc,2) + pres_prev(iloc(1:nloc),1+ii)*(2.0_pr*d2u(3,iloc(1:nloc),2)*d2u(1,iloc(1:nloc),2)*du(locnvar,iloc(1:nloc),ii) - &
                                        du_diag_local(iloc(1:nloc),ii)*d2u(1,iloc(1:nloc),1)) )
                      END DO
                      charI(1:nloc,4) = (SIGN(1.0_pr,faceval*pres_prev(iloc(1:nloc),2))+1.0_pr)/2.0_pr * &
                                      ( (SUM(pres_prev(iloc(1:nloc),2:dim+1)*du_diag_local(iloc(1:nloc),1:dim),DIM=2)+du(jj-nvel(1)+2,iloc(1:nloc),jj-nvel(1)+1)) / &
                                        ulc_prev(iloc(1:nloc),nden) )
                      IF (pertchar) THEN   
                         charI(1:nloc,3) = charI(1:nloc,3) + (SIGN(1.0_pr,faceval*(pres_prev(iloc(1:nloc),2) + d2u(3,iloc(1:nloc),2) ) )+1.0_pr)/2.0_pr * &
                                            d2u(1,iloc(1:nloc),2) * rhoYg(faceint)
                         charI(1:nloc,1) = charI(1:nloc,1) - (SIGN(1.0_pr,faceval*(pres_prev(iloc(1:nloc),2) - d2u(3,iloc(1:nloc),2) ) )+1.0_pr)/2.0_pr * &
                                            d2u(1,iloc(1:nloc),2) * rhoYg(faceint)
                         charI(1:nloc,2) = charI(1:nloc,2) - (SIGN(1.0_pr,faceval*pres_prev(iloc(1:nloc),2))+1.0_pr)/2.0_pr * &
                                            pres_prev(iloc(1:nloc),2)*2.0_pr*d2u(3,iloc(1:nloc),2)*d2u(1,iloc(1:nloc),2)*drho0(faceint)
                      END IF
                      IF (PLpinf) THEN
                         charI(1:nloc,3) = charI(1:nloc,3) + (1.0_pr-SIGN(1.0_pr,faceval* (pres_prev(iloc(1:nloc),2) + d2u(3,iloc(1:nloc),2)) ))/2.0_pr * pBcoef * &
                                                                   (1.0_pr-maxMa**2) * &
                                                              (d2u(1,iloc(1:nloc),2)*(pres_prev(iloc(1:nloc),1) - P0(faceint) ) + d2u(3,iloc(1:nloc),2)*d2u(1,iloc(1:nloc),1))
                         charI(1:nloc,1) = charI(1:nloc,1) + (1.0_pr-SIGN(1.0_pr,faceval* (pres_prev(iloc(1:nloc),2) - d2u(3,iloc(1:nloc),2)) ))/2.0_pr * pBcoef * &
                                                                   (1.0_pr-maxMa**2) * &
                                                              (d2u(1,iloc(1:nloc),2)*(pres_prev(iloc(1:nloc),1) - P0(faceint) ) + d2u(3,iloc(1:nloc),2)*d2u(1,iloc(1:nloc),1))
                      END IF

                      charX(1:nloc,1) = 0.5_pr/d2u(3,iloc(1:nloc),2)**2 * (charI(1:nloc,1)+charI(1:nloc,3)+2.0_pr*charI(1:nloc,2)) - &
                                                     2.0_pr*d2u(1,iloc(1:nloc),2)/d2u(3,iloc(1:nloc),2)*charX_prev(1:nloc,1)
                      charX(1:nloc,3) = charI(1:nloc,4)

                      IF( face(1) == 1  ) THEN                          ! Top
                         charI(1:nloc,1) = pres_prev(iloc(1:nloc),2)/d2u(3,iloc(1:nloc),2)  !beta-=u/c
                         charI(1:nloc,2) = 1.0_pr                                            !beta+=1
                         charI(1:nloc,3) = -pres_prev(iloc(1:nloc),2)/d2u(3,iloc(1:nloc),2)**2*d2u(1,iloc(1:nloc),2)  !beta-=u/c
                         charI(1:nloc,4) = 0.0_pr                                            !beta+=1
                      ELSE IF( face(1) == -1  ) THEN                    ! Bottom
                         charI(1:nloc,2) = pres_prev(iloc(1:nloc),2)/d2u(3,iloc(1:nloc),2)  !beta+=u/c
                         charI(1:nloc,1) = 1.0_pr                                            !beta-=1
                         charI(1:nloc,4) = -pres_prev(iloc(1:nloc),2)/d2u(3,iloc(1:nloc),2)**2*d2u(1,iloc(1:nloc),2)  !beta+=u/c
                         charI(1:nloc,3) = 0.0_pr                                            !beta-=1
                      END IF

                      Drhs_diag_local((jj-1)*mynloc+iloc(1:nloc)) = Drhs_diag_local((jj-1)*mynloc+iloc(1:nloc)) - (charX_prev(1:nloc,1)+ulc_prev(iloc(1:nloc),jj)*charX(1:nloc,1)) / &
                                                     ulc_prev(iloc(1:nloc),nden) - ulc_prev(iloc(1:nloc),nden)*charX(1:nloc,3) - &
                                                     d2u(1,iloc(1:nloc),1)*du_diag_local(iloc(1:nloc),jj-nvel(1)+1) + 0.5_pr * &
                            ( (charI(1:nloc,1)+charI(1:nloc,2))*(hdiv(1:nloc,1) + ulc_prev(iloc(1:nloc),jj)/ulc_prev(iloc(1:nloc),nden)*du_diag_local(iloc(1:nloc),jj-nvel(1)+1)) + &
                              (charI(1:nloc,3)+charI(1:nloc,4))*ulc_prev(iloc(1:nloc),jj)*hdiv(1:nloc,1) )
                   ELSE IF (jj==neng) THEN
                      d2u(2,iloc(1:nloc),1) = 0.0_pr !u_diag
                      d2u(2,iloc(1:nloc),2) = 0.0_pr  !gamma_diag
                      d2u(1,iloc(1:nloc),1) = d2u(4,iloc(1:nloc),2)-1.0_pr !p_diag
                      d2u(1,iloc(1:nloc),2) = 0.5_pr/d2u(3,iloc(1:nloc),2)*d2u(4,iloc(1:nloc),2)/ulc_prev(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),1) !c_diag
                      
                      charI(1:nloc,3) = (SIGN(1.0_pr,faceval*(pres_prev(iloc(1:nloc),2) + d2u(3,iloc(1:nloc),2) ) )+1.0_pr)/2.0_pr * &
                                      ( (pres_prev(iloc(1:nloc),2)+d2u(3,iloc(1:nloc),2))*(du_diag_local(iloc(1:nloc),1)*d2u(1,iloc(1:nloc),1) + &
                                        du(2,iloc(1:nloc),1)*ulc_prev(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)) + &
                                        d2u(1,iloc(1:nloc),2)*(du(1,iloc(1:nloc),1)+ulc_prev(iloc(1:nloc),nden)*d2u(3,iloc(1:nloc),2)*du(2,iloc(1:nloc),1)) )
                      charI(1:nloc,1) = (SIGN(1.0_pr,faceval*(pres_prev(iloc(1:nloc),2) - d2u(3,iloc(1:nloc),2) ) )+1.0_pr)/2.0_pr * &
                                      ( (pres_prev(iloc(1:nloc),2)-d2u(3,iloc(1:nloc),2))*(du_diag_local(iloc(1:nloc),1)*d2u(1,iloc(1:nloc),1) - &
                                        du(2,iloc(1:nloc),1)*ulc_prev(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)) - &
                                        d2u(1,iloc(1:nloc),2)*(du(1,iloc(1:nloc),1)-ulc_prev(iloc(1:nloc),nden)*d2u(3,iloc(1:nloc),2)*du(2,iloc(1:nloc),1)) )
                      charI(1:nloc,2) = faceval*MAX(0.0_pr,faceval*pres_prev(iloc(1:nloc),2)) * &
                                    (2.0_pr*d2u(3,iloc(1:nloc),2)*d2u(1,iloc(1:nloc),2)*du(locnvar,iloc(1:nloc),1) - du_diag_local(iloc(1:nloc),1)*d2u(1,iloc(1:nloc),1))
                      DO ii=2,dim
                         charI(1:nloc,3) = charI(1:nloc,3) + (SIGN(1.0_pr,faceval*(pres_prev(iloc(1:nloc),2) + d2u(3,iloc(1:nloc),2) ) )+1.0_pr)/2.0_pr * &
                                      ( pres_prev(iloc(1:nloc),1+ii)*(du_diag_local(iloc(1:nloc),ii)*d2u(1,iloc(1:nloc),1) + &
                                        du(2,iloc(1:nloc),ii)*ulc_prev(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)) )
                         charI(1:nloc,1) = charI(1:nloc,1) + (SIGN(1.0_pr,faceval*(pres_prev(iloc(1:nloc),2) - d2u(3,iloc(1:nloc),2) ) )+1.0_pr)/2.0_pr * &
                                      ( pres_prev(iloc(1:nloc),1+ii)*(du_diag_local(iloc(1:nloc),ii)*d2u(1,iloc(1:nloc),1) - &
                                        du(2,iloc(1:nloc),ii)*ulc_prev(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)) )
                         charI(1:nloc,2) = charI(1:nloc,2) + (SIGN(1.0_pr,faceval*pres_prev(iloc(1:nloc),2))+1.0_pr)/2.0_pr * &
                                      ( pres_prev(iloc(1:nloc),1+ii)*(2.0_pr*d2u(3,iloc(1:nloc),2)*d2u(1,iloc(1:nloc),2)*du(locnvar,iloc(1:nloc),ii) - &
                                        du_diag_local(iloc(1:nloc),ii)*d2u(1,iloc(1:nloc),1)) )
                      END DO
                      IF (pertchar) THEN   
                         charI(1:nloc,3) = charI(1:nloc,3) + (SIGN(1.0_pr,faceval*(pres_prev(iloc(1:nloc),2) + d2u(3,iloc(1:nloc),2) ) )+1.0_pr)/2.0_pr * &
                                                             d2u(1,iloc(1:nloc),2) * rhoYg(faceint)
                         charI(1:nloc,1) = charI(1:nloc,1) - (SIGN(1.0_pr,faceval*(pres_prev(iloc(1:nloc),2) - d2u(3,iloc(1:nloc),2) ) )+1.0_pr)/2.0_pr * &
                                                             d2u(1,iloc(1:nloc),2) * rhoYg(faceint)
                         charI(1:nloc,2) = charI(1:nloc,2) - (SIGN(1.0_pr,faceval*pres_prev(iloc(1:nloc),2))+1.0_pr)/2.0_pr * &
                                                             pres_prev(iloc(1:nloc),2)*2.0_pr*d2u(3,iloc(1:nloc),2)*d2u(1,iloc(1:nloc),2)*drho0(faceint)
                      END IF
                      IF (PLpinf) THEN
                         charI(1:nloc,3) = charI(1:nloc,3) + (1.0_pr-SIGN(1.0_pr,faceval* (pres_prev(iloc(1:nloc),2) + d2u(3,iloc(1:nloc),2)) ))/2.0_pr * pBcoef * &
                                                                   (1.0_pr-maxMa**2) * &
                                                              (d2u(1,iloc(1:nloc),2)*(pres_prev(iloc(1:nloc),1) - P0(faceint) ) + d2u(3,iloc(1:nloc),2)*d2u(1,iloc(1:nloc),1))
                         charI(1:nloc,1) = charI(1:nloc,1) + (1.0_pr-SIGN(1.0_pr,faceval* (pres_prev(iloc(1:nloc),2) - d2u(3,iloc(1:nloc),2)) ))/2.0_pr * pBcoef * &
                                                                   (1.0_pr-maxMa**2) * &
                                                              (d2u(1,iloc(1:nloc),2)*(pres_prev(iloc(1:nloc),1) - P0(faceint) ) + d2u(3,iloc(1:nloc),2)*d2u(1,iloc(1:nloc),1))
                      END IF

                      charX(1:nloc,1) = 0.5_pr/d2u(3,iloc(1:nloc),2)**2 * (charI(1:nloc,1)+charI(1:nloc,3)+2.0_pr*charI(1:nloc,2)) - &
                                                     2.0_pr*d2u(1,iloc(1:nloc),2)/d2u(3,iloc(1:nloc),2)*charX_prev(1:nloc,1)
                      charX(1:nloc,2) = 0.5_pr/ulc_prev(iloc(1:nloc),nden)/d2u(3,iloc(1:nloc),2) * (charI(1:nloc,3)-charI(1:nloc,1) - &
                                                     d2u(1,iloc(1:nloc),2)/d2u(3,iloc(1:nloc),2)*(charI_prev(1:nloc,3)-charI_prev(1:nloc,1)))
                      charX(1:nloc,3) = 0.5_pr*(charI(1:nloc,1)+charI(1:nloc,3))

                      IF( face(1) == 1  ) THEN                          ! Top
                         charI(1:nloc,1) = pres_prev(iloc(1:nloc),2)/d2u(3,iloc(1:nloc),2)  !beta-=u/c
                         charI(1:nloc,2) = 1.0_pr                                            !beta+=1
                         charI(1:nloc,3) = -pres_prev(iloc(1:nloc),2)/d2u(3,iloc(1:nloc),2)**2*d2u(1,iloc(1:nloc),2)  !beta-=u/c
                         charI(1:nloc,4) = 0.0_pr                                            !beta+=1
                      ELSE IF( face(1) == -1  ) THEN                    ! Bottom
                         charI(1:nloc,2) = pres_prev(iloc(1:nloc),2)/d2u(3,iloc(1:nloc),2)  !beta+=u/c
                         charI(1:nloc,1) = 1.0_pr                                            !beta-=1
                         charI(1:nloc,4) = -pres_prev(iloc(1:nloc),2)/d2u(3,iloc(1:nloc),2)**2*d2u(1,iloc(1:nloc),2)  !beta+=u/c
                         charI(1:nloc,3) = 0.0_pr                                            !beta-=1
                      END IF

                      Drhs_diag_local((jj-1)*mynloc+iloc(1:nloc)) = Drhs_diag_local((jj-1)*mynloc+iloc(1:nloc)) - 0.5_pr*SUM(ulc_prev(iloc(1:nloc),nvel(1):nvel(dim))**2,DIM=2) / &
                                        ulc_prev(iloc(1:nloc),nden)**2*charX(1:nloc,1) - ulc_prev(iloc(1:nloc),nvel(1))*charX(1:nloc,2) - &
                                        1.0_pr/(d2u(4,iloc(1:nloc),2)-1.0_pr)*charX(1:nloc,3) - &
                                        SUM(pres_prev(iloc(1:nloc),3:dim+1)*du_diag_local(iloc(1:nloc),2:dim),DIM=2)*d2u(1,iloc(1:nloc),1) - hdiv(1:nloc,1)/2.0_pr * &
                                        ( (charI(1:nloc,1)+charI(1:nloc,2))*(1.0_pr+d2u(1,iloc(1:nloc),1)) + &
                                          (charI(1:nloc,3)+charI(1:nloc,4))*(ulc_prev(iloc(1:nloc),neng)+pres_prev(iloc(1:nloc),1)) + &
                                          (charI(1:nloc,2)-charI(1:nloc,1))*ulc_prev(iloc(1:nloc),nvel(1))*d2u(1,iloc(1:nloc),2) + &
                                          (charI(1:nloc,4)-charI(1:nloc,3))*ulc_prev(iloc(1:nloc),nvel(1))*d2u(3,iloc(1:nloc),2) ) 
                   ELSE IF (jj>=nspc_lil .AND. jj<=nspc_big) THEN
                      d2u(2,iloc(1:nloc),1) = 0.0_pr !u_diag
                      d2u(2,iloc(1:nloc),2) = (d2u_diag_local(iloc(1:nloc),1)/MW_in(jj-nspc(1)+1) - d2u_diag_local(iloc(1:nloc),2)*cp_in(jj-nspc(1)+1)) / &
                                                    ulc_prev(iloc(1:nloc),nden)/(d2u_diag_local(iloc(1:nloc),1)-d2u_diag_local(iloc(1:nloc),2))**2  !gamma_diag
                      d2u(1,iloc(1:nloc),1) = (ulc_prev(iloc(1:nloc),neng) - 0.5_pr*SUM(ulc_prev(iloc(1:nloc),nvel(1):nvel(dim))**2,DIM=2) / &
                                                    ulc_prev(iloc(1:nloc),nden))*d2u(2,iloc(1:nloc),2) !p_diag
                      d2u(1,iloc(1:nloc),2) = 0.5_pr/d2u(3,iloc(1:nloc),2)/ulc_prev(iloc(1:nloc),nden) * &
                                                    (d2u(2,iloc(1:nloc),2)*pres_prev(iloc(1:nloc),1)+d2u(4,iloc(1:nloc),2)*d2u(1,iloc(1:nloc),1)) !c_diag

                      charI(1:nloc,3) = charI(1:nloc,3) + (SIGN(1.0_pr,faceval*(pres_prev(iloc(1:nloc),2) + d2u(3,iloc(1:nloc),2) ) )+1.0_pr)/2.0_pr * &
                                      ( (pres_prev(iloc(1:nloc),2)+d2u(3,iloc(1:nloc),2))*(du_diag_local(iloc(1:nloc),1)*d2u(1,iloc(1:nloc),1) + &
                                        du(2,iloc(1:nloc),1)*ulc_prev(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)) + &
                                        d2u(1,iloc(1:nloc),2)*(du(1,iloc(1:nloc),1)+ulc_prev(iloc(1:nloc),nden)*d2u(3,iloc(1:nloc),2)*du(2,iloc(1:nloc),1)) )
                      charI(1:nloc,1) = (SIGN(1.0_pr,faceval*(pres_prev(iloc(1:nloc),2) - d2u(3,iloc(1:nloc),2) ) )+1.0_pr)/2.0_pr * &
                                      ( (pres_prev(iloc(1:nloc),2)-d2u(3,iloc(1:nloc),2))*(du_diag_local(iloc(1:nloc),1)*d2u(1,iloc(1:nloc),1) - &
                                        du(2,iloc(1:nloc),1)*ulc_prev(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)) - &
                                        d2u(1,iloc(1:nloc),2)*(du(1,iloc(1:nloc),1)-ulc_prev(iloc(1:nloc),nden)*d2u(3,iloc(1:nloc),2)*du(2,iloc(1:nloc),1)) )
                      charI(1:nloc,2) = faceval*MAX(0.0_pr,faceval*pres_prev(iloc(1:nloc),2))*(2.0_pr*d2u(3,iloc(1:nloc),2)*d2u(1,iloc(1:nloc),2)*du(locnvar,iloc(1:nloc),1) - &
                                                                   du_diag_local(iloc(1:nloc),1)*d2u(1,iloc(1:nloc),1))
                      DO ii=2,dim
                         charI(1:nloc,3) = charI(1:nloc,3) + (SIGN(1.0_pr,faceval*(pres_prev(iloc(1:nloc),2) + d2u(3,iloc(1:nloc),2) ) )+1.0_pr)/2.0_pr * &
                                      ( pres_prev(iloc(1:nloc),1+ii)*(du_diag_local(iloc(1:nloc),ii)*d2u(1,iloc(1:nloc),1) + &
                                        du(2,iloc(1:nloc),ii)*ulc_prev(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)) )
                         charI(1:nloc,1) = charI(1:nloc,1) + (SIGN(1.0_pr,faceval*(pres_prev(iloc(1:nloc),2) - d2u(3,iloc(1:nloc),2) ) )+1.0_pr)/2.0_pr * &
                                      ( pres_prev(iloc(1:nloc),1+ii)*(du_diag_local(iloc(1:nloc),ii)*d2u(1,iloc(1:nloc),1) - &
                                        du(2,iloc(1:nloc),ii)*ulc_prev(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)) )
                         charI(1:nloc,2) = charI(1:nloc,2) + (SIGN(1.0_pr,faceval*pres_prev(iloc(1:nloc),2) )+1.0_pr)/2.0_pr * &
                                      ( pres_prev(iloc(1:nloc),1+ii)*(2.0_pr*d2u(3,iloc(1:nloc),2)*d2u(1,iloc(1:nloc),2)*du(locnvar,iloc(1:nloc),ii) - &
                                        du_diag_local(iloc(1:nloc),ii)*d2u(1,iloc(1:nloc),1)) )
                      END DO
                      charI(1:nloc,4) = (SIGN(1.0_pr,faceval*pres_prev(iloc(1:nloc),2) )+1.0_pr)/2.0_pr * &
                                        SUM(pres_prev(iloc(1:nloc),2:dim+1)*du_diag_local(iloc(1:nloc),1:dim),DIM=2)/ulc_prev(iloc(1:nloc),nden)
                      IF (pertchar) THEN   
                         charI(1:nloc,3) = charI(1:nloc,3) + (SIGN(1.0_pr,faceval*(pres_prev(iloc(1:nloc),2) + d2u(3,iloc(1:nloc),2) ) )+1.0_pr)/2.0_pr * &
                                                               d2u(1,iloc(1:nloc),2) * rhoYg(faceint)
                         charI(1:nloc,1) = charI(1:nloc,1) - (SIGN(1.0_pr,faceval*(pres_prev(iloc(1:nloc),2) - d2u(3,iloc(1:nloc),2) ) )+1.0_pr)/2.0_pr * &
                                                               d2u(1,iloc(1:nloc),2) * rhoYg(faceint)
                         charI(1:nloc,2) = charI(1:nloc,2) - (SIGN(1.0_pr,faceval*pres_prev(iloc(1:nloc),2) )+1.0_pr)/2.0_pr * &
                                                               pres_prev(iloc(1:nloc),2)*2.0_pr*d2u(3,iloc(1:nloc),2)*d2u(1,iloc(1:nloc),2)*drho0(faceint)
                      END IF

                      charX(1:nloc,1) = 0.5_pr/d2u(3,iloc(1:nloc),2)**2 * (charI(1:nloc,1)+charI(1:nloc,3)+2.0_pr*charI(1:nloc,2)) - &
                                                     2.0_pr*d2u(1,iloc(1:nloc),2)/d2u(3,iloc(1:nloc),2)*charX_prev(1:nloc,1)
                      charX(1:nloc,3) = charI(1:nloc,4)

                      IF( face(1) == 1  ) THEN                          ! Top
                         charI(1:nloc,1) = pres_prev(iloc(1:nloc),2)/d2u(3,iloc(1:nloc),2)  !beta-=u/c
                         charI(1:nloc,2) = 1.0_pr                                            !beta+=1
                         charI(1:nloc,3) = -pres_prev(iloc(1:nloc),2)/d2u(3,iloc(1:nloc),2)**2*d2u(1,iloc(1:nloc),2)  !beta-=u/c
                         charI(1:nloc,4) = 0.0_pr                                            !beta+=1
                      ELSE IF( face(1) == -1  ) THEN                    ! Bottom
                         charI(1:nloc,2) = pres_prev(iloc(1:nloc),2)/d2u(3,iloc(1:nloc),2)  !beta+=u/c
                         charI(1:nloc,1) = 1.0_pr                                            !beta-=1
                         charI(1:nloc,4) = -pres_prev(iloc(1:nloc),2)/d2u(3,iloc(1:nloc),2)**2*d2u(1,iloc(1:nloc),2)  !beta+=u/c
                         charI(1:nloc,3) = 0.0_pr                                            !beta-=1
                      END IF

                      Drhs_diag_local((jj-1)*mynloc+iloc(1:nloc)) = Drhs_diag_local((jj-1)*mynloc+iloc(1:nloc)) - (charX_prev(1:nloc,1)+ulc_prev(iloc(1:nloc),jj)*charX(1:nloc,1)) / &
                                                     ulc_prev(iloc(1:nloc),nden) - ulc_prev(iloc(1:nloc),nden)*charX(1:nloc,3) - hdiv(1:nloc,1)/2.0_pr * &
                                                    (charI(1:nloc,1)+charI(1:nloc,2) + (charI(1:nloc,3)+charI(1:nloc,4))*ulc_prev(iloc(1:nloc),jj))
                   END IF 
                END DO
                IF (ALLOCATED(charI)) DEALLOCATE(charI)
                IF (ALLOCATED(charX)) DEALLOCATE(charX)
                IF (ALLOCATED(charI_prev)) DEALLOCATE(charI_prev)
                IF (ALLOCATED(charX_prev)) DEALLOCATE(charX_prev)
             END IF
          END IF
       END IF
    END DO
  END SUBROUTINE user_evol_liu_BC_Drhs_diag 

  SUBROUTINE user_convectzone_BC_rhs (myrhs_loc, ulc, myne_loc, mynloc, myjl, mymeth)
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: myne_loc, mynloc, myjl, mymeth
    REAL (pr), DIMENSION (mynloc*myne_loc), INTENT (INOUT) :: myrhs_loc
    REAL (pr), DIMENSION (mynloc,myne_loc), INTENT (IN) :: ulc

    INTEGER :: ie, i, j, shift
    REAL (pr), DIMENSION (myne_loc,mynloc,dim) :: du, d2u
    REAL (pr), DIMENSION (mynloc,2*dim) :: bigU
    REAL (pr), DIMENSION (mynloc,myne_loc) :: myu_zer
          
!!$    CALL c_diff_fast(myu_zer, du, d2u, myjl, mynloc, mymeth, 10, myne_loc, 1, myne_loc) 
    CALL c_diff_fast(ulc, du, d2u, myjl, mynloc, mymeth, 10, myne_loc, 1, myne_loc) 

    CALL user_bufferfunc(bigU, mynloc, buffcvd, polyconv)

    DO j = 1,dim
       DO ie=1,myne_loc
          shift = (ie-1)*mynloc   
!!$       myrhs_loc(shift+1:shift+mynloc) = myrhs_loc(shift+1:shift+mynloc) - (bigU(:,2)*c0(1)-bigU(:,1)*c0(2))*buffU0*du(ie,:,1)
          myrhs_loc(shift+1:shift+mynloc) = myrhs_loc(shift+1:shift+mynloc) - ( bigU(:,2*j-1)+bigU(:,2*j) )*SUM(c0)/SIZE(c0)*buffU0*du(ie,:,j) 
       END DO
    END DO

  END SUBROUTINE user_convectzone_BC_rhs 

  SUBROUTINE user_convectzone_BC_Drhs (Dmyrhs_loc, ulc, ulc_prev, myne_loc, mynloc, myjl, mymeth)
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: myne_loc, mynloc, myjl, mymeth
    REAL (pr), DIMENSION (mynloc*myne_loc), INTENT (INOUT) :: Dmyrhs_loc
    REAL (pr), DIMENSION (mynloc,myne_loc), INTENT (IN) :: ulc, ulc_prev

    INTEGER :: ie, i, j, shift
    REAL (pr), DIMENSION (myne_loc,mynloc,dim) :: du, d2u
    REAL (pr), DIMENSION (mynloc,2*dim) :: bigU

    CALL c_diff_fast(ulc, du, d2u, myjl, mynloc, mymeth, 10, myne_loc, 1, myne_loc) 
    CALL user_bufferfunc(bigU, mynloc, buffcvd, polyconv)

    DO j = 1,dim
       DO ie=1,myne_loc
          shift = (ie-1)*mynloc   
!!$       Dmyrhs_loc(shift+1:shift+mynloc) = Dmyrhs_loc(shift+1:shift+mynloc) - (bigU(:,2)*c0(1)-bigU(:,1)*c0(2))*buffU0*du(ie,:,1)
          Dmyrhs_loc(shift+1:shift+mynloc) = Dmyrhs_loc(shift+1:shift+mynloc) - ( bigU(:,2*j-1)+bigU(:,2*j) )*SUM(c0)/SIZE(c0)*buffU0*du(ie,:,j)   
       END DO
    END DO
  END SUBROUTINE user_convectzone_BC_Drhs 

  SUBROUTINE user_convectzone_BC_Drhs_diag (Drhs_diag_local, ulc_prev, myne_loc, mynloc, myjl, mymeth)
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: myne_loc, mynloc, myjl, mymeth
    REAL (pr), DIMENSION (mynloc*myne_loc), INTENT (INOUT) :: Drhs_diag_local
    REAL (pr), DIMENSION (mynloc,myne_loc), INTENT (IN) :: ulc_prev

    REAL (pr), DIMENSION (mynloc,dim) :: du_diag_local, d2u_diag_local
    INTEGER :: ie, i,j, shift
    REAL (pr), DIMENSION (mynloc,2*dim) :: bigU

    CALL c_diff_diag(du_diag_local, d2u_diag_local, j_lev, mynloc, mymeth, mymeth, 10)
    CALL user_bufferfunc(bigU, mynloc, buffcvd, polyconv)


    DO j = 1,dim
       DO ie=1,myne_loc
          shift = (ie-1)*mynloc   
!!$          Drhs_diag_local(shift+1:shift+mynloc) = Drhs_diag_local(shift+1:shift+mynloc) - (bigU(:,2)*c0(1)-bigU(:,1)*c0(2))*buffU0*du_diag_local(:,1)
          Drhs_diag_local(shift+1:shift+mynloc) = Drhs_diag_local(shift+1:shift+mynloc) - ( bigU(:,2*j-1) + bigU(:,2*j) )*SUM(c0)/SIZE(c0)*buffU0*du_diag_local(:,j) 
       END DO
    END DO
  END SUBROUTINE user_convectzone_BC_Drhs_diag 

  SUBROUTINE user_bufferfunc (bigU, mynloc, buffd,locpoly)
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: mynloc
    REAL (pr), DIMENSION (mynloc,2*dim), INTENT(INOUT) :: bigU
    REAL (pr), DIMENSION (5), INTENT(IN) :: buffd
    LOGICAL, INTENT(IN) :: locpoly

    REAL (pr), DIMENSION (mynloc,2*dim) :: bigU2
    INTEGER :: ie, i, j, shift, stri, stpi
    REAL (pr) :: topbnd, botbnd, buffsize, maxu, minu

    bigU(:,:)=0.0_pr
    bigU2(:,:)=0.0_pr

!Establish buffer functions for each direction - boundary order for bigU: xmin, xmax, ymin, ymax, zmin, zmax
    DO j = 1,dim
       buffsize=buffd(2)
       botbnd=xyzlimits(1,j)+buffd(1)
       topbnd=xyzlimits(2,j)-buffd(1)
       IF (buffsize>0.0_pr) THEN
          IF (locpoly) THEN
             bigU(:,2*j-1)=((botbnd-MIN(botbnd,MAX(botbnd-buffd(2),x(:,j))))/buffsize)**buffBeta
             bigU(:,2*j  )=((MAX(topbnd,MIN(topbnd+buffd(2),x(:,j)))-topbnd)/buffsize)**buffBeta
          ELSE
             bigU(:,2*j-1)=(TANH(((botbnd-MIN(botbnd,MAX(botbnd-buffd(2),x(:,j))))/buffsize - buffOff)*buffFac) + 1.0_pr)/2.0_pr
             bigU(:,2*j  )=(TANH(((MAX(topbnd,MIN(topbnd+buffd(2),x(:,j)))-topbnd)/buffsize - buffOff)*buffFac) + 1.0_pr)/2.0_pr
          END IF
          maxu=MAXVAL(bigU(:,2*j-1))
          minu=MINVAL(bigU(:,2*j-1))
          CALL parallel_global_sum (REALMAXVAL=maxu)
          CALL parallel_global_sum (REALMINVAL=minu)
          bigU(:,2*j-1)=(bigU(:,2*j-1)-minu)/(maxu-minu)
          maxu=MAXVAL(bigU(:,2*j))
          minu=MINVAL(bigU(:,2*j))
          CALL parallel_global_sum (REALMAXVAL=maxu)
          CALL parallel_global_sum (REALMINVAL=minu)
          bigU(:,2*j)=(bigU(:,2*j)-minu)/(maxu-minu)
       ELSEIF (botbnd>xyzlimits(1,j)) THEN
          bigU(:,2*j-1:2*j)=1.0_pr
          bigU(:,2*j-1)=(SIGN(bigU(:,j  ),botbnd-x(:,j)) + 1.0_pr)/2.0_pr
          bigU(:,2*j  )=(SIGN(bigU(:,j+1),x(:,j)-topbnd) + 1.0_pr)/2.0_pr 
       END IF
       buffsize=buffd(4)
       botbnd=xyzlimits(1,j)+(buffd(1)-buffd(2)-buffd(3))
       topbnd=xyzlimits(2,j)-(buffd(1)-buffd(2)-buffd(3))
       IF (buffsize>0.0_pr) THEN
          IF (locpoly) THEN
             bigU2(:,2*j-1)=((buffd(4)-(botbnd-MIN(botbnd,MAX(botbnd-buffd(4),x(:,1*j)))))/buffsize)**buffBeta - 1.0_pr
             bigU2(:,2*j  )=((buffd(4)-(MAX(topbnd,MIN(topbnd+buffd(4),x(:,1*j)))-topbnd))/buffsize)**buffBeta - 1.0_pr
          ELSE
             bigU2(:,2*j-1)=(TANH(-((botbnd-MIN(botbnd,MAX(botbnd-buffd(4),x(:,1*j))))/buffsize - buffOff)*buffFac) - 1.0_pr)/2.0_pr
             bigU2(:,2*j  )=(TANH(-((MAX(topbnd,MIN(topbnd+buffd(4),x(:,1*j)))-topbnd)/buffsize - buffOff)*buffFac) - 1.0_pr)/2.0_pr
          END IF
          maxu=MAXVAL(bigU2(:,2*j-1))
          minu=MINVAL(bigU2(:,2*j-1))
          CALL parallel_global_sum (REALMAXVAL=maxu)
          CALL parallel_global_sum (REALMINVAL=minu)
          bigU2(:,2*j-1)=(bigU2(:,2*j-1)-minu)/(maxu-minu)-1.0_pr
          maxu=MAXVAL(bigU2(:,2*j))
          minu=MINVAL(bigU2(:,2*j))
          CALL parallel_global_sum (REALMAXVAL=maxu)
          CALL parallel_global_sum (REALMINVAL=minu)
          bigU2(:,2*j)=(bigU2(:,2*j)-minu)/(maxu-minu)-1.0_pr
       ELSEIF (botbnd>xyzlimits(1,j)) THEN
          bigU2(:,2*j-1:2*j)=1.0_pr
          bigU2(:,2*j-1)=-(SIGN(bigU2(:,j  ),botbnd-x(:,1*j)) + 1.0_pr)/2.0_pr
          bigU2(:,2*j  )=-(SIGN(bigU2(:,j+1),x(:,1*j)-topbnd) + 1.0_pr)/2.0_pr 
       END IF
       bigU(:,2*j-1:2*j) = bigU(:,2*j-1:2*j) + bigU2(:,2*j-1:2*j)


    END DO



  END SUBROUTINE user_bufferfunc 

  SUBROUTINE user_bndfunc (bndonly, mynloc, myjl)
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: mynloc, myjl
    REAL (pr), DIMENSION (mynloc), INTENT(INOUT) :: bndonly
    INTEGER :: i
    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc
    bndonly(:) = 0.0_pr
    i_p_face(0) = 1
    DO i=1,dim
       i_p_face(i) = i_p_face(i-1)*3
    END DO
    DO face_type = 0, 3**dim - 1
       face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
       IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
          CALL get_all_indices_by_face (face_type, myjl, nloc, iloc)
          IF(nloc > 0 ) THEN
             IF (ABS(face(1)) == 1) THEN
                bndonly(iloc(1:nloc)) = 1.0_pr
             END IF
          END IF
       END IF
    END DO
  END SUBROUTINE user_bndfunc 

  SUBROUTINE user_flxfunc (flxfunc, mynloc, myjl)
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: mynloc, myjl
    REAL (pr), DIMENSION (dim,mynloc,dim), INTENT(INOUT) :: flxfunc
    REAL (pr), DIMENSION (mynloc) :: myflxfunc
    INTEGER :: i,j
       CALL user_bndfunc(myflxfunc, mynloc, myjl)  !puts 1.0 on boundaries, 0.0 elsewhere 
       DO j=1,dim
          DO i=1,dim
             !IF ((i.ne.j) .AND. (MIN(i,j).eq.1)) THEN
             IF ((i.ne.j) .AND. (j.eq.1)) THEN
                IF (globFlux==1) THEN
                   flxfunc(i,:,j) = 0.0_pr
                ELSE
                   flxfunc(i,:,j) = 1.0_pr - myflxfunc(:)
                END IF
             ELSE
                flxfunc(i,:,j) = 1.0_pr
             END IF
          END DO
       END DO
  END SUBROUTINE user_flxfunc

  SUBROUTINE user_bufferRe (ReU, mynloc)
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: mynloc
    REAL (pr), DIMENSION (mynloc), INTENT(INOUT) :: ReU
    REAL (pr), DIMENSION (mynloc,2*dim) :: bigU
    REAL (pr), DIMENSION (mynloc) :: buffmask

    IF (dobuffRe) THEN

       CALL user_bufferfunc(bigU,mynloc,buffRed,polybuffRe)
       buffmask=SUM(bigU(:,:),2)
       ReU(:) = 1.0_pr + (Re/Rebnd-1.0_pr)*buffmask(:)
    ELSE
       ReU(:) = 1.0_pr
    END IF
  END SUBROUTINE user_bufferRe 

  FUNCTION user_chi (nlocal, t_local)
    IMPLICIT NONE
    INTEGER,          INTENT (IN) :: nlocal
    REAL (pr),        INTENT (IN) :: t_local
    REAL (pr), DIMENSION (nlocal) :: user_chi
    INTEGER :: idim
    REAL (pr) :: pi

    IF (.NOT.use_dist) THEN
       CALL user_dist (nlocal, t_local, DISTANCE=user_chi)
    ELSE
       user_chi = dist
    END IF

!!$    user_chi = 0.5_pr*(1.0_pr + TANH(100.0_pr*user_chi))

    WHERE(user_chi >= 0.0_pr)
       user_chi = 1.0_pr
    ELSEWHERE
       user_chi = 0.0_pr
    END WHERE

  END FUNCTION user_chi

  SUBROUTINE user_bufferzone_BC_rhs (myrhs_loc, ulc, myne_loc, mynloc, myjl, mymeth)
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: myne_loc, mynloc, myjl, mymeth
    REAL (pr), DIMENSION (mynloc*myne_loc), INTENT (INOUT) :: myrhs_loc
    REAL (pr), DIMENSION (mynloc,myne_loc), INTENT (IN) :: ulc

    INTEGER :: ie, i, j, shift, stri, stpi
    REAL (pr), DIMENSION (mynloc,2*dim) :: bigU

    CALL user_bufferfunc(bigU, mynloc, buffbfd, polybuff)

!!$    stri=1
!!$    stpi=myne_loc
!!$    IF (pBrink) THEN
!!$       stri=neng
!!$       stpi=neng
!!$    END IF

    DO j = 1,dim
       DO ie=1,myne_loc
          shift = (ie-1)*mynloc   
          myrhs_loc(shift+1:shift+mynloc) = myrhs_loc(shift+1:shift+mynloc) - buffSig*((bigU(:,2*j)+bigU(:,2*j-1))*ulc(:,ie)-bigU(:,2*j-1)*bndvals(ie,2*j-1)-bigU(:,2*j)*bndvals(ie,2*j)) 
       END DO
    END DO



  END SUBROUTINE user_bufferzone_BC_rhs 

  SUBROUTINE user_bufferzone_BC_Drhs (Dmyrhs_loc, ulc, ulc_prev, myne_loc, mynloc, myjl, mymeth)
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: myne_loc, mynloc, myjl, mymeth
    REAL (pr), DIMENSION (mynloc*myne_loc), INTENT (INOUT) :: Dmyrhs_loc
    REAL (pr), DIMENSION (mynloc,myne_loc), INTENT (IN) :: ulc, ulc_prev

    INTEGER :: ie, i, j, shift, stri, stpi
    REAL (pr), DIMENSION (mynloc,2*dim) :: bigU

    CALL user_bufferfunc(bigU, mynloc, buffbfd, polybuff)

!!$    stri=1
!!$    stpi=myne_loc
!!$    IF (pBrink) THEN
!!$       stri=neng
!!$       stpi=neng
!!$    END IF
    DO j = 1,dim
       DO ie = 1,myne_loc
          shift = (ie-1)*mynloc   
          Dmyrhs_loc(shift+1:shift+mynloc) = Dmyrhs_loc(shift+1:shift+mynloc) - buffSig*(bigU(:,2*j)+bigU(:,2*j-1))*ulc(:,ie)
       END DO
    END DO

  END SUBROUTINE user_bufferzone_BC_Drhs 

  SUBROUTINE user_bufferzone_BC_Drhs_diag (Drhs_diag_local, ulc_prev, myne_loc, mynloc, myjl, mymeth)
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: myne_loc, mynloc, myjl, mymeth
    REAL (pr), DIMENSION (mynloc*myne_loc), INTENT (INOUT) :: Drhs_diag_local
    REAL (pr), DIMENSION (mynloc,myne_loc), INTENT (IN) :: ulc_prev

    INTEGER :: ie, i, j, shift, stri, stpi
    REAL (pr), DIMENSION (mynloc,2*dim) :: bigU

    CALL user_bufferfunc(bigU, mynloc, buffbfd, polybuff)

!!$    stri=1
!!$    stpi=myne_loc
!!$    IF (pBrink) THEN
!!$       stri=neng
!!$       stpi=neng
!!$    END IF

    DO j = 1,dim
       DO ie=1,myne_loc
          shift = (ie-1)*mynloc   
          Drhs_diag_local(shift+1:shift+mynloc) = Drhs_diag_local(shift+1:shift+mynloc) - buffSig*(bigU(:,2*j)+bigU(:,2*j-1))
       END DO
    END DO
  END SUBROUTINE user_bufferzone_BC_Drhs_diag 

  SUBROUTINE internal_rhs (int_rhs,u_integrated,doBC,addingon)
    USE penalization
    IMPLICIT NONE
    REAL (pr), DIMENSION (ng,ne), INTENT(IN) :: u_integrated !1D flat version of integrated variables without BC 
!!$    REAL (pr), DIMENSION (ng,ne) :: u_tmp !1D flat version of integrated variables without BC 
    REAL (pr), DIMENSION (ng*ne), INTENT(INOUT) :: int_rhs
    INTEGER, INTENT(IN) :: doBC !if zero do full rhs, if one no x-derivatives 
    INTEGER, INTENT(IN) :: addingon 

    INTEGER :: i, j, l, ie, shift, zeroFlux, tempintd, jshift
    INTEGER, PARAMETER :: meth=1
    REAL (pr), DIMENSION (ne*dim,ng,dim) :: du, d2u
    REAL (pr), DIMENSION (ng,ne*dim) :: F
    REAL (pr), DIMENSION (ng,dim+Nspec) :: v ! velocity components + temperature + Y
    REAL (pr) :: usej
    REAL (pr), DIMENSION (ng) :: p
    REAL (pr), DIMENSION (ng,3) :: props
    REAL (pr), DIMENSION (ng) :: ReU

    INTEGER :: idim

    zeroFlux = 0
    IF (dozeroFlux .AND. (doBC+addingon)>0) zeroFlux = dervFlux + 1

    CALL user_bufferRe (ReU, ng)

    int_rhs = 0.0_pr
    DO l = 1,Nspecm
       v(:,dim+1+l) = u_integrated(:,nspc(l))/u_integrated(:,nden)
    END DO
    p(:) = (1.0_pr-SUM(v(:,dim+2:dim+Nspec),DIM=2))  !Y_Nspec
    props(:,1) = p(:)*cp_in(Nspec) !cp_Nspec
    props(:,2) = p(:)/MW_in(Nspec) !R_Nspec
    DO l=1,Nspecm
       props(:,1) = props(:,1) + v(:,dim+1+l)*cp_in(l) !cp
       props(:,2) = props(:,2) + v(:,dim+1+l)/MW_in(l) !R
    END DO
    p(:) = (u_integrated(:,neng)-0.5_pr*SUM(u_integrated(:,nvel(1):nvel(dim))**2,DIM=2)/u_integrated(:,nden))/(props(:,1)/props(:,2)-1.0_pr)  !pressure
    v(:,dim+1) = p(:)/props(:,2)/u_integrated(:,nden)  !Temperature
    DO i = 1, dim
       v(:,i) = u_integrated(:,nvel(i)) / u_integrated(:,nden) ! u_i
    END DO
    IF ((doBC .eq. 0) .AND. ((methpd .ne. 0) .OR. splitFBpress)) THEN  !IF doBC>0, these terms are done inside LODI subroutines
       du(1,:,1) = p(:)
       IF (Nspec>1) THEN
          CALL dpdx (du(1,:,1),ng,v(:,dim+2:dim+Nspec),2*methpd+meth,j_lev,splitFBpress)
       ELSE
          CALL dpdx (du(1,:,1),ng,v(:,1),2*methpd+meth,j_lev,.FALSE.)
       END IF
       shift = (nvel(1)-1)*ng   
       int_rhs(shift+1:shift+ng) = int_rhs(shift+1:shift+ng)-du(1,:,1)
       shift = (neng-1)*ng   
       int_rhs(shift+1:shift+ng) = int_rhs(shift+1:shift+ng)-du(1,:,1)*v(:,1) !u*dp/dx
       CALL c_diff_fast(v(:,1), du(1,:,:), d2u(1,:,:), j_lev, ng, meth, 10, 1, 1, 1) ! du/dx
       int_rhs(shift+1:shift+ng) = int_rhs(shift+1:shift+ng)-du(1,:,1)*p(:)   !p*du/dx
    END IF
    !
    !--Form right hand side of Euler Equations
    !
    F(:,:) = 0.0_pr
    DO j = 1+doBC, dim
       jshift=ne*(j-1)
       F(:,nden+jshift) = F(:,nden+jshift) + u_integrated(:,nvel(j)) ! rho*u_j
       DO i = 1, dim
          F(:,nvel(i)+jshift) = F(:,nvel(i)+jshift) + u_integrated(:,nvel(i))*v(:,j) ! rho*u_i*u_j
       END DO
       F(:,nvel(j)+jshift) = F(:,nvel(j)+jshift) + p(:) ! rho*u_i^2+p
       F(:,neng+jshift) = F(:,neng+jshift) + ( u_integrated(:,neng) + p(:) )*v(:,j) ! (rho*e+p)*u_j
       DO l=1,Nspecm
          F(:,nspc(l)+jshift) = F(:,nspc(l)+jshift) + v(:,dim+1+l)*u_integrated(:,nvel(j)) ! rho*Y*u_j
       END DO  
    END DO
    IF ((doBC .eq. 0) .AND. ((methpd .ne. 0) .OR. splitFBpress)) THEN  !IF doBC>0, these terms are done inside LODI subroutines
       F(:,nvel(1)) = F(:,nvel(1)) - p(:)
       F(:,neng)    = F(:,neng)    - p(:)*v(:,1)
    END IF
    IF (doBC .ne. 0) THEN  !Missing term from LODI assumps... gamma=constant
       CALL c_diff_fast(props(:,1)/props(:,2), du(1,:,:), d2u(1,:,:), j_lev, ng, meth, 10, 1, 1, 1)  !d(gamma/(gamma-1))/dx
       shift = (neng-1)*ng   
       int_rhs(shift+1:shift+ng) = int_rhs(shift+1:shift+ng)-du(1,:,1)*v(:,1)*p(:) !pu*d(gamma/(gamma-1))/dx
    END IF
    !
    !-- Brinkman penalization (Convective Term Only)
    !
    IF (imask_obstacle) THEN
       int_rhs(1:ng) = (1.0_pr + penal*(1.0_pr/phi - 1.0_pr))*int_rhs(1:ng)       ! cont eq penalization
       shift = ng*(neng - 1)                                   ! momentum penalization
       int_rhs(shift + 1:shift + ng) = (1.0_pr + penal*(1.0_pr/phi - 1.0_pr))*int_rhs(shift + 1:shift + ng) ! energy penalization
    END IF
    !
    !-- Form right hand side of Navier-Stokes Equations
    !
    IF(NS)THEN 
       CALL c_diff_fast(v, du(1:dim+Nspec,:,:), d2u(1:dim+Nspec,:,:), j_lev, ng, meth, 10, dim+Nspec, 1,dim+Nspec) ! du_i/dx_j & dT/dx_j & dY_l/dx_j
       p(:)=1.0_pr
       IF (Nspec>1) p(:)=p(:)-SUM(v(:,dim+2:dim+Nspec),DIM=2)
       props(:,1)=p(:)*mu_in(Nspec)  !mu_Nspec
       props(:,2)=p(:)*kk_in(Nspec)  !kk_Nspec
       props(:,3)=p(:)*bD_in(Nspec)  !bD_Nspec
       DO l=1,Nspecm
          props(:,1)=props(:,1)+v(:,dim+1+l)*mu_in(l)  !mu
          props(:,2)=props(:,2)+v(:,dim+1+l)*kk_in(l)  !kk
          props(:,3)=props(:,3)+v(:,dim+1+l)*bD_in(l)  !bD
       END DO
       p(:) = du(1,:,1) !reuse of a variable
       DO i = 2, dim
          p(:) = p(:) + du(i,:,i) !du_i/dx_i
       END DO
       IF (zeroFlux==1) THEN
          CALL user_flxfunc (d2u(1:dim,:,1:dim), ng, j_lev)
       ELSE
          d2u(:,:,:) = 1.0_pr
       END IF
       DO j = 1, dim
          jshift=ne*(j-1)
          DO i = 1, dim
             F(:,nvel(i)+jshift) = F(:,nvel(i)+jshift) - ReU(:)*props(:,1)*( du(i,:,j)+du(j,:,i) ) * d2u(i,:,j)  !rho*u_i*u_j - 2*mu*S_ij/Re  
          END DO
          F(:,nvel(j)+jshift) = F(:,nvel(j)+jshift) + 2.0_pr/3.0_pr*ReU(:)*props(:,1)*p(:) * d2u(j,:,j)  !rho*u_i*u_j - tau_ij/Re  [ tau_ij = 2*mu*( S_ij - 2/3*S_kk*delta_ij ) ] 
          DO i = 1, dim
             F(:,neng+jshift) =  F(:,neng+jshift) - props(:,1)*ReU(:)*( du(i,:,j)+du(j,:,i) )*v(:,i) * d2u(i,:,j)  !(e+p)*u_j - u_i*2*mu*S_ij/Re 
          END DO
          F(:,neng+jshift) =  F(:,neng+jshift) + 2.0_pr/3.0_pr*props(:,1)*ReU(:)*p(:)*v(:,j) * d2u(j,:,j) - ReU(:)*props(:,2)*du(dim+1,:,j) * d2u(2,:,j)  
                                                                                  !(e+p)*u_j - u_i*tau_ij/Re - q_j/Re/Pra [ q_j = mu dT/dx_j ]
          IF (Nspec>1) THEN
             DO l=1,Nspecm
                F(:,neng+jshift) =  F(:,neng+jshift) - u_integrated(:,nden)*cp_in(l)*v(:,dim+1)*ReU(:)*props(:,3)*du(dim+1+l,:,j) * d2u(2,:,j)
             END DO
             F(:,neng+jshift) =  F(:,neng+jshift) +  u_integrated(:,nden)*cp_in(Nspec)*v(:,dim+1)*ReU(:)*props(:,3)*SUM(du(dim+2:dim+Nspec,:,j),DIM=1) * d2u(2,:,j)
             DO l=1,Nspecm
                F(:,nspc(l)+jshift) =  F(:,nspc(l)+jshift) - u_integrated(:,nden)*ReU(:)*props(:,3)*du(dim+1+l,:,j) * d2u(2,:,j)
             END DO
          END IF
       END DO
    END IF
    IF (GRAV) THEN    
      shift = (neng-1)*ng   
      DO j=1,dim
         DO l=1,Nspecm
            int_rhs(shift+1:shift+ng) = int_rhs(shift+1:shift+ng)-v(:,dim+1+l)*u_integrated(:,nvel(j))*gr_in(l+Nspec*(j-1))
         END DO
         IF (Nspec>1) THEN
            int_rhs(shift+1:shift+ng) = int_rhs(shift+1:shift+ng) - (1.0_pr-SUM(v(:,dim+2:dim+Nspec),DIM=2))*u_integrated(:,nvel(j))*gr_in(Nspec*j)
         ELSE
            int_rhs(shift+1:shift+ng) = int_rhs(shift+1:shift+ng) - u_integrated(:,nvel(j))*gr_in(Nspec*j)
         END IF
      END DO
    END IF
    tempintd=ne*dim
    CALL c_diff_fast(F(:,1:tempintd), du(1:tempintd,:,:), d2u(1:tempintd,:,:), j_lev, ng, meth, 10, tempintd, 1, tempintd)  !du(i,:,k)=dF_ij/dx_k
    DO j = 1, dim
       jshift=ne*(j-1)
       DO i = 1, dim+1+Nspec
          shift=(i-1)*ng
          int_rhs(shift+1:shift+ng) = int_rhs(shift+1:shift+ng) - du(i+jshift,:,j)  ! -dF_ij/dx_j
       END DO
       IF (GRAV) THEN      
          shift = (nvel(j)-1)*ng
          
          IF (Nspec>1) THEN
             DO l=1,Nspecm
                int_rhs(shift+1:shift+ng) = int_rhs(shift+1:shift+ng) - gr_in(l+Nspec*(j-1))*u_integrated(:,nspc(l))
             END DO
          END IF
          IF (Nspec>1) THEN
             int_rhs(shift+1:shift+ng) = int_rhs(shift+1:shift+ng) - gr_in(Nspec*j)*(u_integrated(:,nden)-SUM(u_integrated(:,nspc(1):nspc(Nspecm)),DIM=2))
          ELSE
             int_rhs(shift+1:shift+ng) = int_rhs(shift+1:shift+ng) - gr_in(Nspec*j)*u_integrated(:,nden)
          END IF
       END IF
    END DO
    IF (NS .AND. zeroFlux==2) THEN
       CALL c_diff_fast(v, du(1:dim+Nspec,:,:), d2u(1:dim+Nspec,:,:), j_lev, ng, meth, 10, dim+Nspec, 1,dim+Nspec) ! du_i/dx_j & dT/dx_j & dY_l/dx_j
       DO i = 2, dim
          F(:,i-1) = ReU(:)*props(:,1)*( du(i,:,1)+du(1,:,i) )  !2*mu*S_ij/Re  
       END DO
       F(:,dim) = ReU(:)*props(:,2)*du(dim+1,:,1)  !q_j/Re/Pra [ q_j = mu dT/dx_j ]
       DO l=1,Nspecm
          F(:,dim+l) =  u_integrated(:,nden)*ReU(:)*props(:,3)*du(dim+1+l,:,1)
       END DO
       IF (Nspec>1) F(:,dim+Nspec) = -u_integrated(:,nden)*ReU(:)*props(:,3)*SUM(du(dim+2:dim+Nspec,:,1),DIM=1)
       CALL c_diff_fast(F(:,1:dim+Nspec), du(1:dim+Nspec,:,:), d2u(1:dim+Nspec,:,:), j_lev, ng, meth, 10, dim+Nspec, 1, dim+Nspec)  !du(i,:,k)=dF_ij/dx_k
       CALL user_flxfunc (d2u(1:dim,:,1:dim), ng, j_lev)
       d2u(1:dim,:,1:dim) = 1.0_pr - d2u(1:dim,:,1:dim)
       DO i=2,dim
          shift=(nvel(i)-1)*ng
          int_rhs(shift+1:shift+ng) = int_rhs(shift+1:shift+ng) - du(i-1,:,1)*d2u(2,:,1)
       END DO
       shift=(neng-1)*ng
       DO i=2,dim
          int_rhs(shift+1:shift+ng) = int_rhs(shift+1:shift+ng) - du(i-1,:,1)*v(:,i)*d2u(2,:,1)
       END DO
       int_rhs(shift+1:shift+ng) = int_rhs(shift+1:shift+ng) - du(dim,:,1)*d2u(2,:,1) 
       DO l=1,Nspec
          IF (Nspec>1) int_rhs(shift+1:shift+ng) = int_rhs(shift+1:shift+ng) - v(:,dim+1)*cp_in(l)*du(dim+l,:,1)*d2u(2,:,1)
       END DO
       DO l=1,Nspecm
          shift=(nspc(l)-1)*ng
          int_rhs(shift+1:shift+ng) = int_rhs(shift+1:shift+ng) - du(dim+l,:,1)*d2u(2,:,1) 
       END DO
    END IF
  END SUBROUTINE internal_rhs

  SUBROUTINE internal_Drhs (int_Drhs, u_p, u_prev, meth,doBC,addingon)
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: meth
    REAL (pr), DIMENSION (ng,ne), INTENT(IN) :: u_p, u_prev
    REAL (pr), DIMENSION (ng*ne), INTENT(INOUT) :: int_Drhs
    INTEGER, INTENT(IN) :: doBC !if zero do full rhs, if one no x-derivatives 
    INTEGER, INTENT(IN) :: addingon 

    INTEGER :: i, j, l, ie, shift, zeroFlux, vshift, jshift, tempintd
    REAL (pr), DIMENSION (ne*dim,ng,dim) :: du, d2u
    REAL (pr), DIMENSION (ng, dim) :: nu
    REAL (pr), DIMENSION (ng,ne*dim) :: F
    REAL (pr), DIMENSION (ng,(dim+Nspec)*2) :: v_prev ! velocity components + temperature
    REAL (pr), DIMENSION (ng) :: p, p_prev
    REAL (pr), DIMENSION (ng,3) :: props, props_prev
    REAL (pr), DIMENSION (ng) :: ReU

    REAL (pr) :: pi

    INTEGER :: idim

    zeroFlux = 0
    IF (dozeroFlux .AND. (doBC+addingon)>0) zeroFlux = dervFlux + 1

    CALL user_bufferRe (ReU, ng)

    int_Drhs = 0.0_pr
    vshift = dim+Nspec
    DO l=1,Nspecm
       v_prev(:,dim+1+l) = u_prev(:,nspc(l))/u_prev(:,nden)
       v_prev(:,vshift+dim+1+l) = u_p(:,nspc(l))/u_prev(:,nden) - u_prev(:,nspc(l))*u_p(:,nden)/u_prev(:,nden)**2
    END DO
    p_prev(:) = 1.0_pr
    p(:) = 0.0_pr
    IF (Nspec>1) p_prev(:) = (p_prev(:)-SUM(v_prev(:,dim+2:dim+Nspec),DIM=2))  !Y0_Nspec
    IF (Nspec>1) p(:)      =           -SUM(v_prev(:,vshift+dim+2:vshift+dim+Nspec),DIM=2)  !Y'_Nspec
    props_prev(:,1) = p_prev(:)*cp_in(Nspec) !cp0_Nspec
    props_prev(:,2) = p_prev(:)/MW_in(Nspec) !R0_Nspec
    props(:,1)      = p(:)*cp_in(Nspec) !cp'_Nspec
    props(:,2)      = p(:)/MW_in(Nspec) !R'_Nspec
    DO l=1,Nspecm
       props_prev(:,1) = props_prev(:,1) + v_prev(:,dim+1+l)*cp_in(l) !cp0
       props_prev(:,2) = props_prev(:,2) + v_prev(:,dim+1+l)/MW_in(l) !R0
       props(:,1)      = props(:,1)      + v_prev(:,vshift+dim+1+l)*cp_in(l) !cp'
       props(:,2)      = props(:,2)      + v_prev(:,vshift+dim+1+l)/MW_in(l) !R'
    END DO
    p_prev(:) = (u_prev(:,neng)-0.5_pr*SUM(u_prev(:,nvel(1):nvel(dim))**2,DIM=2)/u_prev(:,nden))/(props_prev(:,1)/props_prev(:,2)-1.0_pr)  !pressure
    p(:) = (u_prev(:,neng)-0.5_pr*SUM(u_prev(:,nvel(1):nvel(dim))**2,DIM=2)/u_prev(:,nden)) * (props_prev(:,1)*props(:,2)-props(:,1)*props_prev(:,2)) / (props_prev(:,1)-props_prev(:,2))**2 + &
           (u_p(:,neng)+0.5_pr*SUM(u_prev(:,nvel(1):nvel(dim))**2,DIM=2)/u_prev(:,nden)**2*u_p(:,nden)-SUM(u_prev(:,nvel(1):nvel(dim))*u_p(:,nvel(1):nvel(dim)),DIM=2)/u_prev(:,nden)) /  &
             (props_prev(:,1)/props_prev(:,2)-1.0_pr) 
    v_prev(:,dim+1) = p_prev(:)/props_prev(:,2)/u_prev(:,nden)  !Temperature
    v_prev(:,vshift+dim+1) = (p(:)-p_prev(:)*(props(:,2)/props_prev(:,2)+u_p(:,nden)/u_prev(:,nden)))/props_prev(:,2)/u_prev(:,nden)  !Temperature
    DO i = 1, dim
       v_prev(:,i) = u_prev(:,nvel(i)) / u_prev(:,nden) ! u_i
       v_prev(:,vshift+i) = u_p(:,nvel(i))/u_prev(:,nden) - u_prev(:,nvel(i))*u_p(:,nden)/u_prev(:,nden)**2  !u_i
    END DO
    IF ((doBC .eq. 0) .AND. ((methpd .ne. 0) .OR. splitFBpress)) THEN  !IF doBC>0, these terms are done inside LODI subroutines
       du(1,:,1) = p(:)
       du(1,:,2) = p_prev(:)
       IF (Nspec>1) THEN
          CALL dpdx (du(1,:,1),ng,v_prev(:,dim+2:dim+Nspec),2*methpd+meth,j_lev,splitFBpress)
       ELSE
          CALL dpdx (du(1,:,1),ng,v_prev(:,1),2*methpd+meth,j_lev,.FALSE.)
       END IF
       shift = (nvel(1)-1)*ng
       int_Drhs(shift+1:shift+ng) = int_Drhs(shift+1:shift+ng)-du(1,:,1)
       IF (Nspec>1) THEN
          CALL dpdx (du(1,:,2),ng,v_prev(:,dim+2:dim+Nspec),2*methpd+meth,j_lev,splitFBpress)
       ELSE
          CALL dpdx (du(1,:,2),ng,v_prev(:,1),2*methpd+meth,j_lev,.FALSE.)
       END IF
       shift = (neng-1)*ng
       int_Drhs(shift+1:shift+ng) = int_Drhs(shift+1:shift+ng)-du(1,:,1)*v_prev(:,1)-du(1,:,2)*v_prev(:,vshift+1) !u0*dp'/dx+u'*dp0/dx
       CALL c_diff_fast(v_prev(:,vshift+1), du(1,:,:), d2u(1,:,:), j_lev, ng, meth, 10, 1, 1, 1) ! du'/dx
       int_Drhs(shift+1:shift+ng) = int_Drhs(shift+1:shift+ng)-du(1,:,1)*p_prev(:)   !p0*du'/dx
       CALL c_diff_fast(v_prev(:,1), du(1,:,:), d2u(1,:,:), j_lev, ng, meth, 10, 1, 1, 1) ! du0/dx
       int_Drhs(shift+1:shift+ng) = int_Drhs(shift+1:shift+ng)-du(1,:,1)*p(:)   !p'*du0/dx
    END IF
    !
    !--Form right hand side of Euler Equations
    !
    F(:,:) = 0.0_pr
    DO j = 1+doBC, dim
       jshift=ne*(j-1)
       F(:,nden+jshift) = F(:,nden+jshift) + u_p(:,nvel(j)) ! rho*u_j
       DO i = 1, dim ! (rho*u_i*u_j)'
          F(:,nvel(i)+jshift) = F(:,nvel(i)+jshift) + u_prev(:,nvel(i))*v_prev(:,vshift+j) + u_p(:,nvel(i))*v_prev(:,j)
       END DO
       F(:,nvel(j)+jshift) = F(:,nvel(j)+jshift) + p(:) ! (rho*u_i^2)'+p'
       F(:,neng+jshift) = F(:,neng+jshift) + (u_p(:,neng) + p(:))*v_prev(:,j) + (u_prev(:,neng) + p_prev(:))*v_prev(:,vshift+j)  ! ((e+p)*u_j)'
       DO l=1,Nspecm
          F(:,nspc(l)+jshift) = F(:,nspc(l)+jshift) + v_prev(:,vshift+dim+1+l)*u_prev(:,nvel(j))+v_prev(:,dim+1+l)*u_p(:,nvel(j))
       END DO
    END DO
    IF ((doBC .eq. 0) .AND. ((methpd .ne. 0) .OR. splitFBpress)) THEN  !IF doBC>0, these terms are done inside LODI subroutines
       F(:,nvel(1)) = F(:,nvel(1)) - p(:)
       F(:,neng)    = F(:,neng)    - p_prev(:)*v_prev(:,vshift+1) - p(:)*v_prev(:,1)
    END IF
    IF (doBC .ne. 0) THEN  !Missing term from LODI assumps... gamma=constant
       shift = (neng-1)*ng
       props_prev(:,3)=props_prev(:,1)/props_prev(:,2)  !gamma/(gamma-1)0
       CALL c_diff_fast(props_prev(:,3), du(1,:,:), d2u(1,:,:), j_lev, ng, meth, 10, 1, 1, 1)  !d(gamma/(gamma-1))/dx
       int_Drhs(shift+1:shift+ng) = int_Drhs(shift+1:shift+ng)-du(1,:,1)*(v_prev(:,1)*p(:)+v_prev(:,vshift+1)*p_prev(:)) !pu*d(gamma/(gamma-1))/dx
       props(:,3)=((props(:,1)*props_prev(:,2)-props_prev(:,1)*props(:,2))/props_prev(:,2)**2) !gamma/(gamma-1)'
       CALL c_diff_fast(props(:,3), du(1,:,:), d2u(1,:,:), j_lev, ng, meth, 10, 1, 1, 1)  !d(gamma/(gamma-1))/dx
       int_Drhs(shift+1:shift+ng) = int_Drhs(shift+1:shift+ng)-du(1,:,1)*v_prev(:,1)*p_prev(:) !pu*d(gamma/(gamma-1))/dx
    END IF

    IF (imask_obstacle) THEN
       int_Drhs(1:ng) = (1.0_pr + penal*(1.0_pr/phi - 1.0_pr))*int_Drhs(1:ng)    ! cont eq penalization
       shift = ng*(neng - 1)                                   ! momentum penalization
       int_Drhs(shift + 1:shift + ng) = (1.0_pr + penal*(1.0_pr/phi - 1.0_pr))*int_Drhs(shift + 1:shift + ng) ! energy penalization
    END IF
    !
    !-- Form right hand side of Navier-Stokes Equations
    !
    IF(NS) THEN 
       tempintd = 2*vshift
       CALL c_diff_fast(v_prev(:,:), du(1:tempintd,:,:), d2u(1:tempintd,:,:), j_lev, ng, meth, 10, tempintd, 1, tempintd) ! du'_i/dx_j & dT'/dx_j
       p_prev(:) = 1.0_pr
       p(:) = 0.0_pr
       IF (Nspec>1) p_prev(:) = (p_prev(:)-SUM(v_prev(:,dim+2:dim+Nspec),DIM=2))  !Y0_Nspec
       IF (Nspec>1) p(:)      =           -SUM(v_prev(:,vshift+dim+2:vshift+dim+Nspec),DIM=2)  !Y'_Nspec
       props_prev(:,1) = p_prev(:)*mu_in(Nspec)  !mu0_Nspec
       props_prev(:,2) = p_prev(:)*kk_in(Nspec)  !kk0_Nspec
       props_prev(:,3) = p_prev(:)*bD_in(Nspec)  !bD0_Nspec
       props(:,1)      = p(:)*mu_in(Nspec)  !mu'_Nspec
       props(:,2)      = p(:)*kk_in(Nspec)  !kk'_Nspec
       props(:,3)      = p(:)*bD_in(Nspec)  !bD'_Nspec
       DO l=1,Nspecm
          props_prev(:,1) = props_prev(:,1) + v_prev(:,dim+1+l)*mu_in(l)  !mu0
          props_prev(:,2) = props_prev(:,2) + v_prev(:,dim+1+l)*kk_in(l)  !kk0
          props_prev(:,3) = props_prev(:,3) + v_prev(:,dim+1+l)*bD_in(l)  !bD0
          props(:,1)      = props(:,1)      + v_prev(:,vshift+dim+1+l)*mu_in(l)  !mu'
          props(:,2)      = props(:,2)      + v_prev(:,vshift+dim+1+l)*kk_in(l)  !kk'
          props(:,3)      = props(:,3)      + v_prev(:,vshift+dim+1+l)*bD_in(l)  !bD'
       END DO
       p(:) = du(vshift+1,:,1) !reuse of a variable
       DO i = 2, dim
          p(:) = p(:) + du(vshift+i,:,i) !du'_i/dx_i
       END DO
       IF (zeroFlux==1) THEN
          CALL user_flxfunc (d2u(1:dim,:,1:dim), ng, j_lev)
       ELSE
          d2u(:,:,:) = 1.0_pr
       END IF
       DO j = 1, dim
          jshift=ne*(j-1)
          DO i = 1, dim
             F(:,nvel(i)+jshift) = F(:,nvel(i)+jshift) - ReU(:)*props_prev(:,1)*( du(vshift+i,:,j)+du(vshift+j,:,i) ) * d2u(i,:,j)  !rho*u_i*u_j - 2*mu*S_ij/Re
          END DO
          F(:,nvel(j)+jshift) = F(:,nvel(j)+jshift) + 2.0_pr/3.0_pr*ReU(:)*props_prev(:,1)*p(:) * d2u(j,:,j) !rho*u_i*u_j - tau_ij/Re  [ tau_ij = 2*mu*( S_ij - 2/3*S_kk*delta_ij ) ]
          DO i = 1, dim
             F(:,neng+jshift) =  F(:,neng+jshift) - props_prev(:,1)*ReU(:)*( du(vshift+i,:,j)+du(vshift+j,:,i) )*v_prev(:,i) * d2u(i,:,j)  !(e+p)*u_j - u_i*2*mu*S_ij/Re
          END DO
          F(:,neng+jshift) =  F(:,neng+jshift) + 2.0_pr/3.0_pr*props_prev(:,1)*ReU(:)*p(:)*v_prev(:,j) * d2u(j,:,j) - ReU(:)*props_prev(:,2)*du(vshift+dim+1,:,j) * d2u(2,:,j)  
                                                                                             !(e+p)*u_j - u_i*tau_ij/Re - q_j/Re/Pra [ q_j = mu dT/dx_j ]
          DO l=1,Nspecm
             F(:,neng+jshift) =  F(:,neng+jshift) - u_prev(:,nden)*cp_in(l)*v_prev(:,dim+1)*ReU(:)*props_prev(:,3)*du(vshift+dim+1+l,:,j) * d2u(2,:,j)
          END DO
          IF (Nspec>1) F(:,neng+jshift) =  F(:,neng+jshift) +  u_prev(:,nden)*cp_in(Nspec)*v_prev(:,dim+1)*ReU(:)*props_prev(:,3)*SUM(du(vshift+dim+2:vshift+dim+Nspec,:,j),DIM=1) * d2u(2,:,j)
          DO l=1,Nspecm
             F(:,nspc(l)+jshift) =  F(:,nspc(l)+jshift) - u_prev(:,nden)*ReU(:)*props_prev(:,3)*du(vshift+dim+1+l,:,j) * d2u(2,:,j)
          END DO
       END DO

       p_prev(:) = du(1,:,1) !reuse of a variable
       DO i = 2, dim
          p_prev(:) = p_prev(:) + du(i,:,i) !du0_i/dx_i
       END DO
       DO j = 1, dim
          DO i = 1, dim
             F(:,nvel(i)+jshift) = F(:,nvel(i)+jshift) - ReU(:)*props(:,1)*( du(i,:,j)+du(j,:,i) ) * d2u(i,:,j)  !rho*u_i*u_j - 2*mu*S_ij/Re
          END DO
          F(:,nvel(j)+jshift) = F(:,nvel(j)+jshift) + 2.0_pr/3.0_pr*ReU(:)*props(:,1)*p_prev(:) * d2u(j,:,j) !rho*u_i*u_j - tau_ij/Re  [ tau_ij = 2*mu*( S_ij - 2/3*S_kk*delta_ij ) ]
          DO i = 1, dim
             F(:,neng+jshift) =  F(:,neng+jshift) - ReU(:)*( du(i,:,j)+du(j,:,i) )*(v_prev(:,i)*props(:,1)+v_prev(:,vshift+i)*props_prev(:,1)) * d2u(i,:,j)  !(e+p)*u_j - u_i*2*mu*S_ij/Re
          END DO
          F(:,neng+jshift) =  F(:,neng+jshift) + 2.0_pr/3.0_pr*ReU(:)*p_prev(:)*(v_prev(:,j)*props(:,1)+v_prev(:,vshift+j)*props_prev(:,1)) * d2u(j,:,j) - ReU(:)*props(:,2)*du(dim+1,:,j) * d2u(2,:,j)  
                                                                                             !(e+p)*u_j - u_i*tau_ij/Re - q_j/Re/Pra [ q_j = mu dT/dx_j ]
          DO l=1,Nspecm
             F(:,neng+jshift) =  F(:,neng+jshift) - cp_in(l)*ReU(:)*du(dim+1+l,:,j) * &
                             ( u_p(:,nden)*v_prev(:,dim+1)*props_prev(:,3) + u_prev(:,nden)*(v_prev(:,vshift+dim+1)*props_prev(:,3) + v_prev(:,dim+1)*props(:,3)) ) * d2u(2,:,j)
          END DO
          IF (Nspec>1) F(:,neng+jshift) =  F(:,neng+jshift) +  cp_in(Nspec)*ReU(:)*SUM(du(dim+2:dim+Nspec,:,j),DIM=1) * &
                             ( u_p(:,nden)*v_prev(:,dim+1)*props_prev(:,3) + u_prev(:,nden)*(v_prev(:,vshift+dim+1)*props_prev(:,3) + v_prev(:,dim+1)*props(:,3)) ) * d2u(2,:,j)
          DO l=1,Nspecm
             F(:,nspc(l)+jshift) =  F(:,nspc(l)+jshift) - ReU(:)*(u_p(:,nden)*props_prev(:,3)+u_prev(:,nden)*props(:,3))*du(dim+1+l,:,j) * d2u(2,:,j)
          END DO
       END DO
    END IF
    IF (GRAV) THEN
      shift = (neng-1)*ng
      DO j=1,dim
         DO l=1,Nspecm
            int_Drhs(shift+1:shift+ng) = int_Drhs(shift+1:shift+ng)-(v_prev(:,dim+1+l)*u_p(:,nvel(j))+v_prev(:,vshift+dim+1+l)*u_prev(:,nvel(j)))*gr_in(l+Nspec*(j-1))
         END DO
         IF (Nspec>1) THEN
            int_Drhs(shift+1:shift+ng) = int_Drhs(shift+1:shift+ng) - ((1.0_pr-SUM(v_prev(:,dim+2:dim+Nspec),DIM=2))*u_p(:,nvel(j))-SUM(v_prev(:,vshift+dim+2:vshift+dim+Nspec),DIM=2)*u_prev(:,nvel(j)) )*gr_in(Nspec*j)
         ELSE
            int_Drhs(shift+1:shift+ng) = int_Drhs(shift+1:shift+ng) - u_p(:,nvel(j))*gr_in(Nspec*j)
         END IF
      END DO
    END IF
    tempintd=ne*dim
    CALL c_diff_fast(F(:,1:tempintd), du(1:tempintd,:,:), d2u(1:tempintd,:,:), j_lev, ng, meth, 10, tempintd, 1, tempintd)  !du(i,:,k)=dF_ij/dx_k

    DO j = 1, dim
       jshift=ne*(j-1)
       DO i = 1, dim+1+Nspec
          shift=(i-1)*ng
          int_Drhs(shift+1:shift+ng) = int_Drhs(shift+1:shift+ng) - du(i+jshift,:,j)  ! -dF_ij/dx_j
       END DO
       IF (GRAV) THEN      
          shift = (nvel(j)-1)*ng
          DO l=1,Nspecm        
             int_Drhs(shift+1:shift+ng) = int_Drhs(shift+1:shift+ng) - gr_in(l+Nspec*(j-1))*u_p(:,nspc(l))
          END DO
          IF (Nspec>1) THEN
             int_Drhs(shift+1:shift+ng) = int_Drhs(shift+1:shift+ng) - gr_in(Nspec*j)*(u_p(:,nden)-SUM(u_p(:,nspc(1):nspc(Nspecm)),DIM=2))
          ELSE
             int_Drhs(shift+1:shift+ng) = int_Drhs(shift+1:shift+ng) - gr_in(Nspec*j)*u_p(:,nden)
          END IF
       END IF
    END DO
    IF (NS .AND. zeroFlux==2) THEN
       tempintd = 2*vshift
       CALL c_diff_fast(v_prev(:,:), du(1:tempintd,:,:), d2u(1:tempintd,:,:), j_lev, ng, meth, 10, tempintd, 1, tempintd) ! du'_i/dx_j & dT'/dx_j
       DO i = 2, dim
          F(:,i-1) = ReU(:)*props_prev(:,1)*( du(vshift+i,:,1)+du(vshift+1,:,i) )  !2*mu*S_ij/Re
       END DO
       F(:,dim) = ReU(:)*props_prev(:,2)*du(vshift+dim+1,:,1)  !q_j/Re/Pra [ q_j = mu dT/dx_j ]
       DO l=1,Nspecm
          F(:,dim+l) =  u_prev(:,nden)*ReU(:)*props_prev(:,3)*du(vshift+dim+1+l,:,1)
       END DO
       IF (Nspec>1) F(:,dim+Nspec) =  -u_prev(:,nden)*ReU(:)*props_prev(:,3)*SUM(du(vshift+dim+2:vshift+dim+Nspec,:,1),DIM=1)

       DO i = 2, dim
          F(:,i-1) = F(:,i-1) + ReU(:)*props(:,1)*( du(i,:,1)+du(1,:,i) )  !2*mu*S_ij/Re
       END DO
       F(:,dim) = F(:,dim) + ReU(:)*props(:,2)*du(dim+1,:,1)  !q_j/Re/Pra [ q_j = mu dT/dx_j ]
       DO l=1,Nspecm
          F(:,dim+l) = F(:,dim+l) + ReU(:)*(u_prev(:,nden)*props(:,3)+u_p(:,nden)*props_prev(:,3))*du(dim+1+l,:,1)
       END DO
       IF (Nspec>1) F(:,dim+Nspec) =  F(:,dim+Nspec) - ReU(:)*(u_prev(:,nden)*props(:,3)+u_p(:,nden)*props_prev(:,3))*SUM(du(dim+2:dim+Nspec,:,1),DIM=1)
       DO i = 2, dim
          F(:,i-1+ne) = ReU(:)*props_prev(:,1)*( du(i,:,1)+du(1,:,i) )  !2*mu*S_ij/Re
       END DO
       F(:,dim+ne) = ReU(:)*props_prev(:,2)*du(dim+1,:,1)  !q_j/Re/Pra [ q_j = mu dT/dx_j ]
       DO l=1,Nspecm
          F(:,dim+l+ne) = ReU(:)*u_prev(:,nden)*props_prev(:,3)*du(dim+1+l,:,1)
       END DO
       IF (Nspec>1) F(:,dim+Nspec+ne) = -ReU(:)*u_prev(:,nden)*props_prev(:,3)*SUM(du(dim+2:dim+Nspec,:,1),DIM=1)

       tempintd = 2*vshift
       CALL c_diff_fast(F(:,1:tempintd), du(1:tempintd,:,:), d2u(1:tempintd,:,:), j_lev, ng, meth, 10, tempintd, 1, tempintd)
       CALL user_flxfunc (d2u(1:dim,:,1:dim), ng, j_lev)
       d2u(1:dim,:,1:dim) = 1.0_pr - d2u(1:dim,:,1:dim)
       DO i=2,dim
          shift=(nvel(i)-1)*ng
          int_Drhs(shift+1:shift+ng) = int_Drhs(shift+1:shift+ng) - du(i-1,:,1)*d2u(2,:,1)
       END DO
       shift=(neng-1)*ng
       DO i=2,dim
          int_Drhs(shift+1:shift+ng) = int_Drhs(shift+1:shift+ng) - du(i-1,:,1)*v_prev(:,i)*d2u(2,:,1)
       END DO
       int_Drhs(shift+1:shift+ng) = int_Drhs(shift+1:shift+ng) - du(dim,:,1)*d2u(2,:,1)
       DO l=1,Nspec
          IF (Nspec>1) int_Drhs(shift+1:shift+ng) = int_Drhs(shift+1:shift+ng) - v_prev(:,dim+1)*cp_in(l)*du(dim+l,:,1)*d2u(2,:,1)
       END DO
       DO l=1,Nspecm
          shift=(nspc(l)-1)*ng 
          int_Drhs(shift+1:shift+ng) = int_Drhs(shift+1:shift+ng) - du(dim+l,:,1)*d2u(2,:,1)
       END DO

       shift=(neng-1)*ng
       DO i=2,dim
          int_Drhs(shift+1:shift+ng) = int_Drhs(shift+1:shift+ng) - du(i-1+ne,:,1)*v_prev(:,vshift+i)*d2u(2,:,1)
       END DO
       DO l=1,Nspec
          IF (Nspec>1) int_Drhs(shift+1:shift+ng) = int_Drhs(shift+1:shift+ng) - v_prev(:,vshift+dim+1)*cp_in(l)*du(dim+l+ne,:,1)*d2u(2,:,1)
       END DO
    END IF
  END SUBROUTINE internal_Drhs

  SUBROUTINE internal_Drhs_diag (int_diag,u_prev,meth,doBC,addingon)
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: meth
    REAL (pr), DIMENSION (ng*ne), INTENT(INOUT) :: int_diag
    REAL (pr), DIMENSION (ng,ne), INTENT(IN) :: u_prev
    INTEGER, INTENT(IN) :: doBC !if zero do full rhs, if one no x-derivatives 
    INTEGER, INTENT(IN) :: addingon 

    INTEGER :: i, j, l, ll, ie, shift, zeroFlux
    REAL (pr), DIMENSION (ng,dim) :: du_diag, d2u_diag
    REAL (pr), DIMENSION (MAX(Nspec,dim),ng,dim) :: du, d2u
    REAL (pr), DIMENSION (ng,n_integrated,dim) :: F
    REAL (pr), DIMENSION (ng, Nspec) :: v_prev
    REAL (pr), DIMENSION (ng) :: ReU
    REAL (pr), DIMENSION (ng,6) :: props_prev

    zeroFlux = 0
    IF (dozeroFlux .AND. (doBC+addingon)>0) zeroFlux = dervFlux + 1

    CALL user_bufferRe (ReU, ng)

    int_diag = 0.0_pr
    DO l=1,Nspecm
       v_prev(:,l) = u_prev(:,nspc(l))/u_prev(:,nden)  ! Y0
    END DO
    v_prev(:,Nspec)=1.0_pr
    IF (Nspec>1) v_prev(:,Nspec) = v_prev(:,Nspec)-SUM(v_prev(:,1:Nspecm),DIM=2)  !Y0_Nspec
    props_prev(:,:)=0.0_pr
    DO l=1,Nspec
       props_prev(:,1) = props_prev(:,1) + v_prev(:,l)*cp_in(l)  !cp0
       props_prev(:,2) = props_prev(:,2) + v_prev(:,l)/MW_in(l)  !R0
       props_prev(:,4) = props_prev(:,1) + v_prev(:,l)*mu_in(l)  !mu0
       props_prev(:,5) = props_prev(:,2) + v_prev(:,l)*kk_in(l)  !kk0
       props_prev(:,6) = props_prev(:,3) + v_prev(:,l)*bD_in(l)  !bD0
    END DO
    props_prev(:,3) = props_prev(:,1)/(props_prev(:,1)-props_prev(:,2)) !gamma0 = cp0/(cp0-R0)
    IF ((doBC .eq. 0) .AND. ((methpd .ne. 0) .OR. splitFBpress)) THEN  !IF doBC>0, these terms are done inside LODI subroutines
       CALL c_diff_fast(u_prev(:,nvel(1))/u_prev(:,nden), du(1,:,:), d2u(1,:,:), j_lev, ng, meth, 10, 1, 1, 1) ! du0/dx
       IF (Nspec>1) THEN
          CALL dpdx_diag (du_diag(:,1),ng,v_prev(:,1:Nspecm),2*methpd+meth,j_lev,splitFBpress)
       ELSE
          CALL dpdx_diag (du_diag(:,1),ng,v_prev(:,1),2*methpd+meth,j_lev,.FALSE.)
       END IF
       shift = (nvel(1)-1)*ng
       int_diag(shift+1:shift+ng) = int_diag(shift+1:shift+ng) + du_diag(:,1)*u_prev(:,nvel(1))/u_prev(:,nden)*(props_prev(:,3)-1.0_pr)  !pd_rhou*diagFB_x
       shift = (neng-1)*ng
       int_diag(shift+1:shift+ng) = int_diag(shift+1:shift+ng) - (du(1,:,1) + du_diag(:,1)*u_prev(:,nvel(1))/u_prev(:,nden))*(props_prev(:,3)-1.0_pr) !pd_rhoe*(du0/dx+u0*diagFB_x)
    END IF
    CALL c_diff_diag(du_diag, d2u_diag, j_lev, ng, meth, meth, -11)
    !
    !--Form right hand side of Euler Equations
    !
    F(:,:,:) = 0.0_pr
    DO j = 1+doBC, dim
       DO i = 1, dim ! (rho*u_i*u_j)'
          F(:,nvel(i),j) = F(:,nvel(i),j) + u_prev(:,nvel(j))/u_prev(:,nden)
       END DO
       F(:,nvel(j),j) = F(:,nvel(j),j) + u_prev(:,nvel(j))/u_prev(:,nden)*(2.0_pr-props_prev(:,3))
       F(:,neng,j) = F(:,neng,j) + props_prev(:,3)*u_prev(:,nvel(j))/u_prev(:,nden)
       DO l=1,Nspecm
          F(:,nspc(l),j) = F(:,nspc(l),j) + u_prev(:,nvel(j))/u_prev(:,nden)
       END DO
    END DO
    IF ((doBC .eq. 0) .AND. ((methpd .ne. 0) .OR. splitFBpress)) THEN  !IF doBC>0, these terms are done inside LODI subroutines
       F(:,nvel(1),1) = F(:,nvel(1),1) + u_prev(:,nvel(j))/u_prev(:,nden)*(props_prev(:,3)-1.0_pr)
       F(:,neng,1) = F(:,neng,1) - (props_prev(:,3)-1.0_pr)*u_prev(:,nvel(1))/u_prev(:,nden)
    END IF
    IF (doBC .ne. 0) THEN  !Missing term from LODI assumps... gamma=constant
       shift = (neng-1)*ng
       CALL c_diff_fast(props_prev(:,1)/props_prev(:,2), du(1,:,:), d2u(1,:,:), j_lev, ng, meth, 10, 1, 1, 1)  !d(gamma/(gamma-1))/dx
       int_diag(shift+1:shift+ng) = int_diag(shift+1:shift+ng)-du(1,:,1)*u_prev(:,nvel(1))/u_prev(:,nden)*(props_prev(:,3)-1.0_pr) !pu*d(gamma/(gamma-1))/dx
    END IF

    IF (imask_obstacle) THEN
       int_diag(1:ng) = (1.0_pr + penal*(1.0_pr/phi - 1.0_pr))*int_diag(1:ng)    ! cont eq penalization
       shift = ng*(neng - 1)                                   ! momentum penalization
       int_diag(shift + 1:shift + ng) = (1.0_pr + penal*(1.0_pr/phi - 1.0_pr))*int_diag(shift + 1:shift + ng) ! energy penalization
    END IF

    !
    !-- Form right hand side of Navier-Stokes Equations
    !
    IF(NS .AND. NSdiag) THEN
       CALL c_diff_fast(v_prev, du(1:Nspec,:,:), d2u(1:Nspec,:,:), j_lev, ng, meth, 10, Nspec, 1, Nspec) ! dY_l/dx_j
       IF (zeroFlux==1) THEN
          CALL user_flxfunc (d2u(1:dim,:,1:dim), ng, j_lev)
       ELSE
          d2u(:,:,:) = 1.0_pr
       END IF
       DO j = 1, dim
          DO l=1,Nspec
             IF (Nspec>1) F(:,neng,j) =  F(:,neng,j) - ReU(:)*props_prev(:,6)*du(l,:,j)*cp_in(l)/(props_prev(:,1)-props_prev(:,2))*d2u(2,:,j)
          END DO 
       END DO
       DO j = 1, dim
          DO i = 1, dim
             shift=(nvel(i)-1)*ng
             int_diag(shift+1:shift+ng) = int_diag(shift+1:shift+ng) + props_prev(:,4)*ReU(:)/u_prev(:,nden)*d2u_diag(:,j) * d2u(i,:,j)
          END DO
          shift=(nvel(j)-1)*ng
          int_diag(shift+1:shift+ng) = int_diag(shift+1:shift+ng) + props_prev(:,4)*ReU(:)/u_prev(:,nden)*d2u_diag(:,j)/3.0_pr * d2u(j,:,j)
          shift=(neng-1)*ng
          int_diag(shift+1:shift+ng) = int_diag(shift+1:shift+ng) + props_prev(:,5)*ReU(:)/u_prev(:,nden)/(props_prev(:,1)-props_prev(:,2))*d2u_diag(:,j) * d2u(2,:,j)
          DO l = 1, Nspecm
             shift=(nspc(l)-1)*ng
             int_diag(shift+1:shift+ng) = int_diag(shift+1:shift+ng) + ReU(:)*(props_prev(:,6)*d2u_diag(:,j)+bD_in(l)*du(l,:,j)) * d2u(2,:,j)
          END DO
       END DO
    END IF
    DO j = 1, dim
       DO i = 1, dim+1+Nspec
          shift=(i-1)*ng
          int_diag(shift+1:shift+ng) = int_diag(shift+1:shift+ng) - F(:,i,j)*du_diag(:,j)
       END DO
    END DO

    IF (NS .AND. NSdiag .AND. zeroFlux==2) THEN
       CALL user_flxfunc (d2u(1:dim,:,1:dim), ng, j_lev)
       d2u(1:dim,:,1:dim) = 1.0_pr - d2u(1:dim,:,1:dim)
       DO i = 2, dim
          shift=(nvel(i)-1)*ng
          int_diag(shift+1:shift+ng) = int_diag(shift+1:shift+ng) - props_prev(:,4)*ReU(:)/u_prev(:,nden)*d2u_diag(:,1) * d2u(2,:,1)
       END DO
       DO l = 1, Nspecm
          shift=(nspc(l)-1)*ng
          int_diag(shift+1:shift+ng) = int_diag(shift+1:shift+ng) - ReU(:)*(props_prev(:,6)*d2u_diag(:,1)+bD_in(l)*du(l,:,1)) * d2u(2,:,1)
       END DO
       shift=(neng-1)*ng
       int_diag(shift+1:shift+ng) = int_diag(shift+1:shift+ng) - props_prev(:,5)*ReU(:)/u_prev(:,nden)/(props_prev(:,1)-props_prev(:,2))*d2u_diag(:,1) * d2u(2,:,1)
       DO l = 1, Nspec
          IF (Nspec>1) v_prev(:,l) = ReU(:)*du(l,:,1)*props_prev(:,6)*u_prev(:,nden)
       END DO
       CALL c_diff_fast(v_prev, du(1:Nspec,:,:), d2u(1:Nspec,:,:), j_lev, ng, meth, 10, Nspec, 1, Nspec) ! d/dx_j(rho0*D0*dY_l/dx)
       DO l = 1, Nspec
          IF (Nspec>1) int_diag(shift+1:shift+ng) = int_diag(shift+1:shift+ng) - cp_in(l)/u_prev(:,nden)/(props_prev(:,1)-props_prev(:,2))*du(l,:,1) * d2u(2,:,1)
       END DO
    END IF
  END SUBROUTINE internal_Drhs_diag

  FUNCTION user_rhs (u_integrated,scalar)
    IMPLICIT NONE
    REAL (pr), DIMENSION (ng,ne), INTENT(IN) :: u_integrated !1D flat version of integrated variables without BC 
    REAL (pr), DIMENSION (ng), INTENT(IN) :: scalar
    REAL (pr), DIMENSION (ng*ne) :: user_rhs
    INTEGER, PARAMETER :: meth=1
    IF ((LODIit .eq. 4) .OR. ((LODIit .eq. 2) .AND. (globFlux .ne. 1))) THEN
       CALL internal_rhs(user_rhs,u_integrated,0,1)
    ELSE
       CALL internal_rhs(user_rhs,u_integrated,0,0)
    END IF

    IF (imask_obstacle == .TRUE.) CALL Brinkman_rhs (user_rhs, u_integrated, .FALSE., meth) !add penalization ter

    IF (LODIit==1) CALL user_evol_LODI_BC_rhs      (user_rhs, u_integrated, ne, ng, j_lev, meth)
    IF (LODIit==2) CALL user_evol_LODIslip_BC_rhs  (user_rhs, u_integrated, ne, ng, j_lev, meth)
    IF (LODIit==3) CALL user_evol_LODIavg_BC_rhs   (user_rhs, u_integrated, ne, ng, j_lev, meth)
    IF (LODIit==4) CALL user_evol_LODIsimp_BC_rhs  (user_rhs, u_integrated, ne, ng, j_lev, meth)
    IF (LODIit==5) CALL user_evol_liu_BC_rhs       (user_rhs, u_integrated, ne, ng, j_lev, meth)
    IF (LODIit==6) CALL user_evol_LODIaddon_BC_rhs (user_rhs, u_integrated, ne, ng, j_lev, meth)
    IF (cnvtBC) CALL user_convectzone_BC_rhs (user_rhs, u_integrated, ne, ng, j_lev, meth)
    IF (buffBC) CALL user_bufferzone_BC_rhs (user_rhs, u_integrated, ne, ng, j_lev, meth)
    
    IF (hypermodel /= 0) CALL hyperbolic(u_integrated, ng, user_rhs)
    
  END FUNCTION user_rhs

  FUNCTION user_Drhs (u_p, u_prev, meth)
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: meth
    REAL (pr), DIMENSION (ng,ne), INTENT(IN) :: u_p, u_prev
    REAL (pr), DIMENSION (ng*ne) :: user_Drhs
    IF ((LODIit .eq. 4) .OR. ((LODIit .eq. 2) .AND. (globFlux .ne. 1))) THEN
       CALL internal_Drhs(user_Drhs,u_p,u_prev,meth,0,1)
    ELSE
       CALL internal_Drhs(user_Drhs,u_p,u_prev,meth,0,0)
    END IF

    IF (imask_obstacle == .TRUE.) CALL Brinkman_Drhs (user_Drhs, u_p, u_prev, meth)  !add penalization terms

    IF (LODIit==1) CALL user_evol_LODI_BC_Drhs      (user_Drhs, u_p, u_prev, ne, ng, j_lev, meth)
    IF (LODIit==2) CALL user_evol_LODIslip_BC_Drhs  (user_Drhs, u_p, u_prev, ne, ng, j_lev, meth)
    IF (LODIit==3) CALL user_evol_LODIavg_BC_Drhs   (user_Drhs, u_p, u_prev, ne, ng, j_lev, meth)
    IF (LODIit==4) CALL user_evol_LODIsimp_BC_Drhs  (user_Drhs, u_p, u_prev, ne, ng, j_lev, meth)
    IF (LODIit==5) CALL user_evol_liu_BC_Drhs       (user_Drhs, u_p, u_prev, ne, ng, j_lev, meth)
    IF (LODIit==6) CALL user_evol_LODIaddon_BC_Drhs (user_Drhs, u_p, u_prev, ne, ng, j_lev, meth)
    IF (cnvtBC) CALL user_convectzone_BC_Drhs (user_Drhs, u_p, u_prev, ne, ng, j_lev, meth)
    IF (buffBC) CALL user_bufferzone_BC_Drhs (user_Drhs, u_p, u_prev, ne, ng, j_lev, meth)
    
    IF (hypermodel /= 0) CALL hyperbolic(u, ng, user_Drhs)
    
  END FUNCTION user_Drhs

  FUNCTION user_Drhs_diag (meth)
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: meth
    REAL (pr), DIMENSION (ng*ne) :: user_Drhs_diag
    REAL (pr), DIMENSION (ng,ne) :: u_prev
    INTEGER :: ie, shift

    DO ie = 1, ne
       shift=(ie-1)*ng
       u_prev(1:ng,ie) = u_prev_timestep(shift+1:shift+ng)
    END DO

    IF ((LODIit .eq. 4) .OR. ((LODIit .eq. 2) .AND. (globFlux .ne. 1))) THEN
       CALL internal_Drhs_diag(user_Drhs_diag,u_prev,meth,0,1)
    ELSE
       CALL internal_Drhs_diag(user_Drhs_diag,u_prev,meth,0,0)
    END IF

    IF (imask_obstacle == .TRUE.) CALL Brinkman_Drhs_diag (user_Drhs_diag, meth)  !add penalization terms

    IF (LODIit==1) CALL user_evol_LODI_BC_Drhs_diag      (user_Drhs_diag, u_prev, ne, ng, j_lev, meth)
    IF (LODIit==2) CALL user_evol_LODIslip_BC_Drhs_diag  (user_Drhs_diag, u_prev, ne, ng, j_lev, meth)
    IF (LODIit==3) CALL user_evol_LODIavg_BC_Drhs_diag   (user_Drhs_diag, u_prev, ne, ng, j_lev, meth)
    IF (LODIit==4) CALL user_evol_LODIsimp_BC_Drhs_diag  (user_Drhs_diag, u_prev, ne, ng, j_lev, meth)
    IF (LODIit==5) CALL user_evol_liu_BC_Drhs_diag       (user_Drhs_diag, u_prev, ne, ng, j_lev, meth)
    IF (LODIit==6) CALL user_evol_LODIaddon_BC_Drhs_diag (user_Drhs_diag, u_prev, ne, ng, j_lev, meth)
    IF (cnvtBC) CALL user_convectzone_BC_Drhs_diag (user_Drhs_diag, u_prev, ne, ng, j_lev, meth)
    IF (buffBC) CALL user_bufferzone_BC_Drhs_diag (user_Drhs_diag, u_prev, ne, ng, j_lev, meth)
    
    IF (hypermodel /= 0) CALL hyperbolic_diag(user_Drhs_diag, ng)
    
  END FUNCTION user_Drhs_diag

  SUBROUTINE user_project (u, p, nlocal, meth)
    IMPLICIT NONE

    INTEGER, INTENT (IN) :: meth, nlocal
    REAL (pr), DIMENSION (nlocal),     INTENT(INOUT) :: p
    REAL (pr), DIMENSION (nlocal,n_integrated), INTENT(INOUT) :: u

    INTEGER :: i
    INTEGER, PARAMETER :: ne = 1
    INTEGER, DIMENSION(ne) :: clip 
    !no projection for compressible flow
  END SUBROUTINE user_project

  FUNCTION Laplace (jlev, u, nlocal, ne_local, meth)
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: jlev, nlocal, ne_local, meth
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (INOUT) :: u
    REAL (pr), DIMENSION (nlocal*ne_local) :: Laplace

    INTEGER :: i, ii, ie, shift, meth1, meth2, idim
    REAL (pr), DIMENSION (ne_local ,nlocal,dim) :: du
    REAL (pr), DIMENSION (ne_local*dim ,nlocal,dim) :: d2u
    REAL (pr), DIMENSION (ne_local*dim ,nlocal,dim) :: du_dummy ! passed when only calculating 1st derivative.
    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc
    
    Laplace=0.0_pr
  END FUNCTION Laplace

  FUNCTION Laplace_diag (jlev,  nlocal, ne_local, meth)
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: jlev, nlocal, ne_local, meth
    REAL (pr), DIMENSION (nlocal*ne_local) :: Laplace_diag

    INTEGER :: i, ii, ie, shift, meth1, meth2
    REAL (pr), DIMENSION (nlocal,dim) :: du, d2u
    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc

    Laplace_diag=0.0_pr
  END FUNCTION Laplace_diag

  FUNCTION Laplace_rhs(u, nlocal, ne_local, meth)
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: nlocal, ne_local, meth
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (INOUT) :: u
    REAL (pr), DIMENSION (nlocal) :: Laplace_rhs

    INTEGER :: i, ii, ie, meth1, meth2, idim
    REAL (pr), DIMENSION (ne_local ,nlocal,dim) :: du
    REAL (pr), DIMENSION (ne_local*dim ,nlocal,dim) :: d2u
    REAL (pr), DIMENSION (ne_local*dim ,nlocal,dim) :: du_dummy ! passed when only calculating 1st derivative.
    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc

    Laplace_rhs = 0.0_pr 
  END FUNCTION Laplace_rhs

  !
  ! Calculate any statitics
  !
  ! startup_flag - 0 when adapting to IC,then 1 inmain integration loop
  !
  SUBROUTINE user_stats ( u ,j_mn, startup_flag)
    IMPLICIT NONE
    REAL (pr), DIMENSION (nwlt,1:n_var), INTENT (IN) :: u
    INTEGER , INTENT (IN) :: j_mn 
    INTEGER , INTENT (IN) :: startup_flag
  END SUBROUTINE user_stats

  SUBROUTINE user_cal_force (u, n, t_local, force, drag, lift)
    !--Calculates drag and lift on obstacle using penalization formula
    IMPLICIT NONE

    INTEGER, INTENT (IN) :: n
    REAL (pr), INTENT (IN) :: t_local
    REAL (pr), DIMENSION (dim), INTENT (INOUT) :: force
    REAL (pr), INTENT (OUT) :: drag, lift
    REAL (pr), DIMENSION (n,dim) :: u
    drag = 0.0_PR
    lift = 0.0_PR
    !
    ! There is no obstacle in flow
    !

  END SUBROUTINE user_cal_force

  !
  ! Read input from "case_name"_pde.inp file
  ! case_name is in string file_name read from command line
  ! in read_command_line_input()
  ! 
  SUBROUTINE user_read_input()
    IMPLICIT NONE
    INTEGER :: i, j, ind
    CHARACTER(LEN=8):: numstrng, procnum
    INTEGER :: myiter
    LOGICAL :: checkit 
    REAL (pr) :: halfdomsize

    call input_real ('Re',Re,'stop',' Re: Reynolds Number')
    call input_real ('Rebnd',Rebnd,'stop',' Rebnd: Reynolds Number on boundaries')
    call input_real ('Pra',Pra,'stop',' Pra: Prandtl Number')
    call input_real ('Sc',Sc,'stop',' Sc: Schmidt Number')
    call input_real ('Fr',Fr,'stop',' Fr: Froude Number')
    call input_real ('Ma',Ma,'stop',' Ma: Mach Number')
    call input_integer ('Nspec',Nspec,'stop','  Nspec: Number of species')
    call input_integer ('methpd',methpd,'stop','  methpd: meth for taking pressure derivative in time')
    call input_integer ('BCver',BCver,'stop','  BCver: version for BC')
    call input_integer ('ICver',ICver,'stop','  ICver: version for IC')

    call input_integer ('LODIit',LODIit,'stop', 'LODIit: 1 for LODI, 2 for LODIslip, 3 for LODIavg, 4 for LODIsimp')
    call input_integer ('PLpinf',PLpinf,'stop', 'PLpinf: Poinsot Lele P_inf Brinkman term with LODI, 1:2 for P, 3:4 for T, 5:6 for u, (odd const coef, even var coef) 0 for nothing')
    call input_integer ('globFlux',globFlux,'stop', 'globFlux: 1 to do global fluxes, 0 to do local fluxes on boundaries only')
    call input_integer ('dervFlux',dervFlux,'stop', 'dervFlux: 1 to do derivative fluxes, 0 to set fluxes to 0 on boundary only')
 
    Nspec=MAX(Nspec,1)
  
    ALLOCATE(gr_in(1:Nspec*dim))  
    ALLOCATE(bD_in(1:Nspec))
    ALLOCATE(mu_in(1:Nspec))
    ALLOCATE(kk_in(1:Nspec))
    ALLOCATE(cp_in(1:Nspec))
    ALLOCATE(MW_in(1:Nspec))
    ALLOCATE(gamm(1:Nspec))
    ALLOCATE(pureX(1:Nspec), pureY(1:Nspec), pureR(1:Nspec), pureg(1:Nspec), puregamma(1:Nspec), YR(1:Nspec))

    call input_real ('At',At,'stop',' At: Atwood Number')
    call input_real_vector ('gamm',gamm(1:Nspec),Nspec,'stop',' gamm: cp/(cp-R)')
    call input_real ('peakchange',peakchange,'stop',' peakchange: how far offset to put peaks')
    call input_real ('buffBeta',buffBeta,'stop', 'bufBeta: polynomial exponent for buff/convect BC')
    call input_real ('buffU0',buffU0,'stop', 'buffU0: scale for convect BC')
    call input_real ('buffSig',buffSig,'stop', 'buffSig: scale for buff BC')
    call input_real ('buffOff',buffOff,'stop', 'buffOff: x-offset used in tanh for buff BC')
    call input_real ('buffFac',buffFac,'stop', 'buffFac: multiplication factor in tanh for buff BC')
    call input_real ('pBsig',pBsig,'stop', 'pBsig: sigma used in coefficient for pressure Brinkman term')
    call input_real ('Lczn',Lczn,'stop', 'Lcoordzn: length of coord_zone on one side')
    call input_real ('tfacczn',tfacczn,'stop', 'tfacczn: factor multiply by time for coord_zone time=L/min(c)*tfac, set <0 for no czn modification')
    call input_real ('itfacczn',itfacczn,'stop', 'itfacczn: factor multiply by initial time for coord_zone itime=Lczn/min(c)*tfac, set <0 to set itime=0')
    call input_real ('waitrat',waitrat,'stop', 'waitrat: fraction to wait beyond acoustic speed travel time')
    call input_real_vector ('buffRed',buffRed(1:5),5,'stop', 'buffRed: buffRe distances')
    call input_real_vector ('buffRef',buffRef(1:5),5,'stop', 'buffRef: buffRe distance fractions')
    call input_real_vector ('buffcvd',buffcvd(1:5),5,'stop', 'buffcvd: convect zone distances')
    call input_real_vector ('buffcvf',buffcvf(1:5),5,'stop', 'buffcvf: convect zone distance fractions')
    call input_real_vector ('buffbfd',buffbfd(1:5),5,'stop', 'buffbfd: buffer zone distances')
    call input_real_vector ('buffbff',buffbff(1:5),5,'stop', 'buffbff: buffer zone distance fractions')
    call input_real ('maskscl',maskscl,'stop', 'maskscl: scale for mask, set < 0 for default')
    call input_real ('tempscl',tempscl,'stop', 'tempscl: scale for temperature, set < 0 for default')
    call input_real ('specscl',specscl,'stop', 'specscl: scale for species, set < 0 for default')
    call input_real ('mydt_orig',mydt_orig,'stop', 'mydt_orig: max dt for time integration... dt is only initial dt, set <0 to use dt')

    call input_logical ('GRAV',GRAV,'stop', 'GRAV: T to include body force terms, F to ingore them')
    call input_logical ('NS',NS,'stop', 'NS: T to include viscous terms (full Navier-Stokes), F for Euler Eqs')

    call input_logical ('adaptAt',adaptAt,'stop', 'adaptAt: T to modify At to get effectivie At = input At due to peakchange')
    call input_logical ('evolBC',evolBC,'stop', 'evolBC: T to use evol BCs, F to use algebaic BCs')
    call input_logical ('buffBC',buffBC,'stop', 'buffBC: T to use buff zone damping BCs (Brinkman), F to use algebraic BCs')
    call input_logical ('polybuff',polybuff,'stop', 'polybuff: T to use polynomials for buff zone BCs,  F to use tanh')
    call input_logical ('polybuffRe',polybuffRe,'stop', 'polybuffRe: T to use polynomials for buffRe,  F to use tanh')
    call input_logical ('cnvtBC',cnvtBC,'stop', 'cnvtBC: T to use convection zone BCs (Freund), F to use algebraic BCs')
    call input_logical ('polyconv',polyconv,'stop', 'polyconv: T to use polynomials for conv zone BCs,  F to use tanh')
    call input_logical ('pertchar',pertchar,'stop', 'pertchar: T to do characteristics on perturbed p and rho')
    call input_logical ('usedP0',usedP0,'stop', 'usedP0: T to use dP0 for X2 perthcar, F to use -rhoYg')
    call input_logical ('dozeroFlux',dozeroFlux,'stop', 'dozeroFlux: T to set dtau_i1/dx_1 (i.ne.1)=dq_1/dx_1=dY/dx_1=0 for evolBC')
    call input_logical ('pBrink',pBrink,'stop', 'pBrink: T to do Brinkman buffer only on pressure')
    call input_logical ('savebuoys',savebuoys,'stop', 'savebuoys: if T adds and saves dPdx and -(dPdx + rho*g)')
    call input_logical ('adaptbuoy',adaptbuoy,'stop', 'adaptbuoy: T to adapt on buoy term')
    call input_logical ('splitFBpress',splitFBpress,'stop', 'splitFBpress: to split derivs for pressure dp/dx FB/BB light/heavy')
    call input_logical ('stepsplit',stepsplit,'stop', 'stepsplit: T to do Heaviside split, F to use X')
    call input_logical ('buffRe',buffRe,'stop', 'buffRe: T to change Reynolds number near boundaries')
    call input_logical ('adaptbuffRe',adaptbuffRe,'stop', 'adaptbuffRe: T to adapt on buffer layer')
    call input_logical ('adapttmp',adapttmp,'stop', 'adapttmp: T to adapt on temperature instead of density and pressure')
    call input_logical ('adaptvelonly',adaptvelonly,'stop', 'adaptvelonly: T to adapt on velocity instead of momentum')
    call input_logical ('adaptspconly',adaptspconly,'stop', 'adaptspconly: T to adapt on mass fraction (Y) instead of volume fraction (rhoY)')
    call input_logical ('adaptden',adaptden,'stop', 'adapt on density')
    call input_logical ('adaptvel',adaptvel,'stop', 'adapt on momentum')
    call input_logical ('adapteng',adapteng,'stop', 'adapt on energy')
    call input_logical ('adaptspc',adaptspc,'stop', 'adapt on species')
    call input_logical ('adaptprs',adaptprs,'stop', 'adapt on pressure')
    call input_logical ('buffRewait',buffRewait,'stop', 'buffRewait: T to wait until acoustic waves reach boundaries to do buffRe')
    call input_logical ('LODIset',LODIset,'stop', 'LODIset: T to set characteristics u+/-c only, F to check all characteristics')
    call input_logical ('LODIslipeasy',LODIslipeasy,'stop', 'LODIslipeasy: T to simple LODIslip, where dp/dx=0 set in eqns')
    call input_logical ('savepardom',savepardom,'stop', 'savepardom: T to save parallel domain as additional var')
    call input_logical ('doassym',doassym,'stop', 'doassym: T to modify assym parameters in user_code')
    call input_logical ('convcfl',convcfl,'stop', 'convcfl: T to use convective cfl, F to use acoustic cfl')
    call input_logical ('boundY',boundY,'stop', 'boundY: T to bound Y to [0,1] in user_pre/post_process')

    !4extra - these 8 input definitions - MUST ALSO ADD TO .inp 
    call input_logical ('adaptMagVort',adaptMagVort,'stop','adaptMagVort: Adapt on the magnitude of vorticity')
    call input_logical ('adaptComVort',adaptComVort,'stop','adaptComVort: Adapt on the components of vorticity')
    call input_logical ('adaptMagVel',adaptMagVel,'stop','adaptMagVel: Adapt on the magnitude of velocity')
    call input_logical ('adaptNormS',adaptNormS,'stop', 'adaptNormS: Adapt on the L2 of Sij')
    call input_logical ('adaptGradY',adaptGradY,'stop', 'adaptGradY: Adapt on |dY/dx_i*dY/dx_i|')
    call input_logical ('saveMagVort',saveMagVort,'stop', 'saveMagVort: if T adds and saves magnitude of vorticity')
    call input_logical ('saveComVort',saveComVort,'stop', 'saveComVort: if T adds and saves components of vorticity')
    call input_logical ('saveMagVel',saveMagVel,'stop','saveMagVel: if T adds and saves magnitude of velocity')
    call input_logical ('saveNormS',saveNormS,'stop', 'saveNormS: if T adds and saves magnitude of strain rate')
    call input_logical ('saveGradY',saveGradY,'stop', 'saveGradY: if T adds and saves |dY/dx_i*dY/dx_i|')
    call input_logical ('saveUcvt',saveUcvt,'stop', 'saveUcvt: save U and damping support for Freund NRBCs to debug')

    call input_real ('phi',phi,'stop', 'phi - porosity')
    call input_real ('eta',eta,'stop',' penalization parameter')

    call input_real ('xw',xw,'stop', 'wedge apex')
    call input_real ('wa',wa,'stop', 'wedge angle (degree)')
      
    !Initial Conditions
    IF(.NOT.ALLOCATED(uL)) ALLOCATE(uL(dim)); uL = 0.0_pr
    IF(.NOT.ALLOCATED(uR)) ALLOCATE(uR(dim)); uR = 0.0_pr
    rhoL  = 1.0_pr
    uL(1) = 2.0_pr
    pL    = 1.0_pr

    call input_real ('rhoL',rhoL,'default', 'Inflow density')
    call input_real ('uL',uL(1),'default', 'Inflow x-component of velocity')
    call input_real ('pL',pL,'default', 'Inflow pressure')


    IF(imask_obstacle) THEN
       smooth_dist = .FALSE. ! default value
       call input_logical ('smooth_dist',smooth_dist,'default','T smoothes distance function prior finding the normal')

       deltadel_loc = 1.0_pr
       call input_real ('deltadel_loc',deltadel_loc,'default', 'sets final diffusion thickness using spatial resolution (dx, dy, or dz), number of points across final thickness')

       call input_real ('delta_conv',delta_conv,'stop', 'transition thickness of the convective penalization zone')

       call input_real ('delta_diff',delta_diff,'stop', 'transition thickness of the diffusion penalization zone')

       call input_real ('shift_conv',shift_conv,'stop', 'thickness when convections is off')

       call input_real ('shift_diff',shift_diff,'stop', 'thickness of the diffusion is on')
       
       saveUn = .FALSE.
       call input_logical ('saveUn',saveUn,'default', 'saveUn: saves normal and tangential components of the velocity')
    END IF
    
    call input_real ('shock',shock,'stop', 'shock location')

 
    IF (mydt_orig < 0.0_pr) mydt_orig = dt
    IF (IC_restart_mode==1) dt_original = mydt_orig

!!$    IF (buffRe) imask_obstacle=.TRUE.
    IF (NOT(buffRe)) adaptbuffRe=.FALSE.
    dobuffRe = buffRe
    IF (buffRewait) dobuffRe = .FALSE.

    IF (Nspec<2) adaptspc=.FALSE. 
    IF (Nspec<2) adaptspconly=.FALSE. 
    IF (Nspec<2) adaptGradY=.FALSE. 
    IF (Nspec<2) saveGradY=.FALSE. 
    IF (Nspec<2) splitFBpress=.FALSE. 
    Nspecm = Nspec - 1

    IF (NOT(evolBC)) LODIit=0 
    IF (evolBC) BCver=-1    
    IF (evolBC .AND. LODIit==2) BCver=100
    IF (evolBC) cnvtBC=.FALSE.
    IF (cnvtBC) BCver=-1
    IF (buffBC) BCver=-1
    IF (LODIit == 4) globFlux=0

    IF (adaptAt) At=At*(1.0_pr/(1.0_pr-2.0_pr*peakchange))

    gr_in(:) = 0.0_pr  !g's by species, then dimension
    IF (GRAV) gr_in(1:Nspec) = 1.0_pr
    gr_in(:) = gr_in(:)/Fr**2

    MW_in(:) = 1.0_pr
    MW_in(1) = 1.0_pr+At
    IF (Nspec>1) MW_in(2) = 1.0_pr-At
    YR(:) = MW_in(:) / SUM(MW_in(:))
    cp_in(:) = 1.0_pr/MW_in(:)*gamm(:)/(gamm(:)-1.0_pr)
    gammR = SUM(cp_in(:)*YR(:))/( SUM(cp_in(:)*YR(:))-SUM(YR(:)/MW_in(:)) )
    mu_in(:) = 1.0_pr/Re
    kk_in(:) = 1.0_pr/Re/Pra*gammR/(gammR-1.0_pr)
    bD_in(:) = 1.0_pr/Re/Sc

    gammBC(:)=gamm(1)
    IF (Nspec>1) gammBC(2)=gamm(2)

!!$    Tconst = 1.0_pr/Ma**2
!!$    Pint = 1.0_pr/Ma**2

    Tconst = 1.0_pr
    Pint = 1.0_pr
    pBcoef=pBsig

     IF (NOT(ALLOCATED(Lxyz))) ALLOCATE(Lxyz(1:dim))

     IF (NOT(ALLOCATED(Y0top))) ALLOCATE(Y0top(1:Nspec))
     IF (NOT(ALLOCATED(Y0bot))) ALLOCATE(Y0bot(1:Nspec))
     IF (NOT(ALLOCATED(rhoYtopBC))) ALLOCATE(rhoYtopBC(1:Nspecm))
     IF (NOT(ALLOCATED(rhoYbotBC))) ALLOCATE(rhoYbotBC(1:Nspecm))
     IF (NOT(ALLOCATED(rhoYtop))) ALLOCATE(rhoYtop(1:Nspecm))
     IF (NOT(ALLOCATED(rhoYbot))) ALLOCATE(rhoYbot(1:Nspecm))

     pi=4.0_pr*ATAN(1.0_pr)
     Lxyz(1:dim)=xyzlimits(2,1:dim)-xyzlimits(1,1:dim)

    halfdomsize = (xyzlimits(2,1)-xyzlimits(1,1))/2.0_pr
    IF (buffRed(1)<0.0_pr) THEN
       buffRed(1)=halfdomsize*buffRef(1)
    ELSE
       buffRef(1)=buffRed(1)/halfdomsize
    END IF
    IF (ANY(buffRed(2:4)<0.0_pr)) THEN
       buffRef(2)=MAX(0.0_pr,MIN(1.0_pr,buffRef(2)))
       buffRef(3)=MAX(0.0_pr,MIN(1.0_pr-buffRef(2),buffRef(3)))
       buffRef(4)=MAX(0.0_pr,MIN(1.0_pr-SUM(buffRef(2:3)),buffRef(4)))
       buffRef(5)=MAX(0.0_pr,    1.0_pr-SUM(buffRef(2:4)))
       buffRed(2:5)=buffRef(2:5)*buffRed(1)
    ELSE
       buffRed(2)=MIN(buffRed(2),buffRed(1))
       buffRed(3)=MIN(buffRed(3),buffRed(1)-buffRed(2))
       buffRed(4)=MIN(buffRed(4),buffRed(1)-SUM(buffRed(2:3)))
       buffRed(5)=MAX(0.0_pr  ,buffRed(1)-SUM(buffRed(2:4)))
       buffRef(2:5)=buffRed(2:5)/buffRed(1)
    END IF
    IF (verb_level>0 .AND. par_rank==0) PRINT *,'FOR bufferfunc, buffRed=', buffRed
    IF (verb_level>0 .AND. par_rank==0) PRINT *,'FOR bufferfunc, buffRef=', buffRef

    IF (buffcvd(1)<0.0_pr) THEN
       buffcvd(1)=halfdomsize*buffcvf(1)
    ELSE
       buffcvf(1)=buffcvd(1)/halfdomsize
    END IF
    IF (ANY(buffcvd(2:4)<0.0_pr)) THEN
       buffcvf(2)=MAX(0.0_pr,MIN(1.0_pr,buffcvf(2)))
       buffcvf(3)=MAX(0.0_pr,MIN(1.0_pr-buffcvf(2),buffcvf(3)))
       buffcvf(4)=MAX(0.0_pr,MIN(1.0_pr-SUM(buffcvf(2:3)),buffcvf(4)))
       buffcvf(5)=MAX(0.0_pr,    1.0_pr-SUM(buffcvf(2:4)))
       buffcvd(2:5)=buffcvf(2:5)*buffcvd(1)
    ELSE
       buffcvd(2)=MIN(buffcvd(2),buffcvd(1))
       buffcvd(3)=MIN(buffcvd(3),buffcvd(1)-buffcvd(2))
       buffcvd(4)=MIN(buffcvd(4),buffcvd(1)-SUM(buffcvd(2:3)))
       buffcvd(5)=MAX(0.0_pr  ,buffcvd(1)-SUM(buffcvd(2:4)))
       buffcvf(2:5)=buffcvd(2:5)/buffcvd(1)
    END IF
    IF (verb_level>0 .AND. par_rank==0) PRINT *,'FOR bufferfunc, buffcvd=', buffcvd
    IF (verb_level>0 .AND. par_rank==0) PRINT *,'FOR bufferfunc, buffcvf=', buffcvf

    IF (buffbfd(1)<0.0_pr) THEN
       buffbfd(1)=halfdomsize*buffbff(1)
    ELSE
       buffbff(1)=buffbfd(1)/halfdomsize
    END IF
    IF (ANY(buffbfd(2:4)<0.0_pr)) THEN
       buffbff(2)=MAX(0.0_pr,MIN(1.0_pr,buffbff(2)))
       buffbff(3)=MAX(0.0_pr,MIN(1.0_pr-buffbff(2),buffbff(3)))
       buffbff(4)=MAX(0.0_pr,MIN(1.0_pr-SUM(buffbff(2:3)),buffbff(4)))
       buffbff(5)=MAX(0.0_pr,    1.0_pr-SUM(buffbff(2:4)))
       buffbfd(2:5)=buffbff(2:5)*buffbfd(1)
    ELSE
       buffbfd(2)=MIN(buffbfd(2),buffbfd(1))
       buffbfd(3)=MIN(buffbfd(3),buffbfd(1)-buffbfd(2))
       buffbfd(4)=MIN(buffbfd(4),buffbfd(1)-SUM(buffbfd(2:3)))
       buffbfd(5)=MAX(0.0_pr  ,buffbfd(1)-SUM(buffbfd(2:4)))
       buffbff(2:5)=buffbfd(2:5)/buffbfd(1)
    END IF
    IF (verb_level>0 .AND. par_rank==0) PRINT *,'FOR bufferfunc, buffbfd=', buffbfd
    IF (verb_level>0 .AND. par_rank==0) PRINT *,'FOR bufferfunc, buffbff=', buffbff

    buffDomFrac = MIN(buffcvf(1),buffbff(1))

    IF (ALLOCATED(bndvals)) DEALLOCATE(bndvals)
    ALLOCATE(bndvals(dim+Nspec+3,2*dim))  ! 1-flow variable, 2-boundary

  END SUBROUTINE user_read_input

  !
  ! calculate any additional variables
  ! 
  ! arg 
  ! flag  - 0 calledwhile adapting to IC, 1 - called in main time integration loop
  !
  ! These additiona variables are calculated and left in real space.
  !
  SUBROUTINE user_additional_vars( t_local, flag )
    IMPLICIT NONE
    REAL (pr), INTENT (IN) ::  t_local
    INTEGER , INTENT(IN) :: flag ! 0- called during adaption to IC, 1 called during main integration loop
    REAL (pr), DIMENSION (nwlt,dim)     :: norm, Utan, Unorm
    REAL (pr) :: cp, R, mygr, cmax, uval
    INTEGER :: l, i, j, myiter
    REAL (pr), DIMENSION(1,nwlt,MAX(dim,Nspecm)) :: du, d2u    
    REAL (pr) :: iterdiff, initdiff, curtau, fofn, deln, delnold, delfofn
    INTEGER, PARAMETER :: meth=1
    REAL (pr), DIMENSION(nwlt,dim*2) :: bigU

    IF (adaptbuffRe) THEN

       IF (dobuffRe) THEN
          u(:,nbRe) = user_chi (nwlt, t_local) 
       ELSE
          u(:,nbRe) = 1.0_pr
       END IF 
    END IF

    d2u(1,:,2) = cp_in(Nspec) 
    d2u(1,:,1) = 1.0_pr/MW_in(Nspec) 
    IF (Nspec>1) d2u(1,:,2) = (1.0_pr - SUM(u(:,nspc(1):nspc(Nspecm)),DIM=2)/u(:,nden))*cp_in(Nspec)
    IF (Nspec>1) d2u(1,:,1) = (1.0_pr - SUM(u(:,nspc(1):nspc(Nspecm)),DIM=2)/u(:,nden))/MW_in(Nspec)
    DO l=1,Nspecm
        d2u(1,:,2) = d2u(1,:,2) + u(:,nspc(l))/u(:,nden)*cp_in(l) !cp
        d2u(1,:,1) = d2u(1,:,1) + u(:,nspc(l))/u(:,nden)/MW_in(l) !R
    END DO

    u(:,nprs) = ( u(:,neng)-0.5_pr*SUM(u(:,nvel(1):nvel(dim))**2,DIM=2)/u(:,nden))/(d2u(1,:,2)/d2u(1,:,1)-1.0_pr)  ! p
    IF (adapttmp) u(:,nton) = u(:,nprs)/u(:,nden)/d2u(1,:,1)
    IF (adaptvelonly) THEN
       DO i=1,dim
          u(:,nvon(i)) = u(:,nvel(i))/u(:,nden)
       END DO
    END IF
    IF (adaptspconly) THEN
       DO i=1,Nspecm
          u(:,nson(i)) = u(:,nspc(i))/u(:,nden)
       END DO
    END IF
    IF (savebuoys) THEN
       u(:,ndpx) = u(:,nprs)
       DO i=1,Nspecm
          u(:,nspc(i)) = u(:,nspc(i))/u(:,nden)
       END DO
       IF (Nspec>1) THEN
          CALL dpdx (u(:,ndpx),nwlt,u(:,nspc(1):nspc(Nspecm)),2*methpd+meth,j_lev,splitFBpress)
       ELSE
          CALL dpdx (u(:,ndpx),nwlt,u(:,1),2*methpd+meth,j_lev,.FALSE.)
       END IF
       DO i=1,Nspecm
          u(:,nspc(i)) = u(:,nspc(i))*u(:,nden)
       END DO
       DO i=1,nwlt
          mygr=gr_in(Nspec)
          IF (Nspec>1) mygr=mygr*(1.0_pr-SUM(u(i,nspc(1):nspc(Nspecm))) /u(i,nden)   ) 
          DO j=1,Nspecm
             mygr=mygr+gr_in(j)*u(i,nspc(j))   /u(i,nden)   
          END DO
          u(i,nboy) =  u(i,ndpx) + mygr*u(i,nden)
       END DO
       IF (par_rank.EQ.0 .AND. verb_level>0) PRINT *, 'MAXABS of dp/dx+rho*g:', MAXVAL(ABS(u(:,nboy)))
    END IF
    IF( adaptMagVort .OR. saveMagVort .OR. adaptComVort .OR. saveComVort .OR. adaptMagVel .OR. saveMagVel .OR. adaptNormS .OR. saveNormS ) THEN
       DO i=1,dim
          du(1,:,i) = u(:,nvel(i))/u(:,nden)
       END DO
       CALL bonusvars_vel(du(1,:,1:dim))  !4extra - call it
    END IF
    IF( adaptGradY .OR. saveGradY) THEN
       DO i=1,Nspecm
          du(1,:,i) = u(:,nspc(i))/u(:,nden)
       END DO
       CALL bonusvars_spc(du(1,:,1:Nspecm))  !4extra - call it
    END IF

       IF (modczn) THEN
          IF (t>tczn) THEN
             modczn=.FALSE.
             xyzzone(1,1) = czn_min
             xyzzone(2,1) = czn_max            
            IF (par_rank.EQ.0 .AND. verb_level>0) THEN
             PRINT *, '*****************************************************'
             PRINT *, 'FINALIZING TEMPORARY COORD_ZONE'
             PRINT *, 'COORD_ZONE OUTSIDE OF x=[',xyzzone(1,1),',',xyzzone(2,1),']'
             PRINT *, 'TIME: ', t
             PRINT *, 'STOPPING TIME FOR COORD_ZONE: ', tczn
             PRINT *, '*****************************************************'
            END IF
          END IF
       ELSE  
          IF (tfacczn>0.0_pr .AND. itfacczn>0.0_pr .AND. t>itczn) THEN
             itczn=t_end*10.0_pr
             czn_min = xyzzone(1,1)
             czn_max = xyzzone(2,1)
             xyzzone(1,1) = -Lczn
             xyzzone(2,1) =  Lczn
             cczn = MINVAL(SQRT(gamm/MW_in*Tconst))
             tczn = MAXVAL(ABS(xyzlimits(:,1)))/cczn*tfacczn
            IF (par_rank.EQ.0 .AND. verb_level>0) THEN
             PRINT *, '*****************************************************'
             PRINT *, 'APPLYING TEMPORARY COORD_ZONE'
             PRINT *, 'COORD_ZONE OUTSIDE OF x=[',-Lczn,',',Lczn,']'
             PRINT *, 'MINIMUM WAVE SPEED: ', cczn
             PRINT *, 'MAXIMUM DISTANCE: ', MAXVAL(ABS(xyzlimits(:,1)))
             PRINT *, 'TIME: ', t
             PRINT *, 'STARTING TIME FOR COORD_ZONE: ', itczn
             PRINT *, 'TIME SAFETY FACTOR: ', tfacczn
             PRINT *, 'STOPPING TIME FOR COORD_ZONE: ', tczn
             PRINT *, '*****************************************************'
            END IF
             IF (t<tczn) THEN
                modczn = .TRUE.
             ELSE
                IF (par_rank.EQ.0) PRINT *, '!!!TOO LATE TO START COORD_ZONE: t>tczn!!!'
             END IF
          END IF  
       END IF

       IF (buffRe .AND. buffRewait) THEN
          IF ( (t>xyzlimits(1,1)/c0(2)*(1.0_pr+waitrat)) .AND. (t>xyzlimits(2,1)/c0(1)*(1.0_pr+waitrat)) ) dobuffRe = .TRUE.
       END IF

       IF (savepardom) u(:,ndom) = REAL(par_rank,pr)

       IF (imask_obstacle .AND. (.NOT.interpolate_dist .AND. ndist > 0)) THEN
          CALL user_dist (nwlt, t_local, DISTANCE=u(:,ndist) ) 
       END IF

       IF (imask_obstacle) u(:,nmsk) = user_chi(nwlt,t_local)
       
      
       IF (imask_obstacle .AND. saveUn) THEN
          CALL user_dist (nwlt, t, NORMAL=norm)
          
          Utan(:, 1) = 0.0_pr
          DO i = 1, dim
             Utan (:, 1) = Utan(:,1)+ u(:, nvel(i))/u(:,nden)*norm(:,i)
          END DO
          DO i = 1, dim
             Unorm (:, i) = Utan(:,1)*norm(:,i)
          END DO
          DO i = 1, dim
             Utan(:,i)=u(:, nvel(i))/u(:,nden)-Unorm(:,i)
          END DO
          u(:,nUn(1)) = SQRT(SUM(Unorm(:,1:dim)**2,DIM=2))
          u(:,nUn(2)) = SQRT(SUM(Utan(:,1:dim)**2,DIM=2))
       END IF


       IF (saveUcvt) THEN
          CALL user_bufferfunc(bigU, nwlt, buffcvd, polyconv)
          u(:,ncvtU) = SUM(bigU(:,:),2)
       END IF

  END SUBROUTINE user_additional_vars

  SUBROUTINE bonusvars_vel(velvec)   !4extra - this whole subroutine
    IMPLICIT NONE
    REAL (pr), INTENT(IN) :: velvec(nwlt,dim) !velocity
    REAL (pr) :: tmp(nwlt,2*dim) !tmp for vorticity
    INTEGER :: i

    IF( adaptMagVort .OR. saveMagVort .OR. adaptComVort .OR. saveComVort .OR. adaptMagVel .OR. saveMagVel .OR. adaptNormS .OR. saveNormS) THEN
       tmp = 0.0_pr
       ! Calculate the magnitude vorticity
       IF( adaptMagVort .OR. saveMagVort .OR. adaptComVort .OR.  saveComVort ) THEN
          CALL cal_vort (velvec, tmp(:,1:3-MOD(dim,3)), nwlt)
          IF ( adaptMagVort .OR. saveMagVort )  u(:,nmvt) = SQRT(tmp(:,1)**2.0_pr + tmp(:,2)**2.0_pr + tmp(:,3)**2.0_pr )
          IF ( adaptComVort .OR. saveComVort )  THEN
             DO i=1,3
                u(:,ncvt(4-i)) = tmp(:,4-i)
             END DO
          END IF
       END IF
       ! Calculate the magnitude of Sij (sqrt(SijSij) )
       ! s(:,1) = s_11
       ! s(:,2) = s_22
       ! s(:,3) = s_33
       ! s(:,4) = s_12
       ! s(:,5) = s_13
       ! s(:,6) = s_23
       tmp = 0.0_pr
       IF( adaptNormS .OR. saveNormS) THEN
          CALL Sij( velvec ,nwlt, tmp(:,1:2*dim), u(:,nnms), .TRUE. )
       END IF
       ! Calculate the magnitude of velocity
       IF( adaptMagVel .OR. saveMagVel) THEN
          u(:,nmvl) = 0.5_pr*(SUM(velvec(:,:)**2.0_pr,DIM=2))
       END IF
    END IF
  END SUBROUTINE bonusvars_vel

  SUBROUTINE bonusvars_spc(spcvec)   !4extra - this whole subroutine
    IMPLICIT NONE
    REAL (pr), INTENT(IN) :: spcvec(nwlt,Nspecm) !species
    REAL (pr), DIMENSION(Nspecm,nwlt,dim) :: du, d2u    
    INTEGER :: i
    INTEGER, PARAMETER :: meth=1

    IF( adaptGradY .OR. saveGradY) THEN
       CALL c_diff_fast(spcvec(:,1:Nspecm), du(1:Nspecm,:,:), d2u(1:Nspecm,:,:), j_lev, nwlt,meth, 10, Nspecm, 1, Nspecm)  !dY/dx
       DO i=1,Nspecm
          u(:,ngdy(i)) = SUM(du(i,:,:)**2,DIM=2) !|dY/dx_i*dY/dx_i|
       END DO
    END IF
  END SUBROUTINE bonusvars_spc

  !
  ! calculate any additional scalar variables
  ! 
  ! arg 
  ! flag  - 0 calledwhile adapting to IC, 1 - called in main time integration loop
  !
  SUBROUTINE user_scalar_vars( flag )
    IMPLICIT NONE
    INTEGER , INTENT(IN) :: flag ! 0- called during adaption to IC, 1 called during main integration loop

  END SUBROUTINE user_scalar_vars

  !
  !************ Calculating Scales ***************************
  !
  ! Note the order of the components in the scl array
  ! correspond to u_tn, v_tn, w_tn, u_tn-1, v_tn-1, w_tn-1, u_tn-2, v_tn-2, w_tn-2
  ! 
  SUBROUTINE user_scales(flag,use_default, u_loc, nlocal, ne_local, l_n_var_adapt , l_n_var_adapt_index, &
       scl, scl_fltwt) !add
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: flag ! 0 during initial adaptation, then 1 during time advancement
    LOGICAL , INTENT(INOUT) :: use_default
    INTEGER, INTENT (IN) :: nlocal, ne_local
    REAL (pr), DIMENSION (1:nlocal,1:ne_local), INTENT (IN) :: u_loc
    LOGICAL , INTENT (IN) :: l_n_var_adapt(ne_local)
    INTEGER , INTENT (IN) :: l_n_var_adapt_index(1:ne_local)
    REAL (pr), DIMENSION (1:ne_local), INTENT (INOUT) :: scl
    REAL (pr), DIMENSION (1:ne_local) :: mean
    REAL (pr) ,  INTENT(IN) ::scl_fltwt !weight for temporal filter on scl
    REAL (pr) :: floor, tmp,tmpmax,tmpmin,tmpsum
    INTEGER :: i, ie, ie_index, tmpint
    REAL (pr) ,save, ALLOCATABLE, DIMENSION (:) :: scl_old
    LOGICAL  ,save :: startup_init = .TRUE.

    !
    ! Ignore the output of this routine and use default scales routine
    !
    use_default = .FALSE. 

    IF( .NOT. use_default ) THEN
       !IF (par_rank.EQ.0) PRINT *,'Use user_scales() <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<'
       !
       ! ALLOCATE scl_old if it was not done previously in user_scales
       !
       IF( startup_init .AND. .NOT. ALLOCATED(scl_old) ) THEN
          ALLOCATE( scl_old(1:ne_local) )
          scl_old = 0.0_pr
       END IF

       floor = 1.0e-12_pr
       scl   = 1.0_pr
       !
       ! Calculate scl per component
       !
       mean = 0.0_pr
       IF(.TRUE.) THEN
          DO ie=1,ne_local
!!$             IF(l_n_var_adapt(ie)) THEN
                ie_index = l_n_var_adapt_index(ie)
                IF( Scale_Meth == 1 ) THEN ! Use Linf scale
                   tmpmax=MAXVAL ( u_loc(1:nlocal,ie_index) )
                   tmpmin=MINVAL ( u_loc(1:nlocal,ie_index) )
                   CALL parallel_global_sum(REALMAXVAL=tmpmax)
                   CALL parallel_global_sum(REALMINVAL=tmpmin)
                   mean(ie)= 0.5_pr*(tmpmax-tmpmin)
                ELSE IF ( Scale_Meth == 2 ) THEN ! Use L2 scale
                   tmpsum=SUM( u_loc(1:nlocal,ie_index) )
                   tmpint=nlocal
                   CALL parallel_global_sum(REAL=tmpsum)
                   CALL parallel_global_sum(INTEGER=tmpint)
                   mean(ie)= tmpsum/REAL(tmpint,pr)
                ELSE IF ( Scale_Meth == 3 ) THEN ! Use L2 scale using dA
                   tmpsum=SUM( u_loc(1:nlocal,ie_index)*dA )
                   CALL parallel_global_sum(REAL=tmpsum)
                   mean(ie)= tmpsum/ sumdA_global 
                END IF
!!$             END IF
          END DO          
       END IF
       mean = 0.0_pr
       DO ie=1,ne_local
!!$          IF(l_n_var_adapt(ie)) THEN
             ie_index = l_n_var_adapt_index(ie)
             IF( Scale_Meth == 1 ) THEN ! Use Linf scale
                scl(ie)= MAXVAL ( ABS( u_loc(1:nlocal,ie_index) - mean(ie) ) )
                CALL parallel_global_sum( REALMAXVAL=scl(ie) )
             ELSE IF ( Scale_Meth == 2 ) THEN ! Use L2 scale
                scl(ie)= SUM( (u_loc(1:nlocal,ie_index)-mean(ie))**2 )
                tmpint=nlocal
                CALL parallel_global_sum( REAL=scl(ie) )
                CALL parallel_global_sum(INTEGER=tmpint)
                scl(ie)=SQRT(scl(ie)/REAL(tmpint,pr))
             ELSE IF ( Scale_Meth == 3 ) THEN ! Use L2 scale using dA
                scl(ie)= SUM( (u_loc(1:nlocal,ie_index)-mean(ie))**2*dA )
                CALL parallel_global_sum(REAL=scl(ie))
                scl(ie)=SQRT(scl(ie)/sumdA_global)
             ELSE
                PRINT *, 'Error in Scales(), Unknown Scale_Meth type: ', Scale_Meth
                PRINT *, 'Known Scale_Meth types: 1 = Linf scale, 2 = L2 scale '
                PRINT *, 'Exiting ...'
                stop
             END IF
             
        !     IF (par_rank.EQ.0) THEN
        !        WRITE (6,'("Scaling before taking vector(1:dim) magnitude")')
        !        WRITE (6,'("Scale factor eqn", I4 ," = ", es11.4 ," scalecoeff = ",es11.4 )') &
        !             ie, scl(ie), scaleCoeff(ie)
        !     END IF
!!$         END IF
       END DO
       
       !
       ! take appropriate vector norm over scl(1:dim)
       IF (adaptvelonly) THEN
          IF( Scale_Meth == 1 ) THEN ! Use Linf scale
             scl(nvon(1):nvon(dim)) = MAXVAL(scl(nvon(1):nvon(dim)))
          ELSE IF ( Scale_Meth == 2 ) THEN ! Use L2 scale
             scl(nvon(1):nvon(dim)) = SQRT(SUM(scl(nvon(1):nvon(dim))**2))
          ELSE IF ( Scale_Meth == 3 ) THEN ! Use L2 scale using dA
             scl(nvon(1):nvon(dim)) = SQRT(SUM(scl(nvon(1):nvon(dim))**2))
          END IF
       END IF
       IF( Scale_Meth == 1 ) THEN ! Use Linf scale
          scl(nvel(1):nvel(dim)) = MAXVAL(scl(nvel(1):nvel(dim)))
       ELSE IF ( Scale_Meth == 2 ) THEN ! Use L2 scale
          scl(nvel(1):nvel(dim)) = SQRT(SUM(scl(nvel(1):nvel(dim))**2)) !now velocity vector length, rather than individual component
       ELSE IF ( Scale_Meth == 3 ) THEN ! Use L2 scale using dA
          scl(nvel(1):nvel(dim)) = SQRT(SUM(scl(nvel(1):nvel(dim))**2)) !now velocity vector length, rather than individual component
       END IF
       !
       ! Print out new scl
       !
       IF (adaptbuffRe .AND. maskscl>0.0_pr) scl(nbRe)=maskscl
       IF (adapttmp .AND. tempscl>floor) scl(nton)=tempscl
       IF (adapttmp .AND. adaptvelonly .AND. ABS(tempscl)<floor) scl(nton)=scl(nvon(1))
       IF (adaptspconly .AND. specscl>0.0_pr) scl(nson(1):nson(Nspecm))=specscl
       DO ie=1,ne_local
!!$          IF(l_n_var_adapt(ie)) THEN
             !this is done if one of the variable is exactly zero inorder not to adapt to the noise
             IF(scl(ie) .le. floor) scl(ie)=1.0_pr 
             tmp = scl(ie)
             ! temporally filter scl
             IF(.not. startup_init ) scl(ie) = scl_fltwt * scl_old(ie) + (1.0_pr - scl_fltwt) * scl(ie)
             scl(ie) = scaleCoeff(ie) * scl(ie)
             IF (par_rank.EQ.0 .AND. verb_level .GT. 0) THEN
                WRITE (6,'("Scale factor eqn", I4 ," = ", es11.4 ," scalecoeff = ",es11.4 )') &
                     ie, scl(ie), scaleCoeff(ie)
                WRITE (6,'("(before filtering = ",es11.4,")" )') tmp
             END IF
!!$          END IF
       END DO
       
       IF(imask_obstacle) scl(nmsk) = 1.0_pr

       scl_old = scl !save scl for this time step
       startup_init = .FALSE.
    END IF

  END SUBROUTINE user_scales

  SUBROUTINE user_cal_cfl (use_default, ulocal, cfl_out)
    USE precision
    USE sizes
    USE pde
    IMPLICIT NONE
    LOGICAL , INTENT(INOUT) :: use_default
    REAL (pr),                                INTENT (INOUT) :: cfl_out
    REAL (pr), DIMENSION (nwlt,n_integrated), INTENT (IN)    :: ulocal
    INTEGER                    :: i
    REAL (pr)                  :: flor
    REAL (pr), DIMENSION(nwlt,dim) :: cfl, cfl_conv, cfl_acou
    REAL (pr), DIMENSION(dim,nwlt) :: h_arr
    REAL (pr), DIMENSION(nwlt) :: gam, sos
    INTEGER :: l, mynwlt_global, fullnwlt, maxnwlt, minnwlt, avgnwlt

    IF (it>0) dt_original = mydt_orig

    use_default = .FALSE.

    flor = 1e-12_pr
    cfl_out = flor

    CALL get_all_local_h (h_arr)

    gam(:) = cp_in(Nspec)
    sos(:) = 1.0_pr/MW_in(Nspec) 
    IF (Nspec>1) gam(:) = (1.0_pr-SUM(ulocal(:,nspc(1):nspc(Nspecm)),DIM=2)/ulocal(:,nden))*cp_in(Nspec) !cp_Nspec
    IF (Nspec>1) sos(:) = (1.0_pr-SUM(ulocal(:,nspc(1):nspc(Nspecm)),DIM=2)/ulocal(:,nden))/MW_in(Nspec) !R_Nspec
    DO l=1,Nspecm
       gam(:) = gam(:) + ulocal(:,nspc(l))/ulocal(:,nden)*cp_in(l) !cp
       sos(:) = sos(:) + ulocal(:,nspc(l))/ulocal(:,nden)/MW_in(l) !R
    END DO
    gam(:) = gam(:)/(gam(:)-sos(:))
    sos(:) = SQRT(gam(:)*(gam(:)-1.0_pr)*(ulocal(:,neng)-0.5_pr*SUM(ulocal(:,nvel(1):nvel(dim))**2,DIM=2)/ulocal(:,nden))/ulocal(:,nden))! spd of snd = sqrt(gamma p/rho)
    maxsos = MAXVAL(sos(:))
    PRINT *, 'minval maxval sos'
    PRINT *, MINVAL(sos), MAXVAL(sos), MINVAL(gam), MAXVAL(gam)
    CALL parallel_global_sum(REALMAXVAL=maxsos)

    maxMa = MAXVAL((1.0-penal)*SQRT(SUM(ulocal(:,nvel(1):nvel(dim))**2,DIM=2)/ulocal(:,nden)**2)/sos(:)*SQRT(gam(:)))
    CALL parallel_global_sum(REALMAXVAL=maxMa)

    DO i=1,dim
       cfl(:,i)      = ( ABS(ulocal(:,nvel(i))/ulocal(:,nden))+sos(:) ) * dt/h_arr(i,:)
       cfl_conv(:,i) = ( ABS(ulocal(:,nvel(i))/ulocal(:,nden))        ) * dt/h_arr(i,:)
       cfl_acou(:,i) = (                                       sos(:) ) * dt/h_arr(i,:)
       IF(imask_obstacle) THEN
          cfl(:,i)=cfl(:,i)*(1.0_pr-penal)
          cfl_conv(:,i)=cfl_conv(:,i)*(1.0_pr-penal)
          cfl_acou(:,i)=cfl_acou(:,i)*(1.0_pr-penal)
       END IF
    END DO
    CFLreport = MAXVAL(cfl(:,:))
    CFLconv   = MAXVAL(cfl_conv(:,:))
    CFLacou   = MAXVAL(cfl_acou(:,:))
    CALL parallel_global_sum(REALMAXVAL=CFLreport)
    CALL parallel_global_sum(REALMAXVAL=CFLconv)
    CALL parallel_global_sum(REALMAXVAL=CFLacou)

    mynwlt_global = nwlt
    fullnwlt = PRODUCT(mxyz(1:dim)*2**(j_lev-1) + 1 - prd(:))
    CALL parallel_global_sum (INTEGER=mynwlt_global)
    avgnwlt = mynwlt_global/par_size 
    maxnwlt = nwlt
    minnwlt = -nwlt
    CALL parallel_global_sum(INTEGERMAXVAL=maxnwlt)
    CALL parallel_global_sum(INTEGERMAXVAL=minnwlt)
    minnwlt=-minnwlt
    MAreport = maxMa
    IF (convcfl) THEN
       cfl_out = MAX(cfl_out, CFLconv)
    ELSE
       cfl_out = MAX(cfl_out, CFLreport)
    END IF
    IF (par_rank.EQ.0) THEN
       PRINT *, '**********************************************************'
       PRINT *, 'case:    ', file_gen(1:LEN_TRIM(file_gen)-1)
       PRINT *, 'max Ma = ', maxMa
       PRINT *, 'CFL    = ', cfl_out
       PRINT *, 'it     = ', it,             '  t       = ', t
       PRINT *, 'iwrite = ', iwrite-1,       '  dt      = ', dt 
       PRINT *, 'j_mx   = ', j_mx,           '  dt_orig = ', dt_original
       PRINT *, 'j_lev  = ', j_lev,          '  twrite  = ', twrite
       PRINT *, 'nwlt_g = ', mynwlt_global,  '  cpu     = ', timer_val(2)
       PRINT *, 'n_all  = ', fullnwlt,       '  n%      = ', REAL(mynwlt_global,pr)/REAL(fullnwlt,pr)
       PRINT *, 'nwlt   = ', nwlt,           '  eps     = ', eps
       PRINT *, 'nwlt_mx= ', maxnwlt,        '  nwlt_av = ', avgnwlt
       PRINT *, 'nwlt_mn= ', minnwlt,        '  p_size  = ', par_size
       PRINT *, '**********************************************************'
    END IF
    maxMa = MIN(maxMa,1.0_pr)
  END SUBROUTINE user_cal_cfl

  SUBROUTINE user_init_sgs_model( )
    IMPLICIT NONE
  END SUBROUTINE user_init_sgs_model

  SUBROUTINE  user_sgs_force (u_loc, nlocal)
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: nlocal
    REAL (pr), DIMENSION (nlocal,n_integrated), INTENT (INOUT) :: u_loc
  END SUBROUTINE  user_sgs_force

  FUNCTION user_sound_speed (u, neq, nwlt)
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: nwlt, neq
    REAL (pr), DIMENSION (nwlt,neq), INTENT(IN) :: u
    REAL (pr), DIMENSION (nwlt) :: user_sound_speed
    user_sound_speed(:) = SQRT( gamm(1)*(gamm(1)-1.0_pr)*(u(:,neng)-0.5_pr*SUM(u(:,nvel(1:dim))**2,DIM=2)/u(:,nden))/u(:, nden)) ! pressure
!!$    PRINT *, "c = ", MINVAL(user_sound_speed), MAXVAL(user_sound_speed), "nden = ", nden, MINVAL(u(:, nden)), MAXVAL(u(:, nden))
  END FUNCTION user_sound_speed

  SUBROUTINE  user_pre_process
    IMPLICIT NONE
    INTEGER :: i, l
    IF (boundY) THEN
       DO i=1,nwlt
          IF (Nspec>1) u(i,nspc(1)) = MIN(MAX(u(i,nspc(1)),0.0_pr),u(i,nden))
       END DO
       DO l=2,Nspecm
          DO i=1,nwlt
             u(i,nspc(l)) = MIN(MAX(u(i,nspc(l)),0.0_pr),u(i,nden)-SUM(u(i,nspc(1):nspc(l-1))))
          END DO
       END DO
    END IF
  END SUBROUTINE user_pre_process

  SUBROUTINE  user_post_process
    IMPLICIT NONE
    CALL user_pre_process
  END SUBROUTINE user_post_process


END MODULE user_case
