MODULE user_case
  !
  ! Case elliptic poisson 
  USE precision
  USE wlt_vars
  USE wavelet_filters_mod
  USE elliptic_mod
  USE elliptic_vars
  USE wlt_trns_mod									
  USE wlt_trns_vars
  USE wlt_trns_util_mod
  USE io_3d_vars
  USE util_mod
  USE util_vars
  USE share_consts
  USE pde
  USE sizes
  USE share_kry
  USE vector_util_mod
  USE field
  USE input_file_reader
  !
  ! case specific variables
  !
  INTEGER n_var_vorticity ! start of vorticity in u array
  INTEGER n_var_pressure  ! start of pressure in u array


  !
  ! local variables
  !
  REAL(pr)  :: nu, alpha, beta, r0, r1, omega
  REAL(pr)  :: eta, eta_min, eta_max
  REAL(pr)  :: theta_loc(1:3)   !oriantation of axis for ellipsoid 
  REAL(pr)  :: nu_loc(0:3)      ! scales for principal axis of ellipsiod
  REAL(pr)  :: x1_loc(1:3), x0_loc(1:3)
  REAL(pr)  :: delta
  INTEGER   :: Kprd(1:3)
  LOGICAL   :: divgrad, numerical_solution, space_time, adapt_on_mask

  ! debug flag to be read from .inp file to test database independent interpolation
  LOGICAL, PRIVATE :: test_interpolate

CONTAINS

  !
  ! In user_setup_pde() we setup how many variables are integrated, which are interpolated
  ! to the next times step and if any exeact solution exists to compare to during the run.
  ! We also set the variable names that are used when the result files are written.
  !
  ! The following variables must be setup in this routine:
  !
  !
  ! n_integrated     ! first n_integrated eqns will be acted on for time integration
  ! n_var_additional ! 
  ! n_var
  !
  !
  !
  !
  SUBROUTINE  user_setup_pde ( VERB ) 
    IMPLICIT NONE
    LOGICAL, OPTIONAL :: VERB         ! print debug info
    LOGICAL :: do_verb
    INTEGER :: i
    
    do_verb = .TRUE.
    IF (PRESENT(VERB)) do_verb = VERB
    
    IF (do_verb) THEN
       PRINT * ,''
       PRINT *, '**********************Setting up PDE*****************'
       PRINT * ,'CASE Elliptic '
       PRINT *, '*****************************************************'
    END IF

    n_integrated = 1 ! dim
    n_time_levels = 1  !--# time levels

    n_var_time_levels = n_time_levels * n_integrated !--Number of equations (2D velocity at two time levels)

    n_var_additional = 1 !only doing elliptic solver !
    n_var = n_var_time_levels + n_var_additional !--Total number of variables

    n_var_exact = 1 ! 0 <--> No exact solution


    n_var_pressure  = n_var_time_levels ! ! no pressure

    !
    !--Allocate logical arrays 
    !  Must be done after setting number of different types of variables
    !  and before setting logical variable mapping arrays
    !
    CALL alloc_variable_mappings


    !
    ! Fill in variable names for default variables for integration meth 2
    ! These may be changed in the call to the user specified routine
    !  user_setup_pde()
    !
    ! In integrated variables (3D)
    IF( dim == 3 ) THEN
       WRITE (u_variable_names(1), u_variable_names_fmt)      'U       '
    ELSE IF(dim == 2) THEN
       WRITE (u_variable_names(1), u_variable_names_fmt)      'U       '
    ELSE IF(dim == 1) THEN
       WRITE (u_variable_names(1), u_variable_names_fmt)      'U       '
    END IF
    IF(n_var_additional == 1) THEN
       WRITE (u_variable_names(n_var), u_variable_names_fmt)  'F       '
    END IF


    !
    ! setup logical variable mapping arrays
    ! This defined which variables are used for adaptation, Which need to be interpololated to the new grid at 
    ! each time step, which are saved, and for which we have an exect solutiuon we want to check at each time step
    !


    !
    ! setup which components we will base grid adaptation on.
    ! Adapt on the 3 velocity components
    !
    n_var_adapt = .FALSE.
    IF(adapt_on_mask) THEN
       n_var_adapt(1:n_var,0) = .TRUE. !--Initially adapt on integrated variables at first time level
       n_var_adapt(1:n_var,1) = .TRUE. !--After first time step adapt on
    ELSE
       n_var_adapt(1:n_integrated,0) = .TRUE. !--Initially adapt on integrated variables at first time level
       n_var_adapt(1:n_integrated,1) = .TRUE. !--After first time step adapt on
    END IF

    !Variables that need to be interpoleted to new adapted grid in initial grid adaptation
    n_var_interpolate(1:n_integrated,0) = .TRUE. 

    !Variables that need to be interpoleted to new adapted grid at each time step
    n_var_interpolate(1:n_var,1) = .TRUE. 

    !
    ! setup which components we have an exact solution for
    !
    n_var_exact_soln(1,0:1) = .FALSE.

    !
    ! setup which variables we will save the solution
    !
    n_var_save(1) = .TRUE. ! save all for restarting code
    n_var_save(1:n_var) = .TRUE.

    ! Setup which variables are required on restart
    !
    n_var_req_restart = .FALSE.
    n_var_req_restart(1:n_integrated) = .TRUE.
    !
    !--Time level counters for NS integration
    !   Used for integration meth2
    ! n0 is current time step
    ! n1 is t_n-1 time step
    ! n2 is t_n-2 time step
    !
    n0 = 1; n1 = n0+dim; n2 = n1+dim


    !
    ! Set the maximum number of components for which we have an exact solution
    !
    n_var_exact = MAX( COUNT(n_var_exact_soln(:,0)),COUNT(n_var_exact_soln(:,1)) )

    !
    ! Setup a scaleCoeff array of we want to tweak the scaling of a variable
    ! ( if scaleCoeff < 1.0 then more points will be added to grid 
    !
    ALLOCATE ( scaleCoeff(1:n_var), Umn(1:n_var) )
    scaleCoeff = 1.0_pr

    Umn = 0.0_pr !set up here if mean quantities anre not zero and used in scales or equation
    Umn(1) = 1.0_pr !--Uniform mean velocity in x-direction

    IF (do_verb) THEN
       PRINT *, 'n_integrated = ',n_integrated 
       PRINT *, 'n_time_levels = ',n_time_levels
       PRINT *, 'n_var_time_levels = ',n_var_time_levels 
       PRINT *, 'n_var = ',n_var 
       PRINT *, 'n_var_exact = ',n_var_exact 
       PRINT *, '*******************Variable Names*******************'
       DO i = 1,n_var
          WRITE (*, u_variable_names_fmt) u_variable_names(i)
       END DO
       PRINT *, '****************************************************'
    END IF

  END SUBROUTINE  user_setup_pde

  !
  ! read variables from input file for this user case 
  !
  SUBROUTINE  user_read_input()
    IMPLICIT NONE


!!$  ! examples of reading
!!$  !  call input_real ('Mcoeff', Mcoeff, 'stop', &
!!$  !     ' Mcoeff ! Mij = Mcoeff * |S>2eps| Sij>2eps - (|S|Sij )>2eps')
!!$
!!$
!!$  ! call input_integer ('mdl_filt_type_grid', mdl_filt_type_grid, 'stop', &
!!$       ' mdl_filt_type_grid ! dyn mdl grid filter (defined in make_mdl_filts() )')
!!$
!!$   call input_logical ('clip_Cs', clip_Cs, 'stop', &
!!$       ' clip_Cs ! clip Cs if true')
    
    test_interpolate = .FALSE.
    call input_logical ('debug_test_interpolation',test_interpolate,'default', &
         ' test database independent interpolation inside usercase')

    call input_real ('alpha', alpha, 'stop', &
       ' alpha : convection velocity') 

    call input_real ('beta', beta, 'stop', &
       ' beta : forcing parameter') 

    Kprd = 10
    call input_integer_vector ('Kprd',Kprd,dim,'default', &
         '  Kprd: parameter of how many times the funciton is repeated outside of the domain')


    r0 = 1.e-1_pr
    call input_real ('r0', r0, 'default', &
       ' r0 : radius of the obstacle')

    r1 = 0.e-2_pr
    call input_real ('r1', r1, 'default', &
       ' r1 : radius of obstacle rotation')

    omega = 0.0_pr
    call input_real ('omega', omega, 'default', &
       ' r0 : cut of for r, such thar r<r0 is assumed to be zero')
    IF(omega /= 0.0_pr) omega = 1.0_pr 

    nu = 1.e-0_pr
    call input_real ('nu', nu, 'default', &
       ' nu : parametr that smoothes the transition')

    numerical_solution = .TRUE.
    call input_logical ('numerical_solution', numerical_solution, 'default', &
       ' numerical_solution : if .TRUE. solve elliptic problem, esle uses analytical solution')

    call input_logical ('space_time', space_time, 'stop', &
       ' space_time : if .TRUE. solve space-time problem, esle solves evolution problem')

    adapt_on_mask = .FALSE.
    call input_logical ('adapt_on_mask', adapt_on_mask, 'default', &
       ' adapt_on_mask : if .TRUE. adapts on penalty function, .FALSE. does not')

    eta_min = 1.0e-2_pr
    call input_real ('eta_min', eta_min, 'default', &
       ' eta_min: min range of penalization parameter')

    eta_max = 1.0e-2_pr
    call input_real ('eta_max', eta_max, 'default', &
       ' eta_max: max range of penalization parameter')
    eta = eta_max

    delta = 1.0_pr
    call input_real ('delta', delta, 'default', &
       ' delta: parameter describing the thickness of the penalty function')

  END SUBROUTINE  user_read_input


  !
  ! Set the exact solution for comparison to the simulated solution
  !
  ! u          - array to fill in the exact solution
  ! nlocal       - number of active wavelets   
  ! ne_local        - total number of equations
  ! t          - time of current time step 
  ! l_n_var_exact_soln_index - index into the elements of u for which we need to 
  !                            find the exact solution
  SUBROUTINE  user_exact_soln (u, nlocal,  t_local, l_n_var_exact_soln)
    USE penalization
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: nlocal
    REAL (pr), INTENT (IN) ::  t_local
    REAL (pr), DIMENSION (nlocal,n_var_exact), INTENT (INOUT) :: u
    REAL (pr), DIMENSION (nlocal,1:dim) :: Xdel
    LOGICAL , INTENT (IN) :: l_n_var_exact_soln(n_var)
    INTEGER :: i,idim

    REAL(pr) :: r(nlocal), Q(nlocal)

    DO idim =1, dim-1
       Xdel(:,idim) = SIN(pi*(x(:,idim)-alpha*x(:,dim)-0.25_pr*(xyzlimits(2,idim)+xyzlimits(1,idim)))/(xyzlimits(2,idim)-xyzlimits(1,idim)))/delta
    END DO
    Xdel(:,dim) = SIN(pi*(x(:,dim)-0.25_pr*(xyzlimits(2,dim)+xyzlimits(1,dim)))/(xyzlimits(2,dim)-xyzlimits(1,dim)))/delta
    u(:,1)  =  (1.0_pr-beta)*nu/delta**2*(1.0_pr-SUM(Xdel(:,1:dim-1)**2,DIM=2))*EXP(-0.5_pr*SUM(Xdel(:,1:dim)**2,DIM=2)) &
            +  beta*EXP(-0.5_pr*SUM(Xdel(:,1:dim)**2,DIM=2))
    u(:,1) = u(:,1) - u(1,1) ! clipping

     !    !WRITE (*, '("in user_exact_soln min/max U:", E12.5,X,E12.5)')  MINVAL(u), MAXVAL(u)

  END SUBROUTINE  user_exact_soln

  FUNCTION w (r, nlocal, order)
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: nlocal, order
    REAL (pr), DIMENSION (nlocal), INTENT (IN) :: r
    REAL (pr), DIMENSION (nlocal) :: w, rh

    IF(order == 1) THEN
       WHERE (r > 0.0_pr) 
          w = 2.0_pr*nu*exp(-(nu+3.0_pr*LOG(r)*r**2)/r**2)
       ELSEWHERE
          w = 0.0_pr
       END WHERE
    ELSE IF(order == 2) THEN 
       WHERE (r > 0.0_pr) 
          w = (-6.0_pr*nu*r**2+4.0_pr*nu**2)*exp(-(nu+6.0_pr*LOG(r)*r**2)/r**2)
       ELSEWHERE
          w = 0.0_pr
       END WHERE
    ELSE ! order==0
       WHERE (r > 0.0_pr) 
          w = exp(-nu/r**2)
       ELSEWHERE
          w = 0.0_pr
       END WHERE
    END IF
    
  END FUNCTION w



  FUNCTION z (r, nlocal, order)
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: nlocal, order
    REAL (pr), DIMENSION (nlocal), INTENT (IN) :: r
    REAL (pr), DIMENSION (nlocal) :: z

    IF(order == 1) THEN
       WHERE (r < 0.5_pr) 
          z = 0.0_pr
       ELSEWHERE (r > 0.75_pr) 
          z = 0.0_pr
       ELSEWHERE
          z = -(w(0.75_pr-r,nlocal,1)*w(r-0.5_pr, nlocal,0)+w(0.75_pr-r,nlocal,0)*w(r-0.5_pr,nlocal,1)) &
            / (w(r-0.5_pr ,nlocal,0)+w(0.75_pr-r,nlocal,0))**2
       END WHERE
    ELSE IF(order == 2) THEN 
       WHERE (r < 0.5_pr) 
          z = 0.0_pr
       ELSEWHERE (r > 0.75_pr) 
          z = 0.0_pr
       ELSEWHERE 
          z = (w(0.75_pr-r,nlocal,2)*w(r-0.5_pr, nlocal,0)-w(0.75_pr-r,nlocal,0)*w(r-0.5_pr,nlocal,2)) &
            / (w(r-0.5_pr ,nlocal,0)+w(0.75_pr-r,nlocal,0))**2 + 2.0_pr &
            * (w(0.75_pr-r,nlocal,1)*w(r-0.5_pr, nlocal,0)+w(0.75_pr-r,nlocal,0)*w(r-0.5_pr,nlocal,1)) &
            * (w(r-0.5_pr ,nlocal,1)-w(0.75_pr-r,nlocal,1)) &
            / (w(r-0.5_pr ,nlocal,0)+w(0.75_pr-r,nlocal,0))**3
       END WHERE
    ELSE ! order==0
       WHERE (r < 0.5_pr) 
          z = 1.0_pr
       ELSEWHERE (r > 0.75_pr) 
          z = 0.0_pr
       ELSEWHERE
          z = w(0.75_pr-r,nlocal,0)/(w(r-0.5_pr,nlocal,0)+w(0.75_pr-r,nlocal,0))
       END WHERE
    END IF
    
  END FUNCTION 


  SUBROUTINE user_initial_conditions (u, nlocal, ne_local, t_local, scl, scl_fltwt, iter)
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: nlocal, ne_local
    INTEGER  , INTENT (INOUT) :: iter ! iteration of call while adapting initial grid
    REAL (pr), DIMENSION (nlocal,ne_local), INTENT (INOUT) :: u
    REAL (pr), DIMENSION (nlocal,ne_local) :: f
    REAL (pr), DIMENSION (nlocal,1:dim) :: Xdel
    REAL (pr), INTENT (IN)   :: scl(1:n_var),scl_fltwt
    REAL (pr), INTENT (IN) :: t_local
    REAL(pr) :: xp(nlocal,1:dim)
    REAL (pr), DIMENSION(ne_local) :: scl_u

    INTEGER :: i, nb, idim !TEST
    INTEGER, DIMENSION(ne_local) :: clip

    REAL(pr) :: r(nlocal), Q(nlocal)

    clip = 0 !no clipping for Dirichlet BC
    scl_u = 1.0_pr
    f(:,1) =  RESHAPE( Laplace_rhs (u(:,1), nlocal, 1, HIGH_ORDER), (/nlocal/) ) 
    IF(numerical_solution) THEN
       CALL user_pre_process
       CALL Linsolve (u(:,1), f(:,1) , tol2, nlocal, 1, clip, Laplace, Laplace_diag, SCL=scl_u)  !
       CALL user_post_process
    ELSE
       u(:,1)  =  0.0_pr
    END IF

  END SUBROUTINE user_initial_conditions

  !--********************************  
  ! Arguments
  ! u         - field on adaptive grid
  ! nlocal      - number of active points
  ! ne_local       - number of equations
  ! t         - current time
  !
  ! Global variables used
  ! ifn       - number of equations
  ! x()
  ! xO
  ! y0
  !--******************************** 
  SUBROUTINE user_algebraic_BC (Lu, u, nlocal, ne_local, jlev, meth)
    !--Defines boundary condition type
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: jlev, meth, ne_local, nlocal
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (INOUT) :: Lu
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (IN)    :: u

    INTEGER :: ie, ii, shift
    REAL (pr), DIMENSION (nlocal,ne_local) :: du, d2u


    !
    ! There are periodic BC conditions
    !


  END SUBROUTINE user_algebraic_BC

  SUBROUTINE user_algebraic_BC_diag (Lu_diag, nlocal, ne_local, jlev, meth)
    !--Defines boundary condition type
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: jlev, meth, ne_local, nlocal
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (INOUT) :: Lu_diag
    REAL (pr), DIMENSION (nlocal,dim) :: du, d2u

    !
    ! There are periodic BC conditions
    !

  END SUBROUTINE user_algebraic_BC_diag



  SUBROUTINE user_algebraic_BC_rhs (rhs, ne_local, nlocal, jlev)
    !--Sets rhs for boundary conditions
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: ne_local, nlocal, jlev
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (INOUT) :: rhs

    INTEGER :: ie, ii, shift
    !
    ! There are periodic BC conditions
    !

  END SUBROUTINE user_algebraic_BC_rhs

  SUBROUTINE user_project (u, p, nlocal, meth)
    !--Makes u divergence free
    IMPLICIT NONE

    INTEGER, PARAMETER :: ne_local = 1
    INTEGER, INTENT (IN) :: meth, nlocal
    REAL (pr), DIMENSION (nlocal),     INTENT(INOUT) :: p
    REAL (pr), DIMENSION (nlocal,n_integrated), INTENT(INOUT) :: u

    !------------ not used in this case

  END SUBROUTINE user_project

  !==================================================================

  FUNCTION Laplace (jlev, u, nlocal, ne_local, meth)
    USE penalization
    USE util_vars
    USE parallel
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: jlev, nlocal, ne_local, meth
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (INOUT) :: u
    REAL (pr), DIMENSION (nlocal*ne_local) :: Laplace

    INTEGER :: i, ii, ie, shift, idim
    INTEGER :: meth_central, meth_backward, meth_forward 
    REAL (pr), DIMENSION (ne_local,nlocal,dim) :: dub, du, d2u
    REAL (pr), DIMENSION(ne_local) :: Uav
    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc
    REAL (pr) :: tmp1, tmp2

    meth_central  = meth + BIASING_NONE
    meth_backward = meth + BIASING_BACKWARD
    meth_forward  = meth + BIASING_FORWARD

!!$    IF( zero_mean .AND. jlev == j_lev) THEN
!!$       DO ie = 1, ne_local
!!$          shift=(ie-1)*nlocal
!!$          tmp1 = SUM(u(shift+1:shift+Nwlt_lev(jlev,0))*(1.0_pr-penal)*dA_level(1:Nwlt_lev(jlev,0),jlev))
!!$          tmp2 = SUM(*(1.0_pr-penal)*dA_level(1:Nwlt_lev(jlev,0),jlev))
!!$          CALL parallel_global_sum( REAL=tmp1 )
!!$          CALL parallel_global_sum( REAL=tmp2 )
!!$          Uav(ie) = tmp1/tmp2
!!$       END DO
!!$    ELSE
!!$       Uav =0.0_pr
!!$    END IF
!!$    DO ie = 1, ne_local
!!$       shift=(ie-1)*nlocal
!!$       u(shift+1:shift+Nwlt_lev(jlev,0)) = u(shift+1:shift+Nwlt_lev(jlev,0))-Uav(ie)
!!$    END DO

    CALL c_diff_fast (u, dub, d2u, jlev, nlocal, meth_backward, 10, ne_local, 1, ne_local)
    CALL c_diff_fast (u, du,  d2u, jlev, nlocal, meth_central,  11, ne_local, 1, ne_local)

    DO ie = 1, ne_local
       shift=(ie-1)*nlocal
       idim = dim
       Laplace(shift+1:shift+Nwlt_lev(jlev,0)) = dub( ie ,1:Nwlt_lev(jlev,0),idim) 
       DO idim = 1,dim-1
          Laplace(shift+1:shift+Nwlt_lev(jlev,0)) = Laplace(shift+1:shift+Nwlt_lev(jlev,0)) &
                                                  + alpha*du( ie ,1:Nwlt_lev(jlev,0),idim) - nu*d2u(ie ,1:Nwlt_lev(jlev,0),idim)
       END DO

       Laplace(shift+1:shift+nlocal) = Laplace(shift+1:shift+nlocal) + u(shift+1:shift+nlocal) 
       IF(imask_obstacle) THEN
          Laplace(shift+1:shift+nlocal) = Laplace(shift+1:shift+nlocal) &
                                        +  penal/eta*u(shift+1:shift+nlocal) - penal*u(shift+1:shift+nlocal)
       END IF


!!$       !--Boundary points
!!$       i_p_face(0) = 1
!!$       DO i=1,dim
!!$          i_p_face(i) = i_p_face(i-1)*3
!!$       END DO
!!$       DO face_type = 0, 3**dim - 1
!!$          face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
!!$          IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
!!$             CALL get_all_indices_by_face (face_type, jlev, nloc, iloc)
!!$             IF(nloc > 0 ) THEN ! 2-D & 3-D cases
!!$                   Laplace(shift+iloc(1:nloc)) = u(shift+iloc(1:nloc))    !Dirichlet conditions
!!$             END IF
!!$          END IF
!!$       END DO


    END DO

  END FUNCTION Laplace

  FUNCTION Laplace_diag (jlev,  nlocal, ne_local, meth)
    USE penalization
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: jlev, nlocal, ne_local, meth
    REAL (pr), DIMENSION (1:nlocal*ne_local) :: Laplace_diag

    INTEGER :: i, ii, ie, shift, idim
    INTEGER :: meth_central, meth_backward, meth_forward 
    REAL (pr), DIMENSION (nlocal,dim) :: dub, du, d2u

    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc

    meth_central  = meth + BIASING_NONE
    meth_backward = meth + BIASING_BACKWARD
    meth_forward  = meth + BIASING_FORWARD

    CALL c_diff_diag (dub, d2u, jlev, nlocal, meth_backward, meth_backward, 10)
    CALL c_diff_diag (du, d2u, jlev, nlocal, meth_central, meth_central, 11)

    DO ie = 1, ne_local
       shift=(ie-1)*nlocal

       idim = dim
       Laplace_diag(shift+1:shift+Nwlt_lev(jlev,0)) = dub(1:Nwlt_lev(jlev,0),idim) 
       DO idim = 1,dim-1
          Laplace_diag(shift+1:shift+Nwlt_lev(jlev,0)) = Laplace_diag(shift+1:shift+Nwlt_lev(jlev,0)) &
                                                  + alpha*du( 1:Nwlt_lev(jlev,0),idim) - nu*d2u(1:Nwlt_lev(jlev,0),idim) 
       END DO

       Laplace_diag(shift+1:shift+nlocal) = Laplace_diag(shift+1:shift+nlocal) + 1.0_pr
       IF(imask_obstacle) THEN
          Laplace_diag(shift+1:shift+nlocal) = Laplace_diag(shift+1:shift+nlocal) &
                                             + penal/eta - penal
       END IF


!!$       !--Boundary points
!!$       i_p_face(0) = 1
!!$       DO i=1,dim
!!$          i_p_face(i) = i_p_face(i-1)*3
!!$       END DO
!!$       DO face_type = 0, 3**dim - 1
!!$          face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
!!$          IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
!!$             CALL get_all_indices_by_face (face_type, jlev, nloc, iloc)
!!$             IF(nloc > 0 ) THEN ! 2-D & 3-D cases
!!$                   Laplace_diag(shift+iloc(1:nloc)) = 1.0_pr    !Dirichlet conditions
!!$             END IF
!!$          END IF
!!$       END DO


    END DO

  END FUNCTION Laplace_diag

  FUNCTION Laplace_rhs(u, nlocal, ne_local, meth_in)
    USE penalization
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: nlocal, ne_local, meth_in
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (INOUT) :: u
    REAL (pr), DIMENSION (nlocal,ne_local) :: Laplace_rhs

    INTEGER :: i, ii, ie, meth, idim
    INTEGER :: meth_central, meth_backward, meth_forward 
    REAL (pr), DIMENSION (nlocal,dim) :: Xdel
    REAL (pr), DIMENSION (ne_local ,nlocal,dim) :: du, d2u
    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc

    REAL(pr) :: r(nlocal), Q(nlocal)

    meth_central  = meth_in + BIASING_NONE
    meth_backward = meth_in + BIASING_BACKWARD
    meth_forward  = meth_in + BIASING_FORWARD

    ! set Laplace_rhs
!!$    DO idim =1, dim-1
!!$       Xdel(:,idim) = SIN(pi*(x(:,idim)-alpha*x(:,dim)-0.25_pr*(xyzlimits(2,idim)+xyzlimits(1,idim)))/(xyzlimits(2,idim)-xyzlimits(1,idim)))/delta
!!$    END DO
!!$    Xdel(:,dim) = SIN(pi*(x(:,dim)-0.25_pr*(xyzlimits(2,dim)+xyzlimits(1,dim)))/(xyzlimits(2,dim)-xyzlimits(1,dim)))/delta
!!$    Laplace_rhs(:,1)  =  (1.0_pr-beta)*nu/delta**2*(1.0_pr-SUM(Xdel(:,1:dim-1)**2,DIM=2))*EXP(-0.5_pr*SUM(Xdel(:,1:dim)**2,DIM=2)) &
!!$                      +  beta*EXP(-0.5_pr*SUM(Xdel(:,1:dim)**2,DIM=2))
!!$ 
     Laplace_rhs(:,1)  = 0.0_pr
     IF(imask_obstacle) THEN
         Laplace_rhs(:,1)  =  Laplace_rhs(:,1)  -  penal/eta*Umn(1)
     END IF

!!$    !--Boundary points
!!$    i_p_face(0) = 1
!!$    DO i=1,dim
!!$       i_p_face(i) = i_p_face(i-1)*3
!!$    END DO
!!$    DO face_type = 0, 3**dim - 1
!!$       face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
!!$       IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
!!$          CALL get_all_indices_by_face (face_type, j_lev, nloc, iloc)
!!$          IF(nloc > 0 ) THEN 
!!$             ! Xmin face (entire face) 
!!$             Laplace_rhs(iloc(1:nloc),1) = 0.0_pr  !Dirichlet conditions
!!$          END IF
!!$       END IF
!!$    END DO

  END FUNCTION Laplace_rhs


  FUNCTION user_rhs (u_integrated,p)
    USE penalization
    USE parallel
    IMPLICIT NONE
    REAL (pr), DIMENSION (ng,ne), INTENT(IN) :: u_integrated !1D flat version of integrated variables without BC 
    REAL (pr), DIMENSION (ng), INTENT(IN) :: p
    REAL (pr), DIMENSION (n) :: user_rhs

    INTEGER :: ie, shift, idim
    INTEGER, PARAMETER :: meth=1
    INTEGER :: meth_central, meth_backward, meth_forward 
    REAL (pr), DIMENSION (ne,ng,dim) :: du, d2u
    REAL (pr), DIMENSION(dim) :: Uav
    REAL (pr) :: tmp1, tmp2

    meth_central  = meth + BIASING_NONE
    meth_backward = meth + BIASING_BACKWARD
    meth_forward  = meth + BIASING_FORWARD

    IF( zero_mean ) THEN
       DO ie = 1, ne
          IF(imask_obstacle) THEN
             tmp1 = SUM(u_integrated(:,ie)*(1.0_pr-penal)*dA)
             tmp2 = SUM((1.0_pr-penal)*dA)
          ELSE
             tmp1 = SUM(u_integrated(:,ie)*dA)
             tmp2 = SUM(dA)
          END IF
          CALL parallel_global_sum( REAL=tmp1 )
          CALL parallel_global_sum( REAL=tmp2 )
          Uav(ie) = tmp1/tmp2
       END DO
    ELSE
       Uav =0.0_pr
    END IF

    CALL c_diff_fast(u_integrated, du, d2u, j_lev, ng, meth_central, 11, ne, 1, ne)

    DO ie = 1, ne
       shift=(ie-1)*ng
       user_rhs(shift+1:shift+ng) = 0.0_pr
       DO idim = 1,dim
          user_rhs(shift+1:shift+ng) = user_rhs(shift+1:shift+ng) - alpha*du(ie ,:,idim) + nu*d2u(ie ,:,idim)
       END DO

       user_rhs(shift+1:shift+ng) = user_rhs(shift+1:shift+ng) - (u_integrated(:,ie)-Uav(ie))
       IF(imask_obstacle) THEN
          user_rhs(shift+1:shift+ng) = user_rhs(shift+1:shift+ng) - penal/eta*(u_integrated(:,ie)-Uav(ie)+Umn(ie)) + penal*(u_integrated(:,ie)-Uav(ie))
       END IF
    END DO

    !--Set operator on boundaries
    IF( Nwlt_lev(j_lev,1) >  Nwlt_lev(j_lev,0) ) &
         CALL user_algebraic_BC_rhs (user_rhs, ne, ng, j_lev)
    
  END FUNCTION user_rhs

  ! find Jacobian of Right Hand Side of the problem
  FUNCTION user_Drhs (u, u_prev_timestep_loc, meth)
    USE penalization
    USE share_kry
    USE parallel
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: meth
    !u_prev_timestep passed local to cast it into 2dim array to work with vector derivatives
    REAL (pr), DIMENSION (ng,ne) :: u, u_prev_timestep_loc
    REAL (pr), DIMENSION (n) :: user_Drhs

    INTEGER :: ie, shift, idim
    INTEGER :: meth_central, meth_backward, meth_forward 
    REAL (pr), DIMENSION (ne,ng,dim) :: du, d2u
    REAL (pr), DIMENSION(dim) :: Uav_prev, Uav
    REAL (pr) :: tmp1, tmp2, tmp3

    meth_central  = meth + BIASING_NONE
    meth_backward = meth + BIASING_BACKWARD
    meth_forward  = meth + BIASING_FORWARD

    IF( zero_mean ) THEN
       DO ie = 1, ne
          IF(imask_obstacle) THEN
             tmp1 = SUM(u(:,ie)*(1.0_pr-penal)*dA)
             tmp3 = SUM(u_prev_timestep_loc(:,ie)*(1.0_pr-penal)*dA)
             tmp2 = SUM((1.0_pr-penal)*dA)
          ELSE
             tmp1 = SUM(u(:,ie)*dA)
             tmp3 = SUM(u_prev_timestep_loc(:,ie)*dA)
             tmp2 = SUM(dA)
          END IF
          CALL parallel_global_sum( REAL=tmp1 )
          CALL parallel_global_sum( REAL=tmp2 )
          CALL parallel_global_sum( REAL=tmp3 )
          Uav(ie) = tmp1/tmp2
          Uav_prev(ie) = tmp3/tmp2
       END DO
    ELSE
       Uav =0.0_pr
       Uav_prev = 0.0_pr
    END IF

    CALL c_diff_fast(u, du, d2u, j_lev, ng, meth_central, 11, ne, 1, ne)

    DO ie = 1, ne
       shift=(ie-1)*ng
       user_Drhs(shift+1:shift+ng) = 0.0_pr
       DO idim = 1,dim
          user_Drhs(shift+1:shift+ng) = user_Drhs(shift+1:shift+ng) - alpha*du(ie ,:,idim) + nu*d2u(ie ,:,idim)
       END DO

       user_Drhs(shift+1:shift+ng) = user_Drhs(shift+1:shift+ng) - (u(:,ie)-Uav(ie))
       IF(imask_obstacle) THEN
          user_Drhs(shift+1:shift+ng) = user_Drhs(shift+1:shift+ng) - Dpenal_factor * penal/eta*(u(:,ie)-Uav(ie)) + Dpenal_factor * penal*(u(:,ie)-Uav(ie))
       END IF
    END DO

  END FUNCTION user_Drhs


  !
  ! Uses u_prev_timestep, which is global flat (1D ) array set in time_step_cn()
  !
  FUNCTION user_Drhs_diag (meth)
    USE penalization
    USE util_vars
    USE parallel
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: meth
    REAL (pr), DIMENSION (n) :: user_Drhs_diag

    INTEGER :: ie, shift, idim
    INTEGER :: meth_central, meth_backward, meth_forward 
    REAL (pr), DIMENSION (ng,dim) :: du, d2u
!!$    REAL (pr), DIMENSION (ne,ng,dim) :: du_prev_timestep
!!$    REAL (pr), DIMENSION (ne  ,ng,dim) :: du_dummy ! passed when only calculating 1st derivative.
    REAL (pr), DIMENSION(dim) :: Uav_prev
    REAL (pr) :: tmp1, tmp2, tmp3

    meth_central  = meth + BIASING_NONE
    meth_backward = meth + BIASING_BACKWARD
    meth_forward  = meth + BIASING_FORWARD

    IF( zero_mean ) THEN
       DO ie = 1, ne
          shift=(ie-1)*ng
          IF(imask_obstacle) THEN
             tmp3 = SUM(u_prev_timestep(shift+1:shift+ng)*(1.0_pr-penal)*dA)
             tmp2 = SUM((1.0_pr-penal)*dA)
          ELSE
             tmp3 = SUM(u_prev_timestep(shift+1:shift+ng)*dA)
             tmp2 = SUM(dA)
          END IF
          CALL parallel_global_sum( REAL=tmp2 )
          CALL parallel_global_sum( REAL=tmp3 )
          Uav_prev(ie) = tmp3/tmp2
       END DO
    ELSE
       Uav_prev = 0.0_pr
    END IF

!!$    CALL c_diff_fast(u_prev_timestep, du_prev_timestep, du_dummy, j_lev, ng, meth_central, 10, ne, 1, ne)

    !
    ! does not rely on u so we can call it once here
    !
    shift = 0 !tmp
    CALL c_diff_diag(du, d2u, j_lev, ng, meth_central, meth_central, 11)

    DO ie = 1, ne
       shift=(ie-1)*ng
       user_Drhs_diag(shift+1:shift+ng) = 0.0_pr
       DO idim = 1,dim
          user_Drhs_diag(shift+1:shift+ng) = user_Drhs_diag(shift+1:shift+ng) - alpha*du(:,idim) + nu*d2u(:,idim)
       END DO

       user_Drhs_diag(shift+1:shift+ng) = user_Drhs_diag(shift+1:shift+ng) - 1.0_pr
       IF(imask_obstacle) THEN
          user_Drhs_diag(shift+1:shift+ng) = user_Drhs_diag(shift+1:shift+ng) - Dpenal_factor * penal/eta + Dpenal_factor * penal
       END IF
    END DO

  END FUNCTION user_Drhs_diag


  FUNCTION user_chi (nlocal, t_local )
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: nlocal
    REAL (pr), INTENT (IN) :: t_local
    REAL (pr), DIMENSION (nlocal) :: user_chi
    REAL (pr), DIMENSION (nlocal,dim) :: Xr, Xs
    REAL (pr) :: COSt,SINt
    INTEGER :: idim

    Xs = x
    Xr = x
    IF(space_time) THEN
       DO idim =1, dim-1
          Xs(:,idim) = (x(:,idim)-0.5_pr*(xyzlimits(2,idim)+xyzlimits(1,idim)))
       END DO
       Xs(:,dim) = omega*2.0_pr*pi*(x(:,dim)-xyzlimits(1,dim))/(xyzlimits(2,dim)-xyzlimits(1,dim))
       Xr(:,dim) = Xs(:,dim)
       IF(dim == 3) THEN
          Xs(:,1) = Xs(:,1) - r1*COS(Xs(:,dim))
          Xs(:,2) = Xs(:,2) - r1*SIN(Xs(:,dim))
          Xr(:,1) = Xs(:,1)*COS(Xs(:,dim))+Xs(:,2)*SIN(Xs(:,dim))
          Xr(:,2) =-Xs(:,1)*SIN(Xs(:,dim))+Xs(:,2)*COS(Xs(:,dim))
       END IF
       user_chi = 0.5_pr*(1.0_pr-tanh((SQRT(SUM(Xr(:,1:dim-1)**2,DIM=2))-r0)/delta))
    ELSE
       DO idim =1, dim
          Xs(:,idim) = (x(:,idim)-0.5_pr*(xyzlimits(2,idim)+xyzlimits(1,idim)))
       END DO
       SINt = SIN(omega*2.0_pr*pi*t_local)
       COSt = COS(omega*2.0_pr*pi*t_local)
       IF(dim == 2) THEN
          Xs(:,1) = Xs(:,1) - r1*COSt
          Xs(:,2) = Xs(:,2) - r1*SINt
          Xr(:,1) = Xs(:,1)*COSt+Xs(:,2)*SINt
          Xr(:,2) =-Xs(:,1)*SINt+Xs(:,2)*COSt
       ELSE
          PRINT *,'ERROR: the problem is set up only for DIM=2'
          STOP
       END IF
       user_chi = 0.5_pr*(1.0_pr-tanh((SQRT(SUM(Xr(:,1:dim)**2,DIM=2))-r0)/delta))
    END IF

  END FUNCTION user_chi
  !
  ! Calculate any statitics
  !
  ! startup_flag - 0 when adapting to IC,then 1 inmain integration loop
  !
  !
  SUBROUTINE user_stats ( u ,j_mn, startup_flag)
    USE pde
    USE penalization
    USE parallel
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: startup_flag
    INTEGER , INTENT (IN) :: j_mn 
    REAL (pr), DIMENSION (nwlt,1:n_var), INTENT (IN) :: u
    REAL (pr) :: err_local(2), scl_local(2)
    LOGICAL, SAVE :: write_header =.TRUE.
    ! debug: test database independent interpolation
    IF (test_interpolate) CALL user_interpolate (u)

    IF(n_var_exact > 0 .AND. par_rank == 0) THEN
       IF(startup_flag == 0 .AND. write_header) THEN
          OPEN  (UNIT=UNIT_USER_STATS, FILE = file_name_user_stats, FORM='formatted', STATUS='unknown', POSITION='rewind')
          WRITE(UNIT=UNIT_USER_STATS,ADVANCE='YES', &
               FMT='("Np",1x,"Nu",1x,"Nd",2x,"iwrite",1x,"Jlv",7x,"Nxyz",7x,"Nwlt",2x,"eps",13x,"eta",13x,"time",12x,"||U-Ue||inf",5x,"||Ue||inf",7x,"||U-Ue||2",7x,"||Ue||2")' )
          CLOSE (UNIT=UNIT_USER_STATS)
          write_header = .FALSE.
       ELSE IF(startup_flag == -1) THEN
          IF(imask_obstacle) THEN
             err_local(1) = MAXVAL (ABS ((1.0_pr-penal)*(u(:,1) - u_ex(:,1))))
             err_local(2) = SQRT ( SUM ( ((u(:,1)-u_ex(:,1))**2)*(1.0_pr-penal)*dA )/ sumdA_global  )
             scl_local(1) = MAXVAL (ABS ((1.0_pr-penal)*u_ex(:,1)))
             scl_local(2) = SQRT ( SUM ( (u_ex(:,1)**2)*(1.0_pr-penal)*dA )/ sumdA_global  )
          ELSE
             err_local(1) = MAXVAL (ABS (u(:,1) - u_ex(:,1)))
             err_local(2) = SQRT ( SUM ( ((u(:,1)-u_ex(:,1))**2)*dA )/ sumdA_global  )
             scl_local(1) = MAXVAL (ABS (u_ex(:,1)))
             scl_local(2) = SQRT ( SUM ( (u_ex(:,1)**2)*dA )/ sumdA_global  )
          END IF
          OPEN  (UNIT=UNIT_USER_STATS, FILE = file_name_user_stats, FORM='formatted', STATUS='old', POSITION='append')
          WRITE(UNIT_USER_STATS,'(3(I2,1x),1x,I6,1x,I3,1x,I10,1x,I10,1x,7(E15.8,1x))') &
               n_prdct(HIGH_ORDER), n_updt(HIGH_ORDER), n_diff, iwrite, j_lev, PRODUCT(Mxyz(1:dim)*2**(j_lev-1)+1-prd(1:dim)), nwlt, &
               eps, eta, t, err_local(1), scl_local(1), err_local(2), scl_local(2)
          CLOSE (UNIT=UNIT_USER_STATS)
      END IF
    ELSE IF(n_var_exact == 0 .AND. par_rank == 0) THEN
       IF(startup_flag == 0 .AND. write_header) THEN
          OPEN  (UNIT=UNIT_USER_STATS, FILE = file_name_user_stats, FORM='formatted', STATUS='unknown', POSITION='rewind')
          WRITE(UNIT=UNIT_USER_STATS,ADVANCE='YES', &
               FMT='("Np",1x,"Nu",1x,"Nd",2x,"iwrite",1x,"Jlv",7x,"Nxyz",7x,"Nwlt",2x,"eps",13x,"eta",13x,"time",12x,"||U||inf",8x,"||U||2")' )
          CLOSE (UNIT=UNIT_USER_STATS)
          write_header = .FALSE.
       ELSE
          IF(imask_obstacle) THEN
             scl_local(1) = MAXVAL (ABS ((1.0_pr-penal)*u(:,1)))
             scl_local(2) = SQRT ( SUM ( (u(:,1)**2)*(1.0_pr-penal)*dA )/ sumdA_global  )
          ELSE
             scl_local(1) = MAXVAL (ABS (u(:,1)))
             scl_local(2) = SQRT ( SUM ( (u(:,1)**2)*dA )/ sumdA_global  )
          END IF
          OPEN  (UNIT=UNIT_USER_STATS, FILE = file_name_user_stats, FORM='formatted', STATUS='old', POSITION='append')
          WRITE(UNIT_USER_STATS,'(3(I2,1x),1x,I6,1x,I3,1x,I10,1x,I10,1x,5(E15.8,1x))') &
               n_prdct(HIGH_ORDER), n_updt(HIGH_ORDER), n_diff, iwrite, j_lev, PRODUCT(Mxyz(1:dim)*2**(j_lev-1)+1-prd(1:dim)), nwlt, &
               eps, eta, t, scl_local(1), scl_local(2)
          CLOSE (UNIT=UNIT_USER_STATS)
      END IF

    END IF

  END SUBROUTINE user_stats

  !
  ! calculate any additional variables
  ! 
  ! arg 
  ! flag  - 0 calledwhile adapting to IC, 1 - called in main time integration loop
  !
  ! These additiona variables are calculated and left in real space.
  !
  SUBROUTINE user_cal_force (u, n, t_local, force, drag, lift)
    !--Calculates drag and lift on obstacle using penalization formula
    IMPLICIT NONE

    INTEGER, INTENT (IN) :: n
    REAL (pr), INTENT (IN) :: t_local
    REAL (pr), DIMENSION (dim), INTENT (INOUT) :: force
    REAL (pr), INTENT (OUT) :: drag, lift
    REAL (pr), DIMENSION (n,dim) :: u
    drag = 0.0_PR
    lift = 0.0_PR

    !
    ! There is no obstacle in flow
    !

  END SUBROUTINE user_cal_force

  !
  ! calculate any additional variables
  ! 
  ! arg 
  ! flag  - 0 calledwhile adapting to IC, 1 - called in main time integration loop
  !
  ! These additiona variables are calculated and left in real space.
  !
  SUBROUTINE user_additional_vars( t_local, flag )
    IMPLICIT NONE
    REAL (pr), INTENT (IN) ::  t_local
    INTEGER , INTENT(IN) :: flag ! 0- called during adaption to IC, 1 called during main integration loop
    REAL(pr) :: r(nwlt), Q(nwlt)


    IF(n_var_additional > 0) THEN
!!$       u(:,n_var) = RESHAPE( Laplace_rhs (u(:,1), nwlt, 1, HIGH_ORDER), (/nwlt/) ) 
       IF(space_time) THEN
          u(:,n_var) = user_chi (nwlt, 0.0_pr)
       ELSE
          u(:,n_var) = user_chi (nwlt, t_local)
       END IF
    END IF

  END SUBROUTINE user_additional_vars

  !
  ! calculate any additional scalar variables
  ! 
  ! arg 
  ! flag  - 0 calledwhile adapting to IC, 1 - called in main time integration loop
  !
  SUBROUTINE user_scalar_vars( flag )
    IMPLICIT NONE
    INTEGER , INTENT(IN) :: flag ! 0- called during adaption to IC, 1 called during main integration loop



  END SUBROUTINE user_scalar_vars

  !
  !************ Calculating Scales ***************************
  !
  ! Note the order of the components in the scl array
  ! correspond to u_tn, v_tn, w_tn, u_tn-1, v_tn-1, w_tn-1, u_tn-2, v_tn-2, w_tn-2
  ! 
  SUBROUTINE user_scales(flag, use_default, u_loc, nlocal, ne_local, l_n_var_adapt , l_n_var_adapt_index, &
       scl, scl_fltwt) !add
    USE precision
    USE pde
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: flag ! 0 during initial adaptation, then 1 during time advancement
    LOGICAL , INTENT(INOUT) :: use_default
    INTEGER, INTENT (IN) :: nlocal, ne_local
    REAL (pr), DIMENSION (1:nlocal,1:ne_local), INTENT (IN) :: u_loc
    LOGICAL , INTENT (IN) :: l_n_var_adapt(ne_local)
    INTEGER , INTENT (IN) :: l_n_var_adapt_index(1:ne_local)
    REAL (pr), DIMENSION (1:ne_local), INTENT (INOUT) :: scl
    REAL (pr) ,  INTENT(IN) ::scl_fltwt !weight for temporal filter on scl

    !
    ! Ignore the output of this routine and use default scales routine
    !
    use_default = .TRUE. 

    !
    ! NOTE: For a parallel run, synchronize scl(:) across the processors in user_scales.
    !       Use the subroutine scales of default_util.f90 as an example,
    !       subroutine parallel_global_sum of parallel.f90 may be helpful.
    !       Already synchronized: sumdA_global
    !
  END SUBROUTINE user_scales

  SUBROUTINE user_cal_cfl (use_default, u, cfl_out)
    USE precision
    USE sizes
    USE pde
    USE parallel
    IMPLICIT NONE
    LOGICAL , INTENT(INOUT) :: use_default
    REAL (pr),                                INTENT (INOUT) :: cfl_out
    REAL (pr), DIMENSION (nwlt,n_integrated), INTENT (IN)    :: u

    INTEGER                    :: i
    REAL (pr)                  :: floor
    REAL (pr), DIMENSION (dim) :: cfl
    REAL (pr), DIMENSION(dim,nwlt) :: h_arr

    use_default = .FALSE.

    floor = 1e-12_pr
    cfl_out = floor

    CALL get_all_local_h (h_arr)

    DO i = 1, nwlt
       cfl(1:dim) = ABS(alpha) * dt/h_arr(1:dim,i)
       cfl_out = MAX (cfl_out, MAXVAL(cfl))
    END DO
    CALL parallel_global_sum( REALMAXVAL=cfl_out )

  END SUBROUTINE user_cal_cfl

  !******************************************************************************************
  !************************************* SGS MODEL ROUTINES *********************************
  !******************************************************************************************

  !
  ! Intialize sgs model
  ! This routine is called once in the first
  ! iteration of the main time integration loop.
  ! weights and model filters have been setup for first loop when this routine is called.
  !
  SUBROUTINE user_init_sgs_model( )
    IMPLICIT NONE


    ! LDM: Giuliano

    ! THE sgs forcign is stored for the time step in sgs_mdl_force(1:nlocal,1:n_integrated)
    ! where nlocal should be nwlt.


    !          print *,'initializing LDM ...'       
    !          CALL sgs_mdl_force( u(:,1:n_integrated), nwlt, j_lev, .TRUE.)


  END SUBROUTINE user_init_sgs_model

  !
  ! calculate sgs model forcing term
  ! user_sgs_force is called int he beginning of each times step in time_adv_cn().
  ! THE sgs forcign is stored for the time step in sgs_mdl_force(1:nlocal,1:n_integrated)
  ! where nlocal should be nwlt.
  ! 
  ! Accesses u from field module, 
  !          j_lev from wlt_vars module,
  !
  SUBROUTINE  user_sgs_force (u_loc, nlocal)
    IMPLICIT NONE

    INTEGER,                         INTENT (IN) :: nlocal
    REAL (pr), DIMENSION (nlocal,n_integrated), INTENT (INOUT) :: u_loc

  END SUBROUTINE  user_sgs_force

  FUNCTION user_sound_speed (u, neq, nwlt)
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: nwlt, neq
    REAL (pr), DIMENSION (nwlt,neq), INTENT(IN) :: u
    REAL (pr), DIMENSION (nwlt) :: user_sound_speed

    !user_sound_speed(:) = SQRT( gamma*(gamma-1.0_pr)* &
    !       ( u(:,4)-0.5_pr*SUM(u(:,n_var_mom(1:dim))**2,DIM=2)/u(:,1)) ) ! pressure
    user_sound_speed = 0.0_pr

  END FUNCTION user_sound_speed



  !-----------------------------------------------------------------------------------
  SUBROUTINE user_interpolate ( u )
    ! This is an example of database independent interpolation subroutine usage;
    ! predefined number of points and stack allocation has been used for that example.
    ! Read also the info inside subroutine interpolate of module wavelet_filters_mod
    IMPLICIT NONE
    REAL (pr), DIMENSION (nwlt, n_var), INTENT (IN) :: u
    REAL (pr), DIMENSION (n_var, nwlt, dim) :: du, d2u
    INTEGER, PARAMETER :: x_size = 50000                  ! number of points to interpolate into
    REAL(pr) :: points(dim,x_size)
    INTEGER :: var(1:n_var)                           ! interpolate for all variables
    REAL (pr) :: res(1:n_var,1:x_size)                ! result of the interpolation
    INTEGER :: i, j, interpolation_order, var_size
    LOGICAL, SAVE :: BEEN_HERE = .FALSE.              ! file initialization flag
    INTEGER, PARAMETER :: iunit = 91                  ! ...
    CHARACTER*(*), PARAMETER :: filename = 'int.agr'  ! ...
    INTEGER, SAVE :: set_counter = 0
    REAL(pr), PARAMETER :: MARGIN = 0.3_pr            ! margin
    REAL (4)  :: t0(0:2), t1(0:2)

    ! set output file for debugging
    IF (BEEN_HERE) THEN
       ! open file for appending
       OPEN (UNIT=iunit, FILE=filename, FORM='formatted', STATUS='old', POSITION='append', ERR=1)
    ELSE
       ! create new file
       ! and write XMGR header
       OPEN (UNIT=iunit, FILE=filename, FORM='formatted', STATUS='unknown', ERR=1)
       WRITE (iunit,'("@g0 hidden false")')
       BEEN_HERE = .TRUE.
    END IF

    
    ! compute first derivatives to be passed to interpolate()
    ! for 2.5 order interpolation
    CALL c_diff_fast (u, du, d2u, j_lev, nwlt, HIGH_ORDER, 10, n_var, 1, n_var)
    DO i=1,n_var
       WRITE (*,'("   MAXVAL(ABS(u(1:nwlt,",I2,"))) = ",E17.10)') i, MAXVAL(ABS(u(1:nwlt,i)))
       WRITE (*,'("   MAXVAL(ABS(du(1:nwlt,",I2,"))) = ",E17.10)') i, MAXVAL(ABS(du(i,1:nwlt,1:dim)))
    END DO
    

    ! interpolate for all n_var variables,
    ! set var_size and var(:) respectively
    var_size = n_var
    DO i=1,n_var
       var(i) = i
    END DO


    ! predefine points to interpolate into
    ! diagonal has been used for that example
    DO i=1,x_size
       DO j=1,dim ! xx(0:nxyz(:),1:dim)
          points(j,i) = MARGIN + xx(0,j) + (xx(nxyz(j),j) - xx(0,j) - 2*MARGIN)*(i-1)/(1.0*(x_size-1))
       END DO
    END DO


    ! perform interpolation for different orders
    ! and write XMGR file ordered by the first coordinate
    DO interpolation_order = 0,2
       CALL CPU_TIME( t0(interpolation_order) )
       CALL interpolate( u, du, nwlt, n_var, &
            x_size, points, interpolation_order, var_size, var, res )
       CALL CPU_TIME( t1(interpolation_order) )
       DO j=1, n_var
          DO i=1,x_size
             WRITE (iunit,'(2(E12.5,1X))') points(1,i), res(j,i)
          END DO
          WRITE (iunit,'("&")')
       END DO
    END DO
    set_counter = set_counter + 1
    
    
    ! close XMGR file
    CLOSE (iunit)
    WRITE (*, '("   DEBUG: 0,1,3 order interpolation sets written ",I5," times")') set_counter
    DO interpolation_order = 0,2
       WRITE (*, '("CALL interpolate (USING CPU_TIME) = ", es12.5)') t1(interpolation_order) - t0(interpolation_order)
    END DO
    
    ! error handling
    RETURN
1   PRINT *, 'ERROR while opening file:', filename
    STOP 'in user_interpolate'
    
  END SUBROUTINE user_interpolate
  
  SUBROUTINE  user_pre_process
    USE penalization
    USE parallel
    IMPLICIT NONE
    REAL (pr) :: tmp1, tmp2
    INTEGER :: ie

    IF( zero_mean ) THEN
       DO ie = 1, ne
          IF(imask_obstacle) THEN
             tmp1 = SUM(u(:,ie)*(1.0_pr-penal)*dA)
             tmp2 = SUM((1.0_pr-penal)*dA)
          ELSE
             tmp1 = SUM(u(:,ie)*dA)
             tmp2 = SUM(dA)
          END IF
          CALL parallel_global_sum( REAL=tmp1 )
          CALL parallel_global_sum( REAL=tmp2 )
          u(:,ie) = u(:,ie) - tmp1/tmp2
       END DO
    END IF

     
  END SUBROUTINE user_pre_process

  SUBROUTINE  user_post_process
    USE penalization
    USE parallel
    IMPLICIT NONE
    REAL (pr) :: tmp1, tmp2
    INTEGER :: ie

    IF( zero_mean ) THEN
       DO ie = 1, ne
          IF(imask_obstacle) THEN
             tmp1 = SUM(u(:,ie)*(1.0_pr-penal)*dA)
             tmp2 = SUM((1.0_pr-penal)*dA)
          ELSE
             tmp1 = SUM(u(:,ie)*dA)
             tmp2 = SUM(dA)
          END IF
          CALL parallel_global_sum( REAL=tmp1 )
          CALL parallel_global_sum( REAL=tmp2 )
          u(:,ie) = u(:,ie) - tmp1/tmp2
       END DO
    END IF
    
  END SUBROUTINE user_post_process

  
END MODULE user_case
