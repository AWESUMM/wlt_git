function user_stats(s,a)
  if isempty('a') 
      a=1.
  end
  X='Nwlt';
  %X='Nxyz';
  %X='eps';
  %Y='||U-Ue||2';
  %Y='||U-Ue||inf';
  Y='err2';
  %digits(16);

  figure('Name','Error Convergence');
 

  %status = 'new';
  lclr=['g- '; 'k-.';'r--';'b: ';'m-.';'k: ';'r-.';'b--';'m: ';'k--';'r: ';'b-.'];
  Nlnstyles=size(lclr,1);
  lineThickness = 2;
  
  % Load fitness history file and plot
  [FileNameTarget PathNameTarget] = uigetfile('./results/*_user_stats','Insert user_stats file');
  FileNameTarget =  [PathNameTarget FileNameTarget];
  fidTarget = fopen(FileNameTarget,'r');
  A = importdata(FileNameTarget)
  Ndata=length(A.colheaders);
  lname=A.colheaders;
  A.data;
  dimension = size(A.data);
  ix = 1;
  while ix <= Ndata & isempty(findstr(X,lname{ix})) 
      ix=ix+1;
  end
  iy=1;
  while iy <= Ndata & isempty(findstr(Y,lname{iy})) 
      iy=iy+1;
  end
  ieps = 1;
  while ieps <= Ndata & isempty(findstr('eps',lname{ieps})) 
      ieps=ieps+1;
  end
  ieta = 1;
  while ieta <= Ndata & isempty(findstr('eta',lname{ieta})) 
      ieta=ieta+1;
  end
  if ix > Ndata || iy > Ndata
      disp('ERROR: the symbol is not found')
      ierr = 1;
  else
      ierr=0;
      x = A.data(:,ix);
      y = A.data(:,iy);
      eps = A.data(:,ieps);
      eta = A.data(:,ieta);
      eps_unique = sort(unique(eps),'descend');
      eta_unique = sort(unique(eta),'descend');
      l=[1:length(eta)];
      xlabel('N_w');
      ylabel('||u-u_e||');
      clear lg;
      lg = cell(size(eta_unique))
      for i = 1:length(eta_unique)
         lg{i}=['\eta=' num2str(eta_unique(i),'%7.2e\n')];
         L=l(eta==eta_unique(i));
         loglog(x(L),y(L),lclr(i,:),'LineWidth',lineThickness);
         legend(lg(1:i),'Location','EastOutside');
         hold on
      end
      grid on;
      set(gca,'XminorGrid','off','YminorGrid','off')
  end
  plot([min(x),max(x)],a*max(y)*[1.,max(x)/min(x)].^s,'k-')
  if X=='epsa'
      hold off
      figure('Name','Eta Convergence');
      for i = 1:length(eta_unique)
         y_eta(i) = min(y(l(eta==eta_unique(i))));
      end
      loglog(eta_unique,y_eta,'ro');
      grid on
      set(gca,'XminorGrid','off','YminorGrid','off')
      xlabel('\eta');
      ylabel('error');
      hold on
      loglog(eta_unique,a*max(y_eta)*(eta_unique/max(eta_unique)).^s,'k-');
  end
end













