#------------------------------------------------------------------#
# General input file format                                        #
#                                                                  #
# comments start with # till the end of the line                   #
# string constant are quoted by "..." or '...'                     #
# boolean can be (T F 1 0 on off) in or without ' or " quotes      #
# integers are integers                                            #
# real numbers are whatever (e.g. 1   1.0   1e-23   1.123d-54 )    #
# vector elements are separated by commas                          #
# spaces between tokens are not important                          #
# empty or comment lines are not important                         #
# order of lines is not important                                  #
#------------------------------------------------------------------#

file_gen   = 'moving_geometry_dum.'
#***************************************************************************************
IC_filename 		= 'results/start.res'
IC_restart 		= F    #  ICrestart
IC_restart_station 	= 0    #  it_start, restart file number to use (NOT iteration!)
IC_from_file 		= F    # Do a new run with IC from a restart file
IC_file_fmt 		= 0    # IC data file format (0 - native restart file, 1-netcdf, 2- A.Wray in fourier space, 3-simple binary)
Data_file_format 	= F    #  T = formatted, F = unformatted
IC_adapt_grid 		= F    # parameter defaulted to .TRUE. If is set to .FALSE. no grid adaptation is done after the data are read.
#***************************************************************************************

dimension  	= 2		#  dimensions

time_integration_method = 2 	# 0- meth2, 1 -krylov, 2 - Crank Nicolson
t_begin	   = 0.00e+00    	# tbeg
t_end      = 1.00e+01     	# tend
t_adapt    = 1.00e+04       	# when t > t_adapt use an adaptive time step if possible
dt         = 5.00e-04	        # dt
dtwrite    = 1.00e-02 		# dtwrite

dtmax = 1.0000000e-02   #  dtmax
dtmin = 1.0000000e-08 	#  dtmin-if dt < dtmin then exection stops(likely blowing up)

# cfl = 1.0000000e-00 	#  cfl
cflmax = 0.8000000e-00 	#  cflmax
cflmin = 0.0000000e-00 	#  cflmin

coord_min  = -4.0, -4.0, -2.0	#  XMIN, YMIN, ZMIN, etc
coord_max  =  4.0,  4.0,  2.0 	#  XMAX, YMAX, ZMAX, etc

# ZONE parameters: outside of the zone the mesh is uniform at level J_mn  (SHOULD BE WRITTEN j_zn?)
coord_zone_min =  -7.0e+10, -3.5e+10, -1.0e+01  # XMINzone, etc
coord_zone_max =   7.0e+10,  3.5e+10,  1.0e+01  # XMAXzone, etc


M_vector   = 4, 4, 2	        #  Mx, My, Mz=8 
j_mn_init  = 4             	#  J_mn_init force J_mn == J_INIT while adapting to IC
j_lev_init = 4                  #  force j_lev == j_lev_init when initializing
J_MN 	   = 3         		#  J_MN
J_MX 	   = 8        		#  8 J_MX
j_IC       = 20                 #  J_IC if the IC data does not have dimensions in it then mxyz(:)*2^(j_IC-1) is used
J_FILT     = 20        	        #  J_FILT
j_zn       = 20                  #  
periodic 	 = 1,1,1	#  prd(:) (0/1) 0: non-periodic; 1: periodic
uniform 	 = 0,0,0	#  grid(:) (0/1) 0: uniform; 1: non-uniform

tol1 			 = 1.00e-05 	#  tol1 used to set tolerence for non-solenoidal half-step
tol2 			 = 1.00e-04	#  tol2 used to set tolerence for solenoidal half-step
tol3 			 = 1.00e-02     #  used to set tolerence for time step

eps_init 	 = 1.00e-03	#  5.00e-03 (1.5 they match first adapt) EPS used to adapt initial grid  
eps_run 	 = 1.00e-03     #  EPS used in run  
eps_adapt_steps =  2    	# eps_adapt_steps ! how many time steps to adapt from eps_init to eps_run
eps_expl = 5.00e-3         	# eps_expl - threshold sed for explicit filtering

Scale_Meth = 3             	# (3) Scale_Meth !1- Linf, 2-L2(vector), 3-L2(physical)
scl_fltwt = 0.0           	#  scl temporal filter weight, scl_new = scl_fltwt*scl_old + ( 1-scl_fltwt)*scl_new
scaleCoeff = 1 1 1 1 1 1 1 1

i_h = 123456        	#  order of boundaries (1-xmin,2-xmax,3-ymin,4-ymax,5-zmin,6-zmax)
i_l = 111111        	#  algebraic/evolution (1/0) BC order: (lrbt)
N_predict = 2           #  N_predict
N_predict_low_order = 1 # 1 N_predict_low_order
N_update =0           	#  N_update
N_update_low_order = 0  # 0 N_update_low_order
N_diff = 2             		#  N_diff

IJ_ADJ = 1,1,1		# IJ_ADJ(-1:1) = (coarser level), (same level), (finer level)
ADJ_type = 0,0,0        #  ADJ_type(-1:1) = (coarser level), (same level), (finer level) # (0 - less conservative, 1 - more conservative)

BNDzone = F	  	#  BNDzone
BCtype = 0             	#  BCtype (0 - Dirichlet, 1 - Neuman, 2 - Mixed)

debug_force_wrk_wlt_order = F
debug_level = 0                #  debug
debug_c_diff_fast = 0           # 0- nothing,1- derivative MIN/MAXVALS,2- low level, are printed in c_diff_fast
diagnostics_elliptic =  F       #  diagnostics_elliptic: If T print full diagnostic for elliptic solver
GMRESflag = F              	#  GMRESflag
BiCGSTABflag = F              	#  BiCGSTABflag

Jacoby_correction = T           # Jacoby_correction
multigrid_correction = T        # multigrid_correction
GMRES_correction  = F           # GMRES_correction 
kry_p = 3			#  kry_p
kry_perturb = 3			#  kry_perturb
kry_p_coarse = 100           	#  kry_p_coarse
len_bicgstab = 6             	#  len_bicgstab
len_bicgstab_coarse = 100	#  len_bicgstab_coarse
len_iter = 5            	#  len_iter

W0 = 1.0000000e+00 	#  W0 underrelaxation factor for inner V-cycle
W1 = 1.0000000e+00 	#  W1 underrelaxation factor on the finest level
W2 = 0.6000000e+00 	#  W2 underrelaxation factor for weighted Jacoby (inner points)
W3 = 1.0000000e-00 	#  W3 underrelaxation factor for weighted Jacoby (boundary points)
W_min = 1.00000e-02	#  W_min correcton factor 
W_max = 1.00000e-00 	#  W_max correction factor 

tol_gmres =  1.0e-4
tol_gmres_stop_if_larger = F   # If true stop iterating in gmres solver if error of last iteration was smaller

wlog = T              	#  wlog

dtforce = 0.0           	#  dtforce ! time interval for calculating the lift force
Pressure_force = 0,0,0		#  Pressure forcing term in x-dr

#***************************************************************************************
#  USER CASE INPUTS
#***************************************************************************************

adaptMagVort = F    # Adapt on the magnitude of vorticity
adaptNormS   = F    # Adapt on the L2 of Sij
adaptMagVel  = F    # Adapt on the magnitude of velocity

saveMagVort = T    # Save the magnitude of vorticity
saveNormS   = F    # Save the L2 of Sij

#----------------- Parameters related to Obstacle 
stationary_obstacle = F         # T if obstacle does not move and does not deform in time
obstacle            = T         # imask !TRUE  - there is an obstacle defined
eta_chi 	    = 1.00e-03	#  1e-3  eta_chi

#-------------- Parameters related to joint-flat plate settings:
Re        = 1.0e+02    	   #  Reynolds number (nu = 1/Re)
height    = 0.2		   #  height of rectangular box
length    = 2.0	           #  length of rectangular box
radius    = 1.5            #  radius of object rotation
theta_max = 45.0           #  amplitude of rotation in degrees     
omega     = 1.0            #  angular frequency of rotation in rot/unit time.

Zero_Mean = F              #  T- enforce zero mean for 1:dim first variables (velocity usually), F- do nothing


#-------------- Parameters related to sponge layer settings:
sponge_active = F           # if .TRUE. adds sponge layer to dump velocity fluctuations
x_sponge      = 26.0, 24.0  # x_sponge: (xmin,xmax) locations of sponge layer to dump velocity fluctuations
del_sponge    = 1.0, 0.5    # del_sponge: widths of sponge layer at x_sponge(0:1) locations
eta_sponge    = 1.0E-01     # eta_sponge: characteristic dumping time of sponge layer

divergence_correction = F   # if T - additional projection is perfromed to ensure that the velcity on interpolated mesh is div-free
set_to_zero_if_diverged = F # if T - set the solution of elliptic problem to zero if diverged, F - do not do anything

#verb_level = -1         # conrols the amount of the output