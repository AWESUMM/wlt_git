function movie3D(fileName,varName,st_num,fig_type)

global Zmn Zmx

if strcmp(fig_type,'rho')
  Zmn =0.;
  Zmx = 1.;
else if strcmp(fig_type,'P')
  Zmn =0.;
  Zmx = 25.;
else
  Zmn=[];
  Zmx=[];
end
if strcmp(fig_type,'solution')
  printName=varName;
elseif strcmp(fig_type,'grid')
  printName='grid'
end
  
  for i=1:1:length(st_num)
  figure(1)
  showme3D(fileName,varName,st_num(i),fig_type);
  print('-djpeg',[printName '_t'  num2str(0.1*st_num(i),'%2.1f') '.jpg']);
  pause(1);
end

end