MODULE user_case
  ! CASE 2 D Shallow Water Equations
  USE precision
  USE wlt_vars
  USE wavelet_filters_mod
  USE elliptic_mod
  USE elliptic_vars
  USE wlt_trns_mod
  USE wlt_trns_vars
  USE wlt_trns_util_mod
  USE io_3d_vars
  USE util_mod
  USE util_vars
  USE share_consts
  USE pde
  USE sizes
  USE share_kry
  USE vector_util_mod
  USE field
  USE input_file_reader

  !
  ! case specific variables
  !
  INTEGER :: n_var_eta  ! start of eta in u array
  INTEGER :: n_var_Ux   ! start of Ux in u array
  INTEGER :: n_var_Uy   ! start of Uy in u array
  INTEGER :: n_var_Fx   ! start of Fx in u array
  INTEGER :: n_var_pressure   ! start of pressure in u array
  REAL (pr) :: Re, Fr, Af, alpha, beta
  LOGICAL, PARAMETER :: NS=.TRUE. ! if NS=.TRUE. - Navier-Stoke, esle Euler
  LOGICAL, PARAMETER :: NSdiag=.TRUE. ! if NSdiag=.TRUE. - use viscous terms in user_rhs_diag

CONTAINS

  !
  ! In user_setup_pde() we setup how many variables are integrated, which are interpolated
  ! to the next times step and if any exeact solution exists to compare to during the run.
  ! We also set the variable names that are used when the result files are written.
  !
  ! The following variables must be setup in this routine:
  !
  !
  ! n_integrated     ! first n_integrated eqns will be acted on for time integration
  ! n_var_additional ! 
  ! n_var
  !
  !
  !
  !
  SUBROUTINE  user_setup_pde ( VERB ) 
    IMPLICIT NONE
    LOGICAL, OPTIONAL :: VERB         ! print debug info
    INTEGER :: i

    PRINT * ,''
    PRINT *, '**********************Setting up PDE*****************'
    PRINT * ,'CASE: compressible Couette channel Flow '
    PRINT *, '*****************************************************'

    n_integrated = dim + 1 
    n_time_levels = 1  !--3 time levels

    n_var_time_levels = n_time_levels * n_integrated !--Number of equations (2D velocity at two time levels)

    n_var_additional = 1
    n_var = n_var_time_levels + n_var_additional !--Total number of variables

    n_var_exact = dim + 1

    n_var_eta  = 1
    n_var_Ux   = 2
    n_var_Uy   = 3
    n_var_Fx  = n_var_time_levels + 1 !pressure - dummy variable in this case
    n_var_pressure = n_var_Fx
    !
    !--Allocate logical arrays 
    !  Must be done after setting number of different types of variables
    !  and before setting logical variable mapping arrays
    !
    CALL alloc_variable_mappings

    !
    ! Fill in variable names for default variables for integration meth 2
    ! These may be changed in the call to the user specified routine
    !  user_setup_pde()
    !
    ! In integrated variables (3D)

    IF( dim == 3 ) THEN
       WRITE (u_variable_names(1), u_variable_names_fmt) 'eta  '
       WRITE (u_variable_names(2), u_variable_names_fmt) 'Ux  '
       WRITE (u_variable_names(3), u_variable_names_fmt) 'Uy  '
       WRITE (u_variable_names(4), u_variable_names_fmt) 'Uz  '
       WRITE (u_variable_names(5), u_variable_names_fmt) 'Pressure  '
    ELSE IF(dim == 2) THEN
       WRITE (u_variable_names(1), u_variable_names_fmt) 'eta  '
       WRITE (u_variable_names(2), u_variable_names_fmt) 'Ux  '
       WRITE (u_variable_names(3), u_variable_names_fmt) 'Uy  '
       WRITE (u_variable_names(4), u_variable_names_fmt) 'Pressure  '
    ELSE IF(dim == 1) THEN
       WRITE (u_variable_names(1), u_variable_names_fmt) 'eta  '
       WRITE (u_variable_names(2), u_variable_names_fmt) 'Ux  '
       WRITE (u_variable_names(3), u_variable_names_fmt) 'Pressure  '
    END IF

    !
    ! setup logical variable mapping arrays
    ! This defined which variables are used for adaptation, Which need to be interpololated to the new grid at 
    ! each time step, which are saved, and for which we have an exect solutiuon we want to check at each time step
    !

    !
    ! setup which components we will base grid adaptation on.
    !
    n_var_adapt = .FALSE. !intialize
    n_var_adapt(1:n_var_pressure,0)     = .TRUE. !--Initially adapted variables at first time level

    n_var_adapt(1:n_var_pressure,1)     = .TRUE. !--After first time step adapt on 

    !--integrated variables at first time level

    !Variables that need to be interpoleted to new adapted grid in initial grid adaptation
    n_var_interpolate        = .FALSE. !intialize
    n_var_interpolate(1:n_integrated,0) = .TRUE.  

    !Variables that need to be interpoleted to new adapted grid at each time step
    n_var_interpolate(1:n_integrated,1) = .TRUE. 

    !
    ! setup which components we have an exact solution for
    !
    n_var_exact_soln = .FALSE. !intialize
    n_var_exact_soln(1:dim+1,0:1) = .FALSE.

    !
    ! setup which variables we will save the solution
    !
    n_var_save = .FALSE. !intialize 
    n_var_save(1:n_var_pressure) = .TRUE. ! save all for restarting code

    !
    !--Time level counters for NS integration
    !   Used for integration meth2
    ! n0 is current time step
    ! n1 is t_n-1 time step
    ! n2 is t_n-2 time step
    !
    n0 = 1; n1 = n0+n_var; n2 = n1+n_var

    !
    ! Set the maximum number of components for which we have an exact solution
    !
    n_var_exact = MAX( COUNT(n_var_exact_soln(:,0)),COUNT(n_var_exact_soln(:,1)) )

    !
    ! Setup a scaleCoeff array if we want to tweak the scaling of a variable
    ! ( if scaleCoeff < 1.0 then more points will be added to grid 
	!
    ALLOCATE ( scaleCoeff(1:n_var) )
    scaleCoeff = 1.0_pr


    PRINT *, 'n_integrated = ',n_integrated 
    PRINT *, 'n_time_levels = ',n_time_levels
    PRINT *, 'n_var_time_levels = ',n_var_time_levels 
    PRINT *, 'n_var = ',n_var 
    PRINT *, 'n_var_exact = ',n_var_exact 
    PRINT *, '*******************Variable Names*******************'
    DO i = 1,n_var
       WRITE (*, u_variable_names_fmt) u_variable_names(i)
    END DO
    PRINT *, '****************************************************'

  END SUBROUTINE  user_setup_pde


  !
  ! Set the exact solution for comparison to the simulated solution
  !
  ! u          - array to fill in the exact solution
  ! nlocal       - number of active wavelets   
  ! ne_local        - total number of equations
  ! t          - time of current time step 
  ! l_n_var_exact_soln_index - index into the elements of u for which we need to 
  !                            find the exact solution
  SUBROUTINE  user_exact_soln (u, nlocal,  t_local, l_n_var_exact_soln)  
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: nlocal
    REAL (pr), INTENT (IN) ::  t_local
    REAL (pr), DIMENSION (nlocal,n_var_exact), INTENT (INOUT) :: u
    LOGICAL , INTENT (IN) :: l_n_var_exact_soln(n_var)
    INTEGER :: i

    ! n/a 

    !WRITE( *,'( "in user_exact_soln t_local, nwlt " , f30.20, 1x , i8.8 )' ) t_local , nlocal
   
  END SUBROUTINE  user_exact_soln

  SUBROUTINE user_initial_conditions (u, nlocal, ne_local, t_local, scl, scl_fltwt, iter)
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: nlocal, ne_local
    INTEGER  , INTENT (INOUT) :: iter ! iteration of call while adapting initial grid
    REAL (pr), DIMENSION (nlocal,ne_local), INTENT (INOUT) :: u
    REAL (pr)  :: scl(1:n_var),scl_fltwt
    REAL (pr), INTENT (IN) :: t_local
    INTEGER :: i !TEST

! 
! User defined variables
!
    REAL (pr), DIMENSION (nlocal) :: p

  u(:,n_var_eta) = 1.0_pr
  u(:,n_var_Ux:n_var_Uy) = 1.0e-10_pr
  u(:,n_var_Fx) = Af*cos(2.0_pr*pi*x(:,2)) ! dummy variable
  
  WRITE( *,'( "in user_IC t_local, nwlt " , f30.20, 1x , i5.5 )' ) t_local , nlocal

  END SUBROUTINE user_initial_conditions

!--********************************  
  ! Arguments
  ! u         - field on adaptive grid
  ! nlocal      - number of active points
  ! ne_local       - number of equations
  ! t         - current time
  !
  ! Global variables used
  ! ifn       - number of equations
  ! x()
  ! xO
  ! y0
  !--******************************** 
  SUBROUTINE user_algebraic_BC (Lu, u, nlocal, ne_local, jlev, meth)
    !--Defines boundary condition type
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: jlev, meth, ne_local, nlocal
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (INOUT) :: Lu
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (IN)    :: u

    INTEGER :: ie, i, ii, shift
    REAL (pr), DIMENSION (ne_local,nlocal,dim) :: du, d2u !uncomment if Neuman BC are used, need to call 
    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc


    !BC for both 2D and 3D cases

    !if Neuman BC are used, need to call 
    CALL c_diff_fast (u, du, d2u, jlev, nlocal, meth, 10, ne_local, 1, ne_local)

    DO ie = 1, ne_local
       shift = nlocal*(ie-1)
       !--Go through all Boundary points that are specified
       i_p_face(0) = 1
       DO i=1,dim
          i_p_face(i) = i_p_face(i-1)*3
       END DO
       DO face_type = 0, 3**dim - 1
          face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
          IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
             CALL get_all_indices_by_face (face_type, jlev, nloc, iloc)
             IF(nloc > 0 ) THEN 
                IF( ALL( ABS(face) == (/1,0/) )  ) THEN     ! left & ringt faces
                   IF(ie == n_var_Ux) THEN !Ux
                      Lu(shift+iloc(1:nloc)) = u(shift+iloc(1:nloc))  !Dirichlet conditions
                   ELSE IF(ie == n_var_Uy) THEN !Uy
                      Lu(shift+iloc(1:nloc)) = du(ie,iloc(1:nloc),1)  !Neuman conditions
                   END IF
                ELSE IF( ALL( ABS(face) == (/0,1/) )  ) THEN     ! bottom & top faces
                   IF(ie == n_var_Ux) THEN !Ux
                      Lu(shift+iloc(1:nloc)) = du(ie,iloc(1:nloc),2)  !Neuman conditions
                   ELSE IF(ie == n_var_Uy) THEN !Uy
                      Lu(shift+iloc(1:nloc)) = u(shift+iloc(1:nloc))  !Dirichlet conditions
                   END IF
                ELSE IF( ALL( ABS(face) == (/1,1/) )  ) THEN     ! conners
                   IF(ie == n_var_Ux) THEN !Ux
                      Lu(shift+iloc(1:nloc)) = u(shift+iloc(1:nloc))  !Dirichlet conditions
                   ELSE IF(ie == n_var_Uy) THEN !Uy
                      Lu(shift+iloc(1:nloc)) = u(shift+iloc(1:nloc))  !Dirichlet conditions
                   END IF
                END IF
             END IF
          END IF
       END DO
    END DO

  END SUBROUTINE user_algebraic_BC

  SUBROUTINE user_algebraic_BC_diag (Lu_diag, nlocal, ne_local, jlev, meth)
    !--Defines boundary condition type
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: jlev, meth, ne_local, nlocal
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (INOUT) :: Lu_diag

    INTEGER :: ie, i, ii, shift
    REAL (pr), DIMENSION (nlocal,dim) :: du, d2u  !uncomment if Neuman BC are used
    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc
    
    !BC for both 2D and 3D cases

    !if Neuman BC are used, need to call 
    CALL c_diff_diag ( du, d2u, jlev, nlocal, meth, meth, 10)

    DO ie = 1, ne_local
       shift = nlocal*(ie-1)
       !--Go through all Boundary points that are specified
       i_p_face(0) = 1
       DO i=1,dim
          i_p_face(i) = i_p_face(i-1)*3
       END DO
       DO face_type = 0, 3**dim - 1
          face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
          IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
             CALL get_all_indices_by_face (face_type, jlev, nloc, iloc)
             IF(nloc > 0 ) THEN 
                IF( ALL( ABS(face) == (/1,0/) )  ) THEN     ! left & ringt faces
                   IF(ie == n_var_Ux) THEN !Ux
                      Lu_diag(shift+iloc(1:nloc)) = 1.0_pr  !Dirichlet conditions
                   ELSE IF(ie == n_var_Uy) THEN !Uy
                      Lu_diag(shift+iloc(1:nloc)) = du(iloc(1:nloc),1)  !Neuman conditions
                   END IF
                ELSE IF( ALL( ABS(face) == (/0,1/) )  ) THEN     ! bottom & top faces
                   IF(ie == n_var_Ux) THEN !Ux
                      Lu_diag(shift+iloc(1:nloc)) = du(iloc(1:nloc),2)  !Neuman conditions
                   ELSE IF(ie == n_var_Uy) THEN !Uy
                      Lu_diag(shift+iloc(1:nloc)) = 1.0_pr  !Dirichlet conditions
                   END IF
                ELSE IF( ALL( ABS(face) == (/1,1/) )  ) THEN     ! conners
                   IF(ie == n_var_Ux) THEN !Ux
                      Lu_diag(shift+iloc(1:nloc)) = 1.0_pr  !Dirichlet conditions
                   ELSE IF(ie == n_var_Uy) THEN !Uy
                      Lu_diag(shift+iloc(1:nloc)) = 1.0_pr  !Dirichlet conditions
                   END IF
                END IF
             END IF
          END IF
       END DO
    END DO
  END SUBROUTINE user_algebraic_BC_diag



  SUBROUTINE user_algebraic_BC_rhs (rhs, ne_local, nlocal, jlev)
    !--Sets rhs for boundary conditions
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: ne_local, nlocal, jlev
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (INOUT) :: rhs

    INTEGER :: ie, i, ii, shift
     INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc

    !BC for both 2D and 3D cases

    DO ie = 1, ne_local
       shift = nlocal*(ie-1)
       !--Go through all Boundary points that are specified
       i_p_face(0) = 1
       DO i=1,dim
          i_p_face(i) = i_p_face(i-1)*3
       END DO
       DO face_type = 0, 3**dim - 1
          face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
          IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
             CALL get_all_indices_by_face (face_type, jlev, nloc, iloc)
             IF(nloc > 0 ) THEN 
                IF( ALL( ABS(face) == (/1,0/) )  ) THEN     ! left & ringt faces
                   IF(ie == n_var_Ux) THEN !Ux
                      rhs(shift+iloc(1:nloc)) = 0.0_pr  !Dirichlet conditions
                   ELSE IF(ie == n_var_Uy) THEN !Uy
                      rhs(shift+iloc(1:nloc)) = 0.0_pr  !Neuman conditions
                   END IF
                ELSE IF( ALL( ABS(face) == (/0,1/) )  ) THEN     ! bottom & top faces
                   IF(ie == n_var_Ux) THEN !Ux
                      rhs(shift+iloc(1:nloc)) = 0.0_pr  !Neuman conditions
                   ELSE IF(ie == n_var_Uy) THEN !Uy
                      rhs(shift+iloc(1:nloc)) = 0.0_pr  !Dirichlet conditions
                   END IF
                ELSE IF( ALL( ABS(face) == (/1,1/) )  ) THEN     ! conners
                   IF(ie == n_var_Ux) THEN !Ux
                      rhs(shift+iloc(1:nloc)) = 0.0_pr  !Dirichlet conditions
                   ELSE IF(ie == n_var_Uy) THEN !Uy
                      rhs(shift+iloc(1:nloc)) = 0.0_pr  !Dirichlet conditions
                   END IF
                END IF
             END IF
          END IF
       END DO
    END DO

  END SUBROUTINE user_algebraic_BC_rhs

  SUBROUTINE user_project (u, p, nlocal, meth)
    !--Makes u divergence free
    IMPLICIT NONE

    INTEGER, INTENT (IN) :: meth, nlocal
    REAL (pr), DIMENSION (nlocal),     INTENT(INOUT) :: p
    REAL (pr), DIMENSION (nlocal,n_integrated), INTENT(INOUT) :: u

    INTEGER :: i
    INTEGER, PARAMETER :: ne = 1
    !no projection for shallow water flow

  END SUBROUTINE user_project

  FUNCTION user_rhs (u_integrated,scalar)
    IMPLICIT NONE
    REAL (pr), DIMENSION (ng,ne), INTENT(IN) :: u_integrated !1D flat version of integrated variables without BC 
    REAL (pr), DIMENSION (ng), INTENT(IN) :: scalar
    REAL (pr), DIMENSION (n) :: user_rhs

    INTEGER :: i, j, ie
    INTEGER :: shift(n_integrated)
    INTEGER, PARAMETER :: meth=1
    REAL (pr), DIMENSION (MAX(ne,dim+1),ng,dim) :: du, d2u
    !
    ! User defined variables
    !
    REAL (pr), DIMENSION (ng,n_integrated,dim) :: F
    REAL (pr), DIMENSION (ng) :: p 

    !set up for arbitrary dimensionality
    
    IF(dim /= 2) THEN
       PRINT *, 'ERROR: wrong dimension, the case is only set up for 2-D'
       STOP
    END IF
    DO i = 1, dim+1
       shift(i)=(i-1)*ng
    END DO
    !
    !--Form right hand side of Euler Equations
    !
    DO j = 1, dim
       F(:,1,j) = u_integrated(:,1)! eta !redundunt definition for convinience
       F(:,2,j) = u_integrated(:,1)*u_integrated(:,j+1) ! eta*u_j
       F(:,3,j) = u_integrated(:,j+1) ! u_j
    END DO
    user_rhs = 0.0_pr
    DO j = 1, dim
       CALL c_diff_fast(F(:,:,j), du, d2u, j_lev, ng, meth, 11, dim+1, 1, dim+1) ! du(i,:,k)=dF_ij/dx_k
       user_rhs(shift(1)+1:shift(1)+ng) = user_rhs(shift(1)+1:shift(1)+ng) - du(2,:,j)  ! -dF_ij/dx_j
       user_rhs(shift(j+1)+1:shift(j+1)+ng) = user_rhs(shift(j+1)+j:shift(j+1)+ng) - du(1,:,j)   !d eta/d x_j
       DO i=i,dim
          user_rhs(shift(j+1)+1:shift(j+1)+ng) = user_rhs(shift(j+1)+1:shift(j+1)+ng) &
               - u_integrated(:,i+1)*du(3,:,i)  & !-u_i d u_j/d x_i
               + d2u(3,:,i)/Re/Fr ! 1/(Re Fr) d^2u_j/d x_i^2
       END DO
    END DO
    !FORCING
    user_rhs(shift(2)+1:shift(2)+ng) = user_rhs(shift(2)+1:shift(2)+ng) &           !RHS of du/dt
                                     - alpha*(1._pr+beta*x(:,2))*u_integrated(:,3)& ! -alpha*f*v
                                     + Af*cos(2.0_pr*pi*x(:,2))                     ! + Fx

    user_rhs(shift(3)+1:shift(3)+ng) = user_rhs(shift(3)+1:shift(3)+ng) &           !RHS of dv/dt
                                     + alpha*(1._pr+beta*x(:,2))*u_integrated(:,2)  ! +alpha*f*u
    
    !---------------- Algebraic BC are set up in user_algebraic_BC and they autmatically overwrite evolution BC

  END FUNCTION user_rhs

! find Jacobian of Right Hand Side of the problem
  FUNCTION user_Drhs (u, u_prev, meth)
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: meth
	!u_prev_timestep passed local to cast it into 2dim array to work with vector derivatives
    REAL (pr), DIMENSION (ng,ne) :: u, u_prev
    REAL (pr), DIMENSION (n) :: user_Drhs

    INTEGER :: ie
    !
    ! User defined variables
    !
    REAL (pr), DIMENSION (ng) :: scalar
    REAL (pr) :: delta = 1.0e-7_pr
    
    user_Drhs = ( user_rhs(u_prev + delta * u,scalar) - user_rhs(u_prev,scalar) ) / delta

  END FUNCTION user_Drhs


  !
  ! Uses u_prev_timestep, which is global flat (1D ) array set in time_step_cn()
  !
  FUNCTION user_Drhs_diag (meth)
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: meth
    REAL (pr), DIMENSION (n) :: user_Drhs_diag

    INTEGER :: ie, shift
!
! User defined variables
!
    REAL (pr), DIMENSION (ng,ne) :: u_prev
    REAL (pr), DIMENSION (ng) :: p 

    user_Drhs_diag = 1.0_pr

  END FUNCTION user_Drhs_diag

  FUNCTION user_chi (nlocal, t_local )
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: nlocal
    REAL (pr), INTENT (IN) :: t_local
    REAL (pr), DIMENSION (nlocal) :: user_chi
!
!  Create field object here, 1 inside and 0 outside
!
    user_chi = 0.0_pr
  END FUNCTION user_chi


  !
  ! Calculate any statitics
  !
  ! startup_flag - 0 when adapting to IC,then 1 inmain integration loop
  !
  SUBROUTINE user_stats ( u ,j_mn, startup_flag)
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: startup_flag
    INTEGER , INTENT (IN) :: j_mn 
    REAL (pr), DIMENSION (nwlt,1:n_var), INTENT (IN) :: u
!
! Generate statisitics with variables previously defined in this module
!


  END SUBROUTINE user_stats


  SUBROUTINE user_cal_force (u, n, t_local, force, drag, lift)
    !--Calculates drag and lift on obstacle using penalization formula
    IMPLICIT NONE

    INTEGER, INTENT (IN) :: n
    REAL (pr), INTENT (IN) :: t_local
    REAL (pr), DIMENSION (dim), INTENT (INOUT) :: force
    REAL (pr), INTENT (OUT) :: drag, lift
    REAL (pr), DIMENSION (n,dim) :: u
    drag = 0.0_PR
    lift = 0.0_PR

    !
    ! There is no obstacle in flow
    !

  END SUBROUTINE user_cal_force

  !
  ! Read input from "case_name"_pde.inp file
  ! case_name is in string file_name read from command line
  ! in read_command_line_input()
  ! 
  SUBROUTINE user_read_input()
    IMPLICIT NONE

    call input_real ('Re',Re,'stop',' Re: Reynolds Number')
    call input_real ('Fr',Fr,'stop',' Fr: Froude Number')
    call input_real ('Af',Af,'stop',' Af: amplitude of double-gyre forcing')
    call input_real ('alpha',alpha,'stop',' alpha: length ratio')
    call input_real ('beta',beta,'stop',' beta: coefficient of vertical stratification of rotaion')

  END SUBROUTINE user_read_input



  !
  ! calculate any additional variables
  ! 
  ! arg 
  ! flag  - 0 calledwhile adapting to IC, 1 - called in main time integration loop
  !
  ! These additiona variables are calculated and left in real space.
  !
  SUBROUTINE user_additional_vars( t_local, flag )
    IMPLICIT NONE
    REAL (pr), INTENT (IN) ::  t_local
    INTEGER , INTENT(IN) :: flag ! 0- called during adaption to IC, 1 called during main integration loop
!
! Caluculate additional field variables
!

    u(:,n_var_Fx) = Af*cos(2.0_pr*pi*x(:,2)) ! dummy variable
 
  END SUBROUTINE user_additional_vars


  !
  ! calculate any additional scalar variables
  ! 
  ! arg 
  ! flag  - 0 calledwhile adapting to IC, 1 - called in main time integration loop
  !
  SUBROUTINE user_scalar_vars( flag )
    IMPLICIT NONE
    INTEGER , INTENT(IN) :: flag ! 0- called during adaption to IC, 1 called during main integration loop



  END SUBROUTINE user_scalar_vars

  !
  !************ Calculating Scales ***************************
  !
  ! Note the order of the components in the scl array
  ! correspond to u_tn, v_tn, w_tn, u_tn-1, v_tn-1, w_tn-1, u_tn-2, v_tn-2, w_tn-2
  ! 
 SUBROUTINE user_scales(flag, use_default, u_loc, nlocal, ne_local, l_n_var_adapt , l_n_var_adapt_index, &
       scl, scl_fltwt) !add
    USE precision
    USE pde
    USE util_vars
    USE parallel
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: flag ! 0 during initial adaptation, then 1 during time advancement
    LOGICAL , INTENT(INOUT) :: use_default
    INTEGER, INTENT (IN) :: nlocal, ne_local
    REAL (pr), DIMENSION (1:nlocal,1:ne_local), INTENT (IN) :: u_loc
    LOGICAL , INTENT (IN) :: l_n_var_adapt(ne_local)
    INTEGER , INTENT (IN) :: l_n_var_adapt_index(1:ne_local)
    REAL (pr), DIMENSION (1:ne_local), INTENT (INOUT) :: scl
    REAL (pr), DIMENSION (1:ne_local) :: scl_mean
    REAL (pr) ,  INTENT(IN) ::scl_fltwt !weight for temporal filter on scl
    REAL (pr) :: floor, tmp
    INTEGER :: ie, ie_index, itmp
    REAL (pr) ,save, ALLOCATABLE, DIMENSION (:) :: scl_old
    LOGICAL  ,save :: startup_init = .TRUE.

    !
    ! Ignore the output of this routine and use default scales routine
    !
    use_default = .FALSE. 

    !
    ! NOTE: For a parallel run, synchronize scl(:) across the processors in user_scales.
    !       Use the subroutine scales of default_util.f90 as an example,
    !       subroutine parallel_global_sum of parallel.f90 may be helpful.
    !       Already synchronized: sumdA_global
    !
    IF( .NOT. use_default ) THEN

       IF (par_rank.EQ.0) PRINT *,'Use user_scales() <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<'
       !
       ! ALLOCATE scl_old if it was not done previously in user_scales
       !
       IF( startup_init .AND. .NOT. ALLOCATED(scl_old) ) THEN
          ALLOCATE( scl_old(1:ne_local) )
          scl_old = 0.0_pr
       END IF
       
       floor = 1.e-12_pr
       scl   =1.0_pr
       scl_mean = 0.0_pr
       scl_mean(1) = 1.0_pr
       !
       ! Calculate scl per component
       !
       DO ie=1,ne_local
          IF(l_n_var_adapt(ie)) THEN
             ie_index = l_n_var_adapt_index(ie)
             IF( Scale_Meth == 1 ) THEN ! Use Linf scale
                scl(ie)= MAXVAL ( ABS( u_loc(1:nlocal,ie_index) - scl_mean(ie)) )
                CALL parallel_global_sum( REALMAXVAL=scl(ie) )
                
             ELSE IF ( Scale_Meth == 2 ) THEN ! Use L2 scale
                scl(ie) = SUM( ((u_loc(1:nlocal,ie_index) - scl_mean(ie))**2) )
                itmp = nlocal
                CALL parallel_global_sum( REAL=scl(ie) )
                CALL parallel_global_sum( INTEGER=itmp )
                scl(ie) = SQRT ( scl(ie)/itmp  )
                
             ELSE IF ( Scale_Meth == 3 ) THEN ! Use L2 scale using dA
                scl(ie) = SUM( ((u_loc(1:nlocal,ie_index) - scl_mean(ie))**2)*dA )
                CALL parallel_global_sum( REAL=scl(ie) )
                scl(ie)= SQRT ( scl(ie) / sumdA_global  )
                
             ELSE
                IF (par_rank.EQ.0) THEN
                   PRINT *, 'Error in Scales(), Unknown Scale_Meth type: ', Scale_Meth
                   PRINT *, 'Known Scale_Meth types: 1 = Linf scale, 2 = L2 scale '
                   PRINT *, 'Exiting ...'
                END IF
                CALL parallel_finalize; STOP
             END IF
             
             IF (par_rank.EQ.0) THEN
                WRITE (6,'("Scaling before taking vector(1:dim) magnitude")')
                WRITE (6,'("Scale factor eqn", I4 ," = ", es11.4 ," scalecoeff = ",es11.4 )') &
                     ie, scl(ie), scaleCoeff(ie)
             END IF
             
          END IF
       END DO
       !
       ! take appropriate vector norm over scl(1:dim)
       IF( Scale_Meth == 1 ) THEN ! Use Linf scale
          tmp = MAXVAL(scl(2:dim+1)) !this is statistically equivalent to velocity vector length
          scl(2:dim+1) = tmp
       ELSE IF ( Scale_Meth == 2 ) THEN ! Use L2 scale
          tmp = SQRT(SUM( scl(2:dim+1)**2 ) ) !now velocity vector length, rather than individual component
          scl(2:dim+1) = tmp
       ELSE IF ( Scale_Meth == 3 ) THEN ! Use L2 scale using dA
          tmp = SQRT(SUM( scl(2:dim+1)**2 ) ) !now velocity vector length, rather than individual component
          scl(2:dim+1) = tmp
       END IF
       !
       ! Print out new scl
       !
       DO ie=1,ne_local
          IF(l_n_var_adapt(ie)) THEN
             !this is done if one of the variable is exactly zero inorder not to adapt to the nois
             IF(scl(ie) .le. floor) scl(ie)=1.0_pr 
             tmp = scl(ie)
             ! temporally filter scl
             IF(.not. startup_init ) scl(ie) = scl_fltwt * scl_old(ie) + (1.0_pr - scl_fltwt) * scl(ie)
             scl = scaleCoeff * scl
             IF (par_rank.EQ.0) THEN
                WRITE (6,'("Scaling on vector(1:dim) magnitude")')
                WRITE (6,'("Scale factor eqn", I4 ," = ", es11.4 ," scalecoeff = ",es11.4 )') &
                     ie, scl(ie), scaleCoeff(ie)
                WRITE (6,'("(before filtering = ",es11.4,")" )') tmp
             END IF
          END IF
       END DO
       
       scl_old = scl !save scl for this time step
       startup_init = .FALSE.
       !PRINT *,'TEST scl_old ', scl_old
    END IF 

  END SUBROUTINE user_scales

SUBROUTINE user_cal_cfl (use_default, u, cfl_out)
  USE precision
  USE sizes
  USE pde
  USE parallel
  IMPLICIT NONE
  LOGICAL , INTENT(INOUT) :: use_default
  REAL (pr),                                INTENT (INOUT) :: cfl_out
  REAL (pr), DIMENSION (nwlt,n_integrated), INTENT (IN)    :: u

  INTEGER                    :: i
  REAL (pr)                  :: floor
  REAL (pr), DIMENSION (dim) :: cfl
  REAL (pr), DIMENSION(dim,nwlt) :: h_arr

  use_default = .FALSE.

  floor = 1e-12_pr
  cfl_out = floor
  
  CALL get_all_local_h (h_arr)
  
  DO i = 1, nwlt
     cfl(1:dim) = (ABS ( u(i,2:dim+1) ) + SQRT(u(:,n_var_eta))  )* dt/h_arr(1:dim,i) !gravity wave CFL
     !cfl(1:dim) = ABS ( u(i,2:dim+1) ) * dt/h_arr(1:dim,i) !convective CFL
     cfl_out = MAX (cfl_out, MAXVAL(cfl))
  END DO
  CALL parallel_global_sum( REALMAXVAL=cfl_out )

END SUBROUTINE user_cal_cfl

  !******************************************************************************************
  !************************************* SGS MODEL ROUTINES *********************************
  !******************************************************************************************



!******************************************************************************************
!************************************* SGS MODEL ROUTINES *********************************
!******************************************************************************************


!
! Intialize sgs model
! This routine is called once in the first
! iteration of the main time integration loop.
! weights and model filters have been setup for first loop when this routine is called.
!
SUBROUTINE user_init_sgs_model( )
  IMPLICIT NONE


! LDM: Giuliano

! THE sgs forcign is stored for the time step in sgs_mdl_force(1:nlocal,1:n_integrated)
! where nlocal should be nwlt.


!          print *,'initializing LDM ...'       
!          CALL sgs_mdl_force( u(:,1:n_integrated), nwlt, j_lev, .TRUE.)


END SUBROUTINE user_init_sgs_model

!
! calculate sgs model forcing term
! user_sgs_force is called int he beginning of each times step in time_adv_cn().
! THE sgs forcign is stored for the time step in sgs_mdl_force(1:nlocal,1:n_integrated)
! where nlocal should be nwlt.
! 
! Accesses u from field module, 
!          j_lev from wlt_vars module,
!
SUBROUTINE  user_sgs_force (u_loc, nlocal)
  IMPLICIT NONE

  INTEGER,                         INTENT (IN) :: nlocal
  REAL (pr), DIMENSION (nlocal,n_integrated), INTENT (INOUT) :: u_loc

END SUBROUTINE  user_sgs_force

FUNCTION user_sound_speed (u, neq, nwlt)
  IMPLICIT NONE
  INTEGER, INTENT(IN) :: nwlt, neq
  REAL (pr), DIMENSION (nwlt,neq), INTENT(IN) :: u
  REAL (pr), DIMENSION (nwlt) :: user_sound_speed
  
  user_sound_speed(:) = SQRT(u(:,n_var_eta))  ! gravity wave speed of sound
  
END FUNCTION user_sound_speed

  SUBROUTINE  user_pre_process
    IMPLICIT NONE
     
  END SUBROUTINE user_pre_process

  SUBROUTINE  user_post_process
    IMPLICIT NONE
    
  END SUBROUTINE user_post_process

END MODULE user_case
