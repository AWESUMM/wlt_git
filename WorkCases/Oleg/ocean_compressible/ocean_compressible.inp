# test changes
#------------------------------------------------------------------#
# General input file format                                        #
#                                                                  #
# comments start with # till the end of the line                   #
# string constant are quoted by "..." or '...'                     #
# boolean can be (T F 1 0 on off) in or without ' or " quotes      #
# integers are integers                                            # 
# real numbers are whatever (e.g. 1   1.0   1e-23   1.123d-54 )    #
# vector elements are separated by commas                          #
# spaces between tokens are not important                          #
# empty or comment lines are not important                         #
# order of lines is not important                                  #
#------------------------------------------------------------------#

###############################################################################################################################################################
#  F i l e      I n f o r m a t i o n
#
file_gen = 'ocean_R10.'
results_dir = './results/'
#results_dir = '/lustre/work1/erbr9570/channel_DNS/channel_DNS_R3000_Rt200_M1p5_OS/'
###############################################################################################################################################################

###############################################################################################################################################################
#  D o m a i n      I n f o r m a t i o n
#
dimension = 2		#  dim (2,3), # of dimensions
coord_min = -1.0,-1.0,-1.0	#  XMIN, YMIN, ZMIN, etc
coord_max =  1.0, 0.0, 1.0	#  XMAX, YMAX, ZMAX, etc

j_mn_init = 3              	#  J_mn_init force J_mn == J_INIT while adapting to IC
j_lev_init = 3          	#  starts adapting IC by having all the points on this level of resolution
#j_IC = 7                #  J_ICdatafile if the IC data does not have dimensions in it then mxyz(:)*2^(j_ICdatafile-1) is used
J_MN = 3         	#  J_MN
J_MX = 7            	#  J_MX
J_FILT = 20            	#  J_FILT
J_TREE_ROOT = 3         #  J_TREE_ROOT:  root level for trees for parallel

BNDzone = F	  	#  BNDzone
#j_zn = 2                #  j_zone
#coord_zone_min = -5e+05,-5e+05,-5e+05		# XMINzone, etc
#coord_zone_max =  5e+05, 5e+05, 5e+05		# XMAXzone, etc

M_vector = 4,2,2	#  Mx, etc
periodic = 1,0,1	#  prd(:) (0/1) 0: non-periodic; 1: periodic
uniform = 0,0,0		#  grid(:) (0/1) 0: uniform; 1: non-uniform
i_h = 123456        	#  order of boundaries (1-xmin,2-xmax,3-ymin,4-ymax,5-zmin,6-zmax)
i_l = 001100        	#  algebraic/evolution (1/0) BC order: (lrbt)
###############################################################################################################################################################

###############################################################################################################################################################
#  A W C M      I n f o r m a t i o n
#
eps_init = 2.5000000e-3  	#  EPS used to adapt initial grid  
eps_run  = 2.5000000e-3  	#  EPS used in run  
eps_adapt_steps = 100             # eps_adapt_steps ! how many time steps to adapt from eps_init to eps_run

N_predict = 2           #  N_predict
N_predict_low_order = 1 #  N_predict_low_order
N_update = 0           	#  N_update
N_update_low_order = 0  #  N_update_low_order
N_diff = 2              #  N_diff

Scale_Meth = 3             	# Scale_Meth !1- Linf, 2-L2
scl_fltwt = 0.0           	#  scl temporal filter weight, scl_new = scl_fltwt*scl_old + ( 1-scl_fltwt)*scl_new

IJ_ADJ = 1,1,1		# IJ_ADJ(-1:1) = (coarser level), (same level), (finer level)
ADJ_type = 1,1,1	#  ADJ_type(-1:1) = (coarser level), (same level), (finer level) # (0 - less conservative, 1 - more conservative)
###############################################################################################################################################################

###############################################################################################################################################################
#---------- Spatial Adaptive Epsilon ---------
#

do_Adp_Eps_Spatial        = F              # Perform  Spatial Adaptation  for  Epsilon  Using "Lagrangian Path-Line Diffusive Averaging Evolution Equation"  DefaultValue=F
do_Adp_Eps_Spatial_Evol   = F              # Perform  Spatial Adaptation  for  Epsilon  Using Evolution Equation for "Lagrangian Variable Thresholding"      DefaultValue=F

#IC__AdpEpsSp__from_file   = F             # IF .TRUE.  >>  eps_in_SpatialSpace(:)  will be initialized  with  u(:,n_var_Epsilon) and/or u(:,n_var_EpsilonEvol)  from IC_filename
                                           # IF .FALSE. >>  eps_in_SpatialSpace(:)  will be initialized  with  eps_IC__AdpEpsSp
                                           # This parameter is required only for  IC_restart_mode=3
                                           # NOTE: It is user's responsibility (there is no sanity check) to make sure that IC_filename has  n_var_Epsilon and/or n_var_EpsilonEvol

eps_inside  = 1.00000e-2		   # eps used inside a penalization obstacle
eps_outside = 1.00000e-3		   # eps used outside a penalization obstacle


Psgs_diss_goal__AdpEpsSp  = 0.3            # Percent SGS Dissipation we want to model      ( after Performing  Spatial-Adaptive-Epsilon ) 
eps_IC__AdpEpsSp          = 0.3            # Initialization Epsilon  for IC_Type=0   in case of    IC_restart_mode=3 
eps_min__AdpEpsSp         = 0.2            # Allowable Minimum Value for Epsilon           ( after Performing  Spatial-Adaptive-Epsilon ) 
eps_max__AdpEpsSp         = 0.5            # Allowable Maximum Value for Epsilon           ( after Performing  Spatial-Adaptive-Epsilon )  
max_eps_scale__AdpEpsSp   = 0.1            # Allowable Maximum Chang in (eps/eps_old)**2 - 1.0                                            
Forcing_Factor__AdpEpsSp  = 400.0          # Factor   of   Epsilon Change   due to   Forcing Term
C_nu_art_EpsEvol          = 0.0            # Artificial Diffusion Coefficient for  "Lagrangian Path-Line Diffusive Averaging Evolution Equation"  of  Epsilon
Forcing_Type__AdpEpsSp    = 11             # Forcing_Type__AdpEpsSp                                                                         DefaultValue=-1
Choice__AdpEpsSp          = 1              # 1- Adapt Grid based on do_Adp_Eps_Spatial    2- Adapt Grid based on do_Adp_Eps_Spatial_Evol    DefaultValue=-1
                                           # Required Only If both  do_Adp_Eps_Spatial & do_Adp_Eps_Spatial_Evol  are  .TRUE.


Interpolation_Order       = 1              # Interpolation_Order (1 OR 3)     Required Only  for  do_Adp_Eps_Spatial=T


#do_varying_Goal                    = T                                    # Changing   Psgs_diss_goal__AdpEpsSp 
#teddy_interval_numbers             = 6                                    # Number of Intervals                        i.t.o. teddy_interval
#teddy_interval_end                 = 2,    4,    6,   8,    10,  12       # End of Each Interval                       i.t.o. teddy_interval  (unique integer values only)
#Psgs_diss_goal__AdpEpsSp__varying  = 0.2,  0.25, 0.3, 0.2,  0.3, 0.25     # Percent SGS Dissipation we want to model   for each Interval
#eps_min__AdpEpsSp__varying         = 0.2,  0.2,  0.2, 0.2,  0.2, 0.2      # Allowable Minimum Value for Epsilon        for each Interval 
#eps_max__AdpEpsSp__varying         = 0.43, 0.43, 0.5, 0.43, 0.5, 0.43     # Allowable Maximum Value for Epsilon        for each Interval

IC_Type = 5        #  Initial Condition Type  (0: Constant Everywhere,    1: Spot,    2: Sin,    3: Sin + Spot,    4: Triangle + Spot, 5: internal to penalization obstacle) 

#X_Center__Spot = 0.5
#Y_Center__Spot = 0.5
#Z_Center__Spot = 0.5
#Radius__Spot   = 0.2

#Vertex1__Triangle = 0.1, 0.1, 0.0
#Vertex2__Triangle = 0.2, 0.2, 0.0
#Vertex3__Triangle = 0.3, 0.1, 0.0

#saveSGSD = T     # Save the Resolved SGS Dissipation
###############################################################################################################################################################

###############################################################################################################################################################
#  T i m e    I n t e g r a t i o n     I n f o r m a t i o n
#
time_integration_method = 2  # 0- meth2, 1 -krylov, 2 - Crank Nicolson 

t_begin = 0.000e+00   	#  tbeg  
t_end = 1.00000e+02	#  tend  
#dt = 1.666666e-04      #  dt
dt = 1.0000e-02      #  dt
dtmax = 1.0000000e-01 	#  dtmax
dtmin = 1.0000000e-08  	#  dtmin-if dt < dtmin then exection stops(likely blowing up)
dtwrite = 1.0000000e-01	#  dtwrite 1.0000000e-02 
t_adapt = 1.0000000e+10 #0.0    # when t > t_adapt use an adaptive time step if possible

cflmax = 0.5000000e+00 	#  cflmax
cflmin = 1.0000000e-08 	#  cflmin , Exit if cfl < cflmin
###############################################################################################################################################################

###############################################################################################################################################################
#  R e s t a r t     F i l e      I n f o r m a t i o n
#
do_Sequential_run   = F         # do_Sequential_run
IC_restart_mode     = 0         # 0-new run; 1-hard restart; 2-soft restart; 3-restart from IC --------
                                             # Hard restart - restart a previous run (without changing any parameters)
                                                             # Soft restart - epsilons, names, j_mx, j_tree_root, etc, could be changed
                                                                             # Restart from IC - some parameters can be changed and user initial conditions can be imposed
IC_restart_station  = 0032      # it_start, restart file number to use (NOT iteration!)
IC_file_fmt         = 0         # IC data file format  (0 - native restart file, 1-netcdf, 2- A.Wray in fourier space, 3-simple binary) next line is IC filename
IC_filename = '/lustre/work1/erbr9570/channel_DNS/channel_DNS_R3000_Rt200_M1p5_OS/channel_DNS_R3000_Rt200_M1p5_OS.0032.com.res'
#IC_single_loop       = T
#IC_repartition      = T
#IC_restart          = F         # ICrestart  T or F, restart a previous run. (multually exclusive with IC_from_file)
#IC_from_file        = F         # Do a new run with IC from a restart file
#IC_adapt_grid       = T         # parameter defaulted to .TRUE. If is set to .FALSE. no grid adaptation is done after the data are read.
#Data_file_format    = F         # T = formatted, F = unformatted
###############################################################################################################################################################

###############################################################################################################################################################
#  P a r a l l e l     D o m a i n      D e c o m p o s i t i o n
#
domain_debug  =F
domain_split  = 1, 1, 1      # Parallel domain decomposition: 1 allows subdivision in the direction (x,y,z,etc) (nonzero - allow, 0 - do not split that direction)

domain_meth  = 4      # domain meth decomposition is based on . . .
                      #       0 (default) geometric simultaneous - based on prime number 
                      #       1 geometric sequential (recursive pd N) based on N^(1/D) subdivision
                      #       2 Zoltan library, Geometric
                      #       3 Zoltan library, Hypergraph
                      #       4 Zoltan library, Hilbert Space-Filling Curve
                      #       10 tree number
                      #       11 tree number as if the boundaries are excluded
                      #       -1 read domain decomposition from the restart file
                      #           (during restart or in postprocessing only)
#IC_processors = 2
domain_imbalance_tol = 0.1,0.85,0.95
###############################################################################################################################################################

###############################################################################################################################################################
#  D e b u g     F l a g s
#
debug_force_wrk_wlt_order = F      # this ensures that tree and wrk are same, only set T for debug purposes 
debug_level               = 0      #  debug
verb_level                = 0      # 0- nothing is printed except for errors, warnings, and .inp listing     1- output from I/O related subroutines is printed     DefaultValue=1
debug_c_diff_fast         = 0      # 0- nothing,1- derivative MIN/MAXVALS,2- low level, are printed in c_diff_fast
diagnostics_elliptic      = F      #  diagnostics_elliptic: If T print full diagnostic for elliptic solver
GMRESflag                 = F      #  GMRESflag
BiCGSTABflag              = F      #  BiCGSTABflag
wlog                      = F      #  wlog: elliptic verb
###############################################################################################################################################################

###############################################################################################################################################################
#  S o l v e r    S e t t i n g s
#
Zero_Mean = F           #  T- enforce zero mean for 1:dim first variables (velocity usually), F- do nothing

Jacoby_correction = F           # Jacoby_correction
multigrid_correction = F        # multigrid_correction
GMRES_correction  = F           # GMRES_correction 

kry_p = 3			#  kry_p
kry_p_coarse = 100           	#  kry_p_coarse
len_bicgstab = 6             	#  len_bicgstab
len_bicgstab_coarse = 100	#  len_bicgstab_coarse
len_iter = 5            	#  len_iter

W0 = 1.0000000e+00 	#  W0 underrelaxation factor for inner V-cycle
W1 = 1.0000000e+00 	#  W1 underrelaxation factor on the finest level
W2 = 0.6000000e+00 	#  W2 underrelaxation factor for weighted Jacoby (inner points)
W3 = 1.0000000e-00 	#  W3 underrelaxation factor for weighted Jacoby (boundary points)
W_min = 1.00000e-02	#  W_min correcton factor 
W_max = 1.00000e-00 	#  W_max correction factor 

tol1 = 1.00e-06 	#  tol1 used to set tolerence for non-solenoidal half-step
tol2 = 1.00e-06 	#  tol2 used to set tolerence for solenoidal half-step
tol3 = 1.000e-03         #  used to set tolerence for time step
tol_gmres = 1.0e-08     #  used to set tolerence for gmres iterative solver
tol_gmres_stop_if_larger = F  # If true stop iterating in gmres solver if error of last iteration was smaller
###############################################################################################################################################################

###############################################################################################################################################################
#  F i l t e r    T y p e s
#
                                #  filter types: 0 - none, 1 - >eps, 2 - >2eps,
                                #                3 - level <= j_lev-1, 4 - level <= j_lev-2
                                #                5 - 2eps + adjacent zone,
                                #                6 - local low-pass filter (lowpass_filt_type, lowpassfilt_support, tensorial_filt)
                                #                7 - u > eps_explicit filter
                                #                8 - u > eps_explicit filter + adjecent zone
                                #                9 - u > 2eps_explicit filter
                                #               10 - u > 2eps_explicit filter + adjecent zone
mdl_filt_type_grid = 0                  # dyn mdl grid filter, 0 - 6
ExplicitFilter = F      # ExplicitFilter ,apply grid filter as an explicit filter each time step
###############################################################################################################################################################


###############################################################################################################################################################
#  M y     A d d i t i o n a l     P a r a m e t e r s
#

########## CASE SETUP ##########
Nspec = 2                 # Nsp: number of species
mydt_orig = -1.000000e-03 # mydt_orig: max dt for time integration... dt is only initial dt, set <0 to use dt
NS  = T                   # NS: T to include full Navier-Stokes viscous terms, F for Euler
GRAV = T                  # GRAV: T to include body force terms, F to ingore them
convcfl = T               # convcfl: T to use convective cfl, F to use acoustic cfl
################################

########## NONDIMENSIONALS ##########
Re  = 1.0000000e+00          #  Re: Reynolds Number
Pra = 1.0000000e+00          #  Pra: Prandtl Number
Sc  = 1.0000000e+00          #  Sc: Schmidt Number
Fr  = 1.0000000e+00          #  Fr: Froude Number

p_ref = 0.01       #  Reference pressure in density equation p = p_ref + a^2 (rho-rho_ref) - Ai*a^2*rho*Yi
T_w = 1.0  	   # T_w: wall temperature

# *   = salty, fresh 
rho   = 1.03,  1.0  #  Reference densities at refererence pressure
Cv    = 0.93,  1.0     #  C_v: specific heatconstant
Ma    = 0.1,   0.1    # Ma_i defines compressibility of species: dRho_i = a_i^2* dP -> nondimensional dRho_i = dp/Ma_i^2 
#####################################


########## BOUNDARY CONDITIONS ##########    
BCver = 0     # BCver: 0 = all outflow, 1 = inflow added, Dirichlet;    
ICver = 0      # 0 = Mean flow = 0, 1 = Flow at Mach number in x+ direction 
 
cnvtBC = F     # cnvtBC: T to use convection zone BCs (Freund), F to use algebraic BCs
   ##IF TRUE: Convection BC Zone##
   polyconv = F     # polyconv: T to use polynomials for conv zone BCs,  F to use tanh
   buffU0  = 1.5   # buffU0: scale for convect BC above c0
   buffcvd  =  2.00,  1.20,  1.20,  1.20,  1.20     # buffcvd(1:5): convect zone distances
   buffcvf  =  0.50,  0.45,  0.05,  0.45,  0.05     # buffcvf(1:5): convect zone distance fractions
buffBC = F     # buffBC: T to use buff zone damping BCs (Brinkman), F to use algebraic BCs
   ##IF TRUE: Buffer BC Zone##
   pBrink = F            # pBrink: T to do Brinkman buffer only on pressure
   polybuff = F          # polybuff: T to use polynomials for buff zone BCs,  F to use tanh
   buffSig = 1.000e-01   # buffSig: scale for buff BC
   buffbfd  =  2.00,  1.20,  1.20,  1.20,  1.20     # buffbfd(1:5): buffer zone distances
   buffbff  =  0.50,  0.45,  0.05,  0.45,  0.05     # buffbff(1:5): bufferzone distance fractions
buff_thick = 0.1
#########################################

########## COORDZONE ##########
   Lczn = 5.000000e-02      # Lcoordzn: length of coord_zone on one side
   tfacczn = -1.100000e-00  # tfacczn: factor multiply by time for coord_zone time=L/min(c)*tfac, set <0 for no czn modification
   itfacczn = 1.100000e-00  # itfacczn: factor multiply by initial time for coord_zone itime=Lczn/min(c)*tfac, set <0 to set itime=0
###############################

########### SOLVER SETTINGS ############
   splitFBpress = F                 # splitFBpress: to split derivs for pressure dp/dx FB/BB light/heavy
   stepsplit = F                    # stepsplit: T to do Heaviside split, F to use X
########################################

########## EXTRA VARIABLES ##########
    adaptden = T  # adapt on density
    adaptvel = T  # adapt on momentum
    adapteng = F  # adapt on energy
    adaptspc = T  # adapt on species equations
    adaptprs = F  # adapt on pressure

    adapttmp = F  # adapttmponly: T to adapt on temperature
       tempscl = -1.0  # tempscl: scale for temperature, set < 0 for default, set =0 to use velocity scale
    adaptvelonly = F  # adaptvelonly: T to adapt on velocity instead of momentum
    adaptspconly = F  # adaptspconly: T to adapt on mass fraction (Y) instead of volume fraction (rhoY)
       specscl = 1.0  # specscl: scale for species, set < 0 for default

    savepardom = F  # savepardom: T to save parallel domain as additional var

    #!4extra - these 8 input definitions
    adaptMagVort = F  # adaptMagVort: Adapt on the magnitude of vorticity
    adaptComVort = F  # adaptComVort: Adapt on the components of vorticity
    adaptMagVel  = F  # adaptMagVel: Adapt on the magnitude of velocity
    adaptNormS   = F  # adaptNormS: Adapt on the L2 of Sij
    adaptGradY   = F  # adaptGradY: Adapt on |dY/dx_i*dY/dx_i|
    saveMagVort  = F  # saveMagVort: if T adds and saves magnitude of vorticity
    saveComVort  = F  # saveComVort: if T adds and saves components of vorticity
    saveMagVel   = F  # saveMagVel: if T adds and saves magnitude of velocity
    saveNormS    = F  # saveNormS: if T adds and saves magnitude of strain rate
    saveGradY    = F  # saveGradY: if T adds and saves |dY/dx_i*dY/dx_i|
    saveUcvt     = F  # saveUcvt: save suppport for U and damping for Freund NRBCs
#####################################

############## Additional Planes #################
    additional_planes_active    = T	# additional_planes_active: if T adds nodes for additional user specified planes and activates the module
    j_additional_planes         = 6	# j_additional_planes - level at which points on the planes are added
    n_additional_planes         = 2	# n_additional_planes - number of additional planes to be added
    x_planes                    = 0.0, 0.0 	# x-coordinates for additional planes
    y_planes                    = 1.0,-1.0 	# y-coordinates for additional planes
    z_planes                    = 0.0, 0.0 	# z-coordinates for additional planes
    dir_planes                  =   2,   2	# directions of additional planes
#################################################

############Obstacle############    ERIC: not used for channel flow
    obstacle = F                # T to use  penalization  
    brinkman_penal = F   	# use Brinkman penalization
    vp       = F		# use Volume penalization
 
    read_geometry = F 		# TRUE - the geometry for the obstacle is read from IGS geometry file
    file_geom = 'cylinder.IGS'  # geometry file name in local directory ./geometry

    # DISTANCE FUNCTION
    smooth_dist = F # T smoothes distance function prior finding the normal
    deltadel_loc = 3.0  # sets final diffusion thickness using spatial resolution (dx, dy, or dz), number of points across final thickness

    aero_stats = F              # aero_stats: Calculate and save aerodynamic stats - Cp, Cl, Cf 
    surf_pts  = 30              # number of surface points to use for calculating aerodynamics of obstacle

    #PENALIZATION TYPE
    eta_b      	= 1.000000e-03     # penalization parameter
    eta_c  	= 1.000000e-02     # penalization parameter for no-slip (VP)
    porosity 	= 1.000000e+00     # Brinkman porosity
#Eric's penalization zone parameters
    delta_pts	= 1   		#number of pts (Jmin) to transition from convection to diffusion
    IB_pts	= 1   		#number of pts (Jmin) for convection zone near boundary
#Nurlybeck's penalization zone parameters
    delta_conv = -2.0		# (<0.0 to use old formulation) transition thickness of the convective penalization zone
    delta_diff = -2.0		# transition thickness of the diffusion penalization zone
    shift_conv = -2.0		#thickness when convection is off
    shift_diff = -2.0		#thickness when diffusion is on
    
    doCURV = F			# T to use curvature adjustment to penalization for slip condition
    doNORM = 1.0		# Multiplier on nonconvective terms for energy 

    upwind_norm = -1.0          #Normal-averaged upwind differencing thickness outside of interface. <0.0 to turn off


    visc_factor_n = 1.0          #based on cellular reynolds number
    visc_factor_d = 3.0          #to resolve internal boundary layer
 
#########################################

###########Hyperbolic Module############
    hypermodel = 0	       # hypermodel: flag to turn on hyperbolic module     
    eps_high   = 2.000e-03     # eps_high: error upper bound
    eps_low    = 1.000e-04     # eps_low:  error lower bound
    hyper_mode = 1	       # hyper_mode: 0 for non-conservative, 1 for conservative 
    min_wall_type =  0,0,0     # XMIN, YMIN, ZMIN - 1:reflecting wall, 0:evolutionary
    max_wall_type =  0,0,0     # XMAX, YMAX, ZMAX - 1:reflecting wall, 0:evolutionary

#######################################

###############################################################################################################################################################
