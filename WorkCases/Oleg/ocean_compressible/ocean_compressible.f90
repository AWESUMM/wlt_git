!!$
!!$!****************************************************************************
!!$!* Module for adding Brinkman Penalization terms onto the full NS equations *
!!$!*  -Only valid for single species 10/7/2011                                *
!!$!*  -R is scaled to 1.0
!!$!****************************************************************************
!!$
!!$

MODULE user_case_vars
  USE precision
  INTEGER :: Nspec, Nspecm
  INTEGER :: nden, neng, nprs
  INTEGER, ALLOCATABLE :: nvel(:)
  LOGICAL :: NS ! if NS=.TRUE. - Navier-Stoke, else Euler
  REAL (pr), ALLOCATABLE, DIMENSION (:) ::  mu_in, kk_in, cv_in, Ma_in, rho_in

  INTEGER :: Nsp
  REAL (pr) :: Re
  REAL (pr) :: delta_conv, delta_diff, shift_conv, shift_diff
  REAL (pr) :: deltadel_loc
  LOGICAL :: smooth_dist
  LOGICAL :: doCURV
  REAL (pr) :: doNORM
  
  REAL (pr) :: eta_b, eta_c
  REAL (pr) :: visc_factor_d,visc_factor_n  !parameters for viscous zone around penalized obstacle
  INTEGER :: delta_pts, IB_pts


END MODULE user_case_vars


MODULE Brinkman

  USE precision
  USE wlt_vars
  USE wavelet_filters_mod
  USE elliptic_mod
  USE elliptic_vars
  USE wlt_trns_mod
  USE wlt_trns_vars
  USE wlt_trns_util_mod
  USE io_3d_vars
  USE util_mod
  USE util_vars
  USE share_consts
  USE pde
  USE sizes
  USE share_kry
  USE vector_util_mod
  USE field
  USE input_file_reader
  USE additional_nodes
  USE penalization
  USE user_case_vars
  USE hyperbolic_solver
  USE VT


CONTAINS

  SUBROUTINE user_dist (nlocal, t_local, DISTANCE, NORMAL)
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: nlocal
    REAL (pr), INTENT (IN) :: t_local
    REAL (pr), DIMENSION(nlocal), OPTIONAL, INTENT(INOUT) :: DISTANCE
    REAL (pr), DIMENSION(nlocal, dim), OPTIONAL, INTENT(INOUT) :: NORMAL
    REAL (pr), DIMENSION(nlocal) :: DST
    REAL (pr), DIMENSION(nlocal,dim) :: xp
    INTEGER :: idim, isp
    REAL (pr), DIMENSION(dim) :: Xs
    REAL (pr), DIMENSION(nlocal) :: DSTS

  END SUBROUTINE user_dist



  SUBROUTINE Brinkman_rhs (user_rhs, u_integrated, homog, meth)
    USE user_case_vars
    IMPLICIT NONE
    REAL (pr), DIMENSION (n), INTENT(INOUT) :: user_rhs
    REAL (pr), DIMENSION (ng,ne), INTENT(IN) :: u_integrated !1D flat version of integrated variables without BC 
    INTEGER, INTENT(IN) :: meth
    LOGICAL, INTENT(IN) :: homog

    INTEGER :: ie, shift,i,j, idim
    REAL (pr), DIMENSION (ng,dim)     :: dp
    REAL (pr), DIMENSION (ng,dim)     :: Unorm, Utan, xp, norm
   
    REAL (pr), DIMENSION (ng,ne) :: uh, un
    REAL (pr), DIMENSION (ng,ne+2*dim) :: u_diff
    REAL (pr), DIMENSION (ng,dim) :: Vtan, Vnorm   !velocity
    REAL (pr), DIMENSION (ng,dim*dim) :: visc    !artificial/physical viscosity with continuous fluxes
    REAL (pr), DIMENSION (ne,ng,dim) :: du, d2u, d2un
    REAL (pr), DIMENSION (ne,ng,dim) :: duBD, duFD,dunBD, dunFD, duBD_LO, duFD_LO
    REAL (pr), DIMENSION (ne+2*dim,ng,dim) :: du_diff, du_diffFD, du_diffBD, d2u_diff
    INTEGER :: n_deriv, norm_shift, tan_shift
    REAL (pr), DIMENSION (ng) :: Spenal,Spenal2,Spenal3
    REAL (pr) :: slope, offset
    REAL (pr), DIMENSION (ne) :: Ugoal
    REAL (pr) :: delta_Jmx
    INTEGER :: meth_central, meth_backward, meth_forward  
    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc

    REAL (pr), DIMENSION(ng) :: curv                 ! curvature
    REAL (pr), DIMENSION (ng) :: p                   ! pressure, for curvature adjustment if applicable
    REAL (pr), DIMENSION (dim, ng, dim) :: dn, d2n   ! derivative of normal

    REAL (pr), DIMENSION(ng, dim) :: vel_tan, vel_norm, vel_void
    REAL (pr), DIMENSION (ng)     :: A, C, F
    REAL (pr), DIMENSION(ng, dim) :: B

    REAL (pr), DIMENSION(ng, dim) :: v_back, v_for, vTemp

    ! will be defined later for ocean bathimetry and continental topology

  END SUBROUTINE Brinkman_rhs

! find Jacobian of Right Hand Side of the problem
SUBROUTINE  Brinkman_Drhs (user_Drhs, u, u_prev, meth)
    USE penalization
    USE user_case_vars
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: meth
    REAL (pr), DIMENSION (ng,ne), INTENT(IN) :: u, u_prev
    REAL (pr), DIMENSION (n), INTENT(INOUT) :: user_Drhs
    REAL (pr), DIMENSION (ng,dim)     :: Unorm, Unorm_prev, Utan, Utan_prev, xp, norm, Uprime
    REAL (pr), DIMENSION (ng,dim)     :: Vnorm, Vnorm_prev, Vtan, Vtan_prev
    REAL (pr), DIMENSION (dim,ng,dim) :: dv, d2v, dv_prev, d2v_prev
    REAL (pr), DIMENSION (ne*2,ng,dim) :: dunBD, dunFD,du_pass
    REAL (pr), DIMENSION (ne,ng,dim) :: duBD, duFD, duBD_LO, duFD_LO

    REAL (pr), DIMENSION (ng) :: Spenal,Spenal2,Spenal3
    REAL (pr) :: slope, offset

    REAL (pr) :: delta_Jmx

    INTEGER :: ie, shift,i,j, idim
    INTEGER :: meth_central, meth_backward, meth_forward  

    REAL (pr), DIMENSION (2*ne,ng,dim) :: du  ! 1st derivatives for u (in du(1:ne,:,:)) and u_prev_timestep (in du(ne+1:2*ne,:,:))
    REAL (pr), DIMENSION (ne  ,ng,dim) :: d2u ! 2nd derivatives for u 
    REAL (pr), DIMENSION (2*ne  ,ng,dim) :: d2un ! 2nd derivatives for u 
    REAL (pr), DIMENSION (ne  ,ng,dim) :: du_dummy ! passed when only calculating 1st derivative.
!
! User defined variables
!
    REAL (pr), DIMENSION (ng,ne*2) :: uh, un     !more efficient
    REAL (pr), DIMENSION (ng,2*(ne+2*dim)) :: u_diff
    REAL (pr), DIMENSION (2*(ne+2*dim),ng,dim) :: du_diff, du_diffFD, du_diffBD, d2u_diff
    INTEGER :: n_deriv, norm_shift, tan_shift

    REAL (pr), DIMENSION (ng,dim*dim) :: visc, visc_prev    !artificial/physical viscosity with continuous fluxes
    REAL (pr), DIMENSION (ng) ::  rho_b, e_b, press_b,TT_b
    REAL (pr), DIMENSION (ng) ::  rho, e, press, TT
    REAL (pr), DIMENSION (ng,dim) ::vel, vel_b
    REAL (pr), DIMENSION (ng) ::  Fx
    REAL (pr), DIMENSION (ng,2) :: dFx, dDum
    REAL (pr), DIMENSION (ng,2) :: dUx_b, dUy_b  


    REAL (pr), DIMENSION(ng) :: curv                 ! curvature
    REAL (pr), DIMENSION (dim, ng, dim) :: dn, d2n   ! derivative of normal

    REAL (pr), DIMENSION (ng) :: p, p_prev                   ! pressure, for curvature adjustment if applicable
    REAL (pr), DIMENSION(ng, dim) :: vel_tan, vel_norm, vel_void
    REAL (pr), DIMENSION(ng, dim) :: vel_tan_prev, vel_norm_prev, vel_void_prev
    REAL (pr), DIMENSION (ng)     :: A, C, F
    REAL (pr), DIMENSION (ng)     :: A_prev, C_prev, F_prev
    REAL (pr), DIMENSION(ng, dim) :: B, B_prev

    INTEGER :: prev_shift

    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc

    REAL (pr), DIMENSION(ng, dim) :: v_back, v_for, vTemp

    ! will be defined later for ocean bathimetry and continental topology

END SUBROUTINE Brinkman_Drhs

  !
  ! Uses u_prev_timestep, which is global flat (1D ) array set in time_step_cn()
  !
  SUBROUTINE  Brinkman_Drhs_diag (user_Drhs_diag, meth)
    USE user_case_vars
    USE parallel
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: meth
    REAL (pr), DIMENSION (n), INTENT(INOUT) :: user_Drhs_diag
    INTEGER :: ie, shift,i
    REAL (pr), DIMENSION (ng,dim) :: du_diag, d2u_diag
!
! User defined variables
!
    REAL (pr), DIMENSION (ng,ne) :: u_prev

    ! will be defined later for ocean bathimetry and continental topology

  END SUBROUTINE Brinkman_Drhs_diag

END MODULE Brinkman

MODULE user_case

  USE precision
  USE wlt_vars
  USE wavelet_filters_mod
  USE elliptic_mod
  USE elliptic_vars
  USE wlt_trns_mod
  USE wlt_trns_vars
  USE wlt_trns_util_mod
  USE io_3d_vars
  USE util_mod
  USE util_vars
  USE share_consts
  USE pde
  USE sizes
  USE share_kry
  USE vector_util_mod
  USE field
  USE input_file_reader
  USE parallel
  USE hyperbolic_solver
  USE Brinkman
  USE user_case_vars
  USE VT

  !
  ! case specific variables
  !

  REAL (pr) :: MAreport, CFLreport, CFLconv, CFLacou, maxsos, mydt_orig
  REAL (pr) :: peakchange, maxMa
  REAL (pr), ALLOCATABLE, DIMENSION (:) :: pureX, pureY, pureR, pureg, YR
  REAL (pr) :: e0top, e0bot, e0right,e0left, rho0top, drho0top, rho0bot, drho0bot, rhoetopBC, rhoebotBC, rhoetop, rhoebot ,velleft  !used anymore?
  REAL (pr) ::  e0,rho0
  REAL (pr), ALLOCATABLE, DIMENSION (:) :: Y0top, Y0bot, rhoYtopBC, rhoYbotBC, rhoYtop, rhoYbot, Lxyz
  REAL (pr), DIMENSION (2) :: rhoYg, drho0, dP0, c0, P0
  REAL (pr), ALLOCATABLE, DIMENSION (:) :: bD_in, gr_in
  REAL (pr), ALLOCATABLE, DIMENSION (:,:) :: bndvals
  REAL (pr), DIMENSION (5) :: buffcvd, buffcvf, buffbfd, buffbff
  INTEGER :: n_var_pressure  ! start of pressure in u array
  REAL (pr) ::  Pra, Sc, Fr, p_ref, T_w
  INTEGER :: nbRe, ndom, nton, BCver,ICver,nmsk,ncvtU
  INTEGER, ALLOCATABLE :: nspc(:), nvon(:), nson(:) 
  LOGICAL :: brinkman_penal, vp  !penalization type flags
  LOGICAL, PARAMETER :: NSdiag=.TRUE. ! if NSdiag=.TRUE. - use viscous terms in user_rhs_diag
  INTEGER :: nspc_lil, nspc_big
  LOGICAL :: GRAV
  LOGICAL ::  buffBC, cnvtBC
  LOGICAL ::  polybuff,  polyconv,  pBrink
  REAL (pr) ::  buffU0, buffSig, buffDomFrac
  LOGICAL :: adaptbuoy, savebuoys
  LOGICAL :: adaptMagVort, saveMagVort, adaptComVort, saveComVort, adaptMagVel, saveMagVel, adaptNormS, saveNormS, adaptGradY, saveGradY, saveUcvt  !4extra - this whole line
  INTEGER :: surf_pts
  INTEGER :: nmvt, ncvt(3), nmvl, nnms !4extra - this whole line
  INTEGER, ALLOCATABLE :: ngdy(:) !4extra - this whole line
  REAL (pr) :: Lczn, tfacczn, itfacczn, itczn, tczn, cczn, czn_min, czn_max
  LOGICAL :: modczn, savepardom
  REAL (pr) ::  tempscl, specscl
  INTEGER ::  locnvar
  LOGICAL ::  adaptden, adaptvel, adapteng, adaptspc, adaptprs 
  LOGICAL :: adaptvelonly, adaptspconly, adapttmp, convcfl

  REAL (pr) :: porosity
  REAL (pr) :: buff_thick  !thickness of buffer region
  REAL (pr) :: upwind_norm

CONTAINS

   SUBROUTINE  user_setup_pde ( VERB ) 
    IMPLICIT NONE
    LOGICAL, OPTIONAL :: VERB         ! print debug info
    INTEGER :: i, l   !4extra
    CHARACTER(LEN=8) :: specnum  !4extra
    REAL(pr) :: nd_assym_high, nd_assym_bnd_high, nd2_assym_high, nd2_assym_bnd_high, nd_assym_low, nd_assym_bnd_low, nd2_assym_low, nd2_assym_bnd_low

 
    locnvar = dim+Nspec+1
 
    IF (par_rank.EQ.0) THEN
       PRINT * ,''
       PRINT *, '**********************Setting up PDE*****************'
       PRINT * ,'CASE: Compressible Channel Flow '
       PRINT *, '*****************************************************'
    END IF

    n_integrated = dim + 1 + Nspec      
    
       n_var_additional = 1

!    IF(hypermodel .NE. 0) THEN
!       n_var_additional = n_var_additional + 1
!    END IF

    IF (adapttmp) THEN
       n_var_additional = n_var_additional + 1
       nton = n_integrated + n_var_additional
    END IF

    IF (adaptvelonly) THEN
       IF (ALLOCATED(nvon)) DEALLOCATE(nvon)
       ALLOCATE (nvon(dim))
       DO i=1,dim
          n_var_additional = n_var_additional + 1
          nvon(i) = n_integrated + n_var_additional
       END DO
    END IF

    IF (adaptspconly .AND. Nspec > 1) THEN
       IF (ALLOCATED(nson)) DEALLOCATE(nson)
       ALLOCATE (nson(Nspecm))
       DO l=1,Nspecm
          n_var_additional = n_var_additional + 1
          nson(l) = n_integrated + n_var_additional
       END DO
    END IF

    IF (savepardom) THEN
       n_var_additional = n_var_additional + 1
       ndom = n_integrated + n_var_additional
    END IF

    !4extra - need all these definitions
    IF( adaptMagVort .or. saveMagVort ) THEN
       n_var_additional = n_var_additional + 1
       nmvt = n_integrated + n_var_additional
    END IF
    IF( (adaptComVort .or. saveComVort) .AND. dim==2 ) THEN
       n_var_additional = n_var_additional + 1
       ncvt(:) = n_integrated + n_var_additional
    END IF
    IF( (adaptComVort .or. saveComVort) .AND. dim==3 ) THEN
       DO i=1,3
          n_var_additional = n_var_additional + 1
          ncvt(i) = n_integrated + n_var_additional
       END DO
    END IF
    IF( adaptMagVel  .OR. saveMagVel  ) THEN
       n_var_additional = n_var_additional + 1
       nmvl = n_integrated + n_var_additional
    END IF
    IF( adaptNormS   .OR. saveNormS   ) THEN
       n_var_additional = n_var_additional + 1
       nnms = n_integrated + n_var_additional
    END IF
    IF (ALLOCATED(ngdy)) DEALLOCATE(ngdy)
    IF (Nspec>1) THEN
       ALLOCATE(ngdy(Nspecm))
       IF( adaptGradY   .OR. saveGradY   ) THEN
          DO l=1,Nspecm
             n_var_additional = n_var_additional + 1
             ngdy(l) = n_integrated + n_var_additional
          END DO
       END IF
    END IF
    IF (imask_obstacle) THEN
       n_var_additional = n_var_additional + 1
       nmsk = n_integrated + n_var_additional
    END IF
    IF ( saveUcvt) THEN 
       n_var_additional = n_var_additional + 1
       ncvtU = n_integrated + n_var_additional
    END IF


    n_var = n_integrated + n_var_additional !--Total number of variables

    n_var_exact = 0

    n_var_pressure  = n_integrated + 1 !pressure

!***********Variable orders, ensure 1:dim+1+Nspec are density, velocity(dim), energy, species(Nspecm)**************
    IF (ALLOCATED(nvel)) DEALLOCATE(nvel)
    ALLOCATE(nvel(dim))
    IF (ALLOCATED(nspc)) DEALLOCATE(nspc)
    IF (Nspec>1) ALLOCATE(nspc(Nspecm))

    nden = 1
    DO i=1,dim 
       nvel(i) = i+1
    END DO
    neng = dim+2
    IF (Nspec>1) THEN
       DO i=1,Nspecm
          nspc(i) = dim+2+i
       END DO
    END IF
    nprs = dim+2+Nspec

    nspc_lil = 1
    nspc_big = 0
    IF (Nspec>1) nspc_lil=nspc(1)
    IF (Nspec>1) nspc_big=nspc(Nspecm)

!******************************************************************************************************************
    !
    !--Allocate logical arrays 
    !  Must be done after setting number of different types of variables
    !  and before setting logical variable mapping arrays
    !
    CALL alloc_variable_mappings
    !
    ! Fill in variable names for default variables for integration meth 2
    ! These may be changed in the call to the user specified routine
    !  user_setup_pde()
    !
    ! In integrated variables (3D)
    WRITE (u_variable_names(nden), u_variable_names_fmt) 'Den_rho  '
    WRITE (u_variable_names(nvel(1)), u_variable_names_fmt) 'XMom  '
    IF (dim.GE.2)WRITE (u_variable_names(nvel(2)), u_variable_names_fmt) 'YMom  '
    IF (dim.EQ.3) WRITE (u_variable_names(nvel(3)), u_variable_names_fmt) 'ZMom  '
    WRITE (u_variable_names(neng), u_variable_names_fmt) 'E_Total  '
    IF (Nspec>1) THEN
       DO l=1,Nspecm      
          WRITE (specnum,'(I8)') l          
          WRITE (u_variable_names(nspc(l)), u_variable_names_fmt) TRIM('Spec_Scalar_'//ADJUSTL(TRIM(specnum)))//'  '
       END DO
    END IF
    WRITE (u_variable_names(nprs), u_variable_names_fmt) 'Pressure  '
!    IF(hypermodel .NE. 0) WRITE (u_variable_names(nprs+1), u_variable_names_fmt) 'visc  '


    IF (adapttmp) WRITE (u_variable_names(nton), u_variable_names_fmt) 'Temperature  '
    IF (adaptvelonly) THEN
       WRITE (u_variable_names(nvon(1)), u_variable_names_fmt) 'XVelocity  '
       WRITE (u_variable_names(nvon(2)), u_variable_names_fmt) 'YVelocity  '
       IF (dim.eq.3) WRITE (u_variable_names(nvon(3)), u_variable_names_fmt) 'ZVelocity  '
    END IF
    IF (adaptspconly .AND. Nspec > 1) THEN
       DO l=1,Nspecm      
          WRITE (specnum,'(I8)') l          
          WRITE (u_variable_names(nson(l)), u_variable_names_fmt) TRIM('Mass_Frac_'//ADJUSTL(TRIM(specnum)))//'  '
       END DO
    END IF

    IF (savepardom)  WRITE (u_variable_names(ndom), u_variable_names_fmt) 'MyDomain  '
    
    !4extra - need all these definitions
    IF( adaptMagVort .or. saveMagVort ) WRITE (u_variable_names(nmvt), u_variable_names_fmt) 'MagVort  '
    IF( (adaptComVort .or. saveComVort) .AND. dim==2 )  WRITE (u_variable_names(ncvt(1)), u_variable_names_fmt) 'ComVort  '
    IF( (adaptComVort .or. saveComVort) .AND. dim==3 ) THEN
       DO l=1,3
          WRITE (specnum,'(I8)') l
          WRITE (u_variable_names(ncvt(l)), u_variable_names_fmt) TRIM('ComVort_'//ADJUSTL(TRIM(specnum)))//'  '
       END DO
    END IF
    IF( adaptMagVel  .OR. saveMagVel  )  WRITE (u_variable_names(nmvl), u_variable_names_fmt) 'MagVel  '
    IF( adaptNormS   .OR. saveNormS   )  WRITE (u_variable_names(nnms), u_variable_names_fmt) 'NormS  '
    IF( (adaptGradY   .OR. saveGradY) .AND. Nspec > 1 ) THEN
       DO l=1,Nspecm
          WRITE (specnum,'(I8)') l
          WRITE (u_variable_names(ngdy(l)), u_variable_names_fmt) TRIM('GradY_'//ADJUSTL(TRIM(specnum)))//'  '
       END DO
    END IF

    IF (imask_obstacle) WRITE (u_variable_names(nmsk), u_variable_names_fmt) 'Mask  '
    IF ( saveUcvt) WRITE (u_variable_names(ncvtU), u_variable_names_fmt) 'ConvectZone  '



    !
    ! setup logical variable mapping arrays
    ! This defined which variables are used for adaptation, Which need to be interpololated to the new grid at 
    ! each time step, which are saved, and for which we have an exect solutiuon we want to check at each time step
    !
    !
    ! setup which components we will base grid adaptation on.
    !
    n_var_adapt = .FALSE. !intialize
    IF (adaptden) n_var_adapt(nden,:)                  = .TRUE.
    IF (adaptvel) n_var_adapt(nvel(1):nvel(dim),:)     = .TRUE.
    IF (adapteng) n_var_adapt(neng,:)                  = .TRUE.
    IF (adaptspc) n_var_adapt(nspc(1):nspc(Nspecm),:) = .TRUE.
    IF (adaptprs) n_var_adapt(nprs,:)                  = .TRUE.

    IF (adapttmp) n_var_adapt(nton,:)                  = .TRUE.
    IF (adaptvelonly) n_var_adapt(nvel(1):nvel(dim),:)     = .FALSE.
    IF (adaptvelonly) n_var_adapt(nvon(1):nvon(dim),:)     = .TRUE.
    IF (adaptspconly .AND. Nspec > 1) n_var_adapt(nspc(1):nspc(Nspecm),:) = .FALSE.
    IF (adaptspconly .AND. Nspec > 1) n_var_adapt(nson(1):nson(Nspecm),:) = .TRUE.

    !4extra - adapt
    IF( adaptMagVort ) n_var_adapt(nmvt,:)                  = .TRUE.
    IF( adaptComVort ) n_var_adapt(ncvt(1):ncvt(3),:)       = .TRUE.
    IF( adaptMagVel  ) n_var_adapt(nmvl,:)                  = .TRUE.
    IF( adaptNormS   ) n_var_adapt(nnms,:)                  = .TRUE.
    IF( adaptGradY .AND. Nspec > 1 ) n_var_adapt(ngdy(1):ngdy(Nspecm),:) = .TRUE.

    IF( imask_obstacle ) n_var_adapt(nmsk,:)                  = .TRUE.


    !--integrated variables at first time level
    !Variables that need to be interpoleted to new adapted grid in initial grid adaptation
    !Variables that need to be interpoleted to new adapted grid at each time step
    n_var_interpolate        = .FALSE. !intialize
    n_var_interpolate(1:nprs,:) = .TRUE.  
    IF(imask_obstacle) n_var_interpolate(nmsk,:) = .TRUE.

    IF (adapttmp) n_var_interpolate(nton,:)                  = .TRUE.
    IF (adaptvelonly) n_var_interpolate(nvon(1):nvon(dim),:)     = .TRUE.
    IF (adaptspconly .AND. Nspec > 1) n_var_interpolate(nson(1):nson(Nspecm),:) = .TRUE.

    !4extra - need these
    IF( saveMagVort ) n_var_interpolate(nmvt,:)                   = .TRUE.
    IF( saveComVort ) n_var_interpolate(ncvt(1):ncvt(3),:)        = .TRUE.
    IF( saveMagVel  ) n_var_interpolate(nmvl,:)                   = .TRUE.
    IF( saveNormS   ) n_var_interpolate(nnms,:)                   = .TRUE.
    IF( saveGradY .AND. Nspec > 1 ) n_var_interpolate(ngdy(1):ngdy(Nspecm),:)  = .TRUE.
    IF( imask_obstacle    ) n_var_interpolate(nmsk,:)    = .TRUE.
    IF( saveUcvt    ) n_var_interpolate(ncvtU,:)    = .TRUE.

    !
    ! setup which components we have an exact solution for
    !
    n_var_exact_soln = .FALSE. !intialize

    !
    ! setup which variables we will save the solution
    !
    n_var_save = .FALSE. !intialize 
    n_var_save(1:nprs) = .TRUE. ! save all for restarting code
!    IF(hypermodel .NE. 0) n_var_save(nprs+1) = .TRUE.

    IF (adapttmp) n_var_save(nton) = .TRUE.
    IF (savepardom)  n_var_save(ndom)  = .TRUE.

    !4extra - need these
    IF( saveMagVort ) n_var_save(nmvt)     = .TRUE.
    IF( saveComVort ) n_var_save(ncvt(1):ncvt(3))  = .TRUE.
    IF( saveMagVel  ) n_var_save(nmvl)     = .TRUE.
    IF( saveNormS   ) n_var_save(nnms)     = .TRUE.
    IF( saveGradY .AND. Nspec > 1) n_var_save(ngdy(1):ngdy(Nspecm)) = .TRUE.
    IF( imask_obstacle    ) n_var_save(nmsk)    = .TRUE.
    IF( saveUcvt    ) n_var_save(ncvtU)    = .TRUE.

    n_var_req_restart = n_var_save !does not require hyperbolic module viscosity for restart
    


    !
    ! Set the maximum number of components for which we have an exact solution
    !
    n_var_exact = MAX( COUNT(n_var_exact_soln(:,0)),COUNT(n_var_exact_soln(:,1)) )

    !
    ! Setup a scaleCoeff array if we want to tweak the scaling of a variable
    ! ( if scaleCoeff < 1.0 then more points will be added to grid 
    !
    ALLOCATE ( scaleCoeff(1:n_var) )
    scaleCoeff = 1.0_pr

    IF( adaptMagVort ) scaleCoeff(nmvt) = 5.0_pr 
   
    
    IF(hypermodel .NE. 0) THEN
       IF(ALLOCATED(n_var_hyper)) DEALLOCATE(n_var_hyper)
       ALLOCATE(n_var_hyper(1:n_var))
       n_var_hyper = .FALSE.
       !n_var_hyper(nden) = .TRUE.
!       n_var_hyper(neng) = .TRUE.
       n_var_hyper = .TRUE.


       IF(ALLOCATED(n_var_hyper_active)) DEALLOCATE(n_var_hyper_active)
       ALLOCATE(n_var_hyper_active(1:n_integrated))
       n_var_hyper_active = .FALSE.
      ! n_var_hyper_active(nden) = .TRUE.
!       n_var_hyper_active(neng) = .TRUE.
       n_var_hyper_active = .TRUE.

    END IF

    IF (par_rank.EQ.0) THEN
       PRINT *, 'n_integrated = ',n_integrated 

       PRINT *, 'n_var = ',n_var 
       PRINT *, 'n_var_exact = ',n_var_exact 
       PRINT *, '*******************Variable Names*******************'
       DO i = 1,n_var
          WRITE (*, u_variable_names_fmt) u_variable_names(i)
       END DO
       PRINT *, '****************************************************'
    END IF

    !Use variable thresholding
    IF ( do_Adp_Eps_Spatial .OR. do_Adp_Eps_Spatial_Evol ) &
    CALL user_setup_pde__VT

  END SUBROUTINE  user_setup_pde


  !
  ! Set the exact solution for comparison to the simulated solution
  !
  ! u          - array to fill in the exact solution
  ! nlocal       - number of active wavelets   
  ! ne_local        - total number of equations
  ! t          - time of current time step 
  ! l_n_var_exact_soln_index - index into the elements of u for which we need to 
  !                            find the exact solution
  SUBROUTINE  user_exact_soln (u, nlocal,  t_local, l_n_var_exact_soln)  
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: nlocal
    REAL (pr), INTENT (IN) ::  t_local
    REAL (pr), DIMENSION (nlocal,n_var_exact), INTENT (INOUT) :: u
    LOGICAL , INTENT (IN) :: l_n_var_exact_soln(n_var)
    INTEGER :: i

  END SUBROUTINE  user_exact_soln

  SUBROUTINE user_initial_conditions (u, nlocal, ne_local, t_local, scl, scl_fltwt,iter)
    USE penalization
    USE parallel 
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: nlocal, ne_local
    REAL (pr), DIMENSION (nlocal,ne_local), INTENT (INOUT) :: u
    REAL (pr)  :: scl(1:n_var),scl_fltwt
    REAL (pr), INTENT (IN) :: t_local
    INTEGER, INTENT (INOUT) :: iter
    INTEGER :: i, l, ie, ii, k
    INTEGER :: iseedsize, jx, jy, jz, sprt
    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc

    REAL (pr) :: u_max, u_min, v_max, v_min, w_max, w_min, c_max, c_min
    REAL (pr), DIMENSION(nlocal,1) :: forcing
    REAL (pr), DIMENSION(nlocal) :: Ma_m
    REAL (pr), DIMENSION(nlocal) :: rand_array
    REAL (pr), DIMENSION(nlocal,2) :: loc_support, Dloc_support

    REAL (pr) :: r_max, PI,nwlt_total,v_mean
    REAL (pr) :: fourier_coeff1, fourier_coeff2,fourier_coeff3,L_x, L_y, L_z, x_mid, y_mid, z_mid
    REAL (pr) :: a_rndm, b_rndm, c_rndm
    REAL (pr) :: y_pos, width
    REAL (pr), DIMENSION(nlocal) :: u_r
    INTEGER, ALLOCATABLE, DIMENSION(:) :: seed

    REAL (pr) :: nwlt_max
    REAL (pr) :: d_thick, delta_trans 

    REAL (pr), DIMENSION(nlocal) :: u_pert
    INTEGER, DIMENSION(nlocal) :: iy_loc 
    REAL (pr), DIMENSION(1,nlocal,dim) :: du, d2u 

    INTEGER :: kx,ky,kz 
    REAL (pr) ::   Uprime=1.0e-1_pr
    REAL (pr) ::   Umean,U2mean,Ar,Br,Cr,Dr
    REAL (pr) ::   lx,ly,lz                            !!! by Giuliano

    pi = 2.0_pr*ASIN(1.0_pr)

  IF (IC_restart_mode > 0 ) THEN !in the case of restart
     !do nothing
  ELSE IF (IC_restart_mode .EQ. 0) THEN
     u = 0.0_pr
     !Set Density
     !parameters
     ALLOCATE(seed(2))
     SEED = (/54321,12345/)
     CALL RANDOM_SEED(PUT=SEED(1:2))
     !**** NEED to make it more sophisticated (activate all modes) + divergence free
     U2mean = 0.0_pr
     ! domain lengths
     lx = xyzlimits(2,1) - xyzlimits(1,1)        !by Giuliano
     ly = xyzlimits(2,2) - xyzlimits(1,2)        !by Giuliano
     IF (dim == 2) THEN
        DO kx=1,Nxyz(1)/4
        DO ky=1,Nxyz(2)/4
           CALL RANDOM_NUMBER(Ar)
           CALL RANDOM_NUMBER(Br)
           CALL RANDOM_NUMBER(Cr)

           u(:,nvel(1)) = u(:,nvel(1)) + (Ar*SIN(2.0_pr*pi/lx*(x(:,1)-xyzlimits(1,1))*REAL(kx,pr))  &
                             +Br*COS(2.0_pr*pi/lx*(x(:,1)-xyzlimits(1,1))*REAL(kx,pr))) &
                                *SIN(2.0_pr*pi/ly*(x(:,2)-xyzlimits(1,2))*REAL(ky,pr))
           ! to make it divergence-free
           u(:,nvel(2)) = u(:,nvel(2)) + ly/lx*REAL(kx,pr)/REAL(ky,pr) &
                            *(Ar*COS(2.0_pr*pi/lx*(x(:,1)-xyzlimits(1,1))*REAL(kx,pr))  &
                             -Br*SIN(2.0_pr*pi/lx*(x(:,1)-xyzlimits(1,1))*REAL(kx,pr))) &
                               *(COS(2.0_pr*pi/ly*(x(:,2)-xyzlimits(1,2))*REAL(ky,pr))-1.0_pr) 
           IF (Nspec>1) THEN
              u(:,nspc(1)) = u(:,nspc(1)) + ly/lx*REAL(kx,pr)/REAL(ky,pr) &
                            *( Cr*COS(2.0_pr*pi/lx*(x(:,1)-xyzlimits(1,1))*REAL(kx,pr)) &
                              +Ar*SIN(2.0_pr*pi/lx*(x(:,1)-xyzlimits(1,1))*REAL(kx,pr)) )
           END IF
        END DO
        END DO

     ELSE IF(dim ==3) THEN

        lz = xyzlimits(2,3) - xyzlimits(1,3)
        DO kx=1,Nxyz(1)/4
        DO ky=1,Nxyz(2)/4
        DO kz=1,Nxyz(3)/4
           CALL RANDOM_NUMBER(Ar)
           CALL RANDOM_NUMBER(Br)
           CALL RANDOM_NUMBER(Cr)
           CALL RANDOM_NUMBER(Dr)
           u(:,nvel(1)) = u(:,nvel(1)) + (Ar*SIN(2.0_pr*pi/lx*(x(:,1)-xyzlimits(1,1))*REAL(kx,pr))  &
                +Br*COS(2.0_pr*pi/lx*(x(:,1)-xyzlimits(1,1))*REAL(kx,pr))) &
                *SIN(2.0_pr*pi/ly*(x(:,2)-xyzlimits(1,2))*REAL(ky,pr))
           u(:,nvel(3)) = u(:,nvel(3)) + (Cr*SIN(2.0_pr*pi/lz*(x(:,3)-xyzlimits(1,3))*REAL(kz,pr))  &
                +Dr*COS(2.0_pr*pi/lz*(x(:,3)-xyzlimits(1,3))*REAL(kz,pr))) &
                *SIN(2.0_pr*pi/ly*(x(:,2)-xyzlimits(1,2))*REAL(ky,pr))
           ! to make it divergence-free
           u(:,nvel(2)) = u(:,nvel(2))+( ly/lx*REAL(kx,pr)/REAL(ky,pr) &
                *(Ar*COS(2.0_pr*pi/lx*(x(:,1)-xyzlimits(1,1))*REAL(kx,pr))    &
                -Br*SIN(2.0_pr*pi/lx*(x(:,1)-xyzlimits(1,1))*REAL(kx,pr)))   &
                +  ly/lz*REAL(kz,pr)/REAL(ky,pr) &
                *(Cr*COS(2.0_pr*pi/lz*(x(:,3)-xyzlimits(1,3))*REAL(kz,pr))    &
                -Dr*SIN(2.0_pr*pi/lz*(x(:,3)-xyzlimits(1,3))*REAL(kz,pr))) ) &
                *(COS(2.0_pr*pi/ly*(x(:,2)-xyzlimits(1,2))*REAL(ky,pr))-1.0_pr)
         END DO
         END DO
         END DO
         
     ENDIF
     DO ie = 1,dim
        U2mean = U2mean+SUM(u(:,nvel(ie))**2*dA)/sumdA 
     END DO
     u(:,nvel(1:dim)) = Uprime/SQRT(U2mean)*u(:,nvel(1:dim))
!!! Poiseuille profile
!       u(:,1) = u(:,1) - 0.0_pr*grad_P(1)/nu*(1.0_pr-x(:,2)**2)
!     u(:,nvel(1)) = u(:,nvel(1)) +  1.5_pr*(1.0_pr-x(:,2)**2) !background flow       !!$       CALL randspecU(iter)

     u = 0.0_pr
     IF (Nspec>1) THEN
        u(:,nspc(1)) = 0.5_pr*(1.0_pr+COS(2.0_pr*pi/lx*(x(:,1)-xyzlimits(1,1))))
     END IF
     u(:,nden)  = (1.0_pr - SUM(u(:,nspc(1):nspc(Nspecm)),DIM=2)) * rho_in(Nspec)
     Ma_m(:)  = (1.0_pr - SUM(u(:,nspc(1):nspc(Nspecm)),DIM=2)) * Ma_in(Nspec)**2
     DO l=1,Nspecm
        u(:,nden) = u(:,nden) + u(:,nspc(l))*rho_in(l) !rho_m
        Ma_m(:) = Ma_m(:) + u(:,nspc(l))*Ma_in(l)**2 !rho_m
     END DO
     Ma_m = SQRT(Ma_m)
     u(:,nden) = u(:,nden)*exp(-Ma_m**2*(x(:,2)-xyzlimits(2,2))/Fr)
     DO l=1,Nspecm
        u(:,nspc(l)) = u(:,nden)*u(:,nspc(l)) !rho_s
     END DO
     DO l = 1, dim
        u(:,nvel(l)) = u(:,nden)*u(:,nvel(l))
     END DO
     !Set Energy for constant temperature
     u(:,neng)              = 0.5_pr*(SUM(u(:,nvel(1:dim))**2,DIM=2))/u(:,nden) + u(:,nden)*user_Cv(u, ne_local, nlocal)*T_W

     nwlt_max = nwlt
     CALL parallel_global_sum(REALMAXVAL=nwlt_max)
     IF(par_rank .EQ. 0 ) PRINT *, 'nwlt_max:', nwlt_max


  END IF

  !variable thresholding
  IF ( do_Adp_Eps_Spatial .OR. do_Adp_Eps_Spatial_Evol ) &
       CALL user_initial_conditions__VT(u, nlocal, ne_local)!, t_local, scl, scl_fltwt, iter)
 

END SUBROUTINE user_initial_conditions

  !--********************************  
  ! Arguments
  ! u         - field on adaptive grid
  ! nlocal      - number of active points
  ! ne_local       - number of equations
  ! t         - current time
  !
  ! Global variables used
  ! ifn       - number of equations
  ! x()
  ! xO
  ! y0
  !--******************************** 
  SUBROUTINE user_algebraic_BC (Lu, u, nlocal, ne_local, jlev, meth)
    !--Defines boundary condition type
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: jlev, meth, ne_local, nlocal
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (INOUT) :: Lu
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (IN)    :: u

    INTEGER :: ie, i, ii, shift, denshift,eshift
    INTEGER, DIMENSION (dim) :: velshift 
    REAL (pr), DIMENSION (ne_local,nlocal,dim) :: du, d2u !uncomment if Neuman BC are used, need to call 
    REAL (pr), DIMENSION (1,nlocal,dim) :: du_prev
    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc
    
    !shift markers for BC implementation
    denshift  = nlocal*(nden-1)
    DO i = 1,dim
       velshift(i) = nlocal*(nvel(i)-1)
    END DO
    eshift    = nlocal*(neng-1)

    !BC for both 2D and 3D cases

    !if Neuman BC are used, need to call 
    CALL c_diff_fast (u_prev_timestep(denshift+1:denshift+nlocal), du_prev, d2u, jlev, nlocal, meth, 10, 1, 1, 1)
    CALL c_diff_fast (u, du, d2u, jlev, nlocal, meth, 10, ne_local, 1, ne_local)
    DO ie = 1, ne_local
       shift = nlocal*(ie-1)
       !--Go through all Boundary points that are specified
       i_p_face(0) = 1
       DO i=1,dim
          i_p_face(i) = i_p_face(i-1)*3
       END DO
       DO face_type = 0, 3**dim - 1
          face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
          IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
             CALL get_all_indices_by_face (face_type, jlev, nloc, iloc)
             IF(nloc > 0 ) THEN 
                IF( face(2) == 1  ) THEN  !top - Full
                   IF(ie == nvel(2) ) THEN  !Momentums
                      Lu(shift+iloc(1:nloc)) = u(shift+iloc(1:nloc))       !Dirichlet conditions
                   ELSE IF(ie >= nvel(1) .AND. ie <= nvel(dim) .AND. ie /= nvel(2)) THEN  !BC: du/dn = 0 
                         Lu(shift+iloc(1:nloc)) = du(ie,iloc(1:nloc),2) & 
                               - u(shift+iloc(1:nloc))/u_prev_timestep(denshift+iloc(1:nloc))*du_prev(1,iloc(1:nloc),2) &
                               + u_prev_timestep(shift+iloc(1:nloc))/u_prev_timestep(denshift+iloc(1:nloc)) * &
                               ( du_prev(1,iloc(1:nloc),2)*u(denshift+iloc(1:nloc))/u_prev_timestep(denshift+iloc(1:nloc)) - du(nden,iloc(1:nloc),2))
                   ELSE IF(ie == neng) THEN ! BC: dT/dn = 0
                      Lu(shift+iloc(1:nloc)) = du(ie,iloc(1:nloc),2) - u(shift+iloc(1:nloc))/u_prev_timestep(denshift+iloc(1:nloc))*du_prev(1,iloc(1:nloc),2) &
                           + u_prev_timestep(shift+iloc(1:nloc))/u_prev_timestep(denshift+iloc(1:nloc)) * &
                           ( du_prev(1,iloc(1:nloc),2)*u(denshift+iloc(1:nloc))/u_prev_timestep(denshift+iloc(1:nloc)) - du(nden,iloc(1:nloc),2))
                   ELSE IF(Nspec > 1 .AND. ie >= nspc(1) .AND. ie <= nspc(Nspecm)) THEN !BC: dY/dn = 0 
                      Lu(shift+iloc(1:nloc)) = du(ie,iloc(1:nloc),2) - u(shift+iloc(1:nloc))/u_prev_timestep(denshift+iloc(1:nloc))*du_prev(1,iloc(1:nloc),2) &
                           + u_prev_timestep(shift+iloc(1:nloc))/u_prev_timestep(denshift+iloc(1:nloc)) * &
                           ( du_prev(1,iloc(1:nloc),2)*u(denshift+iloc(1:nloc))/u_prev_timestep(denshift+iloc(1:nloc)) - du(nden,iloc(1:nloc),2))
                   END IF
                ELSE IF( face(2) == -1  ) THEN  !bottom - Full
                   IF(ie >= nvel(1) .AND. ie <= nvel(dim)) THEN  !Momentums
                      Lu(shift+iloc(1:nloc)) = u(shift+iloc(1:nloc))       !Dirichlet conditions
                   ELSE IF(ie == nvel(1) ) THEN  !Momentums
                      Lu(shift+iloc(1:nloc)) = u(shift+iloc(1:nloc))  !-     u(denshift+iloc(1:nloc))   !Dirichlet conditions  - additional for couette flow
                   ELSE IF(ie == neng) THEN
                      Lu(shift+iloc(1:nloc)) = u(shift+iloc(1:nloc)) - u(denshift+iloc(1:nloc))*user_Cv (u_prev_timestep, ne_local, nlocal)*T_w     !Dirichlet conditions - Isothermal, no slip 
                      ! once energy equition is coupled, Cv might need to be linerized
                   ELSE IF(Nspec > 1 .AND. ie >= nspc(1) .AND. ie <= nspc(Nspecm)) THEN ! BC: dY/dn=0
                      Lu(shift+iloc(1:nloc)) = du(ie,iloc(1:nloc),2) - u(shift+iloc(1:nloc))/u_prev_timestep(denshift+iloc(1:nloc))*du_prev(1,iloc(1:nloc),2) &
                           + u_prev_timestep(shift+iloc(1:nloc))/u_prev_timestep(denshift+iloc(1:nloc)) * &
                           ( du_prev(1,iloc(1:nloc),2)*u(denshift+iloc(1:nloc))/u_prev_timestep(denshift+iloc(1:nloc)) - du(nden,iloc(1:nloc),2))
                   END IF
                END IF
             END IF
          END IF
       END DO
    END DO

    !variable thresholding if Evolution eps is used, penalization generally uses interpolation
    IF ( do_Adp_Eps_Spatial_Evol ) &
    CALL VT_algebraic_BC (Lu, u, du, nlocal, ne_local, jlev, meth)

  END SUBROUTINE user_algebraic_BC

  SUBROUTINE user_algebraic_BC_diag (Lu_diag, nlocal, ne_local, jlev, meth)
    !--Defines boundary condition type
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: jlev, meth, ne_local, nlocal
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (INOUT) :: Lu_diag

    INTEGER :: ie, i, ii, shift, denshift

    REAL (pr), DIMENSION (nlocal,dim) :: du_diag, d2u_diag  !uncomment if Neuman BC are used
    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc
    REAL (pr), DIMENSION (1,nlocal,dim) :: du_prev, d2u
    INTEGER, PARAMETER :: methprev=1

    !BC for both 2D and 3D cases

    denshift = nlocal*(nden-1)

    !if Neuman BC are used, need to call 
    CALL c_diff_diag ( du_diag, d2u_diag, jlev, nlocal, meth, meth, 10)
    CALL c_diff_fast (u_prev_timestep(denshift+1:denshift+nlocal), du_prev, d2u, jlev, nlocal, meth, 10, 1, 1, 1)

    DO ie = 1, ne_local
       shift = nlocal*(ie-1)
       !--Go through all Boundary points that are specified
       i_p_face(0) = 1
       DO i=1,dim
          i_p_face(i) = i_p_face(i-1)*3
       END DO
       DO face_type = 0, 3**dim - 1
          face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
          IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
             CALL get_all_indices_by_face (face_type, jlev, nloc, iloc)
             IF(nloc > 0 ) THEN 
                IF(face(2) == 1  ) THEN !top
                   IF(ie == nvel(2) ) THEN  !Momentums
                      Lu_diag(shift+iloc(1:nloc)) = 1.0_pr       !Dirichlet conditions
                   ELSE IF(ie >= nvel(1) .AND. ie <= nvel(dim) .AND. ie /= nvel(2)) THEN  !BC: du/dn = 0 
                      Lu_diag(shift+iloc(1:nloc)) = du_diag(iloc(1:nloc),2) - du_prev(1,iloc(1:nloc),2)/u_prev_timestep(denshift+iloc(1:nloc))
                   ELSE IF(ie == neng) THEN ! BC: dT/dn = 0
                      Lu_diag(shift+iloc(1:nloc)) = du_diag(iloc(1:nloc),2) - du_prev(1,iloc(1:nloc),2)/u_prev_timestep(denshift+iloc(1:nloc))
                   ELSE IF(Nspec > 1 .AND. ie >= nspc(1) .AND. ie <= nspc(Nspecm)) THEN !BC: dY/dn = 0 
                      Lu_diag(shift+iloc(1:nloc)) = du_diag(iloc(1:nloc),2) - du_prev(1,iloc(1:nloc),2)/u_prev_timestep(denshift+iloc(1:nloc))
                   END IF
                ELSE IF( face(2) == -1  ) THEN ! bottom
                   IF(ie >= nvel(1) .AND. ie <= nvel(dim)) THEN  !Momentums
                      Lu_diag(shift+iloc(1:nloc)) = 1.0_pr       !Dirichlet conditions
                   ELSE IF(ie == neng) THEN
                      Lu_diag(shift+iloc(1:nloc)) = 1.0_pr  !Dirichlet conditions
                   ELSE IF(Nspec > 1 .AND. ie >= nspc(1) .AND. ie <= nspc(Nspecm)) THEN !BC: dY/dn = 0 
                      Lu_diag(shift+iloc(1:nloc)) = du_diag(iloc(1:nloc),2) - du_prev(1,iloc(1:nloc),2)/u_prev_timestep(denshift+iloc(1:nloc))
                   END IF
                END IF
             END IF
          END IF
       END DO
    END DO

    !varibale thresholding for evolution eps
    !IF ( do_Adp_Eps_Spatial_Evol ) &
    !CALL VT_algebraic_BC_diag (Lu_diag, du, nlocal, ne_local, jlev, meth) 

  END SUBROUTINE user_algebraic_BC_diag

  SUBROUTINE user_algebraic_BC_rhs (rhs, ne_local, nlocal, jlev)
    !--Sets rhs for boundary conditions
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: ne_local, nlocal, jlev
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (INOUT) :: rhs

    INTEGER :: ie, i, ii, shift, denshift
    INTEGER, PARAMETER ::  meth=1
    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc
    REAL (pr), DIMENSION (ne_local,nlocal,dim) :: duall, d2uall
    REAL (pr), DIMENSION (1,nlocal,dim) :: du_prev, du, d2u  
    REAL (pr) :: p_bc

    p_bc = 1.0_pr  !pressure defined for outflow boundary conditions

    denshift=nlocal*(nden-1)    

    DO ie = 1, ne_local
       shift = nlocal*(ie-1)
       !--Go through all Boundary points that are specified
       i_p_face(0) = 1
       DO i=1,dim
          i_p_face(i) = i_p_face(i-1)*3
       END DO
       DO face_type = 0, 3**dim - 1
          face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
          IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
             CALL get_all_indices_by_face (face_type, jlev, nloc, iloc)
             IF(nloc > 0 ) THEN 
                IF( face(2) == 1  ) THEN ! top             
                   IF(ie == nvel(2) ) THEN  !Momentums
                      rhs(shift+iloc(1:nloc)) = 0.0_pr       !Dirichlet conditions
                   ELSE IF(ie >= nvel(1) .AND. ie <= nvel(dim) .AND. ie /= nvel(2)) THEN  !Momentums
                      rhs(shift+iloc(1:nloc)) = 0.0_pr ! no sheer
                   ELSE IF(ie == neng) THEN
                      rhs(shift+iloc(1:nloc)) = 0.0_pr !define temp in algebraic BC
                   ELSE IF(Nspec > 1 .AND. ie >= nspc(1) .AND. ie <= nspc(Nspecm))THEN
                      rhs(shift+iloc(1:nloc)) = 0.0_pr ! no flux
                   END IF
                ELSE IF( face(2) == -1  ) THEN  ! bottom          
                   IF(ie >= nvel(1) .AND. ie <= nvel(dim) ) THEN  !Momentums
                      rhs(shift+iloc(1:nloc)) = 0.0_pr       !Dirichlet conditions
                   ELSE IF(ie == neng) THEN
                      rhs(shift+iloc(1:nloc)) = 0.0_pr !define temp in algebraic BC
                   ELSE IF(Nspec > 1 .AND. ie >= nspc(1) .AND. ie <= nspc(Nspecm))THEN
                      rhs(shift+iloc(1:nloc)) = 0.0_pr ! no flux
                   END IF
                END IF
             END IF
          END IF
       END DO
    END DO

    !variable thresholding for evolution eps
    IF ( do_Adp_Eps_Spatial_Evol ) &
         CALL VT_algebraic_BC_rhs(rhs, ne_local, nlocal, jlev) 

  END SUBROUTINE user_algebraic_BC_rhs

  




  SUBROUTINE user_convectzone_BC_rhs (myrhs_loc, ulc, myne_loc, mynloc, myjl, mymeth)
    IMPLICIT NONE

    !SUBROUTINE only for single species flow
    INTEGER , INTENT (IN) :: myne_loc, mynloc, myjl, mymeth
    REAL (pr), DIMENSION (mynloc*myne_loc), INTENT (INOUT) :: myrhs_loc
    REAL (pr), DIMENSION (mynloc,myne_loc), INTENT (IN) :: ulc

    INTEGER :: ie, i, j, shift
    REAL (pr), DIMENSION (myne_loc,mynloc,dim) :: du, d2u  !derivatives of native variables
    REAL (pr), DIMENSION (mynloc,2*dim) :: bigU
    REAL (pr), DIMENSION (mynloc,myne_loc) :: myu_zer



    CALL c_diff_fast(ulc, du, d2u, myjl, mynloc, mymeth, 10, myne_loc, 1, myne_loc) 

    IF(cnvtBC) THEN !add convection to bufferzone
       CALL user_bufferfunc_eric(bigU, mynloc, buffcvd, polyconv)

       DO j = 1,dim
          DO ie=1,myne_loc
             shift = (ie-1)*mynloc   
!!$       myrhs_loc(shift+1:shift+mynloc) = myrhs_loc(shift+1:shift+mynloc) - (bigU(:,2)*c0(1)-bigU(:,1)*c0(2))*buffU0*du(ie,:,1)
             IF (j ==1 .AND. BCver .NE. 0) THEN  !add inflow at xmin
                myrhs_loc(shift+1:shift+mynloc) = myrhs_loc(shift+1:shift+mynloc) - ( bigU(:,2*j-1)+bigU(:,2*j) )*buffU0*du(ie,:,j) 
             ELSE
                myrhs_loc(shift+1:shift+mynloc) = myrhs_loc(shift+1:shift+mynloc) - ( - bigU(:,2*j-1)+bigU(:,2*j) )*buffU0*du(ie,:,j) 
             END IF
          END DO
       END DO
    END IF


  END SUBROUTINE user_convectzone_BC_rhs 

  SUBROUTINE user_convectzone_BC_Drhs (Dmyrhs_loc, ulc, ulc_prev, myne_loc, mynloc, myjl, mymeth)
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: myne_loc, mynloc, myjl, mymeth
    REAL (pr), DIMENSION (mynloc*myne_loc), INTENT (INOUT) :: Dmyrhs_loc
    REAL (pr), DIMENSION (mynloc,myne_loc), INTENT (IN) :: ulc, ulc_prev

    INTEGER :: ie, i, j, shift
    REAL (pr), DIMENSION (myne_loc,mynloc,dim) :: du, d2u
    REAL (pr), DIMENSION (mynloc,2*dim) :: bigU



    CALL c_diff_fast(ulc, du, d2u, myjl, mynloc, mymeth, 10, myne_loc, 1, myne_loc) 


       CALL user_bufferfunc_eric(bigU, mynloc, buffcvd, polyconv)
       DO j = 1,dim
          DO ie=1,myne_loc
             shift = (ie-1)*mynloc   
!!$       Dmyrhs_loc(shift+1:shift+mynloc) = Dmyrhs_loc(shift+1:shift+mynloc) - (bigU(:,2)*c0(1)-bigU(:,1)*c0(2))*buffU0*du(ie,:,1)
             IF (j == 1 .AND. BCver .NE. 0) THEN
                Dmyrhs_loc(shift+1:shift+mynloc) = Dmyrhs_loc(shift+1:shift+mynloc) - ( bigU(:,2*j-1)+bigU(:,2*j) )*buffU0*du(ie,:,j)   
             ELSE
                Dmyrhs_loc(shift+1:shift+mynloc) = Dmyrhs_loc(shift+1:shift+mynloc) - ( - bigU(:,2*j-1)+bigU(:,2*j) )*buffU0*du(ie,:,j)   
             END IF
          END DO
       END DO
  END SUBROUTINE user_convectzone_BC_Drhs 



  SUBROUTINE user_convectzone_BC_Drhs_diag (Drhs_diag_local, ulc_prev, myne_loc, mynloc, myjl, mymeth)
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: myne_loc, mynloc, myjl, mymeth
    REAL (pr), DIMENSION (mynloc*myne_loc), INTENT (INOUT) :: Drhs_diag_local
    REAL (pr), DIMENSION (mynloc,myne_loc), INTENT (IN) :: ulc_prev

    REAL (pr), DIMENSION (mynloc,dim) :: du_diag_local, d2u_diag_local
    INTEGER :: ie, i,j, shift
    REAL (pr), DIMENSION (mynloc,2*dim) :: bigU

    CALL c_diff_diag(du_diag_local, d2u_diag_local, j_lev, mynloc, mymeth, mymeth, 10)
    CALL user_bufferfunc_eric(bigU, mynloc, buffcvd, polyconv)


    DO j = 1,dim
       DO ie=1,myne_loc
          shift = (ie-1)*mynloc   
!!$          Drhs_diag_local(shift+1:shift+mynloc) = Drhs_diag_local(shift+1:shift+mynloc) - (bigU(:,2)*c0(1)-bigU(:,1)*c0(2))*buffU0*du_diag_local(:,1)
          IF (j==1 .AND. BCver .NE. 0) THEN
             Drhs_diag_local(shift+1:shift+mynloc) = Drhs_diag_local(shift+1:shift+mynloc) - ( bigU(:,2*j-1) + bigU(:,2*j) )*buffU0*du_diag_local(:,j) 
          ELSE
             Drhs_diag_local(shift+1:shift+mynloc) = Drhs_diag_local(shift+1:shift+mynloc) - ( - bigU(:,2*j-1) + bigU(:,2*j) )*buffU0*du_diag_local(:,j) 
          END IF
       END DO
    END DO
  END SUBROUTINE user_convectzone_BC_Drhs_diag 

  SUBROUTINE user_bufferfunc_eric (bigU, mynloc, buffd,locpoly)
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: mynloc
    REAL (pr), DIMENSION (mynloc,2*dim), INTENT(INOUT) :: bigU
    REAL (pr), DIMENSION (5), INTENT(IN) :: buffd
    LOGICAL, INTENT(IN) :: locpoly

    REAL (pr), DIMENSION (mynloc,2*dim) :: bigU2
    INTEGER :: ie, i, j, shift, stri, stpi
    REAL (pr) :: slope, offset

    bigU(:,:)=0.0_pr
    bigU2(:,:)=0.0_pr

!Establish buffer functions for each direction - boundary order for bigU: xmin, xmax, ymin, ymax, zmin, zmax

    !slope = (ATANH( (0.5_pr-10.0_pr**-3.0_pr)/0.5_pr)- ATANH( (-0.5_pr+10.0_pr**-3.0_pr)/0.5_pr))/(buff_thick) !same algorithm as for volume penalization
    slope  =  ( ATANH(  (10.0_pr**-3.0_pr - 0.5_pr  )/0.5_pr)- ATANH(0.49_pr/0.5_pr)    ) / -buff_thick
    offset =  ATANH(0.49_pr/0.5_pr)/slope !offste distance

    DO j = 1,dim
       bigU(:,2*j-1) = MAX(0.5_pr - 10.0_pr**-3.0_pr - 0.5_pr*TANH(slope*( x(:,j) - (xyzlimits(1,j) + offset) )) , 0.0_pr )
       bigU(:,2*j) = MAX(0.5_pr - 10.0_pr**-3.0_pr + 0.5_pr*TANH(slope*( x(:,j) - (xyzlimits(2,j) - offset) )) , 0.0_pr )
    END DO


  END SUBROUTINE user_bufferfunc_eric 


  SUBROUTINE user_bndfunc (bndonly, mynloc, myjl)
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: mynloc, myjl
    REAL (pr), DIMENSION (mynloc), INTENT(INOUT) :: bndonly
    INTEGER :: i
    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc
    bndonly(:) = 0.0_pr
    i_p_face(0) = 1
    DO i=1,dim
       i_p_face(i) = i_p_face(i-1)*3
    END DO
    DO face_type = 0, 3**dim - 1
       face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
       IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
          CALL get_all_indices_by_face (face_type, myjl, nloc, iloc)
          IF(nloc > 0 ) THEN
             IF (ABS(face(1)) == 1) THEN
                bndonly(iloc(1:nloc)) = 1.0_pr
             END IF
          END IF
       END IF
    END DO
  END SUBROUTINE user_bndfunc 

  SUBROUTINE user_flxfunc (flxfunc, mynloc, myjl)
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: mynloc, myjl
    REAL (pr), DIMENSION (dim,mynloc,dim), INTENT(INOUT) :: flxfunc
    REAL (pr), DIMENSION (mynloc) :: myflxfunc
    INTEGER :: i,j
       CALL user_bndfunc(myflxfunc, mynloc, myjl)  !puts 1.0 on boundaries, 0.0 elsewhere 
       DO j=1,dim
          DO i=1,dim
             !IF ((i.ne.j) .AND. (MIN(i,j).eq.1)) THEN
             IF ((i.ne.j) .AND. (j.eq.1)) THEN
          
                   flxfunc(i,:,j) = 1.0_pr - myflxfunc(:)
             ELSE
                flxfunc(i,:,j) = 1.0_pr
             END IF
          END DO
       END DO
  END SUBROUTINE user_flxfunc



  FUNCTION user_chi (nlocal, t_local)
    USE parallel
    USE threedobject
    IMPLICIT NONE
    INTEGER,          INTENT (IN) :: nlocal
    REAL (pr),        INTENT (IN) :: t_local
    REAL (pr), DIMENSION (nlocal) :: user_chi
    REAL (pr), DIMENSION (nlocal) :: dist_loc
    REAL(pr), DIMENSION(nlocal,1) :: forcing
    REAL(pr) :: smooth_coeff

    user_chi = 0.0_pr

    WHERE ( SQRT( (x(:,1)- SIGN(2.5_pr/3.0_pr,x(:,2)))**2 + (ABS(x(:,2)) - 0.7_pr )**2) <= 0.2_pr )
       user_chi = 1.0_pr
    ELSEWHERE
       user_chi = 0.0_pr
    END WHERE

  END FUNCTION user_chi

  SUBROUTINE user_bufferzone_BC_rhs (myrhs_loc, ulc, myne_loc, mynloc, myjl, mymeth)
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: myne_loc, mynloc, myjl, mymeth
    REAL (pr), DIMENSION (mynloc*myne_loc), INTENT (INOUT) :: myrhs_loc
    REAL (pr), DIMENSION (mynloc,myne_loc), INTENT (IN) :: ulc

    INTEGER :: ie, i, j, shift, stri, stpi
    REAL (pr), DIMENSION (mynloc,2*dim) :: bigU

    CALL user_bufferfunc_eric(bigU, mynloc, buffbfd, polybuff)

    DO j = 1,dim
       DO ie=1,myne_loc
          shift = (ie-1)*mynloc   
          myrhs_loc(shift+1:shift+mynloc) = myrhs_loc(shift+1:shift+mynloc) - buffSig*((bigU(:,2*j)+bigU(:,2*j-1))*ulc(:,ie)-bigU(:,2*j-1)*bndvals(ie,2*j-1)-bigU(:,2*j)*bndvals(ie,2*j)) 
       END DO
    END DO



  END SUBROUTINE user_bufferzone_BC_rhs 

  SUBROUTINE user_bufferzone_BC_Drhs (Dmyrhs_loc, ulc, ulc_prev, myne_loc, mynloc, myjl, mymeth)
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: myne_loc, mynloc, myjl, mymeth
    REAL (pr), DIMENSION (mynloc*myne_loc), INTENT (INOUT) :: Dmyrhs_loc
    REAL (pr), DIMENSION (mynloc,myne_loc), INTENT (IN) :: ulc, ulc_prev

    INTEGER :: ie, i, j, shift, stri, stpi
    REAL (pr), DIMENSION (mynloc,2*dim) :: bigU

    CALL user_bufferfunc_eric(bigU, mynloc, buffbfd, polybuff)

    DO j = 1,dim
       DO ie = 1,myne_loc
          shift = (ie-1)*mynloc   
          Dmyrhs_loc(shift+1:shift+mynloc) = Dmyrhs_loc(shift+1:shift+mynloc) - buffSig*(bigU(:,2*j)+bigU(:,2*j-1))*ulc(:,ie)
       END DO
    END DO

  END SUBROUTINE user_bufferzone_BC_Drhs 

  SUBROUTINE user_bufferzone_BC_Drhs_diag (Drhs_diag_local, ulc_prev, myne_loc, mynloc, myjl, mymeth)
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: myne_loc, mynloc, myjl, mymeth
    REAL (pr), DIMENSION (mynloc*myne_loc), INTENT (INOUT) :: Drhs_diag_local
    REAL (pr), DIMENSION (mynloc,myne_loc), INTENT (IN) :: ulc_prev

    INTEGER :: ie, i, j, shift, stri, stpi
    REAL (pr), DIMENSION (mynloc,2*dim) :: bigU

    CALL user_bufferfunc_eric(bigU, mynloc, buffbfd, polybuff)

    DO j = 1,dim
       DO ie=1,myne_loc
          shift = (ie-1)*mynloc   
          Drhs_diag_local(shift+1:shift+mynloc) = Drhs_diag_local(shift+1:shift+mynloc) - buffSig*(bigU(:,2*j)+bigU(:,2*j-1))
       END DO
    END DO
  END SUBROUTINE user_bufferzone_BC_Drhs_diag 

  SUBROUTINE internal_rhs (int_rhs,u_integrated,addingon)
    USE penalization
    IMPLICIT NONE
    REAL (pr), DIMENSION (ng,ne), INTENT(IN) :: u_integrated !1D flat version of integrated variables without BC 
    REAL (pr), DIMENSION (ng*ne), INTENT(INOUT) :: int_rhs
    INTEGER, INTENT(IN) :: addingon 

    INTEGER :: i, j, l, idim, ie, shift,  tempintd, jshift
    INTEGER, PARAMETER :: meth=1
    REAL (pr), DIMENSION (ne*dim,ng,dim) :: du, d2u
    REAL (pr), DIMENSION (ne*dim,ng,dim) :: dudum, d2udum
    REAL (pr), DIMENSION (ne*dim,ng,dim) :: duC, d2uC,duF, d2uF,duB, d2uB,du_upwind, d2u_upwind    !forward and backwards differencing ERIC: apply to derivatives in interior domain
    REAL (pr), DIMENSION (ng,ne*dim) :: F
    REAL (pr), DIMENSION (ng,dim+Nspec) :: v ! velocity components + temperature + Y
    REAL (pr) :: usej
    REAL (pr), DIMENSION (ng) :: p
    REAL (pr), DIMENSION (ng,3) :: props

    REAL (pr) , DIMENSION(ng) :: dist_loc
    REAL (pr) , DIMENSION(ng,dim) :: obs_norm
    REAL (pr) , DIMENSION(ng) :: visc_loc
    REAL (pr) :: min_h
    REAL (pr) , DIMENSION(2,ng,dim) :: bias_mask
    INTEGER :: meth_central,meth_backward,meth_forward

    REAL (pr) :: diff_max,diff_min


    meth_central  = meth + BIASING_NONE
    meth_backward = meth + BIASING_BACKWARD
    meth_forward  = meth + BIASING_FORWARD

    min_h = MINVAL(h(1,1:dim)/2**(j_mx - 1))

    int_rhs = 0.0_pr
    IF (Nspec>1) THEN
       DO l = 1,Nspecm
          v(:,dim+1+l) = u_integrated(:,nspc(l))/u_integrated(:,nden)
       END DO
    END IF
    p(:) = 1.0_pr
    IF (Nspec>1)  p(:) = (1.0_pr-SUM(v(:,dim+2:dim+Nspec),DIM=2))  !Y_Nspec
    props(:,1) = p(:)*cv_in(Nspec) !cv_misc
    props(:,2) = p(:)*Ma_in(Nspec)**2 !(Ma_Nspec)^2
    props(:,3) = p(:)*rho_in(Nspec) !rho_m
    DO l=1,Nspecm
       props(:,1) = props(:,1) + v(:,dim+1+l)*cv_in(l) !cv
       props(:,2) = props(:,2) + v(:,dim+1+l)*Ma_in(l)**2 !Ma^2
       props(:,3) = props(:,3) + v(:,dim+1+l)*rho_in(l) !rho_m_ref
    END DO
    p(:) = p_ref + (u_integrated(:,nden) - props(:,3) )/props(:,2)
    v(:,dim+1) = (u_integrated(:,neng)-0.5_pr*SUM(u_integrated(:,nvel(1):nvel(dim))**2,DIM=2)/u_integrated(:,nden))/props(:,1)/u_integrated(:,nden) !Temperature

    DO i = 1, dim
       v(:,i) = u_integrated(:,nvel(i)) / u_integrated(:,nden) ! u_i
    END DO

    !
    !--Form right hand side of Euler Equations
    !
    F(:,:) = 0.0_pr
    DO j = 1, dim  !iterate across dimensions
       jshift=ne*(j-1)
       F(:,nden+jshift) = F(:,nden+jshift) + u_integrated(:,nvel(j)) ! rho*u_j
       DO i = 1, dim
          F(:,nvel(i)+jshift) = F(:,nvel(i)+jshift) + u_integrated(:,nvel(i))*v(:,j) ! rho*u_i*u_j
       END DO
       F(:,nvel(j)+jshift) = F(:,nvel(j)+jshift) + p(:) ! rho*u_i^2+p
       F(:,neng+jshift) = F(:,neng+jshift) + ( u_integrated(:,neng) + p(:) )*v(:,j) ! (rho*e+p)*u_j
       DO l=1,Nspecm
          F(:,nspc(l)+jshift) = F(:,nspc(l)+jshift) + v(:,dim+1+l)*u_integrated(:,nvel(j)) ! rho*Y*u_j
       END DO  
    END DO

    !
    !-- Form right hand side of Navier-Stokes Equations
    !
    IF(NS) THEN 

       CALL c_diff_fast(v, du(1:dim+Nspec,:,:), d2u(1:dim+Nspec,:,:), j_lev, ng, meth_central, 10, dim+Nspec, 1,dim+Nspec) ! du_i/dx_j & dT/dx_j & dY_l/dx_j

       IF (upwind_norm > 0.0_pr) THEN  !do upwinding near the edges of the obstacle
          CALL c_diff_fast(v, duB(1:dim+Nspec,:,:), d2uB(1:dim+Nspec,:,:), j_lev, ng, meth_backward, 10, dim+Nspec, 1,dim+Nspec) ! du_i/dx_j & dT/dx_j & dY_l/dx_j
          CALL c_diff_fast(v, duF(1:dim+Nspec,:,:), d2uF(1:dim+Nspec,:,:), j_lev, ng, meth_forward, 10, dim+Nspec, 1,dim+Nspec) ! du_i/dx_j & dT/dx_j & dY_l/dx_j
          CALL user_dist (ng, t, DISTANCE=dist_loc, NORMAL=obs_norm)   !distance function from main code

          DO i = 1,dim
             DO j = 1,dim +Nspec
                du_upwind(j,:,i) = (1.0_pr+SIGN(1.0_pr,obs_norm(:,i)))/2.0_pr*duB(j,:,i) + (1.0_pr-SIGN(1.0_pr,obs_norm(:,i)))/2.0_pr*duF(j,:,i)
                d2u_upwind(j,:,i) = (1.0_pr+SIGN(1.0_pr,obs_norm(:,i)))/2.0_pr*d2uB(j,:,i) + (1.0_pr-SIGN(1.0_pr,obs_norm(:,i)))/2.0_pr*d2uF(j,:,i)
                du(j,:,i) = (du_upwind(j,:,i)  - du(j,:,i) )*obs_norm(:,i)*MAX(dist_loc/upwind_norm/min_h + 1.0_pr ,0.0_pr) + du(j,:,i)
             END DO
          END DO

          p(:)=1.0_pr
          IF (Nspec>1) p(:)=p(:)-SUM(v(:,dim+2:dim+Nspec),DIM=2)
          props(:,1)=p(:)*mu_in(Nspec)  !mu_Nspec
          props(:,2)=p(:)*kk_in(Nspec)  !kk_Nspec
          props(:,3)=p(:)*bD_in(Nspec)  !bD_Nspec
          IF (Nspec>1) THEN
             DO l=1,Nspecm
                props(:,1)=props(:,1)+v(:,dim+1+l)*mu_in(l)  !mu
                props(:,2)=props(:,2)+v(:,dim+1+l)*kk_in(l)  !kk
                props(:,3)=props(:,3)+v(:,dim+1+l)*bD_in(l)  !bD
             END DO
          END IF
          p(:) = du(1,:,1) !reuse of a variable
          DO i = 2, dim
             p(:) = p(:) + du(i,:,i) !du_i/dx_i
          END DO
          
          d2u(:,:,:) = 1.0_pr
          DO j = 1, dim
             jshift=ne*(j-1)
             DO i = 1, dim
                F(:,nvel(i)+jshift) = F(:,nvel(i)+jshift) - props(:,1)*( du(i,:,j)+du(j,:,i) ) * d2u(i,:,j)  !rho*u_i*u_j - 2*mu*S_ij/Re  
             END DO
             F(:,nvel(j)+jshift) = F(:,nvel(j)+jshift) + 2.0_pr/3.0_pr*props(:,1)*p(:) * d2u(j,:,j)  !rho*u_i*u_j - tau_ij/Re  [ tau_ij = 2*mu*( S_ij - 2/3*S_kk*delta_ij ) ] 
             DO i = 1, dim
                F(:,neng+jshift) =  F(:,neng+jshift) - props(:,1)*( du(i,:,j)+du(j,:,i) )*v(:,i) * d2u(i,:,j)  !(e+p)*u_j - u_i*2*mu*S_ij/Re 
             END DO
             F(:,neng+jshift) =  F(:,neng+jshift) + 2.0_pr/3.0_pr*props(:,1)*p(:)*v(:,j) * d2u(j,:,j) - props(:,2)*du(dim+1,:,j) * d2u(2,:,j)  
             !(e+p)*u_j - u_i*tau_ij/Re - q_j/Re/Pra [ q_j = mu dT/dx_j ]
             IF (Nspec>1) THEN
                DO l=1,Nspecm
                   F(:,neng+jshift) =  F(:,neng+jshift) - u_integrated(:,nden)*cv_in(l)*v(:,dim+1)*props(:,3)*du(dim+1+l,:,j) * d2u(2,:,j)
                END DO
                F(:,neng+jshift) =  F(:,neng+jshift) +  u_integrated(:,nden)*cv_in(Nspec)*v(:,dim+1)*props(:,3)*SUM(du(dim+2:dim+Nspec,:,j),DIM=1) * d2u(2,:,j)
                DO l=1,Nspecm
                   F(:,nspc(l)+jshift) =  F(:,nspc(l)+jshift) - u_integrated(:,nden)*props(:,3)*du(dim+1+l,:,j) * d2u(2,:,j)
                END DO
             END IF
          END DO
       END IF
    END IF
    IF (GRAV) THEN    
      shift = (neng-1)*ng   
      DO j=1,dim
         int_rhs(shift+1:shift+ng) = int_rhs(shift+1:shift+ng) + u_integrated(:,nvel(j))*gr_in(j)
      END DO
    END IF
    tempintd=ne*dim

    CALL c_diff_fast(F(:,1:tempintd), du(1:tempintd,:,:), d2u(1:tempintd,:,:), j_lev, ng, meth_central, 10, tempintd, 1, tempintd)  !du(i,:,k)=dF_ij/dx_k
    
    IF (upwind_norm > 0.0_pr) THEN  !do upwinding near the edges of the obstacle
       CALL c_diff_fast(F(:,1:tempintd), duB(1:tempintd,:,:), d2uB(1:tempintd,:,:), j_lev, ng, meth_backward, 10, tempintd, 1, tempintd)  !du(i,:,k)=dF_ij/dx_k
       CALL c_diff_fast(F(:,1:tempintd), duF(1:tempintd,:,:), d2uF(1:tempintd,:,:), j_lev, ng, meth_forward, 10, tempintd, 1, tempintd)  !du(i,:,k)=dF_ij/dx_k
       CALL user_dist (ng, t, DISTANCE=dist_loc, NORMAL=obs_norm)   !distance function from main code
!
       DO i = 1,dim
          DO j = 1,tempintd
             du_upwind(j,:,i) = (1.0_pr+SIGN(1.0_pr,obs_norm(:,i)))/2.0_pr*duB(j,:,i) + (1.0_pr-SIGN(1.0_pr,obs_norm(:,i)))/2.0_pr*duF(j,:,i)
             d2u_upwind(j,:,i) = (1.0_pr+SIGN(1.0_pr,obs_norm(:,i)))/2.0_pr*d2uB(j,:,i) + (1.0_pr-SIGN(1.0_pr,obs_norm(:,i)))/2.0_pr*d2uF(j,:,i)
             du(j,:,i) = (du_upwind(j,:,i)  - du(j,:,i) ) * obs_norm(:,i)*MAX(dist_loc/upwind_norm/min_h + 1.0_pr ,0.0_pr) + du(j,:,i)
          END DO
       END DO
       
    END IF
    
    DO j = 1, dim
       jshift=ne*(j-1)
       DO i = 1, dim+1+Nspec
          shift=(i-1)*ng

          !IF (obstacle == .TRUE. .AND. (i == nden .OR. i == neng)) THEN  !add brinkman term to the continuity equation
             int_rhs(shift+1:shift+ng) = int_rhs(shift+1:shift+ng) - du(i+jshift,:,j)  ! -dF_ij/dx_j 
       END DO
       IF (GRAV) THEN      
          shift = (nvel(j)-1)*ng
          int_rhs(shift+1:shift+ng) = int_rhs(shift+1:shift+ng) + gr_in(j)*u_integrated(:,nden)
       END IF
    END DO
 
    
    IF (imask_obstacle) THEN
       !penalize energy for brinkman
       IF(brinkman_penal) THEN 
          int_rhs((nden-1)*ng+1:nden*ng) = int_rhs((nden-1)*ng+1:nden*ng)*(1.0_pr+ (1.0_pr/porosity-1.0_pr)*penal) !brinkman on energy
          DO i = 1,dim  !penalize momentum for both brinkman/vp
             int_rhs((nvel(i)-1)*ng+1:nvel(i)*ng) = int_rhs((nvel(i)-1)*ng+1:nvel(i)*ng) -penal*v(1:ng,i)/eta_b  !we need velocity
          END DO
          int_rhs((neng-1)*ng+1:neng*ng) = int_rhs((neng-1)*ng+1:neng*ng)*(1.0_pr+ (1.0_pr/porosity-1.0_pr)*penal) !brinkman on energy
       END IF
       !volume penalizations are done in separate subroutine
    END IF

  END SUBROUTINE internal_rhs

  SUBROUTINE internal_Drhs (int_Drhs, u_p, u_prev, meth,addingon)
    USE penalization
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: meth
    REAL (pr), DIMENSION (ng,ne), INTENT(IN) :: u_p, u_prev
    REAL (pr), DIMENSION (ng*ne), INTENT(INOUT) :: int_Drhs
    INTEGER, INTENT(IN) :: addingon 

    INTEGER :: i, j, l, idim, ie, shift,  vshift, jshift, tempintd
    REAL (pr), DIMENSION (ne*(dim+Nspec),ng,dim) :: du, d2u
    REAL (pr), DIMENSION (ne*(dim+Nspec),ng,dim) :: dudum, d2udum
    REAL (pr), DIMENSION (ne*(dim+Nspec),ng,dim) :: duC, d2uC,duF, d2uF,duB, d2uB,du_upwind, d2u_upwind    !forward and backwards differencing
    REAL (pr), DIMENSION (ng,ne*dim) :: F
    REAL (pr), DIMENSION (ng,(dim+Nspec)*2) :: v_prev ! velocity components + temperature
    REAL (pr), DIMENSION (ng) :: p, p_prev
    REAL (pr), DIMENSION (ng,3) :: props, props_prev

    REAL (pr) , DIMENSION(ng) :: dist_loc
    REAL (pr) , DIMENSION(ng,dim) :: obs_norm
    REAL (pr) :: min_h
    REAL (pr) , DIMENSION(ng) :: visc_loc
    INTEGER :: meth_central,meth_backward,meth_forward
    REAL (pr) :: diff_max,diff_min
    REAL (pr) , DIMENSION(2,ng,dim) :: bias_mask

    meth_central  = meth + BIASING_NONE
    meth_backward = meth + BIASING_BACKWARD
    meth_forward  = meth + BIASING_FORWARD

    min_h = MINVAL(h(1,1:dim)/2**(j_mx - 1))

    int_Drhs = 0.0_pr
    vshift = dim+Nspec
    DO l=1,Nspecm
       v_prev(:,dim+1+l) = u_prev(:,nspc(l))/u_prev(:,nden)
       v_prev(:,vshift+dim+1+l) = u_p(:,nspc(l))/u_prev(:,nden) - u_prev(:,nspc(l))*u_p(:,nden)/u_prev(:,nden)**2
    END DO
    p_prev(:) = 1.0_pr
    p(:) = 0.0_pr
    IF (Nspec>1) p_prev(:) = (p_prev(:)-SUM(v_prev(:,dim+2:dim+Nspec),DIM=2))  !Y_Nspec
    IF (Nspec>1) p(:)      =           -SUM(v_prev(:,vshift+dim+2:vshift+dim+Nspec),DIM=2)  !Y'_Nspec
    props_prev(:,1) = p_prev(:)*cv_in(Nspec) !cv_Nspec
    props_prev(:,2) = p_prev(:)*Ma_in(Nspec)**2 !(Ma_Nspec)^2
    props_prev(:,3) = p_prev(:)*rho_in(Nspec) !rho_m
    props(:,1)      = p(:)*cv_in(Nspec) !cv'_Nspec
    props(:,2)      = p(:)*Ma_in(Nspec)**2 !(Ma_Nspec)^2
    props(:,3)      = p(:)*rho_in(Nspec) !rho_m'
    DO l=1,Nspecm
       props_prev(:,1) = props_prev(:,1) + v_prev(:,dim+1+l)*cv_in(l) !cv
       props_prev(:,2) = props_prev(:,2) + v_prev(:,dim+1+l)*Ma_in(l)**2 !Ma^2
       props_prev(:,3) = props_prev(:,3) + v_prev(:,dim+1+l)*rho_in(l) !rho_ref
       props(:,1)      = props(:,1)      + v_prev(:,vshift+dim+1+l)*cv_in(l) !cv
       props(:,2)      = props(:,2)      + v_prev(:,vshift+dim+1+l)*Ma_in(l)**2 !Ma^2
       props(:,3)      = props(:,3)      + v_prev(:,vshift+dim+1+l)*rho_in(l) !rho_ref'
    END DO
    p_prev(:) = p_ref + (u_prev(:,nden) - 1.0_pr )/props_prev(:,2)
    p(:) = u_p(:,nden)/props_prev(:,2) - (u_prev(:,nden) - 1.0_pr )/props_prev(:,2)**2*props(:,2)

    v_prev(:,dim+1) = (u_prev(:,neng)-0.5_pr*SUM(u_prev(:,nvel(1):nvel(dim))**2,DIM=2)/u_prev(:,nden))/props_prev(:,1)/u_prev(:,nden) !Temperature
    v_prev(:,vshift+dim+1) = -v_prev(:,dim+1)/props_prev(:,1)*props(:,1) &
         + ( u_p(:,neng) - (u_prev(:,neng)-SUM(u_prev(:,nvel(1):nvel(dim))**2,DIM=2)/u_prev(:,nden))*u_p(:,nden)/u_prev(:,nden) &
            - SUM(u_p(:,nvel(1):nvel(dim))*u_prev(:,nvel(1):nvel(dim)),DIM=2)/u_prev(:,nden) )/u_prev(:,nden)/props_prev(:,1) !Temperature'
    
    DO i = 1, dim
       v_prev(:,i) = u_prev(:,nvel(i)) / u_prev(:,nden) ! u_i
       v_prev(:,vshift+i) = u_p(:,nvel(i))/u_prev(:,nden) - u_prev(:,nvel(i))*u_p(:,nden)/u_prev(:,nden)**2  !u_i
    END DO
 
    !
    !--Form right hand side of Euler Equations
    !
    F(:,:) = 0.0_pr
    DO j = 1, dim
       jshift=ne*(j-1)
       F(:,nden+jshift) = F(:,nden+jshift) + u_p(:,nvel(j)) ! rho*u_j
       DO i = 1, dim ! (rho*u_i*u_j)'
          F(:,nvel(i)+jshift) = F(:,nvel(i)+jshift) + u_prev(:,nvel(i))*v_prev(:,vshift+j) + u_p(:,nvel(i))*v_prev(:,j)
       END DO
       F(:,nvel(j)+jshift) = F(:,nvel(j)+jshift) + p(:) ! (rho*u_i^2)'+p'
       F(:,neng+jshift) = F(:,neng+jshift) + (u_p(:,neng) + p(:))*v_prev(:,j) + (u_prev(:,neng) + p_prev(:))*v_prev(:,vshift+j)  ! ((e+p)*u_j)'
       DO l=1,Nspecm
          F(:,nspc(l)+jshift) = F(:,nspc(l)+jshift) + v_prev(:,vshift+dim+1+l)*u_prev(:,nvel(j))+v_prev(:,dim+1+l)*u_p(:,nvel(j))
       END DO
    END DO

    !
    !-- Form right hand side of Navier-Stokes Equations
    !
    IF(NS) THEN 
       tempintd = 2*vshift

       CALL c_diff_fast(v_prev(:,:), du(1:tempintd,:,:), d2u(1:tempintd,:,:), j_lev, ng, meth_central, 10, tempintd, 1, tempintd) ! du'_i/dx_j & dT'/dx_j

       IF (upwind_norm > 0.0_pr) THEN  !do upwinding near the edges of the obstacle
          CALL c_diff_fast(v_prev(:,:), duB(1:tempintd,:,:), d2uB(1:tempintd,:,:), j_lev, ng, meth_backward, 10, tempintd, 1, tempintd) ! du'_i/dx_j & dT'/dx_j
          CALL c_diff_fast(v_prev(:,:), duF(1:tempintd,:,:), d2uF(1:tempintd,:,:), j_lev, ng, meth_forward, 10, tempintd, 1, tempintd) ! du'_i/dx_j & dT'/dx_j
          
          CALL user_dist (ng, t, DISTANCE=dist_loc, NORMAL=obs_norm)   !distance function from main code
          
          DO i = 1,dim
             DO j = 1,tempintd
                du_upwind(j,:,i) = (1.0_pr+SIGN(1.0_pr,obs_norm(:,i)))/2.0_pr*duB(j,:,i) + (1.0_pr-SIGN(1.0_pr,obs_norm(:,i)))/2.0_pr*duF(j,:,i)
                d2u_upwind(j,:,i) = (1.0_pr+SIGN(1.0_pr,obs_norm(:,i)))/2.0_pr*d2uB(j,:,i) + (1.0_pr-SIGN(1.0_pr,obs_norm(:,i)))/2.0_pr*d2uF(j,:,i)
                du(j,:,i) = (du_upwind(j,:,i)  - du(j,:,i) )*obs_norm(:,i)*MAX(dist_loc/upwind_norm/min_h + 1.0_pr ,0.0_pr) + du(j,:,i)
             END DO
          END DO
          
       END IF
    
       p_prev(:) = 1.0_pr
       p(:) = 0.0_pr
       IF (Nspec>1) p_prev(:) = (p_prev(:)-SUM(v_prev(:,dim+2:dim+Nspec),DIM=2))  !Y0_Nspec
       IF (Nspec>1) p(:)      =           -SUM(v_prev(:,vshift+dim+2:vshift+dim+Nspec),DIM=2)  !Y'_Nspec
       props_prev(:,1) = p_prev(:)*mu_in(Nspec)  !mu0_Nspec
       props_prev(:,2) = p_prev(:)*kk_in(Nspec)  !kk0_Nspec
       props_prev(:,3) = p_prev(:)*bD_in(Nspec)  !bD0_Nspec
       props(:,1)      = p(:)*mu_in(Nspec)  !mu'_Nspec
       props(:,2)      = p(:)*kk_in(Nspec)  !kk'_Nspec
       props(:,3)      = p(:)*bD_in(Nspec)  !bD'_Nspec
       DO l=1,Nspecm
          props_prev(:,1) = props_prev(:,1) + v_prev(:,dim+1+l)*mu_in(l)  !mu0
          props_prev(:,2) = props_prev(:,2) + v_prev(:,dim+1+l)*kk_in(l)   !kk0
          props_prev(:,3) = props_prev(:,3) + v_prev(:,dim+1+l)*bD_in(l)  !bD0
          props(:,1)      = props(:,1)      + v_prev(:,vshift+dim+1+l)*mu_in(l)  !mu'
          props(:,2)      = props(:,2)      + v_prev(:,vshift+dim+1+l)*kk_in(l)  !kk'
          props(:,3)      = props(:,3)      + v_prev(:,vshift+dim+1+l)*bD_in(l)  !bD'
       END DO
       p(:) = du(vshift+1,:,1) !reuse of a variable
       DO i = 2, dim
          p(:) = p(:) + du(vshift+i,:,i) !du'_i/dx_i
       END DO

          d2u(:,:,:) = 1.0_pr
       DO j = 1, dim
          jshift=ne*(j-1)
          DO i = 1, dim
             F(:,nvel(i)+jshift) = F(:,nvel(i)+jshift) - props_prev(:,1)*( du(vshift+i,:,j)+du(vshift+j,:,i) ) * d2u(i,:,j)  !rho*u_i*u_j - 2*mu*S_ij/Re
          END DO
          F(:,nvel(j)+jshift) = F(:,nvel(j)+jshift) + 2.0_pr/3.0_pr*props_prev(:,1)*p(:) * d2u(j,:,j) !rho*u_i*u_j - tau_ij/Re  [ tau_ij = 2*mu*( S_ij - 2/3*S_kk*delta_ij ) ]
          DO i = 1, dim
             F(:,neng+jshift) =  F(:,neng+jshift) - props_prev(:,1)*( du(vshift+i,:,j)+du(vshift+j,:,i) )*v_prev(:,i) * d2u(i,:,j)  !(e+p)*u_j - u_i*2*mu*S_ij/Re
          END DO
          F(:,neng+jshift) =  F(:,neng+jshift) + 2.0_pr/3.0_pr*props_prev(:,1)*p(:)*v_prev(:,j) * d2u(j,:,j) - props_prev(:,2)*du(vshift+dim+1,:,j) * d2u(2,:,j)  
                                                                                             !(e+p)*u_j - u_i*tau_ij/Re - q_j/Re/Pra [ q_j = mu dT/dx_j ]
          DO l=1,Nspecm
             F(:,neng+jshift) =  F(:,neng+jshift) - u_prev(:,nden)*cv_in(l)*v_prev(:,dim+1)*props_prev(:,3)*du(vshift+dim+1+l,:,j) * d2u(2,:,j)
          END DO
          IF (Nspec>1) F(:,neng+jshift) =  F(:,neng+jshift) +  u_prev(:,nden)*cv_in(Nspec)*v_prev(:,dim+1)*props_prev(:,3)*SUM(du(vshift+dim+2:vshift+dim+Nspec,:,j),DIM=1) * d2u(2,:,j)
          DO l=1,Nspecm
             F(:,nspc(l)+jshift) =  F(:,nspc(l)+jshift) - u_prev(:,nden)*props_prev(:,3)*du(vshift+dim+1+l,:,j) * d2u(2,:,j)
          END DO
       END DO

       p_prev(:) = du(1,:,1) !reuse of a variable
       DO i = 2, dim
          p_prev(:) = p_prev(:) + du(i,:,i) !du0_i/dx_i
       END DO
       DO j = 1, dim
          jshift=ne*(j-1)
          DO i = 1, dim
             F(:,nvel(i)+jshift) = F(:,nvel(i)+jshift) - props(:,1)*( du(i,:,j)+du(j,:,i) ) * d2u(i,:,j)  !rho*u_i*u_j - 2*mu*S_ij/Re
          END DO
          F(:,nvel(j)+jshift) = F(:,nvel(j)+jshift) + 2.0_pr/3.0_pr*props(:,1)*p_prev(:) * d2u(j,:,j) !rho*u_i*u_j - tau_ij/Re  [ tau_ij = 2*mu*( S_ij - 2/3*S_kk*delta_ij ) ]
          DO i = 1, dim
             F(:,neng+jshift) =  F(:,neng+jshift) - ( du(i,:,j)+du(j,:,i) )*(v_prev(:,i)*props(:,1)+v_prev(:,vshift+i)*props_prev(:,1)) * d2u(i,:,j)  !(e+p)*u_j - u_i*2*mu*S_ij/Re
          END DO
          F(:,neng+jshift) =  F(:,neng+jshift) + 2.0_pr/3.0_pr*p_prev(:)*(v_prev(:,j)*props(:,1)+v_prev(:,vshift+j)*props_prev(:,1)) * d2u(j,:,j) - props(:,2)*du(dim+1,:,j) * d2u(2,:,j)  
                                                                                             !(e+p)*u_j - u_i*tau_ij/Re - q_j/Re/Pra [ q_j = mu dT/dx_j ]
          DO l=1,Nspecm
             F(:,neng+jshift) =  F(:,neng+jshift) - cv_in(l)*du(dim+1+l,:,j) * &
                             ( u_p(:,nden)*v_prev(:,dim+1)*props_prev(:,3) + u_prev(:,nden)*(v_prev(:,vshift+dim+1)*props_prev(:,3) + v_prev(:,dim+1)*props(:,3)) ) * d2u(2,:,j)
          END DO
          IF (Nspec>1) F(:,neng+jshift) =  F(:,neng+jshift) +  cv_in(Nspec)*SUM(du(dim+2:dim+Nspec,:,j),DIM=1) * &
                             ( u_p(:,nden)*v_prev(:,dim+1)*props_prev(:,3) + u_prev(:,nden)*(v_prev(:,vshift+dim+1)*props_prev(:,3) + v_prev(:,dim+1)*props(:,3)) ) * d2u(2,:,j)
          DO l=1,Nspecm
             F(:,nspc(l)+jshift) =  F(:,nspc(l)+jshift) - (u_p(:,nden)*props_prev(:,3)+u_prev(:,nden)*props(:,3))*du(dim+1+l,:,j) * d2u(2,:,j)
          END DO
       END DO
    END IF
    IF (GRAV) THEN
      shift = (neng-1)*ng
      DO j=1,dim
          int_Drhs(shift+1:shift+ng) = int_Drhs(shift+1:shift+ng) + u_p(:,nvel(j))*gr_in(j)
      END DO
   END IF
    tempintd=ne*dim

    CALL c_diff_fast(F(:,1:tempintd), du(1:tempintd,:,:), d2u(1:tempintd,:,:), j_lev, ng, meth_central, 10, tempintd, 1, tempintd)  !du(i,:,k)=dF_ij/dx_k

    IF (upwind_norm > 0.0_pr) THEN  !do upwinding near the edges of the obstacle
       CALL c_diff_fast(F(:,1:tempintd), duB(1:tempintd,:,:), d2uB(1:tempintd,:,:), j_lev, ng, meth_backward, 10, tempintd, 1, tempintd)  !du(i,:,k)=dF_ij/dx_k
       CALL c_diff_fast(F(:,1:tempintd), duF(1:tempintd,:,:), d2uF(1:tempintd,:,:), j_lev, ng, meth_forward, 10, tempintd, 1, tempintd)  !du(i,:,k)=dF_ij/dx_k
       CALL user_dist (ng, t, DISTANCE=dist_loc, NORMAL=obs_norm)   !distance function from main code

       DO i = 1,dim
          DO j = 1,tempintd
             du_upwind(j,:,i) = (1.0_pr+SIGN(1.0_pr,obs_norm(:,i)))/2.0_pr*duB(j,:,i) + (1.0_pr-SIGN(1.0_pr,obs_norm(:,i)))/2.0_pr*duF(j,:,i)
             d2u_upwind(j,:,i) = (1.0_pr+SIGN(1.0_pr,obs_norm(:,i)))/2.0_pr*d2uB(j,:,i) + (1.0_pr-SIGN(1.0_pr,obs_norm(:,i)))/2.0_pr*d2uF(j,:,i)
             du(j,:,i) = (du_upwind(j,:,i)  - du(j,:,i) )*obs_norm(:,i)*MAX(dist_loc/upwind_norm/min_h + 1.0_pr ,0.0_pr) + du(j,:,i)
          END DO
       END DO
       
    END IF    

    DO j = 1, dim
       jshift=ne*(j-1)
       DO i = 1, dim+1+Nspec
          shift=(i-1)*ng
             int_Drhs(shift+1:shift+ng) = int_Drhs(shift+1:shift+ng) - du(i+jshift,:,j)  ! -dF_ij/dx_j  !continutiy set up
       END DO
       IF (GRAV) THEN      
          shift = (nvel(j)-1)*ng
          int_Drhs(shift+1:shift+ng) = int_Drhs(shift+1:shift+ng) + gr_in(j)*u_p(:,nden)
       END IF
    END DO
    

    !Add Brinkman Penalization Terms

    IF (imask_obstacle == .TRUE.) THEN
       IF(brinkman_penal) THEN
          int_Drhs((nden-1)*ng+1:nden*ng) = int_Drhs((nden-1)*ng+1:nden*ng) *(1.0_pr+ (1.0_pr/porosity-1.0_pr)*penal) 
          DO i = 1,dim
             !internal rhs, not user_Drhs
             int_Drhs((nvel(i)-1)*ng+1:nvel(i)*ng) = int_Drhs((nvel(i)-1)*ng+1:nvel(i)*ng) -penal/eta_b*v_prev(:,vshift+i)
          END DO
          int_Drhs((neng-1)*ng+1:neng*ng) = int_Drhs((neng-1)*ng+1:neng*ng) *(1.0_pr+ (1.0_pr/porosity-1.0_pr)*penal) !/porosity*(TANH(20.0_pr*((x(:,1)**2.0_pr+x(:,2)**2.0_pr)**0.5_pr - 0.25_pr)) + 1.0_pr)*SUM(du(neng,:,1:2)*obs_norm(:,1:2),DIM=2)!check if temperature in subroutine is formed same as TT in brinkman
       END IF
    END IF

  END SUBROUTINE internal_Drhs

  SUBROUTINE internal_Drhs_diag (int_diag,u_prev,meth,addingon)
    USE penalization
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: meth
    REAL (pr), DIMENSION (ng*ne), INTENT(INOUT) :: int_diag
    REAL (pr), DIMENSION (ng,ne), INTENT(IN) :: u_prev
    INTEGER, INTENT(IN) :: addingon 

    INTEGER :: i, j, l, ll, ie, shift
    REAL (pr), DIMENSION (ng,dim) :: du_diag, d2u_diag, du_diag_F, d2u_diag_F, du_diag_B, d2u_diag_B
    REAL (pr), DIMENSION (MAX(Nspec,dim),ng,dim) :: du, d2u, duB, d2uB, duF, d2uF, du_upwind, d2u_upwind
    REAL (pr), DIMENSION (ng,n_integrated,dim) :: F
    REAL (pr), DIMENSION (ng, Nspec) :: v_prev
    REAL (pr), DIMENSION (ng, dim+1) :: T_prev  !temperature and velocity
    REAL (pr), DIMENSION (ng, dim+1) :: T_p_diag    !momentum and energy
    REAL (pr), DIMENSION (ng, dim+2) :: props_diag  !momentum and energy(x2)
    REAL (pr), DIMENSION (ng,6) :: props_prev
    REAL (pr), DIMENSION (ng) :: p_prev

    REAL (pr) , DIMENSION(ng) :: dist_loc
    REAL (pr) , DIMENSION(ng,dim) :: obs_norm
    REAL (pr) :: min_h
    INTEGER :: meth_central,meth_backward,meth_forward

    meth_central  = meth + BIASING_NONE
    meth_backward = meth + BIASING_BACKWARD
    meth_forward  = meth + BIASING_FORWARD

    min_h = MINVAL(h(1,1:dim)/2**(j_mx - 1))

    int_diag = 0.0_pr
    DO l=1,Nspecm
       v_prev(:,l) = u_prev(:,nspc(l))/u_prev(:,nden)  ! Y0
    END DO
    v_prev(:,Nspec)=1.0_pr
    IF (Nspec>1) v_prev(:,Nspec) = 1.0_pr-SUM(v_prev(:,1:Nspecm),DIM=2)  !Y0_Nspec
    props_prev(:,:)=0.0_pr
    DO l=1,Nspec
       props_prev(:,1) = props_prev(:,1) + v_prev(:,l)*cv_in(l)  !cp0
       props_prev(:,2) = props_prev(:,2) + v_prev(:,l)*Ma_in(l)**2  !Ma^2
       props_prev(:,4) = props_prev(:,1) + v_prev(:,l)*mu_in(l)  !mu0  ERIC: not set up for dynamic viscosity, not modularized
       props_prev(:,5) = props_prev(:,2) + v_prev(:,l)*kk_in(l)  !kk0
       props_prev(:,6) = props_prev(:,3) + v_prev(:,l)*bD_in(l)  !bD0
    END DO
   
    CALL c_diff_diag(du_diag, d2u_diag, j_lev, ng, meth, meth, -11)
    !
    !--Form right hand side of Euler Equations
    !
    F(:,:,:) = 0.0_pr
    DO j = 1, dim
       DO i = 1, dim ! (rho*u_i*u_j)'
          F(:,nvel(i),j) = F(:,nvel(i),j) + u_prev(:,nvel(j))/u_prev(:,nden)
       END DO
       F(:,nvel(j),j) = F(:,nvel(j),j) 
       F(:,neng,j) = F(:,neng,j) + u_prev(:,nvel(j))/u_prev(:,nden)
       DO l=1,Nspecm
          F(:,nspc(l),j) = F(:,nspc(l),j) + u_prev(:,nvel(j))/u_prev(:,nden)
       END DO
    END DO

    !
    !-- Form right hand side of Navier-Stokes Equations
    !
    IF(NS .AND. NSdiag) THEN
       CALL c_diff_fast(v_prev, du(1:Nspec,:,:), d2u(1:Nspec,:,:), j_lev, ng, meth, 10, Nspec, 1, Nspec) ! dY_l/dx_j

          d2u(:,:,:) = 1.0_pr
       DO j = 1, dim
          DO l=1,Nspec
             IF (Nspec>1) F(:,neng,j) =  F(:,neng,j) - props_prev(:,6)*du(l,:,j)*cv_in(l)/(props_prev(:,1)-props_prev(:,2))*d2u(2,:,j)
          END DO 
       END DO
       DO j = 1, dim
          DO i = 1, dim
             shift=(nvel(i)-1)*ng
             int_diag(shift+1:shift+ng) = int_diag(shift+1:shift+ng) + props_prev(:,4)/u_prev(:,nden)*d2u_diag(:,j) * d2u(i,:,j)
          END DO
          shift=(nvel(j)-1)*ng
          int_diag(shift+1:shift+ng) = int_diag(shift+1:shift+ng) + props_prev(:,4)/u_prev(:,nden)*d2u_diag(:,j)/3.0_pr * d2u(j,:,j)
          shift=(neng-1)*ng
          int_diag(shift+1:shift+ng) = int_diag(shift+1:shift+ng) + props_prev(:,5)/u_prev(:,nden)/props_prev(:,1)*d2u_diag(:,j) * d2u(2,:,j)
          DO l = 1, Nspecm
             shift=(nspc(l)-1)*ng
             int_diag(shift+1:shift+ng) = int_diag(shift+1:shift+ng) +(props_prev(:,6)*d2u_diag(:,j)+bD_in(l)*du(l,:,j)) * d2u(2,:,j)
          END DO
       END DO
    END IF
    DO j = 1, dim
       DO i = 1, dim+1+Nspec
          shift=(i-1)*ng
             int_diag(shift+1:shift+ng) = int_diag(shift+1:shift+ng) - F(:,i,j)*du_diag(:,j)
       END DO
    END DO

    
    IF (imask_obstacle == .TRUE. .AND. brinkman_penal) THEN  !not sure if these are correct, use krylov
       int_diag((nden-1)*ng+1:nden*ng) = int_diag((nden-1)*ng+1:nden*ng)*(1.0_pr+ (1.0_pr/porosity-1.0_pr)*penal)
       DO i = 1,dim
          int_diag((nvel(i)-1)*ng+1:nvel(i)*ng) = int_diag((nvel(i)-1)*ng+1:nvel(i)*ng) -penal/eta_b/u_prev(1:ng,nden)
       END DO
       int_diag((neng-1)*ng+1:neng*ng) = int_diag((neng-1)*ng+1:neng*ng)*(1.0_pr+ (1.0_pr/porosity-1.0_pr)*penal)
    END IF
        
  END SUBROUTINE internal_Drhs_diag

  FUNCTION user_rhs (u_integrated,scalar)
    IMPLICIT NONE
    REAL (pr), DIMENSION (ng,ne), INTENT(IN) :: u_integrated !1D flat version of integrated variables without BC 
    REAL (pr), DIMENSION (ng), INTENT(IN) :: scalar
    REAL (pr), DIMENSION (ng*ne) :: user_rhs
    INTEGER, PARAMETER :: meth=1

    CALL internal_rhs(user_rhs,u_integrated,0)

    IF (cnvtBC) CALL user_convectzone_BC_rhs (user_rhs, u_integrated, ne, ng, j_lev, meth) !add artificial convection to bufferzone/obstacle
    IF (buffBC) CALL user_bufferzone_BC_rhs (user_rhs, u_integrated, ne, ng, j_lev, meth)
    !IF (imask_obstacle .AND. vp) CALL volume_penalization_rhs (user_rhs, u_integrated, ne, ng, j_lev, LOW_ORDER)
    !IF (imask_obstacle == .TRUE.) CALL Brinkman_rhs (user_rhs, u_integrated, .FALSE., HIGH_ORDER) !add penalization ter

    IF(hypermodel .NE. 0) CALL hyperbolic(u_integrated,ng,user_rhs)

  END FUNCTION user_rhs

  FUNCTION user_Drhs (u_p, u_prev, meth)
!!$    USE Brinkman
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: meth
    REAL (pr), DIMENSION (ng,ne) :: u_p, u_prev
    REAL (pr), DIMENSION (ng*ne) :: user_Drhs

       CALL internal_Drhs(user_Drhs,u_p,u_prev,meth,0)

    IF (cnvtBC) CALL user_convectzone_BC_Drhs (user_Drhs, u_p, u_prev, ne, ng, j_lev, meth) !add artificial convection to bufferzone/obstacle
    IF (buffBC) CALL user_bufferzone_BC_Drhs (user_Drhs, u_p, u_prev, ne, ng, j_lev, meth)
    !IF (imask_obstacle .AND. vp) CALL volume_penalization_Drhs (user_Drhs, u_p, u_prev, ne, ng, j_lev, LOW_ORDER)
    !IF (imask_obstacle == .TRUE.) CALL Brinkman_Drhs (user_Drhs, u_p, u_prev, HIGH_ORDER)  !add penalization terms


    IF (hypermodel .NE. 0 ) CALL hyperbolic(u_p,ng,user_Drhs)

  END FUNCTION user_Drhs

  FUNCTION user_Drhs_diag (meth)
!!$    USE Brinkman
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: meth
    REAL (pr), DIMENSION (ng*ne) :: user_Drhs_diag
    REAL (pr), DIMENSION (ng,ne) :: u_prev
    INTEGER :: ie, shift

    DO ie = 1, ne
       shift=(ie-1)*ng
       u_prev(1:ng,ie) = u_prev_timestep(shift+1:shift+ng)
    END DO

 
       CALL internal_Drhs_diag(user_Drhs_diag,u_prev,meth,0)

    IF (cnvtBC) CALL user_convectzone_BC_Drhs_diag (user_Drhs_diag, u_prev, ne, ng, j_lev, meth)
    IF (buffBC) CALL user_bufferzone_BC_Drhs_diag (user_Drhs_diag, u_prev, ne, ng, j_lev, meth)
    !IF (imask_obstacle .AND. vp) CALL volume_penalization_Drhs_diag (user_Drhs_diag, u_prev, ne, ng, j_lev, LOW_ORDER)
    !IF (imask_obstacle == .TRUE.) CALL Brinkman_Drhs_diag (user_Drhs_diag, meth)  !add penalization terms

    IF(hypermodel .NE. 0 ) CALL hyperbolic_diag(user_Drhs_diag, ng)


  END FUNCTION user_Drhs_diag



  SUBROUTINE user_project (u, p, nlocal, meth)
    IMPLICIT NONE

    INTEGER, INTENT (IN) :: meth, nlocal
    REAL (pr), DIMENSION (nlocal),     INTENT(INOUT) :: p
    REAL (pr), DIMENSION (nlocal,n_integrated), INTENT(INOUT) :: u

    INTEGER :: i
    INTEGER, PARAMETER :: ne = 1
    INTEGER, DIMENSION(ne) :: clip 
    !no projection for compressible flow
  END SUBROUTINE user_project

  FUNCTION Laplace (jlev, u, nlocal, ne_local, meth)
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: jlev, nlocal, ne_local, meth
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (INOUT) :: u
    REAL (pr), DIMENSION (nlocal*ne_local) :: Laplace

    INTEGER :: i, ii, ie, shift, meth1, meth2, idim
    REAL (pr), DIMENSION (ne_local ,nlocal,dim) :: du
    REAL (pr), DIMENSION (ne_local*dim ,nlocal,dim) :: d2u
    REAL (pr), DIMENSION (ne_local*dim ,nlocal,dim) :: du_dummy ! passed when only calculating 1st derivative.
    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc
    
    Laplace=0.0_pr
  END FUNCTION Laplace

  FUNCTION Laplace_diag (jlev,  nlocal, ne_local, meth)
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: jlev, nlocal, ne_local, meth
    REAL (pr), DIMENSION (nlocal*ne_local) :: Laplace_diag

    INTEGER :: i, ii, ie, shift, meth1, meth2
    REAL (pr), DIMENSION (nlocal,dim) :: du, d2u
    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc

    Laplace_diag=0.0_pr
  END FUNCTION Laplace_diag

  FUNCTION Laplace_rhs(u, nlocal, ne_local, meth)
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: nlocal, ne_local, meth
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (INOUT) :: u
    REAL (pr), DIMENSION (nlocal) :: Laplace_rhs

    INTEGER :: i, ii, ie, meth1, meth2, idim
    REAL (pr), DIMENSION (ne_local ,nlocal,dim) :: du
    REAL (pr), DIMENSION (ne_local*dim ,nlocal,dim) :: d2u
    REAL (pr), DIMENSION (ne_local*dim ,nlocal,dim) :: du_dummy ! passed when only calculating 1st derivative.
    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc

    Laplace_rhs = 0.0_pr 
  END FUNCTION Laplace_rhs

  !
  ! Calculate any statitics
  !
  ! startup_flag - 0 when adapting to IC,then 1 inmain integration loop
  !
  SUBROUTINE user_stats ( u ,j_mn, startup_flag)
    USE parallel
    USE penalization
    IMPLICIT NONE
    REAL (pr), DIMENSION (nwlt,1:n_var), INTENT (IN) :: u
    INTEGER , INTENT (IN) :: j_mn 
    INTEGER , INTENT (IN) :: startup_flag

    INTEGER :: i,n

    REAL(pr) :: num_nodes_added

    !surface integration variables
    INTEGER, DIMENSION(1) :: int_var
    INTEGER, DIMENSION(dim,surf_pts) :: nearest_ixyz
    INTEGER, DIMENSION(surf_pts) :: cproc,i_x_interpolate
    

    REAL(pr), DIMENSION(dim,surf_pts) :: x_interpolate, x_interp_local
    REAL(pr), DIMENSION(1,surf_pts) :: u_interpolate,u_interp_local  !interpolated values
    REAL(pr) :: integral
    REAL(pr), DIMENSION(n_var,nwlt,dim) :: du,temp
    REAL(pr), DIMENSION(dim,nwlt) :: h_arr
    REAL(pr) :: hhx, hhy, area_reconstruction,pi,circ_recon
    REAL(pr), DIMENSION(2) :: F_surf, F_chi

    CHARACTER (LEN=4) :: string,string2
    CHARACTER (LEN=60) :: sformat

    !
    ! There is no obstacle in flow
    !
  END SUBROUTINE user_stats

  SUBROUTINE user_cal_force (u, n, t_local, force, drag, lift)
    !--Calculates drag and lift on obstacle using penalization formula
    IMPLICIT NONE

    INTEGER, INTENT (IN) :: n
    REAL (pr), INTENT (IN) :: t_local
    REAL (pr), DIMENSION (dim), INTENT (INOUT) :: force
    REAL (pr), INTENT (OUT) :: drag, lift
    REAL (pr), DIMENSION (n,dim) :: u
    drag = 0.0_PR
    lift = 0.0_PR
    !
    ! There is no obstacle in flow
    !

  END SUBROUTINE user_cal_force

  !
  ! Read input from "case_name"_pde.inp file
  ! case_name is in string file_name read from command line
  ! in read_command_line_input()
  ! 
  SUBROUTINE user_read_input()
    IMPLICIT NONE
    INTEGER :: i, j, ind
    CHARACTER(LEN=8):: numstrng, procnum
    INTEGER :: myiter
    LOGICAL :: checkit 
    REAL (pr) :: halfdomsize

    call input_real ('Re',Re,'stop',' Re: Reynolds Number')
    call input_real ('Pra',Pra,'stop',' Pra: Prandtl Number')
    call input_real ('Sc',Sc,'stop',' Sc: Schmidt Number')
    call input_real ('Fr',Fr,'stop',' Fr: Froude Number')
    call input_integer ('Nspec',Nspec,'stop','  Nspec: Number of species')
    call input_real ('p_ref',p_ref,'stop',' p_ref: reference nondimensional presure')
    call input_real ('T_w',T_w,'stop',' T_w: wall temperature')

    Nspec=MAX(Nspec,1)
    ALLOCATE(gr_in(1:dim))  
    ALLOCATE(bD_in(1:Nspec))
    ALLOCATE(mu_in(1:Nspec))
    ALLOCATE(kk_in(1:Nspec))
    ALLOCATE(Cv_in(1:Nspec))
    ALLOCATE(Ma_in(1:Nspec))
    ALLOCATE(rho_in(1:Nspec))

    call input_real_vector ('Cv',Cv_in(1:Nspec),Nspec,'stop',' Cv')
    call input_real_vector ('Ma',Ma_in(1:Nspec),Nspec,'stop',' Ma')
    call input_real_vector ('rho',rho_in(1:Nspec),Nspec,'stop',' rho')

    call input_real ('buffU0',buffU0,'stop', 'buffU0: scale for convect BC')
    call input_real ('buffSig',buffSig,'stop', 'buffSig: scale for buff BC')
    call input_real ('Lczn',Lczn,'stop', 'Lcoordzn: length of coord_zone on one side')
    call input_real ('tfacczn',tfacczn,'stop', 'tfacczn: factor multiply by time for coord_zone time=L/min(c)*tfac, set <0 for no czn modification')
    call input_real ('itfacczn',itfacczn,'stop', 'itfacczn: factor multiply by initial time for coord_zone itime=Lczn/min(c)*tfac, set <0 to set itime=0')
    call input_real_vector ('buffcvd',buffcvd(1:5),5,'stop', 'buffcvd: convect zone distances')
    call input_real_vector ('buffcvf',buffcvf(1:5),5,'stop', 'buffcvf: convect zone distance fractions')
    call input_real_vector ('buffbfd',buffbfd(1:5),5,'stop', 'buffbfd: buffer zone distances')
    call input_real_vector ('buffbff',buffbff(1:5),5,'stop', 'buffbff: buffer zone distance fractions')

    call input_real ('buff_thick',buff_thick,'stop',' buff_thick: thickness of freund buffer region')


    call input_real ('tempscl',tempscl,'stop', 'tempscl: scale for temperature, set < 0 for default')
    call input_real ('specscl',specscl,'stop', 'specscl: scale for species, set < 0 for default')
    call input_real ('mydt_orig',mydt_orig,'stop', 'mydt_orig: max dt for time integration... dt is only initial dt, set <0 to use dt')

    call input_logical ('GRAV',GRAV,'stop', 'GRAV: T to include body force terms, F to ingore them')
    call input_logical ('NS',NS,'stop', 'NS: T to include viscous terms (full Navier-Stokes), F for Euler Eqs')

    call input_logical ('buffBC',buffBC,'stop', 'buffBC: T to use buff zone damping BCs (Brinkman), F to use algebraic BCs')
    call input_logical ('polybuff',polybuff,'stop', 'polybuff: T to use polynomials for buff zone BCs,  F to use tanh')
    call input_logical ('cnvtBC',cnvtBC,'stop', 'cnvtBC: T to use convection zone BCs (Freund), F to use algebraic BCs')
    call input_logical ('polyconv',polyconv,'stop', 'polyconv: T to use polynomials for conv zone BCs,  F to use tanh')
    call input_logical ('pBrink',pBrink,'stop', 'pBrink: T to do Brinkman buffer only on pressure')
    call input_logical ('adapttmp',adapttmp,'stop', 'adapttmp: T to adapt on temperature instead of density and pressure')
    call input_logical ('adaptvelonly',adaptvelonly,'stop', 'adaptvelonly: T to adapt on velocity instead of momentum')
    call input_logical ('adaptspconly',adaptspconly,'stop', 'adaptspconly: T to adapt on mass fraction (Y) instead of volume fraction (rhoY)')
    call input_logical ('adaptden',adaptden,'stop', 'adapt on density')
    call input_logical ('adaptvel',adaptvel,'stop', 'adapt on momentum')
    call input_logical ('adapteng',adapteng,'stop', 'adapt on energy')
    call input_logical ('adaptspc',adaptspc,'stop', 'adapt on species')
    call input_logical ('adaptprs',adaptprs,'stop', 'adapt on pressure')

    call input_logical ('savepardom',savepardom,'stop', 'savepardom: T to save parallel domain as additional var')
    call input_logical ('convcfl',convcfl,'stop', 'convcfl: T to use convective cfl, F to use acoustic cfl')

    !4extra - these 8 input definitions - MUST ALSO ADD TO .inp 
    call input_logical ('adaptMagVort',adaptMagVort,'stop','adaptMagVort: Adapt on the magnitude of vorticity')
    call input_logical ('adaptComVort',adaptComVort,'stop','adaptComVort: Adapt on the components of vorticity')
    call input_logical ('adaptMagVel',adaptMagVel,'stop','adaptMagVel: Adapt on the magnitude of velocity')
    call input_logical ('adaptNormS',adaptNormS,'stop', 'adaptNormS: Adapt on the L2 of Sij')
    call input_logical ('adaptGradY',adaptGradY,'stop', 'adaptGradY: Adapt on |dY/dx_i*dY/dx_i|')
    call input_logical ('saveMagVort',saveMagVort,'stop', 'saveMagVort: if T adds and saves magnitude of vorticity')
    call input_logical ('saveComVort',saveComVort,'stop', 'saveComVort: if T adds and saves components of vorticity')
    call input_logical ('saveMagVel',saveMagVel,'stop','saveMagVel: if T adds and saves magnitude of velocity')
    call input_logical ('saveNormS',saveNormS,'stop', 'saveNormS: if T adds and saves magnitude of strain rate')
    call input_logical ('saveUcvt',saveUcvt,'stop', 'saveUcvt: save U and damping support for Freund NRBCs to debug')
   
    IF (imask_obstacle) THEN
       !Type of obstacle
       call input_logical ('brinkman_penal',brinkman_penal,'stop','Brinkman penalization to be used')
       call input_logical ('vp',vp,'stop','volume penalization to be used')
       
       smooth_dist = .FALSE. ! default value
       call input_logical ('smooth_dist',smooth_dist,'default','T smoothes distance function prior finding the normal')    
       deltadel_loc = 1.0_pr
       call input_real ('deltadel_loc',deltadel_loc,'default', 'sets final diffusion thickness using spatial resolution (dx, dy, or dz), number of points across final thickness')


       !brinkman parameters
       call input_real ('eta_b',eta_b,'stop',' penalization parameter') !also for volume penalization
       call input_real ('eta_c',eta_c,'stop',' penalization parameter for adiabatic conditions') !also for volume penalization
       
       call input_real ('porosity',porosity,'stop',' porosity for brinkman')

       !penalization control
        call input_logical ('doCURV',doCURV,'stop', 'If true use curvature adjustment')
        call input_real ('doNORM',doNORM,'stop', 'If true add terms with normal velocity')


       !eric's support parameters
       call input_integer ('delta_pts',delta_pts,'stop', 'Number of points to transition from convection to diffusion')
       call input_integer ('IB_pts',IB_pts,'stop', 'Number of points (distance) for convection inside of the immersed boundary')
       !Nurlybek's support parameters
       call input_real ('delta_conv',delta_conv,'stop', 'transition thickness of the convective penalization zone')
       call input_real ('delta_diff',delta_diff,'stop', 'transition thickness of the diffusion penalization zone')
       call input_real ('shift_conv',shift_conv,'stop', 'thickness when convections is off')
       call input_real ('shift_diff',shift_diff,'stop', 'thickness of the diffusion is on')

       call input_real ('upwind_norm',upwind_norm,'stop', 'Normal-averaged upwind differencing thickness outside of interface.  < 0.0 to turn off')

       
       
       call input_real ('visc_factor_n',visc_factor_n,'stop','visc_factor_n: multiplier on numerical viscosity for penalization stability on neumann terms')
       call input_real ('visc_factor_d',visc_factor_d,'stop','visc_factor_d: multiplier on numerical viscosity for penalization stability on dirichlet terms')
    END IF
 
    IF (mydt_orig < 0.0_pr) mydt_orig = dt
    IF (IC_restart_mode==1) dt_original = mydt_orig

!!$    IF (buffNe) imask_obstacle=.TRUE.


    IF (Nspec<2) adaptspc=.FALSE. 
    IF (Nspec<2) adaptspconly=.FALSE. 
    Nspecm = Nspec - 1

    gr_in(:) = 0.0_pr;  gr_in(2) = -1.0_pr; gr_in = gr_in/Fr
    mu_in(:) = 1.0_pr/Re
    kk_in(:) = 1.0_pr/Re/Pra
    bD_in(:) = 1.0_pr/Re/Sc

     IF (NOT(ALLOCATED(Lxyz))) ALLOCATE(Lxyz(1:dim))
     IF (NOT(ALLOCATED(Y0top))) ALLOCATE(Y0top(1:Nspec))
     IF (NOT(ALLOCATED(Y0bot))) ALLOCATE(Y0bot(1:Nspec))
     IF (NOT(ALLOCATED(rhoYtopBC))) ALLOCATE(rhoYtopBC(1:Nspecm))
     IF (NOT(ALLOCATED(rhoYbotBC))) ALLOCATE(rhoYbotBC(1:Nspecm))
     IF (NOT(ALLOCATED(rhoYtop))) ALLOCATE(rhoYtop(1:Nspecm))
     IF (NOT(ALLOCATED(rhoYbot))) ALLOCATE(rhoYbot(1:Nspecm))

     !pi=4.0_pr*ATAN(1.0_pr)
     Lxyz(1:dim)=xyzlimits(2,1:dim)-xyzlimits(1,1:dim)
     halfdomsize = (xyzlimits(2,1)-xyzlimits(1,1))/2.0_pr
   
    IF (buffcvd(1)<0.0_pr) THEN
       buffcvd(1)=halfdomsize*buffcvf(1)
    ELSE
       buffcvf(1)=buffcvd(1)/halfdomsize
    END IF
    IF (ANY(buffcvd(2:4)<0.0_pr)) THEN
       buffcvf(2)=MAX(0.0_pr,MIN(1.0_pr,buffcvf(2)))
       buffcvf(3)=MAX(0.0_pr,MIN(1.0_pr-buffcvf(2),buffcvf(3)))
       buffcvf(4)=MAX(0.0_pr,MIN(1.0_pr-SUM(buffcvf(2:3)),buffcvf(4)))
       buffcvf(5)=MAX(0.0_pr,    1.0_pr-SUM(buffcvf(2:4)))
       buffcvd(2:5)=buffcvf(2:5)*buffcvd(1)
    ELSE
       buffcvd(2)=MIN(buffcvd(2),buffcvd(1))
       buffcvd(3)=MIN(buffcvd(3),buffcvd(1)-buffcvd(2))
       buffcvd(4)=MIN(buffcvd(4),buffcvd(1)-SUM(buffcvd(2:3)))
       buffcvd(5)=MAX(0.0_pr  ,buffcvd(1)-SUM(buffcvd(2:4)))
       buffcvf(2:5)=buffcvd(2:5)/buffcvd(1)
    END IF
    IF (verb_level>0 .AND. par_rank==0) PRINT *,'FOR bufferfunc, buffcvd=', buffcvd
    IF (verb_level>0 .AND. par_rank==0) PRINT *,'FOR bufferfunc, buffcvf=', buffcvf

    IF (buffbfd(1)<0.0_pr) THEN
       buffbfd(1)=halfdomsize*buffbff(1)
    ELSE
       buffbff(1)=buffbfd(1)/halfdomsize
    END IF
    IF (ANY(buffbfd(2:4)<0.0_pr)) THEN
       buffbff(2)=MAX(0.0_pr,MIN(1.0_pr,buffbff(2)))
       buffbff(3)=MAX(0.0_pr,MIN(1.0_pr-buffbff(2),buffbff(3)))
       buffbff(4)=MAX(0.0_pr,MIN(1.0_pr-SUM(buffbff(2:3)),buffbff(4)))
       buffbff(5)=MAX(0.0_pr,    1.0_pr-SUM(buffbff(2:4)))
       buffbfd(2:5)=buffbff(2:5)*buffbfd(1)
    ELSE
       buffbfd(2)=MIN(buffbfd(2),buffbfd(1))
       buffbfd(3)=MIN(buffbfd(3),buffbfd(1)-buffbfd(2))
       buffbfd(4)=MIN(buffbfd(4),buffbfd(1)-SUM(buffbfd(2:3)))
       buffbfd(5)=MAX(0.0_pr  ,buffbfd(1)-SUM(buffbfd(2:4)))
       buffbff(2:5)=buffbfd(2:5)/buffbfd(1)
    END IF
    IF (verb_level>0 .AND. par_rank==0) PRINT *,'FOR bufferfunc, buffbfd=', buffbfd
    IF (verb_level>0 .AND. par_rank==0) PRINT *,'FOR bufferfunc, buffbff=', buffbff

    buffDomFrac = MIN(buffcvf(1),buffbff(1))

    IF (ALLOCATED(bndvals)) DEALLOCATE(bndvals)
    ALLOCATE(bndvals(dim+Nspec+3,2*dim))  ! 1-flow variable, 2-boundary

    !variable thresholding
    CALL user_read_input__VT

  END SUBROUTINE user_read_input

  !
  ! calculate any additional variables
  ! 
  ! flag  - 0 calledwhile adapting to IC, 1 - called in main time integration loop
  !
  ! These additiona variables are calculated and left in real space.
  !
  SUBROUTINE user_additional_vars( t_local, flag )
    USE penalization
    IMPLICIT NONE
    REAL (pr), INTENT (IN) ::  t_local
    INTEGER , INTENT(IN) :: flag ! 0- called during adaption to IC, 1 called during main integration loop
    REAL (pr) :: cp, R, mygr, cmax, uval
    INTEGER :: l, i, j, myiter
    REAL (pr), DIMENSION(1,nwlt,MAX(dim,Nspecm,2)) :: du, d2u    
    REAL (pr) :: iterdiff, initdiff, curtau, fofn, deln, delnold, delfofn
    INTEGER, PARAMETER :: meth=1
    REAL (pr), DIMENSION(nwlt,dim*2) :: bigU


    REAL (pr), DIMENSION(nwlt) :: Ma_m
    INTEGER :: face_type, nloc, wlt_type, k, j_df
    INTEGER :: ie, i_bnd
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim)   :: face
    INTEGER, DIMENSION(nwlt)  :: iloc
    REAL (pr), DIMENSION(nwlt,dim) :: v
    REAL (pr), DIMENSION(nwlt,3) :: props
    REAL (pr), DIMENSION(dim,nwlt,dim) :: dv, d2v
    INTEGER :: j_wall    !minimum j_level at the wall

    REAL (pr), DIMENSION(nwlt,dim*(dim+1)/2) :: s_ij
    REAL (pr), DIMENSION(nwlt)               :: smod

    LOGICAL, SAVE :: header_flag = .TRUE.

    CALL user_bufferfunc_eric(bigU, nwlt, buffcvd, polyconv)
    IF(saveUcvt) u(:,ncvtU) = SUM(bigU(:,:),DIM=2)

    props(:,1) = cv_in(Nspec) 
    props(:,2) = Ma_in(Nspec)**2
    props(:,3) = rho_in(Nspec)
    IF (Nspec>1) props(:,1) = (1.0_pr - SUM(u(:,nspc(1):nspc(Nspecm)),DIM=2)/u(:,nden))*cv_in(Nspec)
    IF (Nspec>1) props(:,2) = (1.0_pr - SUM(u(:,nspc(1):nspc(Nspecm)),DIM=2)/u(:,nden))*Ma_in(Nspec)**2
    IF (Nspec>1) props(:,3) = (1.0_pr - SUM(u(:,nspc(1):nspc(Nspecm)),DIM=2)/u(:,nden))*rho_in(Nspec)
    DO l=1,Nspecm
        props(:,1) = props(:,1) + u(:,nspc(l))/u(:,nden)*cv_in(l) !cv
        props(:,2) = props(:,2) + u(:,nspc(l))/u(:,nden)*Ma_in(l)**2 !Ma^2
        props(:,3) = props(:,3) + u(:,nspc(l))/u(:,nden)*rho_in(l) !rho_m
    END DO
    u(:,nprs) = p_ref + (u(:,nden) - props(:,3) )/props(:,2)

    IF (adapttmp) u(:,nton) = (u(:,neng)-0.5_pr*SUM(u(:,nvel(1):nvel(dim))**2,DIM=2)/u(:,nden))/props(:,1)/u(:,nden) !Temperature
    IF (adaptvelonly) THEN
       DO i=1,dim
          u(:,nvon(i)) = u(:,nvel(i))/u(:,nden)
       END DO
    END IF
    IF (adaptspconly) THEN
       DO i=1,Nspecm
          u(:,nson(i)) = u(:,nspc(i))/u(:,nden)
       END DO
    END IF
    IF( adaptMagVort .OR. saveMagVort .OR. adaptComVort .OR. saveComVort .OR. adaptMagVel .OR. saveMagVel .OR. adaptNormS .OR. saveNormS ) THEN
       DO i=1,dim
          du(1,:,i) = u(:,nvel(i))/u(:,nden)
       END DO
       CALL bonusvars_vel(du(1,:,1:dim))  !4extra - call it
    END IF
    IF (modczn) THEN
       IF (t>tczn) THEN
          modczn=.FALSE.
          xyzzone(1,1) = czn_min
          xyzzone(2,1) = czn_max            
          IF (par_rank.EQ.0 .AND. verb_level>0) THEN
             PRINT *, '*****************************************************'
             PRINT *, 'FINALIZING TEMPORARY COORD_ZONE'
             PRINT *, 'COORD_ZONE OUTSIDE OF x=[',xyzzone(1,1),',',xyzzone(2,1),']'
             PRINT *, 'TIME: ', t
             PRINT *, 'STOPPING TIME FOR COORD_ZONE: ', tczn
             PRINT *, '*****************************************************'
          END IF
       END IF
    ELSE  
       IF (tfacczn>0.0_pr .AND. itfacczn>0.0_pr .AND. t>itczn) THEN
          itczn=t_end*10.0_pr
          czn_min = xyzzone(1,1)
          czn_max = xyzzone(2,1)
          xyzzone(1,1) = -Lczn
          xyzzone(2,1) =  Lczn
          Ma_m(:) = 1.0_pr
          IF (Nspec>1)  Ma_m = 1.0_pr - SUM(u(:,nspc(1):nspc(Nspecm)),DIM=2)/u(:,nden)
          Ma_m(:) = Ma_m(:)*Ma_in(Nspec)**2 !(Ma_Nspec)^2
          DO l=1,Nspecm
             Ma_m(:) = Ma_m(:) + u(:,nspc(l))/u(:,nden)*Ma_in(l)**2 !Ma^2
          END DO
          Ma_m(:) = SQRT(Ma_m(:))
          cczn = MINVAL(1.0_pr/Ma_m)
          tczn = MAXVAL(ABS(xyzlimits(:,1)))/cczn*tfacczn
          IF (par_rank.EQ.0 .AND. verb_level>0) THEN
             PRINT *, '*****************************************************'
             PRINT *, 'APPLYING TEMPORARY COORD_ZONE'
             PRINT *, 'COORD_ZONE OUTSIDE OF x=[',-Lczn,',',Lczn,']'
             PRINT *, 'MINIMUM WAVE SPEED: ', cczn
             PRINT *, 'MAXIMUM DISTANCE: ', MAXVAL(ABS(xyzlimits(:,1)))
             PRINT *, 'TIME: ', t
             PRINT *, 'STARTING TIME FOR COORD_ZONE: ', itczn
             PRINT *, 'TIME SAFETY FACTOR: ', tfacczn
             PRINT *, 'STOPPING TIME FOR COORD_ZONE: ', tczn
             PRINT *, '*****************************************************'
          END IF
          IF (t<tczn) THEN
             modczn = .TRUE.
          ELSE
             IF (par_rank.EQ.0) PRINT *, '!!!TOO LATE TO START COORD_ZONE: t>tczn!!!'
          END IF
       END IF
    END IF

    IF (savepardom) u(:,ndom) = REAL(par_rank,pr)

    IF (imask_obstacle) u(:,nmsk) = penal

    IF (saveUcvt) THEN  !12/2 - modified to carry u = x^2 function to validate surface integrals
!!$          u(:,ncvtU) = 0.0_pr
!!$          DO j = 1,dim
!!$             IF (j == ICver ) THEN
!!$                u(:,ncvtU) = u(:,ncvtU) + (bigU(:,2*j-1) + bigU(:,2*j))*buffU0
!!$             ELSE
!!$                u(:,ncvtU) = u(:,ncvtU) + (-bigU(:,2*j-1) + bigU(:,2*j))*buffU0
!!$             END IF
!!$          END DO

          !u(:,ncvtU) = x(:,1)**2
    END IF

  END SUBROUTINE user_additional_vars

  SUBROUTINE bonusvars_vel(velvec)   !4extra - this whole subroutine
    IMPLICIT NONE
    REAL (pr), INTENT(IN) :: velvec(nwlt,dim) !velocity
    REAL (pr) :: tmp(nwlt,2*dim) !tmp for vorticity
    INTEGER :: i

    IF( adaptMagVort .OR. saveMagVort .OR. adaptComVort .OR. saveComVort .OR. adaptMagVel .OR. saveMagVel .OR. adaptNormS .OR. saveNormS) THEN
       tmp = 0.0_pr
       ! Calculate the magnitude vorticity
       IF( adaptMagVort .OR. saveMagVort .OR. adaptComVort .OR.  saveComVort ) THEN
          CALL cal_vort (velvec, tmp(:,1:3-MOD(dim,3)), nwlt)
          IF ( adaptMagVort .OR. saveMagVort )  u(:,nmvt) = SQRT(tmp(:,1)**2 + tmp(:,2)**2 + tmp(:,3)**2 )
          IF ( adaptComVort .OR. saveComVort )  THEN
             DO i=1,3
                u(:,ncvt(4-i)) = tmp(:,4-i)
             END DO
          END IF
       END IF
       ! Calculate the magnitude of Sij (sqrt(SijSij) )
       ! s(:,1) = s_11
       ! s(:,2) = s_22
       ! s(:,3) = s_33
       ! s(:,4) = s_12
       ! s(:,5) = s_13
       ! s(:,6) = s_23
       tmp = 0.0_pr
       IF( adaptNormS .OR. saveNormS) THEN
          CALL Sij( velvec ,nwlt, tmp(:,1:2*dim), u(:,nnms), .TRUE. )
       END IF
       ! Calculate the magnitude of velocity
       IF( adaptMagVel .OR. saveMagVel) THEN
          u(:,nmvl) = 0.5_pr*(SUM(velvec(:,:)**2,DIM=2))
       END IF
    END IF
  END SUBROUTINE bonusvars_vel

  SUBROUTINE bonusvars_spc(spcvec)   !4extra - this whole subroutine
    IMPLICIT NONE
    REAL (pr), INTENT(IN) :: spcvec(nwlt,Nspecm) !species
    REAL (pr), DIMENSION(Nspecm,nwlt,dim) :: du, d2u    
    INTEGER :: i
    INTEGER, PARAMETER :: meth=1

    IF( adaptGradY .OR. saveGradY) THEN
       CALL c_diff_fast(spcvec(:,1:Nspecm), du(1:Nspecm,:,:), d2u(1:Nspecm,:,:), j_lev, nwlt,meth, 10, Nspecm, 1, Nspecm)  !dY/dx
       DO i=1,Nspecm
          u(:,ngdy(i)) = SUM(du(i,:,:)**2,DIM=2) !|dY/dx_i*dY/dx_i|
       END DO
    END IF
  END SUBROUTINE bonusvars_spc

  !
  ! calculate any additional scalar variables
  ! 
  ! arg 
  ! flag  - 0 calledwhile adapting to IC, 1 - called in main time integration loop
  !
  SUBROUTINE user_scalar_vars( flag )
    IMPLICIT NONE
    INTEGER , INTENT(IN) :: flag ! 0- called during adaption to IC, 1 called during main integration loop

  END SUBROUTINE user_scalar_vars

  !
  !************ Calculating Scales ***************************
  !
  ! Note the order of the components in the scl array
  ! correspond to u_tn, v_tn, w_tn, u_tn-1, v_tn-1, w_tn-1, u_tn-2, v_tn-2, w_tn-2
  ! 
  SUBROUTINE user_scales(flag,use_default, u_loc, nlocal, ne_local, l_n_var_adapt , l_n_var_adapt_index, &
       scl, scl_fltwt) !add
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: flag ! 0 during initial adaptation, then 1 during time advancement
    LOGICAL , INTENT(INOUT) :: use_default
    INTEGER, INTENT (IN) :: nlocal, ne_local
    REAL (pr), DIMENSION (1:nlocal,1:ne_local), INTENT (IN) :: u_loc
    LOGICAL , INTENT (IN) :: l_n_var_adapt(ne_local)
    INTEGER , INTENT (IN) :: l_n_var_adapt_index(1:ne_local)
    REAL (pr), DIMENSION (1:ne_local), INTENT (INOUT) :: scl
    REAL (pr), DIMENSION (1:ne_local) :: mean
    REAL (pr) ,  INTENT(IN) ::scl_fltwt !weight for temporal filter on scl
    REAL (pr) :: floor, tmp,tmpmax,tmpmin,tmpsum
    INTEGER :: i, ie, ie_index, tmpint
    REAL (pr) ,save, ALLOCATABLE, DIMENSION (:) :: scl_old
    LOGICAL  ,save :: startup_init = .TRUE.

    !
    ! Ignore the output of this routine and use default scales routine
    !
    use_default = .FALSE. 

    IF( .NOT. use_default ) THEN
       !IF (par_rank.EQ.0) PRINT *,'Use user_scales() <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<'
       !
       ! ALLOCATE scl_old if it was not done previously in user_scales
       !
       IF( startup_init .AND. .NOT. ALLOCATED(scl_old) ) THEN
          ALLOCATE( scl_old(1:ne_local) )
          scl_old = 0.0_pr
       END IF

       floor = 1.0e-12_pr
       scl   =1.0_pr
       !
       ! Calculate scl per component
       !
       mean = 0.0_pr
       mean(nden) = rho_in(Nspec)
       IF(.TRUE.) THEN
          DO ie=1,ne_local
             IF(l_n_var_adapt(ie)) THEN
                ie_index = l_n_var_adapt_index(ie)
                IF( Scale_Meth == 1 ) THEN ! Use Linf scale
                   tmpmax=MAXVAL ( u_loc(1:nlocal,ie_index) )
                   tmpmin=MINVAL ( u_loc(1:nlocal,ie_index) )
                   CALL parallel_global_sum(REALMAXVAL=tmpmax)
                   CALL parallel_global_sum(REALMINVAL=tmpmin)
                   mean(ie)= 0.5_pr*(tmpmax-tmpmin)
                ELSE IF ( Scale_Meth == 2 ) THEN ! Use L2 scale
                   tmpsum=SUM( u_loc(1:nlocal,ie_index) )
                   tmpint=nlocal
                   CALL parallel_global_sum(REAL=tmpsum)
                   CALL parallel_global_sum(INTEGER=tmpint)
                   mean(ie)= tmpsum/REAL(tmpint,pr)
                ELSE IF ( Scale_Meth == 3 ) THEN ! Use L2 scale using dA
                   tmpsum=SUM( u_loc(1:nlocal,ie_index)*dA )
                   CALL parallel_global_sum(REAL=tmpsum)
                   mean(ie)= tmpsum/ sumdA_global 
                END IF
             END IF
          END DO
       END IF
       DO ie=1,ne_local
          IF(l_n_var_adapt(ie)) THEN
             ie_index = l_n_var_adapt_index(ie)
             IF( Scale_Meth == 1 ) THEN ! Use Linf scale
                scl(ie)= MAXVAL ( ABS( u_loc(1:nlocal,ie_index) - mean(ie) ) )
                CALL parallel_global_sum( REALMAXVAL=scl(ie) )
             ELSE IF ( Scale_Meth == 2 ) THEN ! Use L2 scale
                scl(ie)= SUM( (u_loc(1:nlocal,ie_index)-mean(ie))**2 )
                tmpint=nlocal
                CALL parallel_global_sum( REAL=scl(ie) )
                CALL parallel_global_sum(INTEGER=tmpint)
                scl(ie)=SQRT(scl(ie)/REAL(tmpint,pr))
             ELSE IF ( Scale_Meth == 3 ) THEN ! Use L2 scale using dA
                scl(ie)= SUM( (u_loc(1:nlocal,ie_index)-mean(ie))**2*dA )
                CALL parallel_global_sum(REAL=scl(ie))
                scl(ie)=SQRT(scl(ie)/sumdA_global)
             ELSE
                PRINT *, 'Error in Scales(), Unknown Scale_Meth type: ', Scale_Meth
                PRINT *, 'Known Scale_Meth types: 1 = Linf scale, 2 = L2 scale '
                PRINT *, 'Exiting ...'
                stop
             END IF
             
        !     IF (par_rank.EQ.0) THEN
        !        WRITE (6,'("Scaling before taking vector(1:dim) magnitude")')
        !        WRITE (6,'("Scale factor eqn", I4 ," = ", es11.4 ," scalecoeff = ",es11.4 )') &
        !             ie, scl(ie), scaleCoeff(ie)
        !     END IF
          END IF
       END DO
       !
       ! take appropriate vector norm over scl(1:dim)
       IF (adaptvelonly) THEN
          IF( Scale_Meth == 1 ) THEN ! Use Linf scale
             scl(nvon(1):nvon(dim)) = MAXVAL(scl(nvon(1):nvon(dim)))
          ELSE IF ( Scale_Meth == 2 ) THEN ! Use L2 scale
             scl(nvon(1):nvon(dim)) = SQRT(SUM(scl(nvon(1):nvon(dim))**2))
          ELSE IF ( Scale_Meth == 3 ) THEN ! Use L2 scale using dA
             scl(nvon(1):nvon(dim)) = SQRT(SUM(scl(nvon(1):nvon(dim))**2))
          END IF
       END IF
       IF( Scale_Meth == 1 ) THEN ! Use Linf scale
          scl(nvel(1):nvel(dim)) = MAXVAL(scl(nvel(1):nvel(dim)))
       ELSE IF ( Scale_Meth == 2 ) THEN ! Use L2 scale
          scl(nvel(1):nvel(dim)) = SQRT(SUM(scl(nvel(1):nvel(dim))**2)) !now velocity vector length, rather than individual component
       ELSE IF ( Scale_Meth == 3 ) THEN ! Use L2 scale using dA
          scl(nvel(1):nvel(dim)) = SQRT(SUM(scl(nvel(1):nvel(dim))**2)) !now velocity vector length, rather than individual component
       END IF
       !
       ! Print out new scl
       !
       IF (adapttmp .AND. tempscl>floor) scl(nton)=tempscl
       IF (adapttmp .AND. adaptvelonly .AND. ABS(tempscl)<floor) scl(nton)=scl(nvon(1))
       IF (adaptspconly .AND. specscl>0.0_pr) scl(nson(1):nson(Nspecm))=specscl
       DO ie=1,ne_local
          IF(l_n_var_adapt(ie)) THEN
             !this is done if one of the variable is exactly zero inorder not to adapt to the noise
             IF(scl(ie) .le. floor) scl(ie)=1.0_pr 
             tmp = scl(ie)
             ! temporally filter scl
             IF(.not. startup_init ) scl(ie) = scl_fltwt * scl_old(ie) + (1.0_pr - scl_fltwt) * scl(ie)
             scl = scaleCoeff * scl
             IF (par_rank.EQ.0 .AND. verb_level .GT. 0) THEN
                WRITE (6,'("Scale factor eqn", I4 ," = ", es11.4 ," scalecoeff = ",es11.4 )') &
                     ie, scl(ie), scaleCoeff(ie)
                WRITE (6,'("(before filtering = ",es11.4,")" )') tmp
             END IF
          END IF
       END DO
       
       scl_old = scl !save scl for this time step
       startup_init = .FALSE.
    END IF

  END SUBROUTINE user_scales

  SUBROUTINE user_cal_cfl (use_default, ulocal, cfl_out)
    USE precision
    USE sizes
    USE pde
    IMPLICIT NONE
    LOGICAL , INTENT(INOUT) :: use_default
    REAL (pr),                                INTENT (INOUT) :: cfl_out
    REAL (pr), DIMENSION (nwlt,n_integrated), INTENT (IN)    :: ulocal
    INTEGER                    :: i
    REAL (pr)                  :: flor
    REAL (pr), DIMENSION(nwlt,dim) :: cfl, cfl_conv, cfl_acou
    REAL (pr), DIMENSION(dim,nwlt) :: h_arr
    REAL (pr)                  :: min_h
    REAL (pr), DIMENSION(nwlt) :: sos
    INTEGER :: l, mynwlt_global, fullnwlt, maxnwlt, minnwlt, avgnwlt
    REAL (pr) :: min_w, max_w

    IF (it>0) dt_original = mydt_orig

    use_default = .FALSE.

    flor = 1e-12_pr
    cfl_out = flor

    CALL get_all_local_h (h_arr)

    sos =  user_sound_speed (ulocal, n_integrated, nwlt)
    maxsos = MAXVAL(sos(:))

    CALL parallel_global_sum(REALMAXVAL=maxsos)

    maxMa = MAXVAL(    SQRT(SUM(ulocal(:,nvel(1):nvel(dim))**2,DIM=2))/ulocal(:,nden)/sos(:))

    CALL parallel_global_sum(REALMAXVAL=maxMa)

    DO i=1,dim
       cfl(:,i)      = ( ABS(ulocal(:,nvel(i))/ulocal(:,nden))+sos(:) ) * dt/h_arr(i,:)
       cfl_conv(:,i) = ( ABS(ulocal(:,nvel(i))/ulocal(:,nden))        ) * dt/h_arr(i,:)
       cfl_acou(:,i) = (                                       sos(:) ) * dt/h_arr(i,:)
    END DO
    CFLreport = MAXVAL(cfl(:,:))
    CFLconv   = MAXVAL(cfl_conv(:,:))
    CFLacou   = MAXVAL(cfl_acou(:,:))
    CALL parallel_global_sum(REALMAXVAL=CFLreport)
    CALL parallel_global_sum(REALMAXVAL=CFLconv)
    CALL parallel_global_sum(REALMAXVAL=CFLacou)
    min_h = MINVAL(h(1,1:dim)/2**(j_mx - 1))
    CALL parallel_global_sum(REALMINVAL=min_h)
    mynwlt_global = nwlt
    fullnwlt = PRODUCT(mxyz(1:dim)*2**(j_lev-1) + 1 - prd(:))
    CALL parallel_global_sum (INTEGER=mynwlt_global)
    avgnwlt = mynwlt_global/par_size 
    maxnwlt = nwlt
    minnwlt = -nwlt
    CALL parallel_global_sum(INTEGERMAXVAL=maxnwlt)
    CALL parallel_global_sum(INTEGERMAXVAL=minnwlt)
    minnwlt=-minnwlt
    MAreport = maxMa
    IF (convcfl) THEN  !use acoustic cfl for initial states when acoustics dominate
       cfl_out = MAX(cfl_out, CFLconv)
    ELSE
       cfl_out = MAX(cfl_out, CFLreport)
    END IF
    IF(time_integration_method .NE. 1 .AND. time_integration_method .NE. 2) cfl_out = MAX(cfl_out,dt/Re/(min_h**2)) !viscous length/time scale
    !calculate current Re_tau

    IF(dim .EQ. 3) THEN
       min_w = MINVAL(ulocal(:,nvel(3))/ulocal(:,nden))
       max_w = MAXVAL(ulocal(:,nvel(3))/ulocal(:,nden))
       CALL parallel_global_sum(REALMINVAL=min_w)
       CALL parallel_global_sum(REALMAXVAL=max_w)
    END IF

    IF (par_rank.EQ.0) THEN
       PRINT *, '**********************************************************'
       PRINT *, 'case:    ', file_gen(1:LEN_TRIM(file_gen)-1)
       PRINT *, 'max Ma = ', maxMa
       PRINT *, 'CFL    = ', cfl_out
       PRINT *, 'CFLconv= ', CFLconv, 'CFLacou= ', CFLacou
       PRINT *, 'it     = ', it,             '  t       = ', t
       PRINT *, 'iwrite = ', iwrite-1,       '  dt      = ', dt 
       !PRINT *, 'j_mx   = ', j_mx,           '  dt_orig = ', dt_original
       PRINT *, 'j_mx   = ', j_mx,           '  twrite  = ', twrite
       !PRINT *, 'j_lev  = ', j_lev,          '  twrite  = ', twrite
       PRINT *, 'j_lev  = ', j_lev,          '  n%      = ', REAL(mynwlt_global,pr)/REAL(fullnwlt,pr)
       !PRINT *, 'nwlt_g = ', mynwlt_global,  '  cpu     = ', timer_val(2)
       !PRINT *, 'n_all  = ', fullnwlt,       '  n%      = ', REAL(mynwlt_global,pr)/REAL(fullnwlt,pr)
       !PRINT *, 'nwlt   = ', nwlt,           '  eps     = ', eps
       PRINT *, 'nwlt_mx= ', maxnwlt,        '  nwlt_av = ', avgnwlt
       PRINT *, 'nwlt_mn= ', minnwlt,        '  p_size  = ', par_size
       IF(dim .EQ.3)PRINT *, 'min/max w = ',min_w, max_w
       PRINT *, '**********************************************************'
       
    END IF
    maxMa = MIN(maxMa,1.0_pr)

  END SUBROUTINE user_cal_cfl

  SUBROUTINE user_init_sgs_model( )
    IMPLICIT NONE
  END SUBROUTINE user_init_sgs_model

  SUBROUTINE  user_sgs_force (u_loc, nlocal)
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: nlocal
    REAL (pr), DIMENSION (nlocal,n_integrated), INTENT (INOUT) :: u_loc
  END SUBROUTINE  user_sgs_force

  FUNCTION user_sound_speed (u_loc, neq, nwlt)
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: nwlt, neq
    REAL (pr), DIMENSION (nwlt,neq), INTENT(IN) :: u_loc
    REAL (pr), DIMENSION (nwlt) :: user_sound_speed
    INTEGER :: l

    user_sound_speed(:) = 1.0_pr
    IF (Nspec>1) user_sound_speed(:) = 1.0_pr - SUM(u_loc(:,nspc(1):nspc(Nspecm)),DIM=2)/u_loc(:,nden)
    user_sound_speed(:) = user_sound_speed(:)*Ma_in(Nspec)**2 !(Ma_Nspec)^2
    DO l=1,Nspecm
       user_sound_speed(:) = user_sound_speed(:) + u_loc(:,nspc(l))/u_loc(:,nden)*Ma_in(l)**2 !Ma^2
    END DO
    PRINT *, 'rho:', MINVAL(u_loc(:,nden)),MAXVAL(u_loc(:,nden))
    PRINT *, 'Ma_m^2:', MINVAL(user_sound_speed),MAXVAL(user_sound_speed)
    user_sound_speed(:) = 1.0_pr/SQRT(user_sound_speed(:))

  END FUNCTION user_sound_speed

  FUNCTION user_Cv (u_loc, neq, nwlt)
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: nwlt, neq
    REAL (pr), DIMENSION (nwlt,neq), INTENT(IN) :: u_loc
    REAL (pr), DIMENSION (nwlt) :: user_Cv
    INTEGER :: l

    user_Cv(:) = 1.0_pr
    IF (Nspec>1)  user_Cv(:) = 1.0_pr - SUM(u(:,nspc(1):nspc(Nspecm)),DIM=2)/u(:,nden)  !Y_Nspec
    user_Cv(:) = user_Cv(:)*cv_in(Nspec)
    DO l=1,Nspecm
       user_Cv(:) = user_Cv(:) + u_loc(:,nspc(l))/u_loc(:,nden)*cv_in(l)
    END DO

  END FUNCTION user_Cv


  SUBROUTINE  user_pre_process
    IMPLICIT NONE
    INTEGER :: iseedsize
    REAL (pr), DIMENSION(nwlt) :: rand_array
    INTEGER, ALLOCATABLE, DIMENSION(:) :: seed
    INTEGER, SAVE :: add_noise=.TRUE.
    INTEGER :: l

  END SUBROUTINE user_pre_process

  SUBROUTINE  user_post_process
    IMPLICIT NONE
    INTEGER :: l

    CALL user_pre_process

    !variable thresholding
    IF ( do_Adp_Eps_Spatial .OR. do_Adp_Eps_Spatial_Evol ) &
    CALL user_post_process__VT(n_var_SGSD)    ! n_var_ExtraAdpt_Der    ! n_var_Epsilon   ! n_var_RSGSD         ! Cf: OPTIONAL

  END SUBROUTINE user_post_process

END MODULE user_case
