%status = 'new';
if (~exist('figtype'))
    figtype='inst';
end
if (exist('status') & status == 'new') | ~exist('status')
  close all;
  clear lg;
  clc;
  status = 'new';
  plotnum = 0;
end
if ~isempty(strmatch(figtype,'inst','exact'))
    plt_strt = 1;
    plt_step = 3;
elseif ~isempty(strmatch(figtype,'av','exact'))
    plt_strt = 2;
    plt_step = 3;
elseif ~isempty(strmatch(figtype,'exp_av','exact'))
    plt_strt = 3;
    plt_step = 3;
else
    plt_strt =1;
    plt_step =1;
end
lclr=['k- ';'r--'; 'b-.';'g- ';'m: ';'k--';'b-.';'g-.';'k: ';'r: ';'b: ';'g: '];  
Nlnstyles=size(lclr,1);
lineThickness = 2;

% Load fitness history file and plot
[FileNameTarget PathNameTarget] = uigetfile('./results/*_user_stats','Insert user_stats file');
FileNameTarget =  [PathNameTarget FileNameTarget];
fidTarget = fopen(FileNameTarget,'r');
data = importdata(FileNameTarget);
dimension = size(data);
lname{1} ='Drag';
lname{2} ='Lift';
ylims(:,1)=[0,2];
ylims(:,2)=[-3,3];
plots_per_type = 3;
t = data(:,1);
y = data(:,2:dimension(2));
for icase = plt_strt:plt_step:plots_per_type
    plotnum = plotnum+1;
    if(status == 'new') lg(:,plotnum) = cell(1,plotnum); end
    icase
    if icase == 2
        lg{1,plotnum} = [num2str(plotnum) ': Drag_{av}'];
        lg{2,plotnum} = [num2str(plotnum) ': Lift_{av}'];
    elseif icase == 3
        lg{1,plotnum} = [num2str(plotnum) ': Drag_{exp av}'];
        lg{2,plotnum} = [num2str(plotnum) ': Lift_{exp av}'];
    else
        lg{1,plotnum} = [num2str(plotnum) ': Drag'];
        lg{2,plotnum} = [num2str(plotnum) ': Lift'];
    end 
    for ifig = 1:fix((dimension(2)-1)/plots_per_type)
       figure(ifig);
       set(gcf,'Name',lname{ifig}); %set the window title
       if(status ~= 'new')
           hold on;
       end
       var_num = icase + (ifig-1)*plots_per_type;
       ln_type=mod(plotnum-1,Nlnstyles)+1;
       plot(t,y(:,var_num),lclr(ln_type,:),'LineWidth',lineThickness);
       legend(lg(ifig,1:plotnum),'Location','EastOutside');
       set(gca,'Ylim',ylims(:,ifig));
    end
   status = 'old';
end
fclose( fidTarget );
















