MODULE user_case
  !
  ! Case sphere
  USE precision
  USE elliptic_vars
  USE elliptic_mod
  USE field
  USE input_file_reader
  USE io_3D_vars
  USE pde
  USE share_consts
  USE share_kry
  USE sizes
  USE util_mod
  USE util_vars
  USE vector_util_mod
  USE wavelet_filters_mod
  USE wlt_vars
  USE wlt_trns_vars
  USE wlt_trns_mod
  USE wlt_trns_util_mod
  USE fft_module
  USE SGS_incompressible
  USE hyperbolic_solver
  USE PARALLEL
  !
  ! case specific variables
  !
  INTEGER n_var_vorticity ! start of vorticity in u array (Must exist)
  INTEGER n_var_pressure  ! start of pressure in u array  (Must exist)

  LOGICAL :: smooth_IC
  LOGICAL :: adaptMagVort, adaptNormS, adaptMagVel
  LOGICAL :: saveMagVort, saveNormS
  INTEGER :: IC_type, Nleaves, Jmode

  REAL(pr) :: A, B, C, a0, delta_chi, delta_smooth
  REAL (pr), DIMENSION(:,:), ALLOCATABLE :: Uold
  LOGICAL :: divergence_correction, divgrad
  INTEGER :: testmeth
CONTAINS

  !
  ! In user_setup_pde() we setup how many variables are integrated, which are interpolated
  ! to the next times step and if any exeact solution exists to compare to during the run.
  ! We also set the variable names that are used when the result files are written.
  !
  ! The following variables must be setup in this routine:
  !
  !
  ! n_integrated     ! first n_integrated eqns will be acted on for time integration
  ! n_var_additional ! 
  ! n_var
  !
  !
  !
  !
  SUBROUTINE  user_setup_pde ( VERB ) 
    IMPLICIT NONE
    LOGICAL, OPTIONAL :: VERB         ! print debug info
    INTEGER :: i

    PRINT * ,''
    PRINT *, '**********************Setting up PDE*****************'
    PRINT * ,'CASE ISOTURB '
    PRINT *, '*****************************************************'


    !------------ setting up default values 

    n_integrated = 2 ! uvw - # of equations to solve without SGS model
 
    n_integrated = n_integrated + n_var_SGS ! adds additional equations for SGS model
    
    n_time_levels = 1  !--# time levels

    n_var_time_levels = n_time_levels * n_integrated !--Number of equations (2D velocity at two time levels)

    IF( hypermodel /= 0 ) THEN
       n_var_additional = n_var_additional + 2 !--1 pressure at one time level
    ELSE
       n_var_additional = 2 !--1 pressure at one time level
    END IF
    n_var_exact = 0 !--No exact solution 

    n_var_pressure  = n_integrated + 1 !pressure

    n_var = n_var_time_levels + n_var_additional !--Total number of variables

    !
    !--Allocate logical arrays 
    !  Must be done after setting number of different types of variables
    !  and before setting logical variable mapping arrays
    !
    CALL alloc_variable_mappings
    !
    ! Fill in variable names for default variables for integration meth 2
    ! These may be changed in the call to the user specified routine
    !  user_setup_pde()
    !
    ! In integrated variables (3D)

    WRITE (u_variable_names(1), u_variable_names_fmt) 'P1   '
    WRITE (u_variable_names(2), u_variable_names_fmt) 'P2   '
    WRITE (u_variable_names(3), u_variable_names_fmt) 'eta1   '
    WRITE (u_variable_names(4), u_variable_names_fmt) 'eta2   '
!    WRITE (u_variable_names(n_var_pressure), u_variable_names_fmt)   'P    '

    !
    ! setup logical variable mapping arrays
    ! This defined which variables are used for adaptation, Which need to be interpololated to the new grid at 
    ! each time step, which are saved, and for which we have an exect solutiuon we want to check at each time step
    !

    !
    ! setup which components we will base grid adaptation on.
    ! Adapt on the 3 velocity components
    !
    n_var_adapt = .FALSE. !--Initially adapt on integrated variables at first time level
    n_var_adapt(1:n_integrated,0) = .TRUE. !--After first time step adapt on  velocity

    !eventually adapt to mdl variables    n_var_adapt(1:dim+2,1) = .TRUE. !--After first time step adapt on  velocity and Ilm and Imm

    n_var_adapt(1:n_integrated,1) = .TRUE. !--After first time step adapt on  velocity

    !Variables that need to be interpoleted to new adapted grid in initial grid adaptation
    n_var_interpolate(1:n_var,0) = .TRUE. 

    !Variables that need to be interpoleted to new adapted grid at each time step
    n_var_interpolate(1:n_var,1) = .TRUE. 

    !
    ! setup which components we have an exact solution for

    n_var_exact_soln(:,0:1) = .FALSE.

!!$    IF(testmeth == 1) n_var_exact_soln(2,0:1) = .TRUE.

    !
    ! variables required for restart
    !
    n_var_req_restart = .FALSE.
    n_var_req_restart(1:n_integrated)	        = .TRUE. !restart with velocities and pressure to begin with!

    ! no pressure for restart from initial file
    !n_var_req_restart(n_var_pressure ) = .TRUE.

    !
    ! setup which variables we will save the solution
    !
    n_var_save(1:n_var) = .TRUE. ! save all for restarting code

    !OPTIONAL: set up variables useed for calculation of numerical viscosity
    IF( hypermodel /= 0 ) THEN
       IF(ALLOCATED(n_var_hyper)) DEALLOCATE(n_var_hyper)
       ALLOCATE(n_var_hyper(1:n_var))
       n_var_hyper = .FALSE.
       n_var_hyper(1:2) = .TRUE.
       IF(ALLOCATED(n_var_hyper_active)) DEALLOCATE(n_var_hyper_active)
       ALLOCATE(n_var_hyper_active(1:n_integrated))
       n_var_hyper_active = .FALSE.
       n_var_hyper_active(1:2) = .TRUE.
    END IF

    !
    !--Time level counters for NS integration
    !   Used for integration meth2
    ! n0 is current time step
    ! n1 is t_n-1 time step
    ! n2 is t_n-2 time step
    !
    n0 = 1; n1 = n0+dim; n2 = n1+dim

    !
    ! Set the maximum number of components for which we have an exact solution
    !
    n_var_exact = MAX( COUNT(n_var_exact_soln(:,0)),COUNT(n_var_exact_soln(:,1)) )


    !
	! Setup a scaleCoeff array of we want to tweak the scaling of a variable
	! ( if scaleCoeff < 1.0 then more points will be added to grid 
	!
    ALLOCATE ( scaleCoeff(1:n_var), Umn(1:n_var) )
    scaleCoeff = 1.0_pr

    Umn = 0.0_pr !set up here if mean quantities anre not zero and used in scales or equation
    Umn(1) = 1.0_pr !--Uniform mean velocity in x-direction

    PRINT *, 'n_integrated = ',n_integrated 
    PRINT *, 'n_time_levels = ',n_time_levels
    PRINT *, 'n_var_time_levels = ',n_var_time_levels 
    PRINT *, 'n_var = ',n_var 
    PRINT *, 'n_var_exact = ',n_var_exact 
    PRINT *, '*******************Variable Names*******************'
    DO i = 1,n_var
       WRITE (*, u_variable_names_fmt) u_variable_names(i)
    END DO
    PRINT *, '****************************************************'



  END SUBROUTINE  user_setup_pde

  !
  ! read variables from input file for this user case 
  !
  SUBROUTINE  user_read_input()
 	USE share_consts
	IMPLICIT NONE
    INTEGER:: i
    CHARACTER (LEN=4) :: id_string
   
  call input_real ('A',A,'stop','A: coefficient in definition of eta')

  call input_real ('B',B,'stop','B: coefficient in definition of eta')

  call input_real ('C',C,'stop','C: coefficient in definition of eta')

  call input_real ('a0',a0,'stop','a0: coefficient in definition of eta')

  call input_integer ('Jmode',Jmode,'stop','Jmode: level of random noise')

  smooth_IC = .FALSE.
  call input_logical ('smooth_IC',smooth_IC,'default', &
                  '  if .TRUE. use smooth IC')

  END SUBROUTINE  user_read_input

  !
  ! Set the exact solution for comparison to the simulated solution
  !
  ! u          - array to fill in the exact solution
  ! nlocal       - number of active wavelets   
  ! ne_local        - total number of equations
  ! t          - time of current time step 
  ! l_n_var_exact_soln_index - index into the elements of u for which we need to 
  !                            find the exact solution
  SUBROUTINE  user_exact_soln (u, nlocal,  t_local, l_n_var_exact_soln)  
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: nlocal
    REAL (pr), INTENT (IN) ::  t_local
    REAL (pr), DIMENSION (nlocal,n_var_exact), INTENT (INOUT) :: u
    LOGICAL , INTENT (IN) :: l_n_var_exact_soln(n_var)


    ! There is no exact solution
!!$    IF(testmeth == 1) u(:,2)= 0.5_pr*(1.0_pr-SIGN(1.0_pr,SUM(x(:,:)*2,DIM=2)*EXP(-t_local)-R0**2))

  END SUBROUTINE  user_exact_soln

  SUBROUTINE user_initial_conditions (u, nlocal, ne_local, t_local, scl, scl_fltwt, iter)
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: nlocal, ne_local
    INTEGER  , INTENT (INOUT) :: iter ! iteration of call while adapting initial grid
    REAL (pr), DIMENSION (nlocal,ne_local), INTENT (INOUT) :: u
    REAL (pr), DIMENSION (nlocal,ne_local) :: utmp
    REAL (pr)  :: scl(1:n_var),scl_fltwt
    REAL (pr), DIMENSION(ne_local) :: scl_u
    REAL (pr), INTENT (IN) :: t_local
    REAL (pr) :: Amode
    INTEGER, DIMENSION(1) :: SEED
    INTEGER :: i, ii, ie, idim, ixyz(dim), i_p_mode(0:dim)

    IF(dim /= 2) THEN
       PRINT *, 'This case is only set up for dim=2'
       STOP
    END IF
    pi = 2.0_pr*ASIN(1.0_pr)
    scl_u = 1.0_pr

    u(:,1) = 0.0_pr

!    u(:,2) = user_chi (nwlt, t_local) 

    i_p_mode(0) = 1
    DO i=1,dim
       i_p_mode(i) = i_p_mode(i-1)*2**(Jmode-1)
    END DO
    u(:,1:ne_local)=0.0_pr
    SEED(1) = 54321
    CALL RANDOM_SEED(PUT=SEED(1:1))
    DO ie = 1, ne_local
       DO ii = 1, i_p_mode(dim)
          ixyz(1:dim) = INT(MOD(ii-1,i_p_mode(1:dim))/i_p_mode(0:dim-1))+1 
          CALL RANDOM_NUMBER(Amode)
          Amode = (2.0_pr*Amode-1.0_pr)!*EXP(-0.2_pr*SQRT(REAL(SUM(ixyz(1:dim)**2),pr)))
          utmp(:,ie)=Amode
          DO idim=1,dim
             utmp(:,ie)=utmp(:,ie)*cos(pi/(xyzlimits(2,idim)-xyzlimits(1,idim))*ixyz(idim)*(x(:,idim)-xyzlimits(1,idim)))
          END DO
          u(:,ie)=u(:,ie)+utmp(:,ie)
       END DO
    END DO
    u(:,1:ne_local) =SIGN(1.0_pr,u(:,1:ne_local))

    IF(smooth_IC) THEN
     ! CALL diffsmooth (smoothu,nlocal,ne_local,methdiff,IDdiff,diffdir,Dfac_loc,delta_loc,deltadel_loc,j_sm_in,j_st_in,BCtype,boundit)
       CALL diffsmooth (u(:,1:ne_local),nlocal,ne_local,LOW_ORDER,01,0,0.1_pr,delta_smooth,4.0_pr,-2,j_lev,3,.FALSE.)
    END IF

  END SUBROUTINE user_initial_conditions

!*********************************************************
  ! Arguments
  ! u         - field on adaptive grid
  ! nlocal      - number of active points
  ! ne_local       - number of equations
  ! t         - current time
  !
  ! Global variables used
  ! ifn       - number of equations
  ! x()
  ! xO
  ! y0
  !--******************************** 
  SUBROUTINE user_algebraic_BC (Lu, u, nlocal, ne_local, jlev, meth)
    !--Defines boundary condition type
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: jlev, meth, ne_local, nlocal
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (INOUT) :: Lu
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (IN)    :: u
    REAL (pr), DIMENSION (ne_local, nlocal, dim) :: du
    REAL (pr), DIMENSION (ne_local, nlocal, dim) :: d2u

    INTEGER :: i, ie, ii, shift
    !REAL (pr), DIMENSION (ne_nlocal,nlocal,dim) :: du, d2u !uncomment if Neuman BC are used, need to call 
    INTEGER :: face_type, nloc, meth_local
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc

    !BC for both 2D and 3D cases

    !if Neuman BC are used, need to call 
    CALL c_diff_fast (u, du, d2u, jlev, nlocal, meth, 10, ne_local, 1, ne_local)
    DO ie = 1, ne_local
       shift = nlocal*(ie-1)
       !--Go through all Boundary points that are specified
       i_p_face(0) = 1
       DO i=1,dim
          i_p_face(i) = i_p_face(i-1)*3
       END DO
       DO face_type = 0, 3**dim - 1
          face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
          IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
             CALL get_all_indices_by_face (face_type, jlev, nloc, iloc)
             IF(nloc > 0 ) THEN 
                Lu(shift+iloc(1:nloc)) = 0.0_pr  
                DO i = 1,dim
                   Lu(shift+iloc(1:nloc)) =   Lu(shift+iloc(1:nloc)) - face(i)*du(ie,iloc(1:nloc),i) !Neuman BC
                END DO
!!$                   Lu(shift+iloc(1:nloc)) = u(shift+iloc(1:nloc))  !Dirichlet conditions
             END IF
          END IF
       END DO
    END DO

  END SUBROUTINE user_algebraic_BC

  SUBROUTINE user_algebraic_BC_diag (Lu_diag, nlocal, ne_local, jlev, meth)
    !--Defines boundary condition type
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: jlev, meth, ne_local, nlocal
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (INOUT) :: Lu_diag
    REAL (pr), DIMENSION (nlocal,dim) :: du, d2u

    INTEGER :: i, ie, ii, shift
    ! REAL (pr), DIMENSION (nlocal,dim) :: du, d2u  !uncomment if Neuman BC are used
    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc

    !BC for both 2D and 3D cases

    !if Neuman BC are used, need to call 
    CALL c_diff_diag ( du, d2u, jlev, nlocal, meth, meth, 10)

    DO ie = 1, ne_local
       shift = nlocal*(ie-1)
       !--Go through all Boundary points that are specified
       i_p_face(0) = 1
       DO i=1,dim
          i_p_face(i) = i_p_face(i-1)*3
       END DO
       DO face_type = 0, 3**dim - 1
          face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
          IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
             CALL get_all_indices_by_face (face_type, jlev, nloc, iloc)
             IF(nloc > 0 ) THEN
                Lu_diag(shift+iloc(1:nloc)) = 0.0_pr  
                DO i = 1,dim
                   Lu_diag(shift+iloc(1:nloc)) =   Lu_diag(shift+iloc(1:nloc)) - face(i)*du(iloc(1:nloc),i) !Neuman BC
                END DO
!!$                   Lu_diag(shift+iloc(1:nloc)) = 1.0_pr            ! Dirichlet conditions
             END IF
          END IF
       END DO
    END DO

  END SUBROUTINE user_algebraic_BC_diag

  SUBROUTINE user_algebraic_BC_rhs (rhs, ne_local, nlocal, jlev)
    !--Sets rhs for boundary conditions
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: ne_local, nlocal, jlev
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (INOUT) :: rhs

    INTEGER :: i, ie, ii, shift
    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc

    !BC for both 2D and 3D cases

    DO ie = 1, ne_local
       shift = nlocal*(ie-1)
       !--Go through all Boundary points that are specified
       i_p_face(0) = 1
       DO i=1,dim
          i_p_face(i) = i_p_face(i-1)*3
       END DO
       DO face_type = 0, 3**dim - 1
          face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
          IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
             CALL get_all_indices_by_face (face_type, jlev, nloc, iloc)
             IF(nloc > 0 ) THEN 
                   rhs(shift+iloc(1:nloc)) = 0.0_pr     !Neuman BC
!!$                   rhs(shift+iloc(1:nloc)) = 1.0_pr  !Dirichlet conditions
             END IF
          END IF
       END DO
    END DO

  END SUBROUTINE user_algebraic_BC_rhs

  SUBROUTINE user_project (u, p, nlocal, meth)
    !--Makes u divergence free
    IMPLICIT NONE
    INTEGER, PARAMETER :: ne_local = 1
    INTEGER, INTENT (IN) :: meth, nlocal
    INTEGER :: meth_central, meth_backward, meth_forward, i  
    REAL (pr), DIMENSION (nlocal),     INTENT(INOUT) :: p
    REAL (pr), DIMENSION (nlocal,n_integrated), INTENT(INOUT) :: u
    REAL (pr), DIMENSION (nlocal,ne_local) :: f
    REAL (pr), DIMENSION (nlocal,dim) :: Unew
    REAL (pr), DIMENSION(ne_local) :: scl_p
    INTEGER, DIMENSION(ne_local) :: clip 

    meth_central  = meth + BIASING_NONE
    meth_backward = meth + BIASING_BACKWARD
    meth_forward  = meth + BIASING_FORWARD

  END SUBROUTINE user_project

  FUNCTION Laplace (jlev, u, nlocal, ne_local, meth_in)
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: jlev, nlocal, ne_local, meth_in
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (INOUT) :: u
    REAL (pr), DIMENSION (nlocal*ne_local) :: Laplace

    INTEGER :: i, ii, ie, ie1, meth, idim, shift
    INTEGER :: meth_central, meth_backward, meth_forward 
    REAL (pr), DIMENSION (dim, nlocal, dim) :: du, d2u, du_dummy
    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc

    meth_central  = meth_in + BIASING_NONE
    meth_backward = meth_in + BIASING_BACKWARD
    meth_forward  = meth_in + BIASING_FORWARD

    IF(divgrad) THEN
       CALL c_diff_fast (u, du(1:ne_local, 1:nlocal, 1:dim), d2u(1:ne_local, 1:nlocal, 1:dim), jlev, nlocal, meth_backward, 10, ne_local, 1, ne_local)
    ELSE
       CALL c_diff_fast (u, du(1:ne_local, 1:nlocal, 1:dim), d2u(1:ne_local, 1:nlocal, 1:dim), jlev, nlocal, meth_central, 01, ne_local, 1, ne_local)
    END IF
    DO ie = 1, ne_local
       shift=(ie-1)*nlocal
       Laplace(shift+1:shift+Nwlt_lev(jlev,0)) = 0.0_pr
       IF(divgrad) THEN
          !--- Internal points
          CALL c_diff_fast(du(ie,1:nlocal,1:dim), d2u(1:dim, 1:nlocal, 1:dim), du_dummy(1:dim, 1:nlocal, 1:dim), &
               jlev, nlocal, meth_forward, 10, dim, 1, dim )
          !--- div(grad)
          DO idim = 1,dim
             Laplace(shift+1:shift+Nwlt_lev(jlev,0)) = Laplace(shift+1:shift+Nwlt_lev(jlev,0)) &
                                                     + d2u(idim ,1:Nwlt_lev(jlev,0),idim)
          END DO
       ELSE
          DO idim = 1,dim
             Laplace(shift+1:shift+Nwlt_lev(jlev,0)) = Laplace(shift+1:shift+Nwlt_lev(jlev,0))  &
                                                     + d2u(ie ,1:Nwlt_lev(jlev,0),idim)
          END DO
       END IF
       !--Boundary points
       i_p_face(0) = 1
       DO i=1,dim
          i_p_face(i) = i_p_face(i-1)*3
       END DO
       DO face_type = 0, 3**dim - 1
          face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
          IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
             CALL get_all_indices_by_face (face_type, jlev, nloc, iloc)
             IF(nloc > 0 ) THEN ! ANY dimension
                Laplace(shift+iloc(1:nloc)) = u(shift+iloc(1:nloc))  !Dirichlet conditions
             END IF
          END IF
       END DO
    END DO

  END FUNCTION Laplace

  FUNCTION Laplace_diag (jlev,  nlocal, ne_local, meth_in)
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: jlev, nlocal, ne_local, meth_in
    REAL (pr), DIMENSION (nlocal*ne_local) :: Laplace_diag

    INTEGER :: i, ii, ie, ie1, idim, shift
    INTEGER :: meth_central, meth_backward, meth_forward 
    REAL (pr), DIMENSION (nlocal,dim) :: du, d2u
    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc

    meth_central  = meth_in + BIASING_NONE
    meth_backward = meth_in + BIASING_BACKWARD
    meth_forward  = meth_in + BIASING_FORWARD

    IF(divgrad) THEN
       CALL c_diff_diag ( du, d2u, jlev, nlocal, meth_backward, meth_forward, -01)
    ELSE
       CALL c_diff_diag ( du, d2u, jlev, nlocal, meth_central, meth_central, 01)
    END IF
    !PRINT *,'IN Laplace_diag, ne_local = ', ne_local

    DO ie = 1, ne_local
       shift=(ie-1)*nlocal
       Laplace_diag(shift+1:shift+Nwlt_lev(jlev,0)) = 0.0_pr
       DO idim = 1,dim
          Laplace_diag(shift+1:shift+Nwlt_lev(jlev,0)) = Laplace_diag(shift+1:shift+Nwlt_lev(jlev,0))  &
                                                       + d2u(1:Nwlt_lev(jlev,0),idim)
       END DO
       !--Boundary points
       i_p_face(0) = 1
       DO i=1,dim
          i_p_face(i) = i_p_face(i-1)*3
       END DO
       DO face_type = 0, 3**dim - 1
          face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
          IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
             CALL get_all_indices_by_face (face_type, jlev, nloc, iloc)
             IF(nloc > 0 ) THEN ! ANY dimension
                Laplace_diag(shift+iloc(1:nloc)) = 1.0_pr  !Dirichlet conditions
             END IF
          END IF
       END DO
    END DO

   END FUNCTION Laplace_diag

  FUNCTION Laplace_rhs(u, nlocal, ne_local, meth_in)
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: nlocal, ne_local, meth_in
    REAL (pr), DIMENSION (nlocal,ne_local), INTENT (INOUT) :: u
    REAL (pr), DIMENSION (nlocal,ne_local) :: Laplace_rhs

    INTEGER :: i, ii, ie, meth, idim
    INTEGER :: meth_central, meth_backward, meth_forward 
    REAL (pr), DIMENSION (ne_local ,nlocal,dim) :: du
    REAL (pr), DIMENSION (ne_local*dim ,nlocal,dim) :: d2u
    REAL (pr), DIMENSION (ne_local*dim ,nlocal,dim) :: du_dummy ! passed when only calculating 1st derivative.
    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc

    meth_central  = meth_in + BIASING_NONE
    meth_backward = meth_in + BIASING_BACKWARD
    meth_forward  = meth_in + BIASING_FORWARD

    Laplace_rhs = 0.0_pr

    !--Boundary points
    i_p_face(0) = 1
    DO i=1,dim
       i_p_face(i) = i_p_face(i-1)*3
    END DO
    DO face_type = 0, 3**dim - 1
       face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
       IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
          CALL get_all_indices_by_face (face_type, j_lev, nloc, iloc)
          IF(nloc > 0 ) THEN 
             Laplace_rhs(iloc(1:nloc),:) = 0.0_pr !Dirichlet conditions
          END IF
       END IF
    END DO
    
  END FUNCTION Laplace_rhs


  FUNCTION user_rhs (u_integrated,p)
    USE penalization
    IMPLICIT NONE
    REAL (pr), DIMENSION (ng,ne), INTENT(IN) :: u_integrated !1D flat version of integrated variables without BC 
    REAL (pr), DIMENSION (ng), INTENT(IN) :: p
    REAL (pr), DIMENSION (n) :: user_rhs

    INTEGER :: ie, ie1, shift, i
    INTEGER, PARAMETER :: meth=1
    REAL (pr), DIMENSION (ne,ng,dim) :: du, d2u
    REAL (pr), DIMENSION (ng,dim)     :: dp

    INTEGER :: face_type, nloc, meth_local
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc
    INTEGER :: meth_central, meth_backward, meth_forward 
    
    meth_central  = meth + BIASING_NONE
    meth_backward = meth + BIASING_BACKWARD
    meth_forward  = meth + BIASING_FORWARD
    
    CALL c_diff_fast(u_integrated, du, d2u, j_lev, ng, meth_central, 11, ne, 1, ne) !derivatives are calculated even for SGS terms

    !--Form right hand side of evolution equations
    DO ie = 1, dim
       shift=(ie-1)*ng
       user_rhs(shift+1:shift+ng) = a0*SUM(d2u(ie,:,:),DIM=2) - (A + (B+C)*SUM(u_integrated(:,:)**2,DIM=2) - C*u_integrated(:,ie)**2)*u_integrated(:,ie)
    END DO

    !--Go through all Boundary points that are specified
!!$    i_p_face(0) = 1
!!$    DO i=1,dim
!!$       i_p_face(i) = i_p_face(i-1)*3
!!$    END DO
!!$    DO face_type = 0, 3**dim - 1
!!$       face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
!!$       IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
!!$          CALL get_all_indices_by_face (face_type, j_lev, nloc, iloc)
!!$          IF(nloc > 0 ) THEN 
!!$             user_rhs(ng+iloc(1:nloc)) = 0.0_pr
!!$             DO i = 1,dim
!!$                user_rhs(ng+iloc(1:nloc)) =  user_rhs(ng+iloc(1:nloc)) - face(i)*MAX(0.0_pr,face(i)*Uold(iloc(1:nloc),i))*du(2,iloc(1:nloc),i) 
!!$             END DO
!!$          END IF
!!$       END IF
!!$    END DO


 
    IF( hypermodel /= 0 ) CALL hyperbolic(u_integrated, ng, user_rhs)

    !---------------- Algebraic BC are set up in user_algebraic_BC and they autmatically overwrite evolution BC

  END FUNCTION user_rhs


  ! find Jacobian of Right Hand Side of the problem
  ! u_prev == u_prev_timestep_loc

  FUNCTION user_Drhs (u, u_prev, meth)
    USE penalization
    IMPLICIT NONE
    INTEGER, INTENT(IN) ::  meth
    !u_prev_timestep passed local to cast it into 2dim array to work with vector derivatives
    REAL (pr), DIMENSION (ng,ne) :: u, u_prev
    REAL (pr), DIMENSION (n) :: user_Drhs

    INTEGER :: i, ie, ie1, shift
    REAL (pr), DIMENSION (ne,ng,dim) :: du, d2u
    REAL (pr), DIMENSION (ng,dim)     :: dp

    INTEGER :: face_type, nloc, meth_local
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc
    INTEGER :: meth_central, meth_backward, meth_forward 

    meth_central  = meth + BIASING_NONE
    meth_backward = meth + BIASING_BACKWARD
    meth_forward  = meth + BIASING_FORWARD
    
    ! find 1st and 2nd deriviative of u and
    CALL c_diff_fast(u, du(1:ne,:,:), d2u(1:ne,:,:), j_lev, ng, meth_central, 11, ne , 1, ne )
    
!!$    ! find only first deriviativ e  u_prev_timestep
!!$    CALL c_diff_fast(u_prev, du(ne+1:2*ne,:,:), du_dummy(1:ne,:,:), j_lev, ng, meth, 10, ne , 1, ne )

    !--Form right hand side of evolution equations
    DO ie = 1, dim
       shift=(ie-1)*ng
       user_Drhs(shift+1:shift+ng) = a0*SUM(d2u(ie,:,:),DIM=2) - (A + (B+C)*SUM(u_prev(:,:)**2,DIM=2) - C*u_prev(:,ie)**2)*u(:,ie) &
                                                              - (2.0_pr*(B+C)*SUM(u_prev(:,:)*u(:,:),DIM=2) - 2.0_pr*C*u_prev(:,ie)*u(:,ie))*u_prev(:,ie)
     END DO

    !--Go through all Boundary points that are specified
!!$    i_p_face(0) = 1
!!$    DO i=1,dim
!!$       i_p_face(i) = i_p_face(i-1)*3
!!$    END DO
!!$    DO face_type = 0, 3**dim - 1
!!$       face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
!!$       IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
!!$          CALL get_all_indices_by_face (face_type, j_lev, nloc, iloc)
!!$          IF(nloc > 0 ) THEN 
!!$             user_Drhs(ng+iloc(1:nloc)) = 0.0_pr
!!$             DO i = 1,dim
!!$                user_Drhs(ng+iloc(1:nloc)) =  user_Drhs(ng+iloc(1:nloc)) - face(i)*MAX(0.0_pr,face(i)*Uold(iloc(1:nloc),i))*du(2,iloc(1:nloc),i) 
!!$             END DO
!!$          END IF
!!$       END IF
!!$    END DO

    IF( hypermodel /= 0 ) CALL hyperbolic(u, ng, user_Drhs)

  END FUNCTION user_Drhs


  !
  ! Uses u_prev_timestep, which is global flat (1D ) array set in time_step_cn()
  !
  FUNCTION user_Drhs_diag (meth)
    USE penalization
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: meth
    REAL (pr), DIMENSION (n) :: user_Drhs_diag

    INTEGER :: i, ie, ie1, shift,shiftIlm,shiftImm
    REAL (pr), DIMENSION (ng,dim) :: du, d2u
    REAL (pr), DIMENSION (ne,ng,dim) :: du_prev_timestep
    REAL (pr), DIMENSION (ne  ,ng,dim) :: du_dummy ! passed when only calculating 1st derivative.

    INTEGER :: face_type, nloc, meth_local
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc
    INTEGER :: meth_central, meth_backward, meth_forward 

    meth_central  = meth + BIASING_NONE
    meth_backward = meth + BIASING_BACKWARD
    meth_forward  = meth + BIASING_FORWARD

!!$    CALL c_diff_fast(u_prev_timestep, du_prev_timestep, du_dummy, j_lev, ng, meth, 10, ne, 1, ne)

    !
    ! does not rely on u so we can call it once here
    !
    CALL c_diff_diag(du, d2u, j_lev, ng, meth_central, meth_central, 11)
    !--Form right hand side of evolution equations
    DO ie = 1, dim
       shift=(ie-1)*ng
       user_Drhs_diag(shift+1:shift+ng) = a0*SUM(d2u(:,:),DIM=2) - (A + (B+C)*(u_prev_timestep(1:ng)**2+u_prev_timestep(ng+1:2*ng)**2) - C*u_prev_timestep(shift+1:shift+ng)**2) &
                                                                 - 2.0_pr*B*u_prev_timestep(shift+1:shift+ng)**2
    END DO

    !--Go through all Boundary points that are specified
!!$    i_p_face(0) = 1
!!$    DO i=1,dim
!!$       i_p_face(i) = i_p_face(i-1)*3
!!$    END DO
!!$    DO face_type = 0, 3**dim - 1
!!$       face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
!!$       IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
!!$          CALL get_all_indices_by_face (face_type, j_lev, nloc, iloc)
!!$          IF(nloc > 0 ) THEN 
!!$             user_Drhs_diag(ng+iloc(1:nloc)) = 0.0_pr
!!$             DO i = 1,dim
!!$                user_Drhs_diag(ng+iloc(1:nloc)) =  user_Drhs_diag(ng+iloc(1:nloc)) - face(i)*MAX(0.0_pr,face(i)*Uold(iloc(1:nloc),i))*du(iloc(1:nloc),i) 
!!$             END DO
!!$          END IF
!!$       END IF
!!$    END DO

     IF( hypermodel /= 0 )CALL hyperbolic_diag(user_Drhs_diag, ng)

  END FUNCTION user_Drhs_diag

  FUNCTION user_chi (nlocal, t_local)
    IMPLICIT NONE
    INTEGER,          INTENT (IN) :: nlocal
    REAL (pr),        INTENT (IN) :: t_local
    REAL (pr), DIMENSION (nlocal) :: user_chi, r, theta
    REAL (pr), DIMENSION(nlocal,DIM) :: Xloc
    INTEGER :: i
    LOGICAL, SAVE :: user_chi_initialized = .FALSE.

    !--Defines mask for array of plates

!!$    r= SQRT(SUM(x(:,1:dim)**2,DIM=2))
!!$
!!$    user_chi = 0.5_pr*(1.0_pr + tanh(r/delta_chi))
!!$
!!$    user_chi = MAX(0.0_pr,MIN(1.0_pr, user_chi))

    pi = 2.0_pr*ASIN(1.0_pr)
    user_chi = SIGN(1.0_pr, x(:,1)) ! 2.0_pr/pi*ATAN(x(:,1)/delta_chi)

  END FUNCTION user_chi

  !
  ! Calculate any statitics
  !
  ! startup_flag - 0 when adapting to IC,then 1 inmain integration loop
  !
  !
  SUBROUTINE user_stats (u , j_mn, startup_flag)
!    USE fft_module
!    USE spectra_module
    USE wlt_vars
    USE vector_util_mod
    USE parallel
    IMPLICIT NONE
    REAL (pr), DIMENSION (nwlt,1:n_var), INTENT (IN) :: u
    INTEGER , INTENT (IN) :: j_mn 
    INTEGER , INTENT (IN) :: startup_flag
    CHARACTER (LEN=256)  :: filename

    !USER may define additional statistics ouutput here.

!!$    IF(startup_flag /= -1) THEN !statistics accumulation
!!$       IF (par_rank.EQ.0) THEN
!!$          WRITE(*,'(" ")')
!!$          WRITE(*,'("****************** Statistics on the Fly *******************")')
!!$       END IF
!!$    END IF

  END SUBROUTINE user_stats

  !
  ! calculate any additional variables
  ! 
  ! arg 
  ! flag  - 0 calledwhile adapting to IC, 1 - called in main time integration loop
  !
  ! These additiona variables are calculated and left in real space.
  !
  SUBROUTINE user_additional_vars( t_local, flag )
    IMPLICIT NONE
    REAL (pr), INTENT (IN) ::  t_local
    INTEGER , INTENT(IN) :: flag ! 0- called during adaption to IC, 1 called during main integration loop
    REAL(pr) tmp(nwlt,2*dim) !tmp for vorticity
    INTEGER :: i, ie

    DO ie = 1, n_integrated
       u(:,n_integrated+ie) = (A + (B+C)*SUM(u(:,1:n_integrated)**2,DIM=2) - C*u(:,ie)**2)*u(:,ie)
    END DO

  END SUBROUTINE user_additional_vars


  !
  ! calculate any additional scalar variables
  ! 
  ! arg 
  ! flag  - 0 calledwhile adapting to IC, 1 - called in main time integration loop
  !
  SUBROUTINE user_scalar_vars( flag )
    IMPLICIT NONE
    INTEGER , INTENT(IN) :: flag ! 0- called during adaption to IC, 1 called during main integration loop


  END SUBROUTINE user_scalar_vars

  !
  !************ Calculating Scales ***************************
  !
  ! Note the order of the components in the scl array
  ! correspond to u_tn, v_tn, w_tn, u_tn-1, v_tn-1, w_tn-1, u_tn-2, v_tn-2, w_tn-2
  ! 
  SUBROUTINE user_scales(flag, use_default, u_loc, nlocal, ne_local, l_n_var_adapt , l_n_var_adapt_index, &
       scl, scl_fltwt) !add
    USE parallel
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: flag ! 0 during initial adaptation, then 1 during time advancement
    LOGICAL , INTENT(INOUT) :: use_default
    INTEGER, INTENT (IN) :: nlocal, ne_local
    REAL (pr), DIMENSION (1:nlocal,1:ne_local), INTENT (IN) :: u_loc
    LOGICAL , INTENT (IN) :: l_n_var_adapt(ne_local)
    INTEGER , INTENT (IN) :: l_n_var_adapt_index(1:ne_local)
    REAL (pr), DIMENSION (1:ne_local), INTENT (INOUT) :: scl
    REAL (pr) ,  INTENT(IN) ::scl_fltwt !weight for temporal filter on scl
    REAL (pr) :: floor, tmp
    INTEGER :: ie, ie_index, itmp
    REAL (pr) ,save, ALLOCATABLE, DIMENSION (:) :: scl_old
    LOGICAL  ,save :: startup_init = .TRUE.

    !
    ! Ignore the output of this routine and use default scales routine
    !
    use_default = .TRUE. 

  END SUBROUTINE user_scales

  SUBROUTINE user_cal_cfl (use_default, u, cfl_out)
    USE parallel
    IMPLICIT NONE
    LOGICAL , INTENT(INOUT) :: use_default
    REAL (pr),                                INTENT (INOUT) :: cfl_out
    REAL (pr), DIMENSION (nwlt,n_integrated), INTENT (IN)    :: u
    
    INTEGER                    :: i
    REAL (pr)                  :: floor
    REAL (pr), DIMENSION (dim) :: cfl
    REAL (pr), DIMENSION(dim,nwlt) :: h_arr
    
    use_default = .FALSE.
    
    floor = 1e-12_pr
    cfl_out = floor
    
    CALL get_all_local_h (h_arr)
    
!!$    DO i = 1, nwlt
!!$       cfl(1:dim) = ABS (Uold(i,1:dim)) * dt/h_arr(1:dim,i)
!!$       cfl_out = MAX (cfl_out, MAXVAL(cfl))
!!$    END DO

    CALL parallel_global_sum( REALMAXVAL=cfl_out )
    
  END SUBROUTINE user_cal_cfl
  
  SUBROUTINE  user_sgs_force ( u, nlocal)
    IMPLICIT NONE
    
    INTEGER,                         INTENT (IN) :: nlocal
    REAL (pr), DIMENSION (nlocal,n_integrated), INTENT (INOUT) :: u
    
    !default SGS_force, can be explicitely deifined below instead of calling default SGS_force
    IF(sgsmodel /= 0) CALL sgs_force ( u, nlocal)
    
  END SUBROUTINE user_sgs_force

  FUNCTION user_sound_speed (u, neq, nwlt)
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: nwlt, neq
    INTEGER :: meth
    REAL (pr), DIMENSION (nwlt,neq), INTENT(IN) :: u
    REAL (pr), DIMENSION (nwlt) :: user_sound_speed

    IF(divgrad) THEN
       meth = HIGH_ORDER + BIASING_BACKWARD
    ELSE
       meth  = HIGH_ORDER + BIASING_NONE
    END IF

!!$    IF(testmeth ==1) THEN
!!$       user_sound_speed(:) = 0.5_pr*SQRT(SUM(x(:,1:dim)**2,DIM=2))
!!$    ELSE
!!$       user_sound_speed(:) = SQRT(SUM(grad (u(:,n_var_pressure), nwlt, j_lev,  meth)**2,DIM=2))
!!$    END IF

    user_sound_speed(:) =1.0

  END FUNCTION user_sound_speed

  SUBROUTINE  user_pre_process
    IMPLICIT NONE
    INTEGER, PARAMETER :: ne_local = 1
    INTEGER :: i
    INTEGER :: meth_central, meth_backward, meth_forward 
    REAL (pr), DIMENSION (nwlt,ne_local) :: dp
    REAL (pr), DIMENSION (nwlt,ne_local) :: f
    REAL (pr), DIMENSION(ne_local) :: scl_p
    INTEGER, DIMENSION(ne_local) :: clip 

    meth_central  = HIGH_ORDER + BIASING_NONE
    meth_backward = HIGH_ORDER + BIASING_BACKWARD
    meth_forward  = HIGH_ORDER + BIASING_FORWARD
    
  END SUBROUTINE user_pre_process

  SUBROUTINE  user_post_process
    IMPLICIT NONE

  END SUBROUTINE user_post_process


SUBROUTINE diffsmooth (smoothu,nlocal,ne_local,methdiff,IDdiff,diffdir,Dfac_loc,delta_loc,deltadel_loc,j_sm_in,j_st_in,BCtype,boundit)
    !diffsmooth uses explicit solver of the diffusion eqn to smooth fields
    !written by Scott Reckinger
    !
    !PARAMETERS:
    !   smoothu      - Both input and output of field that will be smoothed
    !   nlocal       - field size (number of points in smoothu)
    !   ne_local     - number of variables in smoothu
    !   methdiff     - meth for c_diff_fast (0 or 1 for central, 2 or 3 for backward, 4 or 5 for forward; even for low order, odd for high order)
    !   IDdiff       - ID for c_diff_fast (+/- [10, 01, or 11])
    !   diffdir      - direction of diffusion (0 = all directions, [1, 2, or 3] = diffuse only in that direction)
    !   Dfac_loc     - factor applied to stability requirement for dt (1 = at stability requirement, 0.1 = one tenth of stability requirement)
    !   delta_loc    - approximate post-smoothing diffusion thickness for and input step function, used to set total diffusion time; set <0 to use deltadel_loc
    !   deltadel_loc - sets final diffusion thickness using spatial resolution (dx, dy, or dz), number of points across final thickness, used to set total diffusion time; only if delta_loc<0
    !   j_sm_in      - level to set dx, dy, and dz for smoothing; only used for deltadel_loc; set =-1 to use j_lev, set =-2 to use j_mx, set =-3 to use j_mx-1
    !   j_st_in      - level to set dx, dy, and dz for stability
    !   BCtype       - type of boundary treatment; 1=Dirichlet (use input boundary values), 2=Neumann (not implemented yet), 3=Diffuse parallel to boundary only
    !   boundit      - set .TRUE. to bound the fields by the input max and min values during diffusion smoothing
    !
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: nlocal, ne_local, j_sm_in, j_st_in
    REAL (pr), DIMENSION (nlocal,ne_local), INTENT(INOUT) :: smoothu
    INTEGER, INTENT(IN) :: methdiff, IDdiff, BCtype !1=Dirichlet, 2=Neumann, 3=Diffuse in parallel direction to boundary
    INTEGER, INTENT(IN) :: diffdir !0=diff all direction, 1,2,3=diff direction
    LOGICAL, INTENT(IN) :: boundit !TRUE to bound solution by initial max and min
    REAL(pr), INTENT(IN) :: Dfac_loc, delta_loc, deltadel_loc
    REAL (pr) :: diffdt, difft, curt, mydelta
    REAL (pr), DIMENSION (ne_local) :: umax,umin
    REAL (pr), DIMENSION (dim) :: delxyz_sm, delxyz_st
    REAL (pr), DIMENSION (ne_local,nlocal,dim) :: du
    REAL (pr), DIMENSION (ne_local,nlocal,dim) :: d2u
    INTEGER :: Nt, tstp, i, ii, j_sm, j_st
    INTEGER, PARAMETER :: meth=1
    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nlocal) :: iloc

       IF (boundit) THEN
          DO i=1,ne_local
             umax(i)=MAXVAL(smoothu(:,i))
             umin(i)=MINVAL(smoothu(:,i))
             CALL parallel_global_sum (REALMAXVAL=umax(i))
             CALL parallel_global_sum (REALMINVAL=umin(i))
          END DO
       END IF
       j_sm=j_sm_in
       j_st=j_st_in
       IF (j_sm==-1) j_sm=j_lev
       IF (j_sm==-2) j_sm=j_mx
       IF (j_sm==-3) j_sm=j_mx-1
       delxyz_st(1:dim)=(xyzlimits(2,1:dim)-xyzlimits(1,1:dim))/REAL(mxyz(1:dim)*2**(j_st-1),pr)  !delxyz_st = L/N in every direction on j_st (for stability)
       delxyz_sm(1:dim)=(xyzlimits(2,1:dim)-xyzlimits(1,1:dim))/REAL(mxyz(1:dim)*2**(j_sm-1),pr)  !delxyz_sm = L/N in every direction on j_sm (for smoothing)
       IF (diffdir .EQ. 0) THEN
          diffdt=Dfac_loc/2.0_pr/(SUM(1.0_pr/delxyz_st(:)**2))
       ELSE
          diffdt=Dfac_loc/2.0_pr*delxyz_st(diffdir)**2
       END IF
       mydelta=delta_loc
       IF (mydelta .LT. 0.0_pr) mydelta=deltadel_loc*delxyz_sm(1)
       difft=mydelta**2/4.0_pr/pi
       Nt=CEILING(difft/diffdt)
       tstp=0
       curt=0.0_pr
       DO WHILE(curt.lt.difft)
          curt=curt+diffdt
          tstp=tstp+1
          CALL c_diff_fast(smoothu(:,1:ne_local), du(1:ne_local,:,:), d2u(1:ne_local,:,:), j_lev, nlocal, methdiff, IDdiff, ne_local, 1, ne_local)

          DO i=1,ne_local
             i_p_face(0) = 1
             DO ii=1,dim
                i_p_face(ii) = i_p_face(ii-1)*3
             END DO
             DO face_type = 0, 3**dim - 1
                face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
                CALL get_all_indices_by_face (face_type, j_lev, nloc, iloc)
                IF(nloc > 0 ) THEN
                   IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
                      IF (BCtype .EQ. 3) THEN !diffuse parallel to boundaries
                         IF (diffdir .EQ. 0) THEN
                            smoothu(iloc(1:nloc),i)=smoothu(iloc(1:nloc),i)+diffdt*SUM(d2u(i,iloc(1:nloc),:),DIM=2)
                         ELSE
                            smoothu(iloc(1:nloc),i)=smoothu(iloc(1:nloc),i)+diffdt*d2u(i,iloc(1:nloc),diffdir)
                         END IF
                         DO ii=1,dim
                            IF ( (ABS(face(ii))==1) .AND. (diffdir==0 .OR. diffdir==ii) ) smoothu(iloc(1:nloc),i)=smoothu(iloc(1:nloc),i)-diffdt*d2u(i,iloc(1:nloc),ii)
                         END DO
                      ELSE IF (BCtype .EQ. 2) THEN !Nuemann BCs

                      END IF !IF BCtype==1, Dirichlet conditions require nothing to be done on boundaries
                   ELSE
                      IF (diffdir .EQ. 0) THEN
                         smoothu(iloc(1:nloc),i)=smoothu(iloc(1:nloc),i)+diffdt*SUM(d2u(i,iloc(1:nloc),:),DIM=2)
                      ELSE
                         smoothu(iloc(1:nloc),i)=smoothu(iloc(1:nloc),i)+diffdt*d2u(i,iloc(1:nloc),diffdir)
                      END IF
                   END IF
                END IF
             END DO
          END DO
          IF (boundit) THEN
             DO i=1,ne_local
                DO ii=1,nlocal
                   smoothu(ii,i) = MIN(umax(i),MAX(umin(i),smoothu(ii,i)))
                END DO
             END DO
          END IF
       END DO
  END SUBROUTINE diffsmooth


END MODULE user_case






