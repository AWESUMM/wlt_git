function movie3D(fileName,varName,st_num,fig_type)

global Zmn Zmx

if strcmp(varName,'rho')
  Zmn =0.;
  Zmx = 1.;
else if strcmp(varName,'P')
  Zmn =0.;
  Zmx = 25.;
else
  Zmn=[];
  Zmx=[];
end
if strcmp(fig_type,'solution')
  printName=varName;
elseif strcmp(fig_type,'grid')
  printName='grid'
end
  
  for i=1:1:length(st_num)
  figure(1)
  showme3D(fileName,varName,st_num(i),fig_type);
  print('-djpeg',['test1_j9_' printName '_t'  num2str(st_num(i),'%4.4i') '.jpg']);
  pause(1);
end

end