MODULE user_case
  !
  ! Case sphere
  USE precision
  USE elliptic_vars
  USE elliptic_mod
  USE field
  USE input_file_reader
  USE io_3D_vars
  USE pde
  USE share_consts
  USE share_kry
  USE sizes
  USE util_mod
  USE util_vars
  USE vector_util_mod
  USE wavelet_filters_mod
  USE wlt_vars
  USE wlt_trns_vars
  USE wlt_trns_mod
  USE wlt_trns_util_mod
  USE fft_module
  USE SGS_incompressible
  USE hyperbolic_solver
  USE PARALLEL
  !
  ! case specific variables
  !
  INTEGER n_var_phi       ! 
  INTEGER n_var_pressure  ! start of pressure in u array  (Must exist)
  INTEGER, DIMENSION(:), ALLOCATABLE :: n_var_Strain, n_var_StrainRate, n_var_Stress
  INTEGER, DIMENSION(:), ALLOCATABLE :: n_var_P, n_var_U, n_var_dU
  LOGICAL :: smooth_IC
  LOGICAL :: adaptMagVort, adaptNormS, adaptMagVel
  LOGICAL :: saveMagVort, saveNormS
  INTEGER :: IC_type, Nleaves, Jmode

  REAL(pr) :: A, B, C, D, C11, C12, C66, B1, B2, B3, Rep, alpha, delta_chi, delta_smooth
  REAL (pr), DIMENSION(:), ALLOCATABLE :: Emn, STRAINmn 
  REAL (pr), DIMENSION(:,:), ALLOCATABLE :: Pold
  LOGICAL :: saveStrain, saveStrainRate, saveStress
  LOGICAL :: divergence_correction, divgrad
  INTEGER :: testmeth, len_strain
CONTAINS

  !
  ! In user_setup_pde() we setup how many variables are integrated, which are interpolated
  ! to the next times step and if any exeact solution exists to compare to during the run.
  ! We also set the variable names that are used when the result files are written.
  !
  ! The following variables must be setup in this routine:
  !
  !
  ! n_integrated     ! first n_integrated eqns will be acted on for time integration
  ! n_var_additional ! 
  ! n_var
  !
  !
  !
  !
  SUBROUTINE  user_setup_pde ( VERB ) 
    IMPLICIT NONE
    LOGICAL, OPTIONAL :: VERB         ! print debug info
    INTEGER :: i

    PRINT * ,''
    PRINT *, '**********************Setting up PDE*****************'
    PRINT * ,'CASE ISOTURB '
    PRINT *, '*****************************************************'

    len_strain = dim*(dim+1)/2
    ALLOCATE(n_var_Strain(len_strain), n_var_StrainRate(len_strain), n_var_Stress(len_strain))
    ALLOCATE(n_var_P(dim),n_var_U(dim),n_var_dU(dim))

    !------------ setting up default values 

    n_integrated = 3*dim ! Pi, Ui, dUi/dt 
 
    n_integrated = n_integrated + n_var_SGS ! adds additional equations for SGS model
    
    IF( hypermodel /= 0 ) THEN
       n_var_additional = n_var_additional + 1 ! phi at one time level
    ELSE
       n_var_additional = 1 ! phi at one time level
    END IF
    n_var_exact = 0 !--No exact solution 

    DO i = 1, dim
       n_var_P(i)  = i
       n_var_U(i)  = dim + i
       n_var_dU(i) = 2*dim + i
    END DO
    n_var_pressure  = n_integrated + 1 !pressure
    n_var_phi       = n_var_pressure

    IF( saveStrain ) THEN
      DO i = 1, len_strain
         n_var_Strain(i) = n_integrated + n_var_additional + i
      END DO
      n_var_additional = n_var_additional + len_strain
    END IF

    IF( saveStrainRate ) THEN
      DO i = 1, len_strain
         n_var_StrainRate(i) = n_integrated + n_var_additional + i
      END DO
      n_var_additional = n_var_additional + len_strain
    END IF

    IF( saveStress ) THEN
      DO i = 1, len_strain
         n_var_Stress(i) = n_integrated + n_var_additional + i
      END DO
      n_var_additional = n_var_additional + len_strain
    END IF

    n_var = n_integrated + n_var_additional !--Total number of variables

    !
    !--Allocate logical arrays 
    !  Must be done after setting number of different types of variables
    !  and before setting logical variable mapping arrays
    !
    CALL alloc_variable_mappings
    !
    ! Fill in variable names for default variables for integration meth 2
    ! These may be changed in the call to the user specified routine
    !  user_setup_pde()
    !
    ! In integrated variables (3D)

    WRITE (u_variable_names(1), u_variable_names_fmt) 'P1  '
    WRITE (u_variable_names(2), u_variable_names_fmt) 'P2  '
    WRITE (u_variable_names(3), u_variable_names_fmt) 'U1  '
    WRITE (u_variable_names(4), u_variable_names_fmt) 'U2  '
    WRITE (u_variable_names(5), u_variable_names_fmt) 'V1  '
    WRITE (u_variable_names(6), u_variable_names_fmt) 'V2  '
    WRITE (u_variable_names(7), u_variable_names_fmt) 'phi '
    IF( saveStrain ) THEN
       WRITE (u_variable_names(n_var_Strain(1)), u_variable_names_fmt) 'eps11'
       WRITE (u_variable_names(n_var_Strain(2)), u_variable_names_fmt) 'eps22'
       WRITE (u_variable_names(n_var_Strain(3)), u_variable_names_fmt) 'eps12'
    END IF
    IF( saveStrainRate ) THEN
       WRITE (u_variable_names(n_var_StrainRate(1)), u_variable_names_fmt) 'S11'
       WRITE (u_variable_names(n_var_StrainRate(2)), u_variable_names_fmt) 'S22'
       WRITE (u_variable_names(n_var_StrainRate(3)), u_variable_names_fmt) 'S12'
    END IF
    IF( saveStress ) THEN
       WRITE (u_variable_names(n_var_Stress(1)), u_variable_names_fmt) 'sigma11'
       WRITE (u_variable_names(n_var_Stress(2)), u_variable_names_fmt) 'sigma22'
       WRITE (u_variable_names(n_var_Stress(3)), u_variable_names_fmt) 'sigma12'
    END IF
    !
    ! setup logical variable mapping arrays
    ! This defined which variables are used for adaptation, Which need to be interpololated to the new grid at 
    ! each time step, which are saved, and for which we have an exect solutiuon we want to check at each time step
    !

    !
    ! setup which components we will base grid adaptation on.
    ! Adapt on the 3 velocity components
    !
    n_var_adapt = .FALSE. !--Initially adapt on integrated variables at first time level
    n_var_adapt(1:n_var_phi,0) = .TRUE. !--After first time step adapt on  velocity

    !eventually adapt to mdl variables    n_var_adapt(1:dim+2,1) = .TRUE. !--After first time step adapt on  velocity and Ilm and Imm

    n_var_adapt(1:n_var_phi,1) = .TRUE. !--After first time step adapt on  velocity

    !Variables that need to be interpoleted to new adapted grid in initial grid adaptation
    n_var_interpolate = .FALSE.
    n_var_interpolate(1:n_var_phi,0) = .TRUE. 

    !Variables that need to be interpoleted to new adapted grid at each time step
    n_var_interpolate(1:n_var_phi,1) = .TRUE. 

    n_var_interpolate(1:n_var,:) = .TRUE. 

    !
    ! setup which components we have an exact solution for

    n_var_exact_soln(:,0:1) = .FALSE.

!!$    IF(testmeth == 1) n_var_exact_soln(2,0:1) = .TRUE.

    !
    ! variables required for restart
    !
    n_var_req_restart = .FALSE.
    n_var_req_restart(1:n_var_phi)	        = .TRUE. !restart with velocities and pressure to begin with!

    ! no pressure for restart from initial file
    !n_var_req_restart(n_var_pressure ) = .TRUE.

    !
    ! setup which variables we will save the solution
    !
    n_var_save(1:n_var) = .TRUE. ! save all for restarting code

    !OPTIONAL: set up variables useed for calculation of numerical viscosity 
    !NOT SET UP yet for the case
    IF( hypermodel /= 0 ) THEN
       IF(ALLOCATED(n_var_hyper)) DEALLOCATE(n_var_hyper)
       ALLOCATE(n_var_hyper(1:n_var))
       n_var_hyper = .FALSE.
       n_var_hyper(1:2) = .TRUE.
       IF(ALLOCATED(n_var_hyper_active)) DEALLOCATE(n_var_hyper_active)
       ALLOCATE(n_var_hyper_active(1:n_integrated))
       n_var_hyper_active = .FALSE.
       n_var_hyper_active(1:2) = .TRUE.
    END IF

    !
    ! Set the maximum number of components for which we have an exact solution
    !
    n_var_exact = MAX( COUNT(n_var_exact_soln(:,0)),COUNT(n_var_exact_soln(:,1)) )


    !
	! Setup a scaleCoeff array of we want to tweak the scaling of a variable
	! ( if scaleCoeff < 1.0 then more points will be added to grid 
	!
    ALLOCATE ( scaleCoeff(1:n_var), Umn(1:n_var))
    scaleCoeff = 1.0_pr

    PRINT *, 'n_integrated = ',n_integrated 
    PRINT *, 'n_var = ',n_var 
    PRINT *, 'n_var_exact = ',n_var_exact 
    PRINT *, '*******************Variable Names*******************'
    DO i = 1,n_var
       WRITE (*, u_variable_names_fmt) u_variable_names(i)
    END DO
    PRINT *, '****************************************************'



  END SUBROUTINE  user_setup_pde

  !
  ! read variables from input file for this user case 
  !
  SUBROUTINE  user_read_input()
 	USE share_consts
	IMPLICIT NONE
    INTEGER:: i
    CHARACTER (LEN=4) :: id_string
   
  call input_real ('A',A,'stop','A: coefficient in definition of eta')

  call input_real ('B',B,'stop','B: coefficient in definition of eta')

  call input_real ('C',C,'stop','C: coefficient in definition of eta')

  call input_real ('D',D,'stop','D: coefficient in definition of eta coupling electric field')

  IF(ALLOCATED(Emn)) DEALLOCATE(Emn)
  ALLOCATE(Emn(dim))
  call input_real_vector ('Emn',Emn, dim,'stop', &
       'Emn: Mean electric field superimposed on perturbation')

  IF(ALLOCATED(STRAINmn)) DEALLOCATE(STRAINmn)
  ALLOCATE(STRAINmn(dim*(dim+1)/2))
  call input_real_vector ('STRAINmn',STRAINmn, dim*(dim+1)/2,'stop', &
       'STRAINmn: eps11, eps22, eps12 - mean strain')

  call input_real ('C11',C11,'stop','C11: sigma coefficient')

  call input_real ('C12',C12,'stop','C12: sigma coefficient')

  call input_real ('C66',C66,'stop','C66: sigma coefficient')

  call input_real ('B1',B1,'stop','B1: sigma coefficient')

  call input_real ('B2',B2,'stop','B2: sigma coefficient')

  call input_real ('B3',B3,'stop','B3: sigma coefficient')

  call input_real ('Rep',Rep,'stop','Rep:  polarization Reynolds number')

  call input_real ('alpha',alpha,'stop','alpha=(t_polarization/t_elastic)^2')

  call input_integer ('Jmode',Jmode,'stop','Jmode: level of random noise')

  saveStrain = .FALSE.
  call input_logical ('saveStrain',saveStrain,'default', &
                  '  if .TRUE. saves 3 cmponents of the strain tensor')

  saveStrainRate = .FALSE.
  call input_logical ('saveStrainRate',saveStrainRate,'default', &
                  '  if .TRUE. saves 3 cmponents of the strain rate tensor')

  saveStress = .FALSE.
  call input_logical ('saveStress',saveStress,'default', &
                  '  if .TRUE. saves 3 cmponents of the stress tensor')

  smooth_IC = .FALSE.
  call input_logical ('smooth_IC',smooth_IC,'default', &
                  '  if .TRUE. use smooth IC')

  delta_smooth =0.0_pr
  call input_real ('delta_smooth',delta_smooth,'default','delta_smooth: initial thickness of smoothed IC')

  divergence_correction = .FALSE. 
  call input_logical ('divergence_correction',divergence_correction,'default', &
       '  if T, additional projection is perfromed to ensure that the velcity on interpolated mesh is div-free')

  divgrad = .FALSE. 
  call input_logical ('divgrad',divgrad,'default', &
       ' if T, uses div(grad) instead of direct Laplacian for Darcy law')


  END SUBROUTINE  user_read_input

  !
  ! Set the exact solution for comparison to the simulated solution
  !
  ! u          - array to fill in the exact solution
  ! nlocal       - number of active wavelets   
  ! ne_local        - total number of equations
  ! t          - time of current time step 
  ! l_n_var_exact_soln_index - index into the elements of u for which we need to 
  !                            find the exact solution
  SUBROUTINE  user_exact_soln (u, nlocal,  t_local, l_n_var_exact_soln)  
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: nlocal
    REAL (pr), INTENT (IN) ::  t_local
    REAL (pr), DIMENSION (nlocal,n_var_exact), INTENT (INOUT) :: u
    LOGICAL , INTENT (IN) :: l_n_var_exact_soln(n_var)


    ! There is no exact solution
!!$    IF(testmeth == 1) u(:,2)= 0.5_pr*(1.0_pr-SIGN(1.0_pr,SUM(x(:,:)*2,DIM=2)*EXP(-t_local)-R0**2))

  END SUBROUTINE  user_exact_soln

  SUBROUTINE user_initial_conditions (u, nlocal, ne_local, t_local, scl, scl_fltwt, iter)
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: nlocal, ne_local
    INTEGER  , INTENT (INOUT) :: iter ! iteration of call while adapting initial grid
    REAL (pr), DIMENSION (nlocal,ne_local), INTENT (INOUT) :: u
    REAL (pr), DIMENSION (nlocal,ne_local) :: utmp
    REAL (pr)  :: scl(1:n_var),scl_fltwt
    REAL (pr), DIMENSION(ne_local) :: scl_u
    REAL (pr), INTENT (IN) :: t_local
    REAL (pr) :: Amode, phase
    INTEGER, DIMENSION(1) :: SEED
    INTEGER :: i, ii, ie, idim, ixyz(dim), i_p_mode(0:dim)

    IF(dim /= 2) THEN
       PRINT *, 'This case is only set up for dim=2'
       STOP
    END IF
    pi = 2.0_pr*ASIN(1.0_pr)
    scl_u = 1.0_pr

    u = 0.0_pr

!    u(:,2) = user_chi (nwlt, t_local) 

    i_p_mode(0) = 1
    DO i=1,dim
       i_p_mode(i) = i_p_mode(i-1)*2**(Jmode-1)
    END DO
    SEED(1) = 54321
    CALL RANDOM_SEED(PUT=SEED(1:1))
    DO ie = 1, dim
       DO ii = 1, i_p_mode(dim)
          ixyz(1:dim) = INT(MOD(ii-1,i_p_mode(1:dim))/i_p_mode(0:dim-1))+1 
          CALL RANDOM_NUMBER(Amode)
          Amode = (2.0_pr*Amode-1.0_pr)!*EXP(-0.2_pr*SQRT(REAL(SUM(ixyz(1:dim)**2),pr)))
          phase = 0.0_pr
          IF(MINVAL(prd) == 1) THEN
             CALL RANDOM_NUMBER(phase)
             phase = 2.0_pr*pi*phase
          END IF
          utmp(:,n_var_P(ie))=Amode
          DO idim=1,dim
             utmp(:,n_var_P(ie))=utmp(:,n_var_P(ie))*cos(REAL(1+MINVAL(prd),pr)*pi/(xyzlimits(2,idim)-xyzlimits(1,idim))*ixyz(idim)*(x(:,idim)-xyzlimits(1,idim))+phase)
          END DO
          u(:,n_var_P(ie))=u(:,ie)+utmp(:,n_var_P(ie))
       END DO
    END DO
    u(:,n_var_P(1:dim)) =SIGN(1.0_pr,u(:,n_var_P(1:dim)))

!!$    u(:,n_var_P(1))=0.0_pr
!!$    u(:,n_var_P(2)) =SIGN(1.0_pr,x(:,1)-0.5_pr*(xyzlimits(2,1)+xyzlimits(1,1)))

!!$    u(:,n_var_P(1)) = 0.5_pr*(1.0_pr-SIGN(1.0_pr,x(:,1)-x(:,2)+0.1_pr))
!!$    u(:,n_var_P(2)) =-0.5_pr*(1.0_pr+SIGN(1.0_pr,x(:,1)-x(:,2)+0.1_pr))
   
    u(:,n_var_U(1:dim)) = 0.0_pr ! displacement and displacemnt rate

    u(:,n_var_dU(1:dim)) = 0.0_pr ! displacement and displacemnt rate

    IF(smooth_IC .AND. j_lev == j_mx) THEN
     ! CALL diffsmooth (smoothu,nlocal,ne_local,methdiff,IDdiff,diffdir,Dfac_loc,delta_loc,deltadel_loc,j_sm_in,j_st_in,BCtype,boundit)
       CALL diffsmooth (u(:,1:dim),nlocal,dim,LOW_ORDER,11,0,0.5_pr,delta_smooth,4.0_pr,-3,j_lev,3,.FALSE.)
    END IF

  END SUBROUTINE user_initial_conditions

!*********************************************************
  ! Arguments
  ! u         - field on adaptive grid
  ! nlocal      - number of active points
  ! ne_local       - number of equations
  ! t         - current time
  !
  ! Global variables used
  ! ifn       - number of equations
  ! x()
  ! xO
  ! y0
  !--******************************** 
  SUBROUTINE user_algebraic_BC (Lu, u, nlocal, ne_local, jlev, meth)
    !--Defines boundary condition type
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: jlev, meth, ne_local, nlocal
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (INOUT) :: Lu
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (IN)    :: u
    REAL (pr), DIMENSION (ne_local, nlocal, dim) :: du
    REAL (pr), DIMENSION (ne_local, nlocal, dim) :: d2u
    REAL (pr), DIMENSION(ng,len_strain) :: sigma, strain

    INTEGER :: i, j, k, ie, ii, shift
    !REAL (pr), DIMENSION (ne_nlocal,nlocal,dim) :: du, d2u !uncomment if Neuman BC are used, need to call 
    INTEGER :: face_type, nloc, meth_local
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc

    !BC for both 2D and 3D cases

    !if Neuman BC are used, need to call 
    CALL c_diff_fast (u, du, d2u, jlev, nlocal, meth, 10, ne_local, 1, ne_local)

!!$    k = 0
!!$    DO i = 1, dim
!!$       strain(:,i) = du(n_var_U(i),:,i) ! s_ii
!!$       DO j = i+1, dim
!!$          k= k + 1
!!$          strain(:,dim+k) = 0.5_pr * ( du(n_var_U(i),:,j) + du(n_var_U(j),:,i) ) ! 0.5* (dui/dxj + dui/dxi)
!!$       END DO
!!$    END DO

    DO ie = 1, ne_local
       shift = nlocal*(ie-1)
       !--Go through all Boundary points that are specified
       i_p_face(0) = 1
       DO i=1,dim
          i_p_face(i) = i_p_face(i-1)*3
       END DO
       DO face_type = 0, 3**dim - 1
          face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
          IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
             CALL get_all_indices_by_face (face_type, jlev, nloc, iloc)
             IF(nloc > 0 ) THEN 
                IF(ie <= dim) THEN
                   Lu(shift+iloc(1:nloc)) = 0.0_pr  
                   DO i = 1,dim
                      Lu(shift+iloc(1:nloc)) =   Lu(shift+iloc(1:nloc)) - face(i)*du(ie,iloc(1:nloc),i) !Neuman BC
                   END DO
                ELSE IF(ABS(face(1))== 1  .AND. face(2)  == 0 ) THEN !vertical edges 
                   IF(ie == n_var_dU(1)) Lu(shift+iloc(1:nloc)) = C11*du(n_var_U(1),:,1)+C12*du(n_var_U(2),:,2)
                   IF(ie == n_var_dU(2)) Lu(shift+iloc(1:nloc)) = 0.5_pr*C66*(du(n_var_U(1),:,2)+du(n_var_U(2),:,1))
                ELSE IF(face(1) == 0  .AND. ABS(face(2)) == 1 ) THEN !horizontal edges
                   IF(ie == n_var_dU(1)) Lu(shift+iloc(1:nloc)) = 0.5_pr*C66*(du(n_var_U(1),:,2)+du(n_var_U(2),:,1))
                   IF(ie == n_var_dU(2)) Lu(shift+iloc(1:nloc)) = C11*du(n_var_U(2),:,2)+C12*du(n_var_U(1),:,1)
                ELSE IF(face(1) == -1  .AND. face(2) == -1 ) THEN !bottom left corner
                  IF (ie == n_var_U(1) .OR. ie == n_var_dU(1) )  Lu(shift+iloc(1:nloc)) = u(shift+iloc(1:nloc))  !Dirichlet conditions
                  IF (ie == n_var_U(2) .OR. ie == n_var_dU(2) )  Lu(shift+iloc(1:nloc)) = u(shift+iloc(1:nloc))  !Dirichlet conditions
                ELSE IF(face(1) ==  1  .AND. face(2) == -1 ) THEN !bottom right corner
                  IF (ie == n_var_U(2) .OR. ie == n_var_dU(2) )  Lu(shift+iloc(1:nloc)) = u(shift+iloc(1:nloc))  !Dirichlet conditions
                  IF(ie == n_var_dU(1)) Lu(shift+iloc(1:nloc)) = 0.5_pr*C66*(du(n_var_U(1),:,2)+du(n_var_U(2),:,1))
                ELSE IF(face(1) == -1  .AND. face(2) ==  1 ) THEN !top left corner
                  IF(ie == n_var_U(1) .OR. ie == n_var_dU(1) )  Lu(shift+iloc(1:nloc)) = u(shift+iloc(1:nloc))  !Dirichlet conditions
                  IF(ie == n_var_dU(2)) Lu(shift+iloc(1:nloc)) = 0.5_pr*C66*(du(n_var_U(1),:,2)+du(n_var_U(2),:,1))
                ELSE IF(face(1) ==  1  .AND. face(2) ==  1 ) THEN !top right corner
                   IF(ie == n_var_dU(1)) Lu(shift+iloc(1:nloc)) = C11*du(n_var_U(1),:,1)+C12*du(n_var_U(2),:,2)
                   IF(ie == n_var_dU(2)) Lu(shift+iloc(1:nloc)) = C11*du(n_var_U(2),:,2)+C12*du(n_var_U(1),:,1)
                END IF 
             END IF
          END IF
       END DO
    END DO

  END SUBROUTINE user_algebraic_BC

  SUBROUTINE user_algebraic_BC_diag (Lu_diag, nlocal, ne_local, jlev, meth)
    !--Defines boundary condition type
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: jlev, meth, ne_local, nlocal
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (INOUT) :: Lu_diag
    REAL (pr), DIMENSION (nlocal,dim) :: du, d2u

    INTEGER :: i, ie, ii, shift
    ! REAL (pr), DIMENSION (nlocal,dim) :: du, d2u  !uncomment if Neuman BC are used
    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc

    !BC for both 2D and 3D cases

    !if Neuman BC are used, need to call 
    CALL c_diff_diag ( du, d2u, jlev, nlocal, meth, meth, 10)

    DO ie = 1, ne_local
       shift = nlocal*(ie-1)
       !--Go through all Boundary points that are specified
       i_p_face(0) = 1
       DO i=1,dim
          i_p_face(i) = i_p_face(i-1)*3
       END DO
       DO face_type = 0, 3**dim - 1
          face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
          IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
             CALL get_all_indices_by_face (face_type, jlev, nloc, iloc)
             IF(nloc > 0 ) THEN
                IF(ie <= 2) THEN
                   Lu_diag(shift+iloc(1:nloc)) = 0.0_pr  
                   DO i = 1,dim
                      Lu_diag(shift+iloc(1:nloc)) =   Lu_diag(shift+iloc(1:nloc)) - face(i)*du(iloc(1:nloc),i) !Neuman BC
                   END DO
                ELSE IF(ABS(face(1))== 1  .AND. face(2)  == 0 ) THEN !vertical edges 
                   IF(ie == n_var_dU(1)) Lu_diag(shift+iloc(1:nloc)) = C11+C12
                   IF(ie == n_var_dU(2)) Lu_diag(shift+iloc(1:nloc)) = C66
                ELSE IF(face(1) == 0  .AND. ABS(face(2)) == 1 ) THEN !horizontal edges
                   IF(ie == n_var_dU(1)) Lu_diag(shift+iloc(1:nloc)) = C66
                   IF(ie == n_var_dU(2)) Lu_diag(shift+iloc(1:nloc)) = C11+C12
                ELSE IF(face(1) == -1  .AND. face(2) == -1 ) THEN !bottom left corner
                  IF (ie == n_var_U(1) .OR. ie == n_var_dU(1) )  Lu_diag(shift+iloc(1:nloc)) = 1.0_pr            ! Dirichlet conditions
                  IF (ie == n_var_U(2) .OR. ie == n_var_dU(2) )  Lu_diag(shift+iloc(1:nloc)) = 1.0_pr            ! Dirichlet conditions
                ELSE IF(face(1) ==  1  .AND. face(2) == -1 ) THEN !bottom right corner
                  IF (ie == n_var_U(2) .OR. ie == n_var_dU(2) )  Lu_diag(shift+iloc(1:nloc)) = 1.0_pr            ! Dirichlet conditions
                  IF(ie == n_var_dU(1)) Lu_diag(shift+iloc(1:nloc)) = C66
                ELSE IF(face(1) == -1  .AND. face(2) ==  1 ) THEN !top left corner
                  IF(ie == n_var_U(1) .OR. ie == n_var_dU(1) )  Lu_diag(shift+iloc(1:nloc)) = 1.0_pr            ! Dirichlet conditions
                  IF(ie == n_var_dU(2)) Lu_diag(shift+iloc(1:nloc)) = C66
                ELSE IF(face(1) ==  1  .AND. face(2) ==  1 ) THEN !top right corner
                   IF(ie == n_var_dU(1)) Lu_diag(shift+iloc(1:nloc)) = C11+C12
                   IF(ie == n_var_dU(2)) Lu_diag(shift+iloc(1:nloc)) = C11+C12
                END IF 
             END IF
          END IF
       END DO
    END DO

  END SUBROUTINE user_algebraic_BC_diag

  SUBROUTINE user_algebraic_BC_rhs (rhs, ne_local, nlocal, jlev)
    !--Sets rhs for boundary conditions    IMPLICIT NONE
    INTEGER , INTENT (IN) :: ne_local, nlocal, jlev
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (INOUT) :: rhs

    INTEGER :: i, ie, ii, shift
    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc

    !BC for both 2D and 3D cases

    DO ie = 1, ne_local
       shift = nlocal*(ie-1)
       !--Go through all Boundary points that are specified
       i_p_face(0) = 1
       DO i=1,dim
          i_p_face(i) = i_p_face(i-1)*3
       END DO
       DO face_type = 0, 3**dim - 1
          face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
          IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
             CALL get_all_indices_by_face (face_type, jlev, nloc, iloc)
             IF(nloc > 0 ) THEN 
                IF(ie <= 2) THEN
                   DO i = 1,dim
                      rhs(shift+iloc(1:nloc)) = 0.0_pr     !Neuman BC
                   END DO
                ELSE IF(ABS(face(1))== 1  .AND. face(2)  == 0 ) THEN !vertical edges 
                   IF(ie == n_var_dU(1)) rhs(shift+iloc(1:nloc)) = 0.5_pr*(B1*Pold(iloc(1:nloc),1)**2+B2*Pold(iloc(1:nloc),2)**2)
                   IF(ie == n_var_dU(2)) rhs(shift+iloc(1:nloc)) = B3*Pold(iloc(1:nloc),1)*Pold(iloc(1:nloc),2)
                ELSE IF(face(1) == 0  .AND. ABS(face(2)) == 1 ) THEN !horizontal edges
                   IF(ie == n_var_dU(1)) rhs(shift+iloc(1:nloc)) = B3*Pold(iloc(1:nloc),1)*Pold(iloc(1:nloc),2)
                   IF(ie == n_var_dU(2)) rhs(shift+iloc(1:nloc)) = 0.5_pr*(B2*Pold(iloc(1:nloc),1)**2+B1*Pold(iloc(1:nloc),2)**2)
                ELSE IF(face(1) == -1  .AND. face(2) == -1 ) THEN !bottom left corner
                  IF (ie == n_var_U(1) .OR. ie == n_var_dU(1) )  rhs(shift+iloc(1:nloc)) = 0.0_pr
                  IF (ie == n_var_U(2) .OR. ie == n_var_dU(2) )  rhs(shift+iloc(1:nloc)) = 0.0_pr
                ELSE IF(face(1) ==  1  .AND. face(2) == -1 ) THEN !bottom right corner
                  IF (ie == n_var_U(2) .OR. ie == n_var_dU(2) )  rhs(shift+iloc(1:nloc)) = 0.0_pr
                  IF(ie == n_var_dU(1)) rhs(shift+iloc(1:nloc)) = B3*Pold(iloc(1:nloc),1)*Pold(iloc(1:nloc),2)
                ELSE IF(face(1) == -1  .AND. face(2) ==  1 ) THEN !top left corner
                  IF(ie == n_var_U(1) .OR. ie == n_var_dU(1) )  rhs(shift+iloc(1:nloc)) = 0.0_pr
                  IF(ie == n_var_dU(2)) rhs(shift+iloc(1:nloc)) = B3*Pold(iloc(1:nloc),1)*Pold(iloc(1:nloc),2)
                ELSE IF(face(1) ==  1  .AND. face(2) ==  1 ) THEN !top right corner
                   IF(ie == n_var_dU(1)) rhs(shift+iloc(1:nloc)) = 0.5_pr*(B1*Pold(iloc(1:nloc),1)**2+B2*Pold(iloc(1:nloc),2)**2)
                   IF(ie == n_var_dU(2)) rhs(shift+iloc(1:nloc)) = 0.5_pr*(B2*Pold(iloc(1:nloc),1)**2+B1*Pold(iloc(1:nloc),2)**2)
                END IF 
             END IF
          END IF
       END DO
    END DO

  END SUBROUTINE user_algebraic_BC_rhs

  SUBROUTINE user_project (u, phi, nlocal, meth)
    !--Makes u divergence free
    IMPLICIT NONE
    INTEGER, PARAMETER :: ne_local = 1
    INTEGER, INTENT (IN) :: meth, nlocal
    INTEGER :: meth_central, meth_backward, meth_forward, i  
    REAL (pr), DIMENSION (nlocal),     INTENT(INOUT) :: phi
    REAL (pr), DIMENSION (nlocal,n_integrated), INTENT(INOUT) :: u
    REAL (pr), DIMENSION (nlocal,ne_local) :: dphi, f
    REAL (pr), DIMENSION(ne_local) :: scl_phi
    INTEGER, DIMENSION(ne_local) :: clip 

    meth_central  = meth + BIASING_NONE
    meth_backward = meth + BIASING_BACKWARD
    meth_forward  = meth + BIASING_FORWARD

    !--Solve for phi and update polarization vector
    dphi(:,1) = phi(:) 
    f = Laplace_rhs (u(:,1:dim), nlocal, dim, meth)
    clip = MINVAL(prd(1:dim))

    scl_phi = MAX(scl_global(n_var_phi),MAXVAL(scl_global(n_var_P(1:dim)))) !scale of phi increment based on dynamic pressure
    PRINT *, 'user_project: scl_phi=',scl_phi
    CALL Linsolve (phi, f , tol2, nlocal, ne_local, clip, Laplace, Laplace_diag, SCL=scl_phi) !update phi with scaling 
    !CALL Linsolve (phi, f , tol2, nlocal, ne_local, clip, Laplace, Laplace_diag)              !update phi without scaling

    dphi(:,1) = phi(:)-dphi(:,1)
    u(:,n_var_P(1:dim)) = u(:,n_var_P(1:dim)) - 0.5_pr*D*dt*grad(dphi, nlocal, j_lev, meth_central) !update pollarization vector

  END SUBROUTINE user_project

  FUNCTION Laplace (jlev, u, nlocal, ne_local, meth_in)
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: jlev, nlocal, ne_local, meth_in
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (INOUT) :: u
    REAL (pr), DIMENSION (nlocal*ne_local) :: Laplace

    INTEGER :: i, ii, ie, ie1, meth, idim, shift
    INTEGER :: meth_central, meth_backward, meth_forward 
    REAL (pr), DIMENSION (dim, nlocal, dim) :: du, d2u, du_dummy
    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc

    meth_central  = meth_in + BIASING_NONE
    meth_backward = meth_in + BIASING_BACKWARD
    meth_forward  = meth_in + BIASING_FORWARD

    IF(divgrad) THEN
       CALL c_diff_fast (u, du(1:ne_local, 1:nlocal, 1:dim), d2u(1:ne_local, 1:nlocal, 1:dim), jlev, nlocal, meth_backward, 10, ne_local, 1, ne_local)
    ELSE
       CALL c_diff_fast (u, du(1:ne_local, 1:nlocal, 1:dim), d2u(1:ne_local, 1:nlocal, 1:dim), jlev, nlocal, meth_central, 01, ne_local, 1, ne_local)
    END IF
    DO ie = 1, ne_local
       shift=(ie-1)*nlocal
       Laplace(shift+1:shift+Nwlt_lev(jlev,0)) = 0.0_pr
       IF(divgrad) THEN
          !--- Internal points
          CALL c_diff_fast(du(ie,1:nlocal,1:dim), d2u(1:dim, 1:nlocal, 1:dim), du_dummy(1:dim, 1:nlocal, 1:dim), &
               jlev, nlocal, meth_forward, 10, dim, 1, dim )
          !--- div(grad)
          DO idim = 1,dim
             Laplace(shift+1:shift+Nwlt_lev(jlev,0)) = Laplace(shift+1:shift+Nwlt_lev(jlev,0)) &
                                                     + d2u(idim ,1:Nwlt_lev(jlev,0),idim)
          END DO
       ELSE
          DO idim = 1,dim
             Laplace(shift+1:shift+Nwlt_lev(jlev,0)) = Laplace(shift+1:shift+Nwlt_lev(jlev,0))  &
                                                     + d2u(ie ,1:Nwlt_lev(jlev,0),idim)
          END DO
       END IF
       !--Boundary points
       i_p_face(0) = 1
       DO i=1,dim
          i_p_face(i) = i_p_face(i-1)*3
       END DO
       DO face_type = 0, 3**dim - 1
          face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
          IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
             CALL get_all_indices_by_face (face_type, jlev, nloc, iloc)
             IF(nloc > 0 ) THEN ! ANY dimension
                Laplace(shift+iloc(1:nloc)) = u(shift+iloc(1:nloc))  !Dirichlet conditions
             END IF
          END IF
       END DO
    END DO

  END FUNCTION Laplace

  FUNCTION Laplace_diag (jlev,  nlocal, ne_local, meth_in)
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: jlev, nlocal, ne_local, meth_in
    REAL (pr), DIMENSION (nlocal*ne_local) :: Laplace_diag

    INTEGER :: i, ii, ie, ie1, idim, shift
    INTEGER :: meth_central, meth_backward, meth_forward 
    REAL (pr), DIMENSION (nlocal,dim) :: du, d2u
    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc

    meth_central  = meth_in + BIASING_NONE
    meth_backward = meth_in + BIASING_BACKWARD
    meth_forward  = meth_in + BIASING_FORWARD

    IF(divgrad) THEN
       CALL c_diff_diag ( du, d2u, jlev, nlocal, meth_backward, meth_forward, -01)
    ELSE
       CALL c_diff_diag ( du, d2u, jlev, nlocal, meth_central, meth_central, 01)
    END IF
    !PRINT *,'IN Laplace_diag, ne_local = ', ne_local

    DO ie = 1, ne_local
       shift=(ie-1)*nlocal
       Laplace_diag(shift+1:shift+Nwlt_lev(jlev,0)) = 0.0_pr
       DO idim = 1,dim
          Laplace_diag(shift+1:shift+Nwlt_lev(jlev,0)) = Laplace_diag(shift+1:shift+Nwlt_lev(jlev,0))  &
                                                       + d2u(1:Nwlt_lev(jlev,0),idim)
       END DO
       !--Boundary points
       i_p_face(0) = 1
       DO i=1,dim
          i_p_face(i) = i_p_face(i-1)*3
       END DO
       DO face_type = 0, 3**dim - 1
          face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
          IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
             CALL get_all_indices_by_face (face_type, jlev, nloc, iloc)
             IF(nloc > 0 ) THEN ! ANY dimension
                Laplace_diag(shift+iloc(1:nloc)) = 1.0_pr  !Dirichlet conditions
             END IF
          END IF
       END DO
    END DO

   END FUNCTION Laplace_diag

  FUNCTION Laplace_rhs(u, nlocal, ne_local, meth_in)
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: nlocal, ne_local, meth_in
    REAL (pr), DIMENSION (nlocal,ne_local), INTENT (INOUT) :: u
    REAL (pr), DIMENSION (nlocal,ne_local) :: Laplace_rhs

    INTEGER :: i, ii, ie, meth, idim
    INTEGER :: meth_central, meth_backward, meth_forward 
    REAL (pr), DIMENSION (ne_local ,nlocal,dim) :: du
    REAL (pr), DIMENSION (ne_local*dim ,nlocal,dim) :: d2u
    REAL (pr), DIMENSION (ne_local*dim ,nlocal,dim) :: du_dummy ! passed when only calculating 1st derivative.
    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc

    meth_central  = meth_in + BIASING_NONE
    meth_backward = meth_in + BIASING_BACKWARD
    meth_forward  = meth_in + BIASING_FORWARD

    Laplace_rhs(:,1) = div(u,nlocal,j_lev,meth_central)

    !--Boundary points
    i_p_face(0) = 1
    DO i=1,dim
       i_p_face(i) = i_p_face(i-1)*3
    END DO
    DO face_type = 0, 3**dim - 1
       face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
       IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
          CALL get_all_indices_by_face (face_type, j_lev, nloc, iloc)
          IF(nloc > 0 ) THEN 
             Laplace_rhs(iloc(1:nloc),:) = 0.0_pr !Dirichlet conditions
          END IF
       END IF
    END DO
    
  END FUNCTION Laplace_rhs


  FUNCTION user_rhs (u_integrated,phi)
    USE penalization
    IMPLICIT NONE
    REAL (pr), DIMENSION (ng,ne), INTENT(IN) :: u_integrated !1D flat version of integrated variables without BC 
    REAL (pr), DIMENSION (ng), INTENT(IN) :: phi
    REAL (pr), DIMENSION (n) :: user_rhs

    INTEGER :: ie, ie1, shift, i, j, k
    INTEGER, PARAMETER :: meth=1
    REAL (pr), DIMENSION (ne,ng,dim) :: du, d2u
    REAL (pr), DIMENSION (ng,dim)     :: dphi
    REAL (pr), DIMENSION(ng,len_strain) :: sigma, strain

    INTEGER :: face_type, nloc, meth_local
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc
    INTEGER :: meth_central, meth_backward, meth_forward 
    
    meth_central  = meth + BIASING_NONE
    meth_backward = meth + BIASING_BACKWARD
    meth_forward  = meth + BIASING_FORWARD
    
    dphi(1:ng,1:dim) = grad (phi, ng, j_lev,  meth_central)

    CALL c_diff_fast(u_integrated, du, d2u, j_lev, ng, meth_central, 11, ne, 1, ne) !derivatives are calculated even for SGS terms

    k = 0
    DO i = 1, dim
       strain(:,i) = du(n_var_U(i),:,i) ! s_ii
       DO j = i+1, dim
          k= k + 1
          strain(:,dim+k) = 0.5_pr * ( du(n_var_U(i),:,j) + du(n_var_U(j),:,i) ) ! 0.5* (dui/dxj + dui/dxi)
       END DO
    END DO

    !--Form right hand side of evolution equations
    DO ie = 1, dim
       shift=(ie-1)*ng
       user_rhs(shift+1:shift+ng) = SUM(d2u(ie,:,1:dim),DIM=2)/Rep - (A + C*SUM(u_integrated(:,1:dim)**2,DIM=2) + (B-C)*u_integrated(:,ie)**2)*u_integrated(:,ie)  &
                                  - D*dphi(:,ie) + Emn(ie) 
    END DO
    shift=(n_var_P(1)-1)*ng
    user_rhs(shift+1:shift+ng) = user_rhs(shift+1:shift+ng) + B1*(STRAINmn(1)+strain(:,1))*u(:,n_var_P(1)) + B2*(STRAINmn(2)+strain(:,2))*u(:,n_var_P(1)) + B3*(STRAINmn(3)+strain(:,3))*u(:,n_var_P(2))
    shift=(n_var_P(2)-1)*ng
    user_rhs(shift+1:shift+ng) = user_rhs(shift+1:shift+ng) + B1*(STRAINmn(2)+strain(:,2))*u(:,n_var_P(2)) + B2*(STRAINmn(1)+strain(:,1))*u(:,n_var_P(2)) + B3*(STRAINmn(3)+strain(:,3))*u(:,n_var_P(1))

    DO ie = 1, dim
       shift=(n_var_U(ie)-1)*ng
       user_rhs(shift+1:shift+ng) = alpha*u_integrated(:,n_var_dU(ie))
    END DO

    sigma(:,1) = C11*strain(:,1)+C12*strain(:,2)-0.5_pr*(B1*u(:,n_var_P(1))**2+B2*u(:,n_var_P(2))**2)
    sigma(:,2) = C12*strain(:,1)+C11*strain(:,2)-0.5_pr*(B1*u(:,n_var_P(2))**2+B2*u(:,n_var_P(1))**2)
    sigma(:,3) = C66*strain(:,3)-B3*u(:,n_var_P(1))*u(:,n_var_P(2))

    CALL c_diff_fast(sigma(:,1:len_strain), du(1:len_strain,:,1:dim), d2u(1:len_strain,:,1:dim), j_lev, ng, meth_central, 10, len_strain, 1, len_strain)

    shift=(n_var_dU(1)-1)*ng
    user_rhs(shift+1:shift+ng) = alpha*(du(1,:,1) + du(3,:,2))
    shift=(n_var_dU(2)-1)*ng
    user_rhs(shift+1:shift+ng) = alpha*(du(3,:,1) + du(2,:,2))

    IF( hypermodel /= 0 ) CALL hyperbolic(u_integrated, ng, user_rhs)

    !---------------- Algebraic BC are set up in user_algebraic_BC and they autmatically overwrite evolution BC

  END FUNCTION user_rhs


  ! find Jacobian of Right Hand Side of the problem
  ! u_prev == u_prev_timestep_loc

  FUNCTION user_Drhs (u, u_prev, meth)
    USE penalization
    IMPLICIT NONE
    INTEGER, INTENT(IN) ::  meth
    !u_prev_timestep passed local to cast it into 2dim array to work with vector derivatives
    REAL (pr), DIMENSION (ng,ne) :: u, u_prev
    REAL (pr), DIMENSION (n) :: user_Drhs
    REAL (pr), DIMENSION(ng,len_strain) :: sigma, strain


    INTEGER :: i, j, k, ie, ie1, shift
    REAL (pr), DIMENSION (ne,ng,dim) :: du, d2u
    REAL (pr), DIMENSION (ne,ng,dim) :: du_prev, d2u_prev
    REAL (pr), DIMENSION (ng,dim)     :: dp

    INTEGER :: face_type, nloc, meth_local
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc
    INTEGER :: meth_central, meth_backward, meth_forward 

    meth_central  = meth + BIASING_NONE
    meth_backward = meth + BIASING_BACKWARD
    meth_forward  = meth + BIASING_FORWARD
    
    ! find 1st and 2nd deriviative of u and
    CALL c_diff_fast(u, du, d2u, j_lev, ng, meth_central, 11, ne, 1, ne) 
    
    ! find only first deriviativ e  u_prev_timestep
    CALL c_diff_fast(u_prev, du_prev, d2u_prev, j_lev, ng, meth, 10, ne , 1, ne )

    !--Form right hand side of evolution equations
    DO ie = 1, dim
       shift=(ie-1)*ng
       user_Drhs(shift+1:shift+ng) = SUM(d2u(ie,:,1:dim),DIM=2)/Rep - (A + C*SUM(u_prev(:,1:dim)**2,DIM=2) +(B-C)*u_prev(:,ie)**2)*u(:,ie) &
                                                              - (2.0_pr*C*SUM(u_prev(:,1:dim)*u(:,1:dim),DIM=2) + 2.0_pr*(B-C)*u_prev(:,ie)*u(:,ie))*u_prev(:,ie)
     END DO
     !strain based on u
     k = 0
     DO i = 1, dim
        strain(:,i) = du(n_var_U(i),:,i) ! s_ii
        DO j = i+1, dim
           k= k + 1
           strain(:,dim+k) = 0.5_pr * ( du(n_var_U(i),:,j) + du(n_var_U(j),:,i) ) ! 0.5* (dui/dxj + dui/dxi)
        END DO
    END DO
    sigma(:,1) = C11*strain(:,1)+C12*strain(:,2)-(B1*u_prev(:,n_var_P(1))*u(:,n_var_P(1))+B2*u_prev(:,n_var_P(2))*u(:,n_var_P(2)))
    sigma(:,2) = C12*strain(:,1)+C11*strain(:,2)-(B1*u_prev(:,n_var_P(2))*u(:,n_var_P(2))+B2*u_prev(:,n_var_P(1))*u(:,n_var_P(1)))
    sigma(:,3) = C66*strain(:,3)-B3*(u_prev(:,n_var_P(1))*u(:,n_var_P(2))+u(:,n_var_P(1))*u_prev(:,n_var_P(2)))

    shift=(n_var_P(1)-1)*ng
    user_Drhs(shift+1:shift+ng) = user_Drhs(shift+1:shift+ng) + B1*strain(:,1)*u_prev(:,n_var_P(1)) + B2*strain(:,2)*u_prev(:,n_var_P(1)) + B3*strain(:,3)*u_prev(:,n_var_P(2))
    shift=(n_var_P(2)-1)*ng
    user_Drhs(shift+1:shift+ng) = user_Drhs(shift+1:shift+ng) + B1*strain(:,2)*u_prev(:,n_var_P(2)) + B2*strain(:,1)*u_prev(:,n_var_P(2)) + B3*strain(:,3)*u_prev(:,n_var_P(1))

    !strain based on u_prev
    k = 0
    DO i = 1, dim
       strain(:,i) = du_prev(n_var_U(i),:,i) ! s_ii
       DO j = i+1, dim
          k= k + 1
          strain(:,dim+k) = 0.5_pr * ( du_prev(n_var_U(i),:,j) + du_prev(n_var_U(j),:,i) ) ! 0.5* (dui/dxj + dui/dxi)
       END DO
    END DO

    shift=(n_var_P(1)-1)*ng
    user_Drhs(shift+1:shift+ng) = user_Drhs(shift+1:shift+ng) + B1*(STRAINmn(1)+strain(:,1))*u(:,n_var_P(1)) + B2*(STRAINmn(2)+strain(:,2))*u(:,n_var_P(1)) + B3*(STRAINmn(3)+strain(:,3))*u(:,n_var_P(2))
    shift=(n_var_P(2)-1)*ng
    user_Drhs(shift+1:shift+ng) = user_Drhs(shift+1:shift+ng) + B1*(STRAINmn(2)+strain(:,2))*u(:,n_var_P(2)) + B2*(STRAINmn(1)+strain(:,1))*u(:,n_var_P(2)) + B3*(STRAINmn(3)+strain(:,3))*u(:,n_var_P(1))

    DO ie = 1, dim
       shift=(n_var_U(ie)-1)*ng
       user_Drhs(shift+1:shift+ng) = alpha*u(:,n_var_dU(ie))
    END DO

    CALL c_diff_fast(sigma(:,1:len_strain), du(1:len_strain,:,1:dim), d2u(1:len_strain,:,1:dim), j_lev, ng, meth_central, 10, len_strain, 1, len_strain)

    shift=(n_var_dU(1)-1)*ng
    user_Drhs(shift+1:shift+ng) = alpha*(du(1,:,1) + du(3,:,2))
    shift=(n_var_dU(2)-1)*ng
    user_Drhs(shift+1:shift+ng) = alpha*(du(3,:,1) + du(2,:,2))

    IF( hypermodel /= 0 ) CALL hyperbolic(u, ng, user_Drhs)

  END FUNCTION user_Drhs


  !
  ! Uses u_prev_timestep, which is global flat (1D ) array set in time_step_cn()
  !
  FUNCTION user_Drhs_diag (meth)
    USE penalization
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: meth
    REAL (pr), DIMENSION (n) :: user_Drhs_diag

    INTEGER :: i, j, k, ie, ie1, shift,shiftIlm,shiftImm
    REAL (pr), DIMENSION (ng,dim) :: du, d2u
    REAL (pr), DIMENSION (ne,ng,dim) :: du_prev, d2u_prev
    REAL (pr), DIMENSION(ng,len_strain) :: sigma, strain

    INTEGER :: face_type, nloc, meth_local
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc
    INTEGER :: meth_central, meth_backward, meth_forward 

    meth_central  = meth + BIASING_NONE
    meth_backward = meth + BIASING_BACKWARD
    meth_forward  = meth + BIASING_FORWARD

    ! find only first deriviativ e  u_prev_timestep
    CALL c_diff_fast(u_prev_timestep, du_prev, d2u_prev, j_lev, ng, meth, 10, ne , 1, ne )

     !strain based on u_prev
     k = 0
     DO i = 1, dim
        strain(:,i) = du_prev(n_var_U(i),:,i) ! s_ii
        DO j = i+1, dim
           k= k + 1
           strain(:,dim+k) = 0.5_pr * ( du_prev(n_var_U(i),:,j) + du_prev(n_var_U(j),:,i) ) ! 0.5* (dui/dxj + dui/dxi)
        END DO
    END DO

    !
    ! does not rely on u so we can call it once here
    !
    CALL c_diff_diag(du, d2u, j_lev, ng, meth_central, meth_central, 11)
    !--Form right hand side of evolution equations
    DO ie = 1, dim
       shift=(ie-1)*ng
       user_Drhs_diag(shift+1:shift+ng) = SUM(d2u(:,1:dim),DIM=2)/Rep - (A + C*(u_prev_timestep(1:ng)**2+u_prev_timestep(ng+1:2*ng)**2) + (B-C)*u_prev_timestep(shift+1:shift+ng)**2) &
                                                                 - 2.0_pr*B*u_prev_timestep(shift+1:shift+ng)**2
    END DO

    shift=(n_var_P(1)-1)*ng
    user_Drhs_diag(shift+1:shift+ng) = user_Drhs_diag(shift+1:shift+ng) + B1*(STRAINmn(1)+strain(:,1)) + B2*(STRAINmn(2)+strain(:,2))
    shift=(n_var_P(2)-1)*ng
    user_Drhs_diag(shift+1:shift+ng) = user_Drhs_diag(shift+1:shift+ng) + B1*(STRAINmn(2)+strain(:,2)) + B2*(STRAINmn(1)+strain(:,1))

    DO ie = 1, dim
       shift=(n_var_U(ie)-1)*ng
       user_Drhs_diag(shift+1:shift+ng) = alpha
    END DO

    DO ie = 1, dim
       shift=(n_var_dU(ie)-1)*ng
       user_Drhs_diag(shift+1:shift+ng) = alpha
    END DO


     IF( hypermodel /= 0 )CALL hyperbolic_diag(user_Drhs_diag, ng)

  END FUNCTION user_Drhs_diag

  FUNCTION user_chi (nlocal, t_local)
    IMPLICIT NONE
    INTEGER,          INTENT (IN) :: nlocal
    REAL (pr),        INTENT (IN) :: t_local
    REAL (pr), DIMENSION (nlocal) :: user_chi, r, theta
    REAL (pr), DIMENSION(nlocal,DIM) :: Xloc
    INTEGER :: i
    LOGICAL, SAVE :: user_chi_initialized = .FALSE.

    !--Defines mask for array of plates

!!$    r= SQRT(SUM(x(:,1:dim)**2,DIM=2))
!!$
!!$    user_chi = 0.5_pr*(1.0_pr + tanh(r/delta_chi))
!!$
!!$    user_chi = MAX(0.0_pr,MIN(1.0_pr, user_chi))

    pi = 2.0_pr*ASIN(1.0_pr)
    user_chi = SIGN(1.0_pr, x(:,1)) ! 2.0_pr/pi*ATAN(x(:,1)/delta_chi)

  END FUNCTION user_chi

  !
  ! Calculate any statitics
  !
  ! startup_flag - 0 when adapting to IC,then 1 inmain integration loop
  !
  !
  SUBROUTINE user_stats (u , j_mn, startup_flag)
!    USE fft_module
!    USE spectra_module
    USE wlt_vars
    USE vector_util_mod
    USE parallel
    IMPLICIT NONE
    REAL (pr), DIMENSION (nwlt,1:n_var), INTENT (IN) :: u
    INTEGER , INTENT (IN) :: j_mn 
    INTEGER , INTENT (IN) :: startup_flag
    CHARACTER (LEN=256)  :: filename

    !USER may define additional statistics ouutput here.

!!$    IF(startup_flag /= -1) THEN !statistics accumulation
!!$       IF (par_rank.EQ.0) THEN
!!$          WRITE(*,'(" ")')
!!$          WRITE(*,'("****************** Statistics on the Fly *******************")')
!!$       END IF
!!$    END IF

  END SUBROUTINE user_stats

  !
  ! calculate any additional variables
  ! 
  ! arg 
  ! flag  - 0 calledwhile adapting to IC, 1 - called in main time integration loop
  !
  ! These additiona variables are calculated and left in real space.
  !
  SUBROUTINE user_additional_vars( t_local, flag )
    IMPLICIT NONE
    REAL (pr), INTENT (IN) ::  t_local
    INTEGER , INTENT(IN) :: flag ! 0- called during adaption to IC, 1 called during main integration loop
    REAL(pr) tmp(nwlt,2*dim) !tmp for vorticity
    INTEGER :: i, j, k, ie
    REAL (pr), DIMENSION(nwlt,len_strain) :: strain
    REAL (pr), DIMENSION (n_integrated,nwlt,dim) :: du, d2u

    IF(saveStrain .OR. saveStrainRate .OR. saveStress) THEN
       CALL c_diff_fast(u(:,1:n_integrated), du, d2u, j_lev, nwlt, HIGH_ORDER, 11, n_integrated, 1, n_integrated) !derivatives are calculated even for SGS terms
    END IF
    IF(saveStrain .OR. saveStress) THEN
       k = 0
       DO i = 1, dim
          strain(:,i) = du(n_var_U(i),:,i) ! s_ii
          DO j = i+1, dim
             k= k + 1
             strain(:,dim+k) = 0.5_pr * ( du(n_var_U(i),:,j) + du(n_var_U(j),:,i) ) ! 0.5* (dui/dxj + dui/dxi)
          END DO
       END DO
    END IF
    IF(saveStrain) THEN
       u(:,n_var_Strain(1:len_strain)) = strain(:,1:len_strain)
    END IF
    IF(saveStress) THEN
       u(:,n_var_Stress(1)) = C11*strain(:,1)+C12*strain(:,2)-0.5_pr*(B1*u(:,n_var_P(1))**2+B2*u(:,n_var_P(2))**2)
       u(:,n_var_Stress(2)) = C12*strain(:,1)+C11*strain(:,2)-0.5_pr*(B1*u(:,n_var_P(2))**2+B2*u(:,n_var_P(1))**2)
       u(:,n_var_Stress(3)) = C66*strain(:,3)-B3*u(:,n_var_P(1))*u(:,n_var_P(2))
    END IF

    IF(saveStrainRate) THEN
       k = 0
       DO i = 1, dim
          strain(:,i) = du(n_var_dU(i),:,i) ! s_ii
          DO j = i+1, dim
             k= k + 1
             strain(:,dim+k) = 0.5_pr * ( du(n_var_dU(i),:,j) + du(n_var_dU(j),:,i) ) ! 0.5* (dui/dxj + dui/dxi)
          END DO
       END DO
       u(:,n_var_StrainRate(1:len_strain)) = strain(:,1:len_strain)
    END IF

!!$    DO ie = 1, n_integrated
!!$       u(:,n_var_phi+ie) = (A + (B+C)*SUM(u(:,1:n_integrated)**2,DIM=2) - C*u(:,ie)**2)*u(:,ie)
!!$    END DO

  END SUBROUTINE user_additional_vars


  !
  ! calculate any additional scalar variables
  ! 
  ! arg 
  ! flag  - 0 calledwhile adapting to IC, 1 - called in main time integration loop
  !
  SUBROUTINE user_scalar_vars( flag )
    IMPLICIT NONE
    INTEGER , INTENT(IN) :: flag ! 0- called during adaption to IC, 1 called during main integration loop


  END SUBROUTINE user_scalar_vars

  !
  !************ Calculating Scales ***************************
  !
  ! Note the order of the components in the scl array
  ! correspond to u_tn, v_tn, w_tn, u_tn-1, v_tn-1, w_tn-1, u_tn-2, v_tn-2, w_tn-2
  ! 
  SUBROUTINE user_scales(flag, use_default, u_loc, nlocal, ne_local, l_n_var_adapt , l_n_var_adapt_index, &
       scl, scl_fltwt) !add
    USE parallel
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: flag ! 0 during initial adaptation, then 1 during time advancement
    LOGICAL , INTENT(INOUT) :: use_default
    INTEGER, INTENT (IN) :: nlocal, ne_local
    REAL (pr), DIMENSION (1:nlocal,1:ne_local), INTENT (IN) :: u_loc
    LOGICAL , INTENT (IN) :: l_n_var_adapt(ne_local)
    INTEGER , INTENT (IN) :: l_n_var_adapt_index(1:ne_local)
    REAL (pr), DIMENSION (1:ne_local), INTENT (INOUT) :: scl
    REAL (pr) ,  INTENT(IN) ::scl_fltwt !weight for temporal filter on scl
    REAL (pr) :: floor, tmp
    INTEGER :: ie, ie_index, itmp
    REAL (pr) ,save, ALLOCATABLE, DIMENSION (:) :: scl_old
    LOGICAL  ,save :: startup_init = .TRUE.

    !
    ! Ignore the output of this routine and use default scales routine
    !
    use_default = .FALSE. 

    IF( startup_init .AND. .NOT. ALLOCATED(scl_old) ) THEN
       ALLOCATE( scl_old(1:ne_local) )
       scl_old = 0.0_pr
    END IF


    IF (verb_level.GT.0.AND.par_rank.EQ.0) &
         WRITE (*,'("Using default Scales() routine, Scale_Meth = ", i3 )' ) Scale_Meth 
    
    floor = 1.e-12_pr
    scl   =1.0_pr
    DO ie=1,ne_local
       IF(l_n_var_adapt(ie)) THEN
          ie_index = l_n_var_adapt_index(ie)
          
          IF( Scale_Meth == 1 ) THEN ! Use Linf scale                
             scl(ie) = MAXVAL ( ABS( u_loc(1:nlocal,ie_index) ) )
             CALL parallel_global_sum( REALMAXVAL=scl(ie) )
             
          ELSE IF ( Scale_Meth == 2 ) THEN ! Use L2 scale
             scl(ie) = SUM( (u_loc(1:nlocal,ie_index)**2) )
             itmp = nlocal
             CALL parallel_global_sum( REAL=scl(ie) )
             CALL parallel_global_sum( INTEGER=itmp )
             scl(ie)= SQRT ( scl(ie)/itmp )
             
          ELSE IF ( Scale_Meth == 3 ) THEN ! Use L2 scale using dA
             scl(ie) = SUM( (u_loc(1:nlocal,ie_index)**2)*dA )
             CALL parallel_global_sum( REAL=scl(ie) )
             scl(ie) = SQRT ( scl(ie)/sumdA_global )
             
          ELSE
             IF (par_rank.EQ.0) THEN
                PRINT *, 'Error in Scales(), Unknown Scale_Meth type: ', Scale_Meth
                PRINT *, 'Known Scale_Meth types: 1 = Linf scale, 2 = L2 scale '
                PRINT *, 'Exiting ...'
             END IF
             CALL parallel_finalize; STOP
          END IF
          IF(scl(ie) .le. floor) scl(ie)=1.0_pr !Shouldn't this just set it to floor????
          tmp = scl(ie)
          ! temporally filter scl
          IF(.not. startup_init ) scl(ie) = scl_fltwt * scl_old(ie) + (1.0_pr - scl_fltwt) * scl(ie)
          
          ! multiply scale coefficient read in from input file
          scl = scaleCoeff * scl
          
          IF (verb_level.GT.0.AND.par_rank.EQ.0) THEN
             WRITE (6,'("Scale factor eqn", I4 ," = ", es11.4 ," scalecoeff = ",es11.4 )' ) &
                  ie, scl(ie), scaleCoeff(ie)
             WRITE (6,'("(before filtering = ",es11.4,")" )') tmp
          END IF
       END IF
    END DO
    IF( Scale_Meth == 1 ) THEN ! Use Linf scale                
       scl(n_var_P(1:dim))  = MAXVAL(scl(n_var_P(1:dim)))
       scl(n_var_U(1:dim))  = MAXVAL(scl(n_var_U(1:dim)))
       scl(n_var_dU(1:dim)) = MAXVAL(scl(n_var_dU(1:dim)))
    ELSE IF ( Scale_Meth == 2 ) THEN ! Use L2 scale
       scl(n_var_P(1:dim))  = SQRT(SUM(scl(n_var_P(1:dim))**2))
       scl(n_var_U(1:dim))  = SQRT(SUM(scl(n_var_U(1:dim))**2))
       scl(n_var_dU(1:dim)) = SQRT(SUM(scl(n_var_dU(1:dim))**2))
    ELSE IF ( Scale_Meth == 3 ) THEN ! Use L2 scale using dA
       scl(n_var_P(1:dim))  = SQRT(SUM(scl(n_var_P(1:dim))**2))
       scl(n_var_U(1:dim))  = SQRT(SUM(scl(n_var_U(1:dim))**2))
       scl(n_var_dU(1:dim)) = SQRT(SUM(scl(n_var_dU(1:dim))**2))
    END IF

    scl_old = scl !save scl for this time step
    startup_init = .FALSE.
    !PRINT *,'TEST scl_old ', scl_old
    
  END SUBROUTINE user_scales

  SUBROUTINE user_cal_cfl (use_default, u, cfl_out)
    USE parallel
    IMPLICIT NONE
    LOGICAL , INTENT(INOUT) :: use_default
    REAL (pr),                                INTENT (INOUT) :: cfl_out
    REAL (pr), DIMENSION (nwlt,n_integrated), INTENT (IN)    :: u
    
    INTEGER                    :: i
    REAL (pr)                  :: floor
    REAL (pr), DIMENSION (dim) :: cfl
    REAL (pr), DIMENSION(dim,nwlt) :: h_arr
    
    use_default = .FALSE.
    
    floor = 1e-12_pr
    cfl_out = floor
    
    CALL get_all_local_h (h_arr)
    
!!$    DO i = 1, nwlt
!!$       cfl(1:dim) = ABS (Uold(i,1:dim)) * dt/h_arr(1:dim,i)
!!$       cfl_out = MAX (cfl_out, MAXVAL(cfl))
!!$    END DO

    CALL parallel_global_sum( REALMAXVAL=cfl_out )
    
  END SUBROUTINE user_cal_cfl
  
  SUBROUTINE  user_sgs_force ( u, nlocal)
    IMPLICIT NONE
    
    INTEGER,                         INTENT (IN) :: nlocal
    REAL (pr), DIMENSION (nlocal,n_integrated), INTENT (INOUT) :: u
    
    !default SGS_force, can be explicitely deifined below instead of calling default SGS_force
    IF(sgsmodel /= 0) CALL sgs_force ( u, nlocal)
    
  END SUBROUTINE user_sgs_force

  FUNCTION user_sound_speed (u, neq, nwlt)
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: nwlt, neq
    INTEGER :: meth
    REAL (pr), DIMENSION (nwlt,neq), INTENT(IN) :: u
    REAL (pr), DIMENSION (nwlt) :: user_sound_speed

    IF(divgrad) THEN
       meth = HIGH_ORDER + BIASING_BACKWARD
    ELSE
       meth  = HIGH_ORDER + BIASING_NONE
    END IF

!!$    IF(testmeth ==1) THEN
!!$       user_sound_speed(:) = 0.5_pr*SQRT(SUM(x(:,1:dim)**2,DIM=2))
!!$    ELSE
!!$       user_sound_speed(:) = SQRT(SUM(grad (u(:,n_var_pressure), nwlt, j_lev,  meth)**2,DIM=2))
!!$    END IF

    user_sound_speed(:) =1.0

  END FUNCTION user_sound_speed

  SUBROUTINE  user_pre_process
    IMPLICIT NONE
    INTEGER, PARAMETER :: ne_local = 1
    INTEGER :: i
    INTEGER :: meth, meth_central, meth_backward, meth_forward 
    REAL (pr), DIMENSION (nwlt) :: phi
    REAL (pr), DIMENSION (nwlt,ne_local) :: dphi
    REAL (pr), DIMENSION (nwlt,ne_local) :: f
    REAL (pr), DIMENSION(ne_local) :: scl_phi
    INTEGER, DIMENSION(ne_local) :: clip 

    IF(divergence_correction) THEN
       PRINT *, '**********************************************************************'
       PRINT *, '*                       divergence correction                        *'
       PRINT *, '**********************************************************************'
       meth          = HIGH_ORDER
       meth_central  = HIGH_ORDER + BIASING_NONE
       meth_backward = HIGH_ORDER + BIASING_BACKWARD
       meth_forward  = HIGH_ORDER + BIASING_FORWARD

       !--Solve for phi and update polarization vector
       phi(:) = u(:,n_var_phi) 
       f = Laplace_rhs (u(:,1:dim), nwlt, dim, meth)
       clip = MINVAL(prd(1:dim))
       
       scl_phi = MAX(scl_global(n_var_phi),MAXVAL(scl_global(n_var_P(1:dim)))) !scale of phi increment based on dynamic pressure
       PRINT *, 'user_pre_process: scl_phi=',scl_phi
       CALL Linsolve (phi, f , tol2, nwlt, ne_local, clip, Laplace, Laplace_diag, SCL=scl_phi) !update phi with scaling 
       !CALL Linsolve (phi, f , tol2, nwlt, ne_local, clip, Laplace, Laplace_diag)              !update phi without scaling

       u(:,n_var_phi) = phi(:)

       !dphi(:,1) = phi(:)-dphi(:,1)
       !u(:,1:dim) = u(:,1:dim) - 0.5_pr*D*dt*grad(dphi, nlocal, j_lev, meth_central) !update pollarization vector

    END IF
    IF(ALLOCATED(Pold)) DEALLOCATE(Pold)
    ALLOCATE(Pold(nwlt,dim))
    DO i=1,dim
       Pold(:,i) = u(:,n_var_P(i))
    END DO

  END SUBROUTINE user_pre_process

  SUBROUTINE  user_post_process
    IMPLICIT NONE

  END SUBROUTINE user_post_process

END MODULE user_case






