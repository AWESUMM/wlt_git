MODULE user_case
  !
  ! Case sphere
  USE precision
  USE elliptic_vars
  USE elliptic_mod
  USE field
  USE input_file_reader
  USE io_3D_vars
  USE pde
  USE share_consts
  USE share_kry
  USE sizes
  USE util_mod
  USE util_vars
  USE vector_util_mod
  USE wavelet_filters_mod
  USE wlt_vars
  USE wlt_trns_vars
  USE wlt_trns_mod
  USE wlt_trns_util_mod
  USE fft_module
  USE SGS_incompressible

  !
  ! case specific variables
  !
  INTEGER :: n_var_phi ! start of level set function in u array (Must exist)
  INTEGER, ALLOCATABLE, DIMENSION (:) :: n_var_U ! start of velecities in the array u (Must exist)
  INTEGER :: n_var_pressure  ! start of pressure in u array  (Must exist)
  INTEGER :: n_var_Msk ! mask, of fluid (1) and gas (0)

  REAL(pr) :: Re, Fr, Re_gh, del_fd
  INTEGER, PARAMETER :: ie_pressure = 1, ie_W = 2, ne_vertical = 2
  REAL (pr), DIMENSION (:), ALLOCATABLE :: G

CONTAINS

  !
  ! In user_setup_pde() we setup how many variables are integrated, which are interpolated
  ! to the next times step and if any exeact solution exists to compare to during the run.
  ! We also set the variable names that are used when the result files are written.
  !
  ! The following variables must be setup in this routine:
  !
  !
  ! n_integrated     ! first n_integrated eqns will be acted on for time integration
  ! n_var_additional ! 
  ! n_var
  !
  !
  !
  !
  SUBROUTINE  user_setup_pde ( VERB ) 
    IMPLICIT NONE
    LOGICAL, OPTIONAL :: VERB         ! print debug info
    INTEGER :: i

    PRINT * ,''
    PRINT *, '**********************Setting up PDE*****************'
    PRINT * ,'CASE ISOTURB '
    PRINT *, '*****************************************************'


    !------------ setting up default values 

    n_integrated = dim ! uvw - # of equations to solve without SGS model
 
    n_integrated = n_integrated + n_var_SGS ! adds additional equations for SGS model
    
    n_time_levels = 1  !--# time levels

    n_var_time_levels = n_time_levels * n_integrated !--Number of equations (2D velocity at two time levels)

    n_var_additional = 3 !--1 pressure & vertical velocity & mask at one time level & mask

    n_var_exact = 0 !--No exact solution 

    ALLOCATE(n_var_U(dim))
    n_var_phi = 1
    DO i = 1, dim-1 
       n_var_U(i)   = 1 + i !u(i)
    END DO
    n_var_U(dim) = n_integrated + ie_W
    n_var_pressure  = n_integrated + ie_pressure !pressure
    n_var_Msk       = n_integrated + n_var_additional ! mask
    
    n_var = n_var_time_levels + n_var_additional !--Total number of variables

    !
    !--Allocate logical arrays 
    !  Must be done after setting number of different types of variables
    !  and before setting logical variable mapping arrays
    !
    CALL alloc_variable_mappings
    !
    ! Fill in variable names for default variables for integration meth 2
    ! These may be changed in the call to the user specified routine
    !  user_setup_pde()
    !
    ! In integrated variables (3D)

    WRITE (u_variable_names(1), u_variable_names_fmt)                              'phi  '
    IF( dim >= 2 ) WRITE (u_variable_names(2), u_variable_names_fmt)               'U    '
    IF( dim >= 3 ) WRITE (u_variable_names(3), u_variable_names_fmt)               'V    '
    WRITE (u_variable_names(n_var_pressure), u_variable_names_fmt)                 'P    '
    IF( size(n_var_U) == dim) WRITE (u_variable_names(n_var_U(dim)), u_variable_names_fmt)'W    '
    IF( n_var_Msk /= 0 ) WRITE (u_variable_names(n_var_Msk), u_variable_names_fmt) 'mask '
    !
    ! setup logical variable mapping arrays
    ! This defined which variables are used for adaptation, Which need to be interpololated to the new grid at 
    ! each time step, which are saved, and for which we have an exect solutiuon we want to check at each time step
    !

    !
    ! setup which components we will base grid adaptation on.
    ! Adapt on the 3 velocity components
    !
    n_var_adapt = .FALSE. !--Initially adapt on integrated variables at first time level

    !eventually adapt to mdl variables    n_var_adapt(1:dim+2,1) = .TRUE. !--After first time step adapt on  velocity and Ilm and Imm

    n_var_adapt(1:dim,0) = .TRUE. !--After first time step adapt on  velocity
    n_var_adapt(1:dim,1) = .TRUE. !--After first time step adapt on  velocity
!    n_var_adapt(n_var_pressure,0) = .TRUE. !--After first time step adapt on  velocity
!    n_var_adapt(n_var_pressure,1) = .TRUE. !--After first time step adapt on  velocity
    n_var_adapt(n_var_Msk,0) = .TRUE. !--After first time step adapt on  velocity
    n_var_adapt(n_var_Msk,1) = .TRUE. !--After first time step adapt on  velocity

    !Variables that need to be interpoleted to new adapted grid in initial grid adaptation
    n_var_interpolate(1:n_integrated,0) = .TRUE. 

    !Variables that need to be interpoleted to new adapted grid at each time step
    n_var_interpolate(1:n_var_Msk,0) = .TRUE. 
    n_var_interpolate(1:n_var_Msk,1) = .TRUE. 

    ! setup which components we have an exact solution for

    n_var_exact_soln(:,0:1) = .FALSE.

    !
    ! variables required for restart
    !
    n_var_req_restart = .FALSE.
    n_var_req_restart(1:dim)	        = .TRUE. !restart with velocities and pressure to begin with!

    ! no pressure for restart from initial file
    !n_var_req_restart(n_var_pressure ) = .TRUE.

    !
    ! setup which variables we will save the solution
    !
    n_var_save(1:n_var) = .TRUE. ! save all for restarting code

    !
    !--Time level counters for NS integration
    !   Used for integration meth_forward
    ! n0 is current time step
    ! n1 is t_n-1 time step
    ! n2 is t_n-2 time step
    !
    n0 = 1; n1 = n0+dim; n2 = n1+dim


    !
    ! Set the maximum number of components for which we have an exact solution
    !
    n_var_exact = MAX( COUNT(n_var_exact_soln(:,0)),COUNT(n_var_exact_soln(:,1)) )


    !
	! Setup a scaleCoeff array of we want to tweak the scaling of a variable
	! ( if scaleCoeff < 1.0 then more points will be added to grid 
	!
    ALLOCATE ( scaleCoeff(1:n_var), Umn(1:n_var) )
    scaleCoeff = 1.0_pr
!!$    scaleCoeff(n_var_Msk) = MAX(1.e-2_pr,eps)/eps ! make relative eps for the mask to be 0.1
!!$    PRINT *, 'scaleCoeff=',scaleCoeff
!!$    PAUSE


    Umn = 0.0_pr !set up here if mean quantities anre not zero and used in scales or equation

    PRINT *, 'n_integrated = ',n_integrated 
    PRINT *, 'n_time_levels = ',n_time_levels
    PRINT *, 'n_var_time_levels = ',n_var_time_levels 
    PRINT *, 'n_var = ',n_var 
    PRINT *, 'n_var_exact = ',n_var_exact 
    PRINT *, '*******************Variable Names*******************'
    DO i = 1,n_var
       WRITE (*, u_variable_names_fmt) u_variable_names(i)
    END DO
    PRINT *, '****************************************************'



  END SUBROUTINE  user_setup_pde

  !
  ! read variables from input file for this user case 
  !
  SUBROUTINE  user_read_input()
    IMPLICIT NONE


  call input_real ('Re',Re,'stop', ' Re: Reynolds number')

  call input_real ('Fr',Fr,'stop', ' Fr: Froude number')

  call input_real ('Re_gh',Re_gh,'stop', ' Re_gh: ghost Reynolds number')

  call input_real ('del_fd',del_fd,'stop', ' del_fd: thickness of fading function')

  END SUBROUTINE  user_read_input

  !
  ! Set the exact solution for comparison to the simulated solution
  !
  ! u          - array to fill in the exact solution
  ! nlocal       - number of active wavelets   
  ! ne_local        - total number of equations
  ! t          - time of current time step 
  ! l_n_var_exact_soln_index - index into the elements of u for which we need to 
  !                            find the exact solution
  SUBROUTINE  user_exact_soln (u, nlocal,  t_local, l_n_var_exact_soln)  
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: nlocal
    REAL (pr), INTENT (IN) ::  t_local
    REAL (pr), DIMENSION (nlocal,n_var_exact), INTENT (INOUT) :: u
    LOGICAL , INTENT (IN) :: l_n_var_exact_soln(n_var)
    INTEGER :: i


    ! There is no exact solution

  END SUBROUTINE  user_exact_soln

  SUBROUTINE user_initial_conditions (u, nlocal, ne_local, t_local, scl, scl_fltwt, iter)
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: nlocal, ne_local
    INTEGER  , INTENT (INOUT) :: iter ! iteration of call while adapting initial grid
    REAL (pr), DIMENSION (nlocal,ne_local), INTENT (INOUT) :: u
    REAL (pr)  :: scl(1:n_var),scl_fltwt
    REAL (pr), INTENT (IN) :: t_local

    INTEGER :: i,ie !TEST

    INTEGER, DIMENSION(1) :: SEED
    REAL (pr) ::   Uprime=1.0e-3_pr
    REAL (pr) ::   Umean,U2mean

	!add or overtise teh values in the fileds
    IF (IC_restart ) THEN !in the case of restart
	   !if restarts - DO NOTHING
    ELSE IF (IC_from_file) THEN  ! Initial conditions read from data file 
       !
	   ! Modify appropriate values
       ! 
       !u(:,n_var_pressure) = 1.0_pr
    ELSE !initialization from random field
       u(:,n_var_phi) = x(:,dim) - 0.2_pr*exp(-10.0_pr*x(:,1)**2) ! phi
       DO ie = 2,dim
          u(:,ie) = 1.0E-10_pr !velocity V(:.1:dim-1)
       END DO
       CALL user_additional_vars( t_local, 0 )
       u(:,n_var_phi) = G*u(:,n_var_phi)+(1.0_pr-G)*SIGN(1.0_pr,u(:,n_var_phi))

    END IF

  END SUBROUTINE user_initial_conditions

  ! Arguments
  ! u         - field on adaptive grid
  ! nlocal      - number of active points
  ! ne_local       - number of equations
  ! t         - current time
  !
  ! Global variables used
  ! ifn       - number of equations
  ! x()
  ! xO
  ! y0
  !--******************************** 
  SUBROUTINE user_algebraic_BC (Lu, u, nlocal, ne_local, jlev, meth)
    !--Defines boundary condition type
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: jlev, meth, ne_local, nlocal
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (INOUT) :: Lu
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (IN)    :: u
    REAL (pr), DIMENSION (ne_local, nlocal, dim) :: du
    REAL (pr), DIMENSION (ne_local, nlocal, dim) :: d2u

    INTEGER :: i, ie, ii, shift
    !REAL (pr), DIMENSION (ne_nlocal,nlocal,dim) :: du, d2u !uncomment if Neuman BC are used, need to call 
    INTEGER :: face_type, nloc, meth_local
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc

    !BC for both 2D and 3D cases

    !if Neuman BC are used, need to call 
    CALL c_diff_fast (u, du, d2u, jlev, nlocal, meth, 10, ne_local, 1, ne_local)

    DO ie = 1, ne_local
       shift = nlocal*(ie-1)
       !--Go through all Boundary points that are specified
       i_p_face(0) = 1
       DO i=1,dim
          i_p_face(i) = i_p_face(i-1)*3
       END DO
       DO face_type = 0, 3**dim - 1
          face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
          IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
             CALL get_all_indices_by_face (face_type, jlev, nloc, iloc)
             IF(nloc > 0 ) THEN 
                IF( face(dim) == -1  ) THEN                          ! min(X_dim) face (entire face) 
!!$                   Lu(shift+iloc(1:nloc)) = u(shift+iloc(1:nloc))  !Dirichlet conditions
                   Lu(shift+iloc(1:nloc)) = du(ie,iloc(1:nloc),dim)    !Neuman conditions
                ELSE IF( face(dim) == 1  ) THEN                      ! max(X_dim) face (entire face) 
!!$                   Lu(shift+iloc(1:nloc)) = u(shift+iloc(1:nloc))  !Dirichlet conditions
                   Lu(shift+iloc(1:nloc)) = du(ie,iloc(1:nloc),dim)    !Newman conditions
                END IF
             END IF
          END IF
       END DO
    END DO

    !default SGS BC, can be explicitely deifined below instead of calling default BCs
    CALL SGS_algebraic_BC (Lu, u, nlocal, ne_local, jlev, meth) 

  END SUBROUTINE user_algebraic_BC

  SUBROUTINE user_algebraic_BC_diag (Lu_diag, nlocal, ne_local, jlev, meth)
    !--Defines boundary condition type
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: jlev, meth, ne_local, nlocal
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (INOUT) :: Lu_diag
    REAL (pr), DIMENSION (nlocal,dim) :: du, d2u

    INTEGER :: i, ie, ii, shift
    ! REAL (pr), DIMENSION (nlocal,dim) :: du, d2u  !uncomment if Neuman BC are used
    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc

    !BC for both 2D and 3D cases

    !if Neuman BC are used, need to call 
    CALL c_diff_diag ( du, d2u, jlev, nlocal, meth, meth, 10)

    DO ie = 1, ne_local
       shift = nlocal*(ie-1)
       !--Go through all Boundary points that are specified
       i_p_face(0) = 1
       DO i=1,dim
          i_p_face(i) = i_p_face(i-1)*3
       END DO
       DO face_type = 0, 3**dim - 1
          face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
          IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
             CALL get_all_indices_by_face (face_type, jlev, nloc, iloc)
             IF(nloc > 0 ) THEN 
                IF( face(dim) == -1  ) THEN                            ! min(X_dim) face (entire face) 
!!$                   Lu_diag(shift+iloc(1:nloc)) = 1.0_pr                       !Dirichlet conditions
                   Lu_diag(shift+iloc(1:nloc)) = du(iloc(1:nloc),dim) !Neuman conditions
                ELSE IF( face(dim) == 1  ) THEN                        ! max(X_dim) face (entire face) 
!!$                   Lu_diag(shift+iloc(1:nloc)) = 1.0_pr                       !Dirichlet conditions
                   Lu_diag(shift+iloc(1:nloc)) = du(iloc(1:nloc),dim)  !Newman conditions
                END IF
             END IF
          END IF
       END DO
    END DO
    
    !default diagonal terms of SGS BC, can be explicitely deifined below instead of calling default BCs
    IF(sgsmodel /=0 ) CALL SGS_algebraic_BC_diag (Lu_diag, nlocal, ne_local, jlev, meth)

  END SUBROUTINE user_algebraic_BC_diag

  SUBROUTINE user_algebraic_BC_rhs (rhs, ne_local, nlocal, jlev)
    !--Sets rhs for boundary conditions
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: ne_local, nlocal, jlev
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (INOUT) :: rhs

    INTEGER :: i, ie, ii, shift
    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc

    !BC for both 2D and 3D cases

    DO ie = 1, ne_local
       shift = nlocal*(ie-1)
       !--Go through all Boundary points that are specified
       i_p_face(0) = 1
       DO i=1,dim
          i_p_face(i) = i_p_face(i-1)*3
       END DO
       DO face_type = 0, 3**dim - 1
          face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
          IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
             CALL get_all_indices_by_face (face_type, jlev, nloc, iloc)
             IF(nloc > 0 ) THEN 
                IF( face(dim) == -1  ) THEN                            ! min(X_dim) face (entire face) 
!!$                   rhs(shift+iloc(1:nloc)) = 0.0_pr                 !Dirichlet conditions
                   rhs(shift+iloc(1:nloc)) = 0.0_pr                    !Neuman conditions
                ELSE IF( face(dim) == 1  ) THEN                        ! max(X_dim) face (entire face) 
!!$                   rhs(shift+iloc(1:nloc)) = 0.0_pr                 !Dirichlet conditions
                   rhs(shift+iloc(1:nloc)) = 0.0_pr                    !Newman conditions
                END IF
             END IF
          END IF
       END DO
    END DO

    !default  SGS BC, can be explicitely deifined below instead of calling default BCs
    IF(sgsmodel /= 0) CALL SGS_algebraic_BC_rhs (rhs, ne_local, nlocal, jlev)

  END SUBROUTINE user_algebraic_BC_rhs

  SUBROUTINE user_project (u, p, nlocal, meth)
    !--Makes u divergence free
    IMPLICIT NONE
    INTEGER, PARAMETER :: ne_local = 1
    INTEGER, INTENT (IN) :: meth, nlocal
    REAL (pr), DIMENSION (nlocal),     INTENT(INOUT) :: p
    REAL (pr), DIMENSION (nlocal,n_integrated), INTENT(INOUT) :: u
    REAL (pr), DIMENSION (nlocal,ne_local) :: dp
    REAL (pr), DIMENSION (nlocal,ne_local) :: f
    REAL (pr), DIMENSION(ne_local) :: scl_p
    INTEGER, DIMENSION(ne_local) :: clip 

!!$    !--Make u  divergence free
!!$    dp = 0.0_pr 
!!$    f = Laplace_rhs (u(:,1:dim), nlocal, dim, meth)
!!$    clip = 1
!!$
!!$    scl_p = MAXVAL(scl_global(1:dim)) !scale of pressure increment based on dynamic pressure
!!$    PRINT *, 'scl_p=',scl_p
!!$    CALL Linsolve (dp, f , tol2, nlocal, ne_local, clip, Laplace, Laplace_diag, SCL=scl_p) !new with scaling 
!!$!    CALL Linsolve (dp, f , tol2, nlocal, ne_local, clip, Laplace, Laplace_diag)  !old without scaling
!!$    u(:,1:dim) = u(:,1:dim) - grad(dp, nlocal, j_lev, meth+2)
!!$    p = p + dp(:,1)/dt

  END SUBROUTINE user_project

  FUNCTION Laplace (jlev, u, nlocal, ne_local, meth_order)
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: jlev, nlocal, ne_local, meth_order
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (INOUT) :: u
    REAL (pr), DIMENSION (nlocal*ne_local) :: Laplace

    INTEGER :: i, ii, ie, idim, shift
    INTEGER :: meth_central, meth_backward, meth_forward 
    REAL (pr), DIMENSION (ne_local, nlocal, dim) :: du
    REAL (pr), DIMENSION (dim, nlocal, dim)      :: d2u
    REAL (pr), DIMENSION (dim, nlocal, dim)      :: du_dummy ! passed when only calculating 1st derivative.
    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc

    meth_central  = meth_order + BIASING_NONE
    meth_backward = meth_order + BIASING_BACKWARD
    meth_forward  = meth_order + BIASING_FORWARD

    !
    ! Find 1st deriviative of u. 
    !
    CALL c_diff_fast (u, du, d2u(1:ne_local, 1:nlocal, 1:dim), jlev, nlocal, meth_backward, 10, ne_local, 1, ne_local)

    DO ie = 1, ne_local
       shift=(ie-1)*nlocal
       !--- Internal points
       CALL c_diff_fast(du(ie,1:nlocal,1:dim), d2u(1:dim, 1:nlocal, 1:dim), du_dummy(1:dim, 1:nlocal, 1:dim), &
            jlev, nlocal, meth_forward, 10, dim, 1, dim )
       !--- div(grad)
       idim = 1
       Laplace(shift+1:shift+Nwlt_lev(jlev,0)) = d2u( idim ,1:Nwlt_lev(jlev,0),idim)
       DO idim = 2,dim
          Laplace(shift+1:shift+Nwlt_lev(jlev,0)) = Laplace(shift+1:shift+Nwlt_lev(jlev,0)) + &
               d2u(idim ,1:Nwlt_lev(jlev,0),idim)
       END DO
       !--Boundary points
       i_p_face(0) = 1
       DO i=1,dim
          i_p_face(i) = i_p_face(i-1)*3
       END DO
       DO face_type = 0, 3**dim - 1
          face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
          IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
             CALL get_all_indices_by_face (face_type, jlev, nloc, iloc)
             IF(nloc > 0 ) THEN ! ANY dimension
                IF( face(1) == -1  ) THEN                          ! Xmin face (entire face) 
                   Laplace(shift+iloc(1:nloc)) = u(shift+iloc(1:nloc))  !Dirichlet conditions
                ELSE IF( face(1) == 1  ) THEN                      ! Xmax face (entire face) 
!!$                   Laplace(shift+iloc(1:nloc)) = u(shift+iloc(1:nloc))  !Dirichlet conditions
                   Laplace(shift+iloc(1:nloc)) = du(ie,iloc(1:nloc),1)  !Neuman conditions
                END IF
             END IF
          END IF
       END DO
    END DO
  END FUNCTION Laplace

  FUNCTION Laplace_diag (jlev,  nlocal, ne_local, meth_order)
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: jlev, nlocal, ne_local, meth_order
    REAL (pr), DIMENSION (nlocal*ne_local) :: Laplace_diag

    INTEGER :: i, ii, ie, idim, shift
    INTEGER :: meth_central, meth_backward, meth_forward 
    REAL (pr), DIMENSION (nlocal,dim) :: du, d2u
    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc

    meth_central  = meth_order + BIASING_NONE
    meth_backward = meth_order + BIASING_BACKWARD
    meth_forward  = meth_order + BIASING_FORWARD

    CALL c_diff_diag ( du, d2u, jlev, nlocal, meth_backward, meth_forward, -11)

    !PRINT *,'IN Laplace_diag, ne_local = ', ne_local

    DO ie = 1, ne_local
       shift=(ie-1)*nlocal
       !--- div(grad)
       !PRINT *,'CAlling c_diff_diag from Laplace_diag() '
       !PRINT *,'--- jlev, nlocal, meth_backward, meth_forward', jlev, nlocal, meth_backward, meth_forward

       idim = 1
       Laplace_diag(shift+1:shift+Nwlt_lev(jlev,0)) = d2u(1:Nwlt_lev(jlev,0),idim)
       DO idim = 2,dim
          Laplace_diag(shift+1:shift+Nwlt_lev(jlev,0)) = Laplace_diag(shift+1:shift+Nwlt_lev(jlev,0)) + &
               d2u(1:Nwlt_lev(jlev,0),idim)
       END DO
       !--Boundary points
       i_p_face(0) = 1
       DO i=1,dim
          i_p_face(i) = i_p_face(i-1)*3
       END DO
       DO face_type = 0, 3**dim - 1
          face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
          IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
             CALL get_all_indices_by_face (face_type, jlev, nloc, iloc)
             IF(nloc > 0 ) THEN ! ANY dimension
                IF( face(1) == -1  ) THEN                          ! Xmin face (entire face) 
                   Laplace_diag(shift+iloc(1:nloc)) = 1.0_pr                 !Dirichlet conditions
                ELSE IF( face(1) == 1  ) THEN                      ! Xmax face (entire face) 
!!$                   Laplace_diag(shift+iloc(1:nloc)) = 1.0_pr                 !Dirichlet conditions
                   Laplace_diag(shift+iloc(1:nloc)) = du(iloc(1:nloc),1)     !Neuman conditions
                END IF
             END IF
          END IF
       END DO
    END DO

   END FUNCTION Laplace_diag

  FUNCTION Laplace_rhs(u, nlocal, ne_local, meth_order)
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: nlocal, ne_local, meth_order
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (INOUT) :: u
    REAL (pr), DIMENSION (nlocal,ne_local) :: Laplace_rhs

    INTEGER :: i, ii, ie, meth, idim
    INTEGER :: meth_central, meth_backward, meth_forward 
    REAL (pr), DIMENSION (ne_local ,nlocal,dim) :: du
    REAL (pr), DIMENSION (ne_local*dim ,nlocal,dim) :: d2u
    REAL (pr), DIMENSION (ne_local*dim ,nlocal,dim) :: du_dummy ! passed when only calculating 1st derivative.
    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc

    meth_central  = meth_order + BIASING_NONE
    meth_backward = meth_order + BIASING_BACKWARD
    meth_forward  = meth_order + BIASING_FORWARD

    Laplace_rhs(:,1) = div(u,nlocal,j_lev,meth_forward)

    !--Boundary points
    i_p_face(0) = 1
    DO i=1,dim
       i_p_face(i) = i_p_face(i-1)*3
    END DO
    DO face_type = 0, 3**dim - 1
       face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
       IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
          CALL get_all_indices_by_face (face_type, j_lev, nloc, iloc)
          IF(nloc > 0 ) THEN 
             IF( face(1) == -1  ) THEN                          ! Xmin face (entire face) 
                Laplace_rhs(iloc(1:nloc),1) = 0.0_pr  !Dirichlet conditions
             ELSE IF( face(1) == 1  ) THEN                      ! Xmax face (entire face) 
!!$                Laplace_rhs(iloc(1:nloc),1) = 0.0_pr !Dirichlet conditions
                Laplace_rhs(iloc(1:nloc),1) = 0.0_pr !Neuman conditions
             END IF
          END IF
       END IF
    END DO
    
  END FUNCTION Laplace_rhs


  FUNCTION user_rhs (u_integrated,p)
    USE penalization
    IMPLICIT NONE
    REAL (pr), DIMENSION (ng,ne), INTENT(IN) :: u_integrated !1D version of integrated variables without BC 
    REAL (pr), DIMENSION (ng), INTENT(IN) :: p
    REAL (pr), DIMENSION (n) :: user_rhs

    INTEGER :: ie, ie1, shift, i, idim
    INTEGER :: meth_central, meth_backward, meth_forward 
    REAL (pr), DIMENSION (dim+1,ng,dim) :: du, d2u, du_dummy
    REAL (pr), DIMENSION (ng,dim+2)     :: V
    REAL (pr), DIMENSION (ng) :: Msk  
    

    meth_central  = HIGH_ORDER + BIASING_NONE
    meth_backward = HIGH_ORDER + BIASING_BACKWARD
    meth_forward  = HIGH_ORDER + BIASING_FORWARD
   !just for testing:

   WHERE(u_integrated(:,n_var_phi) <= 0.0_pr)
      Msk = 1.0_pr
   ELSEWHERE
      Msk = 0.0_pr
   ENDWHERE
     
   V(:,ie_pressure) = p(:)
   V(:,ie_W) = 0.0_pr
   CALL vert_integ (u_integrated, V(:,1:ne_vertical), Msk, nwlt, ne, ne_vertical, HIGH_ORDER)
   V(:,dim+1) = V(:,ie_pressure)      ! pressures
   V(:,dim+2) = V(:,ie_W)             ! W velocity component
   V(:,1:dim) = u_integrated(:,1:dim) ! phi and 1-(dim-1) velocity components

   CALL c_diff_fast(V(:,1:dim+1), du, d2u, j_lev, ng, HIGH_ORDER, 11, dim+1, 1, dim+1) !derivatives are calculated even for SGS terms
   V(:,1:dim-1) = V(:,2:dim) ! 1-(dim-1) velocity components
   V(:,dim) = V(:,dim+2)     ! dim - velocity component
  
   !--Form right hand side of Navier-Stokes equations
 
   user_rhs = 0.0_pr

   DO ie = 1, dim
      shift=(ie-1)*ng
      DO ie1 = 1,dim
         user_rhs(shift+1:shift+ng) = user_rhs(shift+1:shift+ng) - V(:,ie1)*du(ie,:,ie1) 
      END DO
   END DO
   !adding pressure gradient starting from 2nd equation for u
   DO ie = 1, dim-1 
      shift=ie*ng 
      user_rhs(shift+1:shift+ng) = user_rhs(shift+1:shift+ng) - du(dim+1,:,ie) 
   END DO
   

   !div(grad)
   CALL c_diff_fast (u(:,1:ne), du(1:ne,:,1:dim), d2u(1:ne,:,1:dim), j_lev, ng, meth_backward, 10, ne, 1, ne)
   
   DO ie = 1, ne !no diffusion in phi for now
      shift=(ie-1)*ng
      !--- Internal points
      CALL c_diff_fast(du(ie,1:ng,1:dim), d2u(1:dim, 1:ng, 1:dim), du_dummy(1:dim, 1:ng, 1:dim), &
           j_lev, ng, meth_forward, 10, dim, 1, dim )
      DO idim = 1,dim
         user_rhs(shift+1:shift+ng) = user_rhs(shift+1:shift+ng) + &
               d2u(idim ,1:ng, idim)/Re
      END DO
   END DO
     
   user_rhs(1:ng) = user_rhs(1:ng)*G
   
   IF(sgsmodel /= 0) CALL SGS_rhs (user_rhs, u_integrated, du, d2u)

  END FUNCTION user_rhs


  ! find Jacobian of Right Hand Side of the problem
  ! u_prev == u_prev_timestep_loc

  FUNCTION user_Drhs (u, u_prev, meth)
    USE penalization
    IMPLICIT NONE
    INTEGER, INTENT(IN) ::  meth
    !u_prev_timestep passed local to cast it into 2dim array to work with vector derivatives
    REAL (pr), DIMENSION (ng,ne) :: u, u_prev
    REAL (pr), DIMENSION (n) :: user_Drhs

    INTEGER :: ie, ie1, shift
    REAL (pr), DIMENSION (dim+1,ng,dim) :: du, d2u, du_prev
    REAL (pr), DIMENSION (ng,dim+2)     :: V, V_prev
    REAL (pr), DIMENSION (ng) :: Msk  
    

   !just for testing:

!!$   G = 1.0_pr
!!$    G = exp(-u_prev(:,n_var_phi)**2/del_fd**2)

    WHERE(u_prev(:,n_var_phi) <= 0.0_pr)
       Msk = 1.0_pr
    ELSEWHERE
       Msk = 0.0_pr
    ENDWHERE
    
    !calculation at previous time step  
    V_prev(:,ie_pressure) = 0.0_pr
    V_prev(:,ie_W) = 0.0_pr
    CALL vert_integ (u_prev, V_prev(:,1:ne_vertical), Msk, nwlt, ne, ne_vertical, HIGH_ORDER)
    V_prev(:,dim+1) = V_prev(:,ie_pressure)      ! pressures
    V_prev(:,dim+2) = V_prev(:,ie_W)             ! W velocity component
    V_prev(:,1:dim) = u_prev(:,1:dim) ! phi and 1-(dim-1) velocity components
    
    CALL c_diff_fast(V_prev(:,1:dim+1), du_prev, d2u, j_lev, ng, meth, 10, dim+1, 1, dim+1) !derivatives are calculated even for SGS terms
    V_prev(:,1:dim-1) = V_prev(:,2:dim) ! 1-(dim-1) velocity components
    V_prev(:,dim) = V_prev(:,dim+2)     ! dim - velocity component

    !calculation of preturbation
    V(:,ie_pressure) = 0.0_pr
    V(:,ie_W) = 0.0_pr
    CALL vert_integ (u, V(:,1:ne_vertical), Msk, nwlt, ne, ne_vertical, HIGH_ORDER)
    V(:,dim+1) = V(:,ie_pressure)      ! pressures
    V(:,dim+2) = V(:,ie_W)             ! W velocity component
    V(:,1:dim) = u(:,1:dim) ! phi and 1-(dim-1) velocity components
    
    CALL c_diff_fast(V(:,1:dim+1), du, d2u, j_lev, ng, meth, 11, dim+1, 1, dim+1) !derivatives are calculated even for SGS terms
    V(:,1:dim-1) = V(:,2:dim) ! 1-(dim-1) velocity components
    V(:,dim) = V(:,dim+2)     ! dim - velocity component
    
    !--Form right hand side of Navier-Stokes equations
    
    user_Drhs = 0.0_pr
    
    DO ie = 1, dim
       shift=(ie-1)*ng
       DO ie1 = 1,dim
          user_Drhs(shift+1:shift+ng) = user_Drhs(shift+1:shift+ng) - V(:,ie1)*du_prev(ie,:,ie1) & ! u_j' dU_i/dx_j 
                                                                    - V_prev(:,ie1)*du(ie,:,ie1) 
      END DO
   END DO
   !adding pressure gradient starting from 2nd equation for u
   DO ie = 1, dim-1 
      shift=ie*ng 
      user_Drhs(shift+1:shift+ng) = user_Drhs(shift+1:shift+ng) - du(dim+1,:,ie) 
   END DO
   DO ie = 1, dim !diffusion in phi for now
      shift=(ie-1)*ng
      user_Drhs(shift+1:shift+ng) = user_Drhs(shift+1:shift+ng) + SUM(d2u(ie,:,:),2)/Re 
   END DO
   
   IF(sgsmodel /= 0) CALL SGS_Drhs (user_Drhs, u, u_prev, du, d2u, meth)

 END FUNCTION user_Drhs


  !
  ! Uses u_prev_timestep, which is global flat (1D ) array set in time_step_cn()
  !
  FUNCTION user_Drhs_diag (meth)
    USE penalization
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: meth
    REAL (pr), DIMENSION (n) :: user_Drhs_diag

    INTEGER :: ie, ie1, shift,shiftIlm,shiftImm
    REAL (pr), DIMENSION (ng,dim) :: du, d2u
    REAL (pr), DIMENSION (dim+1,ng,dim) :: du_prev_timestep
    REAL (pr), DIMENSION (dim+1,ng,dim) :: du_dummy ! passed when only calculating 1st derivative.
    REAL (pr), DIMENSION (ng,dim+2)     :: V_prev
    REAL (pr), DIMENSION (ng) :: Msk

    
   shift = (n_var_phi-1)*ng
!!$   G = 1.0_pr
!!$    G = exp(-u_prev_timestep(shift+1:shift+ng)**2/del_fd**2)
   WHERE( u_prev_timestep(shift+1:shift+ng) <= 0.0_pr)
      Msk = 1.0_pr
   ELSEWHERE
      Msk = 0.0_pr
   ENDWHERE
     
   V_prev(:,ie_pressure) = 0.0_pr
   V_prev(:,ie_W) = 0.0_pr
   CALL vert_integ (u_prev_timestep(1:ne*ng), V_prev(:,1:ne_vertical), Msk, ng, ne, ne_vertical, HIGH_ORDER)
   V_prev(:,dim+1) = V_prev(:,ie_pressure)      ! pressures
   V_prev(:,dim+2) = V_prev(:,ie_W)             ! W velocity component
   DO ie = 1, dim
      shift=(ie-1)*ng
      V_prev(:,ie) = u_prev_timestep(shift+1:shift+ng)
   END DO
   CALL c_diff_fast(V_prev(:,1:dim+1), du_prev_timestep, du_dummy, j_lev, ng, meth, 11, dim+1, 1, dim+1)
   V_prev(:,1:dim-1) = V_prev(:,2:dim) ! 1-(dim-1) velocity components
   V_prev(:,dim) = V_prev(:,dim+2)     ! dim - velocity component
  
    !
    ! does not rely on u so we can call it once here
    !
    CALL c_diff_diag(du, d2u, j_lev, ng, meth, meth, 11)

    !--Form right hand side of Navier--Stokes equations
    user_Drhs_diag = 0.0_pr

    DO ie = 1, dim
       shift=(ie-1)*ng
       user_Drhs_diag(shift+1:shift+ng) = user_Drhs_diag(shift+1:shift+ng) - V_prev(:,ie)*du(:,ie) 
    END DO
    DO ie = 1, dim-1 ! only convective term in 1-(dim-1) equaions
       shift=ie*ng 
       user_Drhs_diag(shift+1:shift+ng) = user_Drhs_diag(shift+1:shift+ng) - du_prev_timestep(ie,:,ie) 
    END DO
    DO ie = 1, dim !diffusion in phi for now
       shift=(ie-1)*ng
       user_Drhs_diag(shift+1:shift+ng) = user_Drhs_diag(shift+1:shift+ng) + SUM(d2u(:,:),2)/Re 
    END DO

    IF(sgsmodel /= 0) CALL SGS_Drhs_diag (user_Drhs_diag, du_prev_timestep, du, d2u, meth)

  END FUNCTION user_Drhs_diag

  FUNCTION user_chi (nlocal, t_local)
    IMPLICIT NONE
    INTEGER,          INTENT (IN) :: nlocal
    REAL (pr),        INTENT (IN) :: t_local
    REAL (pr), DIMENSION (nlocal) :: user_chi
    !--Defines mask for sphere (3d) or cylinder (2d)

    user_chi = 0.0_pr !0.5_pr*(1.0_pr+SIGN(1.0_pr, R_obst**2-SUM(x**2,DIM=2)))

  END FUNCTION user_chi

  !
  ! Calculate any statitics
  !
  ! startup_flag - 0 when adapting to IC,then 1 inmain integration loop
  !
  !
  SUBROUTINE user_stats (u , j_mn, startup_flag)
!    USE fft_module
!    USE spectra_module
    USE wlt_vars
    USE vector_util_mod
    IMPLICIT NONE
    REAL (pr), DIMENSION (nwlt,1:n_var), INTENT (IN) :: u
    INTEGER , INTENT (IN) :: j_mn 
    INTEGER , INTENT (IN) :: startup_flag
    CHARACTER (LEN=256)  :: filename

    !================= OBSTACLE ====================
    REAL (pr)                       :: drag, lift
    REAL (pr), DIMENSION (dim)      :: force
    !==============================================
    
    !USER may define additional statistics ouutput here.

    WRITE(*,'(" ")')
    WRITE(*,'("****************** Turbulence Statistics on the Fly *******************")')

    IF (imask_obstacle) THEN !--Calculate force
       CALL user_cal_force (u(:,1:dim), nwlt, t, force, drag, lift)
       WRITE (6,'("Drag = ", es9.2, 1x, "Lift = ", es9.2)') drag, lift
       OPEN  (11, FILE = 'results/'//TRIM(file_gen)//'_user_stats', FORM='formatted', STATUS='old', POSITION='append')
       WRITE (11,'(3(es15.8,1x))') t, drag, lift
       CLOSE (11)
    END IF

    WRITE(*,'("***********************************************************************")')
    WRITE(*,'(" ")')

  END SUBROUTINE user_stats

  SUBROUTINE user_cal_force (u, n, t_local, force, drag, lift)
    !--Calculates drag and lift on obstacle using penalization formula
    USE penalization
    IMPLICIT NONE

    INTEGER, INTENT (IN) :: n
    INTEGER :: ie
    REAL (pr), INTENT (IN) :: t_local
    REAL (pr), DIMENSION (dim), INTENT (INOUT) :: force
    REAL (pr), INTENT (OUT) :: drag, lift
    REAL (pr), DIMENSION (n,dim) :: u
    REAL (pr) :: A

!    IF(dim == 3) THEN
!       A = pi * R_obst**2
!    ELSE IF(dim == 2) THEN
!       A = 2.0_pr* R_obst
!    ELSE
!       A = 1.0_pr
!    END IF
!   
!    DO ie = 1, dim
!       force(ie) = SUM(penal/eta_chi*(u(:,ie)+Umn(ie))*dA) / (0.5_pr * A * SUM(Umn(1:dim)**2))
!    END DO

!    drag = force(1) ; lift = force(2)
    
  END SUBROUTINE user_cal_force

  !
  ! calculate any additional variables
  ! 
  ! arg 
  ! flag  - 0 calledwhile adapting to IC, 1 - called in main time integration loop
  !
  ! These additiona variables are calculated and left in real space.
  !
  SUBROUTINE user_additional_vars( t_local, flag )
    IMPLICIT NONE
    REAL (pr), INTENT (IN) ::  t_local
    INTEGER , INTENT(IN) :: flag ! 0- called during adaption to IC, 1 called during main integration loop
    INTEGER :: i
    LOGICAL :: init_status

    PRINT *, 'user_additional_vars is running'
    IF(.NOT.init_status) THEN
       init_status=.TRUE.
       u(:,n_var_pressure) = 0.0_pr  
       u(:,n_var_U(dim)) =  0.0_pr
    END IF
    WHERE(u(:,n_var_phi) <= 0.0_pr)
       u(:,n_var_Msk) = 1.0_pr
    ELSEWHERE
       u(:,n_var_Msk) = 0.0_pr
    ENDWHERE

    IF(ALLOCATED(G) ) THEN
       IF(SIZE(G) /= nwlt ) THEN
          DEALLOCATE(G)
          ALLOCATE(G(1:nwlt))
       END IF
    ELSE 
       ALLOCATE(G(1:nwlt))
    END IF
 !!$   G = 1.0_pr
   G = exp(-u(:,n_var_phi)**2/del_fd**2)

    CALL vert_integ (u(:,1:n_integrated), u(:,n_integrated+1:n_integrated+ne_vertical), u(:,n_var_Msk), nwlt, n_integrated, ne_vertical, HIGH_ORDER)

  END SUBROUTINE user_additional_vars


  !
  ! calculate any additional scalar variables
  ! 
  ! arg 
  ! flag  - 0 calledwhile adapting to IC, 1 - called in main time integration loop
  !
  SUBROUTINE user_scalar_vars( flag )
    IMPLICIT NONE
    INTEGER , INTENT(IN) :: flag ! 0- called during adaption to IC, 1 called during main integration loop



  END SUBROUTINE user_scalar_vars

  !
  !************ Calculating Scales ***************************
  !
  ! Note the order of the components in the scl array
  ! correspond to u_tn, v_tn, w_tn, u_tn-1, v_tn-1, w_tn-1, u_tn-2, v_tn-2, w_tn-2
  ! 
  SUBROUTINE user_scales(flag, use_default, u_loc, nlocal, ne_local, l_n_var_adapt , l_n_var_adapt_index, &
       scl, scl_fltwt) !add
    USE parallel
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: flag ! 0 during initial adaptation, then 1 during time advancement
    LOGICAL , INTENT(INOUT) :: use_default
    INTEGER, INTENT (IN) :: nlocal, ne_local
    REAL (pr), DIMENSION (1:nlocal,1:ne_local), INTENT (IN) :: u_loc
    LOGICAL , INTENT (IN) :: l_n_var_adapt(ne_local)
    INTEGER , INTENT (IN) :: l_n_var_adapt_index(1:ne_local)
    REAL (pr), DIMENSION (1:ne_local), INTENT (INOUT) :: scl
    REAL (pr) ,  INTENT(IN) ::scl_fltwt !weight for temporal filter on scl
    REAL (pr) :: floor, tmp
    INTEGER :: ie, ie_index, itmp
    REAL (pr) ,save, ALLOCATABLE, DIMENSION (:) :: scl_old
    LOGICAL  ,save :: startup_init = .TRUE.

    !
    ! Ignore the output of this routine and use default scales routine
    !
    use_default = .FALSE. 

    !
    ! NOTE: For a parallel run, synchronize scl(:) across the processors in user_scales.
    !       Use the subroutine scales of default_util.f90 as an example,
    !       subroutine parallel_global_sum of parallel.f90 may be helpful.
    !       Already synchronized: sumdA_global
    IF( .NOT. use_default ) THEN

       IF (par_rank.EQ.0) PRINT *,'Use user_scales() <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<'
       !
       ! ALLOCATE scl_old if it was not done previously in user_scales
       !
       IF( startup_init .AND. .NOT. ALLOCATED(scl_old) ) THEN
          ALLOCATE( scl_old(1:ne_local) )
          scl_old = 0.0_pr
       END IF
       
       floor = 1.e-12_pr
       scl   =1.0_pr
       !
       ! Calculate scl per component
       !
       DO ie=1,ne_local
          IF(l_n_var_adapt(ie)) THEN
             ie_index = l_n_var_adapt_index(ie)
             IF( Scale_Meth == 1 ) THEN ! Use Linf scale
                scl(ie) = MAXVAL ( ABS( u_loc(1:nlocal,ie_index) ) )
                CALL parallel_global_sum( REALMAXVAL=scl(ie) )
                
             ELSE IF ( Scale_Meth == 2 ) THEN ! Use L2 scale
                scl(ie) = SUM( (u_loc(1:nlocal,ie_index)**2) )
                itmp = nlocal
                CALL parallel_global_sum( REAL=scl(ie) )
                CALL parallel_global_sum( INTEGER=itmp )
                scl(ie) = SQRT ( scl(ie)/itmp )
                
             ELSE IF ( Scale_Meth == 3 ) THEN ! Use L2 scale using dA
                scl(ie) = SUM( (u_loc(1:nlocal,ie_index)**2)*dA )
                CALL parallel_global_sum( REAL=scl(ie) )
                scl(ie) = SQRT ( scl(ie)/sumdA_global )
                
             ELSE
                IF (par_rank.EQ.0) THEN
                   PRINT *, 'Error in Scales(), Unknown Scale_Meth type: ', Scale_Meth
                   PRINT *, 'Known Scale_Meth types: 1 = Linf scale, 2 = L2 scale '
                   PRINT *, 'Exiting ...'
                END IF
                CALL parallel_finalize; STOP
             END IF
             
             IF (par_rank.EQ.0) THEN
                WRITE (6,'("Scaling before taking vector(1:dim) magnitude")')
                WRITE (6,'("Scale factor eqn", I4 ," = ", es11.4 ," scalecoeff = ",es11.4 )') &
                     ie, scl(ie), scaleCoeff(ie)
             END IF
             
          END IF
       END DO
       !
       ! take appropriate vector norm over scl(1:dim)
       IF( Scale_Meth == 1 ) THEN ! Use Linf scale
          tmp = MAXVAL(scl(2:dim)) !this is statistically equivalent to velocity vector length
          scl(2:dim) = tmp
       ELSE IF ( Scale_Meth == 2 ) THEN ! Use L2 scale
          tmp = SQRT(SUM( scl(2:dim)**2 ) ) !now velocity vector length, rather than individual component
          scl(2:dim) = tmp
       ELSE IF ( Scale_Meth == 3 ) THEN ! Use L2 scale using dA
          tmp = SQRT(SUM( scl(2:dim)**2 ) ) !now velocity vector length, rather than individual component
          scl(2:dim) = tmp
       END IF
       scl(n_var_U(dim)) = tmp
       scl(2:dim) = 1.0_pr
       !
       ! Print out new scl
       !
       DO ie=1,ne_local
          IF(l_n_var_adapt(ie)) THEN
             !this is done if one of the variable is exactly zero inorder not to adapt to the nois
             IF(scl(ie) .le. floor) scl(ie)=1.0_pr 
             tmp = scl(ie)
             ! temporally filter scl
             IF(.not. startup_init ) scl(ie) = scl_fltwt * scl_old(ie) + (1.0_pr - scl_fltwt) * scl(ie)
             scl = scaleCoeff * scl
             IF (par_rank.EQ.0) THEN
                WRITE (6,'("Scaling on vector(1:dim) magnitude")')
                WRITE (6,'("Scale factor eqn", I4 ," = ", es11.4 ," scalecoeff = ",es11.4 )') &
                     ie, scl(ie), scaleCoeff(ie)
                WRITE (6,'("(before filtering = ",es11.4,")" )') tmp
             END IF
          END IF
       END DO
       
       scl_old = scl !save scl for this time step
       startup_init = .FALSE.
       !PRINT *,'TEST scl_old ', scl_old
    END IF ! use default
    
  END SUBROUTINE user_scales

  SUBROUTINE user_cal_cfl (use_default, u, cfl_out)
    USE parallel
    IMPLICIT NONE
    LOGICAL , INTENT(INOUT) :: use_default
    REAL (pr),                                INTENT (INOUT) :: cfl_out
    REAL (pr), DIMENSION (nwlt,n_integrated), INTENT (IN)    :: u
    
    INTEGER                    :: i
    REAL (pr)                  :: floor
    REAL (pr), DIMENSION (dim) :: cfl
    REAL (pr), DIMENSION(dim,nwlt) :: h_arr
    
    use_default = .FALSE.
    
    floor = 1e-12_pr
    cfl_out = floor
    
    CALL get_all_local_h (h_arr)
    
    DO i = 1, nwlt
       cfl(1:dim) = ABS (u(i,1:dim)+Umn(1:dim)) * dt/h_arr(1:dim,i)
       cfl_out = MAX (cfl_out, MAXVAL(cfl))
    END DO
    CALL parallel_global_sum( REALMAXVAL=cfl_out )
    
  END SUBROUTINE user_cal_cfl
  
  SUBROUTINE  user_sgs_force ( u, nlocal)
    IMPLICIT NONE
    
    INTEGER,                         INTENT (IN) :: nlocal
    REAL (pr), DIMENSION (nlocal,n_integrated), INTENT (INOUT) :: u
    
    !default SGS_force, can be explicitely deifined below instead of calling default SGS_force
    IF(sgsmodel /= 0) CALL sgs_force ( u, nlocal)
    
  END SUBROUTINE user_sgs_force

  FUNCTION user_sound_speed (u, neq, nwlt)
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: nwlt, neq
    REAL (pr), DIMENSION (nwlt,neq), INTENT(IN) :: u
    REAL (pr), DIMENSION (nwlt) :: user_sound_speed

    !user_sound_speed(:) = SQRT( gamma*(gamma-1.0_pr)* &
    !       ( u(:,4)-0.5_pr*SUM(u(:,n_var_mom(1:dim))**2,DIM=2)/u(:,1)) ) ! pressure

    user_sound_speed(:) = 0.0_pr

  END FUNCTION user_sound_speed



!!!!!!!!!!!!!!!!!!!
  SUBROUTINE vert_integ (u_local, u_vert, u_mask, nlocal, ne_local, ne_vert, meth_order)
    !--Makes u divergence free
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: meth_order, nlocal, ne_local, ne_vert
    REAL (pr), DIMENSION (nlocal,ne_local), INTENT(IN) :: u_local
    REAL (pr), DIMENSION (nlocal,ne_vert), INTENT(INOUT) :: u_vert
    REAL (pr), DIMENSION (nlocal), INTENT(INOUT) :: u_mask
    REAL (pr), DIMENSION (nlocal,ne_vert) :: f_local
    REAL (pr), DIMENSION(ne_vert) :: scl_u_local
    INTEGER, DIMENSION(ne_vert) :: clip
    
    f_local = rhs_vert_integ (u_local(:,1:dim), u_mask, nlocal, dim, ne_vert, meth_order)
    clip = 0
    scl_u_local = MAXVAL(scl_global(n_integrated+1:n_integrated+ne_vert)) !scale of pressure increment based on dynamic pressure

    CALL Linsolve (u_vert, f_local, tol2, nlocal, ne_vert, clip, L_vert_integ, L_vert_integ_diag, SCL=scl_u_local(1)) !new with scaling 
!!$    CALL GMRES (j_lev, 10, nlocal, ne_local, HIGH_ORDER, clip, u_vert, f_local, L_vert_integ, L_vert_integ_diag)
!!$    CALL BiCGSTAB (j_lev, nlocal, ne_local, 100, HIGH_ORDER, clip, tol2, u_vert, f_local, L_vert_integ, L_vert_integ_diag, SCL=scl_u_local(1))

 
  END SUBROUTINE vert_integ

  FUNCTION L_vert_integ (jlev, u_local, nlocal, ne_vert, meth_order)
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: jlev, nlocal, ne_vert, meth_order
    REAL (pr), DIMENSION (nlocal*ne_vert), INTENT (INOUT) :: u_local
    REAL (pr), DIMENSION (nlocal*ne_vert) :: L_vert_integ

    INTEGER :: i, ii, ie, idim, shift
    INTEGER :: meth_central, meth_backward, meth_forward 
    REAL (pr), DIMENSION (nlocal, dim) :: du, d2u
    INTEGER :: face_type, nloc, ne_one
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc
    INTEGER, DIMENSION(ne_vert) :: meth_u

    meth_central  = meth_order + BIASING_NONE
    meth_backward = meth_order + BIASING_BACKWARD
    meth_forward  = meth_order + BIASING_FORWARD
    DO ie =1, ne_vert
       IF(ie == ie_pressure) meth_u(ie) = meth_forward
       IF(ie == ie_W)        meth_u(ie) = meth_backward
    END DO
    ne_one = 1


    DO ie = 1, ne_vert
       shift=(ie-1)*nlocal
       !--Internal points
       CALL c_diff_fast (u_local(shift+1:shift+nlocal), du, d2u, jlev, nlocal, meth_u(ie), 10, ne_one, 1, ne_one)
       L_vert_integ(shift+1:shift+nlocal) = du( :, dim)
       !--Boundary points
       i_p_face(0) = 1
       DO i=1,dim
          i_p_face(i) = i_p_face(i-1)*3
       END DO
       DO face_type = 0, 3**dim - 1
          face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
          IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
             CALL get_all_indices_by_face (face_type, jlev, nloc, iloc)
             IF(nloc > 0 ) THEN ! ANY dimension
                IF( face(dim) == 1 .AND. ie == ie_pressure ) THEN                      ! top boundary (entire face) 
                   L_vert_integ(shift+iloc(1:nloc)) = u_local(shift+iloc(1:nloc))  ! Dirichlet conditions
                ELSE IF( face(dim) == -1 .AND. ie == ie_W ) THEN                ! bottom boundary (entire face) 
                   L_vert_integ(shift+iloc(1:nloc)) = u_local(shift+iloc(1:nloc))  ! Dirichlet conditions
                END IF
             END IF
          END IF
       END DO
    END DO
  END FUNCTION L_vert_integ

  FUNCTION L_vert_integ_diag (jlev, nlocal, ne_vert, meth_order)
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: jlev, nlocal, ne_vert, meth_order
    REAL (pr), DIMENSION (nlocal*ne_vert) :: L_vert_integ_diag

    INTEGER :: i, ii, ie, idim, shift
    INTEGER :: meth_central, meth_backward, meth_forward 
    REAL (pr), DIMENSION (nlocal, dim) :: du, d2u
    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc
    INTEGER, DIMENSION(ne_vert) :: meth_u

    meth_central  = meth_order + BIASING_NONE
    meth_backward = meth_order + BIASING_BACKWARD
    meth_forward  = meth_order + BIASING_FORWARD
    DO ie =1, ne_vert
       IF(ie == ie_pressure) meth_u(ie) = meth_forward
       IF(ie == ie_W) meth_u(ie) = meth_backward
    END DO

    DO ie = 1, ne_vert
       shift=(ie-1)*nlocal
       !--Internal points
       CALL c_diff_diag ( du, d2u, jlev, nlocal, meth_u(ie), meth_u(ie), 10)
       L_vert_integ_diag(shift+1:shift+nlocal) = du(:, dim)
       !--Boundary points
       i_p_face(0) = 1
       DO i=1,dim
          i_p_face(i) = i_p_face(i-1)*3
       END DO
       DO face_type = 0, 3**dim - 1
          face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
          IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
             CALL get_all_indices_by_face (face_type, jlev, nloc, iloc)
             IF(nloc > 0 ) THEN ! ANY dimension
                IF( face(dim) == 1 .AND. ie == ie_pressure ) THEN                      ! top boundary (entire face) 
                   L_vert_integ_diag(shift+iloc(1:nloc)) = 1.0_pr                 ! Dirichlet conditions
                ELSE IF( face(dim) == -1 .AND. ie == ie_W ) THEN                ! bottom boundary (entire face) 
                   L_vert_integ_diag(shift+iloc(1:nloc)) = 1.0_pr  ! Dirichlet conditions
                END IF
             END IF
          END IF
       END DO
    END DO
    
    IF(MINVAL(ABS(L_vert_integ_diag)) < 1.e-12_pr) THEN
	   PRINT *, 'RANGE of L_vert_integ_daig=',  MINVAL(ABS(L_vert_integ_diag)),  MAXVAL(ABS(L_vert_integ_diag))
       PAUSE
    END IF

  END FUNCTION L_vert_integ_diag


  FUNCTION rhs_vert_integ (u_local, u_mask, nlocal, ne_local, ne_vert, meth_order)
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: meth_order, nlocal, ne_local, ne_vert
    REAL (pr), DIMENSION (nlocal,ne_local), INTENT(IN) :: u_local
    REAL (pr), DIMENSION (nlocal), INTENT(INOUT) :: u_mask
    REAL (pr), DIMENSION (nlocal,ne_vert) :: rhs_vert_integ
    REAL (pr), DIMENSION (dim-1, nlocal, dim) :: du, d2u

    INTEGER :: i, ii, ie, idim, shift
    INTEGER :: meth_central, meth_backward, meth_forward 
    INTEGER :: face_type, nloc, ne_h
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc
    INTEGER, DIMENSION(ne_local) :: meth_u


    meth_central  = meth_order + BIASING_NONE
    meth_backward = meth_order + BIASING_BACKWARD
    meth_forward  = meth_order + BIASING_FORWARD
    DO ie =1, ne_vert
       IF(ie == ie_pressure) meth_u(ie) = meth_forward
       IF(ie == ie_W) meth_u(ie) = meth_backward
    END DO

    CALL c_diff_fast (u_local(:,2:dim), du, d2u, j_lev, nlocal, meth_central, 10, dim-1, 1, dim-1)
       
    DO ie = 1, ne_vert
       shift=(ie-1)*nlocal
	   !--Internal points
       IF(ie == ie_pressure) rhs_vert_integ(:,ie) = -u_mask/Fr**2
       IF(ie == ie_W) THEN
          rhs_vert_integ(:,ie) = 0.0_pr
          DO i = 1, dim -1
             rhs_vert_integ(:,ie) = rhs_vert_integ(:,ie) - u_mask*du(i,:,i)
          END DO
       END IF
       !--Boundary points
       i_p_face(0) = 1
       DO i=1,dim
          i_p_face(i) = i_p_face(i-1)*3
       END DO
       DO face_type = 0, 3**dim - 1
          face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
          IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
             CALL get_all_indices_by_face (face_type, j_lev, nloc, iloc)
             IF(nloc > 0 ) THEN ! ANY dimension
                IF( face(dim) == 1 .AND. ie == ie_pressure ) THEN                      ! top boundary (entire face) 
                   rhs_vert_integ(iloc(1:nloc),ie) = 0.0_pr  ! Dirichlet conditions
                ELSE IF( face(dim) == -1 .AND. ie == ie_W ) THEN                ! bottom boundary (entire face)
                   rhs_vert_integ(iloc(1:nloc),ie) = 0.0_pr  ! Dirichlet conditions
                END IF
             END IF
          END IF
       END DO
    END DO

  END FUNCTION rhs_vert_integ

  SUBROUTINE  user_pre_process
    IMPLICIT NONE
     
  END SUBROUTINE user_pre_process

  SUBROUTINE  user_post_process
    IMPLICIT NONE
    
  END SUBROUTINE user_post_process

END MODULE user_case





