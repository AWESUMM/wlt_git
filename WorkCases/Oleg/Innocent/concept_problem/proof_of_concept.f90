!> \brief Module user_case, allows user to describe its case by writing th body of subroutines used by the solver.
!! \detail User should provide body of routines defined here. These routines are named user_xxx and are call by the solver.
!! many variables imported from included modules are availaible as global variables.
!! - ne number of equation defining the problems, this is usually equql to the numbers of unknowns variable to solve
!! - ng number of discrete values defining each single unknown variable
!! - dim dimension of the physical space in which the problem is being solved
!! - t current integration time
!! - 
!! - 
!! - 
!! - 
!! - 
!! - 
!! - 
!! - 
!! - 
!! - 
!! - 
!! 
!<
MODULE user_case
  !
  ! Case elliptic poisson 
  USE precision
  USE wlt_vars
  USE wavelet_filters_mod
  USE elliptic_mod
  USE elliptic_vars
  USE wlt_trns_mod									
  USE wlt_trns_vars
  USE wlt_trns_util_mod
  USE io_3d_vars
  USE util_mod
  USE util_vars
  USE share_consts
  USE pde
  USE sizes
  USE share_kry
  USE vector_util_mod
  USE field
  USE input_file_reader

  !constants
  INTEGER, PARAMETER :: NB_PARAM = 3
  CHARACTER (LEN=*), PARAMETER :: SCREEN = "SCREEN",&
                                  IFMT = '(I10)',&
                                  OBS_FILE = 'obs.dat'
  !
  ! case specific variables
  !
  INTEGER n_var_vorticity ! start of vorticity in u array
  INTEGER n_var_pressure  ! start of pressure in u array

  !variables for the minimizer
  INTEGER, PARAMETER :: sp = 4 !> simple precision for real variables
  INTEGER, PARAMETER :: dp = 8 !> double precision for real variables
  INTEGER, PARAMETER :: cp = pr !> computing precision for real variables
  INTEGER, PARAMETER ::  &
       M1QN3_DIRECT   = 0,& !> direct communication in M1QN3, used for parameter reverse
       M1QN3_REVERSE  = 1,& !> reverse communication in M1QN3, used for parameter reverse
       M1QN3_NOTHING  = 1,& !> nothing required by M1QN3, used for parameter indic
       M1QN3_COSTGRAD = 4   !> M1QN3 requires cost function and its gradient, used for parameter indic
!!$  !> brief Name of the subroutine that provide the cost function and its gradient ine direct communication
!!$  !! \detail In direct communication, simul is the name of the simulator inside m1qn3.
!!$  !! The simulator is the subroutine that computes the value of the (cost) function f and
!!$  !! the value of its gradient g at the current iterate. When m1qn3 needs these values, it executes the instruction
!!$  !! call simulator (...)
!!$  !! In reverse communication, the empty subroutine simul_rc, provided in the standard distribution can be used
!!$  !! EXTERNAL simul_rc
!!$  !<
!!$  EXTERNAL simulator
!!$  !> \brief Calling name of the subroutine that computes the inner product <u, v> of two vectors u and v of Rn.
!!$  !! \detail This subroutine is supposed to have the following declaration statement:
!!$  !! subroutine prosca (n, u, v, ps, izs, rzs, dzs).
!!$  !<
!!$  EXTERNAL prosca
!!$  !> \brief Calling name of the subroutine that makes the change of variable
!!$  !! \detail This subroutine is used only in DIS mode. It is supposed to have the following declaration statement:
!!$  !!   subroutine ctonb (n, x, y, izs, rzs, dzs).
!!$  !<
!!$  EXTERNAL ctonb
!!$  !> \brief Calling name of the subroutine that does the operation reverse to the one done by ctonb
!!$  !! \detail This subroutine is used only in DIS mode. It is supposed to have the following declaration statement:
!!$  !!  subroutine ctcab (n, y, x, izs, rzs, dzs).
!!$  !<
!!$  EXTERNAL ctcab
  !> \brief Positive integer variable that gives the numbers of vector pairs used to approximate the Hessian.
  !! \detail
  !<
  INTEGER :: ig_npairs = 2
  !> \brief Positive integer variable that gives the size of the control vector or the dimension of the problem.
  !! \detail
  !<
  INTEGER :: ig_nctl
  !> \brief Control vector
  !! \detail It must be an array of dimension ig_nctl. On entry, it is supposed to be the initial value.
  !! On return, it is the value of the final point calculated by m1qn3.
  !<
  REAL(KIND=cp), DIMENSION(:), ALLOCATABLE  :: rga_ctl
  
  !> \brief
  !! \detail On entry, it is supposed to be the value of the cost function evaluated at the initial value of the control variable
  !! On return, it is the value of f at the final point.
  !<
  REAL(KIND=cp) :: rg_cost
  
  !> \brief Array variable for the gradient of the cost function
  !! \detail On entry, it is supposed to be the value of the gradient of the cost function at the initial control variable.
  !! On return with omode = 1 (see below), it is the value of the gradient of the cost function at the final point.
  !! For other output modes, its value is undetermined.
  !<
  REAL(KIND=cp), DIMENSION(:), ALLOCATABLE  :: rga_grad
  
  !> \brief Resolution in the control vector for the l$B!g(B norm (positive)
  !! \detail two points whose distance in the sup-norm is less than dxmin will be considered as indistinguishable by the optimizer.
  !<
  REAL(KIND=cp) :: rg_xmin
  
  !> \brief Estimation of the expected decrease in the cost function during the first iteration (positive)
  !! \detail
  !<
  REAL(KIND=cp) :: rg_df1
  
  !> \brief Stopping criterion that is based on the norm of the gradient of the cost function
  !! \detail value range is ]0, 1[
  !!
  !<
  REAL(KIND=cp) :: rg_epsg
  
  !> \brief norm that is used to test optimality (see the argument rg_epsg).
  !! \detail It can be one of the following strings:
  !! - .FN!twoN" denotes the Euclidean or $(C'$(B2 norm
  !! - .FN!supN" denotes the sup or $(C'$$B!g(B norm
  !! - .FN!dfnN" denotes the norm k associated with the scalar product defined in the user-supplied subroutine prosca
  !<
  CHARACTER(LEN=3) :: ala_norm
  
  !> \brief variable that controls the outputs on channel io
  !! \detail
  !! 0 : No print.
  !! >= 1 Initial and final printouts, error messages.
  !! >= 3 One line of printout per iteration that gives: the index k of the iteration going from the point xk to the point xk+1
  !!      the number of time the simulator has been called, the value f(xk) of the objective function and the directional derivative
  !! >= 4 Print outs from mlis3 during the line-search: see the write-up on mlis3 in modulopt library.
  !! >= 5 Some more printouts at the end of iteration k (see m1qn3 doc for details)
  !<
  INTEGER :: ig_impres
  
  !> \brief variable that will be taken as the channel number for the outputs
  !! \detail
  !<
  INTEGER :: ig_m1qn3io
  
  !> \brief  Input mode of m1qn3 that tunes its behavior.
  !! \detail Integer array of dimension 3 with the following values are meaningful.
  !! imode(1) determines the scaling mode of m1qn3.
  !!   - M1qn3 will run in DIS (recommended) mode if imode(1) = 0
  !!   - M1qn3 will run in SIS mode if imode(1) = 1
  !! imode(2) specifies the starting mode of m1qn3.
  !!   - A cold start is performed if imode(2) = 0: the first descent direction is then $B!](Bg1.
  !!   - A warm start is performed if imode(2) = 1: the first descent direction is $B!](BW1g1 (W1 Hessian approx)
  !! imode(3) specifies in direct communication when the simulator has to be called with indic = 1 or
  !!   similarly in reverse communication, when m1qn3 returns to the calling subroutine with indic = 1.
  !!   When imode(3) = 0, the simulator is never called with indic = 1
  !!   When imode(3) > 0, the simulator is called with indic = 1 every imode(3) iteration(s), starting at iteration 1.
  !<
  INTEGER, DIMENSION(3) :: iga_imode
  
  !> \brief output mode of m1qn3
  !! \detail Meaningful values
  !! = 0: The simulator asks to stop by returning the value indic = 0.
    !! = 1: This is the normal way of stopping for m1qn3: the test on the gradient is satisfied (see the meaning of rg_epsg).
  !! = 2: One of the input arguments is not well initialized. This can be:
  !!  - n <= 0, niter <= 0, nsim <= 0, dxmin <= 0.0 or epsg mot in ]0, 1[,
  !!  - ndz < 5n + 1 (in SIS mode) or ndz < 6n + 1 (in DIS mode): not enough storage in memory
  !!  - the contents of iz is not correct for a warm restart,
  !!  - the starting point is almost optimal (the norm of the initial gradient is less than 10$B!](B20).
  !! = 3: The line-search is blocked on tmax = 1020 (see section 4.4 and then documentation on mlis3 in modulopt library).
  !! = 4: The maximal number of iterations is reached.
  !! = 5: The maximal number of simulations is reached.
  !! = 6: Stop on dxmin during the line-search (see section 4.4 of m1qn3 doc).
  !! = 7: Either hg, di is nonnegative or hy, si is nonpositive (see section 4.4 of m1qn3 doc).
  !! For additional information and comments, see section 4  of m1qn3 doc.
  !<
  INTEGER :: ig_omode
  
  !> \brief Maximal number of iterations accepted from m1qn3.
  !! \detail m1qn3 uses this variable to return the number of iterations really done
  !<
  INTEGER :: ig_niter
  
  !> \brief Maximal number of simulations accepted from m1qn3.
  !! \detail m1qn3 uses this variable to return the number of simulations really done
  !<
  INTEGER :: ig_nsimul
  
  !> \brief working array for m1qn3
  !! \detail
  !<
  INTEGER, DIMENSION(5) :: iga_iz
  
  !> \brief working array for m1qn3
  !! \detail this array is of size ndz
  !<
  INTEGER, DIMENSION(:), ALLOCATABLE :: rga_dz
  
  !> \brief Size of the working array rga_dz for m1qn3
  !! \detail In SIS mode, m1qn3 needs a working area formed of at least 3 vectors of
  !! dimension n (dk, gk and an auxiliary vector) and it needs for each update (Hessian approx)
  !! one scalar and two vectors. Therefore, if m is the desired number of updates for forming the matrix Wk(Hessian approx),
  !! it is necessary to have: ig_ndz >= 3n +m(2n + 1). m1qn3 based its calculation on this value to determine m.
  !!  if ndz is less than 5n + 1, m1qn3 stops with omode = 2.
  !! In DIS mode, m1qn3 needs an additional vector of dimension n for storing Dk. So, take ig_ndz >= 4n + m(2n + 1) and m >= 1.
  !<
  INTEGER :: ig_ndz
  
  !> \brief Specify whether direct or reverse communication is desired for m1qn3
  !! \detail In reverse communication, it is used to communicate with the call loop
  !! values :
  !! < 0: implies that m1qn3 will stop immediately using the instruction stop, to prevent entering an infinite call loop in reverse communication,
  !!      due to the fact that the calling program has not left the call loop when m1qn3 returns a negative value;
  !! = 0: indicates that m1qn3 has to work in direct communication;
  !! = 1: indicates that m1qn3 has to work in reverse communication.
  !! it is used by m1qn3 on return to send informations to the calling loop, values
  !! < 0: when m1qn3 has terminated, in which case the call loop must be interrupted;
  !! = 1: the call loop must be pursued.
  !<
  INTEGER :: ig_reverse
  
  !> \brief Indicates the state of the computation required by m1qn3
  !! \detail values
  !! < 0: the computation of f and g required by m1qn3 on its last return was not possible at the given control variable
    !!      this indicates to m1qn3 to adjust the step-size;
  !! = 0: m1qn3 has to stop, for example because some events that m1qn3 cannot understand (not in the field of optimization) has occurred;
  !! > 0: the required computation has been done.
  !! m1qn3 also uses this variable to send information to the calling loo (reverse mode) or simulator (direct mode)
  !! values :
  !! = 1: means that the calling program can do anything except changing the values of indic, n, x, f, and g;
  !!      this value of indic is used by m1qn3 every imode(3) iteration(s), when imode(3) > 0, and never, when imode(3) = 0;
  !! = 4: means that the calling program has to compute f and g, to put them in f and g, and to call back m1qn3.
  !<
  INTEGER :: ig_indic
  
  !> \brief Integer working array for simulator, prosca, ctonb, and ctcab
  !! \detail not used in practice
    !<
  INTEGER, DIMENSION(2)  :: iga_wa !(izs in m1qn3
  
  !> \brief Real working array for simulator, prosca, ctonb, and ctcab
  !! \detail not used in practice
  !<
  REAL, DIMENSION(sp)  :: rga_wa !(rzs in m1qn3)
  
  !> \brief Real working array for simulator, prosca, ctonb, and ctcab
  !! \detail not used in practice
  !<
  REAL(KIND=dp), DIMENSION(2)  :: dga_wa !(dzs in m1qn3)

  !!!!!!!!!!!!!!!!!!!!!!!!
  REAL(pr) :: rg_costb
  REAL(pr), PRIVATE :: rg_mu, rg_sigma, rg_v0, rg_omega, rg_alpha
  INTEGER , PRIVATE :: ig_var_v_idx,&! index of the additional variable for advection velocity
                       ig_jmin,& !min wavelet level
                       ig_jmax   !max wavelet level
  INTEGER , PRIVATE, DIMENSION(3) :: iga_Mvector !> Wavelets count at coarse scale
  LOGICAL :: lg_assim               ,& !> running data assimilation experiment
             lg_direct              ,& !> running the direct model
             lg_adjoint             ,& !> running the adjoint model
             lg_cost                ,& !> computation of the cost function
             lg_grad                ,& !> computation of the gradient of the cost function
             lg_cost_ok             ,& !> The cost function is computed
             lg_grad_ok             ,& !> The gradient of the cost function is computed
             lg_m1qn3_initialization,& !> m1qn3 initialization runing
             lg_run_m1qn3           ,& !> requires m1qn3 call
             lg_simulator           ,& !> call simulator
             lg_make_obs            ,& !> make observations for twin experiments?
             lg_obs_gap             ,& !> make observations for twin experiments?
             lg_amplitude           ,& !>
             lg_location            ,& !>
             lg_sigma                  !>

  REAL(pr), PRIVATE, DIMENSION(3) :: rga_x0, rga_xmin, rga_xmax
  INTEGER , PRIVATE, DIMENSION(NB_PARAM) :: iga_idx_param
  REAL(pr), PRIVATE, DIMENSION(NB_PARAM) :: rga_sigmaP,& !> standard deviation of the params background error
                                     rga_param,&
                                     rga_b_param,&
                                     rga_paramt,&
                                     rga_sigma_param

  REAL(pr), PRIVATE :: rga_sigmaB,&
                       rg_sigmaR !> standard deviation of the observation error
  !
  !variables for data assimilation
  INTEGER , PRIVATE :: im_ctl    !> size of the control vector, to be loaded from input file
  REAL(pr), PRIVATE, POINTER, DIMENSION(:) :: rga_Bm1 !> control vector and background covarriance matrix
  !Observations information, observation locations are supposed to be regular in space and time
  INTEGER, PRIVATE :: ig_nobs_x, ig_nobs_t !> observation count in space and time dimension, to be loaded from input file
  REAL(pr), PRIVATE, POINTER, DIMENSION(:) :: rga_obs_t, rga_obs_x
  REAL(pr), PRIVATE, POINTER, DIMENSION(:) :: rga_uobs, rga_ugap, rga_Rm1!> observation and observation covariance matrix
  INTEGER , PRIVATE, POINTER, DIMENSION(:) :: iga_H !> observation operator, indices of observation in the state vector
  INTEGER , PRIVATE, POINTER, DIMENSION(:) :: iga_obs_idx !> observation indices
  INTEGER , PRIVATE, POINTER, DIMENSION(:, :) :: iga_obs_coord !> observation coordinates

  !
  ! local variables
  !
  REAL(pr)  :: nu
  

  LOGICAL :: divgrad

  ! debug flag to be read from .inp file to test database independent interpolation
  LOGICAL, PRIVATE :: test_interpolate

CONTAINS

  !
  ! In user_setup_pde() we setup how many variables are integrated, which are interpolated
  ! to the next times step and if any exeact solution exists to compare to during the run.
  ! We also set the variable names that are used when the result files are written.
  !
  ! The following variables must be setup in this routine:
  !
  !
  ! n_integrated     ! first n_integrated eqns will be acted on for time integration
  ! n_var_additional ! 
  ! n_var
  !
  !
  !
  !
  SUBROUTINE  user_setup_pde ( VERB ) 
    IMPLICIT NONE
    LOGICAL, OPTIONAL :: VERB         ! print debug info
    LOGICAL :: do_verb
    INTEGER :: i
    
    do_verb = .TRUE.
    IF (PRESENT(VERB)) do_verb = VERB
    
    IF (do_verb) THEN
       PRINT * ,''
       PRINT *, '**********************Setting up PDE*****************'
       PRINT * ,'CASE Elliptic '
       PRINT *, '*****************************************************'
    END IF

    n_integrated = 2 ! dim
    n_time_levels = 1  !--# time levels

    n_var_time_levels = n_time_levels * n_integrated !--Number of equations (2D velocity at two time levels)

    n_var_additional = 1 !advection velocity
    n_var = n_var_time_levels + n_var_additional !--Total number of variables

    n_var_exact = 1 ! 0 <--> No exact solution

    ig_var_v_idx = n_integrated + 1
    n_var_pressure  = n_var_time_levels ! ! no pressure

    !
    !--Allocate logical arrays 
    !  Must be done after setting number of different types of variables
    !  and before setting logical variable mapping arrays
    !
    CALL alloc_variable_mappings


    !
    ! Fill in variable names for default variables for integration meth 2
    ! These may be changed in the call to the user specified routine
    !  user_setup_pde()
    !
    ! In integrated variables (3D)
    IF( dim == 3 ) THEN
       WRITE (u_variable_names(1), u_variable_names_fmt)  'u'
       WRITE (u_variable_names(2), u_variable_names_fmt)  'p'
    ELSE IF(dim == 2) THEN
       WRITE (u_variable_names(1), u_variable_names_fmt)  'u'
       WRITE (u_variable_names(2), u_variable_names_fmt)  'p'
    ELSE IF(dim == 1) THEN
       WRITE (u_variable_names(1), u_variable_names_fmt)  'u'
       WRITE (u_variable_names(2), u_variable_names_fmt)  'p'
    END IF

    WRITE (u_variable_names(ig_var_v_idx), u_variable_names_fmt) 'w'

    !
    ! setup logical variable mapping arrays
    ! This defined which variables are used for adaptation, Which need to be interpololated to the new grid at 
    ! each time step, which are saved, and for which we have an exect solutiuon we want to check at each time step
    !


    !
    ! setup which components we will base grid adaptation on.
    ! Adapt on the 3 velocity components
    !
    n_var_adapt(1:1,0) = .TRUE. !--Initially adapt on integrated variables at first time level
    n_var_adapt(1:1,1) = .TRUE. !--After first time step adapt on 

    !Variables that need to be interpoleted to new adapted grid in initial grid adaptation
    n_var_interpolate(1:n_var,0) = .TRUE. 

    !Variables that need to be interpoleted to new adapted grid at each time step
    n_var_interpolate(1:n_var,1) = .TRUE. 

    !
    ! setup which components we have an exact solution for
    !
    n_var_exact_soln(:,0:1) = .TRUE.



    !
    ! setup which variables we will save the solution
    !
    n_var_save(:) = .TRUE. ! save all for restarting code


    ! Setup which variables are required on restart
    !
    n_var_req_restart = .FALSE.
    n_var_req_restart(1:n_integrated) = .TRUE.
    !
    !--Time level counters for NS integration
    !   Used for integration meth2
    ! n0 is current time step
    ! n1 is t_n-1 time step
    ! n2 is t_n-2 time step
    !
    n0 = 1; n1 = n0+dim; n2 = n1+dim


    !
    ! Set the maximum number of components for which we have an exact solution
    !
    n_var_exact = 1 ! MAX( COUNT(n_var_exact_soln(:,0)),COUNT(n_var_exact_soln(:,1)) )

    !
    ! Setup a scaleCoeff array of we want to tweak the scaling of a variable
    ! ( if scaleCoeff < 1.0 then more points will be added to grid 
    !
    ALLOCATE ( scaleCoeff(1:n_var), Umn(1:n_var) )
    scaleCoeff = 1.0_pr

    IF (do_verb) THEN
       PRINT *, 'n_integrated = ',n_integrated 
       PRINT *, 'n_time_levels = ',n_time_levels
       PRINT *, 'n_var_time_levels = ',n_var_time_levels 
       PRINT *, 'n_var = ',n_var 
       PRINT *, 'n_var_exact = ',n_var_exact 
       PRINT *, '*******************Variable Names*******************'
       DO i = 1,n_var
          WRITE (*, u_variable_names_fmt) u_variable_names(i)
       END DO
       PRINT *, '****************************************************'
    END IF

  END SUBROUTINE  user_setup_pde

  !> \brief read input variables from input file ("case_name"_pde.inp) 
  !!
  !! \detail
  !! Read input from "case_name"_pde.inp file
  !! case_name is in string file_name read from command line
  !! in read_command_line_input()
  !< 
  SUBROUTINE user_read_input()
    IMPLICIT NONE
    INTEGER il_dim
    
    call input_real ('nu',nu,'stop', &
         ' nu: viscosity coefficient')
    
    call input_real ('rg_mu',rg_mu,'stop', &
         ' rg_mu: diffusion coefficient')
    
    call input_real ('rg_sigma', rg_sigma,'stop', &
         ' rg_sigma: exponential function corfficient')
    
    call input_real ('rg_v0', rg_v0,'stop', &
         ' rg_v0: advection velocity factor')
    
    call input_real ('rg_omega', rg_omega,'stop', &
         ' rg_omega: advection velocity factor')
    
    call input_integer ('dimension', il_dim, 'stop', 'dim : dimension of the problem')
    
    call input_integer ('J_MN', ig_jmin, 'stop', 'ig_jmin : wavelet min level')
    
    call input_integer ('J_MX', ig_jmax, 'stop', 'ig_jmax : wavelet max level')
    
    call input_integer_vector ('M_vector', iga_Mvector, il_dim, 'stop', 'M_vector : wvlt at coarse')
    
    call input_real_vector ('coord_min', rga_xmin, il_dim, 'stop', ' rga_xmin: ')
    
    call input_real_vector ('coord_max', rga_xmax, il_dim, 'stop', ' rga_xmax: ')
    
    call input_logical ('lg_direct', lg_direct,'stop', 'lg_direct : running direct model')
    
    call input_logical ('lg_adjoint', lg_adjoint,'stop', 'lg_adjoint : running adjoint model')
    
    call input_logical ('lg_assim', lg_assim,'stop', 'lg_assim : running data assimilation?')
    
    call input_logical ('lg_make_obs', lg_make_obs,'stop', 'lg_make_obs : make twin obs?')

    !lg_make_obs

    call input_integer ('ig_nctl', ig_nctl, 'stop', 'ig_nctl : size of the control vector')

    call input_integer ('ig_nobs_x', ig_nobs_x, 'stop', 'ig_nobs_x : size of the control vector')

    call input_integer ('ig_nobs_t', ig_nobs_t, 'stop', 'ig_nobs_t : size of the control vector')

!!$    ALLOCATE( rga_obs_x(ig_nobs_x), rga_obs_t(ig_nobs_t) )
!!$    
!!$    CALL input_real_vector ('rga_obs_x', rga_obs_x, ig_nobs_x, 'stop', 'rga_obs_x : control params')
!!$    
!!$    CALL input_real_vector ('rga_obs_t', rga_obs_t, ig_nobs_t , 'stop', 'rga_obs_t : control params')

    call input_real ('rg_sigmaR', rg_sigmaR, 'stop', 'rg_sigmaR : size of the control vector')

    call input_logical ('lg_amplitude', lg_amplitude,'stop', 'lg_amplitude : controls the qmplitude?')
    
    call input_logical ('lg_location', lg_location,'stop', 'lg_location : controls the location?')
    
    call input_logical ('lg_sigma', lg_sigma,'stop', 'lg_sigma : controls sigma?')
    
    CALL input_integer_vector ('iga_idx_param', iga_idx_param, NB_PARAM, 'stop', ' iga_idx_param: idx of params')
    
    CALL input_real_vector ('rga_sigma_param', rga_sigma_param, NB_PARAM, 'stop', 'rga_sigma_param : control params')
    
    CALL input_real_vector ('rga_paramt', rga_paramt, NB_PARAM, 'stop', ' rga_paramt: control params')

    CALL input_real_vector ('rga_b_param', rga_b_param, NB_PARAM, 'stop', ' rga_b_param: controlbackground params')
    
    
    !CALL input_real_vector ('', , , 'stop', ' : control params')!model

  END SUBROUTINE user_read_input!lg_direct, lg_adjoint

  
  !> Set the exact solution for comparison to the simulated solution
  !!
  !! \param[in,out] u state variable for exact solution, array to fill in the exact solution
  !! \param[in] nlocal number of active wavelets   
  !! \param[in] ne_local total number of equations
  !! \param[in] t time of current time step 
  !! \param[in] l_n_var_exact_soln_index index into the elements of u for which we need to find the exact solution
  !<
  SUBROUTINE  user_exact_soln (u, nlocal,  t_local, l_n_var_exact_soln)
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: nlocal
    REAL (pr), INTENT (IN) ::  t_local
    REAL (pr), DIMENSION (nlocal,n_var_exact), INTENT (INOUT) :: u
    LOGICAL , INTENT (IN) :: l_n_var_exact_soln(n_var)

  END SUBROUTINE  user_exact_soln

  !< \brief Computes initial condition for the first variable of the direct problem
  !! \param[in] id_ng size of the state vector
  !! \details
  !! uses the global variables x and rg_sigma
  !>
  FUNCTION direct_initial_condition(id_ng) RESULT(rla_u)
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: id_ng
    REAL(KIND=pr), DIMENSION(id_ng) :: rla_u
    
    !rga_x0 = ( MINVAL(x,1)+MAXVAL(x,1) )/2

    rla_u = rg_alpha*exp( -( x(:, 1) - rga_x0(1))**2/(2 * rg_sigma**2) )
    PRINT*,'In direct_initial_condition ***********************************'
    PRINT*,'   rg_alpha  = ', rg_alpha
    PRINT*,'   rga_x0(1) = ', rga_x0(1)
    PRINT*,'   rg_sigma  = ', rg_sigma
    
  END FUNCTION direct_initial_condition

  !> Computes initial condition, this is the system state at initial time
  !! \param[in, out] u sytem state
  !! \param[in] nlocal number of grid point in each single variable of the system state
  !! \param[in] ne_local number of equation in the system of PDE
  !! \param[in] t_local time associated to the initial condition
  !! \param[in] scl [to be specified]
  !! \param[in] scl_fltwt [to be specified]
  !! \param[in, out] iter [to be specified]
  !!
  !! \detail here this routine is used as the driver for the minimizer algorithm
  !<
  SUBROUTINE user_initial_conditions (u, nlocal, ne_local, t_local, scl, scl_fltwt, iter)
    IMPLICIT NONE
    INTEGER  , INTENT (IN) :: nlocal, ne_local
    INTEGER  , INTENT (INOUT) :: iter ! iteration of call while adapting initial grid
    REAL (pr), DIMENSION (nlocal,ne_local), INTENT (INOUT) :: u
    REAL (pr), DIMENSION (nlocal,ne_local) :: f
    REAL (pr), INTENT (IN)   :: scl(1:n_var),scl_fltwt
    REAL (pr), INTENT (IN) :: t_local
    REAL (pr), DIMENSION(ne_local) :: scl_u
    INTEGER, DIMENSION(ne_local) :: clip
    INTEGER :: eq1, eq2, il_neq!number of equations to be solve, just for test purposes.

    clip = 0 !no clipping for Dirichlet BC
    u(:, 1) = direct_initial_condition(nlocal)! exp( -( x(:, 1) - x0(1) )**2/(2 * rg_sigma**2) )
    u(:, 2) = 0.0_pr

    scl_u = 1.0_pr
    PRINT*, 'In user_initial_conditions : calling  Laplace_rhs***'
    f = Laplace_rhs (u, nlocal, ne_local, HIGH_ORDER)

    IF(lg_direct .AND. lg_adjoint)THEN  !both direct and adjoint simulation required
       eq1 = 1
       eq2 = 2
    ELSE IF(lg_direct)THEN !only direct simulation required, when called by the routine <cost>
       eq1 = 1
       eq2 = 1
    ELSE IF(lg_adjoint) THEN !only adjoint required, when called by the routine <costb>
       eq1 = 2
       eq2 = 2
    ELSE
       PRINT*, 'Nothing to do********************************'
       STOP
    END IF

    il_neq = eq2 - eq1 + 1
    PRINT*, 'In user_initial_conditions : calling Linsolve ***'
    CALL Linsolve ( u(:, eq1:eq2), f(:, eq1:eq2), tol2, nlocal, il_neq, clip, Laplace, Laplace_diag, SCL=scl_u )  !
  END SUBROUTINE user_initial_conditions

  !--********************************  
  ! Arguments
  ! u         - field on adaptive grid
  ! nlocal      - number of active points
  ! ne_local       - number of equations
  ! t         - current time
  !
  ! Global variables used
  ! ifn       - number of equations
  ! x()
  ! xO
  ! y0
  !--******************************** 
  SUBROUTINE user_algebraic_BC (Lu, u, nlocal, ne_local, jlev, meth)
    !--Defines boundary condition type
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: jlev, meth, ne_local, nlocal
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (INOUT) :: Lu
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (IN)    :: u

    INTEGER :: ie, ii, shift
    REAL (pr), DIMENSION (nlocal,ne_local) :: du, d2u


    !
    ! There are periodic BC conditions
    !


  END SUBROUTINE user_algebraic_BC

  !> \brief
  !!
  !<
  SUBROUTINE user_algebraic_BC_diag (Lu_diag, nlocal, ne_local, jlev, meth)
    !--Defines boundary condition type
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: jlev, meth, ne_local, nlocal
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (INOUT) :: Lu_diag
    REAL (pr), DIMENSION (nlocal,dim) :: du, d2u

    !
    ! There are periodic BC conditions
    !

  END SUBROUTINE user_algebraic_BC_diag

  !> \brief
  !!
  !<
  SUBROUTINE user_algebraic_BC_rhs (rhs, ne_local, nlocal, jlev)
    !--Sets rhs for boundary conditions
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: ne_local, nlocal, jlev
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (INOUT) :: rhs

    INTEGER :: ie, ii, shift
    !
    ! There are periodic BC conditions
    !

  END SUBROUTINE user_algebraic_BC_rhs

  !> \brief
  !!
  !<
  SUBROUTINE user_project (u, p, nlocal, meth)
    !--Makes u divergence free
    IMPLICIT NONE

    INTEGER, PARAMETER :: ne_local = 1
    INTEGER, INTENT (IN) :: meth, nlocal
    REAL (pr), DIMENSION (nlocal),     INTENT(INOUT) :: p
    REAL (pr), DIMENSION (nlocal,n_integrated), INTENT(INOUT) :: u

    !------------ not used in this case

  END SUBROUTINE user_project

  !> \brief Computes the advection velocity
  !! \param[in] id_jlev level of computation, level refers to wavelet decomposition
  !! \param[in] id_nlocal 
  !! \detail computes the advection velocity field on the grid associate to jlev wavelet decomposition
  !<
  FUNCTION advection_velocity(id_jlev, id_nlocal)RESULT(rla_v)
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: id_jlev
    INTEGER, INTENT(IN) :: id_nlocal
    REAL (pr), DIMENSION ( id_nlocal  ) :: rla_v!spatial advection velocity field. No component in the time direction, to be adapted according to the number of spatial dimensions
    INTEGER :: ibi, face_type, nloc, j, wlt_type, j_df, k, i, ii, id, vshift
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc
    REAL(KIND=pr) :: rl_omega

    rl_omega = 2*pi
  
    i_p_face(0) = 1
    DO ibi=1,dim
       i_p_face(ibi) = i_p_face(ibi-1)*3
    END DO
    !computing advection velocity
    DO j = 1, id_jlev
       DO wlt_type = MIN(j-1,1),2**dim-1
          DO face_type = 0, 3**dim - 1
             !face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
             DO j_df = j, j_lev
                DO k = 1, indx_DB(j_df,wlt_type,face_type,j)%length  
                   i = indx_DB(j_df,wlt_type,face_type,j)%p(k)%i 
!! /!\alert ...
                   ii = i+ indx_DB(id_jlev,wlt_type,face_type,j)%shift !!*************************shift not defined********************
                   id = 1
                   vshift=(id-1)*id_nlocal
                   rla_v(vshift+ii) = rg_v0*sin( rl_omega*x(i,2) )
                   !DO id = 1, dim-1!advection velocity only along spatial dimensions
                      !vshift=(id-1)*id_nlocal
                      !rla_v(vshift+ii) = rg_v0*sin( rl_omega*x(i,2) )
                      !a(shift+ii) =(1.0_pr+4.0_pr*x(i,2)*(x(i,1)**2-x(i,1)))
                      !ax(shift+ii)= 4.0_pr*x(i,2)*(2.0_pr*x(i,1)-1.0_pr)
                   !END DO
                END DO
             END DO
          END DO
       END DO
    END DO

  END FUNCTION advection_velocity

  !> \brief direct model equation
  !! \param[in] id_jlev level of computation, level refers to wavelet decomposition
  !! \param[in] rda_u state variable
  !! \param[in] id_ng size of a algebraic component of the state vector, this is the number of grid point (discretization)
  !! \param[in] id_neq number of algebraic variables in the system state, this is also the number of equations in the direct model
  !! \param[in] id_meth method to use for computing derivatives
  !! \detail the goal is to separate direct and adjoint model so that each can be checked separately
  !<
  FUNCTION direct(id_jlev, rda_u, id_ng, id_neq, id_meth)
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: id_jlev, id_ng, id_neq, id_meth
    REAL (pr), DIMENSION ( id_ng*id_neq ), INTENT (INOUT) :: rda_u
    REAL (pr), DIMENSION ( id_ng*id_neq ) :: direct
    REAL (pr), DIMENSION ( id_ng  ) :: rla_v!spatial advection velocity field. No component in the time direction, to be adapted according to the number of spatial dimensions

    INTEGER :: i, ii, ie, shift, idim
    INTEGER :: meth_central, meth_backward, meth_forward
    REAL (pr), DIMENSION (id_neq,id_ng, dim) :: dub, du, d2u
    REAL (pr), DIMENSION (: , :, :), POINTER :: du_dummy ! passed when only calculating 1st derivative.
    INTEGER :: face_type, nloc, j, wlt_type, j_df, k
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc
    INTEGER :: ibi

    PRINT*, ' direct starting <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<'
    meth_central  = id_meth + BIASING_NONE
    meth_backward = id_meth + BIASING_BACKWARD
    meth_forward  = id_meth + BIASING_FORWARD

    CALL c_diff_fast (rda_u, dub, du_dummy, id_jlev, id_ng, meth_backward, 10, id_neq, 1, id_neq)!first order derivatives for direct model, for time dimension
    CALL c_diff_fast (rda_u, du , d2u     , id_jlev, id_ng, meth_central , 11, id_neq, 1, id_neq)!first order der and laplacian for spatial dimensions

    rla_v = advection_velocity(id_jlev, id_ng)

    !****first equation of the  direct model
    ie = 1
    shift=(ie-1)*id_ng
    direct(shift+1:shift+Nwlt_lev(id_jlev,1)) = dub(ie, 1:Nwlt_lev(id_jlev,1), dim) !time dimension
    DO idim = 1, dim-1
       direct(shift+1:shift+Nwlt_lev(id_jlev,1)) = direct(shift+1:shift+Nwlt_lev(id_jlev,1)) &
            - nu*d2u(ie, 1:Nwlt_lev(id_jlev,1), idim)&
            + rla_v(1:Nwlt_lev(id_jlev,1))*du( ie ,1:Nwlt_lev(id_jlev,1),idim)
    END DO
    !--Boundary points
    i_p_face(0) = 1
    DO i=1,dim
       i_p_face(i) = i_p_face(i-1)*3
    END DO
    DO face_type = 0, 3**dim - 1
       face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1

       IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
          CALL get_all_indices_by_face (face_type, id_jlev, nloc, iloc)

          IF(nloc > 0 ) THEN ! 2-D & 3-D cases
             IF( face(1) == -1  ) THEN                                 ! Xmin face, entire face
                direct(shift+iloc(1:nloc)) = rda_u(shift+iloc(1:nloc))    !Boundaries conditions
              ELSE IF( face(1) == 1 ) THEN                              ! Xmax face, entire face
                direct(shift+iloc(1:nloc)) = rda_u(shift+iloc(1:nloc))    !Boundaries conditions
             ELSE IF( ALL( face(1:2) == (/0,-1/) ) ) THEN              ! Ymin face, edges || to X only, no corners
                direct(shift+iloc(1:nloc)) = rda_u(shift+iloc(1:nloc))    !boundaries conditions in the time direction
             END IF
          END IF
       END IF
    END DO

    PRINT*, ' direct ending <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<'
  END FUNCTION direct

  !> \brief adjoint model equation
  !! \param[in] id_jlev level of computation, level refers to wavelet decomposition
  !! \param[in] rda_u state variable
  !! \param[in] id_ng size of a algebraic component of the state vector, this is the number of grid point (discretization)
  !! \param[in] id_neq number of algebraic variables in the system state, this is also the number of equations in the direct model
  !! \param[in] id_meth method to use for computing derivatives
  !! \detail the goal is to separate direct and adjoint model so that each can be checked separately
  !<
  FUNCTION adjoint(id_jlev, rda_u, id_ng, id_neq, id_meth)
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: id_jlev, id_ng, id_neq, id_meth
    REAL (pr), DIMENSION ( id_ng*id_neq ), INTENT (INOUT) :: rda_u
    REAL (pr), DIMENSION ( id_ng*id_neq ) :: adjoint
    REAL (pr), DIMENSION ( id_ng  ) :: rla_v!spatial advection velocity field. No component in the time direction, to be adapted according to the number of spatial dimensions

    INTEGER :: i, ii, ie, shift, idim
    INTEGER :: meth_central, meth_backward, meth_forward
    REAL (pr), DIMENSION (id_neq,id_ng, dim) :: duf, du, d2u
    REAL (pr), DIMENSION (1       ,id_ng, dim) :: dv!single advection velocity, to be adapted according to the number of spatial dimensions
    REAL (pr), DIMENSION (: , :, :), POINTER :: du_dummy ! passed when only calculating 1st derivative.
    INTEGER :: face_type, nloc, j, wlt_type, j_df, k
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc
    INTEGER :: ibi

    PRINT*, ' adjoint starting vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv '
    meth_central  = id_meth + BIASING_NONE
    meth_backward = id_meth + BIASING_BACKWARD
    meth_forward  = id_meth + BIASING_FORWARD

    CALL c_diff_fast (rda_u, duf, du_dummy, id_jlev, id_ng, meth_forward , 10, id_neq, 1, id_neq)!first order derivatives for adjoint model, for time dimension
    CALL c_diff_fast (rda_u, du , d2u     , id_jlev, id_ng, meth_central , 11, id_neq, 1, id_neq)!first order der and laplacian for spatial dimensions

    rla_v = advection_velocity(id_jlev, id_ng)
    CALL c_diff_fast (rla_v, dv,  du_dummy, id_jlev, id_ng, meth_central , 10, 1, 1, 1)

    !****first equation of the adjoint model
    ie = 1
    shift=(ie-1)*id_ng
    adjoint(shift+1:shift+Nwlt_lev(id_jlev,1)) = duf(ie, 1:Nwlt_lev(id_jlev,1), dim) !time dimension
    DO idim = 1, dim-1
       adjoint(shift+1:shift+Nwlt_lev(id_jlev,1)) = adjoint(shift+1:shift+Nwlt_lev(id_jlev,1)) &
            + nu*d2u(ie, 1:Nwlt_lev(id_jlev,1), idim)&
            + rla_v(1:Nwlt_lev(id_jlev,1))*du( ie ,1:Nwlt_lev(id_jlev,1),idim)&
            + rda_u(shift+1:shift+Nwlt_lev(id_jlev,1))*dv( 1, 1:Nwlt_lev(id_jlev,1), idim)
    END DO
    !--Boundary points
    i_p_face(0) = 1
    DO i=1,dim
       i_p_face(i) = i_p_face(i-1)*3
    END DO
    DO face_type = 0, 3**dim - 1
       face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1

       IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
          CALL get_all_indices_by_face (face_type, id_jlev, nloc, iloc)

          IF(nloc > 0 ) THEN ! 2-D & 3-D cases
             IF( face(1) == -1  ) THEN                    ! Xmin face, entire face
                adjoint(shift+iloc(1:nloc)) = rda_u(shift+iloc(1:nloc))      !Boundaries conditions
             ELSE IF( face(1) == 1 ) THEN                ! Xmax face, entire face
                adjoint(shift+iloc(1:nloc)) = rda_u(shift+iloc(1:nloc))      !Boundaries conditions
             ELSE IF( ALL( face(1:2) == (/0, 1/) ) ) THEN ! Ymin face, edges || to X only, no corners
                adjoint(shift+iloc(1:nloc)) = rda_u(shift+iloc(1:nloc))      !boundaries conditions in the time direction
             END IF
          END IF
       END IF
    END DO
    PRINT*, ' adjoint ending vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv '
  END FUNCTION adjoint
  
  !> \brief Computes the left hand side of time indepemdent problem written as F(u) = 0
  !! \param[in] jlev level of computation, level refers to wavelet decomposition
  !! \param[in] u state variable
  !! \param[in] nlocal size of a algebraic component of the state vector, this is the number of grid point (discretization)
  !! \param[in] ne_local number of algebraic variables in the system state, this is also the number of equations in the direct model
  !! \param[in] meth method to use for computing derivatives
  !! \detail the goal is to separate direct and adjoint model so that each can be checked separately
  !<
  FUNCTION Laplace (jlev, u, nlocal, ne_local, meth)
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: jlev, nlocal, ne_local, meth
    REAL (pr), DIMENSION ( nlocal*ne_local ), INTENT (INOUT) :: u
    REAL (pr), DIMENSION ( nlocal*ne_local ) :: Laplace
    INTEGER :: il_dshift, il_ashift
    
    il_dshift = -nlocal
    PRINT*, 'Entering Laplace ************************************************'
    !direct model
    IF(lg_direct)THEN
       il_dshift = 0
       Laplace(il_dshift+1:il_dshift+nlocal) = direct(jlev, u(il_dshift+1:il_dshift+nlocal), nlocal, 1, meth)
    ELSE
       il_dshift = -nlocal
    END IF

    !adjoint model
    IF(lg_adjoint)THEN
       il_ashift = il_dshift + nlocal
       Laplace(il_ashift+1:il_ashift+nlocal) = adjoint(jlev, u(il_ashift+1:il_ashift+nlocal), nlocal, 1, meth)
    END IF
    
    PRINT*, 'Ending Laplace ************************************************'
  END FUNCTION Laplace

  !> \brief Computes the diagonal part of direct model equations
  !! \param[in] id_jlev level of computation, level refers to wavelet decomposition
  !! \param[in] id_ng size of a algebraic component of the state vector, this is the number of grid point (discretization)
  !! \param[in] id_neq number of algebraic variables in the system state, this is also the number of equations in the direct model
  !! \param[in] id_meth method to use for computing derivatives
  !! \detail the goal is to separate direct and adjoint model so that each can be checked separately
  !<
  FUNCTION direct_diag(id_jlev, id_ng, id_neq, id_meth)
    USE precision
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: id_jlev, id_ng, id_neq, id_meth
    REAL (pr), DIMENSION (1:id_ng*id_neq) :: direct_diag
    REAL (pr), DIMENSION ( id_ng  ) :: rla_v

    INTEGER :: i, ii, ie, shift, idim
    INTEGER :: meth_central, meth_backward, meth_forward
    REAL (pr), DIMENSION (id_ng,dim) :: dub, du, d2u
    REAL (pr), DIMENSION (:, :), POINTER :: du_dummy ! passed when only calculating 1st derivative.

    INTEGER :: face_type, nloc, j, wlt_type, j_df, k
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc
    INTEGER :: ibi

    PRINT*, ' direct_diag starting >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>'
    meth_central  = id_meth + BIASING_NONE
    meth_backward = id_meth + BIASING_BACKWARD
    meth_forward  = id_meth + BIASING_FORWARD

    CALL c_diff_diag (dub, du_dummy, id_jlev, id_ng, meth_backward, meth_backward, 10)
    CALL c_diff_diag (du , d2u     , id_jlev, id_ng, meth_central , meth_central , 11)

    rla_v = advection_velocity(id_jlev, id_ng)

    !****first equation : direct model
    ie = 1
    shift=(ie-1)*id_ng
    direct_diag(shift+1:shift+Nwlt_lev(id_jlev,1)) = dub(1:Nwlt_lev(id_jlev,1),dim)
    DO idim = 1,dim-1
       direct_diag(shift+1:shift+Nwlt_lev(id_jlev,1)) = direct_diag(shift+1:shift+Nwlt_lev(id_jlev,1)) &
            - nu*d2u(1:Nwlt_lev(id_jlev,1),idim)&
            + rla_v(shift+1:shift+Nwlt_lev(id_jlev,1))*du( 1:Nwlt_lev(id_jlev,1), idim)
    END DO
    !--Boundary points
    i_p_face(0) = 1
    DO i=1,dim
       i_p_face(i) = i_p_face(i-1)*3
    END DO

    DO face_type = 0, 3**dim - 1
       face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
       IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points

          CALL get_all_indices_by_face (face_type, id_jlev, nloc, iloc)

          IF(nloc > 0 ) THEN ! 2-D (1D + time)
             IF( face(1) == -1  ) THEN                                    ! Xmin face, entire face
                direct_diag(shift+iloc(1:nloc)) = 1.0_pr                 !Dirichlet conditions
             ELSE IF( face(1) == 1 ) THEN                                 ! Xmax face, entire face
                direct_diag(shift+iloc(1:nloc)) = 1.0_pr                 !Dirichlet conditions
             ELSE IF( ALL( face(1:2) == (/0,-1/) ) ) THEN                 ! Ymin face, edges || to X only, no corners
                direct_diag(shift+iloc(1:nloc)) = 1.0_pr                 !initial condition
             END IF
          END IF
       END IF
    END DO

    PRINT*, ' Ending direct_diag >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>'
  END FUNCTION direct_diag

  !> \brief Computes the diagonal part of adjoint model equations
  !! \param[in] id_jlev level of computation, level refers to wavelet decomposition
  !! \param[in] id_ng size of a algebraic component of the state vector, this is the number of grid point (discretization)
  !! \param[in] id_neq number of algebraic variables in the system state, this is also the number of equations in the direct model
  !! \param[in] id_meth method to use for computing derivatives
  !! \detail the goal is to separate direct and adjoint model so that each can be checked separately
  !<
  FUNCTION adjoint_diag(id_jlev, id_ng, id_neq, id_meth)
    USE precision
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: id_jlev, id_ng, id_neq, id_meth
    REAL (pr), DIMENSION (id_ng*id_neq) :: adjoint_diag
    REAL (pr), DIMENSION ( id_ng  ) :: rla_v

    INTEGER :: i, ii, ie, shift, idim
    INTEGER :: meth_central, meth_backward, meth_forward
    REAL (pr), DIMENSION (id_ng,dim) :: duf, du, d2u
    REAL (pr), DIMENSION (id_ng,dim) :: dv
    REAL (pr), DIMENSION (:, :), POINTER :: du_dummy ! passed when only calculating 1st derivative.

    INTEGER :: face_type, nloc, j, wlt_type, j_df, k
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc
    INTEGER :: ibi

    PRINT*, ' adjoint_diag starting ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^'
    meth_central  = id_meth + BIASING_NONE
    meth_backward = id_meth + BIASING_BACKWARD
    meth_forward  = id_meth + BIASING_FORWARD

    CALL c_diff_diag (duf, du_dummy, id_jlev, id_ng, meth_forward, meth_forward, 10)!first order derivatives for adjoint model
    CALL c_diff_diag (du , d2u     , id_jlev, id_ng, meth_central, meth_central, 11)

    rla_v = advection_velocity(id_jlev, id_ng)
    CALL c_diff_diag (dv , du_dummy, id_jlev, id_ng, meth_central, meth_central, 10)

    !****second equation : adjoint model
    ie = 1
    shift=(ie-1)*id_ng
    adjoint_diag(shift+1:shift+Nwlt_lev(id_jlev,1)) = duf(1:Nwlt_lev(id_jlev,1),dim) !time dimension
    DO idim = 1,dim-1
       adjoint_diag(shift+1:shift+Nwlt_lev(id_jlev,1)) = adjoint_diag(shift+1:shift+Nwlt_lev(id_jlev,1)) &
            + nu*d2u(1:Nwlt_lev(id_jlev,1),idim)&
            + rla_v(1:Nwlt_lev(id_jlev,1))*du( 1:Nwlt_lev(id_jlev,1), idim)&
            + dv(1:Nwlt_lev(id_jlev,1), idim)
    END DO
    i_p_face(0) = 1
    DO i=1,dim
       i_p_face(i) = i_p_face(i-1)*3
    END DO
    
    DO face_type = 0, 3**dim - 1
       face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
       IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
          
          CALL get_all_indices_by_face (face_type, id_jlev, nloc, iloc)
          
          IF(nloc > 0 ) THEN ! 2-D (1D + time)
             IF( face(1) == -1  ) THEN                                    ! Xmin face, entire face
                adjoint_diag(shift+iloc(1:nloc)) = 1.0_pr                 !Dirichlet conditions
             ELSE IF( face(1) == 1 ) THEN                                 ! Xmax face, entire face
                adjoint_diag(shift+iloc(1:nloc)) = 1.0_pr                 !Dirichlet conditions
             ELSE IF( ALL( face(1:2) == (/0, 1/) ) ) THEN                 ! Ymin face, edges || to X only, no corners
                adjoint_diag(shift+iloc(1:nloc)) = 1.0_pr                 !initial condition
             END IF
          END IF
       END IF
    END DO
       
    PRINT*, ' Ending adjoint_diag ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^'
  END FUNCTION adjoint_diag
  
  !> \brief Computes the diagonal of the left hand side of time indepemdent problem written as F(u) = 0
  !! \param[in] jlev level of computation, level refers to wavelet decomposition
  !! \param[in] nlocal size of a algebraic component of the state vector, this is the number of grid point (discretization)
  !! \param[in] ne_local number of algebraic variables in the system state, this is also the number of equations in the direct model
  !! \param[in] meth method to use for computing derivatives
  !! \detail the goal is to separate direct and adjoint model so that each can be checked separately
  !<
  FUNCTION Laplace_diag (jlev, nlocal, ne_local, meth)
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: jlev, nlocal, ne_local, meth
    REAL (pr), DIMENSION (1:nlocal*ne_local) :: Laplace_diag
    INTEGER :: il_dshift, il_ashift

    PRINT*, 'Entering Laplace_diag ************************************************'
    il_dshift = 0
    il_ashift = il_dshift + nlocal

    !direct model
    IF(lg_direct)THEN
       Laplace_diag(il_dshift+1:il_dshift+nlocal) = direct_diag (jlev, nlocal, 1, meth)
    END IF

    !adjoint model
    IF(lg_adjoint)THEN
       Laplace_diag(il_ashift+1:il_ashift+nlocal) = adjoint_diag(jlev, nlocal, 1, meth)
       PRINT*, ' In Laplace_diag : after adjoint_diag **********'
    END IF

    PRINT*, 'Ending Laplace_diag ************************************************'
  END FUNCTION Laplace_diag

  FUNCTION Laplace_rhs(u, nlocal, ne_local, meth_in)
    USE penalization
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: nlocal, ne_local, meth_in
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (INOUT) :: u
    REAL (pr), DIMENSION (nlocal,ne_local) :: Laplace_rhs

    INTEGER :: i, ii, ie, meth, idim
    INTEGER :: meth_central, meth_backward, meth_forward 
    REAL (pr), DIMENSION (nlocal,dim) :: Xdel
    REAL (pr), DIMENSION (ne_local ,nlocal,dim) :: du, d2u
    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc

    REAL(pr) :: r(nlocal), Q(nlocal)
    REAL (pr), DIMENSION(dim) :: x0
    

    PRINT*, 'Laplace_rhs starting ********************************************'
    meth_central  = meth_in + BIASING_NONE
    meth_backward = meth_in + BIASING_BACKWARD
    meth_forward  = meth_in + BIASING_FORWARD

    Laplace_rhs(:,1)  = 0.0_pr
    x0 = ( MINVAL(x,1)+MAXVAL(x,1) )/2
    Laplace_rhs(:,2)  = cos( 4*pi*( x(:,1) - x0(1) )**2 + 4*pi*( x(:,2) - x0(2) )**2 )

    !--Boundary points
    i_p_face(0) = 1
    DO i=1,dim
       i_p_face(i) = i_p_face(i-1)*3
    END DO
    
    x0 = ( MINVAL(x,1)+MAXVAL(x,1) )/2

    DO face_type = 0, 3**dim - 1
       face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
       IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
          
          CALL get_all_indices_by_face (face_type, j_lev, nloc, iloc)
          
          IF(nloc > 0 ) THEN ! 2-D (1D + time)
             IF( face(1) == -1  ) THEN                    ! Xmin face, entire face
                Laplace_rhs(iloc(1:nloc),1) = 0.0_pr      !Dirichlet conditions
                Laplace_rhs(iloc(1:nloc),2) = 0.0_pr      !Dirichlet conditions
             ELSE IF( face(1) == 1 ) THEN                 ! Xmax face, entire face
                Laplace_rhs(iloc(1:nloc),1) = 0.0_pr      !Dirichlet conditions
                Laplace_rhs(iloc(1:nloc),2) = 0.0_pr      !Dirichlet conditions
             ELSE IF( ALL( face(1:2) == (/0,-1/) ) ) THEN ! Ymin face, edges || to X only, no corners
                Laplace_rhs(iloc(1:nloc),1) = rg_alpha*exp( -( x(iloc(1:nloc), 1) - rga_x0(1))**2/(2 * rg_sigma**2) )!initial condition for direct model
             ELSE IF( ALL( face(1:2) == (/0, 1/) ) ) THEN ! Ymax face, edges || to X only, no corners, final condition for adjoint model
                Laplace_rhs(iloc(1:nloc),2) = 0.0_pr      !Final condition for adjoint model
             END IF
          END IF
       END IF
    END DO
   
    PRINT*, 'Laplace_rhs ending ********************************************'
  END FUNCTION Laplace_rhs

  !> \brief Compute the right hand side of the PDE
  !!
  !<
  FUNCTION user_rhs (u_integrated,p)
    IMPLICIT NONE
    REAL (pr), DIMENSION (ng,ne), INTENT(IN) :: u_integrated !1D flat version of integrated variables without BC 
    REAL (pr), DIMENSION (ng), INTENT(IN) :: p
    REAL (pr), DIMENSION (n) :: user_rhs

    INTEGER :: ie, shift
    INTEGER, PARAMETER :: meth=1
    REAL (pr), DIMENSION (ne,ng,dim) :: du, d2u
    REAL (pr), DIMENSION (ng,dim)    :: dp
  
    user_rhs = 0.0_pr
    !--Set operator on boundaries
!!$    IF( Nwlt_lev(j_lev,1) >  Nwlt_lev(j_lev,0) ) &
!!$         CALL user_algebraic_BC_rhs (user_rhs, ne, ng, j_lev)
    
  END FUNCTION user_rhs

  ! find Jacobian of Right Hand Side of the problem
  FUNCTION user_Drhs (u, u_prev_timestep_loc, meth)
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: meth
    !u_prev_timestep passed local to cast it into 2dim array to work with vector derivatives
    REAL (pr), DIMENSION (ng,ne) :: u, u_prev_timestep_loc
    REAL (pr), DIMENSION (n) :: user_Drhs

    INTEGER :: ie, shift
    REAL (pr), DIMENSION (2*ne,ng,dim) :: du  ! 1st derivatives for u (in du(1:ne,:,:)) and u_prev_timestep (in du(ne+1:2*ne,:,:))
    REAL (pr), DIMENSION (ne  ,ng,dim) :: d2u ! 2nd derivatives for u 
    REAL (pr), DIMENSION (ne  ,ng,dim) :: du_dummy ! passed when only calculating 1st derivative.
    !Find batter way to do this!! du_dummy with no storage..
    
    user_Drhs = 0.0_pr
  END FUNCTION user_Drhs


  !
  ! Uses u_prev_timestep, which is global flat (1D ) array set in time_step_cn()
  !
  FUNCTION user_Drhs_diag (meth)
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: meth
    REAL (pr), DIMENSION (n) :: user_Drhs_diag

    INTEGER :: ie, shift
    REAL (pr), DIMENSION (ng,dim) :: du, d2u
    REAL (pr), DIMENSION (ne,ng,dim) :: du_prev_timestep
    REAL (pr), DIMENSION (ne  ,ng,dim) :: du_dummy ! passed when only calculating 1st derivative.

    user_Drhs_diag = 0.0+pr
  END FUNCTION user_Drhs_diag

  !>
  !!
  !<
  FUNCTION user_chi (nlocal, t_local )
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: nlocal
    REAL (pr), INTENT (IN) :: t_local
    REAL (pr), DIMENSION (nlocal) :: user_chi

    user_chi = 0.0_pr
  END FUNCTION user_chi



  !
  ! Calculate any statitics
  !
  ! startup_flag - 0 when adapting to IC,then 1 inmain integration loop
  !
  !
  SUBROUTINE user_stats ( u ,j_mn, startup_flag)
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: startup_flag
    INTEGER , INTENT (IN) :: j_mn 
    REAL (pr), DIMENSION (nwlt,1:n_var), INTENT (IN) :: u

    ! debug: test database independent interpolation
    IF (test_interpolate) CALL user_interpolate (u)

  END SUBROUTINE user_stats

  !
  ! calculate any additional variables
  ! 
  ! arg 
  ! flag  - 0 calledwhile adapting to IC, 1 - called in main time integration loop
  !
  ! These additiona variables are calculated and left in real space.
  !
  SUBROUTINE user_cal_force (u, n, t_local, force, drag, lift)
    !--Calculates drag and lift on obstacle using penalization formula
    IMPLICIT NONE

    INTEGER, INTENT (IN) :: n
    REAL (pr), INTENT (IN) :: t_local
    REAL (pr), DIMENSION (dim), INTENT (INOUT) :: force
    REAL (pr), INTENT (OUT) :: drag, lift
    REAL (pr), DIMENSION (n,dim) :: u
    drag = 0.0_PR
    lift = 0.0_PR

    !
    ! There is no obstacle in flow
    !

  END SUBROUTINE user_cal_force

  !
  ! calculate any additional variables
  ! 
  ! arg 
  ! flag  - 0 calledwhile adapting to IC, 1 - called in main time integration loop
  !
  ! These additiona variables are calculated and left in real space.
  !
  SUBROUTINE user_additional_vars( t_local, flag )
    IMPLICIT NONE
    REAL (pr), INTENT (IN) ::  t_local
    INTEGER , INTENT(IN) :: flag ! 0- called during adaption to IC, 1 called during main integration loop


    ! Calculate the vorticity if we are in the main integration loop and we
    ! are saving the solution
    ! 
    ! NOTE we do not calculate vorticity in initial adaptation because
    ! derivatives are not setup yet (to save memmory)
    ! This shoild be changed eventually DG

    u(:, ig_var_v_idx) = rg_v0*sin( rg_omega*x(:,2) )

  END SUBROUTINE user_additional_vars

  !
  ! calculate any additional scalar variables
  ! 
  ! arg 
  ! flag  - 0 calledwhile adapting to IC, 1 - called in main time integration loop
  !
  SUBROUTINE user_scalar_vars( flag )
    IMPLICIT NONE
    INTEGER , INTENT(IN) :: flag ! 0- called during adaption to IC, 1 called during main integration loop



  END SUBROUTINE user_scalar_vars

  !
  !************ Calculating Scales ***************************
  !
  ! Note the order of the components in the scl array
  ! correspond to u_tn, v_tn, w_tn, u_tn-1, v_tn-1, w_tn-1, u_tn-2, v_tn-2, w_tn-2
  ! 
  SUBROUTINE user_scales(flag, use_default, u_loc, nlocal, ne_local, l_n_var_adapt , l_n_var_adapt_index, &
       scl, scl_fltwt) !add
    USE precision
    USE pde
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: flag ! 0 during initial adaptation, then 1 during time advancement
    LOGICAL , INTENT(INOUT) :: use_default
    INTEGER, INTENT (IN) :: nlocal, ne_local
    REAL (pr), DIMENSION (1:nlocal,1:ne_local), INTENT (IN) :: u_loc
    LOGICAL , INTENT (IN) :: l_n_var_adapt(ne_local)
    INTEGER , INTENT (IN) :: l_n_var_adapt_index(1:ne_local)
    REAL (pr), DIMENSION (1:ne_local), INTENT (INOUT) :: scl
    REAL (pr) ,  INTENT(IN) ::scl_fltwt !weight for temporal filter on scl

    !
    ! Ignore the output of this routine and use default scales routine
    !
    use_default = .TRUE. 

    !
    ! NOTE: For a parallel run, synchronize scl(:) across the processors in user_scales.
    !       Use the subroutine scales of default_util.f90 as an example,
    !       subroutine parallel_global_sum of parallel.f90 may be helpful.
    !       Already synchronized: sumdA_global
    !
  END SUBROUTINE user_scales

  SUBROUTINE user_cal_cfl (use_default, u, cfl_out)
    USE precision
    USE sizes
    USE pde
    USE parallel
    IMPLICIT NONE
    LOGICAL , INTENT(INOUT) :: use_default
    REAL (pr),                                INTENT (INOUT) :: cfl_out
    REAL (pr), DIMENSION (nwlt,n_integrated), INTENT (IN)    :: u

    INTEGER                    :: i
    REAL (pr)                  :: floor
    REAL (pr), DIMENSION (dim) :: cfl
    REAL (pr), DIMENSION(dim,nwlt) :: h_arr

    use_default = .FALSE.

    floor = 1e-12_pr
    cfl_out = floor

    CALL get_all_local_h (h_arr)

    DO i = 1, nwlt
       cfl(1:dim) = ABS (u(i,1:dim)+Umn(1:dim)) * dt/h_arr(1:dim,i)
       cfl_out = MAX (cfl_out, MAXVAL(cfl))
    END DO
    CALL parallel_global_sum( REALMAXVAL=cfl_out )

  END SUBROUTINE user_cal_cfl

  !******************************************************************************************
  !************************************* SGS MODEL ROUTINES *********************************
  !******************************************************************************************

  !
  ! Intialize sgs model
  ! This routine is called once in the first
  ! iteration of the main time integration loop.
  ! weights and model filters have been setup for first loop when this routine is called.
  !
  SUBROUTINE user_init_sgs_model( )
    IMPLICIT NONE


    ! LDM: Giuliano

    ! THE sgs forcign is stored for the time step in sgs_mdl_force(1:nlocal,1:n_integrated)
    ! where nlocal should be nwlt.


    !          print *,'initializing LDM ...'       
    !          CALL sgs_mdl_force( u(:,1:n_integrated), nwlt, j_lev, .TRUE.)


  END SUBROUTINE user_init_sgs_model

  !
  ! calculate sgs model forcing term
  ! user_sgs_force is called int he beginning of each times step in time_adv_cn().
  ! THE sgs forcign is stored for the time step in sgs_mdl_force(1:nlocal,1:n_integrated)
  ! where nlocal should be nwlt.
  ! 
  ! Accesses u from field module, 
  !          j_lev from wlt_vars module,
  !
  SUBROUTINE  user_sgs_force (u_loc, nlocal)
    IMPLICIT NONE

    INTEGER,                         INTENT (IN) :: nlocal
    REAL (pr), DIMENSION (nlocal,n_integrated), INTENT (INOUT) :: u_loc

  END SUBROUTINE  user_sgs_force

  FUNCTION user_sound_speed (u, neq, nwlt)
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: nwlt, neq
    REAL (pr), DIMENSION (nwlt,neq), INTENT(IN) :: u
    REAL (pr), DIMENSION (nwlt) :: user_sound_speed

    !user_sound_speed(:) = SQRT( gamma*(gamma-1.0_pr)* &
    !       ( u(:,4)-0.5_pr*SUM(u(:,n_var_mom(1:dim))**2,DIM=2)/u(:,1)) ) ! pressure
    user_sound_speed = 0.0_pr

  END FUNCTION user_sound_speed



  !-----------------------------------------------------------------------------------
  SUBROUTINE user_interpolate ( u )
    ! This is an example of database independent interpolation subroutine usage;
    ! predefined number of points and stack allocation has been used for that example.
    ! Read also the info inside subroutine interpolate of module wavelet_filters_mod
    USE parallel
    IMPLICIT NONE
    REAL (pr), DIMENSION (nwlt, n_var), INTENT (IN) :: u
    REAL (pr), DIMENSION (n_var, nwlt, dim) :: du, d2u
    INTEGER, PARAMETER :: x_size = 5000                  ! number of points to interpolate into
    REAL(pr) :: points(dim,x_size)
    INTEGER :: var(1:n_var)                           ! interpolate for all variables
    REAL (pr) :: res(1:n_var,1:x_size)                ! result of the interpolation
    INTEGER :: i, j, interpolation_order, var_size
    LOGICAL, SAVE :: BEEN_HERE = .FALSE.              ! file initialization flag
    INTEGER, PARAMETER :: iunit = 91                  ! ...
    INTEGER, SAVE :: set_counter = 0
    REAL(pr), PARAMETER :: MARGIN = 0.3_pr            ! margin
    REAL (4)  :: t0(0:2), t1(0:2)
    INTEGER :: cproc, ixyz(1:dim)

    BEEN_HERE = .FALSE. ! force overwriting

    ! set output file for debugging
    IF (BEEN_HERE) THEN
       ! open file for appending
       OPEN (UNIT=iunit, FILE='int'//TRIM(par_rank_str)//'.agr', FORM='formatted', STATUS='old', POSITION='append', ERR=1)
    ELSE
       ! create new file
       ! and write XMGR header
       OPEN (UNIT=iunit, FILE='int'//TRIM(par_rank_str)//'.agr', FORM='formatted', STATUS='unknown', ERR=1)
       WRITE (iunit,'("@g0 hidden false")')
       BEEN_HERE = .TRUE.
    END IF

    
    ! compute first derivatives to be passed to interpolate()
    ! for 2.5 order interpolation
    CALL c_diff_fast (u, du, d2u, j_lev, nwlt, HIGH_ORDER, 10, n_var, 1, n_var)
    DO i=1,n_var
       WRITE (*,'("   MAXVAL(ABS(u(1:nwlt,",I2,"))) = ",E17.10)') i, MAXVAL(ABS(u(1:nwlt,i)))
       WRITE (*,'("   MAXVAL(ABS(du(1:nwlt,",I2,"))) = ",E17.10)') i, MAXVAL(ABS(du(i,1:nwlt,1:dim)))
    END DO
    

    ! interpolate for all n_var variables,
    ! set var_size and var(:) respectively
    var_size = n_var
    DO i=1,n_var
       var(i) = i
    END DO


    ! predefine points to interpolate into
    ! diagonal has been used for that example
    DO i=1,x_size
       DO j=1,dim ! xx(0:nxyz(:),1:dim)
          points(j,i) = MARGIN + xx(0,j) + (xx(nxyz(j),j) - xx(0,j) - 2*MARGIN)*(i-1)/(1.0*(x_size-1))
       END DO
    END DO


    ! perform interpolation for different orders
    ! and write XMGR file ordered by the first coordinate
    DO interpolation_order = 1,1 !0,2 !0,2
       CALL CPU_TIME( t0(interpolation_order) )
       CALL interpolate( u, du, nwlt, n_var, &
            x_size, points, interpolation_order, var_size, var, &
            res, &                                    ! output interpolation result
            VERB = .TRUE.)                            ! show interpolation tree statistics (default T)
       CALL CPU_TIME( t1(interpolation_order) )
!!$       DO j=1, n_var
!!$          DO i=1,x_size
!!$             ixyz(1:dim) = INT( (points(1:dim,i)-xx(0,1:dim))/(xx(nxyz(j),j) - xx(0,j))*nxyz(1:dim) )
!!$             CALL DB_get_proc_by_coordinates (ixyz, j_lev, cproc)
!!$             IF (cproc.EQ.par_rank) &
!!$                  WRITE (iunit,'(2(E12.5,1X))') points(1,i), res(j,i)
!!$          END DO
!!$          WRITE (iunit,'("&")')
!!$       END DO
    END DO
    set_counter = set_counter + 1
    
    
    ! close XMGR file
    CLOSE (iunit)
    WRITE (*, '("   DEBUG: 0,1,3 order interpolation sets written ",I5," times")') set_counter
    DO interpolation_order = 0,2
       WRITE (*, '("CALL interpolate (USING CPU_TIME) = ", es12.5)') t1(interpolation_order) - t0(interpolation_order)
    END DO
    !PAUSE
    IF (set_counter.EQ.2) THEN
       CALL parallel_finalize
       STOP
    END IF
    
    
    ! error handling
    RETURN
1   PRINT *, 'ERROR while opening XMGR file'
    STOP 'in user_interpolate'
    
  END SUBROUTINE user_interpolate
  
  SUBROUTINE  user_pre_process
    IMPLICIT NONE
     
    CALL init_simulator()

  END SUBROUTINE user_pre_process

  SUBROUTINE  user_post_process
    IMPLICIT NONE

    IF(lg_direct)THEN
       IF(lg_make_obs)THEN
          CALL make_twin_obs()
       END IF

       IF(lg_obs_gap)THEN
          !compute obs gap
       END IF
    END IF
  END SUBROUTINE user_post_process

!! Data assimilation routines

  !> Computes initial condition, this is the system state at initial time
  !! \param[in, out] u sytem state
  !! \param[in] nlocal number of grid point in each single variable of the system state
  !! \param[in] ne_local number of equation in the system of PDE
  !! \param[in] t_local time associated to the initial condition
  !! \param[in] scl [to be specified]
  !! \param[in] scl_fltwt [to be specified]
  !! \param[in, out] iter [to be specified]
  !!
  !! \detail here this routine is used as the driver for the minimizer algorithm
  !<
  SUBROUTINE m1qn3_driver (u, nlocal, ne_local, t_local, scl, scl_fltwt, iter)
    IMPLICIT NONE
    INTEGER  , INTENT (IN) :: nlocal, ne_local
    INTEGER  , INTENT (INOUT) :: iter ! iteration of call while adapting initial grid
    REAL (pr), DIMENSION (nlocal,ne_local), INTENT (INOUT) :: u
    REAL (pr), DIMENSION (nlocal,ne_local) :: f
    REAL (pr), INTENT (IN)   :: scl(1:n_var),scl_fltwt
    REAL (pr), INTENT (IN) :: t_local
    !local variable
    INTEGER ibi !loop counter for m1qn3 loop call
    INTEGER il_stat
    
    !initialization
    !CALL make_twin_obs(u, nlocal, ne_local, t_local, scl, scl_fltwt, iter)

    !1 - computing or loading the initial control variable
    
    !2 - computing the cost function and its gradient at the initial control variable
    CALL init_assim

    ig_indic = 4
    PRINT*, 'In M1QN3_driver'
    PAUSE
    CALL simulator(ig_indic, ig_nctl, rga_ctl, rg_cost, rga_grad, iga_wa, rga_wa, dga_wa,&
                   u, nlocal, ne_local, t_local, scl, scl_fltwt, iter&
         )
    rg_df1 = rg_cost/2.0_cp
    !3 - initializing other variables
    !   3.1 initializing input mode
    iga_imode(1) = 0 !scaling mode 0 for DIS(recommended) and 1 for SIS
    iga_imode(2) = 0 !start mode of m1qn3, 0 for cold start and 1 for warm start
    iga_imode(3) = 0 ! frequency of call to simulator or return to driver with ig_indic=1, 0, never, k>0 every k iter from 1

    !   3.3 other initialization from input file
    !   ig_nctl    = 1
    !   rg_xmin    = 1.0e-16
    !   rg_epsg    = 1.0e-10
    !   ala_norm   = 'two'
    !   ig_impres  = 3
    !   ig_m1qn3io = 29
    !   iga_imode  = 0, 0, 0
    !   ig_niter   = 100
    !   ig_nsimul  = 200
    !   ig_reverse = 1
    !

    !Setting the communication mode
    ig_reverse = M1QN3_REVERSE !set reverse communication on
    DO WHILE (ig_reverse == M1QN3_REVERSE)
       CALL m1qn3(simulator, prosca, ctonb, ctcab, ig_nctl, rga_ctl, rg_cost, rga_grad,&
            rg_xmin, rg_df1, rg_epsg, ala_norm, ig_impres, ig_m1qn3io,&
            iga_imode, ig_omode, ig_niter, ig_nsimul, iga_iz, rga_dz, ig_ndz,&
            ig_reverse, ig_indic, iga_wa, rga_wa, dga_wa &
            )
       IF (ig_reverse == M1QN3_REVERSE) THEN
          SELECT CASE (ig_indic)
          CASE (M1QN3_NOTHING) !anything except changing the values of ig_indic, ig_nctl, rga_ctl, rg_cost and rga_grad
          CASE (M1QN3_COSTGRAD)
             !compute the cost function and its gradient
             CALL simulator(ig_indic, ig_nctl, rga_ctl, rg_cost, rga_grad, iga_wa, rga_wa, dga_wa,&
                            u, nlocal, ne_local, t_local, scl, scl_fltwt, iter&
                  )
             !if impossible to compute the cost function or its gradient then set ig_indic to -1 for the next call
             !if cost function and its gradient computed, then set ig_indic to 1 for the next call
          CASE DEFAULT
             !exception, unexpected value of ig_indic
             WRITE(*, '(A, I5)') 'In m1qn3_driver : unexpected value of indic from m1qn3 : ', ig_indic
          END SELECT
          
          !if something that m1qn3 can not understand happens, set ig_indic to 0 to force m1qn3 stop.
       END IF
    END DO
    !post processing
  END SUBROUTINE m1qn3_driver

  SUBROUTINE init_simulator()

    IF(lg_amplitude)THEN
       rg_alpha = rga_b_param(1) + rga_param(1)
    ELSE
       rg_alpha = rga_paramt(1)
    END IF
    IF(lg_location)THEN
       rga_x0(1) = rga_b_param(2) + rga_param(2)
    ELSE
       rga_x0(1) = rga_paramt(2)
    END IF
    IF(lg_sigma)THEN
       rg_sigma  = rga_b_param(3) + rga_param(3)
    ELSE
       rg_sigma  = rga_paramt(3)
    END IF
    PRINT*,'In init_simulator ***********************************'
    PRINT*,'   rg_alpha = ', rg_alpha
    PRINT*,'   rga_x0 = ',rga_x0
    PRINT*,'   rg_sigma = ', rg_sigma
  END SUBROUTINE init_simulator

  !> \brief Computes the cost function and its gradient
  !! \param [in,out] id_indic Indicates what is required from the simulator
  !! \param [in] id_ctl size of the control vector
  !! \param [in,out] rda_ctl control vector
  !! \param [in,out] rda_grad Gradient of the cost function
  !! \param [in,out] rd_cost value of the cost function,
  !! \param [in] ida_wa Integer working array for simulator
  !! \param [in] rda_wa Simple precision working array for simulator
  !! \param [in] dda_wa Double precision working array for simulator
  !! \detail parameter id_indic has the following meaningful values
  !! - 4 : compute the cost function and its gradient
  !! - 1 : informes the simulator that the minimizer is moving to the next iteration
  !<
  SUBROUTINE simulator(id_indic, id_ctl, rda_ctl, rd_cost, rda_grad, ida_wa, rda_wa, dda_wa,&
                        u, nlocal, ne_local, t_local, scl, scl_fltwt, iter&
                      )
    IMPLICIT NONE
    !parameters for m1qn3 simulator
    INTEGER, INTENT(INOUT) :: id_indic
    INTEGER, INTENT(IN)    :: id_ctl
    REAL(KIND=cp),DIMENSION(id_ctl),INTENT(INOUT) :: rda_ctl
    REAL(KIND=cp),DIMENSION(id_ctl),INTENT(INOUT) :: rda_grad
    REAL(KIND=cp),INTENT(INOUT) :: rd_cost
    !*V [idr]da_wa : working array inustite (contrainte modulopt)
    INTEGER,DIMENSION(2),INTENT(IN) :: ida_wa
    REAL(KIND=sp), DIMENSION(2),INTENT(IN) :: rda_wa
    REAL(KIND=dp), DIMENSION(2),INTENT(IN) :: dda_wa
    !parameter for Linsolve driver
    INTEGER, INTENT (IN) :: nlocal, ne_local
    INTEGER  , INTENT (INOUT) :: iter ! iteration of call while adapting initial grid
    REAL (pr), DIMENSION (nlocal,ne_local), INTENT (INOUT) :: u
    REAL (pr), INTENT (IN)   :: scl(1:n_var),scl_fltwt
    REAL (pr), INTENT (IN) :: t_local
    !local variables
    REAL(KIND=cp) :: rl_costb, rl_tmp
    
    PRINT*, 'Entering Simulator ---------------------------------'
 
    !**   1. Computing the cost function, no need to check the mode, the call to simul
    !         is at least for the computation of the cost function.
    !PRINT*, 'integrating direct '
    
    IF ( id_indic == M1QN3_COSTGRAD) THEN !required : cost function and its gradient
       IF(lg_amplitude)THEN
          rg_alpha = rga_b_param(1) + rga_param(1)
       ELSE
          rg_alpha = rga_paramt(1)
       END IF
       IF(lg_location)THEN
          rga_x0(1) = rga_b_param(2) + rga_param(2)
       ELSE
          rga_x0(1) = rga_paramt(2)
       END IF
       IF(lg_sigma)THEN
          rg_sigma  = rga_b_param(3) + rga_param(3)
       ELSE
          rg_sigma  = rga_paramt(3)
       END IF
       
       CALL cost(id_ctl, rda_ctl, rd_cost&
!!$              , u, nlocal, ne_local, t_local, scl, scl_fltwt, iter&
            )
       
       !**   2. computing the gradient
       !
       !**   2.1 Initialisation of the local adjoint variables
       !
       rl_costb = 1.0_cp
       rda_grad = 0.0_cp
       !**   2.2 Initialisation of the global adjoint variables
       !
       !CALL adjoint_zeroing
       !**   2.3 integration of the adjoint model
            !PRINT*, 'integrating adjoint '
       CALL costb(id_ctl, rda_ctl, rda_grad, rl_costb&
!!$              , u, nlocal, ne_local, t_local, scl, scl_fltwt, iter&
            )
       id_indic = M1QN3_NOTHING
    ELSE! m1qn3 is moving to the next iteration
       !nothing
    END IF

    PRINT*, 'Exiting Simulator -----------------------------'
  END SUBROUTINE simulator
  
  !> \brief Computes the cost funtion
  !! \param[in] id_ctl size of the control vector
  !! \param[in] rda_ctl control vector
  !! \param[out] rd_cost contains the value of the cost function
  !<
  SUBROUTINE cost(id_ctl, rda_ctl, rd_cost&
!!$                  , u, nlocal, ne_local, t_local, scl, scl_fltwt, iter&
             )
    IMPLICIT NONE
    !parameters for the cost function
    INTEGER, INTENT(IN)    :: id_ctl
    REAL(KIND=cp), DIMENSION(id_ctl), INTENT(IN) :: rda_ctl
    REAL(KIND=cp), INTENT(OUT)                   :: rd_cost
    !parameter for Linsolve driver
!!$    INTEGER, INTENT (IN) :: nlocal, ne_local
!!$    INTEGER  , INTENT (INOUT) :: iter ! iteration of call while adapting initial grid
!!$    REAL (pr), DIMENSION (nlocal,ne_local), INTENT (INOUT) :: u
!!$    REAL (pr), INTENT (IN)   :: scl(1:n_var),scl_fltwt
!!$
!!$    REAL (pr), INTENT (IN) :: t_local
    !local variables

    !statements
    PRINT*, 'Entering cost ++++++++++++++++++++++++++++'
!!$    lg_direct =  .TRUE.
!!$    lg_adjoint = .FALSE.
    
!!$    CALL Linsolve_driver(u, nlocal, ne_local, t_local, scl, scl_fltwt, iter)! space time integration of the direct model
    !compute the cost
    PRINT*, 'In cost : **********************************'
    PRINT*, ' ig_nobs_x = ',ig_nobs_x 
    PRINT*, ' ig_nobs_t = ', ig_nobs_t
    PRINT*, ' SIZE(iga_obs_coord) = ', SIZE(iga_obs_coord)
    PRINT*, ' SIZE(iga_obs_idx) = ', SIZE(iga_obs_idx)
    CALL get_indices_by_coordinate(ig_nobs_x*ig_nobs_t, 2**(j_lev-J_MN)*iga_obs_coord, j_lev, iga_obs_idx, 1)! Initialization
    CALL get_indices_by_coordinate(ig_nobs_x*ig_nobs_t, 2**(j_lev-J_MN)*iga_obs_coord, j_lev, iga_obs_idx, 0)! Extraction
    PRINT*, 'In cost : after getting indexes ************'
    rga_ugap = u(iga_obs_idx, 1) - rga_uobs
    rd_cost = SUM(rga_Rm1*rga_ugap**2) + SUM(rga_Bm1*rda_ctl**2)

    CALL get_indices_by_coordinate(ig_nobs_x*ig_nobs_t, 2**(j_lev-J_MN)*iga_obs_coord, j_lev, iga_obs_idx,-1)! Cleaning

    PRINT*, 'Exiting cost ++++++++++++++++++++++++++++'
  END SUBROUTINE cost
  
  !> \brief compute the gradient of the cost funtion
  !<
  SUBROUTINE costb(id_ctl, rda_ctl, rda_ctlb, rd_costb&
!!$                  , u, nlocal, ne_local, t_local, scl, scl_fltwt, iter&
             )
    IMPLICIT NONE
    INTEGER, INTENT(IN)    :: id_ctl
    REAL(KIND=cp),DIMENSION(id_ctl),INTENT(IN)    :: rda_ctl
    REAL(KIND=cp),DIMENSION(id_ctl),INTENT(INOUT) :: rda_ctlb
    REAL(KIND=cp),INTENT(INOUT)                   :: rd_costb
    !parameter for Linsolve driver
!!$    INTEGER, INTENT (IN) :: nlocal, ne_local
!!$    INTEGER  , INTENT (INOUT) :: iter ! iteration of call while adapting initial grid
!!$    REAL (pr), DIMENSION (nlocal,ne_local), INTENT (INOUT) :: u
!!$    REAL (pr), INTENT (IN)   :: scl(1:n_var),scl_fltwt
!!$    REAL (pr), INTENT (IN) :: t_local
    !local variables

    !statements
    
    PRINT*, 'Entering costb *********************************'
!!$    lg_direct = .FALSE.
!!$    lg_adjoint = .TRUE.
!/!\ think of the forcing term for the adjoint model
!!$    CALL Linsolve_driver(u, nlocal, ne_local, t_local, scl, scl_fltwt, iter)! space time integration of the adjoint model
    !compute the gradient
    rda_ctlb = rga_Bm1*rda_ctl

    !gradient with respect to the amplitude
    IF(lg_amplitude)THEN
       rda_ctlb(1) = rda_ctlb(1) &
         - SUM( u(:,2)*exp( -(x(:,1)- rga_x0(1) )**2/(2.0_pr*rg_sigma**2) ), mask = x(:,2)==rga_x0(2) )
    END IF

    !gradient with respect to the location of the gaussian
    IF(lg_location) THEN
       rda_ctlb(2) = rda_ctlb(2) &
            - SUM( 2.0_pr*u(:,2)*( (x(:,1)- rga_x0(1))/(2.0_pr*rg_sigma**2))&
                   *exp( -(x(:,1)- rga_x0(1) )**2/(2.0_pr*rg_sigma**2) ),&
                   mask = x(:,2)==rga_x0(2)&
                 )
    END IF

    !gradient with respect to the standard deviation of the gaussian
    IF(lg_sigma) THEN
       rda_ctlb(3) = rda_ctlb(3) &
            + SUM( 2.0_pr*u(:,2)*( (x(:,1)- rga_x0(1))**2/(2.0_pr*rg_sigma**3))&
                   *exp( -(x(:,1)- rga_x0(1) )**2/(2.0_pr*rg_sigma**2) ),&
                   mask = x(:,2)==rga_x0(2)&
                 )
    END IF

    PRINT*, 'Exiting costb *************************************'
  END SUBROUTINE costb
  
  !> \brief Initialized variables for data assimilation
  !<
  SUBROUTINE init_assim()
    IMPLICIT NONE
    LOGICAL, SAVE :: ll_isinit = .FALSE.
    LOGICAL, DIMENSION(3) :: lla_param
    REAL(pr), DIMENSION(:, :), ALLOCATABLE :: rla_obs
    INTEGER :: il_n_obs, il_nbCol
    
    PRINT*, 'Entering init_assim'
    IF(ll_isinit)THEN
       
    ELSE
       IF(ig_nctl==0)THEN
          WRITE(*,*) 'In init_assim : zero size control vector'
          STOP
       END IF
       lla_param = (/lg_amplitude, lg_location, lg_sigma/)
       ig_ndz = 4*ig_nctl + ig_npairs*( 2*ig_nctl + 1 )
       WRITE(*,*) 'In init_assim : allocating space for optimization process'
       ALLOCATE(&
            rga_ctl (ig_nctl),&
            rga_grad(ig_nctl),&
            rga_Bm1 (ig_nctl),&
            rga_dz  (ig_ndz ),&
            rga_Rm1 (ig_nobs_x*ig_nobs_t),&
            rga_uobs(ig_nobs_x*ig_nobs_t),&
            rga_ugap(ig_nobs_x*ig_nobs_t),&
            iga_obs_idx(ig_nobs_x*ig_nobs_t),&
            iga_obs_coord(dim, ig_nobs_x*ig_nobs_t)&
       )
       
       rga_Bm1(iga_idx_param) = 0.0_pr
       WHERE( rga_sigmaP > 0.0_pr ) rga_Bm1(iga_idx_param) = 1.0_pr/rga_sigma_param**2
       WHERE( .NOT.lla_param ) rga_Bm1(iga_idx_param) = 0.0_pr
       rga_Rm1 = 1.0_pr/rg_sigmaR**2
       ll_isinit = .TRUE.
    END IF
    !loading observations
    CALL myio_readInfo   (OBS_FILE, il_n_obs, il_nbCol)
    IF(il_nbCol/=3)THEN
       PRINT*, 'bad observation file <', OBS_FILE, '>'
       PRINT*, 'the number of column reauired is 3, found :', il_nbCol
       STOP
    END IF
    ALLOCATE ( rla_obs(il_n_obs, il_nbCol) )
    CALL myio_read_matrix(OBS_FILE, rla_obs)
    iga_obs_coord(1, :) = NINT( rla_obs(:, 1) )
    iga_obs_coord(2, :) = NINT( rla_obs(:, 2) )
    rga_uobs            = rla_obs(:, 3)
    PRINT*, 'In init_assim, obs file contains :'
    CALL print_obs(iga_obs_coord, rga_uobs)
    DEALLOCATE(rla_obs)
    
    !setting general parameters
    lg_m1qn3_initialization = .TRUE.
    lg_cost = .TRUE.
    lg_grad = .FALSE.
    lg_run_m1qn3 = .FALSE.

    PRINT*, 'Exiting init_assim '
  END SUBROUTINE init_assim

  SUBROUTINE init_direct_run()

    lg_m1qn3_initialization = .FALSE.
    lg_cost = .FALSE.
    lg_grad = .FALSE.
    lg_amplitude = .FALSE.
    lg_location  = .FALSE.
    lg_sigma     = .FALSE.
    
  END SUBROUTINE init_direct_run
  
  SUBROUTINE make_twin_obs()!u, nlocal, ne_local, t_local, scl, scl_fltwt, iter)
    IMPLICIT NONE
    !parameter for Linsolve driver
!!$    INTEGER, INTENT (IN) :: nlocal, ne_local
!!$    INTEGER  , INTENT (INOUT) :: iter ! iteration of call while adapting initial grid
!!$    REAL (pr), DIMENSION (nlocal, ne_local), INTENT (INOUT) :: u
!!$    REAL (pr), INTENT (IN)   :: scl(1:n_var),scl_fltwt
!!$    REAL (pr), INTENT (IN)   :: t_local
    !local variables
    INTEGER  :: ibi, ibj, il_shift
    REAL(pr), DIMENSION(dim) :: rla_step
    INTEGER , DIMENSION(dim) :: ila_coord
    REAL(pr), DIMENSION(ig_nobs_x*ig_nobs_t, dim+1) :: rla_obs

    PRINT*, 'Entering make_twin_obs '
    !compute obs locations and dates
    !extract obs
    rla_step(1) = REAL( 2**(ig_jmin-1)*iga_Mvector(1) )/(2*ig_nobs_x)
    rla_step(2) = REAL( 2**(ig_jmin-1)*iga_Mvector(2) )/MAX(1, ig_nobs_t-1)
    ALLOCATE(&
         iga_obs_coord(dim, ig_nobs_x*ig_nobs_t ),&
         iga_obs_idx(ig_nobs_x*ig_nobs_t), &
         rga_uobs(ig_nobs_x*ig_nobs_t)&
    )
    
    PRINT*, 'ig_nobs_x  = ', ig_nobs_x
    PRINT*, 'ig_nobs_t  = ', ig_nobs_t
    PRINT*, ' rla_step  = ', rla_step
    PRINT*, ' rga_xmin  = ', rga_xmin(1:2)
    PRINT*, ' rga_xmax  = ', rga_xmax(1:2)
    DO ibj = 1, ig_nobs_t
       il_shift = (ibj-1)*ig_nobs_x
       ila_coord(2) = INT( (ibj-1)*rla_step(2) )
       DO ibi = 1, ig_nobs_x
          ila_coord(1) = INT( (2*ibi-1)*rla_step(1) )
          iga_obs_coord( 1:2, il_shift + ibi) = ila_coord
       END DO
    END DO
    IF(ig_nobs_t == 1) iga_obs_coord(2, :) = rga_xmax(2)
    
    CALL get_indices_by_coordinate(ig_nobs_x*ig_nobs_t, 2**(j_lev-J_MN)*iga_obs_coord, j_lev, iga_obs_idx, 1)! Initialization
    CALL get_indices_by_coordinate(ig_nobs_x*ig_nobs_t, 2**(j_lev-J_MN)*iga_obs_coord, j_lev, iga_obs_idx, 0)! Extraction
    CALL get_indices_by_coordinate(ig_nobs_x*ig_nobs_t, 2**(j_lev-J_MN)*iga_obs_coord, j_lev, iga_obs_idx,-1)! Cleaning

    rga_uobs = u(iga_obs_idx, 1)
    PRINT*, 'in make_twin_obs '
    PRINT*, '***************iga_obs_coord(1,:) = '
    PRINT*, iga_obs_coord(1, :)
    PRINT*, '***************iga_obs_coord(2,:) = '
    PRINT*, iga_obs_coord(2, :)
    PRINT*, '***************iga_obs_idx = '
    PRINT*, iga_obs_idx
    PRINT*, '***************SIZE of rga_uobs = '
    PRINT*, SIZE(rga_uobs)
    PRINT*, '***************rga_uobs = '
    PRINT*, rga_uobs
    
    rla_obs(:, 1) = iga_obs_coord(1, :)
    rla_obs(:, 2) = iga_obs_coord(2, :)
    rla_obs(:, 3) = rga_uobs
    CALL myio_write_matrix('obs.dat', rla_obs)
    PRINT*, 'In make_twin_obs, obs file contains :'
    !CALL myio_write_matrix(SCREEN, rla_obs)
    CALL print_obs(iga_obs_coord, rga_uobs)
    DEALLOCATE(iga_obs_coord, iga_obs_idx, rga_uobs)
    !nvec, ixyz_vec, j_vec, i_vec, ireg
  END SUBROUTINE make_twin_obs
  
  SUBROUTINE prosca(id_ctl, rda_u, rda_v, rd_ps, ida_wa, rda_wa, dda_wa)
    INTEGER , INTENT(IN) :: id_ctl
    REAL(pr), DIMENSION(id_ctl), INTENT(IN) :: rda_u, rda_v
    REAL(pr), INTENT(OUT) :: rd_ps
    INTEGER,DIMENSION(2),INTENT(IN) :: ida_wa
    REAL(KIND=sp), DIMENSION(2),INTENT(IN) :: rda_wa
    REAL(KIND=dp), DIMENSION(2),INTENT(IN) :: dda_wa
    
    rd_ps = DOT_PRODUCT(rda_u, rda_v)
  END SUBROUTINE prosca
  
  SUBROUTINE ctonb(id_ctl, rda_x, rda_y, ida_wa, rda_wa, dda_wa)
    INTEGER , INTENT(IN) :: id_ctl
    REAL(pr), DIMENSION(id_ctl), INTENT(IN) :: rda_x
    REAL(pr), DIMENSION(id_ctl), INTENT(OUT) :: rda_y
    INTEGER,DIMENSION(2),INTENT(IN) :: ida_wa
    REAL(KIND=sp), DIMENSION(2),INTENT(IN) :: rda_wa
    REAL(KIND=dp), DIMENSION(2),INTENT(IN) :: dda_wa
     
    rda_y = rda_x
  END SUBROUTINE ctonb
  
  SUBROUTINE ctcab(id_ctl, rda_y, rda_x, ida_wa, rda_wa, dda_wa)
    INTEGER , INTENT(IN) :: id_ctl
    REAL(pr), DIMENSION(id_ctl), INTENT(IN) :: rda_y
    REAL(pr), DIMENSION(id_ctl), INTENT(OUT) :: rda_x
    INTEGER,DIMENSION(2),INTENT(IN) :: ida_wa
    REAL(KIND=sp), DIMENSION(2),INTENT(IN) :: rda_wa
    REAL(KIND=dp), DIMENSION(2),INTENT(IN) :: dda_wa
     
    rda_x = rda_y
  END SUBROUTINE ctcab

  SUBROUTINE myio_write_matrix(fileName, rda_A)
    REAL(KIND=dp), DIMENSION(:, :), INTENT(IN) :: rda_A
    CHARACTER(LEN=*)         , INTENT(IN) :: fileName
    INTEGER ib_i, ib_j, il_ios
    INTEGER :: ip_fid = 41

    IF(fileName==SCREEN) THEN
      ip_fid = 6!standard output
      PRINT*, 'rows count = ', size(rda_A, 1)
      PRINT*, 'columns count = ', size(rda_A, 2)
    ELSE
      ip_fid = 41
      OPEN( UNIT = ip_fid, FILE = fileName, STATUS = 'REPLACE', FORM = 'FORMATTED', IOSTAT = il_ios)
      IF (il_ios /= 0) then
        WRITE(* , *) 'Error creating file', fileName
        STOP;
      END IF
    END IF
    PRINT*, "In writeMatrix", LBOUND(rda_A,1), UBOUND(rda_A,1), LBOUND(rda_A,2), UBOUND(rda_A,2)
    !stop
    WRITE(unit=ip_fid, fmt=IFMT) size(rda_A, 1)
    WRITE(unit=ip_fid, fmt=IFMT) size(rda_A, 2)
    DO ib_j = LBOUND(rda_A,2), UBOUND(rda_A,2)
    	DO ib_i = LBOUND(rda_A,1), UBOUND(rda_A,1)
        WRITE(unit=ip_fid, fmt='(E18.8)', advance="no") rda_A(ib_i,ib_j)!$ to avoid end of line
      END DO
      WRITE(unit=ip_fid, fmt=*)''! just for adding end of line
    END DO
    IF(fileName/=SCREEN) THEN
      CLOSE(ip_fid )
    END IF
  END SUBROUTINE myio_write_matrix

  SUBROUTINE myio_read_matrix(fileName, rda_A)
    REAL(KIND=dp), DIMENSION(:, :), INTENT(OUT) :: rda_A
    CHARACTER(LEN=*)         , INTENT(IN)  :: fileName
    INTEGER ib_i, ib_j, il_nbRow, il_nbCol, il_ios
    INTEGER , PARAMETER :: ip_fid =615
      
    OPEN( UNIT = ip_fid, FILE = fileName, IOSTAT = il_ios)
    IF (il_ios /= 0) then
      WRITE(* , *) 'Error opening file', fileName
      STOP;
    END IF
    !stop
    READ(UNIT=ip_fid, FMT=IFMT) il_nbRow
    READ(UNIT=ip_fid, FMT=IFMT) il_nbCol
    PRINT*, "In readMatrix, il_nbRow = ", il_nbRow, "; il_nbCol = ", il_nbCol
    DO ib_j = 1, il_nbCol
       DO ib_i = 1, il_nbRow 
          READ(UNIT=ip_fid, FMT='(E18.8)', advance="no") rda_A(ib_i,ib_j)!$ to avoid going to the next line
          PRINT*, 'row, col', ib_i, ib_j, ' : ', rda_A(ib_i,ib_j)
       END DO
       READ(unit=ip_fid, fmt=*) !just to skip the end of line
    END DO
    CLOSE(ip_fid )
  END SUBROUTINE myio_read_matrix

  !!read the head of the file: nbRow and nbCol
  SUBROUTINE  myio_readInfo(fileName, id_nbRow, id_nbCol)
    INTEGER , PARAMETER :: ip_fid =616
    CHARACTER(LEN=*)         , INTENT(IN)  :: fileName
    INTEGER, INTENT(OUT) :: id_nbRow, id_nbCol
    INTEGER :: il_ios
    
    OPEN( UNIT = ip_fid, FILE = fileName, IOSTAT = il_ios)
    IF (il_ios /= 0) then
      WRITE(* , *) 'In readInfo : Error opening file', fileName
      STOP;
    END IF
    READ(UNIT=ip_fid, FMT=IFMT) id_nbRow
    READ(UNIT=ip_fid, FMT=IFMT) id_nbCol
    PRINT*, "In readInfo, id_nbRow = ", id_nbRow, "; id_nbCol = ", id_nbCol
    CLOSE(ip_fid )
  END SUBROUTINE myio_readInfo

  SUBROUTINE print_obs(iga_obs_coord, rga_uobs)
    INTEGER , DIMENSION(:,:), INTENT(IN) :: iga_obs_coord
    REAL(pr), DIMENSION(:), INTENT(IN) :: rga_uobs
    INTEGER ::  ibj
    
    WRITE(*,*) ' Num  Coord1  Coord2  Value'
    WRITE(*,*) '--------------------------'
    
    DO ibj = 1, SIZE(rga_uobs)
       WRITE(*, '(I5, I8, I8, E10.2)') ibj, iga_obs_coord(:, ibj), rga_uobs(ibj)
    END DO
  END SUBROUTINE print_obs
!!!!!!!!!!!!!!!!!!!!
END MODULE user_case
