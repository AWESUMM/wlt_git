MODULE user_case
!
! Case elliptic poisson 
  USE precision
  USE elliptic_vars
  USE elliptic_mod
  USE field
  USE input_file_reader
  USE io_3D_vars
  USE pde
  USE share_consts
  USE share_kry
  USE sizes
  USE util_mod
  USE util_vars
  USE vector_util_mod
  USE wavelet_filters_mod
  USE wlt_vars
  USE wlt_trns_vars
  USE wlt_trns_mod
  USE wlt_trns_util_mod
  USE io_3D_vars
  USE fft_module
  USE spectra_module
  USE SGS_incompressible
  !
  ! case specific variables
  !
  INTEGER n_var_vorticity ! start of vorticity in u array
  INTEGER n_var_pressure  ! start of pressure in u array
  INTEGER n_var_phase     ! start phase in u array
  !
  !--------------- variables for phase-diagrpam calculation
  !
  INTEGER, PARAMETER ::  nphase_max = 1000
  INTEGER::  nphase
  INTEGER , DIMENSION(10,nphase_max)::  iphase
  INTEGER , DIMENSION(nphase_max)::  shuffle, shuffle_inv
  !
  ! local variables
  !
CONTAINS

  !
  ! In user_setup_pde() we setup how many variables are integrated, which are interpolated
  ! to the next times step and if any exeact solution exists to compare to during the run.
  ! We also set the variable names that are used when the result files are written.
  !
  ! The following variables must be setup in this routine:
  !
  !
  ! n_integrated     ! first n_integrated eqns will be acted on for time integration
  ! n_var_additional ! 
  ! n_var
  !
  !
  !
  !
  SUBROUTINE  user_setup_pde ( VERB ) 
    IMPLICIT NONE
    LOGICAL, OPTIONAL :: VERB         ! print debug info
    INTEGER :: i

    PRINT * ,''
    PRINT *, '**********************Setting up PDE*****************'
    PRINT * ,'CASE Elliptic '
    PRINT *, '*****************************************************'

    n_integrated = 3 ! dim
	n_var_phase = 3
    n_time_levels = 1  !--# time levels

    n_var_time_levels = n_time_levels * n_integrated !--Number of equations (2D velocity at two time levels)

    n_var_additional = 0 !only doing elliptic solver !
    n_var = n_var_time_levels + n_var_additional !--Total number of variables

    n_var_exact = 0 !--No exact solution 


    n_var_pressure  = n_var_time_levels ! ! no pressure

    !
    !--Allocate logical arrays 
    !  Must be done after setting number of different types of variables
    !  and before setting logical variable mapping arrays
    !
    CALL alloc_variable_mappings


    !
    ! Fill in variable names for default variables for integration meth 2
    ! These may be changed in the call to the user specified routine
    !  user_setup_pde()
    !
    ! In integrated variables (3D)
    IF( dim == 3 ) THEN
       WRITE (u_variable_names(1), u_variable_names_fmt)  'Density     '
       WRITE (u_variable_names(2), u_variable_names_fmt)  'Enthalpy    '
       WRITE (u_variable_names(3), u_variable_names_fmt)  'Phase       '
    ELSE IF(dim == 2) THEN
       WRITE (u_variable_names(1), u_variable_names_fmt)  'Density     '
       WRITE (u_variable_names(2), u_variable_names_fmt)  'Enthalpy    '
       WRITE (u_variable_names(3), u_variable_names_fmt)  'Phase       '
    END IF


    !
    ! setup logical variable mapping arrays
    ! This defined which variables are used for adaptation, Which need to be interpololated to the new grid at 
    ! each time step, which are saved, and for which we have an exect solutiuon we want to check at each time step
    !


    !
    ! setup which components we will base grid adaptation on.
    ! Adapt on the 3 velocity components
    !
    n_var_adapt(1:n_integrated,0) = .TRUE. !--Initially adapt on integrated variables at first time level
    n_var_adapt(1:n_integrated,1) = .TRUE. !--After first time step adapt on 

    !Variables that need to be interpoleted to new adapted grid in initial grid adaptation
    n_var_interpolate(1:n_integrated,0) = .TRUE. 

    !Variables that need to be interpoleted to new adapted grid at each time step
    !n_var_interpolate(1:n_var_pressure,1) = .TRUE. 

    !
    ! setup which components we have an exact solution for
    !
    n_var_exact_soln(:,0:1) = .FALSE.
 


    !
    ! setup which variables we will save the solution
    !
    n_var_save(1:3) = .TRUE. ! save all for restarting code


	! Setup which variables are required on restart
	!
    n_var_req_restart = .FALSE.
    n_var_req_restart(1:n_integrated) = .TRUE.
    !
    !--Time level counters for NS integration
    !   Used for integration meth2
    ! n0 is current time step
    ! n1 is t_n-1 time step
    ! n2 is t_n-2 time step
    !
    n0 = 1; n1 = n0+dim; n2 = n1+dim


    !
    ! Set the maximum number of components for which we have an exact solution
    !
    n_var_exact = 1 ! MAX( COUNT(n_var_exact_soln(:,0)),COUNT(n_var_exact_soln(:,1)) )

    !
	! Setup a scaleCoeff array of we want to tweak the scaling of a variable
	! ( if scaleCoeff < 1.0 then more points will be added to grid 
	!
    ALLOCATE ( scaleCoeff(1:n_var) )
    scaleCoeff = 1.0_pr
    scaleCoeff(n_var_phase) = 1.0e1_pr

    PRINT *, 'n_integrated = ',n_integrated 
    PRINT *, 'n_time_levels = ',n_time_levels
    PRINT *, 'n_var_time_levels = ',n_var_time_levels 
    PRINT *, 'n_var = ',n_var 
    PRINT *, 'n_var_exact = ',n_var_exact 
    PRINT *, '*******************Variable Names*******************'
    DO i = 1,n_var
       WRITE (*, u_variable_names_fmt) u_variable_names(i)
    END DO
    PRINT *, '****************************************************'

  END SUBROUTINE  user_setup_pde

  !
  ! read variables from input file for this user case 
  !
  SUBROUTINE  user_read_input()
    IMPLICIT NONE


!!$  ! examples of reading
!!$  !  call input_real ('Mcoeff', Mcoeff, 'stop', &
!!$  !     ' Mcoeff ! Mij = Mcoeff * |S>2eps| Sij>2eps - (|S|Sij )>2eps')
!!$
!!$
!!$  ! call input_integer ('mdl_filt_type_grid', mdl_filt_type_grid, 'stop', &
!!$       ' mdl_filt_type_grid ! dyn mdl grid filter (defined in make_mdl_filts() )')
!!$
!!$   call input_logical ('clip_Cs', clip_Cs, 'stop', &
!!$       ' clip_Cs ! clip Cs if true')

  END SUBROUTINE  user_read_input


  !
  ! Set the exact solution for comparison to the simulated solution
  !
  ! u          - array to fill in the exact solution
  ! nlocal       - number of active wavelets   
  ! ne_local        - total number of equations
  ! t          - time of current time step 
  ! l_n_var_exact_soln_index - index into the elements of u for which we need to 
  !                            find the exact solution
  SUBROUTINE  user_exact_soln (u, nlocal,  t_local, l_n_var_exact_soln)  
    USE precision
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: nlocal
    REAL (pr), INTENT (IN) ::  t_local
    REAL (pr), DIMENSION (nlocal,n_var_exact), INTENT (INOUT) :: u
    LOGICAL , INTENT (IN) :: l_n_var_exact_soln(n_var)
    INTEGER :: i

  END SUBROUTINE  user_exact_soln

  SUBROUTINE user_initial_conditions (u, nlocal, ne_local, t_local, scl, scl_fltwt, iter)
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: nlocal, ne_local
    INTEGER  , INTENT (INOUT) :: iter ! iteration of call while adapting initial grid
    REAL (pr), DIMENSION (nlocal,ne_local), INTENT (INOUT) :: u
    REAL (pr), DIMENSION (nlocal,ne_local) :: f

    REAL (pr), INTENT (IN)   :: scl(1:n_var),scl_fltwt
    REAL (pr), INTENT (IN) :: t_local
    REAL (pr), DIMENSION (1:4):: vec
    INTEGER, DIMENSION (10) ::  phs

    INTEGER :: i, ii, i1, i2, ih

  INTERFACE
     SUBROUTINE PTpoint3D(x,y,z,vec,phs)
       USE precision
       REAL (pr) ::  x, y, z
       REAL (pr), DIMENSION (4) ::  vec
       INTEGER, DIMENSION (10) ::  phs
     END SUBROUTINE PTpoint3D
  END INTERFACE

    !set variables by hand
    IF(dim == 2) THEN
    ELSE
    END IF

   ! set Initial Conditions
    IF( dim == 2 ) THEN
       DO i = 1, nwlt
          CALL PTpoint3D(x(i,1),x(i,2),1.0_pr,vec,phs)
          u(i,1) = vec(2)
          u(i,2) = vec(4)
          u(i,3) = phase_number(phs)
       END DO
    ELSE IF (dim == 3 ) THEN
       DO i = 1, nwlt
          CALL PTpoint3D(x(i,1),x(i,2),x(i,3),vec,phs)
          u(i,1) = vec(2)
          u(i,2) = vec(4)
          u(i,3) = phase_number(phs)
       END DO
    END IF
  WRITE(*,'("finish calculating phases at new points")')
  WRITE(*,'("# of phases: ",I4)') nphase
  IF(nphase > 2) THEN
     ii = 163
     DO i = 1,nphase
        shuffle(i) = i
     END DO
     DO i = 1,nphase
        i1  = MIN(INT(nphase*URAN(ii))+1,nphase)
        i2  = MIN(INT(nphase*URAN(ii))+1,nphase)
        ih           = shuffle(i1) 
        phs(:)       = iphase(:,i1)
        shuffle(i1)  = shuffle(i2)
        shuffle(i2)  = ih
        iphase(:,i1) = iphase(:,i2)
        iphase(:,i2) = phs
     END DO
     DO i =1,nphase
        shuffle_inv(shuffle(i)) = i
     END DO
     DO i =1,nwlt
        ih = INT(u(i,3)+0.01_pr)
        u(i,3) = shuffle_inv(ih)
     END DO
  END IF

  END SUBROUTINE user_initial_conditions

!--********************************  
  ! Arguments
  ! u         - field on adaptive grid
  ! nlocal      - number of active points
  ! ne_local       - number of equations
  ! t         - current time
  !
  ! Global variables used
  ! ifn       - number of equations
  ! x()
  ! xO
  ! y0
  !--******************************** 
  SUBROUTINE user_algebraic_BC (Lu, u, nlocal, ne_local, jlev, meth)
    !--Defines boundary condition type
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: jlev, meth, ne_local, nlocal
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (INOUT) :: Lu
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (IN)    :: u

    INTEGER :: ie, ii, shift
    REAL (pr), DIMENSION (nlocal,ne_local) :: du, d2u

    
  !
  ! There are periodic BC conditions
  !


  END SUBROUTINE user_algebraic_BC

  SUBROUTINE user_algebraic_BC_diag (Lu, nlocal, ne_local, jlev, meth)
    !--Defines boundary condition type
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: jlev, meth, ne_local, nlocal
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (INOUT) :: Lu

    INTEGER :: ie, ii, shift
    REAL (pr), DIMENSION (nlocal,ne_local) :: du, d2u

  !
  ! There are periodic BC conditions
  !

  END SUBROUTINE user_algebraic_BC_diag



  SUBROUTINE user_algebraic_BC_rhs (rhs, ne_local, nlocal, jlev)
    !--Sets rhs for boundary conditions
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: ne_local, nlocal, jlev
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (INOUT) :: rhs

    INTEGER :: ie, ii, shift
  !
  ! There are periodic BC conditions
  !

  END SUBROUTINE user_algebraic_BC_rhs

  FUNCTION user_rhs (u_integrated,p)
    IMPLICIT NONE
    REAL (pr), DIMENSION (ng,ne), INTENT(IN) :: u_integrated !1D flat version of integrated variables without BC 
    REAL (pr), DIMENSION (ng), INTENT(IN) :: p
    REAL (pr), DIMENSION (n) :: user_rhs

  END FUNCTION user_rhs

  ! find Jacobian of Right Hand Side of the problem
  FUNCTION user_Drhs (u, u_prev_timestep_loc, meth)
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: meth
	!u_prev_timestep passed local to cast it into 2dim array to work with vector derivatives
    REAL (pr), DIMENSION (ng,ne) :: u, u_prev_timestep_loc
    REAL (pr), DIMENSION (n) :: user_Drhs

  END FUNCTION user_Drhs


  !
  ! Uses u_prev_timestep, which is global flat (1D ) array set in time_step_cn()
  !
  FUNCTION user_Drhs_diag (meth)
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: meth
    REAL (pr), DIMENSION (n) :: user_Drhs_diag

  END FUNCTION user_Drhs_diag

  SUBROUTINE user_project (u, p, nlocal, meth)
    !--Makes u divergence free
    IMPLICIT NONE

    INTEGER, INTENT (IN) :: meth, nlocal
    REAL (pr), DIMENSION (nlocal),     INTENT(INOUT) :: p
    REAL (pr), DIMENSION (nlocal,n_integrated), INTENT(INOUT) :: u

  END SUBROUTINE user_project

  FUNCTION user_chi (nlocal, t_local )
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: nlocal
    REAL (pr), INTENT (IN) :: t_local
    REAL (pr), DIMENSION (nlocal) :: user_chi

    user_chi = 0.0_pr
  END FUNCTION user_chi
!
! Calculate any statitics
!
! startup_flag - 0 when adapting to IC,then 1 inmain integration loop
!
!
SUBROUTINE user_stats ( u ,j_mn, startup_flag)
  USE vector_util_mod
  IMPLICIT NONE
  REAL (pr), DIMENSION (nwlt,1:n_var), INTENT (IN) :: u
  INTEGER , INTENT (IN) :: j_mn 
  INTEGER , INTENT (IN) :: startup_flag

END SUBROUTINE user_stats

  SUBROUTINE user_cal_force (u, n, t_local, force, drag, lift)
    !--Calculates drag and lift on obstacle using penalization formula
    IMPLICIT NONE

    INTEGER, INTENT (IN) :: n
    REAL (pr), INTENT (IN) :: t_local
    REAL (pr), DIMENSION (dim), INTENT (INOUT) :: force
    REAL (pr), INTENT (OUT) :: drag, lift
    REAL (pr), DIMENSION (n,dim) :: u
    drag = 0.0_PR
    lift = 0.0_PR

    !
    ! There is no obstacle in flow
    !

  END SUBROUTINE user_cal_force

  !
  ! calculate any additional variables
  ! 
  ! arg 
  ! flag  - 0 calledwhile adapting to IC, 1 - called in main time integration loop
  !
  ! These additiona variables are calculated and left in real space.
  !
  SUBROUTINE user_additional_vars( t_local, flag )
    IMPLICIT NONE
    REAL (pr), INTENT (IN) ::  t_local
    INTEGER , INTENT(IN) :: flag ! 0- called during adaption to IC, 1 called during main integration loop

  END SUBROUTINE user_additional_vars


  !
  ! calculate any additional scalar variables
  ! 
  ! arg 
  ! flag  - 0 calledwhile adapting to IC, 1 - called in main time integration loop
  !
  SUBROUTINE user_scalar_vars( flag )
    IMPLICIT NONE
    INTEGER , INTENT(IN) :: flag ! 0- called during adaption to IC, 1 called during main integration loop

  END SUBROUTINE user_scalar_vars

  !
  !************ Calculating Scales ***************************
  !
  ! Note the order of the components in the scl array
  ! correspond to u_tn, v_tn, w_tn, u_tn-1, v_tn-1, w_tn-1, u_tn-2, v_tn-2, w_tn-2
  ! 
  SUBROUTINE user_scales(flag, use_default, u_loc, nlocal, ne_local, l_n_var_adapt , l_n_var_adapt_index, &
       scl, scl_fltwt) !add
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: flag ! 0 during initial adaptation, then 1 during time advancement
    LOGICAL , INTENT(INOUT) :: use_default
    INTEGER, INTENT (IN) :: nlocal, ne_local
    REAL (pr), DIMENSION (1:nlocal,1:ne_local), INTENT (IN) :: u_loc
    LOGICAL , INTENT (IN) :: l_n_var_adapt(ne_local)
    INTEGER , INTENT (IN) :: l_n_var_adapt_index(1:ne_local)
    REAL (pr), DIMENSION (1:ne_local), INTENT (INOUT) :: scl
    REAL (pr) ,  INTENT(IN) ::scl_fltwt !weight for temporal filter on scl
    REAL (pr) :: floor, tmp
    INTEGER :: ie, ie_index
    REAL (pr) ,save, ALLOCATABLE, DIMENSION (:) :: scl_old
    LOGICAL  ,save :: startup_init = .TRUE.

    !
    ! Ignore the output of this routine and use default scales routine
    !
    use_default = .TRUE. 

    !
    ! NOTE: For a parallel run, synchronize scl(:) across the processors in user_scales.
    !       Use the subroutine scales of default_util.f90 as an example,
    !       subroutine parallel_global_sum of parallel.f90 may be helpful.
    !       Already synchronized: sumdA_global
  END SUBROUTINE user_scales

  SUBROUTINE user_cal_cfl (use_default, u, cfl_out)
    IMPLICIT NONE
    LOGICAL , INTENT(INOUT) :: use_default
    REAL (pr),                                INTENT (INOUT) :: cfl_out
    REAL (pr), DIMENSION (nwlt,n_integrated), INTENT (IN)    :: u
    
    use_default = .TRUE.
    
  END SUBROUTINE user_cal_cfl

  SUBROUTINE  user_sgs_force ( u, nlocal)
    IMPLICIT NONE

    INTEGER,                         INTENT (IN) :: nlocal
    REAL (pr), DIMENSION (nlocal,n_integrated), INTENT (INOUT) :: u

  END SUBROUTINE  user_sgs_force 

  INTEGER FUNCTION phase_number (phs)
    IMPLICIT NONE
    INTEGER, DIMENSION (10), INTENT(IN) ::  phs
    INTEGER :: check, iph
    
    check = 1
    iph   = 0
    DO WHILE(iph < nphase .AND. check /= 0)
       iph = iph + 1
       check = MAXVAL(ABS(phs - iphase(:,iph))) 
    END DO
    IF(check /= 0 ) THEN
       nphase = nphase +1
       iphase(:,nphase) = phs
       phase_number = nphase
    ELSE
       phase_number = iph
    END IF
    IF(nphase >= nphase_max) THEN
       WRITE(*,'("ERROR: the number of phases is too low!")')
       STOP
    END IF
    
  END FUNCTION phase_number
  !*********************************************************************
  !*     > SUB.URAND1 <                                                *
  !*-------------------------------------------------------------------*
  !*    Uniform RANDom number generator                                *
  !*-------------------------------------------------------------------*
  !*    MIXED CONGRUENTIAL METHOD                                      *
  !*    PORTABLE BUT SLOW.  THE PERIOD IS ONLY 1664501.                *
  !* PARAMETERS                                                        *
  !*   (1) N      (I) THE NUMBER OF RANDOM NUMBERS TO BE GENERATED     *
  !*                  (INPUT)                                          *
  !*   (2) X      (D) UNIFORM RANDOM NUMBERS (OUTPUT)                  *
  !*   (3) IR     (I) THE INITIAL SEED  (INPUT)                        *
  !*                  THE SEED FOR THE NEXT CALL (OUTPUT)              *
  !* COPYRIGHT: Y. OYANAGI, JUNE 30, 1989  V.1                         *
  !*********************************************************************
  FUNCTION URAN(IR)
    USE precision
    IMPLICIT NONE
    INTEGER :: IR
    REAL(pr)::   URAN
    REAL(pr)::   INVM
    INTEGER, PARAMETER :: M = 1664501, LAMBDA = 1229, MU = 351750
    
    INVM = 1.0_pr / REAL(M,pr)
    IR = MOD( LAMBDA * IR + MU, M)
    URAN = IR * INVM
  END FUNCTION URAN
  !*********************************************************************

  FUNCTION user_sound_speed (u, neq, nwlt)
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: nwlt, neq
    REAL (pr), DIMENSION (nwlt,neq), INTENT(IN) :: u
    REAL (pr), DIMENSION (nwlt) :: user_sound_speed

    !user_sound_speed(:) = SQRT( gamma*(gamma-1.0_pr)* &
    !       ( u(:,4)-0.5_pr*SUM(u(:,n_var_mom(1:dim))**2,DIM=2)/u(:,1)) ) ! pressure

    user_sound_speed(:) = 0.0_pr

  END FUNCTION user_sound_speed

  SUBROUTINE  user_pre_process
    IMPLICIT NONE
     
  END SUBROUTINE user_pre_process

  SUBROUTINE  user_post_process
    IMPLICIT NONE
    
  END SUBROUTINE user_post_process

END MODULE user_case
