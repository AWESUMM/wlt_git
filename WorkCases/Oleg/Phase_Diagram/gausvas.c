/* CLEAR mat0(rm) */
void mat0clear(int rm, double *adr)
/* rm - rank of matrix */
/* adr - adres of first element of matrix */
{
/* String lenth */
/* int rm1=rm+1; */
/* Counters */
int m1,m2;
/**/
for (m1=0;m1<rm;m1++)
for (m2=0;m2<=rm;m2++)
	{
	*adr=0;
	adr++;
	}
}
/* End CLEAR mat0(rm) */



/* SOLVE mat0() BY USING GAUSS METHOD */
void gausmat0(int rm, double *adr, double *adr1)
/* rm - rank of matrix */
/* adr - adres of first element of matrix */
/* adr1 - adres of first element of solutions */
{
/* Counters */
int m1,m2,m3;
/* Buffer for val */
double val1, val2;
/* String lenth */
int rm1=rm+1;
/**/
/* STEP 2: KOEF OPERATIONS */
for (m1=0;m1<rm-1;m1++)
if ((val1=*(adr+m1*rm1+m1))!=0)
	for (m2=m1+1;m2<rm;m2++)
	if ((val2=*(adr+m2*rm1+m1))!=0)
		for (m3=m1+1;m3<=rm;m3++)
		/* F[]  G[] */
		*(adr+m2*rm1+m3)=(*(adr+m2*rm1+m3))/val2-(*(adr+m1*rm1+m3))/val1;
/* End STEP 2: KOEF OPERATIONS */
/**/
/* STEP 3: SOLUTION CALC CHECK */
for (m1=rm-1;m1>=0;m1--)
	{
	/* Clear Sol */
	*(adr1+m1)=0;
	if ((val1=*(adr+m1*rm1+m1))!=0)
		{
		/* Calc Sol */
		val2=*(adr+m1*rm1+rm)/val1;
		*(adr1+m1)=val2;
		/* Use Sol */
		for (m2=0;m2<=m1-1;m2++)
		if ((val1=*(adr+m2*rm1+m1))!=0)
		*(adr+m2*rm1+rm)-=val1*val2;
		}
	}
/* End STEP 3: SOLUTION CALC CHECK */
}
/* End SOLVE m0() BY USING GAUSS METHOD */

