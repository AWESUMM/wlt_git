/* CALC G OF ASSEMBLAGE */
double gsyscalc()
{
/* Counter */
int m1;
/* G */
double gs=0;
/**/
for (m1=0;m1<cnum;m1++)
gs+=phasgg[m1][0]*phascc[m1];
return gs;
}
/* End CALC G OF ASSEMBLAGE */


/* RELOAD wi[] FROM dwgg[] */
void wiload(int mdl)
/* mdl - model number */
{
/* Counters */
int m2,p,w,word;
/**/
/* Load wi[] */
if ((p=dmod[mdl][4])>=0)
	{
	w=dmod[mdl][3];
	/**/
	/* wi[] add */
	for (m2=0;m2<w;m2++)
	/* Check Activ(0), Empty(-1) W in model */
	if (dwww[p+m2][1]>=0)
		{
		/* wi[] add:  H S V */
		word=dwww[p+m2][0];
		wi[word]=dwgg[p+m2];
		}
	}
}
/* End RELOAD wi[] FROM dwgg[] */


/* CLEAR wi[] */
void wiclear(int mdl)
/* mdl - model number */
{
/* Counters */
int m2,p,w,word;
/**/
/* Load wi[] */
if ((p=dmod[mdl][4])>=0)
	{
	w=dmod[mdl][3];
	/**/
	/* wi[] add */
	for (m2=0;m2<w;m2++)
	/* Check Activ(0), Empty(-1) W in model */
	if (dwww[p+m2][1]>=0)
		{
		/* wi[] add:  H S V */
		word=dwww[p+m2][0];
		wi[word]=0;
		}
	}
}
/* End CLEAR wi[] */



/* LOAD xi[] from phasxx[], kmnm[] */
void xiload(int minpos, double *fromxi)
/* minpos - position in dmnm[] */
/* *fromxi - pos in source array */
{
/* Counters */
int m1,min,xord;
double *fxi;
/**/
/* Reload operation */
for(m1=0;m1<dmnm[minpos][1];m1++)
	{
	/* Num of cur minal */
	min=dmnm[minpos][2+m1];
	xord=dmin[min][1];
	fxi=fromxi+m1;
	xi[xord]=*fxi;
	}
}
/* End LOAD xi[] from phasxx[], kmnm[] */


/* G CALC FOR CUR SS */
double gsscalc(int minpos, int gpos)
/* minpos - number of SS in dmnm[] */
/* gpos - pos in phasgg[][gpos] */
{
/* Counter */
int m1,min,xord;
/* G SS */
double gss=0,ival;
/**/
/* Calc Gst of SS */
for (m1=0;m1<dmnm[minpos][1];m1++)
	{
	/* Num of cur minal */
	min=dmnm[minpos][2+m1];
	xord=dmin[min][1];
	gio[xord]=phasgg[cnum+1+min][gpos];
/*
printf("%d %d %d %s %s %e %e\n",gpos,xord,min,&nmin[min][0],&mdnm[dmnm[minpos][0]][0],phasgg[cnum+1+min][gpos],xi[xord]);
*/
	}
/**/
/* Calc Gmix of SS */
/* Model Number */
/* Gmix Calc */
gss=models(dmod[dmnm[minpos][0]][0],1);
/*
printf("%d %s %e",minpos,&mdnm[dmnm[minpos][0]][0],gss);
getchar();
*/
/**/
return gss;
}
/* End G CALC FOR CUR SS */


/* CALCULATION OF G FOR MINALS & SS */
void curgcalc()
{
/* Counters */
int m1,m2,m3,mdl,mdl1,p,w,minpos,setpos;
/**/
/* T K for Cp, V */
double ctk=tkcur,ival;
double vtk=tkcur-TR;
/* P bar for V */
double vpb=pbcur-PR;
/**/
/**/
/**/
/* WG CALC FOR ALL MODELS */
for (m1=0;m1<pnum;m1++)
/* Check Not Empty, Non Ideal Model, 0<xi[]<1 */
if ((p=dmod[m1][4])>=0)
	{
	w=dmod[m1][3];
	/**/
	/* wi[] add */
	for (m2=0;m2<w;m2++)
	/* Check Activ(0), Empty(-1) W in model */
	if (dwww[p+m2][1]>=0)
		{
		/* Clear  Add WG */
		dwgg[p+m2]=0;
		if (dwww[p+m2][2] != -1) dwgg[p+m2]+=solut[0][dwww[p+m2][2]]*1e+3/4.1837;
		if (dwww[p+m2][3] != -1) dwgg[p+m2]-=solut[0][dwww[p+m2][3]]*ctk/4.1837;
		if (dwww[p+m2][4] != -1) dwgg[p+m2]+=solut[0][dwww[p+m2][4]]*pbcur/4.1837;
		}
	}
/* End WG CALC FOR ALL MODELS */
/**/
/**/
/**/
/* G CALC FOR ALL MINALS */
for (m1=0;m1<mnum;m1++)
	{
	/* Model Num read */
	mdl=dmin[m1][0];
	mdl1=dmod[mdl][0];
	/* Clear G */
	setpos=cnum+1+m1;
	phasgg[setpos][0]=0;
	/* Load ws[] */
	for (m3=1;m3<=mpos;m3++) 
		{
		if (dmin[m1][m3+1] != -1) 
			{
			ws[m3]=solut[0][dmin[m1][m3+1]];
			}
		}
			
        /* G stand calc */
	ival=models(mdl1,-1);
	phasgg[setpos][0]=models(mdl1,-1);
	/* Save G */
	phasgg[setpos][1]=phasgg[setpos][0];
/*
printf("%e %e %d %s %s %e\n",tkcur-273.0,pbcur*1e-3,m1,&nmin[m1][0],&mdnm[mdl][0],phasgg[setpos][0]);
*/
	}
/*
getchar();
*/
/* End G CALC FOR ALL MINALS */
/**/
/**/
/**/
/* G CALC FOR ALL SS */
for (m1=0;m1<inum;m1++)
	{
	minpos=m1;
	setpos=m1+mnum+cnum+1;
	xiload(minpos,&phasxx[cnum+m1][0]);
	/* Load wi[] */
	wiload(dmnm[minpos][0]);
	/* Calc G */
	phasgg[setpos][0]=phasgg[setpos][1]=gsscalc(minpos,0);
	/* Clear wi[] */
	wiclear(dmnm[minpos][0]);
	}
/**/
/**/
/**/
/* G CALC FOR ALL BEST */
for (m1=0;m1<cnum;m1++)
	{
	setpos=m1;
	/* Fictiv phases */
	if(phaset[m1]<cnum)
	phasgg[setpos][0]=phasgg[setpos][1]=ginit;
	/* Pure Minals */
	if(phaset[m1]>cnum && phaset[m1]<cnum+1+mnum)
	phasgg[setpos][0]=phasgg[setpos][1]=phasgg[phaset[m1]][0];
	/* SS */
	if(phaset[m1]>cnum+mnum)
		{
		minpos=phaset[m1]-mnum-cnum-1;
		xiload(minpos,&phasxx[m1][0]);
		/* Load wi[] */
		wiload(dmnm[minpos][0]);
		/* Calc G */
		phasgg[setpos][0]=phasgg[setpos][1]=gsscalc(minpos,0);
		/* Clear wi[] */
		wiclear(dmnm[minpos][0]);
		}
	}
/**/
/* Calculation of G of system */
gsys0=gsyscalc();
}
/* End CALCULATION OF G FOR MINALS & SS */



/* CALC STEHIOMETRY & dG OF REACTION */
double stdgreac()
{
/* Counters */
int m0,m1,m2;
/* dG of reaction */
double dgreac;
/**/
/**/
/**/
/* Clear mat1[] matrix */
mat0clear(cnum,mat1);
/**/
/* Add m0[] matrix */
for (m0=0;m0<cnum;m0++)
for (m1=0;m1<cnum;m1++)
for (m2=0;m2<=cnum;m2++)
mat1[m1*(cnum+1)+m2]+=phasmb[m2][m0]*phasmb[m1][m0];
/**/
/* Solve mat1[] matrix */
gausmat0(cnum,mat1,sol1);
/**/
/* CALC G EFFECT OF REACTION */
dgreac=phasgg[cnum][0];
for (m1=0;m1<cnum;m1++)
	{
/*
	if(ABSV(sol1[m1])<minsol) sol1[m1]=0;
*/
	dgreac-=phasgg[m1][0]*sol1[m1];
	}
/**/
return dgreac;
}
/* End CALC STEHIOMETRY & dG OF REACTION */


/* MIN G ASSEMBLAGE SELECTION */
int curming()
{
/* Counters */
int m1;
/* Positions of counter in phaset[] */
int setpos=0;
/* Min Founded Y/N */
int ynokbest=0;
/* Smalest Phase num, concentration, cur concentration */
int phminn;
double phmins,phval;
/**/
/**/
/* CUR MIN G ASSEMBLAGE SERCH */
for (setpos=cnum+1;setpos<cnum+1+mnum+inum;setpos++)
/* Use only free Phases */
if (phaset[setpos]==0 && (setpos>cnum+1+mnum || dmod[dmin[setpos-1-cnum][0]][1]==0))
	{
	/* Save Cur Phase num, G, composition for checking */
	phaset[cnum]=setpos;
	phasgg[cnum][0]=phasgg[setpos][0];
	for(m1=0;m1<cnum;m1++) phasmb[cnum][m1]=phasmb[setpos][m1];
	/**/
	/* CALC, CHECK G EFFECT OF REACTION */
	/* OK reaction => change best */
	if (stdgreac()<0)
		{
		ynokbest=1;
		/* Serch smallest phase */
		phminn=-1;
		phmins=0;
		for (m1=0;m1<cnum;m1++)
			{
			/* Check limits */
			if (sol1[m1]>minsol)
				{
				phval=phascc[m1]/sol1[m1];
				if (phminn== -1 || phval<phmins || (phval==0 && phmins==0 && sol1[phminn]<sol1[m1]))
					{
					phminn=m1;
					phmins=phval;
					}
				}
			}
		/* Check limits */
/*
if (phmins<=minsol) phmins=0;
*/
		/* Calc new phases amount */
		if (phmins>0)
		for (m1=0;m1<cnum;m1++)
			{
			phascc[m1]-=sol1[m1]*phmins;
if (phascc[m1]<0) phascc[m1]=0;
/*
if (phascc[m1]<=minsol) phascc[m1]=0;
*/
			}
		/**/
		/* Replace smalest phase by new */
		if (phaset[phminn]>cnum) phaset[phaset[phminn]]=0;
		/* Note new phase */
		phascc[phminn]=phmins;
		phaset[phminn]=setpos;
		phaset[setpos]=1;
		phasgg[phminn][0]=phasgg[setpos][0];
		phasgg[phminn][1]=phasgg[setpos][1];
		/**/
		/* Reload Compos */
		for (m1=0;m1<cnum;m1++)
		phasmb[phminn][m1]=phasmb[setpos][m1];
		/**/
		/* Reload SS xi[] */
		if (setpos>cnum+mnum)
		for (m1=0;m1<dmnm[setpos-mnum-1-cnum][1];m1++)
		phasxx[phminn][m1]=phasxx[setpos-mnum-1][m1];
		/**/
		/* Minimum Y/N mark */
		ynokbest=1;
		}
	/* End CHECK G EFFECT OF REACTION */
	}
/* End CUR MIN G ASSEMBLAGE SERCH */
/**/
return ynokbest;
}
/* End  MIN G ASSEMBLAGE SELECTION */


/* CHANGE OF BASE OPERATION */
void chbase()
{
/* Counter */
int m1;
/* Positions of counter in phaset[] */
int setpos=0;
/**/
/**/
/* G RECALC CYCLE FOR FREE PHASES */
for (setpos=cnum+1;setpos<cnum+1+mnum;setpos++)
/* Use only free Phases */
if (phaset[setpos]==0)
	{
	/* Save Cur Phase num, G, composition for checking */
	phaset[cnum]=setpos;
	phasgg[cnum][0]=phasgg[setpos][0];
	for(m1=0;m1<cnum;m1++) phasmb[cnum][m1]=phasmb[setpos][m1];
	/**/
	/* CALC, SAVE G EFFECT OF REACTION */

	phasgg[setpos][1]=stdgreac();
	}
/* End G RECALC CYCLE FOR FREE PHASES */
/**/
/* G CLEAR CYCLE FOR BEST PHASES */
for (setpos=0;setpos<cnum;setpos++)
	{
	phasgg[setpos][1]=0;
	phasgg[phaset[setpos]][1]=0;
	}
/* End G CLEAR CYCLE FOR BEST PHASES */
}
/* End CHANGE OF BASE OPERATION */



/* CALCULATION OF dX */
double dxcalc(int minpos)
/* minpos - pos in dmnm[] */
{
/* Counters */
int m1,min,xord;
double ival,dx=10.0;
/**/
/* dX Calc */
dx=10.0;
for (m1=0;m1<dmnm[minpos][1];m1++)
	{
	/* Num of cur minal */
	min=dmnm[minpos][2+m1];
	xord=dmin[min][1];
	/* Check minal */
	if (xi[xord]>lmin[min][0] && xi[xord]<lmin[min][1])
		{
		/* dX calc */
		ival=(lmin[min][1]-lmin[min][0])/2.0;
		ival=(ival-ABSV(ival-(xi[xord]-lmin[min][0])))*1e-3;
		dx=MINV(dx,ival);
		/**/
		}
	}
/**/
return dx;
}
/* End CALCULATION OF dX */


/* Gi CALCULATION  SAVE to gi[] */
double gicalc(int minpos)
/* minpos - pos in dmnm[] */
{
/* Counters */
int m1,m2,min,xord,min1,xord1;
double s1,dx,ggi,gss,dgdx,xdep,xind;
/**/
/* dX Calc */
dx=dxcalc(minpos);
/**/
/* Gi cycle */
gss=gsscalc(minpos,1);
for (m2=0;m2<dmnm[minpos][1];m2++)
	{
	/* Num of cur minal */
	min1=dmnm[minpos][2+m2];
	xord1=dmin[min1][1];
	/* Check minal */
	if (xi[xord1]>lmin[min1][0] && xi[xord1]<lmin[min1][1])
		{
		/* Restore G integr ss */
		ggi=gss;
		/* Save dependent xi[] */
		xdep=xi[xord1];
		for (m1=0;m1<dmnm[minpos][1];m1++)
			{
			/* Num of cur minal */
			min=dmnm[minpos][2+m1];
			xord=dmin[min][1];
			/* Clear dG/dX */
			dgdx=0;
			/* Check minal */
			if (xi[xord]>lmin[min][0] && xi[xord]<lmin[min][1] && xord!=xord1)
				{
				/* Independ xi[] Save */
				xind=xi[xord];
				for (s1=-1.0;s1<2.0;s1+=2.0)
					{
					/* Change Indep xi[], Dep xi[] */
					xi[xord]=xind+dx*s1; 
					xi[xord1]=xdep-dx*s1;
					/* Calc G of SS */
					dgdx+=gsscalc(minpos,1)*s1;
					/* Restore Indep xi[], Dep xi[] */
					xi[xord]=xind; 
					xi[xord1]=xdep;
					}
				/* Recalc dG/dX */
				dgdx/=2.0*dx;
				/**/
				/* Add G-Gi */
				ggi-=dgdx*xind;
				}
			}
		/* Save gi[] */
		gi[xord1]=ggi;
		}
	}
/* End Gi cycle */
/**/
return gss;
}
/* End Gi CALCULATION */



/* EXCHANGE xi[] - xi1[] - xi2[] */
void xisave(int minpos, double *fromxi, double *toxi)
/* minpos - pos in dmnm[] */
/* *fromx1, *toxi - adres of source & target arrays */
{
/* Counters */
int m1,min,xord;
double *fxi, *txi;
/**/
/* Reload operation */
for (m1=0;m1<dmnm[minpos][1];m1++)
	{
	/* Num of cur minal */
	min=dmnm[minpos][2+m1];
	xord=dmin[min][1];
	fxi=fromxi+xord;
	txi=toxi+xord;
	*txi=*fxi;
	}
}
/* End EXCHANGE xi[] - xi1[] - xi2[] */



/* MIN G COMPOSITION OF CURRENT SS SERCH */
void mingserch(int minpos, int ynsave)
/* minpos - pos in dmnm[] */
/* ynsave - Save min SS to kmnm[] Y/N */
{
/* Counters */
int m1,m2,min,xord,badres,setpos,xxpos,cycnum1;
int mdl=dmod[dmnm[minpos][0]][0];
double ival,varkoef,gssmin,gssmin0,gparterr,gparterr0,gpartnum,rt=1.987*tkcur;
/**/
/* Load xi[] */
xiload(minpos,&kmnm[minpos][0]);
/* Load wi[] */
wiload(dmnm[minpos][0]);
/**/
/* Save gio[] of minals */
for (m1=0;m1<dmnm[minpos][1];m1++)
	{
	/* Num of cur minal */
	min=dmnm[minpos][2+m1];
	xord=dmin[min][1];
	gio[xord]=phasgg[cnum+1+min][1];
	}
/**/
/**/
/* gi[], gi0[]  Calc */
gssmin=models(mdl,1);
gpartnum=0;
gparterr=0;
for (m1=1;m1<=dmnm[minpos][1];m1++)
	{
	/* Recalc gi[] */
	gi[m1]=gio[m1]+gii[m1]+gie[m1];
	/* Add gi err */
	gi0[m1]=gi[m1]-gssmin;
	gparterr+=gi0[m1]*gi0[m1];
	gpartnum+=1.0;
	}
/* Calc gi err */
gparterr=pow(gparterr/gpartnum,0.5);
/**/
/* Number of minimiz cycles */
cycnum1=0;
do
	{
	/* Save gi err */
	gparterr0=gparterr;
	gssmin0=gssmin;
	/**/
	/* Save xi[] */
	for (m1=1;m1<=dmnm[minpos][1];m1++)
		{
		xi1[m1]=xi[m1];
		}
	/**/
	/* New Xi Calc Save to xi2[] */
	models(mdl,2);
	/**/
	/* Check, recalc new xi[] */
	/* Varkoef init val */
	varkoef=2.0;
	/* G decrease */
	do
		{
		/* OK solution */
		do
			{
			/* Calc xi[] */
			varkoef/=2.0;
			/* Recalc minal Cycle */
			for (m1=1;m1<=dmnm[minpos][1];m1++)
				{
				ival=varkoef*(xi2[m1]-xi1[m1]);
				xi[m1]=xi1[m1]+ival;
				}
			/**/
			/* G SS Calc, Check */
			/* gi[], gi0[]  Calc */
			gssmin=models(mdl,1);
			}
		while(gii[1]==0);
		gpartnum=0;
		gparterr=0;
		for (m1=1;m1<=dmnm[minpos][1];m1++)
			{
			/* Recalc gi[] */
			gi[m1]=gio[m1]+gii[m1]+gie[m1];
			/* Add gi err */
			gi0[m1]=gi[m1]-gssmin;
			gparterr+=gi0[m1]*gi0[m1];
			gpartnum+=1.0;
			}
		/* Calc gi err */
		gparterr=pow(gparterr/gpartnum,0.5);
if(printmod>3)
	{
printf("\n%d %d    %d %s    %d    %f %f\n",krug,nraz,minpos,&mdnm[dmnm[minpos][0]][0],cycnum1,tkcur-273.0,pbcur*1e-3);
printf("           %e %e %e %e %e\n",gi[1],gi[2],gi[3],gi[4],gi[5]);
printf("           %e %e %e %e %e\n",gii[1],gii[2],gii[3],gii[4],gii[5]);
printf("           %e %e %e %e %e\n",gio[1],gio[2],gio[3],gio[4],gio[5]);
printf("           %e %e %e %e %e\n",gie[1],gie[2],gie[3],gie[4],gie[5]);
printf("           %e %e %e %e %e\n",gi0[1],gi0[2],gi0[3],gi0[4],gi0[5]);
printf("%e %e %e %e %e\n",xi[1],xi[2],xi[3],xi[4],xi[5]);
printf("%e %e %e %e %e\n",xi1[1],xi1[2],xi1[3],xi1[4],xi1[5]);
printf("%e %e %e %e %e\n",xi2[1],xi2[2],xi2[3],xi2[4],xi2[5]);
printf("MINGSS!! %e %e %e %e %e",varkoef,gssmin0,gssmin,gparterr0,gparterr);
getchar();
	}
/*
*/
		}
	while(gssmin>gssmin0 && ABSV(gssmin-gssmin0)>gemin);
/*
	while(gparterr>gparterr0 && (ABSV(gparterr-gparterr0)>gemin || varkoef>gemin));
	while(gparterr>gparterr0 && gssmin>gssmin0 && (ABSV(gparterr-gparterr0)>gemin || varkoef>gemin));
while(cycnum1<100 && gparterr>gemin && (ABSV(gparterr-gparterr0)>gemin || varkoef>gemin || cycnum1<50));
*/
	cycnum1++;
	}
while(cycnum1<100 && ABSV(gssmin-gssmin0)>gemin);
if(printmod>2)
	{
printf("\n%d %d    %d %s    %d    %f %f\n",krug,nraz,minpos,&mdnm[dmnm[minpos][0]][0],cycnum1,tkcur-273.0,pbcur*1e-3);
printf("           %e %e %e %e %e\n",gi[1],gi[2],gi[3],gi[4],gi[5]);
printf("           %e %e %e %e %e\n",gii[1],gii[2],gii[3],gii[4],gii[5]);
printf("           %e %e %e %e %e\n",gio[1],gio[2],gio[3],gio[4],gio[5]);
printf("           %e %e %e %e %e\n",gie[1],gie[2],gie[3],gie[4],gie[5]);
printf("           %e %e %e %e %e\n",gi0[1],gi0[2],gi0[3],gi0[4],gi0[5]);
printf("%e %e %e %e %e\n",xi[1],xi[2],xi[3],xi[4],xi[5]);
printf("%e %e %e %e %e\n",xi1[1],xi1[2],xi1[3],xi1[4],xi1[5]);
printf("%e %e %e %e %e\n",xi2[1],xi2[2],xi2[3],xi2[4],xi2[5]);
printf("MINGSS!! %e %e %e %e %e",varkoef,gssmin0,gssmin,gparterr0,gparterr);
getchar();
	}
/*
if(nraz && ABSV(gssmin-gssmin0)>gemin)
if(nraz && gparterr>gemin)
	nraz=0;
if(gparterr>gemin*1e-1 && nraz && ynsave==1)
*/
/**/
/* Compositions of new SS Clear */
for(m1=0;m1<cnum;m1++)
phasmb[cnum+1+mnum+minpos][m1]=0;
/**/
/* Save xi[] */
xxpos=minpos+cnum;
setpos=minpos+cnum+1+mnum;
for (m1=0;m1<dmnm[minpos][1];m1++)
	{
	/* Num of cur minal */
	min=dmnm[minpos][2+m1];
	xord=dmin[min][1];
	phasxx[xxpos][m1]=xi[xord];
	/* Save SS min to kmnm[] */
	if (ynsave==1) kmnm[minpos][m1]=xi[xord];
	/**/
	/* Compositions of new SS Calc */
	for(m2=0;m2<cnum;m2++)
		{
		phasmb[setpos][m2]+=phasxx[xxpos][m1]*phasmb[min+cnum+1][m2];
		}
	}
/* Save Gss min */
phasgg[setpos][1]=gssmin;
phasgg[setpos][0]=gsscalc(minpos,0);
/* Clear wi[] */
wiclear(dmnm[minpos][0]);
/*
printf("gssmin=%e minpos=%d oknum=%d cycnum1=%d gssmin0=%e\n",gssmin,minpos,oknum,cycnum1,gssmin0);
getchar();
*/
}
/* End MIN G COMPOSITION OF CURRENT SS SERCH */



/* RESULT PRINT */
void resprn(double tk0cur, double pb0cur, double tk1cur, double pb1cur)
/* krug -number of minimiz cycles */
/* nraz - number of last OK minim cycles */
{
/* Counters */
int yn1,yn2,m1,m2,m3,min,mdl,mdl1,mdl2,mdl3,word,xord,p,w;
double ival,ival1,vsys=0,hsys=0,vfld=0,mfld=0,hfld=0;
/* T K for V */
double vtk=tkcur-TR;
/* P bar for V */
double vpb=pbcur-PR;
/* P buffer*/
double pbcur0=pbcur,dp0=0.001*pbcur,s0;
double tkcur0=tkcur,dt0=1.0;
if (dp0==0) dp0=1e-3;
/**/
/* P T */
if (printmod>0) 
	{
	printf("\nkrug=%d nraz=%d\n Equilibrium Pkbar=%6.3f tC=%6.1f Density Pkbar=%6.3f tC=%6.1f\n",krug,nraz,pb0cur/1000.0,tk0cur-273.0,pbcur/1000.0,tkcur-273.0);
	}
else
	{
/*
	printf("%6.3f %6.1f %6.3f %6.1f",pb0cur/1000.0,tk0cur-273.0,pbcur/1000.0,tkcur-273.0);
*/
	}
/**/
/* System Composition */
if (printmod>0) printf("SYSTEM CMP:(");
for(m1=0;m1<cnum;m1++)
	{
	ival=0;
	for(m2=0;m2<cnum;m2++)
	ival+=phasmb[m2][m1]*phascc[m2];
	/* V */
	phasvv[m1]=0;
	/* H */
	phashh[m1]=0;
	if (printmod>0) 
		{
		printf(" %s=%5.3f",&cpnm[m1][0],ival);
		}
	}
if (printmod>0) 
	{
	printf(" )\n");
	}
/**/
/* G of assemblage */
if (printmod>0) 
	{
	printf("SYSTEM G = % 17.15e cal\n",gsyscalc());
	}
/* PHASE description */
for (m1=0;m1<cnum;m1++)
	{
	if (printmod>0) 
		{
		printf("%d ",m1+1);
		}
	/* COMPONENTS */
	if (phaset[m1]<cnum)
		{
		/* Mark phase */
		if(phascc[m1]>minphas) 	
			{
			phavec[phaset[m1]]=1;
			resvec[m1]=phaset[m1];
			}
		/**/
		/* Volume */
		phasvv[m1]=1.0;
		if (printmod>0) 
			{
			printf("%s=%e (%d)  V=1  G0=%e G1=%e\n",&cpnm[phaset[m1]][0],phascc[m1],phavec[resvec[m1]],phasgg[m1][0],phasgg[m1][1]);
			}
		else
			{
			}
		}
	/* MINALS */
	if (phaset[m1]>cnum && phaset[m1]<cnum+1+mnum)
		{
		/* Mark phase */
		if(phascc[m1]>minphas)
			{
			phavec[phaset[m1]]=1;
			resvec[m1]=phaset[m1];
			}
		/**/
		/* Volume V=dG/dP */
		min=phaset[m1]-cnum-1;
		/* Model Num read */
		mdl=dmin[min][0];
		mdl1=dmod[mdl][0];
		/* Load ws[] */
		for (m3=1;m3<=mpos;m3++) if (dmin[min][m3+1] != -1) ws[m3]=solut[0][dmin[min][m3+1]];
	        /* V=dG/dP stand calc */
                pbcur=pbcur0-dp0;
		ival=-models(mdl1,-2);
                pbcur=pbcur0+dp0;
		ival+=models(mdl1,-2);
                pbcur=pbcur0;
		phasvv[m1]=ival/2.0/dp0;
	        /* H=G-dG/dT*T stand calc */
                tkcur=tkcur0-dt0;
		ival1=-models(mdl1,-1);
                tkcur=tkcur0+dt0;
		ival1+=models(mdl1,-1);
                tkcur=tkcur0;
		phashh[m1]=models(mdl1,-1)-tkcur0*ival1/2.0/dt0;
		/* Save fluid V,H */
		if(nmin[min][0]=='H' && nmin[min][1]=='2' && nmin[min][2]=='O')
			{
			vfld=phasvv[m1]*phascc[m1]; 
			hfld=phashh[m1]*phascc[m1]; 
			mfld=phascc[m1]*18.018; 
/*
printf(" \n H2O %e %e \n",vfld,mfld);
*/
			}
		/* Clear ws[] */
		for (m3=1;m3<=mpos;m3++) ws[m3]=0;
		/* Name, concentr */
		if (printmod>0) 
			{
			printf("%s=%e (%d)  V=%e H=%e G0=%e G1=%e\n",&nmin[phaset[m1]-cnum-1][0],phascc[m1],phavec[resvec[m1]],phasvv[m1],phashh[m1],phasgg[m1][0],phasgg[m1][1]);
			}
		else
			{
			}
		/* Composition */
		if (printmod>0) 
			{
			printf("     Compos:(");
			}
		for (m2=0;m2<cnum;m2++)
		if(phasmb[m1][m2]!=0) 
			{
			if (printmod>0) 
				{
				printf(" %s=%5.3e",&cpnm[m2][0],phasmb[m1][m2]);
				}
			}
		if (printmod>0) 
			{
			printf(")\n");
			}
		}
	/* SS */
	if (phaset[m1]>cnum+mnum)
		{
		/* Model Num */
		mdl=dmnm[phaset[m1]-cnum-1-mnum][0];
		mdl1=dmod[mdl][0];
		/**/
		/* Mark phase */
		if(phascc[m1]>minphas) 
			{
			/* Initial SS phase mark */
			if (!modvec[mdl]) 
				{
				phavec[cnum+mnum+mdl*cnum]=1;
				modvec[mdl]=1; 
				resvec[m1]=cnum+mnum+mdl*cnum;
				yn1=1; 
				}
			else
				{
				/* SS PHASE COMPARISON */
				yn1=0; 
				phavec[cnum+mnum+mdl*cnum+modvec[mdl]]=0;
				for (m2=0;m2<m1;m2++)
				if (phaset[m2]>cnum+mnum && phavec[resvec[m2]] && mdl==dmnm[phaset[m2]-cnum-1-mnum][0])
					{
					yn2=0; 
					for (m3=0;m3<cnum;m3++) 
					if(ABSV(phasmb[m2][m3]-phasmb[m1][m3])>mindiff) 
						{
						yn2=1;
						break;
						}
					yn1=yn2;
					if(!yn2) break;
					}
				if(yn1) 
					{
					phavec[cnum+mnum+mdl*cnum+modvec[mdl]]=1;
					resvec[m1]=cnum+mnum+mdl*cnum+modvec[mdl];
					modvec[mdl]++; 
					}
				}
			}
		/**/
		/* Name, concentr */
		if (printmod>0) 
			{
			printf("%s=%e (%d)  G0=%e G1=%e\n",&mdnm[mdl][0],phascc[m1],phavec[resvec[m1]],phasgg[m1][0],phasgg[m1][1]);
			}
		else
			{
			}
		/* Composition */
		if (printmod>0) printf("     Cmp:(");
		for (m2=0;m2<cnum;m2++)
		if(phasmb[m1][m2]!=0) 
			{
			if (printmod>0) 
				{
				printf(" %s=%5.3e",&cpnm[m2][0],phasmb[m1][m2]);
				}
			}
		if (printmod>0) 
			{
			printf(")\n");
			}
		/* Minals */
		if (printmod>0) 
			{
			printf("     Min:(");
			}
		/* Volume Clear */
		phasvv[m1]=0;
		for (m2=0;m2<dmnm[phaset[m1]-cnum-1-mnum][1];m2++)
			{
			/* Volume add */
			min=dmnm[phaset[m1]-cnum-1-mnum][2+m2];
			/* Load ws[] */
			for (m3=1;m3<=mpos;m3++) if (dmin[min][m3+1] != -1) ws[m3]=solut[0][dmin[min][m3+1]];
	        	/* V=dG/dP stand calc */
	                pbcur=pbcur0-dp0;
			ival=-models(mdl1,-2);
                	pbcur=pbcur0+dp0;
			ival+=models(mdl1,-2);
        	        pbcur=pbcur0;
			ival/=2.0*dp0;
			phasvv[m1]+=ival*phasxx[m1][m2];
		        /* H=G-dG/dT*T stand calc */
        	        tkcur=tkcur0-dt0;
			ival1=-models(mdl1,-1);
        	        tkcur=tkcur0+dt0;
			ival1+=models(mdl1,-1);
	                tkcur=tkcur0;
			ival1/=2.0*dt0;
			ival1=models(mdl1,-1)-tkcur0*ival1;
			phashh[m1]+=ival1*phasxx[m1][m2];
			/* Clear ws[] */
			for (m3=1;m3<=mpos;m3++) ws[m3]=0;
			if (printmod>0) 
				{
				printf(" %s=%5.3e",&nmin[dmnm[phaset[m1]-cnum-1-mnum][2+m2]][0],phasxx[m1][m2]);
/*
				printf(" %s=%5.3e[V=%5.3e H=%5.3e]",&nmin[dmnm[phaset[m1]-cnum-1-mnum][2+m2]][0],phasxx[m1][m2],ival,ival1);
*/
				}
			/* Load xi[] */
			xord=dmin[min][1];
			xi[xord]=phasxx[m1][m2];
			}
		/**/
		/* Ve CALC */
		/* Check Not Empty, Non Ideal Model, 0<xi[]<1 */
		if ((p=dmod[mdl][4])>=0)
			{
			w=dmod[mdl][3];
	        	/* V=dG/dP stand calc */
	                pbcur=pbcur0-dp0;
			/* wi[] add */
			for (m2=0;m2<w;m2++)
			/* Check Activ(0), Empty(-1) W in model */
			if (dwww[p+m2][1]>=0)
				{
				/* Load Wv */
				word=dwww[p+m2][0];
				/* Clear WG */
				wi[word]=0;
				if (dwww[p+m2][2] != -1) wi[word]+=solut[0][dwww[p+m2][2]]*1e+3/4.1837;
				if (dwww[p+m2][3] != -1) wi[word]-=solut[0][dwww[p+m2][3]]*tkcur/4.1837;
				if (dwww[p+m2][4] != -1) wi[word]+=solut[0][dwww[p+m2][4]]*pbcur/4.1837;
				}
			ival=-models(mdl1,0);
                	pbcur=pbcur0+dp0;
			/* wi[] add */
			for (m2=0;m2<w;m2++)
			/* Check Activ(0), Empty(-1) W in model */
			if (dwww[p+m2][1]>=0)
				{
				/* Load Wv */
				word=dwww[p+m2][0];
				/* Clear WG */
				wi[word]=0;
				if (dwww[p+m2][2] != -1) wi[word]+=solut[0][dwww[p+m2][2]]*1e+3/4.1837;
				if (dwww[p+m2][3] != -1) wi[word]-=solut[0][dwww[p+m2][3]]*tkcur/4.1837;
				if (dwww[p+m2][4] != -1) wi[word]+=solut[0][dwww[p+m2][4]]*pbcur/4.1837;
				}
			ival+=models(mdl1,0);
        	        pbcur=pbcur0;
			ival/=2.0*dp0;
			phasvv[m1]+=ival;
		        /* H=G-dG/dT*T stand calc */
        	        tkcur=tkcur0-dt0;
			/* wi[] add */
			for (m2=0;m2<w;m2++)
			/* Check Activ(0), Empty(-1) W in model */
			if (dwww[p+m2][1]>=0)
				{
				/* Load Wv */
				word=dwww[p+m2][0];
				/* Clear WG */
				wi[word]=0;
				if (dwww[p+m2][2] != -1) wi[word]+=solut[0][dwww[p+m2][2]]*1e+3/4.1837;
				if (dwww[p+m2][3] != -1) wi[word]-=solut[0][dwww[p+m2][3]]*tkcur/4.1837;
				if (dwww[p+m2][4] != -1) wi[word]+=solut[0][dwww[p+m2][4]]*pbcur/4.1837;
				}
			ival1=-models(mdl1,0);
        	        tkcur=tkcur0+dt0;
			/* wi[] add */
			for (m2=0;m2<w;m2++)
			/* Check Activ(0), Empty(-1) W in model */
			if (dwww[p+m2][1]>=0)
				{
				/* Load Wv */
				word=dwww[p+m2][0];
				/* Clear WG */
				wi[word]=0;
				if (dwww[p+m2][2] != -1) wi[word]+=solut[0][dwww[p+m2][2]]*1e+3/4.1837;
				if (dwww[p+m2][3] != -1) wi[word]-=solut[0][dwww[p+m2][3]]*tkcur/4.1837;
				if (dwww[p+m2][4] != -1) wi[word]+=solut[0][dwww[p+m2][4]]*pbcur/4.1837;
				}
			ival1+=models(mdl1,0);
	                tkcur=tkcur0;
			ival1/=2.0*dt0;
			/* wi[] add */
			for (m2=0;m2<w;m2++)
			/* Check Activ(0), Empty(-1) W in model */
			if (dwww[p+m2][1]>=0)
				{
				/* Load Wv */
				word=dwww[p+m2][0];
				/* Clear WG */
				wi[word]=0;
				if (dwww[p+m2][2] != -1) wi[word]+=solut[0][dwww[p+m2][2]]*1e+3/4.1837;
				if (dwww[p+m2][3] != -1) wi[word]-=solut[0][dwww[p+m2][3]]*tkcur/4.1837;
				if (dwww[p+m2][4] != -1) wi[word]+=solut[0][dwww[p+m2][4]]*pbcur/4.1837;
				}
			ival1=models(mdl1,0)-tkcur0*ival1;
			phashh[m1]+=ival1;
			/**/
			/* Clear Wv */
			wiclear(mdl);
			}
		/* V print */
		if (printmod>0) 
			{
			printf(") Vst=%5.3e Ve=%5.3e Vss=%5.3e Hst=%5.3e He=%5.3e Hss=%5.3e\n",phasvv[m1]-ival,ival,phasvv[m1],phashh[m1]-ival1,ival1,phashh[m1]);
			}
		}
	/* Add System V */
	vsys+=phasvv[m1]*phascc[m1];
	/* Add System H */
	hsys+=phashh[m1]*phascc[m1];
	}
/* Calc, Print System V,H & Densyty */
	rowet=satwt/(vsys*41.837);
        rodry=(satwt-mfld)/((vsys-vfld)*41.837);
        hwet=hsys/satwt;
        hdry=(hsys-hfld)/(satwt-mfld);
if (printmod>0) 
	{
	printf("SYSTEM: H(cal/g)=% 6.3e Hdry(cal/g)=% 6.3e V(cal/bar)=%6.3f V(cm3)=%6.2f Wt(g)=%6.2f D(g/sm3)=%6.4f Ddry(g/sm3)=%6.4f\n \n \n",hsys/satwt,(hsys-hfld)/(satwt-mfld),vsys,vsys*41.837,satwt,satwt/(vsys*41.837),(satwt-mfld)/((vsys-vfld)*41.837));
	}
else
	{
/*
	printf(" %8.6f %8.6f % 8.6e % 8.6e\n",satwt/(vsys*41.837),(satwt-mfld)/((vsys-vfld)*41.837),hsys/satwt,(hsys-hfld)/(satwt-mfld));
*/
	}
if (printmod==2) 
	{
	getchar();
	}
}
/* End Result Print */

