#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <string.h>


/* --------------------- UNCLUDE PARTS --------------------- */
#include"headvas.cxx"
#include"lohpvas.cxx"
#include"gausvas.cxx"
#include"mdhpvas.cxx"
#include"procvas.cxx"
/* --------------------- UNCLUDE PARTS --------------------- */


/* Calculation of P-T point */
void ptpoint (double *tk0c, double *pb0c, double *hrores, int *phvect)
/* tk0c, pb0c - P-T for assemblage calculation */
{
/* tk0cur, pb0cur - P-T for assemblage calculation */
/* tk1cur, pb1cur - P-T for density, enthalpy calculation */
double tk0cur=*tk0c, pb0cur=*pb0c;
double tk1cur, pb1cur;
/* Counters */
int ynokbest,minpos,ynsave,m1,m2,m3,vbeg,vend,mdl;
/**/
/**/
/**/
/* Current P-T for equilibrium calculation */
tkcur=tk0cur;
pbcur=pb0cur;
if (tk0cur<573.0) tk0cur=573.0;
if (tk0cur>1873.0) tk0cur=1873.0;
if (pb0cur<1000.0) pb0cur=1000.0;
if (pb0cur>80000.0) pb0cur=80000.0;
tk1cur=tkcur;
pb1cur=pbcur;
/**/
/**/
/**/
/* Load Configuration */
if(!ynload) {loader(); ynload=1;}
/**/
/* Process One point */
/* Current P-T for equilibrium calculation */
tkcur=tk0cur;
pbcur=pb0cur;
/**/
/* CALCULATION OF G FOR PHASES & SS */
/*
printf("A %d %e %e",mnum,tkcur,pbcur);getchar();
*/
curgcalc();
/* GENERAL MIN G ASSEMBLAGE SERCH */
/* cnum - comp num in the system */
/* setpos - pos of counter in phaset[] (in list of phases) */
krug=0;
nraz=0;
/**/
/**/
/**/
/* Restore fictive mineral composition */
for(m1=0;m1<cnum;m1++)
	{
	if (phaset[m1]>cnum) phaset[phaset[m1]]=0;
	phascc[m1]=cpcc[m1];
	phaset[m1]=m1;
	phasgg[m1][0]=ginit;
	phasgg[m1][1]=ginit;
	/* Reload Compos */
	for (m2=0;m2<cnum;m2++)
	phasmb[m1][m2]=0;
	phasmb[m1][m1]=1.0;
	}
/**/
/**/
/**/
/* GENERAL Minimisation cycle */
do
	{
	/* CUR MIN G ASSEMBLAGE SERCH */
	ynokbest=curming();
	/**/
	/* CALC G of assemblage */
	gsys=gsyscalc();
	if (krug>5  && ABSV(gsys-gsys0)<gemin)
		nraz++;
	else
		nraz=0;
	/**/
	/* Save G of assemblage */
	gsys0=gsys;
	/* Exite Cycle */
	if (nraz>4  || krug>50) break;
	/**/
	/* CHANGE OF BASE OPERATION */
	chbase();
	/**/
	/* MIN SS SERCH */
	if(krug>2)
		{
		ynsave=1;
		for (minpos=0;minpos<inum;minpos++)
			{
			ynsave=1-ynsave;
			if (krug<1) ynsave=0;
			mingserch(minpos,ynsave);
			}
		}
	/**/
	krug++;
	}
while(nraz<5  && krug<50);
/**/
/**/
/**/
/* ASSEMBLAGE Print, Density Calc */
tkcur=tk1cur;
pbcur=pb1cur;
if (pbcur<1.0) pbcur=1.0;
resprn(tk0cur,pb0cur,tk1cur,pb1cur);
tkcur=tk1cur;
pbcur=pb1cur;
/* 
printf(" % 8.6f % 8.6f % 8.6e % 8.6e",rowet,rodry,hwet,hdry); getchar();
*/
*hrores=rowet;
*(hrores+1)=rodry;
*(hrores+2)=hwet;
*(hrores+3)=hdry;
/**/
/**/
/**/
/* PHASE vector reading */
vbeg=10000;
vend=-10000;
/* Check limits for reading */
for (m1=0;m1<cnum;m1++)
if(phavec[resvec[m1]]==1)
	{
	/* SS phases Clear */
	if (phaset[m1]>cnum+mnum) modvec[dmnm[phaset[m1]-cnum-1-mnum][0]]=0;
	/* Min-Max Set */
	if(resvec[m1]>vend) vend=resvec[m1];
	if(resvec[m1]<vbeg) vbeg=resvec[m1];
	/* Numbering clear */
	resvec[m1]=0;
	}
/* Reading Vector */
m2=0;
for (m1=vbeg;m1<=vend;m1++)
if(phavec[m1]==1)
	{
	/* Add vector */
	*(phvect+m2)=m1+1;
	phavec[m1]=0;
	m2++;
	}
/* Adding empty positions to the Vector */
for (m1=m2;m1<cnum;m1++) *(phvect+m1)=0;
}
/* End Calculation of P-T point */
