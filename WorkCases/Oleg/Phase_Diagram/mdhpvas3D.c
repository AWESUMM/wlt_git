/* Standard G at given P# and T# calc after H&P 1998 */
double gstand(int yn)
/* yn - Mode of calculation: */
/* yn=0 - G */
/* yn=1 - G-(Ho-TSo) calc */
{
/* Interm val */
double ho,so,vo,a,b,c,d,ao,k,tcro,sm,vm;
double hcp,scp,v1t,kt,gv,gpt;
double tcr,q,q298,h298,s298,vlamt,gvlam,gland;
/* T */
double T=tkcur;
/* P */
double P=pbcur;
/* Reload curent thermodynamic data */
ho=ws[1]*1e+3;
so=ws[2];
vo=ws[3];
a=ws[4]*1e+3;
b=ws[5]*1e-5*1e+3;
c=ws[6]*1e+3;
d=ws[7]*1e+3;
ao=ws[8]*1e-5;
k=ws[9]*1e+3;
tcro=ws[10];
sm=ws[11];
vm=ws[12];
/**/
/* Cp Contribution to H */
hcp=a*(T-298.0)+b/2.0*(T*T-298.0*298.0)-c*(1.0/T-1.0/298.0)+2.0*d*(pow(T,0.5)-pow(298.0,0.5));
/*
printf("HCP %e %e %e %e %e %e %e",P,T,a,b,c,d,hcp);getchar();
*/
/**/
/* Cp Contribution to S */
scp=a*log(T/298.0)+b*(T-298.0)-c/2.0*(1.0/T/T-1.0/298.0/298.0)-2*d*(1.0/pow(T,0.5)-1.0/pow(298.0,0.5));
/*
printf("SCP %e",scp);getchar();
*/
/**/
/* V Contribution to G */
v1t=vo*(1.0+ao*(T-298.0)-20.0*ao*(pow(T,0.5)-pow(298.0,0.5)));
kt=k*(1.0-1.5e-4*(T-298.0));
gv=v1t*kt/3.0*(pow((1.0+4.0*P/kt),0.75)-1.0);
/*
printf("GV %e",gv);getchar();
*/
/**/
/* Standart G-(Ho-TSo) calc */
gpt=hcp-T*scp+gv;
/* Standart G calc */
if (yn==0) gpt+=ho-T*so;
/**/
/* Lambda Transition */
/*
printf("%d %e %e %e",yn,tkcur,pbcur,gpt);getchar();
*/
if (tcro==0) return gpt/4.1837;
/**/
/* T crit at given P */
tcr=tcro+vm/sm*P;
/**/
/* Order parameter Q */
if (T<tcr) q=pow((1.0-T/tcr),0.25); else q=0;
q298=pow((1.0-298.0/tcro),0.25);
/**/
/* Lambda transition contribution to H */
h298=sm*tcro*(q298*q298-1.0/3.0*pow(q298,6.0));
/**/
/* Lambda transition contribution to S */
s298=sm*q298*q298;
/**/
/* Lambda transition contribution to gv */
vlamt=vm*q298*q298*(1.0+ao*(T-298.0)-20.0*ao*(pow(T,0.5)-pow(298.0,0.5)));
gvlam=vlamt*kt/3.0*(pow((1.0+4.0*P/kt),0.75)-1.0);
/**/
/* Lambda transition contribution to Gcp */
gland=sm*((T-tcr)*q*q+1.0/3.0*tcr*pow(q,6.0));
/**/
/* Add standard G by lambda */
gpt+=h298-T*s298+gvlam+gland;
/*
printf("LAM %d %e %e %e",yn,tkcur,pbcur,gpt);getchar();
*/
return gpt/4.1837;
}
/* Standard G at given P# and T# calc after H&P 1998 */



/* PVT, G, H2O equation Holland and Powell 1991, 1998 */
double gh2o(int yn)
/* yn - Mode of calculation: */
/* yn=0 - G */
/* yn=1 - G-(Ho-TSo) calc */
{
/* Interm val */
double aa,bb,a0,a1,a2,a3,a4,a5,a6,a7,a8,a9,c0,c1,d0,d1,pvir0,avir,bvir,cvir;
double ho,so,a,b,c,d;
double psat,vcmin,vcmax,vc,pkbmin,pkbmax,pkb,pkbcur;
double vmrk,pvir,rt,z,ag,bg,lng,gv,hcp,scp,gpt;
int cyc;
/* T */
double T=tkcur;
/* P */
double PRE=pbcur*1e-3;
/**/
/* Reload PVT equation parameters */
bb=   1.465;
a0=  1113.4;
a1= -0.88517;
a2=  4.5300e-3;
a3= -1.3183e-5;
a4= -0.22291;
a5= -3.8022e-4;
a6=  1.7791e-7;
a7=  5.8487;
a8= -2.1370e-2;
a9=  6.8133e-5;
c0= -3.025650e-2;
c1= -5.343144e-06;
d0= -3.2297554e-03;
d1=  2.2215221e-6;
pvir0=  2.0;
avir=1.9853e-3;
bvir=-8.9090e-2;
cvir=8.0331e-2;
/**/
/* P saturation T<695 K */
psat=-13.627e-3+7.29395e-7*T*T-2.34622e-9*T*T*T+4.83607e-15*pow(T,5.0);
/**/
/* a parameter for MRK calc */
if (T>=673.0) 
	{
	/* Fluid T>673K */
	aa=a0+a4*(T-673.0)+a5*pow((T-673.0),2.0)+a6*pow((T-673.0),3.0);
	/* V kJ/kbar max */
	vcmax=(1.987*T/PRE/1000.0+100.0)*4.1837;
	}
else
	{
	if (PRE<psat)
		{
		/* Gas T<673K */
		aa=a0+a7*(673.0-T)+a8*pow((673.0-T),2.0)+a9*pow((673.0-T),3.0);
		/* V kJ/kbar max */
		vcmax=(1.987*T/PRE/1000.0+100.0)*4.1837;
		}
	else
		{
		/* Liquid T<673K */
		aa=a0+a1*(673.0-T)+a2*pow((673.0-T),2.0)+a3*pow((673.0-T),3.0);
		/* V kJ/kbar max */
		vcmax=4.0;
		}
	}
/**/
/* P kbar iteration, V MRK calc */
/* V kJ/kbar min */
vcmin=bb+0.0001;
/* Check braekets */
vc=vcmin;
pkb=8.313e-3*T/(vc-bb)-aa/vc/(vc+bb)/pow(T,0.5);
pkbmin=pkb-PRE;
vc=vcmax;
pkb=8.313e-3*T/(vc-bb)-aa/vc/(vc+bb)/pow(T,0.5);
pkbmax=pkb-PRE;
if ((pkbmin<0 && pkbmax<0) || (pkbmin>0 && pkbmax>0))
	{
	printf("Min-Max empty-H2O: TC=% e Pkbar=% e",T-273.0,PRE);
	exit(0);
	}
/* MRK Cycle */
cyc=0;
/*
printf("H2O %e %e %e %e %e %e",aa,bb,pkbmin,pkbmax,vcmin,vcmax);getchar();
*/
do 
	{
	cyc++;
	vc=(vcmin+vcmax)/2.0;
	pkb=8.313e-3*T/(vc-bb)-aa/vc/(vc+bb)/pow(T,0.5);
	pkbcur=pkb-PRE;
	if ((pkbcur<0 && pkbmax<0) || (pkbcur>0 && pkbmax>0) || (pkbcur==0 && pkbmax==0))
		{
		vcmax=vc;
		pkbmax=pkbcur;
		}
	 else 
	 	{
	 	vcmin=vc; 
	 	pkbmin=pkbcur;
	 	}
	 }
while (ABSV(pkbcur/PRE)>1e-8 && cyc<50);
if (cyc>=50) 
	{ 
	printf ("Long MRK Cycle-H2O: T C= %e P kbar= %e",T-273.0,PRE);
	exit(0);
	}
/* Save V MRK */
vmrk=vc;
/**/
/* Virial V kJ/kbar add */
if (PRE>pvir0)
	{
	pvir=PRE-pvir0;
	vc=vc+avir*pvir+bvir*pow(pvir,0.5)+cvir*pow(pvir,0.25);
	}
/**/
/* G, kJ at given PVT calc */
/* V MRK Contribution to G */
rt=8.313e-3*T;
z=PRE*vmrk/rt;
ag=aa/bb/rt/pow(T,0.5);
bg=bb*PRE/rt;
lng=z-1.0-log(z-bg)-ag*log(1.0+bg/z);
gv=rt*(log(PRE*1e+3)+lng);
/*
printf("GVH2O %e %e %e",gv,vmrk,vc);getchar();
*/
/**/
/* V Virial Contribution to G */
if(PRE>pvir0)
	{
	pvir=PRE-pvir0;
	gv=gv+avir*pow(pvir,2.0)/2.0+bvir*pow(pvir,1.5)/1.5+cvir*pow(pvir,1.25)/1.25;
	}
/**/
/* Reload 1 bar thermodynamic data */
ho=ws[1]*1e+3;
so=ws[2];
a=ws[4]*1e+3;
b=ws[5]*1e-5*1e+3;
c=ws[6]*1e+3;
d=ws[7]*1e+3;
/**/
/* Cp Contribution to H */
hcp=a*(T-298.0)+b/2.0*(T*T-298.0*298.0)-c*(1/T-1/298.0)+2.0*d*(pow(T,0.5)-pow(298.0,0.5));
/*
printf("HCPH2O %e %e %e %e %e %e %e",PRE,T,a,b,c,d,hcp);getchar();
*/
/**/
/* Cp Contribution to S */
scp=a*log(T/298.0)+b*(T-298.0)-c/2*(1/T/T-1/298.0/298.0)-2.0*d*(1.0/pow(T,0.5)-1.0/pow(298.0,0.5));
/*
printf("SCPH2O %e",scp);getchar();
*/
/**/
/* Standart G-(Ho-TSo) calc */
gpt=hcp-T*scp+gv*1000.0;
/* Standart G calc */
if (yn==0) gpt+=ho-T*so;
/*
printf("H2O %e %e %e",T-273.0,PRE,gpt);getchar();
*/
return gpt/4.1837;
}
/* PVT, G, H2O equation Holland and Powell 1991, 1998 */



/* X, A OF MINALS CALCULATION MODELS */
double models(int mdl, int min)
/* mdl - model num in set of model */
/* min - cur min num or other reg of calc for cur model */
{
/* Interm val */
int m1;
double ival=0,xb,tl,ts,ah2o;
/* Chl,Hbl,Bt,Mu,Ep,Omph site variables */
double xmgm23,xfem23,xmgm1,xfem1,xalm1,xmgm4,xalm4,xfem4,xsit2,xalt2,xalm3,xfem3,x1,x2,x3,x4,x5,dx,x,y,z,w,y1,w1;
double xvaa,xnaa,xka,xnam4,xcam4,xfem13,xmgm13,xfem2,xmgm2,xalm2,xalt1,xsit1,xfe3m2;
double xnam2a,xnam2b,xcam2a,xcam2b,xalm1a,xalm1b,xfem1a,xfem1b,xmgm1a,xmgm1b,xfe;
double gimin,gimax,z1,z2,z3,z4,z5,k1,k2,k3;
/* RT */
double rt=1.987*tkcur;
/* T */
double vtk=tkcur;
/* P */
double vpb=pbcur;
/* Limits for new xi[] Calc */
gimin=log(1e-12);
gimax=-gimin;
/**/
/* Mole fraction, Activity Calc */
switch (mdl)
	{

	/* MODEL 1 - PURE  pure (not SS phases) (x=1, ge=0) */
	case 1:
	switch (min)
        	{
                /* Mix G calc */
		case 1:
		return 0;
                /* Mix G for V calc */
		case 0:
		return 0;
                /**/
                /**/
                /**/
                /* Stand G calc */
		case -1:
	        return gstand(0);
                /* Stand G for V calc */
		case -2:
	        return gstand(1);
                }
	/* End MODEL 1 - PURE  pure (not SS phases) (x=1, ge=0) */


        /* MODEL 3 - H2O */
	case 3:
        /* Stand Gp calc */
	switch(min)
		{
                /* Mix G calc */
		case 1:
		return 0;
                /* Mix G for V calc */
		case 0:
		return 0;
                /**/
                /**/
                /**/
                /* Stand G calc */
		case -1:
	        ival=gh2o(0);
		/* Correct G for aH2O */
		ah2o=h2ocur;
		ival+=rt*log(ah2o);
	        return ival;
                /* Stand G for V calc */
		case -2:
	        return gh2o(1);
		}
        /* End MODEL 3 - H2O */


        /* MODEL 4 - H2OG IN GRANITIC MELT REGION */
	case 4:
        /* Stand Gp calc */
	switch(min)
		{
                /* Mix G calc */
		case 1:
		return 0;
                /* Mix G for V calc */
		case 0:
		return 0;
                /**/
                /**/
                /**/
                /* Stand G calc */
		case -1:
		ival=gh2o(0);
		/* Granit liquidus T K at given P kbar */
		tl=877.1+160.0/pow((vpb*1e-3+0.348),0.75);
		vtk=tkcur+20.0;
		if(vtk>tl)
			{
			/* Granit solidus T K at given P kbar */
			ts=1262.0+9.0*vpb*1e-3;
			if (vtk<ts)
				{
				/* H2O activity at granit liquidus at P,T given */
				ah2o=1.0-pow((vtk-tl)/(ts-tl)*1.2,0.865);
				if(ah2o>1.0) ah2o=1.0;
				if(ah2o<0.1) ah2o=0.1;
				}
			else
				{
				ah2o=0.1;
				}
			/* Correct G for aH2O */
			ival+=rt*log(ah2o);
/*
printf("aH2O %e %e %e %e %e",vtk,vpb,tl,ts,ah2o);getchar();
*/
			}
	        return ival;
                /* Stand G for V calc */
		case -2:
	        return gh2o(1);
		}
        /* End MODEL 4 - H2O IN GRANITIC MELT REGION */


	/* MODEL 10 - OPX En(1)-Fs(2)-Fm(3)-Mgts(4) */
	case 10:
	switch (min)
        	{
                /* Mix G calc */
		case 1:
		/* Gmix-id */
		/* En(1)-Fs(2)-Fm(3)-Mgts(4) site ideal mixing */
		/* Site variables */
		/* M1 */
		xmgm1=xi[1]+xi[3];
		xalm1=xi[4];
		xfem1=1.0-xmgm1-xalm1;
		/* M2 */
		xfem2=xi[2]+xi[3];
		xmgm2=1.0-xfem2;
		/* Check site variables */
		if(xfem1<=0 || xmgm1<=0 || xalm1<=0 || xfem2<=0 || xmgm2<=0) 
			{
			gii[1]=0;
			return 0;
			}
		/* En(1) */
		gii[1]=rt*log(xmgm2*xmgm1);
		/* Fs(2) */
		gii[2]=rt*log(xfem2*xfem1);
		/* Fm(3) */
		gii[3]=rt*log(xfem2*xmgm1);
		/* Mgts(4) */
		gii[4]=rt*log(xmgm2*xalm1);
                /**/
		/* Calc Integral energy */
		ival=xi[1]*(gio[1]+gii[1])+xi[2]*(gio[2]+gii[2])+xi[3]*(gio[3]+gii[3])+xi[4]*(gio[4]+gii[4]);
		/**/
                /* Mix G for V calc */
		case 0:
		/* Gmix-ex */
		/* Regular mixing */
		/* wi[1] En(1)-Fs(2) */
		/* wi[2] En(1)-Mgts(4) */
		/* wi[3] Fs(2)-Mgts(4) */
		/* wi[4] En(1)-Fm(3) */
		/* wi[5] Fs(2)-Fm(3) */
		/* wi[6] Fm(3)-Mgts(4) */
		/* En(1) */
		gie[1]=(1.0-xi[1])*xi[2]*wi[1]+(1.0-xi[1])*xi[4]*wi[2]-xi[2]*xi[4]*wi[3]+(1.0-xi[1])*xi[3]*wi[4]-xi[2]*xi[3]*wi[5]-xi[3]*xi[4]*wi[6];
		/* Fs(2) */
		gie[2]=(1.0-xi[2])*xi[1]*wi[1]-xi[1]*xi[4]*wi[2]+(1.0-xi[2])*xi[4]*wi[3]-xi[1]*xi[3]*wi[4]+(1.0-xi[2])*xi[3]*wi[5]-xi[3]*xi[4]*wi[6];
		/* Fm(3) */
		gie[3]=-xi[2]*xi[1]*wi[1]-xi[1]*xi[4]*wi[2]-xi[2]*xi[4]*wi[3]+(1.0-xi[3])*xi[1]*wi[4]+(1.0-xi[3])*xi[2]*wi[5]+(1.0-xi[3])*xi[4]*wi[6];
		/* Mgts(4) */
		gie[4]=-xi[2]*xi[1]*wi[1]+(1.0-xi[4])*xi[1]*wi[2]+(1.0-xi[4])*xi[2]*wi[3]-xi[3]*xi[1]*wi[4]-xi[3]*xi[2]*wi[5]+(1.0-xi[4])*xi[3]*wi[6];
                /**/
		/* Add Integral energy */
		ival+=xi[1]*gie[1]+xi[2]*gie[2]+xi[3]*gie[3]+xi[4]*gie[4];
		return ival;
                /**/
		case 2:
		/* Site variables */
		/* M1 */
		xmgm1=xi[1]+xi[3];
		xalm1=xi[4];
		xfem1=1.0-xmgm1-xalm1;
		/* M2 */
		xfem2=xi[2]+xi[3];
		xmgm2=1.0-xfem2;
		/* New site variables calc */
		/* XfeM2 En=Fm */
	        z1=(gio[1]+gie[1]+gii[1])-rt*log(xmgm2);
	        z2=(gio[3]+gie[3]+gii[3])-rt*log(xfem2);
		z3=MINV(z1,z2);
	        z1=(z3-z1)/rt; if(z1<gimin) z1=gimin; z1=exp(z1);
	        z2=(z3-z2)/rt; if(z2<gimin) z2=gimin; z2=exp(z2);
	        xfem2=z2/(z1+z2);
		/* XalM1,XfeM1 Fm=Fs=Fets (Fets=Mgts+Fm-En) */
	        z1=(gio[3]+gie[3]+gii[3])-rt*log(xmgm1);
	        z2=(gio[2]+gie[2]+gii[2])-rt*log(xfem1);
	        z3=(gio[3]+gie[3]+gii[3])+(gio[4]+gie[4]+gii[4])-(gio[1]+gie[1]+gii[1])-rt*log(xalm1);
		z4=MINV(z1,z2); z4=MINV(z4,z3);
	        z1=(z4-z1)/rt; if(z1<gimin) z1=gimin; z1=exp(z1);
	        z2=(z4-z2)/rt; if(z2<gimin) z2=gimin; z2=exp(z2);
	        z3=(z4-z3)/rt; if(z3<gimin) z3=gimin; z3=exp(z3);
	        z4=z1+z2+z3;
	        xalm1=z3/z4;
	        xfem1=z2/z4;
		/* New xi[] calc */
		/* Mgts(4) */
		xi2[4]=xalm1;
		/* Fs(2) */
		xi2[2]=xfem1;
		/* Fm(3) */
		xi2[3]=xfem2-xfem1;
		/* En(1) */
		xi2[1]=1.0-xi2[2]-xi2[3]-xi2[4];
		return 0;
                /**/
                /* Stand G calc */
		case -1:
	        return gstand(0);
                /* Stand G for V calc */
		case -2:
	        return gstand(1);
                }
	/* End MODEL 10 - OPX En(1)-Fs(2)-Fm(3)-Mgts(4) */


	/* MODEL 15 - BT Phl(1)-Ann(2)-East(3)-Obi(4) */
	case 15:
	switch (min)
        	{
                /* Mix G calc */
		case 1:
		/* Gmix-id */
		/* Phl(1)-Ann(2)-East(3)-Obi(4)  site ideal mixing */
		/* Site variables */
		/* M1 */
		xmgm1=xi[1];
		xalm1=xi[3];
		xfem1=1.0-xmgm1-xalm1;
		/* M2 */
		xfem2=xi[2];
		xmgm2=1.0-xfem2;
		/* T1 */
		xalt1=xi[3]+0.5*(xi[1]+xi[2]+xi[4]);
		xsit1=1.0-xalt1;
/*
printf("%e %e %e %e\n",xi[1],xi[2],xi[3],xi[4]);
printf("%e %e %e %e %e %e %e",xfem1,xmgm1,xalm1,xfem2,xmgm2,xalt1,xsit1);getchar(); 
*/
		/* Check site variables */
		if(xfem1<=0 || xmgm1<=0 || xalm1<=0 || xfem2<=0 || xmgm2<=0 || xalt1<=0 || xsit1<=0) 
			{
			gii[1]=0;
			return 0;
			}
		/* Phl(1)*/
		gii[1]=rt*log(4.0*xmgm1*xmgm2*xmgm2*xalt1*xsit1);
		/* Ann(2)*/
		gii[2]=rt*log(4.0*xfem1*xfem2*xfem2*xalt1*xsit1);
		/* East(3)*/
		gii[3]=rt*log(xalm1*xmgm2*xmgm2*xalt1*xalt1);
		/* Obi(4)*/
		gii[4]=rt*log(4.0*xfem1*xmgm2*xmgm2*xalt1*xsit1);
                /**/
		/* Calc Integral energy */
		ival=xi[1]*(gio[1]+gii[1])+xi[2]*(gio[2]+gii[2])+xi[3]*(gio[3]+gii[3])+xi[4]*(gio[4]+gii[4]);
		/**/
                /* Mix G for V calc */
		/**/
		/**/
		/**/
                /* Mix G for V calc */
		case 0:
		/* Regular mixing */
		/* wi[1] Phl(1)-Ann(2) */
		/* wi[2] Phl(1)-East(3) */
		/* wi[3] Ann(2)-East(3) */
		/* wi[4] Ann(2)-Obi(4) */
		/* wi[5] Phl(1)-Obi(4) */
		/* wi[6] East(3)-Obi(4) */
		/* Phl(1) */
		gie[1]=(1.0-xi[1])*xi[2]*wi[1]+(1.0-xi[1])*xi[3]*wi[2]-xi[2]*xi[3]*wi[3]-xi[2]*xi[4]*wi[4]+(1.0-xi[1])*xi[4]*wi[5]-xi[3]*xi[4]*wi[6];
		/* Ann(2) */
		gie[2]=(1.0-xi[2])*xi[1]*wi[1]-xi[1]*xi[3]*wi[2]+(1.0-xi[2])*xi[3]*wi[3]+(1.0-xi[2])*xi[4]*wi[4]-xi[1]*xi[4]*wi[5]-xi[3]*xi[4]*wi[6];
		/* East(3) */
		gie[3]=-xi[2]*xi[1]*wi[1]+(1.0-xi[3])*xi[1]*wi[2]+(1.0-xi[3])*xi[2]*wi[3]-xi[2]*xi[4]*wi[4]-xi[1]*xi[4]*wi[5]+(1.0-xi[3])*xi[4]*wi[6];
		/* Obi(4) */
		gie[4]=-xi[2]*xi[1]*wi[1]-xi[3]*xi[1]*wi[2]-xi[3]*xi[2]*wi[3]+(1.0-xi[4])*xi[2]*wi[4]+(1.0-xi[4])*xi[1]*wi[5]+(1.0-xi[4])*xi[3]*wi[6];
                /**/
		/* Add Integral energy */
		ival+=xi[1]*gie[1]+xi[2]*gie[2]+xi[3]*gie[3]+xi[4]*gie[4];
		return ival;
                /**/
		case 2:
		/* Site variables */
		/* M1 */
		xmgm1=xi[1];
		xalm1=xi[3];
		xfem1=1.0-xmgm1-xalm1;
		/* M2 */
		xfem2=xi[2];
		xmgm2=1.0-xfem2;
		/* New site variables calc */
		/* XfeM2 Phl=Dobi (Dobi=Ann+Phl-Obi) */
	        z1=(gio[1]+gie[1]+gii[1])-rt*2.0*log(xmgm2);
	        z2=(gio[1]+gie[1]+gii[1])+(gio[2]+gie[2]+gii[2])-(gio[4]+gie[4]+gii[4])-rt*2.0*log(xfem2);
		z3=MINV(z1,z2);
	        z1=(z3-z1)/2.0/rt; if(z1<gimin) z1=gimin; z1=exp(z1);
	        z2=(z3-z2)/2.0/rt; if(z2<gimin) z2=gimin; z2=exp(z2);
	        xfem2=z2/(z1+z2);
		/* XalM1,XmgM1 Phl=Obi=East */
	        z1=(gio[1]+gie[1]+gii[1])-rt*log(xmgm1);
	        z2=(gio[4]+gie[4]+gii[4])-rt*log(xfem1);
	        z3=(gio[3]+gie[3]+gii[3])-rt*log(xalm1);
		z4=MINV(z1,z2); z4=MINV(z4,z3);
	        z1=(z4-z1)/rt; if(z1<gimin) z1=gimin; z1=exp(z1);
	        z2=(z4-z2)/rt; if(z2<gimin) z2=gimin; z2=exp(z2);
	        z3=(z4-z3)/rt; if(z3<gimin) z3=gimin; z3=exp(z3);
	        z4=z1+z2+z3;
	        xalm1=z3/z4;
	        xmgm1=z1/z4;
		/* New xi[] calc */
		/* East(3) */
		xi2[3]=xalm1;
		/* Ann(2) */
		xi2[2]=xfem2;
		/* Phl(1) */
		xi2[1]=xmgm1;
		/* Obi(4) */
		xi2[4]=1.0-xi2[1]-xi2[2]-xi2[3];
		return 0;
                /**/
                /* Stand G calc */
		case -1:
	        return gstand(0);
                /* Stand G for V calc */
		case -2:
	        return gstand(1);
                }
	/* End MODEL 15 - BT Phl(1)-Ann(2)-East(3)-Obi(4) */

        
	/* MODEL 20 - GRT  Prp(1)-Alm(2)-Grs(3) */
	case 20:
	switch (min)
        	{
                /* Mix G calc */
		case 1:
		/* Gmix-id */
		/* Prp(1) */
		gii[1]=3.0*rt*log(xi[1]);
		/* Alm(2) */
		gii[2]=3.0*rt*log(xi[2]);
		/* Grs(3) */
		gii[3]=3.0*rt*log(xi[3]);
                /**/
		/* Calc Integral energy */
		ival=xi[1]*(gio[1]+gii[1])+xi[2]*(gio[2]+gii[2])+xi[3]*(gio[3]+gii[3]);
		/**/
                /* Mix G for V calc */
		case 0:
		/* Gmix-ex */
		/* wi[1] Py(1)-Alm(2) */
		/* wi[2] Py(1)-Grs(3) */
		/* Py(1) */
		gie[1]=(1.0-xi[1])*xi[2]*wi[1]+(1.0-xi[1])*xi[3]*wi[2];
		/* Alm(2) */
		gie[2]=(1.0-xi[2])*xi[1]*wi[1]-xi[1]*xi[3]*wi[2];
		/* Grs(3) */
		gie[3]=-xi[1]*xi[2]*wi[1]+(1.0-xi[3])*xi[1]*wi[2];
                /**/
		/* Add Integral energy */
		ival+=xi[1]*gie[1]+xi[2]*gie[2]+xi[3]*gie[3];
		return ival;
                /**/
		case 2:
		/* New xi[] calc */
		/* Prp=Alm=Grs */
	        z1=(gio[1]+gie[1]+gii[1])-rt*3.0*log(xi[1]);
	        z2=(gio[2]+gie[2]+gii[2])-rt*3.0*log(xi[2]);
	        z3=(gio[3]+gie[3]+gii[3])-rt*3.0*log(xi[3]);
		z4=MINV(z1,z2); z4=MINV(z4,z3);
	        z1=(z4-z1)/3.0/rt; if(z1<gimin) z1=gimin; z1=exp(z1);
	        z2=(z4-z2)/3.0/rt; if(z2<gimin) z2=gimin; z2=exp(z2);
	        z3=(z4-z3)/3.0/rt; if(z3<gimin) z3=gimin; z3=exp(z3);
	        z4=z1+z2+z3;
		xi2[1]=z1/z4;
		xi2[2]=z2/z4;
		xi2[3]=1.0-xi2[1]-xi2[2];
		return 0;
                /**/
                /**/
                /* Stand G calc */
		case -1:
	        return gstand(0);
                /* Stand G for V calc */
		case -2:
	        return gstand(1);
                }
	/* End MODEL 20 - GRT  Prp(1)-Alm(2)-Grs(3) */

	/* MODEL 21 - GRT1 Py(1)-Alm(2)-Grs(3)-Spss(4) */
	case 21:
	switch (min)
        	{
                /* Mix G calc */
		case 1:
		/* Gmix-id */
		/* Py(1) */
		gii[1]=3.0*rt*log(xi[1]);
		/* Alm(2) */
		gii[2]=3.0*rt*log(xi[2]);
		/* Grs(3) */
		gii[3]=3.0*rt*log(xi[3]);
                /* Spss(4) */
		gii[4]=3.0*rt*log(xi[4]);
                /**/
		/* Calc Integral energy */
		ival=xi[1]*(gio[1]+gii[1])+xi[2]*(gio[2]+gii[2])+xi[3]*(gio[3]+gii[3])+xi[4]*(gio[4]+gii[4]);
		/**/
                /* Mix G for V calc */
		case 0:
		/* Gmix-ex */
		/* wi[1] Py(1)-Alm(2) */
		/* wi[2] Py(1)-Grs(3) */
		/* wi[3] Py(1)-Spss(4)*/
		/* wi[4] Alm(2)-Grs(3)*/
		/* wi[5] Alm(2)-Spss(4)*/
		/* wi[6] Grs(3)-Spss(4)*/
		/* Py(1) */
		gie[1]=(1.0-xi[1])*xi[2]*wi[1]+(1.0-xi[1])*xi[3]*wi[2]+(1.0-xi[1])*xi[4]*wi[3]-xi[2]*xi[3]*wi[4]-xi[2]*xi[4]*wi[5]-xi[3]*xi[4]*wi[6];
		/* Alm(2) */
		gie[2]=(1.0-xi[2])*xi[1]*wi[1]-xi[1]*xi[3]*wi[2]-xi[1]*xi[4]*wi[3]+(1.0-xi[2])*xi[3]*wi[4]+(1.0-xi[2])*xi[4]*wi[5]-xi[3]*xi[4]*wi[6];
		/* Grs(3) */
		gie[3]=-xi[1]*xi[2]*wi[1]+(1.0-xi[3])*xi[1]*wi[2]-xi[1]*xi[4]*wi[3]+(1.0-xi[3])*xi[2]*wi[4]-xi[2]*xi[4]*wi[5]+(1.0-xi[3])*xi[4]*wi[6];
                /* Spss(4) */
		gie[4]=-xi[1]*xi[2]*wi[1]-xi[1]*xi[3]*wi[2]+(1.0-xi[4])*xi[1]*wi[3]-xi[2]*xi[3]*wi[4]+(1.0-xi[4])*xi[2]*wi[5]+(1.0-xi[4])*xi[3]*wi[6];
                /**/
		/* Add Integral energy */
		ival+=xi[1]*gie[1]+xi[2]*gie[2]+xi[3]*gie[3]+xi[4]*gie[4];
		return ival;
                /**/
		case 2:
		/* New xi[] calc */
		/* Prp=Alm=Grs=Spss */
	        z1=(gio[1]+gie[1]+gii[1])-rt*3.0*log(xi[1]);
	        z2=(gio[2]+gie[2]+gii[2])-rt*3.0*log(xi[2]);
	        z3=(gio[3]+gie[3]+gii[3])-rt*3.0*log(xi[3]);
	        z4=(gio[4]+gie[4]+gii[4])-rt*3.0*log(xi[4]);
		z5=MINV(z1,z2);
                z5=MINV(z5,z3);
                z5=MINV(z5,z4);
	        z1=(z5-z1)/3.0/rt; if(z1<gimin) z1=gimin; z1=exp(z1);
	        z2=(z5-z2)/3.0/rt; if(z2<gimin) z2=gimin; z2=exp(z2);
	        z3=(z5-z3)/3.0/rt; if(z3<gimin) z3=gimin; z3=exp(z3);
	        z4=(z5-z4)/3.0/rt; if(z4<gimin) z4=gimin; z4=exp(z4);
	        z5=z1+z2+z3+z4;
		xi2[1]=z1/z5;
		xi2[2]=z2/z5;
		xi2[3]=z3/z5;
		xi2[4]=1.0-xi2[1]-xi2[2]-xi2[3];
		return 0;
                /**/
                /**/
                /* Stand G calc */
		case -1:
	        return gstand(0);
                /* Stand G for V calc */
		case -2:
	        return gstand(1);
                }
	/* End MODEL 21 - GRT1  Prp(1)-Alm(2)-Grs(3)-Spss(4) */

	/* MODEL 30 - CRD  Crd(1)-Fcrd(2)-Hcrd(3) */
	case 30:
	switch (min)
        	{
                /* Mix G calc */
		case 1:
		/* Gmix id */
		/* Crd(1)-Fcrd(2)-Hcrd(3) site ideal mixing */
		/* Crd(1) */
		gii[1]=rt*log((1.0-xi[2])*(1.0-xi[2])*(1.0-xi[3]));
		/* Fcrd(2) */
		gii[2]=rt*log(xi[2]*xi[2]*(1.0-xi[3]));
		/* Hcrd(3) */
		gii[3]=rt*log((1.0-xi[2])*(1.0-xi[2])*xi[3]);
                /**/
		/* Calc Integral energy */
		ival=xi[1]*(gio[1]+gii[1])+xi[2]*(gio[2]+gii[2])+xi[3]*(gio[3]+gii[3]);
                /**/
                /* Mix G for V calc */
		case 0:
		gie[1]=gie[2]=gie[3]=0;
		return ival;
                /**/
		case 2:
		/* New xi[] calc */
		/* Fcrd(2) Crd=Fcrd */
	        z1=(gio[1]-gio[2])/rt/2.0;
	        if(z1<gimin) z1=gimin;
	        if(z1>gimax) z1=gimax;
	        z1=exp(z1);
		xi2[2]=z1/(1.0+z1);
		/* Hcrd(3) Crd=Hcrd */
	        z1=(gio[1]-gio[3])/rt;
	        if(z1<gimin) z1=gimin;
	        if(z1>gimax) z1=gimax;
	        z1=exp(z1);
		xi2[3]=z1/(1.0+z1);
		/* Crd(1) 1-Fcrd-Hcrd */
		xi2[1]=1.0-xi2[2]-xi2[3];
		return 0;
                /**/
                /* Stand G calc */
		case -1:
	        return gstand(0);
                /* Stand G for V calc */
		case -2:
	        return gstand(1);
                }
	/* End MODEL 30 - CRD  Crd(1)-Fcrd(2)-Hcrd(3) */


	/* MODEL 35 - ST  Mst(1)-Fst(2) */
	case 35:
	switch (min)
        	{
                /* Mix G calc */
		case 1:
		/* Gmix-id */
		/* Mst(1) */
		gii[1]=4.0*rt*log(xi[1]);
		/* Fst(2) */
		gii[2]=4.0*rt*log(xi[2]);
                /**/
		/* Calc Integral energy */
		ival=xi[1]*(gio[1]+gii[1])+xi[2]*(gio[2]+gii[2]);
		/**/
                /* Mix G for V calc */
		case 0:
		gie[1]=gie[2]=0;
		return ival;
                /**/
		case 2:
		/* New xi[] calc */
		/* Fst(2) Mst=Fst */
	        z1=(gio[1]-gio[2])/rt/4.0;
	        if(z1<gimin) z1=gimin;
	        if(z1>gimax) z1=gimax;
	        z1=exp(z1);
		xi2[2]=z1/(1.0+z1);
		/* Mst(1) 1-Fst */
		xi2[1]=1.0-xi2[2];
		return 0;
                /**/
                /* Stand G calc */
		case -1:
	        return gstand(0);
                /* Stand G for V calc */
		case -2:
	        return gstand(1);
                }
	/* End MODEL 35 - ST  Mst(1)-Fst(2) */


	/* MODEL 40 - CHL  Afchl(1)-Clin(2)-Ames(3)-Daph(4) */
	case 40:
	switch (min)
        	{
                /* Mix G calc */
		case 1:
		/* Gmix-id */
		/* Site variables */
		/* M2,M3 Mg,Fe */
		xfem23=xi[4]/(1.0+0.2*xi[1]-0.2*xi[3]);
		xmgm23=1-xfem23;
		/* M1 Mg,Fe,Al*/
		xalm1=xi[3];
		xmgm1=xmgm23*(1.0-xalm1);
		xfem1=1.0-xalm1-xmgm1;
		/* M4 Mg,Fe,Al */
		xalm4=1.0-xi[1];
		xmgm4=xi[1]*xmgm23;
		/* T2 */
		xsit2=xi[1]+0.5*(xi[2]+xi[4]);
		xalt2=1.0-xsit2;
		/* Check site variables */
		if(xfem23<=0 || xmgm23<=0 || xmgm1<=0 || xfem1<=0 || xalm1<=0 || xmgm4<=0 || xalm4<=0 || xalt2<=0 || xsit2<=0) 
			{
			gii[1]=0;
			return 0;
			}
		/* Afchl(1)-Clin(2)-Ames(3)-Daph(4) site ideal mixing */
		/* Site variables */
		/* Afchl(1) */
		gii[1]=rt*log(pow(xmgm23,4.0)*xmgm1*xmgm4*xsit2*xsit2);
		/* Clin(2) */
		gii[2]=rt*log(4.0*pow(xmgm23,4.0)*xmgm1*xalm4*xsit2*xalt2);
		/* Ames(3) */
		gii[3]=rt*log(pow(xmgm23,4.0)*xalm1*xalm4*xalt2*xalt2);
		/* Daph(4) */
		gii[4]=rt*log(4.0*pow(xfem23,4.0)*xfem1*xalm4*xsit2*xalt2);
                /**/
		/* Calc Integral energy */
		ival=xi[1]*(gio[1]+gii[1])+xi[2]*(gio[2]+gii[2])+xi[3]*(gio[3]+gii[3])+xi[4]*(gio[4]+gii[4]);
		/**/
                /* Mix G for V calc */
		/**/
		/**/
		/**/
                /* Mix G for V calc */
		case 0:
		/* Regular mixing */
		/* wi[1] Afchl(1)-Clin(2) */
		/* wi[2] Afchl(1)-Ames(3) */
		/* wi[3] Clin(2)-Ames(3) */
		/* wi[4] Clin(2)-Daph(4) */
		/* wi[5] Afchl(1)-Daph(4) */
		/* wi[6] Ames(3)-Daph(4) */
		/* Afchl(1) */
		gie[1]=(1.0-xi[1])*xi[2]*wi[1]+(1.0-xi[1])*xi[3]*wi[2]-xi[2]*xi[3]*wi[3]-xi[2]*xi[4]*wi[4]+(1.0-xi[1])*xi[4]*wi[5]-xi[3]*xi[4]*wi[6];
		/* Clin(2) */
		gie[2]=(1.0-xi[2])*xi[1]*wi[1]-xi[1]*xi[3]*wi[2]+(1.0-xi[2])*xi[3]*wi[3]+(1.0-xi[2])*xi[4]*wi[4]-xi[1]*xi[4]*wi[5]-xi[3]*xi[4]*wi[6];
		/* Ames(3) */
		gie[3]=-xi[2]*xi[1]*wi[1]+(1.0-xi[3])*xi[1]*wi[2]+(1.0-xi[3])*xi[2]*wi[3]-xi[2]*xi[4]*wi[4]-xi[1]*xi[4]*wi[5]+(1.0-xi[3])*xi[4]*wi[6];
		/* Daph(4) */
		gie[4]=-xi[2]*xi[1]*wi[1]-xi[3]*xi[1]*wi[2]-xi[3]*xi[2]*wi[3]+(1.0-xi[4])*xi[2]*wi[4]+(1.0-xi[4])*xi[1]*wi[5]+(1.0-xi[4])*xi[3]*wi[6];
                /**/
		/* Add Integral energy */
		ival+=xi[1]*gie[1]+xi[2]*gie[2]+xi[3]*gie[3]+xi[4]*gie[4];
		return ival;
                /**/
		case 2:
		/* Save mol fractions */
		xi2[1]=xi[1];
		xi2[2]=xi[2];
		xi2[3]=xi[3];
		xi2[4]=xi[4];
		/* Compositional variables x=Fe/(Fe+Mg), y=XalM1, z=1-XalM4 */
		x=xi[4]/(1.0+0.2*xi[1]-0.2*xi[3]);
		y=xi[3];
		z=xi[1];
		/* Integral Gibbs Energy calc */
		ival=models(40,1);
		/* New site variables calc */
		/* x  ----------------------------------*/
		/* Increment Calc */
		dx=(0.5-ABSV(0.5-x))*1e-3;
		/* Differentiation */
		x1=x-dx;
		xi[4]=x1*(1.0+0.2*z-0.2*y);
		xi[2]=1.0-xi[1]-xi[3]-xi[4];
		k1=-models(40,1);
		x1=x+dx;
		xi[4]=x1*(1.0+0.2*z-0.2*y);
		xi[2]=1.0-xi[1]-xi[3]-xi[4];
		k1+=models(40,1);
		/* Restore variables */
		xi[4]=xi2[4];
		xi[2]=xi2[2];
		/* Partial Energy of x and (1-x) calc */
		k1=ival+(1.0-x)*k1/2.0/dx;
		k2=(ival-x*k1)/(1.0-x);
	        k1=k1-rt*(5.0+z-y)*log(x);
	        k2=k2-rt*(5.0+z-y)*log(1.0-x);
		k3=MINV(k1,k2);
	        k1=(k3-k1)/(5.0+z-y)/rt; if(k1<gimin) k1=gimin; k1=exp(k1);
	        k2=(k3-k2)/(5.0+z-y)/rt; if(k2<gimin) k2=gimin; k2=exp(k2);
		x1=k1/(k1+k2);
		/* y ----------------------------------*/
		/* Increment Calc */
		dx=(0.5-ABSV(0.5-y))*1e-3;
		/* Differentiation */
		y1=y-dx;
		xi[3]=y1;
		xi[4]=x*(1.0+0.2*z-0.2*y1);
		xi[2]=1.0-xi[1]-xi[3]-xi[4];
		k1=-models(40,1);
		y1=y+dx;
		xi[3]=y1;
		xi[4]=x*(1.0+0.2*z-0.2*y1);
		xi[2]=1.0-xi[1]-xi[3]-xi[4];
		k1+=models(40,1);
		/* Restore variables */
		xi[3]=xi2[3];
		xi[4]=xi2[4];
		xi[2]=xi2[2];
		/* Partial Energy of y and (1-y) calc */
		k1=ival+(1.0-y)*k1/2.0/dx;
		k2=(ival-y*k1)/(1.0-y);
	        k1=k1-rt*log(y);
	        k2=k2-rt*log(1.0-y);
		k3=MINV(k1,k2);
	        k1=(k3-k1)/rt; if(k1<gimin) k1=gimin; k1=exp(k1);
	        k2=(k3-k2)/rt; if(k2<gimin) k2=gimin; k2=exp(k2);
		y1=k1/(k1+k2);
		/* z ----------------------------------*/
		/* Increment Calc */
		dx=(0.5-ABSV(0.5-z))*1e-3;
		/* Differentiation */
		z1=z-dx;
		xi[1]=z1;
		xi[4]=x*(1.0+0.2*z1-0.2*y);
		xi[2]=1.0-xi[1]-xi[3]-xi[4];
		k1=-models(40,1);
		z1=z+dx;
		xi[1]=z1;
		xi[4]=x*(1.0+0.2*z1-0.2*y);
		xi[2]=1.0-xi[1]-xi[3]-xi[4];
		k1+=models(40,1);
		/* Restore variables */
		xi[1]=xi2[1];
		xi[4]=xi2[4];
		xi[2]=xi2[2];
		/* Partial Energy of z and (1-z) calc */
		k1=ival+(1.0-z)*k1/2.0/dx;
		k2=(ival-z*k1)/(1.0-z);
	        k1=k1-rt*(1.0-y)*log(z);
	        k2=k2-rt*(1.0-y)*log(1.0-z);
		k3=MINV(k1,k2);
	        k1=(k3-k1)/(1.0-y)/rt; if(k1<gimin) k1=gimin; k1=exp(k1);
	        k2=(k3-k2)/(1.0-y)/rt; if(k2<gimin) k2=gimin; k2=exp(k2);
		z1=k1/(k1+k2);
		/* New xi[] calc ---------------------------*/
		xi2[1]=z1;
		xi2[3]=y1;
		xi2[4]=x1*(1.0+0.2*z1-0.2*y1);
		xi2[2]=1.0-xi2[1]-xi2[3]-xi2[4];
		return 0;
		/**/
                /* Stand G calc */
		case -1:
	        return gstand(0);
                /* Stand G for V calc */
		case -2:
	        return gstand(1);
                }
	/* End MODEL 40 - CHL  Afchl(1)-Clin(2)-Ames(3)-Daph(4) */

	/* MODEL 44 - CAR  Mcar(1)-Fcar(2) */
	case 44:
	switch (min)
        	{
                /* Mix G calc */
		case 1:
		/* Gmix-id */
		/* Mcar(1) */
		gii[1]=rt*log(xi[1]);
		/* Fcar(2) */
		gii[2]=rt*log(xi[2]);
                /**/
		/* Calc Integral energy */
		ival=xi[1]*(gio[1]+gii[1])+xi[2]*(gio[2]+gii[2]);
		/**/
                /* Mix G for V calc */
		case 0:
		gie[1]=gie[2]=0;
		return ival;
                /**/
		case 2:
		/* New xi[] calc */
		/* Fcar(2) Mcar=Fcar */
	        z1=(gio[1]-gio[2])/rt;
	        if(z1<gimin) z1=gimin;
	        if(z1>gimax) z1=gimax;
	        z1=exp(z1);
		xi2[2]=z1/(1.0+z1);
		/* Mcar(1) 1-Fcar */
		xi2[1]=1.0-xi2[2];
		return 0;
                /**/
                /* Stand G calc */
		case -1:
	        return gstand(0);
                /* Stand G for V calc */
		case -2:
	        return gstand(1);
                }
	/* End MODEL 44 - CAR  Mcar(1)-Fcar(2) */


	/* MODEL 45 - CTD  Mctd(1)-Fctd(2) */
	case 45:
	switch (min)
        	{
                /* Mix G calc */
		case 1:
		/* Gmix-id */
		/* Mctd(1) */
		gii[1]=rt*log(xi[1]);
		/* Fctd(2) */
		gii[2]=rt*log(xi[2]);
                /**/
		/* Calc Integral energy */
		ival=xi[1]*(gio[1]+gii[1])+xi[2]*(gio[2]+gii[2]);
		/**/
                /* Mix G for V calc */
		case 0:
		gie[1]=gie[2]=0;
		return ival;
                /**/
		case 2:
		/* New xi[] calc */
		/* Fctd(2) Mctd=Fctd */
	        z1=(gio[1]-gio[2])/rt;
	        if(z1<gimin) z1=gimin;
	        if(z1>gimax) z1=gimax;
	        z1=exp(z1);
		xi2[2]=z1/(1.0+z1);
		/* Mctd(1) 1-Fctd */
		xi2[1]=1.0-xi2[2];
		return 0;
                /**/
                /* Stand G calc */
		case -1:
	        return gstand(0);
                /* Stand G for V calc */
		case -2:
	        return gstand(1);
                }
	/* End MODEL 45 - CTD  Mctd(1)-Fctd(2) */


	/* MODEL 50 - OL  Fo(1)-Fa(2) */
	case 50:
	switch (min)
        	{
                /* Mix G calc */
		case 1:
		/* Gmix-id */
		/* Fo(1) */
		gii[1]=2.0*rt*log(xi[1]);
		/* Fa(2) */
		gii[2]=2.0*rt*log(xi[2]);
                /**/
		/* Calc Integral energy */
		ival=xi[1]*(gio[1]+gii[1])+xi[2]*(gio[2]+gii[2]);
		/**/
                /* Mix G for V calc */
		case 0:
		/* Gmix-ex */
		/* wi[1] Fo(1)-Fa(2) */
		/* Fo(1) */
		gie[1]=(1.0-xi[1])*xi[2]*wi[1];
		/* Fa(2) */
		gie[2]=(1.0-xi[2])*xi[1]*wi[1];
                /**/
		/* Add Integral energy */
		ival+=xi[1]*gie[1]+xi[2]*gie[2];
		return ival;
                /**/
		case 2:
		/* New xi[] calc */
		/* Fa(2) Fo=Fa */
	        z1=((gio[1]+gie[1])-(gio[2]+gie[2]))/rt/2.0;
	        if(z1<gimin) z1=gimin;
	        if(z1>gimax) z1=gimax;
	        z1=exp(z1);
		xi2[2]=z1/(1.0+z1);
		/* Fo(1) 1-Fa */
		xi2[1]=1.0-xi2[2];
		return 0;
                /**/
                /* Stand G calc */
		case -1:
	        return gstand(0);
                /* Stand G for V calc */
		case -2:
	        return gstand(1);
                }
	/* End MODEL 50 - OL  Fo-Fa */


	/* MODEL 55 - TA Ta(1)-Tats(2)-Fta(3) */
	case 55:
	switch (min)
        	{
                /* Mix G calc */
		case 1:
		/* Ta(1)-Tats(2)-Fta(3) site ideal mixing */
		/* Site variables */
		/* M1 */
		xfem1=xi[3]/(1.0-xi[2]/3.0);
		xmgm1=1.0-xfem1;
		/* M2 */
		xalm2=xi[2];
		xfem2=xfem1*(1.0-xalm2);
		xmgm2=1.0-xfem2-xalm2;
		/* T1 */
		xalt2=0.5*xi[2];
		xsit2=1.0-xalt2;
		/* Check site variables */
		if(xfem1<=0 || xmgm1<=0 || xalm2<=0 || xfem2<=0 || xmgm2<=0 || xalt2<=0 || xsit2<=0) 
			{
			gii[1]=0;
			return 0;
			}
		/* Ta(1)*/
		gii[1]=rt*log(xmgm1*xmgm1*xmgm2*xsit2*xsit2);
		/* Tats(2)*/
		gii[2]=rt*log(4.0*xmgm1*xmgm1*xalm2*xalt2*xsit2);
		/* Fta(3)*/
		gii[3]=rt*log(xfem1*xfem1*xfem2*xsit2*xsit2);
                /**/
		/* Calc Integral energy */
		ival=xi[1]*(gio[1]+gii[1])+xi[2]*(gio[2]+gii[2])+xi[3]*(gio[3]+gii[3]);
                /**/
                /* Mix G for V calc */
		case 0:
		gie[1]=gie[2]=gie[3]=0;
		return ival;
                /**/
		case 2:
		/* Save mol fractions */
		xi2[1]=xi[1];
		xi2[2]=xi[2];
		xi2[3]=xi[3];
		/* Compositional variables x=Fe/(Fe+Mg), y=XalM2 */
		x=xi[3]/(1.0-xi[2]/3.0);
		y=xi[2];
		/* Integral Gibbs Energy calc */
		ival=models(55,1);
		/* New site variables calc */
		/* x  ----------------------------------*/
		/* Increment Calc */
		dx=(0.5-ABSV(0.5-x))*1e-3;
		/* Differentiation */
		x1=x-dx;
		xi[3]=x1*(1.0-y/3.0);
		xi[1]=1.0-xi[2]-xi[3];
		k1=-models(55,1);
		x1=x+dx;
		xi[3]=x1*(1.0-y/3.0);
		xi[1]=1.0-xi[2]-xi[3];
		k1+=models(55,1);
		/* Restore variables */
		xi[3]=xi2[3];
		xi[1]=xi2[1];
		/* Partial Energy of x and (1-x) calc */
		k1=ival+(1.0-x)*k1/2.0/dx;
		k2=(ival-x*k1)/(1.0-x);
	        k1=k1-rt*(3.0-y)*log(x);
	        k2=k2-rt*(3.0-y)*log(1.0-x);
		k3=MINV(k1,k2);
	        k1=(k3-k1)/(3.0-y)/rt; if(k1<gimin) k1=gimin; k1=exp(k1);
	        k2=(k3-k2)/(3.0-y)/rt; if(k2<gimin) k2=gimin; k2=exp(k2);
		x1=k1/(k1+k2);
		/* y ----------------------------------*/
		/* Increment Calc */
		dx=(0.5-ABSV(0.5-y))*1e-3;
		/* Differentiation */
		y1=y-dx;
		xi[2]=y1;
		xi[3]=x*(1.0-y1/3.0);
		xi[1]=1.0-xi[2]-xi[3];
		k1=-models(55,1);
		y1=y+dx;
		xi[2]=y1;
		xi[3]=x*(1.0-y1/3.0);
		xi[1]=1.0-xi[2]-xi[3];
		k1+=models(55,1);
		/* Restore variables */
		xi[2]=xi2[2];
		xi[3]=xi2[3];
		xi[1]=xi2[1];
		/* Partial Energy of y and (1-y) calc */
		k1=ival+(1.0-y)*k1/2.0/dx;
		k2=(ival-y*k1)/(1.0-y);
	        k1=k1-rt*log(y);
	        k2=k2-rt*log(1.0-y);
		k3=MINV(k1,k2);
	        k1=(k3-k1)/rt; if(k1<gimin) k1=gimin; k1=exp(k1);
	        k2=(k3-k2)/rt; if(k2<gimin) k2=gimin; k2=exp(k2);
		y1=k1/(k1+k2);
		/* New xi[] calc ---------------------------*/
		xi2[2]=y1;
		xi2[3]=x1*(1.0-y1/3.0);
		xi2[1]=1.0-xi2[2]-xi2[3];
	        return 0;
                /**/
                /* Stand G calc */
		case -1:
	        return gstand(0);
                /* Stand G for V calc */
		case -2:
	        return gstand(1);
                }
	/* End MODEL 55 - TA Ta(1)-Tats(2)-Fta(3) */

     /* MODEL 56 - NAM2 - Gl2(1)-Fgl2(2)-Tr2(3)-Ts2(4) */
	case 56:
	switch (min)
        	{
                /* Mix G calc */
		case 1:
		/* Gl2(1)-Fgl2(2)-Tr2(3)-Ts2(4) site ideal mixing */
		/* Site variables */
		/* M4 */
		xnam4=1.0-xi[3]-xi[4];
		xcam4=1.0-xnam4;
		/* M13 */
		xfem13=3.0*xi[2]/(3.0+2.0*xi[3]);
		xmgm13=1.0-xfem13;
		/* M2 */
		xalm2=1.0-xi[3];
		xmgm2=(1.0-xalm2)*xmgm13;
                xfem2=1.0-xmgm2-xalm2;
		/* T1 */
		xalt1=0.5*xi[4];
		xsit1=1.0-xalt1;
		/* Check site variables */
		if(xnam4<=0 || xcam4<=0 || xfem13<=0 || xmgm13<=0 || xalm2<=0 || xmgm2<=0 || xalt1<=0 || xsit1<=0 || xfem2<=0) 
			{
			gii[1]=0;
			return 0;
			}
		/* Gl2(1)*/
		gii[1]=rt*log(xnam4*xnam4*pow(xmgm13,3.0)*xalm2*xalm2*xsit1*xsit1);
		/* Fgl2(2)*/
		gii[2]=rt*log(xnam4*xnam4*pow(xfem13,3.0)*xalm2*xalm2*xsit1*xsit1);
		/* Tr2(3)*/
		gii[3]=rt*log(xcam4*xcam4*pow(xmgm13,3.0)*xmgm2*xmgm2*xsit1*xsit1);
		/* Ts(4)*/
		gii[4]=rt*log(4.0*xcam4*xcam4*pow(xmgm13,3.0)*xalm2*xalm2*xalt1*xsit1);
                /**/
		/* Calc Integral energy */
		ival=xi[1]*(gio[1]+gii[1])+xi[2]*(gio[2]+gii[2])+xi[3]*(gio[3]+gii[3])+xi[4]*(gio[4]+gii[4]);
                /**/
                /* Mix G for V calc */
		case 0:
		/* Regular mixing */
		/* wi[1] Gl2(1)-Fgl2(2) */
		/* wi[2] Gl2(1)-Tr2(3) */
		/* wi[3] Fgl2(2)-Tr2(3) */
		/* wi[4] Fgl2(2)-Ts2(4) */
		/* wi[5] Gl2(1)-Ts2(4) */
		/* wi[6] Tr2(3)-Ts2(4) */
		/* Gl2(1) */
		gie[1]=(1.0-xi[1])*xi[2]*wi[1]+(1.0-xi[1])*xi[3]*wi[2]-xi[2]*xi[3]*wi[3]-xi[2]*xi[4]*wi[4]+(1.0-xi[1])*xi[4]*wi[5]-xi[3]*xi[4]*wi[6];
		/* Fgl2(2) */
		gie[2]=(1.0-xi[2])*xi[1]*wi[1]-xi[1]*xi[3]*wi[2]+(1.0-xi[2])*xi[3]*wi[3]+(1.0-xi[2])*xi[4]*wi[4]-xi[1]*xi[4]*wi[5]-xi[3]*xi[4]*wi[6];
		/* Tr2(3) */
		gie[3]=-xi[2]*xi[1]*wi[1]+(1.0-xi[3])*xi[1]*wi[2]+(1.0-xi[3])*xi[2]*wi[3]-xi[2]*xi[4]*wi[4]-xi[1]*xi[4]*wi[5]+(1.0-xi[3])*xi[4]*wi[6];
		/* Ts2(4) */
		gie[4]=-xi[2]*xi[1]*wi[1]-xi[3]*xi[1]*wi[2]-xi[3]*xi[2]*wi[3]+(1.0-xi[4])*xi[2]*wi[4]+(1.0-xi[4])*xi[1]*wi[5]+(1.0-xi[4])*xi[3]*wi[6];
                /**/
		/* Add Integral energy */
		ival+=xi[1]*gie[1]+xi[2]*gie[2]+xi[3]*gie[3]+xi[4]*gie[4];
		return ival;
                /**/
		case 2:
		/* Save mol fractions */
		xi2[1]=xi[1];
		xi2[2]=xi[2];
		xi2[3]=xi[3];
		xi2[4]=xi[4];
		/* Compositional variables x=XnaM4, y=Fe/(Fe+Mg), z=AlT1/2=AlM2(nur aus Tschermakssubstitution))  */
		x=1.0-xi[3]-xi[4];
		y=3.0*xi[2]/(3.0+2.0*xi[3]);
		z=xi[4];                        /* oder z=1.0-xi[3]; */
		/* Integral Gibbs Energy calc */
		ival=models(56,1);
		/* New site variables calc */
		/* x  ----------------------------------*/
		/* Increment Calc */
		dx=(0.5-ABSV(0.5-x))*1e-3;
		/* Differentiation */
		x1=x-dx;
                xi[3]=1.0-x1-z;
		xi[2]=(1.0/3.0)*y*(5.0-2.0*x1-2.0*z);
		xi[1]=1.0-xi[2]-xi[3]-xi[4];
		k1=-models(56,1);
		x1=x+dx;
                xi[3]=1.0-x1-z;
		xi[2]=(1.0/3.0)*y*(5.0-2.0*x1-2.0*z);
		xi[1]=1.0-xi[2]-xi[3]-xi[4];
		k1+=models(56,1);
		/* Restore variables */
		xi[3]=xi2[3];
		xi[2]=xi2[2];
		xi[1]=xi2[1];
		/* Partial Energy of x and (1-x) calc */
		k1=ival+(1.0-x)*k1/2.0/dx;
		k2=(ival-x*k1)/(1.0-x);
	        k1=k1-rt*2.0*log(x);
	        k2=k2-rt*2.0*log(1.0-x);
		k3=MINV(k1,k2);
	        k1=(k3-k1)/2.0/rt; if(k1<gimin) k1=gimin; k1=exp(k1);
	        k2=(k3-k2)/2.0/rt; if(k2<gimin) k2=gimin; k2=exp(k2);
		x1=k1/(k1+k2);
		/* y ----------------------------------*/
		/* Increment Calc */
		dx=(0.5-ABSV(0.5-y))*1e-3;
		/* Differentiation */
		y1=y-dx;
		xi[2]=(1.0/3.0)*y1*(5.0-2.0*x-2.0*z);
		xi[1]=1.0-xi[2]-xi[3]-xi[4];
		k1=-models(56,1);
		y1=y+dx;
		xi[2]=(1.0/3.0)*y1*(5.0-2.0*x-2.0*z);
		xi[1]=1.0-xi[2]-xi[3]-xi[4];
		k1+=models(56,1);
		/* Restore variables */
		xi[2]=xi2[2];
		xi[1]=xi2[1];
		/* Partial Energy of y and (1-y) calc */
		k1=ival+(1.0-y)*k1/2.0/dx;
		k2=(ival-y*k1)/(1.0-y);
	        k1=k1-rt*(5.0-2.0*(x+z))*log(y);
	        k2=k2-rt*(5.0-2.0*(x+z))*log(1.0-y);
		k3=MINV(k1,k2);
	        k1=(k3-k1)/(5.0-2.0*(x+z))/rt; if(k1<gimin) k1=gimin; k1=exp(k1);
	        k2=(k3-k2)/(5.0-2.0*(x+z))/rt; if(k2<gimin) k2=gimin; k2=exp(k2);
		y1=k1/(k1+k2);
		/* z ----------------------------------*/
		/* Increment Calc */
		dx=(0.5-ABSV(0.5-z))*1e-3;
		/* Differentiation */
		z1=z-dx;
                xi[4]=z1;
                xi[2]=(1.0/3.0)*y*(5.0-2.0*x-2.0*z1);
		xi[1]=1.0-xi[2]-xi[3]-xi[4];
		k1=-models(56,1);
		z1=z+dx;
		xi[4]=z1;
                xi[2]=(1.0/3.0)*y*(5.0-2.0*x-2.0*z1);
		xi[1]=1.0-xi[2]-xi[3]-xi[4];
		k1+=models(56,1);
		/* Restore variables */
                xi[4]=xi2[4];
		xi[2]=xi2[2];
		xi[1]=xi2[1];
		/* Partial Energy of z and (1-z) calc */
		k1=ival+(1.0-z)*k1/2.0/dx;
		k2=(ival-z*k1)/(1.0-z);
	        k1=k1-rt*2.0*log(z);
	        k2=k2-rt*2.0*log(1.0-z);
		k3=MINV(k1,k2);
	        k1=(k3-k1)/2.0/rt; if(k1<gimin) k1=gimin; k1=exp(k1);
	        k2=(k3-k2)/2.0/rt; if(k2<gimin) k2=gimin; k2=exp(k2);
		z1=k1/(k1+k2);
		/* New xi[] calc ---------------------------*/
		xi2[2]=(1.0/3.0)*y1*(5.0-2.0*x1-2.0*z1);
		xi2[4]=z1;
		xi2[3]=1.0-x1-z1;
		xi2[1]=1.0-xi2[2]-xi2[3]-xi2[4];
	        return 0;
                /**/
                /* Stand G calc */
		case -1:
	        return gstand(0);
                /* Stand G for V calc */
		case -2:
	        return gstand(1);
                }
	/* End MODEL 56 - NAM2 Gl2(1)-Fgl2(2)-Tr2(3)-Ts2(4) */                                                                                


     /* MODEL 57 - NAM2 - Gl2(1)-Fgl2(2)-Tr2(3)-Ts2(4) */
	case 57:
	switch (min)
        	{
                /* Mix G calc */
		case 1:
		/* Gl2(1)-Fgl2(2)-Tr2(3)-Ts2(4) site ideal mixing */
		/* Site variables */
		/* M4 */
		xnam4=1.0-xi[3]-xi[4];
		xcam4=1.0-xnam4;
		/* M13 */
		xfem13=3.0*xi[2]/(3.0+2.0*xi[3]);
		xmgm13=1.0-xfem13;
		/* M2 */
		xalm2=1.0-xi[3];
		xmgm2=(1.0-xalm2)*xmgm13;
                xfem2=1.0-xmgm2-xalm2;
		/* T1 */
		xalt1=0.5*xi[4];
		xsit1=1.0-xalt1;
		/* Check site variables */
		if(xnam4<=0 || xcam4<=0 || xfem13<=0 || xmgm13<=0 || xalm2<=0 || xmgm2<=0 || xalt1<=0 || xsit1<=0 || xfem2<=0) 
			{
			gii[1]=0;
			return 0;
			}
		/* Gl2(1)*/
		gii[1]=rt*log(xnam4*xnam4*pow(xmgm13,3.0)*xalm2*xalm2*xsit1*xsit1);
		/* Fgl2(2)*/
		gii[2]=rt*log(xnam4*xnam4*pow(xfem13,3.0)*xalm2*xalm2*xsit1*xsit1);
		/* Tr2(3)*/
		gii[3]=rt*log(xcam4*xcam4*pow(xmgm13,3.0)*xmgm2*xmgm2*xsit1*xsit1);
		/* Ts(4)*/
		gii[4]=rt*log(4.0*xcam4*xcam4*pow(xmgm13,3.0)*xalm2*xalm2*xalt1*xsit1);
                /**/
		/* Calc Integral energy */
		ival=xi[1]*(gio[1]+gii[1])+xi[2]*(gio[2]+gii[2])+xi[3]*(gio[3]+gii[3])+xi[4]*(gio[4]+gii[4]);
                /**/
                /* Mix G for V calc */
		case 0:
		/* Gmix-ex */
		/* Darken Quardratic Formalism*/
		/* wi[1] Tr2(3) = 46.3 kJ/mol */
		/* wi[2] Ts2(4) = 21.18 kJ/mol */
		/* Gl2(1) */
                gie[1]=0;
		/* Fgl2(2) */
		gie[2]=0;
		/* Tr2(3) */
		gie[3]=wi[1];
		/* Ts2(4) */
		gie[4]=wi[2];
                /**/
		/* Add Integral energy */
		ival+=xi[1]*gie[1]+xi[2]*gie[2]+xi[3]*gie[3]+xi[4]*gie[4];
		return ival;
                /**/
		case 2:
		/* Save mol fractions */
		xi2[1]=xi[1];
		xi2[2]=xi[2];
		xi2[3]=xi[3];
		xi2[4]=xi[4];
		/* Compositional variables x=XnaM4, y=Fe/(Fe+Mg), z=AlT1/2=AlM2(nur aus Tschermakssubstitution))  */
		x=1.0-xi[3]-xi[4];
		y=3.0*xi[2]/(3.0+2.0*xi[3]);
		z=xi[4];                        /* oder z=1.0-xi[3]; */
		/* Integral Gibbs Energy calc */
		ival=models(57,1);
		/* New site variables calc */
		/* x  ----------------------------------*/
		/* Increment Calc */
		dx=(0.5-ABSV(0.5-x))*1e-3;
		/* Differentiation */
		x1=x-dx;
                xi[3]=1.0-x1-z;
		xi[2]=(1.0/3.0)*y*(5.0-2.0*x1-2.0*z);
		xi[1]=1.0-xi[2]-xi[3]-xi[4];
		k1=-models(57,1);
		x1=x+dx;
                xi[3]=1.0-x1-z;
		xi[2]=(1.0/3.0)*y*(5.0-2.0*x1-2.0*z);
		xi[1]=1.0-xi[2]-xi[3]-xi[4];
		k1+=models(57,1);
		/* Restore variables */
		xi[3]=xi2[3];
		xi[2]=xi2[2];
		xi[1]=xi2[1];
		/* Partial Energy of x and (1-x) calc */
		k1=ival+(1.0-x)*k1/2.0/dx;
		k2=(ival-x*k1)/(1.0-x);
	        k1=k1-rt*2.0*log(x);
	        k2=k2-rt*2.0*log(1.0-x);
		k3=MINV(k1,k2);
	        k1=(k3-k1)/2.0/rt; if(k1<gimin) k1=gimin; k1=exp(k1);
	        k2=(k3-k2)/2.0/rt; if(k2<gimin) k2=gimin; k2=exp(k2);
		x1=k1/(k1+k2);
		/* y ----------------------------------*/
		/* Increment Calc */
		dx=(0.5-ABSV(0.5-y))*1e-3;
		/* Differentiation */
		y1=y-dx;
		xi[2]=(1.0/3.0)*y1*(5.0-2.0*x-2.0*z);
		xi[1]=1.0-xi[2]-xi[3]-xi[4];
		k1=-models(57,1);
		y1=y+dx;
		xi[2]=(1.0/3.0)*y1*(5.0-2.0*x-2.0*z);
		xi[1]=1.0-xi[2]-xi[3]-xi[4];
		k1+=models(57,1);
		/* Restore variables */
		xi[2]=xi2[2];
		xi[1]=xi2[1];
		/* Partial Energy of y and (1-y) calc */
		k1=ival+(1.0-y)*k1/2.0/dx;
		k2=(ival-y*k1)/(1.0-y);
	        k1=k1-rt*(5.0-2.0*(x+z))*log(y);
	        k2=k2-rt*(5.0-2.0*(x+z))*log(1.0-y);
		k3=MINV(k1,k2);
	        k1=(k3-k1)/(5.0-2.0*(x+z))/rt; if(k1<gimin) k1=gimin; k1=exp(k1);
	        k2=(k3-k2)/(5.0-2.0*(x+z))/rt; if(k2<gimin) k2=gimin; k2=exp(k2);
		y1=k1/(k1+k2);
		/* z ----------------------------------*/
		/* Increment Calc */
		dx=(0.5-ABSV(0.5-z))*1e-3;
		/* Differentiation */
		z1=z-dx;
                xi[4]=z1;
                xi[2]=(1.0/3.0)*y*(5.0-2.0*x-2.0*z1);
		xi[1]=1.0-xi[2]-xi[3]-xi[4];
		k1=-models(57,1);
		z1=z+dx;
		xi[4]=z1;
                xi[2]=(1.0/3.0)*y*(5.0-2.0*x-2.0*z1);
		xi[1]=1.0-xi[2]-xi[3]-xi[4];
		k1+=models(57,1);
		/* Restore variables */
                xi[4]=xi2[4];
		xi[2]=xi2[2];
		xi[1]=xi2[1];
		/* Partial Energy of z and (1-z) calc */
		k1=ival+(1.0-z)*k1/2.0/dx;
		k2=(ival-z*k1)/(1.0-z);
	        k1=k1-rt*2.0*log(z);
	        k2=k2-rt*2.0*log(1.0-z);
		k3=MINV(k1,k2);
	        k1=(k3-k1)/2.0/rt; if(k1<gimin) k1=gimin; k1=exp(k1);
	        k2=(k3-k2)/2.0/rt; if(k2<gimin) k2=gimin; k2=exp(k2);
		z1=k1/(k1+k2);
		/* New xi[] calc ---------------------------*/
		xi2[2]=(1.0/3.0)*y1*(5.0-2.0*x1-2.0*z1);
		xi2[4]=z1;
		xi2[3]=1.0-x1-z1;
		xi2[1]=1.0-xi2[2]-xi2[3]-xi2[4];
	        return 0;
                /**/
                /* Stand G calc */
		case -1:
	        return gstand(0);
                /* Stand G for V calc */
		case -2:
	        return gstand(1);
                }
	/* End MODEL 57 - NAM2 Gl2(1)-Fgl2(2)-Tr2(3)-Ts2(4) */                                                                                

     /* MODEL 58 - NACAAM Tr2(1)-Fact2(2)-Parg2(3)-Gl(4) */
	case 58:
	switch (min)
        	{
                /* Mix G calc */
		case 1:
		/* Tr2(1)-Fact2(2)-Parg2(3)-Gl(4) site ideal mixing */
		/* Site variables */
		/* A */
		xvaa=1.0-xi[3];
		xnaa=xi[3];
		/* M4 */
		xnam4=xi[4];
		xcam4=1.0-xi[4];
		/* M13 */
		xfem13=xi[2]/(1.0-0.2*xi[3]-0.4*xi[4]);
		xmgm13=1.0-xfem13;
		/* M2 */
		xalm2=0.5*xi[3]+xi[4];
		xfem2=xfem13*(1.0-xalm2);
		xmgm2=1.0-xfem2-xalm2;
		/* T1 */
		xalt1=0.5*xi[3];
		xsit1=1.0-xalt1;
		/* Check site variables */
		if(xvaa<=0 || xnaa<=0 || xnam4<=0 || xcam4<=0 || xfem13<=0 || xmgm13<=0 || xfem2<=0 || xalm2<=0 || xmgm2<=0 || xalt1<=0 || xsit1<=0) 
			{
			gii[1]=0;
			return 0;
			}
		/* Tr2(1)*/
		gii[1]=rt*log(xvaa*xcam4*xcam4*pow(xmgm13,3.0)*xmgm2*xmgm2*xsit1*xsit1);
		/* Fact2(2)*/
		gii[2]=rt*log(xvaa*xcam4*xcam4*pow(xfem13,3.0)*xfem2*xfem2*xsit1*xsit1);
		/* Parg2(3)*/
		gii[3]=rt*log(16.0*xnaa*xcam4*xcam4*pow(xmgm13,3.0)*xmgm2*xalm2*xalt1*xsit1);
		/* Gl(4)*/
		gii[4]=rt*log(xvaa*xnam4*xnam4*pow(xmgm13,3.0)*xalm2*xalm2*xsit1*xsit1);
                /**/
		/* Calc Integral energy */
		ival=xi[1]*(gio[1]+gii[1])+xi[2]*(gio[2]+gii[2])+xi[3]*(gio[3]+gii[3])+xi[4]*(gio[4]+gii[4]);
                /**/
                /* Mix G for V calc */
		case 0:
		/* Gmix-ex */
		/* Darken Quardratic Formalism*/
		/* wi[1] Par2(3)=40 kJ/mol */
		/* wi[2] Gl(4) =32 kJ/mol */
		/* Tr2(1) */
                gie[1]=0;
		/* Fact2(2) */
		gie[2]=0;
		/* Parg2(3) */
		gie[3]=wi[1];
		/* Gl(4) */
		gie[4]=wi[2];
                /**/
		/* Add Integral energy */
		ival+=xi[1]*gie[1]+xi[2]*gie[2]+xi[3]*gie[3]+xi[4]*gie[4];
		return ival;
                /**/
		case 2:
		/* Save mol fractions */
		xi2[1]=xi[1];
		xi2[2]=xi[2];
		xi2[3]=xi[3];
		xi2[4]=xi[4];
		/* Compositional variables x=XnaA, y=XnaM4, z=Fe/(Fe+Mg)  */
		x=xi[3];
		y=xi[4];
		z=xi[2]/(1.0-0.4*y-0.2*x);
		/* Integral Gibbs Energy calc */
		ival=models(58,1);
		/* New site variables calc */
		/* x  ----------------------------------*/
		/* Increment Calc */
		dx=(0.5-ABSV(0.5-x))*1e-3;
		/* Differentiation */
		x1=x-dx;
		xi[3]=x1;
                xi[2]=z*(1.0-0.4*y-0.2*x1);
		xi[1]=1.0-xi[2]-xi[3]-xi[4];
		k1=-models(58,1);
		x1=x+dx;
		xi[3]=x1;
                xi[2]=z*(1.0-0.4*y-0.2*x1);
		xi[1]=1.0-xi[2]-xi[3]-xi[4];
		k1+=models(58,1);
		/* Restore variables */
		xi[3]=xi2[3];
		xi[2]=xi2[2];
		xi[1]=xi2[1];
		/* Partial Energy of x and (1-x) calc */
		k1=ival+(1.0-x)*k1/2.0/dx;
		k2=(ival-x*k1)/(1.0-x);
	        k1=k1-rt*log(x);
	        k2=k2-rt*log(1.0-x);
		k3=MINV(k1,k2);
	        k1=(k3-k1)/rt; if(k1<gimin) k1=gimin; k1=exp(k1);
	        k2=(k3-k2)/rt; if(k2<gimin) k2=gimin; k2=exp(k2);
		x1=k1/(k1+k2);
		/* y ----------------------------------*/
		/* Increment Calc */
		dx=(0.5-ABSV(0.5-y))*1e-3;
		/* Differentiation */
		y1=y-dx;
		xi[4]=y1;
		xi[2]=z*(1.0-0.4*y1-0.2*x);
		xi[1]=1.0-xi[2]-xi[3]-xi[4];
		k1=-models(58,1);
		y1=y+dx;
		xi[4]=y1;
		xi[2]=z*(1.0-0.4*y1-0.2*x);
		xi[1]=1.0-xi[2]-xi[3]-xi[4];
		k1+=models(58,1);
		/* Restore variables */
		xi[2]=xi2[2];
		xi[4]=xi2[4];
		xi[1]=xi2[1];
		/* Partial Energy of y and (1-y) calc */
		k1=ival+(1.0-y)*k1/2.0/dx;
		k2=(ival-y*k1)/(1.0-y);
	        k1=k1-rt*2.0*log(y);
	        k2=k2-rt*2.0*log(1.0-y);
		k3=MINV(k1,k2);
	        k1=(k3-k1)/2.0/rt; if(k1<gimin) k1=gimin; k1=exp(k1);
	        k2=(k3-k2)/2.0/rt; if(k2<gimin) k2=gimin; k2=exp(k2);
		y1=k1/(k1+k2);
		/* z ----------------------------------*/
		/* Increment Calc */
		dx=(0.5-ABSV(0.5-z))*1e-3;
		/* Differentiation */
		z1=z-dx;
                xi[2]=z1*(1.0-0.4*y-0.2*x);
		xi[1]=1.0-xi[2]-xi[3]-xi[4];
		k1=-models(58,1);
		z1=z+dx;
		xi[2]=z1*(1.0-0.4*y-0.2*x);
		xi[1]=1.0-xi[2]-xi[3]-xi[4];
		k1+=models(58,1);
		/* Restore variables */
		xi[2]=xi2[2];
		xi[1]=xi2[1];
		/* Partial Energy of z and (1-z) calc */
		k1=ival+(1.0-z)*k1/2.0/dx;
		k2=(ival-z*k1)/(1.0-z);
	        k1=k1-rt*(5.0-(2.0*y+x))*log(z);
	        k2=k2-rt*(5.0-(2.0*y+x))*log(1.0-z);
		k3=MINV(k1,k2);
	        k1=(k3-k1)/(5.0-(2.0*y+x))/rt; if(k1<gimin) k1=gimin; k1=exp(k1);
	        k2=(k3-k2)/(5.0-(2.0*y+x))/rt; if(k2<gimin) k2=gimin; k2=exp(k2);
		z1=k1/(k1+k2);
		/* New xi[] calc ---------------------------*/
		xi2[2]=z1*(1.0-0.4*y1-0.2*x1);
		xi2[4]=y1;
		xi2[3]=x1;
		xi2[1]=1.0-xi2[2]-xi2[3]-xi2[4];
	        return 0;
                /**/
                /* Stand G calc */
		case -1:
	        return gstand(0);
                /* Stand G for V calc */
		case -2:
	        return gstand(1);
                }
	/* End MODEL 58 - NACAAM Tr2(1)-Fact2(2)-Parg2(3)-Gl(4) */                                                                                

	/* MODEL 59 - AMPH Tr(1)-Ts(2)-Parg(3)-Gl(4)-Fact(5) */
	case 59:
	switch (min)
        	{
                /* Mix G calc */
		case 1:
		/* Tr(1)-Ts(2)-Parg(3)-Gl(4)-Fact(5) site ideal mixing */
		/* Site variables */
		/* A */
		xvaa=1.0-xi[3];
		xnaa=xi[3];
		/* M4 */
		xnam4=xi[4];
		xcam4=1.0-xi[4];
		/* M13 */
		xfem13=xi[5]/(1.0-0.4*xi[2]-0.2*xi[3]-0.4*xi[4]);
		xmgm13=1.0-xfem13;
		/* M2 */
		xalm2=xi[2]+xi[4]+0.5*xi[3];
		xfem2=xfem13*(1.0-xalm2);
		xmgm2=1.0-xfem2-xalm2;
		/* T1 */
		xalt1=0.5*(xi[2]+xi[3]);
		xsit1=1.0-xalt1;
		/* Check site variables */
		if(xvaa<=0 || xnaa<=0 || xnam4<=0 || xcam4<=0 || xfem13<=0 || xmgm13<=0 || xfem2<=0 || xalm2<=0 || xmgm2<=0 || xalt1<=0 || xsit1<=0) 
			{
			gii[1]=0;
/*
printf("%e %e %e %e %e\n",xi[1],xi[2],xi[3],xi[4],xi[5]);
printf("%e %e %e %e %e %e %e %e %e %e %e",xvaa,xnaa,xnam4,xcam4,xfem13,xmgm13,xfem2,xalm2,xmgm2,xalt1,xsit1);getchar(); 
*/
			return 0;
			}
		/* Tr(1)*/
		gii[1]=rt*log(xvaa*xcam4*xcam4*pow(xmgm13,3.0)*xmgm2*xmgm2*xsit1*xsit1);
		/* Ts(2)*/
		gii[2]=rt*log(4.0*xvaa*xcam4*xcam4*pow(xmgm13,3.0)*xalm2*xalm2*xalt1*xsit1);
		/* Parg(3)*/
		gii[3]=rt*log(16.0*xnaa*xcam4*xcam4*pow(xmgm13,3.0)*xmgm2*xalm2*xalt1*xsit1);
		/* Gl(4)*/
		gii[4]=rt*log(xvaa*xnam4*xnam4*pow(xmgm13,3.0)*xalm2*xalm2*xsit1*xsit1);
		/* Fact(5)*/
		gii[5]=rt*log(xvaa*xcam4*xcam4*pow(xfem13,3.0)*xfem2*xfem2*xsit1*xsit1);
                /**/
		/* Calc Integral energy */
		ival=xi[1]*(gio[1]+gii[1])+xi[2]*(gio[2]+gii[2])+xi[3]*(gio[3]+gii[3])+xi[4]*(gio[4]+gii[4])+xi[5]*(gio[5]+gii[5]);
                /**/
                /* Mix G for V calc */
		case 0:
		/* Gmix-ex */
		/* Darken Quardratic Formalism*/
		/* wi[1] Par(3)=40 kJ/mol */
		/* wi[2] Gl(4) =32 kJ/mol */
                /* wi[3] Ts(2)*/
		/* Tr(1) */
                gie[1]=0;
		/* Ts(2) */
		gie[2]=wi[3];
		/* Parg(3) */
		gie[3]=wi[1];
		/* Gl(4) */
		gie[4]=wi[2];
		/* Fact(5) */
		gie[5]=0;
                /**/
		/* Add Integral energy */
		ival+=xi[1]*gie[1]+xi[2]*gie[2]+xi[3]*gie[3]+xi[4]*gie[4]+xi[5]*gie[5];
		return ival;
                /**/
		case 2:
		/* Save mol fractions */
		xi2[1]=xi[1];
		xi2[2]=xi[2];
		xi2[3]=xi[3];
		xi2[4]=xi[4];
		xi2[5]=xi[5];
		/* Compositional variables x=XnaA, y=XnaM4, z=Fe/(Fe+Mg), w=(alT1-XnaA)/2 */
		x=xi[3];
		y=xi[4];
		w=xi[2]+0.5*xi[3];
		z=xi[5]/(1.0-0.4*(y+w));
		/* Integral Gibbs Energy calc */
		ival=models(59,1);
		/* New site variables calc */
		/* x  ----------------------------------*/
		/* Increment Calc */
		dx=(0.5-ABSV(0.5-x))*1e-3;
		/* Differentiation */
		x1=x-dx;
		xi[3]=x1;
		xi[2]=w-0.5*x1;
		xi[1]=1.0-xi[2]-xi[3]-xi[4]-xi[5];
		k1=-models(59,1);
		x1=x+dx;
		xi[3]=x1;
		xi[2]=w-0.5*x1;
		xi[1]=1.0-xi[2]-xi[3]-xi[4]-xi[5];
		k1+=models(59,1);
		/* Restore variables */
		xi[3]=xi2[3];
		xi[2]=xi2[2];
		xi[1]=xi2[1];
		/* Partial Energy of x and (1-x) calc */
		k1=ival+(1.0-x)*k1/2.0/dx;
		k2=(ival-x*k1)/(1.0-x);
	        k1=k1-rt*log(x);
	        k2=k2-rt*log(1.0-x);
		k3=MINV(k1,k2);
	        k1=(k3-k1)/rt; if(k1<gimin) k1=gimin; k1=exp(k1);
	        k2=(k3-k2)/rt; if(k2<gimin) k2=gimin; k2=exp(k2);
		x1=k1/(k1+k2);
		/* y ----------------------------------*/
		/* Increment Calc */
		dx=(0.5-ABSV(0.5-y))*1e-3;
		/* Differentiation */
		y1=y-dx;
		xi[4]=y1;
		xi[5]=z*(1.0-0.4*(y1+w));
		xi[1]=1.0-xi[2]-xi[3]-xi[4]-xi[5];
		k1=-models(59,1);
		y1=y+dx;
		xi[4]=y1;
		xi[5]=z*(1.0-0.4*(y1+w));
		xi[1]=1.0-xi[2]-xi[3]-xi[4]-xi[5];
		k1+=models(59,1);
		/* Restore variables */
		xi[5]=xi2[5];
		xi[4]=xi2[4];
		xi[1]=xi2[1];
		/* Partial Energy of y and (1-y) calc */
		k1=ival+(1.0-y)*k1/2.0/dx;
		k2=(ival-y*k1)/(1.0-y);
	        k1=k1-rt*2.0*log(y);
	        k2=k2-rt*2.0*log(1.0-y);
		k3=MINV(k1,k2);
	        k1=(k3-k1)/2.0/rt; if(k1<gimin) k1=gimin; k1=exp(k1);
	        k2=(k3-k2)/2.0/rt; if(k2<gimin) k2=gimin; k2=exp(k2);
		y1=k1/(k1+k2);
		/* z ----------------------------------*/
		/* Increment Calc */
		dx=(0.5-ABSV(0.5-z))*1e-3;
		/* Differentiation */
		z1=z-dx;
		xi[5]=z1*(1.0-0.4*(y+w));
		xi[1]=1.0-xi[2]-xi[3]-xi[4]-xi[5];
		k1=-models(59,1);
		z1=z+dx;
		xi[5]=z1*(1.0-0.4*(y+w));
		xi[1]=1.0-xi[2]-xi[3]-xi[4]-xi[5];
		k1+=models(59,1);
		/* Restore variables */
		xi[5]=xi2[5];
		xi[1]=xi2[1];
		/* Partial Energy of z and (1-z) calc */
		k1=ival+(1.0-z)*k1/2.0/dx;
		k2=(ival-z*k1)/(1.0-z);
	        k1=k1-rt*(5.0-2.0*(y+w))*log(z);
	        k2=k2-rt*(5.0-2.0*(y+w))*log(1.0-z);
		k3=MINV(k1,k2);
	        k1=(k3-k1)/(5.0-2.0*(y+w))/rt; if(k1<gimin) k1=gimin; k1=exp(k1);
	        k2=(k3-k2)/(5.0-2.0*(y+w))/rt; if(k2<gimin) k2=gimin; k2=exp(k2);
		z1=k1/(k1+k2);
		/* w ----------------------------------*/
		/* Increment Calc */
		dx=(0.5-ABSV(0.5-w))*1e-3;
		/* Differentiation */
		w1=w-dx;
		xi[5]=z*(1.0-0.4*(y+w1));
		xi[2]=w1-0.5*x;
		xi[1]=1.0-xi[2]-xi[3]-xi[4]-xi[5];
		k1=-models(59,1);
		w1=w+dx;
		xi[5]=z*(1.0-0.4*(y+w1));
		xi[2]=w1-0.5*x;
		xi[1]=1.0-xi[2]-xi[3]-xi[4]-xi[5];
		k1+=models(59,1);
		/* Restore variables */
		xi[5]=xi2[5];
		xi[2]=xi2[2];
		xi[1]=xi2[1];
		/* Partial Energy of w and (1-w) calc */
		k1=ival+(1.0-w)*k1/2.0/dx;
		k2=(ival-w*k1)/(1.0-w);
	        k1=k1-rt*2.0*log(w);
	        k2=k2-rt*2.0*log(1.0-w);
		k3=MINV(k1,k2);
	        k1=(k3-k1)/2.0/rt; if(k1<gimin) k1=gimin; k1=exp(k1);
	        k2=(k3-k2)/2.0/rt; if(k2<gimin) k2=gimin; k2=exp(k2);
		w1=k1/(k1+k2);
		/* New xi[] calc ---------------------------*/
		xi2[5]=z1*(1.0-0.4*(y1+w1));
		xi2[4]=y1;
		xi2[3]=x1;
		xi2[2]=w1-0.5*x1;
		xi2[1]=1.0-xi2[2]-xi2[3]-xi2[4]-xi2[5];
	        return 0;
                /**/
                /* Stand G calc */
		case -1:
	        return gstand(0);
                /* Stand G for V calc */
		case -2:
	        return gstand(1);
                }
	/* End MODEL 59 - AMPH Tr(1)-Ts(2)-Parg(3)-Gl(4)-Fact(5) */

	/* MODEL 60 - AMPH Tr(1)-Ts(2)-Parg(3)-Gl(4)-Fact(5) */
	case 60:
	switch (min)
        	{
                /* Mix G calc */
		case 1:
		/* Tr(1)-Ts(2)-Parg(3)-Gl(4)-Fact(5) site ideal mixing */
		/* Site variables */
		/* A */
		xvaa=1.0-xi[3];
		xnaa=xi[3];
		/* M4 */
		xnam4=xi[4];
		xcam4=1.0-xi[4];
		/* M13 */
		xfem13=xi[5]/(1.0-0.4*xi[2]-0.2*xi[3]-0.4*xi[4]);
		xmgm13=1.0-xfem13;
		/* M2 */
		xalm2=xi[2]+xi[4]+0.5*xi[3];
		xfem2=xfem13*(1.0-xalm2);
		xmgm2=1.0-xfem2-xalm2;
		/* T1 */
		xalt1=0.5*(xi[2]+xi[3]);
		xsit1=1.0-xalt1;
		/* Check site variables */
		if(xvaa<=0 || xnaa<=0 || xnam4<=0 || xcam4<=0 || xfem13<=0 || xmgm13<=0 || xfem2<=0 || xalm2<=0 || xmgm2<=0 || xalt1<=0 || xsit1<=0) 
			{
			gii[1]=0;
/*
printf("%e %e %e %e %e\n",xi[1],xi[2],xi[3],xi[4],xi[5]);
printf("%e %e %e %e %e %e %e %e %e %e %e",xvaa,xnaa,xnam4,xcam4,xfem13,xmgm13,xfem2,xalm2,xmgm2,xalt1,xsit1);getchar(); 
*/
			return 0;
			}
		/* Tr(1)*/
		gii[1]=rt*log(xvaa*xcam4*xcam4*pow(xmgm13,3.0)*xmgm2*xmgm2*xsit1*xsit1);
		/* Ts(2)*/
		gii[2]=rt*log(4.0*xvaa*xcam4*xcam4*pow(xmgm13,3.0)*xalm2*xalm2*xalt1*xsit1);
		/* Parg(3)*/
		gii[3]=rt*log(16.0*xnaa*xcam4*xcam4*pow(xmgm13,3.0)*xmgm2*xalm2*xalt1*xsit1);
		/* Gl(4)*/
		gii[4]=rt*log(xvaa*xnam4*xnam4*pow(xmgm13,3.0)*xalm2*xalm2*xsit1*xsit1);
		/* Fact(5)*/
		gii[5]=rt*log(xvaa*xcam4*xcam4*pow(xfem13,3.0)*xfem2*xfem2*xsit1*xsit1);
                /**/
		/* Calc Integral energy */
		ival=xi[1]*(gio[1]+gii[1])+xi[2]*(gio[2]+gii[2])+xi[3]*(gio[3]+gii[3])+xi[4]*(gio[4]+gii[4])+xi[5]*(gio[5]+gii[5]);
                /**/
                /* Mix G for V calc */
		case 0:
		/* Gmix-ex */
		/* Regular mixing */
		/* wi[1] Tr(1)-Ts(2) */
		/* wi[2] Tr(1)-Parg(3) */
		/* wi[3] Tr(1)-Fact(5) */
		/* wi[4] Tr(1)-Gl(4) */
		/* wi[5] Ts(2)-Parg(3) */
		/* wi[6] Ts(2)-Fact(5) */
		/* wi[7] Ts(2)-Gl(4) */
		/* wi[8] Parg(3)-Fact(5) */
		/* wi[9] Parg(3)-Gl(4) */
		/* wi[10] Fact(5)-Gl(4) */
		/* Tr(1) */
		gie[1]=(1.0-xi[1])*xi[2]*wi[1]+(1.0-xi[1])*xi[3]*wi[2]+(1.0-xi[1])*xi[5]*wi[3]+(1.0-xi[1])*xi[4]*wi[4]-xi[2]*xi[3]*wi[5]-xi[2]*xi[5]*wi[6]-xi[2]*xi[4]*wi[7]-xi[3]*xi[5]*wi[8]-xi[3]*xi[4]*wi[9]-xi[5]*xi[4]*wi[10];
		/* Ts(2) */
		gie[2]=(1.0-xi[2])*xi[1]*wi[1]-xi[1]*xi[3]*wi[2]-xi[1]*xi[5]*wi[3]-xi[1]*xi[4]*wi[4]+(1.0-xi[2])*xi[3]*wi[5]+(1.0-xi[2])*xi[5]*wi[6]+(1.0-xi[2])*xi[4]*wi[7]-xi[3]*xi[5]*wi[8]-xi[3]*xi[4]*wi[9]-xi[5]*xi[4]*wi[10];
		/* Parg(3) */
		gie[3]=-xi[1]*xi[2]*wi[1]+(1.0-xi[3])*xi[1]*wi[2]-xi[1]*xi[5]*wi[3]-xi[1]*xi[4]*wi[4]+(1.0-xi[3])*xi[2]*wi[5]-xi[2]*xi[5]*wi[6]-xi[2]*xi[4]*wi[7]+(1.0-xi[3])*xi[5]*wi[8]+(1.0-xi[3])*xi[4]*wi[9]-xi[5]*xi[4]*wi[10];
		/* Gl(4) */
		gie[4]=-xi[1]*xi[2]*wi[1]-xi[1]*xi[3]*wi[2]-xi[1]*xi[5]*wi[3]+(1.0-xi[4])*xi[1]*wi[4]-xi[2]*xi[3]*wi[5]-xi[2]*xi[5]*wi[6]+(1.0-xi[4])*xi[2]*wi[7]-xi[3]*xi[5]*wi[8]+(1.0-xi[4])*xi[3]*wi[9]+(1.0-xi[4])*xi[5]*wi[10];
		/* Fact(5) */
		gie[5]=-xi[1]*xi[2]*wi[1]-xi[1]*xi[3]*wi[2]+(1.0-xi[5])*xi[1]*wi[3]-xi[1]*xi[4]*wi[4]-xi[2]*xi[3]*wi[5]+(1.0-xi[5])*xi[2]*wi[6]-xi[2]*xi[4]*wi[7]+(1.0-xi[5])*xi[3]*wi[8]-xi[3]*xi[4]*wi[9]+(1.0-xi[5])*xi[4]*wi[10];
                /**/
		/* Add Integral energy */
		ival+=xi[1]*gie[1]+xi[2]*gie[2]+xi[3]*gie[3]+xi[4]*gie[4]+xi[5]*gie[5];
		return ival;
                /**/
		case 2:
		/* Save mol fractions */
		xi2[1]=xi[1];
		xi2[2]=xi[2];
		xi2[3]=xi[3];
		xi2[4]=xi[4];
		xi2[5]=xi[5];
		/* Compositional variables x=XnaA, y=XnaM4, z=Fe/(Fe+Mg), w=(alT1-XnaA)/2 */
		x=xi[3];
		y=xi[4];
		w=xi[2]+0.5*xi[3];
		z=xi[5]/(1.0-0.4*(y+w));
		/* Integral Gibbs Energy calc */
		ival=models(60,1);
		/* New site variables calc */
		/* x  ----------------------------------*/
		/* Increment Calc */
		dx=(0.5-ABSV(0.5-x))*1e-3;
		/* Differentiation */
		x1=x-dx;
		xi[3]=x1;
		xi[2]=w-0.5*x1;
		xi[1]=1.0-xi[2]-xi[3]-xi[4]-xi[5];
		k1=-models(60,1);
		x1=x+dx;
		xi[3]=x1;
		xi[2]=w-0.5*x1;
		xi[1]=1.0-xi[2]-xi[3]-xi[4]-xi[5];
		k1+=models(60,1);
		/* Restore variables */
		xi[3]=xi2[3];
		xi[2]=xi2[2];
		xi[1]=xi2[1];
		/* Partial Energy of x and (1-x) calc */
		k1=ival+(1.0-x)*k1/2.0/dx;
		k2=(ival-x*k1)/(1.0-x);
	        k1=k1-rt*log(x);
	        k2=k2-rt*log(1.0-x);
		k3=MINV(k1,k2);
	        k1=(k3-k1)/rt; if(k1<gimin) k1=gimin; k1=exp(k1);
	        k2=(k3-k2)/rt; if(k2<gimin) k2=gimin; k2=exp(k2);
		x1=k1/(k1+k2);
		/* y ----------------------------------*/
		/* Increment Calc */
		dx=(0.5-ABSV(0.5-y))*1e-3;
		/* Differentiation */
		y1=y-dx;
		xi[4]=y1;
		xi[5]=z*(1.0-0.4*(y1+w));
		xi[1]=1.0-xi[2]-xi[3]-xi[4]-xi[5];
		k1=-models(60,1);
		y1=y+dx;
		xi[4]=y1;
		xi[5]=z*(1.0-0.4*(y1+w));
		xi[1]=1.0-xi[2]-xi[3]-xi[4]-xi[5];
		k1+=models(60,1);
		/* Restore variables */
		xi[5]=xi2[5];
		xi[4]=xi2[4];
		xi[1]=xi2[1];
		/* Partial Energy of y and (1-y) calc */
		k1=ival+(1.0-y)*k1/2.0/dx;
		k2=(ival-y*k1)/(1.0-y);
	        k1=k1-rt*2.0*log(y);
	        k2=k2-rt*2.0*log(1.0-y);
		k3=MINV(k1,k2);
	        k1=(k3-k1)/2.0/rt; if(k1<gimin) k1=gimin; k1=exp(k1);
	        k2=(k3-k2)/2.0/rt; if(k2<gimin) k2=gimin; k2=exp(k2);
		y1=k1/(k1+k2);
		/* z ----------------------------------*/
		/* Increment Calc */
		dx=(0.5-ABSV(0.5-z))*1e-3;
		/* Differentiation */
		z1=z-dx;
		xi[5]=z1*(1.0-0.4*(y+w));
		xi[1]=1.0-xi[2]-xi[3]-xi[4]-xi[5];
		k1=-models(60,1);
		z1=z+dx;
		xi[5]=z1*(1.0-0.4*(y+w));
		xi[1]=1.0-xi[2]-xi[3]-xi[4]-xi[5];
		k1+=models(60,1);
		/* Restore variables */
		xi[5]=xi2[5];
		xi[1]=xi2[1];
		/* Partial Energy of z and (1-z) calc */
		k1=ival+(1.0-z)*k1/2.0/dx;
		k2=(ival-z*k1)/(1.0-z);
	        k1=k1-rt*(5.0-2.0*(y+w))*log(z);
	        k2=k2-rt*(5.0-2.0*(y+w))*log(1.0-z);
		k3=MINV(k1,k2);
	        k1=(k3-k1)/(5.0-2.0*(y+w))/rt; if(k1<gimin) k1=gimin; k1=exp(k1);
	        k2=(k3-k2)/(5.0-2.0*(y+w))/rt; if(k2<gimin) k2=gimin; k2=exp(k2);
		z1=k1/(k1+k2);
		/* w ----------------------------------*/
		/* Increment Calc */
		dx=(0.5-ABSV(0.5-w))*1e-3;
		/* Differentiation */
		w1=w-dx;
		xi[5]=z*(1.0-0.4*(y+w1));
		xi[2]=w1-0.5*x;
		xi[1]=1.0-xi[2]-xi[3]-xi[4]-xi[5];
		k1=-models(60,1);
		w1=w+dx;
		xi[5]=z*(1.0-0.4*(y+w1));
		xi[2]=w1-0.5*x;
		xi[1]=1.0-xi[2]-xi[3]-xi[4]-xi[5];
		k1+=models(60,1);
		/* Restore variables */
		xi[5]=xi2[5];
		xi[2]=xi2[2];
		xi[1]=xi2[1];
		/* Partial Energy of w and (1-w) calc */
		k1=ival+(1.0-w)*k1/2.0/dx;
		k2=(ival-w*k1)/(1.0-w);
	        k1=k1-rt*2.0*log(w);
	        k2=k2-rt*2.0*log(1.0-w);
		k3=MINV(k1,k2);
	        k1=(k3-k1)/2.0/rt; if(k1<gimin) k1=gimin; k1=exp(k1);
	        k2=(k3-k2)/2.0/rt; if(k2<gimin) k2=gimin; k2=exp(k2);
		w1=k1/(k1+k2);
		/* New xi[] calc ---------------------------*/
		xi2[5]=z1*(1.0-0.4*(y1+w1));
		xi2[4]=y1;
		xi2[3]=x1;
		xi2[2]=w1-0.5*x1;
		xi2[1]=1.0-xi2[2]-xi2[3]-xi2[4]-xi2[5];
	        return 0;
                /**/
                /* Stand G calc */
		case -1:
	        return gstand(0);
                /* Stand G for V calc */
		case -2:
	        return gstand(1);
                }
	/* End MODEL 60 - AMPH Tr(1)-Ts(2)-Parg(3)-Gl(4)-Fact(5) */



	/* MODEL 61 - HBL Tr(1)-Ts(2)-Parg(3)-Fact(4) */
	case 61:
	switch (min)
        	{
                /* Mix G calc */
		case 1:
		/* Tr(1)-Ts(2)-Parg(3)-Fact(4) site ideal mixing */
		/* Site variables */
		/* A */
		xnaa=xi[3];
		xvaa=1.0-xnaa;
		/* M13 */
		xfem13=xi[4]/(1.0-0.4*xi[2]-0.2*xi[3]);
		xmgm13=1.0-xfem13;
		/* M2 */
		xalm2=xi[2]+0.5*xi[3];
		xfem2=xfem13*(1.0-xalm2);
		xmgm2=1.0-xfem2-xalm2;
		/* T1 */
		xalt1=0.5*(xi[2]+xi[3]);
		xsit1=1.0-xalt1;
		/* Check site variables */
		if(xvaa<=0 || xnaa<=0 || xfem13<=0 || xmgm13<=0 || xfem2<=0 || xalm2<=0 || xmgm2<=0 || xalt1<=0 || xsit1<=0) 
			{
			gii[1]=0;
/*
printf("%e %e %e %e %e\n",xi[1],xi[2],xi[3],xi[4],xi[5]);
printf("%e %e %e %e %e %e %e %e %e %e %e",xvaa,xnaa,xnam4,xcam4,xfem13,xmgm13,xfem2,xalm2,xmgm2,xalt1,xsit1);getchar(); 
*/
			return 0;
			}
		/* Tr(1)*/
		gii[1]=rt*log(xvaa*pow(xmgm13,3.0)*xmgm2*xmgm2*xsit1*xsit1);
		/* Ts(2)*/
		gii[2]=rt*log(4.0*xvaa*pow(xmgm13,3.0)*xalm2*xalm2*xalt1*xsit1);
		/* Parg(3)*/
		gii[3]=rt*log(16.0*xnaa*pow(xmgm13,3.0)*xmgm2*xalm2*xalt1*xsit1);
		/* Fact(4)*/
		gii[4]=rt*log(xvaa*pow(xfem13,3.0)*xfem2*xfem2*xsit1*xsit1);
                /**/
		/* Calc Integral energy */
		ival=xi[1]*(gio[1]+gii[1])+xi[2]*(gio[2]+gii[2])+xi[3]*(gio[3]+gii[3])+xi[4]*(gio[4]+gii[4]);
                /**/
                /* Mix G for V calc */
		case 0:
		/* Gmix-ex */
		/* Darken quadratic formalism */
		/* wi[1] Ts(2)*/
		/* wi[2] Parg(3) */
		/* Tr(1) */
		gie[1]=0;
		/* Ts(2) */
		gie[2]=wi[1];
		/* Parg(3) */
		gie[3]=wi[2];
		/* Fact(4) */
		gie[4]=0;
                /**/
		/* Add Integral energy */
		ival+=xi[1]*gie[1]+xi[2]*gie[2]+xi[3]*gie[3]+xi[4]*gie[4];
		return ival;
                /**/
		case 2:
		/* Save mol fractions */
		xi2[1]=xi[1];
		xi2[2]=xi[2];
		xi2[3]=xi[3];
		xi2[4]=xi[4];
		/* Compositional variables x=XnaA, y=Fe/(Fe+Mg), z=(alM2/2) */
		x=xi[3];
		z=xi[2]+0.5*xi[3];
		y=xi[4]/(1.0-0.4*z);
		/* Integral Gibbs Energy calc */
		ival=models(61,1);
		/* New site variables calc */
		/* x  ----------------------------------*/
		/* Increment Calc */
		dx=(0.5-ABSV(0.5-x))*1e-3;
		/* Differentiation */
		x1=x-dx;
		xi[3]=x1;
		xi[2]=z-0.5*x1;
		xi[1]=1.0-xi[2]-xi[3]-xi[4];
		k1=-models(61,1);
		x1=x+dx;
		xi[3]=x1;
		xi[2]=z-0.5*x1;
		xi[1]=1.0-xi[2]-xi[3]-xi[4];
		k1+=models(61,1);
		/* Restore variables */
		xi[3]=xi2[3];
		xi[2]=xi2[2];
		xi[1]=xi2[1];
		/* Partial Energy of x and (1-x) calc */
		k1=ival+(1.0-x)*k1/2.0/dx;
		k2=(ival-x*k1)/(1.0-x);
	        k1=k1-rt*log(x);
	        k2=k2-rt*log(1.0-x);
		k3=MINV(k1,k2);
	        k1=(k3-k1)/rt; if(k1<gimin) k1=gimin; k1=exp(k1);
	        k2=(k3-k2)/rt; if(k2<gimin) k2=gimin; k2=exp(k2);
		x1=k1/(k1+k2);
		/* y ----------------------------------*/
		/* Increment Calc */
		dx=(0.5-ABSV(0.5-y))*1e-3;
		/* Differentiation */
		y1=y-dx;
		xi[4]=y1*(1.0-0.4*z);
		xi[1]=1.0-xi[2]-xi[3]-xi[4];
		k1=-models(61,1);
		y1=y+dx;
		xi[4]=y1*(1.0-0.4*z);
		xi[1]=1.0-xi[2]-xi[3]-xi[4];
		k1+=models(61,1);
		/* Restore variables */
		xi[4]=xi2[4];
		xi[1]=xi2[1];
		/* Partial Energy of y and (1-y) calc */
		k1=ival+(1.0-y)*k1/2.0/dx;
		k2=(ival-y*k1)/(1.0-y);
	        k1=k1-rt*(5.0-2.0*z)*log(y);
	        k2=k2-rt*(5.0-2.0*z)*log(1.0-y);
		k3=MINV(k1,k2);
	        k1=(k3-k1)/(5.0-2.0*z)/rt; if(k1<gimin) k1=gimin; k1=exp(k1);
	        k2=(k3-k2)/(5.0-2.0*z)/rt; if(k2<gimin) k2=gimin; k2=exp(k2);
		z1=k1/(k1+k2);
		/* z ----------------------------------*/
		/* Increment Calc */
		dx=(0.5-ABSV(0.5-z))*1e-3;
		/* Differentiation */
		z1=z-dx;
		xi[4]=y*(1.0-0.4*z1);
		xi[2]=z1-0.5*x;
		xi[1]=1.0-xi[2]-xi[3]-xi[4];
		k1=-models(61,1);
		z1=z+dx;
		xi[4]=y*(1.0-0.4*z1);
		xi[2]=z1-0.5*x;
		xi[1]=1.0-xi[2]-xi[3]-xi[4];
		k1+=models(61,1);
		/* Restore variables */
		xi[4]=xi2[4];
		xi[2]=xi2[2];
		xi[1]=xi2[1];
		/* Partial Energy of z and (1-z) calc */
		k1=ival+(1.0-z)*k1/2.0/dx;
		k2=(ival-z*k1)/(1.0-z);
	        k1=k1-rt*2.0*log(z);
	        k2=k2-rt*2.0*log(1.0-z);
		k3=MINV(k1,k2);
	        k1=(k3-k1)/2.0/rt; if(k1<gimin) k1=gimin; k1=exp(k1);
	        k2=(k3-k2)/2.0/rt; if(k2<gimin) k2=gimin; k2=exp(k2);
		z1=k1/(k1+k2);
		/* New xi[] calc ---------------------------*/
		xi2[4]=y1*(1.0-0.4*z1);
		xi2[3]=x1;
		xi2[2]=z1-0.5*x1;
		xi2[1]=1.0-xi2[2]-xi2[3]-xi2[4];
	        return 0;
                /**/
                /* Stand G calc */
		case -1:
	        return gstand(0);
                /* Stand G for V calc */
		case -2:
	        return gstand(1);
                }
	/* End MODEL 61 - HBL Tr(1)-Ts(2)-Parg(3)-Fact(4) */


	/* MODEL 62 - GL Gl(1)-Fgl(2) */
	case 62:
	switch (min)
        	{
                /* Mix G calc */
		case 1:
		/* Gmix-id */
		/* Gl(1) */
		gii[1]=3.0*rt*log(xi[1]);
		/* Fgl(2) */
		gii[2]=3.0*rt*log(xi[2]);
                /**/
		/* Calc Integral energy */
		ival=xi[1]*(gio[1]+gii[1])+xi[2]*(gio[2]+gii[2]);
		/**/
                /* Mix G for V calc */
		case 0:
		/* Gmix-ex */
		/* wi[1] Gl(1)-Fgl(2) */
		/* Gl(1) */
		gie[1]=(1.0-xi[1])*xi[2]*wi[1];
		/* Fgl(2) */
		gie[2]=(1.0-xi[2])*xi[1]*wi[1];
                /**/
		/* Add Integral energy */
		ival+=xi[1]*gie[1]+xi[2]*gie[2];
		return ival;
                /**/
		case 2:
		/* New xi[] calc */
		/* Fgl(2) Gl=Fgl */
	        z1=((gio[1]+gie[1])-(gio[2]+gie[2]))/rt/3.0;
	        if(z1<gimin) z1=gimin;
	        if(z1>gimax) z1=gimax;
	        z1=exp(z1);
		xi2[2]=z1/(1.0+z1);
		/* Gl(1) 1-Fgl */
		xi2[1]=1.0-xi2[2];
		return 0;
                /**/
                /* Stand G calc */
		case -1:
	        return gstand(0);
                /* Stand G for V calc */
		case -2:
	        return gstand(1);
		}
	/* End MODEL 62 - GL Gl(1)-Fgl(2) */
	
	

	/* MODEL 63 - NAM Gl1(1)-Tr1(2)-Fact1(3) */
	case 63:
	switch (min)
        	{
                /* Mix G calc */
		case 1:
		/* Gl1(1)-Tr1(2)-Fact1(3) site ideal mixing */
		/* Site variables */
		/* M4 */
		xnam4=xi[1];
		xcam4=1.0-xi[1];
		/* M13 */
		xfem13=xi[3];
		xmgm13=1.0-xi[3];
		/* M2 */
		xfem2=xi[3];
		xalm2=xi[1];
		xmgm2=1.0-xfem2-xalm2;
		/* Check site variables */
		if(xnam4<=0 || xcam4<=0 || xfem13<=0 || xmgm13<=0 || xfem2<=0 || xalm2<=0 || xmgm2<=0) 
			{
			gii[1]=0;
/*
printf("%e %e %e %e %e\n",xi[1],xi[2],xi[3],xi[4],xi[5]);
printf("%e %e %e %e %e %e %e %e %e %e %e",xvaa,xnaa,xnam4,xcam4,xfem13,xmgm13,xfem2,xalm2,xmgm2,xalt1,xsit1);getchar(); 
*/
			return 0;
			}
		/* Gl1(1)*/
		gii[1]=rt*log(xnam4*xnam4*pow(xmgm13,3.0)*xalm2*xalm2);
		/* Tr1(2)*/
		gii[2]=rt*log(xcam4*xcam4*pow(xmgm13,3.0)*xmgm2*xmgm2);
		/* Fact1(3)*/
		gii[3]=rt*log(xcam4*xcam4*pow(xfem13,3.0)*xfem2*xfem2);
                /**/
		/* Calc Integral energy */
		ival=xi[1]*(gio[1]+gii[1])+xi[2]*(gio[2]+gii[2])+xi[3]*(gio[3]+gii[3]);
                /**/
                /* Mix G for V calc */
		case 0:
		/* Gmix-ex */
		/* Darken  mixing */
		/* wi[1] Tr1(2) */
		/* wi[2] Fact1(3) */
		/* Gl1(1) */
		gie[1]=0;
		/* Tr1(2) */
		gie[2]=wi[1];
		/* Fact1(3) */
		gie[3]=wi[2];
                /**/
		/* Add Integral energy */
		ival+=xi[1]*gie[1]+xi[2]*gie[2]+xi[3]*gie[3];
		return ival;
                /**/
		case 2:
		/* Site variables */
		/* M2 */
		xfem2=xi[3];
		xalm2=xi[1];
		xmgm2=1.0-xfem2-xalm2;
		/* New site variables calc */
		/* XalM2,XfeM2 Gl1=Tr1=Fact1 */
	        z1=(gio[2]+gie[2]+gii[2])-rt*2.0*log(xmgm2);
	        z2=(gio[3]+gie[3]+gii[3])-rt*2.0*log(xfem2);
	        z3=(gio[1]+gie[1]+gii[1])-rt*2.0*log(xalm2);
		z4=MINV(z1,z2); z4=MINV(z4,z3);
	        z1=(z4-z1)/2.0/rt; if(z1<gimin) z1=gimin; z1=exp(z1);
	        z2=(z4-z2)/2.0/rt; if(z2<gimin) z2=gimin; z2=exp(z2);
	        z3=(z4-z3)/2.0/rt; if(z3<gimin) z3=gimin; z3=exp(z3);
	        z4=z1+z2+z3;
	        xalm2=z3/z4;
	        xfem2=z2/z4;
		/* New xi[] calc */
		/* Gl1(1) */
		xi2[1]=xalm2;
		/* Fact1(3) */
		xi2[3]=xfem2;
		/* Tr(2) */
		xi2[2]=1.0-xi2[1]-xi2[3];
	        return 0;
                /**/
                /* Stand G calc */
		case -1:
	        return gstand(0);
                /* Stand G for V calc */
		case -2:
	        return gstand(1);
                }
	/* End MODEL 63 - NAM Gl1(1)-Tr1(2)-Fact1(3) */

	/* MODEL 64 - NAM1 Gl1(1)-Fgl(2)-Rieb(3) */
	case 64:
	switch (min)
        	{
                /* Mix G calc */
		case 1:
		/* Gl1(1)-Fgl(2)-Rieb(3) site ideal mixing */
		/* Site variables */
		/* M13 */
		xfem13=xi[2];
		xmgm13=1.0-xi[2];
		/* M2 */
		xfe3m2=xi[3];
		xalm2=1.0-xi[3];
		/* Check site variables */
		if(xfem13<=0 || xmgm13<=0 || xfe3m2<=0 || xalm2<=0) 
			{
			gii[1]=0;
			return 0;
			}
		/* Gl1(1)*/
		gii[1]=rt*log(pow(xmgm13,3.0)*pow(xalm2,2.0));
		/* Fgl(2)*/
		gii[2]=rt*log(pow(xfem13,3.0)*pow(xalm2,2.0));
		/* Rieb(3)*/
		gii[3]=rt*log(pow(xfem13,3.0)*pow(xfe3m2,2.0));
                /**/
		/* Calc Integral energy */
		ival=xi[1]*(gio[1]+gii[1])+xi[2]*(gio[2]+gii[2])+xi[3]*(gio[3]+gii[3]);
                /**/
                /* Mix G for V calc */
		case 0:
		return ival;
		
	        case 2:
		/* Site variables */
		/* M13 */
		xfem13=xi[2];
		xmgm13=1.0-xi[2];
		/* M2 */
		xfe3m2=xi[3];
                xalm2=1.0-xi[3];
                /* New site variables calc */
		/* XFe M13 Fgl=Gl(1) */
	        z1=(gio[1]+gii[1])-rt*3.0*log(xmgm13);
	        z2=(gio[2]+gii[2])-rt*3.0*log(xfem13);
		z3=MINV(z1,z2); 
	        z1=(z3-z1)/3.0/rt; if(z1<gimin) z1=gimin; z1=exp(z1);
	        z2=(z3-z2)/3.0/rt; if(z2<gimin) z2=gimin; z2=exp(z2);
	        xfem13=z2/(z1+z2);
	        /* x Fe3+M2 Gl(1)=Rieb  */
                z1=(gio[1]+gii[1])-rt*2.0*log(xalm2);
                z2=(gio[3]+gii[3])-rt*2.0*log(xfe3m2);
                z3=MINV(z1,z2);
                z1=(z3-z1)/2.0/rt; if(z1<gimin) z1=gimin; z1=exp(z1);
                z2=(z3-z2)/2.0/rt; if(z2<gimin) z2=gimin; z2=exp(z2);
                xfe3m2=z2/(z1+z2);
		/* New xi[] calc */
		/* Fgl(2) */
		xi2[2]=xfem13;
		/* Rieb(3) */
		xi2[3]=xfe3m2;
		/* Gl(1) */
		xi2[1]=1.0-xi2[2]-xi2[3];
	        return 0;
                /**/
                /* Stand G calc */
		case -1:
	        return gstand(0);
                /* Stand G for V calc */
		case -2:
	        return gstand(1);
                }
	/* End MODEL 64 - NAM1 Gl1(1)-Fgl(2)-Rieb(3) */

	/* MODEL 69 - CPX1 Di1(1)-Hed1(2) */
	case 69:
	switch (min)
        	{
                /* Mix G calc */
		case 1:
		/* Gmix-id */
		/* Di1(1) */
		gii[1]=rt*log(xi[1]);
		/* Hed1(2) */
		gii[2]=rt*log(xi[2]);
                /**/
		/* Calc Integral energy */
		ival=xi[1]*(gio[1]+gii[1])+xi[2]*(gio[2]+gii[2]);
		/**/
                /* Mix G for V calc */
		case 0:
		/* Gmix-ex */
		/* wi[1] Di1(1)-Hed1(2) */
		/* Di1(1) */
		gie[1]=(1.0-xi[1])*xi[2]*wi[1];
		/* Hed1(2) */
		gie[2]=(1.0-xi[2])*xi[1]*wi[1];
                /**/
		/* Add Integral energy */
		ival+=xi[1]*gie[1]+xi[2]*gie[2];
		return ival;
                /**/
		case 2:
		/* New xi[] calc */
		/* Di1=Hed1 */
	        z1=(gio[1]+gie[1]+gii[1])-rt*log(xi[1]);
	        z2=(gio[2]+gie[2]+gii[2])-rt*log(xi[2]);
		z3=MINV(z1,z2); 
	        z1=(z3-z1)/rt; if(z1<gimin) z1=gimin; z1=exp(z1);
	        z2=(z3-z2)/rt; if(z2<gimin) z2=gimin; z2=exp(z2);
	        z3=z1+z2;
		xi2[1]=z1/z3;
		xi2[2]=1.0-xi2[1];
		return 0;
                /**/
                /**/
                /* Stand G calc */
		case -1:
	        ival=gstand(0);
/*
if(tkcur<850.0) ival+=1000.0;
*/
	        return ival;
                /* Stand G for V calc */
		case -2:
	        return gstand(1);
                }
	/* End MODEL 69 - CPX1 Di1(1)-Hd1(2) */

	/* MODEL 70 - CPX Di(1)-Hed(2)-Cats(3) */
	case 70:
	switch (min)
        	{
                /* Mix G calc */
		case 1:
		/* Gmix-id */
		/* Di(1) */
		gii[1]=rt*log(xi[1]);
		/* Hed(2) */
		gii[2]=rt*log(xi[2]);
		/* Cats(3) */
		gii[3]=rt*log(xi[3]);
                /**/
		/* Calc Integral energy */
		ival=xi[1]*(gio[1]+gii[1])+xi[2]*(gio[2]+gii[2])+xi[3]*(gio[3]+gii[3]);
		/**/
                /* Mix G for V calc */
		case 0:
		/* Gmix-ex */
		/* wi[1] Di(1)-Hed(2) */
		/* Di(1) */
		gie[1]=(1.0-xi[1])*xi[2]*wi[1];
		/* Hed(2) */
		gie[2]=(1.0-xi[2])*xi[1]*wi[1];
		/* Cats(3) */
		gie[3]=-xi[1]*xi[2]*wi[1];
                /**/
		/* Add Integral energy */
		ival+=xi[1]*gie[1]+xi[2]*gie[2]+xi[3]*gie[3];
		return ival;
                /**/
		case 2:
		/* New xi[] calc */
		/* Di=Hed=Cats */
	        z1=(gio[1]+gie[1]+gii[1])-rt*log(xi[1]);
	        z2=(gio[2]+gie[2]+gii[2])-rt*log(xi[2]);
	        z3=(gio[3]+gie[3]+gii[3])-rt*log(xi[3]);
		z4=MINV(z1,z2); z4=MINV(z4,z3);
	        z1=(z4-z1)/rt; if(z1<gimin) z1=gimin; z1=exp(z1);
	        z2=(z4-z2)/rt; if(z2<gimin) z2=gimin; z2=exp(z2);
	        z3=(z4-z3)/rt; if(z3<gimin) z3=gimin; z3=exp(z3);
	        z4=z1+z2+z3;
		xi2[1]=z1/z4;
		xi2[2]=z2/z4;
		xi2[3]=1.0-xi2[1]-xi2[2];
		return 0;
                /**/
                /**/
                /* Stand G calc */
		case -1:
	        ival=gstand(0);
/*
if(tkcur<850.0) ival+=1000.0;
*/
	        return ival;
                /* Stand G for V calc */
		case -2:
	        return gstand(1);
                }
	/* End MODEL 70 - CPX Di(1)-Hd(2)-Cats(3) */
	

	/* MODEL 71 - OMPH Di(1)-Hed(2)-Jd(3) */
	case 71:
	switch (min)
        	{
                /* Mix G calc */
		case 1:
		/* Gmix-id */
		/* Model Variables */
		/* Di(1) */
		gii[1]=rt*log(xi[1]);
		/* Hed(2) */
		gii[2]=rt*log(xi[2]);
		/* Jd(3) */
		gii[3]=rt*log(xi[3]);
                /**/
		/* Calc Integral energy */
		ival=xi[1]*(gio[1]+gii[1])+xi[2]*(gio[2]+gii[2])+xi[3]*(gio[3]+gii[3]);
		/**/
                /* Mix G for V calc */
		case 0:
		/* Gmix-ex */
		/* wi[1] Di(1)-Hed(2) */
		/* wi[2] Di(1)-Jd(3) */
		/* wi[3] Di(2)-Jd(3) */
		/* Di(1) */
		gie[1]=(1.0-xi[1])*xi[2]*wi[1]+(1.0-xi[1])*xi[3]*wi[2]-xi[2]*xi[3]*wi[3];
		/* Hed(2) */
		gie[2]=(1.0-xi[2])*xi[1]*wi[1]-xi[1]*xi[3]*wi[2]+(1.0-xi[2])*xi[3]*wi[3];
		/* Jd(3) */
		gie[3]=-xi[2]*xi[1]*wi[1]+(1.0-xi[3])*xi[1]*wi[2]+(1.0-xi[3])*xi[2]*wi[3];
                /**/
		/* Add Integral energy */
		ival+=xi[1]*gie[1]+xi[2]*gie[2]+xi[3]*gie[3];
		return ival;
                /**/
		case 2:
		/* New xi[] calc */
		/* x1 Di=Hed=Jd */
	        z1=(gio[1]+gie[1]+gii[1])-rt*log(xi[1]);
	        z2=(gio[2]+gie[2]+gii[2])-rt*log(xi[2]);
	        z3=(gio[3]+gie[3]+gii[3])-rt*log(xi[3]);
		z4=MINV(z1,z2); z4=MINV(z4,z3);
	        z1=(z4-z1)/rt; if(z1<gimin) z1=gimin; z1=exp(z1);
	        z2=(z4-z2)/rt; if(z2<gimin) z2=gimin; z2=exp(z2);
	        z3=(z4-z3)/rt; if(z3<gimin) z3=gimin; z3=exp(z3);
	        z4=z1+z2+z3;
		xi2[1]=z1/z4;
		xi2[2]=z2/z4;
		xi2[3]=1.0-xi2[1]-xi2[2];
		return 0;
                /**/
                /**/
                /* Stand G calc */
		case -1:
	        ival=gstand(0);
/*
if(tkcur<850.0) ival+=1000.0;
*/
	        return ival;
                /* Stand G for V calc */
		case -2:
	        return gstand(1);
                }
	/* End MODEL 71 - OMPH Di(1)-Hed(2)-Jd(3) */

	/* MODEL 72 - OMPH Di(1)-Hed(2)-Jd(3) */
	case 72:
	switch (min)
        	{
                /* Mix G calc */
		case 1:
		/* Gmix-id */
		/* Model Variables */
		/* Di(1) */
		gii[1]=rt*log(xi[1]);
		/* Hed(2) */
		gii[2]=rt*log(xi[2]);
		/* Jd(3) */
		gii[3]=rt*log(xi[3]);
                /**/
		/* Calc Integral energy */
		ival=xi[1]*(gio[1]+gii[1])+xi[2]*(gio[2]+gii[2])+xi[3]*(gio[3]+gii[3]);
		/**/
                /* Mix G for V calc */
		case 0:
		/* Gmix-ex */
		/* Darken quadratic formalism */
		/* wi[1] Di(1) = 1.5 kJ/mol */
		/* wi[2] Hed(2) = 1.5 kJ/mol */
		/* wi[3] Jd(3) = 1.5 kJ/mol */
		/* Di(1) */
		gie[1]=wi[1];
		/* Hed(2) */
		gie[2]=wi[2];
		/* Jd(3) */
		gie[3]=wi[3];
                /**/
		/* Add Integral energy */
		ival+=xi[1]*gie[1]+xi[2]*gie[2]+xi[3]*gie[3];
		return ival;
                /**/
		case 2:
		/* New xi[] calc */
		/* x1 Di=Hed=Jd */
	        z1=(gio[1]+gie[1]+gii[1])-rt*log(xi[1]);
	        z2=(gio[2]+gie[2]+gii[2])-rt*log(xi[2]);
	        z3=(gio[3]+gie[3]+gii[3])-rt*log(xi[3]);
		z4=MINV(z1,z2); z4=MINV(z4,z3);
	        z1=(z4-z1)/rt; if(z1<gimin) z1=gimin; z1=exp(z1);
	        z2=(z4-z2)/rt; if(z2<gimin) z2=gimin; z2=exp(z2);
	        z3=(z4-z3)/rt; if(z3<gimin) z3=gimin; z3=exp(z3);
	        z4=z1+z2+z3;
		xi2[1]=z1/z4;
		xi2[2]=z2/z4;
		xi2[3]=1.0-xi2[1]-xi2[2];
		return 0;
                /**/
                /**/
                /* Stand G calc */
		case -1:
	        ival=gstand(0);
/*
if(tkcur<850.0) ival+=1000.0;
*/
	        return ival;
                /* Stand G for V calc */
		case -2:
	        return gstand(1);
                }
	/* End MODEL 72 - OMPH Di(1)-Hed(2)-Jd(3) */

	/* MODEL 73 - OMPH Di(1)-Hed(2)-Jd(3) */
	case 73:
	switch (min)
        	{
                /* Mix G calc */
		case 1:
		/* Gmix-id */
		/* Model Variables */
		/* Di(1) */
		gii[1]=rt*log(xi[1]);
		/* Hed(2) */
		gii[2]=rt*log(xi[2]);
		/* Jd(3) */
		gii[3]=rt*log(xi[3]);
                /**/
		/* Calc Integral energy */
		ival=xi[1]*(gio[1]+gii[1])+xi[2]*(gio[2]+gii[2])+xi[3]*(gio[3]+gii[3]);
		/**/
                /* Mix G for V calc */
		case 0:
		/* Gmix-ex */
		/* wi[1] Di(1)-Hed(2) */
		/* wi[2] Di(1)-Jd(3) */
		/* wi[3] Di(2)-Jd(3) */
		/* wi[4] Darken Parameter = 1.3 kJ/mol */
		/* Di(1) */
		gie[1]=((1.0-xi[1])*xi[2]*wi[1]+(1.0-xi[1])*xi[3]*wi[2]-xi[2]*xi[3]*wi[3])+wi[4];
		/* Hed(2) */
		gie[2]=((1.0-xi[2])*xi[1]*wi[1]-xi[1]*xi[3]*wi[2]+(1.0-xi[2])*xi[3]*wi[3])+wi[4];
		/* Jd(3) */
		gie[3]=(-xi[2]*xi[1]*wi[1]+(1.0-xi[3])*xi[1]*wi[2]+(1.0-xi[3])*xi[2]*wi[3])+wi[4];
                /**/
		/* Add Integral energy */
		ival+=xi[1]*gie[1]+xi[2]*gie[2]+xi[3]*gie[3];
		return ival;
                /**/
		case 2:
		/* New xi[] calc */
		/* x1 Di=Hed=Jd */
	        z1=(gio[1]+gie[1]+gii[1])-rt*log(xi[1]);
	        z2=(gio[2]+gie[2]+gii[2])-rt*log(xi[2]);
	        z3=(gio[3]+gie[3]+gii[3])-rt*log(xi[3]);
		z4=MINV(z1,z2); z4=MINV(z4,z3);
	        z1=(z4-z1)/rt; if(z1<gimin) z1=gimin; z1=exp(z1);
	        z2=(z4-z2)/rt; if(z2<gimin) z2=gimin; z2=exp(z2);
	        z3=(z4-z3)/rt; if(z3<gimin) z3=gimin; z3=exp(z3);
	        z4=z1+z2+z3;
		xi2[1]=z1/z4;
		xi2[2]=z2/z4;
		xi2[3]=1.0-xi2[1]-xi2[2];
		return 0;
                /**/
                /**/
                /* Stand G calc */
		case -1:
	        ival=gstand(0);
	        return ival;
                /* Stand G for V calc */
		case -2:
	        return gstand(1);
                }
	/* End MODEL 73 - OMPH Di(1)-Hed(2)-Jd(3) */

	/* MODEL 74 - OMPH Jd(1)-Di(2)-Hed(3)-Omp(4) */
	case 74:
	switch (min)
        	{
                /* Mix G calc */
		case 1:
		/* Jd(1)-Di(2)-Hed(3)-Omp(4) site ideal mixing */
		/* Site variables */
		/* M2 */
                xnam2a=xi[1]+xi[4];
                xnam2b=xi[1];
                xcam2a=1.0-xnam2a;
                xcam2b=1.0-xnam2b;
		/* M1 */
                xalm1a=xi[1]+xi[4];
                xalm1b=xi[1];
                xfe=xi[3]/(1.0-xi[1]-0.5*xi[4]);
                xfem1a=(1.0-xalm1a)*xfe;
                xfem1b=(1.0-xalm1b)*xfe;
                xmgm1a=1.0-xalm1a-xfem1a;
                xmgm1b=1.0-xalm1b-xfem1b;
		/* Check site variables */
		if(xnam2a<=0 || xnam2b<=0 || xcam2a<=0 || xcam2b<=0 || xalm1a<=0 || xalm1b<=0 || xfem1a<=0 || xfem1b<=0 || xmgm1a<=0 || xmgm1b<=0)
                    {
                    gii[1]=0;
                    return 0;
                    }
		/* Jd(1) */
                gii[1]=rt*0.5*log(xnam2a*xnam2b*xalm1a*xalm1b);
		/* Di(2) */
		gii[2]=rt*0.5*log(xcam2a*xcam2b*xmgm1a*xmgm1b);
		/* Hed(3) */
		gii[3]=rt*0.5*log(xcam2a*xcam2b*xfem1a*xfem1b);
		/* Omp(4) */
		gii[4]=rt*0.5*log(xnam2a*xcam2b*xalm1a*xmgm1b);
                /**/
		/* Calc Integral energy */
		ival=xi[1]*(gio[1]+gii[1])+xi[2]*(gio[2]+gii[2])+xi[3]*(gio[3]+gii[3])+xi[4]*(gio[4]+gii[4]);
		/**/
                /* Mix G for V calc */
		case 0:
		/* Gmix-ex */
		/* wi[1] Jd(1)-Di(2) */
		/* wi[2] Jd(1)-Hed(3) */
		/* wi[3] Jd(1)-Omp(4) */
		/* wi[4] Di(2)-Hed(3) */
                /* wi[5] Di(2)-Omp(4) */
                /* wi[6] Hed(3)-Omp(4)*/
                /* wi[7] DQF Omp(4)   */
                /* wi[8] DQF Jd(1)    */
                /* wi[9] DQF Di(2)    */
                /* wi[10] DQF Hed(3)  */
		/* Jd(1) */
		gie[1]=((1.0-xi[1])*xi[2]*wi[1]+(1.0-xi[1])*xi[3]*wi[2]+(1.0-xi[1])*xi[4]*wi[3]-xi[2]*xi[3]*wi[4]-xi[2]*xi[4]*wi[5]-xi[3]*xi[4]*wi[6])+wi[8];
		/* Di(2) */
		gie[2]=((1.0-xi[2])*xi[1]*wi[1]-xi[1]*xi[3]*wi[2]-xi[1]*xi[4]*wi[3]+(1.0-xi[2])*xi[3]*wi[4]+(1.0-xi[2])*xi[4]*wi[5]-xi[3]*xi[4]*wi[6])+wi[9];
		/* Hed(3) */
		gie[3]=(-xi[1]*xi[2]*wi[1]+(1.0-xi[3])*xi[1]*wi[2]-xi[1]*xi[4]*wi[3]+(1.0-xi[3])*xi[2]*wi[4]-xi[2]*xi[4]*wi[5]+(1.0-xi[3])*xi[4]*wi[6])+wi[10];
		/* Omp(4) */
		gie[4]=(-xi[1]*xi[2]*wi[1]-xi[1]*xi[3]*wi[2]+(1.0-xi[4])*xi[1]*wi[3]-xi[2]*xi[3]*wi[4]+(1.0-xi[4])*xi[2]*wi[5]+(1.0-xi[4])*xi[3]*wi[6])+wi[7];
                /**/
		/* Add Integral energy */
		ival+=xi[1]*gie[1]+xi[2]*gie[2]+xi[3]*gie[3]+xi[4]*gie[4];
		return ival;
                /**/
		case 2:
		/* Save mol fractions */
		/* x1 Di=Hed=Jd */
                xi2[1]=xi[1];
                xi2[2]=xi[2];
                xi2[3]=xi[3];
                xi2[4]=xi[4];
                /* Compositional variables x=Fe/(Fe+Mg), y=XNaM2a, z=XNaM2b */
                x=xi[3]/(1.0-(xi[1]+0.5*xi[4]));
                y=xi[1]+xi[4];
                z=xi[1];
                /* Integral Gibbs Energy calc */
                ival=models(74,1);
                /* New site variabeles calc */
                /* x -------------------------------------------------------------------------------------------------------------------- */
                /* Increment Calc */
                dx=(0.5-ABSV(0.5-x))*1e-3;
                /* Differentiation */
                x1=x-dx;
                xi[3]=x1*(1.0-0.5*(y+z));
                xi[2]=1.0-xi[1]-xi[3]-xi[4];
                k1=-models(74,1);
                x1=x+dx;
                xi[3]=x1*(1.0-0.5*(y+z));
                xi[2]=1.0-xi[1]-xi[3]-xi[4];
                k1+=models(74,1);
                /* Restore variables */
                xi[3]=xi2[3];
                xi[2]=xi2[2];
                /* Partial Energy of x and (1-x) calc */
                k1=ival+(1.0-x)*k1/2.0/dx;
                k2=(ival-x*k1)/(1.0-x);
                  k1=k1-rt*(1.0-0.5*(y+z))*log(x);
                  k2=k2-rt*(1.0-0.5*(y+z))*log(1.0-x);
                k3=MINV(k1,k2);
                  k1=(k3-k1)/(1.0-0.5*(y+z))/rt; if(k1<gimin) k1=gimin; k1=exp(k1);
                  k2=(k3-k2)/(1.0-0.5*(y+z))/rt; if(k2<gimin) k2=gimin; k2=exp(k2);
                x1=k1/(k1+k2);
                /* y ------------------------------------------------------------------------------------------------------------------- */
                /* Increment Calc */
                dx=(0.5-ABSV(0.5-y))*1e-3;
                /* Differentiation */
                y1=y-dx;
                xi[3]=x*(1.0-0.5*(y1+z));
                xi[4]=y1-z;
                xi[2]=1.0-xi[1]-xi[3]-xi[4];
                k1=-models(74,1);
                y1=y+dx;
                xi[3]=x*(1.0-0.5*(y1+z));
                xi[4]=y1-z;
                xi[2]=1.0-xi[1]-xi[3]-xi[4];
                k1+=models(74,1);
                /* Restore variables */
                xi[3]=xi2[3];
                xi[4]=xi2[4];
                xi[2]=xi2[2];
                /* Partial Energy of y and (1-y) calc */
                k1=ival+(1.0-y)*k1/2.0/dx;
                k2=(ival-y*k1)/(1.0-y);
                  k1=k1-rt*log(y);
                  k2=k2-rt*log(1.0-y);
                k3=MINV(k1,k2);
                  k1=(k3-k1)/rt; if(k1<gimin) k1=gimin; k1=exp(k1);
                  k2=(k3-k2)/rt; if(k2<gimin) k2=gimin; k2=exp(k2);
                y1=k1/(k1+k2);
                /* z ---------------------------------------------------------------------------------------------------------------------- */
                /* Increment Calc */
                dx=(0.5-ABSV(0.5-z))*1e-3;
                /* Differentiation */
                z1=z-dx;
                xi[3]=x*(1.0-0.5*(y+z1));
                xi[1]=z1;
                xi[4]=y-z1;
                xi[2]=1.0-xi[1]-xi[3]-xi[4];
                k1=-models(74,1);
                z1=z+dx;
                xi[3]=x*(1.0-0.5*(y+z1));
                xi[1]=z1;
                xi[4]=y-z1;
                xi[2]=1.0-xi[1]-xi[3]-xi[4];
                k1+=models(74,1);
                /* Restore variables */
                xi[3]=xi2[3];
                xi[1]=xi2[1];
                xi[4]=xi2[4];
                xi[2]=xi2[2];
                /* Partial Energy of z and (1-z) calc */
                k1=ival+(1.0-z)*k1/2.0/dx;
                k2=(ival-z*k1)/(1.0-z);
                  k1=k1-rt*log(z);
                  k2=k2-rt*log(1.0-z);
                k3=MINV(k1,k2);
                  k1=(k3-k1)/rt; if(k1<gimin) k1=gimin; k1=exp(k1);
                  k2=(k3-k2)/rt; if(k2<gimin) k2=gimin; k2=exp(k2);
                z1=k1/(k1+k2);
                /* New xi[] calc -------------------------------------------------------------------------------------------------------- */
                xi2[3]=x1*(1.0-0.5*(y1+z1));
                xi2[1]=z1;
                xi2[4]=y1-z1;
                xi2[2]=1.0-xi2[1]-xi2[3]-xi2[4];
                   return 0;
                   /**/
                   /* Stand G calc */
                case -1:
                   return gstand (0);
                   /* Stand G for V calc */
                case -2:
                   return gstand (1);
                }
	/* End MODEL 74 - OMPH Jd(1)-Di(2)-Hed(3)-Omp(4) */

	/* MODEL 85 - MU Mu(1)-Cel(2)-Fcel(3)-Pa(4) */
	case 85:
	switch (min)
        	{
                /* Mix G calc */
		case 1:
		/* Mu(1)-Cel(2)-Fcel(3)-Pa(4)  site ideal mixing */
		/* Site variables */
		/* A */
		xnaa=xi[4];
		xka=1.0-xnaa;
		/* M2A */
		xmgm2=xi[2];
		xfem2=xi[3];
		xalm2=1.0-xmgm2-xfem2;
		/* T1 */
		xalt1=0.5*(xi[1]+xi[4]);
		xsit1=1.0-xalt1;
		/* Mu(1)*/
		/* Check site variables */
		if(xnaa<=0 || xka<=0 || xalm2<=0 || xfem2<=0 || xmgm2<=0 || xalt1<=0 || xsit1<=0) 
			{
			gii[1]=0;
			return 0;
			}
		/* Mu(1)*/
		gii[1]=rt*log(4.0*xka*xalm2*xalt1*xsit1);
		/* Cel(2)*/
		gii[2]=rt*log(xka*xmgm2*xsit1*xsit1);
		/* Fcel(3)*/
		gii[3]=rt*log(xka*xfem2*xsit1*xsit1);
		/* Pa(4)*/
		gii[4]=rt*log(4.0*xnaa*xalm2*xalt1*xsit1);
                /**/
		/* Calc Integral energy */
		ival=xi[1]*(gio[1]+gii[1])+xi[2]*(gio[2]+gii[2])+xi[3]*(gio[3]+gii[3])+xi[4]*(gio[4]+gii[4]);
		/**/
                /* Mix G for V calc */
		case 0:
		/* Regular mixing */
		/* wi[1] Mu(1)-Pa(4) */
		/* wi[2] Cel(2)-Pa(4) */
		/* wi[3] Fcel(3)-Pa(4) */
		/* wi[4] WG for fictive Pa(4) End member in Ms */
		/* Mu(1) */
		gie[1]=(1.0-xi[1])*xi[4]*wi[1]-xi[2]*xi[4]*wi[2]-xi[3]*xi[4]*wi[3];
		/* Cel(2) */
		gie[2]=-xi[1]*xi[4]*wi[1]+(1.0-xi[2])*xi[4]*wi[2]-xi[3]*xi[4]*wi[3];
		/* Fcel(3) */
		gie[3]=-xi[1]*xi[4]*wi[1]-xi[2]*xi[4]*wi[2]+(1.0-xi[3])*xi[4]*wi[3];
		/* Pa(4) */
		w=wi[4];
/*
		if(xi[4]>0.3) w=wi[4]*(1.0-(xi[4]-0.3)/0.4);
		if(xi[4]>0.7) w=0;
*/
		gie[4]=(1.0-xi[4])*xi[1]*wi[1]+(1.0-xi[4])*xi[2]*wi[2]+(1.0-xi[4])*xi[3]*wi[3]+w;
                /**/
		/* Add Integral energy */
		ival+=xi[1]*gie[1]+xi[2]*gie[2]+xi[3]*gie[3]+xi[4]*gie[4];
		return ival;
                /**/
		case 2:
		/* Save mol fractions */
		xi2[1]=xi[1];
		xi2[2]=xi[2];
		xi2[3]=xi[3];
		xi2[4]=xi[4];
		/* Compositional variables x=XnaA, y=XalM2A, z=Fe/(Fe+Mg) */
		x=xi[4];
		y=xi[4]+xi[1];
		z=xi[3]/(xi[2]+xi[3]);
		/* Integral Gibbs Energy calc */
		ival=models(85,1);
		/* New site variables calc */
		/* x  ----------------------------------*/
		/* Increment Calc */
		dx=(0.5-ABSV(0.5-x))*1e-3;
		/* Differentiation */
		x1=x-dx;
		xi[4]=x1;
		xi[1]=1.0-xi[2]-xi[3]-xi[4];
		k1=-models(85,1);
		x1=x+dx;
		xi[4]=x1;
		xi[1]=1.0-xi[2]-xi[3]-xi[4];
		k1+=models(85,1);
		/* Restore variables */
		xi[4]=xi2[4];
		xi[1]=xi2[1];
		/* Partial Energy of x and (1-x) calc */
		k1=ival+(1.0-x)*k1/2.0/dx;
		k2=(ival-x*k1)/(1.0-x);
	        k1=k1-rt*log(x);
	        k2=k2-rt*log(1.0-x);
		k3=MINV(k1,k2);
	        k1=(k3-k1)/rt; if(k1<gimin) k1=gimin; k1=exp(k1);
	        k2=(k3-k2)/rt; if(k2<gimin) k2=gimin; k2=exp(k2);
		x1=k1/(k1+k2);
		/* y ----------------------------------*/
		/* Increment Calc */
		dx=(0.5-ABSV(0.5-y))*1e-3;
		/* Differentiation */
		y1=y-dx;
		xi[3]=(1.0-y1)*z;
		xi[2]=(1.0-y1)*(1.0-z);
		xi[1]=1.0-xi[2]-xi[3]-xi[4];
		k1=-models(85,1);
		y1=y+dx;
		xi[3]=(1.0-y1)*z;
		xi[2]=(1.0-y1)*(1.0-z);
		xi[1]=1.0-xi[2]-xi[3]-xi[4];
		k1+=models(85,1);
		/* Restore variables */
		xi[3]=xi2[3];
		xi[2]=xi2[2];
		xi[1]=xi2[1];
		/* Partial Energy of y and (1-y) calc */
		k1=ival+(1.0-y)*k1/2.0/dx;
		k2=(ival-y*k1)/(1.0-y);
	        k1=k1-rt*log(y);
	        k2=k2-rt*log(1.0-y);
		k3=MINV(k1,k2);
	        k1=(k3-k1)/rt; if(k1<gimin) k1=gimin; k1=exp(k1);
	        k2=(k3-k2)/rt; if(k2<gimin) k2=gimin; k2=exp(k2);
		y1=k1/(k1+k2);
		/* z ----------------------------------*/
		/* Increment Calc */
		dx=(0.5-ABSV(0.5-z))*1e-3;
		/* Differentiation */
		z1=z-dx;
		xi[3]=(1.0-y)*z1;
		xi[2]=(1.0-y)*(1.0-z1);
		xi[1]=1.0-xi[2]-xi[3]-xi[4];
		k1=-models(85,1);
		z1=z+dx;
		xi[3]=(1.0-y)*z1;
		xi[2]=(1.0-y)*(1.0-z1);
		xi[1]=1.0-xi[2]-xi[3]-xi[4];
		k1+=models(85,1);
		/* Restore variables */
		xi[3]=xi2[3];
		xi[2]=xi2[2];
		xi[1]=xi2[1];
		/* Partial Energy of z and (1-z) calc */
		k1=ival+(1.0-z)*k1/2.0/dx;
		k2=(ival-z*k1)/(1.0-z);
	        k1=k1-rt*(1.0-y)*log(z);
	        k2=k2-rt*(1.0-y)*log(1.0-z);
		k3=MINV(k1,k2);
	        k1=(k3-k1)/(1.0-y)/rt; if(k1<gimin) k1=gimin; k1=exp(k1);
	        k2=(k3-k2)/(1.0-y)/rt; if(k2<gimin) k2=gimin; k2=exp(k2);
		z1=k1/(k1+k2);
		/* New xi[] calc ---------------------------*/
		xi2[4]=x1;
		xi2[3]=(1.0-y1)*z1;
		xi2[2]=(1.0-y1)*(1.0-z1);
		xi2[1]=1.0-xi2[2]-xi2[3]-xi2[4];
	        return 0;
		/**/
	        if(xi[1]>0.5) 
			{
			z3=(gio[1]+gie[1]+gii[1])-rt*log(xalm2);
			}
		else
			{
	        	z3=(gio[4]+gie[4]+gii[4])-rt*log(xalm2);
			}
                /* Stand G calc */
		case -1:
	        return gstand(0);
                /* Stand G for V calc */
		case -2:
	        return gstand(1);
                }
	/* End MODEL 85 - MU Mu(1)-Cel(2)-Fcel(3)-Pa(4) */


	/* MODEL 86 - PA Pa1(1)-Ma1(2) */
	case 86:
	switch (min)
        	{
                /* Mix G calc */
		case 1:
		/* Gmix-id */
		/* Pa1(1) */
		gii[1]=rt*log(xi[1]);
		/* Ma1(2) */
		gii[2]=rt*log(xi[2]);
                /**/
		/* Calc Integral energy */
		ival=xi[1]*(gio[1]+gii[1])+xi[2]*(gio[2]+gii[2]);
		/**/
                /* Mix G for V calc */
		case 0:
		/* Gmix-ex */
		/* wi[1] Mu(1)-Pa1(2) */
		/* Pa1(1) */
		gie[1]=(1.0-xi[1])*xi[2]*wi[1];
		/* Ma1(2) */
		gie[2]=(1.0-xi[2])*xi[1]*wi[1];
                /**/
		/* Add Integral energy */
		ival+=xi[1]*gie[1]+xi[2]*gie[2];
		return ival;
                /**/
		case 2:
		/* New xi[] calc */
		/* Ma1(2) Pa1=Ma1 */
	        z1=((gio[1]+gie[1])-(gio[2]+gie[2]))/rt;
	        if(z1<gimin) z1=gimin;
	        if(z1>gimax) z1=gimax;
	        z1=exp(z1);
		xi2[2]=z1/(1.0+z1);
		/* Pa1(1) 1-Ma1 */
		xi2[1]=1.0-xi2[2];
		return 0;
                /**/
                /* Stand G calc */
		case -1:
	        return gstand(0);
                /* Stand G for V calc */
		case -2:
	        return gstand(1);
                }
	/* End MODEL 86 - PA Pa1(1)-Ma1(2) */


	/* MODEL 87 - MU Mu(1)-Cel(2)-Fcel(3) */
	case 87:
	switch (min)
        	{
                /* Mix G calc */
		case 1:
		/* Mu(1)-Cel(2)-Fcel(3)  site ideal mixing */
		/* Site variables */
		/* M2A */
		xmgm2=xi[2];
		xfem2=xi[3];
		xalm2=1.0-xmgm2-xfem2;
		/* T1 */
		xalt1=0.5*xi[1];
		xsit1=1.0-xalt1;
		/* Mu(1)*/
		/* Check site variables */
		if(xalm2<=0 || xfem2<=0 || xmgm2<=0 || xalt1<=0 || xsit1<=0) 
			{
			gii[1]=0;
			return 0;
			}
		/* Mu(1)*/
		gii[1]=rt*log(4.0*xalm2*xalt1*xsit1);
		/* Cel(2)*/
		gii[2]=rt*log(xmgm2*xsit1*xsit1);
		/* Fcel(3)*/
		gii[3]=rt*log(xfem2*xsit1*xsit1);
                /**/
		/* Calc Integral energy */
		ival=xi[1]*(gio[1]+gii[1])+xi[2]*(gio[2]+gii[2])+xi[3]*(gio[3]+gii[3]);
		/**/
                /* Mix G for V calc */
		/**/
		/**/
		/**/
                /* Mix G for V calc */
		case 0:
		gie[1]=gie[2]=gie[3]=0;
		return ival;
                /**/
		case 2:
		/* Site variables */
		/* M2A */
		xmgm2=xi[2];
		xfem2=xi[3];
		xalm2=1.0-xmgm2-xfem2;
		/* New site variables calc */
		/* XmgM2,XfeM2 Mu=Cel=Fcel */
	        z1=(gio[2]+gie[2]+gii[2])-rt*log(xmgm2);
	        z2=(gio[3]+gie[3]+gii[3])-rt*log(xfem2);
		z3=(gio[1]+gie[1]+gii[1])-rt*log(xalm2);
		z4=MINV(z1,z2); z4=MINV(z4,z3);
	        z1=(z4-z1)/rt; if(z1<gimin) z1=gimin; z1=exp(z1);
	        z2=(z4-z2)/rt; if(z2<gimin) z2=gimin; z2=exp(z2);
	        z3=(z4-z3)/rt; if(z3<gimin) z3=gimin; z3=exp(z3);
	        z4=z1+z2+z3;
	        xmgm2=z1/z4;
	        xfem2=z2/z4;
		/* New xi[] calc */
		/* Cel(2) */
		xi2[2]=xmgm2;
		/* Fcel(3) */
		xi2[3]=xfem2;
		/* Mu(1) */
		xi2[1]=1.0-xi2[2]-xi2[3];
	        return 0;
		/**/
                /**/
                /* Stand G calc */
		case -1:
	        return gstand(0);
                /* Stand G for V calc */
		case -2:
	        return gstand(1);
                }
	/* End MODEL 87 - Pa Mu(1)-Cel(2)-Fcel(3) */

	/* MODEL 88 - PA Pa1(1)-Ma1(2) */
	case 88:
	switch (min)
        	{
                /* Mix G calc */
		case 1:
		/* Gmix-id */
		/* Pa1(1) */
		gii[1]=rt*log(xi[1]);
		/* Ma1(2) */
		gii[2]=rt*log(xi[2]);
                /**/
		/* Calc Integral energy */
		ival=xi[1]*(gio[1]+gii[1])+xi[2]*(gio[2]+gii[2]);
		/**/
                /* Mix G for V calc */
		case 0:
		/* Gmix-ex */
		/* Darken quadratic formalism */
		/* wi[1] = 12 kJ/mol */
		/* Pa1(1) */
		gie[1]=0;
		/* Ma1(2) */
		gie[2]=wi[1];
                /**/
		/* Add Integral energy */
		ival+=xi[1]*gie[1]+xi[2]*gie[2];
		return ival;
                /**/
		case 2:
		/* New xi[] calc */
		/* Ma1(2) Pa1=Ma1 */
	        z1=((gio[1]+gie[1])-(gio[2]+gie[2]))/rt;
	        if(z1<gimin) z1=gimin;
	        if(z1>gimax) z1=gimax;
	        z1=exp(z1);
		xi2[2]=z1/(1.0+z1);
		/* Pa1(1) 1-Ma1 */
		xi2[1]=1.0-xi2[2];
		return 0;
                /**/
                /* Stand G calc */
		case -1:
	        return gstand(0);
                /* Stand G for V calc */
		case -2:
	        return gstand(1);
                }
	/* End MODEL 88 - PA Pa1(1)-Ma1(2) */


	/* MODEL 90 - PLI An(1)-Ab(2) */
	case 90:
	switch (min)
        	{
                /* Mix G calc */
		case 1:
		/* Gmix-id */
		/* An(1) */
		gii[1]=rt*log(xi[1]);
		/* Ab(2) */
		gii[2]=rt*log(xi[2]);
                /**/
		/* Calc Integral energy */
		ival=xi[1]*(gio[1]+gii[1])+xi[2]*(gio[2]+gii[2]);
		/**/
                /* Mix G for V calc */
		case 0:
		/* Gmix-ex H&P, 1992 */
		xb=0.12+0.00038*vtk;
		if(xi[1]>xb)
			{
			/* I1 An(1) */
			gie[1]=wi[1]*xi[2]*xi[2];
			/* I1 Ab(2) */
			gie[2]=wi[1]*xi[1]*xi[1]-(wi[1]-wi[2])*xb*xb;
			}
		else
			{
			/* C1 An(1) */
			gie[1]=wi[2]*xi[2]*xi[2]+(wi[1]-wi[2])*(1.0-xb)*(1.0-xb);
			/* C1 Ab(2) */
			gie[2]=wi[2]*xi[1]*xi[1];
			}
		/* Add Integral energy */
		ival+=xi[1]*gie[1]+xi[2]*gie[2];
		return ival;
                /**/
		case 2:
		/* New xi[] calc */
		/* Ab(2) An=Ab */
	        z1=((gio[1]+gie[1])-(gio[2]+gie[2]))/rt;
	        if(z1<gimin) z1=gimin;
	        if(z1>gimax) z1=gimax;
	        z1=exp(z1);
		xi2[2]=z1/(1.0+z1);
		/* An(1) 1-Ab */
		xi2[1]=1.0-xi2[2];
		return 0;
                /**/
                /* Stand G calc */
		case -1:
	        return gstand(0);
                /* Stand G for V calc */
		case -2:
	        return gstand(1);
                }
	/* End MODEL 90 - PLI An(1)-Ab(2) */


	/* MODEL 95 - EP Cz(1)-Ep(2)-Fep(3) */
	case 95:
	switch (min)
        	{
                /* Mix G calc */
		case 1:
		/* Cz(1)-Ep(2)-Fep(3) site ideal mixing */
		/* Site variables */
		/* M1 */
		xfem1=xi[3];
		xalm1=1.0-xfem1;
		/* M3 */
		xalm3=xi[1];
		xfem3=1.0-xalm3;
		/* Mu(1)*/
		/* Check site variables */
		if(xalm1<=0 || xfem1<=0 || xalm3<=0 || xfem3<=0) 
			{
			gii[1]=0;
			return 0;
			}
		/* Cz(1) */
		gii[1]=rt*log(xalm1*xalm3);
		/* Ep(2) */
		gii[2]=rt*log(xalm1*xfem3);
		/* Fep(3) */
		gii[3]=rt*log(xfem1*xfem3);
                /**/
		/* Calc Integral energy */
		ival=xi[1]*(gio[1]+gii[1])+xi[2]*(gio[2]+gii[2])+xi[3]*(gio[3]+gii[3]);
		/**/
                /* Mix G for V calc */
		case 0:
		/* wi[1] Cz(1)-Fep(3) */
		/* wi[2] Cz(1)-Ep(2) */
		/* wi[3] Ep(2)-Fep(3) */
		/* Cz(1) */
		gie[1]=(1.0-xi[1])*xi[3]*wi[1]+(1.0-xi[1])*xi[2]*wi[2]-xi[2]*xi[3]*wi[3];
		/* Ep(2) */
		gie[2]=-xi[1]*xi[3]*wi[1]+(1.0-xi[2])*xi[1]*wi[2]+(1.0-xi[2])*xi[3]*wi[3];
		/* Fep(3) */
		gie[3]=(1.0-xi[3])*xi[1]*wi[1]-xi[2]*xi[1]*wi[2]+(1.0-xi[3])*xi[2]*wi[3];
                /**/
		/* Add Integral energy */
		ival+=xi[1]*gie[1]+xi[2]*gie[2]+xi[3]*gie[3];
		return ival;
                /**/
		case 2:
		/* New xi[] calc */
		/* Cz(1) Cz=Ep */
	        z1=((gio[2]+gie[2])-(gio[1]+gie[1]))/rt;
	        if(z1<gimin) z1=gimin;
	        if(z1>gimax) z1=gimax;
	        z1=exp(z1);
		/* Fep(3) Fep=Ep */
	        z2=((gio[2]+gie[2])-(gio[3]+gie[3]))/rt;
	        if(z2<gimin) z2=gimin;
	        if(z2>gimax) z2=gimax;
	        z2=exp(z2);
		/* Ep(2) 1-Fep-Cz */
		xi2[1]=z1/(1.0+z1);
		xi2[3]=z2/(1.0+z2);
		xi2[2]=1.0-xi2[1]-xi2[3];
		return 0;
                /**/
                /* Stand G calc */
		case -1:
	        return gstand(0);
                /* Stand G for V calc */
		case -2:
	        return gstand(1);
                }
	/* End MODEL 95 - EP Cz(1)-Ep(2)-Fep(3) */



	/* ERROR - model not exist */
	default:
	printf("\n Undescripted model <%d>\n",mdl);
	/*t3stop();*/

	}
}
/* End X, A OF MINALS CALCULATION MODELS */

