/* SERCH W NUM IN dwww[] */
int nserch(int m, int n)
/*  m - Model num */
/*  n - W num */
{
/* Counter */
int m1;
/* Compare W num cycle */
for (m1=0;m1<dmod[m][3];m1++)
if (dwww[dmod[m][4]+m1][0]==n)
return m1;
printf("\n <%dW> absent in <%s> model description\n",n,&mdnm[m][0]);
exit(0);
return 0;
}
/* End  SERCH W NUM IN dwww[] */



/* SERCH WORD IN WORDS ARRAYS */
int wserch(char *aa, int al, int an, char *aw, char *note)
/* *aa - Array adres */
/*  al - Array line Lenth */
/*  an - Array word Number */
/* *aw - Word Adres */
/* *note - Note(if Word not Found)*/
{
/* Counter */
int m1;
/* Compare Words Cycle */
for (m1=0;m1<an;m1++)
if (strcmp(aw,(aa+m1*al))==0)
return m1;
printf("\n<%s> not found in <%s> catalog\n",aw,note);
exit(0);
return 0;
}
/* End SERCH WORD IN WORDS ARRAYS */




/* SPACE CONTROL */
void scontrol(long c1, long c2, char *note)
/* c1,c2 - Compared cises */
/* *note - Note (if c1>=c2) */
{
if (c1>=c2)
	{
	printf("\nSpace out (%ld>=%ld) in <%s> buffer\n",c1,c2,note);
	exit(0);
	}
}
/* End SPACE CONTROL */



/* LOAD WITHOUT EMPTY LINES */
void ffscanf()
{
/* Counter */
int m1;
/**/
/* Read cycle */
do
	{
	/* Input string */
	m1=fscanf(fl,"%s",sa);
	/* Check end of file */
	if (m1<1)
		{
		printf("\n Unexpected end of file\n");
		fclose(fl);
		exit(0);
		}
	/* Delete last symbol <32 */
	for(m1=strlen(sa)-1;m1>=0;m1--)
	if (*(sa+m1)<=32)
	*(sa+m1)=0;
	else
	break;
	}
while (strlen(sa)<1);
}
/* End LOAD WITHOUT EMPTY LINES */


/* ************* End LOAD MODE.T3D ************* */
void loader()
{
/* Counters */
int m1,m2;
long int m3,m4;
int n1,n2,n3,n4,n5;
/* Adreses of string & file names */
char *fa;
/**/
/**/
/**/
/*  ----------------------- READ mode.t3h -------------------------- */
/* Special Counters clear */
nnum=xnum=tnum=0;
printf("Reading mode.t3h ...");
/* Open */
fl = fopen("mode.t3h","rt");
/**/
/**/
/* Read "COMPONENTS_DESCRIPTION"  title */
ffscanf();
/* Read components number */
ffscanf(); cnum=atoi(sa);
scontrol((long int)(cnum-1),MAXCOM,"COMPONENTS NUMBER");
/* Read components names, wt */
for (m1=0;m1<cnum;m1++)
	{
	/* COMP NAME */
	ffscanf(); strcpy(&cpnm[m1][0],sa);
	/* COMP WT */
	ffscanf(); cpwt[m1]=atof(sa);
	}
/**/
/**/
/**/
/* Read "MODELS_DESCRIPTION"  title */
ffscanf();
/**/
/**/
/* Read wpos val */
ffscanf(); wpos=atoi(sa);
scontrol((long int)(wpos-1),MAXWTD,"W TD POSITION");
/**/
/**/
/* Read MODELS description */
pnum=0;
wnum=0;
mcmax=0;
fa=&mdnm[0][0];
do
	{
	/* Read MODEL NUM, NAME, TYPE, XNUM, WNUM pos in dwww[] */
	/* MODEL NUM */
	ffscanf();
	if (*sa!='~')
		{
		/* Check/Save  MOD num */
		dmod[pnum][0]=atoi(sa);
		/* MOD NAME */
		ffscanf(); strcpy(&mdnm[pnum][0],sa);
		/* MOD TYPE XNUM WNUM KOEF */
		for (m2=1;m2<4;m2++)
			{
			ffscanf(); dmod[pnum][m2]=atoi(sa);
			}
		/* Clear type */
		dmod[pnum][1]=0;
		/* First pos in dwww[] Save */
		dmod[pnum][4]=wnum;
		/* Read Model W description */
		n4=0;
		for (m2=0;m2<dmod[pnum][3];m2++)
			{
			/* W num read, Check wi[] Space */
			ffscanf(); dwww[wnum][0]=atoi(sa);
			scontrol((long int)(dwww[wnum][0]-1),MAXWIN,"WI");
			/**/
			/* tnum - max Number of W in model */
			tnum=MAXV(tnum,dwww[wnum][0]);
			/* Read W TD Description */
			n5=0;
			for (m3=2;m3<wpos+2;m3++)
				{
				ffscanf();
				if (*sa=='/')
					{
					/* / - Empty Parameters Note */
					dwww[wnum][m3]=-1;
					}
				else
					{
					if (*sa=='=')
						{
						/* TD Par Equel to Other model */
						/**/
						/* Read other model name */
						ffscanf();
						/* Serch/Check Model name name */
						n1=wserch(fa,LENMOD,pnum+1,sa,"MODEL NAME");
						/**/
						/* Read W line number for other model name */
						ffscanf(); n2=atoi(sa);
						/* Serch/Check W number */
						n3=nserch(n1,n2);
						/**/
						/* Save TD description adres */
						dwww[wnum][m3]=dwww[dmod[n1][4]+n3][m3];
						/* Par Exist for Cur MODEL, W */
						n4=1; n5=1;
						/* End TD Par Equel to Other model */
						}
					else
						{
						/* Independent TD Parameters Read, Save */
						/* Save TD Par num in dwww[] */
						dwww[wnum][m3]=mcmax;
						/* Par Exist for Cur MODEL, W */
						n4=1; n5=1;
						/* Save cur values of TD Par, Stab Par, dA, Amin */
						solut[0][mcmax]=atof(sa);
						/**/
						/* Read, Save other values of TD Par */
						/**/
						/* Increase counter, solut[] space control */
						mcmax++; scontrol((long int)mcmax,MAXMAT,"MODEL SOLUTION");
						/* End Independent TD Parameters Read */
						}
					}
				}
			/* Note Empty W Lines in dwww[] */
			if (n5==0)
			/* Empty */
			dwww[wnum][1]=-1;
			else
			/* Activ */
			dwww[wnum][1]=0;
			/* Incr counter, check dwww[] Space */
			wnum++; scontrol((long int)wnum,MAXWWW,"W DESCRIPTION");
			}
		/* Note Empty MODELS in dmod[] */
		if (n4==0)
		dmod[pnum][4]=-1;
		/* Incr counter, mdnm[] space check */
		pnum++; scontrol((long int)pnum,MAXMOD,"MODEL NAME");
		}
	}
while (*sa!='~');
/**/
/**/
/* Read "MINALS_DESCRIPTION"  title */
ffscanf();
/**/
/**/
/* Read mpos val */
ffscanf(); mpos=atoi(sa);
scontrol((long int)(mpos-1),MAXMTD,"MINAL TD POSITIONS");
/**/
/**/
/* Read MINALS description */
mnum=0;
do
	{
	/* Read Minal NUM, NAME, MODEL, NUM IN MODEL */
	/* MIN NAME */
	ffscanf();
	if (*sa!='~')
		{
		/* MIN NAME Save */
		strcpy(&nmin[mnum][0],sa);
		/* MOD NAME READ*/
		ffscanf();
		/* Mod Name Check, Num Save */
		dmin[mnum][0]=wserch(&mdnm[0][0],LENMOD,pnum,sa,"MODEL NAME");
		/**/
		/* NUM IN MODEL READ */
		ffscanf(); dmin[mnum][1]=atoi(sa);
		/* Check xi[] Space */
		scontrol((long int)(dmin[mnum][1]-1),MAXXIN,"XI");
		/**/
		/* MASS BALANCE Descr for Cur Minal */
		for (m2=0;m2<cnum;m2++)
			{
			ffscanf(); if (*sa=='*') break;
			/* COMP NUM */
			m3=wserch(&cpnm[0][0],LENCOM,cnum,sa,"COMPONENT NAME");
			/* COMP KOEF */
			ffscanf();
			phasmb[cnum+1+mnum][m3]=atof(sa);
			}
		/**/
		/* xnum - max Number of X in model */
		xnum=MAXV(xnum,dmin[mnum][1]);
		/**/
		/* TD Descr for Cur Minal [2..]*/
		for (m2=2;m2<mpos+2;m2++)
			{
			ffscanf();
			if (*sa=='/')
				{
				/* Empty Parameters Note */
				dmin[mnum][m2]=-1;
				}
			else
				{
				if (*sa=='=')
					{
					/* TD Par Equel to Other minal */
					/* Read other minal name */
					ffscanf();
					/* Minal Name Check, Num Save */
					m3=wserch(&nmin[0][0],LENMIN,mnum,sa,"MINAL NAME");
					dmin[mnum][m2]=dmin[m3][m2];
					/* End TD Par Equel to Other minal */
					}
				else
					{
					/* Independent TD Parameters Read, Save */
					/* Save TD Par num in dmin[] */
					dmin[mnum][m2]=mcmax;
						/* Save cur values of TD Par, Stab Par, dA, Amin */
					solut[0][mcmax]=atof(sa);
					/**/
					/* Increase counter, solut[] space control */
					mcmax++; scontrol((long int)mcmax,MAXMAT,"MINAL SOLUTION");
					/* End Independent TD Parameters Read */
					}
				}
			}
		/* Incr counter, Check nmin[] space */
		mnum++; scontrol((long int)mnum,MAXMIN,"MINAL NAME");
		}
	}
while (*sa!='~');
/* Increase mnum mcmax */
/**/
/**/
/**/
/* Read "MINIMUMS_CONFIGURATION"  title */
ffscanf();
/* Read ipos val */
ffscanf(); ipos=atoi(sa);
scontrol((long int)(ipos-1),MAXMDP,"MINIMUMS DESCRIPTION POSITIONS");
/**/
/* Read MINIMUMS description */
inum=0;
do
	{
	/* Read Minimum MODEL, SET OF MINALS */
	/* MOD NAME READ*/
	ffscanf();
	if (*sa!='~')
		{
		/* MOD NAME READ*/
		/* Mod Name Check, Num Save */
		dmnm[inum][0]=wserch(&mdnm[0][0],LENMOD,pnum,sa,"MODEL NAME");
		dmnm[inum+1][0]=dmnm[inum][0];
		/* Note model for SS */
		dmod[dmnm[inum][0]][1]=1;
		/**/
		/* MINALS SET for Cur Minal */
		for (m2=0;m2<ipos;m2++)
			{
			ffscanf(); if (*sa=='*') break;
			/* Minal Name Check, Num Save */
			dmnm[inum][m2+2]=wserch(&nmin[0][0],LENMIN,mnum,sa,"MINAL NAME");
			dmnm[inum][1]++;
			dmnm[inum+1][m2+2]=dmnm[inum][m2+2];
			dmnm[inum+1][1]=dmnm[inum][1];
			/* X VALUE */
			ffscanf();
			phasxx[cnum+inum+1][m2]=phasxx[cnum+inum][m2]=kmnm[inum+1][m2]=kmnm[inum][m2]=atof(sa);
			/* COMPOS OF SS CALC */
			for(m1=0;m1<cnum;m1++)
			phasmb[cnum+1+mnum+inum+1][m1]=(phasmb[cnum+1+mnum+inum][m1]+=kmnm[inum][m2]*phasmb[cnum+1+dmnm[inum][m2+2]][m1]);
			}
		/**/
		/* Incr counter, Check nmin[] space */
		inum+=2; scontrol((long int)inum+1,MAXMNP,"MINIMUMS DESCRIPTION SET");
		}
	}
while (*sa!='~');
/**/
/**/
/* Read "SYSTEM_CONFIGURATION"  title */
ffscanf();
/**/
/* Program Parameters Read/Save/Check */
ffscanf(); printmod=atoi(sa);
ffscanf(); gemin=atof(sa);
ffscanf(); ginit=atof(sa);
ffscanf(); minsol=atof(sa);
ffscanf(); minphas=atof(sa);
ffscanf(); mindiff=atof(sa);
ffscanf(); pbmin=atof(sa)*1000;
ffscanf(); pbmax=atof(sa)*1000;
ffscanf(); pbstp=atof(sa)*1000;
ffscanf(); tkmin=atof(sa)+273.0;
ffscanf(); tkmax=atof(sa)+273.0;
ffscanf(); tkstp=atof(sa);
/**/
/* Read initial composition */
for (m1=0;m1<cnum;m1++)
	{
	/* COMP NAME */
	ffscanf();
	m2=wserch(&cpnm[0][0],LENCOM,cnum,sa,"COMPONENT NAME");
	phaset[m2]=m2;
	phasmb[m2][m2]=1.0;
	/* At Wt of the system calc */
	/* COMP CONCENTR */
	ffscanf(); phascc[m2]=cpcc[m2]=atof(sa);
	satwt+=phascc[m2]*cpwt[m2];
	}
/**/
/**/
/* CLOSE mode.t3d */
fclose(fl);
printf(" OK\n");
/*  ----------------------- End READ mode.t3d -------------------------- */
/**/
/**/
/* Print Arrays state */
/*
printf("\n");
printf("MAXMAT=%d - mcmax=%d\n",MAXMAT,mcmax);
printf("MAXMIN=%d - mnum =%d\n",MAXMIN,mnum);
printf("MAXCOM=%d - cnum =%d\n",MAXCOM,cnum);
printf("MAXMTD=%d - mpos =%d\n",MAXMTD,mpos);
printf("MAXMOD=%d - pnum =%d\n",MAXMOD,pnum);
printf("MAXWWW=%d - wnum =%d\n",MAXWWW,wnum);
printf("MAXWTD=%d - wpos =%d\n",MAXWTD,wpos);
printf("MAXMMM=%d - nnum =%d\n",MAXMMM,nnum);
printf("MAXXIN=%d - xnum =%d\n",MAXXIN,xnum);
printf("MAXWIN=%d - tnum =%d\n",MAXWIN,tnum);
printf("MAXMDP=%d - ipos =%d\n",MAXMDP,ipos);
printf("MAXMNP=%d - ipos =%d\n",MAXMNP,inum);
*/
}
/* ************* End LOAD MODE.T3D ************* */

