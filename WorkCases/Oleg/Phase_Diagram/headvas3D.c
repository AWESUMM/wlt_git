	
/* -----------------------   MAIN PARAMETERS ------------------------ */
/**/
/* Min, Max, Abs */
#define MINV(a,b) ((a)<=(b)? (a):(b))
#define MAXV(a,b) ((a)>=(b)? (a):(b))
#define ABSV(a)   ((a)>=0? (a):-(a))
#define POW2(a)   ((a)*(a))
#define POW3(a)   ((a)*(a)*(a))
/**/
/**/
/* Main  Cises */
#define MAXMAT 10000   /* Max Main Parameters Number */
#define MAXMIN 200    /* Max Minal Number */
#define MAXMDP 10    /* Max Minimums Description Positions */
#define MAXMNP 10000  /* Max Minimums Number */
#define MAXCOM 20    /* Max Components Number */
#define MAXMTD 25     /* Max TD par for one Minal */
#define MAXMOD 200   /* Max Model Number */
#define MAXWWW 1000  /* Max Total W in all Models Number */
#define MAXWTD 10     /* Max TD par for one W */
#define MAXMMM 200    /* Max Calculated VAR Number */
#define MAXXIN 10     /* Max X number in one model */
#define MAXWIN 50    /* Max W number in one model */
#define LENCOM 10  /* COMPONENT name Lenth */
#define LENMIN 10  /* MINAL Name Lenth */
#define LENMOD 10  /* MODEL Name Lenth */
/**/
/**/
#define TR 298.15    /* T K standart */
#define PR 1.0      /* P bar standart */
/**/
/* -----------------------   MAIN PARAMETERS ------------------------ */


/* ---------------------------- MAIN ARRAYS ---------------------- */
/**/
/* solut[]  - Main Solutions */
double  solut[4][MAXMAT];
/**/
/**/
/**/
/* mat1[] - matrix for 1Step minimisation */
/* sol1[] - solutions for 1Step minimisation */
/* buf1[] - G[], F[] buffer for 1Step minimisation */
double mat1[MAXMMM*(MAXMMM+1)],sol1[MAXMMM],buf1[MAXMMM][MAXMMM+1];
/**/
/* dmin[] - Mod[0] Num in Mod[1] TD par adreses in solut[] [2-..] */
/* lmin[] - Minals concentration limits */
/* nmin[] - Minals names */
/* dmnm[] - MINIMUMS descr: Mod[0] Num of minals[1], Minals descr [2-..] */
/* kmnm[] - Initial Minimums components koef */
int dmin[MAXMIN][MAXMTD+2];
char   nmin[MAXMIN][LENMIN];
double lmin[MAXMIN][2];
int dmnm[MAXMNP][MAXMDP+2];
double kmnm[MAXMNP][MAXMDP];
/**/
/**/
/* phaset[] - Phases numbers   */
/* phavec[] - Phases vector    */
/* modvec[] - Models vector    */
/* phasgg[][0,1] - Phases G 0-cur 1-init */
/* phasmb[][] - Phases Compositions */
/* phasxx[][] - Phases xi[] */
/* phascc[] - Phases concentration */
/* phasvv[] - Phases volumes */
/* phashh[] - Phases ethalpies */
/* cpnm[][] - Components Names */
/* cpwt[] - Components AtWt */
/* cpcc[] - Components concentration */
/* mdnm[][] - Models Names */
/* dmod[][n] - Models configuration: */
/*       [0] - model number */
/*       [1] - type: 0-id, 1-reg, 2-RK; */
/*       [2] - X num; */
/*       [3] - W num; */
/*       [4] - first pos in dwww[]; */
/* dwww[][n]  - W numbers[0], activY/N[1] TD parameters adreses[2-4] in solut[] */
/* dwgg[]  - cur Wg values */
/* dkfc[] - koef for minimization in the model (number of mixing atoms) */
int dmod[MAXMOD][5],dwww[MAXWWW][MAXWTD+2];
double dwgg[MAXWWW],dkfc[MAXMOD];
char  mdnm[MAXMOD][LENMOD];
char  cpnm[MAXCOM][LENCOM];
double  cpwt[MAXCOM];
double  cpcc[MAXCOM];
int     phaset[MAXCOM+1+MAXMIN+MAXMNP];
int     phavec[MAXCOM+1+MAXMIN+MAXMNP];
int     modvec[MAXMOD];
int     resvec[MAXCOM];
double  phasgg[MAXCOM+1+MAXMIN+MAXMNP][2];
double  phasmb[MAXCOM+1+MAXMIN+MAXMNP][MAXCOM];
double  phasxx[MAXCOM+MAXMNP][MAXMDP];
double  phascc[MAXCOM];
double  phasvv[MAXCOM];
double  phashh[MAXCOM];
double  systatwt;
/**/
/**/
/**/
/* wi[] - W values */
/* ws[] - par for standard G */
/* xi[] xi1[] xi2[] - X values */
/* gi[],gio[],gii[],gie[] - total,standard,ideal,exeed Gi for Xi values */
/* gi0[] - err  Gi for Xi values */
/* gi1[] - recalc Gi for Xi values */
double wi[MAXWIN], ws[MAXMTD], xi[MAXXIN], xi1[MAXXIN], xi2[MAXXIN], gi[MAXXIN], gi0[MAXXIN], gi1[MAXXIN];
double gio[MAXXIN], gii[MAXXIN], gie[MAXXIN];
/**/
/* sa[] - input string buffer */
char sa[250];
/* ---------------------------- MAIN ARRAYS ---------------------- */






/* ......................  MAIN VARIABLES ..................  */
/* DEKAP.C */
/* krug - Num of cycle of minimization */
/* nras - Num of cycle with OK results */
/* ynload - TD base loaded Y(1)/N(0) */
int nraz,ynload=0;

/* MODE.T3H */
/* mcmax - Number of calculated parameters An */
/* mpos - Number of TD position for one Minal */
/* wpos - Number of TD position for one W */
/* ipos - Number of Minimums positions */
/* pnum - Number of models (phases) */
/* mnum - Number of Minals */
/* cnum - Number of Components */
/* inum - Number of Minimums */
int mcmax,mpos,wpos,ipos,pnum,mnum,cnum,inum;
/**/
/**/
/**/
/* gpart - G[] part of equations (left) */
/* fpart - An*F[] part of equations (right) */
/* gerr=fpart-gpart - Err of Equat */
/* gerrsum - gerr^2/gbrk sum for cur point */
/* aerrsum - An*Fn^2 normalised sum for cur point */
/* aerrnum - num of member in An*Fn^2 normalised sum for cur point */
/* zerr  - NlMNK error for cur point */
/* addmod - mode of adding: No(0) ab2[](1,3) */
double gpart,fpart,gerr,zerr,gerrsum,aerrsum;
int aerrnum,addmod;
/**/
/**/
/* ADRESES OF PROCESSING */
/* krug - Cur circle of processing */
/* adres - Current adres in main circle */
/*       0 - CLEAR MAIN MATRIX */
/*       1 - ADD MAIN MATRIX */
/*       2 - SAVE MAIN MATRIX */
/*       3 - SOLVE MAIN MATRIX */
/*       4 - CALC ERROR */
/*       5 - CHECK ERROR IN STABILISATION CYCLE */
/*       6 - CHANGE STAB PARAMETERS */
/*       7 - SAVE ERROR AFTER STABILISATION CYCLE */
/* fcur - cur data file number */
/* pcur - cur point in data file */
/* ecur - cur equation for point */
int krug,adres,fcur,pcur,ecur;
/**/
/**/
/* Processing regim var */
/* gemin - Gerr min value */
/* ginit - initial G for components */
/* minsol - Min of Minal concentration */
/* minphas - Min of phase concentration for detection */
/* mindiff - Min of Minal concentration defferences for detection different phases */
/* pmin,pmax,pstp,pcur - P limits */
/* tmin,tmax,tstp,tcur - T limits */
/* hwet,rowet,hdry,rodry - wet/dry Ro H */
/* hwet,rowet,hdry,rodry - wet/dry Ro H */
double gemin,ginit,minsol,minphas,mindiff,pbmin,pbmax,pbstp,pbcur,tkmin,tkmax,tkstp,tkcur,h2omin,h2omax,h2ostp,h2ocur;
double hwet,rowet,hdry,rodry;
/**/
/**/
/* cycnum1 - Minimis cycle Num for 1Step minimisation */
/* cycnum2 - Minimis cycle Num for 2Step minimisation */
int cycnum1,cycnum2;
/**/
/**/
/**/
/* Special Counters */
/* mode.t3c */
/* wnum - Number of W */
/* nnum - max Number of calculated VAR */
/* xnum - max Number of X in model */
/* tnum - max Number of W in model */
/* gsys,gsys0 - tottal G of system tnum */
/* satwt - at wt of the system */
int wnum,nnum,xnum,tnum;
double gsys,gsys0;
double  satwt;
/**/
int printmod;
/**/
/* Streem for File Input/Output */
FILE *fl;
/**/
/* ......................  MAIN VARIABLES ..................  */




/* ================== PROTOTYPES OF FUNCTIONS ======================= */
/* ----------- CALLVAS.C --------------- */
/* ptpoint - Calculation of Equilibr for P-T given */
void ptpoint(double *, double *, double *, int *);
/**/
/**/
/**/
/**/
/* ----------- LOHPVAS.C --------------- */
/* loader - Load configuration and data from files */
/* saver  - Save configuration and data to files */
/* wserch - Serch word in word array */
/* nserch - Serch W num in dwww[] */
/* scontrol - Space control in arrays */
/* ffscanf - load without empty lines */
void loader();
void saver();
int wserch(char *, int, int, char *, char *);
int nserch(int, int);
void scontrol(long, long, char *);
void ffscanf();
/**/
/**/
/**/
/**/
/* ----------- GAUSVAS.C --------------- */
/* mat0clear - Clear mat0[rm] */
/* gausmat0 - Change and Solve mat0[rm] by using Gauss method */
void mat0clear(int, double *);
void gausmat0(int, double *, double *);
/**/
/**/
/**/
/**/
/* ----------- MDHPVAS.C --------------- */
/* model - Calculation of X(mole fraction) and a(activity) of minal */
double models(int, int);
/**/
/**/
/**/
/**/
/**/
/* ----------- PROCVAS.C --------------- */
/* gsyscalc - CALC G OF ASSEMBLAGE */
/* wiload - RELOAD wi[] FROM dwgg[] */
/* wiclear - CLEAR wi[] */
/* xiload - LOAD xi[] from phasxx[], kmnm[] */
/* gsscalc - G CALC FOR CUR SS */
/* curgcalc - CALCULATION OF G FOR MINALS & SS */
/* stdgreac() - CALC STEHIOMETRY & dG OF REACTION */
/* curming - MIN G ASSEMBLAGE SELECTION */
/* chbase - CHANGE OF BASE OPERATION */
/* dxcalc - CALCULATION OF dX */
/* gicalc - Gi CALCULATION  SAVE to gi[] */
/* xisave - EXCHANGE xi[] - xi1[] - xi2[] */
/* mingsearch - MIN G COMPOSITION OF CURRENT SS SERCH */
/* resprn - RESULT PRINT */
double gsyscalc();
void wiload(int);
void wiclear(int);
void xiload(int, double *);
double gsscalc(int, int);
void curgcalc();
double stdgreac();
int curming();
void chbase();
double dxcalc(int);
double gicalc(int);
void xisave(int, double *, double *);
void mingserch(int minpos, int ynsave);
void resprn(double, double, double, double);
/* ================== End  PROTOTYPES OF FUNCTIONS ======================= */
