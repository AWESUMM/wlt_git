!$
!!$!****************************************************************************
!!$!* Module for adding Brinkman Penalization terms onto the full NS equations *
!!$!*  -Only valid for single species 10/7/2011                                *
!!$!*  -R is scaled to 1.0
!!$!****************************************************************************
!!$
!!$

MODULE user_case

  USE precision
  USE wlt_vars
  USE wavelet_filters_mod
  USE elliptic_mod
  USE elliptic_vars
  USE wlt_trns_mod
  USE wlt_trns_vars
  USE wlt_trns_util_mod
  USE io_3d_vars
  USE util_mod
  USE util_vars
  USE share_consts
  USE pde
  USE sizes
  USE share_kry
  USE vector_util_mod
  USE field
  USE input_file_reader
  USE parallel
  USE hyperbolic_solver
  USE equations_compressible

  !
  ! case specific variables
  !
  REAL (pr), PROTECTED :: Re, Pra, Sc, Fr, Ma

  REAL (pr), ALLOCATABLE, DIMENSION (:) :: Lxyz
  INTEGER :: n_var_pressure  ! start of pressure in u array
  INTEGER, PRIVATE :: n_var_wallS = 0
  INTEGER :: nmsk,n_util
  LOGICAL ::  saveUtil
  LOGICAL ::  saveWallS 
  REAL (pr) :: Lczn, tfacczn, itfacczn, itczn, tczn, cczn, czn_min, czn_max
  LOGICAL :: modczn

  REAL (pr) :: grav_coeff
  LOGICAL :: dynamic_gravity
  REAL (pr), DIMENSION(20) :: Re_tau_hist, Re_bulk_hist, grav_coeff_hist    !vectors with spatially averaged values for the previous 1000 steps
  REAL (pr) :: Re_tau, Re_bulk, Re_tau_target                                                !spatio-temporal averaged values

  INTEGER :: correct_thermo = -1

  LOGICAL :: use_OS_IC 

  REAL (pr) :: mesh_cluster_param 
  REAL (pr) :: van_driest_damping 

  INTEGER :: hyper_it = 0  ! for temporary hyperbolic solver application - often used to smooth local instabilities
  INTEGER :: hyper_it_limit = 0  ! for temporary hyperbolic solver application - often used to smooth local instabilities
  LOGICAL :: filter_field

  LOGICAL :: add_extract_planes = .FALSE.

CONTAINS

   SUBROUTINE  user_setup_pde ( VERB ) 
    USE variable_mapping
    USE sgs_compressible 
    IMPLICIT NONE
    LOGICAL, OPTIONAL :: VERB         ! print debug info
    INTEGER :: i, idim, l

    IF( sgsmodel_compress .NE. sgsmodel_compress_none ) CALL sgs_compress_setup_pde
 
 
    IF (par_rank.EQ.0) THEN
       PRINT * ,''
       PRINT *, '**********************Setting up PDE*****************'
       PRINT * ,'CASE: Compressible Channel Flow '
       PRINT *, '*****************************************************'
    END IF

    ! Set up compressible equation variables 
    CALL compressible_setup_pde

    IF (imask_obstacle) THEN
        CALL register_var( 'Mask',       integrated=.FALSE.,  adapt=(/.TRUE.,.TRUE./),   interpolate=(/.TRUE.,.TRUE./),   exact=(/.FALSE.,.FALSE./),   saved=.TRUE., req_restart=.TRUE. )
    END IF

    IF ( saveUtil) THEN 
        CALL register_var( 'Utility',       integrated=.FALSE.,  adapt=(/.FALSE.,.FALSE./),   interpolate=(/.TRUE.,.TRUE./),   exact=(/.FALSE.,.FALSE./),   saved=.TRUE., req_restart=.FALSE. )
    END IF

    IF ( saveWallS ) THEN 
        CALL register_var( 'S_12',       integrated=.FALSE.,  adapt=(/.FALSE.,.FALSE./),   interpolate=(/.TRUE.,.TRUE./),   exact=(/.FALSE.,.FALSE./),   saved=.TRUE., req_restart=.FALSE. )
    END IF

    CALL setup_mapping() 
    scaleCoeff = 1.0_pr

    ! Finalize setup of compressible equations 
    CALL compressible_finalize_setup_pde
    IF( sgsmodel_compress .NE. sgsmodel_compress_none ) CALL sgs_compress_finalize_setup_pde


    n_var_pressure  = n_prs  ! redundant, but required by code.  Unused in this module
    IF(hypermodel .NE. 0 ) THEN
      n_var_mom_hyper(1:dim) = n_mom(1:dim)
      n_var_den_hyper = n_den
    END IF

    nmsk = get_index('Mask') 
    n_util = get_index('Utility') 
    n_var_wallS = get_index('S_12') 
    
    IF(hypermodel .NE. 0) THEN
       IF(ALLOCATED(n_var_hyper)) DEALLOCATE(n_var_hyper)
       ALLOCATE(n_var_hyper(1:n_var))
       n_var_hyper = .FALSE.
       !n_var_hyper(n_den) = .TRUE.
       n_var_hyper(n_mom(1:dim)) = .TRUE.
       n_var_hyper(n_eng) = .TRUE.
       IF ( n_mvort .GT. 0) n_var_hyper(n_mvort) = .TRUE.  ! Then the magnitude of vorticity is being tracked and saved
       !n_var_hyper = .TRUE.


       IF(ALLOCATED(n_var_hyper_active)) DEALLOCATE(n_var_hyper_active)
       ALLOCATE(n_var_hyper_active(1:n_integrated))
       n_var_hyper_active = .FALSE.
      ! n_var_hyper_active(n_den) = .TRUE.
!       n_var_hyper_active(n_eng) = .TRUE.
       n_var_hyper_active = .TRUE.
       IF( sgsmodel_compress .EQ. sgsmodel_compress_ksgs ) n_var_hyper_active(n_var_k) = .FALSE.  ! Do not smooth the ksgs field 

    END IF


    IF (par_rank.EQ.0) THEN
       PRINT *, 'n_integrated = ',n_integrated 

       PRINT *, 'n_var = ',n_var 
       PRINT *, 'n_var_exact = ',n_var_exact 
       PRINT *, '*******************Variable Names*******************'
       DO i = 1,n_var
          WRITE (*, u_variable_names_fmt) u_variable_names(i)
       END DO
       PRINT *, '****************************************************'
    END IF

  END SUBROUTINE  user_setup_pde


  !
  ! Set the exact solution for comparison to the simulated solution
  !
  ! u          - array to fill in the exact solution
  ! nlocal       - number of active wavelets   
  ! ne_local        - total number of equations
  ! t          - time of current time step 
  ! l_n_var_exact_soln_index - index into the elements of u for which we need to 
  !                            find the exact solution
  SUBROUTINE  user_exact_soln (u, nlocal,  t_local, l_n_var_exact_soln)  
    USE variable_mapping
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: nlocal
    REAL (pr), INTENT (IN) ::  t_local
    REAL (pr), DIMENSION (nlocal,n_var_exact), INTENT (INOUT) :: u
    LOGICAL , INTENT (IN) :: l_n_var_exact_soln(n_var)
    INTEGER :: i

  END SUBROUTINE  user_exact_soln

  SUBROUTINE user_initial_conditions (u, nlocal, ne_local, t_local, scl, scl_fltwt,iter)
    USE variable_mapping
    USE penalization
    USE parallel 
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: nlocal, ne_local
    REAL (pr), DIMENSION (nlocal,ne_local), INTENT (INOUT) :: u
    REAL (pr)  :: scl(1:n_var),scl_fltwt
    REAL (pr), INTENT (IN) :: t_local
    INTEGER, INTENT (INOUT) :: iter
    INTEGER :: i, j, idim, l, ie, ii, k
    INTEGER :: iseedsize, jx, jy, jz, sprt
    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc

    REAL (pr), DIMENSION(nlocal,1:dim) :: x_phys 
    REAL (pr), DIMENSION(2,1:dim) :: x_phys_limits

    REAL (pr) :: u_max, u_min, v_max, v_min, w_max, w_min
    REAL (pr), DIMENSION(nlocal,1) :: forcing
    REAL (pr), DIMENSION(nlocal) :: p
    REAL (pr), DIMENSION(nlocal) :: x_rndm
    REAL (pr), DIMENSION(nlocal) :: rand_array
    REAL (pr), DIMENSION(nlocal,2) :: loc_support, Dloc_support

    REAL (pr) :: r_max, v_mean
    REAL (pr) :: fourier_coeff1, fourier_coeff2,fourier_coeff3,L_x, L_y, L_z, x_mid, y_mid, z_mid
    REAL (pr) :: a_rndm, b_rndm, c_rndm
    REAL (pr) :: y_pos, width
    REAL (pr), DIMENSION(nlocal) :: u_r
    INTEGER, ALLOCATABLE, DIMENSION(:) :: seed

    REAL (pr) :: tmp1, tmp2 
    REAL (pr) :: d_thick, delta_trans 
    INTEGER :: nwlt_total, nwlt_max

    INTEGER :: n_cycles
    REAL (pr) :: phase_loc
    REAL (pr), DIMENSION(18) :: fourier_coeffs 
    REAL (pr), DIMENSION(nlocal) :: signal, d_signal
    !REAL (pr), DIMENSION(2049) :: eigenfcn_real, eigenfcn_imag 
    !REAL (pr), DIMENSION(nlocal) :: u_pert
    !INTEGER, DIMENSION(nlocal) :: iy_loc 
    !REAL (pr), DIMENSION(1,nlocal,dim) :: du, d2u 

    REAL (pr) :: volume_total 
       volume_total = SUM(dA)
       CALL parallel_global_sum(REAL    = volume_total               )   
       IF(par_rank .EQ. 0) PRINT *, 'INITIAL CONDITIONS VOLUME: ', volume_total


    x_phys(:,:) = user_mapping( x, nlocal, t )
    DO idim = 1,dim
       x_phys_limits(1,idim) = MINVAL( x_phys(:,idim) )
       x_phys_limits(2,idim) = MAXVAL( x_phys(:,idim) )
       CALL parallel_global_sum( REALMINVAL=x_phys_limits(1,idim) )
       CALL parallel_global_sum( REALMAXVAL=x_phys_limits(2,idim) )
       IF ( par_rank .EQ. 0 )  PRINT *, idim,'X_phys min = ', x_phys_limits(1,idim), 'max = ',x_phys_limits(2,idim)      
    END DO

  IF (IC_restart_mode > 0 ) THEN !in the case of restart
     !do nothing


    IF ( correct_thermo .EQ. 1 .OR. correct_thermo .EQ. 2 ) THEN ! For Ma = 1.5 case

      ! Store current pressure and velocity field
      p(:) = calc_pressure( u, nlocal, ne_local ) 
      DO idim = 1,dim
        u(:,n_mom(idim)) = u(:,n_mom(idim))/u(:,n_den)
      END DO  

      IF ( correct_thermo .EQ. 1) THEN ! For Ma = 1.5 case
        IF( ABS( Ma - 1.5_pr ) .GT. 1.0e-6_pr ) CALL error(' Thermo correction does not match mach number' )
        u(:,n_den) = 4.8_pr * x_phys(:,2)* x_phys(:,2) - 7.2_pr * ABS(x_phys(:,2)) + 3.4_pr
        WHERE( ABS(x_phys(:,2)) .LT. 0.75_pr )
          u(:,n_den) = 0.7_pr 
        END WHERE

      ELSE IF ( correct_thermo .EQ. 2 ) THEN ! For Ma = 3.0 case
        IF( ABS( Ma - 3.0_pr ) .GT. 1.0e-6_pr ) CALL error(' Thermo correction does not match mach number' )

        u(:,n_den) = 6.88888_pr * x_phys(:,2)* x_phys(:,2) - 9.6444444_pr * ABS(x_phys(:,2)) + 3.755555_pr
        WHERE( ABS(x_phys(:,2)) .LT. 0.7_pr )
          u(:,n_den) = 0.38_pr 
        END WHERE
      END IF

      CALL renormalize_density( u, nlocal, ne_local )

      DO idim = 1,dim
        u(:,n_mom(idim)) =  u(:,n_den) * u(:,n_mom(idim))
      END DO  
     
      p(:) = p(:)/(R_in(Nspec))/u(:,n_den) ! Calculate temperature ERIC: single species
      u(:,n_eng)              = 0.5_pr*SUM(u(:,n_mom(1:dim))**2.0_pr, DIM=2)/u(:,n_den) + u(:,n_den)*cv_in(Nspec) * F_compress * p(:) 

    END IF


  ELSE IF (IC_restart_mode .EQ. 0) THEN
     !Set Density
     u(:,n_den)              = 1.0_pr

     !Set Background Momentum
     DO i=1,dim
        u(:,n_mom(i)) = 0.0_pr
     END DO


          !parameters
        u(:,n_mom(1)) = 1.5_pr*(1.0_pr-x_phys(:,2)**2) !background flow

     IF ( use_os_ic ) THEN
        !******** Orr - Sommerfeld Solution from fourier modes *************

        !DO k = 1,2049
        !   READ(20,'(E12.5)') eigenfcn_real(k) 
        !END DO
        !DO k = 1,2049
        !   READ(20,'(E12.5)') eigenfcn_imag(k) 
        !END DO


        ! eigenfunction indice, valid on all j_level <= j_mx (where ny = 2049 at j_mx), ERIC: needs to be reconciled as a more general expression, rather than point-by-point lookup
        !iy_loc = INT((x(:,2)-xyzlimits(1,2))/(xyzlimits(2,2)-xyzlimits(1,2))*2048.0_pr) + 1  


        !u_pert(:) = eigenfcn_real(iy_loc)*COS(-x(:,1)) - eigenfcn_imag(iy_loc)*SIN(-x(:,1))
        !CALL c_diff_fast(u_pert(:), du(:,:,:), d2u(:,:,:), j_lev, nlocal, 1, 10, 1, 1, 1)
        !u(:,n_mom(1)) = u(:,n_mom(1)) + 0.085_pr*du(1,:,2)    ! %8.5 turbulence level
        !u(:,n_mom(2)) = 0.085_pr*eigenfcn_real(iy_loc)*SIN(-x(:,1)) + 0.085_pr*eigenfcn_imag(iy_loc)*COS(-x(:,1))
        n_cycles = 0
        IF ( par_rank .EQ. 0 ) THEN 
           OPEN (20, FILE='SPECTRA_OS_eigenfunction_R3000.dat', STATUS='OLD', FORM='FORMATTED', READONLY, POSITION='REWIND')
           READ(20,'(I20)') n_cycles
        END IF
        CALL parallel_global_sum( INTEGER = n_cycles )
        !PRINT *, 'cycles', n_cycles
        d_signal(:) = 0.0_pr
        ! Real Part
        DO i = 0,n_cycles - 1
                fourier_coeffs(:) = 0.0_pr
                IF ( par_rank .EQ. 0 ) READ(20,'(18(E20.12))') fourier_coeffs(:)
                CALL parallel_vector_sum( REAL = fourier_coeffs(1:18), LENGTH = 18 )
                phase_loc = fourier_coeffs(18);
                signal(:) = 0.0_pr
                signal = signal + fourier_coeffs(1);
                !PRINT *, 'a_0', fourier_coeffs(1);
                DO j = 1,8
                        signal   = signal   + fourier_coeffs( 2 * j     )                 * COS( j * phase_loc * x_phys(:,2));
                        d_signal = d_signal - fourier_coeffs( 2 * j     ) * j * phase_loc * SIN( j * phase_loc * x_phys(:,2)) * COS(- x_phys(:,1));
                        signal   = signal   + fourier_coeffs( 2 * j + 1 )                 * SIN( j * phase_loc * x_phys(:,2));
                        d_signal = d_signal + fourier_coeffs( 2 * j + 1 ) * j * phase_loc * COS( j * phase_loc * x_phys(:,2)) * COS(- x_phys(:,1));
                END DO
                u(:,n_mom(2)) = u(:,n_mom(2)) + 0.02_pr*signal(:)*SIN(- x_phys(:,1)) 
        END DO
        ! Imaginary Part
        DO i = 0,n_cycles - 1
                fourier_coeffs(:) = 0.0_pr
                IF ( par_rank .EQ. 0 ) READ(20,'(18(E20.12))') fourier_coeffs(:)
                CALL parallel_vector_sum( REAL = fourier_coeffs(1:18), LENGTH = 18 )
                phase_loc = fourier_coeffs(18);
                signal(:) = 0.0_pr
                signal = signal + fourier_coeffs(1);
                !PRINT *, 'a_0', fourier_coeffs(1);
                DO j = 1,8
                        signal   = signal   + fourier_coeffs( 2 * j     )                 * COS( j * phase_loc * x_phys(:,2));
                        d_signal = d_signal - fourier_coeffs( 2 * j     ) * j * phase_loc * SIN( j * phase_loc * x_phys(:,2)) * ( -1.0_pr )*SIN(- x_phys(:,1));
                        signal   = signal   + fourier_coeffs( 2 * j + 1 )                 * SIN( j * phase_loc * x_phys(:,2));
                        d_signal = d_signal + fourier_coeffs( 2 * j + 1 ) * j * phase_loc * COS( j * phase_loc * x_phys(:,2)) * ( -1.0_pr )*SIN(- x_phys(:,1));
                END DO
                u(:,n_mom(2)) = u(:,n_mom(2)) + 0.02_pr*signal(:)*COS(- x_phys(:,1))
        END DO
        u(:,n_mom(1)) = u(:,n_mom(1)) + 0.02_pr * d_signal(:)    ! %8.5 turbulence level
        IF( par_rank .EQ. 0 )CLOSE(20)
        !******** Orr - Sommerfeld Solution *************

     END IF

     !CALL add_smooth_noise( u(:,n_mom(1)), nlocal, 100, 0.02_pr, 23423, DEFINE_Z = 1.0_pr )
     !CALL add_smooth_noise( u(:,n_mom(1)), nlocal, 100, 0.02_pr, 234463, DEFINE_Z = -1.0_pr )
     !CALL add_smooth_noise( u(:,n_mom(2)), nlocal, 100, 0.02_pr, 233, DEFINE_Z = 1.0_pr )
     !CALL add_smooth_noise( u(:,n_mom(2)), nlocal, 100, 0.02_pr, 235173, DEFINE_Z = -1.0_pr )
     !CALL add_smooth_noise( u(:,n_mom(2)), nlocal, 50, 0.010_pr, 233, DEFINE_Y = 0.20_pr )
     !CALL add_smooth_noise( u(:,n_mom(2)), nlocal, 50, 0.010_pr, 3389, DEFINE_Y = -0.20_pr )
     !IF( dim .EQ. 3 ) THEN
     !   CALL add_smooth_noise( u(:,n_mom(3)), nlocal, 50, 0.010_pr, 3987983, DEFINE_Y = 0.20_pr )
     !   CALL add_smooth_noise( u(:,n_mom(3)), nlocal, 50, 0.010_pr, 9234, DEFINE_Y = -0.20_pr )
     !  CALL add_smooth_noise( u(:,n_mom(3)), nlocal, 100, 0.02_pr, 423, DEFINE_Z = 1.0_pr )
     !  CALL add_smooth_noise( u(:,n_mom(3)), nlocal, 100, 0.02_pr, 42673, DEFINE_Z = -1.0_pr )
     !END IF

     !Set Energy for constant temperature
     ! ERIC: different nondimensionalization
     !u(:,n_eng)              = 0.5_pr*SUM(u(:,n_mom(1:dim))**2.0_pr, DIM=2)/u(:,n_den) + u(:,n_den)*(cp_in(Nspec)-1.0_pr/MW_in(Nspec)/Ma**2.0_pr)*Tconst
     u(:,n_eng)              = 0.5_pr*SUM(u(:,n_mom(1:dim))**2.0_pr, DIM=2)/u(:,n_den) + u(:,n_den)*cv_in(Nspec) * F_compress * 1.0_pr 
     !Set species
     IF (Nspec>1) u(:,n_spc(1)) = 0.0_pr

     DO i = 1,dim + 2
        tmp1 = MINVAL(u(:,i))
        tmp2 = MAXVAL(u(:,i))
        CALL parallel_global_sum( REALMINVAL=tmp1 )
        CALL parallel_global_sum( REALMAXVAL=tmp2 )
        IF(par_rank .EQ. 0) PRINT *, 'Initialize var', i, tmp1,tmp2
     END DO
             
     !introduce boundary conditions so that the code will adapt on IC's if
     !necessary
     DO ie = 1, ne_local
        !--Go through all Boundary points that are specified
        i_p_face(0) = 1
        DO i=1,dim
           i_p_face(i) = i_p_face(i-1)*3
        END DO
        DO face_type = 0, 3**dim - 1
           face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
           IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
              CALL get_all_indices_by_face (face_type, j_lev, nloc, iloc)
              IF(nloc > 0 ) THEN 
                 IF( ABS(face(2)) == 1  ) THEN  !Left and Right - Full
                   IF(ie >= n_mom(1) .AND. ie <= n_mom(dim)) THEN  !Momentums
                      u(iloc(1:nloc),ie)              =  0.0_pr  !no-slip
                   ELSE IF(ie == n_eng) THEN
                      !u(iloc(1:nloc),ie)              =  u(iloc(1:nloc),n_den)*cv_in(Nspec)*Tconst/Ma**2 !temperatures
                      u(iloc(1:nloc),ie)              =  u(iloc(1:nloc),n_den)*cv_in(Nspec)*F_compress * 1.0_pr !temperatures
                   END IF

                 END IF
              END IF
           END IF
        END DO
     END DO
     


     nwlt_max = nwlt
     CALL parallel_global_sum(INTEGERMAXVAL=nwlt_max)
     IF(par_rank .EQ. 0 ) PRINT *, 'nwlt_max:', nwlt_max


  END IF

  nwlt_total = nwlt
  CALL parallel_global_sum(INTEGER=nwlt_total) 
  IF (par_rank .EQ. 0) THEN
        PRINT *, '********************************************'
        PRINT *, '* j_lev = ',j_lev,        '  *' 
        PRINT *, '* nwlt  = ', nwlt_total,  '  *' 
        PRINT *, '********************************************'
  END IF

 
END SUBROUTINE user_initial_conditions

  !--********************************  
  ! Arguments
  ! u         - field on adaptive grid
  ! nlocal      - number of active points
  ! ne_local       - number of equations
  ! t         - current time
  !
  ! Global variables used
  ! ifn       - number of equations
  ! x()
  ! xO
  ! y0
  !--******************************** 
  SUBROUTINE user_algebraic_BC (Lu, ulocal, nlocal, ne_local, jlev, meth)
    USE sgs_compressible 
    !--Defines boundary condition type
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: jlev, meth, ne_local, nlocal
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (INOUT) :: Lu
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (IN)    :: ulocal

    INTEGER :: ie, i, ii, shift, denshift,eshift
    INTEGER, DIMENSION (dim) :: velshift 
    REAL (pr), DIMENSION (ne_local,nlocal,dim) :: du, d2u !uncomment if Neuman BC are used, need to call 
    REAL (pr), DIMENSION (1,nlocal,dim) :: du_prev
    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc
   
    INTEGER :: lowvar, highvar
 
    !shift markers for BC implementation
    denshift  = nlocal*(n_den-1)
    DO i = 1,dim
       velshift(i) = nlocal*(n_mom(i)-1)
    END DO
    eshift    = nlocal*(n_eng-1)


    !IF(sgsmodel_compressible == sgsmodel_compressible_LDKM) THEN 
    !   lowvar = n_var_k
    !   highvar = n_var_k
    !   IF (lagrangian_avg .EQ. 1) highvar = n_var_compressible_Inn
    !END IF


    !BC for both 2D and 3D cases

    !if Neuman BC are used, need to call 
!!$    CALL c_diff_fast (u, du, d2u, jlev, nlocal, meth, 10, ne_local, 1, ne_local)
    DO ie = 1, ne_local
       shift = nlocal*(ie-1)
       !--Go through all Boundary points that are specified
       i_p_face(0) = 1
       DO i=1,dim
          i_p_face(i) = i_p_face(i-1)*3
       END DO
       DO face_type = 0, 3**dim - 1
          face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
          IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
             CALL get_all_indices_by_face (face_type, jlev, nloc, iloc)
             IF(nloc > 0 ) THEN 
                IF( abs(face(2)) == 1  ) THEN  !Left and Right - Full
                   IF(ie >= n_mom(1) .AND. ie <= n_mom(dim)) THEN  !Momentums
                      Lu(shift+iloc(1:nloc)) = ulocal(shift+iloc(1:nloc))       !Dirichlet conditions
                   ELSE IF(ie == n_eng) THEN
                      Lu(shift+iloc(1:nloc)) = ulocal(shift+iloc(1:nloc))   - ulocal(denshift+iloc(1:nloc))*cv_in(Nspec) * F_compress * 1.0_pr     !Dirichlet conditions - Isothermal, no slip
                      !Lu(shift+iloc(1:nloc)) = ulocal(shift+iloc(1:nloc))   - ulocal(denshift+iloc(1:nloc))*(cp_in(Nspec)-1.0_pr/MW_in(Nspec)/Ma**2.0_pr)*1.0_pr     !Dirichlet conditions - Isothermal, no slip
                   ELSE IF(ie .EQ. n_var_k .AND. sgsmodel_compress .EQ. sgsmodel_compress_ksgs ) THEN
                      Lu(shift+iloc(1:nloc)) = ulocal(shift+iloc(1:nloc)) 
                   END IF
                END IF
             END IF
          END IF
       END DO
    END DO

  END SUBROUTINE user_algebraic_BC

  SUBROUTINE user_algebraic_BC_diag (Lu_diag, nlocal, ne_local, jlev, meth)
    USE sgs_compressible 
    !--Defines boundary condition type
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: jlev, meth, ne_local, nlocal
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (INOUT) :: Lu_diag

    INTEGER :: ie, i, ii, shift, denshift

    REAL (pr), DIMENSION (nlocal,dim) :: du_diag, d2u_diag  !uncomment if Neuman BC are used
    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc
    REAL (pr), DIMENSION (1,nlocal,dim) :: du_prev, d2u
    INTEGER, PARAMETER :: methprev=1

    !BC for both 2D and 3D cases
    INTEGER :: lowvar, highvar

    denshift = nlocal*(n_den-1)


    !IF(sgsmodel_compressible == sgsmodel_compressible_LDKM) THEN 
    !   lowvar = n_var_k
    !   highvar = n_var_k
    !   IF (lagrangian_avg .EQ. 1) highvar = n_var_compressible_Inn
    !END IF

    !if Neuman BC are used, need to call 
!!$    CALL c_diff_diag ( du_diag, d2u_diag, jlev, nlocal, meth, meth, 10)

    DO ie = 1, ne_local
       shift = nlocal*(ie-1)
       !--Go through all Boundary points that are specified
       i_p_face(0) = 1
       DO i=1,dim
          i_p_face(i) = i_p_face(i-1)*3
       END DO
       DO face_type = 0, 3**dim - 1
          face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
          IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
             CALL get_all_indices_by_face (face_type, jlev, nloc, iloc)
             IF(nloc > 0 ) THEN 
                IF( ABS(face(2)) == 1  ) THEN 
                 
                   IF(ie >= n_mom(1) .AND. ie <= n_mom(dim)) THEN
                      Lu_diag(shift+iloc(1:nloc)) = 1.0_pr  !Dirichlet conditions
                   ELSE IF(ie == n_eng) THEN
                      Lu_diag(shift+iloc(1:nloc)) = 1.0_pr  !Dirichlet conditions
                   ELSE IF(ie .EQ. n_var_k .AND. sgsmodel_compress .EQ. sgsmodel_compress_ksgs ) THEN
                      Lu_diag(shift+iloc(1:nloc)) = 1.0_pr  !Dirichlet conditions
                   END IF
                END IF
             END IF
          END IF
       END DO
    END DO

    !varibale thresholding for evolution eps
    !IF ( do_Adp_Eps_Spatial_Evol ) &
    !CALL VT_algebraic_BC_diag (Lu_diag, du_diag, nlocal, ne_local, jlev, meth) 
  END SUBROUTINE user_algebraic_BC_diag

  SUBROUTINE user_algebraic_BC_rhs (rhs, ne_local, nlocal, jlev)
    USE sgs_compressible 
    !--Sets rhs for boundary conditions
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: ne_local, nlocal, jlev
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (INOUT) :: rhs

    INTEGER :: ie, i, ii, shift, denshift
    INTEGER, PARAMETER ::  meth=1
    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc
    REAL (pr), DIMENSION (ne_local,nlocal,dim) :: duall, d2uall
    REAL (pr), DIMENSION (1,nlocal,dim) :: du_prev, du, d2u  
    REAL (pr) :: p_bc
    INTEGER :: lowvar, highvar

    p_bc = 1.0_pr  !pressure defined for outflow boundary conditions

    denshift=nlocal*(n_den-1)

    DO ie = 1, ne_local
       shift = nlocal*(ie-1)
       !--Go through all Boundary points that are specified
       i_p_face(0) = 1
       DO i=1,dim
          i_p_face(i) = i_p_face(i-1)*3
       END DO
       DO face_type = 0, 3**dim - 1
          face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
          IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
             CALL get_all_indices_by_face (face_type, jlev, nloc, iloc)
             IF(nloc > 0 ) THEN 
                IF( ABS(face(2)) == 1  ) THEN              
                   IF(ie == n_mom(1)) THEN
                         rhs(shift+iloc(1:nloc)) = 0.0_pr 
                   ELSE IF(ie >= n_mom(2) .AND. ie <= n_mom(dim)) THEN
                      rhs(shift+iloc(1:nloc)) = 0.0_pr 
                   ELSE IF(ie == n_eng) THEN
                      rhs(shift+iloc(1:nloc)) = 0.0_pr !define temp in algebraic BC
                   ELSE IF(ie .EQ. n_var_k .AND. sgsmodel_compress .EQ. sgsmodel_compress_ksgs ) THEN
                      rhs(shift+iloc(1:nloc)) = 0.0_pr !define temp in algebraic BC
                   END IF
                END IF
             END IF
          END IF
       END DO
    END DO

  END SUBROUTINE user_algebraic_BC_rhs

  FUNCTION user_chi (nlocal, t_local)
    USE parallel
    USE threedobject
    IMPLICIT NONE
    INTEGER,          INTENT (IN) :: nlocal
    REAL (pr),        INTENT (IN) :: t_local
    REAL (pr), DIMENSION (nlocal) :: user_chi
    REAL (pr), DIMENSION (nlocal) :: dist_loc
    REAL(pr), DIMENSION(nlocal,1) :: forcing
    REAL(pr) :: smooth_coeff

    user_chi = 0.0_pr

  END FUNCTION user_chi

  FUNCTION user_mapping ( xlocal, nlocal, t_local )
    USE curvilinear
    USE curvilinear_mesh
    USE error_handling 
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: nlocal
    REAL (pr), INTENT (IN) :: t_local
    REAL (pr), DIMENSION (nlocal,dim), INTENT(IN) :: xlocal
    REAL (pr), DIMENSION (nlocal,dim) :: user_mapping

    user_mapping(:,1:dim) = xlocal(:,1:dim)

    ! channel boundary layer stretching 
    IF ( dim .GE. 2 .AND. transform_dir(2) .EQ. 1 ) THEN 
       user_mapping(:,:) = channel_tanh( xlocal, nlocal, mesh_cluster_param, 2 )
    END IF

    IF( transform_dir(1) .NE. 0 &
        .OR. ( transform_dir(3) .NE. 0 .AND. dim .GE. 3  )  ) THEN
       CALL error( 'Incorrect transform directions for channel flow.  Check transform_dir()' )
    END IF

  END FUNCTION user_mapping

  FUNCTION user_rhs (u_integrated,scalar)
    USE sgs_compressible 
    IMPLICIT NONE
    REAL (pr), DIMENSION (ng,ne), INTENT(IN) :: u_integrated !1D flat version of integrated variables without BC 
    REAL (pr), DIMENSION (ng), INTENT(IN) :: scalar
    REAL (pr), DIMENSION (ng*ne) :: user_rhs

    !REAL (pr), DIMENSION (ne,ng,dim) :: du, d2u
    INTEGER, PARAMETER :: meth=1

    CALL compressible_rhs ( user_rhs, u_integrated, meth) 
    IF( sgsmodel_compress .NE. sgsmodel_compress_none ) CALL sgs_compress_rhs ( user_rhs, u_integrated, meth) 


    IF(hypermodel .NE. 0 .AND. ( hyper_it .LT. hyper_it_limit .OR. hyper_it_limit < 0) ) THEN
      !hyper_mask = set_hyper_mask(ng) 
       CALL hyperbolic(u_integrated,ng,user_rhs)
       IF( par_rank .EQ. 0 ) PRINT *, 'Using Hyperbolic Module'
    END IF

  END FUNCTION user_rhs

  FUNCTION user_Drhs (u_p, u_prev, meth)
    USE sgs_compressible 
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: meth
    REAL (pr), DIMENSION (ng,ne) :: u_p, u_prev
    REAL (pr), DIMENSION (ng*ne) :: user_Drhs
    !REAL (pr), DIMENSION (2*ne,ng,dim) :: du
    !REAL (pr), DIMENSION (ne,ng,dim)   :: d2u

    CALL compressible_Drhs ( user_Drhs, u_p, u_prev, meth ) 
    IF( sgsmodel_compress .NE. sgsmodel_compress_none ) CALL sgs_compress_Drhs ( user_Drhs, u_p, u_prev, meth ) 


    IF (hypermodel .NE. 0 .AND. ( hyper_it .LT. hyper_it_limit .OR. hyper_it_limit < 0)) THEN
      !hyper_mask = set_hyper_mask(ng) 
      CALL hyperbolic(u_p,ng,user_Drhs)
    END IF

  END FUNCTION user_Drhs

  FUNCTION user_Drhs_diag (meth)
    USE sgs_compressible 
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: meth
    REAL (pr), DIMENSION (ng*ne) :: user_Drhs_diag
    REAL (pr), DIMENSION (ng,ne) :: u_prev
    !REAL (pr), DIMENSION (ne,ng,dim) :: du_dummy
    !REAL (pr), DIMENSION (ng,dim)    :: du_diag, d2u_diag 
    INTEGER :: ie, shift
    !REAL (pr), DIMENSION(ng) :: hyper_mask

    DO ie = 1, ne
       shift=(ie-1)*ng
       u_prev(1:ng,ie) = u_prev_timestep(shift+1:shift+ng)
    END DO

    CALL compressible_Drhs_diag(user_Drhs_diag,u_prev,meth )
    IF( sgsmodel_compress .NE. sgsmodel_compress_none ) CALL sgs_compress_Drhs_diag(user_Drhs_diag,u_prev,meth )

    IF(hypermodel .NE. 0 .AND. ( hyper_it .LT. hyper_it_limit .OR. hyper_it_limit < 0)) THEN
      !hyper_mask = set_hyper_mask(ng) 
      CALL hyperbolic_diag(user_Drhs_diag, ng)
    END IF


  END FUNCTION user_Drhs_diag

  SUBROUTINE user_project (u, p, nlocal, meth)
    USE variable_mapping
    IMPLICIT NONE

    INTEGER, INTENT (IN) :: meth, nlocal
    REAL (pr), DIMENSION (nlocal),     INTENT(INOUT) :: p
    REAL (pr), DIMENSION (nlocal,n_integrated), INTENT(INOUT) :: u

    !no projection for compressible flow
  END SUBROUTINE user_project

  !
  ! Calculate any statitics
  !
  ! startup_flag - 0 when adapting to IC,then 1 inmain integration loop
  !
  SUBROUTINE user_stats ( u, j_mn_local, startup_flag)
    USE parallel
    USE penalization
    USE variable_mapping
    IMPLICIT NONE
    REAL (pr), DIMENSION (nwlt,1:n_var), INTENT (IN) :: u
    INTEGER , INTENT (IN) :: j_mn_local 
    INTEGER , INTENT (IN) :: startup_flag
    REAL (pr), DIMENSION(nwlt,1:dim) :: x_phys 

    INTEGER :: i
    INTEGER, DIMENSION (:), ALLOCATABLE :: plane_ids 
    CHARACTER (LEN=u_variable_name_len), DIMENSION(:), ALLOCATABLE  :: var_names 

    IF ( startup_flag .AND. add_extract_planes ) THEN
      x_phys(:,:) = user_mapping( x, nwlt, t )

      ALLOCATE(plane_ids(n_additional_planes-2), var_names(n_var) )

      DO i = 1, n_additional_planes-2
          plane_ids(i) = i + 2
      END DO
      DO i = 1,n_var
        var_names(i) = u_variable_names(i)
      END DO
      CALL extract_planes ( u, x_phys, VAR_NAMES, n_var, 1, plane_ids )

      DEALLOCATE(plane_ids, var_names )
    END IF

  END SUBROUTINE user_stats

  SUBROUTINE user_cal_force (u, n, t_local, force, drag, lift)
    !--Calculates drag and lift on obstacle using penalization formula
    IMPLICIT NONE

    INTEGER, INTENT (IN) :: n
    REAL (pr), INTENT (IN) :: t_local
    REAL (pr), DIMENSION (dim), INTENT (INOUT) :: force
    REAL (pr), INTENT (OUT) :: drag, lift
    REAL (pr), DIMENSION (n,dim) :: u
    drag = 0.0_PR
    lift = 0.0_PR
    !
    ! There is no obstacle in flow
    !

  END SUBROUTINE user_cal_force

  !
  ! Read input from "case_name"_pde.inp file
  ! case_name is in string file_name read from command line
  ! in read_command_line_input()
  ! 
  SUBROUTINE user_read_input()
    USE sgs_compressible 
    IMPLICIT NONE
    INTEGER :: i, j, ind
    CHARACTER(LEN=8):: numstrng, procnum
    REAL (pr), ALLOCATABLE, DIMENSION(:) :: YR, gammR  ! temporary calculation quantities 

    CALL sgs_compress_read_input()
    CALL compressible_read_input()

    call input_real ('Re',Re,'stop',' Re: Reynolds Number')
    call input_real ('Pra',Pra,'stop',' Pra: Prandtl Number')
    call input_real ('Sc',Sc,'stop',' Sc: Schmidt Number')
    call input_real ('Fr',Fr,'stop',' Fr: Froude Number')
    call input_real ('Ma',Ma,'stop',' Ma: Mach Number')

    call input_real ('Re_tau_target',Re_tau_target,'stop',' Re_tau_target: Target Wall Reynolds Number')

    !Correct thermo averages
    correct_thermo = -1
    call input_integer ('correct_thermo',correct_thermo,'default',' Correct thermodynamic average profiles: 0-none, 1-Ma=1.5, 2-Ma=3' ) 
    IF (correct_thermo .GT. 2) CALL error( 'Unknown selection to correct channer thermodynamics')

    call input_logical ('use_OS_IC',use_OS_IC,'stop', 'use_OS_IC: Use the Orr-Sommerfield solution from file for the IC')

    call input_integer ('hyper_it_limit',hyper_it_limit,'stop','  hyper_it_limit: number of iterations to apply the hyperbolic viscosity to smooth local instabilities.  hyper_it_limit < 0 for all iterations')
 
    filter_field = .FALSE.
    call input_logical ('filter_field',filter_field,'default', 'filter_field: pre_process stabilization of field by applying lowpass filter once')


    call input_real ('Lczn',Lczn,'stop', 'Lcoordzn: length of coord_zone on one side')
    call input_real ('tfacczn',tfacczn,'stop', 'tfacczn: factor multiply by time for coord_zone time=L/min(c)*tfac, set <0 for no czn modification')
    call input_real ('itfacczn',itfacczn,'stop', 'itfacczn: factor multiply by initial time for coord_zone itime=Lczn/min(c)*tfac, set <0 to set itime=0')



    call input_real ('grav_coeff',grav_coeff,'stop', 'grav_coeff: coefficient for gravity forcing')
    call input_logical ('dynamic_gravity',dynamic_gravity,'stop', 'dynamic_gravity: use dynamic volume forcing to maintain some bulk flow quantity' )

    !4extra - these 8 input definitions - MUST ALSO ADD TO .inp 
    call input_logical ('saveUtil',saveUtil,'stop', 'saveUtil: saveUtil: save utility variable') 

    ! Channel-flow specific variable
    call input_logical ('saveWallS',saveWallS,'stop', 'saveWallS: Save wall normal strain rate for channel flow (S_12)') 
   
    ! channel clow mesh stretchin
    call input_real ('mesh_cluster_param', mesh_cluster_param, 'stop',' mesh_cluster_param: Mesh clustering parameter for curvilinear stretching in boundary layers')

    ! LES van driest damping parameter
    call input_real ('van_driest_damping',van_driest_damping,'stop',' van_driest_damping: Van Driest number for scaling the eddy viscosity damping function' ) 

    ! Extract additional planes for statistics 
    call input_logical ('add_extract_planes',add_extract_planes,'default', 'add_extract_planes: extract field values on additional planes')


    IF (par_rank.EQ.0) THEN
       PRINT *, 'Using Re=', Re     
    END IF

    CALL set_F_compress( 1.0_pr/Ma**2 / gamm(1) )
    ! Set properties
    DO i = 1, Nspec
       IF (GRAV) CALL set_body_force(-grav_coeff, i, 1 )  !sign change for forcing in the positive x-direction
       ! All molecular weights set equal
       !CALL set_thermo_props( 1.0_pr, 1.0_pr/MW_in(i) * gamm_loc(i)/(gamm_loc(i)-1.0_pr), gamm_loc(i), i )  ! To consistantly initialize derived thermo properties 
       CALL set_thermo_props( 1.0_pr/MW_in(i) * gamm(i)/(gamm(i)-1.0_pr), 1.0_pr/MW_in(i), i )  ! To consistantly initialize derived thermo properties 
    END DO

    ! Intermediate quantities for calculating diffusivity
    IF( ALLOCATED( YR ) ) DEALLOCATE( YR )
    IF( ALLOCATED( gammR ) ) DEALLOCATE( gammR )
    ALLOCATE( YR(1:Nspec), gammR(1:Nspec) )
    YR(:) = MW_in(:) / SUM(MW_in(1:Nspec))                                   
    IF( Nspec .GT. 1 ) YR(Nspec) = 1.0_pr - SUM(YR(1:Nspecm)) ! higher accuracy
    gammR = SUM(cp_in(:)*YR(:))/( SUM(cp_in(:)*YR(:))-SUM(YR(:)/MW_in(:)) )   
    DO i = 1, Nspec
       CALL set_dynamic_viscosity( 1.0_pr/Re, i )
       CALL set_conductivity( 1.0_pr/Re/Pra*gammR(i)/(gammR(i)-1.0_pr), i )   
       CALL set_diffusivity( 1.0_pr/Re/Sc, i )
    END DO
    DEALLOCATE( YR )
    DEALLOCATE( gammR )



    grav_coeff_hist = grav_coeff
    Re_bulk_hist = Re
    Re_tau_hist = Re_tau_target 
    
     IF (NOT(ALLOCATED(Lxyz))) ALLOCATE(Lxyz(1:dim))

     Lxyz(1:dim)=xyzlimits(2,1:dim)-xyzlimits(1,1:dim)


  END SUBROUTINE user_read_input

  !
  ! calculate any additional variables
  ! 
  ! arg 
  ! flag  - 0 calledwhile adapting to IC, 1 - called in main time integration loop
  !
  ! These additiona variables are calculated and left in real space.
  !
  SUBROUTINE user_additional_vars( t_local, flag )
    USE sgs_compressible 
    USE penalization
    IMPLICIT NONE
    REAL (pr), INTENT (IN) ::  t_local
    INTEGER , INTENT(IN) :: flag ! 0- called during adaption to IC, 1 called during main integration loop
    REAL (pr) :: cp, R, mygr, cmax, uval
    INTEGER :: l, i, j
    REAL (pr), DIMENSION(1,nwlt,MAX(dim,Nspecm,2)) :: du, d2u    
    REAL (pr) :: iterdiff, initdiff, curtau, fofn, deln, delnold, delfofn
    INTEGER, PARAMETER :: meth=1
    REAL (pr), DIMENSION(nwlt,dim*2) :: bigU


    REAL (pr), DIMENSION(nwlt) :: a_wall
    INTEGER :: face_type, nloc, wlt_type, k, j_df
    INTEGER :: ie, i_bnd
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim)   :: face
    INTEGER, DIMENSION(nwlt)  :: iloc
    REAL (pr) :: a_total, rho_wav, rho_m, volume_total, vel_bulk
    REAL (pr), DIMENSION(2) :: tau_wall
    INTEGER,   DIMENSION(2) :: pts_total
    REAL (pr), DIMENSION(nwlt,dim) :: v
    REAL (pr), DIMENSION(dim,nwlt,dim) :: dv, d2v
    INTEGER :: j_wall    !minimum j_level at the wall

    REAL (pr), DIMENSION(nwlt,dim*(dim+1)/2) :: s_ij
    REAL (pr), DIMENSION(nwlt)               :: smod

    LOGICAL, SAVE :: header_flag = .TRUE.
 
    IF (saveUtil) u(:,n_util) = 0.0_pr  ! dummy field 
    IF (imask_obstacle) u(:,nmsk) = penal

    CALL compressible_additional_vars( t_local, flag )

    IF(flag .EQ. 1) THEN
    !Wall averaging:

    j_wall = j_mn
    IF(additional_planes_active) j_wall = j_additional_planes

    DO i = 1,dim
       v(:,i) = u(:,n_mom(i))/u(:,n_den)
    END DO
    CALL Sij( v, nwlt, s_ij, smod, .TRUE.) 

    IF (saveWallS ) u(:,n_var_wallS) = s_ij(:,dim+1) 

    s_ij = s_ij*2.0_pr  !S_ij -> tau_ij

    !IF(MOD(it,21)==0   ) THEN
    IF(.TRUE.   ) THEN
       tau_wall(:)     = 0.0_pr
       a_total         = 0.0_pr
       pts_total(:)    = 0
       rho_wav         = 0.0_pr
       IF ( dim .EQ. 3 ) THEN
          DO j = 1, j_wall
             i_p_face(0) = 1
             DO i=1,dim
                i_p_face(i) = i_p_face(i-1)*3
             END DO
             DO face_type = 0, 3**dim - 1
                face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
                IF( face(2) == -1  ) THEN                                    !bottom wall
                   DO wlt_type = MIN(j-1,1),2**dim-1
                      DO j_df = j, j_lev
                         DO k = 1, indx_DB(j_df,wlt_type,face_type,j)%length
                            i_bnd = indx_DB(j_df,wlt_type,face_type,j)%p(k)%i

                            tau_wall(1)  = tau_wall(1) + S_ij(i_bnd,4)
                            rho_wav      = rho_wav + u(i_bnd,n_den) 
                            pts_total(1) = pts_total(1) + 1
                            a_total      = a_total + h(j_wall,1)*h(j_wall,3)   !y-norm area based on j_mn    
                         END DO
                      END DO
                   END DO
                ELSE IF( face(2) == 1  ) THEN                                 !top wall
                   DO wlt_type = MIN(j-1,1),2**dim-1
                      DO j_df = j, j_lev
                         DO k = 1, indx_DB(j_df,wlt_type,face_type,j)%length
                            i_bnd = indx_DB(j_df,wlt_type,face_type,j)%p(k)%i

                            tau_wall(2)  = tau_wall(2) + S_ij(i_bnd,4)
                            rho_wav      = rho_wav + u(i_bnd,n_den) 
                            pts_total(2) = pts_total(2) + 1
                            a_total      = a_total + h(j_wall,1)*h(j_wall,3)   !y-norm area based on j_mn    
                         END DO
                      END DO
                   END DO
                END IF
             END DO
          END DO
       END IF
       !Bulk averaged values
       rho_m        = SUM(u(:,n_den)*dA)
       volume_total = SUM(dA)
       !vel_bulk     = SUM(SQRT(SUM( u(:,n_mom(1:dim))**2.0_pr, DIM=2)/u(:,n_den)**2.0_pr)*dA)
       vel_bulk     =  SUM(u(:,n_mom(1))/u(:,n_den)*dA) 
   
       !PRINT *, 'summing on proc', par_rank
       !sum across processors
       CALL parallel_vector_sum(REAL    = tau_wall(1:2), LENGTH = 2  )
       CALL parallel_global_sum(REAL    = rho_wav                    )   
       CALL parallel_global_sum(REAL    = rho_m                      )   
       CALL parallel_global_sum(REAL    = volume_total               )   
       CALL parallel_global_sum(REAL    = vel_bulk                   )   
       CALL parallel_vector_sum(INTEGER = pts_total(1:2), LENGTH = 2 )  !equal sized area samples
       CALL parallel_global_sum(REAL    = a_total                    )    !area_check

       !SANITY CHECK
       IF(pts_total(1) .NE. pts_total(2) .AND. par_rank ==0) THEN
          PRINT *, 'WARNING: Total points on top and bottom walls do not agree', pts_total(1), pts_total(2)
       END IF

       !Wall averaging
       tau_wall(1:2) = tau_wall(1:2)/REAL(pts_total(1:2),pr)
       rho_wav       = rho_wav/REAL( SUM(pts_total)  ,pr) 
       Re_tau        = SQRT(rho_wav * (tau_wall(1)-tau_wall(2))/2.0_pr * Re)

       !Bulk Averaging
       rho_m    = rho_m/volume_total
       vel_bulk = vel_bulk/volume_total

 
        !Temporal averaging
       !wall
       Re_tau_hist(MOD(it,20)+1)     = Re_tau
       !bulk
       Re_bulk_hist(MOD(it,20)+1)    = vel_bulk*Re 
       Re_bulk                        = vel_bulk*Re 
       !grav_coeff_hist(MOD(it,20)+1) = !Re_tau_target**2.0_pr/Re**2.0_pr/rho_m/rho_wav !with target of Re_tau = 200
       !grav_coeff_hist(MOD(it,20)+1) = !grav_coeff_hist(MOD(it,20)+1)*(5.0_pr*(Re_tau_target-Re_tau)/Re_tau_target + 1.2_pr) !adaptive forcing to hasten convergence
       !IF(Re_bulk .GT. 2.0_pr*Re) grav_coeff_hist(MOD(it,20)+1) = grav_coeff/2.0_pr  !upper limiter to avoid high Re during transition
       !grav_coeff = SUM(grav_coeff_hist)/20.0_pr
       
       !grav_coeff = grav_coeff + 0.01_pr*grav_coeff*(Re_tau_target-Re_tau)/Re_tau_target

       IF ( dynamic_gravity ) THEN
          grav_coeff = grav_coeff*(1.0_pr + (1.0 - vel_bulk)*0.03_pr)  !0.01 means it will take 100 steps to adjust F by % delta in target velocity
          grav_coeff = MIN(0.02_pr,MAX(grav_coeff,0.0_pr)) !limiting in case timescale of grav_coeff is too much faster than vel_bulk

          grav_coeff_hist(MOD(it,20)+1) = grav_coeff 
       END IF
 
         ! ERIC: how to set gravity? 
         DO i = 1, Nspec
            IF (GRAV) CALL set_body_force(-grav_coeff, i, 1 )  !sign change for forcing in the positive x-direction
         END DO
         !IF (GRAV) gr_in(1+Nspec:Nspec*2) = 0.05_pr  !laterla forcing to re-energize the boundary layer

         IF(par_rank == 0) THEN 
             PRINT *, '*********************************************************'
             PRINT *, 'WALL PTS (TOP)', pts_total(2),               'WALL AREA       ', a_total
            ! PRINT *, 'DEN WAV       ', rho_wav,                    'TAU WAV         ', (tau_wall(2)-tau_wall(1))/2.0_pr 
            ! PRINT *, 'TAU WAV(1)    ', tau_wall(1),                'TAU WAV (2)     ', tau_wall(2) 
             PRINT *, 'VOLUME        ', volume_total,               'RHO_M           ', rho_m
             PRINT *, 'Re_m (tmp avg)', SUM(Re_bulk_hist)/20.0_pr, 'Re_tau (tmp avg)', SUM(Re_tau_hist)/20.0_pr
             PRINT *, 'Re_m          ', Re_bulk,                    'Re_tau          ', Re_tau
             PRINT *, 'Grav_coeff    ', grav_coeff,                 'Bulk Vel        ', vel_bulk 
             PRINT *,'**********************************************************'
            
             IF(header_flag) THEN
                OPEN(UNIT=431, FILE = TRIM(res_path)//TRIM(file_gen)//'_wall.stat', STATUS = 'UNKNOWN', form="FORMATTED", POSITION="APPEND")
                WRITE(431,  ADVANCE='YES', FMT='( a )' ) &
                  '%t      Re_bulk             Re_tau           vel_bulk'
                header_flag = .FALSE.
             END IF 

             OPEN(UNIT=431, FILE = TRIM(res_path)//TRIM(file_gen)//'_wall.stat', STATUS = 'OLD', form="FORMATTED", POSITION="APPEND")
             WRITE(431,'(4(E20.12))') t, Re_bulk, Re_tau, vel_bulk 
          END IF
       END IF
       Re_bulk    = SUM(Re_bulk_hist   )/20.0_pr
       Re_tau     = SUM(Re_tau_hist    )/20.0_pr
    END IF




       IF( sgsmodel_compress .NE. sgsmodel_compress_none ) CALL sgs_compress_additional_vars( t_local, flag )


       IF (modczn) THEN
          IF (t>tczn) THEN
             modczn=.FALSE.
             xyzzone(1,1) = czn_min
             xyzzone(2,1) = czn_max            
            IF (par_rank.EQ.0 .AND. verb_level>0) THEN
             PRINT *, '*****************************************************'
             PRINT *, 'FINALIZING TEMPORARY COORD_ZONE'
             PRINT *, 'COORD_ZONE OUTSIDE OF x=[',xyzzone(1,1),',',xyzzone(2,1),']'
             PRINT *, 'TIME: ', t
             PRINT *, 'STOPPING TIME FOR COORD_ZONE: ', tczn
             PRINT *, '*****************************************************'
            END IF
          END IF
       ELSE  
          IF (tfacczn>0.0_pr .AND. itfacczn>0.0_pr .AND. t>itczn) THEN
             itczn=t_end*10.0_pr
             czn_min = xyzzone(1,1)
             czn_max = xyzzone(2,1)
             xyzzone(1,1) = -Lczn
             xyzzone(2,1) =  Lczn
             cczn = MINVAL(SQRT(gamm/MW_in/Ma**2))
             tczn = MAXVAL(ABS(xyzlimits(:,1)))/cczn*tfacczn
            IF (par_rank.EQ.0 .AND. verb_level>0) THEN
             PRINT *, '*****************************************************'
             PRINT *, 'APPLYING TEMPORARY COORD_ZONE'
             PRINT *, 'COORD_ZONE OUTSIDE OF x=[',-Lczn,',',Lczn,']'
             PRINT *, 'MINIMUM WAVE SPEED: ', cczn
             PRINT *, 'MAXIMUM DISTANCE: ', MAXVAL(ABS(xyzlimits(:,1)))
             PRINT *, 'TIME: ', t
             PRINT *, 'STARTING TIME FOR COORD_ZONE: ', itczn
             PRINT *, 'TIME SAFETY FACTOR: ', tfacczn
             PRINT *, 'STOPPING TIME FOR COORD_ZONE: ', tczn
             PRINT *, '*****************************************************'
            END IF
             IF (t<tczn) THEN
                modczn = .TRUE.
             ELSE
                IF (par_rank.EQ.0) PRINT *, '!!!TOO LATE TO START COORD_ZONE: t>tczn!!!'
             END IF
          END IF  
       END IF


  END SUBROUTINE user_additional_vars

  !
  ! calculate any additional scalar variables
  ! 
  ! arg 
  ! flag  - 0 calledwhile adapting to IC, 1 - called in main time integration loop
  !
  SUBROUTINE user_scalar_vars( flag )
    IMPLICIT NONE
    INTEGER , INTENT(IN) :: flag ! 0- called during adaption to IC, 1 called during main integration loop

  END SUBROUTINE user_scalar_vars

  !
  !************ Calculating Scales ***************************
  !
  ! Note the order of the components in the scl array
  ! correspond to u_tn, v_tn, w_tn, u_tn-1, v_tn-1, w_tn-1, u_tn-2, v_tn-2, w_tn-2
  ! 
  SUBROUTINE user_scales(flag,use_default, u_loc, nlocal, ne_local, l_n_var_adapt , l_n_var_adapt_index, &
       scl, scl_fltwt) !add
    USE curvilinear_mesh
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: flag ! 0 during initial adaptation, then 1 during time advancement
    LOGICAL , INTENT(INOUT) :: use_default
    INTEGER, INTENT (IN) :: nlocal, ne_local
    REAL (pr), DIMENSION (1:nlocal,1:ne_local), INTENT (IN) :: u_loc
    LOGICAL , INTENT (IN) :: l_n_var_adapt(ne_local)
    INTEGER , INTENT (IN) :: l_n_var_adapt_index(1:ne_local)
    REAL (pr), DIMENSION (1:ne_local), INTENT (INOUT) :: scl
    REAL (pr) ,  INTENT(IN) ::scl_fltwt !weight for temporal filter on scl
    LOGICAL  , SAVE :: startup_init = .TRUE.

    !
    ! Ignore the output of this routine and use default scales routine
    !
    use_default = .FALSE. 

    IF( .NOT. use_default ) THEN
       CALL compressible_scales(flag,use_default, u_loc, nlocal, ne_local, l_n_var_adapt , l_n_var_adapt_index, scl, scl_fltwt) 
       CALL curv_mesh_scales(flag,use_default, u_loc, nlocal, ne_local, l_n_var_adapt , l_n_var_adapt_index, scl, scl_fltwt) 
       startup_init = .FALSE.
    END IF

  END SUBROUTINE user_scales

  SUBROUTINE user_cal_cfl (use_default, ulocal, cfl_out)
    USE precision
    USE sizes
    USE pde
    USE variable_mapping
    IMPLICIT NONE
    LOGICAL , INTENT(INOUT) :: use_default
    REAL (pr),                                INTENT (INOUT) :: cfl_out
    REAL (pr), DIMENSION (nwlt,n_integrated), INTENT (IN)    :: ulocal

    INTEGER                    :: i, idim
    REAL (pr), DIMENSION(nwlt,dim) :: cfl
    REAL (pr), DIMENSION(nwlt,dim) :: v 
    REAL (pr)                  :: min_h
    REAL (pr) :: min_w, max_w
    REAL (pr) :: inv_vortex_timescale, cfl_vortex

    use_default = .FALSE.
    CALL compressible_cal_cfl (use_default, ulocal, cfl_out)

    IF(dim .EQ. 3) THEN
       min_w = MINVAL(ulocal(:,n_mom(3))/ulocal(:,n_den))
       max_w = MAXVAL(ulocal(:,n_mom(3))/ulocal(:,n_den))
       CALL parallel_global_sum(REALMINVAL=min_w)
       CALL parallel_global_sum(REALMAXVAL=max_w)
    END IF

    IF (par_rank.EQ.0) THEN
       PRINT *, '**********************************************************'
       !PRINT *, 'BL pts = ', 1.0_pr/((Re)**0.5_pr*min_h)
       !PRINT *, 'Re_tau = ', Re_tau
       IF(dim .EQ.3)PRINT *, 'min/max w = ',min_w, max_w
       PRINT *, '**********************************************************'
       
    END IF
  END SUBROUTINE user_cal_cfl

  SUBROUTINE user_init_sgs_model( )
    IMPLICIT NONE
  END SUBROUTINE user_init_sgs_model

  SUBROUTINE  user_sgs_force (u_loc, nlocal)
    USE variable_mapping
    USE sgs_compressible 
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: nlocal
    REAL (pr), DIMENSION (nlocal,n_integrated), INTENT (INOUT) :: u_loc

    IF( sgsmodel_compress .NE. sgsmodel_compress_none ) CALL sgs_compress_force( u_loc(1:nwlt,1:n_integrated),nlocal)

  END SUBROUTINE  user_sgs_force

  FUNCTION user_sound_speed (u, neq, nwlt)
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: nwlt, neq
    REAL (pr), DIMENSION (nwlt,neq), INTENT(IN) :: u
    REAL (pr), DIMENSION (nwlt) :: user_sound_speed,gam

    user_sound_speed(:) = calc_speed_of_sound( u, nwlt, neq )

  END FUNCTION user_sound_speed

  SUBROUTINE  user_pre_process
    USE variable_mapping
    USE sgs_compressible 
    IMPLICIT NONE

    ! Random noise
    INTEGER :: iseedsize
    REAL (pr), DIMENSION(nwlt) :: rand_array
    INTEGER, ALLOCATABLE, DIMENSION(:) :: seed
    INTEGER, SAVE :: add_noise=.TRUE.

    REAL (pr), DIMENSION(1:nwlt,1:n_integrated) :: u_tmp 
    INTEGER :: i, l
    REAL (pr), DIMENSION(nwlt) :: mu_loc, kappa_loc, Temperature
!    REAL (pr), DIMENSION(nwlt) :: Pressure 
    REAL (pr)                  :: a0_in, cv_loc
    REAL (pr)                  :: tmp1, tmp2, tmp3
    REAL (pr)                  :: rho_m_before, rho_m_after 

    LOGICAL, SAVE :: first_skip = .TRUE.   !  Skip the first iteration - for sgs_calc_forcing control to allow restart.  Forcing needs to be called in wlt_3d_main once compressible variables and properties are set up
    LOGICAL, SAVE :: filter_field_flag = .TRUE.

    CALL compressible_pre_process() 

    ! Use a damping function for constant coefficient smagorinsky
    IF( sgsmodel_compress .EQ. sgsmodel_compress_fixSM ) THEN
      rand_array(:) = 1.0_pr - EXP( -(1.0_pr - ABS(x(:,2)) )*Re_tau/van_driest_damping ) 

      ! Modify the eddy viscosity
      mu_turb(:) = mu_turb(:) * rand_array(:) * rand_array(:) 
      kk_turb(:) = kk_turb(:) * rand_array(:) * rand_array(:) 
      IF(Nspec>1)bD_turb(:) = bD_turb(:) * rand_array(:) * rand_array(:) 

    END IF 

!    IF(add_noise .AND. t<dt) THEN  ! only call during initial run to avoid wiping out solution
!    !IF(add_noise) THEN  ! only call during initial run to avoid wiping out solution
!        call RANDOM_SEED(SIZE=iseedsize)  
!        ALLOCATE(seed(iseedsize))
!        seed(:)=1234
!        call RANDOM_SEED(PUT=seed)
!        DEALLOCATE(seed)
!        !generate random coefficient
!        call RANDOM_NUMBER(rand_array)


!        IF(par_rank ==0) PRINT *, 'Random noise added to z-momentum'
!        u(:,n_mom(3)) = 0.01_pr*(rand_array - 0.5_pr)
    
!        add_noise = .FALSE.
!     END IF


    !IF(par_rank ==0) PRINT *, 'user_pre_process'

    IF ( filter_field .AND. filter_field_flag ) THEN
       IF( par_rank .EQ. 0 ) PRINT *, 'FILTERING FIELD FOR STABILIZATION'
       rho_m_before        = SUM(u(:,n_den)*dA)
       u_tmp(1:nwlt,1:n_integrated) = u(1:nwlt,1:n_integrated)
       CALL local_lowpass_filt( u_tmp(:,1:n_integrated), j_lev, nwlt, n_integrated, 1, n_integrated,&
             lowpassfilt_support, lowpass_filt_type, tensorial_filt)
       u(1:nwlt,1:n_integrated) = 0.75_pr*u(1:nwlt,1:n_integrated) + 0.25_pr*u_tmp(1:nwlt,1:n_integrated) ! To avoid zero coefficients
       ! maintain total mass
       rho_m_after        = SUM(u(:,n_den)*dA)
       CALL parallel_global_sum(REAL    = rho_m_before   )   
       CALL parallel_global_sum(REAL    = rho_m_after    )   
       u(:,n_den) = u(:,n_den) * rho_m_before / rho_m_after

       filter_field_flag = .FALSE.
    END IF  
    !IF(compressible_SCALES ) CALL SGS_compressible_pre_process(u(:,1:n_integrated),n_integrated,nwlt)
    !IF(compressible_SCALES .AND. .NOT.( first_skip ) .AND. it > 0) THEN 
    !!!!!IF(compressible_SCALES ) THEN 
    !   cv_loc = (cp_in(Nspec)-1.0_pr/MW_in(Nspec)/Ma**2.0_pr)
    !   !calculate mu (dynamic) 
    !   !Pressure    = (u(:,n_eng)-0.5_pr*SUM(u(:,n_mom(1):n_mom(dim))**2,DIM=2)/u(:,n_den))/( cp_in(Nspec) /(1.0_pr/MW_in(Nspec)/Ma**2.0_pr)-1.0_pr) 
    !   !Temperature = u(:,n_prs)/(1.0_pr/MW_in(Nspec)/Ma**2.0_pr)/u(:,n_den)  !Temperature
    !   Temperature = (u(:,n_eng) - 0.5_pr*SUM(u(:,n_mom(1):n_mom(dim))**2.0_pr,DIM=2)/u(:,n_den)) /cv_loc /u(:,n_den) !Temperature
    !   mu_loc      = mu_in(Nspec) * (1.0_pr+S1)/(Temperature +S1)*(Temperature)**(1.5_pr)  !mu_loc - dynamic viscosity
    !   kappa_loc   = kk_in(Nspec) * (1.0_pr+S1)/(Temperature +S1)*(Temperature)**(1.5_pr)  !kappa_loc - dynamic conductivity 
    !   !Sanity check
    !   IF (verb_level.GT.0 ) THEN
    !      tmp1 = SUM(Temperature*dA)
    !      tmp2 = SUM(mu_loc*dA)
    !      tmp3 = SUM(dA)
    !      CALL parallel_global_sum(REAL = tmp1)
    !      CALL parallel_global_sum(REAL = tmp2)
    !      CALL parallel_global_sum(REAL = tmp3)
    !      IF(par_rank .EQ. 0) PRINT *, 'usr_pre_process: preparing to call sgs_compressible_model_setup and _model_force'
    !      IF(par_rank .EQ. 0) PRINT *, 'SANITY CHECK: nondimensional, volume-averaged TEMPERATURE/MU:', tmp1/tmp3, tmp2/tmp3
    !   END IF
    !
    !   a0_in = 1.0_pr/Ma          !nondimensional based on freestream velocity      
    !   ! if it < = 0, this performs initialization
    !   CALL sgs_compressible_force ( u, nwlt, mu_loc, kappa_loc, cv_loc, a0_in, MW_in(Nspec), 1.0_pr/MW_in(Nspec)/Ma**2.0_pr )  !ERIC: double check c_v
    !END IF 

    !IF ( do_Adp_Eps_Spatial .OR. do_Adp_Eps_Spatial_Evol ) &
    !CALL user_pre_process__VT
  
    first_skip = .FALSE.
 

  END SUBROUTINE user_pre_process

  SUBROUTINE  user_post_process
    USE variable_mapping
    IMPLICIT NONE

    CALL compressible_post_process() 
    !IF(compressible_SCALES .AND. it > 0) CALL SGS_compressible_post_process(u(:,1:n_integrated),n_integrated,nwlt)

    IF(hypermodel .NE. 0 .AND. ( hyper_it .LT. hyper_it_limit ) ) hyper_it = hyper_it + 1 


  END SUBROUTINE user_post_process

  FUNCTION initial_velocity_spectra( nloc, k0, direction, RNDM_OFFSET_SEED ) 
    IMPLICIT NONE
    INTEGER,                      INTENT(IN) :: k0, direction, nloc
    INTEGER, OPTIONAL,            INTENT(IN) :: RNDM_OFFSET_SEED 
    REAL (pr), DIMENSION(nloc)               :: initial_velocity_spectra

    INTEGER :: k      ! Iterators
    INTEGER :: nyquist      ! nyquist wavenumber 

    REAL (pr)                           :: offset, spectra
    INTEGER                             :: iseedsize
    INTEGER, ALLOCATABLE, DIMENSION(:)  :: seed
    
    IF( direction < 1 .OR. direction > dim ) THEN
      PRINT *, 'Cannot compute spectra for invalid direction', direction, 'of', dim
      STOP
    END IF
    
    IF( PRESENT(RNDM_OFFSET_SEED)) THEN 
      CALL RANDOM_SEED(SIZE=iseedsize)  
      ALLOCATE(seed(iseedsize))
      seed(:) = RNDM_OFFSET_SEED
      CALL RANDOM_SEED(PUT=seed)
      DEALLOCATE(seed)
    END IF

    initial_velocity_spectra = 0.0_pr

    offset = 0.0_pr
    nyquist = (mxyz(direction)*2**(j_mx - 1) + 1 - prd(direction))/2
    DO k = 0, nyquist/4
      ! Energy spectra
      spectra = ( REAL(k,pr)/REAL(k0,pr) )**4 * EXP( -2.0_pr *  ( REAL(k,pr)/REAL(k0,pr) )**2 )
      IF( PRESENT(RNDM_OFFSET_SEED)) call RANDOM_NUMBER(offset)
      !IF(par_rank .EQ. 0)PRINT *, 'spectra with offset', spectra, offset
      initial_velocity_spectra = initial_velocity_spectra + &
        spectra * SIN( 2.0_pr * PI * REAL(k,pr) * ( x(:,direction) / (xyzlimits(2,direction) - xyzlimits(1,direction) )+ offset + ( 0.25_pr - 0.25_pr*REAL(MOD(k,2),pr) )   ) )
    END DO



  END FUNCTION initial_velocity_spectra


  !! ADD_SMOOTH_NOISE:  Adds gaussian perturbations at random locations to the
  !given field
  ! field       - field to add noise to
  ! nloc        - number of points in the field
  ! number_points - number of noise perturbations to add
  ! scale       - height of the perturbations
  ! seed_input  - random seed for reproducability across initial adaptation cycles
  ! OPTIONAL ARGUMENTS:
  ! DEFINE_X    - specify perturbation locations on the x plane
  ! DEFINE_Y    - specify perturbation locations on the y plane
  ! DEFINE_Z    - specify perturbation locations on the z plane
  SUBROUTINE add_smooth_noise( field, nloc, number_points, scale, seed_input, DEFINE_X, DEFINE_Y, DEFINE_Z )
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: nloc, number_points, seed_input
    REAL (pr), DIMENSION(nloc), INTENT(INOUT) :: field
    REAL (pr), INTENT (IN) :: scale
    REAL (pr), OPTIONAL, INTENT (IN) :: DEFINE_X, DEFINE_Y, DEFINE_Z

    INTEGER :: iseedsize
    REAL (pr), DIMENSION(nwlt) :: rand_array
    INTEGER, ALLOCATABLE, DIMENSION(:) :: seed
    REAL (pr), DIMENSION(3) :: x_loc

    INTEGER :: ipoint, idim, iloc



    CALL RANDOM_SEED(SIZE=iseedsize)  
    
    ALLOCATE(seed(iseedsize))
    seed(:) = seed_input 
    CALL RANDOM_SEED(PUT=seed)
    DEALLOCATE(seed)

       
    DO ipoint = 1,number_points
      ! generate random point
      call RANDOM_NUMBER(x_loc)
      IF( par_rank .EQ. 0 ) THEN 
        x_loc(1:dim) = x_loc(1:dim) * (xyzlimits(2,1:dim) - xyzlimits(1,1:dim)) + xyzlimits(1,1:dim)
        !PRINT *, 'NOISE:', par_rank, INT( 1.0_pr - 2.0_pr * MODULO(ipoint, 2 )) * ipoint, x_loc(1), x_loc(2), x_loc(3)    
      ELSE
        x_loc = 0.0_pr
      END IF  
      CALL parallel_vector_sum( REAL=x_loc, LENGTH=3)
  
      IF( PRESENT(DEFINE_X) ) x_loc(1) = DEFINE_X
      IF( PRESENT(DEFINE_Y) ) x_loc(2) = DEFINE_Y
      IF( PRESENT(DEFINE_Z) ) x_loc(3) = DEFINE_Z

      ! Scale and generate point
      DO iloc = 1,nloc
        field(iloc) = field(iloc) + ( 1.0_pr - 2.0_pr * MODULO(ipoint, 2 )) * scale * EXP( - (SUM( ( x(iloc,1:dim) - x_loc(1:dim))**2 ))/ ( 2.0_pr*MINVAL(h(1,1:dim)/2.0_pr**(j_mx - 1)))   )
      END DO
    END DO

  END SUBROUTINE add_smooth_noise

  SUBROUTINE calc_total_KE( momentum, density, total_KE, nloc )
    IMPLICIT NONE
    INTEGER,                        INTENT(IN)  :: nloc
    REAL (pr), DIMENSION(nloc,dim), INTENT(IN)  :: momentum
    REAL (pr), DIMENSION(nloc),     INTENT(IN)  :: density 
    REAL (pr),                      INTENT(OUT) :: total_KE 

    INTEGER   :: idim
    total_KE = SUM(  0.5_pr * momentum(:,1) * momentum(:,1) * dA(:) / density(:) )
    DO idim = 2,dim
      total_KE = total_KE + SUM(  0.5_pr * momentum(:,idim) * momentum(:,idim) * dA(:) / density(:) )
    END DO

    CALL parallel_global_sum( REAL=total_KE )

  END SUBROUTINE calc_total_KE

  ! Renormalize density based on the bulk density
  SUBROUTINE renormalize_density( u, nlocal, ne_local ) 
    IMPLICIT NONE
    INTEGER,                        INTENT(IN)  :: nlocal, ne_local
    REAL (pr), DIMENSION(nlocal,ne_local), INTENT(INOUT)  :: u

    REAL (pr) :: rho_m, volume_total

    INTEGER   :: idim

    !Bulk averaged values
    rho_m        = SUM(u(:,n_den)*dA)
    volume_total = SUM(dA)
   
    CALL parallel_global_sum(REAL    = rho_m                      )   
    CALL parallel_global_sum(REAL    = volume_total               )   

    !Bulk Averaging
    rho_m    = rho_m/volume_total

    u(:,n_den) = u(:,n_den) / rho_m

  END SUBROUTINE renormalize_density 


  !
  ! Calculate plane statitics
  !
  ! u_plane: variables to extract on the planes 
  ! abscissa: abscissa to plot against, for generality
  ! var_names: variable names to include in files
  ! plane_ids: vector of plane ids to extract (allows subset)
  !
  SUBROUTINE extract_planes ( u_plane, abscissa, var_names, n_var_loc, n_planes_loc, plane_ids )
    USE precision
    USE db_tree
    USE wlt_vars
    USE vector_util_mod
    USE parallel            ! par_size
    USE variable_mapping 
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: n_var_loc, n_planes_loc
    REAL (pr), DIMENSION (nwlt,1:n_var_loc), INTENT (IN) :: u_plane
    REAL (pr), DIMENSION (nwlt,1:dim), INTENT (IN) :: abscissa 
    INTEGER, DIMENSION (n_planes_loc), INTENT (IN) :: plane_ids 
    CHARACTER (LEN=u_variable_name_len), DIMENSION(n_var_loc)  :: var_names 
    CHARACTER (LEN=256)  :: plane_filename

    INTEGER :: i_var, i_loc, ii
    INTEGER :: next_dim1, next_dim2 

    REAL(pr), DIMENSION(:,:), ALLOCATABLE :: extracted_vars, extracted_coords 

    !================= PLANE STATS ====================
    INTEGER, DIMENSION(dim) :: plane_size
    REAL (pr) TMPij(nwlt,2*dim), tmp(nwlt)  !tmp for Sij

    INTEGER, DIMENSION(:), ALLOCATABLE :: momN, momSHIFT, dir_line
    INTEGER :: dir_homog, mom, inwlt(1)
    INTEGER :: i_plane, j_plane, i_line, proc
    INTEGER :: i, idim, step, ixyz(1:dim), ixyz_plane(1:dim)
    LOGICAL , SAVE :: start = .TRUE.
    !==============================================

    INTEGER, PARAMETER :: LOCAL_UNIT = 9324

    !--Calculate statistics on the additional planes
    IF (additional_planes_active .AND. dim .EQ. 3 ) THEN
       IF (par_rank.EQ.0) WRITE(*,'("STATISTICS ON THE ADDITIONAL PLANES")')

       j_plane = MIN(j_additional_planes,j_lev)
       step = MAX(1,2**(j_lev-j_plane))
       DO idim = 1,dim
         next_dim1 = MOD(idim,dim) + 1
         next_dim2 = MOD(idim+1,dim) + 1
         plane_size(idim) =  (nxyz(next_dim1)/step+1-prd(next_dim1)) * (nxyz(next_dim2)/step+1-prd(next_dim2))
       END DO
       ALLOCATE( extracted_vars(n_var_loc,MAXVAL(plane_size)), extracted_coords(dim,MAXVAL(plane_size))  )
       ALLOCATE(momN(n_additional_planes),momSHIFT(n_additional_planes),dir_line(n_additional_planes))


       DO ii = 1,n_planes_loc!n_additional_planes
          i_plane = plane_ids(ii)
          IF ( i_plane .LT. 1 .OR. i_plane .GT. n_additional_planes ) THEN
             CALL error('Invalid plane ID in EXTRACT_PLANES')
          END IF

          HOMOG_LOOP: DO idim=1,dim
             IF(prd(idim) == 1 .AND. idim .NE. dir_planes(i_plane)) THEN
                dir_homog = idim
                !dir_homog = 3 
                EXIT HOMOG_LOOP
             END IF
          END DO HOMOG_LOOP

          !IF(prd(dir_planes(i_plane)) /= 1) THEN
          IF(.TRUE.) THEN
             DO idim=1,dim
                IF(idim /= dir_homog .AND. idim /= dir_planes(i_plane)) dir_line(i_plane)=idim
             END DO
             i=0
             DO WHILE (xx(i*STEP,dir_line(i_plane)) <= xyzzone(1,dir_line(i_plane)) .AND. xx(i*STEP,dir_line(i_plane)) < xyzlimits(2,dir_line(i_plane)) )
                i=i+1
             END DO
             momSHIFT(i_plane)=i-1
             i=nxyz(dir_line(i_plane))/STEP
             DO WHILE (xx(i*STEP,dir_line(i_plane)) >= xyzzone(2,dir_line(i_plane)) .AND. xx(i*STEP,dir_line(i_plane)) > xyzlimits(1,dir_line(i_plane)) )
                i=i-1
             END DO
             momN(i_plane)=i-momSHIFT(i_plane)

             DO idim = 1, dim
                ixyz_plane(idim) = (MINLOC( ABS(xx(0:nxyz(idim)-prd(idim):step,idim) - xyz_planes(idim,i_plane)), DIM = 1  ) - 1 )*step
             END DO
             !---------- go through all points on the plane for  j <= j_plane ------------------
             extracted_vars = 0.0_pr
             extracted_coords = 0.0_pr
             ixyz(dir_planes(i_plane)) = ixyz_plane(dir_planes(i_plane))
             i_loc = 0
             DO i_line = 1 ,momN(i_plane)
                ixyz(dir_line(i_plane)) = (momSHIFT(i_plane)+i_line)*STEP
                DO i=0,nxyz(dir_homog)-prd(dir_homog),STEP
                   ixyz(dir_homog) = i
                   i_loc = i_loc + 1
                   CALL DB_get_proc_by_coordinates (ixyz, j_lev, proc)
                   IF (proc.EQ.par_rank) THEN
                      CALL get_indices_by_coordinate(1,ixyz(1:dim),j_lev,inwlt(1), 0)
                      DO i_var = 1,n_var_loc
                         extracted_vars(i_var,i_loc) = u_plane(inwlt(1),i_var)
                      END DO
                      DO i_var = 1,dim
                         extracted_coords(i_var,i_loc) = abscissa(inwlt(1),i_var)
                      END DO
                   END IF
                END DO
             END DO
             CALL parallel_vector_sum( REAL=extracted_vars, LENGTH=(plane_size(dir_planes(i_plane))*n_var_loc) )
             CALL parallel_vector_sum( REAL=extracted_coords, LENGTH=(plane_size(dir_planes(i_plane))*dim) )
             IF (par_rank.EQ.0)  THEN
                !---------- write output file for time-average ------------------
                WRITE(plane_filename,'(A,I4.4,A,I4.4,A)') TRIM(res_path)//TRIM(file_gen),iwrite,'_plane_', i_plane,'.dat'
                ! Overwrite plane file
                OPEN (LOCAL_UNIT , FILE = plane_filename, FORM='formatted', STATUS='unknown')
                WRITE(LOCAL_UNIT ,ADVANCE='NO', FMT='(A1,A11,A2,A12,A2,A12,A2)' ) '%','X',', ','Y ',', ', 'Z',', '
                DO i_var = 1,n_var_loc-1
                   WRITE(LOCAL_UNIT ,ADVANCE='NO', FMT='(A12,A2)' ) var_names(i_var), ', ' 
                END DO
                WRITE(LOCAL_UNIT ,ADVANCE='YES', FMT='(A12)' ) var_names(n_var_loc) 
                DO i_loc = 1,plane_size(dir_planes(i_plane))
                  DO i_var = 1,dim
                     WRITE(LOCAL_UNIT ,ADVANCE='NO', FMT='(E12.5)' ) extracted_coords(i_var,i_loc) 
                     WRITE(LOCAL_UNIT ,ADVANCE='NO', FMT='(A2)' ) ', ' 
                  END DO
                  DO i_var = 1,n_var_loc-1
                     WRITE(LOCAL_UNIT ,ADVANCE='NO', FMT='(E12.5)' ) extracted_vars(i_var,i_loc) 
                     WRITE(LOCAL_UNIT ,ADVANCE='NO', FMT='(A2)' ) ', ' 
                  END DO
                  WRITE(LOCAL_UNIT ,ADVANCE='YES', FMT='(E12.5)' ) extracted_vars(n_var_loc,i_loc) 
                END DO
                CLOSE (LOCAL_UNIT)
             END IF

          END IF

       END DO

       DEALLOCATE( extracted_vars, extracted_coords )
       DEALLOCATE(momN,momSHIFT,dir_line)
    END IF

  END SUBROUTINE extract_planes 
  

END MODULE user_case
