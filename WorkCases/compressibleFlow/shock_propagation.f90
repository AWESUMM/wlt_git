!$
!!$!****************************************************************************
!!$!* Module for adding Brinkman Penalization terms onto the full NS equations *
!!$!*  -Only valid for single species 10/7/2011                                *
!!$!*  -R is scaled to 1.0
!!$!****************************************************************************
!!$
!!$

MODULE user_case

  USE precision
  USE wlt_vars
  USE wavelet_filters_mod
  USE elliptic_mod
  USE elliptic_vars
  USE wlt_trns_mod
  USE wlt_trns_vars
  USE wlt_trns_util_mod
  USE io_3d_vars
  USE util_mod
  USE util_vars
  USE share_consts
  USE pde
  USE sizes
  USE share_kry
  USE vector_util_mod
  USE field
  USE input_file_reader
  USE parallel
  USE hyperbolic_solver
  USE equations_compressible
  USE freundBC 

  !
  ! case specific variables
  !
  REAL (pr), PROTECTED :: Re, Pra, Sc, Fr, Ma

  REAL (pr), ALLOCATABLE, DIMENSION (:) :: Lxyz
  INTEGER :: n_var_pressure  ! start of pressure in u array
  INTEGER :: nmsk,n_util
  LOGICAL ::  saveUtil  !4extra - this whole line
  REAL (pr) :: Lczn, tfacczn, itfacczn, itczn, tczn, cczn, czn_min, czn_max
  LOGICAL :: modczn

  REAL (pr) :: grav_coeff
  LOGICAL :: dynamic_gravity

  REAL (pr) :: mesh_cluster_param 

  INTEGER :: hyper_it = 0  ! for temporary hyperbolic solver application - often used to smooth local instabilities
  INTEGER :: hyper_it_limit = 0  ! for temporary hyperbolic solver application - often used to smooth local instabilities

  REAL (pr) :: meanflow 
  REAL (pr) :: rhoL, pL, uL

CONTAINS

   SUBROUTINE  user_setup_pde ( VERB ) 
    USE variable_mapping
    IMPLICIT NONE
    LOGICAL, OPTIONAL :: VERB         ! print debug info
    INTEGER :: i, idim, l
    INTEGER :: n_int_comp, n_add_comp

    IF (par_rank.EQ.0) THEN
       PRINT * ,''
       PRINT *, '**********************Setting up PDE*****************'
       PRINT * ,'CASE: Shock Propagation on an open domain' 
       PRINT *, '*****************************************************'
    END IF

    ! Set up compressible equation variables 
    CALL compressible_setup_pde

    IF (imask_obstacle) THEN
        CALL register_var( 'Mask',       integrated=.FALSE.,  adapt=(/.TRUE.,.TRUE./),   interpolate=(/.TRUE.,.TRUE./),   exact=(/.FALSE.,.FALSE./),   saved=.TRUE., req_restart=.TRUE. )
    END IF

    IF ( saveUtil) THEN 
        CALL register_var( 'Utility',       integrated=.FALSE.,  adapt=(/.FALSE.,.FALSE./),   interpolate=(/.TRUE.,.TRUE./),   exact=(/.FALSE.,.FALSE./),   saved=.TRUE., req_restart=.TRUE. )
    END IF


    CALL setup_mapping() 
    scaleCoeff = 1.0_pr

    ! Finalize setup of compressible equations 
    CALL compressible_finalize_setup_pde
    !CALL set_exact( get_index('Pressure'), (/.TRUE.,.TRUE./) )  ! Set pressure as an exact solution

    n_var_pressure  = n_prs  ! redundant, but required by code.  Unused in this module
    IF(hypermodel .NE. 0 ) THEN
      n_var_mom_hyper(1:dim) = n_mom(1:dim)
      n_var_den_hyper = n_den
    END IF

    nmsk = get_index('Mask') 
    n_util = get_index('Utility') 
    
    IF(hypermodel .NE. 0) THEN
       IF(ALLOCATED(n_var_hyper)) DEALLOCATE(n_var_hyper)
       ALLOCATE(n_var_hyper(1:n_var))
       n_var_hyper = .FALSE.
       !n_var_hyper(n_den) = .TRUE.
       n_var_hyper(n_mom(1:dim)) = .TRUE.
       n_var_hyper(n_eng) = .TRUE.
       IF ( n_mvort .GT. 0) n_var_hyper(n_mvort) = .TRUE.  ! Then the magnitude of vorticity is being tracked and saved
       !n_var_hyper = .TRUE.


       IF(ALLOCATED(n_var_hyper_active)) DEALLOCATE(n_var_hyper_active)
       ALLOCATE(n_var_hyper_active(1:n_integrated))
       n_var_hyper_active = .FALSE.
      ! n_var_hyper_active(n_den) = .TRUE.
!       n_var_hyper_active(n_eng) = .TRUE.
       n_var_hyper_active = .TRUE.

    END IF


    IF (par_rank.EQ.0) THEN
       PRINT *, 'n_integrated = ',n_integrated 

       PRINT *, 'n_var = ',n_var 
       PRINT *, 'n_var_exact = ',n_var_exact 
       PRINT *, '*******************Variable Names*******************'
       DO i = 1,n_var
          WRITE (*, u_variable_names_fmt) u_variable_names(i)
       END DO
       PRINT *, '****************************************************'
    END IF

  END SUBROUTINE  user_setup_pde


  !
  ! Set the exact solution for comparison to the simulated solution
  !
  ! u          - array to fill in the exact solution
  ! nlocal       - number of active wavelets   
  ! ne_local        - total number of equations
  ! t          - time of current time step 
  ! l_n_var_exact_soln_index - index into the elements of u for which we need to 
  !                            find the exact solution
  SUBROUTINE  user_exact_soln (u, nlocal,  t_local, l_n_var_exact_soln)  
    USE variable_mapping
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: nlocal
    REAL (pr), INTENT (IN) ::  t_local
    REAL (pr), DIMENSION (nlocal,n_var_exact), INTENT (INOUT) :: u
    LOGICAL , INTENT (IN) :: l_n_var_exact_soln(n_var)
    INTEGER :: i

  END SUBROUTINE  user_exact_soln

  SUBROUTINE user_initial_conditions (u, nlocal, ne_local, t_local, scl, scl_fltwt,iter)
    USE variable_mapping
    USE penalization
    USE parallel 
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: nlocal, ne_local
    REAL (pr), DIMENSION (nlocal,ne_local), INTENT (INOUT) :: u
    REAL (pr)  :: scl(1:n_var),scl_fltwt
    REAL (pr), INTENT (IN) :: t_local
    INTEGER, INTENT (INOUT) :: iter
    INTEGER :: i, j, idim, l, ie, ii, k
    INTEGER :: iseedsize, jx, jy, jz, sprt
    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc

    REAL (pr), DIMENSION(nlocal,1:dim) :: x_phys 
    REAL (pr), DIMENSION(2,1:dim) :: x_phys_limits

    REAL (pr), DIMENSION(nlocal) :: perturb

    REAL (pr) :: tmp1, tmp2 
    REAL (pr) :: d_thick, delta_trans 
    INTEGER :: nwlt_total, nwlt_max

    ! Shock related params
    REAL (pr) :: EL, rhoR, pRR, ER, SSPEED
    REAL (pr) :: uR
    REAL (pr) :: shock_delta, shock_position 
    REAL (pr) :: hypdelta, alpha, beta 
    INTEGER :: j_shock

    x_phys(:,:) = user_mapping( x, nlocal, t )
    DO idim = 1,dim
       x_phys_limits(1,idim) = MINVAL( x_phys(:,idim) )
       x_phys_limits(2,idim) = MAXVAL( x_phys(:,idim) )
       CALL parallel_global_sum( REALMINVAL=x_phys_limits(1,idim) )
       CALL parallel_global_sum( REALMAXVAL=x_phys_limits(2,idim) )
       IF ( par_rank .EQ. 0 )  PRINT *, idim,'X_phys min = ', x_phys_limits(1,idim), 'max = ',x_phys_limits(2,idim)      
    END DO

    rhoL =  1.0_pr
    uL =    2.0_pr
    uR =    0.0_pr
    pL =    1.0_pr
    shock_delta = 0.01_pr
    j_shock = j_mx
    shock_position = 0.0_pr

    EL = rhoL*uL**2/2.0_pr + pL/(gamm(1) - 1.0_pr)
    hypdelta = shock_delta*h(1, 1)/2.0_pr**(j_shock - 1) ! sometimes hypdelta is too high. NURLYBEK

    alpha = SQRT(rhoL)*(uL - uR); 
    beta = SQRT(1 + 16.0_pr*pL*gamm(1)/(alpha**2*(1.0_pr + gamm(1))**2))
    pRR = pL - 0.25_pr*alpha**2*(gamm(1) + 1.0_pr)*(beta - 1.0_pr)
    rhoR = rhoL*(4.0_pr*pL*gamm(1) - alpha**2*(1.0_pr + gamm(1))*(beta - 1.0_pr))/(2*(alpha**2*(gamm(1) - 1.0_pr) + 2*pL*gamm(1)))
    ER = rhoR*uR**2/2.0_pr + pRR/(gamm(1) - 1.0_pr)
    SSPEED = (uL - uR)*(3.0_pr - gamm(1) + (1.0_pr + gamm(1))*beta)/4.0_pr + uR



  IF (IC_restart_mode > 0 ) THEN !in the case of restart
     !do nothing
  ELSE IF (IC_restart_mode .EQ. 0) THEN

       u(:,n_den) = rhoL + 0.5_pr*(rhoR - rhoL)*(1.0_pr + TANH((x_phys(:, 1) - shock_position)/hypdelta))!*0.5_pr*(1.0_pr - SIGN(1.0_pr, x_phys(:, 1) - shock))
       u(:,n_mom(1:dim)) = 0.0_pr
       u(:,n_mom(1)) = uL + 0.5_pr*(uR - uL)*(1.0_pr + TANH((x_phys(:, 1) - shock_position)/hypdelta))!*0.5_pr*(1.0_pr - SIGN(1.0_pr, x_phys(:, 1) - shock))
       u(:,n_mom(1)) = u(:,n_den) * u(:,n_mom(1)) 
       u(:,n_eng) = pL + 0.5_pr*(pRR - pL)*(1.0_pr + TANH((x_phys(:, 1) - shock_position)/hypdelta))!*0.5_pr*(1.0_pr - SIGN(1.0_pr, x_phys(:, 1) - shock))

       !w = project_U(w)

      u(:,n_eng)  = 0.5_pr*SUM(u(:,n_mom(1:dim))**2.0_pr, DIM=2)/u(:,n_den) + F_compress * u(:,n_eng)/(gamm(1) - 1.0_pr) 


     DO i = 1,dim + 2
        tmp1 = MINVAL(u(:,i))
        tmp2 = MAXVAL(u(:,i))
        CALL parallel_global_sum( REALMINVAL=tmp1 )
        CALL parallel_global_sum( REALMAXVAL=tmp2 )
        IF(par_rank .EQ. 0) PRINT *, 'Initialize var', i, tmp1,tmp2
     END DO
             
     nwlt_max = nwlt
     CALL parallel_global_sum(INTEGERMAXVAL=nwlt_max)
     IF(par_rank .EQ. 0 ) PRINT *, 'nwlt_max:', nwlt_max

  END IF

  ! Initialize Freund zone
  IF ( use_freund_damping .OR. use_freund_convect ) CALL setup_freund()

  IF ( use_freund_damping ) THEN
    freund_damping_target(1:2,1:dim,n_den)    = 1.0_pr 
    freund_damping_target(1:2,1:dim,n_mom(:)) = 0.0_pr 
    freund_damping_target(1:2,1:dim,n_mom(1)) = meanflow 
    freund_damping_target(1:2,1:dim,n_eng)    = 0.5_pr*SUM(freund_damping_target(1:2,1:dim,n_mom(1:dim))**2.0_pr, DIM=3)/freund_damping_target(1:2,1:dim,n_den) &
                                                + freund_damping_target(1:2,1:dim,n_den)*cv_in(Nspec) * F_compress * 1.0_pr 
    freund_damping_active = .TRUE.  
  END IF
  IF ( use_freund_convect ) THEN 
    freund_convect_active = .TRUE.  
  END IF

  nwlt_total = nwlt
  CALL parallel_global_sum(INTEGER=nwlt_total) 
  IF (par_rank .EQ. 0) THEN
        PRINT *, '********************************************'
        PRINT *, '* j_lev = ',j_lev,        '  *' 
        PRINT *, '* nwlt  = ', nwlt_total,  '  *' 
        PRINT *, '********************************************'
  END IF

 
END SUBROUTINE user_initial_conditions

  !--********************************  
  ! Arguments
  ! u         - field on adaptive grid
  ! nlocal      - number of active points
  ! ne_local       - number of equations
  ! t         - current time
  !
  ! Global variables used
  ! ifn       - number of equations
  ! x()
  ! xO
  ! y0
  !--******************************** 
  SUBROUTINE user_algebraic_BC (Lu, u, nlocal, ne_local, jlev, meth)
    !--Defines boundary condition type
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: jlev, meth, ne_local, nlocal
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (INOUT) :: Lu
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (IN)    :: u
    REAL (pr), DIMENSION (ne_local,nlocal,dim) :: du, d2u !uncomment if Neuman BC are used, need to call 

    INTEGER :: ie, i, ii, shift, denshift,eshift
    INTEGER, DIMENSION (dim) :: velshift 
    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc
   
    !shift markers for BC implementation
    denshift  = nlocal*(n_den-1)

    !if Neuman BC are used, need to call 
    CALL c_diff_fast (u, du, d2u, jlev, nlocal, meth, 10, ne_local, 1, ne_local)
 
    DO ie = 1, ne_local
       shift = nlocal*(ie-1)
       !--Go through all Boundary points that are specified
       i_p_face(0) = 1
       DO i=1,dim
          i_p_face(i) = i_p_face(i-1)*3
       END DO
       DO face_type = 0, 3**dim - 1
          face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
          IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
             CALL get_all_indices_by_face (face_type, jlev, nloc, iloc)
             IF(nloc > 0 ) THEN 
                IF( face(1) .EQ. -1  ) THEN  !Xmin, inflow
                   IF(ie >= n_mom(1) .AND. ie <= n_mom(dim) .OR. ie==n_den .OR. ie==n_eng ) THEN   
                      Lu(shift+iloc(1:nloc)) = u(shift+iloc(1:nloc))   !Dirichlet conditions
                   END IF
                ELSE IF(dim >  1 .AND. ABS(face(2)) == 1) THEN         !Ymin and Ymax
                   IF (n_mom(2) == ie) THEN
                      Lu(shift+iloc(1:nloc)) = u(shift+iloc(1:nloc))   ! Dirichlet
                   ELSE
                      Lu(shift+iloc(1:nloc)) = du(ie,iloc(1:nloc),2)   !Neuman conditions
                   END IF
                END IF
             END IF
          END IF
       END DO
    END DO

  END SUBROUTINE user_algebraic_BC

  SUBROUTINE user_algebraic_BC_diag (Lu_diag, nlocal, ne_local, jlev, meth)
    !--Defines boundary condition type
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: jlev, meth, ne_local, nlocal
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (INOUT) :: Lu_diag
    REAL (pr), DIMENSION (nlocal,dim) :: du_diag, d2u_diag  !uncomment if Neuman BC are used

    INTEGER :: ie, i, ii, shift, denshift

    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc
    INTEGER, PARAMETER :: methprev=1

    !BC for both 2D and 3D cases
    INTEGER :: lowvar, highvar

    denshift = nlocal*(n_den-1)

    !if Neuman BC are used, need to call 
    CALL c_diff_diag ( du_diag, d2u_diag, jlev, nlocal, meth, meth, 10)

    DO ie = 1, ne_local
       shift = nlocal*(ie-1)
       !--Go through all Boundary points that are specified
       i_p_face(0) = 1
       DO i=1,dim
          i_p_face(i) = i_p_face(i-1)*3
       END DO
       DO face_type = 0, 3**dim - 1
          face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
          IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
             CALL get_all_indices_by_face (face_type, jlev, nloc, iloc)
             IF(nloc > 0 ) THEN 
                IF( face(1) .EQ. -1  ) THEN  !Xmin, inflow                   
                   IF(ie >= n_mom(1) .AND. ie <= n_mom(dim) .OR. ie==n_den .OR. ie==n_eng ) THEN  !Momentums
                      Lu_diag(shift+iloc(1:nloc)) = 1.0_pr                    !Dirichlet conditions
                   END IF
                ELSE IF(dim >  1 .AND. ABS(face(2)) == 1) THEN                !Ymin and Ymax
                   IF (n_mom(2) == ie) THEN
                      Lu_diag(shift+iloc(1:nloc)) = 1.0_pr                    !Dirichlet conditions
                   ELSE
                      Lu_diag(shift+iloc(1:nloc)) = du_diag(iloc(1:nloc),2)   !Neuman conditions
                   END IF
                END IF
             END IF
          END IF
       END DO
    END DO

    !varibale thresholding for evolution eps
    !IF ( do_Adp_Eps_Spatial_Evol ) &
    !CALL VT_algebraic_BC_diag (Lu_diag, du_diag, nlocal, ne_local, jlev, meth) 
  END SUBROUTINE user_algebraic_BC_diag

  SUBROUTINE user_algebraic_BC_rhs (rhs, ne_local, nlocal, jlev)
    !--Sets rhs for boundary conditions
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: ne_local, nlocal, jlev
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (INOUT) :: rhs

    INTEGER :: ie, i, ii, shift, denshift
    INTEGER, PARAMETER ::  meth=1
    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc
    REAL (pr) :: p_bc

    denshift=nlocal*(n_den-1)

    DO ie = 1, ne_local
       shift = nlocal*(ie-1)
       !--Go through all Boundary points that are specified
       i_p_face(0) = 1
       DO i=1,dim
          i_p_face(i) = i_p_face(i-1)*3
       END DO
       DO face_type = 0, 3**dim - 1
          face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
          IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
             CALL get_all_indices_by_face (face_type, jlev, nloc, iloc)
             IF(nloc > 0 ) THEN
                IF( face(1) .EQ. -1  ) THEN  !Xmin, inflow
                   IF(ie == n_mom(1)) THEN
                      rhs(shift+iloc(1:nloc)) = rhoL * uL 
                   ELSE IF(ie >= n_mom(2) .AND. ie <= n_mom(dim)) THEN
                      rhs(shift+iloc(1:nloc)) = 0.0_pr 
                   ELSE IF(ie == n_den) THEN
                      rhs(shift+iloc(1:nloc)) = rhoL 
                   ELSE IF(ie == n_eng) THEN
                      rhs(shift+iloc(1:nloc)) = 0.5_pr * uL * uL + F_compress*pL/(gamm(1) - 1.0_pr)  
                   END IF
                ELSE IF(dim >  1 .AND. ABS(face(2)) == 1) THEN         !Ymin and Ymax
                   IF (n_mom(2) == ie) THEN
                      rhs(shift+iloc(1:nloc)) = 0.0_pr                 ! Dirichlet
                   ELSE
                      rhs(shift+iloc(1:nloc)) = 0.0_pr                 !Neuman conditions
                   END IF
                END IF
             END IF
          END IF
       END DO
    END DO

  END SUBROUTINE user_algebraic_BC_rhs


  FUNCTION user_chi (nlocal, t_local)
    USE parallel
    USE threedobject
    IMPLICIT NONE
    INTEGER,          INTENT (IN) :: nlocal
    REAL (pr),        INTENT (IN) :: t_local
    REAL (pr), DIMENSION (nlocal) :: user_chi
    REAL (pr), DIMENSION (nlocal) :: dist_loc
    REAL(pr), DIMENSION(nlocal,1) :: forcing
    REAL(pr) :: smooth_coeff

    user_chi = 0.0_pr

  END FUNCTION user_chi

  FUNCTION user_mapping ( xlocal, nlocal, t_local )
    USE curvilinear
    USE curvilinear_mesh
    USE error_handling 
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: nlocal
    REAL (pr), INTENT (IN) :: t_local
    REAL (pr), DIMENSION (nlocal,dim), INTENT(IN) :: xlocal
    REAL (pr), DIMENSION (nlocal,dim) :: user_mapping
    INTEGER :: idim 

    user_mapping(:,1:dim) = xlocal(:,1:dim)

    ! Tensorial stretching 
    DO idim = 1,dim
      IF( transform_dir(idim).EQ.1)user_mapping(:,:) = channel_sinh( user_mapping, nlocal, mesh_cluster_param, idim )
    END DO

  END FUNCTION user_mapping

  FUNCTION user_rhs (u_integrated,scalar)
    IMPLICIT NONE
    REAL (pr), DIMENSION (ng,ne), INTENT(IN) :: u_integrated !1D flat version of integrated variables without BC 
    REAL (pr), DIMENSION (ng), INTENT(IN) :: scalar
    REAL (pr), DIMENSION (ng*ne) :: user_rhs

    INTEGER, PARAMETER :: meth = HIGH_ORDER

    CALL compressible_rhs ( user_rhs, u_integrated, meth) 
    IF ( use_freund_damping ) CALL freund_damping_rhs( user_rhs, u_integrated ) 
    IF ( use_freund_convect ) CALL freund_convect_rhs( user_rhs, u_integrated, j_lev ) 

    IF(hypermodel .NE. 0 .AND. ( hyper_it .LT. hyper_it_limit .OR. hyper_it_limit < 0) ) THEN
      !hyper_mask = set_hyper_mask(ng) 
       CALL hyperbolic(u_integrated,ng,user_rhs)
       IF( par_rank .EQ. 0 ) PRINT *, 'Using Hyperbolic Module'
    END IF

  END FUNCTION user_rhs

  FUNCTION user_Drhs (u_p, u_prev, meth)
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: meth
    REAL (pr), DIMENSION (ng,ne) :: u_p, u_prev
    REAL (pr), DIMENSION (ng*ne) :: user_Drhs

    CALL compressible_Drhs ( user_Drhs, u_p, u_prev, meth ) 
    IF ( use_freund_damping ) CALL freund_damping_drhs( user_Drhs, u_p, u_prev )  
    IF ( use_freund_convect ) CALL freund_convect_drhs( user_Drhs, u_p, u_prev, j_lev ) 

    IF (hypermodel .NE. 0 .AND. ( hyper_it .LT. hyper_it_limit .OR. hyper_it_limit < 0)) THEN
      !hyper_mask = set_hyper_mask(ng) 
      CALL hyperbolic(u_p,ng,user_Drhs)
    END IF

  END FUNCTION user_Drhs

  FUNCTION user_Drhs_diag (meth)
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: meth
    REAL (pr), DIMENSION (ng*ne) :: user_Drhs_diag
    REAL (pr), DIMENSION (ng,ne) :: u_prev
    INTEGER :: ie, shift

    DO ie = 1, ne
       shift=(ie-1)*ng
       u_prev(1:ng,ie) = u_prev_timestep(shift+1:shift+ng)
    END DO

    CALL compressible_Drhs_diag(user_Drhs_diag,u_prev,meth )
    IF ( use_freund_damping ) CALL freund_damping_drhs_diag( user_Drhs_diag, u_prev )  
    IF ( use_freund_convect ) CALL freund_convect_drhs_diag( user_Drhs_diag, u_prev, j_lev ) 

    IF(hypermodel .NE. 0 .AND. ( hyper_it .LT. hyper_it_limit .OR. hyper_it_limit < 0)) THEN
      !hyper_mask = set_hyper_mask(ng) 
      CALL hyperbolic_diag(user_Drhs_diag, ng)
    END IF


  END FUNCTION user_Drhs_diag

  SUBROUTINE user_project (u, p, nlocal, meth)
    USE variable_mapping
    IMPLICIT NONE

    INTEGER, INTENT (IN) :: meth, nlocal
    REAL (pr), DIMENSION (nlocal),     INTENT(INOUT) :: p
    REAL (pr), DIMENSION (nlocal,n_integrated), INTENT(INOUT) :: u

    !no projection for compressible flow
  END SUBROUTINE user_project

  !
  ! Calculate any statitics
  !
  ! startup_flag - 0 when adapting to IC,then 1 inmain integration loop
  !
  SUBROUTINE user_stats ( u, j_mn_local, startup_flag)
    USE parallel
    USE penalization
    USE variable_mapping
    IMPLICIT NONE
    REAL (pr), DIMENSION (nwlt,1:n_var), INTENT (IN) :: u
    INTEGER , INTENT (IN) :: j_mn_local 
    INTEGER , INTENT (IN) :: startup_flag

  END SUBROUTINE user_stats

  SUBROUTINE user_cal_force (u, n, t_local, force, drag, lift)
    !--Calculates drag and lift on obstacle using penalization formula
    IMPLICIT NONE

    INTEGER, INTENT (IN) :: n
    REAL (pr), INTENT (IN) :: t_local
    REAL (pr), DIMENSION (dim), INTENT (INOUT) :: force
    REAL (pr), INTENT (OUT) :: drag, lift
    REAL (pr), DIMENSION (n,dim) :: u
    drag = 0.0_PR
    lift = 0.0_PR
    !
    ! There is no obstacle in flow
    !

  END SUBROUTINE user_cal_force

  !
  ! Read input from "case_name"_pde.inp file
  ! case_name is in string file_name read from command line
  ! in read_command_line_input()
  ! 
  SUBROUTINE user_read_input()
    IMPLICIT NONE
    INTEGER :: i, j, ind
    CHARACTER(LEN=8):: numstrng, procnum
    REAL (pr), ALLOCATABLE, DIMENSION(:) :: YR, gammR  ! temporary calculation quantities 

    CALL compressible_read_input()
    CALL freund_read_input()  

    call input_real ('Re',Re,'stop',' Re: Reynolds Number')
    call input_real ('Pra',Pra,'stop',' Pra: Prandtl Number')
    call input_real ('Sc',Sc,'stop',' Sc: Schmidt Number')
    call input_real ('Fr',Fr,'stop',' Fr: Froude Number')
    call input_real ('Ma',Ma,'stop',' Ma: Mach Number')

    call input_real ('meanflow',meanflow,'stop',' meanflow: mean flow velocity')

    call input_integer ('hyper_it_limit',hyper_it_limit,'stop','  hyper_it_limit: number of iterations to apply the hyperbolic viscosity to smooth local instabilities.  hyper_it_limit < 0 for all iterations')
 
    call input_real ('Lczn',Lczn,'stop', 'Lcoordzn: length of coord_zone on one side')
    call input_real ('tfacczn',tfacczn,'stop', 'tfacczn: factor multiply by time for coord_zone time=L/min(c)*tfac, set <0 for no czn modification')
    call input_real ('itfacczn',itfacczn,'stop', 'itfacczn: factor multiply by initial time for coord_zone itime=Lczn/min(c)*tfac, set <0 to set itime=0')



    call input_real ('grav_coeff',grav_coeff,'stop', 'grav_coeff: coefficient for gravity forcing')
    call input_logical ('dynamic_gravity',dynamic_gravity,'stop', 'dynamic_gravity: use dynamic volume forcing to maintain some bulk flow quantity' )

    !4extra - these 8 input definitions - MUST ALSO ADD TO .inp 
    call input_logical ('saveUtil',saveUtil,'stop', 'saveUtil: saveUtil: save utility variable') 
   
     
    ! channel clow mesh stretchin
    call input_real ('mesh_cluster_param', mesh_cluster_param, 'stop',' mesh_cluster_param: Mesh clustering parameter for curvilinear stretching in boundary layers')
 
    IF (par_rank.EQ.0) THEN
       PRINT *, 'Using Re=', Re     
    END IF

    CALL set_F_compress( 1.0_pr/Ma**2 / gamm(1) )
    ! Set properties
    DO i = 1, Nspec
       IF (GRAV) CALL set_body_force(-grav_coeff, i, 1 )  !sign change for forcing in the positive x-direction
       ! All molecular weights set equal
       !CALL set_thermo_props( 1.0_pr, 1.0_pr/MW_in(i) * gamm_loc(i)/(gamm_loc(i)-1.0_pr), gamm_loc(i), i )  ! To consistantly initialize derived thermo properties 
       CALL set_thermo_props( 1.0_pr/MW_in(i) * gamm(i)/(gamm(i)-1.0_pr), 1.0_pr/MW_in(i), i )  ! To consistantly initialize derived thermo properties 
    END DO

    ! Intermediate quantities for calculating diffusivity
    IF( ALLOCATED( YR ) ) DEALLOCATE( YR )
    IF( ALLOCATED( gammR ) ) DEALLOCATE( gammR )
    ALLOCATE( YR(1:Nspec), gammR(1:Nspec) )
    YR(:) = MW_in(:) / SUM(MW_in(1:Nspec))                                   
    IF( Nspec .GT. 1 ) YR(Nspec) = 1.0_pr - SUM(YR(1:Nspecm)) ! higher accuracy
    gammR = SUM(cp_in(:)*YR(:))/( SUM(cp_in(:)*YR(:))-SUM(YR(:)/MW_in(:)) )   
    DO i = 1, Nspec
       CALL set_dynamic_viscosity( 1.0_pr/Re, i )
       CALL set_conductivity( 1.0_pr/Re/Pra*gammR(i)/(gammR(i)-1.0_pr), i )   
       CALL set_diffusivity( 1.0_pr/Re/Sc, i )
    END DO
    print *, 'gammR',gammR
    print *, 'YR',YR
    DEALLOCATE( YR )
    DEALLOCATE( gammR )


    print *, 'f_comp',F_compress
    print *, 'cp',cp_in
    print *, 'cv',cv_in
    print *, 'R',R_in
    print *, 'mu_in',mu_in
    print *, 'kk_in',kk_in
    print *, 'bd_in',bd_in
    print *, 'gamma',gamm
    print *, 'MW',MW_in
    
     IF (NOT(ALLOCATED(Lxyz))) ALLOCATE(Lxyz(1:dim))

     !pi=4.0_pr*ATAN(1.0_pr)
     Lxyz(1:dim)=xyzlimits(2,1:dim)-xyzlimits(1,1:dim)


  END SUBROUTINE user_read_input

  !
  ! calculate any additional variables
  ! 
  ! arg 
  ! flag  - 0 calledwhile adapting to IC, 1 - called in main time integration loop
  !
  ! These additiona variables are calculated and left in real space.
  !
  SUBROUTINE user_additional_vars( t_local, flag )
    USE penalization
    USE variable_mapping 
    IMPLICIT NONE
    REAL (pr), INTENT (IN) ::  t_local
    INTEGER , INTENT(IN) :: flag ! 0- called during adaption to IC, 1 called during main integration loop
    REAL (pr) :: cp, R, mygr, cmax, uval
    INTEGER :: l, i, j
    REAL (pr), DIMENSION(1,nwlt,MAX(dim,Nspecm,2)) :: du, d2u    
    REAL (pr) :: iterdiff, initdiff, curtau, fofn, deln, delnold, delfofn
    INTEGER, PARAMETER :: meth=1
    REAL (pr), DIMENSION(nwlt,dim*2) :: bigU


    REAL (pr), DIMENSION(nwlt) :: a_wall
    INTEGER :: face_type, nloc, wlt_type, k, j_df
    INTEGER :: ie, i_bnd
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim)   :: face
    INTEGER, DIMENSION(nwlt)  :: iloc
    REAL (pr) :: a_total, rho_wav, rho_m, volume_total, vel_bulk
    REAL (pr), DIMENSION(2) :: tau_wall
    INTEGER,   DIMENSION(2) :: pts_total
    REAL (pr), DIMENSION(nwlt,dim) :: v
    REAL (pr), DIMENSION(dim,nwlt,dim) :: dv, d2v
    INTEGER :: j_wall    !minimum j_level at the wall

    REAL (pr), DIMENSION(nwlt,dim*(dim+1)/2) :: s_ij
    REAL (pr), DIMENSION(nwlt)               :: smod

    LOGICAL, SAVE :: header_flag = .TRUE.

    LOGICAL :: dummy(1:n_var)
 
    IF (saveUtil) THEN
      CALL user_exact_soln ( s_ij(:,1), nwlt,  t_local, dummy) 
      u(:,n_util) = s_ij(:,1) 
    END IF
    IF (imask_obstacle) u(:,nmsk) = penal

    CALL compressible_additional_vars( t_local, flag )


       IF (modczn) THEN
          IF (t>tczn) THEN
             modczn=.FALSE.
             xyzzone(1,1) = czn_min
             xyzzone(2,1) = czn_max            
            IF (par_rank.EQ.0 .AND. verb_level>0) THEN
             PRINT *, '*****************************************************'
             PRINT *, 'FINALIZING TEMPORARY COORD_ZONE'
             PRINT *, 'COORD_ZONE OUTSIDE OF x=[',xyzzone(1,1),',',xyzzone(2,1),']'
             PRINT *, 'TIME: ', t
             PRINT *, 'STOPPING TIME FOR COORD_ZONE: ', tczn
             PRINT *, '*****************************************************'
            END IF
          END IF
       ELSE  
          IF (tfacczn>0.0_pr .AND. itfacczn>0.0_pr .AND. t>itczn) THEN
             itczn=t_end*10.0_pr
             czn_min = xyzzone(1,1)
             czn_max = xyzzone(2,1)
             xyzzone(1,1) = -Lczn
             xyzzone(2,1) =  Lczn
             cczn = MINVAL(SQRT(gamm/MW_in/Ma**2))
             tczn = MAXVAL(ABS(xyzlimits(:,1)))/cczn*tfacczn
            IF (par_rank.EQ.0 .AND. verb_level>0) THEN
             PRINT *, '*****************************************************'
             PRINT *, 'APPLYING TEMPORARY COORD_ZONE'
             PRINT *, 'COORD_ZONE OUTSIDE OF x=[',-Lczn,',',Lczn,']'
             PRINT *, 'MINIMUM WAVE SPEED: ', cczn
             PRINT *, 'MAXIMUM DISTANCE: ', MAXVAL(ABS(xyzlimits(:,1)))
             PRINT *, 'TIME: ', t
             PRINT *, 'STARTING TIME FOR COORD_ZONE: ', itczn
             PRINT *, 'TIME SAFETY FACTOR: ', tfacczn
             PRINT *, 'STOPPING TIME FOR COORD_ZONE: ', tczn
             PRINT *, '*****************************************************'
            END IF
             IF (t<tczn) THEN
                modczn = .TRUE.
             ELSE
                IF (par_rank.EQ.0) PRINT *, '!!!TOO LATE TO START COORD_ZONE: t>tczn!!!'
             END IF
          END IF  
       END IF


  END SUBROUTINE user_additional_vars

  !
  ! calculate any additional scalar variables
  ! 
  ! arg 
  ! flag  - 0 calledwhile adapting to IC, 1 - called in main time integration loop
  !
  SUBROUTINE user_scalar_vars( flag )
    IMPLICIT NONE
    INTEGER , INTENT(IN) :: flag ! 0- called during adaption to IC, 1 called during main integration loop

  END SUBROUTINE user_scalar_vars

  !
  !************ Calculating Scales ***************************
  !
  ! Note the order of the components in the scl array
  ! correspond to u_tn, v_tn, w_tn, u_tn-1, v_tn-1, w_tn-1, u_tn-2, v_tn-2, w_tn-2
  ! 
  SUBROUTINE user_scales(flag,use_default, u_loc, nlocal, ne_local, l_n_var_adapt , l_n_var_adapt_index, &
       scl, scl_fltwt) !add
    USE curvilinear_mesh
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: flag ! 0 during initial adaptation, then 1 during time advancement
    LOGICAL , INTENT(INOUT) :: use_default
    INTEGER, INTENT (IN) :: nlocal, ne_local
    REAL (pr), DIMENSION (1:nlocal,1:ne_local), INTENT (IN) :: u_loc
    LOGICAL , INTENT (IN) :: l_n_var_adapt(ne_local)
    INTEGER , INTENT (IN) :: l_n_var_adapt_index(1:ne_local)
    REAL (pr), DIMENSION (1:ne_local), INTENT (INOUT) :: scl
    REAL (pr) ,  INTENT(IN) ::scl_fltwt !weight for temporal filter on scl
    LOGICAL  , SAVE :: startup_init = .TRUE.

    !
    ! Ignore the output of this routine and use default scales routine
    !
    use_default = .FALSE. 

    IF( .NOT. use_default ) THEN
       CALL compressible_scales(flag,use_default, u_loc, nlocal, ne_local, l_n_var_adapt , l_n_var_adapt_index, scl, scl_fltwt) 
       CALL curv_mesh_scales(flag,use_default, u_loc, nlocal, ne_local, l_n_var_adapt , l_n_var_adapt_index, scl, scl_fltwt) 
       startup_init = .FALSE.
    END IF

  END SUBROUTINE user_scales

  SUBROUTINE user_cal_cfl (use_default, ulocal, cfl_out)
    USE precision
    USE sizes
    USE pde
    USE variable_mapping
    IMPLICIT NONE
    LOGICAL , INTENT(INOUT) :: use_default
    REAL (pr),                                INTENT (INOUT) :: cfl_out
    REAL (pr), DIMENSION (nwlt,n_integrated), INTENT (IN)    :: ulocal

    INTEGER                    :: i, idim
    REAL (pr), DIMENSION(nwlt,dim) :: cfl
    REAL (pr), DIMENSION(nwlt,dim) :: v 
    REAL (pr)                  :: min_h
    REAL (pr) :: min_w, max_w
    REAL (pr) :: inv_vortex_timescale, cfl_vortex

    use_default = .FALSE.
    CALL compressible_cal_cfl (use_default, ulocal, cfl_out)

    IF ( ( use_freund_damping .OR. use_freund_convect) .AND. time_integration_method == TIME_INT_RK ) THEN
      cfl_out = MAX( cfl_out, freund_cal_cfl() )
    END IF 


    IF(dim .EQ. 3) THEN
       min_w = MINVAL(ulocal(:,n_mom(3))/ulocal(:,n_den))
       max_w = MAXVAL(ulocal(:,n_mom(3))/ulocal(:,n_den))
       CALL parallel_global_sum(REALMINVAL=min_w)
       CALL parallel_global_sum(REALMAXVAL=max_w)
    END IF

    IF (par_rank.EQ.0) THEN
       PRINT *, '**********************************************************'
       !PRINT *, 'BL pts = ', 1.0_pr/((Re)**0.5_pr*min_h)
       IF(dim .EQ.3)PRINT *, 'min/max w = ',min_w, max_w
       PRINT *, '**********************************************************'
       
    END IF
  END SUBROUTINE user_cal_cfl

  SUBROUTINE user_init_sgs_model( )
    IMPLICIT NONE
  END SUBROUTINE user_init_sgs_model

  SUBROUTINE  user_sgs_force (u_loc, nlocal)
    USE variable_mapping
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: nlocal
    REAL (pr), DIMENSION (nlocal,n_integrated), INTENT (INOUT) :: u_loc
  END SUBROUTINE  user_sgs_force

  FUNCTION user_sound_speed (u, neq, nwlt)
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: nwlt, neq
    REAL (pr), DIMENSION (nwlt,neq), INTENT(IN) :: u
    REAL (pr), DIMENSION (nwlt) :: user_sound_speed,gam

    user_sound_speed(:) = calc_speed_of_sound( u, nwlt, neq )

  END FUNCTION user_sound_speed

  SUBROUTINE  user_pre_process
    USE variable_mapping
    IMPLICIT NONE

    CALL compressible_pre_process() 

  END SUBROUTINE user_pre_process

  SUBROUTINE  user_post_process
    USE variable_mapping
    IMPLICIT NONE

    CALL compressible_post_process() 

    IF(hypermodel .NE. 0 .AND. ( hyper_it .LT. hyper_it_limit ) ) hyper_it = hyper_it + 1 


  END SUBROUTINE user_post_process

END MODULE user_case

