MODULE user_case

  USE precision
  USE wlt_vars
  USE wavelet_filters_mod
  USE elliptic_mod
  USE elliptic_vars
  USE wlt_trns_mod
  USE wlt_trns_vars
  USE wlt_trns_util_mod
  USE io_3d_vars
  USE util_mod
  USE util_vars
  USE share_consts
  USE pde
  USE sizes
  USE share_kry
  USE vector_util_mod
  USE input_file_reader
  USE parallel
  USE hyperbolic_solver
  !
  ! case specific variables
  !

  REAL (pr) :: MAreport, CFLreport, CFLnonl, CFLconv, CFLacou, CFLbffv, CFLbfft, CFLbffs, CFLvisc, CFLthrm, CFLspec, CFLvort, CFLmods, CFLdiff, maxsos, mydt_orig
  REAL (pr) :: peakchange, Amp_in, mytol, maxMa, lammag, layertop, layerbot, input_dt, cfldt
  REAL (pr) :: sm_dfac, pert_dfac, smTP_dfac, sm_delta, pert_delta, smTP_delta, sm_deltadel, pert_deltadel, smTP_deltadel
  REAL (pr) :: Tconst, Pint, Ptop, newtontol, Amp, nold, gammR, Y_Xhalf
  REAL (pr), ALLOCATABLE, DIMENSION (:) :: pureX, pureY, pureR, pureg, puregamma, YR, olddifftop, difftop, olddiffbot, diffbot, difflay, ydifflay, zdifflay
  REAL (pr) :: e0top, e0bot, rho0top, drho0top, rho0bot, drho0bot, rhoetopBC, rhoebotBC, rhoetop, rhoebot 
  REAL (pr), ALLOCATABLE, DIMENSION (:) :: lams, Y0top, Y0bot, rhoYtopBC, rhoYbotBC, rhoYtop, rhoYbot, Lxyz
  REAL (pr), DIMENSION (2) :: rhoYg, drho0, dP0, c0, P0
  REAL (pr), ALLOCATABLE, DIMENSION (:) ::  mu_in, kk_in, cp_in, MW_in, gamm  
  REAL (pr), ALLOCATABLE, DIMENSION (:) :: bD_in, gr_in
  REAL (pr), ALLOCATABLE, DIMENSION (:) :: globalY, globalrho
  REAL (pr), ALLOCATABLE, DIMENSION (:,:) :: bndvals
  REAL (pr), DIMENSION (5) :: buffRed, buffRef, buffcvd, buffcvf, buffbfd, buffbff, buffdfd, buffdff
  INTEGER :: n_var_pressure  ! start of pressure in u array
  REAL (pr) :: Re, Rebnd, Rep, Pra, Sc, Fr, Ma, At, Gr, icerfdelta, ictanhdelta, threshforpts, dellen, diffdely, diffdelz, diffvisc, diffcfl, difffac
  INTEGER :: Nspec, Nspecm, methpdic, methpd, mykry, mylen, curspec, diffstab, adddifftype, adddiffvisc, adddiffhiord
  INTEGER :: nden, neng, nprs, ndpx, nboy, nmsk, ndom, nton, BCver, ICver, ptsacross, j_ptsacross, delpts
  INTEGER :: limitj_pts, minpts, ptsdiff, ptsdiffy,ptsdiffz,ptsdifftot
  INTEGER :: sm_j, smTP_j, pert_j
  REAL (pr), ALLOCATABLE, DIMENSION (:) :: ks
  REAL (pr), ALLOCATABLE, DIMENSION (:) :: centrho, centrhoY, centY, centX, centxs, centA
  INTEGER  , ALLOCATABLE, DIMENSION (:) :: centN
  INTEGER :: cent_j, cent_n, iwriteclean
  REAL (pr) :: cent_dx, centrhoval, centrhoYval, centYval, centXval, centxrhoval, centxrhoYval, centxYval, centxXval
  INTEGER, ALLOCATABLE :: nvel(:), nspc(:), nvon(:), nson(:) 
  LOGICAL, PARAMETER :: NS=.TRUE. ! if NS=.TRUE. - Navier-Stoke, else Euler
  LOGICAL, PARAMETER :: NSdiag=.TRUE. ! if NSdiag=.TRUE. - use viscous terms in user_rhs_diag
  LOGICAL, PARAMETER :: GRAV=.TRUE. ! if GRAV=.TRUE. - include gravit
  LOGICAL :: sm_alldir, smTP_alldir, pert_alldir
  INTEGER :: smdir, smTPdir, pertdir
  LOGICAL :: doicdiff, pertdoicdiff, diffalldir, pertdiffalldir, stepic, pertstepic, pertuseX, pertuseY, shiftp
  LOGICAL :: adaptAt, AmpisInt, perturbIC, useerf, pertuseerf, hydroln, compdelta, TICconst
  LOGICAL :: usedellen, useminpts, evolBC, buffBC, cnvtBC, diffBC
  LOGICAL :: polybuffRe, polybuff, polydiff, polyconv, polykine, usewall, modwallvals, pBrink
  INTEGER :: dervFlux, PLpinf, ICbasetype, hydropic 
  REAL (pr) :: buffBeta, buffU0, buffSig, buffDomFrac, buffOff, buffFac, pBcoef, pBsig
  LOGICAL :: adaptbuoy, savebuoys, Ypert, ppert, upert, rhopert
  LOGICAL :: adaptMagVort, saveMagVort, adaptComVort, saveComVort, adaptMagVel, saveMagVel, adaptNormS, saveNormS, adaptGradY, saveGradY  !4extra - this whole line
  LOGICAL :: same_filename, erase_old, ComboIMEXRK
  CHARACTER (LEN=64)  ::  stat_filebase
  INTEGER :: nmvt, ncvt(3), nmvl, nnms !4extra - this whole line
  INTEGER, ALLOCATABLE :: ngdy(:) !4extra - this whole line
  REAL (pr) :: layerthresh, Lczn, tfacczn, itfacczn, itczn, tczn, cczn, czn_min, czn_max
  LOGICAL :: docent, boundY, modczn, cuttop, cutbot, splitFBpress, stepsplit, hydrofull, savepardom
  REAL (pr) :: cuttopat, cutbotat, waitrat, maskscl, tempscl, specscl, tempsig, tempamp, visccfl, imexcfl, kineviscbuff, kinevisctran
  INTEGER :: chartype, locnvar, hydrosolver, hydrotype, kinevisc
  LOGICAL :: dobuffRe, buffRe, adaptbuffRe, buffRewait, adaptden, adaptvel, adapteng, adaptspc, adaptprs 
  LOGICAL :: adaptvelonly, adaptspconly, savetmp, adapttmp, addmixu, smoothT, smoothP, getrho, convcfl, temphump, bonusCFL
  LOGICAL :: AlgVals, useorgfiles, savemyres

CONTAINS

   SUBROUTINE  user_setup_pde ( VERB ) 
    USE hyperbolic_solver
    IMPLICIT NONE
    LOGICAL, OPTIONAL :: VERB         ! print debug info
    INTEGER :: i, l   !4extra
    CHARACTER(LEN=8) :: specnum  !4extra
 
    locnvar = dim+Nspec+1
 
    IF (par_rank.EQ.0) THEN
       PRINT * ,''
       PRINT *, '**********************Setting up PDE*****************'
       PRINT * ,'CASE: Rayleigh-Taylor Instability '
       PRINT *, '*****************************************************'
    END IF

    n_integrated = dim + 1 + Nspec      

    n_var_additional = 1

        !IF(hypermodel .NE. 0) THEN
        !   n_var_additional = n_var_additional + 1
        !END IF

    IF (adapttmp .OR. savetmp) n_var_additional = n_var_additional + 1
    nton = n_integrated + n_var_additional

    IF (ALLOCATED(nvon)) DEALLOCATE(nvon)
    ALLOCATE (nvon(dim))
    DO i=1,dim
       IF (adaptvelonly) n_var_additional = n_var_additional + 1
       nvon(i) = n_integrated + n_var_additional
    END DO

    IF (ALLOCATED(nson)) DEALLOCATE(nson)
    ALLOCATE (nson(Nspecm))
    DO l=1,Nspecm
       IF (adaptspconly) n_var_additional = n_var_additional + 1
       nson(l) = n_integrated + n_var_additional
    END DO

    IF (adaptbuffRe) n_var_additional = n_var_additional + 1
    nmsk = n_integrated + n_var_additional

    IF (savebuoys) n_var_additional = n_var_additional + 2
    ndpx = n_integrated + n_var_additional - 1
    nboy = n_integrated + n_var_additional

    IF (savepardom) n_var_additional = n_var_additional + 1
    ndom = n_integrated + n_var_additional

    !4extra - need all these definitions
    IF( adaptMagVort .or. saveMagVort ) n_var_additional = n_var_additional + 1
    nmvt = n_integrated + n_var_additional
    IF( (adaptComVort .or. saveComVort) .AND. dim==2 ) n_var_additional = n_var_additional + 1
    ncvt(:) = n_integrated + n_var_additional
    DO i=1,3
       IF( (adaptComVort .or. saveComVort) .AND. dim==3 ) n_var_additional = n_var_additional + 1
       ncvt(i) = n_integrated + n_var_additional
    END DO
    IF( adaptMagVel  .OR. saveMagVel  ) n_var_additional = n_var_additional + 1
    nmvl = n_integrated + n_var_additional
    IF( adaptNormS   .OR. saveNormS   ) n_var_additional = n_var_additional + 1
    nnms = n_integrated + n_var_additional

    IF (ALLOCATED(ngdy)) DEALLOCATE(ngdy)
    ALLOCATE(ngdy(Nspecm))
    DO l=1,Nspecm
       IF( adaptGradY   .OR. saveGradY   ) n_var_additional = n_var_additional + 1
       ngdy(l) = n_integrated + n_var_additional
    END DO

    n_var = n_integrated + n_var_additional !--Total number of variables

    n_var_exact = 0

    n_var_pressure  = n_integrated + 1 !pressure

!***********Variable orders, ensure 1:dim+1+Nspec are density, velocity(dim), energy, species(Nspecm)**************
    IF (ALLOCATED(nvel)) DEALLOCATE(nvel)
    IF (ALLOCATED(nspc)) DEALLOCATE(nspc)

    ALLOCATE(nvel(dim),nspc(Nspecm))

    nden = 1
    DO i=1,dim 
       nvel(i) = i+1
    END DO
    neng = dim+2
    DO i=1,Nspecm
       nspc(i) = dim+2+i
    END DO
    nprs = dim+2+Nspec

!    nden = dim+Nspec+1
!    DO i=1,dim 
!       nvel(i) = i
!    END DO
!    neng = dim+Nspec
!    DO i=1,Nspecm
!       nspc(i) = dim+i
!    END DO
!    nprs = dim+2+Nspec
!******************************************************************************************************************
    !
    !--Allocate logical arrays 
    !  Must be done after setting number of different types of variables
    !  and before setting logical variable mapping arrays
    !

    CALL alloc_variable_mappings


    !
    ! Fill in variable names for default variables for integration meth 2
    ! These may be changed in the call to the user specified routine
    !  user_setup_pde()
    !
    ! In integrated variables (3D)
    WRITE (u_variable_names(nden), u_variable_names_fmt) 'Density_rho  '
    WRITE (u_variable_names(nvel(1)), u_variable_names_fmt) 'X_Momentum  '
    WRITE (u_variable_names(nvel(2)), u_variable_names_fmt) 'Y_Momentum  '
    IF (dim.eq.3) WRITE (u_variable_names(nvel(3)), u_variable_names_fmt) 'Z_Momentum  '
    WRITE (u_variable_names(neng), u_variable_names_fmt) 'E_Total  '
    DO l=1,Nspecm      
       WRITE (specnum,'(I8)') l          
       WRITE (u_variable_names(nspc(l)), u_variable_names_fmt) TRIM('Species_Scalar_'//ADJUSTL(TRIM(specnum)))//'  '
    END DO
    WRITE (u_variable_names(nprs), u_variable_names_fmt) 'Pressure  '

    IF (adapttmp .OR. savetmp) WRITE (u_variable_names(nton), u_variable_names_fmt) 'Temperature  '
    IF (adaptvelonly) THEN
       WRITE (u_variable_names(nvon(1)), u_variable_names_fmt) 'X_Velocity  '
       WRITE (u_variable_names(nvon(2)), u_variable_names_fmt) 'Y_Velocity  '
       IF (dim.eq.3) WRITE (u_variable_names(nvon(3)), u_variable_names_fmt) 'Z_Velocity  '
    END IF
    IF (adaptspconly) THEN
       DO l=1,Nspecm      
          WRITE (specnum,'(I8)') l          
          WRITE (u_variable_names(nson(l)), u_variable_names_fmt) TRIM('Mass_Frac_'//ADJUSTL(TRIM(specnum)))//'  '
       END DO
    END IF

    IF(adaptbuffRe) WRITE (u_variable_names(nmsk), u_variable_names_fmt) 'ReMask  ' 
    IF (savebuoys) THEN
       WRITE (u_variable_names(ndpx), u_variable_names_fmt) 'dPdx  '
       WRITE (u_variable_names(nboy), u_variable_names_fmt) 'Buoy  '
    END IF
    IF (savepardom)  WRITE (u_variable_names(ndom), u_variable_names_fmt) 'MyDomain  '
    
    !4extra - need all these definitions
    IF( adaptMagVort .or. saveMagVort ) WRITE (u_variable_names(nmvt), u_variable_names_fmt) 'MagVort  '
    IF( (adaptComVort .or. saveComVort) .AND. dim==2 )  WRITE (u_variable_names(ncvt(1)), u_variable_names_fmt) 'ComVort  '
    IF( (adaptComVort .or. saveComVort) .AND. dim==3 ) THEN
       DO l=1,3
          WRITE (specnum,'(I8)') l
          WRITE (u_variable_names(ncvt(l)), u_variable_names_fmt) TRIM('ComVort_'//ADJUSTL(TRIM(specnum)))//'  '
       END DO
    END IF
    IF( adaptMagVel  .OR. saveMagVel  )  WRITE (u_variable_names(nmvl), u_variable_names_fmt) 'MagVel  '
    IF( adaptNormS   .OR. saveNormS   )  WRITE (u_variable_names(nnms), u_variable_names_fmt) 'NormS  '
    IF( adaptGradY   .or. saveGradY   ) THEN
       DO l=1,Nspecm
          WRITE (specnum,'(I8)') l
          WRITE (u_variable_names(ngdy(l)), u_variable_names_fmt) TRIM('GradY_'//ADJUSTL(TRIM(specnum)))//'  '
       END DO
    END IF

    !
    ! setup logical variable mapping arrays
    ! This defined which variables are used for adaptation, Which need to be interpololated to the new grid at 
    ! each time step, which are saved, and for which we have an exect solutiuon we want to check at each time step
    !
    !
    ! setup which components we will base grid adaptation on.
    !
    n_var_adapt = .FALSE. !intialize
    IF (adaptden) n_var_adapt(nden,:)                  = .TRUE.
    IF (adaptvel) n_var_adapt(nvel(1):nvel(dim),:)     = .TRUE.
    IF (adapteng) n_var_adapt(neng,:)                  = .TRUE.
    IF (adaptspc) n_var_adapt(nspc(1):nspc(Nspecm),:) = .TRUE.
    IF (adaptprs) n_var_adapt(nprs,:)                  = .TRUE.

    IF (adapttmp) n_var_adapt(nton,:)                  = .TRUE.
    IF (adaptvelonly) n_var_adapt(nvel(1):nvel(dim),:)     = .FALSE.
    IF (adaptvelonly) n_var_adapt(nvon(1):nvon(dim),:)     = .TRUE.
    IF (adaptspconly) n_var_adapt(nspc(1):nspc(Nspecm),:) = .FALSE.
    IF (adaptspconly) n_var_adapt(nson(1):nson(Nspecm),:) = .TRUE.

    IF (adaptbuffRe) n_var_adapt(nmsk,:)  = .TRUE.
    !IF (savebuoys) n_var_adapt(ndpx,:)    = .TRUE.
    IF (adaptbuoy) n_var_adapt(nboy,:)    = .TRUE.

    !4extra - adapt
    IF( adaptMagVort ) n_var_adapt(nmvt,:)                  = .TRUE.
    IF( adaptComVort ) n_var_adapt(ncvt(1):ncvt(3),:)       = .TRUE.
    IF( adaptMagVel  ) n_var_adapt(nmvl,:)                  = .TRUE.
    IF( adaptNormS   ) n_var_adapt(nnms,:)                  = .TRUE.
    IF( adaptGradY   ) n_var_adapt(ngdy(1):ngdy(Nspecm),:) = .TRUE.

    !--integrated variables at first time level
    !Variables that need to be interpoleted to new adapted grid in initial grid adaptation
    !Variables that need to be interpoleted to new adapted grid at each time step
    n_var_interpolate        = .FALSE. !intialize
    n_var_interpolate(1:n_var_pressure,:) = .TRUE.  

    IF (adapttmp .OR. savetmp) n_var_interpolate(nton,:)                  = .TRUE.
    IF (adaptvelonly) n_var_interpolate(nvon(1):nvon(dim),:)     = .TRUE.
    IF (adaptspconly) n_var_interpolate(nson(1):nson(Nspecm),:) = .TRUE.

    IF (adaptbuffRe) n_var_interpolate(nmsk,:)  = .TRUE.
    IF (savebuoys) n_var_interpolate(ndpx,:)    = .TRUE.
    IF (savebuoys) n_var_interpolate(nboy,:)    = .TRUE.

    !4extra - need these
    IF( saveMagVort ) n_var_interpolate(nmvt,:)                   = .TRUE.
    IF( saveComVort ) n_var_interpolate(ncvt(1):ncvt(3),:)        = .TRUE.
    IF( saveMagVel  ) n_var_interpolate(nmvl,:)                   = .TRUE.
    IF( saveNormS   ) n_var_interpolate(nnms,:)                   = .TRUE.
    IF( saveGradY   ) n_var_interpolate(ngdy(1):ngdy(Nspecm),:)  = .TRUE.

    !
    ! setup which components we have an exact solution for
    !
    n_var_exact_soln = .FALSE. !intialize

    !
    ! setup which variables we will save the solution
    !
    n_var_save = .FALSE. !intialize 
    n_var_save(1:n_var_pressure) = .TRUE. ! save all for restarting code
    IF (adapttmp .OR. savetmp) n_var_save(nton) = .TRUE.
    IF (adaptbuffRe) n_var_save(nmsk)  = .TRUE.
    IF (savebuoys) n_var_save(ndpx)    = .TRUE.
    IF (savebuoys) n_var_save(nboy)    = .TRUE.
    IF (savepardom)  n_var_save(ndom)  = .TRUE.
    IF (adaptvelonly) n_var_save(nvon(1):nvon(dim))     = .TRUE.
    IF (adaptspconly) n_var_save(nson(1):nson(Nspecm)) = .TRUE.


    !4extra - need these
    IF( saveMagVort ) n_var_save(nmvt)     = .TRUE.
    IF( saveComVort ) n_var_save(ncvt(1):ncvt(3))  = .TRUE.
    IF( saveMagVel  ) n_var_save(nmvl)     = .TRUE.
    IF( saveNormS   ) n_var_save(nnms)     = .TRUE.
    IF( saveGradY   ) n_var_save(ngdy(1):ngdy(Nspecm)) = .TRUE.
    n_var_req_restart = n_var_save

    !
    ! Set the maximum number of components for which we have an exact solution
    !
    n_var_exact = MAX( COUNT(n_var_exact_soln(:,0)),COUNT(n_var_exact_soln(:,1)) )

    !
    ! Setup a scaleCoeff array if we want to tweak the scaling of a variable
    ! ( if scaleCoeff < 1.0 then more points will be added to grid 
    !
    ALLOCATE ( scaleCoeff(1:n_var) )
    scaleCoeff = 1.0_pr

    IF(hypermodel .NE. 0) THEN
       IF(ALLOCATED(n_var_hyper)) DEALLOCATE(n_var_hyper)
       ALLOCATE(n_var_hyper(1:n_var))
       n_var_hyper = .TRUE.
       !n_var_hyper(nden) = .TRUE.
       !n_var_hyper(neng) = .TRUE.
       !n_var_hyper = .TRUE.
       !n_var_hyper(nspc(1)) = .TRUE. !calculate visc on species scalar

       IF(ALLOCATED(n_var_hyper_active)) DEALLOCATE(n_var_hyper_active)
       ALLOCATE(n_var_hyper_active(1:n_integrated))
       n_var_hyper_active = .FALSE.  !if all false, will not apply visc
       ! n_var_hyper_active(nden) = .TRUE.
       !       n_var_hyper_active(neng) = .TRUE.
       !n_var_hyper_active(nspc(1)) = .TRUE. !add visc on species scalar

    END IF
 
    IF (par_rank.EQ.0) THEN
       PRINT *, 'n_integrated = ',n_integrated 
       PRINT *, 'n_var = ',n_var 
       PRINT *, 'n_var_exact = ',n_var_exact 
       PRINT *, '*******************Variable Names*******************'
       DO i = 1,n_var
          WRITE (*, u_variable_names_fmt) u_variable_names(i)
       END DO
       PRINT *, '****************************************************'
    END IF
  END SUBROUTINE  user_setup_pde


  !
  ! Set the exact solution for comparison to the simulated solution
  !
  ! u          - array to fill in the exact solution
  ! nlocal       - number of active wavelets   
  ! ne_local        - total number of equations
  ! t          - time of current time step 
  ! l_n_var_exact_soln_index - index into the elements of u for which we need to 
  !                            find the exact solution
  SUBROUTINE  user_exact_soln (uin, nlocal,  t_local, l_n_var_exact_soln)  
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: nlocal
    REAL (pr), INTENT (IN) ::  t_local
    REAL (pr), DIMENSION (nlocal,n_var_exact), INTENT (INOUT) :: uin
    LOGICAL , INTENT (IN) :: l_n_var_exact_soln(n_var)
    INTEGER :: i

  END SUBROUTINE  user_exact_soln

  SUBROUTINE user_initial_conditions (uin, nlocal, ne_local, t_local, scl, scl_fltwt,iter)
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: nlocal, ne_local
    REAL (pr), DIMENSION (nlocal,ne_local), INTENT (INOUT) :: uin
    REAL (pr)  :: scl(1:n_var),scl_fltwt
    REAL (pr), INTENT (IN) :: t_local
    INTEGER, INTENT (INOUT) :: iter
    INTEGER :: i, j, jj, ll
   ! 
   ! User defined variables
   !
    REAL (pr) :: pudu(nlocal,4,Nspec)
    REAL (pr) :: mybD, mycp, myR, mygamm, zdiff, mygr, tempuse, pshift, minzdiff
    REAL (pr) :: ntem, iterdiff, initdiff, mydel, curtau, fofn, deln, delnold, delfofn
    REAL (pr) :: tempdel, Xtopg, Xbotg, Xdelta, layerwidth, actdellen, cutat
    REAL (pr), DIMENSION (Nspec) :: myF, myG, myA
    REAL (pr), DIMENSION (2,Nspec) :: lamda
    REAL (pr), DIMENSION (dim) :: delxyz
    REAL (pr), DIMENSION (Nspecm,nlocal,dim) :: du
    REAL (pr), DIMENSION (Nspecm,nlocal,dim) :: d2u
    REAL (pr), DIMENSION(nlocal) :: Xvals, zdiffs, zdiffsm
    INTEGER :: myiter, bigi
    INTEGER :: Nt, tstp, minzloc, j_del
    INTEGER, PARAMETER :: methdiff=1, IDdiff=01, meth=1
    REAL (pr) :: topbnd, botbnd, buffsize, npert
    REAL (pr), DIMENSION(dim,nwlt) :: h_arr
    INTEGER :: mynwlt_global, fullnwlt, maxnwlt, minnwlt, avgnwlt

     mynwlt_global = nwlt
     fullnwlt = PRODUCT(mxyz(1:dim))
     CALL parallel_global_sum (INTEGER=mynwlt_global)
     npert = REAL(mynwlt_global)
     DO i=1,dim
        npert = npert/REAL(mxyz(i)*2**(j_lev-1)+1-prd(i),pr)
     END DO
     avgnwlt = mynwlt_global/par_size 
     maxnwlt = nwlt
     minnwlt = -nwlt
     CALL parallel_global_sum(INTEGERMAXVAL=maxnwlt)
     CALL parallel_global_sum(INTEGERMAXVAL=minnwlt)
     minnwlt=-minnwlt
     IF (par_rank.EQ.0) THEN
        PRINT *, '*****************BEGIN************************************'
        PRINT *, 'case:    ', file_gen(1:LEN_TRIM(file_gen)-1)
        PRINT *, 'IC iter= ', iter,           '  t       = ', t
        PRINT *, 'iwrite = ', iwrite-1,       '  dt      = ', dt 
        PRINT *, 'j_mx   = ', j_mx,           '  dt_orig = ', dt_original
        PRINT *, 'j_lev  = ', j_lev,          '  twrite  = ', twrite
        PRINT *, 'nwlt_g = ', mynwlt_global,  '  cpu     = ', timer_val(2)
        PRINT *, 'n_mvec = ', fullnwlt,       '  n%      = ', npert
        PRINT *, 'nwlt   = ', nwlt,           '  eps     = ', eps
        PRINT *, 'nwlt_mx= ', maxnwlt,        '  nwlt_av = ', avgnwlt
        PRINT *, 'nwlt_mn= ', minnwlt,        '  p_size  = ', par_size
        PRINT *, '**********************************************************'
     END IF

     CALL get_all_local_h (h_arr)
     IF (ALLOCATED(globalY)) DEALLOCATE(globalY)
     ALLOCATE(globalY(1:nlocal))
     IF (ALLOCATED(globalrho)) DEALLOCATE(globalrho)
     ALLOCATE(globalrho(1:nlocal))

     !___________________________USE THIS FOR LABELING___________________________

     !^^^^^^^^^^^^^^^^^^^^^^^^^^^USE THIS FOR LABELING^^^^^^^^^^^^^^^^^^^^^^^^^^^
     pureX(1)=1.0_pr-peakchange
     pureX(2)=1.0_pr-pureX(1)
     pureY(:)=MW_in(1)*pureX(:)/(MW_in(1)*pureX(:) + MW_in(2)*(1.0_pr-pureX(:)))
     pureg(:)=pureY(:)*gr_in(1)+(1.0_pr-pureY(:))*gr_in(2)
     pureR(:)=pureY(:)/MW_in(1)+(1.0_pr-pureY(:))/MW_in(2)
     puregamma(:)=pureY(:)*cp_in(1)+(1.0_pr-pureY(:))*cp_in(2)  !cp
     puregamma(:)=puregamma(:)/(puregamma(:)-pureR(:))  !gamma=cp/(cp-R)
     c0(:)=SQRT(puregamma(:)*pureR(:)*Tconst)
     Y_Xhalf = MW_in(1)/SUM(MW_in(1:Nspec))

     IF (par_rank.EQ.0 .AND. verb_level>0) THEN
        PRINT *,'MW_in:', MW_in
        PRINT *,'cp_in:', cp_in
        PRINT *,'gamm: ', gamm
        PRINT *,'At:   ', At
        PRINT *,'pureX:', pureX
        PRINT *,'pureY:', pureY
        PRINT *,'Y_Xhalf:', Y_Xhalf
        PRINT *,'pureR:', pureR
        PRINT *,'pureg:', pureg
        PRINT *,'puregamma:', puregamma
     END IF

  IF (IC_restart_mode > 0 ) THEN !in the case of restart
    !if restarts - DO NOTHING
    IF (savepardom) uin(:,ndom) = REAL(par_rank,pr)
  ELSE
     IF (compdelta) THEN
        j_del = j_ptsacross
        IF (limitj_pts == 4) j_del=MIN(j_ptsacross,j_lev)
        IF (limitj_pts == 3) j_del=MIN(j_ptsacross,j_lev_init)
        IF (limitj_pts == 2) j_del=MIN(j_ptsacross,j_mx)
        IF (limitj_pts == 1) j_del=MIN(j_ptsacross,j_mx-1)
        IF (usedellen) THEN
           IF (useminpts) j_del=MIN(j_del,j_lev)
           ptsacross = CEILING(dellen*REAL(mxyz(1)*2**(j_del-1),pr)/Lxyz(1)) + 1  !p-1 is number of intervals
           IF (useminpts) ptsacross = MAX(ptsacross,minpts)
        END IF
        actdellen = REAL(ptsacross-1,pr)*Lxyz(1)/REAL(mxyz(1)*2**(j_del-1),pr)
        IF (par_rank.EQ.0 .AND. verb_level>0) THEN
           PRINT *, ' ptsacross=', ptsacross
           PRINT *, ' dellen=', dellen
           PRINT *, ' actual dellen=', actdellen
           PRINT *, ' j_lev=', j_lev
           PRINT *, ' j_del=', j_del
        END IF
        CALL deltapts(j_del,tempdel,1,ptsacross)
        icerfdelta=tempdel
        CALL deltapts(j_del,tempdel,0,ptsacross)
        ictanhdelta=tempdel
     END IF

    IF (iter == 1) THEN
     !___________________________Finding n___________________________
     nold=10.0_pr
     iterdiff=10.0_pr
     mydel=1.0e-8_pr
     myiter=1
     curtau=0.1_pr
     IF (par_rank.EQ.0) PRINT *,'ENTERING MY NEWTON METHOD FOR n... ','n=',nold
     DO WHILE (iterdiff>newtontol)
        DO bigi=1,2
           ntem=nold
           IF (bigi.eq.1) ntem=ntem+mydel 
           !************f(n)=0 starts here for given ntem resulting fofn****************
           DO j=1,Nspec
              lamda(:,j)=pureg(j)/2.0_pr/Tconst/pureR(j)    
              tempuse = 1.0_pr + ntem**2/lammag**2/puregamma(j)/Tconst/pureR(j) + &
                        (puregamma(j)-1.0_pr)/puregamma(j)/pureR(j)/Tconst*(pureg(j)/ntem)**2 + &
                        (pureg(j)/2.0_pr/lammag/Tconst/pureR(j))**2
              lamda(1,j)=lamda(1,j)+lammag*SQRT(tempuse) 
              lamda(2,j)=lamda(2,j)-lammag*SQRT(tempuse) 
           END DO
      
           myA(:) = pureR(:)*(lammag**2+ntem**2/c0(:)**2)
           myG(1) = lamda(1,1)/(1.0_pr-EXP(xyzlimits(2,1)*(lamda(1,1)-lamda(2,1)))) + lamda(2,1)/(1.0_pr-EXP(xyzlimits(2,1)*(lamda(2,1)-lamda(1,1)))) !Du_1
           myG(2) = lamda(1,2)/(1.0_pr-EXP(xyzlimits(1,1)*(lamda(1,2)-lamda(2,2)))) + lamda(2,2)/(1.0_pr-EXP(xyzlimits(1,1)*(lamda(2,2)-lamda(1,2)))) !Du_2
           myF(:) = lammag**2*pureg(:)/ntem**2           

           IF (BCver<0) THEN
              myG(1) = lamda(2,1)
              myG(2) = lamda(1,2)
           END IF

           fofn = ( myG(1) + myF(1) )/myA(1) - ( myG(2) + myF(2) )/myA(2)  
           !************f(n)=0  ends  here for given ntem resulting fofn**************** 
           IF (bigi.eq.1) delfofn=fofn
        END DO
        deln=fofn*mydel/(delfofn-fofn)  
        IF (myiter.ne.1) curtau = curtau*delnold/(delnold-deln)
        delnold=deln
        nold=nold-deln*curtau
        IF (ABS(nold) < 1.0e-04) nold=nold*1.0e+05
        IF (myiter.eq.1) initdiff=ABS(deln)
        iterdiff=ABS(deln)/initdiff
        IF (ABS(deln)<1.0e-16) iterdiff=0.0_pr

        IF (par_rank.EQ.0 .AND. verb_level>0) THEN
           PRINT *, 'During iter ', myiter, ' iterdiff/initdiff = ', iterdiff 
           PRINT *, 'initdiff=', initdiff, '  n=', nold
           PRINT *, 'tau=',curtau
        END IF
        myiter=myiter+1
     END DO
     nold=ABS(nold)
     IF (par_rank.EQ.0) PRINT *,'LEAVING  MY NEWTON METHOD FOR n... ','n=',nold
     !^^^^^^^^^^^^^^^^^^^^^^^^^^^Finding n^^^^^^^^^^^^^^^^^^^^^^^^^^^
    END IF

     DO bigi=1,Nspec
     !___________________________Setting up individual pressure___________________________
        uin(:,nprs)=Pint*EXP(-x(:,1)*pureg(bigi)/pureR(bigi)/Tconst)  !P
        uin(:,nden)=uin(:,nprs)/pureR(bigi)/Tconst !rho
        hydrofull = .FALSE.
        shiftP = .FALSE.
        IF (ICbasetype == 3) THEN
           hydrotype = 1
           CALL hydroP(uin(:,nprs),uin(:,nden),uin(:,nspc(1)),nlocal,bigi)
           uin(:,nden)=uin(:,nprs)/pureR(bigi)/Tconst !rho
        END IF
        IF (NOT(TICconst)) THEN
           IF (ICbasetype == 1) THEN
              globalrho(:) = uin(:,nden)
              hydrotype = 2
              CALL hydroP(uin(:,nprs),uin(:,nden),uin(:,nspc(1)),nlocal,bigi)
           ELSE IF (ICbasetype == 2) THEN
              CALL c_diff_fast(uin(:,nprs), du(1,:,:), d2u(1,:,:), j_lev, nlocal,meth, 10, 1, 1, 1)  !dP/dx
              uin(:,nden) = -du(1,:,1)/pureg(bigi)
           ELSE
              uin(:,nden)=Pint/pureR(bigi)/Tconst*EXP(-x(:,1)*pureg(bigi)/pureR(bigi)/Tconst)  !rho
           END IF
        END IF
        IF (cuttop .OR. cutbot) THEN
           DO i=1,nlocal
              IF (cuttop .AND. bigi == 1 .AND. (x(i,1) .lt. cuttopat)) THEN
                 uin(i,nprs) = Pint*EXP(-pureg(bigi)/Tconst/pureR(bigi)*cuttopat)
                 uin(i,nden) = uin(i,nprs)/Tconst/pureR(bigi)
              END IF
              IF (cutbot .AND. bigi == 2 .AND. (x(i,1) .gt. cutbotat)) THEN
                 uin(i,nprs) = Pint*EXP(-pureg(bigi)/Tconst/pureR(bigi)*cutbotat)
                 uin(i,nden) = uin(i,nprs)/Tconst/pureR(bigi)
              END IF
           END DO
        END IF
        pudu(:,1,bigi) = uin(:,nprs)
        pudu(:,2,bigi) = uin(:,nden)
     !^^^^^^^^^^^^^^^^^^^^^^^^^^^Setting up individual pressure^^^^^^^^^^^^^^^^^^^^^^^^^^^

     !___________________________u and Du pert___________________________
        IF (AmpisInt) THEN
           Amp=Amp_in
        ELSE
           Amp=Amp_in/nold
        END IF

        IF (perturbIC) THEN
           DO j=1,Nspec
              lamda(:,j)=pureg(j)/2.0_pr/Tconst/pureR(j)    
              tempuse = 1.0_pr + nold**2/lammag**2/puregamma(j)/Tconst/pureR(j) + &
                        (puregamma(j)-1.0_pr)/puregamma(j)/pureR(j)/Tconst*(pureg(j)/nold)**2 + &
                        (pureg(j)/2.0_pr/lammag/Tconst/pureR(j))**2
              lamda(1,j)=lamda(1,j)+lammag*SQRT(tempuse) 
              lamda(2,j)=lamda(2,j)-lammag*SQRT(tempuse) 
           END DO
      
           myF(1)= EXP((lamda(2,1)-lamda(1,1))*xyzlimits(2,1)) - 1.0_pr
           myF(2)= EXP((lamda(1,2)-lamda(2,2))*xyzlimits(1,1)) - 1.0_pr  
           IF (BCver<0) THEN
              myF(:) = 1.0_pr
           END IF

           IF (BCver<0) THEN
              IF (bigi == 1) THEN
                 pudu(:,3,1) = EXP(lamda(2,1)*MAX(x(:,1),cuttopat)) !u^_1
                 pudu(:,4,1) = lamda(2,1)*EXP(lamda(2,1)*MAX(x(:,1),cuttopat)) !u^_1
              ELSE
                 pudu(:,3,2) = EXP(lamda(1,2)*MIN(x(:,1),cutbotat)) !u^_1
                 pudu(:,4,2) = lamda(1,2)*EXP(lamda(1,2)*MIN(x(:,1),cutbotat)) !u^_1
              END IF
           ELSE
              IF (bigi == 1) THEN
                 pudu(:,3,1) = (            EXP(lamda(1,1)*MAX(x(:,1),cuttopat))*EXP((lamda(2,1)-lamda(1,1))*xyzlimits(2,1)) -            EXP(lamda(2,1)*MAX(x(:,1),cuttopat)) )/myF(1)
                 pudu(:,4,1) = ( lamda(1,1)*EXP(lamda(1,1)*MAX(x(:,1),cuttopat))*EXP((lamda(2,1)-lamda(1,1))*xyzlimits(2,1)) - lamda(2,1)*EXP(lamda(2,1)*MAX(x(:,1),cuttopat)) )/myF(1)
              ELSE
                 pudu(:,3,2) = (            EXP(lamda(2,2)*MIN(x(:,1),cutbotat))*EXP((lamda(1,2)-lamda(2,2))*xyzlimits(1,1)) -            EXP(lamda(1,2)*MIN(x(:,1),cutbotat)) )/myF(2)
                 pudu(:,4,2) = ( lamda(2,2)*EXP(lamda(2,2)*MIN(x(:,1),cutbotat))*EXP((lamda(1,2)-lamda(2,2))*xyzlimits(1,1)) - lamda(1,2)*EXP(lamda(1,2)*MIN(x(:,1),cutbotat)) )/myF(2)
              END IF
           END IF
           pudu(:,3:4,bigi) = pudu(:,3:4,bigi)*Amp*nold
        END IF
     !^^^^^^^^^^^^^^^^^^^^^^^^^^^u and Du pert^^^^^^^^^^^^^^^^^^^^^^^^^^^
     END DO
     !___________________________Setting up X and Y___________________________
        IF (ppert) THEN
           IF (AmpisInt) THEN
              Amp=Amp_in
           ELSE
              Amp=Amp_in/nold
           END IF
        ELSE
           Amp = 0.0_pr
        END IF
        CALL smoothX (uin(:,nspc(1):nspc(Nspecm)),nlocal,methdiff,IDdiff)
        uin(:,neng)=uin(:,nspc(1)) !storing X as energy        
        uin(:,neng)=(uin(:,neng)-peakchange)/(1.0_pr-2.0_pr*peakchange)  
                   !undo-ing the peakchange, to be used for discountinuous field solutions.
        uin(:,nspc(1))=MW_in(1)*uin(:,nspc(1))/(MW_in(1)*uin(:,nspc(1))+MW_in(2)*(1.0_pr-uin(:,nspc(1))))   !Y from X 
     !^^^^^^^^^^^^^^^^^^^^^^^^^^^Setting up X and Y^^^^^^^^^^^^^^^^^^^^^^^^^^^

     !___________________________Getting full p and rho solutions___________________________
        DO j=1,Nspec
           uin(:,nvel(j)) = pudu(:,2,j)
        END DO
        IF (ppert) THEN
           DO j=1,Nspec
              CALL c_diff_fast(pudu(:,2,j), du(1,:,:), d2u(1,:,:), j_lev, nlocal,meth, 10, 1, 1, 1)  !drho0/dx
              DO i=1,nlocal
                 uin(i,nvel(j)) = uin(i,nvel(j)) + ( -pudu(i,2,j)/nold*( lammag**2/(lammag**2 + nold**2/c0(j)**2)*(pudu(i,3,j)*pureg(j)/c0(j)**2 - pudu(i,4,j) ) + pudu(i,4,j)) &
                                                    - pudu(i,3,j)*du(1,i,1)/nold ) * SUM(COS(x(i,2:dim)*lams(1:dim-1)))
              END DO
           END DO
        END IF
        CALL smoothpert (uin(:,nvel(1)),uin(:,nvel(2)),uin(:,nden),uin(:,nspc(1)),nlocal,methdiff,IDdiff,2)         
           
        DO j=1,Nspec
           uin(:,nvel(j)) = pudu(:,1,j)
        END DO
        IF (ppert) THEN
           DO j=1,Nspec
              DO i=1,nlocal
                 uin(i,nvel(j)) = uin(i,nvel(j)) + nold/(lammag**2 + nold**2/c0(j)**2)*pudu(i,2,j) * (pudu(i,3,j)*pureg(j)/c0(j)**2 - pudu(i,4,j)) * &
                                    SUM(COS(x(i,2:dim)*lams(1:dim-1)))
              END DO
           END DO
        END IF
        CALL smoothpert (uin(:,nvel(1)),uin(:,nvel(2)),uin(:,nprs),uin(:,nspc(1)),nlocal,methdiff,IDdiff,1)        

        IF (smoothP) CALL mydiffsmooth (uin(:,nprs),nlocal,1,methdiff,IDdiff,smTPdir,smTP_dfac,smTP_delta,smTP_deltadel,smTP_j,j_lev,1,.TRUE.) !smooth P

        IF (smoothT) THEN
           d2u(1,:,1) = (1.0_pr-SUM(uin(:,nspc(1):nspc(Nspecm)),DIM=2))/MW_in(Nspec) 
           DO i=1,Nspecm
              d2u(1,:,1) = d2u(1,:,1) + uin(:,nspc(i))/MW_in(i) !R
           END DO
           d2u(1,:,2) = uin(:,nprs)/uin(:,nden)/d2u(1,:,1) !T
           CALL mydiffsmooth (d2u(1,:,2),nlocal,1,methdiff,IDdiff,smTPdir,smTP_dfac,smTP_delta,smTP_deltadel,smTP_j,j_lev,1,.TRUE.) !smooth T
           IF (getrho) THEN
              uin(:,nden) = uin(:,nprs)/d2u(1,:,1)/d2u(1,:,2)  !rho
           ELSE
              uin(:,nprs) = uin(:,nden)*d2u(1,:,1)*d2u(1,:,2)  !P
           END IF
        END IF
     !^^^^^^^^^^^^^^^^^^^^^^^^^^^Getting full p and rho solutions^^^^^^^^^^^^^^^^^^^^^^^^^^^

     !___________________________Getting full velocity solutions___________________________
        IF (upert) THEN
           DO bigi=2,dim
             DO j=1,Nspec
                DO i=1,nlocal
                   d2u(1,i,j) = lams(bigi-1)/(lammag**2 + nold**2/c0(j)**2) * (pudu(i,3,j)*pureg(j)/c0(j)**2 - pudu(i,4,j)) *SIN(x(i,bigi)*lams(bigi-1))
                END DO
             END DO
             CALL smoothpert (d2u(1,:,1),d2u(1,:,2),uin(:,nvel(bigi)),uin(:,neng),nlocal,methdiff,IDdiff,0)         
           END DO
           DO j=1,Nspec
              DO i=1,nlocal
                 pudu(i,3,j) = pudu(i,3,j)*SUM(COS(x(i,2:dim)*lams(1:dim-1)))
              END DO
           END DO
           CALL smoothpert (pudu(:,3,1),pudu(:,3,2),uin(:,nvel(1)),uin(:,neng),nlocal,methdiff,IDdiff,0)         
        ELSE
           uin(:,nvel(1):nvel(dim)) = 0.0_pr
        END IF
     !^^^^^^^^^^^^^^^^^^^^^^^^^^^Getting full velocity solutions^^^^^^^^^^^^^^^^^^^^^^^^^^^

     !___________________________Setting up perturbed X and Y___________________________

     IF (Ypert .AND. NOT(ppert)) THEN
        IF (AmpisInt) THEN
           Amp=Amp_in
        ELSE
           Amp=Amp_in/nold
        END IF
        CALL smoothX (uin(:,nspc(1):nspc(Nspecm)),nlocal,methdiff,IDdiff)
        uin(:,neng)=uin(:,nspc(1)) !storing X as energy        
        uin(:,neng)=(uin(:,neng)-peakchange)/(1.0_pr-2.0_pr*peakchange)  
                   !undo-ing the peakchange, to be used for discountinuous field solutions.
        uin(:,nspc(1))=MW_in(1)*uin(:,nspc(1))/(MW_in(1)*uin(:,nspc(1))+MW_in(2)*(1.0_pr-uin(:,nspc(1))))   !Y from X 
        IF (rhopert) THEN
           uin(:,nden) = 0.0_pr
           DO i=1,Nspecm
              uin(:,nden) = uin(:,nden) + uin(:,nspc(i))/MW_in(i)
           END DO
           uin(:,nden) = uin(:,nden) + (1.0_pr-SUM(uin(:,nspc(1):nspc(Nspecm)),DIM=2))/MW_in(Nspec)  !R
           uin(:,nden) = uin(:,nprs)/uin(:,nden)/Tconst !rho
        END IF
     END IF
     !^^^^^^^^^^^^^^^^^^^^^^^^^^^Setting up perturbed X and Y^^^^^^^^^^^^^^^^^^^^^^^^^^^

     !___________________________Finalizing all variables___________________________
     IF (addmixu) THEN
        uin(:,neng)=(1.0_pr-SUM(uin(:,nspc(1):nspc(Nspecm)),DIM=2))/MW_in(Nspec)  !R
        DO j=1,Nspecm
           uin(:,neng)=uin(:,neng)+uin(:,nspc(j))/MW_in(j)
        END DO
        CALL c_diff_fast(LOG(uin(:,neng)), du(1,:,:), d2u(1,:,:), j_lev, nlocal,meth, 10, 1, 1, 1)  !dlnR/dx
        uin(:,neng)=(1.0_pr-SUM(uin(:,nspc(1):nspc(Nspecm)),DIM=2))*bD_in(Nspec)  !D
        DO j=1,Nspecm
           uin(:,neng)=uin(:,neng)+uin(:,nspc(j))*bD_in(j)
        END DO
        DO j=1,dim
           uin(:,nvel(j)) = uin(:,nvel(j)) + uin(:,neng)*du(1,:,j)  !u_i=D*dlnR/dx_i
        END DO
     END IF

     IF (temphump) THEN
        uin(:,neng)=(1.0_pr-SUM(uin(:,nspc(1):nspc(Nspecm)),DIM=2))/MW_in(Nspec)  !R
        DO j=1,Nspecm
           uin(:,neng)=uin(:,neng)+uin(:,nspc(j))/MW_in(j)
        END DO
        DO i=1,nlocal
           uin(i,nprs)=uin(i,nprs) + tempamp*uin(i,nden)*uin(i,neng)*Tconst*EXP(-(x(i,1) - Amp*SUM(COS(x(i,2:dim)*lams(1:dim-1))))**2/2.0_pr/tempsig**2)  !/SQRT(2.0_pr*pi*tempsig**2)  !P=rho*R*T_hump
        END DO
     END IF

     DO i=1,nlocal
        mycp=(1.0_pr-SUM(uin(i,nspc(1):nspc(Nspecm))))*cp_in(Nspec) 
        myR =(1.0_pr-SUM(uin(i,nspc(1):nspc(Nspecm))))/MW_in(Nspec) 
        DO j=1,Nspecm
           mycp=mycp+cp_in(j)*uin(i,nspc(j))
           myR =myR +uin(i,nspc(j))/MW_in(j)
        END DO
        uin(i,neng)=uin(i,nden)*SUM(uin(i,nvel(1):nvel(dim))**2)*0.5_pr+uin(i,nprs)*(mycp/myR-1.0_pr)  !rhoe
     END DO
     DO j=1,dim
        uin(:,nvel(j))=uin(:,nvel(j))*uin(:,nden)  !rho*u_i
     END DO
     DO j=1,Nspecm
        uin(:,nspc(j))=uin(:,nspc(j))*uin(:,nden)  !rho*Y_l
     END DO 

     IF (savepardom) uin(:,ndom) = REAL(par_rank,pr)
     !^^^^^^^^^^^^^^^^^^^^^^^^^^^Finalilzing all variables^^^^^^^^^^^^^^^^^^^^^^^^^^^
  END IF

     !___________________________Zero Order Quantities___________________________
  IF (AlgVals) THEN
     Y0top = 0.0_pr
     Y0top(1) = pureY(1) 
     Y0top(Nspec)=1.0_pr-Y0top(1)
     rho0top = Pint/Tconst/pureR(1)*EXP(-pureg(1)/pureR(1)/Tconst*(xyzlimits(2,1)))
     drho0top= -rho0top*pureg(1)/pureR(1)/Tconst
     rhoYtopBC(1:Nspecm) = Y0top(1:Nspecm)*drho0top
     rhoYtop(1:Nspecm) = Y0top(1:Nspecm)*rho0top
     rhoetopBC = Tconst*SUM((cp_in(:) - 1.0_pr/MW_in(:))*Y0top(:))*drho0top
     e0top = Tconst*SUM((cp_in(:) - 1.0_pr/MW_in(:))*Y0top(:))
     rhoetop = e0top*rho0top

     Y0bot = 0.0_pr
     Y0bot(1) = pureY(2) 
     Y0bot(Nspec)=1.0_pr-Y0bot(1)
     rho0bot = Pint/Tconst/pureR(2)*EXP(-pureg(2)/pureR(2)/Tconst*(xyzlimits(1,1)))
     drho0bot= -rho0bot*pureg(2)/pureR(2)/Tconst
     rhoYbotBC(1:Nspecm) = Y0bot(1:Nspecm)*drho0bot
     rhoYbot(1:Nspecm) = Y0bot(1:Nspecm)*rho0bot
     rhoebotBC = Tconst*SUM((cp_in(:) - 1.0_pr/MW_in(:))*Y0bot(:))*drho0bot
     e0bot = Tconst*SUM((cp_in(:) - 1.0_pr/MW_in(:))*Y0bot(:))
     rhoebot = e0bot*rho0bot

     IF (par_rank.EQ.0 .AND. verb_level>0) THEN
        PRINT *, '---BOUNDARY VALUES---'

        PRINT *, 'Y0_top=',Y0top
        PRINT *, 'rho0_top= ',rho0top
        PRINT *, 'drho0_top=',drho0top
        PRINT *, 'e0_top= ', e0top
        PRINT *, 'Y0_bot=',Y0bot
        PRINT *, 'rho0_bot= ',rho0bot
        PRINT *, 'drho0_bot=',drho0bot
        PRINT *, 'e0_bot=',e0bot

        PRINT *, 'drhoY_top=', rhoYtopBC
        PRINT *, 'drhoe_top= ',rhoetopBC
        PRINT *, 'drhoY_bot= ',rhoYbotBC
        PRINT *, 'drhoe_bot=', rhoebotBC

        PRINT *, 'rhoY_top=', rhoYtop
        PRINT *, 'rhoe_top= ',rhoetop
        PRINT *, 'rhoY_bot= ',rhoYbot
        PRINT *, 'rhoe_bot=', rhoebot

        PRINT *, '^^^BOUNDARY VALUES^^^'
     END IF

     bndvals(nprs,1)=Pint
     bndvals(nprs,2)=Pint
     bndvals(nden,1)=Pint/Tconst/pureR(2)
     bndvals(nden,2)=Pint/Tconst/pureR(1)
     bndvals(nvel(1):nvel(dim),:)=0.0_pr    
     bndvals(neng,1)=Pint/(puregamma(2)-1.0_pr)
     bndvals(neng,2)=Pint/(puregamma(1)-1.0_pr)
     bndvals(nspc(1):nspc(Nspecm),1)=0.0_pr
     bndvals(nspc(1),1)=Pint/Tconst/pureR(2)*pureY(2) 
     bndvals(nspc(1):nspc(Nspecm),2)=0.0_pr
     bndvals(nspc(1),2)=Pint/Tconst/pureR(1)*pureY(1) 
     bndvals(dim+Nspec+3,:)=0.0_pr

     rhoYg(1) = (SUM(rhoYtop(1:Nspecm)*gr_in(1:Nspecm)) + (rho0top-SUM(rhoYtop(1:Nspecm)))*gr_in(Nspec))
     rhoYg(2) = (SUM(rhoYbot(1:Nspecm)*gr_in(1:Nspecm)) + (rho0bot-SUM(rhoYbot(1:Nspecm)))*gr_in(Nspec))
     drho0(1) = drho0top
     drho0(2) = drho0bot
     P0(1) = rho0top*Tconst*SUM(1.0_pr/MW_in(:)*Y0top(:))
     P0(2) = rho0bot*Tconst*SUM(1.0_pr/MW_in(:)*Y0bot(:))  
     dP0(1) = drho0top*Tconst*pureR(1)
     dP0(2) = drho0top*Tconst*pureR(2)

     IF (par_rank.EQ.0 .AND. verb_level>0) THEN
        PRINT *, 'rhoYg', rhoYg
        PRINT *, 'dP0', dP0
        PRINT *, 'P0', P0
        PRINT *, 'drho0', drho0
        PRINT *, 'c0', c0
     END IF
  ELSE
          IF (modwallvals) THEN
             buffsize=(xyzlimits(2,1)-xyzlimits(1,1))*buffDomFrac/2.0
             botbnd=xyzlimits(1,1)+buffsize
             topbnd=xyzlimits(2,1)-buffsize

             minzdiff=Lxyz(1)
             minzloc=1
             DO i=1,nlocal
                zdiff = x(i,1) - topbnd
                IF (ABS(zdiff) < minzdiff) THEN 
                   minzloc=i
                   minzdiff=ABS(zdiff)
                END IF   
             END DO
             pshift = x(minzloc,1)
             CALL valminzdiff(minzdiff,pshift)
             bndvals(dim+Nspec+3,2) = pshift 
             pshift = uin(minzloc,nden)
             CALL valminzdiff(minzdiff,pshift)
             bndvals(nden,2) = pshift 

             minzdiff=Lxyz(1)
             minzloc=1
             DO i=1,nlocal
                zdiff = x(i,1) - botbnd
                IF (ABS(zdiff) < minzdiff) THEN 
                   minzloc=i
                   minzdiff=ABS(zdiff)
                END IF   
             END DO
             pshift = x(minzloc,1)
             CALL valminzdiff(minzdiff,pshift)
             bndvals(dim+Nspec+3,1) = pshift 
             pshift = uin(minzloc,nden)
             CALL valminzdiff(minzdiff,pshift)
             bndvals(nden,1) = pshift 
             DO i=1,nlocal
                IF (x(i,1)<botbnd) THEN
                   uin(i,nden) = bndvals(nden,1)*EXP(-pureg(2)/pureR(2)/Tconst*(x(i,1)-bndvals(dim+Nspec+3,1)))
                   uin(i,nprs) = uin(i,nden)*pureR(2)*Tconst
                ELSE IF (x(i,1)>topbnd) THEN
                   uin(i,nden) = bndvals(nden,2)*EXP(-pureg(1)/pureR(1)/Tconst*(x(i,1)-bndvals(dim+Nspec+3,2)))
                   uin(i,nprs) = uin(i,nden)*pureR(1)*Tconst
                END IF
             END DO
          END IF

          CALL c_diff_fast(uin(:,nden), du(1,:,:), d2u(1,:,:), j_lev, nlocal, meth, 10, 1, 1, 1)
          minzdiff=Lxyz(1)
          minzloc=1
          DO i=1,nlocal
             zdiff = x(i,1) - xyzlimits(2,1)
             IF (ABS(zdiff) < minzdiff) THEN 
                minzloc=i
                minzdiff=ABS(zdiff)
             END IF   
          END DO
          DO i=1,Nspecm
             pshift=uin(minzloc,nspc(i))/uin(minzloc,nden)
             CALL valminzdiff(minzdiff,pshift)
             Y0top(i) = pshift 
          END DO
          Y0top(Nspec)=1.0_pr-SUM(Y0top(1:Nspecm))

          pshift = uin(minzloc,nden)
          CALL valminzdiff(minzdiff,pshift)
          rho0top = pshift 

          pshift=du(1,minzloc,1)
          CALL valminzdiff(minzdiff,pshift)
          drho0top=pshift
          
          rhoYtopBC(1:Nspecm) = Y0top(1:Nspecm)*drho0top
          rhoYtop(1:Nspecm) = Y0top(1:Nspecm)*rho0top
          rhoetopBC = Tconst*SUM((cp_in(:) - 1.0_pr/MW_in(:))*Y0top(:))*drho0top
          e0top = Tconst*SUM((cp_in(:) - 1.0_pr/MW_in(:))*Y0top(:))
          rhoetop = e0top*rho0top

          IF (par_rank.EQ.0 .AND. verb_level>0) PRINT *, 'Top location:', x(minzloc,:)

          minzdiff=Lxyz(1)
          minzloc=1
          DO i=1,nlocal
             zdiff = x(i,1) - xyzlimits(1,1)
             IF (ABS(zdiff) < minzdiff) THEN 
                minzloc=i
                minzdiff=ABS(zdiff)
             END IF   
          END DO
          DO i=1,Nspecm
             pshift=uin(minzloc,nspc(i))/uin(minzloc,nden)
             CALL valminzdiff(minzdiff,pshift)
             Y0bot(i) = pshift 
          END DO
          Y0bot(Nspec)=1.0_pr-SUM(Y0bot(1:Nspecm))

          pshift = uin(minzloc,nden)
          CALL valminzdiff(minzdiff,pshift)
          rho0bot = pshift 

          pshift=du(1,minzloc,1)
          CALL valminzdiff(minzdiff,pshift)
          drho0bot=pshift
          
          rhoYbotBC(1:Nspecm) = Y0bot(1:Nspecm)*drho0bot
          rhoYbot(1:Nspecm) = Y0bot(1:Nspecm)*rho0bot
          rhoebotBC = Tconst*SUM((cp_in(:) - 1.0_pr/MW_in(:))*Y0bot(:))*drho0bot
          e0bot = Tconst*SUM((cp_in(:) - 1.0_pr/MW_in(:))*Y0bot(:))
          rhoebot = e0bot*rho0bot

         IF (par_rank.EQ.0 .AND. verb_level>0) THEN
          PRINT *, 'Bottom location:', x(minzloc,:)

          PRINT *, '---BOUNDARY VALUES---'

          PRINT *, 'Y0_top=',Y0top
          PRINT *, 'rho0_top= ',rho0top
          PRINT *, 'drho0_top=',drho0top
          PRINT *, 'e0_top= ', e0top
          PRINT *, 'Y0_bot=',Y0bot
          PRINT *, 'rho0_bot= ',rho0bot
          PRINT *, 'drho0_bot=',drho0bot
          PRINT *, 'e0_bot=',e0bot

          PRINT *, 'drhoY_top=', rhoYtopBC
          PRINT *, 'drhoe_top= ',rhoetopBC
          PRINT *, 'drhoY_bot= ',rhoYbotBC
          PRINT *, 'drhoe_bot=', rhoebotBC

          PRINT *, 'rhoY_top=', rhoYtop
          PRINT *, 'rhoe_top= ',rhoetop
          PRINT *, 'rhoY_bot= ',rhoYbot
          PRINT *, 'rhoe_bot=', rhoebot

          PRINT *, '^^^BOUNDARY VALUES^^^'
         END IF

          IF (usewall) THEN
             bndvals(nden,1)=rho0bot
             bndvals(nden,2)=rho0top
             bndvals(nvel(1):nvel(dim),:)=0.0_pr    
             bndvals(neng,1)=rhoebot
             bndvals(neng,2)=rhoetop
             bndvals(nspc(1):nspc(Nspecm),1)=rhoYbot(1:Nspecm)
             bndvals(nspc(1):nspc(Nspecm),2)=rhoYtop(1:Nspecm)
             bndvals(nprs,1)=rhoebot/( (SUM(rhoYbot(1:Nspecm)*cp_in(1:Nspecm))+(rho0bot-SUM(rhoYbot(1:Nspecm)))*cp_in(Nspec)) / &
                                       (SUM(rhoYbot(1:Nspecm)/MW_in(1:Nspecm))+(rho0bot-SUM(rhoYbot(1:Nspecm)))/MW_in(Nspec)) - 1.0_pr )
             bndvals(nprs,2)=rhoetop/( (SUM(rhoYtop(1:Nspecm)*cp_in(1:Nspecm))+(rho0top-SUM(rhoYtop(1:Nspecm)))*cp_in(Nspec)) / &
                                       (SUM(rhoYtop(1:Nspecm)/MW_in(1:Nspecm))+(rho0top-SUM(rhoYtop(1:Nspecm)))/MW_in(Nspec)) - 1.0_pr )
             bndvals(dim+Nspec+3,:)=xyzlimits(:,1)
          ELSE          
             buffsize=(xyzlimits(2,1)-xyzlimits(1,1))*buffDomFrac/2.0
             botbnd=xyzlimits(1,1)+buffsize
             topbnd=xyzlimits(2,1)-buffsize

             minzdiff=Lxyz(1)
             minzloc=1
             DO i=1,nlocal
                zdiff = x(i,1) - topbnd
                IF (ABS(zdiff) < minzdiff) THEN 
                   minzloc=i
                   minzdiff=ABS(zdiff)
                END IF   
             END DO
             pshift = x(minzloc,1)
             CALL valminzdiff(minzdiff,pshift)
             bndvals(dim+Nspec+3,2) = pshift 
             pshift = uin(minzloc,nden)
             CALL valminzdiff(minzdiff,pshift)
             bndvals(nden,2) = pshift 

             minzdiff=Lxyz(1)
             minzloc=1
             DO i=1,nlocal
                zdiff = x(i,1) - botbnd
                IF (ABS(zdiff) < minzdiff) THEN 
                   minzloc=i
                   minzdiff=ABS(zdiff)
                END IF   
             END DO
             pshift = x(minzloc,1)
             CALL valminzdiff(minzdiff,pshift)
             bndvals(dim+Nspec+3,1) = pshift 
             pshift = uin(minzloc,nden)
             CALL valminzdiff(minzdiff,pshift)
             bndvals(nden,1) = pshift 

             bndvals(nvel(1):nvel(dim),:)=0.0_pr    
             bndvals(neng,1)=e0bot*bndvals(nden,1)
             bndvals(neng,2)=e0top*bndvals(nden,2)
             bndvals(nspc(1):nspc(Nspecm),1)=Y0bot(1:Nspecm)*bndvals(nden,1)
             bndvals(nspc(1):nspc(Nspecm),2)=Y0top(1:Nspecm)*bndvals(nden,2)
             bndvals(nprs,1)=bndvals(neng,1) / &
                      ( (SUM(bndvals(nspc(1):nspc(Nspecm),1)*cp_in(1:Nspecm))+(bndvals(nden,1)-SUM(bndvals(nspc(1):nspc(Nspecm),1)))*cp_in(Nspec)) / &
                        (SUM(bndvals(nspc(1):nspc(Nspecm),1)/MW_in(1:Nspecm))+(bndvals(nden,1)-SUM(bndvals(nspc(1):nspc(Nspecm),1)))/MW_in(Nspec)) - 1.0_pr )
             bndvals(nprs,2)=bndvals(neng,2) / &
                      ( (SUM(bndvals(nspc(1):nspc(Nspecm),2)*cp_in(1:Nspecm))+(bndvals(nden,2)-SUM(bndvals(nspc(1):nspc(Nspecm),2)))*cp_in(Nspec)) / &
                        (SUM(bndvals(nspc(1):nspc(Nspecm),2)/MW_in(1:Nspecm))+(bndvals(nden,2)-SUM(bndvals(nspc(1):nspc(Nspecm),2)))/MW_in(Nspec)) - 1.0_pr )
          END IF

          rhoYg(1) = (SUM(rhoYtop(1:Nspecm)*gr_in(1:Nspecm)) + (rho0top-SUM(rhoYtop(1:Nspecm)))*gr_in(Nspec))
          rhoYg(2) = (SUM(rhoYbot(1:Nspecm)*gr_in(1:Nspecm)) + (rho0bot-SUM(rhoYbot(1:Nspecm)))*gr_in(Nspec))
          drho0(1) = drho0top
          drho0(2) = drho0bot
          P0(1) = rho0top*Tconst*SUM(1.0_pr/MW_in(:)*Y0top(:))
          P0(2) = rho0bot*Tconst*SUM(1.0_pr/MW_in(:)*Y0bot(:))
          
          IF (splitFBpress) THEN
             CALL c_diff_fast(uin(:,nprs), du(1,:,:), d2u(1,:,:), j_lev, nlocal, BIASING_BACKWARD+meth, 10, 1, 1, 1)
          ELSE
             CALL c_diff_fast(uin(:,nprs), du(1,:,:), d2u(1,:,:), j_lev, nlocal, meth, 10, 1, 1, 1)
          END IF
          minzdiff=Lxyz(1)
          minzloc=1
          DO i=1,nlocal
             zdiff = x(i,1) - xyzlimits(2,1)
             IF (ABS(zdiff) < minzdiff) THEN 
                minzloc=i
                minzdiff=ABS(zdiff)
             END IF   
          END DO
          pshift=du(1,minzloc,1)
          CALL valminzdiff(minzdiff,pshift)
          dP0(1)=pshift
          
          IF (splitFBpress) CALL c_diff_fast(uin(:,nprs), du(1,:,:), d2u(1,:,:), j_lev, nlocal, BIASING_FORWARD+meth, 10, 1, 1, 1)
          minzdiff=Lxyz(1)
          minzloc=1
          DO i=1,nlocal
             zdiff = x(i,1) - xyzlimits(1,1)
             IF (ABS(zdiff) < minzdiff) THEN 
                minzloc=i
                minzdiff=ABS(zdiff)
             END IF   
          END DO
          pshift=du(1,minzloc,1)
          CALL valminzdiff(minzdiff,pshift)
          dP0(2)=pshift

         IF (par_rank.EQ.0 .AND. verb_level>0) THEN
          PRINT *, 'rhoYg', rhoYg
          PRINT *, 'dP0', dP0
          PRINT *, 'P0', P0
          PRINT *, 'drho0', drho0
          PRINT *, 'c0', c0
         END IF
  END IF
     !^^^^^^^^^^^^^^^^^^^^^^^^^^^Zero Order Quantities^^^^^^^^^^^^^^^^^^^^^^^^^^^
  
       IF (tfacczn>0.0_pr) THEN
        IF (itfacczn<0.0_pr) THEN
          czn_min = xyzzone(1,1)
          czn_max = xyzzone(2,1)
          xyzzone(1,1) = -Lczn
          xyzzone(2,1) =  Lczn
          cczn = MINVAL(SQRT(gamm/MW_in*Tconst))
          tczn = MAXVAL(ABS(xyzlimits(:,1)))/cczn*tfacczn
         IF (par_rank.EQ.0 .AND. verb_level>0) THEN
          PRINT *, '*****************************************************'
          PRINT *, 'APPLYING TEMPORARY COORD_ZONE'
          PRINT *, 'COORD_ZONE OUTSIDE OF x=[',-Lczn,',',Lczn,']'
          PRINT *, 'MINIMUM WAVE SPEED: ', cczn
          PRINT *, 'MAXIMUM DISTANCE: ', MAXVAL(ABS(xyzlimits(:,1)))
          PRINT *, 'TIME SAFETY FACTOR: ', tfacczn
          PRINT *, 'STOPPING TIME FOR COORD_ZONE: ', tczn
          PRINT *, '*****************************************************'
         END IF
          modczn = .TRUE.
        ELSE
          modczn = .FALSE.
          cczn = MINVAL(SQRT(gamm/MW_in*Tconst))
          itczn = Lczn/cczn*itfacczn
         IF (par_rank.EQ.0 .AND. verb_level>0) THEN
          PRINT *, '*****************************************************'
          PRINT *, 'TEMPORARY COORD_ZONE WILL START LATER'
          PRINT *, 'COORD_ZONE OUTSIDE OF x=[',-Lczn,',',Lczn,']'
          PRINT *, 'MINIMUM WAVE SPEED: ', cczn
          PRINT *, 'MAXIMUM DISTANCE: ', MAXVAL(ABS(xyzlimits(:,1)))
          PRINT *, 'STARTING TIME SAFETY FACTOR: ', itfacczn
          PRINT *, 'STARTING TIME FOR COORD_ZONE: ', itczn
          PRINT *, '*****************************************************'
         END IF
        END IF  
       END IF

       maxMa=0.0_pr
       IF (iter .NE. -1) THEN
          layertop = -Lxyz(1)
          layerbot =  Lxyz(1)
          difftop(:) = layertop
          diffbot(:) = layerbot
       END IF

       maxMa = MAXVAL(SQRT(SUM(uin(:,nvel(1):nvel(dim))**2,DIM=2)/uin(:,nden)/uin(:,nprs)))
       CALL parallel_global_sum(REALMAXVAL=maxMa)
       MAreport = maxMa
       IF (par_rank.EQ.0) THEN
          PRINT *, '*****************AFTER************************************'
          PRINT *, 'case:    ', file_gen(1:LEN_TRIM(file_gen)-1)
          PRINT *, 'max Ma = ', maxMa
          PRINT *, 'IC iter= ', iter,           '  t       = ', t
          PRINT *, 'iwrite = ', iwrite-1,       '  dt      = ', dt 
          PRINT *, 'j_mx   = ', j_mx,           '  dt_orig = ', dt_original
          PRINT *, 'j_lev  = ', j_lev,          '  twrite  = ', twrite
          PRINT *, 'nwlt_g = ', mynwlt_global,  '  cpu     = ', timer_val(2)
          PRINT *, 'n_mvec = ', fullnwlt,       '  n%      = ', npert
          PRINT *, 'nwlt   = ', nwlt,           '  eps     = ', eps
          PRINT *, 'nwlt_mx= ', maxnwlt,        '  nwlt_av = ', avgnwlt
          PRINT *, 'nwlt_mn= ', minnwlt,        '  p_size  = ', par_size
          PRINT *, '**********************************************************'
       END IF
       IF (MOD(PLpinf,2)==1) maxMa=Ma
       maxMa = MIN(maxMa,0.9_pr)
       iwriteclean = MAX(iwrite-1,-1)
  END SUBROUTINE user_initial_conditions

  SUBROUTINE hydroP (hydP,hydrho,hydY,nlocal,speci)
    !--Gives hydrostatic pressure with constant Temperature
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: nlocal, speci
    REAL (pr), DIMENSION (nlocal), INTENT(INOUT) :: hydP
    REAL (pr), DIMENSION (nlocal), INTENT(IN) :: hydrho, hydY
    INTEGER :: i,  minzloc
    REAL (pr) :: minzdiff, zdiff, pshift
       IF (hydropic .EQ. 1) THEN
          Ptop=MINVAL(hydP)
          CALL parallel_global_sum (REALMINVAL=Ptop)
          curspec=speci
          CALL my_hydropress(hydP,nlocal) ! pressure
          IF (shiftp) CALL shiftingP(hydP,nlocal)
          Ptop=MINVAL(hydP) 
          CALL parallel_global_sum (REALMINVAL=Ptop)
       ELSE IF (hydropic .EQ. 2) THEN
          Ptop=MINVAL(hydP)
          CALL parallel_global_sum (REALMINVAL=Ptop)
          curspec=speci
          CALL hyperP (hydP,hydrho,hydY,nlocal)
          IF (shiftp) CALL shiftingP(hydP,nlocal)
          Ptop=MINVAL(hydP) 
          CALL parallel_global_sum (REALMINVAL=Ptop)
       END IF
  END SUBROUTINE hydroP

  SUBROUTINE hyperP (hypP,hyprho,hypY,nlocal)
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: nlocal
    REAL (pr), DIMENSION (nlocal), INTENT(INOUT) :: hypP
    REAL (pr), DIMENSION (nlocal), INTENT(IN) :: hyprho, hypY
    REAL (pr), DIMENSION (nlocal,dim) :: du, d2u !, du_c, du_f, du_b, d2u
    INTEGER :: i, meth, methc, methf, methb, myiter
    REAL(pr) :: hyperr, hyptol, initerr, deltau, delx, taufac
 
    hyptol = 1.0e-8_pr
    hyperr = 1.0_pr
    myiter = 0
    meth = 1
    taufac = 0.1_pr
    methc = meth
    methf = meth + BIASING_FORWARD
    methb = meth + BIASING_BACKWARD
    delx=(xyzlimits(2,1)-xyzlimits(1,1))/REAL(mxyz(1)*2**(j_mx-1),pr) 
    deltau = delx*taufac
    DO WHILE (hyperr .GT. hyptol)
       CALL c_diff_fast (hypP, du, d2u, j_lev, nlocal, methf, 10, 1, 1, 1)
       !CALL c_diff_fast (hypP, du_f, d2u, j_lev, nlocal, methf, 10, 1, 1, 1)
       !CALL c_diff_fast (hypP, du_b, d2u, j_lev, nlocal, methb, 10, 1, 1, 1)
       !du_c(:,2) = 0.0_pr
       !du_f(:,2) = 0.0_pr
       !du_b(:,2) = 0.0_pr
       !DO i=1,nlocal
       !   IF (x(i,1).GT.1.0_pr) THEN 
       !      du_b(i,2) = 1.0_pr
       !   ELSE IF (x(i,1).LT.-1.0_pr) THEN 
       !      du_f(i,2) = 1.0_pr
       !   ELSE
       !      du_c(i,2) = 1.0_pr
       !   END IF
       !END DO
       !du(:,1) = du_c(:,1)*du_c(:,2) + du_f(:,1)*du_f(:,2) + du_b(:,1)*du_b(:,2) 
       IF (hydrotype == 1) THEN
          IF (hydrofull) THEN
             d2u(:,1) = hypY(:)*(gr_in(1)-gr_in(2)) + gr_in(2)  !g
             d2u(:,2) = hypY(:)*(1.0/MW_in(1)-1.0_pr/MW_in(2)) + 1.0_pr/MW_in(2)  !R
             du(:,1) = du(:,1) + hypP*d2u(:,1)/Tconst/d2u(:,2)
          ELSE
             du(:,1) = du(:,1) + hypP*pureg(curspec)/Tconst/pureR(curspec)
          END IF
       ELSE
          d2u(:,1) = hypY(:)*(gr_in(1)-gr_in(2)) + gr_in(2)  !g
          du(:,1) = du(:,1) + hyprho(:)*d2u(:,1) !dp/dx + rho*g
       END IF
       DO i=1,nlocal
           IF (ABS(x(i,1)-xyzlimits(2,1)) .lt. 1.0e-12) du(i,1) = 0.0_pr 
       END DO
       hypP = hypP + deltau*du(:,1)  
       myiter = myiter + 1
       hyperr = MAXVAL(ABS(du(:,1)))
       CALL parallel_global_sum (REALMAXVAL=hyperr)
       !PRINT *, 'In hyperP: myiter=', myiter, 'error=', hyperr
    END DO
       PRINT *, 'In hyperP: myiter=', myiter, 'error=', hyperr
  END SUBROUTINE hyperP

  SUBROUTINE shiftingP (hydP,nlocal)
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: nlocal
    REAL (pr), DIMENSION (nlocal), INTENT(INOUT) :: hydP
    INTEGER :: i,  minzloc
    REAL (pr) :: minzdiff, zdiff, pshift

     minzdiff=xyzlimits(2,1)-xyzlimits(1,1)
     minzloc=1
     DO i=1,nlocal
        zdiff=x(i,1)
        IF (ABS(zdiff) < minzdiff) THEN 
           minzloc=i
           minzdiff=ABS(zdiff)
        END IF   
     END DO
     pshift=hydP(minzloc)/Pint 
     CALL valminzdiff(minzdiff,pshift)
     IF (par_rank.EQ.0 .AND. verb_level>0) THEN
        PRINT *, 'SHIFTING PRESSURE AFTER LINSOLVE'
        PRINT *, 'MINZDIFF=', minzdiff, ' MINZLOC=', minzloc
        PRINT *, 'p(minzloc)=', hydP(minzloc), ' pshift=', pshift
        PRINT *, 'Ptop=', Ptop
     END IF        
     hydP=hydP/pshift
     IF (par_rank.EQ.0 .AND. verb_level>0) THEN
        PRINT *, 'AFTER SHIFTING PRESSURE'
        PRINT *, 'new p(minzloc)', hydP(minzloc)    
     END IF
  END SUBROUTINE shiftingP

  SUBROUTINE valminzdiff (loczdiff,locval)
     IMPLICIT NONE
     REAL (pr), INTENT(INOUT) :: loczdiff, locval
     REAL (pr), DIMENSION(par_size) :: trupshift, truminzdiff
     REAL (pr), DIMENSION(1) :: pshiftp, minzdiffp
     INTEGER, DIMENSION(1) :: minzlocp
             minzdiffp(1)=loczdiff
             pshiftp(1)=locval
             trupshift=locval
             truminzdiff=loczdiff
             CALL parallel_gather(minzdiffp,truminzdiff,1)
             CALL parallel_gather(pshiftp,  trupshift,  1)
             minzlocp=MINLOC(truminzdiff(:))
             pshiftp(1)=trupshift(minzlocp(1))
             CALL parallel_broadcast(pshiftp(1))
             locval=pshiftp(1)
  END SUBROUTINE valminzdiff

  SUBROUTINE dpdx (pderiv,nlocal,Yloc,meth,myjl,split)
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: nlocal
    REAL (pr), DIMENSION (nlocal), INTENT(INOUT) :: pderiv
    REAL (pr), DIMENSION (nlocal,Nspecm), INTENT(IN) :: Yloc
    INTEGER, INTENT(IN) :: meth, myjl
    LOGICAL, INTENT(IN) :: split
    INTEGER :: i
    REAL (pr), DIMENSION (nlocal,dim) :: du
    REAL (pr), DIMENSION (nlocal,dim) :: d2u

    IF (split) THEN 
       CALL c_diff_fast(pderiv(:), du(:,:), d2u(:,:), myjl, nlocal, MOD(meth,2)+BIASING_BACKWARD, 10, 1, 1, 1)
       d2u(:,1) = du(:,1)
       CALL c_diff_fast(pderiv(:), du(:,:), d2u(:,:), myjl, nlocal, MOD(meth,2)+BIASING_FORWARD , 10, 1, 1, 1)
       IF (stepsplit) THEN
          d2u(:,2) = 1.0_pr
          d2u(:,2) = (SIGN(d2u(:,2), Yloc(:,1) - Y_Xhalf) + 1.0_pr)/2.0_pr
       ELSE
          d2u(:,2) = (1.0_pr - SUM(Yloc(:,1:Nspecm),DIM=2))/MW_in(Nspec)  !Y_Nspec/W_Nspec
          DO i=1,Nspecm
             d2u(:,2) = d2u(:,2) + Yloc(:,i)/MW_in(i)  !Y_l/W_l
          END DO
          d2u(:,2) = Yloc(:,1)/MW_in(1)/d2u(:,2)  !X_1
          d2u(:,2) = (d2u(:,2)-peakchange)/(1.0_pr-2.0_pr*peakchange)  !X undoing peak change
       END IF
       d2u(:,2) = MIN(1.0_pr,MAX(0.0_pr,d2u(:,2)))
       pderiv(:) = d2u(:,2)*d2u(:,1) + (1.0_pr - d2u(:,2))*du(:,1)
    ELSE
       CALL c_diff_fast(pderiv(:), du(:,:), d2u(:,:), myjl, nlocal, meth, 10, 1, 1, 1)
       pderiv(:) = du(:,1)
    END IF
  END SUBROUTINE dpdx

  SUBROUTINE dpdx_diag (pd_diag,nlocal,Yloc,meth,myjl,split)
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: nlocal
    REAL (pr), DIMENSION (nlocal), INTENT(INOUT) :: pd_diag
    REAL (pr), DIMENSION (nlocal,Nspecm), INTENT(IN) :: Yloc
    INTEGER, INTENT(IN) :: meth, myjl
    LOGICAL, INTENT(IN) :: split
    INTEGER :: i
    REAL (pr), DIMENSION (nlocal,dim) :: du_diag, d2u_diag

    IF (split) THEN 
       CALL c_diff_diag(du_diag, d2u_diag, myjl, nlocal, MOD(meth,2)+BIASING_BACKWARD, MOD(meth,2)+BIASING_FORWARD, 10) 
       pd_diag(:) = du_diag(:,1)
       CALL c_diff_diag(du_diag, d2u_diag, myjl, nlocal, MOD(meth,2)+BIASING_FORWARD, MOD(meth,2)+BIASING_BACKWARD, 10)   
       IF (stepsplit) THEN
          d2u_diag(:,2) = 1.0_pr
          d2u_diag(:,2) = (SIGN(d2u_diag(:,2), Yloc(:,1) - Y_Xhalf) + 1.0_pr)/2.0_pr
       ELSE
          d2u_diag(:,2) = (1.0_pr - SUM(Yloc(:,1:Nspecm),DIM=2))/MW_in(Nspec)
          DO i=1,Nspecm
             d2u_diag(:,2) = d2u_diag(:,2) + Yloc(:,i)/MW_in(i)
          END DO
          d2u_diag(:,2)=Yloc(:,1)/MW_in(1)/d2u_diag(:,2)  !X
          d2u_diag(:,2)=(d2u_diag(:,2)-peakchange)/(1.0_pr-2.0_pr*peakchange)  !X undoing peak change
       END IF
       d2u_diag(:,2) = MIN(1.0_pr,MAX(0.0_pr,d2u_diag(:,2)))
       pd_diag(:) = d2u_diag(:,2)*pd_diag(:) + (1.0_pr - d2u_diag(:,2))*du_diag(:,1)
    ELSE
       CALL c_diff_diag(du_diag, d2u_diag, j_lev, ng, 2*methpd+meth, 2*methpd+meth, 10)   !dp/dx done using methpd 
       pd_diag(:) = du_diag(:,1)
    END IF
  END SUBROUTINE dpdx_diag

  SUBROUTINE smoothX (smoothu,nlocal,methdiff,IDdiff)
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: nlocal
    REAL (pr), DIMENSION (nlocal,Nspecm), INTENT(INOUT) :: smoothu
    INTEGER, INTENT(IN) :: methdiff, IDdiff
    REAL (pr), DIMENSION (dim) :: delxyz
    REAL (pr), DIMENSION (Nspecm,nlocal,dim) :: du
    REAL (pr), DIMENSION (Nspecm,nlocal,dim) :: d2u
    INTEGER :: Nt, tstp
    REAL (pr) :: zdiff
    INTEGER :: i
        IF (stepic .OR. doicdiff) THEN
           DO i=1,nlocal
              zdiff=(x(i,1) - Amp*SUM(COS(x(i,2:dim)*lams(1:dim-1)))) 
              IF (zdiff .gt. 1.0e-12_pr) THEN
                 smoothu(i,:)=1.0_pr-peakchange
              ELSEIF (zdiff .lt. -1.0e-12_pr) THEN
                 smoothu(i,:)=peakchange
              ELSE
                 smoothu(i,:)=0.5_pr
              END IF
           END DO
           IF (doicdiff) CALL mydiffsmooth (smoothu(:,1:Nspecm),nlocal,Nspecm,methdiff,IDdiff,smdir,sm_dfac,sm_delta,sm_deltadel,sm_j,j_lev,1,.TRUE.) !smooth X
        ELSE
           IF (useerf) THEN
              DO i=1,nlocal
                 zdiff=(x(i,1) - Amp*SUM(COS(x(i,2:dim)*lams(1:dim-1)))) 
                 smoothu(i,:) = 1.0_pr/2.0_pr +  (1.0_pr/2.0_pr - peakchange) * DERF(zdiff/icerfdelta)
              END DO
           ELSE
              DO i=1,nlocal
                 zdiff=(x(i,1) - Amp*SUM(COS(x(i,2:dim)*lams(1:dim-1)))) 
                 smoothu(i,:) = 1.0_pr/2.0_pr +  (1.0_pr/2.0_pr - peakchange) * DTANH(zdiff/ictanhdelta)
              END DO
           END IF
        END IF
  END SUBROUTINE smoothX

  SUBROUTINE smoothpert (u1,u2,smoothu,uX,nlocal,methdiff,IDdiff,vartype)
    !---vartype: 1-pressure, 2-density, else-smooth based on species
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: nlocal,vartype
    REAL (pr), DIMENSION (nlocal), INTENT(IN) :: u1,u2,uX
    REAL (pr), DIMENSION (nlocal), INTENT(INOUT) :: smoothu
    INTEGER, INTENT(IN) :: methdiff, IDdiff
    REAL (pr), DIMENSION (dim) :: delxyz
    REAL (pr), DIMENSION (Nspecm,nlocal,dim) :: du
    REAL (pr), DIMENSION (Nspecm,nlocal,dim) :: d2u
    INTEGER :: Nt, tstp
    REAL (pr) :: zdiff
    INTEGER :: i
    INTEGER, PARAMETER :: meth=1

        IF (vartype==1) THEN
           smoothu = uX(:)*(1.0_pr/MW_in(1)-1.0_pr/MW_in(2)) + 1.0_pr/MW_in(2) !R
           CALL c_diff_fast(LOG(smoothu(:)), du(1,:,:), d2u(1,:,:), j_lev, nlocal,meth, 10, 1, 1, 1)  !dlnR/dx
           d2u(1,:,1) = smoothu
           d2u(1,:,2) = uX(:)*(gr_in(1)-gr_in(2)) + gr_in(2) !g
           DO i=1,nlocal
              zdiff=(x(i,1) - Amp*SUM(COS(x(i,2:dim)*lams(1:dim-1))))
              IF (zdiff .gt. 0.0_pr) THEN
                 smoothu(i) = u1(i)*Pint*EXP(-d2u(1,i,2)/d2u(1,i,1)/Tconst*( zdiff-icerfdelta**2/2.0_pr*du(1,i,1) ))/(Pint*EXP(-pureg(1)/pureR(1)/Tconst*zdiff))
              ELSE
                 smoothu(i) = u2(i)*Pint*EXP(-d2u(1,i,2)/d2u(1,i,1)/Tconst*( zdiff-icerfdelta**2/2.0_pr*du(1,i,1) ))/(Pint*EXP(-pureg(2)/pureR(2)/Tconst*zdiff))
              END IF
           END DO
        ELSE IF (vartype==2) THEN
           smoothu = uX(:)*(1.0_pr/MW_in(1)-1.0_pr/MW_in(2)) + 1.0_pr/MW_in(2) !R
           CALL c_diff_fast(LOG(smoothu(:)), du(1,:,:), d2u(1,:,:), j_lev, nlocal,meth, 10, 1, 1, 1)  !dlnR/dx
           d2u(1,:,1) = smoothu
           d2u(1,:,2) = uX(:)*(gr_in(1)-gr_in(2)) + gr_in(2) !g
           DO i=1,nlocal
              zdiff=(x(i,1) - Amp*SUM(COS(x(i,2:dim)*lams(1:dim-1))))
              IF (zdiff .gt. 0.0_pr) THEN
                 smoothu(i) = u1(i)*Pint*EXP(-d2u(1,i,2)/d2u(1,i,1)/Tconst*( zdiff-icerfdelta**2/2.0_pr*du(1,i,1) ))/d2u(1,i,1)/Tconst/(Pint*EXP(-pureg(1)/pureR(1)/Tconst*zdiff)/pureR(1)/Tconst)
              ELSE
                 smoothu(i) = u2(i)*Pint*EXP(-d2u(1,i,2)/d2u(1,i,1)/Tconst*( zdiff-icerfdelta**2/2.0_pr*du(1,i,1) ))/d2u(1,i,1)/Tconst/(Pint*EXP(-pureg(2)/pureR(2)/Tconst*zdiff)/pureR(2)/Tconst)
              END IF
           END DO
        ELSE
           IF (pertuseX) THEN
              smoothu(:) = uX(:)*(u1(:)-u2(:)) + u2(:)
           ELSE IF (pertuseY) THEN
              smoothu(:) = uX(:)*MW_in(1)/(uX(:)*(MW_in(1)-MW_in(2))+MW_in(2))*(u1(:)-u2(:)) + u2(:)
           ELSE IF (pertstepic .OR. pertdoicdiff) THEN
              DO i=1,nlocal
                 zdiff=(x(i,1) - Amp*SUM(COS(x(i,2:dim)*lams(1:dim-1))))
                 IF (zdiff .gt. 1.0e-12_pr) THEN
                    smoothu(i) = u1(i)
                 ELSEIF (zdiff .lt. -1.0e-12_pr) THEN
                    smoothu(i) = u2(i)
                 ELSE
                    smoothu(i) = 0.5_pr*(u1(i)+u2(i))
                 END IF
              END DO
              IF (pertdoicdiff) CALL mydiffsmooth (smoothu,nlocal,1,methdiff,IDdiff,pertdir,pert_dfac,pert_delta,pert_deltadel,pert_j,j_lev,1,.TRUE.) !smooth X
           ELSE 
              DO i=1,nlocal
                 zdiff=(x(i,1) - Amp*SUM(COS(x(i,2:dim)*lams(1:dim-1)))) 
                 IF (pertuseerf) THEN
                    smoothu(i) = ( (1.0_pr+ DERF(zdiff/ icerfdelta))*u1(i) + (1.0_pr- DERF(zdiff/ icerfdelta))*u2(i) )*0.5_pr
                 ELSE
                    smoothu(i) = ( (1.0_pr+DTANH(zdiff/ictanhdelta))*u1(i) + (1.0_pr-DTANH(zdiff/ictanhdelta))*u2(i) )*0.5_pr
                 END IF
              END DO
           END IF
        END IF
  END SUBROUTINE smoothpert

  SUBROUTINE mydiffsmooth (smoothu,nlocal,ne_local,methdiff,IDdiff,diffdir,Dfac_loc,delta_loc,deltadel_loc,j_sm_in,j_st_in,BCtype,boundit)
    !mydiffsmooth uses explicit solver of the diffusion eqn to smooth fields
    !written by Scott Reckinger
    !
    !PARAMETERS:
    !   smoothu      - Both input and output of field that will be smoothed
    !   nlocal       - field size (number of points in smoothu)
    !   ne_local     - number of variables in smoothu
    !   methdiff     - meth for c_diff_fast (0 or 1 for central, 2 or 3 for backward, 4 or 5 for forward; even for low order, odd for high order) 
    !   IDdiff       - ID for c_diff_fast (+/- [10, 01, or 11])
    !   diffdir      - direction of diffusion (0 = all directions, [1, 2, or 3] = diffuse only in that direction)
    !   Dfac_loc     - factor applied to stability requirement for dt (1 = at stability requirement, 0.1 = one tenth of stability requirement)
    !   delta_loc    - approximate post-smoothing diffusion thickness for and input step function, used to set total diffusion time; set <0 to use deltadel_loc
    !   deltadel_loc - sets final diffusion thickness using spatial resolution (dx, dy, or dz), number of points across final thickness, used to set total diffusion time; only if delta_loc<0
    !   j_sm_in      - level to set dx, dy, and dz for smoothing; only used for deltadel_loc; set =-1 to use j_lev, set =-2 to use j_mx, set =-3 to use j_mx-1
    !   j_st_in      - level to set dx, dy, and dz for stability
    !   BCtype       - type of boundary treatment; ODD=Dirichlet (use input boundary values)
    !                                              EVEN=Diffuse parallel to boundary only
    !                                               >2 for Neumann BCs at final iteration
    !                                               >4 for N BCs every iteration
    !   boundit      - set .TRUE. to bound the fields by the input max and min values during diffusion smoothing
    !
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: nlocal, ne_local, j_sm_in, j_st_in
    REAL (pr), DIMENSION (nlocal,ne_local), INTENT(INOUT) :: smoothu
    INTEGER, INTENT(IN) :: methdiff, IDdiff, BCtype !ODD=Dirichlet, EVEN=Diffuse in parallel direction to boundary, >2 for Neumann BCs at final iteration, >4 for Neumann BCs every iteration
    INTEGER, INTENT(IN) :: diffdir !0=diff all direction, 1,2,3=diff direction
    LOGICAL, INTENT(IN) :: boundit !TRUE to bound solution by initial max and min
    REAL(pr), INTENT(IN) :: Dfac_loc, delta_loc, deltadel_loc
    REAL (pr) :: diffdt, difft, curt, mydelta
    REAL (pr), DIMENSION (ne_local) :: umax,umin
    REAL (pr), DIMENSION (dim) :: delxyz_sm, delxyz_st
    REAL (pr), DIMENSION (ne_local,nlocal,dim) :: du
    REAL (pr), DIMENSION (ne_local,nlocal,dim) :: d2u
    INTEGER :: Nt, tstp, i, ii, j_sm, j_st
    INTEGER, PARAMETER :: meth=1
    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nlocal) :: iloc

       IF (boundit) THEN
          DO i=1,ne_local
             umax(i)=MAXVAL(smoothu(:,i))
             umin(i)=MINVAL(smoothu(:,i))
             CALL parallel_global_sum (REALMAXVAL=umax(i))
             CALL parallel_global_sum (REALMINVAL=umin(i))
          END DO
       END IF
       j_sm=j_sm_in
       j_st=j_st_in
       IF (j_sm==-1) j_sm=j_lev
       IF (j_sm==-2) j_sm=j_mx
       IF (j_sm==-3) j_sm=j_mx-1
       delxyz_st(1:dim)=(xyzlimits(2,1:dim)-xyzlimits(1,1:dim))/REAL(mxyz(1:dim)*2**(j_st-1),pr)  !delxyz_st = L/N in every direction on j_st (for stability)
       delxyz_sm(1:dim)=(xyzlimits(2,1:dim)-xyzlimits(1,1:dim))/REAL(mxyz(1:dim)*2**(j_sm-1),pr)  !delxyz_sm = L/N in every direction on j_sm (for smoothing)
       IF (diffdir .EQ. 0) THEN
          diffdt=Dfac_loc/2.0_pr/(SUM(1.0_pr/delxyz_st(:)**2))
       ELSE
          diffdt=Dfac_loc/2.0_pr*delxyz_st(diffdir)**2
       END IF
       mydelta=delta_loc
       IF (mydelta .LT. 0.0_pr) mydelta=deltadel_loc*delxyz_sm(1)
       difft=mydelta**2/4.0_pr/pi
       Nt=CEILING(difft/diffdt)
       tstp=0
       curt=0.0_pr
       IF (par_rank.EQ.0 .AND. verb_level>0) THEN
          PRINT *, '*******BEFORE INITIALIZATION DIFFUSION FOR PERTURBATION*******'
          IF (diffdir .EQ. 0) THEN
             PRINT *, 'DIFFUSING IN ALL DIRECTIONS'
          ELSE
             PRINT *, 'DIFFUSING ONLY IN THE DIRECTION:', diffdir
          END IF
          WRITE(*,'("diff dt=",E12.5,"  stop t=",E12.5,"  Nt=",I5)') diffdt, difft, Nt
       END IF
       DO WHILE(curt.lt.difft)  
          curt=curt+diffdt
          tstp=tstp+1   
          CALL c_diff_fast(smoothu(:,1:ne_local), du(1:ne_local,:,:), d2u(1:ne_local,:,:), j_lev, nlocal, methdiff, IDdiff, ne_local, 1, ne_local)

          DO i=1,ne_local
             i_p_face(0) = 1
             DO ii=1,dim
                i_p_face(ii) = i_p_face(ii-1)*3
             END DO
             DO face_type = 0, 3**dim - 1
                face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
                CALL get_all_indices_by_face (face_type, j_lev, nloc, iloc)
                IF(nloc > 0 ) THEN 
                   IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
                      IF (MOD(BCtype,2) .EQ. 0) THEN !diffuse parallel to boundaries
                         IF (diffdir .EQ. 0) THEN
                            smoothu(iloc(1:nloc),i)=smoothu(iloc(1:nloc),i)+diffdt*SUM(d2u(i,iloc(1:nloc),:),DIM=2)
                         ELSE
                            smoothu(iloc(1:nloc),i)=smoothu(iloc(1:nloc),i)+diffdt*d2u(i,iloc(1:nloc),diffdir)
                         END IF
                         DO ii=1,dim                   
                            IF ( (ABS(face(ii))==1) .AND. (diffdir==0 .OR. diffdir==ii) ) smoothu(iloc(1:nloc),i)=smoothu(iloc(1:nloc),i)-diffdt*d2u(i,iloc(1:nloc),ii)
                         END DO
                      END IF !IF BCtype==ODD, Dirichlet conditions require nothing to be done on boundaries
                   ELSE
                      IF (diffdir .EQ. 0) THEN
                         smoothu(iloc(1:nloc),i)=smoothu(iloc(1:nloc),i)+diffdt*SUM(d2u(i,iloc(1:nloc),:),DIM=2)
                      ELSE
                         smoothu(iloc(1:nloc),i)=smoothu(iloc(1:nloc),i)+diffdt*d2u(i,iloc(1:nloc),diffdir)
                      END IF
                   END IF
                END IF
             END DO
          END DO
!Neumann BCs here
          IF (boundit) THEN
             DO i=1,ne_local
                DO ii=1,nlocal
                   smoothu(ii,i) = MIN(umax(i),MAX(umin(i),smoothu(ii,i)))
                END DO
             END DO
          END IF
       END DO
       IF (par_rank.EQ.0 .AND. verb_level>0) THEN
          PRINT *, '********AFTER INITIALIZATION DIFFUSION FOR PERTURBATION*******'
          WRITE(*,'("diff dt=",E12.5,"    stop t=",E12.5,"         Nt=",I5)') diffdt, difft, Nt
          WRITE(*,'("diff dt=",E12.5,"  actual t=",E12.5,"  actual Nt=",I5)') diffdt, curt, tstp
       END IF
  END SUBROUTINE mydiffsmooth

  SUBROUTINE shiftindices(uwlt, f, jlev, nlocal, ne_local)
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: jlev, nlocal, ne_local
    REAL (pr), DIMENSION (nwlt,ne_local), INTENT (IN)    :: uwlt
    REAL (pr), DIMENSION (nlocal,ne_local), INTENT (INOUT) :: f 

    INTEGER :: nloc, ixyz, ixyz_jlev, i
    INTEGER :: j, j_df, face_type, wlt_type, shift
    INTEGER :: mysum

       mysum = 0

       DO j = 1, jlev
          DO wlt_type = MIN(j-1,1),2**dim-1
             DO face_type = 0, 3**dim - 1
                DO j_df = j, j_lev
                   nloc = indx_DB(j_df,wlt_type,face_type,j)%length
                   mysum=mysum+nloc
                   IF(nloc > 0) THEN
                      shift = indx_DB(jlev,wlt_type,face_type,j)%shift
                      DO i =1, nloc
                         ixyz = indx_DB(j_df,wlt_type,face_type,j)%p(i)%i 
                         ixyz_jlev = ixyz+shift
                         f(ixyz_jlev,:) = uwlt(ixyz,:)
                      END DO
                   END IF
               END DO
             END DO
          END DO
       END DO
  END SUBROUTINE shiftindices

  SUBROUTINE my_hydropress (myp,nlocal)
    !--Gives hydrostatic pressure with constant Temperature
    IMPLICIT NONE
    INTEGER, PARAMETER :: ne_local = 1
    INTEGER, INTENT (IN) :: nlocal
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT(INOUT) :: myp
    REAL (pr), DIMENSION (nlocal,ne_local) :: f
    REAL (pr), DIMENSION (ne_local) :: scl_p
    INTEGER, DIMENSION(ne_local) :: clip
    INTEGER :: i, mymeth

    IF (hydrofull .OR. (hydrotype .ne. 1)) curspec = 0
    f = myrhs_hydropress_FB(nlocal,ne_local,j_lev)
    clip = 0
    mymeth = 1
    scl_p = Pint

    IF (hydrosolver == 1) THEN
       IF (par_rank.EQ.0) PRINT *, 'In my_hydropress, calling Linsolve.........'
       CALL Linsolve (myp, f , mytol, nlocal, ne_local, clip, myLu_hydropress_FB, myLu_diag_hydropress_FB, SCL=scl_p) !with scaling
       IF (par_rank.EQ.0) PRINT *, 'In my_hydropress, ................Linsolved'
    ELSEIF (hydrosolver == 2) THEN
       IF (par_rank.EQ.0) PRINT *, 'In my_hydropress, calling gmres.........'
       CALL gmres (j_lev, mykry, nlocal, ne_local, mymeth, clip, myp, f, myLu_hydropress_FB, myLu_diag_hydropress_FB)
       IF (par_rank.EQ.0) PRINT *, 'In my_hydropress, ...............gmresed'
    ELSEIF (hydrosolver == 3) THEN
       IF (par_rank.EQ.0) PRINT *, 'In my_hydropress, calling BiCGSTAB.........'
       CALL BiCGSTAB (j_lev, nlocal, ne_local, mylen, mymeth, clip, mytol, myp, f, myLu_hydropress_FB, myLu_diag_hydropress_FB, SCL=scl_p) !with scaling
       IF (par_rank.EQ.0) PRINT *, 'In my_hydropress, ...............BiCGSTABed'
    ELSEIF (hydrosolver == 4) THEN
       IF (par_rank.EQ.0) PRINT *, 'In my_hydropress, calling Jacoby.........'
       CALL Jacoby (myp, f, j_lev, nlocal, ne_local,mylen, mymeth, clip, myLu_hydropress_FB, myLu_diag_hydropress_FB)
       IF (par_rank.EQ.0) PRINT *, 'In my_hydropress, ...............Jacboyed'
    END IF
  END SUBROUTINE my_hydropress

  FUNCTION myLu_hydropress_FB (jlev, myu, nlocal, ne_local, meth_in)
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: jlev, nlocal, ne_local, meth_in
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (INOUT) :: myu
    REAL (pr), DIMENSION (nlocal*ne_local) :: myLu_hydropress_FB

    INTEGER :: i, ii, meth, methc, methf, methb, methuse, idim
    REAL (pr), DIMENSION (ne_local,nlocal,dim) :: du
    REAL (pr), DIMENSION (ne_local,nlocal,dim) :: d2u
    REAL (pr), DIMENSION (nlocal,1:Nspecm) :: localY
    REAL (pr) :: myg, myr

    IF (hydrotype == 1) THEN
       IF (hydrofull) THEN
          CALL shiftindices(globalY, localY, jlev, nlocal, ne_local)
          DO ii=1,nlocal
             myr = (1.0_pr-SUM(localY(ii,:)))/MW_in(Nspec)  !R
             myg = (1.0_pr-SUM(localY(ii,:)))*gr_in(Nspec)  !g in x-direction
             DO i=1,Nspecm
                myr = myr + localY(ii,i)/MW_in(i)
                myg = myg + gr_in(i)*localY(ii,i)
             END DO
             localY(ii,1) = myg/Tconst/myr  
          END DO
       ELSE
          localY(:,1) = pureg(curspec)/Tconst/pureR(curspec)  
       END IF
    ELSE
       localY(:,1) = 0.0_pr
    END IF

    methc = meth_in
    methf = meth_in + BIASING_FORWARD
    methb = meth_in + BIASING_BACKWARD

    IF (methpdic == 0) THEN
       CALL c_diff_fast (myu, du, d2u, jlev, nlocal, methc, 10, ne_local, 1, ne_local)
       myLu_hydropress_FB(1:nlocal) = du(1,1:nlocal,1) + localY(1:nlocal,1)*myu(1:nlocal) 
    ELSE
       CALL c_diff_fast (myu, du, d2u, jlev, nlocal, methb, 10, ne_local, 1, ne_local)
       myLu_hydropress_FB(1:nlocal) = du(1,1:nlocal,1) 
       CALL c_diff_fast (myu, du, d2u, jlev, nlocal, methf, 10, ne_local, 1, ne_local)
       d2u(1,:,1) = 0.0_pr
       DO i=1,nlocal
          IF (x(i,1) .gt. 0.0_pr) d2u(1,i,1) = 1.0_pr
       END DO
       myLu_hydropress_FB(1:nlocal) = myLu_hydropress_FB(1:nlocal)*d2u(1,1:nlocal,1) + du(1,1:nlocal,1)*(1.0_pr-d2u(1,1:nlocal,1)) + localY(1:nlocal,1)*myu(1:nlocal) 
    END IF

    DO i=1,nlocal
       IF (ABS(x(i,1)) .lt. 1.0e-12) myLu_hydropress_FB(i) = myu(i)
       IF (cuttop .AND. curspec == 1 .AND. (x(i,1) .lt. cuttopat)) myLu_hydropress_FB(i) = myu(i)
       IF (cutbot .AND. curspec == 2 .AND. (x(i,1) .gt. cutbotat)) myLu_hydropress_FB(i) = myu(i)
    END DO
  END FUNCTION myLu_hydropress_FB

  FUNCTION myLu_diag_hydropress_FB (jlev,  nlocal, ne_local, meth_in)
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: jlev, nlocal, ne_local, meth_in
    REAL (pr), DIMENSION (nlocal*ne_local) :: myLu_diag_hydropress_FB

    INTEGER :: i, ii, methc, methf, methb, methuse
    REAL (pr), DIMENSION (nlocal,dim) :: du_diag, d2u_diag
    REAL (pr), DIMENSION (nlocal,1:Nspecm) :: localY
    REAL (pr) :: myg, myr

    IF (hydrotype == 1) THEN
       IF (hydrofull) THEN
          CALL shiftindices(globalY, localY, jlev, nlocal, ne_local)
          DO ii=1,nlocal
             myr = (1.0_pr-SUM(localY(ii,:)))/MW_in(Nspec)  !R
             myg = (1.0_pr-SUM(localY(ii,:)))*gr_in(Nspec)  !g in x-direction
             DO i=1,Nspecm
                myr = myr + localY(ii,i)/MW_in(i)
                myg = myg + gr_in(i)*localY(ii,i)
             END DO
             localY(ii,1) = myg/Tconst/myr  
          END DO
       ELSE
          localY(:,1) = pureg(curspec)/Tconst/pureR(curspec)  
       END IF
    ELSE
       localY(:,1) = 0.0_pr
    END IF

    methc = meth_in
    methf = meth_in + BIASING_FORWARD
    methb = meth_in + BIASING_BACKWARD

    IF (methpdic == 0) THEN
       CALL c_diff_diag ( du_diag, d2u_diag, jlev, nlocal, methc, methc, 10)
       myLu_diag_hydropress_FB(1:nlocal) = du_diag(1:nlocal,1) + localY(1:nlocal,1) 
    ELSE
       CALL c_diff_diag ( du_diag, d2u_diag, jlev, nlocal, methb, methb, 10)
       myLu_diag_hydropress_FB(1:nlocal) = du_diag(1:nlocal,1) 
       CALL c_diff_diag ( du_diag, d2u_diag, jlev, nlocal, methf, methf, 10)
       d2u_diag(:,1) = 0.0_pr
       DO i=1,nlocal
          IF (x(i,1) .gt. 0.0_pr) d2u_diag(i,1) = 1.0_pr
       END DO
       myLu_diag_hydropress_FB(1:nlocal) = myLu_diag_hydropress_FB(1:nlocal)*d2u_diag(1:nlocal,1) + du_diag(1:nlocal,1)*(1.0_pr-d2u_diag(1:nlocal,1)) + localY(1:nlocal,1) 
    END IF

    DO i=1,nlocal
       IF (ABS(x(i,1)) .lt. 1.0e-12) myLu_diag_hydropress_FB(i) = 1.0_pr
       IF (cuttop .AND. curspec == 1 .AND. (x(i,1) .lt. cuttopat)) myLu_diag_hydropress_FB(i) = 1.0_pr
       IF (cutbot .AND. curspec == 2 .AND. (x(i,1) .gt. cutbotat)) myLu_diag_hydropress_FB(i) = 1.0_pr
    END DO
  END FUNCTION myLu_diag_hydropress_FB

  FUNCTION myrhs_hydropress_FB(nlocal, ne_local,jlev)
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: nlocal, ne_local, jlev
    REAL (pr), DIMENSION (nlocal,ne_local) :: myrhs_hydropress_FB
    INTEGER :: i, ii, idim
    REAL (pr), DIMENSION (nlocal) :: localrho
    REAL (pr), DIMENSION (nlocal,1:Nspecm) :: localY
    REAL (pr) :: myg, myr

    IF (hydrotype == 1) THEN
       myrhs_hydropress_FB(1:nlocal,1) = 0.0_pr
    ELSE
       CALL shiftindices(globalrho, localrho, jlev, nlocal, ne_local)
       CALL shiftindices(globalY, localY, jlev, nlocal, ne_local)
       DO ii=1,nlocal
          myg = (1.0_pr-SUM(localY(ii,:)))*gr_in(Nspec)  !g in x-direction
          DO i=1,Nspecm
             myg = myg + gr_in(i)*localY(ii,i)
          END DO
          myrhs_hydropress_FB(ii,1) = -myg*localrho(ii)
       END DO
    END IF

    DO i=1,nlocal
       IF (ABS(x(i,1)) .lt. 1.0e-12) myrhs_hydropress_FB(i,1) = Pint
       IF (cuttop .AND. curspec == 1 .AND. (x(i,1) .lt. cuttopat)) myrhs_hydropress_FB(i,1) = Pint*EXP(-pureg(curspec)/Tconst/pureR(curspec)*cuttopat)
       IF (cutbot .AND. curspec == 2 .AND. (x(i,1) .gt. cutbotat)) myrhs_hydropress_FB(i,1) = Pint*EXP(-pureg(curspec)/Tconst/pureR(curspec)*cutbotat)
    END DO
  END FUNCTION myrhs_hydropress_FB


!--********************************  
  ! Arguments
  ! u         - field on adaptive grid
  ! nlocal      - number of active points
  ! ne_local       - number of equations
  ! t         - current time
  !
  ! Global variables used
  ! ifn       - number of equations
  ! x()
  ! xO
  ! y0
  !--******************************** 
  SUBROUTINE user_algebraic_BC (Lu, uin, nlocal, ne_local, jlev, meth)
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: jlev, meth, ne_local, nlocal
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (INOUT) :: Lu
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (IN)    :: uin

    INTEGER :: ie, i, ii, shift, denshift
    REAL (pr), DIMENSION (ne_local,nlocal,dim) :: du, d2u 
    REAL (pr), DIMENSION (1,nlocal,dim) :: du_prev
    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc

    denshift=nlocal*(nden-1)
    IF (BCver == 5 ) CALL c_diff_fast (u_prev_timestep(denshift+1:denshift+nlocal), du_prev, d2u, jlev, nlocal, meth, 10, 1, 1, 1)
    IF (BCver == 15 .OR. BCver == 19) CALL c_diff_fast (u_for_BCs(denshift+1:denshift+nlocal), du_prev, d2u, jlev, nlocal, meth, 10, 1, 1, 1)

    CALL c_diff_fast (uin, du, d2u, jlev, nlocal, meth, 10, ne_local, 1, ne_local)
    DO ie = 1, ne_local
       shift = nlocal*(ie-1)
       i_p_face(0) = 1
       DO i=1,dim
          i_p_face(i) = i_p_face(i-1)*3
       END DO
       DO face_type = 0, 3**dim - 1
          face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
          IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
             CALL get_all_indices_by_face (face_type, jlev, nloc, iloc)
             IF(nloc > 0 ) THEN 
                IF( face(1) == 1  ) THEN
                   IF (BCver == 100) THEN
                      IF(ie == nvel(1)) THEN
                         Lu(shift+iloc(1:nloc)) = uin(shift+iloc(1:nloc))  !Dirichlet conditions
                      END IF
                   ELSE IF (BCver == 101) THEN 
                      IF(ie == nvel(1)) THEN
                         Lu(shift+iloc(1:nloc)) = uin(shift+iloc(1:nloc))  !Dirichlet conditions
                      ELSE IF(ie >= nvel(2) .AND. ie <= nvel(dim)) THEN
                         Lu(shift+iloc(1:nloc)) = uin(shift+iloc(1:nloc))  !Dirichlet conditions
                      END IF
                   ELSE IF (BCver == 5) THEN
                      IF(ie == nvel(1)) THEN
                         Lu(shift+iloc(1:nloc)) = uin(shift+iloc(1:nloc))  !Dirichlet conditions
                      ELSE IF(ie >= nvel(2) .AND. ie <= nvel(dim)) THEN
                         Lu(shift+iloc(1:nloc)) = du(ie,iloc(1:nloc),1) - uin(shift+iloc(1:nloc))/u_prev_timestep(denshift+iloc(1:nloc))*du_prev(1,iloc(1:nloc),1) &
                                                + u_prev_timestep(shift+iloc(1:nloc))/u_prev_timestep(denshift+iloc(1:nloc)) * &
                                                ( du_prev(1,iloc(1:nloc),1)*uin(denshift+iloc(1:nloc))/u_prev_timestep(denshift+iloc(1:nloc)) - du(nden,iloc(1:nloc),1))
                      ELSE IF(ie == neng) THEN
                         Lu(shift+iloc(1:nloc)) = du(ie,iloc(1:nloc),1) - uin(shift+iloc(1:nloc))/u_prev_timestep(denshift+iloc(1:nloc))*du_prev(1,iloc(1:nloc),1) &
                                                + u_prev_timestep(shift+iloc(1:nloc))/u_prev_timestep(denshift+iloc(1:nloc)) * &
                                                ( du_prev(1,iloc(1:nloc),1)*uin(denshift+iloc(1:nloc))/u_prev_timestep(denshift+iloc(1:nloc)) - du(nden,iloc(1:nloc),1))
                      ELSE IF(ie >= nspc(1) .AND. ie <= nspc(Nspecm)) THEN
                         Lu(shift+iloc(1:nloc)) = du(ie,iloc(1:nloc),1) - uin(shift+iloc(1:nloc))/u_prev_timestep(denshift+iloc(1:nloc))*du_prev(1,iloc(1:nloc),1) &
                                                + u_prev_timestep(shift+iloc(1:nloc))/u_prev_timestep(denshift+iloc(1:nloc)) * &
                                                ( du_prev(1,iloc(1:nloc),1)*uin(denshift+iloc(1:nloc))/u_prev_timestep(denshift+iloc(1:nloc)) - du(nden,iloc(1:nloc),1))
                      END IF
                   ELSE IF (BCver == 11) THEN 
                      IF(ie == nvel(1)) THEN
                         Lu(shift+iloc(1:nloc)) = uin(shift+iloc(1:nloc))  !Dirichlet conditions
                      ELSE IF(ie >= nvel(2) .AND. ie <= nvel(dim)) THEN
                         Lu(shift+iloc(1:nloc)) = uin(shift+iloc(1:nloc))  !Dirichlet conditions
                      ELSE IF(ie == neng) THEN
                         Lu(shift+iloc(1:nloc)) = uin(shift+iloc(1:nloc)) - uin(denshift+iloc(1:nloc))*e0top  !Dirichlet conditions
                      ELSE IF(ie >= nspc(1) .AND. ie <= nspc(Nspecm)) THEN
                         Lu(shift+iloc(1:nloc)) = uin(shift+iloc(1:nloc)) - uin(denshift+iloc(1:nloc))*Y0top(ie-nspc(1)+1)  !Dirichlet conditions
                      END IF
                   ELSE IF (BCver == 15) THEN
                      IF(ie == nvel(1)) THEN
                         Lu(shift+iloc(1:nloc)) = uin(shift+iloc(1:nloc))  !Dirichlet conditions
                      ELSE IF(ie >= nvel(2) .AND. ie <= nvel(dim)) THEN
                         Lu(shift+iloc(1:nloc)) = du(ie,iloc(1:nloc),1) - uin(shift+iloc(1:nloc))/u_for_BCs(denshift+iloc(1:nloc))*du_prev(1,iloc(1:nloc),1) 
                      ELSE IF(ie == neng) THEN
                         Lu(shift+iloc(1:nloc)) = du(ie,iloc(1:nloc),1) - uin(shift+iloc(1:nloc))/u_for_BCs(denshift+iloc(1:nloc))*du_prev(1,iloc(1:nloc),1) 
                      ELSE IF(ie >= nspc(1) .AND. ie <= nspc(Nspecm)) THEN
                         Lu(shift+iloc(1:nloc)) = du(ie,iloc(1:nloc),1) - uin(shift+iloc(1:nloc))/u_for_BCs(denshift+iloc(1:nloc))*du_prev(1,iloc(1:nloc),1) 
                      END IF
                   ELSE IF (BCver == 19) THEN
                      IF(ie == nvel(1)) THEN
                         Lu(shift+iloc(1:nloc)) = uin(shift+iloc(1:nloc))  !Dirichlet conditions
                      ELSE IF(ie >= nvel(2) .AND. ie <= nvel(dim)) THEN
                         Lu(shift+iloc(1:nloc)) = du(ie,iloc(1:nloc),1) - uin(shift+iloc(1:nloc))/u_for_BCs(denshift+iloc(1:nloc))*du_prev(1,iloc(1:nloc),1) 
                      ELSE IF(ie == neng) THEN
                         Lu(shift+iloc(1:nloc)) = uin(shift+iloc(1:nloc)) - uin(denshift+iloc(1:nloc))*e0top  !Dirichlet conditions
                      ELSE IF(ie >= nspc(1) .AND. ie <= nspc(Nspecm)) THEN
                         Lu(shift+iloc(1:nloc)) = uin(shift+iloc(1:nloc)) - uin(denshift+iloc(1:nloc))*Y0top(ie-nspc(1)+1)  !Dirichlet conditions
                      END IF
                   ELSE IF (BCver == -20) THEN 
                      IF(ie == neng) THEN
                         d2u(1,:,1) = 0.0_pr
                         DO i=1,dim
                            d2u(1,iloc(1:nloc),1) = d2u(1,iloc(1:nloc),1) + uin(nlocal*(nvel(i)-1)+iloc(1:nloc))**2
                         END DO
                         Lu(shift+iloc(1:nloc)) = uin(shift+iloc(1:nloc)) - d2u(1,iloc(1:nloc),1)/uin(denshift+iloc(1:nloc))/2.0_pr  !Dirichlet conditions
                      ELSE IF(ie >= nspc(1) .AND. ie <= nspc(Nspecm)) THEN
                         Lu(shift+iloc(1:nloc)) = uin(shift+iloc(1:nloc)) - uin(denshift+iloc(1:nloc))*Y0top(ie-nspc(1)+1)  !Dirichlet conditions
                      END IF
                   ELSE IF (BCver == -21) THEN 
                      IF(ie == neng) THEN
                         d2u(:,:,:) = 0.0_pr
                         DO i=1,Nspecm
                            d2u(2,iloc(1:nloc),1) = d2u(2,iloc(1:nloc),1) + uin(nlocal*(nspc(i)-1)+iloc(1:nloc))*cp_in(i) !rhocp_l
                            d2u(2,iloc(1:nloc),2) = d2u(2,iloc(1:nloc),2) + uin(nlocal*(nspc(i)-1)+iloc(1:nloc))/MW_in(i) !rhoR_l
                            d2u(1,iloc(1:nloc),2) = d2u(1,iloc(1:nloc),2) + uin(nlocal*(nspc(i)-1)+iloc(1:nloc))          !rhoY_l
                         END DO
                         d2u(2,iloc(1:nloc),1) = d2u(2,iloc(1:nloc),1) + (uin(denshift+iloc(1:nloc)) - d2u(1,iloc(1:nloc),2))*cp_in(Nspec) !rhocp
                         d2u(2,iloc(1:nloc),2) = d2u(2,iloc(1:nloc),2) + (uin(denshift+iloc(1:nloc)) - d2u(1,iloc(1:nloc),2))/MW_in(Nspec) !rhoR
                         DO i=1,dim
                            d2u(1,iloc(1:nloc),1) = d2u(1,iloc(1:nloc),1) + uin(nlocal*(nvel(1)-1)+iloc(1:nloc))**2 
                         END DO
                         Lu(shift+iloc(1:nloc)) = uin(shift+iloc(1:nloc)) - d2u(1,iloc(1:nloc),1)/uin(denshift+iloc(1:nloc))/2.0_pr - &
                                                        P0(1)*(d2u(2,iloc(1:nloc),1)/d2u(2,iloc(1:nloc),2) - 1.0_pr)  !Dirichlet conditions
                      END IF
                   ELSE IF (BCver == -22) THEN 
                      IF(ie == neng) THEN
                         d2u(:,:,:) = 0.0_pr
                         DO i=1,Nspecm
                            d2u(2,iloc(1:nloc),1) = d2u(2,iloc(1:nloc),1) + uin(nlocal*(nspc(i)-1)+iloc(1:nloc))*cp_in(i) !rhocp_l
                            d2u(2,iloc(1:nloc),2) = d2u(2,iloc(1:nloc),2) + uin(nlocal*(nspc(i)-1)+iloc(1:nloc))/MW_in(i) !rhoR_l
                            d2u(1,iloc(1:nloc),2) = d2u(1,iloc(1:nloc),2) + uin(nlocal*(nspc(i)-1)+iloc(1:nloc))          !rhoY_l
                         END DO
                         d2u(2,iloc(1:nloc),1) = d2u(2,iloc(1:nloc),1) + (uin(denshift+iloc(1:nloc)) - d2u(1,iloc(1:nloc),2))*cp_in(Nspec) !rhocp
                         d2u(2,iloc(1:nloc),2) = d2u(2,iloc(1:nloc),2) + (uin(denshift+iloc(1:nloc)) - d2u(1,iloc(1:nloc),2))/MW_in(Nspec) !rhoR
                         DO i=1,dim
                            d2u(1,iloc(1:nloc),1) = d2u(1,iloc(1:nloc),1) + uin(nlocal*(nvel(1)-1)+iloc(1:nloc))**2 
                         END DO
                         Lu(shift+iloc(1:nloc)) = uin(shift+iloc(1:nloc)) - d2u(1,iloc(1:nloc),1)/uin(denshift+iloc(1:nloc))/2.0_pr - &
                                                        Tconst*(d2u(2,iloc(1:nloc),1)-d2u(2,iloc(1:nloc),2))  !Dirichlet conditions
                      END IF
                   ELSE IF (BCver == -30) THEN 
                      IF(ie == neng) THEN
                         d2u(1,:,1) = 0.0_pr
                         DO i=1,dim
                            d2u(1,iloc(1:nloc),1) = d2u(1,iloc(1:nloc),1) + u_prev_timestep(nlocal*(nvel(i)-1)+iloc(1:nloc))/u_prev_timestep(denshift+iloc(1:nloc)) *  &
                             (uin(nlocal*(nvel(i)-1)+iloc(1:nloc)) - u_prev_timestep(nlocal*(nvel(i)-1)+iloc(1:nloc))*uin(denshift+iloc(1:nloc))/u_prev_timestep(denshift+iloc(1:nloc))/2.0_pr)
                         END DO
                         Lu(shift+iloc(1:nloc)) = uin(shift+iloc(1:nloc)) - d2u(1,iloc(1:nloc),1) !Dirichlet conditions
                      ELSE IF(ie >= nspc(1) .AND. ie <= nspc(Nspecm)) THEN
                         Lu(shift+iloc(1:nloc)) = uin(shift+iloc(1:nloc)) - uin(denshift+iloc(1:nloc))*Y0top(ie-nspc(1)+1)  !Dirichlet conditions
                      END IF
                   ELSE IF (BCver == -32) THEN 
                      IF(ie == neng) THEN
                         d2u(:,:,:) = 0.0_pr
                         DO i=1,Nspecm
                            d2u(2,iloc(1:nloc),1) = d2u(2,iloc(1:nloc),1) + uin(nlocal*(nspc(i)-1)+iloc(1:nloc))*cp_in(i) !rhocp_l
                            d2u(2,iloc(1:nloc),2) = d2u(2,iloc(1:nloc),2) + uin(nlocal*(nspc(i)-1)+iloc(1:nloc))/MW_in(i) !rhoR_l
                            d2u(1,iloc(1:nloc),2) = d2u(1,iloc(1:nloc),2) + uin(nlocal*(nspc(i)-1)+iloc(1:nloc))          !rhoY_l
                         END DO
                         d2u(2,iloc(1:nloc),1) = d2u(2,iloc(1:nloc),1) + (uin(denshift+iloc(1:nloc)) - d2u(1,iloc(1:nloc),2))*cp_in(Nspec) !rhocp
                         d2u(2,iloc(1:nloc),2) = d2u(2,iloc(1:nloc),2) + (uin(denshift+iloc(1:nloc)) - d2u(1,iloc(1:nloc),2))/MW_in(Nspec) !rhoR
                         DO i=1,dim
                            d2u(1,iloc(1:nloc),1) = d2u(1,iloc(1:nloc),1) + u_prev_timestep(nlocal*(nvel(i)-1)+iloc(1:nloc))/u_prev_timestep(denshift+iloc(1:nloc)) *  &
                             (uin(nlocal*(nvel(i)-1)+iloc(1:nloc)) - u_prev_timestep(nlocal*(nvel(i)-1)+iloc(1:nloc))*uin(denshift+iloc(1:nloc))/u_prev_timestep(denshift+iloc(1:nloc))/2.0_pr)
                         END DO
                         Lu(shift+iloc(1:nloc)) = uin(shift+iloc(1:nloc)) - d2u(1,iloc(1:nloc),1) - &
                                                        Tconst*(d2u(2,iloc(1:nloc),1)-d2u(2,iloc(1:nloc),2))  !Dirichlet conditions
                      END IF
                   ELSE IF (BCver == 1) THEN 
                      IF(ie == nvel(1)) THEN
                         Lu(shift+iloc(1:nloc)) = uin(shift+iloc(1:nloc))  !Dirichlet conditions
                      ELSE IF(ie >= nvel(2) .AND. ie <= nvel(dim)) THEN
                         Lu(shift+iloc(1:nloc)) = uin(shift+iloc(1:nloc))  !Dirichlet conditions
                      ELSE IF(ie == neng) THEN
                         Lu(shift+iloc(1:nloc)) = uin(shift+iloc(1:nloc))  !Dirichlet conditions
                      ELSE IF(ie >= nspc(1) .AND. ie <= nspc(Nspecm)) THEN
                         Lu(shift+iloc(1:nloc)) = uin(shift+iloc(1:nloc))  !Dirichlet conditions
                      END IF
                   END IF
                ELSE IF( face(1) == -1  ) THEN
                   IF (BCver == 100) THEN
                      IF(ie == nvel(1)) THEN
                         Lu(shift+iloc(1:nloc)) = uin(shift+iloc(1:nloc))  !Dirichlet conditions
                      END IF
                   ELSE IF (BCver == 101) THEN 
                      IF(ie == nvel(1)) THEN
                         Lu(shift+iloc(1:nloc)) = uin(shift+iloc(1:nloc))  !Dirichlet conditions
                      ELSE IF(ie >= nvel(2) .AND. ie <= nvel(dim)) THEN
                         Lu(shift+iloc(1:nloc)) = uin(shift+iloc(1:nloc))  !Dirichlet conditions
                      END IF
                   ELSE IF (BCver == 5) THEN
                      IF(ie == nvel(1)) THEN
                         Lu(shift+iloc(1:nloc)) = uin(shift+iloc(1:nloc))  !Dirichlet conditions
                      ELSE IF(ie >= nvel(2) .AND. ie <= nvel(dim)) THEN
                         Lu(shift+iloc(1:nloc)) = du(ie,iloc(1:nloc),1) - uin(shift+iloc(1:nloc))/u_prev_timestep(denshift+iloc(1:nloc))*du_prev(1,iloc(1:nloc),1) &
                                                + u_prev_timestep(shift+iloc(1:nloc))/u_prev_timestep(denshift+iloc(1:nloc)) * &
                                                ( du_prev(1,iloc(1:nloc),1)*uin(denshift+iloc(1:nloc))/u_prev_timestep(denshift+iloc(1:nloc)) - du(nden,iloc(1:nloc),1))
                      ELSE IF(ie == neng) THEN
                         Lu(shift+iloc(1:nloc)) = du(ie,iloc(1:nloc),1) - uin(shift+iloc(1:nloc))/u_prev_timestep(denshift+iloc(1:nloc))*du_prev(1,iloc(1:nloc),1) &
                                                + u_prev_timestep(shift+iloc(1:nloc))/u_prev_timestep(denshift+iloc(1:nloc)) * &
                                                ( du_prev(1,iloc(1:nloc),1)*uin(denshift+iloc(1:nloc))/u_prev_timestep(denshift+iloc(1:nloc)) - du(nden,iloc(1:nloc),1))
                      ELSE IF(ie >= nspc(1) .AND. ie <= nspc(Nspecm)) THEN
                         Lu(shift+iloc(1:nloc)) = du(ie,iloc(1:nloc),1) - uin(shift+iloc(1:nloc))/u_prev_timestep(denshift+iloc(1:nloc))*du_prev(1,iloc(1:nloc),1) &
                                                + u_prev_timestep(shift+iloc(1:nloc))/u_prev_timestep(denshift+iloc(1:nloc)) * &
                                                ( du_prev(1,iloc(1:nloc),1)*uin(denshift+iloc(1:nloc))/u_prev_timestep(denshift+iloc(1:nloc)) - du(nden,iloc(1:nloc),1))
                      END IF
                   ELSE IF (BCver == 11) THEN 
                      IF(ie == nvel(1)) THEN
                         Lu(shift+iloc(1:nloc)) = uin(shift+iloc(1:nloc))  !Dirichlet conditions
                      ELSE IF(ie >= nvel(2) .AND. ie <= nvel(dim)) THEN
                         Lu(shift+iloc(1:nloc)) = uin(shift+iloc(1:nloc))  !Dirichlet conditions
                      ELSE IF(ie == neng) THEN
                         Lu(shift+iloc(1:nloc)) = uin(shift+iloc(1:nloc)) - uin(denshift+iloc(1:nloc))*e0bot  !Dirichlet conditions
                      ELSE IF(ie >= nspc(1) .AND. ie <= nspc(Nspecm)) THEN
                         Lu(shift+iloc(1:nloc)) = uin(shift+iloc(1:nloc)) - uin(denshift+iloc(1:nloc))*Y0bot(ie-nspc(1)+1)  !Dirichlet conditions
                      END IF
                   ELSE IF (BCver == 15) THEN
                      IF(ie == nvel(1)) THEN
                         Lu(shift+iloc(1:nloc)) = uin(shift+iloc(1:nloc))  !Dirichlet conditions
                      ELSE IF(ie >= nvel(2) .AND. ie <= nvel(dim)) THEN
                         Lu(shift+iloc(1:nloc)) = du(ie,iloc(1:nloc),1) - uin(shift+iloc(1:nloc))/u_for_BCs(denshift+iloc(1:nloc))*du_prev(1,iloc(1:nloc),1) 
                      ELSE IF(ie == neng) THEN
                         Lu(shift+iloc(1:nloc)) = du(ie,iloc(1:nloc),1) - uin(shift+iloc(1:nloc))/u_for_BCs(denshift+iloc(1:nloc))*du_prev(1,iloc(1:nloc),1) 
                      ELSE IF(ie >= nspc(1) .AND. ie <= nspc(Nspecm)) THEN
                         Lu(shift+iloc(1:nloc)) = du(ie,iloc(1:nloc),1) - uin(shift+iloc(1:nloc))/u_for_BCs(denshift+iloc(1:nloc))*du_prev(1,iloc(1:nloc),1) 
                      END IF
                   ELSE IF (BCver == 19) THEN
                      IF(ie == nvel(1)) THEN
                         Lu(shift+iloc(1:nloc)) = uin(shift+iloc(1:nloc))  !Dirichlet conditions
                      ELSE IF(ie >= nvel(2) .AND. ie <= nvel(dim)) THEN
                         Lu(shift+iloc(1:nloc)) = du(ie,iloc(1:nloc),1) - uin(shift+iloc(1:nloc))/u_for_BCs(denshift+iloc(1:nloc))*du_prev(1,iloc(1:nloc),1) 
                      ELSE IF(ie == neng) THEN
                         Lu(shift+iloc(1:nloc)) = uin(shift+iloc(1:nloc)) - uin(denshift+iloc(1:nloc))*e0bot  !Dirichlet conditions
                      ELSE IF(ie >= nspc(1) .AND. ie <= nspc(Nspecm)) THEN
                         Lu(shift+iloc(1:nloc)) = uin(shift+iloc(1:nloc)) - uin(denshift+iloc(1:nloc))*Y0bot(ie-nspc(1)+1)  !Dirichlet conditions
                      END IF
                   ELSE IF (BCver == -20) THEN 
                      IF(ie == neng) THEN
                         d2u(1,:,1) = 0.0_pr
                         DO i=1,dim
                            d2u(1,iloc(1:nloc),1) = d2u(1,iloc(1:nloc),1) + uin(nlocal*(nvel(i)-1)+iloc(1:nloc))**2
                         END DO
                         Lu(shift+iloc(1:nloc)) = uin(shift+iloc(1:nloc)) - d2u(1,iloc(1:nloc),1)/uin(denshift+iloc(1:nloc))/2.0_pr  !Dirichlet conditions
                      ELSE IF(ie >= nspc(1) .AND. ie <= nspc(Nspecm)) THEN
                         Lu(shift+iloc(1:nloc)) = uin(shift+iloc(1:nloc)) - uin(denshift+iloc(1:nloc))*Y0bot(ie-nspc(1)+1)  !Dirichlet conditions
                      END IF
                   ELSE IF (BCver == -21) THEN 
                      IF(ie == neng) THEN
                         d2u(:,:,:) = 0.0_pr
                         DO i=1,Nspecm
                            d2u(2,iloc(1:nloc),1) = d2u(2,iloc(1:nloc),1) + uin(nlocal*(nspc(i)-1)+iloc(1:nloc))*cp_in(i) !rhocp_l
                            d2u(2,iloc(1:nloc),2) = d2u(2,iloc(1:nloc),2) + uin(nlocal*(nspc(i)-1)+iloc(1:nloc))/MW_in(i) !rhoR_l
                            d2u(1,iloc(1:nloc),2) = d2u(1,iloc(1:nloc),2) + uin(nlocal*(nspc(i)-1)+iloc(1:nloc))          !rhoY_l
                         END DO
                         d2u(2,iloc(1:nloc),1) = d2u(2,iloc(1:nloc),1) + (uin(denshift+iloc(1:nloc)) - d2u(1,iloc(1:nloc),2))*cp_in(Nspec) !rhocp
                         d2u(2,iloc(1:nloc),2) = d2u(2,iloc(1:nloc),2) + (uin(denshift+iloc(1:nloc)) - d2u(1,iloc(1:nloc),2))/MW_in(Nspec) !rhoR
                         DO i=1,dim
                            d2u(1,iloc(1:nloc),1) = d2u(1,iloc(1:nloc),1) + uin(nlocal*(nvel(1)-1)+iloc(1:nloc))**2 
                         END DO
                         Lu(shift+iloc(1:nloc)) = uin(shift+iloc(1:nloc)) - d2u(1,iloc(1:nloc),1)/uin(denshift+iloc(1:nloc))/2.0_pr - &
                                                        P0(2)*(d2u(2,iloc(1:nloc),1)/d2u(2,iloc(1:nloc),2) - 1.0_pr)  !Dirichlet conditions
                      END IF
                   ELSE IF (BCver == -22) THEN 
                      IF(ie == neng) THEN
                         d2u(:,:,:) = 0.0_pr
                         DO i=1,Nspecm
                            d2u(2,iloc(1:nloc),1) = d2u(2,iloc(1:nloc),1) + uin(nlocal*(nspc(i)-1)+iloc(1:nloc))*cp_in(i) !rhocp_l
                            d2u(2,iloc(1:nloc),2) = d2u(2,iloc(1:nloc),2) + uin(nlocal*(nspc(i)-1)+iloc(1:nloc))/MW_in(i) !rhoR_l
                            d2u(1,iloc(1:nloc),2) = d2u(1,iloc(1:nloc),2) + uin(nlocal*(nspc(i)-1)+iloc(1:nloc))          !rhoY_l
                         END DO
                         d2u(2,iloc(1:nloc),1) = d2u(2,iloc(1:nloc),1) + (uin(denshift+iloc(1:nloc)) - d2u(1,iloc(1:nloc),2))*cp_in(Nspec) !rhocp
                         d2u(2,iloc(1:nloc),2) = d2u(2,iloc(1:nloc),2) + (uin(denshift+iloc(1:nloc)) - d2u(1,iloc(1:nloc),2))/MW_in(Nspec) !rhoR
                         DO i=1,dim
                            d2u(1,iloc(1:nloc),1) = d2u(1,iloc(1:nloc),1) + uin(nlocal*(nvel(1)-1)+iloc(1:nloc))**2 
                         END DO
                         Lu(shift+iloc(1:nloc)) = uin(shift+iloc(1:nloc)) - d2u(1,iloc(1:nloc),1)/uin(denshift+iloc(1:nloc))/2.0_pr - &
                                                        Tconst*(d2u(2,iloc(1:nloc),1)-d2u(2,iloc(1:nloc),2))  !Dirichlet conditions
                      END IF
                   ELSE IF (BCver == -30) THEN 
                      IF(ie == neng) THEN
                         d2u(1,:,1) = 0.0_pr
                         DO i=1,dim
                            d2u(1,iloc(1:nloc),1) = d2u(1,iloc(1:nloc),1) + u_prev_timestep(nlocal*(nvel(i)-1)+iloc(1:nloc))/u_prev_timestep(denshift+iloc(1:nloc)) *  &
                             (uin(nlocal*(nvel(i)-1)+iloc(1:nloc)) - u_prev_timestep(nlocal*(nvel(i)-1)+iloc(1:nloc))*uin(denshift+iloc(1:nloc))/u_prev_timestep(denshift+iloc(1:nloc))/2.0_pr)
                         END DO
                         Lu(shift+iloc(1:nloc)) = uin(shift+iloc(1:nloc)) - d2u(1,iloc(1:nloc),1) !Dirichlet conditions
                      ELSE IF(ie >= nspc(1) .AND. ie <= nspc(Nspecm)) THEN
                         Lu(shift+iloc(1:nloc)) = uin(shift+iloc(1:nloc)) - uin(denshift+iloc(1:nloc))*Y0top(ie-nspc(1)+1)  !Dirichlet conditions
                      END IF
                   ELSE IF (BCver == -32) THEN 
                      IF(ie == neng) THEN
                         d2u(:,:,:) = 0.0_pr
                         DO i=1,Nspecm
                            d2u(2,iloc(1:nloc),1) = d2u(2,iloc(1:nloc),1) + uin(nlocal*(nspc(i)-1)+iloc(1:nloc))*cp_in(i) !rhocp_l
                            d2u(2,iloc(1:nloc),2) = d2u(2,iloc(1:nloc),2) + uin(nlocal*(nspc(i)-1)+iloc(1:nloc))/MW_in(i) !rhoR_l
                            d2u(1,iloc(1:nloc),2) = d2u(1,iloc(1:nloc),2) + uin(nlocal*(nspc(i)-1)+iloc(1:nloc))          !rhoY_l
                         END DO
                         d2u(2,iloc(1:nloc),1) = d2u(2,iloc(1:nloc),1) + (uin(denshift+iloc(1:nloc)) - d2u(1,iloc(1:nloc),2))*cp_in(Nspec) !rhocp
                         d2u(2,iloc(1:nloc),2) = d2u(2,iloc(1:nloc),2) + (uin(denshift+iloc(1:nloc)) - d2u(1,iloc(1:nloc),2))/MW_in(Nspec) !rhoR
                         DO i=1,dim
                            d2u(1,iloc(1:nloc),1) = d2u(1,iloc(1:nloc),1) + u_prev_timestep(nlocal*(nvel(i)-1)+iloc(1:nloc))/u_prev_timestep(denshift+iloc(1:nloc)) *  &
                             (uin(nlocal*(nvel(i)-1)+iloc(1:nloc)) - u_prev_timestep(nlocal*(nvel(i)-1)+iloc(1:nloc))*uin(denshift+iloc(1:nloc))/u_prev_timestep(denshift+iloc(1:nloc))/2.0_pr)
                         END DO
                         Lu(shift+iloc(1:nloc)) = uin(shift+iloc(1:nloc)) - d2u(1,iloc(1:nloc),1) - &
                                                        Tconst*(d2u(2,iloc(1:nloc),1)-d2u(2,iloc(1:nloc),2))  !Dirichlet conditions
                      END IF
                   ELSE IF (BCver == 1) THEN 
                      IF(ie == nvel(1)) THEN
                         Lu(shift+iloc(1:nloc)) = uin(shift+iloc(1:nloc))  !Dirichlet conditions
                      ELSE IF(ie >= nvel(2) .AND. ie <= nvel(dim)) THEN
                         Lu(shift+iloc(1:nloc)) = uin(shift+iloc(1:nloc))  !Dirichlet conditions
                      ELSE IF(ie == neng) THEN
                         Lu(shift+iloc(1:nloc)) = uin(shift+iloc(1:nloc))  !Dirichlet conditions
                      ELSE IF(ie >= nspc(1) .AND. ie <= nspc(Nspecm)) THEN
                         Lu(shift+iloc(1:nloc)) = uin(shift+iloc(1:nloc))  !Dirichlet conditions
                      END IF
                   END IF
                END IF
             END IF
          END IF
       END DO
    END DO
  END SUBROUTINE user_algebraic_BC

  SUBROUTINE user_algebraic_BC_diag (Lu_diag, nlocal, ne_local, jlev, meth)
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: jlev, meth, ne_local, nlocal
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (INOUT) :: Lu_diag

    INTEGER :: ie, i, ii, shift, denshift

    REAL (pr), DIMENSION (nlocal,dim) :: du_diag, d2u_diag 
    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc
    REAL (pr), DIMENSION (1,nlocal,dim) :: du_prev, d2u
    INTEGER, PARAMETER :: methprev=1

    denshift = nlocal*(nden-1)

    IF (BCver == 5)  CALL c_diff_fast (u_prev_timestep(denshift+1:denshift+nlocal), du_prev, d2u, jlev, nlocal, meth, 10, 1, 1, 1)
    IF (BCver == 15 .OR. BCver == 19)  CALL c_diff_fast (u_for_BCs(denshift+1:denshift+nlocal), du_prev, d2u, jlev, nlocal, meth, 10, 1, 1, 1)

    CALL c_diff_diag ( du_diag, d2u_diag, jlev, nlocal, meth, meth, 10)

    DO ie = 1, ne_local
       shift = nlocal*(ie-1)
       i_p_face(0) = 1
       DO i=1,dim
          i_p_face(i) = i_p_face(i-1)*3
       END DO
       DO face_type = 0, 3**dim - 1
          face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
          IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
             CALL get_all_indices_by_face (face_type, jlev, nloc, iloc)
             IF(nloc > 0 ) THEN 
                IF( ABS(face(1)) == 1  ) THEN 
                   IF (BCver == 100) THEN
                      IF(ie == nvel(1)) THEN
                         Lu_diag(shift+iloc(1:nloc)) = 1.0_pr  !Dirichlet conditions
                      END IF
                   ELSE IF (BCver == 101) THEN 
                      IF(ie == nvel(1)) THEN
                         Lu_diag(shift+iloc(1:nloc)) = 1.0_pr  !Dirichlet conditions
                      ELSE IF(ie >= nvel(2) .AND. ie <= nvel(dim)) THEN
                         Lu_diag(shift+iloc(1:nloc)) = 1.0_pr  !Dirichlet conditions
                      END IF
                   ELSE IF (BCver == 5) THEN
                      IF(ie == nvel(1)) THEN
                         Lu_diag(shift+iloc(1:nloc)) = 1.0_pr  !Dirichlet conditions
                      ELSE IF(ie >= nvel(2) .AND. ie <= nvel(dim)) THEN
                         Lu_diag(shift+iloc(1:nloc)) = du_diag(iloc(1:nloc),1) - du_prev(1,iloc(1:nloc),1)/u_prev_timestep(denshift+iloc(1:nloc))
                      ELSE IF(ie == neng) THEN
                         Lu_diag(shift+iloc(1:nloc)) = du_diag(iloc(1:nloc),1) - du_prev(1,iloc(1:nloc),1)/u_prev_timestep(denshift+iloc(1:nloc))
                      ELSE IF(ie >= nspc(1) .AND. ie <= nspc(Nspecm)) THEN
                         Lu_diag(shift+iloc(1:nloc)) = du_diag(iloc(1:nloc),1) - du_prev(1,iloc(1:nloc),1)/u_prev_timestep(denshift+iloc(1:nloc))
                      END IF
                   ELSE IF (BCver == 15) THEN
                      IF(ie == nvel(1)) THEN
                         Lu_diag(shift+iloc(1:nloc)) = 1.0_pr  !Dirichlet conditions
                      ELSE IF(ie >= nvel(2) .AND. ie <= nvel(dim)) THEN
                         Lu_diag(shift+iloc(1:nloc)) = du_diag(iloc(1:nloc),1) - du_prev(1,iloc(1:nloc),1)/u_for_BCs(denshift+iloc(1:nloc))
                      ELSE IF(ie == neng) THEN
                         Lu_diag(shift+iloc(1:nloc)) = du_diag(iloc(1:nloc),1) - du_prev(1,iloc(1:nloc),1)/u_for_BCs(denshift+iloc(1:nloc))
                      ELSE IF(ie >= nspc(1) .AND. ie <= nspc(Nspecm)) THEN
                         Lu_diag(shift+iloc(1:nloc)) = du_diag(iloc(1:nloc),1) - du_prev(1,iloc(1:nloc),1)/u_for_BCs(denshift+iloc(1:nloc))
                      END IF
                   ELSE IF (BCver == 19) THEN
                      IF(ie == nvel(1)) THEN
                         Lu_diag(shift+iloc(1:nloc)) = 1.0_pr  !Dirichlet conditions
                      ELSE IF(ie >= nvel(2) .AND. ie <= nvel(dim)) THEN
                         Lu_diag(shift+iloc(1:nloc)) = du_diag(iloc(1:nloc),1) - du_prev(1,iloc(1:nloc),1)/u_for_BCs(denshift+iloc(1:nloc))
                      ELSE IF(ie == neng) THEN
                         Lu_diag(shift+iloc(1:nloc)) = 1.0_pr  !Dirichlet conditions
                      ELSE IF(ie >= nspc(1) .AND. ie <= nspc(Nspecm)) THEN
                         Lu_diag(shift+iloc(1:nloc)) = 1.0_pr  !Dirichlet conditions
                      END IF
                   ELSE IF (BCver == 1 .OR. BCver == 11) THEN 
                      IF(ie == nvel(1)) THEN
                         Lu_diag(shift+iloc(1:nloc)) = 1.0_pr  !Dirichlet conditions
                      ELSE IF(ie >= nvel(2) .AND. ie <= nvel(dim)) THEN
                         Lu_diag(shift+iloc(1:nloc)) = 1.0_pr  !Dirichlet conditions
                      ELSE IF(ie == neng) THEN
                         Lu_diag(shift+iloc(1:nloc)) = 1.0_pr  !Dirichlet conditions
                      ELSE IF(ie >= nspc(1) .AND. ie <= nspc(Nspecm)) THEN
                         Lu_diag(shift+iloc(1:nloc)) = 1.0_pr  !Dirichlet conditions
                      END IF
                   ELSE IF (BCver == -20 .OR. BCver == -30) THEN 
                      IF(ie == neng) THEN
                         Lu_diag(shift+iloc(1:nloc)) = 1.0_pr  !Dirichlet conditions
                      ELSE IF(ie >= nspc(1) .AND. ie <= nspc(Nspecm)) THEN
                         Lu_diag(shift+iloc(1:nloc)) = 1.0_pr  !Dirichlet conditions
                      END IF
                   ELSE IF (BCver == -21 .OR. BCver == -22 .OR. BCver == -32) THEN 
                      IF(ie == neng) THEN
                         Lu_diag(shift+iloc(1:nloc)) = 1.0_pr  !Dirichlet conditions
                      END IF
                   END IF
                END IF
             END IF
          END IF
       END DO
    END DO
  END SUBROUTINE user_algebraic_BC_diag

  SUBROUTINE user_algebraic_BC_rhs (rhs, ne_local, nlocal, jlev)
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: ne_local, nlocal, jlev
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (INOUT) :: rhs

    INTEGER :: ie, i, ii, shift, denshift
    INTEGER, PARAMETER ::  meth=1
    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc
    REAL (pr), DIMENSION (ne_local,nlocal,dim) :: duall, d2uall
    REAL (pr), DIMENSION (1,nlocal,dim) :: du_prev, du, d2u  

    denshift=nlocal*(nden-1)

    DO ie = 1, ne_local
       shift = nlocal*(ie-1)
       i_p_face(0) = 1
       DO i=1,dim
          i_p_face(i) = i_p_face(i-1)*3
       END DO
       DO face_type = 0, 3**dim - 1
          face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
          IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
             CALL get_all_indices_by_face (face_type, jlev, nloc, iloc)
             IF(nloc > 0 ) THEN 
                IF( face(1) == 1  ) THEN              
                   IF (BCver == 100) THEN
                      IF(ie == nvel(1)) THEN
                         rhs(shift+iloc(1:nloc)) = 0.0_pr 
                      END IF
                   ELSE IF (BCver == 101) THEN 
                      IF(ie == nvel(1)) THEN
                         rhs(shift+iloc(1:nloc)) = 0.0_pr 
                      ELSE IF(ie >= nvel(2) .AND. ie <= nvel(dim)) THEN
                         rhs(shift+iloc(1:nloc)) = 0.0_pr 
                      END IF
                   ELSE IF (BCver == 5 .OR. BCver == 11 .OR. BCver == 15 .OR. BCver == 19) THEN
                      IF(ie == nvel(1)) THEN
                         rhs(shift+iloc(1:nloc)) = 0.0_pr 
                      ELSE IF(ie >= nvel(2) .AND. ie <= nvel(dim)) THEN
                         rhs(shift+iloc(1:nloc)) = 0.0_pr 
                      ELSE IF(ie == neng) THEN
                         rhs(shift+iloc(1:nloc)) = 0.0_pr 
                      ELSE IF(ie >= nspc(1) .AND. ie <= nspc(Nspecm)) THEN
                         rhs(shift+iloc(1:nloc)) = 0.0_pr 
                      END IF
                   ELSE IF (BCver == -20 .OR. BCver == -30) THEN
                      IF(ie == neng) THEN
                         rhs(shift+iloc(1:nloc)) = P0(1)/(puregamma(1)-1.0_pr) 
                      ELSE IF(ie >= nspc(1) .AND. ie <= nspc(Nspecm)) THEN
                         rhs(shift+iloc(1:nloc)) = 0.0_pr 
                      END IF
                   ELSE IF (BCver == -21 .OR. BCver == -22 .OR. BCver == -32) THEN
                      IF(ie == neng) THEN
                         rhs(shift+iloc(1:nloc)) = 0.0_pr 
                      END IF
                   ELSE IF (BCver == 1) THEN 
                      IF(ie == nvel(1)) THEN
                         rhs(shift+iloc(1:nloc)) = 0.0_pr 
                      ELSE IF(ie >= nvel(2) .AND. ie <= nvel(dim)) THEN
                         rhs(shift+iloc(1:nloc)) = 0.0_pr 
                      ELSE IF(ie == neng) THEN
                         rhs(shift+iloc(1:nloc)) = rhoetop 
                      ELSE IF(ie >= nspc(1) .AND. ie <= nspc(Nspecm)) THEN
                         rhs(shift+iloc(1:nloc)) = rhoYtop(ie+1-nspc(1))
                      END IF
                   END IF
                ELSE IF( face(1) == -1  ) THEN              
                   IF (BCver == 100) THEN
                      IF(ie == nvel(1)) THEN
                         rhs(shift+iloc(1:nloc)) = 0.0_pr 
                      END IF
                   ELSE IF (BCver == 101) THEN 
                      IF(ie == nvel(1)) THEN
                         rhs(shift+iloc(1:nloc)) = 0.0_pr 
                      ELSE IF(ie >= nvel(2) .AND. ie <= nvel(dim)) THEN
                         rhs(shift+iloc(1:nloc)) = 0.0_pr 
                      END IF
                   ELSE IF (BCver == 5 .OR. BCver == 11 .OR. BCver == 15) THEN
                      IF(ie == nvel(1)) THEN
                         rhs(shift+iloc(1:nloc)) = 0.0_pr 
                      ELSE IF(ie >= nvel(2) .AND. ie <= nvel(dim)) THEN
                         rhs(shift+iloc(1:nloc)) = 0.0_pr 
                      ELSE IF(ie == neng) THEN
                         rhs(shift+iloc(1:nloc)) = 0.0_pr 
                      ELSE IF(ie >= nspc(1) .AND. ie <= nspc(Nspecm)) THEN
                         rhs(shift+iloc(1:nloc)) = 0.0_pr 
                      END IF
                   ELSE IF (BCver == -20 .OR. BCver == -30) THEN
                      IF(ie == neng) THEN
                         rhs(shift+iloc(1:nloc)) = P0(2)/(puregamma(2)-1.0_pr) 
                      ELSE IF(ie >= nspc(1) .AND. ie <= nspc(Nspecm)) THEN
                         rhs(shift+iloc(1:nloc)) = 0.0_pr 
                      END IF
                   ELSE IF (BCver == -21 .OR. BCver == -22 .OR. BCver == -32) THEN
                      IF(ie == neng) THEN
                         rhs(shift+iloc(1:nloc)) = 0.0_pr 
                      END IF
                   ELSE IF (BCver == 1) THEN
                      IF(ie == nvel(1)) THEN
                         rhs(shift+iloc(1:nloc)) = 0.0_pr 
                      ELSE IF(ie >= nvel(2) .AND. ie <= nvel(dim)) THEN
                         rhs(shift+iloc(1:nloc)) = 0.0_pr 
                      ELSE IF(ie == neng) THEN
                         rhs(shift+iloc(1:nloc)) = rhoebot 
                      ELSE IF(ie >= nspc(1) .AND. ie <= nspc(Nspecm)) THEN
                         rhs(shift+iloc(1:nloc)) = rhoYbot(ie+1-nspc(1))
                      END IF
                   END IF
                END IF
             END IF
          END IF
       END DO
    END DO
  END SUBROUTINE user_algebraic_BC_rhs

  SUBROUTINE user_evol_LODI_BC_rhs (myrhs_loc, ulc, myne_loc, mynloc, myjl, mymeth)
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: myne_loc, mynloc, myjl, mymeth
    REAL (pr), DIMENSION (mynloc*myne_loc), INTENT (INOUT) :: myrhs_loc
    REAL (pr), DIMENSION (mynloc,myne_loc), INTENT (IN) :: ulc

    INTEGER :: ie, i, ii, shift, jj
    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc

    REAL (pr), DIMENSION (mynloc,Nspec+1+dim) :: pres
    REAL (pr), DIMENSION (Nspec+1+dim,mynloc,dim) :: du, d2u
    REAL (pr), DIMENSION (:),   ALLOCATABLE :: charI, charV
    REAL (pr), DIMENSION (:,:),   ALLOCATABLE :: charX

    REAL (pr) :: faceval
    INTEGER :: l, faceint, vardnum

    vardnum = 2
    IF (chartype == 2) vardnum = Nspec+1+dim

    pres(:,dim+2) = ulc(:,nden)
    DO l=1,Nspecm
       pres(:,dim+2+l) = ulc(:,nspc(l))/ulc(:,nden)
    END DO
    DO i=1,dim
       pres(:,1+i) = ulc(:,nvel(i))/ulc(:,nden)
    END DO
    
    d2u(1,:,1) = 1.0_pr-SUM(pres(:,dim+3:dim+Nspec+1),DIM=2) !Y_Nspec
    d2u(2,:,1) = d2u(1,:,1)*cp_in(Nspec)  !cp_Nspec
    d2u(2,:,2) = d2u(1,:,1)/MW_in(Nspec)  !R_Nspec
    DO l=1,Nspecm
       d2u(2,:,1)  = d2u(2,:,1)  + pres(:,dim+2+l)*cp_in(l)  !cp
       d2u(2,:,2)  = d2u(2,:,2)  + pres(:,dim+2+l)/MW_in(l)  !R
    END DO
    d2u(1,:,1) = d2u(2,:,1)/(d2u(2,:,1)-d2u(2,:,2))  !gamma=cp/(cp-R)
    pres(:,1) = (ulc(:,neng)-0.5_pr*SUM(ulc(:,nvel(1):nvel(dim))**2,DIM=2)/ulc(:,nden))*(d2u(1,:,1)-1.0_pr)  !1: pressure
    d2u(1,:,2) = SQRT(d2u(1,:,1)*pres(:,1)/ulc(:,nden))  !c=SQRT(gamma*P/rho)

    IF ((methpd .ne. 0) .OR. splitFBpress) THEN 
       du(1,:,1) = pres(:,1)
       CALL dpdx (du(1,:,1),ng,pres(:,dim+3:dim+Nspec+1),2*methpd+mymeth,myjl,splitFBpress)
       CALL c_diff_fast(pres(:,2:vardnum), du(2:vardnum,:,:), d2u(2:vardnum,:,:), myjl, mynloc, mymeth, 10, vardnum-1, 1, vardnum-1)  
    ELSE
       CALL c_diff_fast(pres(:,1:vardnum), du(1:vardnum,:,:), d2u(1:vardnum,:,:), myjl, mynloc, mymeth, 10, vardnum,   1, vardnum  ) 
    END IF

    i_p_face(0) = 1
    DO i=1,dim
       i_p_face(i) = i_p_face(i-1)*3
    END DO
    DO face_type = 0, 3**dim - 1
       face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
       IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
          CALL get_all_indices_by_face (face_type, myjl, nloc, iloc)
          IF(nloc > 0 ) THEN
             IF (ABS(face(1)) == 1) THEN
                IF (ALLOCATED(charV)) DEALLOCATE(charV)
                IF (ALLOCATED(charI)) DEALLOCATE(charI)
                IF (ALLOCATED(charX)) DEALLOCATE(charX)
                ALLOCATE(charV(nloc), charI(nloc), charX(nloc,3))
                
                faceval=REAL(face(1),pr)
                faceint=(3-face(1))/2

                IF (chartype == 3) THEN
                   charV(1:nloc) = pres(iloc(1:nloc),2) - faceval*d2u(1,iloc(1:nloc),2)
                   charI(1:nloc) = charV(1:nloc)*(du(1,iloc(1:nloc),1) - dP0(faceint) - faceval*ulc(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)*du(2,iloc(1:nloc),1))  !(u+/-c)*(dp/dx+/-rho*c*du/dx)

                   IF     (PLpinf==1) THEN
                      charI(1:nloc) = charI(1:nloc) - pBcoef*(1.0_pr-maxMa**2)*c0(faceint)          *(pres(iloc(1:nloc),1) - P0(faceint))
                   ELSEIF (PLpinf==2) THEN
                      charI(1:nloc) = charI(1:nloc) - pBcoef*(1.0_pr-maxMa**2)*d2u(1,iloc(1:nloc),2)*(pres(iloc(1:nloc),1) - P0(faceint))
                   ELSEIF (PLpinf==3) THEN
                      charI(1:nloc) = charI(1:nloc) - pBcoef*(1.0_pr-maxMa**2)*c0(faceint)*P0(faceint)/Tconst*(pres(iloc(1:nloc),1)/ulc(iloc(1:nloc),nden)/d2u(2,iloc(1:nloc),2) - Tconst)
                   ELSEIF (PLpinf==4) THEN
                      charI(1:nloc) = charI(1:nloc) - pBcoef*(1.0_pr-maxMa**2)*d2u(1,iloc(1:nloc),2)*(pres(iloc(1:nloc),1) - ulc(iloc(1:nloc),nden)*d2u(2,iloc(1:nloc),2)*Tconst)
                   ELSEIF (PLpinf==5) THEN
                      charI(1:nloc) = charI(1:nloc) + faceval*pBcoef*(1.0_pr-maxMa**2)*gamm(faceint)*Pint                             *pres(iloc(1:nloc),2)
                   ELSEIF (PLpinf==6) THEN
                      charI(1:nloc) = charI(1:nloc) + faceval*pBcoef*(1.0_pr-maxMa**2)*ulc(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)**2*pres(iloc(1:nloc),2)
                   END IF ! I_(1 or N) - K(P-Pinf)

                   charX(1:nloc,1) = -0.5_pr/d2u(1,iloc(1:nloc),2)**2*charI(1:nloc)
                   charX(1:nloc,2) = faceval*0.5_pr/d2u(1,iloc(1:nloc),2)/ulc(iloc(1:nloc),nden)*charI(1:nloc)
                   charX(1:nloc,3) = -0.5_pr*charI(1:nloc)
                ELSE IF (chartype == 4) THEN
                   charI(1:nloc) = 2.0_pr*(pres(iloc(1:nloc),2)*(du(1,iloc(1:nloc),1)-dP0(faceint)) + ulc(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)**2*du(2,iloc(1:nloc),1))
                                   ! I_1 + I_N
                   charX(1:nloc,1) = -0.5_pr/d2u(1,iloc(1:nloc),2)**2*charI(1:nloc)
                   charX(1:nloc,2) = faceval*0.5_pr/d2u(1,iloc(1:nloc),2)/ulc(iloc(1:nloc),nden)*charI(1:nloc)
                   charX(1:nloc,3) = -0.5_pr*charI(1:nloc)
                ELSE
                   charI(1:nloc) = -2.0_pr*(d2u(1,iloc(1:nloc),2)*(du(1,iloc(1:nloc),1)-dP0(faceint)) + &
                                             ulc(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)*pres(iloc(1:nloc),2)*du(2,iloc(1:nloc),1))  ! I_1 - I_N

                   charX(1:nloc,1) = -faceval*0.5_pr/d2u(1,iloc(1:nloc),2)**2*charI(1:nloc)
                   charX(1:nloc,2) = 0.5_pr/d2u(1,iloc(1:nloc),2)/ulc(iloc(1:nloc),nden)*charI(1:nloc)
                   charX(1:nloc,3) = -faceval*0.5_pr*charI(1:nloc)                  
                   IF (chartype == 2) THEN
                      charX(1:nloc,1) = charX(1:nloc,1) - pres(iloc(1:nloc),2)*(du(dim+2,iloc(1:nloc),1)-drho0(faceint) - (du(1,iloc(1:nloc),1)-dP0(faceint))/d2u(1,iloc(1:nloc),2)**2)
                   END IF
                END IF

                myrhs_loc((nden-1)*mynloc+iloc(1:nloc)) = myrhs_loc((nden-1)*mynloc+iloc(1:nloc)) - charX(1:nloc,1)
                DO ii=nvel(1),nvel(dim)
                   myrhs_loc((ii-1)*mynloc+iloc(1:nloc)) = myrhs_loc((ii-1)*mynloc+iloc(1:nloc)) - ulc(iloc(1:nloc),ii)/ulc(iloc(1:nloc),nden)*charX(1:nloc,1)
                END DO
                myrhs_loc((nvel(1)-1)*mynloc+iloc(1:nloc)) = myrhs_loc((nvel(1)-1)*mynloc+iloc(1:nloc)) - ulc(iloc(1:nloc),nden)*charX(1:nloc,2)
                myrhs_loc((neng-1)*mynloc+iloc(1:nloc)) = myrhs_loc((neng-1)*mynloc+iloc(1:nloc)) - 1.0_pr/(d2u(1,iloc(1:nloc),1)-1.0_pr)*charX(1:nloc,3) - &
                                                          0.5_pr*SUM(ulc(iloc(1:nloc),nvel(1):nvel(dim))**2,DIM=2)/ulc(iloc(1:nloc),nden)**2*charX(1:nloc,1) - &
                                                          ulc(iloc(1:nloc),nvel(1))*charX(1:nloc,2)
                DO ii=nspc(1),nspc(Nspecm)
                   myrhs_loc((ii-1)*mynloc+iloc(1:nloc)) = myrhs_loc((ii-1)*mynloc+iloc(1:nloc)) -  ulc(iloc(1:nloc),ii)/ulc(iloc(1:nloc),nden)*charX(1:nloc,1)
                END DO

                IF (chartype == 2) THEN
                   DO ii=2,dim
                      myrhs_loc((nvel(ii)-1)*mynloc+iloc(1:nloc)) = myrhs_loc((nvel(ii)-1)*mynloc+iloc(1:nloc)) + ulc(iloc(1:nloc),nvel(1))*du(ii+1,iloc(1:nloc),1)
                      myrhs_loc((neng-1)*mynloc+iloc(1:nloc)) = myrhs_loc((neng-1)*mynloc+iloc(1:nloc)) + ulc(iloc(1:nloc),nvel(ii))*pres(iloc(1:nloc),2)*du(ii+1,iloc(1:nloc),1)
                   END DO
                   DO ii=1,Nspecm
                      myrhs_loc((nspc(ii)-1)*mynloc+iloc(1:nloc)) = myrhs_loc((nspc(ii)-1)*mynloc+iloc(1:nloc)) + ulc(iloc(1:nloc),nvel(1))*du(ii+dim+2,iloc(1:nloc),1)
                   END DO
                END IF
                IF (ALLOCATED(charV)) DEALLOCATE(charV)
                IF (ALLOCATED(charI)) DEALLOCATE(charI)
                IF (ALLOCATED(charX)) DEALLOCATE(charX)
             END IF
          END IF
       END IF
    END DO
  END SUBROUTINE user_evol_LODI_BC_rhs 

  SUBROUTINE user_evol_LODI_BC_Drhs (Dmyrhs_loc, ulc, ulc_prev, myne_loc, mynloc, myjl, mymeth)
    !--Sets rhs for boundary conditions
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: myne_loc, mynloc, myjl, mymeth
    REAL (pr), DIMENSION (mynloc*myne_loc), INTENT (INOUT) :: Dmyrhs_loc
    REAL (pr), DIMENSION (mynloc,myne_loc), INTENT (IN) :: ulc, ulc_prev

    INTEGER :: ie, i, ii, shift, jj
    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc

    REAL (pr), DIMENSION (mynloc,Nspec+1+dim) :: pres, pres_prev
    REAL (pr), DIMENSION (Nspec+1+dim,mynloc,dim) :: du, d2u, du_prev
    REAL (pr), DIMENSION (:),   ALLOCATABLE :: charI, charV, charI_prev, charV_prev
    REAL (pr), DIMENSION (:,:),   ALLOCATABLE :: charX, charX_prev
    
    REAL (pr) :: faceval
    INTEGER :: l, faceint, vardnum

    vardnum = 2
    IF (chartype == 2) vardnum = Nspec+1+dim

    pres_prev(:,dim+2) = ulc_prev(:,nden)  !rho0
    DO l=1,Nspecm
       pres_prev(:,dim+2+l) = ulc_prev(:,nspc(l))/ulc_prev(:,nden)  !Y0_l
    END DO
    DO i=1,dim
       pres_prev(:,1+i) = ulc_prev(:,nvel(i))/ulc_prev(:,nden)  !u0_i
    END DO

    pres(:,dim+2) = ulc(:,nden)  !rho'
    DO l=1,Nspecm
       pres(:,dim+2+l) = (ulc(:,nspc(l)) - ulc_prev(:,nspc(l))*ulc(:,nden)/ulc_prev(:,nden))/ulc_prev(:,nden)  !Y'_l
    END DO
    DO i=1,dim
       pres(:,1+i) = (ulc(:,nvel(i)) - ulc_prev(:,nvel(i))*ulc(:,nden)/ulc_prev(:,nden))/ulc_prev(:,nden)  !u'_i
    END DO

    d2u(1,:,1) = 1.0_pr-SUM(ulc_prev(:,nspc(1):nspc(Nspecm)),DIM=2)/ulc_prev(:,nden) !Y0_Nspec
    d2u(1,:,2) = (SUM(ulc_prev(:,nspc(1):nspc(Nspecm)),DIM=2)*ulc(:,nden)/ulc_prev(:,nden) - SUM(ulc(:,nspc(1):nspc(Nspecm)),DIM=2))/ulc_prev(:,nden) !Y'_Nspec 
    d2u(4,:,1) = d2u(1,:,2)*cp_in(Nspec)  !cp'_Nspec
    d2u(4,:,2) = d2u(1,:,2)/MW_in(Nspec)  !R'_Nspec
    d2u(2,:,1) = d2u(1,:,1)*cp_in(Nspec)  !cp0_Nspec
    d2u(2,:,2) = d2u(1,:,1)/MW_in(Nspec)  !R0_Nspec
    DO l=1,Nspecm
       d2u(2,:,1)  = d2u(2,:,1)  + ulc_prev(:,nspc(l))/ulc_prev(:,nden)*cp_in(l)  !cp0
       d2u(2,:,2)  = d2u(2,:,2)  + ulc_prev(:,nspc(l))/ulc_prev(:,nden)/MW_in(l)  !R0
       d2u(4,:,1)  = d2u(4,:,1)  + (ulc(:,nspc(l))-ulc_prev(:,nspc(l))/ulc_prev(:,nden)*ulc(:,nden))/ulc_prev(:,nden)*cp_in(l)  !cp'
       d2u(4,:,2)  = d2u(4,:,2)  + (ulc(:,nspc(l))-ulc_prev(:,nspc(l))/ulc_prev(:,nden)*ulc(:,nden))/ulc_prev(:,nden)/MW_in(l)  !R'
    END DO
    d2u(1,:,1) = d2u(2,:,1)/(d2u(2,:,1)-d2u(2,:,2))  !gamma0=cp0/(cp0-R0)
    pres_prev(:,1) = (ulc_prev(:,neng)-0.5_pr*SUM(ulc_prev(:,nvel(1):nvel(dim))**2,DIM=2)/ulc_prev(:,nden))*(d2u(1,:,1)-1.0_pr)  !1: p0
    d2u(1,:,2) = SQRT(d2u(1,:,1)*pres_prev(:,1)/ulc_prev(:,nden))  !c0=SQRT(gamma0*P0/rho0) 
    d2u(3,:,1) = (d2u(2,:,1)*d2u(4,:,2) - d2u(4,:,1)*d2u(2,:,2)) / (d2u(2,:,1)-d2u(2,:,2))**2  !gamma'=(cp0*R'-cp'*R0)/(cp0-R0)^2
    pres(:,1) =  d2u(3,:,1)*(ulc_prev(:,neng)-0.5_pr*SUM(ulc_prev(:,nvel(1):nvel(dim))**2,DIM=2))/ulc_prev(:,nden) +  &
                (d2u(1,:,1)-1.0_pr)*(ulc(:,neng)+0.5_pr*SUM(ulc_prev(:,nvel(1):nvel(dim))**2,DIM=2)/ulc_prev(:,nden)**2*ulc(:,nden) -  &
                 SUM(ulc_prev(:,nvel(1):nvel(dim))*ulc(:,nvel(1):nvel(dim)),DIM=2)/ulc_prev(:,nden) )  !1: p'
    d2u(3,:,2) = 0.5_pr/SQRT(d2u(1,:,1)*pres_prev(:,1)/ulc_prev(:,nden)) * ( d2u(1,:,1)*pres(:,1)+d2u(3,:,1)*pres_prev(:,1) - &
                        d2u(1,:,1)*pres_prev(:,1)/ulc_prev(:,nden)*ulc(:,nden) )/ulc_prev(:,nden)   !c'=1/2/SQRT(gamma0*P0/rho0)*(gamma*P/rho)'

    IF ((methpd .ne. 0) .OR. splitFBpress) THEN 
       du(1,:,1) = pres(:,1)
       CALL dpdx (du(1,:,1),ng,pres_prev(:,dim+3:dim+Nspec+1),2*methpd+mymeth,myjl,splitFBpress)
       CALL c_diff_fast(pres(:,2:vardnum), du(2:vardnum,:,:), d2u(2:vardnum,:,:), myjl, mynloc, mymeth, 10, vardnum-1, 1, vardnum-1)  
    ELSE
       CALL c_diff_fast(pres(:,1:vardnum), du(1:vardnum,:,:), d2u(1:vardnum,:,:), myjl, mynloc, mymeth, 10, vardnum  , 1, vardnum  ) 
    END IF
    IF ((methpd .ne. 0) .OR. splitFBpress) THEN 
       du_prev(1,:,1) = pres_prev(:,1)
       CALL dpdx (du_prev(1,:,1),ng,pres_prev(:,dim+3:dim+Nspec+1),2*methpd+mymeth,myjl,splitFBpress)
       CALL c_diff_fast(pres_prev(:,2:vardnum), du_prev(2:vardnum,:,:), d2u(2:vardnum,:,:), myjl, mynloc, mymeth, 10, vardnum-1, 1, vardnum-1)  
    ELSE
       CALL c_diff_fast(pres_prev(:,1:vardnum), du_prev(1:vardnum,:,:), d2u(1:vardnum,:,:), myjl, mynloc, mymeth, 10, vardnum  , 1, vardnum  ) 
    END IF

    i_p_face(0) = 1
    DO i=1,dim
       i_p_face(i) = i_p_face(i-1)*3
    END DO
    DO face_type = 0, 3**dim - 1
       face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
       IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
          CALL get_all_indices_by_face (face_type, myjl, nloc, iloc)
          IF(nloc > 0 ) THEN
             IF (ABS(face(1)) == 1) THEN
                IF (ALLOCATED(charV)) DEALLOCATE(charV)
                IF (ALLOCATED(charI)) DEALLOCATE(charI)
                IF (ALLOCATED(charX)) DEALLOCATE(charX)
                IF (ALLOCATED(charV_prev)) DEALLOCATE(charV_prev)
                IF (ALLOCATED(charI_prev)) DEALLOCATE(charI_prev)
                IF (ALLOCATED(charX_prev)) DEALLOCATE(charX_prev)
                ALLOCATE(charV(nloc),charI(nloc),charX(nloc,3),charV_prev(nloc),charI_prev(nloc),charX_prev(nloc,3))

                faceval=REAL(face(1),pr)
                faceint=(3-face(1))/2

                IF (chartype == 3) THEN
                   charV_prev(1:nloc) = pres_prev(iloc(1:nloc),2) - faceval*d2u(1,iloc(1:nloc),2)
                   charV(1:nloc) = pres(iloc(1:nloc),2) - faceval*d2u(3,iloc(1:nloc),2)

                   charI_prev(1:nloc) = charV_prev(1:nloc)*(du_prev(1,iloc(1:nloc),1) - dP0(faceint) - &
                                                           faceval*ulc_prev(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)*du_prev(2,iloc(1:nloc),1))  !(u-c)*(dp/dx-rho*c*du/dx)
                   charI(1:nloc) = charV_prev(1:nloc)*(du(1,iloc(1:nloc),1) - faceval*ulc_prev(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)*du(2,iloc(1:nloc),1)) + &
                                   charV(1:nloc)*(du_prev(1,iloc(1:nloc),1) - dP0(faceint) - faceval*ulc_prev(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)*du_prev(2,iloc(1:nloc),1)) - &
                                   faceval*charV_prev(1:nloc)*du_prev(2,iloc(1:nloc),1) * (ulc(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)+ulc_prev(iloc(1:nloc),nden)*d2u(3,iloc(1:nloc),2)) 
                   IF     (PLpinf==1) THEN
                      charI_prev(1:nloc) = charI_prev(1:nloc) - pBcoef*(1.0_pr-maxMa**2)*c0(faceint)          *(pres_prev(iloc(1:nloc),1) - P0(faceint))
                      charI(1:nloc) = charI(1:nloc) - pBcoef*(1.0_pr-maxMa**2)*c0(faceint)*pres(iloc(1:nloc),1)
                   ELSEIF (PLpinf==2) THEN
                      charI_prev(1:nloc) = charI_prev(1:nloc) - pBcoef*(1.0_pr-maxMa**2)*d2u(1,iloc(1:nloc),2)*(pres_prev(iloc(1:nloc),1) - P0(faceint))
                      charI(1:nloc) = charI(1:nloc) - pBcoef*(1.0_pr-maxMa**2) * (d2u(3,iloc(1:nloc),2)*(pres_prev(iloc(1:nloc),1) - P0(faceint)) + d2u(1,iloc(1:nloc),2)*pres(iloc(1:nloc),1) )
                   ELSEIF (PLpinf==3) THEN
                      charI_prev(1:nloc) = charI_prev(1:nloc) - pBcoef*(1.0_pr-maxMa**2)*c0(faceint)*P0(faceint)/Tconst * &
                                                                   (pres_prev(iloc(1:nloc),1)/ulc_prev(iloc(1:nloc),nden)/d2u(2,iloc(1:nloc),2) - Tconst)
                      charI(1:nloc) = charI(1:nloc) - pBcoef*(1.0_pr-maxMa**2)*c0(faceint)*P0(faceint)/Tconst/ulc_prev(iloc(1:nloc),nden)/d2u(2,iloc(1:nloc),2) * &
                                           (pres(iloc(1:nloc),1)-pres_prev(iloc(1:nloc),1)*(ulc(iloc(1:nloc),nden)/ulc_prev(iloc(1:nloc),nden)+d2u(4,iloc(1:nloc),2)/d2u(2,iloc(1:nloc),2)) )
                   ELSEIF (PLpinf==4) THEN
                      charI_prev(1:nloc) = charI_prev(1:nloc) - pBcoef*(1.0_pr-maxMa**2)*d2u(1,iloc(1:nloc),2) * &
                                                                   (pres_prev(iloc(1:nloc),1) - ulc_prev(iloc(1:nloc),nden)*d2u(2,iloc(1:nloc),2)*Tconst)
                      charI(1:nloc) = charI(1:nloc) - pBcoef*(1.0_pr-maxMa**2) * & 
                                                      ( d2u(3,iloc(1:nloc),2)*(pres_prev(iloc(1:nloc),1) - ulc_prev(iloc(1:nloc),nden)*d2u(2,iloc(1:nloc),2)*Tconst) + d2u(1,iloc(1:nloc),2) * &
                                                      (pres(iloc(1:nloc),1) - ulc(iloc(1:nloc),nden)*d2u(2,iloc(1:nloc),2)*Tconst - ulc_prev(iloc(1:nloc),nden)*d2u(4,iloc(1:nloc),2)*Tconst) )
                   ELSEIF (PLpinf==5) THEN
                      charI_prev(1:nloc) = charI_prev(1:nloc) + faceval*pBcoef*(1.0_pr-maxMa**2)*gamm(faceint)*Pint*pres_prev(iloc(1:nloc),2)
                      charI(1:nloc) = charI(1:nloc) + faceval*pBcoef*(1.0_pr-maxMa**2)*gamm(faceint)*Pint*pres(iloc(1:nloc),2)
                   ELSEIF (PLpinf==6) THEN
                      charI_prev(1:nloc) = charI_prev(1:nloc) + faceval*pBcoef*(1.0_pr-maxMa**2)*ulc_prev(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)**2*pres_prev(iloc(1:nloc),2)
                      charI(1:nloc) = charI(1:nloc) + faceval*pBcoef*(1.0_pr-maxMa**2)*d2u(1,iloc(1:nloc),2) * &
                                                      (ulc(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)*pres_prev(iloc(1:nloc),2) + &
                                                       2.0_pr*ulc_prev(iloc(1:nloc),nden)*d2u(3,iloc(1:nloc),2)*pres_prev(iloc(1:nloc),2) + &
                                                       ulc_prev(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)*pres(iloc(1:nloc),2) )
                   END IF ! I_(1 or N) - K(P-Pinf)

                   charX_prev(1:nloc,1) = -0.5_pr/d2u(1,iloc(1:nloc),2)**2*charI_prev(1:nloc)
                   charX_prev(1:nloc,2) = faceval*0.5_pr/d2u(1,iloc(1:nloc),2)/ulc_prev(iloc(1:nloc),nden)*charI_prev(1:nloc)
                   charX_prev(1:nloc,3) = -0.5_pr*charI_prev(1:nloc)
                   charX(1:nloc,1) = -0.5_pr/d2u(1,iloc(1:nloc),2)**2*charI(1:nloc) + d2u(3,iloc(1:nloc),2)/d2u(1,iloc(1:nloc),2)**3*charI_prev(1:nloc)
                   charX(1:nloc,2) = faceval*0.5_pr/d2u(1,iloc(1:nloc),2)/ulc_prev(iloc(1:nloc),nden)*(charI(1:nloc)-charI_prev(1:nloc) * &
                                                    (ulc(iloc(1:nloc),nden)/ulc_prev(iloc(1:nloc),nden) + d2u(3,iloc(1:nloc),2)/d2u(1,iloc(1:nloc),2)) )
                   charX(1:nloc,3) = -0.5_pr*charI(1:nloc)
                ELSE IF (chartype == 4) THEN
                   charI_prev(1:nloc) = 2.0_pr*(pres_prev(iloc(1:nloc),2)*(du_prev(1,iloc(1:nloc),1)-dP0(faceint)) + &
                                                ulc_prev(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)**2*du_prev(2,iloc(1:nloc),1))
                   charI(1:nloc) = 2.0_pr*(pres(iloc(1:nloc),2)*(du_prev(1,iloc(1:nloc),1)-dP0(faceint)) + pres_prev(iloc(1:nloc),2)*du(1,iloc(1:nloc),1) + &
                                           d2u(1,iloc(1:nloc),2)*(ulc_prev(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)*du(2,iloc(1:nloc),1) + &
                                           du_prev(2,iloc(1:nloc),1)*(ulc(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2) + 2.0_pr*ulc_prev(iloc(1:nloc),nden)*d2u(3,iloc(1:nloc),2))))  ! I_1 + I_N

                   charX_prev(1:nloc,1) = -0.5_pr/d2u(1,iloc(1:nloc),2)**2*charI_prev(1:nloc)
                   charX_prev(1:nloc,2) = faceval*0.5_pr/d2u(1,iloc(1:nloc),2)/ulc_prev(iloc(1:nloc),nden)*charI_prev(1:nloc)
                   charX_prev(1:nloc,3) = -0.5_pr*charI_prev(1:nloc)
                   charX(1:nloc,1) = -0.5_pr/d2u(1,iloc(1:nloc),2)**2*charI(1:nloc) + d2u(3,iloc(1:nloc),2)/d2u(1,iloc(1:nloc),2)**3*charI_prev(1:nloc)
                   charX(1:nloc,2) = faceval*0.5_pr/d2u(1,iloc(1:nloc),2)/ulc_prev(iloc(1:nloc),nden)*(charI(1:nloc)-charI_prev(1:nloc) * &
                                                    (ulc(iloc(1:nloc),nden)/ulc_prev(iloc(1:nloc),nden) + d2u(3,iloc(1:nloc),2)/d2u(1,iloc(1:nloc),2)) )
                   charX(1:nloc,3) = -0.5_pr*charI(1:nloc)
                ELSE
                   charI_prev(1:nloc) = -2.0_pr*(d2u(1,iloc(1:nloc),2)*(du_prev(1,iloc(1:nloc),1)-dP0(faceint)) + &
                                                 ulc_prev(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)*pres_prev(iloc(1:nloc),2)*du_prev(2,iloc(1:nloc),1))
                   charI(1:nloc) = -2.0_pr*(d2u(3,iloc(1:nloc),2)*(du_prev(1,iloc(1:nloc),1)-dP0(faceint)) + d2u(1,iloc(1:nloc),2)*du(1,iloc(1:nloc),1) + &
                                                 ulc_prev(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)*pres_prev(iloc(1:nloc),2)*du(2,iloc(1:nloc),1) + &
                                                 du_prev(2,iloc(1:nloc),1)*(ulc(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)*pres_prev(iloc(1:nloc),2) + &
                                                 ulc_prev(iloc(1:nloc),nden)*(d2u(3,iloc(1:nloc),2)*pres_prev(iloc(1:nloc),2) + d2u(1,iloc(1:nloc),2)*pres(iloc(1:nloc),2))))  ! I_1 - I_N

                   charX_prev(1:nloc,1) = -faceval*0.5_pr/d2u(1,iloc(1:nloc),2)**2*charI_prev(1:nloc)
                   charX_prev(1:nloc,2) = 0.5_pr/d2u(1,iloc(1:nloc),2)/ulc_prev(iloc(1:nloc),nden)*charI_prev(1:nloc)
                   charX_prev(1:nloc,3) = -faceval*0.5_pr*charI_prev(1:nloc)
                   charX(1:nloc,1) = -faceval*0.5_pr/d2u(1,iloc(1:nloc),2)**2*charI(1:nloc) + d2u(3,iloc(1:nloc),2)/d2u(1,iloc(1:nloc),2)**3*charI_prev(1:nloc)
                   charX(1:nloc,2) = 0.5_pr/d2u(1,iloc(1:nloc),2)/ulc_prev(iloc(1:nloc),nden)*(charI(1:nloc)-charI_prev(1:nloc) * &
                                                    (ulc(iloc(1:nloc),nden)/ulc_prev(iloc(1:nloc),nden) + d2u(3,iloc(1:nloc),2)/d2u(1,iloc(1:nloc),2)) )
                   charX(1:nloc,3) = -faceval*0.5_pr*charI(1:nloc)
                   IF (chartype == 2) THEN
                      charX_prev(1:nloc,1) = charX_prev(1:nloc,1) - pres_prev(iloc(1:nloc),2)*(du_prev(dim+2,iloc(1:nloc),1)-drho0(faceint) - &
                                                                                              (du_prev(1,iloc(1:nloc),1)-dP0(faceint))/d2u(1,iloc(1:nloc),2)**2)
                      charX(1:nloc,1) = charX(1:nloc,1) - pres(iloc(1:nloc),2)*(du_prev(dim+2,iloc(1:nloc),1)-drho0(faceint) - &
                                                                               (du_prev(1,iloc(1:nloc),1)-dP0(faceint))/d2u(1,iloc(1:nloc),2)**2) - &
                                                          pres_prev(iloc(1:nloc),2)*(du(dim+2,iloc(1:nloc),1) - du(1,iloc(1:nloc),1)/d2u(1,iloc(1:nloc),2)**2 + &
                                                                                     2.0_pr*d2u(3,iloc(1:nloc),2)/d2u(1,iloc(1:nloc),2)**3*(du_prev(1,iloc(1:nloc),1)-dP0(faceint)))
                   END IF
                END IF

                Dmyrhs_loc((nden-1)*mynloc+iloc(1:nloc)) = Dmyrhs_loc((nden-1)*mynloc+iloc(1:nloc)) - charX(1:nloc,1)
                DO ii=nvel(1),nvel(dim)
                   Dmyrhs_loc((ii-1)*mynloc+iloc(1:nloc)) = Dmyrhs_loc((ii-1)*mynloc+iloc(1:nloc)) - ulc_prev(iloc(1:nloc),ii)/ulc_prev(iloc(1:nloc),nden)*charX(1:nloc,1) - &
                                 (ulc(iloc(1:nloc),ii) - ulc_prev(iloc(1:nloc),ii)*ulc(iloc(1:nloc),nden)/ulc_prev(iloc(1:nloc),nden) )/ulc_prev(iloc(1:nloc),nden)*charX_prev(1:nloc,1)
                END DO
                Dmyrhs_loc((nvel(1)-1)*mynloc+iloc(1:nloc)) = Dmyrhs_loc((nvel(1)-1)*mynloc+iloc(1:nloc)) - ulc_prev(iloc(1:nloc),nden)*charX(1:nloc,2) - &
                                                                                                            ulc(iloc(1:nloc),nden)*charX_prev(1:nloc,2)
                Dmyrhs_loc((neng-1)*mynloc+iloc(1:nloc)) = Dmyrhs_loc((neng-1)*mynloc+iloc(1:nloc)) - &
                               1.0_pr/(d2u(1,iloc(1:nloc),1)-1.0_pr)*(charX(1:nloc,3) - d2u(3,iloc(1:nloc),1)/(d2u(1,iloc(1:nloc),1)-1.0_pr)*charX_prev(1:nloc,3)) - & 
                               0.5_pr*SUM(ulc_prev(iloc(1:nloc),nvel(1):nvel(dim))**2,DIM=2)/ulc_prev(iloc(1:nloc),nden)**2*charX(1:nloc,1) - &
                              (SUM(ulc_prev(iloc(1:nloc),nvel(1):nvel(dim))*ulc(iloc(1:nloc),nvel(1):nvel(dim)),DIM=2) - &
                               SUM(ulc_prev(iloc(1:nloc),nvel(1):nvel(dim))**2,DIM=2)/ulc_prev(iloc(1:nloc),nden)*ulc(iloc(1:nloc),nden) ) / ulc_prev(iloc(1:nloc),nden)**2*charX_prev(1:nloc,1) - &
                               ulc_prev(iloc(1:nloc),nvel(1))*charX(1:nloc,2) - ulc(iloc(1:nloc),nvel(1))*charX_prev(1:nloc,2)
                DO ii=nspc(1),nspc(Nspecm)
                   Dmyrhs_loc((ii-1)*mynloc+iloc(1:nloc)) = Dmyrhs_loc((ii-1)*mynloc+iloc(1:nloc)) - ulc_prev(iloc(1:nloc),ii)/ulc_prev(iloc(1:nloc),nden)*charX(1:nloc,1) - &
                                 (ulc(iloc(1:nloc),ii) - ulc_prev(iloc(1:nloc),ii)*ulc(iloc(1:nloc),nden)/ulc_prev(iloc(1:nloc),nden) )/ulc_prev(iloc(1:nloc),nden)*charX_prev(1:nloc,1)
                END DO

                IF (chartype == 2) THEN
                   DO ii=2,dim
                      Dmyrhs_loc((nvel(ii)-1)*mynloc+iloc(1:nloc)) = Dmyrhs_loc((nvel(ii)-1)*mynloc+iloc(1:nloc)) + ulc_prev(iloc(1:nloc),nvel(1))*du(ii+1,iloc(1:nloc),1) + & 
                                                                                                                    ulc(iloc(1:nloc),nvel(1))*du_prev(ii+1,iloc(1:nloc),1)
                      Dmyrhs_loc((neng-1)*mynloc+iloc(1:nloc)) = Dmyrhs_loc((neng-1)*mynloc+iloc(1:nloc)) + ulc_prev(iloc(1:nloc),nvel(ii))*pres_prev(iloc(1:nloc),2)*du(ii+1,iloc(1:nloc),1) + &
                                                    du_prev(ii+1,iloc(1:nloc),1)*(ulc_prev(iloc(1:nloc),nvel(ii))*pres(iloc(1:nloc),2) + ulc(iloc(1:nloc),nvel(ii))*pres_prev(iloc(1:nloc),2))
                   END DO
                   DO ii=1,Nspecm
                      Dmyrhs_loc((nspc(ii)-1)*mynloc+iloc(1:nloc)) = Dmyrhs_loc((nspc(ii)-1)*mynloc+iloc(1:nloc)) + ulc_prev(iloc(1:nloc),nvel(1))*du(ii+dim+2,iloc(1:nloc),1) + &
                                                                                                                    ulc(iloc(1:nloc),nvel(1))*du_prev(ii+dim+2,iloc(1:nloc),1)
                   END DO
                END IF
                IF (ALLOCATED(charV)) DEALLOCATE(charV)
                IF (ALLOCATED(charI)) DEALLOCATE(charI)
                IF (ALLOCATED(charX)) DEALLOCATE(charX)
                IF (ALLOCATED(charV_prev)) DEALLOCATE(charV_prev)
                IF (ALLOCATED(charI_prev)) DEALLOCATE(charI_prev)
                IF (ALLOCATED(charX_prev)) DEALLOCATE(charX_prev)
             END IF
          END IF
       END IF
    END DO
  END SUBROUTINE user_evol_LODI_BC_Drhs 

  SUBROUTINE user_evol_LODI_BC_Drhs_diag (Drhs_diag_local, ulc_prev, myne_loc, mynloc, myjl, mymeth)
    !--Sets rhs for boundary conditions
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: myne_loc, mynloc, myjl, mymeth
    REAL (pr), DIMENSION (mynloc*myne_loc), INTENT (INOUT) :: Drhs_diag_local
    REAL (pr), DIMENSION (mynloc,myne_loc), INTENT (IN) :: ulc_prev

    INTEGER :: ie, i, ii, shift, jj
    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc

    REAL (pr), DIMENSION (:),   ALLOCATABLE :: charI, charV_prev, charI_prev, charX_prev
    REAL (pr), DIMENSION (:,:),   ALLOCATABLE :: charX
    REAL (pr), DIMENSION (mynloc,dim) :: du_diag_local, d2u_diag_local
    REAL (pr), DIMENSION (mynloc) :: du_diag_PFB
    REAL (pr), DIMENSION (mynloc,Nspec+1+dim) :: pres_prev
    REAL (pr), DIMENSION (Nspec+1+dim,mynloc,dim) :: du, d2u
    REAL (pr) :: faceval
    INTEGER :: l, faceint, vardnum

    CALL c_diff_diag(du_diag_local, d2u_diag_local, j_lev, mynloc, mymeth, mymeth, 10)

    vardnum = 2
    IF (chartype == 2) vardnum = Nspec+1+dim

    pres_prev(:,dim+2) = ulc_prev(:,nden)  !rho0
    DO l=1,Nspecm
       pres_prev(:,dim+2+l) = ulc_prev(:,nspc(l))/ulc_prev(:,nden)  !Y0_l
    END DO
    DO i=1,dim
       pres_prev(:,1+i) = ulc_prev(:,nvel(i))/ulc_prev(:,nden)  !u0_i
    END DO

    d2u(1,:,1) = 1.0_pr-SUM(ulc_prev(:,nspc(1):nspc(Nspecm)),DIM=2)/ulc_prev(:,nden) !Y0_Nspec
    d2u(2,:,1) = d2u(1,:,1)*cp_in(Nspec)  !cp0_Nspec
    d2u(2,:,2) = d2u(1,:,1)/MW_in(Nspec)  !R0_Nspec
    DO l=1,Nspecm
       d2u(2,:,1)  = d2u(2,:,1)  + ulc_prev(:,nspc(l))/ulc_prev(:,nden)*cp_in(l)  !cp0
       d2u(2,:,2)  = d2u(2,:,2)  + ulc_prev(:,nspc(l))/ulc_prev(:,nden)/MW_in(l)  !R0
    END DO
    d2u(1,:,1) = d2u(2,:,1)/(d2u(2,:,1)-d2u(2,:,2))  !gamma0=cp0/(cp0-R0)
    pres_prev(:,1) = (ulc_prev(:,neng)-0.5_pr*SUM(ulc_prev(:,nvel(1):nvel(dim))**2,DIM=2)/ulc_prev(:,nden))*(d2u(1,:,1)-1.0_pr)  !1: pressure
    d2u(1,:,2) = SQRT(d2u(1,:,1)*pres_prev(:,1)/ulc_prev(:,nden))  !c0=SQRT(gamma*P/rho)
    pres_prev(:,2) = ulc_prev(:,nvel(1))/ulc_prev(:,nden)  !u0

    IF ((methpd .ne. 0) .OR. splitFBpress) THEN 
       du(1,:,1) = pres_prev(:,1)
       CALL dpdx (du(1,:,1),ng,pres_prev(:,dim+3:dim+Nspec+1),2*methpd+mymeth,myjl,splitFBpress)
       CALL c_diff_fast(pres_prev(:,2:vardnum), du(2:vardnum,:,:), d2u(2:vardnum,:,:), myjl, mynloc, mymeth, 10, vardnum-1, 1, vardnum-1)  
       CALL dpdx_diag (du_diag_PFB(:),ng,pres_prev(:,3:Nspec+1),2*methpd+mymeth,myjl,splitFBpress)
    ELSE
       CALL c_diff_fast(pres_prev(:,1:vardnum), du(1:vardnum,:,:), d2u(1:vardnum,:,:), myjl, mynloc, mymeth, 10, vardnum  , 1, vardnum  ) 
       du_diag_PFB(:) = du_diag_local(:,1)
    END IF

    i_p_face(0) = 1
    DO i=1,dim
       i_p_face(i) = i_p_face(i-1)*3
    END DO
    DO face_type = 0, 3**dim - 1
       face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
       IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
          CALL get_all_indices_by_face (face_type, myjl, nloc, iloc)
          IF(nloc > 0 ) THEN
             IF (ABS(face(1)) == 1) THEN
                IF (ALLOCATED(charI)) DEALLOCATE(charI)
                IF (ALLOCATED(charX)) DEALLOCATE(charX)
                IF (ALLOCATED(charV_prev)) DEALLOCATE(charV_prev)
                IF (ALLOCATED(charI_prev)) DEALLOCATE(charI_prev)
                IF (ALLOCATED(charX_prev)) DEALLOCATE(charX_prev)
                ALLOCATE(charI(nloc),charX(nloc,3),charV_prev(nloc),charI_prev(nloc),charX_prev(nloc))

                faceval=REAL(face(1),pr)
                faceint=(3-face(1))/2
               
                IF (chartype == 3) THEN
                   charV_prev(1:nloc) = pres_prev(iloc(1:nloc),2) - faceval*d2u(1,iloc(1:nloc),2)

                   charI_prev(1:nloc) = charV_prev(1:nloc)*(du(1,iloc(1:nloc),1)-dP0(faceint) - &
                                                            faceval*ulc_prev(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)*du(2,iloc(1:nloc),1))  !(u-c)*(dp/dx-rho*c*du/dx)
                   IF     (PLpinf==1) THEN
                      charI_prev(1:nloc) = charI_prev(1:nloc) - pBcoef*(1.0_pr-maxMa**2)*c0(faceint)          *(pres_prev(iloc(1:nloc),1) - P0(faceint))
                   ELSEIF (PLpinf==2) THEN
                      charI_prev(1:nloc) = charI_prev(1:nloc) - pBcoef*(1.0_pr-maxMa**2)*d2u(1,iloc(1:nloc),2)*(pres_prev(iloc(1:nloc),1) - P0(faceint))
                   ELSEIF (PLpinf==3) THEN
                      charI_prev(1:nloc) = charI_prev(1:nloc) - pBcoef*(1.0_pr-maxMa**2)*c0(faceint)*P0(faceint)/Tconst* &
                                                                       (pres_prev(iloc(1:nloc),1)/ulc_prev(iloc(1:nloc),nden)/d2u(2,iloc(1:nloc),2) - Tconst)
                   ELSEIF (PLpinf==4) THEN
                      charI_prev(1:nloc) = charI_prev(1:nloc) - pBcoef*(1.0_pr-maxMa**2)*d2u(1,iloc(1:nloc),2)*(pres_prev(iloc(1:nloc),1) - ulc_prev(iloc(1:nloc),nden)*d2u(2,iloc(1:nloc),2)*Tconst)
                   ELSEIF (PLpinf==5) THEN
                      charI_prev(1:nloc) = charI_prev(1:nloc) + faceval*pBcoef*(1.0_pr-maxMa**2)*gamm(faceint)*Pint                                  *pres_prev(iloc(1:nloc),2)
                   ELSEIF (PLpinf==6) THEN
                      charI_prev(1:nloc) = charI_prev(1:nloc) + faceval*pBcoef*(1.0_pr-maxMa**2)*ulc_prev(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)**2*pres_prev(iloc(1:nloc),2)
                   END IF
 
                   charX_prev(1:nloc) = -0.5_pr/d2u(1,iloc(1:nloc),2)**2*charI_prev(1:nloc)
                
                   DO jj=1,myne_loc
                      IF (jj==nden) THEN
                         du (2,iloc(1:nloc),2) = -ulc_prev(iloc(1:nloc),nvel(1))/ulc_prev(iloc(1:nloc),nden)**2 !u_diag
                         d2u(3,iloc(1:nloc),1) = 0.0_pr  !gamma_diag
                         du (1,iloc(1:nloc),2) = 0.5_pr*SUM(ulc_prev(iloc(1:nloc),nvel(1):nvel(dim))**2,DIM=2)/ulc_prev(iloc(1:nloc),nden)**2*(d2u(1,iloc(1:nloc),1)-1.0_pr) !p_diag
                         d2u(3,iloc(1:nloc),2) = 0.5_pr/d2u(1,iloc(1:nloc),2)*d2u(1,iloc(1:nloc),1)/ulc_prev(iloc(1:nloc),nden) * &
                                                             (du(1,iloc(1:nloc),2)-pres_prev(iloc(1:nloc),1)/ulc_prev(iloc(1:nloc),nden) ) !c_diag
                         charI(1:nloc) = (du(2,iloc(1:nloc),2)-faceval*d2u(3,iloc(1:nloc),2))*(du(1,iloc(1:nloc),1)-dP0(faceint)-faceval*ulc_prev(iloc(1:nloc),nden)* &
                                                             d2u(1,iloc(1:nloc),2)*du(2,iloc(1:nloc),1)) + charV_prev(1:nloc)*(du_diag_PFB(iloc(1:nloc))*du(1,iloc(1:nloc),2) - &
                                                             faceval*du_diag_local(iloc(1:nloc),1)*ulc_prev(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)*du(2,iloc(1:nloc),2) - &
                                                             faceval*du(2,iloc(1:nloc),1)*(ulc_prev(iloc(1:nloc),nden)*d2u(3,iloc(1:nloc),2) + d2u(1,iloc(1:nloc),2)))
                         IF     (PLpinf==1) THEN
                            charI(1:nloc) = charI(1:nloc) - pBcoef*(1.0_pr-maxMa**2)*c0(faceint)*du(1,iloc(1:nloc),2)
                         ELSEIF (PLpinf==2) THEN
                            charI(1:nloc) = charI(1:nloc) - pBcoef*(1.0_pr-maxMa**2)*(d2u(3,iloc(1:nloc),2)*(pres_prev(iloc(1:nloc),1)-P0(faceint)) + d2u(1,iloc(1:nloc),2)*du(1,iloc(1:nloc),2))
                         ELSEIF (PLpinf==3) THEN
                            charI(1:nloc) = charI(1:nloc) - pBcoef*(1.0_pr-maxMa**2)*c0(faceint)*P0(faceint)/Tconst/ulc_prev(iloc(1:nloc),nden)/d2u(2,iloc(1:nloc),2) * du(1,iloc(1:nloc),2)
                         ELSEIF (PLpinf==4) THEN
                            charI(1:nloc) = charI(1:nloc) - pBcoef*(1.0_pr-maxMa**2) * ( d2u(3,iloc(1:nloc),2)*(pres_prev(iloc(1:nloc),1) - & 
                                                             ulc_prev(iloc(1:nloc),nden)*d2u(2,iloc(1:nloc),2)*Tconst) + d2u(1,iloc(1:nloc),2)* du(1,iloc(1:nloc),2) )
                         ELSEIF (PLpinf==5) THEN
                            charI(1:nloc) = charI(1:nloc) + faceval*pBcoef*(1.0_pr-maxMa**2)*gamm(faceint)*Pint*du(2,iloc(1:nloc),2)
                         ELSEIF (PLpinf==6) THEN
                            charI(1:nloc) = charI(1:nloc) + faceval*pBcoef*(1.0_pr-maxMa**2)*d2u(1,iloc(1:nloc),2) * &
                                                            (d2u(1,iloc(1:nloc),2)*pres_prev(iloc(1:nloc),2) + &
                                                             2.0_pr*ulc_prev(iloc(1:nloc),nden)*d2u(3,iloc(1:nloc),2)*pres_prev(iloc(1:nloc),2) + &
                                                             ulc_prev(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)*du(2,iloc(1:nloc),2) )
                         END IF
                         charX(1:nloc,1) = -0.5_pr/d2u(1,iloc(1:nloc),2)**2*charI(1:nloc) + d2u(3,iloc(1:nloc),2)/d2u(1,iloc(1:nloc),2)**3*charI_prev(1:nloc)
                         Drhs_diag_local((jj-1)*mynloc+iloc(1:nloc)) = Drhs_diag_local((jj-1)*mynloc+iloc(1:nloc)) - charX(1:nloc,1)
                      ELSE IF (jj==nvel(1)) THEN
                         du (2,iloc(1:nloc),2) = 1.0_pr/ulc_prev(iloc(1:nloc),nden) !u_diag
                         d2u(3,iloc(1:nloc),1) = 0.0_pr  !gamma_diag
                         du (1,iloc(1:nloc),2) = -ulc_prev(iloc(1:nloc),nvel(1))/ulc_prev(iloc(1:nloc),nden)*(d2u(1,iloc(1:nloc),1)-1.0_pr) !p_diag
                         d2u(3,iloc(1:nloc),2) = 0.5_pr/d2u(1,iloc(1:nloc),2)*d2u(1,iloc(1:nloc),1)/ulc_prev(iloc(1:nloc),nden)*du(1,iloc(1:nloc),2) !c_diag
                         charI(1:nloc) = (du(2,iloc(1:nloc),2)-faceval*d2u(3,iloc(1:nloc),2))* &
                                                            (du(1,iloc(1:nloc),1)-dP0(faceint)-faceval*ulc_prev(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)*du(2,iloc(1:nloc),1)) + &
                                         charV_prev(1:nloc)*(du_diag_PFB(iloc(1:nloc))*du(1,iloc(1:nloc),2) - &
                                                             faceval*du_diag_local(iloc(1:nloc),1)*ulc_prev(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)*du(2,iloc(1:nloc),2) - &
                                                             faceval*du(2,iloc(1:nloc),1)*ulc_prev(iloc(1:nloc),nden)*d2u(3,iloc(1:nloc),2)  )
                         IF     (PLpinf==1) THEN
                            charI(1:nloc) = charI(1:nloc) - pBcoef*(1.0_pr-maxMa**2)*c0(faceint)*du(1,iloc(1:nloc),2)
                         ELSEIF (PLpinf==2) THEN
                            charI(1:nloc) = charI(1:nloc) - pBcoef*(1.0_pr-maxMa**2)*(d2u(3,iloc(1:nloc),2)*(pres_prev(iloc(1:nloc),1) - P0(faceint)) + d2u(1,iloc(1:nloc),2)*du(1,iloc(1:nloc),2))
                         ELSEIF (PLpinf==3) THEN
                            charI(1:nloc) = charI(1:nloc) - pBcoef*(1.0_pr-maxMa**2)*c0(faceint)*P0(faceint)/Tconst/ulc_prev(iloc(1:nloc),nden)/d2u(2,iloc(1:nloc),2) * du(1,iloc(1:nloc),2)
                         ELSEIF (PLpinf==4) THEN
                            charI(1:nloc) = charI(1:nloc) - pBcoef*(1.0_pr-maxMa**2) * (d2u(3,iloc(1:nloc),2)*(pres_prev(iloc(1:nloc),1) - &
                                                             ulc_prev(iloc(1:nloc),nden)*d2u(2,iloc(1:nloc),2)*Tconst) + d2u(1,iloc(1:nloc),2)* du(1,iloc(1:nloc),2) )
                         ELSEIF (PLpinf==5) THEN
                            charI(1:nloc) = charI(1:nloc) + faceval*pBcoef*(1.0_pr-maxMa**2)*gamm(faceint)*Pint*du(2,iloc(1:nloc),2)
                         ELSEIF (PLpinf==6) THEN
                            charI(1:nloc) = charI(1:nloc) + faceval*pBcoef*(1.0_pr-maxMa**2)*d2u(1,iloc(1:nloc),2) * &
                                                            (2.0_pr*ulc_prev(iloc(1:nloc),nden)*d2u(3,iloc(1:nloc),2)*pres_prev(iloc(1:nloc),2) + &
                                                             ulc_prev(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)*du(2,iloc(1:nloc),2) )
                         END IF
                         charX(1:nloc,1) = -0.5_pr/d2u(1,iloc(1:nloc),2)**2*charI(1:nloc) + d2u(3,iloc(1:nloc),2)/d2u(1,iloc(1:nloc),2)**3*charI_prev(1:nloc)
                         charX(1:nloc,2) = faceval*0.5_pr/ulc_prev(iloc(1:nloc),nden)/d2u(1,iloc(1:nloc),2)*(charI(1:nloc) - charI_prev(1:nloc)*d2u(3,iloc(1:nloc),2)/d2u(1,iloc(1:nloc),2))
                         Drhs_diag_local((jj-1)*mynloc+iloc(1:nloc)) = Drhs_diag_local((jj-1)*mynloc+iloc(1:nloc)) - du(2,iloc(1:nloc),2)*charX_prev(1:nloc) - &
                                                        pres_prev(iloc(1:nloc),2)*charX(1:nloc,1) - ulc_prev(iloc(1:nloc),nden)*charX(1:nloc,2)
                      ELSE IF (jj>=nvel(2) .AND. jj<=nvel(dim)) THEN
                         du (2,iloc(1:nloc),2) = 0.0_pr !u_diag
                         d2u(3,iloc(1:nloc),1) = 0.0_pr  !gamma_diag
                         du (1,iloc(1:nloc),2) = -ulc_prev(iloc(1:nloc),jj)/ulc_prev(iloc(1:nloc),nden)*(d2u(1,iloc(1:nloc),1)-1.0_pr) !p_diag
                         d2u(3,iloc(1:nloc),2) = 0.5_pr/d2u(1,iloc(1:nloc),2)*d2u(1,iloc(1:nloc),1)/ulc_prev(iloc(1:nloc),nden)*du(1,iloc(1:nloc),2) !c_diag
                         charI(1:nloc) = -faceval*d2u(3,iloc(1:nloc),2)*(du(1,iloc(1:nloc),1)-dP0(faceint)-faceval*ulc_prev(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)*du(2,iloc(1:nloc),1)) + &
                                         charV_prev(1:nloc)*(du_diag_PFB(iloc(1:nloc))*du(1,iloc(1:nloc),2) - faceval*du(2,iloc(1:nloc),1)*ulc_prev(iloc(1:nloc),nden)*d2u(3,iloc(1:nloc),2))
                         IF     (PLpinf==1) THEN
                            charI(1:nloc) = charI(1:nloc) - pBcoef*(1.0_pr-maxMa**2)*c0(faceint)*du(1,iloc(1:nloc),2)
                         ELSEIF (PLpinf==2) THEN
                            charI(1:nloc) = charI(1:nloc) - pBcoef*(1.0_pr-maxMa**2)*(d2u(3,iloc(1:nloc),2)*(pres_prev(iloc(1:nloc),1) - P0(faceint)) + d2u(1,iloc(1:nloc),2)*du(1,iloc(1:nloc),2))
                         ELSEIF (PLpinf==3) THEN
                            charI(1:nloc) = charI(1:nloc) - pBcoef*(1.0_pr-maxMa**2)*c0(faceint)*P0(faceint)/Tconst/ulc_prev(iloc(1:nloc),nden)/d2u(2,iloc(1:nloc),2) * du(1,iloc(1:nloc),2)
                         ELSEIF (PLpinf==4) THEN
                            charI(1:nloc) = charI(1:nloc) - pBcoef*(1.0_pr-maxMa**2) * (d2u(3,iloc(1:nloc),2)*(pres_prev(iloc(1:nloc),1) - &
                                                             ulc_prev(iloc(1:nloc),nden)*d2u(2,iloc(1:nloc),2)*Tconst) + d2u(1,iloc(1:nloc),2)* du(1,iloc(1:nloc),2) )
                         ELSEIF (PLpinf==6) THEN
                            charI(1:nloc) = charI(1:nloc) + faceval*pBcoef*(1.0_pr-maxMa**2)*d2u(1,iloc(1:nloc),2) * &
                                                            (2.0_pr*ulc_prev(iloc(1:nloc),nden)*d2u(3,iloc(1:nloc),2)*pres_prev(iloc(1:nloc),2))
                         END IF
                         charX(1:nloc,1) = -0.5_pr/d2u(1,iloc(1:nloc),2)**2*charI(1:nloc) + d2u(3,iloc(1:nloc),2)/d2u(1,iloc(1:nloc),2)**3*charI_prev(1:nloc)
                         Drhs_diag_local((jj-1)*mynloc+iloc(1:nloc)) = Drhs_diag_local((jj-1)*mynloc+iloc(1:nloc)) - (charX_prev(1:nloc)+ulc_prev(iloc(1:nloc),jj)*charX(1:nloc,1)) / &
                                                        ulc_prev(iloc(1:nloc),nden)
                      ELSE IF (jj==neng) THEN
                         du (2,iloc(1:nloc),2) = 0.0_pr !u_diag
                         d2u(3,iloc(1:nloc),1) = 0.0_pr  !gamma_diag
                         du (1,iloc(1:nloc),2) = d2u(1,iloc(1:nloc),1)-1.0_pr !p_diag
                         d2u(3,iloc(1:nloc),2) = 0.5_pr/d2u(1,iloc(1:nloc),2)*d2u(1,iloc(1:nloc),1)/ulc_prev(iloc(1:nloc),nden)*du(1,iloc(1:nloc),2) !c_diag
                         charI(1:nloc) = -faceval*d2u(3,iloc(1:nloc),2)*(du(1,iloc(1:nloc),1)-dP0(faceint)-faceval*ulc_prev(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)*du(2,iloc(1:nloc),1)) + &
                                         charV_prev(1:nloc)*(du_diag_PFB(iloc(1:nloc))*du(1,iloc(1:nloc),2) - faceval*du(2,iloc(1:nloc),1)*ulc_prev(iloc(1:nloc),nden)*d2u(3,iloc(1:nloc),2))
                         IF     (PLpinf==1) THEN
                            charI(1:nloc) = charI(1:nloc) - pBcoef*(1.0_pr-maxMa**2)*c0(faceint)*du(1,iloc(1:nloc),2)
                         ELSEIF (PLpinf==2) THEN
                            charI(1:nloc) = charI(1:nloc) - pBcoef*(1.0_pr-maxMa**2)*(d2u(3,iloc(1:nloc),2)*(pres_prev(iloc(1:nloc),1) - P0(faceint)) + d2u(1,iloc(1:nloc),2)*du(1,iloc(1:nloc),2))
                         ELSEIF (PLpinf==3) THEN
                            charI(1:nloc) = charI(1:nloc) - pBcoef*(1.0_pr-maxMa**2)*c0(faceint)*P0(faceint)/Tconst/ulc_prev(iloc(1:nloc),nden)/d2u(2,iloc(1:nloc),2) * du(1,iloc(1:nloc),2)
                         ELSEIF (PLpinf==4) THEN
                            charI(1:nloc) = charI(1:nloc) - pBcoef*(1.0_pr-maxMa**2) * (d2u(3,iloc(1:nloc),2)*(pres_prev(iloc(1:nloc),1) - &
                                                             ulc_prev(iloc(1:nloc),nden)*d2u(2,iloc(1:nloc),2)*Tconst) + d2u(1,iloc(1:nloc),2)* du(1,iloc(1:nloc),2) )
                         ELSEIF (PLpinf==6) THEN
                            charI(1:nloc) = charI(1:nloc) + faceval*pBcoef*(1.0_pr-maxMa**2)*d2u(1,iloc(1:nloc),2) * &
                                                            (2.0_pr*ulc_prev(iloc(1:nloc),nden)*d2u(3,iloc(1:nloc),2)*pres_prev(iloc(1:nloc),2))
                         END IF
                         charX(1:nloc,1) = -0.5_pr/d2u(1,iloc(1:nloc),2)**2*charI(1:nloc) + d2u(3,iloc(1:nloc),2)/d2u(1,iloc(1:nloc),2)**3*charI_prev(1:nloc)
                         charX(1:nloc,2) = faceval*0.5_pr/ulc_prev(iloc(1:nloc),nden)/d2u(1,iloc(1:nloc),2)*(charI(1:nloc) + charI_prev(1:nloc)*d2u(3,iloc(1:nloc),2)/d2u(1,iloc(1:nloc),2))
                         charX(1:nloc,3) = -0.5_pr*charI(1:nloc)
                         Drhs_diag_local((jj-1)*mynloc+iloc(1:nloc)) = Drhs_diag_local((jj-1)*mynloc+iloc(1:nloc)) - 0.5_pr*SUM(ulc_prev(iloc(1:nloc),nvel(1):nvel(dim))**2,DIM=2) / &
                                           ulc_prev(iloc(1:nloc),nden)**2*charX(1:nloc,1) - ulc_prev(iloc(1:nloc),nvel(1))*charX(1:nloc,2) - &
                                           1.0_pr/(d2u(1,iloc(1:nloc),1)-1.0_pr)*charX(1:nloc,3)
                      ELSE IF (jj>=nspc(1) .AND. jj<=nspc(Nspecm)) THEN
                         du (2,iloc(1:nloc),2) = 0.0_pr !u_diag
                         d2u(3,iloc(1:nloc),1) = (d2u(2,iloc(1:nloc),1)/MW_in(jj-nspc(1)+1) - d2u(2,iloc(1:nloc),2)*cp_in(jj-nspc(1)+1)) / &
                                                       ulc_prev(iloc(1:nloc),nden)/(d2u(2,iloc(1:nloc),1)-d2u(2,iloc(1:nloc),2))**2  !gamma_diag
                         du (1,iloc(1:nloc),2) = (ulc_prev(iloc(1:nloc),neng) - 0.5_pr*SUM(ulc_prev(iloc(1:nloc),nvel(1):nvel(dim))**2,DIM=2) / &
                                                       ulc_prev(iloc(1:nloc),nden))*d2u(3,iloc(1:nloc),1) !p_diag
                         d2u(3,iloc(1:nloc),2) = 0.5_pr/d2u(1,iloc(1:nloc),2)/ulc_prev(iloc(1:nloc),nden) * &
                                                       (d2u(3,iloc(1:nloc),1)*pres_prev(iloc(1:nloc),1)+d2u(1,iloc(1:nloc),1)*du(1,iloc(1:nloc),2)) !c_diag
                         charI(1:nloc) = -faceval*d2u(3,iloc(1:nloc),2)*(du(1,iloc(1:nloc),1)-dP0(faceint)-faceval*ulc_prev(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)*du(2,iloc(1:nloc),1)) + &
                                         charV_prev(1:nloc)*(du_diag_PFB(iloc(1:nloc))*du(1,iloc(1:nloc),2) - faceval*du(2,iloc(1:nloc),1)*ulc_prev(iloc(1:nloc),nden)*d2u(3,iloc(1:nloc),2))
                         IF     (PLpinf==1) THEN
                            charI(1:nloc) = charI(1:nloc) - pBcoef*(1.0_pr-maxMa**2)*c0(faceint)*du(1,iloc(1:nloc),2)
                         ELSEIF (PLpinf==2) THEN
                            charI(1:nloc) = charI(1:nloc) - pBcoef*(1.0_pr-maxMa**2)*(d2u(3,iloc(1:nloc),2)*(pres_prev(iloc(1:nloc),1) - P0(faceint)) + d2u(1,iloc(1:nloc),2)*du(1,iloc(1:nloc),2))
                         ELSEIF (PLpinf==3) THEN
                            charI(1:nloc) = charI(1:nloc) - pBcoef*(1.0_pr-maxMa**2)*c0(faceint)*P0(faceint)/Tconst/ulc_prev(iloc(1:nloc),nden)/d2u(2,iloc(1:nloc),2) * &
                                                            (du(1,iloc(1:nloc),2) - pres_prev(iloc(1:nloc),1)/MW_in(faceint)/ulc_prev(iloc(1:nloc),nden)/d2u(2,iloc(1:nloc),2) ) 
                         ELSEIF (PLpinf==4) THEN
                            charI(1:nloc) = charI(1:nloc) - pBcoef*(1.0_pr-maxMa**2) * (d2u(3,iloc(1:nloc),2)*(pres_prev(iloc(1:nloc),1) - &
                                                             ulc_prev(iloc(1:nloc),nden)*d2u(2,iloc(1:nloc),2)*Tconst) + d2u(1,iloc(1:nloc),2)*(du(1,iloc(1:nloc),2) - Tconst/MW_in(faceint)) )
                         ELSEIF (PLpinf==6) THEN
                            charI(1:nloc) = charI(1:nloc) + faceval*pBcoef*(1.0_pr-maxMa**2)*d2u(1,iloc(1:nloc),2) * &
                                                            (2.0_pr*ulc_prev(iloc(1:nloc),nden)*d2u(3,iloc(1:nloc),2)*pres_prev(iloc(1:nloc),2))
                         END IF
                         charX(1:nloc,1) = -0.5_pr/d2u(1,iloc(1:nloc),2)**2*charI(1:nloc) + d2u(3,iloc(1:nloc),2)/d2u(1,iloc(1:nloc),2)**3*charI_prev(1:nloc) 
                         Drhs_diag_local((jj-1)*mynloc+iloc(1:nloc)) = Drhs_diag_local((jj-1)*mynloc+iloc(1:nloc)) - (charX_prev(1:nloc)+ulc_prev(iloc(1:nloc),jj)*charX(1:nloc,1)) / &
                                                        ulc_prev(iloc(1:nloc),nden)
                      END IF 
                   END DO
                ELSE IF (chartype == 4) THEN
                   charI_prev(1:nloc) = 2.0_pr*(pres_prev(iloc(1:nloc),2)*(du(1,iloc(1:nloc),1)-dP0(faceint)) + &
                                                ulc_prev(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)**2*du(2,iloc(1:nloc),1))
                   charX_prev(1:nloc) = -0.5_pr/d2u(1,iloc(1:nloc),2)**2*charI_prev(1:nloc)
                   DO jj=1,myne_loc
                      IF (jj==nden) THEN
                         du (2,iloc(1:nloc),2) = -ulc_prev(iloc(1:nloc),nvel(1))/ulc_prev(iloc(1:nloc),nden)**2 !u_diag
                         d2u(3,iloc(1:nloc),1) = 0.0_pr  !gamma_diag
                         du (1,iloc(1:nloc),2) = 0.5_pr*SUM(ulc_prev(iloc(1:nloc),nvel(1):nvel(dim))**2,DIM=2)/ulc_prev(iloc(1:nloc),nden)**2*(d2u(1,iloc(1:nloc),1)-1.0_pr) !p_diag
                         d2u(3,iloc(1:nloc),2) = 0.5_pr/d2u(1,iloc(1:nloc),2)*d2u(1,iloc(1:nloc),1)/ulc_prev(iloc(1:nloc),nden) * &
                                                             (du(1,iloc(1:nloc),2)-pres_prev(iloc(1:nloc),1)/ulc_prev(iloc(1:nloc),nden) ) !c_diag
                         charI(1:nloc) = 2.0_pr*(du(2,iloc(1:nloc),2)*(du(1,iloc(1:nloc),1)-dP0(faceint)) + pres_prev(iloc(1:nloc),2)*du_diag_PFB(iloc(1:nloc))*du(1,iloc(1:nloc),2) + &
                                                 d2u(1,iloc(1:nloc),2)*(ulc_prev(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)*du_diag_local(iloc(1:nloc),1)*du(2,iloc(1:nloc),2) + &
                                                 du(2,iloc(1:nloc),1)*(d2u(1,iloc(1:nloc),2) + 2.0_pr*ulc_prev(iloc(1:nloc),nden)*d2u(3,iloc(1:nloc),2))))  ! I_1 + I_N
                         charX(1:nloc,1) = -0.5_pr/d2u(1,iloc(1:nloc),2)**2*charI(1:nloc) + d2u(3,iloc(1:nloc),2)/d2u(1,iloc(1:nloc),2)**3*charI_prev(1:nloc)
                         Drhs_diag_local((jj-1)*mynloc+iloc(1:nloc)) = Drhs_diag_local((jj-1)*mynloc+iloc(1:nloc)) - charX(1:nloc,1)
                      ELSE IF (jj==nvel(1)) THEN
                         du (2,iloc(1:nloc),2) = 1.0_pr/ulc_prev(iloc(1:nloc),nden) !u_diag
                         d2u(3,iloc(1:nloc),1) = 0.0_pr  !gamma_diag
                         du (1,iloc(1:nloc),2) = -ulc_prev(iloc(1:nloc),nvel(1))/ulc_prev(iloc(1:nloc),nden)*(d2u(1,iloc(1:nloc),1)-1.0_pr) !p_diag
                         d2u(3,iloc(1:nloc),2) = 0.5_pr/d2u(1,iloc(1:nloc),2)*d2u(1,iloc(1:nloc),1)/ulc_prev(iloc(1:nloc),nden)*du(1,iloc(1:nloc),2) !c_diag
                         charI(1:nloc) = 2.0_pr*(du(2,iloc(1:nloc),2)*(du(1,iloc(1:nloc),1)-dP0(faceint)) + pres_prev(iloc(1:nloc),2)*du_diag_PFB(iloc(1:nloc))*du(1,iloc(1:nloc),2) + &
                                                 d2u(1,iloc(1:nloc),2)*(ulc_prev(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)*du_diag_local(iloc(1:nloc),1)*du(2,iloc(1:nloc),2) + &
                                                 du(2,iloc(1:nloc),1)*2.0_pr*ulc_prev(iloc(1:nloc),nden)*d2u(3,iloc(1:nloc),2)))  ! I_1 + I_N
                         charX(1:nloc,1) = -0.5_pr/d2u(1,iloc(1:nloc),2)**2*charI(1:nloc) + d2u(3,iloc(1:nloc),2)/d2u(1,iloc(1:nloc),2)**3*charI_prev(1:nloc)
                         charX(1:nloc,2) = faceval*0.5_pr/ulc_prev(iloc(1:nloc),nden)/d2u(1,iloc(1:nloc),2)*(charI(1:nloc) - charI_prev(1:nloc)*d2u(3,iloc(1:nloc),2)/d2u(1,iloc(1:nloc),2))
                         Drhs_diag_local((jj-1)*mynloc+iloc(1:nloc)) = Drhs_diag_local((jj-1)*mynloc+iloc(1:nloc)) - du(2,iloc(1:nloc),2)*charX_prev(1:nloc) - &
                                                        pres_prev(iloc(1:nloc),2)*charX(1:nloc,1) - ulc_prev(iloc(1:nloc),nden)*charX(1:nloc,2)
                      ELSE IF (jj>=nvel(2) .AND. jj<=nvel(dim)) THEN
                         du (2,iloc(1:nloc),2) = 0.0_pr !u_diag
                         d2u(3,iloc(1:nloc),1) = 0.0_pr  !gamma_diag
                         du (1,iloc(1:nloc),2) = -ulc_prev(iloc(1:nloc),jj)/ulc_prev(iloc(1:nloc),nden)*(d2u(1,iloc(1:nloc),1)-1.0_pr) !p_diag
                         d2u(3,iloc(1:nloc),2) = 0.5_pr/d2u(1,iloc(1:nloc),2)*d2u(1,iloc(1:nloc),1)/ulc_prev(iloc(1:nloc),nden)*du(1,iloc(1:nloc),2) !c_diag
                         charI(1:nloc) = 2.0_pr*(du(2,iloc(1:nloc),2)*(du(1,iloc(1:nloc),1)-dP0(faceint)) + pres_prev(iloc(1:nloc),2)*du_diag_PFB(iloc(1:nloc))*du(1,iloc(1:nloc),2) + &
                                                 d2u(1,iloc(1:nloc),2)*(ulc_prev(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)*du_diag_local(iloc(1:nloc),1)*du(2,iloc(1:nloc),2) + &
                                                 du(2,iloc(1:nloc),1)*2.0_pr*ulc_prev(iloc(1:nloc),nden)*d2u(3,iloc(1:nloc),2)))  ! I_1 + I_N
                         charX(1:nloc,1) = -0.5_pr/d2u(1,iloc(1:nloc),2)**2*charI(1:nloc) + d2u(3,iloc(1:nloc),2)/d2u(1,iloc(1:nloc),2)**3*charI_prev(1:nloc)
                         Drhs_diag_local((jj-1)*mynloc+iloc(1:nloc)) = Drhs_diag_local((jj-1)*mynloc+iloc(1:nloc)) - (charX_prev(1:nloc)+ulc_prev(iloc(1:nloc),jj)*charX(1:nloc,1)) / &
                                                        ulc_prev(iloc(1:nloc),nden)
                      ELSE IF (jj==neng) THEN
                         du (2,iloc(1:nloc),2) = 0.0_pr !u_diag
                         d2u(3,iloc(1:nloc),1) = 0.0_pr  !gamma_diag
                         du (1,iloc(1:nloc),2) = d2u(1,iloc(1:nloc),1)-1.0_pr !p_diag
                         d2u(3,iloc(1:nloc),2) = 0.5_pr/d2u(1,iloc(1:nloc),2)*d2u(1,iloc(1:nloc),1)/ulc_prev(iloc(1:nloc),nden)*du(1,iloc(1:nloc),2) !c_diag
                         charI(1:nloc) = 2.0_pr*(du(2,iloc(1:nloc),2)*(du(1,iloc(1:nloc),1)-dP0(faceint)) + pres_prev(iloc(1:nloc),2)*du_diag_PFB(iloc(1:nloc))*du(1,iloc(1:nloc),2) + &
                                                 d2u(1,iloc(1:nloc),2)*(ulc_prev(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)*du_diag_local(iloc(1:nloc),1)*du(2,iloc(1:nloc),2) + &
                                                 du(2,iloc(1:nloc),1)*2.0_pr*ulc_prev(iloc(1:nloc),nden)*d2u(3,iloc(1:nloc),2)))  ! I_1 + I_N
                         charX(1:nloc,1) = -0.5_pr/d2u(1,iloc(1:nloc),2)**2*charI(1:nloc) + d2u(3,iloc(1:nloc),2)/d2u(1,iloc(1:nloc),2)**3*charI_prev(1:nloc)
                         charX(1:nloc,2) = faceval*0.5_pr/ulc_prev(iloc(1:nloc),nden)/d2u(1,iloc(1:nloc),2)*(charI(1:nloc) + charI_prev(1:nloc)*d2u(3,iloc(1:nloc),2)/d2u(1,iloc(1:nloc),2))
                         charX(1:nloc,3) = -0.5_pr*charI(1:nloc)
                         Drhs_diag_local((jj-1)*mynloc+iloc(1:nloc)) = Drhs_diag_local((jj-1)*mynloc+iloc(1:nloc)) - 0.5_pr*SUM(ulc_prev(iloc(1:nloc),nvel(1):nvel(dim))**2,DIM=2) / &
                                           ulc_prev(iloc(1:nloc),nden)**2*charX(1:nloc,1) - ulc_prev(iloc(1:nloc),nvel(1))*charX(1:nloc,2) - &
                                           1.0_pr/(d2u(1,iloc(1:nloc),1)-1.0_pr)*charX(1:nloc,3)
                      ELSE IF (jj>=nspc(1) .AND. jj<=nspc(Nspecm)) THEN
                         du (2,iloc(1:nloc),2) = 0.0_pr !u_diag
                         d2u(3,iloc(1:nloc),1) = (d2u(2,iloc(1:nloc),1)/MW_in(jj-nspc(1)+1) - d2u(2,iloc(1:nloc),2)*cp_in(jj-nspc(1)+1)) / &
                                                       ulc_prev(iloc(1:nloc),nden)/(d2u(2,iloc(1:nloc),1)-d2u(2,iloc(1:nloc),2))**2  !gamma_diag
                         du (1,iloc(1:nloc),2) = (ulc_prev(iloc(1:nloc),neng) - 0.5_pr*SUM(ulc_prev(iloc(1:nloc),nvel(1):nvel(dim))**2,DIM=2) / &
                                                       ulc_prev(iloc(1:nloc),nden))*d2u(3,iloc(1:nloc),1) !p_diag
                         d2u(3,iloc(1:nloc),2) = 0.5_pr/d2u(1,iloc(1:nloc),2)/ulc_prev(iloc(1:nloc),nden) * &
                                                       (d2u(3,iloc(1:nloc),1)*pres_prev(iloc(1:nloc),1)+d2u(1,iloc(1:nloc),1)*du(1,iloc(1:nloc),2)) !c_diag
                         charI(1:nloc) = 2.0_pr*(du(2,iloc(1:nloc),2)*(du(1,iloc(1:nloc),1)-dP0(faceint)) + pres_prev(iloc(1:nloc),2)*du_diag_PFB(iloc(1:nloc))*du(1,iloc(1:nloc),2) + &
                                                 d2u(1,iloc(1:nloc),2)*(ulc_prev(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)*du_diag_local(iloc(1:nloc),1)*du(2,iloc(1:nloc),2) + &
                                                 du(2,iloc(1:nloc),1)*2.0_pr*ulc_prev(iloc(1:nloc),nden)*d2u(3,iloc(1:nloc),2)))  ! I_1 + I_N
                         charX(1:nloc,1) = -0.5_pr/d2u(1,iloc(1:nloc),2)**2*charI(1:nloc) + d2u(3,iloc(1:nloc),2)/d2u(1,iloc(1:nloc),2)**3*charI_prev(1:nloc) 
                         Drhs_diag_local((jj-1)*mynloc+iloc(1:nloc)) = Drhs_diag_local((jj-1)*mynloc+iloc(1:nloc)) - (charX_prev(1:nloc)+ulc_prev(iloc(1:nloc),jj)*charX(1:nloc,1)) / &
                                                        ulc_prev(iloc(1:nloc),nden)
                      END IF 
                   END DO
                ELSE
                   charI_prev(1:nloc) = -2.0_pr*(d2u(1,iloc(1:nloc),2)*(du(1,iloc(1:nloc),1)-dP0(faceint)) + &
                                                 ulc_prev(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)*pres_prev(iloc(1:nloc),2)*du(2,iloc(1:nloc),1))
                   charX_prev(1:nloc) = -faceval*0.5_pr/d2u(1,iloc(1:nloc),2)**2*charI_prev(1:nloc)
                   DO jj=1,myne_loc
                      IF (jj==nden) THEN
                         du (2,iloc(1:nloc),2) = -ulc_prev(iloc(1:nloc),nvel(1))/ulc_prev(iloc(1:nloc),nden)**2 !u_diag
                         d2u(3,iloc(1:nloc),1) = 0.0_pr  !gamma_diag
                         du (1,iloc(1:nloc),2) = 0.5_pr*SUM(ulc_prev(iloc(1:nloc),nvel(1):nvel(dim))**2,DIM=2)/ulc_prev(iloc(1:nloc),nden)**2*(d2u(1,iloc(1:nloc),1)-1.0_pr) !p_diag
                         d2u(3,iloc(1:nloc),2) = 0.5_pr/d2u(1,iloc(1:nloc),2)*d2u(1,iloc(1:nloc),1)/ulc_prev(iloc(1:nloc),nden) * &
                                                             (du(1,iloc(1:nloc),2)-pres_prev(iloc(1:nloc),1)/ulc_prev(iloc(1:nloc),nden) ) !c_diag
                         charI(1:nloc) = -2.0_pr*(d2u(3,iloc(1:nloc),2)*(du(1,iloc(1:nloc),1)-dP0(faceint)) + d2u(1,iloc(1:nloc),2)*du_diag_PFB(iloc(1:nloc))*du(1,iloc(1:nloc),2) + &
                                                 ulc_prev(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)*pres_prev(iloc(1:nloc),2)*du_diag_local(iloc(1:nloc),1)*du(2,iloc(1:nloc),2) + &
                                                 du(2,iloc(1:nloc),1)*(d2u(1,iloc(1:nloc),2)*pres_prev(iloc(1:nloc),2) + &
                                                 ulc_prev(iloc(1:nloc),nden)*(d2u(3,iloc(1:nloc),2)*pres_prev(iloc(1:nloc),2) + d2u(1,iloc(1:nloc),2)*du (2,iloc(1:nloc),2))))  ! I_1 - I_N
                         charX(1:nloc,1) = -faceval*0.5_pr/d2u(1,iloc(1:nloc),2)**2*charI(1:nloc) + d2u(3,iloc(1:nloc),2)/d2u(1,iloc(1:nloc),2)**3*charI_prev(1:nloc)
                         IF (chartype == 2) charX(1:nloc,1) = charX(1:nloc,1) - du(2,iloc(1:nloc),2)*(du(dim+2,iloc(1:nloc),1)-drho0(faceint) - & 
                                                                                 (du(1,iloc(1:nloc),1)-dP0(faceint))/d2u(1,iloc(1:nloc),2)**2) - &
                                                                                pres_prev(iloc(1:nloc),2)*(du_diag_local(iloc(1:nloc),1) - &
                                                                                 du_diag_PFB(iloc(1:nloc))*du(1,iloc(1:nloc),2)/d2u(1,iloc(1:nloc),2)**2 + &
                                                                                 2.0_pr*d2u(3,iloc(1:nloc),2)/d2u(1,iloc(1:nloc),2)**3*(du(1,iloc(1:nloc),1)-dP0(faceint)))
                         Drhs_diag_local((jj-1)*mynloc+iloc(1:nloc)) = Drhs_diag_local((jj-1)*mynloc+iloc(1:nloc)) - charX(1:nloc,1)
                      ELSE IF (jj==nvel(1)) THEN
                         du (2,iloc(1:nloc),2) = 1.0_pr/ulc_prev(iloc(1:nloc),nden) !u_diag
                         d2u(3,iloc(1:nloc),1) = 0.0_pr  !gamma_diag
                         du (1,iloc(1:nloc),2) = -ulc_prev(iloc(1:nloc),nvel(1))/ulc_prev(iloc(1:nloc),nden)*(d2u(1,iloc(1:nloc),1)-1.0_pr) !p_diag
                         d2u(3,iloc(1:nloc),2) = 0.5_pr/d2u(1,iloc(1:nloc),2)*d2u(1,iloc(1:nloc),1)/ulc_prev(iloc(1:nloc),nden)*du(1,iloc(1:nloc),2) !c_diag
                         charI(1:nloc) = -2.0_pr*(d2u(3,iloc(1:nloc),2)*(du(1,iloc(1:nloc),1)-dP0(faceint)) + d2u(1,iloc(1:nloc),2)*du_diag_PFB(iloc(1:nloc))*du(1,iloc(1:nloc),2) + &
                                                 ulc_prev(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)*pres_prev(iloc(1:nloc),2)*du_diag_local(iloc(1:nloc),1)*du(2,iloc(1:nloc),2) + &
                                                 du(2,iloc(1:nloc),1)*(ulc_prev(iloc(1:nloc),nden) * &
                                                 (d2u(3,iloc(1:nloc),2)*pres_prev(iloc(1:nloc),2) + d2u(1,iloc(1:nloc),2)*du (2,iloc(1:nloc),2))))  ! I_1 - I_N
                         charX(1:nloc,1) = -faceval*0.5_pr/d2u(1,iloc(1:nloc),2)**2*charI(1:nloc) + d2u(3,iloc(1:nloc),2)/d2u(1,iloc(1:nloc),2)**3*charI_prev(1:nloc)
                         charX(1:nloc,2) = 0.5_pr/ulc_prev(iloc(1:nloc),nden)/d2u(1,iloc(1:nloc),2)*(charI(1:nloc) - charI_prev(1:nloc)*d2u(3,iloc(1:nloc),2)/d2u(1,iloc(1:nloc),2))
                         IF (chartype == 2) charX(1:nloc,1) = charX(1:nloc,1) - du(2,iloc(1:nloc),2)*(du(dim+2,iloc(1:nloc),1)-drho0(faceint) - & 
                                                                                 (du(1,iloc(1:nloc),1)-dP0(faceint))/d2u(1,iloc(1:nloc),2)**2) + &
                                                                                pres_prev(iloc(1:nloc),2)*(du_diag_PFB(iloc(1:nloc))*du(1,iloc(1:nloc),2)/d2u(1,iloc(1:nloc),2)**2 - &
                                                                                 2.0_pr*d2u(3,iloc(1:nloc),2)/d2u(1,iloc(1:nloc),2)**3*(du(1,iloc(1:nloc),1)-dP0(faceint)))
                         Drhs_diag_local((jj-1)*mynloc+iloc(1:nloc)) = Drhs_diag_local((jj-1)*mynloc+iloc(1:nloc)) - du(2,iloc(1:nloc),2)*charX_prev(1:nloc) - &
                                                        pres_prev(iloc(1:nloc),2)*charX(1:nloc,1) - ulc_prev(iloc(1:nloc),nden)*charX(1:nloc,2)
                      ELSE IF (jj>=nvel(2) .AND. jj<=nvel(dim)) THEN
                         du (2,iloc(1:nloc),2) = 0.0_pr !u_diag
                         d2u(3,iloc(1:nloc),1) = 0.0_pr  !gamma_diag
                         du (1,iloc(1:nloc),2) = -ulc_prev(iloc(1:nloc),jj)/ulc_prev(iloc(1:nloc),nden)*(d2u(1,iloc(1:nloc),1)-1.0_pr) !p_diag
                         d2u(3,iloc(1:nloc),2) = 0.5_pr/d2u(1,iloc(1:nloc),2)*d2u(1,iloc(1:nloc),1)/ulc_prev(iloc(1:nloc),nden)*du(1,iloc(1:nloc),2) !c_diag
                         charI(1:nloc) = -2.0_pr*(d2u(3,iloc(1:nloc),2)*(du(1,iloc(1:nloc),1)-dP0(faceint)) + d2u(1,iloc(1:nloc),2)*du_diag_PFB(iloc(1:nloc))*du(1,iloc(1:nloc),2) + &
                                                 ulc_prev(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)*pres_prev(iloc(1:nloc),2)*du_diag_local(iloc(1:nloc),1)*du(2,iloc(1:nloc),2) + &
                                                 du(2,iloc(1:nloc),1)*(ulc_prev(iloc(1:nloc),nden) * &
                                                 (d2u(3,iloc(1:nloc),2)*pres_prev(iloc(1:nloc),2) + d2u(1,iloc(1:nloc),2)*du (2,iloc(1:nloc),2))))  ! I_1 - I_N
                         charX(1:nloc,1) = -faceval*0.5_pr/d2u(1,iloc(1:nloc),2)**2*charI(1:nloc) + d2u(3,iloc(1:nloc),2)/d2u(1,iloc(1:nloc),2)**3*charI_prev(1:nloc)
                         IF (chartype == 2) charX(1:nloc,1) = charX(1:nloc,1) - du(2,iloc(1:nloc),2)*(du(dim+2,iloc(1:nloc),1)-drho0(faceint) - & 
                                                                                 (du(1,iloc(1:nloc),1)-dP0(faceint))/d2u(1,iloc(1:nloc),2)**2) + &
                                                                                pres_prev(iloc(1:nloc),2)*(du_diag_PFB(iloc(1:nloc))*du(1,iloc(1:nloc),2)/d2u(1,iloc(1:nloc),2)**2 - &
                                                                                 2.0_pr*d2u(3,iloc(1:nloc),2)/d2u(1,iloc(1:nloc),2)**3*(du(1,iloc(1:nloc),1)-dP0(faceint)))
                         Drhs_diag_local((jj-1)*mynloc+iloc(1:nloc)) = Drhs_diag_local((jj-1)*mynloc+iloc(1:nloc)) - (charX_prev(1:nloc)+ulc_prev(iloc(1:nloc),jj)*charX(1:nloc,1)) / &
                                                        ulc_prev(iloc(1:nloc),nden)
                      ELSE IF (jj==neng) THEN
                         du (2,iloc(1:nloc),2) = 0.0_pr !u_diag
                         d2u(3,iloc(1:nloc),1) = 0.0_pr  !gamma_diag
                         du (1,iloc(1:nloc),2) = d2u(1,iloc(1:nloc),1)-1.0_pr !p_diag
                         d2u(3,iloc(1:nloc),2) = 0.5_pr/d2u(1,iloc(1:nloc),2)*d2u(1,iloc(1:nloc),1)/ulc_prev(iloc(1:nloc),nden)*du(1,iloc(1:nloc),2) !c_diag
                         charI(1:nloc) = -2.0_pr*(d2u(3,iloc(1:nloc),2)*(du(1,iloc(1:nloc),1)-dP0(faceint)) + d2u(1,iloc(1:nloc),2)*du_diag_PFB(iloc(1:nloc))*du(1,iloc(1:nloc),2) + &
                                                 ulc_prev(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)*pres_prev(iloc(1:nloc),2)*du_diag_local(iloc(1:nloc),1)*du(2,iloc(1:nloc),2) + &
                                                 du(2,iloc(1:nloc),1)*(ulc_prev(iloc(1:nloc),nden) * &
                                                 (d2u(3,iloc(1:nloc),2)*pres_prev(iloc(1:nloc),2) + d2u(1,iloc(1:nloc),2)*du (2,iloc(1:nloc),2))))  ! I_1 - I_N
                         charX(1:nloc,1) = -faceval*0.5_pr/d2u(1,iloc(1:nloc),2)**2*charI(1:nloc) + d2u(3,iloc(1:nloc),2)/d2u(1,iloc(1:nloc),2)**3*charI_prev(1:nloc)
                         charX(1:nloc,2) = 0.5_pr/ulc_prev(iloc(1:nloc),nden)/d2u(1,iloc(1:nloc),2)*(charI(1:nloc) + charI_prev(1:nloc)*d2u(3,iloc(1:nloc),2)/d2u(1,iloc(1:nloc),2))
                         charX(1:nloc,3) = -faceval*0.5_pr*charI(1:nloc)
                         IF (chartype == 2) charX(1:nloc,1) = charX(1:nloc,1) - du(2,iloc(1:nloc),2)*(du(dim+2,iloc(1:nloc),1)-drho0(faceint) - & 
                                                                                 (du(1,iloc(1:nloc),1)-dP0(faceint))/d2u(1,iloc(1:nloc),2)**2) + &
                                                                                pres_prev(iloc(1:nloc),2)*(du_diag_PFB(iloc(1:nloc))*du(1,iloc(1:nloc),2)/d2u(1,iloc(1:nloc),2)**2 - &
                                                                                 2.0_pr*d2u(3,iloc(1:nloc),2)/d2u(1,iloc(1:nloc),2)**3*(du(1,iloc(1:nloc),1)-dP0(faceint)))
                         Drhs_diag_local((jj-1)*mynloc+iloc(1:nloc)) = Drhs_diag_local((jj-1)*mynloc+iloc(1:nloc)) - 0.5_pr*SUM(ulc_prev(iloc(1:nloc),nvel(1):nvel(dim))**2,DIM=2) / &
                                           ulc_prev(iloc(1:nloc),nden)**2*charX(1:nloc,1) - ulc_prev(iloc(1:nloc),nvel(1))*charX(1:nloc,2) - &
                                           1.0_pr/(d2u(1,iloc(1:nloc),1)-1.0_pr)*charX(1:nloc,3)
                      ELSE IF (jj>=nspc(1) .AND. jj<=nspc(Nspecm)) THEN
                         du (2,iloc(1:nloc),2) = 0.0_pr !u_diag
                         d2u(3,iloc(1:nloc),1) = (d2u(2,iloc(1:nloc),1)/MW_in(jj-nspc(1)+1) - d2u(2,iloc(1:nloc),2)*cp_in(jj-nspc(1)+1)) / &
                                                       ulc_prev(iloc(1:nloc),nden)/(d2u(2,iloc(1:nloc),1)-d2u(2,iloc(1:nloc),2))**2  !gamma_diag
                         du (1,iloc(1:nloc),2) = (ulc_prev(iloc(1:nloc),neng) - 0.5_pr*SUM(ulc_prev(iloc(1:nloc),nvel(1):nvel(dim))**2,DIM=2) / &
                                                       ulc_prev(iloc(1:nloc),nden))*d2u(3,iloc(1:nloc),1) !p_diag
                         d2u(3,iloc(1:nloc),2) = 0.5_pr/d2u(1,iloc(1:nloc),2)/ulc_prev(iloc(1:nloc),nden) * &
                                                       (d2u(3,iloc(1:nloc),1)*pres_prev(iloc(1:nloc),1)+d2u(1,iloc(1:nloc),1)*du(1,iloc(1:nloc),2)) !c_diag
                         charI(1:nloc) = -2.0_pr*(d2u(3,iloc(1:nloc),2)*(du(1,iloc(1:nloc),1)-dP0(faceint)) + d2u(1,iloc(1:nloc),2)*du_diag_PFB(iloc(1:nloc))*du(1,iloc(1:nloc),2) + &
                                                 ulc_prev(iloc(1:nloc),nden)*d2u(1,iloc(1:nloc),2)*pres_prev(iloc(1:nloc),2)*du_diag_local(iloc(1:nloc),1)*du(2,iloc(1:nloc),2) + &
                                                 du(2,iloc(1:nloc),1)*(ulc_prev(iloc(1:nloc),nden) * &
                                                 (d2u(3,iloc(1:nloc),2)*pres_prev(iloc(1:nloc),2) + d2u(1,iloc(1:nloc),2)*du (2,iloc(1:nloc),2))))  ! I_1 - I_N
                         charX(1:nloc,1) = -faceval*0.5_pr/d2u(1,iloc(1:nloc),2)**2*charI(1:nloc) + d2u(3,iloc(1:nloc),2)/d2u(1,iloc(1:nloc),2)**3*charI_prev(1:nloc) 
                         IF (chartype == 2) charX(1:nloc,1) = charX(1:nloc,1) - du(2,iloc(1:nloc),2)*(du(dim+2,iloc(1:nloc),1)-drho0(faceint) - & 
                                                                                 (du(1,iloc(1:nloc),1)-dP0(faceint))/d2u(1,iloc(1:nloc),2)**2) + &
                                                                                pres_prev(iloc(1:nloc),2)*(du_diag_PFB(iloc(1:nloc))*du(1,iloc(1:nloc),2)/d2u(1,iloc(1:nloc),2)**2 - &
                                                                                 2.0_pr*d2u(3,iloc(1:nloc),2)/d2u(1,iloc(1:nloc),2)**3*(du(1,iloc(1:nloc),1)-dP0(faceint)))
                         Drhs_diag_local((jj-1)*mynloc+iloc(1:nloc)) = Drhs_diag_local((jj-1)*mynloc+iloc(1:nloc)) - (charX_prev(1:nloc)+ulc_prev(iloc(1:nloc),jj)*charX(1:nloc,1)) / &
                                                        ulc_prev(iloc(1:nloc),nden)
                      END IF 
                   END DO
                END IF
                IF (chartype == 2) THEN
                   DO ii=2,dim
                      Drhs_diag_local((nvel(ii)-1)*mynloc+iloc(1:nloc)) = Drhs_diag_local((nvel(ii)-1)*mynloc+iloc(1:nloc)) + &
                                                                             du_diag_local(iloc(1:nloc),1)*ulc_prev(iloc(1:nloc),nvel(1))/ulc_prev(iloc(1:nloc),nden)
                   END DO
                   DO ii=1,Nspecm
                      Drhs_diag_local((nspc(ii)-1)*mynloc+iloc(1:nloc)) = Drhs_diag_local((nspc(ii)-1)*mynloc+iloc(1:nloc)) + &
                                                                             du_diag_local(iloc(1:nloc),1)*ulc_prev(iloc(1:nloc),nvel(1))/ulc_prev(iloc(1:nloc),nden)
                   END DO
                END IF
                IF (ALLOCATED(charI)) DEALLOCATE(charI)
                IF (ALLOCATED(charX)) DEALLOCATE(charX)
                IF (ALLOCATED(charV_prev)) DEALLOCATE(charV_prev)
                IF (ALLOCATED(charI_prev)) DEALLOCATE(charI_prev)
                IF (ALLOCATED(charX_prev)) DEALLOCATE(charX_prev)
             END IF
          END IF
       END IF
    END DO
  END SUBROUTINE user_evol_LODI_BC_Drhs_diag 

  SUBROUTINE user_convectzone_BC_rhs (myrhs_loc, ulc, myne_loc, mynloc, myjl, mymeth)
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: myne_loc, mynloc, myjl, mymeth
    REAL (pr), DIMENSION (mynloc*myne_loc), INTENT (INOUT) :: myrhs_loc
    REAL (pr), DIMENSION (mynloc,myne_loc), INTENT (IN) :: ulc

    INTEGER :: ie, i, shift
    REAL (pr), DIMENSION (myne_loc,mynloc,dim) :: du, d2u
    REAL (pr), DIMENSION (mynloc,2) :: bigU
    REAL (pr), DIMENSION (mynloc,myne_loc) :: myu_zer
         
    d2u(1,:,1)=1.0_pr
    d2u(1,:,1)=(SIGN(d2u(1,:,1),x(:,1)) + 1.0_pr)/2.0_pr;  !=1 for x>0, =0 for x<0
    du(1,:,1)=EXP(-pureg(2)/pureR(2)/Tconst*(x(:,1)-bndvals(dim+Nspec+3,1))) * (1.0_pr-d2u(1,:,1))  !hydrostatic relationship for bot fluid
    du(1,:,2)=EXP(-pureg(1)/pureR(1)/Tconst*(x(:,1)-bndvals(dim+Nspec+3,2))) * d2u(1,:,1)           !hydrostatic relationship for top fluid
    DO i=1,myne_loc
       myu_zer(:,i) = ulc(:,i) - (bndvals(i,1)*du(1,:,1) + bndvals(i,2)*du(1,:,2))
    END DO

    CALL c_diff_fast(myu_zer, du, d2u, myjl, mynloc, mymeth, 10, myne_loc, 1, myne_loc) 
   
    CALL user_bufferfunc(bigU, mynloc, buffcvd, polyconv)
    DO ie=1,myne_loc
       shift = (ie-1)*mynloc   
       myrhs_loc(shift+1:shift+mynloc) = myrhs_loc(shift+1:shift+mynloc) - (bigU(:,2)*c0(1)-bigU(:,1)*c0(2))*buffU0*du(ie,:,1)
    END DO
  END SUBROUTINE user_convectzone_BC_rhs 

  SUBROUTINE user_convectzone_BC_Drhs (Dmyrhs_loc, ulc, ulc_prev, myne_loc, mynloc, myjl, mymeth)
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: myne_loc, mynloc, myjl, mymeth
    REAL (pr), DIMENSION (mynloc*myne_loc), INTENT (INOUT) :: Dmyrhs_loc
    REAL (pr), DIMENSION (mynloc,myne_loc), INTENT (IN) :: ulc, ulc_prev

    INTEGER :: ie, i, shift
    REAL (pr), DIMENSION (myne_loc,mynloc,dim) :: du, d2u
    REAL (pr), DIMENSION (mynloc,2) :: bigU

    CALL c_diff_fast(ulc, du, d2u, myjl, mynloc, mymeth, 10, myne_loc, 1, myne_loc) 
    CALL user_bufferfunc(bigU, mynloc, buffcvd, polyconv)
    DO ie=1,myne_loc
       shift = (ie-1)*mynloc   
       Dmyrhs_loc(shift+1:shift+mynloc) = Dmyrhs_loc(shift+1:shift+mynloc) - (bigU(:,2)*c0(1)-bigU(:,1)*c0(2))*buffU0*du(ie,:,1)
    END DO
  END SUBROUTINE user_convectzone_BC_Drhs 

  SUBROUTINE user_convectzone_BC_Drhs_diag (Drhs_diag_local, ulc_prev, myne_loc, mynloc, myjl, mymeth)
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: myne_loc, mynloc, myjl, mymeth
    REAL (pr), DIMENSION (mynloc*myne_loc), INTENT (INOUT) :: Drhs_diag_local
    REAL (pr), DIMENSION (mynloc,myne_loc), INTENT (IN) :: ulc_prev

    REAL (pr), DIMENSION (mynloc,dim) :: du_diag_local, d2u_diag_local
    INTEGER :: ie, i, shift
    REAL (pr), DIMENSION (mynloc,2) :: bigU

    CALL c_diff_diag(du_diag_local, d2u_diag_local, j_lev, mynloc, mymeth, mymeth, 10)
    CALL user_bufferfunc(bigU, mynloc, buffcvd, polyconv)
    DO ie=1,myne_loc
       shift = (ie-1)*mynloc   
       Drhs_diag_local(shift+1:shift+mynloc) = Drhs_diag_local(shift+1:shift+mynloc) - (bigU(:,2)*c0(1)-bigU(:,1)*c0(2))*buffU0*du_diag_local(:,1)
    END DO
  END SUBROUTINE user_convectzone_BC_Drhs_diag 

  SUBROUTINE user_bufferfunc (bigU, mynloc, buffd,locpoly)
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: mynloc
    REAL (pr), DIMENSION (mynloc,2), INTENT(INOUT) :: bigU
    REAL (pr), DIMENSION (5), INTENT(IN) :: buffd
    LOGICAL, INTENT(IN) :: locpoly

    REAL (pr), DIMENSION (mynloc,2) :: bigU2
    INTEGER :: ie, i, shift, stri, stpi
    REAL (pr) :: topbnd, botbnd, buffsize, maxu, minu

    bigU(:,:)=0.0_pr
    bigU2(:,:)=0.0_pr

    buffsize=buffd(2)
    botbnd=xyzlimits(1,1)+buffd(1)
    topbnd=xyzlimits(2,1)-buffd(1)
    IF (buffsize>0.0_pr) THEN
       IF (locpoly) THEN
          bigU(:,1)=((botbnd-MIN(botbnd,MAX(botbnd-buffd(2),x(:,1))))/buffsize)**buffBeta;
          bigU(:,2)=((MAX(topbnd,MIN(topbnd+buffd(2),x(:,1)))-topbnd)/buffsize)**buffBeta;
       ELSE
          bigU(:,1)=(TANH(((botbnd-MIN(botbnd,MAX(botbnd-buffd(2),x(:,1))))/buffsize - buffOff)*buffFac) + 1.0_pr)/2.0_pr;
          bigU(:,2)=(TANH(((MAX(topbnd,MIN(topbnd+buffd(2),x(:,1)))-topbnd)/buffsize - buffOff)*buffFac) + 1.0_pr)/2.0_pr;
       END IF
       maxu=MAXVAL(bigU(:,1))
       minu=MINVAL(bigU(:,1))
       CALL parallel_global_sum (REALMAXVAL=maxu)
       CALL parallel_global_sum (REALMINVAL=minu)
       bigU(:,1)=(bigU(:,1)-minu)/(maxu-minu);
       maxu=MAXVAL(bigU(:,2))
       minu=MINVAL(bigU(:,2))
       CALL parallel_global_sum (REALMAXVAL=maxu)
       CALL parallel_global_sum (REALMINVAL=minu)
       bigU(:,2)=(bigU(:,2)-minu)/(maxu-minu);
    ELSEIF (botbnd>xyzlimits(1,1)) THEN
       bigU(:,:)=1.0_pr
       bigU(:,1)=(SIGN(bigU(:,1),botbnd-x(:,1)) + 1.0_pr)/2.0_pr;
       bigU(:,2)=(SIGN(bigU(:,2),x(:,1)-topbnd) + 1.0_pr)/2.0_pr; 
    END IF
    buffsize=buffd(4);
    botbnd=xyzlimits(1,1)+(buffd(1)-buffd(2)-buffd(3));
    topbnd=xyzlimits(2,1)-(buffd(1)-buffd(2)-buffd(3));
    IF (buffsize>0.0_pr) THEN
       IF (locpoly) THEN
           bigU2(:,1)=((buffd(4)-(botbnd-MIN(botbnd,MAX(botbnd-buffd(4),x(:,1)))))/buffsize)**buffBeta - 1.0_pr;
           bigU2(:,2)=((buffd(4)-(MAX(topbnd,MIN(topbnd+buffd(4),x(:,1)))-topbnd))/buffsize)**buffBeta - 1.0_pr;
       ELSE
           bigU2(:,1)=(TANH(-((botbnd-MIN(botbnd,MAX(botbnd-buffd(4),x(:,1))))/buffsize - buffOff)*buffFac) - 1.0_pr)/2.0_pr;
           bigU2(:,2)=(TANH(-((MAX(topbnd,MIN(topbnd+buffd(4),x(:,1)))-topbnd)/buffsize - buffOff)*buffFac) - 1.0_pr)/2.0_pr;
       END IF
       maxu=MAXVAL(bigU2(:,1))
       minu=MINVAL(bigU2(:,1))
       CALL parallel_global_sum (REALMAXVAL=maxu)
       CALL parallel_global_sum (REALMINVAL=minu)
       bigU2(:,1)=(bigU2(:,1)-minu)/(maxu-minu)-1.0_pr;
       maxu=MAXVAL(bigU2(:,2))
       minu=MINVAL(bigU2(:,2))
       CALL parallel_global_sum (REALMAXVAL=maxu)
       CALL parallel_global_sum (REALMINVAL=minu)
       bigU2(:,2)=(bigU2(:,2)-minu)/(maxu-minu)-1.0_pr;
    ELSEIF (botbnd>xyzlimits(1,1)) THEN
       bigU2(:,:)=1.0_pr
       bigU2(:,1)=-(SIGN(bigU2(:,1),botbnd-x(:,1)) + 1.0_pr)/2.0_pr;
       bigU2(:,2)=-(SIGN(bigU2(:,2),x(:,1)-topbnd) + 1.0_pr)/2.0_pr; 
    END IF
    bigU(:,:) = bigU(:,:) + bigU2(:,:)
  END SUBROUTINE user_bufferfunc 

  SUBROUTINE user_bndfunc (bndonly, mynloc, myjl)
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: mynloc, myjl
    REAL (pr), DIMENSION (mynloc), INTENT(INOUT) :: bndonly
    INTEGER :: i
    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc
    bndonly(:) = 0.0_pr
    i_p_face(0) = 1
    DO i=1,dim
       i_p_face(i) = i_p_face(i-1)*3
    END DO
    DO face_type = 0, 3**dim - 1
       face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
       IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
          CALL get_all_indices_by_face (face_type, myjl, nloc, iloc)
          IF(nloc > 0 ) THEN
             IF (ABS(face(1)) == 1) THEN
                bndonly(iloc(1:nloc)) = 1.0_pr
             END IF
          END IF
       END IF
    END DO
  END SUBROUTINE user_bndfunc 

  SUBROUTINE user_flxfunc (flxfunc, mynloc, myjl)
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: mynloc, myjl
    REAL (pr), DIMENSION (dim,mynloc,dim), INTENT(INOUT) :: flxfunc
    REAL (pr), DIMENSION (mynloc) :: myflxfunc
    INTEGER :: i,j
       CALL user_bndfunc(myflxfunc, mynloc, myjl)  !puts 1.0 on boundaries, 0.0 elsewhere 
       DO j=1,dim
          DO i=1,dim
             IF ((i.ne.j) .AND. (j.eq.1)) THEN
                flxfunc(i,:,j) = 1.0_pr - myflxfunc(:)
             ELSE
                flxfunc(i,:,j) = 1.0_pr
             END IF
          END DO
       END DO
  END SUBROUTINE user_flxfunc

  SUBROUTINE user_buffkine (kinefunc, u_dens, mynloc)
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: mynloc
    REAL (pr), DIMENSION (mynloc), INTENT(INOUT) :: kinefunc
    REAL (pr), DIMENSION (mynloc), INTENT(IN) :: u_dens
    REAL (pr), DIMENSION (mynloc,2) :: mod_dens
    REAL (pr), DIMENSION (mynloc,2) :: bigU
    REAL (pr), DIMENSION (5) :: buffknd

    IF (kinevisc.EQ.1) THEN
       kinefunc(:) = u_dens(:)
    ELSE
       kinefunc(:) = 1.0_pr
       IF (layertop.GT.0.0_pr) THEN
          buffknd(1) = MAX(xyzlimits(2,1)-layertop-kineviscbuff,0.0_pr)
          buffknd(2) = MIN(kinevisctran,buffknd(1))
          buffknd(3) = buffknd(1)-buffknd(2)
          buffknd(4:5) = 0.0_pr
          CALL user_bufferfunc(bigU, mynloc, buffknd, polykine)  !bigU is 1 in zone, 0 outside of zone; bigU(:,1) is for bot, bigU(:,2) is for top 
          IF (kinevisc.EQ.2) THEN
             kinefunc(:) = kinefunc(:) + bigU(:,2)*(u_dens(:)-1.0_pr)
          ELSE
             kinefunc(:) = kinefunc(:) + bigU(:,2)*(u_dens(:)/(Pint/pureR(1)/Tconst*EXP(-pureg(1)*(layertop+kineviscbuff+kinevisctran)/pureR(1)/Tconst))-1.0_pr)
          END IF
       END IF
       IF (layerbot.LT.0.0_pr) THEN
          buffknd(1) = MAX(layerbot-kineviscbuff-xyzlimits(1,1),0.0_pr)
          buffknd(2) = MIN(kinevisctran,buffknd(1))
          buffknd(3) = buffknd(1)-buffknd(2)
          buffknd(4:5) = 0.0_pr
          CALL user_bufferfunc(bigU, mynloc, buffknd, polykine)  !bigU is 1 in zone, 0 outside of zone; bigU(:,1) is for bot, bigU(:,2) is for top 
          IF (kinevisc.EQ.2) THEN
             kinefunc(:) = kinefunc(:) + bigU(:,1)*(u_dens(:)-1.0_pr)
          ELSE
             kinefunc(:) = kinefunc(:) + bigU(:,1)*(u_dens(:)/(Pint/pureR(2)/Tconst*EXP(-pureg(2)*(layerbot-kineviscbuff-kinevisctran)/pureR(2)/Tconst))-1.0_pr)
          END IF
       END IF
    END IF
  END SUBROUTINE user_buffkine 

  SUBROUTINE user_bufferRe (ReU, mynloc)
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: mynloc
    REAL (pr), DIMENSION (mynloc), INTENT(INOUT) :: ReU
    REAL (pr), DIMENSION (mynloc,2) :: bigU

    IF (dobuffRe) THEN
       CALL user_bufferfunc(bigU, mynloc, buffRed, polybuffRe)
       ReU(:) = bigU(:,1) + bigU(:,2)
       ReU(:) = 1.0_pr + (Re/Rebnd-1.0_pr)*ReU(:)
    ELSE
       ReU(:) = 1.0_pr
    END IF
  END SUBROUTINE user_bufferRe 

  FUNCTION user_chi (nlocal, t_local)
    IMPLICIT NONE
    INTEGER,          INTENT (IN) :: nlocal
    REAL (pr),        INTENT (IN) :: t_local
    REAL (pr), DIMENSION (nlocal) :: user_chi
    user_chi(:) = 0.0_pr
  END FUNCTION user_chi

  SUBROUTINE user_diffusionzone_BC_rhs (myrhs_loc, ulc, myne_loc, mynloc, myjl, mymeth)
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: myne_loc, mynloc, myjl, mymeth
    REAL (pr), DIMENSION (mynloc*myne_loc), INTENT (INOUT) :: myrhs_loc
    REAL (pr), DIMENSION (mynloc,myne_loc), INTENT (IN) :: ulc

    INTEGER :: ie, idim, i, shift
    REAL (pr), DIMENSION (mynloc,2) :: bigU
    REAL (pr), DIMENSION (mynloc) :: myvisc
    REAL (pr), DIMENSION (mynloc,dim) :: myviscfor, myviscbck
    REAL (pr), DIMENSION (dim,mynloc) :: h_arr
    REAL (pr), DIMENSION (myne_loc,mynloc,dim) :: du, d2u
    REAL (pr), DIMENSION (mynloc,myne_loc) :: ulc_star, diffterm

    CALL get_all_local_h (h_arr)
    CALL user_bufferfunc(bigU, mynloc, buffdfd, polydiff)  !bigU is 1 in zone, 0 outside of zone; bigU(:,1) is for bot, bigU(:,2) is for top
 
    IF (diffstab .EQ. 0) THEN
       myvisc = difffac/2.0_pr/MAX(MIN(cflmax/CFLnonl*dt,mydt_orig),dt)/SUM(1.0_pr/h_arr**2,DIM=1)*(bigU(:,1) + bigU(:,2)) !visc(x,y,z)
    ELSE
       myvisc = diffvisc*(bigU(:,1) + bigU(:,2)) !visc(x,y,z)
    END IF

    IF (adddiffvisc .EQ. 1) THEN  !Low/High-Order Interpolation
       CALL c_diff_fast(myvisc, du(1,:,:), d2u(1,:,:), myjl, mynloc, 2+adddiffhiord, 10, 1, 1, 1) ! back
       DO i=1,dim
          myviscbck(:,i) = myvisc - du(1,:,i)*h_arr(i,:)/2.0_pr
       END DO
       CALL c_diff_fast(myvisc, du(1,:,:), d2u(1,:,:), myjl, mynloc, 4+adddiffhiord, 10, 1, 1, 1) ! forward
       DO i=1,dim
          myviscfor(:,i) = myvisc + du(1,:,i)*h_arr(i,:)/2.0_pr
       END DO
       myvisc=(SUM(myviscfor,DIM=2)+SUM(myviscbck,DIM=2))/2.0_pr/REAL(dim,pr)
    ELSE
       DO i=1,dim
          myviscfor(:,i)=myvisc(:)
          myviscbck(:,i)=myvisc(:)
       END DO
    END IF

    d2u(1,:,1)=1.0_pr
    d2u(1,:,1)=(SIGN(d2u(1,:,1),x(:,1)) + 1.0_pr)/2.0_pr;  !=1 for x>0, =0 for x<0
    du(1,:,1)=EXP(-pureg(2)/pureR(2)/Tconst*(x(:,1)-bndvals(dim+Nspec+3,1))) * (1.0_pr-d2u(1,:,1))  !hydrostatic relationship for bot fluid
    du(1,:,2)=EXP(-pureg(1)/pureR(1)/Tconst*(x(:,1)-bndvals(dim+Nspec+3,2))) * d2u(1,:,1)           !hydrostatic relationship for top fluid
    DO i=1,myne_loc
       ulc_star(:,i) = ulc(:,i) - (bndvals(i,1)*du(1,:,1) + bndvals(i,2)*du(1,:,2))
    END DO

    diffterm=0.0_pr
    IF (adddifftype .EQ. 1) THEN
       CALL c_diff_fast(ulc_star(:,:), du(:,:,:), d2u(:,:,:), myjl, mynloc, 0+adddiffhiord, 10, myne_loc, 1, myne_loc) ! central
       d2u = du
       DO idim=1,dim
          DO ie=1,myne_loc
             ulc_star(:,ie) = d2u(ie,:,idim)*myvisc(:)
          END DO
          CALL c_diff_fast(ulc_star(:,:), du(:,:,:), d2u(:,:,:), myjl, mynloc, 0+adddiffhiord, 10, myne_loc, 1, myne_loc) ! central
          DO ie=1,myne_loc
             diffterm(:,ie) = diffterm(:,ie) + du(ie,:,idim)
          END DO
       END DO
    ELSEIF (adddifftype .EQ. 2) THEN
       CALL c_diff_fast(ulc_star(:,:), du(:,:,:), d2u(:,:,:), myjl, mynloc, 0+adddiffhiord, 01, myne_loc, 1, myne_loc) ! central
       DO i=1,myne_loc
          diffterm(:,i) = diffterm(:,i) + myvisc(:)*SUM(d2u(i,:,:),DIM=2)
       END DO
    ELSEIF (adddifftype .EQ. 3) THEN
       CALL c_diff_fast(ulc_star(:,:), du(:,:,:), d2u(:,:,:), myjl, mynloc, 0+adddiffhiord, -01, myne_loc, 1, myne_loc) ! back then forward
       DO i=1,myne_loc
          diffterm(:,i) = diffterm(:,i) + myvisc(:)*SUM(d2u(i,:,:),DIM=2)
       END DO
    ELSE
       CALL c_diff_fast(ulc_star(:,:), du(:,:,:), d2u(:,:,:), myjl, mynloc, 2+adddiffhiord, 10, myne_loc, 1, myne_loc) ! back
       DO i=1,myne_loc
          DO idim=1,dim
             diffterm(:,i) = diffterm(:,i) - myviscbck(:,idim)*du(i,:,idim)/h_arr(idim,:)
          END DO
       END DO
       CALL c_diff_fast(ulc_star(:,:), du(:,:,:), d2u(:,:,:), myjl, mynloc, 4+adddiffhiord, 10, myne_loc, 1, myne_loc) ! forward
       DO i=1,myne_loc
          DO idim=1,dim
             diffterm(:,i) = diffterm(:,i) + myviscfor(:,idim)*du(i,:,idim)/h_arr(idim,:)
          END DO
       END DO
    END IF

    DO ie=1,myne_loc
       shift = (ie-1)*mynloc   
       myrhs_loc(shift+1:shift+mynloc) = myrhs_loc(shift+1:shift+mynloc) + diffterm(:,ie) 
    END DO
  END SUBROUTINE user_diffusionzone_BC_rhs 

  SUBROUTINE user_diffusionzone_BC_Drhs (Dmyrhs_loc, ulc, ulc_prev, myne_loc, mynloc, myjl, mymeth)
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: myne_loc, mynloc, myjl, mymeth
    REAL (pr), DIMENSION (mynloc*myne_loc), INTENT (INOUT) :: Dmyrhs_loc
    REAL (pr), DIMENSION (mynloc,myne_loc), INTENT (IN) :: ulc, ulc_prev

    INTEGER :: ie, idim, i, shift
    REAL (pr), DIMENSION (mynloc,2) :: bigU
    REAL (pr), DIMENSION (mynloc) :: myvisc
    REAL (pr), DIMENSION (mynloc,dim) :: myviscfor, myviscbck
    REAL (pr), DIMENSION (dim,mynloc) :: h_arr
    REAL (pr), DIMENSION (myne_loc,mynloc,dim) :: du, d2u
    REAL (pr), DIMENSION (mynloc,myne_loc) :: ulc_star, diffterm

    CALL get_all_local_h (h_arr)
    CALL user_bufferfunc(bigU, mynloc, buffdfd, polydiff)  !bigU is 1 in zone, 0 outside of zone; bigU(:,1) is for bot, bigU(:,2) is for top

    IF (diffstab .EQ. 0) THEN
       myvisc = difffac/2.0_pr/MAX(MIN(cflmax/CFLnonl*dt,mydt_orig),dt)/SUM(1.0_pr/h_arr**2,DIM=1)*(bigU(:,1) + bigU(:,2)) !visc(x,y,z)
    ELSE
       myvisc = diffvisc*(bigU(:,1) + bigU(:,2)) !visc(x,y,z)
    END IF

    IF (adddiffvisc .EQ. 1) THEN  !Low/High-Order Interpolation
       CALL c_diff_fast(myvisc, du(1,:,:), d2u(1,:,:), myjl, mynloc, 2+adddiffhiord, 10, 1, 1, 1) ! back
       DO i=1,dim
          myviscbck(:,i) = myvisc - du(1,:,i)*h_arr(i,:)/2.0_pr
       END DO
       CALL c_diff_fast(myvisc, du(1,:,:), d2u(1,:,:), myjl, mynloc, 4+adddiffhiord, 10, 1, 1, 1) ! forward
       DO i=1,dim
          myviscfor(:,i) = myvisc + du(1,:,i)*h_arr(i,:)/2.0_pr
       END DO
       myvisc=(SUM(myviscfor,DIM=2)+SUM(myviscbck,DIM=2))/2.0_pr/REAL(dim,pr)
    ELSE
       DO i=1,dim
          myviscfor(:,i)=myvisc(:)
          myviscbck(:,i)=myvisc(:)
       END DO
    END IF

    diffterm=0.0_pr
    IF (adddifftype .EQ. 1) THEN
       CALL c_diff_fast(ulc(:,:), du(:,:,:), d2u(:,:,:), myjl, mynloc, 0+adddiffhiord, 10, myne_loc, 1, myne_loc) ! central
       d2u = du
       DO idim=1,dim
          DO ie=1,myne_loc
             ulc_star(:,ie) = d2u(ie,:,idim)*myvisc(:)
          END DO
          CALL c_diff_fast(ulc_star(:,:), du(:,:,:), d2u(:,:,:), myjl, mynloc, 0+adddiffhiord, 10, myne_loc, 1, myne_loc) ! central
          DO ie=1,myne_loc
             diffterm(:,ie) = diffterm(:,ie) + du(ie,:,idim)
          END DO
       END DO
    ELSEIF (adddifftype .EQ. 2) THEN
       CALL c_diff_fast(ulc(:,:), du(:,:,:), d2u(:,:,:), myjl, mynloc, 0+adddiffhiord, 01, myne_loc, 1, myne_loc) ! central
       DO i=1,myne_loc
          diffterm(:,i) = diffterm(:,i) + myvisc(:)*SUM(d2u(i,:,:),DIM=2)
       END DO
    ELSEIF (adddifftype .EQ. 3) THEN
       CALL c_diff_fast(ulc(:,:), du(:,:,:), d2u(:,:,:), myjl, mynloc, 0+adddiffhiord, -01, myne_loc, 1, myne_loc) ! back then forward
       DO i=1,myne_loc
          diffterm(:,i) = diffterm(:,i) + myvisc(:)*SUM(d2u(i,:,:),DIM=2)
       END DO
    ELSE
       CALL c_diff_fast(ulc(:,:), du(:,:,:), d2u(:,:,:), myjl, mynloc, 2+adddiffhiord, 10, myne_loc, 1, myne_loc) ! back
       DO i=1,myne_loc
          DO idim=1,dim
             diffterm(:,i) = diffterm(:,i) - myviscbck(:,idim)*du(i,:,idim)/h_arr(idim,:)
          END DO
       END DO
       CALL c_diff_fast(ulc(:,:), du(:,:,:), d2u(:,:,:), myjl, mynloc, 4+adddiffhiord, 10, myne_loc, 1, myne_loc) ! forward
       DO i=1,myne_loc
          DO idim=1,dim
             diffterm(:,i) = diffterm(:,i) + myviscfor(:,idim)*du(i,:,idim)/h_arr(idim,:)
          END DO
       END DO
    END IF

    DO ie=1,myne_loc
       shift = (ie-1)*mynloc   
       Dmyrhs_loc(shift+1:shift+mynloc) = Dmyrhs_loc(shift+1:shift+mynloc) + diffterm(:,ie) 
    END DO
  END SUBROUTINE user_diffusionzone_BC_Drhs 

  SUBROUTINE user_diffusionzone_BC_Drhs_diag (Drhs_diag_local, ulc_prev, myne_loc, mynloc, myjl, mymeth)
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: myne_loc, mynloc, myjl, mymeth
    REAL (pr), DIMENSION (mynloc*myne_loc), INTENT (INOUT) :: Drhs_diag_local
    REAL (pr), DIMENSION (mynloc,myne_loc), INTENT (IN) :: ulc_prev

    REAL (pr), DIMENSION (mynloc,2) :: bigU
    REAL (pr), DIMENSION (mynloc,dim) :: du_diag, d2u_diag
    REAL (pr), DIMENSION (1,mynloc,dim) :: du, d2u
    INTEGER :: ie, idim, i, shift
    REAL (pr), DIMENSION (mynloc) :: myvisc
    REAL (pr), DIMENSION (mynloc,dim) :: myviscfor, myviscbck
    REAL (pr), DIMENSION (dim,mynloc) :: h_arr
    REAL (pr), DIMENSION (mynloc,myne_loc) :: diffterm

    CALL get_all_local_h (h_arr)
    CALL user_bufferfunc(bigU, mynloc, buffdfd, polydiff)  !bigU is 1 in zone, 0 outside of zone; bigU(:,1) is for bot, bigU(:,2) is for top

    IF (diffstab .EQ. 0) THEN
       myvisc = difffac/2.0_pr/MAX(MIN(cflmax/CFLnonl*dt,mydt_orig),dt)/SUM(1.0_pr/h_arr**2,DIM=1)*(bigU(:,1) + bigU(:,2)) !visc(x,y,z)
    ELSE
       myvisc = diffvisc*(bigU(:,1) + bigU(:,2)) !visc(x,y,z)
    END IF

    IF (adddiffvisc .EQ. 1) THEN  !Low/High-Order Interpolation
       CALL c_diff_fast(myvisc, du(1,:,:), d2u(1,:,:), myjl, mynloc, 2+adddiffhiord, 10, 1, 1, 1) ! back
       DO i=1,dim
          myviscbck(:,i) = myvisc - du(1,:,i)*h_arr(i,:)/2.0_pr
       END DO
       CALL c_diff_fast(myvisc, du(1,:,:), d2u(1,:,:), myjl, mynloc, 4+adddiffhiord, 10, 1, 1, 1) ! forward
       DO i=1,dim
          myviscfor(:,i) = myvisc + du(1,:,i)*h_arr(i,:)/2.0_pr
       END DO
       myvisc=(SUM(myviscfor,DIM=2)+SUM(myviscbck,DIM=2))/2.0_pr/REAL(dim,pr)
    ELSE
       DO i=1,dim
          myviscfor(:,i)=myvisc(:)
          myviscbck(:,i)=myvisc(:)
       END DO
    END IF

    diffterm=0.0_pr
    IF (adddifftype .EQ. 1) THEN
       CALL c_diff_diag(du_diag, d2u_diag, myjl, mynloc, 0+adddiffhiord, 0+adddiffhiord, 10)
       DO idim=1,dim
          DO ie=1,myne_loc
             diffterm(:,ie) = diffterm(:,ie) + myvisc(:)*du_diag(:,idim)**2
          END DO
       END DO
    ELSEIF (adddifftype .EQ. 2) THEN
       CALL c_diff_diag(du_diag, d2u_diag, myjl, mynloc, 0+adddiffhiord, 0+adddiffhiord, 01)
       DO i=1,myne_loc
          diffterm(:,i) = diffterm(:,i) + myvisc(:)*SUM(d2u_diag(:,:),DIM=2)
       END DO
    ELSEIF (adddifftype .EQ. 3) THEN
       CALL c_diff_diag(du_diag, d2u_diag, myjl, mynloc, 0+adddiffhiord, 0+adddiffhiord, -01)
       DO i=1,myne_loc
          diffterm(:,i) = diffterm(:,i) + myvisc(:)*SUM(d2u_diag(:,:),DIM=2)
       END DO
    ELSE
       CALL c_diff_diag(du_diag, d2u_diag, myjl, mynloc, 2+adddiffhiord, 2+adddiffhiord, 10)
       DO i=1,myne_loc
          DO idim=1,dim
             diffterm(:,i) = diffterm(:,i) - myviscbck(:,idim)*du_diag(:,idim)/h_arr(idim,:)
          END DO
       END DO
       CALL c_diff_diag(du_diag, d2u_diag, myjl, mynloc, 4+adddiffhiord, 4+adddiffhiord, 10)
       DO i=1,myne_loc
          DO idim=1,dim
             diffterm(:,i) = diffterm(:,i) + myviscbck(:,idim)*du_diag(:,idim)/h_arr(idim,:)
          END DO
       END DO
    END IF

    DO ie=1,myne_loc
       shift = (ie-1)*mynloc   
       Drhs_diag_local(shift+1:shift+mynloc) = Drhs_diag_local(shift+1:shift+mynloc) + diffterm(:,ie) 
    END DO
  END SUBROUTINE user_diffusionzone_BC_Drhs_diag 

  SUBROUTINE user_bufferzone_BC_rhs (myrhs_loc, ulc, myne_loc, mynloc, myjl, mymeth)
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: myne_loc, mynloc, myjl, mymeth
    REAL (pr), DIMENSION (mynloc*myne_loc), INTENT (INOUT) :: myrhs_loc
    REAL (pr), DIMENSION (mynloc,myne_loc), INTENT (IN) :: ulc

    INTEGER :: ie, i, shift, stri, stpi
    REAL (pr), DIMENSION (mynloc,2) :: bigU
    REAL (pr), DIMENSION (mynloc,myne_loc) :: myu_zer

    stri=1
    stpi=myne_loc
    IF (pBrink) THEN
       stri=neng
       stpi=neng
    END IF

    bigU(:,2)=1.0_pr
    bigU(:,2)=(SIGN(bigU(:,2),x(:,1)) + 1.0_pr)/2.0_pr;  !=1 for x>0, =0 for x<0
    bigU(:,1)=EXP(-pureg(2)/pureR(2)/Tconst*(x(:,1)-bndvals(dim+Nspec+3,1))) * (1.0_pr-bigU(:,2))  !hydrostatic relationship for bot fluid
    bigU(:,2)=EXP(-pureg(1)/pureR(1)/Tconst*(x(:,1)-bndvals(dim+Nspec+3,2))) * bigU(:,2)           !hydrostatic relationship for top fluid
    DO ie=1,myne_loc
       myu_zer(:,ie) = ulc(:,ie) - (bndvals(ie,1)*bigU(:,1) + bndvals(ie,2)*bigU(:,2))
    END DO

    CALL user_bufferfunc(bigU, mynloc, buffbfd, polybuff)
    DO ie=stri,stpi
       shift = (ie-1)*mynloc   
       myrhs_loc(shift+1:shift+mynloc) = myrhs_loc(shift+1:shift+mynloc) - (bigU(:,2)+bigU(:,1))*buffSig*myu_zer(:,ie)
    END DO
  END SUBROUTINE user_bufferzone_BC_rhs 

  SUBROUTINE user_bufferzone_BC_Drhs (Dmyrhs_loc, ulc, ulc_prev, myne_loc, mynloc, myjl, mymeth)
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: myne_loc, mynloc, myjl, mymeth
    REAL (pr), DIMENSION (mynloc*myne_loc), INTENT (INOUT) :: Dmyrhs_loc
    REAL (pr), DIMENSION (mynloc,myne_loc), INTENT (IN) :: ulc, ulc_prev

    INTEGER :: ie, i, shift, stri, stpi
    REAL (pr), DIMENSION (mynloc,2) :: bigU

    stri=1
    stpi=myne_loc
    IF (pBrink) THEN
       stri=neng
       stpi=neng
    END IF

    CALL user_bufferfunc(bigU, mynloc, buffbfd, polybuff)
    DO ie=stri,stpi
       shift = (ie-1)*mynloc   
       Dmyrhs_loc(shift+1:shift+mynloc) = Dmyrhs_loc(shift+1:shift+mynloc) - buffSig*(bigU(:,2)+bigU(:,1))*ulc(:,ie)
    END DO
  END SUBROUTINE user_bufferzone_BC_Drhs 

  SUBROUTINE user_bufferzone_BC_Drhs_diag (Drhs_diag_local, ulc_prev, myne_loc, mynloc, myjl, mymeth)
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: myne_loc, mynloc, myjl, mymeth
    REAL (pr), DIMENSION (mynloc*myne_loc), INTENT (INOUT) :: Drhs_diag_local
    REAL (pr), DIMENSION (mynloc,myne_loc), INTENT (IN) :: ulc_prev

    INTEGER :: ie, i, shift, stri, stpi
    REAL (pr), DIMENSION (mynloc,2) :: bigU

    stri=1
    stpi=myne_loc
    IF (pBrink) THEN
       stri=neng
       stpi=neng
    END IF

    CALL user_bufferfunc(bigU, mynloc, buffbfd, polybuff)
    DO ie=stri,stpi
       shift = (ie-1)*mynloc   
       Drhs_diag_local(shift+1:shift+mynloc) = Drhs_diag_local(shift+1:shift+mynloc) - buffSig*(bigU(:,2)+bigU(:,1))
    END DO
  END SUBROUTINE user_bufferzone_BC_Drhs_diag 

  SUBROUTINE internal_rhs (int_rhs,u_integrated,doBC,addingon)
    IMPLICIT NONE
    REAL (pr), DIMENSION (ng,ne), INTENT(IN) :: u_integrated !1D flat version of integrated variables without BC 
    REAL (pr), DIMENSION (ng*ne), INTENT(INOUT) :: int_rhs
    INTEGER, INTENT(IN) :: doBC !if zero do full rhs, if one no x-derivatives 
    INTEGER, INTENT(IN) :: addingon 

    INTEGER :: i, j, l, ie, shift, tempintd, jshift
    INTEGER, PARAMETER :: meth=1
    REAL (pr), DIMENSION (ne*dim,ng,dim) :: du, d2u
    REAL (pr), DIMENSION (ng,ne*dim) :: F
    REAL (pr), DIMENSION (ng,dim+Nspec) :: v ! velocity components + temperature + Y
    REAL (pr) :: usej
    REAL (pr), DIMENSION (ng) :: p
    REAL (pr), DIMENSION (ng,3) :: props
    REAL (pr), DIMENSION (ng) :: ReU

    CALL user_bufferRe (ReU, ng)

    int_rhs = 0.0_pr
    DO l = 1,Nspecm
       v(:,dim+1+l) = u_integrated(:,nspc(l))/u_integrated(:,nden) !Y_l
    END DO
    p(:) = (1.0_pr-SUM(v(:,dim+2:dim+Nspec),DIM=2))  !Y_Nspec
    props(:,1) = p(:)*cp_in(Nspec) !cp_Nspec
    props(:,2) = p(:)/MW_in(Nspec) !R_Nspec
    DO l=1,Nspecm
       props(:,1) = props(:,1) + v(:,dim+1+l)*cp_in(l) !cp
       props(:,2) = props(:,2) + v(:,dim+1+l)/MW_in(l) !R
    END DO
    p(:) = (u_integrated(:,neng)-0.5_pr*SUM(u_integrated(:,nvel(1):nvel(dim))**2,DIM=2)/u_integrated(:,nden))/(props(:,1)/props(:,2)-1.0_pr)  !pressure
    v(:,dim+1) = p(:)/props(:,2)/u_integrated(:,nden)  !Temperature
    DO i = 1, dim
       v(:,i) = u_integrated(:,nvel(i)) / u_integrated(:,nden) ! u_i
    END DO
    IF ((doBC .eq. 0) .AND. ((methpd .ne. 0) .OR. splitFBpress) .AND. IMEXswitch .LE. 0) THEN  !IF doBC>0, these terms are done inside LODI subroutines
       du(1,:,1) = p(:)
       CALL dpdx (du(1,:,1),ng,v(:,dim+2:dim+Nspec),2*methpd+meth,j_lev,splitFBpress)
       shift = (nvel(1)-1)*ng   
       int_rhs(shift+1:shift+ng) = int_rhs(shift+1:shift+ng)-du(1,:,1)
       shift = (neng-1)*ng   
       int_rhs(shift+1:shift+ng) = int_rhs(shift+1:shift+ng)-du(1,:,1)*v(:,1) !u*dp/dx
       CALL c_diff_fast(v(:,1), du(1,:,:), d2u(1,:,:), j_lev, ng, meth, 10, 1, 1, 1) ! du/dx
       int_rhs(shift+1:shift+ng) = int_rhs(shift+1:shift+ng)-du(1,:,1)*p(:)   !p*du/dx
    END IF
    !
    !--Form right hand side of Euler Equations
    !
    F(:,:) = 0.0_pr
 IF (IMEXswitch .LE. 0) THEN
   DO j = 1+doBC, dim
       jshift=ne*(j-1)
       F(:,nden+jshift) = F(:,nden+jshift) + u_integrated(:,nvel(j)) ! rho*u_j
       DO i = 1, dim
          F(:,nvel(i)+jshift) = F(:,nvel(i)+jshift) + u_integrated(:,nvel(i))*v(:,j) ! rho*u_i*u_j
       END DO
       F(:,nvel(j)+jshift) = F(:,nvel(j)+jshift) + p(:) ! rho*u_i^2+p
       F(:,neng+jshift) = F(:,neng+jshift) + ( u_integrated(:,neng) + p(:) )*v(:,j) ! (rho*e+p)*u_j
       DO l=1,Nspecm
          F(:,nspc(l)+jshift) = F(:,nspc(l)+jshift) + v(:,dim+1+l)*u_integrated(:,nvel(j)) ! rho*Y*u_j
       END DO  
    END DO
    IF ((doBC .eq. 0) .AND. ((methpd .ne. 0) .OR. splitFBpress)) THEN  !IF doBC>0, these terms are done inside LODI subroutines
       F(:,nvel(1)) = F(:,nvel(1)) - p(:)
       F(:,neng)    = F(:,neng)    - p(:)*v(:,1)
    END IF
    IF (doBC .ne. 0) THEN  !Missing term from LODI assumps... gamma=constant
       CALL c_diff_fast(props(:,1)/props(:,2), du(1,:,:), d2u(1,:,:), j_lev, ng, meth, 10, 1, 1, 1)  !d(gamma/(gamma-1))/dx
       shift = (neng-1)*ng   
       int_rhs(shift+1:shift+ng) = int_rhs(shift+1:shift+ng)-du(1,:,1)*v(:,1)*p(:) !pu*d(gamma/(gamma-1))/dx
    END IF
  END IF
    !
    !-- Form right hand side of Navier-Stokes Equations
    !
  IF (IMEXswitch .GE. 0) THEN
    IF(NS) THEN 
       CALL c_diff_fast(v, du(1:dim+Nspec,:,:), d2u(1:dim+Nspec,:,:), j_lev, ng, meth, 10, dim+Nspec, 1,dim+Nspec) ! du_i/dx_j & dT/dx_j & dY_l/dx_j
       p(:)=(1.0_pr-SUM(v(:,dim+2:dim+Nspec),DIM=2))
       props(:,1)=p(:)*mu_in(Nspec)  !mu_Nspec
       props(:,2)=p(:)*kk_in(Nspec)  !kk_Nspec
       props(:,3)=p(:)*bD_in(Nspec)  !bD_Nspec
       DO l=1,Nspecm
          props(:,1)=props(:,1)+v(:,dim+1+l)*mu_in(l)  !mu
          props(:,2)=props(:,2)+v(:,dim+1+l)*kk_in(l)  !kk
          props(:,3)=props(:,3)+v(:,dim+1+l)*bD_in(l)  !bD
       END DO
       IF (kinevisc .GT. 0) THEN
          CALL user_buffkine (p, u_integrated(:,nden), ng)
          props(:,1) = props(:,1)*p(:)
          props(:,2) = props(:,2)*p(:)
       END IF
       p(:) = du(1,:,1) !reuse of a variable
       DO i = 2, dim
          p(:) = p(:) + du(i,:,i) !du_i/dx_i
       END DO
       IF (dervFlux==1) THEN
          CALL user_flxfunc (d2u(1:dim,:,1:dim), ng, j_lev)
       ELSE
          d2u(:,:,:) = 1.0_pr
       END IF
       DO j = 1, dim
          jshift=ne*(j-1)
          DO i = 1, dim
             F(:,nvel(i)+jshift) = F(:,nvel(i)+jshift) - ReU(:)*props(:,1)*( du(i,:,j)+du(j,:,i) ) * d2u(i,:,j)  !rho*u_i*u_j - 2*mu*S_ij/Re  
          END DO
          F(:,nvel(j)+jshift) = F(:,nvel(j)+jshift) + 2.0_pr/3.0_pr*ReU(:)*props(:,1)*p(:) * d2u(j,:,j)  !rho*u_i*u_j - tau_ij/Re  [ tau_ij = 2*mu*( S_ij - 2/3*S_kk*delta_ij ) ] 
          DO i = 1, dim
             F(:,neng+jshift) =  F(:,neng+jshift) - props(:,1)*ReU(:)*( du(i,:,j)+du(j,:,i) )*v(:,i) * d2u(i,:,j)  !(e+p)*u_j - u_i*2*mu*S_ij/Re 
          END DO
          F(:,neng+jshift) =  F(:,neng+jshift) + 2.0_pr/3.0_pr*props(:,1)*ReU(:)*p(:)*v(:,j) * d2u(j,:,j) - ReU(:)*props(:,2)*du(dim+1,:,j) * d2u(2,:,j)  
                                                                                  !(e+p)*u_j - u_i*tau_ij/Re - q_j/Re/Pra [ q_j = mu dT/dx_j ]
          DO l=1,Nspecm
             F(:,neng+jshift) =  F(:,neng+jshift) - u_integrated(:,nden)*cp_in(l)*v(:,dim+1)*ReU(:)*props(:,3)*du(dim+1+l,:,j) * d2u(2,:,j)
          END DO
          F(:,neng+jshift) =  F(:,neng+jshift) +  u_integrated(:,nden)*cp_in(Nspec)*v(:,dim+1)*ReU(:)*props(:,3)*SUM(du(dim+2:dim+Nspec,:,j),DIM=1) * d2u(2,:,j)
          DO l=1,Nspecm
             F(:,nspc(l)+jshift) =  F(:,nspc(l)+jshift) - u_integrated(:,nden)*ReU(:)*props(:,3)*du(dim+1+l,:,j) * d2u(2,:,j)
          END DO
       END DO
    END IF
  END IF
    IF (GRAV .AND. IMEXswitch .LE. 0) THEN
      shift = (neng-1)*ng   
      DO j=1,dim
         DO l=1,Nspecm
            int_rhs(shift+1:shift+ng) = int_rhs(shift+1:shift+ng)-v(:,dim+1+l)*u_integrated(:,nvel(j))*gr_in(l+Nspec*(j-1))
         END DO
         int_rhs(shift+1:shift+ng) = int_rhs(shift+1:shift+ng) - (1.0_pr-SUM(v(:,dim+2:dim+Nspec),DIM=2))*u_integrated(:,nvel(j))*gr_in(Nspec*j)
      END DO
    END IF
    tempintd=ne*dim
    CALL c_diff_fast(F(:,1:tempintd), du(1:tempintd,:,:), d2u(1:tempintd,:,:), j_lev, ng, meth, 10, tempintd, 1, tempintd)  !du(i,:,k)=dF_ij/dx_k
    DO j = 1, dim
       jshift=ne*(j-1)
       DO i = 1, dim+1+Nspec
          shift=(i-1)*ng
          int_rhs(shift+1:shift+ng) = int_rhs(shift+1:shift+ng) - du(i+jshift,:,j)  ! -dF_ij/dx_j
       END DO
       IF (GRAV .AND. IMEXswitch .LE. 0) THEN
          shift = (nvel(j)-1)*ng
          DO l=1,Nspecm
             int_rhs(shift+1:shift+ng) = int_rhs(shift+1:shift+ng) - gr_in(l+Nspec*(j-1))*u_integrated(:,nspc(l))
          END DO
          int_rhs(shift+1:shift+ng) = int_rhs(shift+1:shift+ng) - gr_in(Nspec*j)*(u_integrated(:,nden)-SUM(u_integrated(:,nspc(1):nspc(Nspecm)),DIM=2))
       END IF
    END DO
    IF (NS .AND. dervFlux==2 .AND. IMEXswitch .GE. 0) THEN
       CALL c_diff_fast(v, du(1:dim+Nspec,:,:), d2u(1:dim+Nspec,:,:), j_lev, ng, meth, 10, dim+Nspec, 1,dim+Nspec) ! du_i/dx_j & dT/dx_j & dY_l/dx_j
       DO i = 2, dim
          F(:,i-1) = ReU(:)*props(:,1)*( du(i,:,1)+du(1,:,i) )  !2*mu*S_ij/Re  
       END DO
       F(:,dim) = ReU(:)*props(:,2)*du(dim+1,:,1)  !q_j/Re/Pra [ q_j = mu dT/dx_j ]
       DO l=1,Nspecm
          F(:,dim+l) =  u_integrated(:,nden)*ReU(:)*props(:,3)*du(dim+1+l,:,1)
       END DO
       F(:,dim+Nspec) = -u_integrated(:,nden)*ReU(:)*props(:,3)*SUM(du(dim+2:dim+Nspec,:,1),DIM=1)
       CALL c_diff_fast(F(:,1:dim+Nspec), du(1:dim+Nspec,:,:), d2u(1:dim+Nspec,:,:), j_lev, ng, meth, 10, dim+Nspec, 1, dim+Nspec)  !du(i,:,k)=dF_ij/dx_k
       CALL user_flxfunc (d2u(1:dim,:,1:dim), ng, j_lev)
       d2u(1:dim,:,1:dim) = 1.0_pr - d2u(1:dim,:,1:dim)
       DO i=2,dim
          shift=(nvel(i)-1)*ng
          int_rhs(shift+1:shift+ng) = int_rhs(shift+1:shift+ng) - du(i-1,:,1)*d2u(2,:,1)
       END DO
       shift=(neng-1)*ng
       DO i=2,dim
          int_rhs(shift+1:shift+ng) = int_rhs(shift+1:shift+ng) - du(i-1,:,1)*v(:,i)*d2u(2,:,1)
       END DO
       int_rhs(shift+1:shift+ng) = int_rhs(shift+1:shift+ng) - du(dim,:,1)*d2u(2,:,1) 
       DO l=1,Nspec
          int_rhs(shift+1:shift+ng) = int_rhs(shift+1:shift+ng) - v(:,dim+1)*cp_in(l)*du(dim+l,:,1)*d2u(2,:,1)
       END DO
       DO l=1,Nspecm
          shift=(nspc(l)-1)*ng
          int_rhs(shift+1:shift+ng) = int_rhs(shift+1:shift+ng) - du(dim+l,:,1)*d2u(2,:,1) 
       END DO
    END IF
  END SUBROUTINE internal_rhs

  SUBROUTINE internal_Drhs (int_Drhs, u_p, u_prev, meth,doBC,addingon)
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: meth
    REAL (pr), DIMENSION (ng,ne), INTENT(IN) :: u_p, u_prev
    REAL (pr), DIMENSION (ng*ne), INTENT(INOUT) :: int_Drhs
    INTEGER, INTENT(IN) :: doBC !if zero do full rhs, if one no x-derivatives 
    INTEGER, INTENT(IN) :: addingon 

    INTEGER :: i, j, l, ie, shift, vshift, jshift, tempintd
    REAL (pr), DIMENSION (ne*dim,ng,dim) :: du, d2u
    REAL (pr), DIMENSION (ng,ne*dim) :: F
    REAL (pr), DIMENSION (ng,(dim+Nspec)*2) :: v_prev ! velocity components + temperature
    REAL (pr), DIMENSION (ng) :: p, p_prev
    REAL (pr), DIMENSION (ng,3) :: props, props_prev
    REAL (pr), DIMENSION (ng) :: ReU

    CALL user_bufferRe (ReU, ng)

    int_Drhs = 0.0_pr
    vshift = dim+Nspec
    DO l=1,Nspecm
       v_prev(:,dim+1+l) = u_prev(:,nspc(l))/u_prev(:,nden)
       v_prev(:,vshift+dim+1+l) = u_p(:,nspc(l))/u_prev(:,nden) - u_prev(:,nspc(l))*u_p(:,nden)/u_prev(:,nden)**2
    END DO
    p_prev(:) = (1.0_pr-SUM(v_prev(:,dim+2:dim+Nspec),DIM=2))  !Y0_Nspec
    p(:)      =        -SUM(v_prev(:,vshift+dim+2:vshift+dim+Nspec),DIM=2)  !Y'_Nspec
    props_prev(:,1) = p_prev(:)*cp_in(Nspec) !cp0_Nspec
    props_prev(:,2) = p_prev(:)/MW_in(Nspec) !R0_Nspec
    props(:,1)      = p(:)*cp_in(Nspec) !cp'_Nspec
    props(:,2)      = p(:)/MW_in(Nspec) !R'_Nspec
    DO l=1,Nspecm
       props_prev(:,1) = props_prev(:,1) + v_prev(:,dim+1+l)*cp_in(l) !cp0
       props_prev(:,2) = props_prev(:,2) + v_prev(:,dim+1+l)/MW_in(l) !R0
       props(:,1)      = props(:,1)      + v_prev(:,vshift+dim+1+l)*cp_in(l) !cp'
       props(:,2)      = props(:,2)      + v_prev(:,vshift+dim+1+l)/MW_in(l) !R'
    END DO
    p_prev(:) = (u_prev(:,neng)-0.5_pr*SUM(u_prev(:,nvel(1):nvel(dim))**2,DIM=2)/u_prev(:,nden))/(props_prev(:,1)/props_prev(:,2)-1.0_pr)  !pressure
    p(:) = (u_prev(:,neng)-0.5_pr*SUM(u_prev(:,nvel(1):nvel(dim))**2,DIM=2)/u_prev(:,nden)) * (props_prev(:,1)*props(:,2)-props(:,1)*props_prev(:,2)) / (props_prev(:,1)-props_prev(:,2))**2 + &
           (u_p(:,neng)+0.5_pr*SUM(u_prev(:,nvel(1):nvel(dim))**2,DIM=2)/u_prev(:,nden)**2*u_p(:,nden)-SUM(u_prev(:,nvel(1):nvel(dim))*u_p(:,nvel(1):nvel(dim)),DIM=2)/u_prev(:,nden)) /  &
             (props_prev(:,1)/props_prev(:,2)-1.0_pr) 
    v_prev(:,dim+1) = p_prev(:)/props_prev(:,2)/u_prev(:,nden)  !Temperature
    v_prev(:,vshift+dim+1) = (p(:)-p_prev(:)*(props(:,2)/props_prev(:,2)+u_p(:,nden)/u_prev(:,nden)))/props_prev(:,2)/u_prev(:,nden)  !Temperature
    DO i = 1, dim
       v_prev(:,i) = u_prev(:,nvel(i)) / u_prev(:,nden) ! u_i
       v_prev(:,vshift+i) = u_p(:,nvel(i))/u_prev(:,nden) - u_prev(:,nvel(i))*u_p(:,nden)/u_prev(:,nden)**2  !u_i
    END DO
    IF ((doBC .eq. 0) .AND. ((methpd .ne. 0) .OR. splitFBpress) .AND. IMEXswitch .LE. 0) THEN  !IF doBC>0, these terms are done inside LODI subroutines
       du(1,:,1) = p(:)
       du(1,:,2) = p_prev(:)
       CALL dpdx (du(1,:,1),ng,v_prev(:,dim+2:dim+Nspec),2*methpd+meth,j_lev,splitFBpress)
       shift = (nvel(1)-1)*ng
       int_Drhs(shift+1:shift+ng) = int_Drhs(shift+1:shift+ng)-du(1,:,1)
       CALL dpdx (du(1,:,2),ng,v_prev(:,dim+2:dim+Nspec),2*methpd+meth,j_lev,splitFBpress)
       shift = (neng-1)*ng
       int_Drhs(shift+1:shift+ng) = int_Drhs(shift+1:shift+ng)-du(1,:,1)*v_prev(:,1)-du(1,:,2)*v_prev(:,vshift+1) !u0*dp'/dx+u'*dp0/dx
       CALL c_diff_fast(v_prev(:,vshift+1), du(1,:,:), d2u(1,:,:), j_lev, ng, meth, 10, 1, 1, 1) ! du'/dx
       int_Drhs(shift+1:shift+ng) = int_Drhs(shift+1:shift+ng)-du(1,:,1)*p_prev(:)   !p0*du'/dx
       CALL c_diff_fast(v_prev(:,1), du(1,:,:), d2u(1,:,:), j_lev, ng, meth, 10, 1, 1, 1) ! du0/dx
       int_Drhs(shift+1:shift+ng) = int_Drhs(shift+1:shift+ng)-du(1,:,1)*p(:)   !p'*du0/dx
    END IF
    !
    !--Form right hand side of Euler Equations
    !
    F(:,:) = 0.0_pr
  IF (IMEXswitch .LE. 0) THEN
    DO j = 1+doBC, dim
       jshift=ne*(j-1)
       F(:,nden+jshift) = F(:,nden+jshift) + u_p(:,nvel(j)) ! rho*u_j
       DO i = 1, dim ! (rho*u_i*u_j)'
          F(:,nvel(i)+jshift) = F(:,nvel(i)+jshift) + u_prev(:,nvel(i))*v_prev(:,vshift+j) + u_p(:,nvel(i))*v_prev(:,j)
       END DO
       F(:,nvel(j)+jshift) = F(:,nvel(j)+jshift) + p(:) ! (rho*u_i^2)'+p'
       F(:,neng+jshift) = F(:,neng+jshift) + (u_p(:,neng) + p(:))*v_prev(:,j) + (u_prev(:,neng) + p_prev(:))*v_prev(:,vshift+j)  ! ((e+p)*u_j)'
       DO l=1,Nspecm
          F(:,nspc(l)+jshift) = F(:,nspc(l)+jshift) + v_prev(:,vshift+dim+1+l)*u_prev(:,nvel(j))+v_prev(:,dim+1+l)*u_p(:,nvel(j))
       END DO
    END DO
    IF ((doBC .eq. 0) .AND. ((methpd .ne. 0) .OR. splitFBpress)) THEN  !IF doBC>0, these terms are done inside LODI subroutines
       F(:,nvel(1)) = F(:,nvel(1)) - p(:)
       F(:,neng)    = F(:,neng)    - p_prev(:)*v_prev(:,vshift+1) - p(:)*v_prev(:,1)
    END IF
    IF (doBC .ne. 0) THEN  !Missing term from LODI assumps... gamma=constant
       shift = (neng-1)*ng
       props_prev(:,3)=props_prev(:,1)/props_prev(:,2)  !gamma/(gamma-1)0
       CALL c_diff_fast(props_prev(:,3), du(1,:,:), d2u(1,:,:), j_lev, ng, meth, 10, 1, 1, 1)  !d(gamma/(gamma-1))/dx
       int_Drhs(shift+1:shift+ng) = int_Drhs(shift+1:shift+ng)-du(1,:,1)*(v_prev(:,1)*p(:)+v_prev(:,vshift+1)*p_prev(:)) !pu*d(gamma/(gamma-1))/dx
       props(:,3)=((props(:,1)*props_prev(:,2)-props_prev(:,1)*props(:,2))/props_prev(:,2)**2) !gamma/(gamma-1)'
       CALL c_diff_fast(props(:,3), du(1,:,:), d2u(1,:,:), j_lev, ng, meth, 10, 1, 1, 1)  !d(gamma/(gamma-1))/dx
       int_Drhs(shift+1:shift+ng) = int_Drhs(shift+1:shift+ng)-du(1,:,1)*v_prev(:,1)*p_prev(:) !pu*d(gamma/(gamma-1))/dx
    END IF
  END IF
    !
    !-- Form right hand side of Navier-Stokes Equations
    !
    IF(NS .AND. IMEXswitch .GE. 0) THEN
       tempintd = 2*vshift
       CALL c_diff_fast(v_prev(:,:), du(1:tempintd,:,:), d2u(1:tempintd,:,:), j_lev, ng, meth, 10, tempintd, 1, tempintd) ! du'_i/dx_j & dT'/dx_j
       p_prev(:) = (1.0_pr-SUM(v_prev(:,dim+2:dim+Nspec),DIM=2))  !Y0_Nspec
       p(:)      =        -SUM(v_prev(:,vshift+dim+2:vshift+dim+Nspec),DIM=2)  !Y'_Nspec
       props_prev(:,1) = p_prev(:)*mu_in(Nspec)  !mu0_Nspec
       props_prev(:,2) = p_prev(:)*kk_in(Nspec)  !kk0_Nspec
       props_prev(:,3) = p_prev(:)*bD_in(Nspec)  !bD0_Nspec
       props(:,1)      = p(:)*mu_in(Nspec)  !mu'_Nspec
       props(:,2)      = p(:)*kk_in(Nspec)  !kk'_Nspec
       props(:,3)      = p(:)*bD_in(Nspec)  !bD'_Nspec
       DO l=1,Nspecm
          props_prev(:,1) = props_prev(:,1) + v_prev(:,dim+1+l)*mu_in(l)  !mu0
          props_prev(:,2) = props_prev(:,2) + v_prev(:,dim+1+l)*kk_in(l)  !kk0
          props_prev(:,3) = props_prev(:,3) + v_prev(:,dim+1+l)*bD_in(l)  !bD0
          props(:,1)      = props(:,1)      + v_prev(:,vshift+dim+1+l)*mu_in(l)  !mu'
          props(:,2)      = props(:,2)      + v_prev(:,vshift+dim+1+l)*kk_in(l)  !kk'
          props(:,3)      = props(:,3)      + v_prev(:,vshift+dim+1+l)*bD_in(l)  !bD'
       END DO
       IF (kinevisc .GT. 0) THEN
          CALL user_buffkine (p_prev, u_prev(:,nden), ng)
          CALL user_buffkine (p,      u_p(:,nden),    ng)
          props_prev(:,1) = props_prev(:,1)*p_prev(:)
          props_prev(:,2) = props_prev(:,2)*p_prev(:)
          props(:,1) = props(:,1)*p_prev(:) + props_prev(:,1)*p(:) 
          props(:,2) = props(:,2)*p_prev(:) + props_prev(:,2)*p(:)
       END IF
       p(:) = du(vshift+1,:,1) !reuse of a variable
       DO i = 2, dim
          p(:) = p(:) + du(vshift+i,:,i) !du'_i/dx_i
       END DO
       IF (dervFlux==1) THEN
          CALL user_flxfunc (d2u(1:dim,:,1:dim), ng, j_lev)
       ELSE
          d2u(:,:,:) = 1.0_pr
       END IF
       DO j = 1, dim
          jshift=ne*(j-1)
          DO i = 1, dim
             F(:,nvel(i)+jshift) = F(:,nvel(i)+jshift) - ReU(:)*props_prev(:,1)*( du(vshift+i,:,j)+du(vshift+j,:,i) ) * d2u(i,:,j)  !rho*u_i*u_j - 2*mu*S_ij/Re
          END DO
          F(:,nvel(j)+jshift) = F(:,nvel(j)+jshift) + 2.0_pr/3.0_pr*ReU(:)*props_prev(:,1)*p(:) * d2u(j,:,j) !rho*u_i*u_j - tau_ij/Re  [ tau_ij = 2*mu*( S_ij - 2/3*S_kk*delta_ij ) ]
          DO i = 1, dim
             F(:,neng+jshift) =  F(:,neng+jshift) - props_prev(:,1)*ReU(:)*( du(vshift+i,:,j)+du(vshift+j,:,i) )*v_prev(:,i) * d2u(i,:,j)  !(e+p)*u_j - u_i*2*mu*S_ij/Re
          END DO
          F(:,neng+jshift) =  F(:,neng+jshift) + 2.0_pr/3.0_pr*props_prev(:,1)*ReU(:)*p(:)*v_prev(:,j) * d2u(j,:,j) - ReU(:)*props_prev(:,2)*du(vshift+dim+1,:,j) * d2u(2,:,j)  
                                                                                             !(e+p)*u_j - u_i*tau_ij/Re - q_j/Re/Pra [ q_j = mu dT/dx_j ]
          DO l=1,Nspecm
             F(:,neng+jshift) =  F(:,neng+jshift) - u_prev(:,nden)*cp_in(l)*v_prev(:,dim+1)*ReU(:)*props_prev(:,3)*du(vshift+dim+1+l,:,j) * d2u(2,:,j)
          END DO
          F(:,neng+jshift) =  F(:,neng+jshift) +  u_prev(:,nden)*cp_in(Nspec)*v_prev(:,dim+1)*ReU(:)*props_prev(:,3)*SUM(du(vshift+dim+2:vshift+dim+Nspec,:,j),DIM=1) * d2u(2,:,j)
          DO l=1,Nspecm
             F(:,nspc(l)+jshift) =  F(:,nspc(l)+jshift) - u_prev(:,nden)*ReU(:)*props_prev(:,3)*du(vshift+dim+1+l,:,j) * d2u(2,:,j)
          END DO
       END DO

       p_prev(:) = du(1,:,1) !reuse of a variable
       DO i = 2, dim
          p_prev(:) = p_prev(:) + du(i,:,i) !du0_i/dx_i
       END DO
       DO j = 1, dim
          jshift=ne*(j-1)
          DO i = 1, dim
             F(:,nvel(i)+jshift) = F(:,nvel(i)+jshift) - ReU(:)*props(:,1)*( du(i,:,j)+du(j,:,i) ) * d2u(i,:,j)  !rho*u_i*u_j - 2*mu*S_ij/Re
          END DO
          F(:,nvel(j)+jshift) = F(:,nvel(j)+jshift) + 2.0_pr/3.0_pr*ReU(:)*props(:,1)*p_prev(:) * d2u(j,:,j) !rho*u_i*u_j - tau_ij/Re  [ tau_ij = 2*mu*( S_ij - 2/3*S_kk*delta_ij ) ]
          DO i = 1, dim
             F(:,neng+jshift) =  F(:,neng+jshift) - ReU(:)*( du(i,:,j)+du(j,:,i) )*(v_prev(:,i)*props(:,1)+v_prev(:,vshift+i)*props_prev(:,1)) * d2u(i,:,j)  !(e+p)*u_j - u_i*2*mu*S_ij/Re
          END DO
          F(:,neng+jshift) =  F(:,neng+jshift) + 2.0_pr/3.0_pr*ReU(:)*p_prev(:)*(v_prev(:,j)*props(:,1)+v_prev(:,vshift+j)*props_prev(:,1)) * d2u(j,:,j) - ReU(:)*props(:,2)*du(dim+1,:,j) * d2u(2,:,j)  
                                                                                             !(e+p)*u_j - u_i*tau_ij/Re - q_j/Re/Pra [ q_j = mu dT/dx_j ]
          DO l=1,Nspecm
             F(:,neng+jshift) =  F(:,neng+jshift) - cp_in(l)*ReU(:)*du(dim+1+l,:,j) * &
                             ( u_p(:,nden)*v_prev(:,dim+1)*props_prev(:,3) + u_prev(:,nden)*(v_prev(:,vshift+dim+1)*props_prev(:,3) + v_prev(:,dim+1)*props(:,3)) ) * d2u(2,:,j)
          END DO
          F(:,neng+jshift) =  F(:,neng+jshift) +  cp_in(Nspec)*ReU(:)*SUM(du(dim+2:dim+Nspec,:,j),DIM=1) * &
                             ( u_p(:,nden)*v_prev(:,dim+1)*props_prev(:,3) + u_prev(:,nden)*(v_prev(:,vshift+dim+1)*props_prev(:,3) + v_prev(:,dim+1)*props(:,3)) ) * d2u(2,:,j)
          DO l=1,Nspecm
             F(:,nspc(l)+jshift) =  F(:,nspc(l)+jshift) - ReU(:)*(u_p(:,nden)*props_prev(:,3)+u_prev(:,nden)*props(:,3))*du(dim+1+l,:,j) * d2u(2,:,j)
          END DO
       END DO
    END IF
    IF (GRAV .AND. IMEXswitch .LE. 0) THEN
      shift = (neng-1)*ng
      DO j=1,dim
         DO l=1,Nspecm
            int_Drhs(shift+1:shift+ng) = int_Drhs(shift+1:shift+ng)-(v_prev(:,dim+1+l)*u_p(:,nvel(j))+v_prev(:,vshift+dim+1+l)*u_prev(:,nvel(j)))*gr_in(l+Nspec*(j-1))
         END DO
         int_Drhs(shift+1:shift+ng) = int_Drhs(shift+1:shift+ng) - ((1.0_pr-SUM(v_prev(:,dim+2:dim+Nspec),DIM=2))*u_p(:,nvel(j))-SUM(v_prev(:,vshift+dim+2:vshift+dim+Nspec),DIM=2)*u_prev(:,nvel(j)) )*gr_in(Nspec*j)
      END DO
    END IF
    tempintd=ne*dim
    CALL c_diff_fast(F(:,1:tempintd), du(1:tempintd,:,:), d2u(1:tempintd,:,:), j_lev, ng, meth, 10, tempintd, 1, tempintd)  !du(i,:,k)=dF_ij/dx_k

    DO j = 1, dim
       jshift=ne*(j-1)
       DO i = 1, dim+1+Nspec
          shift=(i-1)*ng
          int_Drhs(shift+1:shift+ng) = int_Drhs(shift+1:shift+ng) - du(i+jshift,:,j)  ! -dF_ij/dx_j
       END DO
       IF (GRAV .AND. IMEXswitch .LE. 0) THEN
          shift = (nvel(j)-1)*ng
          DO l=1,Nspecm        
             int_Drhs(shift+1:shift+ng) = int_Drhs(shift+1:shift+ng) - gr_in(l+Nspec*(j-1))*u_p(:,nspc(l))
          END DO
          int_Drhs(shift+1:shift+ng) = int_Drhs(shift+1:shift+ng) - gr_in(Nspec*j)*(u_p(:,nden)-SUM(u_p(:,nspc(1):nspc(Nspecm)),DIM=2))
       END IF
    END DO
    IF (NS .AND. dervFlux==2 .AND. IMEXswitch .GE. 0) THEN
       tempintd = 2*vshift
       CALL c_diff_fast(v_prev(:,:), du(1:tempintd,:,:), d2u(1:tempintd,:,:), j_lev, ng, meth, 10, tempintd, 1, tempintd) ! du'_i/dx_j & dT'/dx_j
       DO i = 2, dim
          F(:,i-1) = ReU(:)*props_prev(:,1)*( du(vshift+i,:,1)+du(vshift+1,:,i) )  !2*mu*S_ij/Re
       END DO
       F(:,dim) = ReU(:)*props_prev(:,2)*du(vshift+dim+1,:,1)  !q_j/Re/Pra [ q_j = mu dT/dx_j ]
       DO l=1,Nspecm
          F(:,dim+l) =  u_prev(:,nden)*ReU(:)*props_prev(:,3)*du(vshift+dim+1+l,:,1)
       END DO
       F(:,dim+Nspec) =  -u_prev(:,nden)*ReU(:)*props_prev(:,3)*SUM(du(vshift+dim+2:vshift+dim+Nspec,:,1),DIM=1)

       DO i = 2, dim
          F(:,i-1) = F(:,i-1) + ReU(:)*props(:,1)*( du(i,:,1)+du(1,:,i) )  !2*mu*S_ij/Re
       END DO
       F(:,dim) = F(:,dim) + ReU(:)*props(:,2)*du(dim+1,:,1)  !q_j/Re/Pra [ q_j = mu dT/dx_j ]
       DO l=1,Nspecm
          F(:,dim+l) = F(:,dim+l) + ReU(:)*(u_prev(:,nden)*props(:,3)+u_p(:,nden)*props_prev(:,3))*du(dim+1+l,:,1)
       END DO
       F(:,dim+Nspec) =  F(:,dim+Nspec) - ReU(:)*(u_prev(:,nden)*props(:,3)+u_p(:,nden)*props_prev(:,3))*SUM(du(dim+2:dim+Nspec,:,1),DIM=1)
       DO i = 2, dim
          F(:,i-1+ne) = ReU(:)*props_prev(:,1)*( du(i,:,1)+du(1,:,i) )  !2*mu*S_ij/Re
       END DO
       F(:,dim+ne) = ReU(:)*props_prev(:,2)*du(dim+1,:,1)  !q_j/Re/Pra [ q_j = mu dT/dx_j ]
       DO l=1,Nspecm
          F(:,dim+l+ne) = ReU(:)*u_prev(:,nden)*props_prev(:,3)*du(dim+1+l,:,1)
       END DO
       F(:,dim+Nspec+ne) = -ReU(:)*u_prev(:,nden)*props_prev(:,3)*SUM(du(dim+2:dim+Nspec,:,1),DIM=1)

       tempintd = 2*vshift
       CALL c_diff_fast(F(:,1:tempintd), du(1:tempintd,:,:), d2u(1:tempintd,:,:), j_lev, ng, meth, 10, tempintd, 1, tempintd)
       CALL user_flxfunc (d2u(1:dim,:,1:dim), ng, j_lev)
       d2u(1:dim,:,1:dim) = 1.0_pr - d2u(1:dim,:,1:dim)
       DO i=2,dim
          shift=(nvel(i)-1)*ng
          int_Drhs(shift+1:shift+ng) = int_Drhs(shift+1:shift+ng) - du(i-1,:,1)*d2u(2,:,1)
       END DO
       shift=(neng-1)*ng
       DO i=2,dim
          int_Drhs(shift+1:shift+ng) = int_Drhs(shift+1:shift+ng) - du(i-1,:,1)*v_prev(:,i)*d2u(2,:,1)
       END DO
       int_Drhs(shift+1:shift+ng) = int_Drhs(shift+1:shift+ng) - du(dim,:,1)*d2u(2,:,1)
       DO l=1,Nspec
          int_Drhs(shift+1:shift+ng) = int_Drhs(shift+1:shift+ng) - v_prev(:,dim+1)*cp_in(l)*du(dim+l,:,1)*d2u(2,:,1)
       END DO
       DO l=1,Nspecm
          shift=(nspc(l)-1)*ng 
          int_Drhs(shift+1:shift+ng) = int_Drhs(shift+1:shift+ng) - du(dim+l,:,1)*d2u(2,:,1)
       END DO

       shift=(neng-1)*ng
       DO i=2,dim
          int_Drhs(shift+1:shift+ng) = int_Drhs(shift+1:shift+ng) - du(i-1+ne,:,1)*v_prev(:,vshift+i)*d2u(2,:,1)
       END DO
       DO l=1,Nspec
          int_Drhs(shift+1:shift+ng) = int_Drhs(shift+1:shift+ng) - v_prev(:,vshift+dim+1)*cp_in(l)*du(dim+l+ne,:,1)*d2u(2,:,1)
       END DO
    END IF
  END SUBROUTINE internal_Drhs

  SUBROUTINE internal_Drhs_diag (int_diag,u_prev,meth,doBC,addingon)
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: meth
    REAL (pr), DIMENSION (ng*ne), INTENT(INOUT) :: int_diag
    REAL (pr), DIMENSION (ng,ne), INTENT(IN) :: u_prev
    INTEGER, INTENT(IN) :: doBC !if zero do full rhs, if one no x-derivatives 
    INTEGER, INTENT(IN) :: addingon 

    INTEGER :: i, j, l, ll, ie, shift
    REAL (pr), DIMENSION (ng,dim) :: du_diag, d2u_diag
    REAL (pr), DIMENSION (Nspec,ng,dim) :: du, d2u
    REAL (pr), DIMENSION (ng,n_integrated,dim) :: F
    REAL (pr), DIMENSION (ng, Nspec) :: v_prev
    REAL (pr), DIMENSION (ng) :: ReU
    REAL (pr), DIMENSION (ng,6) :: props_prev

    CALL user_bufferRe (ReU, ng)

    int_diag = 0.0_pr
    DO l=1,Nspecm
       v_prev(:,l) = u_prev(:,nspc(l))/u_prev(:,nden)  ! Y0
    END DO
    v_prev(:,Nspec) = 1.0_pr-SUM(v_prev(:,1:Nspecm),DIM=2)  !Y0_Nspec
    props_prev(:,:)=0.0_pr
    DO l=1,Nspec
       props_prev(:,1) = props_prev(:,1) + v_prev(:,l)*cp_in(l)  !cp0
       props_prev(:,2) = props_prev(:,2) + v_prev(:,l)/MW_in(l)  !R0
       props_prev(:,4) = props_prev(:,4) + v_prev(:,l)*mu_in(l)  !mu0
       props_prev(:,5) = props_prev(:,5) + v_prev(:,l)*kk_in(l)  !kk0
       props_prev(:,6) = props_prev(:,6) + v_prev(:,l)*bD_in(l)  !bD0
    END DO
    IF (kinevisc .GT. 0) THEN
       CALL user_buffkine (props_prev(:,3), u_prev(:,nden), ng)
       props_prev(:,4) = props_prev(:,4)*props_prev(:,3)
       props_prev(:,5) = props_prev(:,5)*props_prev(:,3)
    END IF
    props_prev(:,3) = props_prev(:,1)/(props_prev(:,1)-props_prev(:,2)) !gamma0 = cp0/(cp0-R0)
    IF ((doBC .eq. 0) .AND. ((methpd .ne. 0) .OR. splitFBpress) .AND. IMEXswitch .LE. 0) THEN  !IF doBC>0, these terms are done inside LODI subroutines
       CALL c_diff_fast(u_prev(:,nvel(1))/u_prev(:,nden), du(1,:,:), d2u(1,:,:), j_lev, ng, meth, 10, 1, 1, 1) ! du0/dx
       CALL dpdx_diag (du_diag(:,1),ng,v_prev(:,1:Nspecm),2*methpd+meth,j_lev,splitFBpress)
       shift = (nvel(1)-1)*ng
       int_diag(shift+1:shift+ng) = int_diag(shift+1:shift+ng) + du_diag(:,1)*u_prev(:,nvel(1))/u_prev(:,nden)*(props_prev(:,3)-1.0_pr)  !pd_rhou*diagFB_x
       shift = (neng-1)*ng
       int_diag(shift+1:shift+ng) = int_diag(shift+1:shift+ng) - (du(1,:,1) + du_diag(:,1)*u_prev(:,nvel(1))/u_prev(:,nden))*(props_prev(:,3)-1.0_pr) !pd_rhoe*(du0/dx+u0*diagFB_x)
    END IF
    CALL c_diff_diag(du_diag, d2u_diag, j_lev, ng, meth, meth, -11)
    !
    !--Form right hand side of Euler Equations
    !
    F(:,:,:) = 0.0_pr
  IF (IMEXswitch .LE. 0) THEN
    DO j = 1+doBC, dim
       DO i = 1, dim ! (rho*u_i*u_j)'
          F(:,nvel(i),j) = F(:,nvel(i),j) + u_prev(:,nvel(j))/u_prev(:,nden)
       END DO
       F(:,nvel(j),j) = F(:,nvel(j),j) + u_prev(:,nvel(j))/u_prev(:,nden)*(2.0_pr-props_prev(:,3))
       F(:,neng,j) = F(:,neng,j) + props_prev(:,3)*u_prev(:,nvel(j))/u_prev(:,nden)
       DO l=1,Nspecm
          F(:,nspc(l),j) = F(:,nspc(l),j) + u_prev(:,nvel(j))/u_prev(:,nden)
       END DO
    END DO
    IF ((doBC .eq. 0) .AND. ((methpd .ne. 0) .OR. splitFBpress)) THEN  !IF doBC>0, these terms are done inside LODI subroutines
       F(:,nvel(1),1) = F(:,nvel(1),1) + u_prev(:,nvel(j))/u_prev(:,nden)*(props_prev(:,3)-1.0_pr)
       F(:,neng,1) = F(:,neng,1) - (props_prev(:,3)-1.0_pr)*u_prev(:,nvel(1))/u_prev(:,nden)
    END IF
    IF (doBC .ne. 0) THEN  !Missing term from LODI assumps... gamma=constant
       shift = (neng-1)*ng
       CALL c_diff_fast(props_prev(:,1)/props_prev(:,2), du(1,:,:), d2u(1,:,:), j_lev, ng, meth, 10, 1, 1, 1)  !d(gamma/(gamma-1))/dx
       int_diag(shift+1:shift+ng) = int_diag(shift+1:shift+ng)-du(1,:,1)*u_prev(:,nvel(1))/u_prev(:,nden)*(props_prev(:,3)-1.0_pr) !pu*d(gamma/(gamma-1))/dx
    END IF
  END IF
    !
    !-- Form right hand side of Navier-Stokes Equations
    !
    IF(NS .AND. NSdiag .AND. IMEXswitch .GE. 0) THEN
       CALL c_diff_fast(v_prev, du(1:Nspec,:,:), d2u(1:Nspec,:,:), j_lev, ng, meth, 10, Nspec, 1, Nspec) ! dY_l/dx_j
       IF (dervFlux==1) THEN
          CALL user_flxfunc (d2u(1:dim,:,1:dim), ng, j_lev)
       ELSE
          d2u(:,:,:) = 1.0_pr
       END IF
       DO j = 1, dim
          DO l=1,Nspec
             F(:,neng,j) =  F(:,neng,j) - ReU(:)*props_prev(:,6)*du(l,:,j)*cp_in(l)/(props_prev(:,1)-props_prev(:,2))*d2u(2,:,j)
          END DO 
       END DO
       DO j = 1, dim
          DO i = 1, dim
             shift=(nvel(i)-1)*ng
             int_diag(shift+1:shift+ng) = int_diag(shift+1:shift+ng) + props_prev(:,4)*ReU(:)/u_prev(:,nden)*d2u_diag(:,j) * d2u(i,:,j)
          END DO
          shift=(nvel(j)-1)*ng
          int_diag(shift+1:shift+ng) = int_diag(shift+1:shift+ng) + props_prev(:,4)*ReU(:)/u_prev(:,nden)*d2u_diag(:,j)/3.0_pr * d2u(j,:,j)
          shift=(neng-1)*ng
          int_diag(shift+1:shift+ng) = int_diag(shift+1:shift+ng) + props_prev(:,5)*ReU(:)/u_prev(:,nden)/(props_prev(:,1)-props_prev(:,2))*d2u_diag(:,j) * d2u(2,:,j)
          DO l = 1, Nspecm
             shift=(nspc(l)-1)*ng
             int_diag(shift+1:shift+ng) = int_diag(shift+1:shift+ng) + ReU(:)*(props_prev(:,6)*d2u_diag(:,j)+bD_in(l)*du(l,:,j)) * d2u(2,:,j)
          END DO
       END DO
    END IF
    DO j = 1, dim
       DO i = 1, dim+1+Nspec
          shift=(i-1)*ng
          int_diag(shift+1:shift+ng) = int_diag(shift+1:shift+ng) - F(:,i,j)*du_diag(:,j)
       END DO
    END DO

    IF (NS .AND. NSdiag .AND. dervFlux==2 .AND. IMEXswitch .GE. 0) THEN
       CALL user_flxfunc (d2u(1:dim,:,1:dim), ng, j_lev)
       d2u(1:dim,:,1:dim) = 1.0_pr - d2u(1:dim,:,1:dim)
       DO i = 2, dim
          shift=(nvel(i)-1)*ng
          int_diag(shift+1:shift+ng) = int_diag(shift+1:shift+ng) - props_prev(:,4)*ReU(:)/u_prev(:,nden)*d2u_diag(:,1) * d2u(2,:,1)
       END DO
       DO l = 1, Nspecm
          shift=(nspc(l)-1)*ng
          int_diag(shift+1:shift+ng) = int_diag(shift+1:shift+ng) - ReU(:)*(props_prev(:,6)*d2u_diag(:,1)+bD_in(l)*du(l,:,1)) * d2u(2,:,1)
       END DO
       shift=(neng-1)*ng
       int_diag(shift+1:shift+ng) = int_diag(shift+1:shift+ng) - props_prev(:,5)*ReU(:)/u_prev(:,nden)/(props_prev(:,1)-props_prev(:,2))*d2u_diag(:,1) * d2u(2,:,1)
       DO l = 1, Nspec
          v_prev(:,l) = ReU(:)*du(l,:,1)*props_prev(:,6)*u_prev(:,nden)
       END DO
       CALL c_diff_fast(v_prev, du(1:Nspec,:,:), d2u(1:Nspec,:,:), j_lev, ng, meth, 10, Nspec, 1, Nspec) ! d/dx_j(rho0*D0*dY_l/dx)
       DO l = 1, Nspec
          int_diag(shift+1:shift+ng) = int_diag(shift+1:shift+ng) - cp_in(l)/u_prev(:,nden)/(props_prev(:,1)-props_prev(:,2))*du(l,:,1) * d2u(2,:,1)
       END DO
    END IF
  END SUBROUTINE internal_Drhs_diag

  FUNCTION user_rhs (u_integrated,scalar)
    IMPLICIT NONE
    REAL (pr), DIMENSION (ng,ne), INTENT(IN) :: u_integrated !1D flat version of integrated variables without BC 
    REAL (pr), DIMENSION (ng), INTENT(IN) :: scalar
    REAL (pr), DIMENSION (ng*ne) :: user_rhs
    INTEGER, PARAMETER :: meth=1
    IF (evolBC) THEN
       CALL internal_rhs(user_rhs,u_integrated,0,1)
    ELSE
       CALL internal_rhs(user_rhs,u_integrated,0,0)
    END IF

    IF (evolBC .AND. (IMEXswitch .LE. 0)) CALL user_evol_LODI_BC_rhs (user_rhs, u_integrated, ne, ng, j_lev, meth)
    IF (diffBC .AND. (IMEXswitch .LE. 0)) CALL user_diffusionzone_BC_rhs (user_rhs, u_integrated, ne, ng, j_lev, meth)
    IF (cnvtBC .AND. (IMEXswitch .LE. 0)) CALL user_convectzone_BC_rhs (user_rhs, u_integrated, ne, ng, j_lev, meth)
    IF (buffBC .AND. (IMEXswitch .LE. 0)) CALL user_bufferzone_BC_rhs (user_rhs, u_integrated, ne, ng, j_lev, meth)

   IF(hypermodel .NE. 0) CALL hyperbolic(u_integrated,ng,user_rhs)

  END FUNCTION user_rhs

  FUNCTION user_Drhs (u_p, u_prev, meth)
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: meth
    REAL (pr), DIMENSION (ng,ne) :: u_p, u_prev
    REAL (pr), DIMENSION (ng*ne) :: user_Drhs
    IF (evolBC) THEN
       CALL internal_Drhs(user_Drhs,u_p,u_prev,meth,0,1)
    ELSE
       CALL internal_Drhs(user_Drhs,u_p,u_prev,meth,0,0)
    END IF

    IF (evolBC .AND. (IMEXswitch .LE. 0)) CALL user_evol_LODI_BC_Drhs (user_Drhs, u_p, u_prev, ne, ng, j_lev, meth)
    IF (diffBC .AND. (IMEXswitch .LE. 0)) CALL user_diffusionzone_BC_Drhs (user_Drhs, u_p, u_prev, ne, ng, j_lev, meth)
    IF (cnvtBC .AND. (IMEXswitch .LE. 0)) CALL user_convectzone_BC_Drhs (user_Drhs, u_p, u_prev, ne, ng, j_lev, meth)
    IF (buffBC .AND. (IMEXswitch .LE. 0)) CALL user_bufferzone_BC_Drhs (user_Drhs, u_p, u_prev, ne, ng, j_lev, meth)

    IF (hypermodel .NE. 0 ) CALL hyperbolic(u_p,ng,user_Drhs)
  END FUNCTION user_Drhs

  FUNCTION user_Drhs_diag (meth)
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: meth
    REAL (pr), DIMENSION (ng*ne) :: user_Drhs_diag
    REAL (pr), DIMENSION (ng,ne) :: u_prev
    INTEGER :: ie, shift
    DO ie = 1, ne
       shift=(ie-1)*ng
       u_prev(1:ng,ie) = u_prev_timestep(shift+1:shift+ng)
    END DO

    IF (evolBC) THEN
       CALL internal_Drhs_diag(user_Drhs_diag,u_prev,meth,0,1)
    ELSE
       CALL internal_Drhs_diag(user_Drhs_diag,u_prev,meth,0,0)
    END IF

    IF (evolBC .AND. (IMEXswitch .LE. 0)) CALL user_evol_LODI_BC_Drhs_diag (user_Drhs_diag, u_prev, ne, ng, j_lev, meth)
    IF (diffBC .AND. (IMEXswitch .LE. 0)) CALL user_diffusionzone_BC_Drhs_diag (user_Drhs_diag, u_prev, ne, ng, j_lev, meth)
    IF (cnvtBC .AND. (IMEXswitch .LE. 0)) CALL user_convectzone_BC_Drhs_diag (user_Drhs_diag, u_prev, ne, ng, j_lev, meth)
    IF (buffBC .AND. (IMEXswitch .LE. 0)) CALL user_bufferzone_BC_Drhs_diag (user_Drhs_diag, u_prev, ne, ng, j_lev, meth)

    IF(hypermodel .NE. 0 ) CALL hyperbolic_diag(user_Drhs_diag, ng)

  END FUNCTION user_Drhs_diag

  SUBROUTINE user_project (uin, p, nlocal, meth)
    IMPLICIT NONE

    INTEGER, INTENT (IN) :: meth, nlocal
    REAL (pr), DIMENSION (nlocal),     INTENT(INOUT) :: p
    REAL (pr), DIMENSION (nlocal,n_integrated), INTENT(INOUT) :: uin

    INTEGER :: i
    INTEGER, PARAMETER :: ne = 1
    INTEGER, DIMENSION(ne) :: clip 
    !no projection for compressible flow
  END SUBROUTINE user_project

  FUNCTION Laplace (jlev, uin, nlocal, ne_local, meth)
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: jlev, nlocal, ne_local, meth
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (INOUT) :: uin
    REAL (pr), DIMENSION (nlocal*ne_local) :: Laplace

    INTEGER :: i, ii, ie, shift, meth1, meth2, idim
    REAL (pr), DIMENSION (ne_local ,nlocal,dim) :: du
    REAL (pr), DIMENSION (ne_local*dim ,nlocal,dim) :: d2u
    REAL (pr), DIMENSION (ne_local*dim ,nlocal,dim) :: du_dummy ! passed when only calculating 1st derivative.
    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc
    
    Laplace=0.0_pr
  END FUNCTION Laplace

  FUNCTION Laplace_diag (jlev,  nlocal, ne_local, meth)
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: jlev, nlocal, ne_local, meth
    REAL (pr), DIMENSION (nlocal*ne_local) :: Laplace_diag

    INTEGER :: i, ii, ie, shift, meth1, meth2
    REAL (pr), DIMENSION (nlocal,dim) :: du, d2u
    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc

    Laplace_diag=0.0_pr
  END FUNCTION Laplace_diag

  FUNCTION Laplace_rhs(uin, nlocal, ne_local, meth)
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: nlocal, ne_local, meth
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (INOUT) :: uin
    REAL (pr), DIMENSION (nlocal) :: Laplace_rhs

    INTEGER :: i, ii, ie, meth1, meth2, idim
    REAL (pr), DIMENSION (ne_local ,nlocal,dim) :: du
    REAL (pr), DIMENSION (ne_local*dim ,nlocal,dim) :: d2u
    REAL (pr), DIMENSION (ne_local*dim ,nlocal,dim) :: du_dummy ! passed when only calculating 1st derivative.
    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc

    Laplace_rhs = 0.0_pr 
  END FUNCTION Laplace_rhs

  !
  ! Calculate any statitics
  !
  ! startup_flag - 0 when adapting to IC,then 1 inmain integration loop
  !
  SUBROUTINE user_stats ( uin, j_mn_local, startup_flag)
    IMPLICIT NONE
    REAL (pr), DIMENSION (nwlt,1:n_var), INTENT (IN) :: uin
    INTEGER , INTENT (IN) :: j_mn_local 
    INTEGER , INTENT (IN) :: startup_flag
    INTEGER :: i, minzloc, mynwlt_global
    REAL (pr) :: minzdiff, zdiff, Xval, layerwidth, Xtopg, Xbotg, Xdelta, zdiffsp, zdiffsm
    REAL (pr) :: VolXtop, VolXbot, VolXmix, VolXtot, VolYtop, VolYbot, VolYmix, VolYtot, VolDtop, VolDbot, VolDmix, VolDtot, VolVtop, VolVbot, VolVmix, VolVtot, totvol
    REAL (pr) :: Velb, Vels, DragCoef, Rebm, Rebd, Rebk, Resm, Resd, Resk, Recm, Recd, Reck, Revm, Revd, Revk, Rerm, Rerd, Rerk, Rehm, Rehd, Rehk, Relm, Reld, Relk 
    REAL (pr), DIMENSION(nwlt) :: kinep, Xvals, TopVals, BotVals, DiffsTop, DiffsBot, VolsTop, VolsBot, VolsNot 
    REAL (pr), DIMENSION(nwlt,2) :: Xvalstopbot, zdiffs, tbspots, mixspots, diffspots
    REAL (pr), DIMENSION(dim,nwlt) :: h_arr
    REAL (pr), DIMENSION(2,nwlt,dim) :: du, d2u    
    INTEGER :: methc, methf, methb, diffint, diffintz
    INTEGER, PARAMETER :: meth=1
    CHARACTER(LEN=8):: numstrng
    CHARACTER(LEN=100):: commandstrng
    REAL (pr) :: nwltper, nwltcomp, nwltpermx, nwltcompmx
    LOGICAL :: dodiffhere
    REAL (pr), DIMENSION(15) :: minvals, maxvals, avevals
    REAL (pr), DIMENSION(dim) :: delxyz
    INTEGER :: curxind, centiter
    REAL (pr) :: curxrho, curxrhoY, curxY, curxX
    REAL (pr) :: curxcrho, curxcrhoY, curxcY, curxcX, diffdamp

    olddifftop = difftop
    olddiffbot = diffbot
    difftop = -Lxyz(1) 
    diffbot =  Lxyz(1) 
 
    minvals = 0.0_pr
    maxvals = 0.0_pr
    avevals = 0.0_pr
    delxyz(1:dim)=(xyzlimits(2,1:dim)-xyzlimits(1,1:dim))/REAL(mxyz(1:dim)*2**(j_mx-1),pr)  !delxyz = L/N in every direction on j_mx
 
    DO i=1,nwlt
       du(1,i,1) = SUM(uin(i,nspc(1):nspc(Nspecm))/MW_in(1:Nspecm)) + (uin(i,nden)-SUM(uin(i,nspc(1):nspc(Nspecm))))/MW_in(Nspec) !rhoR
    END DO

    du(1,:,2) = uin(:,nspc(1))/MW_in(1)/du(1,:,1) !X
    minvals(1) =MINVAL(uin(:,nden))
    minvals(2) =MINVAL(uin(:,nvel(1)))
    minvals(3) =MINVAL(uin(:,nvel(2)))
    IF (dim==3) minvals(4) =MINVAL(uin(:,nvel(3)))
    minvals(5) =MINVAL(uin(:,nvel(1))/uin(:,nden))
    minvals(6) =MINVAL(uin(:,nvel(2))/uin(:,nden))
    IF (dim==3) minvals(7) =MINVAL(uin(:,nvel(3))/uin(:,nden))
    minvals(8) =MINVAL(SUM(uin(:,nvel(1):nvel(dim))**2,DIM=2))
    minvals(9) =MINVAL(uin(:,neng))
    minvals(10)=MINVAL(uin(:,neng)/uin(:,nden))
    minvals(11)=MINVAL(uin(:,nspc(1)))
    minvals(12)=MINVAL(uin(:,nspc(1))/uin(:,nden))
    minvals(13)=MINVAL(du(1,:,2))
    minvals(14)=MINVAL(uin(:,nprs))
    minvals(15)=MINVAL(uin(:,nprs)/du(1,:,1))
 
    maxvals(1) =MAXVAL(uin(:,nden))
    maxvals(2) =MAXVAL(uin(:,nvel(1)))
    maxvals(3) =MAXVAL(uin(:,nvel(2)))
    IF (dim==3) maxvals(4) =MAXVAL(uin(:,nvel(3)))
    maxvals(5) =MAXVAL(uin(:,nvel(1))/uin(:,nden))
    maxvals(6) =MAXVAL(uin(:,nvel(2))/uin(:,nden))
    IF (dim==3) maxvals(7) =MAXVAL(uin(:,nvel(3))/uin(:,nden))
    maxvals(8) =MAXVAL(SUM(uin(:,nvel(1):nvel(dim))**2,DIM=2))
    maxvals(9) =MAXVAL(uin(:,neng))
    maxvals(10)=MAXVAL(uin(:,neng)/uin(:,nden))
    maxvals(11)=MAXVAL(uin(:,nspc(1)))
    maxvals(12)=MAXVAL(uin(:,nspc(1))/uin(:,nden))
    maxvals(13)=MAXVAL(du(1,:,2))
    maxvals(14)=MAXVAL(uin(:,nprs))
    maxvals(15)=MAXVAL(uin(:,nprs)/du(1,:,1))
   
    avevals(1) =SUM(dA*uin(:,nden))
    avevals(2) =SUM(dA*uin(:,nvel(1)))
    avevals(3) =SUM(dA*uin(:,nvel(2)))
    IF (dim==3) avevals(4) =SUM(dA*uin(:,nvel(3)))
    avevals(5) =SUM(dA*uin(:,nvel(1))/uin(:,nden))
    avevals(6) =SUM(dA*uin(:,nvel(2))/uin(:,nden))
    IF (dim==3) avevals(7) =SUM(dA*uin(:,nvel(3))/uin(:,nden))
    avevals(8) =SUM(dA*SUM(uin(:,nvel(1):nvel(dim))**2,DIM=2))
    avevals(9) =SUM(dA*uin(:,neng))
    avevals(10)=SUM(dA*uin(:,neng)/uin(:,nden))
    avevals(11)=SUM(dA*uin(:,nspc(1)))
    avevals(12)=SUM(dA*uin(:,nspc(1))/uin(:,nden))
    avevals(13)=SUM(dA*du(1,:,2))
    avevals(14)=SUM(dA*uin(:,nprs))
    avevals(15)=SUM(dA*uin(:,nprs)/du(1,:,1))
   
    DO i=1,15
       CALL parallel_global_sum(REALMAXVAL=maxvals(i))
       CALL parallel_global_sum(REALMINVAL=minvals(i))
       CALL parallel_global_sum(REAL      =avevals(i))
       avevals(i)=avevals(i)/sumdA_global
    END DO

    mynwlt_global = nwlt
    CALL parallel_global_sum (INTEGER=mynwlt_global)
    nwltper = REAL(mynwlt_global,pr)
    DO i = 1,dim
       nwltper=nwltper/REAL(mxyz(i)*2**(j_lev-1)+1-prd(i))
    END DO
    nwltcomp = 100.0_pr*(1.0_pr-nwltper)
    nwltpermx = REAL(mynwlt_global,pr)
    DO i = 1,dim
       nwltpermx=nwltpermx/REAL(mxyz(i)*2**(j_mx-1)+1-prd(i))
    END DO
    nwltcompmx = 100.0_pr*(1.0_pr-nwltpermx)

    methc = meth
    methf = meth + BIASING_FORWARD
    methb = meth + BIASING_BACKWARD

    CALL get_all_local_h (h_arr)

    Xtopg = (1.0_pr-layerthresh)*(1.0_pr-peakchange)+layerthresh*peakchange
    Xbotg = layerthresh*(1.0_pr-peakchange)+(1.0_pr-layerthresh)*peakchange
          
    Xdelta = SQRT(layerthresh*(1.0_pr-2.0_pr*peakchange))
          
    Xvals(:) = ( uin(:,nden) - SUM(uin(:,nspc(1):nspc(Nspecm)),DIM=2) ) / MW_in(Nspec)
    DO i=1,Nspecm
       Xvals(:) = Xvals(:) + uin(:,nspc(i))/MW_in(i)
    END DO
    Xvals(:) = uin(:,nspc(1))/MW_in(1)/Xvals(:)
    Xvalstopbot(:,1) = (Xvals(:)-Xtopg)/((Xvals(:)-Xtopg)**2 + Xdelta**2)
    Xvalstopbot(:,2) = (Xbotg-Xvals(:))/((Xvals(:)-Xbotg)**2 + Xdelta**2)
    CALL c_diff_fast(Xvalstopbot(:,1:2), du(1:2,:,:), d2u(1:2,:,:), j_lev, nwlt, methc, 11, 2, 1, 2) 

    zdiffs(:,1) =   du(1,:,1)**2 - 2.0_pr*Xvalstopbot(:,1)*d2u(1,:,1)
    zdiffs(:,2) =   du(2,:,1)**2 - 2.0_pr*Xvalstopbot(:,2)*d2u(2,:,1)

    tbspots(:,1) = -Lxyz(1)
    tbspots(:,2) =  Lxyz(1)
    DO i=1,nwlt
       IF (ABS(Xvals(i)-Xtopg) .LT. Xdelta) THEN
          IF ((ABS(d2u(1,i,1)) .ge. 1.0e-12_pr) .AND. (zdiffs(i,1) .ge. 0.0_pr)) THEN
             zdiffsp = (-du(1,i,1)+SQRT(zdiffs(i,1)))/d2u(1,i,1)
             zdiffsm = (-du(1,i,1)-SQRT(zdiffs(i,1)))/d2u(1,i,1)
             IF (ABS(zdiffsm) > ABS(zdiffsp)) THEN
                IF (ABS(zdiffsp)<h_arr(1,i)) tbspots(i,1) = x(i,1) + zdiffsp
             ELSE
                IF (ABS(zdiffsm)<h_arr(1,i)) tbspots(i,1) = x(i,1) + zdiffsm
             END IF
          ELSEIF ((ABS(d2u(1,i,1)) .lt. 1.0e-12_pr) .AND. (ABS(du(1,i,1)) .ge. 1.0e-12_pr)) THEN
             zdiffsp = -Xvalstopbot(i,1)/du(1,i,1)
             IF (ABS(zdiffsp)<h_arr(1,i)) tbspots(i,1) = x(i,1) + zdiffsp
          END IF
       END IF

       IF (ABS(Xvals(i)-Xbotg) .LT. Xdelta) THEN
          IF ((ABS(d2u(2,i,1)) .ge. 1.0e-12_pr) .AND. (zdiffs(i,2) .ge. 0.0_pr)) THEN
             zdiffsp = (-du(2,i,1)+SQRT(zdiffs(i,2)))/d2u(2,i,1)
             zdiffsm = (-du(2,i,1)-SQRT(zdiffs(i,2)))/d2u(2,i,1)
             IF (ABS(zdiffsm) > ABS(zdiffsp)) THEN
                IF (ABS(zdiffsp)<h_arr(1,i)) tbspots(i,2) = x(i,1) + zdiffsp
             ELSE
                IF (ABS(zdiffsm)<h_arr(1,i)) tbspots(i,2) = x(i,1) + zdiffsm
             END IF
          ELSEIF ((ABS(d2u(2,i,1)) .lt. 1.0e-12_pr) .AND. (ABS(du(2,i,1)) .ge. 1.0e-12_pr)) THEN
             zdiffsp = -Xvalstopbot(i,2)/du(2,i,1)
             IF (ABS(zdiffsp)<h_arr(1,i)) tbspots(i,2) = x(i,1) + zdiffsp
          END IF
       END IF

       mixspots(i,:) = tbspots(i,:)
       IF (layertop-layerbot>0.0_pr) THEN
          IF (ABS(mixspots(i,1)-layertop)/2.0_pr >h_arr(1,i)) mixspots(i,1) = -Lxyz(1)
          IF (ABS(mixspots(i,2)-layerbot)/2.0_pr >h_arr(1,i)) mixspots(i,2) =  Lxyz(1)
       END IF

       diffspots(i,:) = tbspots(i,:)
       diffint = NINT(x(i,2)/diffdely)
       IF (dim==3) diffintz = NINT(x(i,3)/diffdelz)
       dodiffhere = ABS(x(i,2)-REAL(diffint,pr)*diffdely) .LT. 1.0e-12_pr
       IF (dim==3) dodiffhere = (ABS(x(i,3)-REAL(diffintz,pr)*diffdelz) .LT. 1.0e-12_pr) .AND. dodiffhere
       IF (dodiffhere) THEN
          diffint = diffint + 1
          IF (dim==3) diffint = diffint + ptsdiffy*diffintz 
          IF (olddifftop(diffint)-olddiffbot(diffint)>0.0_pr) THEN
             IF (ABS(diffspots(i,1)-olddifftop(diffint))/2.0_pr >h_arr(1,i)) diffspots(i,1) = -Lxyz(1)
             IF (ABS(diffspots(i,2)-olddiffbot(diffint))/2.0_pr >h_arr(1,i)) diffspots(i,2) =  Lxyz(1)
             difftop(diffint) = MAX(difftop(diffint),diffspots(i,1)) 
             diffbot(diffint) = MIN(diffbot(diffint),diffspots(i,2)) 
          ELSE
             difftop(diffint) = MAX(difftop(diffint),diffspots(i,1)) 
             diffbot(diffint) = MIN(diffbot(diffint),diffspots(i,2)) 
          END IF
       END IF
    END DO          
    layertop = MAXVAL(mixspots(:,1))
    CALL parallel_global_sum (REALMAXVAL=layertop)
    layerbot = MINVAL(mixspots(:,2))
    CALL parallel_global_sum (REALMINVAL=layerbot)
    layerwidth=layertop-layerbot

    DO i=1,ptsdifftot
       CALL parallel_global_sum (REALMAXVAL=difftop(i))
       CALL parallel_global_sum (REALMINVAL=diffbot(i))
    END DO
    difflay(:) = difftop(:) - diffbot(:)

    totvol = SUM(dA)
    CALL parallel_global_sum (REAL=totvol)

    TopVals = pureX(1)
    BotVals = pureX(2)
    DiffsTop = ABS(TopVals - BotVals)*layerthresh - ABS(Xvals - TopVals)
    DiffsBot = ABS(TopVals - BotVals)*layerthresh - ABS(Xvals - BotVals)
    VolsTop = SIGN(dA,DiffsTop)
    VolXtop = SUM(MAX(VolsTop,0.0_pr))
    VolsNot = MAX(-VolsTop,0.0_pr)
    VolsBot = SIGN(dA,DiffsBot)
    VolXbot = SUM(MAX(VolsBot,0.0_pr))
    VolsNot = SIGN(VolsNot,DiffsBot)
    VolXmix = SUM(MAX(-VolsNot,0.0_pr))
    CALL parallel_global_sum (REAL=VolXtop)
    CALL parallel_global_sum (REAL=VolXbot)
    CALL parallel_global_sum (REAL=VolXmix)
    VolXtot = VolXtop + VolXbot + VolXmix
    VolXtop = VolXtop - (VolXtot - totvol)/2.0_pr
    VolXbot = VolXbot - (VolXtot - totvol)/2.0_pr
    VolXtot = VolXtop + VolXbot + VolXmix

    TopVals = pureY(1) 
    BotVals = pureY(2) 
    DiffsTop = ABS(TopVals - BotVals)*layerthresh - ABS(uin(:,nspc(1))/uin(:,nden) - TopVals)
    DiffsBot = ABS(TopVals - BotVals)*layerthresh - ABS(uin(:,nspc(1))/uin(:,nden) - BotVals)
    VolsTop = SIGN(dA,DiffsTop)
    VolYtop = SUM(MAX(VolsTop,0.0_pr))
    VolsNot = MAX(-VolsTop,0.0_pr)
    VolsBot = SIGN(dA,DiffsBot)
    VolYbot = SUM(MAX(VolsBot,0.0_pr))
    VolsNot = SIGN(VolsNot,DiffsBot)
    VolYmix = SUM(MAX(-VolsNot,0.0_pr))
    CALL parallel_global_sum (REAL=VolYtop)
    CALL parallel_global_sum (REAL=VolYbot)
    CALL parallel_global_sum (REAL=VolYmix)
    VolYtot = VolYtop + VolYbot + VolYmix
    VolYtop = VolYtop - (VolYtot - totvol)/2.0_pr
    VolYbot = VolYbot - (VolYtot - totvol)/2.0_pr
    VolYtot = VolYtop + VolYbot + VolYmix

    TopVals = Pint/pureR(1)/Tconst*EXP(-pureg(1)*x(:,1)/pureR(1)/Tconst) 
    BotVals = Pint/pureR(2)/Tconst*EXP(-pureg(2)*x(:,1)/pureR(2)/Tconst) 
    DiffsTop = ABS(TopVals - BotVals)*layerthresh - ABS(uin(:,nden) - TopVals)
    DiffsBot = ABS(TopVals - BotVals)*layerthresh - ABS(uin(:,nden) - BotVals)
    VolsTop = SIGN(dA,DiffsTop)
    VolDtop = SUM(MAX(VolsTop,0.0_pr))
    VolsNot = MAX(-VolsTop,0.0_pr)
    VolsBot = SIGN(dA,DiffsBot)
    VolDbot = SUM(MAX(VolsBot,0.0_pr))
    VolsNot = SIGN(VolsNot,DiffsBot)
    VolDmix = SUM(MAX(-VolsNot,0.0_pr))
    CALL parallel_global_sum (REAL=VolDtop)
    CALL parallel_global_sum (REAL=VolDbot)
    CALL parallel_global_sum (REAL=VolDmix)
    VolDtot = VolDtop + VolDbot + VolDmix
    VolDtop = VolDtop - (VolDtot - totvol)/2.0_pr
    VolDbot = VolDbot - (VolDtot - totvol)/2.0_pr
    VolDtot = VolDtop + VolDbot + VolDmix

    TopVals = Pint/pureR(1)/Tconst*EXP(-pureg(1)*x(:,1)/pureR(1)/Tconst)*pureY(1) 
    BotVals = Pint/pureR(2)/Tconst*EXP(-pureg(2)*x(:,1)/pureR(2)/Tconst)*pureY(2)
    DiffsTop = ABS(TopVals - BotVals)*layerthresh - ABS(uin(:,nspc(1)) - TopVals)
    DiffsBot = ABS(TopVals - BotVals)*layerthresh - ABS(uin(:,nspc(1)) - BotVals)
    VolsTop = SIGN(dA,DiffsTop)
    VolVtop = SUM(MAX(VolsTop,0.0_pr))
    VolsNot = MAX(-VolsTop,0.0_pr)
    VolsBot = SIGN(dA,DiffsBot)
    VolVbot = SUM(MAX(VolsBot,0.0_pr))
    VolsNot = SIGN(VolsNot,DiffsBot)
    VolVmix = SUM(MAX(-VolsNot,0.0_pr))
    CALL parallel_global_sum (REAL=VolVtop)
    CALL parallel_global_sum (REAL=VolVbot)
    CALL parallel_global_sum (REAL=VolVmix)
    VolVtot = VolVtop + VolVbot + VolVmix
    VolVtop = VolVtop - (VolVtot - totvol)/2.0_pr
    VolVbot = VolVbot - (VolVtot - totvol)/2.0_pr
    VolVtot = VolVtop + VolVbot + VolVmix

    d2u(1,:,2) = (1.0_pr - SUM(uin(:,nspc(1):nspc(Nspecm)),DIM=2)/uin(:,nden))*cp_in(Nspec)
    d2u(1,:,1) = (1.0_pr - SUM(uin(:,nspc(1):nspc(Nspecm)),DIM=2)/uin(:,nden))*mu_in(Nspec)
    du(1,:,2)  = (1.0_pr - SUM(uin(:,nspc(1):nspc(Nspecm)),DIM=2)/uin(:,nden))*bD_in(Nspec)
    du(1,:,1)  = (1.0_pr - SUM(uin(:,nspc(1):nspc(Nspecm)),DIM=2)/uin(:,nden))*kk_in(Nspec)
    DO i=1,Nspecm
       d2u(1,:,2) = d2u(1,:,2) + uin(:,nspc(i))/uin(:,nden)*cp_in(i) !cp
       d2u(1,:,1) = d2u(1,:,1) + uin(:,nspc(i))/uin(:,nden)*mu_in(i) !mu
       du(1,:,2)  = du(1,:,2)  + uin(:,nspc(i))/uin(:,nden)*bD_in(i) !bD
       du(1,:,1)  = du(1,:,1)  + uin(:,nspc(i))/uin(:,nden)*kk_in(i) !kk
    END DO
    IF (kinevisc .GT. 0) THEN
       CALL user_buffkine (kinep(:), uin(:,nden), ng)
       d2u(1,:,1) = d2u(1,:,1)*kinep(:)
       du (1,:,1) = du (1,:,1)*kinep(:)
    END IF

    DragCoef=2.0_pr*pi
    IF (dim==2) DragCoef=DragCoef*3.0_pr
    Velb = SQRT(2.0_pr*At/(1.0_pr+At)/DragCoef) 
    Vels = SQRT(2.0_pr*At/(1.0_pr-At)/DragCoef) 

    zdiffs(:,1) = MINVAL(h_arr(:,:),DIM=1) !Delta
    Rebm = MAXVAL(Velb*MINVAL(delxyz)/d2u(1,:,1))
    Rebd = MAXVAL(Velb*MINVAL(delxyz)/ du(1,:,2))
    Rebk = MAXVAL(Velb*MINVAL(delxyz)*d2u(1,:,2)/du(1,:,1))
    Resm = MAXVAL(Vels*MINVAL(delxyz)/d2u(1,:,1))
    Resd = MAXVAL(Vels*MINVAL(delxyz)/ du(1,:,2))
    Resk = MAXVAL(Vels*MINVAL(delxyz)*d2u(1,:,2)/du(1,:,1))
    Recm = MAXVAL(SQRT(SUM(uin(:,nvel(1):nvel(dim))**2,DIM=2))*MAXVAL(h_arr(:,:),DIM=1)/d2u(1,:,1))
    Recd = MAXVAL(SQRT(SUM(uin(:,nvel(1):nvel(dim))**2,DIM=2))/uin(:,nden)*MAXVAL(h_arr(:,:),DIM=1)/du(1,:,2))
    Reck = MAXVAL(SQRT(SUM(uin(:,nvel(1):nvel(dim))**2,DIM=2))*MAXVAL(h_arr(:,:),DIM=1)*d2u(1,:,2)/du(1,:,1))
    Revm = MAXVAL(uin(:,nmvt)*uin(:,nden)*zdiffs(:,1)**2/d2u(1,:,1))
    Revd = MAXVAL(uin(:,nmvt)*zdiffs(:,1)**2/du(1,:,2))
    Revk = MAXVAL(uin(:,nmvt)*uin(:,nden)*zdiffs(:,1)**2*d2u(1,:,2)/du(1,:,1))
    Rerm = MAXVAL(uin(:,nnms)*uin(:,nden)*zdiffs(:,1)**2/d2u(1,:,1))
    Rerd = MAXVAL(uin(:,nnms)*zdiffs(:,1)**2/du(1,:,2))
    Rerk = MAXVAL(uin(:,nnms)*uin(:,nden)*zdiffs(:,1)**2*d2u(1,:,2)/du(1,:,1))
    Rehm = MAXVAL(c0(1)*MINVAL(delxyz)/d2u(1,:,1))
    Rehd = MAXVAL(c0(1)*MINVAL(delxyz)/ du(1,:,2))
    Rehk = MAXVAL(c0(1)*MINVAL(delxyz)*d2u(1,:,2)/du(1,:,1))
    Relm = MAXVAL(c0(2)*MINVAL(delxyz)/d2u(1,:,1))
    Reld = MAXVAL(c0(2)*MINVAL(delxyz)/ du(1,:,2))
    Relk = MAXVAL(c0(2)*MINVAL(delxyz)*d2u(1,:,2)/du(1,:,1))

    CALL parallel_global_sum (REALMAXVAL=Rebm)  !Bubble   viscosity (momentum diffusion)
    CALL parallel_global_sum (REALMAXVAL=Rebd)  !Bubble   species diffusion        
    CALL parallel_global_sum (REALMAXVAL=Rebk)  !Bubble   thermal diffusion
    CALL parallel_global_sum (REALMAXVAL=Resm)  !Spike
    CALL parallel_global_sum (REALMAXVAL=Resd)
    CALL parallel_global_sum (REALMAXVAL=Resk)
    CALL parallel_global_sum (REALMAXVAL=Recm)  !convection
    CALL parallel_global_sum (REALMAXVAL=Recd)
    CALL parallel_global_sum (REALMAXVAL=Reck)
    CALL parallel_global_sum (REALMAXVAL=Revm)  !vorticity modulus
    CALL parallel_global_sum (REALMAXVAL=Revd)
    CALL parallel_global_sum (REALMAXVAL=Revk)
    CALL parallel_global_sum (REALMAXVAL=Rerm)  !strain rate modulus
    CALL parallel_global_sum (REALMAXVAL=Rerd)
    CALL parallel_global_sum (REALMAXVAL=Rerk)
    CALL parallel_global_sum (REALMAXVAL=Rehm)  !Heavy Fluid acoustic speed
    CALL parallel_global_sum (REALMAXVAL=Rehd)          
    CALL parallel_global_sum (REALMAXVAL=Rehk)  
    CALL parallel_global_sum (REALMAXVAL=Relm)  !Light Fluid acoustic speed
    CALL parallel_global_sum (REALMAXVAL=Reld)
    CALL parallel_global_sum (REALMAXVAL=Relk)

    centrho(:)=0.0_pr 
    centrhoY(:)=0.0_pr 
    centY(:)=0.0_pr 
    centX(:)=0.0_pr 
    centA(:)=0.0_pr 
    centN(:)=0
    DO i=1,nwlt
       curxind = NINT( (x(i,1)-xyzlimits(1,1))/cent_dx ) + 1
       centrho(curxind)  = centrho(curxind)  + uin(i,nden)*dA(i) 
       centrhoY(curxind) = centrhoY(curxind) + uin(i,nspc(1))*dA(i) 
       centY(curxind)    = centY(curxind)    + uin(i,nspc(1))/uin(i,nden)*dA(i) 
       centX(curxind)    = centX(curxind)    + Xvals(i)*dA(i) 
       centA(curxind)    = centA(curxind)    + dA(i) 
       centN(curxind)    = centN(curxind)    + 1
    END DO
    DO i=1,cent_n
       CALL parallel_global_sum(INTEGER=centN(i))
       IF (centN(i).EQ.0) THEN
          centA(i) = -1.0_pr
          centrho(i) = 1.0_pr
          centrhoY(i) = 1.0_pr
          centY(i) = 1.0_pr
          centX(i) = 1.0_pr
       ELSE
          CALL parallel_global_sum(REAL=centA(i))
          CALL parallel_global_sum(REAL=centrho(i))
          CALL parallel_global_sum(REAL=centrhoY(i))
          CALL parallel_global_sum(REAL=centY(i))
          CALL parallel_global_sum(REAL=centX(i))
       END IF    
    END DO
    IF (par_rank .EQ. 0) THEN
       centrho(:)  = centrho(:) /centA(:)
       centrhoY(:) = centrhoY(:)/centA(:)
       centY(:)    = centY(:)   /centA(:)
       centX(:)    = centX(:)   /centA(:)

       curxrho  = xyzlimits(1,1) - 1.0_pr
       curxrhoY = xyzlimits(1,1) - 1.0_pr
       curxY    = xyzlimits(1,1) - 1.0_pr
       curxX    = xyzlimits(1,1) - 1.0_pr
       curxind = 1
       DO WHILE (curxind<cent_n)
          centiter=1
          DO WHILE (centiter>0)
             IF (centN(curxind+centiter).EQ.0) THEN
                centiter = centiter+1
                IF (curxind+centiter>cent_n) THEN
                   centiter=0
                   curxind=cent_n
                END IF
             ELSE
                IF ((centrhoval - centrho(curxind))*(centrhoval - centrho(curxind+centiter)).LE.0.0_pr) THEN
                   curxcrho=(centrhoval-centrho(curxind))/(centrho(curxind+centiter)-centrho(curxind))*(centxs(curxind+centiter)-centxs(curxind))+centxs(curxind)
                   IF (ABS(curxcrho - centxrhoval)<ABS(curxrho - centxrhoval)) curxrho=curxcrho
                END IF
                IF ((centrhoYval - centrhoY(curxind))*(centrhoYval - centrhoY(curxind+centiter)).LE.0.0_pr) THEN
                   curxcrhoY=(centrhoYval-centrhoY(curxind))/(centrhoY(curxind+centiter)-centrhoY(curxind))*(centxs(curxind+centiter)-centxs(curxind))+centxs(curxind)
                   IF (ABS(curxcrhoY - centxrhoYval)<ABS(curxrhoY - centxrhoYval)) curxrhoY=curxcrhoY
                END IF
                IF ((centYval - centY(curxind))*(centYval - centY(curxind+centiter)).LE.0.0_pr) THEN
                   curxcY=(centYval-centY(curxind))/(centY(curxind+centiter)-centY(curxind))*(centxs(curxind+centiter)-centxs(curxind))+centxs(curxind)
                   IF (ABS(curxcY - centxYval)<ABS(curxY - centxYval)) curxY=curxcY
                END IF
                IF ((centXval - centX(curxind))*(centXval - centX(curxind+centiter)).LE.0.0_pr) THEN
                   curxcX=(centXval-centX(curxind))/(centX(curxind+centiter)-centX(curxind))*(centxs(curxind+centiter)-centxs(curxind))+centxs(curxind)
                   IF (ABS(curxcX - centxXval)<ABS(curxX - centxXval)) curxX=curxcX
                END IF
                curxind = curxind+centiter
                centiter = 0
             END IF    
          END DO
       END DO
       IF (curxrho.GE.xyzlimits(1,1)) centxrhoval = curxrho
       IF (curxrhoY.GE.xyzlimits(1,1)) centxrhoYval = curxrhoY
       IF (curxY.GE.xyzlimits(1,1)) centxYval = curxY
       IF (curxX.GE.xyzlimits(1,1)) centxXval = curxX
    END IF
    delxyz(1:dim)=(xyzlimits(2,1:dim)-xyzlimits(1,1:dim))/REAL(mxyz(1:dim)*2**(j_lev-1),pr)  !delxyz = L/N in every direction on j_mn_local
    IF (diffstab .EQ. -5) THEN
       diffvisc = difffac/2.0_pr/cfldt/SUM(1.0_pr/delxyz**2)
    ELSEIF (diffstab .EQ. -6) THEN
       diffvisc = difffac/2.0_pr/dt/SUM(1.0_pr/delxyz**2)
    ELSEIF (diffstab .EQ. 0) THEN
       diffvisc = difffac/2.0_pr/dt/SUM(1.0_pr/delxyz**2)
    END IF
    diffdamp = EXP(-diffvisc*(2.0_pr*pi)**2*SUM(1.0_pr/delxyz**2)*dt)

    !ORDER OF STATS WRITE TO FILE
    ! time eps nwlt nper comp npermax compmax cpu denscl velscl prsscl spcscl vonscl tmpscl sonscl CFL CFLconv CFLacou Ma maxsos dt layertop layerbot layerwid VolXtop VolXbot VolXmix'
    IF (par_rank==0) THEN
       WRITE(*,'(" ")')
       WRITE(*,'("************************ RayTayInst Statistics on the Fly **************************")')
       WRITE(*,'("*         Ma=",E12.5,"          maxsos=",E12.5,"    denscl=",E12.5,5x," *")') &
                     MAreport,maxsos,scl_global(nden)
       WRITE(*,'("*     velscl=",E12.5,"          prsscl=",E12.5,"    spcscl=",E12.5,5x," *")') &
                     scl_global(nvel(1)),scl_global(nprs),scl_global(nspc(1))
       WRITE(*,'("*     vonscl=",E12.5,"          tmpscl=",E12.5,"    sonscl=",E12.5,5x," *")') &
                     scl_global(nvon(1)),scl_global(nton),scl_global(nson(1))
       WRITE(*,'("*  CFLreport=",E12.5,"         CFLnonl=",E12.5,"   CFLvisc=",E12.5,5x," *")') &
                     CFLreport,CFLnonl,MAX(CFLvisc,CFLthrm,CFLspec)
       WRITE(*,'("*    CFLbuff=",E12.5,"        CFLvy/sr=",E12.5,"   CFLdiff=",E12.5,5x," *")') &
                     MAX(CFLbffv,CFLbfft,CFLbffs),MAX(CFLvort,CFLmods),CFLdiff
       WRITE(*,'("*   layertop=",E12.5,"        layerbot=",E12.5,"  layerwid=",E12.5,5x," *")') &
                     layertop, layerbot, layerwidth
       WRITE(*,'("*centlinerho=",E12.5,"       centlineY=",E12.5," centlineX=",E12.5,5x," *")') &
                    centxrhoval,centxYval,centxXval
       WRITE(*,'("*    VolXtop=",E12.5,"         VolXbot=",E12.5,"   VolXmix=",E12.5,5x," *")') &
                     VolXtop,VolXbot,VolXmix
       WRITE(*,'("* Re_bub/spk=",E12.5,"    Re_conv/acou=",E12.5,"  Re_vt/st=",E12.5,5x," *")') &
                     MAX(Rebm,Rebd,Rebk,Resm,Resd,Resk),MAX(Recm,Recd,Reck,Rehm,Rehd,Rehk,Relm,Reld,Relk),MAX(Revm,Revd,Revk,Rerm,Rerd,Rerk)
       WRITE(*,'("*      diffy=",E12.5,"           diffz=",E12.5," difflayer=",E12.5,5x," *")') &
                     ydifflay(1),zdifflay(1), difflay(1)
       WRITE(*,'("************************************************************************************")')
       WRITE(*,'(" ")')
    END IF

    IF (par_rank==0) THEN
       OPEN(unit=55,file=TRIM(stat_filebase)//".stats.txt",position="append")
       WRITE(UNIT=55,ADVANCE='NO', FMT='( 2(e16.8 , '' ''),i12, '' '', 69(e16.8 , '' ''),i12, '' ''  )' )  &
                     t,eps,mynwlt_global,nwltper,nwltcomp,nwltpermx,nwltcompmx, timer_val(2), &
                     scl_global(nden),scl_global(nvel(1)),scl_global(nprs),scl_global(nspc(1)),scl_global(nvon(1)),scl_global(nton),scl_global(nson(1)), &
                     CFLreport,CFLnonl,CFLconv,CFLacou,CFLvisc,CFLthrm,CFLspec,CFLvort,CFLmods,CFLbffv,CFLbfft,CFLbffs,CFLdiff,diffdamp,MAreport,maxsos,dt, &
                     layertop,layerbot,layerwidth,centxrhoval,centxrhoYval,centxYval,centxXval, &
                     VolXtop,VolXbot,VolXmix,VolYtop,VolYbot,VolYmix,VolDtop,VolDbot,VolDmix,VolVtop,VolVbot,VolVmix, &
                     Rebm,Rebd,Rebk,Resm,Resd,Resk,Recm,Recd,Reck,Revm,Revd,Revk,Rerm,Rerd,Rerk,Rehm,Rehd,Rehk,Relm,Reld,Relk,ptsdifftot
       DO i=1,15
          WRITE(UNIT=55,ADVANCE='NO', FMT='( 3(e16.8 , '' '') )' )  maxvals(i),minvals(i),avevals(i)
       END DO
       DO i=1,ptsdifftot-1
          WRITE(UNIT=55,ADVANCE='NO', FMT='( 5(e16.8 , '' '') )' )  ydifflay(i), zdifflay(i), difftop(i), diffbot(i), difflay(i)
       END DO
       WRITE(UNIT=55,ADVANCE='YES', FMT='( 5(e16.8 , '' '') )' )  ydifflay(ptsdifftot), zdifflay(ptsdifftot), difftop(ptsdifftot), diffbot(ptsdifftot), difflay(ptsdifftot)
       CLOSE(UNIT=55)
    END IF

    IF (docent) THEN
       IF (t.GE.twrite) THEN
          IF (par_rank==0) THEN
             WRITE (numstrng,'(I8)') iwrite
          END IF
          IF (par_rank==0) THEN
             OPEN(unit=85,file=TRIM(stat_filebase)//"."//TRIM(ADJUSTL(numstrng))//".centspots.txt")
             WRITE(85,*) '#Horz aves; nx,dx,xmin,xmax,N(nx),A(nx),rho(nx),rhoY(nx),Y(nx),X(nx)#'
             WRITE(85,*) cent_n
             WRITE(85,*) cent_dx
             WRITE(85,*) xyzlimits(:,1)
             WRITE(85,*) centN
             WRITE(85,*) centA
             WRITE(85,*) centrhoval
             WRITE(85,*) centrho
             WRITE(85,*) centrhoYval
             WRITE(85,*) centrhoY
             WRITE(85,*) centYval
             WRITE(85,*) centY
             WRITE(85,*) centXval
             WRITE(85,*) centX
             CLOSE(85)
          END IF
       END IF
    END IF
  END SUBROUTINE user_stats

  SUBROUTINE user_cal_force (uin, n, t_local, force, drag, lift)
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: n
    REAL (pr), INTENT (IN) :: t_local
    REAL (pr), DIMENSION (dim), INTENT (INOUT) :: force
    REAL (pr), INTENT (OUT) :: drag, lift
    REAL (pr), DIMENSION (n,dim) :: uin
    drag = 0.0_PR
    lift = 0.0_PR
  END SUBROUTINE user_cal_force

  !
  ! Read input from "case_name"_pde.inp file
  ! case_name is in string file_name read from command line
  ! in read_command_line_input()
  ! 
  SUBROUTINE user_read_input()
    IMPLICIT NONE
    INTEGER :: i, j, ind
    CHARACTER(LEN=64)::stat_filebase_temp
    CHARACTER(LEN=8):: numstrng
    INTEGER :: myiter
    LOGICAL :: checkit 
    REAL (pr) :: halfdomsize
    REAL (pr), DIMENSION (dim) :: delxyz

    call input_real ('Re',Re,'stop',' Re: Reynolds Number')
    call input_real ('Rep',Rep,'stop',' Rep: Perturbation Reynolds Number')
    call input_real ('Rebnd',Rebnd,'stop',' Rebnd: Reynolds Number on boundaries')
    call input_real ('Pra',Pra,'stop',' Pra: Prandtl Number')
    call input_real ('Sc',Sc,'stop',' Sc: Schmidt Number')
    call input_real ('Fr',Fr,'stop',' Fr: Froude Number')
    call input_real ('Ma',Ma,'stop',' Ma: Mach Number')
    call input_integer ('Nspec',Nspec,'stop','  Nspec: Number of species')
    call input_integer ('methpd',methpd,'stop','  methpd: meth for taking pressure derivative in time')
    call input_integer ('methpdic',methpdic,'stop','  methpdic: meth for taking IC pressure derivative')
    call input_integer ('mykry',mykry,'stop','  mykry: kry for gmres in IC')
    call input_integer ('mylen',mylen,'stop','  mylen: len for BiCGSTAB in IC')
    call input_integer ('BCver',BCver,'stop','  BCver: version for BC')
    call input_integer ('ICver',ICver,'stop','  ICver: version for IC')
    call input_integer ('ptsacross',ptsacross,'stop', '  ptsacross: number of points across interface')
    call input_integer ('j_ptsacross',j_ptsacross,'stop', '  j_ptsacross: level used for computing delta')
    call input_integer ('limitj_pts', limitj_pts,'stop',  '  limitj_pts: 0 to use input, 1 to use MIN(input, j_mx-1), 2 to use MIN(input, j_mx), 3 to use MIN(j_lev_init)')
    call input_integer ('minpts',minpts,'stop', '  minpts: minimum number of points across interface at j_lev')
    call input_integer ('chartype',chartype,'stop', 'chartype: 1 for wall, 2 for no-slip wall, 3 for non-refl, 4 for refl')
    call input_integer ('PLpinf',PLpinf,'stop', 'PLpinf: Poinsot Lele P_inf Brinkman term with LODI, 1:2 for P, 3:4 for T, 5:6 for u, (odd const coef, even var coef) 0 for nothing')
    call input_integer ('hydrosolver',hydrosolver,'stop', 'hydrosolver: 1-Linsolve, 2-gmres, 3-BiCGSTAB, 4-Jacoby')
    call input_integer ('ICbasetype',ICbasetype,'stop', 'ICbasetype: 1-rho is analytical, 2-P is analytical, 3-P from dpdx')
    call input_integer ('dervFlux',dervFlux,'stop', 'dervFlux: 1 to do derivative fluxes, 0 to set fluxes to 0 on boundary only')
    call input_integer ('hydropic',hydropic,'stop', 'hydropic: 1 to use elliptic solver -my_hydropress- to solve dp/dz=-rho*g IC, 2 to use hypersolver')
    call input_integer ('ptsdiff',ptsdiff,'stop', 'ptsdiff: points in y and z to calc diffusion layer height')
    call input_integer ('sm_j',sm_j,'stop', 'j: level for deltaxyz, -1=j_lev, -2=j_mx, -3=j_mx-1')
    call input_integer ('pert_j',pert_j,'stop', 'j: level for deltaxyz, -1=j_lev, -2=j_mx, -3=j_mx-1')
    call input_integer ('smTP_j',smTP_j,'stop', 'j: level for deltaxyz, -1=j_lev, -2=j_mx, -3=j_mx-1')
    call input_integer ('cent_j',cent_j,'stop', 'j: level for centerline resolution, <=0 to set equal to  j_mx-cent_j')
    call input_integer ('diffstab',diffstab,'stop', 'diffstab: diffusion definition type for stability')
    call input_integer ('adddifftype',adddifftype,'stop', 'adddifftype: diff zone type; 0=f/b low-order, 1=d(nu*du), 2=nu*d2_c(u), 3=nu*d2_fb(u)')
    call input_integer ('adddiffvisc',adddiffvisc,'stop', 'adddiffvisc: diff zone visc type; 1=interp neighbor vals to apporx nu_(i +/- 1/2), 0=just use local nu val')
    call input_integer ('adddiffhiord',adddiffhiord,'stop', 'adddiffhiord: diff zone order; 0=lo, 1=hi')
    CALL input_integer ('kinevisc',kinevisc,'stop', 'kinevisc: scale mu and kk by density (1=global, 2=use zone, 3=zone with edgevalues), 0=use dynamic visc and thermal conductivity')
  
    ALLOCATE(gr_in(1:Nspec*dim))  
    ALLOCATE(bD_in(1:Nspec))
    ALLOCATE(mu_in(1:Nspec))
    ALLOCATE(kk_in(1:Nspec))
    ALLOCATE(cp_in(1:Nspec))
    ALLOCATE(MW_in(1:Nspec))
    ALLOCATE(gamm(1:Nspec))
    ALLOCATE(ks(1:dim-1))
    ALLOCATE(pureX(1:Nspec), pureY(1:Nspec), pureR(1:Nspec), pureg(1:Nspec), puregamma(1:Nspec), YR(1:Nspec))

    call input_real ('At',At,'stop',' At: Atwood Number')
    call input_real ('Gr',Gr,'stop',' Gr: Mesh Grashof Number, set <0 to use inp Re')
    call input_real_vector ('gamm',gamm(1:Nspec),Nspec,'stop',' gamm: cp/(cp-R)')
    call input_real ('peakchange',peakchange,'stop',' peakchange: how far offset to put peaks')
    call input_real ('Amp_in',Amp_in,'stop', 'Amp_in: initial amplitude for sine wave')
    call input_real ('icerfdelta',icerfdelta,'stop', 'icerfdelta: delta to be used in IC inside DERF')
    call input_real ('ictanhdelta',ictanhdelta,'stop', 'ictanhdelta: delta to be used in IC inside TANH')
    call input_real_vector ('ks',ks(1:dim-1),dim-1,'stop', 'ks: y&z-direction perturbation wavenumber')
    call input_real ('mytol',mytol,'stop', 'mytol: tolerance used in my PDEs solved with Linsolve')
    call input_real ('newtontol',newtontol,'stop', 'newtontol: tolerance used in my Newton solver')
    call input_real ('threshforpts',threshforpts,'stop', 'threshforpts: treshold to tell whether a point counts')
    call input_real ('dellen',dellen,'stop', 'dellen: delta length for IC mixing layer height')
    call input_real ('buffBeta',buffBeta,'stop', 'bufBeta: polynomial exponent for buff/convect BC')
    call input_real ('buffU0',buffU0,'stop', 'buffU0: scale for convect BC')
    call input_real ('buffSig',buffSig,'stop', 'buffSig: scale for buff BC')
    call input_real ('buffOff',buffOff,'stop', 'buffOff: x-offset used in tanh for buff BC')
    call input_real ('buffFac',buffFac,'stop', 'buffFac: multiplication factor in tanh for buff BC')
    call input_real ('pBsig',pBsig,'stop', 'pBsig: sigma used in coefficient for pressure Brinkman term')
    call input_real ('layerthresh',layerthresh,'stop', 'layerthresh: threshold for layer width calculation')
    call input_real ('Lczn',Lczn,'stop', 'Lcoordzn: length of coord_zone on one side')
    call input_real ('tfacczn',tfacczn,'stop', 'tfacczn: factor multiply by time for coord_zone time=L/min(c)*tfac, set <0 for no czn modification')
    call input_real ('itfacczn',itfacczn,'stop', 'itfacczn: factor multiply by initial time for coord_zone itime=Lczn/min(c)*tfac, set <0 to set itime=0')
    call input_real ('cuttopat',cuttopat,'stop', 'cuttopat: location to cut solution for hydropic-FB/BB for heavy fluid')
    call input_real ('cutbotat',cutbotat,'stop', 'cutbotat: location to cut solution for hydropic-FB/BB for light fluid')
    call input_real ('waitrat',waitrat,'stop', 'waitrat: fraction to wait beyond acoustic speed travel time')
    call input_real_vector ('buffRed',buffRed(1:5),5,'stop', 'buffRed: buffRe distances')
    call input_real_vector ('buffRef',buffRef(1:5),5,'stop', 'buffRef: buffRe distance fractions')
    call input_real_vector ('buffcvd',buffcvd(1:5),5,'stop', 'buffcvd: convect zone distances')
    call input_real_vector ('buffcvf',buffcvf(1:5),5,'stop', 'buffcvf: convect zone distance fractions')
    call input_real_vector ('buffbfd',buffbfd(1:5),5,'stop', 'buffbfd: buffer zone distances')
    call input_real_vector ('buffbff',buffbff(1:5),5,'stop', 'buffbff: buffer zone distance fractions')
    call input_real_vector ('buffdfd',buffdfd(1:5),5,'stop', 'buffdfd: diffusion zone distances')
    call input_real_vector ('buffdff',buffdff(1:5),5,'stop', 'buffdff: diffusion zone distance fractions')
    call input_real ('maskscl',maskscl,'stop', 'maskscl: scale for mask, set < 0 for default')
    call input_real ('tempscl',tempscl,'stop', 'tempscl: scale for temperature, set < 0 for default, set =0 to bottom bound by velocity scale')
    call input_real ('specscl',specscl,'stop', 'specscl: scale for species, set < 0 for default')
    call input_real ('sm_dfac',sm_dfac,'stop', 'dfac: fraction to apply to diffusion dt in ic - stability vs. speed')
    call input_real ('pert_dfac',pert_dfac,'stop', 'dfac: fraction to apply to diffusion dt in ic - stability vs. speed')
    call input_real ('smTP_dfac',smTP_dfac,'stop', 'dfac: fraction to apply to diffusion dt in ic - stability vs. speed')
    call input_real ('sm_delta',sm_delta,'stop', 'delta: diffusion width after smoothing, set <0 to use deltadel')
    call input_real ('pert_delta',pert_delta,'stop', 'delta: diffusion width after smoothing, set <0 to use deltadel')
    call input_real ('smTP_delta',smTP_delta,'stop', 'delta: diffusion width after smoothing, set <0 to use deltadel')
    call input_real ('sm_deltadel',sm_deltadel,'stop', 'sm_deltadel = 1.00000e-01  # deltadel: delta/deltaxyz ratio')
    call input_real ('pert_deltadel',pert_deltadel,'stop', 'sm_deltadel = 1.00000e-01  # deltadel: delta/deltaxyz ratio')
    call input_real ('smTP_deltadel',smTP_deltadel,'stop', 'sm_deltadel = 1.00000e-01  # deltadel: delta/deltaxyz ratio')
    call input_real ('tempsig',tempsig,'stop', 'tempsig: sigma for temperature hump')
    call input_real ('tempamp',tempamp,'stop', 'tempamp: amplitude for temperature hump')
    call input_real ('mydt_orig',mydt_orig,'stop', 'mydt_orig: max dt for time integration... dt is only initial dt, set <0 to use dt')
    call input_real ('diffcfl',diffcfl,'stop', '|diffcfl|: cfl to impose on diff zone, set <0 to not check in time')
    call input_real ('visccfl',visccfl,'stop', '|visccfl|: cfl to impose on diffusion terms, set <0 to not check in time')
    call input_real ('imexcfl',imexcfl,'stop', 'imexcfl  : cfl threshold for going to imex, set <0 to not check in time')
    call input_real ('difffac',difffac,'stop', ' difffac : factor to multiply diffvisc by')
    call input_real ('diffvisc',diffvisc,'stop', 'diffvisc: diffusion coef., only used if diffstab=-10')
    CALL input_real ('kineviscbuff',kineviscbuff,'stop', 'kineviscbuff: size of buffer until kinevisc is used beyond layertop/bot')
    CALL input_real ('kinevisctran',kinevisctran,'stop', 'kinevisctran: size of transition for kinevisc')

    call input_logical ('adaptAt',adaptAt,'stop', 'adaptAt: T to modify At to get effectivie At = input At due to peakchange')
    call input_logical ('AmpisInt',AmpisInt,'stop', 'AmpisInt: T Amp_in= Interface Amp, F Amp_in= Vel Amp')
    call input_logical ('useerf',useerf,'stop', 'useerf: T to use ERF to smooth IC, F to use TANH')
    call input_logical ('pertuseerf',pertuseerf,'stop', 'pertuseerf: T to use ERF to smooth IC perturbation, F to use TANH')
    call input_logical ('doicdiff',doicdiff,'stop', 'doicdiff: T to diffuse IC, F to use ERF or TANH')
    call input_logical ('pertdoicdiff',pertdoicdiff,'stop', 'pertdoicdiff: T to diffuse IC perturbation, F to use ERF or TANH')
    call input_logical ('sm_alldir',sm_alldir,'stop', 'alldir: T to diffuse IC in all directions, F to do just in x')
    call input_logical ('pert_alldir',pert_alldir,'stop', 'alldir: T to diffuse IC in all directions, F to do just in x')
    call input_logical ('smTP_alldir',smTP_alldir,'stop', 'alldir: T to diffuse IC in all directions, F to do just in x')
    call input_logical ('stepic',stepic,'stop', 'stepic: T to use step interface for IC, F to smooth')
    call input_logical ('pertstepic',pertstepic,'stop', 'pertstepic: T to use step interface for IC perturbation, F to smooth')
    call input_logical ('pertuseX',pertuseX,'stop', 'pertuseX: T to use X distribution to smooth perturbation for IC')
    call input_logical ('pertuseY',pertuseY,'stop', 'pertuseY: T to use Y distribution to smooth perturbation for IC')
    call input_logical ('compdelta',compdelta,'stop', 'compdelta: T to calculate delta to get ptsacross, F to use input delta')
    call input_logical ('usedellen',usedellen,'stop', 'usedellen: T to use dellen for ptsacross, F to use ptsacross')
    call input_logical ('useminpts',useminpts,'stop', 'useminpts: T to use minpts')
    call input_logical ('evolBC',evolBC,'stop', 'evolBC: T to use evol BCs, F to use algebraic BCs')
    call input_logical ('buffBC',buffBC,'stop', 'buffBC: T to use buff zone damping BCs (Brinkman), F to use algebraic BCs')
    call input_logical ('diffBC',diffBC,'stop', 'diffBC: T to use a diffusion zone ')
    call input_logical ('polybuff',polybuff,'stop', 'polybuff: T to use polynomials for buff zone BCs,  F to use tanh')
    call input_logical ('polydiff',polydiff,'stop', 'polydiff: T to use polynomials for diff zone BCs,  F to use tanh')
    call input_logical ('polybuffRe',polybuffRe,'stop', 'polybuffRe: T to use polynomials for buffRe,  F to use tanh')
    call input_logical ('cnvtBC',cnvtBC,'stop', 'cnvtBC: T to use convection zone BCs (Freund), F to use algebraic BCs')
    call input_logical ('polyconv',polyconv,'stop', 'polyconv: T to use polynomials for conv zone BCs,  F to use tanh')
    call input_logical ('polykine',polykine,'stop', 'polykine: T to use polynomials for kine zone BCs,  F to use tanh')
    call input_logical ('usewall',usewall,'stop', 'usewall: T to use wall values for buff BC,  F to use edge of buffer zone')
    call input_logical ('modwallvals',modwallvals,'stop', 'modwallvals: T to modify wall values inside buffer')
    call input_logical ('pBrink',pBrink,'stop', 'pBrink: T to do Brinkman buffer only on pressure')
    call input_logical ('savebuoys',savebuoys,'stop', 'savebuoys: if T adds and saves dPdx and -(dPdx + rho*g)')
    call input_logical ('adaptbuoy',adaptbuoy,'stop', 'adaptbuoy: T to adapt on buoy term')
    call input_logical ('same_filename',same_filename,'stop', 'logical: T use filegen for stat_filebase')
    call input_logical ('erase_old',erase_old,'stop', 'logical: T write over existing stat_filebase')
    call input_logical ('cuttop',cuttop,'stop', 'cuttop: to cut solution for hydropic-FB/BB for heavy fluid')
    call input_logical ('cutbot',cutbot,'stop', 'cutbot: to cut solution for hydropic-FB/BB for light fluid')
    call input_logical ('splitFBpress',splitFBpress,'stop', 'splitFBpress: to split derivs for pressure dp/dx FB/BB light/heavy')
    call input_logical ('stepsplit',stepsplit,'stop', 'stepsplit: T to do Heaviside split, F to use X')
    call input_logical ('buffRe',buffRe,'stop', 'buffRe: T to change Reynolds number near boundaries')
    call input_logical ('adaptbuffRe',adaptbuffRe,'stop', 'adaptbuffRe: T to adapt on buffer layer')
    call input_logical ('adapttmp',adapttmp,'stop', 'adapttmp: T to adapt on temperature instead of density and pressure')
    call input_logical ('savetmp',savetmp,'stop', 'savetmp: T to save temperature')
    call input_logical ('adaptvelonly',adaptvelonly,'stop', 'adaptvelonly: T to adapt on velocity instead of momentum')
    call input_logical ('adaptspconly',adaptspconly,'stop', 'adaptspconly: T to adapt on mass fraction (Y) instead of volume fraction (rhoY)')
    call input_logical ('adaptden',adaptden,'stop', 'adapt on density')
    call input_logical ('adaptvel',adaptvel,'stop', 'adapt on momentum')
    call input_logical ('adapteng',adapteng,'stop', 'adapt on energy')
    call input_logical ('adaptspc',adaptspc,'stop', 'adapt on species')
    call input_logical ('adaptprs',adaptprs,'stop', 'adapt on pressure')
    call input_logical ('buffRewait',buffRewait,'stop', 'buffRewait: T to wait until acoustic waves reach boundaries to do buffRe')
    call input_logical ('savepardom',savepardom,'stop', 'savepardom: T to save parallel domain as additional var')
    call input_logical ('TICconst',TICconst,'stop', 'TICconst: T to ensure T=const in base state, F to satisfy other conditions')
    call input_logical ('addmixu',addmixu,'stop', 'addmixu: T to add velocity due to pre-IC mixing')
    call input_logical ('smoothT',smoothT,'stop', 'smoothT: T to smooth temperature after pert added')
    call input_logical ('getrho',getrho,'stop', 'getrho: T to calc rho after smoothT, F to calc pressure')
    call input_logical ('smoothP',smoothP,'stop', 'smoothP: T to smooth pressure after pert added')
    call input_logical ('convcfl',convcfl,'stop', 'convcfl: T to use convective cfl, F to use acoustic cfl')
    call input_logical ('temphump',temphump,'stop', 'temphump: T to add temperature hump')
    call input_logical ('boundY',boundY,'stop', 'boundY: T to bound Y to [0,1] in user_pre/post_process')
    call input_logical ('bonusCFL',bonusCFL,'stop', 'bonusCFL: T to do CFL check for modulus strain rate and magnitude of vorticity')
    CALL input_logical ('docent',docent,'stop', 'docent: T to save cents')
    CALL input_logical ('AlgVals',AlgVals,'stop', 'AlgVals: T to use algebraic values for bndvals')
    CALL input_logical ('ComboIMEXRK',ComboIMEXRK,'stop', 'ComboIMEXRK: T to use IMEX when cfl for diffusion terms becomes restrictive, RK otherwise')
    CALL input_logical ('useorgfiles',useorgfiles,'stop', 'useorgfils: T to use orgfiles to mv all results/file_gen####*res files to results/file_gen#### folder')
    CALL input_logical ('savemyres',savemyres,'stop', 'savemyres: T to use save myres files in real space')

    !4extra - these 8 input definitions - MUST ALSO ADD TO .inp 
    call input_logical ('adaptMagVort',adaptMagVort,'stop','adaptMagVort: Adapt on the magnitude of vorticity')
    call input_logical ('adaptComVort',adaptComVort,'stop','adaptComVort: Adapt on the components of vorticity')
    call input_logical ('adaptMagVel',adaptMagVel,'stop','adaptMagVel: Adapt on the magnitude of velocity')
    call input_logical ('adaptNormS',adaptNormS,'stop', 'adaptNormS: Adapt on the L2 of Sij')
    call input_logical ('adaptGradY',adaptGradY,'stop', 'adaptGradY: Adapt on |dY/dx_i*dY/dx_i|')
    call input_logical ('saveMagVort',saveMagVort,'stop', 'saveMagVort: if T adds and saves magnitude of vorticity')
    call input_logical ('saveComVort',saveComVort,'stop', 'saveComVort: if T adds and saves components of vorticity')
    call input_logical ('saveMagVel',saveMagVel,'stop','saveMagVel: if T adds and saves magnitude of velocity')
    call input_logical ('saveNormS',saveNormS,'stop', 'saveNormS: if T adds and saves magnitude of strain rate')
    call input_logical ('saveGradY',saveGradY,'stop', 'saveGradY: if T adds and saves |dY/dx_i*dY/dx_i|')
 
    call input_string ('stat_filebase',stat_filebase,'stop', 'base filename for statfiles')
 
    IF (bonusCFL) saveMagVort = .TRUE.
    IF (bonusCFL) saveNormS   = .TRUE.

    input_dt = dt
    IF (mydt_orig < 0.0_pr) mydt_orig = dt
    IF (IC_restart_mode==1) dt_original = mydt_orig

    IF (NOT(buffBC) .AND. NOT(cnvtBC) .AND. NOT(diffBC)) modwallvals=.FALSE.

    IF (buffRe) imask_obstacle=.TRUE.
    IF (NOT(buffRe)) adaptbuffRe=.FALSE.
    dobuffRe = buffRe
    IF (buffRewait) dobuffRe = .FALSE.

    IF (Nspec<2) adaptspc=.FALSE. 
    IF (Nspec<2) adaptspconly=.FALSE. 
    IF (Nspec<2) adaptGradY=.FALSE. 
    IF (Nspec<2) saveGradY=.FALSE. 
    Nspecm = Nspec - 1

    IF (NOT(evolBC)) dervFlux = 0

    Ypert=.FALSE.
    perturbIC=.FALSE.
    ppert=.FALSE.
    rhopert=.FALSE.
    upert=.FALSE.
    IF (ICver==2 .OR. ICver==4) Ypert=.TRUE.
    IF (ICver==2) rhopert=.TRUE.
    IF (ICver==3 .OR. ICver==4) perturbIC=.TRUE.
    IF (ICver==4) ppert=.TRUE.
    IF (ICver==3) upert=.TRUE.

    IF (adaptAt .AND. (At*(1.0_pr/(1.0_pr-2.0_pr*peakchange)) .LT. 1.0_pr)  ) THEN
       IF (par_rank.EQ.0) PRINT *, 'Effective (Input) Atwood number: ', At
       At=At*(1.0_pr/(1.0_pr-2.0_pr*peakchange))
       IF (par_rank.EQ.0) PRINT *, 'Applied Atwood number: ', At
    ELSE
       IF (par_rank.EQ.0) PRINT *, 'Applied (Input) Atwood number: ', At
       IF (par_rank.EQ.0) PRINT *, 'Effective Atwood number: ', At*(1.0_pr-2.0_pr*peakchange)
    END IF

    delxyz(1:dim)=(xyzlimits(2,1:dim)-xyzlimits(1,1:dim))/REAL(mxyz(1:dim)*2**(j_mx-1),pr)  !delxyz = L/N in every direction on j_mx
    IF (Gr>0.0_pr)  Re=SQRT(Gr*Fr**2/2.0_pr/At/MINVAL(delxyz)**3)
    IF (Rep>0.0_pr) Re=Rep*SQRT((1.0_pr+At)/At)
    IF (par_rank.EQ.0) THEN
       PRINT *, 'Using Re=', Re     
       PRINT *, 'Using Rep=', Re*SQRT(At/(1.0_pr+At))     
       PRINT *, 'Using Gr=', Re**2/Fr**2*2.0_pr*At*MINVAL(delxyz)**3     
    END IF

    gr_in(:) = 0.0_pr  !g's by species, then dimension
    IF (GRAV) gr_in(1:Nspec) = 1.0_pr
    gr_in(:) = gr_in(:)/Fr**2

    MW_in(1) = 1.0_pr+At
    MW_in(2) = 1.0_pr-At
    YR(1) = MW_in(1) / SUM(MW_in(:))
    YR(Nspec) = 1.0_pr - SUM(YR(1:Nspecm))
    cp_in(:) = 1.0_pr/MW_in(:)*gamm(:)/(gamm(:)-1.0_pr)
    gammR = SUM(cp_in(:)*YR(:))/( SUM(cp_in(:)*YR(:))-SUM(YR(:)/MW_in(:)) )
    mu_in(:) = 1.0_pr/Re
    kk_in(:) = 1.0_pr/Re/Pra*gammR/(gammR-1.0_pr)
    bD_in(:) = 1.0_pr/Re/Sc

    Tconst = 1.0/Ma**2
    Pint = 1.0/Ma**2
    pBcoef=pBsig

    ptsdiffy=1
    ptsdiffz=1
    IF (ptsdiff.LE.0) THEN
       diffdely=(xyzlimits(2,2)-xyzlimits(1,2))/REAL(mxyz(2),pr)
       ptsdiffy=mxyz(2)
    ELSE
       diffdely=(xyzlimits(2,2)-xyzlimits(1,2))/REAL(ptsdiff,pr)
       ptsdiffy=ptsdiff
    END IF
    IF (dim==3) THEN
       IF (ptsdiff.LE.0) THEN
          diffdelz=(xyzlimits(2,3)-xyzlimits(1,3))/REAL(mxyz(3),pr)
          ptsdiffz=mxyz(3)
       ELSE
          diffdelz=(xyzlimits(2,3)-xyzlimits(1,3))/REAL(ptsdiff,pr)
          ptsdiffz=ptsdiff
       END IF
    ELSE
       diffdelz=0.0_pr
    END IF
    ptsdifftot = ptsdiffy*ptsdiffz

    IF (ALLOCATED(olddifftop)) DEALLOCATE(olddifftop)
    IF (ALLOCATED(olddiffbot)) DEALLOCATE(olddiffbot)
    IF (ALLOCATED(difftop)) DEALLOCATE(difftop)
    IF (ALLOCATED(diffbot)) DEALLOCATE(diffbot)
    IF (ALLOCATED(difflay)) DEALLOCATE(difflay)
    IF (ALLOCATED(ydifflay)) DEALLOCATE(ydifflay)
    IF (ALLOCATED(zdifflay)) DEALLOCATE(zdifflay)
    ALLOCATE(difftop(ptsdifftot),diffbot(ptsdifftot),olddifftop(ptsdifftot),olddiffbot(ptsdifftot),difflay(ptsdifftot),ydifflay(ptsdifftot),zdifflay(ptsdifftot))

    DO j=1,ptsdiffz
       DO i=1,ptsdiffy
          ind = i + (j-1)*ptsdiffy
          ydifflay(ind) = diffdely*(i-1)
          zdifflay(ind) = diffdelz*(j-1)
       END DO
    END DO

    IF (IC_restart_mode==1) erase_old=.TRUE.
    IF (same_filename) stat_filebase='stats/'//file_gen(1:LEN_TRIM(file_gen)-1)
    myiter=0
    stat_filebase_temp=stat_filebase
    IF (erase_old) THEN
       IF (par_rank==0) THEN
          INQUIRE(FILE=TRIM(stat_filebase)//".stats.txt",EXIST=checkit)
          IF (checkit) THEN
             IF (IC_restart_mode.NE.1) THEN
                PRINT *, 'REPLACING OLD STAT FILES:', TRIM(stat_filebase)
                OPEN(unit=55,file=TRIM(stat_filebase)//".stats.txt")
                WRITE(55,ADVANCE='YES',  FMT='( '' % check for order of stats in rt.f90 '')')
                CLOSE(55)
             END IF
          END IF
       END IF
    ELSE
       IF (par_rank==0) THEN
          checkit=.TRUE.
          DO WHILE (checkit)
             INQUIRE(FILE=TRIM(stat_filebase)//".stats.txt",EXIST=checkit)
             IF (checkit) THEN
                myiter=myiter+1
                PRINT *, 'FILE ALREADY EXISTS:  ',TRIM(stat_filebase)//".stats.txt" 
                WRITE (numstrng,'(I8)') myiter
                stat_filebase=TRIM(stat_filebase_temp)//"_"//TRIM(ADJUSTL(numstrng))
                PRINT *, 'USING:  ',TRIM(stat_filebase)//".stats.txt"
             END IF
          END DO
          PRINT *, 'FINAL STAT BASE FILENAME:  ', TRIM(stat_filebase)
          OPEN(unit=55,file=TRIM(stat_filebase)//".stats.txt")
          WRITE(55,ADVANCE='YES',  FMT='( '' % check for order of stats in rt.f90 '')')
          CLOSE(55)
       END IF
       CALL parallel_global_sum(INTEGERMAXVAL=myiter)
       WRITE (numstrng,'(I8)') myiter
       IF (myiter>0) stat_filebase=TRIM(stat_filebase_temp)//"_"//TRIM(ADJUSTL(numstrng))
    END IF
    IF (same_filename) file_gen = stat_filebase(7:LEN_TRIM(stat_filebase))//"."

     IF (NOT(ALLOCATED(lams))) ALLOCATE(lams(1:dim-1))
     IF (NOT(ALLOCATED(Lxyz))) ALLOCATE(Lxyz(1:dim))

     IF (NOT(ALLOCATED(Y0top))) ALLOCATE(Y0top(1:Nspec))
     IF (NOT(ALLOCATED(Y0bot))) ALLOCATE(Y0bot(1:Nspec))
     IF (NOT(ALLOCATED(rhoYtopBC))) ALLOCATE(rhoYtopBC(1:Nspecm))
     IF (NOT(ALLOCATED(rhoYbotBC))) ALLOCATE(rhoYbotBC(1:Nspecm))
     IF (NOT(ALLOCATED(rhoYtop))) ALLOCATE(rhoYtop(1:Nspecm))
     IF (NOT(ALLOCATED(rhoYbot))) ALLOCATE(rhoYbot(1:Nspecm))

     pi=4.0_pr*ATAN(1.0_pr)
     Lxyz(1:dim)=xyzlimits(2,1:dim)-xyzlimits(1,1:dim)
     lams(1:dim-1)=ks(1:dim-1)*2.0_pr*pi/Lxyz(2:dim)
     !lammag=SQRT(SUM(lams(1:dim-1)**2))
     lammag=lams(1)

    halfdomsize = (xyzlimits(2,1)-xyzlimits(1,1))/2.0_pr
    IF (buffRed(1)<0.0_pr) THEN
       buffRed(1)=halfdomsize*buffRef(1)
    ELSE
       buffRef(1)=buffRed(1)/halfdomsize
    END IF
    IF (ANY(buffRed(2:4)<0.0_pr)) THEN
       buffRef(2)=MAX(0.0_pr,MIN(1.0_pr,buffRef(2)))
       buffRef(3)=MAX(0.0_pr,MIN(1.0_pr-buffRef(2),buffRef(3)))
       buffRef(4)=MAX(0.0_pr,MIN(1.0_pr-SUM(buffRef(2:3)),buffRef(4)))
       buffRef(5)=MAX(0.0_pr,    1.0_pr-SUM(buffRef(2:4)))
       buffRed(2:5)=buffRef(2:5)*buffRed(1)
    ELSE
       buffRed(2)=MIN(buffRed(2),buffRed(1))
       buffRed(3)=MIN(buffRed(3),buffRed(1)-buffRed(2))
       buffRed(4)=MIN(buffRed(4),buffRed(1)-SUM(buffRed(2:3)))
       buffRed(5)=MAX(0.0_pr  ,buffRed(1)-SUM(buffRed(2:4)))
       buffRef(2:5)=buffRed(2:5)/buffRed(1)
    END IF
    IF (verb_level>0 .AND. par_rank==0) PRINT *,'FOR bufferfunc, buffRed=', buffRed
    IF (verb_level>0 .AND. par_rank==0) PRINT *,'FOR bufferfunc, buffRef=', buffRef

    IF (buffcvd(1)<0.0_pr) THEN
       buffcvd(1)=halfdomsize*buffcvf(1)
    ELSE
       buffcvf(1)=buffcvd(1)/halfdomsize
    END IF
    IF (ANY(buffcvd(2:4)<0.0_pr)) THEN
       buffcvf(2)=MAX(0.0_pr,MIN(1.0_pr,buffcvf(2)))
       buffcvf(3)=MAX(0.0_pr,MIN(1.0_pr-buffcvf(2),buffcvf(3)))
       buffcvf(4)=MAX(0.0_pr,MIN(1.0_pr-SUM(buffcvf(2:3)),buffcvf(4)))
       buffcvf(5)=MAX(0.0_pr,    1.0_pr-SUM(buffcvf(2:4)))
       buffcvd(2:5)=buffcvf(2:5)*buffcvd(1)
    ELSE
       buffcvd(2)=MIN(buffcvd(2),buffcvd(1))
       buffcvd(3)=MIN(buffcvd(3),buffcvd(1)-buffcvd(2))
       buffcvd(4)=MIN(buffcvd(4),buffcvd(1)-SUM(buffcvd(2:3)))
       buffcvd(5)=MAX(0.0_pr  ,buffcvd(1)-SUM(buffcvd(2:4)))
       buffcvf(2:5)=buffcvd(2:5)/buffcvd(1)
    END IF
    IF (verb_level>0 .AND. par_rank==0) PRINT *,'FOR bufferfunc, buffcvd=', buffcvd
    IF (verb_level>0 .AND. par_rank==0) PRINT *,'FOR bufferfunc, buffcvf=', buffcvf

    IF (buffbfd(1)<0.0_pr) THEN
       buffbfd(1)=halfdomsize*buffbff(1)
    ELSE
       buffbff(1)=buffbfd(1)/halfdomsize
    END IF
    IF (ANY(buffbfd(2:4)<0.0_pr)) THEN
       buffbff(2)=MAX(0.0_pr,MIN(1.0_pr,buffbff(2)))
       buffbff(3)=MAX(0.0_pr,MIN(1.0_pr-buffbff(2),buffbff(3)))
       buffbff(4)=MAX(0.0_pr,MIN(1.0_pr-SUM(buffbff(2:3)),buffbff(4)))
       buffbff(5)=MAX(0.0_pr,    1.0_pr-SUM(buffbff(2:4)))
       buffbfd(2:5)=buffbff(2:5)*buffbfd(1)
    ELSE
       buffbfd(2)=MIN(buffbfd(2),buffbfd(1))
       buffbfd(3)=MIN(buffbfd(3),buffbfd(1)-buffbfd(2))
       buffbfd(4)=MIN(buffbfd(4),buffbfd(1)-SUM(buffbfd(2:3)))
       buffbfd(5)=MAX(0.0_pr  ,buffbfd(1)-SUM(buffbfd(2:4)))
       buffbff(2:5)=buffbfd(2:5)/buffbfd(1)
    END IF
    IF (verb_level>0 .AND. par_rank==0) PRINT *,'FOR bufferfunc, buffbfd=', buffbfd
    IF (verb_level>0 .AND. par_rank==0) PRINT *,'FOR bufferfunc, buffbff=', buffbff

    IF (buffdfd(1)<0.0_pr) THEN
       buffdfd(1)=halfdomsize*buffdff(1)
    ELSE
       buffdff(1)=buffdfd(1)/halfdomsize
    END IF
    IF (ANY(buffdfd(2:4)<0.0_pr)) THEN
       buffdff(2)=MAX(0.0_pr,MIN(1.0_pr,buffdff(2)))
       buffdff(3)=MAX(0.0_pr,MIN(1.0_pr-buffdff(2),buffdff(3)))
       buffdff(4)=MAX(0.0_pr,MIN(1.0_pr-SUM(buffdff(2:3)),buffdff(4)))
       buffdff(5)=MAX(0.0_pr,    1.0_pr-SUM(buffdff(2:4)))
       buffdfd(2:5)=buffdff(2:5)*buffdfd(1)
    ELSE
       buffdfd(2)=MIN(buffdfd(2),buffdfd(1))
       buffdfd(3)=MIN(buffdfd(3),buffdfd(1)-buffdfd(2))
       buffdfd(4)=MIN(buffdfd(4),buffdfd(1)-SUM(buffdfd(2:3)))
       buffdfd(5)=MAX(0.0_pr  ,buffdfd(1)-SUM(buffdfd(2:4)))
       buffdff(2:5)=buffdfd(2:5)/buffdfd(1)
    END IF
    IF (verb_level>0 .AND. par_rank==0) PRINT *,'FOR bufferfunc, buffdfd=', buffdfd
    IF (verb_level>0 .AND. par_rank==0) PRINT *,'FOR bufferfunc, buffdff=', buffdff

    buffDomFrac = MIN(buffcvf(1),buffbff(1),buffdff(1))

    IF (ALLOCATED(bndvals)) DEALLOCATE(bndvals)
    ALLOCATE(bndvals(dim+Nspec+3,2))

    pertdir = 1
    smdir   = 1
    smTPdir = 1
    IF (pert_alldir) pertdir = 0
    IF (sm_alldir)   smdir   = 0
    IF (smTP_alldir) smTPdir = 0
    
    IF (NOT(cuttop)) cuttopat=1.0e12_pr
    IF (NOT(cutbot)) cutbotat=-1.0e12_pr

    IF (cent_j .LE. 0) cent_j = j_mx + cent_j
    cent_n = mxyz(1)*2**(cent_j-1) + 1
    cent_dx = (xyzlimits(2,1)-xyzlimits(1,1))/REAL(cent_n-1,pr)
    IF (ALLOCATED(centrho))   DEALLOCATE(centrho)
    IF (ALLOCATED(centrhoY))  DEALLOCATE(centrhoY)
    IF (ALLOCATED(centY))     DEALLOCATE(centY)
    IF (ALLOCATED(centX))     DEALLOCATE(centX)
    IF (ALLOCATED(centA))     DEALLOCATE(centA)
    IF (ALLOCATED(centN))     DEALLOCATE(centN)
    IF (ALLOCATED(centxs))    DEALLOCATE(centxs)
    ALLOCATE(centrho(cent_n),centrhoY(cent_n),centY(cent_n),centX(cent_n),centA(cent_n),centN(cent_n),centxs(cent_n))
    DO i=1,cent_n
       centxs(i) = REAL(i-1,pr)*cent_dx + xyzlimits(1,1)
    END DO 
    centrhoval  = 1.0_pr
    centrhoYval = (1.0_pr + At)/2.0_pr 
    centYval    = (1.0_pr + At)/2.0_pr 
    centXval    = 0.5_pr
    centxrhoval  = 0.0_pr
    centxrhoYval = 0.0_pr 
    centxYval    = 0.0_pr 
    centxXval    = 0.0_pr

    IF (IC_restart_mode .EQ. 1) eps_init=eps_run

    IF (diffstab.NE.-10) THEN
       delxyz(1:dim)=(xyzlimits(2,1:dim)-xyzlimits(1,1:dim))/REAL(mxyz(1:dim)*2**(j_mx-1),pr)  !delxyz = L/N in every direction on j_mx
       cfldt = MIN(  mydt_orig,cflmax*MINVAL(delxyz)/MAXVAL(SQRT( gamm(:)*Tconst/MW_in(:) ))  )
       IF (diffstab .GT. 0) THEN
          delxyz(1:dim)=(xyzlimits(2,1:dim)-xyzlimits(1,1:dim))/REAL(mxyz(1:dim)*2**(diffstab-1),pr)  !delxyz = L/N in every direction on diffstab
       ELSEIF (diffstab .EQ. -1) THEN
          delxyz(1:dim)=(xyzlimits(2,1:dim)-xyzlimits(1,1:dim))/REAL(mxyz(1:dim)*2**(j_mx-1),pr)  !delxyz = L/N in every direction on j_mx
       ELSEIF (diffstab .EQ. -2) THEN
          delxyz(1:dim)=(xyzlimits(2,1:dim)-xyzlimits(1,1:dim))/REAL(mxyz(1:dim)*2**(j_mx-2),pr)  !delxyz = L/N in every direction on j_mx-1
       ELSEIF (diffstab .EQ. -3) THEN
          delxyz(1:dim)=(xyzlimits(2,1:dim)-xyzlimits(1,1:dim))/REAL(mxyz(1:dim)*2**(j_mn_local  ),pr)  !delxyz = L/N in every direction on j_mn_local+1
       ELSEIF (diffstab .EQ. -4) THEN
          delxyz(1:dim)=(xyzlimits(2,1:dim)-xyzlimits(1,1:dim))/REAL(mxyz(1:dim)*2**(j_mn_local-1),pr)  !delxyz = L/N in every direction on j_mn_local
       ELSE
          delxyz(1:dim)=(xyzlimits(2,1:dim)-xyzlimits(1,1:dim))/REAL(mxyz(1:dim)*2**(j_mx_local-1),pr)  !delxyz = L/N in every direction on j_mx
       END IF
       diffvisc = difffac/2.0_pr/cfldt/SUM(1.0_pr/delxyz**2)
    ELSE
       diffvisc = MAX(diffvisc,0.0_pr)
    END IF
    IF (par_rank .EQ. 0) PRINT *, ' Using diffvisc=', diffvisc
    CFLdiff=0.0_pr
    IF (NOT(diffBC)) diffcfl=-1.0_pr

    CFLnonl=0.1_pr
  END SUBROUTINE user_read_input

  SUBROUTINE ptsdelta(localj,delta,isiterf,pts,curx)
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: localj, isiterf
    INTEGER, INTENT(INOUT) :: pts
    REAL (pr), INTENT(IN) :: delta
    REAL (pr), INTENT(INOUT) :: curx
    REAL (pr) :: curf, delx

       delx=(xyzlimits(2,1)-xyzlimits(1,1))/REAL(mxyz(1)*2**(localj-1),pr)
       curf=0.0_pr
       curx=delx
       DO WHILE (curf < 1.0_pr-threshforpts)
          curx=curx+delx
          pts=pts+2
          IF (isiterf == 1) THEN 
             curf=DERF(curx/delta)
          ELSE
             curf=DTANH(curx/delta)
          END IF
       END DO
  END SUBROUTINE ptsdelta

  SUBROUTINE deltapts(localj,delta,isiterf,myptsacross)
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: localj, isiterf, myptsacross
    REAL (pr), INTENT(INOUT) :: delta
    REAL (pr) :: delhi, dello, curdel, curf, delx, curx
    INTEGER :: curpts, iter, cnthi, cntlo

       delx=(xyzlimits(2,1)-xyzlimits(1,1))/REAL(mxyz(1)*2**(localj-1),pr)
       delhi=1.0_pr
       dello=1.0e-12_pr
       curpts=myptsacross+10
       iter=0

       cnthi=-1
       cntlo=-1

      DO WHILE (cnthi==cntlo)
       cnthi=-1
       curf=0.0_pr
       curx=delx
       DO WHILE (curf < 1.0_pr-threshforpts)
          IF (isiterf == 1) THEN 
             curf=DERF(curx/delhi)
          ELSE
             curf=DTANH(curx/delhi)
          END IF
          curx=curx+delx
          cnthi=cnthi+2
       END DO

       cntlo=-1
       curf=0.0_pr
       curx=delx
       DO WHILE (curf < 1.0_pr-threshforpts)
          IF (isiterf == 1) THEN 
             curf=DERF(curx/dello)
          ELSE
             curf=DTANH(curx/dello)
          END IF
          curx=curx+delx
          cntlo=cntlo+2
       END DO
       IF (cnthi==cntlo) THEN
          delhi=delhi*2.0_pr
          dello=dello/2.0_pr
       END IF
     END DO

       DO WHILE (ABS(curpts - myptsacross) > 1)
          curdel=REAL(myptsacross-cntlo,pr)/REAL(cnthi-cntlo,pr)*(delhi-dello)+dello
          curpts=-1
          curf=0.0_pr
          curx=delx

          DO WHILE (curf < 1.0_pr-threshforpts)
             IF (isiterf == 1) THEN 
                curf=DERF(curx/curdel)
             ELSE
                curf=DTANH(curx/curdel)
             END IF
             curx=curx+delx
             curpts=curpts+2
          END DO
          IF (curpts>myptsacross) THEN
             delhi=curdel
             cnthi=curpts
          ELSE
             dello=curdel
             cntlo=curpts
          END IF
          iter=iter+1
          IF (par_rank.EQ.0) PRINT *, 'For iter=',iter,' delta=',curdel,' pts=',curpts
       END DO
       delta=curdel

  END SUBROUTINE deltapts


  !
  ! calculate any additional variables
  ! 
  ! arg 
  ! flag  - 0 calledwhile adapting to IC, 1 - called in main time integration loop
  !
  ! These additiona variables are calculated and left in real space.
  !
  SUBROUTINE user_additional_vars( t_local, flag )
    USE field
    IMPLICIT NONE

    REAL (pr), INTENT (IN) ::  t_local
    INTEGER , INTENT(IN) :: flag ! 0- called during adaption to IC, 1 called during main integration loop
    REAL (pr) :: cp, R, mygr, cmax, uval
    INTEGER :: l, i, j, myiter
    REAL (pr), DIMENSION(1,nwlt,MAX(dim,Nspecm)) :: du, d2u    
    REAL (pr) :: iterdiff, initdiff, curtau, fofn, deln, delnold, delfofn
    INTEGER, PARAMETER :: meth=1
    CHARACTER(LEN=8):: numstrng
    
    IF (adaptbuffRe) THEN
       IF (dobuffRe) THEN
          CALL user_bufferRe (u(:,nmsk), nwlt)
       ELSE
          u(:,nmsk) = 1.0_pr
       END IF 
    END IF

    d2u(1,:,2) = (1.0_pr - SUM(u(:,nspc(1):nspc(Nspecm)),DIM=2)/u(:,nden))*cp_in(Nspec)
    d2u(1,:,1) = (1.0_pr - SUM(u(:,nspc(1):nspc(Nspecm)),DIM=2)/u(:,nden))/MW_in(Nspec)
    DO l=1,Nspecm
        d2u(1,:,2) = d2u(1,:,2) + u(:,nspc(l))/u(:,nden)*cp_in(l) !cp
        d2u(1,:,1) = d2u(1,:,1) + u(:,nspc(l))/u(:,nden)/MW_in(l) !R
    END DO
    u(:,nprs) = (u(:,neng)-0.5_pr*SUM(u(:,nvel(1):nvel(dim))**2,DIM=2)/u(:,nden))/(d2u(1,:,2)/d2u(1,:,1)-1.0_pr)  ! p
    IF (adapttmp .OR. savetmp) u(:,nton) = u(:,nprs)/u(:,nden)/d2u(1,:,1)
    IF (adaptvelonly) THEN
       DO i=1,dim
          u(:,nvon(i)) = u(:,nvel(i))/u(:,nden)
       END DO
    END IF
    IF (adaptspconly) THEN
       DO i=1,Nspecm
          u(:,nson(i)) = u(:,nspc(i))/u(:,nden)
       END DO
    END IF
    IF (savebuoys) THEN
       u(:,ndpx) = u(:,nprs)
       DO i=1,Nspecm
          u(:,nspc(i)) = u(:,nspc(i))/u(:,nden)
       END DO
       CALL dpdx (u(:,ndpx),nwlt,u(:,nspc(1):nspc(Nspecm)),2*methpd+meth,j_lev,splitFBpress)
       DO i=1,Nspecm
          u(:,nspc(i)) = u(:,nspc(i))*u(:,nden)
       END DO
       DO i=1,nwlt
          mygr=gr_in(Nspec)*(1.0_pr-SUM(u(i,nspc(1):nspc(Nspecm)))   /u(i,nden)   ) 
          DO j=1,Nspecm
             mygr=mygr+gr_in(j)*u(i,nspc(j))   /u(i,nden)   
          END DO
          u(i,nboy) =  u(i,ndpx) + mygr*u(i,nden)
       END DO
       IF (par_rank.EQ.0 .AND. verb_level>0) PRINT *, 'MAXABS of dp/dx+rho*g:', MAXVAL(ABS(u(:,nboy)))
    END IF
    IF( adaptMagVort .OR. saveMagVort .OR. adaptComVort .OR. saveComVort .OR. adaptMagVel .OR. saveMagVel .OR. adaptNormS .OR. saveNormS ) THEN
       DO i=1,dim
          du(1,:,i) = u(:,nvel(i))/u(:,nden)
       END DO
       CALL bonusvars_vel(du(1,:,1:dim))  !4extra - call it
    END IF
    IF( adaptGradY .OR. saveGradY) THEN
       DO i=1,Nspecm
          du(1,:,i) = u(:,nspc(i))/u(:,nden)
       END DO
       CALL bonusvars_spc(du(1,:,1:Nspecm))  !4extra - call it
    END IF
    IF (bonusCFL) THEN   !--Updating cfl
       CFLvort = MAXVAL(u(:,nmvt))*dt
       CFLmods = MAXVAL(u(:,nnms))*dt
       CALL parallel_global_sum(REALMAXVAL=CFLvort)
       CALL parallel_global_sum(REALMAXVAL=CFLmods)
       cfl = MAX(cfl,CFLvort,CFLmods)
       IF (cfl>cflmax) THEN !--Limit time step
          IF (par_rank.EQ.0) PRINT *,'WARNING: dt being limited by cfl>cflmax: dt, dt_new',dt , MIN(dt * cflmax/cfl,dt_original)
          dt = MIN(dtmax,dt * cflmax/cfl,dt_original)
          cfl = cflmax
          dt_changed = .TRUE.
       ELSE IF( dt_changed .AND. t-t_begin<=t_adapt) THEN
          dt = MIN(dt, dt_original, dt * cflmax/cfl)
          !IF (par_rank.EQ.0) PRINT *, 'dt is increased toward original: dt=',dt
          IF (it>0) dt_changed = .FALSE.
          IF( dt < dt_original) dt_changed = .TRUE.
       END IF
    END IF

       IF (modczn) THEN
          IF (t>tczn) THEN
             modczn=.FALSE.
             xyzzone(1,1) = czn_min
             xyzzone(2,1) = czn_max            
            IF (par_rank.EQ.0 .AND. verb_level>0) THEN
             PRINT *, '*****************************************************'
             PRINT *, 'FINALIZING TEMPORARY COORD_ZONE'
             PRINT *, 'COORD_ZONE OUTSIDE OF x=[',xyzzone(1,1),',',xyzzone(2,1),']'
             PRINT *, 'TIME: ', t
             PRINT *, 'STOPPING TIME FOR COORD_ZONE: ', tczn
             PRINT *, '*****************************************************'
            END IF
          END IF
       ELSE  
          IF (tfacczn>0.0_pr .AND. itfacczn>0.0_pr .AND. t>itczn) THEN
             itczn=t_end*10.0_pr
             czn_min = xyzzone(1,1)
             czn_max = xyzzone(2,1)
             xyzzone(1,1) = -Lczn
             xyzzone(2,1) =  Lczn
             cczn = MINVAL(SQRT(gamm/MW_in*Tconst))
             tczn = MAXVAL(ABS(xyzlimits(:,1)))/cczn*tfacczn
            IF (par_rank.EQ.0 .AND. verb_level>0) THEN
             PRINT *, '*****************************************************'
             PRINT *, 'APPLYING TEMPORARY COORD_ZONE'
             PRINT *, 'COORD_ZONE OUTSIDE OF x=[',-Lczn,',',Lczn,']'
             PRINT *, 'MINIMUM WAVE SPEED: ', cczn
             PRINT *, 'MAXIMUM DISTANCE: ', MAXVAL(ABS(xyzlimits(:,1)))
             PRINT *, 'TIME: ', t
             PRINT *, 'STARTING TIME FOR COORD_ZONE: ', itczn
             PRINT *, 'TIME SAFETY FACTOR: ', tfacczn
             PRINT *, 'STOPPING TIME FOR COORD_ZONE: ', tczn
             PRINT *, '*****************************************************'
            END IF
             IF (t<tczn) THEN
                modczn = .TRUE.
             ELSE
                IF (par_rank.EQ.0) PRINT *, '!!!TOO LATE TO START COORD_ZONE: t>tczn!!!'
             END IF
          END IF  
       END IF

       IF (buffRe .AND. buffRewait) THEN
          IF ( (t>xyzlimits(1,1)/c0(2)*(1.0_pr+waitrat)) .AND. (t>xyzlimits(2,1)/c0(1)*(1.0_pr+waitrat)) ) dobuffRe = .TRUE.
       END IF

       IF (savepardom) u(:,ndom) = REAL(par_rank,pr)
  END SUBROUTINE user_additional_vars

  SUBROUTINE bonusvars_vel(velvec)   !4extra - this whole subroutine
    USE field
    IMPLICIT NONE
    REAL (pr), INTENT(IN) :: velvec(nwlt,dim) !velocity
    REAL (pr) :: tmp(nwlt,2*dim) !tmp for vorticity
    INTEGER :: i

    IF( adaptMagVort .OR. saveMagVort .OR. adaptComVort .OR. saveComVort .OR. adaptMagVel .OR. saveMagVel .OR. adaptNormS .OR. saveNormS) THEN
       tmp = 0.0_pr
       ! Calculate the magnitude vorticity
       IF( adaptMagVort .OR. saveMagVort .OR. adaptComVort .OR.  saveComVort ) THEN
          CALL cal_vort (velvec, tmp(:,1:3-MOD(dim,3)), nwlt)
          IF ( adaptMagVort .OR. saveMagVort )  u(:,nmvt) = SQRT(tmp(:,1)**2.0_pr + tmp(:,2)**2.0_pr + tmp(:,3)**2.0_pr )
          IF ( adaptComVort .OR. saveComVort )  THEN
             DO i=1,3
                u(:,ncvt(4-i)) = tmp(:,4-i)
             END DO
          END IF
       END IF
       ! Calculate the magnitude of Sij (sqrt(SijSij) )
       ! s(:,1) = s_11
       ! s(:,2) = s_22
       ! s(:,3) = s_33
       ! s(:,4) = s_12
       ! s(:,5) = s_13
       ! s(:,6) = s_23
       tmp = 0.0_pr
       IF( adaptNormS .OR. saveNormS) THEN
          CALL Sij( velvec ,nwlt, tmp(:,1:2*dim), u(:,nnms), .TRUE. )
       END IF
       ! Calculate the magnitude of velocity
       IF( adaptMagVel .OR. saveMagVel) THEN
          u(:,nmvl) = 0.5_pr*(SUM(velvec(:,:)**2.0_pr,DIM=2))
       END IF
    END IF
  END SUBROUTINE bonusvars_vel

  SUBROUTINE bonusvars_spc(spcvec)   !4extra - this whole subroutine
    USE field
    IMPLICIT NONE
    REAL (pr), INTENT(IN) :: spcvec(nwlt,Nspecm) !species
    REAL (pr), DIMENSION(Nspecm,nwlt,dim) :: du, d2u    
    INTEGER :: i
    INTEGER, PARAMETER :: meth=1

    IF( adaptGradY .OR. saveGradY) THEN
       CALL c_diff_fast(spcvec(:,1:Nspecm), du(1:Nspecm,:,:), d2u(1:Nspecm,:,:), j_lev, nwlt,meth, 10, Nspecm, 1, Nspecm)  !dY/dx
       DO i=1,Nspecm
          u(:,ngdy(i)) = SUM(du(i,:,:)**2,DIM=2) !|dY/dx_i*dY/dx_i|
       END DO
    END IF
  END SUBROUTINE bonusvars_spc

  !
  ! calculate any additional scalar variables
  ! 
  ! arg 
  ! flag  - 0 calledwhile adapting to IC, 1 - called in main time integration loop
  !
  SUBROUTINE user_scalar_vars( flag )
    IMPLICIT NONE
    INTEGER , INTENT(IN) :: flag ! 0- called during adaption to IC, 1 called during main integration loop

  END SUBROUTINE user_scalar_vars

  !
  !************ Calculating Scales ***************************
  !
  ! Note the order of the components in the scl array
  ! correspond to u_tn, v_tn, w_tn, u_tn-1, v_tn-1, w_tn-1, u_tn-2, v_tn-2, w_tn-2
  ! 
  SUBROUTINE user_scales(flag,use_default, u_loc, nlocal, ne_local, l_n_var_adapt , l_n_var_adapt_index, &
       scl, scl_fltwt) !add
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: flag ! 0 during initial adaptation, then 1 during time advancement
    LOGICAL , INTENT(INOUT) :: use_default
    INTEGER, INTENT (IN) :: nlocal, ne_local
    REAL (pr), DIMENSION (1:nlocal,1:ne_local), INTENT (IN) :: u_loc
    LOGICAL , INTENT (IN) :: l_n_var_adapt(ne_local)
    INTEGER , INTENT (IN) :: l_n_var_adapt_index(1:ne_local)
    REAL (pr), DIMENSION (1:ne_local), INTENT (INOUT) :: scl
    REAL (pr), DIMENSION (1:ne_local) :: mean
    REAL (pr) ,  INTENT(IN) ::scl_fltwt !weight for temporal filter on scl
    REAL (pr) :: floor, tmp,tmpmax,tmpmin,tmpsum
    INTEGER :: i, ie, ie_index, tmpint
    REAL (pr), SAVE, ALLOCATABLE, DIMENSION (:) :: scl_old
    LOGICAL, SAVE :: startup_init = .TRUE.

    !
    ! Ignore the output of this routine and use default scales routine
    !
    use_default = .FALSE. 

    IF( .NOT. use_default ) THEN
       !IF (par_rank.EQ.0) PRINT *,'Use user_scales() <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<'
       !
       ! ALLOCATE scl_old if it was not done previously in user_scales
       !
       IF( startup_init .AND. .NOT. ALLOCATED(scl_old) ) THEN
          ALLOCATE( scl_old(1:ne_local) )
          scl_old = 0.0_pr
       END IF

       floor = 1.0e-12_pr
       scl   =1.0_pr
       !
       ! Calculate scl per component
       !
       mean = 0.0_pr
       DO ie=1,ne_local
          !IF(l_n_var_adapt(ie)) THEN
          !   ie_index = l_n_var_adapt_index(ie)
          IF( Scale_Meth == 1 ) THEN ! Use Linf scale
             !tmpmax=MAXVAL ( u_loc(1:nlocal,ie_index) )
             !tmpmin=MINVAL ( u_loc(1:nlocal,ie_index) )
             tmpmax=MAXVAL ( u_loc(1:nlocal,ie) )
             tmpmin=MINVAL ( u_loc(1:nlocal,ie) )
             CALL parallel_global_sum(REALMAXVAL=tmpmax)
             CALL parallel_global_sum(REALMINVAL=tmpmin)
             mean(ie)= 0.5_pr*(tmpmax+tmpmin)
          ELSE IF ( Scale_Meth == 2 ) THEN ! Use L2 scale
             !tmpsum=SUM( u_loc(1:nlocal,ie_index) )
             tmpsum=SUM( u_loc(1:nlocal,ie) )
             tmpint=nlocal
             CALL parallel_global_sum(REAL=tmpsum)
             CALL parallel_global_sum(INTEGER=tmpint)
             mean(ie)= tmpsum/REAL(tmpint,pr)
          ELSE IF ( Scale_Meth == 3 ) THEN ! Use L2 scale using dA
             !tmpsum=SUM( u_loc(1:nlocal,ie_index)*dA )
             tmpsum=SUM( u_loc(1:nlocal,ie)*dA )
             CALL parallel_global_sum(REAL=tmpsum)
             mean(ie)= tmpsum/ sumdA_global 
          END IF
          !END IF
       END DO
       DO ie=1,ne_local
          IF( Scale_Meth == 1 ) THEN ! Use Linf scale
             !scl(ie)= MAXVAL ( ABS( u_loc(1:nlocal,ie_index) - mean(ie) ) )
             scl(ie)= MAXVAL ( ABS( u_loc(1:nlocal,ie) - mean(ie) ) )
             CALL parallel_global_sum( REALMAXVAL=scl(ie) )
          ELSE IF ( Scale_Meth == 2 ) THEN ! Use L2 scale
             !scl(ie)= SUM( (u_loc(1:nlocal,ie_index)-mean(ie))**2 )
             scl(ie)= SUM( (u_loc(1:nlocal,ie)-mean(ie))**2 )
             tmpint=nlocal
             CALL parallel_global_sum( REAL=scl(ie) )
             CALL parallel_global_sum(INTEGER=tmpint)
             scl(ie)=SQRT(scl(ie)/REAL(tmpint,pr))
          ELSE IF ( Scale_Meth == 3 ) THEN ! Use L2 scale using dA
             !scl(ie)= SUM( (u_loc(1:nlocal,ie_index)-mean(ie))**2*dA )
             scl(ie)= SUM( (u_loc(1:nlocal,ie)-mean(ie))**2*dA )
             CALL parallel_global_sum(REAL=scl(ie))
             scl(ie)=SQRT(scl(ie)/sumdA_global)
          ELSE
             PRINT *, 'Error in Scales(), Unknown Scale_Meth type: ', Scale_Meth
             PRINT *, 'Known Scale_Meth types: 1 = Linf scale, 2 = L2 scale '
             PRINT *, 'Exiting ...'
             stop
          END IF
          !END IF
       END DO
       IF (adaptspconly) THEN
          DO ie=nson(1),nson(Nspecm)
             scl(ie) = SUM( (MIN(ABS(u_loc(1:nlocal,ie)-pureY(1)),ABS(u_loc(1:nlocal,ie)-pureY(2))))**2*dA )
             CALL parallel_global_sum(REAL=scl(ie))
             scl(ie)=SQRT(scl(ie)/sumdA_global)
          END DO
       END IF
       !
       ! take appropriate vector norm over scl(1:dim)
       IF (adaptvelonly) THEN
          IF( Scale_Meth == 1 ) THEN ! Use Linf scale
             scl(nvon(1):nvon(dim)) = MAXVAL(scl(nvon(1):nvon(dim)))
          ELSE IF ( Scale_Meth == 2 ) THEN ! Use L2 scale
             scl(nvon(1):nvon(dim)) = SQRT(SUM(scl(nvon(1):nvon(dim))**2))
          ELSE IF ( Scale_Meth == 3 ) THEN ! Use L2 scale using dA
             scl(nvon(1):nvon(dim)) = SQRT(SUM(scl(nvon(1):nvon(dim))**2))
          END IF
       END IF
       IF( Scale_Meth == 1 ) THEN ! Use Linf scale
          scl(nvel(1):nvel(dim)) = MAXVAL(scl(nvel(1):nvel(dim)))
       ELSE IF ( Scale_Meth == 2 ) THEN ! Use L2 scale
          scl(nvel(1):nvel(dim)) = SQRT(SUM(scl(nvel(1):nvel(dim))**2)) !now velocity vector length, rather than individual component
       ELSE IF ( Scale_Meth == 3 ) THEN ! Use L2 scale using dA
          scl(nvel(1):nvel(dim)) = SQRT(SUM(scl(nvel(1):nvel(dim))**2)) !now velocity vector length, rather than individual component
       END IF
       scl(nden) = 1.0_pr !MINVAL(u_loc(1:nlocal,nden))
       !CALL parallel_global_sum(REALMINVAL=scl(nden))
       scl(neng)= Pint/(gammR-1.0_pr) !MINVAL(u_loc(1:nlocal,neng))
       !CALL parallel_global_sum(REALMINVAL=scl(neng))
       scl(nprs) = Pint !MINVAL(u_loc(1:nlocal,nprs))
       !CALL parallel_global_sum(REALMINVAL=scl(nprs))
       scl(nspc(1):nspc(Nspecm))= 1.0_pr !MINVAL(u_loc(1:nlocal,nspc(ie)))
       !DO ie=1,Nspecm
       !   scl(nspc(ie))= MINVAL(u_loc(1:nlocal,nspc(ie)))
       !   CALL parallel_global_sum(REALMINVAL=scl(nspc(ie)))
       !END DO
       !
       ! Print out new scl
       !
       IF (adaptbuffRe .AND. maskscl>0.0_pr) scl(nmsk)=maskscl
       IF ((adapttmp .OR. savetmp) .AND. tempscl>floor) scl(nton)=tempscl
       IF ((adapttmp .OR. savetmp) .AND. adaptvelonly .AND. ABS(tempscl)<floor) scl(nton)=MAX(scl(nton),scl(nvon(1))**2)
       IF (adaptspconly .AND. specscl>0.0_pr) scl(nson(1):nson(Nspecm))=specscl
       DO ie=1,ne_local
          !IF(l_n_var_adapt(ie)) THEN
          !this is done if one of the variable is exactly zero inorder not to adapt to the noise
          IF(scl(ie) .le. floor) scl(ie)=1.0_pr 
          tmp = scl(ie)
          ! temporally filter scl
          IF(.not. startup_init ) scl(ie) = scl_fltwt * scl_old(ie) + (1.0_pr - scl_fltwt) * scl(ie)
          scl_old(ie) = scl(ie)           !save scl for this time step
          scl(ie) = scaleCoeff(ie) * scl(ie)
          IF (par_rank.EQ.0 .AND. verb_level .GT. 0) THEN
             WRITE (6,'("Scale factor eqn", I4 ," = ", es11.4 ," scalecoeff = ",es11.4 )') &
                     ie, scl(ie), scaleCoeff(ie)
             WRITE (6,'("(before filtering = ",es11.4,")" )') tmp
          END IF
          !END IF
       END DO
       
       !scl_old = scl !save scl for this time step
       startup_init = .FALSE.
    END IF
    
    IF (par_rank.EQ.0) THEN
       WRITE(*,'("************************ SCALES **************************")')
       DO ie=1,ne_local
          WRITE(*,'("***", A,E12.5,"        ***")') u_variable_names(ie), scl(ie)
       END DO
       WRITE(*,'("**********************************************************")')
    END IF
  END SUBROUTINE user_scales

  SUBROUTINE user_cal_cfl (use_default, ulocal, cfl_out)
    USE precision
    USE sizes
    USE pde
    IMPLICIT NONE
    LOGICAL , INTENT(INOUT) :: use_default
    REAL (pr),                                INTENT (INOUT) :: cfl_out
    REAL (pr), DIMENSION (nwlt,n_integrated), INTENT (IN)    :: ulocal
    INTEGER                    :: i
    REAL (pr)                  :: flor, npert, CFLtemp
    REAL (pr), DIMENSION(nwlt,dim) :: cfl, cfl_conv, cfl_acou
    REAL (pr), DIMENSION(dim,nwlt) :: h_arr
    REAL (pr), DIMENSION(dim) :: delxyz
    REAL (pr), DIMENSION(nwlt) :: gam, sos, props1, props2, props3
    INTEGER :: l, mynwlt_global, fullnwlt, maxnwlt, minnwlt, avgnwlt
    REAL (pr), DIMENSION (nwlt) :: ReU
    REAL (pr), DIMENSION (nwlt,2) :: bigU

    IF (it>0) dt_original = mydt_orig

    use_default = .FALSE.

    flor = 1e-12_pr
    cfl_out = flor

    CALL get_all_local_h (h_arr)
  
    gam(:) = (1.0_pr-SUM(ulocal(:,nspc(1):nspc(Nspecm)),DIM=2)/ulocal(:,nden))*cp_in(Nspec) !cp_Nspec
    sos(:) = (1.0_pr-SUM(ulocal(:,nspc(1):nspc(Nspecm)),DIM=2)/ulocal(:,nden))/MW_in(Nspec) !R_Nspec
    DO l=1,Nspecm
       gam(:) = gam(:) + ulocal(:,nspc(l))/ulocal(:,nden)*cp_in(l) !cp
       sos(:) = sos(:) + ulocal(:,nspc(l))/ulocal(:,nden)/MW_in(l) !R
    END DO
    gam(:) = gam(:)/(gam(:)-sos(:))
    sos(:) = SQRT(gam(:)*(gam(:)-1.0_pr)*(ulocal(:,neng)-0.5_pr*SUM(ulocal(:,nvel(1):nvel(dim))**2,DIM=2)/ulocal(:,nden))/ulocal(:,nden))! spd of snd = sqrt(gamma p/rho)
    maxsos = MAXVAL(sos(:))
    CALL parallel_global_sum(REALMAXVAL=maxsos)

    maxMa = MAXVAL(SQRT(SUM(ulocal(:,nvel(1):nvel(dim))**2,DIM=2)/ulocal(:,nden)**2)/sos(:)*SQRT(gam(:)))
    CALL parallel_global_sum(REALMAXVAL=maxMa)

    DO i=1,dim
       cfl(:,i)      = ( ABS(ulocal(:,nvel(i))/ulocal(:,nden))+sos(:) ) * dt/h_arr(i,:)
       cfl_conv(:,i) = ( ABS(ulocal(:,nvel(i))/ulocal(:,nden))        ) * dt/h_arr(i,:)
       cfl_acou(:,i) = (                                       sos(:) ) * dt/h_arr(i,:)
    END DO
    CFLnonl = MAXVAL(cfl(:,:))
    CFLconv = MAXVAL(cfl_conv(:,:))
    CFLacou = MAXVAL(cfl_acou(:,:))
    CALL parallel_global_sum(REALMAXVAL=CFLnonl)
    CALL parallel_global_sum(REALMAXVAL=CFLconv)
    CALL parallel_global_sum(REALMAXVAL=CFLacou)
    CFLreport = CFLnonl

    delxyz(1:dim)=(xyzlimits(2,1:dim)-xyzlimits(1,1:dim))/REAL(mxyz(1:dim)*2**(j_lev-1),pr) 
    CFLvisc = 2.0_pr*dt/Re*MAX(1.0_pr/(1.0_pr-At),Ma**2*MAXVAL(gamm-1.0_pr))*SUM(1.0_pr/(delxyz**2))
    CFLspec = 2.0_pr*dt/Re/Sc*MAXVAL(gamm)*SUM(1.0_pr/(delxyz**2))
    CFLthrm = 2.0_pr*dt/Re/Pra*MAXVAL(gamm-1.0_pr)*SUM(1.0_pr/(delxyz**2))

    CALL user_bufferRe (ReU, nwlt)
    props1(:) = (1.0_pr-SUM(ulocal(:,nspc(1):nspc(Nspecm)),DIM=2)/ulocal(:,nden))*mu_in(Nspec) !mu_Nspec
    DO l=1,Nspecm
       props1(:) = props1(:) + ulocal(:,nspc(l))/ulocal(:,nden)*mu_in(l) !mu
    END DO
    IF (kinevisc .GT. 0) THEN
       CALL user_buffkine (gam(:), ulocal(:,nden), ng)
       props1(:) = props1(:)*gam(:)
    END IF
    CFLbffv = MAXVAL(2.0_pr*dt*ReU(:)*props1(:)/ulocal(:,nden)*SUM(1.0_pr/h_arr(:,:)**2,DIM=1)) !dt/Re_local/delta_local/rho_local
    CALL parallel_global_sum(REALMAXVAL=CFLbffv)

    props1(:) = (1.0_pr-SUM(ulocal(:,nspc(1):nspc(Nspecm)),DIM=2)/ulocal(:,nden))*cp_in(Nspec) !cp_Nspec
    props2(:) = (1.0_pr-SUM(ulocal(:,nspc(1):nspc(Nspecm)),DIM=2)/ulocal(:,nden))/MW_in(Nspec) !R_Nspec
    props3(:) = (1.0_pr-SUM(ulocal(:,nspc(1):nspc(Nspecm)),DIM=2)/ulocal(:,nden))*kk_in(Nspec) !kk_Nspec
    DO l=1,Nspecm
       props1(:) = props1(:) + ulocal(:,nspc(l))/ulocal(:,nden)*cp_in(l) !cp
       props2(:) = props2(:) + ulocal(:,nspc(l))/ulocal(:,nden)/MW_in(l) !R
       props3(:) = props3(:) + ulocal(:,nspc(l))/ulocal(:,nden)*kk_in(l) !kk
    END DO
    IF (kinevisc .GT. 0) props3(:) = props3(:)*gam(:)
    CFLbfft = MAXVAL(2.0_pr*dt*ReU(:)*props3(:)/ulocal(:,nden)/(props1(:)-props2(:))*SUM(1.0_pr/h_arr(:,:)**2,DIM=1)) !dt/Re_local/delta_local/rho_local
    CALL parallel_global_sum(REALMAXVAL=CFLbfft)

    props3(:) = props1(:)
    props1(:) = (1.0_pr-SUM(ulocal(:,nspc(1):nspc(Nspecm)),DIM=2)/ulocal(:,nden))*bD_in(Nspec) !bD_Nspec
    DO l=1,Nspecm
       props1(:) = props1(:) + ulocal(:,nspc(l))/ulocal(:,nden)*bD_in(l) !bD
    END DO
    CFLbffs = MAXVAL(2.0_pr*dt*ReU(:)*props1(:)*SUM(1.0_pr/h_arr(:,:)**2,DIM=1)) !dt/Re_local/delta_local/rho_local
    CALL parallel_global_sum(REALMAXVAL=CFLbffs)

    CALL user_bufferfunc(bigU, nwlt, buffdfd, polydiff)  !bigU is 1 in zone, 0 outside of zone; bigU(:,1) is for bot, bigU(:,2) is for top 
    IF (diffstab .EQ. 0) THEN
       ReU = difffac/2.0_pr/MAX(MIN(cflmax/CFLnonl*dt,mydt_orig),dt)/SUM(1.0_pr/h_arr**2,DIM=1)*(bigU(:,1) + bigU(:,2)) !visc(x,y,z)
    ELSE
       ReU = diffvisc*(bigU(:,1) + bigU(:,2)) !visc(x,y,z)
    END IF

    CFLdiff=MAXVAL(2.0_pr*ReU(:)*dt*SUM(1.0_pr/h_arr(:,:)**2,DIM=1))
    CALL parallel_global_sum(REALMAXVAL=CFLdiff)
    IF (NOT(diffBC)) CFLdiff = 0.0_pr 

    CFLtemp = 0.0_pr
    IF (ComboIMEXRK) THEN
       IF (visccfl>0.0_pr) CFLtemp = MAX(CFLtemp,CFLvisc,CFLthrm,CFLspec,CFLbffv,CFLbfft,CFLbffs)
       IF (diffcfl>0.0_pr) CFLtemp = MAX(CFLtemp,CFLdiff)
       IF (CFLtemp .GT. imexcfl) THEN
          time_integration_method = TIME_INT_IMEX
       ELSE
          time_integration_method = TIME_INT_RK
       END IF
    END IF

    !IF (time_integration_method == TIME_INT_RK .AND. NOT(ComboIMEXRK)) THEN
       IF (visccfl>0.0_pr) CFLreport = MAX(CFLreport,cflmax/ABS(visccfl)*MAX(CFLvisc,CFLthrm,CFLspec,CFLbffv,CFLbfft,CFLbffs))
       IF (diffcfl>0.0_pr) CFLreport = MAX(CFLreport,cflmax/ABS(diffcfl)*CFLdiff)
    !END IF

    mynwlt_global = nwlt
    fullnwlt = PRODUCT(mxyz(1:dim))
    CALL parallel_global_sum (INTEGER=mynwlt_global)
    npert = REAL(mynwlt_global)
    DO i=1,dim
       npert = npert/REAL(mxyz(i)*2**(j_lev-1)+1-prd(i),pr)
    END DO
    avgnwlt = mynwlt_global/par_size 
    maxnwlt = nwlt
    minnwlt = -nwlt
    CALL parallel_global_sum(INTEGERMAXVAL=maxnwlt)
    CALL parallel_global_sum(INTEGERMAXVAL=minnwlt)
    minnwlt=-minnwlt
    MAreport = maxMa
    IF (convcfl) THEN
       cfl_out = MAX(cfl_out, CFLconv)
    ELSE
       cfl_out = MAX(cfl_out, CFLreport)
    END IF
    IF (par_rank.EQ.0) THEN
       PRINT *, '**********************************************************'
       PRINT *, 'case:    ', file_gen(1:LEN_TRIM(file_gen)-1)
       PRINT *, 'max Ma = ', maxMa
       PRINT *, 'CFL    = ', cfl_out
       PRINT *, 'it     = ', it,             '  t       = ', t
       PRINT *, 'iwrite = ', iwrite-1,       '  dt      = ', dt 
       PRINT *, 'j_mx   = ', j_mx,           '  dt_orig = ', dt_original
       PRINT *, 'j_lev  = ', j_lev,          '  twrite  = ', twrite
       PRINT *, 'nwlt_g = ', mynwlt_global,  '  cpu     = ', timer_val(2)
       PRINT *, 'n_mvec = ', fullnwlt,       '  n%      = ', npert
       PRINT *, 'nwlt   = ', nwlt,           '  eps     = ', eps
       PRINT *, 'nwlt_mx= ', maxnwlt,        '  nwlt_av = ', avgnwlt
       PRINT *, 'nwlt_mn= ', minnwlt,        '  p_size  = ', par_size
       PRINT *, '**********************************************************'
    END IF
    IF (MOD(PLpinf,2)==1) maxMa=Ma
    maxMa = MIN(maxMa,0.9_pr)
  END SUBROUTINE user_cal_cfl

  SUBROUTINE user_init_sgs_model( )
    IMPLICIT NONE
  END SUBROUTINE user_init_sgs_model

  SUBROUTINE  user_sgs_force (u_loc, nlocal)
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: nlocal
    REAL (pr), DIMENSION (nlocal,n_integrated), INTENT (INOUT) :: u_loc
  END SUBROUTINE  user_sgs_force

  FUNCTION user_sound_speed (uin, neq, nwlt)
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: nwlt, neq
    REAL (pr), DIMENSION (nwlt,neq), INTENT(IN) :: uin
    REAL (pr), DIMENSION (nwlt) :: user_sound_speed
    user_sound_speed(:) = 0.0_pr
  END FUNCTION user_sound_speed

  SUBROUTINE  user_pre_process
    USE field
    IMPLICIT NONE
    INTEGER :: i, l, ie, shift
    CHARACTER(LEN=8):: numstrng
    CHARACTER(LEN=100):: commandstrng
    IF (boundY) THEN
       DO i=1,nwlt
          u(i,nspc(1)) = MIN(MAX(u(i,nspc(1)),0.0_pr),u(i,nden))
       END DO
       DO l=2,Nspecm
          DO i=1,nwlt
             u(i,nspc(l)) = MIN(MAX(u(i,nspc(l)),0.0_pr),u(i,nden)-SUM(u(i,nspc(1):nspc(l-1))))
          END DO
       END DO
    END IF
    IF (iwriteclean.LT.iwrite-1) THEN
       IF (useorgfiles .AND. par_rank.EQ.0) THEN
          WRITE (numstrng,'(I8)') iwriteclean+1
          commandstrng = "./orgfiles.sh results/"//file_gen(1:LEN_TRIM(file_gen))//" "//numstrng
          CALL system(commandstrng)
          PRINT *,'Organized .res files for iwrite <', iwriteclean+1
       END IF
       IF (savemyres .AND. saved_one) THEN
          OPEN(unit=57,file=TRIM(cur_file_wlt)//".myres")
          WRITE(57,*) dim
          WRITE(57,*) par_size
          WRITE(57,*) nwlt
          WRITE(57,*) x
          
          !WRITE(57,*) u(:,nden)
          !WRITE(57,*) u(:,nprs)
          IF (adaptspconly) THEN
             WRITE(57,*) u(:,nson(1))
          ELSE
             WRITE(57,*) u(:,nspc(1))
          END IF
          !IF (adaptvelonly) THEN
          !   WRITE(57,*) u(:,nvon(1):nvon(dim))
          !ELSE
          !   WRITE(57,*) u(:,nvel(1):nvel(dim))
          !END IF

          CLOSE(UNIT=57)
       END IF
       iwriteclean = iwrite-1
    END IF
  END SUBROUTINE user_pre_process

  SUBROUTINE  user_post_process
    USE field
    IMPLICIT NONE
    CALL user_pre_process
  END SUBROUTINE user_post_process

END MODULE user_case
