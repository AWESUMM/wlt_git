!$
!!$!****************************************************************************
!!$!* Module for adding Brinkman Penalization terms onto the full NS equations *
!!$!*  -Only valid for single species 10/7/2011                                *
!!$!*  -R is scaled to 1.0
!!$!****************************************************************************
!!$
!!$

MODULE user_case

  USE precision
  USE wlt_vars
  USE wavelet_filters_mod
  USE elliptic_mod
  USE elliptic_vars
  USE wlt_trns_mod
  USE wlt_trns_vars
  USE wlt_trns_util_mod
  USE io_3d_vars
  USE util_mod
  USE util_vars
  USE share_consts
  USE pde
  USE sizes
  USE share_kry
  USE vector_util_mod
  USE field
  USE input_file_reader
  USE parallel
  USE hyperbolic_solver
  USE equations_compressible
  USE freundBC 

  !
  ! case specific variables
  !
  REAL (pr), PROTECTED :: Re, Pra, Sc, Fr, Ma

  REAL (pr), ALLOCATABLE, DIMENSION (:) :: Lxyz
  INTEGER :: n_var_pressure  ! start of pressure in u array
  INTEGER :: nmsk,n_util
  LOGICAL ::  saveUtil  !4extra - this whole line
  REAL (pr) :: Lczn, tfacczn, itfacczn, itczn, tczn, cczn, czn_min, czn_max
  LOGICAL :: modczn

  REAL (pr) :: grav_coeff
  LOGICAL :: dynamic_gravity
  REAL (pr), DIMENSION(20) :: Re_tau_hist, Re_bulk_hist, grav_coeff_hist    !vectors with spatially averaged values for the previous 1000 steps
  REAL (pr) :: Re_tau, Re_bulk, Re_tau_target                                                !spatio-temporal averaged values

  LOGICAL :: mesh_clustering = .FALSE.
  REAL (pr) :: mesh_cluster_param 

  INTEGER :: hyper_it = 0  ! for temporary hyperbolic solver application - often used to smooth local instabilities
  INTEGER :: hyper_it_limit = 0  ! for temporary hyperbolic solver application - often used to smooth local instabilities
  LOGICAL :: filter_field

CONTAINS

   SUBROUTINE  user_setup_pde ( VERB ) 
    USE sgs_compressible 
    USE variable_mapping
    IMPLICIT NONE
    LOGICAL, OPTIONAL :: VERB         ! print debug info
    INTEGER :: i, idim, l
 
    IF (par_rank.EQ.0) THEN
       PRINT * ,''
       PRINT *, '**********************Setting up PDE*****************'
       PRINT * ,'CASE: Compressible Mixing Layer '
       PRINT *, '*****************************************************'
    END IF

    ! Set up compressible equation variables 
    CALL compressible_setup_pde
    IF( sgsmodel_compress .NE. sgsmodel_compress_none ) CALL sgs_compress_setup_pde

    IF (imask_obstacle) THEN
        CALL register_var( 'Mask',       integrated=.FALSE.,  adapt=(/.TRUE.,.TRUE./),   interpolate=(/.TRUE.,.TRUE./),   exact=(/.FALSE.,.FALSE./),   saved=.TRUE., req_restart=.TRUE. )
    END IF

    IF ( saveUtil) THEN 
        CALL register_var( 'Utility',       integrated=.FALSE.,  adapt=(/.FALSE.,.FALSE./),   interpolate=(/.TRUE.,.TRUE./),   exact=(/.FALSE.,.FALSE./),   saved=.TRUE., req_restart=.TRUE. )
    END IF


    CALL setup_mapping() 
    scaleCoeff = 1.0_pr

    ! Finalize setup of compressible equations 
    CALL compressible_finalize_setup_pde
    IF( sgsmodel_compress .NE. sgsmodel_compress_none ) CALL sgs_compress_finalize_setup_pde

    n_var_pressure  = n_prs  ! redundant, but required by code.  Unused in this module
    IF(hypermodel .NE. 0 ) THEN
      n_var_mom_hyper(1:dim) = n_mom(1:dim)
      n_var_den_hyper = n_den
    END IF

    nmsk = get_index('Mask') 
    n_util = get_index('Utility') 
    
    IF(hypermodel .NE. 0) THEN
       IF(ALLOCATED(n_var_hyper)) DEALLOCATE(n_var_hyper)
       ALLOCATE(n_var_hyper(1:n_var))
       n_var_hyper = .FALSE.
       !n_var_hyper(n_den) = .TRUE.
       n_var_hyper(n_mom(1:dim)) = .TRUE.
       n_var_hyper(n_eng) = .TRUE.
       IF ( n_mvort .GT. 0) n_var_hyper(n_mvort) = .TRUE.  ! Then the magnitude of vorticity is being tracked and saved
       !n_var_hyper = .TRUE.


       IF(ALLOCATED(n_var_hyper_active)) DEALLOCATE(n_var_hyper_active)
       ALLOCATE(n_var_hyper_active(1:n_integrated))
       n_var_hyper_active = .FALSE.
      ! n_var_hyper_active(n_den) = .TRUE.
!       n_var_hyper_active(n_eng) = .TRUE.
       n_var_hyper_active = .TRUE.
       IF( sgsmodel_compress .EQ. sgsmodel_compress_ksgs ) n_var_hyper_active(n_var_k) = .FALSE.  ! Do not smooth the ksgs field 

    END IF


    IF (par_rank.EQ.0) THEN
       PRINT *, 'n_integrated = ',n_integrated 

       PRINT *, 'n_var = ',n_var 
       PRINT *, 'n_var_exact = ',n_var_exact 
       PRINT *, '*******************Variable Names*******************'
       DO i = 1,n_var
          WRITE (*, u_variable_names_fmt) u_variable_names(i)
       END DO
       PRINT *, '****************************************************'
    END IF

  END SUBROUTINE  user_setup_pde


  !
  ! Set the exact solution for comparison to the simulated solution
  !
  ! u          - array to fill in the exact solution
  ! nlocal       - number of active wavelets   
  ! ne_local        - total number of equations
  ! t          - time of current time step 
  ! l_n_var_exact_soln_index - index into the elements of u for which we need to 
  !                            find the exact solution
  SUBROUTINE  user_exact_soln (u, nlocal,  t_local, l_n_var_exact_soln)  
    USE variable_mapping
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: nlocal
    REAL (pr), INTENT (IN) ::  t_local
    REAL (pr), DIMENSION (nlocal,n_var_exact), INTENT (INOUT) :: u
    LOGICAL , INTENT (IN) :: l_n_var_exact_soln(n_var)
    INTEGER :: i

  END SUBROUTINE  user_exact_soln

  SUBROUTINE user_initial_conditions (u, nlocal, ne_local, t_local, scl, scl_fltwt,iter)
    USE variable_mapping
    USE equations_compressible_utils
    USE penalization
    USE parallel 
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: nlocal, ne_local
    REAL (pr), DIMENSION (nlocal,ne_local), INTENT (INOUT) :: u
    REAL (pr)  :: scl(1:n_var),scl_fltwt
    REAL (pr), INTENT (IN) :: t_local
    INTEGER, INTENT (INOUT) :: iter
    INTEGER :: i, j, idim, l, ie, ii, k

    REAL (pr), DIMENSION(nlocal,1:dim) :: x_phys 
    REAL (pr), DIMENSION(2,1:dim) :: x_phys_limits

    REAL (pr) :: tmp1, tmp2 
    INTEGER :: nwlt_total, nwlt_max

    REAL (pr) :: domain_length

    ! PARAMETERS
    REAL (pr), PARAMETER :: momentum_thickness = 1.0_pr

    x_phys(:,:) = user_mapping( x, nlocal, t )
    DO idim = 1,dim
       x_phys_limits(1,idim) = MINVAL( x_phys(:,idim) )
       x_phys_limits(2,idim) = MAXVAL( x_phys(:,idim) )
       CALL parallel_global_sum( REALMINVAL=x_phys_limits(1,idim) )
       CALL parallel_global_sum( REALMAXVAL=x_phys_limits(2,idim) )
       IF ( par_rank .EQ. 0 )  PRINT *, idim,'X_phys min = ', x_phys_limits(1,idim), 'max = ',x_phys_limits(2,idim)      
    END DO

  IF (IC_restart_mode > 0 ) THEN !in the case of restart
     !do nothing
  ELSE IF (IC_restart_mode .EQ. 0) THEN

     !Set Density
     u(:,n_den)              = 1.0_pr
     IF (Nspec>1) THEN
        u(:,n_spc(:)) = 0.0_pr
        u(:,n_spc(1)) = 0.5_pr + 0.5_pr * TANH( x(:,2) / ( 2.0_pr * momentum_thickness ) )
        u(:,n_den)    = MW_in(Nspec)/MW_in(1) * ( 1.0_pr - SUM( u(:,n_spc(1:Nspecm)), DIM=2 ) )   
        DO i = 1,Nspecm
          u(:,n_den)    = u(:,n_den) + MW_in(i)/MW_in(1) * u(:,n_spc(1))  
        END DO 

        ! Convert to mass fraction 
        DO i = 1,Nspecm
           u(:,n_spc(i)) = u(:,n_den)*u(:,n_spc(i))
        END DO 
     END IF

     !Set Background Momentum
     DO i=1,dim
        u(:,n_mom(i)) = 0.0_pr
     END DO

     ! VELOCITY

     ! Perturbations
     ! ***********  Roussel  ****************
     domain_length = xyzlimits(2,1) - xyzlimits(1,1)
     u(:,n_mom(1)) = 0.5_pr * COS(2.0_pr*PI*(x(:,1)+x(:,3))/domain_length)&
                        + 0.5_pr * COS(2.0_pr*PI*(x(:,1)-x(:,3))/domain_length)  
     DO i=1,2
        u(:,n_mom(1)) = u(:,n_mom(1)) + COS(2.0_pr**(i+1)*PI*x(:,1)/ domain_length)
     END DO
     u(:,n_mom(1)) = u(:,n_mom(1))/(3.0_pr*COSH(x(:,2))**2) 

     ! ***********  Roussel  ****************

     !!Enforce locality
     DO idim = 1,dim
        !u(:,n_mom(idim)) = u(:,n_mom(idim)) / ( COSH( x(:,2) )**2 )
        u(:,n_mom(idim)) = u(:,n_mom(idim)) * EXP( -ABS(x(:,2) ) )
     END DO

     CALL add_smooth_noise( u(:,n_mom(1)), nlocal, 1000, 0.1_pr, 3423, DEFINE_Y = 0.0_pr )
     CALL add_smooth_noise( u(:,n_mom(2)), nlocal, 1000, 0.1_pr, 234463, DEFINE_Y = 0.0_pr )
     IF(dim .GE.3) CALL add_smooth_noise( u(:,n_mom(3)), nlocal, 1000, 0.1_pr, 233, DEFINE_Y = 0.0_pr )

     ! Shear layer profile
     u(:,n_mom(1)) = u(:,n_mom(1)) + TANH( x(:,2) / ( 2.0_pr * momentum_thickness ) )

     ! Velocity to momentum
     DO idim = 1,dim
        u(:,n_mom(idim)) = u(:,n_den)*u(:,n_mom(idim))
     END DO 


     !Set Energy for constant temperature
     u(:,n_eng)              = 0.5_pr*SUM(u(:,n_mom(1:dim))**2.0_pr, DIM=2)/u(:,n_den) + u(:,n_den)*cv_in(Nspec) * F_compress * 1.0_pr 
     !Set species

     DO i = 1,dim + 2
        tmp1 = MINVAL(u(:,i))
        tmp2 = MAXVAL(u(:,i))
        CALL parallel_global_sum( REALMINVAL=tmp1 )
        CALL parallel_global_sum( REALMAXVAL=tmp2 )
        IF(par_rank .EQ. 0) PRINT *, 'Initialize var', i, tmp1,tmp2
     END DO
             

     nwlt_max = nwlt
     CALL parallel_global_sum(INTEGERMAXVAL=nwlt_max)
     IF(par_rank .EQ. 0 ) PRINT *, 'nwlt_max:', nwlt_max


  END IF

  !variable thresholding
  !IF ( do_Adp_Eps_Spatial .OR. do_Adp_Eps_Spatial_Evol ) &
  !     CALL user_initial_conditions__VT(u, nlocal, ne_local)!, t_local, scl, scl_fltwt, iter)
  
  ! Initialize Freund zone
  IF ( use_freund_damping .OR. use_freund_convect ) CALL setup_freund()

  IF ( use_freund_damping ) THEN
    freund_damping_target(1:2,1:dim,:) = 0.0_pr 
    freund_damping_target(1:2,1:dim,n_den) = 1.0_pr 
    IF(Nspec .GT. 1) THEN
      freund_damping_target(1,1:dim,n_den)    = MW_in(Nspec)/MW_in(1)!0.0_pr ! Min
      freund_damping_target(2,1:dim,n_den)    = 1.0_pr ! Max
      freund_damping_target(2,1:dim,n_spc(:)) = 0.0_pr 
      freund_damping_target(2,1:dim,n_spc(1)) = 1.0_pr 
    END IF
    freund_damping_target(1:2,1:dim,n_mom(:)) = 0.0_pr 
    freund_damping_target(1,1:dim,n_mom(1)) = -freund_damping_target(1,1:dim,n_den)
    freund_damping_target(2,1:dim,n_mom(1)) = freund_damping_target(2,1:dim,n_den)
    freund_damping_target(1:2,1:dim,n_eng)    = 0.5_pr*SUM(freund_damping_target(1:2,1:dim,n_mom(1:dim))**2.0_pr, DIM=3)/freund_damping_target(1:2,1:dim,n_den) &
                                                + freund_damping_target(1:2,1:dim,n_den)*cv_in(Nspec) * F_compress * 1.0_pr 
    freund_damping_active = .TRUE.  

  END IF
  IF ( use_freund_convect ) THEN 
    freund_convect_active = .TRUE.  
  END IF

  nwlt_total = nwlt
  CALL parallel_global_sum(INTEGER=nwlt_total) 
  IF (par_rank .EQ. 0) THEN
        PRINT *, '********************************************'
        PRINT *, '* j_lev = ',j_lev,        '  *' 
        PRINT *, '* nwlt  = ', nwlt_total,  '  *' 
        PRINT *, '********************************************'
  END IF

 
END SUBROUTINE user_initial_conditions

  !--********************************  
  ! Arguments
  ! u         - field on adaptive grid
  ! nlocal      - number of active points
  ! ne_local       - number of equations
  ! t         - current time
  !
  ! Global variables used
  ! ifn       - number of equations
  ! x()
  ! xO
  ! y0
  !--******************************** 
  SUBROUTINE user_algebraic_BC (Lu, u, nlocal, ne_local, jlev, meth)
    !--Defines boundary condition type
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: jlev, meth, ne_local, nlocal
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (INOUT) :: Lu
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (IN)    :: u


  END SUBROUTINE user_algebraic_BC

  SUBROUTINE user_algebraic_BC_diag (Lu_diag, nlocal, ne_local, jlev, meth)
    !--Defines boundary condition type
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: jlev, meth, ne_local, nlocal
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (INOUT) :: Lu_diag

  END SUBROUTINE user_algebraic_BC_diag

  SUBROUTINE user_algebraic_BC_rhs (rhs, ne_local, nlocal, jlev)
    !--Sets rhs for boundary conditions
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: ne_local, nlocal, jlev
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (INOUT) :: rhs

  END SUBROUTINE user_algebraic_BC_rhs

  FUNCTION user_chi (nlocal, t_local)
    USE parallel
    USE threedobject
    IMPLICIT NONE
    INTEGER,          INTENT (IN) :: nlocal
    REAL (pr),        INTENT (IN) :: t_local
    REAL (pr), DIMENSION (nlocal) :: user_chi

    user_chi = 0.0_pr

  END FUNCTION user_chi

  FUNCTION user_mapping ( xlocal, nlocal, t_local )
    USE curvilinear
    USE curvilinear_mesh
    USE error_handling 
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: nlocal
    REAL (pr), INTENT (IN) :: t_local
    REAL (pr), DIMENSION (nlocal,dim), INTENT(IN) :: xlocal
    REAL (pr), DIMENSION (nlocal,dim) :: user_mapping

    user_mapping(:,1:dim) = xlocal(:,1:dim)

  END FUNCTION user_mapping

  FUNCTION user_rhs (u_integrated,scalar)
    USE sgs_compressible 
    IMPLICIT NONE
    REAL (pr), DIMENSION (ng,ne), INTENT(IN) :: u_integrated !1D flat version of integrated variables without BC 
    REAL (pr), DIMENSION (ng), INTENT(IN) :: scalar
    REAL (pr), DIMENSION (ng*ne) :: user_rhs

    !REAL (pr), DIMENSION (ne,ng,dim) :: du, d2u
    INTEGER, PARAMETER :: meth=1

    CALL compressible_rhs ( user_rhs, u_integrated, meth) 
    IF( sgsmodel_compress .NE. sgsmodel_compress_none ) CALL sgs_compress_rhs ( user_rhs, u_integrated, meth) 
    IF ( use_freund_damping ) CALL freund_damping_rhs( user_rhs, u_integrated ) 
    IF ( use_freund_convect ) CALL freund_convect_rhs( user_rhs, u_integrated, j_lev ) 


    IF(hypermodel .NE. 0 .AND. ( hyper_it .LT. hyper_it_limit .OR. hyper_it_limit < 0) ) THEN
      !hyper_mask = set_hyper_mask(ng) 
       CALL hyperbolic(u_integrated,ng,user_rhs)
       IF( par_rank .EQ. 0 ) PRINT *, 'Using Hyperbolic Module'
    END IF

  END FUNCTION user_rhs

  FUNCTION user_Drhs (u_p, u_prev, meth)
    USE sgs_compressible 
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: meth
    REAL (pr), DIMENSION (ng,ne) :: u_p, u_prev
    REAL (pr), DIMENSION (ng*ne) :: user_Drhs
    !REAL (pr), DIMENSION (2*ne,ng,dim) :: du
    !REAL (pr), DIMENSION (ne,ng,dim)   :: d2u

    CALL compressible_Drhs ( user_Drhs, u_p, u_prev, meth ) 
    IF( sgsmodel_compress .NE. sgsmodel_compress_none ) CALL sgs_compress_Drhs ( user_Drhs, u_p, u_prev, meth ) 
    IF ( use_freund_damping ) CALL freund_damping_drhs( user_Drhs, u_p, u_prev )  
    IF ( use_freund_convect ) CALL freund_convect_drhs( user_Drhs, u_p, u_prev, j_lev ) 


    IF (hypermodel .NE. 0 .AND. ( hyper_it .LT. hyper_it_limit .OR. hyper_it_limit < 0)) THEN
      !hyper_mask = set_hyper_mask(ng) 
      CALL hyperbolic(u_p,ng,user_Drhs)
    END IF

  END FUNCTION user_Drhs

  FUNCTION user_Drhs_diag (meth)
    USE sgs_compressible 
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: meth
    REAL (pr), DIMENSION (ng*ne) :: user_Drhs_diag
    REAL (pr), DIMENSION (ng,ne) :: u_prev
    !REAL (pr), DIMENSION (ne,ng,dim) :: du_dummy
    !REAL (pr), DIMENSION (ng,dim)    :: du_diag, d2u_diag 
    INTEGER :: ie, shift
    !REAL (pr), DIMENSION(ng) :: hyper_mask

    DO ie = 1, ne
       shift=(ie-1)*ng
       u_prev(1:ng,ie) = u_prev_timestep(shift+1:shift+ng)
    END DO

    CALL compressible_Drhs_diag(user_Drhs_diag,u_prev,meth )
    IF( sgsmodel_compress .NE. sgsmodel_compress_none ) CALL sgs_compress_Drhs_diag(user_Drhs_diag,u_prev,meth )
    IF ( use_freund_damping ) CALL freund_damping_drhs_diag( user_Drhs_diag, u_prev )  
    IF ( use_freund_convect ) CALL freund_convect_drhs_diag( user_Drhs_diag, u_prev, j_lev ) 

    IF(hypermodel .NE. 0 .AND. ( hyper_it .LT. hyper_it_limit .OR. hyper_it_limit < 0)) THEN
      !hyper_mask = set_hyper_mask(ng) 
      CALL hyperbolic_diag(user_Drhs_diag, ng)
    END IF


  END FUNCTION user_Drhs_diag

  SUBROUTINE user_project (u, p, nlocal, meth)
    USE variable_mapping
    IMPLICIT NONE

    INTEGER, INTENT (IN) :: meth, nlocal
    REAL (pr), DIMENSION (nlocal),     INTENT(INOUT) :: p
    REAL (pr), DIMENSION (nlocal,n_integrated), INTENT(INOUT) :: u

    !no projection for compressible flow
  END SUBROUTINE user_project

  !
  ! Calculate any statitics
  !
  ! startup_flag - 0 when adapting to IC,then 1 inmain integration loop
  !
  SUBROUTINE user_stats ( u, j_mn_local, startup_flag)
    USE parallel
    USE penalization
    USE variable_mapping
    IMPLICIT NONE
    REAL (pr), DIMENSION (nwlt,1:n_var), INTENT (IN) :: u
    INTEGER , INTENT (IN) :: j_mn_local 
    INTEGER , INTENT (IN) :: startup_flag

  END SUBROUTINE user_stats

  SUBROUTINE user_cal_force (u, n, t_local, force, drag, lift)
    !--Calculates drag and lift on obstacle using penalization formula
    IMPLICIT NONE

    INTEGER, INTENT (IN) :: n
    REAL (pr), INTENT (IN) :: t_local
    REAL (pr), DIMENSION (dim), INTENT (INOUT) :: force
    REAL (pr), INTENT (OUT) :: drag, lift
    REAL (pr), DIMENSION (n,dim) :: u
    drag = 0.0_PR
    lift = 0.0_PR
    !
    ! There is no obstacle in flow
    !

  END SUBROUTINE user_cal_force

  !
  ! Read input from "case_name"_pde.inp file
  ! case_name is in string file_name read from command line
  ! in read_command_line_input()
  ! 
  SUBROUTINE user_read_input()
    USE sgs_compressible 
    IMPLICIT NONE
    INTEGER :: i, j, ind
    CHARACTER(LEN=8):: numstrng, procnum
    REAL (pr), ALLOCATABLE, DIMENSION(:) :: YR, gammR  ! temporary calculation quantities 

    CALL compressible_read_input()
    CALL sgs_compress_read_input()
    CALL freund_read_input()  

    call input_real ('Re',Re,'stop',' Re: Reynolds Number')
    call input_real ('Pra',Pra,'stop',' Pra: Prandtl Number')
    call input_real ('Sc',Sc,'stop',' Sc: Schmidt Number')
    call input_real ('Fr',Fr,'stop',' Fr: Froude Number')
    call input_real ('Ma',Ma,'stop',' Ma: Mach Number')

    call input_real ('Re_tau_target',Re_tau_target,'stop',' Re_tau_target: Target Wall Reynolds Number')

    call input_integer ('hyper_it_limit',hyper_it_limit,'stop','  hyper_it_limit: number of iterations to apply the hyperbolic viscosity to smooth local instabilities.  hyper_it_limit < 0 for all iterations')
 
    filter_field = .FALSE.
    call input_logical ('filter_field',filter_field,'default', 'filter_field: pre_process stabilization of field by applying lowpass filter once')


    call input_real ('Lczn',Lczn,'stop', 'Lcoordzn: length of coord_zone on one side')
    call input_real ('tfacczn',tfacczn,'stop', 'tfacczn: factor multiply by time for coord_zone time=L/min(c)*tfac, set <0 for no czn modification')
    call input_real ('itfacczn',itfacczn,'stop', 'itfacczn: factor multiply by initial time for coord_zone itime=Lczn/min(c)*tfac, set <0 to set itime=0')



    call input_real ('grav_coeff',grav_coeff,'stop', 'grav_coeff: coefficient for gravity forcing')
    call input_logical ('dynamic_gravity',dynamic_gravity,'stop', 'dynamic_gravity: use dynamic volume forcing to maintain some bulk flow quantity' )

    !4extra - these 8 input definitions - MUST ALSO ADD TO .inp 
    call input_logical ('saveUtil',saveUtil,'stop', 'saveUtil: saveUtil: save utility variable') 
   
     
    ! channel clow mesh stretchin
    call input_logical ('mesh_clustering', mesh_clustering, 'stop',' mesh_clustering: Use mesh clustering near centerline')
    call input_real ('mesh_cluster_param', mesh_cluster_param, 'stop',' mesh_cluster_param: parameter to control stretching near centerline')
 
    IF (par_rank.EQ.0) THEN
       PRINT *, 'Using Re=', Re     
    END IF

    CALL set_F_compress( 1.0_pr/Ma**2 / gamm(1) )
    ! Set properties
    DO i = 1, Nspec
       IF (GRAV) CALL set_body_force(-grav_coeff, i, 1 )  !sign change for forcing in the positive x-direction
       ! All molecular weights set equal
       !CALL set_thermo_props( 1.0_pr, 1.0_pr/MW_in(i) * gamm_loc(i)/(gamm_loc(i)-1.0_pr), gamm_loc(i), i )  ! To consistantly initialize derived thermo properties 
       CALL set_thermo_props( 1.0_pr/MW_in(i) * gamm(i)/(gamm(i)-1.0_pr), 1.0_pr/MW_in(i), i )  ! To consistantly initialize derived thermo properties 
    END DO

    ! Intermediate quantities for calculating diffusivity
    IF( ALLOCATED( YR ) ) DEALLOCATE( YR )
    IF( ALLOCATED( gammR ) ) DEALLOCATE( gammR )
    ALLOCATE( YR(1:Nspec), gammR(1:Nspec) )
    YR(:) = MW_in(:) / SUM(MW_in(1:Nspec))                                   
    IF( Nspec .GT. 1 ) YR(Nspec) = 1.0_pr - SUM(YR(1:Nspecm)) ! higher accuracy
    gammR = SUM(cp_in(:)*YR(:))/( SUM(cp_in(:)*YR(:))-SUM(YR(:)/MW_in(:)) )   
    DO i = 1, Nspec
       CALL set_dynamic_viscosity( 1.0_pr/Re, i )
       CALL set_conductivity( 1.0_pr/Re/Pra*gammR(i)/(gammR(i)-1.0_pr), i )   
       CALL set_diffusivity( 1.0_pr/Re/Sc, i )
    END DO
    DEALLOCATE( YR )
    DEALLOCATE( gammR )



    grav_coeff_hist = grav_coeff
    Re_bulk_hist = Re
    Re_tau_hist = Re_tau_target 
    
     IF (NOT(ALLOCATED(Lxyz))) ALLOCATE(Lxyz(1:dim))

     Lxyz(1:dim)=xyzlimits(2,1:dim)-xyzlimits(1,1:dim)

  END SUBROUTINE user_read_input

  !
  ! calculate any additional variables
  ! 
  ! arg 
  ! flag  - 0 calledwhile adapting to IC, 1 - called in main time integration loop
  !
  ! These additiona variables are calculated and left in real space.
  !
  SUBROUTINE user_additional_vars( t_local, flag )
    USE penalization
    USE sgs_compressible 
    IMPLICIT NONE
    REAL (pr), INTENT (IN) ::  t_local
    INTEGER , INTENT(IN) :: flag ! 0- called during adaption to IC, 1 called during main integration loop
    INTEGER :: l, i, j
 
    IF (saveUtil) u(:,n_util) = 0.0_pr  ! dummy field 
    IF (imask_obstacle) u(:,nmsk) = penal

    CALL compressible_additional_vars( t_local, flag )
    IF( sgsmodel_compress .NE. sgsmodel_compress_none ) CALL sgs_compress_additional_vars(t_local,flag)  


       IF (modczn) THEN
          IF (t>tczn) THEN
             modczn=.FALSE.
             xyzzone(1,1) = czn_min
             xyzzone(2,1) = czn_max            
            IF (par_rank.EQ.0 .AND. verb_level>0) THEN
             PRINT *, '*****************************************************'
             PRINT *, 'FINALIZING TEMPORARY COORD_ZONE'
             PRINT *, 'COORD_ZONE OUTSIDE OF x=[',xyzzone(1,1),',',xyzzone(2,1),']'
             PRINT *, 'TIME: ', t
             PRINT *, 'STOPPING TIME FOR COORD_ZONE: ', tczn
             PRINT *, '*****************************************************'
            END IF
          END IF
       ELSE  
          IF (tfacczn>0.0_pr .AND. itfacczn>0.0_pr .AND. t>itczn) THEN
             itczn=t_end*10.0_pr
             czn_min = xyzzone(1,1)
             czn_max = xyzzone(2,1)
             xyzzone(1,1) = -Lczn
             xyzzone(2,1) =  Lczn
             cczn = MINVAL(SQRT(gamm/MW_in/Ma**2))
             tczn = MAXVAL(ABS(xyzlimits(:,1)))/cczn*tfacczn
            IF (par_rank.EQ.0 .AND. verb_level>0) THEN
             PRINT *, '*****************************************************'
             PRINT *, 'APPLYING TEMPORARY COORD_ZONE'
             PRINT *, 'COORD_ZONE OUTSIDE OF x=[',-Lczn,',',Lczn,']'
             PRINT *, 'MINIMUM WAVE SPEED: ', cczn
             PRINT *, 'MAXIMUM DISTANCE: ', MAXVAL(ABS(xyzlimits(:,1)))
             PRINT *, 'TIME: ', t
             PRINT *, 'STARTING TIME FOR COORD_ZONE: ', itczn
             PRINT *, 'TIME SAFETY FACTOR: ', tfacczn
             PRINT *, 'STOPPING TIME FOR COORD_ZONE: ', tczn
             PRINT *, '*****************************************************'
            END IF
             IF (t<tczn) THEN
                modczn = .TRUE.
             ELSE
                IF (par_rank.EQ.0) PRINT *, '!!!TOO LATE TO START COORD_ZONE: t>tczn!!!'
             END IF
          END IF  
       END IF


  END SUBROUTINE user_additional_vars

  !
  ! calculate any additional scalar variables
  ! 
  ! arg 
  ! flag  - 0 calledwhile adapting to IC, 1 - called in main time integration loop
  !
  SUBROUTINE user_scalar_vars( flag )
    IMPLICIT NONE
    INTEGER , INTENT(IN) :: flag ! 0- called during adaption to IC, 1 called during main integration loop

  END SUBROUTINE user_scalar_vars

  !
  !************ Calculating Scales ***************************
  !
  ! Note the order of the components in the scl array
  ! correspond to u_tn, v_tn, w_tn, u_tn-1, v_tn-1, w_tn-1, u_tn-2, v_tn-2, w_tn-2
  ! 
  SUBROUTINE user_scales(flag,use_default, u_loc, nlocal, ne_local, l_n_var_adapt , l_n_var_adapt_index, &
       scl, scl_fltwt) !add
    USE curvilinear_mesh
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: flag ! 0 during initial adaptation, then 1 during time advancement
    LOGICAL , INTENT(INOUT) :: use_default
    INTEGER, INTENT (IN) :: nlocal, ne_local
    REAL (pr), DIMENSION (1:nlocal,1:ne_local), INTENT (IN) :: u_loc
    LOGICAL , INTENT (IN) :: l_n_var_adapt(ne_local)
    INTEGER , INTENT (IN) :: l_n_var_adapt_index(1:ne_local)
    REAL (pr), DIMENSION (1:ne_local), INTENT (INOUT) :: scl
    REAL (pr) ,  INTENT(IN) ::scl_fltwt !weight for temporal filter on scl
    LOGICAL  , SAVE :: startup_init = .TRUE.

    !
    ! Ignore the output of this routine and use default scales routine
    !
    use_default = .FALSE. 

    IF( .NOT. use_default ) THEN
       CALL compressible_scales(flag,use_default, u_loc, nlocal, ne_local, l_n_var_adapt , l_n_var_adapt_index, scl, scl_fltwt) 
       CALL curv_mesh_scales(flag,use_default, u_loc, nlocal, ne_local, l_n_var_adapt , l_n_var_adapt_index, scl, scl_fltwt) 
       startup_init = .FALSE.
    END IF

  END SUBROUTINE user_scales

  SUBROUTINE user_cal_cfl (use_default, ulocal, cfl_out)
    USE precision
    USE sizes
    USE pde
    USE variable_mapping
    IMPLICIT NONE
    LOGICAL , INTENT(INOUT) :: use_default
    REAL (pr),                                INTENT (INOUT) :: cfl_out
    REAL (pr), DIMENSION (nwlt,n_integrated), INTENT (IN)    :: ulocal

    INTEGER                    :: i, idim
    REAL (pr), DIMENSION(nwlt,dim) :: cfl
    REAL (pr), DIMENSION(nwlt,dim) :: v 
    REAL (pr)                  :: min_h
    REAL (pr) :: min_w, max_w
    REAL (pr) :: inv_vortex_timescale, cfl_vortex

    use_default = .FALSE.
    CALL compressible_cal_cfl (use_default, ulocal, cfl_out)
    IF ( ( use_freund_damping .OR. use_freund_convect) .AND. time_integration_method == TIME_INT_RK ) THEN
      cfl_out = MAX( cfl_out, freund_cal_cfl() )
    END IF 

    IF(dim .EQ. 3) THEN
       min_w = MINVAL(ulocal(:,n_mom(3))/ulocal(:,n_den))
       max_w = MAXVAL(ulocal(:,n_mom(3))/ulocal(:,n_den))
       CALL parallel_global_sum(REALMINVAL=min_w)
       CALL parallel_global_sum(REALMAXVAL=max_w)
    END IF

    IF (par_rank.EQ.0) THEN
       PRINT *, '**********************************************************'
       !PRINT *, 'BL pts = ', 1.0_pr/((Re)**0.5_pr*min_h)
       !PRINT *, 'Re_tau = ', Re_tau
       IF(dim .EQ.3)PRINT *, 'min/max w = ',min_w, max_w
       PRINT *, '**********************************************************'
       
    END IF
  END SUBROUTINE user_cal_cfl

  SUBROUTINE user_init_sgs_model( )
    IMPLICIT NONE
  END SUBROUTINE user_init_sgs_model

  SUBROUTINE  user_sgs_force (u_loc, nlocal)
    USE variable_mapping
    USE sgs_compressible 
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: nlocal
    REAL (pr), DIMENSION (nlocal,n_integrated), INTENT (INOUT) :: u_loc
    IF( sgsmodel_compress .NE. sgsmodel_compress_none ) CALL sgs_compress_force( u_loc(1:nwlt,1:n_integrated),nlocal)
  END SUBROUTINE  user_sgs_force

  FUNCTION user_sound_speed (u, neq, nwlt)
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: nwlt, neq
    REAL (pr), DIMENSION (nwlt,neq), INTENT(IN) :: u
    REAL (pr), DIMENSION (nwlt) :: user_sound_speed,gam

    user_sound_speed(:) = calc_speed_of_sound( u, nwlt, neq )

  END FUNCTION user_sound_speed

  SUBROUTINE  user_pre_process
    USE variable_mapping
    USE sgs_compressible 
    IMPLICIT NONE

    CALL compressible_pre_process() 


    !IF(par_rank ==0) PRINT *, 'user_pre_process'

  
 

  END SUBROUTINE user_pre_process

  SUBROUTINE  user_post_process
    USE variable_mapping
    IMPLICIT NONE

    CALL compressible_post_process() 
    !IF(compressible_SCALES .AND. it > 0) CALL SGS_compressible_post_process(u(:,1:n_integrated),n_integrated,nwlt)

    !variable thresholding
    !IF ( do_Adp_Eps_Spatial .OR. do_Adp_Eps_Spatial_Evol ) &
    !CALL user_post_process__VT(n_var_SGSD)    ! n_var_ExtraAdpt_Der    ! n_var_Epsilon   ! n_var_RSGSD         ! Cf: OPTIONAL
    IF(hypermodel .NE. 0 .AND. ( hyper_it .LT. hyper_it_limit ) ) hyper_it = hyper_it + 1 


  END SUBROUTINE user_post_process

  FUNCTION initial_velocity_spectra( nloc, k0, direction, RNDM_OFFSET_SEED ) 
    IMPLICIT NONE
    INTEGER,                      INTENT(IN) :: k0, direction, nloc
    INTEGER, OPTIONAL,            INTENT(IN) :: RNDM_OFFSET_SEED 
    REAL (pr), DIMENSION(nloc)               :: initial_velocity_spectra

    INTEGER :: k      ! Iterators
    INTEGER :: nyquist      ! nyquist wavenumber 

    REAL (pr)                           :: offset, spectra
    INTEGER                             :: iseedsize
    INTEGER, ALLOCATABLE, DIMENSION(:)  :: seed
    
    IF( direction < 1 .OR. direction > dim ) THEN
      PRINT *, 'Cannot compute spectra for invalid direction', direction, 'of', dim
      STOP
    END IF
    
    IF( PRESENT(RNDM_OFFSET_SEED)) THEN 
      CALL RANDOM_SEED(SIZE=iseedsize)  
      ALLOCATE(seed(iseedsize))
      seed(:) = RNDM_OFFSET_SEED
      CALL RANDOM_SEED(PUT=seed)
      DEALLOCATE(seed)
    END IF

    initial_velocity_spectra = 0.0_pr

    offset = 0.0_pr
    nyquist = (mxyz(direction)*2**(j_mx - 1) + 1 - prd(direction))/2
    DO k = 0, nyquist/4
      ! Energy spectra
      spectra = ( REAL(k,pr)/REAL(k0,pr) )**4 * EXP( -2.0_pr *  ( REAL(k,pr)/REAL(k0,pr) )**2 )
      IF( PRESENT(RNDM_OFFSET_SEED)) call RANDOM_NUMBER(offset)
      !IF(par_rank .EQ. 0)PRINT *, 'spectra with offset', spectra, offset
      initial_velocity_spectra = initial_velocity_spectra + &
        spectra * SIN( 2.0_pr * PI * REAL(k,pr) * ( x(:,direction) / (xyzlimits(2,direction) - xyzlimits(1,direction) )+ offset + ( 0.25_pr - 0.25_pr*REAL(MOD(k,2),pr) )   ) )
    END DO



  END FUNCTION initial_velocity_spectra


  SUBROUTINE calc_total_KE( momentum, density, total_KE, nloc )
    IMPLICIT NONE
    INTEGER,                        INTENT(IN)  :: nloc
    REAL (pr), DIMENSION(nloc,dim), INTENT(IN)  :: momentum
    REAL (pr), DIMENSION(nloc),     INTENT(IN)  :: density 
    REAL (pr),                      INTENT(OUT) :: total_KE 

    INTEGER   :: idim
    total_KE = SUM(  0.5_pr * momentum(:,1) * momentum(:,1) * dA(:) / density(:) )
    DO idim = 2,dim
      total_KE = total_KE + SUM(  0.5_pr * momentum(:,idim) * momentum(:,idim) * dA(:) / density(:) )
    END DO

    CALL parallel_global_sum( REAL=total_KE )

  END SUBROUTINE calc_total_KE

END MODULE user_case
