#------------------------------------------------------------------#
# General input file format                                        #
#                                                                  #
# comments start with # till the end of the line                   #
# string constant are quoted by "..." or '...'                     #
# boolean can be (T F 1 0 on off) in or without ' or " quotes      #
# integers are integers                                            #
# real numbers are whatever (e.g. 1   1.0   1e-23   1.123d-54 )    #
# vector elements are separated by commas                          #
# spaces between tokens are not important                          #
# empty or comment lines are not important                         #
# order of lines is not important                                  #
#------------------------------------------------------------------#

###############################################################################################################################################################
#  F i l e      I n f o r m a t i o n
#
file_gen = 'external_flow_R250_M0p5.'
#results_dir = './results/'
###############################################################################################################################################################

###############################################################################################################################################################
#  D o m a i n      I n f o r m a t i o n
#
dimension = 2		#  dim (2,3), # of dimensions
coord_min = 0.5, 0.0, -5.0	#  XMIN, YMIN, ZMIN, etc
#coord_max =  2.4555, 6.283185307179586, 5.0	#  XMAX, YMAX, ZMAX, etc
coord_max =  2.06, 6.283185307179586, 5.0	#  XMAX, YMAX, ZMAX, etc

j_mn_init = 3              	#  J_mn_init force J_mn == J_INIT while adapting to IC
j_lev_init = 3          	#  starts adapting IC by having all the points on this level of resolution
#j_IC = 7                #  J_ICdatafile if the IC data does not have dimensions in it then mxyz(:)*2^(j_ICdatafile-1) is used
J_MN = 2         	#  J_MN
J_MX = 5            	#  J_MX
J_FILT = 20            	#  J_FILT
J_TREE_ROOT = 2         #  J_TREE_ROOT:  root level for trees for parallel

BNDzone = F	  	#  BNDzone
#j_zn = 2                #  j_zone
#coord_zone_min = -5e+05,-5e+05,-5e+05		# XMINzone, etc
#coord_zone_max =  5e+05, 5e+05, 5e+05		# XMAXzone, etc

M_vector = 10,6,4	#  Mx, etc
#M_vector = 512,256,0	#  Mx, etc
#M_vector = 1024,256,0	#  Mx, etc
#M_vector = 1536,256,0	#  Mx, etc
periodic = 0,1,1	#  prd(:) (0/1) 0: non-periodic; 1: periodic
uniform = 0,0,0		#  grid(:) (0/1) 0: uniform; 1: non-uniform
transform_dir = 1,1,0
coordinate_adapt_scale = 1.0  # coordinate_adapt_scale: Scale for adapting on the coordinate map

i_h = 123456        	#  order of boundaries (1-xmin,2-xmax,3-ymin,4-ymax,5-zmin,6-zmax)
i_l = 001100        	#  algebraic/evolution (1/0) BC order: (lrbt)
###############################################################################################################################################################

###############################################################################################################################################################
#  A W C M      I n f o r m a t i o n
#
eps_init = 1.0000000e-2  	#  EPS used to adapt initial grid  
eps_run  = 1.0000000e-2  	#  EPS used in run  
eps_adapt_steps = 100             # eps_adapt_steps ! how many time steps to adapt from eps_init to eps_run

N_predict = 2           #  N_predict
N_predict_low_order = 1 #  N_predict_low_order
N_update = 0           	#  N_update
N_update_low_order = 0  #  N_update_low_order
N_diff = 2              #  N_diff

Scale_Meth = 1             	# Scale_Meth !1- Linf, 2-L2
scl_fltwt = 0.99           	#  scl temporal filter weight, scl_new = scl_fltwt*scl_old + ( 1-scl_fltwt)*scl_new

IJ_ADJ = 1,1,1		# IJ_ADJ(-1:1) = (coarser level), (same level), (finer level)
ADJ_type = 1,1,1	#  ADJ_type(-1:1) = (coarser level), (same level), (finer level) # (0 - less conservative, 1 - more conservative)
###############################################################################################################################################################

###############################################################################################################################################################
#---------- Compressible SCALES ---------
#
sgsmodel_compress = 2   # 0-off, 1-fixSM, 2-AMD, 3-LDKM

# Eddy viscosity
save_eddy_visc = T
save_SGSD = T
save_diss_ratio = T   # save_diss_ratio: save ratio of modeled dissipation to physical

# Residual-based eddy viscosity model (RBEVM)
Kolmog_const = 1.4        # Kolmog_const: Kolmogorev constant in the modeled coefficient computation
modeled_grid_ratio = 1.5  # modeled_grid_ratio : Ratio of resolution to resolution where u fluctuations are calculated

# K-eqn Eddy viscosity
#   Coefficient types: 0-off, 1-const, 2-Germano,clipped, 3-germano, 4-Bardina,clipped 5-Bardina
    Cs_type           = 2
    Pra_t_type        = 2
    q_coeff_type      = 2
    Sc_t_type         = 1
    C_f_type          = 1
    C_diss_sol_type   = 1
    C_diss_dilat_type = 1
    C_pi_type         = 0
#   Constant coeffs 
    Cs_in           = 0.09  # Also used for fixed smagorinsky
    Ciso_in         = 0.09
    C_AMD_in        = 0.212 # coefiecient used in original formulation for 4-th order method  
    Pra_t_in        = 1.0   # Also used for fixed smagorinsky
    Sc_t_in         = 1.0   # Also used for fixed smagorinsky
    C_f_in          = 0.09
    C_diss_sol_in   = 0.09
    C_diss_dilat_in = 0.09
    C_pi_in         = 0.0
#   Coeff limits 
    Cs_limit           = -1.0,1.0
    Ciso_limit         = 0.0, 1.0
    Pra_t_limit        = 0.0, 10.0
    Sc_t_limit         = 0.0, 1.0
    C_f_limit          = 0.0, 1.0
    C_diss_sol_limit   = 0.0, 1.0
    C_diss_dilat_limit = 0.0, 1.0
    C_pi_limit         = 0.0, 0.0 
adaptk = F                # adaptk: adapt on the compressible rho*ksgs variable
ksgs_compress_scale = 1.0 # ksgs_compress_scale: Scale coefficient for ksgs adaptation

filt_C = T      # use lowpass filtering for pseudo-volume averaging of coefficients
preclip_K = T   # preclip_K ! clip Ksgs to positive values in pre-processing')
save_dyn_coeffs = T   # save_dyn_coeffs: save any dynamic coefficients
save_delta = T        # save the filter width for post_processing

background_ksgs = 1.0e-6  # background_ksgs: Background value for initialization with krts

###############################################################################################################################################################

###############################################################################################################################################################
#  F i l t e r    T y p e s
#
                                #  filter types: 0 - none, 1 - >eps, 2 - >2eps,
                                #                3 - level <= j_lev-1, 4 - level <= j_lev-2
                                #                5 - 2eps + adjacent zone,
                                #                6 - local low-pass filter (lowpass_filt_type, lowpassfilt_support, tensorial_filt)
                                #                7 - u > eps_explicit filter
                                #                8 - u > eps_explicit filter + adjecent zone
                                #                9 - u > 2eps_explicit filter
                                #               10 - u > 2eps_explicit filter + adjecent zone
mdl_filt_type_grid = 0                  # dyn mdl grid filter, 0 - 6
mdl_filt_type_test = 6          # dyn mdl test filter, 0 - 6
lowpass_filt_type = 0        # 0 - volume averged, 1 - trapezoidal
lowpass_filt_type_GRID = 0   # 0 - volume averged, 1 - trapezoidal
lowpass_filt_type_TEST = 0   # 0 - volume averged, 1 - trapezoidal
lowpassfilt_support = 1 #  support for lowpass filter used for other prposes, such as in Lagrangian pathtube averging 
lowpassfilt_support_GRID = 0 #  support for lowpass filter if  mdl_filt_type_grid = 6 
lowpassfilt_support_TEST = 1 #  support for lowpass filter if  mdl_filt_type_test = 6 

tensorial_filt = F      # if .TRUE. uses tensorial filter for comparison with lines

Mcoeff =  2.0           # Mcoeff in GDM:  Mij = Mcoeff * |S>2eps| Sij>2eps - (|S|Sij )>2eps 
              # Mcoeff in LDKM: \hat{\Delta} = Mcoeff * \Delta
deltaMij = T            # deltaMij, Mij definition including delta^2 (GSM, LDM) or delta (LKM, LDKM)
ExplicitFilter = F      # ExplicitFilter ,apply grid filter as an explicit filter each time step
ExplicitFilterNL = F    # For SGS compressible, set to True in order to set up filters
###############################################################################################################################################################


###############################################################################################################################################################
# Case Specific LES Parameters
kappa_damp  = 0.41    # kappa_damp: Kappa parameter in Pantons (1997) damping function
Re_tau_damp  = 100.0   # Re_tau_damp: Parameter for normalizing y+ units in Pantons (1997) damping function
c_damp  = 6.80        # c_damp: C+ parameter in Pantons (1997) damping function
###############################################################################################################################################################

###############################################################################################################################################################
#  T i m e    I n t e g r a t i o n     I n f o r m a t i o n
#
time_integration_method = 3  # 0- meth2, 1 -krylov, 2 - Crank Nicolson 
RKtype = 3

t_begin = 0.0000e+00   	#  tbeg  
t_end = 2.50000e+02	#  tend  
#dt = 1.666666e-04      #  dt
dt = 2.00000e-03      #  dt
dtmax = 5.0000000e-02 	#  dtmax
dtmin = 1.0000000e-8 	#  dtmin-if dt < dtmin then exection stops(likely blowing up)
dtwrite = 1.0000000e-02	#  dtwrite 1.0000000e-02 
t_adapt = 1.0000000e+10 #0.0    # when t > t_adapt use an adaptive time step if possible

cflmax = 5.0000000e-01 	#  cflmax
cflmin = 1.0000000e-08 	#  cflmin , Exit if cfl < cflmin
###############################################################################################################################################################

###############################################################################################################################################################
#  R e s t a r t     F i l e      I n f o r m a t i o n
#
do_Sequential_run   = F         # do_Sequential_run
IC_restart_mode     = 0         # 0-new run; 1-hard restart; 2-soft restart; 3-restart from IC --------
                                             # Hard restart - restart a previous run (without changing any parameters)
                                                             # Soft restart - epsilons, names, j_mx, j_tree_root, etc, could be changed
                                                                             # Restart from IC - some parameters can be changed and user initial conditions can be imposed
IC_restart_station  = 0032      # it_start, restart file number to use (NOT iteration!)
IC_file_fmt         = 0         # IC data file format  (0 - native restart file, 1-netcdf, 2- A.Wray in fourier space, 3-simple binary) next line is IC filename
IC_filename = '/lustre/work1/erbr9570/channel_DNS/channel_DNS_R3000_Rt200_M1p5_OS/channel_DNS_R3000_Rt200_M1p5_OS.0032.com.res'
#IC_single_loop       = T
#IC_repartition      = T
#IC_restart          = F         # ICrestart  T or F, restart a previous run. (multually exclusive with IC_from_file)
#IC_from_file        = F         # Do a new run with IC from a restart file
#IC_adapt_grid       = T         # parameter defaulted to .TRUE. If is set to .FALSE. no grid adaptation is done after the data are read.
#Data_file_format    = F         # T = formatted, F = unformatted
###############################################################################################################################################################

###############################################################################################################################################################
#  P a r a l l e l     D o m a i n      D e c o m p o s i t i o n
#
domain_debug  =F
domain_split  = 1, 1, 1      # Parallel domain decomposition: 1 allows subdivision in the direction (x,y,z,etc) (nonzero - allow, 0 - do not split that direction)

domain_meth  = 0      # domain meth decomposition is based on . . .
                      #       0 (default) geometric simultaneous - based on prime number 
                      #       1 geometric sequential (recursive pd N) based on N^(1/D) subdivision
                      #       2 Zoltan library, Geometric
                      #       3 Zoltan library, Hypergraph
                      #       4 Zoltan library, Hilbert Space-Filling Curve
                      #       10 tree number
                      #       11 tree number as if the boundaries are excluded
                      #       -1 read domain decomposition from the restart file
                      #           (during restart or in postprocessing only)
#IC_processors = 2
domain_imbalance_tol = 0.1,0.65,0.75
###############################################################################################################################################################

###############################################################################################################################################################
#  D e b u g     F l a g s
#
debug_force_wrk_wlt_order = F      # this ensures that tree and wrk are same, only set T for debug purposes 
debug_level               = 0      #  debug
verb_level                = 0      # 0- nothing is printed except for errors, warnings, and .inp listing     1- output from I/O related subroutines is printed     DefaultValue=1
debug_c_diff_fast         = 0      # 0- nothing,1- derivative MIN/MAXVALS,2- low level, are printed in c_diff_fast
diagnostics_elliptic      = F      #  diagnostics_elliptic: If T print full diagnostic for elliptic solver
GMRESflag                 = F      #  GMRESflag
BiCGSTABflag              = F      #  BiCGSTABflag
wlog                      = F      #  wlog: elliptic verb
###############################################################################################################################################################

###############################################################################################################################################################
#  S o l v e r    S e t t i n g s
#
Zero_Mean = F           #  T- enforce zero mean for 1:dim first variables (velocity usually), F- do nothing

Jacoby_correction = F           # Jacoby_correction
multigrid_correction = F        # multigrid_correction
GMRES_correction  = F           # GMRES_correction 

kry_p = 3			#  kry_p
kry_p_coarse = 100           	#  kry_p_coarse
len_bicgstab = 6             	#  len_bicgstab
len_bicgstab_coarse = 100	#  len_bicgstab_coarse
len_iter = 5            	#  len_iter

W0 = 1.0000000e+00 	#  W0 underrelaxation factor for inner V-cycle
W1 = 1.0000000e+00 	#  W1 underrelaxation factor on the finest level
W2 = 0.6000000e+00 	#  W2 underrelaxation factor for weighted Jacoby (inner points)
W3 = 1.0000000e-00 	#  W3 underrelaxation factor for weighted Jacoby (boundary points)
W_min = 1.00000e-02	#  W_min correcton factor 
W_max = 1.00000e-00 	#  W_max correction factor 

tol1 = 1.00e-06 	#  tol1 used to set tolerence for non-solenoidal half-step
tol2 = 1.00e-06 	#  tol2 used to set tolerence for solenoidal half-step
tol3 = 1.00e-03         #  used to set tolerence for time step
tol_gmres = 1.0e-08     #  used to set tolerence for gmres iterative solver
tol_gmres_stop_if_larger = F  # If true stop iterating in gmres solver if error of last iteration was smaller
###############################################################################################################################################################

###############################################################################################################################################################
#  M y     A d d i t i o n a l     P a r a m e t e r s
#

########## CASE SETUP ##########
grav_coeff  = 0.0044     #0.0044      # grav_coeff: coefficient for gravity forcing
dynamic_gravity  = F      # Use dynamic volume forcing to hold some bulk flow quantity
Nspec = 1                 # Nsp: number of species
mydt_orig = -1.000000e-03 # mydt_orig: max dt for time integration... dt is only initial dt, set <0 to use dt
NS  = T                   # NS: T to include full Navier-Stokes viscous terms, F for Euler
NSdiag  = T                   # NS: T to include full Navier-Stokes viscous terms, F for Euler
GRAV = F                  # GRAV: T to include body force terms, F to ingore them
viscmodel = 1             # viscmodel: 0 - const, 1-sutherland
convcfl = F               # convcfl: T to use convective cfl, F to use acoustic cfl
visccfl = 1.0               # convcfl: T to use convective cfl, F to use acoustic cfl
imexcfl = -1.0               # convcfl: T to use convective cfl, F to use acoustic cfl
bonusCFL= F               # convcfl: T to use convective cfl, F to use acoustic cfl
ComboIMEXRK = F
boundY = F                # boundY: T to bound Y to [0,1] in user_pre/post_process
################################


########## CASE SETUP ##########
dervFlux = 0 
kinevisc = 0 

########## NONDIMENSIONALS ##########
Re  = 2.5000000e+02                #  Re: Reynolds Number
Rep  = -3.0000000e+03              #  Re: Reynolds Number
Re_tau_target  = 2.0000000e+02     #  Re: Reynolds Number
Pra = 0.7200000e+00                #  Pra: Prandtl Number
Sc  = 1.0000000e+00                #  Sc: Schmidt Number
Fr  = 1.0000000e+00                #  Fr: Froude Number
Gr  = -1.0000000e+00               #  Fr: Froude Number
Ma  = 5.0000000e-01                #  Ma: Mach Number
At  = 0.0000000e-00                #  At: Atwood Number
sutherland_const  = 0.3765990e-00  #  sutherland_const: Sutherland constant S_1/T_0 for variable viscosity

gamm = 1.400e+00, 1.400e+00        #  gamm: specific heat ratio for each pure fluid
MW = 1.0e+00, 1.0e+00
adaptAt = F                        # adaptAt: T to modify At to get effective At = input At due to peakchange
peakchange = 1.0000000e-02         # peakchange: offset from 0 and 1 for double delta PDF of f
#####################################

########## CASE SPECIFIC CONFIGS ##########    
mesh_clustering = T               # Turn clustering optimizations on or off (F - use const aspect ration cylindrical formulation)
BL_cluster_param = 0.5            # clustering parameter for boundary layer hyperbolic tan stretched mesh ( 0 = no stretching)
azimuth_cluster_param = 1.5       # clustering parameter for hyperbolic tan stretched mesh in the azimuthal direction
#####################################

########## FREUND BOUNDARY CONDITIONS ##########    
use_freund_damping = T    # use_freund_damping: use Freund zone damping
  coeff_damping = 0.25     # coeff_damping: damping coefficient for Freund BC
use_freund_convect = T    # use_freund_convect: use Freund zone convection
  coeff_convect = 0.165     # coeff_damping: damping coefficient for Freund BC 
zone_thickness = 0.0, 0.09, 0.0, 0.0, 0.0, 0.0
#########################################

########## COORDZONE ##########
   Lczn = 5.000000e-02      # Lcoordzn: length of coord_zone on one side
   tfacczn = -1.100000e-00  # tfacczn: factor multiply by time for coord_zone time=L/min(c)*tfac, set <0 for no czn modification
   itfacczn = 1.100000e-00  # itfacczn: factor multiply by initial time for coord_zone itime=Lczn/min(c)*tfac, set <0 to set itime=0
###############################

########### SOLVER SETTINGS ############
   methpd = 0                       # methpd: meth for taking pressure derivative in time, 0=central,1=back,2=forward
   splitFBpress = F                 # splitFBpress: to split derivs for pressure dp/dx FB/BB light/heavy
   stepsplit = F                    # stepsplit: T to do Heaviside split, F to use X
########################################

########## EXTRA VARIABLES ##########
    adaptden = 0  # adapt on density
    denScale = 1.0
    adaptmom = 2  # adapt on momentum
    momScale = 1.0
    vel_vect_scale = 2  # vel_vect_scale: Scale for the velocity vector, defaults to global method (1-Linf,2-L2,3-L2)
    adapteng = 2  # adapt on energy
    engScale = 1.0
    adaptspc = 0  # adapt on species equations
    spcScale = 1.0
    adaptprs = 0  # adapt on pressure
    prsScale = 0.1

    adapttmp = 0  # adapttmponly: T to adapt on temperature
       tempScale = 1.0  # tempscl: scale for temperature, set < 0 for default, set =0 to use velocity scale
    savetmp =T
    adaptvelonly = 0  # adaptvelonly: T to adapt on velocity instead of momentum
    adaptengonly = 0  # adaptvelonly: T to adapt on velocity instead of momentum
    adaptspconly = 0  # adaptspconly: T to adapt on mass fraction (Y) instead of volume fraction (rhoY)
       specScale = 1.0  # specscl: scale for species, set < 0 for default

    savebuoys  = F  # savebuoys: if T adds and saves dPdx and -(dPdx + rho*g)
     adaptbuoy = 0  # adaptbuoy: T to adapt on buoy term
    buoyScale = 1.0
    savepardom = F  # savepardom: T to save parallel domain as additional var

    #!4extra - these 8 input definitions
    adaptMagVort = 2  # adaptMagVort: Adapt on the magnitude of vorticity
      magVortScale = 1.0
    adaptComVort = 0  # adaptComVort: Adapt on the components of vorticity
      comVortScale = 1.0
    adaptMagVel  = 0  # adaptMagVel: Adapt on the magnitude of velocity
      magVelScale  = 1.0
    adaptNormS   = 2  # adaptNormS: Adapt on the L2 of Sij
      normSScale   = 1.0
    adaptGradY   = 0  # adaptGradY: Adapt on |dY/dx_i*dY/dx_i|
      gradYScale   = 1.0
    adaptDivU    = 0  # adaptDivU: T to adapt on div(u) term: 0 - off, 1 - const scale, 2 - dynamic scale
      divUScale  = 1.0
    saveMagVort  = T  # saveMagVort: if T adds and saves magnitude of vorticity
    saveComVort  = F  # saveComVort: if T adds and saves components of vorticity
    saveMagVel   = F  # saveMagVel: if T adds and saves magnitude of velocity
    saveNormS    = T  # saveNormS: if T adds and saves magnitude of strain rate
    saveGradY    = F  # saveGradY: if T adds and saves |dY/dx_i*dY/dx_i|
    saveQCrit    = F  # saveQCrit: if T adds and saves Q-criterion
    saveDivU     = F  # saveDivU: if T adds and saves Divergence of Velocity
    saveUtil     = F  # saveUcvt: save suppport for U and damping for Freund NRBCs
    
    saveReyAvgInt = T          #saveReyAvgInt: if T adds and saves Reynolds Averaged Integrated vars
      avg_timestart = 25.0     #   avg_timescale: moving average time window start 
      restart_ReyAvg = T       #   restart_ReyAvg: restart Reynolds averaging
      fluct_scales_vel = F     #   fluct_scales: Use fluctuating scales for momenta/velocity adaptation

#####################################

############## Additional Planes #################
    additional_planes_active    = F	# additional_planes_active: if T adds nodes for additional user specified planes and activates the module
    j_additional_planes         = 5	# j_additional_planes - level at which points on the planes are added
    n_additional_planes         = 1	# n_additional_planes - number of additional planes to be added
    x_planes                    = 0.5, 0.0 	# x-coordinates for additional planes
    y_planes                    = 0.0,-1.0 	# y-coordinates for additional planes
    z_planes                    = 0.0, 0.0 	# z-coordinates for additional planes
    dir_planes                  =   1,   2	# directions of additional planes
#################################################

############## Additional Lines #################
    additional_lines_active    = T	# additional_planes_active: if T adds nodes for additional user specified planes and activates the module
    j_additional_lines         = 4	# j_additional_planes - level at which points on the planes are added
    n_additional_lines         = 1	# n_additional_planes - number of additional planes to be added
    x_lines                   =  0.5, 0.0 	# x-coordinates for additional planes
    y_lines                   = 0.0,-1.0 	# y-coordinates for additional planes
    z_lines                   = 0.0, 0.0 	# z-coordinates for additional planes
    dir_lines                 =   2,   2	# directions of additional planes
#################################################

##########FWH Controls##################
    additional_nodes_active = F      # include additional nodes for patches
    j_additional_nodes      = 9      # j_lev on which to extract data
    n_additional_patches    = 4      # number of additional node patches
    x_patches  = 9.0,-3.0, -3.0,9.0  # x coordinates for patch corners
    y_patches  =  4.0, 4.0, -4.0,-4.0  # y coordinates for patch corners

    FWHstats = F
    FWHdt = 6.0000000e-03            # timestep for FWH. Ensure that it matches dt with no adapt
########################################

###########Hyperbolic Module############
    filter_field = F
    hyper_it_limit = -5
########################################

###########Hyperbolic Module############
    hypermodel = 0	       # hypermodel: flag to turn on hyperbolic module     
    eps_high   = 2.000e-03     # eps_high: error upper bound
    eps_low    = 1.000e-04     # eps_low:  error lower bound
    hyper_mode = 1	       # hyper_mode: 0 for non-conservative, 1 for conservative 
    min_wall_type =  0,0,0     # XMIN, YMIN, ZMIN - 1:reflecting wall, 0:evolutionary
    max_wall_type =  0,0,0     # XMAX, YMAX, ZMAX - 1:reflecting wall, 0:evolutionary

#######################################

###############################################################################################################################################################
