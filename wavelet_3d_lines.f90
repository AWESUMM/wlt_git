! IF there are problems with ghost points then undefine the following which will
! put in code to do an explicit sweep to clear ghost points at the beginning of c_diff_fast
! (THis is in prgress, currently code only works with an explicit sweep... D.G) 
#define EXPLICIT_GHOST_SWEEP

MODULE wlt_trns_mod

  USE precision
  USE debug_vars
  USE wlt_vars
  USE wlt_trns_util_mod
  USE wlt_trns_vars
  USE  db_mod
  USE pde
  USE sizes
  USE io_3D_vars
  USE db_lines_vars
  USE field
  USE share_consts
  USE coord_mapping
  USE wlt_vars
  USE util_vars
  IMPLICIT NONE
  PRIVATE
  INTEGER :: maxval_nxyz ! defined when nxyz is calculated, Needed because some complier do not support intrinsics in declarations
!!  INTEGER, DIMENSION (:,:), ALLOCATABLE:: indx, indx_old
  INTEGER, DIMENSION (:,:,:), ALLOCATABLE :: lvxyz_odd,lvxyz_even
  INTEGER, DIMENSION (:,:,:,:), ALLOCATABLE :: nlvxyz_odd,nlvxyz_even
!!  INTEGER, DIMENSION (:,:,:), ALLOCATABLE :: nlvD,nlvjD
  INTEGER, DIMENSION (:,:), ALLOCATABLE :: lvdxyz_odd,lvdxyz_even
  INTEGER, DIMENSION (:,:,:), ALLOCATABLE :: nlvdxyz_odd,nlvdxyz_even
!!  REAL (pr), DIMENSION (:,:,:,:,:), ALLOCATABLE :: wgh_updt, wgh_prdct !wlt transform weights
  REAL (pr), DIMENSION (:,:,:,:,:), ALLOCATABLE :: wgh_updt_interp, wgh_prdct_interp !wlt transform weights for c_wlt_trns_interp()
  REAL (pr), DIMENSION (:,:,:,:,:), ALLOCATABLE :: wgh_df, wgh_d2f

 LOGICAL , save :: TEST2 = .FALSE. !TESTING ONLY
!==========================================================================================================================

!===================================================================!
!               PUBLIC FUNCTIONS AND SUBROUTINES                    !  
!===================================================================!

  PUBLIC :: adapt_grid, c_diff_fast, c_diff_fast_db, c_diff_diag, c_diff_diag_bnd, c_wlt_trns, c_wlt_trns_mask, &
            init_DB,  update_db_from_u, update_u_from_db_noidices,  weights,  wlt_interpolate, &
			release_memory_db, mdl_filt_EPSplus, &
	get_indices_by_coordinate, &
       c_wlt_trns_interp, &
       c_wlt_trns_interp_free, & 
       c_wlt_trns_interp_setup, &
       pre_init_DB                       ! empty


CONTAINS

SUBROUTINE get_indices_by_coordinate(nvec,ixyz_vec,j_vec,i_vec,ireg)
  USE precision
  USE wlt_vars   ! dim
  IMPLICIT NONE
  INTEGER , INTENT(IN) :: nvec, ireg, j_vec
  INTEGER, DIMENSION(dim,nvec), INTENT(IN) :: ixyz_vec !ixyz coordinates for points needed
  INTEGER, DIMENSION(nvec), INTENT(INOUT) :: i_vec     !i_vec is the vector of indices in

  PRINT *, 'get_indices_by_coordinate is not ready: wavelet_3d_lines.f90'
  STOP 1
END SUBROUTINE get_indices_by_coordinate

SUBROUTINE c_wlt_trns_interp (u_out, c,  nxyz_out, nwlt, j_in, j_out, wlt_fmly, leps) 
  USE precision
  IMPLICIT NONE
  INTEGER, INTENT(IN) :: nwlt,  j_in, j_out, wlt_fmly, nxyz_out(1:3)
  REAL (pr), DIMENSION (1:nwlt), INTENT (IN) :: c
  REAL (pr), DIMENSION (0:nxyz_out(1)+1,0:nxyz_out(2),0:nxyz_out(3)), INTENT (INOUT) :: u_out
  REAL (pr), INTENT (IN) :: leps

  PRINT *, 'c_wlt_trns_interp is not ready: wavelet_3d_lines.f90'
  STOP 1
END SUBROUTINE c_wlt_trns_interp

SUBROUTINE c_wlt_trns_interp_free
  USE precision
  IMPLICIT NONE

  PRINT *, 'c_wlt_trns_interp_free is not ready: wavelet_3d_lines.f90'
  STOP 1
END SUBROUTINE c_wlt_trns_interp_free

SUBROUTINE c_wlt_trns_interp_setup (j_out, nxyz_out, max_nxyz_out, xx_out )
  USE precision
  USE wlt_vars   ! dim
  IMPLICIT NONE
  INTEGER , INTENT(IN) :: j_out, nxyz_out(1:3), max_nxyz_out
  REAL (pr) , INTENT(IN) ::  xx_out(0:max_nxyz_out,1:dim)

  PRINT *, 'c_wlt_trns_interp_setup is not ready: wavelet_3d_lines.f90'
  STOP 1
END SUBROUTINE c_wlt_trns_interp_setup

!
! Initialize a new field data base
!
!  SUBROUTINE init_field( j_mn , j_mx)
!    IMPLICIT NONE
!    INTEGER ,INTENT(IN) :: j_mn, j_mx
!    INTEGER i,j ,ix,iy,iz
!    TYPE (grid_pt) ,POINTER  :: dbpt 
!    

    !
    ! Initialize the field data base
    !
!    CALL init_lines_db(j_mn , j_mx )


!    nwlt = db_nwlt
!    j_lev = j_mn + 1 ! set current wavelet transform level to atleast minimum level
!    j_lev = MIN( j_lev, j_mx ) ! set current wavelet transform level to below the max level


!    nxyz(1:2)=mxyz(1:2)*2**(j_lev-1) !calculate non-adaptive resolution from levels and M
!    nxyz(3)=mxyz(3)*2**(jd*(j_lev-1)) !calculate non-adaptive resolution from levels and M
!	maxval_nxyz = MAXVAL(nxyz)

!  END SUBROUTINE init_field


!
! setup global indices used in transpose routines
! These are line by indices based on j_mx (max resolution of simulation). This routine is 
! called once at the begining.
!

  SUBROUTINE accel_indices_db( ix_lh_db , & 
       mxyz,n_prdct,n_updt,&
       n_assym_prdct,n_assym_updt,n_assym_prdct_bnd,n_assym_updt_bnd,& !changed
       prd,ibc,jd, max_nxyz) 
!    USE precision
!	USE share_consts
    IMPLICIT NONE
    INTEGER, INTENT (IN) ::  n_prdct,n_updt,  max_nxyz
    INTEGER, INTENT (IN) ::  n_assym_prdct(0:n_trnsf_type),n_assym_updt(0:n_trnsf_type)
    INTEGER, INTENT (IN) ::  n_assym_prdct_bnd(0:n_trnsf_type),n_assym_updt_bnd(0:n_trnsf_type)
    INTEGER , DIMENSION(3), INTENT (IN) ::  mxyz,prd
    INTEGER , DIMENSION(6), INTENT (IN) ::  ibc
    TYPE(TRNS_ACCEL) , DIMENSION (0:max_nxyz,1:dim,j_mx_db,0:1), INTENT (INOUT) :: ix_lh_db
    INTEGER, INTENT (IN) :: jd
    INTEGER :: i,j, ix_j
    INTEGER :: ix,ix_l,ix_h,ixp,ixpm,ix_even,ix_odd, trnsf_type

    INTEGER , DIMENSION(6) ::  ibch
    INTEGER :: idim
    INTEGER :: nxyz_out(3)
    INTEGER :: nD, meth
    REAL (pr)  :: xx_db(0:max_nxyz ,1:dim)! xx_db i used to calculate quantities based on the j_mx resolution.
    REAL (pr) :: h_db(1:j_mx_db,1:dim)
    REAL (pr)  :: dxyz(1:dim)

    nxyz_out(1:2) = mxyz(1:2)*2**(j_mx_db-1)
    nxyz_out(3) = mxyz(3)*2**(jd*(j_mx_db-1))
 
    IF (ALLOCATED(wgh_updt_db)) DEALLOCATE (wgh_updt_db)
    IF (ALLOCATED(wgh_prdct_db)) DEALLOCATE (wgh_prdct_db)
    ALLOCATE(wgh_updt_db(-2*n_updt:2*n_updt,   0:max_nxyz,j_mx_db-1,0:1,1:3))
    ALLOCATE(wgh_prdct_db(-2*n_prdct:2*n_prdct,0:max_nxyz,j_mx_db-1,0:1,1:3) )

	
    

    
    IF (ALLOCATED(ix_lh_diff_db)) DEALLOCATE (ix_lh_diff_db)
    ALLOCATE( ix_lh_diff_db(0:max_nxyz, 2, j_mx_db, dim, 0:n_df  ) )


    CALL set_xx (xx_db,h_db,nxyz,MAXVAL(nxyz(1:dim)),j_mx_db)

    !
    !----  Setting the points for wavelet transform at different levels
    ! type == 0 - Do interior and Bnd pnts
    ! type == 1 - Do interior pnts only
	!
    DO trnsf_type = 0,1

    ibch = 0
    IF(trnsf_type == 1) ibch = ibc

    DO j = 1,j_mx_db-1

       !
       !*********** transform in idim-direction *********************
       ! 
       DO idim = 1,dim
          DO ix_j = 1, mxyz(idim)*2**(j-1)
             ! ix_j is consecutive from 1 to max pts on this level j
             ix_odd = (2*ix_j-1)*2**(j_mx_db-1-j) 
             ! ix_odd is the actual index of the point in the unadapted array

             ix_l   = -MIN(ix_j-ibch(2*dim-1),n_prdct)
             ix_h   = ix_l+2*n_prdct-1
             ix_h   = MIN(ix_h,mxyz(idim)*2**(j-1)-ix_j-ibch(2*idim))
             ix_l   = ix_h-2*n_prdct+1
             ix_l = (1-prd(idim))*MAX(ix_l,-ix_j+ibch(idim)) + prd(idim)*(-n_prdct)
             ix_h = (1-prd(idim))*MIN(ix_h,mxyz(idim)*2**(j-1)-ix_j-ibch(2*idim)) + prd(idim)*(n_prdct - 1)
             IF  (ix_l+ix_h+1 < 0 .AND. ix_h >= 0 .AND. prd(idim) ==0 ) THEN      ! the right boundary
                ix_l = MAX(ix_l,-(ix_h+1)-n_assym_prdct(trnsf_type),-ix_j+ibch(1))
             ELSE IF  (ix_l+ix_h+1 < 0 .AND. ix_h == -1 .AND. prd(idim) ==0 ) THEN!right on the right bndry
                ix_l = MAX(ix_l,-1-n_assym_prdct_bnd(trnsf_type),-ix_j+ibch(idim))
             ELSE IF  (ix_l+ix_h+1 > 0 .AND. ix_l < 0 .AND. prd(1) ==0 ) THEN  !  the left boundary
                ix_h = MIN(ix_h,-ix_l-1+n_assym_prdct(trnsf_type),mxyz(idim)*2**(j-1)-ix_j-ibch(2*idim) )
             ELSE IF  (ix_l+ix_h+1 > 0 .AND. ix_l == 0 .AND. prd(idim) ==0 ) THEN !right on the left bndry
                ix_h = MIN(ix_h,n_assym_prdct_bnd(trnsf_type),mxyz(idim)*2**(j-1)-ix_j-ibch(2*idim))   
             END IF
             ix_lh_db(ix_odd,idim,j,trnsf_type)%ix_l =ix_l
             ix_lh_db(ix_odd,idim,j,trnsf_type)%ix_h =ix_h
             ix_lh_db(ix_odd,idim,j,trnsf_type)%ix_j =ix_j

          END DO
          DO ix_j = ibch(idim), mxyz(idim)*2**(j-1)-prd(idim) - ibch(2*idim)
             ix_even = ix_j*2**(j_mx_db-j)
             ix_l = - MIN (ix_j, n_updt)
             ix_h = ix_l + 2*n_updt - 1
             ix_h = MIN (ix_h, mxyz(idim)*2**(j-1)-ix_j-1)
             ix_l = ix_h - 2*n_updt + 1
             ix_l = (1-prd(idim))*MAX(ix_l,-ix_j) + prd(idim)*(-n_updt)
             ix_h = (1-prd(idim))*MIN(ix_h,mxyz(idim)*2**(j-1)-ix_j-1) + prd(idim)*(n_updt - 1)
             IF  (ix_l+ix_h+1 < 0 .AND. ix_h >= 0 .AND. prd(idim) ==0 ) THEN      ! the right boundary
                ix_l = MAX(ix_l,-(ix_h+1)-n_assym_updt(trnsf_type),-ix_j)
             ELSE IF  (ix_l+ix_h+1 < 0 .AND. ix_h == -1 .AND. prd(idim) ==0 ) THEN!right on the right bndry
                ix_l = MAX(ix_l,-1-n_assym_updt_bnd(trnsf_type),-ix_j)
             ELSE IF  (ix_l+ix_h+1 > 0 .AND. ix_l < 0 .AND. prd(idim) ==0 ) THEN  !  the left boundary
                ix_h = MIN(ix_h,-ix_l-1+n_assym_updt(trnsf_type),mxyz(idim)*2**(j-1)-ix_j-1)
             ELSE IF  (ix_l+ix_h+1 > 0 .AND. ix_l == 0 .AND. prd(idim) ==0 ) THEN ! right on the left bndry
                ix_h = MIN(ix_h,n_assym_updt_bnd(trnsf_type),mxyz(idim)*2**(j-1)-ix_j-1)   
             END IF
             ix_lh_db(ix_even,idim,j,trnsf_type)%ix_l =ix_l
             ix_lh_db(ix_even,idim,j,trnsf_type)%ix_h =ix_h
             ix_lh_db(ix_even,idim,j,trnsf_type)%ix_j =ix_j

          END DO
       END DO
    END DO ! j = 1,j_mx_db-1
    END DO



    !
    !-------- setting weights for wavelet transform
    !
    ! type == 0 - Do interior and Bnd pnts
    ! type == 1 - Do interior pnts only
	!
    DO trnsf_type = 0,1
       IF(trnsf_type == 1) THEN
          ibch = ibc
       ELSE
          ibch = 0
       END IF
       DO j = 1, j_mx_db-1
          DO idim =1,dim
             DO ix = 1, mxyz(idim)*2**(j-1)
                ix_odd = (2*ix-1)*2**(j_mx_db-1-j)
                ix_l   = -MIN(ix-ibch(2*idim-1),n_prdct)
                ix_h   = ix_l+2*n_prdct-1
                ix_h   = MIN(ix_h,mxyz(idim)*2**(j-1)-ix-ibch(2*idim))
                ix_l   = ix_h-2*n_prdct+1
                ix_l = (1-prd(idim))*MAX(ix_l,-ix+ibch(2*idim-1)) + prd(idim)*(-n_prdct)
                ix_h = (1-prd(idim))*MIN(ix_h,mxyz(idim)*2**(j-1)-ix-ibch(2*idim)) + prd(idim)*(n_prdct - 1)
                IF  (ix_l+ix_h+1 < 0 .AND. ix_h >= 0 .AND. prd(idim) ==0 ) THEN        ! the right boundary
                   ix_l = MAX(ix_l,-(ix_h+1)-n_assym_prdct(trnsf_type),-ix+ibch(2*idim-1))
                ELSE IF  (ix_l+ix_h+1 < 0 .AND. ix_h == -1 .AND. prd(idim) ==0 ) THEN  ! right on the right boundary
                   ix_l = MAX(ix_l,-1-n_assym_prdct_bnd(trnsf_type),-ix+ibch(2*idim-1) )
                ELSE IF  (ix_l+ix_h+1 > 0 .AND. ix_l < 0 .AND. prd(idim) ==0 ) THEN    !  the left boundary
                   ix_h = MIN(ix_h,-ix_l-1+n_assym_prdct(trnsf_type),mxyz(idim)*2**(j-1)-ix-ibch(2*idim) )
                ELSE IF  (ix_l+ix_h+1 > 0 .AND. ix_l == 0 .AND. prd(idim) ==0 ) THEN   ! right on the left boundary
                   ix_h = MIN(ix_h,n_assym_prdct_bnd(trnsf_type),mxyz(idim)*2**(j-1)-ix-ibch(2*idim))   
                END IF
                DO ixpm = ix_l, ix_h
                   wgh_prdct_db(ixpm,ix_odd,j,trnsf_type,idim)= &
                        wgh (ixpm, ix_odd, ix_l, ix_h, 2**(j_mx_db-1-j), prd(idim), &
                           nxyz_out(idim), xx_db(0:nxyz_out(idim),idim))
                END DO
             END DO
             DO ix = ibch(2*idim-1), mxyz(idim)*2**(j-1)-prd(idim) - ibch(2*idim)
                ix_even = ix*2**(j_mx_db-j)
                ix_l = - MIN (ix, n_updt)
                ix_h = ix_l + 2*n_updt - 1
                ix_h = MIN (ix_h, mxyz(idim)*2**(j-1)-ix-1)
                ix_l = ix_h - 2*n_updt + 1
                ix_l = (1-prd(idim))*MAX(ix_l,-ix) + prd(idim)*(-n_updt)
                ix_h = (1-prd(idim))*MIN(ix_h,mxyz(idim)*2**(j-1)-ix-1) + prd(idim)*(n_updt - 1)
                IF  (ix_l+ix_h+1 < 0 .AND. ix_h >= 0 .AND. prd(idim) == 0) THEN                               ! the right boundary
                   ix_l = MAX(ix_l,-(ix_h+1)-n_assym_updt(trnsf_type),-ix)
                ELSE IF  (ix_l+ix_h+1 < 0 .AND. ix_h == -1 .AND. prd(idim) == 0) THEN                        ! right on the right boundary
                   ix_l = MAX(ix_l,-1-n_assym_updt_bnd(trnsf_type),-ix)
                ELSE IF  (ix_l+ix_h+1 > 0 .AND. ix_l < 0 .AND. prd(idim) == 0) THEN                          !  the left boundary
                   ix_h = MIN(ix_h,-ix_l-1+n_assym_updt(trnsf_type),mxyz(idim)*2**(j-1)-ix-1)
                ELSE IF  (ix_l+ix_h+1 > 0 .AND. ix_l == 0 .AND. prd(idim) == 0) THEN                         ! right on the left boundary
                   ix_h = MIN(ix_h,n_assym_updt_bnd(trnsf_type),mxyz(idim)*2**(j-1)-ix-1)   
                END IF
                DO ixpm = ix_l, ix_h
                   wgh_updt_db(ixpm,ix_even,j,trnsf_type,idim) = &
                        wgh (ixpm, ix_even, ix_l, ix_h, 2**(j_mx_db-1-j), prd(idim), &
                           nxyz_out(idim), xx_db(0:nxyz_out(idim),idim))
                END DO
             END DO
          END DO
       END DO
    END DO
    !
    !-------- END setting weights for wavelet transform
    !


    !
    !----  Setting the ix_l and ix_h points for deriviative at different levels
    !
  DO idim = 1,dim
   DO j = 1, j_mx_db
     DO ix = 0,nxyz_out(idim)
        DO meth = 0, n_df
           IF(meth == 0) THEN 
              nD=1
           ELSE IF(meth == 1) THEN 
              nD=n_diff
           ELSE IF(meth == 2) THEN 
              nD=1
           ELSE IF(meth == 3) THEN 
              nD=n_diff
           ELSE IF(meth == 4) THEN 
              nD=1
           ELSE IF(meth == 5) THEN 
              nD=n_diff
           END IF
           !---------------------- 1-st derivative in idim-direction ---------------
            
           ix_l =-MIN(ix/2**(j_mx_db-j),nD)
           ix_h = ix_l+2*nD
           ix_h = MIN(ix_h, (nxyz_out(idim)-ix)/2**(j_mx_db-j))
           ix_l=ix_h-2*nD
             ix_l=(1-prd(idim))*MAX(MIN(ix_l,0),-ix/2**(j_mx_db-j)) + prd(idim)*(-nD)
			 ix_h=(1-prd(idim))*MIN(MAX(ix_h,0),(nxyz_out(idim)-ix)/2**(j_mx_db-j)) + prd(idim)*(nD)
             IF       (ix_l+ix_h == 0) THEN                                        ! the middle of the domain
                ix_l = ix_l+forward_bias(meth)
                ix_h = ix_h-backward_bias(meth)
             ELSE IF  (ix_l+ix_h < 0 .AND. backward_bias(meth)==1 .AND. ix_h > 0) THEN    ! the right boundary
                ix_l = MAX(ix_l,-ix_h-nd_assym(meth),-ix/2**(j_mx_db-j))
             ELSE IF  (ix_l+ix_h < 0 .AND. forward_bias(meth)==1 .AND. ix_h > 0) THEN  !  the right boundary
                ix_l = MAX(-ix_h+1,-ix/2**(j_mx_db-j))
             ELSE IF  (ix_l+ix_h < 0 .AND. ix_h == 0) THEN                         ! right on the right boundary
                ix_l = MAX(ix_l,-nd_assym_bnd(meth),-ix/2**(j_mx_db-j))
             ELSE IF  (ix_l+ix_h > 0 .AND. backward_bias(meth)==1 .AND. ix_l < 0) THEN    !  the left boundary
                ix_h = MIN(-ix_l-1,(nxyz_out(idim)-ix)/2**(j_mx_db-j))
             ELSE IF  (ix_l+ix_h > 0 .AND. forward_bias(meth)==1 .AND. ix_l < 0 ) THEN !  the left boundary
                ix_h = MIN(ix_h,-ix_l+nd_assym(meth),(nxyz_out(idim)-ix)/2**(j_mx_db-j))!WRONG?
             ELSE IF  (ix_l+ix_h > 0 .AND. ix_l == 0 ) THEN                        ! right on the left boundary
                ix_h = MIN(ix_h,nd_assym_bnd(meth),(nxyz_out(idim)-ix)/2**(j_mx_db-j)) !WRONG?  !comment if do not want reduce the order of derivative 
             END IF
           ix_lh_diff_db(ix,1,j,idim,meth)%ix_l = ix_l
           ix_lh_diff_db(ix,1,j,idim,meth)%ix_h = ix_h
            
           !---------------------- 2-nd derivative in x-direction ---------------
           ix_l =-MIN(ix/2**(j_mx_db-j),nD)
           ix_h = ix_l+2*nD
           ix_h = MIN(ix_h, (nxyz_out(idim)-ix)/2**(j_mx_db-j))
           ix_l=ix_h-2*nD
           IF (ix_l > -nD) ix_h = ix_h+1
           IF (ix_h <  nD) ix_l = ix_l-1
           ix_l = (1-prd(idim))*MAX(ix_l,-ix/2**(j_mx_db-j)) + prd(idim)*(-nD)
           ix_h = (1-prd(idim))*MIN(ix_h,(nxyz_out(idim)-ix)/2**(j_mx_db-j)) + prd(idim)*(nD)!WRONG
           IF(ix_l+ix_h<0) ix_l = MAX(ix_l,-ix_h-nd2_assym(meth))
           IF(ix_l+ix_h>0) ix_h = MIN(ix_h,-ix_l+nd2_assym(meth))
           IF(ix_l+ix_h < 0 .AND. ix_h == 0 ) ix_l = MAX(ix_l,-nd2_assym_bnd(meth),-ix/2**(j_mx_db-j))
           IF(ix_l+ix_h > 0 .AND. ix_l == 0 ) ix_h = MIN(ix_h,nd2_assym_bnd(meth),(nxyz_out(idim)-ix)/2**(j_mx_db-j))
           ix_lh_diff_db(ix,2,j,idim,meth)%ix_l = ix_l ! min pt on left needed by 1st or 2nd deriviative
           ix_lh_diff_db(ix,2,j,idim,meth)%ix_h = ix_h ! max pt on right needed by 1st or 2nd deriviative

          END DO

        END DO
     END DO
  END DO

    !
    !----  END Setting the points for deriviative at different levels
    !


    !
    !-------- setting weights for derivitives
    !
    !
    !-------- END setting weights for derivitives
    !



END SUBROUTINE accel_indices_db


SUBROUTINE set_ghost_pts_db( j_out,j_mn, nxyz_out, max_nxyz)
!  USE precision  
!  USE pde
!  USE share_consts
  IMPLICIT NONE
  INTEGER , INTENT (IN) :: j_out,j_mn, nxyz_out(1:3) ,max_nxyz
  INTEGER (KIND_INT_FLAGS) :: line_flags(0:max_nxyz)
  INTEGER               :: line_coords(0:max_nxyz,dim)  ! coordinated of returned points in terms of j_mx

  TYPE (grid_pt_ptr)    :: line_ptrs(0:max_nxyz)
  INTEGER               :: line_deriv_lev(0:max_nxyz)
  REAL(pr)              :: line_u(1:1 ,0:max_nxyz) !ignored in this routine


  INTEGER :: n_active_pts,active_pts(max_nxyz+1)
  INTEGER :: n_active_pts_even,active_pts_even(max_nxyz+1)
  INTEGER :: n_j_above,j_above(max_nxyz+1)
  INTEGER :: new_pts(max_nxyz+1), n_new_pts
  INTEGER :: delete_pts(max_nxyz+1), n_delete_pts
  INTEGER :: idim, i, ixpm, ixp,  j_deriv_idim, ii
  INTEGER :: ix_l, ix_h, indx_line,indx_line_db, meth
  INTEGER ::   deriv_lev 
  LOGICAL :: lchk
  INTEGER :: db_scale , ixyz_loc(3), ixyz_db(3),ixyz_dbtmp(3) 
  INTEGER :: plocal_numlines_indx_backend ! number of active lines indexed in lines_indx_backend
  TYPE (element_of_face_ptr) , DIMENSION(:), POINTER :: plocal_lines_indx_backend !  list of active line faces
  INTEGER :: plocal_lines_indx_current ! current active line to be returned 



  Print *,'Entering set_ghost(), j_out ', j_out

  db_scale = (2**(j_mx_db-j_out)) 

  n_j_above = 0 ! not used
  n_active_pts_even = 0 ! not used
  n_delete_pts = 0 ! not used

  !
  !----  Find level for derivative calculation at each point
  !
  lv = 0 ! zero out # points at each deriv level
  line_u = 0.0_pr ! make sure the line array is zeroed out
  line_flags = 0  ! make sure the line flags array is zeroed out
  DO idim = 1,dim 

	CALL get_active_lines_indx( idim, j_out, 1, GET_ALL_DERIV_LEV, &
           plocal_lines_indx_backend, plocal_numlines_indx_backend,&
		   plocal_lines_indx_current ) 

	!DO ln = 1, nlines


!ln = 1 !TEST only
	DO WHILE ( get_active_line_flags_deriv_lev( idim, j_out , 0 , j_out ,max_nxyz,   &
                line_flags, line_deriv_lev, line_ptrs, line_coords, &
                active_pts, n_active_pts, NO_DELETE, NO_GHOST_PTS, &
                plocal_lines_indx_backend, plocal_numlines_indx_backend,&
		        plocal_lines_indx_current) )  

		DO i=1,n_active_pts
			indx_line = active_pts(i)


		  j_deriv_idim = j_out+1 !set to max level
          lchk =.TRUE.

          ! check  neighborhood of point ixyx(:) for presence of the scale j_deriv_idim in idim-dir
          ! find the closet point to this point and then find out what level it is at.
		  ! This is done on the line based on local (1-j_out) indexing.
		  !
          DO WHILE(lchk .AND.  j_deriv_idim  > 1)
             j_deriv_idim = j_deriv_idim -1
             ix_l = (1-prd(idim))*&
                  MAX(0,&
                  indx_line-nbhd*2**(j_out-j_deriv_idim)) + prd(idim)*(indx_line-nbhd*2**(j_out-j_deriv_idim))
             ix_h = (1-prd(idim))*&
                  MIN(nxyz_out(idim), &
                  indx_line+nbhd*2**(j_out-j_deriv_idim)) + prd(idim)*(indx_line+nbhd*2**(j_out-j_deriv_idim))
             DO ixpm=ix_l,ix_h,2**(j_out-j_deriv_idim)
                ii  = (1-prd(idim))*ixpm+prd(idim)*MOD(ixpm+9*nxyz_out(idim),nxyz_out(idim))
                ! if this point is active(or marked adj)  and this point is not the point ii
				!   that we are looking at we add it if it is nto already in db

                IF(  IBITS(line_flags(ii),ACTIVE,SIG_FLAG+1) /=0 .AND.  .NOT. BTEST(line_flags(ii),DELETE ) &
					.AND. ii /=indx_line) THEN
					 lchk = .FALSE. 
					 EXIT
			     END IF

				!Sanity Check
				IF( BTEST(line_flags(ii),DELETE ) ) THEN
					PRINT *,'ERROR, Delete point being seen set_ghost_pts_db( ), Exiting...'
					stop
				ENDIF

			    
             END DO
          END DO
 
		  !update the deriviative level for this direction at this point
		  IF( idim == 1) THEN
			 line_deriv_lev(indx_line) =  j_deriv_idim

	      ELSE
			 line_deriv_lev(indx_line) =  MAX(j_deriv_idim,line_deriv_lev(indx_line))
	      END IF

 		END DO ! i=1,n_active_pts_even


		!
		! Put_active_line here
		!
        CALL put_active_line_deriv_lev(  max_nxyz, line_deriv_lev, line_ptrs, &
		    active_pts, n_active_pts) 
!NOTE NEED TO DO THIS IN put_active_line_deriv_lev() to be more efficient D.G.
		line_flags = 0  !BRUTEFORCE make sure the line flags array is zeroed out

!		ln = ln +1 !TEST only
   END DO !ln = 1, nlines
 END DO !idim = 1,dim 


 
  ! ---------------Add ghost points to db --------------------!
  !NOTE the GHOST flag is cleared in the first loop of db_adjacent_wlt()
  !

  !Sanity Check
  IF( db_ghost /= 0  ) THEN
	PRINT *,'ERROR, Entering set_ghost_pts_db, db_ghost /= 0 , exiting...'
	stop
  END IF


  line_u = 0.0_pr ! make sure the line array is zeroed out
  line_flags = 0  ! make sure the line flags array is zeroed out
  DO idim = 1,dim 

	CALL get_active_lines_indx( idim, j_out, 1, GET_ALL_DERIV_LEV, &
           plocal_lines_indx_backend, plocal_numlines_indx_backend,&
		   plocal_lines_indx_current )

	!DO ln = 1, nlines
!ln = 1 !TEST only

	DO WHILE( get_active_line_flags_deriv_lev( idim, j_out , 0 , j_out ,max_nxyz,   &
                line_flags, line_deriv_lev, line_ptrs, line_coords, &
                active_pts, n_active_pts, GET_DEL_INLST, GET_GHOST_PTS, &
                plocal_lines_indx_backend, plocal_numlines_indx_backend,&
		        plocal_lines_indx_current) )  

		!		ixyz_db(:) = lines_indx(ln,:)
		!ixyz_loc(:) = ixyz_db(:) /db_scale 


        n_new_pts = 0
		DO i=1,n_active_pts
			indx_line = active_pts(i)
			ixyz_db   = line_coords(indx_line,:)
			ixyz_loc  = ixyz_db / db_scale 

			!ixyz_loc(idim) = indx_line  
			!ixyz_db(idim)  = ixyz_loc(idim) * db_scale  
			indx_line_db = indx_line * (2**(j_mx_db-j_out)) !find index relative to db coordinates


            !
			! We do not want to add ghost points for current ghost points or
			! points already marked to be deleted.
			!
			IF(   .NOT. BTEST(line_flags(indx_line), GHOST ) .AND. &
			      .NOT. BTEST(line_flags(indx_line), DELETE )  ) THEN 

			    ! we have to do this for each derivative method
				DO meth = 0, n_df

					DO deriv_lev = MAXVAL(lvl_db(ixyz_db(1:dim))), line_deriv_lev(indx_line)
						! add ghost pts from min pt on left needed by 1st or 2nd deriviative to
						!  max pt on right needed by 1st or 2nd deriviative
						DO ixpm = MINVAL(ix_lh_diff_db(indx_line_db,1:2,deriv_lev ,idim,meth)%ix_l), &
							  MAXVAL(ix_lh_diff_db(indx_line_db,1:2,deriv_lev ,idim,meth)%ix_h)

							ixp = (1-prd(idim))*(indx_line+ixpm*2**(j_out-deriv_lev))+ &
								prd(idim)*MOD(indx_line+ixpm*2**(j_out-deriv_lev)+9*nxyz_out(idim),nxyz_out(idim))

							! if this point was not active (or already added as an adjacent)
							! then put it on new points list.
							! check flag bits ACTIVE, ADJACENT_X,ADJACENT_Y,ADJACENT_Z
							ixyz_dbtmp = ixyz_db
							ixyz_dbtmp(idim)  = ixp * db_scale

 
							IF(  IBITS(line_flags(ixp), ACTIVE, dim+1) == 0 .AND. &
								.NOT. BTEST(line_flags(ixp), SIG_FLAG) .AND. &
								.NOT. BTEST(line_flags(ixp), GHOST )  .AND. &
								MAXVAL(lvl_db(ixyz_dbtmp(1:dim))) > j_mn  ) THEN 
								
!DG: we shouldn't need this last check because all j_mn are active
					  
								! this pt is not currently active and it has not
								! already been added as a ghost pt.


								! we check the delete bit to see if this point is still physically in the db
								IF(  BTEST(line_flags(ixp), DELETE ) ) THEN
									line_flags(ixp) = IBSET( line_flags(ixp), GHOST )

									! line_flags(ixp) = IBCLR( line_flags(ixp) , DELETE)   
									! we don't zero out values because this pt could have a value from being
									! previously active and this value is needed in the reverse trns.

	
								ELSE
IF( line_flags(ixp) /= 0 ) THEN
 PRINT *,'ERROR'
END IF
									n_new_pts = n_new_pts +1
									new_pts(n_new_pts) = ixp 
							 
									line_flags(ixp) = IBSET( 0, GHOST )  
							
								END IF


!IF( ixyz_db(1) == 0 .AND. ixyz_db(2) == 0  .AND. ixyz_db(3) == 0  ) 
!WRITE(6,'( I6.6, 3(1X,  I3.3  ) ," ADD GHOST, FROM", 3(  1X, I3.3 ) )' ) &
!    ixyz_dbtmp(1)/db_scale+64*ixyz_dbtmp(2)/db_scale +64*64*ixyz_dbtmp(3)/db_scale,  ixyz_dbtmp/db_scale, ixyz_db/db_scale						
						
								db_ghost = db_ghost + 1 !we increment counter
								!WRITE(*,'("DEBUG:setghost @ ",3(i4,1x))') ixyz_dbtmp 
							END IF
						END DO ! ixpm = ...
					END DO ! 
				END DO !meth = 0, n_df
			END IF
		END DO !i=1,n_active_pts

		
       !
       ! Put the transform result back into the data base
       ! 
       !
       CALL put_active_line( idim, j_out, j_out, max_nxyz, 1,1, &
            line_u, IGNORE_U, line_flags, line_ptrs, &
            active_pts, n_active_pts, &
            active_pts_even, n_active_pts_even, &
            j_above, n_j_above, new_pts, n_new_pts, delete_pts, n_delete_pts, 0 )   

!	   ln = ln +1 !TEST only

	END DO ! ln = 1, nlines
END DO !idim = 1,dim
 CALL deallocate_active_lines_indx(plocal_lines_indx_backend)


! ---------------END Add ghost points to db --------------------!
PRINT *,'set_ghost_pts_db(),  db_ghost           = ', db_ghost 


END SUBROUTINE set_ghost_pts_db




!**************************************************************************
! Begin wavelet transform subroutines
!**************************************************************************
!
! modified to have variable level input-output capability
!
!  u          field  values to be transposed
! npts        number of active collocation points
!             npts=nwlt for tranform
!             npts=nwlt+nghostpts for c_diff inv trns part
!  ie  number first dimension of u_in
!  j_lev_mx_active      Forward transform is done from level j_lev_mx_active to  1
!             Inverse transform is done from level 1 to j_lev_mx_active
!  j_mx       MAximum number of levels (db is stored based on this maximum)
!  i_coef     If 1 then do forward transform, If -1 then do inverse transform
!
SUBROUTINE c_wlt_trns_db(   npts, ie,  mn_var,mx_var, max_nxyz,&
      j_out, j_mx,  i_coef )
!  USE precision
!  USE sizes
  IMPLICIT NONE
  INTEGER, INTENT(IN) :: npts,ie, mn_var,mx_var, max_nxyz
  INTEGER, INTENT(INOUT) :: j_out
  INTEGER, INTENT(IN) :: j_mx, i_coef
  INTEGER :: j
  INTEGER :: trnsf_type


  !
  ! type == 0 - Do transform on interior and Bnd pnts
  ! type == 1 - Do transform on interior pnts only
  !
  trnsf_type = 0 !order of method, used in wgh_ arrays

 
  IF (i_coef == WLT_TRNS_FWD) THEN       ! forward transform


     ! do transform from j_out-1 to 1 levels
     DO j = j_out-1, 1, -1

        !
        ! Do the wlt transform for level j
        !
        call c_wlt_trns_aux_db(npts,  mn_var, mx_var, MAXVAL(mxyz(1:dim)*2**(j_out-1)) , & !max_nxyz, & ! 
              j, j_out, &
             n_prdct, n_updt ,trnsf_type, jd, prd,i_coef )

     END DO


  ELSE IF (i_coef < 0) THEN  !  inverse transform
 
     DO j = 1, j_out-1
 
        !
        ! Do the wlt transform for level j
        !
        call c_wlt_trns_aux_db(npts, mn_var, mx_var,MAXVAL(mxyz(1:dim)*2**(j_out-1)) ,& !max_nxyz, & ! 
             j, j_out, &
             n_prdct, n_updt, trnsf_type, jd, prd,i_coef )

     END DO
   END IF


END SUBROUTINE c_wlt_trns_db

!
! Top level call to do a forward or inverse wlt transform on
! components mn_var,mx_var of u_in and put the result in u_out
!
SUBROUTINE c_wlt_trns(u_in, ie, mn_var,mx_var, wlt_fmly,i_coef)
!  USE precision
  IMPLICIT NONE
  INTEGER  , INTENT (IN)    :: mn_var,mx_var, ie, wlt_fmly, i_coef
  REAL (pr), INTENT (INOUT) :: u_in(1:nwlt,ie)

  INTEGER :: i
  REAL    :: t1_omp, t2_omp

  !
  ! Currently only wlt_fmly == 1 is supported in lines
  !
  IF( wlt_fmly /= 1 ) THEN
     WRITE(*,'(A,I3)') "c_wlt_trns(), called with wlt_fmly = ", wlt_fmly
	 WRITE(*,'(A)') "Only wlt_fmly == 1 is supported in lines currently,"
     WRITE(*,'(A)') "Temporarily force use of wlt_fmly == 1 for this call"
  END IF

  ! update the db from u
  ! u(:, mn_var:mx_var) -> db%u(db_offset:mx_var-mn_var+1)
  ! db_offset = 1 
  CALL update_db_from_u(  u_in , nwlt ,ie  , mn_var, mx_var, 1  )



  IF( flag_timing ) CALL CPU_TIME (t1_omp)

!$OMP PARALLEL DEFAULT(SHARED )
  call c_wlt_trns_db(nwlt, ie ,mn_var,mx_var , MAXVAL(nxyz),&
                     j_lev, j_mx_db,  i_coef )
!$OMP END PARALLEL
  !
  ! Put transformed values back into u
  !
  CALL update_u_from_db_noidices(  u_in , nwlt , ie, mn_var, mx_var , 1)


  IF( flag_timing ) THEN
     CALL CPU_TIME (t2_omp)
     WRITE (6,'("CPU TIME in c_wlt_trns() = ", es12.5)') t2_omp - t1_omp
  END IF

END SUBROUTINE c_wlt_trns

!
! Top level call to do a forward or inverse wlt transform on
! components in u(1:nwlt_in,:)  whose position in mask is .TRUE.
! mask_index locates masked vars in uin/u_out
! 
!! c_wlt_trns_mask() supports 2 masks. The points from mask1 are put at the head of the db array and then the points
! in  mask 2 are added. This is done so that the c_wlt_trns_db() call in adapt_grid() can do the
! inverse transofrm on only the n_var_interpolate variables.
!
SUBROUTINE c_wlt_trns_mask(u_in, u_out, ie, mask, mask2, use_second_mask, old2new, &
     nwlt_in, nwlt_out, j_lev_in, j_lev_out, wlt_fmly, i_coef, do_update_db_from_u_mask, do_update_u_from_db_mask)
  IMPLICIT NONE
  INTEGER, INTENT(IN) :: nwlt_in, nwlt_out, j_lev_out, i_coef, wlt_fmly
  INTEGER, INTENT(INOUT) :: j_lev_in
  INTEGER, INTENT(IN) ::  ie
  LOGICAL, INTENT(IN) :: mask(1:ie), mask2(1:ie) ! transform elements in u whose position in mask is .TRUE.
  LOGICAL, INTENT(IN) :: do_update_db_from_u_mask, do_update_u_from_db_mask
  LOGICAL,   INTENT(IN) :: use_second_mask, old2new
  REAL (pr), DIMENSION (1:nwlt_in,ie) , INTENT (INOUT) :: u_in
  REAL (pr), DIMENSION (1:nwlt_out,ie), INTENT (INOUT) :: u_out

  INTEGER :: i
  REAL t0,t1

  !
  ! Currently only wlt_fmly == 1 is supported in lines
  !
  IF( wlt_fmly /= 1 ) THEN
     WRITE(*,'(A,I3)') "c_wlt_trns_mask(), called with wlt_fmly = ", wlt_fmly
	 WRITE(*,'(A)') "Only wlt_fmly == 1 is supported in lines currently,"
     WRITE(*,'(A)') "Temporarily force use of wlt_fmly == 1 for this call"
  END IF


  IF( flag_timing ) CALL CPU_TIME (t0)



  IF( old2new ) THEN 
     PRINT *,'In c_wlt_trns_mask() old2new== TRUE.. Not sure what this does...'
	 PRINT *,'EXITING...'
	 STOP
  END IF
  !
  ! Put value of u into db if required
  ! 

  IF( do_update_db_from_u_mask )  CALL update_db_from_u_mask(  u_in , nwlt ,ie  , mask , mask2, use_second_mask, 1  )



!$OMP PARALLEL DEFAULT(SHARED )
  CALL c_wlt_trns_db(nwlt_in, ie ,1,COUNT( (mask .OR. mask2) ) , MAXVAL(nxyz),&
                     j_lev_in, j_mx_db,  i_coef )

!$OMP END PARALLEL  
  !
  ! Put value from db back into u if required
  !
  IF( do_update_u_from_db_mask ) CALL update_u_from_db_mask(  u_out , nwlt ,ie  , mask , mask2, use_second_mask, 1  )

  IF( flag_timing ) THEN
     CALL CPU_TIME (t1)
     WRITE (6,'("CPU TIME (c_wlt_trns_mask) = ", es12.5)') t1 - t0
  END IF

END SUBROUTINE c_wlt_trns_mask



!
! do a forward wlt transform for dim dimensions at level j
! 
! dim , 
! npts  -  first subscript of u_in, this will equal nwlt for the normal tranform but it
!          will be nwlt_n for the inv trns in c_diff_fast.
! ie    - number of variables in u_in (second subscript)
! u_in,    the actual u that will be searched in db searches. (For 
!          inv_trns from c_diff this will have to include ghost points
! mx_var  - transform is done across n vars from mn_var to mx_var
! mn_var  - so mn_var=1, mx_var=nvar does the transform for all the variables..
! ix_lh_db - lookup table for the support of the transform at a level 
! j,  
! j_mx,  
! dbmax,  
! nn_prdct,  
! nn_updt,   
! wgh_prdct,  
! wgh_updt,  
! trnsf_type,   
! jd,  
! prd,   
! i_coef
! 
!
! type == 0 - Do transform on interior and Bnd pnts
! type == 1 - Do transform on interior pnts only
!

SUBROUTINE c_wlt_trns_aux_db(npts, mn_var,mx_var, max_nxyz, &
      j, j_out, &
     nn_prdct, nn_updt,  trnsf_type, jd, prd,  i_coef )
  IMPLICIT NONE
  INTEGER, INTENT(IN) :: npts, mn_var,mx_var, max_nxyz
  INTEGER, INTENT(IN) :: j , trnsf_type,i_coef , jd
  INTEGER, INTENT(INOUT) :: j_out

  INTEGER, DIMENSION(3) , INTENT (IN) ::  prd
  INTEGER, DIMENSION(0:n_wlt_fmly), INTENT (IN) :: nn_prdct, nn_updt
  INTEGER :: ix,ix_even,ix_odd,ixp,ixpm,ix_l,ix_h, ix_j
  INTEGER :: indx_line, indx_line_scaled !index into current line being processed
  INTEGER :: n_active_pts_odd,active_pts_odd(max_nxyz+1)
  INTEGER :: n_active_pts_even,active_pts_even(max_nxyz+1)
  INTEGER :: n_j_above,j_above(max_nxyz+1)
  INTEGER :: new_pts(max_nxyz+1), n_new_pts
  INTEGER :: delete_pts(max_nxyz+1), n_delete_pts
  INTEGER :: iy, iz
  INTEGER :: idim, i,ii
  REAL (pr) :: c_predict(mn_var:mx_var)

  REAL (4) :: t0, t1
  REAL(pr)             :: line_u(mn_var:mx_var ,0:max_nxyz)
  INTEGER(KIND_INT_FLAGS):: line_flags(0:max_nxyz)
  INTEGER              :: line_coords(0:max_nxyz,dim)  ! coordinated of returned points in terms of j_mx
  TYPE (grid_pt_ptr)   :: line_ptrs(0:max_nxyz)
  INTEGER :: ixyz_loc(3) !test
  INTEGER :: nxyz_out(3)
  INTEGER :: dbscale, scale_j_out_j, scale_j_out_jm1, ixyz_db(1:dim)
  INTEGER :: inv_use_ghost ! flag for  get_active_line() set from i_coeff
  INTEGER :: odim(1:dim-1) ! the face coordinates of the dimensions other then idim 
  INTEGER :: get_int_bnd_pts ! flag to control if interior or interior and BND points are retrived from DB
  INTEGER , SAVE, DIMENSION(:), ALLOCATABLE :: wrap
  INTEGER :: plocal_numlines_indx_backend ! number of active lines indexed in lines_indx_backend
  TYPE (element_of_face_ptr) , SAVE, DIMENSION(:), POINTER :: plocal_lines_indx_backend !  list of active line faces
  INTEGER :: plocal_lines_indx_current ! current active line to be returned 

!TRY COMPARE 1 LINE FOR OMP and NON-OMP



! Code for running OpenMP start with !$ at the beginning of the line.
! These lines should only be executed when running in OpenMP mode.

!$OMP threadprivate(wrap,plocal_lines_indx_backend)
    INTEGER IAM_OMP, N_Threads_OMP

    IAM_OMP = 0       ! default no OMP
	N_Threads_OMP = 1 ! default no OMP

!$  IAM_OMP = OMP_GET_THREAD_NUM()
!$  N_Threads_OMP = OMP_GET_NUM_THREADS()
!$  PRINT *,'In c_wlt_trns_aux_db(), IAM_OMP, N_Threads_OMP ', IAM_OMP, N_Threads_OMP


!@!  !$  IF( IAM_OMP == 0 ) THEN

  nxyz_out(1:2) = mxyz(1:2)*2**(j_out-1)
  nxyz_out(3) = mxyz(3)*2**(jd*(j_out-1))

  n_delete_pts = 0 ! not used

  !Sanity Check
  IF( max_nxyz /= MAXVAL( nxyz_out ) ) THEN
	PRINT *, 'ERROR, c_wlt_trns_aux_db(), max_nxyz /= MAXVAL( nxyz_out ), Exiting...'
	STOP
  END IF


  dbscale          = (2**(j_mx_db-j_out))
  scale_j_out_j    = 2**(j_out-j)
  scale_j_out_jm1  = 2**(j_out-1-j)

  line_u = 0.0_pr ! make sure the line array is zeroed out
  line_flags = 0  ! make sure the line array is zeroed out
  n_new_pts = 0 ! we will not add new db points from this routine so we can do this once outside get_active_line loop
  

  !
  ! define if we do interior transform (type = 1) or full transform (type=0)
  ! 
  ! type == 0 - Do transform on interior and Bnd pnts
  ! type == 1 - Do transform on interior pnts only
  !

  IF( trnsf_type == 0 ) THEN
     get_int_bnd_pts = GET_ALL_PTS
  ELSE
     get_int_bnd_pts = GET_INT_PTS
  END IF

  !set whether to do inv transform with or without ghost points
  IF( i_coef == WLT_TRNS_INV )        inv_use_ghost = NO_GHOST_PTS
  IF( i_coef == WLT_TRNS_INV_wGHOST ) inv_use_ghost = GET_GHOST_PTS 

  ! Do real to wlt transform
  IF( i_coef == WLT_TRNS_FWD ) THEN

     ! NOTE FOR FUTURE PARALLELIZATION
     ! WE CAN GET ALL THE LINES IN THE 3 DIRECTIONS HERE. tHAT DISTRIBUTES
     ! ALL THE ACTIVE POINTS FOR THIS LEVEL TRANSFORM TO THE N-PROCESSORS.
     ! THEN BELOW BETWEEN CHANGING DIRECTIONS (DO idim = 1,dim) THE 
     ! PROCESSORS CAN EXCANGE THE ACTIVE NODES TO GET THE CORRECT LINES FOR THE
     ! NEXT DIRECTION. THIS IS ESSNETIALLY A SPARSE MATRIX TRANSPOSE OPERATION.
     ! LOOK AT HOW PARALLEL BLAIS ROUTINES DO THIS! dg 16/01/2004


     !
     !*********** transform in idim -direction *********************
     !       
     DO idim = 1,dim


		 !
		 ! wrap mapping array for periodic case
		 ! NOTE: needs to be updated to do wlt_families, 04/25/06 DG
		 i = MAX(n_prdct(1), n_updt(1))*2**(j_out-j) !IS THIS CORRECT
		 ALLOCATE(wrap(-i:max_nxyz+i) ) 
		 DO ii =  -i,max_nxyz+i
			wrap(ii) = MOD(ii+9*nxyz_out(idim),nxyz_out(idim))
		 END DO

        ! Get index of lines that we need to transform for this direction.
        ! Return lines and nlines as the number of active lines
        ! If lines is not allocated or it is not large enough
        ! it will be (re)allocated.
        ! This routine puts the result in the global array: lines_indx

!        PRINT *,'Call get_active_lines_indx, FWD'
        CALL get_active_lines_indx( idim, j+1, 1, GET_ALL_DERIV_LEV, &
           plocal_lines_indx_backend, plocal_numlines_indx_backend,&
		   plocal_lines_indx_current ) !

        !
        ! Do predict and then update for each active line.
        ! PARALLIZE this loop! such that nlines is broken accross 
        ! N processors


        ! for now get one line at a time
        ! line is the actual data line, active points are the
        ! indices of the active collocation points
		
        DO WHILE ( get_active_line(idim, j+1, j_out, max_nxyz, mn_var, mx_var, &
                line_u, line_flags, line_ptrs, line_coords, odim, &
                active_pts_odd, n_active_pts_odd, &
                active_pts_even, n_active_pts_even ,&
                j_above, n_j_above, delete_pts, n_delete_pts, NO_DELETE ,NO_GET_J_ABOVE ,&
			    LINE_EVEN_ODD, NO_GHOST_PTS,get_int_bnd_pts, SORT_PTS_FALSE, &
                plocal_lines_indx_backend, plocal_numlines_indx_backend,&
		        plocal_lines_indx_current)  )   

           IF( prd(idim) == 1 ) THEN !periodic domain in idim direction

			   ! do predict stage for odd points
			   DO i=1,n_active_pts_odd
				  indx_line = active_pts_odd(i)
                  indx_line_scaled = indx_line*dbscale

!TEST
!WRITE(*,'("fwd trns", i6 )') indx_line !, char_flags(line_flags(indx_line))
!print *,char_flags(line_flags(indx_line))

				  ix_l=ix_lh_db(indx_line_scaled, idim,j,trnsf_type)%ix_l ! # support points lower
				  ix_h=ix_lh_db(indx_line_scaled, idim,j,trnsf_type)%ix_h ! # support points higher 
				  ix_j=ix_lh_db(indx_line_scaled, idim,j,trnsf_type)%ix_j ! indx consecutive from 1 to max pts on this level 

				  c_predict(mn_var:mx_var) = 0.0_pr
 
				  DO ixpm = ix_l, ix_h
					 ii = wrap( (ix_j+ixpm)*scale_j_out_j )
					 ! ii = MOD(ii+9*nxyz_out(idim),nxyz_out(idim))
					 c_predict(mn_var:mx_var) = c_predict(mn_var:mx_var) + &
						  wgh_prdct_db(ixpm,indx_line_scaled,j,trnsf_type,idim) * line_u(mn_var:mx_var,ii)

				  END DO
				  line_u(mn_var:mx_var,indx_line) = 0.5_pr*&
					   (line_u(mn_var:mx_var,indx_line) - c_predict(mn_var:mx_var))

			   END DO


			   !do update for even points

			   DO i=1,n_active_pts_even
				  indx_line = active_pts_even(i)
                  indx_line_scaled = indx_line*dbscale

				  ix_l=ix_lh_db(indx_line_scaled, idim,j,trnsf_type)%ix_l ! # support points lower
				  ix_h=ix_lh_db(indx_line_scaled, idim,j,trnsf_type)%ix_h ! # support points higher 
				  ix_j=ix_lh_db(indx_line_scaled, idim,j,trnsf_type)%ix_j ! indx consecutive from 1 to max pts on this lev
				  c_predict(mn_var:mx_var) = 0.0_pr

				  DO ixpm = ix_l, ix_h
					 ii = wrap( (2*( ix_j+ixpm)+1)*scale_j_out_jm1 )
					 !ii = MOD(ii+9*nxyz_out(idim),nxyz_out(idim))
					 c_predict(mn_var:mx_var) = c_predict(mn_var:mx_var) + &
						  wgh_updt_db(ixpm,indx_line_scaled,j,trnsf_type,idim) * line_u(mn_var:mx_var,ii)

				  END DO
				  line_u(mn_var:mx_var,indx_line) = line_u(mn_var:mx_var,indx_line) + &
					   c_predict(mn_var:mx_var)
			   END DO

           ELSE !non-periodic in idim direction

			   ! do predict stage for odd points
			   DO i=1,n_active_pts_odd
				  indx_line = active_pts_odd(i)
                  indx_line_scaled = indx_line*dbscale

				  ix_l=ix_lh_db(indx_line_scaled, idim,j,trnsf_type)%ix_l ! # support points lower
				  ix_h=ix_lh_db(indx_line_scaled, idim,j,trnsf_type)%ix_h ! # support points higher 
				  ix_j=ix_lh_db(indx_line_scaled, idim,j,trnsf_type)%ix_j ! indx consecutive from 1 to max pts on this level 

				  c_predict(mn_var:mx_var) = 0.0_pr
 
				  DO ixpm = ix_l, ix_h
					 ii = (ix_j+ixpm)*scale_j_out_j
					 c_predict(mn_var:mx_var) = c_predict(mn_var:mx_var) + &
						  wgh_prdct_db(ixpm,indx_line_scaled,j,trnsf_type,idim) * line_u(mn_var:mx_var,ii)

				  END DO
				  line_u(mn_var:mx_var,indx_line) = 0.5_pr*&
					   (line_u(mn_var:mx_var,indx_line) - c_predict(mn_var:mx_var))

			   END DO


			   !do update for even points

			   DO i=1,n_active_pts_even
				  indx_line = active_pts_even(i)
                  indx_line_scaled = indx_line*dbscale

				  ix_l=ix_lh_db(indx_line_scaled, idim,j,trnsf_type)%ix_l ! # support points lower
				  ix_h=ix_lh_db(indx_line_scaled, idim,j,trnsf_type)%ix_h ! # support points higher 
				  ix_j=ix_lh_db(indx_line_scaled, idim,j,trnsf_type)%ix_j ! indx consecutive from 1 to max pts on this lev
				  c_predict(mn_var:mx_var) = 0.0_pr

				  DO ixpm = ix_l, ix_h
					 ii = (2*( ix_j+ixpm)+1)*scale_j_out_jm1
					 c_predict(mn_var:mx_var) = c_predict(mn_var:mx_var) + &
						  wgh_updt_db(ixpm,indx_line_scaled,j,trnsf_type,idim) * line_u(mn_var:mx_var,ii)

				  END DO
				  line_u(mn_var:mx_var,indx_line) = line_u(mn_var:mx_var,indx_line) + &
					   c_predict(mn_var:mx_var)
			   END DO

		   END IF !periodic
           !
           ! Put the transform result back into the data base
           ! 
           !
           CALL put_active_line( idim, j, j_out, max_nxyz, mn_var,mx_var, &
                line_u, UPDATE_U, line_flags, line_ptrs, &
                active_pts_odd, n_active_pts_odd, &
                active_pts_even, n_active_pts_even, &
                j_above, n_j_above, new_pts, n_new_pts, delete_pts, n_delete_pts, 0 )   

			!ln = ln +1 !TEST only
        END DO

		DEALLOCATE(wrap)
     END DO

  ELSE IF (i_coef < 0) THEN ! Do wlt to real transform
     !!CALL CPU_TIME (t0)

     DO idim = dim,1,-1


		 !
		 ! wrap mapping array for periodic case
		 !NOTE: needs to be updated to do wlt_families, 04/25/06 DG
		 i = MAX(n_prdct(1), n_updt(1))*2**(j_out-j) !IS THIS CORRECT
		 ALLOCATE(wrap(-i:max_nxyz+i) ) 
		 DO ii =  -i,max_nxyz+i
			wrap(ii) = MOD(ii+9*nxyz_out(idim),nxyz_out(idim))
		 END DO


        ! Get index of lines that we need to transform for this direction.
        ! Return lines and nlines as the number of active lines
        ! If lines is not allocated or it is not large enough
        ! it will be (re)allocated.
        ! This routine puts the result in the global array: lines_indx

!        PRINT *,'Call get_active_lines_indx, INV'
        CALL get_active_lines_indx( idim, j+1, 1, GET_ALL_DERIV_LEV, &
           plocal_lines_indx_backend, plocal_numlines_indx_backend,&
		   plocal_lines_indx_current ) !



        !
        ! Do predict and then update for each active line.
        ! PARALLIZE this loop! such that nlines is broken accross 
        ! N processors
        !DO ln = 1,nlines


           ! for now get one line at a time
           ! line is the actual data line, active points are the
           ! indices of the active collocation points 
           ! (get deleted points in the line (but not in the list of odd/even points
           ! to act on)
!ln = 1 !TEST only
         DO WHILE ( get_active_line(idim, j+1, j_out, max_nxyz, mn_var,mx_var, &
                line_u, line_flags, line_ptrs, line_coords, odim, &
                active_pts_odd, n_active_pts_odd, &
                active_pts_even, n_active_pts_even , &
                j_above, n_j_above, delete_pts, n_delete_pts, GET_DEL_PTS,NO_GET_J_ABOVE, &
				LINE_EVEN_ODD, inv_use_ghost,get_int_bnd_pts, SORT_PTS_FALSE, &
                plocal_lines_indx_backend, plocal_numlines_indx_backend,&
		        plocal_lines_indx_current) )  




           IF( prd(idim) == 1 ) THEN !periodic domain in idim direction

				
			   !do update for even points
			   DO i=1,n_active_pts_even
				  indx_line = active_pts_even(i)
                  indx_line_scaled = indx_line*dbscale

!IF( i_coef == WLT_TRNS_INV_wGHOST .and. line_u(1,indx_line) /= 0.0 )THEN
!   PRINT *,'error',char_flags(line_flags(indx_line))
!END IF 
				  ix_l=ix_lh_db(indx_line_scaled, idim,j,trnsf_type)%ix_l ! # support points lower
				  ix_h=ix_lh_db(indx_line_scaled, idim,j,trnsf_type)%ix_h ! # support points higher 
				  ix_j=ix_lh_db(indx_line_scaled, idim,j,trnsf_type)%ix_j ! indx consecutive from 1 to max pts on this lev
				  c_predict(mn_var:mx_var) = 0.0_pr

				 DO ixpm = ix_l, ix_h
					 ii = wrap( (2*(ix_j+ixpm)+1)*scale_j_out_jm1 )
					 !ii = MOD(ii+9*nxyz_out(idim),nxyz_out(idim))
!IF( i_coef == WLT_TRNS_INV_wGHOST .and. line_u(1,ii) /= 0.0 )THEN
   
!   PRINT *,'error',char_flags(line_flags(ii))
!END IF 

					 c_predict(mn_var:mx_var) = c_predict(mn_var:mx_var) + &
						  wgh_updt_db(ixpm,indx_line_scaled,j,trnsf_type,idim) * line_u(mn_var:mx_var,ii)
				 END DO

				  line_u(mn_var:mx_var,indx_line) = line_u(mn_var:mx_var,indx_line) - &
					   c_predict(mn_var:mx_var)

			   END DO

			   ! do predict stage for odd points
			   DO i=1,n_active_pts_odd
				  indx_line = active_pts_odd(i)
                  indx_line_scaled = indx_line*dbscale
!IF( i_coef == WLT_TRNS_INV_wGHOST .and. line_u(1,indx_line) /= 0.0 )THEN
!   PRINT *,'error',char_flags(line_flags(indx_line))
!END IF 

				  ix_l=ix_lh_db(indx_line_scaled, idim,j,trnsf_type)%ix_l ! # support points lower
				  ix_h=ix_lh_db(indx_line_scaled, idim,j,trnsf_type)%ix_h ! # support points higher 
				  ix_j=ix_lh_db(indx_line_scaled, idim,j,trnsf_type)%ix_j ! indx consecutive from 1 to max pts on this level 
				  c_predict(mn_var:mx_var) = 0.0_pr

				 DO ixpm = ix_l, ix_h
					 ii = wrap( (ix_j+ixpm)*scale_j_out_j )
					 !ii = MOD(ii+9*nxyz_out(idim),nxyz_out(idim))
!IF( i_coef == WLT_TRNS_INV_wGHOST .and. line_u(1,ii) /= 0.0 )THEN
!   PRINT *,'error',char_flags(line_flags(ii))
!END IF 

					 c_predict(mn_var:mx_var) = c_predict(mn_var:mx_var) + &
						  wgh_prdct_db(ixpm,indx_line_scaled,j,trnsf_type,idim) * line_u(mn_var:mx_var,ii)

				  END DO

				  line_u(mn_var:mx_var,indx_line) = 2.0_pr*line_u(mn_var:mx_var,indx_line) &
					   + c_predict(mn_var:mx_var)

			   END DO
           ELSE !non-periodic in idim direction
				
			   !do update for even points
			   DO i=1,n_active_pts_even
				  indx_line = active_pts_even(i)
                  indx_line_scaled = indx_line*dbscale

				  ix_l=ix_lh_db(indx_line_scaled, idim,j,trnsf_type)%ix_l ! # support points lower
				  ix_h=ix_lh_db(indx_line_scaled, idim,j,trnsf_type)%ix_h ! # support points higher 
				  ix_j=ix_lh_db(indx_line_scaled, idim,j,trnsf_type)%ix_j ! indx consecutive from 1 to max pts on this lev
				  c_predict(mn_var:mx_var) = 0.0_pr

				 DO ixpm = ix_l, ix_h
					 ii = (2*(ix_j+ixpm)+1)*scale_j_out_jm1

					 c_predict(mn_var:mx_var) = c_predict(mn_var:mx_var) + &
						  wgh_updt_db(ixpm,indx_line_scaled,j,trnsf_type,idim) * line_u(mn_var:mx_var,ii)
				 END DO

				  line_u(mn_var:mx_var,indx_line) = line_u(mn_var:mx_var,indx_line) - &
					   c_predict(mn_var:mx_var)

			   END DO

			   ! do predict stage for odd points
			   DO i=1,n_active_pts_odd
				  indx_line = active_pts_odd(i)
                  indx_line_scaled = indx_line*dbscale

				  ix_l=ix_lh_db(indx_line_scaled, idim,j,trnsf_type)%ix_l ! # support points lower
				  ix_h=ix_lh_db(indx_line_scaled, idim,j,trnsf_type)%ix_h ! # support points higher 
				  ix_j=ix_lh_db(indx_line_scaled, idim,j,trnsf_type)%ix_j ! indx consecutive from 1 to max pts on this level 
				  c_predict(mn_var:mx_var) = 0.0_pr

				 DO ixpm = ix_l, ix_h
					 ii = (ix_j+ixpm)*scale_j_out_j

					 c_predict(mn_var:mx_var) = c_predict(mn_var:mx_var) + &
						  wgh_prdct_db(ixpm,indx_line_scaled,j,trnsf_type,idim) * line_u(mn_var:mx_var,ii)

				  END DO

				  line_u(mn_var:mx_var,indx_line) = 2.0_pr*line_u(mn_var:mx_var,indx_line) &
					   + c_predict(mn_var:mx_var)

			   END DO

            END IF !periodic
           !
           ! Put the transform result back into the data base
           ! n_new_pts = 0 so j_out will not be changed in this routine
           !
           CALL put_active_line( idim, j, j_out, max_nxyz, mn_var,mx_var, &
                line_u, UPDATE_U,line_flags, line_ptrs, &
                active_pts_odd, n_active_pts_odd, &
                active_pts_even, n_active_pts_even, &
                j_above, n_j_above, new_pts, n_new_pts, delete_pts, n_delete_pts, DELETE_ZERO_ONLY )   
            

        END DO
		DEALLOCATE(wrap)

     END DO
 
  ELSE
     print *,'ERROR, c_wlt_trns_aux_db() called with i_coeff = ', i_coef
     print *,'ERROR  Correct values are 1 -fwd trns or -1 inv trns. Exiting ....'
     stop
  ENDIF
  CALL deallocate_active_lines_indx(plocal_lines_indx_backend)

! OpenMP, sync everything up before returning
!PRINT *,'END SUBROUTINE c_wlt_trns_aux_db'
!$OMP BARRIER

!@! !$  END IF  ! IF( IAM_OMP == 0 ) THEN
END SUBROUTINE c_wlt_trns_aux_db



!
!************ Calculating Adjacent wavelets ***************************
!
! This routine significant and then adjacent points and adds them to the db
! 
! leps                - theshold
! scl                 - scaling of field variables
! l_n_var_adapt       - logical array defining which variables we use to define grid adaptation
! l_n_var_adapt_index - index array to find location in u( ,:) of the variables (to support multiple sime step integration method2)
! ij_adj(-1,0,1)      - Number of adjacent wavelets to retain in level below, current, and above 
! adj_type(-1,0,1)    - for level below,current,above (0 - less conservative, 1 - more conservative)
! j_out               - j_out is the new j_lev if it changed during this call
! j_mn                - All pts retained on levels jmn and below
! j_mx                - j_mx The highest level allowed for this run
! nxyz_out            - extents of resolution at level j_out
!
! 
SUBROUTINE db_adjacent_wlt ( leps, scl, &
     l_n_var_adapt, l_n_var_adapt_index,  ij_adj, adj_type, &
     j_out, j_mn, j_mx,nxyz_out)
  IMPLICIT NONE
  INTEGER, INTENT (IN) ::  j_mn, j_mx

  INTEGER, INTENT (INOUT) :: j_out
  INTEGER, DIMENSION (-1:1) :: ij_adj, adj_type
  INTEGER, DIMENSION(3), INTENT (INOUT) :: nxyz_out
  LOGICAL , INTENT (IN) :: l_n_var_adapt(1:n_var)
  INTEGER , INTENT (IN) :: l_n_var_adapt_index(1:n_var)
  INTEGER :: i, ie
  INTEGER :: ix_l,ix_h!,iy_l,iy_h,iz_l,iz_h
  INTEGER :: ixyz_db(3), ixyz_loc(3), ixyz_wlt(3), ixyz_dummy(3)
  INTEGER :: idim, ipm,ixp,j, j_out_new, j_in, j_ln, j_pt
  INTEGER :: j_adj,ij_step, i_l,i_h, face_switch
  REAL (pr), DIMENSION (1:n_var), INTENT (IN) :: scl
  REAL (pr), INTENT (IN) :: leps
  REAL (pr):: eps_bnd, epsh
  LOGICAL :: check
  ! arguments for db routines (have to allocate these....
  INTEGER :: n_active_pts_odd,n_active_pts_even, n_new_pts, n_j_above, n_delete_pts
  INTEGER :: indx_line !index into current line being processed
  INTEGER , SAVE, ALLOCATABLE ,DIMENSION(:) :: active_pts_odd
  INTEGER , SAVE, ALLOCATABLE ,DIMENSION(:) :: active_pts_even
  INTEGER , SAVE, ALLOCATABLE ,DIMENSION(:) :: j_above
  INTEGER , SAVE, ALLOCATABLE ,DIMENSION(:) :: new_pts
  INTEGER , SAVE, ALLOCATABLE ,DIMENSION(:) :: delete_pts
  REAL(pr), SAVE, ALLOCATABLE ,DIMENSION(:,:) :: line_u
  INTEGER(KIND_INT_FLAGS), SAVE, ALLOCATABLE ,DIMENSION(:) :: line_flags
  INTEGER , SAVE, ALLOCATABLE ,DIMENSION(:,:) :: line_coords
  INTEGER , SAVE, ALLOCATABLE ,DIMENSION(:) :: line_deriv_lev
  INTEGER , SAVE, ALLOCATABLE ,DIMENSION(:) :: line_flat_indx
  TYPE (grid_pt_ptr) , SAVE, ALLOCATABLE  ,DIMENSION(:)  :: line_ptrs
  LOGICAL significant
  INTEGER :: max_l_n_var_adapt
  INTEGER :: nxyz_out_new(3)
  INTEGER :: iidim, iiidim
  INTEGER :: db_scale !scaleing from local to database indexing
  INTEGER :: ixyztmp(3), n_del !
  INTEGER :: odim(1:dim-1) ! the face coordinates of the dimensions other then idim 
  INTEGER (KIND_INT_FLAGS):: tmpflags
  INTEGER ::ix,iy,iz ,tmp, n_sig , lvls_tmp(dim),lvls_db(dim),ii !test
  INTEGER :: plocal_numlines_indx_backend ! number of active lines indexed in lines_indx_backend
  TYPE (element_of_face_ptr) , DIMENSION(:), POINTER :: plocal_lines_indx_backend !  list of active line faces
  INTEGER :: plocal_lines_indx_current ! current active line to be returned 
  INTEGER :: n_j_mn_not_sig ! counter for # points <= j_mn that are NOT significant
!$OMP threadprivate(active_pts_odd)
!$OMP threadprivate(active_pts_even)
!$OMP threadprivate(j_above)
!$OMP threadprivate(new_pts)
!$OMP threadprivate(delete_pts)
!$OMP threadprivate(line_u)
!$OMP threadprivate(line_flags)
!$OMP threadprivate(line_coords)
!$OMP threadprivate(line_deriv_lev)
!$OMP threadprivate(line_flat_indx)
!$OMP threadprivate(line_ptrs)
  db_nwlt_new = 0
  j_out_new   = 0
  ! Zero out line arrays
  n_active_pts_odd =0
  n_active_pts_even=0
  n_new_pts=0
  n_j_above=0
  n_j_mn_not_sig = 0
  PRINT *,' '
  PRINT *,' Entering db_adjacent_wlt(), db_nwlt_new = ', db_nwlt_new
  PRINT *,' j_out, j_lev',j_out, j_lev
  PRINT *,' j_mn,j_mx',j_mn,j_mx
  PRINT *,' j_out_new,nxyz_out',j_out_new,nxyz_out
  PRINT *,' '

  j_in = MAX(j_lev,j_out) !save input value of j_out
  !
  ! Find significant. Look at levels up to j_in
  !
  nxyz_out_new(1:2) = mxyz(1:2)*2**(j_in-1)
  nxyz_out_new(3) = mxyz(3)*2**(jd*(j_in-1))

  db_scale = (2**(j_mx_db-j_in))

  ! allocate the arrays used in db access routines
  ALLOCATE( active_pts_odd(MAXVAL(nxyz_out_new)+1)  )
  ALLOCATE( active_pts_even(MAXVAL(nxyz_out_new)+1) )
  ALLOCATE( j_above(MAXVAL(nxyz_out_new)+1) )
  ALLOCATE( new_pts(MAXVAL(nxyz_out_new)+1)       )
  ALLOCATE( delete_pts(MAXVAL(nxyz_out_new)+1)    )
  ALLOCATE( line_u(1:n_var ,0:MAXVAL(nxyz_out_new))  )
  ALLOCATE( line_flags(0:MAXVAL(nxyz_out_new))    )
  ALLOCATE( line_coords(0:MAXVAL(nxyz_out_new),dim)   )

  ALLOCATE( line_ptrs(0:MAXVAL(nxyz_out_new))     )
  ALLOCATE( line_deriv_lev(0:MAXVAL(nxyz_out_new)) )
  ALLOCATE( line_flat_indx(0:MAXVAL(nxyz_out_new)) )

  n_delete_pts = 0 ! not used yet

  line_u = 0.0_pr ! make sure the line array is zeroed out
  line_flags = 0  ! zero flags

  n_sig = 0
  
  

  n_del = 0 !number being deleted

  !
  ! LOOK FOR POINTS THAT ARE NO LONGER SIGNIFICANT
  !

  idim = 1
  j_out_new = 1 ! j_mn !set j_in_new to lowest possible level

  CALL get_active_lines_indx( idim, j_in, 1, GET_ALL_DERIV_LEV, &
           plocal_lines_indx_backend, plocal_numlines_indx_backend,&
		   plocal_lines_indx_current ) !4/18/05 DG changed from j_in+1 to j_in
       
	       
  !DO ln = 1,nlines
	n_new_pts = 0 ! no new active points added so we can set n_new_pts = 0 once outside of get_active_line loop

	!
	!  get all active pts on this line
	!  
	!
	!ln = 1 !TEST only
	DO WHILE( get_active_line_all_lvl_le_j( idim, 0 ,j_in ,j_in  ,MAXVAL(nxyz_out_new),  &
		1, n_var , &
		line_u, line_flags,  line_deriv_lev, line_flat_indx, line_ptrs, line_coords, &
		active_pts_even, n_active_pts_even, GET_GHOST_PTS, &
        plocal_lines_indx_backend, plocal_numlines_indx_backend,&
		plocal_lines_indx_current  ))
		  
	!ixyz_db(:) = lines_indx(ln,:)
	!ixyz_loc(:) = ixyz_db(:) /db_scale 

	!
	! Mark significant points only in idim ==1 direction
	! for even points
	!


	DO i=1,n_active_pts_even
		indx_line = active_pts_even(i)
		ixyz_db   = line_coords(indx_line,:)
		ixyz_loc  = ixyz_db / db_scale 

		!ixyz_loc(idim) = indx_line
		!ixyz_db(idim)  = ixyz_loc(idim) * db_scale 


		!Clear GHOST flag.
		IF(  BTEST(line_flags(indx_line), GHOST ) ) THEN

			line_flags(indx_line) = IBCLR( line_flags(indx_line), GHOST )
	        line_flags(indx_line) = &
				   IBSET( line_flags(indx_line) , DELETE )   !NEW  
			line_u(1:n_var,indx_line) =	 0.0_pr ! zero out this node !new     
			db_ghost = db_ghost -1 
			!WRITE(*,'("DEBUG:db_adj del ghost @ ",3(i4,1x))') ixyz_db
                 
		ELSE
		   !FIND SIGNIFICANT

		   ! find points that are currently active on level
		   ! j_mn +1 to j_in_new, If theye are >= eps then we retain them
		   ! if they are < eps we mark them for possible future deletion
		   significant = .FALSE.
		   DO ie=1,n_var

			  IF( l_n_var_adapt(ie) )THEN
!PRINT *, 'Adjacent chck sig ',       line_u(l_n_var_adapt_index(ie),indx_line)       
				 IF( ( ABS( line_u(l_n_var_adapt_index(ie),indx_line) ) >= &
					  leps*scl(ie)) )THEN
					significant = .TRUE.
					j_out_new = MAX(j_out_new, MAXVAL(lvl_db(ixyz_db(1:dim)))) !track current highest level
					
					exit
				 END IF
			  END IF
		   END DO

		   IF( significant )THEN
			  n_sig = n_sig+1
			  line_flags(indx_line) = &
				   IBSET( line_flags(indx_line) , SIG_FLAG ) 
			  db_nwlt_new = db_nwlt_new +1 
           ELSE IF( MAXVAL(lvl_db(ixyz_db(1:dim)))<=  j_mn ) THEN
		      n_j_mn_not_sig = n_j_mn_not_sig +1

		   ELSE IF( MAXVAL(lvl_db(ixyz_db(1:dim)))>  j_mn .AND. &
       
			 IBITS(line_flags(indx_line),ADJACENT_X,ADJACENT_X+dim-1 ) ==0 )THEN
!??? why do we check for ADJACENT flags here??? DG			
			  line_flags(indx_line) = &
				   IBSET( line_flags(indx_line) , DELETE )                 
			  line_flags(indx_line) = &
				   IBCLR( line_flags(indx_line) , ACTIVE )
			  n_del = n_del +1!test

		   END IF
		END IF

	END DO
           
!TEST!
!DO i=1,n_active_pts_even
!  DO ii=1,n_active_pts_even
!        IF( active_pts_even(ii) == i ) THEN
!!		  indx_line = active_pts_even(ii)
!		  ixyz_db   = line_coords(indx_line,:)
!
!		  PRINT *,'PT ', indx_line,ixyz_db, char_flags(INT(line_flags(indx_line), KIND_INT_FLAGS))
!		END IF
!  END DO
!END DO
	!
	! Put active line back into db
	! we at level j_in and we will only update flags int he db here
	!
	CALL put_active_line( idim, j_in, j_in, MAXVAL(nxyz_out_new),  1, n_var, &
		line_u, UPDATE_U, line_flags, line_ptrs, &
		active_pts_odd, n_active_pts_odd, &
		active_pts_even, n_active_pts_even, &
		j_above, n_j_above, new_pts, n_new_pts, delete_pts, n_delete_pts, 0 ) 
		
	!ln = ln +1 !TEST only
  
  END DO ! ln = 1,nlines


  ! deallocate arrays not needed anymore
  DEALLOCATE( line_deriv_lev  )
  DEALLOCATE( line_flat_indx  )
	
  !
  ! Find adjacent wlts 
  !
 
 
  IF( debug_level >= 1 ) THEN
     WRITE (*,'(" ")')
     WRITE (*,'("=======================================================")')
     WRITE (*,'("after significant_wlt, # significant wlts = ", I12)') n_sig
     WRITE (*,'("after significant_wlt, j_lev_new          = ", I6)') j_out
     WRITE (*,'("=======================================================")')
     WRITE (*,'(" ")')
  END IF

  IF( debug_level >= 2 ) THEN
     PRINT *,' After db_significant, j_out_new = ', j_out_new
     PRINT *,' After db_significant, n_sig = ', n_sig

     PRINT *,' After db_significant, db_ghost = ', db_ghost
     PRINT *,' After db_significant, n_del = ', n_del
  END IF

  ! set level one more then current max significant level  
  ! (j_out_new) 
  !
  j_out = MIN(j_out_new+1,j_mx_db)

  IF( j_out /= j_in) THEN

	nxyz_out_new(1:2) = mxyz(1:2)*2**(j_out-1)
	nxyz_out_new(3) = mxyz(3)*2**(jd*(j_out-1))

	db_scale = (2**(j_mx_db-j_out))

	! re-allocate the arrays used in db access routines
	DEALLOCATE( active_pts_odd  )
	DEALLOCATE( active_pts_even )
	DEALLOCATE( j_above         )
	DEALLOCATE( new_pts         )
	DEALLOCATE( delete_pts      )
	DEALLOCATE( line_u          )
	DEALLOCATE( line_flags      )
	DEALLOCATE( line_coords     )
	DEALLOCATE( line_ptrs       )


	ALLOCATE( active_pts_odd(MAXVAL(nxyz_out_new)+1)  )
	ALLOCATE( active_pts_even(MAXVAL(nxyz_out_new)+1) )
	ALLOCATE( j_above(MAXVAL(nxyz_out_new)+1) )
	ALLOCATE( new_pts(MAXVAL(nxyz_out_new)+1)       )
	ALLOCATE( delete_pts(MAXVAL(nxyz_out_new)+1)       )
	ALLOCATE( line_u(1:n_var ,0:MAXVAL(nxyz_out_new))  ) !we use ignore_u for this second part
	ALLOCATE( line_flags(0:MAXVAL(nxyz_out_new))    )
    ALLOCATE( line_coords(0:MAXVAL(nxyz_out_new),dim)   )
	ALLOCATE( line_ptrs(0:MAXVAL(nxyz_out_new))     )


  END IF !IF( j_out /= j_out_new )

  ! here j_out_new is the max significant level
  ! j_out is one more than the max significant level (if  <= j_mx) to allow for possible
  ! addition of adjacent points in then level above.

  PRINT *,' Begin db_adj, j_out_new = ', j_out_new


  !****************************************************************************
  ! Find new adjacent points
  !****************************************************************************
PRINT *,'new adjXXXX'
!
! CHECK conservative same level and non-conservative level above is not supported
!
IF( adj_type(0) /= adj_type(1) ) THEN
   PRINT *, 'adj_type(0) /= adj_type(1)  NOT supported for lines version.. Exiting...'
   STOP
END IF

!PRINT *,'db(0,124,124) ', db(0,124,124)%flags , lvl_db(0), lvl_db(124), lvl_db(124)
  !
  ! We loop from j_out to j_mn-1 
  ! or in the case of conservative we loop over level j_out also
  !
  line_u = 0.0_pr ! make sure the line array is zeroed out
  line_flags = 0  ! make sure the line flags array is zeroed out

  !!!!!!! TO MANY IFS
  DO j=MIN(j_mx_db,j_out),j_mn-1,-1 ! j_lev -> j_mn (TESTING IF THIS SHOULD BE  J_MN TRY IT DG MARCH30 ..)
    
     DO idim =1,dim
        
        j_ln = MIN(j+MIN(1,idim-1),j_out) ! j_ln = j if dim == 1 else j_ln = j+1
		!j_pt = MIN(j+1,j_out)             ! get points at j_pt
        IF( adj_type(1) == 0 ) j_ln = j ! if non-conservative above j_ln is always j ADDED DG 

        CALL get_active_lines_indx( idim, j_ln, 1, GET_ALL_DERIV_LEV, &
           plocal_lines_indx_backend, plocal_numlines_indx_backend,&
		   plocal_lines_indx_current ) !XCHANGED !changed from j-1 (I think this should be j )
        
        !DO ln = 1,nlines
           
           ! get one line at a time
           ! line is the actual data line, active points are the
           ! indices of the active collocation points 
           ! we get all variable from 1:Nparam_var and then use the 
           ! logical array:  l_n_var_adapt_index(: ) to decide which
           ! variables we will adapt to.
           
           !We should be using Ignore_u, make sure dimensions of line_u are correct!! ??
        DO WHILE( get_active_line( idim, j, j_out  ,MAXVAL(nxyz_out_new),  &
                1, n_var, &
                line_u, line_flags, line_ptrs, line_coords, odim, &
                active_pts_odd, n_active_pts_odd, &
                active_pts_even, n_active_pts_even ,  &
                j_above, n_j_above, delete_pts, n_delete_pts, GET_DEL_INLST ,GET_ABOVE_PTS ,&
				LINE_ONE , NO_GHOST_PTS,GET_ALL_PTS, SORT_PTS_FALSE, &
                plocal_lines_indx_backend, plocal_numlines_indx_backend,&
		        plocal_lines_indx_current) )            
				 ! active_pts_even are all points on line!! at level j and below

		   n_new_pts = 0 ! zero out count of new 

           !
		   !get the current active line's face coordinate
		   !           
!THIS NEEDS TP BE UPDATED, DO WE NEED FULL FACE COORD OR ODIM?		   ixyz_db(:) = get_active_line_face_coord()
           !ixyz_loc(:) = ixyz_db(:) /db_scale
 

           ! we do this for the even points if atleast one of the other idim directions
           ! is at level j (ie all the points are associated with this level so we need
           ! to do adjacent zone for them at this level.
           !
           iidim = MOD(idim,3)+1
           iiidim = MOD(idim+1,3)+1
           
           !ONLY LOOK FOR ADJACENT AROUND POINTS OF LEVEL >=  J_MN -1 
           !?? what does this do??
           
           ! Find all adjacent points
		   ! We called get_active_line() with argument ONE_LINE so active_pts_even list contains
		   ! all points with  1D level <= j
           DO i=1,n_active_pts_even
              indx_line = active_pts_even(i)
              !ixyz_loc(idim) = indx_line
              !ixyz_db(idim)  = ixyz_loc(idim) * db_scale

			  ixyz_db   = line_coords(indx_line,:)
			  ixyz_loc  = ixyz_db / db_scale

!WRITE(*,'("In adj ", 3(i3.3,1X), 10A )' ) ixyz_db(1), ixyz_db(2),ixyz_db(3),&
!   char_flags(line_flags(indx_line))
!WRITE(*,'("In adj", 10A )' )  char_flags(line_flags(indx_line))
!IF( ixyz_loc(1) == 30 .AND. ixyz_loc(2) == 4 .AND. ixyz_loc(3) == 0 ) THEN
! print *,''
!END IF

			  !           
              ! make 1:dim , 1 if it is psi , 0 if it is phi on level j+1
			  ! psi ==d==odd
              ! phi ==c==even
			  !OLD ixyz_wlt(:) = MOD(ixyz_loc(:)/2**(j_out-j_ln),2)
			  ixyz_wlt(:) = MIN( 1, MOD(ixyz_loc(:)/2**(j_out-j_ln),2**(j_ln-(j-1)) ) )
			  ixyz_wlt(idim) =    MOD(ixyz_loc(idim)/2**(j_out-j),2)

!tmp =1 !TEST
         
!PRINT *,' '
!PRINT *,'----------------'
!PRINT *,' ixyz_wlt ',ixyz_wlt
!PRINT *,' ixyz_loc j',ixyz_loc, j

              !Sanity check
              IF(  BTEST(line_flags(indx_line), GHOST ) ) THEN
                 PRINT *,'ERROR Ghost in adj loop'
                 stop
              END IF

              !
              ! Find adjacent points
              !
              ! we process this point if it is active or in the case of 
              ! conservative adjacent zone (adj_type(0) == 1) we process this point if
              ! active or it was flagged as an adjacent zone coeff in any previous
              ! direction sweep.
            
!OLD              IF( MAXVAL(ixyz_wlt(1:dim)) == 1 .AND. &  ! true at least one psi (==d==odd)
!OLD                   ( MAXVAL(lvl_db(ixyz_db(1:dim)))==j .AND. BTEST(line_flags(indx_line), SIG_FLAG ) &
!OLD				   .OR. &
!OLD                     ( adj_type(0) == 1 .AND. idim  > 1 .AND. &
!OLD                     IBITS(line_flags(indx_line),ADJACENT_X,ADJACENT_X+idim-2 )/=0) ) &
!OLD                     ) THEN
!NEW
              IF( ( MAXVAL(lvl_db(ixyz_db(1:dim)))==j & !3D level == j
			       .AND. BTEST(line_flags(indx_line), SIG_FLAG ) ) & ! pt is significant
				   .OR. &
                     ( MAXVAL(adj_type(0:1)) == 1 .AND. idim  > 1 .AND. & ! conservative and idim>1
                     IBITS(line_flags(indx_line),ADJACENT_X,ADJACENT_X+idim-2 )/=0 ) & ! adjacent pt
                     ) THEN

              !oldnew
!oldnew              IF( ( MAXVAL(ixyz_wlt(1:dim)) == 1 .AND. &  ! true at least one psi (d)
!oldnew                   BTEST(line_flags(indx_line), SIG_FLAG ) ) &
!oldnew				    .OR. &
!oldnew                    (  MAXVAL(adj_type(0:1)) == 1 .AND. idim  > 1 .AND. &
!oldnew                     IBITS(line_flags(indx_line),ADJACENT_X,ADJACENT_X+idim-2 )/=0 ) &
!oldnew                     ) THEN

                 !IF(  .NOT. BTEST(line_flags(indx_line), SIG_FLAG )) THEN
                 !  PRINT *,'In adj', char_flags(line_flags(indx_line))
                 !  PRINT *,''
                    !END IF
                 
                 !testingDO j_adj=1,MIN(1,j_mx_db-j) !CURRENT + FINER LEVEL
                 DO j_adj = 0,MIN(1,j_out-j)!CURRENT + FINER LEVEL
                    ij_step    = MIN(MAX(1,2**(j_out-j-j_adj)),2**(j_out-1))
                    !MAX ensures it does not go above j_out, index is integer)
                    i_l = ixyz_loc(idim)-ij_adj(j_adj)*ij_step
                    i_h = ixyz_loc(idim)+ij_adj(j_adj)*ij_step
                    ! account for boundary in periodic or non-periodic case
                    i_l = (1-prd(idim))*MAX(0,i_l                ) + prd(idim)*i_l
                    i_h = (1-prd(idim))*MIN(  i_h, nxyz_out_new(idim)) + prd(idim)*i_h
                    IF(adj_type(j_adj) == 0) THEN

                       !
                       ! For non-conservative
                       ! zero out i_l and i_h if on face or edge in perpendicular to  idim direction
                       ! 
                       face_switch = MAX(0,ABS(j_adj)) * ixyz_wlt(idim) + & !one if level above or below and psi in idim dir
                                      ( 1-MAX(0, ABS(j_adj)) ) * &          !one if on same level  
									  ( 1 - (1 - MIN(ABS(SUM(ixyz_wlt(1:dim))-1),1)) * ixyz_wlt(idim) ) !zero if on an edge (only one psi)
									                                                                    !one if face or corner or inside cell           
                       i_l = ixyz_loc(idim) + ( i_l-ixyz_loc(idim) ) * face_switch
                       i_h = ixyz_loc(idim) + ( i_h-ixyz_loc(idim) ) * face_switch
                    END IF
!PRINT *,' i_l , i_h ', i_l , i_h
                    DO ipm = i_l, i_h, ij_step
                       ixp =(1-prd(idim))*ipm + prd(idim)* &
                            MOD(ipm+9*nxyz_out_new(idim),nxyz_out_new(idim))
                       IF( ixp /= ixyz_loc(idim) ) THEN
                          ixyztmp=ixyz_db
                          ixyztmp(idim) = ixp*db_scale 

!IF( j < MAXVAL(lvl_db(ixyz_db(1:dim))) ) THEN
!  PRINT *, 'bad point ',ixyz_db
!END IF
!XXXXX
!
!tmpflags = line_flags(ixp)
! PRINT *, ixyztmp(1) /db_scale +   ixyztmp(2)*64 /db_scale   +   ixyztmp(3)*64*64 /db_scale  ,&
!    ixyztmp/db_scale,j_out, char_flags(tmpflags  )
!IF( ixyztmp(1) == 30 .AND. ixyztmp(2) == 2 .AND. ixyztmp(3) == 0 ) THEN
!   lvls_tmp = lvl_db(ixyztmp(1:dim))
!   lvls_db  = lvl_db(ixyz_db(1:dim))
!   PRINT *,''
!END IF
                 
                          !
                          ! if this point was not active (or already added as an adjacent)
                          ! then put it on new points list.
                          ! check flag bits ACTIVE, ADJACENT_X,ADJACENT_Y,ADJACENT_Z
                          ! we check the delete bit to see if this point is still physically in the db
                          
                          IF(  IBITS(line_flags(ixp), ACTIVE, SIG_FLAG+1) == 0 .AND. &
                               .NOT. BTEST(line_flags(ixp), DELETE ) ) THEN
!TEST
!PRINT *,'adj zone add flag',  line_flags(ixp)                            
                             n_new_pts = n_new_pts +1
                             new_pts(n_new_pts) = ixp
                             
                             !
							 ! count this new adjacent if it is above j_mn 
							 !(if below is was already counted above)
							 !
                             IF( MAXVAL(lvl_db(ixyztmp(1:dim)))>  j_mn) n_sig = n_sig +1 !we increment counter!test
                             db_nwlt_new = db_nwlt_new +1!test
                             
                             !
                             ! keep track whether we add any points on the j_out level
                             !
                             j_out_new = MAX(j_out_new,MAXVAL(lvl_db(ixyztmp(1:dim))) )
                             
                          END IF
!IF(tmp==1) THEN
!  WRITE(6,'( 3( I3.3 , 1X ) ,"LOOK " )',ADVANCE='NO' ) ixyz_db 
!  tmp = 2
!END IF
!WRITE(6,'( 3( I3.3 , 1X ) ,"ADD " )',ADVANCE='NO' ) ixyztmp
                             
                          line_flags(ixp)= IBSET( line_flags(ixp), ADJACENT_X+idim-1 )
                          
                          IF(BTEST(line_flags(ixp), DELETE ) ) THEN
                             n_del = n_del -1!test

                             !
							 ! count this new adjacent if it is above j_mn 
							 !(if below is was already counted above)
							 !
                             IF( MAXVAL(lvl_db(ixyztmp(1:dim)))>  j_mn) n_sig = n_sig +1 !we increment counter!test
                             db_nwlt_new = db_nwlt_new +1!test
                          END IF
                          
                          line_flags(ixp) = IBCLR( line_flags(ixp) , DELETE)
                       END IF
                    END DO  !DO ipm
                 END DO  ! DO j_adj=0,1 !CURRENT + FINER LEVEL
              END IF  !end find adjacent
!IF(tmp==2) WRITE(6,'( "" )')
           END DO !DO i=1,n_active_pts_even !all points on line
        
        !
        ! put line back and update new adjacent points
        !
        ! NOTE: we call  put_active_line() with mn_var=mx_var=1 because we have not
        ! changed the actual field values.
        ! 
        !NOTE can add check to see if any points were modified, if not we do not need to cal put_active_line()
 !TEST!
!DO i=1,n_active_pts_even
!  DO ii=1,n_active_pts_even
!        IF( active_pts_even(ii) == i ) THEN
!		  indx_line = active_pts_even(ii)
!		  ixyz_db   = line_coords(indx_line,:)
!
!		  PRINT *,'PT ', indx_line,ixyz_db, char_flags(INT(line_flags(indx_line), KIND_INT_FLAGS))
!		END IF
!  END DO
!END DO       
        CALL put_active_line( idim, j, j_out, MAXVAL(nxyz_out_new),  1, n_var , &
                line_u, IGNORE_U, line_flags, line_ptrs, &
                active_pts_odd, n_active_pts_odd, &
                active_pts_even, n_active_pts_even, &
                j_above, n_j_above, new_pts, n_new_pts, delete_pts, n_delete_pts, 0 )   

        END DO !DO ln = 1,nlines
	END DO !DO idim =1,dim
  END DO !DO j=j_out-1,j_mn,-1 ! j_lev -> j_mn

  PRINT *,'leaving db_adj  n_sig (+adj) = ', n_sig

! deallocate the arrays used in db access routines
  DEALLOCATE( active_pts_odd  )
  DEALLOCATE( active_pts_even )
  DEALLOCATE( j_above         )
  DEALLOCATE( new_pts         )
  DEALLOCATE( delete_pts      )
  DEALLOCATE( line_u          )
  DEALLOCATE( line_flags      )
  DEALLOCATE( line_coords     )
  DEALLOCATE( line_ptrs       )

 CALL deallocate_active_lines_indx(plocal_lines_indx_backend)


  j_out = MAX(j_out,j_mn) ! make sure that j_out is not less then j_mn  
  !update nxyz_out to the level of j_out
  nxyz_out(1:2) = mxyz(1:2)*2**(j_out-1)
  nxyz_out(3) = mxyz(3)*2**(jd*(j_out-1))

  



  !
  ! End find adjacent wlts 
  !


  !
  ! Adding extra points around boundary if we are at a non-periodic boundary
  ! and BNDzone = TRUE in the input file

  IF(BNDzone) THEN
	PRINT * ,' Error, BNDzone == TRUE, this is not currently supported in lines_db database version, Exiting...'
  END IF
  !
!!$  IF(BNDzone) THEN !--- Adding boundary points, modify this subroutine later
!!$
!!$  IF( dim == 3 ) THEN
!!$     PRINT *, ' Code to do BNDzone is not updated for 3D yet. Dan G. 5/22/03 Exiting ...'
!!$  ENDIF
!!$
!!$     DO j = 1, j_out
!!$        IF(prd(1) == 0) THEN
!!$           DO ly = 0, nxyz_out(2) - prd(2), 2**(j_out-j)
!!$              ! left boundary
!!$              check = .FALSE.
!!$              DO lx = 0, MIN(n_prdct*2**(j_out-j),nxyz_out(1)), 2**(j_out-j)
!!$                 IF(i_c(lx,ly,iz)) check=.TRUE.
!!$              END DO
!!$              IF(check) THEN
!!$                 DO lx = 0, MIN(n_prdct*2**(j_out-j),nxyz_out(1)), 2**(j_out-j)
!!$                    i_c(lx,ly,iz) =.TRUE.
!!$              END DO
!!$              END IF
!!$              ! right boundary
!!$              check = .FALSE.
!!$              DO lx = nxyz_out(1), MAX(nxyz_out(1)-n_prdct*2**(j_out-j),0),-2**(j_out-j)
!!$                 IF(i_c(lx,ly,iz)) check=.TRUE.
!!$              END DO
!!$              IF(check) THEN
!!$                 DO lx = nxyz_out(1), MAX(nxyz_out(1)-n_prdct*2**(j_out-j),0),-2**(j_out-j)
!!$                    i_c(lx,ly,iz) =.TRUE.
!!$                 END DO
!!$              END IF
!!$           END DO
!!$        END IF
!!$        IF(prd(2) == 0) THEN
!!$           DO lx = 0, nxyz_out(1) - prd(1), 2**(j_out-j)
!!$              ! bottom boundary
!!$              check = .FALSE.
!!$              DO ly = 0, MIN(n_prdct*2**(j_out-j),nxyz_out(2)), 2**(j_out-j)
!!$                 IF(i_c(lx,ly,iz)) check=.TRUE.
!!$              END DO
!!$              IF(check) THEN
!!$                 DO ly = 0, MIN(n_prdct*2**(j_out-j),nxyz_out(2)), 2**(j_out-j)
!!$                    i_c(lx,ly,iz) =.TRUE.
!!$                 END DO
!!$              END IF
!!$              ! upper boundary
!!$              check = .FALSE.
!!$              DO ly = nxyz_out(2), MAX(nxyz_out(2)-n_prdct*2**(j_out-j),0),-2**(j_out-j)
!!$                 IF(i_c(lx,ly,iz)) check=.TRUE.
!!$              END DO
!!$              IF(check) THEN
!!$                 DO ly = nxyz_out(2), MAX(nxyz_out(2)-n_prdct*2**(j_out-j),0),-2**(j_out-j)
!!$                    i_c(lx,ly,iz) =.TRUE.
!!$                 END DO
!!$              END IF
!!$           END DO
!!$        END IF
!!$     END DO
!!$  END IF




  db_nwlt = n_sig + n_j_mn_not_sig

  IF( debug_level >= 2 ) THEN
     PRINT *, 'leaving db_adjacent_wlt()  sig+adj',n_sig
     PRINT *, 'leaving db_adjacent_wlt()  n_j_mn_not_sig',n_j_mn_not_sig
     PRINT *, 'leaving db_adjacent_wlt() db_nwlt = sig+adj+n_j_mn_not_sig = ',db_nwlt
     PRINT *, 'leaving db_adjacent_wlt() j_out = ', j_out
  END IF

  IF( debug_level >= 1 ) THEN
     WRITE (*,'(" ")')
     WRITE (*,'("=======================================================")')
     WRITE (*,'("after adjacent_wlt, # significant+adjacent wlts = ", I12)') db_nwlt
     WRITE (*,'("after adjacent_wlt, j_lev_new                   = ", I6)') j_out
     WRITE (*,'("=======================================================")')
     WRITE (*,'(" ")')
  END IF

END SUBROUTINE db_adjacent_wlt

#ifdef FALSE
SUBROUTINE db_adjacent_wlt_OLD ( leps, scl, &
     l_n_var_adapt, l_n_var_adapt_index,  ij_adj, adj_type, &
     j_out, j_mn, j_mx,nxyz_out)
  IMPLICIT NONE
  INTEGER, INTENT (IN) ::  j_mn, j_mx

  INTEGER, INTENT (INOUT) :: j_out
  INTEGER, DIMENSION (-1:1) :: ij_adj, adj_type
  INTEGER, DIMENSION(3), INTENT (INOUT) :: nxyz_out
  INTEGER :: i, ie
  INTEGER :: ix_l,ix_h!,iy_l,iy_h,iz_l,iz_h
  INTEGER :: ixyz_db(3), ixyz_loc(3)
  INTEGER :: idim, ipm,ixp,j, j_out_new, j_in
  INTEGER :: j_adj,ij_step, i_l,i_h
  REAL (pr), DIMENSION (1:n_var), INTENT (IN) :: scl
  REAL (pr), INTENT (IN) :: leps
  REAL (pr):: eps_bnd, epsh
  LOGICAL :: check
  LOGICAL , INTENT (IN) :: l_n_var_adapt(1:n_var)
  INTEGER , INTENT (IN) :: l_n_var_adapt_index(1:n_var)
  ! arguments for db routines (have to allocate these....
  INTEGER :: n_active_pts_odd,n_active_pts_even, n_new_pts, n_j_above, n_delete_pts
  INTEGER :: indx_line !index into current line being processed
  INTEGER ,ALLOCATABLE ,DIMENSION(:) :: active_pts_odd
  INTEGER ,ALLOCATABLE ,DIMENSION(:) :: active_pts_even
  INTEGER ,ALLOCATABLE ,DIMENSION(:) :: j_above
  INTEGER ,ALLOCATABLE ,DIMENSION(:) :: new_pts
  INTEGER ,ALLOCATABLE ,DIMENSION(:) :: delete_pts
  REAL(pr),ALLOCATABLE ,DIMENSION(:,:) :: line_u
  INTEGER(KIND_INT_FLAGS),ALLOCATABLE ,DIMENSION(:) :: line_flags
  INTEGER ,ALLOCATABLE ,DIMENSION(:,:) :: line_coords
  INTEGER ,ALLOCATABLE ,DIMENSION(:) :: line_deriv_lev
  INTEGER ,ALLOCATABLE ,DIMENSION(:) :: line_flat_indx
  TYPE (grid_pt_ptr) ,ALLOCATABLE  ,DIMENSION(:)  :: line_ptrs
  LOGICAL significant
  INTEGER :: max_l_n_var_adapt
  INTEGER :: nxyz_out_new(3)
  INTEGER :: iidim, iiidim
  INTEGER :: db_scale !scaleing from local to database indexing
  INTEGER :: ixyztmp(3), n_del !
  INTEGER :: odim(1:dim-1) ! the face coordinates of the dimensions other then idim 

  INTEGER ::ix,iy,iz ,tmp, n_sig !test

  db_nwlt_new =0
  
  ! Zero out line arrays
  n_active_pts_odd =0
  n_active_pts_even=0
  n_new_pts=0
  n_j_above=0

  PRINT *,'Entering db_adjacent_wlt(), db_nwlt_new = ', db_nwlt_new, j_mn,j_mx,j_out_new,nxyz_out

  j_in = j_out !save input value of j_out
  !
  ! Find significant. Look at levels up to j_in
  !
  nxyz_out_new(1:2) = mxyz(1:2)*2**(j_in-1)
  nxyz_out_new(3) = mxyz(3)*2**(jd*(j_in-1))

  db_scale = (2**(j_mx_db-j_in))

  ! allocate the arrays used in db access routines
  ALLOCATE( active_pts_odd(MAXVAL(nxyz_out_new)+1)  )
  ALLOCATE( active_pts_even(MAXVAL(nxyz_out_new)+1) )
  ALLOCATE( j_above(MAXVAL(nxyz_out_new)+1) )
  ALLOCATE( new_pts(MAXVAL(nxyz_out_new)+1)       )
  ALLOCATE( delete_pts(MAXVAL(nxyz_out_new)+1)    )
  ALLOCATE( line_u(1:n_var ,0:MAXVAL(nxyz_out_new))  )
  ALLOCATE( line_flags(0:MAXVAL(nxyz_out_new))    )
  ALLOCATE( line_coords(0:MAXVAL(nxyz_out_new),dim)   )

  ALLOCATE( line_ptrs(0:MAXVAL(nxyz_out_new))     )
  ALLOCATE( line_deriv_lev(0:MAXVAL(nxyz_out_new)) )
  ALLOCATE( line_flat_indx(0:MAXVAL(nxyz_out_new)) )

  n_delete_pts = 0 ! not used yet

  line_u = 0.0_pr ! make sure the line array is zeroed out
  line_flags = 0  ! zero flags

  n_sig = 0
  
  

  n_del = 0 !number being deleted

  !
  ! LOOK FOR POINTS THAT ARE NO LONGER SIGNIFICANT
  !

  idim = 1
  j_out_new = 1 ! j_mn !set j_in_new to lowest possible level

  CALL get_active_lines_indx( idim, j_in, 1, GET_ALL_DERIV_LEV, &
           plocal_lines_indx_backend, plocal_numlines_indx_backend,&
		   plocal_lines_indx_current ) !4/18/05 DG changed from j_in+1 to j_in
       
	       
  !DO ln = 1,nlines
	n_new_pts = 0 ! no new active points added so we can set n_new_pts = 0 once outside of get_active_line loop

	!
	!  get all active pts on this line
	!  
	!
	!ln = 1 !TEST only
	DO WHILE( get_active_line_all_lvl_le_j( idim, 0 ,j_in ,j_in  ,MAXVAL(nxyz_out_new),  &
		1, n_var , &
		line_u, line_flags,  line_deriv_lev, line_flat_indx, line_ptrs, line_coords, &
		active_pts_even, n_active_pts_even, GET_GHOST_PTS, &
           plocal_lines_indx_backend, plocal_numlines_indx_backend,&
		   plocal_lines_indx_current  ))
		  
	!ixyz_db(:) = lines_indx(ln,:)
	!ixyz_loc(:) = ixyz_db(:) /db_scale 

	!
	! Mark significant points only in idim ==1 direction
	! for even points
	!


	DO i=1,n_active_pts_even
		indx_line = active_pts_even(i)
		ixyz_db   = line_coords(indx_line,:)
		ixyz_loc  = ixyz_db / db_scale 

		!ixyz_loc(idim) = indx_line
		!ixyz_db(idim)  = ixyz_loc(idim) * db_scale 


		!Clear GHOST flag.
		IF(  BTEST(line_flags(indx_line), GHOST ) ) THEN

			line_flags(indx_line) = IBCLR( line_flags(indx_line), GHOST )
	              
			db_ghost = db_ghost -1 
                 
		ELSE
		   !FIND SIGNIFICANT

		   ! find points that are currently active on level
		   ! j_mn +1 to j_in_new, If theye are >= eps then we retain them
		   ! if they are < eps we mark them for possible future deletion
		   significant = .FALSE.
		   DO ie=1,n_var

			  IF( l_n_var_adapt(ie) )THEN
!PRINT *, 'Adjacent chck sig ',       line_u(l_n_var_adapt_index(ie),indx_line)       
				 IF( ( ABS( line_u(l_n_var_adapt_index(ie),indx_line) ) >= &
					  leps*scl(ie)) )THEN
					significant = .TRUE.
					j_out_new = MAX(j_out_new, MAXVAL(lvl_db(ixyz_db(1:dim)))) !track current highest level
					
					exit
				 END IF
			  END IF
		   END DO

		   IF( significant )THEN
			  n_sig = n_sig+1
			  line_flags(indx_line) = &
				   IBSET( line_flags(indx_line) , SIG_FLAG ) 
			  db_nwlt_new = db_nwlt_new +1 


		   ELSE IF( MAXVAL(lvl_db(ixyz_db(1:dim)))>  j_mn .AND. &
       
			 IBITS(line_flags(indx_line),ADJACENT_X,ADJACENT_X+dim-1 ) ==0 )THEN
			
			  line_flags(indx_line) = &
				   IBSET( line_flags(indx_line) , DELETE )                 
			  line_flags(indx_line) = &
				   IBCLR( line_flags(indx_line) , ACTIVE )
			  n_del = n_del +1!test

		   END IF
		END IF

	END DO
           

	!
	! Put active line back into db
	! we at level j_in and we will only update flags int he db here
	!
	CALL put_active_line( idim, j_in, j_in, MAXVAL(nxyz_out_new),  1, n_var, &
		line_u, UPDATE_U, line_flags, line_ptrs, &
		active_pts_odd, n_active_pts_odd, &
		active_pts_even, n_active_pts_even, &
		j_above, n_j_above, new_pts, n_new_pts, delete_pts, n_delete_pts, 0 ) 
		
	!ln = ln +1 !TEST only
  
  END DO ! ln = 1,nlines

  
  PRINT *,' After db_significant, j_out_new = ', j_out_new
  PRINT *,' After db_significant, n_sig = ', n_sig

  PRINT *,' After db_significant, db_ghost = ', db_ghost
  PRINT *,' After db_significant, n_del = ', n_del


  ! deallocate arrays not needed anymore
  DEALLOCATE( line_deriv_lev  )
  DEALLOCATE( line_flat_indx  )
	
  !
  ! Find adjacent wlts 
  !
 
  ! set level one more then current max significant level  
  ! (j_out_new) 
  !
  j_out = MIN(j_out_new+1,j_mx_db)
 

  IF( j_out /= j_in) THEN

	nxyz_out_new(1:2) = mxyz(1:2)*2**(j_out-1)
	nxyz_out_new(3) = mxyz(3)*2**(jd*(j_out-1))

	db_scale = (2**(j_mx_db-j_out))

	! re-allocate the arrays used in db access routines
	DEALLOCATE( active_pts_odd  )
	DEALLOCATE( active_pts_even )
	DEALLOCATE( j_above         )
	DEALLOCATE( new_pts         )
	DEALLOCATE( delete_pts      )
	DEALLOCATE( line_u          )
	DEALLOCATE( line_flags      )
	DEALLOCATE( line_coords     )
	DEALLOCATE( line_ptrs       )


	ALLOCATE( active_pts_odd(MAXVAL(nxyz_out_new)+1)  )
	ALLOCATE( active_pts_even(MAXVAL(nxyz_out_new)+1) )
	ALLOCATE( j_above(MAXVAL(nxyz_out_new)+1) )
	ALLOCATE( new_pts(MAXVAL(nxyz_out_new)+1)       )
	ALLOCATE( delete_pts(MAXVAL(nxyz_out_new)+1)       )
	ALLOCATE( line_u(1:n_var ,0:MAXVAL(nxyz_out_new))  ) !we use ignore_u for this second part
	ALLOCATE( line_flags(0:MAXVAL(nxyz_out_new))    )
    ALLOCATE( line_coords(0:MAXVAL(nxyz_out_new),dim)   )
	ALLOCATE( line_ptrs(0:MAXVAL(nxyz_out_new))     )


  END IF !IF( j_out /= j_out_new )

  ! here j_out_new is the max significant level
  ! j_out is one more than the max significant level (if  <= j_mx) to allow for possible
  ! addition of adjacent points in then level above.

  PRINT *,' Begin db_adj, j_out_new = ', j_out_new


  !****************************************************************************
  ! Find new adjacent points
  !****************************************************************************
!PRINT *, 'db(0,124,124) ', db(0,124,124)%flags, lvl_db( 0),lvl_db( 124),lvl_db( 124)

  !
  ! We loop from j_out to j_mn-1 
  ! or in the case of conservative we loop over level j_out also
  !
  line_u = 0.0_pr ! make sure the line array is zeroed out
  line_flags = 0  ! make sure the line flags array is zeroed out

  DO j=MIN(j_mx_db,j_out+adj_type(0)),j_mn-1,-1 ! j_lev -> j_mn (TESTING IF THIS SHOULD BE  J_MN TRY IT DG MARCH30 ..)

	DO idim =1,dim
		CALL get_active_lines_indx( idim, j, 1, GET_ALL_DERIV_LEV, &
           plocal_lines_indx_backend, plocal_numlines_indx_backend,&
		   plocal_lines_indx_current ) !

        !DO ln = 1,nlines
		

			! get one line at a time
			! line is the actual data line, active points are the
			! indices of the active collocation points 
			! we get all variable from 1:Nparam_var and then use the 
			! logical array:  l_n_var_adapt_index(: ) to decide which
			! variables we will adapt to.

!We should be using Ignore_u, make sure dimensions of line_u are correct!! ??
!ln = 1 !TEST only
		DO WHILE (  get_active_line( idim, j, j_out  ,MAXVAL(nxyz_out_new),  &
                1, n_var, &
                line_u, line_flags, line_ptrs, line_coords, odim, &
                active_pts_odd, n_active_pts_odd, &
                active_pts_even, n_active_pts_even ,  &
                j_above, n_j_above, delete_pts, n_delete_pts, GET_DEL_INLST ,GET_ABOVE_PTS ,&
				LINE_EVEN_ODD , NO_GHOST_PTS,GET_ALL_PTS, SORT_PTS_FALSE, &
                plocal_lines_indx_backend, plocal_numlines_indx_backend,&
		        plocal_lines_indx_current) )  

		    n_new_pts = 0 ! zero out count of new 


		    !
			! see if there are active pts to process
			!
		    IF(n_active_pts_even + n_active_pts_odd <= 0) THEN
				 CYCLE !Skip rest of loop body if we have no pts to process
            END IF

            !
			!get the current active line's face coordinate
			! return the coordinates of the the dimensions other than the current idim direction
			!odim = get_active_line_face_coord()

			!ixyz_db(:) = lines_indx(ln,:)
			!ixyz_loc(:) = ixyz_db(:) /db_scale

			! we do this for the even points if atleast one of the other idim directions
			! is at level j (ie all the points are associated with this level so we need
			! to do adjacent zone for them at this level.
			!
			!iidim = MOD(idim,3)+1
			!iiidim = MOD(idim+1,3)+1




			!ONLY LOOK FOR ADJACENT AROUND POINTS OF LEVEL >=  J_MN -1 
!?? what does this do??

			! Find adjacent points EVEN
!			IF( adj_type(0) == 1 .OR. (lvl_db(ixyz_db(iidim)) >= j .OR. &
!                lvl_db(ixyz_db(iiidim)) >= j) ) THEN
			IF( adj_type(0) == 1 .OR. MAXVAL(lvl_db(odim(:))) >= j ) THEN

              
				DO i=1,n_active_pts_even
					indx_line = active_pts_even(i)
					ixyz_db   = line_coords(indx_line,:)
					ixyz_loc  = ixyz_db / db_scale 

					!ixyz_loc(idim) = indx_line
					!ixyz_db(idim)  = ixyz_loc(idim) * db_scale
                 
				 
					!Sanity check
					IF(  BTEST(line_flags(indx_line), GHOST ) ) THEN
					   PRINT *,'ERROR Ghost in adj loop'
					   stop
					END IF

					!
					! Find adjacent points
					!

					! we process this point if it is active or in the case of 
					! conservative adjacent zone (adj_type(0) == 1) we process this point if
					! active or it was flaged as an adjacent zone coeff in any previous
					! direction sweep.


					IF( BTEST(line_flags(indx_line), SIG_FLAG ) .OR. &
						( adj_type(0) == 1 .AND. idim  > 1 .AND. &
						IBITS(line_flags(indx_line),ADJACENT_X,ADJACENT_X+idim-2 )/=0) &
						) THEN

!IF(  .NOT. BTEST(line_flags(indx_line), SIG_FLAG )) THEN
!  PRINT *,'In adj', char_flags(line_flags(indx_line))
!  PRINT *,''
!END IF

						!testingDO j_adj=1,MIN(1,j_mx_db-j) !CURRENT + FINER LEVEL
						DO j_adj=0,0 !CURRENT + FINER LEVEL
							ij_step    = MIN(MAX(1,2**(j_out-(j)-j_adj)),2**(j_out-1))
							!MAX ensures it does not go above j_out, index is integer)
							i_l = ixyz_loc(idim)-ij_adj(j_adj)*ij_step
							i_h = ixyz_loc(idim)+ij_adj(j_adj)*ij_step
							! account for boundary in periodic or non-periodic case
							i_l = (1-prd(idim))*MAX(0,i_l                ) + prd(idim)*i_l
							i_h = (1-prd(idim))*MIN(  i_h, nxyz_out_new(idim)) + prd(idim)*i_h

							DO ipm = i_l, i_h, ij_step
								ixp =(1-prd(idim))*ipm + prd(idim)* &
								MOD(ipm+9*nxyz_out_new(idim),nxyz_out_new(idim))



								IF( ixp /= ixyz_loc(idim) ) THEN

									ixyztmp=ixyz_db
									ixyztmp(idim) = ixp*db_scale 

									!
									! if this point was not active (or already added as an adjacent)
									! then put it on new points list.
									! check flag bits ACTIVE, ADJACENT_X,ADJACENT_Y,ADJACENT_Z
									! we check the delete bit to see if this point is still physically in the db

									IF(  IBITS(line_flags(ixp), ACTIVE, SIG_FLAG+1) == 0 .AND. &
										.NOT. BTEST(line_flags(ixp), DELETE ) ) THEN
                             
										n_new_pts = n_new_pts +1
										new_pts(n_new_pts) = ixp
										n_sig = n_sig +1 !we increment counter!test
										db_nwlt_new = db_nwlt_new +1!test

										!
										! keep track whether we add any points on the j_out level
										!
										j_out_new = MAX(j_out_new,MAXVAL(lvl_db(ixyztmp(1:dim))) )
										
									END IF

									line_flags(ixp)= IBSET( line_flags(ixp), ADJACENT_X+idim-1 )

									IF(BTEST(line_flags(ixp), DELETE ) ) THEN
										n_del = n_del -1!test
										n_sig = n_sig +1 !we increment counter!test
										db_nwlt_new = db_nwlt_new +1!test
									END IF

									line_flags(ixp) = IBCLR( line_flags(ixp) , DELETE)
								END IF
							END DO
						END DO ! DO j_adj=0,1 !CURRENT + FINER LEVEL
					END IF !end find adjacent

				END DO !DO i=1,n_active_pts_even
			END IF ! Find adjacent points EVEN

			! Find adjacent points odd
			DO i=1,n_active_pts_odd
				indx_line = active_pts_odd(i)
				ixyz_db   = line_coords(indx_line,:)
				ixyz_loc  = ixyz_db / db_scale 

				!ixyz_loc(idim) = indx_line
				!ixyz_db(idim)  = ixyz_loc(idim) * db_scale 


				!sanity check
				IF(  BTEST(line_flags(indx_line), GHOST ) ) THEN
					PRINT *,'ERROR Ghost in adj loop'
				END IF

				!
				! Find adjacent points
				!

				! we process this point if it is active or in the case of 
				! conservative adjacent zone (adj_type(0) == 1)we process this point if
				! active or it was flaged as an adjacent zone coeff in any previous
				! direction sweep.
				!
				IF(  BTEST(line_flags(indx_line), SIG_FLAG ) .OR. &
					( adj_type(0) == 1 .AND. idim  > 1 .AND. &
!					( SUM(adj_type(0:MIN(1,j_mx_db-j))) >= 1 .AND. idim  > 1 .AND. &
					IBITS(line_flags(indx_line),ADJACENT_X,ADJACENT_X+idim-2 )/=0) &
					) THEN

!IF(  .NOT. BTEST(line_flags(indx_line), SIG_FLAG )) THEN
!  PRINT *,'In adj',  char_flags(line_flags(indx_line))
!  PRINT *,''
  !PAUSE
!END IF

					!TESTING DO j_adj=1,MIN(1,j_mx_db-j) !CURRENT + FINER LEVEL
					DO j_adj=0,MIN(1,j_mx_db-j) !CURRENT + FINER LEVEL

						ij_step    = MIN(MAX(1,2**(j_out-(j)-j_adj)),2**(j_out-1))

						!MAX ensures it does not go above j_out, index is integer)

						i_l = ixyz_loc(idim)-ij_adj(j_adj)*ij_step
						i_h = ixyz_loc(idim)+ij_adj(j_adj)*ij_step
						! account for boundary in periodic or non-periodic case
						i_l = (1-prd(idim))*MAX(0,i_l                ) + prd(idim)*i_l
						i_h = (1-prd(idim))*MIN(  i_h, nxyz_out_new(idim)) + prd(idim)*i_h

						DO ipm = i_l, i_h, ij_step
							ixp =(1-prd(idim))*ipm + prd(idim)* &
                            MOD(ipm+9*nxyz_out_new(idim),nxyz_out_new(idim))

							IF( ixp /= ixyz_loc(idim) ) THEN


								ixyztmp=ixyz_db
								ixyztmp(idim) = ixp*db_scale

								!
								! if this point was not active (or already added as an adjacent)
								! then put it on new points list.
								! check flag bits ACTIVE, ADJACENT_X,ADJACENT_Y,ADJACENT_Z
								! we check the delete bit to see if this point is still physically in the db

								IF(  IBITS(line_flags(ixp), ACTIVE, SIG_FLAG+1) == 0 .AND. &
									.NOT. BTEST(line_flags(ixp), DELETE ) ) THEN  

                          
									n_new_pts = n_new_pts +1
									new_pts(n_new_pts) = ixp
									n_sig = n_sig +1 !we increment counter !test
									db_nwlt_new = db_nwlt_new +1!test


									!
									! keep track whether we add any points on the j_out level
									!
									!
									j_out_new = MAX(j_out_new,MAXVAL(lvl_db(ixyztmp(1:dim))) )

								END IF

								line_flags(ixp)= IBSET( line_flags(ixp), ADJACENT_X+idim-1 )

								IF(BTEST(line_flags(ixp), DELETE ) ) THEN
									n_del = n_del -1!test
									n_sig = n_sig +1 !we increment counter!test
									db_nwlt_new = db_nwlt_new +1!test
								END IF

                      
								line_flags(ixp) = IBCLR( line_flags(ixp) , DELETE)
								n_sig = n_sig +1 !we increment counter
								db_nwlt_new = db_nwlt_new +1

							END IF
						END DO
					END DO ! DO j_adj=0,1 !CURRENT + FINER LEVEL
				END IF             
			END DO ! DO i=1,n_active_pts_odd






           
           !
           ! put line back and update new adjacent points
           !
           ! NOTE: we call  put_active_line() with mn_var=mx_var=1 because we have not
           ! changed the actual field values.
           ! 
!NOTE can add check to see if any points were modified, if not we do not need to cal put_active_line()

           CALL put_active_line( idim, j, j_out, MAXVAL(nxyz_out_new),  1, n_var , &
                line_u, IGNORE_U, line_flags, line_ptrs, &
                active_pts_odd, n_active_pts_odd, &
                active_pts_even, n_active_pts_even, &
                j_above, n_j_above, new_pts, n_new_pts, delete_pts, n_delete_pts, 0 )   

			!ln = ln +1 !TEST only

        END DO !DO ln = 1,nlines
	END DO !DO idim =1,dim
  END DO !DO j=j_out-1,j_mn,-1 ! j_lev -> j_mn

  PRINT *,'leaving db_adj  n_sig (+adj) = ', n_sig

! deallocate the arrays used in db access routines
  DEALLOCATE( active_pts_odd  )
  DEALLOCATE( active_pts_even )
  DEALLOCATE( j_above         )
  DEALLOCATE( new_pts         )
  DEALLOCATE( delete_pts      )
  DEALLOCATE( line_u          )
  DEALLOCATE( line_flags      )
  DEALLOCATE( line_coords     )
  DEALLOCATE( line_ptrs       )
  CALL deallocate_active_lines_indx(plocal_lines_indx_backend)



  j_out = MAX(j_out,j_mn) ! make sure that j_out is not less then j_mn  
  !update nxyz_out to the level of j_out
  nxyz_out(1:2) = mxyz(1:2)*2**(j_out-1)
  nxyz_out(3) = mxyz(3)*2**(jd*(j_out-1))

  



  !
  ! End find adjacent wlts 
  !


  !
  ! Adding extra points around boundary if we are at a non-periodic boundary
  ! and BNDzone = TRUE in the input file

  IF(BNDzone) THEN
	PRINT * ,' Error, BNDzone == TRUE, this is not currently supported in lines_db database version, Exiting...'
  END IF
  !
!!$  IF(BNDzone) THEN !--- Adding boundary points, modify this subroutine later
!!$
!!$  IF( dim == 3 ) THEN
!!$     PRINT *, ' Code to do BNDzone is not updated for 3D yet. Dan G. 5/22/03 Exiting ...'
!!$  ENDIF
!!$
!!$     DO j = 1, j_out
!!$        IF(prd(1) == 0) THEN
!!$           DO ly = 0, nxyz_out(2) - prd(2), 2**(j_out-j)
!!$              ! left boundary
!!$              check = .FALSE.
!!$              DO lx = 0, MIN(n_prdct*2**(j_out-j),nxyz_out(1)), 2**(j_out-j)
!!$                 IF(i_c(lx,ly,iz)) check=.TRUE.
!!$              END DO
!!$              IF(check) THEN
!!$                 DO lx = 0, MIN(n_prdct*2**(j_out-j),nxyz_out(1)), 2**(j_out-j)
!!$                    i_c(lx,ly,iz) =.TRUE.
!!$              END DO
!!$              END IF
!!$              ! right boundary
!!$              check = .FALSE.
!!$              DO lx = nxyz_out(1), MAX(nxyz_out(1)-n_prdct*2**(j_out-j),0),-2**(j_out-j)
!!$                 IF(i_c(lx,ly,iz)) check=.TRUE.
!!$              END DO
!!$              IF(check) THEN
!!$                 DO lx = nxyz_out(1), MAX(nxyz_out(1)-n_prdct*2**(j_out-j),0),-2**(j_out-j)
!!$                    i_c(lx,ly,iz) =.TRUE.
!!$                 END DO
!!$              END IF
!!$           END DO
!!$        END IF
!!$        IF(prd(2) == 0) THEN
!!$           DO lx = 0, nxyz_out(1) - prd(1), 2**(j_out-j)
!!$              ! bottom boundary
!!$              check = .FALSE.
!!$              DO ly = 0, MIN(n_prdct*2**(j_out-j),nxyz_out(2)), 2**(j_out-j)
!!$                 IF(i_c(lx,ly,iz)) check=.TRUE.
!!$              END DO
!!$              IF(check) THEN
!!$                 DO ly = 0, MIN(n_prdct*2**(j_out-j),nxyz_out(2)), 2**(j_out-j)
!!$                    i_c(lx,ly,iz) =.TRUE.
!!$                 END DO
!!$              END IF
!!$              ! upper boundary
!!$              check = .FALSE.
!!$              DO ly = nxyz_out(2), MAX(nxyz_out(2)-n_prdct*2**(j_out-j),0),-2**(j_out-j)
!!$                 IF(i_c(lx,ly,iz)) check=.TRUE.
!!$              END DO
!!$              IF(check) THEN
!!$                 DO ly = nxyz_out(2), MAX(nxyz_out(2)-n_prdct*2**(j_out-j),0),-2**(j_out-j)
!!$                    i_c(lx,ly,iz) =.TRUE.
!!$                 END DO
!!$              END IF
!!$           END DO
!!$        END IF
!!$     END DO
!!$  END IF




  
  db_nwlt = n_sig
  PRINT *, 'leaving db_adjacent_wlt() db_nwlt ',db_nwlt
  PRINT *, 'leaving db_adjacent_wlt() n_del = ', n_del
  PRINT *, 'leaving db_adjacent_wlt() j_out = ', j_out

END SUBROUTINE db_adjacent_wlt_OLD
#endif


!
! type == 0 - Do reconstruction check on interior and Bnd pnts
! type == 1 - Do reconstruction check on interior pnts only
!
SUBROUTINE  db_reconstr_check( max_nxyz, j_out,trnsf_type, new_pt_type )

  IMPLICIT NONE
  INTEGER, INTENT(IN) ::  max_nxyz
  INTEGER, INTENT(IN) :: trnsf_type
  INTEGER, INTENT(IN) :: new_pt_type !GHOST_PT or ACTIVE_PT
  INTEGER, INTENT(IN) :: j_out

  INTEGER :: ix,ix_even,ix_odd,ixp,ixpm,ix_l,ix_h, ix_j
  INTEGER :: indx_line !index into current line being processed
  INTEGER :: n_active_pts_odd,active_pts_odd(max_nxyz+1)
  INTEGER :: n_active_pts_even,active_pts_even(max_nxyz+1)
  INTEGER :: n_j_above,j_above(max_nxyz+1)
  INTEGER :: new_pts(max_nxyz+1), n_new_pts
  INTEGER :: delete_pts(max_nxyz+1), n_delete_pts
  INTEGER :: iy, iz, j
  INTEGER :: nxyz_out(1:3) !should use global one
  INTEGER :: idim, i,ii
  INTEGER :: active_line_flag !set whether or not to get ghost points based on new_pt_type
  INTEGER :: get_del_flag     !set whether or not to get delete points based on new_pt_type
  REAL (4) :: t0, t1
  REAL(pr)             :: line_u(1:1,0:max_nxyz) !change so we don't get any u data!!!
  INTEGER(KIND_INT_FLAGS):: line_flags(0:max_nxyz)
  INTEGER              :: line_coords(0:max_nxyz,dim)  ! coordinated of returned points in terms of j_mx
  INTEGER :: odim(1:dim-1) ! the face coordinates of the dimensions other then idim 

  TYPE (grid_pt_ptr)   :: line_ptrs(0:max_nxyz)
  INTEGER :: dbscale, ixyz_loc(3), ixyz_db(3), ixyztmp(3)
  INTEGER :: plocal_numlines_indx_backend ! number of active lines indexed in lines_indx_backend
  TYPE (element_of_face_ptr) , DIMENSION(:), POINTER :: plocal_lines_indx_backend !  list of active line faces
  INTEGER :: plocal_lines_indx_current ! current active line to be returned 


  nxyz_out(1:2) = mxyz(1:2)*2**(j_out-1)
  nxyz_out(3) = mxyz(3)*2**(jd*(j_out-1))

  dbscale = (2**(j_mx_db-j_out))
  
  !We do not use u_line data in this routine..              line_u = 0.0_pr ! make sure the line array is zeroed out
  line_flags = 0  ! make sure the line flags array is zeroed out

  IF( new_pt_type == GHOST ) THEN
     WRITE(6,'("Reconstruction check on Ghost points")')
     WRITE(6,'("Entering db_reconstr_check() db_ghost = ", I10.10, " j_out = ", I3.3 ," trnsf_type = ", I1.1 )') &
		 db_ghost,j_out,trnsf_type
	 active_line_flag = GET_GHOST_PTS ! we need to get the ghost points
  ELSE !called on ACTIVE points
     WRITE(6,'("Entering db_reconstr_check() db_nwlt = ", I10.10, " j_out = ", I3.3 ," trnsf_type = ", I1.1 )') &
		 db_nwlt,j_out,trnsf_type
	 active_line_flag = NO_GHOST_PTS ! we do not need to get the ghost points
  END IF
  ! check that we were passed a legal value for new_pt_type
  IF( .NOT. (new_pt_type == ACTIVE .OR. new_pt_type == GHOST ) ) THEN
	PRINT *,'Error in db_reconstr_check, Illegal value passed for new_pt_type.'
	PRINT *,'Legal value for new_pt_type are ACTIVE or HOST.'
	PRINT *,'new_pt_type = ',new_pt_type
	PRINT *,'Exiting...'
	STOP
  ENDIF

  ! we need to get all the points in the list (including delete) because currently
  ! that is the only way to ensure that if a delete point is modified it will be updated in the db in put_active.
  ! I might make another list that is just to pass back to put_active_pts() .
!USE DELETE LIST IN PUT_ACTIVE_LINE!!!  !
  get_del_flag = GET_DEL_INLST

  n_delete_pts = 0
!  line_u = 0.0_pr ! make sure the line array is zeroed out
  line_flags = 0  ! make sure the line flags array is zeroed out

  DO j = j_out-1, 1, -1
	!
	!*********** transform in idim -direction *********************
	!       
	DO idim = 1,dim


		! Get index of lines that we need to transform for this direction.
		! Return lines and nlines as the number of active lines
		! If lines is not allocated or it is not large enough
		! it will be (re)allocated.
		! This routine puts the result in the global array: lines_indx
		CALL get_active_lines_indx( idim, j+1, 1, GET_ALL_DERIV_LEV, &
           plocal_lines_indx_backend, plocal_numlines_indx_backend,&
		   plocal_lines_indx_current ) !4/18/05 DG LOOKING AT, This is j+1 to match call to get_active_line() 

		!
		! Do predict and then update for each active line.
		! PARALLIZE this loop! such that nlines is broken accross 
		! N processors
		!DO ln = 1,nlines
			
        DO WHILE ( get_active_line_flags( idim, j+1, j_out, dbscale, max_nxyz, 1, 1, &
                line_u, line_flags, line_ptrs, line_coords, odim, &
                active_pts_odd, n_active_pts_odd, &
                active_pts_even, n_active_pts_even ,&
                j_above, n_j_above, delete_pts, n_delete_pts, get_del_flag ,NO_GET_J_ABOVE , &
				LINE_EVEN_ODD, active_line_flag, &
                plocal_lines_indx_backend, plocal_numlines_indx_backend,&
		        plocal_lines_indx_current) )

             !WRITE(*,'("DEBUG:recons j,n_active_pts_odd",2(i4,1x))') j,n_active_pts_odd
			n_new_pts = 0 ! zero out count of new 
			DO i=1,n_active_pts_odd

				indx_line = active_pts_odd(i)

			     !   ixyz_loc(idim) = indx_line !TEST
				!ixyz_db(idim)  = ixyz_loc(idim) * dbscale !TEST
                 !WRITE(*,'("DEBUG:recons @ ",3(i4,1x),10A)') ixyz_db,char_flags(line_flags(indx_line))

				! We need to get the points with DELETE flag marked because ghost points can have delete 
				!flag marked also. This is done so that there value will be left intact to be used in the inverse
				! transofrm back to the new grid. Then if they are not GHOST points the DELETE points
				! get deleted in update_grid
				IF( .NOT. BTEST(line_flags(indx_line), DELETE ) .OR. BTEST(line_flags(indx_line), GHOST ) )THEN 

                    !WRITE(*,'("DEBUG:recons acton@ ",3(i4,1x))') ixyz_db

					ix_l=ix_lh_db(indx_line*dbscale, idim,j,trnsf_type)%ix_l ! # support points lower
					ix_h=ix_lh_db(indx_line*dbscale, idim,j,trnsf_type)%ix_h ! # support points higher 
					ix_j=ix_lh_db(indx_line*dbscale, idim,j,trnsf_type)%ix_j ! indx consecutive from 1 to max pts on this level 

					DO ixpm = ix_l, ix_h

						ii = (ix_j+ixpm)*2**(j_out-j)
						ii = (1-prd(idim))*ii+prd(idim)*MOD(ii+9*nxyz_out(idim),nxyz_out(idim))
 
						!ixyztmp=ixyz_db !TEST
						!ixyztmp(idim) = ii*dbscale  !TEST

						!
						! if this point was not active (or already added as an adjacent)
						! then put it on new points list.
						! check flag bits ACTIVE, ADJACENT_X,ADJACENT_Y,ADJACENT_Z
						! we check the delete bit to see if this point is still physically in the db
						IF(  IBITS(line_flags(ii), ACTIVE, SIG_FLAG+1) == 0 .AND. &
						   .NOT. ( new_pt_type == GHOST .AND. BTEST(line_flags(ii), GHOST ) )) THEN

							! set new_pt_type and unset delete
							! If new_pt_type == GHOST we only want to set it if this point is not active
							IF( BTEST(line_flags(ii), DELETE ) ) THEN

								! add point of type new_pt_type (ACTIVE or GHOST )
								line_flags(ii)= IBSET( line_flags(ii),  new_pt_type )

								!
								! We do not want to clear DELETE if this is a ghost point.
								! Ghost points can have delete flag true because they were deleted as active or adjacent
								! but have now become ghost. THis is necessary so these points will get
								! used in the inverse transform done between  adding ghost points and
								! the call to update_grid()
								!
								IF( new_pt_type /= GHOST ) THEN
									line_flags(ii) = IBCLR( line_flags(ii) , DELETE)
								END IF

								IF( new_pt_type == GHOST ) THEN
									db_ghost = db_ghost +1
									!WRITE(*,'("DEBUG:reconst(recup ghost) @ ",3(i4,1x))') ixyztmp
								ELSE
									db_nwlt = db_nwlt +1
								ENDIF
								

							ELSE !add new point to DB
						 
								n_new_pts = n_new_pts +1

								new_pts(n_new_pts) = ii

								IF( new_pt_type == GHOST ) THEN
									db_ghost = db_ghost +1
									!WRITE(*,'("DEBUG:reconst(new ghost) @ ",3(i4,1x))') ixyztmp
								ELSE
									db_nwlt = db_nwlt +1
								ENDIF

								! add point of type new_pt_type (ACTIVE or GHOST )
								line_flags(ii)= IBSET( line_flags(ii),  new_pt_type )
							ENDIF
						
						END IF
				
					END DO
				END IF !  .NOT. BTEST(line_flags(indx_line), DELETE ) ) 
			END DO
          
			!
			! Put the transform result back into the data base
			! 
			CALL put_active_line( idim, j, j_out, MAXVAL(nxyz_out), 1,1, &
                line_u, IGNORE_U, line_flags, line_ptrs, &
                active_pts_odd, n_active_pts_odd, &
                active_pts_even, n_active_pts_even, &
                j_above, n_j_above, new_pts, n_new_pts, delete_pts, n_delete_pts, 0 )   

			!ln = ln +1 !TEST only

		END DO
	END DO
  END DO

  CALL deallocate_active_lines_indx(plocal_lines_indx_backend)

  IF( new_pt_type == GHOST ) THEN
     WRITE(6,'("Leaving  db_reconstr_check() db_ghost = ", I10.10, " j_out = ", I3.3 )') db_ghost,j_out
  ELSE
     WRITE(6,'("Leaving  db_reconstr_check() db_nwlt = ", I10.10, " j_out = ", I3.3 )') db_nwlt,j_out
  ENDIF
END SUBROUTINE  db_reconstr_check 



!***********************************************************
!* Do grid adaptation 
! 
! arguments
! flag  -  0 = in startup code, 1 in main loop
!***********************************************************
SUBROUTINE adapt_grid( flag , leps , j_lev_old, j_out, j_mn, j_mx, nxyz_out, ij_adj,&
     adj_type, scl,new_grid )
  USE debug_vars
  IMPLICIT NONE
  INTEGER , INTENT (IN)     :: flag , j_mn, j_mx
  LOGICAL , INTENT (INOUT) :: new_grid
  INTEGER , INTENT (INOUT ) ::  j_out , j_lev_old
  INTEGER , INTENT (INOUT ), DIMENSION(3) :: nxyz_out
  INTEGER, DIMENSION(-1:1) , INTENT (IN) :: ij_adj, adj_type
  REAL (pr) , INTENT (IN)   :: leps 
  REAL (pr) , INTENT (IN)   :: scl(1:n_var)
  INTEGER :: ie, trnsf_type, lflag

  INTEGER :: j_tmp, nxyz_tmp(3)
  REAL t1, t2 !TEST
! Code for running OpenMP start with !$ at the beginning of the line.
! These lines should only be executed when running in OpenMP mode.


    INTEGER IAM_OMP, N_Threads_OMP

    IAM_OMP = 0       ! default no OMP
	N_Threads_OMP = 1 ! default no OMP
!!!!!!!!!
        
        
        CALL timer_start(3)
        
        
  IF( flag_timing ) THEN
     CALL CPU_TIME (t2)
  END IF

  IF( flag == -1 ) THEN
	!startup = .TRUE.
	lflag = 1 !we have to adapt the same as inthe time integration look if we are doing restart 
  ELSE 
    !startup = .FALSE.
    lflag = flag 
  END IF


  !
  ! Here we will set j_out to one level higher then j_in (if we can)
  ! Then at the end of this routine we will reset j_out to the
  ! correct new j_lev value
  !
  j_lev_old =  j_out ! save old value of j_lev to be passed back out
  j_tmp = j_out ! save original value of j_lev as passed into this routine
  nxyz_tmp = nxyz



  !***********************************************************
  !* Start grid adaptation criteria
  !***********************************************************
  ! j_out is updated to reflect any new _pts added
  CALL db_adjacent_wlt ( leps, scl, &
	n_var_adapt(:,lflag), n_var_index(:),  ij_adj, adj_type, &
	j_tmp, j_mn, j_mx, nxyz_tmp)

  !***********************************************************
  !* End grid adaptation criteria
  !***********************************************************

  WRITE (*,'("add_nodes() not implemented in db_lines yet... ")')
  IF( debug_level >= 1 ) THEN
     WRITE (*,'(" ")')
     WRITE (*,'("=======================================================")')
     WRITE (*,'("after add_nodes, # wlts    = ", I12)') db_nwlt
     WRITE (*,'("after add_nodes, j_lev_new = ", I6)') j_tmp
     WRITE (*,'("=======================================================")')
     WRITE (*,'(" ")')
  END IF

  IF( BNDzone ) WRITE (*,'("bnd_zone() not implemented in db_lines yet... ")')

  IF( BNDzone .AND. debug_level >= 1 ) THEN
     WRITE (*,'(" ")')
     WRITE (*,'("=======================================================")')
     WRITE (*,'("after bnd_zone, # wlts    = ", I12)') db_nwlt
     WRITE (*,'("after bnd_zone, j_lev_new = ", I6)') j_tmp
     WRITE (*,'("=======================================================")')
     WRITE (*,'(" ")')
  END IF

  !***********************************************************
  !* Special Grid adaptation criteria within zone
  !***********************************************************
  !!$
  PRINT *,' zone_wlt() not implemented DG. '
  !Oleg, when implement, make sure it is the same way as in corrected version.
  !!$  CALL zone_wlt (i_c,  nxyz_a, j_lev_a, j_mn)
  !!$   IF (wlog) WRITE (6,'("After zone_wlt , ", I12," active wlts" )') COUNT(i_c)
  IF( debug_level >= 1 ) THEN
     WRITE (*,'(" ")')
     WRITE (*,'("=======================================================")')
     WRITE (*,'("after zone_wlt, # wlts    = ", I12)') db_nwlt
     WRITE (*,'("after zone_wlt, j_lev_new = ", I6)') j_tmp
     WRITE (*,'("=======================================================")')
     WRITE (*,'(" ")')
  END IF

  IF( flag_timing ) THEN
     CALL CPU_TIME (t1)
     WRITE (6,'("CPU TIME (after zone_wlt) = ", es12.5)') t1 - t2
     CALL CPU_TIME (t2)
  END IF

  !
  !------------- Performing a reconstruction check  ----------
  !
  DO trnsf_type =0,1
    CALL db_reconstr_check(   MAXVAL(mxyz)*2**(j_tmp-1) , j_tmp,trnsf_type , ACTIVE )
    !IF (wlog) WRITE (6,'("After db_reconstr_check , ", I12," active wlts" )') 
	 
  END DO

  IF( debug_level >= 1 ) THEN
     WRITE (*,'(" ")')
     WRITE (*,'("=======================================================")')
     WRITE (*,'("after reconstr_check, # wlts    = ", I12)') db_nwlt
     WRITE (*,'("after reconstr_check, j_lev_new = ", I6)') j_tmp
     WRITE (*,'("=======================================================")')
     WRITE (*,'(" ")')
  END IF

  IF( flag_timing ) THEN
     CALL CPU_TIME (t1)
     WRITE (6,'("CPU TIME (reconstr_check) = ", es12.5)') t1 - t2
     CALL CPU_TIME (t2)
  END IF

  !
  !------------- Find  new ghost pts ----------
  !
  CALL set_ghost_pts_db( j_tmp,j_mn, nxyz_tmp, MAXVAL(mxyz)*2**(j_tmp-1))

  IF( debug_level >= 1 ) THEN
     WRITE (*,'(" ")')
     WRITE (*,'("=======================================================")')
     WRITE (*,'("after set_ghost_pts, # wlts+ghost = ", I12)') db_nwlt+db_ghost
     WRITE (*,'("after set_ghost_pts, j_lev_new    = ", I6)') j_tmp
     WRITE (*,'("=======================================================")')
     WRITE (*,'(" ")')
  END IF

  IF( flag_timing ) THEN
     CALL CPU_TIME (t1)
     WRITE (6,'("CPU TIME (bfr reconstr_check ghost) = ", es12.5)') t1 - t2
     CALL CPU_TIME (t2)
  END IF


  !
  !------------- Performing a recontruction check for ghost points ----------
  !
  !DO trnsf_type =0,1
  trnsf_type = 0 !only do trnsf_type 0 reconstruction check on ghost points
  CALL db_reconstr_check(   MAXVAL(mxyz)*2**(j_tmp-1) , j_tmp,trnsf_type , GHOST )
  !END DO

  IF( debug_level >= 1 ) THEN
     WRITE (*,'(" ")')
     WRITE (*,'("=======================================================")')
     WRITE (*,'("after reconstr_check (on ghost pts), # wlts    = ", I12)') db_nwlt
     WRITE (*,'("after reconstr_check,(on ghost pts) j_lev_new  = ", I6)') j_tmp
     WRITE (*,'("=======================================================")')
     WRITE (*,'(" ")')
  END IF
 
  IF( flag_timing ) THEN
     CALL CPU_TIME (t1)
     WRITE (6,'("CPU TIME (reconstr_check ghost) = ", es12.5)') t1 - t2
     CALL CPU_TIME (t2)
  END IF

  !
  !------------- Do inverse wlt trns ----------
  ! 
  ! some points are marked active and delete.
  ! the inverse trns uses these points.
  ! 
  ! The variables in the db are those marked (n_var_adapt(:,0) .OR. n_var_interpolate(:,0))
  ! n_var_interpolate variables are first in db structure.
  ! The inverse transform is done only on these n_var_interpolate(:,0) points  
  !
!$OMP PARALLEL DEFAULT(SHARED )
  CALL c_wlt_trns_db(nwlt, n_var , 1 ,COUNT(n_var_interpolate(:,lflag)) ,  MAXVAL(mxyz)*2**(j_tmp-1) ,&
        j_tmp, j_mx, WLT_TRNS_INV )

  PRINT *,'after c_wlt_trns_db(-1) j_tmp, j_mn, nxyz_tmp',j_tmp, j_mn, nxyz_tmp
!$OMP END PARALLEL

  !
  !------------- Finish updating the grid. ----------
  !
  ! This routine deletes pts marked to be deleted. And updates the
  ! current max level (j_out), It is called with the old max level (before significant, adjacent and recontruction).
  ! so it accesses all points that need to be deleted..
  !
  CALL db_update_grid(j_tmp,MAX(j_out,j_tmp), j_mn, MAXVAL(mxyz)*2**(MAX(j_out,j_tmp)-1) )

  ! Now that grid is updated the new max level is now j_tmp
  j_out = j_tmp
  nwlt = db_nwlt !assign current number of points in data base to the global nwlt
  j_lev = j_out
  ! CLEAN UP J_out, j_lev references globally

  ! 
  ! Reallocate u() flat field array if size of the number of adapted grid points has changed
  !
  IF( nwlt /= nwlt_old ) THEN
	DEALLOCATE(u)
	ALLOCATE ( u(1:nwlt,1:n_var) ); IF( debug_zero_allocated_mem) u = 0.0
  END IF
  IF(n_var_exact > 0) THEN
	IF( ALLOCATED(u_ex) ) DEALLOCATE (u_ex)  
	ALLOCATE   (u_ex(1:nwlt,1:n_var_exact)) 
	IF( debug_zero_allocated_mem) u_ex = 0.0_pr
  END IF

 !
 ! Update indices
 !
 WRITE (*,'(A, I3, I3 )' ) 'calling indices_db(), j_lev_old, j_lev ',j_lev_old, j_lev
 CALL indices_db (nwlt_old, nwlt, nwltj_old, nwltj, new_grid, j_lev_old, j_lev, j_mn, j_mx, 1)


!$  IAM_OMP = OMP_GET_THREAD_NUM()
!$  N_Threads_OMP = OMP_GET_NUM_THREADS()
!$  PRINT *,'In adapt_grid(), before calling update_u_from_db(), IAM_OMP, N_Threads_OMP ', IAM_OMP, N_Threads_OMP


!$  IF( IAM_OMP == 0 ) THEN
 ! 
 ! restore values into u from the db for variable we interpolated to new grid...)
 ! 
 CALL update_u_from_db(  u , nwlt ,n_var  , 1, COUNT(n_var_interpolate(:,lflag)) , 1, j_mx, j_lev, MAXVAL(nxyz) )
!$ END IF
 nwlt_p_ghost = db_ghost + db_nwlt !store total # total active wavelets plus ghost points

  IF( flag_timing ) THEN
     CALL CPU_TIME (t1)
     WRITE (6,'("CPU TIME (end adapt grid) = ", es12.5)') t1 - t2
  END IF
  
  
  CALL timer_stop(3)
  
  
END SUBROUTINE adapt_grid

!
! 
!**************************************************************************
!
! Find spacial derivatives
!
!
! Arguments
!u, du, d2u, j_in, nlocal, meth, meth1, 
! ID Sets which derivative is done 
!    10 - do first derivative
!    11 - do first and secont derivative
!    01 - do second derivative
!    if ID > 0 then the second derivative is central difference D_central
!    if ID < 0 then the second derivative is D_backward_bias followed by D_forward_bias
! meth  - order of the method, high low,
!
!  dimensions of u are u(1:nlocal,1:ie) 
!  derivative is done on  u(1:nlocal,mn_var:mx_var)
SUBROUTINE c_diff_fast(u, du, d2u, j_in, nlocal, meth, id, ie, mn_var, mx_var )
  IMPLICIT NONE
  INTEGER, INTENT(IN) :: j_in, nlocal, meth, id
  INTEGER, INTENT(IN) :: ie ! number of dimensions of u(:,1:ie)  we are doing derivative for (forced to 1 for now)
  INTEGER, INTENT(IN) ::  mn_var, mx_var   ! min and max of second index of u that we are doing the 1st derivatives for.
!  INTEGER, INTENT(IN) ::  mn_varD2, mx_varD2 ! min and max of second index of u that we are doing the 2nd derivatives for.

  INTEGER i,idim !debug
  REAL (pr), DIMENSION (1:nlocal, mn_var:mx_var), INTENT (IN) :: u
  REAL (pr), DIMENSION (mx_var-mn_var+1, 1:nlocal,dim), INTENT (INOUT) :: du,d2u
  REAL t0,t1

  IF( flag_timing ) CALL CPU_TIME (t0)

  IF( debug_c_diff_fast>=1) THEN
     PRINT *,'c_diff_fast() enter' !debug
     DO i = mn_var,mx_var
         WRITE(*,'( "minmax u(:,",i2.2,") ", 2(G20.12,1X) )') &
		     i, MINVAL(u(:,i)),MAXVAL(u(:,i)) !debug 
     END DO
  END IF
  ! update the db from u
  ! u(:, mn_var:mx_var) -> db%u(db_offset:mx_var-mn_var+1)
  ! db_offset = 1 
  !CALL update_db_from_u(  u , nlocal ,ie  , mn_var, mx_var, 1  )
  IF( debug_zero_db ) THEN
      CALL zero_all_db_backend( )
	  PRINT *,'c_diff_fast() : CALL zero_all_db_backend( )'
  END IF
  CALL update_db_from_u_interpolate(  u , nlocal , ie, mn_var, mx_var, j_in, MIN(j_in+1,j_lev) ) !added +1 to last arg....DG 08292006
  CALL c_diff_fast_db(du, d2u, j_in, nlocal, meth, id, ie,  mn_var, mx_var, &
	mn_var, mx_var, mn_var, mx_var )

  IF( debug_c_diff_fast>=1) THEN
     PRINT *,'c_diff_fast()' !debug
     DO i = 1,mx_var-mn_var+1
       DO idim = 1,dim
         WRITE(*,'( "minmax du(",i2.2,":,",i2.2,") ", 2(G20.12,1X) )') &
	          i,idim,MINVAL(du(i,:,idim)),MAXVAL(du(i,:,idim)) !debug 
	   ENDDO
     END DO
	 IF(MOD(id,10) == 1 ) THEN ! du2 was calculated, so print it out also
		 DO i = 1,mx_var-mn_var+1
		   DO idim = 1,dim
			 WRITE(*,'( "minmax d2u(",i2.2,":,",i2.2,") ", 2(G20.12,1X) )') &
  				  i,idim,MINVAL(d2u(i,:,idim)),MAXVAL(d2u(i,:,idim)) !debug 
  		   ENDDO
		 END DO

	 END IF
  END IF
  IF( flag_timing ) THEN
     CALL CPU_TIME (t1)
     WRITE (6,'("CPU TIME (c_diff_fast) = ", es12.5)') t1 - t0
  END IF

END SUBROUTINE c_diff_fast

!
! 
!**************************************************************************
!
! Find spacial derivatives of components already in db
!
!
! Arguments
!u, du, d2u, j_in, nlocal, meth, meth1, 
! ID Sets which derivative is done 
!    10 - do first derivative
!    11 - do first and secont derivative
!    01 - do second derivative
!    if ID > 0 then the second derivative is central difference D_central
!    if ID < 0 then the second derivative is D_backward_bias followed by D_forward_bias
! meth  - order of the method, high low,
!
!  dimensions of u are u(1:nlocal,1:ie) 
!  derivative is done on  u(1:nlocal,mn_var:mx_var)
SUBROUTINE c_diff_fast_db(du, d2u, j_in, nlocal, meth, id, ie, mn_var, mx_var, mn_varD, mx_varD , mn_varD2, mx_varD2)
  IMPLICIT NONE
  INTEGER, INTENT(IN) :: j_in, nlocal, meth, id
  INTEGER, INTENT(IN) :: ie ! number of dimensions of u(:,1:ie)  we are doing derivative for (forced to 1 for now)
  INTEGER, INTENT(IN) ::  mn_var, mx_var ! min and max of second index of u that we are doing the derivatives for.
  INTEGER, INTENT(IN) ::  mn_varD, mx_varD , mn_varD2, mx_varD2 ! min/max extents in db to do 1st/2nd derivative
  REAL (pr), DIMENSION (mn_varD :mx_varD , 1:nlocal,dim), INTENT (INOUT) :: du
  REAL (pr), DIMENSION (mn_varD2:mx_varD2, 1:nlocal,dim), INTENT (INOUT) :: d2u
!  REAL (pr), DIMENSION (mn_varD :mx_varD , 1:nwlt,dim) :: duh
!  REAL (pr), DIMENSION (mn_varD2:mx_varD2, 1:nwlt,dim) :: d2uh


  INTEGER :: i,j,k,idim
  INTEGER :: ixyz(3),ixyzp(3), ixyz2


  INTEGER :: ibndr, istep, ishift, trnsf_type
  REAL    :: t1_omp, t2_omp


  IF( flag_timing ) CALL CPU_TIME (t1_omp)

!print *,'j_in,j_lev,j_full',j_in,j_lev,j_full
!PRINT *,'lv = ', lv !debug only
!PRINT *,'lvj = ', lvj !debug only

!$OMP PARALLEL DEFAULT(SHARED )

!  LOGICAL  :: ghost_zeroed(1:j_in) !set

  !
  ! 
  ! 
!  ghost_zeroed = .FALSE.


  !
  ! sanity check
  !
  IF( mn_var-mx_var+1 > ie ) THEN
	PRINT *,' ERROR in c_diff_fast(), ie <  mn_var-mx_var+1, Exiting...'
	STOP
  END IF
   


  trnsf_type = 0


  !
  !---------- forward transform -----------------------------
  !
  DO j = MIN(j_in,j_lev)-1,  MIN(j_in,j_full), -1
     !
     ! Do the wlt transform for level j
     !
     
     call c_wlt_trns_aux_db(nlocal,  mn_var, mx_var,MAXVAL(mxyz(1:dim)*2**(j_lev-1)) , & !   MAXVAL(nxyz), & !
              j, j_lev, &
             n_prdct, n_updt ,trnsf_type, jd, prd, WLT_TRNS_FWD )
  END DO

  !
  ! Zero out all ghost points
  ! NOTE IF WE DO THIS AT EVERY CALL IT NEEDS TO BE DONE ON LY FOR J<J_IN!!!!!
  !
#ifdef EXPLICIT_GHOST_SWEEP
   CALL zero_ghost_lines_db_backend(MIN(j_in+1,j_lev))
   !CALL zero_ghost_lines_db_backend(j_mx) !TEST
   !PRINT *,'Explicitly zero ghost up to j_mx'
#endif
  !
  !----------------  inverse transform ----------------------
  !
  DO j = MIN(j_in,j_full),MIN(j_in,j_lev)


     !
     ! calculate some of the derivatives at each level as we do the inverse transform
     !

	 IF( ABS(id) == 11 ) THEN 
     !----------------- Calculation of first and second derivative ---------------

		! calculate derivative at this level if there are points to be done at this level
		IF(j < j_in .AND. lv(j-1)+1 <= lv(j))  THEN
			CALL diff_aux_D1andD2_db (nlocal, MAXVAL(nxyz), du, d2u, wgh_df, wgh_d2f, j, meth, 1, 2, mn_var, mx_var, &
				mn_varD, mx_varD , mn_varD2, mx_varD2)! ,ghost_zeroed)
		END IF
        
		!calculate finest level derivative if there are points to be done at this level
		IF(j == j_in .AND. lvj(j-1)+1 <= lvj(j))  THEN
			 CALL diff_aux_D1andD2_finest_lev_db (nlocal, MAXVAL(nxyz), du, d2u, wgh_df, wgh_d2f, j, meth, 1, 2, mn_var, mx_var, &
				mn_varD, mx_varD , mn_varD2, mx_varD2)! ,ghost_zeroed)
		END IF
	 ELSE

		 !----------------- Calculation of first derivative ---------------
		 !
		 IF(INT(id/10) == 1 ) THEN
			! calculate derivative at this level if there are points to be done at this level
			IF(j < j_in .AND. lv(j-1)+1 <= lv(j))  THEN
			  CALL diff_aux_db (nlocal, MAXVAL(nxyz), du, wgh_df, j, meth, 1, mn_varD, mx_varD)! ,ghost_zeroed) 
			END IF

			!calculate finest level derivative if there are points to be done at this level
			IF(j == j_in .AND. lvj(j-1)+1 <= lvj(j))  THEN
			  CALL diff_aux_finest_lev_db (nlocal, MAXVAL(nxyz), du, wgh_df, j, meth, 1, mn_varD, mx_varD)! ,ghost_zeroed)
			END IF
		 END IF
		 !
		 !----------------- Calculation of second derivative ---------------
		 !
		 IF(MOD(id,10) == 1 ) THEN
			! calculate derivative at this level if there are points to be done at this level
			IF(j < j_in .AND. lv(j-1)+1 <= lv(j))  THEN
			  CALL diff_aux_db (nlocal, MAXVAL(nxyz), d2u, wgh_d2f, j, meth, 2, mn_varD2, mx_varD2)! ,ghost_zeroed)
			END IF

			!calculate finest level derivative if there are points to be done at this level
			IF(j == j_in .AND. lvj(j-1)+1 <= lvj(j))  THEN
			  CALL diff_aux_finest_lev_db (nlocal, MAXVAL(nxyz), d2u, wgh_d2f, j, meth, 2, mn_varD2, mx_varD2)! ,ghost_zeroed)
			END IF
		 END IF
     END IF

     !
     !*********** inverse transform  *********************
     !        
     IF( j < j_in ) THEN
		call c_wlt_trns_aux_db(nlocal,  mn_var, mx_var, MAXVAL(mxyz(1:dim)*2**(j_lev-1)) , & !MAXVAL(nxyz), & !
              j, j_lev, &
             n_prdct, n_updt ,trnsf_type, jd, prd,  WLT_TRNS_INV_wGHOST )

     END IF
	 
  END DO



!$OMP END PARALLEL
  IF( flag_timing )THEN
     CALL CPU_TIME (t2_omp)
     WRITE (6,'("CPU TIME in c_diff_fast_db() = ", es12.5)') t2_omp - t1_omp
  END IF

END SUBROUTINE c_diff_fast_db




!
!**************************************************************************
!
! Arguments
! du, d2u, j_in, nlocal, meth, meth1, 
! ID Sets which derivative is done 
!    10 - do first derivative
!    11 - do first and secont derivative
!    01 - do second derivative
!    if ID > 0 then the second derivative is central difference D_central
!    if ID < 0 then the second derivative is D_backward_bias followed by D_forward_bias
!
SUBROUTINE c_diff_diag( du, d2u, j_in, nlocal, meth, meth1, ID)
  IMPLICIT NONE
  INTEGER, INTENT(IN) :: j_in, nlocal, meth, meth1, id
  REAL (pr), DIMENSION (1:nlocal,dim), INTENT (INOUT) :: du,d2u
  REAL (pr), DIMENSION (1:nwlt,dim) :: duh,d2uh
  REAL (pr) :: c_predict
  INTEGER :: i,j,k
  INTEGER :: ix,nx_l,nx_h,ix_even,ix_odd,ixp,ixpm,ix_l,ix_h
  INTEGER :: iy,ny_l,ny_h,iy_even,iy_odd,iyp,iypm,iy_l,iy_h
  INTEGER :: ibndr, istep, ishift, trnsf_type



  trnsf_type = 0

!
!----------------  inverse transform ----------------------
!
  DO j = MIN(j_in,j_full),MIN(j_in,j_lev)
     !
     !----------------- Calculation of first derivative ---------------
     !
     IF(INT(ABS(ID)/10) == 1) THEN
        IF(j < j_in)  THEN
			CALL diff_diag_aux (MAXVAL(nxyz), duh, wgh_df, j, meth, meth, 1, .FALSE.)
        ENDIF

        IF(j == j_in) THEN

			CALL diff_diag_aux (MAXVAL(nxyz), duh, wgh_df, j, meth, meth, 1, .TRUE. )
       END IF
     END IF
     !
     !----------------- Calculation of second derivative D^2 ---------------
     !
     IF(MOD(ID,10) == 1 .AND. ID > 0) THEN
        IF(j < j_in)  THEN
			CALL diff_diag_aux (MAXVAL(nxyz), d2uh, wgh_d2f, j, meth, meth, 2, .FALSE. )
       END IF

        IF(j == j_in) THEN
			CALL diff_diag_aux (MAXVAL(nxyz), d2uh, wgh_d2f, j, meth, meth, 2, .TRUE.)
       END IF
     END IF
     !
     !----------------- Calculation of second derivative DD ---------------
     !
     IF(MOD(-ID,10) == 1 .AND. ID < 0) THEN
        IF(j < j_in)  THEN
			CALL diff_diag_aux (MAXVAL(nxyz), d2uh, wgh_df, j, meth, meth1, 3, .FALSE. )
       END IF
        IF(j == j_in) THEN
			CALL diff_diag_aux (MAXVAL(nxyz), d2uh, wgh_df, j, meth, meth1, 3, .TRUE. )
       END IF
     END IF
  END DO

  !
  ! Put result of the deriviative calculations into du and d2u
  ! This is done to get proper placement of the boundary points at the current level
  !
  IF(INT(ABS(ID)/10) == 1 ) THEN
     du(1:lv_intrnl(j_in),1:dim)=duh(1:lv_intrnl(j_in),1:dim)
     DO ibndr=1,2*dim
        du(lv_bnd(2,ibndr,j_in):lv_bnd(3,ibndr,j_in),1:dim)  = duh(lv_bnd(0,ibndr,1):lv_bnd(1,ibndr,j_in),1:dim)
     END DO
  END IF
  IF(MOD(ID,10) == 1 .AND. ID > 0) THEN
     d2u(1:lv_intrnl(j_in),1:dim)=d2uh(1:lv_intrnl(j_in),1:dim)
     DO ibndr=1,2*dim
        d2u(lv_bnd(2,ibndr,j_in):lv_bnd(3,ibndr,j_in),1:dim) = d2uh(lv_bnd(0,ibndr,1):lv_bnd(1,ibndr,j_in),1:dim)
     END DO
  END IF
  IF(MOD(-ID,10) == 1 .AND. ID < 0) THEN
     d2u(1:lv_intrnl(j_in),1:dim)=d2uh(1:lv_intrnl(j_in),1:dim)
     DO ibndr=1,2*dim
        d2u(lv_bnd(2,ibndr,j_in):lv_bnd(3,ibndr,j_in),1:dim) = d2uh(lv_bnd(0,ibndr,1):lv_bnd(1,ibndr,j_in),1:dim)
     END DO
  END IF

END SUBROUTINE c_diff_diag

!
!**************************************************************************
!
SUBROUTINE c_diff_diag_bnd(du, d2u, j_in, nlocal, meth)
  IMPLICIT NONE
  INTEGER, INTENT(IN) :: j_in, nlocal, meth
  REAL (pr), DIMENSION (1:nlocal,dim), INTENT (INOUT) :: du,d2u
  REAL (pr), DIMENSION (1:nwlt,dim) :: duh,d2uh
  REAL (pr) :: c_predict
  INTEGER :: i,j,k
  INTEGER :: ix,nx_l,nx_h,ix_even,ix_odd,ixp,ixpm,ix_l,ix_h
  INTEGER :: iy,ny_l,ny_h,iy_even,iy_odd,iyp,iypm,iy_l,iy_h
  INTEGER :: ibndr, istep, ishift, trnsf_type


  trnsf_type = 0


  DO j = MIN(j_in,j_full),MIN(j_in,j_lev)
     !
     !----------------- Calculation of first derivative ---------------
     !
        IF(j < j_in)  CALL diff_diag_bnd_aux (MAXVAL(nxyz), duh, wgh_df, wgh_d2f, j, meth, .FALSE.)
        IF(j == j_in) CALL diff_diag_bnd_aux (MAXVAL(nxyz), duh, wgh_df, wgh_d2f, j, meth, .TRUE.)
     !
  END DO

  du(1:lv_intrnl(j_in),1:dim)=duh(1:lv_intrnl(j_in),1:dim)
  DO ibndr=1,2*dim
     du(lv_bnd(2,ibndr,j_in):lv_bnd(3,ibndr,j_in),1:dim)  = duh(lv_bnd(0,ibndr,1):lv_bnd(1,ibndr,j_in),1:dim)
  END DO

END SUBROUTINE c_diff_diag_bnd


!
!
! Do derivative at level j for mn_var to mx_var variables in db
! Result is put in du from 1:mx_var-mn_var+1
!
!
SUBROUTINE diff_aux_db (nlocal, max_nxyz,du, wghd_in, j_in, meth, order, mn_var, mx_var)! ,ghost_zeroed)
  IMPLICIT NONE
  INTEGER, INTENT(IN) ::  nlocal, j_in, meth, order,max_nxyz, mn_var, mx_var
  REAL (pr), DIMENSION (mn_var:mx_var ,1:nlocal,dim),   INTENT (INOUT) :: du
  REAL (pr), DIMENSION (-2*n_diff-1:2*n_diff+1,0:max_nxyz,j_lev,0:n_df,1:3), INTENT (IN) :: wghd_in
!  LOGICAL , INTENT(INOUT) :: ghost_zeroed(1:j_in) !flag for if level j has had ghost pts zeroed
  INTEGER :: i,k,ix,iy,iz,ixp,ixpm,iyp,iypm,izp,izpm,iD 
  
  INTEGER :: iD3d

  INTEGER :: idim  , indx_line, indx_line_j_lev, indx_line_db
  REAL(pr)             :: line_u(mn_var:mx_var ,0:max_nxyz) 

  INTEGER(KIND_INT_FLAGS):: line_flags(0:max_nxyz)
  INTEGER              :: line_coords(0:max_nxyz,dim)  ! coordinated of returned points in terms of j_mx
  INTEGER              :: line_deriv_lev(0:max_nxyz)
  INTEGER              :: line_flat_indx(0:max_nxyz)
  TYPE (grid_pt_ptr)   :: line_ptrs(0:max_nxyz)
  INTEGER :: n_active_pts,active_pts(max_nxyz+1)
  INTEGER :: n_active_pts_even,active_pts_even(max_nxyz+1)
  INTEGER :: n_j_above,j_above(max_nxyz+1)
  INTEGER :: db_scale!, ie
  INTEGER :: nxyz_j_in(1:dim)  !nxyz at level j_in
  INTEGER :: shift(0:2*dim), ibndr
  INTEGER :: plocal_numlines_indx_backend ! number of active lines indexed in lines_indx_backend
  TYPE (element_of_face_ptr) , DIMENSION(:), POINTER :: plocal_lines_indx_backend !  list of active line faces
  INTEGER :: plocal_lines_indx_current ! current active line to be returned 

  INTEGER ,SAVE :: itest = 0 !DEBUG ONLY
  character (len=3) :: testchar !DEBUG ONLY


  IF( debug_c_diff_fast>=4) THEN
     WRITE(*,'(A,i6,A,i6)') "diff_aux_db(): nlocal =",nlocal, " max_nxyz = ",max_nxyz
	 WRITE(*,'(A,i2,A,i2,A,i2)') "j_in = ",j_in, "meth = ",meth, "order = ",order
	 WRITE(*,'(A,i2,A,i2,A)') "mn_var = ", mn_var, "mx_var = ", mx_var
	 itest = itest +1
	 write(testchar,'(i3.3)') itest
	 OPEN(UNIT=111,FILE='tmpdu.lines'//testchar,FORM='formatted')
     WRITE(*,'(A,A,A)') "Opening: ",'tmpdu.lines'//testchar,"for debugging output"
  END IF

  db_scale = (2**(j_mx_db-j_in)) !test
  nxyz_j_in = nxyz/(2**(j_lev-j_in))

  !setup shift array to map things back into du
  shift(0) = 0
  DO ibndr = 1,2*dim
    shift(ibndr)  = lv_bnd(2,ibndr,j_in)-lv_bnd(0,ibndr,1)
  END DO

  line_u = 0.0_pr ! make sure the line array is zeroed out
  line_flags = 0  ! make sure the line flags array is zeroed out
  DO idim = 1,dim 

	! Get lines for 3d level <= j_in+1
	CALL get_active_lines_indx( idim, j_in, j_in, j_in, &
           plocal_lines_indx_backend, plocal_numlines_indx_backend,&
		   plocal_lines_indx_current ) !4/18/05 DG changed from j_in+1 to j_in 

	!TEST
	IF(plocal_numlines_indx_backend==0)THEN
		print *,'diff_aux_db() plocal_numlines_indx_backend==0'
		stop
	END IF

		!
		! Get points on line at 1D level j_in and below
		!
		! NOTE: Here we should get a list of points that have deriv_lev == j_in
	DO WHILE( get_active_line_diff_aux( idim, 0 ,j_in ,j_in  ,max_nxyz ,  &
			mn_var, mx_var , &
			line_u, line_flags,  line_deriv_lev, line_flat_indx, line_ptrs, line_coords, &
			active_pts, n_active_pts, GET_GHOST_PTS ,shift, &
            plocal_lines_indx_backend, plocal_numlines_indx_backend,&
		    plocal_lines_indx_current ))
			  


		DO i=1,n_active_pts
			indx_line = active_pts(i)
			indx_line_db = indx_line * (2**(j_mx_db-j_lev)) !find index relative to db coordinates
            indx_line_j_lev = indx_line * 2**(j_lev-j_in)

            IF(BTEST(line_flags(indx_line),GHOST )) THEN
			  !IF( line_u( 1, indx_line) /= 0.0 ) THEN
			   ! print *,'error'
			   !END IF
#ifndef EXPLICIT_GHOST_SWEEP
               IF( idim == 1 .AND. lvl_db(indx_line_db) <= j_in )  line_u( mn_var:mx_var,indx_line) = 0.0_pr
#endif
			! find deriviative if this point has the current deriv_lev
			! (we know from above if that it is not a ghost pt)
			ELSE IF( j_in == line_deriv_lev(indx_line) ) THEN
				du(mn_var:mx_var, line_flat_indx(indx_line),idim  ) = 0.0_pr

				DO ixpm = ix_lh_diff_db(indx_line_db,order,j_in ,idim,meth)%ix_l, &
						ix_lh_diff_db(indx_line_db,order,j_in ,idim,meth)%ix_h

					ixp = (1-prd(idim))*(indx_line+ixpm)+ &
						prd(idim)*MOD(indx_line+ixpm+9*nxyz_j_in(idim),nxyz_j_in(idim))
			  !IF( line_u( 1, ixp) /= 0.0 ) THEN
			  !  print *,'error'
			  ! END IF
					du(mn_var:mx_var, line_flat_indx(indx_line),idim ) = du(mn_var:mx_var, line_flat_indx(indx_line),idim ) + &
						wghd_in(ixpm,indx_line_j_lev,j_in,meth,idim)*line_u( mn_var:mx_var, ixp)
!print *,'DU = ',du(mn_var:mx_var, line_flat_indx(indx_line),idim ), j_in

                   IF( debug_c_diff_fast>=4 .and. idim == 1) THEN
				      !WRITE(*,'(A,i,A,i)') & 
					  !   "indx_line_db = ",indx_line_db, "ixp = ",ixp
                      !WRITE(*,'(A,G20.12,A,G20.12)') &
					  !  "du =",du(mn_var, line_flat_indx(indx_line),idim ), &
					  !  " line_u ",line_u( mn_var, ixp)
                      WRITE(111,'(i6,1X,i6,1X,i6,1X,G20.12,1X,G20.12)') line_flat_indx(indx_line),&
					    indx_line_db,ixp, &
					    du(mn_var, line_flat_indx(indx_line),idim ), &
					    line_u( mn_var, ixp)
                   END IF
				END DO ! ixpm = 
			END IF
#ifndef EXPLICIT_GHOST_SWEEP
            line_flags(indx_line) = 0.0_pr !  BRUTEFORCE 
#endif
		END DO ! i=1,n_active_pts

	END DO !ln = 1, nlines
END DO ! idim = 1,dim 

 CALL deallocate_active_lines_indx(plocal_lines_indx_backend)

  IF( debug_c_diff_fast>=4) THEN
	 CLOSE(UNIT=111)
  END IF

END SUBROUTINE diff_aux_db


!
! Do derivative at level j for mn_var to mx_var variables in db
! Result is put in du from 1:mx_var-mn_var+1
!
! Only done if deriviative level is greater or equal to j and  physical 3d level is j or less
!
SUBROUTINE diff_aux_finest_lev_db (nlocal, max_nxyz,du, wghd_in, j_in, meth, order, mn_var, mx_var)! ,ghost_zeroed)
  IMPLICIT NONE
  INTEGER, INTENT(IN) ::  nlocal, j_in, meth, order,max_nxyz, mn_var, mx_var
  REAL (pr), DIMENSION (mx_var-mn_var+1,1:nlocal,dim),   INTENT (INOUT) :: du
  REAL (pr), DIMENSION (-2*n_diff-1:2*n_diff+1,0:max_nxyz,j_lev,0:n_df,1:3), INTENT (IN) :: wghd_in
!  LOGICAL , INTENT(INOUT) :: ghost_zeroed(1:j_in) !flag for if level j has had ghost pts zeroed
  INTEGER :: i,k,ix,iy,iz,ixp,ixpm,iyp,iypm,izp,izpm,iD 
  INTEGER :: iD3d
  INTEGER :: idim  , indx_line, indx_line_j_lev, indx_line_db
  REAL(pr)             :: line_u(mn_var:mx_var ,0:max_nxyz) !
  INTEGER(KIND_INT_FLAGS):: line_flags(0:max_nxyz)
  INTEGER              :: line_coords(0:max_nxyz,dim)  ! coordinated of returned points in terms of j_mx
  INTEGER              :: line_deriv_lev(0:max_nxyz)
  INTEGER              :: line_flat_indx(0:max_nxyz)
  TYPE (grid_pt_ptr)   :: line_ptrs(0:max_nxyz)
  INTEGER :: n_active_pts,active_pts(max_nxyz+1)
  INTEGER :: n_active_pts_even,active_pts_even(max_nxyz+1)
  INTEGER :: n_j_above,j_above(max_nxyz+1)
  !INTEGER :: ixyz_loc(1:3), ixyz_db(1:3)
  INTEGER :: db_scale !, ie
  INTEGER :: nxyz_j_in(1:dim)  !nxyz at level j_in
  INTEGER :: shift(0:2*dim), ibndr
  INTEGER :: plocal_numlines_indx_backend ! number of active lines indexed in lines_indx_backend
  TYPE (element_of_face_ptr) , DIMENSION(:), POINTER :: plocal_lines_indx_backend !  list of active line faces
  INTEGER :: plocal_lines_indx_current ! current active line to be returned 
  INTEGER ,SAVE :: itest = 0 !DEBUG ONLY
  character (len=3) :: testchar !DEBUG ONLY



  IF( debug_c_diff_fast>=4) THEN
     WRITE(*,'(A,i6,A,i6)') "diff_aux_finest_lev_db(): nlocal =",nlocal, " max_nxyz = ",max_nxyz
	 WRITE(*,'(A,i2,A,i2,A,i2)') "j_in = ",j_in, "meth = ",meth, "order = ",order
	 WRITE(*,'(A,i2,A,i2,A)') "mn_var = ", mn_var, "mx_var = ", mx_var
	 itest = itest +1
	 write(testchar,'(i3.3)') itest
	 OPEN(UNIT=111,FILE='tmpdu.lines'//testchar,FORM='formatted')
     WRITE(*,'(A,A,A)') "Opening: ",'tmpdu.lines'//testchar,"for debugging output"
  END IF

  db_scale = (2**(j_mx_db-j_in)) !test
  nxyz_j_in = nxyz/(2**(j_lev-j_in))

  !setup shift array to map things back into du
  shift(0) = 0
  DO ibndr = 1,2*dim
    shift(ibndr)  = lv_bnd(2,ibndr,j_in)-lv_bnd(0,ibndr,1)
  END DO

  line_u = 0.0_pr ! make sure the line array is zeroed out
  line_flags = 0  ! make sure the line flags array is zeroed out

  DO idim = 1,dim 

	! Get lines for 3d level <= j_lev+1
	CALL get_active_lines_indx( idim, j_in, j_in, j_lev, &
           plocal_lines_indx_backend, plocal_numlines_indx_backend,&
		   plocal_lines_indx_current ) !

	!TEST
	IF(plocal_numlines_indx_backend==0)THEN
		print *,'diff_aux_finest_lev_db() plocal_numlines_indx_backend==0'
		stop
	END IF

	! get points on line at 1D level j_in and below
	DO WHILE( get_active_line_diff_aux( idim, 0 ,j_in ,j_in  ,max_nxyz ,  &
			mn_var, mx_var , &
			line_u, line_flags,  line_deriv_lev, line_flat_indx, line_ptrs, line_coords, &
			active_pts, n_active_pts, GET_GHOST_PTS, shift, &
            plocal_lines_indx_backend, plocal_numlines_indx_backend,&
		    plocal_lines_indx_current  ) ) 

		DO i=1,n_active_pts
			indx_line = active_pts(i)

			!ixyz_loc(idim) = indx_line  
			!ixyz_db(idim)  = ixyz_loc(idim) * db_scale 
			indx_line_db = indx_line * db_scale ! find index relative to db coordinates
            indx_line_j_lev = indx_line * 2**(j_lev-j_in)

            IF(BTEST(line_flags(indx_line),GHOST )) THEN
#ifndef EXPLICIT_GHOST_SWEEP
               IF( idim == 1 .AND. lvl_db(indx_line_db) <= j_in )  line_u( mn_var:mx_var,indx_line) = 0.0_pr
#endif
			! find deriviative if this point has the current deriv_lev
			! (we know from above if that it is not a ghost pt)
			ELSE IF( j_in <= line_deriv_lev(indx_line) ) THEN
			
				du( mn_var:mx_var,line_flat_indx(indx_line),idim ) = 0.0_pr


				DO ixpm = ix_lh_diff_db(indx_line_db,order,j_in ,idim,meth)%ix_l, &
						ix_lh_diff_db(indx_line_db,order,j_in ,idim,meth)%ix_h

					ixp = (1-prd(idim))*(indx_line+ixpm)+ &
						prd(idim)*MOD(indx_line+ixpm+9*nxyz_j_in(idim),nxyz_j_in(idim))

					du( mn_var:mx_var,line_flat_indx(indx_line),idim ) = du( mn_var:mx_var,line_flat_indx(indx_line),idim ) + &
						wghd_in(ixpm,indx_line_j_lev,j_in,meth,idim)*line_u( mn_var:mx_var, ixp)
!print *,'DU = ',du(mn_var:mx_var, line_flat_indx(indx_line),idim ), j_in
                   IF( debug_c_diff_fast>=4 .and. idim == 1) THEN
				     ! WRITE(*,'(A,i,A,i)') & 
					  !   "indx_line_db = ",indx_line_db, "ixp = ",ixp
                      !WRITE(*,'(A,G20.12,A,G20.12)') &
					  !  "du =",du(mn_var, line_flat_indx(indx_line),idim ), &
					  !  " line_u ",line_u( mn_var, ixp)
                      WRITE(111,'(i6,1X,i6,1X,i6,1X,G20.12,1X,G20.12)') line_flat_indx(indx_line),&
					    indx_line_db,ixp, &
					    du(mn_var, line_flat_indx(indx_line),idim ), &
					    line_u( mn_var, ixp)
                   END IF

				END DO ! ixpm = 
			END IF
		END DO ! i=1,n_active_pts

	END DO !ln = 1, nlines
END DO ! idim = 1,dim 

 CALL deallocate_active_lines_indx(plocal_lines_indx_backend)

  IF( debug_c_diff_fast>=4) THEN
	 CLOSE(UNIT=111)
  END IF

END SUBROUTINE diff_aux_finest_lev_db

!
!
! Do 1st and second derivative at level j for mn_var to mx_var variables in db
! Result is put in du from 1:mx_var-mn_var+1
!
!
SUBROUTINE diff_aux_D1andD2_db (nlocal, max_nxyz,du, d2u, wghd_in, wghd2_in, j_in, meth, orderD1, orderD2,  mn_var, mx_var, &
   mn_varD, mx_varD , mn_varD2, mx_varD2)! ,ghost_zeroed)
  IMPLICIT NONE
  INTEGER, INTENT(IN) :: nlocal, j_in, meth, orderD1, orderD2,max_nxyz, mn_var, mx_var, mn_varD, mx_varD , mn_varD2, mx_varD2
  REAL (pr), DIMENSION (mn_varD :mx_varD , 1:nlocal,dim), INTENT (INOUT) :: du
  REAL (pr), DIMENSION (mn_varD2:mx_varD2, 1:nlocal,dim), INTENT (INOUT) :: d2u
  REAL (pr), DIMENSION (-2*n_diff-1:2*n_diff+1,0:max_nxyz,j_lev,0:n_df,1:3), INTENT (IN) :: wghd_in, wghd2_in
!  LOGICAL , INTENT(INOUT) :: ghost_zeroed(1:j_in) !flag for if level j has had ghost pts zeroed
  INTEGER :: i,k,ix,iy,iz,ixp,ixpm,iyp,iypm,izp,izpm,iD 
  
  INTEGER :: iD3d

  INTEGER :: idim  , indx_line, indx_line_j_lev, indx_line_db
  REAL(pr)             :: line_u(mn_var:mx_var ,0:max_nxyz) 

  INTEGER(KIND_INT_FLAGS):: line_flags(0:max_nxyz)
  INTEGER              :: line_coords(0:max_nxyz,dim)  ! coordinated of returned points in terms of j_mx
  INTEGER              :: line_deriv_lev(0:max_nxyz)
  INTEGER              :: line_flat_indx(0:max_nxyz)
  TYPE (grid_pt_ptr)   :: line_ptrs(0:max_nxyz)
  INTEGER :: n_active_pts,active_pts(max_nxyz+1)
  INTEGER :: n_active_pts_even,active_pts_even(max_nxyz+1)
  INTEGER :: n_j_above,j_above(max_nxyz+1)
  INTEGER :: db_scale!, ie
  INTEGER :: nxyz_j_in(1:dim)  !nxyz at level j_in
  INTEGER :: shift(0:2*dim), ibndr
  INTEGER :: plocal_numlines_indx_backend ! number of active lines indexed in lines_indx_backend
  TYPE (element_of_face_ptr) , DIMENSION(:), POINTER :: plocal_lines_indx_backend !  list of active line faces
  INTEGER :: plocal_lines_indx_current ! current active line to be returned 


  db_scale = (2**(j_mx_db-j_in)) !test
  nxyz_j_in = nxyz/(2**(j_lev-j_in))

  !setup shift array to map things back into du
  shift(0) = 0
  DO ibndr = 1,2*dim
    shift(ibndr)  = lv_bnd(2,ibndr,j_in)-lv_bnd(0,ibndr,1)
  END DO

  line_u = 0.0_pr ! make sure the line array is zeroed out
  line_flags = 0  ! make sure the line flags array is zeroed out

  DO idim = 1,dim 

	! Get lines for 3d level <= j_in+1
	CALL get_active_lines_indx( idim, j_in, j_in, j_in, &
           plocal_lines_indx_backend, plocal_numlines_indx_backend,&
		   plocal_lines_indx_current ) !
    

	IF(plocal_numlines_indx_backend==0)THEN
		print *,'diff_aux_D1andD2_db() plocal_numlines_indx_backend==0'
		stop
	END IF

 
		! get points on line at 1D level j_in and below
	DO WHILE( get_active_line_diff_aux( idim, 0 ,j_in ,j_in  ,max_nxyz ,  &
			mn_var, mx_var , &
			line_u, line_flags,  line_deriv_lev, line_flat_indx, line_ptrs, line_coords, &
			active_pts, n_active_pts, GET_GHOST_PTS , shift, &
            plocal_lines_indx_backend, plocal_numlines_indx_backend,&
		    plocal_lines_indx_current ))
		     
		DO i=1,n_active_pts
			indx_line = active_pts(i)
			indx_line_db = indx_line * db_scale !find index relative to db coordinates
            indx_line_j_lev = indx_line * 2**(j_lev-j_in)

            IF(BTEST(line_flags(indx_line),GHOST )) THEN
#ifndef EXPLICIT_GHOST_SWEEP
               IF( idim == 1 .AND. lvl_db(indx_line_db) <= j_in )  line_u( mn_var:mx_var,indx_line) = 0.0_pr
#endif
			! find deriviative if this point has the current deriv_lev
			! (we know from above if that it is not a ghost pt)
			ELSE IF( j_in == line_deriv_lev(indx_line) ) THEN

			! find deriviative if this point has the current deriv_lev
			!IF( j_in == line_deriv_lev(indx_line) .AND. .NOT. BTEST(line_flags(indx_line),GHOST ) ) THEN
				du( mn_varD :mx_varD , line_flat_indx(indx_line),idim  ) = 0.0_pr
				d2u(mn_varD2:mx_varD2, line_flat_indx(indx_line),idim  ) = 0.0_pr


				
							
				!1st deriviatve
				DO ixpm = ix_lh_diff_db(indx_line_db,orderD1,j_in ,idim,meth)%ix_l, &
						ix_lh_diff_db(indx_line_db,orderD1,j_in ,idim,meth)%ix_h

					ixp = (1-prd(idim))*(indx_line+ixpm)+ &
						prd(idim)*MOD(indx_line+ixpm+9*nxyz_j_in(idim),nxyz_j_in(idim))

					du(mn_varD:mx_varD, line_flat_indx(indx_line),idim ) = du(mn_varD:mx_varD, line_flat_indx(indx_line),idim ) + &
						wghd_in(ixpm,indx_line_j_lev,j_in,meth,idim)*line_u( mn_varD:mx_varD, ixp)
					
				END DO ! ixpm = 

				!2nd deriviative
				DO ixpm = ix_lh_diff_db(indx_line_db,orderD2,j_in ,idim,meth)%ix_l, &
						ix_lh_diff_db(indx_line_db,orderD2,j_in ,idim,meth)%ix_h

					ixp = (1-prd(idim))*(indx_line+ixpm)+ &
						prd(idim)*MOD(indx_line+ixpm+9*nxyz_j_in(idim),nxyz_j_in(idim))

					d2u(mn_varD2:mx_varD2, line_flat_indx(indx_line),idim ) = d2u(mn_varD2:mx_varD2, line_flat_indx(indx_line),idim ) + &
						wghd2_in(ixpm,indx_line_j_lev,j_in,meth,idim)*line_u( mn_varD2:mx_varD2, ixp)

				END DO ! ixpm = 
			END IF
		END DO ! i=1,n_active_pts

	END DO !ln = 1, nlines
END DO ! idim = 1,dim 

 CALL deallocate_active_lines_indx(plocal_lines_indx_backend)

END SUBROUTINE diff_aux_D1andD2_db


!
! Do derivative at level j for mn_var to mx_var variables in db
! Result is put in du from 1:mx_var-mn_var+1
!
! Only done if deriviative level is greater or equal to j and  physical 3d level is j or less
!
SUBROUTINE diff_aux_D1andD2_finest_lev_db (nlocal, max_nxyz,du, d2u, wghd_in, wghd2_in, j_in, meth, orderD1, orderD2, mn_var, mx_var, &
   mn_varD, mx_varD , mn_varD2, mx_varD2)! ,ghost_zeroed)
  IMPLICIT NONE
  INTEGER, INTENT(IN) :: nlocal, j_in, meth, orderD1, orderD2,max_nxyz, mn_var, mx_var, mn_varD, mx_varD , mn_varD2, mx_varD2
  REAL (pr), DIMENSION (mn_varD :mx_varD , 1:nlocal,dim), INTENT (INOUT) :: du
  REAL (pr), DIMENSION (mn_varD2:mx_varD2, 1:nlocal,dim), INTENT (INOUT) :: d2u
  REAL (pr), DIMENSION (-2*n_diff-1:2*n_diff+1,0:max_nxyz,j_lev,0:n_df,1:3), INTENT (IN) :: wghd_in, wghd2_in
!  LOGICAL , INTENT(INOUT) :: ghost_zeroed(1:j_in) !flag for if level j has had ghost pts zeroed
  INTEGER :: i,k,ix,iy,iz,ixp,ixpm,iyp,iypm,izp,izpm,iD 
  INTEGER :: iD3d
  INTEGER :: idim  , indx_line, indx_line_j_lev, indx_line_db
  REAL(pr)             :: line_u(mn_var:mx_var ,0:max_nxyz) !
  INTEGER(KIND_INT_FLAGS):: line_flags(0:max_nxyz)
  INTEGER              :: line_coords(0:max_nxyz,dim)  ! coordinated of returned points in terms of j_mx
  INTEGER              :: line_deriv_lev(0:max_nxyz)
  INTEGER              :: line_flat_indx(0:max_nxyz)
  TYPE (grid_pt_ptr)   :: line_ptrs(0:max_nxyz)
  INTEGER :: n_active_pts,active_pts(max_nxyz+1)
  INTEGER :: n_active_pts_even,active_pts_even(max_nxyz+1)
  INTEGER :: n_j_above,j_above(max_nxyz+1)
  INTEGER :: ixyz_loc(1:3), ixyz_db(1:3)
  INTEGER :: db_scale !, ie
  INTEGER :: nxyz_j_in(1:dim)  !nxyz at level j_in
  INTEGER :: shift(0:2*dim), ibndr
  INTEGER :: plocal_numlines_indx_backend ! number of active lines indexed in lines_indx_backend
  TYPE (element_of_face_ptr) , DIMENSION(:), POINTER :: plocal_lines_indx_backend !  list of active line faces
  INTEGER :: plocal_lines_indx_current ! current active line to be returned 
  

  db_scale = (2**(j_mx_db-j_in)) !test
  nxyz_j_in = nxyz/(2**(j_lev-j_in))

  !setup shift array to map things back into du
  shift(0) = 0
  DO ibndr = 1,2*dim
    shift(ibndr)  = lv_bnd(2,ibndr,j_in)-lv_bnd(0,ibndr,1)
  END DO

  line_u = 0.0_pr ! make sure the line array is zeroed out
  line_flags = 0  ! make sure the line flags array is zeroed out

  DO idim = 1,dim 

	! Get lines for 3d level <= j_in+1
	CALL get_active_lines_indx( idim, j_in, j_in, j_lev, &
           plocal_lines_indx_backend, plocal_numlines_indx_backend,&
		   plocal_lines_indx_current ) !4/18/05 DG changed from j_in+1 to j_in

	!TEST
	IF(plocal_numlines_indx_backend==0)THEN
		print *,'diff_aux_D1andD2_finest_lev_db() plocal_numlines_indx_backend==0'
		stop
	END IF


 
	! get points on line at 1D level j_in and below

	!ln = 1 !TEST only
	DO WHILE( get_active_line_diff_aux( idim, 0 ,j_in ,j_in  ,max_nxyz ,  &
		mn_var, mx_var , &
		line_u, line_flags,  line_deriv_lev, line_flat_indx, line_ptrs, line_coords, &
		active_pts, n_active_pts, GET_GHOST_PTS , shift, &
        plocal_lines_indx_backend, plocal_numlines_indx_backend,&
		plocal_lines_indx_current )  ) 

		DO i=1,n_active_pts
			indx_line = active_pts(i)
			indx_line_db = indx_line * db_scale !find index relative to db coordinates
            indx_line_j_lev = indx_line * 2**(j_lev-j_in)

            IF(BTEST(line_flags(indx_line),GHOST )) THEN
#ifndef EXPLICIT_GHOST_SWEEP
               IF( idim == 1 .AND. lvl_db(indx_line_db) <= j_in )  line_u( mn_var:mx_var,indx_line) = 0.0_pr
#endif
			! find deriviative if this point has the current deriv_lev
			! (we know from above if that it is not a ghost pt)
			ELSE IF( j_in <= line_deriv_lev(indx_line) ) THEN

			! find deriviative if 
			! deriviative level is greater or equal to j and  phsycal 3d level is j or less
				du( mn_varD :mx_varD , line_flat_indx(indx_line),idim  ) = 0.0_pr
				d2u(mn_varD2:mx_varD2, line_flat_indx(indx_line),idim  ) = 0.0_pr


				!1st deriviatve
				DO ixpm = ix_lh_diff_db(indx_line_db,orderD1,j_in ,idim,meth)%ix_l, &
						ix_lh_diff_db(indx_line_db,orderD1,j_in ,idim,meth)%ix_h

					ixp = (1-prd(idim))*(indx_line+ixpm)+ &
						prd(idim)*MOD(indx_line+ixpm+9*nxyz_j_in(idim),nxyz_j_in(idim))

					du( mn_varD:mx_varD,line_flat_indx(indx_line),idim ) = du( mn_varD:mx_varD,line_flat_indx(indx_line),idim ) + &
						wghd_in(ixpm,indx_line_j_lev,j_in,meth,idim)*line_u( mn_varD:mx_varD, ixp)

				END DO ! ixpm = 

				!2nd deriviative
				DO ixpm = ix_lh_diff_db(indx_line_db,orderD2,j_in ,idim,meth)%ix_l, &
						ix_lh_diff_db(indx_line_db,orderD2,j_in ,idim,meth)%ix_h

					ixp = (1-prd(idim))*(indx_line+ixpm)+ &
						prd(idim)*MOD(indx_line+ixpm+9*nxyz_j_in(idim),nxyz_j_in(idim))

					!2nd deriviative
					d2u( mn_varD2:mx_varD2,line_flat_indx(indx_line),idim ) = d2u( mn_varD2:mx_varD2,line_flat_indx(indx_line),idim ) + &
						wghd2_in(ixpm,indx_line_j_lev,j_in,meth,idim)*line_u( mn_varD2:mx_varD2, ixp)

				END DO ! ixpm = 
			END IF
		END DO ! i=1,n_active_pts

	END DO !ln = 1, nlines


END DO ! idim = 1,dim 

 CALL deallocate_active_lines_indx(plocal_lines_indx_backend)


END SUBROUTINE diff_aux_D1andD2_finest_lev_db


SUBROUTINE diff_diag_aux (max_nxyz, du, wghd_in, j_in, meth, meth1, order, j_eq_j_in)
  IMPLICIT NONE
  INTEGER, INTENT(IN) ::  j_in, meth, meth1, order, max_nxyz
  !  INTEGER, DIMENSION (0:j_lev), INTENT (IN) :: lv_in
  !  INTEGER, DIMENSION (1:4,1:len_nlv), INTENT (IN) :: nlv_in
  REAL (pr), DIMENSION (1:nwlt,dim),   INTENT (INOUT) :: du
  REAL (pr), DIMENSION (-2*n_diff-1:2*n_diff+1,0:max_nxyz,j_lev,0:n_df,1:3), INTENT (IN) :: wghd_in
  LOGICAL  , INTENT (IN) :: j_eq_j_in ! if true we are doing case j == j_in

  INTEGER :: i,k,ix,iy,iz,ixp,ixpm,iyp,iypm,iD,nD,idim 
  INTEGER :: ih,ix_l,ix_h,iy_l,iy_h,l,nxj,nyj,ixl,iyl
  INTEGER :: wlt_type, face_type, j_df, ixyz_loc(1:dim), ii, i_flt, j_h, j_df_h, j
  INTEGER :: face_type_l,face_type_h
  REAL (pr) :: dum

!!$  REAL (pr), DIMENSION (1:nwlt,dim)  :: du_tmp !TMP
!!$!du_tmp = 0.0_pr !TEST
!!$!du = 0.0_pr      !TEST
!!$REAL (pr) :: ZERO_tmp
!!$ZERO_tmp = 1.0D-10

  iD=0
  IF(ORDER == 2) iD=4

!!$!OLD 
!!$  IF(ORDER < 3) THEN
!!$     DO k = lv_in(j_in-1)+1, lv_in(j_in)
!!$        i = nlv_in(1,k)
!!$        ix   = nlv_in(2,k)
!!$        iy   = nlv_in(3,k)
!!$        iz   = nlv_in(4,k)
!!$
!!$        du_tmp(i,1) = wghd_in(0,ix,j_in,meth,1)
!!$        du_tmp(i,2) = wghd_in(0,iy,j_in,meth,2)
!!$        IF(dim==3) du_tmp(i,3) = wghd_in(0,iz,j_in,meth,3)
!!$
!!$     END DO
!!$  ELSE IF(ORDER == 3) THEN
!!$     nD=n_diff
!!$
!!$     DO k = lv_in(j_in-1)+1, lv_in(j_in)
!!$        i = nlv_in(1,k)
!!$        ix   = nlv_in(2,k)
!!$        iy   = nlv_in(3,k)
!!$        iz   = nlv_in(4,k)
!!$
!!$        DO idim = 1,dim
!!$           nxj = mxyz(idim)*2**(j_in-1)
!!$
!!$           ! some weight are zero by definition in [-2nD-1,2nD+1]
!!$           ih   = ix/2**(j_lev-j_in)
!!$           ix_l=(1-prd(idim))*MAX(-2*nD-1,-ih) + prd(idim)*(-2*nD-1)
!!$           ix_h=(1-prd(idim))*MIN(2*nD+1,nxj-ih) + prd(idim)*(2*nD+1)
!!$           du_tmp(i,idim) = 0.0_pr
!!$           DO l = ix_l, ix_h
!!$              ixl = ix+l*2**(j_lev-j_in)
!!$              ixl = (1-prd(idim))*ixl + prd(idim)*MOD(ixl+9*nxyz(idim),nxyz(idim))
!!$              du_tmp(i,idim) = du_tmp(i,idim)+ wghd_in(l,ix,j_in,meth,idim)*wghd_in(-l,ixl,j_in,meth1,idim)
!!$           END DO
!!$        END DO
!!$
!!$     END DO
!!$  END IF
!!$!END OLD

  !new version
  !
  ! if j_eq_j_in == FALSE then we are doing diag terms for j_in == j deriviative level points
  ! if j_eq_j_in == TRUE  then we are doing diag terms for points with deriviatve level >= j_in and 3D level <= j_in
  !
  j_h    = j_lev
  j_df_h = j_in
  face_type_l = 0
  face_type_h = 3**dim - 1
  IF( j_eq_j_in )  THEN
     j_h    = j_in ! for case where j == j_in in calling routine
     j_df_h = j_lev
  END IF
  DO j = 1, j_h
     DO wlt_type = MIN(j-1,1),2**dim-1
        DO face_type = face_type_l, face_type_h           
           DO j_df=j_in,j_df_h
              DO ii = 1, indx_DB(j_df,wlt_type,face_type,j)%length
                 i_flt = indx_DB(j_df,wlt_type,face_type,j)%p(ii )%i !flat indx
                 CALL coord_map_ixyz( indx_DB(j_df,wlt_type,face_type,j)%p(ii )%ixyz , ixyz_loc )
                 !TEST PRINT *,i_flt, ixyz_loc, j_df,MAXVAL(lvl_db(ixyz_loc(1:dim)*(2**(j_mx_db-j_lev)))), 'new ixyz_loc '

                 IF(ORDER < 3) THEN
                    du(i_flt,1) = wghd_in(0,ixyz_loc(1),j_in,meth,1)
                    du(i_flt,2) = wghd_in(0,ixyz_loc(2),j_in,meth,2)
                    IF(dim==3) du(i_flt,3) = wghd_in(0,ixyz_loc(3),j_in,meth,3)
                 ELSE IF(ORDER == 3) THEN
                    nD=n_diff

                    DO idim = 1,dim
                       nxj = mxyz(idim)*2**(j_in-1)

                       ! some weight are zero by definition in [-2nD-1,2nD+1]
                       ih   = ixyz_loc(idim)/2**(j_lev-j_in)                             !<<fixed
                       ix_l=(1-prd(idim))*MAX(-2*nD-1,-ih) + prd(idim)*(-2*nD-1)
                       ix_h=(1-prd(idim))*MIN(2*nD+1,nxj-ih) + prd(idim)*(2*nD+1)
                       du(i_flt,idim) = 0.0_pr
                       DO l = ix_l, ix_h
                          ixl = ixyz_loc(idim)+l*2**(j_lev-j_in) !<<fixed
                          ixl = (1-prd(idim))*ixl + prd(idim)*MOD(ixl+9*nxyz(idim),nxyz(idim))
                          du(i_flt,idim) = du(i_flt,idim)+ &
                               wghd_in(l,ixyz_loc(idim),j_in,meth,idim)*&               !<<fixed
                               wghd_in(-l,ixl,j_in,meth1,idim)
                       END DO
                    END DO
                 END IF

!!$                 !TEST it matches
!!$                 DO idim = 1, dim
!!$                    IF( ABS(du(i_flt,idim) - du_tmp(i_flt,idim)) > ZERO_tmp )THEN
!!$                       PRINT *,'ERROR ',i_flt, idim,  du(i_flt,idim), du_tmp(i_flt,idim),&
!!$                            ABS(du(i_flt,idim) - du_tmp(i_flt,idim)),ZERO_tmp
!!$                       stop
!!$                    END IF
!!$                 END DO

              END DO
           END DO
        END DO
     END DO
  END DO



!!$  !TEST matches old
!!$
!!$  DO k = lv_in(j_in-1)+1, lv_in(j_in)
!!$     i = nlv_in(1,k)
!!$     ix   = nlv_in(2,k)
!!$     iy   = nlv_in(3,k)
!!$     iz   = nlv_in(4,k)
!!$     DO idim = 1, dim
!!$        IF( ABS(du(i,idim) - du_tmp(i,idim)) > ZERO_tmp )THEN
!!$           PRINT *,'ERROR2 ',i, idim,  du(i,idim), du_tmp(i,idim)
!!$
!!$
!!$           DO j = 1, j_lev
!!$              DO wlt_type = MIN(j-1,1),2**dim-1
!!$                 DO face_type = 0, 3**dim - 1           
!!$                    DO j_df=1,j_lev
!!$                       DO ii = 1, indx_DB(j_df,wlt_type,face_type,j)%length
!!$
!!$                          IF(  indx_DB(j_df,wlt_type,face_type,j)%p(ii )%i  == i ) THEN
!!$                             PRINT *,'type ', wlt_type,face_type,j_df,j
!!$                          END IF
!!$                       END DO
!!$                    END DO
!!$                 END DO
!!$              END DO
!!$           END DO
!!$
!!$           stop
!!$        END IF
!!$     END DO
!!$  END DO


END SUBROUTINE diff_diag_aux

!
!
!
! apply the weights to the wlt coefficients to calculate a derivative.
!
SUBROUTINE diff_diag_bnd_aux (max_nxyz, du, wghd_in, wghd2_in, j_in, meth,j_eq_j_in)
  IMPLICIT NONE
  INTEGER, INTENT(IN) ::  j_in, meth,max_nxyz
  !  INTEGER, DIMENSION (0:j_lev), INTENT (IN) :: lv_in
  !  INTEGER, DIMENSION (1:4,1:len_nlv), INTENT (IN) :: nlv_in
  REAL (pr), DIMENSION (1:nwlt,dim),   INTENT (INOUT) :: du
  LOGICAL  , INTENT (IN) :: j_eq_j_in ! if true we are doing case j == j_in

  REAL (pr), DIMENSION (-2*n_diff-1:2*n_diff+1,0:max_nxyz,j_lev,0:n_df,1:3), INTENT (IN) :: wghd_in, wghd2_in
  INTEGER :: i,k,ixp,ixpm,iyp,iypm,iD,nD,idim 
  INTEGER :: ih,ix_l,ix_h,iy_l,iy_h,l,nxj,nyj,ixl,iyl
  !  INTEGER ::ixyz(3)
  REAL (pr) :: dum

  INTEGER :: wlt_type, face_type, j_df, ixyz_loc(1:dim), ii, i_flt, j_h, j_df_h, j
  INTEGER :: face_type_l,face_type_h


!!$  REAL (pr), DIMENSION (1:nwlt,dim)  :: du_tmp !TMP
!!$REAL (pr) :: ZERO
!!$ZERO = 1.0D-10
!!$!OLD
!!$  DO k = lv_in(j_in-1)+1, lv_in(j_in)
!!$     i = nlv_in(1,k)
!!$     ixyz_loc(1:3) = nlv_in(2:4,k)
!!$
!!$     DO idim = 1,dim
!!$        IF(ixyz_loc(idim) > 0 .AND. ixyz_loc(idim) < nxyz(idim)) THEN
!!$           ix_l=MIN(ixyz_loc(idim)/2**(j_lev-j_in),2*n_diff+1)
!!$           ix_h=MIN((nxyz(idim)-ixyz_loc(idim))/2**(j_lev-j_in),2*n_diff+1)
!!$           du_tmp(i,idim) = -wghd2_in(-ix_l,ixyz_loc(idim),j_in,meth,idim)* &
!!$                wghd_in(ix_l,0,j_in,meth,idim)/wghd_in(0,0,j_in,meth,idim) &
!!$                -wghd2_in(ix_h,ixyz_loc(idim),j_in,meth,idim)* &
!!$                wghd_in(-ix_h,nxyz(idim),j_in,meth,idim)/wghd_in(0,nxyz(idim),j_in,meth,idim)
!!$        ELSE
!!$           du_tmp(i,idim) = 0.0_pr
!!$        END IF
!!$     END DO
!!$
!!$  END DO
!!$!END OLD


  !new version
  !
  ! if j_eq_j_in == FALSE then we are doing diag terms for j_in == j deriviative level points
  ! if j_eq_j_in == TRUE  then we are doing diag terms for points with deriviatve level >= j_in and 3D level <= j_in
  !
  j_h    = j_lev
  j_df_h = j_in
  face_type_l = 0
  face_type_h = 3**dim - 1
  IF( j_eq_j_in )  THEN
     j_h    = j_in ! for case where j == j_in in calling routine
     j_df_h = j_lev
     !face_type_l = face_type_internal
     !face_type_h = face_type_internal
  END IF
  DO j = 1, j_h
     DO wlt_type = MIN(j-1,1),2**dim-1
        DO face_type = face_type_l, face_type_h           
           DO j_df=j_in,j_df_h
              DO ii = 1, indx_DB(j_df,wlt_type,face_type,j)%length
                 i_flt = indx_DB(j_df,wlt_type,face_type,j)%p(ii )%i !flat indx
                 CALL coord_map_ixyz( indx_DB(j_df,wlt_type,face_type,j)%p(ii )%ixyz , ixyz_loc )
                 !TEST PRINT *,i_flt, ixyz_loc, j_df,MAXVAL(lvl_db(ixyz_loc(1:dim)*(2**(j_mx_db-j_lev)))), 'new ixyz_loc '



                 DO idim = 1,dim
                    IF(ixyz_loc(idim) > 0 .AND. ixyz_loc(idim) < nxyz(idim)) THEN
                       ix_l=MIN(ixyz_loc(idim)/2**(j_lev-j_in),2*n_diff+1)
                       ix_h=MIN((nxyz(idim)-ixyz_loc(idim))/2**(j_lev-j_in),2*n_diff+1)
                       du(i_flt,idim) = -wghd2_in(-ix_l,ixyz_loc(idim),j_in,meth,idim)* &
                            wghd_in(ix_l,0,j_in,meth,idim)/wghd_in(0,0,j_in,meth,idim) &
                            -wghd2_in(ix_h,ixyz_loc(idim),j_in,meth,idim)* &
                            wghd_in(-ix_h,nxyz(idim),j_in,meth,idim)/wghd_in(0,nxyz(idim),j_in,meth,idim)
                    ELSE
                       du(i_flt,idim) = 0.0_pr
                    END IF
                 END DO



!!$                 !TEST it matches
!!$                 DO idim = 1, dim
!!$                    IF( ABS(du(i_flt,idim) - du_tmp(i_flt,idim)) > ZERO )THEN
!!$                       PRINT *,'diff_diag_bnd_aux ERROR ',i_flt, idim,  du(i_flt,idim), du_tmp(i_flt,idim)
!!$                       stop
!!$                    END IF
!!$                 END DO

              END DO
           END DO
        END DO
     END DO
  END DO



!!$  !TEST matches old
!!$
!!$  DO k = lv_in(j_in-1)+1, lv_in(j_in)
!!$     i = nlv_in(1,k)
!!$     !     ixyz_loc(1:3) = nlv_in(2:4,k)
!!$     DO idim = 1, dim
!!$        IF( ABS(du(i,idim) - du_tmp(i,idim)) > ZERO )THEN
!!$           PRINT *,'diff_diag_bnd_aux ERROR2 ',i, idim,  du(i,idim), du_tmp(i,idim)
!!$           stop
!!$        END IF
!!$     END DO
!!$  END DO
END SUBROUTINE diff_diag_bnd_aux


!
!**************************************************************************
! type = 0 - regular transform
! type = 1 - internal transform and along boundaries  
!
SUBROUTINE wlt_interpolate(u, ie,  mn_var,mx_var, j_in, j_out, nlocal, wlt_fmly, trnsf_type)
  IMPLICIT NONE
  INTEGER, INTENT(IN) :: nlocal, j_in, j_out, trnsf_type,ie, mn_var,mx_var,wlt_fmly
  REAL (pr), DIMENSION (1:nlocal,ie), INTENT (INOUT) :: u
  REAL (pr) :: c_predict
  INTEGER , DIMENSION(6)::  ibch
  INTEGER :: i,j,k,kk
  INTEGER :: ix,nx_l,nx_h,ix_even,ix_odd,ixp,ixpm,ix_l,ix_h
  INTEGER :: iy,ny_l,ny_h,iy_even,iy_odd,iyp,iypm,iy_l,iy_h
  INTEGER :: iz,nz_l,nz_h,iz_even,iz_odd,izp,izpm,iz_l,iz_h
  INTEGER :: ibndr, istep
  INTEGER :: ishift,jmin,jmax,idim

  !
  ! Currently only wlt_fmly == 1 is supported in lines
  !
  IF( wlt_fmly /= 1 ) THEN
     WRITE(*,'(A,I3)') "wlt_interpolate(), called with wlt_fmly = ", wlt_fmly
	 WRITE(*,'(A)') "Only wlt_fmly == 1 is supported in lines currently,"
     WRITE(*,'(A)') "Temporarily force use of wlt_fmly == 1 for this call"
  END IF

  !
  ! We access the db directly through ptrs here to load the db and then zero out
  ! some entries by level
  !  
  ibch = 0
  IF(trnsf_type == 1) ibch = ibc


  !
  ! Fill data into the db
  !
  CALL update_db_from_u_interpolate(  u , nwlt , ie, mn_var, mx_var, j_in, j_out )

  jmin = MIN(j_in,j_out)
  jmax = MAX(j_in,j_out)
!
!---------- forward transform -----------------------------
!
  DO j = j_in-1, jmin, -1
			         
		call c_wlt_trns_aux_db(nlocal,  mn_var, mx_var, MAXVAL(mxyz(1:dim)*2**(j_lev-1)) , & !MAXVAL(nxyz), & !
              j, j_lev, &
             n_prdct, n_updt ,trnsf_type, jd, prd, WLT_TRNS_FWD )
  END DO

!
!----------------  inverse transform ----------------------
!
  DO j = jmin, j_out-1

		call c_wlt_trns_aux_db(nlocal,  mn_var, mx_var, MAXVAL(mxyz(1:dim)*2**(j_lev-1)) , & !MAXVAL(nxyz), & !
              j, j_lev, &
             n_prdct, n_updt ,trnsf_type, jd, prd, WLT_TRNS_INV )

  END DO

  

  !
  ! update u with result from db
  !
  CALL update_u_from_db_interpolate(u , nwlt , ie, mn_var, mx_var, j_in, j_out )



END SUBROUTINE wlt_interpolate


SUBROUTINE indices_db (nwlt_old, nwlt, nwltj_old, nwltj, new_grid, j_lev_old, j_lev_new, j_mn, j_mx, ireg)
  IMPLICIT NONE
  INTEGER, INTENT (IN) :: j_mn, j_mx, ireg
  INTEGER, INTENT (INOUT) :: j_lev_new, j_lev_old, nwlt, nwlt_old, nwltj, nwltj_old
  LOGICAL, INTENT (INOUT) :: new_grid
  LOGICAL :: lchk,check1,check2
  INTEGER :: i,ii,ih,j,k,l,meth,nD,trnsf_type,iup,idwn
  INTEGER :: ix,ix_l,ix_h,ixpm,ix_even,ix_odd
  INTEGER :: iy,iy_l,iy_h
  INTEGER :: iz,iz_l,iz_h
  INTEGER :: jxyz(3), ixyz(3), ixyz2
  INTEGER , DIMENSION(6)::  ibch
  INTEGER :: iuph, idwnh
  INTEGER :: idim
  REAL (pr) :: dxyz(dim)
  LOGICAL , SAVE :: start_up = .TRUE.


 ! 
 ! Things that get done once at startup that do not depend on if we are doing a restart
 !
 IF( start_up ) THEN
    start_up = .FALSE.
    IF (ALLOCATED (face_type_map)) DEALLOCATE (face_type_map)
    ALLOCATE(face_type_map(0:3**dim-1) ) 
 END IF
 
   
 IF (ireg == -1 ) THEN !Restart version
    
    CALL init_lines_db(nwlt, j_mn , j_mx, restart=.TRUE.  ) 
    
    nwltj=2*nwlt
    
    IF (ALLOCATED(Nwlt_lev)) DEALLOCATE (Nwlt_lev)
    ALLOCATE(Nwlt_lev(1:j_mx,0:1)) ! allocates only once for all levels up to j_mx

    IF (ALLOCATED(x)) DEALLOCATE (x)
    ALLOCATE(x(1:nwlt,1:dim))
    IF (ALLOCATED(h)) DEALLOCATE (h)
    ALLOCATE(h(1:j_lev,1:dim))
    IF (ALLOCATED(wgh_df)) DEALLOCATE (wgh_df)
    ALLOCATE(wgh_df(-2*n_diff-1:2*n_diff+1,0:MAXVAL(nxyz),j_lev,0:n_df,1:3))
    IF (ALLOCATED(wgh_d2f)) DEALLOCATE (wgh_d2f)
    ALLOCATE(wgh_d2f(-2*n_diff-1:2*n_diff+1,0:MAXVAL(nxyz),j_lev,0:n_df,1:3) )
    !now done in create_indices_from_indx_DB()   ALLOCATE(lv_intrnl(0:j_lev),lv_bnd(0:3,1:2*dim,0:j_lev)) !changed
    
    !OLD	ALLOCATE(nlv(1:4,1:nwlt) )
    IF (ALLOCATED(lv)) DEALLOCATE (lv)
    ALLOCATE(lv(0:j_mx))
    IF (ALLOCATED(lvj)) DEALLOCATE (lvj)
    ALLOCATE(lvj(0:j_mx)) 


    wgh_df = 0.0_pr; wgh_d2f = 0.0_pr
    x=0.0_pr;  h = 0.0_pr
    nwlt_old=0
    j_lev_old=0
    j_lev_new=j_lev
    !DG CHANGE TO RESTART WITH DIFFERENT Mxyz:   j_lev_old=j_lev ! here j_lev  is the j_lev read from the restart file. DG
 
    nwltj_old =0


 ELSE IF (ireg == 0) THEN !first initialization call
    j_lev = MAX(j_mn,j_lev_init) ! set current wavelet transform level to atleast minimum level
    j_lev = MIN( j_lev, j_mx ) ! set current wavelet transform level to below the max level
   
    CALL init_lines_db(nwlt, j_mn , j_mx, restart=.FALSE.  ) 

    nwltj=2*nwlt 
    nxyz(1:2)=mxyz(1:2)*2**(j_lev-1) !calculate non-adaptive resolution from levels and M
    nxyz(3)=mxyz(3)*2**(jd*(j_lev-1)) !calculate non-adaptive resolution from levels and M
    maxval_nxyz = MAXVAL(nxyz)
    IF (ALLOCATED(Nwlt_lev)) DEALLOCATE (Nwlt_lev)
    ALLOCATE(Nwlt_lev(1:j_mx,0:1)) ! allocates only once for all levels up to j_mx
    !OLD    ALLOCATE(nlv(1:4,1:nwlt) )
    IF (ALLOCATED(lv)) DEALLOCATE (lv)
    ALLOCATE(lv(0:j_mx))
    IF (ALLOCATED(lvj)) DEALLOCATE (lvj)
    ALLOCATE(lvj(0:j_mx)) 
    IF (ALLOCATED(lv_intrnl)) DEALLOCATE (lv_intrnl)
    IF (ALLOCATED(lv_bnd)) DEALLOCATE (lv_bnd)
    ALLOCATE(lv_intrnl(0:j_lev),lv_bnd(0:3,1:2*dim,0:j_lev)) !changed
    IF (ALLOCATED(xx)) DEALLOCATE (xx)
    IF (ALLOCATED(h)) DEALLOCATE (h)
    ALLOCATE(xx(0:MAXVAL(nxyz),1:dim),h(1:j_lev,1:dim) )
    IF (ALLOCATED(wgh_df)) DEALLOCATE (wgh_df)
    ALLOCATE(wgh_df(-2*n_diff-1:2*n_diff+1,0:MAXVAL(nxyz),j_lev,0:n_df,1:3))
    IF (ALLOCATED(wgh_d2f)) DEALLOCATE (wgh_d2f)
    ALLOCATE(wgh_d2f(-2*n_diff-1:2*n_diff+1,0:MAXVAL(nxyz),j_lev,0:n_df,1:3) )
    wgh_df = 0.0_pr; wgh_d2f = 0.0_pr

    xx=0.0_pr;  h = 0.0_pr
    nwlt_old=0
    j_lev_old=0
    j_lev_new=j_lev
 ELSE
    j_lev = j_lev_new

!OLD    IF(nwlt /= nwlt_old ) then
!OLD        IF(allocated(nlv)) DEALLOCATE(nlv)
!OLD		ALLOCATE(nlv(1:4,1:nwlt))
!OLD    END IF



    IF( j_lev /= j_lev_old ) then
       IF(allocated(lv_intrnl)) DEALLOCATE(lv_intrnl) 
       IF(allocated(lv_bnd)) DEALLOCATE(lv_bnd) 
       ALLOCATE(lv_intrnl(0:j_lev),lv_bnd(0:3,1:2*dim,0:j_lev)) 
    END IF
 END IF
 
 

!
!-------------- mesh reassignment ---------------------------
!
 IF(j_lev_old /= j_lev .OR. ireg == 0) THEN 
    nxyz(1:2)=mxyz(1:2)*2**(j_lev-1)
    nxyz(3)=mxyz(3)*2**(jd*(j_lev-1))
	maxval_nxyz = MAXVAL(nxyz)

	CALL setup_coord_mapping(nxyz,dim)

    !
    IF (ALLOCATED(xx)) DEALLOCATE(xx)
    IF (ALLOCATED(h)) DEALLOCATE(h)
    IF (ALLOCATED(wgh_df)) DEALLOCATE(wgh_df)
    IF (ALLOCATED(wgh_d2f)) DEALLOCATE(wgh_d2f) 
    ALLOCATE(xx(0:MAXVAL(nxyz),1:dim),h(1:j_lev,1:dim) )
    ALLOCATE(wgh_df(-2*n_diff-1:2*n_diff+1,0:MAXVAL(nxyz),j_lev,0:n_df,1:3))
    ALLOCATE(wgh_d2f(-2*n_diff-1:2*n_diff+1,0:MAXVAL(nxyz),j_lev,0:n_df,1:3) )
    wgh_df = 0.0_pr; wgh_d2f = 0.0_pr
    xx=0.0_pr;  h = 0.0_pr; lv =0

    ! calculate dx and xx for each dim
    ! dx is delta x and xx(i,1), xx(i,2), xx(i,3) is the real space coordinates of point i
    DO idim = 1,dim
       IF(grid(idim) == 0) THEN !regular grid
          dxyz(idim)=(xyzlimits(2,idim)-xyzlimits(1,idim))/REAL(nxyz(idim),pr)
          DO i=0,nxyz(idim)
             xx(i,idim)=xyzlimits(1,idim)+dxyz(idim)*REAL(i,pr)
          END DO
       ELSE !chebechev grid
          dxyz(idim)=0.5_pr*(xyzlimits(2,idim)-xyzlimits(1,idim))
          DO i=0,nxyz(idim)
             xx(i,idim)=xyzlimits(1,idim)+dxyz(idim)*(1.0_pr-COS(pi&
                  &*REAL(i,pr)/REAL(nxyz(idim),pr)) )
          END DO
       END IF
    END DO


    ! real space distance to the next point on the next coarser level
    DO i=1,dim
       DO j=1,j_lev
          h(j,i)=(xyzlimits(2,i)-xyzlimits(1,i))/REAL(mxyz(i),pr)/2.0e0_pr**(j-1)
       END DO
    END DO


    !
    !-------- setting weights for derivatives calculation
    !
    DO meth =0, n_df
       IF(meth == 0) THEN 
          nD=1
       ELSE IF(meth == 1) THEN 
          nD=n_diff
       ELSE IF(meth == 2) THEN 
          nD=1
       ELSE IF(meth == 3) THEN 
          nD=n_diff
       ELSE IF(meth == 4) THEN 
          nD=1
       ELSE IF(meth == 5) THEN 
          nD=n_diff
       END IF
       DO j = 1, j_lev
          DO idim =1,dim
             DO ih = 0,  mxyz(idim)*2**(j-1)-prd(idim)
                ix=ih*2**(j_lev-j)
                ix_l =-MIN(ih,nD)
                ix_h = ix_l+2*nD
                ix_h = MIN(ix_h,  mxyz(idim)*2**(j-1)-ih)
                ix_l=ix_h-2*nD
                ix_l=(1-prd(idim))*MAX(MIN(ix_l,0),-ih) + prd(idim)*(-nD)
                ix_h=(1-prd(idim))*MIN(MAX(ix_h,0),mxyz(idim)*2**(j-1)-ih) + prd(idim)*(nD)
                IF       (ix_l+ix_h == 0) THEN                                        ! the middle of the domain
                   ix_l = ix_l+forward_bias(meth)
                   ix_h = ix_h-backward_bias(meth)
                ELSE IF  (ix_l+ix_h < 0 .AND. backward_bias(meth)==1 .AND. ix_h > 0) THEN    ! the right boundary
                   ix_l = MAX(ix_l,-ix_h-nd_assym(meth),-ih)
                ELSE IF  (ix_l+ix_h < 0 .AND. forward_bias(meth)==1 .AND. ix_h > 0) THEN  !  the right boundary
                   ix_l = MAX(-ix_h+1,-ih)
                ELSE IF  (ix_l+ix_h < 0 .AND. ix_h == 0) THEN                         ! right on the right boundary
                   ix_l = MAX(ix_l,-nd_assym_bnd(meth),-ih)
                ELSE IF  (ix_l+ix_h > 0 .AND. backward_bias(meth)==1 .AND. ix_l < 0) THEN    !  the left boundary
                   ix_h = MIN(-ix_l-1,mxyz(idim)*2**(j-1)-ih)
                ELSE IF  (ix_l+ix_h > 0 .AND. forward_bias(meth)==1 .AND. ix_l < 0 ) THEN !  the left boundary
                   ix_h = MIN(ix_h,-ix_l+nd_assym(meth),mxyz(idim)*2**(j-1)-ih)
                ELSE IF  (ix_l+ix_h > 0 .AND. ix_l == 0 ) THEN                        ! right on the left boundary
                   ix_h = MIN(ix_h,nd_assym_bnd(meth),mxyz(idim)*2**(j-1)-ih)             !comment if do not want reduce the order of derivative 
                END IF
                DO ixpm = ix_l, ix_h
                   wgh_df(ixpm,ix,j,meth,idim) = &
                        diff_wgh(ixpm, ix, ix_l, ix_h, 2**(j_lev-j), prd(idim), nxyz(idim), xx(0:nxyz(idim),idim), h(j,idim), 1)
                END DO
             END DO
          END DO
          DO idim =1,dim
             DO ih = 0,  mxyz(idim)*2**(j-1)-prd(idim)
                ix=ih*2**(j_lev-j)
                ix_l =-MIN(ih,nD)
                ix_h = ix_l+2*nD
                ix_h = MIN(ix_h,  mxyz(idim)*2**(j-1)-ih)
                ix_l=ix_h-2*nD
                IF (ix_l > -nD) ix_h = ix_h+1
                IF (ix_h <  nD) ix_l = ix_l-1
                ix_l=(1-prd(idim))*MAX(ix_l,-ih) + prd(idim)*(-nD)
                ix_h=(1-prd(idim))*MIN(ix_h,mxyz(idim)*2**(j-1)-ih) + prd(idim)*(nD)
                IF(ix_l+ix_h<0) ix_l = MAX(ix_l,-ix_h-nd2_assym(meth))
                IF(ix_l+ix_h>0) ix_h = MIN(ix_h,-ix_l+nd2_assym(meth))
                IF(ix_l+ix_h < 0 .AND. ix_h == 0 ) ix_l = MAX(ix_l,-nd2_assym_bnd(meth),-ih)
                IF(ix_l+ix_h > 0 .AND. ix_l == 0 ) ix_h = MIN(ix_h,nd2_assym_bnd(meth),mxyz(idim)*2**(j-1)-ih)
                DO ixpm = ix_l, ix_h
                   wgh_d2f(ixpm,ix,j,meth,idim) = &
                        diff_wgh(ixpm, ix, ix_l, ix_h, 2**(j_lev-j), prd(idim), nxyz(idim), xx(0:nxyz(idim),idim), h(j,idim), 2)
                END DO
             END DO
          END DO
       END DO
    END DO
 END IF
 


  IF(nwlt == nwlt_old ) then
	new_grid = .FALSE.
  ELSE
     !UNUSED FOR NOW IF( ALLOCATED(indx_list_DB_array) )  DEALLOCATE(indx_list_DB_array)
	 !UNUSED FOR NOW  ALLOCATE(indx_list_DB_array(1:nwlt))

  END IF
  nwlt_old = nwlt




END SUBROUTINE indices_db

!
! Initialize new Field data base
!
SUBROUTINE init_DB (nwlt_old, nwlt, nwltj_old, nwltj, new_grid,&
          & j_lev_old, j_lev_a, j_mn, j_mx, max_nxyz, nxyz_a, IC_from_restart_file)
  IMPLICIT NONE
  INTEGER, INTENT (IN) :: j_mn, j_mx, max_nxyz, nxyz_a(1:3) 
  INTEGER, INTENT (INOUT) :: j_lev_old, nwlt, nwlt_old, nwltj, nwltj_old, j_lev_a
  LOGICAL, INTENT (INOUT) :: new_grid
  LOGICAL, INTENT (IN)    ::  IC_from_restart_file ! true if restart or IC from restart file
  INTEGER :: wlt_fmly, trnsf_type, ireg, j_mn_tmp
  INTEGER :: i !TEST

  REAL(pr) :: u_tmp(1:nwlt,1) !used with restart call to update_u_from_db

	ireg=0
	IF(IC_from_restart_file ) ireg=-1

    !
	! If we are restarting we use j_mn as the lowest level instead of j_mn_init.
	!
	IF((IC_restart .OR. (IC_from_file .AND. IC_file_fmt == 0)) ) THEN
		j_mn_tmp = j_mn
	ELSE
		j_mn_tmp = j_mn_init
	END IF
		 
 


	!
    ! Initialize the field data base
    !
    !moved below.. D. G. CALL init_lines_db(j_mn_tmp , j_mx, IC_from_restart_file  ) ! 


! done in init_lines_db() DG.
!    IF(.NOT. IC_from_restart_file ) THEN
!		nwlt = db_nwlt
!		j_lev = j_mn_tmp + 1 ! set current wavelet transform level to atleast minimum level ! 
!		j_lev = MIN( j_lev, j_mx ) ! set current wavelet transform level to below the max level
!	END IF

    nxyz(1:2)=mxyz(1:2)*2**(j_lev-1) !calculate non-adaptive resolution from levels and M
    nxyz(3)=mxyz(3)*2**(jd*(j_lev-1)) !calculate non-adaptive resolution from levels and M
    maxval_nxyz = MAXVAL(nxyz)
    
    
    IF (ALLOCATED(ix_lh_db)) DEALLOCATE (ix_lh_db)
    ALLOCATE( ix_lh_db(0:MAXVAL(mxyz)*2**(j_mx-1),1:dim,j_mx,0:1) )

    !
    ! This routine creates internal indices from the indx_DB stucture that was
	! read from the restart file if we are doign a restart of a new run with 
	! a restart file as the IC.
    !
	IF( IC_from_restart_file )  Call create_indices_from_indx_DB()

	!
    ! Initialize the field data base
    !
    !CALL init_lines_db(nwlt, j_mn_tmp , j_mx, IC_from_restart_file  ) ! 

	CALL indices_db (nwlt_old, nwlt, nwltj_old, nwltj, new_grid,&
		& j_lev_old, j_lev_a, j_mn_tmp, j_mx, ireg)

    wlt_fmly = 1 ! NOTE NEED TO MAKE WORK FOR ALL WLT FAMILIES, 4/25/06 DG
	CALL accel_indices_db( ix_lh_db(:,:,:,:) , & 
		mxyz,n_prdct(wlt_fmly),n_updt(wlt_fmly),&
		n_assym_prdct(wlt_fmly,:),n_assym_updt(wlt_fmly,:),&
		n_assym_prdct_bnd(wlt_fmly,:),n_assym_updt_bnd(wlt_fmly,:),& 
		prd,ibc,jd, MAXVAL(mxyz)*2**(j_mx-1)) 

	!
	! If not restarting
	!
	not_IC_restart: IF( .NOT. IC_from_restart_file )  THEN

		!
		!Allocate field variables
		!
                IF (ALLOCATED(u)) DEALLOCATE (u)
                IF (ALLOCATED(f)) DEALLOCATE (f)
		ALLOCATE (  u(1:nwlt,1:n_var),f(1:nwlt,1:n_integrated))
		f = 0.0_pr ; u = 0.0_pr
		IF(n_var_exact > 0) THEN
                        IF (ALLOCATED(u_ex)) DEALLOCATE (u_ex)
			ALLOCATE ( u_ex(1:nwlt,1:n_var_exact) )
			u_ex = 0.0_pr
		END IF




		!
		! first call after init_field is always update_u_from_db
		! to initialize all internal indices
		! I
		CALL update_u_from_db( u ,nwlt ,n_var ,1 ,n_var, 1, j_mx, j_lev, MAXVAL(nxyz))

    ELSE 
		!
		! first call after init_field is always update_u_from_db
		! to initialize all internal indices
		! In the case of restart we call update_u_from_db() with n_var_min/max = 0 because the correct
		! field variables are already in u
		!
		CALL update_u_from_db( u_tmp ,nwlt ,n_var ,1 ,1, 1, j_mx, j_lev, MAXVAL(nxyz))
 
        !
		! Now get the correct values of u into the db
		!
		CALL update_db_from_u(  u , nwlt ,n_var  , 1, n_var, 1  )

		!TEST
!should this still be here D.G. ?
		CALL update_u_from_db_noidices(  u , nwlt ,n_var  , 1, n_var, 1 )
        !debug
		PRINT *,'In init_DB, nwlt = ', nwlt
        PRINT *,'Field var stats, In init_DB'
        DO i=1,n_var
           WRITE(*,'(" MINMAX(u_i) ", A, 2(es15.8,1X) )' ) u_variable_names(i), MINVAL(u(:,i)),MAXVAL(u(:,i))
	       WRITE(*,'(" ")' )
        END DO
        !end debug 

		
	END IF not_IC_restart 

END SUBROUTINE init_DB

SUBROUTINE weights
!--Calculates weights for volume integration based on the Trapezoid
!--integration rule (second-order accurate).
IMPLICIT NONE

INTEGER :: i, ii, iii, j, k, m, ip, im, idim, odim(1:dim-1)
INTEGER, DIMENSION(dim) :: ixyz
INTEGER, DIMENSION(3,2) :: i_k
REAL (pr), DIMENSION(dim) :: lxyz
REAL (pr) :: shft(dim,2), dAh
INTEGER :: i1(1), j_df, wlt_type, face_type, dir_type, step, step2
INTEGER, DIMENSION(0:dim) ::  i_p_cube
INTEGER, DIMENSION(dim) ::  dir, ixyz1,  i_l, i_h
INTEGER, DIMENSION(dim,1) :: ZERO
REAL(pr), DIMENSION(1:j_lev) :: dA_j ! area associated with a ppint of level j if it had no neighbors
INTEGER, DIMENSION(dim) :: wlt, face, ivec
INTEGER, DIMENSION(0:dim) :: i_p, i_p_face, i_p_wlt

INTEGER :: ixyz_loc(3), ixyz_db(3)
REAL(pr)             :: line_u(1:1 ,0:maxval_nxyz) !

INTEGER(KIND_INT_FLAGS):: line_flags(0:maxval_nxyz)
INTEGER              :: line_coords(0:maxval_nxyz,dim)  ! coordinated of returned points in terms of j_mx
INTEGER              :: line_deriv_lev(0:maxval_nxyz)
INTEGER              :: line_flat_indx(0:maxval_nxyz)
TYPE (grid_pt_ptr)   :: line_ptrs(0:maxval_nxyz)
INTEGER :: n_active_pts,active_pts(maxval_nxyz+1), indx_line, indx_flat
INTEGER :: db_scale , j_curr_3d, j_line
REAL(PR) :: s
  INTEGER :: plocal_numlines_indx_backend ! number of active lines indexed in lines_indx_backend
  TYPE (element_of_face_ptr) , DIMENSION(:), POINTER :: plocal_lines_indx_backend !  list of active line faces
  INTEGER :: plocal_lines_indx_current ! current active line to be returned 



IF( Weights_Meth ==   0  ) THEN ! # Weights_Meth, 0: dA=area/nwlt, 1: normal
   WRITE(*,'(A)')  " "
   WRITE(*,'(A)')  "************************************************************"
   WRITE(*,'(A,I3)')  'Setting dA = area/nwlt for Weights Method = ',Weights_Meth
   WRITE(*,'(A)')  "************************************************************"
   WRITE(*,'(A)')  " "
   dA = PRODUCT(xyzlimits(2,1:dim)- xyzlimits(1,1:dim))/nwlt
   dA_level = 0.0_pr
   DO j = 1, j_lev
      dA_level(1:Nwlt_lev(j,1),j) = PRODUCT(xyzlimits(2,1:dim)- xyzlimits(1,1:dim))/Nwlt_lev(j,1)
   END DO 
ELSE

   WRITE(*,'(A)')  " "
   WRITE(*,'(A)')  "************************************************************"
   WRITE(*,'(A)')  "dA_level currently not implemented in db_lines "
   WRITE(*,'(A)')  "************************************************************"
   WRITE(*,'(A)')  " "
   ! until dA_level is implemeneted in db_lines we just set it as in Weights_Meth == 0 case.
 
   dA_level = 0.0_pr
   DO j = 1, j_lev
      dA_level(1:Nwlt_lev(j,1),j) = PRODUCT(xyzlimits(2,1:dim)- xyzlimits(1,1:dim))/Nwlt_lev(j,1)
   END DO 

db_scale = (2**(j_mx_db-j_lev))

ZERO = 0
dA = 0.0_pr ! zero out the glabal dA array
i_k = 0
lxyz(:) = xyzlimits(2,:)-xyzlimits(1,:)
i_p(0) = 1
i_p_cube(0) = 1
i_p_face(0) = 1
DO i=1,dim
   i_p(i) = i_p(i-1)*(1+nxyz(i))
END DO
DO i = 1,dim
   ivec(i) = dim-i+1
END DO
DO i=1,dim
   i_p_face(i) = i_p_face(i-1)*3
END DO

!OLEG: if we do weights for arbitrary non-unifrome meshes, this need to be changed.

!
!first go through coarse levels and assign areas to coarse levels only
!
dA_j(1) = PRODUCT(xyzlimits(2,1:dim)-xyzlimits(1,1:dim))/REAL(PRODUCT(mxyz(1:dim)),pr)
DO j = 2, j_lev
   dA_j(j) = dA_j(j-1)/2.0_pr**dim
END DO

DO j = 1, j_mn
DO wlt_type = MIN(j-1,1),2**dim-1
   DO face_type = 0, 3**dim - 1
      face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
      DO j_df = j, j_lev
         DO k = 1, indx_DB(j_df,wlt_type,face_type,j)%length              
		    ii = indx_DB(j_df,wlt_type,face_type,j)%p(k)%ixyz
            ixyz(1:dim) = INT(MOD(ii-1,i_p(1:dim))/i_p(0:dim-1))
            i = indx_DB(j_df,wlt_type,face_type,j)%p(k)%i
            dA(i) = dA_j(j_mn)/2.0_pr**SUM(ABS(face(1:dim))) !area around point taking into
            ! account any part outside domain
!PRINT *,'dA(i) , sum(dA) ', dA(i) , sum(dA), ixyz 
         END DO
      END DO
   END DO
END DO
END DO

!sumdA = sum(dA)
!PRINT *,'sum(dA) ', sumdA
!
! Go through higher levels
!
!Use machinery of reconstruction check procedure
!
DO j = j_mn+1,j_lev
   step = 2**(j_lev-j)
   step2 = 2*step
   DO idim = 1, dim

      CALL get_active_lines_indx( idim, j, 1, GET_ALL_DERIV_LEV, &
           plocal_lines_indx_backend, plocal_numlines_indx_backend,&
		   plocal_lines_indx_current )

      !
      ! Get the active points on a given line
      ! with 1D lvl in idim direction <= j
      !
      ! j_mx - get pts on line associeted with level "j_mx" and below (1D level) .AND.
      ! j_mn - get points associated with level j_mn and above (1D level)
      ! j_lev_out - current max level in solver (j_lev) which is different then the
      !             max level internal to the db.
! NOTE: for effency we don't actually need anything except line_flat_indx, and active_pts, n_active_pts
      line_flat_indx = 0 !BRUTEFORCE
      line_coords    = 0 !BRUTEFORCE
      DO WHILE( get_active_line_all_lvl_le_j( idim, 1, j, j_lev, maxval_nxyz,   1,1, &
           line_u, line_flags,  line_deriv_lev, line_flat_indx ,line_ptrs, line_coords, &
           active_pts, n_active_pts, NO_GHOST_PTS, &
           plocal_lines_indx_backend, plocal_numlines_indx_backend,&
		   plocal_lines_indx_current ) )

         !ixyz_db(:) = lines_indx(ln,:)
         !ixyz_loc(:) = ixyz_db(:) /db_scale



         DO i=1, n_active_pts
            indx_line = active_pts(i)
            indx_flat = line_flat_indx(indx_line) !index into weights_lev, dA (same as ordering of u )
            ixyz_db   = line_coords(indx_line,:)
            ixyz_loc  = ixyz_db / db_scale
            j_curr_3d = MAXVAL(lvl_db(ixyz_db(1:dim))) ! 3D level of current point

           !OLEG: need to introduce jxyz(1:dim), level per direction

            !extract line in idim direction on level <=j

            IF( i == 1 ) THEN !once for line

               ! Find level based on other dim-1
               ii = 1
               DO iii = 1,dim
                  IF( iii /= idim) THEN
                     odim(ii) = iii
                     ii = ii+1
                  END IF
               END DO
               j_line = MAXVAL(lvl_db(ixyz_db(odim)))  ! level based on other dim-1 coordinates

               ! calculate sign
               IF(j_line == j) THEN
                  s=1.0_pr
               ELSE
                  s=-1.0_pr
               END IF
            END IF


!IF( ixyz_loc(3) /= 0 ) continue
            !let i be a point and ixyz(1:dim) is its coordinate relative to j_lev
            IF(idim == 1 .AND. j_curr_3d == j) THEN
			    face = (-1 + 2*INT(ixyz_loc(1:dim)/nxyz(1:dim))+ MIN(1,MOD(ixyz_loc(1:dim),nxyz(1:dim)))) * (1 - prd(1:dim))
			    dA(indx_flat) = dA_j(j)/2.0_pr**SUM(ABS(face(1:dim)))
!sumdA = sum(dA)
!PRINT *,'sum(dA) ', sumdA
		    END IF
            !OLEG: this needs to be changed if we want to have non-uniform mesh
         END DO

         !OLEG: this logical statment is needed in order not to do extra unnesessary work along the planes that are already reassign theoir weights
         !IF(idim == 1  .OR.  (idim > 1 .AND.  ALL(jxyz(1:dim-1)  /= j) ) ) THEN
		 IF(idim == 1  .OR.  (idim > 1 .AND.  ALL(lvl_db(ixyz_db(odim(1:idim-1))) /= j) ) ) THEN !CHECK thiS

            DO i = 1, n_active_pts
			   indx_line = active_pts(i)
               indx_flat = line_flat_indx(indx_line) !index into weights_lev, dA (same as ordering of u )
               ixyz_db   = line_coords(indx_line,:)
               ixyz_loc  = ixyz_db / db_scale
               !j_curr_3d = MAXVAL(lvl_db(ixyz_db(1:dim))) ! 3D level of current point
!IF( ixyz_loc(3) /= 0 ) continue
               IF(  lvl_db(ixyz_db(idim)) == j ) THEN !odd points

                  ip = ixyz_loc(idim)+step  
                  im = ixyz_loc(idim)-step
                  IF(im<0) THEN
				     PRINT *,'ERROR weights,im<0'
					 STOP
				  END IF
                  ip = (1-prd(idim))*ip + prd(idim)*MOD(ip+ 9*nxyz(idim),nxyz(idim))
				  !im = (1-prd(idim))*im + prd(idim)*MOD(im+ 9*nxyz(idim),nxyz(idim)) !Added DG
                  dA(line_flat_indx(im)) =  dA(line_flat_indx(im)) + s * 0.5_pr*dA(line_flat_indx(indx_line))
                  dA(line_flat_indx(ip)) =  dA(line_flat_indx(ip)) + s * 0.5_pr*dA(line_flat_indx(indx_line))
                  IF( s==1.0_pr) dA(line_flat_indx(indx_line)) = 0.0_pr ! Just for debugging
!sumdA = sum(dA)
!PRINT *,'sum(dA) ', sumdA
               END IF

            END DO
         END IF

!IF(.FALSE.)THEN


         IF( idim == dim) THEN
!sumdA = sum(dA)
!PRINT *,'sum(dA) ', sumdA


            DO i=1, n_active_pts
               indx_line = active_pts(i)
               indx_flat = line_flat_indx(indx_line) !index into weights_lev, dA (same as ordering of u )
               ixyz_db   = line_coords(indx_line,:)
               ixyz_loc  = ixyz_db / db_scale
               j_curr_3d = MAXVAL(lvl_db(ixyz_db(1:dim))) ! 3D level of current point

               !let i be a point and ixyz(1:dim) is its coordinate relative to j_lev
               IF(j_curr_3d == j) THEN
			   	  face = (-1 + 2*INT(ixyz_loc(1:dim)/nxyz(1:dim))+ MIN(1,MOD(ixyz_loc(1:dim),nxyz(1:dim)))) * (1 - prd(1:dim))
			      dA(indx_flat) = dA_j(j)/2.0_pr**SUM(ABS(face(1:dim)))
!sumdA = sum(dA)
!PRINT *,'sum(dA) ', sumdA

			   END IF
               !OLEG: this nees tobe changed if we want ot have it general for non-uniform meshes
            END DO

         END IF

!END IF !TEST

!sumdA = sum(dA)
!PRINT *,'sum(dA) ', sumdA
      line_flat_indx = 0
      line_coords    = 0  !!BRUTEFORCE  zero (Can be done for only necessary pts...)
      END DO
   END DO
END DO

END IF

IF( ANY(dA < 0) ) PRINT *,'WARNING: dA is negative'

sumdA = sum(dA) ! calculate here to use in varies places
!WRITE (*,'( f15.8 )' ) dA
!!$  PRINT *, 'finish claculating weights'
 CALL deallocate_active_lines_indx(plocal_lines_indx_backend)

END SUBROUTINE weights



SUBROUTINE weights_newV1 
  !--Calculates weights for volume integration based on the Trapezoid
  !--integration rule (second-order accurate).
  IMPLICIT NONE

  INTEGER :: i, ii, iii, j, k, m, ip, im, idim, odim(1:dim-1)
  INTEGER, DIMENSION(dim) :: ixyz
  INTEGER, DIMENSION(3,2) :: i_k
  REAL (pr), DIMENSION(dim) :: lxyz
  REAL (pr) :: shft(dim,2), dAh 
  INTEGER :: i1(1), j_df, wlt_type, face_type, dir_type, step, step2
  INTEGER, DIMENSION(0:dim) ::  i_p_cube
  INTEGER, DIMENSION(dim) ::  dir, ixyz1,  i_l, i_h
  INTEGER, DIMENSION(dim,1) :: ZERO
  REAL(pr), DIMENSION(1:j_lev) :: dA_j ! area associated with a ppint of level j if it had no neighbors
  INTEGER, DIMENSION(dim) :: wlt, face, ivec
  INTEGER, DIMENSION(0:dim) :: i_p, i_p_face, i_p_wlt

  INTEGER :: ixyz_loc(3), ixyz_db(3)
  REAL(pr)             :: line_u(1:1 ,0:maxval_nxyz) !

  INTEGER(KIND_INT_FLAGS):: line_flags(0:maxval_nxyz)
  INTEGER              :: line_coords(0:maxval_nxyz,dim)  ! coordinated of returned points in terms of j_mx
  INTEGER              :: line_deriv_lev(0:maxval_nxyz)
  INTEGER              :: line_flat_indx(0:maxval_nxyz)
  TYPE (grid_pt_ptr)   :: line_ptrs(0:maxval_nxyz)
  INTEGER :: n_active_pts,active_pts(maxval_nxyz+1), indx_line, indx_flat
  INTEGER :: db_scale , j_curr_3d, j_line
  REAL(PR) :: s
  INTEGER :: plocal_numlines_indx_backend ! number of active lines indexed in lines_indx_backend
  TYPE (element_of_face_ptr) , DIMENSION(:), POINTER :: plocal_lines_indx_backend !  list of active line faces
  INTEGER :: plocal_lines_indx_current ! current active line to be returned 


!!$  PRINT *, 'start calculating weights'
  !PRINT  *,' NO weights for testing.. !!!!'
  !  dA = 1.0_pr/nwlt
  !IF( .FALSE. ) THEN

  db_scale = (2**(j_mx_db-j_lev)) 

  ZERO = 0
  dA = 0.0_pr ! zero out the glabal dA array
  i_k = 0
  lxyz(:) = xyzlimits(2,:)-xyzlimits(1,:)
  i_p(0) = 1
  i_p_cube(0) = 1
  i_p_face(0) = 1
  DO i=1,dim
     i_p(i) = i_p(i-1)*(1+nxyz(i))
  END DO
  DO i = 1,dim
     ivec(i) = dim-i+1
  END DO
  DO i=1,dim
     i_p_face(i) = i_p_face(i-1)*3
  END DO

  !
  !first go through coarse levels and assign areas to coarse levels only
  !  
  dA_j(1) = PRODUCT(xyzlimits(2,1:dim)-xyzlimits(1,1:dim))/REAL(PRODUCT(mxyz(1:dim)),pr)
  DO j = 2, j_lev
     dA_j(j) = dA_j(j-1)/2.0_pr**dim
  END DO

  DO j = 1,j_mn
  DO wlt_type = MIN(j-1,1),2**dim-1
     DO face_type = 0, 3**dim - 1
        face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
        DO j_df = j, j_lev
           DO k = 1, indx_DB(j_df,wlt_type,face_type,j)%length  
              ii = indx_DB(j_df,wlt_type,face_type,j)%p(k)%ixyz
              ixyz(1:dim) = INT(MOD(ii-1,i_p(1:dim))/i_p(0:dim-1))
              i = indx_DB(j_df,wlt_type,face_type,j)%p(k)%i
              dA(i) = dA_j(j_mn)/2.0_pr**SUM(ABS(face(1:dim))) !area around point taking into
              ! account any part outside domain
           END DO
        END DO
     END DO
  END DO
  END DO

  !
  ! Go through higher levels 
  !
  !Use machinery of reconstruction check procedure 
  ! 
  DO j = j_mn+1,j_lev
     step = 2**(j_lev-j)
     step2 = 2*step
     DO idim = 1, dim

        CALL get_active_lines_indx( idim, j, 1, GET_ALL_DERIV_LEV, &
           plocal_lines_indx_backend, plocal_numlines_indx_backend,&
		   plocal_lines_indx_current ) 



        !
        ! Get the active points on a given line 
        ! with 1D lvl in idim direction <= j
        !
        ! j_mx - get pts on line associeted with level "j_mx" and below (1D level) .AND.
        ! j_mn - get points associated with level j_mn and above (1D level)
        ! j_lev_out - current max level in solver (j_lev) which is different then the
        !             max level internal to the db. 
! NOTE: for effency we don't actually need anything except line_flat_indx, and active_pts, n_active_pts
        DO WHILE( get_active_line_all_lvl_le_j( idim, 1, j, j_lev, maxval_nxyz,   1,1, &
             line_u, line_flags,  line_deriv_lev, line_flat_indx ,line_ptrs, line_coords, &
             active_pts, n_active_pts, NO_GHOST_PTS, &
             plocal_lines_indx_backend, plocal_numlines_indx_backend,&
		     plocal_lines_indx_current ) )

           !ixyz_db(:) = lines_indx(ln,:) 
           !ixyz_loc(:) = ixyz_db(:) /db_scale 



           DO i=1, n_active_pts
              indx_line = active_pts(i)
              indx_flat = line_flat_indx(indx_line) !index into weights_lev, dA (same as ordering of u )
              ixyz_db   = line_coords(indx_line,:)
              !ixyz_loc  = ixyz_db / db_scale 
              j_curr_3d = MAXVAL(lvl_db(ixyz_db(1:dim))) ! 3D level of current point



              !extract line in idim direction on level <=j

              
              IF( i == 1 ) THEN !once for line

			     ! Find level based on other dim-1
                 ii = 1
                 DO iii = 1,dim
                    IF( iii /= idim) THEN
                       odim(ii) = iii
                       ii = ii+1
                    END IF
                 END DO
                 j_line = MAXVAL(lvl_db(ixyz_db(odim)))  ! level based on other dim-1 coordinates 

                 ! calculate sign 
                 IF(j_line == j) THEN 
                    s=-1.0_pr
                 ELSE
                    s=1.0_pr
                 END IF
              END IF



              !let i be a point and ixyz(1:dim) is its coordinate relative to j_lev
              IF(idim == 1 .AND. j_curr_3d == j) dA(indx_flat) = dA_j(j)/2.0_pr**SUM(ABS(face(1:dim)))
           END DO

           IF(j_line /= j) THEN
              DO i = 1, n_active_pts
                 IF(  lvl_db(ixyz_db(idim)) == j ) THEN !odd points
                    indx_line = active_pts(i)
                    !indx_flat = line_flat_indx(indx_line) !index into weights_lev, dA (same as ordering of u )
                    !ixyz_db   = line_coords(indx_line,:)
                    !ixyz_loc  = ixyz_db / db_scale 
                    !j_curr_3d = MAXVAL(lvl_db(ixyz_db(1:dim))) ! 3D level of current point

                    ip = ixyz(idim)+step
                    im = ixyz(idim)-step
                    ip = (1-prd(idim))*ip + prd(idim)*MOD(ip+ 9*nxyz(idim),nxyz(idim)) 
                    dA(line_flat_indx(im)) =  dA(line_flat_indx(im)) + s * 0.5_pr*dA(line_flat_indx(indx_line))
                    dA(line_flat_indx(ip)) =  dA(line_flat_indx(ip)) + s * 0.5_pr*dA(line_flat_indx(indx_line))
                 END IF
              END DO
           END IF
           IF( idim == dim) THEN
              DO i=1, n_active_pts
                 indx_line = active_pts(i)
                 indx_flat = line_flat_indx(indx_line) !index into weights_lev, dA (same as ordering of u )
                 ixyz_db   = line_coords(indx_line,:)
                 !ixyz_loc  = ixyz_db / db_scale 
                 j_curr_3d = MAXVAL(lvl_db(ixyz_db(1:dim))) ! 3D level of current point

                 !let i be a point and ixyz(1:dim) is its coordinate relative to j_lev
                 IF(j_curr_3d == j) dA(indx_flat) = dA_j(j)/2.0_pr**SUM(ABS(face(1:dim)))
              END DO

           END IF

        END DO
     END DO
  END DO


  IF( ANY(dA < 0) ) PRINT *,'WARNING: dA is negative'

  sumdA = sum(dA) ! calculate here to use in varies places
 CALL deallocate_active_lines_indx(plocal_lines_indx_backend)

!!$  PRINT *, 'finish claculating weights'
END SUBROUTINE weights_newV1


SUBROUTINE release_memory_DB
  IMPLICIT NONE
			  
   DEALLOCATE (	xx, Nwlt_lev, indx_DB)

END SUBROUTINE release_memory_DB


SUBROUTINE mdl_filt_EPSplus( filt_mask, u_in, scl, leps, ne_local) 
  USE precision
  USE main_vars
  USE share_consts
  USE pde
  USE sizes
  USE field
  IMPLICIT NONE
  INTEGER , INTENT (IN) :: ne_local  
  REAL (pr), DIMENSION (1:nwlt,1:ne_local) , INTENT (IN) :: u_in
  REAL (pr), DIMENSION (1:nwlt) , INTENT (INOUT) :: filt_mask
  REAL (pr) , INTENT (IN)   :: scl(1:ne_local)
  REAL (pr) , INTENT (IN)   :: leps ! local epsilon
  INTEGER :: i,ie

  PRINT *,'SUBROUTINE mdl_filt_EPSplus currently not supported in db_lines..Exiting...'
  stop

END SUBROUTINE mdl_filt_EPSplus
!
!
! This routine creates internal indices from the indx_DB stucture.
! It is called from indices(_db) for both wrk and lines DB versions
! on restart or IC from restart file format.
!
SUBROUTINE create_indices_from_indx_DB()
  IMPLICIT NONE
  INTEGER i,j
  !------------ arrays used for indx_DB --------------------
  INTEGER :: j_df, j_lv, wlt_type, face_type, ishift, ibndr
  INTEGER :: face_dim, face_val
  INTEGER, DIMENSION(dim) :: wlt, face, ivec
  INTEGER, DIMENSION(0:dim) :: i_p, i_p_face, i_p_wlt
  LOGICAL, DIMENSION(:,:), ALLOCATABLE :: face_assigned
  !---------------------------------------------------------
  
     !Creating lv_intrnl, lv_bnd, & indx from indx_DB
     i_p(0) = 1
     i_p_face(0) = 1
     DO i=1,dim
        i_p(i) = i_p(i-1)*(1+nxyz(i))
        i_p_face(i) = i_p_face(i-1)*3
     END DO
     IF(ALLOCATED(lv_intrnl)) DEALLOCATE(lv_intrnl)
     ALLOCATE(lv_intrnl(0:j_lev))
     face = 0 
     face_type = SUM((face+1)*i_p_face(0:dim-1))
     lv_intrnl(0) = 0
     DO j = 1, j_lev
        lv_intrnl(j) = lv_intrnl(j-1)
        DO wlt_type = MIN(j-1,1),2**dim-1
           DO j_df = j, j_lev
              lv_intrnl(j) = lv_intrnl(j) + indx_DB(j_df,wlt_type,face_type,j)%length  
           END DO
        END DO
     END DO
     nwlt = lv_intrnl(j_lev)
     IF(ALLOCATED(lv_bnd)) DEALLOCATE(lv_bnd) !changed
     ALLOCATE(lv_bnd(0:3,1:2*dim,0:j_lev)) !changed
     IF(ALLOCATED(face_assigned)) DEALLOCATE(face_assigned)
     ALLOCATE(face_assigned(0:3**dim-1,1:j_mx))
     face_assigned = .FALSE.
     face_assigned((3**dim-1)/2,:) = .TRUE. !internal points
     IF( MINVAL(prd(1:dim)) == 0 ) THEN !non-periodic at least in one direction
        DO i=1,2*dim
           IF(ibnd(i) == 1 .AND. prd(1) == 0) THEN 
              !left  boundary (xmin)
              face_dim = 1
              face_val =-1
           ELSE IF(ibnd(i) == 2 .AND. prd(1) == 0) THEN 
              !right boundary (xmax)
              face_dim = 1
              face_val = 1
           ELSE IF(ibnd(i) == 3 .AND. prd(2) == 0) THEN 
              !bottom boundary (ymin)
              face_dim = 2
              face_val =-1
           ELSE IF(ibnd(i) == 4 .AND. prd(2) == 0) THEN 
              !top boundary (ymax)
              face_dim = 2
              face_val = 1
           ELSE IF(dim == 3 .and. ibnd(i) == 5 .AND. prd(3) == 0) THEN 
              !bottom boundary (zmin)
              face_dim = 3
              face_val =-1
           ELSE IF(dim == 3 .and. ibnd(i) == 6 .AND. prd(3) == 0) THEN 
              !top boundary (zmax)
              face_dim = 3
              face_val = 1
           END IF
           DO j=1,j_lev  !--- the following do-loop changed to set up indicies for multilevel calculations.
              lv_bnd(0,i,j) = nwlt+1 
              lv_bnd(1,i,j) = nwlt   
              DO face_type = 0, 3**dim - 1
                 face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
                 IF( face(face_dim) == face_val .AND. .NOT.face_assigned(face_type,j) ) THEN
                    face_assigned(face_type,j) = .TRUE.
                    DO wlt_type = MIN(j-1,1),2**dim-1
                       DO j_df = j, j_lev
                          nwlt = nwlt + indx_DB(j_df,wlt_type,face_type,j)%length
                          lv_bnd(1,i,j) = lv_bnd(1,i,j) + indx_DB(j_df,wlt_type,face_type,j)%length
                       END DO
                    END DO
                 END IF
              END DO
           END DO
        END DO
     END IF
     DEALLOCATE(face_assigned)
     ! update the lv_bnd(2:3, , ) 
     ! The 2:3 indices are for mappign the bnd points to the end of a lower level 
     ! in the flat u array. Used in diff_aux()
     DO j=1,j_lev
        i = 1
        lv_bnd(2,i,j) = lv_intrnl(j) + 1
        lv_bnd(3,i,j) = lv_bnd(2,i,j) + lv_bnd(1,i,j)-lv_bnd(0,i,1)
        DO i=2,2*dim
           lv_bnd(2,i,j) = lv_bnd(3,i-1,j) + 1
           lv_bnd(3,i,j) = lv_bnd(2,i,j) + lv_bnd(1,i,j)-lv_bnd(0,i,1)
        END DO
     END DO
 

END SUBROUTINE create_indices_from_indx_DB

  !--------------------------------------------------------------------------------
  ! empty subroutines
  SUBROUTINE pre_init_DB
  END SUBROUTINE pre_init_DB



END MODULE wlt_trns_mod
