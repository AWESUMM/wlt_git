!
! program NtoMonP
!
! Convert N *.res files into M *.res files while running on P processors
! (currently P=1)
!
#ifndef MULTIPROC
#error NtoMonP has to be compiled in parallel mode only
#endif

PROGRAM NMP
  USE precision
  USE share_consts
  USE pde
  USE sizes
  USE wlt_trns_mod              ! init_DB(), c_wlt_trns_mask()
  USE wlt_trns_vars             ! indx_DB, xx
  USE field                     ! u
  USE io_3D                     ! read_solution_dim()
  USE io_3D_vars                ! DO_READ_ALL_VARS_TRUE
  USE wlt_vars                  ! dim, j_lev, j_mx
  USE nmp_aux
  USE debug_vars                ! debug_level, verb_level, ignore_warnings
  USE parallel                  ! MPI related subroutines

#if ((TREE_VERSION == 2) || (TREE_VERSION == 3))
    USE tree_database_f      ! use Fortran database instead of C/C++ tree
#endif

  IMPLICIT NONE
  
  REAL (pr), DIMENSION (:), POINTER :: scl               ! used in READ_SOLUTION
  TYPE (indx_type_DB), ALLOCATABLE, DIMENSION(:,:,:,:) :: indx_DB_tmp
  REAL(pr), ALLOCATABLE :: u_tmp(:,:)
  INTEGER :: nwlt_tmp, length
  
  INTEGER   :: out_num, i, ii, iargc,           &
       j, wlt_type, face_type, j_df, k,     &
       out_num_counter
  INTEGER*8 :: i_INTEGER8
  INTEGER, ALLOCATABLE :: ixyz(:), i_pt(:)
  INTEGER*8, ALLOCATABLE :: i_p(:)
  
  INTEGER, PARAMETER :: MAXLEN = 512                         ! maximum length of command line argument
  CHARACTER(LEN=MAXLEN) :: input_file, &                     ! file names
       output_base_name, tmp_name
  INTEGER :: out_proc_num,                                 & ! number of output files
       itmp,                                               & ! temporal variable
       d_meth,                                             & ! domain decomposition method
       in_proc_num,                                        & ! user provided IC_processors
       mpi_par_size                                          ! actual par_size
  CHARACTER (LEN=par_LENSTR1) :: in_str, out_str, tmp_str    ! string representation
  LOGICAL :: do_append, &                                    ! append .res files data
       domain_change                                         ! parallel domain decompose() output parameter

  debug_level = 0 !15
  verb_level = 1
  ignore_warnings = .TRUE.
  
  CALL parallel_init
  mpi_par_size = par_size

  ! Read command line arguments
  IF( iargc() < 1 ) THEN
     CALL usage_nmp
  ELSE
     CALL read_file_type_nmp (input_file, output_base_name, out_proc_num, in_proc_num, &
        verb_level, do_append, d_meth)
  END IF
  

  ! owerwrite IC_par_size by user provided value
  IF (in_proc_num.GT.0) THEN
     IF (par_rank.EQ.0) THEN
        WRITE (*,'(" ")')
        WRITE (*,'(" WARNING: I am assuming that input files were created,")')
        WRITE (*,'("          according to user provided input, on ",I," processor(s)")') in_proc_num
        WRITE (*,'(" ")')
    END IF
     IC_par_size = in_proc_num
  END IF
  
!!$  WRITE (*,'(A,I2,5A,3(I2,X),L)') &
!!$       'par_rank=', par_rank, ', input_file=',TRIM(input_file), ', output_base_name=', TRIM(output_base_name), &
!!$       ', out_nproc/meth/verb/append=', out_proc_num, d_meth, verb_level, do_append
  
  
  ! read all but U and INDX_DB to enable domain re-decomposition
  ! (all the processors read the common part from the same file)
  ! (in a single processor run we will read all the files anyway)
  IC_filename = TRIM(input_file)
  IF (mpi_par_size.GT.1 ) THEN
     CALL read_solution_dim( IC_filename, VERBLEVEL=verb_level, &
          EXACT=.TRUE., IGNOREINIT = .TRUE., PARTREAD=3 )
     domain_meth = d_meth
     j_tree_root = j_mn
     CALL pre_init_DB
  END IF
  

  ! read all the required input file(s) at the
  ! current processor, the data to spread are in
  ! u(1:nwlt,1:n_var) and indx_DB()%p()%{i, ixyz}
  CALL read_solution( scl, IC_filename, DO_READ_ALL_VARS_TRUE, VERBLEVEL=verb_level, &
       EXACT=.TRUE., IGNOREINIT = .TRUE., APPEND=do_append, CLEAN_IC_DD=.FALSE., & 
       ZERO_UPDATE=.FALSE. )                                                             ! do not set n_updt to zero

  
  ! decompose domain for a given number of processors: write par_proc_tree(:)
  ! (in a parallel run the decomposition has been already performed)
  ! (decomposition uses global par_size)
  IF (mpi_par_size.EQ.1) THEN
     par_size = out_proc_num
     j_tree_root = j_mn
     CALL parallel_domain_decompose( domain_change, VERB=(par_rank.EQ.0.AND.verb_level.GT.1), METH=d_meth, FIRSTCALL=.TRUE. )
  END IF


  ! copy indx_DB() up to j_lev
  ALLOCATE( indx_DB_tmp(1:j_lev,0:2**dim-1,0:3**dim-1,1:j_lev) )
  DO j = 1, j_lev
     DO wlt_type = MIN(j-1,1),2**dim - 1
        DO face_type = 0, 3**dim - 1
           DO j_df = 1, j_lev
              length = indx_DB(j_df,wlt_type,face_type,j)%length
              indx_DB_tmp(j_df,wlt_type,face_type,j)%length = length
              ALLOCATE( indx_DB_tmp(j_df,wlt_type,face_type,j)%p(length) )
              DO k=1,length
                 indx_DB_tmp(j_df,wlt_type,face_type,j)%p(k)%i =    indx_DB(j_df,wlt_type,face_type,j)%p(k)%i
                 indx_DB_tmp(j_df,wlt_type,face_type,j)%p(k)%ixyz = indx_DB(j_df,wlt_type,face_type,j)%p(k)%ixyz
              END DO
           END DO
        END DO
     END DO
  END DO
  ! copy u
  ALLOCATE( u_tmp(1:nwlt,1:n_var) )
  u_tmp(1:nwlt,1:n_var) = u(1:nwlt,1:n_var)

!!$  OPEN(UNIT=333,FILE='nmp_rdebug'//par_rank_str)
!!$  DO j = 1, j_lev
!!$     DO wlt_type = MIN(j-1,1),2**dim-1
!!$        DO face_type = 0, 3**dim - 1
!!$           DO j_df = j, j_lev
!!$              DO k=1,indx_DB_tmp(j_df,wlt_type,face_type,j)%length
!!$                 !WRITE (333,'(4(I3,X),I4,",",I6,", ",E12.5)') j,wlt_type,face_type,j_df,num_4, &
!!$                 WRITE (333,*) indx_DB_tmp(j_df,wlt_type,face_type,j)%p(k)%ixyz, &
!!$                      u(indx_DB_tmp(j_df,wlt_type,face_type,j)%p(k)%i,:)
!!$              END DO
!!$           END DO
!!$        END DO
!!$     END DO
!!$  END DO
!!$  CLOSE(333)
  
  ! 1D coordinate conversion
  ALLOCATE (i_p(0:dim), i_pt(0:dim), ixyz(1:dim))
  i_p(0) = 1
  i_pt(0) = 1
  DO i=1,dim
     i_p(i) = i_p(i-1)*(1+nxyz(i))                             ! global ixyz 1D coord
     i_pt(i) = i_pt(i-1)*(1-prd(i)+mxyz(i)*2**(j_tree_root-1)) ! coord in array of trees
  END DO


  ! miscellanious variables required by save_solution
  ALLOCATE (n_var_save(1:n_var), n_var_index(1:n_var))
  n_var_save = .TRUE.
  DO i=1,n_var
     n_var_index(i) = i
  END DO

  
  DO out_num_counter = 0, out_proc_num-1
     
     ! Multiprocessor n2m run assumes one OUTPUT file per processor,
     ! single processor will run through all the outputs
     out_num = out_num_counter
     IF (mpi_par_size.GT.1) out_num = par_rank


     ! for each output file find local nwlt and arrange u() and indx_DB() for writing
     nwlt_tmp = 0
     DO j = 1, j_lev
        DO wlt_type = MIN(j-1,1),2**dim-1
           DO face_type = 0, 3**dim - 1
              DO j_df = j, j_lev
                 
                 length = 0                                                   ! length of p() to be written
                 
                 DO k=1,indx_DB_tmp(j_df,wlt_type,face_type,j)%length
                    i_INTEGER8 = indx_DB_tmp(j_df,wlt_type,face_type,j)%p(k)%ixyz     ! 1D coordinate (at j_lev)
                    ixyz(1:dim) = (INT(MOD(i_INTEGER8-1,i_p(1:dim))/i_p(0:dim-1))) &
                         / 2**(j_lev-j_tree_root)
                    ii = 1+SUM(ixyz(1:dim)*i_pt(0:dim-1))                     ! 1D coordinate (at j_mn)
                    
                    IF (par_proc_tree(ii).EQ.out_num) THEN                    ! this node to be written
                       nwlt_tmp = nwlt_tmp + 1
                       u(nwlt_tmp,1:n_var) = u_tmp( indx_DB_tmp(j_df,wlt_type,face_type,j)%p(k)%i, 1:n_var )
                       length = length + 1
                       indx_DB(j_df,wlt_type,face_type,j)%p(length)%i    = nwlt_tmp
                       indx_DB(j_df,wlt_type,face_type,j)%p(length)%ixyz = indx_DB_tmp(j_df,wlt_type,face_type,j)%p(k)%ixyz
                    END IF
                 END DO
                 indx_DB(j_df,wlt_type,face_type,j)%length = length

              END DO
           END DO
        END DO
     END DO
     
!!$     OPEN(UNIT=333,FILE='nmp_rdebug-0'//par_rank_str)
!!$     DO j = 1, j_lev
!!$        DO wlt_type = MIN(j-1,1),2**dim-1
!!$           DO face_type = 0, 3**dim - 1
!!$              DO j_df = j, j_lev
!!$                 DO k=1,indx_DB(j_df,wlt_type,face_type,j)%length
!!$                    !WRITE (333,'(4(I3,X),I4,",",I6,", ",E12.5)') j,wlt_type,face_type,j_df,num_4, &
!!$                    WRITE (333,*) indx_DB(j_df,wlt_type,face_type,j)%p(k)%ixyz, &
!!$                         u(indx_DB(j_df,wlt_type,face_type,j)%p(k)%i,:)
!!$                 END DO
!!$              END DO
!!$           END DO
!!$        END DO
!!$     END DO
!!$     CLOSE(333)
!!$     STOP

     ! save output file
     ! Multiprocessor n2m run assumes one OUTPUT file per processor,
     out_str = ''
     WRITE( tmp_str,'(I'//CHAR(ICHAR('0')+par_LENSTR1)//')') out_num
     out_str = '.p'//TRIM(ADJUSTL(tmp_str))
     tmp_name = TRIM(output_base_name)//TRIM(out_str)//'.res'
     CALL save_solution (nwlt_tmp, n_var, scl, iwrite, VERBLEVEL=verb_level, NAME=tmp_name, EXACT=.TRUE.)
     
     ! Multiprocessor n2m run assumes one OUTPUT file per processor,
     ! single processor will run through all the outputs
     IF (mpi_par_size.GT.1) EXIT
  END DO
  
  
  ! clear the memory after READ_SOLUTION and main
  IF ( ALLOCATED(u_variable_names) ) DEALLOCATE(u_variable_names)
  IF ( ASSOCIATED(scl) ) DEALLOCATE(scl)
  IF ( ASSOCIATED(scl_global) ) DEALLOCATE(scl_global)
  IF ( ALLOCATED(n_var_wlt_fmly) ) DEALLOCATE(n_var_wlt_fmly)
  IF ( ALLOCATED(xx) ) DEALLOCATE(xx)
  IF ( ALLOCATED(u) ) DEALLOCATE(u)

  DO j = 1, j_lev
     DO wlt_type = MIN(j-1,1),2**dim - 1
        DO face_type = 0, 3**dim - 1
           DO j_df = 1, j_lev
              IF (ASSOCIATED(indx_DB_tmp(j_df,wlt_type,face_type,j)%p)) &
                   DEALLOCATE( indx_DB_tmp(j_df,wlt_type,face_type,j)%p )
              IF (ASSOCIATED(indx_DB(j_df,wlt_type,face_type,j)%p)) &
                   DEALLOCATE( indx_DB(j_df,wlt_type,face_type,j)%p )
           END DO
        END DO
     END DO
  END DO
  DEALLOCATE(i_p, i_pt, ixyz, n_var_save, n_var_index, indx_DB_tmp, indx_DB, u_tmp)

  CALL parallel_finalize

END PROGRAM NMP



