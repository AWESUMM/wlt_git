# Microsoft Developer Studio Project File - Name="wlt_3d_dblines" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Console Application" 0x0103

CFG=wlt_3d_dblines - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "wlt_3d_dblines.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "wlt_3d_dblines.mak" CFG="wlt_3d_dblines - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "wlt_3d_dblines - Win32 Release" (based on "Win32 (x86) Console Application")
!MESSAGE "wlt_3d_dblines - Win32 Debug" (based on "Win32 (x86) Console Application")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
F90=df.exe
RSC=rc.exe

!IF  "$(CFG)" == "wlt_3d_dblines - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE F90 /compile_only /include:"Release/" /nologo /warn:nofileopt
# ADD F90 /browser /check:bounds /compile_only /debug:full /fpp /include:"Release/" /nologo /traceback /warn:argument_checking /warn:declarations /warn:truncated_source
# SUBTRACT F90 /check:noflawed_pentium /warn:nouninitialized /warn:unused
# ADD BASE CPP /nologo /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_CONSOLE" /D "_MBCS" /YX /FD /c
# ADD CPP /nologo /MLd /W3 /Gi /GX /Od /D "WIN32" /D "NDEBUG" /D "_CONSOLE" /D "_MBCS" /FR /YX /FD /c
# ADD BASE RSC /l 0x409 /d "NDEBUG"
# ADD RSC /l 0x409 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:console /machine:I386
# ADD LINK32 ..\netcdf.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /stack:0x3b9aca00 /subsystem:console /machine:I386
# SUBTRACT LINK32 /pdb:none /debug

!ELSEIF  "$(CFG)" == "wlt_3d_dblines - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE F90 /check:bounds /compile_only /debug:full /include:"Debug/" /nologo /traceback /warn:argument_checking /warn:nofileopt
# ADD F90 /browser /check:bounds /compile_only /debug:full /fpp /include:"Debug/" /nologo /real_size:32 /traceback /warn:argument_checking /warn:nofileopt
# ADD BASE CPP /nologo /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_CONSOLE" /D "_MBCS" /YX /FD /GZ /c
# ADD CPP /nologo /W3 /Gm /Gi /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_CONSOLE" /D "_MBCS" /FR /YX /FD /GZ /c
# ADD BASE RSC /l 0x409 /d "_DEBUG"
# ADD RSC /l 0x409 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:console /debug /machine:I386 /pdbtype:sept
# ADD LINK32 netcdf.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /stack:0x11e1a300,0x11e1a300 /subsystem:console /profile /debug /machine:I386 /libpath:"c:\dan\research\netcdf\lib"

!ENDIF 

# Begin Target

# Name "wlt_3d_dblines - Win32 Release"
# Name "wlt_3d_dblines - Win32 Debug"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat;f90;for;f;fpp"
# Begin Source File

SOURCE=..\additional_nodes.f90
NODEP_F90_ADDIT=\
	".\Release\debug_vars.mod"\
	".\Release\precision.mod"\
	".\Release\wlt_vars.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\WorkCases\Dan\HomogeneousTurbulence\case_isoturb_new.f90
NODEP_F90_CASE_=\
	".\Release\elliptic_mod.mod"\
	".\Release\elliptic_vars.mod"\
	".\Release\fft_module.mod"\
	".\Release\field.mod"\
	".\Release\input_file_reader.mod"\
	".\Release\io_3D_vars.mod"\
	".\Release\misc_vars.mod"\
	".\Release\pde.mod"\
	".\Release\precision.mod"\
	".\Release\SGS_incompressible.mod"\
	".\Release\share_consts.mod"\
	".\Release\share_kry.mod"\
	".\Release\sizes.mod"\
	".\Release\spectra_module.mod"\
	".\Release\util_mod.mod"\
	".\Release\util_vars.mod"\
	".\Release\vector_util_mod.mod"\
	".\Release\wavelet_filters_mod.mod"\
	".\Release\wlt_trns_mod.mod"\
	".\Release\wlt_trns_util_mod.mod"\
	".\Release\wlt_trns_vars.mod"\
	".\Release\wlt_vars.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\WorkCases\Dan\HomogeneousTurbulence\case_isoturb_new.PARAMS.f90
# End Source File
# Begin Source File

SOURCE=..\fft\ch_resolution_fs.f90
NODEP_F90_CH_RE=\
	".\Release\debug_vars.mod"\
	".\Release\precision.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\coord_mapping.f90
# End Source File
# Begin Source File

SOURCE=..\d1mach.f
# End Source File
# Begin Source File

SOURCE=..\db_lines_V2_backend.f90
NODEP_F90_DB_LI=\
	".\Release\db_lines_vars.mod"\
	".\Release\debug_vars.mod"\
	".\Release\precision.mod"\
	".\Release\user_case_db.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\db_lines_V2_frontend.f90
NODEP_F90_DB_LIN=\
	".\Release\db_lines_vars.mod"\
	".\Release\debug_vars.mod"\
	".\Release\lines_database.mod"\
	".\Release\pde.mod"\
	".\Release\precision.mod"\
	".\Release\sizes.mod"\
	".\Release\user_case_db.mod"\
	".\Release\util_vars.mod"\
	".\Release\wlt_trns_util_mod.mod"\
	".\Release\wlt_trns_vars.mod"\
	".\Release\wlt_vars.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\db_lines_vars.f90
NODEP_F90_DB_LINE=\
	".\Release\precision.mod"\
	".\Release\user_case_db.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\debug_vars.f90
# End Source File
# Begin Source File

SOURCE=..\default_util.f90
NODEP_F90_DEFAU=\
	".\Release\debug_vars.mod"\
	".\Release\field.mod"\
	".\Release\io_3D.mod"\
	".\Release\pde.mod"\
	".\Release\precision.mod"\
	".\Release\share_consts.mod"\
	".\Release\sizes.mod"\
	".\Release\user_case.mod"\
	".\Release\user_case_db.mod"\
	".\Release\util_vars.mod"\
	".\Release\wlt_trns_mod.mod"\
	".\Release\wlt_trns_util_mod.mod"\
	".\Release\wlt_vars.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\derf.f
# End Source File
# Begin Source File

SOURCE=..\derfc.f
# End Source File
# Begin Source File

SOURCE=..\dgamma.f
# End Source File
# Begin Source File

SOURCE=..\dgeev.f
# End Source File
# Begin Source File

SOURCE=..\dgels.f
# End Source File
# Begin Source File

SOURCE=..\dqag.f
# End Source File
# Begin Source File

SOURCE=..\dqage.f
# End Source File
# Begin Source File

SOURCE=..\dtrsm.f
# End Source File
# Begin Source File

SOURCE=..\elliptic_solve_3d.f90
NODEP_F90_ELLIP=\
	".\Release\debug_vars.mod"\
	".\Release\elliptic_vars.mod"\
	".\Release\pde.mod"\
	".\Release\precision.mod"\
	".\Release\share_consts.mod"\
	".\Release\sizes.mod"\
	".\Release\util_vars.mod"\
	".\Release\wlt_trns_mod.mod"\
	".\Release\wlt_vars.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\elliptic_solve_3d_vars.f90
NODEP_F90_ELLIPT=\
	".\Release\precision.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\endienness_small.f90
# End Source File
# Begin Source File

SOURCE=..\fft\fft.f90
NODEP_F90_FFT_F=\
	".\Release\debug_vars.mod"\
	".\Release\fft_interface_module.mod"\
	".\Release\precision.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\fft\fft.interface.temperton.f90
NODEP_F90_FFT_I=\
	".\Release\debug_vars.mod"\
	".\Release\precision.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\fft\fft_util.f90
NODEP_F90_FFT_U=\
	".\Release\debug_vars.mod"\
	".\Release\fft_module.mod"\
	".\Release\precision.mod"\
	".\Release\wlt_vars.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\fft\fftpacktvms.f
# End Source File
# Begin Source File

SOURCE=..\gaussq.f
# End Source File
# Begin Source File

SOURCE=..\input_files_reader.f90
NODEP_F90_INPUT=\
	".\Release\debug_vars.mod"\
	".\Release\precision.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\io_3d.f90
NODEP_F90_IO_3D=\
	".\Release\additional_nodes.mod"\
	".\Release\debug_vars.mod"\
	".\Release\elliptic_vars.mod"\
	".\Release\endianness_mod.mod"\
	".\Release\field.mod"\
	".\Release\input_file_reader.mod"\
	".\Release\io_3D_vars.mod"\
	".\Release\misc_vars.mod"\
	".\Release\pde.mod"\
	".\Release\penalization.mod"\
	".\Release\precision.mod"\
	".\Release\share_consts.mod"\
	".\Release\share_kry.mod"\
	".\Release\sizes.mod"\
	".\Release\user_case.mod"\
	".\Release\wavelet_filters_vars.mod"\
	".\Release\wlt_trns_mod.mod"\
	".\Release\wlt_trns_util_mod.mod"\
	".\Release\wlt_trns_vars.mod"\
	".\Release\wlt_vars.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\io_3d_vars.f90
# End Source File
# Begin Source File

SOURCE=..\needblas.f
# End Source File
# Begin Source File

SOURCE=..\r1mach.f
# End Source File
# Begin Source File

SOURCE=..\read_data_wray.f90
NODEP_F90_READ_=\
	".\Release\debug_vars.mod"\
	".\Release\fft_module.mod"\
	".\Release\precision.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\read_init.f90
NODEP_F90_READ_I=\
	".\Release\ch_resolution.mod"\
	".\Release\debug_vars.mod"\
	".\Release\endianness_mod.mod"\
	".\Release\fft_module.mod"\
	".\Release\pde.mod"\
	".\Release\precision.mod"\
	".\Release\read_data_wry.mod"\
	".\Release\read_netcdf_mod.mod"\
	".\Release\share_consts.mod"\
	".\Release\sizes.mod"\
	".\Release\spectra_module.mod"\
	".\Release\util_mod.mod"\
	".\Release\wlt_trns_mod.mod"\
	".\Release\wlt_trns_util_mod.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\read_netcdf.f90
DEP_F90_READ_N=\
	"..\netcdf.inc"\
	
NODEP_F90_READ_N=\
	".\Release\debug_vars.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\reverse_endian.f
NODEP_F90_REVER=\
	".\Release\precision.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\sgs_incompressible.f90
NODEP_F90_SGS_I=\
	".\Release\input_file_reader.mod"\
	".\Release\io_3D_vars.mod"\
	".\Release\pde.mod"\
	".\Release\precision.mod"\
	".\Release\share_consts.mod"\
	".\Release\share_kry.mod"\
	".\Release\spectra_module.mod"\
	".\Release\util_mod.mod"\
	".\Release\vector_util_mod.mod"\
	".\Release\wavelet_filters_mod.mod"\
	".\Release\wavelet_filters_vars.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\shared_modules.f90
# End Source File
# Begin Source File

SOURCE=..\fft\spectra.f90
NODEP_F90_SPECT=\
	".\Release\debug_vars.mod"\
	".\Release\fft_module.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\time_int_cn.f90
NODEP_F90_TIME_=\
	".\Release\debug_vars.mod"\
	".\Release\elliptic_mod.mod"\
	".\Release\misc_vars.mod"\
	".\Release\pde.mod"\
	".\Release\penalization.mod"\
	".\Release\precision.mod"\
	".\Release\SGS_incompressible.mod"\
	".\Release\share_consts.mod"\
	".\Release\share_kry.mod"\
	".\Release\sizes.mod"\
	".\Release\user_case.mod"\
	".\Release\util_mod.mod"\
	".\Release\util_vars.mod"\
	".\Release\vector_util_mod.mod"\
	".\Release\wlt_trns_mod.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\time_int_krylov_3d.f90
NODEP_F90_TIME_I=\
	".\Release\debug_vars.mod"\
	".\Release\elliptic_mod.mod"\
	".\Release\pde.mod"\
	".\Release\precision.mod"\
	".\Release\share_consts.mod"\
	".\Release\share_kry.mod"\
	".\Release\sizes.mod"\
	".\Release\user_case.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\util_3d.f90
NODEP_F90_UTIL_=\
	".\Release\debug_vars.mod"\
	".\Release\misc_vars.mod"\
	".\Release\pde.mod"\
	".\Release\precision.mod"\
	".\Release\sizes.mod"\
	".\Release\util_vars.mod"\
	".\Release\wavelet_filters_vars.mod"\
	".\Release\wlt_trns_mod.mod"\
	".\Release\wlt_vars.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\vector_util.f90
NODEP_F90_VECTO=\
	".\Release\debug_vars.mod"\
	".\Release\pde.mod"\
	".\Release\precision.mod"\
	".\Release\wlt_trns_mod.mod"\
	".\Release\wlt_vars.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\wavelet_3d.f90
NODEP_F90_WAVEL=\
	".\Release\debug_vars.mod"\
	".\Release\precision.mod"\
	".\Release\sizes.mod"\
	".\Release\util_vars.mod"\
	".\Release\wlt_trns_vars.mod"\
	".\Release\wlt_vars.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\wavelet_3d_lines.f90
NODEP_F90_WAVELE=\
	".\Release\coord_mapping.mod"\
	".\Release\db_lines_vars.mod"\
	".\Release\db_mod.mod"\
	".\Release\debug_vars.mod"\
	".\Release\field.mod"\
	".\Release\io_3D_vars.mod"\
	".\Release\pde.mod"\
	".\Release\precision.mod"\
	".\Release\share_consts.mod"\
	".\Release\sizes.mod"\
	".\Release\util_vars.mod"\
	".\Release\wlt_trns_util_mod.mod"\
	".\Release\wlt_trns_vars.mod"\
	".\Release\wlt_vars.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\wavelet_3d_lines_vars.f90
NODEP_F90_WAVELET=\
	".\Release\lines_database.mod"\
	".\Release\wlt_vars.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\wavelet_3d_vars.f90
NODEP_F90_WAVELET_=\
	".\Release\precision.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\wavelet_filters.f90
NODEP_F90_WAVELET_F=\
	".\Release\debug_vars.mod"\
	".\Release\pde.mod"\
	".\Release\precision.mod"\
	".\Release\sizes.mod"\
	".\Release\wavelet_filters_vars.mod"\
	".\Release\wlt_trns_mod.mod"\
	".\Release\wlt_trns_util_mod.mod"\
	".\Release\wlt_vars.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\wavelet_filters_vars.f90
NODEP_F90_WAVELET_FI=\
	".\Release\precision.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\wlt_3d_main.f90
NODEP_F90_WLT_3=\
	".\Release\debug_vars.mod"\
	".\Release\default_mod.mod"\
	".\Release\elliptic_mod.mod"\
	".\Release\elliptic_vars.mod"\
	".\Release\field.mod"\
	".\Release\io_3D.mod"\
	".\Release\io_3D_vars.mod"\
	".\Release\kry_mod.mod"\
	".\Release\main_vars.mod"\
	".\Release\misc_vars.mod"\
	".\Release\pde.mod"\
	".\Release\precision.mod"\
	".\Release\read_init_mod.mod"\
	".\Release\read_netcdf_mod.mod"\
	".\Release\SGS_incompressible.mod"\
	".\Release\share_consts.mod"\
	".\Release\share_kry.mod"\
	".\Release\sizes.mod"\
	".\Release\time_int_cn_mod.mod"\
	".\Release\user_case.mod"\
	".\Release\util_mod.mod"\
	".\Release\util_vars.mod"\
	".\Release\wavelet_filters_mod.mod"\
	".\Release\wavelet_filters_vars.mod"\
	".\Release\wlt_trns_mod.mod"\
	".\Release\wlt_trns_util_mod.mod"\
	".\Release\wlt_vars.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\wlt_3d_main_vars.f90
NODEP_F90_WLT_3D=\
	".\Release\precision.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\zgetrf.f
# End Source File
# Begin Source File

SOURCE=..\zgetri.f
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl;fi;fd"
# End Group
# Begin Group "Resource Files"

# PROP Default_Filter "ico;cur;bmp;dlg;rc2;rct;bin;rgs;gif;jpg;jpeg;jpe"
# End Group
# End Target
# End Project
