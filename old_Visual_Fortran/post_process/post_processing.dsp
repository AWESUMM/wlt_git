# Microsoft Developer Studio Project File - Name="post_processing" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Console Application" 0x0103

CFG=post_processing - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "post_processing.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "post_processing.mak" CFG="post_processing - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "post_processing - Win32 Release" (based on "Win32 (x86) Console Application")
!MESSAGE "post_processing - Win32 Debug" (based on "Win32 (x86) Console Application")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
F90=df.exe
RSC=rc.exe

!IF  "$(CFG)" == "post_processing - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Target_Dir ""
# ADD BASE F90 /compile_only /include:"Release/" /nologo /warn:nofileopt
# ADD F90 /check:bounds /compile_only /include:"Release/" /nologo /warn:argument_checking /warn:nofileopt
# SUBTRACT F90 /check:noflawed_pentium
# ADD BASE CPP /nologo /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_CONSOLE" /D "_MBCS" /YX /FD /c
# ADD CPP /nologo /MLd /W3 /Gi /GX /Od /D "WIN32" /D "NDEBUG" /D "_CONSOLE" /D "_MBCS" /YX /FD /c
# ADD BASE RSC /l 0x409 /d "NDEBUG"
# ADD RSC /l 0x409 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:console /machine:I386
# ADD LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:console /machine:I386
# SUBTRACT LINK32 /debug

!ELSEIF  "$(CFG)" == "post_processing - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE F90 /check:bounds /compile_only /debug:full /include:"Debug/" /nologo /traceback /warn:argument_checking /warn:nofileopt
# ADD F90 /check:bounds /compile_only /debug:full /define:"_MSC_VER" /fpp /iface:nomixed_str_len_arg /iface:cref /include:"Debug/" /nodefine /nologo /traceback /warn:argument_checking /warn:nofileopt
# ADD BASE CPP /nologo /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_CONSOLE" /D "_MBCS" /YX /FD /GZ /c
# ADD CPP /nologo /W3 /Gm /Gi /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_CONSOLE" /D "_MBCS" /YX /FD /GZ /c
# ADD BASE RSC /l 0x409 /d "_DEBUG"
# ADD RSC /l 0x409 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:console /debug /machine:I386 /pdbtype:sept
# ADD LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /stack:0x3b9aca00 /subsystem:console /incremental:no /debug /machine:I386 /out:"Debug/res2matlab.exe" /pdbtype:sept
# SUBTRACT LINK32 /pdb:none

!ENDIF 

# Begin Target

# Name "post_processing - Win32 Release"
# Name "post_processing - Win32 Debug"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat;f90;for;f;fpp"
# Begin Source File

SOURCE=..\additional_nodes.f90
NODEP_F90_ADDIT=\
	".\Release\debug_vars.mod"\
	".\Release\precision.mod"\
	".\Release\wlt_vars.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\debug_vars.f90
# End Source File
# Begin Source File

SOURCE=..\elliptic_solve_3d_vars.f90
NODEP_F90_ELLIP=\
	".\Release\precision.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\input_files_reader.f90
NODEP_F90_INPUT=\
	".\Release\debug_vars.mod"\
	".\Release\precision.mod"\
	
# End Source File
# Begin Source File

SOURCE="..\C++\io.cxx"
# End Source File
# Begin Source File

SOURCE=..\io_3d.f90
NODEP_F90_IO_3D=\
	".\Release\additional_nodes.mod"\
	".\Release\debug_vars.mod"\
	".\Release\elliptic_vars.mod"\
	".\Release\field.mod"\
	".\Release\input_file_reader.mod"\
	".\Release\io_3D_vars.mod"\
	".\Release\misc_vars.mod"\
	".\Release\parallel.mod"\
	".\Release\pde.mod"\
	".\Release\penalization.mod"\
	".\Release\precision.mod"\
	".\Release\share_consts.mod"\
	".\Release\share_kry.mod"\
	".\Release\sizes.mod"\
	".\Release\wavelet_filters_vars.mod"\
	".\Release\wlt_trns_mod.mod"\
	".\Release\wlt_trns_util_mod.mod"\
	".\Release\wlt_trns_vars.mod"\
	".\Release\wlt_vars.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\io_3d_vars.f90
# End Source File
# Begin Source File

SOURCE=..\parallel.f90
NODEP_F90_PARAL=\
	".\Release\db_tree_vars.mod"\
	".\Release\mpif.h"\
	".\Release\pde.mod"\
	".\Release\precision.mod"\
	".\Release\user_case_db.mod"\
	".\Release\wlt_vars.mod"\
	
# End Source File
# Begin Source File

SOURCE=.\res2matlab.f90
NODEP_F90_RES2M=\
	".\Release\field.mod"\
	".\Release\io_3D.mod"\
	".\Release\io_3D_vars.mod"\
	".\Release\misc_vars.mod"\
	".\Release\pde.mod"\
	".\Release\precision.mod"\
	".\Release\share_consts.mod"\
	".\Release\sizes.mod"\
	".\Release\wlt_trns_vars.mod"\
	".\Release\wlt_vars.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\shared_modules.f90
# End Source File
# Begin Source File

SOURCE=..\user_case_stubs.f90
# End Source File
# Begin Source File

SOURCE=..\wavelet_3d.f90
NODEP_F90_WAVEL=\
	".\Release\precision.mod"\
	".\Release\sizes.mod"\
	".\Release\wlt_trns_vars.mod"\
	".\Release\wlt_vars.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\wavelet_3d_vars.f90
NODEP_F90_WAVELE=\
	".\Release\precision.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\wavelet_3d_wrk.f90
NODEP_F90_WAVELET=\
	".\Release\additional_nodes.mod"\
	".\Release\debug_vars.mod"\
	".\Release\field.mod"\
	".\Release\io_3D_vars.mod"\
	".\Release\main_vars.mod"\
	".\Release\pde.mod"\
	".\Release\precision.mod"\
	".\Release\share_consts.mod"\
	".\Release\sizes.mod"\
	".\Release\util_vars.mod"\
	".\Release\wavelet_filters_vars.mod"\
	".\Release\wlt_trns_util_mod.mod"\
	".\Release\wlt_trns_vars.mod"\
	".\Release\wlt_vars.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\wavelet_3d_wrk_vars.f90
NODEP_F90_WAVELET_=\
	".\Release\wlt_vars.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\wavelet_filters_vars.f90
NODEP_F90_WAVELET_F=\
	".\Release\precision.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\wlt_3d_main_vars.f90
NODEP_F90_WLT_3=\
	".\Release\precision.mod"\
	
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl;fi;fd"
# Begin Source File

SOURCE="..\C++\io.h"
# End Source File
# End Group
# Begin Group "Resource Files"

# PROP Default_Filter "ico;cur;bmp;dlg;rc2;rct;bin;rgs;gif;jpg;jpeg;jpe"
# End Group
# End Target
# End Project
