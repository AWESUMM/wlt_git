# Microsoft Developer Studio Project File - Name="amira_visualization" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Console Application" 0x0103

CFG=amira_visualization - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "amira_visualization.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "amira_visualization.mak" CFG="amira_visualization - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "amira_visualization - Win32 Release" (based on "Win32 (x86) Console Application")
!MESSAGE "amira_visualization - Win32 Debug" (based on "Win32 (x86) Console Application")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
F90=df.exe
RSC=rc.exe

!IF  "$(CFG)" == "amira_visualization - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Target_Dir ""
# ADD BASE F90 /compile_only /include:"Release/" /nologo /warn:nofileopt
# ADD F90 /check:bounds /compile_only /include:"Release/" /nologo /warn:argument_checking /warn:nofileopt
# SUBTRACT F90 /check:noflawed_pentium
# ADD BASE CPP /nologo /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_CONSOLE" /D "_MBCS" /YX /FD /c
# ADD CPP /nologo /MLd /W3 /Gi /GX /Od /D "WIN32" /D "NDEBUG" /D "_CONSOLE" /D "_MBCS" /YX /FD /c
# ADD BASE RSC /l 0x409 /d "NDEBUG"
# ADD RSC /l 0x409 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:console /machine:I386
# ADD LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:console /machine:I386
# SUBTRACT LINK32 /debug

!ELSEIF  "$(CFG)" == "amira_visualization - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE F90 /check:bounds /compile_only /debug:full /include:"Debug/" /include:"scales1___Win32_Debug/" /nologo /traceback /warn:argument_checking /warn:nofileopt
# ADD F90 /browser /check:bounds /compile_only /debug:partial /fpp /include:"Debug/" /include:"scales1___Win32_Debug/" /nologo /optimize:0 /real_size:32 /traceback /warn:argument_checking /warn:nofileopt
# ADD BASE CPP /nologo /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_CONSOLE" /D "_MBCS" /YX /FD /GZ /c
# ADD CPP /nologo /W3 /Gm /Gi /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_CONSOLE" /D "_MBCS" /FR /YX /FD /GZ /c
# ADD BASE RSC /l 0x409 /d "_DEBUG"
# ADD RSC /l 0x409 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:console /incremental:no /debug /machine:I386 /pdbtype:sept
# ADD LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:console /incremental:no /debug /machine:I386 /pdbtype:sept

!ENDIF 

# Begin Target

# Name "amira_visualization - Win32 Release"
# Name "amira_visualization - Win32 Debug"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat;f90;for;f;fpp"
# Begin Source File

SOURCE=..\amira.visualization\c_wlt_mkviz.f90
# End Source File
# Begin Source File

SOURCE=..\..\endienness_small.f90
# End Source File
# Begin Source File

SOURCE=..\..\input_files_reader.f90
NODEP_F90_INPUT=\
	".\Debug\precision.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\..\io_3d.f90
NODEP_F90_IO_3D=\
	".\Debug\endianness_mod.mod"\
	".\Debug\field.mod"\
	".\Debug\input_file_reader.mod"\
	".\Debug\pde.mod"\
	".\Debug\precision.mod"\
	".\Debug\share_consts.mod"\
	".\Debug\share_kry.mod"\
	".\Debug\share_vars.mod"\
	".\Debug\sizes.mod"\
	".\Debug\wlt_trns_mod.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\..\shared_modules.f90
# End Source File
# Begin Source File

SOURCE=..\..\vector_util.f90
NODEP_F90_VECTO=\
	".\Debug\pde.mod"\
	".\Debug\precision.mod"\
	".\Debug\share_vars.mod"\
	".\Debug\wlt_trns_mod.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\..\wavelet_3d.f90
NODEP_F90_WAVEL=\
	".\Debug\precision.mod"\
	".\Debug\share_vars.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\..\wavelet_3d_wrk.f90
NODEP_F90_WAVELE=\
	".\Debug\field.mod"\
	".\Debug\pde.mod"\
	".\Debug\precision.mod"\
	".\Debug\share_consts.mod"\
	".\Debug\share_vars.mod"\
	".\Debug\sizes.mod"\
	".\Debug\wlt_trns_util_mod.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\amira.visualization\write_amira_am_format.f90
NODEP_F90_WRITE=\
	".\Debug\pde.mod"\
	".\Debug\precision.mod"\
	".\Debug\share_consts.mod"\
	".\Debug\share_vars.mod"\
	".\Debug\sizes.mod"\
	".\Debug\wlt_trns_util_mod.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\amira.visualization\write_amira_psi_format.f90
NODEP_F90_WRITE_=\
	".\Debug\pde.mod"\
	".\Debug\precision.mod"\
	".\Debug\share_consts.mod"\
	".\Debug\share_vars.mod"\
	".\Debug\sizes.mod"\
	".\Debug\wlt_trns_util_mod.mod"\
	
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl;fi;fd"
# End Group
# Begin Group "Resource Files"

# PROP Default_Filter "ico;cur;bmp;dlg;rc2;rct;bin;rgs;gif;jpg;jpeg;jpe"
# End Group
# End Target
# End Project
