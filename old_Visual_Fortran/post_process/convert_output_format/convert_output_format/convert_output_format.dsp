# Microsoft Developer Studio Project File - Name="convert_output_format" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Console Application" 0x0103

CFG=convert_output_format - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "convert_output_format.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "convert_output_format.mak" CFG="convert_output_format - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "convert_output_format - Win32 Release" (based on "Win32 (x86) Console Application")
!MESSAGE "convert_output_format - Win32 Debug" (based on "Win32 (x86) Console Application")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
F90=df.exe
RSC=rc.exe

!IF  "$(CFG)" == "convert_output_format - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Target_Dir ""
# ADD BASE F90 /compile_only /include:"Release/" /nologo /warn:nofileopt
# ADD F90 /check:bounds /compile_only /include:"Release/" /nologo /warn:argument_checking /warn:nofileopt
# SUBTRACT F90 /check:noflawed_pentium
# ADD BASE CPP /nologo /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_CONSOLE" /D "_MBCS" /YX /FD /c
# ADD CPP /nologo /MLd /W3 /Gi /GX /Od /D "WIN32" /D "NDEBUG" /D "_CONSOLE" /D "_MBCS" /YX /FD /c
# ADD BASE RSC /l 0x409 /d "NDEBUG"
# ADD RSC /l 0x409 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:console /machine:I386
# ADD LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:console /machine:I386
# SUBTRACT LINK32 /debug

!ELSEIF  "$(CFG)" == "convert_output_format - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE F90 /check:bounds /compile_only /debug:full /include:"Debug/" /nologo /traceback /warn:argument_checking /warn:nofileopt
# ADD F90 /check:bounds /compile_only /debug:partial /fpp /include:"Debug/" /nologo /optimize:3 /real_size:32 /traceback /warn:argument_checking /warn:nofileopt
# ADD BASE CPP /nologo /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_CONSOLE" /D "_MBCS" /YX /FD /GZ /c
# ADD CPP /nologo /W3 /Gm /Gi /GX /ZI /Od /I "\Program Files\vtk42\include\vtk" /D "WIN32" /D "_DEBUG" /D "_CONSOLE" /D "_MBCS" /YX /FD /GZ /c
# ADD BASE RSC /l 0x409 /d "_DEBUG"
# ADD RSC /l 0x409 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:console /debug /machine:I386 /pdbtype:sept
# ADD LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib vtkCommon.lib vtkFiltering.lib vtkIO.lib vtkGraphics.lib /nologo /stack:0x5f5e100,0x5f5e100 /subsystem:console /incremental:no /debug /machine:I386 /pdbtype:sept /libpath:"C:\Program Files\vtk42\lib\vtk"
# SUBTRACT LINK32 /pdb:none

!ENDIF 

# Begin Target

# Name "convert_output_format - Win32 Release"
# Name "convert_output_format - Win32 Debug"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat;f90;for;f;fpp"
# Begin Source File

SOURCE=..\..\..\additional_nodes.f90
NODEP_F90_ADDIT=\
	".\Debug\precision.mod"\
	".\Debug\wlt_vars.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\convert_output_format.f90
NODEP_F90_CONVE=\
	".\Debug\fft_module.mod"\
	".\Debug\field.mod"\
	".\Debug\io_3D.mod"\
	".\Debug\pde.mod"\
	".\Debug\precision.mod"\
	".\Debug\share_consts.mod"\
	".\Debug\sizes.mod"\
	".\Debug\vector_util_mod.mod"\
	".\Debug\wlt_trns_mod.mod"\
	".\Debug\write_field_mod.mod"\
	".\Debug\write_vtk_mod.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\..\..\elliptic_solve_3d_vars.f90
NODEP_F90_ELLIP=\
	".\Debug\precision.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\..\..\endienness_small.f90
# End Source File
# Begin Source File

SOURCE=..\..\..\fft\fft.f90
NODEP_F90_FFT_F=\
	".\Debug\fft_interface_module.mod"\
	".\Debug\precision.mod"\
	

!IF  "$(CFG)" == "convert_output_format - Win32 Release"

!ELSEIF  "$(CFG)" == "convert_output_format - Win32 Debug"

# ADD F90 /debug:full /real_size:32

!ENDIF 

# End Source File
# Begin Source File

SOURCE=..\..\..\fft\fft.interface.temperton.f90
NODEP_F90_FFT_I=\
	".\Debug\precision.mod"\
	

!IF  "$(CFG)" == "convert_output_format - Win32 Release"

!ELSEIF  "$(CFG)" == "convert_output_format - Win32 Debug"

# ADD F90 /debug:full /real_size:32

!ENDIF 

# End Source File
# Begin Source File

SOURCE=..\..\..\fft\fftpacktvms.f

!IF  "$(CFG)" == "convert_output_format - Win32 Release"

!ELSEIF  "$(CFG)" == "convert_output_format - Win32 Debug"

# ADD F90 /debug:full /real_size:64
# SUBTRACT F90 /check:bounds

!ENDIF 

# End Source File
# Begin Source File

SOURCE=..\..\..\input_files_reader.f90
NODEP_F90_INPUT=\
	".\Debug\precision.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\..\..\io_3d.f90
NODEP_F90_IO_3D=\
	".\Debug\additional_nodes.mod"\
	".\Debug\elliptic_vars.mod"\
	".\Debug\endianness_mod.mod"\
	".\Debug\field.mod"\
	".\Debug\input_file_reader.mod"\
	".\Debug\io_3D_vars.mod"\
	".\Debug\misc_vars.mod"\
	".\Debug\pde.mod"\
	".\Debug\penalization.mod"\
	".\Debug\precision.mod"\
	".\Debug\share_consts.mod"\
	".\Debug\share_kry.mod"\
	".\Debug\sizes.mod"\
	".\Debug\user_case.mod"\
	".\Debug\wavelet_filters_vars.mod"\
	".\Debug\wlt_trns_mod.mod"\
	".\Debug\wlt_trns_util_mod.mod"\
	".\Debug\wlt_trns_vars.mod"\
	".\Debug\wlt_vars.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\..\..\io_3d_vars.f90
# End Source File
# Begin Source File

SOURCE=..\..\..\shared_modules.f90
# End Source File
# Begin Source File

SOURCE=..\..\..\user_case_stubs.f90
# End Source File
# Begin Source File

SOURCE=..\..\..\vector_util.f90
NODEP_F90_VECTO=\
	".\Debug\pde.mod"\
	".\Debug\precision.mod"\
	".\Debug\wlt_trns_mod.mod"\
	".\Debug\wlt_vars.mod"\
	

!IF  "$(CFG)" == "convert_output_format - Win32 Release"

!ELSEIF  "$(CFG)" == "convert_output_format - Win32 Debug"

# ADD F90 /optimize:0

!ENDIF 

# End Source File
# Begin Source File

SOURCE="..\vtkXMLWriter-Golds.cxx"
# End Source File
# Begin Source File

SOURCE=..\..\..\wavelet_3d.f90
NODEP_F90_WAVEL=\
	".\Debug\precision.mod"\
	".\Debug\sizes.mod"\
	".\Debug\util_vars.mod"\
	".\Debug\wlt_trns_vars.mod"\
	".\Debug\wlt_vars.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\..\..\wavelet_3d_vars.f90
NODEP_F90_WAVELE=\
	".\Debug\precision.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\..\..\wavelet_3d_wrk.f90
NODEP_F90_WAVELET=\
	".\Debug\additional_nodes.mod"\
	".\Debug\field.mod"\
	".\Debug\io_3D_vars.mod"\
	".\Debug\pde.mod"\
	".\Debug\precision.mod"\
	".\Debug\share_consts.mod"\
	".\Debug\sizes.mod"\
	".\Debug\util_vars.mod"\
	".\Debug\wlt_trns_util_mod.mod"\
	".\Debug\wlt_trns_vars.mod"\
	".\Debug\wlt_vars.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\..\..\wavelet_3d_wrk_vars.f90
NODEP_F90_WAVELET_=\
	".\Debug\wlt_vars.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\..\..\wavelet_filters_vars.f90
NODEP_F90_WAVELET_F=\
	".\Debug\precision.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\write_raw_data.f90
NODEP_F90_WRITE=\
	".\Debug\pde.mod"\
	".\Debug\precision.mod"\
	".\Debug\share_consts.mod"\
	".\Debug\sizes.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\write_vtk_format.f90
NODEP_F90_WRITE_=\
	".\Debug\precision.mod"\
	".\Debug\share_consts.mod"\
	".\Debug\wlt_trns_util_mod.mod"\
	
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl;fi;fd"
# End Group
# Begin Group "Resource Files"

# PROP Default_Filter "ico;cur;bmp;dlg;rc2;rct;bin;rgs;gif;jpg;jpeg;jpe"
# End Group
# End Target
# End Project
