# Microsoft Developer Studio Generated NMAKE File, Based on convert_output_format.dsp
!IF "$(CFG)" == ""
CFG=convert_output_format - Win32 Debug
!MESSAGE No configuration specified. Defaulting to convert_output_format - Win32 Debug.
!ENDIF 

!IF "$(CFG)" != "convert_output_format - Win32 Release" && "$(CFG)" != "convert_output_format - Win32 Debug"
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "convert_output_format.mak" CFG="convert_output_format - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "convert_output_format - Win32 Release" (based on "Win32 (x86) Console Application")
!MESSAGE "convert_output_format - Win32 Debug" (based on "Win32 (x86) Console Application")
!MESSAGE 
!ERROR An invalid configuration is specified.
!ENDIF 

!IF "$(OS)" == "Windows_NT"
NULL=
!ELSE 
NULL=nul
!ENDIF 

CPP=cl.exe
F90=df.exe
RSC=rc.exe

!IF  "$(CFG)" == "convert_output_format - Win32 Release"

OUTDIR=.\Release
INTDIR=.\Release
# Begin Custom Macros
OutDir=.\Release
# End Custom Macros

ALL : "$(OUTDIR)\convert_output_format.exe" "$(OUTDIR)\local_share_vars.mod" "$(OUTDIR)\pde_unused.mod"


CLEAN :
	-@erase "$(INTDIR)\convert_output_format.obj"
	-@erase "$(INTDIR)\endianness_mod.mod"
	-@erase "$(INTDIR)\endienness_small.obj"
	-@erase "$(INTDIR)\field.mod"
	-@erase "$(INTDIR)\input_file_reader.mod"
	-@erase "$(INTDIR)\input_files_reader.obj"
	-@erase "$(INTDIR)\io_3D.mod"
	-@erase "$(INTDIR)\io_3d.obj"
	-@erase "$(INTDIR)\local_share_vars.mod"
	-@erase "$(INTDIR)\misc_vars.mod"
	-@erase "$(INTDIR)\pde.mod"
	-@erase "$(INTDIR)\pde_unused.mod"
	-@erase "$(INTDIR)\penalization.mod"
	-@erase "$(INTDIR)\precision.mod"
	-@erase "$(INTDIR)\share_consts.mod"
	-@erase "$(INTDIR)\share_kry.mod"
	-@erase "$(INTDIR)\shared_modules.obj"
	-@erase "$(INTDIR)\sizes.mod"
	-@erase "$(INTDIR)\util_vars.mod"
	-@erase "$(INTDIR)\wavelet_3d.obj"
	-@erase "$(INTDIR)\wavelet_3d_wrk.obj"
	-@erase "$(INTDIR)\wlt_trns_mod.mod"
	-@erase "$(INTDIR)\wlt_trns_util_mod.mod"
	-@erase "$(INTDIR)\write_amira_psi.mod"
	-@erase "$(INTDIR)\write_amira_psi_format.obj"
	-@erase "$(INTDIR)\write_field_mod.mod"
	-@erase "$(INTDIR)\write_raw_data.obj"
	-@erase "$(INTDIR)\write_vtk_format.obj"
	-@erase "$(INTDIR)\write_vtk_mod.mod"
	-@erase "$(OUTDIR)\convert_output_format.exe"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

F90_PROJ=/compile_only /include:"Release/" /nologo /warn:nofileopt /module:"Release/" /object:"Release/" 
F90_OBJS=.\Release/
CPP_PROJ=/nologo /ML /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_CONSOLE" /D "_MBCS" /Fp"$(INTDIR)\convert_output_format.pch" /YX /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /c 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\convert_output_format.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
LINK32_FLAGS=kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:console /incremental:no /pdb:"$(OUTDIR)\convert_output_format.pdb" /machine:I386 /out:"$(OUTDIR)\convert_output_format.exe" 
LINK32_OBJS= \
	"$(INTDIR)\convert_output_format.obj" \
	"$(INTDIR)\endienness_small.obj" \
	"$(INTDIR)\input_files_reader.obj" \
	"$(INTDIR)\io_3d.obj" \
	"$(INTDIR)\shared_modules.obj" \
	"$(INTDIR)\wavelet_3d.obj" \
	"$(INTDIR)\wavelet_3d_wrk.obj" \
	"$(INTDIR)\write_amira_psi_format.obj" \
	"$(INTDIR)\write_raw_data.obj" \
	"$(INTDIR)\write_vtk_format.obj"

"$(OUTDIR)\convert_output_format.exe" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ELSEIF  "$(CFG)" == "convert_output_format - Win32 Debug"

OUTDIR=.\Debug
INTDIR=.\Debug
# Begin Custom Macros
OutDir=.\Debug
# End Custom Macros

ALL : "$(OUTDIR)\convert_output_format.exe"


CLEAN :
	-@erase "$(INTDIR)\convert_output_format.obj"
	-@erase "$(INTDIR)\endienness_small.obj"
	-@erase "$(INTDIR)\input_files_reader.obj"
	-@erase "$(INTDIR)\io_3d.obj"
	-@erase "$(INTDIR)\shared_modules.obj"
	-@erase "$(INTDIR)\wavelet_3d.obj"
	-@erase "$(INTDIR)\wavelet_3d_wrk.obj"
	-@erase "$(INTDIR)\write_amira_psi_format.obj"
	-@erase "$(INTDIR)\write_raw_data.obj"
	-@erase "$(INTDIR)\write_vtk_format.obj"
	-@erase "$(OUTDIR)\convert_output_format.exe"
	-@erase "$(OUTDIR)\convert_output_format.pdb"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

F90_PROJ=/check:bounds /compile_only /debug:partial /fpp /include:"Debug/" /nologo /optimize:0 /traceback /warn:argument_checking /warn:nofileopt /module:"Debug/" /object:"Debug/" 
F90_OBJS=.\Debug/
CPP_PROJ=/nologo /MLd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_CONSOLE" /D "_MBCS" /Fp"$(INTDIR)\convert_output_format.pch" /YX /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /GZ /c 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\convert_output_format.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
LINK32_FLAGS=kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /stack:0x5f5e100,0x5f5e100 /subsystem:console /incremental:no /pdb:"$(OUTDIR)\convert_output_format.pdb" /debug /machine:I386 /out:"$(OUTDIR)\convert_output_format.exe" /pdbtype:sept 
LINK32_OBJS= \
	"$(INTDIR)\convert_output_format.obj" \
	"$(INTDIR)\endienness_small.obj" \
	"$(INTDIR)\input_files_reader.obj" \
	"$(INTDIR)\io_3d.obj" \
	"$(INTDIR)\shared_modules.obj" \
	"$(INTDIR)\wavelet_3d.obj" \
	"$(INTDIR)\wavelet_3d_wrk.obj" \
	"$(INTDIR)\write_amira_psi_format.obj" \
	"$(INTDIR)\write_raw_data.obj" \
	"$(INTDIR)\write_vtk_format.obj"

"$(OUTDIR)\convert_output_format.exe" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ENDIF 

.c{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.c{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.SUFFIXES: .fpp

.for{$(F90_OBJS)}.obj:
   $(F90) $(F90_PROJ) $<  

.f{$(F90_OBJS)}.obj:
   $(F90) $(F90_PROJ) $<  

.f90{$(F90_OBJS)}.obj:
   $(F90) $(F90_PROJ) $<  

.fpp{$(F90_OBJS)}.obj:
   $(F90) $(F90_PROJ) $<  


!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("convert_output_format.dep")
!INCLUDE "convert_output_format.dep"
!ELSE 
!MESSAGE Warning: cannot find "convert_output_format.dep"
!ENDIF 
!ENDIF 


!IF "$(CFG)" == "convert_output_format - Win32 Release" || "$(CFG)" == "convert_output_format - Win32 Debug"
SOURCE=..\convert_output_format.f90

!IF  "$(CFG)" == "convert_output_format - Win32 Release"

F90_MODOUT=\
	"local_share_vars"


"$(INTDIR)\convert_output_format.obj"	"$(INTDIR)\local_share_vars.mod" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\io_3D.mod" "$(INTDIR)\field.mod" "$(INTDIR)\pde.mod" "$(INTDIR)\precision.mod" "$(INTDIR)\share_consts.mod" "$(INTDIR)\sizes.mod" "$(INTDIR)\write_amira_psi.mod" "$(INTDIR)\write_field_mod.mod" "$(INTDIR)\write_vtk_mod.mod"
	$(F90) $(F90_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "convert_output_format - Win32 Debug"


"$(INTDIR)\convert_output_format.obj" : $(SOURCE) "$(INTDIR)"
	$(F90) $(F90_PROJ) $(SOURCE)


!ENDIF 

SOURCE=..\..\..\endienness_small.f90

!IF  "$(CFG)" == "convert_output_format - Win32 Release"

F90_MODOUT=\
	"endianness_mod"


"$(INTDIR)\endienness_small.obj"	"$(INTDIR)\endianness_mod.mod" : $(SOURCE) "$(INTDIR)"
	$(F90) $(F90_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "convert_output_format - Win32 Debug"


"$(INTDIR)\endienness_small.obj" : $(SOURCE) "$(INTDIR)"
	$(F90) $(F90_PROJ) $(SOURCE)


!ENDIF 

SOURCE=..\..\..\input_files_reader.f90

!IF  "$(CFG)" == "convert_output_format - Win32 Release"

F90_MODOUT=\
	"input_file_reader"


"$(INTDIR)\input_files_reader.obj"	"$(INTDIR)\input_file_reader.mod" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\precision.mod"
	$(F90) $(F90_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "convert_output_format - Win32 Debug"


"$(INTDIR)\input_files_reader.obj" : $(SOURCE) "$(INTDIR)"
	$(F90) $(F90_PROJ) $(SOURCE)


!ENDIF 

SOURCE=..\..\..\io_3d.f90

!IF  "$(CFG)" == "convert_output_format - Win32 Release"

F90_MODOUT=\
	"io_3D"


"$(INTDIR)\io_3d.obj"	"$(INTDIR)\io_3D.mod" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\endianness_mod.mod" "$(INTDIR)\input_file_reader.mod" "$(INTDIR)\field.mod" "$(INTDIR)\misc_vars.mod" "$(INTDIR)\pde.mod" "$(INTDIR)\penalization.mod" "$(INTDIR)\precision.mod" "$(INTDIR)\share_consts.mod" "$(INTDIR)\share_kry.mod" "$(INTDIR)\sizes.mod" "$(INTDIR)\wlt_trns_util_mod.mod" "$(INTDIR)\wlt_trns_mod.mod"
	$(F90) $(F90_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "convert_output_format - Win32 Debug"


"$(INTDIR)\io_3d.obj" : $(SOURCE) "$(INTDIR)"
	$(F90) $(F90_PROJ) $(SOURCE)


!ENDIF 

SOURCE=..\..\..\shared_modules.f90

!IF  "$(CFG)" == "convert_output_format - Win32 Release"

F90_MODOUT=\
	"field" \
	"misc_vars" \
	"pde" \
	"pde_unused" \
	"penalization" \
	"precision" \
	"share_consts" \
	"share_kry" \
	"sizes" \
	"util_vars"


"$(INTDIR)\shared_modules.obj"	"$(INTDIR)\field.mod"	"$(INTDIR)\misc_vars.mod"	"$(INTDIR)\pde.mod"	"$(INTDIR)\pde_unused.mod"	"$(INTDIR)\penalization.mod"	"$(INTDIR)\precision.mod"	"$(INTDIR)\share_consts.mod"	"$(INTDIR)\share_kry.mod"	"$(INTDIR)\sizes.mod"	"$(INTDIR)\util_vars.mod" : $(SOURCE) "$(INTDIR)"
	$(F90) $(F90_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "convert_output_format - Win32 Debug"


"$(INTDIR)\shared_modules.obj" : $(SOURCE) "$(INTDIR)"
	$(F90) $(F90_PROJ) $(SOURCE)


!ENDIF 

SOURCE=..\..\..\wavelet_3d.f90

!IF  "$(CFG)" == "convert_output_format - Win32 Release"

F90_MODOUT=\
	"wlt_trns_util_mod"


"$(INTDIR)\wavelet_3d.obj"	"$(INTDIR)\wlt_trns_util_mod.mod" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\sizes.mod" "$(INTDIR)\precision.mod"
	$(F90) $(F90_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "convert_output_format - Win32 Debug"


"$(INTDIR)\wavelet_3d.obj" : $(SOURCE) "$(INTDIR)"
	$(F90) $(F90_PROJ) $(SOURCE)


!ENDIF 

SOURCE=..\..\..\wavelet_3d_wrk.f90

!IF  "$(CFG)" == "convert_output_format - Win32 Release"

F90_MODOUT=\
	"wlt_trns_mod"


"$(INTDIR)\wavelet_3d_wrk.obj"	"$(INTDIR)\wlt_trns_mod.mod" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\precision.mod" "$(INTDIR)\wlt_trns_util_mod.mod" "$(INTDIR)\sizes.mod" "$(INTDIR)\pde.mod" "$(INTDIR)\util_vars.mod" "$(INTDIR)\share_consts.mod" "$(INTDIR)\field.mod"
	$(F90) $(F90_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "convert_output_format - Win32 Debug"


"$(INTDIR)\wavelet_3d_wrk.obj" : $(SOURCE) "$(INTDIR)"
	$(F90) $(F90_PROJ) $(SOURCE)


!ENDIF 

SOURCE=..\write_amira_psi_format.f90

!IF  "$(CFG)" == "convert_output_format - Win32 Release"

F90_MODOUT=\
	"write_amira_psi"


"$(INTDIR)\write_amira_psi_format.obj"	"$(INTDIR)\write_amira_psi.mod" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\precision.mod" "$(INTDIR)\share_consts.mod" "$(INTDIR)\pde.mod" "$(INTDIR)\sizes.mod" "$(INTDIR)\wlt_trns_util_mod.mod"
	$(F90) $(F90_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "convert_output_format - Win32 Debug"


"$(INTDIR)\write_amira_psi_format.obj" : $(SOURCE) "$(INTDIR)"
	$(F90) $(F90_PROJ) $(SOURCE)


!ENDIF 

SOURCE=..\write_raw_data.f90

!IF  "$(CFG)" == "convert_output_format - Win32 Release"

F90_MODOUT=\
	"write_field_mod"


"$(INTDIR)\write_raw_data.obj"	"$(INTDIR)\write_field_mod.mod" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\precision.mod" "$(INTDIR)\share_consts.mod" "$(INTDIR)\pde.mod" "$(INTDIR)\sizes.mod"
	$(F90) $(F90_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "convert_output_format - Win32 Debug"


"$(INTDIR)\write_raw_data.obj" : $(SOURCE) "$(INTDIR)"
	$(F90) $(F90_PROJ) $(SOURCE)


!ENDIF 

SOURCE=..\write_vtk_format.f90

!IF  "$(CFG)" == "convert_output_format - Win32 Release"

F90_MODOUT=\
	"write_vtk_mod"


"$(INTDIR)\write_vtk_format.obj"	"$(INTDIR)\write_vtk_mod.mod" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\precision.mod" "$(INTDIR)\wlt_trns_util_mod.mod"
	$(F90) $(F90_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "convert_output_format - Win32 Debug"


"$(INTDIR)\write_vtk_format.obj" : $(SOURCE) "$(INTDIR)"
	$(F90) $(F90_PROJ) $(SOURCE)


!ENDIF 


!ENDIF 

