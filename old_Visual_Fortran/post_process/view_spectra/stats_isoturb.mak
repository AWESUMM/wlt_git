# Microsoft Developer Studio Generated NMAKE File, Based on stats_isoturb.dsp
!IF "$(CFG)" == ""
CFG=stats_isoturb - Win32 Debug
!MESSAGE No configuration specified. Defaulting to stats_isoturb - Win32 Debug.
!ENDIF 

!IF "$(CFG)" != "stats_isoturb - Win32 Release" && "$(CFG)" != "stats_isoturb - Win32 Debug"
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "stats_isoturb.mak" CFG="stats_isoturb - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "stats_isoturb - Win32 Release" (based on "Win32 (x86) Console Application")
!MESSAGE "stats_isoturb - Win32 Debug" (based on "Win32 (x86) Console Application")
!MESSAGE 
!ERROR An invalid configuration is specified.
!ENDIF 

!IF "$(OS)" == "Windows_NT"
NULL=
!ELSE 
NULL=nul
!ENDIF 

CPP=cl.exe
F90=df.exe
RSC=rc.exe

!IF  "$(CFG)" == "stats_isoturb - Win32 Release"

OUTDIR=.\Release
INTDIR=.\Release
# Begin Custom Macros
OutDir=.\Release
# End Custom Macros

ALL : "$(OUTDIR)\stats_isoturb.exe" "$(OUTDIR)\fft_util.mod" "$(OUTDIR)\pde_unused.mod"


CLEAN :
	-@erase "$(INTDIR)\c_wlt_turbstats.obj"
	-@erase "$(INTDIR)\elliptic_solve_3d_vars.obj"
	-@erase "$(INTDIR)\elliptic_vars.mod"
	-@erase "$(INTDIR)\endianness_mod.mod"
	-@erase "$(INTDIR)\endienness_small.obj"
	-@erase "$(INTDIR)\fft.interface.temperton.obj"
	-@erase "$(INTDIR)\fft.obj"
	-@erase "$(INTDIR)\fft_interface_module.mod"
	-@erase "$(INTDIR)\FFT_MODULE.mod"
	-@erase "$(INTDIR)\fft_util.mod"
	-@erase "$(INTDIR)\fft_util.obj"
	-@erase "$(INTDIR)\fftpacktvms.obj"
	-@erase "$(INTDIR)\field.mod"
	-@erase "$(INTDIR)\input_file_reader.mod"
	-@erase "$(INTDIR)\input_files_reader.obj"
	-@erase "$(INTDIR)\io_3D.mod"
	-@erase "$(INTDIR)\io_3d.obj"
	-@erase "$(INTDIR)\io_3D_vars.mod"
	-@erase "$(INTDIR)\io_3d_vars.obj"
	-@erase "$(INTDIR)\misc_vars.mod"
	-@erase "$(INTDIR)\pde.mod"
	-@erase "$(INTDIR)\pde_unused.mod"
	-@erase "$(INTDIR)\penalization.mod"
	-@erase "$(INTDIR)\precision.mod"
	-@erase "$(INTDIR)\share_consts.mod"
	-@erase "$(INTDIR)\share_kry.mod"
	-@erase "$(INTDIR)\shared_modules.obj"
	-@erase "$(INTDIR)\sizes.mod"
	-@erase "$(INTDIR)\spectra.obj"
	-@erase "$(INTDIR)\SPECTRA_MODULE.mod"
	-@erase "$(INTDIR)\turb_stats.obj"
	-@erase "$(INTDIR)\turbulence_statistics.mod"
	-@erase "$(INTDIR)\user_case.mod"
	-@erase "$(INTDIR)\user_case_stubs.obj"
	-@erase "$(INTDIR)\util_vars.mod"
	-@erase "$(INTDIR)\wavelet_3d.obj"
	-@erase "$(INTDIR)\wavelet_3d_vars.obj"
	-@erase "$(INTDIR)\wavelet_3d_wrk.obj"
	-@erase "$(INTDIR)\wavelet_3d_wrk_vars.obj"
	-@erase "$(INTDIR)\wavelet_filters_vars.mod"
	-@erase "$(INTDIR)\wavelet_filters_vars.obj"
	-@erase "$(INTDIR)\wlt_trns_mod.mod"
	-@erase "$(INTDIR)\wlt_trns_util_mod.mod"
	-@erase "$(INTDIR)\wlt_trns_vars.mod"
	-@erase "$(INTDIR)\wlt_vars.mod"
	-@erase "$(OUTDIR)\stats_isoturb.exe"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

F90_PROJ=/compile_only /include:"Release/" /nologo /warn:nofileopt /module:"Release/" /object:"Release/" 
F90_OBJS=.\Release/
CPP_PROJ=/nologo /ML /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_CONSOLE" /D "_MBCS" /Fp"$(INTDIR)\stats_isoturb.pch" /YX /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /c 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\stats_isoturb.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
LINK32_FLAGS=kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:console /incremental:no /pdb:"$(OUTDIR)\stats_isoturb.pdb" /machine:I386 /out:"$(OUTDIR)\stats_isoturb.exe" 
LINK32_OBJS= \
	"$(INTDIR)\c_wlt_turbstats.obj" \
	"$(INTDIR)\endienness_small.obj" \
	"$(INTDIR)\fft.obj" \
	"$(INTDIR)\fft.interface.temperton.obj" \
	"$(INTDIR)\fft_util.obj" \
	"$(INTDIR)\fftpacktvms.obj" \
	"$(INTDIR)\input_files_reader.obj" \
	"$(INTDIR)\io_3d.obj" \
	"$(INTDIR)\shared_modules.obj" \
	"$(INTDIR)\spectra.obj" \
	"$(INTDIR)\turb_stats.obj" \
	"$(INTDIR)\user_case_stubs.obj" \
	"$(INTDIR)\wavelet_3d.obj" \
	"$(INTDIR)\wavelet_3d_wrk.obj" \
	"$(INTDIR)\wavelet_3d_vars.obj" \
	"$(INTDIR)\wavelet_filters_vars.obj" \
	"$(INTDIR)\elliptic_solve_3d_vars.obj" \
	"$(INTDIR)\wavelet_3d_wrk_vars.obj" \
	"$(INTDIR)\io_3d_vars.obj"

"$(OUTDIR)\stats_isoturb.exe" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ELSEIF  "$(CFG)" == "stats_isoturb - Win32 Debug"

OUTDIR=.\Debug
INTDIR=.\Debug
# Begin Custom Macros
OutDir=.\Debug
# End Custom Macros

ALL : "$(OUTDIR)\stats_isoturb.exe" "$(OUTDIR)\pde_unused.mod" "$(OUTDIR)\fft_util.mod"


CLEAN :
	-@erase "$(INTDIR)\c_wlt_turbstats.obj"
	-@erase "$(INTDIR)\elliptic_solve_3d_vars.obj"
	-@erase "$(INTDIR)\elliptic_vars.mod"
	-@erase "$(INTDIR)\endianness_mod.mod"
	-@erase "$(INTDIR)\endienness_small.obj"
	-@erase "$(INTDIR)\fft.interface.temperton.obj"
	-@erase "$(INTDIR)\fft.obj"
	-@erase "$(INTDIR)\fft_interface_module.mod"
	-@erase "$(INTDIR)\fft_module.mod"
	-@erase "$(INTDIR)\fft_util.mod"
	-@erase "$(INTDIR)\fft_util.obj"
	-@erase "$(INTDIR)\fftpacktvms.obj"
	-@erase "$(INTDIR)\field.mod"
	-@erase "$(INTDIR)\input_file_reader.mod"
	-@erase "$(INTDIR)\input_files_reader.obj"
	-@erase "$(INTDIR)\io_3D.mod"
	-@erase "$(INTDIR)\io_3d.obj"
	-@erase "$(INTDIR)\io_3D_vars.mod"
	-@erase "$(INTDIR)\io_3d_vars.obj"
	-@erase "$(INTDIR)\misc_vars.mod"
	-@erase "$(INTDIR)\pde.mod"
	-@erase "$(INTDIR)\pde_unused.mod"
	-@erase "$(INTDIR)\penalization.mod"
	-@erase "$(INTDIR)\precision.mod"
	-@erase "$(INTDIR)\share_consts.mod"
	-@erase "$(INTDIR)\share_kry.mod"
	-@erase "$(INTDIR)\shared_modules.obj"
	-@erase "$(INTDIR)\sizes.mod"
	-@erase "$(INTDIR)\spectra.obj"
	-@erase "$(INTDIR)\SPECTRA_MODULE.mod"
	-@erase "$(INTDIR)\turb_stats.obj"
	-@erase "$(INTDIR)\turbulence_statistics.mod"
	-@erase "$(INTDIR)\user_case.mod"
	-@erase "$(INTDIR)\user_case_stubs.obj"
	-@erase "$(INTDIR)\util_vars.mod"
	-@erase "$(INTDIR)\wavelet_3d.obj"
	-@erase "$(INTDIR)\wavelet_3d_vars.obj"
	-@erase "$(INTDIR)\wavelet_3d_wrk.obj"
	-@erase "$(INTDIR)\wavelet_3d_wrk_vars.obj"
	-@erase "$(INTDIR)\wavelet_filters_vars.mod"
	-@erase "$(INTDIR)\wavelet_filters_vars.obj"
	-@erase "$(INTDIR)\wlt_trns_mod.mod"
	-@erase "$(INTDIR)\wlt_trns_util_mod.mod"
	-@erase "$(INTDIR)\wlt_trns_vars.mod"
	-@erase "$(INTDIR)\wlt_vars.mod"
	-@erase "$(OUTDIR)\stats_isoturb.exe"
	-@erase "$(OUTDIR)\stats_isoturb.pdb"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

F90_PROJ=/check:bounds /compile_only /debug:partial /fpp /include:"Debug/" /nologo /optimize:0 /real_size:64 /traceback /warn:argument_checking /warn:nofileopt /module:"Debug/" /object:"Debug/" 
F90_OBJS=.\Debug/
CPP_PROJ=/nologo /MLd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_CONSOLE" /D "_MBCS" /Fp"$(INTDIR)\stats_isoturb.pch" /YX /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /GZ /c 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\stats_isoturb.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
LINK32_FLAGS=kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /stack:0x11e1a300,0x11e1a300 /subsystem:console /incremental:no /pdb:"$(OUTDIR)\stats_isoturb.pdb" /debug /machine:I386 /out:"$(OUTDIR)\stats_isoturb.exe" /pdbtype:sept 
LINK32_OBJS= \
	"$(INTDIR)\c_wlt_turbstats.obj" \
	"$(INTDIR)\endienness_small.obj" \
	"$(INTDIR)\fft.obj" \
	"$(INTDIR)\fft.interface.temperton.obj" \
	"$(INTDIR)\fft_util.obj" \
	"$(INTDIR)\fftpacktvms.obj" \
	"$(INTDIR)\input_files_reader.obj" \
	"$(INTDIR)\io_3d.obj" \
	"$(INTDIR)\shared_modules.obj" \
	"$(INTDIR)\spectra.obj" \
	"$(INTDIR)\turb_stats.obj" \
	"$(INTDIR)\user_case_stubs.obj" \
	"$(INTDIR)\wavelet_3d.obj" \
	"$(INTDIR)\wavelet_3d_wrk.obj" \
	"$(INTDIR)\wavelet_3d_vars.obj" \
	"$(INTDIR)\wavelet_filters_vars.obj" \
	"$(INTDIR)\elliptic_solve_3d_vars.obj" \
	"$(INTDIR)\wavelet_3d_wrk_vars.obj" \
	"$(INTDIR)\io_3d_vars.obj"

"$(OUTDIR)\stats_isoturb.exe" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ENDIF 

.c{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.c{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.SUFFIXES: .fpp

.for{$(F90_OBJS)}.obj:
   $(F90) $(F90_PROJ) $<  

.f{$(F90_OBJS)}.obj:
   $(F90) $(F90_PROJ) $<  

.f90{$(F90_OBJS)}.obj:
   $(F90) $(F90_PROJ) $<  

.fpp{$(F90_OBJS)}.obj:
   $(F90) $(F90_PROJ) $<  


!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("stats_isoturb.dep")
!INCLUDE "stats_isoturb.dep"
!ELSE 
!MESSAGE Warning: cannot find "stats_isoturb.dep"
!ENDIF 
!ENDIF 


!IF "$(CFG)" == "stats_isoturb - Win32 Release" || "$(CFG)" == "stats_isoturb - Win32 Debug"
SOURCE=.\c_wlt_turbstats.f90

!IF  "$(CFG)" == "stats_isoturb - Win32 Release"


"$(INTDIR)\c_wlt_turbstats.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\FFT_MODULE.mod" "$(INTDIR)\io_3D.mod" "$(INTDIR)\field.mod" "$(INTDIR)\pde.mod" "$(INTDIR)\precision.mod" "$(INTDIR)\share_consts.mod" "$(INTDIR)\sizes.mod" "$(INTDIR)\SPECTRA_MODULE.mod" "$(INTDIR)\turbulence_statistics.mod" "$(INTDIR)\wlt_trns_mod.mod"
	$(F90) /compile_only /include:"Release/" /nologo /warn:nofileopt /module:"Release/" /object:"Release/" $(SOURCE)


!ELSEIF  "$(CFG)" == "stats_isoturb - Win32 Debug"


"$(INTDIR)\c_wlt_turbstats.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\precision.mod" "$(INTDIR)\share_consts.mod" "$(INTDIR)\pde.mod" "$(INTDIR)\sizes.mod" "$(INTDIR)\fft_module.mod" "$(INTDIR)\SPECTRA_MODULE.mod" "$(INTDIR)\wlt_trns_mod.mod" "$(INTDIR)\io_3D.mod" "$(INTDIR)\field.mod" "$(INTDIR)\turbulence_statistics.mod"
	$(F90) /check:bounds /compile_only /debug:full /fpp /include:"Debug/" /nologo /optimize:0 /real_size:64 /traceback /warn:argument_checking /warn:nofileopt /module:"Debug/" /object:"Debug/" /pdbfile:"Debug/DF60.PDB" $(SOURCE)


!ENDIF 

SOURCE=..\..\elliptic_solve_3d_vars.f90
F90_MODOUT=\
	"elliptic_vars"


"$(INTDIR)\elliptic_solve_3d_vars.obj"	"$(INTDIR)\elliptic_vars.mod" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\precision.mod"
	$(F90) $(F90_PROJ) $(SOURCE)


SOURCE=..\..\endienness_small.f90

!IF  "$(CFG)" == "stats_isoturb - Win32 Release"

F90_MODOUT=\
	"endianness_mod"


"$(INTDIR)\endienness_small.obj"	"$(INTDIR)\endianness_mod.mod" : $(SOURCE) "$(INTDIR)"
	$(F90) /compile_only /include:"Release/" /nologo /warn:nofileopt /module:"Release/" /object:"Release/" $(SOURCE)


!ELSEIF  "$(CFG)" == "stats_isoturb - Win32 Debug"

F90_MODOUT=\
	"endianness_mod"


"$(INTDIR)\endienness_small.obj"	"$(INTDIR)\endianness_mod.mod" : $(SOURCE) "$(INTDIR)"
	$(F90) /check:bounds /compile_only /debug:full /fpp /include:"Debug/" /nologo /optimize:0 /real_size:64 /traceback /warn:argument_checking /warn:nofileopt /module:"Debug/" /object:"Debug/" /pdbfile:"Debug/DF60.PDB" $(SOURCE)


!ENDIF 

SOURCE=..\..\fft\fft.f90

!IF  "$(CFG)" == "stats_isoturb - Win32 Release"

F90_MODOUT=\
	"fft_module"


"$(INTDIR)\fft.obj"	"$(INTDIR)\FFT_MODULE.mod" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\fft_interface_module.mod" "$(INTDIR)\precision.mod"
	$(F90) /compile_only /include:"Release/" /nologo /warn:nofileopt /module:"Release/" /object:"Release/" $(SOURCE)


!ELSEIF  "$(CFG)" == "stats_isoturb - Win32 Debug"

F90_MODOUT=\
	"fft_module"


"$(INTDIR)\fft.obj"	"$(INTDIR)\fft_module.mod" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\fft_interface_module.mod" "$(INTDIR)\precision.mod"
	$(F90) /check:bounds /compile_only /debug:full /fpp /include:"Debug/" /nologo /optimize:0 /real_size:64 /traceback /warn:argument_checking /warn:nofileopt /module:"Debug/" /object:"Debug/" /pdbfile:"Debug/DF60.PDB" $(SOURCE)


!ENDIF 

SOURCE=..\..\fft\fft.interface.temperton.f90

!IF  "$(CFG)" == "stats_isoturb - Win32 Release"

F90_MODOUT=\
	"fft_interface_module"


"$(INTDIR)\fft.interface.temperton.obj"	"$(INTDIR)\fft_interface_module.mod" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\precision.mod"
	$(F90) /compile_only /include:"Release/" /nologo /warn:nofileopt /module:"Release/" /object:"Release/" $(SOURCE)


!ELSEIF  "$(CFG)" == "stats_isoturb - Win32 Debug"

F90_MODOUT=\
	"fft_interface_module"


"$(INTDIR)\fft.interface.temperton.obj"	"$(INTDIR)\fft_interface_module.mod" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\precision.mod"
	$(F90) /check:bounds /compile_only /debug:full /fpp /include:"Debug/" /nologo /optimize:0 /real_size:64 /traceback /warn:argument_checking /warn:nofileopt /module:"Debug/" /object:"Debug/" /pdbfile:"Debug/DF60.PDB" $(SOURCE)


!ENDIF 

SOURCE=..\..\fft\fft_util.f90

!IF  "$(CFG)" == "stats_isoturb - Win32 Release"

F90_MODOUT=\
	"fft_util"


"$(INTDIR)\fft_util.obj"	"$(INTDIR)\fft_util.mod" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\FFT_MODULE.mod" "$(INTDIR)\precision.mod" "$(INTDIR)\wlt_vars.mod"
	$(F90) /compile_only /include:"Release/" /nologo /warn:nofileopt /module:"Release/" /object:"Release/" $(SOURCE)


!ELSEIF  "$(CFG)" == "stats_isoturb - Win32 Debug"

F90_MODOUT=\
	"fft_util"


"$(INTDIR)\fft_util.obj"	"$(INTDIR)\fft_util.mod" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\precision.mod" "$(INTDIR)\wlt_vars.mod" "$(INTDIR)\fft_module.mod"
	$(F90) /check:bounds /compile_only /debug:full /fpp /include:"Debug/" /nologo /optimize:0 /real_size:64 /traceback /warn:argument_checking /warn:nofileopt /module:"Debug/" /object:"Debug/" /pdbfile:"Debug/DF60.PDB" $(SOURCE)


!ENDIF 

SOURCE=..\..\fft\fftpacktvms.f

!IF  "$(CFG)" == "stats_isoturb - Win32 Release"


"$(INTDIR)\fftpacktvms.obj" : $(SOURCE) "$(INTDIR)"
	$(F90) /compile_only /include:"Release/" /nologo /warn:nofileopt /module:"Release/" /object:"Release/" $(SOURCE)


!ELSEIF  "$(CFG)" == "stats_isoturb - Win32 Debug"


"$(INTDIR)\fftpacktvms.obj" : $(SOURCE) "$(INTDIR)"
	$(F90) /compile_only /debug:full /fpp /include:"Debug/" /nologo /optimize:0 /real_size:64 /traceback /warn:argument_checking /warn:nofileopt /module:"Debug/" /object:"Debug/" /pdbfile:"Debug/DF60.PDB" $(SOURCE)


!ENDIF 

SOURCE=..\..\input_files_reader.f90

!IF  "$(CFG)" == "stats_isoturb - Win32 Release"

F90_MODOUT=\
	"input_file_reader"


"$(INTDIR)\input_files_reader.obj"	"$(INTDIR)\input_file_reader.mod" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\precision.mod"
	$(F90) /compile_only /include:"Release/" /nologo /warn:nofileopt /module:"Release/" /object:"Release/" $(SOURCE)


!ELSEIF  "$(CFG)" == "stats_isoturb - Win32 Debug"

F90_MODOUT=\
	"input_file_reader"


"$(INTDIR)\input_files_reader.obj"	"$(INTDIR)\input_file_reader.mod" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\precision.mod"
	$(F90) /check:bounds /compile_only /debug:full /fpp /include:"Debug/" /nologo /optimize:0 /real_size:64 /traceback /warn:argument_checking /warn:nofileopt /module:"Debug/" /object:"Debug/" /pdbfile:"Debug/DF60.PDB" $(SOURCE)


!ENDIF 

SOURCE=..\..\io_3d.f90

!IF  "$(CFG)" == "stats_isoturb - Win32 Release"

F90_MODOUT=\
	"io_3D"


"$(INTDIR)\io_3d.obj"	"$(INTDIR)\io_3D.mod" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\endianness_mod.mod" "$(INTDIR)\input_file_reader.mod" "$(INTDIR)\field.mod" "$(INTDIR)\misc_vars.mod" "$(INTDIR)\pde.mod" "$(INTDIR)\penalization.mod" "$(INTDIR)\precision.mod" "$(INTDIR)\share_consts.mod" "$(INTDIR)\share_kry.mod" "$(INTDIR)\sizes.mod" "$(INTDIR)\user_case.mod" "$(INTDIR)\wlt_trns_util_mod.mod" "$(INTDIR)\wlt_trns_mod.mod" "$(INTDIR)\elliptic_vars.mod" "$(INTDIR)\io_3D_vars.mod" "$(INTDIR)\wlt_vars.mod" "$(INTDIR)\wlt_trns_vars.mod" "$(INTDIR)\wavelet_filters_vars.mod"
	$(F90) /compile_only /include:"Release/" /nologo /warn:nofileopt /module:"Release/" /object:"Release/" $(SOURCE)


!ELSEIF  "$(CFG)" == "stats_isoturb - Win32 Debug"

F90_MODOUT=\
	"io_3D"


"$(INTDIR)\io_3d.obj"	"$(INTDIR)\io_3D.mod" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\wlt_vars.mod" "$(INTDIR)\wavelet_filters_vars.mod" "$(INTDIR)\misc_vars.mod" "$(INTDIR)\elliptic_vars.mod" "$(INTDIR)\wlt_trns_util_mod.mod" "$(INTDIR)\endianness_mod.mod" "$(INTDIR)\precision.mod" "$(INTDIR)\pde.mod" "$(INTDIR)\field.mod" "$(INTDIR)\wlt_trns_mod.mod" "$(INTDIR)\sizes.mod" "$(INTDIR)\penalization.mod" "$(INTDIR)\share_kry.mod" "$(INTDIR)\input_file_reader.mod" "$(INTDIR)\user_case.mod" "$(INTDIR)\share_consts.mod" "$(INTDIR)\wlt_trns_vars.mod" "$(INTDIR)\io_3D_vars.mod"
	$(F90) /check:bounds /compile_only /debug:full /fpp /include:"Debug/" /nologo /optimize:0 /real_size:64 /traceback /warn:argument_checking /warn:nofileopt /module:"Debug/" /object:"Debug/" /pdbfile:"Debug/DF60.PDB" $(SOURCE)


!ENDIF 

SOURCE=..\..\io_3d_vars.f90
F90_MODOUT=\
	"io_3D_vars"


"$(INTDIR)\io_3d_vars.obj"	"$(INTDIR)\io_3D_vars.mod" : $(SOURCE) "$(INTDIR)"
	$(F90) $(F90_PROJ) $(SOURCE)


SOURCE=..\..\shared_modules.f90

!IF  "$(CFG)" == "stats_isoturb - Win32 Release"

F90_MODOUT=\
	"field" \
	"misc_vars" \
	"pde" \
	"pde_unused" \
	"penalization" \
	"precision" \
	"share_consts" \
	"share_kry" \
	"sizes" \
	"util_vars"


"$(INTDIR)\shared_modules.obj"	"$(INTDIR)\field.mod"	"$(INTDIR)\misc_vars.mod"	"$(INTDIR)\pde.mod"	"$(INTDIR)\pde_unused.mod"	"$(INTDIR)\penalization.mod"	"$(INTDIR)\precision.mod"	"$(INTDIR)\share_consts.mod"	"$(INTDIR)\share_kry.mod"	"$(INTDIR)\sizes.mod"	"$(INTDIR)\util_vars.mod" : $(SOURCE) "$(INTDIR)"
	$(F90) /compile_only /include:"Release/" /nologo /warn:nofileopt /module:"Release/" /object:"Release/" $(SOURCE)


!ELSEIF  "$(CFG)" == "stats_isoturb - Win32 Debug"

F90_MODOUT=\
	"field" \
	"misc_vars" \
	"pde" \
	"pde_unused" \
	"penalization" \
	"precision" \
	"share_consts" \
	"share_kry" \
	"sizes" \
	"util_vars"


"$(INTDIR)\shared_modules.obj"	"$(INTDIR)\field.mod"	"$(INTDIR)\misc_vars.mod"	"$(INTDIR)\pde.mod"	"$(INTDIR)\pde_unused.mod"	"$(INTDIR)\penalization.mod"	"$(INTDIR)\precision.mod"	"$(INTDIR)\share_consts.mod"	"$(INTDIR)\share_kry.mod"	"$(INTDIR)\sizes.mod"	"$(INTDIR)\util_vars.mod" : $(SOURCE) "$(INTDIR)"
	$(F90) /check:bounds /compile_only /debug:full /fpp /include:"Debug/" /nologo /optimize:0 /real_size:64 /traceback /warn:argument_checking /warn:nofileopt /module:"Debug/" /object:"Debug/" /pdbfile:"Debug/DF60.PDB" $(SOURCE)


!ENDIF 

SOURCE=..\..\fft\spectra.f90

!IF  "$(CFG)" == "stats_isoturb - Win32 Release"

F90_MODOUT=\
	"spectra_module"


"$(INTDIR)\spectra.obj"	"$(INTDIR)\SPECTRA_MODULE.mod" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\FFT_MODULE.mod"
	$(F90) /compile_only /include:"Release/" /nologo /warn:nofileopt /module:"Release/" /object:"Release/" $(SOURCE)


!ELSEIF  "$(CFG)" == "stats_isoturb - Win32 Debug"

F90_MODOUT=\
	"spectra_module"


"$(INTDIR)\spectra.obj"	"$(INTDIR)\SPECTRA_MODULE.mod" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\fft_module.mod"
	$(F90) /check:bounds /compile_only /debug:full /fpp /include:"Debug/" /nologo /optimize:0 /real_size:64 /traceback /warn:argument_checking /warn:nofileopt /module:"Debug/" /object:"Debug/" /pdbfile:"Debug/DF60.PDB" $(SOURCE)


!ENDIF 

SOURCE=.\turb_stats.f90

!IF  "$(CFG)" == "stats_isoturb - Win32 Release"

F90_MODOUT=\
	"turbulence_statistics"


"$(INTDIR)\turb_stats.obj"	"$(INTDIR)\turbulence_statistics.mod" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\precision.mod" "$(INTDIR)\FFT_MODULE.mod"
	$(F90) /compile_only /include:"Release/" /nologo /warn:nofileopt /module:"Release/" /object:"Release/" $(SOURCE)


!ELSEIF  "$(CFG)" == "stats_isoturb - Win32 Debug"

F90_MODOUT=\
	"turbulence_statistics"


"$(INTDIR)\turb_stats.obj"	"$(INTDIR)\turbulence_statistics.mod" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\precision.mod" "$(INTDIR)\fft_module.mod"
	$(F90) /check:bounds /compile_only /debug:full /fpp /include:"Debug/" /nologo /optimize:0 /real_size:64 /traceback /warn:argument_checking /warn:nofileopt /module:"Debug/" /object:"Debug/" /pdbfile:"Debug/DF60.PDB" $(SOURCE)


!ENDIF 

SOURCE=..\user_case_stubs.f90
F90_MODOUT=\
	"user_case"


"$(INTDIR)\user_case_stubs.obj"	"$(INTDIR)\user_case.mod" : $(SOURCE) "$(INTDIR)"
	$(F90) $(F90_PROJ) $(SOURCE)


SOURCE=..\..\wavelet_3d.f90

!IF  "$(CFG)" == "stats_isoturb - Win32 Release"

F90_MODOUT=\
	"wlt_trns_util_mod"


"$(INTDIR)\wavelet_3d.obj"	"$(INTDIR)\wlt_trns_util_mod.mod" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\sizes.mod" "$(INTDIR)\precision.mod" "$(INTDIR)\wlt_vars.mod" "$(INTDIR)\wlt_trns_vars.mod"
	$(F90) /compile_only /include:"Release/" /nologo /warn:nofileopt /module:"Release/" /object:"Release/" $(SOURCE)


!ELSEIF  "$(CFG)" == "stats_isoturb - Win32 Debug"

F90_MODOUT=\
	"wlt_trns_util_mod"


"$(INTDIR)\wavelet_3d.obj"	"$(INTDIR)\wlt_trns_util_mod.mod" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\sizes.mod" "$(INTDIR)\precision.mod" "$(INTDIR)\wlt_vars.mod" "$(INTDIR)\wlt_trns_vars.mod"
	$(F90) /check:bounds /compile_only /debug:full /fpp /include:"Debug/" /nologo /optimize:0 /real_size:64 /traceback /warn:argument_checking /warn:nofileopt /module:"Debug/" /object:"Debug/" /pdbfile:"Debug/DF60.PDB" $(SOURCE)


!ENDIF 

SOURCE=..\..\wavelet_3d_vars.f90
F90_MODOUT=\
	"wlt_vars"


"$(INTDIR)\wavelet_3d_vars.obj"	"$(INTDIR)\wlt_vars.mod" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\precision.mod"
	$(F90) $(F90_PROJ) $(SOURCE)


SOURCE=..\..\wavelet_3d_wrk.f90

!IF  "$(CFG)" == "stats_isoturb - Win32 Release"

F90_MODOUT=\
	"wlt_trns_mod"


"$(INTDIR)\wavelet_3d_wrk.obj"	"$(INTDIR)\wlt_trns_mod.mod" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\precision.mod" "$(INTDIR)\wlt_trns_util_mod.mod" "$(INTDIR)\sizes.mod" "$(INTDIR)\pde.mod" "$(INTDIR)\util_vars.mod" "$(INTDIR)\share_consts.mod" "$(INTDIR)\field.mod" "$(INTDIR)\io_3D_vars.mod" "$(INTDIR)\wlt_vars.mod" "$(INTDIR)\wlt_trns_vars.mod"
	$(F90) /compile_only /include:"Release/" /nologo /warn:nofileopt /module:"Release/" /object:"Release/" $(SOURCE)


!ELSEIF  "$(CFG)" == "stats_isoturb - Win32 Debug"

F90_MODOUT=\
	"wlt_trns_mod"


"$(INTDIR)\wavelet_3d_wrk.obj"	"$(INTDIR)\wlt_trns_mod.mod" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\precision.mod" "$(INTDIR)\wlt_vars.mod" "$(INTDIR)\wlt_trns_util_mod.mod" "$(INTDIR)\sizes.mod" "$(INTDIR)\pde.mod" "$(INTDIR)\util_vars.mod" "$(INTDIR)\share_consts.mod" "$(INTDIR)\field.mod" "$(INTDIR)\wlt_trns_vars.mod" "$(INTDIR)\io_3D_vars.mod"
	$(F90) /check:bounds /compile_only /debug:full /fpp /include:"Debug/" /nologo /optimize:0 /real_size:64 /traceback /warn:argument_checking /warn:nofileopt /module:"Debug/" /object:"Debug/" /pdbfile:"Debug/DF60.PDB" $(SOURCE)


!ENDIF 

SOURCE=..\..\wavelet_3d_wrk_vars.f90
F90_MODOUT=\
	"wlt_trns_vars"


"$(INTDIR)\wavelet_3d_wrk_vars.obj"	"$(INTDIR)\wlt_trns_vars.mod" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\wlt_vars.mod"
	$(F90) $(F90_PROJ) $(SOURCE)


SOURCE=..\..\wavelet_filters_vars.f90
F90_MODOUT=\
	"wavelet_filters_vars"


"$(INTDIR)\wavelet_filters_vars.obj"	"$(INTDIR)\wavelet_filters_vars.mod" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\precision.mod"
	$(F90) $(F90_PROJ) $(SOURCE)



!ENDIF 

