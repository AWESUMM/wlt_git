# Microsoft Developer Studio Project File - Name="stats_isoturb" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Console Application" 0x0103

CFG=stats_isoturb - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "stats_isoturb.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "stats_isoturb.mak" CFG="stats_isoturb - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "stats_isoturb - Win32 Release" (based on "Win32 (x86) Console Application")
!MESSAGE "stats_isoturb - Win32 Debug" (based on "Win32 (x86) Console Application")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
F90=df.exe
RSC=rc.exe

!IF  "$(CFG)" == "stats_isoturb - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Target_Dir ""
# ADD BASE F90 /compile_only /include:"Release/" /nologo /warn:nofileopt
# ADD F90 /check:bounds /compile_only /include:"Release/" /nologo /warn:argument_checking /warn:nofileopt
# SUBTRACT F90 /check:noflawed_pentium
# ADD BASE CPP /nologo /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_CONSOLE" /D "_MBCS" /YX /FD /c
# ADD CPP /nologo /MLd /W3 /Gi /GX /Od /D "WIN32" /D "NDEBUG" /D "_CONSOLE" /D "_MBCS" /YX /FD /c
# ADD BASE RSC /l 0x409 /d "NDEBUG"
# ADD RSC /l 0x409 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:console /machine:I386
# ADD LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:console /machine:I386
# SUBTRACT LINK32 /debug

!ELSEIF  "$(CFG)" == "stats_isoturb - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE F90 /check:bounds /compile_only /debug:full /include:"Debug/" /nologo /traceback /warn:argument_checking /warn:nofileopt
# ADD F90 /check:bounds /compile_only /debug:partial /define:"_MSC_VER" /fpp /iface:nomixed_str_len_arg /iface:cref /include:"Debug/" /nodefine /nologo /optimize:0 /traceback /warn:argument_checking /warn:nofileopt
# ADD BASE CPP /nologo /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_CONSOLE" /D "_MBCS" /YX /FD /GZ /c
# ADD CPP /nologo /W3 /Gm /Gi /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_CONSOLE" /D "_MBCS" /YX /FD /GZ /c
# ADD BASE RSC /l 0x409 /d "_DEBUG"
# ADD RSC /l 0x409 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:console /debug /machine:I386 /pdbtype:sept
# ADD LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /stack:0x11e1a300,0x11e1a300 /subsystem:console /incremental:no /debug /machine:I386 /pdbtype:sept
# SUBTRACT LINK32 /pdb:none

!ENDIF 

# Begin Target

# Name "stats_isoturb - Win32 Release"
# Name "stats_isoturb - Win32 Debug"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat;f90;for;f;fpp"
# Begin Source File

SOURCE=..\..\additional_nodes.f90
DEP_F90_ADDIT=\
	".\Debug\debug_vars.mod"\
	".\Debug\precision.mod"\
	".\Debug\wlt_vars.mod"\
	
# End Source File
# Begin Source File

SOURCE=.\c_wlt_turbstats.local.f90
DEP_F90_C_WLT=\
	".\Debug\fft_module.mod"\
	".\Debug\field.mod"\
	".\Debug\io_3D.mod"\
	".\Debug\io_3D_vars.mod"\
	".\Debug\misc_vars.mod"\
	".\Debug\pde.mod"\
	".\Debug\precision.mod"\
	".\Debug\share_consts.mod"\
	".\Debug\sizes.mod"\
	".\Debug\spectra_module.mod"\
	".\Debug\turbulence_statistics.mod"\
	".\Debug\wlt_trns_mod.mod"\
	".\Debug\wlt_vars.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\..\TestCases\autoCAD_geometry\case_geometry.f90
# End Source File
# Begin Source File

SOURCE=..\..\TestCases\autoCAD_geometry\case_geometry.PARAMS.f90
# End Source File
# Begin Source File

SOURCE=..\..\WorkCases\Oleg\HomogeneousTurbulence\case_isoturb_new.PARAMS.f90
# End Source File
# Begin Source File

SOURCE=..\..\coord_mapping.f90
# End Source File
# Begin Source File

SOURCE=..\..\debug_vars.f90
# End Source File
# Begin Source File

SOURCE=..\..\elliptic_solve_3d_vars.f90
NODEP_F90_ELLIP=\
	".\Debug\precision.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\..\endienness_small.f90

!IF  "$(CFG)" == "stats_isoturb - Win32 Release"

!ELSEIF  "$(CFG)" == "stats_isoturb - Win32 Debug"

# ADD F90 /debug:full

!ENDIF 

# End Source File
# Begin Source File

SOURCE=..\..\fft\fft.f90
NODEP_F90_FFT_F=\
	".\Debug\debug_vars.mod"\
	".\Debug\fft_interface_module.mod"\
	".\Debug\precision.mod"\
	

!IF  "$(CFG)" == "stats_isoturb - Win32 Release"

!ELSEIF  "$(CFG)" == "stats_isoturb - Win32 Debug"

# ADD F90 /debug:full

!ENDIF 

# End Source File
# Begin Source File

SOURCE=..\..\fft\fft.interface.temperton.f90
NODEP_F90_FFT_I=\
	".\Debug\debug_vars.mod"\
	".\Debug\precision.mod"\
	

!IF  "$(CFG)" == "stats_isoturb - Win32 Release"

!ELSEIF  "$(CFG)" == "stats_isoturb - Win32 Debug"

# ADD F90 /debug:full

!ENDIF 

# End Source File
# Begin Source File

SOURCE=..\..\fft\fft_util.f90
NODEP_F90_FFT_U=\
	".\Debug\debug_vars.mod"\
	".\Debug\fft_module.mod"\
	".\Debug\precision.mod"\
	".\Debug\wlt_vars.mod"\
	

!IF  "$(CFG)" == "stats_isoturb - Win32 Release"

!ELSEIF  "$(CFG)" == "stats_isoturb - Win32 Debug"

# ADD F90 /debug:full

!ENDIF 

# End Source File
# Begin Source File

SOURCE=..\..\fft\fftpacktvms.f

!IF  "$(CFG)" == "stats_isoturb - Win32 Release"

!ELSEIF  "$(CFG)" == "stats_isoturb - Win32 Debug"

# ADD F90 /debug:full /real_size:64
# SUBTRACT F90 /check:bounds

!ENDIF 

# End Source File
# Begin Source File

SOURCE=..\..\input_files_reader.f90
NODEP_F90_INPUT=\
	".\Debug\debug_vars.mod"\
	".\Debug\precision.mod"\
	

!IF  "$(CFG)" == "stats_isoturb - Win32 Release"

!ELSEIF  "$(CFG)" == "stats_isoturb - Win32 Debug"

# ADD F90 /debug:full

!ENDIF 

# End Source File
# Begin Source File

SOURCE="..\..\C++\io.cxx"
# End Source File
# Begin Source File

SOURCE=..\..\io_3d.f90
DEP_F90_IO_3D=\
	".\Debug\additional_nodes.mod"\
	".\Debug\debug_vars.mod"\
	".\Debug\elliptic_vars.mod"\
	".\Debug\endianness_mod.mod"\
	".\Debug\field.mod"\
	".\Debug\input_file_reader.mod"\
	".\Debug\io_3D_vars.mod"\
	".\Debug\misc_vars.mod"\
	".\Debug\pde.mod"\
	".\Debug\penalization.mod"\
	".\Debug\precision.mod"\
	".\Debug\share_consts.mod"\
	".\Debug\share_kry.mod"\
	".\Debug\sizes.mod"\
	".\Debug\wavelet_filters_vars.mod"\
	".\Debug\wlt_trns_mod.mod"\
	".\Debug\wlt_trns_util_mod.mod"\
	".\Debug\wlt_trns_vars.mod"\
	".\Debug\wlt_vars.mod"\
	

!IF  "$(CFG)" == "stats_isoturb - Win32 Release"

!ELSEIF  "$(CFG)" == "stats_isoturb - Win32 Debug"

# ADD F90 /debug:full

!ENDIF 

# End Source File
# Begin Source File

SOURCE=..\..\io_3d_vars.f90
# End Source File
# Begin Source File

SOURCE=..\..\shared_modules.f90

!IF  "$(CFG)" == "stats_isoturb - Win32 Release"

!ELSEIF  "$(CFG)" == "stats_isoturb - Win32 Debug"

# ADD F90 /debug:full

!ENDIF 

# End Source File
# Begin Source File

SOURCE=..\..\fft\spectra.f90
NODEP_F90_SPECT=\
	".\Debug\debug_vars.mod"\
	".\Debug\fft_module.mod"\
	

!IF  "$(CFG)" == "stats_isoturb - Win32 Release"

!ELSEIF  "$(CFG)" == "stats_isoturb - Win32 Debug"

# ADD F90 /debug:full

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\turb_stats.local.f90
DEP_F90_TURB_=\
	".\Debug\fft_module.mod"\
	".\Debug\precision.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\..\user_case_stubs.f90
# End Source File
# Begin Source File

SOURCE=..\..\util_3d.f90
DEP_F90_UTIL_=\
	".\Debug\debug_vars.mod"\
	".\Debug\misc_vars.mod"\
	".\Debug\pde.mod"\
	".\Debug\precision.mod"\
	".\Debug\sizes.mod"\
	".\Debug\util_vars.mod"\
	".\Debug\wavelet_filters_vars.mod"\
	".\Debug\wlt_trns_mod.mod"\
	".\Debug\wlt_vars.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\..\vector_util.f90
DEP_F90_VECTO=\
	".\Debug\debug_vars.mod"\
	".\Debug\pde.mod"\
	".\Debug\precision.mod"\
	".\Debug\wlt_trns_mod.mod"\
	".\Debug\wlt_vars.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\..\wavelet_3d.f90
DEP_F90_WAVEL=\
	".\Debug\debug_vars.mod"\
	".\Debug\precision.mod"\
	".\Debug\sizes.mod"\
	".\Debug\util_vars.mod"\
	".\Debug\wlt_trns_vars.mod"\
	".\Debug\wlt_vars.mod"\
	

!IF  "$(CFG)" == "stats_isoturb - Win32 Release"

!ELSEIF  "$(CFG)" == "stats_isoturb - Win32 Debug"

# ADD F90 /debug:full

!ENDIF 

# End Source File
# Begin Source File

SOURCE=..\..\wavelet_3d_vars.f90
DEP_F90_WAVELE=\
	".\Debug\precision.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\..\wavelet_3d_wrk.f90
DEP_F90_WAVELET=\
	".\Debug\additional_nodes.mod"\
	".\Debug\debug_vars.mod"\
	".\Debug\field.mod"\
	".\Debug\io_3D_vars.mod"\
	".\Debug\main_vars.mod"\
	".\Debug\pde.mod"\
	".\Debug\precision.mod"\
	".\Debug\share_consts.mod"\
	".\Debug\sizes.mod"\
	".\Debug\util_vars.mod"\
	".\Debug\wavelet_filters_vars.mod"\
	".\Debug\wlt_trns_util_mod.mod"\
	".\Debug\wlt_trns_vars.mod"\
	".\Debug\wlt_vars.mod"\
	

!IF  "$(CFG)" == "stats_isoturb - Win32 Release"

!ELSEIF  "$(CFG)" == "stats_isoturb - Win32 Debug"

# ADD F90 /debug:full

!ENDIF 

# End Source File
# Begin Source File

SOURCE=..\..\wavelet_3d_wrk_vars.f90
DEP_F90_WAVELET_=\
	".\Debug\wlt_vars.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\..\wavelet_filters.f90
DEP_F90_WAVELET_F=\
	".\Debug\debug_vars.mod"\
	".\Debug\pde.mod"\
	".\Debug\precision.mod"\
	".\Debug\sizes.mod"\
	".\Debug\wavelet_filters_vars.mod"\
	".\Debug\wlt_trns_mod.mod"\
	".\Debug\wlt_trns_util_mod.mod"\
	".\Debug\wlt_trns_vars.mod"\
	".\Debug\wlt_vars.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\..\wavelet_filters_vars.f90
NODEP_F90_WAVELET_FI=\
	".\Debug\precision.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\..\wlt_3d_main_vars.f90
NODEP_F90_WLT_3=\
	".\Debug\precision.mod"\
	
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl;fi;fd"
# Begin Source File

SOURCE="..\..\C++\io.h"
# End Source File
# End Group
# Begin Group "Resource Files"

# PROP Default_Filter "ico;cur;bmp;dlg;rc2;rct;bin;rgs;gif;jpg;jpeg;jpe"
# End Group
# End Target
# End Project
