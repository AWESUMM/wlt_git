# Microsoft Developer Studio Project File - Name="res2vis" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Console Application" 0x0103

CFG=res2vis - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "res2vis.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "res2vis.mak" CFG="res2vis - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "res2vis - Win32 Release" (based on "Win32 (x86) Console Application")
!MESSAGE "res2vis - Win32 Debug" (based on "Win32 (x86) Console Application")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
F90=df.exe
RSC=rc.exe

!IF  "$(CFG)" == "res2vis - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE F90 /compile_only /nologo /warn:nofileopt
# ADD F90 /check:bounds /compile_only /dbglibs /debug:none /iface:nomixed_str_len_arg /iface:cref /nologo /optimize:3 /warn:argument_checking /warn:nofileopt
# SUBTRACT F90 /check:noflawed_pentium /traceback
# ADD BASE CPP /nologo /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_CONSOLE" /D "_MBCS" /YX /FD /c
# ADD CPP /nologo /MLd /W3 /Gi /GX /Od /D "WIN32" /D "NDEBUG" /D "_CONSOLE" /D "_MBCS" /D "CGNS_LIB" /FR /YX /FD /c
# ADD BASE RSC /l 0x409 /d "NDEBUG"
# ADD RSC /l 0x409 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:console /machine:I386
# ADD LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib C:\Oleg_Vasilyev\Research\FORTRAN\work_wlt_3d_DBs\trunk\cgnslib_2.4\WIN32\libcgns.lib /nologo /stack:0x3b9aca00 /subsystem:console /incremental:yes /machine:I386 /pdbtype:sept
# SUBTRACT LINK32 /debug

!ELSEIF  "$(CFG)" == "res2vis - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE F90 /check:bounds /compile_only /dbglibs /debug:full /nologo /traceback /warn:argument_checking /warn:nofileopt
# ADD F90 /browser /check:bounds /check:format /check:noflawed_pentium /check:output_conversion /compile_only /dbglibs /debug:full /define:TREE_VERSION=0 /define:DECLARE_NODE_POINTER=INTEGER(pointer_pr) /define:"_MSC_VER" /fpp /fpscomp:general /fpscomp:ioformat /iface:nomixed_str_len_arg /iface:cref /nodefine /nologo /traceback /warn:argument_checking /warn:noalignments /warn:nofileopt /warn:nousage /fpp:"/m"
# SUBTRACT F90 /warn:declarations /warn:unused
# ADD BASE CPP /nologo /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_CONSOLE" /D "_MBCS" /YX /FD /GZ /c
# ADD CPP /nologo /W3 /Gm /Gi /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_CONSOLE" /D "_MBCS" /D "_AFXDLL" /D "CGNS_LIB" /FR /YX /FD /GZ /c
# ADD BASE RSC /l 0x409 /d "_DEBUG"
# ADD RSC /l 0x409 /d "_DEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:console /debug /machine:I386 /pdbtype:sept
# ADD LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /stack:0x1dcd6500 /subsystem:console /debug /machine:I386
# SUBTRACT LINK32 /incremental:no /pdbtype:<none>

!ENDIF 

# Begin Target

# Name "res2vis - Win32 Release"
# Name "res2vis - Win32 Debug"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat;f90;for;f;fpp"
# Begin Source File

SOURCE=..\additional_nodes.f90
NODEP_F90_ADDIT=\
	".\Debug\debug_vars.mod"\
	".\Debug\precision.mod"\
	".\Debug\wlt_vars.mod"\
	
# End Source File
# Begin Source File

SOURCE="..\C++\amr.cxx"
# End Source File
# Begin Source File

SOURCE=..\post_process\visualization\code1.f90
NODEP_F90_CODE1=\
	".\Debug\code_aux.mod"\
	".\Debug\debug_vars.mod"\
	".\Debug\field.mod"\
	".\Debug\io_3D.mod"\
	".\Debug\io_3D_vars.mod"\
	".\Debug\parallel.mod"\
	".\Debug\pde.mod"\
	".\Debug\precision.mod"\
	".\Debug\share_consts.mod"\
	".\Debug\sizes.mod"\
	".\Debug\tree_database_f.mod"\
	".\Debug\wlt_trns_mod.mod"\
	".\Debug\wlt_trns_vars.mod"\
	".\Debug\wlt_vars.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\post_process\visualization\code_aux.f90
NODEP_F90_CODE_=\
	".\Debug\db_tree.mod"\
	".\Debug\db_tree_vars.mod"\
	".\Debug\debug_vars.mod"\
	".\Debug\field.mod"\
	".\Debug\io_3D.mod"\
	".\Debug\parallel.mod"\
	".\Debug\pde.mod"\
	".\Debug\precision.mod"\
	".\Debug\share_consts.mod"\
	".\Debug\sizes.mod"\
	".\Debug\tree_database_f.mod"\
	".\Debug\wlt_trns_mod.mod"\
	".\Debug\wlt_trns_util_mod.mod"\
	".\Debug\wlt_trns_vars.mod"\
	".\Debug\wlt_vars.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\coord_mapping.f90
# End Source File
# Begin Source File

SOURCE=..\db_tree.f90
NODEP_F90_DB_TR=\
	".\Debug\additional_nodes.mod"\
	".\Debug\db_tree_vars.mod"\
	".\Debug\debug_vars.mod"\
	".\Debug\io_3D_vars.mod"\
	".\Debug\parallel.mod"\
	".\Debug\precision.mod"\
	".\Debug\share_consts.mod"\
	".\Debug\sizes.mod"\
	".\Debug\tree_database_f.mod"\
	".\Debug\util_vars.mod"\
	".\Debug\wlt_trns_util_mod.mod"\
	".\Debug\wlt_trns_vars.mod"\
	".\Debug\wlt_vars.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\db_tree_vars.f90
NODEP_F90_DB_TRE=\
	".\Debug\precision.mod"\
	".\Debug\user_case_db.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\debug_vars.f90
# End Source File
# Begin Source File

SOURCE="\My Desktop\work_wlt_3d_DBs\trunk\domain_geometric.f90"
# End Source File
# Begin Source File

SOURCE="\My Desktop\work_wlt_3d_DBs\trunk\domain_zoltan_empty.f90"
NODEP_F90_DOMAI=\
	".\Debug\precision.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\elliptic_solve_3d_vars.f90
NODEP_F90_ELLIP=\
	".\Debug\precision.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\input_files_reader.f90
NODEP_F90_INPUT=\
	".\Debug\debug_vars.mod"\
	".\Debug\precision.mod"\
	
# End Source File
# Begin Source File

SOURCE="..\C++\interface.cxx"
# End Source File
# Begin Source File

SOURCE="..\C++\io.cxx"
# End Source File
# Begin Source File

SOURCE=..\io_3d.f90
NODEP_F90_IO_3D=\
	".\Debug\additional_nodes.mod"\
	".\Debug\debug_vars.mod"\
	".\Debug\elliptic_vars.mod"\
	".\Debug\field.mod"\
	".\Debug\input_file_reader.mod"\
	".\Debug\io_3D_vars.mod"\
	".\Debug\parallel.mod"\
	".\Debug\pde.mod"\
	".\Debug\penalization.mod"\
	".\Debug\precision.mod"\
	".\Debug\share_consts.mod"\
	".\Debug\share_kry.mod"\
	".\Debug\sizes.mod"\
	".\Debug\wavelet_filters_vars.mod"\
	".\Debug\wlt_trns_mod.mod"\
	".\Debug\wlt_trns_util_mod.mod"\
	".\Debug\wlt_trns_vars.mod"\
	".\Debug\wlt_vars.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\io_3d_vars.f90
# End Source File
# Begin Source File

SOURCE=..\parallel.f90
NODEP_F90_PARAL=\
	".\Debug\db_tree_vars.mod"\
	".\Debug\debug_vars.mod"\
	".\Debug\domain_geometric.mod"\
	".\Debug\domain_zoltan.mod"\
	".\Debug\mpif.h"\
	".\Debug\precision.mod"\
	".\Debug\user_case_db.mod"\
	".\Debug\wlt_trns_util_mod.mod"\
	".\Debug\wlt_vars.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\shared_modules.f90
# End Source File
# Begin Source File

SOURCE="..\C++\tree.cxx"
# End Source File
# Begin Source File

SOURCE=..\user_case_stubs.f90
# End Source File
# Begin Source File

SOURCE=..\user_case_stubs.PARAMS.f90
# End Source File
# Begin Source File

SOURCE=..\util_3d.f90
NODEP_F90_UTIL_=\
	".\Debug\debug_vars.mod"\
	".\Debug\parallel.mod"\
	".\Debug\pde.mod"\
	".\Debug\precision.mod"\
	".\Debug\sizes.mod"\
	".\Debug\util_vars.mod"\
	".\Debug\wavelet_filters_vars.mod"\
	".\Debug\wlt_trns_mod.mod"\
	".\Debug\wlt_vars.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\vector_util.f90
NODEP_F90_VECTO=\
	".\Debug\debug_vars.mod"\
	".\Debug\parallel.mod"\
	".\Debug\pde.mod"\
	".\Debug\precision.mod"\
	".\Debug\share_consts.mod"\
	".\Debug\util_vars.mod"\
	".\Debug\wlt_trns_mod.mod"\
	".\Debug\wlt_vars.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\wavelet_3d.f90
NODEP_F90_WAVEL=\
	".\Debug\precision.mod"\
	".\Debug\sizes.mod"\
	".\Debug\wlt_trns_vars.mod"\
	".\Debug\wlt_vars.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\wavelet_3d_tree.f90
NODEP_F90_WAVELE=\
	".\Debug\additional_nodes.mod"\
	".\Debug\db_tree.mod"\
	".\Debug\debug_vars.mod"\
	".\Debug\field.mod"\
	".\Debug\io_3D_vars.mod"\
	".\Debug\main_vars.mod"\
	".\Debug\parallel.mod"\
	".\Debug\pde.mod"\
	".\Debug\precision.mod"\
	".\Debug\share_consts.mod"\
	".\Debug\sizes.mod"\
	".\Debug\tree_database_f.mod"\
	".\Debug\user_case_db.mod"\
	".\Debug\util_vars.mod"\
	".\Debug\wlt_trns_util_mod.mod"\
	".\Debug\wlt_trns_vars.mod"\
	".\Debug\wlt_vars.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\wavelet_3d_tree_vars.f90
NODEP_F90_WAVELET=\
	".\Debug\db_tree_vars.mod"\
	".\Debug\wlt_vars.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\wavelet_3d_vars.f90
NODEP_F90_WAVELET_=\
	".\Debug\precision.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\wavelet_filters.f90
NODEP_F90_WAVELET_F=\
	".\Debug\db_tree.mod"\
	".\Debug\db_tree_vars.mod"\
	".\Debug\debug_vars.mod"\
	".\Debug\parallel.mod"\
	".\Debug\pde.mod"\
	".\Debug\precision.mod"\
	".\Debug\sizes.mod"\
	".\Debug\wavelet_filters_vars.mod"\
	".\Debug\wlt_trns_mod.mod"\
	".\Debug\wlt_trns_util_mod.mod"\
	".\Debug\wlt_trns_vars.mod"\
	".\Debug\wlt_vars.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\wavelet_filters_vars.f90
NODEP_F90_WAVELET_FI=\
	".\Debug\precision.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\wlt_3d_main_vars.f90
NODEP_F90_WLT_3=\
	".\Debug\precision.mod"\
	
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl;fi;fd"
# Begin Source File

SOURCE="\My Desktop\work_wlt_3d_DBs\trunk\C++\amr.h"
# End Source File
# Begin Source File

SOURCE="..\C++\cgnslib.h"
# End Source File
# Begin Source File

SOURCE="..\C++\interface.h"
# End Source File
# Begin Source File

SOURCE="..\C++\io.h"
# End Source File
# Begin Source File

SOURCE="..\C++\lowercase.h"
# End Source File
# Begin Source File

SOURCE="\My Desktop\work_wlt_3d_DBs\trunk\C++\tll_tree.h"
# End Source File
# Begin Source File

SOURCE="\My Desktop\work_wlt_3d_DBs\trunk\C++\tree.h"
# End Source File
# End Group
# Begin Group "Resource Files"

# PROP Default_Filter "ico;cur;bmp;dlg;rc2;rct;bin;rgs;gif;jpg;jpeg;jpe"
# Begin Source File

SOURCE=..\cgnslib_2.4\WIN32\libcgns.lib
# End Source File
# End Group
# End Target
# End Project
