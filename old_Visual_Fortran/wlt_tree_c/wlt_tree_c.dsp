# Microsoft Developer Studio Project File - Name="wlt_tree_c" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Console Application" 0x0103

CFG=wlt_tree_c - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "wlt_tree_c.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "wlt_tree_c.mak" CFG="wlt_tree_c - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "wlt_tree_c - Win32 Release" (based on "Win32 (x86) Console Application")
!MESSAGE "wlt_tree_c - Win32 Debug" (based on "Win32 (x86) Console Application")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
F90=df.exe
RSC=rc.exe

!IF  "$(CFG)" == "wlt_tree_c - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE F90 /compile_only /nologo /warn:nofileopt
# ADD F90 /browser /check:bounds /check:format /check:output_conversion /compile_only /dbglibs /debug:none /define:"_MSC_VER" /fpp /iface:nomixed_str_len_arg /iface:cref /include:"Release/" /nodefine /nologo /optimize:0 /traceback /warn:argument_checking /warn:declarations /warn:nofileopt /warn:unused
# SUBTRACT F90 /check:noflawed_pentium /warn:nouncalled
# ADD BASE CPP /nologo /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_CONSOLE" /D "_MBCS" /YX /FD /c
# ADD CPP /nologo /MLd /W3 /Gm /Gi /GX /ZI /D "WIN32" /D "NDEBUG" /D "_CONSOLE" /D "_MBCS" /FR /YX /FD /c
# ADD BASE RSC /l 0x409 /d "NDEBUG"
# ADD RSC /l 0x409 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:console /machine:I386
# ADD LINK32 netcdf.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /stack:0x1dcd6500 /subsystem:console /debug /machine:I386 /libpath:".."

!ELSEIF  "$(CFG)" == "wlt_tree_c - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE F90 /check:bounds /compile_only /dbglibs /debug:full /nologo /traceback /warn:argument_checking /warn:nofileopt
# ADD F90 /browser /check:bounds /check:format /check:noflawed_pentium /check:power /check:output_conversion /check:overflow /compile_only /dbglibs /debug:full /define:TREE_VERSION=0 /define:DECLARE_NODE_POINTER=INTEGER(pointer_pr) /define:"_MSC_VER" /fpp /fpscomp:ioformat /iface:nomixed_str_len_arg /iface:cref /nodefine /nologo /traceback /warn:argument_checking /warn:declarations /warn:nofileopt /fpp:"/m"
# ADD BASE CPP /nologo /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_CONSOLE" /D "_MBCS" /YX /FD /GZ /c
# ADD CPP /nologo /W3 /Gm /Gi /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_CONSOLE" /D "_MBCS" /FR /YX /FD /GZ /c
# ADD BASE RSC /l 0x409 /d "_DEBUG"
# ADD RSC /l 0x409 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:console /debug /machine:I386 /pdbtype:sept
# ADD LINK32 netcdf.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /stack:0x3b9aca00 /subsystem:console /debug /machine:I386 /libpath:".."
# SUBTRACT LINK32 /pdb:none /incremental:no

!ENDIF 

# Begin Target

# Name "wlt_tree_c - Win32 Release"
# Name "wlt_tree_c - Win32 Debug"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat;f90;for;f;fpp"
# Begin Source File

SOURCE=..\additional_nodes.f90
NODEP_F90_ADDIT=\
	".\Debug\debug_vars.mod"\
	".\Debug\precision.mod"\
	".\Debug\wlt_vars.mod"\
	
# End Source File
# Begin Source File

SOURCE="..\C++\amr.cxx"
# End Source File
# Begin Source File

SOURCE=..\autoCAD_geometry.f90
NODEP_F90_AUTOC=\
	".\Debug\precision.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\fft\ch_resolution_fs.f90
NODEP_F90_CH_RE=\
	".\Debug\debug_vars.mod"\
	".\Debug\precision.mod"\
	

!IF  "$(CFG)" == "wlt_tree_c - Win32 Release"

!ELSEIF  "$(CFG)" == "wlt_tree_c - Win32 Debug"

# ADD F90 /optimize:0

!ENDIF 

# End Source File
# Begin Source File

SOURCE=..\d1mach.f

!IF  "$(CFG)" == "wlt_tree_c - Win32 Release"

!ELSEIF  "$(CFG)" == "wlt_tree_c - Win32 Debug"

# ADD F90 /optimize:0 /stand:none /warn:nouninitialized /warn:nousage
# SUBTRACT F90 /warn:declarations

!ENDIF 

# End Source File
# Begin Source File

SOURCE=..\db_tree.f90
NODEP_F90_DB_TR=\
	".\Debug\additional_nodes.mod"\
	".\Debug\db_tree_vars.mod"\
	".\Debug\debug_vars.mod"\
	".\Debug\io_3d_vars.mod"\
	".\Debug\parallel.mod"\
	".\Debug\precision.mod"\
	".\Debug\share_consts.mod"\
	".\Debug\sizes.mod"\
	".\Debug\tree_database_f.mod"\
	".\Debug\util_vars.mod"\
	".\Debug\wlt_trns_util_mod.mod"\
	".\Debug\wlt_trns_vars.mod"\
	".\Debug\wlt_vars.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\db_tree_vars.f90
NODEP_F90_DB_TRE=\
	".\Debug\precision.mod"\
	".\Debug\user_case_db.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\debug_vars.f90
# End Source File
# Begin Source File

SOURCE=..\default_util.f90
NODEP_F90_DEFAU=\
	".\Debug\debug_vars.mod"\
	".\Debug\field.mod"\
	".\Debug\io_3D.mod"\
	".\Debug\io_3d_vars.mod"\
	".\Debug\parallel.mod"\
	".\Debug\pde.mod"\
	".\Debug\penalization.mod"\
	".\Debug\precision.mod"\
	".\Debug\share_consts.mod"\
	".\Debug\sizes.mod"\
	".\Debug\user_case.mod"\
	".\Debug\user_case_db.mod"\
	".\Debug\util_vars.mod"\
	".\Debug\wlt_trns_mod.mod"\
	".\Debug\wlt_trns_util_mod.mod"\
	".\Debug\wlt_vars.mod"\
	

!IF  "$(CFG)" == "wlt_tree_c - Win32 Release"

!ELSEIF  "$(CFG)" == "wlt_tree_c - Win32 Debug"

# ADD F90 /optimize:0

!ENDIF 

# End Source File
# Begin Source File

SOURCE=..\derf.f

!IF  "$(CFG)" == "wlt_tree_c - Win32 Release"

!ELSEIF  "$(CFG)" == "wlt_tree_c - Win32 Debug"

# ADD F90 /optimize:0 /stand:none /warn:nouninitialized /warn:nousage
# SUBTRACT F90 /warn:declarations

!ENDIF 

# End Source File
# Begin Source File

SOURCE=..\derfc.f

!IF  "$(CFG)" == "wlt_tree_c - Win32 Release"

!ELSEIF  "$(CFG)" == "wlt_tree_c - Win32 Debug"

# ADD F90 /optimize:0 /stand:none /warn:nouninitialized /warn:nousage
# SUBTRACT F90 /warn:declarations

!ENDIF 

# End Source File
# Begin Source File

SOURCE=..\dgamma.f

!IF  "$(CFG)" == "wlt_tree_c - Win32 Release"

!ELSEIF  "$(CFG)" == "wlt_tree_c - Win32 Debug"

# ADD F90 /optimize:0 /stand:none /warn:nouninitialized /warn:nousage
# SUBTRACT F90 /warn:declarations

!ENDIF 

# End Source File
# Begin Source File

SOURCE=..\dgeev.f

!IF  "$(CFG)" == "wlt_tree_c - Win32 Release"

!ELSEIF  "$(CFG)" == "wlt_tree_c - Win32 Debug"

# ADD F90 /optimize:0 /stand:none /warn:nouninitialized /warn:nousage
# SUBTRACT F90 /warn:declarations

!ENDIF 

# End Source File
# Begin Source File

SOURCE=..\dgels.f

!IF  "$(CFG)" == "wlt_tree_c - Win32 Release"

!ELSEIF  "$(CFG)" == "wlt_tree_c - Win32 Debug"

# ADD F90 /optimize:0 /stand:none /warn:nouninitialized /warn:nousage
# SUBTRACT F90 /warn:declarations

!ENDIF 

# End Source File
# Begin Source File

SOURCE=..\domain_zoltan_empty.f90
NODEP_F90_DOMAI=\
	".\Debug\precision.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\dqag.f

!IF  "$(CFG)" == "wlt_tree_c - Win32 Release"

!ELSEIF  "$(CFG)" == "wlt_tree_c - Win32 Debug"

# ADD F90 /optimize:0 /stand:none /warn:nouninitialized /warn:nousage
# SUBTRACT F90 /warn:declarations

!ENDIF 

# End Source File
# Begin Source File

SOURCE=..\dqage.f

!IF  "$(CFG)" == "wlt_tree_c - Win32 Release"

!ELSEIF  "$(CFG)" == "wlt_tree_c - Win32 Debug"

# ADD F90 /optimize:0 /stand:none /warn:nouninitialized /warn:nousage
# SUBTRACT F90 /warn:declarations

!ENDIF 

# End Source File
# Begin Source File

SOURCE=..\dtrsm.f

!IF  "$(CFG)" == "wlt_tree_c - Win32 Release"

!ELSEIF  "$(CFG)" == "wlt_tree_c - Win32 Debug"

# ADD F90 /optimize:0 /stand:none /warn:nouninitialized /warn:nousage
# SUBTRACT F90 /warn:declarations

!ENDIF 

# End Source File
# Begin Source File

SOURCE=..\elliptic_solve_3d.f90
NODEP_F90_ELLIP=\
	".\Debug\debug_vars.mod"\
	".\Debug\elliptic_vars.mod"\
	".\Debug\LOCAL_MATH.mod"\
	".\Debug\parallel.mod"\
	".\Debug\pde.mod"\
	".\Debug\penalization.mod"\
	".\Debug\precision.mod"\
	".\Debug\share_consts.mod"\
	".\Debug\sizes.mod"\
	".\Debug\util_vars.mod"\
	".\Debug\wlt_trns_mod.mod"\
	".\Debug\wlt_trns_vars.mod"\
	".\Debug\wlt_vars.mod"\
	

!IF  "$(CFG)" == "wlt_tree_c - Win32 Release"

!ELSEIF  "$(CFG)" == "wlt_tree_c - Win32 Debug"

# ADD F90 /optimize:0

!ENDIF 

# End Source File
# Begin Source File

SOURCE=..\elliptic_solve_3d_vars.f90
NODEP_F90_ELLIPT=\
	".\Debug\precision.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\fft\fft.f90
NODEP_F90_FFT_F=\
	".\Debug\debug_vars.mod"\
	".\Debug\fft_interface_module.mod"\
	".\Debug\precision.mod"\
	

!IF  "$(CFG)" == "wlt_tree_c - Win32 Release"

!ELSEIF  "$(CFG)" == "wlt_tree_c - Win32 Debug"

# ADD F90 /optimize:0

!ENDIF 

# End Source File
# Begin Source File

SOURCE=..\fft\fft.interface.temperton.f90
NODEP_F90_FFT_I=\
	".\Debug\debug_vars.mod"\
	".\Debug\precision.mod"\
	

!IF  "$(CFG)" == "wlt_tree_c - Win32 Release"

!ELSEIF  "$(CFG)" == "wlt_tree_c - Win32 Debug"

# ADD F90 /optimize:0

!ENDIF 

# End Source File
# Begin Source File

SOURCE=..\fft\fft_util.f90
NODEP_F90_FFT_U=\
	".\Debug\debug_vars.mod"\
	".\Debug\fft_module.mod"\
	".\Debug\precision.mod"\
	".\Debug\wlt_vars.mod"\
	

!IF  "$(CFG)" == "wlt_tree_c - Win32 Release"

!ELSEIF  "$(CFG)" == "wlt_tree_c - Win32 Debug"

# ADD F90 /optimize:0

!ENDIF 

# End Source File
# Begin Source File

SOURCE=..\fft\fftpacktvms.f

!IF  "$(CFG)" == "wlt_tree_c - Win32 Release"

# ADD F90 /real_size:64

!ELSEIF  "$(CFG)" == "wlt_tree_c - Win32 Debug"

# ADD F90 /f77rtl /optimize:0 /real_size:64 /stand:none /warn:nouninitialized /warn:nousage
# SUBTRACT F90 /check:bounds /warn:declarations

!ENDIF 

# End Source File
# Begin Source File

SOURCE=..\gaussq.f

!IF  "$(CFG)" == "wlt_tree_c - Win32 Release"

!ELSEIF  "$(CFG)" == "wlt_tree_c - Win32 Debug"

# ADD F90 /optimize:0 /stand:none /warn:nouninitialized /warn:nousage
# SUBTRACT F90 /warn:declarations

!ENDIF 

# End Source File
# Begin Source File

SOURCE=..\hyperbolic.f90
NODEP_F90_HYPER=\
	".\Debug\field.mod"\
	".\Debug\input_file_reader.mod"\
	".\Debug\io_3d_vars.mod"\
	".\Debug\parallel.mod"\
	".\Debug\pde.mod"\
	".\Debug\precision.mod"\
	".\Debug\share_consts.mod"\
	".\Debug\share_kry.mod"\
	".\Debug\sizes.mod"\
	".\Debug\vector_util_mod.mod"\
	".\Debug\wavelet_filters_mod.mod"\
	".\Debug\wlt_trns_mod.mod"\
	".\Debug\wlt_trns_vars.mod"\
	".\Debug\wlt_vars.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\input_files_reader.f90
NODEP_F90_INPUT=\
	".\Debug\debug_vars.mod"\
	".\Debug\precision.mod"\
	

!IF  "$(CFG)" == "wlt_tree_c - Win32 Release"

!ELSEIF  "$(CFG)" == "wlt_tree_c - Win32 Debug"

# ADD F90 /optimize:0

!ENDIF 

# End Source File
# Begin Source File

SOURCE="..\C++\interface.cxx"
# End Source File
# Begin Source File

SOURCE="..\C++\io.cxx"
# End Source File
# Begin Source File

SOURCE=..\io_3d.f90
NODEP_F90_IO_3D=\
	".\Debug\additional_nodes.mod"\
	".\Debug\debug_vars.mod"\
	".\Debug\elliptic_vars.mod"\
	".\Debug\field.mod"\
	".\Debug\input_file_reader.mod"\
	".\Debug\io_3d_vars.mod"\
	".\Debug\parallel.mod"\
	".\Debug\pde.mod"\
	".\Debug\penalization.mod"\
	".\Debug\precision.mod"\
	".\Debug\share_consts.mod"\
	".\Debug\share_kry.mod"\
	".\Debug\sizes.mod"\
	".\Debug\wavelet_filters_vars.mod"\
	".\Debug\wlt_trns_mod.mod"\
	".\Debug\wlt_trns_util_mod.mod"\
	".\Debug\wlt_trns_vars.mod"\
	".\Debug\wlt_vars.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\io_3d_vars.f90
# End Source File
# Begin Source File

SOURCE=..\needblas.f

!IF  "$(CFG)" == "wlt_tree_c - Win32 Release"

!ELSEIF  "$(CFG)" == "wlt_tree_c - Win32 Debug"

# ADD F90 /optimize:0 /stand:none /warn:nouninitialized /warn:nousage
# SUBTRACT F90 /warn:declarations

!ENDIF 

# End Source File
# Begin Source File

SOURCE=..\parallel.f90
NODEP_F90_PARAL=\
	".\Debug\db_tree_vars.mod"\
	".\Debug\debug_vars.mod"\
	".\Debug\domain_geometric.mod"\
	".\Debug\domain_zoltan.mod"\
	".\Debug\mpif.h"\
	".\Debug\precision.mod"\
	".\Debug\user_case_db.mod"\
	".\Debug\wlt_trns_util_mod.mod"\
	".\Debug\wlt_vars.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\r1mach.f

!IF  "$(CFG)" == "wlt_tree_c - Win32 Release"

!ELSEIF  "$(CFG)" == "wlt_tree_c - Win32 Debug"

# ADD F90 /optimize:0 /stand:none /warn:nouninitialized /warn:nousage
# SUBTRACT F90 /warn:declarations

!ENDIF 

# End Source File
# Begin Source File

SOURCE=..\read_data_wray.f90
NODEP_F90_READ_=\
	".\Debug\debug_vars.mod"\
	".\Debug\fft_module.mod"\
	".\Debug\precision.mod"\
	

!IF  "$(CFG)" == "wlt_tree_c - Win32 Release"

!ELSEIF  "$(CFG)" == "wlt_tree_c - Win32 Debug"

# ADD F90 /optimize:0

!ENDIF 

# End Source File
# Begin Source File

SOURCE=..\read_init.f90
NODEP_F90_READ_I=\
	".\Debug\ch_resolution.mod"\
	".\Debug\debug_vars.mod"\
	".\Debug\fft_module.mod"\
	".\Debug\parallel.mod"\
	".\Debug\pde.mod"\
	".\Debug\precision.mod"\
	".\Debug\read_data_wry.mod"\
	".\Debug\read_netcdf_mod.mod"\
	".\Debug\share_consts.mod"\
	".\Debug\sizes.mod"\
	".\Debug\spectra_module.mod"\
	".\Debug\util_mod.mod"\
	".\Debug\wlt_trns_mod.mod"\
	".\Debug\wlt_trns_util_mod.mod"\
	

!IF  "$(CFG)" == "wlt_tree_c - Win32 Release"

!ELSEIF  "$(CFG)" == "wlt_tree_c - Win32 Debug"

# ADD F90 /optimize:0

!ENDIF 

# End Source File
# Begin Source File

SOURCE=..\read_netcdf.f90
DEP_F90_READ_N=\
	"\My Desktop\work_wlt_3d_DBs\trunk\netcdf.inc"\
	
# ADD F90 /iface:default
# End Source File
# Begin Source File

SOURCE=..\sgs_incompressible.f90
NODEP_F90_SGS_I=\
	".\Debug\field.mod"\
	".\Debug\input_file_reader.mod"\
	".\Debug\io_3d_vars.mod"\
	".\Debug\parallel.mod"\
	".\Debug\pde.mod"\
	".\Debug\precision.mod"\
	".\Debug\share_consts.mod"\
	".\Debug\share_kry.mod"\
	".\Debug\spectra_module.mod"\
	".\Debug\util_mod.mod"\
	".\Debug\util_vars.mod"\
	".\Debug\vector_util_mod.mod"\
	".\Debug\wavelet_filters_mod.mod"\
	".\Debug\wavelet_filters_vars.mod"\
	".\Debug\wlt_trns_mod.mod"\
	".\Debug\wlt_trns_vars.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\shared_modules.f90

!IF  "$(CFG)" == "wlt_tree_c - Win32 Release"

!ELSEIF  "$(CFG)" == "wlt_tree_c - Win32 Debug"

# ADD F90 /optimize:0

!ENDIF 

# End Source File
# Begin Source File

SOURCE=..\fft\spectra.f90
NODEP_F90_SPECT=\
	".\Debug\debug_vars.mod"\
	".\Debug\fft_module.mod"\
	".\Debug\wlt_vars.mod"\
	
# End Source File
# Begin Source File

SOURCE="\My Desktop\work_wlt_3d_DBs\trunk\WorkCases\Oleg\time-periodic\time-periodic.f90"
# End Source File
# Begin Source File

SOURCE="\My Desktop\work_wlt_3d_DBs\trunk\WorkCases\Oleg\time-periodic\time-periodic.PARAMS.f90"
# End Source File
# Begin Source File

SOURCE=..\time_int_cn.f90
NODEP_F90_TIME_=\
	".\Debug\debug_vars.mod"\
	".\Debug\elliptic_mod.mod"\
	".\Debug\parallel.mod"\
	".\Debug\pde.mod"\
	".\Debug\penalization.mod"\
	".\Debug\precision.mod"\
	".\Debug\SGS_incompressible.mod"\
	".\Debug\share_consts.mod"\
	".\Debug\share_kry.mod"\
	".\Debug\sizes.mod"\
	".\Debug\user_case.mod"\
	".\Debug\util_mod.mod"\
	".\Debug\util_vars.mod"\
	".\Debug\vector_util_mod.mod"\
	".\Debug\wlt_trns_mod.mod"\
	

!IF  "$(CFG)" == "wlt_tree_c - Win32 Release"

!ELSEIF  "$(CFG)" == "wlt_tree_c - Win32 Debug"

# ADD F90 /optimize:0

!ENDIF 

# End Source File
# Begin Source File

SOURCE=..\time_int_krylov_3d.f90
NODEP_F90_TIME_I=\
	".\Debug\debug_vars.mod"\
	".\Debug\elliptic_mod.mod"\
	".\Debug\parallel.mod"\
	".\Debug\pde.mod"\
	".\Debug\precision.mod"\
	".\Debug\share_consts.mod"\
	".\Debug\share_kry.mod"\
	".\Debug\sizes.mod"\
	".\Debug\user_case.mod"\
	

!IF  "$(CFG)" == "wlt_tree_c - Win32 Release"

!ELSEIF  "$(CFG)" == "wlt_tree_c - Win32 Debug"

# ADD F90 /optimize:0

!ENDIF 

# End Source File
# Begin Source File

SOURCE="..\C++\tree.cxx"
# End Source File
# Begin Source File

SOURCE=..\util_3d.f90
NODEP_F90_UTIL_=\
	".\Debug\debug_vars.mod"\
	".\Debug\parallel.mod"\
	".\Debug\pde.mod"\
	".\Debug\precision.mod"\
	".\Debug\sizes.mod"\
	".\Debug\util_vars.mod"\
	".\Debug\wavelet_filters_vars.mod"\
	".\Debug\wlt_trns_mod.mod"\
	".\Debug\wlt_vars.mod"\
	

!IF  "$(CFG)" == "wlt_tree_c - Win32 Release"

!ELSEIF  "$(CFG)" == "wlt_tree_c - Win32 Debug"

# ADD F90 /optimize:0

!ENDIF 

# End Source File
# Begin Source File

SOURCE=..\vector_util.f90
NODEP_F90_VECTO=\
	".\Debug\debug_vars.mod"\
	".\Debug\parallel.mod"\
	".\Debug\pde.mod"\
	".\Debug\precision.mod"\
	".\Debug\share_consts.mod"\
	".\Debug\util_vars.mod"\
	".\Debug\wlt_trns_mod.mod"\
	".\Debug\wlt_vars.mod"\
	

!IF  "$(CFG)" == "wlt_tree_c - Win32 Release"

!ELSEIF  "$(CFG)" == "wlt_tree_c - Win32 Debug"

# ADD F90 /optimize:0

!ENDIF 

# End Source File
# Begin Source File

SOURCE=..\wavelet_3d.f90
NODEP_F90_WAVEL=\
	".\Debug\precision.mod"\
	".\Debug\sizes.mod"\
	".\Debug\wlt_trns_vars.mod"\
	".\Debug\wlt_vars.mod"\
	

!IF  "$(CFG)" == "wlt_tree_c - Win32 Release"

!ELSEIF  "$(CFG)" == "wlt_tree_c - Win32 Debug"

# ADD F90 /optimize:0

!ENDIF 

# End Source File
# Begin Source File

SOURCE=..\wavelet_3d_tree.f90
NODEP_F90_WAVELE=\
	".\Debug\additional_nodes.mod"\
	".\Debug\db_tree.mod"\
	".\Debug\debug_vars.mod"\
	".\Debug\field.mod"\
	".\Debug\io_3d_vars.mod"\
	".\Debug\main_vars.mod"\
	".\Debug\parallel.mod"\
	".\Debug\pde.mod"\
	".\Debug\precision.mod"\
	".\Debug\share_consts.mod"\
	".\Debug\sizes.mod"\
	".\Debug\tree_database_f.mod"\
	".\Debug\user_case_db.mod"\
	".\Debug\util_vars.mod"\
	".\Debug\wlt_trns_util_mod.mod"\
	".\Debug\wlt_trns_vars.mod"\
	".\Debug\wlt_vars.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\wavelet_3d_tree_vars.f90
NODEP_F90_WAVELET=\
	".\Debug\db_tree_vars.mod"\
	".\Debug\wlt_vars.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\wavelet_3d_vars.f90
NODEP_F90_WAVELET_=\
	".\Debug\precision.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\wavelet_filters.f90
NODEP_F90_WAVELET_F=\
	".\Debug\db_tree.mod"\
	".\Debug\db_tree_vars.mod"\
	".\Debug\debug_vars.mod"\
	".\Debug\parallel.mod"\
	".\Debug\pde.mod"\
	".\Debug\precision.mod"\
	".\Debug\sizes.mod"\
	".\Debug\wavelet_filters_vars.mod"\
	".\Debug\wlt_trns_mod.mod"\
	".\Debug\wlt_trns_util_mod.mod"\
	".\Debug\wlt_trns_vars.mod"\
	".\Debug\wlt_vars.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\wavelet_filters_vars.f90
NODEP_F90_WAVELET_FI=\
	".\Debug\precision.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\wlt_3d_main.f90
NODEP_F90_WLT_3=\
	".\Debug\debug_vars.mod"\
	".\Debug\default_mod.mod"\
	".\Debug\elliptic_mod.mod"\
	".\Debug\elliptic_vars.mod"\
	".\Debug\field.mod"\
	".\Debug\hyperbolic_solver.mod"\
	".\Debug\io_3D.mod"\
	".\Debug\io_3d_vars.mod"\
	".\Debug\kry_mod.mod"\
	".\Debug\main_vars.mod"\
	".\Debug\parallel.mod"\
	".\Debug\pde.mod"\
	".\Debug\penalization.mod"\
	".\Debug\precision.mod"\
	".\Debug\read_init_mod.mod"\
	".\Debug\read_netcdf_mod.mod"\
	".\Debug\SGS_incompressible.mod"\
	".\Debug\share_consts.mod"\
	".\Debug\share_kry.mod"\
	".\Debug\sizes.mod"\
	".\Debug\time_int_cn_mod.mod"\
	".\Debug\user_case.mod"\
	".\Debug\util_mod.mod"\
	".\Debug\util_vars.mod"\
	".\Debug\wavelet_filters_mod.mod"\
	".\Debug\wavelet_filters_vars.mod"\
	".\Debug\wlt_trns_mod.mod"\
	".\Debug\wlt_trns_util_mod.mod"\
	".\Debug\wlt_vars.mod"\
	

!IF  "$(CFG)" == "wlt_tree_c - Win32 Release"

!ELSEIF  "$(CFG)" == "wlt_tree_c - Win32 Debug"

# ADD F90 /optimize:0

!ENDIF 

# End Source File
# Begin Source File

SOURCE=..\wlt_3d_main_vars.f90
NODEP_F90_WLT_3D=\
	".\Debug\precision.mod"\
	

!IF  "$(CFG)" == "wlt_tree_c - Win32 Release"

!ELSEIF  "$(CFG)" == "wlt_tree_c - Win32 Debug"

# ADD F90 /check:arg_temp_created /check:bounds /check:format /check:power /check:output_conversion /check:overflow /check:underflow /traceback
# SUBTRACT F90 /check:noflawed_pentium

!ENDIF 

# End Source File
# Begin Source File

SOURCE=..\zgetrf.f

!IF  "$(CFG)" == "wlt_tree_c - Win32 Release"

!ELSEIF  "$(CFG)" == "wlt_tree_c - Win32 Debug"

# ADD F90 /optimize:0 /stand:none /warn:nouninitialized /warn:nousage
# SUBTRACT F90 /warn:declarations

!ENDIF 

# End Source File
# Begin Source File

SOURCE=..\zgetri.f

!IF  "$(CFG)" == "wlt_tree_c - Win32 Release"

!ELSEIF  "$(CFG)" == "wlt_tree_c - Win32 Debug"

# ADD F90 /optimize:0 /stand:none /warn:nouninitialized /warn:nousage
# SUBTRACT F90 /warn:declarations

!ENDIF 

# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl;fi;fd"
# Begin Source File

SOURCE="..\C++\amr.h"
# End Source File
# Begin Source File

SOURCE="..\C++\interface.h"
# End Source File
# Begin Source File

SOURCE="..\C++\io.h"
# End Source File
# Begin Source File

SOURCE="..\C++\tree.h"
# End Source File
# End Group
# Begin Group "Resource Files"

# PROP Default_Filter "ico;cur;bmp;dlg;rc2;rct;bin;rgs;gif;jpg;jpeg;jpe"
# End Group
# End Target
# End Project
