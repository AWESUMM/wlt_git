# Microsoft Developer Studio Generated NMAKE File, Based on wlt_3d.dsp
!IF "$(CFG)" == ""
CFG=wlt_3d - Win32 Debug
!MESSAGE No configuration specified. Defaulting to wlt_3d - Win32 Debug.
!ENDIF 

!IF "$(CFG)" != "wlt_3d - Win32 Release" && "$(CFG)" != "wlt_3d - Win32 Debug"
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "wlt_3d.mak" CFG="wlt_3d - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "wlt_3d - Win32 Release" (based on "Win32 (x86) Console Application")
!MESSAGE "wlt_3d - Win32 Debug" (based on "Win32 (x86) Console Application")
!MESSAGE 
!ERROR An invalid configuration is specified.
!ENDIF 

!IF "$(OS)" == "Windows_NT"
NULL=
!ELSE 
NULL=nul
!ENDIF 

CPP=cl.exe
F90=df.exe
RSC=rc.exe

!IF  "$(CFG)" == "wlt_3d - Win32 Release"

OUTDIR=.\Release
INTDIR=.\Release
# Begin Custom Macros
OutDir=.\Release
# End Custom Macros

ALL : "$(OUTDIR)\wlt_3d.exe" "$(OUTDIR)\fft_util.mod" "$(OUTDIR)\pde_unused.mod"


CLEAN :
	-@erase "$(INTDIR)\case_small_vortex_array.obj"
	-@erase "$(INTDIR)\ch_resolution.mod"
	-@erase "$(INTDIR)\ch_resolution_fs.obj"
	-@erase "$(INTDIR)\d1mach.obj"
	-@erase "$(INTDIR)\default_mod.mod"
	-@erase "$(INTDIR)\default_util.obj"
	-@erase "$(INTDIR)\derf.obj"
	-@erase "$(INTDIR)\derfc.obj"
	-@erase "$(INTDIR)\DF60.PDB"
	-@erase "$(INTDIR)\dgamma.obj"
	-@erase "$(INTDIR)\dgeev.obj"
	-@erase "$(INTDIR)\dgels.obj"
	-@erase "$(INTDIR)\dqag.obj"
	-@erase "$(INTDIR)\dqage.obj"
	-@erase "$(INTDIR)\dtrsm.obj"
	-@erase "$(INTDIR)\elliptic_mod.mod"
	-@erase "$(INTDIR)\elliptic_solve_3d.obj"
	-@erase "$(INTDIR)\elliptic_solve_3d_vars.obj"
	-@erase "$(INTDIR)\elliptic_vars.mod"
	-@erase "$(INTDIR)\endianness_mod.mod"
	-@erase "$(INTDIR)\endienness_small.obj"
	-@erase "$(INTDIR)\fft.interface.temperton.obj"
	-@erase "$(INTDIR)\fft.obj"
	-@erase "$(INTDIR)\fft_interface_module.mod"
	-@erase "$(INTDIR)\fft_module.mod"
	-@erase "$(INTDIR)\fft_util.mod"
	-@erase "$(INTDIR)\fft_util.obj"
	-@erase "$(INTDIR)\fftpacktvms.obj"
	-@erase "$(INTDIR)\field.mod"
	-@erase "$(INTDIR)\gaussq.obj"
	-@erase "$(INTDIR)\input_file_reader.mod"
	-@erase "$(INTDIR)\input_files_reader.obj"
	-@erase "$(INTDIR)\io_3D.mod"
	-@erase "$(INTDIR)\io_3d.obj"
	-@erase "$(INTDIR)\io_3d_vars.mod"
	-@erase "$(INTDIR)\io_3d_vars.obj"
	-@erase "$(INTDIR)\kry_mod.mod"
	-@erase "$(INTDIR)\misc_vars.mod"
	-@erase "$(INTDIR)\needblas.obj"
	-@erase "$(INTDIR)\pde.mod"
	-@erase "$(INTDIR)\pde_unused.mod"
	-@erase "$(INTDIR)\penalization.mod"
	-@erase "$(INTDIR)\precision.mod"
	-@erase "$(INTDIR)\r1mach.obj"
	-@erase "$(INTDIR)\read_data_wray.obj"
	-@erase "$(INTDIR)\read_data_wry.mod"
	-@erase "$(INTDIR)\read_init.obj"
	-@erase "$(INTDIR)\read_init_mod.mod"
	-@erase "$(INTDIR)\read_netcdf.obj"
	-@erase "$(INTDIR)\read_netcdf_mod.mod"
	-@erase "$(INTDIR)\reverse_endian.obj"
	-@erase "$(INTDIR)\share_consts.mod"
	-@erase "$(INTDIR)\share_kry.mod"
	-@erase "$(INTDIR)\shared_modules.obj"
	-@erase "$(INTDIR)\sizes.mod"
	-@erase "$(INTDIR)\spectra.obj"
	-@erase "$(INTDIR)\spectra_module.mod"
	-@erase "$(INTDIR)\time_int_cn.obj"
	-@erase "$(INTDIR)\time_int_cn_mod.mod"
	-@erase "$(INTDIR)\time_int_krylov_3d.obj"
	-@erase "$(INTDIR)\user_case.mod"
	-@erase "$(INTDIR)\util_3d.obj"
	-@erase "$(INTDIR)\util_mod.mod"
	-@erase "$(INTDIR)\util_vars.mod"
	-@erase "$(INTDIR)\vector_util.obj"
	-@erase "$(INTDIR)\vector_util_mod.mod"
	-@erase "$(INTDIR)\wavelet_3d.obj"
	-@erase "$(INTDIR)\wavelet_3d_vars.obj"
	-@erase "$(INTDIR)\wavelet_3d_wrk.obj"
	-@erase "$(INTDIR)\wavelet_3d_wrk_vars.obj"
	-@erase "$(INTDIR)\wavelet_filters.obj"
	-@erase "$(INTDIR)\wavelet_filters_mod.mod"
	-@erase "$(INTDIR)\wavelet_filters_vars.mod"
	-@erase "$(INTDIR)\wavelet_filters_vars.obj"
	-@erase "$(INTDIR)\wlt_3d_main.obj"
	-@erase "$(INTDIR)\wlt_trns_mod.mod"
	-@erase "$(INTDIR)\wlt_trns_util_mod.mod"
	-@erase "$(INTDIR)\wlt_trns_vars.mod"
	-@erase "$(INTDIR)\wlt_vars.mod"
	-@erase "$(INTDIR)\zgetrf.obj"
	-@erase "$(INTDIR)\zgetri.obj"
	-@erase "$(OUTDIR)\wlt_3d.exe"
	-@erase "$(OUTDIR)\wlt_3d.pdb"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

F90_PROJ=/check:bounds /check:power /check:overflow /check:underflow /compile_only /debug:full /fpp /include:"Release/" /nologo /traceback /warn:nogeneral /module:"Release/" /object:"Release/" /pdbfile:"Release/DF60.PDB" 
F90_OBJS=.\Release/
CPP_PROJ=/nologo /ML /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_CONSOLE" /D "_MBCS" /Fp"$(INTDIR)\wlt_3d.pch" /YX /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /c 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\wlt_3d.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
LINK32_FLAGS=netcdf.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /stack:0x1dcd6500 /subsystem:console /incremental:no /pdb:"$(OUTDIR)\wlt_3d.pdb" /debug /machine:I386 /out:"$(OUTDIR)\wlt_3d.exe" 
LINK32_OBJS= \
	"$(INTDIR)\case_small_vortex_array.obj" \
	"$(INTDIR)\ch_resolution_fs.obj" \
	"$(INTDIR)\d1mach.obj" \
	"$(INTDIR)\default_util.obj" \
	"$(INTDIR)\derf.obj" \
	"$(INTDIR)\derfc.obj" \
	"$(INTDIR)\dgamma.obj" \
	"$(INTDIR)\dgeev.obj" \
	"$(INTDIR)\dgels.obj" \
	"$(INTDIR)\dqag.obj" \
	"$(INTDIR)\dqage.obj" \
	"$(INTDIR)\dtrsm.obj" \
	"$(INTDIR)\elliptic_solve_3d.obj" \
	"$(INTDIR)\elliptic_solve_3d_vars.obj" \
	"$(INTDIR)\endienness_small.obj" \
	"$(INTDIR)\fft.obj" \
	"$(INTDIR)\fft.interface.temperton.obj" \
	"$(INTDIR)\fft_util.obj" \
	"$(INTDIR)\fftpacktvms.obj" \
	"$(INTDIR)\gaussq.obj" \
	"$(INTDIR)\input_files_reader.obj" \
	"$(INTDIR)\io_3d.obj" \
	"$(INTDIR)\io_3d_vars.obj" \
	"$(INTDIR)\needblas.obj" \
	"$(INTDIR)\r1mach.obj" \
	"$(INTDIR)\read_data_wray.obj" \
	"$(INTDIR)\read_init.obj" \
	"$(INTDIR)\read_netcdf.obj" \
	"$(INTDIR)\reverse_endian.obj" \
	"$(INTDIR)\shared_modules.obj" \
	"$(INTDIR)\spectra.obj" \
	"$(INTDIR)\time_int_cn.obj" \
	"$(INTDIR)\time_int_krylov_3d.obj" \
	"$(INTDIR)\util_3d.obj" \
	"$(INTDIR)\vector_util.obj" \
	"$(INTDIR)\wavelet_3d.obj" \
	"$(INTDIR)\wavelet_3d_vars.obj" \
	"$(INTDIR)\wavelet_3d_wrk.obj" \
	"$(INTDIR)\wavelet_3d_wrk_vars.obj" \
	"$(INTDIR)\wavelet_filters.obj" \
	"$(INTDIR)\wavelet_filters_vars.obj" \
	"$(INTDIR)\wlt_3d_main.obj" \
	"$(INTDIR)\zgetrf.obj" \
	"$(INTDIR)\zgetri.obj"

"$(OUTDIR)\wlt_3d.exe" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ELSEIF  "$(CFG)" == "wlt_3d - Win32 Debug"

OUTDIR=.\Debug
INTDIR=.\Debug
# Begin Custom Macros
OutDir=.\Debug
# End Custom Macros

ALL : "$(OUTDIR)\wlt_3d.exe" "$(OUTDIR)\wlt_3d.bsc" "$(OUTDIR)\fft_util.mod" "$(OUTDIR)\pde_unused.mod"


CLEAN :
	-@erase "$(INTDIR)\case_small_vortex_array.obj"
	-@erase "$(INTDIR)\case_small_vortex_array.sbr"
	-@erase "$(INTDIR)\ch_resolution.mod"
	-@erase "$(INTDIR)\ch_resolution_fs.obj"
	-@erase "$(INTDIR)\ch_resolution_fs.sbr"
	-@erase "$(INTDIR)\d1mach.obj"
	-@erase "$(INTDIR)\d1mach.sbr"
	-@erase "$(INTDIR)\default_mod.mod"
	-@erase "$(INTDIR)\default_util.obj"
	-@erase "$(INTDIR)\default_util.sbr"
	-@erase "$(INTDIR)\derf.obj"
	-@erase "$(INTDIR)\derf.sbr"
	-@erase "$(INTDIR)\derfc.obj"
	-@erase "$(INTDIR)\derfc.sbr"
	-@erase "$(INTDIR)\DF60.PDB"
	-@erase "$(INTDIR)\dgamma.obj"
	-@erase "$(INTDIR)\dgamma.sbr"
	-@erase "$(INTDIR)\dgeev.obj"
	-@erase "$(INTDIR)\dgeev.sbr"
	-@erase "$(INTDIR)\dgels.obj"
	-@erase "$(INTDIR)\dgels.sbr"
	-@erase "$(INTDIR)\dqag.obj"
	-@erase "$(INTDIR)\dqag.sbr"
	-@erase "$(INTDIR)\dqage.obj"
	-@erase "$(INTDIR)\dqage.sbr"
	-@erase "$(INTDIR)\dtrsm.obj"
	-@erase "$(INTDIR)\dtrsm.sbr"
	-@erase "$(INTDIR)\elliptic_mod.mod"
	-@erase "$(INTDIR)\elliptic_solve_3d.obj"
	-@erase "$(INTDIR)\elliptic_solve_3d.sbr"
	-@erase "$(INTDIR)\elliptic_solve_3d_vars.obj"
	-@erase "$(INTDIR)\elliptic_solve_3d_vars.sbr"
	-@erase "$(INTDIR)\elliptic_vars.mod"
	-@erase "$(INTDIR)\endianness_mod.mod"
	-@erase "$(INTDIR)\endienness_small.obj"
	-@erase "$(INTDIR)\endienness_small.sbr"
	-@erase "$(INTDIR)\fft.interface.temperton.obj"
	-@erase "$(INTDIR)\fft.interface.temperton.sbr"
	-@erase "$(INTDIR)\fft.obj"
	-@erase "$(INTDIR)\fft.sbr"
	-@erase "$(INTDIR)\fft_interface_module.mod"
	-@erase "$(INTDIR)\fft_module.mod"
	-@erase "$(INTDIR)\fft_util.mod"
	-@erase "$(INTDIR)\fft_util.obj"
	-@erase "$(INTDIR)\fft_util.sbr"
	-@erase "$(INTDIR)\fftpacktvms.obj"
	-@erase "$(INTDIR)\fftpacktvms.sbr"
	-@erase "$(INTDIR)\field.mod"
	-@erase "$(INTDIR)\gaussq.obj"
	-@erase "$(INTDIR)\gaussq.sbr"
	-@erase "$(INTDIR)\input_file_reader.mod"
	-@erase "$(INTDIR)\input_files_reader.obj"
	-@erase "$(INTDIR)\input_files_reader.sbr"
	-@erase "$(INTDIR)\io_3D.mod"
	-@erase "$(INTDIR)\io_3d.obj"
	-@erase "$(INTDIR)\io_3d.sbr"
	-@erase "$(INTDIR)\io_3D_vars.mod"
	-@erase "$(INTDIR)\io_3d_vars.obj"
	-@erase "$(INTDIR)\io_3d_vars.sbr"
	-@erase "$(INTDIR)\kry_mod.mod"
	-@erase "$(INTDIR)\misc_vars.mod"
	-@erase "$(INTDIR)\needblas.obj"
	-@erase "$(INTDIR)\needblas.sbr"
	-@erase "$(INTDIR)\pde.mod"
	-@erase "$(INTDIR)\pde_unused.mod"
	-@erase "$(INTDIR)\penalization.mod"
	-@erase "$(INTDIR)\precision.mod"
	-@erase "$(INTDIR)\r1mach.obj"
	-@erase "$(INTDIR)\r1mach.sbr"
	-@erase "$(INTDIR)\read_data_wray.obj"
	-@erase "$(INTDIR)\read_data_wray.sbr"
	-@erase "$(INTDIR)\read_data_wry.mod"
	-@erase "$(INTDIR)\read_init.obj"
	-@erase "$(INTDIR)\read_init.sbr"
	-@erase "$(INTDIR)\read_init_mod.mod"
	-@erase "$(INTDIR)\read_netcdf.obj"
	-@erase "$(INTDIR)\read_netcdf.sbr"
	-@erase "$(INTDIR)\read_netcdf_mod.mod"
	-@erase "$(INTDIR)\reverse_endian.obj"
	-@erase "$(INTDIR)\reverse_endian.sbr"
	-@erase "$(INTDIR)\share_consts.mod"
	-@erase "$(INTDIR)\share_kry.mod"
	-@erase "$(INTDIR)\shared_modules.obj"
	-@erase "$(INTDIR)\shared_modules.sbr"
	-@erase "$(INTDIR)\sizes.mod"
	-@erase "$(INTDIR)\spectra.obj"
	-@erase "$(INTDIR)\spectra.sbr"
	-@erase "$(INTDIR)\spectra_module.mod"
	-@erase "$(INTDIR)\time_int_cn.obj"
	-@erase "$(INTDIR)\time_int_cn.sbr"
	-@erase "$(INTDIR)\time_int_cn_mod.mod"
	-@erase "$(INTDIR)\time_int_krylov_3d.obj"
	-@erase "$(INTDIR)\time_int_krylov_3d.sbr"
	-@erase "$(INTDIR)\user_case.mod"
	-@erase "$(INTDIR)\util_3d.obj"
	-@erase "$(INTDIR)\util_3d.sbr"
	-@erase "$(INTDIR)\util_mod.mod"
	-@erase "$(INTDIR)\util_vars.mod"
	-@erase "$(INTDIR)\vector_util.obj"
	-@erase "$(INTDIR)\vector_util.sbr"
	-@erase "$(INTDIR)\vector_util_mod.mod"
	-@erase "$(INTDIR)\wavelet_3d.obj"
	-@erase "$(INTDIR)\wavelet_3d.sbr"
	-@erase "$(INTDIR)\wavelet_3d_vars.obj"
	-@erase "$(INTDIR)\wavelet_3d_vars.sbr"
	-@erase "$(INTDIR)\wavelet_3d_wrk.obj"
	-@erase "$(INTDIR)\wavelet_3d_wrk.sbr"
	-@erase "$(INTDIR)\wavelet_3d_wrk_vars.obj"
	-@erase "$(INTDIR)\wavelet_3d_wrk_vars.sbr"
	-@erase "$(INTDIR)\wavelet_filters.obj"
	-@erase "$(INTDIR)\wavelet_filters.sbr"
	-@erase "$(INTDIR)\wavelet_filters_mod.mod"
	-@erase "$(INTDIR)\wavelet_filters_vars.mod"
	-@erase "$(INTDIR)\wavelet_filters_vars.obj"
	-@erase "$(INTDIR)\wavelet_filters_vars.sbr"
	-@erase "$(INTDIR)\wlt_3d_main.obj"
	-@erase "$(INTDIR)\wlt_3d_main.sbr"
	-@erase "$(INTDIR)\wlt_trns_mod.mod"
	-@erase "$(INTDIR)\wlt_trns_util_mod.mod"
	-@erase "$(INTDIR)\wlt_trns_vars.mod"
	-@erase "$(INTDIR)\wlt_vars.mod"
	-@erase "$(INTDIR)\zgetrf.obj"
	-@erase "$(INTDIR)\zgetrf.sbr"
	-@erase "$(INTDIR)\zgetri.obj"
	-@erase "$(INTDIR)\zgetri.sbr"
	-@erase "$(OUTDIR)\wlt_3d.bsc"
	-@erase "$(OUTDIR)\wlt_3d.exe"
	-@erase "$(OUTDIR)\wlt_3d.ilk"
	-@erase "$(OUTDIR)\wlt_3d.pdb"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

F90_PROJ=/browser:"Debug/" /check:bounds /compile_only /dbglibs /debug:full /fpp /include:"Debug/" /nologo /traceback /warn:argument_checking /warn:declarations /warn:nofileopt /module:"Debug/" /object:"Debug/" /pdbfile:"Debug/DF60.PDB" 
F90_OBJS=.\Debug/
CPP_PROJ=/nologo /MLd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_CONSOLE" /D "_MBCS" /FR"$(INTDIR)\\" /Fp"$(INTDIR)\wlt_3d.pch" /YX /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /GZ /c 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\wlt_3d.bsc" 
BSC32_SBRS= \
	"$(INTDIR)\case_small_vortex_array.sbr" \
	"$(INTDIR)\ch_resolution_fs.sbr" \
	"$(INTDIR)\d1mach.sbr" \
	"$(INTDIR)\default_util.sbr" \
	"$(INTDIR)\derf.sbr" \
	"$(INTDIR)\derfc.sbr" \
	"$(INTDIR)\dgamma.sbr" \
	"$(INTDIR)\dgeev.sbr" \
	"$(INTDIR)\dgels.sbr" \
	"$(INTDIR)\dqag.sbr" \
	"$(INTDIR)\dqage.sbr" \
	"$(INTDIR)\dtrsm.sbr" \
	"$(INTDIR)\elliptic_solve_3d.sbr" \
	"$(INTDIR)\elliptic_solve_3d_vars.sbr" \
	"$(INTDIR)\endienness_small.sbr" \
	"$(INTDIR)\fft.sbr" \
	"$(INTDIR)\fft.interface.temperton.sbr" \
	"$(INTDIR)\fft_util.sbr" \
	"$(INTDIR)\fftpacktvms.sbr" \
	"$(INTDIR)\gaussq.sbr" \
	"$(INTDIR)\input_files_reader.sbr" \
	"$(INTDIR)\io_3d.sbr" \
	"$(INTDIR)\io_3d_vars.sbr" \
	"$(INTDIR)\needblas.sbr" \
	"$(INTDIR)\r1mach.sbr" \
	"$(INTDIR)\read_data_wray.sbr" \
	"$(INTDIR)\read_init.sbr" \
	"$(INTDIR)\read_netcdf.sbr" \
	"$(INTDIR)\reverse_endian.sbr" \
	"$(INTDIR)\shared_modules.sbr" \
	"$(INTDIR)\spectra.sbr" \
	"$(INTDIR)\time_int_cn.sbr" \
	"$(INTDIR)\time_int_krylov_3d.sbr" \
	"$(INTDIR)\util_3d.sbr" \
	"$(INTDIR)\vector_util.sbr" \
	"$(INTDIR)\wavelet_3d.sbr" \
	"$(INTDIR)\wavelet_3d_vars.sbr" \
	"$(INTDIR)\wavelet_3d_wrk.sbr" \
	"$(INTDIR)\wavelet_3d_wrk_vars.sbr" \
	"$(INTDIR)\wavelet_filters.sbr" \
	"$(INTDIR)\wavelet_filters_vars.sbr" \
	"$(INTDIR)\wlt_3d_main.sbr" \
	"$(INTDIR)\zgetrf.sbr" \
	"$(INTDIR)\zgetri.sbr"

"$(OUTDIR)\wlt_3d.bsc" : "$(OUTDIR)" $(BSC32_SBRS)
    $(BSC32) @<<
  $(BSC32_FLAGS) $(BSC32_SBRS)
<<

LINK32=link.exe
LINK32_FLAGS=netcdf.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /stack:0x1dcd6500 /subsystem:console /incremental:yes /pdb:"$(OUTDIR)\wlt_3d.pdb" /debug /machine:I386 /out:"$(OUTDIR)\wlt_3d.exe" 
LINK32_OBJS= \
	"$(INTDIR)\case_small_vortex_array.obj" \
	"$(INTDIR)\ch_resolution_fs.obj" \
	"$(INTDIR)\d1mach.obj" \
	"$(INTDIR)\default_util.obj" \
	"$(INTDIR)\derf.obj" \
	"$(INTDIR)\derfc.obj" \
	"$(INTDIR)\dgamma.obj" \
	"$(INTDIR)\dgeev.obj" \
	"$(INTDIR)\dgels.obj" \
	"$(INTDIR)\dqag.obj" \
	"$(INTDIR)\dqage.obj" \
	"$(INTDIR)\dtrsm.obj" \
	"$(INTDIR)\elliptic_solve_3d.obj" \
	"$(INTDIR)\elliptic_solve_3d_vars.obj" \
	"$(INTDIR)\endienness_small.obj" \
	"$(INTDIR)\fft.obj" \
	"$(INTDIR)\fft.interface.temperton.obj" \
	"$(INTDIR)\fft_util.obj" \
	"$(INTDIR)\fftpacktvms.obj" \
	"$(INTDIR)\gaussq.obj" \
	"$(INTDIR)\input_files_reader.obj" \
	"$(INTDIR)\io_3d.obj" \
	"$(INTDIR)\io_3d_vars.obj" \
	"$(INTDIR)\needblas.obj" \
	"$(INTDIR)\r1mach.obj" \
	"$(INTDIR)\read_data_wray.obj" \
	"$(INTDIR)\read_init.obj" \
	"$(INTDIR)\read_netcdf.obj" \
	"$(INTDIR)\reverse_endian.obj" \
	"$(INTDIR)\shared_modules.obj" \
	"$(INTDIR)\spectra.obj" \
	"$(INTDIR)\time_int_cn.obj" \
	"$(INTDIR)\time_int_krylov_3d.obj" \
	"$(INTDIR)\util_3d.obj" \
	"$(INTDIR)\vector_util.obj" \
	"$(INTDIR)\wavelet_3d.obj" \
	"$(INTDIR)\wavelet_3d_vars.obj" \
	"$(INTDIR)\wavelet_3d_wrk.obj" \
	"$(INTDIR)\wavelet_3d_wrk_vars.obj" \
	"$(INTDIR)\wavelet_filters.obj" \
	"$(INTDIR)\wavelet_filters_vars.obj" \
	"$(INTDIR)\wlt_3d_main.obj" \
	"$(INTDIR)\zgetrf.obj" \
	"$(INTDIR)\zgetri.obj"

"$(OUTDIR)\wlt_3d.exe" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ENDIF 

.c{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.c{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.SUFFIXES: .fpp

.for{$(F90_OBJS)}.obj:
   $(F90) $(F90_PROJ) $<  

.f{$(F90_OBJS)}.obj:
   $(F90) $(F90_PROJ) $<  

.f90{$(F90_OBJS)}.obj:
   $(F90) $(F90_PROJ) $<  

.fpp{$(F90_OBJS)}.obj:
   $(F90) $(F90_PROJ) $<  


!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("wlt_3d.dep")
!INCLUDE "wlt_3d.dep"
!ELSE 
!MESSAGE Warning: cannot find "wlt_3d.dep"
!ENDIF 
!ENDIF 


!IF "$(CFG)" == "wlt_3d - Win32 Release" || "$(CFG)" == "wlt_3d - Win32 Debug"
SOURCE=.\TestCases\SmallVortexArrayTest1\case_small_vortex_array.f90

!IF  "$(CFG)" == "wlt_3d - Win32 Release"

F90_MODOUT=\
	"user_case"


"$(INTDIR)\case_small_vortex_array.obj"	"$(INTDIR)\user_case.mod" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\elliptic_mod.mod" "$(INTDIR)\elliptic_vars.mod" "$(INTDIR)\input_file_reader.mod" "$(INTDIR)\io_3d_vars.mod" "$(INTDIR)\field.mod" "$(INTDIR)\pde.mod" "$(INTDIR)\precision.mod" "$(INTDIR)\share_consts.mod" "$(INTDIR)\share_kry.mod" "$(INTDIR)\sizes.mod" "$(INTDIR)\util_vars.mod" "$(INTDIR)\util_mod.mod" "$(INTDIR)\vector_util_mod.mod" "$(INTDIR)\wlt_trns_util_mod.mod" "$(INTDIR)\wlt_vars.mod" "$(INTDIR)\wlt_trns_mod.mod" "$(INTDIR)\wlt_trns_vars.mod" "$(INTDIR)\wavelet_filters_mod.mod"
	$(F90) $(F90_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "wlt_3d - Win32 Debug"

F90_MODOUT=\
	"user_case"


"$(INTDIR)\case_small_vortex_array.obj"	"$(INTDIR)\case_small_vortex_array.sbr"	"$(INTDIR)\user_case.mod" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\elliptic_mod.mod" "$(INTDIR)\elliptic_vars.mod" "$(INTDIR)\input_file_reader.mod" "$(INTDIR)\io_3D_vars.mod" "$(INTDIR)\field.mod" "$(INTDIR)\pde.mod" "$(INTDIR)\precision.mod" "$(INTDIR)\share_consts.mod" "$(INTDIR)\share_kry.mod" "$(INTDIR)\sizes.mod" "$(INTDIR)\util_vars.mod" "$(INTDIR)\util_mod.mod" "$(INTDIR)\vector_util_mod.mod" "$(INTDIR)\wlt_trns_util_mod.mod" "$(INTDIR)\wlt_vars.mod" "$(INTDIR)\wlt_trns_mod.mod" "$(INTDIR)\wlt_trns_vars.mod" "$(INTDIR)\wavelet_filters_mod.mod"
	$(F90) $(F90_PROJ) $(SOURCE)


!ENDIF 

SOURCE=.\fft\ch_resolution_fs.f90

!IF  "$(CFG)" == "wlt_3d - Win32 Release"

F90_MODOUT=\
	"ch_resolution"


"$(INTDIR)\ch_resolution_fs.obj"	"$(INTDIR)\ch_resolution.mod" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\precision.mod"
	$(F90) /check:bounds /check:power /check:overflow /check:underflow /compile_only /debug:full /fpp /include:"Release/" /nologo /traceback /warn:nogeneral /module:"Release/" /object:"Release/" /pdbfile:"Release/DF60.PDB" $(SOURCE)


!ELSEIF  "$(CFG)" == "wlt_3d - Win32 Debug"

F90_MODOUT=\
	"ch_resolution"


"$(INTDIR)\ch_resolution_fs.obj"	"$(INTDIR)\ch_resolution_fs.sbr"	"$(INTDIR)\ch_resolution.mod" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\precision.mod"
	$(F90) /browser:"Debug/" /check:bounds /compile_only /dbglibs /debug:full /fpp /include:"Debug/" /nologo /optimize:0 /traceback /warn:argument_checking /warn:declarations /warn:nofileopt /module:"Debug/" /object:"Debug/" /pdbfile:"Debug/DF60.PDB" $(SOURCE)


!ENDIF 

SOURCE=.\d1mach.f

!IF  "$(CFG)" == "wlt_3d - Win32 Release"


"$(INTDIR)\d1mach.obj" : $(SOURCE) "$(INTDIR)"
	$(F90) /check:bounds /check:power /check:overflow /check:underflow /compile_only /debug:full /fpp /include:"Release/" /nologo /traceback /warn:nogeneral /module:"Release/" /object:"Release/" /pdbfile:"Release/DF60.PDB" $(SOURCE)


!ELSEIF  "$(CFG)" == "wlt_3d - Win32 Debug"


"$(INTDIR)\d1mach.obj"	"$(INTDIR)\d1mach.sbr" : $(SOURCE) "$(INTDIR)"
	$(F90) /browser:"Debug/" /check:bounds /compile_only /dbglibs /debug:full /fpp /include:"Debug/" /nologo /optimize:0 /traceback /warn:argument_checking /warn:declarations /warn:nofileopt /module:"Debug/" /object:"Debug/" /pdbfile:"Debug/DF60.PDB" $(SOURCE)


!ENDIF 

SOURCE=.\default_util.f90

!IF  "$(CFG)" == "wlt_3d - Win32 Release"

F90_MODOUT=\
	"default_mod"


"$(INTDIR)\default_util.obj"	"$(INTDIR)\default_mod.mod" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\user_case.mod" "$(INTDIR)\io_3D.mod" "$(INTDIR)\field.mod" "$(INTDIR)\pde.mod" "$(INTDIR)\precision.mod" "$(INTDIR)\share_consts.mod" "$(INTDIR)\sizes.mod" "$(INTDIR)\util_vars.mod" "$(INTDIR)\wlt_trns_util_mod.mod" "$(INTDIR)\wlt_vars.mod" "$(INTDIR)\wlt_trns_mod.mod"
	$(F90) /check:bounds /check:power /check:overflow /check:underflow /compile_only /debug:full /fpp /include:"Release/" /nologo /traceback /warn:nogeneral /module:"Release/" /object:"Release/" /pdbfile:"Release/DF60.PDB" $(SOURCE)


!ELSEIF  "$(CFG)" == "wlt_3d - Win32 Debug"

F90_MODOUT=\
	"default_mod"


"$(INTDIR)\default_util.obj"	"$(INTDIR)\default_util.sbr"	"$(INTDIR)\default_mod.mod" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\user_case.mod" "$(INTDIR)\io_3D.mod" "$(INTDIR)\field.mod" "$(INTDIR)\pde.mod" "$(INTDIR)\precision.mod" "$(INTDIR)\share_consts.mod" "$(INTDIR)\sizes.mod" "$(INTDIR)\util_vars.mod" "$(INTDIR)\wlt_trns_util_mod.mod" "$(INTDIR)\wlt_vars.mod" "$(INTDIR)\wlt_trns_mod.mod"
	$(F90) /browser:"Debug/" /check:bounds /compile_only /dbglibs /debug:full /fpp /include:"Debug/" /nologo /optimize:0 /traceback /warn:argument_checking /warn:declarations /warn:nofileopt /module:"Debug/" /object:"Debug/" /pdbfile:"Debug/DF60.PDB" $(SOURCE)


!ENDIF 

SOURCE=.\derf.f

!IF  "$(CFG)" == "wlt_3d - Win32 Release"


"$(INTDIR)\derf.obj" : $(SOURCE) "$(INTDIR)"
	$(F90) /check:bounds /check:power /check:overflow /check:underflow /compile_only /debug:full /fpp /include:"Release/" /nologo /traceback /warn:nogeneral /module:"Release/" /object:"Release/" /pdbfile:"Release/DF60.PDB" $(SOURCE)


!ELSEIF  "$(CFG)" == "wlt_3d - Win32 Debug"


"$(INTDIR)\derf.obj"	"$(INTDIR)\derf.sbr" : $(SOURCE) "$(INTDIR)"
	$(F90) /browser:"Debug/" /check:bounds /compile_only /dbglibs /debug:full /fpp /include:"Debug/" /nologo /optimize:0 /traceback /warn:argument_checking /warn:declarations /warn:nofileopt /module:"Debug/" /object:"Debug/" /pdbfile:"Debug/DF60.PDB" $(SOURCE)


!ENDIF 

SOURCE=.\derfc.f

!IF  "$(CFG)" == "wlt_3d - Win32 Release"


"$(INTDIR)\derfc.obj" : $(SOURCE) "$(INTDIR)"
	$(F90) /check:bounds /check:power /check:overflow /check:underflow /compile_only /debug:full /fpp /include:"Release/" /nologo /traceback /warn:nogeneral /module:"Release/" /object:"Release/" /pdbfile:"Release/DF60.PDB" $(SOURCE)


!ELSEIF  "$(CFG)" == "wlt_3d - Win32 Debug"


"$(INTDIR)\derfc.obj"	"$(INTDIR)\derfc.sbr" : $(SOURCE) "$(INTDIR)"
	$(F90) /browser:"Debug/" /check:bounds /compile_only /dbglibs /debug:full /fpp /include:"Debug/" /nologo /optimize:0 /traceback /warn:argument_checking /warn:declarations /warn:nofileopt /module:"Debug/" /object:"Debug/" /pdbfile:"Debug/DF60.PDB" $(SOURCE)


!ENDIF 

SOURCE=.\dgamma.f

!IF  "$(CFG)" == "wlt_3d - Win32 Release"


"$(INTDIR)\dgamma.obj" : $(SOURCE) "$(INTDIR)"
	$(F90) /check:bounds /check:power /check:overflow /check:underflow /compile_only /debug:full /fpp /include:"Release/" /nologo /traceback /warn:nogeneral /module:"Release/" /object:"Release/" /pdbfile:"Release/DF60.PDB" $(SOURCE)


!ELSEIF  "$(CFG)" == "wlt_3d - Win32 Debug"


"$(INTDIR)\dgamma.obj"	"$(INTDIR)\dgamma.sbr" : $(SOURCE) "$(INTDIR)"
	$(F90) /browser:"Debug/" /check:bounds /compile_only /dbglibs /debug:full /fpp /include:"Debug/" /nologo /optimize:0 /traceback /warn:argument_checking /warn:declarations /warn:nofileopt /module:"Debug/" /object:"Debug/" /pdbfile:"Debug/DF60.PDB" $(SOURCE)


!ENDIF 

SOURCE=.\dgeev.f

!IF  "$(CFG)" == "wlt_3d - Win32 Release"


"$(INTDIR)\dgeev.obj" : $(SOURCE) "$(INTDIR)"
	$(F90) /check:bounds /check:power /check:overflow /check:underflow /compile_only /debug:full /fpp /include:"Release/" /nologo /traceback /warn:nogeneral /module:"Release/" /object:"Release/" /pdbfile:"Release/DF60.PDB" $(SOURCE)


!ELSEIF  "$(CFG)" == "wlt_3d - Win32 Debug"


"$(INTDIR)\dgeev.obj"	"$(INTDIR)\dgeev.sbr" : $(SOURCE) "$(INTDIR)"
	$(F90) /browser:"Debug/" /check:bounds /compile_only /dbglibs /debug:full /fpp /include:"Debug/" /nologo /optimize:0 /traceback /warn:argument_checking /warn:declarations /warn:nofileopt /module:"Debug/" /object:"Debug/" /pdbfile:"Debug/DF60.PDB" $(SOURCE)


!ENDIF 

SOURCE=.\dgels.f

!IF  "$(CFG)" == "wlt_3d - Win32 Release"


"$(INTDIR)\dgels.obj" : $(SOURCE) "$(INTDIR)"
	$(F90) /check:bounds /check:power /check:overflow /check:underflow /compile_only /debug:full /fpp /include:"Release/" /nologo /traceback /warn:nogeneral /module:"Release/" /object:"Release/" /pdbfile:"Release/DF60.PDB" $(SOURCE)


!ELSEIF  "$(CFG)" == "wlt_3d - Win32 Debug"


"$(INTDIR)\dgels.obj"	"$(INTDIR)\dgels.sbr" : $(SOURCE) "$(INTDIR)"
	$(F90) /browser:"Debug/" /check:bounds /compile_only /dbglibs /debug:full /fpp /include:"Debug/" /nologo /optimize:0 /traceback /warn:argument_checking /warn:declarations /warn:nofileopt /module:"Debug/" /object:"Debug/" /pdbfile:"Debug/DF60.PDB" $(SOURCE)


!ENDIF 

SOURCE=.\dqag.f

!IF  "$(CFG)" == "wlt_3d - Win32 Release"


"$(INTDIR)\dqag.obj" : $(SOURCE) "$(INTDIR)"
	$(F90) /check:bounds /check:power /check:overflow /check:underflow /compile_only /debug:full /fpp /include:"Release/" /nologo /traceback /warn:nogeneral /module:"Release/" /object:"Release/" /pdbfile:"Release/DF60.PDB" $(SOURCE)


!ELSEIF  "$(CFG)" == "wlt_3d - Win32 Debug"


"$(INTDIR)\dqag.obj"	"$(INTDIR)\dqag.sbr" : $(SOURCE) "$(INTDIR)"
	$(F90) /browser:"Debug/" /check:bounds /compile_only /dbglibs /debug:full /fpp /include:"Debug/" /nologo /optimize:0 /traceback /warn:argument_checking /warn:declarations /warn:nofileopt /module:"Debug/" /object:"Debug/" /pdbfile:"Debug/DF60.PDB" $(SOURCE)


!ENDIF 

SOURCE=.\dqage.f

!IF  "$(CFG)" == "wlt_3d - Win32 Release"


"$(INTDIR)\dqage.obj" : $(SOURCE) "$(INTDIR)"
	$(F90) /check:bounds /check:power /check:overflow /check:underflow /compile_only /debug:full /fpp /include:"Release/" /nologo /traceback /warn:nogeneral /module:"Release/" /object:"Release/" /pdbfile:"Release/DF60.PDB" $(SOURCE)


!ELSEIF  "$(CFG)" == "wlt_3d - Win32 Debug"


"$(INTDIR)\dqage.obj"	"$(INTDIR)\dqage.sbr" : $(SOURCE) "$(INTDIR)"
	$(F90) /browser:"Debug/" /check:bounds /compile_only /dbglibs /debug:full /fpp /include:"Debug/" /nologo /optimize:0 /traceback /warn:argument_checking /warn:declarations /warn:nofileopt /module:"Debug/" /object:"Debug/" /pdbfile:"Debug/DF60.PDB" $(SOURCE)


!ENDIF 

SOURCE=.\dtrsm.f

!IF  "$(CFG)" == "wlt_3d - Win32 Release"


"$(INTDIR)\dtrsm.obj" : $(SOURCE) "$(INTDIR)"
	$(F90) /check:bounds /check:power /check:overflow /check:underflow /compile_only /debug:full /fpp /include:"Release/" /nologo /traceback /warn:nogeneral /module:"Release/" /object:"Release/" /pdbfile:"Release/DF60.PDB" $(SOURCE)


!ELSEIF  "$(CFG)" == "wlt_3d - Win32 Debug"


"$(INTDIR)\dtrsm.obj"	"$(INTDIR)\dtrsm.sbr" : $(SOURCE) "$(INTDIR)"
	$(F90) /browser:"Debug/" /check:bounds /compile_only /dbglibs /debug:full /fpp /include:"Debug/" /nologo /optimize:0 /traceback /warn:argument_checking /warn:declarations /warn:nofileopt /module:"Debug/" /object:"Debug/" /pdbfile:"Debug/DF60.PDB" $(SOURCE)


!ENDIF 

SOURCE=.\elliptic_solve_3d.f90

!IF  "$(CFG)" == "wlt_3d - Win32 Release"

F90_MODOUT=\
	"elliptic_mod"


"$(INTDIR)\elliptic_solve_3d.obj"	"$(INTDIR)\elliptic_mod.mod" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\elliptic_vars.mod" "$(INTDIR)\pde.mod" "$(INTDIR)\precision.mod" "$(INTDIR)\share_consts.mod" "$(INTDIR)\sizes.mod" "$(INTDIR)\util_vars.mod" "$(INTDIR)\wlt_vars.mod" "$(INTDIR)\wlt_trns_mod.mod"
	$(F90) /check:bounds /check:power /check:overflow /check:underflow /compile_only /debug:full /fpp /include:"Release/" /nologo /traceback /warn:nogeneral /module:"Release/" /object:"Release/" /pdbfile:"Release/DF60.PDB" $(SOURCE)


!ELSEIF  "$(CFG)" == "wlt_3d - Win32 Debug"

F90_MODOUT=\
	"elliptic_mod"


"$(INTDIR)\elliptic_solve_3d.obj"	"$(INTDIR)\elliptic_solve_3d.sbr"	"$(INTDIR)\elliptic_mod.mod" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\elliptic_vars.mod" "$(INTDIR)\pde.mod" "$(INTDIR)\precision.mod" "$(INTDIR)\share_consts.mod" "$(INTDIR)\sizes.mod" "$(INTDIR)\util_vars.mod" "$(INTDIR)\wlt_vars.mod" "$(INTDIR)\wlt_trns_mod.mod"
	$(F90) /browser:"Debug/" /check:bounds /compile_only /dbglibs /debug:full /fpp /include:"Debug/" /nologo /optimize:0 /traceback /warn:argument_checking /warn:declarations /warn:nofileopt /module:"Debug/" /object:"Debug/" /pdbfile:"Debug/DF60.PDB" $(SOURCE)


!ENDIF 

SOURCE=.\elliptic_solve_3d_vars.f90

!IF  "$(CFG)" == "wlt_3d - Win32 Release"

F90_MODOUT=\
	"elliptic_vars"


"$(INTDIR)\elliptic_solve_3d_vars.obj"	"$(INTDIR)\elliptic_vars.mod" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\precision.mod"
	$(F90) $(F90_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "wlt_3d - Win32 Debug"

F90_MODOUT=\
	"elliptic_vars"


"$(INTDIR)\elliptic_solve_3d_vars.obj"	"$(INTDIR)\elliptic_solve_3d_vars.sbr"	"$(INTDIR)\elliptic_vars.mod" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\precision.mod"
	$(F90) $(F90_PROJ) $(SOURCE)


!ENDIF 

SOURCE=.\endienness_small.f90

!IF  "$(CFG)" == "wlt_3d - Win32 Release"

F90_MODOUT=\
	"endianness_mod"


"$(INTDIR)\endienness_small.obj"	"$(INTDIR)\endianness_mod.mod" : $(SOURCE) "$(INTDIR)"
	$(F90) /check:bounds /check:power /check:overflow /check:underflow /compile_only /debug:full /fpp /include:"Release/" /nologo /traceback /warn:nogeneral /module:"Release/" /object:"Release/" /pdbfile:"Release/DF60.PDB" $(SOURCE)


!ELSEIF  "$(CFG)" == "wlt_3d - Win32 Debug"

F90_MODOUT=\
	"endianness_mod"


"$(INTDIR)\endienness_small.obj"	"$(INTDIR)\endienness_small.sbr"	"$(INTDIR)\endianness_mod.mod" : $(SOURCE) "$(INTDIR)"
	$(F90) /browser:"Debug/" /check:bounds /compile_only /dbglibs /debug:full /fpp /include:"Debug/" /nologo /optimize:0 /traceback /warn:argument_checking /warn:declarations /warn:nofileopt /module:"Debug/" /object:"Debug/" /pdbfile:"Debug/DF60.PDB" $(SOURCE)


!ENDIF 

SOURCE=.\fft\fft.f90

!IF  "$(CFG)" == "wlt_3d - Win32 Release"

F90_MODOUT=\
	"fft_module"


"$(INTDIR)\fft.obj"	"$(INTDIR)\fft_module.mod" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\fft_interface_module.mod" "$(INTDIR)\precision.mod"
	$(F90) /check:bounds /check:power /check:overflow /check:underflow /compile_only /debug:full /fpp /include:"Release/" /nologo /traceback /warn:nogeneral /module:"Release/" /object:"Release/" /pdbfile:"Release/DF60.PDB" $(SOURCE)


!ELSEIF  "$(CFG)" == "wlt_3d - Win32 Debug"

F90_MODOUT=\
	"fft_module"


"$(INTDIR)\fft.obj"	"$(INTDIR)\fft.sbr"	"$(INTDIR)\fft_module.mod" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\fft_interface_module.mod" "$(INTDIR)\precision.mod"
	$(F90) /browser:"Debug/" /check:bounds /compile_only /dbglibs /debug:full /fpp /include:"Debug/" /nologo /optimize:0 /traceback /warn:argument_checking /warn:declarations /warn:nofileopt /module:"Debug/" /object:"Debug/" /pdbfile:"Debug/DF60.PDB" $(SOURCE)


!ENDIF 

SOURCE=.\fft\fft.interface.temperton.f90

!IF  "$(CFG)" == "wlt_3d - Win32 Release"

F90_MODOUT=\
	"fft_interface_module"


"$(INTDIR)\fft.interface.temperton.obj"	"$(INTDIR)\fft_interface_module.mod" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\precision.mod"
	$(F90) /check:bounds /check:power /check:overflow /check:underflow /compile_only /debug:full /fpp /include:"Release/" /nologo /traceback /warn:nogeneral /module:"Release/" /object:"Release/" /pdbfile:"Release/DF60.PDB" $(SOURCE)


!ELSEIF  "$(CFG)" == "wlt_3d - Win32 Debug"

F90_MODOUT=\
	"fft_interface_module"


"$(INTDIR)\fft.interface.temperton.obj"	"$(INTDIR)\fft.interface.temperton.sbr"	"$(INTDIR)\fft_interface_module.mod" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\precision.mod"
	$(F90) /browser:"Debug/" /check:bounds /compile_only /dbglibs /debug:full /fpp /include:"Debug/" /nologo /optimize:0 /traceback /warn:argument_checking /warn:declarations /warn:nofileopt /module:"Debug/" /object:"Debug/" /pdbfile:"Debug/DF60.PDB" $(SOURCE)


!ENDIF 

SOURCE=.\fft\fft_util.f90

!IF  "$(CFG)" == "wlt_3d - Win32 Release"

F90_MODOUT=\
	"fft_util"


"$(INTDIR)\fft_util.obj"	"$(INTDIR)\fft_util.mod" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\fft_module.mod" "$(INTDIR)\precision.mod" "$(INTDIR)\wlt_vars.mod"
	$(F90) /check:bounds /check:power /check:overflow /check:underflow /compile_only /debug:full /fpp /include:"Release/" /nologo /traceback /warn:nogeneral /module:"Release/" /object:"Release/" /pdbfile:"Release/DF60.PDB" $(SOURCE)


!ELSEIF  "$(CFG)" == "wlt_3d - Win32 Debug"

F90_MODOUT=\
	"fft_util"


"$(INTDIR)\fft_util.obj"	"$(INTDIR)\fft_util.sbr"	"$(INTDIR)\fft_util.mod" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\fft_module.mod" "$(INTDIR)\precision.mod" "$(INTDIR)\wlt_vars.mod"
	$(F90) /browser:"Debug/" /check:bounds /compile_only /dbglibs /debug:full /fpp /include:"Debug/" /nologo /optimize:0 /traceback /warn:argument_checking /warn:declarations /warn:nofileopt /module:"Debug/" /object:"Debug/" /pdbfile:"Debug/DF60.PDB" $(SOURCE)


!ENDIF 

SOURCE=.\fft\fftpacktvms.f

!IF  "$(CFG)" == "wlt_3d - Win32 Release"


"$(INTDIR)\fftpacktvms.obj" : $(SOURCE) "$(INTDIR)"
	$(F90) /check:bounds /check:power /check:overflow /check:underflow /compile_only /debug:full /fpp /include:"Release/" /nologo /traceback /warn:nogeneral /module:"Release/" /object:"Release/" /pdbfile:"Release/DF60.PDB" $(SOURCE)


!ELSEIF  "$(CFG)" == "wlt_3d - Win32 Debug"


"$(INTDIR)\fftpacktvms.obj"	"$(INTDIR)\fftpacktvms.sbr" : $(SOURCE) "$(INTDIR)"
	$(F90) /browser:"Debug/" /check:bounds /compile_only /dbglibs /debug:full /fpp /include:"Debug/" /nologo /optimize:0 /traceback /warn:argument_checking /warn:declarations /warn:nofileopt /module:"Debug/" /object:"Debug/" /pdbfile:"Debug/DF60.PDB" $(SOURCE)


!ENDIF 

SOURCE=.\gaussq.f

!IF  "$(CFG)" == "wlt_3d - Win32 Release"


"$(INTDIR)\gaussq.obj" : $(SOURCE) "$(INTDIR)"
	$(F90) /check:bounds /check:power /check:overflow /check:underflow /compile_only /debug:full /fpp /include:"Release/" /nologo /traceback /warn:nogeneral /module:"Release/" /object:"Release/" /pdbfile:"Release/DF60.PDB" $(SOURCE)


!ELSEIF  "$(CFG)" == "wlt_3d - Win32 Debug"


"$(INTDIR)\gaussq.obj"	"$(INTDIR)\gaussq.sbr" : $(SOURCE) "$(INTDIR)"
	$(F90) /browser:"Debug/" /check:bounds /compile_only /dbglibs /debug:full /fpp /include:"Debug/" /nologo /optimize:0 /traceback /warn:argument_checking /warn:declarations /warn:nofileopt /module:"Debug/" /object:"Debug/" /pdbfile:"Debug/DF60.PDB" $(SOURCE)


!ENDIF 

SOURCE=.\input_files_reader.f90

!IF  "$(CFG)" == "wlt_3d - Win32 Release"

F90_MODOUT=\
	"input_file_reader"


"$(INTDIR)\input_files_reader.obj"	"$(INTDIR)\input_file_reader.mod" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\precision.mod"
	$(F90) /check:bounds /check:power /check:overflow /check:underflow /compile_only /debug:full /fpp /include:"Release/" /nologo /traceback /warn:nogeneral /module:"Release/" /object:"Release/" /pdbfile:"Release/DF60.PDB" $(SOURCE)


!ELSEIF  "$(CFG)" == "wlt_3d - Win32 Debug"

F90_MODOUT=\
	"input_file_reader"


"$(INTDIR)\input_files_reader.obj"	"$(INTDIR)\input_files_reader.sbr"	"$(INTDIR)\input_file_reader.mod" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\precision.mod"
	$(F90) /browser:"Debug/" /check:bounds /compile_only /dbglibs /debug:full /fpp /include:"Debug/" /nologo /optimize:0 /traceback /warn:argument_checking /warn:declarations /warn:nofileopt /module:"Debug/" /object:"Debug/" /pdbfile:"Debug/DF60.PDB" $(SOURCE)


!ENDIF 

SOURCE=.\io_3d.f90

!IF  "$(CFG)" == "wlt_3d - Win32 Release"

F90_MODOUT=\
	"io_3D"


"$(INTDIR)\io_3d.obj"	"$(INTDIR)\io_3D.mod" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\elliptic_vars.mod" "$(INTDIR)\endianness_mod.mod" "$(INTDIR)\input_file_reader.mod" "$(INTDIR)\user_case.mod" "$(INTDIR)\io_3d_vars.mod" "$(INTDIR)\field.mod" "$(INTDIR)\misc_vars.mod" "$(INTDIR)\pde.mod" "$(INTDIR)\penalization.mod" "$(INTDIR)\precision.mod" "$(INTDIR)\share_consts.mod" "$(INTDIR)\share_kry.mod" "$(INTDIR)\sizes.mod" "$(INTDIR)\wlt_trns_util_mod.mod" "$(INTDIR)\wlt_vars.mod" "$(INTDIR)\wlt_trns_mod.mod" "$(INTDIR)\wlt_trns_vars.mod" "$(INTDIR)\wavelet_filters_vars.mod"
	$(F90) /check:bounds /check:power /check:overflow /check:underflow /compile_only /debug:full /fpp /include:"Release/" /nologo /traceback /warn:nogeneral /module:"Release/" /object:"Release/" /pdbfile:"Release/DF60.PDB" $(SOURCE)


!ELSEIF  "$(CFG)" == "wlt_3d - Win32 Debug"

F90_MODOUT=\
	"io_3D"


"$(INTDIR)\io_3d.obj"	"$(INTDIR)\io_3d.sbr"	"$(INTDIR)\io_3D.mod" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\user_case.mod" "$(INTDIR)\elliptic_vars.mod" "$(INTDIR)\endianness_mod.mod" "$(INTDIR)\input_file_reader.mod" "$(INTDIR)\io_3D_vars.mod" "$(INTDIR)\field.mod" "$(INTDIR)\misc_vars.mod" "$(INTDIR)\pde.mod" "$(INTDIR)\penalization.mod" "$(INTDIR)\precision.mod" "$(INTDIR)\share_consts.mod" "$(INTDIR)\share_kry.mod" "$(INTDIR)\sizes.mod" "$(INTDIR)\wlt_trns_util_mod.mod" "$(INTDIR)\wlt_vars.mod" "$(INTDIR)\wlt_trns_mod.mod" "$(INTDIR)\wlt_trns_vars.mod" "$(INTDIR)\wavelet_filters_vars.mod"
	$(F90) /browser:"Debug/" /check:bounds /compile_only /dbglibs /debug:full /fpp /include:"Debug/" /nologo /optimize:0 /traceback /warn:argument_checking /warn:declarations /warn:nofileopt /module:"Debug/" /object:"Debug/" /pdbfile:"Debug/DF60.PDB" $(SOURCE)


!ENDIF 

SOURCE=.\io_3d_vars.f90

!IF  "$(CFG)" == "wlt_3d - Win32 Release"

F90_MODOUT=\
	"io_3D_vars"


"$(INTDIR)\io_3d_vars.obj"	"$(INTDIR)\io_3d_vars.mod" : $(SOURCE) "$(INTDIR)"
	$(F90) $(F90_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "wlt_3d - Win32 Debug"

F90_MODOUT=\
	"io_3D_vars"


"$(INTDIR)\io_3d_vars.obj"	"$(INTDIR)\io_3d_vars.sbr"	"$(INTDIR)\io_3D_vars.mod" : $(SOURCE) "$(INTDIR)"
	$(F90) $(F90_PROJ) $(SOURCE)


!ENDIF 

SOURCE=.\needblas.f

!IF  "$(CFG)" == "wlt_3d - Win32 Release"


"$(INTDIR)\needblas.obj" : $(SOURCE) "$(INTDIR)"
	$(F90) /check:bounds /check:power /check:overflow /check:underflow /compile_only /debug:full /fpp /include:"Release/" /nologo /traceback /warn:nogeneral /module:"Release/" /object:"Release/" /pdbfile:"Release/DF60.PDB" $(SOURCE)


!ELSEIF  "$(CFG)" == "wlt_3d - Win32 Debug"


"$(INTDIR)\needblas.obj"	"$(INTDIR)\needblas.sbr" : $(SOURCE) "$(INTDIR)"
	$(F90) /browser:"Debug/" /check:bounds /compile_only /dbglibs /debug:full /fpp /include:"Debug/" /nologo /optimize:0 /traceback /warn:argument_checking /warn:declarations /warn:nofileopt /module:"Debug/" /object:"Debug/" /pdbfile:"Debug/DF60.PDB" $(SOURCE)


!ENDIF 

SOURCE=.\r1mach.f

!IF  "$(CFG)" == "wlt_3d - Win32 Release"


"$(INTDIR)\r1mach.obj" : $(SOURCE) "$(INTDIR)"
	$(F90) /check:bounds /check:power /check:overflow /check:underflow /compile_only /debug:full /fpp /include:"Release/" /nologo /traceback /warn:nogeneral /module:"Release/" /object:"Release/" /pdbfile:"Release/DF60.PDB" $(SOURCE)


!ELSEIF  "$(CFG)" == "wlt_3d - Win32 Debug"


"$(INTDIR)\r1mach.obj"	"$(INTDIR)\r1mach.sbr" : $(SOURCE) "$(INTDIR)"
	$(F90) /browser:"Debug/" /check:bounds /compile_only /dbglibs /debug:full /fpp /include:"Debug/" /nologo /optimize:0 /traceback /warn:argument_checking /warn:declarations /warn:nofileopt /module:"Debug/" /object:"Debug/" /pdbfile:"Debug/DF60.PDB" $(SOURCE)


!ENDIF 

SOURCE=.\read_data_wray.f90

!IF  "$(CFG)" == "wlt_3d - Win32 Release"

F90_MODOUT=\
	"read_data_wry"


"$(INTDIR)\read_data_wray.obj"	"$(INTDIR)\read_data_wry.mod" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\fft_module.mod" "$(INTDIR)\precision.mod"
	$(F90) /check:bounds /check:power /check:overflow /check:underflow /compile_only /debug:full /fpp /include:"Release/" /nologo /traceback /warn:nogeneral /module:"Release/" /object:"Release/" /pdbfile:"Release/DF60.PDB" $(SOURCE)


!ELSEIF  "$(CFG)" == "wlt_3d - Win32 Debug"

F90_MODOUT=\
	"read_data_wry"


"$(INTDIR)\read_data_wray.obj"	"$(INTDIR)\read_data_wray.sbr"	"$(INTDIR)\read_data_wry.mod" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\fft_module.mod" "$(INTDIR)\precision.mod"
	$(F90) /browser:"Debug/" /check:bounds /compile_only /dbglibs /debug:full /fpp /include:"Debug/" /nologo /optimize:0 /traceback /warn:argument_checking /warn:declarations /warn:nofileopt /module:"Debug/" /object:"Debug/" /pdbfile:"Debug/DF60.PDB" $(SOURCE)


!ENDIF 

SOURCE=.\read_init.f90

!IF  "$(CFG)" == "wlt_3d - Win32 Release"

F90_MODOUT=\
	"read_init_mod"


"$(INTDIR)\read_init.obj"	"$(INTDIR)\read_init_mod.mod" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\read_data_wry.mod" "$(INTDIR)\fft_module.mod" "$(INTDIR)\ch_resolution.mod" "$(INTDIR)\endianness_mod.mod" "$(INTDIR)\read_netcdf_mod.mod" "$(INTDIR)\pde.mod" "$(INTDIR)\precision.mod" "$(INTDIR)\share_consts.mod" "$(INTDIR)\sizes.mod" "$(INTDIR)\spectra_module.mod" "$(INTDIR)\util_mod.mod" "$(INTDIR)\wlt_trns_util_mod.mod" "$(INTDIR)\wlt_trns_mod.mod"
	$(F90) /check:bounds /check:power /check:overflow /check:underflow /compile_only /debug:full /fpp /include:"Release/" /nologo /traceback /warn:nogeneral /module:"Release/" /object:"Release/" /pdbfile:"Release/DF60.PDB" $(SOURCE)


!ELSEIF  "$(CFG)" == "wlt_3d - Win32 Debug"

F90_MODOUT=\
	"read_init_mod"


"$(INTDIR)\read_init.obj"	"$(INTDIR)\read_init.sbr"	"$(INTDIR)\read_init_mod.mod" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\ch_resolution.mod" "$(INTDIR)\endianness_mod.mod" "$(INTDIR)\fft_module.mod" "$(INTDIR)\read_data_wry.mod" "$(INTDIR)\read_netcdf_mod.mod" "$(INTDIR)\pde.mod" "$(INTDIR)\precision.mod" "$(INTDIR)\share_consts.mod" "$(INTDIR)\sizes.mod" "$(INTDIR)\spectra_module.mod" "$(INTDIR)\util_mod.mod" "$(INTDIR)\wlt_trns_util_mod.mod" "$(INTDIR)\wlt_trns_mod.mod"
	$(F90) /browser:"Debug/" /check:bounds /compile_only /dbglibs /debug:full /fpp /include:"Debug/" /nologo /optimize:0 /traceback /warn:argument_checking /warn:declarations /warn:nofileopt /module:"Debug/" /object:"Debug/" /pdbfile:"Debug/DF60.PDB" $(SOURCE)


!ENDIF 

SOURCE=.\read_netcdf.f90

!IF  "$(CFG)" == "wlt_3d - Win32 Release"

F90_MODOUT=\
	"read_netcdf_mod"


"$(INTDIR)\read_netcdf.obj"	"$(INTDIR)\read_netcdf_mod.mod" : $(SOURCE) "$(INTDIR)"
	$(F90) /check:bounds /check:power /check:overflow /check:underflow /compile_only /debug:full /fpp /include:"Release/" /nologo /traceback /warn:nogeneral /module:"Release/" /object:"Release/" /pdbfile:"Release/DF60.PDB" $(SOURCE)


!ELSEIF  "$(CFG)" == "wlt_3d - Win32 Debug"

F90_MODOUT=\
	"read_netcdf_mod"


"$(INTDIR)\read_netcdf.obj"	"$(INTDIR)\read_netcdf.sbr"	"$(INTDIR)\read_netcdf_mod.mod" : $(SOURCE) "$(INTDIR)"
	$(F90) /browser:"Debug/" /check:bounds /compile_only /dbglibs /debug:full /fpp /include:"Debug/" /nologo /optimize:0 /traceback /warn:argument_checking /warn:declarations /warn:nofileopt /module:"Debug/" /object:"Debug/" /pdbfile:"Debug/DF60.PDB" $(SOURCE)


!ENDIF 

SOURCE=.\reverse_endian.f

!IF  "$(CFG)" == "wlt_3d - Win32 Release"


"$(INTDIR)\reverse_endian.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\precision.mod"
	$(F90) /check:bounds /check:power /check:overflow /check:underflow /compile_only /debug:full /fpp /include:"Release/" /nologo /traceback /warn:nogeneral /module:"Release/" /object:"Release/" /pdbfile:"Release/DF60.PDB" $(SOURCE)


!ELSEIF  "$(CFG)" == "wlt_3d - Win32 Debug"


"$(INTDIR)\reverse_endian.obj"	"$(INTDIR)\reverse_endian.sbr" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\precision.mod"
	$(F90) /browser:"Debug/" /check:bounds /compile_only /dbglibs /debug:full /fpp /include:"Debug/" /nologo /optimize:0 /traceback /warn:argument_checking /warn:declarations /warn:nofileopt /module:"Debug/" /object:"Debug/" /pdbfile:"Debug/DF60.PDB" $(SOURCE)


!ENDIF 

SOURCE=.\shared_modules.f90

!IF  "$(CFG)" == "wlt_3d - Win32 Release"

F90_MODOUT=\
	"field" \
	"misc_vars" \
	"pde" \
	"pde_unused" \
	"penalization" \
	"precision" \
	"share_consts" \
	"share_kry" \
	"sizes" \
	"util_vars"


"$(INTDIR)\shared_modules.obj"	"$(INTDIR)\field.mod"	"$(INTDIR)\misc_vars.mod"	"$(INTDIR)\pde.mod"	"$(INTDIR)\pde_unused.mod"	"$(INTDIR)\penalization.mod"	"$(INTDIR)\precision.mod"	"$(INTDIR)\share_consts.mod"	"$(INTDIR)\share_kry.mod"	"$(INTDIR)\sizes.mod"	"$(INTDIR)\util_vars.mod" : $(SOURCE) "$(INTDIR)"
	$(F90) /check:bounds /check:power /check:overflow /check:underflow /compile_only /debug:full /fpp /include:"Release/" /nologo /traceback /warn:nogeneral /module:"Release/" /object:"Release/" /pdbfile:"Release/DF60.PDB" $(SOURCE)


!ELSEIF  "$(CFG)" == "wlt_3d - Win32 Debug"

F90_MODOUT=\
	"field" \
	"misc_vars" \
	"pde" \
	"pde_unused" \
	"penalization" \
	"precision" \
	"share_consts" \
	"share_kry" \
	"sizes" \
	"util_vars"


"$(INTDIR)\shared_modules.obj"	"$(INTDIR)\shared_modules.sbr"	"$(INTDIR)\field.mod"	"$(INTDIR)\misc_vars.mod"	"$(INTDIR)\pde.mod"	"$(INTDIR)\pde_unused.mod"	"$(INTDIR)\penalization.mod"	"$(INTDIR)\precision.mod"	"$(INTDIR)\share_consts.mod"	"$(INTDIR)\share_kry.mod"	"$(INTDIR)\sizes.mod"	"$(INTDIR)\util_vars.mod" : $(SOURCE) "$(INTDIR)"
	$(F90) /browser:"Debug/" /check:bounds /compile_only /dbglibs /debug:full /fpp /include:"Debug/" /nologo /optimize:0 /traceback /warn:argument_checking /warn:declarations /warn:nofileopt /module:"Debug/" /object:"Debug/" /pdbfile:"Debug/DF60.PDB" $(SOURCE)


!ENDIF 

SOURCE=.\fft\spectra.f90

!IF  "$(CFG)" == "wlt_3d - Win32 Release"

F90_MODOUT=\
	"spectra_module"


"$(INTDIR)\spectra.obj"	"$(INTDIR)\spectra_module.mod" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\fft_module.mod"
	$(F90) /check:bounds /check:power /check:overflow /check:underflow /compile_only /debug:full /fpp /include:"Release/" /nologo /traceback /warn:nogeneral /module:"Release/" /object:"Release/" /pdbfile:"Release/DF60.PDB" $(SOURCE)


!ELSEIF  "$(CFG)" == "wlt_3d - Win32 Debug"

F90_MODOUT=\
	"spectra_module"


"$(INTDIR)\spectra.obj"	"$(INTDIR)\spectra.sbr"	"$(INTDIR)\spectra_module.mod" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\fft_module.mod"
	$(F90) /browser:"Debug/" /check:bounds /compile_only /dbglibs /debug:full /fpp /include:"Debug/" /nologo /optimize:0 /traceback /warn:argument_checking /warn:declarations /warn:nofileopt /module:"Debug/" /object:"Debug/" /pdbfile:"Debug/DF60.PDB" $(SOURCE)


!ENDIF 

SOURCE=.\time_int_cn.f90

!IF  "$(CFG)" == "wlt_3d - Win32 Release"

F90_MODOUT=\
	"time_int_cn_mod"


"$(INTDIR)\time_int_cn.obj"	"$(INTDIR)\time_int_cn_mod.mod" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\misc_vars.mod" "$(INTDIR)\elliptic_mod.mod" "$(INTDIR)\user_case.mod" "$(INTDIR)\share_kry.mod" "$(INTDIR)\precision.mod" "$(INTDIR)\util_vars.mod" "$(INTDIR)\pde.mod" "$(INTDIR)\penalization.mod" "$(INTDIR)\share_consts.mod" "$(INTDIR)\sizes.mod" "$(INTDIR)\util_mod.mod" "$(INTDIR)\vector_util_mod.mod" "$(INTDIR)\wlt_trns_mod.mod"
	$(F90) /check:bounds /check:power /check:overflow /check:underflow /compile_only /debug:full /fpp /include:"Release/" /nologo /traceback /warn:nogeneral /module:"Release/" /object:"Release/" /pdbfile:"Release/DF60.PDB" $(SOURCE)


!ELSEIF  "$(CFG)" == "wlt_3d - Win32 Debug"

F90_MODOUT=\
	"time_int_cn_mod"


"$(INTDIR)\time_int_cn.obj"	"$(INTDIR)\time_int_cn.sbr"	"$(INTDIR)\time_int_cn_mod.mod" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\user_case.mod" "$(INTDIR)\elliptic_mod.mod" "$(INTDIR)\misc_vars.mod" "$(INTDIR)\pde.mod" "$(INTDIR)\penalization.mod" "$(INTDIR)\precision.mod" "$(INTDIR)\share_consts.mod" "$(INTDIR)\share_kry.mod" "$(INTDIR)\sizes.mod" "$(INTDIR)\util_vars.mod" "$(INTDIR)\util_mod.mod" "$(INTDIR)\vector_util_mod.mod" "$(INTDIR)\wlt_trns_mod.mod"
	$(F90) /browser:"Debug/" /check:bounds /compile_only /dbglibs /debug:full /fpp /include:"Debug/" /nologo /optimize:0 /traceback /warn:argument_checking /warn:declarations /warn:nofileopt /module:"Debug/" /object:"Debug/" /pdbfile:"Debug/DF60.PDB" $(SOURCE)


!ENDIF 

SOURCE=.\time_int_krylov_3d.f90

!IF  "$(CFG)" == "wlt_3d - Win32 Release"

F90_MODOUT=\
	"kry_mod"


"$(INTDIR)\time_int_krylov_3d.obj"	"$(INTDIR)\kry_mod.mod" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\user_case.mod" "$(INTDIR)\share_kry.mod" "$(INTDIR)\precision.mod" "$(INTDIR)\pde.mod" "$(INTDIR)\share_consts.mod" "$(INTDIR)\sizes.mod"
	$(F90) /check:bounds /check:power /check:overflow /check:underflow /compile_only /debug:full /fpp /include:"Release/" /nologo /traceback /warn:nogeneral /module:"Release/" /object:"Release/" /pdbfile:"Release/DF60.PDB" $(SOURCE)


!ELSEIF  "$(CFG)" == "wlt_3d - Win32 Debug"

F90_MODOUT=\
	"kry_mod"


"$(INTDIR)\time_int_krylov_3d.obj"	"$(INTDIR)\time_int_krylov_3d.sbr"	"$(INTDIR)\kry_mod.mod" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\user_case.mod" "$(INTDIR)\pde.mod" "$(INTDIR)\precision.mod" "$(INTDIR)\share_consts.mod" "$(INTDIR)\share_kry.mod" "$(INTDIR)\sizes.mod"
	$(F90) /browser:"Debug/" /check:bounds /compile_only /dbglibs /debug:full /fpp /include:"Debug/" /nologo /optimize:0 /traceback /warn:argument_checking /warn:declarations /warn:nofileopt /module:"Debug/" /object:"Debug/" /pdbfile:"Debug/DF60.PDB" $(SOURCE)


!ENDIF 

SOURCE=.\util_3d.f90

!IF  "$(CFG)" == "wlt_3d - Win32 Release"

F90_MODOUT=\
	"util_mod"


"$(INTDIR)\util_3d.obj"	"$(INTDIR)\util_mod.mod" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\misc_vars.mod" "$(INTDIR)\sizes.mod" "$(INTDIR)\precision.mod" "$(INTDIR)\pde.mod" "$(INTDIR)\util_vars.mod" "$(INTDIR)\wlt_vars.mod" "$(INTDIR)\wlt_trns_mod.mod" "$(INTDIR)\wavelet_filters_vars.mod"
	$(F90) /check:bounds /check:power /check:overflow /check:underflow /compile_only /debug:full /fpp /include:"Release/" /nologo /traceback /warn:nogeneral /module:"Release/" /object:"Release/" /pdbfile:"Release/DF60.PDB" $(SOURCE)


!ELSEIF  "$(CFG)" == "wlt_3d - Win32 Debug"

F90_MODOUT=\
	"util_mod"


"$(INTDIR)\util_3d.obj"	"$(INTDIR)\util_3d.sbr"	"$(INTDIR)\util_mod.mod" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\misc_vars.mod" "$(INTDIR)\pde.mod" "$(INTDIR)\precision.mod" "$(INTDIR)\sizes.mod" "$(INTDIR)\util_vars.mod" "$(INTDIR)\wlt_vars.mod" "$(INTDIR)\wlt_trns_mod.mod" "$(INTDIR)\wavelet_filters_vars.mod"
	$(F90) /browser:"Debug/" /check:bounds /compile_only /dbglibs /debug:full /fpp /include:"Debug/" /nologo /optimize:0 /traceback /warn:argument_checking /warn:declarations /warn:nofileopt /module:"Debug/" /object:"Debug/" /pdbfile:"Debug/DF60.PDB" $(SOURCE)


!ENDIF 

SOURCE=.\vector_util.f90

!IF  "$(CFG)" == "wlt_3d - Win32 Release"

F90_MODOUT=\
	"vector_util_mod"


"$(INTDIR)\vector_util.obj"	"$(INTDIR)\vector_util_mod.mod" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\precision.mod" "$(INTDIR)\pde.mod" "$(INTDIR)\wlt_vars.mod" "$(INTDIR)\wlt_trns_mod.mod"
	$(F90) /check:bounds /check:power /check:overflow /check:underflow /compile_only /debug:full /fpp /include:"Release/" /nologo /traceback /warn:nogeneral /module:"Release/" /object:"Release/" /pdbfile:"Release/DF60.PDB" $(SOURCE)


!ELSEIF  "$(CFG)" == "wlt_3d - Win32 Debug"

F90_MODOUT=\
	"vector_util_mod"


"$(INTDIR)\vector_util.obj"	"$(INTDIR)\vector_util.sbr"	"$(INTDIR)\vector_util_mod.mod" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\pde.mod" "$(INTDIR)\precision.mod" "$(INTDIR)\wlt_vars.mod" "$(INTDIR)\wlt_trns_mod.mod"
	$(F90) /browser:"Debug/" /check:bounds /compile_only /dbglibs /debug:full /fpp /include:"Debug/" /nologo /optimize:0 /traceback /warn:argument_checking /warn:declarations /warn:nofileopt /module:"Debug/" /object:"Debug/" /pdbfile:"Debug/DF60.PDB" $(SOURCE)


!ENDIF 

SOURCE=.\wavelet_3d.f90

!IF  "$(CFG)" == "wlt_3d - Win32 Release"

F90_MODOUT=\
	"wlt_trns_util_mod"


"$(INTDIR)\wavelet_3d.obj"	"$(INTDIR)\wlt_trns_util_mod.mod" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\sizes.mod" "$(INTDIR)\precision.mod" "$(INTDIR)\wlt_vars.mod" "$(INTDIR)\wlt_trns_vars.mod"
	$(F90) /check:bounds /check:power /check:overflow /check:underflow /compile_only /debug:full /fpp /include:"Release/" /nologo /traceback /warn:nogeneral /module:"Release/" /object:"Release/" /pdbfile:"Release/DF60.PDB" $(SOURCE)


!ELSEIF  "$(CFG)" == "wlt_3d - Win32 Debug"

F90_MODOUT=\
	"wlt_trns_util_mod"


"$(INTDIR)\wavelet_3d.obj"	"$(INTDIR)\wavelet_3d.sbr"	"$(INTDIR)\wlt_trns_util_mod.mod" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\precision.mod" "$(INTDIR)\sizes.mod" "$(INTDIR)\wlt_vars.mod" "$(INTDIR)\wlt_trns_vars.mod"
	$(F90) /browser:"Debug/" /check:bounds /compile_only /dbglibs /debug:full /fpp /include:"Debug/" /nologo /optimize:0 /traceback /warn:argument_checking /warn:declarations /warn:nofileopt /module:"Debug/" /object:"Debug/" /pdbfile:"Debug/DF60.PDB" $(SOURCE)


!ENDIF 

SOURCE=.\wavelet_3d_vars.f90

!IF  "$(CFG)" == "wlt_3d - Win32 Release"

F90_MODOUT=\
	"wlt_vars"


"$(INTDIR)\wavelet_3d_vars.obj"	"$(INTDIR)\wlt_vars.mod" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\precision.mod"
	$(F90) $(F90_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "wlt_3d - Win32 Debug"

F90_MODOUT=\
	"wlt_vars"


"$(INTDIR)\wavelet_3d_vars.obj"	"$(INTDIR)\wavelet_3d_vars.sbr"	"$(INTDIR)\wlt_vars.mod" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\precision.mod"
	$(F90) $(F90_PROJ) $(SOURCE)


!ENDIF 

SOURCE=.\wavelet_3d_wrk.f90

!IF  "$(CFG)" == "wlt_3d - Win32 Release"

F90_MODOUT=\
	"wlt_trns_mod"


"$(INTDIR)\wavelet_3d_wrk.obj"	"$(INTDIR)\wlt_trns_mod.mod" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\precision.mod" "$(INTDIR)\wlt_vars.mod" "$(INTDIR)\wlt_trns_util_mod.mod" "$(INTDIR)\io_3d_vars.mod" "$(INTDIR)\sizes.mod" "$(INTDIR)\pde.mod" "$(INTDIR)\util_vars.mod" "$(INTDIR)\share_consts.mod" "$(INTDIR)\field.mod" "$(INTDIR)\wlt_trns_vars.mod"
	$(F90) /check:bounds /check:power /check:overflow /check:underflow /compile_only /debug:full /fpp /include:"Release/" /nologo /traceback /warn:nogeneral /module:"Release/" /object:"Release/" /pdbfile:"Release/DF60.PDB" $(SOURCE)


!ELSEIF  "$(CFG)" == "wlt_3d - Win32 Debug"

F90_MODOUT=\
	"wlt_trns_mod"


"$(INTDIR)\wavelet_3d_wrk.obj"	"$(INTDIR)\wavelet_3d_wrk.sbr"	"$(INTDIR)\wlt_trns_mod.mod" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\io_3D_vars.mod" "$(INTDIR)\field.mod" "$(INTDIR)\pde.mod" "$(INTDIR)\precision.mod" "$(INTDIR)\share_consts.mod" "$(INTDIR)\sizes.mod" "$(INTDIR)\util_vars.mod" "$(INTDIR)\wlt_trns_util_mod.mod" "$(INTDIR)\wlt_vars.mod" "$(INTDIR)\wlt_trns_vars.mod"
	$(F90) /browser:"Debug/" /check:bounds /compile_only /dbglibs /debug:full /fpp /include:"Debug/" /nologo /optimize:0 /traceback /warn:argument_checking /warn:declarations /warn:nofileopt /module:"Debug/" /object:"Debug/" /pdbfile:"Debug/DF60.PDB" $(SOURCE)


!ENDIF 

SOURCE=.\wavelet_3d_wrk_vars.f90

!IF  "$(CFG)" == "wlt_3d - Win32 Release"

F90_MODOUT=\
	"wlt_trns_vars"


"$(INTDIR)\wavelet_3d_wrk_vars.obj"	"$(INTDIR)\wlt_trns_vars.mod" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\wlt_vars.mod"
	$(F90) $(F90_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "wlt_3d - Win32 Debug"

F90_MODOUT=\
	"wlt_trns_vars"


"$(INTDIR)\wavelet_3d_wrk_vars.obj"	"$(INTDIR)\wavelet_3d_wrk_vars.sbr"	"$(INTDIR)\wlt_trns_vars.mod" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\wlt_vars.mod"
	$(F90) $(F90_PROJ) $(SOURCE)


!ENDIF 

SOURCE=.\wavelet_filters.f90

!IF  "$(CFG)" == "wlt_3d - Win32 Release"

F90_MODOUT=\
	"wavelet_filters_mod"


"$(INTDIR)\wavelet_filters.obj"	"$(INTDIR)\wavelet_filters_mod.mod" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\wlt_vars.mod" "$(INTDIR)\pde.mod" "$(INTDIR)\sizes.mod" "$(INTDIR)\wlt_trns_mod.mod" "$(INTDIR)\precision.mod" "$(INTDIR)\wlt_trns_vars.mod" "$(INTDIR)\wlt_trns_util_mod.mod" "$(INTDIR)\wavelet_filters_vars.mod"
	$(F90) $(F90_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "wlt_3d - Win32 Debug"

F90_MODOUT=\
	"wavelet_filters_mod"


"$(INTDIR)\wavelet_filters.obj"	"$(INTDIR)\wavelet_filters.sbr"	"$(INTDIR)\wavelet_filters_mod.mod" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\pde.mod" "$(INTDIR)\precision.mod" "$(INTDIR)\sizes.mod" "$(INTDIR)\wlt_trns_util_mod.mod" "$(INTDIR)\wlt_vars.mod" "$(INTDIR)\wlt_trns_mod.mod" "$(INTDIR)\wlt_trns_vars.mod" "$(INTDIR)\wavelet_filters_vars.mod"
	$(F90) $(F90_PROJ) $(SOURCE)


!ENDIF 

SOURCE=.\wavelet_filters_vars.f90

!IF  "$(CFG)" == "wlt_3d - Win32 Release"

F90_MODOUT=\
	"wavelet_filters_vars"


"$(INTDIR)\wavelet_filters_vars.obj"	"$(INTDIR)\wavelet_filters_vars.mod" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\precision.mod"
	$(F90) $(F90_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "wlt_3d - Win32 Debug"

F90_MODOUT=\
	"wavelet_filters_vars"


"$(INTDIR)\wavelet_filters_vars.obj"	"$(INTDIR)\wavelet_filters_vars.sbr"	"$(INTDIR)\wavelet_filters_vars.mod" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\precision.mod"
	$(F90) $(F90_PROJ) $(SOURCE)


!ENDIF 

SOURCE=.\wlt_3d_main.f90

!IF  "$(CFG)" == "wlt_3d - Win32 Release"


"$(INTDIR)\wlt_3d_main.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\precision.mod" "$(INTDIR)\wlt_vars.mod" "$(INTDIR)\util_vars.mod" "$(INTDIR)\wavelet_filters_mod.mod" "$(INTDIR)\wavelet_filters_vars.mod" "$(INTDIR)\elliptic_vars.mod" "$(INTDIR)\misc_vars.mod" "$(INTDIR)\wlt_trns_mod.mod" "$(INTDIR)\share_kry.mod" "$(INTDIR)\pde.mod" "$(INTDIR)\share_consts.mod" "$(INTDIR)\sizes.mod" "$(INTDIR)\user_case.mod" "$(INTDIR)\io_3D.mod" "$(INTDIR)\io_3d_vars.mod" "$(INTDIR)\wlt_trns_util_mod.mod" "$(INTDIR)\kry_mod.mod" "$(INTDIR)\read_init_mod.mod" "$(INTDIR)\util_mod.mod" "$(INTDIR)\elliptic_mod.mod" "$(INTDIR)\time_int_cn_mod.mod" "$(INTDIR)\read_netcdf_mod.mod" "$(INTDIR)\field.mod" "$(INTDIR)\default_mod.mod"
	$(F90) /check:bounds /check:power /check:overflow /check:underflow /compile_only /debug:full /fpp /include:"Release/" /nologo /traceback /warn:nogeneral /module:"Release/" /object:"Release/" /pdbfile:"Release/DF60.PDB" $(SOURCE)


!ELSEIF  "$(CFG)" == "wlt_3d - Win32 Debug"


"$(INTDIR)\wlt_3d_main.obj"	"$(INTDIR)\wlt_3d_main.sbr" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\user_case.mod" "$(INTDIR)\default_mod.mod" "$(INTDIR)\elliptic_mod.mod" "$(INTDIR)\elliptic_vars.mod" "$(INTDIR)\io_3D.mod" "$(INTDIR)\io_3D_vars.mod" "$(INTDIR)\read_init_mod.mod" "$(INTDIR)\read_netcdf_mod.mod" "$(INTDIR)\field.mod" "$(INTDIR)\misc_vars.mod" "$(INTDIR)\pde.mod" "$(INTDIR)\precision.mod" "$(INTDIR)\share_consts.mod" "$(INTDIR)\share_kry.mod" "$(INTDIR)\sizes.mod" "$(INTDIR)\util_vars.mod" "$(INTDIR)\time_int_cn_mod.mod" "$(INTDIR)\kry_mod.mod" "$(INTDIR)\util_mod.mod" "$(INTDIR)\wlt_trns_util_mod.mod" "$(INTDIR)\wlt_vars.mod" "$(INTDIR)\wlt_trns_mod.mod" "$(INTDIR)\wavelet_filters_mod.mod" "$(INTDIR)\wavelet_filters_vars.mod"
	$(F90) /browser:"Debug/" /check:bounds /compile_only /dbglibs /debug:full /fpp /include:"Debug/" /nologo /optimize:0 /traceback /warn:argument_checking /warn:declarations /warn:nofileopt /module:"Debug/" /object:"Debug/" /pdbfile:"Debug/DF60.PDB" $(SOURCE)


!ENDIF 

SOURCE=.\zgetrf.f

!IF  "$(CFG)" == "wlt_3d - Win32 Release"


"$(INTDIR)\zgetrf.obj" : $(SOURCE) "$(INTDIR)"
	$(F90) /check:bounds /check:power /check:overflow /check:underflow /compile_only /debug:full /fpp /include:"Release/" /nologo /traceback /warn:nogeneral /module:"Release/" /object:"Release/" /pdbfile:"Release/DF60.PDB" $(SOURCE)


!ELSEIF  "$(CFG)" == "wlt_3d - Win32 Debug"


"$(INTDIR)\zgetrf.obj"	"$(INTDIR)\zgetrf.sbr" : $(SOURCE) "$(INTDIR)"
	$(F90) /browser:"Debug/" /check:bounds /compile_only /dbglibs /debug:full /fpp /include:"Debug/" /nologo /optimize:0 /traceback /warn:argument_checking /warn:declarations /warn:nofileopt /module:"Debug/" /object:"Debug/" /pdbfile:"Debug/DF60.PDB" $(SOURCE)


!ENDIF 

SOURCE=.\zgetri.f

!IF  "$(CFG)" == "wlt_3d - Win32 Release"


"$(INTDIR)\zgetri.obj" : $(SOURCE) "$(INTDIR)"
	$(F90) /check:bounds /check:power /check:overflow /check:underflow /compile_only /debug:full /fpp /include:"Release/" /nologo /traceback /warn:nogeneral /module:"Release/" /object:"Release/" /pdbfile:"Release/DF60.PDB" $(SOURCE)


!ELSEIF  "$(CFG)" == "wlt_3d - Win32 Debug"


"$(INTDIR)\zgetri.obj"	"$(INTDIR)\zgetri.sbr" : $(SOURCE) "$(INTDIR)"
	$(F90) /browser:"Debug/" /check:bounds /compile_only /dbglibs /debug:full /fpp /include:"Debug/" /nologo /optimize:0 /traceback /warn:argument_checking /warn:declarations /warn:nofileopt /module:"Debug/" /object:"Debug/" /pdbfile:"Debug/DF60.PDB" $(SOURCE)


!ENDIF 


!ENDIF 

