# Microsoft Developer Studio Generated NMAKE File, Based on wlt_3d_pseudo_dblines.dsp
!IF "$(CFG)" == ""
CFG=wlt_3d_pseudo_dblines - Win32 Debug
!MESSAGE No configuration specified. Defaulting to wlt_3d_pseudo_dblines - Win32 Debug.
!ENDIF 

!IF "$(CFG)" != "wlt_3d_pseudo_dblines - Win32 Release" && "$(CFG)" != "wlt_3d_pseudo_dblines - Win32 Debug"
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "wlt_3d_pseudo_dblines.mak" CFG="wlt_3d_pseudo_dblines - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "wlt_3d_pseudo_dblines - Win32 Release" (based on "Win32 (x86) Console Application")
!MESSAGE "wlt_3d_pseudo_dblines - Win32 Debug" (based on "Win32 (x86) Console Application")
!MESSAGE 
!ERROR An invalid configuration is specified.
!ENDIF 

!IF "$(OS)" == "Windows_NT"
NULL=
!ELSE 
NULL=nul
!ENDIF 

CPP=cl.exe
F90=df.exe
RSC=rc.exe

!IF  "$(CFG)" == "wlt_3d_pseudo_dblines - Win32 Release"

OUTDIR=.\Release
INTDIR=.\Release
# Begin Custom Macros
OutDir=.\Release
# End Custom Macros

ALL : "$(OUTDIR)\wlt_3d_pseudo_dblines.exe" "$(OUTDIR)\fft_util.mod" "$(OUTDIR)\sgs_models.mod" "$(OUTDIR)\pde_unused.mod" "$(OUTDIR)\time_int_meth2_mod.mod"


CLEAN :
	-@erase "$(INTDIR)\case_isoturb_timeint_cn.obj"
	-@erase "$(INTDIR)\ch_resolution.mod"
	-@erase "$(INTDIR)\ch_resolution_fs.obj"
	-@erase "$(INTDIR)\d1mach.obj"
	-@erase "$(INTDIR)\db_lines.obj"
	-@erase "$(INTDIR)\db_mod.mod"
	-@erase "$(INTDIR)\default_mod.mod"
	-@erase "$(INTDIR)\default_util.obj"
	-@erase "$(INTDIR)\derf.obj"
	-@erase "$(INTDIR)\derfc.obj"
	-@erase "$(INTDIR)\dgamma.obj"
	-@erase "$(INTDIR)\dgeev.obj"
	-@erase "$(INTDIR)\dgels.obj"
	-@erase "$(INTDIR)\dqag.obj"
	-@erase "$(INTDIR)\dqage.obj"
	-@erase "$(INTDIR)\dtrsm.obj"
	-@erase "$(INTDIR)\elliptic_mod.mod"
	-@erase "$(INTDIR)\elliptic_solve_3d.obj"
	-@erase "$(INTDIR)\endianness_mod.mod"
	-@erase "$(INTDIR)\endienness_small.obj"
	-@erase "$(INTDIR)\fft.interface.temperton.obj"
	-@erase "$(INTDIR)\fft.obj"
	-@erase "$(INTDIR)\fft_interface_module.mod"
	-@erase "$(INTDIR)\fft_module.mod"
	-@erase "$(INTDIR)\fft_util.mod"
	-@erase "$(INTDIR)\fft_util.obj"
	-@erase "$(INTDIR)\fftpacktvms.obj"
	-@erase "$(INTDIR)\field.mod"
	-@erase "$(INTDIR)\gaussq.obj"
	-@erase "$(INTDIR)\input_file_reader.mod"
	-@erase "$(INTDIR)\input_files_reader.obj"
	-@erase "$(INTDIR)\io_3D.mod"
	-@erase "$(INTDIR)\io_3d.obj"
	-@erase "$(INTDIR)\kry_mod.mod"
	-@erase "$(INTDIR)\misc_vars.mod"
	-@erase "$(INTDIR)\needblas.obj"
	-@erase "$(INTDIR)\pde.mod"
	-@erase "$(INTDIR)\pde_unused.mod"
	-@erase "$(INTDIR)\penalization.mod"
	-@erase "$(INTDIR)\precision.mod"
	-@erase "$(INTDIR)\r1mach.obj"
	-@erase "$(INTDIR)\read_data_wray.obj"
	-@erase "$(INTDIR)\read_data_wry.mod"
	-@erase "$(INTDIR)\read_init.obj"
	-@erase "$(INTDIR)\read_init_mod.mod"
	-@erase "$(INTDIR)\read_netcdf.obj"
	-@erase "$(INTDIR)\read_netcdf_mod.mod"
	-@erase "$(INTDIR)\sgs_models.mod"
	-@erase "$(INTDIR)\sgs_models.obj"
	-@erase "$(INTDIR)\share_consts.mod"
	-@erase "$(INTDIR)\share_kry.mod"
	-@erase "$(INTDIR)\shared_modules.obj"
	-@erase "$(INTDIR)\sizes.mod"
	-@erase "$(INTDIR)\spectra.obj"
	-@erase "$(INTDIR)\spectra_module.mod"
	-@erase "$(INTDIR)\time_int_cn.obj"
	-@erase "$(INTDIR)\time_int_cn_mod.mod"
	-@erase "$(INTDIR)\time_int_krylov_3d.obj"
	-@erase "$(INTDIR)\time_int_meth2_aux_mod.mod"
	-@erase "$(INTDIR)\time_int_meth2_mod.mod"
	-@erase "$(INTDIR)\time_integration_meth2.obj"
	-@erase "$(INTDIR)\time_integration_meth2_aux.obj"
	-@erase "$(INTDIR)\user_case.mod"
	-@erase "$(INTDIR)\user_case_db.mod"
	-@erase "$(INTDIR)\user_case_db_defs.obj"
	-@erase "$(INTDIR)\user_elliptic_IC.obj"
	-@erase "$(INTDIR)\util_3d.obj"
	-@erase "$(INTDIR)\util_mod.mod"
	-@erase "$(INTDIR)\util_vars.mod"
	-@erase "$(INTDIR)\vector_util.obj"
	-@erase "$(INTDIR)\vector_util_mod.mod"
	-@erase "$(INTDIR)\wavelet_3d.obj"
	-@erase "$(INTDIR)\wavelet_3d_lines.obj"
	-@erase "$(INTDIR)\wlt_3d_main.obj"
	-@erase "$(INTDIR)\wlt_trns_mod.mod"
	-@erase "$(INTDIR)\wlt_trns_util_mod.mod"
	-@erase "$(INTDIR)\zgetrf.obj"
	-@erase "$(INTDIR)\zgetri.obj"
	-@erase "$(OUTDIR)\wlt_3d_pseudo_dblines.exe"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

F90_PROJ=/compile_only /include:"Release/" /nologo /warn:nofileopt /module:"Release/" /object:"Release/" 
F90_OBJS=.\Release/
CPP_PROJ=/nologo /ML /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_CONSOLE" /D "_MBCS" /Fp"$(INTDIR)\wlt_3d_pseudo_dblines.pch" /YX /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /c 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\wlt_3d_pseudo_dblines.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
LINK32_FLAGS=kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:console /incremental:no /pdb:"$(OUTDIR)\wlt_3d_pseudo_dblines.pdb" /machine:I386 /out:"$(OUTDIR)\wlt_3d_pseudo_dblines.exe" 
LINK32_OBJS= \
	"$(INTDIR)\case_isoturb_timeint_cn.obj" \
	"$(INTDIR)\ch_resolution_fs.obj" \
	"$(INTDIR)\d1mach.obj" \
	"$(INTDIR)\db_lines.obj" \
	"$(INTDIR)\default_util.obj" \
	"$(INTDIR)\derf.obj" \
	"$(INTDIR)\derfc.obj" \
	"$(INTDIR)\dgamma.obj" \
	"$(INTDIR)\dgeev.obj" \
	"$(INTDIR)\dgels.obj" \
	"$(INTDIR)\dqag.obj" \
	"$(INTDIR)\dqage.obj" \
	"$(INTDIR)\dtrsm.obj" \
	"$(INTDIR)\elliptic_solve_3d.obj" \
	"$(INTDIR)\endienness_small.obj" \
	"$(INTDIR)\fft.obj" \
	"$(INTDIR)\fft.interface.temperton.obj" \
	"$(INTDIR)\fft_util.obj" \
	"$(INTDIR)\fftpacktvms.obj" \
	"$(INTDIR)\gaussq.obj" \
	"$(INTDIR)\input_files_reader.obj" \
	"$(INTDIR)\io_3d.obj" \
	"$(INTDIR)\needblas.obj" \
	"$(INTDIR)\r1mach.obj" \
	"$(INTDIR)\read_data_wray.obj" \
	"$(INTDIR)\read_init.obj" \
	"$(INTDIR)\read_netcdf.obj" \
	"$(INTDIR)\sgs_models.obj" \
	"$(INTDIR)\shared_modules.obj" \
	"$(INTDIR)\spectra.obj" \
	"$(INTDIR)\time_int_cn.obj" \
	"$(INTDIR)\time_int_krylov_3d.obj" \
	"$(INTDIR)\time_integration_meth2.obj" \
	"$(INTDIR)\time_integration_meth2_aux.obj" \
	"$(INTDIR)\user_case_db_defs.obj" \
	"$(INTDIR)\user_elliptic_IC.obj" \
	"$(INTDIR)\util_3d.obj" \
	"$(INTDIR)\vector_util.obj" \
	"$(INTDIR)\wavelet_3d.obj" \
	"$(INTDIR)\wavelet_3d_lines.obj" \
	"$(INTDIR)\wlt_3d_main.obj" \
	"$(INTDIR)\zgetrf.obj" \
	"$(INTDIR)\zgetri.obj"

"$(OUTDIR)\wlt_3d_pseudo_dblines.exe" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ELSEIF  "$(CFG)" == "wlt_3d_pseudo_dblines - Win32 Debug"

OUTDIR=.\Debug
INTDIR=.\Debug
# Begin Custom Macros
OutDir=.\Debug
# End Custom Macros

ALL : "$(OUTDIR)\wlt_3d_pseudo_dblines.exe"


CLEAN :
	-@erase "$(INTDIR)\case_isoturb_timeint_cn.obj"
	-@erase "$(INTDIR)\ch_resolution_fs.obj"
	-@erase "$(INTDIR)\d1mach.obj"
	-@erase "$(INTDIR)\db_lines.obj"
	-@erase "$(INTDIR)\default_util.obj"
	-@erase "$(INTDIR)\derf.obj"
	-@erase "$(INTDIR)\derfc.obj"
	-@erase "$(INTDIR)\dgamma.obj"
	-@erase "$(INTDIR)\dgeev.obj"
	-@erase "$(INTDIR)\dgels.obj"
	-@erase "$(INTDIR)\dqag.obj"
	-@erase "$(INTDIR)\dqage.obj"
	-@erase "$(INTDIR)\dtrsm.obj"
	-@erase "$(INTDIR)\elliptic_solve_3d.obj"
	-@erase "$(INTDIR)\endienness_small.obj"
	-@erase "$(INTDIR)\fft.interface.temperton.obj"
	-@erase "$(INTDIR)\fft.obj"
	-@erase "$(INTDIR)\fft_util.obj"
	-@erase "$(INTDIR)\fftpacktvms.obj"
	-@erase "$(INTDIR)\gaussq.obj"
	-@erase "$(INTDIR)\input_files_reader.obj"
	-@erase "$(INTDIR)\io_3d.obj"
	-@erase "$(INTDIR)\needblas.obj"
	-@erase "$(INTDIR)\r1mach.obj"
	-@erase "$(INTDIR)\read_data_wray.obj"
	-@erase "$(INTDIR)\read_init.obj"
	-@erase "$(INTDIR)\read_netcdf.obj"
	-@erase "$(INTDIR)\sgs_models.obj"
	-@erase "$(INTDIR)\shared_modules.obj"
	-@erase "$(INTDIR)\spectra.obj"
	-@erase "$(INTDIR)\time_int_cn.obj"
	-@erase "$(INTDIR)\time_int_krylov_3d.obj"
	-@erase "$(INTDIR)\time_integration_meth2.obj"
	-@erase "$(INTDIR)\time_integration_meth2_aux.obj"
	-@erase "$(INTDIR)\user_case_db_defs.obj"
	-@erase "$(INTDIR)\user_elliptic_IC.obj"
	-@erase "$(INTDIR)\util_3d.obj"
	-@erase "$(INTDIR)\vector_util.obj"
	-@erase "$(INTDIR)\wavelet_3d.obj"
	-@erase "$(INTDIR)\wavelet_3d_lines.obj"
	-@erase "$(INTDIR)\wlt_3d_main.obj"
	-@erase "$(INTDIR)\zgetrf.obj"
	-@erase "$(INTDIR)\zgetri.obj"
	-@erase "$(OUTDIR)\wlt_3d_pseudo_dblines.exe"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

F90_PROJ=/check:bounds /compile_only /dbglibs /debug:partial /fpp /include:"Debug/" /nologo /optimize:0 /traceback /warn:argument_checking /warn:nofileopt /module:"Debug/" /object:"Debug/" 
F90_OBJS=.\Debug/
CPP_PROJ=/nologo /MLd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_CONSOLE" /D "_MBCS" /Fp"$(INTDIR)\wlt_3d_pseudo_dblines.pch" /YX /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /GZ /c 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\wlt_3d_pseudo_dblines.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
LINK32_FLAGS=netcdf.dll kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /stack:0x5f5e100,0x5f5e100 /subsystem:console /profile /debug /machine:I386 /out:"$(OUTDIR)\wlt_3d_pseudo_dblines.exe" /libpath:"c:\dan\research\netcdf\lib" 
LINK32_OBJS= \
	"$(INTDIR)\case_isoturb_timeint_cn.obj" \
	"$(INTDIR)\ch_resolution_fs.obj" \
	"$(INTDIR)\d1mach.obj" \
	"$(INTDIR)\db_lines.obj" \
	"$(INTDIR)\default_util.obj" \
	"$(INTDIR)\derf.obj" \
	"$(INTDIR)\derfc.obj" \
	"$(INTDIR)\dgamma.obj" \
	"$(INTDIR)\dgeev.obj" \
	"$(INTDIR)\dgels.obj" \
	"$(INTDIR)\dqag.obj" \
	"$(INTDIR)\dqage.obj" \
	"$(INTDIR)\dtrsm.obj" \
	"$(INTDIR)\elliptic_solve_3d.obj" \
	"$(INTDIR)\endienness_small.obj" \
	"$(INTDIR)\fft.obj" \
	"$(INTDIR)\fft.interface.temperton.obj" \
	"$(INTDIR)\fft_util.obj" \
	"$(INTDIR)\fftpacktvms.obj" \
	"$(INTDIR)\gaussq.obj" \
	"$(INTDIR)\input_files_reader.obj" \
	"$(INTDIR)\io_3d.obj" \
	"$(INTDIR)\needblas.obj" \
	"$(INTDIR)\r1mach.obj" \
	"$(INTDIR)\read_data_wray.obj" \
	"$(INTDIR)\read_init.obj" \
	"$(INTDIR)\read_netcdf.obj" \
	"$(INTDIR)\sgs_models.obj" \
	"$(INTDIR)\shared_modules.obj" \
	"$(INTDIR)\spectra.obj" \
	"$(INTDIR)\time_int_cn.obj" \
	"$(INTDIR)\time_int_krylov_3d.obj" \
	"$(INTDIR)\time_integration_meth2.obj" \
	"$(INTDIR)\time_integration_meth2_aux.obj" \
	"$(INTDIR)\user_case_db_defs.obj" \
	"$(INTDIR)\user_elliptic_IC.obj" \
	"$(INTDIR)\util_3d.obj" \
	"$(INTDIR)\vector_util.obj" \
	"$(INTDIR)\wavelet_3d.obj" \
	"$(INTDIR)\wavelet_3d_lines.obj" \
	"$(INTDIR)\wlt_3d_main.obj" \
	"$(INTDIR)\zgetrf.obj" \
	"$(INTDIR)\zgetri.obj"

"$(OUTDIR)\wlt_3d_pseudo_dblines.exe" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ENDIF 

.c{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.c{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.SUFFIXES: .fpp

.for{$(F90_OBJS)}.obj:
   $(F90) $(F90_PROJ) $<  

.f{$(F90_OBJS)}.obj:
   $(F90) $(F90_PROJ) $<  

.f90{$(F90_OBJS)}.obj:
   $(F90) $(F90_PROJ) $<  

.fpp{$(F90_OBJS)}.obj:
   $(F90) $(F90_PROJ) $<  


!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("wlt_3d_pseudo_dblines.dep")
!INCLUDE "wlt_3d_pseudo_dblines.dep"
!ELSE 
!MESSAGE Warning: cannot find "wlt_3d_pseudo_dblines.dep"
!ENDIF 
!ENDIF 


!IF "$(CFG)" == "wlt_3d_pseudo_dblines - Win32 Release" || "$(CFG)" == "wlt_3d_pseudo_dblines - Win32 Debug"
SOURCE=..\case_isoturb_timeint_cn.f90

!IF  "$(CFG)" == "wlt_3d_pseudo_dblines - Win32 Release"

F90_MODOUT=\
	"user_case"


"$(INTDIR)\case_isoturb_timeint_cn.obj"	"$(INTDIR)\user_case.mod" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\elliptic_mod.mod" "$(INTDIR)\fft_module.mod" "$(INTDIR)\input_file_reader.mod" "$(INTDIR)\field.mod" "$(INTDIR)\pde.mod" "$(INTDIR)\precision.mod" "$(INTDIR)\share_consts.mod" "$(INTDIR)\share_kry.mod" "$(INTDIR)\sizes.mod" "$(INTDIR)\spectra_module.mod" "$(INTDIR)\util_mod.mod" "$(INTDIR)\vector_util_mod.mod" "$(INTDIR)\wlt_trns_util_mod.mod" "$(INTDIR)\wlt_trns_mod.mod"
	$(F90) $(F90_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "wlt_3d_pseudo_dblines - Win32 Debug"


"$(INTDIR)\case_isoturb_timeint_cn.obj" : $(SOURCE) "$(INTDIR)"
	$(F90) $(F90_PROJ) $(SOURCE)


!ENDIF 

SOURCE=..\fft\ch_resolution_fs.f90

!IF  "$(CFG)" == "wlt_3d_pseudo_dblines - Win32 Release"

F90_MODOUT=\
	"ch_resolution"


"$(INTDIR)\ch_resolution_fs.obj"	"$(INTDIR)\ch_resolution.mod" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\precision.mod"
	$(F90) $(F90_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "wlt_3d_pseudo_dblines - Win32 Debug"


"$(INTDIR)\ch_resolution_fs.obj" : $(SOURCE) "$(INTDIR)"
	$(F90) $(F90_PROJ) $(SOURCE)


!ENDIF 

SOURCE=..\d1mach.f

"$(INTDIR)\d1mach.obj" : $(SOURCE) "$(INTDIR)"
	$(F90) $(F90_PROJ) $(SOURCE)


SOURCE=..\db_lines.f90

!IF  "$(CFG)" == "wlt_3d_pseudo_dblines - Win32 Release"

F90_MODOUT=\
	"db_mod"


"$(INTDIR)\db_lines.obj"	"$(INTDIR)\db_mod.mod" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\pde.mod" "$(INTDIR)\precision.mod" "$(INTDIR)\sizes.mod" "$(INTDIR)\user_case_db.mod" "$(INTDIR)\wlt_trns_util_mod.mod"
	$(F90) $(F90_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "wlt_3d_pseudo_dblines - Win32 Debug"


"$(INTDIR)\db_lines.obj" : $(SOURCE) "$(INTDIR)"
	$(F90) $(F90_PROJ) $(SOURCE)


!ENDIF 

SOURCE=..\default_util.f90

!IF  "$(CFG)" == "wlt_3d_pseudo_dblines - Win32 Release"

F90_MODOUT=\
	"default_mod"


"$(INTDIR)\default_util.obj"	"$(INTDIR)\default_mod.mod" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\user_case.mod" "$(INTDIR)\io_3D.mod" "$(INTDIR)\field.mod" "$(INTDIR)\pde.mod" "$(INTDIR)\precision.mod" "$(INTDIR)\share_consts.mod" "$(INTDIR)\sizes.mod" "$(INTDIR)\util_vars.mod" "$(INTDIR)\user_case_db.mod" "$(INTDIR)\wlt_trns_util_mod.mod" "$(INTDIR)\wlt_trns_mod.mod"
	$(F90) $(F90_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "wlt_3d_pseudo_dblines - Win32 Debug"


"$(INTDIR)\default_util.obj" : $(SOURCE) "$(INTDIR)"
	$(F90) $(F90_PROJ) $(SOURCE)


!ENDIF 

SOURCE=..\derf.f

"$(INTDIR)\derf.obj" : $(SOURCE) "$(INTDIR)"
	$(F90) $(F90_PROJ) $(SOURCE)


SOURCE=..\derfc.f

"$(INTDIR)\derfc.obj" : $(SOURCE) "$(INTDIR)"
	$(F90) $(F90_PROJ) $(SOURCE)


SOURCE=..\dgamma.f

"$(INTDIR)\dgamma.obj" : $(SOURCE) "$(INTDIR)"
	$(F90) $(F90_PROJ) $(SOURCE)


SOURCE=..\dgeev.f

"$(INTDIR)\dgeev.obj" : $(SOURCE) "$(INTDIR)"
	$(F90) $(F90_PROJ) $(SOURCE)


SOURCE=..\dgels.f

"$(INTDIR)\dgels.obj" : $(SOURCE) "$(INTDIR)"
	$(F90) $(F90_PROJ) $(SOURCE)


SOURCE=..\dqag.f

"$(INTDIR)\dqag.obj" : $(SOURCE) "$(INTDIR)"
	$(F90) $(F90_PROJ) $(SOURCE)


SOURCE=..\dqage.f

"$(INTDIR)\dqage.obj" : $(SOURCE) "$(INTDIR)"
	$(F90) $(F90_PROJ) $(SOURCE)


SOURCE=..\dtrsm.f

"$(INTDIR)\dtrsm.obj" : $(SOURCE) "$(INTDIR)"
	$(F90) $(F90_PROJ) $(SOURCE)


SOURCE=..\elliptic_solve_3d.f90

!IF  "$(CFG)" == "wlt_3d_pseudo_dblines - Win32 Release"

F90_MODOUT=\
	"elliptic_mod"


"$(INTDIR)\elliptic_solve_3d.obj"	"$(INTDIR)\elliptic_mod.mod" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\pde.mod" "$(INTDIR)\precision.mod" "$(INTDIR)\share_consts.mod" "$(INTDIR)\sizes.mod" "$(INTDIR)\util_vars.mod" "$(INTDIR)\wlt_trns_mod.mod"
	$(F90) $(F90_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "wlt_3d_pseudo_dblines - Win32 Debug"


"$(INTDIR)\elliptic_solve_3d.obj" : $(SOURCE) "$(INTDIR)"
	$(F90) $(F90_PROJ) $(SOURCE)


!ENDIF 

SOURCE=..\endienness_small.f90

!IF  "$(CFG)" == "wlt_3d_pseudo_dblines - Win32 Release"

F90_MODOUT=\
	"endianness_mod"


"$(INTDIR)\endienness_small.obj"	"$(INTDIR)\endianness_mod.mod" : $(SOURCE) "$(INTDIR)"
	$(F90) $(F90_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "wlt_3d_pseudo_dblines - Win32 Debug"


"$(INTDIR)\endienness_small.obj" : $(SOURCE) "$(INTDIR)"
	$(F90) $(F90_PROJ) $(SOURCE)


!ENDIF 

SOURCE=..\fft\fft.f90

!IF  "$(CFG)" == "wlt_3d_pseudo_dblines - Win32 Release"

F90_MODOUT=\
	"fft_module"


"$(INTDIR)\fft.obj"	"$(INTDIR)\fft_module.mod" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\fft_interface_module.mod" "$(INTDIR)\precision.mod"
	$(F90) $(F90_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "wlt_3d_pseudo_dblines - Win32 Debug"


"$(INTDIR)\fft.obj" : $(SOURCE) "$(INTDIR)"
	$(F90) $(F90_PROJ) $(SOURCE)


!ENDIF 

SOURCE=..\fft\fft.interface.temperton.f90

!IF  "$(CFG)" == "wlt_3d_pseudo_dblines - Win32 Release"

F90_MODOUT=\
	"fft_interface_module"


"$(INTDIR)\fft.interface.temperton.obj"	"$(INTDIR)\fft_interface_module.mod" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\precision.mod"
	$(F90) $(F90_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "wlt_3d_pseudo_dblines - Win32 Debug"


"$(INTDIR)\fft.interface.temperton.obj" : $(SOURCE) "$(INTDIR)"
	$(F90) $(F90_PROJ) $(SOURCE)


!ENDIF 

SOURCE=..\fft\fft_util.f90

!IF  "$(CFG)" == "wlt_3d_pseudo_dblines - Win32 Release"

F90_MODOUT=\
	"fft_util"


"$(INTDIR)\fft_util.obj"	"$(INTDIR)\fft_util.mod" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\fft_module.mod" "$(INTDIR)\precision.mod"
	$(F90) $(F90_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "wlt_3d_pseudo_dblines - Win32 Debug"


"$(INTDIR)\fft_util.obj" : $(SOURCE) "$(INTDIR)"
	$(F90) $(F90_PROJ) $(SOURCE)


!ENDIF 

SOURCE=..\fft\fftpacktvms.f

"$(INTDIR)\fftpacktvms.obj" : $(SOURCE) "$(INTDIR)"
	$(F90) $(F90_PROJ) $(SOURCE)


SOURCE=..\gaussq.f

"$(INTDIR)\gaussq.obj" : $(SOURCE) "$(INTDIR)"
	$(F90) $(F90_PROJ) $(SOURCE)


SOURCE=..\input_files_reader.f90

!IF  "$(CFG)" == "wlt_3d_pseudo_dblines - Win32 Release"

F90_MODOUT=\
	"input_file_reader"


"$(INTDIR)\input_files_reader.obj"	"$(INTDIR)\input_file_reader.mod" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\precision.mod"
	$(F90) $(F90_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "wlt_3d_pseudo_dblines - Win32 Debug"


"$(INTDIR)\input_files_reader.obj" : $(SOURCE) "$(INTDIR)"
	$(F90) $(F90_PROJ) $(SOURCE)


!ENDIF 

SOURCE=..\io_3d.f90

!IF  "$(CFG)" == "wlt_3d_pseudo_dblines - Win32 Release"

F90_MODOUT=\
	"io_3D"


"$(INTDIR)\io_3d.obj"	"$(INTDIR)\io_3D.mod" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\endianness_mod.mod" "$(INTDIR)\input_file_reader.mod" "$(INTDIR)\user_case.mod" "$(INTDIR)\field.mod" "$(INTDIR)\misc_vars.mod" "$(INTDIR)\pde.mod" "$(INTDIR)\penalization.mod" "$(INTDIR)\precision.mod" "$(INTDIR)\share_consts.mod" "$(INTDIR)\share_kry.mod" "$(INTDIR)\sizes.mod" "$(INTDIR)\wlt_trns_util_mod.mod" "$(INTDIR)\wlt_trns_mod.mod"
	$(F90) $(F90_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "wlt_3d_pseudo_dblines - Win32 Debug"


"$(INTDIR)\io_3d.obj" : $(SOURCE) "$(INTDIR)"
	$(F90) $(F90_PROJ) $(SOURCE)


!ENDIF 

SOURCE=..\needblas.f

"$(INTDIR)\needblas.obj" : $(SOURCE) "$(INTDIR)"
	$(F90) $(F90_PROJ) $(SOURCE)


SOURCE=..\r1mach.f

"$(INTDIR)\r1mach.obj" : $(SOURCE) "$(INTDIR)"
	$(F90) $(F90_PROJ) $(SOURCE)


SOURCE=..\read_data_wray.f90

!IF  "$(CFG)" == "wlt_3d_pseudo_dblines - Win32 Release"

F90_MODOUT=\
	"read_data_wry"


"$(INTDIR)\read_data_wray.obj"	"$(INTDIR)\read_data_wry.mod" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\fft_module.mod" "$(INTDIR)\precision.mod"
	$(F90) $(F90_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "wlt_3d_pseudo_dblines - Win32 Debug"


"$(INTDIR)\read_data_wray.obj" : $(SOURCE) "$(INTDIR)"
	$(F90) $(F90_PROJ) $(SOURCE)


!ENDIF 

SOURCE=..\read_init.f90

!IF  "$(CFG)" == "wlt_3d_pseudo_dblines - Win32 Release"

F90_MODOUT=\
	"read_init_mod"


"$(INTDIR)\read_init.obj"	"$(INTDIR)\read_init_mod.mod" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\read_data_wry.mod" "$(INTDIR)\fft_module.mod" "$(INTDIR)\ch_resolution.mod" "$(INTDIR)\endianness_mod.mod" "$(INTDIR)\read_netcdf_mod.mod" "$(INTDIR)\pde.mod" "$(INTDIR)\precision.mod" "$(INTDIR)\share_consts.mod" "$(INTDIR)\sizes.mod" "$(INTDIR)\spectra_module.mod" "$(INTDIR)\util_mod.mod" "$(INTDIR)\wlt_trns_util_mod.mod" "$(INTDIR)\wlt_trns_mod.mod"
	$(F90) $(F90_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "wlt_3d_pseudo_dblines - Win32 Debug"


"$(INTDIR)\read_init.obj" : $(SOURCE) "$(INTDIR)"
	$(F90) $(F90_PROJ) $(SOURCE)


!ENDIF 

SOURCE=..\read_netcdf.f90

!IF  "$(CFG)" == "wlt_3d_pseudo_dblines - Win32 Release"

F90_MODOUT=\
	"read_netcdf_mod"


"$(INTDIR)\read_netcdf.obj"	"$(INTDIR)\read_netcdf_mod.mod" : $(SOURCE) "$(INTDIR)"
	$(F90) $(F90_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "wlt_3d_pseudo_dblines - Win32 Debug"


"$(INTDIR)\read_netcdf.obj" : $(SOURCE) "$(INTDIR)"
	$(F90) $(F90_PROJ) $(SOURCE)


!ENDIF 

SOURCE=..\sgs_models.f90

!IF  "$(CFG)" == "wlt_3d_pseudo_dblines - Win32 Release"

F90_MODOUT=\
	"sgs_models"


"$(INTDIR)\sgs_models.obj"	"$(INTDIR)\sgs_models.mod" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\pde.mod" "$(INTDIR)\precision.mod" "$(INTDIR)\share_consts.mod" "$(INTDIR)\wlt_trns_util_mod.mod" "$(INTDIR)\wlt_trns_mod.mod"
	$(F90) $(F90_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "wlt_3d_pseudo_dblines - Win32 Debug"


"$(INTDIR)\sgs_models.obj" : $(SOURCE) "$(INTDIR)"
	$(F90) $(F90_PROJ) $(SOURCE)


!ENDIF 

SOURCE=..\shared_modules.f90

!IF  "$(CFG)" == "wlt_3d_pseudo_dblines - Win32 Release"

F90_MODOUT=\
	"field" \
	"misc_vars" \
	"pde" \
	"pde_unused" \
	"penalization" \
	"precision" \
	"share_consts" \
	"share_kry" \
	"sizes" \
	"util_vars"


"$(INTDIR)\shared_modules.obj"	"$(INTDIR)\field.mod"	"$(INTDIR)\misc_vars.mod"	"$(INTDIR)\pde.mod"	"$(INTDIR)\pde_unused.mod"	"$(INTDIR)\penalization.mod"	"$(INTDIR)\precision.mod"	"$(INTDIR)\share_consts.mod"	"$(INTDIR)\share_kry.mod"	"$(INTDIR)\sizes.mod"	"$(INTDIR)\util_vars.mod" : $(SOURCE) "$(INTDIR)"
	$(F90) $(F90_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "wlt_3d_pseudo_dblines - Win32 Debug"


"$(INTDIR)\shared_modules.obj" : $(SOURCE) "$(INTDIR)"
	$(F90) $(F90_PROJ) $(SOURCE)


!ENDIF 

SOURCE=..\fft\spectra.f90

!IF  "$(CFG)" == "wlt_3d_pseudo_dblines - Win32 Release"

F90_MODOUT=\
	"spectra_module"


"$(INTDIR)\spectra.obj"	"$(INTDIR)\spectra_module.mod" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\fft_module.mod"
	$(F90) $(F90_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "wlt_3d_pseudo_dblines - Win32 Debug"


"$(INTDIR)\spectra.obj" : $(SOURCE) "$(INTDIR)"
	$(F90) $(F90_PROJ) $(SOURCE)


!ENDIF 

SOURCE=..\time_int_cn.f90

!IF  "$(CFG)" == "wlt_3d_pseudo_dblines - Win32 Release"

F90_MODOUT=\
	"time_int_cn_mod"


"$(INTDIR)\time_int_cn.obj"	"$(INTDIR)\time_int_cn_mod.mod" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\misc_vars.mod" "$(INTDIR)\elliptic_mod.mod" "$(INTDIR)\user_case.mod" "$(INTDIR)\share_kry.mod" "$(INTDIR)\precision.mod" "$(INTDIR)\util_vars.mod" "$(INTDIR)\pde.mod" "$(INTDIR)\penalization.mod" "$(INTDIR)\share_consts.mod" "$(INTDIR)\sizes.mod" "$(INTDIR)\util_mod.mod" "$(INTDIR)\vector_util_mod.mod" "$(INTDIR)\wlt_trns_mod.mod"
	$(F90) $(F90_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "wlt_3d_pseudo_dblines - Win32 Debug"


"$(INTDIR)\time_int_cn.obj" : $(SOURCE) "$(INTDIR)"
	$(F90) $(F90_PROJ) $(SOURCE)


!ENDIF 

SOURCE=..\time_int_krylov_3d.f90

!IF  "$(CFG)" == "wlt_3d_pseudo_dblines - Win32 Release"

F90_MODOUT=\
	"kry_mod"


"$(INTDIR)\time_int_krylov_3d.obj"	"$(INTDIR)\kry_mod.mod" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\user_case.mod" "$(INTDIR)\share_kry.mod" "$(INTDIR)\precision.mod" "$(INTDIR)\pde.mod" "$(INTDIR)\share_consts.mod" "$(INTDIR)\sizes.mod"
	$(F90) $(F90_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "wlt_3d_pseudo_dblines - Win32 Debug"


"$(INTDIR)\time_int_krylov_3d.obj" : $(SOURCE) "$(INTDIR)"
	$(F90) $(F90_PROJ) $(SOURCE)


!ENDIF 

SOURCE=..\time_integration_meth2.f90

!IF  "$(CFG)" == "wlt_3d_pseudo_dblines - Win32 Release"

F90_MODOUT=\
	"time_int_meth2_mod"


"$(INTDIR)\time_integration_meth2.obj"	"$(INTDIR)\time_int_meth2_mod.mod" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\elliptic_mod.mod" "$(INTDIR)\precision.mod" "$(INTDIR)\pde.mod" "$(INTDIR)\default_mod.mod" "$(INTDIR)\user_case.mod" "$(INTDIR)\share_consts.mod" "$(INTDIR)\time_int_meth2_aux_mod.mod" "$(INTDIR)\util_mod.mod" "$(INTDIR)\vector_util_mod.mod" "$(INTDIR)\wlt_trns_mod.mod"
	$(F90) $(F90_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "wlt_3d_pseudo_dblines - Win32 Debug"


"$(INTDIR)\time_integration_meth2.obj" : $(SOURCE) "$(INTDIR)"
	$(F90) $(F90_PROJ) $(SOURCE)


!ENDIF 

SOURCE=..\time_integration_meth2_aux.f90

!IF  "$(CFG)" == "wlt_3d_pseudo_dblines - Win32 Release"

F90_MODOUT=\
	"time_int_meth2_aux_mod"


"$(INTDIR)\time_integration_meth2_aux.obj"	"$(INTDIR)\time_int_meth2_aux_mod.mod" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\elliptic_mod.mod" "$(INTDIR)\precision.mod" "$(INTDIR)\pde.mod" "$(INTDIR)\sizes.mod" "$(INTDIR)\util_mod.mod" "$(INTDIR)\vector_util_mod.mod" "$(INTDIR)\wlt_trns_util_mod.mod" "$(INTDIR)\wlt_trns_mod.mod"
	$(F90) $(F90_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "wlt_3d_pseudo_dblines - Win32 Debug"


"$(INTDIR)\time_integration_meth2_aux.obj" : $(SOURCE) "$(INTDIR)"
	$(F90) $(F90_PROJ) $(SOURCE)


!ENDIF 

SOURCE=..\user_case_db_defs.f90

!IF  "$(CFG)" == "wlt_3d_pseudo_dblines - Win32 Release"

F90_MODOUT=\
	"user_case_db"


"$(INTDIR)\user_case_db_defs.obj"	"$(INTDIR)\user_case_db.mod" : $(SOURCE) "$(INTDIR)"
	$(F90) $(F90_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "wlt_3d_pseudo_dblines - Win32 Debug"


"$(INTDIR)\user_case_db_defs.obj" : $(SOURCE) "$(INTDIR)"
	$(F90) $(F90_PROJ) $(SOURCE)


!ENDIF 

SOURCE=..\user_elliptic_IC.f90

"$(INTDIR)\user_elliptic_IC.obj" : $(SOURCE) "$(INTDIR)"
	$(F90) $(F90_PROJ) $(SOURCE)


SOURCE=..\util_3d.f90

!IF  "$(CFG)" == "wlt_3d_pseudo_dblines - Win32 Release"

F90_MODOUT=\
	"util_mod"


"$(INTDIR)\util_3d.obj"	"$(INTDIR)\util_mod.mod" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\misc_vars.mod" "$(INTDIR)\sizes.mod" "$(INTDIR)\precision.mod" "$(INTDIR)\pde.mod" "$(INTDIR)\util_vars.mod" "$(INTDIR)\wlt_trns_mod.mod"
	$(F90) $(F90_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "wlt_3d_pseudo_dblines - Win32 Debug"


"$(INTDIR)\util_3d.obj" : $(SOURCE) "$(INTDIR)"
	$(F90) $(F90_PROJ) $(SOURCE)


!ENDIF 

SOURCE=..\vector_util.f90

!IF  "$(CFG)" == "wlt_3d_pseudo_dblines - Win32 Release"

F90_MODOUT=\
	"vector_util_mod"


"$(INTDIR)\vector_util.obj"	"$(INTDIR)\vector_util_mod.mod" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\precision.mod" "$(INTDIR)\pde.mod" "$(INTDIR)\wlt_trns_mod.mod"
	$(F90) $(F90_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "wlt_3d_pseudo_dblines - Win32 Debug"


"$(INTDIR)\vector_util.obj" : $(SOURCE) "$(INTDIR)"
	$(F90) $(F90_PROJ) $(SOURCE)


!ENDIF 

SOURCE=..\wavelet_3d.f90

!IF  "$(CFG)" == "wlt_3d_pseudo_dblines - Win32 Release"

F90_MODOUT=\
	"wlt_trns_util_mod"


"$(INTDIR)\wavelet_3d.obj"	"$(INTDIR)\wlt_trns_util_mod.mod" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\sizes.mod" "$(INTDIR)\precision.mod"
	$(F90) $(F90_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "wlt_3d_pseudo_dblines - Win32 Debug"


"$(INTDIR)\wavelet_3d.obj" : $(SOURCE) "$(INTDIR)"
	$(F90) $(F90_PROJ) $(SOURCE)


!ENDIF 

SOURCE=..\wavelet_3d_lines.f90

!IF  "$(CFG)" == "wlt_3d_pseudo_dblines - Win32 Release"

F90_MODOUT=\
	"wlt_trns_mod"


"$(INTDIR)\wavelet_3d_lines.obj"	"$(INTDIR)\wlt_trns_mod.mod" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\precision.mod" "$(INTDIR)\wlt_trns_util_mod.mod" "$(INTDIR)\db_mod.mod" "$(INTDIR)\pde.mod" "$(INTDIR)\sizes.mod" "$(INTDIR)\field.mod" "$(INTDIR)\share_consts.mod" "$(INTDIR)\util_vars.mod"
	$(F90) $(F90_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "wlt_3d_pseudo_dblines - Win32 Debug"


"$(INTDIR)\wavelet_3d_lines.obj" : $(SOURCE) "$(INTDIR)"
	$(F90) $(F90_PROJ) $(SOURCE)


!ENDIF 

SOURCE=..\wlt_3d_main.f90

"$(INTDIR)\wlt_3d_main.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\precision.mod" "$(INTDIR)\util_vars.mod" "$(INTDIR)\misc_vars.mod" "$(INTDIR)\wlt_trns_mod.mod" "$(INTDIR)\share_kry.mod" "$(INTDIR)\pde.mod" "$(INTDIR)\share_consts.mod" "$(INTDIR)\sizes.mod" "$(INTDIR)\user_case.mod" "$(INTDIR)\io_3D.mod" "$(INTDIR)\wlt_trns_util_mod.mod" "$(INTDIR)\kry_mod.mod" "$(INTDIR)\read_init_mod.mod" "$(INTDIR)\util_mod.mod" "$(INTDIR)\elliptic_mod.mod" "$(INTDIR)\time_int_cn_mod.mod" "$(INTDIR)\read_netcdf_mod.mod" "$(INTDIR)\field.mod" "$(INTDIR)\default_mod.mod"
	$(F90) $(F90_PROJ) $(SOURCE)


SOURCE=..\zgetrf.f

"$(INTDIR)\zgetrf.obj" : $(SOURCE) "$(INTDIR)"
	$(F90) $(F90_PROJ) $(SOURCE)


SOURCE=..\zgetri.f

"$(INTDIR)\zgetri.obj" : $(SOURCE) "$(INTDIR)"
	$(F90) $(F90_PROJ) $(SOURCE)



!ENDIF 

