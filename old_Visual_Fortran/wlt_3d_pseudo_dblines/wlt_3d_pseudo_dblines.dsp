# Microsoft Developer Studio Project File - Name="wlt_3d_pseudo_dblines" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Console Application" 0x0103

CFG=wlt_3d_pseudo_dblines - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "wlt_3d_pseudo_dblines.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "wlt_3d_pseudo_dblines.mak" CFG="wlt_3d_pseudo_dblines - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "wlt_3d_pseudo_dblines - Win32 Release" (based on "Win32 (x86) Console Application")
!MESSAGE "wlt_3d_pseudo_dblines - Win32 Debug" (based on "Win32 (x86) Console Application")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
F90=df.exe
RSC=rc.exe

!IF  "$(CFG)" == "wlt_3d_pseudo_dblines - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Target_Dir ""
# ADD BASE F90 /compile_only /include:"Release/" /nologo /warn:nofileopt
# ADD F90 /check:bounds /compile_only /include:"Release/" /nologo /warn:argument_checking /warn:nofileopt
# SUBTRACT F90 /check:noflawed_pentium
# ADD BASE CPP /nologo /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_CONSOLE" /D "_MBCS" /YX /FD /c
# ADD CPP /nologo /MLd /W3 /Gi /GX /Od /D "WIN32" /D "NDEBUG" /D "_CONSOLE" /D "_MBCS" /YX /FD /c
# ADD BASE RSC /l 0x409 /d "NDEBUG"
# ADD RSC /l 0x409 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:console /machine:I386
# ADD LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:console /machine:I386
# SUBTRACT LINK32 /debug

!ELSEIF  "$(CFG)" == "wlt_3d_pseudo_dblines - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE F90 /check:bounds /compile_only /dbglibs /debug:full /include:"Debug/" /nologo /traceback /warn:argument_checking /warn:nofileopt
# ADD F90 /check:bounds /compile_only /dbglibs /debug:partial /define:"_MSC_VER" /fpp /include:"Debug/" /nodefine /nologo /optimize:0 /real_size:32 /traceback /warn:argument_checking /warn:nofileopt
# ADD BASE CPP /nologo /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_CONSOLE" /D "_MBCS" /YX /FD /GZ /c
# ADD CPP /nologo /W3 /Gm /Gi /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_CONSOLE" /D "_MBCS" /YX /FD /GZ /c
# ADD BASE RSC /l 0x409 /d "_DEBUG"
# ADD RSC /l 0x409 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:console /incremental:no /debug /machine:I386 /pdbtype:sept
# ADD LINK32 netcdf.dll kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /stack:0x5f5e100,0x5f5e100 /subsystem:console /profile /debug /machine:I386 /libpath:"c:\dan\research\netcdf\lib"

!ENDIF 

# Begin Target

# Name "wlt_3d_pseudo_dblines - Win32 Release"
# Name "wlt_3d_pseudo_dblines - Win32 Debug"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat;f90;for;f;fpp"
# Begin Source File

SOURCE=..\case_isoturb_timeint_cn.f90
NODEP_F90_CASE_=\
	".\Debug\elliptic_mod.mod"\
	".\Debug\fft_module.mod"\
	".\Debug\field.mod"\
	".\Debug\pde.mod"\
	".\Debug\PRECISION.mod"\
	".\Debug\sgs_models.mod"\
	".\Debug\share_consts.mod"\
	".\Debug\share_kry.mod"\
	".\Debug\share_vars.mod"\
	".\Debug\sizes.mod"\
	".\Debug\spectra_module.mod"\
	".\Debug\util_mod.mod"\
	".\Debug\vector_util_mod.mod"\
	".\Debug\wlt_trns_mod.mod"\
	".\Debug\wlt_trns_util_mod.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\fft\ch_resolution_fs.f90
NODEP_F90_CH_RE=\
	".\Debug\PRECISION.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\d1mach.f
# End Source File
# Begin Source File

SOURCE=..\db_lines.f90
NODEP_F90_DB_LI=\
	".\Debug\pde.mod"\
	".\Debug\PRECISION.mod"\
	".\Debug\share_vars.mod"\
	".\Debug\sizes.mod"\
	".\Debug\user_case_db.mod"\
	".\Debug\wlt_trns_util_mod.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\default_util.f90
NODEP_F90_DEFAU=\
	".\Debug\field.mod"\
	".\Debug\io_3D.mod"\
	".\Debug\pde.mod"\
	".\Debug\PRECISION.mod"\
	".\Debug\share_consts.mod"\
	".\Debug\share_vars.mod"\
	".\Debug\sizes.mod"\
	".\Debug\user_case.mod"\
	".\Debug\user_case_db.mod"\
	".\Debug\wlt_trns_mod.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\derf.f
# End Source File
# Begin Source File

SOURCE=..\derfc.f
# End Source File
# Begin Source File

SOURCE=..\dgamma.f
# End Source File
# Begin Source File

SOURCE=..\dgeev.f
# End Source File
# Begin Source File

SOURCE=..\dgels.f
# End Source File
# Begin Source File

SOURCE=..\dqag.f
# End Source File
# Begin Source File

SOURCE=..\dqage.f
# End Source File
# Begin Source File

SOURCE=..\dtrsm.f
# End Source File
# Begin Source File

SOURCE=..\elliptic_solve_3d.f90
NODEP_F90_ELLIP=\
	".\Debug\pde.mod"\
	".\Debug\PRECISION.mod"\
	".\Debug\share_consts.mod"\
	".\Debug\share_vars.mod"\
	".\Debug\sizes.mod"\
	".\Debug\wlt_trns_mod.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\endienness_small.f90
# End Source File
# Begin Source File

SOURCE=..\fft\fft.f90
NODEP_F90_FFT_F=\
	".\Debug\fft_interface_module.mod"\
	".\Debug\PRECISION.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\fft\fft.interface.temperton.f90
NODEP_F90_FFT_I=\
	".\Debug\PRECISION.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\fft\fft_util.f90
NODEP_F90_FFT_U=\
	".\Debug\fft_module.mod"\
	".\Debug\PRECISION.mod"\
	".\Debug\share_vars.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\fft\fftpacktvms.f
# End Source File
# Begin Source File

SOURCE=..\gaussq.f
# End Source File
# Begin Source File

SOURCE=..\input_files_reader.f90
NODEP_F90_INPUT=\
	".\Debug\PRECISION.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\io_3d.f90
NODEP_F90_IO_3D=\
	".\Debug\endianness_mod.mod"\
	".\Debug\field.mod"\
	".\Debug\input_file_reader.mod"\
	".\Debug\pde.mod"\
	".\Debug\PRECISION.mod"\
	".\Debug\share_consts.mod"\
	".\Debug\share_kry.mod"\
	".\Debug\share_vars.mod"\
	".\Debug\sizes.mod"\
	".\Debug\wlt_trns_mod.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\needblas.f
# End Source File
# Begin Source File

SOURCE=..\r1mach.f
# End Source File
# Begin Source File

SOURCE=..\read_data_wray.f90
NODEP_F90_READ_=\
	".\Debug\fft_module.mod"\
	".\Debug\PRECISION.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\read_init.f90
NODEP_F90_READ_I=\
	".\Debug\ch_resolution.mod"\
	".\Debug\fft_module.mod"\
	".\Debug\pde.mod"\
	".\Debug\PRECISION.mod"\
	".\Debug\read_data_wry.mod"\
	".\Debug\read_netcdf_mod.mod"\
	".\Debug\share_consts.mod"\
	".\Debug\share_vars.mod"\
	".\Debug\spectra_module.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\read_netcdf.f90
DEP_F90_READ_N=\
	"..\netcdf.inc"\
	
# End Source File
# Begin Source File

SOURCE=..\sgs_models.f90
NODEP_F90_SGS_M=\
	".\Debug\pde.mod"\
	".\Debug\PRECISION.mod"\
	".\Debug\share_consts.mod"\
	".\Debug\share_vars.mod"\
	".\Debug\sizes.mod"\
	".\Debug\wlt_trns_mod.mod"\
	".\Debug\wlt_trns_util_mod.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\shared_modules.f90
# End Source File
# Begin Source File

SOURCE=..\fft\spectra.f90
NODEP_F90_SPECT=\
	".\Debug\fft_module.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\time_int_cn.f90
NODEP_F90_TIME_=\
	".\Debug\elliptic_mod.mod"\
	".\Debug\pde.mod"\
	".\Debug\PRECISION.mod"\
	".\Debug\share_consts.mod"\
	".\Debug\share_kry.mod"\
	".\Debug\user_case.mod"\
	".\Debug\util_mod.mod"\
	".\Debug\vector_util_mod.mod"\
	".\Debug\wlt_trns_mod.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\time_int_krylov_3d.f90
NODEP_F90_TIME_I=\
	".\Debug\pde.mod"\
	".\Debug\PRECISION.mod"\
	".\Debug\share_consts.mod"\
	".\Debug\share_kry.mod"\
	".\Debug\sizes.mod"\
	".\Debug\user_case.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\time_integration_meth2.f90
NODEP_F90_TIME_IN=\
	".\Debug\default_mod.mod"\
	".\Debug\elliptic_mod.mod"\
	".\Debug\pde.mod"\
	".\Debug\PRECISION.mod"\
	".\Debug\sgs_models.mod"\
	".\Debug\share_consts.mod"\
	".\Debug\share_vars.mod"\
	".\Debug\time_int_meth2_aux_mod.mod"\
	".\Debug\user_case.mod"\
	".\Debug\util_mod.mod"\
	".\Debug\vector_util_mod.mod"\
	".\Debug\wlt_trns_mod.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\time_integration_meth2_aux.f90
NODEP_F90_TIME_INT=\
	".\Debug\elliptic_mod.mod"\
	".\Debug\pde.mod"\
	".\Debug\PRECISION.mod"\
	".\Debug\sgs_models.mod"\
	".\Debug\share_vars.mod"\
	".\Debug\sizes.mod"\
	".\Debug\vector_util_mod.mod"\
	".\Debug\wlt_trns_mod.mod"\
	".\Debug\wlt_trns_util_mod.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\user_case_db_defs.f90
# End Source File
# Begin Source File

SOURCE=..\user_elliptic_IC.f90
NODEP_F90_USER_=\
	".\Debug\pde.mod"\
	".\Debug\PRECISION.mod"\
	".\Debug\share_vars.mod"\
	".\Debug\sizes.mod"\
	".\Debug\wlt_trns_mod.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\util_3d.f90
NODEP_F90_UTIL_=\
	".\Debug\pde.mod"\
	".\Debug\PRECISION.mod"\
	".\Debug\share_consts.mod"\
	".\Debug\share_vars.mod"\
	".\Debug\sizes.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\vector_util.f90
NODEP_F90_VECTO=\
	".\Debug\pde.mod"\
	".\Debug\PRECISION.mod"\
	".\Debug\share_vars.mod"\
	".\Debug\wlt_trns_mod.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\wavelet_3d.f90
NODEP_F90_WAVEL=\
	".\Debug\PRECISION.mod"\
	".\Debug\share_vars.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\wavelet_3d_lines.f90
NODEP_F90_WAVELE=\
	".\Debug\db_mod.mod"\
	".\Debug\field.mod"\
	".\Debug\pde.mod"\
	".\Debug\PRECISION.mod"\
	".\Debug\share_consts.mod"\
	".\Debug\share_vars.mod"\
	".\Debug\sizes.mod"\
	".\Debug\wlt_trns_util_mod.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\wlt_3d_main.f90
NODEP_F90_WLT_3=\
	".\Debug\default_mod.mod"\
	".\Debug\elliptic_mod.mod"\
	".\Debug\field.mod"\
	".\Debug\io_3D.mod"\
	".\Debug\kry_mod.mod"\
	".\Debug\pde.mod"\
	".\Debug\PRECISION.mod"\
	".\Debug\read_init_mod.mod"\
	".\Debug\read_netcdf_mod.mod"\
	".\Debug\sgs_models.mod"\
	".\Debug\share_consts.mod"\
	".\Debug\share_kry.mod"\
	".\Debug\share_vars.mod"\
	".\Debug\sizes.mod"\
	".\Debug\time_int_cn_mod.mod"\
	".\Debug\time_int_meth2_aux_mod.mod"\
	".\Debug\time_int_meth2_mod.mod"\
	".\Debug\user_case.mod"\
	".\Debug\user_elliptic_IC_mod.mod"\
	".\Debug\util_mod.mod"\
	".\Debug\wlt_trns_mod.mod"\
	".\Debug\wlt_trns_util_mod.mod"\
	
# End Source File
# Begin Source File

SOURCE=..\zgetrf.f
# End Source File
# Begin Source File

SOURCE=..\zgetri.f
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl;fi;fd"
# End Group
# Begin Group "Resource Files"

# PROP Default_Filter "ico;cur;bmp;dlg;rc2;rct;bin;rgs;gif;jpg;jpeg;jpe"
# End Group
# End Target
# End Project
