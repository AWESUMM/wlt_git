
!* CVS Log
!* $Log: wlt_3d_main.f90,v $
!* Revision 1.11  2006/04/18 22:23:30  dan
!* fixed writing of _log file
!*
!* Revision 1.10  2006/04/12 23:55:57  dan
!* Added new program compare_results for testing
!*
!* Revision 1.9  2006/04/11 23:40:56  dan
!* Dan G.
!*
!* Revision 1.8  2006/03/20 20:20:18  dan
!* Fixed some write statements with a comma before the first argument, (IBM compiler didn't like this)
!*
!* Revision 1.7  2006/02/21 22:25:32  dan
!* Add writing file that track number of wavelets per level
!* Change some PRINT to WRITE to facilitate debugging
!*
!* Revision 1.6  2006/01/04 20:36:56  dan
!* Added argument t (time ) to calls to
!* user_additional_vars(t, 0 )
!*
!* Revision 1.5  2005/12/20 16:06:39  dan
!* *** empty log message ***
!*
!* Revision 1.4  2005/11/15 23:35:09  dan
!* working on optimizinf db_lines This version works so far..
!*
!* Revision 1.3  2005/11/05 21:11:32  dan
!* working on optimizinf db_lines This version works so far..
!*
!* Revision 1.2  2005/10/29 22:52:34  dan
!* *** empty log message ***
!*
!* Revision 1.1  2005/10/26 16:54:35  dan
!* New head for wavelet code with both tree and lines dbs
!*
!* Revision 1.38  2005/07/08 02:05:35  dan
!* -- inputs,
!*    I now read inputs from read_input() in io.f90 and then from this read call user_read_input() in case file.
!*    The input file name is now opened exactly as it is given on the command line (no _wlt.inp added )
!*    I deleted the unused routine user_read_geometry()
!*
!* -- added global scaleCoeff that is set in user_setup_pde (or by default it is setup as scaleCoeff = 1.0 in main)
!*    so scl = scaleCoeff * scl
!*    If scaleCoeff < 1 then more points are added in adapting for a particular variable
!*
!* -- fixed logical error that caused scales to not be called for the first initial adaptation step
!* --   needed to call weights  before scales on restart
!* -- I made a subroutine set_weights in util_3d.f90 to call weights and print diagnostics.
!* -- added code in read_solution to resart with n_var > n_var in the restart file (WITH A WARNING MESSAGE)
!*     This is usefull int he case of adding an additonal variable
!*
!* Revision 1.37  2005/06/15 22:33:59  dan
!* --Added post_process/convert_output  and manual and results (empty) directories
!*
!* Revision 1.36  2005/06/13 16:10:03  dan
!* -- FIxed bugs in passing arrays that showed up with two extra scalar equations being used.
!*
!* Revision 1.35  2005/06/06 16:49:54  dan
!* -- merging in Giuliano's local dynamic model case.
!*
!* Revision 1.34  2005/06/02 20:24:49  dan
!* -- in main,  changed it to calculate grid and test mask if sgsmodel >= 1
!*    was >1 (this is safer because const Cs needs filter in some cases..)
!*
!*      !
!*      ! Setup up any model filter masks needed
!*      !
!*      IF(  sgsmodel >= 1 .OR. ExplicitFilter .OR. ExplicitFilterNL ) THEN
!*
!* -- moved sgs_model routines into the case file. The routine  user_sgs_force()
!*    is called at the beginning of the time step from time_adv_cn().
!*
!* -- moved usitlity routines
!*
!* -- main, for restart, initial inverse transform now done in adapt_grid.
!*    Also u_old wasn't allocated correctly for restart in wrk version.
!*    I fixed this.
!*    BUT there is still a problem with restart for the case_isoturb..?
!*
!* -- wavelet_3d_wrk.f90
!*    changed adapt_grid() for correct restart. Now all of routine is
!*    used in restart.
!*
!* -- moved sij() to vector_util.f90
!*
!* -- moved wlt_filt_wmask(), make_mdl_filts(),mdl_filt() to util_3d.f90
!*
!* -- added global logical  first_loop_after_restart which is set true for the
!*    first loop after restart. I thought this might be needed for solver on
!*    restart. But not sure yet.
!*
!* -- added routine reverse_endian.f used in reversing byte ordering.
!*    This is currently not used yet.
!*
!* Revision 1.33  2005/05/25 15:42:28  dan
!* -- wlt_3d_main.f90
!*    In initial adaptation loop now j_mn_init is used for the first
!*    adaptation. THen for subsequent adapations to get the iniatial grid
!*    j_mn is used.
!*   changed print statement of test - filt grid to filt grid - test grid
!*
!* -- sgs_models.f90
!*     changed print statement of test - filt grid to filt grid - test grid
!*
!* -- updated makefile.globals to correctly compile isoturb statistics program
!*
!*  turb_stats.f90 fixed line that was too long for absoft compiler
!*
!* Revision 1.32  2005/05/24 20:35:27  dan
!* -- rewrote special versionof get_active_line (called get_active_line_flags() )
!*   that is called from db_reconstruct_check.
!*
!* Revision 1.31  2005/05/20 15:21:48  dan
!* -partially implemented  little-big endian swapping in read_init.f90
!*
!* -fixed bug in wavelet_3d_lines.f90 and db_lines_V2_frontend.f90
!* new array lines_coords was not deallocated in some routines.
!*
!* -default_util.f90 Check exeact solution now prints relative error
!*  (This is not good when exact soln is near zero (say 1e-15) and soln is
!*   1e-8. THe relative error is then huge. Need to fix this...
!*
!* -update_u_from_db changed args  INTENT(INOUT) :: nwlt , ie
!* to  INTENT(IN) :: nwlt , ie
!*
!* -db_lines_V2_backend.f90  wrote new delete_some_backend()
!*   nullified all pointers in type declarations
!*
!* Revision 1.30  2005/05/10 16:28:26  dan
!* db_reconstruction check. Added if to cut down on points being looked at multiple times
!*
!* Revision 1.29  2005/04/26 18:09:33  dan
!* fixed new adjacent code for db_lines. Now cons and non-cons work!
!*
!* Revision 1.28  2005/04/22 21:24:13  dan
!* fixed restart for dblines, must test for wrk and dblines_pseudoDB
!*
!* Revision 1.27  2005/04/19 22:23:48  dan
!* -added input variable t_adapt used in time integration cn method.
!*  if t > t_adapt then adaptive time step is used...
!*
!* Revision 1.26  2005/04/18 18:37:41  dan
!* - changed all calls to get_active_lines_indx(), internally I did <= j+1 so I changed
!* it to <= j. THen I changed calls that needed changing to get_active_lines_indx(). Now it is
!* consistent and it doesn't get extra unused lines anymore.
!*
!* - db_update_grid moved from wavelet_3d_lines.f90 to db_lines.f90 and db_linesV2_frontend.f90
!*   It is now DB specific...
!*
!* Revision 1.25  2005/04/10 13:08:36  dan
!* - wavelet_3d_wrk.f90, indices()
!*
!*    !DG I BELEIVE THIS IS A BUG 04/08/2005	   IF( dim ==3) ixyz(dim) = 0
!*    ! I beleive this should be:
!*        IF( jd == 0 ) ixyz(dim) = 0  ! THIS DOES NEED TO BE CLEANED UP!!
!*
!* - wavelet_3d_lines.f90
!*    moved back to old adjacent_wlt()
!*    Old routine works for non-cons. New routine still not working.
!*    SHould match for adj_type 011, needs more testing.
!*
!* Revision 1.24  2005/03/30 22:14:39  dan
!* -Updated how error from exact solution is printed in main()
!* -MOdified wavelety_3d_wrk.f90 to compile and run with new versions of
!* deriviatives. The code compiles and adapts but the time integration has a
!* bug in the working array version
!*
!* -wavelet_3d_lines.f90
!*   -added a list for deleted points to get/put_active lines
!*   -changed to zero out line_u and line_flags at top of routines instead
!*    of in lower level get_active_line
!*   - in process of modifiying adjacent zone code to work with conservative
!*     adjacent zone
!*
!* - fixed type on makefile (gmake was added twice on the line in two places..)
!*
!* - made prettier printing in check_exect_soln() in default_util.f90
!*
!* - db_lines.f90
!*    -added a list for deleted points to get/put_active lines
!*
!* - case_vortex_timeint_cn.f90 updated to work correctly with tine_int_cn
!*   time integration
!*
!* - case_isoturb_timeint_cn.f90 added code to do derivatives differently
!*   for TYPE_WRK vs TYPE_LINES data bases
!*
!* - added source files db_lines_V2_frontend.f90 and
!*    db_lines_V2_backend.f90 This will be the new real lines db
!*    The backend module is written by Alexei  Vezolainen
!*
!* Revision 1.23  2005/03/15 22:11:39  dan
!* fix c_wlt_trns() in wavelet_3d_lines.f90 (It was nto putting data in db first
!*
!* Revision 1.22  2005/03/10 17:06:45  dan
!* Zero out all variables in u that are not true in n_var_adapt(:,0) or n_var_interpolate(:,0)  before
!* main integration loop.
!* This way we only do initial adaption correctlyon n_var_adapt(:,0) and n_var_interpolate(:,0)
!* variables.
!*
!* Revision 1.21  2005/03/08 22:49:18  dan
!* modifications so only n_var_interpolate and n_var_adapt and n_var_saved variables are fwn transformed
!* in main loop and then in adapt_grid only
!* n_var_interpolate variables are inverse transformed.
!* Updated c_wlt_trns_mask so it calles update_db_from_u_mask().
!*
!* Revision 1.20  2005/03/08 15:57:47  dan
!* *** empty log message ***
!*
!* Revision 1.19  2005/02/25 17:40:45  dan
!* -- Changes merged from Oleg, 2/24/2005
!*
!*  - case_vortex_timeint_cn.f90 updated to better variable names, ne-> nelocal ...
!*
!*
!* - wlt_3d_main.f90
!*    made calculation on area general for any dim
!*
!* Revision 1.18  2005/02/14 18:16:40  dan
!* Added inpuit flag Zero_Mean
!*  if true a zero mean is imposed for first three variables (currently velocity).
!* This is only applied to time_int_cn method for now.
!*
!* Fixed error in interpolate that didn't zero out db for levels j_in to j_out if
!* j_in < j_out . This only effects db_lines version
!*
!* Added diff_aux2_db to do derivative in case j==j_in from c_diff_fast (db_lines version)
!*
!* Revision 1.17  2005/02/02 16:18:16  dan
!* working on new wavelet_lines weights() routine
!* I had to rename nwtl to nwlt_in in many routines becasue it confliced with
!* global nwlt. This needs to be changed to just use the global version (from shar_vars)
!*
!* Revision 1.16  2005/01/25 19:56:10  dan
!* additional variables calculated in user_additional_vars() in real space!
!* They are then transformed at the top of the main time integration  loop if needed.
!*
!* Revision 1.15  2005/01/25 18:58:44  dan
!*  fixed small typo in c_wlt_inter.f90
!*  updated m files to be unix/windows independent
!*
!*  fix error that caused additional variables not to be calculated
!*   and transformed into wlt space correctly when they are saved (for
!*   example vorticity )
!*
!* Revision 1.14  2005/01/13 21:22:14  dan
!* putting in  new crank nicholson time integration routine.
!*  -used new version of krlov module from Nicholas
!*  -went to new modules and did away with INTERFACE blocks
!*  -Laplace and Laplace_diag are now in case file (is this correct??)
!*
!*  Added scale method with correct L2 norm based on dA
!*  IF( Scale_Meth == 3 ) THEN ! Use L2 scale using dA
!* 			   scl(ie)= SQRT ( SUM( (u(1:nwlt,ie_index)**2)*dA )/ sumdA  )
!*
!*
!*  Added logging of error with exeact solution in file: "run_name".err.exact.soln
!*
!* Revision 1.13  2004/12/10 18:02:19  dan
!* vortex array test case works for db lines and wkr arrays now!!
!*
!* Revision 1.12  2004/12/10 01:26:43  dan
!* working on lines db routines
!*
!* Revision 1.11  2004/11/29 23:50:40  dan
!* added alexei's input file reading routine. It currently does not
!* de-allocate its memory though!!
!*
!* I also commented otu the last ifn's in the code.
!* I added the global variable time_integration_method
!* that is read in to set which time integration method is used.. DG.
!*
!* Revision 1.10  2004/11/19 00:50:34  dan
!* moving routines
!* made rhs and Drhs as arguments (pass the function pointer)
!*  to krylov time step.
!*
!* Revision 1.9  2004/11/18 22:41:30  dan
!* moving routines
!* created default_mod for routines that have a user version
!*
!* Revision 1.8  2004/11/18 17:35:21  dan
!* -made u_ex only allocated if n_var_exeact > 0
!*
!* moving routines into user cases
!*
!* Revision 1.7  2004/11/17 11:14:10  dan
!* Combined adapt_grid
!* and post_adapt_grid
!*
!* Revision 1.6  2004/11/17 01:14:19  dan
!* update_timesteps_maps changed to one call to
!* map_veloc_timesteps that only updated n_var_index
!*
!* Added routines
!* user_additional_vars() user_scalar_vars()
!* ALso now the vorticity is calculated (on a case specific basis)
!* in user_additional_vars()
!*
!* Revision 1.5  2004/11/16 06:48:55  dan
!* Updated mapping arrays.
!* Added n_var_interpolate for variables that are interpolated to new grid.
!*
!* Revision 1.4  2004/11/16 01:03:57  dan
!* Moving around calls to clean up main.
!*
!* Revision 1.3  2004/11/13 21:02:17  dan
!* Combined calls to map_veloc_timesteps into new routine update_timesteps_maps()
!* in module time_int_meth2_mod
!*
!* Combined init_DB1 and init_DB2 into one routine init_DB
!* Renamed internal routine module db_mod init_db() -> init_lines_db()
!*
!* Revision 1.1  2004/11/11 16:51:35  dan
!* Version 2.
!* This version is the starting point for merging Dan's changes with those of Oleg.
!* This verison will do multiple data structures
!* Dan G.
!*

