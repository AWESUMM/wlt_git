\documentclass{article}
\usepackage[utf8]{inputenc}
\usepackage{amsmath,amssymb,amsthm}
\usepackage{fullpage}

\title{\texttt{user\_case} functions}
\date{\today}
\author{Oleg Rogozin, Oleg Vasilyev}

%%% General commands
\newcommand{\pder}[2][]{\frac{\partial#1}{\partial#2}}      % partial derivative
\newcommand{\dder}[2][]{\Delta_{#2}#1}                      % discrete derivative
\newcommand{\vder}[2][]{\frac{\delta#1}{\delta#2}}          % variational derivative
\newcommand{\pderdual}[2][]{\frac{\partial^2#1}{\partial#2^2}}
\newcommand{\dderdual}[2][]{\Delta^2_{#2^2}#1}
%\newcommand{\pder}[2][]{\partial_{#2}{#1}}
%\newcommand{\pderdual}[2][]{\Delta{#1}}
\newcommand{\LL}{\mathcal{L}}
\newcommand{\FF}{\mathcal{F}}
\newcommand{\bx}{\mathbf x}
\newcommand{\diag}[1]{\left(#1\right)^\mathrm{diag}}
\newcommand{\pert}[1]{\delta#1}
\newcommand{\Set}[2]{\{\,{#1}:{#2}\,\}}
\newcommand{\eqdef}{\mathrel{\overset{\makebox[0pt]{\mbox{\normalfont\tiny\sffamily def}}}{=}}}

%%% Special commands
\newcommand{\matr}[4]{\begin{pmatrix} #1 & #2\\#3 & #4\end{pmatrix}}
\newcommand{\vect}[2]{\begin{pmatrix} #1\\#2\end{pmatrix}}
\newcommand{\func}[4]{\texttt{user\_#1\(_{#3}\)(\(#2\))}\(\eqdef\displaystyle#4\)}

\theoremstyle{definition}
\newtheorem{example}{Example}
\newtheorem{remark}{Remark}
\newtheorem{exercise}{Exercise}

\begin{document}

\maketitle

Consider a Cauchy problem for the set of equations:
\begin{equation}\label{eq:problem}
    \pder[u_i]{t} = \FF_i, \quad
    u_i(t,\bx): \mathbb{R}_+\times\mathbb{R}^d\mapsto\mathbb{R}^N \quad (i=1,\dots,N),
\end{equation}
where \(\Set{\FF_i}{i=1,\dots,N}\) is the set of differential operators (nonlinear in general).
\(\FF_i\) is called the \emph{RHS} (\emph{right hand side}).
\(\pert{\FF_i}\) is the \emph{perturbation} of the RHS.
\begin{example}
Perturbation can be expressed in terms of functional derivative:
\begin{itemize}
    \item if \(\FF_i\) is linear, i.e., \(\FF_i = \LL_{ij}u_j\), then
        \(\pert{\FF_i} = \LL_{ij}\pert{u}_j\);
    \item if \(\FF_i = \LL_{ij}(u_k)u_j\), then
        \(\pert{\FF_i} = \left( \pder[\LL_{ik}]{u_j}u_k + \LL_{ij} \right)\pert{u}_j\);
    \item if \(\FF = \FF(u, \pder[u]{\bx})\), then
        \(\pert{\FF} =  \pder[\FF]{u} \pert{u} + \pder[\FF]{\left(\pder[u]{\bx}\right)} \pder[\pert{u}]{\bx}  \).
\end{itemize}
\end{example}
Next, we introduce the discretization of the \(\bx\) space with the set of points \(\Set{\bx^n}{n\in\mathbb{Z}^d}\).
Let \(u_j^n\) and \(\FF_i^n\) be approximations of the solution and RHS, respectively, at the point \(\bx^n\).
The \emph{discrete RHS} can be represented as a matrix product
\begin{equation}\label{eq:matrix_representation}
       \FF_i^n = A_{ij}^{nm}u_j^m,
\end{equation}
then
\begin{equation}\label{eq:diagonal_term}
     \diag{\FF_i^n} \eqdef \left( A_{ij}^{nm} \right)_{j=i}^{m=n}
\end{equation}
is the \emph{diagonal term} of the discrete operator.
Let \(\dder{x_i}\) denotes the discrete approximation of the partial derivative \(\pder{x_i}\).
\begin{example}
    For the forward difference approximation
    \[ \dder[u^n]{x} = \frac{u^n-u^{n-1}}{h}, \quad \diag{\dder[u^n]{x}} = \frac1h. \]
    For the central difference approximation
    \[ \dderdual[u^n]{x} = \frac{u^{n-1}-2u^n-u^{n+1}}{h}, \quad \diag{\dder[u^n]{x}} = -\frac2h. \]
\end{example}

In general, we need to calculate the following functions:
\begin{itemize}
    \item \func{rhs}{u_j^n}{i}{ \FF_i^n },
    \item \func{Drhs}{\pert{u_j^n},u_k^n}{i}{ \pert{\FF_i^n} },
    \item \func{Drhs\_diag}{u_k^n}{i}{ \diag{\vder[\FF_i^n]{u_j}}_{j=i}}.
\end{itemize}
Hereafter, we omit all superscript indices that correspond to the physical space.
\begin{example}
    For \(\FF = u\), they are \[
        u, \quad \pert{u}, \quad 1.
    \]
\end{example}
\begin{example}
    For \(\FF = \pderdual[u]{x_i}\), they are \[
        \dderdual[u]{x_i}, \quad \dderdual[\pert{u}]{x_i}, \quad \diag{\dderdual{x_i}}.
    \]
\end{example}
\begin{example}
    For \(\FF = u\pder[u]{x}\), they are \[
        u\dder[u]{x}, \quad \pert{u}\dder[u]{x} + u\dder[\pert{u}]{x},
        \quad \dder[u]{x} + u \diag{\dder{x}}.
    \]
\end{example}
\begin{example}
    For \(\FF = \frac12\pder[u^2]{x}\), they are \[
        \frac12\dder[u^2]{x}, \quad \dder[u\pert{u}]{x}, \quad u\diag{\dder{x}}.
    \]
\end{example}
\begin{remark}
    When we calculate diagonal term of the perturbation,
    differential operators should be considered as matrix operations.
    For example, \(\diag{\dder{x}} f(u) = f(u)\diag{\dder{x}}\).
\end{remark}
\begin{example}
    For \(\FF = \frac12\left(\pder[u]{x}\right)^2\), they are \[
        \frac12\left(\dder[u]{x}\right)^2, \quad \dder[u]{x}\dder[\pert{u}]{x},
        \quad \dder[u]{x}\diag{\dder{x}}.
    \]
\end{example}
\begin{example}
    For \(\FF_i = \matr1102\vect{u_1}{u_2}\), they are \[
        \vect{u_1+u_2}{2u_2}, \quad
        \vect{\pert{u_1}+\pert{u_2}}{2\pert{u_2}}, \quad
        \vect{1}{2}.
    \]
\end{example}

\begin{exercise}
    Calculate the desired functions for \[
        \pder[u]{t} = u\left(\pder[u]{x} + \pder[v]{x}\right), \quad
        \pder[v]{t} = \pder{x}(vu) + \nu\left(\pderdual[u]{x}+\pderdual[v]{y}\right).
    \]
\end{exercise}

\begin{exercise}
    Calculate the desired functions for \[
        \pder[u]{t} = \left(\pder[u^2]{x} + \pder[uv]{y}\right), \quad
        \pder[v]{t} = \pder{x}(uv) + v \pder[v]{y} + \nu\left(\pderdual[u]{x}+\pderdual[v]{y}\right).
    \]
\end{exercise}
\end{document}
