\clearpage
\section{Parallel Processing}
\label{subsec:parallel}

Adaptive wavelet based code as well as res2vis visualization tool
can be run on several processors using MPI library.
Input/output ideology is the following: if we are using $P$ processors
we expect to read single .inp file and, if necessary, $P$ .res files.
The code running on $P$ processors will normally produce $P$ output files
(.res, .vtk, or others) and a single \_log or \_user\_stats file.
An extension will be added to output files
to indicate the processor number that file has been written by
(p0, p1, p2, etc).

User can restart the adaptive wavelet based code on a different number of processors.
The name of the restart file, \verb|IC_filename|, which is an exact name of the 
restart file in a single processor run, in a multiprocessor run may
point to any of the available result files.
Since the number of processors is stored inside each result file,
the code will figure out which files to read during the restart
and will remap the data into the new number of subdomains.


In addition, a standalone \verb|n2m| tool exists to transform
input/output files from a given number of processors to another.

\subsection{n2m Transformation Tool}
This tool has been designed to transform .res
input/output files from a given number of processors to another,
for example, the data stored in result files case.001.p0.res, case.001.p1.res,
case.001.p2.res, case.001.p3.res (created by wavelet based code on four processors)
could be rearranged by \verb|n2m| tool into a single .res file as well as into
any required number of .res files.
Type \verb|./n2m| for a complete manual.
If required, the tool can run on several processors.
It should be noted that it is up to the user to make sure the system has enough
memory to handle files transformation, e.g. in case of transforming
large number of files into a single .res file.

If adaptive wavelet based code is compiled in parallel mode
(with MP defined, e.g. MP=1) with the compilation target \verb|all|,
this tool will be also created and can be found in the case directory
together with the wavelet based code. To compile the tool alone,
provide the target \verb|n2m| and set parallel compilation mode.

Note, in parallel runs user has to make sure that the number of processors
 is equal to the number of output files. Otherwise, the code is erroneous.
 Each processor expects to write single output file in parallel run.
In short, single processor \verb|n2m| transforms $N$ .res files to $M$ .res files,
where $N,M \geq 1$. Parallel run on $P$ processors of the toot transforms $N$
.res files to $P$ .res files, where $N\geq 1$, and $P > 1$.
Domain decomposition for the output files could be chosen from
the available in the Table~\ref{domain_meth_table}.

\subsubsection{Complete manual of n2m tool}

\begin{verbatim}
Usage: n2m [OPTIONS] IN_FILE OUT_FILE
       [mpirun -np [N_PROC]] n2m -o [N_PROC] ...
Convert .res file(s) for a different domain decomposition.

Options:
  IN_FILE             input filename
  -o OUT_NUM          output number of files
  OUT_FILE            base output filename (without .res extension)

  -h --help -?        show this message
  -d METH             output domain decomposition method (default 0)
  -v VERB             read/save_solution verbosity level (default 0)

Examples:
  n2m -o 3 results/case.000.res out.000
    produce files out.000.p0.res, out.000.p1.res, out.000.p2.res
    from single input file results/case.000.res

  n2m -o 3 results/case.000.p0.res out.000
    produce files out.000.p0.res, out.000.p1.res, out.000.p2.res
    from input files results/case.000.p0.res, ...p1.res, etc.

Warnings:
 In parallel runs, make sure that the number of processors is equal
 to the number of output files. Otherwise, the code is erroneous.
 Each processor expects to write single output file in parallel run.

\end{verbatim}


\subsection{Compilation, Execution and Visualization}
\label{subsec:p:compile}
Compilation of the parallel version of the code is quite similar to that of
the single processor (\ref{subsec:compile}). First of all, MPI library has
to be installed and the corresponding paths are to be defined in machine
specific makefiles. An example could be found in makefile.ifort\_icc.32 file.
The second issue is also related to MPI library. Depending on the
settings during MPI library compilation underscores may be appended to
MPI functions names - use \verb|nm| to figure out what exactly is happening
and set MPIFLAGS respectively. Examine the file parallel.f90 for
possible values for MPIFLAGS: use flag -DDATABASE\_MPI\_APPEND\_UNDERSCORE
in order to append an underscore to MPI functions
(e.g. for the use with pgi compilers) or flag -DDATABASE\_MPI\_APPEND\_UNDERSCORE2
in order to append second underscore to MPI functions.

To compile parallel version of the code, follow the single processor
compilation procedure (\ref{subsec:compile}). The only database to be used in
parallel mode is \verb|db_tree|. An optional parameter \verb|MP|
has to be defined (to be nonzero), e.g.
\begin{tabbing}
  \hspace{12pt} \= gmake CASE=\dots DB=db\_tree MP=1 [target],
\end{tabbing}
where \verb|[target]| can be \verb|wlt_3d| or \verb|vis| to compile
parallel versions of wavelet based code or res2vis visualization tool.
Not providing parameter \verb|MP| will result in a
single processor code compilation.
Similarly, a sequential code will be produced for any database except
\verb|db_tree|.

To execute parallel version of the code, use \verb|mpirun| script provided
by MPI library, e.g.
\begin{tabbing}
  \hspace{12pt} \= mpirun -np N ./c\_wlt\_3d.out input.inp,
\end{tabbing}
where \verb|N| is the number of processors to use.

In order to visualize parallel data the visualization tool res2vis (\ref{subsec:res2vis})
has to be compiled (similarly to the main code compilation, use \verb|db_tree|
database and define \verb|MP|)
and executed in parallel (by using \verb|mpirun| script). Check res2vis complete
manual for details (\ref{subsec:res2vis}).

If the data from all the processors can fit into the memory for a single processor run,
the MATLAB script \verb|showme3D.m| could also be used to visualize the data.
Check the script manual for details (\ref{subsec:matlab}).



\subsection{Parallel Code Structure}
For simplicity, we keep all the parallel related subroutines in parallel.f90 file.
Throughout the code, parallel related things are surrounded either by
\verb|IF(par_size.GT.1)| statement, where \verb|par_size| is a global variable -
the total number of processors, or by preprocessor statement \verb|#ifdef MULTIPROC|.



%
% ----------------------------------------------------------------------------------------
%
\subsection{Domain Partitioning}
In a parallel environment the domain is broken up into parts and
each part's computations are done by a single processor.
Refer to the Table~\ref{domain_meth_table} for a
complete list of the available domain decomposition methods.
User can control domain partitioning via the input parameter file.
Different partitioning approaches use different user provided variables
(e.g. to control domain splitting along predefined axis directions) and
are described in this section.
Figure~\ref{partitioning} presents examples of some of the implemented decompositions.
\begin{figure}[htbp]
        \centering
        (a)\includegraphics[width=1.1in]{fig/b_prime-lowres.pdf}
        (b)\includegraphics[width=1.1in]{fig/b_pow-lowres.pdf}
        (c)\includegraphics[width=1.1in]{fig/b_zol_geo-lowres.pdf}
        (d)\includegraphics[width=1.1in]{fig/b_zol_hyp-lowres.pdf}
        \caption[Domain partitioning]{Partitioning: (a) geometric simultaneous,
        (b) geometric sequential, (c) Zoltan geometric, (d) Zoltan hyper-graph.}
        \label{partitioning}
\end{figure}


Geometric domain decompositions are based on the number
of active wavelets inside each of the sub-domains
(active wavelets are the ones above the threshold and their nearest neighbors
added to account for evolution of the solution; in code: significant and adjacent).
For the  geometric simultaneous partitioning we cut all the spacial directions
of the domain simultaneously (Fig.~\ref{partitioning}a).
If the total number of processors $P$ is represented as a product
\[ P = \prod_{i=1}^{d'} P_i \; , \;\;  d' \le d,\]
where $d$ is the dimension of the computational domain,
we cut each dimension of the domain according to the correspondent $P_i$ thus resulting
in $P$ rectangular subdomains. The partitioning gives nearly perfect load balancing for
uniformly distributed wavelets (and for an appropriate number of processors $P$).
For the given example, minimum number of wavelets per processor
$\min N_p$ is  $60$ and $\max N_p = 77$. The major deficiency of that approach is obviously
poor, and complicated, load balancing for a non-uniform wavelet distribution.
Different partitioning approaches might need to be used in such cases.

For the geometric sequential (Fig.~\ref{partitioning}b) partitioning,
a rectangular $d$-dimensional computational domain is subdivided
perpendicular to the first axis on rounded to the nearest integer
$\sqrt[d]{P}$ sub-domains, where $d$ is the problem dimension and $P$
is the total number of processors.
The available $P$ processors are distributed among these sub-domain according
to the number of active wavelets inside each of the sub-domains.
Then, the first step is repeated, i.e. each of the previously obtained
sub-domains is subdivided perpendicular to the second axis on rounded
to the nearest integer $\sqrt[d-1]{P'}$ number of sub-sub-domains,
where $P'$ is the number of processors previously assigned to that
particular sub-domain. 
This recursion step is repeated $d$ times to get the final partitioning.
An example of the geometric sequential partitioning of 2-D square domain between
different numbers of processors is presented at Fig.~\ref{domdec}.
\begin{figure}[htbp]
        \centering
        \includegraphics[width=5in]{fig/domdec_ln.pdf}
        \caption[]{Decomposition of $8\times 8$ domain of trees
		for different number of processors $N$. First subdivision occurs
		perpendicular to the vertical axis on $\sqrt{N}$ sub-domains.}
	\label{domdec}
\end{figure}

User can provide an optional decomposition control vector through input (.inp) file, e.g.
\begin{tabbing}
	\hspace{12pt} \= \verb|domain_split = 0,1,0|
\end{tabbing}
where zeroes are used to prevent domain subdivision in these
directions. For example, the above mentioned vector (0,1,0) will force domain
decomposition into slabs perpendicular to y-axis. Vector of zeroes prohibits
domain subdivision in all the directions, which seems to be strange for
a parallel application; therefore it is ignored by the code (reset to unities).

Through the optional input parameter vector \verb|domain_div| user can
define the exact number
of domain subdivisions in each directions, through natural number element.
A zero element of the vector makes the code to apply the described above $\sqrt[d]{N}$ rule
to compute the number of subdivisions in that direction.

Geometric sequential partitioning may deliver not quite an optimal load balancing,
though it may be more usable for non-uniform wavelet distributions across the domain.
For the example at Figure~\ref{partitioning}b $\min N_p = 42$ and $\max N_p = 91$.

Different approach may be used for a significantly non-uniform wavelet distribution.
The code is currently using Zoltan partitioning library by Sandia National Laboratories
\cite{ZoltanOverviewArticle},
\cite{ZoltanHypergraphIPDPS06},
\cite{ZoltanParHypRepart07}.
Zoltan geometric (Recursive Coordinate Bisection) partitioning gives
$\min N_p = 60$ and $\max N_p = 78$ (Fig, \ref{partitioning}c).
Zoltan parallel hyper-graph partitioner results in
$\min N_p = 63$ and $\max N_p = 68$ (Fig, \ref{partitioning}d).
Both Zoltan library based partitioners normally produce non-rectangular subdomains.
This may lead to a larger subdomain surface area. More wavelets will have to be
placed into the subdomain boundary zone and therefore communications between
the subdomains will be slower. It is possible though to request rectangular
subdomains in geometric (RCB) partitioning by setting internal Zoltan parameter
"RCB\_RECTILINEAR\_BLOCKS" to "1" in the file C++/zoltan\_partitioning.cxx
of the code distribution. More Zoltan parameters are available. Check the library
documentation (e.g. \cite{ZoltanHomePage}).


It should be noted that in order to use Zoltan library the code has to be compiled
with it. Set the required include and library paths, ZOLTANINC and ZOLTANLIB,
in your machine specific makefile and define SUPPORT\_ZOLTAN=YES in order to use
Zoltan library for domain partitioning.


Additional user control parameters are available.
Setting the optional logical parameter \verb|domain_debug = T| will
currently force the program to terminate after domain decomposition.
The current version reads from the terminal the number of processors and the periodicity
vector, which overwrite they values from the input file and MPI startup script.
For example, executing the program on a single processor as
\begin{tabbing}
	\hspace{12pt} \= \verb|echo -e "128 \n 1 1 1"| $\vert$ 
			\verb| ./wlt_3d.out input.inp|
\end{tabbing}
will output the domain decomposition for 128 processors. The periodicity vector
from the input file will be overwritten by the values \verb|1 1 1|.
The number of input parameters of debugging mode of the domain decomposition may change;
examine \verb|parallel_domain_decompose| subroutine in the module \verb|parallel|
in parallel.f90.



Finally,
an optional input parameter \verb|domain_meth| to be used to choose a domain
decomposition method. The default \verb|domain_meth| value is zero.
Current settings are given in the Table~\ref{domain_meth_table}.
\begin{table}[htbp]
\center
\begin{tabular}{ c l }
	\hline
	domain\_meth	&	decomposition is based on \dots	\\
	\hline
	0	& (default) geometric simultaneous \\
	1	& geometric sequential (recursive $\sqrt[d]{N}$) \\
	2	& Zoltan library,  Geometric\\
	3	& Zoltan library,  Hypergraph\\
	4	& Zoltan library, Space-Filling Curve\\
	10	& tree number \\
	11	& tree number as if the boundaries are excluded \\
	-1	& read domain decomposition from the restart file \\
		& (during restart or in postprocessing only) \\
	\hline
\end{tabular}
	\caption[Available domain decomposition methods]{The values of 'domain\_meth'
		parameter to define the domain decomposition method.}
\label{domain_meth_table}
\end{table}





%
% ----------------------------------------------------------------------------------------
%
\subsection{Load Balancing}
\label{load_balancing}
Computational load of a processor nearly linearly depends
on the number of active (above the threshold and their nearest neighbors)
wavelets  inside that processor domain. Poorly loaded
processor inevitably creates a significant source of bottlenecking and
drastically impedes code performance. The main goal of load balancing
is to assign equal number of wavelets to each processor.
This goal is achieved by distributing trees among the processors, i.e.
tree (with all its wavelets) is the data to be transported
from one processor to another.

Dynamic load balancing is implemented
via domain repartitioning during grid adaptation step and reassigning tree
data structure nodes to the appropriate processors.
After the repartitioning, each processor transmits its relevant trees
to their newly assigned owners as well as receives trees assigned to itself
from the other processors. User provides an
``imbalance tolerance'' which triggers the repartitioning if necessary.
Currently, we monitor the total number of active wavelets, $N_p$, at each processor.
Domain repartitioning is performed during grid adaptation if the
discrepancy between $\min$ and $\max$ values of $N_p$ becomes too large, i.e
\[ \min_{p \in [1,P]} N_p <  A \cdot \max_{p \in [1,P]} N_p , \] where
$P$ is the total number of processors and $A$ is a user provided tolerance.
User can set the tolerance via optional vector parameter, \verb|domain_imbalance_tol(1:3)|,
of the input file (the default values: \verb|domain_imbalance_tol|=0.1, 0.5, 0.75).
If defined, the tolerance vector elements should be in increasing order,
to ensure that partition tolerance is the smallest and refine is the largest value:
\[ 0 \le A[1] \le A[2] \le A[3] \le 1 \]

Zoltan hypergraph partitioner performs
partition (without considering initial decomposition), repartition
(partition while trying to stay close to the current decomposition), and
refine (assumes only small changes in the initial distribution).
Highly imbalanced data will be partitioned, moderately
imbalanced - repartitioned, and not so imbalanced will
be refined, depending on the Imbalance ($\min/\max$) value:
\begin{verbatim}
    !  0 --------- A[1] ---------- A[2] ------ A[3] ------------ 1 !
    !  <-PARTITION-> <-REPARTITION-> <- REFINE -> <- DO NOTHING -> !
\end{verbatim}
Only Zoltan library hypergraph method uses all three values of vector tolerance $A$.
Other implemented partitioning methods may trigger repartitioning of the domain
relying on the maximum (refine) value only.



Two-dimensional example of load balancing
is presented at Figure~\ref{load_balancing}.
Initial domain partitioning (a) had minimum/maximum number of
wavelets per processor $\min N_p = 63$ and $\max N_p = 68$.
After several grid adaptations the processor load became
more imbalanced (b), with $\min N_p = 393$ and $\max N_p = 2208$.
Load balancing procedures replace this imbalanced (b)
by a balanced partitioning (c), with
$\min N_p = 1116$ and $\max N_p = 1295$ (Fig.~\ref{load_balancing}).

\begin{figure}[htbp]
        \centering
        (a)\includegraphics[width=1.5in]{fig/balanced_initial_b-lowres.pdf}
        (b)\includegraphics[width=1.5in]{fig/imbalanced_b-lowres.pdf}
        (c)\includegraphics[width=1.5in]{fig/balanced_final_b-lowres.pdf}
        \caption[]{Load balancing example:
                initial balanced partition (a) after several grid adaptations
                becomes imbalanced (b). Some trees are therefore to be redistributed
                for a more uniform load of the processors (c).}
        \label{load_balancing}
\end{figure}

Currently, dynamic load balancing uses Zoltan based partitioning only,
domain\_meth = 2 or 3 (Table~\ref{domain_meth_table}.
For Zoltan partitioning, the following input file parameter,
with zero default value, is available: \verb|domain_zoltan_verb| - 
the library internal verbosity level (from 0 to 10).



%
% ----------------------------------------------------------------------------------------
%
\subsection{Performance}

\begin{center}{\LARGE \dots under development \dots \\}\end{center}

Current parallel version of the code is nearly linearly scalable up to 64 processors.
The work is going on in order to improve the code scalability.

\begin{center}{\LARGE \dots under development \dots \\}\end{center}


Current parallel implementation of the code with static geometric domain decomposition
has been tested
1) on a single node with 4 Xeon processors and 4G of shared memory,
2) on NUMA (non-uniform memory access) shared memory system with 128 Itanium2 processors,
3) on a cluster of 267 4$\times$Opteron nodes interconnected by Myrinet 2g network.
The results are presented in the Table~\ref{perf1} for different number
of processors $N$.
Efficiency is measured relative to a perfect application
which would run $N$ times faster and utilize $N$ times less memory on $N$ processors.

\begin{table}[htbp]
\center
\begin{tabular}{ l l l l l }
	\hline
	System & N	& Time, s	& Memory	& Time / Memory Efficiency, \%	\\
	\hline
1 node,			& 1	& 530	& 	& 100 /	100\\
4$\times$Xeon,		& 2	& 279	& 	& 95 /	\\
shared			& 3	& 232	& 	& 76 /	\\
			& 4	& 181	& 	& 73 /	\\
	\hline
1 node,			& 1	& 1094	&	& 100 / 100	\\
128$\times$Itanium2,	& 8	& 184	&	& 74 /	\\
NUMA, shared		&	& 	&	&	\\
        		&	& 	&	&	\\
	\hline
267 nodes,		& 1	& 	&	& 100 / 100	\\
4$\times$Opteron,	& 	& 	&	& 	\\
Myrinet			& 	& 	&	& 	\\
			& 	& 	&	& 	\\
	\hline
\end{tabular}
\caption[]{Parallel adaptive wavelet based code performance.}
\label{perf1}
\end{table}

The above results are obtained for the elliptic solver test case.
The corresponding input file is
\begin{tabbing}
	\hspace{12pt} \= \verb|TestCases/EllipticTest1/test_in/base.inp|
\end{tabbing}
