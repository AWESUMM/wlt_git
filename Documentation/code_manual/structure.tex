\clearpage
\section{Code Structure}
\label{sec:structure}

\subsection{Overall Code Organization}

The Adaptive wavelet collocation method (AWCM) solver consists of two
 parts:
 \begin{enumerate}
 \item elliptic solver and
 \item time evolution solver.
 \end{enumerate}
The elliptic solver can be used either to solve general elliptic
problems of the type ${\mathcal L} {\bf u} = {\bf f}$ or as a part
of initial condiitons, where a linear system of PDEs is solved
during each grid iteration instead of prescribing ${\bf u}$
analytically. The adaptive grid refinement procedure provides a
way to obtain the solution (initial conditions) on an optimal
(compressed) grid. The pseudocode for both the iterative global
elliptic solver and the time evolution problem are shown below.

\begin{algorithm}
\rule{\textwidth}{1pt}
   \begin{tabbing}
       \hspace{12pt} \= \hspace{14pt} \= \hspace{14pt} \= \hspace{14pt} \= \kill
     \> {\bf initial guess ($ m=0$):}  ${\bf u}_{\bf k}^{m}$
     and ${\mathcal{G}}^{m}_{\ge}$ \\
%nk: don't we use a criterion based on the residual ||Lu-f||?
     \> {\bf while} $ m=0 \ {\rm  \bf or} \ m> 1 \ {\rm  \bf and} \ \left[ {\mathcal{G}}^{m}_{\ge} \ne {\mathcal{G}}^{m-1}_{\ge} \
     {\rm  \bf or} \
     \| {\mathbf f}^J - {\mathbf L} {\mathbf u}^J_{\ge} \|_{\infty} >
       \delta_{\epsilon} \right]$ \\
      \> \> $m=m+1$ \\
      \> \> {\bf perform} forward wavelet transform for each component of  ${\bf u}_{\bf k}^{m}$ \\
       \> \> {\bf for} \> all levels $j=J  : -1  :  1$ \\
       \> \> \> {\bf create} a mask ${\mathcal{M}}$ for $| d^{\mu,j}_{\mathbf l} | \ge \epsilon$ \\
         \> \> {\bf end}  \\
      \> \> {\bf extend} the mask ${\mathcal{M}}$ with  adjacent wavelets\\
      \> \> {\bf perform} the reconstruction check procedure\\
      \> \> {\bf construct} ${\mathcal{G}}^{m+1}_{\ge}$ \\
      \> \> {\bf if }  ${\mathcal{G}}^{m+1}_{\ge} \ne {\mathcal{G}}^{m}_{\ge}$ \\
      \> \> \> {\bf interpolate} ${\bf u}_{\bf k}^{m}$ to ${\mathcal{G}}^{m+1}_{\ge}$  \\
      \> \> {\bf end if} \\
      \> \> {\bf solve} ${\mathcal L} {\bf u}  = {\bf f}$ using Local Multilevel Elliptic Solver. \\
      \> {\bf end}
   \end{tabbing}
\rule[5pt]{\textwidth}{1pt} \vspace{-20pt} \caption{Global
Elliptic Solver.\vspace{20pt}} 
\label{alg:elliptic}
%\end{algorithm}
%
%\begin{algorithm}
\rule{\textwidth}{1pt}
   \begin{tabbing}
       \hspace{12pt} \= \hspace{14pt} \= \hspace{14pt} \= \hspace{14pt} \= \kill
     \> {\bf initial conditions ($n=0$):}  ${\bf u}_{\bf k}^n$
     and ${\mathcal{G}}^{n}_{\ge}$ \\
     \> {\bf while} $t_{n} < t_{\max}$ \\
      \> \> $t_{n+1} = t_{n} + \Delta t$ \\
     \> \> integrate the system of equations using Krylov time integration to obtain  ${\bf u}_{\bf
     k}^{n+1}$ \\
      \> \> {\bf perform} forward wavelet transform for each component of  ${\bf u}_{\bf k}^{n+1}$ \\
       \> \> {\bf for} \> all levels $j=J  : -1  :  1$ \\
       \> \> \> {\bf create} a mask ${\mathcal{M}}$ for $| d^{\mu,j}_{\mathbf l} | \ge \epsilon$ \\
         \> \> {\bf end}  \\
      \> \> {\bf extend} the mask ${\mathcal{M}}$ with  adjacent wavelets\\
      \> \> {\bf perform} the reconstruction check procedure\\
      \> \> {\bf construct} ${\mathcal{G}}^{n+1}_{\ge}$ \\
      \> \> {\bf if }  ${\mathcal{G}}^{n+1}_{\ge} \ne {\mathcal{G}}^{n}_{\ge}$ \\
      \> \> \> {\bf interpolate} ${\bf u}_{\bf k}^{n+1}$ to ${\mathcal{G}}^{n+1}_{\ge}$  \\
      \> \> {\bf end if} \\
      \> \> $n = n+1 $ \\
      \> {\bf end}
   \end{tabbing}
\rule[5pt]{\textwidth}{1pt} \vspace{-20pt} \caption{Time Evolution
Solver.\vspace{20pt}} 
\label{alg:evolution}
\end{algorithm}

\clearpage
\subsection{Code Categories and Files}


The code consists of FORTRAN and Matlab files. The FORTRAN code
saves results in terms of active wavelet coefficients, while
Matlab files are set up to read output files from the FORTRAN
code, perform an inverse wavelet transform, and visualize the
results.


\vspace{0.1in}\noindent The FORTRAN files can be divided into the
followqing categories:

\vspace{0.1in}\noindent \emph{Case Files:} \vspace{-0.1in} {\bf
\begin{tabbing}
\hspace{12pt} \= \hspace{14pt} \= \hspace{14pt} \= \hspace{14pt}
\= \kill
\> user\_case.f90 \\
\> user\_input.inp
\end{tabbing}
} \vspace{-0.1in}\noindent where the \verb|user_case| is the name
of a specific case set up by user that can be located in any
directory, while \verb|user_input| in the user defined input file.
Note that the same case can have multiple input files.

\vspace{0.1in}\noindent \emph{Core Files:} \vspace{-0.1in} {\bf
\begin{tabbing}
\hspace{12pt} \= \hspace{14pt} \= \hspace{14pt} \= \hspace{14pt}
\= \kill
\> wlt\_3d\_main.f90  \\
\> wavelet\_3d.f90 \\
\> wavelet\_filters.f90 \\
\> elliptic\_solve\_3d.f90  \\
\> time\_int\_cn.f90 \\
\> time\_int\_krylov\_3d.f90
\end{tabbing}
} \vspace{-0.1in}\noindent This category inculdes all the core
files for the Adaptive wavelet collocation method. We don't need
to modify any part of these files.

\vspace{0.1in}\noindent \emph{Data-Structure files:}
\vspace{-0.1in} {\bf
\begin{tabbing}
\hspace{12pt} \= \hspace{14pt} \= \hspace{14pt} \= \hspace{14pt}
\= \kill \> wavelet\_3d\_wrk.f90 \\ \> wavelet\_3d\_wrk\_vars.f90
\end{tabbing}
}

\vspace{0.1in}\noindent \emph{Shared Variables Files:}
\vspace{-0.1in} {\bf
\begin{tabbing}
\hspace{12pt} \= \hspace{14pt} \= \hspace{14pt} \= \hspace{14pt}
\= \kill
\>  shared\_modules.f90 \\
\>  wavelet\_3d\_vars.f90 \\
\>  elliptic\_solve\_3d\_vars.f90\\
\>  wavelet\_filters\_vars.f90 \\
\>  io\_3d\_vars.f90
\end{tabbing}
} \vspace{-0.1in}\noindent This category includes all the variable
only modules, {\em i.e.} these modules contain no functions or
subroutines.

\pagebreak[3] \vspace{0.1in}\noindent \emph{Supplementary Utility
Files:} \vspace{-0.1in} {\bf
\begin{tabbing}
\hspace{12pt} \= \hspace{14pt} \= \hspace{14pt} \= \hspace{14pt}
\= \kill
\> input\_files\_reader.f90 \\
\> read\_init.f90 \\
\> read\_data\_wray.f90 \\
\> io\_3d.f90 \\
\> util\_3d.f90  \\
\> default\_util.f90 \\
\> vector\_util.f90  \\
\> endienness\_big.f90 \\
\> endienness\_small.f90 \\
\> reverse\_endian.f90
\end{tabbing}
}

\vspace{0.1in}\noindent \emph{Supplementary FFT Package Files:}
\vspace{-0.1in} {\bf
\begin{tabbing}
\hspace{12pt} \= \hspace{14pt} \= \hspace{14pt} \= \hspace{14pt}
\= \kill \> fft.f90 \\ \> fftpacktvms.f90 \\ \>
fft.interface.temperton.f90 \\  \> fft\_util.f90 spectra.f90 \\ \>
ch\_resolution\_fs.f90
        \end{tabbing}
} \vspace{-0.1in}\noindent These supplementary files are located
in subdirectory FFT and, if necessary,  can be used for extracting
statistics in homogeneous directions.

\vspace{0.1in}\noindent \emph{Supplementary LINPACK files:}
\vspace{-0.1in} {\bf
\begin{tabbing}
\hspace{12pt} \= \hspace{14pt} \= \hspace{14pt} \= \hspace{14pt}
\= \kill \> d1mach.f \\ \> derfc.f \\ \> derf.f \\ \> dgamma.f \\
\> dgeev.f \\ \> dgels.f \\ \> dqage.f \\ \> dqag.f \\ \> dtrsm.f
\\ \> dum.f \\ \> fort.1
\\ \> gaussq.f \\ \> needblas.f \\ \> r1mach.f \\ \> zgetrf.f \\ \> zgetri.f
\end{tabbing}
}




\pagebreak[3] \vspace{0.1in}\noindent \emph{Visualization Files:}
\vspace{-0.1in} {\bf
\begin{tabbing}
\hspace{12pt} \= \hspace{14pt} \= \hspace{14pt} \= \hspace{14pt}
\= \kill
\> c\_wlt\_3d.m  \\
\> c\_wlt\_3d\_movie.m  \\
\> c\_wlt\_inter.f90    \\
\> inter3d.m \\
\> mycolorbar.m \\
\> mycontourf.m  \\
\> c\_wlt\_3d\_isostats.m \\
\> vor\_pal.m
\end{tabbing}
} \vspace{-0.1in}\noindent These are used to visualize the
output results.  All these files are contained in subdirectory
\verb|post_process|.

