\clearpage
\section{Appendix A}



%===================================================================
%===================================================================
\subsection{Input Parameter File Format: *.inp}
\label{subsec:input_format}

A general input file format has been used for *.inp files
to provide the code with the required input parameters.
Each *.inp ASCII file normally contains a header with short
format specifications:
\begin{verbatim}
#------------------------------------------------------------------#
# General input file format                                        #
#                                                                  #
# comments start with # till the end of the line                   #
# string constant are quoted by "..." or '...'                     #
# boolean can be (T F 1 0 on off) in or without ' or " quotes      #
# integers are integers                                            #
# real numbers are whatever (e.g. 1   1.0   1e-23   1.123d-54 )    #
# vector elements are separated by commas                          #
# spaces between tokens are not important                          #
# empty or comment lines are not important                         #
# order of lines is not important                                  #
#------------------------------------------------------------------#
\end{verbatim}
To provide the code with a real parameter $ParName=1.234\cdot 10^{-5}$,
the following line should be inserted into the input file:
\begin{tabbing}
 \hspace{12pt} \= \verb|ParName = 1.234e-5   #Some comments, if required|
\end{tabbing}
To allow the reading of that input parameter by the main code,
the public function of module INPUT\_FILE\_READER has to be called
from the main code:
\begin{tabbing}
 \hspace{12pt} \= \verb|REAL :: var ! here the value of ParName to be stored| \\
 \hspace{12pt} \= \verb|CALL input_real('ParName', var, KEY, 'SOME COMMENT')| ,
\end{tabbing}
where \verb|KEY| takes the value \verb|'stop'| or \verb|'default'|.
The first one terminates the code execution if the parameter
\verb|ParName| is not present in the input file, while the second
one let the code continue running as if nothing happened.
The last argument of the function is an optional string comment.
If the reading is performed in verbose mode,
the screen output of the function call is the following:
\begin{tabbing}
 \hspace{12pt} \= \verb|ParName = 1.234e-5   # SOME COMMENT|
\end{tabbing}

For details, examine the code for the general input reader, which is
located in the module INPUT\_FILE\_READER, file input\_files\_reader.f90
The module contains several private parameters which might need to be
changed depending on the system and/or compiler.

The complete list of the input parameters is provided in the Appendix B
(\ref{subsec:parameters}).





%===================================================================
%===================================================================
\subsection{Result File Format: *.res}

An endian independent I/O library is used to read or write result files.
The library consists of two files: tree-c/io.h and tree-c/io.cc
The header file io.h contains short description of the functions to
use from Fortran subroutines to read or write result files. The header
file also contains definitions of integer and real types which could
be changed on the systems with nonstandard C integer or real sizes.
Additionally, file io\_3d.f90 contains some private parameters which could
be changed on systems with nonstandard Fortran integer or real sizes.

In order to simplify the code development, it is recommended to keep
all I/O operations with result files inside READ\_SOLUTION and SAVE\_SOLUTION
subroutines of the module IO\_3D, file io\_3d.f90
Providing version control and backward compatibility for all the future
result file formats will be easier in this case. In short, if you need to read
result file, e.g. for some post-processing, call READ\_SOLUTION. If you would
like to save data in *.res format, call SAVE\_SOLUTION.

Current version of the code is capable of reading old, endian dependent
*.res files (so called 01/31/2007 format) as well as new
(so called 02/01/2007 endian independent format) *.res files
without interaction with user.
Unless specifically requested, only new endian independent format
is used to save the result files.

In 09/01/2011 another version of .res format was introduced,
so called Version 5. In a serial run, on a single processor, this version
is identical to the latest modification of 02/01/2007
format, also called as Version 4. In a parallel run, on several processors,
a separate .com.res file is created to store partition table and other
parameters which otherwise would have been duplicated in each processor's
Version 4 .res file.

Though the code should be able to read any version of .res file please keep
in mind that Version 4 and 5 are the only versions currently recommended.
Set optional .inp file parameter $dot\_res\_file\_version=4$ to enforce
.res output in Version 4 even for a parallel run. The default value is $5$.

See Appendix C (\ref{subsec:res_file}) for a more comprehensive explanation
of .res file structure.




%===================================================================
%===================================================================
\subsection{Porting the Code}

Some notes have to be made regarding portability of the code to different
platforms. First of all, for all the platforms some additional libraries
may need to be installed to fully utilize capabilities of the code,
e.g. HDF, CGNS, or LAPACK libraries.

\subsubsection{PC/Linux, gcc, icc, ifort}
A typical machine specific file is makefile.ifort\_icc.32, where
name extensions indicate compilers and machine architecture (32 or 64 bit).
Problems might occur with C and Fortran linking. A correct library has to be
provided as \verb|LINKLIB|. It can be something like /usr/lib/libstdc++.so.5,
or similar, depending on the system/compiler. Additionally, Fortran compiler
has to be informed not to attach underscores to the function names.

\subsubsection{IBM Power4/AIX, xlc, xlf}
A typical machine specific file is makefile.ifort\_gcc.64
In addition to providing a correct library for linking
C and Fortran functions and dealing with underscores a special flag
\verb|-DDATABASE_INTERFACE_LOWERCASE| might need to be used.
The purpose of that flag is to include tree-c/lowercase.h which
forces all C functions into lowercase.
It might need to be used to overcome some compilation problems with xlf compilers.

\subsubsection{PC/WinXP, MSVC, MSVF}
First known problem is the failure of MS to recognize C99 standard. As a consequence,
there are no standard integer types stdint.h; it means that the endian independent
library may require some adjustments on such systems. Examine the files tree-c/io.h
and io\_3d.f90 for details. Currently we are using \_MSC\_VER macro, which may be defined
by MS compilers, to provide MS specific definitions.
Another known problem is that Fortran GETARG function require module DFLIB,
which we currently include through the mentioned \_MSC\_VER macro.
Inside MS Visual Studio, function calling convention might need to be changed
to \_\_cdecl and Fortran string length has to be passed after all the arguments.
Additionally, some care has to be taken of capitalizing and attaching underscores
to the function names, similar to all other compilers.




%===================================================================
%===================================================================
\subsection{Some Important Macros}
Several macros have been used through the code and the makefiles.
Some of the macros are merely debugging ones, e.g. starting with \verb|EBUG_|.
Others may serve to include various features into the code.
Those macros are listed below. To define macro, e.g. {\em NAME}, add \verb|-DNAME |
to the correspondent compiler flags in your machine specific makefile.

\begin{description}
\item[DATABASE\_APPEND\_UNDERSCORE]		Define it in machine specific makefile in order to
						to append an underscore to C++ functions of the database interface
						(e.g. for the use with some pgi compilers which might not have
						a correspondent compiler flag).

\item[DATABASE\_INTERFACE\_LOWERCASE]		Define it in machine specific makefile in order to
						transform uppercase database interface functions to lowercase
						(e.g. for the use with xlf compilers, instead of -U flag for
						the FORTRAN code, which might produce some complications).

\item[MULTIPROC]				Macro to include parallel related functions.
						Always define it in MPIFLAGS line of your machine specific makefile.
						In addition, set \verb|MP=1| parameter of \verb|gmake|
						in order to compile a parallel code.

\item[DATABASE\_MPI\_APPEND\_UNDERSCORE]	Define it in machine specific makefile in order to append
						an underscore to MPI functions
						(e.g. for the use with some pgi compilers).

\item[DATABASE\_MPI\_APPEND\_UNDERSCORE2]	Define it in machine specific makefile in order to append
						second underscore to MPI functions.

\item[USE\_DEBUG\_TIMERS]			Define it in machine specific makefile in order to use
						timer related subroutines of debug\_vars module:
						timer\_start(), timer\_stop(), timer\_val(), etc. Make sure
						to set the required maximum number of timers inside debug\_vars.f90

\item[USE\_USER\_CLI]				Define it in machine specific makefile in order to call user provided
						user\_read\_command\_line\_input() subroutine from the main code to read
						the command line arguments, instead of the regular
						read\_command\_line\_input().


\item[EBUG\_SAFE, EBUG\_AMR, \dots]		Debugging macros. Will degrade code performance.
						Do not use them in a production version of your code unless you really need them.

\item[TREE\_VERSION]				Main makefile internal macro. Accepts values of 0, 1, 2, and 3 for different
						tree structures of the database: db\_tree, db\_tree1, and two Fortran db\_treef
						structures respectively. Defined internally through the name of the database
						during the main compilation.

\item[PTR\_INTEGER8]				An important macro to control the correct size of integers to be used
						as pointers to the nodes of tree structure.
						Define it in the machine specific variable, FFLAGS, for 64 bit systems
						(e.g. {\em FFLAGS = \dots -DPTR\_INTEGER8 \dots})
						
\item[COMM\_ONE2ONE]				Define to substitute MPI\_Alltoall by a series of MPI\_Send/Recv calls.
						Applied to the communication during known list synchronization only,
						which is the most frequently used communication.
\end{description}



