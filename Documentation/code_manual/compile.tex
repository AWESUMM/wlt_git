\clearpage
\section{Getting Started}
\label{sec:compile}

\subsection{Obtaining the Source Code}
\label{subsec:obtain}
Currently, the most efficient means of obtaining the code is through
the use of Subversion. The use of Subversion allows you to keep
your code up to date with the current release without starting all
over again. Subversion is available for download at
\href{http://subversion.tigris.org}{http://subversion.tigris.org}
and documentation is available
at \href{http://svnbook.red-bean.com}{http://svnbook.red-bean.com}.

Once you have subversion installed, make a directory where you want
to put the source code directory called \verb|wlt_3d|. Move into this
directory and use the command
\begin{tabbing}
  \hspace{12pt} \= svn checkout https://www.cora.nwra.com/svn/wlt/{\em
  user-branch}
\end{tabbing}
You will be asked for a login name and password, you should have this
information already. Enter the information and the import will commence.

It is important that you do not modify any of the files in the main
source code. If you do, the next time a newly released version of the
code will conflict with the changes you have made. It is important
that you create your own case files and edit those exclusively so
that you do not introduce any conflicts. If you do find a bug in the
main source code, please send an email to
\href{oleg.vasilyev@colorado.edu}{oleg.vasilyev@colorado.edu} so that
the problem is properly resolved. If these guidelines are followed,
each time an updated version is released

When an update does become available you can see what files have been
changed beforehand by running the command \verb|svn status -q| from the
source directory. The \verb|-q| option suppresses output associated with
files that are not under version control. Then to proceed with updating
the changed files run the command \verb|svn update| and you will have the
latest release. If after updating to the latest release there are
problems and your code will not run anymore you can perform and
\verb|svn revert| and the code will be restored to its previous version.



\subsection{Compilation and Execution}
\label{subsec:compile}
\noindent The source code is always compiled from the source
directory regardless of which case file executable you are creating.
There are three arguments that must be passed to gmake in order for
it to know what to do. First of all, the compiler needs to know
which database to use
for storing the data. As of now, two main databases in use are
\verb|db_wrk| and \verb|db_tree| (\verb|db_lines| will be available soon;
the databases \verb|db_tree1| and \verb|db_treef| are for developers only). The
next argument is which case you are going to run. Since the
compilation is done from the source directory, the path to the
case file needs to be specified and the executable will be placed
in that directory. The path can be specified as an absolute path
or relative to the source directory.

Before attempting to compile anything a makefile must be created specific
to your computer. Under the source directory is a subdirectory called
\verb|makefiles.machine_specific|. Inside this directory are numerous
different makefiles all in the form \verb|makefile.some_name|. The makefile
in the source directory references the makefiles in this directory through
the use of an environment variable \verb|WLT_COMPILE_HOSTNAME|. You will
need to set an environment variable \verb|WLT_COMPILE_HOSTNAME|
to your \verb|some_name| (e.g. `export WLT\_COMPILE\_HOSTNAME = some\_name'
in \verb|bash|, or `setenv WLT\_COMPILE\_HOSTNAME some\_name' in \verb|cshell|).
You will also need to copy one of
the makefiles in the \verb|makefiles.machine_specific| directory that is
similar to your computer's configuration and rename the file
\verb|makefile.some_name|. Make sure to make any changes necessary to
the names of the compiler and flags so that it will work on your machine.
Once all this is done, gmake will know what to do when you compile.

\subsection{Elliptic Solver Example}
\label{subsec:example}

There are several test cases provided with the code and are located
in the TestCases directory. In order to demonstrate how to successfully
compile the code we will start with the EllipticTest1. From the source
directory, if you move into the \verb|TestCases/EllipticTest1| directory and
list the files you will see the following list of files along with
some others that aren't of importance
%
\begin{tabbing}
  \hspace{12pt} \= case\_elliptic\_poisson.f90 \\
  \> case\_elliptic\_poisson.PARAMS.f90 \\
  \> results (directory) \\
  \> test\_in \\
  \> showme3D.m \\
\end{tabbing}
%
From looking at the different files one can conclude that the case
name is \verb|case_elliptic_poisson|.  As of now the exact syntax used to
compile is
\begin{tabbing}
 \hspace{12pt} \= \verb|gmake DB=databaseName CASE=[path of case]/caseName wlt_3d|.
\end{tabbing}
Remember that this is always done in the source directory.
For our example [path of case] is \verb|TestCases/EllipticTest1,| the
case name is \verb|case_elliptic_poisson|, and we'll be using the
database \verb|db_wrk|. The following syntax should successfully compile
the code
\begin{tabbing}
  \hspace{12pt} \= gmake DB=db\_wrk CASE=TestCases/EllipticTest1/case\_elliptic\_poisson wlt\_3d \\
\end{tabbing}
Make sure to add the target \verb|wlt_3d| at the end so the compiler
will know what to do. Once the code has finished compiling the
executable will be located in the case directory and will be
called, %\verb|wlt_3d_databaseName_caseName.out| and
in this example, \verb|wlt_3d_db_wrk_case_elliptic_poisson.out|.
If we move to the case directory the case can be executed by
running the executable and specifying which input file is to
be used as an argument. The input files are located in the
\verb|test_in| subdirectory. For example, if we wanted to run the
2-D test problem we would use the following command
\begin{tabbing}
  \hspace{12pt} \= ./wlt\_3d\_db\_wrk\_case\_elliptic\_poisson.out
    test\_in/base.inp \\
\end{tabbing}
It should be noted at this time that during the compilation
step an important file was overlooked, which is the
\verb|case_elliptic_poisson.PARAMS.f90| file. Inside this file are two
parameters needed by \verb|db_lines| and \verb|db_treef|
database in order to construct the databases
size and dimensions. If this file is absent the compiler will fail.
However, a run time error can occur if the parameters are not set
large enough to handle the input file's dimensions and number of
variables being stored. Inside this file the parameter \verb|max_dim|
is set to 3. This allows the database to allocate enough room to
run both the 2D and 3D input files. The number of variables is also
set to 10 (\verb|n_var_db=10|), which is large enough to handle the
3D case with some extra variables stored. For the 2D case it would
be possible to change \verb|max_dim| to 2 for increased performance,
but it is set to 3 for now so that both cases can be run without
changing this file.


\subsection{Post Processing}
\label{subsec:process}

Once you have succesfully run the test case from within the test case
directory, the output files will be stored in the results subdirectory.
The output files have the file extension \verb|.res|. In order to view
the output files in MATLAB, the post-process codes \verb|c_wlt_inter.out|
and \verb|res2matlab.out| needs to be compiled.
From the source directory compile all the needed
files with the target \verb|inter| using the command
\verb|gmake DB=db_wrk inter|. The executable created is located in the
\verb|post_process| directory.
Since matlab uses regular grid the matlab related post-processing
codes will use regular grid also. Thus, \verb|db_wrk| is the only database
to compile matlab related post-processing with. Another visualization
tool, \verb|res2vis|, is available for large datasets on adaptive grids.
