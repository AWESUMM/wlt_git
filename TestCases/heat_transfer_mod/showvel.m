function showvel(maxrat,numlines,xpts,ypts,usej,jtouse,clo,chi,opfigs,saveit,spm,filebase,S,U1,U2,iter)
global x y u v xs ys s
close all
file=strcat(filebase,'.')
%spm='s'

local_dir = pwd
if isunix
    pl = '/'
else
    pl = '\';
end
global POSTPROCESS_DIR
POSTPROCESS_DIR = [pwd pl '..' pl '..' pl '..' pl 'post_process']

path(path,POSTPROCESS_DIR)
path(path,[POSTPROCESS_DIR pl 'sliceomatic'])
clear pl
cd(local_dir)

%maxrat=10;

eps=0.0;
plot_type = 'surf';
j_range=[0 20];
bounds=[0];
az=0;
el=90;
slev=0.9;
station_num=iter;
x0=[0 0 0 2];
n0=[[0 1 1]' [0 1 -1]'];

mrkcl = 'k'
mrksz = 1.0

if size(j_range,2) == 1                           % set j_min and j_max
    j_min=j_range;
    j_max=j_range;
elseif size(j_range,2) == 2
    j_min=j_range(1);
    j_max=j_range(2);
else
    j_min=0;
    j_max=20;
end

if usej==1
   % j_max=jtouse
end

station_num=iter;

%cd('..')

[s,xs,ys,zs,nxs,nys,nzs,dim,time] ...
    =  inter3d(file,eps,bounds,S,[j_min,j_max],station_num,spm);

[u,x,y,z,nx,ny,nz,dim,time] ...
    =  inter3d(file,eps,bounds,U1,[j_min,jtouse],station_num,spm);

[v,x,y,z,nx,ny,nz,dim,time] ...
    =  inter3d(file,eps,bounds,U2,[j_min,jtouse],station_num,spm);

if dim == 3
    [w,x,y,z,nx,ny,nz,dim,time] ...
        =  inter3d(file,eps,bounds,'P3',[j_min,jtouse],station_num,spm);
end


if usej==0
    xstp=max(floor((length(x)-1)/xpts),1);
    ystp=max(floor((length(y)-1)/ypts),1);
else
    xstp=1;
    ystp=1;
end

fprintf('time = %12.8f\n', time);     % 6/7/07 addition

if ( dim == 2 )&((strcmp(plot_type,'isosurface'))|(strcmp(plot_type,'slice')))
    plot_type = 'surf';
end

fprintf(1,'size(u) =');
disp(size(u));
fprintf(1,'nz = %d\n',nz)

if size(bounds,2) == 6
    xmin=bounds(1);
    xmax=bounds(2);
    ymin=bounds(3);
    ymax=bounds(4);
    zmin=bounds(5);
    zmax=bounds(6);
else
    xmin=min(x);
    xmax=max(x);
    ymin=min(y);
    ymax=max(y);
    if(dim == 3 )
        zmin=min(z);
        zmax=max(z);
    end
end
xmin=min(x);
xmax=max(x);
ymin=min(y);
ymax=max(y);

zmin=min(min(min(s(:,:,:))))
zmax=max(max(max(s(:,:,:))))
if( zmin == zmax )
    zmin = zmax -1;
    zmax= zmax;
end
%zmin=-1;
%zmax=1;
    if (opfigs==1)
        figure
    end
for zslice = 1:nzs
    set(cla,'nextplot','replace')
    clf;
    l1=0.1;b1=0.1;w1=0.75;h1=0.75;
    ax1 = axes('position',[l1,b1,w1,h1]);
    contourf(xs,ys,s(:,:,zslice),100,'Linecolor','none')
hold on
    %surface(ys+max(ys(:)),xs,s(:,:,zslice)')
hold off
    view(az,el);
    caxis([zmin zmax]);
    set(gcf,'renderer','zbuffer');
    set(gca,'xlim',[ymin,ymax],'ylim',[xmin,xmax],'zlim',[zmin,zmax],'clim',[zmin,zmax]);
    minlen=min(min((xmax-xmin),(ymax-ymin)),(zmax-zmin));
    if dim==2
        minlen=min((xmax-xmin),(ymax-ymin));
    end
    yasp=min((ymax-ymin)/minlen,maxrat);
    xasp=min((xmax-xmin)/minlen,maxrat);
    zasp=min((zmax-zmin)/minlen,maxrat);
    set(gca,'PlotBoxAspectRatio',[yasp,xasp,zasp])
    set(gca,'FontSize',16)
    colorbar('vert','FontSize',16)
    zlabel('solution')
%     if( nz ~= 1 )
%         title(['Solution, zSlice =' , num2str(zslice),' at time ', num2str(time,'%8.3f')],'FontSize',18)
%         fprintf('zSlice = %d (hit <CR> for next active Zslice )\n', zslice );
%     else
%         title(['s at time ', num2str(time,'%8.3f')],'FontSize',18)
%     end
    set(gca,'clim',[zmin,zmax])
    if( nz ~= 1 )
        pause;
    end
end

curstr=strcat('plots/',filebase,'_s_',num2str(iter),'.fig')
if saveit==1
    saveas(gcf,curstr)
end
%close all
if usej==0
    xstp=max(floor((length(x)-1)/xpts),1);
    ystp=max(floor((length(y)-1)/ypts),1);
else
    xstp=1;
    ystp=1;
end


fprintf('time = %12.8f\n', time);     % 6/7/07 addition

fprintf(1,'size(u) =');
disp(size(u));
fprintf(1,'nz = %d\n',nz)

if size(bounds,2) == 6
    xmin=bounds(1);
    xmax=bounds(2);
    ymin=bounds(3);
    ymax=bounds(4);
    zmin=bounds(5);
    zmax=bounds(6);
else
    xmin=min(x);
    xmax=max(x);
    ymin=min(y);
    ymax=max(y);
    if(dim == 3 )
        zmin=min(z);
        zmax=max(z);
    end
end
xmin=min(x);
xmax=max(x);
ymin=min(y);
ymax=max(y);


zmin=min(min(min(s(:,:,:))));
zmax=max(max(max(s(:,:,:))));
if( zmin == zmax )
    zmin = zmax -1;
    zmax= zmax;
end
%zmin=-1;
%zmax=1;
%ymax=2*ymax-ymin;

    if (opfigs==1)
        figure
    end

% for zslice = 1:nz
    hold  on
    %set(cla,'nextplot','replace')
    %clf;
    l1=0.1;b1=0.1;w1=0.75;h1=0.75;
    %ax1 = axes('position',[l1,b1,w1,h1]);
    quiver(x,y,u,v,0.5,'Color','w')
hold on
    %quiver(y(1:ystp:end)+max(y(:)),x(1:xstp:end),v(1:ystp:end,1:xstp:end,zslice)',u(1:ystp:end,1:xstp:end,zslice)')
hold off
    view(az,el);
    set(gcf,'renderer','zbuffer');
    set(gca,'xlim',[ymin,ymax],'ylim',[xmin,xmax],'zlim',[zmin,zmax]);
    minlen=min(min((xmax-xmin),(ymax-ymin)),(zmax-zmin));
    if dim==2
        minlen=min((xmax-xmin),(ymax-ymin));
    end
    yasp=min((ymax-ymin)/minlen,maxrat);
    xasp=min((xmax-xmin)/minlen,maxrat);
    zasp=min((zmax-zmin)/minlen,maxrat);
    set(gca,'PlotBoxAspectRatio',[yasp,xasp,zasp])
    set(gca,'FontSize',16)
    zlabel('solution')
%     if( nz ~= 1 )
%         title(['Velocity, zSlice =' , num2str(zslice) ,' at time ', num2str(time,'%8.3f')],'FontSize',18)
%         fprintf('zSlice = %d (hit <CR> for next active Zslice )\n', zslice );
%     else
%         title(['Velocity at time ', num2str(time,'%8.3f')],'FontSize',18)
%     end
    set(gca,'clim',[zmin,zmax])
    if( nz ~= 1 )
        pause;
    end
%end

curstr=strcat('plots/',filebase,'_vel_',num2str(iter),'.fig')
if saveit==1
    saveas(gcf,curstr)
end


minsol=min(min(u))
maxsol=max(max(u))

