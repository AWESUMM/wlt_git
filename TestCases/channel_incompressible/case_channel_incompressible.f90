
MODULE user_case
  ! CASE 2/3 D channel periodic in x-direction and if dim=3 in z-derection
  USE precision
  USE wlt_vars
  USE wavelet_filters_mod
  USE elliptic_mod
  USE elliptic_vars
  USE wlt_trns_mod									
  USE wlt_trns_vars
  USE wlt_trns_util_mod
  USE io_3d_vars
  USE util_mod
  USE util_vars
  USE share_consts
  USE pde
  USE sizes
  USE share_kry
  USE vector_util_mod
  USE field
  USE input_file_reader
  USE debug_vars
  !
  ! case specific variables
  !
  INTEGER n_var_pressure  ! start of pressure in u array

  REAL (pr) :: Re     ! kinematic viscosity (1/Re)
  REAL (pr) :: Le     ! Lewis number
  REAL (pr) :: dPdx   ! mean preasure gradiant (imposed) 
  REAL (pr) :: U_wall ! velocity of the upper wall for the Couette flow
  INTEGER :: alpha    ! power exponent for initial velocity profile
  INTEGER :: meth_p   ! method used to solve Laplace Equation for pressure
  LOGICAL :: passive_scalar

CONTAINS

  !
  ! In user_setup_pde() we setup how many variables are integrated, which are interpolated
  ! to the next times step and if any exeact solution exists to compare to during the run.
  ! We also set the variable names that are used when the result files are written.
  !
  ! The following variables must be setup in this routine:
  !
  !
  ! n_integrated     ! first n_integrated eqns will be acted on for time integration
  ! n_var_additional ! 
  ! n_var
  !
  !
  !
  !
  SUBROUTINE  user_setup_pde ( VERB ) 
    IMPLICIT NONE
    LOGICAL, OPTIONAL :: VERB         ! print debug info
    INTEGER :: i

    IF (verb_level.GT.0) THEN
       PRINT * ,''
       PRINT *, '**********************Setting up PDE*****************'
       PRINT * ,'CASE: incompressible Poiseulle - Couette flow channel flow '
       PRINT *, '*****************************************************'
    END IF

    
    IF(passive_scalar) THEN
       n_integrated = dim + 1 !velocty plus passive scalar
    ELSE
       n_integrated = dim  ! velocty plus passive scalar
    END IF
    n_time_levels = 1  !--3 time levels

    n_var_time_levels = n_time_levels * n_integrated !--Number of equations (2D velocity at two time levels)

    n_var_additional = 1 !--1 pressure at one time level 
    n_var = n_var_time_levels + n_var_additional !--Total number of variables

    n_var_exact = dim !--One exact solution for u,v,w

    n_var_pressure  = n_var_time_levels + 1 !pressure

    !
    !--Allocate logical arrays 
    !  Must be done after setting number of different types of variables
    !  and before setting logical variable mapping arrays
    !
    CALL alloc_variable_mappings


    !
    ! Fill in variable names for default variables for integration meth 2
    ! These may be changed in the call to the user specified routine
    !  user_setup_pde()
    !
    ! In integrated variables (3D)
    IF( dim == 3 ) THEN
       WRITE (u_variable_names(1), u_variable_names_fmt) 'Velocity_u_@t  '
       WRITE (u_variable_names(2), u_variable_names_fmt) 'Velocity_v_@t  '
       WRITE (u_variable_names(3), u_variable_names_fmt) 'Velocity_w_@t  '
       IF(n_integrated == dim+1) WRITE (u_variable_names(dim+1), u_variable_names_fmt) 'Scalar  '
       WRITE (u_variable_names(n_var_pressure), u_variable_names_fmt) 'Pressure  '
    ELSE IF(dim == 2) THEN
       WRITE (u_variable_names(1), u_variable_names_fmt) 'Velocity_u_@t  '
       WRITE (u_variable_names(2), u_variable_names_fmt) 'Velocity_v_@t  '
       IF(n_integrated == dim+1) WRITE (u_variable_names(dim+1), u_variable_names_fmt) 'Scalar  '
       WRITE (u_variable_names(n_var_pressure), u_variable_names_fmt) 'Pressure  '
    END IF


    !
    ! setup logical variable mapping arrays
    ! This defined which variables are used for adaptation, Which need to be interpololated to the new grid at 
    ! each time step, which are saved, and for which we have an exect solutiuon we want to check at each time step
    !


    !
    ! setup which components we will base grid adaptation on.
    ! Adapt on the 3 velocity components
    !
    n_var_adapt = .FALSE. !intialize
    n_var_adapt(1:n_integrated,0)     = .TRUE. !--Initially adapt on u,v integrated variables at first time level

    n_var_adapt(1:n_integrated,1)     = .TRUE. !--After first time step adapt on 

    !--integrated variables at first time level

    !Variables that need to be interpoleted to new adapted grid in initial grid adaptation
    n_var_interpolate        = .FALSE. !intialize
    n_var_interpolate(1: n_integrated,0) = .TRUE.  
    
    !Variables that need to be interpoleted to new adapted grid at each time step
    n_var_interpolate(1:n_var_pressure,1) = .TRUE. 

    ! Assigns wavelet transform family) for each variable
    n_var_wlt_fmly(1:n_var) = 1
    !
    ! setup which components we have an exact solution for
    !
    n_var_exact_soln = .FALSE. !intialize
    n_var_exact_soln(1:dim,0:1) = .TRUE.

    !
    ! setup which variables we will save the solution
    !
    n_var_save = .FALSE. !intialize 
    n_var_save(1:n_var) = .TRUE. ! save all for restarting code

    !
    !--Time level counters for NS integration
    !   Used for integration meth2
    ! n0 is current time step
    ! n1 is t_n-1 time step
    ! n2 is t_n-2 time step
    !
    n0 = 1; n1 = n0+dim; n2 = n1+dim

    !
    ! Set the maximum number of components for which we have an exact solution
    !
    n_var_exact = MAX( COUNT(n_var_exact_soln(:,0)),COUNT(n_var_exact_soln(:,1)) )

    !
    ! Setup a scaleCoeff array of we want to tweak the scaling of a variable
    ! ( if scaleCoeff < 1.0 then more points will be added to grid 
    !
    ALLOCATE ( scaleCoeff(1:n_var), Umn(1:n_var) )
    scaleCoeff = 1.0_pr


    Umn = 0.0_pr !set up here if mean quantities are not zero and used in scales or equation

    IF (verb_level.GT.0) THEN
       PRINT *, 'n_integrated = ',n_integrated 
       PRINT *, 'n_time_levels = ',n_time_levels
       PRINT *, 'n_var_time_levels = ',n_var_time_levels 
       PRINT *, 'n_var = ',n_var 
       PRINT *, 'n_var_exact = ',n_var_exact 
       PRINT *, '*******************Variable Names*******************'
       DO i = 1,n_var
          WRITE (*, u_variable_names_fmt) u_variable_names(i)
       END DO
       PRINT *, '****************************************************'
    END IF

  END SUBROUTINE  user_setup_pde
  !
  ! Set the exact solution for comparison to the simulated solution
  !
  ! u          - array to fill in the exact solution
  ! nlocal       - number of active wavelets   
  ! ne_local        - total number of equations
  ! t          - time of current time step 
  ! l_n_var_exact_soln_index - index into the elements of u for which we need to 
  !                            find the exact solution
  SUBROUTINE  user_exact_soln (u, nlocal,  t_local, l_n_var_exact_soln)  
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: nlocal
    REAL (pr), INTENT (IN) ::  t_local
    REAL (pr), DIMENSION (nlocal,n_var_exact), INTENT (INOUT) :: u
    LOGICAL , INTENT (IN) :: l_n_var_exact_soln(n_var)
    INTEGER :: i

    ! Poiseuille-Couette flow
    !x component of velocity
    u(:,1) = -0.5_pr*Re*dPdx*x(:,2)*(1.0_pr - x(:,2)) &
             +U_wall*(x(:,2)-xyzlimits(1,2))/(xyzlimits(2,2)-xyzlimits(1,2))
    !y component of velocity
    u(:,2) =  0.0_pr 
    !3 component of velocity
    IF(dim == 3) u(:,3) = 0.0_pr

    !WRITE( *,'( "in user_exact_soln t_local, nwlt " , f30.20, 1x , i8.8 )' ) t_local , nlocal
   

  END SUBROUTINE  user_exact_soln

  SUBROUTINE user_initial_conditions (u, nlocal, ne_local, t_local, scl, scl_fltwt, iter)
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: nlocal, ne_local
    INTEGER  , INTENT (INOUT) :: iter ! iteration of call while adapting initial grid
    REAL (pr), DIMENSION (nlocal,ne_local), INTENT (INOUT) :: u
    REAL (pr)  :: scl(1:n_var),scl_fltwt
    REAL (pr), INTENT (IN) :: t_local
    REAL (pr) :: Vperturb, phi, r_vortex 
    REAL, DIMENSION(dim) :: n_periods, x_vortex, del_vortex
    REAL, DIMENSION(nlocal,dim) :: xv
    INTEGER :: i, idim
    INTEGER*4 :: irand

    irand = 1
    Vperturb = 0.1_pr !divergence-free perturbation
    phi = 0.00_pr*Pi
    n_periods = 1.0_pr
    n_periods(2) = 2.0_pr
    r_vortex =0.1_pr*MINVAL(xyzlimits(2,1:dim)-xyzlimits(1,1:dim))
    x_vortex = 0.5_pr*(xyzlimits(2,:)+xyzlimits(1,:))
    del_vortex = 0
    del_vortex(2) = 0.5_pr*r_vortex
    
!!$    u(:,1) = -Re*dPdx/12.0_pr + 0.5_pr*U_wall! mean velocity
    u(:,1) = - Re*dPdx/12.0_pr*REAL(alpha+1,pr)/REAL(alpha,pr)*(1.0_pr-(2.0_pr*x(:,2)-1.0_pr)**alpha) &
             + U_wall*((x(:,2)-xyzlimits(1,2))/(xyzlimits(2,2)-xyzlimits(1,2)))**alpha 
    u(:,2) = 0.0_pr
    IF(dim == 3) u(:,3) = 0.0_pr

    !------------------- white noise perturbation --------------------
!!$    DO i=1,Nwlt_lev(j_lev,0)
!!$       DO idim =1, dim
!!$          u(i,idim) = u(i,idim)+Vperturb*(RAN(irand)-0.5_pr)*(1.0_pr-(2.0_pr*x(i,2)-1.0_pr)**2) 
!!$       END DO
!!$    END DO
    !-------------------- Velocity --------------
    IF(.FALSE.) THEN
       !  f(y) - perturbation 
       u(:,1) = u(:,1)+Vperturb*((x(:,2)-xyzlimits(1,2))/(xyzlimits(2,2)-xyzlimits(1,2)))**2 &
                               *((x(:,2)-xyzlimits(2,2))/(xyzlimits(2,2)-xyzlimits(1,2)))**2 
    ELSE
       !-------------- Vortex pair
       DO idim =1, dim
          xv(:,idim) = (x(:,idim) -x_vortex(idim)-del_vortex(idim))/r_vortex
       END DO
       u(:,1) = u(:,1)-Vperturb*xv(:,2)*EXP(-(xv(:,1)**2+xv(:,2)**2)) 
       u(:,2) = u(:,2)+Vperturb*xv(:,1)*EXP(-(xv(:,1)**2+xv(:,2)**2))
       DO idim =1, dim
          xv(:,idim) = (x(:,idim) -x_vortex(idim)+del_vortex(idim))/r_vortex
       END DO
       u(:,1) = u(:,1)+Vperturb*xv(:,2)*EXP(-(xv(:,1)**2+xv(:,2)**2))
       u(:,2) = u(:,2)-Vperturb*xv(:,1)*EXP(-(xv(:,1)**2+xv(:,2)**2)) 
    END IF
    !-------------------- Scalar -----------------------
    IF(n_integrated == dim+1) THEN
       DO idim =1, dim
          xv(:,idim) = (x(:,idim) -x_vortex(idim))/r_vortex
       END DO
       u(:,dim+1) = EXP(-(xv(:,1)**2+xv(:,2)**2)) 
    END IF
    !
    ! Set Mean flow
    !
    Umn = 0.0_pr

    IF (verb_level.GT.0) THEN
       DO i=0,5
          PRINT *,i,'initial conditions: div =', MAXVAL(ABS(div(u(:,1:dim),nlocal,j_lev,i)))
       END DO
       WRITE( *,'( "in user_IC t_local, nwlt " , f30.20, 1x , i5.5 )' ) t_local , nlocal
    END IF



  END SUBROUTINE user_initial_conditions

!--********************************  
  ! Arguments
  ! u         - field on adaptive grid
  ! nlocal      - number of active points
  ! ne_local       - number of equations
  ! t         - current time
  !
  ! Global variables used
  ! ifn       - number of equations
  ! x()
  ! xO
  ! y0
  !--******************************** 
  SUBROUTINE user_algebraic_BC (Lu, u, nlocal, ne_local, jlev, meth)
    !--Defines boundary condition type
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: jlev, meth, ne_local, nlocal
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (INOUT) :: Lu
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (IN)    :: u
    REAL (pr), DIMENSION (ne_local, nlocal, dim) :: du
    REAL (pr), DIMENSION (ne_local, nlocal, dim) :: d2u

    INTEGER :: i, ie, ii, shift
    !REAL (pr), DIMENSION (ne_nlocal,nlocal,dim) :: du, d2u !uncomment if Neuman BC are used, need to call 
    INTEGER :: face_type, nloc, meth_local
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc

    !BC for both 2D and 3D cases

    !if Neuman BC are used, need to call 
    CALL c_diff_fast (u, du, d2u, jlev, nlocal, meth, 10, ne_local, 1, ne_local)

    DO ie = 1, ne_local
       shift = nlocal*(ie-1)
       !--Go through all Boundary points that are specified
       i_p_face(0) = 1
       DO i=1,dim
          i_p_face(i) = i_p_face(i-1)*3
       END DO
       DO face_type = 0, 3**dim - 1
          face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
          IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
             CALL get_all_indices_by_face (face_type, jlev, nloc, iloc)
             IF(nloc > 0 ) THEN 
                IF( face(2) == -1  ) THEN                          ! Ymin face (entire face) 
                   Lu(shift+iloc(1:nloc)) = u(shift+iloc(1:nloc))  !Dirichlet conditions
!!$                Lu(shift+iloc(1:nloc)) = du(ie,iloc(1:nloc),2)  !Neuman conditions
                   IF(ie == dim+1) Lu(shift+iloc(1:nloc)) = du(ie,iloc(1:nloc),2)  !Neuman conditions
                ELSE IF( face(2) == 1  ) THEN                      ! Ymax face (entire face) 
                   IF(ie == 1) THEN
!!$                      Lu(shift+iloc(1:nloc)) = du(ie,iloc(1:nloc),2)  !Neuman conditions 
                      Lu(shift+iloc(1:nloc)) = u(shift+iloc(1:nloc))  !Dirichlet conditions
                   ELSE IF(ie == dim+1) THEN
                      Lu(shift+iloc(1:nloc)) = du(ie,iloc(1:nloc),2)  !Neuman conditions
                   ELSE
                      Lu(shift+iloc(1:nloc)) = u(shift+iloc(1:nloc))  !Dirichlet conditions
                   END IF
                END IF
             END IF
          END IF
       END DO
    END DO

  END SUBROUTINE user_algebraic_BC

  SUBROUTINE user_algebraic_BC_diag (Lu_diag, nlocal, ne_local, jlev, meth)
    !--Defines boundary condition type
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: jlev, meth, ne_local, nlocal
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (INOUT) :: Lu_diag
    REAL (pr), DIMENSION (nlocal,dim) :: du, d2u

    INTEGER :: i, ie, ii, shift
    ! REAL (pr), DIMENSION (nlocal,dim) :: du, d2u  !uncomment if Neuman BC are used
    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc
    
    !BC for both 2D and 3D cases

    !if Neuman BC are used, need to call 
    CALL c_diff_diag ( du, d2u, jlev, nlocal, meth, meth, 10)

    DO ie = 1, ne_local
       shift = nlocal*(ie-1)
       !--Go through all Boundary points that are specified
       i_p_face(0) = 1
       DO i=1,dim
          i_p_face(i) = i_p_face(i-1)*3
       END DO
       DO face_type = 0, 3**dim - 1
          face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
          IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
             CALL get_all_indices_by_face (face_type, jlev, nloc, iloc)
             IF(nloc > 0 ) THEN 
                IF( face(2) == -1 ) THEN                        ! Ymin face (entire face) 
                   Lu_diag(shift+iloc(1:nloc)) = 1.0_pr                !Dirichlet conditions
!!$                Lu_diag(shift+iloc(1:nloc)) = du(iloc(1:nloc),2)  !Neuman conditions
                   IF(ie == dim+1) Lu_diag(shift+iloc(1:nloc)) = du(iloc(1:nloc),2)  !Neuman conditions
                ELSE IF( face(2) == 1  ) THEN                    ! Ymax face (entire face) 
                   IF(ie == 1) THEN
!!$                      Lu_diag(shift+iloc(1:nloc)) = du(iloc(1:nloc),2)  !Neuman conditions 
                      Lu_diag(shift+iloc(1:nloc)) = 1.0_pr              !Dirichlet conditions
                   ELSE IF(ie == dim+1) THEN
                      Lu_diag(shift+iloc(1:nloc)) = du(iloc(1:nloc),2)  !Neuman conditions
                   ELSE
                      Lu_diag(shift+iloc(1:nloc)) = 1.0_pr              !Dirichlet conditions
                   END IF
                END IF
             END IF
          END IF
       END DO
    END DO
 
  END SUBROUTINE user_algebraic_BC_diag

  SUBROUTINE user_algebraic_BC_rhs (rhs, ne_local, nlocal, jlev)
    !--Sets rhs for boundary conditions
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: ne_local, nlocal, jlev
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (INOUT) :: rhs

    INTEGER :: i, ie, ii, shift
    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc

    !BC for both 2D and 3D cases

    DO ie = 1, ne_local
       shift = nlocal*(ie-1)
       !--Go through all Boundary points that are specified
       i_p_face(0) = 1
       DO i=1,dim
          i_p_face(i) = i_p_face(i-1)*3
       END DO
       DO face_type = 0, 3**dim - 1
          face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
          IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
             CALL get_all_indices_by_face (face_type, jlev, nloc, iloc)
             IF(nloc > 0 ) THEN 
                IF( face(2) == -1  ) THEN                    ! Ymin face (entire face) 
!!$                   rhs(shift+iloc(1:nloc)) = 0.0_pr  !Neuman conditions
                   rhs(shift+iloc(1:nloc)) = 0.0_pr  !Dirichlet conditions
                   IF(ie == dim+1) rhs(shift+iloc(1:nloc)) = 0.0_pr  !Neuman conditions
                ELSE IF( face(2) == 1 ) THEN                ! Ymax face (entire face) 
!!$                   IF(ie == 1) rhs(shift+iloc(1:nloc)) = 0.0_pr  !Neuman conditions
                   IF(ie == 1) THEN
                      rhs(shift+iloc(1:nloc)) = U_wall  !Dirichlet conditions
                   ELSE IF(ie == dim+1) THEN
                      rhs(shift+iloc(1:nloc)) = 0.0_pr  !Neuman conditions
                   ELSE
                      rhs(shift+iloc(1:nloc)) = 0.0_pr  !Dirichlet conditions
                   END IF
                END IF
             END IF
          END IF
       END DO
    END DO

  END SUBROUTINE user_algebraic_BC_rhs

  SUBROUTINE user_project (u, p, nlocal, meth)
    !--Makes u divergence free
    IMPLICIT NONE

    INTEGER, PARAMETER :: ne_local = 1
    INTEGER, INTENT (IN) :: meth, nlocal
    REAL (pr), DIMENSION (nlocal),     INTENT(INOUT) :: p
    REAL (pr), DIMENSION (nlocal,n_integrated), INTENT(INOUT) :: u
    REAL (pr), DIMENSION (nlocal,ne_local) :: dp
    REAL (pr), DIMENSION (nlocal,ne_local) :: f
    REAL (pr), DIMENSION(ne_local) :: scl_p
    INTEGER, DIMENSION(ne_local) :: clip

!!$    meth_p = meth
    !--Make u divergence free
    dp = 0.0_pr
    
    clip = 1 !clipping can be done for selective variables 

    f = Laplace_rhs (u(:,1:dim), nlocal, dim, meth)

    scl_p = dt*MAXVAL(scl_global(1:dim)) !scale of pressure increment based on dynamic pressure

    IF (verb_level.GT.0) &
         PRINT *, 'scl_p=',scl_p

    CALL Linsolve (dp, f , tol2, nlocal, ne_local, clip, Laplace, Laplace_diag, SCL=scl_p)  !

    u(:,1:dim) = u(:,1:dim) - grad(dp, nlocal, j_lev, meth_p+2)

    p = p + dp(:,1)/dt

    
    IF (verb_level.GT.0) THEN
       PRINT *,'After projection: div=',MAXVAL(ABS(Laplace_rhs (u(:,1:dim), nlocal, dim, meth)))
       PRINT *,' '
    END IF

  END SUBROUTINE user_project

  FUNCTION Laplace (jlev, u, nlocal, ne_local, meth_in)
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: jlev, nlocal, ne_local, meth_in
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (INOUT) :: u
    REAL (pr), DIMENSION (nlocal*ne_local) :: Laplace

    INTEGER :: i, ii, ie, meth, meth1, meth2, idim, shift
    REAL (pr), DIMENSION (ne_local, nlocal, dim) :: du
    REAL (pr), DIMENSION (MAX(ne_local,dim), nlocal, dim)      :: d2u
    REAL (pr), DIMENSION (MAX(ne_local,dim), nlocal, dim)      :: du_dummy ! passed when only calculating 1st derivative.
    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc

    meth  = MIN(meth_p,meth_in)
    meth1 = meth + 2
    meth2 = meth + 4

    CALL c_diff_fast (u, du, d2u(1:ne_local, 1:nlocal, 1:dim), jlev, nlocal, meth1, 10, ne_local, 1, ne_local)

    DO ie = 1, ne_local
       shift = nlocal*(ie-1)
       !--- Internal points
       CALL c_diff_fast(du(ie,1:nlocal,1:dim), d2u(1:dim, 1:nlocal, 1:dim), du_dummy(1:dim, 1:nlocal, 1:dim), &
            jlev, nlocal, meth2, 10, dim, 1, dim )
       idim = 1
       Laplace(shift+1:shift+Nwlt_lev(jlev,0)) = d2u( idim ,1:Nwlt_lev(jlev,0),idim)
       DO idim = 2,dim
          Laplace(shift+1:shift+Nwlt_lev(jlev,0)) = Laplace(shift+1:shift+Nwlt_lev(jlev,0)) + &
               d2u(idim ,1:Nwlt_lev(jlev,0),idim)
       END DO
       !--Boundary points
       i_p_face(0) = 1
       DO i=1,dim
          i_p_face(i) = i_p_face(i-1)*3
       END DO
       DO face_type = 0, 3**dim - 1
          face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
          IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
             CALL get_all_indices_by_face (face_type, jlev, nloc, iloc)
             IF(nloc > 0 ) THEN 
                IF( face(2) == -1 ) THEN                                ! Ymin face, entire face
                   !Laplace(shift+iloc(1:nloc)) = u(shift+iloc(1:nloc))  !Dirichlet conditions
                   Laplace(shift+iloc(1:nloc)) = du(ie,iloc(1:nloc),2)  !Neuman conditions
                ELSE IF( face(2) == 1 ) THEN                            ! Ymax face, entire face
                   !Laplace(shift+iloc(1:nloc)) = u(shift+iloc(1:nloc))  !Dirichlet conditions
                   Laplace(shift+iloc(1:nloc)) = du(ie,iloc(1:nloc),2)  !Neuman conditions
                END IF
             END IF
          END IF
       END DO
    END DO
!!$    IF( meth == LOW_ORDER) THEN !------------- overrides
!!$       CALL c_diff_fast (u, du, d2u(1:ne_local, 1:nlocal, 1:dim), jlev, nlocal, meth, 01, ne_local, 1, ne_local)
!!$       DO ie = 1, ne_local
!!$          shift = nlocal*(ie-1)
!!$          !--- Internal points
!!$          idim = 1
!!$          Laplace(shift+1:shift+Nwlt_lev(jlev,0)) = d2u( ie ,1:Nwlt_lev(jlev,0),idim)
!!$          DO idim = 2,dim
!!$             Laplace(shift+1:shift+Nwlt_lev(jlev,0)) = Laplace(shift+1:shift+Nwlt_lev(jlev,0)) + &
!!$                  d2u(ie ,1:Nwlt_lev(jlev,0),idim)
!!$          END DO
!!$       END DO
!!$    END IF

  END FUNCTION Laplace

  FUNCTION Laplace_diag (jlev,  nlocal, ne_local, meth_in)
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: jlev, nlocal, ne_local, meth_in
    REAL (pr), DIMENSION (nlocal*ne_local) :: Laplace_diag

    INTEGER :: i, ii, ie, meth, meth1, meth2, shift
    REAL (pr), DIMENSION (nlocal,dim) :: du, d2u
    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc

    meth  = MIN(meth_p,meth_in)
    meth1 = meth + 2
    meth2 = meth + 4

    !PRINT *,'IN Laplace_diag, ne_local = ', ne_local

    DO ie = 1, ne_local
       shift = nlocal*(ie-1)
       CALL c_diff_diag ( du, d2u, jlev, nlocal, meth1, meth2, -11)
       Laplace_diag(shift+1:shift+Nwlt_lev(jlev,0)) = d2u(1:Nwlt_lev(jlev,0),1) + d2u(1:Nwlt_lev(jlev,0),2)
       IF (dim==3) Laplace_diag(shift+1:shift+Nwlt_lev(jlev,0)) = &
            Laplace_diag(shift+1:shift+Nwlt_lev(jlev,0))+d2u(1:Nwlt_lev(jlev,0),3)
       !--Boundary points
       i_p_face(0) = 1
       DO i=1,dim
          i_p_face(i) = i_p_face(i-1)*3
       END DO
       DO face_type = 0, 3**dim - 1
          face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
          IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
             CALL get_all_indices_by_face (face_type, jlev, nloc, iloc)
             IF(nloc > 0 ) THEN ! 3-D case only
                IF( face(2) == -1 ) THEN                                     ! Ymin face, entire face
                   !Laplace_diag(shift+iloc(1:nloc)) = 1.0_pr                ! Dirichlet conditions
                   Laplace_diag(shift+iloc(1:nloc)) = du(iloc(1:nloc),2)     ! Neuman conditions
                ELSE IF( face(2) == 1 ) THEN                               ! Ymax face, entire face
                   !Laplace_diag(shift+iloc(1:nloc)) = 1.0_pr                ! Dirichlet conditions
                   Laplace_diag(shift+iloc(1:nloc)) = du(iloc(1:nloc),2)     ! Neuman conditions
                END IF
             END IF
          END IF
       END DO
    END DO
!!$    IF( meth == LOW_ORDER) THEN
!!$       CALL c_diff_diag ( du, d2u, jlev, nlocal, meth, meth, 01)
!!$       DO ie = 1, ne_local
!!$          shift = nlocal*(ie-1)
!!$          CALL c_diff_diag ( du, d2u, jlev, nlocal, meth1, meth2, -11)
!!$          Laplace_diag(shift+1:shift+Nwlt_lev(jlev,0)) = d2u(1:Nwlt_lev(jlev,0),1) + d2u(1:Nwlt_lev(jlev,0),2)
!!$          IF (dim==3) Laplace_diag(shift+1:shift+Nwlt_lev(jlev,0)) = &
!!$               Laplace_diag(shift+1:shift+Nwlt_lev(jlev,0))+d2u(1:Nwlt_lev(jlev,0),3)
!!$       END DO
!!$    END IF

  END FUNCTION Laplace_diag

  FUNCTION Laplace_rhs(u, nlocal, ne_local, meth_in)
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: nlocal, ne_local, meth_in
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (INOUT) :: u
    REAL (pr), DIMENSION (nlocal,ne_local) :: Laplace_rhs

    INTEGER :: i, ii, ie, meth, meth1, meth2, idim
    REAL (pr), DIMENSION (ne_local ,nlocal,dim) :: du
    REAL (pr), DIMENSION (ne_local*dim ,nlocal,dim) :: d2u
    REAL (pr), DIMENSION (ne_local*dim ,nlocal,dim) :: du_dummy ! passed when only calculating 1st derivative.
    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc

    meth  = MIN(meth_p,meth_in)
    meth1 = meth + 2
    meth2 = meth + 4

    Laplace_rhs(:,1) = div(u,nlocal,j_lev,meth2)

    !--Boundary points
    i_p_face(0) = 1
    DO i=1,dim
       i_p_face(i) = i_p_face(i-1)*3
    END DO
    DO face_type = 0, 3**dim - 1
       face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
       IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
          CALL get_all_indices_by_face (face_type, j_lev, nloc, iloc)
          IF(nloc > 0 ) THEN 
             IF( face(2) == -1 ) THEN                                ! Ymin face, entire face
                Laplace_rhs(iloc(1:nloc),1) = 0.0_pr  !Neuman conditions
             ELSE IF( face(2) == 1 ) THEN                            ! Ymax face, entire face
                Laplace_rhs(iloc(1:nloc),1) = 0.0_pr
             END IF
          END IF
       END IF
    END DO
    
  END FUNCTION Laplace_rhs



  FUNCTION user_rhs (u_integrated,p)	 
    IMPLICIT NONE
    REAL (pr), DIMENSION (ng,ne), INTENT(IN) :: u_integrated !1D flat version of integrated variables without BC 
    REAL (pr), DIMENSION (ng), INTENT(IN) :: p
    REAL (pr), DIMENSION (n) :: user_rhs


    INTEGER :: ie, shift, idim
    INTEGER, PARAMETER :: meth=1
    REAL (pr), DIMENSION (ne,ng,dim) :: du, d2u
    REAL (pr), DIMENSION (ng,dim)     :: dp


    dp = grad (p, ng, j_lev, meth_p+2)

    CALL c_diff_fast(u_integrated, du, d2u, j_lev, ng, 1, 11, ne, 1, ne)

    !--Form right hand side of Navier-Stokes equations
    DO ie = 1, ne
       shift = ng*(ie-1)
       !CALL c_diff_fast(u_integrated(1:ng,ie), du, d2u, j_lev, ng, 1, 11, 1, 1, 1)
       idim = 1
       IF(ie <= dim) THEN
          user_rhs(shift+1:shift+ng) = 1.0_pr/Re*SUM(d2u(ie,:,:),2) - dp(:,ie)
       ELSE IF(ie == dim+1) THEN     !scalar transport equation
          user_rhs(shift+1:shift+ng) = 1.0_pr/Re/Le*SUM(d2u(ie,:,:),2)
       END IF
       DO idim = 1, dim
          user_rhs(shift+1:shift+ng) = user_rhs(shift+1:shift+ng) &
                                     - (u_integrated(:,idim)+Umn(idim))*du(ie,:,idim)
       END DO
       IF(ie ==1) user_rhs(shift+1:shift+ng) = user_rhs(shift+1:shift+ng) - dPdx
    END DO

  !---------------- Algebraic BC are set up in user_algebraic_BC and they autmatically overwrite evolution BC

  END FUNCTION user_rhs

  ! find Jacobian of Right Hand Side of the problem
  FUNCTION user_Drhs (u, u_prev_timestep_loc, meth)
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: meth
	!u_prev_timestep passed local to cast it into 2dim array to work with vector derivatives
    REAL (pr), DIMENSION (ng,ne) :: u, u_prev_timestep_loc
    REAL (pr), DIMENSION (n) :: user_Drhs

 
    INTEGER :: ie, shift, idim
    REAL (pr), DIMENSION (2*ne,ng,dim) :: du  ! 1st derivatives for u (in du(1:ne,:,:)) and u_prev_timestep (in du(ne+1:2*ne,:,:))
    REAL (pr), DIMENSION (ne  ,ng,dim) :: d2u ! 2nd derivatives for u 
    REAL (pr), DIMENSION (ne  ,ng,dim) :: du_dummy ! passed when only calculating 1st derivative.
    !Find batter way to do this!! du_dummy with no storage..

    IF ( TYPE_DB .NE. DB_TYPE_LINES ) THEN
       ! find 1st and 2nd deriviative of u and
       CALL c_diff_fast(u, du(1:ne,:,:), d2u(1:ne,:,:), j_lev, ng, meth, 11, ne , 1, ne )
!du(1:ne,:,:) = dub(1:ne,:,:) 
       ! find only first deriviativ e  u_prev_timestep
       CALL c_diff_fast(u_prev_timestep_loc, du(ne+1:2*ne,:,:), du_dummy(1:ne,:,:), j_lev, ng, meth, 10, ne , 1, ne )

    ELSE !db_lines
       ! Load u and u_prev_timestep in to db
       ! update the db from u
       ! u(:, mn_var:mx_var) -> db%u(db_offset:mx_var-mn_var+1)
       ! db_offset = 1 
       CALL update_db_from_u(  u       , ng ,ne  , 1, ne, 1  ) !Load u
       CALL update_db_from_u(  u_prev_timestep_loc , ng ,ne  , 1, ne, ne+1  ) !Load u_prev_timestep_loc offset in db by ne+1

       ! find 1st and 2nd deriviative of u and
       ! find only first deriviativ e  u_prev_timestep
       CALL c_diff_fast_db(du, d2u, j_lev, ng, meth, 11, 2*ne,&
            1,   &  ! MIN(mn_varD,mn_varD2)
            2*ne,&  ! MAX(mx_varD,mx_varD2)
            1,   &  ! mn_varD  :   min variable in db that we are doing the 1st derivatives for.
            2*ne,&  ! mn_varD  :   max variable in db that we are doing the 1st derivatives for.
            1 ,  &  ! mn_varD2 :   min variable in db that we are doing the 2nd derivatives for.
            dim   )  ! mn_varD2 :   max variable in db that we are doing the 2nd derivatives for.


    END IF
    !CALL c_diff_fast_db(du_b, du_b, j_lev, ng, meth, 10, ne, 1, ne) !find 1st derivative u_b

    !--Form right hand side of Navier-Stokes equations
    DO ie = 1, ne
       shift = ng*(ie-1)
       !CALL c_diff_fast(u_integrated(1:ng,ie), du, d2u, j_lev, ng, 1, 11, 1, 1, 1)
       IF(ie <= dim) THEN
          user_Drhs(shift+1:shift+ng) = 1.0_pr/Re*SUM(d2u(ie,:,:),2)
       ELSE IF(ie == dim+1) THEN     !scalar transport equation
          user_Drhs(shift+1:shift+ng) = 1.0_pr/Re/Le*SUM(d2u(ie,:,:),2)
       END IF
       DO idim = 1, dim
          user_Drhs(shift+1:shift+ng) = user_Drhs(shift+1:shift+ng) &
                                      - (u_prev_timestep_loc(:,idim)+Umn(idim))*du(ie,:,idim) - u(:,idim)*du(ne+ie,:,idim) 
       END DO
    END DO

  END FUNCTION user_Drhs


  !
  ! Uses u_prev_timestep, which is global flat (1D ) array set in time_step_cn()
  !
  FUNCTION user_Drhs_diag (meth)
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: meth
    REAL (pr), DIMENSION (n) :: user_Drhs_diag

    INTEGER :: ie, shift, idim
    REAL (pr), DIMENSION (ng,dim) :: du, d2u
    REAL (pr), DIMENSION (ne,ng,dim) :: du_prev_timestep
    REAL (pr), DIMENSION (ne  ,ng,dim) :: du_dummy ! passed when only calculating 1st derivative.

    CALL c_diff_fast(u_prev_timestep, du_prev_timestep, du_dummy, j_lev, ng, meth, 10, ne, 1, ne)

    !
    ! does not rely on u so we can call it once here
    !
    CALL c_diff_diag(du, d2u, j_lev, ng, meth, meth, 11)
    
    !--Form right hand side of Navier--Stokes equations
    DO ie = 1, ne
       shift = ng*(ie-1)
       !CALL c_diff_fast(u_integrated(1:ng,ie), du, d2u, j_lev, ng, 1, 11, 1, 1, 1)
       IF(ie <= dim) THEN
          user_Drhs_diag(shift+1:shift+ng) = 1.0_pr/Re*SUM(d2u,2) - du_prev_timestep(ie,:,ie)
       ELSE IF(ie == dim+1) THEN     !scalar transport equation
          user_Drhs_diag(shift+1:shift+ng) = 1.0_pr/Re/Le*SUM(d2u,2)
       END IF
       DO idim = 1, dim
          user_Drhs_diag(shift+1:shift+ng) = user_Drhs_diag(shift+1:shift+ng) &
                                      - (u_prev_timestep(ng*(idim-1)+1:ng*idim)+Umn(idim))*du(:,idim)
       END DO
    END DO

  END FUNCTION user_Drhs_diag

  FUNCTION user_chi (nlocal, t_local )
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: nlocal
    REAL (pr), INTENT (IN) :: t_local
    REAL (pr), DIMENSION (nlocal) :: user_chi

    user_chi = 0.0_pr
  END FUNCTION user_chi


  !
  ! Calculate any statitics
  !
  ! startup_flag - 0 when adapting to IC,then 1 inmain integration loop
  !
  SUBROUTINE user_stats ( u, j_mn_local, startup_flag)
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: startup_flag
    INTEGER , INTENT (IN) :: j_mn_local 
    REAL (pr), DIMENSION (nwlt,1:n_var), INTENT (IN) :: u
  END SUBROUTINE user_stats


  SUBROUTINE user_cal_force (u, n, t_local, force, drag, lift)
    !--Calculates drag and lift on obstacle using penalization formula
    IMPLICIT NONE

    INTEGER, INTENT (IN) :: n
    REAL (pr), INTENT (IN) :: t_local
    REAL (pr), DIMENSION (dim), INTENT (INOUT) :: force
    REAL (pr), INTENT (OUT) :: drag, lift
    REAL (pr), DIMENSION (n,dim) :: u
    drag = 0.0_PR
    lift = 0.0_PR

    !
    ! There is no obstacle in flow
    !

  END SUBROUTINE user_cal_force

  !
  ! Read input from "case_name"_pde.inp file
  ! case_name is in string file_name read from command line
  ! in read_command_line_input()
  ! 
  SUBROUTINE user_read_input()
    IMPLICIT NONE

  call input_real ('Re',Re,'stop', &
       ' Re: Reynolds number')

  call input_real ('Le',Le,'stop', &
       ' Le: Lewis number')

  call input_real ('dPdx',dPdx,'stop', &
       ' dPdx: mean pressure gradient')

  call input_real ('U_wall',U_wall,'stop', &
       ' U_wall: velocity of the upper wall for the Couette flow')

  call input_integer ('alpha',alpha,'stop', &
       ' alpha: power exponent for initial velocity profile')

  call input_logical ('passive_scalar',passive_scalar,'stop', &
       ' passive_scalar: if .TRUE. solves in addition an equation for passive scalar')

  meth_p = LOW_ORDER !overides HIGH_ORDER in ellitpic solver

  END SUBROUTINE user_read_input



  !
  ! calculate any additional variables
  ! 
  ! arg 
  ! flag  - 0 calledwhile adapting to IC, 1 - called in main time integration loop
  !
  ! These additiona variables are calculated and left in real space.
  !
  SUBROUTINE user_additional_vars( t_local, flag )
    IMPLICIT NONE
    REAL (pr), INTENT (IN) ::  t_local
    INTEGER , INTENT(IN) :: flag ! 0- called during adaption to IC, 1 called during main integration loop


    ! Calculate the vorticity if we are in the main integration loop and we
    ! are saving the solution
    ! 
	! NOTE we do not calculate vorticity in initial adaptation because
	! derivatives are not setup yet (to save memmory)
	! This shoild be changed eventually DG
	!
!!$    IF (flag == 1 .AND. t  > twrite ) THEN 
!!$       CALL cal_vort (u(:,n0:n0+dim-1), u(:,n_var_vorticity:n_var_vorticity+3-MOD(dim,3)-1), nwlt)
!!$    END IF
!!$    PRINT * ,'Calc Vort '

  END SUBROUTINE user_additional_vars


  !
  ! calculate any additional scalar variables
  ! 
  ! arg 
  ! flag  - 0 calledwhile adapting to IC, 1 - called in main time integration loop
  !
  SUBROUTINE user_scalar_vars( flag )
    IMPLICIT NONE
    INTEGER , INTENT(IN) :: flag ! 0- called during adaption to IC, 1 called during main integration loop



  END SUBROUTINE user_scalar_vars

  !
  !************ Calculating Scales ***************************
  !
  ! Note the order of the components in the scl array
  ! correspond to u_tn, v_tn, w_tn, u_tn-1, v_tn-1, w_tn-1, u_tn-2, v_tn-2, w_tn-2
  ! 
 SUBROUTINE user_scales(flag, use_default, u_loc, nlocal, ne_local, l_n_var_adapt , l_n_var_adapt_index, &
       scl, scl_fltwt) !add
    USE precision
    USE pde
    USE util_vars
    USE parallel
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: flag ! 0 during initial adaptation, then 1 during time advancement
    LOGICAL , INTENT(INOUT) :: use_default
    INTEGER, INTENT (IN) :: nlocal, ne_local
    REAL (pr), DIMENSION (1:nlocal,1:ne_local), INTENT (IN) :: u_loc
    LOGICAL , INTENT (IN) :: l_n_var_adapt(ne_local)
    INTEGER , INTENT (IN) :: l_n_var_adapt_index(1:ne_local)
    REAL (pr), DIMENSION (1:ne_local), INTENT (INOUT) :: scl
    REAL (pr) ,  INTENT(IN) ::scl_fltwt !weight for temporal filter on scl
    REAL (pr) :: floor, tmp
    INTEGER :: ie, ie_index, itmp
    REAL (pr) ,save, ALLOCATABLE, DIMENSION (:) :: scl_old
    LOGICAL  ,save :: startup_init = .TRUE.

    !
    ! Ignore the output of this routine and use default scales routine
    !
    use_default = .FALSE. 
    !
    ! NOTE: For a parallel run, synchronize scl(:) across the processors in user_scales.
    !       Use the subroutine scales of default_util.f90 as an example,
    !       subroutine parallel_global_sum of parallel.f90 may be helpful.
    !       Already synchronized: sumdA_global

    !
    ! ALLOCATE scl_old if it was not done previously in user_scales
    !
    IF( startup_init .AND. .NOT. ALLOCATED(scl_old) ) THEN
        ALLOCATE( scl_old(1:ne_local) )
        scl_old = 0.0_pr
    END IF

    floor = 1.e-12_pr
    scl   =1.0_pr
    !
    ! Calculate scl per component
    !
    DO ie=1,ne_local
       IF(l_n_var_adapt(ie)) THEN
          ie_index = l_n_var_adapt_index(ie)
          IF( Scale_Meth == 1 ) THEN ! Use Linf scale
             scl(ie)= MAXVAL ( ABS( u_loc(1:nlocal,ie_index) ) )
             CALL parallel_global_sum( REALMAXVAL=scl(ie) )
             
          ELSE IF ( Scale_Meth == 2 ) THEN ! Use L2 scale
             scl(ie) = SUM( (u_loc(1:nlocal,ie_index)**2) )
             itmp = nlocal
             CALL parallel_global_sum( REAL=scl(ie) )
             CALL parallel_global_sum( INTEGER=itmp )
             scl(ie) = SQRT ( scl(ie)/itmp )

          ELSE IF ( Scale_Meth == 3) THEN ! Use L2 scale using dA
             scl(ie) = SUM( (u_loc(1:nlocal,ie_index)**2)*dA )
             CALL parallel_global_sum( REAL=scl(ie) )
             scl(ie) = SQRT ( scl(ie)/sumdA_global )

          ELSE
             IF (verb_level.GT.0.AND.par_rank.EQ.0) THEN
                PRINT *, 'Error in Scales(), Unknown Scale_Meth type: ', Scale_Meth
                PRINT *, 'Known Scale_Meth types: 1 = Linf scale, 2 = L2 scale '
                PRINT *, 'Exiting ...'
             END IF
             CALL parallel_finalize; STOP
          END IF
       END IF
    END DO

    !
    ! take appropriate vector norm over scl(1:dim)
    !
    IF( Scale_Meth == 1 ) THEN ! Use Linf scale
     tmp = MAXVAL(scl(1:dim))
     scl(1:dim) = tmp
    ELSE IF ( Scale_Meth == 2 ) THEN ! Use L2 scale
     tmp = SQRT(SUM( scl(1:dim)**2 ) )
     scl(1:dim) = tmp
    ELSE IF ( Scale_Meth == 3 ) THEN ! Use L2 scale using dA
     tmp = SQRT(SUM( scl(1:dim)**2 ) )
     scl(1:dim) = tmp
    END IF
 
    IF(ne_local == dim+1) scl(dim+1) = 1.0_pr
    !
    ! Print out new scl
    !
    DO ie=1,ne_local
       IF(l_n_var_adapt(ie)) THEN
          IF(scl(ie) .le. floor) scl(ie)=1.0_pr !Shouldn't this just set it to floor????
          tmp = scl(ie)
          ! temporally filter scl
          IF(.not. startup_init ) scl(ie) = scl_fltwt * scl_old(ie) + (1.0_pr - scl_fltwt) * scl(ie)
          scl_old(ie) = scl(ie) !save scl for this time step
          scl(ie) = scaleCoeff(ie) * scl(ie)
          IF (verb_level.GT.0.AND.par_rank.EQ.0) THEN
             WRITE (6,'("Scaling on vector(1:dim) magnitude")')
             WRITE (6,'("Scale factor eqn", I4 ," = ", es11.4 ," scalecoeff = ",es11.4 )') &
                  ie, scl(ie), scaleCoeff(ie)
             WRITE (6,'("(before filtering = ",es11.4,")" )') tmp
          END IF
       END IF
    END DO



    startup_init = .FALSE.
    !PRINT *,'TEST scl_old ', scl_old

  END SUBROUTINE user_scales

SUBROUTINE user_cal_cfl (use_default, u, cfl_out)
  USE precision
  USE sizes
  USE pde
  USE parallel
  IMPLICIT NONE
  LOGICAL , INTENT(INOUT) :: use_default
  REAL (pr),                                INTENT (INOUT) :: cfl_out
  REAL (pr), DIMENSION (nwlt,n_integrated), INTENT (IN)    :: u

  INTEGER                    :: i
  REAL (pr)                  :: floor
  REAL (pr), DIMENSION (dim) :: cfl
  REAL (pr), DIMENSION(dim,nwlt) :: h_arr

  use_default = .FALSE.

  floor = 1e-12_pr
  cfl_out = floor
  
  CALL get_all_local_h (h_arr)
  
  DO i = 1, nwlt
     cfl(1:dim) = ABS (u(i,1:dim)+Umn(1:dim)) * dt/h_arr(1:dim,i)
     cfl_out = MAX (cfl_out, MAXVAL(cfl))
  END DO
  CALL parallel_global_sum( REALMAXVAL=cfl_out )
  
END SUBROUTINE user_cal_cfl

!******************************************************************************************
!************************************* SGS MODEL ROUTINES *********************************
!******************************************************************************************

!
! Intialize sgs model
! This routine is called once in the first
! iteration of the main time integration loop.
! weights and model filters have been setup for first loop when this routine is called.
!
SUBROUTINE user_init_sgs_model( )
  IMPLICIT NONE


! LDM: Giuliano

! THE sgs forcign is stored for the time step in sgs_mdl_force(1:nlocal,1:n_integrated)
! where nlocal should be nwlt.


!          print *,'initializing LDM ...'       
!          CALL sgs_mdl_force( u(:,1:n_integrated), nwlt, j_lev, .TRUE.)


END SUBROUTINE user_init_sgs_model

!
! calculate sgs model forcing term
! user_sgs_force is called int he beginning of each times step in time_adv_cn().
! THE sgs forcign is stored for the time step in sgs_mdl_force(1:nlocal,1:n_integrated)
! where nlocal should be nwlt.
! 
! Accesses u from field module, 
!          j_lev from wlt_vars module,
!
SUBROUTINE  user_sgs_force (u_loc, nlocal)
  IMPLICIT NONE

  INTEGER,                         INTENT (IN) :: nlocal
  REAL (pr), DIMENSION (nlocal,n_integrated), INTENT (INOUT) :: u_loc

END SUBROUTINE  user_sgs_force

  FUNCTION user_sound_speed (u, neq, nwlt)
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: nwlt, neq
    REAL (pr), DIMENSION (nwlt,neq), INTENT(IN) :: u
    REAL (pr), DIMENSION (nwlt) :: user_sound_speed

    !user_sound_speed(:) = SQRT( gamma*(gamma-1.0_pr)* &
    !       ( u(:,4)-0.5_pr*SUM(u(:,n_var_mom(1:dim))**2,DIM=2)/u(:,1)) ) ! pressure

  END FUNCTION user_sound_speed

  SUBROUTINE  user_pre_process
    IMPLICIT NONE
     
  END SUBROUTINE user_pre_process

  SUBROUTINE  user_post_process
    IMPLICIT NONE
    
  END SUBROUTINE user_post_process

END MODULE user_case
