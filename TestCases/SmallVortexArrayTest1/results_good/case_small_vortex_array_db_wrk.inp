#------------------------------------------------------------------#
# General input file format                                        #
#                                                                  #
# comments start with # till the end of the line                   #
# string constant are quoted by "..." or '...'                     #
# boolean can be (T F 1 0 on off) in or without ' or " quotes      #
# integers are integers                                            #
# real numbers are whatever (e.g. 1   1.0   1e-23   1.123d-54 )    #
# vector elements are separated by commas                          #
# spaces between tokens are not important                          #
# empty or comment lines are not important                         #
# order of lines is not important                                  #
#------------------------------------------------------------------#

#file_gen = 'base.'
dimension = 3		#  dim (2,3), # of dimensions
IC_restart = F      #  ICrestart
IC_restart_station  = 0            #  ICrestart_station, after restart the next station number to use

IC_from_file = F    # Do a new run with IC from a restart file
IC_file_fmt = 0	    # IC data file format (0 - native restart file, 1-netcdf, 2-A.Wray in fourier space, 3-simple binary) next line is IC filename


 

IC_filename = ''



Data_file_format = F    #  T = formatted, F = unformatted

coord_min = -3.14159, -3.14159, -3.14159	#  XMIN, YMIN, ZMIN, etc
coord_max = 3.14159, 3.14159, 3.14159		#  XMAX, YMAX, ZMAX, etc

coord_zone_min = -5e+00, -5e+00, -5e+00		# XMINzone, etc
coord_zone_max = 5e+00, 5e+00, 5e+00		# XMAXzone, etc

eps_init = 1.0000000e-4  	#  EPS used to adapt initial grid  
eps_run = 1.0000000e-4  	#  EPS used in run  
eps_adapt_steps =  0            # eps_adapt_steps ! how many time steps to adapt from eps_init to eps_run

Scale_Meth = 1             	# Scale_Meth !1- Linf, 2-L2
Weights_Meth =      0           # Weights_Meth, 0: dA=area/nwlt, 1: normal
scl_fltwt = 0.0           	#  scl temporal filter weight, scl_new = scl_fltwt*scl_old + ( 1-scl_fltwt)*scl_new
j_mn_init = 2             	#  J_mn_init force J_mn == J_INIT while adapting to IC
j_lev_init = 2                  #  force j_lev == j_lev_init when initializing
j_IC = 5             	#  J_IC if the IC data does not have dimensions in it then mxyz(:)*2^(j_IC-1) is used
J_MN = 2             	#  J_MN
J_MX = 4             	#  J_MX
J_FILT = 20            	#  J_FILT

M_vector = 4,4,4	#  Mx, etc
periodic = 1,1,1	#  prd(:) (0/1) 0: non-periodic; 1: periodic
uniform = 0,0,0		#  grid(:) (0/1) 0: uniform; 1: non-uniform

i_h = 123456        	#  order of boundaries (1-xmin,2-xmax,3-ymin,4-ymax,5-zmin,6-zmax)
i_l = 111111        	#  algebraic/evolution (1/0) BC order: (lrbt)
N_predict = 3           #  N_predict
N_predict_low_order = 3 # 1 N_predict_low_order
N_update =3           	#  N_update
N_update_low_order = 3  # 0 N_update_low_order
N_diff = 3             		#  N_diff

IJ_ADJ = 1,1,1		# IJ_ADJ(-1:1) = (coarser level), (same level), (finer level)
ADJ_type = 0,0,0	#  ADJ_type(-1:1) = (coarser level), (same level), (finer level) # (0 - less conservative, 1 - more conservative)

BNDzone = F	  	#  BNDzone
BCtype = 0             	#  BCtype (0 - Dirichlet, 1 - Neuman, 2 - Mixed)


time_integration_method = 2 # 0- meth2, 1 -krylov, 2 - Crank Nicolson 

t_begin = 1.0000e+00    	#  tbeg  0.2500000e+00
t_end = 1.10000 	#  tend  1.50000e+00
dt = 1.0000000e-02 	    #  dt
dtmax = 1.0000000e-01 	#  dtmax
dtmin = 1.0000000e-08 	#  dtmin-if dt < dtmin then exection stops(likely blowing up)
dtwrite = 1.0000000e-04 	#  dtwrite 1.0000000e-02 
t_adapt = 2.0e-2        # when t > t_adapt use an adaptive time step if possible

cflmax = 1.0000000e-00 	#  cflmax
cflmin = 1.0000000e-08 	#  cflmin , Exit if cfl < cflmin

nu = 1.0000000e00  	    #  nu 1.0000000e-03, viscosity
nu1 = 1.0000000e-02 	#  nu1 5.0000000e-02
u0 = 0.010000e+00 	    #  u0
Zero_Mean = F           #  T- enforce zero mean for 1:dim first variables (velocity usually), F- do nothing
eta = 1.0000000e-04 	#  eta, alpha
theta = 0.0000000e+01 		#  theta (in degrees) (angle of 2D Burger's equation, ifn=3=> theta is angular velocity)
theta1 = -4.5000000e+01 	#  theta1 (in degrees) (angle of 2D Burger's equation)

diagnostics_elliptic =  F       #  diagnostics_elliptic: If T print full diagnostic for elliptic solver
GMRESflag = T              	#  GMRESflag
BiCGSTABflag = F              	#  BiCGSTABflag

Jacoby_correction = T           # Jacoby_correction
multigrid_correction = T        # multigrid_correction
GMRES_correction  = T           # GMRES_correction 
kry_p = 4			#  kry_p
kry_perturb = 3			#  kry_perturb
kry_p_coarse = 1000           	#  kry_p_coarse
len_bicgstab = 6             	#  len_bicgstab
len_bicgstab_coarse = 0	        #  len_bicgstab_coarse
len_iter = 5             	#  len_iter

W0 = 1.0000000e+00 	#  W0 underrelaxation factor for inner V-cycle
W1 = 1.0000000e+00 	#  W1 underrelaxation factor on the finest level
W2 = 0.6000000e+00 	#  W2 underrelaxation factor for weighted Jacoby (inner points)
W3 = 1.0000000e-00 	#  W3 underrelaxation factor for weighted Jacoby (boundary points)
W_min = 1.00000e-00	#  W_min correcton factor 
W_max = 1.00000e-00 	#  W_max correction factor 

obstacle = F              	# imask !TRUE  - there is an obstacle defined
itime = 3   	           	# itime ! 0- Euler first-order, 1- Rotational first-order, 2- Second-order, 3-Semi-implicit second-order
obstacle_X = 0,0,0 		#  Xo(:)! Location of obstacle
obstacle_U = 0,0,0		#  Uo(:)! Velocity of obstacle
obstacle_move = 0,0,0		#  1- Obstacle allowed to move in that direction, else == 0


diameter = 1.0		#  d diameter of cylinder as an obstacle
k_star = 8.7400000e+00 	#  k_star
m_star = 5.0000000e+00 	#  m_star
b_star = 0.0000000e-03 	#  b_star
tol1 = 1.0000000e-06 	#  tol1 used to set tolerence for non-solenoidal half-step
tol2 = 1.0000000e-04 	#  tol2 used to set tolerence for solenoidal half-step
tol3 = 1.0e-3           #  tol3 used to set tolerence for time step
tol_gmres = 1.0e-3        #  used to set tolerence for gmres iterative solver
tol_gmres_stop_if_larger = F  # If true stop iterating in gmres solver if error of last iteration was smaller
wlog = T              	#  wlog

dtforce = 0.0           	#  dtforce ! time interval for calculating the lift force
Pressure_force = 0,0,0		#  Pressure forcing term in x-dr

SGS_model = 0             	#  SGS model, 0=none, 1- fixed parameter Smagorinski

################## debugging and timing flags ############################
flag_timing = F                 # Print out extra timing information
debug_level = 2                 # 0- no debugging output, 1- some debugging, 2- more debugging
debug_zero_allocated_mem = T    # 
file_gen = 'case_small_vortex_array_db_wrk.' 	 	 #automatically added
