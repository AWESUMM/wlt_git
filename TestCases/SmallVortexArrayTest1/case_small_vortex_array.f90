
MODULE user_case
  ! CASE 2/3 D Vortex array


  USE precision
  USE debug_vars
  USE wlt_vars
  USE wavelet_filters_mod
  USE elliptic_mod
  USE elliptic_vars
  USE wlt_trns_mod
  USE wlt_trns_vars
  USE wlt_trns_util_mod
  USE io_3d_vars
  USE util_mod
  USE util_vars
  USE share_consts
  USE pde
  USE sizes
  USE share_kry
  USE vector_util_mod
  USE field
  USE input_file_reader
  USE variable_mapping

  !
  ! case specific variables
  !
  INTEGER :: &
       n_var_velocity, &  ! start of velocity in u array
       n_var_pressure, &  ! start of pressure in u array
       n_var_vorticity    ! start of vorticity in u array

  REAL (pr) :: nu ! kinematic viscosity (1/Re)


  ! debug flag to be read from .inp file to test database independent interpolation
  LOGICAL, PRIVATE :: test_interpolate

CONTAINS

  !
  ! In user_setup_pde() we setup how many variables are integrated, which are interpolated
  ! to the next times step and if any exeact solution exists to compare to during the run.
  ! We also set the variable names that are used when the result files are written.
  !
  ! The following variables must be setup in this routine:
  !
  !
  ! n_integrated     ! first n_integrated eqns will be acted on for time integration
  ! n_var_additional ! 
  ! n_var
  !
  !
  !
  !
  SUBROUTINE  user_setup_pde ( VERB )
    USE parallel
    IMPLICIT NONE
    LOGICAL, OPTIONAL :: VERB         ! print debug info
    LOGICAL :: do_verb
    INTEGER :: i

    do_verb = .TRUE.
    IF (PRESENT(VERB)) do_verb = VERB
    
    IF (do_verb) THEN
       PRINT * ,''
       PRINT *, '**********************Setting up PDE*****************'
       PRINT * ,'CASE VORTEX ARRAY '
       PRINT *, '*****************************************************'
    END IF


    


    !
    ! Fill in variable names for default variables for integration meth 2
    ! These may be changed in the call to the user specified routine
    !  user_setup_pde()
    !
    ! In integrated variables (3D)
    IF( dim == 3 ) THEN
       CALL register_var( 'Velocity_u_@t  ', &
            integrated=.TRUE.,  adapt=(/.TRUE.,.TRUE./),   interpolate=(/.TRUE.,.TRUE./),   exact=(/.TRUE.,.TRUE./),   saved=.TRUE., req_restart=.TRUE.) 
       CALL register_var( 'Velocity_v_@t  ', &
            integrated=.TRUE.,  adapt=(/.TRUE.,.TRUE./),   interpolate=(/.TRUE.,.TRUE./),   exact=(/.TRUE.,.TRUE./),   saved=.TRUE., req_restart=.TRUE.) 
       CALL register_var( 'Velocity_w_@t  ', &
            integrated=.TRUE.,  adapt=(/.TRUE.,.TRUE./),   interpolate=(/.FALSE.,.TRUE./),  exact=(/.TRUE.,.TRUE./),   saved=.TRUE., req_restart=.TRUE.)
       CALL register_var( 'Pressure  ', &
            integrated=.FALSE., adapt=(/.FALSE.,.FALSE./), interpolate=(/.FALSE.,.TRUE./),  exact=(/.FALSE.,.FALSE./), saved=.TRUE., req_restart=.FALSE.)
       CALL register_var( 'Vorticity_u  ', &
            integrated=.FALSE., adapt=(/.FALSE.,.FALSE./), interpolate=(/.FALSE.,.FALSE./),  exact=(/.FALSE.,.FALSE./), saved=.TRUE., req_restart=.FALSE.)
       CALL register_var( 'Vorticity_v  ', &
            integrated=.FALSE., adapt=(/.FALSE.,.FALSE./), interpolate=(/.FALSE.,.FALSE./),  exact=(/.FALSE.,.FALSE./), saved=.TRUE., req_restart=.FALSE.)
       CALL register_var( 'Vorticity_w  ', &
            integrated=.FALSE., adapt=(/.FALSE.,.FALSE./), interpolate=(/.FALSE.,.FALSE./),  exact=(/.FALSE.,.FALSE./), saved=.TRUE., req_restart=.FALSE.)
       
    ELSE IF(dim == 2) THEN
       CALL register_var( 'Velocity_u_@t  ', &
            integrated=.TRUE.,  adapt=(/.TRUE.,.TRUE./),   interpolate=(/.TRUE.,.TRUE./),   exact=(/.TRUE.,.TRUE./),   saved=.TRUE., req_restart=.TRUE.) 
       CALL register_var( 'Velocity_v_@t  ', &
            integrated=.TRUE.,  adapt=(/.TRUE.,.TRUE./),   interpolate=(/.TRUE.,.TRUE./),   exact=(/.TRUE.,.TRUE./),   saved=.TRUE., req_restart=.TRUE.) 
       CALL register_var( 'Pressure  ', &
            integrated=.FALSE., adapt=(/.FALSE.,.FALSE./), interpolate=(/.TRUE.,.TRUE./),  exact=(/.FALSE.,.FALSE./), saved=.TRUE., req_restart=.FALSE.)
       CALL register_var( 'Vorticity_u  ', &
            integrated=.FALSE., adapt=(/.FALSE.,.FALSE./), interpolate=(/.FALSE.,.FALSE./),  exact=(/.FALSE.,.FALSE./), saved=.TRUE., req_restart=.FALSE.)
       CALL register_var( 'Vorticity_v  ', &
            integrated=.FALSE., adapt=(/.FALSE.,.FALSE./), interpolate=(/.FALSE.,.FALSE./),  exact=(/.FALSE.,.FALSE./), saved=.TRUE., req_restart=.FALSE.)
       
    ELSE
       PRINT *, 'this dimensionality has not been setup: dim=',dim
       CALL parallel_finalize; STOP 'in user_setup_pde'
    END IF


    CALL setup_mapping()
    CALL print_variable_registery( FULL=.TRUE.)

    n_var_velocity  = get_index('Velocity_u_@t  ')
    n_var_pressure  = get_index('Pressure  ')
    n_var_vorticity = get_index('Vorticity_u  ')


    ALLOCATE ( Umn(1:dim) )
    Umn = 0.0_pr !set up here if mean quantities anre not zero and used in scales or equation
    
    
    IF (verb_level.GT.0.AND.do_verb) THEN
       PRINT *, 'n_integrated = ',n_integrated 
       PRINT *, 'n_var = ',n_var 
       PRINT *, 'n_var_exact = ',n_var_exact 
       PRINT *, '*******************Variable Names*******************'
       DO i = 1,n_var
          WRITE (*, u_variable_names_fmt) u_variable_names(i)
       END DO
       PRINT *, '****************************************************'
    END IF

  END SUBROUTINE  user_setup_pde


  !
  ! Set the exact solution for comparison to the simulated solution
  !
  ! u          - array to fill in the exact solution
  ! nlocal       - number of active wavelets   
  ! ne_local        - total number of equations
  ! t          - time of current time step 
  ! l_n_var_exact_soln_index - index into the elements of u for which we need to 
  !                            find the exact solution
  SUBROUTINE  user_exact_soln (u, nlocal,  t_local, l_n_var_exact_soln)
    USE parallel
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: nlocal
    REAL (pr), INTENT (IN) ::  t_local
    REAL (pr), DIMENSION (nlocal,n_var_exact), INTENT (INOUT) :: u
    LOGICAL , INTENT (IN) :: l_n_var_exact_soln(n_var)
    INTEGER :: i
    
    IF ((dim.NE.2).AND.(dim.NE.3)) THEN
       PRINT *, 'this dimensionality has not been setup: dim=',dim
       CALL parallel_finalize; STOP 'in user_exact_soln'
    END IF

    !u component of velocity
    u(:,1) = -COS(x(:,1))*SIN(x(:,2))*EXP(-2.0_pr*t_local)
    !v component of velocity
    u(:,2) =  SIN(x(:,1))*COS(x(:,2))*EXP(-2.0_pr*t_local)
    !w component of velocity
    IF(dim == 3) u(:,3) = 0.0_pr



    !WRITE( *,'( "in user_exact_soln t_local, nwlt " , f30.20, 1x , i8.8 )' ) t_local , nlocal
   

  END SUBROUTINE  user_exact_soln

  SUBROUTINE user_initial_conditions (u, nlocal, ne_local, t_local, scl, scl_fltwt, iter)
    USE parallel
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: nlocal, ne_local
    INTEGER  , INTENT (INOUT) :: iter ! iteration of call while adapting initial grid
    REAL (pr), DIMENSION (nlocal,ne_local), INTENT (INOUT) :: u
    REAL (pr)  :: scl(1:n_var),scl_fltwt
    REAL (pr), INTENT (IN) :: t_local


    INTEGER :: i, ii, seed(2) !TEST

    IF( .NOT. (IC_restart .OR. IC_from_file )) THEN

       IF ((dim.NE.2).AND.(dim.NE.3)) THEN
          PRINT *, 'this dimensionality has not been setup: dim=',dim
          CALL parallel_finalize; STOP 'in user_initial_conditions'
       END IF
       
       u(:,1) = -COS(x(:,1))*SIN(x(:,2))*EXP(-2.0_pr*t_local)
       u(:,2) =  SIN(x(:,1))*COS(x(:,2))*EXP(-2.0_pr*t_local)
       IF(dim == 3) THEN
          u(:,3) = 0.0_pr
       END IF
       
       IF (verb_level.GT.0) &
            WRITE( *,'( "in user_IC t_local, nwlt " , f30.20, 1x , i5.5 )' ) t_local , nlocal
       !END IF
       !
       ! Set Mean flow
       !
       Umn = 0.0_pr


!!$       seed(1)=123456789; seed(2)=987654321
!!$       u = 0.0_pr
!!$       CALL RANDOM_SEED(PUT=seed)
!!$       DO ii=1,nlocal
!!$          DO i=1,dim
!!$             CALL RANDOM_NUMBER(u(ii,i))
!!$             IF (u(ii,i).GT.0.9) THEN
!!$                !PRINT *, 'U===',u(i,1)
!!$                u(ii,i) = 100*(u(ii,i)-0.5_pr)
!!$             END IF
!!$          END DO
!!$       END DO


    END IF
    
  END SUBROUTINE user_initial_conditions
  
!--********************************  
  ! Arguments
  ! u         - field on adaptive grid
  ! nlocal      - number of active points
  ! ne_local       - number of equations
  ! t         - current time
  !
  ! Global variables used
  ! ifn       - number of equations
  ! x()
  ! xO
  ! y0
  !--******************************** 
  SUBROUTINE user_algebraic_BC (Lu, u, nlocal, ne_local, jlev, meth)
    !--Defines boundary condition type
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: jlev, meth, ne_local, nlocal
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (INOUT) :: Lu
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (IN)    :: u

    INTEGER :: ie, ii, shift
    REAL (pr), DIMENSION (nlocal,ne_local) :: du, d2u


    !
    ! There are periodic BC conditions
    !


  END SUBROUTINE user_algebraic_BC

  SUBROUTINE user_algebraic_BC_diag (Lu, nlocal, ne_local, jlev, meth)
    !--Defines boundary condition type
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: jlev, meth, ne_local, nlocal
    REAL (pr), DIMENSION (nlocal,ne_local), INTENT (INOUT) :: Lu

    INTEGER :: ie, ii, shift
    REAL (pr), DIMENSION (nlocal,ne_local) :: du, d2u

    !
    ! There are periodic BC conditions
    !


 
  END SUBROUTINE user_algebraic_BC_diag



  SUBROUTINE user_algebraic_BC_rhs (rhs, ne_local, nlocal, jlev)
    !--Sets rhs for boundary conditions
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: ne_local, nlocal, jlev
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (INOUT) :: rhs

    INTEGER :: ie, ii, shift
    !
    ! There are periodic BC conditions
    !

  END SUBROUTINE user_algebraic_BC_rhs


  FUNCTION Laplace (jlev, u, nlocal, ne_local, meth)
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: jlev, nlocal, ne_local, meth
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (INOUT) :: u
    REAL (pr), DIMENSION (nlocal*ne_local) :: Laplace

    INTEGER :: i, ii, ie, shift, meth1, meth2, idim
    REAL (pr), DIMENSION (ne_local ,nlocal,dim) :: du
    REAL (pr), DIMENSION (ne_local*dim ,nlocal,dim) :: d2u
    REAL (pr), DIMENSION (ne_local*dim ,nlocal,dim) :: du_dummy ! passed when only calculating 1st derivative.
    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc

    meth1 = meth + 2
    meth2 = meth + 4

    !
    ! Find 1st deriviative of u. 
    !
    CALL c_diff_fast (u, du, d2u, jlev, nlocal, meth1, 10, ne_local, 1, ne_local)
    !PRINT *,'Laplace du', MINVAL(du), MAXVAL(du)

    !
    ! Find 2nd deriviative of u.  d( du ) 
    !
    IF ( TYPE_DB .NE. DB_TYPE_LINES ) THEN 
       d2u = 0.0_pr
       CALL c_diff_fast(du, d2u, du_dummy, jlev, nlocal, meth2, 10, ne_local*dim, 1, ne_local*dim )
       
    ELSE !db_lines
       ! Load du  into db
       DO ie=1,ne_local		
          CALL update_db_from_u(  du(ie,1:nlocal,1:dim)  , nlocal ,dim  , 1, dim, 1+(ne_local-1)*dim  ) !Load du	    
       END DO
       ! find  2nd deriviative of u (so we first derivative of du/dx)
       CALL c_diff_fast_db(d2u, du_dummy, jlev, nlocal, meth2, 10, ne_local*dim,&
            1,         &  ! MIN(mn_varD,mn_varD2)
            ne_local*dim,    &  ! MAX(mx_varD,mx_varD2)
            1,         &  ! mn_varD  :   min variable in db that we are doing the 1st derivatives for.
            ne_local*dim,    &  ! mn_varD  :   max variable in db that we are doing the 1st derivatives for.
            0 ,        &  ! mn_varD2 :   min variable in db that we are doing the 2nd derivatives for.
            0     )  ! mn_varD2 :   max variable in db that we are doing the 2nd derivatives for.
       !PRINT *,'Laplace d2u', MINVAL(d2u), MAXVAL(d2u)
    END IF
    ! Now:
    ! d2u(1,:,1) = d^2 U/ d^2 x
    ! d2u(1,:,2) = d^2 U/ d^2 y
    ! d2u(1,:,3) = d^2 U/ d^2 z
    ! d2u(2,:,1) = d^2 V/ d^2 x
    ! d2u(2,:,2) = d^2 V/ d^2 y
    ! d2u(2,:,3) = d^2 V/ d^2 z
    ! d2u(3,:,1) = d^2 W/ d^2 x
    ! d2u(3,:,2) = d^2 W/ d^2 y
    ! d2u(3,:,3) = d^2 W/ d^2 z

    DO ie = 1, ne_local
       shift=(ie-1)*nlocal
       !--- Internal points
       !--- div(grad)
       idim = 1
       Laplace(shift+1:shift+Nwlt_lev(jlev,0)) = d2u( (ie-1)*dim+idim ,1:Nwlt_lev(jlev,0),1)
       DO idim = 2,dim
          Laplace(shift+1:shift+Nwlt_lev(jlev,0)) = Laplace(shift+1:shift+Nwlt_lev(jlev,0)) + &
               d2u((ie-1)*dim+idim ,1:Nwlt_lev(jlev,0),idim)
       END DO
       !--Boundary points
       i_p_face(0) = 1
       DO i=1,dim
          i_p_face(i) = i_p_face(i-1)*3
       END DO
       DO face_type = 0, 3**dim - 1
          face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
          IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
             CALL get_all_indices_by_face (face_type, jlev, nloc, iloc)
             IF(nloc > 0 .AND. dim == 3) THEN ! 3-D case only
                IF( ALL( face == (/-1,0,0/) ) ) THEN  ! Xmin face, no edges or corners
                   Laplace(shift+iloc(1:nloc)) = u(shift+iloc(1:nloc))  !Dirichlet conditions
                ELSE IF( ALL( face == (/1,0,0/) ) ) THEN  ! Xmax face, no edges or corners
                   Laplace(shift+iloc(1:nloc)) = u(shift+iloc(1:nloc))  !Dirichlet conditions
!!$                Laplace(shift+iloc(1:nloc)) = du(ie,iloc(1:nloc),1)  !Neuman conditions
                ELSE IF( ALL( face(2:dim) == (/-1,0/) ) ) THEN  ! Ymin face, edges || to Z, no corners
                   Laplace(shift+iloc(1:nloc)) = u(shift+iloc(1:nloc))  !Dirichlet conditions
!!$                Laplace(shift+iloc(1:nloc)) = du(ie,iloc(1:nloc),2)  !Neuman conditions
                ELSE IF( ALL( face(2:dim) == (/1,0/) ) ) THEN  ! Ymax face, edges || to Z, no corners
                   Laplace(shift+iloc(1:nloc)) = u(shift+iloc(1:nloc))  !Dirichlet conditions
!!$                Laplace(shift+iloc(1:nloc)) = du(ie,iloc(1:nloc),2)  !Neuman conditions
                ELSE IF( face(3) == -1 ) THEN  ! entire Zmin face
                   Laplace(shift+iloc(1:nloc)) = u(shift+iloc(1:nloc))  !Dirichlet conditions
!!$                Laplace(shift+iloc(1:nloc)) = du(ie,iloc(1:nloc),3)  !Neuman conditions
                ELSE IF( face(3) == 1 ) THEN  ! entire Zmax face
                   Laplace(shift+iloc(1:nloc)) = u(shift+iloc(1:nloc))  !Dirichlet conditions
!!$                Laplace(shift+iloc(1:nloc)) = du(ie,iloc(1:nloc),3)  !Neuman conditions
                END IF
             END IF
          END IF
       END DO
    END DO
  END FUNCTION Laplace

  FUNCTION Laplace_diag (jlev,  nlocal, ne_local, meth)
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: jlev, nlocal, ne_local, meth
    REAL (pr), DIMENSION (nlocal*ne_local) :: Laplace_diag

    INTEGER :: i, ii, ie, shift, meth1, meth2
    REAL (pr), DIMENSION (nlocal,dim) :: du, d2u
    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc

    meth1 = meth + 2
    meth2 = meth + 4


!PRINT *,'IN Laplace_diag, ne_local = ', ne_local

    DO ie = 1, ne_local
       shift=(ie-1)*nlocal
       !--- div(grad)
!PRINT *,'CAlling c_diff_diag from Laplace_diag() '
!PRINT *,'--- jlev, nlocal, meth1, meth2', jlev, nlocal, meth1, meth2

       CALL c_diff_diag ( du, d2u, jlev, nlocal, meth1, meth2, -11)

       Laplace_diag(shift+1:shift+Nwlt_lev(jlev,0)) = d2u(1:Nwlt_lev(jlev,0),1) + d2u(1:Nwlt_lev(jlev,0),2)
       IF (dim==3) Laplace_diag(shift+1:shift+Nwlt_lev(jlev,0)) = &
            Laplace_diag(shift+1:shift+Nwlt_lev(jlev,0))+d2u(1:Nwlt_lev(jlev,0),3)

       !--Boundary points
       i_p_face(0) = 1
       DO i=1,dim
          i_p_face(i) = i_p_face(i-1)*3
       END DO
       DO face_type = 0, 3**dim - 1
          face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
          IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
             CALL get_all_indices_by_face (face_type, jlev, nloc, iloc)
             IF(nloc > 0 .AND. dim == 3) THEN ! 3-D case only
                IF( ALL( face == (/-1,0,0/) ) ) THEN                         ! Xmin face, no edges or corners
                   Laplace_diag(shift+iloc(1:nloc)) = 1.0_pr                 !Dirichlet conditions
                ELSE IF( ALL( face == (/1,0,0/) ) ) THEN                     ! Xmax face, no edges or corners
                   Laplace_diag(shift+iloc(1:nloc)) = 1.0_pr                 !Dirichlet conditions
!!$                Laplace_diag(shift+iloc(1:nloc)) = du(iloc(1:nloc),1)     !Neuman conditions
                ELSE IF( ALL( face(2:dim) == (/-1,0/) ) ) THEN               ! Ymin face, edges || to Z, no corners
                   Laplace_diag(shift+iloc(1:nloc)) = 1.0_pr                 !Dirichlet conditions
!!$                Laplace_diag(shift+iloc(1:nloc)) = du(iloc(1:nloc),2)     !Neuman conditions
                ELSE IF( ALL( face(2:dim) == (/1,0/) ) ) THEN                ! Ymax face, edges || to Z, no corners
                   Laplace_diag(shift+iloc(1:nloc)) = 1.0_pr                 !Dirichlet conditions
!!$                Laplace_diag(shift+iloc(1:nloc)) = du(iloc(1:nloc),2)     !Neuman conditions
                ELSE IF( face(3) == -1 ) THEN                                ! entire Zmin face
                   Laplace_diag(shift+iloc(1:nloc)) = 1.0_pr                 !Dirichlet conditions
!!$                Laplace_diag(shift+iloc(1:nloc)) = du(iloc(1:nloc),3)     !Neuman conditions
                ELSE IF( face(3) == 1 ) THEN                                 ! entire Zmax face
                   Laplace_diag(shift+iloc(1:nloc)) = 1.0_pr                 !Dirichlet conditions
!!$                Laplace_diag(shift+iloc(1:nloc)) = du(iloc(1:nloc),3)     !Neuman conditions
                END IF
             END IF
          END IF
       END DO
    END DO
  END FUNCTION Laplace_diag



  FUNCTION user_rhs (u_integrated,p)
    USE parallel
    IMPLICIT NONE
    REAL (pr), DIMENSION (ng,ne), INTENT(IN) :: u_integrated !1D flat version of integrated variables without BC 
    REAL (pr), DIMENSION (ng), INTENT(IN) :: p
    REAL (pr), DIMENSION (n) :: user_rhs

    INTEGER :: ie, shift
    INTEGER, PARAMETER :: meth=1
    REAL (pr), DIMENSION (ne,ng,dim) :: du, d2u
    REAL (pr), DIMENSION (ng,dim)     :: dp


    dp = grad (p, ng, j_lev, meth+2)

	CALL c_diff_fast(u_integrated, du, d2u, j_lev, ng, 1, 11, ne, 1, ne)

    !--Form right hand side of Navier-Stokes equations
    IF (dim==2) THEN
       DO ie = 1, 2
          shift=(ie-1)*ng
          !CALL c_diff_fast(u_integrated(shift+1:shift+ng), du, d2u, j_lev, ng, 1, 11, 1, 1, 1)
          user_rhs(shift+1:shift+ng) = - (u_integrated(:,1)+Umn(1))*du(ie,:,1) - &
			  (u_integrated(:,2)+Umn(2))*du(ie,:,2) + nu*SUM(d2u(ie,:,:),2) - dp(:,ie)
       END DO
    ELSE IF (dim==3) THEN
       DO ie = 1, 3
          shift=(ie-1)*ng
          !CALL c_diff_fast(u_integrated(shift+1:shift+ng), du, d2u, j_lev, nwlt, 1, 11, 1, 1, 1)
          user_rhs(shift+1:shift+ng) = - (u_integrated(:,1)+Umn(1))*du(ie,:,1) - (u_integrated(:,2)+Umn(2))*du(ie,:,2) - &
			  (u_integrated(:,3)+Umn(3))*du(ie,:,3) &
               + nu*SUM(d2u(ie,:,:),2) - dp(:,ie)
       END DO
    ELSE
       PRINT *, 'this dimensionality has not been setup: dim=',dim
       CALL parallel_finalize; STOP 'in user_rhs'
    END IF

    !--Set operator on boundaries
    IF( Nwlt_lev(j_lev,1) >  Nwlt_lev(j_lev,0) ) &
         CALL user_algebraic_BC_rhs (user_rhs, ne, ng, j_lev)
    
  END FUNCTION user_rhs

  ! find Jacobian of Right Hand Side of the problem
  FUNCTION user_Drhs (u, u_prev_timestep_loc, meth)
    USE parallel
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: meth
	!u_prev_timestep passed local to cast it into 2dim array to work with vector derivatives
    REAL (pr), DIMENSION (ng,ne) :: u, u_prev_timestep_loc
    REAL (pr), DIMENSION (n) :: user_Drhs

    INTEGER :: ie, shift
    REAL (pr), DIMENSION (2*ne,ng,dim) :: du  ! 1st derivatives for u (in du(1:ne,:,:)) and u_prev_timestep (in du(ne+1:2*ne,:,:))
    REAL (pr), DIMENSION (ne  ,ng,dim) :: d2u ! 2nd derivatives for u 
    REAL (pr), DIMENSION (ne  ,ng,dim) :: du_dummy ! passed when only calculating 1st derivative.
    !Find better way to do this!! du_dummy with no storage..

    LOGICAL :: hold_debug_zero_db ! debug only
    
    
    IF ( TYPE_DB .NE. DB_TYPE_LINES ) THEN
       ! find 1st and 2nd deriviative of u
       CALL c_diff_fast(u, du(1:ne,:,:), d2u(1:ne,:,:), j_lev, ng, meth, 11, ne , 1, ne )
       
       !du(1:ne,:,:) = dub(1:ne,:,:) 
       ! find only first deriviativ e  u_prev_timestep
       CALL c_diff_fast(u_prev_timestep_loc, du(ne+1:2*ne,:,:), du_dummy(1:ne,:,:), j_lev, ng, meth, 10, ne , 1, ne )
       
    ELSE !db_lines
       ! Load u and u_prev_timestep in to db
       ! update the db from u
       ! u(:, mn_var:mx_var) -> db%u(db_offset:mx_var-mn_var+1)
       ! db_offset = 1 
       
       CALL update_db_from_u(  u       , ng ,ne  , 1, ne, 1  ) !Load u
       
       hold_debug_zero_db=debug_zero_db  !debug
       debug_zero_db = .FALSE. ! db was already zeroed out above.. Cant do it again or we loose values	  
       CALL update_db_from_u(  u_prev_timestep_loc , ng ,ne  , 1, ne, ne+1  ) !Load u_prev_timestep_loc offset in db by ne+1
       debug_zero_db=hold_debug_zero_db !debug
       ! find 1st and 2nd deriviative of u and
       ! find only first deriviativ e  u_prev_timestep
       CALL c_diff_fast_db(du, d2u, j_lev, ng, meth, 11, 2*ne,&
            1,   &  ! MIN(mn_varD,mn_varD2)
            2*ne,&  ! MAX(mx_varD,mx_varD2)
            1,   &  ! mn_varD  :   min variable in db that we are doing the 1st derivatives for.
            2*ne,&  ! mn_varD  :   max variable in db that we are doing the 1st derivatives for.
            1 ,  &  ! mn_varD2 :   min variable in db that we are doing the 2nd derivatives for.
            dim   )  ! mn_varD2 :   max variable in db that we are doing the 2nd derivatives for.
    END IF
    
    !PRINT *, 'in user_Drhs after derivatives:'
    !PRINT *, 'MINMAX(u)=', MINVAL(u), MAXVAL(u)
    !PRINT *, 'MINMAX(u_prev_timestep_loc)=', MINVAL(u_prev_timestep_loc), MAXVAL(u_prev_timestep_loc)

    !CALL c_diff_fast_db(du_b, du_b, j_lev, ng, meth, 10, ne, 1, ne) !find 1st derivative u_b

    !--Form right hand side of Navier--Stokes equations
    IF (dim==2) THEN
       DO ie = 1, 2
          shift=(ie-1)*ng

          !CALL c_diff_fast(u(shift+1:shift+ng), du, d2u, j_lev, ng, meth, 11, 1, 1, 1)
          user_Drhs(shift+1:shift+ng) = - (u_prev_timestep_loc(:,1)+Umn(1))*du(ie,:,1) - &
			(u_prev_timestep_loc(:,2)+Umn(2))*du(ie,:,2) + &
			 nu*SUM(d2u(ie,:,:),2)

          !CALL c_diff_fast(u_prev_timestep(shift+1:shift+ng), du, d2u, j_lev, ng, meth, 10, 1, 1, 1)
          user_Drhs(shift+1:shift+ng) = user_Drhs(shift+1:shift+ng) - u(:,1)*du(ne+ie,:,1) - u(:,2)*du(ne+ie,:,2) 
       END DO
    ELSE IF (dim==3) THEN
       DO ie = 1, 3
          shift=(ie-1)*ng

          !CALL c_diff_fast(u(shift+1:shift+ng), du, d2u, j_lev, ng, meth, 11, 1, 1, 1)
          user_Drhs(shift+1:shift+ng) = - (u_prev_timestep_loc(:,1)+Umn(1))*du(ie,:,1) - &
			(u_prev_timestep_loc(:,2)+Umn(2))*du(ie,:,2) &
               - (u_prev_timestep_loc(:,3)+Umn(3))*du(ie,:,3) + nu*SUM(d2u(ie,:,:),2)

          !CALL c_diff_fast(u_prev_timestep(shift+1:shift+ng), du, d2u, j_lev, ng, meth, 10, 1, 1, 1)
          user_Drhs(shift+1:shift+ng) = user_Drhs(shift+1:shift+ng) - u(:,1)*du(ne+ie,:,1) - u(:,2)*du(ne+ie,:,2) &
			- u(:,3)*du(ne+ie,:,3)
       END DO
    ELSE
       PRINT *, 'this dimensionality has not been setup: dim=',dim
       CALL parallel_finalize; STOP 'in user_Drhs'
    END IF
  END FUNCTION user_Drhs


  !
  ! Uses u_prev_timestep, which is global flat (1D ) array set in time_step_cn()
  !
  FUNCTION user_Drhs_diag (meth)
    USE parallel
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: meth
    REAL (pr), DIMENSION (n) :: user_Drhs_diag

    INTEGER :: ie, shift
    REAL (pr), DIMENSION (ng,dim) :: du, d2u
    REAL (pr), DIMENSION (ne,ng,dim) :: du_prev_timestep
    REAL (pr), DIMENSION (ne  ,ng,dim) :: du_dummy ! passed when only calculating 1st derivative.

    CALL c_diff_fast(u_prev_timestep, du_prev_timestep, du_dummy, j_lev, ng, meth, 10, ne, 1, ne)

	!
	! does not rely on u so we can call it once here
	!
	shift = 0 !tmp
	CALL c_diff_diag(du, d2u, j_lev, ng, meth, meth, 11)

    !--Form right hand side of Navier--Stokes equations
    IF (dim==2) THEN
       DO ie = 1, 2
          shift=(ie-1)*ng

          user_Drhs_diag(shift+1:shift+ng) = - (u_prev_timestep(1:ng)+Umn(1))*du(:,1) -&
			(u_prev_timestep(ng+1:2*ng)+Umn(2))*du(:,2) + nu*SUM(d2u,2)

          !CALL c_diff_fast(u_prev_timestep(shift+1:shift+ng), du, d2u, j_lev, ng, meth, 10, 1, 1, 1)
          user_Drhs_diag(shift+1:shift+ng) = user_Drhs_diag(shift+1:shift+ng) - du_prev_timestep(ie,:,ie)
       END DO
    ELSE IF (dim==3) THEN
       DO ie = 1, 3
          shift=(ie-1)*ng
          !PRINT *,'CAlling c_diff_diag from user_Drhs_diag()'
          !CALL c_diff_diag(u(shift+1:shift+ng), du, d2u, j_lev, ng, meth, meth, 11)
          user_Drhs_diag(shift+1:shift+ng) = - (u_prev_timestep(1:ng)+Umn(1))*du(:,1) -&
			 (u_prev_timestep(ng+1:2*ng)+Umn(2))*du(:,2) - &
             (u_prev_timestep(2*ng+1:3*ng)+Umn(3))*du(:,3) + nu*SUM(d2u,2)

          !CALL c_diff_fast(u_prev_timestep(shift+1:shift+ng), du, d2u, j_lev, ng, meth, 10, 1, 1, 1)
          user_Drhs_diag(shift+1:shift+ng) = user_Drhs_diag(shift+1:shift+ng) - du_prev_timestep(ie,:,ie)
       END DO
    ELSE
       PRINT *, 'this dimensionality has not been setup: dim=',dim
       CALL parallel_finalize; STOP 'in user_Drhs_diag'
    END IF
  END FUNCTION user_Drhs_diag

  SUBROUTINE user_project (u, p, nlocal, meth)
    !--Makes u divergence free
    IMPLICIT NONE

    INTEGER, PARAMETER :: ne_local = 1
    INTEGER, INTENT (IN) :: meth, nlocal
    REAL (pr), DIMENSION (nlocal),     INTENT(INOUT) :: p
    REAL (pr), DIMENSION (nlocal,n_integrated), INTENT(INOUT) :: u
    REAL (pr), DIMENSION (nlocal) :: dp
    REAL (pr), DIMENSION (nlocal) :: f
    REAL (pr), DIMENSION(ne_local) :: scl_p
    INTEGER, DIMENSION(ne_local) :: clip 
    
    
    !--Make u divergence free
    dp = 0.0_pr
    clip = 1
    IF (verb_level.GT.1) PRINT *, 'MINMAX(u(:,1:dim))=', MINVAL(u(:,1:dim)), MAXVAL(u(:,1:dim))
    f = div(u(:,1:dim),nlocal,j_lev,meth+4)
    clip = 1
    scl_p = MAXVAL(scl_global(1:dim)) !scale of pressure increment based on dynamic pressure
    IF (verb_level.GT.1) THEN
       PRINT *, '===================================================================='
       PRINT *, 'in user_project before Linsolve:'
       PRINT *, 'scl_p=',scl_p
       PRINT *, 'MINMAX(f)=', MINVAL(f), MAXVAL(f)
       PRINT *, 'MINMAX(p)=', MINVAL(p), MAXVAL(p)
       PRINT *, 'MINMAX(dp)=', MINVAL(dp), MAXVAL(dp)
       !PAUSE 'in user_project'
    END IF
    
    CALL Linsolve (dp, f, tol2, nlocal, ne_local, clip, Laplace, Laplace_diag, SCL=scl_p) !new with scaling 
    !    CALL Linsolve (dp, f , tol2, nlocal, ne_local, clip, Laplace, Laplace_diag)  !old without scaling
    IF (verb_level.GT.1) THEN
       PRINT *, 'in user_project after Linsolve:'
       PRINT *, 'MINMAX(p)=', MINVAL(p), MAXVAL(p)
       PRINT *, 'MINMAX(dp)=', MINVAL(dp), MAXVAL(dp)
       PRINT *, '===================================================================='
    END IF

    u(:,1:dim) = u(:,1:dim) - grad(dp, nlocal, j_lev, meth+2)
    p = p + dp/dt
  END SUBROUTINE user_project
  
  FUNCTION user_chi (nlocal, t_local )
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: nlocal
    REAL (pr), INTENT (IN) :: t_local
    REAL (pr), DIMENSION (nlocal) :: user_chi

    user_chi = 0.0_pr
  END FUNCTION user_chi


  !
  ! Calculate any statitics
  !
  ! startup_flag - 0 when adapting to IC,then 1 inmain integration loop
  !
  SUBROUTINE user_stats ( u, j_mn_local, startup_flag)
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: startup_flag
    INTEGER , INTENT (IN) :: j_mn_local 
    REAL (pr), DIMENSION (nwlt,1:n_var), INTENT (IN) :: u

    ! debug: test database independent interpolation
    IF (test_interpolate) CALL user_interpolate (u)
    
  END SUBROUTINE user_stats


  SUBROUTINE user_cal_force (u, n, t_local, force, drag, lift)
    !--Calculates drag and lift on obstacle using penalization formula
    IMPLICIT NONE

    INTEGER, INTENT (IN) :: n
    REAL (pr), INTENT (IN) :: t_local
    REAL (pr), DIMENSION (dim), INTENT (INOUT) :: force
    REAL (pr), INTENT (OUT) :: drag, lift
    REAL (pr), DIMENSION (n,dim) :: u
    drag = 0.0_PR
    lift = 0.0_PR

    !
    ! There is no obstacle in flow
    !

  END SUBROUTINE user_cal_force

  !
  ! Read input from "case_name"_pde.inp file
  ! case_name is in string file_name read from command line
  ! in read_command_line_input()
  ! 
  SUBROUTINE user_read_input()
    IMPLICIT NONE

    call input_real ('nu',nu,'stop', &
         ' nu: viscosity')


    test_interpolate = .FALSE.
    call input_logical ('debug_test_interpolation',test_interpolate,'default', &
         ' test database independent interpolation inside usercase')
    
    
  END SUBROUTINE user_read_input



  !
  ! calculate any additional variables
  ! 
  ! arg 
  ! flag  - 0 calledwhile adapting to IC, 1 - called in main time integration loop
  !
  ! These additiona variables are calculated and left in real space.
  !
  SUBROUTINE user_additional_vars( t_local, flag )
    IMPLICIT NONE
    REAL (pr), INTENT (IN) ::  t_local
    INTEGER , INTENT(IN) :: flag ! 0- called during adaption to IC, 1 called during main integration loop


    ! Calculate the vorticity if we are in the main integration loop and we
    ! are saving the solution
    ! 
	! NOTE we do not calculate vorticity in initial adaptation because
	! derivatives are not setup yet (to save memmory)
	! This should be changed eventually DG
	!
    IF (flag == 1 .AND. t  > twrite ) THEN 

 


       CALL cal_vort (u(:,n_var_velocity:n_var_velocity+dim), u(:,n_var_vorticity:n_var_vorticity+3-MOD(dim,3)-1), nwlt)

    END IF
    IF (debug_level.GT.1) PRINT * ,'Calc Vort '

  END SUBROUTINE user_additional_vars


  !
  ! calculate any additional scalar variables
  ! 
  ! arg 
  ! flag  - 0 calledwhile adapting to IC, 1 - called in main time integration loop
  !
  SUBROUTINE user_scalar_vars( flag )
    IMPLICIT NONE
    INTEGER , INTENT(IN) :: flag ! 0- called during adaption to IC, 1 called during main integration loop



  END SUBROUTINE user_scalar_vars

  !
  !************ Calculating Scales ***************************
  !
  ! Note the order of the components in the scl array
  ! correspond to u_tn, v_tn, w_tn, u_tn-1, v_tn-1, w_tn-1, u_tn-2, v_tn-2, w_tn-2
  ! 
  SUBROUTINE user_scales(flag, use_default, u, nlocal, ne_local, l_n_var_adapt , l_n_var_adapt_index, &
       scl, scl_fltwt) !add
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: flag ! 0 during initial adaptation, then 1 during time advancement
    LOGICAL , INTENT(INOUT) :: use_default
    INTEGER, INTENT (IN) :: nlocal, ne_local
    REAL (pr), DIMENSION (1:nlocal,1:ne_local), INTENT (IN) :: u
    LOGICAL , INTENT (IN) :: l_n_var_adapt(ne_local)
    INTEGER , INTENT (IN) :: l_n_var_adapt_index(1:ne_local)
    REAL (pr), DIMENSION (1:ne_local), INTENT (INOUT) :: scl
    REAL (pr) ,  INTENT(IN) ::scl_fltwt !weight for temporal filter on scl

    !
    ! Ignore the output of this routine and use default scales routine
    !
    use_default = .TRUE. 

    !
    ! NOTE: For a parallel run, synchronize scl(:) across the processors in user_scales.
    !       Use the subroutine scales of default_util.f90 as an example,
    !       subroutine parallel_global_sum of parallel.f90 may be helpful.
    !       Already synchronized: sumdA_global
  END SUBROUTINE user_scales

  SUBROUTINE user_cal_cfl (use_default, u, cfl_out)
    USE precision
    USE sizes
    USE pde
    USE variable_mapping
    USE parallel
    IMPLICIT NONE
    LOGICAL , INTENT(INOUT) :: use_default
    REAL (pr),                                INTENT (INOUT) :: cfl_out
    REAL (pr), DIMENSION (nwlt,n_integrated), INTENT (IN)    :: u
    
    INTEGER                    :: i
    REAL (pr)                  :: floor
    REAL (pr), DIMENSION (dim) :: cfl
    REAL (pr), DIMENSION(dim,nwlt) :: h_arr
    
    use_default = .FALSE.
    
    floor = 1e-12_pr
    cfl_out = floor
    
    CALL get_all_local_h (h_arr)
    
    DO i = 1, nwlt
       cfl(1:dim) = DABS (u(i,n_var_velocity:n_var_velocity+dim)+Umn(1:dim)) * dt/h_arr(1:dim,i)
       cfl_out = MAX (cfl_out, MAXVAL(cfl))
    END DO
    CALL parallel_global_sum( REALMAXVAL=cfl_out )
    
  END SUBROUTINE user_cal_cfl
  
  !******************************************************************************************
  !************************************* SGS MODEL ROUTINES *********************************
  !******************************************************************************************
  
  
  
  !******************************************************************************************
  !************************************* SGS MODEL ROUTINES *********************************
  !******************************************************************************************
  
  
  !
  ! Intialize sgs model
  ! This routine is called once in the first
  ! iteration of the main time integration loop.
  ! weights and model filters have been setup for first loop when this routine is called.
  !
  SUBROUTINE user_init_sgs_model( )
    IMPLICIT NONE
    
    
    ! LDM: Giuliano
    
    ! THE sgs forcign is stored for the time step in sgs_mdl_force(1:nlocal,1:n_integrated)
    ! where nlocal should be nwlt.
    
    
    !          print *,'initializing LDM ...'       
    !          CALL sgs_mdl_force( u(:,1:n_integrated), nwlt, j_lev, .TRUE.)
    
    
  END SUBROUTINE user_init_sgs_model
  
  !
  ! calculate sgs model forcing term
  ! user_sgs_force is called int he beginning of each times step in time_adv_cn().
  ! THE sgs forcign is stored for the time step in sgs_mdl_force(1:nlocal,1:n_integrated)
  ! where nlocal should be nwlt.
  ! 
  ! Accesses u from field module, 
  !          j_lev from wlt_vars module,
  !
  SUBROUTINE  user_sgs_force (u_loc, nlocal)
    IMPLICIT NONE
    
    INTEGER,                         INTENT (IN) :: nlocal
    REAL (pr), DIMENSION (nlocal,n_integrated), INTENT (INOUT) :: u_loc
    
  END SUBROUTINE  user_sgs_force
  
  FUNCTION user_sound_speed (u, neq, nwlt)
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: nwlt, neq
    REAL (pr), DIMENSION (nwlt,neq), INTENT(IN) :: u
    REAL (pr), DIMENSION (nwlt) :: user_sound_speed

    !user_sound_speed(:) = SQRT( gamma*(gamma-1.0_pr)* &
    !       ( u(:,4)-0.5_pr*SUM(u(:,n_var_mom(1:dim))**2,DIM=2)/u(:,1)) ) ! pressure

    user_sound_speed = 0.0_pr      ! not used, for compiler only
  END FUNCTION user_sound_speed

  
  !-----------------------------------------------------------------------------------
  SUBROUTINE user_interpolate ( u )
    ! This is an example of database independent interpolation subroutine usage;
    ! predefined number of points and stack allocation has been used for that example.
    ! Read also the info inside subroutine interpolate of module wavelet_filters_mod
    IMPLICIT NONE
    REAL (pr), DIMENSION (nwlt, n_var), INTENT (IN) :: u
    REAL (pr), DIMENSION (n_var, nwlt, dim) :: du, d2u
    INTEGER, PARAMETER :: x_size = 100                ! number of points to interpolate into
    REAL(pr) :: points(dim,x_size)
    INTEGER :: var(1:n_var)                           ! interpolate for all variables
    REAL (pr) :: res(1:n_var,1:x_size)                ! result of the interpolation
    INTEGER :: i, j, interpolation_order, var_size
    LOGICAL, SAVE :: BEEN_HERE = .FALSE.              ! file initialization flag
    INTEGER, PARAMETER :: iunit = 91                  ! ...
    CHARACTER*(*), PARAMETER :: filename = 'int.agr'  ! ...
    INTEGER, SAVE :: set_counter = 0
    REAL(pr), PARAMETER :: MARGIN = 0.0_pr            ! margin
    REAL (4)  :: t0(0:2), t1(0:2)


    ! set output file for debugging
    IF (BEEN_HERE) THEN
       ! open file for appending
       OPEN (UNIT=iunit, FILE=filename, FORM='formatted', STATUS='old', POSITION='append', ERR=1)
    ELSE
       ! create new file
       ! and write XMGR header
       OPEN (UNIT=iunit, FILE=filename, FORM='formatted', STATUS='unknown', ERR=1)
       WRITE (iunit,'("@g0 hidden false")')
!!$       !BEEN_HERE = .TRUE.
    END IF

    
    ! compute first derivatives to be passed to interpolate()
    ! for 2.5 order interpolation
    CALL c_diff_fast (u, du, d2u, j_lev, nwlt, HIGH_ORDER, 10, n_var, 1, n_var)
    DO i=1,n_var
       WRITE (*,'("   MAXVAL(ABS(u(1:nwlt,",I2,"))) = ",E17.10)') i, MAXVAL(ABS(u(1:nwlt,i)))
       WRITE (*,'("   MAXVAL(ABS(du(1:nwlt,",I2,"))) = ",E17.10)') i, MAXVAL(ABS(du(i,1:nwlt,1:dim)))
    END DO
    

    ! interpolate for all n_var variables,
    ! set var_size and var(:) respectively
    var_size = n_var
    DO i=1,n_var
       var(i) = i
    END DO


    ! predefine points to interpolate into
    ! diagonal has been used for that example
    DO i=1,x_size
       DO j=1,dim ! xx(0:nxyz(:),1:dim)
          points(j,i) = MARGIN + xx(0,j) + (xx(nxyz(j),j) - xx(0,j) - 2*MARGIN)*(i-1)/(1.0*(x_size-1))
       END DO
    END DO


    ! perform interpolation for different orders
    ! and write XMGR file ordered by the first coordinate
    DO interpolation_order = 0,2
!!$       !points (2:dim,:) = 3.1415_pr*0.5_pr
!!$       !points (:,:) = 3.0_pr
!!$       !var_size = 1       
       CALL CPU_TIME( t0(interpolation_order) )
       CALL interpolate( u, du, nwlt, n_var, &
            x_size, points, interpolation_order, var_size, var, res(1:var_size,:) )
       CALL CPU_TIME( t1(interpolation_order) )
       DO j=1, var_size
          DO i=1,x_size
             WRITE (iunit,'(2(E12.5,1X))') points(1,i), res(j,i)
          END DO
          WRITE (iunit,'("&")')
       END DO
    END DO
    set_counter = set_counter + 1
    
    
    ! close XMGR file
    CLOSE (iunit)
    WRITE (*, '("   DEBUG: 0,1,3 order interpolation sets written ",I5," times")') set_counter
    DO interpolation_order = 0,2
       WRITE (*, '("CALL interpolate (USING CPU_TIME) = ", es12.5)') t1(interpolation_order) - t0(interpolation_order)
    END DO
    PAUSE
    
    
    ! error handling
    RETURN
1   PRINT *, 'ERROR while opening file:', filename
    STOP 'in user_interpolate'
    
  END SUBROUTINE user_interpolate
  
  
  
  SUBROUTINE  user_pre_process
    IMPLICIT NONE
     
  END SUBROUTINE user_pre_process

  SUBROUTINE  user_post_process
    IMPLICIT NONE
    
  END SUBROUTINE user_post_process


  FUNCTION user_mapping ( xlocal, nlocal, t_local )
    USE curvilinear
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: nlocal
    REAL (pr), INTENT (IN) :: t_local
    REAL (pr), DIMENSION (nlocal,dim), INTENT(IN) :: xlocal
    REAL (pr), DIMENSION (nlocal,dim) :: user_mapping

    user_mapping(:,1:dim) = xlocal(:,1:dim)

  END FUNCTION user_mapping

END MODULE user_case
