global POSTPROCESS_DIR local_dir line_clr
local_dir = pwd;
if isunix
    pl = '/';
else
    pl = '\';
end

%cd('D:\Oleg_Vasilyev\Research\FORTRAN\wlt_3d_DBs\TestCases\EllipticTest1')
%POSTPROCESS_DIR = 'D:\Oleg_Vasilyev\Research\FORTRAN\wlt_3d_DBs\post_process'
POSTPROCESS_DIR = [pwd pl '..' pl '..' pl 'post_process' pl 'analyze_geometry']

path(path,POSTPROCESS_DIR)
clear pl
cd(local_dir) 

line_clr = 'r';
GEOMETRY_PLOT_TYPE = 1 %0 - only points inside, 1 - contour and points inside
threedimenin('simp',GEOMETRY_PLOT_TYPE)
