#------------------------------------------------------------------#
# General input file format                                        #
#                                                                  #
# comments start with # till the end of the line                   #
# string constant are quoted by "..." or '...'                     #
# boolean can be (T F 1 0 on off) in or without ' or " quotes      #
# integers are integers                                            #
# real numbers are whatever (e.g. 1   1.0   1e-23   1.123d-54 )    #
# vector elements are separated by commas                          #
# spaces between tokens are not important                          #
# empty or comment lines are not important                         #
# order of lines is not important                                  #
#------------------------------------------------------------------#


file_gen = 'cylindrical_conduction2D_restart.'
results_dir = 'results'
#***************************************************************************************
do_Sequential_run       = F    # do_Sequential_run
IC_restart_mode 	= 2    # 0: normal, 1: hard restart, 2: soft restart, 3: from IC 
                               # Hard restart - restart a previous run (without changing any parameters)
                               # Soft restart - epsilons, names, j_mx, j_tree_root, etc, could be changed
                               # Restart from IC - some parameters can be changed and user initial conditions can be imposed
 
IC_restart_station 	= 5    #  it_start, restart file number to use (NOT iteration!)
IC_filename             = './results/cylindrical_conduction2D.0002.p0.res'
IC_file_fmt 		= 0    # IC data file format (0 - native restart file, 1-netcdf, 2- A.Wray in fourier space, 3-simple binary)
IC_adapt_grid 		= T    # parameter defaulted to .TRUE. If is set to .FALSE. no grid adaptation is done after the data are read.
IC_single_loop          = T    # if T grid is adapted on ICs just once 
Data_file_format 	= F    #  T = formatted, F = unformatted
#***************************************************************************************

###############################################################################################################################################################
#  P a r a l l e l     D o m a i n      D e c o m p o s i t i o n
#
domain_debug  = F
domain_split  = 1, 1, 1      # Parallel domain decomposition: 1 allows subdivision in the direction (x,y,z,etc) (nonzero - allow, 0 - do not split that direction)

domain_meth  = 0      # domain meth decomposition is based on . . .
                      #       0 (default) geometric simultaneous - based on prime number 
                      #       1 geometric sequential (recursive pd N) based on N^(1/D) subdivision
                      #       2 Zoltan library, Geometric
                      #       3 Zoltan library, Hypergraph
                      #       4 Zoltan library, Hilbert Space-Filling Curve
                      #       10 tree number
                      #       11 tree number as if the boundaries are excluded
                      #       -1 read domain decomposition from the restart file
                      #           (during restart or in postprocessing only)
domain_imbalance_tol = 0.1,0.85,0.95
###############################################################################################################################################################


dimension = 2		#  dim (2,3), # of dimensions

verb_level = 2 

coord_min = 1.0, 0.0, 0.5            #  XMIN, YMIN, ZMIN, etc
coord_max = 2.0, 6.28318530717959, 1.5   		#  XMAX, YMAX, ZMAX, etc
#coord_max = 2.0, 3.14159265358979, 1.5   		#  XMAX, YMAX, ZMAX, etc

coord_zone_min = -5e+02, -5e+02, -5e+02		# XMINzone, etc
coord_zone_max = 5e+02, 5e+02, 5e+02		# XMAXzone, etc

eps_init = 1.0000000e-3  	#  EPS used to adapt initial grid  
eps_run = 1.0000000e-3  	#  EPS used in run  
eps_adapt_steps =  2            # eps_adapt_steps ! how many time steps to adapt from eps_init to eps_run

Scale_Meth = 1             	# Scale_Meth !1- Linf, 2-L2
scl_fltwt = 0.0           	#  scl temporal filter weight, scl_new = scl_fltwt*scl_old + ( 1-scl_fltwt)*scl_new
scaleCoeff = 1 1 1 1 1 1 1 1

M_vector = 4,4,2  	#  Mx, etc
j_mn_init = 2             	#  J_mn_init force J_mn == J_INIT while adapting to IC
j_lev_init = 2             	#  starts adapting IC by having all the points on this level of resolution
j_IC = 20             	#  J_IC if the IC data does not have dimensions in it then mxyz(:)*2^(j_IC-1) is used
J_MN = 2             	#  J_MN
J_MX = 7             	#  J_MX
j_mn_init = 2           #  J_mn_init force J_mn == J_INIT while adapting to IC
J_FILT = 20            	#  J_FILT
j_tree_root= 2 

periodic = 0,0,0        #  prd(:) (0/1) 0: non-periodic; 1: periodic
uniform = 0,0,0		#  grid(:) (0/1) 0: uniform; 1: non-uniform
transform_dir = 1,1,1
coordinate_adapt_scale = 1.0e+00  # coordinate_adapt_scale: Scale for adapting on the coordinate map

i_h = 123456      	#  order of boundaries (1-xmin,2-xmax,3-ymin,4-ymax,5-zmin,6-zmax)
i_l = 111111        	#  algebraic/evolution (1/0) BC order: (lrbt)
N_predict = 2             	#  N_predict
N_predict_low_order = 1 # N_predict_low_order
N_update = 0             	#  N_update
N_update_low_order = 0  # N_update_low_order
N_diff = 2             		#  N_diff

IJ_ADJ = 1,1,1		# IJ_ADJ(-1:1) = (coarser level), (same level), (finer level)
ADJ_type = 1,1,1	#  ADJ_type(-1:1) = (coarser level), (same level), (finer level) # (0 - less conservative, 1 - more conservative)

BNDzone = F	  	#  BNDzone
BCtype = 0             	#  BCtype (0 - Dirichlet, 1 - Neuman, 2 - Mixed)

###############################################################################################################################################################
#  T i m e    I n t e g r a t i o n     I n f o r m a t i o n
#
time_integration_method = 2 # 0=meth2, 1=krylov, 2=Crank Nicolson, 3=RK, 4=IMEX 
RKtype = 3                  # 1=RK2, 2=RK2TVD, 3=RK3TVD, 4=RK4
IMEXtype = 6                # 1=(111)L(T2), 2=(121)L(T1), 3=(122)A(1.0)(T1), 4=(233)A(0.732)(T1), 5=(232)L(T1), 6=(222)L(T2), 7=(343)L(T1), 8=(443)L(T2), 
                            # 9=(122)A(1.0)TVD(T1), 10=(232)A(0.5)TVD(T1), 11=(332)LTVD(T2)
                            # (srp) => s=# of implicit steps, r=# of explicit steps, p=order of IMEX method
                            # A = A-stable implicit method (R_inf follows in parentheses); L = L-stable implicit method (R_inf = 0)
                            # TVD = explicit scheme is TVD
                            # (T1) = Type 1 where bim=bex, (T2) = Type 2 where bim(j)=aim(s,j) and bex(j)=aex(s+1,j) - good for very stiff problems 

t_begin = 0.00000e-00    	#  tbeg  0.2500000e+00
t_end   = 1.00000e+01	#  tend  1.50000e+00
dt      = 1.0000000e-02	#  dt
dtmax   = 1.0000000e-00 	#  dtmax
dtmin   = 1.0000000e-08 	#  dtmin-if dt < dtmin then exection stops(likely blowing up)
dtwrite = 1.0000000e-01	#  dtwrite 1.0000000e-02 
t_adapt = 1.e+10        # when t > t_adapt use an adaptive time step if possible

cflmax = 1.0000000e-00 	#  cflmax
cflmin = 1.0000000e-08 	#  cflmin , Exit if cfl < cflmin
###############################################################################################################################################################


nu = 1.0000000e00  	    #  nu 1.0000000e-03, viscosity
nu1 = 1.0000000e-02 	#  nu1 5.0000000e-02
u0 = 0.010000e+00 	    #  u0
Zero_Mean = F           #  T- enforce zero mean for 1:dim first variables (velocity usually), F- do nothing
eta = 1.0000000e-04 	#  eta, alpha
theta = 0.0000000e+01 		#  theta (in degrees) (angle of 2D Burger's equation, ifn=3=> theta is angular velocity)
theta1 = -4.5000000e+01 	#  theta1 (in degrees) (angle of 2D Burger's equation)

diagnostics_elliptic =  F       #  diagnostics_elliptic: If T print full diagnostic for elliptic solver
GMRESflag = T              	#  GMRESflag
BiCGSTABflag = F              	#  BiCGSTABflag

Jacoby_correction = T           # Jacoby_correction
multigrid_correction = T        # multigrid_correction
GMRES_correction  = T           # GMRES_correction 

kry_p = 2			#  kry_p
kry_perturb = 2			#  kry_perturb
kry_p_coarse = 100           	#  kry_p_coarse
len_bicgstab = 6             	#  len_bicgstab
len_bicgstab_coarse = 100	#  len_bicgstab_coarse
len_iter = 5             	#  len_iter

W0 = 1.0000000e+00 	#  W0 underrelaxation factor for inner V-cycle
W1 = 1.0000000e+00 	#  W1 underrelaxation factor on the finest level
W2 = 0.6666666e+00 	#  W2 underrelaxation factor for weighted Jacoby (inner points)
W3 = 1.0000000e-00 	#  W3 underrelaxation factor for weighted Jacoby (boundary points)
W_min = 1.00000e-02	#  W_min correcton factor 
W_max = 1.00000e-00 	#  W_max correction factor 

obstacle = F              	# imask !TRUE  - there is an obstacle defined
itime = 3   	           	# itime ! 0- Euler first-order, 1- Rotational first-order, 2- Second-order, 3-Semi-implicit second-order
obstacle_X = 0,0,0 		#  Xo(:)! Location of obstacle
obstacle_U = 0,0,0		#  Uo(:)! Velocity of obstacle
obstacle_move = 0,0,0		#  1- Obstacle allowed to move in that direction, else == 0


diameter = 1.0		#  d diameter of cylinder as an obstacle
k_star = 8.7400000e+00 	#  k_star
m_star = 5.0000000e+00 	#  m_star
b_star = 0.0000000e-03 	#  b_star
tol1 = 1.0000000e-06 	#  tol1 used to set tolerence for non-solenoidal half-step
tol2 = 1.0000000e-06 	#  tol2 used to set tolerence for solenoidal half-step
tol3 = 1.0000000e-04           #  tol3 used to set tolerence for time step
tol_gmres = 1.0e-4        #  used to set tolerence for gmres iterative solver
tol_gmres_stop_if_larger = F  # If true stop iterating in gmres solver if error of last iteration was smaller
wlog = T              	#  wlog

#------------- case specific parameters

nu  = 1.0          #  heat conduction coefficient
T_0 = 0.0          #  initial temperature
T_1 = 1.0          #  temperature at Xmin
q_0 = 0.0          #  flux at top wall
q_1 = 1.0          #  flux at top wall
