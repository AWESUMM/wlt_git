MODULE user_case
  ! CASE 2/3 D channel periodic in x-direction and if dim=3 in z-derection
  USE precision
  USE wlt_vars
  USE wavelet_filters_mod
  USE elliptic_mod
  USE elliptic_vars
  USE wlt_trns_mod
  USE wlt_trns_vars
  USE wlt_trns_util_mod
  USE io_3d_vars
  USE util_mod
  USE util_vars
  USE share_consts
  USE pde
  USE sizes
  USE share_kry
  USE vector_util_mod
  USE field
  USE input_file_reader
  USE debug_vars
  !
  ! case specific variables
  !
  INTEGER n_var_pressure  ! start of pressure in u array
  INTEGER n_var_temp      ! start of temperature in u array

  REAL (pr) :: nu     ! kinematic viscosity (1/Re)heat conduction
  REAL (pr) :: T_0    ! Temperature of the inner cylinder
  REAL (pr) :: T_1    ! Temperature of the outer cylinder
  REAL (pr) :: q_0    ! Flux of the min edge
  REAL (pr) :: q_1    ! Flux of the max edge
  INTEGER :: Tdim = 1 ! radial direction is 1

CONTAINS

  !
  ! In user_setup_pde() we setup how many variables are integrated, which are interpolated
  ! to the next times step and if any exeact solution exists to compare to during the run.
  ! We also set the variable names that are used when the result files are written.
  !
  ! The following variables must be setup in this routine:
  !
  !
  ! n_integrated     ! first n_integrated eqns will be acted on for time integration
  ! n_var_additional ! 
  ! n_var
  !
  !
  !
  !
  SUBROUTINE  user_setup_pde ( VERB ) 
    USE curvilinear
    USE variable_mapping
    IMPLICIT NONE
    LOGICAL, OPTIONAL :: VERB         ! print debug info
    INTEGER :: i

    IF (verb_level.GT.0) THEN
       PRINT * ,''
       PRINT *, '**********************Setting up PDE*****************'
       PRINT * ,'CASE: Heat-transfer: cylindrical domain '
       PRINT *, '*****************************************************'
    END IF

    !register_var( var_name, integrated, adapt, interpolate, exact, saved, req_restart, FRONT_LOAD )
    CALL register_var( 'T ',       integrated=.TRUE.,  adapt=(/.TRUE.,.TRUE./),   interpolate=(/.TRUE.,.TRUE./),   exact=(/.TRUE.,.TRUE./),   saved=.TRUE., req_restart=.TRUE. )
    CALL register_var( 'pressure ', integrated=.FALSE., adapt=(/.FALSE.,.FALSE./), interpolate=(/.FALSE.,.FALSE./), exact=(/.FALSE.,.FALSE./), saved=.TRUE., req_restart=.FALSE. )
    CALL setup_mapping()
    
    ! Diagnostic
    !CALL print_variable_registery( FULL=.TRUE.)

    n_var_pressure  = get_index('pressure ')  
    n_var_temp      = get_index('  T    ')  

    scaleCoeff = 1.0_pr

    IF (verb_level.GT.0) THEN
       PRINT *, 'n_integrated = ',n_integrated 
       PRINT *, 'n_var = ',n_var 
       PRINT *, 'n_var_exact = ',n_var_exact 
       PRINT *, '*******************Variable Names*******************'
       DO i = 1,n_var
          WRITE (*, u_variable_names_fmt) u_variable_names(i)
       END DO
       PRINT *, '****************************************************'
    END IF

  END SUBROUTINE  user_setup_pde
  !
  ! Set the exact solution for comparison to the simulated solution
  !
  ! u          - array to fill in the exact solution
  ! nlocal       - number of active wavelets   
  ! ne_local        - total number of equations
  ! t          - time of current time step 
  ! l_n_var_exact_soln_index - index into the elements of u for which we need to 
  !                            find the exact solution
  SUBROUTINE  user_exact_soln (u, nlocal,  t_local, l_n_var_exact_soln)  
    USE variable_mapping
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: nlocal
    REAL (pr), INTENT (IN) ::  t_local
    REAL (pr), DIMENSION (nlocal,n_var_exact), INTENT (INOUT) :: u
    LOGICAL , INTENT (IN) :: l_n_var_exact_soln(n_var)
    REAL (pr) :: t_zero
    INTEGER :: i
    DOUBLE PRECISION :: DERF
    EXTERNAL :: DERF

   

  END SUBROUTINE  user_exact_soln

  SUBROUTINE user_initial_conditions (u, nlocal, ne_local, t_local, scl, scl_fltwt, iter)
    USE variable_mapping
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: nlocal, ne_local
    INTEGER  , INTENT (INOUT) :: iter ! iteration of call while adapting initial grid
    REAL (pr), DIMENSION (nlocal,ne_local), INTENT (INOUT) :: u
    REAL (pr)  :: scl(1:n_var),scl_fltwt
    REAL (pr), INTENT (IN) :: t_local
    REAL (pr) :: t_zero
    INTEGER :: i
    DOUBLE PRECISION :: DERF
    EXTERNAL :: DERF

    REAL (pr), DIMENSION(nlocal, dim) :: x_phys

  IF (IC_restart_mode > 0 ) THEN !in the case of restart
     !do nothing
  ELSE IF (IC_restart_mode .EQ. 0) THEN
    u(:,n_var_temp) =  T_0 + (T_1-T_0)*((x(:,Tdim)-xyzlimits(1,Tdim))/(xyzlimits(2,Tdim)-xyzlimits(1,Tdim)))**2
  END IF

  END SUBROUTINE user_initial_conditions

!--********************************  
  ! Arguments
  ! u         - field on adaptive grid
  ! nlocal      - number of active points
  ! ne_local       - number of equations
  ! t         - current time
  !
  ! Global variables used
  ! ifn       - number of equations
  ! x()
  ! xO
  ! y0
  !--******************************** 
  SUBROUTINE user_algebraic_BC (Lu, u, nlocal, ne_local, jlev, meth)
    !--Defines boundary condition type
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: jlev, meth, ne_local, nlocal
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (INOUT) :: Lu
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (IN)    :: u

    INTEGER :: i, ie, ii, shift
    REAL (pr), DIMENSION (ne_local,nlocal,dim) :: du, d2u !uncomment if Neuman BC are used, need to call 
    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc

    !BC for both 2D and 3D cases

    !if Neuman BC are used, need to call 
    CALL c_diff_fast (u, du, d2u, jlev, nlocal, meth, 10, ne_local, 1, ne_local, FORCE_RECTILINEAR=.TRUE.)

    DO ie = 1, ne_local
       shift = nlocal*(ie-1)
       !--Go through all Boundary points that are specified
       i_p_face(0) = 1
       DO i=1,dim
          i_p_face(i) = i_p_face(i-1)*3
       END DO
       DO face_type = 0, 3**dim - 1
          face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
          IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
             CALL get_all_indices_by_face (face_type, jlev, nloc, iloc)
             IF(nloc > 0 ) THEN 
                IF( ABS(face(1)) .EQ. 1 ) THEN                    !iner and outer cylinders 
                   Lu(shift+iloc(1:nloc)) = u(shift+iloc(1:nloc))               !Dirichlet conditions
                ELSE IF( dim > 1 .AND. ABS(face(2)) .EQ. 1 ) THEN                             !edges 
                   Lu(shift+iloc(1:nloc)) = face(2)*du(ie,iloc(1:nloc),2)       !Neuman conditions
                END IF
             END IF
          END IF
       END DO
    END DO

  END SUBROUTINE user_algebraic_BC

  SUBROUTINE user_algebraic_BC_diag (Lu_diag, nlocal, ne_local, jlev, meth)
    !--Defines boundary condition type
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: jlev, meth, ne_local, nlocal
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (INOUT) :: Lu_diag

    INTEGER :: i, ie, ii, shift
    REAL (pr), DIMENSION (nlocal,dim) :: du, d2u  !uncomment if Neuman BC are used
    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc
    
    !BC for both 2D and 3D cases

    !if Neuman BC are used, need to call 
    CALL c_diff_diag ( du, d2u, jlev, nlocal, meth, meth, 10, FORCE_RECTILINEAR=.TRUE.)

    DO ie = 1, ne_local
       shift = nlocal*(ie-1)
       !--Go through all Boundary points that are specified
       i_p_face(0) = 1
       DO i=1,dim
          i_p_face(i) = i_p_face(i-1)*3
       END DO
       DO face_type = 0, 3**dim - 1
          face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
          IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
             CALL get_all_indices_by_face (face_type, jlev, nloc, iloc)
             IF(nloc > 0 ) THEN 
                IF( ABS(face(1)) .EQ. 1 ) THEN                  !iner and outer cylinders 
                   Lu_diag(shift+iloc(1:nloc)) = 1.0_pr                       ! Dirichlet conditions
                ELSE IF( dim > 1 .AND. ABS(face(2)) .EQ. 1 ) THEN                           !edges 
                   Lu_diag(shift+iloc(1:nloc)) = face(2)*du(iloc(1:nloc),2)   !Dirichlet conditions
                END IF
             END IF
          END IF
       END DO
    END DO
 
  END SUBROUTINE user_algebraic_BC_diag

  SUBROUTINE user_algebraic_BC_rhs (rhs, ne_local, nlocal, jlev)
    !--Sets rhs for boundary conditions
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: ne_local, nlocal, jlev
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (INOUT) :: rhs

    INTEGER :: i, ie, ii, shift
    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc

    !BC for both 2D and 3D cases

    DO ie = 1, ne_local
       shift = nlocal*(ie-1)
       !--Go through all Boundary points that are specified
       i_p_face(0) = 1
       DO i=1,dim
          i_p_face(i) = i_p_face(i-1)*3
       END DO
       DO face_type = 0, 3**dim - 1
          face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
          IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
             CALL get_all_indices_by_face (face_type, jlev, nloc, iloc)
             IF(nloc > 0 ) THEN 
                IF( face(1) .EQ. -1 )  THEN                   ! outer cylinder 
                   rhs(shift+iloc(1:nloc)) = T_0                 
                ELSE IF( face(1) .EQ. 1 ) THEN            ! inner cylinder 
                   rhs(shift+iloc(1:nloc)) = T_1
                ELSE IF( dim > 1 .AND. face(2) .EQ. -1 ) THEN            ! min edge 
                   rhs(shift+iloc(1:nloc)) = q_0                 
                ELSE IF( dim > 1 .AND. face(2) .EQ.  1 ) THEN            ! min edge 
                   rhs(shift+iloc(1:nloc)) = q_1                 
                END IF
             END IF
          END IF
       END DO
    END DO

  END SUBROUTINE user_algebraic_BC_rhs

  SUBROUTINE user_project (u, p, nlocal, meth)
    USE variable_mapping
    !--Makes u divergence free
    IMPLICIT NONE

    INTEGER, INTENT (IN) :: meth, nlocal
    REAL (pr), DIMENSION (nlocal),     INTENT(INOUT) :: p
    REAL (pr), DIMENSION (nlocal,n_integrated), INTENT(INOUT) :: u

    REAL (pr), DIMENSION (nlocal) :: dp
    REAL (pr), DIMENSION (nlocal) :: f

    INTEGER :: i
	INTEGER, PARAMETER :: ne = 1
    INTEGER, DIMENSION(ne) :: clip 


  END SUBROUTINE user_project

  FUNCTION user_rhs (u_integrated,p)	 
    USE curvilinear
    IMPLICIT NONE
    REAL (pr), DIMENSION (ng,ne), INTENT(IN) :: u_integrated !1D flat version of integrated variables without BC 
    REAL (pr), DIMENSION (ng), INTENT(IN) :: p
    REAL (pr), DIMENSION (n) :: user_rhs


    INTEGER :: idim, ie, shift
    INTEGER, PARAMETER :: meth=1
    REAL (pr), DIMENSION (dim,ng,dim) :: du, d2u
    REAL (pr), DIMENSION (dim,ng,dim) :: du_comp, d2u_comp
    REAL (pr), DIMENSION (ng,dim)     :: temp

!!$    CALL c_diff_fast(u_integrated(:,1), du(1,:,:), d2u(1,:,:), j_lev, ng, 1, 10, 1, 1, 1)

!!$    !diagnostic check
!!$    CALL c_diff_fast(u_integrated(:,1), du_comp(1,:,:), d2u_comp(1,:,:), j_lev, ng, 1, 10, 1, 1, 1, FORCE_RECTILINEAR=.TRUE.)
!!$    IF ( dim .EQ. 2 &
!!$       .OR. ( dim .EQ. 3 .AND. transform_dir(1) .EQ. 1 .AND. transform_dir(2) .EQ. 1  .AND. transform_dir(3) .EQ. 0 ) ) THEN
!!$       PRINT *, 'diff:', MAXVAL(ABS( du_comp(1,:,1)*COS(x(:,2)) - 1.0_pr/x(:,1)*du_comp(1,:,2)*SIN(x(:,2)) - du(1,:,1) )), &
!!$                         MAXVAL(ABS( du_comp(1,:,1)*SIN(x(:,2)) + 1.0_pr/x(:,1)*du_comp(1,:,2)*COS(x(:,2)) - du(1,:,2) ))
!!$    ELSE IF ( dim .EQ. 3 .AND. ALL(transform_dir(1:dim) .EQ. 1 ) ) THEN
!!$       PRINT *, 'diff:', MAXVAL(ABS( du_comp(1,:,1)*COS(x(:,2))*SIN(x(:,3)) - 1.0_pr/(x(:,1)*SIN(x(:,3)))*du_comp(1,:,2)*SIN(x(:,2)) + 1.0_pr/x(:,1)*COS(x(:,2))*COS(x(:,3))*du_comp(1,:,3)- du(1,:,1) )), &
!!$                         MAXVAL(ABS( du_comp(1,:,1)*SIN(x(:,2))*SIN(x(:,3)) + 1.0_pr/(x(:,1) *SIN(x(:,3)))*du_comp(1,:,2)*COS(x(:,2)) + 1.0_pr/x(:,1)*SIN(x(:,2))*COS(x(:,3))*du_comp(1,:,3)- du(1,:,2) )), &
!!$                         MAXVAL(ABS( du_comp(1,:,1)*COS(x(:,3)) - 1.0_pr/x(:,1)*du_comp(1,:,3)*SIN(x(:,3)) - du(1,:,3) ))
!!$    END IF
!!$     

    !--Form right hand side of heat equations
    CALL c_diff_fast(u_integrated(:,n_var_temp), du(1,:,:), d2u(1,:,:), j_lev, ng, 1, 01, 1, 1, 1)
    ie = n_var_temp
    shift = ng*(ie-1)
    user_rhs(shift+1:shift+ng) = nu*SUM(d2u(1,:,:),DIM=2)

  !---------------- Algebraic BC are set up in user_algebraic_BC and they autmatically overwrite evolution BC

  END FUNCTION user_rhs

  ! find Jacobian of Right Hand Side of the problem
  FUNCTION user_Drhs (u, u_prev_timestep_loc, meth)
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: meth
	!u_prev_timestep passed local to cast it into 2dim array to work with vector derivatives
    REAL (pr), DIMENSION (ng,ne) :: u, u_prev_timestep_loc
    REAL (pr), DIMENSION (n) :: user_Drhs

 
    INTEGER :: idim, ie, shift
    REAL (pr), DIMENSION (dim,ng,dim) :: du  ! 1st derivatives for u (in du(1:ne,:,:)) and u_prev_timestep (in du(ne+1:2*ne,:,:))
    REAL (pr), DIMENSION (dim  ,ng,dim) :: d2u ! 2nd derivatives for u 
    !Find batter way to do this!! du_dummy with no storage..

    CALL c_diff_fast(u(:,n_var_temp), du(1,:,:), d2u(1,:,:), j_lev, ng, 1, 01, 1, 1, 1 )

    !--Form right hand side of heat equations
    ie = n_var_temp
    shift = ng*(ie-1)
    user_Drhs(shift+1:shift+ng) = nu*SUM( d2u(1,:,1:dim), DIM=2 )

  END FUNCTION user_Drhs


  !
  ! Uses u_prev_timestep, which is global flat (1D ) array set in time_step_cn()
  !
  FUNCTION user_Drhs_diag (meth)
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: meth
    REAL (pr), DIMENSION (n) :: user_Drhs_diag

    INTEGER :: ie, shift
    REAL (pr), DIMENSION (ng,dim) :: du, d2u
    REAL (pr), DIMENSION (ne,ng,dim) :: du_prev_timestep
    REAL (pr), DIMENSION (ne  ,ng,dim) :: du_dummy ! passed when only calculating 1st derivative.

    CALL c_diff_diag(du, d2u, j_lev, ng, meth, meth, 01)

    ie = n_var_temp
    shift = ng*(ie-1)
    user_Drhs_diag(shift+1:shift+ng) = nu*SUM(d2u(:,1:dim), DIM=2)

  END FUNCTION user_Drhs_diag

  FUNCTION user_chi (nlocal, t_local )
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: nlocal
    REAL (pr), INTENT (IN) :: t_local
    REAL (pr), DIMENSION (nlocal) :: user_chi

    user_chi = 0.0_pr
  END FUNCTION user_chi

  FUNCTION user_mapping ( xlocal, nlocal, t_local )
    USE curvilinear
    USE curvilinear_mesh
    USE wlt_vars!, ONLY: dim 
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: nlocal
    REAL (pr), INTENT (IN) :: t_local
    REAL (pr), DIMENSION (nlocal,dim), INTENT(IN) :: xlocal
    REAL (pr), DIMENSION (nlocal,dim) :: user_mapping


    user_mapping(:,1:dim) = xlocal(:,1:dim)

    IF ( dim .EQ. 2 ) THEN       ! Cylindrical 
       user_mapping = cylindrical( xlocal(:,1:dim), nlocal )
    ELSE IF ( dim .EQ. 3 ) THEN  ! Spherical
       user_mapping = spherical( xlocal(:,1:dim), nlocal )
    END IF

  END FUNCTION user_mapping

  !
  ! Calculate any statitics
  !
  ! startup_flag - 0 when adapting to IC,then 1 inmain integration loop
  !
  SUBROUTINE user_stats ( u, j_mn_local, startup_flag)
    USE variable_mapping
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: startup_flag
    INTEGER , INTENT (IN) :: j_mn_local 
    REAL (pr), DIMENSION (nwlt,1:n_var), INTENT (IN) :: u
  END SUBROUTINE user_stats


  SUBROUTINE user_cal_force (u, n, t_local, force, drag, lift)
    !--Calculates drag and lift on obstacle using penalization formula
    IMPLICIT NONE

    INTEGER, INTENT (IN) :: n
    REAL (pr), INTENT (IN) :: t_local
    REAL (pr), DIMENSION (dim), INTENT (INOUT) :: force
    REAL (pr), INTENT (OUT) :: drag, lift
    REAL (pr), DIMENSION (n,dim) :: u
    drag = 0.0_PR
    lift = 0.0_PR

    !
    ! There is no obstacle in flow
    !

  END SUBROUTINE user_cal_force

  !
  ! Read input from "case_name"_pde.inp file
  ! case_name is in string file_name read from command line
  ! in read_command_line_input()
  ! 
  SUBROUTINE user_read_input()
    IMPLICIT NONE

  call input_real ('nu',nu,'stop', &
       ' nu: heat conduction coefficient')

  call input_real ('T_0',T_0,'stop', &
       ' T_0:  Temperature of the inner cylinder')

  call input_real ('T_1',T_1,'stop', &
       ' T_1: Temperature of the outer cylinder')

    call input_real ('q_0',q_0,'stop', &
       ' q_0:  Flux of the min edge')

    call input_real ('q_1',q_1,'stop', &
       ' q_1:  Flux of the max edge')

  END SUBROUTINE user_read_input



  !
  ! calculate any additional variables
  ! 
  ! arg 
  ! flag  - 0 calledwhile adapting to IC, 1 - called in main time integration loop
  !
  ! These additiona variables are calculated and left in real space.
  !
  SUBROUTINE user_additional_vars( t_local, flag )
    USE curvilinear
    IMPLICIT NONE
    REAL (pr), INTENT (IN) ::  t_local
    INTEGER , INTENT(IN) :: flag ! 0- called during adaption to IC, 1 called during main integration loop
    REAL (pr) :: t_zero
    INTEGER :: i
    DOUBLE PRECISION :: DERF
    EXTERNAL :: DERF

    !Initial conditionsare set up in computational space
    DO i =1, nwlt
       u(i,n_var_pressure) = T_0 + (T_1 - T_0)*LOG(x(i,Tdim)/xyzlimits(1,Tdim))/LOG(xyzlimits(2,Tdim)/xyzlimits(1,Tdim))
    END DO


  END SUBROUTINE user_additional_vars


  !
  ! calculate any additional scalar variables
  ! 
  ! arg 
  ! flag  - 0 calledwhile adapting to IC, 1 - called in main time integration loop
  !
  SUBROUTINE user_scalar_vars( flag )
    IMPLICIT NONE
    INTEGER , INTENT(IN) :: flag ! 0- called during adaption to IC, 1 called during main integration loop



  END SUBROUTINE user_scalar_vars

  !
  !************ Calculating Scales ***************************
  !
  ! Note the order of the components in the scl array
  ! correspond to u_tn, v_tn, w_tn, u_tn-1, v_tn-1, w_tn-1, u_tn-2, v_tn-2, w_tn-2
  ! 
  SUBROUTINE user_scales(flag, use_default, u, nlocal, ne_local, l_n_var_adapt , l_n_var_adapt_index, &
       scl, scl_fltwt) !add
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: flag ! 0 during initial adaptation, then 1 during time advancement
    LOGICAL , INTENT(INOUT) :: use_default
    INTEGER, INTENT (IN) :: nlocal, ne_local
    REAL (pr), DIMENSION (1:nlocal,1:ne_local), INTENT (IN) :: u
    LOGICAL , INTENT (IN) :: l_n_var_adapt(ne_local)
    INTEGER , INTENT (IN) :: l_n_var_adapt_index(1:ne_local)
    REAL (pr), DIMENSION (1:ne_local), INTENT (INOUT) :: scl
    REAL (pr) ,  INTENT(IN) ::scl_fltwt !weight for temporal filter on scl

    !
    ! Ignore the output of this routine and use default scales routine
    !
    use_default = .TRUE. 

    !
    ! NOTE: For a parallel run, synchronize scl(:) across the processors in user_scales.
    !       Use the subroutine scales of default_util.f90 as an example,
    !       subroutine parallel_global_sum of parallel.f90 may be helpful.
    !       Already synchronized: sumdA_global
  END SUBROUTINE user_scales

SUBROUTINE user_cal_cfl (use_default, u, cfl_out)
  USE precision
  USE sizes
  USE pde
  USE variable_mapping
  IMPLICIT NONE
  LOGICAL , INTENT(INOUT) :: use_default
  REAL (pr),                                INTENT (INOUT) :: cfl_out
  REAL (pr), DIMENSION (nwlt,n_integrated), INTENT (IN)    :: u

  INTEGER                    :: i
  REAL (pr)                  :: floor
  REAL (pr), DIMENSION (dim) :: cfl
  REAL (pr), DIMENSION(dim,nwlt) :: h_arr

  use_default = .FALSE.

  floor = 1e-12_pr
  cfl_out = floor
  
  CALL get_all_local_h (h_arr)
  
!!$  DO i = 1, nwlt
!!$     cfl(1:dim) = ABS (u(i,1:dim)+Umn(1:dim)) * dt/h_arr(1:dim,i)
!!$     cfl_out = MAX (cfl_out, MAXVAL(cfl))
!!$  END DO

  cfl_out = 1.0_pr ! no CFL condition

END SUBROUTINE user_cal_cfl

!******************************************************************************************
!************************************* SGS MODEL ROUTINES *********************************
!******************************************************************************************

!
! Intialize sgs model
! This routine is called once in the first
! iteration of the main time integration loop.
! weights and model filters have been setup for first loop when this routine is called.
!
SUBROUTINE user_init_sgs_model( )
  IMPLICIT NONE


! LDM: Giuliano

! THE sgs forcign is stored for the time step in sgs_mdl_force(1:nlocal,1:n_integrated)
! where nlocal should be nwlt.


!          print *,'initializing LDM ...'       
!          CALL sgs_mdl_force( u(:,1:n_integrated), nwlt, j_lev, .TRUE.)


END SUBROUTINE user_init_sgs_model

!
! calculate sgs model forcing term
! user_sgs_force is called int he beginning of each times step in time_adv_cn().
! THE sgs forcign is stored for the time step in sgs_mdl_force(1:nlocal,1:n_integrated)
! where nlocal should be nwlt.
! 
! Accesses u from field module, 
!          j_lev from wlt_vars module,
!
SUBROUTINE  user_sgs_force (u_loc, nlocal)
  USE variable_mapping
  IMPLICIT NONE

  INTEGER,                         INTENT (IN) :: nlocal
  REAL (pr), DIMENSION (nlocal,n_integrated), INTENT (INOUT) :: u_loc

END SUBROUTINE  user_sgs_force

  FUNCTION user_sound_speed (u, neq, nwlt)
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: nwlt, neq
    REAL (pr), DIMENSION (nwlt,neq), INTENT(IN) :: u
    REAL (pr), DIMENSION (nwlt) :: user_sound_speed

    user_sound_speed(:) = 0.0_pr 
    !user_sound_speed(:) = SQRT( gamma*(gamma-1.0_pr)* &
    !       ( u(:,4)-0.5_pr*SUM(u(:,n_var_mom(1:dim))**2,DIM=2)/u(:,1)) ) ! pressure

  END FUNCTION user_sound_speed

  SUBROUTINE  user_pre_process
    IMPLICIT NONE

  END SUBROUTINE user_pre_process

  SUBROUTINE  user_post_process
    IMPLICIT NONE
  END SUBROUTINE user_post_process

END MODULE user_case
