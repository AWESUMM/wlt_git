MODULE user_case_db

  !
  ! n_var_db must be set to
  !   Count( Union (n_var_adapt, n_var_interpolate))
  ! This is the total number of variable that either are used for adaptation
  ! criteria or need to be interpolated to new grid at each time step
  ! or that are needed for vector derivatives.
  !
  !INTEGER , PARAMETER :: n_var_db =  10  !3d
  INTEGER , PARAMETER :: n_var_db = 5   !2d



END MODULE user_case_db
MODULE user_case
  ! CASE 2/3 D Euler Equations


  USE precision
  USE wlt_vars
  USE wavelet_filters_mod
  USE elliptic_mod
  USE elliptic_vars
  USE wlt_trns_mod
  USE wlt_trns_vars
  USE wlt_trns_util_mod
  USE io_3d_vars
  USE util_mod
  USE util_vars
  USE share_consts
  USE pde
  USE sizes
  USE share_kry
  USE vector_util_mod
  USE field
  USE input_file_reader
  USE debug_vars

  !
  ! case specific variables
  !
  INTEGER n_var_pressure  ! start of pressure in u array
  REAL (pr) :: Re, Pra, gamma, alpha
  REAL (pr) :: delta, radius
  LOGICAL, PARAMETER :: NS=.TRUE. ! if NS=.TRUE. - Navier-Stoke, esle Euler

CONTAINS

  !
  ! In user_setup_pde() we setup how many variables are integrated, which are interpolated
  ! to the next times step and if any exeact solution exists to compare to during the run.
  ! We also set the variable names that are used when the result files are written.
  !
  ! The following variables must be setup in this routine:
  !
  !
  ! n_integrated     ! first n_integrated eqns will be acted on for time integration
  ! n_var_additional ! 
  ! n_var
  !
  !
  !
  !
  SUBROUTINE  user_setup_pde ( VERB ) 
    IMPLICIT NONE
    LOGICAL, OPTIONAL :: VERB         ! print debug info
    INTEGER :: i
    LOGICAL :: do_verb

    do_verb = .TRUE.
    IF (PRESENT(VERB)) do_verb = VERB
    
    IF (do_verb) THEN
       PRINT * ,''
       PRINT *, '**********************Setting up PDE*****************'
       PRINT * ,'CASE VORTEX ARRAY '
       PRINT *, '*****************************************************'
    END IF

    n_integrated = dim +2 
    n_time_levels = 1  !--3 time levels

    n_var_time_levels = n_time_levels * n_integrated !--Number of equations (2D velocity at two time levels)

    n_var_additional = 1
    n_var = n_var_time_levels + n_var_additional !--Total number of variables

    n_var_exact = 0

    n_var_pressure  = n_var !pressure

    !
    !--Allocate logical arrays 
    !  Must be done after setting number of different types of variables
    !  and before setting logical variable mapping arrays
    !
    CALL alloc_variable_mappings


    !
    ! Fill in variable names for default variables for integration meth 2
    ! These may be changed in the call to the user specified routine
    !  user_setup_pde()
    !
    ! In integrated variables (3D)

    IF( dim == 3 ) THEN
       WRITE (u_variable_names(1), u_variable_names_fmt) 'Density_rho  '
       WRITE (u_variable_names(2), u_variable_names_fmt) 'X-Momentum  '
       WRITE (u_variable_names(3), u_variable_names_fmt) 'Y-Momentum  '
       WRITE (u_variable_names(4), u_variable_names_fmt) 'Z-Momentum  '
       WRITE (u_variable_names(5), u_variable_names_fmt) 'E_Total  '
       WRITE (u_variable_names(6), u_variable_names_fmt) 'Pressure  '
    ELSE IF(dim == 2) THEN
       WRITE (u_variable_names(1), u_variable_names_fmt) 'Density_rho  '
       WRITE (u_variable_names(2), u_variable_names_fmt) 'X-Momentum  '
       WRITE (u_variable_names(3), u_variable_names_fmt) 'Y-Momentum  '
       WRITE (u_variable_names(4), u_variable_names_fmt) 'E_Total  '
       WRITE (u_variable_names(5), u_variable_names_fmt) 'Pressure  '
    ELSE IF(dim == 1) THEN
       WRITE (u_variable_names(1), u_variable_names_fmt) 'Density_rho  '
       WRITE (u_variable_names(2), u_variable_names_fmt) 'X-Momentum  '
       WRITE (u_variable_names(3), u_variable_names_fmt) 'E_Total  '
       WRITE (u_variable_names(4), u_variable_names_fmt) 'Pressure  '
    END IF

    !
    ! setup logical variable mapping arrays
    ! This defined which variables are used for adaptation, Which need to be interpololated to the new grid at 
    ! each time step, which are saved, and for which we have an exect solutiuon we want to check at each time step
    !


    !
    ! setup which components we will base grid adaptation on.
    !
	n_var_adapt = .FALSE. !intialize
    n_var_adapt(1:n_integrated,0)     = .TRUE. !--Initially adapted variables at first time level

    n_var_adapt(1:n_integrated,1)     = .TRUE. !--After first time step adapt on 

    !--integrated variables at first time level

    !Variables that need to be interpoleted to new adapted grid in initial grid adaptation
    n_var_interpolate        = .FALSE. !intialize
    n_var_interpolate(1:n_integrated,0) = .TRUE.  

    !Variables that need to be interpoleted to new adapted grid at each time step
    n_var_interpolate(1:n_integrated,1) = .TRUE. 

    !
    ! setup which components we have an exact solution for
    !
	n_var_exact_soln = .FALSE. !intialize
 


    !
    ! setup which variables we will save the solution
    !
	n_var_save = .FALSE. !intialize 
    n_var_save(1:n_var) = .TRUE. ! save all for restarting code

    !
    !--Time level counters for NS integration
    !   Used for integration meth2
    ! n0 is current time step
    ! n1 is t_n-1 time step
    ! n2 is t_n-2 time step
    !
    n0 = 1; n1 = n0+dim; n2 = n1+dim


    !
    ! Set the maximum number of components for which we have an exact solution
    !
!!$    n_var_exact = MAX( COUNT(n_var_exact_soln(:,0)),COUNT(n_var_exact_soln(:,1)) )
    n_var_exact = 0


    !
	! Setup a scaleCoeff array if we want to tweak the scaling of a variable
	! ( if scaleCoeff < 1.0 then more points will be added to grid 
	!
    ALLOCATE ( scaleCoeff(1:n_var) )
    scaleCoeff = 1.0_pr


    IF (do_verb) THEN
       PRINT *, 'n_integrated = ',n_integrated 
       PRINT *, 'n_time_levels = ',n_time_levels
       PRINT *, 'n_var_time_levels = ',n_var_time_levels 
       PRINT *, 'n_var = ',n_var 
       PRINT *, 'n_var_exact = ',n_var_exact 
       PRINT *, '*******************Variable Names*******************'
       DO i = 1,n_var
          WRITE (*, u_variable_names_fmt) u_variable_names(i)
       END DO
       PRINT *, '****************************************************'
    END IF

  END SUBROUTINE  user_setup_pde


  !
  ! Set the exact solution for comparison to the simulated solution
  !
  ! u          - array to fill in the exact solution
  ! nlocal       - number of active wavelets   
  ! ne_local        - total number of equations
  ! t          - time of current time step 
  ! l_n_var_exact_soln_index - index into the elements of u for which we need to 
  !                            find the exact solution
  SUBROUTINE  user_exact_soln (u, nlocal,  t_local, l_n_var_exact_soln)  
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: nlocal
    REAL (pr), INTENT (IN) ::  t_local
    REAL (pr), DIMENSION (nlocal,n_var_exact), INTENT (INOUT) :: u
    LOGICAL , INTENT (IN) :: l_n_var_exact_soln(n_var)
    INTEGER :: i

!!$
!!$    u(:,1) = 
!!$    u(:,2) =  
!!$    and so on

  END SUBROUTINE  user_exact_soln

  SUBROUTINE user_initial_conditions (u, nlocal, ne_local, t_local, scl, scl_fltwt, iter)
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: nlocal, ne_local
    INTEGER  , INTENT (INOUT) :: iter ! iteration of call while adapting initial grid
    REAL (pr), DIMENSION (nlocal,ne_local), INTENT (INOUT) :: u
    REAL (pr)  :: scl(1:n_var),scl_fltwt
    REAL (pr), INTENT (IN) :: t_local
    INTEGER :: i !TEST

! 
! User defined variables
!
    REAL (pr), DIMENSION (nlocal) :: p

  u(:,1) = -( tanh((sqrt(x(:,1)**2+x(:,2)**2)+radius)/delta) + tanh((sqrt(x(:,1)**2+x(:,2)**2)-radius)/delta))*0.5_pr*(1.0_pr-0.125_pr)+1.0_pr

  u(:,2) = ( tanh((sqrt(x(:,1)**2)+radius)/delta) + tanh((sqrt(x(:,1)**2)-radius)/delta))*0.5_pr*(1.0_pr-0.125_pr)

  u(:,3) = -( tanh((sqrt(x(:,2)**2)+radius)/delta) + tanh((sqrt(x(:,2)**2)-radius)/delta))*0.5_pr*(1.0_pr-0.125_pr)

  p = -( tanh((sqrt(x(:,1)**2+x(:,2)**2)+radius)/delta) + tanh((sqrt(x(:,1)**2+x(:,2)**2)-radius)/delta))*.5*(1.0-0.1)+1.0_pr
  u(:,4) = p(:)/(gamma-1.0_pr) + 0.5_pr*SUM(u(:,2:1+dim)**2,DIM=2)/u(:,1)

! Save pressure at initial condition
  u(:,5) = p(:)

  IF (verb_level.GT.0) &
       WRITE( *,'( "in user_IC t_local, nwlt " , f30.20, 1x , i5.5 )' ) t_local , nlocal


  END SUBROUTINE user_initial_conditions

!--********************************  
  ! Arguments
  ! u         - field on adaptive grid
  ! nlocal      - number of active points
  ! ne_local       - number of equations
  ! t         - current time
  !
  ! Global variables used
  ! ifn       - number of equations
  ! x()
  ! xO
  ! y0
  !--******************************** 
  SUBROUTINE user_algebraic_BC (Lu, u, nlocal, ne_local, jlev, meth)
    !--Defines boundary condition type
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: jlev, meth, ne_local, nlocal
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (INOUT) :: Lu
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (IN)    :: u

    INTEGER :: ie, ii, shift
    REAL (pr), DIMENSION (nlocal,ne_local) :: du, d2u


    !
    ! There are periodic BC conditions
    !


  END SUBROUTINE user_algebraic_BC

  SUBROUTINE user_algebraic_BC_diag (Lu, nlocal, ne_local, jlev, meth)
    !--Defines boundary condition type
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: jlev, meth, ne_local, nlocal
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (INOUT) :: Lu

    INTEGER :: ie, ii, shift
    REAL (pr), DIMENSION (nlocal,ne_local) :: du, d2u

    !
    ! There are periodic BC conditions
    !


 
  END SUBROUTINE user_algebraic_BC_diag



  SUBROUTINE user_algebraic_BC_rhs (rhs, ne_local, nlocal, jlev)
    !--Sets rhs for boundary conditions
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: ne_local, nlocal, jlev
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (INOUT) :: rhs

    INTEGER :: ie, ii, shift
    !
    ! There are periodic BC conditions
    !

  END SUBROUTINE user_algebraic_BC_rhs


  FUNCTION Laplace (jlev, u, nlocal, ne_local, meth)
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: jlev, nlocal, ne_local, meth
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (INOUT) :: u
    REAL (pr), DIMENSION (nlocal*ne_local) :: Laplace

    INTEGER :: i, ii, ie, shift, meth1, meth2, idim
    REAL (pr), DIMENSION (ne_local ,nlocal,dim) :: du
    REAL (pr), DIMENSION (ne_local*dim ,nlocal,dim) :: d2u
    REAL (pr), DIMENSION (ne_local*dim ,nlocal,dim) :: du_dummy ! passed when only calculating 1st derivative.
    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc

    meth1 = meth + 2
    meth2 = meth + 4

    !
	! Find 1st deriviative of u. 
    !
	CALL c_diff_fast (u, du, d2u, jlev, nlocal, meth1, 10, ne_local, 1, ne_local)
!PRINT *,'Laplace du', MINVAL(du), MAXVAL(du)

    !
	! Find 2nd deriviative of u.  d( du ) 
    !
	IF ( TYPE_DB .NE. DB_TYPE_LINES ) THEN 
		CALL c_diff_fast(du, d2u, du_dummy, jlev, nlocal, meth2, 10, ne_local*dim, 1, ne_local*dim )
	ELSE !db_lines

		! Load du  into db
		DO ie=1,ne_local		
			CALL update_db_from_u(  du(ie,1:nlocal,1:dim)  , nlocal ,dim  , 1, dim, 1+(ne_local-1)*dim  ) !Load du	    
		END DO

		! find  2nd deriviative of u (so we first derivative of du/dx)
		CALL c_diff_fast_db(d2u, du_dummy, jlev, nlocal, meth2, 10, ne_local*dim,&
			 1,         &  ! MIN(mn_varD,mn_varD2)
			 ne_local*dim,    &  ! MAX(mx_varD,mx_varD2)
			 1,         &  ! mn_varD  :   min variable in db that we are doing the 1st derivatives for.
			 ne_local*dim,    &  ! mn_varD  :   max variable in db that we are doing the 1st derivatives for.
			 0 ,        &  ! mn_varD2 :   min variable in db that we are doing the 2nd derivatives for.
			 0     )  ! mn_varD2 :   max variable in db that we are doing the 2nd derivatives for.
!PRINT *,'Laplace d2u', MINVAL(d2u), MAXVAL(d2u)

	END IF
    ! Now:
	! d2u(1,:,1) = d^2 U/ d^2 x
	! d2u(1,:,2) = d^2 U/ d^2 y
	! d2u(1,:,3) = d^2 U/ d^2 z
	! d2u(2,:,1) = d^2 V/ d^2 x
	! d2u(2,:,2) = d^2 V/ d^2 y
	! d2u(2,:,3) = d^2 V/ d^2 z
	! d2u(3,:,1) = d^2 W/ d^2 x
	! d2u(3,:,2) = d^2 W/ d^2 y
	! d2u(3,:,3) = d^2 W/ d^2 z

    DO ie = 1, ne_local
       shift=(ie-1)*nlocal
       !--- Internal points
       !--- div(grad)
	   idim = 1
       Laplace(shift+1:shift+Nwlt_lev(jlev,0)) = d2u( (ie-1)*dim+idim ,1:Nwlt_lev(jlev,0),1)
       DO idim = 2,dim
          Laplace(shift+1:shift+Nwlt_lev(jlev,0)) = Laplace(shift+1:shift+Nwlt_lev(jlev,0)) + &
				d2u((ie-1)*dim+idim ,1:Nwlt_lev(jlev,0),idim)
       END DO
       !--Boundary points
       i_p_face(0) = 1
       DO i=1,dim
          i_p_face(i) = i_p_face(i-1)*3
       END DO
       DO face_type = 0, 3**dim - 1
          face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
          IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
             CALL get_all_indices_by_face (face_type, jlev, nloc, iloc)
             IF(nloc > 0 .AND. dim == 3) THEN ! 3-D case only
                IF( ALL( face == (/-1,0,0/) ) ) THEN  ! Xmin face, no edges or corners
                   Laplace(shift+iloc(1:nloc)) = u(shift+iloc(1:nloc))  !Dirichlet conditions
                ELSE IF( ALL( face == (/1,0,0/) ) ) THEN  ! Xmax face, no edges or corners
                   Laplace(shift+iloc(1:nloc)) = u(shift+iloc(1:nloc))  !Dirichlet conditions
!!$                Laplace(shift+iloc(1:nloc)) = du(ie,iloc(1:nloc),1)  !Neuman conditions
                ELSE IF( ALL( face(2:dim) == (/-1,0/) ) ) THEN  ! Ymin face, edges || to Z, no corners
                   Laplace(shift+iloc(1:nloc)) = u(shift+iloc(1:nloc))  !Dirichlet conditions
!!$                Laplace(shift+iloc(1:nloc)) = du(ie,iloc(1:nloc),2)  !Neuman conditions
                ELSE IF( ALL( face(2:dim) == (/1,0/) ) ) THEN  ! Ymax face, edges || to Z, no corners
                   Laplace(shift+iloc(1:nloc)) = u(shift+iloc(1:nloc))  !Dirichlet conditions
!!$                Laplace(shift+iloc(1:nloc)) = du(ie,iloc(1:nloc),2)  !Neuman conditions
                ELSE IF( face(3) == -1 ) THEN  ! entire Zmin face
                   Laplace(shift+iloc(1:nloc)) = u(shift+iloc(1:nloc))  !Dirichlet conditions
!!$                Laplace(shift+iloc(1:nloc)) = du(ie,iloc(1:nloc),3)  !Neuman conditions
                ELSE IF( face(3) == 1 ) THEN  ! entire Zmax face
                   Laplace(shift+iloc(1:nloc)) = u(shift+iloc(1:nloc))  !Dirichlet conditions
!!$                Laplace(shift+iloc(1:nloc)) = du(ie,iloc(1:nloc),3)  !Neuman conditions
                END IF
             END IF
          END IF
       END DO
    END DO
  END FUNCTION Laplace

  FUNCTION Laplace_diag (jlev,  nlocal, ne_local, meth)
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: jlev, nlocal, ne_local, meth
    REAL (pr), DIMENSION (nlocal*ne_local) :: Laplace_diag

    INTEGER :: i, ii, ie, shift, meth1, meth2
    REAL (pr), DIMENSION (nlocal,dim) :: du, d2u
    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc

    meth1 = meth + 2
    meth2 = meth + 4

 
!PRINT *,'IN Laplace_diag, ne_local = ', ne_local

    DO ie = 1, ne_local
       shift=(ie-1)*nlocal
       !--- div(grad)
!PRINT *,'CAlling c_diff_diag from Laplace_diag() '
!PRINT *,'--- jlev, nlocal, meth1, meth2', jlev, nlocal, meth1, meth2

       CALL c_diff_diag ( du, d2u, jlev, nlocal, meth1, meth2, -11)

       Laplace_diag(shift+1:shift+Nwlt_lev(jlev,0)) = d2u(1:Nwlt_lev(jlev,0),1) + d2u(1:Nwlt_lev(jlev,0),2)
       IF (dim==3) Laplace_diag(shift+1:shift+Nwlt_lev(jlev,0)) = &
            Laplace_diag(shift+1:shift+Nwlt_lev(jlev,0))+d2u(1:Nwlt_lev(jlev,0),3)

       !--Boundary points
       i_p_face(0) = 1
       DO i=1,dim
          i_p_face(i) = i_p_face(i-1)*3
       END DO
       DO face_type = 0, 3**dim - 1
          face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
          IF( ANY( face(1:dim) /= 0) ) THEN ! goes only through boundary points
             CALL get_all_indices_by_face (face_type, jlev, nloc, iloc)
             IF(nloc > 0 .AND. dim == 3) THEN ! 3-D case only
                IF( ALL( face == (/-1,0,0/) ) ) THEN                         ! Xmin face, no edges or corners
                   Laplace_diag(shift+iloc(1:nloc)) = 1.0_pr                 !Dirichlet conditions
                ELSE IF( ALL( face == (/1,0,0/) ) ) THEN                     ! Xmax face, no edges or corners
                   Laplace_diag(shift+iloc(1:nloc)) = 1.0_pr                 !Dirichlet conditions
!!$                Laplace_diag(shift+iloc(1:nloc)) = du(iloc(1:nloc),1)     !Neuman conditions
                ELSE IF( ALL( face(2:dim) == (/-1,0/) ) ) THEN               ! Ymin face, edges || to Z, no corners
                   Laplace_diag(shift+iloc(1:nloc)) = 1.0_pr                 !Dirichlet conditions
!!$                Laplace_diag(shift+iloc(1:nloc)) = du(iloc(1:nloc),2)     !Neuman conditions
                ELSE IF( ALL( face(2:dim) == (/1,0/) ) ) THEN                ! Ymax face, edges || to Z, no corners
                   Laplace_diag(shift+iloc(1:nloc)) = 1.0_pr                 !Dirichlet conditions
!!$                Laplace_diag(shift+iloc(1:nloc)) = du(iloc(1:nloc),2)     !Neuman conditions
                ELSE IF( face(3) == -1 ) THEN                                ! entire Zmin face
                   Laplace_diag(shift+iloc(1:nloc)) = 1.0_pr                 !Dirichlet conditions
!!$                Laplace_diag(shift+iloc(1:nloc)) = du(iloc(1:nloc),3)     !Neuman conditions
                ELSE IF( face(3) == 1 ) THEN                                 ! entire Zmax face
                   Laplace_diag(shift+iloc(1:nloc)) = 1.0_pr                 !Dirichlet conditions
!!$                Laplace_diag(shift+iloc(1:nloc)) = du(iloc(1:nloc),3)     !Neuman conditions
                END IF
             END IF
          END IF
       END DO
    END DO
  END FUNCTION Laplace_diag


  FUNCTION user_rhs (u_integrated,scalar)
    IMPLICIT NONE
    REAL (pr), DIMENSION (ng,ne), INTENT(IN) :: u_integrated !1D flat version of integrated variables without BC 
    REAL (pr), DIMENSION (ng), INTENT(IN) :: scalar
    REAL (pr), DIMENSION (n) :: user_rhs

    INTEGER :: i, j, ie, shift
    INTEGER, PARAMETER :: meth=1
    REAL (pr), DIMENSION (MAX(ne,dim+2),ng,dim) :: du, d2u
    !
    ! User defined variables
    !
    REAL (pr), DIMENSION (ng,n_integrated,dim) :: F
    REAL (pr), DIMENSION (ng,dim+1) :: v ! velocity components + temperature
    REAL (pr), DIMENSION (ng) :: p, mu 

    !set up for arbitrary dimensionality

    p(:) = (gamma-1.0_pr)*( u_integrated(:,dim+2)-0.5_pr*SUM(u_integrated(:,2:1+dim)**2,DIM=2)/u_integrated(:,1) ) ! pressure

    !
    !--Form right hand side of Euler Equations
    !
    DO j = 1, dim
       F(:,1,j) = u_integrated(:,1+j) ! rho*u_j
       DO i = 1, dim
          F(:,i+1,j) = u_integrated(:,i+1)*u_integrated(:,j+1)/u_integrated(:,1) ! rho*u_i*u_j
       END DO
       F(:,j+1,j) = F(:,j+1,j) + p(:) ! rho*u_i^2+p
       F(:,dim+2,j) = ( u_integrated(:,dim+2) + p(:) )*u_integrated(:,j+1)/u_integrated(:,1) ! (e+p)*u_j
    END DO
    !
    !-- Form right hand side of Navier-Stokes Equations
    !
    IF(NS) THEN 
       DO i = 1, dim
          v(:,i) = u_integrated(:,1+i) / u_integrated(:,1) ! u_i
       END DO
       v(:,dim+1) = gamma* p / u_integrated(:,1)  ! T
       CALL c_diff_fast(v, du(1:dim+1,:,:), d2u(1:dim+1,:,:), j_lev, ng, meth, 10, dim+1, 1, dim+1) ! du_i/dx_j & dT/dx_j
       mu(:) = v(:,dim+1)**alpha/Re  ! viscosity / thermal conductivity
       p(:) = du(1,:,1) !reuse of a variable
       DO i = 2, dim
          p(:) = p(:) + du(i,:,i) !du_i/dx_i
       END DO
       DO j = 1, dim
          DO i = 1, dim
             F(:,i+1,j) = F(:,i+1,j) - mu(:)*( du(i,:,j)+du(j,:,i) )! rho*u_i*u_j - 2*mu*S_ij/Re  
          END DO
          F(:,j+1,j) = F(:,j+1,j) + 2.0_pr/3.0_pr*mu(:)*p(:) ! rho*u_i*u_j - tau_ij/Re  [ tau_ij = 2*mu*( S_ij - 1/3*S_kk*delta_ij ) ] 
          DO i = 1, dim
             F(:,dim+2,j) =  F(:,dim+2,j) - mu(:)*( du(i,:,j)+du(j,:,i) )*v(:,i) ! (e+p)*u_j - u_i*2*mu*S_ij/Re 
          END DO
                          ! (e+p)*u_j - u_i*tau_ij/Re - q_j/Re/Pr [ q_j = mu dT/dx_j ]
          F(:,dim+2,j) =  F(:,dim+2,j) + 2.0_pr/3.0_pr*mu(:)*p(:)*v(:,j) - mu(:)/Pra/(gamma-1.0_pr)*du(dim+1,:,j)
       END DO
    END IF
    
    user_rhs = 0.0_pr
    DO j = 1, dim
       CALL c_diff_fast(F(:,1:dim+2,j), du(1:dim+2,:,:), d2u(1:dim+2,:,:), j_lev, ng, meth, 10, dim+2, 1, dim+2) ! du(i,:,k)=dF_ij/dx_k
       DO i = 1, dim+2
          shift=(i-1)*ng
          user_rhs(shift+1:shift+ng) = user_rhs(shift+1:shift+ng) - du(i,:,j)  ! -dF_ij/dx_j
       END DO
    END DO

    !---------------- Algebraic BC are set up in user_algebraic_BC and they autmatically overwrite evolution BC

  END FUNCTION user_rhs

! find Jacobian of Right Hand Side of the problem
  FUNCTION user_Drhs (u, u_prev, meth)
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: meth
	!u_prev_timestep passed local to cast it into 2dim array to work with vector derivatives
    REAL (pr), DIMENSION (ng,ne) :: u, u_prev
    REAL (pr), DIMENSION (n) :: user_Drhs

    INTEGER :: i, j, ie, shift
    REAL (pr), DIMENSION (MAX(ne,dim+2),ng,dim) :: du, d2u
    !
    ! User defined variables
    !
    REAL (pr), DIMENSION (ng,n_integrated,dim) :: F
    REAL (pr), DIMENSION (ng,dim+1) :: v, v_prev ! velocity components + temperature
    REAL (pr), DIMENSION (ng) :: p, mu, p_prev, mu_prev 

    p_prev(:) = (gamma-1.0_pr)*( u_prev(:,dim+2)-0.5_pr*SUM(u_prev(:,2:1+dim)**2,DIM=2)/u_prev(:,1) ) ! pressure
    p(:) = (gamma-1.0_pr)*( u(:,dim+2) - SUM(u_prev(:,2:1+dim)*u(:,2:1+dim),DIM=2)/u_prev(:,1) &
                                + 0.5_pr*SUM(u_prev(:,2:1+dim)**2,DIM=2)/u_prev(:,1)**2*u(:,1) ) ! pressure
    !
    !--Form right hand side of Euler Equations
    !
    DO j = 1, dim
       F(:,1,j) = u(:,1+j) ! rho*u_j
       DO i = 1, dim ! (rho*u_i*u_j)'
          F(:,i+1,j) = -u_prev(:,i+1)*u_prev(:,j+1)/u_prev(:,1)**2*u(:,1) +(u(:,i+1)*u_prev(:,j+1)+u_prev(:,i+1)*u(:,j+1))/u_prev(:,1)
       END DO
       F(:,j+1,j) = F(:,j+1,j) + p(:) ! (rho*u_i^2)'+p'
       F(:,dim+2,j) = ( u(:,dim+2) + p(:) )*u_prev(:,j+1)/u_prev(:,1) & ! ((e+p)*u_j)'
                    + ( u_prev(:,dim+2) + p_prev(:) )*(u(:,j+1)/u_prev(:,1)-u_prev(:,j+1)/u_prev(:,1)**2*u(:,1) )
    END DO
    !
    !-- Form right hand side of Navier-Stokes Equations
    !
    IF(NS) THEN 
       DO i = 1, dim
          v_prev(:,i) = u_prev(:,1+i) / u_prev(:,1) ! u_i
          v(:,i) = u(:,1+i) / u_prev(:,1) - u_prev(:,1+i) / u_prev(:,1)**2 * u(:,1) ! u'_i
       END DO
       v_prev(:,dim+1) = gamma* p_prev / u_prev(:,1)  ! T
       v(:,dim+1) = gamma* (p / u_prev(:,1) - p_prev / u_prev(:,1)**2 *  u(:,1) )  ! T'
       mu_prev(:) = v_prev(:,dim+1)**alpha/Re  ! viscosity / thermal conductivity
       mu(:) = alpha*v_prev(:,dim+1)**(alpha-1.0_pr)*v(:,dim+1)/Re  ! viscosity / thermal conductivity
       ! mu_prev*(...)'
       CALL c_diff_fast(v, du(1:dim+1,:,:), d2u(1:dim+1,:,:), j_lev, ng, meth, 10, dim+1, 1, dim+1) ! du'_i/dx_j & dT'/dx_j
       p(:) = du(1,:,1) !reuse of a variable
       DO i = 2, dim
          p(:) = p(:) + du(i,:,i) !du'_i/dx_i
       END DO
       DO j = 1, dim
          DO i = 1, dim
             F(:,i+1,j) = F(:,i+1,j) - mu_prev(:)*( du(i,:,j)+du(j,:,i) )! rho*u_i*u_j - 2*mu*S_ij/Re  
          END DO
          F(:,j+1,j) = F(:,j+1,j) + 2.0_pr/3.0_pr*mu_prev(:)*p(:) ! rho*u_i*u_j - tau_ij/Re  [ tau_ij = 2*mu*( S_ij - 1/3*S_kk*delta_ij ) ] 
          DO i = 1, dim
             F(:,dim+2,j) =  F(:,dim+2,j) - mu_prev(:)*( du(i,:,j)+du(j,:,i) )*v_prev(:,i) ! (e+p)*u_j - u_i*2*mu*S_ij/Re 
          END DO
                          ! (e+p)*u_j - u_i*tau_ij/Re - q_j/Re/Pr [ q_j = mu dT/dx_j ]
          F(:,dim+2,j) =  F(:,dim+2,j) + 2.0_pr/3.0_pr*mu_prev(:)*p(:)*v_prev(:,j) - mu_prev(:)/Pra/(gamma-1.0_pr)*du(dim+1,:,j)
       END DO
       ! mu'*(...)_prev
       CALL c_diff_fast(v_prev, du(1:dim+1,:,:), d2u(1:dim+1,:,:), j_lev, ng, meth, 10, dim+1, 1, dim+1) ! du_i/dx_j & dT/dx_j
       p(:) = du(1,:,1) !reuse of a variable
       DO i = 2, dim
          p(:) = p(:) + du(i,:,i) !du_i/dx_i
       END DO
       DO j = 1, dim
          DO i = 1, dim
             F(:,i+1,j) = F(:,i+1,j) - mu(:)*( du(i,:,j)+du(j,:,i) )! rho*u_i*u_j - 2*mu'*S_ij/Re  
          END DO
          F(:,j+1,j) = F(:,j+1,j) + 2.0_pr/3.0_pr*mu(:)*p(:) ! rho*u_i*u_j - tau_ij/Re  [ tau_ij = 2*mu'*( S_ij - 1/3*S_kk*delta_ij ) ] 
          DO i = 1, dim
             F(:,dim+2,j) =  F(:,dim+2,j) - mu_prev(:)*( du(i,:,j)+du(j,:,i) )*v(:,i) - mu(:)*( du(i,:,j)+du(j,:,i) )*v_prev(:,i) 
          END DO
                          ! (e+p)*u_j - u_i*tau_ij/Re - q_j/Re/Pr [ q_j = mu dT/dx_j ]
          F(:,dim+2,j) =  F(:,dim+2,j) + 2.0_pr/3.0_pr*mu_prev(:)*p(:)*v(:,j) + 2.0_pr/3.0_pr*mu(:)*p(:)*v_prev(:,j) &
                       - mu(:)/Pra/(gamma-1.0_pr)*du(dim+1,:,j)
       END DO
    END IF

    user_Drhs = 0.0_pr
    DO j = 1, dim
       CALL c_diff_fast(F(:,1:dim+2,j), du(1:dim+2,:,:), d2u(1:dim+2,:,:), j_lev, ng, meth, 10, dim+2, 1, dim+2) ! du(i,:,k)=dF_ij/dx_k
       DO i = 1, dim+2
          shift=(i-1)*ng
          user_Drhs(shift+1:shift+ng) = user_Drhs(shift+1:shift+ng) - du(i,:,j)  ! -dF_ij/dx_j
       END DO
    END DO

  END FUNCTION user_Drhs


  !
  ! Uses u_prev_timestep, which is global flat (1D ) array set in time_step_cn()
  !
  FUNCTION user_Drhs_diag (meth)
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: meth
    REAL (pr), DIMENSION (n) :: user_Drhs_diag

    INTEGER :: i,j, ie, shift
    REAL (pr), DIMENSION (ng,dim) :: du_diag, d2u_diag
    REAL (pr), DIMENSION (MAX(ne,dim+2),ng,dim) :: du, d2u
!
! User defined variables
!
    REAL (pr), DIMENSION (ng,n_integrated,dim) :: F
    REAL (pr), DIMENSION (ng,ne) :: u_prev
    REAL (pr), DIMENSION (ng,dim+2) :: v_prev ! velocity components + temperature
    REAL (pr), DIMENSION (ng) :: mu_prev, mu_T_prev, p 

! Create u_prev from u_prev_timestep
    DO ie = 1, ne
       shift=(ie-1)*ng
       u_prev(1:ng,ie) = u_prev_timestep(shift+1:shift+ng)
    END DO
    !
    ! Calculate Diagonal derivative term
    !
    CALL c_diff_diag(du_diag, d2u_diag, j_lev, ng, meth, meth, 11)
    !
    !--Form right hand side of Euler Equations
    !
    DO j = 1, dim
       F(:,1,j) = 0.0_pr
       DO i = 1, dim ! (rho*u_i*u_j)'
          F(:,i+1,j) = u_prev(:,j+1)/u_prev(:,1)
       END DO
       F(:,j+1,j) =  F(:,j+1,j) + u_prev(:,j+1)/u_prev(:,1) - (gamma-1.0_pr)*u_prev(:,j+1)/u_prev(:,1)
       F(:,dim+2,j) = gamma*u_prev(:,j+1)/u_prev(:,1)
    END DO
    
    IF(NS) THEN 
       DO i = 1, dim
          v_prev(:,i) = u_prev(:,1+i) / u_prev(:,1) ! u_i
       END DO
       p(:) = (gamma-1.0_pr)*( u_prev(:,dim+2)-0.5_pr*SUM(u_prev(:,2:1+dim)**2,DIM=2)/u_prev(:,1) ) ! pressure
       v_prev(:,dim+1) = gamma* p / u_prev(:,1)  ! T
       mu_prev(:) = v_prev(:,dim+1)**alpha/Re  ! viscosity / thermal conductivity
       mu_T_prev(:) = alpha*v_prev(:,dim+1)**(alpha-1.0_pr)/Re*gamma*(gamma-1.0_pr)/u_prev(:,1)  ! dmu/dT * gamma*(gamma-1)/rho
       ! mu_prev*(...)'
       CALL c_diff_fast(v_prev(:,1:dim), du(1:dim,:,:), d2u(1:dim,:,:), j_lev, ng, meth, 10, dim, 1, dim) ! du_i/dx_j & d/dx_j
       p(:) = du(1,:,1) !reuse of a variable
       DO i = 2, dim
          p(:) = p(:) + du(i,:,i) !du_i/dx_i
       END DO
       DO j = 1, dim
          DO i = 1, dim
             F(:,i+1,j) = F(:,i+1,j) + mu_T_prev(:)*u_prev(:,1+i)/u_prev(:,1)*( du(i,:,j)+du(j,:,i) ) 
          END DO
          F(:,j+1,j) = F(:,j+1,j) - 2.0_pr/3.0_pr*mu_T_prev(:)*u_prev(:,1+j)/u_prev(:,1)*p(:) 
          DO i = 1, dim
             F(:,dim+2,j) =  F(:,dim+2,j) - mu_T_prev(:)*( du(i,:,j)+du(j,:,i) )*v_prev(:,i) ! (e+p)*u_j - u_i*2*mu*S_ij/Re 
          END DO
                          ! (e+p)*u_j - u_i*tau_ij/Re - q_j/Re/Pr [ q_j = mu dT/dx_j ]
          F(:,dim+2,j) =  F(:,dim+2,j) + 2.0_pr/3.0_pr*mu_T_prev(:)*p(:)*v_prev(:,j) 
       END DO
    END IF

    user_Drhs_diag = 0.0_pr
    DO j = 1, dim
       CALL c_diff_fast(F(:,1:dim+2,j), du(1:dim+2,:,:), d2u(1:dim+2,:,:), j_lev, ng, meth, 10, dim+2, 1, dim+2) ! du(i,:,k)=dF_ij/dx_k
       DO i = 1, dim+2
          shift=(i-1)*ng
          user_Drhs_diag(shift+1:shift+ng) = user_Drhs_diag(shift+1:shift+ng) - F(:,i,j)*du_diag(:,j)
!!$          IF(NS .AND. (i >=2 .AND. i <= dim+1) ) user_Drhs_diag(shift+1:shift+ng) = user_Drhs_diag(shift+1:shift+ng) &
!!$                                                                                  + mu_prev(:)/u_prev(:,1)*d2u_diag(:,j)
!!$          IF(NS .AND. i == j+1 ) user_Drhs_diag(shift+1:shift+ng) = user_Drhs_diag(shift+1:shift+ng) &
!!$                                                         + 1.0_pr/3.0_pr*mu_prev(:)/u_prev(:,1)*d2u_diag(:,j)
          IF(NS .AND. i == dim+2 ) user_Drhs_diag(shift+1:shift+ng) = user_Drhs_diag(shift+1:shift+ng) &
                                                                    + mu_prev(:)*gamma/Pra/u_prev(:,1)*d2u_diag(:,j)
       END DO
    END DO

  END FUNCTION user_Drhs_diag

  SUBROUTINE user_project (u, p, nlocal, meth)
    !--Makes u divergence free
    IMPLICIT NONE

    INTEGER, INTENT (IN) :: meth, nlocal
    REAL (pr), DIMENSION (nlocal),     INTENT(INOUT) :: p
    REAL (pr), DIMENSION (nlocal,n_integrated), INTENT(INOUT) :: u

    REAL (pr), DIMENSION (nlocal) :: dp
    REAL (pr), DIMENSION (nlocal) :: f


  END SUBROUTINE user_project

  FUNCTION user_chi (nlocal, t_local )
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: nlocal
    REAL (pr), INTENT (IN) :: t_local
    REAL (pr), DIMENSION (nlocal) :: user_chi
!
!  Create field object here, 1 inside and 0 outside
!
    user_chi = 0.0_pr
  END FUNCTION user_chi


  !
  ! Calculate any statitics
  !
  ! startup_flag - 0 when adapting to IC,then 1 inmain integration loop
  !
  SUBROUTINE user_stats ( u, j_mn_local, startup_flag)
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: startup_flag
    INTEGER , INTENT (IN) :: j_mn_local 
    REAL (pr), DIMENSION (nwlt,1:n_var), INTENT (IN) :: u
!
! Generate statisitics with variables previously defined in this module
!


  END SUBROUTINE user_stats


  SUBROUTINE user_cal_force (u, n, t_local, force, drag, lift)
    !--Calculates drag and lift on obstacle using penalization formula
    IMPLICIT NONE

    INTEGER, INTENT (IN) :: itype, n
    REAL (pr), INTENT (IN) :: t_local
    REAL (pr), DIMENSION (dim), INTENT (INOUT) :: force
    REAL (pr), INTENT (OUT) :: drag, lift
    REAL (pr), DIMENSION (n,dim) :: u
    drag = 0.0_PR
    lift = 0.0_PR

    !
    ! There is no obstacle in flow
    !

  END SUBROUTINE user_cal_force

  !
  ! Read input from "case_name"_pde.inp file
  ! case_name is in string file_name read from command line
  ! in read_command_line_input()
  ! 
  SUBROUTINE user_read_input()
    IMPLICIT NONE

  call input_real ('Re',Re,'stop',' Re: Reynolds Number')
  call input_real ('Pra',Pra,'stop',' Pr: Prandtl Number')
  call input_real ('gamma',gamma,'stop',' gamma: Specific Heat Ratio')
  call input_real ('alpha',alpha,'stop',' alpha: power law for both viscosity and conductivity')
  call input_real ('delta',delta,'stop',' delta: width of initial condition')
  call input_real ('radius',radius,'stop',' radius: radius of initial condition')



  END SUBROUTINE user_read_input



  !
  ! calculate any additional variables
  ! 
  ! arg 
  ! flag  - 0 calledwhile adapting to IC, 1 - called in main time integration loop
  !
  ! These additiona variables are calculated and left in real space.
  !
  SUBROUTINE user_additional_vars(t_local, flag )
    IMPLICIT NONE
    REAL (pr), INTENT (IN) ::  t_local
    INTEGER , INTENT(IN) :: flag ! 0- called during adaption to IC, 1 called during main integration loop
!
! Caluculate additional field variables
!

! Calculate pressure
    IF (dim==1) THEN
       u(:,n_var_pressure) = (gamma-1.0_pr)*( u(:,3)-0.5_pr*u(:,2)**2/u(:,1) )
    END IF
    IF (dim==2) THEN
       u(:,n_var_pressure) = (gamma-1.0_pr)*(u(:,4)-0.5_pr*(u(:,2)**2+u(:,3)**2)/u(:,1))
    END IF
    IF (dim==3) THEN
       u(:,n_var_pressure) = (gamma-1.0_pr)*(u(:,5)-0.5_pr*( u(:,2)**2+u(:,3)**2 &
                              +u(:,4)**2 )/u(:,1))
    END IF

 
  END SUBROUTINE user_additional_vars


  !
  ! calculate any additional scalar variables
  ! 
  ! arg 
  ! flag  - 0 calledwhile adapting to IC, 1 - called in main time integration loop
  !
  SUBROUTINE user_scalar_vars( flag )
    IMPLICIT NONE
    INTEGER , INTENT(IN) :: flag ! 0- called during adaption to IC, 1 called during main integration loop



  END SUBROUTINE user_scalar_vars

  !
  !************ Calculating Scales ***************************
  !
  ! Note the order of the components in the scl array
  ! correspond to u_tn, v_tn, w_tn, u_tn-1, v_tn-1, w_tn-1, u_tn-2, v_tn-2, w_tn-2
  ! 
  SUBROUTINE user_scales(use_default, u, nlocal, ne_local, l_n_var_adapt , l_n_var_adapt_index, &
       scl, scl_fltwt) !add
    IMPLICIT NONE
    LOGICAL , INTENT(INOUT) :: use_default
    INTEGER, INTENT (IN) :: nlocal, ne_local
    REAL (pr), DIMENSION (1:nlocal,1:ne_local), INTENT (IN) :: u
    LOGICAL , INTENT (IN) :: l_n_var_adapt(ne_local)
    INTEGER , INTENT (IN) :: l_n_var_adapt_index(1:ne_local)
    REAL (pr), DIMENSION (1:ne_local), INTENT (INOUT) :: scl
    REAL (pr) ,  INTENT(IN) ::scl_fltwt !weight for temporal filter on scl

    !
    ! Ignore the output of this routine and use default scales routine
    !
    use_default = .TRUE. 

    !
    ! NOTE: For a parallel run, synchronize scl(:) across the processors in user_scales.
    !       Use the subroutine scales of default_util.f90 as an example,
    !       subroutine parallel_global_sum of parallel.f90 may be helpful.
    !       Already synchronized: sumdA_global
  END SUBROUTINE user_scales

SUBROUTINE user_cal_cfl (use_default, u, cfl_out)
  USE precision
  USE sizes
  USE pde
  USE parallel
  IMPLICIT NONE
  LOGICAL , INTENT(INOUT) :: use_default
  REAL (pr),                                INTENT (INOUT) :: cfl_out
  REAL (pr), DIMENSION (nwlt,n_integrated), INTENT (IN)    :: u

  INTEGER                    :: i
  REAL (pr)                  :: floor
  REAL (pr), DIMENSION (dim) :: cfl
  REAL (pr), DIMENSION(dim,nwlt) :: h_arr

  use_default = .FALSE.

  floor = 1e-12_pr
  cfl_out = floor
  
  CALL get_all_local_h (h_arr)
  
  DO i = 1, nwlt
     cfl(1:dim) = ABS ( u(i,1:dim) * dt/h_arr(1:dim,i) )
     cfl_out = MAX (cfl_out, MAXVAL(cfl))
  END DO
  CALL parallel_global_sum( REALMAXVAL=cfl_out )
  
END SUBROUTINE user_cal_cfl

  !******************************************************************************************
  !************************************* SGS MODEL ROUTINES *********************************
  !******************************************************************************************



!******************************************************************************************
!************************************* SGS MODEL ROUTINES *********************************
!******************************************************************************************


!
! Intialize sgs model
! This routine is called once in the first
! iteration of the main time integration loop.
! weights and model filters have been setup for first loop when this routine is called.
!
SUBROUTINE user_init_sgs_model( )
  IMPLICIT NONE


! LDM: Giuliano

! THE sgs forcign is stored for the time step in sgs_mdl_force(1:nlocal,1:n_integrated)
! where nlocal should be nwlt.


!          print *,'initializing LDM ...'       
!          CALL sgs_mdl_force( u(:,1:n_integrated), nwlt, j_lev, .TRUE.)


END SUBROUTINE user_init_sgs_model

!
! calculate sgs model forcing term
! user_sgs_force is called int he beginning of each times step in time_adv_cn().
! THE sgs forcign is stored for the time step in sgs_mdl_force(1:nlocal,1:n_integrated)
! where nlocal should be nwlt.
! 
! Accesses u from field module, 
!          j_lev from wlt_vars module,
!
SUBROUTINE  user_sgs_force (u_loc, nlocal)
  IMPLICIT NONE

  INTEGER,                         INTENT (IN) :: nlocal
  REAL (pr), DIMENSION (nlocal,n_integrated), INTENT (INOUT) :: u_loc

END SUBROUTINE  user_sgs_force

  FUNCTION user_sound_speed (u, neq, nwlt)
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: nwlt, neq
    REAL (pr), DIMENSION (nwlt,neq), INTENT(IN) :: u
    REAL (pr), DIMENSION (nwlt) :: user_sound_speed

    !user_sound_speed(:) = SQRT( gamma*(gamma-1.0_pr)* &
    !       ( u(:,4)-0.5_pr*SUM(u(:,n_var_mom(1:dim))**2,DIM=2)/u(:,1)) ) ! pressure

  END FUNCTION user_sound_speed

END MODULE user_case
