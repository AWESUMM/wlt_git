#!/bin/sh
# clean the files */results/*

list_dir=`\ls -dF */*|\grep "results\/"`

for dir in $list_dir; do
    \rm -rf "${dir}"
done
