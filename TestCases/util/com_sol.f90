! Result file comparator
! by Alexei Vezolainen
! May 2006
PROGRAM result_file_comparator
  USE com_sol_aux
  USE precision
  IMPLICIT NONE
  
  
  CHARACTER (LEN=MAXLEN) :: fn1, fn2, of1, tmp
  INTEGER :: i, v1, v2, nn, n_var_total
  REAL(pr) :: L1_eps
  INTEGER :: verb_level, &                             ! report verbose level
       npairs, &                                       ! number of variable pairs
       verb_io                                         ! I/O verb level
  LOGICAL :: COMPARE_LOCATIONS, &                      ! do compare grid points
       COMPARE_ALL_VARS                                ! do compare all variable pairs
  INTEGER :: pair(1:2,1:MAX_PAIRS)                     ! variable pairs


  ! read command line parameters
  CALL read_file_type (fn1, fn2, of1, &
       COMPARE_LOCATIONS, COMPARE_ALL_VARS, verb_level, L1_eps, pair, npairs, verb_io )
  
  ! read files, open report file, allocate all variables
  CALL read_var ( TRIM(fn1),TRIM(fn2),TRIM(of1), verb_level, n_var_total, verb_io )
  
  
  ! if required, compare grids
  IF (COMPARE_LOCATIONS) CALL com_loc
  
  
  ! if required, compare variables for each given pair
  IF (COMPARE_ALL_VARS) THEN
     DO i=1,n_var_total
        CALL com_var ( i, i, L1_eps )
     END DO
  ELSE
     DO i=1,npairs
        CALL com_var ( pair(1,i), pair(2,i), L1_eps )
     END DO
  END IF
  
  
  ! close report file, deallocate variables
  CALL clean_var
  
  
END PROGRAM result_file_comparator
