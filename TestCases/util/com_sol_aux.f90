! Result file comparator
! by Alexei Vezolainen
! May 2006
MODULE com_sol_aux
  USE precision
  IMPLICIT NONE

  INTEGER, PARAMETER, PUBLIC :: MAXLEN = 1024                     ! maximum length of command line argument
  INTEGER, PARAMETER, PUBLIC :: MAX_PAIRS = 256                   ! maximum number of variables
  PUBLIC :: read_var,   &                                            ! global user function:
       com_loc,         &                                            !  open files, compare points
       com_var,         &                                            !  and variables
       clean_var,       &                                            !  close
       usage,           &                                            ! print usage message
       read_file_type                                                ! read command line parameters
  

  PRIVATE :: check_globals, &                                        ! private functions
       check_dimension, report, find_different, &
       clean_after_read_solution, ixyz2indx, com_aux, &
       test_alloc

  CHARACTER(LEN=*), PRIVATE, PARAMETER :: EPS_FMT = '(E15.10)'     ! format for the treshold conversion
  CHARACTER(LEN=*), PRIVATE, PARAMETER :: NUM_FMT = '(I5)'         ! format for the variable number conversion

  REAL (pr), DIMENSION (:), POINTER, PRIVATE :: scl, scl1, scl2            ! scl(1:n_var)
  REAL (pr), DIMENSION (:,:), ALLOCATABLE, PRIVATE ::  u1, u2              ! u(1:nwlt,1:n_var)
  INTEGER, DIMENSION (:),ALLOCATABLE, PRIVATE :: indx_i, indx_i1           ! indx_DB ...%i (1:nwlt)
  INTEGER, DIMENSION (:),ALLOCATABLE, PRIVATE :: indx_ixyz, indx_ixyz1     ! indx_DB ...%ixyz (1:nwlt)
  
  INTEGER, PARAMETER, PRIVATE :: MAXDIM = 8                          ! maximum dimension
  INTEGER, PARAMETER, PRIVATE :: REPORT_UNIT = 1                     ! report file unit
  INTEGER, PARAMETER, PRIVATE :: MAXWARNING = 3                      !
  
  LOGICAL, PRIVATE :: READ_ALL_PARALLEL_1, &           ! read data from all the processors for the file 1
       READ_ALL_PARALLEL_2                             ! read data from all the processors for the file 2
  INTEGER, PRIVATE :: &
       REPORT_VERB, &                                  ! report verbose level (0-minimum, 1-some)
       nvar1, nvar2, &                                                       ! n_var and
       nwlt1, nwlt2, dim1, dim2, &                                           ! other globals
       nxyz1(1:MAXDIM), nxyz2(1:MAXDIM), &
       mxyz1(1:MAXDIM), mxyz2(1:MAXDIM),&
       prd1(1:MAXDIM), prd2(1:MAXDIM), grid1(1:MAXDIM), grid2(1:MAXDIM), &
       warnings(0:MAXWARNING)                                                ! warning: 0-fatal, 3-OK
  
CONTAINS
  !-----------------------------------------------------------------
  ! PUBLIC
  ! print usage message
  SUBROUTINE usage
    WRITE (*,'("Usage: compare_solns [FILE_1] [FILE_2] [FILE_OUT] [OPTIONS] [V1 U1 V2 U2 ...]")')
    WRITE (*,'("Options:")')
    WRITE (*,'("  -h --help -?      Show this message")')
    WRITE (*,'("  -v LEVEL          Report verbose level (0-default minimum, 1-some, 2-compare orders of wavelets)")')
    WRITE (*,'("  -g                Do compare grids (do not - by default)")')
    WRITE (*,'("  -sp, -ps          Treat the file as a single processor (s) or parallel (p) one")')
    WRITE (*,'("                    (the default is -pp; -s or -p has the same effect as -ss or -pp)")')
    WRITE (*,'("Miscellaneous Options:")')
    WRITE (*,'("  -V LEVEL          Debug level (0-default minimum, 1-some, 2-more)")')
    WRITE (*,'("  -t VALUE          Print warning if L1 norm exceeds VALUE")')
    WRITE (*,'("  V1, U1, etc       Positions of the variables in the result files to compare, V1 & U1, etc")')
    WRITE (*,'("                    (by defalt - compare all variables in order 1 1 2 2 ...)")')
    STOP 1
  END SUBROUTINE usage
  !-----------------------------------------------------------------
  ! PUBLIC
  !read command line parameters
  SUBROUTINE read_file_type (fn1, fn2, of1, &
       COMPARE_LOCATIONS, COMPARE_ALL_VARS, verb_level, L1_eps, pair, npairs, verb_io)
    USE precision
#ifdef _MSC_VER
    USE DFLIB
#endif

    CHARACTER(LEN=MAXLEN), INTENT(INOUT) :: fn1, fn2, of1
    LOGICAL, INTENT (INOUT) :: COMPARE_LOCATIONS, COMPARE_ALL_VARS
    INTEGER, INTENT (INOUT) :: verb_level, verb_io
    REAL(pr), INTENT(INOUT) :: L1_eps
    INTEGER, INTENT(INOUT) :: npairs
    INTEGER, INTENT(INOUT) :: pair(1:2,1:MAX_PAIRS)
    INTEGER :: i, j, name_counter, pair_counter,iargc
    INTEGER, PARAMETER :: MAX_NAMES = 3
    CHARACTER (LEN=MAXLEN) :: tmp, name(1:MAX_NAMES)

    ! default values -------
    COMPARE_ALL_VARS = .TRUE.
    COMPARE_LOCATIONS = .FALSE.
    READ_ALL_PARALLEL_1 = .TRUE.   ! read data from all the processors for the file 1
    READ_ALL_PARALLEL_2 = .TRUE.   ! read data from all the processors for the file 2
    verb_level = 0
    verb_io = 0
    L1_eps = 0.0_pr
    pair(1:2,1:MAX_PAIRS) = 0
    ! default values -------

    i = 1
    name_counter = 0
    pair_counter = 0
    DO WHILE ( i.LE.iargc() )
       CALL getarg( i, tmp )
       IF ( tmp(1:1).EQ.'-' ) THEN ! ---- parameter
          DO j=2,LEN(TRIM(tmp))
             IF ( ( tmp(j:j).EQ.'h' ).OR. &                     ! -h -help
                  ( tmp(j:j).EQ.'-' ).OR. &                     ! --help
                  ( tmp(j:j).EQ.'?' ) ) CALL usage              ! -?
             IF ( tmp(j:j).EQ.'g' ) COMPARE_LOCATIONS = .TRUE.
             IF ( tmp(j:j).EQ.'s'.AND.j.EQ.2 ) THEN
                READ_ALL_PARALLEL_1 = .FALSE.                ! -s
                READ_ALL_PARALLEL_2 = .FALSE.
             END IF
             IF ( tmp(j:j).EQ.'s'.AND.j.EQ.3 ) &             ! -ss, -ps
                  READ_ALL_PARALLEL_2 = .FALSE.
             IF ( tmp(j:j).EQ.'p'.AND.j.EQ.3 ) THEN
                READ_ALL_PARALLEL_2 = .TRUE.                 ! -pp, -sp
             END IF
             IF ( tmp(j:j).EQ.'t' ) THEN
                i = i + 1
                CALL getarg( i, tmp )
                READ( UNIT=tmp, FMT=EPS_FMT, ERR=2 ) L1_eps
             END IF
             IF ( tmp(j:j).EQ.'v' ) THEN
                i = i + 1
                CALL getarg( i, tmp )
                READ( UNIT=tmp, FMT=NUM_FMT, ERR=1 ) verb_level
             END IF
             IF ( tmp(j:j).EQ.'V' ) THEN
                i = i + 1
                CALL getarg( i, tmp )
                READ( UNIT=tmp, FMT=NUM_FMT, ERR=1 ) verb_io
             END IF
          END DO
       ELSE ! ---- file name
          name_counter = name_counter + 1
          IF (name_counter.GT.MAX_NAMES) THEN ! ---- variables
             pair_counter = pair_counter + 1
             IF (pair_counter.GT.MAX_PAIRS) THEN
                WRITE (*,'("Internal compare_solns error: too many pairs of variables")')
                WRITE (*,'("Please change MAX_PAIRS and recompile")')
                WRITE (*,'("com_sol_aux.f90:22 and  com_sol.f90:10")')
                STOP 1
             END IF
             CALL getarg( i, tmp )
             READ( UNIT=tmp, FMT=NUM_FMT, ERR=3 ) pair( 1, pair_counter )
             i = i + 1
             CALL getarg( i, tmp )
             READ( UNIT=tmp, FMT=NUM_FMT, ERR=3 ) pair( 2, pair_counter )
          ELSE
             CALL getarg( i, name(name_counter))
          END IF
       END IF
       i = i + 1
    END DO
    IF (name_counter.GE.3) THEN
       fn1 = TRIM(name(1))
       fn2 = TRIM(name(2))
       of1 = TRIM(name(3))
    ELSE
       WRITE (*,'("Error: three file names are required")')
       WRITE (*,'(" ")')
       CALL usage
    END IF
    IF (pair_counter.GT.0) THEN
       COMPARE_ALL_VARS = .FALSE.
       npairs = pair_counter
    END IF

    RETURN
1   WRITE (*,'("Error: strange verbose level value")')
    WRITE (*,'("")')
    CALL usage
2   WRITE (*,'("Error: strange L1 norm warning value")')
    WRITE (*,'("")')
    CALL usage
3   WRITE (*,'("Error: strange variable number")')
    WRITE (*,'("")')
    CALL usage

  END SUBROUTINE read_file_type
  !-----------------------------------------------------------------
  ! PUBLIC
  ! read two result files, allocate all the variables
  ! for all the comparisons
  SUBROUTINE read_var ( filename1, filename2, REPORT_FILE, reportverb, n_var_total, verb_io )
    USE pde
    USE field
    USE wlt_vars                  ! dim, j_lev, j_mx
    USE io_3D
    USE io_3D_vars                ! DO_READ_ALL_VARS_TRUE
    USE sizes                     ! nwlt
    USE wlt_trns_vars             ! indx_DB
    
    INTEGER, INTENT(OUT) :: n_var_total
    CHARACTER*(*), INTENT(IN) :: filename1, filename2, REPORT_FILE
    INTEGER, INTENT(IN) :: reportverb, &       ! report verb level
         verb_io                               ! I/O verb level
    INTEGER :: j, wlt_type, face_type, j_df, nn, length, k, ios
    
    
    ! set module internal
    REPORT_VERB = reportverb
    ! initialize warning counter
    warnings(0:MAXWARNING) = 0
    
    ! read the first file --------------------------------------------------------
    CALL read_solution_dim ( filename1, EXACT=.TRUE., VERBLEVEL=verb_io )
    CALL check_dimension (dim)
    IC_filename = filename1
    CALL read_solution ( scl, IC_filename, DO_READ_ALL_VARS_TRUE, &
         IGNOREINIT = .TRUE., APPEND=READ_ALL_PARALLEL_1, VERBLEVEL=verb_io )

    nvar1 = SIZE(u_variable_names(:))
    CALL clean_after_read_solution
    
    ! save grid coordinates
    ALLOCATE (indx_i1(1:nwlt), STAT=ios);        CALL test_alloc (ios)
    ALLOCATE (indx_ixyz1(1:nwlt), STAT=ios);     CALL test_alloc (ios)

    nn = 1
    DO j = 1, j_lev
       DO wlt_type = MIN(j-1,1),2**dim-1
          DO face_type = 0, 3**dim - 1
             DO j_df = j, j_lev
                DO k=1, indx_DB(j_df,wlt_type,face_type,j)%length
                   indx_ixyz1(nn) = indx_DB(j_df,wlt_type,face_type,j)%p(k)%ixyz
                   indx_i1(nn) = indx_DB(j_df,wlt_type,face_type,j)%p(k)%i
!!$                   PRINT *, '# ', nn, indx_ixyz1(nn), indx_i1(nn), u(indx_i1(nn),1)
                   nn = nn + 1
                END DO
             END DO
          END DO
       END DO
    END DO

    ! sort by 1D coordinate indx_ixyz1(:)
    CALL IVEC2_SORT_A  ( nwlt, indx_ixyz1, indx_i1 )
    
    ! save globals
    dim1 = dim
    nwlt1 = nwlt
    nxyz1(1:dim) = nxyz(1:dim)
    mxyz1(1:dim) = mxyz(1:dim)
    prd1(1:dim) = prd(1:dim)
    grid1(1:dim) = grid(1:dim)
    ! save values
    ALLOCATE (scl1(1:nvar1), STAT=ios);          CALL test_alloc (ios)
    scl1(1:nvar1) = scl(1:nvar1)
    DEALLOCATE (scl, STAT=ios);                  CALL test_alloc (ios)
    ALLOCATE (u1(1:nwlt,1:nvar1), STAT=ios);     CALL test_alloc (ios)
    u1 = u
    DEALLOCATE (u, STAT=ios);                    CALL test_alloc (ios)
    CALL deallocate_indx_DB


    ! read the second file --------------------------------------------------------
    CALL read_solution_dim ( filename2, EXACT=.TRUE., VERBLEVEL=verb_io )
    CALL check_dimension (dim)
    IC_filename = filename2
    CALL read_solution ( scl, IC_filename, DO_READ_ALL_VARS_TRUE, &
         IGNOREINIT =.TRUE., APPEND=READ_ALL_PARALLEL_2, VERBLEVEL=verb_io )

    nvar2 = SIZE(u_variable_names(:))
    n_var_total = MAX( nvar1, nvar2 )
    CALL clean_after_read_solution
    
    ! save grid coordinates
    ALLOCATE (indx_i(1:nwlt), STAT=ios);        CALL test_alloc (ios)
    ALLOCATE (indx_ixyz(1:nwlt), STAT=ios);    CALL test_alloc (ios)
    
    nn = 1
    DO j = 1, j_lev
       DO wlt_type = MIN(j-1,1),2**dim-1
          DO face_type = 0, 3**dim - 1
             DO j_df = j, j_lev
                DO k=1, indx_DB(j_df,wlt_type,face_type,j)%length
                   indx_ixyz(nn) = indx_DB(j_df,wlt_type,face_type,j)%p(k)%ixyz
                   indx_i(nn) = indx_DB(j_df,wlt_type,face_type,j)%p(k)%i
!!$                   PRINT *, '##', nn, indx_ixyz1(nn), indx_i1(nn), u(indx_i1(nn),1)
                   nn = nn + 1
                END DO
             END DO
          END DO
       END DO
    END DO

    ! sort by 1D coordinate indx_ixyz(:)
    CALL IVEC2_SORT_A  ( nwlt, indx_ixyz, indx_i )

    ! save globals
    dim2 = dim
    nwlt2 = nwlt
    nxyz2(1:dim) = nxyz(1:dim)
    mxyz2(1:dim) = mxyz(1:dim)
    prd2(1:dim) = prd(1:dim)
    grid2(1:dim) = grid(1:dim)
    ! save values
    ALLOCATE (scl2(1:nvar2), STAT=ios);          CALL test_alloc (ios)
    scl2(1:nvar2) = scl(1:nvar2)
    DEALLOCATE (scl, STAT=ios);                  CALL test_alloc (ios)
    ALLOCATE (u2(1:nwlt2,1:nvar2), STAT=ios);    CALL test_alloc (ios)
    u2 = u
    DEALLOCATE (u, STAT=ios);                    CALL test_alloc (ios)
    CALL deallocate_indx_DB

    ! open report file
    OPEN (UNIT=REPORT_UNIT, FILE=REPORT_FILE, STATUS='UNKNOWN',FORM='FORMATTED', &
         IOSTAT= ios )
    IF (ios.NE.0) THEN
       WRITE (*,'("Error: opening ",A)') REPORT_FILE
       STOP 1
    END IF
    WRITE (REPORT_UNIT,'("# comparing result files:")' )
    WRITE (REPORT_UNIT,'("# ",A)') filename1
    WRITE (REPORT_UNIT,'("# ",A)') filename2
    WRITE (REPORT_UNIT,'("#")')
    
    ! check some global variables
    CALL check_globals
    
    ! at this point we have scl(), indx_DB() and u() arrays to compare
    
  END SUBROUTINE read_var
  !-----------------------------------------------------------------
  ! compare the grid only
  ! sorted nodes are stored in indx_ixyz(1:nwlt)
  SUBROUTINE com_loc
    USE precision
    CALL com_aux (0,0,0.0_pr)
  END SUBROUTINE com_loc
  !-----------------------------------------------------------------
  ! compare variable # v1 from the first result file
  ! and variable # v2 from the second result file
  SUBROUTINE com_var (v1, v2, L1_eps)
    USE precision
    INTEGER, INTENT(IN) :: v1,v2
    REAL(pr), INTENT(IN) :: L1_eps
    CALL com_aux (v1,v2,L1_eps)
  END SUBROUTINE com_var
  !-----------------------------------------------------------------
  ! clean the memory
  ! finish the report writing
  SUBROUTINE clean_var
    USE field
    INTEGER :: ios
    
    ! print marker of normal finishing
    WRITE (REPORT_UNIT,'("# end")')
    CLOSE (REPORT_UNIT)
    
    IF (ASSOCIATED(scl)) DEALLOCATE (scl)! ios =/ 0 on pgf90, but woks ok like this..Dan G. , STAT=ios);      CALL test_alloc (ios)
    IF (ASSOCIATED(scl1)) DEALLOCATE (scl1, STAT=ios);    CALL test_alloc (ios)
    IF (ASSOCIATED(scl2)) DEALLOCATE (scl2, STAT=ios);    CALL test_alloc (ios)

    IF (ALLOCATED(u1)) DEALLOCATE (u1, STAT=ios);    CALL test_alloc (ios)
    IF (ALLOCATED(u2)) DEALLOCATE (u2, STAT=ios);    CALL test_alloc (ios)
    
  END SUBROUTINE clean_var
  !-----------------------------------------------------------------
  ! PRIVATE
  !-----------------------------------------------------------------
  ! transform 1d into 3d coordinate
  ! similar to weights from wavelet_3d_wrk.f90
  SUBROUTINE ixyz2indx ()
    
!!$    ZERO = 0
!!$    dA = 0.0_pr
!!$    i_k = 0
!!$    lxyz(:) = xyzlimits(2,:)-xyzlimits(1,:)
!!$    i_p(0) = 1
!!$    i_p_cube(0) = 1
!!$    DO i=1,dim
!!$       i_p(i) = i_p(i-1)*(1+nxyz(i))
!!$    END DO
!!$    DO i = 1,dim
!!$       ivec(i) = dim-i+1
!!$    END DO
!!$    
!!$    ii = indx_DB(j_df,wlt_type,face_type,j)%p(k)%ixyz
!!$    ixyz(1:dim) = INT(MOD(ii-1,i_p(1:dim))/i_p(0:dim-1))

  END SUBROUTINE ixyz2indx
  !-----------------------------------------------------------------
  ! print report and compare grids (v=0) or variables (v > 0)
  !  REPORT_VERB=0 prints L1,L2 norms for common, array1, and array2 nodes
  !   and MAX/MIN and the correspondent coordinates for array1 and array2 nodes
  !  REPORT_VERB=1 additionally prints array1 and array2 nodes' 1D coordinates
  !   and the correspondent variables
  SUBROUTINE com_aux (v1, v2, L1_eps)
    USE precision
    REAL(pr), INTENT(IN):: L1_eps     ! warning level for L1 norm
    INTEGER, INTENT(IN) :: v1,v2      ! position of variables to compare, or 0
    LOGICAL :: variables              ! true if comparing variables
    INTEGER :: ic1, ic2, &    ! current position in arrays
         n0, n1, n2, &        ! total number of common nodes and 1 or 2-only
         it                   ! iteration counter
    REAL(pr) :: L1common, L11, L12, &         ! L1 norm for common nodes and 1 or 2-only
         L2common, L21, L22, &                ! L2 norm ...
         Lmin1, Lmin2,  &                     ! MIN values ...
         Lmax1, Lmax2, &                      ! MAX values ...
         diff_max                             ! maximum difference of node value
    INTEGER :: imin1, imin2, imax1, imax2     ! and locations
    REAL(pr) :: val1, val2, val               ! temporal storage for the variables
    LOGICAL :: empty1, empty2                 ! markers of reaching the ends of the arrays
    LOGICAL :: condition_1
    CHARACTER (LEN=5) :: errormark                  ! display if diff_max.GT.errormarklimit
    REAL(pr), PARAMETER :: errormarklimit = 0.01_pr ! ...
    
    ! find what to compare and print header
    variables = .FALSE.
    IF ((v1.GT.0).AND.(v2.GT.0)) variables = .TRUE.
    
    WRITE (REPORT_UNIT,'("#")')
    IF (variables) THEN
       WRITE (REPORT_UNIT,'("# comparing variables ",I5," and ",I5)'),v1,v2
    ELSE
       WRITE (REPORT_UNIT,'("# comparing wavelet nodes locations:")' )
    END IF
    WRITE (REPORT_UNIT,'("#")')
    
    IF (warnings(0).GT.0) THEN
       WRITE (REPORT_UNIT,'("PREVIOUS ERRORS: exiting")')
       RETURN
    END IF
    
    
    IF (variables) THEN
       ! test for reasonable variable numbers
       IF (v1.GT.nvar1) THEN
          WRITE (REPORT_UNIT,'("ERROR: file 1: requested variable ",I5,", from ",I5)') v1,nvar1
          RETURN
       END IF
       IF (v2.GT.nvar2) THEN
          WRITE (REPORT_UNIT,'("ERROR: file 2: requested variable ",I5,", from ",I5)') v2,nvar2
          RETURN
       END IF
       
       ! initialize norms and locations
       L1common = 0.0_pr;  L11 = 0.0_pr;  L12 = 0.0_pr
       L2common = 0.0_pr;  L21 = 0.0_pr;  L22 = 0.0_pr
       val1 = u1 ( 1, v1 )
       val2 = u2 ( 1, v2 )
       Lmin1 = val2;  Lmin2 = val1
       Lmax1 = val2;  Lmax2 = val1
       imin1 = 0; imin2 = 0; imax1 = 0; imax2 = 0
       diff_max = 0.0_pr
    ELSE
       ! compare ordering of wavelets (flags "-v 2 -g")
       IF (REPORT_VERB.EQ.2) THEN
          IF (nwlt1.EQ.nwlt2) THEN
             DO it=1,nwlt1
                IF (indx_i1(it).NE.indx_i(it)) &
                     WRITE (REPORT_UNIT,'("order warning at position ",I8,": ",I8,"<-->",I8)') &
                     it, indx_i1(it), indx_i(it)
             END DO
          ELSE
             WRITE (REPORT_UNIT,'("order warning: number of wavelets is different !")')
          END IF
       END IF
    END IF
    

    DO it=0,MIN(1,REPORT_VERB)
       IF (REPORT_VERB.GT.0) THEN
          IF (it.EQ.0) WRITE (REPORT_UNIT,'("nodes present in first grid only:")')
          IF (it.EQ.1) WRITE (REPORT_UNIT,'("nodes present in second grid only:")')
       END IF
       
       ! initialize markers
       condition_1 = .FALSE.
       empty1 = .FALSE.
       empty2 = .FALSE.
       ic1 = 1;   ic2 = 1
       n0 = 0;   n1 = 0;   n2 = 0

       ! compare
       DO
          IF ( (.NOT.empty1).AND.(.NOT.empty2).AND.(indx_ixyz(ic2).EQ.indx_ixyz1(ic1)) ) THEN
             IF ((it.EQ.0).AND.(variables)) THEN
                val1 = u1 ( indx_i1(ic1), v1 )
                val2 = u2 ( indx_i (ic2), v2 )
                L1common = L1common + ABS(val2-val1)
                L2common = L2common + (val2-val1)*(val2-val1)
                IF (diff_max.LT.ABS(val2-val1)) diff_max = ABS(val2-val1)
             END IF
             n0 = n0 + 1   ! ixyz(ic) present in both arrays
             ic1 = ic1 + 1
             ic2 = ic2 + 1
          ELSE
             IF ( condition_1 ) THEN
                IF ((it.EQ.0).AND.(variables)) THEN
                   val = u1 ( indx_i1(ic1), v1 )
                   L12 = L12 + ABS(val)
                   L22 = L22 + val*val
                   IF (val.GT.Lmax2) THEN
                      Lmax2 = val
                      imax2 = indx_i1(ic1)
                   END IF
                   IF (val.LT.Lmin2) THEN
                      Lmin2 = val
                      imin2 = indx_i1(ic1)
                   END IF
                END IF
                IF ((REPORT_VERB.GT.0).AND.(it.EQ.1)) THEN
                   IF (variables) THEN
                      WRITE (REPORT_UNIT,'(I8," ",E12.5)') indx_ixyz1(ic1), u1 ( indx_i1(ic1), v1 )
                   ELSE
                      WRITE (REPORT_UNIT,'(I8)') indx_ixyz1(ic1)
                   END IF
                END IF
                n1 = n1 + 1    ! ixyz1(ic1) present in the first array only
                ic1 = ic1 + 1
                IF (empty2) ic2 = ic2 + 1
             ELSE
                IF ((it.EQ.0).AND.(variables)) THEN
                   val = u2 ( indx_i (ic2), v2 )
                   L11 = L11 + ABS(val)
                   L21 = L21 + val*val
                   IF (val.GT.Lmax1) THEN
                      Lmax1 = val
                      imax1 = indx_i (ic2)
                   END IF
                   IF (val.LT.Lmin1) THEN
                      Lmin1 = val
                      imin1 = indx_i (ic2)
                   END IF
                END IF
                IF ((REPORT_VERB.GT.0).AND.(it.EQ.0)) THEN
                   IF (variables) THEN
                      WRITE (REPORT_UNIT,'(I8," ",E12.5)') indx_ixyz (ic2), u2 ( indx_i (ic2), v2 )
                   ELSE
                      WRITE (REPORT_UNIT,'(I8)') indx_ixyz (ic2)
                   END IF
                END IF
                n2 = n2 + 1      ! ixyz(ic) present in the second array only
                ic2 = ic2 + 1
                IF (empty1) ic1 = ic1 + 1
             END IF
          END IF
          
          IF ((ic2.GT.nwlt2).AND.(ic1.GT.nwlt1)) EXIT             ! both arrays has been finished
          IF (ic1.GT.nwlt1) THEN                                  ! array 1 has been finished
             ic1 = nwlt1
             empty1 = .TRUE.
          END IF
          IF (ic2.GT.nwlt2) THEN                                  ! array 2 has been finished
             ic2 = nwlt2
             empty2 = .TRUE.
          END IF
          IF ((.NOT.empty1).AND.(.NOT.empty2)) THEN
             condition_1 = (indx_ixyz (ic2).GT.indx_ixyz1(ic1))
          ELSE
             condition_1 = empty2
          END IF

       END DO
    END DO
    
    
    IF ((n2.NE.0).OR.(n1.NE.0)) THEN
       CALL report (1,'nodes present in both grids',n0,n0)
       CALL report (1,'nodes present in the first or secong grids only',n1,n2)
    END IF
    
    errormark = ' '
    IF (diff_max.GT.errormarklimit) errormark = '  <--'

    IF (variables) THEN
       ! print L1,L2, etc norms
       IF (n0.GT.0) THEN
          WRITE (REPORT_UNIT,'("common nodes:", I8)') n0
          WRITE (REPORT_UNIT,'("  L1  = ",E12.5,", L2 = ",E12.5,", max diff=",E12.5,A)') L1common/n0, L2common/n0, diff_max, errormark
          IF ((L1_eps.GT.0.0_pr).AND.(L1common/n0.GT.L1_eps)) &
               WRITE (REPORT_UNIT,'("  warning: L1 is larger than the requested limit of ",E12.5)') L1_eps
       END IF
       IF (n1.GT.0) THEN
          WRITE (REPORT_UNIT,'("nodes in the first grid only:",I8)') n1
          WRITE (REPORT_UNIT,'("  MIN = ",E12.5,"  at ",I8)') Lmin2, imin2
          WRITE (REPORT_UNIT,'("  MAX = ",E12.5,"  at ",I8)') Lmax2, imax2
          WRITE (REPORT_UNIT,'("  L1  = ",E12.5,", L2 = ",E12.5)') L12/n1, L22/n1
          IF ((L1_eps.GT.0.0_pr).AND.(L11/n1.GT.L1_eps)) &
               WRITE (REPORT_UNIT,'("  warning: L1 is larger than the requested limit of ",E12.5)') L1_eps
       END IF
       IF (n2.GT.0) THEN
          WRITE (REPORT_UNIT,'("nodes in the second grid only:",I8)') n2
          WRITE (REPORT_UNIT,'("  MIN = ",E12.5,"  at ",I8)') Lmin1, imin1
          WRITE (REPORT_UNIT,'("  MAX = ",E12.5,"  at ",I8)') Lmax1, imax1
          WRITE (REPORT_UNIT,'("  L1  = ",E12.5,", L2 = ",E12.5)') L11/n2, L21/n2
          IF ((L1_eps.GT.0.0_pr).AND.(L12/n2.GT.L1_eps)) &
               WRITE (REPORT_UNIT,'("  warning: L1 is larger than the requested limit of ",E12.5)') L1_eps
       END IF
    END IF
    
  END SUBROUTINE com_aux
  !-----------------------------------------------------------------
  ! clean what should have been cleaned in read_solution
  SUBROUTINE clean_after_read_solution
    USE pde
    USE wlt_trns_vars             ! indx_DB, xx
    INTEGER :: ios
    
    DEALLOCATE(u_variable_names, STAT=ios);    CALL test_alloc (ios)
    DEALLOCATE(scl_global, STAT=ios);          CALL test_alloc (ios)
    DEALLOCATE(n_var_wlt_fmly, STAT=ios);      CALL test_alloc (ios)
    DEALLOCATE(xx, STAT=ios);                  CALL test_alloc (ios)
    ! header2 - internal to read_solution()
    ! indx_DB (1:j_mx,0:2**dim-1,0:3**dim-1,1:j_mx)
    ! u (1:nwlt,n_var)
    ! scl (1:n_var)
    
  END SUBROUTINE clean_after_read_solution
    
  !-----------------------------------------------------------------
  ! print message into report file
  ! and count warnings
  SUBROUTINE report (wl,s1,i1,i2)
    INTEGER, INTENT(IN) :: wl          ! warning level, 0-fatal, MAXWARNING-OK
    CHARACTER*(*), INTENT(IN) :: s1    ! string for the output report file
    INTEGER, INTENT(IN) :: i1,i2
    
    IF (wl.GE.0) THEN
       warnings(wl) = warnings(wl) + 1
       IF (wl.EQ.0) THEN
          WRITE (REPORT_UNIT,'("ERROR:    ",A," are: ",I8," ",I8)') s1,i1,i2
       ELSE
          WRITE (REPORT_UNIT,'("WARNING:  ",A," are: ",I8," ",I8)') s1,i1,i2
       END IF
    END IF

  END SUBROUTINE report
  !-----------------------------------------------------------------
  ! check internal parameter MAXDIM
  SUBROUTINE check_dimension (dim)
    INTEGER, INTENT(IN) :: dim
    IF (dim.GT.MAXDIM) THEN
       PRINT *, 'Internal error in MODULE COM_SOL_AUX, com_sol_aux.f90:17'
       PRINT *, 'Problem has dimension:', dim
       PRINT *, 'Please increase MAXDIM parameter and recompile'
       STOP 1
    END IF
  END SUBROUTINE check_dimension
  !-----------------------------------------------------------------
  ! insure that the globals are the same in both files
  SUBROUTINE check_globals
    INTEGER :: err,i
    
    IF (dim1.NE.dim2) CALL report ( 0, 'dimensions', dim1, dim2 )
    err = find_different ( nxyz1, nxyz2, dim1 )
    IF (err.NE.0) CALL report ( 1, 'nxyz components', nxyz1(err), nxyz2(err) )
    err = find_different ( mxyz1, mxyz2, dim1 )
    IF (err.NE.0) CALL report ( 1, 'mxyz components', mxyz1(err), mxyz2(err) )
    err = find_different ( prd1, prd2, dim1 )
    IF (err.NE.0) CALL report ( 1, 'prd components', prd1(err), prd2(err) )
    err = find_different ( grid1, grid2, dim1 )
    IF (err.NE.0) CALL report ( 1, 'grid components', grid1(err), grid2(err) )
    
    IF (nvar1.NE.nvar2) CALL report ( 1, 'total number of variables', nvar1, nvar2 )
    IF (nwlt1.NE.nwlt2) CALL report ( 1, 'total number of wavelets', nwlt1, nwlt2 )
    
  END SUBROUTINE check_globals
  !-----------------------------------------------------------------
  ! locate different components
  ! in two vectors of the same length
  FUNCTION find_different (a,b,n)
    INTEGER :: find_different
    INTEGER, INTENT(IN) :: n
    INTEGER, INTENT(IN) :: a(:), b(:)
    INTEGER :: i
    
    find_different = 0
    DO i=1,n
       IF (a(i).NE.b(i)) THEN
          find_different = i
          EXIT
       END IF
    END DO
    
  END FUNCTION find_different
  !-----------------------------------------------------------------
  SUBROUTINE test_alloc(ios)
    INTEGER, INTENT(IN) :: ios
    IF (ios.NE.0) THEN
       WRITE (*,'("Error: allocating")')
       STOP 1
    END IF
  END SUBROUTINE test_alloc
  !-----------------------------------------------------------------
END MODULE com_sol_aux
