#!/bin/sh
# create a list of pairs for a given set

while [ "$1" ]; do
    e1=$1
    shift 1
    for e2 in $*; do
	echo -en "$e1 $e2 "
    done
done

exit 0
