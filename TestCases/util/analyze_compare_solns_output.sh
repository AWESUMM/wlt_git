#!/bin/sh
# Analize the output of result file comparator,
# make a reasonable guess if the comparison was good enough

# check if the comparator finished OK
if [ $1 -ne 0 -o ! -s $2 ]; then exit 1; fi


# look for fatal, or internal comparator errors
if [ "`\grep -i error $2`" ]; then exit 1; fi

# L1 norm is treated as an error
if [ "`\grep \"L1 is larger\" $2`" ]; then exit 1; fi

# warnings (e.g. nxyz or nwlt is different) 
if [ "`\grep -i warning $2`" ]; then exit 2; fi

# otherwise the files are very similar
exit 0