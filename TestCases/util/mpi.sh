#!/bin/sh
# simple timing of multiprocessor execution
#
h0="usage:\n"
h1="1) to run on a given set of numbers of processors, e.g. on 1, 2, 10, and 15\n"
h2="    sh mpi.sh 1 2 10 15\n"
h3="2) to run on N to M processors with optional step S\n"
h4="    sh mpi.sh \`seq N S M\`\n"
h5="3) use external scripting for other options, e.g to run on 1, 2, 4, and 8\n"
h6="    for ((i=1;i<=4;i*=2)); do sh ../util/mpi.sh \$i; done\n"

if [ -z $1 ]; then echo -e " $h0 $h1 $h2 $h3 $h4 $h5 $h6"; fi


# the command to be executed through mpirun
exe="./wlt_3d_db_tree_case_elliptic_poisson.out base.inp"
# the output file written by that script
file="out-time"


while [ "$1" ]; do
    cmd="mpirun -np $1 $exe >out$1 2>&1"
    echo $cmd
    \sh -c "time $cmd"  2>>$file
    \grep -a "CPU TIME (USING CPU_TIME) at #0" out$1 >>$file
    echo -e "$cmd \n" >>$file
    \rm out$1
    shift 1
done
