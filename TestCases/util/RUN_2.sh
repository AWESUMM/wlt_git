#!/bin/sh
# run test cases and compare results
# for different data bases and test cases
# (Cartesian product of list_db and list_ca)
TITLE="The testing suite for the code developers"
VERSION="061109"

function err {
    echo -e "\tRun as \"sh TestCases/util/RUN_2.sh\" from the main directory or"
    echo -e "\tas \"sh util/RUN_2.sh\" from the directory TestCases or"
    echo -e "\tas \"sh RUN_2.sh\" from the directory util"
    echo -e "\tTwo optional parameters: output stream name, error stream name"
    exit 1
}

function print_status() {
    if [ $1 -eq 0 ]; then
	echo -ne "  [OK]    \t"
    else
	echo -ne "  [FAILED]\t"
    fi
    echo -e "$2"
}

# set output streams
o1=/dev/null
o2=/dev/null
if [ "$1" ]; then o1=$1; fi
if [ "$2" ]; then o2=$2; fi

# print invitation message
echo -e "\t$TITLE. Version $VERSION"

# check the current directory
if [ -f RUN.sh ]; then cd ../../
elif [ -f util/RUN.sh ]; then cd ..
elif [ ! -d TestCases ]; then err
fi
cd TestCases

echo "#--------------------------"
echo "#     run test [ TestCases/util/RUN_2.sh ]"
echo "#--------------------------"

# source the lists $list_db and $list_ca
source util/RUN_lists.txt 1>>$o1 2>>$o2

# initialize database pairs
db_pairs=`\sh util/init_pairs.sh $list_db`
np2=`echo $db_pairs|\wc -w`

for ca in $list_ca; do
    echo -e "\n# CASE = $ca"
    cd=`echo $ca | \sed -e "s/\/.*//"`
    cn=`echo $ca | \sed -e "s/.*\///"`
    \mkdir -p "${cd}/results"


if [ -s ${cd}/test_in/local_test_parameters.txt ]; then
    # load local test parameters
    source ${cd}/test_in/local_test_parameters.txt 1>>$o1 2>>$o2
    # issue warning about allow to change accuracy flag
    if [ ${ALLOW_ACCURACY_CHANGE} = "yes" ]; then
	echo -e "\t\t ..............."
	echo -e "\t\t ... Warning ..."
	echo -e "\t\t ... Accuracy is allowed to be degraded for exact solution comparisons"
	echo -e "\t\t ... by the local test flag ALLOW_ACCURACY_CHANGE"
	echo -e "\t\t ... Warning to be issued in case of a successful degraded comparison"
	echo -e "\t\t ..............."
    fi

if [ "$DO_RUN_2" = "yes" ]; then
    DO_RUN_2="no"

    \sh util/input_make.sh ${cd}/test_in/rules.inp 1>>$o1 2>>$o2
    print_status $? "generate input files"
    cd $cd
    \mkdir -p TEST_LOGS

    for da in $list_db; do
	# run, restart, restart as IC
	# (if the correspondent *.RUNTEST.output file is not already present)

	if [ -s results/${cn}_${da}.RUNTEST.output ]; then
	    echo -e "  [SKIPPED]\trun:           ${da}\t > results/...RUNTEST.output"
	else
	    if [ -s ./wlt_3d_${da}_${cn}.out -a -f results/${cn}_${da}.inp ]; then
		time ./wlt_3d_${da}_${cn}.out results/${cn}_${da}.inp > results/${cn}_${da}.RUNTEST.output
		ps=$?
	    else
		ps=1
	    fi
	    print_status $ps "run:           ${da}\t > results/...RUNTEST.output"
	fi
	
	if [ "$DO_RESTART_2" = "yes" ]; then
	if [ -s results/${cn}_${da}_restart.RUNTEST.output ]; then
	    echo -e "  [SKIPPED]\trestart:       ${da}\t > results/...restart.RUNTEST.output"
	else
	    if [ -s ./wlt_3d_${da}_${cn}.out -a -f results/${cn}_${da}_restart.inp ]; then
		./wlt_3d_${da}_${cn}.out results/${cn}_${da}_restart.inp > results/${cn}_${da}_restart.RUNTEST.output
		ps=$?
	    else
		ps=1
	    fi
	    print_status $ps "restart:       ${da}\t > results/...restart.RUNTEST.output"
	fi
	fi
	
	if [ "$DO_RESTARTIC_2" = "yes" ]; then
	if [ -s results/${cn}_${da}_IC_as_restart.RUNTEST.output ]; then
	    echo -e "  [SKIPPED]\trestart as IC: ${da}\t > results/...IC_as_restart.RUNTEST.output"
	else
	    if [ -s ./wlt_3d_${da}_${cn}.out -a -f results/${cn}_${da}_IC_as_restart.inp ]; then
		./wlt_3d_${da}_${cn}.out results/${cn}_${da}_IC_as_restart.inp > results/${cn}_${da}_IC_as_restart.RUNTEST.output
		ps=$?
	    else
		ps=1
	    fi
	    print_status $ps "restart as IC: ${da}\t > results/...IC_as_restart.RUNTEST.output"
	fi
	fi
	
	# comparing exact solutions and *.res files
	# for saved good solution DB_GOOD and RES_GOOD (e.g. "db_wrk" and ".0006.res")
	\sh ../util/compare_results.sh  $cn $da "$DB_GOOD" "$RES_GOOD" "$L1_CT" $COMPARE_RESULTS_2 $ALLOW_ACCURACY_CHANGE
	if [ $? -ne 0 ]; then
	    echo -e "\nInternal error in compare_results.sh called from RUN_2.sh"
	    echo -e "Please fix the testing scripts before testing !"
	    exit 1
	fi
    done

    # comparing between all possible databases
    for (( i=1; i<=`expr $np2 / 2`; i++ )); do
	i2=`expr $i \* 2`
	i1=`expr $i2 - 1`
	d1=`echo $db_pairs|cut -d " " -f $i1`
	d2=`echo $db_pairs|cut -d " " -f $i2`
	\sh ../util/compare_dbs.sh $cn $d1 $d2 "$RES_GOOD" "$L1_CT" $COMPARE_DBS_2 $ALLOW_ACCURACY_CHANGE
	if [ $? -ne 0 ]; then
	    echo -e "\nInternal error in compare_dbs.sh called from RUN_2.sh"
	    echo -e "Please fix the testing scripts before testing !"
	    exit 1
	fi
    done

    cd ..
else           # of DO_RUN_2
    echo "# NOT REQUIRED, by local parameter DO_RUN_2 in ${cd}/test_in/local_test_parameters.txt"
fi             # of DO_RUN_2
else           # of local test parameters loading
    echo "# NOT REQUIRED, file not present: ${cd}/test_in/local_test_parameters.txt"
fi             # of local test parameters loading
done           # of list_ca cycle
exit 0
