#!/bin/sh
# Compare test run results to some saved good solutions.
#  Arguments:
#   $1 base_name                - current test case, e.g. "case_elliptic_poisson"
#   $2 db                       - current database name, e.g "db_lines"
#   $3 db_good                  - name of the database which files were saved in results_good/
#   $4 ending_res               - station number of files saved in results_good/
#   $5 L1_comparator_threshold  - L1 norm result comparator threshold
#   $6-10                       - 0/1 action markers:
#    6               (exact solution) compare saved good solution to full run
#    7               (exact solution) compare saved good solution to restart
#    8               (exact solution) compare saved good solution to restart from IC file in restart format
#    9               (exact solution) compare full run to restart
#    10              (exact solution) compare full run to restart from IC file in restart format
#    11              (*.res file) compare saved good to full run, diff/comparator
#    12              (*.res file) compare saved good to restart, diff/comparator
#    13              (*.res file) compare saved good to restart from IC file in restart format, diff/comparator
#    14              (*.res file) compare full run to restart, diff/comparator
#    15              (*.res file) compare full run to restart from IC file in restart format, diff/comparator
#    16              allow to change accuracy for db_tree flag (yes/no)
base_name=$1
db_name=$2
if [ $# -ne 16 ]; then exit 1; fi

# Set file name ending for different runs to compare
file_ending_run=".err.exact.soln"
file_ending_restart="_restart.err.exact.soln"
file_ending_restart_from_IC="_IC_as_restart.err.exact.soln"
#saved exact solutions
db_good=$3
file_ending_run_good_solution=${db_good}${file_ending_run}
file_ending_restart_good_soln=${db_good}${file_ending_restart}
file_restart_IC_good_soln=${db_good}${file_ending_restart_from_IC}
#saved result file
ending_res=$4
L1_comparator_threshold=$5

# set local debug variable (0/1)
LDBG_CMPRES=1

# Output the status of an executed operation
function print_status() {
    if [ $1 -eq 0 ]; then
	echo -ne "  [OK]     \t"
    elif [ $1 -eq 2 ]; then
	echo -ne "  [WARNING]\t"
    elif [ $1 -eq 3 ]; then
	echo -ne "  [SKIPPED]\t"
    else
	echo -ne "  [FAILED] \t"
    fi
    echo -e "$2"
}


#  Put header for this comparison
echo -e "\n******************************************************" >> TEST.REPORT
echo "Test case  $base_name $db_name" >> TEST.REPORT


if [ $6 -eq 1 ]; then
    if [ -s results_good/${base_name}_${file_ending_run_good_solution} ]; then
	if [ $LDBG_CMPRES -eq 1 ]; then
	    echo -e "\t\t\tresults/${base_name}_${db_name}${file_ending_run}\n\t\t\tresults_good/${base_name}_${file_ending_run_good_solution}"
	fi
	\sh ../util/compare_exact_solns.sh \
	results/${base_name}_${db_name}${file_ending_run} \
	results_good/${base_name}_${file_ending_run_good_solution} \
	"(exact solution) compare saved good solution to full run" 0 ${16} >> TEST.REPORT
	ps=$?
    else
	ps=3
    fi
    print_status $ps "(exact solution) compare saved good to full run"
fi


if [ $7 -eq 1 ]; then
    if [ -s results_good/${base_name}_${file_ending_restart_good_soln} ]; then
	if [ $LDBG_CMPRES -eq 1 ]; then
	    echo -e "\t\t\tresults/${base_name}_${db_name}${file_ending_restart}\n\t\t\tresults_good/${base_name}_${file_ending_restart_good_soln}"
	fi
	\sh ../util/compare_exact_solns.sh \
	results/${base_name}_${db_name}${file_ending_restart} \
	results_good/${base_name}_${file_ending_restart_good_soln} \
	"(exact solution) compare saved good solution to restart" 0  ${16} >> TEST.REPORT
	ps=$?
    else
	ps=3
    fi
    print_status $ps "(exact solution) compare saved good to restart"
fi


if [ $8 -eq 1 ]; then
    if [ -s results_good/${base_name}_${file_restart_IC_good_soln} ]; then
	if [ $LDBG_CMPRES -eq 1 ]; then
	    echo -e "\t\t\tresults/${base_name}_${db_name}${file_ending_restart_from_IC}\n\t\t\tresults_good/${base_name}_${file_restart_IC_good_soln}"
	fi
	\sh ../util/compare_exact_solns.sh \
	results/${base_name}_${db_name}${file_ending_restart_from_IC} \
	results_good/${base_name}_${file_restart_IC_good_soln} \
	"(exact solution) compare saved good solution to restart from IC file in restart format" 0  ${16} >> TEST.REPORT
	ps=$?
    else
	ps=3
    fi
    print_status $ps "(exact solution) compare saved good to restart from IC"
fi


if [ $9 -eq 1 ]; then
    if [ $LDBG_CMPRES -eq 1 ]; then
	echo -e "\t\t\tresults/${base_name}_${db_name}${file_ending_run}\n\t\t\tresults/${base_name}_${db_name}${file_ending_restart}"
    fi
    \sh ../util/compare_exact_solns.sh \
    results/${base_name}_${db_name}${file_ending_run} \
    results/${base_name}_${db_name}${file_ending_restart} \
    "(exact solution) compare full run to restart"  4  ${16} >> TEST.REPORT
    print_status $? "(exact solution) compare full run to restart ${base_name}_${db_name}${file_ending_restart}"
fi


if [ ${10} -eq 1 ]; then
    if [ $LDBG_CMPRES -eq 1 ]; then
	echo -e "\t\t\tresults/${base_name}_${db_name}${file_ending_run}\n\t\t\tresults/${base_name}_${db_name}${file_ending_restart_from_IC}"
    fi
    \sh ../util/compare_exact_solns.sh \
    results/${base_name}_${db_name}${file_ending_run} \
    results/${base_name}_${db_name}${file_ending_restart_from_IC} \
    "(exact solution) compare full run to restart from IC file in restart format"  4  ${16} >> TEST.REPORT
    print_status $? "(exact solution) compare full run to restart from IC ${base_name}_${db_name}${file_ending_restart_from_IC}"
fi


if [ ${11} -eq 1 ]; then
    if [ -s results_good/c${base_name}_${db_good}${ending_res} ]; then
	if [ $LDBG_CMPRES -eq 1 ]; then
	    echo -e "\t\t\tresults_good/c${base_name}_${db_good}${ending_res}\n\t\t\tresults/c${base_name}_${db_name}${ending_res}"
	fi
	# compare *.res files to the saved good *.res (full run)
	\sh ../util/compare_exact_solns.sh \
	results_good/c${base_name}_${db_good}${ending_res} \
	results/c${base_name}_${db_name}${ending_res} \
	"(*.res files) results_good/c${base_name}_${db_good}${ending_res}, results/c${base_name}_${db_name}${ending_res}" \
	0  ${16} >> TEST.REPORT
	ps=$?
	if [ $ps -ne 0 ]; then
	    # start the comparator (compare grid, verbouse level 0, threshold, all variables)
	    echo -e "# result file comparator started\n# please examine the log files TEST_LOGS/compare_solns.*.log" >> TEST.REPORT
	    ../util/compare_solns \
	    results_good/c${base_name}_${db_good}${ending_res} \
	    results/c${base_name}_${db_name}${ending_res} \
	    TEST_LOGS/tmp_compare_solns_out -g -t $L1_comparator_threshold \
	    >> TEST_LOGS/compare_solns.all.log 2>>TEST_LOGS/compare_solns.all.log
	    \sh ../util/analyze_compare_solns_output.sh $? TEST_LOGS/tmp_compare_solns_out
	    ps=$?
	    \cat TEST_LOGS/tmp_compare_solns_out >> TEST_LOGS/compare_solns.out.log 2>>TEST_LOGS/compare_solns.all.log
	    \rm -f TEST_LOGS/tmp_compare_solns_out
	    print_status $ps "(*.res file) compare saved good to full run, result comparator with L1 threshold $L1_comparator_threshold"
	else
	    # files are identical
	    print_status $ps "(*.res file) compare saved good to full run, diff"
	fi
    else
	ps=3
	print_status $ps "(*.res file) compare saved good to full run"
    fi
fi


if [ ${12} -eq 1 ]; then
    if [ -s results_good/c${base_name}_${db_good}_restart${ending_res} ]; then
	if [ $LDBG_CMPRES -eq 1 ]; then
	    echo -e "\t\t\tresults_good/c${base_name}_${db_good}_restart${ending_res}\n\t\t\tresults/c${base_name}_${db_name}_restart${ending_res}"
	fi
	# compare *.res files to the saved good *.res (restart)
	\sh ../util/compare_exact_solns.sh \
	results_good/c${base_name}_${db_good}_restart${ending_res} \
	results/c${base_name}_${db_name}_restart${ending_res} \
	"(*.res files) results_good/c${base_name}_${db_good}_restart${ending_res}, results/c${base_name}_${db_name}_restart${ending_res}" \
	0  ${16} >> TEST.REPORT
	ps=$?
	if [ $ps -ne 0 ]; then
	    # start the comparator (compare grid, verbouse level 0, threshold, all variables)
	    echo -e "# result file comparator started\n# please examine the log files TEST_LOGS/compare_solns.*.log" >> TEST.REPORT
	    ../util/compare_solns \
	    results_good/c${base_name}_${db_good}_restart${ending_res} \
	    results/c${base_name}_${db_name}_restart${ending_res} \
	    TEST_LOGS/tmp_compare_solns_out -g -t $L1_comparator_threshold \
	    >> TEST_LOGS/compare_solns.all.log 2>>TEST_LOGS/compare_solns.all.log
	    \sh ../util/analyze_compare_solns_output.sh $? TEST_LOGS/tmp_compare_solns_out
	    ps=$?
	    \cat TEST_LOGS/tmp_compare_solns_out >> TEST_LOGS/compare_solns.out.log 2>>TEST_LOGS/compare_solns.all.log
	    \rm -f TEST_LOGS/tmp_compare_solns_out
	    print_status $ps "(*.res file) compare saved good to restart, result comparator with L1 threshold $L1_comparator_threshold"
	else
	    # files are identical
	    print_status $ps "(*.res file) compare saved good to restart, diff"
	fi
    else
	ps=3
	print_status $ps "(*.res file) compare saved good to restart"
    fi
fi


if [ ${13} -eq 1 ]; then
    if [ -s results_good/c${base_name}_${db_good}_IC_as_restart${ending_res} ]; then
	if [ $LDBG_CMPRES -eq 1 ]; then
	    echo -e "\t\t\tresults_good/c${base_name}_${db_good}_IC_as_restart${ending_res}\n\t\t\tresults/c${base_name}_${db_name}_IC_as_restart${ending_res}"
	fi
	# compare *.res files to the saved good *.res (restart from IC file)
	\sh ../util/compare_exact_solns.sh \
	results_good/c${base_name}_${db_good}_IC_as_restart${ending_res} \
	results/c${base_name}_${db_name}_IC_as_restart${ending_res} \
	"(*.res files) results_good/c${base_name}_${db_good}_IC_as_restart${ending_res}, results/c${base_name}_${db_name}_IC_as_restart${ending_res}" \
	0  ${16} >> TEST.REPORT
	ps=$?
	if [ $ps -ne 0 ]; then
	    # start the comparator (compare grid, verbouse level 0, threshold, all variables)
	    echo -e "# result file comparator started\n# please examine the log files TEST_LOGS/compare_solns.*.log" >> TEST.REPORT
	    ../util/compare_solns \
	    results_good/c${base_name}_${db_good}_IC_as_restart${ending_res} \
	    results/c${base_name}_${db_name}_IC_as_restart${ending_res} \
	    TEST_LOGS/tmp_compare_solns_out -g -t $L1_comparator_threshold \
	    >> TEST_LOGS/compare_solns.all.log 2>>TEST_LOGS/compare_solns.all.log
	    \sh ../util/analyze_compare_solns_output.sh $? TEST_LOGS/tmp_compare_solns_out
	    ps=$?
	    \cat TEST_LOGS/tmp_compare_solns_out >> TEST_LOGS/compare_solns.out.log 2>>TEST_LOGS/compare_solns.all.log
	    \rm -f TEST_LOGS/tmp_compare_solns_out
	    print_status $ps "(*.res file) compare saved good to restart from IC, result comparator with L1 threshold $L1_comparator_threshold"
	else
	    # files are identical
	    print_status $ps "(*.res file) compare saved good to restart from IC, diff"
	fi
    else
	ps=3
	print_status $ps "(*.res file) compare saved good to restart from IC"
    fi
fi


if [ ${14} -eq 1 ]; then
    if [ $LDBG_CMPRES -eq 1 ]; then
	echo -e "\t\t\tresults/c${base_name}_${db_name}_restart${ending_res}\n\t\t\tresults/c${base_name}_${db_name}${ending_res}"
    fi
    # compare *.res files after restart and a full run
    \sh ../util/compare_exact_solns.sh \
    results/c${base_name}_${db_name}_restart${ending_res} \
    results/c${base_name}_${db_name}${ending_res} \
    "(*.res files) results/c${base_name}_${db_name}_restart${ending_res}, results/c${base_name}_${db_name}${ending_res}" \
    0  ${16} >> TEST.REPORT
    ps=$?
    
    if [ $ps -ne 0 ]; then
	# start the comparator (compare grid, verbouse level 0, threshold, all variables)
	echo -e "# result file comparator started\n# please examine the log files TEST_LOGS/compare_solns.*.log" >> TEST.REPORT
	../util/compare_solns \
	results/c${base_name}_${db_name}_restart${ending_res} \
	results/c${base_name}_${db_name}${ending_res} \
	TEST_LOGS/tmp_compare_solns_out -g -t $L1_comparator_threshold \
	>> TEST_LOGS/compare_solns.all.log 2>>TEST_LOGS/compare_solns.all.log
	\sh ../util/analyze_compare_solns_output.sh $? TEST_LOGS/tmp_compare_solns_out
	ps=$?
	\cat TEST_LOGS/tmp_compare_solns_out >> TEST_LOGS/compare_solns.out.log 2>>TEST_LOGS/compare_solns.all.log
	\rm -f TEST_LOGS/tmp_compare_solns_out
	print_status $ps "(*.res file) compare full run to restart, result comparator with L1 threshold $L1_comparator_threshold"
    else
	# files are identical
	print_status $ps "(*.res file) compare full run to restart, diff"
    fi
fi



if [ ${15} -eq 1 ]; then
    if [ $LDBG_CMPRES -eq 1 ]; then
	echo -e "\t\t\tresults/c${base_name}_${db_name}_IC_as_restart${ending_res}\n\t\t\tresults/c${base_name}_${db_name}${ending_res}"
    fi
    # compare *.res files after restart from IC file in restart format and a full run
    #echo results/c${base_name}_${db_name}_IC_as_restart${ending_res} results/c${base_name}_${db_name}${ending_res}
    \sh ../util/compare_exact_solns.sh \
    results/c${base_name}_${db_name}_IC_as_restart${ending_res} \
    results/c${base_name}_${db_name}${ending_res} \
    "(*.res files) results/c${base_name}_${db_name}_IC_as_restart${ending_res}, results/c${base_name}_${db_name}${ending_res}" \
    0  ${16} >> TEST.REPORT
    ps=$?
    if [ $ps -ne 0 ]; then
	# start the comparator (compare grid, verbouse level 0, threshold, all variables)
	echo -e "# result file comparator started\n# please examine the log files TEST_LOGS/compare_solns.*.log" >> TEST.REPORT
	../util/compare_solns \
	results/c${base_name}_${db_name}_IC_as_restart${ending_res} \
	results/c${base_name}_${db_name}${ending_res} \
	TEST_LOGS/tmp_compare_solns_out -g -t $L1_comparator_threshold \
	>> TEST_LOGS/compare_solns.all.log 2>>TEST_LOGS/compare_solns.all.log
	\sh ../util/analyze_compare_solns_output.sh $? TEST_LOGS/tmp_compare_solns_out
	ps=$?
	\cat TEST_LOGS/tmp_compare_solns_out >> TEST_LOGS/compare_solns.out.log 2>>TEST_LOGS/compare_solns.all.log
	\rm -f TEST_LOGS/tmp_compare_solns_out
	print_status $ps "(*.res file) compare full run to restart from IC file, result comparator with L1 threshold $L1_comparator_threshold"
    else
	# files are identical
	print_status $ps "(*.res file) compare full run to restart from IC file, diff"
    fi
fi


# reset local debug variable (0/1)
LDBG_CMPRES=0

exit 0
