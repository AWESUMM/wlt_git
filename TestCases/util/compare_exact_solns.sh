#!/bin/sh
# Exact comparison of two solutions files
# Arguments:
#  1 - first file to compare
#  2 - second file to compare
#  3 - comment string
#  4 - number of lines to compare (or 0 for all)
#  5 - marker to change accuracy (yes/no) for db_tree comparisons

echo -e "\t________________\n\t$3"
echo -e "\tFILES:  $1\n\t        $2"

# form portions of the files to compare
if [ $4 -eq 0 ]; then
  \cat $1 > tmp_1  2>tmp_1
  \cat $2 > tmp_2  2>tmp_2
else
  \tail -n $4 $1 > tmp_1  2>tmp_1
  \tail -n $4 $2 > tmp_2  2>tmp_2
fi

# compare the portions
\diff tmp_1 tmp_2 > tmp_diff  2>tmp_diff

if [ -s tmp_diff ]; then
    # exact comparison fails
    echo -e "\tERROR, exact solution files do not match"
    echo -e "\t diff between files follows:"
    \cat tmp_diff
    
    if [ ${5} = "yes" ]; then
	# change accuracy (for db_tree comparison)
	\cat tmp_1 | \sed -s 's/....E/E/g' > tmp_diff && \cat tmp_diff > tmp_1
	\cat tmp_2 | \sed -s 's/....E/E/g' > tmp_diff && \cat tmp_diff > tmp_2
	\rm -rf tmp_diff

	# compare the portions
	\diff tmp_1 tmp_2 > tmp_diff  2>tmp_diff

	if [ -s tmp_diff ]; then
	    # degraded accuracy comparison fails
	    \rm -rf tmp_1 tmp_2 tmp_diff
	    exit 1
	else
	    # degraded accuracy comparison is OK
	    \rm -rf tmp_1 tmp_2 tmp_diff
	    exit 2
	fi
    else
        \rm -rf tmp_1 tmp_2 tmp_diff
	exit 1
    fi
else
    # exact comparison is OK
    echo -e "\tTEST PASSED, exact solution files match"
    \rm -rf tmp_1 tmp_2 tmp_diff
    exit 0
fi

