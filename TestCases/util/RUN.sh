#!/bin/sh
# compilation of various targets
# for different data bases and test cases
# (Cartesian product of list_db, list_ca, and list_ta)
TITLE="The testing suite for the code developers"
VERSION="061030"
    
# set output streams
o1=/dev/null
o2=/dev/null
if [ "$1" ]; then o1=TestCases/$1; fi
if [ "$2" ]; then o2=TestCases/$2; fi

function err {
    echo -e "\tRun as \"sh TestCases/util/RUN.sh\" from the main directory or"
    echo -e "\tas \"sh util/RUN.sh\" from the directory TestCases or"
    echo -e "\tas \"sh RUN.sh\" from the directory util"
    echo -e "\tTwo optional parameters: output stream name, error stream name"
    exit 1
}

function cmd_run() {
    echo "[-----" >> $o1
    echo "[     $*" >> $o1
    echo "[-----" >> $o1
    echo "[-----" >> $o2
    echo "[     $*" >> $o2
    echo "[-----" >> $o2
    $* 1>>$o1 2>>$o2
    return $?
}

# print invitation message
echo -e "\t$TITLE. Version $VERSION"

# check the current directory
if [ -f RUN.sh ]; then cd ../../
elif [ -f util/RUN.sh ]; then cd ..
elif [ ! -d TestCases ]; then err
fi

if [ ! $WLT_COMPILE_HOSTNAME ]; then
    echo "Please define WLT_COMPILE_HOSTNAME"
    exit 1
fi

echo "#--------------------------"
echo "#     compilation test [ TestCases/util/RUN.sh ]"
echo "#--------------------------"

# source the lists
source TestCases/util/RUN_lists.txt 1>>$o1 2>>$o2

# compile all the targets
for da in $list_db; do   # list of data bases
for ca in $list_ca; do   # list of test cases
cmd_run gmake clean
for ta in $list_ta; do   # list of compilation targets
    
    cd=`echo $ca | \sed -e "s/\/.*//"`
    \mkdir -p "TestCases/${cd}/results"

    #cmd_run gmake WLT_COMPILE_HOSTNAME=$HOSTNAME DB=$da CASE=TestCases/$ca $ta
    cmd_run gmake WLT_COMPILE_HOSTNAME=$WLT_COMPILE_HOSTNAME DB=$da CASE=TestCases/$ca $ta
    cr=$?
    if [ $cr -eq 0 ]; then
	echo -ne "  [OK]    \t"
    else
	echo -ne "  [FAILED]\t"
    fi
    echo -e "$da\t$ca\t$ta"
done
done
done

# compile result comparator and clean the tree
cd TestCases/util && gmake 1>/dev/null 2>/dev/null
if [ $? -eq 0 ]; then
    echo -ne "  [OK]    \t"
else
    echo -ne "  [FAILED]\t"
fi
echo -e "result file comparator"
cd ../.. && gmake clean 1>/dev/null 2>/dev/null

exit 0
