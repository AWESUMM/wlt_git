function Count___nSig_nGho___Internal_Boundary
%% Count__nSig_nGho___Internal_Boundary.m
%
%% Counting   nSig(Internal & Boundary)   &   nGho(Internal & Boundary)   
%                                     on   requin.sharcnet.ca   
%                                  using   Different Number of Processors   
% 
%%  

clear all;
%close all;

LineColorMarker  =  {'sk--'; '^r--'; 'ob-.';  '>g- '; '<m: '; '^c--'; 'or-.'; 'sg-.'; 'or'; 'xk'; 'c'; 'm'; 'k'; 'r'; 'xg'; 'xm'; 'ob'; '^c'; '<m'; '>y'; 'g'; 'b-.'  };
LineWidth        =  {0.5;    0.5;    0.5;     0.5;    0.5;    0.5;    0.5;    0.5;    0.5;  0.5; 3.0; 3.0; 3.0; 3.0; 0.5;  0.5;  0.5;  0.5;  0.5;  0.5;  2.0; 0.5    };
LineWidth_avg    =  {2.5;    2.5;    2.5;     2.5;    2.5;    2.5;    2.5;    2.5;    2.5;  2.5; 3.0; 3.0; 3.0; 3.0; 0.5;  0.5;  0.5;  0.5;  0.5;  0.5;  2.0; 0.5    };
%                    1        2       3        4       5       6       7       8       9     10   11   12   13   14   15    16    17    18   19     20   21   22  

%LineColorMarker_avg  =  { 'sk-';  '^r-';  'ob-';   '>g-';  '<m-';  '^c-';  'or-';  'sg-'; 'or-'; 'k'; 'c'; 'm'; 'k'; 'r'; 'xg'; 'xm'; 'ob'; '^c'; '<m'; '>y'; 'g'; 'b-.'  };
LineColorMarker_avg  =  {  'k-';   'r-';   'b-';    'g-';   'm-';   'c-';   'r-';   'g-';   'r'; 'k'; 'c'; 'm'; 'k'; 'r'; 'xg'; 'xm'; 'ob'; '^c'; '<m'; '>y'; 'g'; 'b-.'  };
%LineColorMarker_min  =  { 'k: ';  'r: ';  'b: ';   'g: ';  'm: ';  'c: ';  'r: ';  'g: ';   'r'; 'k'; 'c'; 'm'; 'k'; 'r'; 'xg'; 'xm'; 'ob'; '^c'; '<m'; '>y'; 'g'; 'b-.'  };
LineColorMarker_min  =  {  'k-';   'r-';   'b-';    'g-';   'm-';   'c-';   'r-';   'g-';   'r'; 'k'; 'c'; 'm'; 'k'; 'r'; 'xg'; 'xm'; 'ob'; '^c'; '<m'; '>y'; 'g'; 'b-.'  };
LineColorMarker_max  =  {  'k-';   'r-';   'b-';    'g-';   'm-';   'c-';   'r-';   'g-';   'r'; 'k'; 'c'; 'm'; 'k'; 'r'; 'xg'; 'xm'; 'ob'; '^c'; '<m'; '>y'; 'g'; 'b-.'  };
LineColorMarker_std  =  { 'k-.';  'r-.';  'b-.';   'g-.';  'm-.';  'c-.';  'r-.';  'g-.';   'r'; 'k'; 'c'; 'm'; 'k'; 'r'; 'xg'; 'xm'; 'ob'; '^c'; '<m'; '>y'; 'g'; 'b-.'  };
%LineColorMarker_std  =  {  'sk';   '^r';   'ob';    '>g';   '<m';   '^c';   'or';   'sg';  'or'; 'k'; 'c'; 'm'; 'k'; 'r'; 'xg'; 'xm'; 'ob'; '^c'; '<m'; '>y'; 'g'; 'b-.'  };


LineColorMarker_avg_TimeAverage  =  {  'sk-';   '^r-';   'ob-';    '>g-';   '<m-';   'dc-';   'r-';   'g-';   'r'; 'k'; 'c'; 'm'; 'k'; 'r'; 'xg'; 'xm'; 'ob'; '^c'; '<m'; '>y'; 'g'; 'b-.'  };
LineColorMarker_min_TimeAverage  =  {  'sk-';   '^r-';   'ob-';    '>g-';   '<m-';   'dc-';   'r-';   'g-';   'r'; 'k'; 'c'; 'm'; 'k'; 'r'; 'xg'; 'xm'; 'ob'; '^c'; '<m'; '>y'; 'g'; 'b-.'  };
LineColorMarker_max_TimeAverage  =  {  'sk-';   '^r-';   'ob-';    '>g-';   '<m-';   'dc-';   'r-';   'g-';   'r'; 'k'; 'c'; 'm'; 'k'; 'r'; 'xg'; 'xm'; 'ob'; '^c'; '<m'; '>y'; 'g'; 'b-.'  };

case_Dim = 3;
Font_Size = 16;
Font_Size_Label = Font_Size-2;

File_Type_Number = 1;                                      %  1:: '.p0_log'         2 ::  '.p0_user_stats';

Log_Info_Variable_1 = 4; 
%Log_Info_Variable  = 18; 
Timers_No           = 1;                                   %  3- adapt_grid   4- time advancement   5 - parallel_migrate   6 - parallel communicators
Number_of_Timers    = 8;
%Log_Info_Variable = Log_Info_Variable + Timers_No;


LastTime = 1.6866E-02; %1.4432E-02; %1.2794E-02; %9.4285E-03; %8.6398E-03; %8.0457E-03;
%LastTime = 1.70E-02;
%LastLine = 60;


% NWLT_Total,  [pr=0]  NWLT,  iSig,  iSig_I,  iSig_B, iGho, iGho_I, iGho_B, iGhoOnly, iGhoOnly_I, iGhoOnly_B,  [pr=1]  NWLT,  iSig, ... 










Plot_Nwlt                    = false;
Plot_MissBalance             = false;
Plot_MissBalance_NGho        = true;
Plot_MissBalance_NSig        = false;
Plot_MissBalance_Performance = false;
Plot_Ratio_Means             = false;
Plot_Ncpu                    = false;





%% requin.sharcnet.ca   HP
Test_Number =  2048.121;     % 1   2   1024   2048
                          % 1024.31 2048.31  :: Scalability New: November 2011
                          % 1024.21 2048.21  :: Scalability New: December 2011   N_Diff = N_Predict = 2 
                          % 1024.11 2048.11  :: Scalability New: December 2011   N_Diff = N_Predict = 1 
                          % 2048.121         :: RKL1 One2One
                          % 2048.31121       :: Scalability New: November 2011 (All2All)  +  RKL1 One2One
i=1;

switch Test_Number
    case {1}

x_min = 0.0;
x_max = 0.2; 

eps = 1.0000000e-3;
nu  = 0.1;
C_f = 0.0;

Title_Part_1 = 'Convection Diffusion of Rotating Ellipsoids'; 
J_max_Title  = 10;
Resolution_Title = 4096;
Reynolds = 00;
        
DIR      = 'C:\Documents and Settings\Alireza\My Documents\T e r a__G r i d\ConvDif\';

CaseDIR  = 'Janus\countDB__J10d3__36cpu\';
CaseName = 'count_DB_InternalBoundary.log';
Case_Path_Name{i} = [DIR CaseDIR CaseName ];   %.p0_user_stats
t_begin(i) = 0;
CaseLegend{i}      = ['d3  36cpu  Jmax=10  Janus'];
CaseLegend{i}      = ['36cpu'];
Number_Processors{i} = 36;
J_max(i) = 10;
i=i+1;

CaseDIR  = 'Janus\countDB__J10d3__24cpu\';
CaseName = 'count_DB_InternalBoundary.log';
Case_Path_Name{i} = [DIR CaseDIR CaseName ];   %.p0_user_stats
t_begin(i) = 0;
CaseLegend{i}      = ['d3  24cpu  Jmax=10  Janus'];
CaseLegend{i}      = ['24cpu'];
Number_Processors{i} = 24;
J_max(i) = 10;
i=i+1;





    case {2}

x_min = 0.0;
x_max = 0.2; 

eps = 1.0000000e-3;
nu  = 0.1;
C_f = 0.0;

Title_Part_1 = 'Convection Diffusion of Rotating Ellipsoids'; 
J_max_Title  = 6;
Resolution_Title = 256;
Reynolds = 00;
        
DIR      = 'C:\Documents and Settings\Alireza\My Documents\T e r a__G r i d\ConvDif\';

CaseDIR  = 'Janus\countDB__J6d3__4cpu\';
CaseName = 'count_DB_InternalBoundary.log';
Case_Path_Name{i} = [DIR CaseDIR CaseName ];   %.p0_user_stats
t_begin(i) = 0;
CaseLegend{i}      = ['d3  4cpu  Jmax=6  Janus'];
CaseLegend{i}      = ['d3  4cpu'];
Number_Processors{i} = 4;
J_max(i) = 6;
i=i+1;

CaseDIR  = 'Janus\countDB__J6d3__serial_dbtree\';
CaseName = 'count_DB_InternalBoundary.log';
Case_Path_Name{i} = [DIR CaseDIR CaseName ];   %.p0_user_stats
t_begin(i) = 0;
CaseLegend{i}      = ['     serial  Jmax=6  Janus'];
CaseLegend{i}      = ['serial'];
Number_Processors{i} = 1;
J_max(i) = 6;
i=i+1;






    case {3}

x_max = 0.02;

eps = 0.2;
nu  = 0.09; %0.015;
C_f = 6.66667;

Title_Part_1 = 'CVS'; %'CVS  (( Initialized by CVS cinit-00375-eps005.0000.res (serial) ))'
J_max_Title  = 6;
Resolution_Title = 256;


DIR      = 'C:\Documents and Settings\Alireza\Desktop\figures\count_DB_InternalBoundary\';

CaseDIR  = 'EdgeWeight0.1\';
CaseName = 'count_DB_InternalBoundary.d3p128_EdgeWeight0.1';
Case_Path_Name{i} = [DIR CaseDIR CaseName ];   %.p0_user_stats
CaseLegend{i}      = ['d3  128cpu  Re=70   EdgeWeight=0.1'];
Number_Processors{i} = 128;
J_max(i) = 6;
i=i+1;

CaseDIR  = 'EdgeWeight1\';
CaseName = 'count_DB_InternalBoundary.d3p128_EdgeWeight1';
Case_Path_Name{i} = [DIR CaseDIR CaseName ];   %.p0_user_stats
CaseLegend{i}      = ['d3  128cpu  Re=70   EdgeWeight=1'];
Number_Processors{i} = 128;
J_max(i) = 6;
i=i+1;


CaseDIR  = 'NewCode__EdgeWeight0.1\';
CaseName = 'count_DB_InternalBoundary.d3p128_EdgeWeight0.1';
Case_Path_Name{i} = [DIR CaseDIR CaseName ];   %.p0_user_stats
CaseLegend{i}      = ['d3  128cpu  Re=70   EdgeWeight=0.1  New Code'];
Number_Processors{i} = 128;
J_max(i) = 6;
i=i+1;
       






    case {1024}

x_min = 0.0;
x_max = 0.2; 

eps = 0.2;
nu  = 0.015;
C_f = 6.66667;

Title_Part_1 = 'CVS'; %'CVS  (( Initialized by CVS cinit-00375-eps005.0000.res (serial) ))'
J_max_Title  = 8;
Resolution_Title = 1024;
Reynolds = 190;
        
DIR      = 'C:\Documents and Settings\Alireza\My Documents\T e r a__G r i d\CVS_Eps2_Cf666\';

CaseDIR  = 'Janus\Re190_Nu015\countDB__d3_768cpu_dt2e4_smallTol123__Intel11_Zoltan11\';
CaseName = 'count_DB_InternalBoundary.log';
Case_Path_Name{i} = [DIR CaseDIR CaseName ];   %.p0_user_stats
t_begin(i) = 0;
CaseLegend{i}      = ['d3  768cpu  Re=190 Janus'];
CaseLegend{i}      = ['768cpu'];
Number_Processors{i} = 768;
J_max(i) = 8;
i=i+1;

CaseDIR  = 'Janus\Re190_Nu015\countDB__d3_384cpu_dt2e4_smallTol123__Intel11_Zoltan11\';
CaseName = 'count_DB_InternalBoundary.log';
Case_Path_Name{i} = [DIR CaseDIR CaseName ];   %.p0_user_stats
t_begin(i) = 0;
CaseLegend{i}      = ['d3  384cpu  Re=190 Janus'];
CaseLegend{i}      = ['384cpu'];
Number_Processors{i} = 384;
J_max(i) = 8;
i=i+1;
      






    case {2048}

x_min = 0.0;
x_max = 0.2; 

eps = 0.43;
nu  = 0.006;
C_f = 6.66667;

Title_Part_1 = 'SCALES LKM'; %'CVS  (( Initialized by CVS cinit-00375-eps005.0000.res (serial) ))'
J_max_Title  = 9;
Resolution_Title = 2048;
Reynolds = 320;
        
DIR      = 'C:\Documents and Settings\Alireza\My Documents\T e r a__G r i d\SCALES_Eps43_Cf666\';

CaseDIR  = 'Janus\LKM_Re320_Nu006_Eps43\countDB__d3_192cpu_dt2e4_smallTol123\';
CaseName = 'count_DB_InternalBoundary.log';
Case_Path_Name{i} = [DIR CaseDIR CaseName ];   %.p0_user_stats
t_begin(i) = 0;
CaseLegend{i}      = ['d3  192cpu  Re=190 LKM Janus'];
CaseLegend{i}      = ['192cpu'];
Number_Processors{i} = 192;
J_max(i) = 8;
i=i+1;








    case {1024.31}

x_min = 0.0;
x_max = 0.2; 

eps = 0.2;
nu  = 0.015;
C_f = 6.66667;

Title_Part_1 = 'CVS'; %'CVS  (( Initialized by CVS cinit-00375-eps005.0000.res (serial) ))'
J_max_Title  = 8;
Resolution_Title = 1024;
Reynolds = 190;
        
DIR      = 'D:\T e r a__G r i d\CVS_Eps2_Cf666\Janus\Re190_Nu015\';

CaseDIR  = 'Scalability_64cpu\';
CaseName = 'count_DB_InternalBoundary.log';
Case_Path_Name{i} = [DIR CaseDIR CaseName ];   %.p0_user_stats
t_begin(i) = 0;
CaseLegend{i}      = ['d3  64cpu  Re=190 Janus'];
CaseLegend{i}      = ['    64 cpu'];
Number_Processors{i} = 64;
J_max(i) = 8;
i=i+1;

CaseDIR  = 'Scalability_128cpu\';
CaseName = 'count_DB_InternalBoundary.log';
Case_Path_Name{i} = [DIR CaseDIR CaseName ];   %.p0_user_stats
t_begin(i) = 0;
CaseLegend{i}      = ['d3  128cpu  Re=190 Janus'];
CaseLegend{i}      = ['  128 cpu'];
Number_Processors{i} = 128;
J_max(i) = 8;
i=i+1;

CaseDIR  = 'Scalability_256cpu\';
CaseName = 'count_DB_InternalBoundary.log';
Case_Path_Name{i} = [DIR CaseDIR CaseName ];   %.p0_user_stats
t_begin(i) = 0;
CaseLegend{i}      = ['d3  256cpu  Re=190 Janus'];
CaseLegend{i}      = ['  256 cpu'];
Number_Processors{i} = 256;
J_max(i) = 8;
i=i+1;

CaseDIR  = 'Scalability_512cpu\';
CaseName = 'count_DB_InternalBoundary.log';
Case_Path_Name{i} = [DIR CaseDIR CaseName ];   %.p0_user_stats
t_begin(i) = 0;
CaseLegend{i}      = ['d3  512cpu  Re=190 Janus'];
CaseLegend{i}      = ['  512 cpu'];
Number_Processors{i} = 512;
J_max(i) = 8;
i=i+1;

CaseDIR  = 'Scalability_1024cpu\';
CaseName = 'count_DB_InternalBoundary.log';
Case_Path_Name{i} = [DIR CaseDIR CaseName ];   %.p0_user_stats
t_begin(i) = 0;
CaseLegend{i}      = ['d3  1024cpu  Re=190 Janus'];
CaseLegend{i}      = ['1024 cpu'];
Number_Processors{i} = 1024;
J_max(i) = 8;
i=i+1;

      






    case {2048.31}

x_min = 0.0;
x_max = 0.2; 

eps = 0.2;
nu  = 0.006;
C_f = 6.66667;

Title_Part_1 = 'CVS'; %'CVS  (( Initialized by CVS cinit-00375-eps005.0000.res (serial) ))'
J_max_Title  = 9;
Resolution_Title = 2048;
Reynolds = 320;
        
DIR      = 'D:\T e r a__G r i d\CVS_Eps2_Cf666\Janus\Re320_Nu006\';

CaseDIR  = 'Scalability_64cpu\';
CaseName = 'count_DB_InternalBoundary.log';
Case_Path_Name{i} = [DIR CaseDIR CaseName ];   %.p0_user_stats
t_begin(i) = 0;
CaseLegend{i}      = ['d3  64cpu  Re=320 Janus'];
CaseLegend{i}      = ['    64 cpu'];
Number_Processors{i} = 64;
J_max(i) = 9;
i=i+1;

CaseDIR  = 'Scalability_128cpu\';
CaseName = 'count_DB_InternalBoundary.log';
Case_Path_Name{i} = [DIR CaseDIR CaseName ];   %.p0_user_stats
t_begin(i) = 0;
CaseLegend{i}      = ['d3  128cpu  Re=320 Janus'];
CaseLegend{i}      = ['  128 cpu'];
Number_Processors{i} = 128;
J_max(i) = 9;
i=i+1;

CaseDIR  = 'Scalability_256cpu\';
CaseName = 'count_DB_InternalBoundary.log';
Case_Path_Name{i} = [DIR CaseDIR CaseName ];   %.p0_user_stats
t_begin(i) = 0;
CaseLegend{i}      = ['d3  256cpu  Re=320 Janus'];
CaseLegend{i}      = ['  256 cpu'];
Number_Processors{i} = 256;
J_max(i) = 9;
i=i+1;

CaseDIR  = 'Scalability_512cpu\';
CaseName = 'count_DB_InternalBoundary.log';
Case_Path_Name{i} = [DIR CaseDIR CaseName ];   %.p0_user_stats
t_begin(i) = 0;
CaseLegend{i}      = ['d3  512cpu  Re=320 Janus'];
CaseLegend{i}      = ['  512 cpu'];
Number_Processors{i} = 512;
J_max(i) = 9;
i=i+1;

CaseDIR  = 'Scalability_1024cpu\';
CaseName = 'count_DB_InternalBoundary.log';
Case_Path_Name{i} = [DIR CaseDIR CaseName ];   %.p0_user_stats
t_begin(i) = 0;
CaseLegend{i}      = ['d3  1024cpu  Re=320 Janus'];
CaseLegend{i}      = ['1024 cpu'];
Number_Processors{i} = 1024;
J_max(i) = 9;
i=i+1;

CaseDIR  = 'Scalability_2048cpu\';
CaseName = 'count_DB_InternalBoundary.log';
Case_Path_Name{i} = [DIR CaseDIR CaseName ];   %.p0_user_stats
t_begin(i) = 0;
CaseLegend{i}      = ['d3  2048cpu  Re=320 Janus'];
CaseLegend{i}      = ['2048 cpu'];
Number_Processors{i} = 2048;
J_max(i) = 9;
i=i+1;









    case {2048.21}

x_min = 0.0;
x_max = 0.2; 

eps = 0.2;
nu  = 0.006;
C_f = 6.66667;

Title_Part_1 = 'CVS'; %'CVS  (( Initialized by CVS cinit-00375-eps005.0000.res (serial) ))'
J_max_Title  = 9;
Resolution_Title = 2048;
Reynolds = 320;
        
DIR      = 'C:\Documents and Settings\Alireza\My Documents\T e r a__G r i d\CVS_Eps2_Cf666\Janus\Re320_Nu006\';

CaseDIR  = 'Ndifprd2_Scalability_64cpu\';
CaseName = 'count_DB_InternalBoundary.log';
Case_Path_Name{i} = [DIR CaseDIR CaseName ];   %.p0_user_stats
t_begin(i) = 0;
CaseLegend{i}      = ['d3  64cpu  Re=320 Janus'];
CaseLegend{i}      = ['64cpu'];
Number_Processors{i} = 64;
J_max(i) = 9;
i=i+1;

CaseDIR  = 'Ndifprd2_Scalability_128cpu\';
CaseName = 'count_DB_InternalBoundary.log';
Case_Path_Name{i} = [DIR CaseDIR CaseName ];   %.p0_user_stats
t_begin(i) = 0;
CaseLegend{i}      = ['d3  128cpu  Re=320 Janus'];
CaseLegend{i}      = ['128cpu'];
Number_Processors{i} = 128;
J_max(i) = 9;
i=i+1;

CaseDIR  = 'Ndifprd2_Scalability_256cpu\';
CaseName = 'count_DB_InternalBoundary.log';
Case_Path_Name{i} = [DIR CaseDIR CaseName ];   %.p0_user_stats
t_begin(i) = 0;
CaseLegend{i}      = ['d3  256cpu  Re=320 Janus'];
CaseLegend{i}      = ['256cpu'];
Number_Processors{i} = 256;
J_max(i) = 9;
i=i+1;

CaseDIR  = 'Ndifprd2_Scalability_512cpu\';
CaseName = 'count_DB_InternalBoundary.log';
Case_Path_Name{i} = [DIR CaseDIR CaseName ];   %.p0_user_stats
t_begin(i) = 0;
CaseLegend{i}      = ['d3  512cpu  Re=320 Janus'];
CaseLegend{i}      = ['512cpu'];
Number_Processors{i} = 512;
J_max(i) = 9;
i=i+1;

% CaseDIR  = 'Ndifprd2_Scalability_1024cpu\';
% CaseName = 'count_DB_InternalBoundary.log';
% Case_Path_Name{i} = [DIR CaseDIR CaseName ];   %.p0_user_stats
% t_begin(i) = 0;
% CaseLegend{i}      = ['d3  1024cpu  Re=320 Janus'];
% CaseLegend{i}      = ['1024cpu'];
% Number_Processors{i} = 1024;
% J_max(i) = 8;
% i=i+1;
% 
% CaseDIR  = 'Ndifprd2_Scalability_2048cpu\';
% CaseName = 'count_DB_InternalBoundary.log';
% Case_Path_Name{i} = [DIR CaseDIR CaseName ];   %.p0_user_stats
% t_begin(i) = 0;
% CaseLegend{i}      = ['d3  2048cpu  Re=320 Janus'];
% CaseLegend{i}      = ['2048cpu'];
% Number_Processors{i} = 2048;
% J_max(i) = 8;
% i=i+1;








    case {2048.121}

x_min = 0.05;
x_max = 0.07; 

eps = 0.2;
nu  = 0.006;
C_f = 6.66667;

Title_Part_1 = 'CVS'; %'CVS  (( Initialized by CVS cinit-00375-eps005.0000.res (serial) ))'
J_max_Title  = 9;
Resolution_Title = 2048;
Reynolds = 320;
        
DIR      = 'D:\T e r a__G r i d\CVS_Eps2_Cf666\Janus\Re320_Nu006\';

CaseDIR  = 'Scalability_64cpu__ONE2ONE_RKL_1\';
CaseName = 'count_DB_InternalBoundary.log';
Case_Path_Name{i} = [DIR CaseDIR CaseName ];   %.p0_user_stats
t_begin(i) = 0;
CaseLegend{i}      = ['d3  64cpu  Re=320 Janus'];
CaseLegend{i}      = ['    64 cpu'];
Number_Processors{i} = 64;
J_max(i) = 9;
i=i+1;

CaseDIR  = 'Scalability_128cpu__ONE2ONE_RKL_1\';
CaseName = 'count_DB_InternalBoundary.log';
Case_Path_Name{i} = [DIR CaseDIR CaseName ];   %.p0_user_stats
t_begin(i) = 0;
CaseLegend{i}      = ['d3  128cpu  Re=320 Janus'];
CaseLegend{i}      = ['  128 cpu'];
Number_Processors{i} = 128;
J_max(i) = 9;
i=i+1;

CaseDIR  = 'Scalability_256cpu__ONE2ONE_RKL_1\';
CaseName = 'count_DB_InternalBoundary.log';
Case_Path_Name{i} = [DIR CaseDIR CaseName ];   %.p0_user_stats
t_begin(i) = 0;
CaseLegend{i}      = ['d3  256cpu  Re=320 Janus'];
CaseLegend{i}      = ['  256 cpu'];
Number_Processors{i} = 256;
J_max(i) = 9;
i=i+1;

CaseDIR  = 'Scalability_512cpu__ONE2ONE_RKL_1\';
CaseName = 'count_DB_InternalBoundary.log';
Case_Path_Name{i} = [DIR CaseDIR CaseName ];   %.p0_user_stats
t_begin(i) = 0;
CaseLegend{i}      = ['d3  512cpu  Re=320 Janus'];
CaseLegend{i}      = ['  512 cpu'];
Number_Processors{i} = 512;
J_max(i) = 9;
i=i+1;

CaseDIR  = 'Scalability_1024cpu__ONE2ONE_RKL_1\';
CaseName = 'count_DB_InternalBoundary.log';
Case_Path_Name{i} = [DIR CaseDIR CaseName ];   %.p0_user_stats
t_begin(i) = 0;
CaseLegend{i}      = ['d3  1024cpu  Re=320 Janus'];
CaseLegend{i}      = ['1024 cpu'];
Number_Processors{i} = 1024;
J_max(i) = 9;
i=i+1;

CaseDIR  = 'Scalability_2048cpu__ONE2ONE_RKL_1\';
CaseName = 'count_DB_InternalBoundary.log';
Case_Path_Name{i} = [DIR CaseDIR CaseName ];   %.p0_user_stats
t_begin(i) = 0;
CaseLegend{i}      = ['d3  2048cpu  Re=320 Janus'];
CaseLegend{i}      = ['2048 cpu'];
Number_Processors{i} = 2048;
J_max(i) = 9;
i=i+1;

LastTime   = 6.3842E-02; %1.1067E-02;
doLastLine = true;
LastLine1  = 24; %165;        
TIMERS_TYPE = 'Short';















    case {2048.31121}

x_min = 0.0;
x_max = 0.2; 

eps = 0.2;
nu  = 0.006;
C_f = 6.66667;

Title_Part_1 = 'CVS'; %'CVS  (( Initialized by CVS cinit-00375-eps005.0000.res (serial) ))'
J_max_Title  = 9;
Resolution_Title = 2048;
Reynolds = 320;
        
DIR      = 'D:\T e r a__G r i d\CVS_Eps2_Cf666\Janus\Re320_Nu006\';

CaseDIR  = 'Scalability_64cpu\';
CaseName = 'count_DB_InternalBoundary.log';
Case_Path_Name{i} = [DIR CaseDIR CaseName ];   %.p0_user_stats
t_begin(i) = 0;
CaseLegend{i}      = ['d3  64cpu  Re=320 Janus'];
CaseLegend{i}      = ['    64 cpu'];
Number_Processors{i} = 64;
J_max(i) = 9;
i=i+1;

CaseDIR  = 'Scalability_128cpu\';
CaseName = 'count_DB_InternalBoundary.log';
Case_Path_Name{i} = [DIR CaseDIR CaseName ];   %.p0_user_stats
t_begin(i) = 0;
CaseLegend{i}      = ['d3  128cpu  Re=320 Janus'];
CaseLegend{i}      = ['  128 cpu'];
Number_Processors{i} = 128;
J_max(i) = 9;
i=i+1;

CaseDIR  = 'Scalability_256cpu\';
CaseName = 'count_DB_InternalBoundary.log';
Case_Path_Name{i} = [DIR CaseDIR CaseName ];   %.p0_user_stats
t_begin(i) = 0;
CaseLegend{i}      = ['d3  256cpu  Re=320 Janus'];
CaseLegend{i}      = ['  256 cpu'];
Number_Processors{i} = 256;
J_max(i) = 9;
i=i+1;

CaseDIR  = 'Scalability_512cpu\';
CaseName = 'count_DB_InternalBoundary.log';
Case_Path_Name{i} = [DIR CaseDIR CaseName ];   %.p0_user_stats
t_begin(i) = 0;
CaseLegend{i}      = ['d3  512cpu  Re=320 Janus'];
CaseLegend{i}      = ['  512 cpu'];
Number_Processors{i} = 512;
J_max(i) = 9;
i=i+1;

CaseDIR  = 'Scalability_1024cpu\';
CaseName = 'count_DB_InternalBoundary.log';
Case_Path_Name{i} = [DIR CaseDIR CaseName ];   %.p0_user_stats
t_begin(i) = 0;
CaseLegend{i}      = ['d3  1024cpu  Re=320 Janus'];
CaseLegend{i}      = ['1024 cpu'];
Number_Processors{i} = 1024;
J_max(i) = 9;
i=i+1;

CaseDIR  = 'Scalability_2048cpu\';
CaseName = 'count_DB_InternalBoundary.log';
Case_Path_Name{i} = [DIR CaseDIR CaseName ];   %.p0_user_stats
t_begin(i) = 0;
CaseLegend{i}      = ['d3  2048cpu  Re=320 Janus'];
CaseLegend{i}      = ['2048 cpu'];
Number_Processors{i} = 2048;
J_max(i) = 9;
i=i+1;


%x_min = 0.05;
%x_max = 0.07; 

CaseDIR  = 'Scalability_64cpu__ONE2ONE_RKL_1\';
CaseName = 'count_DB_InternalBoundary.log';
Case_Path_Name{i} = [DIR CaseDIR CaseName ];   %.p0_user_stats
t_begin(i) = 0;
CaseLegend{i}      = ['d3  64cpu  Re=320 Janus'];
CaseLegend{i}      = ['    64 cpu'];
Number_Processors{i} = 64;
J_max(i) = 9;
i=i+1;

CaseDIR  = 'Scalability_128cpu__ONE2ONE_RKL_1\';
CaseName = 'count_DB_InternalBoundary.log';
Case_Path_Name{i} = [DIR CaseDIR CaseName ];   %.p0_user_stats
t_begin(i) = 0;
CaseLegend{i}      = ['d3  128cpu  Re=320 Janus'];
CaseLegend{i}      = ['  128 cpu'];
Number_Processors{i} = 128;
J_max(i) = 9;
i=i+1;

CaseDIR  = 'Scalability_256cpu__ONE2ONE_RKL_1\';
CaseName = 'count_DB_InternalBoundary.log';
Case_Path_Name{i} = [DIR CaseDIR CaseName ];   %.p0_user_stats
t_begin(i) = 0;
CaseLegend{i}      = ['d3  256cpu  Re=320 Janus'];
CaseLegend{i}      = ['  256 cpu'];
Number_Processors{i} = 256;
J_max(i) = 9;
i=i+1;

CaseDIR  = 'Scalability_512cpu__ONE2ONE_RKL_1\';
CaseName = 'count_DB_InternalBoundary.log';
Case_Path_Name{i} = [DIR CaseDIR CaseName ];   %.p0_user_stats
t_begin(i) = 0;
CaseLegend{i}      = ['d3  512cpu  Re=320 Janus'];
CaseLegend{i}      = ['  512 cpu'];
Number_Processors{i} = 512;
J_max(i) = 9;
i=i+1;

CaseDIR  = 'Scalability_1024cpu__ONE2ONE_RKL_1\';
CaseName = 'count_DB_InternalBoundary.log';
Case_Path_Name{i} = [DIR CaseDIR CaseName ];   %.p0_user_stats
t_begin(i) = 0;
CaseLegend{i}      = ['d3  1024cpu  Re=320 Janus'];
CaseLegend{i}      = ['1024 cpu'];
Number_Processors{i} = 1024;
J_max(i) = 9;
i=i+1;

CaseDIR  = 'Scalability_2048cpu__ONE2ONE_RKL_1\';
CaseName = 'count_DB_InternalBoundary.log';
Case_Path_Name{i} = [DIR CaseDIR CaseName ];   %.p0_user_stats
t_begin(i) = 0;
CaseLegend{i}      = ['d3  2048cpu  Re=320 Janus'];
CaseLegend{i}      = ['2048 cpu'];
Number_Processors{i} = 2048;
J_max(i) = 9;
i=i+1;

% LastTime   = 6.3842E-02; %1.1067E-02;
% doLastLine = true;
% LastLine1  = 24; %165;        
% TIMERS_TYPE = 'Short';

end







global CaseLegend
global Font_Size

Number_Case = i-1;















%% COMPARE    Nwlt    for   all  Different  Runs

if (Plot_Nwlt)
figure ('FileName', 'nwlt.fig')
hold on;

for i = 1:Number_Case    
    Log_Info = load( Case_Path_Name{i} );
    plot( Log_Info(:,1), LineColorMarker_max{i}, 'LineWidth', LineWidth{i} );    
    fprintf('%d  Case File Name (Full Path)  : %s \n', i, Case_Path_Name{i} );
end




AddLegend(Number_Case);

title([Title_Part_1 10 num2str(Resolution_Title),'^3','   \epsilon=',num2str(eps),'   J_{max}=',num2str(J_max_Title),'   \nu=',num2str(nu),'   C_f=',num2str(C_f),'']);

%xlim([0.0  x_max]);
%ylim([0 0.1]);
             
ylabel('N_w_l_t   ( {\Sigma} nwlt    over all  processors )');
%ylabel('nwlt');

xlabel('Iteration');             

ylabel_hand = get(gca,'ylabel');
set(ylabel_hand,'fontsize',Font_Size_Label);  
xlabel_hand = get(gca,'xlabel');
set(xlabel_hand,'fontsize',Font_Size_Label);  

end

















%% COMPARE  misballance  ( min,mean,max, min/mean max/mean )


if (Plot_MissBalance_NSig) 
    
   
    
    
%%% NSig (mean,max,min)
figure ('FileName', 'NSig.fig')
hold on;
set(gcf,'Position', [34,65,1280,662]); 

for i = 1:Number_Case     

    Log_Info = load( Case_Path_Name{i} );

    for j=2:length(Log_Info(:,1))
        length(Log_Info(:,1));
        
        for k = 1:Number_Processors{i}        
            var_number = 1 + 2 + (k-1)*(1+9);             % NWLT_Total,  [pr=0]  NWLT,  iSig,  iSig_I,  iSig_B, iGho, iGho_I, iGho_B, iGhoOnly, iGhoOnly_I, iGhoOnly_B,  [pr=1]  ...
            NSig_per_proc(k) = Log_Info(j, var_number);    
        end
        
        NSig_mean(i,j) =  mean( NSig_per_proc );
        NSig_std(i,j)  =  std(  NSig_per_proc );
        NSig_min(i,j)  =  min(  NSig_per_proc );
        NSig_max(i,j)  =  max(  NSig_per_proc );
    
        NSig_std_p(i,j)  =  NSig_mean(i,j) + NSig_std(i,j);
        NSig_std_m(i,j)  =  NSig_mean(i,j) - NSig_std(i,j);
    end
        
%     plot( Log_Info(2:end,1), Nwlt_mean(i,2:length(Log_Info(:,1))), LineColorMarker{i}, 'LineWidth', LineWidth{i} );
%     plot( Log_Info(2:end,1), Nwlt_std(i,2:length(Log_Info(:,1))), LineColorMarker{i}, 'LineWidth', LineWidth{i} );
%     plot( Log_Info(2:end,1), Nwlt_min(i,2:length(Log_Info(:,1))), LineColorMarker{i}, 'LineWidth', LineWidth{i} );
%     plot( Log_Info(2:end,1), Nwlt_max(i,2:length(Log_Info(:,1))), LineColorMarker{i}, 'LineWidth', LineWidth{i} );

    plot( NSig_mean(i,2:length(Log_Info(:,1))), LineColorMarker_avg{i}, 'LineWidth', LineWidth_avg{i} );
    plot(  NSig_min(i,2:length(Log_Info(:,1))), LineColorMarker_min{i}, 'LineWidth', LineWidth{i} );
    plot(  NSig_max(i,2:length(Log_Info(:,1))), LineColorMarker_max{i}, 'LineWidth', LineWidth{i} );
    %plot(  NSig_std(i,2:length(Log_Info(:,1))), LineColorMarker_std{i}, 'LineWidth', LineWidth{i} );
    plot(  NSig_std_m(i,2:length(Log_Info(:,1))), LineColorMarker_std{i}, 'LineWidth', LineWidth{i} );
    plot(  NSig_std_p(i,2:length(Log_Info(:,1))), LineColorMarker_std{i}, 'LineWidth', LineWidth{i} );
    
    clear NSig_per_proc
    
    NSig_mean_TimeAveraged(i) = mean(NSig_mean(i,2:length(Log_Info(:,1))));
    NSig_max_TimeAveraged(i)  = mean(NSig_max(i,2:length(Log_Info(:,1))));
    NSig_min_TimeAveraged(i)  = mean(NSig_min(i,2:length(Log_Info(:,1))));

end

box(gca,'on');

% AddLegend(Number_Case);
%legend(CaseLegend{1}, CaseLegend{2}, CaseLegend{3}, CaseLegend{4}, CaseLegend{7}, CaseLegend{10}, CaseLegend{13}, CaseLegend{8}, CaseLegend{9}, CaseLegend{10} ); 
%legend('2   cpu  mean','2   cpu  min','2   cpu  max', '4   cpu  mean','4   cpu  min','4   cpu  max', '8   cpu  mean','8   cpu  min','8   cpu  max', ...
%       '16  cpu  mean','16  cpu  min','16  cpu  max', '32  cpu  mean','32  cpu min','32  cpu  max', '64  cpu  mean','64  cpu  min','64  cpu  max');
%title(['Stochastic Coherent Adaptive Large Eddy Simulations (SCALES)' 10 'Forced Homogeneous Turbulence' 10 '\epsilon=0.43   \nu=0.09   C_f=6']);

%title([Title_Part_1 10 num2str(Resolution_Title),'^3','   \epsilon=',num2str(eps),'   J_{max}=',num2str(J_max_Title),'   \nu=',num2str(nu),'   C_f=',num2str(C_f),'']);

xlabel('Iteration');                     % ylabel('mean nwlt');% ylabel('std  nwlt');% ylabel('min nwlt');% ylabel('max nwlt');
ylabel('(mean, min, max, mean�std)  NSig');
ylabel('${\sf{ (mean, min, max, mean}} \hspace{-.1cm} \pm \hspace{-.1cm} {\sf{std)}} \hspace{0.2cm} {\sf{N}}_{\sf SA}$', 'Interpreter','Latex','fontsize',Font_Size);

ylabel_hand = get(gca,'ylabel');
set(ylabel_hand,'fontsize',Font_Size);  
xlabel_hand = get(gca,'xlabel');
set(xlabel_hand,'fontsize',Font_Size_Label);  
 
clear NSig_mean NSig_std NSig_min NSig_max NSig_std_p NSig_std_m


switch Number_Processors{Number_Case}
   case 1024
        text('String','max',           'Position',[130 604545.454545454 ],     'FontSize',13);
        text('String','mean+stdev',    'Position',[130 568181.818181818 ],     'FontSize',13);
        text('String','mean   64 cpu', 'Position',[130 517532.467532468 ],     'FontSize',13);
        text('String','mean-stdev',    'Position',[130 451298.701298701 ],     'FontSize',13);
        text('String','min',           'Position',[130 229220.779220779 ],     'FontSize',13);
       %text('String','min',           'Position',[113.214285714286 229220.779220779 ],     'FontSize',13);

        text('String','Represents Miss Balance',    'Position',[253.928571428571 515965.583173996 ],    'FontSize',13);
        annotation('doublearrow',[0.4046875 0.4046875],    [0.783489096573209 0.638629283489097]);

        text('String','128 cpu',    'Position',[228.214285714286 305831.739961759 ],    'FontSize',13,    'Color',[1 0 0]);
        text('String','256 cpu',    'Position',[373.214285714286 185372.848948375 ],    'FontSize',13,    'Color',[0 0 1]);
        text('String','512 cpu',    'Position',[561.071428571429 134512.428298279 ],    'FontSize',13,    'Color',[0 1 0]);
        text('String','1024 cpu',   'Position',[648.214285714286 56883.3652007648 ],    'FontSize',13,    'Color',[1 0 1]);
   case 2048
       switch Test_Number
           case 2048.31
        text('String','max',           'Position',[32 1456029.68460111 ],     'FontSize',13);
        text('String','mean+stdev',    'Position',[32 1390723.56215213 ],     'FontSize',13);
        text('String','mean   64 cpu', 'Position',[32 1248237.47680891 ],     'FontSize',13);
        text('String','mean-stdev',    'Position',[32 1093877.55102041 ],     'FontSize',13);
        text('String','min',           'Position',[32 440816.326530612 ],     'FontSize',13);
       %text('String','min',           'Position',[30.2466598150051 440816.326530612 ],     'FontSize',13);

        text('String','Represents Miss Balance',    'Position',[62.9907502569373 1245269.01669759 ],    'FontSize',13);
        annotation('doublearrow',[0.4 0.4], [0.821752265861027 0.664309041797254]);

        text('String','128 cpu',    'Position',[53.5560123329908 684230.055658627 ],    'FontSize',13,    'Color',[1 0 0]);
        text('String','256 cpu',    'Position',[98.5097636176773 387384.044526902 ],    'FontSize',13,    'Color',[0 0 1]);
        text('String','512 cpu',    'Position',[172.322713257965 212244.897959183 ],    'FontSize',13,    'Color',[0 1 0]);
        text('String','1024 cpu',   'Position',[170.65775950668 123191.094619666 ],     'FontSize',13,    'Color',[1 0 1]);
        text('String','2048 cpu',   'Position',[26.5467625899281 78664.1929499072 ],    'FontSize',13,    'Color','c');
           case 2048.121
        text('String','max',           'Position',[32 1456029.68460111 ],     'FontSize',13);
        text('String','mean+stdev',    'Position',[32 1390723.56215213 ],     'FontSize',13);
        text('String','mean   64 cpu', 'Position',[32 1248237.47680891 ],     'FontSize',13);
        text('String','mean-stdev',    'Position',[32 1093877.55102041 ],     'FontSize',13);
        text('String','min',           'Position',[32 440816.326530612 ],     'FontSize',13);
       %text('String','min',           'Position',[30.2466598150051 440816.326530612 ],     'FontSize',13);

        text('String','Represents Miss Balance',    'Position',[87.449083503055 1245269.01669759 ],    'FontSize',13);
        annotation('doublearrow',[0.4 0.4], [0.821752265861027 0.664309041797254]);

        text('String','128 cpu',    'Position',[53.5560123329908 684230.055658627 ],    'FontSize',13,    'Color',[1 0 0]);
        text('String','256 cpu',    'Position',[98.5097636176773 387384.044526902 ],    'FontSize',13,    'Color',[0 0 1]);
        text('String','512 cpu',    'Position',[172.322713257965 266666.666666667 ],    'FontSize',13,    'Color',[0 1 0]);
        text('String','1024 cpu',   'Position',[232.56109979633408 123861.56648451742 ],     'FontSize',13,    'Color',[1 0 1]);
        text('String','2048 cpu',   'Position',[202.77494908350312 77231.32969034626 ],    'FontSize',13,    'Color','c');
       end
end










figure('FileName', 'NSig_Ncpu.fig')
hold on;
i_start=1;
    
for i = i_start:Number_Case  % ii:ii+2
    plot( Number_Processors{i}, NSig_mean_TimeAveraged(i), LineColorMarker_avg_TimeAverage{i}, 'LineWidth', LineWidth_avg{i} );
    plot( Number_Processors{i},  NSig_min_TimeAveraged(i), LineColorMarker_min_TimeAverage{i}, 'LineWidth', LineWidth{i} );
    plot( Number_Processors{i},  NSig_max_TimeAveraged(i), LineColorMarker_max_TimeAverage{i}, 'LineWidth', LineWidth{i} );
end

% legend('mean', 'min', 'max', 'Location','NorthEast', 'FontSize',Font_Size-4);


switch Number_Processors{Number_Case}
   case 64
       set(gca,'XTick',[2 4 8 16 32 64])
       %set(gca,'YTick',[2 4 8 16 32 64])
   case 128
       set(gca,'XTick',[2 4 8 16 32 64 128])
       %set(gca,'YTick',[2 4 8 16 32 64 128])
   case 256
       set(gca,'XTick',[2 4 8 16 32 64 128 256])
       %set(gca,'YTick',[2 4 8 16 32 64 128 256])
   case 512
       set(gca,'XTick',[2 4 8 16 32 64 128 256 512])
       %set(gca,'YTick',[2 4 8 16 32 64 128 256 512])
   case 1024
       set(gca,'XTick',[2 4 8 16 32 64 128 256 512 1024])
       set(gca,'XTick',[64 128 256 512 1024])
       %set(gca,'YTick',[2 4 8 16 32 64 128 256 512 1024])
   case 2048
       set(gca,'XTick',[2 4 8 16 32 64 128 256 512 1024 2048])
       %set(gca,'YTick',[2 4 8 16 32 64 128 256 512 1024 2048])
       set(gca,'XTick',[64 128 256 512 1024 2048])
       %set(gca,'YTick',[64 128 256 512 1024 2048])
end

box(gca,'on');

xlim([0 Number_Processors{Number_Case}+50]);
% ylim([0 Number_Processors{Number_Case}+10]);

%set(gca,'XTickLabel',{'2', '4', '8', '16', '32', '64', '128'})

ylabel('$\overline{<\sf{NSig}>}$','interpreter','latex');
%ylabel('Speed Up      based on      CPU_T_i_m_e   for    Time Integration');
xlabel('Number of Processors');
ylabel('$\overline{{\sf{min}}({\sf{N}}_{\sf SA})}, \hspace{0.2cm} \overline{ \langle {\sf{N}}_{\sf SA} \rangle}, \hspace{0.2cm} \overline{{\sf{max}}({\sf{N}}_{\sf SA})}$', 'Interpreter','Latex','fontsize',Font_Size);

%title([Title_Part_1 10 num2str(Resolution_Title),'^3','   \epsilon=',num2str(eps),'   J_{max}=',num2str(J_max_Title),'   \nu=',num2str(nu),'   C_f=',num2str(C_f),'']);

ylabel_hand = get(gca,'ylabel');
set(ylabel_hand,'fontsize',Font_Size);  
xlabel_hand = get(gca,'xlabel');
set(xlabel_hand,'fontsize',Font_Size_Label);  

clear NSig_mean_TimeAveraged NSig_min_TimeAveraged NSig_max_TimeAveraged







%%% NSig_I (mean,max,min)
figure ('FileName', 'NSig_I.fig')
hold on;
set(gcf,'Position', [34,65,1280,662]); 

for i = 1:Number_Case     

    Log_Info = load( Case_Path_Name{i} );

    for j=2:length(Log_Info(:,1))
        length(Log_Info(:,1));
        
        for k = 1:Number_Processors{i}        
            var_number = 1 + 3 + (k-1)*(1+9);             % NWLT_Total,  [pr=0]  NWLT,  iSig,  iSig_I,  iSig_B, iGho, iGho_I, iGho_B, iGhoOnly, iGhoOnly_I, iGhoOnly_B,  [pr=1]  ...
            NSig_per_proc(k) = Log_Info(j, var_number);    
        end
        
        NSig_mean(i,j) =  mean( NSig_per_proc );
        NSig_std(i,j)  =  std(  NSig_per_proc );
        NSig_min(i,j)  =  min(  NSig_per_proc );
        NSig_max(i,j)  =  max(  NSig_per_proc );
    
        NSig_std_p(i,j)  =  NSig_mean(i,j) + NSig_std(i,j);
        NSig_std_m(i,j)  =  NSig_mean(i,j) - NSig_std(i,j);
    
    end
        
%     plot( Log_Info(2:end,1), Nwlt_mean(i,2:length(Log_Info(:,1))), LineColorMarker{i}, 'LineWidth', LineWidth{i} );
%     plot( Log_Info(2:end,1), Nwlt_std(i,2:length(Log_Info(:,1))), LineColorMarker{i}, 'LineWidth', LineWidth{i} );
%     plot( Log_Info(2:end,1), Nwlt_min(i,2:length(Log_Info(:,1))), LineColorMarker{i}, 'LineWidth', LineWidth{i} );
%     plot( Log_Info(2:end,1), Nwlt_max(i,2:length(Log_Info(:,1))), LineColorMarker{i}, 'LineWidth', LineWidth{i} );

    plot( NSig_mean(i,2:length(Log_Info(:,1))), LineColorMarker_avg{i}, 'LineWidth', LineWidth_avg{i} );
    plot(  NSig_min(i,2:length(Log_Info(:,1))), LineColorMarker_min{i}, 'LineWidth', LineWidth{i} );
    plot(  NSig_max(i,2:length(Log_Info(:,1))), LineColorMarker_max{i}, 'LineWidth', LineWidth{i} );
    %plot(  NSig_std(i,2:length(Log_Info(:,1))), LineColorMarker_std{i}, 'LineWidth', LineWidth{i} );
    plot(  NSig_std_m(i,2:length(Log_Info(:,1))), LineColorMarker_std{i}, 'LineWidth', LineWidth{i} );
    plot(  NSig_std_p(i,2:length(Log_Info(:,1))), LineColorMarker_std{i}, 'LineWidth', LineWidth{i} );
    
    clear NSig_per_proc

end


% AddLegend(Number_Case);
%legend(CaseLegend{1}, CaseLegend{2}, CaseLegend{3}, CaseLegend{4}, CaseLegend{7}, CaseLegend{10}, CaseLegend{13}, CaseLegend{8}, CaseLegend{9}, CaseLegend{10} ); 
%legend('2   cpu  mean','2   cpu  min','2   cpu  max', '4   cpu  mean','4   cpu  min','4   cpu  max', '8   cpu  mean','8   cpu  min','8   cpu  max', ...
%       '16  cpu  mean','16  cpu  min','16  cpu  max', '32  cpu  mean','32  cpu min','32  cpu  max', '64  cpu  mean','64  cpu  min','64  cpu  max');
%title(['Stochastic Coherent Adaptive Large Eddy Simulations (SCALES)' 10 'Forced Homogeneous Turbulence' 10 '\epsilon=0.43   \nu=0.09   C_f=6']);
%title([Title_Part_1 10 num2str(Resolution_Title),'^3','   \epsilon=',num2str(eps),'   J_{max}=',num2str(J_max_Title),'   \nu=',num2str(nu),'   C_f=',num2str(C_f),'']);

xlabel('Iteration');                     % ylabel('mean nwlt');% ylabel('std  nwlt');% ylabel('min nwlt');% ylabel('max nwlt');
ylabel('(mean, min, max, mean�std)  NSig_{Internal}');
ylabel('${\sf{ (mean, min, max, mean}} \hspace{-.1cm} \pm \hspace{-.1cm} {\sf{std)}} \hspace{0.2cm} {\sf{N}}_{{\sf SA}_{\sf \; Internal}}$', 'Interpreter','Latex','fontsize',Font_Size);

ylabel_hand = get(gca,'ylabel');
set(ylabel_hand,'fontsize',Font_Size);  
xlabel_hand = get(gca,'xlabel');
set(xlabel_hand,'fontsize',Font_Size_Label);  

clear NSig_mean NSig_std NSig_min NSig_max NSig_std_p NSig_std_m



switch Number_Processors{Number_Case}
   case 1024
        text('String','max',           'Position',[130 604545.454545454 ],     'FontSize',13);
        text('String','mean+stdev',    'Position',[130 568181.818181818 ],     'FontSize',13);
        text('String','mean   64 cpu', 'Position',[130 517532.467532468 ],     'FontSize',13);
        text('String','mean-stdev',    'Position',[130 451298.701298701 ],     'FontSize',13);
        text('String','min',           'Position',[130 229220.779220779 ],     'FontSize',13);
       %text('String','min',           'Position',[113.214285714286 229220.779220779 ],     'FontSize',13);

        text('String','Represents Miss Balance',    'Position',[253.928571428571 515965.583173996 ],    'FontSize',13);
        annotation('doublearrow',[0.4046875 0.4046875],    [0.783489096573209 0.638629283489097]);

        text('String','128 cpu',    'Position',[228.214285714286 305831.739961759 ],    'FontSize',13,    'Color',[1 0 0]);
        text('String','256 cpu',    'Position',[373.214285714286 185372.848948375 ],    'FontSize',13,    'Color',[0 0 1]);
        text('String','512 cpu',    'Position',[561.071428571429 134512.428298279 ],    'FontSize',13,    'Color',[0 1 0]);
        text('String','1024 cpu',   'Position',[648.214285714286 56883.3652007648 ],    'FontSize',13,    'Color',[1 0 1]);
   case 2048
       switch Test_Number
           case 2048.31
        text('String','max',           'Position',[32 961966.6048237474 ],     'FontSize',13);
        text('String','mean+stdev',    'Position',[32 932282.0037105749 ],     'FontSize',13);
        text('String','mean   64 cpu', 'Position',[32 828385.8998144713 ],     'FontSize',13);
        text('String','mean-stdev',    'Position',[32 715213.3580705007 ],     'FontSize',13);
        text('String','min',           'Position',[32 271799.6289424859 ],     'FontSize',13);
       %text('String','min',           'Position',[30.2466598150051 440816.326530612 ],     'FontSize',13);

        text('String','Represents Miss Balance',    'Position',[60.65420560747662 826530.612244898 ],    'FontSize',13);
        annotation('doublearrow',[0.4 0.4], [0.862537764350453 0.689988800105411]);

        text('String','128 cpu',    'Position',[53.5560123329908 410946.1966604822 ],    'FontSize',13,    'Color',[1 0 0]);
        text('String','256 cpu',    'Position',[98.5097636176773 201298.7012987011 ],    'FontSize',13,    'Color',[0 0 1]);
        text('String','512 cpu',    'Position',[172.322713257965 99257.88497217058 ],    'FontSize',13,    'Color',[0 1 0]);
        text('String','1024 cpu',   'Position',[170.65775950668  49165.12059369194 ],    'FontSize',13,    'Color',[1 0 1]);
        text('String','2048 cpu',   'Position',[26.5467625899281 25046.382189239142],    'FontSize',13,    'Color','c');
           case 2048.121
        text('String','max',           'Position',[32 961966.6048237474 ],     'FontSize',13);
        text('String','mean+stdev',    'Position',[32 932282.0037105749 ],     'FontSize',13);
        text('String','mean   64 cpu', 'Position',[32 828385.8998144713 ],     'FontSize',13);
        text('String','mean-stdev',    'Position',[32 715213.3580705007 ],     'FontSize',13);
        text('String','min',           'Position',[32 271799.6289424859 ],     'FontSize',13);
       %text('String','min',           'Position',[30.2466598150051 440816.326530612 ],     'FontSize',13);

        text('String','Represents Miss Balance',    'Position',[84.24195223260645 826530.612244898 ],    'FontSize',13);
        annotation('doublearrow',[0.4 0.4], [0.862537764350453 0.689988800105411]);

        text('String','128 cpu',    'Position',[53.5560123329908   410946.1966604822 ],    'FontSize',13,    'Color',[1 0 0]);
        text('String','256 cpu',    'Position',[98.5097636176773   201298.7012987011 ],    'FontSize',13,    'Color',[0 0 1]);
        text('String','512 cpu',    'Position',[172.322713257965   121521.3358070501 ],    'FontSize',13,    'Color',[0 1 0]);
        text('String','1024 cpu',   'Position',[232.56109979633408 49165.12059369194 ],    'FontSize',13,    'Color',[1 0 1]);
        text('String','2048 cpu',   'Position',[202.77494908350312 25046.382189239142 ],   'FontSize',13,    'Color','c');
       end
end


















%%% NSig_B (mean,max,min)
figure ('FileName', 'NSig_B.fig')
hold on;

for i = 1:Number_Case     

    Log_Info = load( Case_Path_Name{i} );

    for j=2:length(Log_Info(:,1))
        length(Log_Info(:,1));
        
        for k = 1:Number_Processors{i}        
            var_number = 1 + 4 + (k-1)*(1+9);             % NWLT_Total,  [pr=0]  NWLT,  iSig,  iSig_I,  iSig_B, iGho, iGho_I, iGho_B, iGhoOnly, iGhoOnly_I, iGhoOnly_B,  [pr=1]  ...
            NSig_per_proc(k) = Log_Info(j, var_number);    
        end
        
        NSig_mean(i,j) =  mean( NSig_per_proc );
        NSig_std(i,j)  =  std(  NSig_per_proc );
        NSig_min(i,j)  =  min(  NSig_per_proc );
        NSig_max(i,j)  =  max(  NSig_per_proc );
    
        NSig_std_p(i,j)  =  NSig_mean(i,j) + NSig_std(i,j);
        NSig_std_m(i,j)  =  NSig_mean(i,j) - NSig_std(i,j);
    
    end
        
%     plot( Log_Info(2:end,1), Nwlt_mean(i,2:length(Log_Info(:,1))), LineColorMarker{i}, 'LineWidth', LineWidth{i} );
%     plot( Log_Info(2:end,1), Nwlt_std(i,2:length(Log_Info(:,1))), LineColorMarker{i}, 'LineWidth', LineWidth{i} );
%     plot( Log_Info(2:end,1), Nwlt_min(i,2:length(Log_Info(:,1))), LineColorMarker{i}, 'LineWidth', LineWidth{i} );
%     plot( Log_Info(2:end,1), Nwlt_max(i,2:length(Log_Info(:,1))), LineColorMarker{i}, 'LineWidth', LineWidth{i} );

    plot( NSig_mean(i,2:length(Log_Info(:,1))), LineColorMarker_avg{i}, 'LineWidth', LineWidth_avg{i} );
    plot(  NSig_min(i,2:length(Log_Info(:,1))), LineColorMarker_min{i}, 'LineWidth', LineWidth{i} );
    plot(  NSig_max(i,2:length(Log_Info(:,1))), LineColorMarker_max{i}, 'LineWidth', LineWidth{i} );
    %plot(  NSig_std(i,2:length(Log_Info(:,1))), LineColorMarker_std{i}, 'LineWidth', LineWidth{i} );
    plot(  NSig_std_m(i,2:length(Log_Info(:,1))), LineColorMarker_std{i}, 'LineWidth', LineWidth{i} );
    plot(  NSig_std_p(i,2:length(Log_Info(:,1))), LineColorMarker_std{i}, 'LineWidth', LineWidth{i} );
    
    clear NSig_per_proc

end


% AddLegend(Number_Case);
%legend(CaseLegend{1}, CaseLegend{2}, CaseLegend{3}, CaseLegend{4}, CaseLegend{7}, CaseLegend{10}, CaseLegend{13}, CaseLegend{8}, CaseLegend{9}, CaseLegend{10} ); 
%legend('2   cpu  mean','2   cpu  min','2   cpu  max', '4   cpu  mean','4   cpu  min','4   cpu  max', '8   cpu  mean','8   cpu  min','8   cpu  max', ...
%       '16  cpu  mean','16  cpu  min','16  cpu  max', '32  cpu  mean','32  cpu min','32  cpu  max', '64  cpu  mean','64  cpu  min','64  cpu  max');
%title(['Stochastic Coherent Adaptive Large Eddy Simulations (SCALES)' 10 'Forced Homogeneous Turbulence' 10 '\epsilon=0.43   \nu=0.09   C_f=6']);
title([Title_Part_1 10 num2str(Resolution_Title),'^3','   \epsilon=',num2str(eps),'   J_{max}=',num2str(J_max_Title),'   \nu=',num2str(nu),'   C_f=',num2str(C_f),'']);

xlabel('Iteration');                     % ylabel('mean nwlt');% ylabel('std  nwlt');% ylabel('min nwlt');% ylabel('max nwlt');
ylabel('(mean, min, max, mean�std)  NSig_{Boundary}');

ylabel_hand = get(gca,'ylabel');
set(ylabel_hand,'fontsize',Font_Size_Label);  
xlabel_hand = get(gca,'xlabel');
set(xlabel_hand,'fontsize',Font_Size_Label);  

clear NSig_mean NSig_std NSig_min NSig_max NSig_std_p NSig_std_m






end
if (Plot_MissBalance) 




%%% NSig_B / NSig_I (mean,max,min)
figure ('FileName', 'NSig_B__NSig_I.fig')
hold on;

for i = 1:Number_Case     

    Log_Info = load( Case_Path_Name{i} );

    for j=2:length(Log_Info(:,1))
        length(Log_Info(:,1));
        
        for k = 1:Number_Processors{i}        
            var_number_I = 1 + 3 + (k-1)*(1+9);             % NWLT_Total,  [pr=0]  NWLT,  iSig,  iSig_I,  iSig_B, iGho, iGho_I, iGho_B, iGhoOnly, iGhoOnly_I, iGhoOnly_B,  [pr=1]  ...
            var_number_B = 1 + 4 + (k-1)*(1+9);             % NWLT_Total,  [pr=0]  NWLT,  iSig,  iSig_I,  iSig_B, iGho, iGho_I, iGho_B, iGhoOnly, iGhoOnly_I, iGhoOnly_B,  [pr=1]  ...
            NSig_per_proc(k) = Log_Info(j, var_number_B) / Log_Info(j, var_number_I);    
        end
        
        NSig_mean(i,j) =  mean( NSig_per_proc );
        NSig_std(i,j)  =  std(  NSig_per_proc );
        NSig_min(i,j)  =  min(  NSig_per_proc );
        NSig_max(i,j)  =  max(  NSig_per_proc );
    
    end
        
%     plot( Log_Info(2:end,1), Nwlt_mean(i,2:length(Log_Info(:,1))), LineColorMarker{i}, 'LineWidth', LineWidth{i} );
%     plot( Log_Info(2:end,1), Nwlt_std(i,2:length(Log_Info(:,1))), LineColorMarker{i}, 'LineWidth', LineWidth{i} );
%     plot( Log_Info(2:end,1), Nwlt_min(i,2:length(Log_Info(:,1))), LineColorMarker{i}, 'LineWidth', LineWidth{i} );
%     plot( Log_Info(2:end,1), Nwlt_max(i,2:length(Log_Info(:,1))), LineColorMarker{i}, 'LineWidth', LineWidth{i} );

    plot( NSig_mean(i,2:length(Log_Info(:,1))), LineColorMarker_avg{i}, 'LineWidth', LineWidth_avg{i} );
    plot(  NSig_min(i,2:length(Log_Info(:,1))), LineColorMarker_min{i}, 'LineWidth', LineWidth{i} );
    plot(  NSig_max(i,2:length(Log_Info(:,1))), LineColorMarker_max{i}, 'LineWidth', LineWidth{i} );
    plot(  NSig_std(i,2:length(Log_Info(:,1))), LineColorMarker_std{i}, 'LineWidth', LineWidth{i} );
    
    clear NSig_per_proc

end


% AddLegend(Number_Case);
%legend(CaseLegend{1}, CaseLegend{2}, CaseLegend{3}, CaseLegend{4}, CaseLegend{7}, CaseLegend{10}, CaseLegend{13}, CaseLegend{8}, CaseLegend{9}, CaseLegend{10} ); 
%legend('2   cpu  mean','2   cpu  min','2   cpu  max', '4   cpu  mean','4   cpu  min','4   cpu  max', '8   cpu  mean','8   cpu  min','8   cpu  max', ...
%       '16  cpu  mean','16  cpu  min','16  cpu  max', '32  cpu  mean','32  cpu min','32  cpu  max', '64  cpu  mean','64  cpu  min','64  cpu  max');
%title(['Stochastic Coherent Adaptive Large Eddy Simulations (SCALES)' 10 'Forced Homogeneous Turbulence' 10 '\epsilon=0.43   \nu=0.09   C_f=6']);
title([Title_Part_1 10 num2str(Resolution_Title),'^3','   \epsilon=',num2str(eps),'   J_{max}=',num2str(J_max_Title),'   \nu=',num2str(nu),'   C_f=',num2str(C_f),'']);

xlabel('Iteration');                     % ylabel('mean nwlt');% ylabel('std  nwlt');% ylabel('min nwlt');% ylabel('max nwlt');
ylabel('(mean, min, max, std)  NSig_{Boundary}  /  NSig_{Internal}');

ylabel_hand = get(gca,'ylabel');
set(ylabel_hand,'fontsize',Font_Size_Label);  
xlabel_hand = get(gca,'xlabel');
set(xlabel_hand,'fontsize',Font_Size_Label);  





end
if (Plot_MissBalance_NGho) 
    
    


%%% NGho (mean,max,min)
figure ('FileName', 'NSAG.fig')
hold on;
set(gcf,'Position', [34,65,1280,662]); 

for i = 1:Number_Case     

    Log_Info = load( Case_Path_Name{i} );

    for j=2:length(Log_Info(:,1))
        length(Log_Info(:,1));
        
        for k = 1:Number_Processors{i}        
            var_number = 1 + 5 + (k-1)*(1+9);             % NWLT_Total,  [pr=0]  NWLT,  iSig,  iSig_I,  iSig_B, iGho, iGho_I, iGho_B, iGhoOnly, iGhoOnly_I, iGhoOnly_B,  [pr=1]  ...
            NSig_per_proc(k) = Log_Info(j, var_number);    
        end
        
        NSig_mean(i,j) =  mean( NSig_per_proc );
        NSig_std(i,j)  =  std(  NSig_per_proc );
        NSig_min(i,j)  =  min(  NSig_per_proc );
        NSig_max(i,j)  =  max(  NSig_per_proc );
    
        NSig_std_p(i,j)  =  NSig_mean(i,j) + NSig_std(i,j);
        NSig_std_m(i,j)  =  NSig_mean(i,j) - NSig_std(i,j);

    end
        
%     plot( Log_Info(2:end,1), Nwlt_mean(i,2:length(Log_Info(:,1))), LineColorMarker{i}, 'LineWidth', LineWidth{i} );
%     plot( Log_Info(2:end,1), Nwlt_std(i,2:length(Log_Info(:,1))), LineColorMarker{i}, 'LineWidth', LineWidth{i} );
%     plot( Log_Info(2:end,1), Nwlt_min(i,2:length(Log_Info(:,1))), LineColorMarker{i}, 'LineWidth', LineWidth{i} );
%     plot( Log_Info(2:end,1), Nwlt_max(i,2:length(Log_Info(:,1))), LineColorMarker{i}, 'LineWidth', LineWidth{i} );

    plot( NSig_mean(i,2:length(Log_Info(:,1))), LineColorMarker_avg{i}, 'LineWidth', LineWidth_avg{i} );
    plot(  NSig_min(i,2:length(Log_Info(:,1))), LineColorMarker_min{i}, 'LineWidth', LineWidth{i} );
    plot(  NSig_max(i,2:length(Log_Info(:,1))), LineColorMarker_max{i}, 'LineWidth', LineWidth{i} );
    %plot(  NSig_std(i,2:length(Log_Info(:,1))), LineColorMarker_std{i}, 'LineWidth', LineWidth{i} );
    plot(  NSig_std_m(i,2:length(Log_Info(:,1))), LineColorMarker_std{i}, 'LineWidth', LineWidth{i} );
    plot(  NSig_std_p(i,2:length(Log_Info(:,1))), LineColorMarker_std{i}, 'LineWidth', LineWidth{i} );
    
    clear NSig_per_proc
    
    NSig_mean_TimeAveraged(i) = mean(NSig_mean(i,2:length(Log_Info(:,1))));
    NSig_max_TimeAveraged(i)  = mean(NSig_max(i,2:length(Log_Info(:,1))));
    NSig_min_TimeAveraged(i)  = mean(NSig_min(i,2:length(Log_Info(:,1))));

end


% AddLegend(Number_Case);
%legend(CaseLegend{1}, CaseLegend{2}, CaseLegend{3}, CaseLegend{4}, CaseLegend{7}, CaseLegend{10}, CaseLegend{13}, CaseLegend{8}, CaseLegend{9}, CaseLegend{10} ); 
%legend('2   cpu  mean','2   cpu  min','2   cpu  max', '4   cpu  mean','4   cpu  min','4   cpu  max', '8   cpu  mean','8   cpu  min','8   cpu  max', ...
%       '16  cpu  mean','16  cpu  min','16  cpu  max', '32  cpu  mean','32  cpu min','32  cpu  max', '64  cpu  mean','64  cpu  min','64  cpu  max');
%title(['Stochastic Coherent Adaptive Large Eddy Simulations (SCALES)' 10 'Forced Homogeneous Turbulence' 10 '\epsilon=0.43   \nu=0.09   C_f=6']);
%title([Title_Part_1 10 num2str(Resolution_Title),'^3','   \epsilon=',num2str(eps),'   J_{max}=',num2str(J_max_Title),'   \nu=',num2str(nu),'   C_f=',num2str(C_f),'']);

xlabel('Iteration');                     % ylabel('mean nwlt');% ylabel('std  nwlt');% ylabel('min nwlt');% ylabel('max nwlt');
ylabel('(mean, min, max, mean�std)  NGho');
ylabel('${\sf{ (mean, min, max, mean}} \hspace{-.1cm} \pm \hspace{-.1cm} {\sf{std)}} \hspace{0.2cm} {\sf{N}}_{\sf SAG}$', 'Interpreter','Latex','fontsize',Font_Size);

ylabel_hand = get(gca,'ylabel');
set(ylabel_hand,'fontsize',Font_Size_Label);  
xlabel_hand = get(gca,'xlabel');
set(xlabel_hand,'fontsize',Font_Size_Label);  

clear NSig_mean NSig_std NSig_min NSig_max NSig_std_p NSig_std_m

switch Number_Processors{Number_Case}
   case 1024
        text('String','max',           'Position',[130 604545.454545454 ],     'FontSize',13);
        text('String','mean+stdev',    'Position',[130 568181.818181818 ],     'FontSize',13);
        text('String','mean   64 cpu', 'Position',[130 517532.467532468 ],     'FontSize',13);
        text('String','mean-stdev',    'Position',[130 451298.701298701 ],     'FontSize',13);
        text('String','min',           'Position',[130 229220.779220779 ],     'FontSize',13);
       %text('String','min',           'Position',[113.214285714286 229220.779220779 ],     'FontSize',13);

        text('String','Represents Miss Balance',    'Position',[253.928571428571 515965.583173996 ],    'FontSize',13);
        annotation('doublearrow',[0.4046875 0.4046875],    [0.783489096573209 0.638629283489097]);

        text('String','128 cpu',    'Position',[228.214285714286 305831.739961759 ],    'FontSize',13,    'Color',[1 0 0]);
        text('String','256 cpu',    'Position',[373.214285714286 185372.848948375 ],    'FontSize',13,    'Color',[0 0 1]);
        text('String','512 cpu',    'Position',[561.071428571429 134512.428298279 ],    'FontSize',13,    'Color',[0 1 0]);
        text('String','1024 cpu',   'Position',[648.214285714286 56883.3652007648 ],    'FontSize',13,    'Color',[1 0 1]);
   case 2048
       switch Test_Number
           case 2048.31
        text('String','max',           'Position',[32 3275974.0259740264 ],     'FontSize',13);
        text('String','mean+stdev',    'Position',[32 3172077.9220779226 ],     'FontSize',13);
        text('String','mean   64 cpu', 'Position',[32 2808441.558441559 ],      'FontSize',13);
        text('String','mean-stdev',    'Position',[32 2425324.6753246756 ],     'FontSize',13);
        text('String','min',           'Position',[32 1100649.3506493508 ],     'FontSize',13);
       %text('String','min',           'Position',[30.2466598150051 440816.326530612 ],     'FontSize',13);

        text('String','Represents Miss Balance',    'Position',[64.14314516129032 2730519.480519481 ],    'FontSize',13);
        annotation('doublearrow',[0.4 0.4], [0.821752265861027 0.664309041797254]);

        text('String','128 cpu',    'Position',[53.5560123329908  1470779.220779221 ],    'FontSize',13,    'Color',[1 0 0]);
        text('String','256 cpu',    'Position',[98.5097636176773  788961.0389610389 ],    'FontSize',13,    'Color',[0 0 1]);
        text('String','512 cpu',    'Position',[172.322713257965  529220.7792207794 ],    'FontSize',13,    'Color',[0 1 0]);
        text('String','1024 cpu',   'Position',[170.65775950668  230519.48051948054 ],    'FontSize',13,    'Color',[1 0 1]);
        text('String','2048 cpu',   'Position',[26.5467625899281 113636.36363636376 ],    'FontSize',13,    'Color','c');
           case 2048.121
        text('String','max',           'Position',[32 3275974.0259740264 ],     'FontSize',13);
        text('String','mean+stdev',    'Position',[32 3172077.9220779226 ],     'FontSize',13);
        text('String','mean   64 cpu', 'Position',[32 2808441.558441559 ],      'FontSize',13);
        text('String','mean-stdev',    'Position',[32 2425324.6753246756 ],     'FontSize',13);
        text('String','min',           'Position',[32 1100649.3506493508 ],     'FontSize',13);
       %text('String','min',           'Position',[30.2466598150051 440816.326530612 ],     'FontSize',13);

        text('String','Represents Miss Balance',    'Position',[89.0877016129032 2730519.480519481 ],    'FontSize',13);
        annotation('doublearrow',[0.4 0.4], [0.821752265861027 0.664309041797254]);

        text('String','128 cpu',    'Position',[53.5560123329908    1470779.220779221 ],    'FontSize',13,    'Color',[1 0 0]);
        text('String','256 cpu',    'Position',[98.5097636176773    788961.0389610389 ],    'FontSize',13,    'Color',[0 0 1]);
        text('String','512 cpu',    'Position',[172.322713257965    529220.7792207794 ],    'FontSize',13,    'Color',[0 1 0]);
        text('String','1024 cpu',   'Position',[232.56109979633408 230519.48051948054 ],    'FontSize',13,    'Color',[1 0 1]);
        text('String','2048 cpu',   'Position',[202.77494908350312 113636.36363636376 ],    'FontSize',13,    'Color','c');
       end
end









figure('FileName', 'NGho_Ncpu.fig')
hold on;
i_start=1;
    
for i = i_start:Number_Case  % ii:ii+2
    plot( Number_Processors{i}, NSig_mean_TimeAveraged(i), LineColorMarker_avg_TimeAverage{i}, 'LineWidth', LineWidth_avg{i} );
    plot( Number_Processors{i},  NSig_min_TimeAveraged(i), LineColorMarker_min_TimeAverage{i}, 'LineWidth', LineWidth{i} );
    plot( Number_Processors{i},  NSig_max_TimeAveraged(i), LineColorMarker_max_TimeAverage{i}, 'LineWidth', LineWidth{i} );
end

% legend('mean', 'min', 'max', 'Location','NorthEast', 'FontSize',Font_Size-4);


switch Number_Processors{Number_Case}
   case 64
       set(gca,'XTick',[2 4 8 16 32 64])
       %set(gca,'YTick',[2 4 8 16 32 64])
   case 128
       set(gca,'XTick',[2 4 8 16 32 64 128])
       %set(gca,'YTick',[2 4 8 16 32 64 128])
   case 256
       set(gca,'XTick',[2 4 8 16 32 64 128 256])
       %set(gca,'YTick',[2 4 8 16 32 64 128 256])
   case 512
       set(gca,'XTick',[2 4 8 16 32 64 128 256 512])
       %set(gca,'YTick',[2 4 8 16 32 64 128 256 512])
   case 1024
       set(gca,'XTick',[2 4 8 16 32 64 128 256 512 1024])
       set(gca,'XTick',[64 128 256 512 1024])
       %set(gca,'YTick',[2 4 8 16 32 64 128 256 512 1024])
   case 2048
       set(gca,'XTick',[2 4 8 16 32 64 128 256 512 1024 2048])
       %set(gca,'YTick',[2 4 8 16 32 64 128 256 512 1024 2048])
       set(gca,'XTick',[64 128 256 512 1024 2048])
       %set(gca,'YTick',[64 128 256 512 1024 2048])
end

box(gca,'on');

xlim([0 Number_Processors{Number_Case}+50]);
% ylim([0 Number_Processors{Number_Case}+10]);

%set(gca,'XTickLabel',{'2', '4', '8', '16', '32', '64', '128'})

ylabel('$\overline{<\sf{NGho}>}$','interpreter','latex');
%ylabel('Speed Up      based on      CPU_T_i_m_e   for    Time Integration');
ylabel('$\overline{{\sf{min}}({\sf{N}}_{\sf SAG})}, \hspace{0.2cm} \overline{ \langle {\sf{N}}_{\sf SAG} \rangle}, \hspace{0.2cm} \overline{{\sf{max}}({\sf{N}}_{\sf SAG})}$', 'Interpreter','Latex','fontsize',Font_Size);


xlabel('Number of Processors');

%title([Title_Part_1 10 num2str(Resolution_Title),'^3','   \epsilon=',num2str(eps),'   J_{max}=',num2str(J_max_Title),'   \nu=',num2str(nu),'   C_f=',num2str(C_f),'']);

ylabel_hand = get(gca,'ylabel');
set(ylabel_hand,'fontsize',Font_Size);  
xlabel_hand = get(gca,'xlabel');
set(xlabel_hand,'fontsize',Font_Size_Label);  

clear NSig_mean_TimeAveraged NSig_min_TimeAveraged NSig_max_TimeAveraged







    
    

%%% NGho_I (mean,max,min)
figure ('FileName', 'NGho_I.fig')
hold on;

for i = 1:Number_Case     

    Log_Info = load( Case_Path_Name{i} );

    for j=2:length(Log_Info(:,1))
        length(Log_Info(:,1));
        
        for k = 1:Number_Processors{i}        
            var_number = 1 + 6 + (k-1)*(1+9);             % NWLT_Total,  [pr=0]  NWLT,  iSig,  iSig_I,  iSig_B, iGho, iGho_I, iGho_B, iGhoOnly, iGhoOnly_I, iGhoOnly_B,  [pr=1]  ...
            NSig_per_proc(k) = Log_Info(j, var_number);    
        end
        
        NSig_mean(i,j) =  mean( NSig_per_proc );
        NSig_std(i,j)  =  std(  NSig_per_proc );
        NSig_min(i,j)  =  min(  NSig_per_proc );
        NSig_max(i,j)  =  max(  NSig_per_proc );
    
        NSig_std_p(i,j)  =  NSig_mean(i,j) + NSig_std(i,j);
        NSig_std_m(i,j)  =  NSig_mean(i,j) - NSig_std(i,j);
    
    end
        
%     plot( Log_Info(2:end,1), Nwlt_mean(i,2:length(Log_Info(:,1))), LineColorMarker{i}, 'LineWidth', LineWidth{i} );
%     plot( Log_Info(2:end,1), Nwlt_std(i,2:length(Log_Info(:,1))), LineColorMarker{i}, 'LineWidth', LineWidth{i} );
%     plot( Log_Info(2:end,1), Nwlt_min(i,2:length(Log_Info(:,1))), LineColorMarker{i}, 'LineWidth', LineWidth{i} );
%     plot( Log_Info(2:end,1), Nwlt_max(i,2:length(Log_Info(:,1))), LineColorMarker{i}, 'LineWidth', LineWidth{i} );

    plot( NSig_mean(i,2:length(Log_Info(:,1))), LineColorMarker_avg{i}, 'LineWidth', LineWidth_avg{i} );
    plot(  NSig_min(i,2:length(Log_Info(:,1))), LineColorMarker_min{i}, 'LineWidth', LineWidth{i} );
    plot(  NSig_max(i,2:length(Log_Info(:,1))), LineColorMarker_max{i}, 'LineWidth', LineWidth{i} );
    %plot(  NSig_std(i,2:length(Log_Info(:,1))), LineColorMarker_std{i}, 'LineWidth', LineWidth{i} );
    plot(  NSig_std_m(i,2:length(Log_Info(:,1))), LineColorMarker_std{i}, 'LineWidth', LineWidth{i} );
    plot(  NSig_std_p(i,2:length(Log_Info(:,1))), LineColorMarker_std{i}, 'LineWidth', LineWidth{i} );
    
    clear NSig_per_proc

end


% AddLegend(Number_Case);
%legend(CaseLegend{1}, CaseLegend{2}, CaseLegend{3}, CaseLegend{4}, CaseLegend{7}, CaseLegend{10}, CaseLegend{13}, CaseLegend{8}, CaseLegend{9}, CaseLegend{10} ); 
%legend('2   cpu  mean','2   cpu  min','2   cpu  max', '4   cpu  mean','4   cpu  min','4   cpu  max', '8   cpu  mean','8   cpu  min','8   cpu  max', ...
%       '16  cpu  mean','16  cpu  min','16  cpu  max', '32  cpu  mean','32  cpu min','32  cpu  max', '64  cpu  mean','64  cpu  min','64  cpu  max');
%title(['Stochastic Coherent Adaptive Large Eddy Simulations (SCALES)' 10 'Forced Homogeneous Turbulence' 10 '\epsilon=0.43   \nu=0.09   C_f=6']);
title([Title_Part_1 10 num2str(Resolution_Title),'^3','   \epsilon=',num2str(eps),'   J_{max}=',num2str(J_max_Title),'   \nu=',num2str(nu),'   C_f=',num2str(C_f),'']);

xlabel('Iteration');                     % ylabel('mean nwlt');% ylabel('std  nwlt');% ylabel('min nwlt');% ylabel('max nwlt');
ylabel('(mean, min, max, mean�std)  NGho_{Internal}');

ylabel_hand = get(gca,'ylabel');
set(ylabel_hand,'fontsize',Font_Size_Label);  
xlabel_hand = get(gca,'xlabel');
set(xlabel_hand,'fontsize',Font_Size_Label);  

clear NSig_mean NSig_std NSig_min NSig_max NSig_std_p NSig_std_m










%%% NGho_B (mean,max,min)
figure ('FileName', 'NGho_B.fig')
hold on;

for i = 1:Number_Case     

    Log_Info = load( Case_Path_Name{i} );

    for j=2:length(Log_Info(:,1))
        length(Log_Info(:,1));
        
        for k = 1:Number_Processors{i}        
            var_number = 1 + 7 + (k-1)*(1+9);             % NWLT_Total,  [pr=0]  NWLT,  iSig,  iSig_I,  iSig_B, iGho, iGho_I, iGho_B, iGhoOnly, iGhoOnly_I, iGhoOnly_B,  [pr=1]  ...
            NSig_per_proc(k) = Log_Info(j, var_number);    
        end
        
        NSig_mean(i,j) =  mean( NSig_per_proc );
        NSig_std(i,j)  =  std(  NSig_per_proc );
        NSig_min(i,j)  =  min(  NSig_per_proc );
        NSig_max(i,j)  =  max(  NSig_per_proc );
    
        NSig_std_p(i,j)  =  NSig_mean(i,j) + NSig_std(i,j);
        NSig_std_m(i,j)  =  NSig_mean(i,j) - NSig_std(i,j);
    
    end
        
%     plot( Log_Info(2:end,1), Nwlt_mean(i,2:length(Log_Info(:,1))), LineColorMarker{i}, 'LineWidth', LineWidth{i} );
%     plot( Log_Info(2:end,1), Nwlt_std(i,2:length(Log_Info(:,1))), LineColorMarker{i}, 'LineWidth', LineWidth{i} );
%     plot( Log_Info(2:end,1), Nwlt_min(i,2:length(Log_Info(:,1))), LineColorMarker{i}, 'LineWidth', LineWidth{i} );
%     plot( Log_Info(2:end,1), Nwlt_max(i,2:length(Log_Info(:,1))), LineColorMarker{i}, 'LineWidth', LineWidth{i} );

    plot( NSig_mean(i,2:length(Log_Info(:,1))), LineColorMarker_avg{i}, 'LineWidth', LineWidth_avg{i} );
    plot(  NSig_min(i,2:length(Log_Info(:,1))), LineColorMarker_min{i}, 'LineWidth', LineWidth{i} );
    plot(  NSig_max(i,2:length(Log_Info(:,1))), LineColorMarker_max{i}, 'LineWidth', LineWidth{i} );
    %plot(  NSig_std(i,2:length(Log_Info(:,1))), LineColorMarker_std{i}, 'LineWidth', LineWidth{i} );
    plot(  NSig_std_m(i,2:length(Log_Info(:,1))), LineColorMarker_std{i}, 'LineWidth', LineWidth{i} );
    plot(  NSig_std_p(i,2:length(Log_Info(:,1))), LineColorMarker_std{i}, 'LineWidth', LineWidth{i} );
    
    clear NSig_per_proc

end


% AddLegend(Number_Case);
%legend(CaseLegend{1}, CaseLegend{2}, CaseLegend{3}, CaseLegend{4}, CaseLegend{7}, CaseLegend{10}, CaseLegend{13}, CaseLegend{8}, CaseLegend{9}, CaseLegend{10} ); 
%legend('2   cpu  mean','2   cpu  min','2   cpu  max', '4   cpu  mean','4   cpu  min','4   cpu  max', '8   cpu  mean','8   cpu  min','8   cpu  max', ...
%       '16  cpu  mean','16  cpu  min','16  cpu  max', '32  cpu  mean','32  cpu min','32  cpu  max', '64  cpu  mean','64  cpu  min','64  cpu  max');
%title(['Stochastic Coherent Adaptive Large Eddy Simulations (SCALES)' 10 'Forced Homogeneous Turbulence' 10 '\epsilon=0.43   \nu=0.09   C_f=6']);
title([Title_Part_1 10 num2str(Resolution_Title),'^3','   \epsilon=',num2str(eps),'   J_{max}=',num2str(J_max_Title),'   \nu=',num2str(nu),'   C_f=',num2str(C_f),'']);

xlabel('Iteration');                     % ylabel('mean nwlt');% ylabel('std  nwlt');% ylabel('min nwlt');% ylabel('max nwlt');
ylabel('(mean, min, max, mean�std)  NGho_{Boundary}');

ylabel_hand = get(gca,'ylabel');
set(ylabel_hand,'fontsize',Font_Size_Label);  
xlabel_hand = get(gca,'xlabel');
set(xlabel_hand,'fontsize',Font_Size_Label);  

clear NSig_mean NSig_std NSig_min NSig_max NSig_std_p NSig_std_m






end
if (Plot_MissBalance) 

    




%%% NGho_B / NGho_I (mean,max,min)
figure ('FileName', 'NGho_B__NGho_I.fig')
hold on;

for i = 1:Number_Case     

    Log_Info = load( Case_Path_Name{i} );

    for j=2:length(Log_Info(:,1))
        length(Log_Info(:,1));
        
        for k = 1:Number_Processors{i}        
            var_number_I = 1 + 6 + (k-1)*(1+9);             % NWLT_Total,  [pr=0]  NWLT,  iSig,  iSig_I,  iSig_B, iGho, iGho_I, iGho_B, iGhoOnly, iGhoOnly_I, iGhoOnly_B,  [pr=1]  ...
            var_number_B = 1 + 7 + (k-1)*(1+9);             % NWLT_Total,  [pr=0]  NWLT,  iSig,  iSig_I,  iSig_B, iGho, iGho_I, iGho_B, iGhoOnly, iGhoOnly_I, iGhoOnly_B,  [pr=1]  ...
            NSig_per_proc(k) = Log_Info(j, var_number_B) / Log_Info(j, var_number_I);    
        end
        
        NSig_mean(i,j) =  mean( NSig_per_proc );
        NSig_std(i,j)  =  std(  NSig_per_proc );
        NSig_min(i,j)  =  min(  NSig_per_proc );
        NSig_max(i,j)  =  max(  NSig_per_proc );
    
        NSig_std_p(i,j)  =  NSig_mean(i,j) + NSig_std(i,j);
        NSig_std_m(i,j)  =  NSig_mean(i,j) - NSig_std(i,j);
    
    end
        
%     plot( Log_Info(2:end,1), Nwlt_mean(i,2:length(Log_Info(:,1))), LineColorMarker{i}, 'LineWidth', LineWidth{i} );
%     plot( Log_Info(2:end,1), Nwlt_std(i,2:length(Log_Info(:,1))), LineColorMarker{i}, 'LineWidth', LineWidth{i} );
%     plot( Log_Info(2:end,1), Nwlt_min(i,2:length(Log_Info(:,1))), LineColorMarker{i}, 'LineWidth', LineWidth{i} );
%     plot( Log_Info(2:end,1), Nwlt_max(i,2:length(Log_Info(:,1))), LineColorMarker{i}, 'LineWidth', LineWidth{i} );

    plot( NSig_mean(i,2:length(Log_Info(:,1))), LineColorMarker_avg{i}, 'LineWidth', LineWidth_avg{i} );
    plot(  NSig_min(i,2:length(Log_Info(:,1))), LineColorMarker_min{i}, 'LineWidth', LineWidth{i} );
    plot(  NSig_max(i,2:length(Log_Info(:,1))), LineColorMarker_max{i}, 'LineWidth', LineWidth{i} );
    %plot(  NSig_std(i,2:length(Log_Info(:,1))), LineColorMarker_std{i}, 'LineWidth', LineWidth{i} );
    plot(  NSig_std_m(i,2:length(Log_Info(:,1))), LineColorMarker_std{i}, 'LineWidth', LineWidth{i} );
    plot(  NSig_std_p(i,2:length(Log_Info(:,1))), LineColorMarker_std{i}, 'LineWidth', LineWidth{i} );
    
    clear NSig_per_proc

end


% AddLegend(Number_Case);
%legend(CaseLegend{1}, CaseLegend{2}, CaseLegend{3}, CaseLegend{4}, CaseLegend{7}, CaseLegend{10}, CaseLegend{13}, CaseLegend{8}, CaseLegend{9}, CaseLegend{10} ); 
%legend('2   cpu  mean','2   cpu  min','2   cpu  max', '4   cpu  mean','4   cpu  min','4   cpu  max', '8   cpu  mean','8   cpu  min','8   cpu  max', ...
%       '16  cpu  mean','16  cpu  min','16  cpu  max', '32  cpu  mean','32  cpu min','32  cpu  max', '64  cpu  mean','64  cpu  min','64  cpu  max');
%title(['Stochastic Coherent Adaptive Large Eddy Simulations (SCALES)' 10 'Forced Homogeneous Turbulence' 10 '\epsilon=0.43   \nu=0.09   C_f=6']);
title([Title_Part_1 10 num2str(Resolution_Title),'^3','   \epsilon=',num2str(eps),'   J_{max}=',num2str(J_max_Title),'   \nu=',num2str(nu),'   C_f=',num2str(C_f),'']);

xlabel('Iteration');                     % ylabel('mean nwlt');% ylabel('std  nwlt');% ylabel('min nwlt');% ylabel('max nwlt');
ylabel('(mean, min, max, mean�std)  NGho_{Boundary}  /  NGho_{Internal}');

ylabel_hand = get(gca,'ylabel');
set(ylabel_hand,'fontsize',Font_Size_Label);  
xlabel_hand = get(gca,'xlabel');
set(xlabel_hand,'fontsize',Font_Size_Label);  








%%%  NGho_I / NSig_I (mean,max,min,std)
figure ('FileName', 'NGho_I__NSig_I.fig')
hold on;

for i = 1:Number_Case     

    Log_Info = load( Case_Path_Name{i} );

    for j=2:length(Log_Info(:,1))
        length(Log_Info(:,1));
        
        for k = 1:Number_Processors{i}        
            var_number_I = 1 + 3 + (k-1)*(1+9);             % NWLT_Total,  [pr=0]  NWLT,  iSig,  iSig_I,  iSig_B, iGho, iGho_I, iGho_B, iGhoOnly, iGhoOnly_I, iGhoOnly_B,  [pr=1]  ...
            var_number_B = 1 + 6 + (k-1)*(1+9);             % NWLT_Total,  [pr=0]  NWLT,  iSig,  iSig_I,  iSig_B, iGho, iGho_I, iGho_B, iGhoOnly, iGhoOnly_I, iGhoOnly_B,  [pr=1]  ...
            NSig_per_proc(k) = Log_Info(j, var_number_B) / Log_Info(j, var_number_I);    
        end
        
        NSig_mean(i,j) =  mean( NSig_per_proc );
        NSig_std(i,j)  =  std(  NSig_per_proc );
        NSig_min(i,j)  =  min(  NSig_per_proc );
        NSig_max(i,j)  =  max(  NSig_per_proc );
    
    end
        
%     plot( Log_Info(2:end,1), Nwlt_mean(i,2:length(Log_Info(:,1))), LineColorMarker{i}, 'LineWidth', LineWidth{i} );
%     plot( Log_Info(2:end,1), Nwlt_std(i,2:length(Log_Info(:,1))), LineColorMarker{i}, 'LineWidth', LineWidth{i} );
%     plot( Log_Info(2:end,1), Nwlt_min(i,2:length(Log_Info(:,1))), LineColorMarker{i}, 'LineWidth', LineWidth{i} );
%     plot( Log_Info(2:end,1), Nwlt_max(i,2:length(Log_Info(:,1))), LineColorMarker{i}, 'LineWidth', LineWidth{i} );

    plot( NSig_mean(i,2:length(Log_Info(:,1))), LineColorMarker_avg{i}, 'LineWidth', LineWidth_avg{i} );
    plot(  NSig_min(i,2:length(Log_Info(:,1))), LineColorMarker_min{i}, 'LineWidth', LineWidth{i} );
    plot(  NSig_max(i,2:length(Log_Info(:,1))), LineColorMarker_max{i}, 'LineWidth', LineWidth{i} );
    plot(  NSig_std(i,2:length(Log_Info(:,1))), LineColorMarker_std{i}, 'LineWidth', LineWidth{i} );
    
    clear NSig_per_proc

end
clear NSig_mean NSig_std NSig_min NSig_max

% AddLegend(Number_Case);
%legend(CaseLegend{1}, CaseLegend{2}, CaseLegend{3}, CaseLegend{4}, CaseLegend{7}, CaseLegend{10}, CaseLegend{13}, CaseLegend{8}, CaseLegend{9}, CaseLegend{10} ); 
%legend('2   cpu  mean','2   cpu  min','2   cpu  max', '4   cpu  mean','4   cpu  min','4   cpu  max', '8   cpu  mean','8   cpu  min','8   cpu  max', ...
%       '16  cpu  mean','16  cpu  min','16  cpu  max', '32  cpu  mean','32  cpu min','32  cpu  max', '64  cpu  mean','64  cpu  min','64  cpu  max');
%title(['Stochastic Coherent Adaptive Large Eddy Simulations (SCALES)' 10 'Forced Homogeneous Turbulence' 10 '\epsilon=0.43   \nu=0.09   C_f=6']);
title([Title_Part_1 10 num2str(Resolution_Title),'^3','   \epsilon=',num2str(eps),'   J_{max}=',num2str(J_max_Title),'   \nu=',num2str(nu),'   C_f=',num2str(C_f),'']);

xlabel('Iteration');                     % ylabel('mean nwlt');% ylabel('std  nwlt');% ylabel('min nwlt');% ylabel('max nwlt');
ylabel('(mean, min, max, std)  NGho_{Internal}  /  NSig_{Internal}');

ylabel_hand = get(gca,'ylabel');
set(ylabel_hand,'fontsize',Font_Size_Label);  
xlabel_hand = get(gca,'xlabel');
set(xlabel_hand,'fontsize',Font_Size_Label);  










%%%  NGho_B / NSig_I (mean,max,min,std)
figure ('FileName', 'NGho_B__NSig_I.fig')
hold on;

for i = 1:Number_Case     

    Log_Info = load( Case_Path_Name{i} );

    for j=2:length(Log_Info(:,1))
        length(Log_Info(:,1));
        
        for k = 1:Number_Processors{i}        
            var_number_I = 1 + 3 + (k-1)*(1+9);             % NWLT_Total,  [pr=0]  NWLT,  iSig,  iSig_I,  iSig_B, iGho, iGho_I, iGho_B, iGhoOnly, iGhoOnly_I, iGhoOnly_B,  [pr=1]  ...
            var_number_B = 1 + 7 + (k-1)*(1+9);             % NWLT_Total,  [pr=0]  NWLT,  iSig,  iSig_I,  iSig_B, iGho, iGho_I, iGho_B, iGhoOnly, iGhoOnly_I, iGhoOnly_B,  [pr=1]  ...
            NSig_per_proc(k) = Log_Info(j, var_number_B) / Log_Info(j, var_number_I);    
        end
        
        NSig_mean(i,j) =  mean( NSig_per_proc );
        NSig_std(i,j)  =  std(  NSig_per_proc );
        NSig_min(i,j)  =  min(  NSig_per_proc );
        NSig_max(i,j)  =  max(  NSig_per_proc );
    
    end
        
%     plot( Log_Info(2:end,1), Nwlt_mean(i,2:length(Log_Info(:,1))), LineColorMarker{i}, 'LineWidth', LineWidth{i} );
%     plot( Log_Info(2:end,1), Nwlt_std(i,2:length(Log_Info(:,1))), LineColorMarker{i}, 'LineWidth', LineWidth{i} );
%     plot( Log_Info(2:end,1), Nwlt_min(i,2:length(Log_Info(:,1))), LineColorMarker{i}, 'LineWidth', LineWidth{i} );
%     plot( Log_Info(2:end,1), Nwlt_max(i,2:length(Log_Info(:,1))), LineColorMarker{i}, 'LineWidth', LineWidth{i} );

    plot( NSig_mean(i,2:length(Log_Info(:,1))), LineColorMarker_avg{i}, 'LineWidth', LineWidth_avg{i} );
    plot(  NSig_min(i,2:length(Log_Info(:,1))), LineColorMarker_min{i}, 'LineWidth', LineWidth{i} );
    plot(  NSig_max(i,2:length(Log_Info(:,1))), LineColorMarker_max{i}, 'LineWidth', LineWidth{i} );
    plot(  NSig_std(i,2:length(Log_Info(:,1))), LineColorMarker_std{i}, 'LineWidth', LineWidth{i} );
    
    clear NSig_per_proc

end
clear NSig_mean NSig_std NSig_min NSig_max

% AddLegend(Number_Case);
%legend(CaseLegend{1}, CaseLegend{2}, CaseLegend{3}, CaseLegend{4}, CaseLegend{7}, CaseLegend{10}, CaseLegend{13}, CaseLegend{8}, CaseLegend{9}, CaseLegend{10} ); 
%legend('2   cpu  mean','2   cpu  min','2   cpu  max', '4   cpu  mean','4   cpu  min','4   cpu  max', '8   cpu  mean','8   cpu  min','8   cpu  max', ...
%       '16  cpu  mean','16  cpu  min','16  cpu  max', '32  cpu  mean','32  cpu min','32  cpu  max', '64  cpu  mean','64  cpu  min','64  cpu  max');
%title(['Stochastic Coherent Adaptive Large Eddy Simulations (SCALES)' 10 'Forced Homogeneous Turbulence' 10 '\epsilon=0.43   \nu=0.09   C_f=6']);
title([Title_Part_1 10 num2str(Resolution_Title),'^3','   \epsilon=',num2str(eps),'   J_{max}=',num2str(J_max_Title),'   \nu=',num2str(nu),'   C_f=',num2str(C_f),'']);

xlabel('Iteration');                     % ylabel('mean nwlt');% ylabel('std  nwlt');% ylabel('min nwlt');% ylabel('max nwlt');
ylabel('(mean, min, max, std)  NGho_{Boundary}  /  NSig_{Internal}');

ylabel_hand = get(gca,'ylabel');
set(ylabel_hand,'fontsize',Font_Size_Label);  
xlabel_hand = get(gca,'xlabel');
set(xlabel_hand,'fontsize',Font_Size_Label);  










%%%  NSig_B / NSig_I (mean,max,min,std)
figure ('FileName', 'NSig_B__NSig_I.fig')
hold on;

for i = 1:Number_Case     

    Log_Info = load( Case_Path_Name{i} );

    for j=2:length(Log_Info(:,1))
        length(Log_Info(:,1));
        
        for k = 1:Number_Processors{i}        
            var_number_I = 1 + 3 + (k-1)*(1+9);             % NWLT_Total,  [pr=0]  NWLT,  iSig,  iSig_I,  iSig_B, iGho, iGho_I, iGho_B, iGhoOnly, iGhoOnly_I, iGhoOnly_B,  [pr=1]  ...
            var_number_B = 1 + 4 + (k-1)*(1+9);             % NWLT_Total,  [pr=0]  NWLT,  iSig,  iSig_I,  iSig_B, iGho, iGho_I, iGho_B, iGhoOnly, iGhoOnly_I, iGhoOnly_B,  [pr=1]  ...
            NSig_per_proc(k) = Log_Info(j, var_number_B) / Log_Info(j, var_number_I);    
        end
        
        NSig_mean(i,j) =  mean( NSig_per_proc );
        NSig_std(i,j)  =  std(  NSig_per_proc );
        NSig_min(i,j)  =  min(  NSig_per_proc );
        NSig_max(i,j)  =  max(  NSig_per_proc );
    
    end
        
%     plot( Log_Info(2:end,1), Nwlt_mean(i,2:length(Log_Info(:,1))), LineColorMarker{i}, 'LineWidth', LineWidth{i} );
%     plot( Log_Info(2:end,1), Nwlt_std(i,2:length(Log_Info(:,1))), LineColorMarker{i}, 'LineWidth', LineWidth{i} );
%     plot( Log_Info(2:end,1), Nwlt_min(i,2:length(Log_Info(:,1))), LineColorMarker{i}, 'LineWidth', LineWidth{i} );
%     plot( Log_Info(2:end,1), Nwlt_max(i,2:length(Log_Info(:,1))), LineColorMarker{i}, 'LineWidth', LineWidth{i} );

    plot( NSig_mean(i,2:length(Log_Info(:,1))), LineColorMarker_avg{i}, 'LineWidth', LineWidth_avg{i} );
    plot(  NSig_min(i,2:length(Log_Info(:,1))), LineColorMarker_min{i}, 'LineWidth', LineWidth{i} );
    plot(  NSig_max(i,2:length(Log_Info(:,1))), LineColorMarker_max{i}, 'LineWidth', LineWidth{i} );
    plot(  NSig_std(i,2:length(Log_Info(:,1))), LineColorMarker_std{i}, 'LineWidth', LineWidth{i} );
    
    clear NSig_per_proc

end
clear NSig_mean NSig_std NSig_min NSig_max

% AddLegend(Number_Case);
%legend(CaseLegend{1}, CaseLegend{2}, CaseLegend{3}, CaseLegend{4}, CaseLegend{7}, CaseLegend{10}, CaseLegend{13}, CaseLegend{8}, CaseLegend{9}, CaseLegend{10} ); 
%legend('2   cpu  mean','2   cpu  min','2   cpu  max', '4   cpu  mean','4   cpu  min','4   cpu  max', '8   cpu  mean','8   cpu  min','8   cpu  max', ...
%       '16  cpu  mean','16  cpu  min','16  cpu  max', '32  cpu  mean','32  cpu min','32  cpu  max', '64  cpu  mean','64  cpu  min','64  cpu  max');
%title(['Stochastic Coherent Adaptive Large Eddy Simulations (SCALES)' 10 'Forced Homogeneous Turbulence' 10 '\epsilon=0.43   \nu=0.09   C_f=6']);
title([Title_Part_1 10 num2str(Resolution_Title),'^3','   \epsilon=',num2str(eps),'   J_{max}=',num2str(J_max_Title),'   \nu=',num2str(nu),'   C_f=',num2str(C_f),'']);

xlabel('Iteration');                     % ylabel('mean nwlt');% ylabel('std  nwlt');% ylabel('min nwlt');% ylabel('max nwlt');
ylabel('(mean, min, max, std)  NSig_{Boundary}  /  NSig_{Internal}');

ylabel_hand = get(gca,'ylabel');
set(ylabel_hand,'fontsize',Font_Size_Label);  
xlabel_hand = get(gca,'xlabel');
set(xlabel_hand,'fontsize',Font_Size_Label);  










%%%   The most important one for the cost  
%%%  ( NSig_B + NGho_I + NGho_B ) / NSig_I (mean,max,min,std)
figure ('FileName', 'NSig_B+NGho_I+NGho_B__NSig_I.fig')
hold on;

for i = 1:Number_Case     

    Log_Info = load( Case_Path_Name{i} );

    for j=2:length(Log_Info(:,1))
        length(Log_Info(:,1));
        
        for k = 1:Number_Processors{i}        
            var_number_1 = 1 + 4 + (k-1)*(1+9);             % NWLT_Total,  [pr=0]  NWLT,  iSig,  iSig_I,  iSig_B, iGho, iGho_I, iGho_B, iGhoOnly, iGhoOnly_I, iGhoOnly_B,  [pr=1]  ...
            var_number_2 = 1 + 6 + (k-1)*(1+9);             % NWLT_Total,  [pr=0]  NWLT,  iSig,  iSig_I,  iSig_B, iGho, iGho_I, iGho_B, iGhoOnly, iGhoOnly_I, iGhoOnly_B,  [pr=1]  ...
            var_number_3 = 1 + 7 + (k-1)*(1+9);             % NWLT_Total,  [pr=0]  NWLT,  iSig,  iSig_I,  iSig_B, iGho, iGho_I, iGho_B, iGhoOnly, iGhoOnly_I, iGhoOnly_B,  [pr=1]  ...
            var_number_4 = 1 + 3 + (k-1)*(1+9);             % NWLT_Total,  [pr=0]  NWLT,  iSig,  iSig_I,  iSig_B, iGho, iGho_I, iGho_B, iGhoOnly, iGhoOnly_I, iGhoOnly_B,  [pr=1]  ...
            NSig_per_proc(k) = ( Log_Info(j, var_number_1) + Log_Info(j, var_number_2) + Log_Info(j, var_number_3) ) / Log_Info(j, var_number_4);    
        end
        
        NSig_mean(i,j) =  mean( NSig_per_proc );
        NSig_std(i,j)  =  std(  NSig_per_proc );
        NSig_min(i,j)  =  min(  NSig_per_proc );
        NSig_max(i,j)  =  max(  NSig_per_proc );
    
    end
        
%     plot( Log_Info(2:end,1), Nwlt_mean(i,2:length(Log_Info(:,1))), LineColorMarker{i}, 'LineWidth', LineWidth{i} );
%     plot( Log_Info(2:end,1), Nwlt_std(i,2:length(Log_Info(:,1))), LineColorMarker{i}, 'LineWidth', LineWidth{i} );
%     plot( Log_Info(2:end,1), Nwlt_min(i,2:length(Log_Info(:,1))), LineColorMarker{i}, 'LineWidth', LineWidth{i} );
%     plot( Log_Info(2:end,1), Nwlt_max(i,2:length(Log_Info(:,1))), LineColorMarker{i}, 'LineWidth', LineWidth{i} );

    plot( NSig_mean(i,2:length(Log_Info(:,1))), LineColorMarker_avg{i}, 'LineWidth', LineWidth_avg{i} );
    plot(  NSig_min(i,2:length(Log_Info(:,1))), LineColorMarker_min{i}, 'LineWidth', LineWidth{i} );
    plot(  NSig_max(i,2:length(Log_Info(:,1))), LineColorMarker_max{i}, 'LineWidth', LineWidth{i} );
    plot(  NSig_std(i,2:length(Log_Info(:,1))), LineColorMarker_std{i}, 'LineWidth', LineWidth{i} );
    
    clear NSig_per_proc

end
clear NSig_mean NSig_std NSig_min NSig_max

% AddLegend(Number_Case);
%legend(CaseLegend{1}, CaseLegend{2}, CaseLegend{3}, CaseLegend{4}, CaseLegend{7}, CaseLegend{10}, CaseLegend{13}, CaseLegend{8}, CaseLegend{9}, CaseLegend{10} ); 
%legend('2   cpu  mean','2   cpu  min','2   cpu  max', '4   cpu  mean','4   cpu  min','4   cpu  max', '8   cpu  mean','8   cpu  min','8   cpu  max', ...
%       '16  cpu  mean','16  cpu  min','16  cpu  max', '32  cpu  mean','32  cpu min','32  cpu  max', '64  cpu  mean','64  cpu  min','64  cpu  max');
%title(['Stochastic Coherent Adaptive Large Eddy Simulations (SCALES)' 10 'Forced Homogeneous Turbulence' 10 '\epsilon=0.43   \nu=0.09   C_f=6']);
title([Title_Part_1 10 num2str(Resolution_Title),'^3','   \epsilon=',num2str(eps),'   J_{max}=',num2str(J_max_Title),'   \nu=',num2str(nu),'   C_f=',num2str(C_f),'']);

xlabel('Iteration');                     % ylabel('mean nwlt');% ylabel('std  nwlt');% ylabel('min nwlt');% ylabel('max nwlt');
ylabel('(mean, min, max, std)  ( NSig_{Boundary} + NGho_{Internal} + NGho_{Boundary} ) / NSig_{Internal}');

ylabel_hand = get(gca,'ylabel');
set(ylabel_hand,'fontsize',Font_Size_Label);  
xlabel_hand = get(gca,'xlabel');
set(xlabel_hand,'fontsize',Font_Size_Label);  










%%%  The most important one for communication is
%%%  ( NSig_B + NGho_B ) (mean,max,min,std)
figure ('FileName', 'NSig_B+NGho_B.fig')
hold on;

for i = 1:Number_Case     

    Log_Info = load( Case_Path_Name{i} );

    for j=2:length(Log_Info(:,1))
        length(Log_Info(:,1));
        
        for k = 1:Number_Processors{i}        
            var_number_1 = 1 + 4 + (k-1)*(1+9);             % NWLT_Total,  [pr=0]  NWLT,  iSig,  iSig_I,  iSig_B, iGho, iGho_I, iGho_B, iGhoOnly, iGhoOnly_I, iGhoOnly_B,  [pr=1]  ...
            var_number_2 = 1 + 7 + (k-1)*(1+9);             % NWLT_Total,  [pr=0]  NWLT,  iSig,  iSig_I,  iSig_B, iGho, iGho_I, iGho_B, iGhoOnly, iGhoOnly_I, iGhoOnly_B,  [pr=1]  ...
            NSig_per_proc(k) = ( Log_Info(j, var_number_1) + Log_Info(j, var_number_2) );    
        end
        
        NSig_mean(i,j) =  mean( NSig_per_proc );
        NSig_std(i,j)  =  std(  NSig_per_proc );
        NSig_min(i,j)  =  min(  NSig_per_proc );
        NSig_max(i,j)  =  max(  NSig_per_proc );
    
    end
        
%     plot( Log_Info(2:end,1), Nwlt_mean(i,2:length(Log_Info(:,1))), LineColorMarker{i}, 'LineWidth', LineWidth{i} );
%     plot( Log_Info(2:end,1), Nwlt_std(i,2:length(Log_Info(:,1))), LineColorMarker{i}, 'LineWidth', LineWidth{i} );
%     plot( Log_Info(2:end,1), Nwlt_min(i,2:length(Log_Info(:,1))), LineColorMarker{i}, 'LineWidth', LineWidth{i} );
%     plot( Log_Info(2:end,1), Nwlt_max(i,2:length(Log_Info(:,1))), LineColorMarker{i}, 'LineWidth', LineWidth{i} );

    plot( NSig_mean(i,2:length(Log_Info(:,1))), LineColorMarker_avg{i}, 'LineWidth', LineWidth_avg{i} );
    plot(  NSig_min(i,2:length(Log_Info(:,1))), LineColorMarker_min{i}, 'LineWidth', LineWidth{i} );
    plot(  NSig_max(i,2:length(Log_Info(:,1))), LineColorMarker_max{i}, 'LineWidth', LineWidth{i} );
    plot(  NSig_std(i,2:length(Log_Info(:,1))), LineColorMarker_std{i}, 'LineWidth', LineWidth{i} );
    
    clear NSig_per_proc

end
clear NSig_mean NSig_std NSig_min NSig_max

% AddLegend(Number_Case);
%legend(CaseLegend{1}, CaseLegend{2}, CaseLegend{3}, CaseLegend{4}, CaseLegend{7}, CaseLegend{10}, CaseLegend{13}, CaseLegend{8}, CaseLegend{9}, CaseLegend{10} ); 
%legend('2   cpu  mean','2   cpu  min','2   cpu  max', '4   cpu  mean','4   cpu  min','4   cpu  max', '8   cpu  mean','8   cpu  min','8   cpu  max', ...
%       '16  cpu  mean','16  cpu  min','16  cpu  max', '32  cpu  mean','32  cpu min','32  cpu  max', '64  cpu  mean','64  cpu  min','64  cpu  max');
%title(['Stochastic Coherent Adaptive Large Eddy Simulations (SCALES)' 10 'Forced Homogeneous Turbulence' 10 '\epsilon=0.43   \nu=0.09   C_f=6']);
title([Title_Part_1 10 num2str(Resolution_Title),'^3','   \epsilon=',num2str(eps),'   J_{max}=',num2str(J_max_Title),'   \nu=',num2str(nu),'   C_f=',num2str(C_f),'']);

xlabel('Iteration');                     % ylabel('mean nwlt');% ylabel('std  nwlt');% ylabel('min nwlt');% ylabel('max nwlt');
ylabel('(mean, min, max, std)  ( NSig_{Boundary} + NGho_{Boundary} )');

ylabel_hand = get(gca,'ylabel');
set(ylabel_hand,'fontsize',Font_Size_Label);  
xlabel_hand = get(gca,'xlabel');
set(xlabel_hand,'fontsize',Font_Size_Label);  





end















if (Plot_MissBalance_Performance) 





%%%   Rgho_CPU=(<Ngho_I>+<Ngho_B>)/max(Ngho_I+Ngho_B)  or  (<Ngho_I>+<Ngho_B>)/[max(Ngho_I)+max(Ngho_B)], whatever is available
%figure ('FileName', '<Ngho_I>+<Ngho_B>___max_Ngho_I+Ngho_B.fig')
figure ('FileName', 'RGho_CPU_1.fig')
hold on;

for i = 1:Number_Case     

    Log_Info = load( Case_Path_Name{i} );

    for j=2:length(Log_Info(:,1))
        length(Log_Info(:,1));
        
        for k = 1:Number_Processors{i}        
            var_number_1 = 1 + 6 + (k-1)*(1+9);             % NWLT_Total,  [pr=0]  NWLT,  iSig,  iSig_I,  iSig_B, iGho, iGho_I, iGho_B, iGhoOnly, iGhoOnly_I, iGhoOnly_B,  [pr=1]  ...
            var_number_2 = 1 + 7 + (k-1)*(1+9);             % NWLT_Total,  [pr=0]  NWLT,  iSig,  iSig_I,  iSig_B, iGho, iGho_I, iGho_B, iGhoOnly, iGhoOnly_I, iGhoOnly_B,  [pr=1]  ...
            N_per_proc__var_number_1(k) = Log_Info(j, var_number_1);    
            N_per_proc__var_number_2(k) = Log_Info(j, var_number_2);    
        end
        
        NSig_mean(i,j) =  ( mean(N_per_proc__var_number_1) + mean(N_per_proc__var_number_2) ) / max( N_per_proc__var_number_1+N_per_proc__var_number_2 );
        %NSig_std(i,j)  =  std(   );
        %NSig_min(i,j)  =  min(   );
        %NSig_max(i,j)  =  max(   );
    
    end
        
%     plot( Log_Info(2:end,1), Nwlt_mean(i,2:length(Log_Info(:,1))), LineColorMarker{i}, 'LineWidth', LineWidth{i} );
%     plot( Log_Info(2:end,1), Nwlt_std(i,2:length(Log_Info(:,1))), LineColorMarker{i}, 'LineWidth', LineWidth{i} );
%     plot( Log_Info(2:end,1), Nwlt_min(i,2:length(Log_Info(:,1))), LineColorMarker{i}, 'LineWidth', LineWidth{i} );
%     plot( Log_Info(2:end,1), Nwlt_max(i,2:length(Log_Info(:,1))), LineColorMarker{i}, 'LineWidth', LineWidth{i} );

    plot( NSig_mean(i,2:length(Log_Info(:,1))), LineColorMarker_avg{i}, 'LineWidth', LineWidth{i} );
    %plot(  NSig_min(i,2:length(Log_Info(:,1))), LineColorMarker_min{i}, 'LineWidth', LineWidth{i} );
    %plot(  NSig_max(i,2:length(Log_Info(:,1))), LineColorMarker_max{i}, 'LineWidth', LineWidth{i} );
    %plot(  NSig_std(i,2:length(Log_Info(:,1))), LineColorMarker_std{i}, 'LineWidth', LineWidth{i} );
    
    clear N_per_proc__var_number_1 N_per_proc__var_number_2

end
clear NSig_mean NSig_std NSig_min NSig_max

% AddLegend(Number_Case);
%legend(CaseLegend{1}, CaseLegend{2}, CaseLegend{3}, CaseLegend{4}, CaseLegend{7}, CaseLegend{10}, CaseLegend{13}, CaseLegend{8}, CaseLegend{9}, CaseLegend{10} ); 
%legend('2   cpu  mean','2   cpu  min','2   cpu  max', '4   cpu  mean','4   cpu  min','4   cpu  max', '8   cpu  mean','8   cpu  min','8   cpu  max', ...
%       '16  cpu  mean','16  cpu  min','16  cpu  max', '32  cpu  mean','32  cpu min','32  cpu  max', '64  cpu  mean','64  cpu  min','64  cpu  max');
%title(['Stochastic Coherent Adaptive Large Eddy Simulations (SCALES)' 10 'Forced Homogeneous Turbulence' 10 '\epsilon=0.43   \nu=0.09   C_f=6']);
title([Title_Part_1 10 num2str(Resolution_Title),'^3','   \epsilon=',num2str(eps),'   J_{max}=',num2str(J_max_Title),'   \nu=',num2str(nu),'   C_f=',num2str(C_f),'']);

xlabel('Iteration');                     % ylabel('mean nwlt');% ylabel('std  nwlt');% ylabel('min nwlt');% ylabel('max nwlt');
ylabel('(mean, min, max, std)  Rgho_CPU=(<Ngho_I>+<Ngho_B>)/max(Ngho_I+Ngho_B)');
ylabel('RGho_{CPU}=(<NGho_I>+<NGho_B>)/max(NGho_I+NGho_B)');

ylabel_hand = get(gca,'ylabel');
set(ylabel_hand,'fontsize',Font_Size_Label);  
xlabel_hand = get(gca,'xlabel');
set(xlabel_hand,'fontsize',Font_Size_Label);  










%%%   Rgho_CPU=(<Ngho_I>+<Ngho_B>)/max(Ngho_I+Ngho_B)  or  (<Ngho_I>+<Ngho_B>)/[max(Ngho_I)+max(Ngho_B)], whatever is available
figure ('FileName', 'RGho_CPU_2.fig')
hold on;

for i = 1:Number_Case     

    Log_Info = load( Case_Path_Name{i} );

    for j=2:length(Log_Info(:,1))
        length(Log_Info(:,1));
        
        for k = 1:Number_Processors{i}        
            var_number_1 = 1 + 6 + (k-1)*(1+9);             % NWLT_Total,  [pr=0]  NWLT,  iSig,  iSig_I,  iSig_B, iGho, iGho_I, iGho_B, iGhoOnly, iGhoOnly_I, iGhoOnly_B,  [pr=1]  ...
            var_number_2 = 1 + 7 + (k-1)*(1+9);             % NWLT_Total,  [pr=0]  NWLT,  iSig,  iSig_I,  iSig_B, iGho, iGho_I, iGho_B, iGhoOnly, iGhoOnly_I, iGhoOnly_B,  [pr=1]  ...
            N_per_proc__var_number_1(k) = Log_Info(j, var_number_1);    
            N_per_proc__var_number_2(k) = Log_Info(j, var_number_2);    
        end
        
        NSig_mean(i,j) =  ( mean(N_per_proc__var_number_1) + mean(N_per_proc__var_number_2) ) / ( max(N_per_proc__var_number_1) + max(N_per_proc__var_number_2) );
        %NSig_std(i,j)  =  std(   );
        %NSig_min(i,j)  =  min(   );
        %NSig_max(i,j)  =  max(   );
    
    end
        
%     plot( Log_Info(2:end,1), Nwlt_mean(i,2:length(Log_Info(:,1))), LineColorMarker{i}, 'LineWidth', LineWidth{i} );
%     plot( Log_Info(2:end,1), Nwlt_std(i,2:length(Log_Info(:,1))), LineColorMarker{i}, 'LineWidth', LineWidth{i} );
%     plot( Log_Info(2:end,1), Nwlt_min(i,2:length(Log_Info(:,1))), LineColorMarker{i}, 'LineWidth', LineWidth{i} );
%     plot( Log_Info(2:end,1), Nwlt_max(i,2:length(Log_Info(:,1))), LineColorMarker{i}, 'LineWidth', LineWidth{i} );

    plot( NSig_mean(i,2:length(Log_Info(:,1))), LineColorMarker_avg{i}, 'LineWidth', LineWidth{i} );
    %plot(  NSig_min(i,2:length(Log_Info(:,1))), LineColorMarker_min{i}, 'LineWidth', LineWidth{i} );
    %plot(  NSig_max(i,2:length(Log_Info(:,1))), LineColorMarker_max{i}, 'LineWidth', LineWidth{i} );
    %plot(  NSig_std(i,2:length(Log_Info(:,1))), LineColorMarker_std{i}, 'LineWidth', LineWidth{i} );
    
    clear N_per_proc__var_number_1 N_per_proc__var_number_2

end
clear NSig_mean NSig_std NSig_min NSig_max

% AddLegend(Number_Case);
%legend(CaseLegend{1}, CaseLegend{2}, CaseLegend{3}, CaseLegend{4}, CaseLegend{7}, CaseLegend{10}, CaseLegend{13}, CaseLegend{8}, CaseLegend{9}, CaseLegend{10} ); 
%legend('2   cpu  mean','2   cpu  min','2   cpu  max', '4   cpu  mean','4   cpu  min','4   cpu  max', '8   cpu  mean','8   cpu  min','8   cpu  max', ...
%       '16  cpu  mean','16  cpu  min','16  cpu  max', '32  cpu  mean','32  cpu min','32  cpu  max', '64  cpu  mean','64  cpu  min','64  cpu  max');
%title(['Stochastic Coherent Adaptive Large Eddy Simulations (SCALES)' 10 'Forced Homogeneous Turbulence' 10 '\epsilon=0.43   \nu=0.09   C_f=6']);
title([Title_Part_1 10 num2str(Resolution_Title),'^3','   \epsilon=',num2str(eps),'   J_{max}=',num2str(J_max_Title),'   \nu=',num2str(nu),'   C_f=',num2str(C_f),'']);

xlabel('Iteration');                     % ylabel('mean nwlt');% ylabel('std  nwlt');% ylabel('min nwlt');% ylabel('max nwlt');
ylabel('(mean, min, max, std)  RGho_{CPU}=(<NGho_I>+<NGho_B>)/[max(NGho_I)+max(NGho_B)]');
ylabel('RGho_{CPU}=(<NGho_I>+<NGho_B>)/[max(NGho_I)+max(NGho_B)]');

ylabel_hand = get(gca,'ylabel');
set(ylabel_hand,'fontsize',Font_Size_Label);  
xlabel_hand = get(gca,'xlabel');
set(xlabel_hand,'fontsize',Font_Size_Label);  











%%%   Parallel efficiency in terms of overhead related to parallel processor:  Rgho_N=<Ngho_I>/[<Ngho_I>+<Ngho_B>]
figure ('FileName', 'RGho_N.fig')
hold on;

for i = 1:Number_Case     

    Log_Info = load( Case_Path_Name{i} );

    for j=2:length(Log_Info(:,1))
        length(Log_Info(:,1));
        
        for k = 1:Number_Processors{i}        
            var_number_1 = 1 + 6 + (k-1)*(1+9);             % NWLT_Total,  [pr=0]  NWLT,  iSig,  iSig_I,  iSig_B, iGho, iGho_I, iGho_B, iGhoOnly, iGhoOnly_I, iGhoOnly_B,  [pr=1]  ...
            var_number_2 = 1 + 7 + (k-1)*(1+9);             % NWLT_Total,  [pr=0]  NWLT,  iSig,  iSig_I,  iSig_B, iGho, iGho_I, iGho_B, iGhoOnly, iGhoOnly_I, iGhoOnly_B,  [pr=1]  ...
            N_per_proc__var_number_1(k) = Log_Info(j, var_number_1);    
            N_per_proc__var_number_2(k) = Log_Info(j, var_number_2);    
        end
        
        NSig_mean(i,j) =  ( mean(N_per_proc__var_number_1) ) / ( mean(N_per_proc__var_number_1) + mean(N_per_proc__var_number_2) );
        %NSig_std(i,j)  =  std(   );
        %NSig_min(i,j)  =  min(   );
        %NSig_max(i,j)  =  max(   );
    
    end
        
%     plot( Log_Info(2:end,1), Nwlt_mean(i,2:length(Log_Info(:,1))), LineColorMarker{i}, 'LineWidth', LineWidth{i} );
%     plot( Log_Info(2:end,1), Nwlt_std(i,2:length(Log_Info(:,1))), LineColorMarker{i}, 'LineWidth', LineWidth{i} );
%     plot( Log_Info(2:end,1), Nwlt_min(i,2:length(Log_Info(:,1))), LineColorMarker{i}, 'LineWidth', LineWidth{i} );
%     plot( Log_Info(2:end,1), Nwlt_max(i,2:length(Log_Info(:,1))), LineColorMarker{i}, 'LineWidth', LineWidth{i} );

    plot( NSig_mean(i,2:length(Log_Info(:,1))), LineColorMarker_avg{i}, 'LineWidth', LineWidth{i} );
    %plot(  NSig_min(i,2:length(Log_Info(:,1))), LineColorMarker_min{i}, 'LineWidth', LineWidth{i} );
    %plot(  NSig_max(i,2:length(Log_Info(:,1))), LineColorMarker_max{i}, 'LineWidth', LineWidth{i} );
    %plot(  NSig_std(i,2:length(Log_Info(:,1))), LineColorMarker_std{i}, 'LineWidth', LineWidth{i} );
    
    clear N_per_proc__var_number_1 N_per_proc__var_number_2

end
clear NSig_mean NSig_std NSig_min NSig_max

% AddLegend(Number_Case);
%legend(CaseLegend{1}, CaseLegend{2}, CaseLegend{3}, CaseLegend{4}, CaseLegend{7}, CaseLegend{10}, CaseLegend{13}, CaseLegend{8}, CaseLegend{9}, CaseLegend{10} ); 
%legend('2   cpu  mean','2   cpu  min','2   cpu  max', '4   cpu  mean','4   cpu  min','4   cpu  max', '8   cpu  mean','8   cpu  min','8   cpu  max', ...
%       '16  cpu  mean','16  cpu  min','16  cpu  max', '32  cpu  mean','32  cpu min','32  cpu  max', '64  cpu  mean','64  cpu  min','64  cpu  max');
%title(['Stochastic Coherent Adaptive Large Eddy Simulations (SCALES)' 10 'Forced Homogeneous Turbulence' 10 '\epsilon=0.43   \nu=0.09   C_f=6']);
title([Title_Part_1 10 num2str(Resolution_Title),'^3','   \epsilon=',num2str(eps),'   J_{max}=',num2str(J_max_Title),'   \nu=',num2str(nu),'   C_f=',num2str(C_f),'']);

xlabel('Iteration');                     % ylabel('mean nwlt');% ylabel('std  nwlt');% ylabel('min nwlt');% ylabel('max nwlt');
ylabel('(mean, min, max, std)  RGho_N=<NGho_I>/[<NGho_I>+<NGho_B>]');
ylabel('RGho_N=<NGho_I>/[<NGho_I>+<NGho_B>]');

ylabel_hand = get(gca,'ylabel');
set(ylabel_hand,'fontsize',Font_Size_Label);  
xlabel_hand = get(gca,'xlabel');
set(xlabel_hand,'fontsize',Font_Size_Label);  



























%%%   Interesting to compare if ghost are not taken into account:
%%%   Parallel efficiency in terms of stagnant CPU based on Nsig:  
%%%   Rsig_CPU=(<Nsig_I>+<Nsig_B>)/max(Nsig_I+Nsig_B)  or  (<Nsig_I>+<Nsig_B>)/[max(Nsig_I)+max(Nsig_B)], whatever is avalable
figure ('FileName', 'RSig_CPU_1.fig')
hold on;

for i = 1:Number_Case     

    Log_Info = load( Case_Path_Name{i} );

    for j=2:length(Log_Info(:,1))
        length(Log_Info(:,1));
        
        for k = 1:Number_Processors{i}        
            var_number_1 = 1 + 3 + (k-1)*(1+9);             % NWLT_Total,  [pr=0]  NWLT,  iSig,  iSig_I,  iSig_B, iGho, iGho_I, iGho_B, iGhoOnly, iGhoOnly_I, iGhoOnly_B,  [pr=1]  ...
            var_number_2 = 1 + 4 + (k-1)*(1+9);             % NWLT_Total,  [pr=0]  NWLT,  iSig,  iSig_I,  iSig_B, iGho, iGho_I, iGho_B, iGhoOnly, iGhoOnly_I, iGhoOnly_B,  [pr=1]  ...
            N_per_proc__var_number_1(k) = Log_Info(j, var_number_1);    
            N_per_proc__var_number_2(k) = Log_Info(j, var_number_2);    
        end
        
        NSig_mean(i,j) =  ( mean(N_per_proc__var_number_1) + mean(N_per_proc__var_number_2) ) / max( N_per_proc__var_number_1+N_per_proc__var_number_2 );
        %NSig_std(i,j)  =  std(   );
        %NSig_min(i,j)  =  min(   );
        %NSig_max(i,j)  =  max(   );
    
    end
        
%     plot( Log_Info(2:end,1), Nwlt_mean(i,2:length(Log_Info(:,1))), LineColorMarker{i}, 'LineWidth', LineWidth{i} );
%     plot( Log_Info(2:end,1), Nwlt_std(i,2:length(Log_Info(:,1))), LineColorMarker{i}, 'LineWidth', LineWidth{i} );
%     plot( Log_Info(2:end,1), Nwlt_min(i,2:length(Log_Info(:,1))), LineColorMarker{i}, 'LineWidth', LineWidth{i} );
%     plot( Log_Info(2:end,1), Nwlt_max(i,2:length(Log_Info(:,1))), LineColorMarker{i}, 'LineWidth', LineWidth{i} );

    plot( NSig_mean(i,2:length(Log_Info(:,1))), LineColorMarker_avg{i}, 'LineWidth', LineWidth{i} );
    %plot(  NSig_min(i,2:length(Log_Info(:,1))), LineColorMarker_min{i}, 'LineWidth', LineWidth{i} );
    %plot(  NSig_max(i,2:length(Log_Info(:,1))), LineColorMarker_max{i}, 'LineWidth', LineWidth{i} );
    %plot(  NSig_std(i,2:length(Log_Info(:,1))), LineColorMarker_std{i}, 'LineWidth', LineWidth{i} );
    
    clear N_per_proc__var_number_1 N_per_proc__var_number_2

end
clear NSig_mean NSig_std NSig_min NSig_max

% AddLegend(Number_Case);
%legend(CaseLegend{1}, CaseLegend{2}, CaseLegend{3}, CaseLegend{4}, CaseLegend{7}, CaseLegend{10}, CaseLegend{13}, CaseLegend{8}, CaseLegend{9}, CaseLegend{10} ); 
%legend('2   cpu  mean','2   cpu  min','2   cpu  max', '4   cpu  mean','4   cpu  min','4   cpu  max', '8   cpu  mean','8   cpu  min','8   cpu  max', ...
%       '16  cpu  mean','16  cpu  min','16  cpu  max', '32  cpu  mean','32  cpu min','32  cpu  max', '64  cpu  mean','64  cpu  min','64  cpu  max');
%title(['Stochastic Coherent Adaptive Large Eddy Simulations (SCALES)' 10 'Forced Homogeneous Turbulence' 10 '\epsilon=0.43   \nu=0.09   C_f=6']);
title([Title_Part_1 10 num2str(Resolution_Title),'^3','   \epsilon=',num2str(eps),'   J_{max}=',num2str(J_max_Title),'   \nu=',num2str(nu),'   C_f=',num2str(C_f),'']);

xlabel('Iteration');                     % ylabel('mean nwlt');% ylabel('std  nwlt');% ylabel('min nwlt');% ylabel('max nwlt');
ylabel('(mean, min, max, std)  RSig_CPU=(<NSig_I>+<NSig_B>)/max(NSig_I+NSig_B)');
ylabel('RSig_{CPU}=(<NSig_I>+<NSig_B>)/max(NSig_I+NSig_B)');

ylabel_hand = get(gca,'ylabel');
set(ylabel_hand,'fontsize',Font_Size_Label);  
xlabel_hand = get(gca,'xlabel');
set(xlabel_hand,'fontsize',Font_Size_Label);  










%%%   Rsig_CPU=(<Nsig_I>+<Nsig_B>)/max(Nsig_I+Nsig_B)  or  (<Nsig_I>+<Nsig_B>)/[max(Nsig_I)+max(Nsig_B)], whatever is avalable
figure ('FileName', 'RSig_CPU_2.fig')
hold on;

for i = 1:Number_Case     

    Log_Info = load( Case_Path_Name{i} );

    for j=2:length(Log_Info(:,1))
        length(Log_Info(:,1));
        
        for k = 1:Number_Processors{i}        
            var_number_1 = 1 + 3 + (k-1)*(1+9);             % NWLT_Total,  [pr=0]  NWLT,  iSig,  iSig_I,  iSig_B, iGho, iGho_I, iGho_B, iGhoOnly, iGhoOnly_I, iGhoOnly_B,  [pr=1]  ...
            var_number_2 = 1 + 4 + (k-1)*(1+9);             % NWLT_Total,  [pr=0]  NWLT,  iSig,  iSig_I,  iSig_B, iGho, iGho_I, iGho_B, iGhoOnly, iGhoOnly_I, iGhoOnly_B,  [pr=1]  ...
            N_per_proc__var_number_1(k) = Log_Info(j, var_number_1);    
            N_per_proc__var_number_2(k) = Log_Info(j, var_number_2);    
        end
        
        NSig_mean(i,j) =  ( mean(N_per_proc__var_number_1) + mean(N_per_proc__var_number_2) ) / ( max(N_per_proc__var_number_1) + max(N_per_proc__var_number_2) );
        %NSig_std(i,j)  =  std(   );
        %NSig_min(i,j)  =  min(   );
        %NSig_max(i,j)  =  max(   );
    
    end
        
%     plot( Log_Info(2:end,1), Nwlt_mean(i,2:length(Log_Info(:,1))), LineColorMarker{i}, 'LineWidth', LineWidth{i} );
%     plot( Log_Info(2:end,1), Nwlt_std(i,2:length(Log_Info(:,1))), LineColorMarker{i}, 'LineWidth', LineWidth{i} );
%     plot( Log_Info(2:end,1), Nwlt_min(i,2:length(Log_Info(:,1))), LineColorMarker{i}, 'LineWidth', LineWidth{i} );
%     plot( Log_Info(2:end,1), Nwlt_max(i,2:length(Log_Info(:,1))), LineColorMarker{i}, 'LineWidth', LineWidth{i} );

    plot( NSig_mean(i,2:length(Log_Info(:,1))), LineColorMarker_avg{i}, 'LineWidth', LineWidth{i} );
    %plot(  NSig_min(i,2:length(Log_Info(:,1))), LineColorMarker_min{i}, 'LineWidth', LineWidth{i} );
    %plot(  NSig_max(i,2:length(Log_Info(:,1))), LineColorMarker_max{i}, 'LineWidth', LineWidth{i} );
    %plot(  NSig_std(i,2:length(Log_Info(:,1))), LineColorMarker_std{i}, 'LineWidth', LineWidth{i} );
    
    clear N_per_proc__var_number_1 N_per_proc__var_number_2

end
clear NSig_mean NSig_std NSig_min NSig_max

% AddLegend(Number_Case);
%legend(CaseLegend{1}, CaseLegend{2}, CaseLegend{3}, CaseLegend{4}, CaseLegend{7}, CaseLegend{10}, CaseLegend{13}, CaseLegend{8}, CaseLegend{9}, CaseLegend{10} ); 
%legend('2   cpu  mean','2   cpu  min','2   cpu  max', '4   cpu  mean','4   cpu  min','4   cpu  max', '8   cpu  mean','8   cpu  min','8   cpu  max', ...
%       '16  cpu  mean','16  cpu  min','16  cpu  max', '32  cpu  mean','32  cpu min','32  cpu  max', '64  cpu  mean','64  cpu  min','64  cpu  max');
%title(['Stochastic Coherent Adaptive Large Eddy Simulations (SCALES)' 10 'Forced Homogeneous Turbulence' 10 '\epsilon=0.43   \nu=0.09   C_f=6']);
title([Title_Part_1 10 num2str(Resolution_Title),'^3','   \epsilon=',num2str(eps),'   J_{max}=',num2str(J_max_Title),'   \nu=',num2str(nu),'   C_f=',num2str(C_f),'']);

xlabel('Iteration');                     % ylabel('mean nwlt');% ylabel('std  nwlt');% ylabel('min nwlt');% ylabel('max nwlt');
ylabel('(mean, min, max, std)  RSig_{CPU}=(<NSig_I>+<NSig_B>)/[max(NSig_I)+max(NSig_B)]');
ylabel('RSig_{CPU}=(<NSig_I>+<NSig_B>)/[max(NSig_I)+max(NSig_B)]');

ylabel_hand = get(gca,'ylabel');
set(ylabel_hand,'fontsize',Font_Size_Label);  
xlabel_hand = get(gca,'xlabel');
set(xlabel_hand,'fontsize',Font_Size_Label);  










%%%   Parallel efficiency based on Nsig in terms of overhead related to parallel processor:  Rsig_N=<Nsig_I>/[<Nsig_I>+<Nsig_B>]
figure ('FileName', 'RSig_N.fig')
hold on;

for i = 1:Number_Case     

    Log_Info = load( Case_Path_Name{i} );

    for j=2:length(Log_Info(:,1))
        length(Log_Info(:,1));
        
        for k = 1:Number_Processors{i}        
            var_number_1 = 1 + 3 + (k-1)*(1+9);             % NWLT_Total,  [pr=0]  NWLT,  iSig,  iSig_I,  iSig_B, iGho, iGho_I, iGho_B, iGhoOnly, iGhoOnly_I, iGhoOnly_B,  [pr=1]  ...
            var_number_2 = 1 + 4 + (k-1)*(1+9);             % NWLT_Total,  [pr=0]  NWLT,  iSig,  iSig_I,  iSig_B, iGho, iGho_I, iGho_B, iGhoOnly, iGhoOnly_I, iGhoOnly_B,  [pr=1]  ...
            N_per_proc__var_number_1(k) = Log_Info(j, var_number_1);    
            N_per_proc__var_number_2(k) = Log_Info(j, var_number_2);    
        end
        
        NSig_mean(i,j) =  ( mean(N_per_proc__var_number_1) ) / ( mean(N_per_proc__var_number_1) + mean(N_per_proc__var_number_2) );
        %NSig_std(i,j)  =  std(   );
        %NSig_min(i,j)  =  min(   );
        %NSig_max(i,j)  =  max(   );
    
    end
        
%     plot( Log_Info(2:end,1), Nwlt_mean(i,2:length(Log_Info(:,1))), LineColorMarker{i}, 'LineWidth', LineWidth{i} );
%     plot( Log_Info(2:end,1), Nwlt_std(i,2:length(Log_Info(:,1))), LineColorMarker{i}, 'LineWidth', LineWidth{i} );
%     plot( Log_Info(2:end,1), Nwlt_min(i,2:length(Log_Info(:,1))), LineColorMarker{i}, 'LineWidth', LineWidth{i} );
%     plot( Log_Info(2:end,1), Nwlt_max(i,2:length(Log_Info(:,1))), LineColorMarker{i}, 'LineWidth', LineWidth{i} );

    plot( NSig_mean(i,2:length(Log_Info(:,1))), LineColorMarker_avg{i}, 'LineWidth', LineWidth{i} );
    %plot(  NSig_min(i,2:length(Log_Info(:,1))), LineColorMarker_min{i}, 'LineWidth', LineWidth{i} );
    %plot(  NSig_max(i,2:length(Log_Info(:,1))), LineColorMarker_max{i}, 'LineWidth', LineWidth{i} );
    %plot(  NSig_std(i,2:length(Log_Info(:,1))), LineColorMarker_std{i}, 'LineWidth', LineWidth{i} );
    
    clear N_per_proc__var_number_1 N_per_proc__var_number_2

end
clear NSig_mean NSig_std NSig_min NSig_max

% AddLegend(Number_Case);
%legend(CaseLegend{1}, CaseLegend{2}, CaseLegend{3}, CaseLegend{4}, CaseLegend{7}, CaseLegend{10}, CaseLegend{13}, CaseLegend{8}, CaseLegend{9}, CaseLegend{10} ); 
%legend('2   cpu  mean','2   cpu  min','2   cpu  max', '4   cpu  mean','4   cpu  min','4   cpu  max', '8   cpu  mean','8   cpu  min','8   cpu  max', ...
%       '16  cpu  mean','16  cpu  min','16  cpu  max', '32  cpu  mean','32  cpu min','32  cpu  max', '64  cpu  mean','64  cpu  min','64  cpu  max');
%title(['Stochastic Coherent Adaptive Large Eddy Simulations (SCALES)' 10 'Forced Homogeneous Turbulence' 10 '\epsilon=0.43   \nu=0.09   C_f=6']);
title([Title_Part_1 10 num2str(Resolution_Title),'^3','   \epsilon=',num2str(eps),'   J_{max}=',num2str(J_max_Title),'   \nu=',num2str(nu),'   C_f=',num2str(C_f),'']);

xlabel('Iteration');                     % ylabel('mean nwlt');% ylabel('std  nwlt');% ylabel('min nwlt');% ylabel('max nwlt');
ylabel('(mean, min, max, std)  RSig_N=<NSig_I>/[<NSig_I>+<NSig_B>]');
ylabel('RSig_N=<NSig_I>/[<NSig_I>+<NSig_B>]');

ylabel_hand = get(gca,'ylabel');
set(ylabel_hand,'fontsize',Font_Size_Label);  
xlabel_hand = get(gca,'xlabel');
set(xlabel_hand,'fontsize',Font_Size_Label);  







































% (<Ngho_I>+<Ngho_B>)/<Ngho_I>
figure ('FileName', '1.fig')
hold on;

for i = 1:Number_Case     

    Log_Info = load( Case_Path_Name{i} );

    for j=2:length(Log_Info(:,1))
        length(Log_Info(:,1));
        
        for k = 1:Number_Processors{i}        
            var_number_1 = 1 + 6 + (k-1)*(1+9);             % NWLT_Total,  [pr=0]  NWLT,  iSig,  iSig_I,  iSig_B, iGho, iGho_I, iGho_B, iGhoOnly, iGhoOnly_I, iGhoOnly_B,  [pr=1]  ...
            var_number_2 = 1 + 7 + (k-1)*(1+9);             % NWLT_Total,  [pr=0]  NWLT,  iSig,  iSig_I,  iSig_B, iGho, iGho_I, iGho_B, iGhoOnly, iGhoOnly_I, iGhoOnly_B,  [pr=1]  ...
            N_per_proc__var_number_1(k) = Log_Info(j, var_number_1);    
            N_per_proc__var_number_2(k) = Log_Info(j, var_number_2);    
        end
        
        NSig_mean(i,j) =  ( mean(N_per_proc__var_number_1) + mean(N_per_proc__var_number_2) ) / ( mean(N_per_proc__var_number_1) );
        %NSig_std(i,j)  =  std(   );
        %NSig_min(i,j)  =  min(   );
        %NSig_max(i,j)  =  max(   );
    
    end
        
%     plot( Log_Info(2:end,1), Nwlt_mean(i,2:length(Log_Info(:,1))), LineColorMarker{i}, 'LineWidth', LineWidth{i} );
%     plot( Log_Info(2:end,1), Nwlt_std(i,2:length(Log_Info(:,1))), LineColorMarker{i}, 'LineWidth', LineWidth{i} );
%     plot( Log_Info(2:end,1), Nwlt_min(i,2:length(Log_Info(:,1))), LineColorMarker{i}, 'LineWidth', LineWidth{i} );
%     plot( Log_Info(2:end,1), Nwlt_max(i,2:length(Log_Info(:,1))), LineColorMarker{i}, 'LineWidth', LineWidth{i} );

    plot( NSig_mean(i,2:length(Log_Info(:,1))), LineColorMarker_avg{i}, 'LineWidth', LineWidth{i} );
    %plot(  NSig_min(i,2:length(Log_Info(:,1))), LineColorMarker_min{i}, 'LineWidth', LineWidth{i} );
    %plot(  NSig_max(i,2:length(Log_Info(:,1))), LineColorMarker_max{i}, 'LineWidth', LineWidth{i} );
    %plot(  NSig_std(i,2:length(Log_Info(:,1))), LineColorMarker_std{i}, 'LineWidth', LineWidth{i} );
    
    clear N_per_proc__var_number_1 N_per_proc__var_number_2

end
clear NSig_mean NSig_std NSig_min NSig_max

% AddLegend(Number_Case);
%legend(CaseLegend{1}, CaseLegend{2}, CaseLegend{3}, CaseLegend{4}, CaseLegend{7}, CaseLegend{10}, CaseLegend{13}, CaseLegend{8}, CaseLegend{9}, CaseLegend{10} ); 
%legend('2   cpu  mean','2   cpu  min','2   cpu  max', '4   cpu  mean','4   cpu  min','4   cpu  max', '8   cpu  mean','8   cpu  min','8   cpu  max', ...
%       '16  cpu  mean','16  cpu  min','16  cpu  max', '32  cpu  mean','32  cpu min','32  cpu  max', '64  cpu  mean','64  cpu  min','64  cpu  max');
%title(['Stochastic Coherent Adaptive Large Eddy Simulations (SCALES)' 10 'Forced Homogeneous Turbulence' 10 '\epsilon=0.43   \nu=0.09   C_f=6']);
title([Title_Part_1 10 num2str(Resolution_Title),'^3','   \epsilon=',num2str(eps),'   J_{max}=',num2str(J_max_Title),'   \nu=',num2str(nu),'   C_f=',num2str(C_f),'']);

xlabel('Iteration');                     % ylabel('mean nwlt');% ylabel('std  nwlt');% ylabel('min nwlt');% ylabel('max nwlt');
ylabel('(mean, min, max, std)  (<NGho_I>+<NGho_B>)/<NGho_I>');
ylabel('(<NGho_I>+<NGho_B>)/<NGho_I>');

ylabel_hand = get(gca,'ylabel');
set(ylabel_hand,'fontsize',Font_Size_Label);  
xlabel_hand = get(gca,'xlabel');
set(xlabel_hand,'fontsize',Font_Size_Label);  











% max(Ngho_I+Ngho_B)/<Ngho_I>, <Ngho_B/Ngho_I>
figure ('FileName', '2.fig')
hold on;

for i = 1:Number_Case     

    Log_Info = load( Case_Path_Name{i} );

    for j=2:length(Log_Info(:,1))
        length(Log_Info(:,1));
        
        for k = 1:Number_Processors{i}        
            var_number_1 = 1 + 6 + (k-1)*(1+9);             % NWLT_Total,  [pr=0]  NWLT,  iSig,  iSig_I,  iSig_B, iGho, iGho_I, iGho_B, iGhoOnly, iGhoOnly_I, iGhoOnly_B,  [pr=1]  ...
            var_number_2 = 1 + 7 + (k-1)*(1+9);             % NWLT_Total,  [pr=0]  NWLT,  iSig,  iSig_I,  iSig_B, iGho, iGho_I, iGho_B, iGhoOnly, iGhoOnly_I, iGhoOnly_B,  [pr=1]  ...
            N_per_proc__var_number_1(k) = Log_Info(j, var_number_1);    
            N_per_proc__var_number_2(k) = Log_Info(j, var_number_2);    
        end
        
        NSig_mean(i,j) =  ( max(N_per_proc__var_number_1 + N_per_proc__var_number_2) ) / ( mean(N_per_proc__var_number_1) );
        %NSig_std(i,j)  =  std(   );
        %NSig_min(i,j)  =  min(   );
        %NSig_max(i,j)  =  max(   );
    
    end
        
%     plot( Log_Info(2:end,1), Nwlt_mean(i,2:length(Log_Info(:,1))), LineColorMarker{i}, 'LineWidth', LineWidth{i} );
%     plot( Log_Info(2:end,1), Nwlt_std(i,2:length(Log_Info(:,1))), LineColorMarker{i}, 'LineWidth', LineWidth{i} );
%     plot( Log_Info(2:end,1), Nwlt_min(i,2:length(Log_Info(:,1))), LineColorMarker{i}, 'LineWidth', LineWidth{i} );
%     plot( Log_Info(2:end,1), Nwlt_max(i,2:length(Log_Info(:,1))), LineColorMarker{i}, 'LineWidth', LineWidth{i} );

    plot( NSig_mean(i,2:length(Log_Info(:,1))), LineColorMarker_avg{i}, 'LineWidth', LineWidth{i} );
    %plot(  NSig_min(i,2:length(Log_Info(:,1))), LineColorMarker_min{i}, 'LineWidth', LineWidth{i} );
    %plot(  NSig_max(i,2:length(Log_Info(:,1))), LineColorMarker_max{i}, 'LineWidth', LineWidth{i} );
    %plot(  NSig_std(i,2:length(Log_Info(:,1))), LineColorMarker_std{i}, 'LineWidth', LineWidth{i} );
    
    clear N_per_proc__var_number_1 N_per_proc__var_number_2

end
clear NSig_mean NSig_std NSig_min NSig_max

% AddLegend(Number_Case);
%legend(CaseLegend{1}, CaseLegend{2}, CaseLegend{3}, CaseLegend{4}, CaseLegend{7}, CaseLegend{10}, CaseLegend{13}, CaseLegend{8}, CaseLegend{9}, CaseLegend{10} ); 
%legend('2   cpu  mean','2   cpu  min','2   cpu  max', '4   cpu  mean','4   cpu  min','4   cpu  max', '8   cpu  mean','8   cpu  min','8   cpu  max', ...
%       '16  cpu  mean','16  cpu  min','16  cpu  max', '32  cpu  mean','32  cpu min','32  cpu  max', '64  cpu  mean','64  cpu  min','64  cpu  max');
%title(['Stochastic Coherent Adaptive Large Eddy Simulations (SCALES)' 10 'Forced Homogeneous Turbulence' 10 '\epsilon=0.43   \nu=0.09   C_f=6']);
title([Title_Part_1 10 num2str(Resolution_Title),'^3','   \epsilon=',num2str(eps),'   J_{max}=',num2str(J_max_Title),'   \nu=',num2str(nu),'   C_f=',num2str(C_f),'']);

xlabel('Iteration');                     % ylabel('mean nwlt');% ylabel('std  nwlt');% ylabel('min nwlt');% ylabel('max nwlt');
ylabel('(mean, min, max, std)  max(NGho_I+NGho_B)/<NGho_I>');
ylabel('max(NGho_I+NGho_B)/<NGho_I>');

ylabel_hand = get(gca,'ylabel');
set(ylabel_hand,'fontsize',Font_Size_Label);  
xlabel_hand = get(gca,'xlabel');
set(xlabel_hand,'fontsize',Font_Size_Label);  




end
    







if (Plot_Ratio_Means)

% <Ngho_B/Ngho_I>
figure ('FileName', '4.fig')
hold on;
set(gcf,'Position', [34,65,1280,662]); 

for i = 1:Number_Case     

    Log_Info = load( Case_Path_Name{i} );

    for j=2:length(Log_Info(:,1))
        length(Log_Info(:,1));
        
        for k = 1:Number_Processors{i}        
            var_number_1 = 1 + 6 + (k-1)*(1+9);             % NWLT_Total,  [pr=0]  NWLT,  iSig,  iSig_I,  iSig_B, iGho, iGho_I, iGho_B, iGhoOnly, iGhoOnly_I, iGhoOnly_B,  [pr=1]  ...
            var_number_2 = 1 + 7 + (k-1)*(1+9);             % NWLT_Total,  [pr=0]  NWLT,  iSig,  iSig_I,  iSig_B, iGho, iGho_I, iGho_B, iGhoOnly, iGhoOnly_I, iGhoOnly_B,  [pr=1]  ...
            N_per_proc__var_number_1(k) = Log_Info(j, var_number_1);    
            N_per_proc__var_number_2(k) = Log_Info(j, var_number_2);    
        end
        
        NSig_mean(i,j) =  mean( N_per_proc__var_number_2 / N_per_proc__var_number_1 ) ;
        %NSig_std(i,j)  =  std(   );
        %NSig_min(i,j)  =  min(   );
        %NSig_max(i,j)  =  max(   );
    
    end
        
%     plot( Log_Info(2:end,1), Nwlt_mean(i,2:length(Log_Info(:,1))), LineColorMarker{i}, 'LineWidth', LineWidth{i} );
%     plot( Log_Info(2:end,1), Nwlt_std(i,2:length(Log_Info(:,1))), LineColorMarker{i}, 'LineWidth', LineWidth{i} );
%     plot( Log_Info(2:end,1), Nwlt_min(i,2:length(Log_Info(:,1))), LineColorMarker{i}, 'LineWidth', LineWidth{i} );
%     plot( Log_Info(2:end,1), Nwlt_max(i,2:length(Log_Info(:,1))), LineColorMarker{i}, 'LineWidth', LineWidth{i} );

    plot( NSig_mean(i,2:length(Log_Info(:,1))), LineColorMarker_avg{i}, 'LineWidth', LineWidth{i} );
    %plot(  NSig_min(i,2:length(Log_Info(:,1))), LineColorMarker_min{i}, 'LineWidth', LineWidth{i} );
    %plot(  NSig_max(i,2:length(Log_Info(:,1))), LineColorMarker_max{i}, 'LineWidth', LineWidth{i} );
    %plot(  NSig_std(i,2:length(Log_Info(:,1))), LineColorMarker_std{i}, 'LineWidth', LineWidth{i} );
    
    clear N_per_proc__var_number_1 N_per_proc__var_number_2

end
clear NSig_mean NSig_std NSig_min NSig_max

AddLegend(Number_Case);
%legend(CaseLegend{1}, CaseLegend{2}, CaseLegend{3}, CaseLegend{4}, CaseLegend{7}, CaseLegend{10}, CaseLegend{13}, CaseLegend{8}, CaseLegend{9}, CaseLegend{10} ); 
%legend('2   cpu  mean','2   cpu  min','2   cpu  max', '4   cpu  mean','4   cpu  min','4   cpu  max', '8   cpu  mean','8   cpu  min','8   cpu  max', ...
%       '16  cpu  mean','16  cpu  min','16  cpu  max', '32  cpu  mean','32  cpu min','32  cpu  max', '64  cpu  mean','64  cpu  min','64  cpu  max');
%title(['Stochastic Coherent Adaptive Large Eddy Simulations (SCALES)' 10 'Forced Homogeneous Turbulence' 10 '\epsilon=0.43   \nu=0.09   C_f=6']);

%title([Title_Part_1 10 num2str(Resolution_Title),'^3','   \epsilon=',num2str(eps),'   J_{max}=',num2str(J_max_Title),'   \nu=',num2str(nu),'   C_f=',num2str(C_f),'']);

xlabel('Iteration');                     % ylabel('mean nwlt');% ylabel('std  nwlt');% ylabel('min nwlt');% ylabel('max nwlt');
ylabel('(mean, min, max, std)  <NGho_B/NGho_I>');
ylabel('<NGho_B  /  NGho_I>');

ylabel_hand = get(gca,'ylabel');
set(ylabel_hand,'fontsize',Font_Size);  
xlabel_hand = get(gca,'xlabel');
set(xlabel_hand,'fontsize',Font_Size_Label);  

box(gca,'on');

clear NSig_mean NSig_std NSig_min NSig_max NSig_std_p NSig_std_m







    






% <Ngho_B>/<Ngho_I>
figure ('FileName', '3.fig')
hold on;
set(gcf,'Position', [34,65,1280,662]); 

for i = 1:Number_Case     

    Log_Info = load( Case_Path_Name{i} );

    for j=2:length(Log_Info(:,1))
        length(Log_Info(:,1));
        
        for k = 1:Number_Processors{i}        
            var_number_1 = 1 + 6 + (k-1)*(1+9);             % NWLT_Total,  [pr=0]  NWLT,  iSig,  iSig_I,  iSig_B, iGho, iGho_I, iGho_B, iGhoOnly, iGhoOnly_I, iGhoOnly_B,  [pr=1]  ...
            var_number_2 = 1 + 7 + (k-1)*(1+9);             % NWLT_Total,  [pr=0]  NWLT,  iSig,  iSig_I,  iSig_B, iGho, iGho_I, iGho_B, iGhoOnly, iGhoOnly_I, iGhoOnly_B,  [pr=1]  ...
            N_per_proc__var_number_1(k) = Log_Info(j, var_number_1);    
            N_per_proc__var_number_2(k) = Log_Info(j, var_number_2);    
        end
        
        NSig_mean(i,j) =  mean( N_per_proc__var_number_2 ) / mean ( N_per_proc__var_number_1 ) ;
        %NSig_std(i,j)  =  std(   );
        %NSig_min(i,j)  =  min(   );
        %NSig_max(i,j)  =  max(   );
    
    end
        
%     plot( Log_Info(2:end,1), Nwlt_mean(i,2:length(Log_Info(:,1))), LineColorMarker{i}, 'LineWidth', LineWidth{i} );
%     plot( Log_Info(2:end,1), Nwlt_std(i,2:length(Log_Info(:,1))), LineColorMarker{i}, 'LineWidth', LineWidth{i} );
%     plot( Log_Info(2:end,1), Nwlt_min(i,2:length(Log_Info(:,1))), LineColorMarker{i}, 'LineWidth', LineWidth{i} );
%     plot( Log_Info(2:end,1), Nwlt_max(i,2:length(Log_Info(:,1))), LineColorMarker{i}, 'LineWidth', LineWidth{i} );

    plot( NSig_mean(i,2:length(Log_Info(:,1))), LineColorMarker_avg{i}, 'LineWidth', LineWidth{i} );
    %plot(  NSig_min(i,2:length(Log_Info(:,1))), LineColorMarker_min{i}, 'LineWidth', LineWidth{i} );
    %plot(  NSig_max(i,2:length(Log_Info(:,1))), LineColorMarker_max{i}, 'LineWidth', LineWidth{i} );
    %plot(  NSig_std(i,2:length(Log_Info(:,1))), LineColorMarker_std{i}, 'LineWidth', LineWidth{i} );
    
    clear N_per_proc__var_number_1 N_per_proc__var_number_2

end
clear NSig_mean NSig_std NSig_min NSig_max

AddLegend(Number_Case);
%legend(CaseLegend{1}, CaseLegend{2}, CaseLegend{3}, CaseLegend{4}, CaseLegend{7}, CaseLegend{10}, CaseLegend{13}, CaseLegend{8}, CaseLegend{9}, CaseLegend{10} ); 
%legend('2   cpu  mean','2   cpu  min','2   cpu  max', '4   cpu  mean','4   cpu  min','4   cpu  max', '8   cpu  mean','8   cpu  min','8   cpu  max', ...
%       '16  cpu  mean','16  cpu  min','16  cpu  max', '32  cpu  mean','32  cpu min','32  cpu  max', '64  cpu  mean','64  cpu  min','64  cpu  max');
%title(['Stochastic Coherent Adaptive Large Eddy Simulations (SCALES)' 10 'Forced Homogeneous Turbulence' 10 '\epsilon=0.43   \nu=0.09   C_f=6']);

%title([Title_Part_1 10 num2str(Resolution_Title),'^3','   \epsilon=',num2str(eps),'   J_{max}=',num2str(J_max_Title),'   \nu=',num2str(nu),'   C_f=',num2str(C_f),'']);

xlabel('Iteration');                     % ylabel('mean nwlt');% ylabel('std  nwlt');% ylabel('min nwlt');% ylabel('max nwlt');
ylabel('(mean, min, max, std)  <NGho_B>/<NGho_I>');
ylabel('<NGho_B>  /  <NGho_I>');
ylabel('$\frac{\langle{\sf{N}}_{{\sf SAG}_{\sf B}}\rangle}{\langle{\sf{N}}_{{\sf SAG}_{\sf I}}\rangle}$', 'Interpreter','Latex','fontsize',Font_Size);

ylabel_hand = get(gca,'ylabel');
set(ylabel_hand,'fontsize',Font_Size);  
xlabel_hand = get(gca,'xlabel');
set(xlabel_hand,'fontsize',Font_Size_Label);  

box(gca,'on');

clear NSig_mean NSig_std NSig_min NSig_max NSig_std_p NSig_std_m












% <N_SA_B/N_SA_I>
figure ('FileName', '6.fig')
hold on;
set(gcf,'Position', [34,65,1280,662]); 

for i = 1:Number_Case     

    Log_Info = load( Case_Path_Name{i} );

    for j=2:length(Log_Info(:,1))
        length(Log_Info(:,1));
        
        for k = 1:Number_Processors{i}        
            var_number_1 = 1 + 3 + (k-1)*(1+9);             % NWLT_Total,  [pr=0]  NWLT,  iSig,  iSig_I,  iSig_B, iGho, iGho_I, iGho_B, iGhoOnly, iGhoOnly_I, iGhoOnly_B,  [pr=1]  ...
            var_number_2 = 1 + 4 + (k-1)*(1+9);             % NWLT_Total,  [pr=0]  NWLT,  iSig,  iSig_I,  iSig_B, iGho, iGho_I, iGho_B, iGhoOnly, iGhoOnly_I, iGhoOnly_B,  [pr=1]  ...
            N_per_proc__var_number_1(k) = Log_Info(j, var_number_1);    
            N_per_proc__var_number_2(k) = Log_Info(j, var_number_2);    
        end
        
        NSig_mean(i,j) =  mean( N_per_proc__var_number_2 / N_per_proc__var_number_1 ) ;
        %NSig_std(i,j)  =  std(   );
        %NSig_min(i,j)  =  min(   );
        %NSig_max(i,j)  =  max(   );
    
    end
        
%     plot( Log_Info(2:end,1), Nwlt_mean(i,2:length(Log_Info(:,1))), LineColorMarker{i}, 'LineWidth', LineWidth{i} );
%     plot( Log_Info(2:end,1), Nwlt_std(i,2:length(Log_Info(:,1))), LineColorMarker{i}, 'LineWidth', LineWidth{i} );
%     plot( Log_Info(2:end,1), Nwlt_min(i,2:length(Log_Info(:,1))), LineColorMarker{i}, 'LineWidth', LineWidth{i} );
%     plot( Log_Info(2:end,1), Nwlt_max(i,2:length(Log_Info(:,1))), LineColorMarker{i}, 'LineWidth', LineWidth{i} );

    plot( NSig_mean(i,2:length(Log_Info(:,1))), LineColorMarker_avg{i}, 'LineWidth', LineWidth{i} );
    %plot(  NSig_min(i,2:length(Log_Info(:,1))), LineColorMarker_min{i}, 'LineWidth', LineWidth{i} );
    %plot(  NSig_max(i,2:length(Log_Info(:,1))), LineColorMarker_max{i}, 'LineWidth', LineWidth{i} );
    %plot(  NSig_std(i,2:length(Log_Info(:,1))), LineColorMarker_std{i}, 'LineWidth', LineWidth{i} );
    
    clear N_per_proc__var_number_1 N_per_proc__var_number_2

end
clear NSig_mean NSig_std NSig_min NSig_max

AddLegend(Number_Case);
%legend(CaseLegend{1}, CaseLegend{2}, CaseLegend{3}, CaseLegend{4}, CaseLegend{7}, CaseLegend{10}, CaseLegend{13}, CaseLegend{8}, CaseLegend{9}, CaseLegend{10} ); 
%legend('2   cpu  mean','2   cpu  min','2   cpu  max', '4   cpu  mean','4   cpu  min','4   cpu  max', '8   cpu  mean','8   cpu  min','8   cpu  max', ...
%       '16  cpu  mean','16  cpu  min','16  cpu  max', '32  cpu  mean','32  cpu min','32  cpu  max', '64  cpu  mean','64  cpu  min','64  cpu  max');
%title(['Stochastic Coherent Adaptive Large Eddy Simulations (SCALES)' 10 'Forced Homogeneous Turbulence' 10 '\epsilon=0.43   \nu=0.09   C_f=6']);

%title([Title_Part_1 10 num2str(Resolution_Title),'^3','   \epsilon=',num2str(eps),'   J_{max}=',num2str(J_max_Title),'   \nu=',num2str(nu),'   C_f=',num2str(C_f),'']);

xlabel('Iteration');                     % ylabel('mean nwlt');% ylabel('std  nwlt');% ylabel('min nwlt');% ylabel('max nwlt');
ylabel('(mean, min, max, std)  <NSA_B/NSA_I>');
ylabel('<NSA_B  /  NSA_I>');

ylabel_hand = get(gca,'ylabel');
set(ylabel_hand,'fontsize',Font_Size);  
xlabel_hand = get(gca,'xlabel');
set(xlabel_hand,'fontsize',Font_Size_Label);  

box(gca,'on');

clear NSig_mean NSig_std NSig_min NSig_max NSig_std_p NSig_std_m










% <N_SA_B>/<N_SA_I>
figure ('FileName', '5.fig')
hold on;
set(gcf,'Position', [34,65,1280,662]); 

for i = 1:Number_Case     

    Log_Info = load( Case_Path_Name{i} );

    for j=2:length(Log_Info(:,1))
        length(Log_Info(:,1));
        
        for k = 1:Number_Processors{i}        
            var_number_1 = 1 + 3 + (k-1)*(1+9);             % NWLT_Total,  [pr=0]  NWLT,  iSig,  iSig_I,  iSig_B, iGho, iGho_I, iGho_B, iGhoOnly, iGhoOnly_I, iGhoOnly_B,  [pr=1]  ...
            var_number_2 = 1 + 4 + (k-1)*(1+9);             % NWLT_Total,  [pr=0]  NWLT,  iSig,  iSig_I,  iSig_B, iGho, iGho_I, iGho_B, iGhoOnly, iGhoOnly_I, iGhoOnly_B,  [pr=1]  ...
            N_per_proc__var_number_1(k) = Log_Info(j, var_number_1);    
            N_per_proc__var_number_2(k) = Log_Info(j, var_number_2);    
        end
        
        NSig_mean(i,j) =  mean( N_per_proc__var_number_2 ) / mean( N_per_proc__var_number_1 ) ;
        %NSig_std(i,j)  =  std(   );
        %NSig_min(i,j)  =  min(   );
        %NSig_max(i,j)  =  max(   );
    
    end
        
%     plot( Log_Info(2:end,1), Nwlt_mean(i,2:length(Log_Info(:,1))), LineColorMarker{i}, 'LineWidth', LineWidth{i} );
%     plot( Log_Info(2:end,1), Nwlt_std(i,2:length(Log_Info(:,1))), LineColorMarker{i}, 'LineWidth', LineWidth{i} );
%     plot( Log_Info(2:end,1), Nwlt_min(i,2:length(Log_Info(:,1))), LineColorMarker{i}, 'LineWidth', LineWidth{i} );
%     plot( Log_Info(2:end,1), Nwlt_max(i,2:length(Log_Info(:,1))), LineColorMarker{i}, 'LineWidth', LineWidth{i} );

    plot( NSig_mean(i,2:length(Log_Info(:,1))), LineColorMarker_avg{i}, 'LineWidth', LineWidth{i} );
    %plot(  NSig_min(i,2:length(Log_Info(:,1))), LineColorMarker_min{i}, 'LineWidth', LineWidth{i} );
    %plot(  NSig_max(i,2:length(Log_Info(:,1))), LineColorMarker_max{i}, 'LineWidth', LineWidth{i} );
    %plot(  NSig_std(i,2:length(Log_Info(:,1))), LineColorMarker_std{i}, 'LineWidth', LineWidth{i} );
    
    clear N_per_proc__var_number_1 N_per_proc__var_number_2

end
clear NSig_mean NSig_std NSig_min NSig_max

AddLegend(Number_Case);
%legend(CaseLegend{1}, CaseLegend{2}, CaseLegend{3}, CaseLegend{4}, CaseLegend{7}, CaseLegend{10}, CaseLegend{13}, CaseLegend{8}, CaseLegend{9}, CaseLegend{10} ); 
%legend('2   cpu  mean','2   cpu  min','2   cpu  max', '4   cpu  mean','4   cpu  min','4   cpu  max', '8   cpu  mean','8   cpu  min','8   cpu  max', ...
%       '16  cpu  mean','16  cpu  min','16  cpu  max', '32  cpu  mean','32  cpu min','32  cpu  max', '64  cpu  mean','64  cpu  min','64  cpu  max');
%title(['Stochastic Coherent Adaptive Large Eddy Simulations (SCALES)' 10 'Forced Homogeneous Turbulence' 10 '\epsilon=0.43   \nu=0.09   C_f=6']);

%title([Title_Part_1 10 num2str(Resolution_Title),'^3','   \epsilon=',num2str(eps),'   J_{max}=',num2str(J_max_Title),'   \nu=',num2str(nu),'   C_f=',num2str(C_f),'']);

xlabel('Iteration');                     % ylabel('mean nwlt');% ylabel('std  nwlt');% ylabel('min nwlt');% ylabel('max nwlt');
ylabel('(mean, min, max, std)  <NSA_B>/<NSA_I>');
ylabel('<NSA_B>  /  <NSA_I>');
ylabel('$\frac{\langle{\sf{N}}_{{\sf SA}_{\sf B}}\rangle}{\langle{\sf{N}}_{{\sf SA}_{\sf I}}\rangle}$', 'Interpreter','Latex','fontsize',Font_Size);

ylabel_hand = get(gca,'ylabel');
set(ylabel_hand,'fontsize',Font_Size);  
xlabel_hand = get(gca,'xlabel');
set(xlabel_hand,'fontsize',Font_Size_Label);  

box(gca,'on');

clear NSig_mean NSig_std NSig_min NSig_max NSig_std_p NSig_std_m

end









if (Plot_Ncpu)

% LineColorMarker_avg_TimeAverage  =  {  'sk-';   '^r-';   'ob-';    '>g-';   '<m-';   'dc-';   };
% LineColorMarker_min_TimeAverage  =  {  'sk-';   '^r-';   'ob-';    '>g-';   '<m-';   'dc-';   };
% LineColorMarker_max_TimeAverage  =  {  'sk-';   '^r-';   'ob-';    '>g-';   '<m-';   'dc-';   };
LineColorMarker_avg_TimeAverage  =  {  'sk-';   '^k-';   'ok-';    '>k-';   '<k-';   'dk-';  'sr-';   '^r-';   'or-';    '>r-';   '<r-';   'dr-';   };
LineColorMarker_min_TimeAverage  =  {  'sk-';   '^k-';   'ok-';    '>k-';   '<k-';   'dk-';  'sr-';   '^r-';   'or-';    '>r-';   '<r-';   'dr-';   };
LineColorMarker_max_TimeAverage  =  {  'sk-';   '^k-';   'ok-';    '>k-';   '<k-';   'dk-';  'sr-';   '^r-';   'or-';    '>r-';   '<r-';   'dr-';   };

%Marker_Size = { 10; 10; 10; 10; 10; 10; 6; 6; 6; 6; 6; 6; };
Marker_Size = { 6; 6; 6; 6; 6; 6; 4; 4; 4; 4; 4; 4; };

LineWidth        =  {0.5;    0.5;    0.5;     0.5;    0.5;    0.5;    0.5;    0.5;    0.5;   0.5;   0.5;    0.5;    0.5;    0.5;    0.5;    0.5;    0.5;    0.5;    0.5;   0.5; };
LineWidth_avg    =  {0.5;    0.5;    0.5;     0.5;    0.5;    0.5;    0.5;    0.5;    0.5;   0.5;   0.5;    0.5;    0.5;    0.5;    0.5;    0.5;    0.5;    0.5;    0.5;   0.5; };
%LineWidth_avg    =  {2.5;    2.5;    2.5;     2.5;    2.5;    2.5;    2.5;    2.5;    2.5;   2.5;   2.5;    2.5;    2.5;    2.5;    2.5;    2.5;    2.5;    2.5;    2.5;   2.5; };


var_index = [3 4 5];    
    
for Fig_Number = 1:3
    
switch Fig_Number
    case 1
        figure ('FileName', 'NSA_I_Ncpu__All2AllOne2One.fig')
    case 2
        figure ('FileName', 'NSA_B_Ncpu__All2AllOne2One.fig')
    case 3
        figure ('FileName', 'NSAG_Ncpu__All2AllOne2One.fig')
end
hold on;
set(gcf,'Position', [34,65,1280,662]); 

for i = 1:Number_Case     

    Log_Info = load( Case_Path_Name{i} );

    for j=2:length(Log_Info(:,1))
        length(Log_Info(:,1));
        
        for k = 1:Number_Processors{i}        
            var_number = 1 + var_index(Fig_Number) + (k-1)*(1+9);   % NWLT_Total,  [pr=0]  NWLT,  iSig,  iSig_I,  iSig_B, iGho, iGho_I, iGho_B, iGhoOnly, iGhoOnly_I, iGhoOnly_B,  [pr=1]  ...
            NSig_per_proc(k) = Log_Info(j, var_number);    
        end
        
        NSig_mean(i,j) =  mean( NSig_per_proc );
       %NSig_std(i,j)  =  std(  NSig_per_proc );
        NSig_min(i,j)  =  min(  NSig_per_proc );
        NSig_max(i,j)  =  max(  NSig_per_proc );
    
       %NSig_std_p(i,j)  =  NSig_mean(i,j) + NSig_std(i,j);
       %NSig_std_m(i,j)  =  NSig_mean(i,j) - NSig_std(i,j);
    end
        
    clear NSig_per_proc
    
    NSig_mean_TimeAveraged(i) = mean(NSig_mean(i,2:length(Log_Info(:,1))));
    NSig_max_TimeAveraged(i)  = mean(NSig_max(i,2:length(Log_Info(:,1))));
    NSig_min_TimeAveraged(i)  = mean(NSig_min(i,2:length(Log_Info(:,1))));

end

clear NSig_mean NSig_std NSig_min NSig_max NSig_std_p NSig_std_m








i_start=1;
    
for i = i_start:Number_Case  % ii:ii+2
    plot( Number_Processors{i}, NSig_mean_TimeAveraged(i), LineColorMarker_avg_TimeAverage{i}, 'LineWidth', LineWidth_avg{i}, 'MarkerSize', Marker_Size{i} );
    plot( Number_Processors{i},  NSig_min_TimeAveraged(i), LineColorMarker_min_TimeAverage{i}, 'LineWidth', LineWidth{i},     'MarkerSize', Marker_Size{i} );
    plot( Number_Processors{i},  NSig_max_TimeAveraged(i), LineColorMarker_max_TimeAverage{i}, 'LineWidth', LineWidth{i},     'MarkerSize', Marker_Size{i} );
end

% legend('mean', 'min', 'max', 'Location','NorthEast', 'FontSize',Font_Size-4);


switch Number_Processors{Number_Case}
   case 64
       set(gca,'XTick',[2 4 8 16 32 64])
       %set(gca,'YTick',[2 4 8 16 32 64])
   case 128
       set(gca,'XTick',[2 4 8 16 32 64 128])
       %set(gca,'YTick',[2 4 8 16 32 64 128])
   case 256
       set(gca,'XTick',[2 4 8 16 32 64 128 256])
       %set(gca,'YTick',[2 4 8 16 32 64 128 256])
   case 512
       set(gca,'XTick',[2 4 8 16 32 64 128 256 512])
       %set(gca,'YTick',[2 4 8 16 32 64 128 256 512])
   case 1024
       set(gca,'XTick',[64 128 256 512 1024])
       %set(gca,'XTick',[2 4 8 16 32 64 128 256 512 1024])
       %set(gca,'YTick',[2 4 8 16 32 64 128 256 512 1024])
   case 2048
       set(gca,'XTick',[64 128 256 512 1024 2048])
       %set(gca,'XTick',[2 4 8 16 32 64 128 256 512 1024 2048])
       %set(gca,'YTick',[2 4 8 16 32 64 128 256 512 1024 2048])
       %set(gca,'YTick',[64 128 256 512 1024 2048])
end

box(gca,'on');

xlim([0 Number_Processors{Number_Case}+50]);
% ylim([0 Number_Processors{Number_Case}+10]);

%set(gca,'XTickLabel',{'2', '4', '8', '16', '32', '64', '128'})

%ylabel('$\overline{<\sf{NSig}>}$','interpreter','latex');
%ylabel('Speed Up      based on      CPU_T_i_m_e   for    Time Integration');
xlabel('Number of Processors');
switch Fig_Number
    case 1
        ylabel('$\overline{{\sf{min}}({\sf{N}}_{{\sf SA}_{\sf \; I}})}, \hspace{0.2cm} \overline{ \langle {\sf{N}}_{{\sf SA}_{\sf \; I}} \rangle}, \hspace{0.2cm} \overline{{\sf{max}}({\sf{N}}_{{\sf SA}_{\sf \; I}})}$', 'Interpreter','Latex','fontsize',Font_Size);
    case 2
        ylabel('$\overline{{\sf{min}}({\sf{N}}_{{\sf SA}_{\sf \; B}})}, \hspace{0.2cm} \overline{ \langle {\sf{N}}_{{\sf SA}_{\sf \; B}} \rangle}, \hspace{0.2cm} \overline{{\sf{max}}({\sf{N}}_{{\sf SA}_{\sf \; B}})}$', 'Interpreter','Latex','fontsize',Font_Size);
    case 3
        ylabel('$\overline{{\sf{min}}({\sf{N}}_{\sf SAG})}, \hspace{0.2cm} \overline{ \langle {\sf{N}}_{\sf SAG} \rangle}, \hspace{0.2cm} \overline{{\sf{max}}({\sf{N}}_{\sf SAG})}$', 'Interpreter','Latex','fontsize',Font_Size);
end
%title([Title_Part_1 10 num2str(Resolution_Title),'^3','   \epsilon=',num2str(eps),'   J_{max}=',num2str(J_max_Title),'   \nu=',num2str(nu),'   C_f=',num2str(C_f),'']);

ylabel_hand = get(gca,'ylabel');
set(ylabel_hand,'fontsize',Font_Size);  
xlabel_hand = get(gca,'xlabel');
set(xlabel_hand,'fontsize',Font_Size_Label);  

clear NSig_mean_TimeAveraged NSig_min_TimeAveraged NSig_max_TimeAveraged

    
end    
end












end





function AddLegend(Number_Legend)
global CaseLegend
global Font_Size

switch Number_Legend
    case (2)
            LL=legend(CaseLegend{1}, CaseLegend{2}); 
    case (3)
            LL=legend(CaseLegend{1}, CaseLegend{2}, CaseLegend{3}); 
    case (4)
            LL=legend(CaseLegend{1}, CaseLegend{2}, CaseLegend{3}, CaseLegend{4}); 
    case (5)
            LL=legend(CaseLegend{1}, CaseLegend{2}, CaseLegend{3}, CaseLegend{4}, CaseLegend{5}); 
    case (6)
            LL=legend(CaseLegend{1}, CaseLegend{2}, CaseLegend{3}, CaseLegend{4}, CaseLegend{5}, CaseLegend{6}); 
    case (7)
            LL=legend(CaseLegend{1}, CaseLegend{2}, CaseLegend{3}, CaseLegend{4}, CaseLegend{5}, CaseLegend{6}, CaseLegend{7}); 
    case (8)
            LL=legend(CaseLegend{1}, CaseLegend{2}, CaseLegend{3}, CaseLegend{4}, CaseLegend{5}, CaseLegend{6}, CaseLegend{7}, CaseLegend{8}); 
    case (9)
            LL=legend(CaseLegend{1}, CaseLegend{2}, CaseLegend{3}, CaseLegend{4}, CaseLegend{5}, CaseLegend{6}, CaseLegend{7}, CaseLegend{8}, CaseLegend{9}); 
    case (10)
            LL=legend(CaseLegend{1}, CaseLegend{2}, CaseLegend{3}, CaseLegend{4}, CaseLegend{5}, CaseLegend{6}, CaseLegend{7}, CaseLegend{8}, CaseLegend{9}, CaseLegend{10} ); 
    case (11)
            LL=legend(CaseLegend{1}, CaseLegend{2}, CaseLegend{3}, CaseLegend{4}, CaseLegend{5}, CaseLegend{6}, CaseLegend{7}, CaseLegend{8}, CaseLegend{9}, CaseLegend{10}, CaseLegend{11} ); 
    case (14)
            LL=legend(CaseLegend{1}, CaseLegend{2}, CaseLegend{3}, CaseLegend{4}, CaseLegend{5}, CaseLegend{6}, CaseLegend{7}, CaseLegend{8}, CaseLegend{9}, CaseLegend{10}, CaseLegend{11}, CaseLegend{12}, CaseLegend{13}, CaseLegend{14} ); 
    case (22)
            LL=legend(CaseLegend{1}, CaseLegend{2}, CaseLegend{3}, CaseLegend{4}, CaseLegend{5}, CaseLegend{6}, CaseLegend{7}, CaseLegend{8}, CaseLegend{9}, CaseLegend{10}, CaseLegend{11}, CaseLegend{12}, CaseLegend{13}, CaseLegend{14}, CaseLegend{15}, CaseLegend{16}, CaseLegend{17}, CaseLegend{18}, CaseLegend{19}, CaseLegend{20}, CaseLegend{21}, CaseLegend{22} ); 
    otherwise
            disp('Unknown Legend Number')
end

%legend('Variable \epsilon','Constant \epsilon');
%legend('\fontsize{16} {\epsilon}       \fontsize{11} Np=Nu=2','\fontsize{16} {|| f ||}_{\infty}       \fontsize{11} Np=Nu=2', '\fontsize{16} {\epsilon}       \fontsize{11} Np=3 Nu=2','\fontsize{16} {|| f ||}_{\infty}       \fontsize{11} Np=3 Nu=2',  '\fontsize{16} {\epsilon}       \fontsize{11} Np=4 Nu=4','\fontsize{16} {|| f ||}_{\infty}       \fontsize{11} Np=4 Nu=4')

set(LL, 'Box','off');
% set(LL, 'FontName','Times New-Roman');
set(LL, 'FontSize',Font_Size); 
set(LL, 'Location','SouthEast');   %NorthWest  SouthEast  SouthWest   NorthEast


end
