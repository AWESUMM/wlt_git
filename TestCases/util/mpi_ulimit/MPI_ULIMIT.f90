      program ulimit
      include 'mpif.h'

      integer irecv(4), rank, nprocessors, isend, root
      integer DEBUG, EVENT, HDR

      call MPI_INIT(ierr)
      call MPI_COMM_RANK(MPI_COMM_WORLD, rank, ierr)
      call MPI_COMM_SIZE(MPI_COMM_WORLD, nprocessors, ierr)

      root = 0

      print *,"From processor ",rank
      call system('ulimit -s')

      call MPI_FINALIZE(ierr)

      end

