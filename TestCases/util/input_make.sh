#!/bin/sh
# Script to generate input files for the test cases
# from a single input file.
# The rules are in the input file for that script [in.txt]
#-----------------------------------------------------------------

TEST_NUMBER=0
TEST_OK=0

echo -e "\tGenerating input files for the tests:"

#-----------------------------------------------------------------
function usage()
{
	echo "Usage: make_input input_file"
	echo " input_file is a text file which contains the"
	echo " rules to generate the input files for the tests"
	exit 1
}
function read_line()
{
	if [ "$1" = "#" ]; then				# skip comments
		return
	elif [ "$1" = "TEST_DIR" ]; then		# create directory
		TEST_NUMBER=`expr $TEST_NUMBER + 1`	# count the tests
		TEST_DIR=$2
		mkdir -p $TEST_DIR
		if [ $? -ne 0 ]; then
			echo "Error: test $TEST_NUMBER skipped"
		else
			echo "TEST $TEST_NUMBER"
			TEST_OK=`expr $TEST_OK + 1`
		fi
	elif [ "$1" = "BASE_INPUT" ]; then
		BASE_INPUT=$2
		if [ ! -f $BASE_INPUT ]; then
			echo "Error: file not found: $BASE_INPUT"
		else
			TEST_OK=`expr $TEST_OK + 1`
		fi
	elif [ "$1" = "OUT_INPUT" ]; then
		OUT_INPUT=$TEST_DIR/$2
		if [ ! `cat $BASE_INPUT > $OUT_INPUT` ]; then
			TEST_OK=3
		else
			echo "ERROR: writing $OUT_INPUT"
		fi
	elif [ "$1" = "-" -o "$1" = "+" ]; then
		if [ $TEST_OK -ge 3 ]; then             # process the rule
			TEST_OK=`expr $TEST_OK + 1`

			sed -e "s/$2 /#$2 /g" -e "s/$2=/#$2=/g" $OUT_INPUT > $OUT_INPUT.tmp
			if [ "$1" = "+" ]; then
				shift
				echo -e "$* \t \t #automatically added" >> $OUT_INPUT.tmp
			fi
			mv -f $OUT_INPUT.tmp $OUT_INPUT
		else
			echo "rule skipped, globals have not been defined"
		fi
	else                                            # skip all other lines
		return
	fi
}
#-----------------------------------------------------------------

if [ $# -ne 1 ]; then					# test rule file name presence
	usage
fi

if [ ! -f $1 ]; then					# test rule file presence
	echo "Error: file not found: $1"
	exit 1
fi

cat $1 | while read line; do				# read rule file
	read_line $line
done
exit 0
