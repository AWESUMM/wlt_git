function ComputationalComplexity

Reynolds    = [70  120 190  320];
Resolution  = [256 512 1024 2048];
J_max       = [6   7    8   9];

CVS_nwlt    = [1100000 5000000 20000000 90000000];
CVS_active    = CVS_nwlt    ./ Resolution.^3.0 * 100;

SCALES_nwlt(1) =  115000;
SCALES_nwlt(2:4) = SCALES_nwlt(1) * CVS_active(1)./CVS_active(2:4);
SCALES_nwlt(2) =  437000;
SCALES_nwlt(3) = 1000000;
SCALES_nwlt(4) = 1850000;

SCALES_AdpEps_nwlt(1) =   116000;
SCALES_AdpEps_nwlt(2) =  1172000;

SCALES_AdpEpsSpa_nwlt(1) =   107000;
SCALES_AdpEpsSpa_nwlt(2) =   972000;
SCALES_AdpEpsSpa_nwlt(3) =  6120795; %5716053;
SCALES_AdpEpsSpa_nwlt(4) = 23242382; %21999282; %20000000; %18949695; %21177964;

SCALES_active           = SCALES_nwlt           ./ Resolution.^3.0 * 100;
SCALES_AdpEpsSpa_active = SCALES_AdpEpsSpa_nwlt ./ Resolution.^3.0 * 100;
% SCALES_active = [0.00673585943096896       0.00320900670063545      0.000811817564713218     0.000181891887491074]*100;

DNS     = (256.^3)*(Reynolds/70) .^ (9.0/2.0);
WDNS_2D = Reynolds .^ (7.0/10.0);

WDNS_2D_ReT     = [2.8e3  5.5e3   1.5e4  2.4e4  4e4  7e4];
ReT_WDNS_2D_ReT = [138    195     275    389    551  779];

CVS_Slope    = Reynolds .^ 3.25; %(13.0/4.0)  (6.5/2.0);
SCALES_Slope = Reynolds .^ 2.75; %(11.0/4.0)  (5.5/2.0);

SCALES_AdpEps_Slope       = Reynolds .^ 3.75; %(11.0/4.0)  (5.5/2.0);
SCALES_AdpEpsSpa_Slope    = Reynolds .^ (2.75*(0.0007*(Reynolds-Reynolds(1))+1)); %(11.0/4.0)  (5.5/2.0);

FSGSDMean = [ 0.324579457948068         0.473883133184279               0.617932525         0.758042277150274];
FSGSDMean = [ 0.323177663542739         0.475869061686194         0.594733100644382         0.745060358062536];







figure
hold on;    

loglog(Reynolds, SCALES_nwlt,           '-ob');
%loglog(Reynolds(1:2), SCALES_AdpEps_nwlt(1:2),    '-db');
%loglog(Reynolds(1:4), SCALES_AdpEpsSpa_nwlt(1:4), '-pb');
loglog(Reynolds, CVS_nwlt,     '-sr');
%loglog(Reynolds, WDNS_2D,      '-<m');
loglog(ReT_WDNS_2D_ReT, WDNS_2D_ReT, '-<m');
loglog(Reynolds, DNS,          '-^k');
loglog(Reynolds, CVS_Slope,    '--r');
loglog(Reynolds, SCALES_Slope, '--b');
% loglog(Reynolds, SCALES_AdpEps_Slope,    '--b');
%loglog(Reynolds, SCALES_AdpEpsSpa_Slope, '--b');

xlim([50 500]);
%ylim([10^5 3*10^10])

set(gca,'XTick',      Reynolds)
% set(gca,'XTickLabel', {'70', '120', '190', '320'})   

Font_Size = 16;
xlabel('Taylor Microscale Reynolds number', 'FontSize', Font_Size);
ylabel('Number of Points',                  'FontSize', Font_Size);


set(gca,'XScale','log','YScale','log')
box(gca,'on'); 

%title([Title_Part_1 10 num2str(Resolution_Title),'^3','   \epsilon=',num2str(eps),'   J_{max}=',num2str(J_max_Title),'   \nu=',num2str(nu),'   C_f=',num2str(C_f),'   Re=',num2str(Reynolds),'']);

%LL=legend('SCALES', 'SCALES Time Varying \epsilon', 'SCALES Spatially Adaptive \epsilon', 'CVS', 'WDNS 2D', 'DNS'); 
LL=legend('SCALES', 'CVS', 'WDNS 2D', 'DNS'); 
set(LL, 'Box','off'); %'FontName','Times New-Roman'
set(LL, 'FontSize',Font_Size-4); 
set(LL, 'Location','NorthWest');


%text(Number_Processors{i}+0.5, SppedUp{i},{'    ';num2str(SppedUp{i})},'HorizontalAlignment','left');
text(Reynolds(4)+9,  DNS(4)-100,           {'    ';'Re_{\lambda}^{9/2}'}, 'HorizontalAlignment','left', 'Color','k', 'FontSize',Font_Size-4);
text(Reynolds(2)-15, WDNS_2D_ReT(1)-10,    {'    ';'Re_{\lambda}^{7/10}'},'HorizontalAlignment','left', 'Color','m', 'FontSize',Font_Size-4);

text(Reynolds(4)+9,  CVS_Slope(4)+1000,    {'    ';'Re_{\lambda}^{3.25}'}, 'HorizontalAlignment','left', 'Color','r', 'FontSize',Font_Size-4);
text(Reynolds(4)+9,  SCALES_Slope(4)+1000, {'    ';'Re_{\lambda}^{2.75}'}, 'HorizontalAlignment','left', 'Color','b', 'FontSize',Font_Size-4);

%text(Reynolds(4)+9, SCALES_AdpEpsSpa_Slope(4)+1000, {'    ';'Re_{\lambda}^{2.75 x (0.0007(Re_{\lambda}-70)+1)}'}, 'HorizontalAlignment','left', 'Color','b', 'FontSize',Font_Size-4);







figure
hold on;    

loglog(Reynolds, SCALES_nwlt,           '-ob');
%loglog(Reynolds(1:2), SCALES_AdpEps_nwlt(1:2),    '-db');
%loglog(Reynolds(1:4), SCALES_AdpEpsSpa_nwlt(1:4), '-pb');
loglog(Reynolds, CVS_nwlt,     '-sr');
%loglog(Reynolds, WDNS_2D,      '-<m');
%loglog(ReT_WDNS_2D_ReT, WDNS_2D_ReT, '-<m');
loglog(Reynolds, DNS,          '-^k');
loglog(Reynolds, CVS_Slope,    '--r');
loglog(Reynolds, SCALES_Slope, '--b');
% loglog(Reynolds, SCALES_AdpEps_Slope,    '--b');
%loglog(Reynolds, SCALES_AdpEpsSpa_Slope, '--b');

xlim([50 500]);
%ylim([10^5 3*10^10])

set(gca,'XTick',      Reynolds)
% set(gca,'XTickLabel', {'70', '120', '190', '320'})   

Font_Size = 16;
xlabel('Taylor Microscale Reynolds number', 'FontSize', Font_Size);
ylabel('Number of Points',                  'FontSize', Font_Size);


set(gca,'XScale','log','YScale','log')
box(gca,'on'); 

%title([Title_Part_1 10 num2str(Resolution_Title),'^3','   \epsilon=',num2str(eps),'   J_{max}=',num2str(J_max_Title),'   \nu=',num2str(nu),'   C_f=',num2str(C_f),'   Re=',num2str(Reynolds),'']);

%LL=legend('SCALES', 'SCALES Time Varying \epsilon', 'SCALES Spatially Adaptive \epsilon', 'CVS', 'WDNS 2D', 'DNS'); 
LL=legend('SCALES', 'CVS', 'DNS'); 
set(LL, 'Box','off'); %'FontName','Times New-Roman'
set(LL, 'FontSize',Font_Size-4); 
set(LL, 'Location','NorthWest');


%text(Number_Processors{i}+0.5, SppedUp{i},{'    ';num2str(SppedUp{i})},'HorizontalAlignment','left');
text(Reynolds(4)+9,  DNS(4)-100,           {'    ';'Re_{\lambda}^{9/2}'}, 'HorizontalAlignment','left', 'Color','k', 'FontSize',Font_Size-4);
text(Reynolds(2)-15, WDNS_2D_ReT(1)-10,    {'    ';'Re_{\lambda}^{7/10}'},'HorizontalAlignment','left', 'Color','m', 'FontSize',Font_Size-4);

text(Reynolds(4)+9,  CVS_Slope(4)+1000,    {'    ';'Re_{\lambda}^{3.25}'}, 'HorizontalAlignment','left', 'Color','r', 'FontSize',Font_Size-4);
text(Reynolds(4)+9,  SCALES_Slope(4)+1000, {'    ';'Re_{\lambda}^{2.75}'}, 'HorizontalAlignment','left', 'Color','b', 'FontSize',Font_Size-4);

%text(Reynolds(4)+9, SCALES_AdpEpsSpa_Slope(4)+1000, {'    ';'Re_{\lambda}^{2.75 x (0.0007(Re_{\lambda}-70)+1)}'}, 'HorizontalAlignment','left', 'Color','b', 'FontSize',Font_Size-4);












figure
hold on;    

loglog(Reynolds, SCALES_nwlt,           '-ob');
loglog(Reynolds(1:2), SCALES_AdpEps_nwlt(1:2),    '-db');
loglog(Reynolds(1:4), SCALES_AdpEpsSpa_nwlt(1:4), '-pb');
loglog(Reynolds, CVS_nwlt,     '-sr');
%loglog(Reynolds, WDNS_2D,      '-<m');
%loglog(ReT_WDNS_2D_ReT, WDNS_2D_ReT, '-<m');
loglog(Reynolds, DNS,          '-^k');
loglog(Reynolds, CVS_Slope,    '--r');
loglog(Reynolds, SCALES_Slope, '--b');
% loglog(Reynolds, SCALES_AdpEps_Slope,    '--b');
%loglog(Reynolds, SCALES_AdpEpsSpa_Slope, '--b');

xlim([50 500]);
%ylim([10^5 3*10^10])

set(gca,'XTick',      Reynolds)
% set(gca,'XTickLabel', {'70', '120', '190', '320'})   

Font_Size = 16;
xlabel('Taylor Microscale Reynolds number', 'FontSize', Font_Size);
ylabel('Number of Points',                  'FontSize', Font_Size);


set(gca,'XScale','log','YScale','log')
box(gca,'on'); 

%title([Title_Part_1 10 num2str(Resolution_Title),'^3','   \epsilon=',num2str(eps),'   J_{max}=',num2str(J_max_Title),'   \nu=',num2str(nu),'   C_f=',num2str(C_f),'   Re=',num2str(Reynolds),'']);

LL=legend('SCALES', 'SCALES Time Varying \epsilon', 'SCALES Spatially Adaptive \epsilon', 'CVS', 'DNS'); 
% LL=legend('SCALES', 'CVS', 'WDNS 2D', 'DNS'); 
set(LL, 'Box','off'); %'FontName','Times New-Roman'
set(LL, 'FontSize',Font_Size-4); 
set(LL, 'Location','NorthWest');


%text(Number_Processors{i}+0.5, SppedUp{i},{'    ';num2str(SppedUp{i})},'HorizontalAlignment','left');
text(Reynolds(4)+9,  DNS(4)-100,           {'    ';'Re_{\lambda}^{9/2}'}, 'HorizontalAlignment','left', 'Color','k', 'FontSize',Font_Size-4);
%text(Reynolds(2)-15, WDNS_2D_ReT(1)-10,    {'    ';'Re_{\lambda}^{7/10}'},'HorizontalAlignment','left', 'Color','m', 'FontSize',Font_Size-4);

text(Reynolds(4)+9,  CVS_Slope(4)+1000,    {'    ';'Re_{\lambda}^{3.25}'}, 'HorizontalAlignment','left', 'Color','r', 'FontSize',Font_Size-4);
text(Reynolds(4)+9,  SCALES_Slope(4)+1000, {'    ';'Re_{\lambda}^{2.75}'}, 'HorizontalAlignment','left', 'Color','b', 'FontSize',Font_Size-4);

%text(Reynolds(4)+9, SCALES_AdpEpsSpa_Slope(4)+1000, {'    ';'Re_{\lambda}^{2.75 x (0.0007(Re_{\lambda}-70)+1)}'}, 'HorizontalAlignment','left', 'Color','b', 'FontSize',Font_Size-4);














figure
hold on;    

loglog(Reynolds, SCALES_nwlt,           '-ob');
%loglog(Reynolds(1:2), SCALES_AdpEps_nwlt(1:2),    '-db');
loglog(Reynolds(1:4), SCALES_AdpEpsSpa_nwlt(1:4), '-pb');
loglog(Reynolds, CVS_nwlt,     '-sr');
%loglog(Reynolds, WDNS_2D,      '-<m');
%loglog(ReT_WDNS_2D_ReT, WDNS_2D_ReT, '-<m');
loglog(Reynolds, DNS,          '-^k');
loglog(Reynolds, CVS_Slope,    '--r');
loglog(Reynolds, SCALES_Slope, '--b');
% loglog(Reynolds, SCALES_AdpEps_Slope,    '--b');
%loglog(Reynolds, SCALES_AdpEpsSpa_Slope, '--b');

xlim([50 500]);
%ylim([10^5 3*10^10])

set(gca,'XTick',      Reynolds)
% set(gca,'XTickLabel', {'70', '120', '190', '320'})   

Font_Size = 16;
xlabel('Taylor Microscale Reynolds number', 'FontSize', Font_Size);
ylabel('Number of Points',                  'FontSize', Font_Size);


set(gca,'XScale','log','YScale','log')
box(gca,'on'); 

%title([Title_Part_1 10 num2str(Resolution_Title),'^3','   \epsilon=',num2str(eps),'   J_{max}=',num2str(J_max_Title),'   \nu=',num2str(nu),'   C_f=',num2str(C_f),'   Re=',num2str(Reynolds),'']);

LL=legend('SCALES', 'SCALES Spatially Adaptive \epsilon', 'CVS', 'DNS'); 
% LL=legend('SCALES', 'CVS', 'WDNS 2D', 'DNS'); 
set(LL, 'Box','off'); %'FontName','Times New-Roman'
set(LL, 'FontSize',Font_Size-4); 
set(LL, 'Location','NorthWest');


%text(Number_Processors{i}+0.5, SppedUp{i},{'    ';num2str(SppedUp{i})},'HorizontalAlignment','left');
text(Reynolds(4)+9,  DNS(4)-100,           {'    ';'Re_{\lambda}^{9/2}'}, 'HorizontalAlignment','left', 'Color','k', 'FontSize',Font_Size-4);
%text(Reynolds(2)-15, WDNS_2D_ReT(1)-10,    {'    ';'Re_{\lambda}^{7/10}'},'HorizontalAlignment','left', 'Color','m', 'FontSize',Font_Size-4);

text(Reynolds(4)+9,  CVS_Slope(4)+1000,    {'    ';'Re_{\lambda}^{3.25}'}, 'HorizontalAlignment','left', 'Color','r', 'FontSize',Font_Size-4);
text(Reynolds(4)+9,  SCALES_Slope(4)+1000, {'    ';'Re_{\lambda}^{2.75}'}, 'HorizontalAlignment','left', 'Color','b', 'FontSize',Font_Size-4);

%text(Reynolds(4)+9, SCALES_AdpEpsSpa_Slope(4)+1000, {'    ';'Re_{\lambda}^{2.75 x (0.0007(Re_{\lambda}-70)+1)}'}, 'HorizontalAlignment','left', 'Color','b', 'FontSize',Font_Size-4);
















figure
hold on;    

loglog(Reynolds, SCALES_active,              '-ob');
%loglog(Reynolds, SCALES_AdpEpsSpa_active,    '-pb');
loglog(Reynolds, CVS_active,                 '-sr');

xlim([50 500]);
%ylim([50000 100000000])

set(gca,'XTick',      Reynolds)
% set(gca,'XTickLabel', {'70', '120', '190', '320'})   

Font_Size = 16;
xlabel('Taylor Microscale Reynolds number', 'FontSize', Font_Size);
ylabel('Percentage of Active Points',       'FontSize', Font_Size);


set(gca,'XScale','log','YScale','log')
box(gca,'on'); 

%title([Title_Part_1 10 num2str(Resolution_Title),'^3','   \epsilon=',num2str(eps),'   J_{max}=',num2str(J_max_Title),'   \nu=',num2str(nu),'   C_f=',num2str(C_f),'   Re=',num2str(Reynolds),'']);

%LL=legend('SCALES', 'SCALES Spatially Adaptive \epsilon', 'CVS'); 
LL=legend('SCALES', 'CVS'); 
set(LL, 'Box','off'); %'FontName','Times New-Roman'
set(LL, 'FontSize',Font_Size-4); 
set(LL, 'Location','SouthWest');  % NorthWest NorthEast









figure
hold on;    

loglog(Reynolds, FSGSDMean, '-ob');
%loglog(Reynolds, CVS_active,    '-sg');

xlim([50 500]);
ylim([.301 .8])

set(gca,'XTick',      Reynolds)
% set(gca,'XTickLabel', {'70', '120', '190', '320'})   

Font_Size = 16;
xlabel('Taylor Microscale Reynolds number', 'FontSize', Font_Size);
ylabel('Total  FSGSD',                      'FontSize', Font_Size);


% set(gca,'XScale','log','YScale','log')
box(gca,'on'); 

%title([Title_Part_1 10 num2str(Resolution_Title),'^3','   \epsilon=',num2str(eps),'   J_{max}=',num2str(J_max_Title),'   \nu=',num2str(nu),'   C_f=',num2str(C_f),'   Re=',num2str(Reynolds),'']);

LL=legend('SCALES'); 
set(LL, 'Box','off'); %'FontName','Times New-Roman'
set(LL, 'FontSize',Font_Size-4); 
set(LL, 'Location','NorthEast');










%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%








SCALES_AdpEpsSpa_G2_nwlt(1) =   401204;
SCALES_AdpEpsSpa_G2_nwlt(2) =  3523162;
SCALES_AdpEpsSpa_G2_nwlt(3) =  1;
SCALES_AdpEpsSpa_G2_nwlt(4) =  1; 

SCALES_AdpEpsSpa_G25_nwlt(1) =   240517;
SCALES_AdpEpsSpa_G25_nwlt(2) =  2351768; %2382158; %2412403; %2719003;
SCALES_AdpEpsSpa_G25_nwlt(3) = 11485004; %10566238;
SCALES_AdpEpsSpa_G25_nwlt(4) =  1; 

SCALES_AdpEpsSpa_G4_nwlt(1) =    63201;
SCALES_AdpEpsSpa_G4_nwlt(2) =   553772;
SCALES_AdpEpsSpa_G4_nwlt(3) =  3854323;
SCALES_AdpEpsSpa_G4_nwlt(4) =  1;

SCALES_AdpEpsSpa_G5_nwlt(1) =    37433;
SCALES_AdpEpsSpa_G5_nwlt(2) =   269490; %356277;
SCALES_AdpEpsSpa_G5_nwlt(3) =  1787306;
SCALES_AdpEpsSpa_G5_nwlt(4) =  1;

SCALES_AdpEpsSpa_G2_active  = SCALES_AdpEpsSpa_G2_nwlt  ./ Resolution.^3.0 * 100;
SCALES_AdpEpsSpa_G25_active = SCALES_AdpEpsSpa_G25_nwlt ./ Resolution.^3.0 * 100;
SCALES_AdpEpsSpa_G4_active  = SCALES_AdpEpsSpa_G4_nwlt  ./ Resolution.^3.0 * 100;
SCALES_AdpEpsSpa_G5_active  = SCALES_AdpEpsSpa_G5_nwlt  ./ Resolution.^3.0 * 100;





figure
hold on;    

loglog(Reynolds,        SCALES_nwlt,                     '-ob');
loglog(Reynolds(1:2),   SCALES_AdpEpsSpa_G2_nwlt(1:2),   '-db');
loglog(Reynolds(1:3),   SCALES_AdpEpsSpa_G25_nwlt(1:3),  '-sb');
loglog(Reynolds(1:4),   SCALES_AdpEpsSpa_nwlt(1:4),      '-pb');
loglog(Reynolds(1:3),   SCALES_AdpEpsSpa_G4_nwlt(1:3),   '->b');
loglog(Reynolds(1:3),   SCALES_AdpEpsSpa_G5_nwlt(1:3),   '-^b');
loglog(Reynolds,        CVS_nwlt,     '-sr');
%loglog(ReT_WDNS_2D_ReT, WDNS_2D_ReT,  '-<m');
loglog(Reynolds,        DNS,          '-^k');
loglog(Reynolds,        CVS_Slope,    '--r');
loglog(Reynolds,        SCALES_Slope, '--b');

xlim([50 500]);
%ylim([10^5 3*10^10])

set(gca,'XTick',      Reynolds)
% set(gca,'XTickLabel', {'70', '120', '190', '320'})   

Font_Size = 16;
xlabel('Taylor Microscale Reynolds number', 'FontSize', Font_Size);
ylabel('Number of Points',                  'FontSize', Font_Size);


set(gca,'XScale','log','YScale','log')
box(gca,'on'); 


%LL=legend('SCALES $\epsilon=0.43$', 'SCALES $\mathcal{G}=0.2$', 'SCALES $\mathcal{G}=0.25$', 'SCALES $\mathcal{G}=0.32$', 'SCALES $\mathcal{G}=0.4$', 'SCALES $\mathcal{G}=0.5$', 'CVS $\epsilon=0.2$', 'WDNS 2D', 'DNS'); 
LL=legend('SCALES $\epsilon=0.43$', 'SCALES $\mathcal{G}=0.2$', 'SCALES $\mathcal{G}=0.25$', 'SCALES $\mathcal{G}=0.32$', 'SCALES $\mathcal{G}=0.4$', 'SCALES $\mathcal{G}=0.5$', 'CVS $\epsilon=0.2$', 'DNS'); 
set(LL, 'Box','off'); %'FontName','Times New-Roman'
set(LL, 'FontSize',Font_Size); 
set(LL, 'Location','NorthWest');
set(LL, 'Interpreter','Latex');


%text(Number_Processors{i}+0.5, SppedUp{i},{'    ';num2str(SppedUp{i})},'HorizontalAlignment','left');
text(Reynolds(4)+9,  DNS(4)-100,           {'    ';'Re_{\lambda}^{9/2}'}, 'HorizontalAlignment','left', 'Color','k', 'FontSize',Font_Size-4);
%text(Reynolds(2)-15, WDNS_2D_ReT(1)-10,    {'    ';'Re_{\lambda}^{7/10}'},'HorizontalAlignment','left', 'Color','m', 'FontSize',Font_Size-4);

text(Reynolds(4)+9,  CVS_Slope(4)+1000,    {'    ';'Re_{\lambda}^{3.25}'}, 'HorizontalAlignment','left', 'Color','r', 'FontSize',Font_Size-4);
text(Reynolds(4)+9,  SCALES_Slope(4)+1000, {'    ';'Re_{\lambda}^{2.75}'}, 'HorizontalAlignment','left', 'Color','b', 'FontSize',Font_Size-4);

%text(Reynolds(4)+9, SCALES_AdpEpsSpa_Slope(4)+1000, {'    ';'Re_{\lambda}^{2.75 x (0.0007(Re_{\lambda}-70)+1)}'}, 'HorizontalAlignment','left', 'Color','b', 'FontSize',Font_Size-4);














figure
hold on;    

plot(Reynolds,        SCALES_nwlt,                     '-ob');
plot(Reynolds(1:2),   SCALES_AdpEpsSpa_G2_nwlt(1:2),   '-db');
plot(Reynolds(1:3),   SCALES_AdpEpsSpa_G25_nwlt(1:3),  '-sb');
plot(Reynolds(1:4),   SCALES_AdpEpsSpa_nwlt(1:4),      '-pb');
plot(Reynolds(1:3),   SCALES_AdpEpsSpa_G4_nwlt(1:3),   '->b');
plot(Reynolds(1:3),   SCALES_AdpEpsSpa_G5_nwlt(1:3),   '-^b');
plot(Reynolds,        SCALES_Slope, '--b');

xlim([50 500]);
%ylim([10^5 3*10^10])

set(gca,'XTick',      Reynolds)
% set(gca,'XTickLabel', {'70', '120', '190', '320'})   

Font_Size = 16;
xlabel('Taylor Microscale Reynolds number', 'FontSize', Font_Size);
ylabel('Number of Points',                  'FontSize', Font_Size);


set(gca,'XScale','linear','YScale','linear')
box(gca,'on'); 


LL=legend('SCALES $\epsilon=0.43$', 'SCALES $\mathcal{G}=0.2$', 'SCALES $\mathcal{G}=0.25$', 'SCALES $\mathcal{G}=0.32$', 'SCALES $\mathcal{G}=0.4$', 'SCALES $\mathcal{G}=0.5$'); 
set(LL, 'Box','off'); %'FontName','Times New-Roman'
set(LL, 'FontSize',Font_Size-4); 
set(LL, 'Location','NorthWest');
set(LL, 'Interpreter','Latex');

text(Reynolds(4)+9,  SCALES_Slope(4)+1000, {'    ';'Re_{\lambda}^{2.75}'}, 'HorizontalAlignment','left', 'Color','b', 'FontSize',Font_Size-4);


















figure
hold on;    



loglog(Reynolds,        SCALES_active,                     '-ob');
loglog(Reynolds(1:2),   SCALES_AdpEpsSpa_G2_active(1:2),   '-db');
loglog(Reynolds(1:3),   SCALES_AdpEpsSpa_G25_active(1:3),  '-sb');
loglog(Reynolds(1:4),   SCALES_AdpEpsSpa_active(1:4),      '-pb');
loglog(Reynolds(1:3),   SCALES_AdpEpsSpa_G4_active(1:3),   '->b');
loglog(Reynolds(1:3),   SCALES_AdpEpsSpa_G5_active(1:3),   '-^b');
loglog(Reynolds,        CVS_active,                        '-sr');



xlim([50 500]);
%ylim([50000 100000000])

set(gca,'XTick',      Reynolds)
% set(gca,'XTickLabel', {'70', '120', '190', '320'})   

Font_Size = 16;
xlabel('Taylor Microscale Reynolds number', 'FontSize', Font_Size);
ylabel('Percentage of Active Points',       'FontSize', Font_Size);


set(gca,'XScale','log','YScale','log')
box(gca,'on'); 

%title([Title_Part_1 10 num2str(Resolution_Title),'^3','   \epsilon=',num2str(eps),'   J_{max}=',num2str(J_max_Title),'   \nu=',num2str(nu),'   C_f=',num2str(C_f),'   Re=',num2str(Reynolds),'']);

% LL=legend('SCALES', 'CVS'); 
LL=legend('SCALES', 'SCALES Spatially Adaptive \epsilon', 'CVS'); 
LL=legend('SCALES $\epsilon=0.43$', 'SCALES $\mathcal{G}=0.2$', 'SCALES $\mathcal{G}=0.25$', 'SCALES $\mathcal{G}=0.32$', 'SCALES $\mathcal{G}=0.4$', 'SCALES $\mathcal{G}=0.5$', 'CVS $\epsilon=0.2$'); 
set(LL, 'Box','off'); %'FontName','Times New-Roman'
set(LL, 'FontSize',Font_Size); 
set(LL, 'Location','SouthWest');  % NorthWest NorthEast
set(LL, 'Interpreter','Latex');














figure
hold on;    


Slope_up  = Reynolds .^ 3.2;
Slope_low = Reynolds .^ 2.5;

%loglog(Reynolds,        SCALES_nwlt,                     '-ob');
loglog(Reynolds(1:2),   SCALES_AdpEpsSpa_G2_nwlt(1:2),   '-db');
loglog(Reynolds(1:3),   SCALES_AdpEpsSpa_G25_nwlt(1:3),  '-sb');
loglog(Reynolds(1:4),   SCALES_AdpEpsSpa_nwlt(1:4),      '-pb');
loglog(Reynolds(1:3),   SCALES_AdpEpsSpa_G4_nwlt(1:3),   '->b');
loglog(Reynolds(1:3),   SCALES_AdpEpsSpa_G5_nwlt(1:3),   '-^b');
%loglog(Reynolds,        CVS_nwlt,     '-sr');
%loglog(ReT_WDNS_2D_ReT, WDNS_2D_ReT,  '-<m');
loglog(Reynolds,        DNS,          '-^k');
loglog(Reynolds,        Slope_up,  '--r');
loglog(Reynolds,        Slope_low, '--r');

xlim([50 500]);
%ylim([10^5 3*10^10])

set(gca,'XTick',      Reynolds)
% set(gca,'XTickLabel', {'70', '120', '190', '320'})   

Font_Size = 16;
xlabel('Taylor Microscale Reynolds number', 'FontSize', Font_Size);
ylabel('Number of Points',                  'FontSize', Font_Size);


set(gca,'XScale','log','YScale','log')
box(gca,'on'); 


%LL=legend('SCALES $\epsilon=0.43$', 'SCALES $\mathcal{G}=0.2$', 'SCALES $\mathcal{G}=0.25$', 'SCALES $\mathcal{G}=0.32$', 'SCALES $\mathcal{G}=0.4$', 'SCALES $\mathcal{G}=0.5$', 'CVS $\epsilon=0.2$', 'WDNS 2D', 'DNS'); 
LL=legend('SCALES $\mathcal{G}=0.2$', 'SCALES $\mathcal{G}=0.25$', 'SCALES $\mathcal{G}=0.32$', 'SCALES $\mathcal{G}=0.4$', 'SCALES $\mathcal{G}=0.5$', 'DNS'); 
set(LL, 'Box','off'); %'FontName','Times New-Roman'
set(LL, 'FontSize',Font_Size); 
set(LL, 'Location','NorthWest');
set(LL, 'Interpreter','Latex');


%text(Number_Processors{i}+0.5, SppedUp{i},{'    ';num2str(SppedUp{i})},'HorizontalAlignment','left');
text(Reynolds(4)+9,  DNS(4)-100,           {'    ';'Re_{\lambda}^{9/2}'}, 'HorizontalAlignment','left', 'Color','k', 'FontSize',Font_Size-4);
%text(Reynolds(2)-15, WDNS_2D_ReT(1)-10,    {'    ';'Re_{\lambda}^{7/10}'},'HorizontalAlignment','left', 'Color','m', 'FontSize',Font_Size-4);

text(Reynolds(4)+9,  Slope_up(4)+1000, {'    ';'Re_{\lambda}^{3.2}'}, 'HorizontalAlignment','left', 'Color','r', 'FontSize',Font_Size-4);
text(Reynolds(4)+9,  Slope_low(4)+1000,{'    ';'Re_{\lambda}^{2.5}'}, 'HorizontalAlignment','left', 'Color','r', 'FontSize',Font_Size-4);

%text(Reynolds(4)+9, SCALES_AdpEpsSpa_Slope(4)+1000, {'    ';'Re_{\lambda}^{2.75 x (0.0007(Re_{\lambda}-70)+1)}'}, 'HorizontalAlignment','left', 'Color','b', 'FontSize',Font_Size-4);










end