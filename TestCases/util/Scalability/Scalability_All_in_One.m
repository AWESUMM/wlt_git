%close all;
clear all;

LineColorMarker  =  {'sk--'; '^r--'; 'ob-.';  '>g- '; '<m: '; '^c--'; 'or-.'; 'sg-.'; 'or'; 'xk'; 'c'; 'm'; 'k'; 'r'; 'xg'; 'xm'; 'ob'; '^c'; '<m'; '>y'; 'g'; 'b-.'  };
LineWidth        =  {0.5;    0.5;    0.5;     0.5;    0.5;    0.5;    0.5;    0.5;    0.5;  0.5; 3.0; 3.0; 3.0; 3.0; 0.5;  0.5;  0.5;  0.5;  0.5;  0.5;  2.0; 0.5    };

TextColor        =  {'k'; 'r'; 'b';  'g'; 'm'; 'c'; 'r'; 'g'; 'r'; 'k'; 'c'; 'm'; 'k'; 'r'; 'xg'; 'xm'; 'ob'; '^c'; '<m'; '>y'; 'g'; 'b-.'  };

case_Dim = 3;
Font_Size = 16;
Font_Size_Label = Font_Size-2;
File_Type_Number = 1;             





%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%256   d1 & d3
Number_Processors = {4,4,8,8,16,16,128,128,256,256};
SpeedUp={4,4.87160407863269,8.65535259864340,9.38816104514814,15.3205490507138,16.5885620064219,56.7896266810146,52.8313185619995,31.1922743027225,48.9755621385723};
CaseLegend={'d1      4 cpu','d3      4 cpu','d1      8 cpu','d3      8 cpu','d1    16 cpu','d3    16 cpu','d1  128 cpu','d3  128 cpu','d1  256 cpu','d3  256 cpu'};





%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
figure
hold on;

for i = 1:5:6
    clear Number_Processors SpeedUp CaseLegend
    
    switch (i)
        case (1)
            %256  d3
            Number_Processors = {4,8,16,128,256};
            SpeedUp={4,7.70847621737201,13.6206159110351,43.3789919782049,40.2130890343767};
            CaseLegend={'d3      4 cpu','d3      8 cpu','d3    16 cpu','d3  128 cpu','d3  256 cpu'};
        case (6)
            %256  d1
            Number_Processors = {4,8,16,128,256};
            SpeedUp={4,8.65535259864340,15.3205490507138,56.7896266810146,31.1922743027225};
            CaseLegend={'d1      4 cpu','d1      8 cpu','d1    16 cpu','d1  128 cpu','d1  256 cpu'};
    end


    plot( [Number_Processors{1:end}], [SpeedUp{1:end}], LineColorMarker{i}, 'LineWidth', LineWidth{i} ); 
    for k = 1:length(Number_Processors)   
        text( Number_Processors{k}+0.5, SpeedUp{k},{'    ';num2str(SpeedUp{k})},'HorizontalAlignment','left', 'Color',TextColor{i});
    end
end


LL=legend('256^3  Zoltan HyperGraph','256^3  Geometric');
set(LL, 'Box','off'); %'FontName','Times New-Roman'
set(LL, 'FontSize',Font_Size-4); 
set(LL, 'Location','NorthWest');

ideal_x = [1.9  max(cell2mat (Number_Processors))];
ideal_y = [0.9 max(cell2mat (Number_Processors))];
plot( ideal_x, ideal_y, '--b')

set(gca,'XTick',[4 8 16 128 256])
set(gca,'YTick',[4 8 16 128 256])

xlim([0 Number_Processors{end}+10]);
ylim([0 Number_Processors{end}+10]);

ylabel('Speed Up');
xlabel('Number of Processors');
box(gca,'on');

ylabel_hand = get(gca,'ylabel');
set(ylabel_hand,'fontsize',Font_Size_Label);  
xlabel_hand = get(gca,'xlabel');
set(xlabel_hand,'fontsize',Font_Size_Label);  







%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
figure
hold on;

for i = 1:4
    clear Number_Processors SpeedUp CaseLegend
    
    switch (i)
        case (1)
            %256  d3
            Number_Processors = {4,8,16,128,256};
            SpeedUp={4,7.70847621737201,13.6206159110351,43.3789919782049,40.2130890343767};
            CaseLegend={'d3      4 cpu','d3      8 cpu','d3    16 cpu','d3  128 cpu','d3  256 cpu'};
        case (2)
            %512
            Number_Processors={32,64,128,256};
            SpeedUp={32,87.2398190045249,128.319467554077,214.818941504178};
        case (3)
            %1024
            Number_Processors={64,128,256,512,1024};
            SpeedUp={64,122.322041511781,203.492981518494,307.680135703884,356.248851552413};
        case (4)
            %2048
            Number_Processors={64,128,256,512,1024,2048};
            SpeedUp={64,114.153846153846,210.123893805310,364.731182795699,371,15.9355704697987};
    end


    plot( [Number_Processors{1:end}], [SpeedUp{1:end}], LineColorMarker{i}, 'LineWidth', LineWidth{i} ); 
    for k = 1:length(Number_Processors)   
        text( Number_Processors{k}+0.5, SpeedUp{k},{'    ';num2str(SpeedUp{k})},'HorizontalAlignment','left', 'Color',TextColor{i});
    end
end


LL=legend('  256^3','  512^3','1024^3','2048^3');
set(LL, 'Box','off'); %'FontName','Times New-Roman'
set(LL, 'FontSize',Font_Size); 
set(LL, 'Location','NorthWest');

ideal_x = [1.9  max(cell2mat (Number_Processors))];
ideal_y = [0.9 max(cell2mat (Number_Processors))];
plot( ideal_x, ideal_y, '--b')

set(gca,'XTick',[4 8 16 32 64 128 256 512 1024 2048])
set(gca,'YTick',[4 8 16 32 64 128 256 512 1024 2048])

xlim([0 Number_Processors{end}+10]);
ylim([0 Number_Processors{end}+10]);

ylabel('Speed Up');
xlabel('Number of Processors');
box(gca,'on');

ylabel_hand = get(gca,'ylabel');
set(ylabel_hand,'fontsize',Font_Size_Label);  
xlabel_hand = get(gca,'xlabel');
set(xlabel_hand,'fontsize',Font_Size_Label);  







%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
figure ('FileName', '256_512_1024_2048.fig')
hold on;
set(gcf,'Position', [34,65,1280,662]); 

for i = 1:5
    clear Number_Processors SpeedUp CaseLegend
    
    switch (i)
        case (1)
            %256  d3
            Number_Processors = {4,8,16,128,256};
            SpeedUp={4,7.70847621737201,13.6206159110351,43.3789919782049,40.2130890343767};
            CaseLegend={'d3      4 cpu','d3      8 cpu','d3    16 cpu','d3  128 cpu','d3  256 cpu'};
        case (2)
            %512
            Number_Processors={32,64,128,256};
            SpeedUp={32,87.2398190045249,128.319467554077,214.818941504178};
        case (3)
            %1024
            Number_Processors={64,128,256,512,1024};
            SpeedUp={64,122.322041511781,203.492981518494,307.680135703884,356.248851552413};
        case (4)
            %2048
            Number_Processors={64,128,256,512,1024,2048};
            SpeedUp={64,114.153846153846,210.123893805310,364.731182795699,371,15.9355704697987};
        case (5)
            %2048  RKL1
            Number_Processors={64,128,256,512,1024,2048};
            SpeedUp={64,112.995215311005,205.356521739130,362.764976958525,531.891891891892,411.428571428571};
    end


    plot( [Number_Processors{1:end}], [SpeedUp{1:end}], LineColorMarker{i}, 'LineWidth', LineWidth{i} ); 
    %for k = 1:length(Number_Processors)   
    %    text( Number_Processors{k}+0.5, SpeedUp{k},{'    ';num2str(SpeedUp{k})},'HorizontalAlignment','left', 'Color',TextColor{i});
    %end
end


LL=legend('  256^3','  512^3','1024^3','2048^3  AlltoAll','2048^3  OnetoOne');
set(LL, 'Box','off'); %'FontName','Times New-Roman'
set(LL, 'FontSize',Font_Size); 
set(LL, 'Location','NorthWest');

%ideal_x = [1.9  max(cell2mat (Number_Processors))];
%ideal_y = [0.9 max(cell2mat (Number_Processors))];
%plot( ideal_x, ideal_y, '--b')
loglog([1 2 4 8 16 32 64 128 256 512 1024 2048],[1 2 4 8 16 32 64 128 256 512 1024 2048], '--b');

set(gca,'XTick',[4 8 16 32 64 128 256 512 1024 2048])
set(gca,'YTick',[4 8 16 32 64 128 256 512 1024 2048])

set(gca,'XScale','log','YScale','log')

xlim([0 Number_Processors{end}+10]);
ylim([0 Number_Processors{end}+10]);

ylabel('Speed Up');
xlabel('Number of Processors');
box(gca,'on');

ylabel_hand = get(gca,'ylabel');
set(ylabel_hand,'fontsize',Font_Size_Label);  
xlabel_hand = get(gca,'xlabel');
set(xlabel_hand,'fontsize',Font_Size_Label);  









%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
figure ('FileName', 'TheoreticalEfficiency.fig')
hold on;
set(gcf,'Position', [34,65,1280,662]); 
%scrsz = get(0,'ScreenSize');
%figure('Position',[10 scrsz(4)/2 scrsz(3)/2 scrsz(4)/2])
%figure('Position',[10 10 800 800])
%figure('units','normlized','position',[.1 .1 .4 .4])

for i = 4:5
    clear Number_Processors SpeedUp CaseLegend
    
    switch (i)
        case (4)
            %2048
            Number_Processors={64,128,256,512,1024,2048};
            SpeedUp={64,114.153846153846,210.123893805310,364.731182795699,371,15.9355704697987};
        case (5)
            %2048  RKL1
            Number_Processors={64,128,256,512,1024,2048};
            SpeedUp={64,112.995215311005,205.356521739130,362.764976958525,531.891891891892,411.428571428571};
    end


    plot( [Number_Processors{1:end}], [SpeedUp{1:end}], LineColorMarker{i}, 'LineWidth', LineWidth{i} ); 
    for k = 1:length(Number_Processors)   
        text( Number_Processors{k}+0.5, SpeedUp{k},{'    ';num2str(SpeedUp{k})},'HorizontalAlignment','left', 'Color',TextColor{i});
    end
end


set(gca,'XTick',[4  64 128 256 512 1024 2048])
set(gca,'YTick',[4  64 128 256 512 1024 2048])

xlim([0 Number_Processors{end}+10]);
ylim([0 Number_Processors{end}+10]);

ylabel('Speed Up');
xlabel('Number of Processors');
box(gca,'on');

ylabel_hand = get(gca,'ylabel');
set(ylabel_hand,'fontsize',Font_Size_Label);  
xlabel_hand = get(gca,'xlabel');
set(xlabel_hand,'fontsize',Font_Size_Label);  


theoretical_x = cell2mat (Number_Processors);
% theoretical_y = theoretical_x ./ ( 1. + (theoretical_x/(2048^3)).^(1./3.) );
% plot( theoretical_x, theoretical_y, '--pk');


NGB_NGI_2048 = [ 0.2526  0.3317  0.4494  0.5963  0.7844  0.9955 ];     % N_G_B / N_G_I  for 2048^3 on  64,128,256,512,1024,2048 cpu
theoretical_with_ActualNGBNGI = theoretical_x ./ ( 1. + NGB_NGI_2048 );
plot( theoretical_x, theoretical_with_ActualNGBNGI, '--pb');




ideal_x = [1.9  max(cell2mat (Number_Processors))];
ideal_y = [0.9 max(cell2mat (Number_Processors))];
plot( ideal_x, ideal_y, '--b')


LL=legend('2048^3  AlltoAll','2048^3  OnetoOne','Theoretical Efficiency');
set(LL, 'Box','off'); %'FontName','Times New-Roman'
set(LL, 'FontSize',Font_Size); 
set(LL, 'Location','NorthWest');









%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
figure
hold on;


NGB_NGI_2048 = [ 0.2526  0.3317  0.4494  0.5963  0.7844  0.9955 ];     % N_G_B / N_G_I  for 2048^3 on  64,128,256,512,1024,2048 cpu


theoretical_x = cell2mat (Number_Processors);
theoretical_y = theoretical_x ./ ( 1. + (theoretical_x/(2048^3)).^(1./3.) );
plot( theoretical_x, theoretical_y, '--pk');


NGB_NGI_2048 = [ 0.2526  0.3317  0.4494  0.5963  0.7844  0.9955 ];     % N_G_B / N_G_I  for 2048^3 on  64,128,256,512,1024,2048 cpu
theoretical_with_ActualNGBNGI = theoretical_x ./ ( 1. + NGB_NGI_2048 );
plot( theoretical_x, theoretical_with_ActualNGBNGI, '--pb');


ideal_x = [1.9  max(cell2mat (Number_Processors))];
ideal_y = [0.9 max(cell2mat (Number_Processors))];
plot( ideal_x, ideal_y, '--b')


set(gca,'XTick',[4  64 128 256 512 1024 2048])
set(gca,'YTick',[4  64 128 256 512 1024 2048])

xlim([0 Number_Processors{end}+10]);
%ylim([0 Number_Processors{end}+10]);

ylabel('Speed Up');
xlabel('Number of Processors');
box(gca,'on');

ylabel_hand = get(gca,'ylabel');
set(ylabel_hand,'fontsize',Font_Size_Label);  
xlabel_hand = get(gca,'xlabel');
set(xlabel_hand,'fontsize',Font_Size_Label);  

LL=legend('$\textrm{Theoretical Efficiency  using Asymptotic} N_{{G}_{B}}/N_{{G}_{I}} = {\frac {\sf{np}}{N_I}}^{\frac{1}{3}} $', ...
          '$\textrm{Theoretical Efficiency  using Actual}     \frac{\overline{\langle N_{{G}_{B}} \rangle}} {\overline{\langle N_{{G}_{I}} \rangle}} $');
          %$\overline{<\sf{NGho}>}$
set(LL, 'Box','on'); %'FontName','Times New-Roman'
set(LL, 'FontSize',Font_Size-4); 
set(LL, 'Location','NorthEast');
set(LL, 'Interpreter','Latex');%, 'T1', 'fontenc');

