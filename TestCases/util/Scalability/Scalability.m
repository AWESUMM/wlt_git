function Scalability
%% Scalability.m
%
%% Comparing Different Parallel Runs   
%                                     on   requin.sharcnet.ca  &  tg-steele.purdue.teragrid.org   
%                                  using   Different Number of Processors   
% 
%%  

clear all;
%close all;

LineColorMarker  =  {'sk--'; '^r--'; 'ob-.';  '>g- '; '<m: '; '^c--'; 'or-.'; 'sg-.'; 'or'; 'xk'; 'c'; 'm'; 'k'; 'r'; 'xg'; 'xm'; 'ob'; '^c'; '<m'; '>y'; 'g'; 'b-.'  };
LineWidth        =  {0.5;    0.5;    0.5;     0.5;    0.5;    0.5;    0.5;    0.5;    0.5;  0.5; 3.0; 3.0; 3.0; 3.0; 0.5;  0.5;  0.5;  0.5;  0.5;  0.5;  2.0; 0.5    };
%                    1        2       3        4       5       6       7       8       9     10   11   12   13   14   15    16    17    18   19     20   21   22  

LineColorMarker_avg  =  { 'sk-';  '^r-';  'ob-';   '>g-';  '<m-';  '^c-';  'or-';  'sg-'; 'or-'; 'k'; 'c'; 'm'; 'k'; 'r'; 'xg'; 'xm'; 'ob'; '^c'; '<m'; '>y'; 'g'; 'b-.'  };
LineColorMarker_min  =  { 'k: ';  'r: ';  'b: ';   'g: ';  'm: ';  'c: ';  'r: ';  'g: ';   'r'; 'k'; 'c'; 'm'; 'k'; 'r'; 'xg'; 'xm'; 'ob'; '^c'; '<m'; '>y'; 'g'; 'b-.'  };
LineColorMarker_max  =  {  'k-';   'r-';   'b-';    'g-';   'm-';   'c-';   'r-';   'g-';   'r'; 'k'; 'c'; 'm'; 'k'; 'r'; 'xg'; 'xm'; 'ob'; '^c'; '<m'; '>y'; 'g'; 'b-.'  };

case_Dim = 3;
Font_Size = 16;
Font_Size_Label = Font_Size-2;
File_Type_Number = 1;                                      %  1:: '.p0_log'         2 ::  '.p0_user_stats';

Log_Info_Variable_1 = 4; 
%Log_Info_Variable  = 18; 
Timers_No           = 6;                                   %  3- adapt_grid   4- time advancement   5 - parallel_migrate   6 - parallel communicators
Number_of_Timers    = 8;
%Log_Info_Variable = Log_Info_Variable + Timers_No;



% '._log'                F i l e     V a r i a b l e s  ::: 
% t,  dt,  j_lev,  nwlt,  nxyz(1),  nxyz(2),  nxyz(3),  eps,  nwlt(j=1),  nwlt(j=2),  nwlt(j=3),  nwlt(j=4),  nwlt(j=5),  nwlt(j=6),  [pr=0]  NWLT,  GHO,  SEND,  RECV,  TIMER(1),  TIMER(2),  TIMER(3),  TIMER(4),  TIMER(5),  TIMER(6),  [pr=1]  NWLT,  GHO,  SEND,  RECV,  TIMER(1),  TIMER(2),  TIMER(3),  TIMER(4),  TIMER(5),  TIMER(6) 

% '.p0_user_stats'       F i l e     V a r i a b l e s  ::: 
%Time          t_eddy           Nwlt    Nwlt/Nmax     Re_taylor     eta/h         <KE>          <DISS_res>    <DISS_SGS>    fracSGS_DISS  epsilon



%  ------------------------------------
%  !   timers:  1 - inputs and initializations   / reset after IC ==>  total time integration time, CUMULATIVE
%  !            2 - adapt to initial conditions  / reset after IC ==>  main time integration loop time for each timestep
%  !            3 - adapt_grid                   / reset after IC ==>  adapt_grid, CUMULATIVE
%  !            4 - time advancement (time_adv_cn or kry_time_step) for each time step
%  !            5 - parallel_migrate             / reset after IC ==>  parallel_migrate, CUMULATIVE
%  !            6 - parallel communicators       / reset after IC ==>  parallel communicators, CUMULATIVE
%  !            7 - wlt transform in main for each time step
%  !            8 - calculate cfl for each time step
%  ------------------------------------






switch (File_Type_Number)
    case (1)
        File_Type = '.p0_log'; 
    case (2)
        File_Type = '.p0_user_stats';
end



switch (case_Dim)
    case (2)
        no_columns = 7;    % t,  dt,  j_lev,  nwlt,  nxyz(1),  nxyz(2),  eps
    case (3)
        no_columns = 8;    % t,  dt,  j_lev,  nwlt,  nxyz(1),  nxyz(2),  nxyz(3),  eps
end



Plot_LogFile       = false;
Plot_SpeedUp       = true;
do_average__TIMERS ='T';     
TIMERS_CUMULATIVE  ='F';     % NOTE ::  do_average__TIMERS ='T' && TIMERS_CUMULATIVE  ='F'   ONLY  for  non-CUMULATIVE  Timers  like  2 & 4    with    
if ( Timers_No == 5 || Timers_No == 6 )
    TIMERS_CUMULATIVE  ='T'; %          do_average__TIMERS ='T' && TIMERS_CUMULATIVE  ='T'   ONLY  for      CUMULATIVE  Timers  like  5 & 6    with     
end
Plot_MissBalance   = false;
Plot_CPUTime       = false;
Plot_TIMERS        = false;
Plot_TimeIntegration_to_Communication = false;







%% requin.sharcnet.ca[HP]     tg-steele.purdue.teragrid.org[Dell]     login-lincoln.ncsa.teragrid.org     lonestar.tacc.utexas.edu[Dell]    
Test_Number =2048.31;      % 256   256.12   512   512.1   512.6   1024   1024.3   1024.9   2048   2048.3   2048.7
                        % 256.10   512.4  :: Scalability
                        % 1024.31 2048.31  :: Scalability New: November 2011
                        % 2048.121          :: RKL1 One2One
                        % do_average__TIMERS ='T'  for  1024.31    
                        % do_average__TIMERS ='F'  for  2048.31
                      
                        % 1024.21 2048.21  :: Scalability New: December 2011   N_Diff = N_Predict = 2 
                        % 1024.11 2048.11  :: Scalability New: December 2011   N_Diff = N_Predict = 1 
                        
                        
ROOT_DIR      = 'D:\';

                        
i=1;
switch Test_Number


    case {256}

x_min = 0.0;
x_max = 2.6;

eps = 0.2;
nu  = 0.09; %0.015;
C_f = 6.66667;

Title_Part_1 = 'CVS & SCALES'; %'CVS  (( Initialized by CVS cinit-00375-eps005.0000.res (serial) ))'
J_max_Title  = 6;
Resolution_Title = 256;
Reynolds = 70;
        
DIR      = [ROOT_DIR 'T e r a__G r i d\CVS_Eps2_Cf666\Janus\Re070_Nu09\'];
CaseDIR  = 'd3_96cpu_dt2e4_smallTol123\';
CaseName = 'CVS_256M8J6_Cf666_eps2_nu09_Re70_dttol123_d3';
Case_Path_Name{i} = [DIR CaseDIR CaseName File_Type];   %.p0_user_stats
t_begin(i) = 0;
CaseLegend{i}      = ['d3  96cpu  Re=70 Janus'];
CaseLegend{i}      = ['96cpu CVS'];
Number_Processors{i} = 96;
J_max(i) = 6;
i=i+1;

DIR      = [ROOT_DIR 'T e r a__G r i d\SCALES_Eps43_Cf666\Janus\LKM_Re070_Nu09_Eps43\'];
t_begin(i) = 1.2376;
CaseDIR  = 'd3_48cpu_dt2e4_smallTol123\';
CaseName = 'SCALES_LKM_256M8J6_Cf666_eps43_nu09_Re70_dttol123_d3';
Case_Path_Name{i} = [DIR CaseDIR CaseName File_Type];   %.p0_user_stats
t_begin(i) = 1.2376;
CaseLegend{i}      = ['d3  48cpu  Re=70 Janus'];
CaseLegend{i}      = ['48cpu SCALES {\epsilon}=0.43'];
Number_Processors{i} = 48;
J_max(i) = 6;
i=i+1;

TIMERS_TYPE = 'Long';






    case {512}

x_min = 0.0;
x_max = 2.6; 

eps = 0.2;
nu  = 0.035;
C_f = 6.66667;

Title_Part_1 = 'CVS & SCALES'; %'CVS  (( Initialized by CVS cinit-00375-eps005.0000.res (serial) ))'
J_max_Title  = 7;
Resolution_Title = 512;
Reynolds = 120;

% DIR      = 'C:\Documents and Settings\Alireza\My Documents\T e r a__G r i d\CVS_Eps2_Cf666\Lonestar\Re120_Nu035\';
% CaseDIR  = 'd3_96cpu_dt2e4_smallTol123\';
% CaseName = 'CVS_512M8J7_Cf666_eps2_nu035_Re120_dttol123_d3';
% Case_Path_Name{i} = [DIR CaseDIR CaseName File_Type];   %.p0_user_stats
% t_begin(i) = 0;
% CaseLegend{i}      = ['d3  96cpu  Re=120 Lonestar'];
% CaseLegend{i}      = ['96cpu'];
% Number_Processors{i} = 96;
% J_max(i) = 7;
% i=i+1;
% 
% DIR      = 'C:\Documents and Settings\Alireza\My Documents\T e r a__G r i d\CVS_Eps2_Cf666\orca\Re120_Nu035\';
% CaseDIR  = 'd3_192cpu_dt2e4_smallTol123\';
% CaseName = 'CVS_512M8J7_Cf666_eps2_nu035_Re120_dttol123_d3';
% Case_Path_Name{i} = [DIR CaseDIR CaseName File_Type];   %.p0_user_stats
% t_begin(i) = 0;
% CaseLegend{i}      = ['d3  192cpu  Re=120 orca'];
% CaseLegend{i}      = ['192cpu'];
% Number_Processors{i} = 192;
% J_max(i) = 7;
% i=i+1;

% DIR      = 'C:\Documents and Settings\Alireza\My Documents\T e r a__G r i d\CVS_Eps2_Cf666\Steele_Purdue_Dell\Re120_Nu035\';
% CaseDIR  = 'd3_192cpu_dt2e4_smallTol123\';
% CaseName = 'CVS_512M8J7_Cf666_eps2_nu035_Re120_dttol123_d3';
% Case_Path_Name{i} = [DIR CaseDIR CaseName File_Type];   %.p0_user_stats
% t_begin(i) = 0;
% CaseLegend{i}      = ['d3  192cpu  Re=120 Steele'];
% CaseLegend{i}      = ['192cpu'];
% Number_Processors{i} = 192;
% J_max(i) = 7;
% i=i+1;

% DIR      = 'C:\Documents and Settings\Alireza\My Documents\T e r a__G r i d\CVS_Eps2_Cf666\Janus\Re120_Nu035\';
% CaseDIR  = 'd3_384cpu_dt2e4_smallTol123\';
% CaseName = 'CVS_512M8J7_Cf666_eps2_nu035_Re120_dttol123_d3';
% Case_Path_Name{i} = [DIR CaseDIR CaseName File_Type];   %.p0_user_stats
% t_begin(i) = 0;
% CaseLegend{i}      = ['d3  384cpu  Re=120 Janus'];
% CaseLegend{i}      = ['384cpu'];
% Number_Processors{i} = 384;
% J_max(i) = 7;
% i=i+1;

% DIR      = 'C:\Documents and Settings\Alireza\My Documents\T e r a__G r i d\CVS_Eps2_Cf666\Lonestar\Re120_Nu035\';
% CaseDIR  = 'd3_192cpu_dt2e4_smallTol123\';
% CaseName = 'CVS_512M8J7_Cf666_eps2_nu035_Re120_dttol123_d3';
% Case_Path_Name{i} = [DIR CaseDIR CaseName File_Type];   %.p0_user_stats
% t_begin(i) = 0;
% CaseLegend{i}      = ['d3  192cpu  Re=120 Lonestar'];
% CaseLegend{i}      = ['192cpu'];
% Number_Processors{i} = 192;
% J_max(i) = 7;
% i=i+1;

DIR      = [ROOT_DIR 'T e r a__G r i d\CVS_Eps2_Cf666\Janus\Re120_Nu035\'];
CaseDIR  = 'd3_192cpu_dt2e4_smallTol123\';
CaseName = 'CVS_512M8J7_Cf666_eps2_nu035_Re120_dttol123_d3';
Case_Path_Name{i} = [DIR CaseDIR CaseName File_Type];   %.p0_user_stats
t_begin(i) = 0;
CaseLegend{i}      = ['d3  192cpu  Re=120 Janus'];
CaseLegend{i}      = ['192cpu'];
Number_Processors{i} = 192;
J_max(i) = 7;
i=i+1;

DIR      = [ROOT_DIR 'T e r a__G r i d\SCALES_Eps43_Cf666\Janus\LKM_Re120_Nu035_Eps43\'];
CaseDIR  = 'd3_96cpu_dt2e4_smallTol123\';
CaseName = 'SCALES_LKM_512M8J7_Cf666_eps43_nu035_Re120_dttol123_d3';
Case_Path_Name{i} = [DIR CaseDIR CaseName File_Type];   %.p0_user_stats
t_begin(i) = 0.4842096;
CaseLegend{i}      = ['d3  96cpu  Re=120 Janus'];
CaseLegend{i}      = ['96cpu SCALES {\epsilon}=0.43'];
Number_Processors{i} = 96;
J_max(i) = 7;
i=i+1;


DIR      = [ROOT_DIR 'T e r a__G r i d\SCALES_Eps43_Cf666\Janus\LKM_Re120_Nu035_Eps304\'];
CaseDIR  = 'd3_96cpu_dt2e4_smallTol123\';
CaseName = 'SCALES_LKM_512M8J7_Cf666_eps304_nu035_Re120_dttol123_d3';
Case_Path_Name{i} = [DIR CaseDIR CaseName File_Type];   %.p0_user_stats
t_begin(i) = 0.4842096;
CaseLegend{i}      = ['d3  96cpu  Re=120 Janus'];
CaseLegend{i}      = ['96cpu SCALES {\epsilon}=0.304'];
Number_Processors{i} = 96;
J_max(i) = 7;
i=i+1;

LastTime   = 3.1312E-02;   %Test_Number = 4       %3.4028E-02; %3.1312E-02; %2.8995E-02; %2.1180E-02; %1.8875E-02; %1.6866E-02; %1.4432E-02; %1.2794E-02; %9.4285E-03; %8.6398E-03; %8.0457E-03;
doLastLine = true;
LastLine1  = 77;          %77  Test_Number = 4    
TIMERS_TYPE = 'Long';    






    case {1024}

x_min = 0.0;
x_max = 0.9; 

eps = 0.2;
nu  = 0.015;
C_f = 6.66667;

Title_Part_1 = 'CVS & SCALES'; %'CVS  (( Initialized by CVS cinit-00375-eps005.0000.res (serial) ))'
J_max_Title  = 8;
Resolution_Title = 1024;
Reynolds = 190;
        
DIR      = [ROOT_DIR 'T e r a__G r i d\CVS_Eps2_Cf666\'];

% CaseDIR  = 'Janus\Re190_Nu015\d3_768cpu_dt2e4_smallTol123__Intel11_Zoltan11\';
% CaseName = 'CVS_1024M8J8_Cf666_eps2_nu015_Re190_dttol123_d3';
% Case_Path_Name{i} = [DIR CaseDIR CaseName File_Type];   %.p0_user_stats
% t_begin(i) = 0;
% CaseLegend{i}      = ['d3  768cpu  Re=190 Janus'];
% CaseLegend{i}      = ['768cpu'];
% Number_Processors{i} = 768;
% J_max(i) = 8;
% i=i+1;

% CaseDIR  = 'Janus\Re190_Nu015\d3_768cpu_dt2e4__Intel11_Zoltan11\';
% CaseName = 'CVS_1024M8J8_Cf666_eps2_nu015_Re190_dt_d3';
% Case_Path_Name{i} = [DIR CaseDIR CaseName File_Type];   %.p0_user_stats
% t_begin(i) = 0;
% CaseLegend{i}      = ['d3  768cpu  Re=190 Janus Large Tol123'];
% CaseLegend{i}      = ['768cpu'];
% Number_Processors{i} = 768;
% J_max(i) = 8;
% i=i+1;

CaseDIR  = 'Janus\Re190_Nu015\d3_640cpu_dt2e4__Intel11_Zoltan11\';
CaseName = 'CVS_1024M8J8_Cf666_eps2_nu015_Re190_dt_d3';
Case_Path_Name{i} = [DIR CaseDIR CaseName File_Type];   %.p0_user_stats
t_begin(i) = 0;
CaseLegend{i}      = ['d3  640cpu  Re=190 Janus Large Tol123'];
CaseLegend{i}      = ['640cpu'];
Number_Processors{i} = 640;
J_max(i) = 8;
i=i+1;


DIR      = [ROOT_DIR 'T e r a__G r i d\SCALES_Eps43_Cf666\Janus\LKM_Re190_Nu015_Eps43\'];
CaseDIR  = 'd3_192cpu_dt2e4_smallTol123\';
CaseName = 'SCALES_LKM_1024M8J8_Cf666_eps43_nu015_Re190_dttol123_d3';
Case_Path_Name{i} = [DIR CaseDIR CaseName File_Type];   %.p0_user_stats
t_begin(i) = 2.902701E-01;
CaseLegend{i}      = ['d3  192cpu  Re=190 Janus'];
CaseLegend{i}      = ['192cpu SCALES {\epsilon}=0.43'];
Number_Processors{i} = 192;
J_max(i) = 7;
i=i+1;

LastTime   = 4.3408E-03;
doLastLine = true;
LastLine1  = 16;        %77  Test_Number = 4     %16  Test_Number = 5
TIMERS_TYPE = 'Long';






    case {2048}

x_min = 0.0;
x_max = 0.4; 

eps = 0.2;
nu  = 0.006;
C_f = 6.66667;

Title_Part_1 = 'CVS & SCALES'; %'CVS  (( Initialized by CVS cinit-00375-eps005.0000.res (serial) ))'
J_max_Title  = 9;
Resolution_Title = 2048;
Reynolds = 320;
        
% DIR      = [ROOT_DIR 'T e r a__G r i d\CVS_Eps2_Cf666\';
% CaseDIR  = 'Janus\Re320_Nu006\d3_1536cpu_dt2e4__Intel11_Zoltan11_O2O2O3\';
% CaseName = 'CVS_2048M8J9_Cf666_eps2_nu006_Re320_dt_d3';
% Case_Path_Name{i} = [DIR CaseDIR CaseName File_Type];   %.p0_user_stats
% t_begin(i) = 0;
% CaseLegend{i}      = ['d3  1536cpu  Re=320 Janus O2O2O3 ICrestartmode=1'];
% CaseLegend{i}      = ['1536cpu'];
% Number_Processors{i} = 1536;
% J_max(i) = 9;
% i=i+1;
% 
% DIR      = [ROOT_DIR 'T e r a__G r i d\CVS_Eps2_Cf666\';
% CaseDIR  = 'Janus\Re320_Nu006\d3_3072cpu_dt2e4__Intel11_Zoltan11\';
% CaseName = 'CVS_2048M8J9_Cf666_eps2_nu006_Re320_dt_d3';
% Case_Path_Name{i} = [DIR CaseDIR CaseName File_Type];   %.p0_user_stats
% t_begin(i) = 0;
% CaseLegend{i}      = ['d3  3072cpu  Re=320 Janus'];
% CaseLegend{i}      = ['3072cpu'];
% Number_Processors{i} = 3072;
% J_max(i) = 9;
% i=i+1;

DIR      = [ROOT_DIR 'T e r a__G r i d\CVS_Eps2_Cf666\'];
CaseDIR  = 'Janus\Re320_Nu006\d3_512cpu_dt2e4__Intel11_Zoltan11\';
CaseName = 'CVS_2048M8J9_Cf666_eps2_nu006_Re320_dt_d3';
Case_Path_Name{i} = [DIR CaseDIR CaseName File_Type];   %.p0_user_stats
t_begin(i) = 0;
CaseLegend{i}      = ['d3  512cpu  Re=320 Janus'];
CaseLegend{i}      = ['512cpu'];
Number_Processors{i} = 512;
J_max(i) = 9;
i=i+1;

DIR      = [ROOT_DIR 'T e r a__G r i d\SCALES_Eps43_Cf666\'];
CaseDIR  = 'Janus\LKM_Re320_Nu006_Eps43\d3_192cpu_dt2e4_smallTol123\';
CaseName = 'SCALES_LKM_2048M8J9_Cf666_eps43_nu006_Re320_dttol123_d3';
Case_Path_Name{i} = [DIR CaseDIR CaseName File_Type];   %.p0_user_stats
t_begin(i) = 0.6007612E-01;
CaseLegend{i}      = ['d3  192cpu  Re=320'];
CaseLegend{i}      = ['192cpu SCALES {\epsilon}=0.43'];
Number_Processors{i} = 192;
J_max(i) = 9;
i=i+1;

% DIR      = 'C:\Documents and Settings\Alireza\My Documents\T e r a__G r i d\SCALES_Eps43_Cf666\';
% CaseDIR  = 'Janus\LKM_Re320_Nu006_Eps43\d3_192cpu_dt2e4_smallTol123_Jmin5\';
% CaseName = 'SCALES_LKM_2048M8Jm5J9_Cf666_eps43_nu006_Re320_dttol123_d3';
% Case_Path_Name{i} = [DIR CaseDIR CaseName File_Type];   %.p0_user_stats
% t_begin(i) = 0.6007612E-01;
% CaseLegend{i}      = ['d3  192cpu  Re=320'];
% CaseLegend{i}      = ['192cpu SCALES {\epsilon}=0.43 J_{min}=5'];
% Number_Processors{i} = 192;
% J_max(i) = 9;
% i=i+1;

% DIR      = 'C:\Documents and Settings\Alireza\My Documents\T e r a__G r i d\SCALES_Eps43_Cf666\';
% CaseDIR  = 'Janus\LKM_Re320_Nu006_Eps43\d3_96cpu_dt2e4_smallTol123\';
% CaseName = 'SCALES_LKM_2048M8J9_Cf666_eps43_nu006_Re320_dttol123_d3';
% Case_Path_Name{i} = [DIR CaseDIR CaseName File_Type];   %.p0_user_stats
% t_begin(i) = 0.6007612E-01;
% CaseLegend{i}      = ['d3  96cpu  Re=320'];
% CaseLegend{i}      = ['96cpu SCALES {\epsilon}=0.43'];
% Number_Processors{i} = 96;
% J_max(i) = 9;
% i=i+1;

LastTime   = 4.3408E-03;
doLastLine = true;
LastLine1  = 16;        %77  Test_Number = 4     %16  Test_Number = 5
TIMERS_TYPE = '';    
    



    














    
    
    case {1}

x_min = 0.0;
x_max = 1.2;

eps = 0.2;
nu  = 0.015;
C_f = 6.66667;

Title_Part_1 = 'CVS'; %'CVS  (( Initialized by CVS cinit-00375-eps005.0000.res (serial) ))'
J_max_Title  = 8;
Resolution_Title = 1024;
Reynolds = 190;

DIR      = [ROOT_DIR 'T e r a__G r i d\requin.sharcnet.ca\SpeedUpTest_03__Eps2_Cf666\'];

CaseDIR  = 'Re190_Nu015\April_2010\0032cpu\';
CaseName = 'CVS_1024M8J8_eps2_nu015_Re190';      
Case_Path_Name{i} = [DIR CaseDIR 'results\' CaseName File_Type];   %.p0_user_stats
t_begin(i) = 0;
CaseLegend{i}      = ['32  cpu  Re=190'];
Number_Processors{i} = 32;
J_max(i) = 8;
i=i+1;

CaseDIR  = 'Re190_Nu015\April_2010\0064cpu\';
CaseName = 'CVS_1024M8J8_eps2_nu015_Re190';      
Case_Path_Name{i} = [DIR CaseDIR 'results\' CaseName File_Type];   %.p0_user_stats
t_begin(i) = 0;
CaseLegend{i}      = ['64  cpu  Re=190'];
Number_Processors{i} = 64;
J_max(i) = 8;
i=i+1;

CaseDIR  = 'Re190_Nu015\April_2010\0128cpu\';
CaseName = 'CVS_1024M8J8_eps2_nu015_Re190';      
Case_Path_Name{i} = [DIR CaseDIR 'results\' CaseName File_Type];   %.p0_user_stats
t_begin(i) = 0;
CaseLegend{i}      = ['128  cpu  Re=190'];
Number_Processors{i} = 128;
J_max(i) = 8;
i=i+1;

CaseDIR  = 'Re190_Nu015\April_2010\0256cpu\';
CaseName = 'CVS_1024M8J8_eps2_nu015_Re190';      
Case_Path_Name{i} = [DIR CaseDIR 'results\' CaseName File_Type];   %.p0_user_stats
t_begin(i) = 0;
CaseLegend{i}      = ['256  cpu  Re=190'];

Number_Processors{i} = 256;
J_max(i) = 8;
i=i+1;
TIMERS_TYPE = 'Long';







    case {256.10}

x_min = 0.0;
x_max = 1.2;

eps = 0.2;
nu  = 0.09; %0.015;
C_f = 6.66667;

Title_Part_1 = 'CVS'; %'CVS  (( Initialized by CVS cinit-00375-eps005.0000.res (serial) ))'
J_max_Title  = 6;
Resolution_Title = 256;
Reynolds = 70;
        
DIR      = [ROOT_DIR 'T e r a__G r i d\CVS_Eps2_Cf666\requin.sharcnet.ca\Re070_Nu09\'];

CaseDIR  = 'd1_4cpu_dt2e4_smallTol123\';
CaseName = 'CVS_256M8J6_Cf666_eps2_nu09_Re70_dttol123_d1';
Case_Path_Name{i} = [DIR CaseDIR CaseName File_Type];   %.p0_user_stats
t_begin(i) = 0;
CaseLegend{i}      = ['d1      4 cpu'];
Number_Processors{i} = 4;
J_max(i) = 6;
i=i+1;

CaseDIR  = 'd3_4cpu_dt2e4_smallTol123\';
CaseName = 'CVS_256M8J6_Cf666_eps2_nu09_Re70_dttol123_d3';
Case_Path_Name{i} = [DIR CaseDIR CaseName File_Type];   %.p0_user_stats
t_begin(i) = 0;
CaseLegend{i}      = ['d3      4 cpu'];
Number_Processors{i} = 4;  
J_max(i) = 6;
i=i+1;

CaseDIR  = 'd1_8cpu_dt2e4_smallTol123\';
CaseName = 'CVS_256M8J6_Cf666_eps2_nu09_Re70_dttol123_d1';
Case_Path_Name{i} = [DIR CaseDIR CaseName File_Type];   %.p0_user_stats
t_begin(i) = 0;
CaseLegend{i}      = ['d1      8 cpu'];
Number_Processors{i} = 8;
J_max(i) = 6;
i=i+1;

CaseDIR  = 'd3_8cpu_dt2e4_smallTol123\';
CaseName = 'CVS_256M8J6_Cf666_eps2_nu09_Re70_dttol123_d3';
Case_Path_Name{i} = [DIR CaseDIR CaseName File_Type];   %.p0_user_stats
t_begin(i) = 0;
CaseLegend{i}      = ['d3      8 cpu'];
Number_Processors{i} = 8;
J_max(i) = 6;
i=i+1;

CaseDIR  = 'd1_16cpu_dt2e4_smallTol123\';
CaseName = 'CVS_256M8J6_Cf666_eps2_nu09_Re70_dttol123_d1';
Case_Path_Name{i} = [DIR CaseDIR CaseName File_Type];   %.p0_user_stats
t_begin(i) = 0;
CaseLegend{i}      = ['d1    16 cpu'];
Number_Processors{i} = 16;
J_max(i) = 6;
i=i+1;

CaseDIR  = 'd3_16cpu_dt2e4_smallTol123\';
CaseName = 'CVS_256M8J6_Cf666_eps2_nu09_Re70_d3_dttol123';
Case_Path_Name{i} = [DIR CaseDIR CaseName File_Type];   %.p0_user_stats
t_begin(i) = 0;
CaseLegend{i}      = ['d3    16 cpu'];
Number_Processors{i} = 16;
J_max(i) = 6;
i=i+1;

CaseDIR  = 'd1_128cpu_dt2e4_smallTol123\';
CaseName = 'CVS_256M8J6_Cf666_eps2_nu09_Re70_dttol123_d1';
Case_Path_Name{i} = [DIR CaseDIR CaseName File_Type];   %.p0_user_stats
t_begin(i) = 0;
CaseLegend{i}      = ['d1  128 cpu'];
Number_Processors{i} = 128;
J_max(i) = 6;
i=i+1;

CaseDIR  = 'd3_128cpu_dt2e4_smallTol123\';
CaseName = 'CVS_256M8J6_Cf666_eps2_nu09_Re70_d3_dttol123';
Case_Path_Name{i} = [DIR CaseDIR CaseName File_Type];   %.p0_user_stats
t_begin(i) = 0;
CaseLegend{i}      = ['d3  128 cpu'];
Number_Processors{i} = 128;
J_max(i) = 6;
i=i+1;

CaseDIR  = 'd1_256cpu_dt2e4_smallTol123\';
CaseName = 'CVS_256M8J6_Cf666_eps2_nu09_Re70_dttol123_d1';
Case_Path_Name{i} = [DIR CaseDIR CaseName File_Type];   %.p0_user_stats
t_begin(i) = 0;
CaseLegend{i}      = ['d1  256 cpu'];
Number_Processors{i} = 256;
J_max(i) = 6;
i=i+1;

CaseDIR  = 'd3_256cpu_dt2e4_smallTol123\';
CaseName = 'CVS_256M8J6_Cf666_eps2_nu09_Re70_dttol123_d3';
Case_Path_Name{i} = [DIR CaseDIR CaseName File_Type];   %.p0_user_stats
t_begin(i) = 0;
CaseLegend{i}      = ['d3  256 cpu'];
Number_Processors{i} = 256;
J_max(i) = 6;
i=i+1;

LastTime   = 1.8200E-02;
doLastLine = true;        % IF    Comment-Out 4cpu    THEM     use  doLastLine = true
LastLine1  = 62;              
TIMERS_TYPE = 'Long';







    case {256.12}

x_min = 0.0;
x_max = 1.2;

eps = 0.2;
nu  = 0.09; %0.015;
C_f = 6.66667;

Title_Part_1 = 'CVS'; %'CVS  (( Initialized by CVS cinit-00375-eps005.0000.res (serial) ))'
J_max_Title  = 6;
Resolution_Title = 256;
Reynolds = 70;
        
DIR      = [ROOT_DIR 'T e r a__G r i d\CVS_Eps2_Cf666\requin.sharcnet.ca\Re070_Nu09\'];

CaseDIR  = 'd1_4cpu_dt2e4_smallTol123\';
CaseName = 'CVS_256M8J6_Cf666_eps2_nu09_Re70_dttol123_d1';
Case_Path_Name{i} = [DIR CaseDIR CaseName File_Type];   %.p0_user_stats
t_begin(i) = 0;
CaseLegend{i}      = ['d1  4cpu  Re=70'];
Number_Processors{i} = 4;
J_max(i) = 6;
i=i+1;

CaseDIR  = 'd3_4cpu_dt2e4_smallTol123\';
CaseName = 'CVS_256M8J6_Cf666_eps2_nu09_Re70_dttol123_d3';
Case_Path_Name{i} = [DIR CaseDIR CaseName File_Type];   %.p0_user_stats
t_begin(i) = 0;
CaseLegend{i}      = ['d3  4cpu  Re=70'];
Number_Processors{i} = 4;
J_max(i) = 6;
i=i+1;

CaseDIR  = 'd1_8cpu_dt2e4_smallTol123\';
CaseName = 'CVS_256M8J6_Cf666_eps2_nu09_Re70_dttol123_d1';
Case_Path_Name{i} = [DIR CaseDIR CaseName File_Type];   %.p0_user_stats
t_begin(i) = 0;
CaseLegend{i}      = ['d1  8cpu  Re=70'];
Number_Processors{i} = 8;
J_max(i) = 6;
i=i+1;

CaseDIR  = 'd3_8cpu_dt2e4_smallTol123\';
CaseName = 'CVS_256M8J6_Cf666_eps2_nu09_Re70_dttol123_d3';
Case_Path_Name{i} = [DIR CaseDIR CaseName File_Type];   %.p0_user_stats
t_begin(i) = 0;
CaseLegend{i}      = ['d3  8cpu  Re=70'];
Number_Processors{i} = 8;
J_max(i) = 6;
i=i+1;

CaseDIR  = 'd1_16cpu_dt2e4_smallTol123\';
CaseName = 'CVS_256M8J6_Cf666_eps2_nu09_Re70_dttol123_d1';
Case_Path_Name{i} = [DIR CaseDIR CaseName File_Type];   %.p0_user_stats
t_begin(i) = 0;
CaseLegend{i}      = ['d1  16cpu  Re=70'];
Number_Processors{i} = 16;
J_max(i) = 6;
i=i+1;

CaseDIR  = 'd3_16cpu_dt2e4_smallTol123\';
CaseName = 'CVS_256M8J6_Cf666_eps2_nu09_Re70_d3_dttol123';
Case_Path_Name{i} = [DIR CaseDIR CaseName File_Type];   %.p0_user_stats
t_begin(i) = 0;
CaseLegend{i}      = ['d3  16cpu  Re=70'];
Number_Processors{i} = 16;
J_max(i) = 6;
i=i+1;

CaseDIR  = 'd1_128cpu_dt2e4_smallTol123\';
CaseName = 'CVS_256M8J6_Cf666_eps2_nu09_Re70_dttol123_d1';
Case_Path_Name{i} = [DIR CaseDIR CaseName File_Type];   %.p0_user_stats
t_begin(i) = 0;
CaseLegend{i}      = ['d1  128cpu  Re=70'];
Number_Processors{i} = 128;
J_max(i) = 6;
i=i+1;

CaseDIR  = 'd3_128cpu_dt2e4_smallTol123\';
CaseName = 'CVS_256M8J6_Cf666_eps2_nu09_Re70_d3_dttol123';
Case_Path_Name{i} = [DIR CaseDIR CaseName File_Type];   %.p0_user_stats
t_begin(i) = 0;
CaseLegend{i}      = ['d3  128cpu  Re=70'];
Number_Processors{i} = 128;
J_max(i) = 6;
i=i+1;

CaseDIR  = 'd1_256cpu_dt2e4_smallTol123\';
CaseName = 'CVS_256M8J6_Cf666_eps2_nu09_Re70_dttol123_d1';
Case_Path_Name{i} = [DIR CaseDIR CaseName File_Type];   %.p0_user_stats
t_begin(i) = 0;
CaseLegend{i}      = ['d1  256cpu  Re=70'];
Number_Processors{i} = 256;
J_max(i) = 6;
i=i+1;

CaseDIR  = 'd3_256cpu_dt2e4_smallTol123\';
CaseName = 'CVS_256M8J6_Cf666_eps2_nu09_Re70_dttol123_d3';
Case_Path_Name{i} = [DIR CaseDIR CaseName File_Type];   %.p0_user_stats
t_begin(i) = 0;
CaseLegend{i}      = ['d3  256cpu  Re=70'];
Number_Processors{i} = 256;
J_max(i) = 6;
i=i+1;

DIR      = [ROOT_DIR 'T e r a__G r i d\CVS_Eps2_Cf666\Janus\Re070_Nu09\';
CaseDIR  = 'd3_64cpu_dt2e4_smallTol123\';
CaseName = 'CVS_256M8J6_Cf666_eps2_nu09_Re70_dttol123_d3';
Case_Path_Name{i} = [DIR CaseDIR CaseName File_Type];   %.p0_user_stats
t_begin(i) = 0;
CaseLegend{i}      = ['d3  64cpu  Re=70 Janus'];
Number_Processors{i} = 64;
J_max(i) = 6;
i=i+1;

CaseDIR  = 'd3_96cpu_dt2e4_smallTol123\';
CaseName = 'CVS_256M8J6_Cf666_eps2_nu09_Re70_dttol123_d3';
Case_Path_Name{i} = [DIR CaseDIR CaseName File_Type];   %.p0_user_stats
t_begin(i) = 0;
CaseLegend{i}      = ['d3  96cpu  Re=70 Janus'];
Number_Processors{i} = 96;
J_max(i) = 6;
i=i+1;

% DIR      = 'C:\Documents and Settings\Alireza\My Documents\T e r a__G r i d\CVS_Eps2_Cf666\orca\Re070_Nu09\';
% CaseDIR  = 'd3_64cpu_dt2e4_smallTol123\';
% CaseName = 'CVS_256M8J6_Cf666_eps2_nu09_Re70_dttol123_d3';
% Case_Path_Name{i} = [DIR CaseDIR CaseName File_Type];   %.p0_user_stats
% CaseLegend{i}      = ['d3  64cpu  Re=70 orca'];
% Number_Processors{i} = 64;
% J_max(i) = 6;
% i=i+1;

TIMERS_TYPE = 'Long';







    case {512.1}

x_min = 0.0;
x_max = 0.026; 

eps = 0.2;
nu  = 0.035;
C_f = 6.66667;

Title_Part_1 = 'CVS'; %'CVS  (( Initialized by CVS cinit-00375-eps005.0000.res (serial) ))'
J_max_Title  = 7;
Resolution_Title = 512;
        
DIR      = [ROOT_DIR 'T e r a__G r i d\CVS_Eps2_Cf666\requin.sharcnet.ca\Re120_Nu035\'];



CaseDIR  = 'd1_32cpu_dt2e4_smallTol123\run1\';
CaseName = 'CVS_512M8J7_Cf666_eps2_nu035_Re120_dttol123_d1';
Case_Path_Name{i} = [DIR CaseDIR CaseName File_Type];   %.p0_user_stats
t_begin(i) = 0;
CaseLegend{i}      = ['d1  32cpu  Re=120'];
Number_Processors{i} = 32;
J_max(i) = 7;
i=i+1;

CaseDIR  = 'd3_32cpu_dt2e4_smallTol123\run1\';
CaseName = 'CVS_512M8J7_Cf666_eps2_nu035_Re120_dttol123_d3';
Case_Path_Name{i} = [DIR CaseDIR CaseName File_Type];   %.p0_user_stats
t_begin(i) = 0;
CaseLegend{i}      = ['d3  32cpu  Re=120'];
Number_Processors{i} = 32;
J_max(i) = 7;
i=i+1;

TIMERS_TYPE = 'Long';







    case {512.4}

x_min = 0.0;
x_max = 8.0E-01; 

eps = 0.2;
nu  = 0.035;
C_f = 6.66667;

Title_Part_1 = 'CVS'; %'CVS  (( Initialized by CVS cinit-00375-eps005.0000.res (serial) ))'
J_max_Title  = 7;
Resolution_Title = 512;
Reynolds = 120;

DIR      = [ROOT_DIR 'T e r a__G r i d\CVS_Eps2_Cf666\requin.sharcnet.ca\Re120_Nu035\'];

CaseDIR  = 'd3_32cpu_dt2e4_smallTol123\';
CaseName = 'CVS_512M8J7_Cf666_eps2_nu035_Re120_dttol123_d3';
Case_Path_Name{i} = [DIR CaseDIR CaseName File_Type];   %.p0_user_stats
t_begin(i) = 0;
CaseLegend{i}      = ['d3  32cpu  Re=120'];
CaseLegend{i}      = ['  32 cpu'];
Number_Processors{i} = 32;
J_max(i) = 7;
i=i+1;

CaseDIR  = 'd3_64cpu_dt2e4_smallTol123\';
CaseName = 'CVS_512M8J7_Cf666_eps2_nu035_Re120_dttol123_d3';
Case_Path_Name{i} = [DIR CaseDIR CaseName File_Type];   %.p0_user_stats
t_begin(i) = 0;
CaseLegend{i}      = ['d3  64cpu  Re=120'];
CaseLegend{i}      = ['  64 cpu'];
Number_Processors{i} = 64;
J_max(i) = 7;
i=i+1;

CaseDIR  = 'd3_128cpu_dt2e4_smallTol123\';
CaseName = 'CVS_512M8J7_Cf666_eps2_nu035_Re120_dttol123_d3';
Case_Path_Name{i} = [DIR CaseDIR CaseName File_Type];   %.p0_user_stats
t_begin(i) = 0;
CaseLegend{i}      = ['d3  128cpu  Re=120'];
CaseLegend{i}      = ['128 cpu'];
Number_Processors{i} = 128;
J_max(i) = 7;
i=i+1;

CaseDIR  = 'd3_256cpu_dt2e4_smallTol123\';
CaseName = 'CVS_512M8J7_Cf666_eps2_nu035_Re120_dttol123_d3';
Case_Path_Name{i} = [DIR CaseDIR CaseName File_Type];   %.p0_user_stats
t_begin(i) = 0;
CaseLegend{i}      = ['d3  256cpu  Re=120'];
CaseLegend{i}      = ['256 cpu'];
Number_Processors{i} = 256;
J_max(i) = 7;
i=i+1;

LastTime   = 3.1312E-02;   %Test_Number = 4       %3.4028E-02; %3.1312E-02; %2.8995E-02; %2.1180E-02; %1.8875E-02; %1.6866E-02; %1.4432E-02; %1.2794E-02; %9.4285E-03; %8.6398E-03; %8.0457E-03;
doLastLine = true;
LastLine1  = 77;          %77  Test_Number = 4    
TIMERS_TYPE = 'Long';









    case {512.6}

x_min = 0.0;
x_max = 8.0E-01; 

eps = 0.2;
nu  = 0.035;
C_f = 6.66667;

Title_Part_1 = 'CVS'; %'CVS  (( Initialized by CVS cinit-00375-eps005.0000.res (serial) ))'
J_max_Title  = 7;
Resolution_Title = 512;
Reynolds = 120;

DIR      = [ROOT_DIR 'T e r a__G r i d\CVS_Eps2_Cf666\requin.sharcnet.ca\Re120_Nu035\'];

CaseDIR  = 'd3_32cpu_dt2e4_smallTol123\';
CaseName = 'CVS_512M8J7_Cf666_eps2_nu035_Re120_dttol123_d3';
Case_Path_Name{i} = [DIR CaseDIR CaseName File_Type];   %.p0_user_stats
t_begin(i) = 0;
CaseLegend{i}      = ['d3  32cpu  Re=120'];
CaseLegend{i}      = ['32cpu'];
Number_Processors{i} = 32;
J_max(i) = 7;
i=i+1;

CaseDIR  = 'd3_64cpu_dt2e4_smallTol123\';
CaseName = 'CVS_512M8J7_Cf666_eps2_nu035_Re120_dttol123_d3';
Case_Path_Name{i} = [DIR CaseDIR CaseName File_Type];   %.p0_user_stats
t_begin(i) = 0;
CaseLegend{i}      = ['d3  64cpu  Re=120'];
CaseLegend{i}      = ['64cpu'];
Number_Processors{i} = 64;
J_max(i) = 7;
i=i+1;

CaseDIR  = 'd3_128cpu_dt2e4_smallTol123\';
CaseName = 'CVS_512M8J7_Cf666_eps2_nu035_Re120_dttol123_d3';
Case_Path_Name{i} = [DIR CaseDIR CaseName File_Type];   %.p0_user_stats
t_begin(i) = 0;
CaseLegend{i}      = ['d3  128cpu  Re=120'];
CaseLegend{i}      = ['128cpu'];
Number_Processors{i} = 128;
J_max(i) = 7;
i=i+1;

CaseDIR  = 'd3_256cpu_dt2e4_smallTol123\';
CaseName = 'CVS_512M8J7_Cf666_eps2_nu035_Re120_dttol123_d3';
Case_Path_Name{i} = [DIR CaseDIR CaseName File_Type];   %.p0_user_stats
t_begin(i) = 0;
CaseLegend{i}      = ['d3  256cpu  Re=120'];
CaseLegend{i}      = ['256cpu'];
Number_Processors{i} = 256;
J_max(i) = 7;
i=i+1;

DIR      = [ROOT_DIR 'T e r a__G r i d\CVS_Eps2_Cf666\Lonestar\Re120_Nu035\'];

CaseDIR  = 'd3_64cpu_dt2e4_smallTol123\';
CaseName = 'CVS_512M8J7_Cf666_eps2_nu035_Re120_dttol123_d3';
Case_Path_Name{i} = [DIR CaseDIR CaseName File_Type];   %.p0_user_stats
t_begin(i) = 0;
CaseLegend{i}      = ['d3  64cpu  Re=120 Lonestar'];
CaseLegend{i}      = ['64cpu'];
Number_Processors{i} = 64;
J_max(i) = 7;
i=i+1;

CaseDIR  = 'd3_96cpu_dt2e4_smallTol123\';
CaseName = 'CVS_512M8J7_Cf666_eps2_nu035_Re120_dttol123_d3';
Case_Path_Name{i} = [DIR CaseDIR CaseName File_Type];   %.p0_user_stats
t_begin(i) = 0;
CaseLegend{i}      = ['d3  96cpu  Re=120 Lonestar'];
CaseLegend{i}      = ['96cpu'];
Number_Processors{i} = 96;
J_max(i) = 7;
i=i+1;

LastTime   = 3.1312E-02;   %Test_Number = 4       %3.4028E-02; %3.1312E-02; %2.8995E-02; %2.1180E-02; %1.8875E-02; %1.6866E-02; %1.4432E-02; %1.2794E-02; %9.4285E-03; %8.6398E-03; %8.0457E-03;
doLastLine = true;
LastLine1  = 77;          %77  Test_Number = 4    
TIMERS_TYPE = 'Long';






    case {1024.3}

x_min = 0.0;
x_max = 5E-02; 

eps = 0.2;
nu  = 0.015;
C_f = 6.66667;

Title_Part_1 = 'CVS'; %'CVS  (( Initialized by CVS cinit-00375-eps005.0000.res (serial) ))'
J_max_Title  = 8;
Resolution_Title = 1024;
Reynolds = 190;

DIR      = [ROOT_DIR 'T e r a__G r i d\CVS_Eps2_Cf666\requin.sharcnet.ca\Re190_Nu015\'];

CaseDIR  = 'd3_256cpu_dt2e4__O3O2O2\6hours\';
CaseName = 'CVS_1024M8J8_Cf666_eps2_nu015_Re190_dt_d3';
Case_Path_Name{i} = [DIR CaseDIR CaseName File_Type];   %.p0_user_stats
t_begin(i) = 0;
CaseLegend{i}      = ['d3  256cpu  Re=190 requin Large Tol123'];
CaseLegend{i}      = ['256cpu'];
Number_Processors{i} = 256;
J_max(i) = 8;
i=i+1;


% CaseDIR  = 'd3_512cpu_dt2e4__O3O2O2\6hours\';
% CaseName = 'CVS_1024M8J8_Cf666_eps2_nu015_Re190_dt_d3';
% Case_Path_Name{i} = [DIR CaseDIR CaseName File_Type];   %.p0_user_stats
% CaseLegend{i}      = ['d3  512cpu  Re=190 requin Large Tol123'];
% Number_Processors{i} = 512;
% J_max(i) = 8;
% i=i+1;


CaseDIR  = 'd3_1024cpu_dt2e4__O3O2O2\3hours\';
CaseName = 'CVS_1024M8J8_Cf666_eps2_nu015_Re190_dt_d3';
Case_Path_Name{i} = [DIR CaseDIR CaseName File_Type];   %.p0_user_stats
t_begin(i) = 0;
CaseLegend{i}      = ['d3  1024cpu  Re=190 requin Large Tol123'];
CaseLegend{i}      = ['1024cpu'];
Number_Processors{i} = 1024;
J_max(i) = 8;
i=i+1;

LastTime   = 1.0518E-02;
doLastLine = false;
LastLine1  = 16;        %77  Test_Number = 4     %16  Test_Number = 5
TIMERS_TYPE = 'Short';














    case {1024.31}

x_min = 0.33;
x_max = 0.36; 

eps = 0.2;
nu  = 0.015;
C_f = 6.66667;

Title_Part_1 = 'CVS'; %'CVS  (( Initialized by CVS cinit-00375-eps005.0000.res (serial) ))'
J_max_Title  = 8;
Resolution_Title = 1024;
Reynolds = 190;
        
DIR      = [ROOT_DIR 'T e r a__G r i d\CVS_Eps2_Cf666\Janus\Re190_Nu015\'];

CaseDIR  = 'Scalability_64cpu\';
CaseName = 'CVS_1024M8J8_Cf666_eps2_nu015_Re190_dt_d3';
Case_Path_Name{i} = [DIR CaseDIR CaseName File_Type];   %.p0_user_stats
t_begin(i) = 0;
CaseLegend{i}      = ['d3  64cpu  Re=190 Janus'];
CaseLegend{i}      = ['    64 cpu'];
Number_Processors{i} = 64;
J_max(i) = 8;
i=i+1;

CaseDIR  = 'Scalability_128cpu\';
CaseName = 'CVS_1024M8J8_Cf666_eps2_nu015_Re190_dt_d3';
Case_Path_Name{i} = [DIR CaseDIR CaseName File_Type];   %.p0_user_stats
t_begin(i) = 0;
CaseLegend{i}      = ['d3  128cpu  Re=190 Janus'];
CaseLegend{i}      = ['  128 cpu'];
Number_Processors{i} = 128;
J_max(i) = 8;
i=i+1;

CaseDIR  = 'Scalability_256cpu\';
CaseName = 'CVS_1024M8J8_Cf666_eps2_nu015_Re190_dt_d3';
Case_Path_Name{i} = [DIR CaseDIR CaseName File_Type];   %.p0_user_stats
t_begin(i) = 0;
CaseLegend{i}      = ['d3  256cpu  Re=190 Janus'];
CaseLegend{i}      = ['  256 cpu'];
Number_Processors{i} = 256;
J_max(i) = 8;
i=i+1;

CaseDIR  = 'Scalability_512cpu\';
CaseName = 'CVS_1024M8J8_Cf666_eps2_nu015_Re190_dt_d3';
Case_Path_Name{i} = [DIR CaseDIR CaseName File_Type];   %.p0_user_stats
t_begin(i) = 0;
CaseLegend{i}      = ['d3  512cpu  Re=190 Janus'];
CaseLegend{i}      = ['  512 cpu'];
Number_Processors{i} = 512;
J_max(i) = 8;
i=i+1;

CaseDIR  = 'Scalability_1024cpu\';
CaseName = 'CVS_1024M8J8_Cf666_eps2_nu015_Re190_dt_d3';
Case_Path_Name{i} = [DIR CaseDIR CaseName File_Type];   %.p0_user_stats
t_begin(i) = 0;
CaseLegend{i}      = ['d3  1024cpu  Re=190 Janus'];
CaseLegend{i}      = ['1024 cpu'];
Number_Processors{i} = 1024;
J_max(i) = 8;
i=i+1;

LastTime   = 1.0518E-02;
doLastLine = true;
LastLine1  = 117;        %77  Test_Number = 4     %16  Test_Number = 5
TIMERS_TYPE = 'Short';









    case {1024.9}

x_min = 0.0;
x_max = 0.2; 

eps = 0.2;
nu  = 0.015;
C_f = 6.66667;

Title_Part_1 = 'CVS'; %'CVS  (( Initialized by CVS cinit-00375-eps005.0000.res (serial) ))'
J_max_Title  = 8;
Resolution_Title = 1024;
Reynolds = 190;
        
DIR      = [ROOT_DIR 'T e r a__G r i d\CVS_Eps2_Cf666\'];



CaseDIR  = 'Steele_Purdue_Dell\Re190_Nu015\d3_128cpu_dt2e4_smallTol123\';
CaseName = 'CVS_1024M8J8_Cf666_eps2_nu015_Re190_dttol123_d3';
Case_Path_Name{i} = [DIR CaseDIR CaseName File_Type];   %.p0_user_stats
CaseLegend{i}      = ['d3  128cpu  Re=190'];
Number_Processors{i} = 128;
J_max(i) = 8;
i=i+1;

CaseDIR  = 'Steele_Purdue_Dell\Re190_Nu015\d3_256cpu_dt2e4_smallTol123\';
CaseName = 'CVS_1024M8J8_Cf666_eps2_nu015_Re190_dttol123_d3';
Case_Path_Name{i} = [DIR CaseDIR CaseName File_Type];   %.p0_user_stats
CaseLegend{i}      = ['d3  256cpu  Re=190'];
Number_Processors{i} = 256;
J_max(i) = 8;
i=i+1;

CaseDIR  = 'requin.sharcnet.ca\Re190_Nu015\d3_256cpu_dt2e4_smallTol123\';
CaseName = 'CVS_1024M8J8_Cf666_eps2_nu015_Re190_dttol123_d3';
Case_Path_Name{i} = [DIR CaseDIR CaseName File_Type];   %.p0_user_stats
CaseLegend{i}      = ['d3  256cpu  Re=190 requin'];
Number_Processors{i} = 256;
J_max(i) = 8;
i=i+1;

CaseDIR  = 'requin.sharcnet.ca\Re190_Nu015\d3_256cpu_dt2e4_smallTol123\2\';
CaseName = 'CVS_1024M8J8_Cf666_eps2_nu015_Re190_dttol123_d3';
Case_Path_Name{i} = [DIR CaseDIR CaseName File_Type];   %.p0_user_stats
CaseLegend{i}      = ['d3  256cpu  Re=190 requin 1'];
Number_Processors{i} = 256;
J_max(i) = 8;
i=i+1;

CaseDIR  = 'requin.sharcnet.ca\Re190_Nu015\d3_256cpu_dt2e4__O3O2O2\';
CaseName = 'CVS_1024M8J8_Cf666_eps2_nu015_Re190_dt_d3';
Case_Path_Name{i} = [DIR CaseDIR CaseName File_Type];   %.p0_user_stats
CaseLegend{i}      = ['d3  256cpu  Re=190 requin Large Tol123'];
Number_Processors{i} = 256;
J_max(i) = 8;
i=i+1;

CaseDIR  = 'Janus\Re190_Nu015\d3_256cpu_dt2e4_smallTol123__Intel11\';
CaseName = 'CVS_1024M8J8_Cf666_eps2_nu015_Re190_dttol123_d3';
Case_Path_Name{i} = [DIR CaseDIR CaseName File_Type];   %.p0_user_stats
CaseLegend{i}      = ['d3  256cpu  Re=190 Janus'];
Number_Processors{i} = 256;
J_max(i) = 8;
i=i+1;

CaseDIR  = 'Janus\Re190_Nu015\d3_256cpu_dt2e4_smallTol123__Intel11_Zoltan11\';
CaseName = 'CVS_1024M8J8_Cf666_eps2_nu015_Re190_dttol123_d3';
Case_Path_Name{i} = [DIR CaseDIR CaseName File_Type];   %.p0_user_stats
CaseLegend{i}      = ['d3  256cpu  Re=190 Janus'];
Number_Processors{i} = 256;
J_max(i) = 8;
i=i+1;

CaseDIR  = 'Janus\Re190_Nu015\d3_512cpu_dt2e4_smallTol123__Intel11_Zoltan11\';
CaseName = 'CVS_1024M8J8_Cf666_eps2_nu015_Re190_dttol123_d3';
Case_Path_Name{i} = [DIR CaseDIR CaseName File_Type];   %.p0_user_stats
CaseLegend{i}      = ['d3  512cpu  Re=190 Janus'];
Number_Processors{i} = 512;
J_max(i) = 8;
i=i+1;

CaseDIR  = 'Janus\Re190_Nu015\d3_768cpu_dt2e4_smallTol123__Intel11_Zoltan11\';
CaseName = 'CVS_1024M8J8_Cf666_eps2_nu015_Re190_dttol123_d3';
Case_Path_Name{i} = [DIR CaseDIR CaseName File_Type];   %.p0_user_stats
CaseLegend{i}      = ['d3  768cpu  Re=190 Janus'];
Number_Processors{i} = 768;
J_max(i) = 8;
i=i+1;

LastTime   = 4.3408E-03;
doLastLine = true;
LastLine1  = 16;        %77  Test_Number = 4     %16  Test_Number = 5
TIMERS_TYPE = 'Long';







    case {2048.7}

x_min = 0.0;
x_max = 5E-02; 

eps = 0.2;
nu  = 0.006;
C_f = 6.66667;

Title_Part_1 = 'CVS'; %'CVS  (( Initialized by CVS cinit-00375-eps005.0000.res (serial) ))'
J_max_Title  = 9;
Resolution_Title = 2048;
Reynolds = 320;
        
DIR      = [ROOT_DIR 'T e r a__G r i d\CVS_Eps2_Cf666\Lincoln\Re320_Nu006\'];



CaseDIR  = 'd3_256cpu_dt2e4_smallTol123\';
CaseName = 'CVS_2048M8J9_Cf666_eps2_nu006_Re320_dttol123_d3';
Case_Path_Name{i} = [DIR CaseDIR CaseName File_Type];   %.p0_user_stats
t_begin(i) = 0;
CaseLegend{i}      = ['d3  256cpu  Re=320   Small Tol123'];
Number_Processors{i} = 256;
J_max(i) = 9;
i=i+1;

CaseDIR  = 'd3_256cpu_dt2e4__O3O2O2\';
CaseName = 'CVS_2048M8J9_Cf666_eps2_nu006_Re320_dt_d3';
Case_Path_Name{i} = [DIR CaseDIR CaseName File_Type];   %.p0_user_stats
t_begin(i) = 0;
CaseLegend{i}      = ['d3  256cpu  Re=320   O3O2O2'];
Number_Processors{i} = 256;
J_max(i) = 9;
i=i+1;

DIR      = [ROOT_DIR 'T e r a__G r i d\CVS_Eps2_Cf666\'];

CaseDIR  = 'Janus\Re320_Nu006\d3_512cpu_dt2e4__Intel11_Zoltan11\results1_O1O1O1__ICrestartmode3\';
CaseName = 'CVS_2048M8J9_Cf666_eps2_nu006_Re320_dt_d3';
Case_Path_Name{i} = [DIR CaseDIR CaseName File_Type];   %.p0_user_stats
t_begin(i) = 0;
CaseLegend{i}      = ['d3  512cpu  Re=320 Janus O1O1O1 ICrestartmode=3'];
Number_Processors{i} = 512;
J_max(i) = 9;
i=i+1;

CaseDIR  = 'Janus\Re320_Nu006\d3_512cpu_dt2e4__Intel11_Zoltan11\results2_O1O1O3__ICrestartmode1\';
CaseName = 'CVS_2048M8J9_Cf666_eps2_nu006_Re320_dt_d3';
Case_Path_Name{i} = [DIR CaseDIR CaseName File_Type];   %.p0_user_stats
t_begin(i) = 0;
CaseLegend{i}      = ['d3  512cpu  Re=320 Janus O1O1O3 ICrestartmode=1'];
Number_Processors{i} = 512;
J_max(i) = 9;
i=i+1;

CaseDIR  = 'Janus\Re320_Nu006\d3_512cpu_dt2e4__Intel11_Zoltan11_O2O2O3\';
CaseName = 'CVS_2048M8J9_Cf666_eps2_nu006_Re320_dt_d3';
Case_Path_Name{i} = [DIR CaseDIR CaseName File_Type];   %.p0_user_stats
t_begin(i) = 0;
CaseLegend{i}      = ['d3  512cpu  Re=320 Janus O2O2O3 ICrestartmode=1'];
Number_Processors{i} = 512;
J_max(i) = 9;
i=i+1;

CaseDIR  = 'Janus\Re320_Nu006\d3_1024cpu_dt2e4__Intel11_Zoltan11_O2O2O3\';
CaseName = 'CVS_2048M8J9_Cf666_eps2_nu006_Re320_dt_d3';
Case_Path_Name{i} = [DIR CaseDIR CaseName File_Type];   %.p0_user_stats
t_begin(i) = 0;
CaseLegend{i}      = ['d3  1024cpu  Re=320 Janus O2O2O3 ICrestartmode=1'];
Number_Processors{i} = 1024;
J_max(i) = 9;
i=i+1;

CaseDIR  = 'Janus\Re320_Nu006\d3_1536cpu_dt2e4__Intel11_Zoltan11_O2O2O3\';
CaseName = 'CVS_2048M8J9_Cf666_eps2_nu006_Re320_dt_d3';
Case_Path_Name{i} = [DIR CaseDIR CaseName File_Type];   %.p0_user_stats
t_begin(i) = 0;
CaseLegend{i}      = ['d3  1536cpu  Re=320 Janus O2O2O3 ICrestartmode=1'];
Number_Processors{i} = 1536;
J_max(i) = 9;
i=i+1;


LastTime   = 4.3408E-03;
doLastLine = true;
LastLine1  = 16;        %77  Test_Number = 4     %16  Test_Number = 5
TIMERS_TYPE = '';







    case {2048.3}

x_min = 0.0;
x_max = 5E-02; 

eps = 0.2;
nu  = 0.006;
C_f = 6.66667;

Title_Part_1 = 'CVS'; %'CVS  (( Initialized by CVS cinit-00375-eps005.0000.res (serial) ))'
J_max_Title  = 9;
Resolution_Title = 2048;
Reynolds = 320;
        
DIR      = [ROOT_DIR 'T e r a__G r i d\CVS_Eps2_Cf666\requin.sharcnet.ca\Re320_Nu006\'];

CaseDIR  = 'd3_256cpu_dt2e4__O3O2O2\6hours\';
CaseName = 'CVS_2048M8J9_Cf666_eps2_nu006_Re320_dt_d3';
Case_Path_Name{i} = [DIR CaseDIR CaseName File_Type];   %.p0_user_stats
t_begin(i) = 0;
CaseLegend{i}      = ['d3  256cpu  Re=320   O3O2O2'];
Number_Processors{i} = 256;
J_max(i) = 9;
i=i+1;

CaseDIR  = 'd3_512cpu_dt2e4__O3O2O2\3hours\';
CaseName = 'CVS_2048M8J9_Cf666_eps2_nu006_Re320_dt_d3';
Case_Path_Name{i} = [DIR CaseDIR CaseName File_Type];   %.p0_user_stats
t_begin(i) = 0;
CaseLegend{i}      = ['d3  512cpu  Re=320   O3O2O2'];
Number_Processors{i} = 512;
J_max(i) = 9;
i=i+1;

LastTime   = 1.1067E-02;
doLastLine = false;
LastLine1  = 16;        %77  Test_Number = 4     %16  Test_Number = 5
TIMERS_TYPE = 'Short';














    case {2048.31}

x_min = 0.05;
x_max = 0.07; 

eps = 0.2;
nu  = 0.006;
C_f = 6.66667;

Title_Part_1 = 'CVS'; %'CVS  (( Initialized by CVS cinit-00375-eps005.0000.res (serial) ))'
J_max_Title  = 9;
Resolution_Title = 2048;
Reynolds = 320;
        
DIR      = [ROOT_DIR 'T e r a__G r i d\CVS_Eps2_Cf666\Janus\Re320_Nu006\'];

CaseDIR  = 'Scalability_64cpu\';
CaseName = 'CVS_2048M8J9_Cf666_eps2_nu006_Re320_dt_d3';
Case_Path_Name{i} = [DIR CaseDIR CaseName File_Type];   %.p0_user_stats
t_begin(i) = 0;
CaseLegend{i}      = ['d3  64cpu  Re=320 Janus'];
CaseLegend{i}      = ['    64 cpu'];
Number_Processors{i} = 64;
J_max(i) = 9;
i=i+1;

CaseDIR  = 'Scalability_128cpu\';
CaseName = 'CVS_2048M8J9_Cf666_eps2_nu006_Re320_dt_d3';
Case_Path_Name{i} = [DIR CaseDIR CaseName File_Type];   %.p0_user_stats
t_begin(i) = 0;
CaseLegend{i}      = ['d3  128cpu  Re=320 Janus'];
CaseLegend{i}      = ['  128 cpu'];
Number_Processors{i} = 128;
J_max(i) = 9;
i=i+1;

CaseDIR  = 'Scalability_256cpu\';
CaseName = 'CVS_2048M8J9_Cf666_eps2_nu006_Re320_dt_d3';
Case_Path_Name{i} = [DIR CaseDIR CaseName File_Type];   %.p0_user_stats
t_begin(i) = 0;
CaseLegend{i}      = ['d3  256cpu  Re=320 Janus'];
CaseLegend{i}      = ['  256 cpu'];
Number_Processors{i} = 256;
J_max(i) = 9;
i=i+1;

CaseDIR  = 'Scalability_512cpu\';
CaseName = 'CVS_2048M8J9_Cf666_eps2_nu006_Re320_dt_d3';
Case_Path_Name{i} = [DIR CaseDIR CaseName File_Type];   %.p0_user_stats
t_begin(i) = 0;
CaseLegend{i}      = ['d3  512cpu  Re=320 Janus'];
CaseLegend{i}      = ['  512 cpu'];
Number_Processors{i} = 512;
J_max(i) = 9;
i=i+1;

CaseDIR  = 'Scalability_1024cpu\';
CaseName = 'CVS_2048M8J9_Cf666_eps2_nu006_Re320_dt_d3';
Case_Path_Name{i} = [DIR CaseDIR CaseName File_Type];   %.p0_user_stats
t_begin(i) = 0;
CaseLegend{i}      = ['d3  1024cpu  Re=320 Janus'];
CaseLegend{i}      = ['1024 cpu'];
Number_Processors{i} = 1024;
J_max(i) = 9;
i=i+1;

CaseDIR  = 'Scalability_2048cpu\';
CaseName = 'CVS_2048M8J9_Cf666_eps2_nu006_Re320_dt_d3';
Case_Path_Name{i} = [DIR CaseDIR CaseName File_Type];   %.p0_user_stats
t_begin(i) = 0;
CaseLegend{i}      = ['d3  2048cpu  Re=320 Janus'];
CaseLegend{i}      = ['2048 cpu'];
Number_Processors{i} = 2048;
J_max(i) = 9;
i=i+1;

LastTime   = 6.3842E-02; %1.1067E-02;
doLastLine = true;
LastLine1  = 24; %165;        
TIMERS_TYPE = 'Short';














    case {2048.121}

x_min = 0.05;
x_max = 0.07; 

eps = 0.2;
nu  = 0.006;
C_f = 6.66667;

Title_Part_1 = 'CVS'; %'CVS  (( Initialized by CVS cinit-00375-eps005.0000.res (serial) ))'
J_max_Title  = 9;
Resolution_Title = 2048;
Reynolds = 320;
        
DIR      = [ROOT_DIR 'T e r a__G r i d\CVS_Eps2_Cf666\Janus\Re320_Nu006\'];

CaseDIR  = 'Scalability_64cpu__ONE2ONE_RKL_1\';
CaseName = 'CVS_2048M8J9_Cf666_eps2_nu006_Re320_dt_d3';
Case_Path_Name{i} = [DIR CaseDIR CaseName File_Type];   %.p0_user_stats
t_begin(i) = 0;
CaseLegend{i}      = ['d3  64cpu  Re=320 Janus'];
CaseLegend{i}      = ['    64 cpu'];
Number_Processors{i} = 64;
J_max(i) = 9;
i=i+1;

CaseDIR  = 'Scalability_128cpu__ONE2ONE_RKL_1\';
CaseName = 'CVS_2048M8J9_Cf666_eps2_nu006_Re320_dt_d3';
Case_Path_Name{i} = [DIR CaseDIR CaseName File_Type];   %.p0_user_stats
t_begin(i) = 0;
CaseLegend{i}      = ['d3  128cpu  Re=320 Janus'];
CaseLegend{i}      = ['  128 cpu'];
Number_Processors{i} = 128;
J_max(i) = 9;
i=i+1;

CaseDIR  = 'Scalability_256cpu__ONE2ONE_RKL_1\';
CaseName = 'CVS_2048M8J9_Cf666_eps2_nu006_Re320_dt_d3';
Case_Path_Name{i} = [DIR CaseDIR CaseName File_Type];   %.p0_user_stats
t_begin(i) = 0;
CaseLegend{i}      = ['d3  256cpu  Re=320 Janus'];
CaseLegend{i}      = ['  256 cpu'];
Number_Processors{i} = 256;
J_max(i) = 9;
i=i+1;

CaseDIR  = 'Scalability_512cpu__ONE2ONE_RKL_1\';
CaseName = 'CVS_2048M8J9_Cf666_eps2_nu006_Re320_dt_d3';
Case_Path_Name{i} = [DIR CaseDIR CaseName File_Type];   %.p0_user_stats
t_begin(i) = 0;
CaseLegend{i}      = ['d3  512cpu  Re=320 Janus'];
CaseLegend{i}      = ['  512 cpu'];
Number_Processors{i} = 512;
J_max(i) = 9;
i=i+1;

CaseDIR  = 'Scalability_1024cpu__ONE2ONE_RKL_1\';
CaseName = 'CVS_2048M8J9_Cf666_eps2_nu006_Re320_dt_d3';
Case_Path_Name{i} = [DIR CaseDIR CaseName File_Type];   %.p0_user_stats
t_begin(i) = 0;
CaseLegend{i}      = ['d3  1024cpu  Re=320 Janus'];
CaseLegend{i}      = ['1024 cpu'];
Number_Processors{i} = 1024;
J_max(i) = 9;
i=i+1;

CaseDIR  = 'Scalability_2048cpu__ONE2ONE_RKL_1\';
CaseName = 'CVS_2048M8J9_Cf666_eps2_nu006_Re320_dt_d3';
Case_Path_Name{i} = [DIR CaseDIR CaseName File_Type];   %.p0_user_stats
t_begin(i) = 0;
CaseLegend{i}      = ['d3  2048cpu  Re=320 Janus'];
CaseLegend{i}      = ['2048 cpu'];
Number_Processors{i} = 2048;
J_max(i) = 9;
i=i+1;

LastTime   = 6.3842E-02; %1.1067E-02;
doLastLine = true;
LastLine1  = 24; %165;        
TIMERS_TYPE = 'Short';














    case {2048.21}

x_min = 0.05;
x_max = 0.07; 

eps = 0.2;
nu  = 0.006;
C_f = 6.66667;

Title_Part_1 = 'CVS   N_{prd}=N_{dif}=2'; %'CVS  (( Initialized by CVS cinit-00375-eps005.0000.res (serial) ))'
J_max_Title  = 9;
Resolution_Title = 2048;
Reynolds = 320;
        
DIR      = [ROOT_DIR 'T e r a__G r i d\CVS_Eps2_Cf666\Janus\Re320_Nu006\'];

CaseDIR  = 'Ndifprd2_Scalability_64cpu\';
CaseName = 'CVS_2048M8J9_Cf666_eps2_nu006_Re320_dt_d3';
Case_Path_Name{i} = [DIR CaseDIR CaseName File_Type];   %.p0_user_stats
t_begin(i) = 0;
CaseLegend{i}      = ['d3  64cpu  Re=320 Janus'];
CaseLegend{i}      = ['  64 cpu'];
Number_Processors{i} = 64;
J_max(i) = 9;
i=i+1;

CaseDIR  = 'Ndifprd2_Scalability_128cpu\';
CaseName = 'CVS_2048M8J9_Cf666_eps2_nu006_Re320_dt_d3';
Case_Path_Name{i} = [DIR CaseDIR CaseName File_Type];   %.p0_user_stats
t_begin(i) = 0;
CaseLegend{i}      = ['d3  128cpu  Re=320 Janus'];
CaseLegend{i}      = [' 128 cpu'];
Number_Processors{i} = 128;
J_max(i) = 9;
i=i+1;

CaseDIR  = 'Ndifprd2_Scalability_256cpu\';
CaseName = 'CVS_2048M8J9_Cf666_eps2_nu006_Re320_dt_d3';
Case_Path_Name{i} = [DIR CaseDIR CaseName File_Type];   %.p0_user_stats
t_begin(i) = 0;
CaseLegend{i}      = ['d3  256cpu  Re=320 Janus'];
CaseLegend{i}      = [' 256 cpu'];
Number_Processors{i} = 256;
J_max(i) = 9;
i=i+1;

CaseDIR  = 'Ndifprd2_Scalability_512cpu\';
CaseName = 'CVS_2048M8J9_Cf666_eps2_nu006_Re320_dt_d3';
Case_Path_Name{i} = [DIR CaseDIR CaseName File_Type];   %.p0_user_stats
t_begin(i) = 0;
CaseLegend{i}      = ['d3  512cpu  Re=320 Janus'];
CaseLegend{i}      = [' 512 cpu'];
Number_Processors{i} = 512; 
J_max(i) = 9;
i=i+1;

CaseDIR  = 'Ndifprd2_Scalability_1024cpu\';
CaseName = 'CVS_2048M8J9_Cf666_eps2_nu006_Re320_dt_d3';
Case_Path_Name{i} = [DIR CaseDIR CaseName File_Type];   %.p0_user_stats
t_begin(i) = 0;
CaseLegend{i}      = ['d3  1024cpu  Re=320 Janus'];
CaseLegend{i}      = ['1024 cpu'];
Number_Processors{i} = 1024;
J_max(i) = 9;
i=i+1;
% 
% CaseDIR  = 'Ndifprd2_Scalability_2048cpu\';
% CaseName = 'CVS_2048M8J9_Cf666_eps2_nu006_Re320_dt_d3';
% Case_Path_Name{i} = [DIR CaseDIR CaseName File_Type];   %.p0_user_stats
% t_begin(i) = 0;
% CaseLegend{i}      = ['d3  2048cpu  Re=320 Janus'];
% CaseLegend{i}      = ['2048cpu'];
% Number_Processors{i} = 2048;
% J_max(i) = 8;
% i=i+1;

LastTime   = 6.4898E-02; %1.1067E-02;
doLastLine = true;
LastLine1  = 56; %165;        
TIMERS_TYPE = 'Short';














    case {2048.11}

x_min = 0.05;
x_max = 0.07; 

eps = 0.2;
nu  = 0.006;
C_f = 6.66667;

Title_Part_1 = 'CVS   N_{prd}=N_{dif}=1'; %'CVS  (( Initialized by CVS cinit-00375-eps005.0000.res (serial) ))'
J_max_Title  = 9;
Resolution_Title = 2048;
Reynolds = 320;
        
DIR      = [ROOT_DIR 'T e r a__G r i d\CVS_Eps2_Cf666\Janus\Re320_Nu006\'];

CaseDIR  = 'Ndifprd1_Scalability_64cpu\';
CaseName = 'CVS_2048M8J9_Cf666_eps2_nu006_Re320_dt_d3';
Case_Path_Name{i} = [DIR CaseDIR CaseName File_Type];   %.p0_user_stats
t_begin(i) = 0;
CaseLegend{i}      = ['d3  64cpu  Re=320 Janus'];
CaseLegend{i}      = ['64cpu'];
Number_Processors{i} = 64;
J_max(i) = 9;
i=i+1;

CaseDIR  = 'Ndifprd1_Scalability_128cpu\';
CaseName = 'CVS_2048M8J9_Cf666_eps2_nu006_Re320_dt_d3';
Case_Path_Name{i} = [DIR CaseDIR CaseName File_Type];   %.p0_user_stats
t_begin(i) = 0;
CaseLegend{i}      = ['d3  128cpu  Re=320 Janus'];
CaseLegend{i}      = ['128cpu'];
Number_Processors{i} = 128;
J_max(i) = 9;
i=i+1;

CaseDIR  = 'Ndifprd1_Scalability_256cpu\';
CaseName = 'CVS_2048M8J9_Cf666_eps2_nu006_Re320_dt_d3';
Case_Path_Name{i} = [DIR CaseDIR CaseName File_Type];   %.p0_user_stats
t_begin(i) = 0;
CaseLegend{i}      = ['d3  256cpu  Re=320 Janus'];
CaseLegend{i}      = ['256cpu'];
Number_Processors{i} = 256;
J_max(i) = 9;
i=i+1;

CaseDIR  = 'Ndifprd1_Scalability_512cpu\';
CaseName = 'CVS_2048M8J9_Cf666_eps2_nu006_Re320_dt_d3';
Case_Path_Name{i} = [DIR CaseDIR CaseName File_Type];   %.p0_user_stats
t_begin(i) = 0;
CaseLegend{i}      = ['d3  512cpu  Re=320 Janus'];
CaseLegend{i}      = ['512cpu'];
Number_Processors{i} = 512;
J_max(i) = 9;
i=i+1;

% CaseDIR  = 'Ndifprd1_Scalability_1024cpu\';
% CaseName = 'CVS_2048M8J9_Cf666_eps2_nu006_Re320_dt_d3';
% Case_Path_Name{i} = [DIR CaseDIR CaseName File_Type];   %.p0_user_stats
% t_begin(i) = 0;
% CaseLegend{i}      = ['d3  1024cpu  Re=320 Janus'];
% CaseLegend{i}      = ['1024cpu'];
% Number_Processors{i} = 1024;
% J_max(i) = 8;
% i=i+1;
% 
% CaseDIR  = 'Ndifprd1_Scalability_2048cpu\';
% CaseName = 'CVS_2048M8J9_Cf666_eps2_nu006_Re320_dt_d3';
% Case_Path_Name{i} = [DIR CaseDIR CaseName File_Type];   %.p0_user_stats
% t_begin(i) = 0;
% CaseLegend{i}      = ['d3  2048cpu  Re=320 Janus'];
% CaseLegend{i}      = ['2048cpu'];
% Number_Processors{i} = 2048;
% J_max(i) = 8;
% i=i+1;

LastTime   = 6.5083E-02; %1.1067E-02;
doLastLine = true;
LastLine1  = 60; %165;        
TIMERS_TYPE = 'Short';









end










% % CaseDIR  = '128cpu\';
% % CaseName = 'CVS__initGDMeps43__eps2_Nu9_Cf5.2__128cpu';
% % Case_Path_Name{i} = [DIR CaseDIR 'results\' CaseName File_Type];   %.p0_user_stats
% % CaseLegend{i}      = [10 'Steele' 10 '128 cpu'];
% % Number_Processors{i} = 128;
% % Case_CPUTime__Path_Name{i} = [DIR CaseDIR CaseName '.cputime'];
% % i=i+1;
% 
% % CaseDIR  = '64cpu\';
% % CaseName = 'CVS__initGDMeps43__eps2_Nu9_Cf5.2__64cpu';
% % Case_Path_Name{i} = [DIR CaseDIR 'results\' CaseName File_Type];   %.p0_user_stats
% % CaseLegend{i}      = ['64 cpu'];
% % Number_Processors{i} = 64;
% % Case_CPUTime__Path_Name{i} = [DIR CaseDIR CaseName '.cputime'];
% % i=i+1;
% 
% % CaseDIR  = '32cpu\';
% % CaseName = 'CVS__initGDMeps43__eps2_Nu9_Cf5.2__32cpu';
% % Case_Path_Name{i} = [DIR CaseDIR 'results\' CaseName File_Type];   %.p0_user_stats
% % CaseLegend{i}      = ['32 cpu'];
% % Number_Processors{i} = 32;
% % Case_CPUTime__Path_Name{i} = [DIR CaseDIR CaseName '.cputime'];
% % i=i+1;
% 
% % CaseDIR  = '16cpu\';
% % CaseName = 'CVS__initGDMeps43__eps2_Nu9_Cf5.2__16cpu';
% % Case_Path_Name{i} = [DIR CaseDIR 'results\' CaseName File_Type];   %.p0_user_stats
% % CaseLegend{i}      = ['16 cpu'];
% % Number_Processors{i} = 16;
% % Case_CPUTime__Path_Name{i} = [DIR CaseDIR CaseName '.cputime'];
% % i=i+1;

% CaseDIR  = 'Re070_Nu09\8cpu\';
% CaseName = 'CVS_256M8J6_eps2_nu09_Re70';      
% Case_Path_Name{i} = [DIR CaseDIR 'results\' CaseName File_Type];   %.p0_user_stats
% CaseLegend{i}      = [' 8  cpu  Re=70'];
% Number_Processors{i} = 8;
% J_max(i) = 6;
% i=i+1;
% 
% CaseDIR  = 'Re070_Nu09\32cpu\';
% CaseName = 'CVS_256M8J6_eps2_nu09_Re70';      
% Case_Path_Name{i} = [DIR CaseDIR 'results\' CaseName File_Type];   %.p0_user_stats
% CaseLegend{i}      = ['32  cpu  Re=70'];
% Number_Processors{i} = 32;
% J_max(i) = 6;
% i=i+1;
% 
% CaseDIR  = 'Re120_Nu035\8cpu\';
% CaseName = 'CVS_512M8J7_eps2_nu035_Re120';      
% Case_Path_Name{i} = [DIR CaseDIR 'results\' CaseName File_Type];   %.p0_user_stats
% CaseLegend{i}      = [' 8  cpu  Re=120'];
% Number_Processors{i} = 8;
% J_max(i) = 7;
% i=i+1;
% 
% CaseDIR  = 'Re120_Nu035\32cpu\';
% CaseName = 'CVS_512M8J7_eps2_nu035_Re120';      
% Case_Path_Name{i} = [DIR CaseDIR 'results\' CaseName File_Type];   %.p0_user_stats
% CaseLegend{i}      = ['32  cpu  Re=120'];
% Number_Processors{i} = 32;
% J_max(i) = 7;
% i=i+1;
% 
% CaseDIR  = 'Re190_Nu015\8cpu\';
% CaseName = 'CVS_1024M8J8_eps2_nu015_Re190';      
% Case_Path_Name{i} = [DIR CaseDIR 'results\' CaseName File_Type];   %.p0_user_stats
% CaseLegend{i}      = [' 8  cpu  Re=190'];
% Number_Processors{i} = 8;
% J_max(i) = 8;
% i=i+1;
% 
% CaseDIR  = 'Re190_Nu015\32cpu\';
% CaseName = 'CVS_1024M8J8_eps2_nu015_Re190';      
% Case_Path_Name{i} = [DIR CaseDIR 'results\' CaseName File_Type];   %.p0_user_stats
% CaseLegend{i}      = ['32  cpu  Re=190'];
% Number_Processors{i} = 32;
% J_max(i) = 8;
% i=i+1;


% CaseDIR  = 'Re190_Nu015\April_2010\domain_meth=3\0064cpu\';
% CaseName = 'CVS_1024M8J8_eps2_nu015_Re190';      
% Case_Path_Name{i} = [DIR CaseDIR 'J_TREE_ROOT=5\' CaseName File_Type];   %.p0_user_stats
% CaseLegend{i}      = ['64  cpu  Re=190   domain-meth=3   J-TREE-ROOT=5'];
% Number_Processors{i} = 64;
% J_max(i) = 8;
% i=i+1;


% DIR      = 'C:\Documents and Settings\AliReza\My Documents\T e r a__G r i d\bull.sharcnet.ca\SpeedUpTest_01__Eps43_Nu9_Cf6\DLB\';
% 
% CaseDIR  = '8cpu\domain_meth=2\';
% CaseName = 'CVS_Nu0.09_Cf6_eps0.43__8cpu';      
% Case_Path_Name{i} = [DIR CaseDIR 'results\' CaseName File_Type];   %.p0_user_stats
% CaseLegend{i}      = ['8  cpu   domain-meth=2'];
% Number_Processors{i} = 8;
% Case_CPUTime__Path_Name{i} = [DIR CaseDIR CaseName '.cputime'];
% i=i+1;
% 
% CaseDIR  = '8cpu\domain_meth=3\';
% CaseName = 'CVS_Nu0.09_Cf6_eps0.43__8cpu';      
% Case_Path_Name{i} = [DIR CaseDIR 'results\' CaseName File_Type];   %.p0_user_stats
% CaseLegend{i}      = ['8  cpu   domain-meth=3'];
% Number_Processors{i} = 8;
% Case_CPUTime__Path_Name{i} = [DIR CaseDIR CaseName '.cputime'];
% i=i+1;


% DIR      = 'C:\Documents and Settings\AliReza\My Documents\T e r a__G r i d\bull.sharcnet.ca\SpeedUpTest_02__Eps35_Nu375_Cf666\';
% 
% CaseDIR  = '16cpu\';
% CaseName = 'FHT-00375_gdm-TEPS035-wrk';      
% Case_Path_Name{i} = [DIR CaseDIR 'results\' CaseName File_Type];   %.p0_user_stats
% CaseLegend{i}      = ['16  cpu   \nu=0.0375  Cf=6.66  \epsilon=0.35'];
% Number_Processors{i} = 16;
% Case_CPUTime__Path_Name{i} = [DIR CaseDIR CaseName '.cputime'];
% i=i+1;
% 
% CaseDIR  = '64cpu\';
% CaseName = 'FHT-00375_gdm-TEPS035-wrk';      
% Case_Path_Name{i} = [DIR CaseDIR 'results\' CaseName File_Type];   %.p0_user_stats
% CaseLegend{i}      = ['64  cpu   \nu=0.0375  Cf=6.66  \epsilon=0.35'];
% Number_Processors{i} = 64;
% Case_CPUTime__Path_Name{i} = [DIR CaseDIR CaseName '.cputime'];
% i=i+1;





%% blast   MSMSL   HP (Intel)
DIR       = 'C:\Documents and Settings\AliReza\My Documents\T e r a__G r i d\blast\';





global CaseLegend
global Font_Size

Number_Case = i-1;


switch TIMERS_TYPE
    case {'Long'}
        no_NWLT = 4;
    case {'Short'}
        no_NWLT = 16;
end




















%% COMPARE    Log_Info_Variable_1    for   all  Different  Runs

if (Plot_LogFile)
figure
hold on;

for i = 1:Number_Case    
    Log_Info = load( Case_Path_Name{i} );
    plot( Log_Info(:,1)+t_begin(i), Log_Info(:,Log_Info_Variable_1), LineColorMarker_max{i}, 'LineWidth', LineWidth{i} );    
    fprintf('%d  Case File Name (Full Path)  : %s \n', i, Case_Path_Name{i} );
end




%ylabel('%SGS_D_i_s_s_i_p_a_t_i_o_n');
%ylabel('<Energy _K_i_n_e_t_i_c>');
%ylabel('CPU _T_i_m_e');
xlabel('Time');
xlabel('{\tau}_{eddy}');
xlabel('{\tau}_{eddy}', 'FontSize', Font_Size_Label);
box(gca,'on');

AddLegend(Number_Case);

% title(['CVS  (( Initialized by SCALES GDM \epsilon=0.43 Nupdt=0 ))' 10 '\nu=0.09   C_f=5.2']);
% title(['CVS  (( Initialized by CVS 0040.res on 64cpu ))' 10 '\epsilon=0.2   \nu=0.09   C_f=5.2' 10 'Loop CPU_T_i_m_e']);
% title(['CVS  (( Initialized by CVS 0004.res (serial) ))' 10 '\epsilon=0.43   \nu=0.09   C_f=6']);
% title(['|c| / max|c|       &       |d| / max|d|' 10 'All J'])
title([Title_Part_1 10 num2str(Resolution_Title),'^3','   \epsilon=',num2str(eps),'   J_{max}=',num2str(J_max_Title),'   \nu=',num2str(nu),'   C_f=',num2str(C_f),'   Re=',num2str(Reynolds),'']);
% Unix (\n=char(10)), PCs (\n=[char(10),char(13)]) and Macs (\n=char(13)). 
% http://www.mathworks.com/matlabcentral/newsreader/view_thread/154845

xlim([x_min  x_max]);
%ylim([0 0.1]);

x_tick_vector = 0:1/(3*C_f):x_max;
x_tick_label = cell(1,length(x_tick_vector)); 
%x_tick_label(1:end) = num2str(fix(x_tick_vector(1:end)*(3*C_f)));
for i=1:length(x_tick_vector)
    x_tick_label{i} = num2str(round(x_tick_vector(i)*(3*C_f)));
end 
set(gca,'XTick',x_tick_vector)
set(gca,'XTickLabel',x_tick_label)   %{'-pi','-pi/2','0','pi/2','pi'})


% '.p0_log'             F i l e     V a r i a b l e s  ::: 
% t, dt, loop_cpu_time , j_lev, nwlt, nxyz(1), nxyz(2), nxyz(3), eps , active_pts_3dlvl_001 , active_pts_3dlvl_002 , active_pts_3dlvl_003 , 

% '.p0_user_stats'      F i l e     V a r i a b l e s  ::: 
%Time          t_eddy           Nwlt    Nwlt/Nmax     Re_taylor     eta/h         <KE>          <DISS_res>    <DISS_SGS>    fracSGS_DISS  epsilon

switch lower(File_Type)
   case {'.p0_user_stats'}
      switch (Log_Info_Variable_1)
          case (2)
              ylabel('{\tau}_{eddy}');
          case (3)
              ylabel('N_w_l_t   ( {\Sigma} nwlt    over all  processors )');
          case (4)
              ylabel('N _w_l_t / N _m_a_x');
          case (5)
              ylabel('Re _T_a_y_l_o_r');
          case (6)
              ylabel('eta/h');
          case (7)
              ylabel('<Energy _K_i_n_e_t_i_c>');
          case (8)
              ylabel('<Dissipation _r_e_s>');
          case (9)
              ylabel('<Dissipation _S_G_S>');
          case (10)
              ylabel('% Dissipation _S_G_S');
          case (11)
              ylabel('{\epsilon}');
      end
   case '.p0_log'
      switch (Log_Info_Variable_1)
      % t,  dt,  j_lev,  nwlt,  nxyz(1),  nxyz(2),  nxyz(3),  eps,  nwlt(j=1),  nwlt(j=2),  nwlt(j=3),  nwlt(j=4),  nwlt(j=5),  nwlt(j=6),  [pr=0]  NWLT,  GHO,  SEND,  RECV,  TIMER(1),  TIMER(2),  [pr=1]  NWLT,  GHO,  SEND,  RECV,  TIMER(1),  TIMER(2),  [pr=2]  NWLT,  GHO,  SEND,  RECV,  TIMER(1),  TIMER(2),  [pr=3]  NWLT,  GHO,  SEND,  RECV,  TIMER(1),  TIMER(2),  [pr=4]  NWLT,  GHO,  SEND,  RECV,  TIMER(1),  TIMER(2),  [pr=5]  NWLT,  GHO,  SEND,  RECV,  TIMER(1),  TIMER(2),  [pr=6]  NWLT,  GHO,  SEND,  RECV,  TIMER(1),  TIMER(2),  [pr=7]  NWLT,  GHO,  SEND,  RECV,  TIMER(1),  TIMER(2),  [pr=8]  NWLT,  GHO,  SEND,  RECV,  TIMER(1),  TIMER(2),  [pr=9]  NWLT,  GHO,  SEND,  RECV,  TIMER(1),  TIMER(2),  [pr=10]  NWLT,  GHO,  SEND,  RECV,  TIMER(1),  TIMER(2),  [pr=11]  NWLT,  GHO,  SEND,  RECV,  TIMER(1),  TIMER(2),  [pr=12]  NWLT,  GHO,  SEND,  RECV,  TIMER(1),  TIMER(2),  [pr=13]  NWLT,  GHO,  SEND,  RECV,  TIMER(1),  TIMER(2),  [pr=14]  NWLT,  GHO,  SEND,  RECV,  TIMER(1),  TIMER(2),  [pr=15]  NWLT,  GHO,  SEND,  RECV,  TIMER(1),  TIMER(2) 
          case (2)
              ylabel('{\Delta} _t');
          %case (3)
          %    ylabel('Loop  CPU _T_i_m_e');
          case (3)
              ylabel('J _l_e_v_e_l');
          case (4)
              ylabel('nwlt');
          case (5)
              ylabel('nxyz(1)');
          case (6)
              ylabel('nxyz(2)');
          case (7)
              ylabel('nxyz(3)');
          case (8)
              ylabel('{\epsilon}');
          case (9)
              ylabel('nwlt(j=1)');
          case (10)
              ylabel('nwlt(j=2)');
          case (11)
              ylabel('nwlt(j=3)');
          case (12)
              ylabel('nwlt(j=4)');
          case (13)
              ylabel('nwlt(j=5)');
          case (14)
              ylabel('nwlt(j=6)');
          case (20)
              ylabel('main time integration loop   TIMER(2)   [pr=0] ');
      end
   otherwise
      disp('Unknown File_Type.')
end



ylabel_hand = get(gca,'ylabel');
set(ylabel_hand,'fontsize',Font_Size_Label);  
xlabel_hand = get(gca,'xlabel');
set(xlabel_hand,'fontsize',Font_Size_Label);  

end










%% COMPARE  CPU Time for Timers_No    (e.g. "Time-Integration for each Time-Step"   when   Timers_No=4)

if (Plot_CPUTime)
figure
hold on;

for i = 1:Number_Case    
    Log_Info = load( Case_Path_Name{i} );

   %Log_Info_Variable = no_columns + J_max(i) + 4 + Timers_No;                           %  [pr=??]  NWLT,  GHO,  SEND,  RECV     +    8 TIMER        
   %Log_Info_Variable = Log_Info_Variable + Timers_No;
    Log_Info_Variable = no_columns + J_max(i) + no_NWLT + Timers_No;  
    
   %plot( Log_Info(:,1), Log_Info(:,Log_Info_Variable), LineColorMarker{i}, 'LineWidth', LineWidth{i} );    
    plot( Log_Info(:,1), Log_Info(:,Log_Info_Variable)/Log_Info(:,no_columns + J_max(i) + no_NWLT + 1), LineColorMarker{i}, 'LineWidth', LineWidth{i} );    
    fprintf('%d  Case File Name (Full Path)  : %s \n', i, Case_Path_Name{i} );
end

% for i = 1:Number_Case
%     [Line_Number, message, CPUTime__For_Integration]  = textread( Case_CPUTime__Path_Name{i}, '%u %33c %f' );
%     plot( CPUTime__For_Integration, LineColorMarker{i}, 'LineWidth', LineWidth{i} );    
%     fprintf('%d  Case CPUTime File Name (Full Path)  : %s \n', i, Case_CPUTime__Path_Name{i} );
% end


AddLegend(Number_Case);

%title(['CVS  (( Initialized by CVS 0004.res (serial) ))' 10 '\epsilon=0.43   \nu=0.09   C_f=6'  10 'CPU_T_i_m_e   for    Time Integration']);
%title(['CVS  (( Initialized by CVS 0004.res (serial) ))' 10 '\epsilon=0.43   \nu=0.09   C_f=6']);
%title(['Stochastic Coherent Adaptive Large Eddy Simulations (SCALES)' 10 'Forced Homogeneous Turbulence' 10 '\epsilon=0.2   C_f=6.66']);
title([Title_Part_1 10 num2str(Resolution_Title),'^3','   \epsilon=',num2str(eps),'   J_{max}=',num2str(J_max_Title),'   \nu=',num2str(nu),'   C_f=',num2str(C_f),'   Re=',num2str(Reynolds),'']);

xlim([0.0  x_max]);
%ylim([0 0.1]);

ylabel('CPU_T_i_m_e   for    Time Integration');
xlabel('Time Step Number');
box(gca,'on');

%Timers_No                   %  4- time advancement   5 - parallel_migrate   6 - parallel communicators
switch lower(File_Type)
   case {'.p0_user_stats'}
      switch (Log_Info_Variable)
          case (2)
              ylabel('t _e_d_d_y');
          case (3)
              ylabel('N_w_l_t   ( {\Sigma} nwlt    over all  processors )');
          case (4)
              ylabel('N _w_l_t / N _m_a_x');
          case (5)
              ylabel('Re _T_a_y_l_o_r');
          case (6)
              ylabel('eta/h');
          case (7)
              ylabel('<Energy _K_i_n_e_t_i_c>');
          case (8)
              ylabel('<Dissipation _r_e_s>');
          case (9)
              ylabel('<Dissipation _S_G_S>');
          case (10)
              ylabel('% Dissipation _S_G_S');
          case (11)
              ylabel('{\epsilon}');
      end
   case '.p0_log'
      switch (Timers_No)
          case (1)
              ylabel('inputs and initializations ');
          case (2)
              ylabel('adapt to initial conditions ');
          case (3)
              ylabel('adapt grid ');
          case (4)
              ylabel('time advancement');
          case (5)
              ylabel('parallel migrate');
          case (6)
              ylabel('parallel communicators');
      end
   otherwise
      disp('Unknown File_Type.')
end



ylabel_hand = get(gca,'ylabel');
set(ylabel_hand,'fontsize',Font_Size_Label);  
xlabel_hand = get(gca,'xlabel');
set(xlabel_hand,'fontsize',Font_Size_Label);  


end









%% COMPARE  CPU Time for Time-Integration  ( SpeedUp )

% figure
% hold on;
% 
% for i = 1:Number_Case    
%     [Line_Number, message, CPUTime__For_Integration]  = textread( Case_CPUTime__Path_Name{i}, '%u %33c %f' );
%     plot( Number_Processors{i}, sum(CPUTime__For_Integration), LineColorMarker{i}, 'LineWidth', LineWidth{i} );    
%     fprintf('%d  Case CPUTime File Name (Full Path)  : %s \n', i, Case_CPUTime__Path_Name{i} );
% end
% 
% 
% legend(CaseLegend{1}, CaseLegend{2}, CaseLegend{3}, CaseLegend{4}, CaseLegend{5}, CaseLegend{6}, CaseLegend{7} ); 
% title(['CVS  (( Initialized by CVS 0040.res on 64cpu ))' 10 '\epsilon=0.2   \nu=0.09   C_f=5.2' 10 'SpeedUp' 10 'based on' 10 'CPU_T_i_m_e   for    Time Integration']);
% 
% %xlim([0 20.0]);
% %ylim([0 0.1]);
% 
% set(gca,'XTick',[2 4 8 16 32 64 128])
% %set(gca,'XTickLabel',{'2', '4', '8', '16', '32', '64', '128'})
% 
% ylabel('{\Sigma} CPU_{Time}_{_T_i_m_e _I_n_t_e_g_r_a_t_i_o_n}    over All Time-Steps');
% xlabel('Number of Processors');








%% COMPARE  CPU Time  for  Timers_No   ( SpeedUp Based on / np )

if (Plot_SpeedUp)
figure
hold on;
i_start=1;
np = Number_Processors{i_start};


%[Line_Number, message, CPUTime__For_Integration]  = textread( Case_CPUTime__Path_Name{7}, '%u %33c %f' );
%Sum__CPUTime__For_Integration__2cpu = sum(CPUTime__For_Integration);
Log_Info = load( Case_Path_Name{i_start} );
Log_Info_Variable = no_columns + J_max(i_start) + no_NWLT + Timers_No;                             %  [pr=??]  NWLT,  GHO,  SEND,  RECV     +    8 TIMER        

switch do_average__TIMERS
   case 'T'
       switch TIMERS_CUMULATIVE
           case 'T'        
               Sum__CPUTime__For_Integration__npcpu = (Log_Info(end,Log_Info_Variable)-Log_Info(5,Log_Info_Variable))/length( Log_Info(5:end,Log_Info_Variable) ); %  based on average
           otherwise
               Sum__CPUTime__For_Integration__npcpu = sum(Log_Info(5:end,Log_Info_Variable))/length( Log_Info(5:end,Log_Info_Variable) ); %  based on average
       end
    otherwise  
        k=1;
        while k <= size(Log_Info,1)
            if ( Log_Info(k,1) == LastTime )
                LastLine = k
                break
            else
                k = k +1;
            end            
        end
        if (doLastLine) 
            LastLine = LastLine1; 
            Log_Info(LastLine,1)
        end
        Sum__CPUTime__For_Integration__npcpu = Log_Info(LastLine,Log_Info_Variable); %  based on last value    
end

    
for i = i_start:Number_Case  % ii:ii+2
    %[Line_Number, message, CPUTime__For_Integration]  = textread( Case_CPUTime__Path_Name{i}, '%u %33c %f' );
    %plot( Number_Processors{i}, Sum__CPUTime__For_Integration__2cpu/sum(CPUTime__For_Integration), LineColorMarker{i}, 'LineWidth', LineWidth{i} );    
    %fprintf('%d  Case CPUTime File Name (Full Path)  : %s \n', i, Case_CPUTime__Path_Name{i} );
    %SppedUp{i} = Sum__CPUTime__For_Integration__2cpu/sum(CPUTime__For_Integration)

    Log_Info = load( Case_Path_Name{i} );
    Log_Info_Variable = no_columns + J_max(i) + no_NWLT + Timers_No;                               %  [pr=??]  NWLT,  GHO,  SEND,  RECV     +    8 TIMER        

    switch do_average__TIMERS   
        case 'T'
            switch TIMERS_CUMULATIVE
                case 'T'
                    SpeedUp{i} = np*Sum__CPUTime__For_Integration__npcpu/((Log_Info(end,Log_Info_Variable)-Log_Info(5,Log_Info_Variable))/length( Log_Info(5:end,Log_Info_Variable))); %  based on average
                   %SpeedUp{i} = ((Log_Info(end,Log_Info_Variable)-Log_Info(5,Log_Info_Variable))/length( Log_Info(5:end,Log_Info_Variable)));
                    plot( Number_Processors{i}, SpeedUp{i}, LineColorMarker{i}, 'LineWidth', LineWidth{i} ); %  based on average      
                    fprintf('%d  Length= %d   Case File Name (Full Path)  : %s \n', i, length( Log_Info(5:end,Log_Info_Variable)), Case_Path_Name{i} );
                otherwise
                    SpeedUp{i} = np*Sum__CPUTime__For_Integration__npcpu/(sum(Log_Info(5:end,Log_Info_Variable))/length( Log_Info(5:end,Log_Info_Variable))); %  based on average
                   %SpeedUp{i} = (sum(Log_Info(5:end,Log_Info_Variable))/length( Log_Info(5:end,Log_Info_Variable)));
                    plot( Number_Processors{i}, SpeedUp{i}, LineColorMarker{i}, 'LineWidth', LineWidth{i} ); %  based on average      
                    fprintf('%d  Length= %d   Case File Name (Full Path)  : %s \n', i, length( Log_Info(5:end,Log_Info_Variable)), Case_Path_Name{i} );
            end
        otherwise        
            k=1;
            while k <= size(Log_Info,1)
                if ( Log_Info(k,1) == LastTime )
                    LastLine = k;
                    break
                else
                    k = k +1;
                end            
            end
            if (doLastLine) 
                LastLine = LastLine1;
                Log_Info(LastLine,1)                
            end
            SpeedUp{i} = np*Sum__CPUTime__For_Integration__npcpu/Log_Info(LastLine,Log_Info_Variable); %  based on last value
           %SpeedUp{i} = Log_Info(LastLine,Log_Info_Variable) %  based on last value
            plot( Number_Processors{i}, SpeedUp{i}, LineColorMarker{i}, 'LineWidth', LineWidth{i} ); %  based on last value
            fprintf('%d  LastLine= %d   Case File Name (Full Path)  : %s \n', i, LastLine, Case_Path_Name{i} );
            %np
            %Sum__CPUTime__For_Integration__npcpu
    end

    %text([2 4 8], [1.1 2.2 3.4],{'  1.1', '  2.2', '  3.4'},'HorizontalAlignment','left')
    text(Number_Processors{i}+0.5, SpeedUp{i},{'    ';num2str(SpeedUp{i})},'HorizontalAlignment','left');
end



if (i_start==1)
    AddLegend(Number_Case);
else
    legend(CaseLegend{3}, CaseLegend{4}); 
end

ideal_x = [1.9  max(cell2mat (Number_Processors))];
ideal_y = [0.9 max(cell2mat (Number_Processors))];
plot( ideal_x, ideal_y, '--b')

% xlim([1.9  Number_Processors{1}]);
% ylim([0.9 Number_Processors{1}/2]);

switch Number_Processors{Number_Case}
   case 64
       set(gca,'XTick',[2 4 8 16 32 64])
       set(gca,'YTick',[2 4 8 16 32 64])
   case 128
       set(gca,'XTick',[2 4 8 16 32 64 128])
       set(gca,'YTick',[2 4 8 16 32 64 128])
   case 256
       set(gca,'XTick',[2 4 8 16 32 64 128 256])
       set(gca,'YTick',[2 4 8 16 32 64 128 256])
   case 512
       set(gca,'XTick',[2 4 8 16 32 64 128 256 512])
       set(gca,'YTick',[2 4 8 16 32 64 128 256 512])
   case 1024
       set(gca,'XTick',[2 4 8 16 32 64 128 256 512 1024])
       set(gca,'YTick',[2 4 8 16 32 64 128 256 512 1024])
       set(gca,'XTick',[64 128 256 512 1024])
       set(gca,'YTick',[64 128 256 512 1024])
   case 2048
       set(gca,'XTick',[2 4 8 16 32 64 128 256 512 1024 2048])
       set(gca,'YTick',[2 4 8 16 32 64 128 256 512 1024 2048])
       set(gca,'XTick',[64 128 256 512 1024 2048])
       set(gca,'YTick',[64 128 256 512 1024 2048])
end

xlim([0 Number_Processors{Number_Case}+10]);
ylim([0 Number_Processors{Number_Case}+10]);

%set(gca,'XTickLabel',{'2', '4', '8', '16', '32', '64', '128'})

ylabel('Speed Up');
%ylabel('Speed Up      based on      CPU_T_i_m_e   for    Time Integration');
xlabel('Number of Processors');
box(gca,'on');


%Timers_No                   %  4- time advancement   5 - parallel_migrate   6 - parallel communicators
switch lower(File_Type)
   case '.p0_log'
      switch (Timers_No)
          case (1) 
              title([Title_Part_1 10 num2str(Resolution_Title),'^3','   \epsilon=',num2str(eps),'   J_{max}=',num2str(J_max_Title),'   \nu=',num2str(nu),'   C_f=',num2str(C_f),'   Re=',num2str(Reynolds),'' 10 'SpeedUp' 10 'based on' 10 'CPU_T_i_m_e   for    Time Integration Time, CUMULATIVE']);
              ylabel('Speed Up      based on      CPU_T_i_m_e   for    Total Time Integration Time, CUMULATIVE');
          case (2)
              title([Title_Part_1 10 num2str(Resolution_Title),'^3','   \epsilon=',num2str(eps),'   J_{max}=',num2str(J_max_Title),'   \nu=',num2str(nu),'   C_f=',num2str(C_f),'   Re=',num2str(Reynolds),'' 10 'SpeedUp' 10 'based on' 10 'CPU_T_i_m_e   for    Main Time Integration Loop Time for each TimeStep']);
              ylabel('Speed Up      based on      CPU_T_i_m_e   for    Main Time Integration Loop Time for each TimeStep');
          case (3)
              title([Title_Part_1 10 num2str(Resolution_Title),'^3','   \epsilon=',num2str(eps),'   J_{max}=',num2str(J_max_Title),'   \nu=',num2str(nu),'   C_f=',num2str(C_f),'   Re=',num2str(Reynolds),'' 10 'SpeedUp' 10 'based on' 10 'CPU_T_i_m_e   for    Adapt Grid, CUMULATIVE']);
              ylabel('Speed Up      based on      CPU_T_i_m_e   for    Adapt Grid, CUMULATIVE');
          case (4)
              title([Title_Part_1 10 num2str(Resolution_Title),'^3','   \epsilon=',num2str(eps),'   J_{max}=',num2str(J_max_Title),'   \nu=',num2str(nu),'   C_f=',num2str(C_f),'   Re=',num2str(Reynolds),'' 10 'SpeedUp' 10 'based on' 10 'CPU_T_i_m_e   for    Time Integration']);
              ylabel('Speed Up      based on      CPU_T_i_m_e   for    Time Integration');
              %title(['Stochastic Coherent Adaptive Large Eddy Simulations (SCALES)' 10 'Forced Homogeneous Turbulence' 10 '\epsilon=0.43   \nu=0.09   C_f=6']);
          case (5)
              title([Title_Part_1 10 num2str(Resolution_Title),'^3','   \epsilon=',num2str(eps),'   J_{max}=',num2str(J_max_Title),'   \nu=',num2str(nu),'   C_f=',num2str(C_f),'   Re=',num2str(Reynolds),'' 10 'SpeedUp' 10 'based on' 10 'CPU_T_i_m_e   for    Parallel Migrate, CUMULATIVE']);
              ylabel('Speed Up      based on      CPU_T_i_m_e   for    Parallel Migrate, CUMULATIVE');
          case (6)
              title([Title_Part_1 10 num2str(Resolution_Title),'^3','   \epsilon=',num2str(eps),'   J_{max}=',num2str(J_max_Title),'   \nu=',num2str(nu),'   C_f=',num2str(C_f),'   Re=',num2str(Reynolds),'' 10 'SpeedUp' 10 'based on' 10 'CPU_T_i_m_e   for    Parallel Communicators, CUMULATIVE']);
              ylabel('Speed Up      based on      CPU_T_i_m_e   for    Parallel Communicators, CUMULATIVE');
      end
   otherwise
      disp('Unknown File_Type.')
end

% title(['CVS  (( Initialized by CVS 0040.res on 64cpu ))' 10 '\epsilon=0.2   \nu=0.09   C_f=6' 10 'SpeedUp' 10 'based on' 10 'CPU_T_i_m_e   for    Time Integration']);



ylabel_hand = get(gca,'ylabel');
set(ylabel_hand,'fontsize',Font_Size_Label);  
xlabel_hand = get(gca,'xlabel');
set(xlabel_hand,'fontsize',Font_Size_Label);  

end




















%% COMPARE  CPU Time  for  Timers_No   ( SpeedUp Based on / np )

if (Plot_TIMERS)
figure
hold on;
i_start=1;
np = Number_Processors{i_start};


%[Line_Number, message, CPUTime__For_Integration]  = textread( Case_CPUTime__Path_Name{7}, '%u %33c %f' );
%Sum__CPUTime__For_Integration__2cpu = sum(CPUTime__For_Integration);
Log_Info = load( Case_Path_Name{i_start} );
Log_Info_Variable = no_columns + J_max(i_start) + no_NWLT + Timers_No;                             %  [pr=??]  NWLT,  GHO,  SEND,  RECV     +    8 TIMER        

switch do_average__TIMERS
   case 'T'
       switch TIMERS_CUMULATIVE
           case 'T'        
               Sum__CPUTime__For_Integration__npcpu = (Log_Info(end,Log_Info_Variable)-Log_Info(5,Log_Info_Variable))/length( Log_Info(5:end,Log_Info_Variable) ); %  based on average
           otherwise
               Sum__CPUTime__For_Integration__npcpu = sum(Log_Info(5:end,Log_Info_Variable))/length( Log_Info(5:end,Log_Info_Variable) ); %  based on average
       end
    otherwise  
        k=1;
        while k <= size(Log_Info,1)
            if ( Log_Info(k,1) == LastTime )
                LastLine = k
                break
            else
                k = k +1;
            end            
        end
        if (doLastLine) 
            LastLine = LastLine1; 
            Log_Info(LastLine,1)
        end
        Sum__CPUTime__For_Integration__npcpu = Log_Info(LastLine,Log_Info_Variable); %  based on last value    
end

    
for i = i_start:Number_Case  % ii:ii+2
    %[Line_Number, message, CPUTime__For_Integration]  = textread( Case_CPUTime__Path_Name{i}, '%u %33c %f' );
    %plot( Number_Processors{i}, Sum__CPUTime__For_Integration__2cpu/sum(CPUTime__For_Integration), LineColorMarker{i}, 'LineWidth', LineWidth{i} );    
    %fprintf('%d  Case CPUTime File Name (Full Path)  : %s \n', i, Case_CPUTime__Path_Name{i} );
    %SppedUp{i} = Sum__CPUTime__For_Integration__2cpu/sum(CPUTime__For_Integration)

    Log_Info = load( Case_Path_Name{i} );
    Log_Info_Variable = no_columns + J_max(i) + no_NWLT + Timers_No;                               %  [pr=??]  NWLT,  GHO,  SEND,  RECV     +    8 TIMER        

    switch do_average__TIMERS   
        case 'T'
            switch TIMERS_CUMULATIVE
                case 'T'
                   %SpeedUp{i} = np*Sum__CPUTime__For_Integration__npcpu/((Log_Info(end,Log_Info_Variable)-Log_Info(5,Log_Info_Variable))/length( Log_Info(5:end,Log_Info_Variable))); %  based on average
                    SpeedUp{i} = ((Log_Info(end,Log_Info_Variable)-Log_Info(5,Log_Info_Variable))/length( Log_Info(5:end,Log_Info_Variable)));
                    plot( Number_Processors{i}, SpeedUp{i}, LineColorMarker{i}, 'LineWidth', LineWidth{i} ); %  based on average      
                    fprintf('%d  Length= %d   Case File Name (Full Path)  : %s \n', i, length( Log_Info(5:end,Log_Info_Variable)), Case_Path_Name{i} );
                otherwise
                   %SpeedUp{i} = np*Sum__CPUTime__For_Integration__npcpu/(sum(Log_Info(5:end,Log_Info_Variable))/length( Log_Info(5:end,Log_Info_Variable))); %  based on average
                    SpeedUp{i} = (sum(Log_Info(5:end,Log_Info_Variable))/length( Log_Info(5:end,Log_Info_Variable)));
                    plot( Number_Processors{i}, SpeedUp{i}, LineColorMarker{i}, 'LineWidth', LineWidth{i} ); %  based on average      
                    fprintf('%d  Length= %d   Case File Name (Full Path)  : %s \n', i, length( Log_Info(5:end,Log_Info_Variable)), Case_Path_Name{i} );
            end
        otherwise        
            k=1;
            while k <= size(Log_Info,1)
                if ( Log_Info(k,1) == LastTime )
                    LastLine = k;
                    break
                else
                    k = k +1;
                end            
            end
            if (doLastLine) 
                LastLine = LastLine1;
                Log_Info(LastLine,1)                
            end
           %SpeedUp{i} = np*Sum__CPUTime__For_Integration__npcpu/Log_Info(LastLine,Log_Info_Variable); %  based on last value
            SpeedUp{i} = Log_Info(LastLine,Log_Info_Variable) %  based on last value
            plot( Number_Processors{i}, SpeedUp{i}, LineColorMarker{i}, 'LineWidth', LineWidth{i} ); %  based on last value
            fprintf('%d  LastLine= %d   Case File Name (Full Path)  : %s \n', i, LastLine, Case_Path_Name{i} );
            %np
            %Sum__CPUTime__For_Integration__npcpu
    end

    %text([2 4 8], [1.1 2.2 3.4],{'  1.1', '  2.2', '  3.4'},'HorizontalAlignment','left')
    text(Number_Processors{i}+0.5, SpeedUp{i},{'    ';num2str(SpeedUp{i})},'HorizontalAlignment','left');
end



if (i_start==1)
    AddLegend(Number_Case);
else
    legend(CaseLegend{3}, CaseLegend{4}); 
end

ideal_x = [1.9  max(cell2mat (Number_Processors))];
ideal_y = [0.9 max(cell2mat (Number_Processors))];
plot( ideal_x, ideal_y, '--b')

% xlim([1.9  Number_Processors{1}]);
% ylim([0.9 Number_Processors{1}/2]);

switch Number_Processors{Number_Case}
   case 64
       set(gca,'XTick',[2 4 8 16 32 64])
       set(gca,'YTick',[2 4 8 16 32 64])
   case 128
       set(gca,'XTick',[2 4 8 16 32 64 128])
       set(gca,'YTick',[2 4 8 16 32 64 128])
   case 256
       set(gca,'XTick',[2 4 8 16 32 64 128 256])
       set(gca,'YTick',[2 4 8 16 32 64 128 256])
   case 512
       set(gca,'XTick',[2 4 8 16 32 64 128 256 512])
       set(gca,'YTick',[2 4 8 16 32 64 128 256 512])
   case 1024
       set(gca,'XTick',[2 4 8 16 32 64 128 256 512 1024])
       set(gca,'YTick',[2 4 8 16 32 64 128 256 512 1024])
       set(gca,'XTick',[64 128 256 512 1024])
       set(gca,'YTick',[64 128 256 512 1024])
   case 2048
       set(gca,'XTick',[2 4 8 16 32 64 128 256 512 1024 2048])
       set(gca,'YTick',[2 4 8 16 32 64 128 256 512 1024 2048])
       set(gca,'XTick',[64 128 256 512 1024 2048])
       set(gca,'YTick',[64 128 256 512 1024 2048])
end

xlim([0 Number_Processors{Number_Case}+10]);
ylim([0 Number_Processors{Number_Case}+10]);

%set(gca,'XTickLabel',{'2', '4', '8', '16', '32', '64', '128'})

ylabel('Speed Up');
%ylabel('Speed Up      based on      CPU_T_i_m_e   for    Time Integration');
xlabel('Number of Processors');
box(gca,'on');


%Timers_No                   %  4- time advancement   5 - parallel_migrate   6 - parallel communicators
switch lower(File_Type)
   case '.p0_log'
      switch (Timers_No)
          case (1) 
              title([Title_Part_1 10 num2str(Resolution_Title),'^3','   \epsilon=',num2str(eps),'   J_{max}=',num2str(J_max_Title),'   \nu=',num2str(nu),'   C_f=',num2str(C_f),'   Re=',num2str(Reynolds),'' 10 'SpeedUp' 10 'based on' 10 'CPU_T_i_m_e   for    Time Integration Time, CUMULATIVE']);
              ylabel('CPU_T_i_m_e   for    Total Time Integration Time, CUMULATIVE');
          case (2)
              title([Title_Part_1 10 num2str(Resolution_Title),'^3','   \epsilon=',num2str(eps),'   J_{max}=',num2str(J_max_Title),'   \nu=',num2str(nu),'   C_f=',num2str(C_f),'   Re=',num2str(Reynolds),'' 10 'SpeedUp' 10 'based on' 10 'CPU_T_i_m_e   for    Main Time Integration Loop Time for each TimeStep']);
              ylabel('CPU_T_i_m_e   for    Main Time Integration Loop Time for each TimeStep');
          case (3)
              title([Title_Part_1 10 num2str(Resolution_Title),'^3','   \epsilon=',num2str(eps),'   J_{max}=',num2str(J_max_Title),'   \nu=',num2str(nu),'   C_f=',num2str(C_f),'   Re=',num2str(Reynolds),'' 10 'SpeedUp' 10 'based on' 10 'CPU_T_i_m_e   for    Adapt Grid, CUMULATIVE']);
              ylabel('CPU_T_i_m_e   for    Adapt Grid, CUMULATIVE');
          case (4)
              title([Title_Part_1 10 num2str(Resolution_Title),'^3','   \epsilon=',num2str(eps),'   J_{max}=',num2str(J_max_Title),'   \nu=',num2str(nu),'   C_f=',num2str(C_f),'   Re=',num2str(Reynolds),'' 10 'SpeedUp' 10 'based on' 10 'CPU_T_i_m_e   for    Time Integration']);
              ylabel('CPU_T_i_m_e   for    Time Integration');
              %title(['Stochastic Coherent Adaptive Large Eddy Simulations (SCALES)' 10 'Forced Homogeneous Turbulence' 10 '\epsilon=0.43   \nu=0.09   C_f=6']);
          case (5)
              title([Title_Part_1 10 num2str(Resolution_Title),'^3','   \epsilon=',num2str(eps),'   J_{max}=',num2str(J_max_Title),'   \nu=',num2str(nu),'   C_f=',num2str(C_f),'   Re=',num2str(Reynolds),'' 10 'SpeedUp' 10 'based on' 10 'CPU_T_i_m_e   for    Parallel Migrate, CUMULATIVE']);
              ylabel('CPU_T_i_m_e   for    Parallel Migrate, CUMULATIVE');
          case (6)
              title([Title_Part_1 10 num2str(Resolution_Title),'^3','   \epsilon=',num2str(eps),'   J_{max}=',num2str(J_max_Title),'   \nu=',num2str(nu),'   C_f=',num2str(C_f),'   Re=',num2str(Reynolds),'' 10 'SpeedUp' 10 'based on' 10 'CPU_T_i_m_e   for    Parallel Communicators, CUMULATIVE']);
              ylabel('CPU_T_i_m_e   for    Parallel Communicators, CUMULATIVE');
      end
   otherwise
      disp('Unknown File_Type.')
end

% title(['CVS  (( Initialized by CVS 0040.res on 64cpu ))' 10 '\epsilon=0.2   \nu=0.09   C_f=6' 10 'SpeedUp' 10 'based on' 10 'CPU_T_i_m_e   for    Time Integration']);



ylabel_hand = get(gca,'ylabel');
set(ylabel_hand,'fontsize',Font_Size_Label);  
xlabel_hand = get(gca,'xlabel');
set(xlabel_hand,'fontsize',Font_Size_Label);  

end















%% COMPARE  CPU Time     Time Integration / Communicaation     

if (Plot_TimeIntegration_to_Communication)

clear SpeedUp
figure
hold on;
i_start=1;
np = Number_Processors{i_start};




    
for i = i_start:Number_Case  % ii:ii+2

    Log_Info = load( Case_Path_Name{i} );
    Log_Info_Variable_Num = no_columns + J_max(i) + no_NWLT + 4;          %  [pr=??]  NWLT,  GHO,  SEND,  RECV     +    8 TIMER        
    Log_Info_Variable_Den = no_columns + J_max(i) + no_NWLT + 6;          %  [pr=??]  NWLT,  GHO,  SEND,  RECV     +    8 TIMER        


    % NOTE:  Denominator is     CUMULATIVE  (Communication    Timer)     but
    %        Numerator   is not CUMULATIVE  (Time-Integration Timer)
    Numerator   = sum(Log_Info(5:end,Log_Info_Variable_Num))/length( Log_Info(5:end,Log_Info_Variable_Num));
    Denominator = (Log_Info(end,Log_Info_Variable_Den)-Log_Info(5,Log_Info_Variable_Den))/length( Log_Info(5:end,Log_Info_Variable_Den));
    SpeedUp{i}   = Numerator / Denominator;
    plot( Number_Processors{i}, SpeedUp{i}, LineColorMarker{i}, 'LineWidth', LineWidth{i} ); %  based on average      
    fprintf('%d  Length= %d   Case File Name (Full Path)  : %s \n', i, length( Log_Info(5:end,Log_Info_Variable)), Case_Path_Name{i} );

                     
%     switch do_average__TIMERS   
%         case 'T'
%             switch TIMERS_CUMULATIVE
%                 case 'T'
%                     Numerator   = ((Log_Info(end,Log_Info_Variable_Num)-Log_Info(5,Log_Info_Variable_Num))/length( Log_Info(5:end,Log_Info_Variable_Num)));
%                     Denominator = ((Log_Info(end,Log_Info_Variable_Den)-Log_Info(5,Log_Info_Variable_Den))/length( Log_Info(5:end,Log_Info_Variable_Den)));
%                     SpeedUp{i}   = Numerator / Denominator;
%                     plot( Number_Processors{i}, SpeedUp{i}, LineColorMarker{i}, 'LineWidth', LineWidth{i} ); %  based on average      
%                     fprintf('%d  Length= %d   Case File Name (Full Path)  : %s \n', i, length( Log_Info(5:end,Log_Info_Variable)), Case_Path_Name{i} );
%                 otherwise
%                     Numerator   = (sum(Log_Info(5:end,Log_Info_Variable_Num))/length( Log_Info(5:end,Log_Info_Variable_Num)));
%                     Denominator = (sum(Log_Info(5:end,Log_Info_Variable_Den))/length( Log_Info(5:end,Log_Info_Variable_Den)));
%                     SpeedUp{i}  = Numerator / Denominator;
%                     plot( Number_Processors{i}, SpeedUp{i}, LineColorMarker{i}, 'LineWidth', LineWidth{i} ); %  based on average      
%                     fprintf('%d  Length= %d   Case File Name (Full Path)  : %s \n', i, length( Log_Info(5:end,Log_Info_Variable)), Case_Path_Name{i} );
%             end
%         otherwise        
%             k=1;
%             while k <= size(Log_Info,1)
%                 if ( Log_Info(k,1) == LastTime )
%                     LastLine = k;
%                     break
%                 else
%                     k = k +1;
%                 end            
%             end
%             if (doLastLine) 
%                 LastLine = LastLine1;
%                 Log_Info(LastLine,1)                
%             end
%             Numerator   = Log_Info(LastLine,Log_Info_Variable); %  based on last value
%             Denominator = Log_Info(LastLine,Log_Info_Variable); %  based on last value
%             SpeedUp{i}  = Numerator / Denominator;
%             plot( Number_Processors{i}, SpeedUp{i}, LineColorMarker{i}, 'LineWidth', LineWidth{i} ); %  based on last value
%             fprintf('%d  LastLine= %d   Case File Name (Full Path)  : %s \n', i, LastLine, Case_Path_Name{i} );
%             %np
%             %Sum__CPUTime__For_Integration__npcpu
%     end

    %text([2 4 8], [1.1 2.2 3.4],{'  1.1', '  2.2', '  3.4'},'HorizontalAlignment','left')
    text(Number_Processors{i}+0.5, SpeedUp{i},{'    ';num2str(SpeedUp{i})},'HorizontalAlignment','left');
end



if (i_start==1)
    AddLegend(Number_Case);
else
    legend(CaseLegend{3}, CaseLegend{4}); 
end

% ideal_x = [1.9  max(cell2mat (Number_Processors))];
% ideal_y = [0.9 max(cell2mat (Number_Processors))];
% plot( ideal_x, ideal_y, '--b')

% xlim([1.9  Number_Processors{1}]);
% ylim([0.9 Number_Processors{1}/2]);

switch Number_Processors{Number_Case}
   case 64
       set(gca,'XTick',[2 4 8 16 32 64])
   case 128
       set(gca,'XTick',[2 4 8 16 32 64 128])
   case 256
       set(gca,'XTick',[2 4 8 16 32 64 128 256])
   case 512
       set(gca,'XTick',[2 4 8 16 32 64 128 256 512])
   case 1024
       set(gca,'XTick',[2 4 8 16 32 64 128 256 512 1024])
       set(gca,'XTick',[64 128 256 512 1024])
   case 2048
       set(gca,'XTick',[2 4 8 16 32 64 128 256 512 1024 2048])
       set(gca,'XTick',[64 128 256 512 1024 2048])
end

xlim([0 Number_Processors{Number_Case}+10]);
%ylim([0.4 1.2]);

%set(gca,'XTickLabel',{'2', '4', '8', '16', '32', '64', '128'})

ylabel('Speed Up');
%ylabel('Speed Up      based on      CPU_T_i_m_e   for    Time Integration');
xlabel('Number of Processors');
box(gca,'on');


title([Title_Part_1 10 num2str(Resolution_Title),'^3','   \epsilon=',num2str(eps),'   J_{max}=',num2str(J_max_Title),'   \nu=',num2str(nu),'   C_f=',num2str(C_f),'   Re=',num2str(Reynolds),'' 10 'SpeedUp' 10 'based on' 10 'CPU_T_i_m_e   for    Time Integration']);
ylabel('Time Integration  CPU_T_i_m_e   /   Parallel Communicators  CPU_T_i_m_e');


ylabel_hand = get(gca,'ylabel');
set(ylabel_hand,'fontsize',Font_Size_Label);  
xlabel_hand = get(gca,'xlabel');
set(xlabel_hand,'fontsize',Font_Size_Label);  

end






























%% COMPARE  CPU Time for Time-Integration  ( SpeedUp )

% figure
% hold on;
% 
% for i = 1:Number_Case    
%     [Line_Number, message, CPUTime__For_Integration]  = textread( Case_CPUTime__Path_Name{i}, '%u %33c %f' );
%     plot( Number_Processors{i}, sum(CPUTime__For_Integration), LineColorMarker{i}, 'LineWidth', LineWidth{i} );    
%     fprintf('%d  Case CPUTime File Name (Full Path)  : %s \n', i, Case_CPUTime__Path_Name{i} );
% end
% 
% 
% title(['CVS  (( Initialized by CVS 0040.res on 64cpu ))' 10 '\epsilon=0.2   \nu=0.09   C_f=5.2' 10 'SpeedUp' 10 'based on' 10 'CPU_T_i_m_e   for    Time Integration']);
% 
% %xlim([0 20.0]);
% %ylim([0 0.1]);
% 
% set(gca,'XTick',[2 4 8 16 32 64 128])
% %set(gca,'XTickLabel',{'2', '4', '8', '16', '32', '64', '128'})
% 
% ylabel('{\Sigma} CPU_{Time}_{_T_i_m_e _I_n_t_e_g_r_a_t_i_o_n}    over All Time-Steps');
% xlabel('Number of Processors');









%% COMPARE  misballance  ( min,mean,max, min/mean max/mean )

if (Plot_MissBalance) 
    
    
    
%%% nwlt (mean,max,min)
figure
hold on;

for i = 1:Number_Case     

    Log_Info = load( Case_Path_Name{i} );

    for j=2:length(Log_Info(:,1))
        length(Log_Info(:,1));
        
        for k = 1:Number_Processors{i}        
            var_number = no_columns + J_max(i) + 1 + (k-1)*(4+Number_of_Timers);             %  [pr=??]  NWLT,  GHO,  SEND,  RECV     +    8 TIMER        
            Nwlt_per_proc(k) = Log_Info(j, var_number);    
        end
        
        Nwlt_mean(i,j) =  mean( Nwlt_per_proc );
        Nwlt_std(i,j)  =  std(  Nwlt_per_proc );
        Nwlt_min(i,j)  =  min(  Nwlt_per_proc );
        Nwlt_max(i,j)  =  max(  Nwlt_per_proc );
    
    end
        
%     plot( Log_Info(2:end,1), Nwlt_mean(i,2:length(Log_Info(:,1))), LineColorMarker{i}, 'LineWidth', LineWidth{i} );
%     plot( Log_Info(2:end,1), Nwlt_std(i,2:length(Log_Info(:,1))), LineColorMarker{i}, 'LineWidth', LineWidth{i} );
%     plot( Log_Info(2:end,1), Nwlt_min(i,2:length(Log_Info(:,1))), LineColorMarker{i}, 'LineWidth', LineWidth{i} );
%     plot( Log_Info(2:end,1), Nwlt_max(i,2:length(Log_Info(:,1))), LineColorMarker{i}, 'LineWidth', LineWidth{i} );

    plot( Log_Info(2:end,1), Nwlt_mean(i,2:length(Log_Info(:,1))), LineColorMarker_avg{i}, 'LineWidth', LineWidth{i} );
    plot( Log_Info(2:end,1),  Nwlt_min(i,2:length(Log_Info(:,1))), LineColorMarker_min{i}, 'LineWidth', LineWidth{i} );
    plot( Log_Info(2:end,1),  Nwlt_max(i,2:length(Log_Info(:,1))), LineColorMarker_max{i}, 'LineWidth', LineWidth{i} );
    
    clear Nwlt_per_proc

end


% AddLegend(Number_Case);
%legend(CaseLegend{1}, CaseLegend{2}, CaseLegend{3}, CaseLegend{4}, CaseLegend{7}, CaseLegend{10}, CaseLegend{13}, CaseLegend{8}, CaseLegend{9}, CaseLegend{10} ); 
%legend('2   cpu  mean','2   cpu  min','2   cpu  max', '4   cpu  mean','4   cpu  min','4   cpu  max', '8   cpu  mean','8   cpu  min','8   cpu  max', ...
%       '16  cpu  mean','16  cpu  min','16  cpu  max', '32  cpu  mean','32  cpu min','32  cpu  max', '64  cpu  mean','64  cpu  min','64  cpu  max');
%title(['Stochastic Coherent Adaptive Large Eddy Simulations (SCALES)' 10 'Forced Homogeneous Turbulence' 10 '\epsilon=0.43   \nu=0.09   C_f=6']);
title([Title_Part_1 10 num2str(Resolution_Title),'^3','   \epsilon=',num2str(eps),'   J_{max}=',num2str(J_max_Title),'   \nu=',num2str(nu),'   C_f=',num2str(C_f),'   Re=',num2str(Reynolds),'']);

xlabel('Time');                     % ylabel('mean nwlt');% ylabel('std  nwlt');% ylabel('min nwlt');% ylabel('max nwlt');
ylabel('(mean, min, max)  nwlt');


xlim([0.0  x_max]);


ylabel_hand = get(gca,'ylabel');
set(ylabel_hand,'fontsize',Font_Size_Label);  
xlabel_hand = get(gca,'xlabel');
set(xlabel_hand,'fontsize',Font_Size_Label);  









%%% nwlt (variance)
figure
hold on;

for i = 1:Number_Case     

    Log_Info = load( Case_Path_Name{i} );

    for j=2:length(Log_Info(:,1))
        length(Log_Info(:,1));
        
        for k = 1:Number_Processors{i}        
            var_number = no_columns + J_max(i) + 1 + (k-1)*(no_NWLT+Number_of_Timers);             %  [pr=??]  NWLT,  GHO,  SEND,  RECV     +    8 TIMER        
            Nwlt_per_proc(k) = Log_Info(j, var_number);    
        end
        
        Nwlt_mean(i,j) =  mean( Nwlt_per_proc );
        Nwlt_std(i,j)  =  std(  Nwlt_per_proc );
        Nwlt_min(i,j)  =  min(  Nwlt_per_proc );
        Nwlt_max(i,j)  =  max(  Nwlt_per_proc );
    
    end
        
%     plot( Log_Info(2:end,1), Nwlt_mean(i,2:length(Log_Info(:,1))), LineColorMarker{i}, 'LineWidth', LineWidth{i} );
%     plot( Log_Info(2:end,1), Nwlt_std(i,2:length(Log_Info(:,1))), LineColorMarker{i}, 'LineWidth', LineWidth{i} );
%     plot( Log_Info(2:end,1), Nwlt_min(i,2:length(Log_Info(:,1))), LineColorMarker{i}, 'LineWidth', LineWidth{i} );
%     plot( Log_Info(2:end,1), Nwlt_max(i,2:length(Log_Info(:,1))), LineColorMarker{i}, 'LineWidth', LineWidth{i} );

    plot( Log_Info(2:end,1),  Nwlt_std(i,2:length(Log_Info(:,1))), LineColorMarker_max{i}, 'LineWidth', LineWidth{i} );
    
    clear Nwlt_per_proc

end


% AddLegend(Number_Case);
%legend(CaseLegend{1}, CaseLegend{2}, CaseLegend{3}, CaseLegend{4}, CaseLegend{7}, CaseLegend{10}, CaseLegend{13}, CaseLegend{8}, CaseLegend{9}, CaseLegend{10} ); 
%legend('2   cpu  mean','2   cpu  min','2   cpu  max', '4   cpu  mean','4   cpu  min','4   cpu  max', '8   cpu  mean','8   cpu  min','8   cpu  max', ...
%       '16  cpu  mean','16  cpu  min','16  cpu  max', '32  cpu  mean','32  cpu min','32  cpu  max', '64  cpu  mean','64  cpu  min','64  cpu  max');
%title(['Stochastic Coherent Adaptive Large Eddy Simulations (SCALES)' 10 'Forced Homogeneous Turbulence' 10 '\epsilon=0.43   \nu=0.09   C_f=6']);
title([Title_Part_1 10 num2str(Resolution_Title),'^3','   \epsilon=',num2str(eps),'   J_{max}=',num2str(J_max_Title),'   \nu=',num2str(nu),'   C_f=',num2str(C_f),'   Re=',num2str(Reynolds),'']);

xlabel('Time');                     % ylabel('mean nwlt');% ylabel('std  nwlt');% ylabel('min nwlt');% ylabel('max nwlt');
ylabel('(variance)  nwlt');


xlim([0.0  x_max]);

ylabel_hand = get(gca,'ylabel');
set(ylabel_hand,'fontsize',Font_Size_Label);  
xlabel_hand = get(gca,'xlabel');
set(xlabel_hand,'fontsize',Font_Size_Label);  











%%% Ratio of   max(nwlt) / mean(nwlt)
figure
hold on;


for i = 1:Number_Case     

    clear Log_Info
    Log_Info = load( Case_Path_Name{i} );

    for j=1:length(Log_Info(:,1))
        length(Log_Info(:,1));
        
        for k = 1:Number_Processors{i}        
            var_number = no_columns + J_max(i) + 1 + (k-1)*(no_NWLT+Number_of_Timers);             %  [pr=??]  NWLT,  GHO,  SEND,  RECV     +    8 TIMER        
            Nwlt_per_proc(k) = Log_Info(j, var_number);    
        end
        
        Nwlt_mean(i,j) =  mean( Nwlt_per_proc );
        Nwlt_min(i,j)  =  min(  Nwlt_per_proc );
        Nwlt_max(i,j)  =  max(  Nwlt_per_proc );
        Nwlt_maxmean(i,j)  =  max(  Nwlt_per_proc ) / mean( Nwlt_per_proc );
        Nwlt_minmean(i,j)  =  min(  Nwlt_per_proc ) / mean( Nwlt_per_proc );        
    
    end
        
    plot( Log_Info(2:end,1), Nwlt_maxmean(i,2:length(Log_Info(:,1))), LineColorMarker_avg{i}, 'LineWidth', LineWidth{i} );

    clear Nwlt_per_proc

end

xlabel('Time');
ylabel('(max/mean)  nwlt');

xlim([0.0  x_max]);

AddLegend(Number_Case);
title([Title_Part_1 10 num2str(Resolution_Title),'^3','   \epsilon=',num2str(eps),'   J_{max}=',num2str(J_max_Title),'   \nu=',num2str(nu),'   C_f=',num2str(C_f),'   Re=',num2str(Reynolds),'']);

ylabel_hand = get(gca,'ylabel');
set(ylabel_hand,'fontsize',Font_Size_Label);  
xlabel_hand = get(gca,'xlabel');
set(xlabel_hand,'fontsize',Font_Size_Label);  









%%% Ratio of   CPU-Time(time advancement)   /   mean(nwlt)
figure
hold on;


for i = 1:Number_Case     

    clear Log_Info
    Log_Info = load( Case_Path_Name{i} );

    for j=1:length(Log_Info(:,1))
        length(Log_Info(:,1));
        
        for k = 1:Number_Processors{i}        
            var_number = no_columns + J_max(i) + 1 + (k-1)*(no_NWLT+Number_of_Timers);             %  [pr=??]  NWLT,  GHO,  SEND,  RECV     +    8 TIMER        
            Nwlt_per_proc(k) = Log_Info(j, var_number);    
        end
        
        Nwlt_mean(i,j) =  mean( Nwlt_per_proc );
        Nwlt_min(i,j)  =  min(  Nwlt_per_proc );
        Nwlt_max(i,j)  =  max(  Nwlt_per_proc );
        Nwlt_maxmean(i,j)  =  max(  Nwlt_per_proc ) / mean( Nwlt_per_proc );
        Nwlt_minmean(i,j)  =  min(  Nwlt_per_proc ) / mean( Nwlt_per_proc );        
    
    end
        
    Log_Info_Variable = no_columns + J_max(i) + 4 + 4;%=Timers_No;                           %  [pr=??]  NWLT,  GHO,  SEND,  RECV     +    8 TIMER        
    mean_nwlt = Nwlt_mean(i,1:length(Log_Info(:,1)));
    cpu_time_adv=Log_Info(:,Log_Info_Variable);
    y_axis_temp = cpu_time_adv'./mean_nwlt;
    plot( Log_Info(:,1), y_axis_temp, LineColorMarker{i}, 'LineWidth', LineWidth{i} );    

    clear Nwlt_per_proc

end

xlabel('Time');
ylabel('CPU-Time(time advancement)   /   mean(nwlt)');

xlim([0.0  x_max]);

AddLegend(Number_Case);
title([Title_Part_1 10 num2str(Resolution_Title),'^3','   \epsilon=',num2str(eps),'   J_{max}=',num2str(J_max_Title),'   \nu=',num2str(nu),'   C_f=',num2str(C_f),'   Re=',num2str(Reynolds),'']);

ylabel_hand = get(gca,'ylabel');
set(ylabel_hand,'fontsize',Font_Size_Label);  
xlabel_hand = get(gca,'xlabel');
set(xlabel_hand,'fontsize',Font_Size_Label);  










%%% NGhost (mean,max,min)
figure
hold on;

for i = 1:Number_Case     

    Log_Info = load( Case_Path_Name{i} );

    for j=2:length(Log_Info(:,1))
        length(Log_Info(:,1));
        
        for k = 1:Number_Processors{i}        
            var_number = no_columns + J_max(i) + 2 + (k-1)*(no_NWLT+Number_of_Timers);             %  [pr=??]  NWLT,  GHO,  SEND,  RECV     +    8 TIMER        
            Nwlt_per_proc(k) = Log_Info(j, var_number);    
        end
        
        Nwlt_mean(i,j) =  mean( Nwlt_per_proc );
        Nwlt_std(i,j)  =  std(  Nwlt_per_proc );
        Nwlt_min(i,j)  =  min(  Nwlt_per_proc );
        Nwlt_max(i,j)  =  max(  Nwlt_per_proc );
    
    end
        
%     plot( Log_Info(2:end,1), Nwlt_mean(i,2:length(Log_Info(:,1))), LineColorMarker{i}, 'LineWidth', LineWidth{i} );
%     plot( Log_Info(2:end,1), Nwlt_std(i,2:length(Log_Info(:,1))), LineColorMarker{i}, 'LineWidth', LineWidth{i} );
%     plot( Log_Info(2:end,1), Nwlt_min(i,2:length(Log_Info(:,1))), LineColorMarker{i}, 'LineWidth', LineWidth{i} );
%     plot( Log_Info(2:end,1), Nwlt_max(i,2:length(Log_Info(:,1))), LineColorMarker{i}, 'LineWidth', LineWidth{i} );

    plot( Log_Info(2:end,1), Nwlt_mean(i,2:length(Log_Info(:,1))), LineColorMarker_avg{i}, 'LineWidth', LineWidth{i} );
    plot( Log_Info(2:end,1),  Nwlt_min(i,2:length(Log_Info(:,1))), LineColorMarker_min{i}, 'LineWidth', LineWidth{i} );
    plot( Log_Info(2:end,1),  Nwlt_max(i,2:length(Log_Info(:,1))), LineColorMarker_max{i}, 'LineWidth', LineWidth{i} );
    
    clear Nwlt_per_proc

end


title([Title_Part_1 10 num2str(Resolution_Title),'^3','   \epsilon=',num2str(eps),'   J_{max}=',num2str(J_max_Title),'   \nu=',num2str(nu),'   C_f=',num2str(C_f),'   Re=',num2str(Reynolds),'']);

ylabel('(mean, min, max)  NGhost');
xlabel('Time');

xlim([0.0  x_max]);

ylabel_hand = get(gca,'ylabel');
set(ylabel_hand,'fontsize',Font_Size_Label);  
xlabel_hand = get(gca,'xlabel');
set(xlabel_hand,'fontsize',Font_Size_Label);  










%%% NGhost/nwlt (mean,max,min)
figure
hold on;

for i = 1:Number_Case     

    Log_Info = load( Case_Path_Name{i} );

    for j=2:length(Log_Info(:,1))
        length(Log_Info(:,1));
        
        for k = 1:Number_Processors{i}        
            var_number_nwlt   = no_columns + J_max(i) + 1 + (k-1)*(no_NWLT+Number_of_Timers);       %  [pr=??]  NWLT,  GHO,  SEND,  RECV     +    8 TIMER        
            var_number_NGhost = no_columns + J_max(i) + 2 + (k-1)*(no_NWLT+Number_of_Timers);       %  [pr=??]  NWLT,  GHO,  SEND,  RECV     +    8 TIMER        
            Nwlt_per_proc(k)  = Log_Info(j, var_number_NGhost) / Log_Info(j, var_number_nwlt);
        end
        
        Nwlt_mean(i,j) =  mean( Nwlt_per_proc );
        Nwlt_std(i,j)  =  std(  Nwlt_per_proc );
        Nwlt_min(i,j)  =  min(  Nwlt_per_proc );
        Nwlt_max(i,j)  =  max(  Nwlt_per_proc );
    
    end
        
%     plot( Log_Info(2:end,1), Nwlt_mean(i,2:length(Log_Info(:,1))), LineColorMarker{i}, 'LineWidth', LineWidth{i} );
%     plot( Log_Info(2:end,1), Nwlt_std(i,2:length(Log_Info(:,1))), LineColorMarker{i}, 'LineWidth', LineWidth{i} );
%     plot( Log_Info(2:end,1), Nwlt_min(i,2:length(Log_Info(:,1))), LineColorMarker{i}, 'LineWidth', LineWidth{i} );
%     plot( Log_Info(2:end,1), Nwlt_max(i,2:length(Log_Info(:,1))), LineColorMarker{i}, 'LineWidth', LineWidth{i} );

    plot( Log_Info(2:end,1), Nwlt_mean(i,2:length(Log_Info(:,1))), LineColorMarker_avg{i}, 'LineWidth', LineWidth{i} );
    plot( Log_Info(2:end,1),  Nwlt_min(i,2:length(Log_Info(:,1))), LineColorMarker_min{i}, 'LineWidth', LineWidth{i} );
    plot( Log_Info(2:end,1),  Nwlt_max(i,2:length(Log_Info(:,1))), LineColorMarker_max{i}, 'LineWidth', LineWidth{i} );
    
    clear Nwlt_per_proc

end


title([Title_Part_1 10 num2str(Resolution_Title),'^3','   \epsilon=',num2str(eps),'   J_{max}=',num2str(J_max_Title),'   \nu=',num2str(nu),'   C_f=',num2str(C_f),'   Re=',num2str(Reynolds),'']);

ylabel('(mean, min, max)  NGhost/nwlt');
xlabel('Time');

xlim([0.0  x_max]);

ylabel_hand = get(gca,'ylabel');
set(ylabel_hand,'fontsize',Font_Size_Label);  
xlabel_hand = get(gca,'xlabel');
set(xlabel_hand,'fontsize',Font_Size_Label);  








end



end





function AddLegend(Number_Legend)
global CaseLegend
global Font_Size

switch Number_Legend
    case (1)
            LL=legend(CaseLegend{1}); 
    case (2)
            LL=legend(CaseLegend{1}, CaseLegend{2}); 
    case (3)
            LL=legend(CaseLegend{1}, CaseLegend{2}, CaseLegend{3}); 
    case (4)
            LL=legend(CaseLegend{1}, CaseLegend{2}, CaseLegend{3}, CaseLegend{4}); 
    case (5)
            LL=legend(CaseLegend{1}, CaseLegend{2}, CaseLegend{3}, CaseLegend{4}, CaseLegend{5}); 
    case (6)
            LL=legend(CaseLegend{1}, CaseLegend{2}, CaseLegend{3}, CaseLegend{4}, CaseLegend{5}, CaseLegend{6}); 
    case (7)
            LL=legend(CaseLegend{1}, CaseLegend{2}, CaseLegend{3}, CaseLegend{4}, CaseLegend{5}, CaseLegend{6}, CaseLegend{7}); 
    case (8)
            LL=legend(CaseLegend{1}, CaseLegend{2}, CaseLegend{3}, CaseLegend{4}, CaseLegend{5}, CaseLegend{6}, CaseLegend{7}, CaseLegend{8}); 
    case (9)
            LL=legend(CaseLegend{1}, CaseLegend{2}, CaseLegend{3}, CaseLegend{4}, CaseLegend{5}, CaseLegend{6}, CaseLegend{7}, CaseLegend{8}, CaseLegend{9}); 
    case (10)
            LL=legend(CaseLegend{1}, CaseLegend{2}, CaseLegend{3}, CaseLegend{4}, CaseLegend{5}, CaseLegend{6}, CaseLegend{7}, CaseLegend{8}, CaseLegend{9}, CaseLegend{10} ); 
    case (11)
            LL=legend(CaseLegend{1}, CaseLegend{2}, CaseLegend{3}, CaseLegend{4}, CaseLegend{5}, CaseLegend{6}, CaseLegend{7}, CaseLegend{8}, CaseLegend{9}, CaseLegend{10}, CaseLegend{11} ); 
    case (12)
            LL=legend(CaseLegend{1}, CaseLegend{2}, CaseLegend{3}, CaseLegend{4}, CaseLegend{5}, CaseLegend{6}, CaseLegend{7}, CaseLegend{8}, CaseLegend{9}, CaseLegend{10}, CaseLegend{11}, CaseLegend{12} ); 
    case (14)
            LL=legend(CaseLegend{1}, CaseLegend{2}, CaseLegend{3}, CaseLegend{4}, CaseLegend{5}, CaseLegend{6}, CaseLegend{7}, CaseLegend{8}, CaseLegend{9}, CaseLegend{10}, CaseLegend{11}, CaseLegend{12}, CaseLegend{13}, CaseLegend{14} ); 
    case (22)
            LL=legend(CaseLegend{1}, CaseLegend{2}, CaseLegend{3}, CaseLegend{4}, CaseLegend{5}, CaseLegend{6}, CaseLegend{7}, CaseLegend{8}, CaseLegend{9}, CaseLegend{10}, CaseLegend{11}, CaseLegend{12}, CaseLegend{13}, CaseLegend{14}, CaseLegend{15}, CaseLegend{16}, CaseLegend{17}, CaseLegend{18}, CaseLegend{19}, CaseLegend{20}, CaseLegend{21}, CaseLegend{22} ); 
    otherwise
            disp('Unknown Legend Number')
end

%legend('Variable \epsilon','Constant \epsilon');
%legend('\fontsize{16} {\epsilon}       \fontsize{11} Np=Nu=2','\fontsize{16} {|| f ||}_{\infty}       \fontsize{11} Np=Nu=2', '\fontsize{16} {\epsilon}       \fontsize{11} Np=3 Nu=2','\fontsize{16} {|| f ||}_{\infty}       \fontsize{11} Np=3 Nu=2',  '\fontsize{16} {\epsilon}       \fontsize{11} Np=4 Nu=4','\fontsize{16} {|| f ||}_{\infty}       \fontsize{11} Np=4 Nu=4')

% set(LL, 'Box','off');
% set(LL, 'FontName','Times New-Roman');
set(LL, 'FontSize',Font_Size-4); 
set(LL, 'Location','NorthEast');   %NorthWest  SouthEast  SouthWest

    
end
