#!/bin/sh
# run test cases and compare results for numerous rules
# for different data bases and test cases
# (Cartesian product of list_db and list_ca)
TITLE="The testing suite for the code developers"
VERSION="061113"

# MAX_SINGLE_RUN_TIME_3 and RUN_TIME_CHECK_3 to be sourced from local parameter file
function wait_or_kill {       # argument: PID of process to control
    time_to_sleep=$RUN_TIME_CHECK_3
    while [ $time_to_sleep -le $MAX_SINGLE_RUN_TIME_3 ]; do
	\sleep $RUN_TIME_CHECK_3
	if [ ! "`\ps -e|\grep $1`" ]; then
	    return `wait $1`
	fi
	time_to_sleep=`expr $time_to_sleep + $RUN_TIME_CHECK_3`
    done
    if [ "`\ps -e|\grep $1`" ]; then
	\kill -9 $1 1>/dev/null 2>/dev/null
	return 4
    else
	return `wait $1`
    fi
}


function err {
    echo -e "\tRun as \"sh TestCases/util/RUN_3.sh \" from the main directory or"
    echo -e "\tas \"sh util/RUN_3.sh\" from the directory TestCases or"
    echo -e "\tas \"sh RUN_3.sh\" from the directory util"
    echo -e "\tOptional parameters: output stream, error stream. You might like to indicate"
    echo -e "\terror stream ( as 2>/dev/null ) to suppress messages about killed subprocesses."
    exit 1
}


# set output streams
o1=/dev/null
o2=/dev/null
if [ "$1" ]; then o1=$1; fi
if [ "$2" ]; then o2=$2; fi


function print_status() {
    if [ $1 -eq 0 ]; then
	echo -ne "  [OK]    \t"
    else
	echo -ne "  [FAILED]\t"
	if [ $1 -eq 4 ]; then echo -ne "[ KILLED ]\t"; fi
    fi
    echo -e "$2"
}


# print invitation message
echo -e "\t$TITLE. Version $VERSION"

# check the current directory
if [ -f RUN.sh ]; then cd ../../
elif [ -f util/RUN.sh ]; then cd ..
elif [ ! -d TestCases ]; then err
fi
cd TestCases

echo "#--------------------------"
echo "#     run test [ TestCases/util/RUN_3.sh ]"
echo "#--------------------------"

# source the lists
source util/RUN_lists.txt 1>>$o1 2>>$o2

# initialize database pairs
db_pairs=`\sh util/init_pairs.sh $list_db`
np2=`echo $db_pairs|\wc -w`


for ca in $list_ca; do
    # set names
    echo -e "\n# CASE = $ca"
    cd=`echo $ca | \sed -e "s/\/.*//"`
    cn=`echo $ca | \sed -e "s/.*\///"`
    \mkdir -p "${cd}/results"
    
    if [ -s ${cd}/test_in/local_test_parameters.txt ]; then
	# load local test parameters
	source ${cd}/test_in/local_test_parameters.txt 1>>$o1 2>>$o2
	# issue warning about allow to change accuracy flag
	if [ ${ALLOW_ACCURACY_CHANGE} = "yes" ]; then
	    echo -e "\t\t ..............."
	    echo -e "\t\t ... Warning ..."
	    echo -e "\t\t ... Accuracy is allowed to be degraded for exact solution comparisons"
	    echo -e "\t\t ... by the local test flag ALLOW_ACCURACY_CHANGE"
	    echo -e "\t\t ... Warning to be issued in case of a successful degraded comparison"
	    echo -e "\t\t ..............."
	fi
	
	if [ "$DO_RUN_3" = "yes" ]; then
	    # generate script to generate input rules for each case
	    \sh util/numerous_input_rules_generator.sh  \
	    "${cd}/test_in/numerous_input_rules_parameters.txt" \
	    "${cd}/results/numerous_input_rules.sh" \
	    "${cd}/results" \
	    "$DO_RESTART_3" "$RES_RESTART_3" 2>>$o2
	    ps1=$?
	    print_status $ps1 "generate script to generate numerous input rules file"
	    cd $cd
	    \mkdir -p TEST_LOGS

	    if [ $ps1 -eq 0 ]; then
		for da in $list_db; do
		    # generate input rules by the previously generated script
		    \sh results/numerous_input_rules.sh \
		    "test_in/numerous_input_rules_parameters.txt" \
		    "results/numerous_input_rules.${da}.txt"  "$da" 2>>$o2
		    ps=$?
		    total=`\grep OUT_INPUT results/numerous_input_rules.${da}.txt | \wc -l | sed 's/^ *//'`
		    print_status $ps "generate numerous (${total}) input rules, $da"
		    # generate input files for the previously generated rules
		    \sh ../util/input_make.sh "results/numerous_input_rules.${da}.txt" 1>>$o1 2>>$o2
		    print_status $ps "generate numerous input files for these rules"
		done
		
		# obtain generic names from one of the input rule file (skip restarts to avoid doubling)
		db1=`echo $list_db | \cut -f 1 -d " "`
		\grep "file_gen" "results/numerous_input_rules.${db1}.txt" | \cut -f 4 -d " " | \sed -e "s/'//g" -e "s/_db_.*//" | \
		\sort | \uniq | while read file_gen; do

		    for da in $list_db; do
			if [ -s results/${file_gen}_${da}.RUNTEST.output ]; then
			    echo -e "  [SKIPPED]\trun:       results/${file_gen}_${da}.inp  > results/...RUNTEST.output"
			else
			    # run the case
			    ( ./wlt_3d_${da}_${cn}.out results/${file_gen}_${da}.inp > results/${file_gen}_${da}.RUNTEST.output 2>>$o2 ) &
			    wait_or_kill $!
			    print_status $? "run: ${file_gen}_${da}.inp"
			fi

			if [ "$DO_RESTART_3" = "yes" ]; then
			if [ -s results/${file_gen}_${da}_restart.RUNTEST.output ]; then
			    echo -e "  [SKIPPED]\trestart:   results/${file_gen}_${da}_restart.inp > results/...restart.RUNTEST.output"
			else
			    # run the restart
			    ( ./wlt_3d_${da}_${cn}.out results/${file_gen}_${da}_restart.inp > results/${file_gen}_${da}_restart.RUNTEST.output 2>>$o2 ) &
			    wait_or_kill $!
			    print_status $? "run: ${file_gen}_${da}_restart.inp"
			fi
			fi

			# compare exact solutions and *.res files
			\sh ../util/compare_results.sh  $file_gen $da  "$DB_GOOD"  "$RES_GOOD"  "$L1_CT" $COMPARE_RESULTS_3 $ALLOW_ACCURACY_CHANGE
			if [ $? -ne 0 ]; then
			    echo -e "\nInternal error in compare_results.sh called from RUN_3.sh"
			    echo -e "Please fix the testing scripts before testing !"
			    exit 1
			fi
		    done

		    # comparing all possible databases
		    for (( i=1; i<=`expr $np2 / 2`; i++ )); do
			i2=`expr $i \* 2`
			i1=`expr $i2 - 1`
			d1=`echo $db_pairs|cut -d " " -f $i1`
			d2=`echo $db_pairs|cut -d " " -f $i2`
			\sh ../util/compare_dbs.sh $file_gen $d1 $d2 "$RES_GOOD" "$L1_CT" $COMPARE_DBS_3 $ALLOW_ACCURACY_CHANGE
			if [ $? -ne 0 ]; then
			    echo -e "\nInternal error in compare_dbs.sh called from RUN_3.sh"
			    echo -e "Please fix the testing scripts before testing !"
			    exit 1
			fi
		    done
		done   # the end of while
	    fi    # the end of [ $ps1 -eq 0 ] section
	    cd ..

	else           # of DO_RUN_3
	    echo "# NOT REQUIRED, by local parameter in ${cd}/test_in/local_test_parameters.txt"
	fi             # of DO_RUN_3
    else               # of local test parameters loading
	echo "# NOT REQUIRED, file not present: ${cd}/test_in/local_test_parameters.txt"
    fi                 # of local test parameters loading
    # clear local variables for each case
    DO_RUN_3=""
    DO_RESTART_3=""
done           # of list_ca cycle


exit 0
