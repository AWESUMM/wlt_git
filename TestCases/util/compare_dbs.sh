#!/bin/sh
# Compare results of a test run for two databases
#  Arguments:
#   $1 base_name                  - current test case, e.g. "case_elliptic_poisson"
#   $2 db1                        - first database name, e.g "db_lines"
#   $3 db2                        - second database name, e.g "db_lines"
#   $4 ending_res                 - station number of files saved in results_good/
#   $5 L1_comparator_threshold     - L1 norm result comparator threshold
#   $6-9                          - 0/1 action markers:
#     6        (exact solution) compare full runs
#     7        (exact solution) compare restarts
#     8        (exact solution) compare restarts from IC file in restart format
#     9        (*.res file) compare full runs, diff/comparator
#     10       allow to change accuracy flag (yes/no)

base_name=$1
db1_name=$2
db2_name=$3
if [ $# -ne 10 ]; then exit 1; fi

#Use high resolution exact solution errors
file_ending_run=".err.exact.soln_HighPrec"
file_ending_restart="_restart.err.exact.soln_HighPrec"
file_ending_restart_from_IC="_IC_as_restart.err.exact.soln_HighPrec"
#result file to compare
ending_res=$4

# set local debug variable (0/1)
LDBG_CMPDBS=1

function print_status() {
    if [ $1 -eq 0 ]; then
	echo -ne "  [OK]     \t"
    elif [ $1 -eq 2 ]; then
	echo -ne "  [WARNING]\t"
    else
	echo -ne "  [FAILED] \t"
    fi
    echo -e "$2"
}

#  Put header for this comparison
echo -e "\n******************************************************" >> TEST.REPORT
echo "Test case  $base_name Compare: $db1_name $db2_name" >> TEST.REPORT


if [ $6 -eq 1 ]; then
    if [ $LDBG_CMPDBS -eq 1 ]; then
	echo -e "\t\t\tresults/${base_name}_${db1_name}${file_ending_run}\n\t\t\tresults/${base_name}_${db2_name}${file_ending_run}"
    fi
    # compare full runs
    \sh ../util/compare_exact_solns.sh \
   results/${base_name}_${db1_name}${file_ending_run} \
   results/${base_name}_${db2_name}${file_ending_run} \
   "Compare full run: ${db1_name}${file_ending_run}, ${db2_name}${file_ending_run}" \
   0 ${10} >> TEST.REPORT
    print_status $? "(DB: ${db1_name}, ${db2_name}) (exact solution) compare full runs"
fi


if [ $7 -eq 1 ]; then
    if [ $LDBG_CMPDBS -eq 1 ]; then
	echo -e "\t\t\tresults/${base_name}_${db1_name}${file_ending_restart}\n\t\t\tresults/${base_name}_${db2_name}${file_ending_restart}"
    fi
    # compare restarts
    \sh ../util/compare_exact_solns.sh \
   results/${base_name}_${db1_name}${file_ending_restart} \
   results/${base_name}_${db2_name}${file_ending_restart} \
   "Compare restarts: ${db1_name}${file_ending_restart}, ${db2_name}${file_ending_restart}" \
   0 ${10} >> TEST.REPORT
    print_status $? "(DB: ${db1_name}, ${db2_name}) (exact solution) compare restarts"
fi


if [ $8 -eq 1 ]; then
    if [ $LDBG_CMPDBS -eq 1 ]; then
	echo -e "\t\t\tresults/${base_name}_${db1_name}${file_ending_restart_from_IC}\n\t\t\tresults/${base_name}_${db2_name}${file_ending_restart_from_IC}"
    fi
    # compare restarts from IC file in restart format
    \sh ../util/compare_exact_solns.sh \
   results/${base_name}_${db1_name}${file_ending_restart_from_IC} \
   results/${base_name}_${db2_name}${file_ending_restart_from_IC} \
   "Compare restarts from IC file in restart format: ${db1_name}${file_ending_restart_from_IC}, ${db2_name}${file_ending_restart_from_IC}" \
   0 ${10} >> TEST.REPORT
    print_status $? "(DB: ${db1_name}, ${db2_name}) (exact solution) compare restarts from IC file in restart format"
fi


if [ $9 -eq 1 ]; then
    if [ $LDBG_CMPDBS -eq 1 ]; then
	echo -e "\t\t\tresults/c${base_name}_${db1_name}${ending_res}\n\t\t\tresults/c${base_name}_${db2_name}${ending_res}"
    fi
    # compare *.res files (full runs)
    echo -e "\t#compare *.res files" >> TEST.REPORT
    \sh ../util/compare_exact_solns.sh \
    results/c${base_name}_${db1_name}${ending_res} \
    results/c${base_name}_${db2_name}${ending_res} \
    "(*.res files) results/${base_name}_${db1_name}${ending_res}, results/${base_name}_${db2_name}${ending_res}" \
    0 ${10}>> TEST.REPORT
    ps=$?
    if [ $ps -ne 0 ]; then
	# start the comparator (compare grid, verbouse level 0, treshold, all variables)
	echo -e "# result file comparator started\n# please examine the log files TEST_LOGS/compare_solns.*.log" >> TEST.REPORT
	../util/compare_solns \
	results/c${base_name}_${db1_name}${ending_res} \
	results/c${base_name}_${db2_name}${ending_res} \
	TEST_LOGS/tmp_compare_solns_out -g -t $5 \
	>> TEST_LOGS/compare_solns.all.log 2>>TEST_LOGS/compare_solns.all.log
	\sh ../util/analyze_compare_solns_output.sh $? TEST_LOGS/tmp_compare_solns_out
	ps=$?
	\cat TEST_LOGS/tmp_compare_solns_out >> TEST_LOGS/compare_solns.out.log 2>>TEST_LOGS/compare_solns.all.log
	\rm -f TEST_LOGS/tmp_compare_solns_out
	print_status $ps "(DB: ${db1_name}, ${db2_name}) (*.res file) compare full runs, result comparator with threshold ${5}"
    else
	# files are identical
	print_status $ps "(DB: ${db1_name}, ${db2_name}) (*.res file) compare full runs, diff"
    fi
fi

# reset local debug variable (0/1)
LDBG_CMPDBS=0

exit 0
