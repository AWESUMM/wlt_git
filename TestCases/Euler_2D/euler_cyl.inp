#------------------------------------------------------------------#
# General input file format                                        #
#                                                                  #
# comments start with # till the end of the line                   #
# string constant are quoted by "..." or '...'                     #
# boolean can be (T F 1 0 on off) in or without ' or " quotes      #
# integers are integers                                            #
# real numbers are whatever (e.g. 1   1.0   1e-23   1.123d-54 )    #
# vector elements are separated by commas                          #
# spaces between tokens are not important                          #
# empty or comment lines are not important                         #
# order of lines is not important                                  #
#------------------------------------------------------------------#

file_gen = 'euler_test2D.'
dimension = 2		#  dim (1,2,3), # of dimensions
IC_restart = F      #  ICrestart
IC_restart_station  = 7            #  ICrestart_station, after restart the next station number to use

IC_from_file = F    # Do a new run with IC from a restart file
IC_file_fmt = 0	    # IC data file format (0 - native restart file, 1-netcdf, 2-A.Wray in fourier space, 3-simple binary) next line is IC filename


 

IC_filename = 'results/ceuler_test.0006.res'



Data_file_format = F    #  T = formatted, F = unformatted

coord_min = -0.5, -0.5, -0.5		#  XMIN, YMIN, ZMIN, etc
coord_max = 1.0, 0.5, 1.0		#  XMAX, YMAX, ZMAX, etc

coord_zone_min = -5e+05, -5e+05, -5e+05	# XMINzone, etc
coord_zone_max = 5e+05, 5e+05, 5e+05	# XMAXzone, etc

eps_init = 2.0000000e-4  	#  EPS used to adapt initial grid  
eps_run = 2.0000000e-4  	#  EPS used in run  
eps_adapt_steps =  0            # eps_adapt_steps ! how many time steps to adapt from eps_init to eps_run

Scale_Meth = 1             	# Scale_Meth !1- Linf, 2-L2
scl_fltwt = 0.0           	#  scl temporal filter weight, scl_new = scl_fltwt*scl_old + ( 1-scl_fltwt)*scl_new
j_mn_init = 2             	#  J_mn_init force J_mn == J_INIT while adapting to IC
j_lev_init = 4           	#  starts adapting IC by having all the points on this level of resolution
j_IC = 4             	#  J_IC if the IC data does not have dimensions in it then mxyz(:)*2^(j_IC-1) is used
J_MN = 2             	#  J_MN
J_MX = 4             	#  J_MX
J_TREE_ROOT = 2         #  j_tree_root - OPTIONAL for db_tree, DEAFALT: j_tree_ro
J_FILT = 20            	#  J_FILT

M_vector = 6,4,6	#  Mx, etc
periodic = 0,0,0	#  prd(:) (0/1) 0: non-periodic; 1: periodic
uniform = 0,0,0		#  grid(:) (0/1) 0: uniform; 1: non-uniform

i_h = 123456        	#  order of boundaries (1-xmin,2-xmax,3-ymin,4-ymax,5-zmin,6-zmax)
i_l = 111111        	#  algebraic/evolution (1/0) BC order: (lrbt)
N_predict = 2             	#  N_predict
N_update = 0             	#  N_update
N_diff = 2             		#  N_diff

IJ_ADJ = 1,1,1		# IJ_ADJ(-1:1) = (coarser level), (same level), (finer level)
ADJ_type = 0,0,0	#  ADJ_type(-1:1) = (coarser level), (same level), (finer level) # (0 - less conservative, 1 - more conservative)

BNDzone = F	  	#  BNDzone
BCtype = 0             	#  BCtype (0 - Dirichlet, 1 - Neuman, 2 - Mixed)


time_integration_method = 1 # 0- meth2, 1 -krylov, 2 - Crank Nicolson 

t_begin = 0.0000e+00   	#  tbeg  
t_end = 0.08000e-00 	#  tend  
dt = 1.0000000e-03 	#  dt
dtmax = 2.000000e-02 	#  dtmax
dtmin = 1.0000000e-08 	#  dtmin-if dt < dtmin then exection stops(likely blowing up)
dtwrite = 4.0000000e-02 	#  dtwrite 1.0000000e-02 
t_adapt = 0.0000000e+06      # when t > t_adapt use an adaptive time step if possible

cflmax = 0.8000000e-00 	#  cflmax
cflmin = 1.0000000e-08 	#  cflmin , Exit if cfl < cflmin

Zero_Mean = F           #  T- enforce zero mean for 1:dim first variables (velocity usually), F- do nothing
eta = 1.0000000e-04 	#  eta

diagnostics_elliptic =  F       #  diagnostics_elliptic: If T print full diagnostic for elliptic solver
GMRESflag = T              	#  GMRESflag
BiCGSTABflag = F              	#  BiCGSTABflag

Jacoby_correction = T           # Jacoby_correction
multigrid_correction = T        # multigrid_correction
GMRES_correction  = T           # GMRES_correction 

kry_p = 4			#  kry_p
kry_p_coarse = 100           	#  kry_p_coarse
len_bicgstab = 6             	#  len_bicgstab
len_bicgstab_coarse = 100	#  len_bicgstab_coarse
len_iter = 3             	#  len_iter

W0 = 1.0000000e+00 	#  W0 underrelaxation factor for inner V-cycle
W1 = 1.0000000e+00 	#  W1 underrelaxation factor on the finest level
W2 = 0.6000000e+00 	#  W2 underrelaxation factor for weighted Jacoby (inner points)
W3 = 1.0000000e-00 	#  W3 underrelaxation factor for weighted Jacoby (boundary points)
W_min = 1.00000e-02	#  W_min correcton factor 
W_max = 1.00000e-00 	#  W_max correction factor 

obstacle = F              	# imask !TRUE  - there is an obstacle defined
itime = 3   	           	# itime ! 0- Euler first-order, 1- Rotational first-order, 2- Second-order, 3-Semi-implicit second-order
obstacle_X = 0,0,0 		#  Xo(:)! Location of obstacle
obstacle_U = 0,0,0		#  Uo(:)! Velocity of obstacle
obstacle_move = 0,0,0		#  1- Obstacle allowed to move in that direction, else == 0


diameter = 1.0		#  d diameter of cylinder as an obstacle
k_star = 8.7400000e+00 	#  k_star
m_star = 5.0000000e+00 	#  m_star
b_star = 0.0000000e-03 	#  b_star
tol1 = 1.0000000e-04 	#  tol1 used to set tolerence for non-solenoidal half-step
tol2 = 1.0000000e-03 	#  tol2 used to set tolerence for solenoidal half-step
tol3 = 1.0e-3           #  tol3 used to set tolerence for time step
tol_gmres = 1.0e-4        #  used to set tolerence for gmres iterative solver
tol_gmres_stop_if_larger = F  # If true stop iterating in gmres solver if error of last iteration was smaller
wlog = T              	#  wlog

dtforce = 0.0           	#  dtforce ! time interval for calculating the lift force
Pressure_force = 0,0,0		#  Pressure forcing term in x-dr

SGS_model = 0             	#  SGS model, 0=none, 1- fixed parameter Smagorinski

# Initial condition and general flow variables
delta = 1.00000000e-04	# delta: width of initial condition
gamma = 1.40000000e-00  # gamma: specific heat ratio
xL = -0.4000000000e-00	# xL: left boundary
xR = 0.40000000000e-00  # xR: right boundary

# Hyperbolic solver variables
hypermodel = 1		# hypermodel: flag to turn on hyperbolic module
eps_low = 1.00000e-04	# eps_low: lower bound error threshold
eps_high = 1.0000e-03	# eps_high: upper bound error threshold
hyper_mode = 1		# hyper_mode: 0 for non-conservative, 1 for conservative
min_wall_type = 1,1,1	#  XMIN, YMIN, ZMIN - 1:reflecting wall, 0: evolutionary
max_wall_type = 0,0,0	#  XMAX, YMAX, ZMAX - 1:reflecting wall, 0: evolutionary

