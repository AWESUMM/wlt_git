MODULE curv
  USE precision
  USE wlt_vars
  USE wavelet_filters_mod
  USE elliptic_mod
  USE elliptic_vars
  USE wlt_trns_mod
  USE wlt_trns_vars
  USE wlt_trns_util_mod
  USE io_3d_vars
  USE util_mod
  USE util_vars
  USE share_consts
  USE pde
  USE variable_mapping 
  USE sizes
  USE share_kry
  USE vector_util_mod
  USE field
  USE input_file_reader
  USE debug_vars

  IMPLICIT NONE
  PUBLIC :: form_curv_jacobian
  PUBLIC :: balancing_function
  PUBLIC :: frobenius_norm
  PUBLIC :: functional_rhs
  
CONTAINS
  FUNCTION Winslow_function (u, n_w, n_e, center, scales, radius, amplitude, sigma) ! the final mesh will be localized near the maximum of this function

	IMPLICIT NONE
	REAL (pr), DIMENSION (n_w, n_e), INTENT(IN) :: u
	INTEGER, INTENT(IN) :: n_w, n_e !(n_w = nwlt -- active wavelets), (n_e) -- number of equations
	REAL (pr), DIMENSION (dim), INTENT(IN) :: center, scales
	REAL (pr), INTENT (IN) :: radius, amplitude, sigma
	REAL (pr) :: Winslow_function (n_w)
	REAL (pr), DIMENSION (n_w) :: r
	INTEGER :: ie
	r=0.0_pr

	DO ie = 1, dim
		r(:) = r(:) + scales(ie)*((u(:,ie)-center(ie))**2)
		!r(:) = x(:,2)-0.5_pr*x(:,1)
		!r(:) = x(:,2)*x(:,1)
	END DO
	
	Winslow_function(:)=SQRT(  1 + amplitude*EXP( (r(:)-radius)**2/(-2*sigma) )  )
	
  END FUNCTION Winslow_function

  FUNCTION balancing_function (u, n_w, n_e, j_j, M, theta, gamm) ! this function makes diffusion uniform

	IMPLICIT NONE
    INTEGER, INTENT(IN) :: n_w, n_e, j_j !(n_w = nwlt -- active wavelets), (n_e) -- number of equations, j_j = j_wlt -- level
	REAL (pr), DIMENSION (n_w,n_e), INTENT(IN) :: u
	REAL (pr), INTENT(IN) :: theta, gamm
	REAL (pr), DIMENSION (dim, n_w, dim), INTENT(IN) :: M
    
	REAL (pr), DIMENSION (n_w) :: balancing_function
    REAL (pr), DIMENSION(1:dim,1:dim, 1:n_w) :: jacobian, JMJ
    REAL (pr), DIMENSION(dim, 1:n_w) :: delta
	REAL (pr), DIMENSION (n_w) :: trace_JMJ
	REAL (pr), DIMENSION (n_w) :: det_M	

	INTEGER :: ie, je, ke
	
	JMJ = 0.0_pr
	trace_JMJ = 0.0_pr
	jacobian(:, :, :) = form_curv_jacobian(u, j_j, n_w)
	det_M=determinant(M, dim, n_w)
	CALL get_all_local_h(delta)

	DO ie = 1, dim
		DO je = 1, dim
			DO ke = 1, dim
				JMJ(ie, je, 1:n_w) = JMJ(ie, je, 1:n_w) + SUM(M(1:dim, 1:n_w, ke)*jacobian(je, 1:dim, 1:n_w),1)*jacobian (ie, ke, 1:n_w)/(delta(ie, 1:n_w)*delta(je, 1:n_w)) 
			END DO
		END DO
		trace_JMJ(1:n_w) = trace_JMJ(1:n_w) + JMJ(ie, ie, 1:n_w)*delta(ie, 1:n_w)*delta(ie, 1:n_w)
	END DO
	
	balancing_function = theta*gamm*dim*frobenius_norm(JMJ, n_w)*ABS(trace_JMJ**(dim*gamm*0.5_pr-1.0_pr))*ABS(det_M)**(-0.5_pr)

  END FUNCTION balancing_function

  FUNCTION frobenius_norm (A, n_w) ! (n_w = nwlt -- active wavelets)
	IMPLICIT NONE
	INTEGER, INTENT(IN) :: n_w
	REAL (pr), DIMENSION (dim, dim, n_w), INTENT(IN) :: A
	REAL (pr), DIMENSION (n_w) :: frobenius_norm	
	frobenius_norm = SQRT(SUM(SUM(A**2,1),1))
  END FUNCTION frobenius_norm

  FUNCTION determinant (A, N, n_w) ! (Determinant of matrix N*N, N = 1, 2, 3) A[i, :, j] - i rows, j columns, n_w - active wavelets
	IMPLICIT NONE
	INTEGER, INTENT(IN) :: N, n_w
	REAL (pr), DIMENSION (dim, n_w, dim), INTENT(IN) :: A
	REAL (pr), DIMENSION (n_w) :: determinant

	IF (N == 1) determinant (:) = A(1, :, 1)

	IF (N == 2) determinant (:) = A(1, :, 1)*A(2, :, 2)-A(1, :, 2)*A(2, :, 1)
	
	IF (N == 3) determinant (:) = A(1, :, 1)*( A(2, :, 2)*A(3, :, 3) - A(2, :, 3)*A(3, :, 2) ) &
							- A(1, :, 2)*( A(2, :, 1)*A(3, :, 3) - A(2, :, 3)*A(3, :, 1) ) &
							+ A(1, :, 3)*( A(2, :, 1)*A(3, :, 2) - A(2, :, 2)*A(3, :, 1) )

	IF (N > 3) STOP

  END FUNCTION determinant

! The function below compute a functional dependent part of the MMPDE. It has two indexes both from 1 to dim.
! x_phys - physical coordinates, inv_M [i, :, j] - INVERSE of the monitor function - i rows, j columns.
! n_w - active wavelets, gamm (>0), theta ([0,1]) - parameters of the functional
! j_j = j_wlt -- level
  FUNCTION functional_rhs (x_phys, inv_M, theta, gamm, n_w, j_j)
	IMPLICIT NONE
	INTEGER, INTENT(IN) :: n_w, j_j
	REAL (pr), DIMENSION (n_w, dim), INTENT(IN) :: x_phys
	REAL (pr), DIMENSION (dim, n_w, dim), INTENT(IN) :: inv_M
	REAL (pr), INTENT(IN) :: theta, gamm
	REAL (pr), DIMENSION (dim, n_w, dim) :: du, du_dummy
	REAL (pr), DIMENSION (n_w) :: det_inv_M, det_J ! determinant of INVERSE monitor function and of J = dx_phys/dx_comp
	REAL (pr), DIMENSION (dim, dim, n_w) :: jacobian ! matrix - dx_comp/dx_phys
	REAL (pr), DIMENSION (dim, n_w, dim) :: functional_rhs
	REAL (pr), DIMENSION (n_w) :: trace_JMJ
	INTEGER :: ie, je, ke
	
	CALL c_diff_fast (x_phys(1:n_w, 1:dim), du(1:dim,1:n_w,1:dim), du_dummy, j_j, n_w, 1, 10, dim, 1, dim)
	jacobian = form_curv_jacobian(x_phys, j_j, n_w)

	! determinants		
	det_inv_M = determinant (inv_M, dim, n_w)
	det_J = determinant (du, dim, n_w)
	
	trace_JMJ = 0.0_pr
	DO ie = 1, dim
		DO je = 1, dim
			trace_JMJ (1:n_w) = trace_JMJ (1:n_w) + inv_M(ie, 1:n_w, je)*SUM(jacobian(:,ie,1:n_w)*jacobian(:,je,1:n_w),1)
		END DO
	END DO

	DO ie = 1, dim
		DO je = 1, dim
			functional_rhs (ie, 1:n_w, je) = gamm*(1.0_pr-2.0_pr*theta)*(dim**(dim*gamm*0.5_pr))*ABS(det_inv_M (1:n_w) )**( (gamm-1.0_pr)*0.5_pr )*ABS(det_J (1:n_w))**( -gamm )*du (ie, 1:n_w, je)
			DO ke = 1, dim
				functional_rhs (ie, 1:n_w, je) = functional_rhs (ie, 1:n_w, je) + theta*dim*gamm*ABS(det_inv_M (1:n_w))**(-0.5_pr)*inv_M(ie, 1:n_w, ke)*jacobian(ke, je, 1:n_w)*(trace_JMJ**(dim*gamm*0.5_pr-1.0_pr))
			END DO
		END DO
	END DO

  END FUNCTION functional_rhs

FUNCTION check_mesh_quality(x_phys, j_j, n_w)
	IMPLICIT NONE
	INTEGER, INTENT(IN) :: n_w, j_j
	REAL (pr), DIMENSION (n_w, dim), INTENT(IN) :: x_phys
	REAL (pr), DIMENSION (dim, dim, n_w) :: jacobian
	REAL (pr), DIMENSION (n_w) :: det_J 
	REAL (pr), DIMENSION (dim, n_w, dim) :: du, du_dummy
	REAL (pr), DIMENSION (n_w) :: check_mesh_quality	
	INTEGER :: ie
	CALL c_diff_fast (x_phys(1:n_w, 1:dim), du(1:dim,1:n_w,1:dim), du_dummy, j_j, n_w, 1, 10, dim, 1, dim)
	
	det_J = determinant(du, dim, n_w)

	check_mesh_quality(1:n_w) = 1.0_pr/ABS(det_J)

	DO ie = 1, dim
		check_mesh_quality(1:n_w) = check_mesh_quality(1:n_w)*ABS(du(ie, :, ie))
	END DO
	check_mesh_quality(1:n_w)=LOG(ABS(check_mesh_quality(1:n_w)))	

END FUNCTION check_mesh_quality

FUNCTION form_curv_jacobian(x_phys, j_j, n_w) ! (j_j = j_wlt -- level), (n_w = nwlt -- active wavelets)
    USE precision
    USE wlt_vars
    USE wlt_trns_mod 
    USE parallel 
    USE curvilinear
    USE error_handling 
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: n_w
    INTEGER, INTENT(IN) :: j_j
    REAL (pr), DIMENSION(1:n_w,1:dim), INTENT(IN) :: x_phys
    INTEGER :: i, j, k
    REAL (pr), DIMENSION(1:dim,1:n_w,1:dim) :: du, du_dummy
    REAL (pr), DIMENSION(1:n_w)             :: jacobian_det 
    REAL (pr), DIMENSION(1:dim,1:dim,1:n_w) :: form_curv_jacobian
    INTEGER :: flag

    CALL c_diff_fast (x_phys(1:n_w, 1:dim), du(1:dim,1:n_w,1:dim), du_dummy, j_j, n_w, 1, 10, dim, 1, dim)

    IF( dim .EQ. 3) THEN
      jacobian_det(:) =    du(1,:,1) * ( du(2,:,2) * du(3,:,3) - du(2,:,3) * du(3,:,2) ) &
                          - du(1,:,2) * ( du(2,:,1) * du(3,:,3) - du(2,:,3) * du(3,:,1) ) &
                          + du(1,:,3) * ( du(2,:,1) * du(3,:,2) - du(2,:,2) * du(3,:,1) ) 
      flag = 0
      IF ( MINVAL( ABS( jacobian_det ) ) .LT. 1.0e-10_pr ) flag = 1
      CALL parallel_global_sum( INTEGERMAXVAL= flag )
      IF ( flag .EQ. 1 ) THEN
        CALL error('Transform Jacobian Determinant is zero.')
      ELSE
        jacobian_det(:) = 1.0_pr/jacobian_det(:) 
      END IF

      form_curv_jacobian(1,1,:) =   jacobian_det(:) * (du(2,:,2)*du(3,:,3) - du(2,:,3)*du(3,:,2)) ! d xi / dx
      form_curv_jacobian(2,1,:) = - jacobian_det(:) * (du(2,:,1)*du(3,:,3) - du(2,:,3)*du(3,:,1)) ! d eta / dx
      form_curv_jacobian(3,1,:) =   jacobian_det(:) * (du(2,:,1)*du(3,:,2) - du(2,:,2)*du(3,:,1)) ! d zeta / dx

      form_curv_jacobian(1,2,:) = - jacobian_det(:) * (du(1,:,2)*du(3,:,3) - du(1,:,3)*du(3,:,2)) ! d xi / dy
      form_curv_jacobian(2,2,:) =   jacobian_det(:) * (du(1,:,1)*du(3,:,3) - du(1,:,3)*du(3,:,1)) ! d eta / dy
      form_curv_jacobian(3,2,:) = - jacobian_det(:) * (du(1,:,1)*du(3,:,2) - du(1,:,2)*du(3,:,1)) ! d zeta / dy

      form_curv_jacobian(1,3,:) =   jacobian_det(:) * (du(1,:,2)*du(2,:,3) - du(1,:,3)*du(2,:,2)) ! d xi / dz
      form_curv_jacobian(2,3,:) = - jacobian_det(:) * (du(1,:,1)*du(2,:,3) - du(1,:,3)*du(2,:,1)) ! d eta / dz
      form_curv_jacobian(3,3,:) =   jacobian_det(:) * (du(1,:,1)*du(2,:,2) - du(1,:,2)*du(2,:,1)) ! d zeta / dz
           
    ELSE IF( dim .EQ. 2) THEN

      jacobian_det(:) =    du(1,:,1) * du(2,:,2) &
                         - du(1,:,2) * du(2,:,1) 
      flag = 0
      IF ( MINVAL( ABS( jacobian_det ) ) .LT. 1.0e-10_pr ) flag = 1
      CALL parallel_global_sum( INTEGERMAXVAL= flag )
      IF ( flag .EQ. 1 ) THEN
        CALL error('Transform Jacobian Determinant is zero.')
      ELSE
        jacobian_det(:) = 1.0_pr/jacobian_det(:) 
      END IF

      form_curv_jacobian(1,1,:) =   jacobian_det(:) * du(2,:,2) ! d xi / dx 
      form_curv_jacobian(2,1,:) = - jacobian_det(:) * du(2,:,1) ! d eta / dx

      form_curv_jacobian(1,2,:) = - jacobian_det(:) * du(1,:,2) ! d xi / dy
      form_curv_jacobian(2,2,:) =   jacobian_det(:) * du(1,:,1) ! d eta / dy

    ELSE IF( dim .EQ. 1) THEN
      flag = 0
      IF ( MINVAL( ABS( du(1,:,1) ) ) .LT. 1.0e-10_pr ) flag = 1
      CALL parallel_global_sum( INTEGERMAXVAL= flag )
      IF ( flag .EQ. 1 ) THEN
        CALL error('Transform Jacobian Determinant is zero.')
      ELSE
        form_curv_jacobian(1,1,:) =    1.0_pr/du(1,:,1)  
      END IF
    ELSE
      CALL error( 'UNKNOWN DIMENSIONALITY (CURVILINEAR JACOBIAN).' )
    END IF
   
  END FUNCTION form_curv_jacobian

END MODULE curv
