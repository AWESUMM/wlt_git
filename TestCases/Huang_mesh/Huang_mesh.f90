MODULE user_case
  USE precision
  USE wlt_vars
  USE wavelet_filters_mod
  USE elliptic_mod
  USE elliptic_vars
  USE wlt_trns_mod
  USE wlt_trns_vars
  USE wlt_trns_util_mod
  USE io_3d_vars
  USE util_mod
  USE util_vars
  USE share_consts
  USE pde
  USE variable_mapping 
  USE sizes
  USE share_kry
  USE vector_util_mod
  USE field
  USE input_file_reader
  USE debug_vars
  USE curv

  PRIVATE
  INTEGER n_var_pressure, n_var_quality  ! pressure = Winslow function
  INTEGER, ALLOCATABLE :: n_Xphys(:)     ! Physical coordinates
  REAL (pr) :: tau ! tuning variable

  ! This set is use in any case
  REAL (pr) :: sigma ! dispersion
  REAL (pr) :: amplitude ! amplitude

  ! This set is used for elliptic perturbation. 2D and 3D
  REAL (pr), ALLOCATABLE :: center(:) ! center coordinate for the "monitor function"
  REAL (pr) :: radius ! radius of the circle
  REAL (pr), ALLOCATABLE :: scales(:) ! compress x, y, z axes

  REAL (pr) :: theta, gamm ! parameters of the functional

CONTAINS

  SUBROUTINE  user_setup_pde ( VERB ) 
    USE variable_mapping
    IMPLICIT NONE
    LOGICAL, OPTIONAL :: VERB
    INTEGER :: i

    IF (dim .GE. 1) CALL register_var( 'xCoord ', integrated=.TRUE.,  adapt=(/.TRUE.,.TRUE./),   interpolate=(/.TRUE.,.TRUE./),   exact=(/.FALSE.,.FALSE./),   saved=.TRUE., req_restart=.TRUE., IC_MAP_NAME='xCoord' )
    IF (dim .GE. 2) CALL register_var( 'yCoord ', integrated=.TRUE.,  adapt=(/.TRUE.,.TRUE./),   interpolate=(/.TRUE.,.TRUE./),   exact=(/.FALSE.,.FALSE./),   saved=.TRUE., req_restart=.TRUE., IC_MAP_NAME='yCoord' )
    IF (dim .GE. 3) CALL register_var( 'zCoord ', integrated=.TRUE.,  adapt=(/.TRUE.,.TRUE./),   interpolate=(/.TRUE.,.TRUE./),   exact=(/.FALSE.,.FALSE./),   saved=.TRUE., req_restart=.TRUE., IC_MAP_NAME='zCoord' )
    IF (dim .GE. 4) STOP ! There is no 4th coordinate in the real world
    
    CALL register_var( 'pressure ', integrated=.FALSE., adapt=(/.FALSE.,.FALSE./), interpolate=(/.FALSE.,.FALSE./), exact=(/.FALSE.,.FALSE./), saved=.TRUE., req_restart=.FALSE., IC_MAP_NAME='pressure_old' )
	CALL register_var( 'quality ', integrated=.FALSE., adapt=(/.FALSE.,.FALSE./), interpolate=(/.FALSE.,.FALSE./), exact=(/.FALSE.,.FALSE./), saved=.TRUE., req_restart=.FALSE., IC_MAP_NAME='quality' )
    CALL setup_mapping()
    CALL print_variable_registery( FULL=.TRUE.)

    IF (ALLOCATED(n_Xphys)) DEALLOCATE(n_Xphys)
    ALLOCATE( n_Xphys(dim) )
    IF (dim .GE. 1) THEN 
	n_Xphys(1) = get_index('  xCoord    ')
    END IF
    IF (dim .GE. 2) THEN
	n_Xphys(2) = get_index('  yCoord    ')
    END IF
    IF (dim .GE. 3) THEN 
	n_Xphys(3) = get_index('  zCoord    ')  
    END IF

    n_var_pressure  = get_index('pressure ') 
	n_var_quality  = get_index('quality ') 

    ALLOCATE ( Umn(1:n_var) )
    Umn = 0.0_pr 

    IF (verb_level.GT.0) THEN
       PRINT *, 'n_integrated = ',n_integrated 
       PRINT *, 'n_var = ',n_var 
       PRINT *, 'n_var_exact = ',n_var_exact 
       PRINT *, '*******************Variable Names*******************'
       DO i = 1,n_var
          WRITE (*, u_variable_names_fmt) u_variable_names(i)
       END DO
       PRINT *, '****************************************************'
    END IF

  END SUBROUTINE  user_setup_pde

  SUBROUTINE  user_exact_soln (u, nlocal,  t_local, l_n_var_exact_soln)
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: nlocal
    REAL (pr), INTENT (IN) ::  t_local
    REAL (pr), DIMENSION (nlocal,n_var_exact), INTENT (INOUT) :: u
    LOGICAL , INTENT (IN) :: l_n_var_exact_soln(n_var)
    REAL (pr) :: t_zero
    INTEGER :: i

  END SUBROUTINE  user_exact_soln

  SUBROUTINE user_initial_conditions (u, nlocal, ne_local, t_local, scl, scl_fltwt, iter)
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: nlocal, ne_local
    INTEGER  , INTENT (INOUT) :: iter 
    REAL (pr), DIMENSION (nlocal,ne_local), INTENT (INOUT) :: u
    REAL (pr)  :: scl(1:n_var),scl_fltwt
    REAL (pr), INTENT (IN) :: t_local

    u(:,:)=x(:,:) ! initial conditions: xPhys=xComp, yPhys=yComp, zPhys=zComp

  END SUBROUTINE user_initial_conditions

  SUBROUTINE user_algebraic_BC (Lu, u, nlocal, ne_local, jlev, meth)
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: jlev, meth, ne_local, nlocal
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (INOUT) :: Lu
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (IN)    :: u

    INTEGER :: i, ie, ii, shift, jj
    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc
    REAL (pr), DIMENSION (ne_local, nlocal, dim) :: du, d2u
    CALL c_diff_fast (u, du, d2u, jlev, nlocal, meth, 10, ne_local, 1, ne_local)

    DO ie = 1, dim ! Change (dim -> ne_local) if there are additional variables apart from n_Xphys
       shift = nlocal*(ie-1)
       i_p_face(0) = 1
       DO i=1, dim
          i_p_face(i) = i_p_face(i-1)*3
       END DO
       DO face_type = 0, 3**dim - 1
          face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
          IF( ANY( face(1:dim) /= 0) ) THEN
             CALL get_all_indices_by_face (face_type, jlev, nloc, iloc)
             IF(nloc > 0 ) THEN
		DO jj = 1, dim
		    IF( ABS(face(jj)) == 1) THEN
	                !IF (jj == ie) THEN
				Lu(shift+iloc(1:nloc)) = u(shift+iloc(1:nloc)) ! Dirichlet boundary condition on each -1 face
			!ELSE IF (jj .NE. ie) THEN
			!	Lu(shift+iloc(1:nloc)) = du(ie,  iloc(1:nloc), jj) 
			!END IF
		    END IF
		END DO
             END IF
          END IF
       END DO
    END DO

  END SUBROUTINE user_algebraic_BC

  SUBROUTINE user_algebraic_BC_diag (Lu_diag, nlocal, ne_local, jlev, meth)
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: jlev, meth, ne_local, nlocal
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (INOUT) :: Lu_diag

    INTEGER :: i, ie, ii, jj, shift
    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc
    REAL (pr), DIMENSION (nlocal,dim) :: du, d2u

    CALL c_diff_diag ( du, d2u, jlev, nlocal, meth, meth, 10)
    	
    DO ie = 1, ne_local
       shift = nlocal*(ie-1)
       i_p_face(0) = 1
       DO i=1, dim
          i_p_face(i) = i_p_face(i-1)*3
       END DO
       DO face_type = 0, 3**dim - 1
          face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
          IF( ANY( face(1:dim) /= 0) ) THEN
             CALL get_all_indices_by_face (face_type, jlev, nloc, iloc)
             IF(nloc > 0 ) THEN
		DO jj = 1, dim
		    IF( ABS(face(jj)) == 1  ) THEN
	                !IF (jj == ie) THEN
	                    Lu_diag(shift+iloc(1:nloc)) = 1.0_pr ! Dirichlet boundary condition on each -1 face
		        !ELSE IF (jj .NE. ie) THEN
		        !    Lu_diag(shift+iloc(1:nloc)) = du(iloc(1:nloc), jj) 
		        !END IF
		    END IF
		END DO
             END IF
          END IF
       END DO
    END DO


 
  END SUBROUTINE user_algebraic_BC_diag

  SUBROUTINE user_algebraic_BC_rhs (rhs, ne_local, nlocal, jlev)
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: ne_local, nlocal, jlev
    REAL (pr), DIMENSION (nlocal*ne_local), INTENT (INOUT) :: rhs

    INTEGER :: i, ie, ii, jj, shift
    INTEGER :: face_type, nloc
    INTEGER, DIMENSION(0:dim) :: i_p_face
    INTEGER, DIMENSION(dim) :: face
    INTEGER, DIMENSION(nwlt) :: iloc

    DO ie = 1, dim
       shift = nlocal*(ie-1)
       i_p_face(0) = 1
       DO i=1,dim
          i_p_face(i) = i_p_face(i-1)*3
       END DO
       DO face_type = 0, 3**dim - 1
          face = INT(MOD(face_type,i_p_face(1:dim))/i_p_face(0:dim-1))-1
          IF( ANY( face(1:dim) /= 0) ) THEN
             CALL get_all_indices_by_face (face_type, jlev, nloc, iloc)
             IF(nloc > 0 ) THEN
		DO jj = 1, dim
		    IF( ABS(face(jj)) == 1  ) THEN
	                !IF (jj == ie) THEN
	                    rhs(shift+iloc(1:nloc)) = x(iloc(1:nloc), ie) ! Dirichlet boundary condition on each -1 face
		        !ELSE IF (jj .NE. ie) THEN
		        !    rhs(shift+iloc(1:nloc)) = 0 
		        !END IF
		    END IF
		END DO
             END IF
          END IF
       END DO
    END DO

  END SUBROUTINE user_algebraic_BC_rhs

  SUBROUTINE user_project (u, p, nlocal, meth)
    IMPLICIT NONE

    INTEGER, INTENT (IN) :: meth, nlocal
    REAL (pr), DIMENSION (nlocal),     INTENT(INOUT) :: p
    REAL (pr), DIMENSION (nlocal,n_integrated), INTENT(INOUT) :: u

    REAL (pr), DIMENSION (nlocal) :: dp
    REAL (pr), DIMENSION (nlocal) :: f

    INTEGER :: i
	INTEGER, PARAMETER :: ne = 1
    INTEGER, DIMENSION(ne) :: clip 


  END SUBROUTINE user_project

  FUNCTION user_rhs (u_integrated)	 
    IMPLICIT NONE
    REAL (pr), DIMENSION (ng, dim), INTENT(IN) :: u_integrated
    REAL (pr), DIMENSION (n) :: user_rhs
    REAL (pr), DIMENSION (ng, dim) :: f
    INTEGER :: ie, je, ke, le, shift
    INTEGER, PARAMETER :: meth=1
    REAL (pr), DIMENSION (dim, dim, ng) :: jacobian ! Format J_{i,j} (i, j, :)
    REAL (pr), DIMENSION (ng) :: winslow, balancing ! Winslow function -- w(x,t) and p(x,t). x -- physical coordinates 
    REAL (pr), DIMENSION(dim, ng, dim) :: du, d2u_dummy ! only first derivative is needed. Format [D_j x_i] du(i, :, j)
    REAL (pr), DIMENSION (dim, ng, dim, dim) :: d_func_rhs ! derivative of W*J. Format [D_k (WJ)_{i,j}] (j, :, i, k)
	REAL (pr), DIMENSION (dim, ng, dim) :: inv_monitor_function, func_rhs
    user_rhs = 0.0_pr
    
    Winslow(1:ng) = Winslow_function (u_integrated, ng, ne, center, scales, radius, amplitude, sigma) ! Set up Winslow function

	inv_monitor_function(1,:,1) = 0.1_pr/Winslow
	inv_monitor_function(2,:,2) = 2.0_pr/Winslow
	inv_monitor_function(1,:,2) = 0
	inv_monitor_function(2,:,1) = 0
	
	    
	balancing (1:ng) = balancing_function (u_integrated, ng, ne, j_lev, inv_monitor_function, theta, gamm) ! Set up balancing function
    jacobian(1:dim,1:dim,:) = form_curv_jacobian(u_integrated, j_lev, ng) ! Calculate jacobian
    CALL c_diff_fast(u_integrated, du, d2u_dummy, j_lev, ng, meth, 10, dim, 1, dim) ! First derivative for r.h.s.
	
	func_rhs=functional_rhs (u_integrated, inv_monitor_function, theta, gamm, ng, j_lev)

    ! Derivative of rhs
    DO ie = 1, dim
		CALL c_diff_fast( func_rhs(ie, 1:ng, 1:dim), d_func_rhs( 1:dim, 1:ng, ie, 1:dim), d2u_dummy, j_lev, ng, meth, 10, dim, 1, dim )	
    END DO

    ! There is no IMEX here
    DO ie = 1, dim
        shift = ng*(ie-1)
		DO je = 1, dim
	    	DO ke = 1, dim
				DO le = 1, dim
			    	user_rhs(shift+1:shift+ng) = user_rhs(shift+1:shift+ng) + du(ie, :, je)*jacobian(ke, le, :)*d_func_rhs(je, :, le, ke)
				END DO
	    	END DO
		END DO
		user_rhs(shift+1:shift+ng) = user_rhs(shift+1:shift+ng)/(-tau*balancing(:))
    END DO

  END FUNCTION user_rhs


  FUNCTION user_Drhs (u, u_prev_timestep_loc, meth)
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: meth
    REAL (pr), DIMENSION (ng,ne) :: u, u_prev_timestep_loc
    REAL (pr), DIMENSION (n) :: user_Drhs
    REAL (pr), DIMENSION (ng,ne) :: f
 
    INTEGER :: ie, shift
    REAL (pr), DIMENSION (2*ne,ng,dim) :: du
    REAL (pr), DIMENSION (ne  ,ng,dim) :: d2u
    REAL (pr), DIMENSION (ne  ,ng,dim) :: du_dummy 
    user_Drhs=0.0_pr

  END FUNCTION user_Drhs

  FUNCTION user_Drhs_diag (meth)
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: meth
    REAL (pr), DIMENSION (n) :: user_Drhs_diag

    INTEGER :: ie, shift
    REAL (pr), DIMENSION (ng,dim) :: du, d2u
    REAL (pr), DIMENSION (ne,ng,dim) :: du_prev_timestep
    REAL (pr), DIMENSION (ne  ,ng,dim) :: du_dummy
    user_Drhs_diag=0.0_pr

 END FUNCTION user_Drhs_diag

  FUNCTION user_chi (nlocal, t_local )
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: nlocal
    REAL (pr), INTENT (IN) :: t_local
    REAL (pr), DIMENSION (nlocal) :: user_chi

    user_chi = 0.0_pr
  END FUNCTION user_chi


  FUNCTION user_mapping ( xlocal, nlocal, t_local )
    USE curvilinear
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: nlocal
    REAL (pr), INTENT (IN) :: t_local
    REAL (pr), DIMENSION (nlocal,dim), INTENT(IN) :: xlocal
    REAL (pr), DIMENSION (nlocal,dim) :: user_mapping

    user_mapping(:,1:dim) = xlocal(:,1:dim)

  END FUNCTION user_mapping

  SUBROUTINE user_stats ( u ,j_mn, startup_flag)
    IMPLICIT NONE
    INTEGER , INTENT (IN) :: startup_flag
    INTEGER , INTENT (IN) :: j_mn 
    REAL (pr), DIMENSION (nwlt,1:n_var), INTENT (IN) :: u
  END SUBROUTINE user_stats


  SUBROUTINE user_cal_force (u, n, t_local, force, drag, lift)
    IMPLICIT NONE

    INTEGER, INTENT (IN) :: n
    REAL (pr), INTENT (IN) :: t_local
    REAL (pr), DIMENSION (dim), INTENT (INOUT) :: force
    REAL (pr), INTENT (OUT) :: drag, lift
    REAL (pr), DIMENSION (n,dim) :: u
    drag = 0.0_PR
    lift = 0.0_PR
  END SUBROUTINE user_cal_force


  SUBROUTINE user_read_input()
    IMPLICIT NONE

  IF (ALLOCATED(center)) DEALLOCATE(center)
  ALLOCATE( center(dim) )

  IF (ALLOCATED(scales)) DEALLOCATE(scales)
  ALLOCATE( scales(dim) )
  
  call input_real ('tau', tau, 'stop', &
       ' tau: tuning coefficient')

  call input_real ('amplitude', amplitude, 'stop', &
       ' amplitude: amplitude of perturbation')


  call input_real ('radius', radius, 'stop', 'radius of the circle')

  IF (dim .GE. 1) THEN
      call input_real ('x_0', center(1), 'stop', &
       ' x_0: monitor function is localized nearby')
  END IF

  IF (dim .GE. 2) THEN
      call input_real ('y_0', center(2), 'stop', &
       ' y_0: monitor function is localized nearby')
  END IF

  IF (dim .GE. 3) THEN
      call input_real ('z_0', center(3), 'stop', &
       ' z_0: monitor function is localized nearby')
  END IF

  call input_real ('sigma', sigma, 'stop', &
       ' sigma: dispersion of the monitor function')

  
  IF (dim .GE. 1) THEN
      call input_real ('a', scales(1), 'stop', &
       ' a: a(x-x_0)**2 + ...')
  END IF

  IF (dim .GE. 2) THEN
      call input_real ('b', scales(2), 'stop', &
       ' b: ... + b(y-y_0)**2 + ...')
  END IF

  IF (dim .GE. 3) THEN
      call input_real ('c', scales(3), 'stop', &
       ' c: ... + c(z-z_0)**2')  
  END IF

  call input_real ('theta', theta, 'stop', &
       ' theta: balance equidistribution and alignment')

  call input_real ('gamm', gamm, 'stop', &
       ' gamma: tuning coefficient')

  END SUBROUTINE user_read_input


  SUBROUTINE user_additional_vars( t_local, flag )
    IMPLICIT NONE
    REAL (pr), INTENT (IN) ::  t_local
    INTEGER , INTENT(IN) :: flag
    INTEGER :: i
    u(:,n_var_pressure)=Winslow_function(u, nwlt, dim, center, scales, radius, amplitude, sigma)
	u(:,n_var_quality)=check_mesh_quality(u, j_lev, nwlt)
  END SUBROUTINE user_additional_vars


  SUBROUTINE user_scalar_vars( flag )
    IMPLICIT NONE
    INTEGER , INTENT(IN) :: flag



  END SUBROUTINE user_scalar_vars


  SUBROUTINE user_scales(flag, use_default, u, nlocal, ne_local, l_n_var_adapt , l_n_var_adapt_index, &
       scl, scl_fltwt)
    IMPLICIT NONE
    INTEGER, INTENT (IN) :: flag
    LOGICAL , INTENT(INOUT) :: use_default
    INTEGER, INTENT (IN) :: nlocal, ne_local
    REAL (pr), DIMENSION (1:nlocal,1:ne_local), INTENT (IN) :: u
    LOGICAL , INTENT (IN) :: l_n_var_adapt(ne_local)
    INTEGER , INTENT (IN) :: l_n_var_adapt_index(1:ne_local)
    REAL (pr), DIMENSION (1:ne_local), INTENT (INOUT) :: scl
    REAL (pr) ,  INTENT(IN) ::scl_fltwt

    use_default = .TRUE. 

  END SUBROUTINE user_scales

SUBROUTINE user_cal_cfl (use_default, u, cfl_out)
  USE precision
  USE sizes
  USE pde
  IMPLICIT NONE
  LOGICAL , INTENT(INOUT) :: use_default
  REAL (pr),                                INTENT (INOUT) :: cfl_out
  REAL (pr), DIMENSION (nwlt,n_integrated), INTENT (IN)    :: u

  REAL (pr), DIMENSION(dim,nwlt) :: h_arr
  REAL (pr), DIMENSION (nwlt) :: f
 
  REAL (pr), DIMENSION (nwlt, dim) :: du, d2u
  use_default = .FALSE.
  
  cfl_out=1.0_pr

END SUBROUTINE user_cal_cfl


SUBROUTINE user_init_sgs_model( )
  IMPLICIT NONE

END SUBROUTINE user_init_sgs_model

SUBROUTINE  user_sgs_force (u_loc, nlocal)
  IMPLICIT NONE

  INTEGER,                         INTENT (IN) :: nlocal
  REAL (pr), DIMENSION (nlocal,n_integrated), INTENT (INOUT) :: u_loc

END SUBROUTINE  user_sgs_force

  FUNCTION user_sound_speed (u, neq, nwlt)
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: nwlt, neq
    REAL (pr), DIMENSION (nwlt,neq), INTENT(IN) :: u
    REAL (pr), DIMENSION (nwlt) :: user_sound_speed

    user_sound_speed(:) = 0.0_pr 

  END FUNCTION user_sound_speed

  SUBROUTINE  user_pre_process
    IMPLICIT NONE
     
  END SUBROUTINE user_pre_process

  SUBROUTINE  user_post_process
    IMPLICIT NONE
    
  END SUBROUTINE user_post_process

END MODULE user_case
