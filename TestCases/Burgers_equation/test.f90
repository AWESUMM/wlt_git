PROGRAM test
	IMPLICIT NONE
	INTEGER :: i
	INTEGER, PARAMETER :: nwlt=100
	INTEGER, DIMENSION (nwlt) :: u, v
	DO i = 1, nwlt
		u(i) = 3*i
		v(i) = i*i
	END DO
	OPEN (UNIT=100, FILE='output_test', FORM='formatted')
	DO i = 1, nwlt
		WRITE (100, FMT="(2(i6,1x))") u(i), v(i)
	END DO
	CLOSE(100)
	PRINT *, 'Saving coordinates: output_test'
END PROGRAM test
